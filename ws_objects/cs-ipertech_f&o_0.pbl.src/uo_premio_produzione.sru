﻿$PBExportHeader$uo_premio_produzione.sru
forward
global type uo_premio_produzione from nonvisualobject
end type
end forward

global type uo_premio_produzione from nonvisualobject
end type
global uo_premio_produzione uo_premio_produzione

type variables
//Nome origine dati ODBC, UID e PWD del database delle presenze
string is_odbc_presenze
string is_uid_presenze
string is_pwd_presenze
//---------------------------------------------------------------------------------------------------

//variabile codice azienda nel database presenze
string is_cod_azienda = ""

//---------------------------------------------------------------------------------------------------

//variabile valore colonna gruppo in tabella gruppi_anag_a
//corrispondente ai centri di costo di Apice
//nella tabella gruppi_anag_a, solo quelli con gruppo pari a questo valore
//corrispondono ai centri di costo
string is_gruppo = "0020"

//valore della colonna gruppo in tabella dipen, per fare in modo che le ore
//ordinarie, starordinarie, flessibili e diaria non vengano conteggiate
//nel calcolo di OL reparto e quindi RP reparto
string is_escludi_da_rp = ""			//"QUADRO"

//---------------------------------------------------------------------------------------------------

//variabile transazione per la connessione al datrabase presenze
transaction itran_presenze

//---------------------------------------------------------------------------------------------------

//abilita log su file
boolean ib_log = true

//percorso del file di log
string is_path_log

//prefissi errore nel file di log
string is_prefisso_errore = "#################### "

//livelli di log (0 nessuno, 1 basso, 2 alto)
//se vale 1 o 2 ma ib_log=false allora considera come ii_verbose=0
integer ii_verbose = 1

//---------------------------------------------------------------------------------------------------

//var booleana che attiva una window di attesa durante le elaborazioni
boolean ib_apri_wait = true

//---------------------------------------------------------------------------------------------------

////causali legate a lavoro ordinario, straordinario, flessibile, ritardo
////contenute in vettori e in stringhe tipo ('CAUS1','CAUS2', ....)
string is_ordinarie[]
string is_straordinarie[]
string is_flessibilita[]
string is_nonordinarie[]
string is_ritardo[]
string is_diaria[]

string is_sql_ordinarie
string is_sql_straordinarie
string is_sql_flessibilita
string is_sql_nonordinarie
string is_sql_ritardo
string is_sql_diaria


end variables

forward prototypes
public function integer uof_disconnetti_presenze ()
public function integer uof_add_log (string fs_valore)
public function integer uof_path_log ()
public function string uof_date_format_msaccess (datetime fdt_datetime)
public function boolean uof_crea_ds_tran (ref datastore fds_data, string fs_sql, transaction ft_tran, ref string fs_msg)
public function integer uof_connetti_presenze (string fs_errore)
public function integer uof_operai_reparto (string fs_cod_reparto, datetime fdt_data_inizio, datetime fdt_data_fine, ref string fs_cod_operai[], ref string fs_matricole[], ref string fs_errore)
public function time uof_orelav_totime (long fl_ore)
public function integer uof_orelav_matricola_data (string fs_matricola, datetime fdt_data_dal, datetime fdt_data_al, ref decimal fd_ore, ref string fs_errore)
public function integer uof_orelav_matricola_data (long fl_matricola, datetime fdt_data_dal, datetime fdt_data_al, ref decimal fd_ore, ref string fs_errore)
public function integer uof_get_causali (string fs_errore)
public function integer uof_get_cdc_presenze (ref string fs_cdc[], ref string fs_cdc_des[], ref string fs_errore)
public subroutine uof_get_inizio_fine_mese (long fl_num_mese, long fl_anno, ref datetime fdt_data_dal, ref datetime fdt_data_al, ref datetime fdt_date_mese[])
public function integer uof_fatbol_cdc (datetime fdt_data_dal, datetime fdt_data_al, string fs_tipo, ref string fs_cdc[], ref decimal fd_importi[], ref decimal fd_totale, ref string fs_errore)
public function integer uof_fatbol_cdc_percent (datastore fds_regole, long fl_anno_reg, long fl_num_reg, long fl_prog_riga, string fs_tipo, string fs_cod_prodotto, decimal fd_imponibile_iva, ref string fs_cdc[], ref decimal fd_importi[])
public function integer uof_fatbol_cdc_nosfuso (datetime fdt_data_dal, datetime fdt_data_al, string fs_tipo, ref string fs_cdc[], ref decimal fd_importi[], ref decimal fd_totale, ref string fs_errore)
public function integer uof_fatbol_cdc_aggiorna (string fs_cod_cdc, decimal fd_imponibile_iva, ref string fs_cdc[], ref decimal fd_importi[])
public function integer uof_fatbol_cdc_sfuso (datetime fdt_data_dal, datetime fdt_data_al, string fs_tipo, ref string fs_cdc[], ref decimal fd_importi[], ref decimal fd_totale, ref string fs_errore)
public function integer uof_fatbol_flrep_scrivilog (datetime fdt_data_dal, datetime fdt_data_al, string fs_tipo, string fs_cdc[], decimal fd_importi[], decimal fd_totale)
public function integer uof_risultati_matricola_data (string fs_matricola, datetime fdt_data_dal, datetime fdt_data_al, ref decimal fd_ore_ordinarie, ref decimal fd_ore_straordinarie, ref decimal fd_ore_flessibilita, ref string fs_errore)
public function integer uof_risultati_matricola_data (long fl_matricola, datetime fdt_data_dal, datetime fdt_data_al, ref decimal fd_ore_ordinarie, ref decimal fd_ore_straordinarie, ref decimal fd_ore_flessibilita, ref string fs_errore)
public function integer uof_rp_cdc_variaz_online (datetime fdt_data_dal, datetime fdt_data_al, string fs_cdc[], ref decimal fd_ol[], ref string fs_errore)
public function integer uof_get_codoperaio_matricola (ref string fs_cod_operaio, ref string fs_matricola, ref string fs_errore)
public function integer uof_get_cdc_dipendente_presenze (string fs_matricola, ref string fs_cdc_default, ref string fs_errore)
public function integer uof_rp_cdc_online (datetime fdt_data_dal, datetime fdt_data_al, string fs_cdc[], decimal fd_importi_cdc[], decimal fd_totale, string fs_tipo, ref string fs_errore)
public function integer uof_rp_cdc_mese_online (datetime fdt_data_dal, datetime fdt_data_al, string fs_cdc[], decimal fd_importi_cdc[], decimal fd_totale, ref string fs_errore)
public function integer uof_ordini_cdc (datetime fdt_data_dal, datetime fdt_data_al, ref string fs_cdc[], ref decimal fd_importi[], ref decimal fd_totale, ref string fs_errore)
public function integer uof_ordini_cdc_sfuso (datetime fdt_data_dal, datetime fdt_data_al, ref string fs_cdc[], ref decimal fd_importi[], ref decimal fd_totale, ref string fs_errore)
public function integer uof_ordini_cdc_nosfuso (datetime fdt_data_dal, datetime fdt_data_al, ref string fs_cdc[], ref decimal fd_importi[], ref decimal fd_totale, ref string fs_errore)
public function integer uof_get_nomecognome_da_matricola (string fs_matricola, ref string fs_nome_cognome, ref string fs_errore)
public function integer uof_get_hsp_vhs_vio (string fs_matricola, datetime fdt_data_inizio, datetime fdt_data_fine, ref string fs_nome_cognome, ref decimal fd_hsp, ref decimal fd_vhs, ref decimal fd_vio, ref string fs_errore)
public function integer uof_get_nr_ipia_via (string fs_matricola, datetime fdt_data_inizio, datetime fdt_data_fine, ref string fs_nome_cognome, ref integer fi_nr, ref decimal fd_ipia, ref decimal fd_via_area, ref decimal fd_via, ref string fs_errore)
public function integer uof_get_impp_from_isfa (decimal fd_isfa, ref decimal fd_impp, ref string fs_errore)
public function integer uof_get_cdc_noripartizione (ref string fs_sql_uff_mag_aut[], ref string fs_sql_uff_mag_aut_descriz[], ref string fs_errore)
public function integer uof_get_nr (string fs_matricola, datetime fdt_data_dal, datetime fdt_data_al, ref integer fl_nr, ref string fs_errore)
public function integer uof_risultati_cdc_data (string fs_cod_centro_costo, datetime fdt_data_dal, datetime fdt_data_al, ref decimal fd_ore_ordinarie, ref decimal fd_ore_straordinarie, ref decimal fd_ore_flessibilita, ref decimal fd_ore_diaria, ref string fs_errore)
public function integer uof_risultati_cdc_data (datetime fdt_data_dal, datetime fdt_data_al, ref string fs_cdc[], ref decimal fd_ore_ordinarie[], ref decimal fd_ore_straordinarie[], ref decimal fd_ore_flessibilita[], ref decimal fd_ore_diaria[], ref string fs_errore)
public function integer uof_fatbol_cdc_sfuso_old (datetime fdt_data_dal, datetime fdt_data_al, string fs_tipo, ref string fs_cdc[], ref decimal fd_importi[], ref decimal fd_totale, ref string fs_errore)
public function integer uof_get_vpr (string fs_matricola, ref decimal fd_vpr, ref string fs_errore)
public function integer uof_risultati_matricola_data_v (string fs_matricola, datetime fdt_data_dal, datetime fdt_data_al, ref decimal fd_fd_hofp[], ref string fs_errore)
public function integer uof_get_hopp_hofp_iapp (string fs_matricola, string fs_cdc, datetime fdt_data_inizio, datetime fdt_data_fine, ref decimal fd_hopp, ref decimal fd_hofp, ref decimal fd_iapp, ref string fs_errore)
public function integer uof_get_hopp (string fs_matricola, datetime fdt_data_inizio, datetime fdt_data_fine, ref decimal fd_hopp[], ref integer fi_mesi[], ref string fs_errore)
public function integer uof_get_vpt (string fs_matricola, datetime fdt_inizio, datetime fdt_fine, ref string fs_nome_cognome, ref integer fi_p, ref decimal fd_vpr, ref integer fi_t, ref decimal fd_bpt, ref decimal fd_is_area, ref decimal fd_ix_area, ref decimal fd_vflp, ref decimal fd_vfla, ref decimal fd_impp, ref decimal fd_fl, ref decimal fd_ol, ref decimal fd_rpr_reparto, ref decimal fd_hofp, ref decimal fd_hopp, ref decimal fd_ip, ref decimal fd_vpt, ref string fs_errore)
public function integer uof_get_dipendenti (string fs_cdc_ricerca, string fs_matricola_ricerca, ref string fs_cdc[], ref string fs_des_cdc[], ref string fs_matricole[], ref string fs_cognome_nome[], ref string fs_errore)
public subroutine uof_get_periodi_lettera (long fl_anno_rif, ref datetime fdt_data_inizio[], ref datetime fdt_data_fine[])
end prototypes

public function integer uof_disconnetti_presenze ();//esegue la disconnessione dal database delle presenze e la distruzione della transazione

rollback using itran_presenze;
disconnect using itran_presenze;
destroy itran_presenze;

if ii_verbose = 2 then
	uof_add_log("---------------------------------------------------")
	uof_add_log("Disconnesso dal DATABASE presenze")
	uof_add_log("---------------------------------------------------")
end if

return 1
end function

public function integer uof_add_log (string fs_valore);//scrive nel file di log


long ll_ret

if not ib_log or ii_verbose=0 then return 1

ll_ret = fileopen(is_path_log, LineMode!, Write!)

if ll_ret<0 then
	ib_log = false
	return -1
end if

fs_valore = string(today(), "dd-mm-yyyy") + + "~t" + string(now(), "hh:mm:ss") + "~t" + fs_valore

filewrite(ll_ret, fs_valore)

fileclose(ll_ret)
end function

public function integer uof_path_log ();String ls_path, ls_appo[]
long ll_owner, ll_ret, ll_i

setnull(ll_owner)

if RegistryGet("HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", &
							"Personal", RegString!, ls_path) = 1 then
	is_path_log = ls_path + "\"
else
	f_getuserpath(ls_path)
	is_path_log = ls_path
end if

//is_path_log +="log_premi_"+string(datetime(today(),now()),"ddMMyy-hhmmss")+"_"+s_cs_xx.cod_utente+".log"
is_path_log +="log_premi_"+string(datetime(today(),now()),"ddMMyy_hhmmss")+"_"+s_cs_xx.cod_utente+".log"

return 1
end function

public function string uof_date_format_msaccess (datetime fdt_datetime);date ldt_date
string ls_mese, ls_giorno, ls_return

ldt_date = date(fdt_datetime)

//torna la stringa #mm/dd/yyyy# , formato leggibile per le date im Microsoft Access
//ls_return = "#" + string(month (ldt_date)) + "/" + string(day(ldt_date)) + "/" + string(year(ldt_date)) + "#"


//torna la stringa 'yyyymmdd' , formato leggibile per le date in SQL SERVER
ls_mese = string(month(ldt_date))
if month(ldt_date) < 10 then
	ls_mese = "0" + ls_mese
end if
ls_giorno = string(day(ldt_date))
if day(ldt_date) < 10 then
	ls_giorno = "0" + ls_giorno
end if
ls_return =  "'" + string(year(ldt_date)) + ls_mese + ls_giorno  + "'"

return ls_return
end function

public function boolean uof_crea_ds_tran (ref datastore fds_data, string fs_sql, transaction ft_tran, ref string fs_msg);//crea un datastore in base ad un sql e su una transazione

string ls_sintassi, ls_errore

setpointer(HourGlass!)

ls_sintassi = ft_tran.syntaxfromsql(fs_sql, "style(type=grid)", ls_errore)
if Len(ls_errore) > 0 then
	fs_msg = "Errore durante la creazione della sintassi del datastore:" + ls_errore	
	setpointer(Arrow!)
	return false
end if

fds_data = create datastore

fds_data.create( ls_sintassi, ls_errore)
if Len(ls_errore) > 0 then
	fs_msg = "Errore durante la creazione del datastore:" + ls_errore
	destroy fds_data;
	setpointer(Arrow!)
	return false
end if

fds_data.settransobject(ft_tran)

setpointer(Arrow!)
return true
end function

public function integer uof_connetti_presenze (string fs_errore);string ls_dbparm
//esegue la connessione al database delle presenze, via ODBC

//legge dal parametro aziendale DBP (database presenze)
select stringa
into :is_odbc_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='DBP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro DBP: "+sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
	
elseif sqlca.sqlcode=100 or is_odbc_presenze="" or isnull(is_odbc_presenze) then
	fs_errore = "Parametro DBP (odbc DB presenze) non trovato oppure non impostato nella tabella parametri aziendali!"
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
	
end if

//legge dal parametro aziendale UTP (UID database presenze)
select stringa
into :is_uid_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='UTP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro UTP: "+sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	
   return -1
end if
if isnull(is_uid_presenze) then is_uid_presenze = ""

//legge dal parametro aziendalePAP (password database presenze)
select stringa
into :is_pwd_presenze
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='PAP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura parametro PAP: "+sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	
   return -1
end if
if isnull(is_pwd_presenze) then is_pwd_presenze = ""


itran_presenze = create transaction

itran_presenze.dbms = "ODBC"
itran_presenze.autocommit = false

//itran_presenze.dbparm = "Connectstring='DSN="+is_odbc_presenze+"',DisableBind=1"
ls_dbparm = "ConnectString='DSN="+is_odbc_presenze

if is_uid_presenze<>"" then
ls_dbparm += ";UID="+is_uid_presenze
end if

if is_pwd_presenze <> "" then
	ls_dbparm += ";PWD="+is_pwd_presenze
end if

ls_dbparm+= "',CommitOnDisconnect='No',DisableBind=1"

itran_presenze.dbparm = ls_dbparm

if f_po_connect(itran_presenze, true) <> 0 then
	fs_errore = "Errore in connessione DB presenze: "+itran_presenze.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	
   return -1
end if

if ii_verbose = 2 then
	uof_add_log("---------------------------------------------------")
	uof_add_log("CREAZIONE TRANSAZIONE DATABASE PRESENZE")
	uof_add_log("Connessione DB presenze avvenuta")
	uof_add_log("---------------------------------------------------")
end if

return 1
end function

public function integer uof_operai_reparto (string fs_cod_reparto, datetime fdt_data_inizio, datetime fdt_data_fine, ref string fs_cod_operai[], ref string fs_matricole[], ref string fs_errore);//dato un reparto restituisce un array di cod.operai assegnati nel periodo data inizio - data fine specificati

string ls_cod_centro_costo, ls_sql, ls_matricola, ls_cod_operaio
datastore lds_matricole
long ll_rows, ll_index, ll_matricola

//leggo il CDC del reparto
select cod_centro_costo
into :ls_cod_centro_costo
from anag_reparti
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_reparto=:fs_cod_reparto
using sqlca;
		
if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura CdC da tabella reparti: "+sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore + fs_errore)
   return -1
end if

if isnull(ls_cod_centro_costo) or ls_cod_centro_costo="" then
	fs_errore="CdC non importato per il reparto: "+fs_cod_reparto
	uof_add_log(is_prefisso_errore +fs_errore)
   return -1
end if

//con questo CdC vado a leggere dalla tabella "fine" del DB delle presenze
//N.B. i codici CdC di APICE e DB presenze coincidono

if uof_connetti_presenze(fs_errore) < 0 then
	//in fs_errore il messaggio di errore
	return -1
end if

ls_sql = "select matricola "+&
			"from fine "+&
			"where azienda='"+is_cod_azienda+"' and "+&
					"datainizio>= "+uof_date_format_msaccess(fdt_data_inizio)+" and "+&
					"datainizio<= "+uof_date_format_msaccess(fdt_data_fine)+" "

//creo il datastore
if not uof_crea_ds_tran(lds_matricole,ls_sql,itran_presenze,fs_errore) then
	//in fs_errore il messaggio di errore
	uof_add_log(is_prefisso_errore + fs_errore)
	uof_disconnetti_presenze()
   return -1
end if

ll_rows = lds_matricole.rowcount()

//non serve più la transazione con access
uof_disconnetti_presenze()

for ll_index=1 to ll_rows
	ls_matricola = lds_matricole.getitemstring(ll_index, 1)
	
	fs_matricole[ll_index] = ls_matricola
	
	//recupera il cod_operaio
	
	ll_matricola = long(ls_matricola)
	
	select cod_operaio
	into :ls_cod_operaio
	from anag_operai
	where cod_azienda=:s_cs_xx.cod_azienda and
			num_matricola=:ll_matricola
	using sqlca;
	
	if sqlca.sqlcode<0 then
		fs_errore="Errore in lettura cod.dipendente in CdC da tabella reparti: "+sqlca.sqlerrtext
		uof_add_log(is_prefisso_errore + fs_errore)
   	return -1
	end if
	
	fs_cod_operai[ll_index]=ls_cod_operaio
	
next



return 1
end function

public function time uof_orelav_totime (long fl_ore);//trasforma il numero fl_ore in variabile di tipo time
//es.  64000  ->   6:40:00
//		122900  ->	12:29:00

string ls_mask
time ltm_return

ls_mask = right("000000" + string(fl_ore), 6)

ltm_return = time(left(ls_mask,2)+":"+mid(ls_mask,3,2)+":"+right(ls_mask,2))

return ltm_return
end function

public function integer uof_orelav_matricola_data (string fs_matricola, datetime fdt_data_dal, datetime fdt_data_al, ref decimal fd_ore, ref string fs_errore);//estrae le ore lavorate da un dipendente in un dato periodo dal database delle trimbrature
//probabilmente questa funzione non servirà, ma potrebbe essere comunque utile

string ls_sql, ls_verso, ls_tipo
datastore lds_data
long ll_tot, ll_index, ll_lettura, ll_counter, ll_secondi
time ltm_in[], ltm_out[]

//imposto lo statement sql
ls_sql = "select ora, verso, tipo "+&
			"from timbrature "+&
			"where 	azienda='"+is_cod_azienda+"' and "+&
						"matricola='"+fs_matricola+"' and "+&
						"data>="+uof_date_format_msaccess(fdt_data_dal)+" and "+&
						"data<="+uof_date_format_msaccess(fdt_data_al)+" "+&
			"order by data asc, ora asc"

//connettiti al database delle timbrature, creando la transazione
if uof_connetti_presenze(fs_errore)<0 then
	//in fs_errore il messaggio di errore
	
	return -1
end if

//crea il datastore sulla transazione
if not uof_crea_ds_tran(lds_data,ls_sql,itran_presenze,fs_errore) then
	//in fs_msg il messaggio di errore
	return -1
end if

ll_tot = lds_data.retrieve()

//disconnettiti dal database delle timbrature, distruggendo la transazione
uof_disconnetti_presenze()

//elabora il datastore
for ll_index=1 to ll_tot
	ll_lettura = lds_data.getitemnumber(ll_index, 1)
	ls_verso = lds_data.getitemstring(ll_index, 2)
	ls_tipo = lds_data.getitemstring(ll_index, 3)
	
	if isnull(ll_lettura) or ll_lettura=0 then continue
	
	if upper(ls_tipo) <> "P" then continue
	
	if upper(ls_verso) = "E" then
		ll_counter = upperbound(ltm_in)
	else
		ll_counter = upperbound(ltm_out)
	end if
	
	if isnull(ll_counter) then ll_counter = 0
	ll_counter += 1
	
	if upper(ls_verso) = "E" then
		ltm_in[ll_counter] = uof_orelav_totime(ll_lettura)
	else
		ltm_out[ll_counter] = uof_orelav_totime(ll_lettura)
	end if
	
next

destroy lds_data;

ll_tot = upperbound(ltm_in)
if upperbound(ltm_out) < ll_tot then ll_tot = upperbound(ltm_out)

//calcola le ore lavorate con secondafters
fd_ore = 0
ll_secondi = 0
for ll_index=1 to ll_tot
	ll_lettura = secondsafter(ltm_in[ll_index], ltm_out[ll_index])
	
	if isnull(ll_lettura) or ll_lettura<0 then ll_lettura = 0
	ll_secondi += ll_lettura
next

//in fl_ore ho il tempo lavorato nella giornata in secondi, quindi trasformo in ore
fd_ore = round(ll_secondi / 3600, 2)

if ii_verbose = 2 then &
	uof_add_log("Ore da Timbrature matr."+fs_matricola+" dal "&
					+string(fdt_data_dal, "dd/mm/yyyy")+" al "+string(fdt_data_al, "dd/mm/yyyy")+" :"+string(fd_ore, "###,###,##0.00"))

return 1
end function

public function integer uof_orelav_matricola_data (long fl_matricola, datetime fdt_data_dal, datetime fdt_data_al, ref decimal fd_ore, ref string fs_errore);//estrae le ore lavorate da un dipendente in un dato periodo dal database delle trimbrature
//probabilmente questa funzione non servirà, ma potrebbe essere comunque utile

string ls_matricola

//la matricola nel DB timbrature è un char(10), riempito con "0"
ls_matricola = right("0000000000" + string(fl_matricola), 10)

if uof_orelav_matricola_data(ls_matricola,fdt_data_dal, fdt_data_al,fd_ore,fs_errore) < 0 then
	//in fs_errore il messaggio
	return -1
end if

return 1
end function

public function integer uof_get_causali (string fs_errore);long ll_index, ll_tot, ll_counter
string ls_vuoto[], ls_sql_ordinarie, ls_sql_straordinarie, ls_sql_flessibilita, &
		ls_sql_ritardo, ls_sql_base, ls_sql_diaria

datastore lds_ordinarie, lds_straordinarie, lds_flessibilita, lds_ritardo, lds_diaria

/*
cosi l'eventuale messaggio di errore futuri

uof_add_log(is_prefisso_errore+"Messaggio di errore ....."))
*/

//reset variabili
is_ordinarie = ls_vuoto
is_straordinarie = ls_vuoto
is_flessibilita = ls_vuoto
is_nonordinarie = ls_vuoto
is_ritardo = ls_vuoto
is_diaria = ls_vuoto

is_sql_ordinarie = ""
is_sql_straordinarie = ""
is_sql_flessibilita = ""
is_sql_nonordinarie = ""
is_sql_ritardo = ""
is_sql_diaria = ""
/*
is_ordinarie[1] = "ORDI"
is_ordinarie[2] = "PRES"

is_straordinarie[1] = "STRD"

is_flessibilita[1] = "FL+"

is_ritardo[1] = "RITA"

is_ritardo_sotto_1h[1] = "RIT1"  ????
*/

ls_sql_base = "select cod_causale "+&
			"from premi_causali "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' "
			
ls_sql_ordinarie = ls_sql_base + " and flag_tipo_causale='S' "

ls_sql_straordinarie = ls_sql_base + " and flag_tipo_causale='N' "

ls_sql_flessibilita = ls_sql_base + " and flag_tipo_causale='I' "

ls_sql_ritardo = ls_sql_base + " and flag_tipo_causale='R' "

ls_sql_diaria = ls_sql_base + " and flag_tipo_causale='D' "

//crea i datastore sulla transazione
if not uof_crea_ds_tran(lds_ordinarie,ls_sql_ordinarie,sqlca,fs_errore) then
	//in fs_msg il messaggio di errore
	return -1
end if
if not uof_crea_ds_tran(lds_straordinarie,ls_sql_straordinarie,sqlca,fs_errore) then
	//in fs_msg il messaggio di errore
	return -1
end if
if not uof_crea_ds_tran(lds_flessibilita,ls_sql_flessibilita,sqlca,fs_errore) then
	//in fs_msg il messaggio di errore
	return -1
end if
if not uof_crea_ds_tran(lds_ritardo,ls_sql_ritardo,sqlca,fs_errore) then
	//in fs_msg il messaggio di errore
	return -1
end if
if not uof_crea_ds_tran(lds_diaria,ls_sql_diaria,sqlca,fs_errore) then
	//in fs_msg il messaggio di errore
	return -1
end if

lds_ordinarie.retrieve()
lds_straordinarie.retrieve()
lds_flessibilita.retrieve()
lds_ritardo.retrieve()
lds_diaria.retrieve()

for ll_index=1 to lds_ordinarie.rowcount()
	is_ordinarie[ll_index] = lds_ordinarie.getitemstring(ll_index, 1)
next
for ll_index=1 to lds_straordinarie.rowcount()
	is_straordinarie[ll_index] = lds_straordinarie.getitemstring(ll_index, 1)
next
for ll_index=1 to lds_flessibilita.rowcount()
	is_flessibilita[ll_index] = lds_flessibilita.getitemstring(ll_index, 1)
next
for ll_index=1 to lds_ritardo.rowcount()
	is_ritardo[ll_index] = lds_ritardo.getitemstring(ll_index, 1)
next
for ll_index=1 to lds_diaria.rowcount()
	is_diaria[ll_index] = lds_diaria.getitemstring(ll_index, 1)
next

destroy lds_ordinarie;
destroy lds_straordinarie;
destroy lds_flessibilita;
destroy lds_ritardo;
destroy lds_diaria;

//carico il vettore delle causali NON ordinarie come somma dei due precedenti
ll_counter = 0

for ll_index=1 to upperbound(is_straordinarie)
	ll_counter += 1
	is_nonordinarie[ll_counter] = is_straordinarie[ll_index]
next

for ll_index=1 to upperbound(is_flessibilita)
	ll_counter += 1
	is_nonordinarie[ll_counter] = is_flessibilita[ll_index]
next
//-------

//adesso in base al contenuto dei vettori costruisco la parte di sql
//da includere come causale in ('CAUS1','CAUS2',...)
//ORDINARIE ------------------------------------------
if upperbound(is_ordinarie) > 0 then
	is_sql_ordinarie = "("
	for ll_index=1 to upperbound(is_ordinarie)
		if ll_index>1 then
			//faccio precedere tutto da una virgola
			is_sql_ordinarie += ","
		end if
		is_sql_ordinarie += "'"+is_ordinarie[ll_index]+"'"
	next
	is_sql_ordinarie += ")"
end if

//STRAORDINARIE --------------------------------------
if upperbound(is_straordinarie) > 0 then
	is_sql_straordinarie = "("
	for ll_index=1 to upperbound(is_straordinarie)
		if ll_index>1 then
			//faccio precedere tutto da una virgola
			is_sql_straordinarie += ","
		end if
		is_sql_straordinarie += "'"+is_straordinarie[ll_index]+"'"
	next
	is_sql_straordinarie += ")"
end if

//FLESSIBILITA ---------------------------------------
if upperbound(is_flessibilita) > 0 then
	is_sql_flessibilita = "("
	for ll_index=1 to upperbound(is_flessibilita)
		if ll_index>1 then
			//faccio precedere tutto da una virgola
			is_sql_flessibilita += ","
		end if
		is_sql_flessibilita += "'"+is_flessibilita[ll_index]+"'"
	next
	is_sql_flessibilita += ")"
end if

//STRAORDINARIE + FLESSIBILITA -----------------------
if upperbound(is_nonordinarie) > 0 then
	is_sql_nonordinarie = "("
	for ll_index=1 to upperbound(is_nonordinarie)
		if ll_index>1 then
			//faccio precedere tutto da una virgola
			is_sql_nonordinarie += ","
		end if
		is_sql_nonordinarie += "'"+is_nonordinarie[ll_index]+"'"
	next
	is_sql_nonordinarie += ")"
end if

//RITARDO --------------------------------
if upperbound(is_ritardo) > 0 then
	is_sql_ritardo = "("
	for ll_index=1 to upperbound(is_ritardo)
		if ll_index>1 then
			//faccio precedere tutto da una virgola
			is_sql_ritardo += ","
		end if
		is_sql_ritardo += "'"+is_ritardo[ll_index]+"'"
	next
	is_sql_ritardo += ")"
end if

//DIARIA ---------------------------------------------
if upperbound(is_diaria) > 0 then
	is_sql_diaria = "("
	for ll_index=1 to upperbound(is_diaria)
		if ll_index>1 then
			//faccio precedere tutto da una virgola
			is_sql_diaria += ","
		end if
		is_sql_diaria += "'"+is_diaria[ll_index]+"'"
	next
	is_sql_diaria += ")"
end if


return 1
end function

public function integer uof_get_cdc_presenze (ref string fs_cdc[], ref string fs_cdc_des[], ref string fs_errore);long ll_index, ll_tot
string ls_sql

datastore lds_data


ls_sql = "select codice, descrizione "+&
			"from gruppi_anag_a "+&
			"where azienda='"+is_cod_azienda+"' and "+&
					"gruppo='"+is_gruppo+"' "+&
			"order by codice "

if uof_connetti_presenze(fs_errore) < 0 then
	//in fs_errore il messaggio di errore
	return -1
end if

//creo il datastore
if not uof_crea_ds_tran(lds_data,ls_sql,itran_presenze,fs_errore) then
	//in fs_errore il messaggio di errore
	uof_add_log(is_prefisso_errore + fs_errore)
	uof_disconnetti_presenze()
   return -1
end if

lds_data.retrieve()

//non serve più la transazione con access
uof_disconnetti_presenze()

for ll_index=1 to lds_data.rowcount()
	fs_cdc[ll_index] = lds_data.getitemstring(ll_index, 1)
	fs_cdc_des[ll_index] = lds_data.getitemstring(ll_index, 2)
next

destroy lds_data;

return 1
end function

public subroutine uof_get_inizio_fine_mese (long fl_num_mese, long fl_anno, ref datetime fdt_data_dal, ref datetime fdt_data_al, ref datetime fdt_date_mese[]);//restituisce la data del primo giorno del mese e dell'ultimo
//per il primo è banale, per l'ultimo occorre verificare
//inoltre popola un array di date del mese

long ll_ultimo_giorno, ll_index

fdt_data_dal = datetime(date(fl_anno, fl_num_mese, 1), 00:00:00)

choose case fl_num_mese
	case 11,4,6,9
		ll_ultimo_giorno = 30
		
	case 2
		//se bisestile sono 29, altrimenti 28
		//sono bisestili gli anni divisibili per quattro, tranne quelli di inizio secolo (cioè divisibili per 100) non divisibili per 400
		if mod(fl_anno, 4) = 0 then
			
			if mod(fl_anno, 100) = 0 then
				
				if mod(fl_anno, 400) = 0 then
					//bisestile
					ll_ultimo_giorno = 28
				else
					//non bisestile
					ll_ultimo_giorno = 29
				end if
				
			else
				//sicuramente bisestile
				ll_ultimo_giorno = 29
			end if
			
		else
			//sicuramente non bisestile
			ll_ultimo_giorno = 28
		end if
		
	case else
		ll_ultimo_giorno = 31
		
end choose

fdt_data_al = datetime(date(fl_anno, fl_num_mese, ll_ultimo_giorno), 00:00:00)

for ll_index=1 to ll_ultimo_giorno
	fdt_date_mese[ll_index] = datetime(date(fl_anno, fl_num_mese, ll_index), 00:00:00)
next


end subroutine

public function integer uof_fatbol_cdc (datetime fdt_data_dal, datetime fdt_data_al, string fs_tipo, ref string fs_cdc[], ref decimal fd_importi[], ref decimal fd_totale, ref string fs_errore);//estrae il bollettato/fatturato (dipende dalla variabile fs_tipo B/F)
//per il confezionato considera i prodotti con 2nda cifra='0' (tutto il resto è non confezionato)

//il bollettato/fatturato del confezionato viene ripartito in % secondo le regole scritte in premi_prodotti_reparti
//dove si hanno fino a 2 reparti con relative percentuali
//per il non confezionato ci si riferisce all'unico reparto scritto in anag_prodotti.cod_reparto

string ls_vuoto[]
decimal ld_vuoto[]

//reset variabili di uscita
fs_cdc = ls_vuoto
fd_importi = ld_vuoto
fd_totale = 0

uof_add_log(" ")
uof_add_log("---------------------------------------------------")
uof_add_log("INIZIO CALCOLO FLrep per i diversi Centri di Costo")
uof_add_log("---------------------------------------------------")

//finestra di scorrimento elaborazione
if ib_apri_wait then open(w_stato_elaborazione)

//calcolo per non confezionato (sfuso, ecc...): seconda cifra del prodotto <>'0'
if uof_fatbol_cdc_sfuso(fdt_data_dal,fdt_data_al,fs_tipo,fs_cdc[],fd_importi[],fd_totale, fs_errore) < 0 then
	//in fs_errore il messaggio
	
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	
	return -1
end if

//calcolo per confezionato: seconda cifra del prodotto ='0'
if uof_fatbol_cdc_nosfuso(fdt_data_dal,fdt_data_al,fs_tipo,fs_cdc[],fd_importi[],fd_totale,fs_errore) < 0 then
	//in fs_errore il messaggio
	
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	
	return -1
end if

uof_fatbol_flrep_scrivilog(fdt_data_dal,fdt_data_al,fs_tipo,fs_cdc[],fd_importi[],fd_totale)

if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)

uof_add_log("---------------------------------------------------")
uof_add_log("FINE CALCOLO FLrep per i diversi Centri di Costo")
uof_add_log("---------------------------------------------------")

return 1
end function

public function integer uof_fatbol_cdc_percent (datastore fds_regole, long fl_anno_reg, long fl_num_reg, long fl_prog_riga, string fs_tipo, string fs_cod_prodotto, decimal fd_imponibile_iva, ref string fs_cdc[], ref decimal fd_importi[]);//legge le percentuali per reparto, attribuendo all'imponibile riga

long ll_tot, ll_index
string ls_regola, ls_test, ls_cifra_tenda, ls_cod_cdc_1, ls_cod_cdc_2, ls_riga_documento
decimal ld_perc_rep_1, ld_perc_rep_2, ld_imponibile
boolean lb_trovata=false

ll_tot = fds_regole.rowcount()
ls_cifra_tenda = "0"

for ll_index=1 to ll_tot
	//esamina la regola, se soddisfatta, preleva le percentuali
	ls_regola = fds_regole.getitemstring(ll_index, 1)
	
	select cod_prodotto
	into :ls_test
	from anag_prodotti
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:fs_cod_prodotto and
			substring(cod_prodotto,2,1)=:ls_cifra_tenda and
			cod_prodotto like :ls_regola;
	
	if sqlca.sqlcode=0 and ls_test<>"" and not isnull(ls_test) then
		//regola verificata
		lb_trovata = true
		
		ls_cod_cdc_1	= fds_regole.getitemstring(ll_index, 2)
		ld_perc_rep_1		= fds_regole.getitemdecimal(ll_index, 3)
		ls_cod_cdc_2	= fds_regole.getitemstring(ll_index, 4)
		ld_perc_rep_2		= fds_regole.getitemdecimal(ll_index, 5)
		
		if ls_cod_cdc_2="" then setnull(ls_cod_cdc_2)
		if isnull(ld_perc_rep_1) then ld_perc_rep_1 = 0
		if isnull(ld_perc_rep_2) then ld_perc_rep_2 = 0
		
		//esci dal ciclo for
		exit
	end if
	
next

if fs_tipo="B" then
	ls_riga_documento = "Bolla"
elseif fs_tipo="F" then
	ls_riga_documento = "Fattura"
else
	ls_riga_documento = "Ordine"
end if
ls_riga_documento += " Riga n°"+string(fl_anno_reg)+"/"+string(fl_num_reg)+"/"+string(fl_prog_riga)

if not lb_trovata then
	//il prodotto non soddisfa nessuna delle regole, l'importo finirà nel totale globale!!!
	
	if ii_verbose = 2 then &
		uof_add_log(is_prefisso_errore+"Il prodotto "+fs_cod_prodotto+&
					" non soddisfa nessuna delle regole predisposte ("+ls_riga_documento+"): l'importo finirà nel totale globale")
					
	return 1
end if

ld_imponibile = fd_imponibile_iva * (ld_perc_rep_1 / 100)

//aggiorna i vettori dei CdC e degli importi
uof_fatbol_cdc_aggiorna(ls_cod_cdc_1,ld_imponibile,fs_cdc[],fd_importi[])

if ii_verbose = 2 then &
	uof_add_log(ls_riga_documento+" - Imponibile "+string(fd_imponibile_iva, "###,###,##0.0000")+" - Prodotto "+fs_cod_prodotto+" - Cdc 1 "+ls_cod_cdc_1+&
					" percentuale "+string(ld_perc_rep_1) + " Val.calcolato "+string(ld_imponibile,"###,###,##0.0000"))

if ls_cod_cdc_2<>"" and not isnull(ls_cod_cdc_2) then
	
	if ld_perc_rep_2>0 then
		
		ld_imponibile = fd_imponibile_iva * (ld_perc_rep_2 / 100)
		
		//aggiorna i vettori dei reparti e degli importi
		uof_fatbol_cdc_aggiorna(ls_cod_cdc_2,ld_imponibile,fs_cdc[],fd_importi[])
		
		if ii_verbose = 2 then &
			uof_add_log(ls_riga_documento+" - Imponibile "+string(fd_imponibile_iva,"###,###,##0.0000")+" - Prodotto "+fs_cod_prodotto+" - CdC 2 "+ls_cod_cdc_2+&
					" percentuale "+string(ld_perc_rep_1) + " Val.calcolato "+string(ld_imponibile,"###,###,##0.0000"))
		
	else
		//segnala percentuale non impostata su cdc 2 (cioè cdc 2 impostato ma non la relativa percentuale)!!!
		uof_add_log(is_prefisso_errore+"% non impostata per il prodotto "+fs_cod_prodotto+"- CdC 2 "+ &
										ls_cod_cdc_2+" ("+ls_riga_documento+")")
	end if
	
else
	//segnala cdc 2 vuoto!!!
	if ii_verbose = 2 then &
		uof_add_log(is_prefisso_errore+"CdC 2 non impostato per il "+fs_cod_prodotto+" ("+ls_riga_documento+"),"+&
							"probabilmente la percentuale del CdC 1 è 100...")
end if

return 1
end function

public function integer uof_fatbol_cdc_nosfuso (datetime fdt_data_dal, datetime fdt_data_al, string fs_tipo, ref string fs_cdc[], ref decimal fd_importi[], ref decimal fd_totale, ref string fs_errore);//estrae il bollettato/fatturato (dipende dalla variabile fs_tipo B/F)
//considera i prodotti con 2nda cifra='0'
//ci si riferisce ai reparti con percentuali ripartire in base alla tabella premi_prodotti_reparti

string 		ls_sql, ls_cifra_tenda, ls_nome_tabella_tes, ls_nome_tabella_det, ls_colonna_data, ls_cod_prodotto, ls_sql_regole, &
				ls_regola, ls_cod_reparto_1, ls_cod_reparto_2, ls_sintassi_regole, ls_err, ls_cod_reparto, ls_colonna_prog_riga, ls_sql_count, &
				ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven
decimal 		ld_imponibile_iva, ld_perc_rep_1, ld_perc_rep_2
datastore	lds_regole
long			ll_tot_regole, ll_anno_reg, ll_num_reg, ll_prog_riga, ll_count,ll_index

//--------------------------------------------------------------------------
//preparo un cursore con le regole da cui leggere la percentuale di suddivisione
//del fatturato/bollettato per reparti
//ls_sql_regole = 	"select regola, cod_reparto_1, perc_rep_1, cod_reparto_2, perc_rep_2 "+&
//						"from premi_prodotti_reparti "+&
//						"order by posizione "
ls_sql_regole = 	"select regola, cod_centro_costo_1, perc_rep_1, cod_centro_costo_2, perc_rep_2 "+&
						"from premi_prodotti_cdc "+&
						"order by posizione "

//creazione datastore dinamico per le regole
ls_sintassi_regole = sqlca.syntaxfromsql(ls_sql_regole, "style(type=grid)", ls_err)
if Len(ls_err) > 0 then
	g_mb.error("Errore durante la creazione della sintassi del datastore regole ripartizione CdC:" + ls_err + "~r~n"+ls_sql_regole)
	uof_add_log(is_prefisso_errore+"Errore durante la creazione della sintassi del datastore regole ripartizione CdC:" + ls_err + "~r~n"+ls_sql_regole)
	return -1
end if

lds_regole = create datastore

lds_regole.Create(ls_sintassi_regole, ls_err)
if len(ls_err) > 0 then
	g_mb.error("Errore durante la creazione del datastore regole ripartizione CdC:" + ls_err)
	uof_add_log(is_prefisso_errore+"Errore durante la creazione del datastore regole ripartizione CdC:" + ls_err)
	destroy lds_regole;
	return -1
end if

lds_regole.settransobject(sqlca)

ll_tot_regole = lds_regole.retrieve()
if ll_tot_regole < 0 then
	g_mb.error("Errore durante la retrieve delle righe del datastore ripartiz.CdC:" + sqlca.sqlerrtext)
	uof_add_log(is_prefisso_errore+"Errore durante la retrieve delle righe del datastore ripartiz.CdC:" + sqlca.sqlerrtext)
	destroy lds_regole;
	return -1
end if

if ll_tot_regole=0 then
	g_mb.error("Tabella di ripartizione fatturato/bollettato in base al prodotto/CdC non impostata! Calcolo non effettuato!")
	uof_add_log(is_prefisso_errore+"Tabella di ripartizione fatturato/bollettato in base al prodotto/CdC non impostata! Calcolo non effettuato!")
	destroy lds_regole;
	return 0
end if

//adesso in lds_regole, ordinate per priorità ci sono le regole per filtrare il cod prodotto
//e stabilire le percentuali di imponibile per CdC
//es.   	'%Y' 		'cdc1' 75% 	'cdc2' 	25%
//			'A09%'	'cdc1' 60%	'cdc2'	40%	ecc...
//------------------------------------------------------------------------------
ls_cifra_tenda = "0"

if fs_tipo= "B" then
	//bollettato
	ls_nome_tabella_tes = "tes_bol_ven"
	ls_nome_tabella_det = "det_bol_ven"
	ls_colonna_data = "data_bolla"
	ls_colonna_prog_riga = "prog_riga_bol_ven"
else
	//fatturato
	ls_nome_tabella_tes = "tes_fat_ven"
	ls_nome_tabella_det = "det_fat_ven"
	ls_colonna_data = "data_fattura"
	ls_colonna_prog_riga = "prog_riga_fat_ven"
end if

//select count righe ##############################################################################################
ll_count = 0

ls_sql_count = "select count(*) "+&
					"from "+ls_nome_tabella_det+" as a "+&
					"join "+ls_nome_tabella_tes+" as t on t.cod_azienda=a.cod_azienda and "+&
																"t.anno_registrazione=a.anno_registrazione and "+&
																"t.num_registrazione=a.num_registrazione "+&
					"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"a.cod_prodotto is not null and "+&
							"substring(a.cod_prodotto, 2, 1)='"+ls_cifra_tenda+"' and "+&
							"t."+ls_colonna_data+" is not null and "+&
							"t."+ls_colonna_data+">='"+string(fdt_data_dal, s_cs_xx.db_funzioni.formato_data)+"' and "+&
							"t."+ls_colonna_data+"<='"+string(fdt_data_al, s_cs_xx.db_funzioni.formato_data)+"' "

//creazione cursore dinamico conteggio righe da elaborare
declare cu_fatbolcount dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql_count;
open dynamic cu_fatbolcount;

if sqlca.sqlcode <> 0 then 
	fs_errore = "Errore nella open del cursore bol/fat count non sfuso. Dettaglio = " + sqlca.sqlerrtext
	
	uof_add_log(is_prefisso_errore+"Errore nella open del cursore bol/fat count non sfuso. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

//una sola riga
fetch cu_fatbolcount into 
		 :ll_count;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore nella fetch del cursore bol/fat count non sfuso. Dettaglio = " + sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+"Errore nella fetch del cursore bol/fat count non sfuso. Dettaglio = " + sqlca.sqlerrtext)
	
	close cu_fatbolcount;
	return -1
	
end if	

close cu_fatbolcount;

//in ll_count il numero di righe
ll_index = 0

//#################################################################################################################


//impostazione sql: righe del bollettato/fatturato
//riga per riga sarà poi elaborata per imputarne la % per reparto ...
ls_sql = "select a.anno_registrazione, a.num_registrazione,a."+ls_colonna_prog_riga+","+&
					 "a.cod_prodotto, isnull(a.imponibile_iva,0) as importo "+&
			"from "+ls_nome_tabella_det+" as a "+&
			"join "+ls_nome_tabella_tes+" as t on t.cod_azienda=a.cod_azienda and "+&
														"t.anno_registrazione=a.anno_registrazione and "+&
														"t.num_registrazione=a.num_registrazione "+&
			"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"a.cod_prodotto is not null and "+&
					"substring(a.cod_prodotto, 2, 1)='"+ls_cifra_tenda+"' and "+&
					"t."+ls_colonna_data+" is not null and "+&
					"t."+ls_colonna_data+">='"+string(fdt_data_dal, s_cs_xx.db_funzioni.formato_data)+"' and "+&
					"t."+ls_colonna_data+"<='"+string(fdt_data_al, s_cs_xx.db_funzioni.formato_data)+"' "

//creazione cursore dinamico da elaborare
declare cu_fatbol dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_fatbol;

if sqlca.sqlcode <> 0 then 
	fs_errore = "Errore nella open del cursore bol/fat non sfuso. Dettaglio = " + sqlca.sqlerrtext
	
	uof_add_log(is_prefisso_errore+"Errore nella open del cursore bol/fat non sfuso. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if


do while true
	fetch cu_fatbol into 
			 :ll_anno_reg,:ll_num_reg,:ll_prog_riga,:ls_cod_prodotto,:ld_imponibile_iva;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore nella fetch del cursore bol/fat non sfuso. Dettaglio = " + sqlca.sqlerrtext
		uof_add_log(is_prefisso_errore+"Errore nella fetch del cursore bol/fat non sfuso. Dettaglio = " + sqlca.sqlerrtext)
		
		close cu_fatbol;
		return -1
		
	elseif sqlca.sqlcode = 100 then
		exit
	end if	
	
	//prosegui
	Yield()
	ll_index += 1
	
	if fs_tipo= "F" then
		//se fattura con nota di credito cambia il segno in negativo
		
		select flag_tipo_fat_ven
		into :ls_flag_tipo_fat_ven
		from tab_tipi_fat_ven
		where cod_azienda=:s_cs_xx.cod_azienda and
				cod_tipo_fat_ven=:ls_cod_tipo_fat_ven
		using sqlca;
		
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in lettura flag_tipo_fat_ven" + sqlca.sqlerrtext
			uof_add_log(is_prefisso_errore+fs_errore)
			
			close cu_fatbol;
			return -1
		end if
		
		if ls_flag_tipo_fat_ven="N" then
			//nota di credito
			ld_imponibile_iva = ld_imponibile_iva * (-1)
		end if
		
	end if
	
	//in questa variabile metto il totale imponibile, a prescindere dalla suddivisione percentuale dei reparti
	fd_totale += ld_imponibile_iva
	
	if isvalid(w_stato_elaborazione) then &
			w_stato_elaborazione.st_log.text = "Elab.righe Mod. dal "+string(fdt_data_dal, "dd/mm/yyyy")+&
					" al "+string(fdt_data_al, "dd/mm/yyyy")+"~r~n~r~n"+ &
					string(ll_anno_reg)+"/"+string(ll_num_reg)+"/"+string(ll_prog_riga)+" ("+string(ll_index)+" di "+string(ll_count)+")"
	
	//decidi le percentuali e aggiorna i reparti e imponibili
	uof_fatbol_cdc_percent(lds_regole,ll_anno_reg,ll_num_reg,ll_prog_riga,fs_tipo,ls_cod_prodotto,ld_imponibile_iva,fs_cdc[],fd_importi[])
	
loop

uof_add_log("Uscito dal cursore FLrep per prodotti non confezionati")

close cu_fatbol;
destroy lds_regole;

return 1
end function

public function integer uof_fatbol_cdc_aggiorna (string fs_cod_cdc, decimal fd_imponibile_iva, ref string fs_cdc[], ref decimal fd_importi[]);//aggiorna i due vettori contenenti l'elenco dei CdC corrispondenti ai reparti ed il corrispondente imponibile iva

long ll_tot, ll_index
string ls_temp, ls_CdC

ll_tot = upperbound(fs_cdc)

if isnull(ll_tot) or ll_tot=0 then
	//vettore vuoto, inserisci
	fs_cdc[1] = fs_cod_cdc
	fd_importi[1] = fd_imponibile_iva
	
	if ii_verbose = 2 then &
		uof_add_log("Cursore FLrep: Struttura vuota, aggiungo info:"+fs_cod_cdc+" - " + string(fd_imponibile_iva,"###,###,##0.0000"))
	
	//ora esci subito dalla funzione
	return 1
end if

//c'è già qualcosa nei vettori, verifica se devi incrementare o aggiungere a quanto presente
for ll_index = 1 to ll_tot
	ls_temp = fs_cdc[ll_index]
	
	if ls_temp = fs_cod_cdc then
		//trovato, fai un incremento
		fd_importi[ll_index] += fd_imponibile_iva
		
		if ii_verbose = 2 then &
			uof_add_log("Cursore FLrep: aggiorno valori:"+fs_cod_cdc+" a " + string(fd_importi[ll_index],"###,###,##0.0000"))
		
		//ora esci subito dalla funzione
		return 1
	end if
next

//se arrivi fin qui il CdC non è presente nella struttura, aggiungilo
ll_index = upperbound(fs_cdc)
ll_index += 1

fs_cdc[ll_index] = fs_cod_cdc
fd_importi[ll_index] = fd_imponibile_iva

if ii_verbose = 2 then &
	uof_add_log("Cursore FLrep: CdC non trovato in struttura, aggiungo info:"+fs_cod_cdc+" - " + string(fd_imponibile_iva,"###,###,##0.0000"))

return 1
end function

public function integer uof_fatbol_cdc_sfuso (datetime fdt_data_dal, datetime fdt_data_al, string fs_tipo, ref string fs_cdc[], ref decimal fd_importi[], ref decimal fd_totale, ref string fs_errore);//estrae il bollettato/fatturato (dipende dalla variabile fs_tipo B/F)
//considera i prodotti con 2nda cifra<>'0'
//ci si riferisce all'unico reparto scritto in anag_prodotti.cod_reparto

//ATTENZIONE: in fs_cdc[] mettiamo i codici dei rispettivi CdC e non dei singoli Reparti!!!!!

string ls_sql, ls_sql_count, ls_cifra_tenda, ls_nome_tabella_tes, ls_nome_tabella_det, ls_colonna_data, ls_cod_centro_costo,&
			ls_cod_prodotto,ls_cod_tipo_fat_ven, ls_colonna_tipo_doc,ls_colonna_prog_riga, ls_flag_tipo_fat_ven, ls_cod_deposito_origine
decimal ld_imponibile_iva
long ll_count, ll_index, ll_anno_reg, ll_num_reg,ll_prog_riga

ls_cifra_tenda = "0"

if fs_tipo= "B" then
	//bollettato
	ls_nome_tabella_tes = "tes_bol_ven"
	ls_nome_tabella_det = "det_bol_ven"
	ls_colonna_data = "data_bolla"
	ls_colonna_prog_riga = "prog_riga_bol_ven"
	ls_colonna_tipo_doc = "'' as cod_tipo_documento "
else
	//fatturato
	ls_nome_tabella_tes = "tes_fat_ven"
	ls_nome_tabella_det = "det_fat_ven"
	ls_colonna_data = "data_fattura"
	ls_colonna_prog_riga = "prog_riga_fat_ven"
	ls_colonna_tipo_doc = "t.cod_tipo_fat_ven as cod_tipo_documento "
end if

//select count righe ##############################################################################################
ll_count = 0
ls_sql_count = "select count(*) "+&
					"from "+ls_nome_tabella_det+" as a "+&
					"join "+ls_nome_tabella_tes+" as t on t.cod_azienda=a.cod_azienda and "+&
																"t.anno_registrazione=a.anno_registrazione and "+&
																"t.num_registrazione=a.num_registrazione "+&
					"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"("+&
								  "(a.cod_prodotto is not null and substring(a.cod_prodotto, 2, 1)<>'"+ls_cifra_tenda+"') or "+&
								  "(a.cod_prodotto is null or a.cod_prodotto='') "+&
							 ") and "+&
							"t."+ls_colonna_data+" is not null and "+&
							"t."+ls_colonna_data+">='"+string(fdt_data_dal, "yyyymmdd")+"' and "+&
							"t."+ls_colonna_data+"<='"+string(fdt_data_al, "yyyymmdd")+"' "

//creazione cursore dinamico conteggio righe da elaborare
declare cu_fatbolcount dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql_count;
open dynamic cu_fatbolcount;

if sqlca.sqlcode <> 0 then 
	fs_errore = "Errore nella open del cursore bol/fat count sfuso. Dettaglio = " + sqlca.sqlerrtext
	
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
end if

//una sola riga
fetch cu_fatbolcount into :ll_count;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore nella fetch del cursore bol/fat count sfuso. Dettaglio = " + sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	
	close cu_fatbolcount;
	return -1
	
end if	

close cu_fatbolcount;

//in ll_count il numero di righe
ll_index = 0

//righe con codice prodotto presente (no 0 in seconda posizione), caso comune ai DDT
ls_sql = "select a.anno_registrazione, a.num_registrazione,a."+ls_colonna_prog_riga+","+&
					 "a.cod_prodotto, isnull(a.imponibile_iva,0) as importo,"+ls_colonna_tipo_doc+", "+&
					 "t.cod_deposito "+&
			"from "+ls_nome_tabella_det+" as a "+&
			"join "+ls_nome_tabella_tes+" as t on t.cod_azienda=a.cod_azienda and "+&
														"t.anno_registrazione=a.anno_registrazione and "+&
														"t.num_registrazione=a.num_registrazione "+&
			"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"("+&
								  "(a.cod_prodotto is not null and substring(a.cod_prodotto, 2, 1)<>'"+ls_cifra_tenda+"') or "+&
								  "(a.cod_prodotto is null or a.cod_prodotto='') "+&
							 ") and "+&
							"t."+ls_colonna_data+" is not null and "+&
							"t."+ls_colonna_data+">='"+string(fdt_data_dal, "yyyymmdd")+"' and "+&
							"t."+ls_colonna_data+"<='"+string(fdt_data_al, "yyyymmdd")+"' "

//creazione cursore dinamico da elaborare
declare cu_fatbol dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_fatbol;

if sqlca.sqlcode <> 0 then 
	fs_errore = "Errore nella open del cursore bol/fat sfuso. Dettaglio = " + sqlca.sqlerrtext
	
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
end if

do while true
	fetch cu_fatbol into :ll_anno_reg,:ll_num_reg,:ll_prog_riga,:ls_cod_prodotto,:ld_imponibile_iva,:ls_cod_tipo_fat_ven, :ls_cod_deposito_origine;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore nella fetch del cursore bol/fat sfuso. Dettaglio = " + sqlca.sqlerrtext
		uof_add_log(is_prefisso_errore+fs_errore)
		
		close cu_fatbol;
		return -1
		
	elseif sqlca.sqlcode = 100 then
		exit
	end if	
	
	Yield()
	ll_index += 1
	
	//prosegui
	if isvalid(w_stato_elaborazione) then &
			w_stato_elaborazione.st_log.text = "Elab.righe Sfuso o senza prodotto. dal "+string(fdt_data_dal, "dd/mm/yyyy")+&
					" al "+string(fdt_data_al, "dd/mm/yyyy")+"~r~n~r~n"+ &
					string(ll_anno_reg)+"/"+string(ll_num_reg)+"/"+string(ll_prog_riga)+" ("+string(ll_index)+" di "+string(ll_count)+")"
	
	if fs_tipo= "F" then
		//se fattura con nota di credito cambia il segno in negativo
		select flag_tipo_fat_ven
		into :ls_flag_tipo_fat_ven
		from tab_tipi_fat_ven
		where cod_azienda=:s_cs_xx.cod_azienda and
				cod_tipo_fat_ven=:ls_cod_tipo_fat_ven
		using sqlca;
		
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in lettura flag_tipo_fat_ven" + sqlca.sqlerrtext
			uof_add_log(is_prefisso_errore+fs_errore)
			
			close cu_fatbol;
			return -1
		end if
		
		if ls_flag_tipo_fat_ven="N" then
			//nota di credito
			ld_imponibile_iva = ld_imponibile_iva * (-1)
		end if
	end if
	
	//in questa variabile metto il totale imponibile
	fd_totale += ld_imponibile_iva
	
	if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) and ls_cod_deposito_origine<>"" and not isnull(ls_cod_deposito_origine) then
		//cerca il Centro di costo
		//verifica il reparto del prodotto in base al deposito del documento
		select r.cod_centro_costo
		into :ls_cod_centro_costo
		from anag_reparti as r
		join anag_prodotti_reparti_depositi a on a.cod_azienda=r.cod_azienda and
										 					a.cod_reparto_produzione=r.cod_reparto
		where 	r.cod_azienda=:s_cs_xx.cod_azienda and
					a.cod_prodotto=:ls_cod_prodotto and
					a.cod_deposito_origine=:ls_cod_deposito_origine
		using sqlca;
		
//		select r.cod_centro_costo
//		into :ls_cod_centro_costo
//		from anag_reparti as r
//		join anag_prodotti as p on p.cod_azienda=r.cod_azienda and
//									p.cod_reparto=r.cod_reparto
//		where r.cod_azienda=:s_cs_xx.cod_azienda and
//				p.cod_prodotto=:ls_cod_prodotto
//		using sqlca;
		
	else
		//no CdC, metti comunque nel calderone
		setnull(ls_cod_centro_costo)
	end if
	
	if ls_cod_centro_costo<>"" and not isnull(ls_cod_centro_costo) then
		//aggiorna i vettori dei CdC e degli importi
		uof_fatbol_cdc_aggiorna(ls_cod_centro_costo,ld_imponibile_iva,fs_cdc[],fd_importi[])
	end if
	
loop

uof_add_log("Uscito dal cursore FLrep per prodotti NON confezionati")

close cu_fatbol;

return 1
end function

public function integer uof_fatbol_flrep_scrivilog (datetime fdt_data_dal, datetime fdt_data_al, string fs_tipo, string fs_cdc[], decimal fd_importi[], decimal fd_totale);//scrive nel file di log il rusiultato dell'elaborazione di FLrep

long ll_tot, ll_index
string ls_documento

if not ib_log or ii_verbose=0 then return 1

if fs_tipo="B" then
	ls_documento = "Bolle"
elseif fs_tipo="F" then
	ls_documento = "Fatture"
else
	ls_documento = "Ordini"
end if

uof_add_log("--------------------------------------------------")
uof_add_log("LOG RISULTATI ELABORAZIONE FLrep Calcolo su "+ls_documento+ &
									" dal "+string(date(fdt_data_dal),"dd/mm/yy")+" al "+string(date(fdt_data_al),"dd/mm/yy"))

ll_tot = upperbound(fs_cdc[])

for ll_index=1 to ll_tot
	Yield()
	uof_add_log(fs_cdc[ll_index] + ":~t~t"+string(fd_importi[ll_index], "###,###,##0.0000"))
next

uof_add_log("Totale per le aree MAGAZZ.,UFFICI, AUTISTI: "+string(fd_totale, "###,###,##0.0000"))

uof_add_log(" ")
uof_add_log("--------------------------------------------------")

return 1
end function

public function integer uof_risultati_matricola_data (string fs_matricola, datetime fdt_data_dal, datetime fdt_data_al, ref decimal fd_ore_ordinarie, ref decimal fd_ore_straordinarie, ref decimal fd_ore_flessibilita, ref string fs_errore);//estrae le ore lavorate da un dipendente in un dato periodo dal database delle presenze
//dalla tabella dei risultati

string ls_sql_base, ls_sql_ord, ls_sql_flex, ls_sql_straord
datastore lds_data_ord, lds_data_straord, lds_data_flex
long ll_tot_ord, ll_tot_straord, ll_tot_flex, ll_index, ll_lettura, ll_counter, ll_secondi
time ltm_in[], ltm_out[]


//recupero le varie tipologie di causali
if uof_get_causali(fs_errore) < 0 then
	//in fs_msg il messaggio di errore
	return -1
end if

//imposto lo statement sql di base (N.B. il dato sum(ore) rappresenta in realtà il numero di secondi)
ls_sql_base = 	"select sum(ore) "+&
					"from risultati "+&
					"where 	azienda='"+is_cod_azienda+"' and "+&
								"matricola='"+fs_matricola+"' and "+&
								"data>="+uof_date_format_msaccess(fdt_data_dal)+" and "+&
								"data<="+uof_date_format_msaccess(fdt_data_al)+" "

//per le ore ORDINARIE ------------------------------------------------------------------------------------
ls_sql_ord = ls_sql_base + "and causale in "+is_sql_ordinarie+" "


//per le ore STRAORDINARIE --------------------------------------------------------------------------------
ls_sql_straord = ls_sql_base + "and causale in "+is_sql_straordinarie+" "

//per gli straordinari non conteggiare quelle per le matricole che hanno in anagrafica il campo ca9='SI' (straord. forfettari)
ls_sql_straord += " and matricola in (select matricola "+&
													"from dipen "+&
													"where azienda='"+is_cod_azienda+"' and "+&
															"ca9<>'SI') "


//per le ore FLESSIBILITA --------------------------------------------------------------------------------
ls_sql_flex = ls_sql_base + "and causale in "+is_sql_flessibilita+" "

//connettiti al database delle timbrature, creando la transazione
if uof_connetti_presenze(fs_errore)<0 then
	//in fs_errore il messaggio di errore
	
	return -1
end if

//crea i datastore sulla transazione
if not uof_crea_ds_tran(lds_data_ord,ls_sql_ord,itran_presenze,fs_errore) then
	//in fs_msg il messaggio di errore
	return -1
end if
if not uof_crea_ds_tran(lds_data_straord,ls_sql_straord,itran_presenze,fs_errore) then
	//in fs_msg il messaggio di errore
	return -1
end if
if not uof_crea_ds_tran(lds_data_flex,ls_sql_flex,itran_presenze,fs_errore) then
	//in fs_msg il messaggio di errore
	return -1
end if

ll_tot_ord = lds_data_ord.retrieve()
ll_tot_straord = lds_data_straord.retrieve()
ll_tot_flex = lds_data_flex.retrieve()

//disconnettiti dal database delle timbrature, distruggendo la transazione
uof_disconnetti_presenze()

//elabora i datastore (ci dovrebbe essere una sola riga su ogni datatstore)
if ll_tot_ord > 0 then fd_ore_ordinarie = lds_data_ord.getitemnumber(1, 1)
if ll_tot_straord > 0 then fd_ore_straordinarie = lds_data_straord.getitemnumber(1, 1)
if ll_tot_flex > 0 then fd_ore_flessibilita = lds_data_flex.getitemnumber(1, 1)

//distruggi i datastore
destroy lds_data_ord;
destroy lds_data_straord;
destroy lds_data_flex;

if isnull(fd_ore_ordinarie) then fd_ore_ordinarie = 0
if isnull(fd_ore_straordinarie) then fd_ore_straordinarie = 0
if isnull(fd_ore_flessibilita) then fd_ore_flessibilita = 0

//converti in ore da secondi
fd_ore_ordinarie = round(fd_ore_ordinarie / 3600 , 4)
fd_ore_straordinarie = round(fd_ore_straordinarie / 3600 , 4)
fd_ore_flessibilita = round(fd_ore_flessibilita / 3600 , 4)

if ii_verbose = 2 then
	uof_add_log("Ore Ordinarie matr."+fs_matricola+" dal "&
					+string(fdt_data_dal, "dd/mm/yyyy")+" al "+string(fdt_data_al, "dd/mm/yyyy")+" :"+string(fd_ore_ordinarie, "###,###,##0.0000"))
	uof_add_log("Ore Flex+Straord. matr."+fs_matricola+" dal "&
					+string(fdt_data_dal, "dd/mm/yyyy")+" al "+string(fdt_data_al, "dd/mm/yyyy")+&
					" :"+string(fd_ore_straordinarie + fd_ore_flessibilita, "###,###,##0.0000"))
end if

return 1
end function

public function integer uof_risultati_matricola_data (long fl_matricola, datetime fdt_data_dal, datetime fdt_data_al, ref decimal fd_ore_ordinarie, ref decimal fd_ore_straordinarie, ref decimal fd_ore_flessibilita, ref string fs_errore);//estrae le ore lavorate da un dipendente in un dato periodo dal database delle trimbrature
//tabella risultati

string ls_matricola

//la matricola nel DB timbrature è un char(10), riempito con "0"
ls_matricola = right("0000000000" + string(fl_matricola), 10)

if uof_risultati_matricola_data(ls_matricola,fdt_data_dal,fdt_data_al,fd_ore_ordinarie,fd_ore_straordinarie,fd_ore_flessibilita,fs_errore) < 0 then
	//in fs_errore il messaggio
	return -1
end if

return 1
end function

public function integer uof_rp_cdc_variaz_online (datetime fdt_data_dal, datetime fdt_data_al, string fs_cdc[], ref decimal fd_ol[], ref string fs_errore);//modifica se necessario la struttura vettoriale OL[] (ore ordinarie lavorate nel mese dal-al) tenendo
//conto delle possibili variazioni per matricola rispetto al centro di costo di default
//vedi tabella "fine" nel database delle presenze

datastore lds_variazioni
string ls_sql, ls_cdc_variazione, ls_cdc_default, ls_temp, ls_mese
decimal ld_ore_variazione, ld_temp
long ll_index, ll_index2, ll_month
boolean lb_trovato

if isvalid(w_stato_elaborazione) then &
			w_stato_elaborazione.st_log.text = "Lettura Dati Variazioni CdC~r~n~r~ndal: "&
							+string(fdt_data_dal,"dd/mm/yyyy")+" al "+string(fdt_data_al,"dd/mm/yyyy")

ll_month = month(date(fdt_data_dal))

choose case ll_month
	case 1
		ls_mese=" Gennaio "
	case 1
		ls_mese=" Febbraio "
	case 1
		ls_mese=" Marzo "
	case 1
		ls_mese=" Aprile "
	case 1
		ls_mese=" Maggio "
	case 1
		ls_mese=" Giugno "
	case 1
		ls_mese=" Luglio "
	case 1
		ls_mese=" Agosto "
	case 1
		ls_mese=" Settembre "
	case 1
		ls_mese=" Ottobre "
	case 1
		ls_mese=" Novembre "
	case 1
		ls_mese=" Dicembre "
end choose

//ls_sql = "select fine.tempou,fine.cenco,dipen.cenco "+&
//			"from fine "+&
//			"inner join dipen on dipen.azienda=fine.azienda and "+&
//										"dipen.matricola=fine.matricola "+&
//			"where fine.azienda='"+is_cod_azienda+"' and "+&
//					"month(fine.data_inizio)="+string(ll_month)+" "
ls_sql = "select fine.tempou,fine.cenco,dipen.cenco "+&
			"from fine "+&
			"inner join dipen on dipen.azienda=fine.azienda and "+&
										"dipen.matricola=fine.matricola "+&
			"where fine.azienda='"+is_cod_azienda+"' and "+&
					"fine.data_inizio>="+uof_date_format_msaccess(fdt_data_dal)+" and "+&
					"fine.data_inizio<="+uof_date_format_msaccess(fdt_data_al)+" "
					
					
if uof_connetti_presenze(fs_errore) < 0 then
	//in fs_errore il messaggio di errore
	return -1
end if

//creo il datastore
if not uof_crea_ds_tran(lds_variazioni,ls_sql,itran_presenze,fs_errore) then
	//in fs_errore il messaggio di errore
	uof_add_log(is_prefisso_errore + fs_errore)
	uof_disconnetti_presenze()
   return -1
end if

lds_variazioni.retrieve()

//non serve più la transazione con access
uof_disconnetti_presenze()

for ll_index=1 to lds_variazioni.rowcount()
	Yield()
	
	ld_ore_variazione = lds_variazioni.getitemnumber(ll_index, 1)
	ls_cdc_variazione = lds_variazioni.getitemstring(ll_index, 2)
	ls_cdc_default = lds_variazioni.getitemstring(ll_index, 3)
	
	if isvalid(w_stato_elaborazione) then &
			w_stato_elaborazione.st_log.text = "Variaz. da CdC "+ls_cdc_default+" a CdC "+ls_cdc_variazione+"~r~n~r~ndal: "&
							+string(fdt_data_dal,"dd/mm/yyyy")+" al "+string(fdt_data_al,"dd/mm/yyyy")
	
	//trasformo i secondi letti in ore
	ld_ore_variazione = round(ld_ore_variazione / 3600, 4)
	
	//-----------------------------------------------------------------------------------------------
	//cerco la posizione CdC di default nella struttura per DE-CREMENTARE OL di ld_ore_variazione
	lb_trovato = false
	ld_temp = 0
	
	for ll_index2=1 to upperbound(fs_cdc)
		ls_temp = fs_cdc[ll_index2]
		
		if ls_temp=ls_cdc_default then
			//trovato: de-cremento
			ld_temp = fd_ol[ll_index2]
			
			//decremento
			fd_ol[ll_index2] -= ld_ore_variazione
			
			if fd_ol[ll_index2] < 0 then
				fd_ol[ll_index2] = 0
				
				uof_add_log(is_prefisso_errore+"Il valore di OL del CdC "+ls_temp+" è stato decrementato da "+string(ld_temp,"###,###,##0.00##")+&
								" del valore "+string(ld_ore_variazione, "###,###,##0.00##")+&
								"come impostato nelle variazioni del mese di"+ls_mese+&
								"~r~nPoichè il nuovo valore risulta NEGATIVO viene impostato a ZERO!!!")
			else
				uof_add_log("Il valore di OL del CdC "+ls_temp+" è stato decrementato da "+string(ld_temp, "###,###,##0.00##")+&
								" del valore "+string(ld_ore_variazione, "###,###,##0.00##")+&
								"come impostato nelle variazioni del mese di"+ls_mese+&
								"~r~nNuovo Valore: "+string(fd_ol[ll_index2],"###,###,##0.00##"))
			end if
			
			lb_trovato = true
		end if
		
	next
	
	//alla fine avviso, caso mai non ho trovato il CdC di default nella struttura
	if not lb_trovato then
		uof_add_log(is_prefisso_errore+"Il CdC "+ls_temp+" non è stato trovato nella struttura (mese di "+ls_mese+")"+&
								" Non viene quindi effettuata nessuna operazione di decremento su OL")
	end if
	//-----------------------------------------------------------------------------------------------
	
	//cerco la posizione CdC di variazione nella struttura per INCREMENTARE OL di ld_ore_variazione
	lb_trovato = false
	ld_temp = 0
	
	for ll_index2=1 to upperbound(fs_cdc)
		ls_temp = fs_cdc[ll_index2]
		
		if ls_temp=ls_cdc_variazione then
			//trovato: INcremento
			ld_temp = fd_ol[ll_index2]
			
			//decremento
			fd_ol[ll_index2] += ld_ore_variazione
			
			uof_add_log("Il valore di OL del CdC "+ls_temp+" è stato INcrementato da "+string(ld_temp,"###,###,##0.00##")+&
								" del valore "+string(ld_ore_variazione, "###,###,##0.00##")+&
								"come impostato nelle variazioni del mese di"+ls_mese+&
								"~r~nNuovo Valore: "+string(fd_ol[ll_index2],"###,###,##0.00##"))
			lb_trovato = true
		end if
		
	next
	
	//alla fine avviso, caso mai non ho trovato il CdC di default nella struttura
	if not lb_trovato then
		uof_add_log(is_prefisso_errore+"Il CdC "+ls_temp+" non è stato trovato nella struttura (mese di "+ls_mese+")"+&
								" Non viene quindi effettuata nessuna operazione di incremento su OL")
	end if
	//-----------------------------------------------------------------------------------------------
		
	lb_trovato = false
	ld_temp = 0
next

destroy lds_variazioni;


return 1
end function

public function integer uof_get_codoperaio_matricola (ref string fs_cod_operaio, ref string fs_matricola, ref string fs_errore);//fs_cod_operaio : colonna cod_operaio della tabella anag_operai di APICE 				(varchar(6))
//fs_matricola : colonna matricola della tabella dipen del database delle Presenze  (varchar(10))
long ll_matricola

if isnull(fs_cod_operaio) or fs_cod_operaio="" then
	//estrapola il cod_operaio a partire dalla matricola
	ll_matricola = long(fs_matricola)
	
	select cod_operaio
	into :fs_cod_operaio
	from anag_operai
	where cod_azienda=:s_cs_xx.cod_azienda and
			num_matricola=:ll_matricola
	using sqlca;
	
	if sqlca.sqlcode<0 then
		fs_errore="Errore in lettura cod_operaio a partire dalla matricola! "+sqlca.sqlerrtext
		uof_add_log(is_prefisso_errore+fs_errore)
		
		return -1
	end if
	
	if sqlca.sqlcode=100 or fs_cod_operaio="" or isnull(fs_cod_operaio) then
		fs_errore="Operaio con matricola "+string(ll_matricola)+" non trovato in APICE!"
		uof_add_log(is_prefisso_errore+fs_errore)
		
		return -1
	end if
	
else
	//estrapola la matricola nel formato del database delle Presenze a partire da cod_operaio
	select num_matricola
	into :ll_matricola
	from anag_operai
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_operaio=:fs_cod_operaio
	using sqlca;
	
	if sqlca.sqlcode<0 then
		fs_errore="Errore in lettura num_matricola a partire dalla cod_operaio! "+sqlca.sqlerrtext
		uof_add_log(is_prefisso_errore+fs_errore)
		
		return -1
	end if
	
	if sqlca.sqlcode=100 or isnull(ll_matricola) then
		fs_errore="Operaio con codice "+fs_cod_operaio+" non trovato in APICE!"
		uof_add_log(is_prefisso_errore+fs_errore)
		
		return -1
	end if
	
	fs_matricola = right("0000000000" + string(ll_matricola), 10)
	
end if


return 1
end function

public function integer uof_get_cdc_dipendente_presenze (string fs_matricola, ref string fs_cdc_default, ref string fs_errore);//legge dal database delle presenze il CdC assegnato al dipendente (dipen.cenco)

if uof_connetti_presenze(fs_errore) < 0 then
	//in fs_errore il messaggio di errore
	return -1
end if


select cenco
into :fs_cdc_default
from dipen
where azienda=:is_cod_azienda and
		matricola=:fs_matricola
using itran_presenze;

if itran_presenze.sqlcode<0 then
	fs_errore = "Errore in lettura CdC default per la matricola "+fs_matricola+" dal DB presenze: "+itran_presenze.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	
	uof_disconnetti_presenze()
	
	return -1
end if

if itran_presenze.sqlcode=100 or isnull(fs_cdc_default) or fs_cdc_default="" then
	fs_errore = "Matricola "+fs_matricola+" non trovata oppure CdC non impostato nel DB presenze "
	uof_add_log(is_prefisso_errore+fs_errore)
	
	uof_disconnetti_presenze()
	
	return -1
end if

uof_disconnetti_presenze()

return 1
end function

public function integer uof_rp_cdc_online (datetime fdt_data_dal, datetime fdt_data_al, string fs_cdc[], decimal fd_importi_cdc[], decimal fd_totale, string fs_tipo, ref string fs_errore);
//per tutti i CdC possibili dal database presenze elaboro giornalmente dal bollettato (RPrep = FLrep / OLrep)
//le due date passate sono sempre riferite al primo del mese e alla fine dello stesso mese

string ls_cod_centro_costo, ls_cdc_v[], ls_cdc_desc_v[], ls_cdc_temp
decimal ld_ore_ordinarie, ld_ore_straordinarie, ld_ore_flessibilita, ld_ore_diaria
decimal ld_ol, ld_fl, ld_rp, ld_ol_progressivo, ld_fl_progressivo, ld_ol_v[], ld_fl_v[]
long ll_index, ll_index2, ll_anno_rif
boolean lb_trovato, lb_inizio

ll_anno_rif = year(date(fdt_data_dal))

//il sistema deve saper distinguere se un'area ripartisce il bollettato/fatturato oppure no
//per questi codici non deve essere ripartito, ma FLrep coincide con VFLP (intero fatturato/bollettato)
string ls_sql_UFF_MAG_AUT[], ls_sql_UFF_MAG_AUT_descriz[]

//ls_sql_UFF_MAG_AUT[1] = "008"
//ls_sql_UFF_MAG_AUT[2] = "006"
//ls_sql_UFF_MAG_AUT[3] = "007"
if uof_get_cdc_noripartizione(ls_sql_UFF_MAG_AUT[],ls_sql_UFF_MAG_AUT_descriz[],fs_errore) < 0 then
	//in fs_errore il messaggio
	
	return -1
end if


if fs_tipo = "F" then
	fs_tipo = "S"
else
	fs_tipo = "N"
end if


//--------------------------------------------------------------
lb_inizio = false

uof_add_log(" ")
uof_add_log("-------------------------------------------------------------------")
uof_add_log("INIZIO AGGIORNAMENTO e MEMORIZZAZIONE RPrep in database per i diversi Centri di Costo")
uof_add_log("-------------------------------------------------------------------")

//finestra di scorrimento elaborazione
if ib_apri_wait then open(w_stato_elaborazione)

//estrapola per tutti i CdC possibili dal database presenze
if uof_get_cdc_presenze(ls_cdc_v[],ls_cdc_desc_v[],fs_errore) < 0 then
	//in fs_errore il messaggio
	
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	
	return -1
end if


//per quanto riguarda FLrep
//considera quanto scritto nella variabili passate (fs_cdc[],fd_importi_cdc[],fd_totale)
//elabora ls_cdc_v[] che li contiene tutti
//se il cdc è presente in fs_cdc[] allora leggi l'importo da fd_importi_cdc[] o da fd_totale (quest'ultimo per le aree
//UFFICI, MAGAZZINO, AUTISTI, per i quali non abbiamo ripartizione % del fatturato/bollettato), altrimenti sarà ZERO
//per quanto riguarda OLrep è la somma dei vettori ld_ore_ordinarie[],
//																	ld_ore_straordinarie[],
//																	ld_ore_flessibilita[]
//per ciascuna singola componente corrispondente di ls_cdc_v[]

for ll_index = 1 to upperbound(ls_cdc_v[])
	Yield()
	
	ls_cdc_temp = ls_cdc_v[ll_index]
	
	if isvalid(w_stato_elaborazione) then &
			w_stato_elaborazione.st_log.text = "Elab.CdC "+ls_cdc_temp+"~r~n~r~ndal: "&
							+string(fdt_data_dal,"dd/mm/yyyy")+" al "+string(fdt_data_al,"dd/mm/yyyy")
	
	//azzera le variabili
	ld_ol = 0
	ld_fl = 0
	ld_rp = 0
	lb_trovato = false
	
	for ll_index2=1 to upperbound(ls_sql_UFF_MAG_AUT[])
		if ls_cdc_temp = ls_sql_UFF_MAG_AUT[ll_index2] then
			//no ripartizione
			lb_trovato = true
		end if
	next
	
	if lb_trovato then
		//no ripartizione ##############################
		
		//recupera FLrep, che corrisponde al totale
		ld_fl = fd_totale
	else
		//ripartizione #################################
		
		//recupera FLrep dal vettore fs_cdc[], se presente, se non presente considera ZERO
		
		for ll_index2=1 to upperbound(fs_cdc[])
			if ls_cdc_temp = fs_cdc[ll_index2] then
				//trovato
				lb_trovato = true
				
				//esci dal primo ciclo
				//in ll_index2 la posizione relativa
				exit
				
			end if
		next
		
		if lb_trovato then
			//trovato
			ld_fl = fd_importi_cdc[ll_index2]
		else
			//non trovato, considera ZERO
			ld_fl = 0
		end if
		
	end if
	
	//recupera OLrep del CdC corrente: N.B. non tiene conto delle variazioni risp. ai CdC di default dei dipendenti
	//verrà tenuto conto di questo successivamente
	if uof_risultati_cdc_data(ls_cdc_temp,fdt_data_dal,fdt_data_al,&
										ld_ore_ordinarie,ld_ore_straordinarie,ld_ore_flessibilita,ld_ore_diaria,fs_errore) < 0 then
		
		if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
		
		//in fs_errore il messaggio
		return -1
	end if
	
	//ore di lavoro totali del CdC nel periodo considerato
	//N.B. i valori sono stati già trasformati in ore (divisione per 3600)
	ld_ol = ld_ore_ordinarie + ld_ore_straordinarie + ld_ore_flessibilita + ld_ore_diaria
	

	ld_fl = round(ld_fl, 4)
	ld_ol = round(ld_ol, 4)
	
	//MEMORIZZA IN ARRAY ----------------------------
	ld_fl_v[ll_index] = ld_fl
	ld_ol_v[ll_index] = ld_ol
	//-----------------------------------------------
	
	//SCRIVI NEL LOG IL VALORE RELATIVO OL[]
	uof_add_log("     OL["+ls_cdc_temp+"] = " + string(ld_ol, "###,###,##0.0000") + "  NOTA: senza variazioni!!")
	
next

//adesso riciclo i vettori per fare gli inserimenti
//ma prima verifico se per caso c'è stata, nel mese, qualche variazione di operatori sui CdC rispetto al default
//queste variazioni le trovo nella tabella fine del database delle presenze

if uof_rp_cdc_variaz_online(fdt_data_dal,fdt_data_al,ls_cdc_v[],ld_ol_v[],fs_errore) < 0 then
	//in fs_errore il messaggio
	
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	
	return -1
end if

for ll_index = 1 to upperbound(ls_cdc_v[])
	Yield()
	
	ls_cdc_temp = ls_cdc_v[ll_index]
	ld_fl = ld_fl_v[ll_index]
	ld_ol = ld_ol_v[ll_index]
	
	//prima dell'inserimento valuta i progressivi OL e FL nell'anno riferimento --------------
	select sum(fl)
	into :ld_fl_progressivo
	from premi_rp_online
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_centro_costo = :ls_cdc_temp and
			year(data_giorno)=:ll_anno_rif;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in lettura progressivi FL da premi_rp_online: "+sqlca.sqlerrtext
		uof_add_log(is_prefisso_errore + fs_errore)
		
		if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
		
		return -1
	end if
	
	select sum(ol)
	into :ld_ol_progressivo
	from premi_rp_online
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_centro_costo = :ls_cdc_temp and
			year(data_giorno)=:ll_anno_rif;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in lettura progressivi OL da premi_rp_online: "+sqlca.sqlerrtext
		uof_add_log(is_prefisso_errore + fs_errore)
		
		if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
		
		return -1
	end if
	
	if isnull(ld_fl_progressivo) then ld_fl_progressivo = 0
	if isnull(ld_ol_progressivo) then ld_ol_progressivo = 0
	
	//incremento i progressivi #########
	ld_fl_progressivo += ld_fl
	ld_ol_progressivo += ld_ol
	
	//fai il rapporto in base ai progressivi raggiunti
	if ld_ol_progressivo > 0 then
		ld_rp = ld_fl_progressivo / ld_ol_progressivo
	else
		ld_rp = 0
	end if
	
	ld_fl = round(ld_fl, 4)
	ld_ol = round(ld_ol, 4)
	ld_fl_progressivo = round(ld_fl_progressivo, 4)
	ld_ol_progressivo = round(ld_ol_progressivo, 4)
	ld_rp = round(ld_rp, 4)
	//------------------------------------------------------------------------
	
	//memorizza in tabella,servirà per le elaborazioni grafiche per i capi reparto ---------------------
	insert into premi_rp_online  
         ( cod_azienda,   
           cod_centro_costo,   
           data_giorno,   
           rp,   
           fl,   
           ol,
			  fl_progressivo,   
           ol_progressivo,
			  flag_consolidato)  
  	values (:s_cs_xx.cod_azienda,   
           :ls_cdc_temp,   
           :fdt_data_dal,   
           :ld_rp,   
           :ld_fl,   
           :ld_ol,
			  :ld_fl_progressivo,
			  :ld_ol_progressivo,
			  :fs_tipo)
	using sqlca;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in inserimento premi_rp_online: "+sqlca.sqlerrtext
		uof_add_log(is_prefisso_errore + fs_errore)
		uof_add_log("insert into premi_rp_online: "+string(fdt_data_dal,"dd/mm/yy")+"~t"+&
								ls_cdc_temp+"~t"+string(ld_rp)+"~t"+string(ld_fl)+"~t"+string(ld_ol)+&
								"~t"+string(ld_fl_progressivo)+"~t"+string(ld_ol_progressivo))
		
		if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
		
		return -1
	end if
	
	if not lb_inizio then
		uof_add_log("---------------------------: "+"D A T A"+"~t"+&
									"CdC"+"~t"+"RP prog"+"~t"+"F L"+"~t"+"O L"+&
									"~t"+"FL prog"+"~t"+"OL prog")
									
		lb_inizio = true
	end if
	
	uof_add_log("insert into premi_rp_online: "+string(fdt_data_dal,"dd/mm/yy")+"~t"+&
								ls_cdc_temp+"~t"+string(ld_rp)+"~t"+string(ld_fl)+"~t"+string(ld_ol)+&
								"~t"+string(ld_fl_progressivo)+"~t"+string(ld_ol_progressivo))
	
next

if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)

uof_add_log(" ")
uof_add_log("-------------------------------------------------------------------")
uof_add_log("FINE AGGIORNAMENTO e MEMORIZZAZIONE RPrep in database per i diversi Centri di Costo")
uof_add_log("-------------------------------------------------------------------")

return 1
end function

public function integer uof_rp_cdc_mese_online (datetime fdt_data_dal, datetime fdt_data_al, string fs_cdc[], decimal fd_importi_cdc[], decimal fd_totale, ref string fs_errore);
//per tutti i CdC possibili dal database presenze elaboro giornalmente dal bollettato (RPrep = FLrep / OLrep)
//le due date passate sono sempre riferite al primo del mese e alla fine dello stesso mese

string ls_cod_centro_costo, ls_cdc_v[], ls_cdc_desc_v[], ls_cdc_temp
decimal ld_ore_ordinarie, ld_ore_straordinarie, ld_ore_flessibilita, ld_ore_diaria
decimal ld_ol, ld_fl, ld_rp, ld_ol_progressivo, ld_fl_progressivo, ld_ol_v[], ld_fl_v[]
long ll_index, ll_index2, ll_anno_rif, ll_mese_inizio
boolean lb_trovato, lb_inizio

//il sistema deve saper distinguere se un'area ripartisce il bollettato/fatturato oppure no
//per questi codici non deve essere ripartito, ma FLrep coincide con VFLP (intero fatturato/bollettato)
string ls_sql_UFF_MAG_AUT[],ls_sql_UFF_MAG_AUT_descriz[]

//ls_sql_UFF_MAG_AUT[1] = "008"
//ls_sql_UFF_MAG_AUT[2] = "006"
//ls_sql_UFF_MAG_AUT[3] = "007"
if uof_get_cdc_noripartizione(ls_sql_UFF_MAG_AUT[],ls_sql_UFF_MAG_AUT_descriz[],fs_errore) < 0 then
	//in fs_errore il messaggio
	
	return -1
end if


//ocio che quello mensile è sempre sul bollettato
//e in fdt_data_dal c'è sempre il primo del mese corrente ed anno corrente
ll_anno_rif = year(date(fdt_data_dal))
ll_mese_inizio = month(date(fdt_data_dal))

//--------------------------------------------------------------
lb_inizio = false

uof_add_log(" ")
uof_add_log("-------------------------------------------------------------------")
uof_add_log("INIZIO AGGIORNAMENTO e MEMORIZZAZIONE RPrep MESE CORRENTE in database per i diversi Centri di Costo")
uof_add_log("-------------------------------------------------------------------")

//finestra di scorrimento elaborazione
if ib_apri_wait then open(w_stato_elaborazione)

//estrapola per tutti i CdC possibili dal database presenze
if uof_get_cdc_presenze(ls_cdc_v[],ls_cdc_desc_v[],fs_errore) < 0 then
	//in fs_errore il messaggio
	
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	
	return -1
end if


//per quanto riguarda FLrep
//considera quanto scritto nella variabili passate (fs_cdc[],fd_importi_cdc[],fd_totale)
//elabora ls_cdc_v[] che li contiene tutti
//se il cdc è presente in fs_cdc[] allora leggi l'importo da fd_importi_cdc[] o da fd_totale (quest'ultimo per le aree
//UFFICI, MAGAZZINO, AUTISTI, per i quali non abbiamo ripartizione % del fatturato/bollettato), altrimenti sarà ZERO
//per quanto riguarda OLrep è la somma dei vettori ld_ore_ordinarie[],
//																	ld_ore_straordinarie[],
//																	ld_ore_flessibilita[]
//per ciascuna singola componente corrispondente di ls_cdc_v[]

for ll_index = 1 to upperbound(ls_cdc_v[])
	Yield()
	
	ls_cdc_temp = ls_cdc_v[ll_index]
	
	if isvalid(w_stato_elaborazione) then &
			w_stato_elaborazione.st_log.text = "Elab.CdC "+ls_cdc_temp+"~r~n~r~ndal: "&
							+string(fdt_data_dal,"dd/mm/yyyy")+" al "+string(fdt_data_al,"dd/mm/yyyy")
	
	//azzera le variabili
	ld_ol = 0
	ld_fl = 0
	ld_rp = 0
	lb_trovato = false
	
	for ll_index2=1 to upperbound(ls_sql_UFF_MAG_AUT[])
		if ls_cdc_temp = ls_sql_UFF_MAG_AUT[ll_index2] then
			//no ripartizione
			lb_trovato = true
		end if
	next
	
	if lb_trovato then
		//no ripartizione ##############################
		
		//recupera FLrep, che corrisponde al totale
		ld_fl = fd_totale
	else
		//ripartizione #################################
		
		//recupera FLrep dal vettore fs_cdc[], se presente, se non presente considera ZERO
		
		for ll_index2=1 to upperbound(fs_cdc[])
			if ls_cdc_temp = fs_cdc[ll_index2] then
				//trovato
				lb_trovato = true
				
				//esci dal primo ciclo
				//in ll_index2 la posizione relativa
				exit
				
			end if
		next
		
		if lb_trovato then
			//trovato
			ld_fl = fd_importi_cdc[ll_index2]
		else
			//non trovato, considera ZERO
			ld_fl = 0
		end if
		
	end if
	
	//recupera OLrep del CdC corrente: N.B. non tiene conto delle variazioni risp. ai CdC di default dei dipendenti
	//verrà tenuto conto di questo successivamente
	if uof_risultati_cdc_data(ls_cdc_temp,fdt_data_dal,fdt_data_al,&
										ld_ore_ordinarie,ld_ore_straordinarie,ld_ore_flessibilita,ld_ore_diaria,fs_errore) < 0 then
		
		if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
		
		//in fs_errore il messaggio
		return -1
	end if
	
	//ore di lavoro totali del CdC nel periodo considerato
	//N.B. i valori sono stati già trasformati in ore (divisione per 3600)
	ld_ol = ld_ore_ordinarie + ld_ore_straordinarie + ld_ore_flessibilita + ld_ore_diaria
	

	ld_fl = round(ld_fl, 4)
	ld_ol = round(ld_ol, 4)
	
	//MEMORIZZA IN ARRAY ----------------------------
	ld_fl_v[ll_index] = ld_fl
	ld_ol_v[ll_index] = ld_ol
	//-----------------------------------------------
	
next

//adesso riciclo i vettori per fare gli inserimenti
//ma prima verifico se per caso c'è stata, nel mese, qualche variazione di operatori sui CdC rispetto al default
//queste variazioni le trovo nella tabella fine del database delle presenze

if uof_rp_cdc_variaz_online(fdt_data_dal,fdt_data_al,ls_cdc_v[],ld_ol_v[],fs_errore) < 0 then
	//in fs_errore il messaggio
	
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	
	return -1
end if

for ll_index = 1 to upperbound(ls_cdc_v[])
	Yield()
	
	ls_cdc_temp = ls_cdc_v[ll_index]
	ld_fl = ld_fl_v[ll_index]
	ld_ol = ld_ol_v[ll_index]
	
	//prima dell'inserimento valuta i progressivi OL e FL --------------
	select sum(fl)
	into :ld_fl_progressivo
	from premi_rp_mese_online
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_centro_costo = :ls_cdc_temp and
			year(data_giorno)=:ll_anno_rif and
			month(data_giorno)=:ll_mese_inizio;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in lettura progressivi FL da premi_rp_mese_online: "+sqlca.sqlerrtext
		uof_add_log(is_prefisso_errore + fs_errore)
		
		if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
		
		return -1
	end if
	
	select sum(ol)
	into :ld_ol_progressivo
	from premi_rp_mese_online
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_centro_costo = :ls_cdc_temp and
			year(data_giorno)=:ll_anno_rif and
			month(data_giorno)=:ll_mese_inizio;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in lettura progressivi OL da premi_rp_mese_online: "+sqlca.sqlerrtext
		uof_add_log(is_prefisso_errore + fs_errore)
		
		if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
		
		return -1
	end if
	
	if isnull(ld_fl_progressivo) then ld_fl_progressivo = 0
	if isnull(ld_ol_progressivo) then ld_ol_progressivo = 0
	
	//incremento i progressivi #########
	ld_fl_progressivo += ld_fl
	ld_ol_progressivo += ld_ol
	
	//fai il rapporto in base ai progressivi raggiunti
	if ld_ol_progressivo > 0 then
		ld_rp = ld_fl_progressivo / ld_ol_progressivo
	else
		ld_rp = 0
	end if
	
	ld_fl = round(ld_fl, 4)
	ld_ol = round(ld_ol, 4)
	ld_fl_progressivo = round(ld_fl_progressivo, 4)
	ld_ol_progressivo = round(ld_ol_progressivo, 4)
	ld_rp = round(ld_rp, 4)
	//------------------------------------------------------------------------
	
	//memorizza in tabella,servirà per le elaborazioni grafiche per i capi reparto ---------------------
	insert into premi_rp_mese_online  
         ( cod_azienda,   
           cod_centro_costo,   
           data_giorno,   
           rp,   
           fl,   
           ol,
			  fl_progressivo,   
           ol_progressivo)  
  	values (:s_cs_xx.cod_azienda,   
           :ls_cdc_temp,   
           :fdt_data_dal,   
           :ld_rp,   
           :ld_fl,   
           :ld_ol,
			  :ld_fl_progressivo,
			  :ld_ol_progressivo)
	using sqlca;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in inserimento premi_rp_mese_online: "+sqlca.sqlerrtext
		uof_add_log(is_prefisso_errore + fs_errore)
		uof_add_log("insert into premi_rp_mese_online: "+string(fdt_data_dal,"dd/mm/yy")+"~t"+&
								ls_cdc_temp+"~t"+string(ld_rp)+"~t"+string(ld_fl)+"~t"+string(ld_ol)+&
								"~t"+string(ld_fl_progressivo)+"~t"+string(ld_ol_progressivo))
		
		if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
		
		return -1
	end if
	
	if not lb_inizio then
		uof_add_log("---------------------------: "+"D A T A"+"~t"+&
									"CdC"+"~t"+"RP prog"+"~t"+"F L"+"~t"+"O L"+&
									"~t"+"FL prog"+"~t"+"OL prog")
									
		lb_inizio = true
	end if
	
	uof_add_log("insert into premi_rp_mese_online: "+string(fdt_data_dal,"dd/mm/yy")+"~t"+&
								ls_cdc_temp+"~t"+string(ld_rp)+"~t"+string(ld_fl)+"~t"+string(ld_ol)+&
								"~t"+string(ld_fl_progressivo)+"~t"+string(ld_ol_progressivo))
	
next

if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)

uof_add_log(" ")
uof_add_log("-------------------------------------------------------------------")
uof_add_log("FINE AGGIORNAMENTO e MEMORIZZAZIONE RPrep MESE CORRENTE in database per i diversi Centri di Costo")
uof_add_log("-------------------------------------------------------------------")

return 1
end function

public function integer uof_ordini_cdc (datetime fdt_data_dal, datetime fdt_data_al, ref string fs_cdc[], ref decimal fd_importi[], ref decimal fd_totale, ref string fs_errore);//estrae gli importi righe ordini

//gli importi vengono ripartit in % secondo le regole scritte in premi_prodotti_reparti
//dove si hanno fino a 2 reparti con relative percentuali, se non c'è nessuna regola allora l'importo va solo nel "calderone" del totale

//ovvio che occorre distinguere tra confezionato e non confezione (2nda cifra del prodotto = 0)

string ls_vuoto[],ls_sql_regole,ls_sintassi_regole,ls_err,ls_cifra_tenda,ls_sql_count,ls_sql,ls_cod_prodotto
string ls_cod_centro_costo, ls_riga_ordine
decimal ld_vuoto[], ld_imponibile_iva
long ll_index, ll_ret, ll_tot_regole,ll_count,ll_anno_reg,ll_num_reg,ll_prog_riga
uo_produzione luo_prod
datastore lds_regole

//reset variabili di uscita
fs_cdc = ls_vuoto
fd_importi = ld_vuoto
fd_totale = 0

uof_add_log(" ")
uof_add_log("---------------------------------------------------")
uof_add_log("INIZIO CALCOLO FLrep giornaliero per i diversi Centri di Costo")
uof_add_log("---------------------------------------------------")

if uof_ordini_cdc_sfuso(fdt_data_dal,fdt_data_al,fs_cdc[],fd_importi[],fd_totale,fs_errore) < 0 then
	uof_add_log(is_prefisso_errore+fs_errore)
	
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	
	return -1
end if

if uof_ordini_cdc_nosfuso(fdt_data_dal,fdt_data_al,fs_cdc[],fd_importi[],fd_totale,fs_errore) < 0 then
	uof_add_log(is_prefisso_errore+fs_errore)
	
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	
	return -1
end if


if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)

uof_add_log("---------------------------------------------------")
uof_add_log("FINE CALCOLO FLrep giornaliero per i diversi Centri di Costo")
uof_add_log("---------------------------------------------------")

return 1
end function

public function integer uof_ordini_cdc_sfuso (datetime fdt_data_dal, datetime fdt_data_al, ref string fs_cdc[], ref decimal fd_importi[], ref decimal fd_totale, ref string fs_errore);//estrae gli importi righe ordini

//gli importi vengono ripartit in % secondo le regole scritte in premi_prodotti_reparti
//dove si hanno fino a 2 reparti con relative percentuali, se non c'è nessuna regola allora l'importo va solo nel "calderone" del totale

//ovvio che occorre distinguere tra confezionato e non confezione (2nda cifra del prodotto = 0)

string ls_vuoto[],ls_sql_regole,ls_sintassi_regole,ls_err,ls_cifra_tenda,ls_sql_count,ls_sql,ls_cod_prodotto
string ls_cod_centro_costo, ls_riga_ordine
decimal ld_vuoto[], ld_imponibile_iva
long ll_index, ll_ret, ll_tot_regole,ll_count,ll_anno_reg,ll_num_reg,ll_prog_riga
uo_produzione luo_prod
datastore lds_regole
datetime ldt_data_fine

//reset variabili di uscita
fs_cdc = ls_vuoto
fd_importi = ld_vuoto
fd_totale = 0

uof_add_log(" ")
uof_add_log("---------------------------------------------------")
uof_add_log("INIZIO CALCOLO su ordini sfuso FLrep giornaliero per i diversi Centri di Costo")
uof_add_log("---------------------------------------------------")

//finestra di scorrimento elaborazione
if ib_apri_wait then open(w_stato_elaborazione)

ls_cifra_tenda = "0"

ll_count = 0

ldt_data_fine = datetime(date(fdt_data_al), 23:59:59)

//##################################################################################################################
//select count righe delle chiusure sessione nel giorno (ORDINI DI SFUSO)
ls_sql_count = "select distinct count(*) "+&
					"from sessioni_lavoro as s "+&
					"join det_ordini_produzione as b on b.cod_azienda=s.cod_azienda and "+&
																	"b.progr_det_produzione=s.progr_det_produzione "+&
					"join det_ord_ven as d on d.cod_azienda=b.cod_azienda and "+&
														"d.anno_registrazione=b.anno_registrazione and "+&
														"d.num_registrazione=b.num_registrazione and "+&
														"d.prog_riga_ord_ven=b.prog_riga_ord_ven "+&
					"join tes_ord_ven as t on t.cod_azienda=d.cod_azienda and "+&
														"t.anno_registrazione=d.anno_registrazione and "+&
														"t.num_registrazione=d.num_registrazione "+&
					"where s.cod_azienda='"+s_cs_xx.cod_azienda+"' and s.flag_fine_sessione='S' and "+&
							"t.flag_blocco<>'S' and d.imponibile_iva>0 and "+&
							"s.fine_sessione>='"+string(fdt_data_dal, 'yyyymmdd hh:mm:ss')+"' and "+&
							"s.fine_sessione<='"+string(ldt_data_fine, 'yyyymmdd hh:mm:ss')+"' and "+&
							"substring(d.cod_prodotto, 2, 1)<>'"+ls_cifra_tenda+"' and d.cod_prodotto is not null "

//creazione cursore dinamico conteggio righe da elaborare
declare cu_righesfusocount dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql_count;
open dynamic cu_righesfusocount;

if sqlca.sqlcode <> 0 then
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	fs_errore = "Errore nella open del cursore righe ordini sfuso count. Dettaglio = " + sqlca.sqlerrtext
	
	return -1
end if

//una sola riga
fetch cu_righesfusocount into 
		 :ll_count;

if sqlca.sqlcode < 0 then
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	fs_errore = "Errore nella fetch del cursore righe ordini sfuso count. Dettaglio = " + sqlca.sqlerrtext
	close cu_righesfusocount;
	
	return -1
	
end if	

close cu_righesfusocount;

//in ll_count il numero di righe
ll_index = 0

//impostazione sql: righe ordini (ORDINI DI SFUSO)
ls_sql = "select distinct d.anno_registrazione,d.num_registrazione,"+&
								 "d.prog_riga_ord_ven,d.cod_prodotto,d.imponibile_iva "+&
					"from sessioni_lavoro as s "+&
					"join det_ordini_produzione as b on b.cod_azienda=s.cod_azienda and "+&
																	"b.progr_det_produzione=s.progr_det_produzione "+&
					"join det_ord_ven as d on d.cod_azienda=b.cod_azienda and "+&
														"d.anno_registrazione=b.anno_registrazione and "+&
														"d.num_registrazione=b.num_registrazione and "+&
														"d.prog_riga_ord_ven=b.prog_riga_ord_ven "+&
					"join tes_ord_ven as t on t.cod_azienda=d.cod_azienda and "+&
														"t.anno_registrazione=d.anno_registrazione and "+&
														"t.num_registrazione=d.num_registrazione "+&
					"where s.cod_azienda='"+s_cs_xx.cod_azienda+"' and s.flag_fine_sessione='S' and "+&
							"t.flag_blocco<>'S' and d.imponibile_iva>0 and "+&
							"s.fine_sessione>='"+string(fdt_data_dal, 'yyyymmdd hh:mm:ss')+"' and "+&
							"s.fine_sessione<='"+string(ldt_data_fine, 'yyyymmdd hh:mm:ss')+"' and "+&
							"substring(d.cod_prodotto, 2, 1)<>'"+ls_cifra_tenda+"' and d.cod_prodotto is not null "

//creazione cursore dinamico da elaborare
declare cu_righesfuso dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_righesfuso;

if sqlca.sqlcode <> 0 then
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	fs_errore = "Errore nella open del cursore righe ordini sfuso. Dettaglio = " + sqlca.sqlerrtext
	
	return -1
end if

do while true
	fetch cu_righesfuso into 
			 :ll_anno_reg,:ll_num_reg,:ll_prog_riga,:ls_cod_prodotto,:ld_imponibile_iva;
	
	if sqlca.sqlcode < 0 then
		if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
		fs_errore = "Errore nella fetch del cursore righe ordini sfuso. Dettaglio = " + sqlca.sqlerrtext
		close cu_righesfuso;
		
		return -1
		
	elseif sqlca.sqlcode = 100 then
		exit
	end if	
	
	//prosegui
	Yield()
	ll_index += 1
	
	ls_riga_ordine = " "+string(ll_anno_reg)+"/"+string(ll_num_reg)+"/"+string(ll_prog_riga)
	
	//controlla lo stato produzione della riga ---------------------------------
	luo_prod = create uo_produzione
	ll_ret = luo_prod.uof_stato_prod_det_ord_ven(ll_anno_reg,ll_num_reg,ll_prog_riga,fs_errore)
	destroy luo_prod;
	
	choose case ll_ret
		case -1			//errore
			fs_errore = is_prefisso_errore+ls_riga_ordine
			return -1
			
		case -100		//nessuna fase di produzione per la riga
			uof_add_log(is_prefisso_errore+ls_riga_ordine)
			continue
			
		case 0			//niente
			uof_add_log(is_prefisso_errore+"Stato Produzione NIENTE, riga saltata"+ls_riga_ordine)
			continue
			
		case 1			//tutto: OK, elabora
			
		case 2			//parziale
			uof_add_log(is_prefisso_errore+"Stato Produzione PARZIALE, riga saltata"+ls_riga_ordine)
			continue
	end choose
	//----------------------------------------------------------------------------
	
	//in questa variabile metto il totale imponibile
	fd_totale += ld_imponibile_iva
	
	if isvalid(w_stato_elaborazione) then &
			w_stato_elaborazione.st_log.text = "Elab.righe ordini sfusi chiusi dal "+string(fdt_data_dal, "dd/mm/yy")+&
					" al "+string(fdt_data_al, "dd/mm/yy")+"~r~n~r~n"+ &
					string(ll_anno_reg)+"/"+string(ll_num_reg)+"/"+string(ll_prog_riga)+" ("+string(ll_index)+" di "+string(ll_count)+")"
	
	//leggi il CdC del prodotto ---------------
	select r.cod_centro_costo
	into :ls_cod_centro_costo
	from anag_reparti as r
	join anag_prodotti as p on p.cod_azienda=r.cod_azienda and
										p.cod_reparto=r.cod_reparto
	where r.cod_azienda=:s_cs_xx.cod_azienda and
			p.cod_prodotto=:ls_cod_prodotto
	using sqlca;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in lettura CdC prodotto "+ls_cod_prodotto+" - "+ls_riga_ordine
		return -1
	end if
	
	if ls_cod_centro_costo="" or isnull(ls_cod_centro_costo) then
		uof_add_log(is_prefisso_errore+"CdC non trovato per prodotto "+ls_cod_prodotto+" - "+ls_riga_ordine)
		continue
	end if
	
	
	//aggiorna i vettori dei CdC e degli importi
	uof_fatbol_cdc_aggiorna(ls_cod_centro_costo,ld_imponibile_iva,fs_cdc[],fd_importi[])
	
loop

close cu_righesfuso;

if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)

uof_add_log("---------------------------------------------------")
uof_add_log("FINE CALCOLO su ordini sfuso FLrep giornaliero per i diversi Centri di Costo")
uof_add_log("---------------------------------------------------")

return 1
end function

public function integer uof_ordini_cdc_nosfuso (datetime fdt_data_dal, datetime fdt_data_al, ref string fs_cdc[], ref decimal fd_importi[], ref decimal fd_totale, ref string fs_errore);//estrae gli importi righe ordini

//gli importi vengono ripartit in % secondo le regole scritte in premi_prodotti_reparti
//dove si hanno fino a 2 reparti con relative percentuali, se non c'è nessuna regola allora l'importo va solo nel "calderone" del totale

//ovvio che occorre distinguere tra confezionato e non confezione (2nda cifra del prodotto = 0)

string ls_vuoto[],ls_sql_regole,ls_sintassi_regole,ls_err,ls_cifra_tenda,ls_sql_count,ls_sql,ls_cod_prodotto
string ls_cod_centro_costo, ls_riga_ordine, ls_cod_centro_costo_coll
decimal ld_vuoto[], ld_imponibile_iva
long ll_index, ll_ret, ll_tot_regole,ll_count,ll_anno_reg,ll_num_reg,ll_prog_riga
uo_produzione luo_prod
datastore lds_regole
datetime ldt_data_fine
long ll_prog_riga_coll
string ls_cod_prodotto_coll
decimal ld_imponibile_riga_coll

//reset variabili di uscita
fs_cdc = ls_vuoto
fd_importi = ld_vuoto
fd_totale = 0

uof_add_log(" ")
uof_add_log("---------------------------------------------------")
uof_add_log("INIZIO CALCOLO su ordini NON di sfuso FLrep giornaliero per i diversi Centri di Costo")
uof_add_log("---------------------------------------------------")

//finestra di scorrimento elaborazione
if ib_apri_wait then open(w_stato_elaborazione)

ls_cifra_tenda = "0"

ll_count = 0

ldt_data_fine = datetime(date(fdt_data_al), 23:59:59)

//########################################################################################
//--------------------------------------------------------------------------
//preparo un cursore con le regole da cui leggere la percentuale di suddivisione
//dell'importo righe ordini per reparti
ls_sql_regole = 	"select regola, cod_centro_costo_1, perc_rep_1, cod_centro_costo_2, perc_rep_2 "+&
						"from premi_prodotti_cdc "+&
						"order by posizione "

//creazione datastore dinamico per le regole
ls_sintassi_regole = sqlca.syntaxfromsql(ls_sql_regole, "style(type=grid)", ls_err)
if Len(ls_err) > 0 then
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	fs_errore = "Errore durante la creazione della sintassi del datastore regole ripartizione CdC:" + ls_err + "~r~n"+ls_sql_regole
	
	return -1
end if

lds_regole = create datastore

lds_regole.Create(ls_sintassi_regole, ls_err)
if len(ls_err) > 0 then
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	fs_errore="Errore durante la creazione del datastore regole ripartizione CdC:" + ls_err
	destroy lds_regole;
	
	return -1
end if

lds_regole.settransobject(sqlca)

ll_tot_regole = lds_regole.retrieve()
if ll_tot_regole < 0 then
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	fs_errore="Errore durante la retrieve delle righe del datastore ripartiz.CdC:" + sqlca.sqlerrtext
	destroy lds_regole;
	
	return -1
end if

if ll_tot_regole=0 then
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	fs_errore="Tabella di ripartizione fatturato/bollettato/ordinato in base al prodotto/CdC non impostata! Calcolo non effettuato!"
	destroy lds_regole;
	
	return -1
end if

//adesso in lds_regole, ordinate per priorità ci sono le regole per filtrare il cod prodotto
//e stabilire le percentuali di imponibile per CdC
//es.   	'%Y' 		'cdc1' 75% 	'cdc2' 	25%
//			'A09%'	'cdc1' 60%	'cdc2'	40%	ecc...
//N.B. questa cosa vale solo per il confezionato
//------------------------------------------------------------------------------
//########################################################################################


//##################################################################################################################
//select count righe delle chiusure sessione nel giorno (ORDINI NON DI SFUSO)
ls_sql_count = "select distinct count(*) "+&
					"from sessioni_lavoro as s "+&
					"join det_ordini_produzione as b on b.cod_azienda=s.cod_azienda and "+&
																	"b.progr_det_produzione=s.progr_det_produzione "+&
					"join det_ord_ven as d on d.cod_azienda=b.cod_azienda and "+&
														"d.anno_registrazione=b.anno_registrazione and "+&
														"d.num_registrazione=b.num_registrazione and "+&
														"d.prog_riga_ord_ven=b.prog_riga_ord_ven "+&
					"join tes_ord_ven as t on t.cod_azienda=d.cod_azienda and "+&
														"t.anno_registrazione=d.anno_registrazione and "+&
														"t.num_registrazione=d.num_registrazione "+&
					"where s.cod_azienda='"+s_cs_xx.cod_azienda+"' and s.flag_fine_sessione='S' and "+&
							"t.flag_blocco<>'S' and d.imponibile_iva>0 and "+&
							"s.fine_sessione>='"+string(fdt_data_dal, 'yyyymmdd hh:mm:ss')+"' and "+&
							"s.fine_sessione<='"+string(ldt_data_fine, 'yyyymmdd hh:mm:ss')+"' and "+&
							"substring(d.cod_prodotto, 2, 1)='"+ls_cifra_tenda+"' and d.cod_prodotto is not null and "+&
							"(d.num_riga_appartenenza=0 or d.num_riga_appartenenza is null) "

//creazione cursore dinamico conteggio righe da elaborare
declare cu_righecount dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql_count;
open dynamic cu_righecount;

if sqlca.sqlcode <> 0 then
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	fs_errore = "Errore nella open del cursore righe ordini NON di sfuso count. Dettaglio = " + sqlca.sqlerrtext
	destroy lds_regole;
	
	return -1
end if

//una sola riga
fetch cu_righecount into 
		 :ll_count;

if sqlca.sqlcode < 0 then
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	fs_errore = "Errore nella fetch del cursore righe ordini count. Dettaglio = " + sqlca.sqlerrtext
	close cu_righecount;
	destroy lds_regole;
	
	return -1
	
end if	

close cu_righecount;

//in ll_count il numero di righe
ll_index = 0

//impostazione sql: righe ordini (ORDINI NON DI SFUSO)
ls_sql = "select distinct d.anno_registrazione,d.num_registrazione,"+&
								 "d.prog_riga_ord_ven,d.cod_prodotto,d.imponibile_iva "+&
					"from sessioni_lavoro as s "+&
					"join det_ordini_produzione as b on b.cod_azienda=s.cod_azienda and "+&
																	"b.progr_det_produzione=s.progr_det_produzione "+&
					"join det_ord_ven as d on d.cod_azienda=b.cod_azienda and "+&
														"d.anno_registrazione=b.anno_registrazione and "+&
														"d.num_registrazione=b.num_registrazione and "+&
														"d.prog_riga_ord_ven=b.prog_riga_ord_ven "+&
					"join tes_ord_ven as t on t.cod_azienda=d.cod_azienda and "+&
														"t.anno_registrazione=d.anno_registrazione and "+&
														"t.num_registrazione=d.num_registrazione "+&
					"where s.cod_azienda='"+s_cs_xx.cod_azienda+"' and s.flag_fine_sessione='S' and "+&
							"t.flag_blocco<>'S' and d.imponibile_iva>0 and "+&
							"s.fine_sessione>='"+string(fdt_data_dal, 'yyyymmdd hh:mm:ss')+"' and "+&
							"s.fine_sessione<='"+string(ldt_data_fine, 'yyyymmdd hh:mm:ss')+"' and "+&
							"substring(d.cod_prodotto, 2, 1)='"+ls_cifra_tenda+"' and d.cod_prodotto is not null and "+&
							"(d.num_riga_appartenenza=0 or d.num_riga_appartenenza is null) "

//creazione cursore dinamico da elaborare
declare cu_righe dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_righe;

if sqlca.sqlcode <> 0 then
	if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
	fs_errore = "Errore nella open del cursore righe ordini NON di sfuso. Dettaglio = " + sqlca.sqlerrtext
	destroy lds_regole;
	
	return -1
end if

do while true
	fetch cu_righe into 
			 :ll_anno_reg,:ll_num_reg,:ll_prog_riga,:ls_cod_prodotto,:ld_imponibile_iva;
	
	if sqlca.sqlcode < 0 then
		if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)
		fs_errore = "Errore nella fetch del cursore righe ordini NON di sfuso. Dettaglio = " + sqlca.sqlerrtext
		close cu_righe;
		destroy lds_regole;
		
		return -1
		
	elseif sqlca.sqlcode = 100 then
		exit
	end if	
	
	//prosegui
	Yield()
	ll_index += 1
	
	ls_riga_ordine = " "+string(ll_anno_reg)+"/"+string(ll_num_reg)+"/"+string(ll_prog_riga)
	
	//controlla lo stato produzione della riga ---------------------------------
	luo_prod = create uo_produzione
	ll_ret = luo_prod.uof_stato_prod_det_ord_ven(ll_anno_reg,ll_num_reg,ll_prog_riga,fs_errore)
	destroy luo_prod;
	
	choose case ll_ret
		case -1			//errore
			fs_errore = is_prefisso_errore+ls_riga_ordine
			close cu_righe;
			destroy lds_regole;
			
			return -1
			
		case -100		//nessuna fase di produzione per la riga
			uof_add_log(is_prefisso_errore+ls_riga_ordine)
			continue
			
		case 0			//niente
			uof_add_log(is_prefisso_errore+"Stato Produzione NIENTE, riga saltata"+ls_riga_ordine)
			continue
			
		case 1			//tutto: OK, elabora
			
		case 2			//parziale
			uof_add_log(is_prefisso_errore+"Stato Produzione PARZIALE, riga saltata"+ls_riga_ordine)
			continue
	end choose
	//----------------------------------------------------------------------------
	
	//in questa variabile metto il totale imponibile
	fd_totale += ld_imponibile_iva
	
	if isvalid(w_stato_elaborazione) then &
			w_stato_elaborazione.st_log.text = "Elab.righe ordini NON di sfuso chiusi dal "+string(fdt_data_dal, "dd/mm/yy")+&
					" al "+string(fdt_data_al, "dd/mm/yy")+"~r~n~r~n"+ &
					string(ll_anno_reg)+"/"+string(ll_num_reg)+"/"+string(ll_prog_riga)+" ("+string(ll_index)+" di "+string(ll_count)+")"
	
	
	//elabora in base alle regole
	
	//decidi le percentuali e aggiorna i reparti e imponibili
	uof_fatbol_cdc_percent(lds_regole,ll_anno_reg,ll_num_reg,ll_prog_riga,"O",ls_cod_prodotto,ld_imponibile_iva,fs_cdc[],fd_importi[])
	

	//°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
	//elabora anche per le righe collegate °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
	declare cu_righe_coll cursor for  
		select prog_riga_ord_ven,cod_prodotto,imponibile_iva
		from det_ord_ven
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_reg and num_registrazione=:ll_num_reg and
				num_riga_appartenenza=:ll_prog_riga and cod_prodotto is not null and
				imponibile_iva>0
		using sqlca;

	open cu_righe_coll;

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in OPEN cursore righe collegate a "+ls_riga_ordine+" (riga collegata:"+string(ll_prog_riga_coll)+"~r~n" + sqlca.sqlerrtext
		close cu_righe;
		destroy lds_regole;
		
		return -1
	end if

	do while true
		fetch cu_righe_coll into :ll_prog_riga_coll,:ls_cod_prodotto_coll,:ld_imponibile_riga_coll;
	
		
		if sqlca.sqlcode < 0 then
			close cu_righe_coll;
			fs_errore = "Errore in FETCH cursore righe "+ls_riga_ordine+" (riga collegata:"+string(ll_prog_riga_coll)+")~r~n" + sqlca.sqlerrtext
			close cu_righe_coll;
			close cu_righe;
			destroy lds_regole;
			
			return -1
		end if
		
		if sqlca.sqlcode = 100 then exit
	
		//procedi
		//incremento il totale
		fd_totale += ld_imponibile_riga_coll
		
		
		//leggi il CdC del prodotto ---------------
		select r.cod_centro_costo
		into :ls_cod_centro_costo_coll
		from anag_reparti as r
		join anag_prodotti as p on p.cod_azienda=r.cod_azienda and
											p.cod_reparto=r.cod_reparto
		where r.cod_azienda=:s_cs_xx.cod_azienda and
				p.cod_prodotto=:ls_cod_prodotto_coll
		using sqlca;
		
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in lettura CdC prodotto (riga colleg.) "+ls_cod_prodotto_coll+" - "+ls_riga_ordine
			close cu_righe_coll;
			close cu_righe;
			destroy lds_regole;
			
			return -1
		end if
		
		if ls_cod_centro_costo_coll="" or isnull(ls_cod_centro_costo_coll) then
			uof_add_log(is_prefisso_errore+"CdC non trovato per prodotto (riga colleg.) "+ls_cod_prodotto_coll+" - "+ls_riga_ordine)
			continue
		end if
		
		
		//aggiorna i vettori dei CdC e degli importi
		uof_fatbol_cdc_aggiorna(ls_cod_centro_costo_coll,ld_imponibile_riga_coll,fs_cdc[],fd_importi[])
			
	loop
	close cu_righe_coll;
	//°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
loop

close cu_righe;
destroy lds_regole;

uof_add_log("---------------------------------------------------")
uof_add_log("FINE CALCOLO su ordini NON di sfuso FLrep giornaliero per i diversi Centri di Costo")
uof_add_log("---------------------------------------------------")

uof_fatbol_flrep_scrivilog(fdt_data_dal,fdt_data_al,"O",fs_cdc[],fd_importi[],fd_totale)

if isvalid(w_stato_elaborazione) then close(w_stato_elaborazione)

return 1
end function

public function integer uof_get_nomecognome_da_matricola (string fs_matricola, ref string fs_nome_cognome, ref string fs_errore);
if uof_connetti_presenze(fs_errore) < 0 then
	//in fs_errore il messaggio di errore
	return -1
end if


select cognome + ' ' + nome
into :fs_nome_cognome
from dipen
where azienda = :is_cod_azienda and
		matricola = :fs_matricola
using itran_presenze;

if itran_presenze.sqlcode < 0 then
	//errore
	fs_errore = "Errore in lettura Cognome e Niome dipendente: "+itran_presenze.sqlerrtext
	uof_disconnetti_presenze()
	
	return -1
	
elseif itran_presenze.sqlcode = 100 then
	fs_errore = "Dipendente non trovato per la matricola "+fs_matricola
	uof_disconnetti_presenze()
	
	return -1
	
end if

//non serve più la transazione con access
uof_disconnetti_presenze()

return 1
end function

public function integer uof_get_hsp_vhs_vio (string fs_matricola, datetime fdt_data_inizio, datetime fdt_data_fine, ref string fs_nome_cognome, ref decimal fd_hsp, ref decimal fd_vhs, ref decimal fd_vio, ref string fs_errore);//N.B. passare le variabili data inizio-fine come primo e ultimo dello stesso mese (per cui calcolare tale premio)

//estrapola dal database delle presenze le ore di lavoro straordinarie+flessibilità: HSP
//legge dalla tabella premi_aree il valore impostato di vhs_area, 
//					per l'anno di riferimento ed il CdC default del dipendente: VHS_area
//valuta poi il valore mensile del premio Impegno Straordinario ad personam:	VIO = HSP x VHSarea


decimal ld_ordinarie, ld_straordinarie, ld_flessibili
string ls_matricola, ls_CdC
long ll_matricola, ll_anno_rif, ll_ret


fd_hsp = 0
fd_vhs = 0
fd_vio = 0

uof_add_log("------------------------------------------------------------------------------------------------------------------")
uof_add_log("INIZIO CALCOLO HSP (H staord+flex) e Premio Impegno Straord. VIO=(HSP x VHSarea) ad-personam")


////leggo la matricola del dipendente nel formato 00000000xx -------------------------------------------
////il dato viene letto dal database APICE
//setnull(ls_matricola)
//
//if uof_get_codoperaio_matricola(fs_cod_operaio, ls_matricola, fs_errore) < 0 then
//	// in fs_errore il messaggio
//	
//	return -1
//end if

ls_matricola = fs_matricola


ll_ret = uof_get_nomecognome_da_matricola(ls_matricola, fs_nome_cognome, fs_errore)
if ll_ret < 0 then
	// in fs_errore il messaggio 
	uof_add_log(is_prefisso_errore+fs_errore)

	return -1
	
elseif ll_ret = 100 then
	// in fs_errore il messaggio 
	uof_add_log(is_prefisso_errore+fs_errore)
	
	return -1
	
end if

//------------------------------------------------------
uof_add_log("	Dipendente matr: "+ls_matricola +"-"+ fs_nome_cognome+ " dal "+string(fdt_data_inizio,"dd/mm/yyyy")+" al "+string(fdt_data_fine,"dd/mm/yyyy"))

//calcola HSP, ore di straordinario + quelle di flessibilità (di ld_ordinarie in tal caso ce ne fregherà nulla !!!)
if uof_risultati_matricola_data(ls_matricola, fdt_data_inizio, fdt_data_fine, &
											ld_ordinarie, ld_straordinarie, ld_flessibili, fs_errore) < 0 then
	// in fs_errore il messaggio
	
	return -1
end if

fd_hsp = ld_straordinarie + ld_flessibili

if isnull(fd_hsp) then fd_hsp = 0
fd_hsp = round(fd_hsp, 4)

uof_add_log("	HSP = "+string(fd_hsp))



//leggi CdC di default dal database presenze: la matricola però deve esistere nel database presenze
if uof_get_cdc_dipendente_presenze(ls_matricola, ls_CdC, fs_errore) < 0 then
	//in fs_errore il messaggio
	
	return -1
end if

//leggi vhs_area del CdC per l'anno di riferimento
ll_anno_rif = year(date(fdt_data_inizio))

select vhs_area
into :fd_vhs
from premi_aree
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_riferimento = :ll_anno_rif and
		cod_centro_costo = :ls_CdC
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura VHS area per dipendente "+ls_matricola +"-"+ fs_nome_cognome+": "+sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_errore = "Dato non trovato lettura VHS area da tabella premi_aree per CdC "+ls_CdC +", anno rif."+ string(ll_anno_rif)
	uof_add_log(is_prefisso_errore+fs_errore)
	
	return -1
end if

if isnull(fd_vhs) then fd_vhs = 0
fd_vhs = round(fd_vhs, 4)

uof_add_log("	VHSarea = "+string(fd_vhs))


//Calcola VIO -------------------------------------------------------------------------------------------------------------
fd_vio = fd_hsp * fd_vhs

if isnull(fd_vio) then fd_vio = 0
fd_vio = round(fd_vio, 4)

uof_add_log("	VIO = "+string(fd_vio))

uof_add_log("------------------------------------------------------------------------------------------------------------------")
uof_add_log("FINE CALCOLO HSP e VIO ad-personam Dipendente: "+ls_matricola +"-"+ fs_nome_cognome+" dal "+string(fdt_data_inizio,"dd/mm/yyyy")+" al "+string(fdt_data_fine,"dd/mm/yyyy"))
uof_add_log("------------------------------------------------------------------------------------------------------------------")

return 1
end function

public function integer uof_get_nr_ipia_via (string fs_matricola, datetime fdt_data_inizio, datetime fdt_data_fine, ref string fs_nome_cognome, ref integer fi_nr, ref decimal fd_ipia, ref decimal fd_via_area, ref decimal fd_via, ref string fs_errore);//N.B. passare le variabili data inizio-fine come primo e ultimo dello stesso mese (per cui calcolare tale premio)

//estrapola dal database delle presenze il numero di ritardi (non oltre 1H) nel mese di rif.: NR
//legge dalla tabella premi_aree il valore impostato di vhs_area, 
//					per l'anno di riferimento ed il CdC default del dipendente: VHS_area
//valuta poi il valore mensile del premio Impegno Straordinario ad personam:	VIO = HSP x VHSarea


decimal		ld_ordinarie, ld_straordinarie, ld_flessibili, ld_ipia_1, ld_ipia_2, ld_ipia_3
string		ls_CdC,ls_flag_via[], ls_flag_eroga_via
long			ll_matricola, ll_anno_rif, ll_ret, ll_mese_rif
decimal		ld_hofp,ld_ore_straord,ld_ore_flessibilita

fi_NR = 0
fd_ipia = 0
fd_via = 0

uof_add_log("------------------------------------------------------------------------------------------------------------------")
uof_add_log("INIZIO CALCOLO IPIA e Premio Presenza VIA=(IPIA x VIAarea) ad-personam")


////leggo la matricola del dipendente nel formato 00000000xx -------------------------------------------
////il dato viene letto dal database APICE
//setnull(ls_matricola)
//
//if uof_get_codoperaio_matricola(fs_cod_operaio, ls_matricola, fs_errore) < 0 then
//	// in fs_errore il messaggio
//	
//	return -1
//end if
ll_ret = uof_get_nomecognome_da_matricola(fs_matricola, fs_nome_cognome, fs_errore)
if ll_ret < 0 then
	// in fs_errore il messaggio 
	uof_add_log(is_prefisso_errore+fs_errore)

	return -1
	
elseif ll_ret = 100 then
	// in fs_errore il messaggio 
	uof_add_log(is_prefisso_errore+fs_errore)
	
	return -1
	
end if

//------------------------------------------------------
uof_add_log("	Dipendente matricola: "+fs_matricola +"-"+ fs_nome_cognome+ " dal "+string(fdt_data_inizio,"dd/mm/yyyy")+" al "+string(fdt_data_fine,"dd/mm/yyyy"))

//calcola NR, numero di ritardi nel mese non oltre 1 ora
//N.B. se fai un ritardo oltre 1 ora viene considerato permesso e quindi non va considerato in NR....
//leggi da tabella risultati di Access le causali RITA e RIT1
//		RITA n° di eventi di ritardo con ingresso oltre 1H del normale
//		RIT1 n° di eventi di ritardo con ingresso oltre 1H del normale

//###################################
if uof_get_nr(fs_matricola,fdt_data_inizio,fdt_data_fine,fi_NR,fs_errore) < 0 then
	//in fs_errore il messaggio
	
	return -1
end if
//###################################

//leggi CdC di default dal database presenze: la matricola però deve esistere nel database presenze
if uof_get_cdc_dipendente_presenze(fs_matricola, ls_CdC, fs_errore) < 0 then
	//in fs_errore il messaggio
	
	return -1
end if

//leggi via_area, e i vari ipia per il CdC e per l'anno di riferimento
ll_anno_rif = year(date(fdt_data_inizio))

select via_area, ipia_1, ipia_2, ipia_3
into :fd_via_area, :ld_ipia_1, :ld_ipia_2, :ld_ipia_3
from premi_aree
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_riferimento = :ll_anno_rif and
		cod_centro_costo = :ls_CdC
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura VIA_area e IPIA per dipendente "+fs_matricola +"-"+ fs_nome_cognome+": "+sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_errore = "Dato non trovato lettura VIA_area e IPIA da tabella premi_aree per CdC "+ls_CdC +", anno rif."+ string(ll_anno_rif)
	uof_add_log(is_prefisso_errore+fs_errore)
	
	return -1
end if

choose case fi_NR
	case 0
		//nessun ingresso NON oltre 1 ora del normale orario di inizio del lavoro
		fd_ipia = ld_ipia_3
		
	case 1
		//n°1 ingresso NON oltre 1 ora del normale orario di inizio del lavoro
		fd_ipia = ld_ipia_2
		
	case else
		//più di un ingresso NON oltre 1 ora del normale orario di inizio del lavoro
		//oppure anche un solo ingresso OLTRE 1 ora del normale orario di inizio del lavoro
		fd_ipia = ld_ipia_1
		
end choose

//Chiesto da Daniele 11/07//2011
//se nel mese di calcolo non ci sono ore ORDINARIE non imputare il premio
//estrapola le ore ordinarie effettive lavorate ------------------------------------------------------------------------
//(in realtà la funzione estrapola pure le ore straordinarie e flessibili, ma in questo contesto non servono)
if uof_risultati_matricola_data(fs_matricola,fdt_data_inizio,fdt_data_fine,&
										ld_hofp,ld_ore_straord,ld_ore_flessibilita,fs_errore) < 0 then
	//in fs_errore il messaggio
	
	return -1
end if

if isnull(ld_hofp) or ld_hofp<=0 then
	uof_add_log("	Non risultano ore lavorative ordinarie: imposto premio nullo!")
	fi_NR = 990
	fd_ipia = 0
	fd_via = 0
	
	fd_ipia = round(fd_ipia, 4)
	fd_via = round(fd_via, 4)
	
	uof_add_log("	NR = "+string(fi_NR) + "  **** NOTA: non risultano presenze ORDINARIE nel mese!!")
else
	uof_add_log("	NR = "+string(fi_NR))
end if
//-------------------------------------------------------------------------------------------------------------------

if isnull(fd_ipia) then fd_ipia = 0
fd_ipia = round(fd_ipia, 4)


uof_add_log("	IPIA = "+string(fd_ipia))
uof_add_log("	VIA_area ("+ls_CdC+") = "+string(fd_via_area))
//--------------------------------------------------------------------------------------------------

//Calcola VIA ---------------------------------------------------------------------------------------
//NOTA: se il mese in cui calcolare non è tra queeli di erogazione si pone VIA=0
select 	flag_gen_via,flag_feb_via,flag_mar_via,flag_apr_via,flag_mag_via,flag_giu_via,
   		flag_lug_via,flag_ago_via,flag_set_via,flag_ott_via,flag_nov_via,flag_dic_via
into		:ls_flag_via[1],:ls_flag_via[2],:ls_flag_via[3],:ls_flag_via[4],:ls_flag_via[5],:ls_flag_via[6],
			:ls_flag_via[7],:ls_flag_via[8],:ls_flag_via[9],:ls_flag_via[10],:ls_flag_via[11],:ls_flag_via[12]
from premi
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_riferimento=:ll_anno_rif
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura flag erogazione VIA anno "+string(ll_anno_rif)+": "+sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_errore = "Dato non trovato in lettura flag erogazione VIA anno "+string(ll_anno_rif)
	uof_add_log(is_prefisso_errore+fs_errore)
	
	return -1
end if

ll_mese_rif = month(date(fdt_data_inizio))
ls_flag_eroga_via = ls_flag_via[ll_mese_rif]

if ls_flag_eroga_via="S" then
	//si tratta di un mese in cui si eroga il VIA
	fd_via = fd_ipia * fd_via_area
	if isnull(fd_via) then fd_via = 0
	
else
	//non si eroga il VIA
	fd_via = 0
	uof_add_log("nel mese n° "+string(ll_mese_rif)+" non si eroga il VIA, che viene quindi posto a ZERO!")
end if

fd_via = round(fd_via, 4)

uof_add_log("	VIA = "+string(fd_via))

uof_add_log("------------------------------------------------------------------------------------------------------------------")
uof_add_log("FINE CALCOLO IPIA e VIA ad-personam Dipendente: "+fs_matricola +"-"+ fs_nome_cognome+" dal "+string(fdt_data_inizio,"dd/mm/yyyy")+" al "+string(fdt_data_fine,"dd/mm/yyyy"))
uof_add_log("------------------------------------------------------------------------------------------------------------------")

return 1
end function

public function integer uof_get_impp_from_isfa (decimal fd_isfa, ref decimal fd_impp, ref string fs_errore);decimal ld_max_isfa, ld_min_isfa, ld_temp

//leggo i limiti superiori ed inferiori ----------------------------------
select max(isfa)
into :ld_max_isfa
from premi_impp_isfa
where cod_azienda=:s_cs_xx.cod_azienda
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore il lettura max isfa da premi_impp_isfa "+sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode=100 then
	fs_errore = "Dato non trovato in lettura max isfa da premi_impp_isfa "
	return -1
end if


select min(isfa)
into :ld_min_isfa
from premi_impp_isfa
where cod_azienda=:s_cs_xx.cod_azienda
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore il lettura min isfa da premi_impp_isfa "+sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode=100 then
	fs_errore = "Dato non trovato in lettura min isfa da premi_impp_isfa "
	return -1
end if

//-------------------------------------------------------------------------

select max(isfa)
into :ld_temp
from premi_impp_isfa
where cod_azienda=:s_cs_xx.cod_azienda and
		isfa <= :fd_isfa
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore il lettura posizionamento isfa da premi_impp_isfa "+sqlca.sqlerrtext
	return -1
end if

if isnull(ld_temp) or sqlca.sqlcode=100 or ld_temp=0 then
	//azzeramento IMPP
	fd_impp = 0
else
	//leggo il corrispondente IMPP
	select impp
	into :fd_impp
	from premi_impp_isfa
	where cod_azienda=:s_cs_xx.cod_azienda and
			isfa = :ld_temp
	using sqlca;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore il lettura impp da isfa in premi_impp_isfa "+sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(fd_impp) then fd_impp = 0
	
end if

return 1
end function

public function integer uof_get_cdc_noripartizione (ref string fs_sql_uff_mag_aut[], ref string fs_sql_uff_mag_aut_descriz[], ref string fs_errore);string ls_sql
datastore lds_data
long ll_index

ls_sql = "select cod_centro_costo,des_centro_costo "+&
			"from tab_centri_costo "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"flag_ripartisci_importi<>'S' "

//crea i datastore sulla transazione
if not uof_crea_ds_tran(lds_data,ls_sql,sqlca,fs_errore) then
	//in fs_msg il messaggio di errore
	return -1
end if

lds_data.retrieve()

for ll_index=1 to lds_data.rowcount()
	fs_sql_uff_mag_aut[ll_index] = lds_data.getitemstring(ll_index, 1)
	fs_sql_uff_mag_aut_descriz[ll_index] = lds_data.getitemstring(ll_index, 2)
next

return 1
end function

public function integer uof_get_nr (string fs_matricola, datetime fdt_data_dal, datetime fdt_data_al, ref integer fl_nr, ref string fs_errore);//estrae le ore lavorate da un centro di costo in un dato periodo dal database delle presenze
//dalla tabella dei risultati
//non tiene conto delle variazioni risp. ai CdC di default dei dipendenti

string ls_sql_rita
datastore lds_data_rita
long ll_tot_rita, ll_index, ll_lettura, ll_counter, ll_secondi
decimal ld_rita

//recupero le varie tipologie di causali
if uof_get_causali(fs_errore) < 0 then
	//in fs_msg il messaggio di errore
	return -1
end if

//N.B si tratta di valori in secondi (per le ore dividere per 3600)
//imposto lo statement sql di base
ls_sql_rita = 	"select ore, data "+&
					"from risultati "+&
					"where 	azienda='"+is_cod_azienda+"' and "+&
								"matricola='"+fs_matricola+"' and "+&
								"data>="+uof_date_format_msaccess(fdt_data_dal)+" and "+&
								"data<="+uof_date_format_msaccess(fdt_data_al)+" and "+&
								"causale in "+is_sql_ritardo+" "

//connettiti al database delle timbrature, creando la transazione
if uof_connetti_presenze(fs_errore)<0 then
	//in fs_errore il messaggio di errore
	
	return -1
end if

//crea i datastore sulla transazione
if not uof_crea_ds_tran(lds_data_rita,ls_sql_rita,itran_presenze,fs_errore) then
	//in fs_msg il messaggio di errore
	return -1
end if

ll_tot_rita = lds_data_rita.retrieve()

//disconnettiti dal database delle timbrature, distruggendo la transazione
uof_disconnetti_presenze()

fl_nr = 0

if ll_tot_rita > 0 then
else
	//fl_nr già impostato a 0
	return 1
end if

for ll_index=1 to ll_tot_rita
	ll_secondi = lds_data_rita.getitemnumber(ll_index, 1)
	
	if ll_secondi> 0 then
		//hai fatto ritardo, vediamo di quanto ...
		
		ld_rita = ll_secondi / 3600		//se maggiore di 1 ora allora segna già 999
		
		if ld_rita > 1 then
			fl_nr = 999
			destroy lds_data_rita;
			return 1
		end if
		
		//se arrivi qui vuol dire che si tratta di un ritardo sotto 1 ora, registralo in fl_nr
		fl_nr += 1
	end if
	
next


return 1
end function

public function integer uof_risultati_cdc_data (string fs_cod_centro_costo, datetime fdt_data_dal, datetime fdt_data_al, ref decimal fd_ore_ordinarie, ref decimal fd_ore_straordinarie, ref decimal fd_ore_flessibilita, ref decimal fd_ore_diaria, ref string fs_errore);//estrae le ore lavorate da un centro di costo in un dato periodo dal database delle presenze
//dalla tabella dei risultati
//non tiene conto delle variazioni risp. ai CdC di default dei dipendenti

string ls_sql_base, ls_sql_ord, ls_sql_flex, ls_sql_straord, ls_sql_diaria, ls_flag_considera_timbrature, ls_escludi_rp
datastore lds_data_ord, lds_data_straord, lds_data_flex, lds_diaria
long ll_tot_ord, ll_tot_straord, ll_tot_flex, ll_tot_diaria, ll_index, ll_lettura, ll_counter, ll_secondi
time ltm_in[], ltm_out[]

ls_escludi_rp = " and gruppo not in ("+is_escludi_da_rp+") "

//recupero le varie tipologie di causali
if uof_get_causali(fs_errore) < 0 then
	//in fs_msg il messaggio di errore
	return -1
end if

//N.B si tratta di valori in secondi (per le ore dividere per 3600)
//imposto lo statement sql di base
ls_sql_base = 	"select sum(ore) "+&
					"from risultati "+&
					"where 	azienda='"+is_cod_azienda+"' and "+&
								"matricola in (select matricola "+&
													"from dipen "+&
													"where azienda='"+is_cod_azienda+"' and "+&
															"cenco='"+fs_cod_centro_costo+"' "+ls_escludi_rp+") and "+&
								"data>="+uof_date_format_msaccess(fdt_data_dal)+" and "+&
								"data<="+uof_date_format_msaccess(fdt_data_al)+" "

//per le ore ORDINARIE ------------------------------------------------------------------------------
ls_sql_ord = ls_sql_base + "and causale in "+is_sql_ordinarie+" "


//per le ore STRAORDINARIE --------------------------------------------------------------------------
ls_sql_straord = ls_sql_base + "and causale in "+is_sql_straordinarie+" "

//per gli straordinari non conteggiare quelle per le matricole che hanno in anagrafica il campo ca9='SI' (straord. forfettari)
ls_sql_straord += " and matricola in (select matricola "+&
													"from dipen "+&
													"where azienda='"+is_cod_azienda+"' and "+&
															"ca9<>'SI') "


//per le ore FLESSIBILITA ---------------------------------------------------------------------------
ls_sql_flex = ls_sql_base + "and causale in "+is_sql_flessibilita+" "

//per le ore DIARIA ---------------------------------------------------------------------------
ls_sql_diaria = ls_sql_base + "and causale in "+is_sql_diaria+" "


//connettiti al database delle timbrature, creando la transazione
if uof_connetti_presenze(fs_errore)<0 then
	//in fs_errore il messaggio di errore
	
	return -1
end if

//crea i datastore sulla transazione
if not uof_crea_ds_tran(lds_data_ord,ls_sql_ord,itran_presenze,fs_errore) then
	//in fs_msg il messaggio di errore
	return -1
end if
if not uof_crea_ds_tran(lds_data_straord,ls_sql_straord,itran_presenze,fs_errore) then
	//in fs_msg il messaggio di errore
	return -1
end if
if not uof_crea_ds_tran(lds_data_flex,ls_sql_flex,itran_presenze,fs_errore) then
	//in fs_msg il messaggio di errore
	return -1
end if
if not uof_crea_ds_tran(lds_diaria,ls_sql_diaria,itran_presenze,fs_errore) then
	//in fs_msg il messaggio di errore
	return -1
end if

ll_tot_ord = lds_data_ord.retrieve()
ll_tot_straord = lds_data_straord.retrieve()
ll_tot_flex = lds_data_flex.retrieve()
ll_tot_diaria = lds_diaria.retrieve()

//disconnettiti dal database delle timbrature, distruggendo la transazione
uof_disconnetti_presenze()

//elabora i datastore (ci dovrebbe essere una sola riga su ogni datatstore)
if ll_tot_ord > 0 then fd_ore_ordinarie = lds_data_ord.getitemnumber(1, 1)
if ll_tot_straord > 0 then fd_ore_straordinarie = lds_data_straord.getitemnumber(1, 1)
if ll_tot_flex > 0 then fd_ore_flessibilita = lds_data_flex.getitemnumber(1, 1)
if ll_tot_diaria > 0 then fd_ore_diaria = lds_diaria.getitemnumber(1, 1)

//distruggi i datastore
destroy lds_data_ord;
destroy lds_data_straord;
destroy lds_data_flex;
destroy lds_diaria;


//considera fd_ore_diaria solo nei casi in cui è espressamente indivato in tab_centri_costo (flag_considera_timbrature='S')
//nota bene: nel database del cliente la label è stata rinominata "Diaria"
select flag_considera_timbrature
into :ls_flag_considera_timbrature
from tab_centri_costo
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_centro_costo=:fs_cod_centro_costo
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura flag si/no Diaria da tab_centri_costo per codice "+fs_cod_centro_costo+" - "+sqlca.sqlerrtext
	return -1
end if

if ls_flag_considera_timbrature="S" then
else
	//reset ore diaria
	fd_ore_diaria = 0
end if

if isnull(fd_ore_ordinarie) then fd_ore_ordinarie = 0
if isnull(fd_ore_straordinarie) then fd_ore_straordinarie = 0
if isnull(fd_ore_flessibilita) then fd_ore_flessibilita = 0
if isnull(fd_ore_diaria) then fd_ore_diaria = 0

//per ottenere le ore divido per 3600
fd_ore_ordinarie = round(fd_ore_ordinarie / 3600 , 4)
fd_ore_straordinarie = round(fd_ore_straordinarie / 3600 , 4)
fd_ore_flessibilita = round(fd_ore_flessibilita / 3600 , 4)
fd_ore_diaria = round(fd_ore_diaria / 3600 , 4)

//Daniele e Beatrice, chiesto il 16/06/2011: la diaria va moltiplicata per un fattore 3
fd_ore_diaria = fd_ore_diaria * 3

if ii_verbose = 2 then
	uof_add_log("Ore Ordinarie CdC "+fs_cod_centro_costo+" dal "+&
					string(fdt_data_dal, "dd/mm/yyyy")+" al "+string(fdt_data_al, "dd/mm/yyyy")+" :"+string(fd_ore_ordinarie, "###,###,##0.00"))
	uof_add_log("Ore Flex+Straord+Diaria. CdC "+fs_cod_centro_costo+" dal "+&
					string(fdt_data_dal, "dd/mm/yyyy")+" al "+string(fdt_data_al, "dd/mm/yyyy")+&
					" :"+string(fd_ore_flessibilita, "###,###,##0.00")+" + "+&
					string(fd_ore_straordinarie, "###,###,##0.00")+" + "+&
					string(fd_ore_diaria, "###,###,##0.00")+" = "+&
					string(fd_ore_flessibilita + fd_ore_straordinarie + fd_ore_diaria, "###,###,##0.00"))
end if

return 1
end function

public function integer uof_risultati_cdc_data (datetime fdt_data_dal, datetime fdt_data_al, ref string fs_cdc[], ref decimal fd_ore_ordinarie[], ref decimal fd_ore_straordinarie[], ref decimal fd_ore_flessibilita[], ref decimal fd_ore_diaria[], ref string fs_errore);//Popola tutti i centri di costo possibili con le relative ore (ord, straord, flex) in un dato periodo
//estrapola questi dati dal database delle presenze

string ls_cdc_des_v[]
long ll_index
decimal ld_ore_ord, ld_ore_straord, ld_ore_flex, ld_ore_diaria

//prelevo tutti i CdC dal database delle presenze
if uof_get_cdc_presenze(fs_cdc[],ls_cdc_des_v[],fs_errore) < 0 then
	//in fs_errore il messaggio
	return -1
end if

for ll_index=1 to upperbound(fs_cdc[])
	
	//azzero variabili
	ld_ore_ord=0
	ld_ore_straord=0
	ld_ore_flex=0
	
	if uof_risultati_cdc_data(fs_cdc[ll_index],fdt_data_dal,fdt_data_al,ld_ore_ord,ld_ore_straord,ld_ore_flex,ld_ore_diaria,fs_errore) < 0 then
		//in fs_errore il messaggio
		return -1
	end if
	
	fd_ore_ordinarie[ll_index] = ld_ore_ord
	fd_ore_straordinarie[ll_index] = ld_ore_straord
	fd_ore_flessibilita[ll_index] = ld_ore_flex
	fd_ore_diaria[ll_index] = ld_ore_diaria
	
next


return 1
end function

public function integer uof_fatbol_cdc_sfuso_old (datetime fdt_data_dal, datetime fdt_data_al, string fs_tipo, ref string fs_cdc[], ref decimal fd_importi[], ref decimal fd_totale, ref string fs_errore);//estrae il bollettato/fatturato (dipende dalla variabile fs_tipo B/F)
//considera i prodotti con 2nda cifra='0'
//ci si riferisce ai reparti con percentuali ripartire in base alla tabella premi_prodotti_reparti

string 		ls_sql, ls_cifra_tenda, ls_nome_tabella_tes, ls_nome_tabella_det, ls_colonna_data, ls_cod_prodotto, ls_sql_regole, &
				ls_regola, ls_cod_reparto_1, ls_cod_reparto_2, ls_sintassi_regole, ls_err, ls_cod_reparto, ls_colonna_prog_riga, ls_sql_count,&
				ls_colonna_tipo_doc, ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven
decimal 		ld_imponibile_iva, ld_perc_rep_1, ld_perc_rep_2
datastore	lds_regole
long			ll_tot_regole, ll_anno_reg, ll_num_reg, ll_prog_riga, ll_count,ll_index

//--------------------------------------------------------------------------
//preparo un cursore con le regole da cui leggere la percentuale di suddivisione
//del fatturato/bollettato per reparti
//ls_sql_regole = 	"select regola, cod_reparto_1, perc_rep_1, cod_reparto_2, perc_rep_2 "+&
//						"from premi_prodotti_reparti "+&
//						"order by posizione "
ls_sql_regole = 	"select regola, cod_centro_costo_1, perc_rep_1, cod_centro_costo_2, perc_rep_2 "+&
						"from premi_prodotti_cdc "+&
						"order by posizione "

//creazione datastore dinamico per le regole
ls_sintassi_regole = sqlca.syntaxfromsql(ls_sql_regole, "style(type=grid)", ls_err)
if Len(ls_err) > 0 then
	fs_errore = "Errore durante la creazione della sintassi del datastore regole ripartizione CdC:" + ls_err + "~r~n"+ls_sql_regole
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
end if

lds_regole = create datastore

lds_regole.Create(ls_sintassi_regole, ls_err)
if len(ls_err) > 0 then
	fs_errore = "Errore durante la creazione del datastore regole ripartizione CdC:" + ls_err
	uof_add_log(is_prefisso_errore+fs_errore)
	destroy lds_regole;
	return -1
end if

lds_regole.settransobject(sqlca)

ll_tot_regole = lds_regole.retrieve()
if ll_tot_regole < 0 then
	fs_errore = "Errore durante la retrieve delle righe del datastore ripartiz.CdC:" + sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	destroy lds_regole;
	return -1
end if

if ll_tot_regole=0 then
	fs_errore = "Tabella di ripartizione fatturato/bollettato in base al prodotto/CdC non impostata! Calcolo non effettuato!"
	uof_add_log(is_prefisso_errore+fs_errore)
	destroy lds_regole;
	return 0
end if

//adesso in lds_regole, ordinate per priorità ci sono le regole per filtrare il cod prodotto
//e stabilire le percentuali di imponibile per CdC
//es.   	'%Y' 		'cdc1' 75% 	'cdc2' 	25%
//			'A09%'	'cdc1' 60%	'cdc2'	40%	ecc...
//------------------------------------------------------------------------------


ls_cifra_tenda = "0"

if fs_tipo= "B" then
	//bollettato
	ls_nome_tabella_tes = "tes_bol_ven"
	ls_nome_tabella_det = "det_bol_ven"
	ls_colonna_data = "data_bolla"
	ls_colonna_prog_riga = "prog_riga_bol_ven"
	ls_colonna_tipo_doc = "'' as cod_tipo_documento "
else
	//fatturato
	ls_nome_tabella_tes = "tes_fat_ven"
	ls_nome_tabella_det = "det_fat_ven"
	ls_colonna_data = "data_fattura"
	ls_colonna_prog_riga = "prog_riga_fat_ven"
	ls_colonna_tipo_doc = "t.cod_tipo_fat_ven as cod_tipo_documento "
end if

//select count righe ##############################################################################################
ll_count = 0

ls_sql_count = "select count(*) "+&
					"from "+ls_nome_tabella_det+" as a "+&
					"join "+ls_nome_tabella_tes+" as t on t.cod_azienda=a.cod_azienda and "+&
																"t.anno_registrazione=a.anno_registrazione and "+&
																"t.num_registrazione=a.num_registrazione "+&
					"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"a.cod_prodotto is not null and "+&
							"substring(a.cod_prodotto, 2, 1)='"+ls_cifra_tenda+"' and "+&
							"t."+ls_colonna_data+" is not null and "+&
							"t."+ls_colonna_data+">='"+string(fdt_data_dal, "yyyymmdd")+"' and "+&
							"t."+ls_colonna_data+"<='"+string(fdt_data_al, "yyyymmdd")+"' "

//creazione cursore dinamico conteggio righe da elaborare
declare cu_fatbolcount dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql_count;
open dynamic cu_fatbolcount;

if sqlca.sqlcode <> 0 then 
	fs_errore = "Errore nella open del cursore bol/fat count non sfuso. Dettaglio = " + sqlca.sqlerrtext
	
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
end if

//una sola riga
fetch cu_fatbolcount into 
		 :ll_count;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore nella fetch del cursore bol/fat count non sfuso. Dettaglio = " + sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	
	close cu_fatbolcount;
	return -1
	
end if	

close cu_fatbolcount;

//in ll_count il numero di righe
ll_index = 0

//#################################################################################################################


//impostazione sql: righe del bollettato/fatturato
//riga per riga sarà poi elaborata per imputarne la % per reparto ...
ls_sql = "select a.anno_registrazione, a.num_registrazione,a."+ls_colonna_prog_riga+","+&
					 "a.cod_prodotto, isnull(a.imponibile_iva,0) as importo,"+ls_colonna_tipo_doc+" "+&
			"from "+ls_nome_tabella_det+" as a "+&
			"join "+ls_nome_tabella_tes+" as t on t.cod_azienda=a.cod_azienda and "+&
														"t.anno_registrazione=a.anno_registrazione and "+&
														"t.num_registrazione=a.num_registrazione "+&
			"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"a.cod_prodotto is not null and "+&
					"substring(a.cod_prodotto, 2, 1)='"+ls_cifra_tenda+"' and "+&
					"t."+ls_colonna_data+" is not null and "+&
					"t."+ls_colonna_data+">='"+string(fdt_data_dal, "yyyymmdd")+"' and "+&
					"t."+ls_colonna_data+"<='"+string(fdt_data_al, "yyyymmdd")+"' "

//creazione cursore dinamico da elaborare
declare cu_fatbol dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_fatbol;

if sqlca.sqlcode <> 0 then 
	fs_errore = "Errore nella open del cursore bol/fat non sfuso. Dettaglio = " + sqlca.sqlerrtext
	
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
end if


do while true
	fetch cu_fatbol into 
			 :ll_anno_reg,:ll_num_reg,:ll_prog_riga,:ls_cod_prodotto,:ld_imponibile_iva,:ls_cod_tipo_fat_ven;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore nella fetch del cursore bol/fat non sfuso. Dettaglio = " + sqlca.sqlerrtext
		uof_add_log(is_prefisso_errore+fs_errore)
		
		close cu_fatbol;
		return -1
		
	elseif sqlca.sqlcode = 100 then
		exit
	end if	
	
	//prosegui
	Yield()
	ll_index += 1
	
	if fs_tipo= "F" then
		//se fattura con nota di credito cambia il segno in negativo
		
		select flag_tipo_fat_ven
		into :ls_flag_tipo_fat_ven
		from tab_tipi_fat_ven
		where cod_azienda=:s_cs_xx.cod_azienda and
				cod_tipo_fat_ven=:ls_cod_tipo_fat_ven
		using sqlca;
		
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in lettura flag_tipo_fat_ven" + sqlca.sqlerrtext
			uof_add_log(is_prefisso_errore+fs_errore)
			
			close cu_fatbol;
			return -1
		end if
		
		if ls_flag_tipo_fat_ven="N" then
			//nota di credito
			ld_imponibile_iva = ld_imponibile_iva * (-1)
		end if
		
	end if
	
	//in questa variabile metto il totale imponibile, a prescindere dalla suddivisione percentuale dei reparti
	fd_totale += ld_imponibile_iva
	
	if isvalid(w_stato_elaborazione) then &
			w_stato_elaborazione.st_log.text = "Elab.righe Mod. dal "+string(fdt_data_dal, "dd/mm/yyyy")+&
					" al "+string(fdt_data_al, "dd/mm/yyyy")+"~r~n~r~n"+ &
					string(ll_anno_reg)+"/"+string(ll_num_reg)+"/"+string(ll_prog_riga)+" ("+string(ll_index)+" di "+string(ll_count)+")"
	
	//decidi le percentuali e aggiorna i reparti e imponibili
	uof_fatbol_cdc_percent(lds_regole,ll_anno_reg,ll_num_reg,ll_prog_riga,fs_tipo,ls_cod_prodotto,ld_imponibile_iva,fs_cdc[],fd_importi[])
	
loop

uof_add_log("Uscito dal cursore FLrep per prodotti non confezionati")

close cu_fatbol;
destroy lds_regole;

return 1
end function

public function integer uof_get_vpr (string fs_matricola, ref decimal fd_vpr, ref string fs_errore);string ls_test

if uof_connetti_presenze(fs_errore) < 0 then
	//in fs_errore il messaggio di errore
	return -1
end if

select valore
into :ls_test
from dipen_attributi
where azienda=:is_cod_azienda and
		matricola=:fs_matricola and attributo = 1
using itran_presenze;

if itran_presenze.sqlcode<0 then
	fs_errore = "Errore in lettura attributo VPR da dipen_attributi per "+fs_matricola +": "+itran_presenze.sqlerrtext
	
	//disconnessione
	uof_disconnetti_presenze()
	
	uof_add_log(is_prefisso_errore+fs_errore)
	
	return -1
	
elseif itran_presenze.sqlcode=100 then
	fs_errore = "Dato non trovato lettura attributo VPR da dipen_attributi per matricola "+fs_matricola
	
	//disconnessione
	uof_disconnetti_presenze()
	
	uof_add_log(is_prefisso_errore+fs_errore)
	fd_vpr = 0
	return 0
end if

//disconnessione
uof_disconnetti_presenze()
 
if isnumber(ls_test) then
	fd_vpr = dec(ls_test)
end if

if fd_vpr > 0 then
	uof_add_log("	VPR letto da dipen_attributi = "+ string(fd_vpr, "###,##0,00"))
	
else
	fs_errore = "VPR null in dipen_attributi per matricola "+fs_matricola
	uof_add_log(is_prefisso_errore+fs_errore)
	fd_vpr = 0
	return 0
end if

return 1
end function

public function integer uof_risultati_matricola_data_v (string fs_matricola, datetime fdt_data_dal, datetime fdt_data_al, ref decimal fd_fd_hofp[], ref string fs_errore);//estrae le ore lavorate da un dipendente in un dato periodo dal database delle presenze
//dalla tabella dei risultati

string ls_sql_ord
datastore lds_data_ord
long ll_tot_ord, ll_index, ll_i, ll_mese_1, ll_mese_2, ll_anno_rif
decimal ld_ore_ordinarie
datetime ldt_v[], ldt_1,ldt_2

//recupero le varie tipologie di causali
if uof_get_causali(fs_errore) < 0 then
	//in fs_msg il messaggio di errore
	return -1
end if


ll_mese_1 = month(date(fdt_data_dal))
ll_mese_2 = month(date(fdt_data_al))
ll_anno_rif = year(date(fdt_data_dal))

ll_i = 0
for ll_index = ll_mese_1 to ll_mese_2
	ll_i += 1
	uof_get_inizio_fine_mese(ll_index,ll_anno_rif,ldt_1,ldt_2,ldt_v[])

	//imposto lo statement sql di base (N.B. il dato sum(ore) rappresenta in realtà il numero di secondi)
	ls_sql_ord = 	"select sum(ore) "+&
						"from risultati "+&
						"where 	azienda='"+is_cod_azienda+"' and "+&
									"matricola='"+fs_matricola+"' and "+&
									"data>="+uof_date_format_msaccess(ldt_1)+" and "+&
									"data<="+uof_date_format_msaccess(ldt_2)+" and "+&
									"causale in "+is_sql_ordinarie+" "
	
	//connettiti al database delle timbrature, creando la transazione
	if uof_connetti_presenze(fs_errore)<0 then
		//in fs_errore il messaggio di errore
		
		return -1
	end if
	
	//crea i datastore sulla transazione
	if not uof_crea_ds_tran(lds_data_ord,ls_sql_ord,itran_presenze,fs_errore) then
		//in fs_msg il messaggio di errore
		return -1
	end if
	
	ll_tot_ord = lds_data_ord.retrieve()
	
	//disconnettiti dal database delle timbrature, distruggendo la transazione
	uof_disconnetti_presenze()

	//elabora i datastore (ci dovrebbe essere una sola riga su ogni datastore)
	if ll_tot_ord > 0 then ld_ore_ordinarie = lds_data_ord.getitemnumber(1, 1)
	
	//distruggi i datastore
	destroy lds_data_ord;
	
	if isnull(ld_ore_ordinarie) then ld_ore_ordinarie = 0

	//converti in ore da secondi
	ld_ore_ordinarie = round(ld_ore_ordinarie / 3600 , 4)
	
	if ii_verbose = 2 then
		uof_add_log("Ore Ordinarie matr."+fs_matricola+" dal "&
						+string(fdt_data_dal, "dd/mm/yyyy")+" al "+string(ldt_2, "dd/mm/yyyy")+" :"&
						+string(ld_ore_ordinarie, "###,###,##0.0000"))
	end if
	
	fd_fd_hofp[ll_i] = ld_ore_ordinarie
next

return 1
end function

public function integer uof_get_hopp_hofp_iapp (string fs_matricola, string fs_cdc, datetime fdt_data_inizio, datetime fdt_data_fine, ref decimal fd_hopp, ref decimal fd_hofp, ref decimal fd_iapp, ref string fs_errore);//estrapola dal database delle timbrature le ore di lavoro ordinarie effettive HOFP
//e valuta il rapporto con quelle previste (HOPP) come (indice presenza ad personam) IAPP = HOFP / HOPP


decimal ld_ore_straord, ld_ore_flessibilita, ld_hopp[], ld_hofp[]
string ls_matricola, ls_dipendente
long ll_matricola, ll_anno_rif, ll_index
integer li_mesi[]
string ls_flag_IAPP[]


fd_hopp = 0
fd_hofp = 0
fd_iapp = 0
ll_anno_rif = year(date(fdt_data_inizio))

//HOPP=ore ordinarie previste
//Con Daniele e Beatrice abbiamo deciso che normalmente ogni lavoratore fa 8 ore al giorno
//nel mese quindi vanno tolti i sabato e domenica ed eventuali festività comandate (es. 2 giugno, ...)
//che vanno codificate a cura del cliente nella tabella di APICE tab_cal_ferie (w_tab_cal_ferie)
//a meno che nella tabella dipen del database ACCESS viene scritto un valore in un opportuno campo

//valuto il numero di giorni lavorabili nel periodo fdt_data_inizio e fdt_data_fine
//valuto il numero di ore lavorabili per giorno (8 o qualcosa di diverso)
//valuto fp_hopp
if uof_get_hopp(fs_matricola, fdt_data_inizio,fdt_data_fine, ld_hopp[], li_mesi[], fs_errore) < 0 then
	//in fs_errore il messaggio
	
	return -1
end if
//nella variabile REF fd_hopp ci sono ora le ore ordinarie previste HOPP ----------------------------------------------

//estrapola le ore ordinarie effettive lavorate ------------------------------------------------------------------------
if uof_risultati_matricola_data_v(fs_matricola,fdt_data_inizio,fdt_data_fine,&
										ld_hofp[],fs_errore) < 0 then
	//in fs_errore il messaggio
	
	return -1
end if


select flag_1,flag_2,flag_3,flag_4,flag_5,flag_6,flag_7,flag_8,flag_9,flag_10,flag_11,flag_12
into 	:ls_flag_IAPP[1],:ls_flag_IAPP[2],:ls_flag_IAPP[3],:ls_flag_IAPP[4],:ls_flag_IAPP[5],:ls_flag_IAPP[6],
		:ls_flag_IAPP[7],:ls_flag_IAPP[8],:ls_flag_IAPP[9],:ls_flag_IAPP[10],:ls_flag_IAPP[11],:ls_flag_IAPP[12]
from premi_aree
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_riferimento=:ll_anno_rif and
   	cod_centro_costo=:fs_cdc
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura flags IAPP da tabella premi_aree per CdC "+fs_cdc +", anno rif."+ string(ll_anno_rif) +": "+sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_errore = "Dato non trovato lettura flags IAPP da tabella premi_aree per CdC "+fs_cdc +", anno rif."+ string(ll_anno_rif)
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
end if

fd_hopp = 0
fd_hofp = 0
for ll_index=1 to upperbound(ld_hofp[])
	fd_hopp += ld_hopp[ll_index]
	
	//verifico se siamo nei casi in cui IAPP viene forzato ad 1
	if ls_flag_IAPP[li_mesi[ll_index]] = "S" then
		//in questo mese poni IAPP=1, cioè poni HOFP del mese = HOPP del mese
		//cioè come se il dipendente avesse lavorato in questo mese un numero di ore uguale a quelle previste!!
		fd_hofp += ld_hopp[ll_index]
		uof_add_log("	IAPP mese n°"+string(li_mesi[ll_index])+"  FORZATO AD 1 come previsto in premi_aree!!!!!")
	else
		fd_hofp += ld_hofp[ll_index]
	end if
	
next
//nella variabile REF fd_hofp ci sono ora le ore effettive ordinarie HOFP ----------------------------------------------
//nella variabile REF fd_hopp ci sono ora le ore ordinarie previste HOPP ----------------------------------------------


//calcolo IAPP = HOFP / HOPP -------------------------------------------------------------------------------------------
if fd_hopp>0 then
	fd_iapp = round(fd_hofp / fd_hopp, 4)
else
	fs_errore="HOPP=0 (H ord. prev.), quindi imposto IAPP=0!"
	fd_iapp = 0
end if

if fd_hofp > 0 then
else
	if len(fs_errore)>0 then fs_errore+=" - "
	fs_errore="HOFP=0 (H ord. eff.), quindi anche IAPP=0!"
end if

return 1
end function

public function integer uof_get_hopp (string fs_matricola, datetime fdt_data_inizio, datetime fdt_data_fine, ref decimal fd_hopp[], ref integer fi_mesi[], ref string fs_errore);//estrapola dal DB access presenze le ore di lavoro ordinarie ad personam previste
//tiene conto anche del contenuto nella tabella festivita "tab_cal_ferie" (che deve però essere compilata dall'utente di APICE)
//esclude anche i giorni di sabato e domenica

//restituisce un array fd_hopp[] contenete HOPP per ogni mese del periodo fdt_data_inizio, fdt_data_fine

date ldt_data_1, ldt_data_2
long ll_prog_struttura, ll_count, ll_giorno_settimana, ll_giorni_lavorabili_periodo, ll_ore_lavorabili_gg
decimal ld_temp, ld_hopp
long ll_mese_1, ll_mese_2, ll_index, ll_i, ll_anno_rif
datetime ldt_1, ldt_2, ldt_v[]
string ls_temp


ll_mese_1 = month(date(fdt_data_inizio))
ll_mese_2 = month(date(fdt_data_fine))
ll_anno_rif = year(date(fdt_data_inizio))

//leggo dal db presenze le ORE lavorabili giornaliere ----------
if uof_connetti_presenze(fs_errore) < 0 then
	//in fs_errore il messaggio di errore
	return -1
end if

select ca6
into :ls_temp
from dipen
where azienda=:is_cod_azienda and 
		matricola=:fs_matricola
using itran_presenze;

if itran_presenze.sqlcode<0 then
	fs_errore = "Errore in lettura ore lavorabili per giorno della matricola "+fs_matricola+" "+itran_presenze.sqlerrtext
	uof_disconnetti_presenze()
	return -1
end if

uof_disconnetti_presenze()

if isnumber(ls_temp) then
	ll_ore_lavorabili_gg = long(ls_temp)
	uof_add_log("	ORE lavorabili giornaliere  = "+string(ll_ore_lavorabili_gg) + "  -> (** variante in dipen.ca6 **)")
else
	ll_ore_lavorabili_gg = 8
	uof_add_log("	ORE lavorabili giornaliere  = "+string(ll_ore_lavorabili_gg) + "  -> non trovato in dipen.ca6, imposto a 8 ore")
end if
//----------------------------------------------------------------

ll_i = 0
for ll_index = ll_mese_1 to ll_mese_2
	ll_i += 1
	fi_mesi[ll_i] = ll_index
	
	uof_get_inizio_fine_mese(ll_index,ll_anno_rif,ldt_1,ldt_2,ldt_v[])

	//inizializzo variabili
	ldt_data_1 = date(ldt_1)
	ldt_data_2 = date(ldt_2)
	ld_hopp = 0
	
	//calcola il numero totale di giorni nell'intervallo ldt_data_1 - ldt_data_2
	ll_giorni_lavorabili_periodo = daysafter(ldt_data_1, ldt_data_2) + 1
	
	//cicla le varie date da inizio a fine
	//eliminiamo i giorni del tipo sabato, domenica e le festività comandate
	do while ldt_data_1 <= ldt_data_2
		
		//verifica se il giorno in esame è presente come festività
		setnull(ll_count)
		
		select count(*)
		into :ll_count
		from tab_cal_ferie
		where cod_azienda=:s_cs_xx.cod_azienda and
				data_giorno=:ldt_data_1 and
				flag_tipo_giorno = 'F'
		using sqlca;
		
		if sqlca.sqlcode<0 then
			fs_errore = "Errore in lettura da tabella festività: "+sqlca.sqlerrtext
			uof_add_log(is_prefisso_errore+fs_errore)
			
			return -1
		end if
		
		if ll_count>0 then
			//presente nelle festività: decrementa
			
			ll_giorni_lavorabili_periodo -= 1
			
		else
			//non è una festività pre-impostata, verifica se sabato o domenica
			
			//leggi il numero del giorno della settimana (Domenica è 1, Sabato è 7)
			ll_giorno_settimana = daynumber(ldt_data_1)
			
			if ll_giorno_settimana=1 or ll_giorno_settimana=7 then
				//è un sabato o una domenica, decrementa
				
				ll_giorni_lavorabili_periodo -= 1
				
			end if
			
		end if
		
		//passa al giorno successivo
		ldt_data_1 = relativedate(ldt_data_1, 1)
	loop
	
	uof_add_log("	GG lavorabili nel periodo dal "+string(date(ldt_1),"dd/mm/yyyy")+" al "+ &
						+string(ldt_2,"dd/mm/yyyy")+"  = "+string(ll_giorni_lavorabili_periodo))
	
	
	//adesso che so il numero di giorni lavorabili nel periodo, 
	//moltiplico per il,fattore ore lavorabili giornalmente per la matricola (dipen.ca6)
	//se valorizzato con un numero allora considera questo valore, altrimenti fissa a 8
	
	
	ld_hopp = ll_giorni_lavorabili_periodo * ll_ore_lavorabili_gg
	fd_hopp[ll_i] = ld_hopp
	uof_add_log("	HOPP del mese n° "+string(ll_index)+"  = "+string(ld_hopp))
next

return 1
end function

public function integer uof_get_vpt (string fs_matricola, datetime fdt_inizio, datetime fdt_fine, ref string fs_nome_cognome, ref integer fi_p, ref decimal fd_vpr, ref integer fi_t, ref decimal fd_bpt, ref decimal fd_is_area, ref decimal fd_ix_area, ref decimal fd_vflp, ref decimal fd_vfla, ref decimal fd_impp, ref decimal fd_fl, ref decimal fd_ol, ref decimal fd_rpr_reparto, ref decimal fd_hofp, ref decimal fd_hopp, ref decimal fd_ip, ref decimal fd_vpt, ref string fs_errore);long ll_anno_rif, ll_ret, ll_prog_periodo, ll_mese_fine
string ls_CdC
decimal ld_isfa, ld_bpp, ld_rp, ld_iapp, ld_isfa_origine
integer li_ret

uof_add_log("------------------------------------------------------------------------------------------------------------------")
uof_add_log("INIZIO CALCOLO VPT ad-personam")

//leggi il nome e cognome del dipendente
ll_ret = uof_get_nomecognome_da_matricola(fs_matricola, fs_nome_cognome, fs_errore)
if ll_ret < 0 then
	// in fs_errore il messaggio 
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
	
elseif ll_ret = 100 then
	// in fs_errore il messaggio 
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
end if

//------------------------------------------------------
uof_add_log("	Dipendente matricola: "+fs_matricola +"-"+ fs_nome_cognome+ " dal "+string(fdt_inizio,"dd/mm/yyyy")+" al "+string(fdt_fine,"dd/mm/yyyy"))

//anno di riferimento e mese fine
ll_anno_rif = year(date(fdt_inizio))
ll_mese_fine = month(date(fdt_fine))

//leggi CdC di default ---------------------------------
if uof_get_cdc_dipendente_presenze(fs_matricola, ls_CdC, fs_errore) < 0 then
	//in fs_errore il messaggio
	return -1
end if


//leggiamo p -----------------------

select p
into :fi_p
from premi_aree
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_riferimento=:ll_anno_rif and
   	cod_centro_costo=:ls_CdC
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura P da tabella premi_aree per CdC "+ls_CdC +", anno rif."+ string(ll_anno_rif)+": "+sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_errore = "Dato non trovato lettura P da tabella premi_aree per CdC "+ls_CdC +", anno rif."+ string(ll_anno_rif)
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
end if
 
if isnull(fi_p) then fi_p = 0

//VPR lo leggi dal database access delle presenze
li_ret = uof_get_vpr(fs_matricola,fd_vpr,fs_errore)

if li_ret<0 then
	//in fs_errore il messaggio
	return -1
	
elseif li_ret=0 then
	//VPR non impostato in tabella dipen: VPR=0, ma non bloccare
	
end if

if isnull(fd_vpr) then fd_vpr = 0

fd_vpr = round(fd_vpr, 4)
ld_bpp = (fd_vpr * fi_p) / 100

uof_add_log("	P% = "+string(fi_p))
uof_add_log("	VPR = "+string(fd_vpr))
uof_add_log("	BPP = "+string(ld_bpp) + "		(VPR x P(%))")

//----------------------------------------------------------


//leggiamo t, vfla, IX ed IS da premi_periodo in base a dove cade la data fine 
//NOTA il progressivo mi servirà + sotto per leggere RPRrep da premi_periodo_aree
select progressivo, t, vfla, ix_area, is_area
into :ll_prog_periodo, :fi_t, :fd_vfla, :fd_ix_area, :fd_is_area
from premi_periodo
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_riferimento=:ll_anno_rif and
   	data_dal<=:fdt_fine and data_al>=:fdt_fine
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura T,VFLA,IXarea ed ISarea per dipendente "+fs_matricola +"-"+ fs_nome_cognome+": "+sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_errore = "Dato non trovato lettura T,VFLA,IXarea ed ISarea da tabella premi_periodo per data "+string(fdt_fine,"dd/mm/yyyy")
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
end if

if isnull(fi_t) then fi_t = 0
if isnull(fd_vfla) then fd_vfla = 0

fd_vfla = round(fd_vfla, 4)
uof_add_log("	T% = "+string(fi_t))
uof_add_log("	VFLA = "+string(fd_vfla))
//-------------------------------------------------------------

//calcolo il fatturato totale vflp fino a fine periodo "fdt_fine"
//è stato già calcolato una volta sola prima di chiamare la funzione,
//tanto vale uguale per tutti a parità di periodo
//quindi la variale passata BYREF "fd_vflp" contiene il nostro valore
fd_vflp = round(fd_vflp, 4)

uof_add_log("	VFLP = "+string(fd_vflp))
//-------------------------------------------------------------

//valuto ISFA come (VFLP/VFLA) * 100 ---------------------------
if fd_vfla>0 then
	ld_isfa = (fd_vflp / fd_vfla) * 100		//è quindi una %
else
	ld_isfa = 0
end if

//28/07/2011  Daniele dice di fare un arrotondamento ad un numero intero per ISFA
//es. 102.5  -> 103       102.4999  -> 102
ld_isfa_origine = ld_isfa
ld_isfa = round(ld_isfa, 0)
ld_isfa = round(ld_isfa, 4)


uof_add_log("	ISFA = "+string(ld_isfa,"##0.0000")+"		(VFLP/VFLA) x 100   (ISFA originario:"+string(ld_isfa_origine,"##0.0000")+")")
//-------------------------------------------------------------

/*
//18/06/2012 Integrazioni calcolo prenio produzione con gestione multi-stabilimento
//leggiamo VFSA (statistico) da premi_periodo_depositi in base al deposito del dipendente e a dove cade la data fine
select rpr_area
into :fd_rpr_reparto
from premi_periodo_aree
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_riferimento=:ll_anno_rif and
   	progressivo=:ll_prog_periodo and
		cod_centro_costo=:ls_CdC
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura RPRrep per il CdC "+ls_CdC+" e data "+string(fdt_fine,"dd/mm/yyyy")+sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_errore = "Dato non trovato lettura RPRrep da premi_periodo_aree per il CdC "+ls_CdC+" e data "+string(fdt_fine,"dd/mm/yyyy")
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
end if

if isnull(fd_rpr_reparto) then fd_rpr_reparto=0

fd_rpr_reparto = round(fd_rpr_reparto, 4)
uof_add_log("	RPRrep = "+string(fd_rpr_reparto) + "			(dato statistico)")
*/



//ricavo IMPP funzione di ISFA da premi_impp_isfa -------------
if uof_get_impp_from_isfa(ld_isfa, fd_impp, fs_errore) < 0 then
	//in fs_errore il messaggio
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
end if
fd_impp = round(fd_impp, 4)

uof_add_log("	IMPP = "+string(fd_impp)+"		funzione di ISFA")
//-------------------------------------------------------------


//calcolo BPT
fd_bpt = ((ld_bpp * fi_t) / 100) * fd_impp
fd_bpt = round(fd_bpt, 4)
uof_add_log("	BPT% = "+string(fd_bpt)+"		BPP x T% x IMPP = (VPR x P%) x T% x IMPP")


//leggo RPrep dalla tabella premi_rp_online (che ovviamente viene elaborata di notte ogni giorno)
//il valore va letto fino a data fine, quindi leggo i valori progressivi
select 	fl_progressivo,   
			ol_progressivo   
into		:fd_fl,   
         :fd_ol  
from premi_rp_online
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_centro_costo=:ls_CdC and
		month(data_giorno)=:ll_mese_fine
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura RP,FL e OL da tabella premi_rp_online per data "+sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_errore = "Dato non trovato in lettura RP,FL e OL da tabella premi_rp_online per data "+string(fdt_fine,"dd/mm/yyyy") + " e CdC "+ls_CdC
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
end if

fd_fl = round(fd_fl, 4)
fd_ol = round(fd_ol, 4)
uof_add_log("	FLrep = "+string(fd_fl))
uof_add_log("	OLrep = "+string(fd_ol))

if fd_ol=0 then
	ld_rp = 0
else
	ld_rp = fd_fl / fd_ol
end if

ld_rp = round(ld_rp, 4)
uof_add_log("	RPrep = "+string(ld_rp))


//leggiamo RPRrep (statistico) da premi_periodo_aree in base al CdC e a dove cade la data fine
select rpr_area
into :fd_rpr_reparto
from premi_periodo_aree
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_riferimento=:ll_anno_rif and
   	progressivo=:ll_prog_periodo and
		cod_centro_costo=:ls_CdC
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura RPRrep per il CdC "+ls_CdC+" e data "+string(fdt_fine,"dd/mm/yyyy")+sqlca.sqlerrtext
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_errore = "Dato non trovato lettura RPRrep da premi_periodo_aree per il CdC "+ls_CdC+" e data "+string(fdt_fine,"dd/mm/yyyy")
	uof_add_log(is_prefisso_errore+fs_errore)
	return -1
end if

if isnull(fd_rpr_reparto) then fd_rpr_reparto=0

fd_rpr_reparto = round(fd_rpr_reparto, 4)
uof_add_log("	RPRrep = "+string(fd_rpr_reparto) + "			(dato statistico)")


//valuto HOPP, HOFP e IAPP ---------------------------------------------------------------------
if uof_get_hopp_hofp_iapp(fs_matricola, ls_CdC,fdt_inizio,fdt_fine,fd_hopp,fd_hofp,ld_iapp,fs_errore) < 0 then
	//in fs_errore il messaggio
	return -1
end if

fd_hopp = round(fd_hopp, 4)
fd_hofp = round(fd_hofp, 4)
ld_iapp = round(ld_iapp, 4)
uof_add_log("	HOFP = "+string(fd_hofp)) 
uof_add_log("	HOPP = "+string(fd_hopp))
//------------------------------------------------------------------------

//calcolo quindi IP
if fd_rpr_reparto=0 then
	uof_add_log("	IP = 0     ****** FORZATO a 0 perchè RPRrep=0!")
	fd_ip = 0
else
	fd_ip = (ld_rp / fd_rpr_reparto) * ld_iapp
	fd_ip = round(fd_ip, 4)
	uof_add_log("	IP = "+string(fd_ip))
end if

//valuto VPT -------------------------------------------------------------
if isnull(fd_ix_area) then fd_ix_area=0
if isnull(fd_is_area) then fd_is_area=0

fd_ix_area = round(fd_ix_area, 4)
fd_is_area = round(fd_is_area, 4)
uof_add_log("	IXarea = "+string(fd_ix_area))
uof_add_log("	ISarea = "+string(fd_is_area))

if fd_ip < fd_is_area then
	fd_vpt = 0
	fd_vpt = round(fd_vpt, 4)
	uof_add_log("	VPT = 0				********* forzato a 0 perchè IP < ISarea")
	
elseif fd_ip > fd_ix_area then
	fd_vpt = fd_ix_area * fd_bpt
	fd_vpt = round(fd_vpt, 4)
	uof_add_log("	VPT = "+string(fd_vpt)+"				********* calcolato come IXarea x BPT poichè IP > IXarea")
	
else
	fd_vpt = fd_ip * fd_bpt
	fd_vpt = round(fd_vpt, 4)
	uof_add_log("	VPT = "+string(fd_vpt)+"				(IP x BPT)")
end if

return 1
end function

public function integer uof_get_dipendenti (string fs_cdc_ricerca, string fs_matricola_ricerca, ref string fs_cdc[], ref string fs_des_cdc[], ref string fs_matricole[], ref string fs_cognome_nome[], ref string fs_errore);//restituisce 4 arrays CDC[], DES_CDC[] MATR[] COGNOME_NOME[] dal database presenze

string ls_cod_centro_costo, ls_sql, ls_matricola, ls_cognome, ls_nome, ls_des_cdc
datastore lds_dati
long ll_rows, ll_index, ll_matricola


if uof_connetti_presenze(fs_errore) < 0 then
	//in fs_errore il messaggio di errore
	return -1
end if

ls_sql = "select dipen.matricola, dipen.cognome, dipen.nome, dipen.cenco, gruppi_anag_a.descrizione "+&
			"from dipen "+&
			"inner join gruppi_anag_a ON gruppi_anag_a.azienda = dipen.azienda and "+&
												 "gruppi_anag_a.codice = dipen.cenco "+&
			"where dipen.azienda='"+is_cod_azienda+"' and "+&
					"gruppi_anag_a.gruppo='"+is_gruppo+"' and "+&
					"dipen.ca5='SI' "

if fs_cdc_ricerca<>"" and not isnull(fs_cdc_ricerca) then
	ls_sql += " and dipen.cenco='"+fs_cdc_ricerca+"' "
end if
if fs_matricola_ricerca<>"" and not isnull(fs_matricola_ricerca) then
	ls_sql += " and dipen.matricola='"+fs_matricola_ricerca+"' "
end if


//creo il datastore
if not uof_crea_ds_tran(lds_dati,ls_sql,itran_presenze,fs_errore) then
	//in fs_errore il messaggio di errore
	uof_disconnetti_presenze()
   return -1
end if

ll_rows = lds_dati.retrieve()

//non serve più la transazione con access
uof_disconnetti_presenze()

for ll_index=1 to ll_rows
	ls_matricola = lds_dati.getitemstring(ll_index, 1)
	ls_cognome = lds_dati.getitemstring(ll_index, 2)
	ls_nome = lds_dati.getitemstring(ll_index, 3)
	ls_cod_centro_costo = lds_dati.getitemstring(ll_index, 4)
	ls_des_cdc = lds_dati.getitemstring(ll_index, 5)
	
	fs_matricole[ll_index] = ls_matricola
	fs_cognome_nome[ll_index] = ls_cognome + " " + ls_nome
	fs_cdc[ll_index] = ls_cod_centro_costo
	fs_des_cdc[ll_index] = ls_des_cdc
	
next


return 1
end function

public subroutine uof_get_periodi_lettera (long fl_anno_rif, ref datetime fdt_data_inizio[], ref datetime fdt_data_fine[]);datetime ldt_appo
time							lt_ora_inizio, lt_ora_fine
integer li_index

lt_ora_inizio = time("00:00:00")
lt_ora_fine = time("23:59:59")

//----------------------------------------------------------------------------------------------------------------------------
li_index = 1
ldt_appo = datetime(date(fl_anno_rif, 1, 1), lt_ora_inizio)		// 1	gennaio		YYYY (compreso)
fdt_data_inizio[li_index] = ldt_appo

ldt_appo = datetime(date(fl_anno_rif, 8, 11), lt_ora_fine)		//12	agosto		YYYY (escluso)
fdt_data_fine[li_index] = ldt_appo


//----------------------------------------------------------------------------------------------------------------------------
li_index += 1
ldt_appo = datetime(date(fl_anno_rif, 8, 12), lt_ora_inizio)		//12	agosto		YYYY (compreso)
fdt_data_inizio[li_index] = ldt_appo

ldt_appo = datetime(date(fl_anno_rif, 8, 12), lt_ora_fine)		//12	agosto		YYYY (compreso)
fdt_data_fine[li_index] = ldt_appo


//----------------------------------------------------------------------------------------------------------------------------
li_index += 1
ldt_appo = datetime(date(fl_anno_rif, 8, 13), lt_ora_inizio)		//13	agosto		YYYY (compreso)
fdt_data_inizio[li_index] = ldt_appo

ldt_appo = datetime(date(fl_anno_rif, 12, 14), lt_ora_fine)		//15	dicembre		YYYY (escluso)
fdt_data_fine[li_index] = ldt_appo


//----------------------------------------------------------------------------------------------------------------------------
li_index += 1
ldt_appo = datetime(date(fl_anno_rif, 12, 15), lt_ora_inizio)		//15	dicembre		YYYY (compreso)
fdt_data_inizio[li_index] = ldt_appo

ldt_appo = datetime(date(fl_anno_rif, 12, 31), lt_ora_fine)		//31	dicembre		YYYY (compreso)
fdt_data_fine[li_index] = ldt_appo


return
end subroutine

on uo_premio_produzione.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_premio_produzione.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;
//recupera il percorso del file di log
uof_path_log()



//recupero i gruppi da escludere nella elaborazione di OLrep ------------------------------------
select stringa
into :is_escludi_da_rp
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='GDP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	//fs_errore = "Errore in lettura parametro GDP: "+sqlca.sqlerrtext
	uof_add_log("############### Errore in lettura parametro GDP, gruppi da escludere per OL: "+sqlca.sqlerrtext)
	return -1
	
elseif sqlca.sqlcode=100 or is_escludi_da_rp="" or isnull(is_escludi_da_rp) then
	//fs_errore = "Parametro GDP (Azienda DB presenze) non trovato oppure non impostato nella tabella parametri aziendali!"
	uof_add_log("############### Parametro ADP (gruppi da escludere per OL) non trovato oppure non impostato nella tabella parametri aziendali!")
	return -1
	
else
	//se passi da qui continua
end if



//recupero il codice azienda database presenze ------------------------------------
select stringa
into :is_cod_azienda
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_parametro='ADP' and
		flag_parametro='S'
using sqlca;

if sqlca.sqlcode<0 then
	//fs_errore = "Errore in lettura parametro ADP: "+sqlca.sqlerrtext
	uof_add_log("############### Errore in lettura parametro ADP: "+sqlca.sqlerrtext)
	
   return -1
	
elseif sqlca.sqlcode=100 or is_cod_azienda="" or isnull(is_cod_azienda) then
	//fs_errore = "Parametro ADP (Azienda DB presenze) non trovato oppure non impostato nella tabella parametri aziendali!"
	uof_add_log("############### Parametro ADP (Azienda DB presenze) non trovato oppure non impostato nella tabella parametri aziendali!")
	
   return -1
end if
//-------------------------------------------------------
end event

