﻿$PBExportHeader$uo_ricerca_condizioni_varianti.sru
forward
global type uo_ricerca_condizioni_varianti from nonvisualobject
end type
end forward

global type uo_ricerca_condizioni_varianti from nonvisualobject
end type
global uo_ricerca_condizioni_varianti uo_ricerca_condizioni_varianti

type variables
string cod_prodotto, cod_versione, cod_valuta, &
         cod_tipo_listino_prodotto, messaggi, &
         materie_prime[], tipo_gestione
long anno_registrazione, num_registrazione, prog_riga
dec{4} quantita_prod_finito, quan_mp[]
dec{4} prezzo_listino, prezzo_varianti
str_distinta lstr_distinta[]
end variables

forward prototypes
public function integer uof_condizioni_varianti ()
end prototypes

public function integer uof_condizioni_varianti ();string ls_materie_prime[]
long   ll_i, ll_num_legami, ll_num_righe, ll_y
datastore lds_listini

if f_esplodi_distinta_varianti ( cod_prodotto, &
										cod_versione, &
										materie_prime[], &
										quan_mp[], &
										quantita_prod_finito, &
										anno_registrazione, &
										num_registrazione, &
										prog_riga, &
										tipo_gestione, &
										lstr_distinta[], &
										messaggi) <> 0 then return -1
ll_num_legami = upperbound(lstr_distinta)

lds_listini = Create DataStore
lds_listini.DataObject = "d_ds_listini_diba"
lds_listini.SetTransObject(sqlca)

for ll_i = 1 to ll_num_legami
	ll_num_righe = lds_listini.retrieve(s_cs_xx.cod_azienda, cod_valuta, cod_tipo_listino_prodotto, cod_prodotto,cod_versione,&
	               lstr_distinta[ll_i].cod_padre, lstr_distinta[ll_i].cod_figlio)
	for ll_y = 1 to  ll_num_righe
		choose case lstr_distinta[ll_i].quan_utilizzo
			case is > lds_listini.object.scaglione_4[ll_y]
				// uso scaglione 5
			case is > lds_listini.object.scaglione_3[ll_y]
			case is > lds_listini.object.scaglione_2[ll_y]
			case is > lds_listini.object.scaglione_1[ll_y]
			case else
		end choose
	next
next

return 0





end function

on uo_ricerca_condizioni_varianti.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_ricerca_condizioni_varianti.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

