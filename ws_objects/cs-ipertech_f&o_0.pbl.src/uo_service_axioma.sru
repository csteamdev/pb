﻿$PBExportHeader$uo_service_axioma.sru
forward
global type uo_service_axioma from uo_service_base
end type
end forward

global type uo_service_axioma from uo_service_base
end type
global uo_service_axioma uo_service_axioma

forward prototypes
public function integer dispatch (uo_commandparm_service auo_params)
end prototypes

public function integer dispatch (uo_commandparm_service auo_params);// Funzione dedicata a Gibus per il controllo delle 
boolean lb_connesso
long 		ll_ret,  ll_prog_riga_ord_ven, ll_anno_registrazione, ll_num_registrazione,ll_i, ll_anno_reg[], ll_num_reg[], ll_index, ll_cont, ll_y
string 	ls_profilo,ls_errore,ls_path_file, ls_sql, ls_cod_operatore, ls_keys[], ls_email_utente, ls_destinatari[], ls_allegati[], ls_messaggio, &
			ls_username, ls_nome_cognome, ls_cc[]
transaction sqlc_ax
datastore lds_data
uo_outlook luo_mail
uo_array luo_array
str_service_axioma lstr_service_axioma[]

iuo_log.warning("Avvio servizio Alterter Axioma " )

ll_ret = registryget( s_cs_xx.chiave_root + "\applicazione_" + string(s_cs_xx.profilocorrente) , "AXP", RegString!, ls_profilo)

if ll_ret < 1 then
	iuo_log.error("Errore nella lettura del parametro di registro AXP: procedura interrotta. ")
	return -1
end if


s_cs_xx.cod_utente = auo_params.uof_get_string(1, "")
if s_cs_xx.cod_utente = "" or isnull(s_cs_xx.cod_utente) then
	iuo_log.error("Attenzione, tra gli argomentio passati manca l'utente. Procedura arrestata (uo_service_axioma.dispach()) ")
	return -1
else
	select cod_azienda,
			e_mail
	into 	:s_cs_xx.cod_azienda,
			:ls_email_utente
	from utenti
	where cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode <> 0 then
		iuo_log.error("Errore in ricerca del codice azienda associato all'utente " + s_cs_xx.cod_utente)
		return -1
	end if
	
	if isnull(ls_email_utente) and ls_email_utente = "" then
		iuo_log.error("Attenzione, l'utente  " + s_cs_xx.cod_utente + " non ha associata alcuna email ")
		return -1
	end if
		
end if

// Richiesto da Alberto 28/2/2013: mettere tra i destinatari anche Beatrice e Alberto.
ls_cc[1] = "alberto.bottelli@gibus.it"
ls_cc[2] = "beatrice.bortolotto@gibus.it"

try 
	if ll_ret = 1 then
			
		lb_connesso = guo_functions.uof_crea_transazione("database_" + ls_profilo, sqlc_ax, ls_errore)
		
		if not lb_connesso then
			iuo_log.error( "Errore durante la connessione con il database di AXIOMA.~r~n" + ls_errore)
			return -1
		end if
		
		luo_array = create uo_array
		// ----------   eseguo un ciclo per ricerca gli ordini di Axioma non ancora importati.
		lds_data = create datastore
	
		ls_sql = "select anno_reg_ord_ven, num_reg_ord_ven, prog_riga_ord_ven from ax_exp_apice where flag_elaborato_apice = 'N' "
		if guo_functions.uof_crea_datastore(lds_data,ls_sql, sqlc_ax, ls_errore) < 0 then
			iuo_log.error( "Errore in creazione del datastore lds_data (uo_service_axioma.dispach). " + ls_errore)
			return -1
		end if
		
		for ll_i = 1 to lds_data.rowcount()
	
			ll_anno_registrazione = lds_data.getitemnumber(ll_i, 1)
			ll_num_registrazione = lds_data.getitemnumber(ll_i, 2)
	
			select cod_operatore
			into 	:ls_cod_operatore
			from 	tes_ord_ven
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione and
					num_registrazione = :ll_num_registrazione  ;
	
			if sqlca.sqlcode <> 0 then continue
					
			if not isnull(luo_array.get(ls_cod_operatore)) then
				lstr_service_axioma[] = luo_array.get(ls_cod_operatore)
			end if
			ll_index = upperbound(lstr_service_axioma[])
			if isnull(ll_index) then ll_index = 0
			ll_index ++
	
			lstr_service_axioma[ll_index].al_anno_registrazione = ll_anno_registrazione
			lstr_service_axioma[ll_index].al_num_registrazione = ll_num_registrazione
	
			luo_array.set( ls_cod_operatore, lstr_service_axioma[])
	
		next
		
		// ---------------------------- INVIO LA MAIL AD OGNI UTENTE -------------------------
		
		ll_cont = luo_array.uof_get_keys(ls_keys[])
		luo_mail = create uo_outlook
		luo_mail.ib_force_open = false
		luo_mail.ib_silent_mode = true
				
		for ll_i = 1 to ll_cont
			
			lstr_service_axioma[] = luo_array.get( ls_keys[ll_i] )
			
			select 	e_mail, 
						username, 
						nome_cognome
			into 		:ls_destinatari[1], 
						:ls_username, 
						:ls_nome_cognome
			from 		tab_operatori_utenti 
						join utenti on utenti.cod_utente = tab_operatori_utenti.cod_utente
			where 	tab_operatori_utenti.cod_azienda = :s_cs_xx.cod_azienda and 
						tab_operatori_utenti.cod_operatore = :ls_keys[ll_i] and
						flag_default='S';
			if sqlca.sqlcode = 100 then
				iuo_log.error( "Attenzione: per l'operatore " + ls_keys[ll_i] + " non esiste alcun collegamento con l'utente di default nella tabella operatori-utenti" )
				continue
			end if
			
			if isnull(ls_destinatari[1]) or len(ls_destinatari[1]) < 0 then
				iuo_log.error( g_str.format("Attenzione: l'operatore $1, $2 non ha alcuna email specificata nel profilo utente. Nessun invio mail possibile ", ls_keys[ll_i] ,ls_nome_cognome)  )
				continue
			end if
			
			if upperbound(lstr_service_axioma[]) > 0 and not isnull(lstr_service_axioma[]) then
			
				ls_messaggio = "Gentile operatore " + ls_keys[ll_i] + ", " + ls_nome_cognome + "<br />Con la presente siamo a segnalare che le seguenti configurazioni di Axioma non risultano essere importate in Apice. <br /> "
		
				for ll_y = 1 to upperbound(lstr_service_axioma[])
					
					ls_messaggio += string(lstr_service_axioma[ll_y].al_anno_registrazione) + "/" + string(lstr_service_axioma[ll_y].al_num_registrazione) + "<br />"
					
				next
								
				ls_messaggio += "<br /> Si prega di verificare le configurazioni e procedere con l'importazione o l'annullamento degli ordini.<br /> "
				ls_messaggio += "<br /> Cordiali Saluti.<br /> Gibus S.r.l. <br /> "
				
				if luo_mail.uof_invio_sendmail( ls_destinatari[], ls_cc[], "Avviso ordini non importati da Axioma", ls_messaggio, ls_allegati, ls_errore) then
					iuo_log.info("Avviso inviato correttamente alla mail " + ls_destinatari[1])
				else
					iuo_log.error( "Attenzione: impossibile inviare email per l'operatore " + ls_keys[ll_i] + ", " + ls_nome_cognome + "; " + ls_errore  )
				end if
				
	//			ll_ret = luo_mail.uof_outlook( 0, "A", "Avviso ordini non importati da Axioma", ls_messaggio, ls_destinatari[], ls_allegati[], false, ref ls_errore)
	//			
	//			if ll_ret < 0 then
	//				iuo_log.error( "Attenzione: impossibile inviare email per l'operatore " + ls_keys[ll_i] + ", " + ls_nome_cognome + "; " + ls_errore  )
	//				continue
	//			end if
					
				
			end if
		next
	
		// ----------------------------
	end if
catch (RuntimeError e)
	iuo_log.error( "Exception: " + e.getmessage() )
finally
	destroy luo_array
	destroy luo_mail
	disconnect using sqlc_ax;
	destroy sqlc_ax;
end try

iuo_log.warning("Chiusura procedura Axioma " )

return 0
end function

on uo_service_axioma.create
call super::create
end on

on uo_service_axioma.destroy
call super::destroy
end on

