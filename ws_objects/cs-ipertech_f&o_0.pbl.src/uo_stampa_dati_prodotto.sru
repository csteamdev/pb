﻿$PBExportHeader$uo_stampa_dati_prodotto.sru
forward
global type uo_stampa_dati_prodotto from nonvisualobject
end type
end forward

global type uo_stampa_dati_prodotto from nonvisualobject
end type
global uo_stampa_dati_prodotto uo_stampa_dati_prodotto

forward prototypes
public function integer uof_des_capitolati ()
public function integer uof_des_capitolato (string as_tipo_documento, long al_anno_registrazione, long al_num_registrazione, string as_cod_nota, ref string as_risultato)
public function integer uof_stampa_variabili (string as_modello_prodotto, string as_tipo_documento_origine, string as_tipo_documento_stampa, string as_cod_tipo_documento_stampa, long al_anno_registrazione, long al_num_registrazione, long al_prog_riga, ref string as_risultato)
public function string uof_get_separatore (string as_tipo_documento_stampa, string as_cod_tipo_documento_stampa)
end prototypes

public function integer uof_des_capitolati ();return 1
end function

public function integer uof_des_capitolato (string as_tipo_documento, long al_anno_registrazione, long al_num_registrazione, string as_cod_nota, ref string as_risultato);string	ls_sql, ls_cod_cliente, ls_cod_lingua, ls_cod_contatto, ls_nota_prodotto_lingua, ls_nota_prodotto, ls_cod_prodotto
long	ll_rows, ll_i
datastore lds_data

setnull(ls_cod_contatto)
ls_sql = " select distinct cod_prodotto "
choose case upper(as_tipo_documento)
	case "ORD_VEN"
		ls_sql += "det_ord_ven"
		
		select cod_cliente
		into	:ls_cod_cliente
		from	tes_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :al_anno_registrazione and
				num_registrazione = :al_num_registrazione;
		if sqlca.sqlcode <> 0 then
			as_risultato = "Ordine non trovato"
			return -1
		end if
		
	case "OFF_VEN"
		
		ls_sql += "det_off_ven"
		
		select cod_cliente, cod_contatto
		into	:ls_cod_cliente, :ls_cod_contatto
		from	tes_off_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :al_anno_registrazione and
				num_registrazione = :al_num_registrazione;
		if sqlca.sqlcode <> 0 then
			as_risultato = "Offerta non trovata"
			return -1
		end if
				
end choose

if not isnull(ls_cod_cliente) then
	select cod_lingua
	into	:ls_cod_lingua
	from anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and
				cod_cliente = :ls_cod_cliente;
	if sqlca.sqlcode <> 0 then
		as_risultato = "Cliente non trovato"
		return -1
	end if
elseif isnull(ls_cod_cliente) and not isnull(ls_cod_contatto) then
	select cod_lingua
	into	:ls_cod_lingua
	from anag_contatti
	where cod_azienda = :s_cs_xx.cod_azienda and
				cod_contatto = :ls_cod_contatto;
	if sqlca.sqlcode <> 0 then
		as_risultato = "Cliente non trovato"
		return -1
	end if
else
	as_risultato = "Cliente e Contatto non specificati"
	return -1
end if
	

ls_sql += " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione=" + string(al_anno_registrazione) + " and num_registrazione="+string(al_num_registrazione)

ls_sql += " order by cod_prodotto "

ll_rows = guo_functions.uof_crea_datastore( ref lds_data, ls_sql)

if ll_rows < 0 then 
	as_risultato = "Errore durante la creazione del datastore"
	return -1
end if

as_risultato = ""
for ll_i = 1 to ll_rows
	
	ls_cod_prodotto = lds_data.getitemstring(ll_i, 1)
	
	if isnull(ls_cod_prodotto) then continue
	
	setnull(ls_nota_prodotto_lingua)
	setnull(ls_nota_prodotto)
	
	select 	nota_prodotto_lingua, nota_prodotto
	into		:ls_nota_prodotto_lingua, :ls_nota_prodotto
	from		anag_prodotti_note
	left outer join	anag_prodotti_note_lingue on anag_prodotti_note.cod_azienda = anag_prodotti_note_lingue.cod_azienda and anag_prodotti_note.cod_prodotto = anag_prodotti_note_lingue.cod_prodotto and anag_prodotti_note.cod_nota_prodotto = anag_prodotti_note_lingue.cod_nota_prodotto
	where 	anag_prodotti_note.cod_azienda = :s_cs_xx.cod_azienda and
				anag_prodotti_note.cod_prodotto = :ls_cod_prodotto and
				anag_prodotti_note.cod_nota_prodotto = :as_cod_nota and
				anag_prodotti_note_lingue.cod_lingua = :ls_cod_lingua;
				
	if len(as_risultato) > 0 then as_risultato += "~r~n"
	
	if not isnull(ls_nota_prodotto_lingua) then
				as_risultato += ls_cod_prodotto + "~r~n" + ls_nota_prodotto_lingua
	elseif not isnull(ls_nota_prodotto) then
		as_risultato += ls_cod_prodotto + "~r~n" + ls_nota_prodotto
	end if

next

destroy lds_data

return 0
end function

public function integer uof_stampa_variabili (string as_modello_prodotto, string as_tipo_documento_origine, string as_tipo_documento_stampa, string as_cod_tipo_documento_stampa, long al_anno_registrazione, long al_num_registrazione, long al_prog_riga, ref string as_risultato);string	ls_sql_variabili, ls_str, ls_sql, ls_cod_variabile, ls_des_variabile, ls_flag_off_ven, ls_flag_ord_ven, ls_flag_bol_ven, ls_flag_fat_ven,&
		ls_cod_lingua,ls_cod_cliente, ls_cod_contatto, ls_des_variabile_lingua
long	ll_rows, ll_i
datastore lds_data

ls_sql = " select cod_variabile, flag_tipo_dato, valore_stringa, valore_numero, valore_datetime, des_valore from "
choose case upper(as_tipo_documento_origine)
	case "ORD_VEN"
		ls_sql += "det_ord_ven_conf_variabili"
	case "OFF_VEN"
		ls_sql += "det_off_ven_conf_variabili"
end choose

ls_sql += " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione=" + string(al_anno_registrazione) + " and num_registrazione="+string(al_num_registrazione)

choose case upper(as_tipo_documento_origine)
	case "ORD_VEN"
		ls_sql += " and prog_riga_ord_ven="
	case "OFF_VEN"
		ls_sql += " and prog_riga_off_ven="
end choose

ls_sql += string(al_prog_riga) + " and flag_visible_in_stampa='S' order by prog_variabile "

ll_rows = guo_functions.uof_crea_datastore( ref lds_data, ls_sql)

if ll_rows < 0 then 
	as_risultato = "Errore durante la creazione del datastore"
	return -1
end if

// cerco la eventuale lingua diversa del cliente/contatto
choose case upper(as_tipo_documento_origine)
	case "ORD_VEN"
		select anag_clienti.cod_lingua
		into	:ls_cod_lingua
		from	tes_ord_ven
		join	anag_clienti on tes_ord_ven.cod_azienda = anag_clienti.cod_azienda and tes_ord_ven.cod_cliente = anag_clienti.cod_cliente
		where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				tes_ord_ven.anno_registrazione = :al_anno_registrazione and
				tes_ord_ven.num_registrazione = :al_num_registrazione;
				
	case "OFF_VEN"

		select cod_cliente, cod_contatto
		into :ls_cod_cliente, :ls_cod_contatto
		from tes_off_ven
		where tes_off_ven.cod_azienda = :s_cs_xx.cod_azienda and
				tes_off_ven.anno_registrazione = :al_anno_registrazione and
				tes_off_ven.num_registrazione = :al_num_registrazione;
		if sqlca.sqlcode = 0 then
			if isnull(ls_cod_cliente) and not isnull(ls_cod_contatto) then
				select cod_lingua
				into	:ls_cod_lingua
				from	anag_contatti
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_contatto = :ls_cod_contatto;
			elseif not isnull(ls_cod_cliente) then
				select cod_lingua
				into	:ls_cod_lingua
				from	anag_clienti
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_cliente = :ls_cod_cliente;
			end if					
		end if
end choose


as_risultato = ""
for ll_i = 1 to ll_rows
	ls_cod_variabile =  lds_data.getitemstring(ll_i, 1)
	
	if not isnull(as_modello_prodotto) then
		setnull(ls_des_variabile_lingua)
		
		if not isnull(ls_cod_lingua) then
			select des_variabile
			into	:ls_des_variabile_lingua
			from	tab_des_variabili_lingue
			where	 cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :as_modello_prodotto and
						cod_variabile = :ls_cod_variabile and
						cod_lingua = :ls_cod_lingua;			
		end if
		
		select isnull(des_commerciale, des_variabile), 
				flag_off_ven, 
				flag_ord_ven, 
				flag_bol_ven, 
				flag_fat_ven
		into	:ls_des_variabile, 
				:ls_flag_off_ven, 
				:ls_flag_ord_ven, 
				:ls_flag_bol_ven, 
				:ls_flag_fat_ven
		from	tab_des_variabili
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :as_modello_prodotto and
					cod_variabile = :ls_cod_variabile;			
		if sqlca.sqlcode <> 0 then 
			//ls_des_variabile = ""
			continue
		end if
		
		// se c'è una descrizione in lingua, prendo quella
		if not isnull(ls_des_variabile_lingua) and len(ls_des_variabile_lingua) > 0 then ls_des_variabile = ls_des_variabile_lingua
		
	else	
		select note
		into	:ls_des_variabile
		from	tab_variabili_formule
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_variabile = :ls_cod_variabile;
		
		if sqlca.sqlcode <> 0 then ls_des_variabile = ""
	end if
	
	choose case as_tipo_documento_stampa
		case "OFF_VEN"
			if ls_flag_off_ven = "N" then continue
		case "ORD_VEN"
			if ls_flag_ord_ven = "N" then continue
		case "BOL_VEN"
			if ls_flag_bol_ven = "N" then continue
		case "FAT_VEN"
			if ls_flag_fat_ven = "N" then continue
	end choose	
	
	
	if len(as_risultato) > 0 then as_risultato += uof_get_separatore(as_tipo_documento_origine, as_cod_tipo_documento_stampa)

	choose case lds_data.getitemstring(ll_i, 2)
		case "S"
			if not isnull(lds_data.getitemstring(ll_i, 6)) and len(lds_data.getitemstring(ll_i, 3)) > 0 then
				as_risultato += g_str.format( "$1 = $2  $3", ls_des_variabile, lds_data.getitemstring(ll_i, 3), lds_data.getitemstring(ll_i, 6) )
			else
				as_risultato += g_str.format( "$1 = $2", ls_des_variabile, lds_data.getitemstring(ll_i, 3) )
			end if
		case "N"
			as_risultato += g_str.format( "$1= $2", ls_des_variabile, lds_data.getitemnumber(ll_i, 4) )
		case "D"
			as_risultato += g_str.format( "$1= $2", ls_des_variabile, lds_data.getitemdatetime(ll_i, 5) )
	end choose

next

return 0
end function

public function string uof_get_separatore (string as_tipo_documento_stampa, string as_cod_tipo_documento_stampa);string ls_flag_tipo_separatore_variabili

if isnull(as_tipo_documento_stampa) then return ""

choose case as_tipo_documento_stampa
	case "OFF_VEN"		// offerte
		
		select flag_tipo_separatore_variabili
		into	:ls_flag_tipo_separatore_variabili
		from  tab_tipi_off_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_off_ven = :as_cod_tipo_documento_stampa;
				
		if sqlca.sqlcode <> 0 then return "~r~n"
		
	case "ORD_VEN"
		
		select flag_tipo_separatore_variabili
		into	:ls_flag_tipo_separatore_variabili
		from  tab_tipi_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_ord_ven = :as_cod_tipo_documento_stampa;
				
		if sqlca.sqlcode <> 0 then return "~r~n"
		
	case "BOL_VEN"
		
		select flag_tipo_separatore_variabili
		into	:ls_flag_tipo_separatore_variabili
		from  tab_tipi_bol_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_bol_ven = :as_cod_tipo_documento_stampa;
				
		if sqlca.sqlcode <> 0 then return "~r~n"
		
	case "FAT_VEN"
		
		select flag_tipo_separatore_variabili
		into	:ls_flag_tipo_separatore_variabili
		from  tab_tipi_fat_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_fat_ven = :as_cod_tipo_documento_stampa;
				
		if sqlca.sqlcode <> 0 then return "~r~n"
end choose

if isnull(ls_flag_tipo_separatore_variabili) then ls_flag_tipo_separatore_variabili=""

choose case ls_flag_tipo_separatore_variabili
	case 	""
		return "~r~n"
	case "A"		// capo riga
		return "~r~n"
	case "B"		// capo riga
		return "; "
	case "C"		// capo riga
		return ", "
	case "D"		// capo riga
		return " - "
	case else
		return "~r~n"
end choose

return "~r~n"
 
end function

on uo_stampa_dati_prodotto.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_stampa_dati_prodotto.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

