﻿$PBExportHeader$uo_stampa_etichette.sru
forward
global type uo_stampa_etichette from nonvisualobject
end type
end forward

global type uo_stampa_etichette from nonvisualobject
end type
global uo_stampa_etichette uo_stampa_etichette

type variables
boolean ib_cancella_spool = true
string is_errore, is_nome_files_aperti[], is_nome_file_unico_stampa
long   il_elenco_etichette[]
end variables

forward prototypes
private function string uof_riempimento (string fs_valore, string fs_allineamento, long fl_num_caratteri)
private function integer uof_sostituisci (ref string fs_stringa, string fs_nome_campo, string fs_valore)
public function integer uof_prepara_etichetta (string fs_nome_modello)
public function integer uof_prepara_stampa (integer fl_numero_etichetta)
public function integer uof_stampa_etichette ()
public function integer uof_aggiungi_valore (integer fl_num_etichetta, string fs_nome_campo, string fs_valore_campo)
public function integer uof_taglio_carta (integer fl_num_etichetta, string fs_azione)
end prototypes

private function string uof_riempimento (string fs_valore, string fs_allineamento, long fl_num_caratteri);choose case upper(fs_allineamento)
		
	case "S"
		
		if fl_num_caratteri > 0 then
			
			if len(fs_valore) > fl_num_caratteri then		// devo stampare meno caratteri di quelli contenuti nel valore
				fs_valore = left(fs_valore, fl_num_caratteri)
			else														// eseguo il riempimento a destra
				fs_valore = fs_valore + space(fl_num_caratteri - len(fs_valore))
			end if				
			
		end if
		
	case "D"
		
		if fl_num_caratteri > 0 then
			
			if len(fs_valore) > fl_num_caratteri then		// devo stampare meno caratteri di quelli contenuti nel valore
				fs_valore = left(fs_valore, fl_num_caratteri)
			else														// eseguo il riempimento a destra
				fs_valore = space(fl_num_caratteri - len(fs_valore)) + fs_valore
			end if				
			
		end if
		
end choose

return fs_valore
end function

private function integer uof_sostituisci (ref string fs_stringa, string fs_nome_campo, string fs_valore);/* 		FUNZIONE DI SOSTITUZIONE DEL NOME CAMPO CON L'EFFETTIVO VALORE NELL'ETICHETTA

fs_stringa = STRINGA CON IL FILE ETICHETTA
fs_nome_campo = NOME DEL CAMPO DA SOSTITUIRE
fs_valore = VALORE DA SOSTITUIRE

RETURN  0  TUTTO OK !!  ()
       -1  ERRORE
		  1  NON HO TROVATO NIENTE DA SOSTITUIRE OPPURE SONO STATI PASSATI ARGOMENTI NULL O VUOTI   */

boolean lb_sostituito = FALSE
string ls_stringa_ricerca, ls_campo, ls_valore, ls_allineamento
long ll_pos_inizio, ll_pos_fine, ll_num_caratteri

if isnull(fs_nome_campo) or len(fs_nome_campo) < 1 then return 1

if isnull(fs_valore) then fs_valore = ""

fs_nome_campo = upper(fs_nome_campo)
ls_stringa_ricerca = "<" + fs_nome_campo
ll_pos_fine = 0
do while true
	ll_pos_inizio = pos(fs_stringa, ls_stringa_ricerca, ll_pos_fine + 1)
	
	if isnull(ll_pos_inizio) then return 1
	if ll_pos_inizio = 0 and lb_sostituito then return 0			// trovato niente, ma evevo già trovato qualcosa prima
	if ll_pos_inizio = 0 and not lb_sostituito then return 1		// trovato niente da sostituire

	// a questo punto ho trovato qualcosa
	
	ll_pos_fine = pos(fs_stringa, ">", ll_pos_inizio)
	// se non trovo ninete errore perchè vuol dire che il nome del campo non è stato chiuso con il carattere >
	if ll_pos_fine = 0 or isnull(ll_pos_fine) then return -1
	
	// a questo punto ho la posizione di inizio e di fine e posso estrarre il campo completo
	
	ls_campo = mid(fs_stringa, ll_pos_inizio, ll_pos_fine - ll_pos_inizio + 1)
	
	// elimino i caratteri < >
	ls_valore = mid(ls_campo, 2, len(ls_campo) -2)
	
	// verifico se c'è riempimento o allineamento da fare
	if pos(ls_valore, "/") > 0 then
		ls_allineamento = mid(ls_valore,pos(ls_valore, "/") + 1 , 1)
		ll_num_caratteri = long(mid(ls_valore,pos(ls_valore, "/") + 2))
		if isnull(ll_num_caratteri) then ll_num_caratteri = 0
		
		ls_valore = uof_riempimento(fs_valore, ls_allineamento, ll_num_caratteri)
	else
		
		ls_valore = fs_valore
		
	end if
	
	fs_stringa = replace(fs_stringa, ll_pos_inizio, len(ls_campo), ls_valore)
	lb_sostituito = true
loop

return 0
end function

public function integer uof_prepara_etichetta (string fs_nome_modello);/*		funzione di preparazione etichetta con i dati di default 

fs_nome_modello = nome del modello dell'etichetta

il sistema andrà a prendere un file di testo cercandolo nella cartella VOL + RISORSE + fs_nome_modello.txt


RETURN = -1 errore
       SE MAGGIORE DI ZERO RESTITUISCE IL NUMERO ASSOCIATO ALL'ETICHETTA IN PREPARAZIONE (in pratica il numero del file aperto in memoria).
		 
*/

string ls_nome_modello, ls_nome_etichetta, ls_stream, ls_nome_file_stampa
long ll_file_modello, ll_file_etichetta, ll_errore, ll_num_etichetta

if isnull(fs_nome_modello) or len(fs_nome_modello) < 1 then
	is_errore = "Indicare un nome modello valido."
	return -1
end if

ls_nome_modello = s_cs_xx.volume + s_cs_xx.risorse + fs_nome_modello + ".txt"

if not fileexists(ls_nome_modello) then
	is_errore = "Il file del modello " + fs_nome_modello + " non esiste."
	return -1
end if

ll_file_modello = fileopen(ls_nome_modello, streammode!)
if ll_file_modello < 1 then
	is_errore = "Errore nell'apertura del file modello " + ls_nome_modello + ". "
	return -1
end if

ll_errore = fileread(ll_file_modello, ls_stream)
if ll_errore < 1 then
	is_errore = "Errore nella lettura del file modello " + ls_nome_modello + ". "
	return -1
end if

fileclose(ll_file_modello)
// ---------------- creo il file con l'etichetta richiesta -----------------------------------

randomize(0)
ll_errore = rand(100000)

ls_nome_etichetta = string(year(today())) + string(month(today()),"0000") + string(day(today()),"000") + string(hour(now()),"00") + string(minute(now()),"00") + string(second(now()),"00") + string(ll_errore,"000000")

ls_nome_file_stampa = ls_nome_etichetta

ls_nome_etichetta = s_cs_xx.volume + s_cs_xx.risorse + ls_nome_etichetta + ".txt"

ll_file_etichetta = fileopen(ls_nome_etichetta, streammode!,Write!, LockReadWrite!, Replace!)
if ll_file_modello < 1 then
	is_errore = "Errore nell'apertura del file etichetta " + ls_nome_modello + ". "
	return -1
end if

ll_errore = filewrite(ll_file_etichetta, ls_stream)
if ll_errore < 1 then
	is_errore = "Errore nella creazione del file etichetta " + ls_nome_modello + ". "
	return -1
end if

ll_num_etichetta = upperbound(il_elenco_etichette) + 1

if ll_num_etichetta = 1 then
	is_nome_file_unico_stampa = s_cs_xx.volume + s_cs_xx.risorse + "PRN_" + ls_nome_file_stampa + ".txt"
end if
	
il_elenco_etichette[ll_num_etichetta] = ll_num_etichetta
is_nome_files_aperti[ll_num_etichetta] = ls_nome_etichetta

fileclose(ll_file_etichetta)

return ll_num_etichetta
end function

public function integer uof_prepara_stampa (integer fl_numero_etichetta);/* 			funzione di preparazione del file unico di stampa etichette

fl_numero_etichetta = NUMERO DEL MODELLO DI ETICHETTA DA STAMPARE

return = 0 TUTTO OK
		  -1 ERRORE
*/

string ls_stampante,ls_default, ls_stream
long ll_file, ll_i, ll_indice, ll_ret, ll_stampante
integer li_risposta
time lt_ora

ll_indice = 0
for ll_i = 1 to upperbound(il_elenco_etichette)
	if il_elenco_etichette[ll_i] = fl_numero_etichetta then
		ll_indice = ll_i
		exit
	end if
next

// richiesta la stampa di ina etichetta inesistente
if ll_indice = 0 then
	is_errore = "Etichetta non trovata"
	return -1
end if

ll_file = FileOpen(is_nome_files_aperti[ll_indice], StreamMode!, Read!)
fileread(ll_file, ls_stream)
fileclose(ll_file)

//ll_stampante = printopen()
//li_risposta = printsend(ll_stampante, ls_stream)
//printclose(ll_stampante)


ll_stampante = fileopen(is_nome_file_unico_stampa,linemode!,write!)

if ll_stampante > 0 then 
	// aggiungo l'etichetta al file unico di stampa
	filewrite(ll_stampante,ls_stream)
else
	g_mb.messagebox("APICE","Non posso aprire o trovare il file unico di stampa " + is_nome_file_unico_stampa + ".~r~nProvare rilanciare la stampa." ,stopsign!)
end if

fileclose(ll_stampante)


//lt_ora =RelativeTime ( now(),1 )
//
//do while 1=1
//	
//if now()>lt_ora then exit
//
//loop

RETURN 0
end function

public function integer uof_stampa_etichette ();/* 			funzione di esecuzione della stampa etichette

fl_numero_etichetta = NUMERO DEL MODELLO DI ETICHETTA DA STAMPARE
fl_num_etichette = COPIE DA STAMPARE DELLA STESSA ETICHETTA

return = 0 TUTTO OK
		  -1 ERRORE
*/
//------------claudia 11/07/07 modificato l'errore e messa la variabile is_nome_file_unico_stampa corretta //
string ls_stampante,ls_default, ls_str, ls_stream
long ll_ret, ll_indice, ll_i, ll_file

ll_ret = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "prn", ls_stampante)
if ll_ret = -1 then
	is_errore = "Attenzione !!! Manca il parametro di registro PRN nella chiave applicazione; tale parametro indica la stampante."
	return -1
end if


// leggo il file unico di stampa
ll_file = FileOpen(is_nome_file_unico_stampa, StreamMode!, Read!)
if ll_file < 0 then
	is_errore = "Attenzione !!! Problemi con l'apertura dell'etichetta " + is_nome_file_unico_stampa + "~r~n(uof_stampa_etichetta)"
	//messagebox("Apice", is_errore, stopsign!)
	return -1
end if

ll_ret = fileread(ll_file, ls_stream)
if ll_ret < 0 then
	is_errore = "Attenzione !!! Problemi con la lettura dell'etichetta " + is_nome_file_unico_stampa+ "~r~n(uof_stampa_etichetta)"
	//messagebox("Apice", is_errore, stopsign!)
	return -1
end if

ll_ret = fileclose(ll_file)
if ll_ret < 0 then
	is_errore = "Attenzione !!! Problemi con la chisura dell'etichetta " +is_nome_file_unico_stampa + "~r~n(uof_stampa_etichetta)"
	//messagebox("Apice", is_errore, stopsign!)
	return -1
end if


// apro la stampante e lancio il file unico di stampa
//messagebox("Apice", "controllare file ", stopsign!)
ll_file = FileOpen(ls_stampante, StreamMode!, Write!)
if ll_file < 0 then
	is_errore = "Attenzione !!! Problemi con l'apertura della stampante " +is_nome_file_unico_stampa+ "~r~n(uof_stampa_etichetta)"
	//messagebox("Apice", is_errore, stopsign!)
	return -1
end if

ll_ret = filewrite(ll_file, ls_stream)
if ll_ret < 0 then
	is_errore = "Attenzione !!! Problemi con la scrittura sulla stampante" +is_nome_file_unico_stampa+ "~r~n(uof_stampa_etichetta)"
	//messagebox("Apice", is_errore, stopsign!)
	return -1
end if

ll_ret = fileclose(ll_file)
if ll_ret < 0 then
	is_errore = "Attenzione !!! Problemi con la chiusura della stampante" +is_nome_file_unico_stampa+ "~r~n(uof_stampa_etichetta)"
	//messagebox("Apice", is_errore, stopsign!)
	return -1
end if

return 0
end function

public function integer uof_aggiungi_valore (integer fl_num_etichetta, string fs_nome_campo, string fs_valore_campo);/* 				FUNZIONE DI COMPOSIZIONE ETICHETTA

fs_nome_campo = NOME DEL CAMPO DA ANDARE A SOSTITUIRE
fs_valore_campo = VALORE DA ANDARE A SOSTITUIRE  

RETURN 0 = TUTTO ok
		 -1 = ERRORE
*/
string ls_stream
long ll_i, ll_file, ll_ret, ll_indice=0

for ll_i = 1 to upperbound(il_elenco_etichette)
	if il_elenco_etichette[ll_i] = fl_num_etichetta then 
		ll_indice = ll_i
		exit
	end if
next

if ll_indice = 0 then
	is_errore = "Etichetta richiesta non trovata. Usare la funzione di preparazione etichetta."
	return -1
end if

ll_file = fileopen(is_nome_files_aperti[ll_indice], Streammode!)
if ll_file < 0 then 
	is_errore = "Il file " + is_nome_files_aperti[ll_indice] + " è inesistente; usare la funzione di preparazione etichetta !"
	return -1
end if

ll_ret = fileread(ll_file, ls_stream)
if ll_ret < 0 then
	is_errore = "Errore nella lettura del file etichetta " + is_nome_files_aperti[ll_indice] + ". "
	return -1
end if

fileclose(ll_file)

ll_ret = uof_sostituisci(ref ls_stream, fs_nome_campo, fs_valore_campo)

if ll_ret = -1 then
	is_errore = "Errata formattazione del file o del campo; verificare il modello etichetta."
	return -1
end if

if ll_ret = 1 then
	is_errore = "Campo richiesto non trovato."
	return -1
end if

ll_file = fileopen(is_nome_files_aperti[ll_indice], Streammode!, Write!, LockReadWrite!, Replace!)

if ll_file < 0 then 
	is_errore = "Il file " + is_nome_files_aperti[ll_indice] + " è inesistente; usare la funzione di preparazione etichetta !"
	return -1
end if

ll_ret = filewrite(ll_file, ls_stream)
if ll_ret < 1 then
	is_errore = "Errore nella creazione del file etichetta " + is_nome_files_aperti[ll_indice] + ". "
	return -1
end if

fileclose(ll_file)

return 0

end function

public function integer uof_taglio_carta (integer fl_num_etichetta, string fs_azione);/* 				FUNZIONE DI COMPOSIZIONE ETICHETTA

fl_num_etichetta = numero dell'etichetta a cui aggiungere il comando di taglio
fb_taglio  TRUE = aggiungi comando di arretramento e taglio
			  FALSE= elimina comando di arretramento e taglio

RETURN 0 = TUTTO ok
		 -1 = ERRORE
*/
string ls_stream, ls_taglio_fine
long ll_i, ll_file, ll_ret, ll_indice=0

for ll_i = 1 to upperbound(il_elenco_etichette)
	if il_elenco_etichette[ll_i] = fl_num_etichetta then 
		ll_indice = ll_i
		exit
	end if
next
if ll_indice = 0 then
	is_errore = "Etichetta richiesta non trovata. Usare la funzione di preparazione etichetta."
	return -1
end if

ll_file = fileopen(is_nome_files_aperti[ll_indice], Streammode!)
if ll_file < 0 then 
	is_errore = "Il file " + is_nome_files_aperti[ll_indice] + " è inesistente; usare la funzione di preparazione etichetta !"
	return -1
end if

ll_ret = fileread(ll_file, ls_stream)
if ll_ret < 0 then
	is_errore = "Errore nella lettura del file etichetta " + is_nome_files_aperti[ll_indice] + ". "
	return -1
end if

fileclose(ll_file)
ls_taglio_fine = char(02) + "o~r~n"

choose case fs_azione
	case "T"		// taglio carta
		ls_stream = char(02) + "f315~r~n" + char(02) + "O0210~r~n" + ls_stream + ls_taglio_fine
	case "N"		// offset = 0 e non taglia
		ls_stream = char(02) + "f210~r~n" + char(02) + "O0210~r~n" + ls_stream
	case "A"		// arretramento carta
		ls_stream = char(02) + "f315~r~n" + char(02) + "O0210~r~n" + ls_stream
	case else
		// errore !!!!
end choose

	
ll_file = fileopen(is_nome_files_aperti[ll_indice], Streammode!, Write!, LockReadWrite!, Replace!)

if ll_file < 0 then 
	is_errore = "Il file " + is_nome_files_aperti[ll_indice] + " è inesistente; usare la funzione di preparazione etichetta !"
	return -1
end if

ll_ret = filewrite(ll_file, ls_stream)
if ll_ret < 1 then
	is_errore = "Errore nella creazione del file etichetta " + is_nome_files_aperti[ll_indice] + ". "
	return -1
end if

fileclose(ll_file)

return 0

end function

on uo_stampa_etichette.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_stampa_etichette.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event destructor;// chiudo gli eventuali files aperti

long ll_i 

for ll_i = 1 to upperbound(il_elenco_etichette)
	
	fileclose(il_elenco_etichette[ll_i])
	
	if ib_cancella_spool then filedelete(is_nome_files_aperti[ll_i])
	
next

if ib_cancella_spool then filedelete(is_nome_file_unico_stampa)
end event

