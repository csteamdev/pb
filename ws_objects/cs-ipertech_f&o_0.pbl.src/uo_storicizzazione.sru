﻿$PBExportHeader$uo_storicizzazione.sru
forward
global type uo_storicizzazione from nonvisualobject
end type
end forward

global type uo_storicizzazione from nonvisualobject
end type
global uo_storicizzazione uo_storicizzazione

type variables
string is_flag_ar, is_cod_materia_prima[], is_cod_prodotto_origine, is_cod_prodotto_ar
double id_quan_utilizzo[], id_valore_storico
long   il_anno_reg_bol_ven, il_num_reg_bol_ven

// *** Michela 26/01/2006: aggiungo questa proprietà che di default è a true. Quando storicizzo una bolla, ptenda
//                         non vuole cancellare le righe riferite, quindi anche per l'ordine, non vado a cancellare le 
//                         righe riferite. mi limito a mettere il riferimento a null. per fare questo processo, anzichè
//                         l'altro metto la proprietà a false. specifica storico bolle revisione 5 del 23/12/2005.
boolean ib_bolla = false
public boolean ib_riferite = true
end variables

forward prototypes
public function integer uof_storicizza (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, boolean fb_prodotto_finito, string fs_mp_escluse[], long fl_righe_escluse[], ref string fs_testo_esclusioni, ref string fs_messaggio)
public function integer uof_storicizza_det_ord_ven_mp (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, long fl_prog_storico, string fs_mp_escluse[], ref string fs_messaggio)
public function integer uof_elimina_righe (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, boolean fb_prodotto_finito, ref string fs_messaggio)
public function integer uof_storicizza_det_ord_ven (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, long fl_prog_storico, long fl_righe_escluse[], ref string fs_messaggio)
public function integer uof_storicizza_comp_det_ord_ven (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, long fl_prog_storico, string fs_flag_riga_originale, ref string fs_messaggio)
public function integer uof_storicizza_det_ord_ven_mp_ar (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, long fl_prog_storico, string fs_cod_materia_prima[], double fd_quan_utilizzo[], ref string fs_messaggio)
public function integer uof_elimina_riga_bol_ven (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_registrazione, ref string fs_messaggio)
public function integer uof_rinumera_righe_bol_ven (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio)
public function integer uof_storicizza_riga_bol_ven (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_bol_ven, ref string fs_errore)
public function integer uof_elimina_mov_mag_commesse (long fl_anno_commessa, long fl_num_commessa, ref string fs_errore)
public function integer uof_storicizza_riga_bol_ven_parziale (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_bol_ven, string fs_mp_escluse[], long fl_righe_escluse[], ref string fs_testo_esclusioni, ref string fs_errore)
public function integer uof_valorizza_semilavorato (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto_trasferimento, ref decimal fd_prezzo, string fs_messaggio)
public function integer uof_calcola_prezzo (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, ref decimal fd_prezzo, ref string fs_errore)
end prototypes

public function integer uof_storicizza (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, boolean fb_prodotto_finito, string fs_mp_escluse[], long fl_righe_escluse[], ref string fs_testo_esclusioni, ref string fs_messaggio);/*	FUNZIONE DI GESTIONE DELLA PROCEDURA DI STORICIZZAZIONE 

SPIEGAZIONE DEI PARAMETRI

NOME PARAMETRO	             	|    DESCRIZIONE PARAMETRI
------------------------------|-----------------------------------------------------------------------------------------
fl_anno_registrazione			| anno ordine
fl_num_registrazione				| numero ordine
fl_prog_riga_ord_ven				| riga ordine
fb_prodotto_finito				| storicizzazione di riga riferimento o prodotto finito (TRUE= prodotto finito, FALSE=addizionale o roba riferita)
fs_mp_escluse[]					| elenco materie prime che non devono essere storicizzate
fl_righe_escluse[]				| elenco delle righe optional/addizionali/latro riferito che non devono essere storicizzate
fs_testo_esclusioni				| testo autocomposto relatiovo alle esclusioni (solo nel caso fb_prodotto_finito=false)
fs_messaggio						| testo errore
-------------------------------------------------------------------------------------------------------------------------

*/

string ls_vuoto[],ls_cod_versione
long   ll_vuoto[], ll_prog_storico, ll_ret

select max(prog_storico)
into   :ll_prog_storico
from   det_ord_ven_storico
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione  and
		 prog_riga_ord_ven  = :fl_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in calcolo massimo progressivo in tabella comp_det_ord_ven_storico~r~n" + sqlca.sqlerrtext
	return -1
end if

if isnull(ll_prog_storico) or ll_prog_storico = 0 then
	ll_prog_storico = 1
else
	ll_prog_storico ++
end if

select cod_versione
into   :ls_cod_versione
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione  and
		 prog_riga_ord_ven  = :fl_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca cod_versione in tabella det_ord_ven~r~n" + sqlca.sqlerrtext
	return -1
end if

ll_ret = uof_storicizza_det_ord_ven(fl_anno_registrazione, fl_num_registrazione,fl_prog_riga_ord_ven, ll_prog_storico, fl_righe_escluse[], ref fs_messaggio)
if ll_ret < 0 then
	return -1
end if


if fb_prodotto_finito then
	// solo per i prodotti finiti, non per addizionali opt o altro
	ll_ret = uof_storicizza_comp_det_ord_ven(fl_anno_registrazione, fl_num_registrazione,fl_prog_riga_ord_ven, ll_prog_storico, "S", ref fs_messaggio)
	if ll_ret < 0 then
		return -1
	end if
	
end if

if is_flag_ar = "N" then
	
	if not isnull(ls_cod_versione) and len(ls_cod_versione) > 0 and fb_prodotto_finito then
	
		ll_ret = uof_storicizza_det_ord_ven_mp(fl_anno_registrazione, fl_num_registrazione,fl_prog_riga_ord_ven, ll_prog_storico, fs_mp_escluse[], ref fs_messaggio)
		if ll_ret < 0 then
			return -1
		end if
		
	end if

	ll_ret = uof_elimina_righe(fl_anno_registrazione, fl_num_registrazione,fl_prog_riga_ord_ven, fb_prodotto_finito, ref fs_messaggio)
	
	if ll_ret < 0 then
		return -1
	end if

else
	
	if not isnull(ls_cod_versione) and len(ls_cod_versione) > 0 and fb_prodotto_finito then
	
		ll_ret = uof_storicizza_det_ord_ven_mp_ar(fl_anno_registrazione, fl_num_registrazione,fl_prog_riga_ord_ven, ll_prog_storico, is_cod_materia_prima[], id_quan_utilizzo[], ref fs_messaggio)
		if ll_ret < 0 then
			return -1
		end if
		
	end if
	
end if
		
return 0
end function

public function integer uof_storicizza_det_ord_ven_mp (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, long fl_prog_storico, string fs_mp_escluse[], ref string fs_messaggio);boolean lb_salta
string ls_materia_prima[], ls_cod_prodotto, ls_cod_versione, ls_cod_misura_mag, ls_versione_mat_prima[], ls_null
long   ll_i, ll_y,ll_cont_escluse, ll_cont_mp, ll_ret, ll_progressivo
double ld_quantita_utilizzo[], ld_quan_ordine
uo_funzioni_1  luo_funzioni_1



setnull(ls_null)

select cod_prodotto,
       cod_versione,
		 quan_ordine
into   :ls_cod_prodotto,
       :ls_cod_versione,
		 :ld_quan_ordine
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca riga ordine in funzione (uof_storicizza_det_ord_ven_mp).~r~n" + sqlca.sqlerrtext
	return -1
end if

luo_funzioni_1 = create uo_funzioni_1
	
if luo_funzioni_1.uof_trova_mat_prime_varianti (ls_cod_prodotto, &
									 ls_cod_versione, &
									 ls_null,&
									ls_materia_prima[], &
									ls_versione_mat_prima[], &
									ld_quantita_utilizzo[], &
									ld_quan_ordine, &
									fl_anno_registrazione, &
									fl_num_registrazione, &
									fl_prog_riga_ord_ven, &
									"varianti_det_ord_ven", &
									ls_null,&
									ref fs_messaggio ) < 0 then
									
	fs_messaggio = "Errore in estrazione materie prime (uof_trova_mat_prime_varianti): "  + fs_messaggio
	destroy luo_funzioni_1
	return -1
end if
	
destroy luo_funzioni_1

ll_cont_escluse = upperbound(fs_mp_escluse)
ll_cont_mp      = upperbound(ls_materia_prima)
ll_progressivo  = 0

// se ci sono materie prime
if ll_cont_mp > 0 and not isnull(ll_cont_mp) then
	for ll_i = 1 to ll_cont_mp
		
		// verifico mp escluse
		if ll_cont_escluse > 0 and not isnull(ll_cont_escluse) then
			
			lb_salta = false
			
			for ll_y = 1 to ll_cont_escluse
				
				if ls_materia_prima[ll_i] = fs_mp_escluse[ll_y] then
					lb_salta = true
					exit
				end if
				
			next
			
			if lb_salta then continue
			
		end if
		
		ll_progressivo ++
		
		select cod_misura_mag
		into   :ls_cod_misura_mag
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_materia_prima[ll_i];
				 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca materia prima a magazzino (uof_storicizza_det_ord_ven_mp).~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		INSERT INTO det_ord_ven_storico_mp  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  prog_riga_ord_ven,   
				  prog_storico,   
				  progressivo,   
				  cod_prodotto,   
				  quan_utilizzo,   
				  cod_misura )  
		VALUES (:s_cs_xx.cod_azienda,   
				  :fl_anno_registrazione,   
				  :fl_num_registrazione,   
				  :fl_prog_riga_ord_ven,   
				  :fl_prog_storico,   
				  :ll_progressivo,   
				  :ls_materia_prima[ll_i],   
				  :ld_quantita_utilizzo[ll_i],   
				  :ls_cod_misura_mag);
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in creazione storico mp (uof_storicizza_det_ord_ven_mp).~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
	next
	
end if

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in storicizzazione materie prime.~r~n" + sqlca.sqlerrtext
	return -1
end if


return 0
end function

public function integer uof_elimina_righe (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, boolean fb_prodotto_finito, ref string fs_messaggio);long ll_anno_commessa, ll_num_commessa, ll_null, ll_i, ll_righe, ll_riga_ordine, ll_num_riga_appartenenza, &
     ll_riga_corrente, ll_anno_comm_riferita,ll_num_comm_riferita, ll_errore, ll_ret

string ls_cod_prodotto, ls_cod_versione, ls_flag_evasione, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_flag_blocco, &
       ls_cod_prodotto_rif, ls_cod_tipo_det_ven_rif, ls_cod_prodotto_raggruppato

dec{4} ldd_quan_in_produzione, ldd_quan_assegnata, ldd_quan_prodotta, ldd_quan_in_ordine, ldd_quan_impegnata, &
       ldd_quan_evasa, ldd_quan_in_evasione, ld_quan_ordine, ld_quan_ordine_rif, ld_quan_raggruppo

uo_funzioni_1 luo_funzioni_commesse

uo_produzione luo_produzione

setnull(ll_null)

select anno_commessa,
       num_commessa,
		 cod_tipo_det_ven,
		 cod_prodotto,
		 quan_ordine,
		 cod_versione
into   :ll_anno_commessa,
       :ll_num_commessa,
		 :ls_cod_tipo_det_ven,
		 :ls_cod_prodotto,
		 :ld_quan_ordine,
		 :ls_cod_versione
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_riga_ord_ven = :fl_prog_riga_ord_ven;

// 13/04/2004 Enrico
//  cancellazione avanzamento di produzione su richiesta beatrice


// 20/01/2006 Michela
// ***        ho portato fuori dell'if la cancellazione delle sessioni, altrimenti
//            con gli sfusi da errore


luo_produzione = Create uo_produzione
ll_errore = luo_produzione.uof_elimina_prod_det(fl_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ref fs_messaggio)
if ll_errore < 0 then
	fs_messaggio = "Errore in cancellazione produzione~r~n" + fs_messaggio
	destroy luo_produzione
	return -1
end if
destroy luo_produzione



// se manca la versione, e quindi la distinta base, molte cose non hanno senso
if not isnull(ls_cod_versione) and len(ls_cod_versione) > 0 then
	
//	luo_produzione = Create uo_produzione
//	ll_errore = luo_produzione.uof_elimina_prod_det(fl_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ref fs_messaggio)
//	if ll_errore < 0 then
//		fs_messaggio = "Errore in cancellazione produzione~r~n" + fs_messaggio
//		destroy luo_produzione
//		return -1
//	end if
//	destroy luo_produzione
// ------------------


	if not isnull(ll_anno_commessa) then
		luo_funzioni_commesse = CREATE uo_funzioni_1
		if luo_funzioni_commesse.uof_cancella_commessa(ll_anno_commessa,ll_num_commessa, fs_messaggio) = -1 then
			fs_messaggio = "Errorein cancellazione commessa~r~n" + fs_messaggio
			destroy luo_funzioni_commesse
			return -1
		end if
		
		delete from anag_commesse
		where  cod_azienda   = :s_cs_xx.cod_azienda and
				 anno_commessa = :ll_anno_commessa and
				 num_commessa  = :ll_num_commessa;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la cancellazione della commessa~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		// *** Michela 26/01/2006: dopo aver cancellato la commessa elimino tutti i movimenti magazzino
		//                         ad essa collegati. su richiesta di Enrico per ptenda (specifica storico bolle)
		
		ll_ret = uof_elimina_mov_mag_commesse( ll_anno_commessa, ll_num_commessa, fs_messaggio)
		if ll_ret <> 0 then
			return -1
		end if
		
		// *** fine modifica
		
	end if

	delete from varianti_det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in cancellazione varianti~r~n" + sqlca.sqlerrtext
		return -1
	end if


	delete from tab_optionals_comp_det
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in cancellazione optionals (tab_optionals_comp_det)~r~n" + sqlca.sqlerrtext
		return -1
	end if



	delete from tab_mp_non_ric_comp_det
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in cancellazione del dettaglio complementare (tab_mp_non_ric_comp_det)~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	
	delete from comp_det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore cancellazione del dettaglio complementare~r~n" + sqlca.sqlerrtext
		return -1
	end if

end if


if fb_prodotto_finito then

	declare cu_righe_ordine cursor for
	select  prog_riga_ord_ven,
			  cod_tipo_det_ven,
			  cod_prodotto,
			  quan_ordine,
			  anno_commessa,
			  num_commessa
	from    det_ord_ven
	where   cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :fl_anno_registrazione and
			  num_registrazione = :fl_num_registrazione and
			  num_riga_appartenenza = :fl_prog_riga_ord_ven;
			  
	open    cu_righe_ordine;
	
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in OPEN cursore cu_righe_ordine (uof_elimina_righe)~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	do while true
		
		fetch cu_righe_ordine into :ll_riga_ordine, 
		                           :ls_cod_tipo_det_ven_rif, 
											:ls_cod_prodotto_rif, 
											:ld_quan_ordine_rif, 
											:ll_anno_comm_riferita, 
											:ll_num_comm_riferita ;
		
		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in FETCH cursore cu_righe_ordine (uof_elimina_righe)~r~n" + sqlca.sqlerrtext
			close cu_righe_ordine;
			return -1
		end if
		
		
		// *** Michela 26/01/2006: se ib_riferite = true allora cancello le righe riferite
		//                         se ib_riferite = false allora metto a null la riga di appartenenza, in modo da cancellare il legame 
		//                         richiesto da ptenda specifica storico bolle versione 5 del 23/12/2005
		
		if ib_riferite then
		
		
			select flag_tipo_det_ven
			into   :ls_flag_tipo_det_ven
			from   tab_tipi_det_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_tipo_det_ven = :ls_cod_tipo_det_ven_rif;
		
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Si è verificato un errore in fase di lettura tipi dettaglio di vendita(uof_elimina_righe).~r~n" + sqlca.sqlerrtext
				return -1
			end if
		
			if ls_flag_tipo_det_ven = "M" then
		
				update anag_prodotti  
					set quan_impegnata = quan_impegnata - :ld_quan_ordine_rif  
				 where cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_prodotto = :ls_cod_prodotto_rif;
		
				if sqlca.sqlcode = -1 then
					fs_messaggio = "Si è verificato un errore in fase di aggiornamento magazzino(uof_elimina_righe).~r~n" + sqlca.sqlerrtext
					return -1
				end if
				
				// enme 08/1/2006 gestione prodotto raggruppato
				setnull(ls_cod_prodotto_raggruppato)
				
				select cod_prodotto_raggruppato
				into   :ls_cod_prodotto_raggruppato
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto_rif;
						 
				if not isnull(ls_cod_prodotto_raggruppato) then
					
					ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto_rif, ld_quan_ordine_rif, "M")
		
					update anag_prodotti  
						set quan_impegnata = quan_impegnata - :ld_quan_raggruppo
					 where cod_azienda = :s_cs_xx.cod_azienda and  
							 cod_prodotto = :ls_cod_prodotto_raggruppato;
			
					if sqlca.sqlcode = -1 then
						fs_messaggio = "Si è verificato un errore in fase di aggiornamento magazzino(uof_elimina_righe).~r~n" + sqlca.sqlerrtext
						return -1
					end if
					
				end if
				
			end if
			
			// cancello eventuali righe di appartenenza che hanno già creato bolla
			// se ci sono dei dettagli di bolla collegati a questa riga di ordine devo eseguire gli 
			// update. lo stock lo prendo dalla bolla. se ci sono + righe bolla per lo stesso ordine annullo tutto
			long   ll_ndet, ll_progr_stock
			string ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_cod_prodotto_appo
			datetime ldt_data_stock
			
			select count(*)
			into   :ll_ndet
			from   det_bol_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione_ord_ven = :fl_anno_registrazione and
					 num_registrazione_ord_ven = :fl_num_registrazione and
					 prog_riga_ord_ven = :ll_riga_ordine;
					 
			if sqlca.sqlcode < 0 then
				fs_messaggio = "Errore durante il controllo riferimenti ordine.~r~n" + sqlca.sqlerrtext
				return -1			
			end if
			
			if not isnull(ll_ndet) and ll_ndet > 0 then      // *** esistono bolle
			
				if ll_ndet > 1 then
					fs_messaggio = "Attenzione: questa riga d'ordine è evasa con più DDT.~r~nRif. Ordine:" + string(fl_anno_registrazione) + "/" + string(fl_num_registrazione) + "/" + string(ll_riga_ordine) + "~r~nOperazione interrotta."
					return -1							
				end if
				
				// *** leggo i dati dello stock
				
				select cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 progr_stock,
						 data_stock,
						 cod_prodotto
				into   :ls_cod_deposito,
						 :ls_cod_ubicazione,
						 :ls_cod_lotto,
						 :ll_progr_stock,
						 :ldt_data_stock,
						 :ls_cod_prodotto_appo
				from   det_bol_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione_ord_ven = :fl_anno_registrazione and
						 num_registrazione_ord_ven = :fl_num_registrazione and
						 prog_riga_ord_ven = :ll_riga_ordine;			
										 
				if sqlca.sqlcode <> 0 then
					fs_messaggio = "Errore durante lettura valori stock da bolla: " + sqlca.sqlerrtext
					return -1
				end if
				
				update anag_prodotti
				set    quan_in_spedizione = quan_in_spedizione - :ld_quan_ordine_rif,
						 quan_impegnata = quan_impegnata + :ld_quan_ordine_rif
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto_rif;
							 
				if sqlca.sqlcode <> 0 then
					fs_messaggio = "Errore durante aggiornamento valori prodotto: " +ls_cod_prodotto_rif+ "~r~n" + sqlca.sqlerrtext
					return -1
				end if
				
				// enme 08/1/2006 gestione prodotto raggruppato
				setnull(ls_cod_prodotto_raggruppato)
				
				select cod_prodotto_raggruppato
				into   :ls_cod_prodotto_raggruppato
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto_rif;
						 
				if not isnull(ls_cod_prodotto_raggruppato) then

					ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto_rif, ld_quan_ordine_rif, "M")

					update anag_prodotti
					set    quan_impegnata = quan_impegnata + :ld_quan_raggruppo
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_prodotto_raggruppato;
								 
					if sqlca.sqlcode <> 0 then
						fs_messaggio = "Errore durante aggiornamento valori prodotto: " +ls_cod_prodotto_raggruppato+ "~r~n" + sqlca.sqlerrtext
						return -1
					end if
				end if
								
				update stock
				set    quan_in_spedizione = quan_in_spedizione - :ld_quan_ordine_rif
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto_rif and
						 cod_deposito = :ls_cod_deposito and
						 cod_ubicazione = :ls_cod_ubicazione and
						 cod_lotto = :ls_cod_lotto and
						 prog_stock = :ll_progr_stock and
						 data_stock = :ldt_data_stock;
											 
				if sqlca.sqlcode <> 0 then
					fs_messaggio = "Errore durante aggiornamento valori prodotto: " + sqlca.sqlerrtext
					return -1
				end if		
				
				/// *** cancello la riga della bolla
				
				delete from det_bol_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione_ord_ven = :fl_anno_registrazione and
						 num_registrazione_ord_ven = :fl_num_registrazione and
						 prog_riga_ord_ven = :ll_riga_ordine;	
						 
				if sqlca.sqlcode <> 0 then
					fs_messaggio = "Errore durante cancellazione riga bolla: " + sqlca.sqlerrtext
					return -1
				end if							 
			
			end if
			// ------------------------------------------------------------		
			
			// cancello la commessa delle righe riferite  [EnMe 22/4/2003]
			
			if not isnull(ll_anno_comm_riferita) then
				luo_funzioni_commesse = CREATE uo_funzioni_1
				if luo_funzioni_commesse.uof_cancella_commessa(ll_anno_comm_riferita,ll_num_comm_riferita, fs_messaggio) = -1 then
					fs_messaggio = "Errore in cancellazione commessa riferita (uof_elimina_righe)~r~n" + fs_messaggio
					destroy luo_funzioni_commesse
					return -1
				end if
				
				delete from anag_commesse
				where  cod_azienda   = :s_cs_xx.cod_azienda and
						 anno_commessa = :ll_anno_comm_riferita and
						 num_commessa  = :ll_num_comm_riferita;
				if sqlca.sqlcode <> 0 then
					fs_messaggio = "Errore durante la cancellazione della commessa.~r~n" + sqlca.sqlerrtext
					return -1
				end if
				
				// *** Michela 26/01/2006: dopo aver cancellato la commessa elimino tutti i movimenti magazzino
				//                         ad essa collegati. su richiesta di Enrico per ptenda (specifica storico bolle)
				
				ll_ret = uof_elimina_mov_mag_commesse( ll_anno_comm_riferita, ll_num_comm_riferita, fs_messaggio)
				if ll_ret <> 0 then
					return -1
				end if
				
				// *** fine modifica			
				
				
			end if
			
			// -----------------------------------------------------------
			
			
	
			
			delete from det_ord_ven_note
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :fl_anno_registrazione and
					 num_registrazione  = :fl_num_registrazione and
					 prog_riga_ord_ven  = :ll_riga_ordine;
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore cancellazione delle note della riga ordine.~r~n" + sqlca.sqlerrtext
				return -1
			end if
		
			delete from det_ord_ven_corrispondenze
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :fl_anno_registrazione and
					 num_registrazione  = :fl_num_registrazione and
					 prog_riga_ord_ven  = :ll_riga_ordine;
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore cancellazione delle corrispondenze riga ordine.~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
			// stefanop: 28/01/2011: eliminio storicizzazione:
			delete from det_ord_ven_prod
			where cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :fl_anno_registrazione and
					 num_registrazione  = :fl_num_registrazione and
					 prog_riga_ord_ven  = :ll_riga_ordine;
					 
			if sqlca.sqlcode < 0 then
				fs_messaggio = "Errore cancellazione det_ord_ven_prod riga ordine.~r~n" + sqlca.sqlerrtext
				return -1
			end if
			// ----
		
			delete from det_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :fl_anno_registrazione and
					 num_registrazione  = :fl_num_registrazione and
					 prog_riga_ord_ven  = :ll_riga_ordine;
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore cancellazione del riga ordine.~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
			
		elseif ib_riferite = false then           // **************************** elimino il collegamento della riga riferita

			update det_ord_ven
			set    num_riga_appartenenza = 0 
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :fl_anno_registrazione and
					 num_registrazione = :fl_num_registrazione and
					 prog_riga_ord_ven = :ll_riga_ordine;
					 
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore durante la cancellazione del riferimento di riga:~r~n" + sqlca.sqlerrtext
				return -1
			end if				

		end if
	loop
	
	close cu_righe_ordine;

end if


//// -------------------------------- cancello la riga principale  ------------------------------------------------

select flag_tipo_det_ven
into   :ls_flag_tipo_det_ven
from   tab_tipi_det_ven
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_tipo_det_ven = :ls_cod_tipo_det_ven;

if sqlca.sqlcode = -1 then
	fs_messaggio = "Si è verificato un errore in fase di lettura tipi dettaglio di vendita.~r~n" + sqlca.sqlerrtext
	return -1
end if

if ls_flag_tipo_det_ven = "M" then

	update anag_prodotti  
		set quan_impegnata = quan_impegnata - :ld_quan_ordine  
	 where cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento impegnato magazzino.~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	// enme 08/1/2006 gestione prodotto raggruppato
	setnull(ls_cod_prodotto_raggruppato)
	
	select cod_prodotto_raggruppato
	into   :ls_cod_prodotto_raggruppato
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if not isnull(ls_cod_prodotto_raggruppato) then

		ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordine, "M")
		
		update anag_prodotti  
			set quan_impegnata = quan_impegnata - :ld_quan_raggruppo  
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_prodotto = :ls_cod_prodotto_raggruppato;
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Si è verificato un errore in fase di aggiornamento impegnato magazzino.~r~n" + sqlca.sqlerrtext
			return -1
		end if
	end if
	
end if


//donato 21/01/2013: elimino eventuali righe di sospensione produzione --------------------------------
delete from tab_sosp_produzione
where cod_azienda=:s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione and
		(prog_riga_ord_ven = :fl_prog_riga_ord_ven or prog_riga_ord_ven is null);
		
if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore cancellazione tab_sosp_produzione della riga ordine.~r~n" + sqlca.sqlerrtext
	return -1
end if
//-----------------------------------------------------------------------------------------------------------------

delete from det_ordini_produzione
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione and
		 prog_riga_ord_ven  = :fl_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore cancellazione det_ordini_produzione della riga ordine.~r~n" + sqlca.sqlerrtext
	return -1
end if

delete from det_ord_ven_note
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione and
		 prog_riga_ord_ven  = :fl_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore cancellazione det_ord_ven_note della riga ordine.~r~n" + sqlca.sqlerrtext
	return -1
end if

delete from det_ord_ven_corrispondenze
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione and
		 prog_riga_ord_ven  = :fl_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore cancellazione delle corrispondenze riga ordine.~r~n" + sqlca.sqlerrtext
	return -1
end if

// stefanop: 28/01/2011: eliminio storicizzazione:
delete from det_ord_ven_prod
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione and
		 prog_riga_ord_ven  = :ll_riga_ordine;
		 
if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore cancellazione det_ord_ven_prod riga ordine.~r~n" + sqlca.sqlerrtext
	return -1
end if
// ----


//elimino eventuali ologrammi
luo_produzione = create uo_produzione
ll_errore = luo_produzione.uof_elimina_ologrammi(fl_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ref fs_messaggio)
destroy luo_produzione
if ll_errore < 0 then
	//in fs_messaggio l'errore
	return -1
elseif ll_errore=1 then
	//operazione annullata
	return -1
end if



delete from det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione and
		 prog_riga_ord_ven  = :fl_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore cancellazione della riga ordine.~r~n" + sqlca.sqlerrtext
	return -1
end if

//pulisci eventuali dati dei colli --------------------------------------------------
delete from tab_ord_ven_colli
where  	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :fl_anno_registrazione and
			num_registrazione = :fl_num_registrazione and
			prog_riga_ord_ven=:fl_prog_riga_ord_ven;

if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore cancellazione tab_ord_ven_colli.~r~n" + sqlca.sqlerrtext
	return -1
end if
//---------------------------------------------------------------------------------------
	
return 0
end function

public function integer uof_storicizza_det_ord_ven (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, long fl_prog_storico, long fl_righe_escluse[], ref string fs_messaggio);boolean lb_salta_riga
string ls_cod_cliente,ls_cod_giro_consegna, ls_des_prodotto, ls_cod_prodotto_riga
long   ll_prog_riga_ord_ven, ll_i
double ld_quan_ordine, ld_imponibile_iva, ld_valore_storico, ld_nuovo_imponibile_iva, ld_prezzo_vendita, ld_quan_vendita, &
       ld_fat_conversione, ld_prezzo_um, ld_nuovo_imponibile_riga
datetime ldt_data_registrazione, ldt_data_documento


select cod_cliente, 
       data_registrazione,
		 cod_giro_consegna
into   :ls_cod_cliente,
       :ldt_data_registrazione,
		 :ls_cod_giro_consegna
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca ordine nella funzione (uof_storicizza_det_ord_ven).~r~n" + sqlca.sqlerrtext
	return -1
end if

// aggiunto per Viropa 28/02/2008; questo campo deve rappresentare la data in cui viene effettuata l'operazione di storicizzazione.
ldt_data_documento = datetime(today(),00:00:00)

// aggiunto per Viropa 06/10/2009; carico la descrizione riga se presente altrimenti la descrizione del magazzino

setnull(ls_des_prodotto)

select cod_prodotto, des_prodotto
into   :ls_cod_prodotto_riga, :ls_des_prodotto
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in storicizzazione dettaglio ordine~r~nErrore in ricerca riga d'ordine~r~n" + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_des_prodotto) or len(ls_des_prodotto) < 1 then

	if not isnull(ls_cod_prodotto_riga) and len(ls_cod_prodotto_riga) > 0 then
		
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		 		 cod_prodotto = :ls_cod_prodotto_riga;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in storicizzazione dettaglio ordine~r~nErrore in ricerca prodotto in anagrafica~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
	end if
	
end if

// fine modifica Viropa 06/10/2009


insert into det_ord_ven_storico  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           prog_riga_ord_ven,   
           prog_storico,   
           cod_cliente,   
           cod_prodotto,   
           cod_misura,   
           quan_ordine,   
           prezzo_vendita,   
           fat_conversione_ven,   
           sconto_1,   
           sconto_2,   
           sconto_3,   
           sconto_4,   
           sconto_5,   
           sconto_6,   
           sconto_7,   
           sconto_8,   
           sconto_9,   
           sconto_10,   
           provvigione_1,   
           provvigione_2,   
           nota_dettaglio,   
           cod_versione,   
           num_riga_appartenenza,   
           quantita_um,   
           prezzo_um,   
           imponibile_iva,   
           nota_prodotto,   
           data_registrazione,
			  data_documento,
			  cod_giro_consegna,
			  des_prodotto)  
select      det_ord_ven.cod_azienda,   
            det_ord_ven.anno_registrazione,   
            det_ord_ven.num_registrazione,   
            det_ord_ven.prog_riga_ord_ven,   
            :fl_prog_storico,   
            :ls_cod_cliente,   
            det_ord_ven.cod_prodotto,   
            det_ord_ven.cod_misura,   
            det_ord_ven.quan_ordine,   
            det_ord_ven.prezzo_vendita,   
            det_ord_ven.fat_conversione_ven,   
            det_ord_ven.sconto_1,   
            det_ord_ven.sconto_2,   
            det_ord_ven.sconto_3,   
            det_ord_ven.sconto_4,   
            det_ord_ven.sconto_5,   
            det_ord_ven.sconto_6,   
            det_ord_ven.sconto_7,   
            det_ord_ven.sconto_8,   
            det_ord_ven.sconto_9,   
            det_ord_ven.sconto_10,   
            det_ord_ven.provvigione_1,   
            det_ord_ven.provvigione_2,   
            det_ord_ven.nota_dettaglio,   
            det_ord_ven.cod_versione,   
            det_ord_ven.num_riga_appartenenza,   
            det_ord_ven.quantita_um,   
            det_ord_ven.prezzo_um,   
            det_ord_ven.imponibile_iva,   
            det_ord_ven.nota_prodotto,   
            :ldt_data_registrazione,
            :ldt_data_documento,
				:ls_cod_giro_consegna,
				:ls_des_prodotto
from        det_ord_ven
where       cod_azienda = :s_cs_xx.cod_azienda and
            anno_registrazione = :fl_anno_registrazione and
		      num_registrazione = :fl_num_registrazione and
		      prog_riga_ord_ven = :fl_prog_riga_ord_ven;


if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in storicizzazione dettaglio ordine~r~n" + sqlca.sqlerrtext
	return -1
end if

// gestione del prezzo della riga storicizzata
if is_flag_ar = "S" then
	
	select quan_ordine, imponibile_iva, fat_conversione_ven
	into   :ld_quan_ordine, :ld_imponibile_iva, :ld_fat_conversione
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in storicizzazione dettaglio ordine~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	ld_valore_storico = round(ld_imponibile_iva - ( id_valore_storico * ld_quan_ordine), 2)
	ld_prezzo_vendita = round(ld_valore_storico / ld_quan_ordine, 2)
	
	if ld_fat_conversione <> 1 then
		ld_prezzo_um = round(ld_prezzo_vendita / ld_fat_conversione, 2)
	else
		ld_prezzo_um = 0
	end if
	
	update det_ord_ven_storico
	set    cod_prodotto   = :is_cod_prodotto_ar,
	       imponibile_iva = :ld_valore_storico,
	       prezzo_vendita = :ld_prezzo_vendita,
			 prezzo_um      = :ld_prezzo_um,
			 sconto_1       = 0,
			 sconto_2       = 0,
			 sconto_3       = 0,
			 sconto_4       = 0,
			 sconto_5       = 0,
			 sconto_6       = 0,
			 sconto_7       = 0,
			 sconto_8       = 0,
			 sconto_9       = 0,
			 sconto_10      = 0
	where  cod_azienda = :s_cs_xx.cod_azienda and
	   	 anno_registrazione = :fl_anno_registrazione and
          num_registrazione = :fl_num_registrazione and
          prog_riga_ord_ven =:fl_prog_riga_ord_ven and
          prog_storico = :fl_prog_storico ;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in aggiornamento dettaglio ordine.~r~n" + sqlca.sqlerrtext
		return -1
	end if
			 
	ld_nuovo_imponibile_iva = round(id_valore_storico * ld_quan_ordine, 2)
	
	ld_prezzo_vendita = round(ld_nuovo_imponibile_iva / ld_quan_ordine, 2)
	
	if ld_fat_conversione <> 1 then
		ld_prezzo_um = round(ld_prezzo_vendita / ld_fat_conversione, 2)
	else
		ld_prezzo_um = 0
	end if
	
	ld_nuovo_imponibile_riga = ld_quan_ordine * id_valore_storico
			 
	update det_ord_ven
	set    imponibile_iva = :ld_nuovo_imponibile_riga,
			 val_riga       = :ld_nuovo_imponibile_riga,
	       prezzo_vendita = :ld_prezzo_vendita,
			 prezzo_um      = :ld_prezzo_um,
			 sconto_1       = 0,
			 sconto_2       = 0,
			 sconto_3       = 0,
			 sconto_4       = 0,
			 sconto_5       = 0,
			 sconto_6       = 0,
			 sconto_7       = 0,
			 sconto_8       = 0,
			 sconto_9       = 0,
			 sconto_10      = 0
	where  cod_azienda = :s_cs_xx.cod_azienda and
	   	 anno_registrazione = :fl_anno_registrazione and
          num_registrazione = :fl_num_registrazione and
          prog_riga_ord_ven =:fl_prog_riga_ord_ven ;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in aggiornamento dettaglio ordine ufficiale~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	return 0
	
end if


declare cu_righe_riferite cursor for
select prog_riga_ord_ven
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 num_riga_appartenenza = :fl_prog_riga_ord_ven;
		 
open cu_righe_riferite;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore OPEN cursore cu_righe_riferite (uof_storicizza_det_ord_ven)~r~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_righe_riferite into :ll_prog_riga_ord_ven;
	
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then
		fs_messaggio = "Errore FETCH cursore cu_righe_riferite (uof_storicizza_det_ord_ven)~r~n" + sqlca.sqlerrtext
		close cu_righe_riferite;
		return -1
	end if
	
	lb_salta_riga = false
	
	for ll_i = 1 to upperbound(fl_righe_escluse)
		if not isnull(fl_righe_escluse[ll_i]) and fl_righe_escluse[ll_i] > 0 then
			// se la riga è esclusa dalla storicizzazione la salto
			if fl_righe_escluse[ll_i] = ll_prog_riga_ord_ven then
				lb_salta_riga = true
				exit
			end if
		end if
	next
	
	if lb_salta_riga then continue
	
	
	// aggiunto per Viropa 06/10/2009; carico la descrizione riga se presente altrimenti la descrizione del magazzino
	
	setnull(ls_des_prodotto)
	
	select cod_prodotto, des_prodotto
	into   :ls_cod_prodotto_riga, :ls_des_prodotto
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in storicizzazione dettaglio ordine~r~nErrore in ricerca riga d'ordine~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ls_des_prodotto) or len(ls_des_prodotto) < 1 then
	
		if not isnull(ls_cod_prodotto_riga) and len(ls_cod_prodotto_riga) > 0 then
			
			select des_prodotto
			into   :ls_des_prodotto
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto_riga;
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore in storicizzazione dettaglio ordine~r~nErrore in ricerca prodotto in anagrafica~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
		end if
		
	end if
	
	// fine modifica Viropa 06/10/2009
	
	

	insert into det_ord_ven_storico  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  prog_riga_ord_ven,   
				  prog_storico,   
				  cod_cliente,   
				  cod_prodotto,   
				  cod_misura,   
				  quan_ordine,   
				  prezzo_vendita,   
				  fat_conversione_ven,   
				  sconto_1,   
				  sconto_2,   
				  sconto_3,   
				  sconto_4,   
				  sconto_5,   
				  sconto_6,   
				  sconto_7,   
				  sconto_8,   
				  sconto_9,   
				  sconto_10,   
				  provvigione_1,   
				  provvigione_2,   
				  nota_dettaglio,   
				  cod_versione,   
				  num_riga_appartenenza,   
				  quantita_um,   
				  prezzo_um,   
				  imponibile_iva,   
				  nota_prodotto,   
				  data_registrazione,
				  data_documento,
				  cod_giro_consegna,
				  des_prodotto)  
	select      det_ord_ven.cod_azienda,   
					det_ord_ven.anno_registrazione,   
					det_ord_ven.num_registrazione,   
					det_ord_ven.prog_riga_ord_ven,   
					:fl_prog_storico,   
					:ls_cod_cliente,   
					det_ord_ven.cod_prodotto,   
					det_ord_ven.cod_misura,   
					det_ord_ven.quan_ordine,   
					det_ord_ven.prezzo_vendita,   
					det_ord_ven.fat_conversione_ven,   
					det_ord_ven.sconto_1,   
					det_ord_ven.sconto_2,   
					det_ord_ven.sconto_3,   
					det_ord_ven.sconto_4,   
					det_ord_ven.sconto_5,   
					det_ord_ven.sconto_6,   
					det_ord_ven.sconto_7,   
					det_ord_ven.sconto_8,   
					det_ord_ven.sconto_9,   
					det_ord_ven.sconto_10,   
					det_ord_ven.provvigione_1,   
					det_ord_ven.provvigione_2,   
					det_ord_ven.nota_dettaglio,   
					det_ord_ven.cod_versione,   
					det_ord_ven.num_riga_appartenenza,   
					det_ord_ven.quantita_um,   
					det_ord_ven.prezzo_um,   
					det_ord_ven.imponibile_iva,   
					det_ord_ven.nota_prodotto,   
					:ldt_data_registrazione,
					:ldt_data_registrazione,
					:ls_cod_giro_consegna,
					:ls_des_prodotto
	from        det_ord_ven
	where       cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :fl_anno_registrazione and
					num_registrazione = :fl_num_registrazione and
					prog_riga_ord_ven = :ll_prog_riga_ord_ven;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in storicizzazione dettaglio ordine~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
loop


close cu_righe_riferite;

return 0
end function

public function integer uof_storicizza_comp_det_ord_ven (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, long fl_prog_storico, string fs_flag_riga_originale, ref string fs_messaggio);string ls_cod_prodotto

select cod_modello
into   :ls_cod_prodotto
from   comp_det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione  and
		 prog_riga_ord_ven  = :fl_prog_riga_ord_ven;
		 
if sqlca.sqlcode = 100 then
	// è un prodotto sfuso, non creato tramite il configuratore
	return 0
end if

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in storicizzazione dettaglio complementare~r~n" + sqlca.sqlerrtext
	return -1
end if

if is_flag_ar = "S" then
	
	ls_cod_prodotto = is_cod_prodotto_origine
	
end if

INSERT INTO comp_det_ord_ven_storico  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           prog_riga_ord_ven,   
           flag_riga_originale,   
           prog_storico,   
           cod_non_a_magazzino,   
           cod_prod_finito,   
           cod_modello,   
           dim_x,   
           dim_y,   
           dim_z,   
           dim_t,   
           cod_tessuto,   
           cod_tessuto_mant,   
           flag_tipo_mantovana,   
           alt_mantovana,   
           flag_cordolo,   
           flag_passamaneria,   
           cod_verniciatura,   
           cod_comando,   
           pos_comando,   
           alt_asta,   
           cod_comando_tenda_verticale,   
           cod_colore_lastra,   
           cod_tipo_lastra,   
           spes_lastra,   
           cod_tipo_supporto,   
           flag_cappotta_frontale,   
           flag_rollo,   
           flag_kit_laterale,   
           flag_misura_luce_finita,   
           cod_tipo_stampo,   
           cod_mat_sis_stampaggio,   
           rev_stampo,   
           num_gambe,   
           num_campate,   
           note,   
           note_esterne,   
           flag_autoblocco,   
           num_fili_plisse,   
           num_boccole_plisse,   
           intervallo_punzoni_plisse,   
           num_pieghe_plisse,   
           prof_inf_plisse,   
           prof_sup_plisse,   
           colore_passamaneria,   
           colore_cordolo,   
           num_punzoni_plisse,   
           des_vernice_fc,   
           des_comando_1,   
           cod_comando_2,   
           des_comando_2,   
           flag_addizionale_comando_1,   
           flag_addizionale_comando_2,   
           flag_addizionale_supporto,   
           flag_addizionale_tessuto,   
           flag_addizionale_vernic,   
           flag_fc_comando_1,   
           flag_fc_comando_2,   
           flag_fc_supporto,   
           flag_fc_tessuto,   
           flag_fc_vernic,   
           flag_allegati,   
           data_ora_ar,   
           flag_tessuto_cliente,   
           flag_tessuto_mant_cliente,   
           flag_ar )  
SELECT    cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           prog_riga_ord_ven,   
           :fs_flag_riga_originale,   
           :fl_prog_storico,   
           cod_non_a_magazzino,   
           cod_prod_finito,   
           :ls_cod_prodotto,   
           dim_x,   
           dim_y,   
           dim_z,   
           dim_t,   
           cod_tessuto,   
           cod_tessuto_mant,   
           flag_tipo_mantovana,   
           alt_mantovana,   
           flag_cordolo,   
           flag_passamaneria,   
           cod_verniciatura,   
           cod_comando,   
           pos_comando,   
           alt_asta,   
           cod_comando_tenda_verticale,   
           cod_colore_lastra,   
           cod_tipo_lastra,   
           spes_lastra,   
           cod_tipo_supporto,   
           flag_cappotta_frontale,   
           flag_rollo,   
           flag_kit_laterale,   
           flag_misura_luce_finita,   
           cod_tipo_stampo,   
           cod_mat_sis_stampaggio,   
           rev_stampo,   
           num_gambe,   
           num_campate,   
           note,   
           note_esterne,   
           flag_autoblocco,   
           num_fili_plisse,   
           num_boccole_plisse,   
           intervallo_punzoni_plisse,   
           num_pieghe_plisse,   
           prof_inf_plisse,   
           prof_sup_plisse,   
           colore_passamaneria,   
           colore_cordolo,   
           num_punzoni_plisse,   
           des_vernice_fc,   
           des_comando_1,   
           cod_comando_2,   
           des_comando_2,   
           flag_addizionale_comando_1,   
           flag_addizionale_comando_2,   
           flag_addizionale_supporto,   
           flag_addizionale_tessuto,   
           flag_addizionale_vernic,   
           flag_fc_comando_1,   
           flag_fc_comando_2,   
           flag_fc_supporto,   
           flag_fc_tessuto,   
           flag_fc_vernic,   
           flag_allegati,   
           data_ora_ar,   
           flag_tessuto_cliente,   
           flag_tessuto_mant_cliente,   
           :is_flag_ar	
from       comp_det_ord_ven
where      cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :fl_anno_registrazione and
			  num_registrazione  = :fl_num_registrazione  and
			  prog_riga_ord_ven  = :fl_prog_riga_ord_ven;
			  
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in storicizzazione dettaglio complementare~r~n" + sqlca.sqlerrtext
	return -1
end if

return 0
end function

public function integer uof_storicizza_det_ord_ven_mp_ar (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, long fl_prog_storico, string fs_cod_materia_prima[], double fd_quan_utilizzo[], ref string fs_messaggio);boolean lb_salta
string ls_cod_prodotto, ls_cod_versione, ls_cod_misura_mag
long   ll_i, ll_y, ll_cont_mp, ll_progressivo
double ld_quan_ordine

select cod_prodotto,
       cod_versione,
		 quan_ordine
into   :ls_cod_prodotto,
       :ls_cod_versione,
		 :ld_quan_ordine
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca riga ordine in funzione (uof_storicizza_det_ord_ven_mp).~r~n" + sqlca.sqlerrtext
	return -1
end if

ll_progressivo  = 0
ll_cont_mp = upperbound(fs_cod_materia_prima)

// se ci sono materie prime
if ll_cont_mp > 0 and not isnull(ll_cont_mp) then
	for ll_i = 1 to ll_cont_mp
		
		ll_progressivo ++
		
		select cod_misura_mag
		into   :ls_cod_misura_mag
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :fs_cod_materia_prima[ll_i];
				 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca materia prima a magazzino (uof_storicizza_det_ord_ven_mp_ar).~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		INSERT INTO det_ord_ven_storico_mp  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  prog_riga_ord_ven,   
				  prog_storico,   
				  progressivo,   
				  cod_prodotto,   
				  quan_utilizzo,   
				  cod_misura )  
		VALUES (:s_cs_xx.cod_azienda,   
				  :fl_anno_registrazione,   
				  :fl_num_registrazione,   
				  :fl_prog_riga_ord_ven,   
				  :fl_prog_storico,   
				  :ll_progressivo,   
				  :fs_cod_materia_prima[ll_i],   
				  :fd_quan_utilizzo[ll_i],   
				  :ls_cod_misura_mag);
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in creazione storico mp (uof_storicizza_det_ord_ven_mp).~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
	next
	
end if


if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in storicizzazione materie prime AR.~r~n" + sqlca.sqlerrtext
	return -1
end if


return 0
end function

public function integer uof_elimina_riga_bol_ven (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_registrazione, ref string fs_messaggio);// *** Michela 18/01/2006: nuova funzione per la specifica storico bolle di ptenda.  

delete det_bol_ven  
where  det_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
       det_bol_ven.anno_registrazione = :fl_anno_registrazione and
       det_bol_ven.num_registrazione = :fl_num_registrazione and  
       det_bol_ven.prog_riga_bol_ven = :fl_prog_riga_registrazione ;
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore durante la cancellazione della riga di dettaglio della bolla:~r~n" + sqlca.sqlerrtext
	return -1
end if
fs_messaggio = ""
return 0
end function

public function integer uof_rinumera_righe_bol_ven (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio);// *** Michela 18/01/2006: nuova funzione per la specifica storico bolle di ptenda.  
long ll_prog_riga_bol_ven, ll_prog_righe[], ll_i, ll_new

declare cu_righe cursor for
select prog_riga_bol_ven
from   det_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione
order by prog_riga_bol_ven ASC;
		 
open cu_righe;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore durante l'apertura del cursore delle righe della bolla:~r~n" + sqlca.sqlerrtext
	return -1
end if

ll_i = 0
do while 1 = 1
	fetch cu_righe into :ll_prog_riga_bol_ven;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then 
		fs_messaggio = "Errore durante la fetch del cursore cu_righe:~r~n" + sqlca.sqlerrtext
		close cu_righe;
		return -1
	end if
	ll_i ++
	ll_prog_righe[ll_i] = ll_prog_riga_bol_ven
loop

close cu_righe;		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore durante la chiusura del cursore cu_righe:~r~n" + sqlca.sqlerrtext
	return -1
end if

ll_new = 10
for ll_i = 1 to Upperbound(ll_prog_righe)
	
	update det_bol_ven
	set    prog_riga_bol_ven = :ll_new
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_bol_ven = :ll_prog_righe[ll_i];
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento riga bolla:~r~n" + sqlca.sqlerrtext
		return -1
	end if
	ll_new = ll_new + 10
	
next

fs_messaggio = ""
return 0
end function

public function integer uof_storicizza_riga_bol_ven (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_bol_ven, ref string fs_errore);string  ls_errore, ls_storico, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_cod_prodotto, ls_mp_escluse[], ls_testo_esclusioni, &
        ls_messaggio,ls_cod_prodotto_raggruppato
long    ll_ret, ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven, ll_progr_stock, ll_anno_registrazione_ord_ven, &
        ll_num_registrazione_ord_ven, ll_prog_riga_ord_ven,ll_righe_escluse[], ll_count_aperti, ll_count_parziali, ll_anno_commessa, ll_num_commessa, ll_count
boolean lb_trovato
datetime ldt_data_stock
dec{4}  ld_quan_consegnata, ld_quan_evasa, ld_quan_ordine
uo_generazione_documenti luo_obj

ll_anno_registrazione = fl_anno_registrazione
ll_num_registrazione = fl_num_registrazione
ll_prog_riga_bol_ven = fl_prog_riga_bol_ven

select cod_prodotto,
       quan_consegnata,
		 cod_deposito,
		 cod_lotto,
		 progr_stock,
		 data_stock,
		 anno_registrazione_ord_ven,
		 num_registrazione_ord_ven,
		 prog_riga_ord_ven,
		 anno_commessa,
		 num_commessa
into  :ls_cod_prodotto,
		:ld_quan_consegnata,
		:ls_cod_deposito,
		:ls_cod_ubicazione,
		:ll_progr_stock,
		:ldt_data_stock,
		:ll_anno_registrazione_ord_ven,
		:ll_num_registrazione_ord_ven,
		:ll_prog_riga_ord_ven,
		:ll_anno_commessa,
		:ll_num_commessa
from  det_bol_ven		
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :ll_anno_registrazione and
      num_registrazione = :ll_num_registrazione and
		prog_riga_bol_ven = :ll_prog_riga_bol_ven;
		
		
// *** michela 18/01/2006: sistemo il magazzino (funzione 3 della specifica storico bolle di ptenda)
//     in questo punto devo decrementare la quantità spedita (prima di elimare la riga) e aumentare la quantità
//     impegnata (che viene poi decrementata quando storicizzo l'ordine)
			
if not isnull(ls_cod_prodotto) and ls_cod_prodotto <> "" then
				
	update anag_prodotti
	set    quan_in_spedizione = quan_in_spedizione - :ld_quan_consegnata,
	       quan_impegnata = quan_impegnata + :ld_quan_consegnata
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto;
			 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante aggiornamento valori prodotto: " +ls_cod_prodotto+ "~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	// enme 08/1/2006 gestione prodotto raggruppato
	setnull(ls_cod_prodotto_raggruppato)
	
	select cod_prodotto_raggruppato
	into   :ls_cod_prodotto_raggruppato
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if not isnull(ls_cod_prodotto_raggruppato) then
		update anag_prodotti
		set    quan_impegnata = quan_impegnata + :ld_quan_consegnata
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_raggruppato;
				 
		if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante aggiornamento valori prodotto: " +ls_cod_prodotto_raggruppato+ "~r~n" + sqlca.sqlerrtext
			return -1
		end if
	end if
	
			
	update stock
	set    quan_in_spedizione = quan_in_spedizione - :ld_quan_consegnata
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto and
			 cod_deposito = :ls_cod_deposito and
			 cod_ubicazione = :ls_cod_ubicazione and
			 cod_lotto = :ls_cod_lotto and
			 prog_stock = :ll_progr_stock and
			 data_stock = :ldt_data_stock;
						 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante aggiornamento valori prodotto: " + sqlca.sqlerrtext
		return -1
	end if						 
end if
			
// *** se esiste il riferimento all'ordine di vendita devo sistemare tutte le quantità ed eventualmente 
//     storicizzare l'ordine

if not isnull(ll_anno_registrazione_ord_ven) and not isnull(ll_num_registrazione_ord_ven) and not isnull(ll_prog_riga_ord_ven) and ll_anno_registrazione_ord_ven > 0 and ll_num_registrazione_ord_ven > 0 and ll_prog_riga_ord_ven > 0 then
			
	// *** aggiorno la quantità evasa dell'ordine, togliendo quella della bolla
			
	update det_ord_ven
	set    quan_evasa = quan_evasa - :ld_quan_consegnata
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione_ord_ven and
			 num_registrazione = :ll_num_registrazione_ord_ven and
			 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
			 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante aggiornamento valori ordine: " + sqlca.sqlerrtext
		return -1
	end if		
		
	// *** se l'ordine ha quantità evasa > 0 allora devo cambiare il flag evasione
		
	select quan_evasa,
			 quan_ordine
	into   :ld_quan_evasa,
			 :ld_quan_ordine
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione_ord_ven and
			 num_registrazione = :ll_num_registrazione_ord_ven and
			 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
				 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante aggiornamento valori ordine: " + sqlca.sqlerrtext
		return -1
	end if		
		
	if not isnull(ld_quan_evasa) and ld_quan_evasa > 0 and ld_quan_evasa < ld_quan_ordine then // flag a parziale
	
		// *** nel caso in cui l'ordine sia parziale, vuol dire che esiste una bolla collegata quindi non posso storicizzare
		//     l'ordine, quindi elimino solo le righe della bolla
	
		update det_ord_ven
		set    flag_evasione = 'P'
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione_ord_ven and
				 num_registrazione = :ll_num_registrazione_ord_ven and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
						 
		ll_ret = uof_elimina_riga_bol_ven( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven, ref ls_errore)
		if ll_ret < 0 then
			fs_errore = ls_errore
			return -1
		end if	
		
		// aggiorno l'ordine
		luo_obj = create uo_generazione_documenti		
		ll_ret = luo_obj.uof_calcola_stato_ordine( ll_anno_registrazione_ord_ven, ll_num_registrazione_ord_ven)
		if ll_ret < 0 then
			destroy luo_obj
			return -1
		end if
		destroy luo_obj
					 
	elseif ld_quan_evasa = 0 or isnull(ld_quan_evasa) then // aperto
					
		// *** nel caso in cui l'ordine sia aperto, vuol dire che non è stato evaso con nessuna altra bolla, quindi lo posso storicizzare
		//     ovviamente dopo aver cancellato la riga della bolla
			
		update det_ord_ven
		set    flag_evasione = 'A'
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione_ord_ven and
				 num_registrazione = :ll_num_registrazione_ord_ven and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;	
				 
				 
					 
		ll_ret = uof_elimina_riga_bol_ven( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven, ref ls_errore)
		if ll_ret < 0 then
			fs_errore = ls_errore
			return -1
		end if	
		
		// *** controllo la commessa
		if not isnull(ll_anno_commessa) and ll_anno_commessa > 0 and not isnull(ll_num_commessa) and ll_num_commessa> 0 then

			select count(*)
			into   :ll_count
			from   det_bol_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_commessa = :ll_anno_commessa and
					 num_commessa = :ll_num_commessa;
					 
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante controllo commessa: " + sqlca.sqlerrtext
				return -1
			end if
			
			if ll_count > 0 and not isnull(ll_count) then
				fs_errore = "Attenzione: la riga di bolla " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "/" + string(ll_prog_riga_ord_ven) + " fa riferimento ad una commessa distribuita in più DDT.~r~nProcedere manualmente."
				return -1
			end if
			
		end if
	
		il_anno_reg_bol_ven = fl_anno_registrazione
		il_num_reg_bol_ven = fl_num_registrazione
		ll_ret = uof_storicizza(ll_anno_registrazione_ord_ven, ll_num_registrazione_ord_ven, ll_prog_riga_ord_ven, true, ls_mp_escluse[], ll_righe_escluse[], ref ls_testo_esclusioni, ref ls_messaggio )
				
		if ll_ret < 0 then
			fs_errore = ls_messaggio
			return -1
		end if	
		
		// aggiorno l'ordine		
		luo_obj = create uo_generazione_documenti		
		ll_ret = luo_obj.uof_calcola_stato_ordine( ll_anno_registrazione_ord_ven, ll_num_registrazione_ord_ven)
		if ll_ret < 0 then
			destroy luo_obj
			return -1
		end if		
		destroy luo_obj
		
	else               // ***** cancello la riga della bolla
		
		ll_ret = uof_elimina_riga_bol_ven( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven, ref ls_errore)
		if ll_ret < 0 then
			fs_errore = ls_errore
			return -1
		end if			
				
	end if	

else                  // *** se non esiste l'ordine elimino solo la riga della bolla
	
	ll_ret = uof_elimina_riga_bol_ven( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven, ref ls_errore)
	if ll_ret < 0 then
		fs_errore = ls_errore
		return -1
	end if				
			
end if			

return 0
end function

public function integer uof_elimina_mov_mag_commesse (long fl_anno_commessa, long fl_num_commessa, ref string fs_errore);long						ll_anno_registrazione[], ll_num_registrazione[], ll_ret, ll_rows, ll_i
datastore				lds_mov_mag
uo_magazzino			luo_mag

lds_mov_mag = CREATE datastore
lds_mov_mag.dataobject = 'd_ds_storicizzazione_mov_mag'
lds_mov_mag.settransobject(sqlca)

ll_rows = lds_mov_mag.retrieve(s_cs_xx.cod_azienda, fl_anno_commessa,fl_num_commessa)
if ll_rows < 0 then
	fs_errore = "Errore durante ricerca movimenti magazzino (d_ds_storicizzazione_mov_mag) "
	destroy lds_mov_mag
	return -1
end if

if ib_bolla then
	
	for ll_i = 1 to ll_rows
		
		ll_anno_registrazione[ll_i] = lds_mov_mag.getitemnumber(ll_i, "anno_registrazione")
		ll_num_registrazione[ll_i]  = lds_mov_mag.getitemnumber(ll_i, "num_registrazione")
		
		INSERT INTO buffer_mov_mag  
				( cod_azienda,   
				  anno_reg_bol_ven,   
				  num_reg_bol_ven,   
				  anno_reg_mov_mag,   
				  num_reg_mov_mag )  
		VALUES (:s_cs_xx.cod_azienda,   
				  :il_anno_reg_bol_ven,   
				  :il_num_reg_bol_ven,   
				  :ll_anno_registrazione[ll_i],   
				  :ll_num_registrazione[ll_i])  ;
			
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore buffer_mov_mag del movimento di magazzino:~r~n" + sqlca.sqlerrtext
			destroy lds_mov_mag
			rollback;
			return -1
		end if
	next
	
	destroy lds_mov_mag
	
	for ll_i = 1 to ll_rows
		luo_mag = create uo_magazzino
		ll_ret = luo_mag.uof_elimina_movimenti(ll_anno_registrazione[ll_i], ll_num_registrazione[ll_i], false)
		destroy luo_mag
		
		if ll_ret <> 0 then
			fs_errore = "Errore durante la cancellazione del movimento di magazzino " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + ":~r~n" + sqlca.sqlerrtext
			destroy lds_mov_mag
			rollback;
			return -1
		end if
	next
	
else
	
	for ll_i = 1 to ll_rows
		ll_anno_registrazione[ll_i] = lds_mov_mag.getitemnumber(ll_i, "anno_registrazione")
		ll_num_registrazione[ll_i]  = lds_mov_mag.getitemnumber(ll_i, "num_registrazione")
	next
	
	destroy lds_mov_mag
	
	for ll_i = 1 to ll_rows
		luo_mag = create uo_magazzino
		ll_ret = luo_mag.uof_elimina_movimenti(ll_anno_registrazione[ll_i], ll_num_registrazione[ll_i], true)
		destroy luo_mag
		
		if ll_ret <> 0 then
			fs_errore = "Errore durante la cancellazione del movimento di magazzino " + string(ll_anno_registrazione[ll_i]) + "/" + string(ll_num_registrazione[ll_i]) + ":~r~n" + sqlca.sqlerrtext
			rollback;
			return -1
		end if
	next
	
end if

return 0

end function

public function integer uof_storicizza_riga_bol_ven_parziale (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_bol_ven, string fs_mp_escluse[], long fl_righe_escluse[], ref string fs_testo_esclusioni, ref string fs_errore);string  ls_errore, ls_storico, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_cod_prodotto, ls_messaggio,ls_cod_prodotto_raggruppato
long    ll_ret, ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven, ll_progr_stock, ll_anno_registrazione_ord_ven, &
        ll_num_registrazione_ord_ven, ll_prog_riga_ord_ven, ll_count_aperti, ll_count_parziali, ll_anno_commessa, ll_num_commessa, ll_count
boolean lb_trovato
datetime ldt_data_stock
dec{4}  ld_quan_consegnata, ld_quan_evasa, ld_quan_ordine, ld_quan_raggruppo
uo_generazione_documenti luo_obj

ll_anno_registrazione = fl_anno_registrazione
ll_num_registrazione = fl_num_registrazione
ll_prog_riga_bol_ven = fl_prog_riga_bol_ven

select cod_prodotto,
       quan_consegnata,
		 cod_deposito,
		 cod_lotto,
		 progr_stock,
		 data_stock,
		 anno_registrazione_ord_ven,
		 num_registrazione_ord_ven,
		 prog_riga_ord_ven,
		 anno_commessa,
		 num_commessa
into  :ls_cod_prodotto,
		:ld_quan_consegnata,
		:ls_cod_deposito,
		:ls_cod_ubicazione,
		:ll_progr_stock,
		:ldt_data_stock,
		:ll_anno_registrazione_ord_ven,
		:ll_num_registrazione_ord_ven,
		:ll_prog_riga_ord_ven,
		:ll_anno_commessa,
		:ll_num_commessa
from  det_bol_ven		
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :ll_anno_registrazione and
      num_registrazione = :ll_num_registrazione and
		prog_riga_bol_ven = :ll_prog_riga_bol_ven;
		
		
// *** michela 18/01/2006: sistemo il magazzino (funzione 3 della specifica storico bolle di ptenda)
//     in questo punto devo decrementare la quantità spedita (prima di elimare la riga) e aumentare la quantità
//     impegnata (che viene poi decrementata quando storicizzo l'ordine)
			
if not isnull(ls_cod_prodotto) and ls_cod_prodotto <> "" then
				
	update anag_prodotti
	set    quan_in_spedizione = quan_in_spedizione - :ld_quan_consegnata,
	       quan_impegnata = quan_impegnata + :ld_quan_consegnata
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto;
			 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante aggiornamento valori prodotto: " + sqlca.sqlerrtext
		return -1
	end if
	
	// enme 08/1/2006 gestione prodotto raggruppato
	setnull(ls_cod_prodotto_raggruppato)
	
	select cod_prodotto_raggruppato
	into   :ls_cod_prodotto_raggruppato
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if not isnull(ls_cod_prodotto_raggruppato) then
		
		ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, 	ld_quan_consegnata, "M")
		
		update anag_prodotti
		set    quan_impegnata = quan_impegnata + :ld_quan_raggruppo
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_raggruppato;
				 
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante aggiornamento valori prodotto: " + sqlca.sqlerrtext
			return -1
		end if
	end if
	
			
	update stock
	set    quan_in_spedizione = quan_in_spedizione - :ld_quan_consegnata
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto and
			 cod_deposito = :ls_cod_deposito and
			 cod_ubicazione = :ls_cod_ubicazione and
			 cod_lotto = :ls_cod_lotto and
			 prog_stock = :ll_progr_stock and
			 data_stock = :ldt_data_stock;
						 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante aggiornamento valori prodotto: " + sqlca.sqlerrtext
		return -1
	end if						 
end if
			
// *** se esiste il riferimento all'ordine di vendita devo sistemare tutte le quantità ed eventualmente 
//     storicizzare l'ordine

if not isnull(ll_anno_registrazione_ord_ven) and not isnull(ll_num_registrazione_ord_ven) and not isnull(ll_prog_riga_ord_ven) and ll_anno_registrazione_ord_ven > 0 and ll_num_registrazione_ord_ven > 0 and ll_prog_riga_ord_ven > 0 then
			
	// *** aggiorno la quantità evasa dell'ordine, togliendo quella della bolla
			
	update det_ord_ven
	set    quan_evasa = quan_evasa - :ld_quan_consegnata
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione_ord_ven and
			 num_registrazione = :ll_num_registrazione_ord_ven and
			 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
			 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante aggiornamento valori ordine: " + sqlca.sqlerrtext
		return -1
	end if		
		
	// *** se l'ordine ha quantità evasa > 0 allora devo cambiare il flag evasione
		
	select quan_evasa,
			 quan_ordine
	into   :ld_quan_evasa,
			 :ld_quan_ordine
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione_ord_ven and
			 num_registrazione = :ll_num_registrazione_ord_ven and
			 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
				 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante aggiornamento valori ordine: " + sqlca.sqlerrtext
		return -1
	end if		
		
	if not isnull(ld_quan_evasa) and ld_quan_evasa > 0 and ld_quan_evasa < ld_quan_ordine then // flag a parziale
	
		// *** nel caso in cui l'ordine sia parziale, vuol dire che esiste una bolla collegata quindi non posso storicizzare
		//     l'ordine, quindi elimino solo le righe della bolla
	
		update det_ord_ven
		set    flag_evasione = 'P'
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione_ord_ven and
				 num_registrazione = :ll_num_registrazione_ord_ven and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
						 
		ll_ret = uof_elimina_riga_bol_ven( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven, ref ls_errore)
		if ll_ret < 0 then
			fs_errore = ls_errore
			return -1
		end if	
		
		// aggiorno l'ordine
		luo_obj = create uo_generazione_documenti		
		ll_ret = luo_obj.uof_calcola_stato_ordine( ll_anno_registrazione_ord_ven, ll_num_registrazione_ord_ven)
		if ll_ret < 0 then
			destroy luo_obj
			return -1
		end if
		destroy luo_obj
					 
	elseif ld_quan_evasa = 0 or isnull(ld_quan_evasa) then // aperto
					
		// *** nel caso in cui l'ordine sia aperto, vuol dire che non è stato evaso con nessuna altra bolla, quindi lo posso storicizzare
		//     ovviamente dopo aver cancellato la riga della bolla
			
		update det_ord_ven
		set    flag_evasione = 'A'
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione_ord_ven and
				 num_registrazione = :ll_num_registrazione_ord_ven and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;	
				 
				 
					 
		ll_ret = uof_elimina_riga_bol_ven( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven, ref ls_errore)
		if ll_ret < 0 then
			fs_errore = ls_errore
			return -1
		end if	
		
		// *** controllo la commessa
		if not isnull(ll_anno_commessa) and ll_anno_commessa > 0 and not isnull(ll_num_commessa) and ll_num_commessa> 0 then

			select count(*)
			into   :ll_count
			from   det_bol_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_commessa = :ll_anno_commessa and
					 num_commessa = :ll_num_commessa;
					 
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante controllo commessa: " + sqlca.sqlerrtext
				return -1
			end if
			
			if ll_count > 0 and not isnull(ll_count) then
				fs_errore = "Attenzione: la riga di bolla " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "/" + string(ll_prog_riga_ord_ven) + " fa riferimento ad una commessa distribuita in più DDT.~r~nProcedere manualmente."
				return -1
			end if
			
		end if
	
		il_anno_reg_bol_ven = fl_anno_registrazione
		il_num_reg_bol_ven = fl_num_registrazione
		ll_ret = uof_storicizza(ll_anno_registrazione_ord_ven, ll_num_registrazione_ord_ven, ll_prog_riga_ord_ven, true, fs_mp_escluse[], fl_righe_escluse[], ref fs_testo_esclusioni, ref ls_messaggio )
				
		if ll_ret < 0 then
			fs_errore = ls_messaggio
			return -1
		end if	
		
		// aggiorno l'ordine		
		luo_obj = create uo_generazione_documenti		
		ll_ret = luo_obj.uof_calcola_stato_ordine( ll_anno_registrazione_ord_ven, ll_num_registrazione_ord_ven)
		if ll_ret < 0 then
			destroy luo_obj
			return -1
		end if		
		destroy luo_obj
		
	else               // ***** cancello la riga della bolla
		
		ll_ret = uof_elimina_riga_bol_ven( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven, ref ls_errore)
		if ll_ret < 0 then
			fs_errore = ls_errore
			return -1
		end if			
				
	end if	

else                  // *** se non esiste l'ordine elimino solo la riga della bolla
	
	ll_ret = uof_elimina_riga_bol_ven( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven, ref ls_errore)
	if ll_ret < 0 then
		fs_errore = ls_errore
		return -1
	end if				
			
end if			

return 0
end function

public function integer uof_valorizza_semilavorato (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto_trasferimento, ref decimal fd_prezzo, string fs_messaggio);/*
FUNZIONE DI CALCOLO DEL PREZZO DI LISTINO DI UN PRODOTTO SEMILAVORATO SOGGETTO A TRASFERIMENTO.
Questa procedura viene utilizzata nei trasferimenti dei semilavorati tra uno stabilimento ed un altro.
Esempio:
1- Ordine du una tenda B022 a Saccolongo
2- Il telo B022Y viene prodotto a Saccolongo e poi trasferito a Veggiano per la produzione della struttura.
3- poi il prodotto finito viene ritrasferito a Saccolongo
La procedura in oggetto riguarda solo il prodotto di cui al punto 2
*/
string ls_flag_tessuto, ls_flag_mantovana, ls_flag_altezza_mantovana,  ls_flag_tessuto_mantovana, ls_flag_cordolo, ls_flag_passamaneria, &
          ls_flag_verniciatura, ls_flag_comando, ls_flag_pos_comando, ls_flag_alt_asta, ls_flag_supporto, ls_flag_luce_finita,   &
		 ld_limite_max_dim_x, ld_limite_max_dim_y, ld_limite_max_dim_z, ld_limite_max_dim_t, ld_limite_min_dim_x, ld_limite_min_dim_y,   &
		 ld_limite_min_dim_z, ld_limite_min_dim_t, ls_flag_secondo_comando, ls_flag_tessuto_1, ls_flag_mantovana_1, ls_flag_altezza_mantovana_1, &
		 ls_flag_tessuto_mantovana_1, ls_flag_cordolo_1, ls_flag_passamaneria_1, ls_flag_verniciatura_1, ls_flag_comando_1, ls_flag_pos_comando_1,&
		 ls_flag_alt_asta_1, ls_flag_supporto_1, ls_flag_luce_finita_1, ld_limite_max_dim_x_1, ld_limite_max_dim_y_1, ld_limite_max_dim_z_1, &
		 ld_limite_max_dim_t_1, ld_limite_min_dim_x_1, ld_limite_min_dim_y_1, ld_limite_min_dim_z_1, ld_limite_min_dim_t_1, &
		 ls_flag_secondo_comando_1, ls_cod_modello, ls_cod_prodotto_origine, ld_des_dim_x,ld_des_dim_y,ld_des_dim_z,ld_des_dim_t, &
		 ls_messaggio_modifica, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_cod_prodotto_finito, ls_messaggio,ls_cod_tipo_ord_ven, &
		 ls_des_prodotto_mag, ls_mp_escluse[], ls_testo_esclusioni, ls_cod_prodotto_raggruppato, ls_null, &
		 ls_materia_prima[], ls_errore, ls_cod_versione, ls_versione_mat_prima[]

integer ll_anno_commessa,li_risposta

long  ll_num_commessa, ll_null, ll_righe_escluse[], ll_ret, ll_riga

double ld_quantita_utilizzo[]

dec{4} ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t, ld_quan_ordine, ld_quan_raggruppo,ld_prezzo_vendita

datetime ldt_adesso

uo_funzioni_1 luo_funzioni_1

uo_gestione_conversioni luo_gestione_conversioni

setnull(ls_null)

select cod_prodotto,
		cod_tipo_det_ven,
		quan_ordine,
		cod_versione
into	:ls_cod_prodotto_origine,
		:ls_cod_tipo_det_ven,
		:ld_quan_ordine,
		:ls_cod_versione
from det_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :fl_anno_registrazione and
		num_registrazione = :fl_num_registrazione and
		prog_riga_ord_ven = :fl_prog_riga_ord_ven;
if sqlca.sqlcode = 100 then
	fs_messaggio = "Ordine non trovato in det_ord_ven" 
	rollback;
	return -1
elseif sqlca.sqlcode = -1 then
	fs_messaggio = "Errore in select su tabella det_ord_ven. " + sqlca.sqlerrtext 
	rollback;
	return -1
end if



select dim_x,
		dim_y,
		dim_z,
		dim_t,
		cod_modello
into	:ld_dim_x,
		:ld_dim_y,
		:ld_dim_z,
		:ld_dim_t,
		:ls_cod_modello
from comp_det_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :fl_anno_registrazione and
		num_registrazione = :fl_num_registrazione and
		prog_riga_ord_ven = :fl_prog_riga_ord_ven;
if sqlca.sqlcode = 100 then
	fs_messaggio = "Ordine non trovato in comp_det_ord_ven" 
	rollback;
	return -1
elseif sqlca.sqlcode = -1 then
	fs_messaggio = "Errore in select su tabella comp_det_ord_ven. " + sqlca.sqlerrtext 
	rollback;
	return -1
end if
	
	
ls_cod_modello = fs_cod_prodotto_trasferimento
// trovo le materie prime

if pos(fs_cod_prodotto_trasferimento,ls_cod_prodotto_origine,1) = 0  then
	fs_messaggio = "Prodotto ordine diverso da prodotto trasferito"
	rollback;
	return -1
end if

luo_funzioni_1 = create uo_funzioni_1
	
if luo_funzioni_1.uof_trova_mat_prime_varianti (fs_cod_prodotto_trasferimento, &
									 ls_cod_versione, &
									 ls_null,&
									ref ls_materia_prima[], &
									ref ls_versione_mat_prima[], &
									ref ld_quantita_utilizzo[], &
									ld_quan_ordine, &
									fl_anno_registrazione, &
									fl_num_registrazione, &
									fl_prog_riga_ord_ven, &
									"varianti_det_ord_ven", &
									ls_null,&
									ref ls_errore ) < 0 then
									
	fs_messaggio = "Errore in estrazione materie prime (uof_trova_mat_prime_varianti): "  + ls_errore
	destroy luo_funzioni_1
	rollback;
	return -1
end if
	
destroy luo_funzioni_1

// imposto alcuni parametri di istanza per passare i dati necessari
is_flag_ar = "S"
is_cod_prodotto_origine = ls_cod_prodotto_origine
is_cod_prodotto_ar      = fs_cod_prodotto_trasferimento
is_cod_materia_prima[] = ls_materia_prima[]
id_quan_utilizzo[] = ld_quantita_utilizzo[]
id_valore_storico = s_cs_xx.parametri.parametro_d_1
	
ll_ret = uof_storicizza(fl_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, true, ls_mp_escluse[], ll_righe_escluse[], ls_testo_esclusioni, ls_messaggio )

if ll_ret = 1 then
	fs_messaggio = "Errore in trasf. AR~r~n" + ls_messaggio + "~r~n" + sqlca.sqlerrtext 
	rollback;
	return -1
end if
	
SELECT flag_tessuto,   
		flag_tipo_mantovana,   
		flag_altezza_mantovana,   
		flag_tessuto_mantovana,   
		flag_cordolo,   
		flag_passamaneria,   
		flag_verniciatura,   
		flag_comando,   
		flag_pos_comando,   
		flag_alt_asta,   
		flag_supporto,   
		flag_luce_finita,   
		limite_max_dim_x,   
		limite_max_dim_y,   
		limite_max_dim_z,   
		limite_max_dim_t,   
		limite_min_dim_x,   
		limite_min_dim_y,   
		limite_min_dim_z,   
		limite_min_dim_t,   
		flag_secondo_comando  
 INTO :ls_flag_tessuto,   
		:ls_flag_mantovana,   
		:ls_flag_altezza_mantovana,   
		:ls_flag_tessuto_mantovana,   
		:ls_flag_cordolo,   
		:ls_flag_passamaneria,   
		:ls_flag_verniciatura,   
		:ls_flag_comando,   
		:ls_flag_pos_comando,   
		:ls_flag_alt_asta,   
		:ls_flag_supporto,   
		:ls_flag_luce_finita,   
		:ld_limite_max_dim_x,   
		:ld_limite_max_dim_y,   
		:ld_limite_max_dim_z,   
		:ld_limite_max_dim_t,   
		:ld_limite_min_dim_x,   
		:ld_limite_min_dim_y,   
		:ld_limite_min_dim_z,   
		:ld_limite_min_dim_t,   
		:ls_flag_secondo_comando  
 FROM tab_flags_configuratore
 WHERE cod_azienda = :s_cs_xx.cod_azienda and
		 cod_modello = :ls_cod_modello;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Mancano i parametri nella tabella flag_configuratore: riga saltata" 
	rollback;
	return -1
end if

// *********************  pulisco i passi che adesso non sono più validi ****************************************

if ls_flag_luce_finita = "N" or isnull(ls_flag_luce_finita) then
	update comp_det_ord_ven
	set    flag_misura_luce_finita = 'L'
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext
		rollback;
		return -1
	end if
end if

if ls_flag_tessuto = "N" or isnull(ls_flag_tessuto) then
	update comp_det_ord_ven
	set    cod_tessuto = null, cod_non_a_magazzino = null, flag_fc_tessuto = 'N', flag_addizionale_tessuto = 'N'
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext
		rollback;
		return -1
	end if
end if

if ls_flag_tessuto_mantovana = "N" or isnull(ls_flag_tessuto_mantovana) then
	update comp_det_ord_ven
	set    cod_tessuto_mant = null, cod_mant_non_a_magazzino = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext
		rollback;
		return -1
	end if
end if

if ls_flag_altezza_mantovana = "N" or isnull(ls_flag_altezza_mantovana) then
	update comp_det_ord_ven
	set    alt_mantovana = 0
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext
		rollback;
		return -1
	end if
end if

if ls_flag_mantovana = "N" or isnull(ls_flag_mantovana) then
	update comp_det_ord_ven
	set    flag_tipo_mantovana = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext
		rollback;
		return -1
	end if
end if

if ls_flag_cordolo = "N" or isnull(ls_flag_cordolo) then
	update comp_det_ord_ven
	set    flag_cordolo = 'N', colore_cordolo = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext
		rollback;
		return -1
	end if
end if

if ls_flag_passamaneria = "N" or isnull(ls_flag_passamaneria) then
	update comp_det_ord_ven
	set    flag_passamaneria = 'N', colore_passamaneria = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext
		rollback;
		return -1
	end if
end if

if ls_flag_verniciatura = "N" or isnull(ls_flag_verniciatura) then
	update comp_det_ord_ven
	set    cod_verniciatura = null, flag_fc_vernic = 'N', des_vernice_fc = null, flag_addizionale_vernic = 'N'
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext
		rollback;
		return -1
	end if
end if

if ls_flag_comando = "N" or isnull(ls_flag_comando) then
	update comp_det_ord_ven
	set    cod_comando = null, flag_addizionale_comando_1 ='N', flag_fc_comando_1 = 'N', des_comando_1 = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext
		rollback;
		return -1
	end if
end if

if ls_flag_pos_comando = "N" or isnull(ls_flag_pos_comando) then
	update comp_det_ord_ven
	set    pos_comando = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext
		rollback;
		return -1
	end if
end if

if ls_flag_alt_asta = "N" or isnull(ls_flag_alt_asta) then
	update comp_det_ord_ven
	set    alt_asta = 0
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext
		rollback;
		return -1
	end if
end if

if ls_flag_secondo_comando = "N" or isnull(ls_flag_secondo_comando) then
	update comp_det_ord_ven
	set    cod_comando_2 = null, flag_addizionale_comando_2 ='N', flag_fc_comando_2 = 'N', des_comando_2 = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext
		rollback;
		return -1
	end if
end if

if ls_flag_supporto = "N" or isnull(ls_flag_supporto) then
	update comp_det_ord_ven
	set    cod_tipo_supporto = null, flag_addizionale_supporto = 'N', flag_fc_supporto ='N'
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext
		rollback;
		return -1
	end if
end if

str_conf_prodotto.flag_cambio_modello = false
setnull(str_conf_prodotto.messaggio_modifiche_modello)

select des_prodotto
into   :ls_des_prodotto_mag
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :ls_cod_modello;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore durante ricerca della descrizione del prodotto "+ls_cod_modello+".~r~n" + sqlca.sqlerrtext
	rollback;
	return -1
end if

update det_ord_ven
set cod_prodotto = :ls_cod_modello,
	 des_prodotto = :ls_des_prodotto_mag
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_riga_ord_ven= :fl_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore durante cambio prodotto nella riga di dettaglio.~r~n" + sqlca.sqlerrtext
	rollback;
	return -1
end if

ll_ret = uof_calcola_prezzo(fl_anno_registrazione,fl_num_registrazione,fl_prog_riga_ord_ven,  fs_cod_prodotto_trasferimento, ld_prezzo_vendita, ls_messaggio)

if ll_ret = -1 then
	fs_messaggio =  "Errore in calcolo prezzo  " + ls_messaggio
	rollback;
	return -1
end if
	
fd_prezzo = ld_prezzo_vendita
//dw_1.setitem(ll_riga, "prezzo_ricalcolato", ld_prezzo_vendita)

//g_mb.warning("PREZZO = " + string(ld_prezzo_vendita) )

rollback;

fs_messaggio = ""
return 0

end function

public function integer uof_calcola_prezzo (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, ref decimal fd_prezzo, ref string fs_errore);// *****************************************************************************************************************
// FUNZIONE DI CALCOLO DEL PREZZO DEL PRODOTTO FINITO DEL CONFIGURATORE
// SENZA PASSARE DENTRO LA MASCHERA DEL CONFIGURATORE
//
// 									AUTORE: EnMe 29/1/2002
// ----------------------------------  calcolo del prezzo  --------------------------------------------------- //
boolean lb_test
string ls_valuta_lire, ls_cod_valuta, ls_sql, ls_prezzo_vendita, ls_valore_riga, ls_fat_conversione, ls_cod_tessuto, ls_errore,&
		ls_sconto, ls_provvigione_1, ls_provvigione_2, ls_quantita_um, ls_prezzo_um, ls_null,ls_flag_prezzo_superficie, &
		ls_cod_gruppo_var_quan_distinta,ls_tabella_det_doc, ls_progressivo,ls_cod_tipo_det_ven,ls_cod_versione,&
		ls_flag_uso_quan_distinta, ls_tabella_varianti, ls_messaggio
long ll_i, ll_y
dec{4} ld_variazioni[], ld_gruppi_sconti[], ld_provv_1, ld_provv_2, ld_valore_riga, ld_dimensione_1, ld_dimensione_2, &
		ld_dimensione_3, ld_quan_ordinata, ld_provvigione_1, ld_provvigione_2, ld_prezzo_um, ld_quantita_um,ld_quan_tessuto, &
		ld_dim_x, ld_dim_y, ld_dim_z, ld_prezzo_vendita
dec{5} ld_fat_conversione
uo_gruppi_sconti luo_gruppi_sconti
uo_condizioni_cliente luo_condizioni_cliente



setnull(ls_null)
ls_tabella_det_doc = "det_ord_ven"
ls_progressivo = "prog_riga_ord_ven"
ls_tabella_varianti = "varianti_det_ord_ven"

luo_condizioni_cliente = CREATE uo_condizioni_cliente
s_cs_xx.listino_db.flag_calcola = "S"
s_cs_xx.listino_db.anno_documento = fl_anno_registrazione
s_cs_xx.listino_db.num_documento = fl_num_registrazione
s_cs_xx.listino_db.prog_riga = fl_prog_riga_ord_ven
//s_cs_xx.listino_db.tipo_gestione = "ORD_VEN"
s_cs_xx.listino_db.tipo_gestione = "varianti_det_ord_ven"

select cod_versione,
		quan_ordine
into   :s_cs_xx.listino_db.cod_versione,
		:s_cs_xx.listino_db.quan_prodotto_finito
from   det_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :fl_anno_registrazione and
		num_registrazione = :fl_num_registrazione and
		prog_riga_ord_ven = :fl_prog_riga_ord_ven;

select cod_cliente, 
		cod_valuta, 
		cod_tipo_listino_prodotto, 
		data_registrazione,
		cod_agente_1
into  :s_cs_xx.listino_db.cod_cliente, 
      :s_cs_xx.listino_db.cod_valuta, 
		:s_cs_xx.listino_db.cod_tipo_listino_prodotto, 
		:s_cs_xx.listino_db.data_riferimento,
		:luo_condizioni_cliente.str_parametri.cod_agente_1
from  tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :fl_anno_registrazione and
      num_registrazione = :fl_num_registrazione ;

select dim_x,
		dim_y,
		dim_z
into   	:ld_dim_x,
		:ld_dim_y,
		:ld_dim_z
from   comp_det_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :fl_anno_registrazione and
		num_registrazione = :fl_num_registrazione and
		prog_riga_ord_ven = :fl_prog_riga_ord_ven;
		

luo_condizioni_cliente.ib_setitem=false
luo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
luo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
luo_condizioni_cliente.str_parametri.cod_prodotto = fs_cod_prodotto
luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = s_cs_xx.listino_db.cod_tipo_listino_prodotto
luo_condizioni_cliente.str_parametri.cod_valuta = s_cs_xx.listino_db.cod_valuta
luo_condizioni_cliente.str_parametri.data_riferimento = s_cs_xx.listino_db.data_riferimento
luo_condizioni_cliente.str_parametri.cod_cliente = s_cs_xx.listino_db.cod_cliente
luo_condizioni_cliente.str_parametri.quantita = s_cs_xx.listino_db.quan_prodotto_finito
luo_condizioni_cliente.str_parametri.valore = 0
luo_condizioni_cliente.str_parametri.cod_agente_2 = ls_null
luo_condizioni_cliente.str_parametri.colonna_quantita = ""
luo_condizioni_cliente.str_parametri.colonna_prezzo = ""
luo_condizioni_cliente.ib_setitem = false
luo_condizioni_cliente.ib_setitem_provvigioni = false
luo_condizioni_cliente.wf_condizioni_cliente()

//for ll_i = 1 to 10
//	if str_conf_prodotto.stato = "N" and not str_conf_prodotto.flag_cambio_modello then
//										//            ^^ aggiunto per spec_varie_5; funzione 14
//		if upperbound(luo_condizioni_cliente.str_output.sconti) >= ll_i then
//			istr_riepilogo.sconti[ll_i] = luo_condizioni_cliente.str_output.sconti[ll_i]
//		else
//			istr_riepilogo.sconti[ll_i] = 0
//		end if
//	else
//		istr_riepilogo.sconti[ll_i] = str_conf_prodotto.sconti[ll_i]
//	end if
//next

//if str_conf_prodotto.stato = "N" then
//	ld_provvigione_1 = luo_condizioni_cliente.str_output.provvigione_1
//	ld_provvigione_2 = luo_condizioni_cliente.str_output.provvigione_2
//else
//	ld_provvigione_1 = str_conf_prodotto.provvigione_1
//	ld_provvigione_2 = str_conf_prodotto.provvigione_2
//end if
//if isnull(ld_provvigione_1) then ld_provvigione_1 = 0
//if isnull(ld_provvigione_2) then ld_provvigione_2 = 0
	
if upperbound(luo_condizioni_cliente.str_output.variazioni) < 1 or isnull(upperbound(luo_condizioni_cliente.str_output.variazioni)) then
	ld_prezzo_vendita = 0
	ld_fat_conversione = 1
else
	ld_fat_conversione = 1
	ld_prezzo_vendita = luo_condizioni_cliente.str_output.variazioni[upperbound(luo_condizioni_cliente.str_output.variazioni)]
end if

//	 calcolo del valore netto riga //
select cod_tipo_det_ven_prodotto,
		flag_prezzo_superficie,
		flag_uso_quan_distinta,
		cod_gruppo_var_quan_distinta
into   :ls_cod_tipo_det_ven,
		:ls_flag_prezzo_superficie,
		:ls_flag_uso_quan_distinta,
		:ls_cod_gruppo_var_quan_distinta
from   tab_flags_configuratore
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_modello = :fs_cod_prodotto;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca tipi dettaglio nella tabella parametri configuratore. Dettaglio errore "+sqlca.sqlerrtext
	return -1
end if
if isnull(ls_cod_tipo_det_ven) then
	fs_errore = "Il tipo dettaglio vendita del prodotto non è stato indicato nella tabella flags configuratore"
	return -1
end if


// ----------------------- gestione del prezzo per metro quadarato con il fattore di conversione ------------------------

if ls_flag_prezzo_superficie = "S" then
	if ld_dim_x > 0 then
		if luo_condizioni_cliente.str_output.min_fat_larghezza > 0 and not isnull(luo_condizioni_cliente.str_output.min_fat_larghezza) and ld_dim_x < luo_condizioni_cliente.str_output.min_fat_larghezza then
			ld_dimensione_1 = luo_condizioni_cliente.str_output.min_fat_larghezza
		else
			ld_dimensione_1 = ld_dim_x
		end if
	end if
	if ld_dim_y > 0 then
		if luo_condizioni_cliente.str_output.min_fat_altezza > 0 and not isnull(luo_condizioni_cliente.str_output.min_fat_altezza) and ld_dim_y < luo_condizioni_cliente.str_output.min_fat_altezza then
			ld_dimensione_2 = luo_condizioni_cliente.str_output.min_fat_altezza
		else
			ld_dimensione_2 = ld_dim_y
		end if
	end if
	if ld_dim_z > 0 then
		if luo_condizioni_cliente.str_output.min_fat_profondita > 0 and not isnull(luo_condizioni_cliente.str_output.min_fat_profondita) and ld_dim_z < luo_condizioni_cliente.str_output.min_fat_profondita then
			ld_dimensione_3 = luo_condizioni_cliente.str_output.min_fat_profondita
		else
			ld_dimensione_3 = ld_dim_x
		end if
	end if
	
	// --------------------- nuova gestione vuoto per pieno -------------------------
	//                         nicola ferrari 23/05/2000
	if ls_flag_uso_quan_distinta = "N" then
		ld_fat_conversione = round((ld_dimensione_1 * ld_dimensione_2) / 10000,2)
		if ld_fat_conversione <= 0 or isnull(ld_fat_conversione) then
			fs_errore = "Attenzione la quantità risultante dal calcolo DIM1 x DIM2 è pari a zero o è nulla; verrà FORZATA 1, ma è necessario il controllo dell'operatore!"
			ld_fat_conversione = 1
		end if
	else
		uo_conf_varianti uo_ins_varianti
		uo_ins_varianti = CREATE uo_conf_varianti
		if uo_ins_varianti.uof_ricerca_quan_mp(fs_cod_prodotto,  &
														s_cs_xx.listino_db.cod_versione, &
														s_cs_xx.listino_db.quan_prodotto_finito, &
														ls_cod_gruppo_var_quan_distinta, &
														fl_anno_registrazione, &
														fl_num_registrazione, &
														fl_prog_riga_ord_ven, &
														ls_tabella_varianti, &
														ls_cod_tessuto, &
														ld_quan_tessuto,&
														ls_errore) = -1 then
			fs_errore = "Errore nella ricerca quantità tessuto in disntita base. Dettaglio errore: " + ls_errore
			rollback;
			return -1
		end if

		if ld_quan_tessuto <= 0 or isnull(ld_quan_tessuto) then
			fs_errore = "Attenzione la quantità trovata nel gruppo variante " + ls_cod_gruppo_var_quan_distinta + " è pari a zero o è nulla; verrà FORZATA 1, ma è necessario il controllo dell'operatore!"
			ld_quan_tessuto = 1
		end if
		destroy uo_ins_varianti
		ld_fat_conversione = round(ld_quan_tessuto,2)
	end if

	// ---------------------- fine vuoto per pieno ----------------------------------
	// controllo il minimale di superficie
	if luo_condizioni_cliente.str_output.min_fat_superficie > 0 and not isnull(luo_condizioni_cliente.str_output.min_fat_superficie) and ld_fat_conversione < luo_condizioni_cliente.str_output.min_fat_superficie then
		ld_fat_conversione = luo_condizioni_cliente.str_output.min_fat_superficie
	end if
	ld_quantita_um = s_cs_xx.listino_db.quan_prodotto_finito * ld_fat_conversione
	ld_prezzo_um = ld_prezzo_vendita
	ld_prezzo_vendita = ld_prezzo_vendita * ld_fat_conversione
end if	

if luo_condizioni_cliente.uof_arrotonda_prezzo(ld_prezzo_vendita, str_conf_prodotto.cod_valuta, ld_prezzo_vendita, ls_messaggio) = -1 then
	fs_errore = "Errore durante l'arrotondamento del prezzo di vendita: " + ls_messaggio
	rollback;
	return -1
end if

fd_prezzo = ld_prezzo_vendita


rollback;

return 0

end function

on uo_storicizzazione.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_storicizzazione.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;is_flag_ar = "N"
end event

