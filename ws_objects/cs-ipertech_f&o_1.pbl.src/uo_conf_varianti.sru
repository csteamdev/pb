﻿$PBExportHeader$uo_conf_varianti.sru
$PBExportComments$UO configurazione delle varianti nel configuratore
forward
global type uo_conf_varianti from nonvisualobject
end type
end forward

global type uo_conf_varianti from nonvisualobject
end type
global uo_conf_varianti uo_conf_varianti

type variables
string					is_gestione, is_cod_versione, is_cod_modello, is_figlio_fittizio = "REPARTO", is_errore_vendita_sl = ""

long					il_anno_registrazione, il_num_registrazione, il_prog_riga

str_varianti			str_varianti[]

boolean				ib_fase_su_radice = false, ib_nofase_su_figli = true

string					is_warning_formule[], is_elaborazione_formule[], is_elaborazione_formule_qta[], is_elaborazione_formule_qtatec[]
end variables

forward prototypes
public function integer uof_ins_varianti (string fs_prodotto_finito, string fs_versione, ref string fs_messaggio)
public function integer uof_ricerca_quan_mp (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quan_prodotto, string fs_cod_gruppo_variante, long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_registrazione, string fs_tab_varianti, ref string fs_cod_materia_prima, ref decimal fd_quan_materia_prima, ref string fs_errore)
public function integer uof_valorizza_formule (string fs_cod_prodotto, string fs_cod_versione, ref string fs_errore)
public function integer uof_crea_varianti_esclusione (string fs_cod_prodotto, string fs_cod_versione)
public function integer uof_carica_mp_automatiche (string as_cod_modello_prodotto, decimal ad_dim_1, decimal ad_dim_2, decimal ad_dim_3, decimal ad_dim_4, decimal ad_altezza_asta, string as_cod_vernicitura_1, string as_cod_vernicitura_2, string as_cod_vernicitura_3, ref str_mp_automatiche atr_mp_automatiche[], ref string as_errore)
public function integer uof_crea_varianti (wstr_riepilogo astr_riepilogo, wstr_flags astr_flag_configuratore, ref string as_errore)
public function integer uof_valorizza_formule_prod_figlio (string as_padre, string as_versione_padre, long al_num_sequenza, ref string as_figlio, ref string as_versione_figlio, ref long al_sequenza_figlio, string as_formula_figlio, string as_formula_versione_figlio, ref string as_errore)
public function integer uof_valorizza_formule_figlio_varianti (string as_padre, string as_versione_padre, long al_num_sequenza, string as_figlio, string as_versione_figlio, ref string as_cod_variante, ref string as_versione_variante, string as_formula_figlio, string as_formula_versione_figlio, ref string as_errore)
public subroutine uof_fase_su_radice_db (string as_cod_prodotto, string as_cod_versione)
public function integer uof_crea_ramo_fittizio (string as_cod_prodotto, string as_cod_versione, ref string as_errore)
public function integer uof_controlla_figlio_formula (string as_codice)
end prototypes

public function integer uof_ins_varianti (string fs_prodotto_finito, string fs_versione, ref string fs_messaggio);/*	Funzione che scorre la distinta base e inserisce la variante di tipo codice prodotto (fs_cod_prodotto_var)
		quando incontra un semilavorato o una materia prima di gruppo variante fs_cod_gruppo_variante

 nome: wf_ins_variante
 tipo: integer
  
	le variabili sono tutte passate attraverso la struttura str_varianti

	messaggio di errore contenuto nella variabile fs_messaggio

	valore della funzione = -1 si è verificato un errore
									 0 tutto ok elaborazione terminata con successo
									 1 c'è una segnalazione, ma si può andare avanti lo stesso


	Creata il 31-08-98 
	Autore ENRICO MENEGOTTO
   modificata il 31/08/1998
   MODIFICATA IL 20/10/2000 PER UNA SOLA PASSATA DELLA DISTINTA.
	modificata il 31/10/2006 per adeguarla alla nuova gestione distinta base con versioni figli diversi dalla versione padre

*/
string					ls_cod_prodotto_figlio[],ls_test, ls_tabella, ls_progressivo, ls_cod_misura, ls_cod_formula_quan_utilizzo, &
							ls_des_estesa, ls_formula_tempo, ls_sql_del, ls_sql_ins, ls_cod_formula_vpp, ls_cod_gruppo_variante, &
							ls_flag_materia_prima, ls_cod_versione_figlio[], ls_tabella_comp, ls_cod_deposito_produzione, ls_cod_reparto_produzione[], &
							ls_errore, ls_vuoto[], ls_tabella_var_rep, ls_flag_materia_prima_distinta, ls_cod_formula_prod_figlio, ls_cod_formula_vers_figlio, &
							ls_cod_formula_quan_tecnica, ls_nuova_variante, ls_nuova_versione_variante, ls_temp, ls_tipo_ritorno, ls_sql_upd, ls_tmp, &
							ls_flag_ins_varianti_ricorsivo
							
dec{4}					ld_quan_tecnica, ld_quan_utilizzo, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_coef_calcolo

long						ll_conteggio,ll_t,ll_num_sequenza, ll_max, ll_i, ll_y, ll_ordine[],  ll_num_sequenza_new, ll_index

integer					li_risposta, li_count, li_ret

uo_funzioni_1			luo_funzioni_1

s_chiave_distinta	lstr_distinta



choose case is_gestione
		
	case "ORD_VEN"
		ls_tabella = "varianti_det_ord_ven"
		ls_progressivo = "prog_riga_ord_ven"
		ls_tabella_comp = "comp_det_ord_ven"
		ls_tabella_var_rep = "varianti_det_ord_ven_prod"
		
	case "OFF_VEN"
		ls_tabella = "varianti_det_off_ven"
		ls_progressivo = "prog_riga_off_ven"
		ls_tabella_comp = "comp_det_off_ven"
		ls_tabella_var_rep = "varianti_det_ord_ven_prod"
		
end choose

ll_conteggio = 1
ll_max = upperbound(str_varianti)
// Tolto EnMe 21/06/2013
//if ll_max < 1 then return 0

luo_funzioni_1 = create uo_funzioni_1

declare righe_distinta_3 cursor for 
  SELECT distinta.cod_prodotto_figlio,   
         distinta.cod_versione_figlio,   
         distinta.num_sequenza,   
         distinta.cod_misura,   
         distinta.quan_tecnica,   
         distinta.quan_utilizzo,   
         distinta.dim_x,   
         distinta.dim_y,   
         distinta.dim_z,   
         distinta.dim_t,   
         distinta.coef_calcolo,   
         distinta.des_estesa,   
         distinta.formula_tempo,   
         distinta.cod_formula_quan_utilizzo,   
		distinta.flag_materia_prima,
         distinta_gruppi_varianti.cod_gruppo_variante,
		distinta.cod_formula_prod_figlio,
		distinta.cod_formula_vers_figlio,
		distinta.cod_formula_quan_tecnica,
		gruppi_varianti.flag_ins_varianti_ricorsivo
    FROM distinta
	 left outer join distinta_gruppi_varianti on 	distinta_gruppi_varianti.cod_azienda = distinta.cod_azienda and  
	 															distinta_gruppi_varianti.cod_prodotto_padre = distinta.cod_prodotto_padre and  
																distinta_gruppi_varianti.num_sequenza = distinta.num_sequenza and  
																distinta_gruppi_varianti.cod_prodotto_figlio = distinta.cod_prodotto_figlio and  
																distinta_gruppi_varianti.cod_versione_figlio = distinta.cod_versione_figlio and  
																distinta_gruppi_varianti.cod_versione = distinta.cod_versione 
	left outer join gruppi_varianti on	distinta_gruppi_varianti.cod_azienda = gruppi_varianti.cod_azienda and  
	 												distinta_gruppi_varianti.cod_gruppo_variante = gruppi_varianti.cod_gruppo_variante
	WHERE 	distinta.cod_azienda 				= :s_cs_xx.cod_azienda  AND  
        			distinta.cod_prodotto_padre 	= :fs_prodotto_finito AND  
				distinta.cod_versione 				= :fs_versione   ;

open righe_distinta_3;

do while true

	fetch righe_distinta_3 
	into  	:ls_cod_prodotto_figlio[ll_conteggio],
			:ls_cod_versione_figlio[ll_conteggio],
			:ll_num_sequenza,
			:ls_cod_misura,
			:ld_quan_tecnica,
			:ld_quan_utilizzo,
			:ld_dim_x,
			:ld_dim_y,
			:ld_dim_z,
			:ld_dim_t,
			:ld_coef_calcolo,
			:ls_des_estesa,
			:ls_formula_tempo,
			:ls_cod_formula_quan_utilizzo,
			:ls_flag_materia_prima_distinta,
			:ls_cod_gruppo_variante,
			:ls_cod_formula_prod_figlio,
			:ls_cod_formula_vers_figlio,
			:ls_cod_formula_quan_tecnica,
			:ls_flag_ins_varianti_ricorsivo ;

	if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

	if sqlca.sqlcode<>0 then
		close righe_distinta_3;
		fs_messaggio = "Configuratore: uof_ins_varianti ~r~nSi è verificato un errore imprevisto nel cursore che scorre la distinta" + SQLCA.SQLErrText
		return -1
	end if
	
	if not isnull(ls_cod_gruppo_variante) and ll_max > 0 then

		for ll_i = 1 to ll_max
			li_count = 0
			if ls_cod_gruppo_variante = str_varianti[ll_i].cod_gruppo_variante then
				
				if is_gestione = "ORD_VEN" then
					//in questo caso esiste la tabella varianti_det_ord_ven_prod
					
					ls_sql_del ="delete from varianti_det_ord_ven_prod " + &
									" where cod_azienda ='" + s_cs_xx.cod_azienda + "' and " + &
									" anno_registrazione=" + string(il_anno_registrazione) + " and " + &
									" num_registrazione=" + string(il_num_registrazione) + " and " + &
									ls_progressivo + "=" + string(il_prog_riga) + " and " + &
									" cod_prodotto_padre='" + fs_prodotto_finito + "' and " + &
									" num_sequenza=" + string(ll_num_sequenza) + " and " +&
									" cod_versione='" + fs_versione + "' "
					
					ls_sql_del += " and cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_conteggio] + "' "
					ls_sql_del += " and cod_versione_figlio='" + ls_cod_versione_figlio[ll_conteggio] + "' "

					execute immediate :ls_sql_del;
					if sqlca.sqlcode = -1 then
						fs_messaggio = "Configuratore uof_ins_varianti~r~nErrore nella Cancellazione della Variante da varianti_det_ord_ven_PROD. Dettaglio " + sqlca.sqlerrtext
						close righe_distinta_3;
						rollback;
						return -1
					end if
				end if
				
				ls_sql_del ="delete from " + ls_tabella + &
								" where cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
								" anno_registrazione=" + string(il_anno_registrazione) + " and " + &
								" num_registrazione=" + string(il_num_registrazione) + " and " + &
								ls_progressivo + "=" + string(il_prog_riga) + " and " + &
								" cod_prodotto_padre='" + fs_prodotto_finito + "' and " + &
								" num_sequenza=" + string(ll_num_sequenza) + " and " + &
								" cod_versione='" + fs_versione + "' "
				
				ls_sql_del += " and cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_conteggio] + "' "
				ls_sql_del += " and cod_versione_figlio='" + ls_cod_versione_figlio[ll_conteggio] + "' "
				
				execute immediate :ls_sql_del;
				if sqlca.sqlcode = -1 then
					fs_messaggio = "Configuratore uof_ins_varianti~r~nErrore nella Cancellazione della Variante da varianti_det_ord_ven. Dettaglio " + sqlca.sqlerrtext
					close righe_distinta_3;
					rollback;
					return -1
				end if
				
				// 	EnMe + Donato 24/02/2014: la versione della variante DEVE restare quella del prodotto figlio del ramo di distinta; altrimenti
				//	bisogna operare con le formule.
				str_varianti[ll_i].cod_versione_figlio = ls_cod_versione_figlio[ll_conteggio]
				
			
				//INSERISCO IN TABELLA VARIANTI DOCUMENTO, CON LE RELATIVE QUANTITà PRESENTI IN DISTINTA BASE (O SE <0 DELLA VARIANTE PREVISTA)
				//######################################################################################
				
				// Ptenda 28-9/2011
				// importante che rimangano le prossime 2 righe di codice; infatti se la quantità della materia prima automatica è zero, deve prendere la quantità in distinta.
				if not isnull(str_varianti[ll_i].quan_utilizzo) and str_varianti[ll_i].quan_utilizzo > 0 then ld_quan_utilizzo = str_varianti[ll_i].quan_utilizzo
				if not isnull(str_varianti[ll_i].quan_tecnica) and str_varianti[ll_i].quan_tecnica > 0 then ld_quan_tecnica = str_varianti[ll_i].quan_tecnica

				if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
				if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0
				
				if isnull(ls_cod_misura) then ls_cod_misura = ""
				if isnull(ld_dim_x) then ld_dim_x = 0
				if isnull(ld_dim_y) then ld_dim_y = 0
				if isnull(ld_dim_z) then ld_dim_z = 0
				if isnull(ld_dim_t) then ld_dim_t = 0
				if isnull(ld_coef_calcolo) then ld_coef_calcolo = 0
				if isnull(ls_des_estesa) then ls_des_estesa = ""
				if isnull(ls_formula_tempo) then ls_formula_tempo = ""
				
				ls_des_estesa = f_cazzillo(ls_des_estesa)
				
				ld_quan_utilizzo = round(ld_quan_utilizzo, 4)
				ld_quan_tecnica = round(ld_quan_tecnica, 4)

				// EnMe il 12/1/2011 la UM della variante va sempre presa dal magazzino.
				select flag_materia_prima, 
						cod_misura_mag
				into   :ls_flag_materia_prima, 
						:ls_cod_misura
				from   anag_prodotti
				where cod_azienda  = :s_cs_xx.cod_azienda and
						 cod_prodotto = :str_varianti[ll_i].cod_mp_variante;
						 
				if sqlca.sqlcode <> 0 then
					fs_messaggio = "Configuratore uof_ins_varianti~r~nErrore nella ricerca del flag_materia_prima e dell'unità di misura in anagrafica prodotti " + str_varianti[ll_i].cod_mp_variante + ". Dettaglio errore " + sqlca.sqlerrtext
					close righe_distinta_3;
					rollback;
					return -1
				end if
				
				if isnull(ls_flag_materia_prima) then ls_flag_materia_prima = "N"
				
				// EnMe 07/02/2012: aggiunto per gestione stabilimento e reparto produzione nelle varianti
				ls_cod_reparto_produzione = ls_vuoto
				setnull(ls_cod_deposito_produzione)
				
				if is_gestione = "ORD_VEN" then
					li_ret = luo_funzioni_1.uof_estrai_reparto_stabilimento ( ls_cod_prodotto_figlio[ll_conteggio], ls_cod_versione_figlio[ll_conteggio],is_gestione, il_anno_registrazione, il_num_registrazione, ref ls_cod_deposito_produzione, ref ls_cod_reparto_produzione[], ref ls_errore )			
												
					choose case li_ret
						case is > 0 
							// se ll_ret = 1 vuol dire che non c'è la fase di lavorazione; quindi procedo normalmente
							
						case 0
							//fase trovata
							//predispongo il secondo semaforo (SR Vendita_semilavorati)
							//in modo che non erediti la fase in distinta base
							if ib_fase_su_radice then	ib_nofase_su_figli = false
							
						case else
							fs_messaggio = ls_errore
							close righe_distinta_3;
							rollback;
							return -1
							
					end choose
					
					if isnull(ls_cod_deposito_produzione) then 
						ls_cod_deposito_produzione = " null "
					else
						ls_cod_deposito_produzione = " '" + f_cazzillo(ls_cod_deposito_produzione) + "' "
					end if
				end if
				
				if isnull(ls_cod_misura) then 
					ls_cod_misura = "null"
				else
					ls_cod_misura = "'" + ls_cod_misura + "'"
				end if
				
				ls_sql_ins =" insert into " + ls_tabella + &
								"(cod_azienda," + &
								"anno_registrazione," + &
								"num_registrazione," + &
								ls_progressivo + "," + &
								"cod_prodotto_padre," + &
								"num_sequenza," + &
								"cod_prodotto_figlio," + &
								"cod_versione_figlio," + &
								"cod_versione," + &
								"cod_misura," + &
								"cod_prodotto," + &
								"cod_versione_variante," + &
								"quan_tecnica," + &
								"quan_utilizzo," + &
								"dim_x," + &
								"dim_y," + &
								"dim_z," + &
								"dim_t," + &
								"coef_calcolo," + &
								"flag_esclusione," + &
								"formula_tempo," + &
								"flag_materia_prima," + &
								"des_estesa "
				if is_gestione = "ORD_VEN" then
					ls_sql_ins += ",cod_deposito_produzione)"
				else
					ls_sql_ins += " ) "
				end if
				
				ls_sql_ins += " VALUES ('" +  s_cs_xx.cod_azienda + "', " + &
								string(il_anno_registrazione) + ", " + &   
								string(il_num_registrazione) + ", " + &   
								string(il_prog_riga) + ", '" + &   
								fs_prodotto_finito + "', " + &
								string(ll_num_sequenza) + ", '" + &   
								ls_cod_prodotto_figlio[ll_conteggio] + "', '" + &
								ls_cod_versione_figlio[ll_conteggio] + "', '" + &
								fs_versione + "'," + &
								ls_cod_misura + ", '" + &
								str_varianti[ll_i].cod_mp_variante + "', '" + &
								str_varianti[ll_i].cod_versione_figlio + "', " + &
								f_double_to_string(ld_quan_tecnica) + ", " + &   
								f_double_to_string(ld_quan_utilizzo) + ", " + &
								f_double_to_string(ld_dim_x) + ", " + &   
								f_double_to_string(ld_dim_y) + ", " + &   
								f_double_to_string(ld_dim_z) + ", " + &   
								f_double_to_string(ld_dim_t) + ", " + &   
								f_double_to_string(ld_coef_calcolo) + ", '" + &   
								"N', '" + &
								ls_formula_tempo + "', '" + &
								ls_flag_materia_prima + "', '" + &
								ls_des_estesa + "' "

				if is_gestione = "ORD_VEN" then
					ls_sql_ins += ", " + ls_cod_deposito_produzione + ")" 
				else
					ls_sql_ins += " )"
				end if
		
				execute immediate :ls_sql_ins;
				if sqlca.sqlcode = -1 then
					
					if isnull(str_varianti[ll_i].cod_mp_variante) then 
						ls_tmp = "NULL"
					elseif str_varianti[ll_i].cod_mp_variante = "" then
						ls_tmp = "VUOTA"
					else
						ls_tmp = str_varianti[ll_i].cod_mp_variante
					end if
	
					ls_tmp += " su RAMO "+ls_cod_prodotto_figlio[ll_conteggio] + " Nseq: " + string(ll_num_sequenza)
					
					fs_messaggio = "uof_ins_varianti: Errore insert Variante ("+ls_tmp+"): " + sqlca.sqlerrtext
					close righe_distinta_3;
					rollback;
					return -1
				end if
				
				//ADESSO, SE CI SONO FORMULE SUL FIGLIO/VERSIONE DEL RAMO VANNO VALUTATE, IN QUANTO, EVENTULAI FORMULE SU QTA UTILIZZO/TECNICA
				//DEVONO FARE RIFERIMENTO A VARIABILI FIGLIO DEI NUOVI VALORI
				//######################################################################################
				
				//carico la variante e relativa versione prevista
				ls_nuova_variante = str_varianti[ll_i].cod_mp_variante
				ls_nuova_versione_variante = str_varianti[ll_i].cod_versione_figlio
				
				if (not isnull(ls_cod_formula_prod_figlio) and ls_cod_formula_prod_figlio<>"") or (not isnull(ls_cod_formula_vers_figlio) and ls_cod_formula_vers_figlio<>"") then
					
					li_ret = uof_valorizza_formule_figlio_varianti(	fs_prodotto_finito, fs_versione, ll_num_sequenza, ls_cod_prodotto_figlio[ll_conteggio], ls_cod_versione_figlio[ll_conteggio], &
																				ls_nuova_variante, ls_nuova_versione_variante, &
																				ls_cod_formula_prod_figlio, ls_cod_formula_vers_figlio, fs_messaggio)
					if li_ret<0 then
						//mettiamo nel messaggio anche l'informazione sul ramo di distinta in cui la formula ha dato errore
						fs_messaggio = 	"Ramo ("+fs_prodotto_finito+","+fs_versione+","+string(ll_num_sequenza)+","+ls_cod_prodotto_figlio[ll_conteggio]+","+ls_cod_versione_figlio[ll_conteggio]+"): " + &
												fs_messaggio
												
						close righe_distinta_3;
						rollback;
						return -1
					end if
				end if
				
				//SE LA VARIANTE O RELATIVA VERSIONE è STATA CAMBIATA, DEVO FARE UPDATE NELLATABELLA VARIANTI DOCUMENTO
				//PRIMA DI PASSARE AL CALCOLO DELLA QUANTITà UTILIZZO O TECNICA MEDIANTE EVENTUALE FORMULA
				//######################################################################################
				
				if ls_nuova_variante <> str_varianti[ll_i].cod_mp_variante or ls_nuova_versione_variante <> str_varianti[ll_i].cod_versione_figlio then
					
					//in ls_nuova_variante e ls_nuova_versione_variante ci sono i nuovi valori
					str_varianti[ll_i].cod_mp_variante = ls_nuova_variante
					str_varianti[ll_i].cod_versione_figlio = ls_nuova_versione_variante
					
					//faccio update delle quantità calcolate
					ls_sql_upd ="update " + ls_tabella + " "+&
									"set cod_prodotto='"+str_varianti[ll_i].cod_mp_variante+ "'," +&
										"cod_versione_variante='"+str_varianti[ll_i].cod_versione_figlio+"' "+ &
									"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
												"anno_registrazione="+string(il_anno_registrazione)+" and "+&
												"num_registrazione="+string(il_num_registrazione)+" and "+&
												ls_progressivo+"="+string(il_prog_riga)+" and "+&
												"cod_prodotto_padre='"+fs_prodotto_finito+"' and "+&
												"cod_versione='"+fs_versione+"' and "+&
												"num_sequenza="+string(ll_num_sequenza)+" and "+&
												"cod_prodotto_figlio='"+ls_cod_prodotto_figlio[ll_conteggio]+"' and "+&
												"cod_versione_figlio='"+ls_cod_versione_figlio[ll_conteggio]+"' "
												
					execute immediate :ls_sql_upd;
					
					if sqlca.sqlcode = -1 then
						fs_messaggio = "Configuratore uof_ins_varianti: Errore in update variante/vers.variante nella tabella " + ls_tabella + ": "+ sqlca.sqlerrtext
						close righe_distinta_3;
						rollback;
						return -1
					end if
					
				end if
				
				//#####################################################################################
				//ADESSO FINALMENTE VALUTO LE FORMULE QUANTITà UTILIZZO E TECNICA
				
				// ------------------- NUOVA GESTIONE VUOTO PER PIENO -------------------------------
				select cod_formula
				into :ls_cod_formula_vpp
				from tab_formule_legami
				where 	cod_azienda         		= :s_cs_xx.cod_azienda and
							cod_prodotto_finito 	= :is_cod_modello and
							cod_versione        	= :str_conf_prodotto.cod_versione and
							cod_prodotto_padre  	= :fs_prodotto_finito and
							cod_versione_padre  	= :fs_versione and
							cod_prodotto_figlio 	= :str_varianti[ll_i].cod_mp_variante and
							cod_versione_figlio 	= :str_varianti[ll_i].cod_versione_figlio;
		
				if sqlca.sqlcode < 0 then
					fs_messaggio = "Configuratore uof_ins_varianti: Errore in fase di ricerca formula Vuoto per Pieno su prodotto padre " + fs_prodotto_finito + " prodotto figlio " + ls_cod_prodotto_figlio[ll_conteggio] + "  DETT:" + sqlca.sqlerrtext
				elseif sqlca.sqlcode = 0 and not isnull(ls_cod_formula_vpp) then
					ls_cod_formula_quan_utilizzo = ls_cod_formula_vpp
				end if
				//-----------------------------------------------------------------------------------------------
				
				// Ptenda 28-9/2011
				// importante che rimangano le prossime 2 righe di codice; infatti se la quantità della materia prima automatica è zero, deve prendere la quantità in distinta.
				if not isnull(str_varianti[ll_i].quan_utilizzo) and str_varianti[ll_i].quan_utilizzo > 0 then ld_quan_utilizzo = str_varianti[ll_i].quan_utilizzo
				if not isnull(str_varianti[ll_i].quan_tecnica) and str_varianti[ll_i].quan_tecnica > 0 then ld_quan_tecnica = str_varianti[ll_i].quan_tecnica

				if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
				if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0
				
				//formula q.tà utilizzo ##########################
				if not isnull(ls_cod_formula_quan_utilizzo) and ls_cod_formula_quan_utilizzo<>"" then
					ll_ordine[1] = il_anno_registrazione
					ll_ordine[2] = il_num_registrazione
					ll_ordine[3] = il_prog_riga
					
					lstr_distinta.cod_prodotto_padre = fs_prodotto_finito
					lstr_distinta.cod_versione_padre = fs_versione
					lstr_distinta.num_sequenza = ll_num_sequenza
					lstr_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio[ll_conteggio]
					lstr_distinta.cod_versione_figlio = ls_cod_versione_figlio[ll_conteggio]
					
					li_ret = f_calcola_formula_db_v2(ls_cod_formula_quan_utilizzo, ll_ordine[],  lstr_distinta, is_gestione, ld_quan_utilizzo, ls_temp, ls_tipo_ritorno, fs_messaggio)
					if li_ret<0 then
						fs_messaggio = 	"Calcolo Q.ta utilizzo; Ramo ("+fs_prodotto_finito+","+fs_versione+","+string(ll_num_sequenza)+","+ls_cod_prodotto_figlio[ll_conteggio]+","+ls_cod_versione_figlio[ll_conteggio]+"): " + &
												fs_messaggio			
						close righe_distinta_3;
						rollback;
						return -1
					end if
					
					//OK
					ll_index = upperbound(is_elaborazione_formule_qta[]) + 1
					if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
					is_elaborazione_formule_qta[ll_index] = "RAMO " + ls_cod_prodotto_figlio[ll_conteggio] + " (VAR. "+ ls_nuova_variante + ") - F.LA " + ls_cod_formula_quan_utilizzo + " - RIS. " + string(ld_quan_utilizzo,"#####.00") + " OK!"
		
				end if
				
				if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
				//##########################################
				
				//formula su quantità tecnica ##########################
				if not isnull(ls_cod_formula_quan_tecnica) and ls_cod_formula_quan_tecnica<>"" then
					ll_ordine[1] = il_anno_registrazione
					ll_ordine[2] = il_num_registrazione
					ll_ordine[3] = il_prog_riga
					
					lstr_distinta.cod_prodotto_padre = fs_prodotto_finito
					lstr_distinta.cod_versione_padre = fs_versione
					lstr_distinta.num_sequenza = ll_num_sequenza
					lstr_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio[ll_conteggio]
					lstr_distinta.cod_versione_figlio = ls_cod_versione_figlio[ll_conteggio]
					
					li_ret = f_calcola_formula_db_v2(ls_cod_formula_quan_tecnica, ll_ordine[],  lstr_distinta, is_gestione, ld_quan_tecnica, ls_temp, ls_tipo_ritorno, fs_messaggio)
					if li_ret<0 then
						fs_messaggio = 	"Calcolo Q.ta tecnica; Ramo ("+fs_prodotto_finito+","+fs_versione+","+string(ll_num_sequenza)+","+ls_cod_prodotto_figlio[ll_conteggio]+","+ls_cod_versione_figlio[ll_conteggio]+"): " + &
												fs_messaggio			
						close righe_distinta_3;
						rollback;
						return -1
					end if
					
					//OK
					ll_index = upperbound(is_elaborazione_formule_qtatec[]) + 1
					if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0
					is_elaborazione_formule_qtatec[ll_index] = "RAMO " + ls_cod_prodotto_figlio[ll_conteggio] + " (VAR. "+ ls_nuova_variante + ") - F.LA " + ls_cod_formula_quan_utilizzo + " - RIS. " + string(ld_quan_utilizzo,"#####.00") + " OK!"
					
				end if
				
				if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0
				//##########################################
				
				//faccio update delle quantità calcolate
				ls_sql_upd ="update " + ls_tabella + " "+&
								"set quan_tecnica="+f_double_to_string(ld_quan_tecnica)+ "," +&
									"quan_utilizzo="+f_double_to_string(ld_quan_utilizzo)+" "+ &
								"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
											"anno_registrazione="+string(il_anno_registrazione)+" and "+&
											"num_registrazione="+string(il_num_registrazione)+" and "+&
											ls_progressivo+"="+string(il_prog_riga)+" and "+&
											"cod_prodotto_padre='"+fs_prodotto_finito+"' and "+&
											"cod_versione='"+fs_versione+"' and "+&
											"num_sequenza="+string(ll_num_sequenza)+" and "+&
											"cod_prodotto_figlio='"+ls_cod_prodotto_figlio[ll_conteggio]+"' and "+&
											"cod_versione_figlio='"+ls_cod_versione_figlio[ll_conteggio]+"' "
											
				execute immediate :ls_sql_upd;
				
				if sqlca.sqlcode = -1 then
					fs_messaggio = "Configuratore uof_ins_varianti: Errore in update q.tà utilizzo e tecnica nella tabella " + ls_tabella + ": "+ sqlca.sqlerrtext
					close righe_distinta_3;
					rollback;
					return -1
				end if
				
				// inserisco anche tutti gli eventuali reparti di produzione
				if is_gestione = "ORD_VEN" then
				
					for ll_y = 1 to upperbound(ls_cod_reparto_produzione)
						
						INSERT INTO varianti_det_ord_ven_prod  
								( cod_azienda,   
								  anno_registrazione,   
								  num_registrazione,   
								  prog_riga_ord_ven,   
								  cod_prodotto_padre,   
								  num_sequenza,   
								  cod_prodotto_figlio,   
								  cod_versione,   
								  cod_versione_figlio,   
								  cod_reparto )  
						VALUES ( :s_cs_xx.cod_azienda,
								  :il_anno_registrazione,   
								  :il_num_registrazione,   
								  :il_prog_riga,   
								  :fs_prodotto_finito,   
								  :ll_num_sequenza,   
								  :ls_cod_prodotto_figlio[ll_conteggio],   
								  :fs_versione,   
								  :ls_cod_versione_figlio[ll_conteggio],   
								  :ls_cod_reparto_produzione[ll_y] )  ;

						if sqlca.sqlcode = -1 then
							fs_messaggio = "Configuratore uof_ins_varianti~r~nErrore nell'inserimento della dei reparti produttivi.Dettaglio errore " + sqlca.sqlerrtext
							close righe_distinta_3;
							rollback;
							return -1
						end if
					next
				end if
				
				// ----- EnMe + Donèto 13-02-2014 (Dekora) ----------------------------
				// se il gruppo variante ha il ls_flag_ins_varianti_ricorsivo = "S" procedo in ricorsione con la nuova variante
				if ls_flag_ins_varianti_ricorsivo = "S" then
					ls_cod_prodotto_figlio[ll_conteggio] = str_varianti[ll_i].cod_mp_variante
					ls_cod_versione_figlio[ll_conteggio]  = str_varianti[ll_i].cod_versione_figlio
				end if
				// ----- fine modifica x Dekora
				
				//siccome il cod_gruppo_variante è stata trovata, inutile continuare nel ciclo for, quindi esci
				exit
				
			end if	
		next
		
	else
		// EnMe 07/02/2012: aggiunto per gestione stabilimento e reparto produzione nelle varianti
		// gruppo variante assente, ma verifico de creare la variante
		// dal momento che devo inserire il deposito ed il reparto.
		
		// verifico se al prodotto padre è collegata una fase di lavorazione
		ls_cod_reparto_produzione = ls_vuoto
		setnull(ls_cod_deposito_produzione)
		
		if is_gestione = "ORD_VEN" then
		
			li_ret = luo_funzioni_1.uof_estrai_reparto_stabilimento (  ls_cod_prodotto_figlio[ll_conteggio], ls_cod_versione_figlio[ll_conteggio],is_gestione, il_anno_registrazione, il_num_registrazione, ref ls_cod_deposito_produzione, ref ls_cod_reparto_produzione[], ref ls_errore )			
										
			if li_ret < 0 then
				fs_messaggio = ls_errore
				close righe_distinta_3;
				rollback;
				return -1
			end if
			
			// se ll_ret = 1 vuol dire che non c'è la fase di lavorazione.
			
			// ho trovato lo stabilimento e i reparti associati quindi creo la variante solo per il cambio stabilimento e reparto
			if li_ret = 0 then
				
				//predispongo il secondo semaforo (SR Vendita_semilavorati)
				//in modo che non erediti la fase in distinta base
				if ib_fase_su_radice then	ib_nofase_su_figli = false
			
				ls_sql_del ="delete from " + ls_tabella + &
								" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
								" anno_registrazione = " + string(il_anno_registrazione) + " and " + &
								" num_registrazione = " + string(il_num_registrazione) + " and " + &
								ls_progressivo + " = " + string(il_prog_riga) + " and " + &
								" cod_prodotto_padre = '" + fs_prodotto_finito + "' and " + &
								" num_sequenza = " + string(ll_num_sequenza) + " and " + &
								" cod_prodotto_figlio = '" + ls_cod_prodotto_figlio[ll_conteggio] + "' and " + &
								" cod_versione_figlio = '" + ls_cod_versione_figlio[ll_conteggio] + "' and " + &
								" cod_versione = '" + fs_versione + "'"
				
				execute immediate :ls_sql_del;
				if sqlca.sqlcode = -1 then
					fs_messaggio = "Configuratore uof_ins_varianti~r~nErrore nella Cancellazione della Variante da varianti_det_ord_ven. Dettaglio " + sqlca.sqlerrtext
					rollback;
					return -1
				end if
				
				if is_gestione = "ORD_VEN" then
				
					ls_sql_del ="delete from varianti_det_ord_ven_prod " + &
									" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
									" anno_registrazione = " + string(il_anno_registrazione) + " and " + &
									" num_registrazione = " + string(il_num_registrazione) + " and " + &
									ls_progressivo + " = " + string(il_prog_riga) + " and " + &
									" cod_prodotto_padre = '" + fs_prodotto_finito + "' and " + &
									" num_sequenza = " + string(ll_num_sequenza) + " and " + &
									" cod_prodotto_figlio = '" + ls_cod_prodotto_figlio[ll_conteggio] + "' and " + &
									" cod_versione_figlio = '" + ls_cod_versione_figlio[ll_conteggio] + "' and " + &
									" cod_versione = '" + fs_versione + "'"
					
					execute immediate :ls_sql_del;
					if sqlca.sqlcode = -1 then
						fs_messaggio = "Configuratore uof_ins_varianti~r~nErrore nella Cancellazione della Variante da varianti_det_ord_ven_PROD. Dettaglio " + sqlca.sqlerrtext
						rollback;
						return -1
					end if
				end if

				setnull(ls_cod_formula_quan_utilizzo)
		
				if isnull(ls_cod_misura) then ls_cod_misura = ""
				if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0
				if isnull(ld_dim_x) then ld_dim_x = 0
				if isnull(ld_dim_y) then ld_dim_y = 0
				if isnull(ld_dim_z) then ld_dim_z = 0
				if isnull(ld_dim_t) then ld_dim_t = 0
				if isnull(ld_coef_calcolo) then ld_coef_calcolo = 0
				if isnull(ls_des_estesa) then ls_des_estesa = ""
				if isnull(ls_formula_tempo) then ls_formula_tempo = ""
				
				ls_des_estesa = f_cazzillo(ls_des_estesa)
				
				if isnull(ls_flag_materia_prima_distinta) then
					ls_flag_materia_prima = "N"
				else
					ls_flag_materia_prima = ls_flag_materia_prima_distinta
				end if
				
				if isnull(ls_cod_deposito_produzione) then 
					ls_cod_deposito_produzione = " null "
				else
					ls_cod_deposito_produzione = " '" + f_cazzillo(ls_cod_deposito_produzione) + "' "
				end if
				
				if isnull(ls_cod_misura) then 
					ls_cod_misura = "null"
				else
					ls_cod_misura = "'" + ls_cod_misura + "'"
				end if
				
				
				ls_sql_ins =" insert into " + ls_tabella + &
								"(cod_azienda," + &
								"anno_registrazione," + &
								"num_registrazione," + &
								ls_progressivo + "," + &
								"cod_prodotto_padre," + &
								"num_sequenza," + &
								"cod_prodotto_figlio," + &
								"cod_versione_figlio," + &
								"cod_versione," + &
								"cod_misura," + &
								"cod_prodotto," + &
								"cod_versione_variante," + &
								"quan_tecnica," + &
								"quan_utilizzo," + &
								"dim_x," + &
								"dim_y," + &
								"dim_z," + &
								"dim_t," + &
								"coef_calcolo," + &
								"flag_esclusione," + &
								"formula_tempo," + &
								"flag_materia_prima," + &
								"des_estesa"
					
				if is_gestione = "ORD_VEN" then
					ls_sql_ins += ", cod_deposito_produzione)"
				else
					ls_sql_ins += " ) "
				end if
					
				ls_sql_ins += " VALUES ('" +  s_cs_xx.cod_azienda + "', " + &
									string(il_anno_registrazione) + ", " + &   
									string(il_num_registrazione) + ", " + &   
									string(il_prog_riga) + ", '" + &   
									fs_prodotto_finito + "', " + &
									string(ll_num_sequenza) + ", '" + &   
									ls_cod_prodotto_figlio[ll_conteggio] + "', '" + &
									ls_cod_versione_figlio[ll_conteggio] + "', '" + &
									fs_versione + "'," + &
									ls_cod_misura + ", '" + &
									ls_cod_prodotto_figlio[ll_conteggio] + "', '" + &
									ls_cod_versione_figlio[ll_conteggio] + "', " + &
									f_double_to_string(ld_quan_tecnica) + ", " + &   
									f_double_to_string(ld_quan_utilizzo) + ", " + &
									f_double_to_string(ld_dim_x) + ", " + &   
									f_double_to_string(ld_dim_y) + ", " + &   
									f_double_to_string(ld_dim_z) + ", " + &   
									f_double_to_string(ld_dim_t) + ", " + &   
									f_double_to_string(ld_coef_calcolo) + ", '" + &   
									"N', '" + &
									ls_formula_tempo + "', '" + &
									ls_flag_materia_prima + "', '" + &
									ls_des_estesa + "' "
				if is_gestione = "ORD_VEN" then
					ls_sql_ins += ", "+ls_cod_deposito_produzione+")"
				else
					ls_sql_ins += " )"
				end if
		
				execute immediate :ls_sql_ins;
				if sqlca.sqlcode = -1 then
					fs_messaggio = "Configuratore uof_ins_varianti~r~nErrore nell'inserimento della Variante nella tabella " + ls_tabella + ". Dettaglio errore " + sqlca.sqlerrtext
					close righe_distinta_3;
					rollback;
					return -1
				end if
				
				// inserisco anche tutti gli eventuali reparti di produzione
				
				if is_gestione = "ORD_VEN" then
					for ll_y = 1 to upperbound(ls_cod_reparto_produzione)
						
						INSERT INTO varianti_det_ord_ven_prod  
								( cod_azienda,   
								  anno_registrazione,   
								  num_registrazione,   
								  prog_riga_ord_ven,   
								  cod_prodotto_padre,   
								  num_sequenza,   
								  cod_prodotto_figlio,   
								  cod_versione,   
								  cod_versione_figlio,   
								  cod_reparto )  
						VALUES ( :s_cs_xx.cod_azienda,
								  :il_anno_registrazione,   
								  :il_num_registrazione,   
								  :il_prog_riga,   
								  :fs_prodotto_finito,   
								  :ll_num_sequenza,   
								  :ls_cod_prodotto_figlio[ll_conteggio],   
								  :fs_versione,   
								  :ls_cod_versione_figlio[ll_conteggio],   
								  :ls_cod_reparto_produzione[ll_y] )  ;
	
						if sqlca.sqlcode = -1 then
							fs_messaggio = "Configuratore uof_ins_varianti~r~nErrore nell'inserimento della dei reparti produttivi.Dettaglio errore " + sqlca.sqlerrtext
							close righe_distinta_3;
							rollback;
							return -1
						end if
					next
				end if
			end if
			
		end if
		
	end if
	ll_conteggio++	
	
loop

close righe_distinta_3;


for ll_t = 1 to ll_conteggio -1

   SELECT count(*)
   INTO   :li_count  
   FROM   distinta  
   WHERE  cod_azienda        = :s_cs_xx.cod_azienda
	AND    cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t]
	and    cod_versione       = :ls_cod_versione_figlio[ll_t];

	if sqlca.sqlcode=-1 then // Errore
		fs_messaggio = "Configuratore uof_ins_varianti~r~nErrore in lettura tabella distinta. Dettaglio " + SQLCA.SQLErrText
		rollback;
		return -1
	end if

	if li_count > 0 then
		li_risposta = uof_ins_varianti(ls_cod_prodotto_figlio[ll_t], ls_cod_versione_figlio[ll_t], ref fs_messaggio)
		if li_risposta = -1 then return -1
	end if
next

destroy luo_funzioni_1
return 0
end function

public function integer uof_ricerca_quan_mp (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quan_prodotto, string fs_cod_gruppo_variante, long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_registrazione, string fs_tab_varianti, ref string fs_cod_materia_prima, ref decimal fd_quan_materia_prima, ref string fs_errore);/* ------------------------------------------------------------------------------------
	Funzione che trova la materia prima e la sua quantità partendo dal gruppo variante
 nella tabella <tabella_varianti>

 nome: uof_ricerca_quantita_mp
 tipo: integer

 return:
 		-1 errore
			 0 trovato materia prima tutto OK
  		 1 trovato nulla
			 2 valido solo all'interno indica l'uscita

	Variabili passate: 		nome					 tipo				passaggio per			commento
							

	Creata il 10-05-2000 Enrico 
	modificata il 31/10/2006 per adeguarla alla nuova gestione distinta base con versioni figli diversi dalla versione padre
 ------------------------------------------------------------------------------------
*/
string  ls_cod_prodotto_figlio[],ls_cod_prodotto_variante, ls_sql, ls_flag_materia_prima, &
		  ls_flag_materia_prima_variante,ls_errore, ls_cod_gruppo_variante, ls_cod_versione_figlio[], &
		  ls_cod_versione_variante
long    ll_conteggio,ll_t,ll_max, ll_i, ll_cont, ll_num_legami, ll_num_sequenza,ll_test
integer li_risposta
dec{4}  ldd_quantita_utilizzo,ldd_quan_utilizzo_variante

ll_conteggio = 1

declare cu_varianti dynamic cursor for sqlsa;

declare righe_distinta_3 cursor for 
select  cod_prodotto_figlio,
        cod_versione_figlio,
        quan_utilizzo,
		  flag_materia_prima,
		  num_sequenza
from    distinta 
where   cod_azienda = :s_cs_xx.cod_azienda 
and     cod_prodotto_padre = :fs_cod_prodotto
and     cod_versione=:fs_cod_versione;

open righe_distinta_3;

do while 1 = 1

	fetch righe_distinta_3 
	into  :ls_cod_prodotto_figlio[ll_conteggio],
		   :ls_cod_versione_figlio[ll_conteggio],
		   :ldd_quantita_utilizzo,
			:ls_flag_materia_prima,
			:ll_num_sequenza;

   if (sqlca.sqlcode = 100) then 
		close righe_distinta_3;	
		exit
	end if

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close righe_distinta_3;
		return -1
	end if
	
	if not isnull(fl_anno_registrazione) and not isnull(fl_num_registrazione) then
		
		ls_sql = "select quan_utilizzo, " + &
					" cod_prodotto, " + &
					" cod_versione_variante, " + &
					" flag_materia_prima " + &
					" from " + fs_tab_varianti + &
					" where  cod_azienda='" + s_cs_xx.cod_azienda + "'"
		choose case fs_tab_varianti
			case "varianti_commesse"
					ls_sql = ls_sql + " and anno_commessa=" + string(fl_anno_registrazione) + &
											" and num_commessa=" + string(fl_num_registrazione) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_off_ven"
					ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_registrazione) + &
											" and num_registrazione=" + string(fl_num_registrazione) + &
											" and prog_riga_off_ven=" + string(fl_prog_registrazione) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_ord_ven"
					ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_registrazione) + &
											" and num_registrazione=" + string(fl_num_registrazione) + &
											" and prog_riga_ord_ven=" + string(fl_prog_registrazione) + &
											" and flag_esclusione<>'S'"
			case "varianti_det_trattative"
					ls_sql = ls_sql + " and anno_trattativa=" + string(fl_anno_registrazione) + &
											" and num_trattativa=" + string(fl_num_registrazione) + &
											" and prog_trattativa=" + string(fl_prog_registrazione) + &
											" and flag_esclusione<>'S'"
		end choose
		ls_sql = ls_sql + " and cod_prodotto_padre='" + fs_cod_prodotto + "'" + &
								" and cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_conteggio] + "'" + &
								" and cod_versione_figlio='" + ls_cod_versione_figlio[ll_conteggio] + "'" + &
								" and cod_versione='" + fs_cod_versione + "'"
		
		prepare sqlsa from :ls_sql;
		open dynamic cu_varianti;

		if sqlca.sqlcode<>0 then
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if


		fetch cu_varianti 
		into :ldd_quan_utilizzo_variante, 
			  :ls_cod_prodotto_variante,
			  :ls_cod_versione_variante,
			  :ls_flag_materia_prima_variante;
				
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			close cu_varianti;
			return -1
		end if

		if sqlca.sqlcode =0  then
			ldd_quantita_utilizzo = ldd_quan_utilizzo_variante
			ls_flag_materia_prima = ls_flag_materia_prima_variante
			
			select cod_gruppo_variante
			into   :ls_cod_gruppo_variante
			from   distinta_gruppi_varianti
			where  cod_azienda         = :s_cs_xx.cod_azienda and
					 cod_prodotto_padre  = :fs_cod_prodotto and
					 num_sequenza        = :ll_num_sequenza and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
					 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
					 cod_versione        = :fs_cod_versione;
					 
			if sqlca.sqlcode = 0 then
				if ls_cod_gruppo_variante = fs_cod_gruppo_variante then
					fs_cod_materia_prima   = ls_cod_prodotto_figlio[ll_conteggio]
					fd_quan_materia_prima  = ldd_quantita_utilizzo
					close cu_varianti;
					close righe_distinta_3;
					return 0
				end if
			end if
			ls_cod_prodotto_figlio[ll_conteggio] = ls_cod_prodotto_variante
		end if
		
		ldd_quantita_utilizzo = ldd_quantita_utilizzo * fd_quan_prodotto
		ll_conteggio++	
	
		close cu_varianti;
		// ----------------- verifico se gruppo variante corrisponde ------------------
	end if
loop

close righe_distinta_3;

for ll_t = 1 to ll_conteggio -1

   if ls_flag_materia_prima = 'N' then
		
		select count(*)
		into   :ll_test
		from   distinta  
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
				 cod_versione = :ls_cod_versione_figlio[ll_t];
		
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
	
		if ll_test = 0 or isnull(ll_test) then				// è una materia prima
			if not isnull(fl_anno_registrazione) and not isnull(fl_num_registrazione) then		// verifico se ha varianti
				ls_sql = "select quan_utilizzo, " + &
							" cod_prodotto, " + &
							" cod_versione_variante " + &
							" from " + fs_tab_varianti + &
							" where  cod_azienda='" + s_cs_xx.cod_azienda + "'"
				choose case fs_tab_varianti
					case "varianti_commesse"
							ls_sql = ls_sql + " and anno_commessa=" + string(fl_anno_registrazione) + &
													" and num_commessa=" + string(fl_num_registrazione) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_off_ven"
							ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_registrazione) + &
													" and num_registrazione=" + string(fl_num_registrazione) + &
													" and prog_riga_off_ven=" + string(fl_prog_registrazione) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_ord_ven"
							ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_registrazione) + &
													" and num_registrazione=" + string(fl_num_registrazione)  + &
													" and prog_riga_ord_ven=" + string(fl_prog_registrazione) + &
													" and flag_esclusione<>'S'"
					case "varianti_det_trattative"
							ls_sql = ls_sql + " and anno_trattativa=" + string(fl_anno_registrazione) + &
													" and num_trattativa=" + string(fl_num_registrazione) + &
													" and prog_trattativa=" + string(fl_prog_registrazione) + &
													" and flag_esclusione<>'S'"
				end choose
				ls_sql = ls_sql + " and    cod_prodotto_padre='" + fs_cod_prodotto + "'" + &
										" and    cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_t] + "'" + &
										" and    cod_versione_figlio='" + ls_cod_versione_figlio[ll_t] + "'" + &
										" and    cod_versione='" + fs_cod_versione + "'" 
				
				prepare sqlsa from :ls_sql;
	
				open dynamic cu_varianti;
	
				fetch cu_varianti 
				into  :ldd_quan_utilizzo_variante, 
						:ls_cod_prodotto_variante,
						:ls_cod_versione_variante;
			
				
				if sqlca.sqlcode < 0 then 
					fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
					close cu_varianti;
					return -1
				end if
			
				if sqlca.sqlcode <> 100  then
					ldd_quan_utilizzo_variante    = ldd_quan_utilizzo_variante * fd_quan_prodotto
					ldd_quantita_utilizzo         = ldd_quan_utilizzo_variante
					ls_cod_prodotto_figlio[ll_t]  = ls_cod_prodotto_variante
					ls_cod_versione_figlio[ll_t]  = ls_cod_versione_variante
					
				end if
	
				close cu_varianti;
			end if
			
		else		// è una semilavorato; vado ad esaminare ulteriori livelli
			
			li_risposta = uof_ricerca_quan_mp(ls_cod_prodotto_figlio[ll_t], ls_cod_versione_figlio[ll_t], ldd_quantita_utilizzo, &
			                                  fs_cod_gruppo_variante, fl_anno_registrazione, fl_num_registrazione, fl_prog_registrazione, fs_tab_varianti, &
														 fs_cod_materia_prima, fd_quan_materia_prima, fs_errore)
	
			if li_risposta = -1 then 
				fs_errore = ls_errore
				return -1
			end if
			if li_risposta = 0 then
				return 0
			end if
		end if
	else				// flag mp = "S"
		if not isnull(fl_anno_registrazione) and not isnull(fl_num_registrazione) then
			ls_sql = "select quan_utilizzo, " + &
						" cod_prodotto, " + &
						" cod_versione_variante " + &
						" from " + fs_tab_varianti + &
						" where  cod_azienda='" + s_cs_xx.cod_azienda + "'"
			choose case fs_tab_varianti
				case "varianti_commesse"
						ls_sql = ls_sql + " and anno_commessa=" + string(fl_anno_registrazione) + &
												" and num_commessa=" + string(fl_num_registrazione) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_off_ven"
						ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_registrazione) + &
												" and num_registrazione=" + string(fl_num_registrazione) + &
												" and prog_riga_off_ven=" + string(fl_prog_registrazione) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_ord_ven"
						ls_sql = ls_sql + " and anno_registrazione=" + string(fl_anno_registrazione) + &
												" and num_registrazione=" + string(fl_num_registrazione)  + &
												" and prog_riga_ord_ven=" + string(fl_prog_registrazione) + &
												" and flag_esclusione<>'S'"
				case "varianti_det_trattative"
						ls_sql = ls_sql + " and anno_trattativa=" + string(fl_anno_registrazione) + &
												" and num_trattativa=" + string(fl_num_registrazione) + &
												" and prog_trattativa=" + string(fl_prog_registrazione) + &
												" and flag_esclusione<>'S'"
			end choose
			ls_sql = ls_sql + " and    cod_prodotto_padre='" + fs_cod_prodotto + "'" + &
									" and    cod_prodotto_figlio='" + ls_cod_prodotto_figlio[ll_t] + "'" + &
									" and    cod_versione_figlio='" + ls_cod_versione_figlio[ll_t] + "'" + &
									" and    cod_versione='" + fs_cod_versione + "'" 
			
			prepare sqlsa from :ls_sql;

			open dynamic cu_varianti;

			fetch cu_varianti 
			into  :ldd_quan_utilizzo_variante, 
					:ls_cod_prodotto_variante,
					:ls_cod_versione_variante;
		
			if sqlca.sqlcode < 0 then 
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				close cu_varianti;
				return -1
			end if
		
			if sqlca.sqlcode <> 100  then
				ldd_quan_utilizzo_variante = ldd_quan_utilizzo_variante * fd_quan_prodotto
				ldd_quantita_utilizzo=ldd_quan_utilizzo_variante
				ls_cod_prodotto_figlio[ll_t]=ls_cod_prodotto_variante
				ls_cod_versione_figlio[ll_t]=ls_cod_versione_variante
			end if
			close cu_varianti;
			
			// --- verifico corrispondenza del gruppo variante -----------------------
			select cod_gruppo_variante
			into   :ls_cod_gruppo_variante
			from   distinta_gruppi_varianti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto_padre = :fs_cod_prodotto and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_t] and
					 cod_versione_figlio = :ls_cod_versione_figlio[ll_t] and
					 cod_versione = :fs_cod_versione;
			if sqlca.sqlcode = 0 then
				if ls_cod_gruppo_variante = fs_cod_gruppo_variante then
					fs_cod_materia_prima = ls_cod_prodotto_figlio[ll_t]
					fd_quan_prodotto = ldd_quantita_utilizzo
					return 0
				end if
			end if
	
		end if
	end if
next
return 1
end function

public function integer uof_valorizza_formule (string fs_cod_prodotto, string fs_cod_versione, ref string fs_errore);/*	Funzione che scorre la distinta base e inserisce la variante di tipo codice prodotto (fs_cod_prodotto_var)
// FUNZIONE CHE VALORIZZA TUTTE LE FORMULE PRESENTI DELLA DISTINTA CREANDO LA RELATIVA VARIANTE.
//
// nome: uof_valirizza_formule
// tipo: integer
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							
//							 fs_cod_prodotto	 		string			valore
//							 fs_cod_versione			string			valore
//
//		Creata il 01/01/2000
//		Autore ENRICO MENEGOTTO


27/9/2006 Richiesta di Daniele Riello; impostare il flag_materia_prima allo stesso valore 
 													che ha nella distinta.

31/10/2006 Adeguamento della funzione alla nuova gestione distinta cpn versione del figlio.

//21/05/2013 Aggiunto calcolo formule su cod. prodotto figlio e cod. versione figlio
*/

string						ls_cod_prodotto_figlio[],ls_test, ls_tabella, ls_progressivo, ls_cod_misura, ls_cod_formula_quan_utilizzo, &
							ls_des_estesa, ls_formula_tempo, ls_sql_del, ls_sql_ins, ls_flag_materia_prima,ls_cod_versione_figlio[],&
							ls_cod_prodotto_variante, ls_cod_versione_variante, ls_tabella_comp, ls_cod_formula_quan_tecnica, ls_cod_formula_prod_figlio, &
							ls_cod_formula_vers_figlio, ls_nuovo_figlio, ls_nuova_versione_figlio, ls_temp, ls_tipo_ritorno,ls_sql_upd, ls_messaggio
							
dec{4}					ld_quan_tecnica, ld_quan_utilizzo, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_coef_calcolo

long						ll_conteggio,ll_t,ll_num_sequenza, ll_cont_1, ll_ordine[], ll_num_sequenza_new,ll_index

integer					li_risposta, li_count

s_chiave_distinta		lstr_distinta



choose case is_gestione
		
	case "ORD_VEN"
		ls_tabella = "varianti_det_ord_ven"
		ls_progressivo = "prog_riga_ord_ven"
		ls_tabella_comp = "comp_det_ord_ven"
		
	case "OFF_VEN"
		ls_tabella = "varianti_det_off_ven"
		ls_progressivo = "prog_riga_off_ven"
		ls_tabella_comp = "comp_det_off_ven"
		
end choose

ll_conteggio = 1

declare righe_distinta_3 cursor for 
select  cod_prodotto_figlio,
		  cod_versione_figlio,
		  num_sequenza,
		  cod_misura,
		  quan_tecnica,
		  quan_utilizzo,
		  dim_x,
		  dim_y,
		  dim_z,
		  dim_t,
		  coef_calcolo,
		  des_estesa,
		  formula_tempo,
		  cod_formula_quan_utilizzo,
		  flag_materia_prima,
		  cod_formula_quan_tecnica,
		  cod_formula_prod_figlio,
		  cod_formula_vers_figlio
from    distinta 
where   cod_azienda = :s_cs_xx.cod_azienda  and
		  cod_prodotto_padre = :fs_cod_prodotto and
		  cod_versione=:fs_cod_versione;

open righe_distinta_3;

do while true
	fetch righe_distinta_3 
	into  :ls_cod_prodotto_figlio[ll_conteggio],
			:ls_cod_versione_figlio[ll_conteggio],
			:ll_num_sequenza,
			:ls_cod_misura,
			:ld_quan_tecnica,
			:ld_quan_utilizzo,
			:ld_dim_x,
			:ld_dim_y,
			:ld_dim_z,
			:ld_dim_t,
			:ld_coef_calcolo,
			:ls_des_estesa,
			:ls_formula_tempo,
			:ls_cod_formula_quan_utilizzo,
			:ls_flag_materia_prima,
			:ls_cod_formula_quan_tecnica,
			:ls_cod_formula_prod_figlio,
			:ls_cod_formula_vers_figlio;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

	if sqlca.sqlcode<>0 then
		fs_errore = "Errore in fase di fetch cursore righe_distinta_3 (uo_conf_varianti.uof_valorizza_formule) ~r~n" + SQLCA.SQLErrText
		close righe_distinta_3;
		rollback;
		return -1
	end if
	
	
	// verifico presenza o meno della variante --------------------------------------------------------------------------------------------------
	choose case is_gestione
		case "ORD_VEN"
			select count(*)
			into   :ll_cont_1
			from   varianti_det_ord_ven
			where  cod_azienda         = :s_cs_xx.cod_azienda and
					 anno_registrazione  = :il_anno_registrazione and
					 num_registrazione   = :il_num_registrazione and
					 prog_riga_ord_ven   = :il_prog_riga and
					 cod_prodotto_padre  = :fs_cod_prodotto and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
					 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
					 cod_versione        = :fs_cod_versione and
					 num_sequenza = :ll_num_sequenza;
					 
		case "OFF_VEN"
			select count(*)
			into   :ll_cont_1
			from   varianti_det_off_ven
			where  cod_azienda         = :s_cs_xx.cod_azienda and
					 anno_registrazione  = :il_anno_registrazione and
					 num_registrazione   = :il_num_registrazione and
					 prog_riga_off_ven   = :il_prog_riga and
					 cod_prodotto_padre  = :fs_cod_prodotto and
					 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
					 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
					 cod_versione        = :fs_cod_versione and
					 num_sequenza = :ll_num_sequenza;
					 
	end choose
	
	//-------------------------------------------
	if ll_cont_1 = 0 then						 
		//NON C'è LA VARIANTE (uso la formula presente sul ramo di distinta	) #####################################################
		//######################################################################################
		//qui valuto le formule eventuali su prodotto figlio e versione figlio
		//la funzione modificherà gli argomenti, solo se sono diversi
		ls_nuovo_figlio = ls_cod_prodotto_figlio[ll_conteggio]
		ls_nuova_versione_figlio = ls_cod_versione_figlio[ll_conteggio]
		ll_num_sequenza_new = ll_num_sequenza
		
		if (not isnull(ls_cod_formula_prod_figlio) and ls_cod_formula_prod_figlio<>"") or (not isnull(ls_cod_formula_vers_figlio) and ls_cod_formula_vers_figlio<>"") then
			
			li_risposta = uof_valorizza_formule_prod_figlio(	fs_cod_prodotto, fs_cod_versione, ll_num_sequenza, ls_nuovo_figlio, ls_nuova_versione_figlio, ll_num_sequenza_new, &
																			ls_cod_formula_prod_figlio, ls_cod_formula_vers_figlio, fs_errore)
			
			if li_risposta<0 then
				//mettiamo nel messaggio anche l'informazione sul ramo di distinta in cui la formula ha dato errore
				fs_errore = 	"Ramo ("+fs_cod_prodotto+","+fs_cod_versione+","+string(ll_num_sequenza)+","+ls_cod_prodotto_figlio[ll_conteggio]+","+ls_cod_versione_figlio[ll_conteggio]+"): " + &
										fs_errore
				close righe_distinta_3;
				rollback;
				return -1
			end if
			
			//LASCIARE COMMENTATO QUESTO, PERCHè NON HA SENSO
//			//se ho cambiato ramo devo rileggere i valori, formule quantità comprese
//			if ls_nuovo_figlio<>ls_cod_prodotto_figlio[ll_conteggio] or ls_nuova_versione_figlio<>ls_cod_versione_figlio[ll_conteggio] then
//				
//				 select 	cod_formula_quan_utilizzo, cod_formula_quan_tecnica,
//				 			quan_tecnica, quan_utilizzo, cod_misura, dim_x, dim_y, dim_z, dim_t,
//							coef_calcolo, des_estesa, formula_tempo, flag_materia_prima
//				into	:ls_cod_formula_quan_utilizzo, :ls_cod_formula_quan_tecnica,
//						:ld_quan_tecnica, :ld_quan_utilizzo, :ls_cod_misura, :ld_dim_x, :ld_dim_y, :ld_dim_z, :ld_dim_t,
//						:ld_coef_calcolo, :ls_des_estesa, :ls_formula_tempo, :ls_flag_materia_prima
//				from    distinta 
//				where   cod_azienda = :s_cs_xx.cod_azienda  and
//						  cod_prodotto_padre = :fs_cod_prodotto and cod_versione=:fs_cod_versione and
//						  num_sequenza=:ll_num_sequenza_new and cod_prodotto_figlio=:ls_nuovo_figlio and cod_versione_figlio=:ls_nuova_versione_figlio;
//						  
//			end if

		end if
		//######################################################################################
		
		//prima l'inserimento veniva fatto solo se c'era una formula quantità utilizzo da calcolare
		//adesso va fatto se c'è una formula q.tà utilizzo, q.tà tecnica o sono stati cambiati prodotto figlio e/o versione figlio
		
		//Prima va fatto l'inserimento del codice prodotto e versione in tabella varianti, e poi valutate le formule sulle quantità,
		//questo per fare in modo che eventuali variabili su prodotto figlio e relativa versione vengano lette dalla tabella varianti con i nuovi valori eventualmente sostituiti
	
	
		//Se non devo inserire, passo al ramo successivo
		if not isnull(ls_cod_formula_quan_utilizzo) or not isnull(ls_cod_formula_quan_tecnica) or &
				ls_nuovo_figlio<>ls_cod_prodotto_figlio[ll_conteggio] or ls_nuova_versione_figlio<>ls_cod_versione_figlio[ll_conteggio] then
			//predispongo per inserimento
			
			if isnull(ls_cod_misura) then 
				ls_cod_misura = "null"
			else
				ls_cod_misura = "'" + ls_cod_misura + "'"
			end if
			
			if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
			if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0
			
			ld_quan_tecnica = round(ld_quan_tecnica, 4)
			ld_quan_utilizzo = round(ld_quan_utilizzo, 4)
			
			if isnull(ld_dim_x) then ld_dim_x = 0
			if isnull(ld_dim_y) then ld_dim_y = 0
			if isnull(ld_dim_z) then ld_dim_z = 0
			if isnull(ld_dim_t) then ld_dim_t = 0
			if isnull(ld_coef_calcolo) then ld_coef_calcolo = 0
			if isnull(ls_des_estesa) then ls_des_estesa = ""
			if isnull(ls_formula_tempo) then ls_formula_tempo = ""
			if isnull(ls_flag_materia_prima) then ls_flag_materia_prima = "N"
			
			
			ls_sql_ins =" insert into " + ls_tabella + &
							"(cod_azienda," + &
							"anno_registrazione," + &
							"num_registrazione," + &
							ls_progressivo + "," + &
							"cod_prodotto_padre," + &
							"num_sequenza," + &
							"cod_prodotto_figlio," + &
							"cod_versione_figlio," + &
							"cod_versione," + &
							"cod_misura," + &
							"cod_prodotto," + &
							"cod_versione_variante," + &
							"quan_tecnica," + &
							"quan_utilizzo," + &
							"dim_x," + &
							"dim_y," + &
							"dim_z," + &
							"dim_t," + &
							"coef_calcolo," + &
							"flag_esclusione," + &
							"formula_tempo," + &
							"flag_materia_prima," + &
							"des_estesa)" + &
						" VALUES ('" +  s_cs_xx.cod_azienda + "', " + &
							string(il_anno_registrazione) + ", " + &   
							string(il_num_registrazione) + ", " + &   
							string(il_prog_riga) + ", '" + &   
							fs_cod_prodotto + "', " + &
							string(ll_num_sequenza) + ", '" + &   
							ls_cod_prodotto_figlio[ll_conteggio] + "', '" + &
							ls_cod_versione_figlio[ll_conteggio] + "', '" + &
							fs_cod_versione + "'," + &
							ls_cod_misura + ", '" + &
							ls_nuovo_figlio + "', '" + &
							ls_nuova_versione_figlio + "', " + &
							f_double_to_string(ld_quan_tecnica) + ", " + &   
							f_double_to_string(ld_quan_utilizzo) + ", " + &
							f_double_to_string(ld_dim_x) + ", " + &   
							f_double_to_string(ld_dim_y) + ", " + &   
							f_double_to_string(ld_dim_z) + ", " + &   
							f_double_to_string(ld_dim_t) + ", " + &   
							f_double_to_string(ld_coef_calcolo) + ", '" + &   
							"N', '" + &
							ls_formula_tempo + "', '" + &
							ls_flag_materia_prima + "', '" + &
							ls_des_estesa + "')" 

			execute immediate :ls_sql_ins;
			if sqlca.sqlcode = -1 then
				
				if isnull(ls_nuovo_figlio) then 
					ls_messaggio = "NULL"
				elseif ls_nuovo_figlio = "" then
					ls_messaggio = "VUOTA"
				else
					ls_messaggio = ls_nuovo_figlio
				end if

				ls_messaggio += " su RAMO "+ls_cod_prodotto_figlio[ll_conteggio] + " Nseq: " + string(ll_num_sequenza)
				
				fs_errore = "uo_conf_varianti.uof_valorizza_formule: Errore insert Variante ("+ls_messaggio+") : " + sqlca.sqlerrtext
				close righe_distinta_3;
				return -1
			end if
			
			//valuto le formule quantità, se previsto
			if not isnull(ls_cod_formula_quan_utilizzo) then
				ll_ordine[1] = il_anno_registrazione
				ll_ordine[2] = il_num_registrazione
				ll_ordine[3] = il_prog_riga
			
				lstr_distinta.cod_prodotto_padre = fs_cod_prodotto
				lstr_distinta.cod_versione_padre = fs_cod_versione
				lstr_distinta.num_sequenza = ll_num_sequenza
				lstr_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio[ll_conteggio]
				lstr_distinta.cod_versione_figlio = ls_cod_versione_figlio[ll_conteggio]
				
				li_risposta = f_calcola_formula_db_v2(ls_cod_formula_quan_utilizzo, ll_ordine[],  lstr_distinta, is_gestione, ld_quan_utilizzo, ls_temp, ls_tipo_ritorno, fs_errore)
				if li_risposta<0 then
					fs_errore = 	"F.la Q.tà Utilizzo: Ramo ("+fs_cod_prodotto+","+fs_cod_versione+","+string(ll_num_sequenza)+","+ls_cod_prodotto_figlio[ll_conteggio]+","+ls_cod_versione_figlio[ll_conteggio]+"): " + &
											fs_errore
											
					close righe_distinta_3;
					rollback;
					return -1
				end if
				
				//OK
				ll_index = upperbound(is_elaborazione_formule_qta[]) + 1
				if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
				is_elaborazione_formule_qta[ll_index] = "RAMO " + ls_cod_prodotto_figlio[ll_conteggio] + " - F.LA " + ls_cod_formula_quan_utilizzo + " - RIS. " + string(ld_quan_utilizzo,"#####.00") + " OK!"
				
			end if
			
			if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
			
			ld_quan_utilizzo = round(ld_quan_utilizzo, 4)
			
			
			if not isnull(ls_cod_formula_quan_tecnica) then
				ll_ordine[1] = il_anno_registrazione
				ll_ordine[2] = il_num_registrazione
				ll_ordine[3] = il_prog_riga
			
				lstr_distinta.cod_prodotto_padre = fs_cod_prodotto
				lstr_distinta.cod_versione_padre = fs_cod_versione
				lstr_distinta.num_sequenza = ll_num_sequenza
				lstr_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio[ll_conteggio]
				lstr_distinta.cod_versione_figlio = ls_cod_versione_figlio[ll_conteggio]
				
				li_risposta = f_calcola_formula_db_v2(ls_cod_formula_quan_tecnica, ll_ordine[],  lstr_distinta, is_gestione, ld_quan_tecnica, ls_temp, ls_tipo_ritorno, fs_errore)
				if li_risposta<0 then
					fs_errore = 	"F.la Q.tà Tecnica: Ramo ("+fs_cod_prodotto+","+fs_cod_versione+","+string(ll_num_sequenza)+","+ls_cod_prodotto_figlio[ll_conteggio]+","+ls_cod_versione_figlio[ll_conteggio]+"): " + &
											fs_errore
											
					close righe_distinta_3;
					rollback;
					return -1
				end if
				
				//OK
				ll_index = upperbound(is_elaborazione_formule_qtatec[]) + 1
				if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0
				is_elaborazione_formule_qtatec[ll_index] = "RAMO " + ls_cod_prodotto_figlio[ll_conteggio] + " - F.LA " + ls_cod_formula_quan_utilizzo + " - RIS. " + string(ld_quan_utilizzo,"#####.00") + " OK!"
				
			end if
			
			if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0
			
			ld_quan_tecnica = round(ld_quan_tecnica, 4)
			
			//ora predispongo per UPDATE
			//faccio update delle quantità calcolate
			ls_sql_upd ="update " + ls_tabella + " " +&
							"set quan_tecnica="+f_double_to_string(ld_quan_tecnica)+ "," +&
								 "quan_utilizzo="+f_double_to_string(ld_quan_utilizzo)+" "+ &
							"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
										"anno_registrazione="+string(il_anno_registrazione)+" and "+&
										"num_registrazione="+string(il_num_registrazione)+" and "+&
										ls_progressivo+"="+string(il_prog_riga)+" and "+&
										"cod_prodotto_padre='"+fs_cod_prodotto+"' and "+&
										"cod_versione='"+fs_cod_versione+"' and "+&
										"num_sequenza="+string(ll_num_sequenza)+" and "+&
										"cod_prodotto_figlio='"+ls_cod_prodotto_figlio[ll_conteggio]+"' and "+&
										"cod_versione_figlio='"+ls_cod_versione_figlio[ll_conteggio]+"' "
										
			execute immediate :ls_sql_upd;
			
			if sqlca.sqlcode < 0 then
				fs_errore = g_str.format("Configuratore uof_ins_varianti: Errore in update quantità post sostituzione figlio/vers.figlio. Padre:$1  Figlio:$2  Qta_UT:$3  Qta_TEC:$4 Cod_formula_UT:$5  Cod_formula_TEC:$6",fs_cod_prodotto,ls_cod_prodotto_figlio[ll_conteggio],ld_quan_utilizzo ,ld_quan_tecnica, ls_cod_formula_quan_utilizzo, ls_cod_formula_quan_tecnica) + "~r~nTABELLA:"+  ls_tabella + "~r~nERRORE"+sqlca.sqlerrtext
				close righe_distinta_3;
				rollback;
				return -1
			end if
			
			//Se le formule cod prodotto figlio e/o cod versione figlio hanno cambiato i valori faccio la sostituzione
			if ls_nuovo_figlio<>ls_cod_prodotto_figlio[ll_conteggio] then
				ls_cod_prodotto_figlio[ll_conteggio] = ls_nuovo_figlio
			end if
			
			if ls_nuova_versione_figlio<>ls_cod_versione_figlio[ll_conteggio] then
				ls_cod_versione_figlio[ll_conteggio] = ls_nuova_versione_figlio
			end if
			
		end if
		
	else
		//#################################################################
		// HO TROVATO LA VARIANTE; sicuramente la variante è già stata inserita con le formule valorzzate, ma esso potrebbe essere a sua volta
		// un componente semilavorato che contiene la distinta che quindi devo andare a controllare;
		// infatti potrebbero esserci figli con formula associata
		
		choose case  is_gestione
			case "ORD_VEN"
				select 	cod_prodotto,
						cod_versione_variante
				into   	:ls_cod_prodotto_variante,
							:ls_cod_versione_variante
				from   varianti_det_ord_ven
				where  cod_azienda         = :s_cs_xx.cod_azienda and
						 anno_registrazione  = :il_anno_registrazione and
						 num_registrazione   = :il_num_registrazione and
						 prog_riga_ord_ven   = :il_prog_riga and
						 cod_prodotto_padre  = :fs_cod_prodotto and
						 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
						 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
						 cod_versione        = :fs_cod_versione and
						 num_sequenza = :ll_num_sequenza;
						 
			case "OFF_VEN"
				select 	cod_prodotto,
						cod_versione_variante
				into   	:ls_cod_prodotto_variante,
						:ls_cod_versione_variante
				from   varianti_det_off_ven
				where  cod_azienda         = :s_cs_xx.cod_azienda and
						 anno_registrazione  = :il_anno_registrazione and
						 num_registrazione   = :il_num_registrazione and
						 prog_riga_off_ven   = :il_prog_riga and
						 cod_prodotto_padre  = :fs_cod_prodotto and
						 cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_conteggio] and
						 cod_versione_figlio = :ls_cod_versione_figlio[ll_conteggio] and
						 cod_versione        = :fs_cod_versione and
						 num_sequenza = :ll_num_sequenza;
					 
		end choose

		if sqlca.sqlcode = 100 or sqlca.sqlcode < 0 then
			fs_errore = "Errore in ricerca della variante del ramo di distinta:~r~nPadre " + fs_cod_prodotto + " vers.:" + fs_cod_versione + &
																				"~r~nFiglio " + ls_cod_prodotto_figlio[ll_conteggio] + " vers.:" + ls_cod_versione_figlio[ll_conteggio] + &
																				"~r~n " + sqlca.sqlerrtext
			close righe_distinta_3;
			return -1
			
		end if		
			
		ls_cod_prodotto_figlio[ll_conteggio] = ls_cod_prodotto_variante
		ls_cod_versione_figlio[ll_conteggio] = ls_cod_versione_variante
		
	end if
	
	ll_conteggio++	
	
loop

close righe_distinta_3;


for ll_t = 1 to ll_conteggio -1
   SELECT count(*)
   INTO   :li_count  
   FROM   distinta  
   WHERE  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_t] and
			 cod_versione=:ls_cod_versione_figlio[ll_t];

	if sqlca.sqlcode=-1 then // Errore
		fs_errore = "Errore nel DB: "+SQLCA.SQLErrText	
		rollback;
		return -1
	end if

	if li_count > 0 then
		li_risposta = uof_valorizza_formule(ls_cod_prodotto_figlio[ll_t], ls_cod_versione_figlio[ll_t], ref fs_errore )
		if li_risposta < 0 then
			return -1
		end if
	end if
next

return 0
end function

public function integer uof_crea_varianti_esclusione (string fs_cod_prodotto, string fs_cod_versione);/*	Funzione che scorre la distinta base e inserisce la variante con flag_esclusione = 'N' 
	se non esistono altre varianti per quella materia prima (non per i semilavorati)

 nome: wf_crea_varianti_esclusione
 tipo: integer
  
	Variabili passate: 		nome					 tipo				passaggio per			commento
							
							 fs_cod_prodotto	 		string			valore
							 fs_cod_versione			string			valore

Creata il 03-09-98 
Autore Mauro Bordin

EnMe 03/02/2009
		Adeguamento per l'utilizzo della gestione offerte
*/
string				ls_cod_prodotto_figlio[], ls_tabella, ls_progressivo, ls_cod_misura, &
					ls_des_estesa, ls_formula_tempo, ls_sql_ins, ls_sql_sel, ls_flag_escludibile, ls_cod_formula_quan_utilizzo,&
					ls_cod_versione_figlio[], ls_temp, ls_tipo_ritorno, ls_errore, ls_cod_formula_quan_tecnica, ls_cod_formula_prod_figlio, ls_cod_formula_vers_figlio
					
dec{4}			ld_quan_tecnica, ld_quan_utilizzo, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_coef_calcolo

long				ll_conteggio,ll_t,ll_num_sequenza, ll_ordine[]

integer			li_risposta, li_count

boolean			lb_materia_prima[]

s_chiave_distinta		lstr_distinta


choose case str_conf_prodotto.tipo_gestione
		
	case "ORD_VEN"
		ls_tabella = "varianti_det_ord_ven"
		ls_progressivo = "prog_riga_ord_ven"
		
	case "OFF_VEN"
		ls_tabella = "varianti_det_off_ven"
		ls_progressivo = "prog_riga_off_ven"
		
end choose

ll_conteggio = 1

declare righe_distinta_3 cursor for 
select  cod_prodotto_figlio,
		  cod_versione_figlio,
		  num_sequenza,
		  cod_misura,
		  quan_tecnica,
		  quan_utilizzo,
		  dim_x,
		  dim_y,
		  dim_z,
		  dim_t,
		  coef_calcolo,
		  des_estesa,
		  formula_tempo,
		  cod_formula_quan_utilizzo,
		  flag_escludibile,
		  cod_formula_quan_tecnica,
		  cod_formula_prod_figlio,
		  cod_formula_vers_figlio
from    distinta 
where   cod_azienda = :s_cs_xx.cod_azienda 
and     cod_prodotto_padre = :fs_cod_prodotto
and     cod_versione=:fs_cod_versione;

open righe_distinta_3;

do while 1 = 1

	fetch righe_distinta_3 
	into  :ls_cod_prodotto_figlio[ll_conteggio],
			:ls_cod_versione_figlio[ll_conteggio],
			:ll_num_sequenza,
			:ls_cod_misura,
			:ld_quan_tecnica,
			:ld_quan_utilizzo,
			:ld_dim_x,
			:ld_dim_y,
			:ld_dim_z,
			:ld_dim_t,
			:ld_coef_calcolo,
			:ls_des_estesa,
			:ls_formula_tempo,
			:ls_cod_formula_quan_utilizzo,
			:ls_flag_escludibile,
			:ls_cod_formula_quan_tecnica,
			:ls_cod_formula_prod_figlio,
			:ls_cod_formula_vers_figlio;

   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit

	if sqlca.sqlcode<>0 then
		close righe_distinta_3;
		g_mb.error("Errore nel DB", SQLCA.SQLErrText)
		return -1
	end if

	SELECT count(*)
	INTO   :li_count  
	FROM   distinta  
	WHERE  cod_azienda = :s_cs_xx.cod_azienda
			AND    cod_prodotto_padre = :ls_cod_prodotto_figlio[ll_conteggio]
			and    cod_versione=:ls_cod_versione_figlio[ll_conteggio];

	if sqlca.sqlcode=-1 then // Errore
		g_mb.messagebox("Errore nel DB",SQLCA.SQLErrText,Information!)		
		rollback;
		return -1
	end if

	if li_count = 0 then
		lb_materia_prima[ll_conteggio] = true

		declare cu_varianti dynamic cursor for sqlsa;

		ls_sql_sel ="select count(*) from " + ls_tabella + &
						" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
						" anno_registrazione = " + string(str_conf_prodotto.anno_documento) + " and " + &
						" num_registrazione = " + string(str_conf_prodotto.num_documento) + " and " + &
						ls_progressivo + " = " + string(str_conf_prodotto.prog_riga_documento) + " and " + &
						" cod_prodotto_padre = '" + fs_cod_prodotto + "' and " + &
						" num_sequenza = " + string(ll_num_sequenza) + " and " + &
						" cod_prodotto_figlio = '" + ls_cod_prodotto_figlio[ll_conteggio] + "' and " + &
						" cod_versione_figlio = '" + ls_cod_versione_figlio[ll_conteggio] + "' and " + &
						" cod_versione = '" + fs_cod_versione + "'"

		prepare sqlsa from :ls_sql_sel ;

		open dynamic cu_varianti;

		fetch cu_varianti into :li_count;

		if sqlca.sqlcode = -1 then
			g_mb.error("Configuratore di Prodotto: Errore nella Ricerca delle Varianti: "+sqlca.sqlerrtext)
			close cu_varianti;
			rollback;
			return -1
		end if
		
		close cu_varianti;
		
		if li_count = 0 then
			if isnull(ls_cod_misura) then ls_cod_misura = ""
			if isnull(ld_quan_tecnica) then ld_quan_tecnica = 0

			if isnull(ld_dim_x) then ld_dim_x = 0
			if isnull(ld_dim_y) then ld_dim_y = 0
			if isnull(ld_dim_z) then ld_dim_z = 0
			if isnull(ld_dim_t) then ld_dim_t = 0
			if isnull(ld_coef_calcolo) then ld_coef_calcolo = 0
			if isnull(ls_des_estesa) then ls_des_estesa = ""
			if isnull(ls_formula_tempo) then ls_formula_tempo = ""

			if ls_cod_formula_quan_utilizzo <> "" and not isnull(ls_cod_formula_quan_utilizzo) then
				
				
				ll_ordine[1] = str_conf_prodotto.anno_documento
				ll_ordine[2] = str_conf_prodotto.num_documento
				ll_ordine[3] = str_conf_prodotto.prog_riga_documento
				
				lstr_distinta.cod_prodotto_padre = fs_cod_prodotto
				lstr_distinta.cod_versione_padre = fs_cod_versione
				lstr_distinta.num_sequenza = ll_num_sequenza
				lstr_distinta.cod_prodotto_figlio = ls_cod_prodotto_figlio[ll_conteggio]
				lstr_distinta.cod_versione_figlio = ls_cod_versione_figlio[ll_conteggio]
				
				setnull(ld_quan_utilizzo)
				
				li_risposta = f_calcola_formula_db_v2(ls_cod_formula_quan_utilizzo, ll_ordine[],  lstr_distinta, str_conf_prodotto.tipo_gestione, ld_quan_utilizzo, ls_temp, ls_tipo_ritorno, ls_errore)
				if li_risposta<0 then
					ls_errore = 	"Ramo ("+fs_cod_prodotto+","+fs_cod_versione+","+string(ll_num_sequenza)+","+ls_cod_prodotto_figlio[ll_conteggio]+","+ls_cod_versione_figlio[ll_conteggio]+"): " + &
											ls_errore
											
					close righe_distinta_3;
					rollback;
					
					g_mb.error(ls_errore)
					
					return -1
				end if
				
//				ld_quan_utilizzo = f_calcola_formula_db(ls_cod_formula_quan_utilizzo, ls_tabella, str_conf_prodotto.anno_documento, str_conf_prodotto.num_documento, str_conf_prodotto.prog_riga_documento)
//				if ld_quan_utilizzo = -1 then
//					close righe_distinta_3;
//					rollback;
//					return -1
//				end if

			end if
			
			if isnull(ld_quan_utilizzo) then ld_quan_utilizzo = 0
			
			ld_quan_utilizzo = round(ld_quan_utilizzo,4)
	
			ls_sql_ins =" insert into " + ls_tabella + &
							"(cod_azienda," + &
							"anno_registrazione," + &
							"num_registrazione," + &
							ls_progressivo + "," + &
							"cod_prodotto_padre," + &
							"num_sequenza," + &
							"cod_prodotto_figlio," + &
							"cod_versione_figlio," + &
							"cod_versione," + &
							"cod_misura," + &
							"cod_prodotto," + &
							"cod_versione_variante," + &
							"quan_tecnica," + &
							"quan_utilizzo," + &
							"dim_x," + &
							"dim_y," + &
							"dim_z," + &
							"dim_t," + &
							"coef_calcolo," + &
							"flag_esclusione," + &
							"formula_tempo," + &
							"des_estesa)" + &
						"VALUES ('" +  s_cs_xx.cod_azienda + "', " + &
							string(str_conf_prodotto.anno_documento) + ", " + &   
							string(str_conf_prodotto.num_documento) + ", " + &   
							string(str_conf_prodotto.prog_riga_documento) + ", '" + &   
							fs_cod_prodotto + "', " + &
							string(ll_num_sequenza) + ", '" + &   
							ls_cod_prodotto_figlio[ll_conteggio] + "', '" + &
							ls_cod_versione_figlio[ll_conteggio] + "', '" + &
							fs_cod_versione + "', '" + &
							ls_cod_misura + "', '" + &
							ls_cod_prodotto_figlio[ll_conteggio] + "', " + &
							ls_cod_versione_figlio[ll_conteggio] + "', " + &
							f_double_to_string(ld_quan_tecnica) + ", " + &   
							f_double_to_string(ld_quan_utilizzo) + ", " + &   
							f_double_to_string(ld_dim_x) + ", " + &   
							f_double_to_string(ld_dim_y) + ", " + &   
							f_double_to_string(ld_dim_z) + ", " + &   
							f_double_to_string(ld_dim_t) + ", " + &   
							f_double_to_string(ld_coef_calcolo) + ", '" + &   
							ls_flag_escludibile + "', '" + &
							ls_formula_tempo + "', '" + &
							ls_des_estesa + "')" 
	
			execute immediate :ls_sql_ins;
			if sqlca.sqlcode = -1 then
				g_mb.error("Configuratore di Prodotto", "Errore nell'inserimento della Variante: "+sqlca.sqlerrtext)
				close righe_distinta_3;
				rollback;
				return -1
			end if		
		end if
		lb_materia_prima[ll_conteggio] = true
	elseif li_count > 0 then
		lb_materia_prima[ll_conteggio] = false
	end if	
	ll_conteggio++	
loop

close righe_distinta_3;

for ll_t = 1 to ll_conteggio -1
	if not lb_materia_prima[ll_t] then
		li_risposta = uof_crea_varianti_esclusione(ls_cod_prodotto_figlio[ll_t],fs_cod_versione)
	
//	else Materia Prima (già inserita variante)

	end if
next

return 0
end function

public function integer uof_carica_mp_automatiche (string as_cod_modello_prodotto, decimal ad_dim_1, decimal ad_dim_2, decimal ad_dim_3, decimal ad_dim_4, decimal ad_altezza_asta, string as_cod_vernicitura_1, string as_cod_vernicitura_2, string as_cod_vernicitura_3, ref str_mp_automatiche atr_mp_automatiche[], ref string as_errore);/*	Funzione importata dalla finestra W_configuratore (dalla quale è stata eliminata) 
	che gestisce il caricamento delle Materie Prime 

							
Creata il 04-09-98 
Autore Enrico Menegotto
modificata il 04/09/1998
modificata in Ptenda 30/07/1999
enrico 13/10/1999: numero illimitato di mp automatiche

EnMe 03/02/2009: Adeguata la funzione per l'uso con le offerte clienti
EnMe 29/03/2012: 	gestione della seconda e terza verniciatura 
							gestione anche della dimensione 3 e 4 per cercare le MP automatiche	
EnMe 11/01/2013: Funziona importata dalle WF del configuratore e messa in oggetto riusabile.
*/
string		ls_cod_mp_non_richiesta, ls_cod_verniciatura, ls_flag_verniciatura[3], ls_asta
long		ll_prog_mp_non_richiesta, ll_num_mp, ll_i, li_count
datetime ldt_oggi
str_mp_automatiche lstr_mp_automatiche[]

atr_mp_automatiche = lstr_mp_automatiche

select 	flag_verniciatura, flag_verniciatura_2, flag_verniciatura_3
into   		:ls_flag_verniciatura[1], :ls_flag_verniciatura[2], :ls_flag_verniciatura[3]
from   	tab_flags_configuratore
where  	cod_azienda = :s_cs_xx.cod_azienda and
       		cod_modello = :as_cod_modello_prodotto;
				 
if sqlca.sqlcode = 100 then
	as_errore = "Attezione il modello " + as_cod_modello_prodotto + " non esiste "
	return -1
end if

if sqlca.sqlcode = -1 then
	as_errore = "Errore in lettura parametri configuratore. ~r~nDettaglio errore: " + sqlca.sqlerrtext
	return -1
end if

ldt_oggi = datetime(today(), 00:00:00)
li_count = 0

//**********************
//		ESTRAZIONE MP AUTOMATICHE PER 3 VERNICIATURE
//		EnMe 30-03-2012
// ----------------------------------

for ll_i = 1 to 3
	if  ls_flag_verniciatura[1]="S" or  ls_flag_verniciatura[2]="S" or  ls_flag_verniciatura[3]="S" then
		
		if  ls_flag_verniciatura[ll_i] = "N" then continue
		
		choose case ll_i
			case 1
				ls_cod_verniciatura = as_cod_vernicitura_1
			case 2
				ls_cod_verniciatura = as_cod_vernicitura_2
			case 3
				ls_cod_verniciatura = as_cod_vernicitura_3
		end choose

		if isnull(ls_cod_verniciatura) or len(ls_cod_verniciatura) < 1 then
			select cod_verniciatura  
			into   :ls_cod_verniciatura  
			from   tab_modelli_verniciature  
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_modello = :as_cod_modello_prodotto and  
					 flag_default = 'S' and
					 num_verniciatura = :ll_i and
					 flag_blocco  = 'N' ;
					 
			if sqlca.sqlcode = 100 then
				ls_cod_verniciatura = ""
			end if
			if sqlca.sqlcode = -1 then
				as_errore = "Errore in lettura tabella tab_modelli_verniciature. ~r~nDettaglio errore: " + sqlca.sqlerrtext
				return -1
			end if
			
		end if 
	
		if  len(ls_cod_verniciatura) > 0 and not isnull(ls_cod_verniciatura) then
				
			declare cur_mp_non_richieste_vern cursor for
			SELECT tab_mp_non_richieste.cod_mp_non_richiesta,   
					tab_mp_non_richieste.prog_mp_non_richiesta
			 FROM tab_mp_non_richieste,   
					tab_mp_automatiche_mod_vern  
			WHERE ( tab_mp_automatiche_mod_vern.cod_azienda = tab_mp_non_richieste.cod_azienda ) and  
					( tab_mp_automatiche_mod_vern.cod_mp_non_richiesta = tab_mp_non_richieste.cod_mp_non_richiesta ) and  
					( tab_mp_automatiche_mod_vern.prog_mp_non_richiesta = tab_mp_non_richieste.prog_mp_non_richiesta ) and  
					( ( tab_mp_non_richieste.cod_azienda = :s_cs_xx.cod_azienda ) AND  
					( :ad_dim_1 between lim_inf_dim_1 and lim_sup_dim_1  or  lim_sup_dim_1 = 0 or lim_sup_dim_1 is null ) AND  
					( :ad_dim_2 between lim_inf_dim_2 and lim_sup_dim_2   or  lim_sup_dim_2 = 0 or lim_sup_dim_2 is null) AND
					( :ad_dim_3 between lim_inf_dim_3 and lim_sup_dim_3   or  lim_sup_dim_3 = 0 or lim_sup_dim_3 is null) AND
					( :ad_dim_4 between lim_inf_dim_4 and lim_sup_dim_4   or  lim_sup_dim_4 = 0 or lim_sup_dim_4 is null) AND
					(tab_mp_non_richieste.flag_asta = 'N') AND
					(tab_mp_non_richieste.flag_blocco = 'N' OR  
					(tab_mp_non_richieste.flag_blocco = 'S' AND  
					tab_mp_non_richieste.data_blocco > :ldt_oggi)) AND  
					tab_mp_automatiche_mod_vern.cod_modello = :as_cod_modello_prodotto AND  
					tab_mp_automatiche_mod_vern.cod_verniciatura = :ls_cod_verniciatura and 
					tab_mp_automatiche_mod_vern.num_verniciatura = :ll_i)   ;
					
					
			open cur_mp_non_richieste_vern;
			if sqlca.sqlcode < 0 then
				as_errore = "Errore in apertura cursore cur_mp_non_richieste_vern.  DETTAGLIO: " + sqlca.sqlerrtext
				return -1
			end if
		end if
		
	else
		// se almeno una delle 3 verniciature è attivata allora non devo passare qui dentro 
		
		if ls_flag_verniciatura[1]="S" or  ls_flag_verniciatura[2]="S" or  ls_flag_verniciatura[3]="S" then continue
		
		if ll_i > 1 then continue
		
		// nessuna verniciatura selezionata
		declare cur_mp_non_richieste cursor for
		select cod_mp_non_richiesta,
				prog_mp_non_richiesta
		 from tab_mp_non_richieste
		where cod_azienda = :s_cs_xx.cod_azienda and
					cod_modello_tenda = :as_cod_modello_prodotto and
				(:ad_dim_1 between lim_inf_dim_1 and lim_sup_dim_1 or  	lim_inf_dim_1 = 0 and lim_sup_dim_1 = 0 ) and
				(:ad_dim_2 between lim_inf_dim_2 and lim_sup_dim_2 or   lim_inf_dim_2 = 0 and lim_sup_dim_2 = 0 ) and
				(:ad_dim_3 between lim_inf_dim_3 and lim_sup_dim_3 or   lim_inf_dim_3 = 0 and lim_sup_dim_3 = 0 ) and
				(:ad_dim_4 between lim_inf_dim_4 and lim_sup_dim_4 or   lim_inf_dim_4 = 0 and lim_sup_dim_4 = 0 ) and
				(flag_asta = 'N') and
				(flag_blocco = 'N' or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
				
		open cur_mp_non_richieste;
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore in apertura cursore cur_mp_non_richieste DETTAGLIO: " + sqlca.sqlerrtext
			return -1
		end if
		
	end if

	if ls_flag_verniciatura[ll_i]="S" then
		if  len(ls_cod_verniciatura) > 0 and not isnull(ls_cod_verniciatura) then
			fetch cur_mp_non_richieste_vern into :ls_cod_mp_non_richiesta, :ll_prog_mp_non_richiesta;
		end if
	else
		if ll_i = 1 then
			fetch cur_mp_non_richieste into :ls_cod_mp_non_richiesta, :ll_prog_mp_non_richiesta;
		end if
	end if
	if sqlca.sqlcode = -1 then
		as_errore =  "Errore durante l'Estrazione delle materie prime.~r~nDETTAGLIO: " + sqlca.sqlerrtext
		close cur_mp_non_richieste_vern;
		return -1
	end if
	
	do while sqlca.sqlcode = 0
		li_count ++
		atr_mp_automatiche[li_count].cod_mp_automatica = ls_cod_mp_non_richiesta
		atr_mp_automatiche[li_count].prog_mp_automatica = ll_prog_mp_non_richiesta
		if ls_flag_verniciatura[ll_i] = "S" then
			fetch cur_mp_non_richieste_vern into :ls_cod_mp_non_richiesta, :ll_prog_mp_non_richiesta;
		elseif ll_i = 1 then
			fetch cur_mp_non_richieste into :ls_cod_mp_non_richiesta, :ll_prog_mp_non_richiesta;
		end if
	loop

	if ls_flag_verniciatura[ll_i] = "S" then
		close cur_mp_non_richieste_vern;
	elseif ll_i = 1 then
		close cur_mp_non_richieste;
	end if	

next



// ---------------------  selezione dell'asta in funzione dell'altezza comando -------------------------------------

if ad_altezza_asta > 0 then
	if ls_flag_verniciatura[1] = "S" then
		declare cu_asta_vern cursor for
		SELECT tab_mp_non_richieste.cod_mp_non_richiesta,   
				tab_mp_non_richieste.prog_mp_non_richiesta
		 FROM tab_mp_non_richieste,   
				tab_mp_automatiche_mod_vern  
		WHERE ( tab_mp_automatiche_mod_vern.cod_azienda = tab_mp_non_richieste.cod_azienda ) and  
				( tab_mp_automatiche_mod_vern.cod_mp_non_richiesta = tab_mp_non_richieste.cod_mp_non_richiesta ) and  
				( tab_mp_automatiche_mod_vern.prog_mp_non_richiesta = tab_mp_non_richieste.prog_mp_non_richiesta ) and  
				( ( tab_mp_non_richieste.cod_azienda = :s_cs_xx.cod_azienda ) AND  
				(tab_mp_non_richieste.flag_asta = 'S') AND
				(tab_mp_non_richieste.altezza_comando = :ad_altezza_asta) AND
				(tab_mp_non_richieste.flag_blocco = 'N' OR  
				(tab_mp_non_richieste.flag_blocco = 'S' AND  
				tab_mp_non_richieste.data_blocco > :ldt_oggi)) AND  
				tab_mp_automatiche_mod_vern.cod_modello = :as_cod_modello_prodotto AND  
				tab_mp_automatiche_mod_vern.cod_verniciatura = :ls_cod_verniciatura )   ;
		open cu_asta_vern;
		if sqlca.sqlcode < 0 then
			as_errore = "Errore in apertura cursore  cu_asta_vern  DETTAGLIO: " + sqlca.sqlerrtext
		end if
	else
		declare cu_asta cursor for
		SELECT cod_mp_non_richiesta,   
				 prog_mp_non_richiesta
		 FROM  tab_mp_non_richieste
		WHERE  ( ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
				 ( tab_mp_non_richieste.flag_asta = 'S') AND
				 ( tab_mp_non_richieste.altezza_comando = :ad_altezza_asta ) AND
				 ( tab_mp_non_richieste.flag_blocco = 'N' OR  
				 ( tab_mp_non_richieste.flag_blocco = 'S' AND  
				 tab_mp_non_richieste.data_blocco > :ldt_oggi ) ) )   ;
		open cu_asta;
		if sqlca.sqlcode < 0 then
			as_errore = "Errore in apertura cursore  cu_asta  DETTAGLIO: " + sqlca.sqlerrtext
		end if
	end if
	
	
	do
		
		if ls_flag_verniciatura[1] = "S" then
			fetch cu_asta_vern into :ls_cod_mp_non_richiesta, :ll_prog_mp_non_richiesta;
		else
			fetch cu_asta into :ls_cod_mp_non_richiesta, :ll_prog_mp_non_richiesta;
		end if
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode < 0 then
			as_errore = "Errore in fetch cursor cu_asta_vern: " + sqlca.sqlerrtext
		end if
		
		li_count ++
		atr_mp_automatiche[li_count].cod_mp_automatica = ls_cod_mp_non_richiesta
		atr_mp_automatiche[li_count].prog_mp_automatica = ll_prog_mp_non_richiesta
		
	loop 	while true
	
	if ls_flag_verniciatura[1] = "S" then
		close cu_asta_vern;
	else
		close cu_asta;
	end if
	
end if
return 0
end function

public function integer uof_crea_varianti (wstr_riepilogo astr_riepilogo, wstr_flags astr_flag_configuratore, ref string as_errore);/*	Funzione che verifica se per la DW passata come parametro ci sono dei campi da inserire in
		distinta base, in tal caso chiama la funzione uo_ins_varianti.uof_ins_varianti che mi inserisce la variante
		relativa al cod_prodotto e gruppo_variante in questione

nome: uo_ins_varianti.uof_ins_varianti
tipo: integer
  
	Variabili passate: 		nome					 tipo				passaggio per			commento
							
Creata il 31-08-98 
		Autore Mauro Bordin

modificata il 31/08/1998

EnMe 13/10/1999:
		gestione numero illimitato di mp automatiche
		
EnMe 03/02/2009
		Adeguamento per la gestione offerte clienti
		
EnMe 29/03/2012
		Aggiunta della seconda e terza verniciatura
EnMe 13/02/2014
		Creazione della variante TELO finito (Dekora)

*/
string ls_cod_gruppo_variante
integer li_return, ll_index
long    ll_num_mp, ll_i
dec{4} ld_null, ld_quan_utilizzo, ld_quan_tecnica


setnull(ld_null)
ll_index = 0


// **********************************
// INIZIO Materie Prime Automatiche
// **********************************
//

ll_num_mp = upperbound(astr_riepilogo.mp_automatiche)
for ll_i = 1 to ll_num_mp
	
	if not(isnull(astr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica)) and astr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica <> "" and &
		not(isnull(astr_riepilogo.mp_automatiche[ll_i].prog_mp_automatica)) and astr_riepilogo.mp_automatiche[ll_i].prog_mp_automatica <> 0 then
		select cod_gruppo_variante,
				 quan_utilizzo,
				 quan_tecnica
		into   :ls_cod_gruppo_variante,
				 :ld_quan_utilizzo,
				 :ld_quan_tecnica
		from   tab_mp_non_richieste
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_mp_non_richiesta = :astr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica and
				 prog_mp_non_richiesta = :astr_riepilogo.mp_automatiche[ll_i].prog_mp_automatica ;
	
		if sqlca.sqlcode <> 0 then
			as_errore =  "Materia Prima Automatica " + astr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica + " non presente in tabella MP automatiche"
			return -1				
		end if
		
		if not(isnull(ls_cod_gruppo_variante)) and ls_cod_gruppo_variante <> "" then
			ll_index ++
			str_varianti[ll_index].cod_versione = is_cod_versione
			str_varianti[ll_index].cod_versione_figlio = is_cod_versione
			str_varianti[ll_index].cod_gruppo_variante = ls_cod_gruppo_variante
			str_varianti[ll_index].cod_mp_variante = astr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica
			str_varianti[ll_index].quan_utilizzo = ld_quan_utilizzo
			str_varianti[ll_index].quan_tecnica  = ld_quan_tecnica
			
		end if
	end if
next

// **********************************
// INIZIO inserimento variante telo finito
// **********************************

if not(isnull(astr_riepilogo.cod_modello_telo_finito )) and astr_riepilogo.cod_modello_telo_finito <> "" then
	ll_index ++
	str_varianti[ll_index].cod_versione = is_cod_versione
	str_varianti[ll_index].cod_versione_figlio = is_cod_versione
	str_varianti[ll_index].cod_gruppo_variante = "TELOF"
	str_varianti[ll_index].cod_mp_variante = astr_riepilogo.cod_modello_telo_finito
	str_varianti[ll_index].quan_utilizzo = 1
	str_varianti[ll_index].quan_tecnica  = 1
	
end if

// **********************************
// FINE  variante telo finito
// **********************************


// **********************************
// INIZIO Comando
// **********************************

if astr_flag_configuratore.flag_comando = "S" then
	if not(isnull(astr_riepilogo.cod_comando)) and astr_riepilogo.cod_comando <> "" then
		select cod_gruppo_variante
		into   :ls_cod_gruppo_variante
		from   tab_comandi
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_comando = :astr_riepilogo.cod_comando;

		if sqlca.sqlcode <> 0 then
			as_errore = "Comando non presente in Anagrafica"
			return -1				
		end if

		ll_index ++
		str_varianti[ll_index].cod_versione = is_cod_versione
		str_varianti[ll_index].cod_versione_figlio = is_cod_versione
		str_varianti[ll_index].cod_gruppo_variante = ls_cod_gruppo_variante
		str_varianti[ll_index].cod_mp_variante = astr_riepilogo.cod_comando
		str_varianti[ll_index].quan_utilizzo = ld_null
		str_varianti[ll_index].quan_tecnica  = ld_null

	end if
end if

// **********************************
// FINE Comando
// **********************************


// **********************************
// INIZIO Tessuto
// **********************************

if astr_flag_configuratore.flag_tessuto = "S" then
	if not(isnull(astr_riepilogo.cod_tessuto)) and astr_riepilogo.cod_tessuto <> "" and &
		not(isnull(astr_riepilogo.cod_non_a_magazzino)) and astr_riepilogo.cod_non_a_magazzino <> "" then
		select cod_gruppo_variante
		into   :ls_cod_gruppo_variante
		from   tab_tessuti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tessuto = :astr_riepilogo.cod_tessuto and
				 cod_non_a_magazzino = :astr_riepilogo.cod_non_a_magazzino;

		if sqlca.sqlcode <> 0 then
			as_errore =  "Tessuto non presente in Anagrafica"
			return -1				
		end if
		ll_index ++
		str_varianti[ll_index].cod_versione = is_cod_versione
		str_varianti[ll_index].cod_versione_figlio = is_cod_versione
		str_varianti[ll_index].cod_gruppo_variante = ls_cod_gruppo_variante
		str_varianti[ll_index].cod_mp_variante = astr_riepilogo.cod_tessuto
		str_varianti[ll_index].quan_utilizzo = ld_null
		str_varianti[ll_index].quan_tecnica  = ld_null
		
	end if
end if

// **********************************
// FINE Tessuto
// **********************************

// **********************************
// INIZIO Tessuto Mantovana (blocco aggiunto da EnMe 27.03.2012)
// **********************************

if astr_flag_configuratore.flag_tessuto_mantovana = "S" then
	if not(isnull(astr_riepilogo.cod_tessuto_mant)) and astr_riepilogo.cod_tessuto_mant <> "" and &
		not(isnull(astr_riepilogo.cod_mant_non_a_magazzino)) and astr_riepilogo.cod_mant_non_a_magazzino <> "" then
		select cod_gruppo_variante
		into   :ls_cod_gruppo_variante
		from   tab_tessuti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tessuto = :astr_riepilogo.cod_tessuto_mant and
				 cod_non_a_magazzino = :astr_riepilogo.cod_mant_non_a_magazzino;

		if sqlca.sqlcode <> 0 then
			as_errore =  "Tessuto Mantovana non presente in Anagrafica"
			return -1				
		end if
		ll_index ++
		str_varianti[ll_index].cod_versione = is_cod_versione
		str_varianti[ll_index].cod_versione_figlio = is_cod_versione
		str_varianti[ll_index].cod_gruppo_variante = ls_cod_gruppo_variante
		str_varianti[ll_index].cod_mp_variante = astr_riepilogo.cod_tessuto_mant
		str_varianti[ll_index].quan_utilizzo = ld_null
		str_varianti[ll_index].quan_tecnica  = ld_null
		
	end if
end if

// **********************************
// FINE Tessuto MANTOVANA
// **********************************




// **********************************
// INIZIO Verniciatura
// **********************************

if astr_flag_configuratore.flag_verniciatura = "S" then
	if not(isnull(astr_riepilogo.cod_verniciatura)) and astr_riepilogo.cod_verniciatura <> "" then
		
//		29/03/2012 EnMe: 
//
//		select cod_gruppo_variante
//		into   :ls_cod_gruppo_variante
//		from   tab_verniciatura
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_verniciatura = :astr_riepilogo.cod_verniciatura;

		select cod_gruppo_variante
		into   :ls_cod_gruppo_variante
		from tab_modelli_verniciature
		where  cod_azienda = :s_cs_xx.cod_azienda and
		 		cod_modello = :is_cod_modello and
				num_verniciatura = 1 and
				 cod_verniciatura = :astr_riepilogo.cod_verniciatura;
		
		if sqlca.sqlcode <> 0 then
			as_errore =  "Verniciatura non presente in tabella modelli / verniciature"
			return -1
		end if
		
		ll_index ++
		str_varianti[ll_index].cod_versione = is_cod_versione
		str_varianti[ll_index].cod_versione_figlio = is_cod_versione
		str_varianti[ll_index].cod_gruppo_variante = ls_cod_gruppo_variante
		str_varianti[ll_index].cod_mp_variante = astr_riepilogo.cod_verniciatura
		str_varianti[ll_index].quan_utilizzo = ld_null
		str_varianti[ll_index].quan_tecnica  = ld_null
	end if
end if

// **********************************
// FINE Verniciatura
// **********************************

// **********************************
// INIZIO Supporto
// **********************************

if not(isnull(astr_riepilogo.cod_tipo_supporto)) and astr_riepilogo.cod_tipo_supporto <> "" then
	select cod_gruppo_variante
	into   :ls_cod_gruppo_variante
	from   tab_tipi_supporto
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_supporto = :astr_riepilogo.cod_tipo_supporto;

	if sqlca.sqlcode <> 0 then
		as_errore = "Supporto non presente in Anagrafica"
		return -1				
	end if
	ll_index ++
	str_varianti[ll_index].cod_versione = is_cod_versione
	str_varianti[ll_index].cod_versione_figlio = is_cod_versione
	str_varianti[ll_index].cod_gruppo_variante = ls_cod_gruppo_variante
	str_varianti[ll_index].cod_mp_variante = astr_riepilogo.cod_tipo_supporto
	str_varianti[ll_index].quan_utilizzo = ld_null
	str_varianti[ll_index].quan_tecnica  = ld_null

end if		

// **********************************
// FINE Supporto
// **********************************


// **********************************
// INIZIO SECONDA Verniciatura
// **********************************

if astr_flag_configuratore.flag_verniciatura_2 = "S" then
	if not(isnull(astr_riepilogo.cod_verniciatura_2)) and astr_riepilogo.cod_verniciatura_2 <> "" then

		select cod_gruppo_variante
		into   :ls_cod_gruppo_variante
		from tab_modelli_verniciature
		where  cod_azienda = :s_cs_xx.cod_azienda and
		 		cod_modello = :is_cod_modello and
				num_verniciatura = 2 and
				 cod_verniciatura = :astr_riepilogo.cod_verniciatura_2;
		
		if sqlca.sqlcode <> 0 then
			as_errore = "Verniciatura non presente in tabella modelli / verniciature"
			return -1
		end if
		
		
		ll_index ++
		str_varianti[ll_index].cod_versione = is_cod_versione
		str_varianti[ll_index].cod_versione_figlio = is_cod_versione
		str_varianti[ll_index].cod_gruppo_variante = ls_cod_gruppo_variante
		str_varianti[ll_index].cod_mp_variante = astr_riepilogo.cod_verniciatura_2
		str_varianti[ll_index].quan_utilizzo = ld_null
		str_varianti[ll_index].quan_tecnica  = ld_null
	end if
end if

// **********************************
// FINE Verniciatura
// **********************************


// **********************************
// INIZIO TERZA Verniciatura
// **********************************

if astr_flag_configuratore.flag_verniciatura_3 = "S" then
	if not(isnull(astr_riepilogo.cod_verniciatura_3)) and astr_riepilogo.cod_verniciatura_3 <> "" then

		select cod_gruppo_variante
		into   :ls_cod_gruppo_variante
		from tab_modelli_verniciature
		where  cod_azienda = :s_cs_xx.cod_azienda and
		 		cod_modello = :is_cod_modello and
				num_verniciatura = 3 and
				 cod_verniciatura = :astr_riepilogo.cod_verniciatura_3;
		
		if sqlca.sqlcode <> 0 then
			as_errore = "Verniciatura non presente in tabella modelli / verniciature"
			return -1
		end if
		
		
		ll_index ++
		str_varianti[ll_index].cod_versione = is_cod_versione
		str_varianti[ll_index].cod_versione_figlio = is_cod_versione
		str_varianti[ll_index].cod_gruppo_variante = ls_cod_gruppo_variante
		str_varianti[ll_index].cod_mp_variante = astr_riepilogo.cod_verniciatura_3
		str_varianti[ll_index].quan_utilizzo = ld_null
		str_varianti[ll_index].quan_tecnica  = ld_null
	end if
end if

// **********************************
// FINE Verniciatura
// **********************************

// **********************************
// INIZIO SECONDO Comando
// **********************************

if astr_flag_configuratore.flag_secondo_comando = "S" then
	if not(isnull(astr_riepilogo.cod_comando_2)) and astr_riepilogo.cod_comando_2 <> "" then
		select cod_gruppo_variante
		into   :ls_cod_gruppo_variante
		from   tab_comandi
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_comando = :astr_riepilogo.cod_comando_2;

		if sqlca.sqlcode <> 0 then
			as_errore = "Comando non presente in Anagrafica"
			return -1				
		end if

		ll_index ++
		str_varianti[ll_index].cod_versione = is_cod_versione
		str_varianti[ll_index].cod_versione_figlio = is_cod_versione
		str_varianti[ll_index].cod_gruppo_variante = ls_cod_gruppo_variante
		str_varianti[ll_index].cod_mp_variante = astr_riepilogo.cod_comando_2
		str_varianti[ll_index].quan_utilizzo = ld_null
		str_varianti[ll_index].quan_tecnica  = ld_null

	end if
end if

// **********************************
// FINE SECONDO Comando
// **********************************


// -------------------------------------------------------------------------------------------------------------
//prima di entrare nella funzione di creazione delle varianti, 
//verifico se il prodotto venduto ha una fase a livello root in distinta base
//semplicemente questa funzione attiva o meno un primo semaforo: variabile booleana -> ib_fase_su_radice
// -------------------------------------------------------------------------------------------------------------
ib_fase_su_radice = false
ib_nofase_su_figli = true
if is_gestione = "ORD_VEN" then uof_fase_su_radice_db(is_cod_modello, is_cod_versione)


// -------------------------------------------------------------------------------------------------------------
// ELABORAZIONE DELLE VARIANTI
// -------------------------------------------------------------------------------------------------------------
li_return = uof_ins_varianti(is_cod_modello, is_cod_versione, ref as_errore)
if li_return < 0 then
	as_errore = "Errore generaz. varianti (uof_ins_varianti): " + as_errore
	return -1
end if

is_errore_vendita_sl = ""

if ib_fase_su_radice and ib_nofase_su_figli then
	li_return = uof_crea_ramo_fittizio(is_cod_modello, is_cod_versione, is_errore_vendita_sl)
end if

//reset semafori gestione vendita semilavorato
ib_fase_su_radice = false
ib_nofase_su_figli = true


// -------------------------------------------------------------------------------------------------------------
// VALORIZZO LE FORMULE NELLA DISTINTA NEI RAMI SENZA VARIANTE
// -------------------------------------------------------------------------------------------------------------
li_return = uof_valorizza_formule(is_cod_modello, is_cod_versione, ref as_errore)

if li_return = -1 then
	as_errore = "Errore valoriz. formule rami senza variante (uof_valorizza_formule): " + as_errore
	return -1				
end if

return 0
end function

public function integer uof_valorizza_formule_prod_figlio (string as_padre, string as_versione_padre, long al_num_sequenza, ref string as_figlio, ref string as_versione_figlio, ref long al_sequenza_figlio, string as_formula_figlio, string as_formula_versione_figlio, ref string as_errore);long						al_rif_riga_ordine[], ll_index
s_chiave_distinta		ls_chiave_distinta
integer					li_ret
dec{4}					ld_valore
string						ls_valore, ls_tipo_ritorno, ls_sql, ls_messaggio
boolean					lb_cambiato
datastore				lds_data
uo_log_sistema			luo_log


as_errore = ""
lb_cambiato = false

al_rif_riga_ordine[1] = il_anno_registrazione
al_rif_riga_ordine[2] = il_num_registrazione
al_rif_riga_ordine[3] = il_prog_riga

ls_chiave_distinta.cod_prodotto_padre = as_padre
ls_chiave_distinta.cod_versione_padre = as_versione_padre
ls_chiave_distinta.num_sequenza = al_num_sequenza
ls_chiave_distinta.cod_prodotto_figlio = as_figlio
ls_chiave_distinta.cod_versione_figlio = as_versione_figlio

setnull(ls_valore)
setnull(ld_valore)

ls_messaggio =""

//###################################################################################
if as_formula_figlio<>"" and not isnull(as_formula_figlio) then
	//valuto la formula sul cod.prodotto.figlio
	
	ls_messaggio += " F.la Figlio "+as_formula_figlio + " "
	
	li_ret = f_calcola_formula_db_v2 (	as_formula_figlio, al_rif_riga_ordine[], ls_chiave_distinta, is_gestione, &
													ld_valore, ls_valore, ls_tipo_ritorno, as_errore)
	
	if li_ret<0 then
		return -1
	end if
	
	//il ritorno deve essere STRINGA
	if ls_tipo_ritorno<>"2" then
		//as_errore="Anomalia NON BLOCCANTE: La formula impostata sul prodotto figlio ha valore ritorno numerico al posto di stringa!"
		//return 1

	else
		//se ritorna VUOTO fai come se non ci fosse niente (Niente sostituzione prodotto figlio...)
		if ls_valore<>"" and not isnull(ls_valore) then
			
			if ls_valore<>as_figlio then
				//se è tornato qualcosa di diverso ... imposto il nuovo valore per il prodotto figlio
				if uof_controlla_figlio_formula(ls_valore) = 0 then
					//OK
					ll_index = upperbound(is_elaborazione_formule[]) + 1
					is_elaborazione_formule[ll_index] = "RAMO " + as_figlio + " - F.LA " + as_formula_figlio + " - RIS. " + ls_valore + " OK!"
					
					as_figlio = ls_valore
					lb_cambiato = true
					
				else
					//la formula ha restituito un codice inesistente
					ll_index = upperbound(is_warning_formule[]) + 1
					is_warning_formule[ll_index] = "RAMO " + as_figlio + " - F.LA " + as_formula_figlio + " - RIS. " + ls_valore + " inesistente!"
				end if
				
			else
				//formula calcolata ma il valore non è diverso dal figlio
				ll_index = upperbound(is_elaborazione_formule[]) + 1
				is_elaborazione_formule[ll_index] = "RAMO " + as_figlio + " - F.LA " + as_formula_figlio + " - RIS. " + ls_valore + " OK*!"
			end if
		end if
		
	end if
end if


setnull(ls_valore)
setnull(ld_valore)

//###################################################################################
if as_formula_versione_figlio<>"" and not isnull(as_formula_versione_figlio) then
	//valuto la formula sul cod.versione.figlio
	
	ls_messaggio += " F.la Vers.Figlio "+as_formula_versione_figlio + " "
	
	li_ret = f_calcola_formula_db_v2 (	as_formula_versione_figlio, al_rif_riga_ordine[], ls_chiave_distinta, is_gestione, &
													ld_valore, ls_valore, ls_tipo_ritorno, as_errore)
	
	if li_ret<0 then
		return -1
	end if
	
	//il ritorno deve essere STRINGA
	if ls_tipo_ritorno<>"2" then
//		as_errore="Anomalia NON BLOCCANTE: La formula impostata sulla versione figlio ha valore ritorno numerico al posto di stringa!"
//		return -1

	else
		
		//se ritorna VUOTO fai come se non ci fosse niente (Niente sostituzione versione prodotto figlio...)
		if ls_valore<>"" and not isnull(ls_valore) then
			
			if ls_valore<>as_versione_figlio then
				//se è tornato qualcosa di diverso ... imposto il nuovo valore per la versione del prodotto figlio
				as_versione_figlio = ls_valore
				lb_cambiato = true
			end if
			
		end if

	end if
end if

//if lb_cambiato then
//	//se è stato cambiato figlio o versione figlio devo recuperare il relativo num sequenza,
//	//servirà dopo quando devo calcolarne la formula q.tà utilizzo del nuovo ramo
//	
//	ls_sql = 	"select num_sequenza from distinta "+&
//				"where    cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
//							"cod_prodotto_padre='"+as_padre+"' and cod_versione='"+as_versione_padre+"' and "+&
//							"cod_prodotto_figlio='"+as_figlio+"' and cod_versione_figlio='"+as_versione_figlio+"'"
//	
//	li_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
//	
//	if li_ret>0 then
//		//prendo il primo che trovo
//		al_sequenza_figlio = lds_data.getitemnumber(1, 1)
//	else
//		ls_messaggio = ls_messaggio + " hanno prodotto un ramo inesistente: " + &
//																				as_padre+" "+as_versione_padre+" "+as_figlio+" "+as_versione_figlio
////		luo_log = create uo_log_sistema
////		luo_log.uof_write_log_sistema_not_sqlca("FORMULE", ls_messaggio)
////		destroy luo_log
//		
//		return -1
//		
//	end if
//end if
//





return 0
end function

public function integer uof_valorizza_formule_figlio_varianti (string as_padre, string as_versione_padre, long al_num_sequenza, string as_figlio, string as_versione_figlio, ref string as_cod_variante, ref string as_versione_variante, string as_formula_figlio, string as_formula_versione_figlio, ref string as_errore);long						al_rif_riga_ordine[], ll_index
s_chiave_distinta		ls_chiave_distinta
integer					li_ret
dec{4}					ld_valore
string						ls_valore, ls_tipo_ritorno, ls_sql, ls_messaggio
datastore				lds_data
uo_log_sistema			luo_log


as_errore = ""

al_rif_riga_ordine[1] = il_anno_registrazione
al_rif_riga_ordine[2] = il_num_registrazione
al_rif_riga_ordine[3] = il_prog_riga

ls_chiave_distinta.cod_prodotto_padre = as_padre
ls_chiave_distinta.cod_versione_padre = as_versione_padre
ls_chiave_distinta.num_sequenza = al_num_sequenza
ls_chiave_distinta.cod_prodotto_figlio = as_figlio
ls_chiave_distinta.cod_versione_figlio = as_versione_figlio

setnull(ls_valore)
setnull(ld_valore)

ls_messaggio =""

//###################################################################################
if as_formula_figlio<>"" and not isnull(as_formula_figlio) then
	//valuto la formula sul cod.prodotto.figlio
	
	ls_messaggio += " F.la Figlio "+as_formula_figlio + " "
	
	li_ret = f_calcola_formula_db_v2 (	as_formula_figlio, al_rif_riga_ordine[], ls_chiave_distinta, is_gestione, &
													ld_valore, ls_valore, ls_tipo_ritorno, as_errore)
	
	if li_ret<0 then
		return -1
	end if
	
	//il ritorno deve essere STRINGA
	if ls_tipo_ritorno<>"2" then
		//as_errore="Anomalia NON BLOCCANTE: La formula impostata sul prodotto figlio ha valore ritorno numerico al posto di stringa!"
		//return 1

	else
		//se ritorna VUOTO fai come se non ci fosse niente (Niente sostituzione prodotto figlio...)
		if ls_valore<>"" and not isnull(ls_valore) then
			
			if ls_valore<>as_cod_variante then
				//se è tornato qualcosa di diverso ... imposto il nuovo valore per il prodotto figlio
				if uof_controlla_figlio_formula(ls_valore) = 0 then
					//OK
					ll_index = upperbound(is_elaborazione_formule[]) + 1
					is_elaborazione_formule[ll_index] = "RAMO " + as_figlio + " (VAR. "+as_cod_variante+") - F.LA " + as_formula_figlio + " - RIS. " + ls_valore + " OK!"
					
					as_cod_variante = ls_valore
					
				else
					//la formula ha restituito un codice inesistente
					ll_index = upperbound(is_warning_formule[]) + 1
					is_warning_formule[ll_index] = "RAMO " + as_figlio + " (VAR. "+as_cod_variante+") - F.LA " + as_formula_figlio + " - RIS. " + ls_valore + " inesistente!"
					
				end if
				
			else
				//formula calcolata ma il valore non è diverso dal figlio
				ll_index = upperbound(is_elaborazione_formule[]) + 1
				is_elaborazione_formule[ll_index] = "RAMO " + as_figlio + " (VAR. "+as_cod_variante+") - F.LA " + as_formula_figlio + " - RIS. " + ls_valore + " OK*!"
			end if
		end if
		
	end if
end if


setnull(ls_valore)
setnull(ld_valore)

//###################################################################################
if as_formula_versione_figlio<>"" and not isnull(as_formula_versione_figlio) then
	//valuto la formula sul cod.versione.figlio
	
	ls_messaggio += " F.la Vers.Figlio "+as_formula_versione_figlio + " "
	
	li_ret = f_calcola_formula_db_v2 (	as_formula_versione_figlio, al_rif_riga_ordine[], ls_chiave_distinta, is_gestione, &
													ld_valore, ls_valore, ls_tipo_ritorno, as_errore)
	
	if li_ret<0 then
		return -1
	end if
	
	//il ritorno deve essere STRINGA
	if ls_tipo_ritorno<>"2" then
//		as_errore="Anomalia NON BLOCCANTE: La formula impostata sulla versione figlio ha valore ritorno numerico al posto di stringa!"
//		return -1

	else
		//se ritorna VUOTO fai come se non ci fosse niente (Niente sostituzione versione prodotto figlio...)
		if ls_valore<>"" and not isnull(ls_valore) then
			
			if ls_valore<>as_versione_variante then
				//se è tornato qualcosa di diverso ... imposto il nuovo valore per la versione del prodotto figlio
				as_versione_variante = ls_valore
			end if
			
		end if

	end if
end if


return 0
end function

public subroutine uof_fase_su_radice_db (string as_cod_prodotto, string as_cod_versione);//controlla se il prodotto ha una fase con flag_stampa posto a si

string		ls_flag_stampa

ib_fase_su_radice = false

select flag_stampa_fase
into :ls_flag_stampa
from tes_fasi_lavorazione
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:as_cod_prodotto and
			cod_versione=:as_cod_versione;

if sqlca.sqlcode=0 and ls_flag_stampa="S" then
	ib_fase_su_radice = true
end if

return
end subroutine

public function integer uof_crea_ramo_fittizio (string as_cod_prodotto, string as_cod_versione, ref string as_errore);string					ls_cod_prodotto_figlio, ls_cod_versione_figlio, ls_cod_misura, ls_cod_deposito_produzione, ls_cod_reparto_produzione[]

long						ll_count, ll_num_sequenza

integer					li_ret

uo_funzioni_1			luo_funzioni_1


//--------------------------------------------------------------------------------------------------------------------------------------------
//di questo IF non ce ne sarebbe bisogno ma lo faccio per sicurezza
if ib_fase_su_radice and ib_nofase_su_figli and is_gestione = "ORD_VEN" then
else
	ib_fase_su_radice = false
	ib_nofase_su_figli = true

	return 0
end if

//--------------------------------------------------------------------------------------------------------------------------------------------
//se il prodotto fittizio da utilizzare non esiste in anagrafica allora lo inserisco
setnull(ll_count)

select count(*)
into :ll_count
from anag_prodotti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:is_figlio_fittizio;

ls_cod_misura = "PZ"

if sqlca.sqlcode<0 then
	as_errore = "Errore in controllo esistenza prodotto fittizio in anag prodotti: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 or isnull(ll_count) or ll_count=0 then
	//inserisci in anagrafica
	insert into anag_prodotti
		(	cod_azienda, cod_prodotto, des_prodotto,
			cod_deposito, cod_misura_mag, cod_misura_acq, cod_misura_ven,
			flag_articolo_fiscale, flag_stato_rifiuto) 
	values
		(	:s_cs_xx.cod_azienda, :is_figlio_fittizio, 'Prodotto Fittizio per la Stampa',
			'100', :ls_cod_misura, :ls_cod_misura, :ls_cod_misura,
			'N', 'S');
		
	if sqlca.sqlcode<0 then
		as_errore = "Errore in inserimento prodotto fittizio in anag prodotti: "+sqlca.sqlerrtext
		return -1
	end if
end if

//1) --------------------------------------------------------------------------------------------------------------------------------------------
//creo il ramo di distinta con prodotto fittizio (se NON presente)
setnull(ls_cod_versione_figlio)
setnull(ll_num_sequenza)

select cod_versione, num_sequenza
into :ls_cod_versione_figlio, :ll_num_sequenza
from distinta
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto_padre=:as_cod_prodotto and cod_versione=:as_cod_versione and
			cod_prodotto_figlio=:is_figlio_fittizio;

if sqlca.sqlcode<0 then
	as_errore = "Errore in controllo esistenza ramo prodotto fittizio in distinta: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 or isnull(ls_cod_versione_figlio) then
	//inserisci il ramo distinta fittizio
	
	//mettiamo come versione la stessa del padre e come num sequenza leggiamo il max + 1000
	ls_cod_versione_figlio = as_cod_versione
	
	select max(num_sequenza)
	into :ll_num_sequenza
	from distinta
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto_padre=:as_cod_prodotto and cod_versione=:as_cod_versione;
	
	if isnull(ll_num_sequenza) then ll_num_sequenza = 0
	ll_num_sequenza += 1000
	
	insert into distinta
		(	cod_azienda, cod_prodotto_padre, cod_versione, num_sequenza,
			cod_prodotto_figlio, cod_versione_figlio )
	values
		(	:s_cs_xx.cod_azienda, :as_cod_prodotto, :as_cod_versione, :ll_num_sequenza,
			:is_figlio_fittizio, :ls_cod_versione_figlio);
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore in inserimento ramo prodotto fittizio in distinta: "+sqlca.sqlerrtext
		return -1
	end if
end if

//lettura deposito produzione e relativi reparti
luo_funzioni_1 = create uo_funzioni_1
li_ret = luo_funzioni_1.uof_estrai_reparto_stabilimento (		as_cod_prodotto, as_cod_versione, "ORD_VEN", &
																					il_anno_registrazione, il_num_registrazione, &
																					ls_cod_deposito_produzione, ls_cod_reparto_produzione[], as_errore)	
destroy luo_funzioni_1

if li_ret > 0 then
	//vuol dire che NON c'è la fase di lavorazione
	as_errore = "Anomalia: fase non trovata per prodotto-versione ("+as_cod_prodotto+"-"+as_cod_versione+")"
	return -1
	
elseif li_ret = 0 then
	//caso normale
	
	if isnull(ls_cod_deposito_produzione)or ls_cod_deposito_produzione="" then
		as_errore = "Anomalia: Deposito produzione non impostato per prodotto-versione ("+as_cod_prodotto+"-"+as_cod_versione+")"
		return -1
	end if
	if upperbound(ls_cod_reparto_produzione[])=0 then
		as_errore = "Anomalia: reparto/i produzione non impostat/io per prodotto-versione ("+as_cod_prodotto+"-"+as_cod_versione+")"
		return -1
	end if
else
	//errore
	return -1
end if


//--------------------------------------------------------------------------------------------------------------------------------------------
//2) creazione variante con gli stessi dati del ramo distinta
setnull(ll_count)

select count(*)
into :ll_count
from varianti_det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:il_anno_registrazione and num_registrazione=:il_num_registrazione and prog_riga_ord_ven=:il_prog_riga and
			cod_prodotto_padre=:as_cod_prodotto and cod_versione=:as_cod_versione and num_sequenza=:ll_num_sequenza and
			cod_prodotto_figlio=:is_figlio_fittizio and cod_versione_figlio=:ls_cod_versione_figlio;

if sqlca.sqlcode<0 then
	as_errore = "Errore in controllo esistenza variante prodotto fittizio in varianti ordine: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 or ll_count=0 or isnull(ll_count) then
	//non esiste, la creo
	insert into varianti_det_ord_ven
	(	cod_azienda, anno_registrazione, num_registrazione, prog_riga_ord_ven,
		cod_prodotto_padre, cod_versione, num_sequenza, 
		cod_prodotto_figlio, cod_versione_figlio, 
		cod_misura, cod_prodotto, cod_versione_variante,
		quan_tecnica, quan_utilizzo, cod_deposito_produzione )
	values
		(	:s_cs_xx.cod_azienda, :il_anno_registrazione, :il_num_registrazione, :il_prog_riga,
			:as_cod_prodotto, :as_cod_versione, :ll_num_sequenza, 
			:is_figlio_fittizio, :ls_cod_versione_figlio,
			:ls_cod_misura, :is_figlio_fittizio, :ls_cod_versione_figlio,
			0, 0, :ls_cod_deposito_produzione);
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore in inserimento variante prodotto fittizio in varianti ordine: "+sqlca.sqlerrtext
		return -1
	end if
	
end if

//--------------------------------------------------------------------------------------------------------------------------------------------
//3) associazione del reparto prelevato dalla fase di lavorazione del prodotto as_cod_prodotto
for ll_count = 1 to upperbound(ls_cod_reparto_produzione[])
	
	insert into varianti_det_ord_ven_prod
	(	cod_azienda, anno_registrazione, num_registrazione, prog_riga_ord_ven,
		cod_prodotto_padre, cod_versione, num_sequenza,
		cod_prodotto_figlio, cod_versione_figlio,
		cod_reparto )  
	values
	(	:s_cs_xx.cod_azienda, :il_anno_registrazione, :il_num_registrazione, :il_prog_riga,
		:as_cod_prodotto, :as_cod_versione, :ll_num_sequenza,   
		:is_figlio_fittizio, :ls_cod_versione_figlio,   
		:ls_cod_reparto_produzione[ll_count] )  ;

	if sqlca.sqlcode < 0 then
		as_errore = "Errore in inserimento variante reparto produzione (gestione prodotto fittizio): " + sqlca.sqlerrtext
		return -1
	end if
next

//a questo punto le date pronto del reparto saranno calcolate grazie alla presenza della variante cosi creata ...


//--------------------------------------------------------------------------------------------------------------------------------------------
//alla fine resetto i semafori
ib_fase_su_radice = false
ib_nofase_su_figli = true


return 0
end function

public function integer uof_controlla_figlio_formula (string as_codice);long			ll_count

setnull(ll_count)

select count(*)
into :ll_count
from anag_prodotti
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :as_codice;

if ll_count > 0 then
	return 0
else
	return -1
end if

end function

on uo_conf_varianti.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_conf_varianti.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

