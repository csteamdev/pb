﻿$PBExportHeader$uo_itemchanged.sru
$PBExportComments$UO eventi itemchange configuratore di prodotto
forward
global type uo_itemchanged from nonvisualobject
end type
end forward

global type uo_itemchanged from nonvisualobject
end type
global uo_itemchanged uo_itemchanged

type variables
string is_cod_modello
end variables

forward prototypes
public function integer uof_itemchanged_plisse (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio)
public function integer uof_itemchanged_supporto_flags (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio)
public function integer uof_itemchanged_optional (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio)
public function integer uof_itemchanged_inizio (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio)
public function integer uof_itemchanged_vern_lastra (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio)
public function integer uof_itemchanged_comandi (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio)
public function integer uof_itemchanged_nota_dettaglio (datawindow fdw_passo, string fs_col_name, string fs_data, string fs_messaggio)
public function integer uof_itemchanged_tessuto (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio)
public function integer uof_controlla_atelier (string fs_cod_tipo_campionario, ref string fs_messaggio)
public function integer uof_reset_tessuto (datawindow fdw_passo, string fs_tessuto_mantovana)
public function integer uof_itemchanged_verniciature (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio)
public function integer uof_validazione_dimensioni (ref datawindow fdw_inizio, ref string fs_messaggio)
public function integer uof_validazione_dimensione_singola (string as_cod_modello, string as_dimensione, decimal ad_valore, ref string as_messaggio)
public function string uof_get_formula_valore_predefinito (long ad_num_variabile)
public function integer uof_calcola_formula_inizio (datawindow fdw_passo, long al_num_dimensione)
end prototypes

public function integer uof_itemchanged_plisse (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio);//	Funzione che gestisce gli eventi itemchanged delle varie DataWindow in maniera da slegare
//		la dipendenza tra Controllo Dw e Oggetto Dw
//
// nome: wf_item_changed
// tipo: none
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//
//									fdw_passo			 DataWindow		valore
//									fs_col_name			 String			valore
//									fs_data				 String			valore
//									fs_messaggio		 String			referenza
//
//		Creata il 31-08-98 
//		Autore Mauro Bordin
//    modificata il 31/08/1998

string ls_cod_tessuto_manuale, ls_cod_verniciatura_manuale, ls_flag_fuori_campionario, ls_flag_addizionale
string ls_cod_veloce, ls_cod_tessuto, ls_cod_tipo_stampo_manuale, ls_cod_tipo_stampo, ls_cod_comando_manuale, &
		 ls_cod_modello_manuale, ls_cod_comando, ls_des_dim_x, ls_des_dim_y, ls_des_dim_z, ls_des_dim_t, &
       ls_cod_tessuto_mant, ls_cod_colore_mant, ls_colore_passamaneria, ls_colore_cordolo 		 
integer li_row
double ld_dim_hp
li_row = 1

choose case fs_col_name

//------------------------------------ d_ext_plisse  --------------------------------------

	case "rs_flag_autoblocco"
		w_configuratore.istr_riepilogo.flag_autoblocco = fs_data
		if w_configuratore.istr_flags.flag_plisse = "S" then
			if w_configuratore.wf_calcoli_plisse(w_configuratore.istr_riepilogo.dimensione_1, w_configuratore.istr_riepilogo.dimensione_2, fs_data, &
										w_configuratore.istr_riepilogo.flag_misura_luce_finita, w_configuratore.istr_riepilogo.cod_tessuto, &
										w_configuratore.istr_riepilogo.cod_non_a_magazzino, fs_messaggio) = -1 then
				return -1
			end if
		end if
		fdw_passo.setitem(1, "rn_num_fili_plisse", w_configuratore.istr_riepilogo.num_fili_plisse)
		fdw_passo.setitem(1, "rn_num_boccole_plisse", w_configuratore.istr_riepilogo.num_boccole_plisse)
		fdw_passo.setitem(1, "rn_intervallo_punzoni_plisse", w_configuratore.istr_riepilogo.intervallo_punzoni_plisse)
		fdw_passo.setitem(1, "rn_num_pieghe_plisse", w_configuratore.istr_riepilogo.num_pieghe_plisse)
		fdw_passo.setitem(1, "rn_prof_inf_plisse", w_configuratore.istr_riepilogo.prof_inf_plisse)
		fdw_passo.setitem(1, "rn_prof_sup_plisse", w_configuratore.istr_riepilogo.prof_sup_plisse)

	case "rn_num_fili_plisse"
		w_configuratore.istr_riepilogo.num_fili_plisse = double(fs_data)
	case "rn_num_boccole_plisse"
		w_configuratore.istr_riepilogo.num_boccole_plisse = double(fs_data)
	case "rn_intervallo_punzoni_plisse"
		w_configuratore.istr_riepilogo.intervallo_punzoni_plisse = double(fs_data)
	case "rn_num_pieghe_plisse"
		w_configuratore.istr_riepilogo.num_pieghe_plisse = double(fs_data)
	case "rn_prof_inf_plisse"
		w_configuratore.istr_riepilogo.prof_inf_plisse = double(fs_data)
	case "rn_prof_sup_plisse"
		w_configuratore.istr_riepilogo.prof_sup_plisse = double(fs_data)
end choose
end function

public function integer uof_itemchanged_supporto_flags (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio);////	Funzione che gestisce gli eventi itemchanged delle varie DataWindow in maniera da slegare
////		la dipendenza tra Controllo Dw e Oggetto Dw
////
//// nome: wf_item_changed
//// tipo: none
////  
////	Variabili passate: 		nome					 tipo				passaggio per			commento
////
////									fdw_passo			 DataWindow		valore
////									fs_col_name			 String			valore
////									fs_data				 String			valore
////									fs_messaggio		 String			referenza
////
////		Creata il 31-08-98 
////		Autore Mauro Bordin
////    modificata il 31/08/1998
//
//string ls_flag_fuori_campionario, ls_flag_addizionale, ls_cod_veloce, ls_sup_verniciatura	, &
//       ls_cod_supporto_default
//integer li_row
//long   ll_cont
//double ld_dim_hp
//li_row = 1
//
//ls_sup_verniciatura = w_configuratore.istr_riepilogo.cod_verniciatura
//if isnull(ls_sup_verniciatura) or ls_sup_verniciatura = "" then
//	messagebox("Proposta Supporto","Non essendo stata impostata nessuna verniciatura non è possibile proporre un supporto", Information!)
//	return -1
//end if
//
//choose case fs_col_name
//
/////--------------------------------- d_ext_supporto_flags ------------------------------------
//
//	case "rs_cod_supporto"
//		if fs_data = "" or isnull(fs_data) then
//			ls_flag_fuori_campionario = "N"
//			ls_flag_addizionale = "N"
//		else
//			select count(*)
//			into   :ll_cont
//			from   tab_modelli_tipi_supp_vernic
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//					 cod_modello = :is_cod_modello and
//					 cod_verniciatura = :ls_sup_verniciatura and
//					 flag_default = 'S';
//			if sqlca.sqlcode <> 0 or ll_cont > 1 then
//				messagebox("Ricerca tipo supporto","Attenzione! Per questo modello/verniciatura non esistono tipi supporti associate oppure è stata specificata più di un supporto di default", Information!)
//				return -1
//			else				
//				f_PO_LoadDDDW_DW(fdw_passo,"rs_cod_supporto",sqlca,&
//									  "anag_prodotti","cod_prodotto","des_prodotto",&
//									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_tipo_supporto from tab_modelli_tipi_supp_vernic where cod_azienda = '" + &
//									  s_cs_xx.cod_azienda + "' and cod_modello = '" + is_cod_modello + "' and cod_verniciatura = '" + ls_sup_verniciatura + "')")
//				
//				select cod_tipo_supporto,
//						 flag_fuori_campionario,
//						 flag_addizionale
//				into   :ls_cod_supporto_default,
//						 :ls_flag_fuori_campionario,
//						 :ls_flag_addizionale
//				from   tab_modelli_tipi_supp_vernic
//				where  cod_azienda = :s_cs_xx.cod_azienda and
//						 cod_modello = :is_cod_modello and
//						 cod_verniciatura = :ls_sup_verniciatura and
//						 flag_default = 'S';
//				if sqlca.sqlcode <> 0 then
//					messagebox("Estrazione tipo supporto di Default", "Dettaglio errore: " + sqlca.sqlerrtext)
//					return -1
//				end if
//			end if
//		end if
//		w_configuratore.istr_riepilogo.cod_tipo_supporto = fs_data
//		fdw_passo.setitem(li_row, "rs_flag_fuori_campionario", ls_flag_fuori_campionario)
//		fdw_passo.setitem(li_row, "rs_flag_addizionale", ls_flag_addizionale)
//		fdw_passo.setitem(li_row, "rs_cod_supporto_manuale", fs_data)
//		
//	case "rs_cod_supporto_manuale"
//		if fs_data = "" or isnull(fs_data) then
//			ls_flag_fuori_campionario = "N"
//			ls_flag_addizionale = "N"
//		else
//			select count(*)
//			into   :ll_cont
//			from   tab_modelli_tipi_supp_vernic
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//					 cod_modello = :is_cod_modello and
//					 cod_verniciatura = :ls_sup_verniciatura and
//					 flag_default = 'S';
//			if ll_cont <> 0 then
//				messagebox("Ricerca tipo supporto","Attenzione! Per questo modello/verniciatura non esistono tipi supporti associate oppure è stata specificata più di un supporto di default", Information!)
//				return -1
//			else
//				f_PO_LoadDDDW_DW(fdw_passo,"rs_cod_supporto",sqlca,&
//									  "anag_prodotti","cod_prodotto","des_prodotto",&
//									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_tipo_supporto from tab_modelli_tipi_supp_vernic where cod_azienda = '" + &
//									  s_cs_xx.cod_azienda + "' and cod_modello = '" + is_cod_modello + "' and cod_verniciatura = '" + ls_sup_verniciatura + "')")
//				
//				select cod_tipo_supporto,
//						 flag_fuori_campionario,
//						 flag_addizionale
//				into   :ls_cod_supporto_default,
//						 :ls_flag_fuori_campionario,
//						 :ls_flag_addizionale
//				from   tab_modelli_tipi_supp_vernic
//				where  cod_azienda = :s_cs_xx.cod_azienda and
//						 cod_modello = :is_cod_modello and
//						 cod_verniciatura = :ls_sup_verniciatura and
//						 flag_default = 'S';
//				if sqlca.sqlcode <> 0 then
//					messagebox("Estrazione tipo supporto di Default", "Dettaglio errore: " + sqlca.sqlerrtext)
//					return -1
//				end if
//			end if
//		end if
//
//
//		w_configuratore.istr_riepilogo.cod_tipo_supporto = fs_data
//		fdw_passo.setitem(li_row, "rs_flag_fuori_campionario", ls_flag_fuori_campionario)
//		fdw_passo.setitem(li_row, "rs_flag_addizionale", ls_flag_addizionale)
//		fdw_passo.setitem(li_row, "rs_cod_supporto", fs_data)
//	case "rs_flag_cappotta_frontale"
//		w_configuratore.istr_riepilogo.flag_cappotta_frontale = fs_data
//	case "rs_flag_rollo"
//		w_configuratore.istr_riepilogo.flag_rollo = fs_data
//	case "rs_flag_kit_laterale"
//		w_configuratore.istr_riepilogo.flag_kit_laterale = fs_data
//	case "rs_flag_misura_luce_finita"
//		w_configuratore.istr_riepilogo.flag_misura_luce_finita = fs_data
//	case "rn_num_gambe"
//		w_configuratore.istr_riepilogo.num_gambe = double(fs_data)
//	case "rn_num_campate"
//		w_configuratore.istr_riepilogo.num_campate = double(fs_data)
//	case "rs_note"
//		w_configuratore.istr_riepilogo.note = fs_data
//	case "rs_flag_addizionale"
//		w_configuratore.istr_riepilogo.flag_addizionale_supporto = fs_data
//end choose
//return 0

//	Funzione che gestisce gli eventi itemchanged delle varie DataWindow in maniera da slegare
//		la dipendenza tra Controllo Dw e Oggetto Dw
//
// nome: wf_item_changed
// tipo: none
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//
//									fdw_passo			 DataWindow		valore
//									fs_col_name			 String			valore
//									fs_data				 String			valore
//									fs_messaggio		 String			referenza
//
//		Creata il 31-08-98 
//		Autore Mauro Bordin
//    modificata il 31/08/1998

string ls_flag_fuori_campionario, ls_flag_addizionale, ls_cod_veloce, ls_sup_verniciatura	, &
       ls_cod_supporto_default
integer li_row
long   ll_cont
double ld_dim_hp
li_row = 1

ls_sup_verniciatura = w_configuratore.istr_riepilogo.cod_verniciatura
if isnull(ls_sup_verniciatura) or ls_sup_verniciatura = "" then
	g_mb.messagebox("Proposta Supporto","Non essendo stata impostata nessuna verniciatura non è possibile proporre un supporto", Information!)
	return -1
end if

choose case fs_col_name

///--------------------------------- d_ext_supporto_flags ------------------------------------

	case "rs_cod_supporto"
		if fs_data = "" or isnull(fs_data) then
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select count(*)
			into   :ll_cont
			from   tab_modelli_tipi_supp_vernic
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_modello = :is_cod_modello and
					 cod_verniciatura = :ls_sup_verniciatura and
					 cod_tipo_supporto = :fs_data;
			if sqlca.sqlcode = 100 then
				g_mb.messagebox("Ricerca tipo supporto","Attenzione! Per questo modello/verniciatura non esiste il tipo supporto selezionato", Information!)
				return -1
			elseif sqlca.sqlcode = -1 then
				g_mb.messagebox("Ricerca tipo supporto","Errore in select tab_modelli_tipi_supp_vernic. Dettaglio " + sqlca.sqlerrtext, Information!)
				return -1
			else				
				select cod_tipo_supporto,
						 flag_fuori_campionario,
						 flag_addizionale
				into   :ls_cod_supporto_default,
						 :ls_flag_fuori_campionario,
						 :ls_flag_addizionale
				from   tab_modelli_tipi_supp_vernic
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_modello = :is_cod_modello and
						 cod_verniciatura = :ls_sup_verniciatura and
						 cod_tipo_supporto = :fs_data;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Estrazione tipo supporto di Default", "Dettaglio errore: " + sqlca.sqlerrtext)
					return -1
				end if
			end if
		end if
		w_configuratore.istr_riepilogo.cod_tipo_supporto = fs_data
		w_configuratore.istr_riepilogo.flag_addizionale_supporto = ls_flag_addizionale
		w_configuratore.istr_riepilogo.flag_fc_supporto = ls_flag_fuori_campionario
		fdw_passo.setitem(li_row, "rs_flag_fuori_campionario", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_flag_addizionale", ls_flag_addizionale)
		fdw_passo.setitem(li_row, "rs_cod_supporto_manuale", fs_data)
		
	case "rs_cod_supporto_manuale"
		if fs_data = "" or isnull(fs_data) then
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select count(*)
			into   :ll_cont
			from   tab_modelli_tipi_supp_vernic
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_modello = :is_cod_modello and
					 cod_verniciatura = :ls_sup_verniciatura and
					 cod_tipo_supporto = :fs_data;
			if sqlca.sqlcode = 100 then
				g_mb.messagebox("Ricerca tipo supporto","Attenzione! Per questo modello/verniciatura non esiste il tipo supporto selezionato", Information!)
				return -1
			elseif sqlca.sqlcode = -1 then
				g_mb.messagebox("Ricerca tipo supporto","Errore in select tab_modelli_tipi_supp_vernic. Dettaglio " + sqlca.sqlerrtext, Information!)
				return -1
			else				
				select cod_tipo_supporto,
						 flag_fuori_campionario,
						 flag_addizionale
				into   :ls_cod_supporto_default,
						 :ls_flag_fuori_campionario,
						 :ls_flag_addizionale
				from   tab_modelli_tipi_supp_vernic
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_modello = :is_cod_modello and
						 cod_verniciatura = :ls_sup_verniciatura and
						 cod_tipo_supporto = :fs_data;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Estrazione tipo supporto di Default", "Dettaglio errore: " + sqlca.sqlerrtext)
					return -1
				end if
			end if
		end if

		w_configuratore.istr_riepilogo.cod_tipo_supporto = fs_data
		w_configuratore.istr_riepilogo.flag_addizionale_supporto = ls_flag_addizionale
		w_configuratore.istr_riepilogo.flag_fc_supporto = ls_flag_fuori_campionario
		fdw_passo.setitem(li_row, "rs_flag_fuori_campionario", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_flag_addizionale", ls_flag_addizionale)
		fdw_passo.setitem(li_row, "rs_cod_supporto", fs_data)
	case "rs_flag_cappotta_frontale"
		w_configuratore.istr_riepilogo.flag_cappotta_frontale = fs_data
	case "rs_flag_rollo"
		w_configuratore.istr_riepilogo.flag_rollo = fs_data
	case "rs_flag_kit_laterale"
		w_configuratore.istr_riepilogo.flag_kit_laterale = fs_data
	case "rs_flag_misura_luce_finita"
		w_configuratore.istr_riepilogo.flag_misura_luce_finita = fs_data
	case "rn_num_gambe"
		w_configuratore.istr_riepilogo.num_gambe = double(fs_data)
	case "rn_num_campate"
		w_configuratore.istr_riepilogo.num_campate = double(fs_data)
	case "rs_note"
		w_configuratore.istr_riepilogo.note = fs_data
	case "rs_flag_addizionale"
		w_configuratore.istr_riepilogo.flag_addizionale_supporto = fs_data
	case "rs_flag_fuori_campionario"
		w_configuratore.istr_riepilogo.flag_fc_supporto = fs_data
end choose
return 0
end function

public function integer uof_itemchanged_optional (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio);//	Funzione che gestisce gli eventi itemchanged delle varie DataWindow in maniera da slegare
//		la dipendenza tra Controllo Dw e Oggetto Dw
//
// nome: wf_item_changed
// tipo: none
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
// ========================================================================================
//									fdw_passo			 DataWindow		valore
//									fs_col_name			 String			valore
//									fs_data				 String			valore
//									fs_messaggio		 String			referenza
//
//		Creata il 06/10/2000
//		Autore Enrico Menegotto

integer li_row
boolean lb_test
string  ls_ret, ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_colonna_sconto, ls_cod_tipo_listino_prodotto, ls_cod_cliente, ls_cod_valuta, &
		 ls_flag_tipo_det_ven, ls_modify, ls_null, ls_cod_agente_1, ls_des_prodotto, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_messaggio, &
		 ls_nota_prodotto, ls_cod_versione, ls_flag_prezzo,ls_flag_blocco, ls_flag_dimensioni, ls_cod_cat_mer_optionals_config, &
		 ls_cod_cat_mer,ls_des_variabile
double ld_quantita, ld_cambio_ven, ld_variazioni[], ld_num_confezioni, ld_num_pezzi, ld_ultimo_prezzo, ll_sconti[], ll_y, &
       ll_maggiorazioni[], ld_provv_1, ld_provv_2, ld_gruppi_sconti[]
datetime ldt_data_consegna, ldt_data_esenzione_iva, ldt_data_riferimento
long ll_riga_origine, ll_i, ll_riga
dec{4} ld_dim_x, ld_dim_y
datawindow ld_datawindow
commandbutton lc_prodotti_ricerca
picturebutton lp_prod_view
uo_condizioni_cliente luo_condizioni_cliente

li_row = 1
ll_riga = fdw_passo.getrow()
if ll_riga < 1 then return 0

setnull(ls_null)
lb_test = false
ls_cod_tipo_listino_prodotto = str_conf_prodotto.cod_tipo_listino_prodotto
ls_cod_cliente = str_conf_prodotto.cod_cliente
ls_cod_valuta = str_conf_prodotto.cod_valuta
ls_cod_agente_1 = str_conf_prodotto.cod_agente_1
ls_cod_agente_2 = str_conf_prodotto.cod_agente_2
ls_cod_tipo_det_ven = fdw_passo.getitemstring(ll_riga, "rs_cod_tipo_det_ven")
ldt_data_consegna = str_conf_prodotto.data_consegna
ld_dim_x = fdw_passo.getitemnumber(ll_riga, "dim_x")
ld_dim_y = fdw_passo.getitemnumber(ll_riga, "dim_y")
if isnull(ld_dim_x) then ld_dim_x = 0
if isnull(ld_dim_y) then ld_dim_y = 0
ls_flag_dimensioni="N"

//EnMe 10/12/2012 Valorizzazione prezzi optional configurabili anche con opzioni da distinta
s_cs_xx.listino_db.flag_calcola ="S"

guo_functions.uof_get_parametro_azienda( "OCF", ls_cod_cat_mer_optionals_config)

if isnull(ls_cod_cat_mer_optionals_config) then ls_cod_cat_mer_optionals_config = ""

// 27/3/2008 deciso con Beatrice; da oggi sempre data di registrazione ordine
ldt_data_riferimento = str_conf_prodotto.data_registrazione

choose case fs_col_name
	case "rs_cod_tipo_det_ven"
		select flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_tipo_det_ven = :fs_data;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Attenzione ! Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita."
			return - 1
		end if
		fdw_passo.setitem(ll_riga, "rs_cod_prodotto", ls_null)
		fdw_passo.setitem(ll_riga, "rs_des_prodotto", ls_null)
		fdw_passo.setitem(ll_riga, "rs_cod_misura", ls_null)
		fdw_passo.setitem(ll_riga, "rd_quan_vendita", 0)
		fdw_passo.setitem(ll_riga, "sconto_1", 0)
		fdw_passo.setitem(ll_riga, "sconto_2", 0)
		fdw_passo.setitem(ll_riga, "sconto_3", 0)
		fdw_passo.setitem(ll_riga, "sconto_4", 0)
		fdw_passo.setitem(ll_riga, "sconto_5", 0)
		fdw_passo.setitem(ll_riga, "sconto_6", 0)
		fdw_passo.setitem(ll_riga, "sconto_7", 0)
		fdw_passo.setitem(ll_riga, "sconto_8", 0)
		fdw_passo.setitem(ll_riga, "sconto_9", 0)
		fdw_passo.setitem(ll_riga, "sconto_10", 0)
		fdw_passo.setitem(ll_riga, "prezzo_vendita", 0)
		fdw_passo.setitem(ll_riga, "provvigione_1", 0)
		fdw_passo.setitem(ll_riga, "provvigione_2", 0)
		fdw_passo.setitem(ll_riga, "rs_cod_iva", ls_null)
		fdw_passo.setitem(ll_riga, "flag_dimensioni", ls_flag_dimensioni)
		fdw_passo.setitem(ll_riga, "dim_x", 0)
		fdw_passo.setitem(ll_riga, "dim_y", 0)
		
		if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" then
			ls_modify = "rs_cod_prodotto.protect='0'~t"
			ls_modify = ls_modify + "rs_cod_prodotto.background.color='16777215'~t"
			fdw_passo.modify(ls_modify)
		else
			ls_modify = "rs_cod_prodotto.protect='1'~t"
			ls_modify = ls_modify + "rs_cod_prodotto.background.color='12632256'~t"
			fdw_passo.modify(ls_modify)
		end if
		if isnull(fdw_passo.getitemstring(ll_riga, "rs_cod_iva")) then
			select cod_iva  
			into   :ls_cod_iva  
			from   tab_tipi_det_ven  
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_tipo_det_ven = :fs_data;
			if sqlca.sqlcode = 0 then
				fdw_passo.setitem(ll_riga, "rs_cod_iva", ls_cod_iva)
			end if
		end if
		
		
	case "rs_cod_prodotto"
		if not isnull(ls_cod_tipo_det_ven) then
			select 	cod_iva
			into   		:ls_cod_iva
			from   	tab_tipi_det_ven  
			where  	cod_azienda = :s_cs_xx.cod_azienda and  
					 	cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			if sqlca.sqlcode = 0 then
				fdw_passo.setitem(ll_riga, "rs_cod_iva", ls_cod_iva)
			end if
		end if
		
		select cod_misura_mag,
				 cod_iva,
				 des_prodotto,
				 flag_blocco,
				 cod_cat_mer
		into     :ls_cod_misura,
				 :ls_cod_iva,
				 :ls_des_prodotto,
				 :ls_flag_blocco,
				 :ls_cod_cat_mer
		from   anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_prodotto = :fs_data;
		if sqlca.sqlcode = 0 then
			// 17/06/2008: aggiunto su richiesta di beatrice controllo del blocco
			if ls_flag_blocco = "S" then
				g_mb.messagebox("Configuratore","Attenzione! Prodotto Bloccato da anagrafica prodotti.", Information!)
			end if
			
			fdw_passo.setitem(ll_riga, "rs_cod_misura", ls_cod_misura)
			fdw_passo.setitem(ll_riga, "rs_des_prodotto", ls_des_prodotto)
			
			if not isnull(ls_cod_iva) then fdw_passo.setitem(ll_riga, "rs_cod_iva", ls_cod_iva)
			
			if ls_cod_cat_mer = ls_cod_cat_mer_optionals_config then
				fdw_passo.setitem(ll_riga, "flag_dimensioni", "S")
				ls_des_variabile = ""
				
				select des_variabile
				into   :ls_des_variabile
				from tab_des_variabili 
				where cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :fs_data and
						 cod_variabile = 'DIM_1' and 
						 flag_ipertech_apice='S';
				if sqlca.sqlcode <> 0 then ls_des_variabile = " - - - "
				fdw_passo.setitem(ll_riga, "des_dim_x", ls_des_variabile)
				
				ls_des_variabile = ""
				
				select des_variabile
				into   :ls_des_variabile
				from tab_des_variabili 
				where cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :fs_data and
						 cod_variabile = 'DIM_2' and
						 flag_ipertech_apice='S';
				if sqlca.sqlcode <> 0 then ls_des_variabile = " - - - "
				fdw_passo.setitem(ll_riga, "des_dim_y", ls_des_variabile)
				
			else
				fdw_passo.setitem(ll_riga, "flag_dimensioni", "N")
			end if
			
		else
			
			g_mb.messagebox("APICE","Prodotto inesistente")
			return 2
			
		end if

	   if not isnull(str_conf_prodotto.cod_contatto) then
			
			f_trova_codice_iva ( 	str_conf_prodotto.data_documento, &
											ls_cod_tipo_det_ven, &
											"O", &
											str_conf_prodotto.cod_contatto, &
											ref ls_cod_iva )
			
//			select cod_iva,
//					 data_esenzione_iva
//			into   :ls_cod_iva,
//					 :ldt_data_esenzione_iva
//			from   anag_contatti
//			where  cod_azienda = :s_cs_xx.cod_azienda and 
//					 cod_contatto = :str_conf_prodotto.cod_contatto ;
//			if sqlca.sqlcode = 0 then
//				if not isnull(ls_cod_iva) and (ldt_data_esenzione_iva <= str_conf_prodotto.data_documento or isnull(str_conf_prodotto.data_documento)) then
//					ls_iva = ls_cod_iva
//				end if
//			end if
			
	   elseif not isnull(str_conf_prodotto.cod_cliente) then
			
			f_trova_codice_iva ( 	str_conf_prodotto.data_documento, &
											ls_cod_tipo_det_ven, &
											"C", &
											str_conf_prodotto.cod_cliente, &
											ref ls_cod_iva )
//			select cod_iva,
//					 data_esenzione_iva
//			into   :ls_cod_iva,
//					 :ldt_data_esenzione_iva
//			from   anag_clienti
//			where  cod_azienda = :s_cs_xx.cod_azienda and 
//					 cod_cliente = :str_conf_prodotto.cod_cliente;
//			if sqlca.sqlcode = 0 then
//				if not isnull(ls_cod_iva) and (ldt_data_esenzione_iva <= str_conf_prodotto.data_documento or isnull(str_conf_prodotto.data_documento)) then
//					ls_iva = ls_cod_iva
//				end if
//			end if
			
		end if

		fdw_passo.setitem(ll_riga, "rs_cod_iva", ls_cod_iva)

//		if not isnull(ls_cod_cliente) then
//			select cod_iva,     data_esenzione_iva
//			into   :ls_cod_iva, :ldt_data_esenzione_iva
//			from   anag_clienti
//			where  cod_azienda = :s_cs_xx.cod_azienda and 
//					 cod_cliente = :ls_cod_cliente;
//		end if

//		if sqlca.sqlcode = 0 then
//			if ls_cod_iva <> "" and not isnull(ls_cod_iva) and	(ldt_data_esenzione_iva <= str_conf_prodotto.data_registrazione or isnull(str_conf_prodotto.data_registrazione)) then
//				fdw_passo.setitem(ll_riga, "rs_cod_iva", ls_cod_iva)
//			end if
//		end if

		fdw_passo.setitem(ll_riga, "dim_x", 0)
		fdw_passo.setitem(ll_riga, "dim_y", 0)
		ld_dim_x = 0
		ld_dim_y = 0

		ls_cod_prodotto = fs_data
		ld_quantita = fdw_passo.getitemnumber(ll_riga, "rd_quan_vendita")
		luo_condizioni_cliente = CREATE uo_condizioni_cliente
		luo_condizioni_cliente.str_parametri.ldw_oggetto = fdw_passo
		luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
		luo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
		luo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_riferimento
		luo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
		luo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
		luo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
		luo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
		luo_condizioni_cliente.str_parametri.quantita = ld_quantita
		luo_condizioni_cliente.str_parametri.valore = 0
		luo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
		luo_condizioni_cliente.str_parametri.colonna_quantita = "rd_quan_vendita"
		luo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
		luo_condizioni_cliente.wf_condizioni_cliente()
		destroy luo_condizioni_cliente
		
	case "rd_quan_vendita"
		
		ld_quantita = double(fs_data)

		select flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Attenzione ! Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita."
			return - 1
		end if
		
		if (ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C") and (ld_quantita = 0 or isnull(ld_quantita)) then
			g_mb.messagebox("APICE","Attenzione quantità a ZERO", Information!)
		end if 
		
		luo_condizioni_cliente = CREATE uo_condizioni_cliente
		ls_cod_prodotto = fdw_passo.getitemstring(ll_riga, "rs_cod_prodotto")
		luo_condizioni_cliente.str_parametri.ldw_oggetto = fdw_passo
		luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
		luo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
		luo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_riferimento
		luo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
		luo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
		luo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
		luo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
		luo_condizioni_cliente.str_parametri.quantita = ld_quantita
		luo_condizioni_cliente.str_parametri.valore = 0
		luo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
		luo_condizioni_cliente.str_parametri.colonna_quantita = "rd_quan_vendita"
		luo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
		luo_condizioni_cliente.wf_condizioni_cliente()
		destroy luo_condizioni_cliente
		
	case "dim_x"
		
		ld_dim_x = dec(fs_data)

		select flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Attenzione ! Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita."
			return - 1
		end if
		
		ld_quantita = fdw_passo.getitemnumber(ll_riga, "rd_quan_vendita")		
		
		if (ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C") and (ld_quantita = 0 or isnull(ld_quantita)) then
			g_mb.messagebox("APICE","Attenzione quantità a ZERO", Information!)
		end if 
		
		luo_condizioni_cliente = CREATE uo_condizioni_cliente
		ls_cod_prodotto = fdw_passo.getitemstring(ll_riga, "rs_cod_prodotto")
		luo_condizioni_cliente.str_parametri.ldw_oggetto = fdw_passo
		luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
		luo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
		luo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_riferimento
		luo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
		luo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
		luo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
		luo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
		luo_condizioni_cliente.str_parametri.quantita = ld_quantita
		luo_condizioni_cliente.str_parametri.valore = 0
		luo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
		luo_condizioni_cliente.str_parametri.colonna_quantita = "rd_quan_vendita"
		luo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
		luo_condizioni_cliente.wf_condizioni_cliente()
		destroy luo_condizioni_cliente
		
	case "dim_y"
		
		ld_dim_y = dec(fs_data)

		select flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Attenzione ! Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita."
			return - 1
		end if
		
		ld_quantita = fdw_passo.getitemnumber(ll_riga, "rd_quan_vendita")		
		
		if (ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C") and (ld_quantita = 0 or isnull(ld_quantita)) then
			g_mb.messagebox("APICE","Attenzione quantità a ZERO", Information!)
		end if 
		
		luo_condizioni_cliente = CREATE uo_condizioni_cliente
		ls_cod_prodotto = fdw_passo.getitemstring(ll_riga, "rs_cod_prodotto")
		luo_condizioni_cliente.str_parametri.ldw_oggetto = fdw_passo
		luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
		luo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
		luo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_riferimento
		luo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
		luo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
		luo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
		luo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
		luo_condizioni_cliente.str_parametri.quantita = ld_quantita
		luo_condizioni_cliente.str_parametri.valore = 0
		luo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
		luo_condizioni_cliente.str_parametri.colonna_quantita = "rd_quan_vendita"
		luo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
		luo_condizioni_cliente.wf_condizioni_cliente()
		destroy luo_condizioni_cliente
		
end choose
return 0
end function

public function integer uof_itemchanged_inizio (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio);//	Funzione che gestisce gli eventi itemchanged delle varie DataWindow in maniera da slegare
//		la dipendenza tra Controllo Dw e Oggetto Dw
//
// nome: wf_item_changed
// tipo: none
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//
//									fdw_passo			 DataWindow		valore
//									fs_col_name			 String			valore
//									fs_data				 String			valore
//									fs_messaggio		 String			referenza
//
//		Creata il 31-08-98 
//		Autore ENRICO MENEGOTTO
//    modificata il 31/08/1998

string ls_cod_modello_manuale,  ls_des_dim_x, ls_des_dim_y, ls_des_dim_z, ls_des_dim_t, ls_flag_blocco,&
		 ls_des_modello, ls_des_prodotto, ls_null, ls_errore
integer li_row
long    ll_prog_des_prodotto
double ld_dim_hp, ld_dim_max, ld_dim_min, ld_dim
dec{4} ld_risultato

li_row = 1
setnull(ls_null)

choose case fs_col_name

	case "rs_cod_modello"
		select flag_blocco
		 into :ls_flag_blocco
		 from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :fs_data;

		// 17/06/2008: aggiunto su richiesta di beatrice controllo del blocco
		if ls_flag_blocco = "S" then
			g_mb.messagebox("Configuratore","Attenzione! Modello Bloccato da anagrafica prodotti.", Information!)
		end if

		fdw_passo.setitem(li_row, "rs_cod_modello_manuale", fs_data)
		
		// aggiunto da Enrico per specifica "Richieste_varie_2010" 03/06/2010
		f_PO_LoadDDDW_DW(fdw_passo,"rn_prog_des_modello",sqlca,&
							  "tab_modelli_descrizioni","progressivo","des_modello",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_modello = '" + fs_data + "' and " + &
							  "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")


	case "rs_cod_modello_manuale"
		select cod_prodotto,
				 flag_blocco
		 into :ls_cod_modello_manuale,
		 		:ls_flag_blocco
		 from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :fs_data;

		if SQLCA.sqlcode <> 0 then
			fs_messaggio = "Codice Modello Manuale non Valido"
			return -1
		end if
		
		// 17/06/2008: aggiunto su richiesta di beatrice controllo del blocco
		if ls_flag_blocco = "S" then
			g_mb.messagebox("Configuratore","Attenzione! Modello Bloccato da anagrafica prodotti.", Information!)
		end if
		
		fdw_passo.setitem(li_row, "rs_cod_modello", fs_data)

		// aggiunto da Enrico per specifica "Richieste_varie_2010" 03/06/2010
		f_PO_LoadDDDW_DW(fdw_passo,"rn_prog_des_modello",sqlca,&
							  "tab_modelli_descrizioni","progressivo","des_modello",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_modello = '" + fs_data + "' and " + &
							  "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")


	case "rs_cod_versione"
		if isnull(fs_data) or fs_data = "" then return -1
		str_conf_prodotto.cod_versione = fs_data

	case "rd_dimensione_1"
		ls_cod_modello_manuale = fdw_passo.getitemstring(li_row, "rs_cod_modello")
		if isnull(ls_cod_modello_manuale) then
			fs_messaggio = "Inserire prima il codice del modello"
			return -1
		end if
		
		if uof_validazione_dimensione_singola( ls_cod_modello_manuale, "dim_x", dec(fs_data), ref ls_errore) < 0 then
			g_mb.messagebox("Dimensioni",ls_errore, Information!)
		end if		
		/*
		select limite_max_dim_x, limite_min_dim_x
		into   :ld_dim_max, :ld_dim_min
		from   tab_flags_configuratore
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_modello = :ls_cod_modello_manuale;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca limiti dimensionali del modello"
			return -1
		end if
		ld_dim = double(fs_data)
		if ld_dim > ld_dim_max or ld_dim < ld_dim_min then
			g_mb.messagebox("Dimensioni","Attenzione! Dimensione fuori dei limiti consentiti per il modello", Information!)
		end if
		*/	
		w_configuratore.istr_riepilogo.dimensione_1 = double(fs_data)
		
		ld_dim_hp = w_configuratore.wf_verifica_rapp_misure(double(fs_data), fdw_passo.getitemnumber(1, "rd_dimensione_2"), ref fs_messaggio)
		
		if ld_dim_hp = -1 then
			return -1
		elseif ld_dim_hp > 0 then
			fdw_passo.setitem(1, "rd_dimensione_3", ld_dim_hp)			
		end if
		if w_configuratore.istr_flags.flag_plisse = "S" then
			if w_configuratore.wf_calcoli_plisse(double(fs_data), w_configuratore.istr_riepilogo.dimensione_2, w_configuratore.istr_riepilogo.flag_autoblocco, &
										w_configuratore.istr_riepilogo.flag_misura_luce_finita, w_configuratore.istr_riepilogo.cod_tessuto, &
										w_configuratore.istr_riepilogo.cod_non_a_magazzino, fs_messaggio) = -1 then
				return -1
			end if
		end if
		
		w_configuratore.wf_gest_mp_non_richieste(dec(fs_data), fdw_passo.getitemnumber(1, "rd_dimensione_2"), fdw_passo.getitemnumber(1, "rd_dimensione_3"), fdw_passo.getitemnumber(1, "rd_dimensione_4") ,w_configuratore.istr_riepilogo.alt_asta)

		uof_calcola_formula_inizio(  fdw_passo, 1)
	case "rd_dimensione_2"
		ls_cod_modello_manuale = fdw_passo.getitemstring(li_row, "rs_cod_modello")
		if isnull(ls_cod_modello_manuale) then
			fs_messaggio = "Inserire prima il codice del modello"
			return -1
		end if
		
		if uof_validazione_dimensione_singola( ls_cod_modello_manuale, "dim_y", dec(fs_data), ref ls_errore) < 0 then
			g_mb.messagebox("Dimensioni",ls_errore, Information!)
		end if		
/*		
		select limite_max_dim_y, limite_min_dim_y
		into   :ld_dim_max, :ld_dim_min
		from   tab_flags_configuratore
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_modello = :ls_cod_modello_manuale;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca limiti dimensionali del modello"
			return -1
		end if
		ld_dim = double(fs_data)
		if ld_dim > ld_dim_max or ld_dim < ld_dim_min then
			g_mb.messagebox("Dimensioni","Attenzione! Dimensione fuori dei limiti consentiti per il modello", Information!)
		end if
*/
		w_configuratore.istr_riepilogo.dimensione_2 = double(fs_data)
		
		if w_configuratore.istr_flags.flag_plisse = "S" then
			if w_configuratore.wf_calcoli_plisse(w_configuratore.istr_riepilogo.dimensione_1, double(fs_data), w_configuratore.istr_riepilogo.flag_autoblocco, &
										w_configuratore.istr_riepilogo.flag_misura_luce_finita, w_configuratore.istr_riepilogo.cod_tessuto, &
										w_configuratore.istr_riepilogo.cod_non_a_magazzino, fs_messaggio) = -1 then
				return -1
			end if
		end if
		w_configuratore.wf_gest_mp_non_richieste(fdw_passo.getitemnumber(1, "rd_dimensione_1"), dec(fs_data), fdw_passo.getitemnumber(1, "rd_dimensione_3"),fdw_passo.getitemnumber(1, "rd_dimensione_4"), w_configuratore.istr_riepilogo.alt_asta)
		
		uof_calcola_formula_inizio(  fdw_passo, 2)
	case "rd_dimensione_3"
		ls_cod_modello_manuale = fdw_passo.getitemstring(li_row, "rs_cod_modello")
		if isnull(ls_cod_modello_manuale) then
			fs_messaggio = "Inserire prima il codice del modello"
			return -1
		end if
		if uof_validazione_dimensione_singola( ls_cod_modello_manuale, "dim_z", dec(fs_data), ref ls_errore) < 0 then
			g_mb.messagebox("Dimensioni",ls_errore, Information!)
		end if		
/*		
		select limite_max_dim_z, limite_min_dim_z
		into   :ld_dim_max, :ld_dim_min
		from   tab_flags_configuratore
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_modello = :ls_cod_modello_manuale;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca limiti dimensionali del modello"
			return -1
		end if
		ld_dim = double(fs_data)
		if ld_dim > ld_dim_max or ld_dim < ld_dim_min then
			g_mb.messagebox("Dimensioni","Attenzione! Dimensione fuori dei limiti consentiti per il modello", Information!)
		end if
*/
		w_configuratore.istr_riepilogo.dimensione_3 = double(fs_data)

		w_configuratore.wf_gest_mp_non_richieste(fdw_passo.getitemnumber(1, "rd_dimensione_1"),  fdw_passo.getitemnumber(1, "rd_dimensione_2"), dec(fs_data), fdw_passo.getitemnumber(1, "rd_dimensione_4"), w_configuratore.istr_riepilogo.alt_asta)
		
		uof_calcola_formula_inizio(  fdw_passo, 3)

	case "rd_dimensione_4"
		ls_cod_modello_manuale = fdw_passo.getitemstring(li_row, "rs_cod_modello")
		if isnull(ls_cod_modello_manuale) then
			fs_messaggio = "Inserire prima il codice del modello"
			return -1
		end if
		
		if uof_validazione_dimensione_singola( ls_cod_modello_manuale, "dim_z", dec(fs_data), ref ls_errore) < 0 then
			g_mb.messagebox("Dimensioni",ls_errore, Information!)
		end if		
/*
		select limite_max_dim_t, limite_min_dim_t
		into   :ld_dim_max, :ld_dim_min
		from   tab_flags_configuratore
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_modello = :ls_cod_modello_manuale;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca limiti dimensionali del modello"
			return -1
		end if
		ld_dim = double(fs_data)
		if ld_dim > ld_dim_max or ld_dim < ld_dim_min then
			g_mb.messagebox("Dimensioni","Attenzione! Dimensione fuori dei limiti consentiti per il modello", Information!)
		end if
*/
		w_configuratore.istr_riepilogo.dimensione_4 = double(fs_data)
		
		w_configuratore.wf_gest_mp_non_richieste(fdw_passo.getitemnumber(1, "rd_dimensione_1"),  fdw_passo.getitemnumber(1, "rd_dimensione_2"), fdw_passo.getitemnumber(1, "rd_dimensione_3"), dec(fs_data), w_configuratore.istr_riepilogo.alt_asta)

		uof_calcola_formula_inizio(  fdw_passo, 4)
	case "rd_dimensione_5"
		w_configuratore.istr_riepilogo.dimensione_5 = double(fs_data)
	
case "rd_dimensione_6"
		w_configuratore.istr_riepilogo.dimensione_6 = double(fs_data)
	
case "rd_dimensione_7"
		w_configuratore.istr_riepilogo.dimensione_7 = double(fs_data)
	
case "rd_dimensione_8"
		w_configuratore.istr_riepilogo.dimensione_8 = double(fs_data)
	
case "rd_dimensione_9"
		w_configuratore.istr_riepilogo.dimensione_9 = double(fs_data)
	
	
case "rs_flag_misura_luce_finita"
		w_configuratore.istr_riepilogo.flag_misura_luce_finita = fs_data
		if w_configuratore.istr_flags.flag_plisse = "S" then
			if w_configuratore.wf_calcoli_plisse(w_configuratore.istr_riepilogo.dimensione_1, w_configuratore.istr_riepilogo.dimensione_2, &
										w_configuratore.istr_riepilogo.flag_autoblocco, fs_data, w_configuratore.istr_riepilogo.cod_tessuto, &
										w_configuratore.istr_riepilogo.cod_non_a_magazzino, fs_messaggio) = -1 then
				return -1
			end if
		end if
	case "rn_quan_vendita"
		w_configuratore.istr_riepilogo.quan_vendita = double(fs_data)
		
	case "rs_flag_allegati"
		w_configuratore.istr_riepilogo.flag_allegati = fs_data
		
	case "rn_prog_des_modello"
		
		ls_cod_modello_manuale = fdw_passo.getitemstring(li_row, "rs_cod_modello")
		ll_prog_des_prodotto = long(fs_data)
		setnull(ls_des_modello)
		setnull(ls_des_prodotto)
		
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_modello_manuale;
				 
		select des_modello
		into   :ls_des_modello
		from   tab_modelli_descrizioni
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_modello = :ls_cod_modello_manuale and
				 progressivo = :ll_prog_des_prodotto;
				 
		if isnull(ls_des_modello) then
			fdw_passo.setitem(li_row, "rs_des_prodotto", ls_des_prodotto)
			str_conf_prodotto.des_prodotto = ls_des_prodotto
		else
			fdw_passo.setitem(li_row, "rs_des_prodotto", left(ls_des_prodotto + " " + ls_des_modello, 40))
			str_conf_prodotto.des_prodotto = left(ls_des_prodotto + " " + ls_des_modello, 40)
		end if			
		
	case "rs_des_prodotto"
		
		str_conf_prodotto.des_prodotto = left(fs_data, 40)
		
end choose

return 0
end function

public function integer uof_itemchanged_vern_lastra (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio);//	Funzione che gestisce gli eventi itemchanged delle varie DataWindow in maniera da slegare
//		la dipendenza tra Controllo Dw e Oggetto Dw
//
// nome: wf_item_changed
// tipo: none
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//
//									fdw_passo			 DataWindow		valore
//									fs_col_name			 String			valore
//									fs_data				 String			valore
//									fs_messaggio		 String			referenza
//
//		Creata il 31-08-98 
//		Autore enrico menegotto
//    modificata il 31/08/1998

string ls_cod_tessuto_manuale, ls_cod_verniciatura_manuale, ls_flag_fuori_campionario, ls_flag_addizionale
string ls_cod_veloce, ls_cod_tessuto, ls_cod_tipo_stampo_manuale, ls_cod_tipo_stampo, ls_cod_comando_manuale, &
		 ls_cod_modello_manuale, ls_cod_comando, ls_des_dim_x, ls_des_dim_y, ls_des_dim_z, ls_des_dim_t, &
       ls_cod_tessuto_mant, ls_cod_colore_mant, ls_colore_passamaneria, ls_colore_cordolo 		 
integer li_row
long   ll_cont
double ld_dim_hp
li_row = 1

choose case fs_col_name

//----------------------------------- d_ext_vern_lastra	------------------------------------

	case "rs_cod_verniciatura"
		if isnull(fs_data) then
			fdw_passo.setitem(li_row, "rs_cod_verniciatura", "")
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale", "")
			ls_cod_veloce = "?"
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select count(*)
			into   :ll_cont
			from   tab_modelli_verniciature
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_modello = :is_cod_modello and
					 cod_verniciatura = :fs_data and
					 flag_blocco = 'N';
			if ll_cont <> 1 then
				fs_messaggio = "Codice Verniciatura non Valido per questo modello"
				return -1
			end if
			
			select cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale
			into  :ls_cod_veloce,
					:ls_flag_fuori_campionario,
					:ls_flag_addizionale
			from  tab_verniciatura
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_verniciatura = :fs_data;
			if SQLCA.sqlcode <> 0 then
				fs_messaggio = "Errore durante ricerca codice verniciatura in tabella verniciature.~r~nDettaglio errore: " + sqlca.sqlerrtext
				return -1
			end if
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale", fs_data)
		end if
		w_configuratore.istr_riepilogo.cod_verniciatura = fs_data
		fdw_passo.setitem(li_row, "rs_vern_fuori_campionario", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_vern_addizionale", ls_flag_addizionale)
		if ls_flag_fuori_campionario = "S" then
			fdw_passo.object.rs_des_vernice_fc_t.color = 8388608
			fdw_passo.object.rs_des_vernice_fc.background.color = 16777215
			fdw_passo.object.rs_des_vernice_fc.protect = 0
		else
			fdw_passo.object.rs_des_vernice_fc_t.color = 8421504
			fdw_passo.object.rs_des_vernice_fc.background.color = 79741120
			fdw_passo.object.rs_des_vernice_fc.protect = 1
		end if

		w_configuratore.wf_gest_mp_non_richieste(w_configuratore.istr_riepilogo.dimensione_1, w_configuratore.istr_riepilogo.dimensione_2, w_configuratore.istr_riepilogo.dimensione_3, w_configuratore.istr_riepilogo.dimensione_4, w_configuratore.istr_riepilogo.alt_asta)
	
	case "rs_cod_verniciatura_manuale"
		if isnull(fs_data) or fs_data = "" then
			fdw_passo.setitem(li_row, "rs_cod_verniciatura", "")
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale", "")
			ls_cod_veloce = "?"
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select count(*)
			into   :ll_cont
			from   tab_modelli_verniciature
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_modello = :is_cod_modello and
					 cod_verniciatura = :fs_data and
					 flag_blocco = 'N';
			if ll_cont <> 1 then
				fs_messaggio = "Codice Verniciatura non Valido per questo modello"
				return -1
			end if
			
			select cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale
			into  :ls_cod_veloce,
					:ls_flag_fuori_campionario,
					:ls_flag_addizionale
			from  tab_verniciatura
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_verniciatura = :fs_data;
			if SQLCA.sqlcode <> 0 then
				fs_messaggio = "Errore durante ricerca codice verniciatura in tabella verniciature.~r~nDettaglio errore: " + sqlca.sqlerrtext
				return -1
			end if
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale", fs_data)
		end if

		w_configuratore.istr_riepilogo.cod_verniciatura = fs_data
		fdw_passo.setitem(li_row, "rs_vern_fuori_campionario", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_vern_addizionale", ls_flag_addizionale)
		if ls_flag_fuori_campionario = "S" then
			fdw_passo.object.rs_des_vernice_fc_t.color = 8388608
			fdw_passo.object.rs_des_vernice_fc.background.color = 16777215
			fdw_passo.object.rs_des_vernice_fc.protect = 0
		else
			fdw_passo.object.rs_des_vernice_fc_t.color = 8421504
			fdw_passo.object.rs_des_vernice_fc.background.color = 79741120
			fdw_passo.object.rs_des_vernice_fc.protect = 1
		end if
//		str_conf_prodotto.cod_prodotto_finito = Replace(str_conf_prodotto.cod_prodotto_finito, 6, 1, ls_cod_veloce)

	case "rs_cod_colore_lastra"
		if fs_data = "" or isnull(fs_data) then
			ls_cod_veloce = "?"
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale
			 into :ls_cod_veloce,
					:ls_flag_fuori_campionario,
					:ls_flag_addizionale
			 from tab_colori_lastre
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_colore_lastra = :fs_data and
					cod_modello = :is_cod_modello;
	
			if SQLCA.sqlcode = 100 then
				fs_messaggio = "Codice Colore Lastra non Valido"
				return -1
			elseif SQLCA.sqlcode = -1 then
				fs_messaggio = "Errore durante l'Estrazione"
				return -1
			end if
		end if

		w_configuratore.istr_riepilogo.cod_colore_lastra = fs_data
		fdw_passo.setitem(li_row, "rs_col_fuori_campionario", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_col_addizionale", ls_flag_addizionale)
		fdw_passo.setitem(li_row, "rs_cod_colore_lastra_manuale", fs_data)
		
	case "rs_cod_colore_lastra_manuale"
		if fs_data = "" or isnull(fs_data) then
			ls_cod_veloce = "?"
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale
			 into :ls_cod_veloce,
					:ls_flag_fuori_campionario,
					:ls_flag_addizionale
			 from tab_colori_lastre
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_colore_lastra = :fs_data and
					cod_modello = :is_cod_modello;
	
			if SQLCA.sqlcode = 100 then
				fs_messaggio = "Codice Colore Lastra non Valido"
				return -1
			elseif SQLCA.sqlcode = -1 then
				fs_messaggio = "Errore durante l'Estrazione"
				return -1
			end if
		end if
		
		w_configuratore.istr_riepilogo.cod_colore_lastra = fs_data
		fdw_passo.setitem(li_row, "rs_col_fuori_campionario", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_col_addizionale", ls_flag_addizionale)
		fdw_passo.setitem(li_row, "rs_cod_colore_lastra", fs_data)

	case "rs_cod_tipo_lastra"
		if fs_data = "" or isnull(fs_data) then
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select flag_fuori_campionario,
					 flag_addizionale
			 into :ls_flag_fuori_campionario,
					:ls_flag_addizionale
			 from tab_tipi_lastre
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_lastra = :fs_data  and
					cod_modello = :is_cod_modello;
	
			if SQLCA.sqlcode = 100 then
				fs_messaggio = "Codice Tipo Lastra non Valido"
				return -1
			elseif SQLCA.sqlcode = -1 then
				fs_messaggio = "Errore durante l'Estrazione"
				return -1
			end if
		end if

		w_configuratore.istr_riepilogo.cod_tipo_lastra = fs_data
		fdw_passo.setitem(li_row, "rs_tipo_fuori_campionario", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_tipo_addizionale", ls_flag_addizionale)
		fdw_passo.setitem(li_row, "rs_cod_tipo_lastra_manuale", fs_data)
		
	case "rs_cod_tipo_lastra_manuale"
		if fs_data = "" or isnull(fs_data) then
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select flag_fuori_campionario,
					 flag_addizionale
			 into :ls_flag_fuori_campionario,
					:ls_flag_addizionale
			 from tab_tipi_lastre
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_lastra = :fs_data  and
					cod_modello = :is_cod_modello;
	
			if SQLCA.sqlcode = 100 then
				fs_messaggio = "Codice Tipo Lastra non Valido"
				return -1
			elseif SQLCA.sqlcode = -1 then
				fs_messaggio = "Errore durante l'Estrazione"
				return -1
			end if
		end if
		
		w_configuratore.istr_riepilogo.cod_tipo_lastra = fs_data
		fdw_passo.setitem(li_row, "rs_tipo_fuori_campionario", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_tipo_addizionale", ls_flag_addizionale)
		fdw_passo.setitem(li_row, "rs_cod_tipo_lastra", fs_data)

	case "rs_spessore_lastra"
		w_configuratore.istr_riepilogo.spes_lastra = fs_data
	case "rs_des_vernice_fc"
		w_configuratore.istr_riepilogo.des_vernice_fc = fs_data
	case "rs_vern_addizionale"
		w_configuratore.istr_riepilogo.flag_addizionale_vernic = fs_data
end choose

RETURN 0
end function

public function integer uof_itemchanged_comandi (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio);//	Funzione che gestisce gli eventi itemchanged delle varie DataWindow in maniera da slegare
//		la dipendenza tra Controllo Dw e Oggetto Dw
//
// nome: wf_item_changed
// tipo: none
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//
//									fdw_passo			 DataWindow		valore
//									fs_col_name			 String			valore
//									fs_data				 String			valore
//									fs_messaggio		 String			referenza
//
//		Creata il 31-08-98 
//		Autore Enrico Menegotto
//    modificata il 31/08/1998
//    modificata il 25/07/2000 per secondo comando

string ls_cod_tessuto_manuale, ls_cod_verniciatura_manuale, ls_flag_fuori_campionario, ls_flag_addizionale
string ls_cod_veloce, ls_cod_tessuto, ls_cod_tipo_stampo_manuale, ls_cod_tipo_stampo, ls_cod_comando_manuale, &
		 ls_cod_modello_manuale, ls_cod_comando, ls_des_dim_x, ls_des_dim_y, ls_des_dim_z, ls_des_dim_t, &
       ls_cod_tessuto_mant, ls_cod_colore_mant, ls_colore_passamaneria, ls_colore_cordolo 		 
integer li_row
double ld_dim_hp
li_row = 1

choose case fs_col_name

//--------------------------------- d_ext_comandi ------------------------------------------

	// ------------------------------- PRIMO COMANDO -----------------------------------------
	case "rs_cod_comando"
		if fs_data = "" or isnull(fs_data) then
			ls_cod_veloce = "??"
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale
			 into :ls_cod_veloce,
					:ls_flag_fuori_campionario,
					:ls_flag_addizionale
			 from tab_comandi
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_comando = :fs_data;
	
			if SQLCA.sqlcode = 100 then
				fs_messaggio = "Codice comando non Valido"
				return -1
			elseif SQLCA.sqlcode = -1 then
				fs_messaggio = "Errore durante l'Estrazione"
				return -1
			end if
		end if

		f_PO_LoadDDDW_DW(fdw_passo,"rs_pos_comando",sqlca,&
							  "tab_comandi_posizioni","posizione","des_posizione",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_comando = '" + fs_data + "'")
		w_configuratore.istr_riepilogo.cod_comando = fs_data
		fdw_passo.setitem(li_row, "rs_com_flag_fuori_campionario", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_com_flag_addizionale", ls_flag_addizionale)
		fdw_passo.setitem(li_row, "rs_cod_comando_manuale", fs_data)
//		str_conf_prodotto.cod_prodotto_finito = Replace(str_conf_prodotto.cod_prodotto_finito, 7, 2, ls_cod_veloce)
		
	case "rs_cod_comando_manuale"
		if fs_data = "" or isnull(fs_data) then
			ls_cod_veloce = "??"
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale
			 into :ls_cod_veloce,
					:ls_flag_fuori_campionario,
					:ls_flag_addizionale
			 from tab_comandi
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_comando = :fs_data;
	
			if SQLCA.sqlcode = 100 then
				fs_messaggio = "Codice comando non Valido"
				return -1
			elseif SQLCA.sqlcode = -1 then
				fs_messaggio = "Errore durante l'Estrazione"
				return -1
			end if
		end if

		f_PO_LoadDDDW_DW(fdw_passo,"rs_pos_comando",sqlca,&
							  "tab_comandi_posizioni","posizione","des_posizione",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_comando = '" + fs_data + "'")
		w_configuratore.istr_riepilogo.cod_comando = fs_data
		fdw_passo.setitem(li_row, "rs_com_flag_fuori_campionario", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_com_flag_addizionale", ls_flag_addizionale)
		fdw_passo.setitem(li_row, "rs_cod_comando", fs_data)
//		str_conf_prodotto.cod_prodotto_finito = Replace(str_conf_prodotto.cod_prodotto_finito, 7, 2, ls_cod_veloce)

	case "rs_pos_comando"
		w_configuratore.istr_riepilogo.pos_comando = fs_data
	case "rs_com_flag_addizionale"
		w_configuratore.istr_riepilogo.flag_addizionale_comando_1 = fs_data
	case "rn_alt_asta"
		w_configuratore.istr_riepilogo.alt_asta = double(fs_data)
		w_configuratore.wf_gest_mp_non_richieste(w_configuratore.istr_riepilogo.dimensione_1, w_configuratore.istr_riepilogo.dimensione_2, w_configuratore.istr_riepilogo.dimensione_3,w_configuratore.istr_riepilogo.dimensione_4,w_configuratore.istr_riepilogo.alt_asta)
	case "nota_comando_1"
		w_configuratore.istr_riepilogo.des_comando_1 = fs_data
		
	// ------------------------------- SECONDO COMANDO -----------------------------------------------------------------
	case "rs_cod_comando_2"
		if fs_data = "" or isnull(fs_data) then
			ls_cod_veloce = "??"
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale
			 into :ls_cod_veloce,
					:ls_flag_fuori_campionario,
					:ls_flag_addizionale
			 from tab_comandi
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_comando = :fs_data;
	
			if SQLCA.sqlcode = 100 then
				fs_messaggio = "Codice comando secondario non Valido"
				return -1
			elseif SQLCA.sqlcode = -1 then
				fs_messaggio = "Errore durante l'Estrazione"
				return -1
			end if
		end if

		w_configuratore.istr_riepilogo.cod_comando_2 = fs_data
		fdw_passo.setitem(li_row, "rs_com_2_flag_fuori_campionario", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_com_2_flag_addizionale", ls_flag_addizionale)
		fdw_passo.setitem(li_row, "rs_cod_comando_2_manuale", fs_data)
		
	case "rs_cod_comando_2_manuale"
		if fs_data = "" or isnull(fs_data) then
			ls_cod_veloce = "??"
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale
			 into :ls_cod_veloce,
					:ls_flag_fuori_campionario,
					:ls_flag_addizionale
			 from tab_comandi
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_comando = :fs_data;
	
			if SQLCA.sqlcode = 100 then
				fs_messaggio = "Codice comando secondario non Valido"
				return -1
			elseif SQLCA.sqlcode = -1 then
				fs_messaggio = "Errore durante l'Estrazione"
				return -1
			end if
		end if

		w_configuratore.istr_riepilogo.cod_comando = fs_data
		fdw_passo.setitem(li_row, "rs_com_2_flag_fuori_campionario", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_com_2_flag_addizionale", ls_flag_addizionale)
		fdw_passo.setitem(li_row, "rs_cod_comando_2", fs_data)
	case "rs_com_2_flag_addizionale"
		w_configuratore.istr_riepilogo.flag_addizionale_comando_2 = fs_data
	case "nota_comando_2"
		w_configuratore.istr_riepilogo.des_comando_2 = fs_data
end choose	
return 0
end function

public function integer uof_itemchanged_nota_dettaglio (datawindow fdw_passo, string fs_col_name, string fs_data, string fs_messaggio);//	Funzione che gestisce gli eventi itemchanged delle varie DataWindow in maniera da slegare
//		la dipendenza tra Controllo Dw e Oggetto Dw
//
// nome: wf_item_changed
// tipo: none
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//
//									fdw_passo			 DataWindow		valore
//									fs_col_name			 String			valore
//									fs_data				 String			valore
//									fs_messaggio		 String			referenza
//
//		Creata il 18/1/2002 per nota di dettaglio del prodotto finito.



choose case fs_col_name

	case "nota_dettaglio"
		if len(fs_data) > 254 then return 1
		if fs_data = "" or isnull(fs_data) then
			w_configuratore.istr_riepilogo.nota_dettaglio = ""
		else
			w_configuratore.istr_riepilogo.nota_dettaglio = fs_data
		end if
		
	
	case "flag_azzera_dt"
		if fs_data = "" or isnull(fs_data) then
			w_configuratore.istr_riepilogo.flag_azzera_dt = "N"
		else
			w_configuratore.istr_riepilogo.flag_azzera_dt = fs_data
		end if

end choose	
return 0
end function

public function integer uof_itemchanged_tessuto (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio);/* ---------------------------------------------------------------------------------------------
	Funzione che gestisce gli eventi itemchanged delle varie DataWindow in maniera da slegare
		la dipendenza tra Controllo Dw e Oggetto Dw

 nome: wf_item_changed
 tipo: none
  
	Variabili passate: 		nome					 tipo				passaggio per			commento

									fdw_passo			 DataWindow		valore
									fs_col_name			 String			valore
									fs_data				 String			valore
									fs_messaggio		 String			referenza

		Creata il 31-08-98 
    modificata il 31/08/1998

Valore di return della funzione:
-2 = ritorno con richiesta di conferma
-1 = errore; nessuna richiesta
0  = tutto OK
1  = Errore logico; impossibile spostarsi dal campo corrente

 ---------------------------------------------------------------------------------------------*/

boolean lb_test
string ls_cod_tessuto_manuale, ls_cod_verniciatura_manuale, ls_flag_fuori_campionario, ls_flag_addizionale, &
       ls_cod_veloce, ls_cod_tessuto, ls_cod_tipo_stampo_manuale, ls_cod_tipo_stampo, ls_cod_comando_manuale, &
		 ls_cod_modello_manuale, ls_cod_comando, ls_des_dim_x, ls_des_dim_y, ls_des_dim_z, ls_des_dim_t, ls_null, &
       ls_cod_tessuto_mant, ls_cod_colore_mant, ls_colore_passamaneria, ls_colore_cordolo, ls_des_colore, ls_colore_tessuto, &
		 ls_cod_tipo_campionario, ls_cod_tipo_anagrafica, ls_flag_tipo_unione
integer li_row
long   ll_cont, ll_cont_campionario
double ld_dim_hp
datetime ldt_oggi

li_row = 1
setnull(ls_null)
ldt_oggi = datetime(today(), 00:00:00)

choose case fs_col_name
	case "rs_cod_non_a_magazzino"					// colore tessuto
		if isnull(fs_data) then							// se null azzero tutti i dati dei tessuti
			w_configuratore.istr_riepilogo.cod_non_a_magazzino = ""
			w_configuratore.istr_riepilogo.cod_tessuto = ""
			fdw_passo.setitem(li_row, "rs_cod_tessuto", ls_null)
			fdw_passo.setitem(li_row, "rs_cod_tessuto_manuale", ls_null)
			fdw_passo.setitem(li_row, "rs_tess_fuori_campionario", "N")
			fdw_passo.setitem(li_row, "rs_tess_addizionale", "N")
			fdw_passo.setitem(li_row, "rs_cod_mant_non_a_magazzino", ls_null)
			fdw_passo.setitem(li_row, "rs_cod_tessuto_mant_manuale", ls_null)
			fdw_passo.setitem(li_row, "rs_cod_tessuto_mant", ls_null)
			fdw_passo.setitem(li_row, "rs_colore_cordolo", ls_null)
			fdw_passo.setitem(li_row, "rs_colore_passamaneria", ls_null)
			return 0
		end if		
		
		// verifico esistenza colore tessuto
		
		select des_colore_tessuto,
				flag_tipo_unione
		into   :ls_des_colore,
				:ls_flag_tipo_unione
		from   tab_colori_tessuti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_colore_tessuto = :fs_data and
				 ((flag_blocco = 'N') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
			 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca colore tessuto; verificare che il colore digitato sia stato codificato"
			return -1
		end if
		
		// verifico se tale colore esiste in più tipi tessuti 
		select count(cod_tessuto)
		into   :ll_cont
		from   tab_tessuti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_non_a_magazzino = :fs_data and
				 ((flag_blocco = 'N') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
		lb_test = false
		choose case ll_cont
			case 0
				fs_messaggio = "Colore tessuto non codificato nella tabella tessuti"
				return -1
			case is > 1 
				g_mb.messagebox("Configuratore Prodotto","Esistono più codici tessuto con questo colore")
				f_PO_LoadDDDW_DW(fdw_passo,"rs_cod_tessuto",sqlca,&
									  "tab_tessuti","cod_tessuto","cod_non_a_magazzino",&
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_non_a_magazzino = '" + fs_data + "' and " + &
					              "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")
				lb_test = false
			case 1 
				f_PO_LoadDDDW_DW(fdw_passo,"rs_cod_tessuto",sqlca,&
									  "tab_tessuti","cod_tessuto", "cod_non_a_magazzino", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_non_a_magazzino = '" + fs_data + "' and " + &
					              "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")
				lb_test = true
		end choose		

		fdw_passo.setitem(li_row, "rs_cod_mant_non_a_magazzino", ls_null)
		fdw_passo.setitem(li_row, "rs_cod_tessuto_mant_manuale", ls_null)
		fdw_passo.setitem(li_row, "rs_cod_tessuto_mant", ls_null)
		fdw_passo.setitem(li_row, "rs_colore_cordolo", ls_null)
		fdw_passo.setitem(li_row, "rs_colore_passamaneria", ls_null)
		if lb_test then
			select cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale,
					 cod_tessuto,
					 cod_tessuto_mantovana,   
					 cod_colore_mantovana,   
					 colore_passamaneria,   
					 colore_cordolo,
					 cod_tipo_campionario
			 into :ls_cod_veloce,
					:ls_flag_fuori_campionario,
					:ls_flag_addizionale,
					:ls_cod_tessuto,
					:ls_cod_tessuto_mant,   
					:ls_cod_colore_mant,   
					:ls_colore_passamaneria,   
					:ls_colore_cordolo,
					:ls_cod_tipo_campionario
			 from tab_tessuti
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_non_a_magazzino = :fs_data and
				   ((flag_blocco = 'N') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
	
			if SQLCA.sqlcode = 100 then
				fs_messaggio = "Codice Tessuto non Valido"
				return -1
			elseif SQLCA.sqlcode <> 0 then
				fs_messaggio = "Errore durante la ricerca proprietà tessuto in tabella tessuti"
				return -1
			end if
			
			w_configuratore.istr_riepilogo.cod_non_a_magazzino = fs_data
			w_configuratore.istr_riepilogo.cod_tessuto = ls_cod_tessuto
			w_configuratore.istr_riepilogo.flag_tipo_unione = ls_flag_tipo_unione
			fdw_passo.setitem(li_row, "rs_cod_tessuto", ls_cod_tessuto)
			fdw_passo.setitem(li_row, "rs_cod_tessuto_manuale", ls_cod_tessuto)
			fdw_passo.setitem(li_row, "rs_tess_fuori_campionario", ls_flag_fuori_campionario)
			fdw_passo.setitem(li_row, "rs_tess_addizionale", ls_flag_addizionale)
			fdw_passo.setitem(li_row, "rs_flag_tipo_unione", ls_flag_tipo_unione)

			if w_configuratore.istr_flags.flag_tessuto_mantovana = "S" and fdw_passo.getitemstring(li_row,"rs_flag_mantovana")="S" then
				fdw_passo.setitem(li_row, "rs_cod_mant_non_a_magazzino", ls_cod_colore_mant)
				fdw_passo.setitem(li_row, "rs_cod_tessuto_mant_manuale", ls_cod_tessuto_mant)
				fdw_passo.setitem(li_row, "rs_cod_tessuto_mant", ls_cod_tessuto_mant)
				if w_configuratore.istr_flags.flag_cordolo = "S" then fdw_passo.setitem(li_row, "rs_colore_cordolo", ls_colore_cordolo)
				if w_configuratore.istr_flags.flag_passamaneria = "S" then fdw_passo.setitem(li_row, "rs_colore_passamaneria", ls_colore_passamaneria)
			end if
			
			if w_configuratore.istr_flags.flag_plisse = "S" then
				if w_configuratore.wf_calcoli_plisse(w_configuratore.istr_riepilogo.dimensione_1, w_configuratore.istr_riepilogo.dimensione_2, w_configuratore.istr_riepilogo.flag_autoblocco, &
											w_configuratore.istr_riepilogo.flag_misura_luce_finita, w_configuratore.istr_riepilogo.cod_tessuto, &
											fs_data, fs_messaggio) = -1 then
					return -1
				end if
			end if
			
			// Specifica requisiti "clienti_atelier_2" EnMe 25/3/2008
			choose case uof_controlla_atelier(ls_cod_tipo_campionario, ref fs_messaggio)
				case -1
					return -1
				case -2
					return -2
			end choose
			
		end if	
		
		
	case "rs_cod_tessuto"
		if isnull(fs_data) then
			fdw_passo.setitem(li_row, "rs_cod_tessuto", "")
			fdw_passo.setitem(li_row, "rs_cod_tessuto_manuale", "")
		else
			fdw_passo.setitem(li_row, "rs_cod_tessuto_manuale", fs_data)
		end if
		w_configuratore.istr_riepilogo.cod_tessuto = fs_data
		ls_colore_tessuto = fdw_passo.getitemstring(li_row,"rs_cod_non_a_magazzino")
		
		if ls_colore_tessuto = "" or isnull(ls_colore_tessuto) then			// non c'è colore cerco default
		
			select cod_non_a_magazzino,
			       cod_tipo_campionario
			into   :ls_colore_tessuto,
			       :ls_cod_tipo_campionario
			from   tab_tessuti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tessuto = :fs_data and
					 flag_default = 'S';
			if sqlca.sqlcode >= 0 and sqlca.sqlcode <> 100 then			// trovato colore default
				fdw_passo.setitem(li_row, "rs_cod_non_a_magazzino", ls_colore_tessuto)
				w_configuratore.istr_riepilogo.cod_non_a_magazzino = ls_colore_tessuto
			else
				fdw_passo.setitem(li_row, "rs_cod_non_a_magazzino", "")
				w_configuratore.istr_riepilogo.cod_non_a_magazzino = ""
			end if
			
		else			// colore già impostato, ma cambio gruppo tessuto
			
			select cod_non_a_magazzino,
			       cod_tessuto_mantovana,   
					 cod_colore_mantovana,   
					 colore_passamaneria,   
					 colore_cordolo ,
			       cod_tipo_campionario
			into   :ls_colore_tessuto,
					 :ls_cod_tessuto_mant,   
					 :ls_cod_colore_mant,   
					 :ls_colore_passamaneria,   
					 :ls_colore_cordolo,
			       :ls_cod_tipo_campionario
			from   tab_tessuti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tessuto = :fs_data and
					 cod_non_a_magazzino = :ls_colore_tessuto;
					 
			if sqlca.sqlcode = 100 then
				fs_messaggio = "Il colore " + w_configuratore.istr_riepilogo.cod_non_a_magazzino + " non esiste per il gruppo tessuti " + fs_data + ": selezionare un colore valido!"
				fdw_passo.setitem(li_row, "rs_cod_non_a_magazzino", ls_null)
				w_configuratore.istr_riepilogo.cod_tessuto = fs_data
				return 1
				
			elseif sqlca.sqlcode = 0 then    /// cerco i tessuti abbinati
				fdw_passo.setitem(li_row, "rs_cod_mant_non_a_magazzino", ls_null)
				fdw_passo.setitem(li_row, "rs_cod_tessuto_mant_manuale", ls_null)
				fdw_passo.setitem(li_row, "rs_cod_tessuto_mant", ls_null)
				fdw_passo.setitem(li_row, "rs_colore_cordolo", ls_null)
				fdw_passo.setitem(li_row, "rs_colore_passamaneria", ls_null)
				
				if w_configuratore.istr_flags.flag_tessuto_mantovana = "S" and fdw_passo.getitemstring(li_row,"rs_flag_mantovana")="S" then
					fdw_passo.setitem(li_row, "rs_cod_mant_non_a_magazzino", ls_cod_colore_mant)
					fdw_passo.setitem(li_row, "rs_cod_tessuto_mant_manuale", ls_cod_tessuto_mant)
					fdw_passo.setitem(li_row, "rs_cod_tessuto_mant", ls_cod_tessuto_mant)
					if w_configuratore.istr_flags.flag_cordolo = "S" then fdw_passo.setitem(li_row, "rs_colore_cordolo", ls_colore_cordolo)
					if w_configuratore.istr_flags.flag_passamaneria = "S" then fdw_passo.setitem(li_row, "rs_colore_passamaneria", ls_colore_passamaneria)
				end if
				
			end if
		end if	
		
		// Specifica requisiti "clienti_atelier_2" EnMe 25/3/2008
		choose case uof_controlla_atelier(ls_cod_tipo_campionario, ref fs_messaggio)
			case -1
				return -1
			case -2
				return -2
		end choose
			
		

		if w_configuratore.istr_flags.flag_plisse = "S" then
			if w_configuratore.wf_calcoli_plisse(w_configuratore.istr_riepilogo.dimensione_1, w_configuratore.istr_riepilogo.dimensione_2, w_configuratore.istr_riepilogo.flag_autoblocco, &
										w_configuratore.istr_riepilogo.flag_misura_luce_finita, fs_data, &
										w_configuratore.istr_riepilogo.cod_non_a_magazzino, fs_messaggio) = -1 then
				return -1
			end if
		end if
	
	case "rs_cod_tessuto_manuale"
		if isnull(fs_data) or fs_data = "" then
			fdw_passo.setitem(li_row, "rs_cod_tessuto", "")
			w_configuratore.istr_riepilogo.cod_tessuto = ""
			return 1
		else
			select cod_tessuto
			 into :ls_cod_tessuto_manuale
			 from tab_tessuti
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_tessuto = :fs_data and
				   ((flag_blocco = 'N') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
			if SQLCA.sqlcode = 100 then
				fs_messaggio = "Codice Tessuto Manuale non Valido"
				return -1
			end if
			fdw_passo.setitem(li_row, "rs_cod_tessuto", fs_data)
		end if
		
		ls_colore_tessuto = fdw_passo.getitemstring(li_row,"rs_cod_non_a_magazzino")
		if ls_colore_tessuto = "" or isnull(ls_colore_tessuto) then
			select cod_non_a_magazzino,
			       cod_tipo_campionario
			into   :ls_colore_tessuto,
			       :ls_cod_tipo_campionario
			from   tab_tessuti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tessuto = :fs_data and
					 flag_default = 'S';
			if sqlca.sqlcode >= 0 and sqlca.sqlcode <> 100 then
				fdw_passo.setitem(li_row, "rs_cod_non_a_magazzino", ls_colore_tessuto)
				w_configuratore.istr_riepilogo.cod_non_a_magazzino = ls_colore_tessuto
			else
				fdw_passo.setitem(li_row, "rs_cod_non_a_magazzino", "")
				w_configuratore.istr_riepilogo.cod_non_a_magazzino = ""
			end if
			f_PO_LoadDDDW_DW(fdw_passo,"rs_cod_tessuto",sqlca, &
			                 "tab_tessuti","cod_tessuto", "cod_non_a_magazzino", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tessuto = '" + fs_data+ "' and " + &
					           "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")
		else					
			// verifico che il tessuto esista con il colore selezionato
			select cod_non_a_magazzino,
			       cod_tipo_campionario
			into   :ls_colore_tessuto,
			       :ls_cod_tipo_campionario
			from   tab_tessuti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tessuto = :fs_data and
					 cod_non_a_magazzino = :ls_colore_tessuto;
			if sqlca.sqlcode = 100 or sqlca.sqlcode < 0 then
				fs_messaggio = "Il colore " + w_configuratore.istr_riepilogo.cod_non_a_magazzino + " non esiste per il gruppo tessuti " + fs_data + ": selezionare un colore valido!"
				fdw_passo.setitem(li_row, "rs_cod_non_a_magazzino", ls_null)
				w_configuratore.istr_riepilogo.cod_tessuto = fs_data
				return -1						// questo tessuto non esiste con questo colore
			else
				fdw_passo.setitem(li_row, "rs_cod_tessuto", fs_data)
			end if	
		end if		
		
		// Specifica requisiti "clienti_atelier_2" EnMe 25/3/2008
		choose case uof_controlla_atelier(ls_cod_tipo_campionario, ref fs_messaggio)
			case -1
				return -1
			case -2
				return -2
		end choose
			
		w_configuratore.istr_riepilogo.cod_tessuto = fs_data
		if w_configuratore.istr_flags.flag_plisse = "S" then
			if w_configuratore.wf_calcoli_plisse(w_configuratore.istr_riepilogo.dimensione_1, w_configuratore.istr_riepilogo.dimensione_2, w_configuratore.istr_riepilogo.flag_autoblocco, &
										w_configuratore.istr_riepilogo.flag_misura_luce_finita, fs_data, &
										w_configuratore.istr_riepilogo.cod_non_a_magazzino, fs_messaggio) = -1 then
				return -1
			end if
		end if

	case "rs_cod_mant_non_a_magazzino"
		if isnull(fs_data) or fs_data = "" then
			fdw_passo.setitem(li_row, "rs_cod_mant_non_a_magazzino", ls_null)
			fdw_passo.setitem(li_row, "rs_cod_tessuto_mant_manuale", ls_null)
			fdw_passo.setitem(li_row, "rs_cod_tessuto_mant", ls_null)
			return 1
		end if
		
		// verifico esistenza colore tessuto
		select des_colore_tessuto
		into   :ls_des_colore
		from   tab_colori_tessuti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_colore_tessuto = :fs_data  and
				 ((flag_blocco = 'N') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca colore tessuto mantovana; verificare che il colore digitato sia stato codificato"
			return -1
		end if
		
		// verifico se tale colore esiste in più tipi tessuti 
		select count(cod_tessuto)
		into   :ll_cont
		from   tab_tessuti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_non_a_magazzino = :fs_data  and
				 ((flag_blocco = 'N') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
		lb_test = false
		choose case ll_cont
			case 0
				fs_messaggio = "Colore tessuto mantovana non codificato nella tabella tessuti"
				return -1
			case is > 1 
				g_mb.messagebox("Configuratore Prodotto","Esistono più codici tessuto con questo colore")
				f_PO_LoadDDDW_DW(fdw_passo,"rs_cod_tessuto_mant",sqlca,&
									  "tab_tessuti","cod_tessuto","cod_non_a_magazzino",&
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_non_a_magazzino = '" + fs_data + "' and " + &
					              "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")
				w_configuratore.istr_riepilogo.cod_tessuto_mant = ls_null
				fdw_passo.setitem(li_row, "rs_cod_tessuto_mant", ls_null)
				fdw_passo.setitem(li_row, "rs_cod_tessuto_mant_manuale", ls_null)
				if w_configuratore.istr_flags.flag_cordolo = "S" then fdw_passo.setitem(li_row, "rs_colore_cordolo", ls_null)
				if w_configuratore.istr_flags.flag_passamaneria = "S" then fdw_passo.setitem(li_row, "rs_colore_passamaneria", ls_null)
				lb_test = false
			case 1 
				f_PO_LoadDDDW_DW(fdw_passo,"rs_cod_tessuto_mant",sqlca,&
									  "tab_tessuti","cod_tessuto", "cod_non_a_magazzino", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_non_a_magazzino = '" + fs_data+ "' and " + &
					              "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")
				lb_test = true
		end choose		

		if lb_test then
			select cod_tessuto,
			       colore_passamaneria,   
					 colore_cordolo,
					 cod_tipo_campionario
			 into :ls_cod_tessuto,
			 		:ls_colore_passamaneria,   
					:ls_colore_cordolo ,
					:ls_cod_tipo_campionario
			 from tab_tessuti
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_non_a_magazzino = :fs_data and
				   ((flag_blocco = 'N') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
			if SQLCA.sqlcode = 100 then
				fs_messaggio = "Codice Tessuto Mantovana non Valido"
				return -1
			elseif SQLCA.sqlcode <> 0 then
				fs_messaggio = "Errore durante la ricerca proprietà tessuto in tabella tessuti"
				return -1
			end if
	
			w_configuratore.istr_riepilogo.cod_mant_non_a_magazzino = fs_data
			w_configuratore.istr_riepilogo.cod_tessuto_mant = ls_cod_tessuto
			fdw_passo.setitem(li_row, "rs_cod_tessuto_mant", ls_cod_tessuto)
			fdw_passo.setitem(li_row, "rs_cod_tessuto_mant_manuale", ls_cod_tessuto)
			
			if w_configuratore.istr_flags.flag_cordolo = "S" then fdw_passo.setitem(li_row, "rs_colore_cordolo", ls_colore_cordolo)
			if w_configuratore.istr_flags.flag_passamaneria = "S" then fdw_passo.setitem(li_row, "rs_colore_passamaneria", ls_colore_passamaneria)
			
			// Specifica requisiti "clienti_atelier_2" EnMe 25/3/2008
			choose case uof_controlla_atelier(ls_cod_tipo_campionario, ref fs_messaggio)
				case -1
					return -1
				case -2
					return -2
			end choose
			
		end if		
		
	case "rs_cod_tessuto_mant"
		lb_test = false
		if isnull(fs_data) then
			fdw_passo.setitem(li_row, "rs_cod_tessuto_mant", ls_null)
			fdw_passo.setitem(li_row, "rs_cod_tessuto_mant_manuale", ls_null)
		else
			fdw_passo.setitem(li_row, "rs_cod_tessuto_mant_manuale", fs_data)
		end if
		w_configuratore.istr_riepilogo.cod_tessuto_mant = fs_data
		ls_colore_tessuto = fdw_passo.getitemstring(li_row,"rs_cod_mant_non_a_magazzino")
		if ls_colore_tessuto = "" or isnull(ls_colore_tessuto) then			// non c'è colore cerco default
			select cod_non_a_magazzino,
					 colore_passamaneria,   
					 colore_cordolo,
					 cod_tipo_campionario
			into   :ls_colore_tessuto,
					 :ls_colore_passamaneria,   
					 :ls_colore_cordolo,
					 :ls_cod_tipo_campionario
			from   tab_tessuti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tessuto = :fs_data and
					 flag_default = 'S' and
				    ((flag_blocco = 'N') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
			if sqlca.sqlcode >= 0 and sqlca.sqlcode <> 100 then			// trovato colore default
				fdw_passo.setitem(li_row, "rs_cod_mant_non_a_magazzino", ls_colore_tessuto)
				w_configuratore.istr_riepilogo.cod_mant_non_a_magazzino = ls_colore_tessuto
				if w_configuratore.istr_flags.flag_cordolo = "S" then fdw_passo.setitem(li_row, "rs_colore_cordolo", ls_colore_cordolo)
				if w_configuratore.istr_flags.flag_passamaneria = "S" then fdw_passo.setitem(li_row, "rs_colore_passamaneria", ls_colore_passamaneria)
			else
				fdw_passo.setitem(li_row, "rs_cod_mant_non_a_magazzino", ls_null)
				w_configuratore.istr_riepilogo.cod_non_a_magazzino = ""
			end if
		else			// colore già impostato, ma cambio gruppo tessuto
			select cod_non_a_magazzino,
					 colore_passamaneria,   
					 colore_cordolo,
					 cod_tipo_campionario
			into   :ls_colore_tessuto,
					 :ls_colore_passamaneria,   
					 :ls_colore_cordolo,
					 :ls_cod_tipo_campionario
			from   tab_tessuti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tessuto = :fs_data and
					 cod_non_a_magazzino = :ls_colore_tessuto  and
				    ((flag_blocco = 'N') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
			if sqlca.sqlcode = 100 then
				fs_messaggio = "Il colore " + w_configuratore.istr_riepilogo.cod_mant_non_a_magazzino + " non esiste per il gruppo tessuti " + fs_data + ": selezionare un colore valido!"
				fdw_passo.setitem(li_row, "rs_cod_mant_non_a_magazzino", ls_null)
				w_configuratore.istr_riepilogo.cod_tessuto_mant = fs_data
				return 0
			end if
			if w_configuratore.istr_flags.flag_cordolo = "S" then fdw_passo.setitem(li_row, "rs_colore_cordolo", ls_colore_cordolo)
			if w_configuratore.istr_flags.flag_passamaneria = "S" then fdw_passo.setitem(li_row, "rs_colore_passamaneria", ls_colore_passamaneria)
		end if	
		
		// Specifica requisiti "clienti_atelier_2" EnMe 25/3/2008
		choose case uof_controlla_atelier(ls_cod_tipo_campionario, ref fs_messaggio)
			case -1
				return -1
			case -2
				return -2
		end choose
			
		
		
	case "rs_cod_tessuto_mant_manuale"
		lb_test = false
		if isnull(fs_data) or fs_data = "" then
			fdw_passo.setitem(li_row, "rs_cod_tessuto_mant", "")
			w_configuratore.istr_riepilogo.cod_tessuto_mant = ""
			return 1
		else
			select cod_tessuto,
			       cod_tipo_campionario
			 into :ls_cod_tessuto_manuale,
			      :ls_cod_tipo_campionario
			 from tab_tessuti
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_tessuto = :fs_data and
  				   ((flag_blocco = 'N') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
			if SQLCA.sqlcode = 100 then
				fs_messaggio = "Codice Tessuto Manuale non Valido"
				return 1
			end if
			fdw_passo.setitem(li_row, "rs_cod_tessuto_mant", fs_data)
		end if
		
		ls_colore_tessuto = fdw_passo.getitemstring(li_row,"rs_cod_mant_non_a_magazzino")
		if ls_colore_tessuto = "" or isnull(ls_colore_tessuto) then
			select cod_non_a_magazzino,
					 colore_passamaneria,   
					 colore_cordolo,
					 cod_tipo_campionario
			into   :ls_colore_tessuto,
					 :ls_colore_passamaneria,   
					 :ls_colore_cordolo ,
					 :ls_cod_tipo_campionario
			from   tab_tessuti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tessuto = :fs_data and
					 flag_default = 'S'  and
				    ((flag_blocco = 'N') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
			if sqlca.sqlcode >= 0 and sqlca.sqlcode <> 100 then
				fdw_passo.setitem(li_row, "rs_cod_mant_non_a_magazzino", ls_colore_tessuto)
				w_configuratore.istr_riepilogo.cod_mant_non_a_magazzino = ls_colore_tessuto
				if w_configuratore.istr_flags.flag_cordolo = "S" then fdw_passo.setitem(li_row, "rs_colore_cordolo", ls_colore_cordolo)
				if w_configuratore.istr_flags.flag_passamaneria = "S" then fdw_passo.setitem(li_row, "rs_colore_passamaneria", ls_colore_passamaneria)
			else
				fdw_passo.setitem(li_row, "rs_cod_mant_non_a_magazzino", "")
				w_configuratore.istr_riepilogo.cod_mant_non_a_magazzino = ""
			end if
			f_PO_LoadDDDW_DW(fdw_passo,"rs_cod_tessuto_mant",sqlca, &
			                 "tab_tessuti","cod_tessuto", "cod_non_a_magazzino", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tessuto = '" + fs_data+ "' and " + &
				              "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")
		else					
			// verifico che il tessuto esista con il colore selezionato
			select cod_non_a_magazzino,
					 colore_passamaneria,   
					 colore_cordolo,
					 cod_tipo_campionario
			into   :ls_colore_tessuto,
					 :ls_colore_passamaneria,   
					 :ls_colore_cordolo ,
					 :ls_cod_tipo_campionario
			from   tab_tessuti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tessuto = :fs_data and
					 cod_non_a_magazzino = :ls_colore_tessuto and
				    ((flag_blocco = 'N') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
			if sqlca.sqlcode = 100 or sqlca.sqlcode < 0 then
				fs_messaggio = "Il colore " + w_configuratore.istr_riepilogo.cod_mant_non_a_magazzino + " non esiste per il gruppo tessuti " + fs_data + ": selezionare un colore valido!"
				fdw_passo.setitem(li_row, "rs_cod_mant_non_a_magazzino", ls_null)
				w_configuratore.istr_riepilogo.cod_tessuto_mant = fs_data
				return -1						// questo tessuto non esiste con questo colore
			else
				fdw_passo.setitem(li_row, "rs_cod_tessuto_mant", fs_data)
				if w_configuratore.istr_flags.flag_cordolo = "S" then fdw_passo.setitem(li_row, "rs_colore_cordolo", ls_colore_cordolo)
				if w_configuratore.istr_flags.flag_passamaneria = "S" then fdw_passo.setitem(li_row, "rs_colore_passamaneria", ls_colore_passamaneria)
			end if	
		end if		
		w_configuratore.istr_riepilogo.cod_tessuto_mant = fs_data
		
		// Specifica requisiti "clienti_atelier_2" EnMe 25/3/2008
		choose case uof_controlla_atelier(ls_cod_tipo_campionario, ref fs_messaggio)
			case -1
				return -1
			case -2
				return -2
		end choose

	case "rs_flag_mantovana"
		if fs_data = "N" then
			fdw_passo.setitem(li_row, "rs_cod_mant_non_a_magazzino", ls_null)
			fdw_passo.setitem(li_row, "rs_cod_tessuto_mant", ls_null)
			fdw_passo.setitem(li_row, "rn_diam_tubetto_mant", 0)
			fdw_passo.setitem(li_row, "rn_rapporto_mantovana", 0)
			fdw_passo.setitem(li_row, "rn_alt_mantovana", 0)
			fdw_passo.setitem(li_row, "rs_colore_cordolo", ls_null)
			fdw_passo.setitem(li_row, "rs_colore_passamaneria",ls_null)
		end if
	case "rs_flag_tipo_mantovana"
		w_configuratore.istr_riepilogo.flag_tipo_mantovana = fs_data
		
	case "rn_alt_mantovana"
		w_configuratore.istr_riepilogo.alt_mantovana = double(fs_data)

	case "rs_flag_cordolo"
		w_configuratore.istr_riepilogo.flag_cordolo = fs_data

	case "rs_flag_passamaneria"
		w_configuratore.istr_riepilogo.flag_passamaneria = fs_data


	case "rs_colore_cordolo"
		w_configuratore.istr_riepilogo.colore_cordolo = fs_data

	case "rs_colore_passamaneria"
		w_configuratore.istr_riepilogo.colore_passamaneria = fs_data
		
	case "rs_tess_addizionale"
		w_configuratore.istr_riepilogo.flag_addizionale_tessuto = fs_data

	case "rs_flag_tessuto_cliente"
		w_configuratore.istr_riepilogo.flag_tessuto_cliente = fs_data
		if fs_data = "S" then
			fdw_passo.setitem(li_row, "rs_flag_tessuto_mant_cliente", "S")
			w_configuratore.istr_riepilogo.flag_tessuto_mant_cliente = "S"
		end if

	case "rs_flag_tessuto_mant_cliente"
		w_configuratore.istr_riepilogo.flag_tessuto_mant_cliente = fs_data
		
	case "pos_comando"
		w_configuratore.istr_riepilogo.pos_comando = fs_data
		

end choose
return 0
end function

public function integer uof_controlla_atelier (string fs_cod_tipo_campionario, ref string fs_messaggio);/* ---------------------------------------------------------------------------------------------
	Funzione che esegue il controllo per tessuto/campionario/tipo cliente

	Variabili passate: 		nome					 		tipo

									fs_cod_tipo_campionario	string
									fs_messaggio				string
									fs_data				 String			valore
									fs_messaggio		 String			referenza


Valore di return della funzione:
-2 = ritorno con richiesta di conferma
-1 = errore; nessuna richiesta
0  = tutto OK

 ---------------------------------------------------------------------------------------------*/
boolean lb_flag_aca	
string ls_cod_tipo_anagrafica
long   ll_cont_campionario


guo_functions.uof_get_parametro_azienda("ACA", lb_flag_aca)

if lb_flag_aca then

	// Specifica requisiti "clienti_atelier_2" EnMe 25/3/2008
	select cod_tipo_anagrafica
	into   :ls_cod_tipo_anagrafica
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_cliente = :str_conf_prodotto.cod_cliente;
	
	choose case sqlca.sqlcode
		case 0	// cliente trovato
			if isnull(ls_cod_tipo_anagrafica) then
				fs_messaggio = "ATTENZIONE: il cliente non è associato ad alcun 'tipo anagrafica'. PROCEDO LO STESSO ?"
				return -2
			end if
			
			if isnull(fs_cod_tipo_campionario) then
				fs_messaggio = "ATTENZIONE: il tessuto scelto non è associato ad alcun 'tipo campionario'. Potrebbe essere ESAURITO o IN ESAURIMENTO. PROCEDO LO STESSO ?"
				return -2
			end if
			
			select count(*)
			into   :ll_cont_campionario
			from   tab_tipi_anag_tipi_camp
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_anagrafica = :ls_cod_tipo_anagrafica and
					 cod_tipo_campionario = :fs_cod_tipo_campionario;
					 
			if ll_cont_campionario < 1 then
				fs_messaggio = "ATTENZIONE: il campionario non è associato al cliente: PROCEDO LO STESSO ?"
				return -2
			end if			
					 
		case 100 /// cliente non esiste
			fs_messaggio = "ATTENZIONE: il tipo anagrafica "+ls_cod_tipo_anagrafica+" non è associato al campionario " + fs_cod_tipo_campionario + " PROCEDO LO STESSO ?"
			return -2
			
		case is < 0 // errore SQL
			fs_messaggio = "Errore in ricerca associazione tipo_anagrafica e tipo_campionario~r~n" + sqlca.sqlerrtext
			return -1
			
	end choose
	// fine specifica "Clienti_Atelier_2"
	
end if	

return 0
end function

public function integer uof_reset_tessuto (datawindow fdw_passo, string fs_tessuto_mantovana);string ls_null,ls_nome_dw

setnull(ls_null)

ls_nome_dw = fdw_passo.dataobject

choose case ls_nome_dw
	case "d_ext_tessuto", "d_ext_tessuto_dk", "d_ext_tessuto_alu"

		w_configuratore.istr_riepilogo.cod_non_a_magazzino = ""
		w_configuratore.istr_riepilogo.cod_tessuto = ""
		fdw_passo.setitem(fdw_passo.getrow(), "rs_cod_tessuto", ls_null)
		fdw_passo.setitem(fdw_passo.getrow(), "rs_cod_tessuto_manuale", ls_null)
		fdw_passo.setitem(fdw_passo.getrow(), "rs_tess_fuori_campionario", "N")
		fdw_passo.setitem(fdw_passo.getrow(), "rs_tess_addizionale", "N")
		fdw_passo.setitem(fdw_passo.getrow(), "rs_cod_mant_non_a_magazzino", ls_null)
		fdw_passo.setitem(fdw_passo.getrow(), "rs_cod_tessuto_mant_manuale", ls_null)
		fdw_passo.setitem(fdw_passo.getrow(), "rs_cod_tessuto_mant", ls_null)
		fdw_passo.setitem(fdw_passo.getrow(), "rs_colore_cordolo", ls_null)
		fdw_passo.setitem(fdw_passo.getrow(), "rs_colore_passamaneria", ls_null)
		
end choose

return 0

end function

public function integer uof_itemchanged_verniciature (datawindow fdw_passo, string fs_col_name, string fs_data, ref string fs_messaggio);//	Funzione che gestisce gli eventi itemchanged delle varie DataWindow in maniera da slegare
//		la dipendenza tra Controllo Dw e Oggetto Dw
//
// nome: wf_item_changed
// tipo: none
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//
//									fdw_passo			 DataWindow		valore
//									fs_col_name			 String			valore
//									fs_data				 String			valore
//									fs_messaggio		 String			referenza
//
//		Creata il 31-08-98 
//		Autore enrico menegotto
//    modificata il 31/08/1998

string ls_cod_tessuto_manuale, ls_cod_verniciatura_manuale, ls_flag_fuori_campionario, ls_flag_addizionale
string ls_cod_veloce, ls_cod_tessuto, ls_cod_tipo_stampo_manuale, ls_cod_tipo_stampo, ls_cod_comando_manuale, &
		 ls_cod_modello_manuale, ls_cod_comando, ls_des_dim_x, ls_des_dim_y, ls_des_dim_z, ls_des_dim_t, &
       ls_cod_tessuto_mant, ls_cod_colore_mant, ls_colore_passamaneria, ls_colore_cordolo 		 
integer li_row
long   ll_cont
li_row = 1

choose case fs_col_name

	case "rs_cod_verniciatura"
		
		if isnull(fs_data) then
			fdw_passo.setitem(li_row, "rs_cod_verniciatura", "")
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale", "")
			ls_cod_veloce = "?"
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select count(*)
			into   :ll_cont
			from   tab_modelli_verniciature
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_modello = :is_cod_modello and
					 cod_verniciatura = :fs_data and
					 num_verniciatura = 1 and
					 flag_blocco = 'N';
			if ll_cont <> 1 then
				fs_messaggio = "Codice Verniciatura non Valido per questo modello"
				return -1
			end if
			
			select cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale
			into  :ls_cod_veloce,
					:ls_flag_fuori_campionario,
					:ls_flag_addizionale
			from  tab_verniciatura
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_verniciatura = :fs_data;
			if SQLCA.sqlcode <> 0 then
				fs_messaggio = "Errore durante ricerca codice verniciatura in tabella verniciature.~r~nDettaglio errore: " + sqlca.sqlerrtext
				return -1
			end if
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale", fs_data)
		end if
		w_configuratore.istr_riepilogo.cod_verniciatura = fs_data
		fdw_passo.setitem(li_row, "rs_vern_fuori_campionario", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_vern_addizionale", ls_flag_addizionale)
		if ls_flag_fuori_campionario = "S" then
			fdw_passo.object.rs_des_vernice_fc_t.color = 8388608
			fdw_passo.object.rs_des_vernice_fc.background.color = 16777215
			fdw_passo.object.rs_des_vernice_fc.protect = 0
		else
			fdw_passo.object.rs_des_vernice_fc_t.color = 8421504
			fdw_passo.object.rs_des_vernice_fc.background.color = 79741120
			fdw_passo.object.rs_des_vernice_fc.protect = 1
		end if

		w_configuratore.wf_gest_mp_non_richieste(w_configuratore.istr_riepilogo.dimensione_1, w_configuratore.istr_riepilogo.dimensione_2, w_configuratore.istr_riepilogo.dimensione_3, w_configuratore.istr_riepilogo.dimensione_4, w_configuratore.istr_riepilogo.alt_asta)
	
	case "rs_cod_verniciatura_manuale"
		if isnull(fs_data) or fs_data = "" then
			fdw_passo.setitem(li_row, "rs_cod_verniciatura", "")
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale", "")
			ls_cod_veloce = "?"
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select count(*)
			into   :ll_cont
			from   tab_modelli_verniciature
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_modello = :is_cod_modello and
					 cod_verniciatura = :fs_data and
					 num_verniciatura = 1 and
					 flag_blocco = 'N';
			if ll_cont <> 1 then
				fs_messaggio = "Codice Verniciatura non Valido per questo modello"
				return -1
			end if
			
			select cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale
			into  :ls_cod_veloce,
					:ls_flag_fuori_campionario,
					:ls_flag_addizionale
			from  tab_verniciatura
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_verniciatura = :fs_data;
			if SQLCA.sqlcode <> 0 then
				fs_messaggio = "Errore durante ricerca codice verniciatura in tabella verniciature.~r~nDettaglio errore: " + sqlca.sqlerrtext
				return -1
			end if
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale", fs_data)
		end if

		w_configuratore.istr_riepilogo.cod_verniciatura = fs_data
		fdw_passo.setitem(li_row, "rs_vern_fuori_campionario", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_vern_addizionale", ls_flag_addizionale)
		if ls_flag_fuori_campionario = "S" then
			fdw_passo.object.rs_des_vernice_fc.background.color = 16777215
			fdw_passo.object.rs_des_vernice_fc.protect = 0
		else
			fdw_passo.object.rs_des_vernice_fc.background.color = 79741120
			fdw_passo.object.rs_des_vernice_fc.protect = 1
		end if

	case "rs_des_vernice_fc"
		w_configuratore.istr_riepilogo.des_vernice_fc = fs_data
		
	case "rs_vern_addizionale"
		w_configuratore.istr_riepilogo.flag_addizionale_vernic = fs_data
		
	// ***********************************
	//
	//		VERNICIATURA 2
	//
	// ************************************
		
	case "rs_cod_verniciatura_2"
		
		if isnull(fs_data) then
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_2", "")
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale_2", "")
			ls_cod_veloce = "?"
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select count(*)
			into   :ll_cont
			from   tab_modelli_verniciature
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_modello = :is_cod_modello and
					 cod_verniciatura = :fs_data and
					 num_verniciatura = 2 and
					 flag_blocco = 'N';
			if ll_cont <> 1 then
				fs_messaggio = "Codice Verniciatura non Valido per questo modello"
				return -1
			end if
			
			select cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale
			into  :ls_cod_veloce,
					:ls_flag_fuori_campionario,
					:ls_flag_addizionale
			from  tab_verniciatura
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_verniciatura = :fs_data;
			if SQLCA.sqlcode <> 0 then
				fs_messaggio = "Errore durante ricerca codice verniciatura in tabella verniciature.~r~nDettaglio errore: " + sqlca.sqlerrtext
				return -1
			end if
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale_2", fs_data)
		end if
		w_configuratore.istr_riepilogo.cod_verniciatura_2 = fs_data
		fdw_passo.setitem(li_row, "rs_vern_fuori_campionario_2", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_vern_addizionale_2", ls_flag_addizionale)
		if ls_flag_fuori_campionario = "S" then
			fdw_passo.object.rs_des_vernice_fc_2.background.color = 16777215
			fdw_passo.object.rs_des_vernice_fc_2.protect = 0
		else
			fdw_passo.object.rs_des_vernice_fc_2.background.color = 79741120
			fdw_passo.object.rs_des_vernice_fc_2.protect = 1
		end if

		w_configuratore.wf_gest_mp_non_richieste(w_configuratore.istr_riepilogo.dimensione_1, w_configuratore.istr_riepilogo.dimensione_2, w_configuratore.istr_riepilogo.dimensione_3, w_configuratore.istr_riepilogo.dimensione_4, w_configuratore.istr_riepilogo.alt_asta)
	
	case "rs_cod_verniciatura_manuale_2"
		if isnull(fs_data) or fs_data = "" then
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_2", "")
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale_2", "")
			ls_cod_veloce = "?"
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select count(*)
			into   :ll_cont
			from   tab_modelli_verniciature
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_modello = :is_cod_modello and
					 cod_verniciatura = :fs_data and
					 num_verniciatura = 2 and
					 flag_blocco = 'N';
			if ll_cont <> 1 then
				fs_messaggio = "Codice Verniciatura non Valido per questo modello"
				return -1
			end if
			
			select cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale
			into  :ls_cod_veloce,
					:ls_flag_fuori_campionario,
					:ls_flag_addizionale
			from  tab_verniciatura
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_verniciatura = :fs_data;
			if SQLCA.sqlcode <> 0 then
				fs_messaggio = "Errore durante ricerca codice verniciatura in tabella verniciature.~r~nDettaglio errore: " + sqlca.sqlerrtext
				return -1
			end if
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale_2", fs_data)
		end if

		w_configuratore.istr_riepilogo.cod_verniciatura_2 = fs_data
		fdw_passo.setitem(li_row, "rs_vern_fuori_campionario_2", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_vern_addizionale_2", ls_flag_addizionale)
		if ls_flag_fuori_campionario = "S" then
			fdw_passo.object.rs_des_vernice_fc_2.background.color = 16777215
			fdw_passo.object.rs_des_vernice_fc_2.protect = 0
		else
			fdw_passo.object.rs_des_vernice_fc_2.background.color = 79741120
			fdw_passo.object.rs_des_vernice_fc_2.protect = 1
		end if

	case "rs_des_vernice_fc_2"
		w_configuratore.istr_riepilogo.des_vernice_fc_2 = fs_data
		
	case "rs_vern_addizionale_3"
		w_configuratore.istr_riepilogo.flag_addizionale_vernic_2 = fs_data
		
		
		
	// ***********************************
	//
	//		VERNICIATURA 3
	//
	// ************************************
	
	case "rs_cod_verniciatura_3"
		
		if isnull(fs_data) then
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_3", "")
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale_3", "")
			ls_cod_veloce = "?"
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select count(*)
			into   :ll_cont
			from   tab_modelli_verniciature
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_modello = :is_cod_modello and
					 cod_verniciatura = :fs_data and
					 num_verniciatura = 3 and
					 flag_blocco = 'N';
			if ll_cont <> 1 then
				fs_messaggio = "Codice Verniciatura non Valido per questo modello"
				return -1
			end if
			
			select cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale
			into  :ls_cod_veloce,
					:ls_flag_fuori_campionario,
					:ls_flag_addizionale
			from  tab_verniciatura
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_verniciatura = :fs_data;
			if SQLCA.sqlcode <> 0 then
				fs_messaggio = "Errore durante ricerca codice verniciatura in tabella verniciature.~r~nDettaglio errore: " + sqlca.sqlerrtext
				return -1
			end if
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale_3", fs_data)
		end if
		w_configuratore.istr_riepilogo.cod_verniciatura_3 = fs_data
		fdw_passo.setitem(li_row, "rs_vern_fuori_campionario_3", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_vern_addizionale_3", ls_flag_addizionale)
		if ls_flag_fuori_campionario = "S" then
			fdw_passo.object.rs_des_vernice_fc_3.background.color = 16777215
			fdw_passo.object.rs_des_vernice_fc_3.protect = 0
		else
			fdw_passo.object.rs_des_vernice_fc_3.background.color = 79741120
			fdw_passo.object.rs_des_vernice_fc_3.protect = 1
		end if

		w_configuratore.wf_gest_mp_non_richieste(w_configuratore.istr_riepilogo.dimensione_1, w_configuratore.istr_riepilogo.dimensione_2, w_configuratore.istr_riepilogo.dimensione_3, w_configuratore.istr_riepilogo.dimensione_4, w_configuratore.istr_riepilogo.alt_asta)
	
	case "rs_cod_verniciatura_manuale_3"
		if isnull(fs_data) or fs_data = "" then
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_3", "")
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale_3", "")
			ls_cod_veloce = "?"
			ls_flag_fuori_campionario = "N"
			ls_flag_addizionale = "N"
		else
			select count(*)
			into   :ll_cont
			from   tab_modelli_verniciature
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_modello = :is_cod_modello and
					 cod_verniciatura = :fs_data and
					 num_verniciatura = 3 and
					 flag_blocco = 'N';
			if ll_cont <> 1 then
				fs_messaggio = "Codice Verniciatura non Valido per questo modello"
				return -1
			end if
			
			select cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale
			into  :ls_cod_veloce,
					:ls_flag_fuori_campionario,
					:ls_flag_addizionale
			from  tab_verniciatura
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_verniciatura = :fs_data;
			if SQLCA.sqlcode <> 0 then
				fs_messaggio = "Errore durante ricerca codice verniciatura in tabella verniciature.~r~nDettaglio errore: " + sqlca.sqlerrtext
				return -1
			end if
			fdw_passo.setitem(li_row, "rs_cod_verniciatura_manuale_3", fs_data)
		end if

		w_configuratore.istr_riepilogo.cod_verniciatura_3 = fs_data
		fdw_passo.setitem(li_row, "rs_vern_fuori_campionario_3", ls_flag_fuori_campionario)
		fdw_passo.setitem(li_row, "rs_vern_addizionale_3", ls_flag_addizionale)
		if ls_flag_fuori_campionario = "S" then
			fdw_passo.object.rs_des_vernice_fc_3.background.color = 16777215
			fdw_passo.object.rs_des_vernice_fc_3.protect = 0
		else
			fdw_passo.object.rs_des_vernice_fc_3.background.color = 79741120
			fdw_passo.object.rs_des_vernice_fc_3.protect = 1
		end if

	case "rs_des_vernice_fc_3"
		w_configuratore.istr_riepilogo.des_vernice_fc_2 = fs_data
		
	case "rs_vern_addizionale_3"
		w_configuratore.istr_riepilogo.flag_addizionale_vernic_3 = fs_data
		
		
	
	
		
end choose

RETURN 0
end function

public function integer uof_validazione_dimensioni (ref datawindow fdw_inizio, ref string fs_messaggio);string ls_cod_modello_manuale, ls_des_variabile, ls_errore
dec{4} ld_dim_max, ld_dim_min, ld_dim

ls_cod_modello_manuale = fdw_inizio.getitemstring(1, "rs_cod_modello")
if isnull(ls_cod_modello_manuale) then
	fs_messaggio = "Inserire prima il codice del modello"
	return -1
end if

ld_dim = fdw_inizio.getitemnumber(fdw_inizio.getrow(),"rd_dimensione_1")
if uof_validazione_dimensione_singola( ls_cod_modello_manuale, 'dim_x', ld_dim, ref ls_errore) < 0 then
	g_mb.warning(ls_errore)
end if

ld_dim = fdw_inizio.getitemnumber(fdw_inizio.getrow(),"rd_dimensione_2")
if uof_validazione_dimensione_singola( ls_cod_modello_manuale, 'dim_y', ld_dim, ref ls_errore) < 0 then
	g_mb.warning(ls_errore)
end if

ld_dim = fdw_inizio.getitemnumber(fdw_inizio.getrow(),"rd_dimensione_3")
if uof_validazione_dimensione_singola( ls_cod_modello_manuale, 'dim_z', ld_dim, ref ls_errore) < 0 then
	g_mb.warning(ls_errore)
end if

ld_dim = fdw_inizio.getitemnumber(fdw_inizio.getrow(),"rd_dimensione_4")
if uof_validazione_dimensione_singola( ls_cod_modello_manuale, 'dim_t', ld_dim, ref ls_errore) < 0 then
	g_mb.warning(ls_errore)
end if

return 0
end function

public function integer uof_validazione_dimensione_singola (string as_cod_modello, string as_dimensione, decimal ad_valore, ref string as_messaggio);string ls_des_variabile, ls_sql
long	ll_ret
dec{4} ld_dim_max, ld_dim_min
datastore lds_data

if isnull(as_cod_modello) then
	as_messaggio = "uof_validazione_dimensione_singola(): manca il codice modello del prodotto finito!"
	return -1
end if

select 	tab_des_variabili.des_variabile
into		:ls_des_variabile
from 		tab_des_variabili
join 		tab_variabili_formule on tab_des_variabili.cod_azienda = tab_variabili_formule.cod_azienda and tab_des_variabili.cod_variabile = tab_variabili_formule.cod_variabile
where 	tab_des_variabili.cod_azienda = :s_cs_xx.cod_azienda and
			tab_des_variabili.cod_prodotto = :as_cod_modello and
			tab_variabili_formule.nome_campo_database =:as_dimensione;

if sqlca.sqlcode = 0 then
	ls_sql = " select limite_max_" + as_dimensione + ", limite_min_" + as_dimensione + " from tab_flags_configuratore " + &
				" where  cod_azienda = '"  + s_cs_xx.cod_azienda + "' and cod_modello = '" + as_cod_modello + "' "

	ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql)
	choose case ll_ret
		case is < 0
			as_messaggio = "uof_validazione_dimensione_singola(): errore nella creazione del datastore lds_data"
			return -1
		case is > 0
			ld_dim_max = lds_data.getitemdecimal(1,1)
			ld_dim_min = lds_data.getitemdecimal(1,2)
	
			if (ld_dim_min > 0 and not isnull(ld_dim_min)) or ( ld_dim_max > 0 and not isnull(ld_dim_max)) then
				if ad_valore > ld_dim_max or ad_valore < ld_dim_min or ad_valore = 0 then
					as_messaggio = "Attenzione! "+ls_des_variabile+" fuori dei limiti consentiti per il modello" + as_cod_modello
					return -1
				end if
			end if
	end choose
end if

return 0
end function

public function string uof_get_formula_valore_predefinito (long ad_num_variabile);long	ll_i
string ls_formula_valore_default, ls_cod_variabile_corrente, ls_cod_variabile, ls_nome_campo_database

ls_cod_variabile_corrente = ""

for ll_i = 1 to 9 
	
	choose case ll_i
		case 1
			ls_nome_campo_database = "dim_x"
		case 2
			ls_nome_campo_database = "dim_y"
		case 3
			ls_nome_campo_database = "dim_z"
		case 4
			ls_nome_campo_database = "dim_t"
		case 5
			ls_nome_campo_database = "dim_05"
		case 6
			ls_nome_campo_database = "dim_06"
		case 7
			ls_nome_campo_database = "dim_07"
		case 8
			ls_nome_campo_database = "dim_08"
		case 9
			ls_nome_campo_database = "dim_09"
	end choose
	
	
	select cod_variabile
	into	:ls_cod_variabile
	from	tab_variabili_formule
	where cod_azienda = :s_cs_xx.cod_azienda and
			nome_campo_database = :ls_nome_campo_database;
	if sqlca.sqlcode = 0 and ll_i = ad_num_variabile then
		ls_cod_variabile_corrente = ls_cod_variabile
		exit
	end if
next

select formula_valore_default
into	:ls_formula_valore_default
from	tab_des_variabili
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :is_cod_modello and
		cod_variabile = :ls_cod_variabile_corrente ;
if sqlca.sqlcode = 0 then
	return ls_formula_valore_default
else
	return ""
end if
end function

public function integer uof_calcola_formula_inizio (datawindow fdw_passo, long al_num_dimensione);long		ll_i, ll_num_valore
string		ls_val_variabile, ls_cod_variabile, ls_nome_campo_database, ls_errore, ls_formula
decimal	ld_risultato
uo_formule_calcolo luo_formule


for ll_num_valore = 1 to 9
	if ll_num_valore = al_num_dimensione then continue
	
	ls_formula=uof_get_formula_valore_predefinito( ll_num_valore )
	if isnull(ls_formula) or len(ls_formula) < 1 then continue
	
	for ll_i = 1 to 9 
		choose case ll_i
			case 1
				ls_nome_campo_database = "dim_x"
				ls_val_variabile =	string(w_configuratore.istr_riepilogo.dimensione_1)
			case 2
				ls_nome_campo_database = "dim_y"
				ls_val_variabile =	string(w_configuratore.istr_riepilogo.dimensione_2)
			case 3
				ls_nome_campo_database = "dim_z"
				ls_val_variabile =	string(w_configuratore.istr_riepilogo.dimensione_3)
			case 4
				ls_nome_campo_database = "dim_t"
				ls_val_variabile =	string(w_configuratore.istr_riepilogo.dimensione_4)
			case 5
				ls_nome_campo_database = "dim_05"
				ls_val_variabile =	string(w_configuratore.istr_riepilogo.dimensione_5)
			case 6
				ls_nome_campo_database = "dim_06"
				ls_val_variabile =	string(w_configuratore.istr_riepilogo.dimensione_6)
			case 7
				ls_nome_campo_database = "dim_07"
				ls_val_variabile =	string(w_configuratore.istr_riepilogo.dimensione_7)
			case 8
				ls_nome_campo_database = "dim_08"
				ls_val_variabile =	string(w_configuratore.istr_riepilogo.dimensione_8)
			case 9
				ls_nome_campo_database = "dim_09"
				ls_val_variabile =	string(w_configuratore.istr_riepilogo.dimensione_9)
		end choose
		
		
		select cod_variabile
		into	:ls_cod_variabile
		from	tab_variabili_formule
		where cod_azienda = :s_cs_xx.cod_azienda and
				nome_campo_database = :ls_nome_campo_database;
		if sqlca.sqlcode = 0 then
			guo_functions.uof_replace_string(ls_val_variabile, ",", ".")
			guo_functions.uof_replace_string(ls_formula, ls_cod_variabile, ls_val_variabile)
		end if
		
	next
	
	luo_formule = CREATE uo_formule_calcolo
	
	luo_formule.uof_pre_calcolo_formula_db(ls_formula)
	
	if luo_formule.uof_eval_with_datastore(ls_formula, ld_risultato, ls_errore) < 0 then
		return -1
	end if
	destroy luo_formule
	
	ld_risultato = round(ld_risultato,4)
	
	fdw_passo.setitem(1, "rd_dimensione_" + string(ll_num_valore), ld_risultato)
	
	choose case ll_num_valore
		case 1
			w_configuratore.istr_riepilogo.dimensione_1= ld_risultato
		case 2
			w_configuratore.istr_riepilogo.dimensione_2 = ld_risultato
		case 3
			w_configuratore.istr_riepilogo.dimensione_3 = ld_risultato
		case 4
			w_configuratore.istr_riepilogo.dimensione_4 = ld_risultato
		case 5
			w_configuratore.istr_riepilogo.dimensione_5=ld_risultato
		case 6
			w_configuratore.istr_riepilogo.dimensione_6=ld_risultato
		case 7
			w_configuratore.istr_riepilogo.dimensione_7=ld_risultato
		case 8
			w_configuratore.istr_riepilogo.dimensione_8=ld_risultato
		case 9
			w_configuratore.istr_riepilogo.dimensione_9=ld_risultato
	end choose
next
	
return 0
end function

on uo_itemchanged.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_itemchanged.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

