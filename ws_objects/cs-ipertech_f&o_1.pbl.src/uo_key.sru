﻿$PBExportHeader$uo_key.sru
$PBExportComments$UO gestione evento UE_KEY nel configuratore
forward
global type uo_key from nonvisualobject
end type
end forward

global type uo_key from nonvisualobject
end type
global uo_key uo_key

forward prototypes
public function integer uof_key (uo_cs_xx_dw fdw_passo, keycode key, unsignedlong keyflags)
end prototypes

public function integer uof_key (uo_cs_xx_dw fdw_passo, keycode key, unsignedlong keyflags);any						la_any[]
datawindow				ldw_data
string						ls_cod_prodotto, ls_messaggio


choose case fdw_passo.dataobject
	case "d_ext_inizio"
		
		if key =  keyF1! and keyflags = 1 and fdw_passo.getcolumnname() = "rs_cod_modello_manuale" then
			guo_ricerca.uof_set_response()
			guo_ricerca.uof_ricerca_prodotto(fdw_passo, "rs_cod_modello_manuale")
			string ls_prod
			ls_prod = fdw_passo.getitemstring(fdw_passo.getrow(), "rs_cod_modello_manuale")
			fdw_passo.event itemchanged(fdw_passo.getrow(), fdw_passo.object.rs_cod_modello_manuale, ls_prod)
		end if
		
	case "d_ext_tessuto"
		
		if key =  keyF1! and keyflags = 1 and fdw_passo.getcolumnname() = "rs_cod_modello_telo_finito" then
			guo_ricerca.uof_set_response()
			guo_ricerca.uof_ricerca_prodotto(fdw_passo, "rs_cod_modello_telo_finito")
		end if
		
		if key =  keyF1! and keyflags = 1 and ( fdw_passo.getcolumnname() = "rs_cod_non_a_magazzino" or  fdw_passo.getcolumnname() = "rs_cod_tessuto_manuale" ) then
			
			s_cs_xx.parametri.parametro_uo_dw_1 = fdw_passo
			s_cs_xx.parametri.parametro_s_1 = "rs_cod_non_a_magazzino"
			s_cs_xx.parametri.parametro_s_2 = "rs_cod_tessuto"
			
			window_open(w_tessuti_ricerca, 0)
			
			fdw_passo.setcolumn("rs_cod_tessuto")
			fdw_passo.settext(s_cs_xx.parametri.parametro_s_1)
			fdw_passo.setcolumn("rs_cod_non_a_magazzino")
			fdw_passo.settext(s_cs_xx.parametri.parametro_s_2)
			fdw_passo.accepttext()
			
		end if
		
		if key =  keyF1! and keyflags = 1 and ( fdw_passo.getcolumnname() = "rs_cod_mant_non_a_magazzino" or  fdw_passo.getcolumnname() = "rs_cod_tessuto_mant" ) then
			
			s_cs_xx.parametri.parametro_uo_dw_1 = fdw_passo
			s_cs_xx.parametri.parametro_s_1 = "rs_cod_mant_non_a_magazzino"
			s_cs_xx.parametri.parametro_s_2 = "rs_cod_tessuto_mant"
			
			window_open(w_tessuti_ricerca, 0)
			
			fdw_passo.setcolumn("rs_cod_tessuto_mant")
			fdw_passo.settext(s_cs_xx.parametri.parametro_s_1)
			fdw_passo.setcolumn("rs_cod_mant_non_a_magazzino")
			fdw_passo.settext(s_cs_xx.parametri.parametro_s_2)
			fdw_passo.accepttext()
			
		end if
		
	case "d_ext_tessuto_dk", "d_ext_tessuto_alu"
		
		if key =  keyF1! and keyflags = 1 and fdw_passo.getcolumnname() = "rs_cod_modello_telo_finito" then
			guo_ricerca.uof_set_response()
			guo_ricerca.uof_ricerca_prodotto(fdw_passo, "rs_cod_modello_telo_finito")
		end if
		
		if key =  keyF1! and keyflags = 1 and ( fdw_passo.getcolumnname() = "rs_cod_non_a_magazzino" or  fdw_passo.getcolumnname() = "rs_cod_tessuto_manuale" ) then
			
			s_cs_xx.parametri.parametro_uo_dw_1 = fdw_passo
			s_cs_xx.parametri.parametro_s_1 = "rs_cod_non_a_magazzino"
			s_cs_xx.parametri.parametro_s_2 = "rs_cod_tessuto"
			
			window_open(w_tessuti_ricerca, 0)
			
			fdw_passo.setcolumn("rs_cod_tessuto")
			fdw_passo.settext(s_cs_xx.parametri.parametro_s_1)
			fdw_passo.setcolumn("rs_cod_non_a_magazzino")
			fdw_passo.settext(s_cs_xx.parametri.parametro_s_2)
			fdw_passo.accepttext()
			
		end if
		
		if key =  keyF1! and keyflags = 1 and ( fdw_passo.getcolumnname() = "rs_cod_mant_non_a_magazzino" or  fdw_passo.getcolumnname() = "rs_cod_tessuto_mant" ) then
			
			s_cs_xx.parametri.parametro_uo_dw_1 = fdw_passo
			s_cs_xx.parametri.parametro_s_1 = "rs_cod_mant_non_a_magazzino"
			s_cs_xx.parametri.parametro_s_2 = "rs_cod_tessuto_mant"
			
			window_open(w_tessuti_ricerca, 0)
			
			fdw_passo.setcolumn("rs_cod_tessuto_mant")
			fdw_passo.settext(s_cs_xx.parametri.parametro_s_1)
			fdw_passo.setcolumn("rs_cod_mant_non_a_magazzino")
			fdw_passo.settext(s_cs_xx.parametri.parametro_s_2)
			fdw_passo.accepttext()
			
		end if
		
		
	case "d_ext_vern_lastra"
	case "d_ext_comandi"
	case "d_ext_plisse"
	case "d_ext_supporto_flags"
	case "d_ext_tipi_stampi"
	case "d_ext_optional"
		string ls_cod_tipo_det_ven_optional 
		long   ll_riga
		
		if (key = KeyUpArrow! and keyflags = 1) or (key = KeyF11! and keyflags = 1) then
			
			fdw_passo.accepttext()
			ll_riga = fdw_passo.insertrow(fdw_passo.getrow())
			fdw_passo.setrow(ll_riga)
			SELECT 	cod_tipo_det_ven_optionals  
			INTO  		 :ls_cod_tipo_det_ven_optional  
			FROM   	tab_flags_configuratore  
			WHERE  	cod_azienda = :s_cs_xx.cod_azienda AND  
					 	cod_modello = :str_conf_prodotto.cod_modello;
			if not isnull(ls_cod_tipo_det_ven_optional) then
				fdw_passo.setitem(ll_riga, "rs_cod_tipo_det_ven", ls_cod_tipo_det_ven_optional)
			end if
			fdw_passo.setcolumn(2)
			
		end if
		if (key = KeyDownArrow! and keyflags = 1) or (key = KeyF12! and keyflags = 1) then
			fdw_passo.accepttext()
			if fdw_passo.getrow() = fdw_passo.rowcount() then
				ll_riga = fdw_passo.insertrow(0)
			else
				ll_riga = fdw_passo.insertrow(fdw_passo.getrow() + 1)
			end if
			fdw_passo.setrow(ll_riga)
			SELECT cod_tipo_det_ven_optionals  
			INTO   :ls_cod_tipo_det_ven_optional  
			FROM   tab_flags_configuratore  
			WHERE  cod_azienda = :s_cs_xx.cod_azienda AND  
					 cod_modello = :str_conf_prodotto.cod_modello;
			if not isnull(ls_cod_tipo_det_ven_optional) then
				fdw_passo.setitem(ll_riga, "rs_cod_tipo_det_ven", ls_cod_tipo_det_ven_optional)
			end if
			fdw_passo.setcolumn(2)
		end if
		if key = KeyDelete! then
			fdw_passo.deleterow(fdw_passo.getrow())
		end if
		
		if key =  keyF1! and keyflags = 1 and fdw_passo.getcolumnname() = "rs_cod_prodotto" then
			fdw_passo.change_dw_current()
			
			
			//Donato 29/12/2014
			//apertura nuova finestra di ricerca, chiesto da Alberto:  (ticket 2014/568)
			setnull(ldw_data)
			guo_ricerca.uof_set_response( )
			guo_ricerca.uof_ricerca_prodotto(ldw_data, "cod_prodotto")
			guo_ricerca.uof_get_results(la_any)
			
			if upperbound(la_any) > 0 then
				ls_cod_prodotto = string(la_any[1])
			end if
			
			setfocus(fdw_passo)
			
			if ls_cod_prodotto <> "" and not isnull(ls_cod_prodotto) then
				
				fdw_passo.setitem(fdw_passo.getrow(),"rs_cod_prodotto", ls_cod_prodotto)
				if w_configuratore.wf_item_changed (fdw_passo, "rs_cod_prodotto", ls_cod_prodotto, ref ls_messaggio) = -1 then
					g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
					return 2
				end if			
			end if
			
			//COMMENTATO Il CODICE SEGUENTE PERCHE' SOSTITUITO DAl CODICE PRECEDENTE
			
			//s_cs_xx.parametri.parametro_uo_dw_1 = fdw_passo
			//s_cs_xx.parametri.parametro_s_1 = ""
			//s_cs_xx.parametri.parametro_tipo_ricerca = 1
			//s_cs_xx.parametri.parametro_pos_ricerca = ""
			//if not isvalid(w_prodotti_ricerca_response) then
				//openwithparm(w_prodotti_ricerca_optional, w_configuratore)
				////open(w_prodotti_ricerca_optional)
			//end if
			
			//setfocus(fdw_passo)
			
			//if s_cs_xx.parametri.parametro_s_1 <> "" then
				
				//fdw_passo.setitem(fdw_passo.getrow(),"rs_cod_prodotto", s_cs_xx.parametri.parametro_s_1)
				//if w_configuratore.wf_item_changed (fdw_passo, "rs_cod_prodotto", s_cs_xx.parametri.parametro_s_1, ref ls_messaggio) = -1 then
					//g_mb.messagebox("Configuratore di Prodotto", ls_messaggio)
					//return 2
				//end if			
			//end if
			
			//fine modifica
			//--------------------------
			
			
			
		end if
		
	case "d_ext_fine"
		if key = keyF1! and keyflags = 1 then					// pressione tasto SHIFT-F1 = generazione commessa + avanzamento
			if w_configuratore.cb_fine.enabled = TRUE then
				w_configuratore.istr_flags.flag_genera_commessa = TRUE
				w_configuratore.istr_flags.flag_avanza_commessa = TRUE
				w_configuratore.cb_fine.postevent("clicked")
			end if
		end if
		if key = keyF2! and keyflags = 1 then					// pressione tasto SHIFT-F2 = generazione commessa
			if w_configuratore.cb_fine.enabled = TRUE then
				w_configuratore.istr_flags.flag_genera_commessa = TRUE
				w_configuratore.istr_flags.flag_avanza_commessa = FALSE
				w_configuratore.cb_fine.postevent("clicked")
			end if
		end if
		
end choose

// ---------------------- uso dei tasti generali ---------------------------------

if fdw_passo.dataobject <> "d_ext_nota_prodotto_finito" then
	choose case key
		case KeyEnter!
			if fdw_passo.dataobject <> "d_ext_note" then
				fdw_passo.accepttext()
				w_configuratore.wf_spostamento("A")
			end if
	end choose
end if
// -------------------------------------------------------------------------------

return 0
end function

on uo_key.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_key.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

