﻿$PBExportHeader$uo_optional.sru
$PBExportComments$UO Gestione Optional
forward
global type uo_optional from nonvisualobject
end type
end forward

global type uo_optional from nonvisualobject
end type
global uo_optional uo_optional

forward prototypes
public function integer uof_scrivi_optional (string fs_tipo_gestione, long fl_anno_documento, long fl_num_documento, long fl_prog_riga_documento, ref str_optional fstr_optional[], ref string fs_messaggio)
public function integer uof_leggi_optional (string fs_tipo_gestione, long fl_anno_documento, long fl_num_documento, long fl_prog_riga_documento, ref str_optional fstr_optional[], ref string fs_messaggio)
end prototypes

public function integer uof_scrivi_optional (string fs_tipo_gestione, long fl_anno_documento, long fl_num_documento, long fl_prog_riga_documento, ref str_optional fstr_optional[], ref string fs_messaggio);
// -----------------------------------------------------------------------------------------------------------
//                 funzione di scrittura righe optional sul dettaglio documento 
//
//
// variabile 					tipo		passaggio	descrizione
// ---------------------------------------------------------------------------------	
// fs_tipo_gestione			string	val			tipo gestione su cui sto lavorando
// fl_anno_documento 		long		val			anno documento di riferimento
// fl_num_documento			long		val			numero del documento
// fl_prog_riga_documento	long		val			progressivo riga del prodotto finito
// fstr_optional				struttura ref			struttura dove vengono caricati gli optional
//
//
//	autore: Enrico Menegotto   11/10/2000
//
// -----------------------------------------------------------------------------------------------------------
boolean  lb_found=false
string 	ls_flag_tipo_det_ven, ls_data_consegna, ls_sql, ls_des_prodotto, ls_note, ls_cod_valuta, ls_cod_prodotto, ls_cod_misura, &
       		ls_iva, ls_cod_iva, ls_flag_doc_suc_or, ls_flag_st_note_or, ls_cod_misura_mag,ls_cod_misura_ven,ls_cod_versione, &
			ls_flag_gen_commessa, ls_cod_prodotto_raggruppato, ls_tabella_det, ls_progressivo, ls_quantita, ls_errore, ls_cod_versione_formula, &
			ls_tabella_conf_var, ls_nome_var_dim_x, ls_nome_var_dim_y
long 	 ll_num_optional, ll_i, ll_prog_riga_documento, ll_ret
dec{4}	ld_quan_raggruppo, ld_dim_x, ld_dim_y
dec{5} 	ld_fat_conversione, ld_quantita_um, ld_prezzo_um
double 	ld_valore_riga, ld_sconti[],ld_precisione_prezzo_mag,ld_precisione_prezzo_ven
datetime ldt_data_consegna, ldt_data_esenzione_iva
uo_gestione_conversioni luo_gestione_conversioni
uo_default_prodotto luo_default_prodotto
uo_funzioni_1 luo_funzioni_1		
		
choose case fs_tipo_gestione
	case "ORD_VEN"
		
		ls_tabella_det = "det_ord_ven"
		ls_progressivo = "prog_riga_ord_ven"
		ls_quantita  	= "quan_ordine"
		ls_tabella_conf_var = "det_ord_ven_conf_variabili"
		
	case "OFF_VEN"
		
		ls_tabella_det = "det_off_ven"
		ls_progressivo = "prog_riga_off_ven"
		ls_quantita  	= "quan_offerta"
		ls_tabella_conf_var = "det_off_ven_conf_variabili"

end choose

select 	precisione_prezzo_mag
into   	:ld_precisione_prezzo_mag
from   	con_vendite
where  	cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in select tabella parametri vendite. Dettaglio = " + sqlca.sqlerrtext
	return -1
end if


setnull(ls_cod_valuta)

select 	cod_valuta
into   	:ls_cod_valuta
from   	tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       		anno_registrazione = :fl_anno_documento and
		 num_registrazione = :fl_num_documento;
		 
ll_num_optional = upperbound(fstr_optional)
for ll_i = 1 to ll_num_optional
	if len(fstr_optional[ll_i].cod_tipo_det_ven) > 0 and not isnull(fstr_optional[ll_i].cod_tipo_det_ven) then
		
		// ---------------------------- aggiornamento impegnato magazzino --------------------------------------
		select flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_det_ven = :fstr_optional[ll_i].cod_tipo_det_ven;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "uof_scrivi_optional: errore in ricerca flag_tipo_det_ven in tipi dettaglio di vendita"
			return -1
		end if
		
		if (ls_flag_tipo_det_ven ="M" or ls_flag_tipo_det_ven ="C") and (fstr_optional[ll_i].quan_vendita = 0 or isnull(fstr_optional[ll_i].quan_vendita) ) then
			g_mb.messagebox("Configuratore di Prodotto", "Prodotto: "+ fstr_optional[ll_i].cod_prodotto + "-" + fstr_optional[ll_i].des_prodotto+" Quantità a ZERO !")
		end if
		
		if fs_tipo_gestione = "ORD_VEN" then
			// solo in caso di ordine procedo con aggiornamento impegnato
		
			if ls_flag_tipo_det_ven = "M" then
				
				update anag_prodotti
				set    quan_impegnata = quan_impegnata + :fstr_optional[ll_i].quan_vendita
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :fstr_optional[ll_i].cod_prodotto;
				if sqlca.sqlcode <> 0 then
					fs_messaggio = "uof_scrivi_optional: errore in aggiornamento quantità impegnata del prodotto " + fstr_optional[ll_i].cod_prodotto
					return -1
				end if
				
				// enme 08/1/2006 gestione prodotto raggruppato
				setnull(ls_cod_prodotto_raggruppato)
				
				select cod_prodotto_raggruppato
				into   :ls_cod_prodotto_raggruppato
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :fstr_optional[ll_i].cod_prodotto;
						 
				if not isnull(ls_cod_prodotto_raggruppato) then
					
					ld_quan_raggruppo = f_converti_qta_raggruppo(fstr_optional[ll_i].cod_prodotto, fstr_optional[ll_i].quan_vendita, "M")
		
					update anag_prodotti
					set    quan_impegnata = quan_impegnata + :ld_quan_raggruppo
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_prodotto_raggruppato;
							 
					if sqlca.sqlcode <> 0 then
						fs_messaggio = "uof_scrivi_optional: errore in aggiornamento quantità impegnata del prodotto " + fstr_optional[ll_i].cod_prodotto
						return -1
					end if
					
				end if
				
			end if
			
		end if
		// ----------------------------- calcolo del valore della riga ------------------------------------------
		uo_scrivi_tabelle luo_scrivi_tabelle
		luo_scrivi_tabelle = CREATE uo_scrivi_tabelle
		
		ld_sconti[1] = fstr_optional[ll_i].sconto_1
		ld_sconti[2] = fstr_optional[ll_i].sconto_2
		ld_sconti[3] = fstr_optional[ll_i].sconto_3
		ld_sconti[4] = fstr_optional[ll_i].sconto_4
		ld_sconti[5] = fstr_optional[ll_i].sconto_5
		ld_sconti[6] = fstr_optional[ll_i].sconto_6
		ld_sconti[7] = fstr_optional[ll_i].sconto_7
		ld_sconti[8] = fstr_optional[ll_i].sconto_8
		ld_sconti[9] = fstr_optional[ll_i].sconto_9
		ld_sconti[10] = fstr_optional[ll_i].sconto_10
		if isnull(fstr_optional[ll_i].prezzo_vendita) then fstr_optional[ll_i].prezzo_vendita = 0
		
		if luo_scrivi_tabelle.uof_calcola_val_riga(fstr_optional[ll_i].cod_tipo_det_ven, &
																 ls_cod_valuta, &
																 fstr_optional[ll_i].quan_vendita, &
																 fstr_optional[ll_i].prezzo_vendita, &
																 ld_sconti[], &
																 str_conf_prodotto.sconto_testata, &
																 str_conf_prodotto.sconto_pagamento ,&
																 ld_valore_riga, &
																 fs_messaggio ) = -1 then 
			
			destroy luo_scrivi_tabelle
			return -1
		end if
		destroy luo_scrivi_tabelle
		
		if isnull(ld_valore_riga) then ld_valore_riga = 0
		
		
		// aggiunto per QUANTITA' VERSATILI (nuove colonne quantita_um e prezzo_um
		ld_fat_conversione = 1
		setnull(ls_cod_misura)
		ld_quantita_um = 0 
		ld_prezzo_um = 0
		
		if not isnull(fstr_optional[ll_i].cod_prodotto) and len(fstr_optional[ll_i].cod_prodotto) > 1 then
			select fat_conversione_ven, cod_misura_mag, cod_misura_ven
			into   :ld_fat_conversione, :ls_cod_misura_mag, :ls_cod_misura_ven
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :fstr_optional[ll_i].cod_prodotto;
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "uof_scrivi_optional: errore " + string(sqlca.sqlcode) + " in ricerca dell'anagrafica del prodotto  " + fstr_optional[ll_i].cod_prodotto
				return -1
			end if
				
			if fstr_optional[ll_i].cod_misura <> ls_cod_misura_ven and ld_fat_conversione <> 0 then
				ld_prezzo_um = fstr_optional[ll_i].prezzo_vendita / ld_fat_conversione
				luo_gestione_conversioni = CREATE uo_gestione_conversioni
				luo_gestione_conversioni.uof_arrotonda(ld_prezzo_um, ld_precisione_prezzo_mag)
				destroy luo_gestione_conversioni
				ld_quantita_um = round(fstr_optional[ll_i].quan_vendita * ld_fat_conversione, 4)
//				ld_prezzo_um = round(fstr_optional[ll_i].prezzo_vendita / ld_fat_conversione, 4)
				ls_cod_misura = ls_cod_misura_ven
			else
				ld_quantita_um = 0 
				ld_prezzo_um = 0
				ls_cod_misura = fstr_optional[ll_i].cod_misura
			end if
		end if
		// -------------------------------- scrittura di det_ord_ven ----------------------------------------------------------
		
		str_conf_prodotto.ultima_riga_inserita ++
		
		// controllo data consegna
		choose case fs_tipo_gestione
				
			case "ORD_VEN"
				
				select data_consegna
				into   :ldt_data_consegna
				from   tes_ord_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :str_conf_prodotto.anno_documento and
						 num_registrazione = :str_conf_prodotto.num_documento ;
				if sqlca.sqlcode <> 0 then
					setnull(ldt_data_consegna)
				end if
				
			case "OFF_VEN"
				
				select data_consegna
				into   :ldt_data_consegna
				from   tes_off_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :str_conf_prodotto.anno_documento and
						 num_registrazione = :str_conf_prodotto.num_documento ;
				if sqlca.sqlcode <> 0 then
					setnull(ldt_data_consegna)
				end if
				
		end choose
		
		ls_cod_prodotto = fstr_optional[ll_i].cod_prodotto
		ls_cod_versione = " null"
		if not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then
			select cod_versione
			into   :ls_cod_versione
			from   distinta_padri
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto and
					 flag_predefinita = 'S';
			if sqlca.sqlcode <> 0 then
				ls_cod_versione = "null"
				setnull(ls_cod_versione_formula)
			else
				ls_cod_versione_formula = ls_cod_versione
				ls_cod_versione = "'" + ls_cod_versione + "'"
			end if
		end if

		
		if isnull(str_conf_prodotto.data_consegna) then
			ls_data_consegna = " null"
		else
			ls_data_consegna = "'" + string(str_conf_prodotto.data_consegna, s_cs_xx.db_funzioni.formato_data) + "'"
		end if
		if isnull(fstr_optional[ll_i].note) then
			ls_note = " null"
		else
			ls_note = "'" + f_cazzillo(fstr_optional[ll_i].note) + "'"
		end if
		if isnull(fstr_optional[ll_i].des_prodotto) then
			ls_des_prodotto = " null "
		else
			ls_des_prodotto = "'" + f_cazzillo(fstr_optional[ll_i].des_prodotto) + "'"
		end if
		
		if isnull(fstr_optional[ll_i].cod_prodotto) then
			ls_cod_prodotto = " null "
		else
			ls_cod_prodotto = "'" + fstr_optional[ll_i].cod_prodotto + "'"
		end if
		if isnull(ls_cod_misura) or len(trim(ls_cod_misura)) < 1 then
			ls_cod_misura = " null "
		else
			ls_cod_misura = "'" + ls_cod_misura + "'"
		end if
		if isnull(fstr_optional[ll_i].provvigione_1) then fstr_optional[ll_i].provvigione_1 = 0
		if isnull(fstr_optional[ll_i].provvigione_2) then fstr_optional[ll_i].provvigione_2 = 0
		
		// verificare esenzione del cliente
		ls_iva = fstr_optional[ll_i].cod_iva
		
	   if not isnull(str_conf_prodotto.cod_contatto) then
			setnull(ls_cod_iva)
			
			f_trova_codice_iva ( 	str_conf_prodotto.data_documento, &
											fstr_optional[ll_i].cod_tipo_det_ven, &
											"O", &
											str_conf_prodotto.cod_contatto, &
											ref ls_iva )
			
			
	   elseif not isnull(str_conf_prodotto.cod_cliente) then
			setnull(ls_cod_iva)
			
			f_trova_codice_iva ( 	str_conf_prodotto.data_documento, &
											fstr_optional[ll_i].cod_tipo_det_ven, &
											"C", &
											str_conf_prodotto.cod_cliente, &
											ref ls_iva )
			
		end if
		
		// verificare nuovi flag note e documento successivo
		select flag_doc_suc_or, 
		       	flag_st_note_or
		into   :ls_flag_doc_suc_or,
		       	:ls_flag_st_note_or
		from   tab_tipi_det_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
		       	cod_tipo_det_ven = :fstr_optional[ll_i].cod_tipo_det_ven;
		if sqlca.sqlcode <> 0 then
			ls_flag_doc_suc_or = "I"
			ls_flag_st_note_or = "I"
		end if
		
		luo_default_prodotto = CREATE uo_default_prodotto
		
		if luo_default_prodotto.uof_flag_gen_commessa(fstr_optional[ll_i].cod_prodotto) <> 0 then
			ls_flag_gen_commessa = 'N'
		else
			ls_flag_gen_commessa = luo_default_prodotto.is_flag_gen_commessa
		end if
			
		destroy luo_default_prodotto
		
		// composizione sql
		
		ls_sql = " INSERT INTO " + ls_tabella_det + &
					"  ( cod_azienda, " + &
					"anno_registrazione, " + &
					"num_registrazione, " + &
					ls_progressivo +  ", " + &
					"cod_prodotto, " + &
					"cod_tipo_det_ven, " + &
					"cod_misura, " + &
					"des_prodotto, " + &
					ls_quantita + ", " + &
					"prezzo_vendita, " +&
					"fat_conversione_ven, " +&
					"sconto_1, " +&
					"sconto_2, " + &
					"provvigione_1, " + &
					"provvigione_2, " + &
					"cod_iva, " + &
					"data_consegna, " + &
					"val_riga, " + &
					"imponibile_iva, " + &
					"nota_dettaglio, " + &
					"cod_centro_costo, " + &
					"sconto_3, " + &
					"sconto_4, " + &
					"sconto_5, " + &
					"sconto_6, " + &
					"sconto_7, " + &
					"sconto_8, " + &
					"sconto_9, " + &
					"sconto_10, " + &
					"cod_versione, " + &
					"flag_presso_ptenda, " + &
					"flag_presso_cgibus, " + &
					"num_confezioni, " + &
					"num_pezzi_confezione, " + &
					"num_riga_appartenenza, " + &
					"flag_gen_commessa, " + &
					"flag_doc_suc_det, " + &
					"flag_st_note_det, " + &
					"quantita_um, " + &
					"prezzo_um, " + &
					"dim_x, " + &
					"dim_y " 
					
		if fs_tipo_gestione = "ORD_VEN" then
			ls_sql += ", " + &
			"quan_in_evasione, " + &
			"quan_evasa, " + &
			"flag_evasione, " + &
			"flag_blocco, " + &
			"anno_registrazione_off, " + &
			"num_registrazione_off, " + &
			"prog_riga_off_ven, " + &
			"anno_commessa, " + &
			"num_commessa) "
		else
			ls_sql += ") "
		end if
		
		ls_sql +=" VALUES ( '" + s_cs_xx.cod_azienda + "', " + &
					string(fl_anno_documento) + ", " + &
					string(fl_num_documento) + ", " + &
					string(str_conf_prodotto.ultima_riga_inserita) + ", " + &
					ls_cod_prodotto + ", " + &
					"'" + fstr_optional[ll_i].cod_tipo_det_ven + "', " + &
					ls_cod_misura + ", " + &
					"'" + f_cazzillo(fstr_optional[ll_i].des_prodotto) + "', " + &
					f_double_to_string(fstr_optional[ll_i].quan_vendita) + ", " + &
					f_double_to_string(fstr_optional[ll_i].prezzo_vendita) + ", " + &
					f_double_to_string(ld_fat_conversione) + ", " + &
					f_double_to_string(fstr_optional[ll_i].sconto_1) + ", " + &
					f_double_to_string(fstr_optional[ll_i].sconto_2) + ", " + &
					f_double_to_string(fstr_optional[ll_i].provvigione_1) + ", " + &
					f_double_to_string(fstr_optional[ll_i].provvigione_2) + ", " + &
					"'" + ls_iva + "', " + &
					ls_data_consegna + ", " + &
					f_double_to_string(ld_valore_riga) + ", " + &
					f_double_to_string(ld_valore_riga) + ", " + &
					ls_note + ", " + &
					"null, " + &
					f_double_to_string(fstr_optional[ll_i].sconto_3) + ", " + &
					f_double_to_string(fstr_optional[ll_i].sconto_4) + ", " + &
					f_double_to_string(fstr_optional[ll_i].sconto_5) + ", " + &
					f_double_to_string(fstr_optional[ll_i].sconto_6) + ", " + &
					f_double_to_string(fstr_optional[ll_i].sconto_7) + ", " + &
					f_double_to_string(fstr_optional[ll_i].sconto_8) + ", " + &
					f_double_to_string(fstr_optional[ll_i].sconto_9) + ", " + &
					f_double_to_string(fstr_optional[ll_i].sconto_10) + ", " + &
					ls_cod_versione  + ", " + &
					"'N', " + &
					"'N', " + &
					"0, " + &
					"0, " + &
					string(fl_prog_riga_documento) + ", " + &
					"'" + ls_flag_gen_commessa + "', " + &
					"'"+ ls_flag_doc_suc_or +"', " + &
					"'" + ls_flag_st_note_or + "', " + &
					f_double_to_string(ld_quantita_um) + ", " + &
					f_double_to_string(ld_prezzo_um) + ", " + &
					g_str.replace(  string(fstr_optional[ll_i].dim_x), ",", ".")	 + ", " + &
					g_str.replace(  string(fstr_optional[ll_i].dim_y), ",", ".")			
					
		if fs_tipo_gestione = "ORD_VEN" then
			ls_sql += ", " + &
					"0, " + &
					"0, " + &
					"'A', " + &
					"'N', " + &
					"null, " + &
					"null, " + &
					"null, " + &
					"null, " + &
					"null) "
		else
			ls_sql += ") "
		end if
					
		execute immediate :ls_sql;
		if sqlca.sqlcode = -1 then
			if isnull(fstr_optional[ll_i].cod_prodotto) then fstr_optional[ll_i].cod_prodotto = ""
			if isnull(fstr_optional[ll_i].des_prodotto) then fstr_optional[ll_i].des_prodotto = ""
			g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento dell'optional "+ fstr_optional[ll_i].cod_prodotto + "-" + fstr_optional[ll_i].des_prodotto+". Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
			rollback;
			return -1
		end if
		
		// 09.03.2020 EnMe: inserisco i valori delle variabili nella tabella det_ord_ven_conf_variabili
		// composizione sql per tabelle variabili OPTIONALS
		
		select V.cod_variabile
		into	:ls_nome_var_dim_x
		from 	tab_des_variabili V
		join 	tab_variabili_formule F on V.cod_azienda = F.cod_azienda and V.cod_variabile = F.cod_variabile 
		where cod_prodotto =:fstr_optional[ll_i].cod_prodotto and nome_campo_database = 'dim_x';
		
		if sqlca.sqlcode = 0 then
			ls_sql = " INSERT INTO " + ls_tabella_conf_var + &
						"  ( cod_azienda, " + &
						"anno_registrazione, " + &
						"num_registrazione, " + &
						ls_progressivo +  ", " + &
						"prog_variabile, " + &
						"cod_variabile, " + &
						"flag_tipo_dato, " + &
						"valore_stringa, " + &
						"valore_numero, " + &
						"valore_datetime, " + &
						"des_valore, " + &
						"prog_ordinamento, " + &
						"flag_visible_in_stampa ) "  + &
						" VALUES ( '" + s_cs_xx.cod_azienda + "', " + &
						string(fl_anno_documento) + ", " + &
						string(fl_num_documento) + ", " + &
						string(str_conf_prodotto.ultima_riga_inserita) + ", " + &
						"1000 ," + &
						"'"+ls_nome_var_dim_x+"', " + &
						"'N', " + &
						"null, " + &
						f_double_to_string(fstr_optional[ll_i].dim_x) + ", " + &
						"null, " + &
						"null, " + &
						"0, " + &
						"'S' ) " 
						
			execute immediate :ls_sql;
			if sqlca.sqlcode = -1 then
				if isnull(fstr_optional[ll_i].cod_prodotto) then fstr_optional[ll_i].cod_prodotto = ""
				if isnull(fstr_optional[ll_i].des_prodotto) then fstr_optional[ll_i].des_prodotto = ""
				g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento dell'optional "+ fstr_optional[ll_i].cod_prodotto + "-" + fstr_optional[ll_i].des_prodotto+". Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
				rollback;
				return -1
			end if
			lb_found=true
		end if
		
		select V.cod_variabile
		into	:ls_nome_var_dim_y
		from 	tab_des_variabili V
		join 	tab_variabili_formule F on V.cod_azienda = F.cod_azienda and V.cod_variabile = F.cod_variabile 
		where cod_prodotto =:fstr_optional[ll_i].cod_prodotto and nome_campo_database = 'dim_y';
		
		if sqlca.sqlcode = 0 then
			ls_sql = " INSERT INTO " + ls_tabella_conf_var + &
						"  ( cod_azienda, " + &
						"anno_registrazione, " + &
						"num_registrazione, " + &
						ls_progressivo +  ", " + &
						"prog_variabile, " + &
						"cod_variabile, " + &
						"flag_tipo_dato, " + &
						"valore_stringa, " + &
						"valore_numero, " + &
						"valore_datetime, " + &
						"des_valore, " + &
						"prog_ordinamento, " + &
						"flag_visible_in_stampa )" 
			
			ls_sql +=" VALUES ( '" + s_cs_xx.cod_azienda + "', " + &
						string(fl_anno_documento) + ", " + &
						string(fl_num_documento) + ", " + &
						string(str_conf_prodotto.ultima_riga_inserita) + ", " + &
						"1001 ," + &
						"'"+ls_nome_var_dim_y+"', " + &
						"'N', " + &
						"null, " + &
						f_double_to_string(fstr_optional[ll_i].dim_y) + ", " + &
						"null, " + &
						"null, " + &
						"0, " + &
						"'S' ) " 
	
			execute immediate :ls_sql;
			if sqlca.sqlcode = -1 then
				if isnull(fstr_optional[ll_i].cod_prodotto) then fstr_optional[ll_i].cod_prodotto = ""
				if isnull(fstr_optional[ll_i].des_prodotto) then fstr_optional[ll_i].des_prodotto = ""
				g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento dell'optional "+ fstr_optional[ll_i].cod_prodotto + "-" + fstr_optional[ll_i].des_prodotto+". Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
				rollback;
				return -1
			end if
			lb_found=true
		end if
		
		// inizio ciclo di scorrimento della distinta dell'optional e anche della valorizzazione delle sue formule
		// ovviamente lo faccio solo se c'è una versione il che significa che ha una distinta base
		if not isnull(ls_cod_versione_formula) and lb_found then
			
			ls_sql = " INSERT INTO " + ls_tabella_conf_var + &
						"  ( cod_azienda, " + &
						"anno_registrazione, " + &
						"num_registrazione, " + &
						ls_progressivo +  ", " + &
						"prog_variabile, " + &
						"cod_variabile, " + &
						"flag_tipo_dato, " + &
						"valore_stringa, " + &
						"valore_numero, " + &
						"valore_datetime, " + &
						"des_valore, " + &
						"prog_ordinamento, " + &
						"flag_visible_in_stampa ) " + &
						"select " + &
						"cod_azienda, " + &
						"anno_registrazione, " + &
						"num_registrazione, " + &
						string(str_conf_prodotto.ultima_riga_inserita) +  ", " + &
						"prog_variabile, " + &
						"cod_variabile, " + &
						"flag_tipo_dato, " + &
						"valore_stringa, " + &
						"valore_numero, " + &
						"valore_datetime, " + &
						"des_valore, " + &
						"prog_ordinamento, " + &
						"flag_visible_in_stampa " + &
						"from " + ls_tabella_conf_var + &
						g_str.format(" where cod_azienda = '$1' and anno_registrazione = $2 and num_registrazione = $3 and prog_riga_ord_ven = $4 and cod_variabile not in ('DIM_1','DIM_2') ",s_cs_xx.cod_azienda, fl_anno_documento, fl_num_documento, fl_prog_riga_documento)
			
			execute immediate :ls_sql;
			if sqlca.sqlcode = -1 then
				if isnull(fstr_optional[ll_i].cod_prodotto) then fstr_optional[ll_i].cod_prodotto = ""
				if isnull(fstr_optional[ll_i].des_prodotto) then fstr_optional[ll_i].des_prodotto = ""
				g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento variabili dell'optional "+ fstr_optional[ll_i].cod_prodotto + "-" + fstr_optional[ll_i].des_prodotto+". Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
				rollback;
				return -1
			end if
			
			uo_conf_varianti luo_varianti
			luo_varianti = create uo_conf_varianti
			
			luo_varianti.il_anno_registrazione = fl_anno_documento
			luo_varianti.il_num_registrazione = fl_num_documento
			luo_varianti.il_prog_riga = str_conf_prodotto.ultima_riga_inserita
			luo_varianti.is_gestione = fs_tipo_gestione
			
			select cod_versione
			into   :ls_cod_versione
			from   distinta_padri
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :fstr_optional[ll_i].cod_prodotto and
					 flag_predefinita = 'S';

			ll_ret = luo_varianti.uof_valorizza_formule( fstr_optional[ll_i].cod_prodotto, ls_cod_versione, ref ls_errore)
			
			if ll_ret = -1 then
				fs_messaggio = g_str.format("Errore valoriz. formule optional $1 (uof_valorizza_formule): $2", ls_cod_prodotto,ls_errore)
				return -1				
			end if
			destroy luo_varianti
			
		end if

end if
next

return 0
end function

public function integer uof_leggi_optional (string fs_tipo_gestione, long fl_anno_documento, long fl_num_documento, long fl_prog_riga_documento, ref str_optional fstr_optional[], ref string fs_messaggio);// -----------------------------------------------------------------------------------------------------------
//                 funzione di caricamento optional da dettaglio documento 
//
//
// variabile 					tipo		passaggio	descrizione
// ---------------------------------------------------------------------------------	
// fs_tipo_gestione			string	val			tipo gestione su cui sto lavorando
// fl_anno_documento 		long		val			anno documento di riferimento
// fl_num_documento			long		val			numero del documento
// fl_prog_riga_documento	long		val			progressivo riga del prodotto finito
// fstr_ripeologo				struttura ref			struttura dove vengono caricati gli optional
//
//
//	autore: Enrico Menegotto   06/10/2000
//
// -----------------------------------------------------------------------------------------------------------
string ls_sql, ls_cod_prodotto, ls_cod_tipo_det_ven, ls_des_prodotto, ls_nota_dettaglio, ls_cod_misura, ls_cod_iva, &
       	ls_cod_tipo_det_ven_add,ls_cod_tipo_listino_prodotto,ls_cod_valuta,ls_cod_cliente,ls_cod_agente_1,ls_cod_agente_2, &
		ls_tabella_det, ls_progressivo,ls_quantita, ls_cod_tipo_ord_ven, ls_flag_prezzo_bloccato, ls_cod_cat_mer_optionals_config, ls_cod_cat_mer, &
		ls_cod_tipo_det_ven_addizionali[], ls_sql_ds
		 
long   ll_i, ll_prog_riga, ll_ind=0, ll_rows

dec{4} 	ld_quan_vendita, ld_sconto_1, ld_sconto_2, ld_prezzo_vendita, ld_provvigione_1, ld_provvigione_2, &
       		ld_sconto_3, ld_sconto_4,ld_sconto_5, ld_sconto_6,ld_sconto_7, ld_sconto_8,ld_sconto_9, ld_sconto_10, &
			ld_dim_x, ld_dim_y
		 
datetime ldt_data_consegna,ldt_data_registrazione,ldt_data_riferimento
uo_condizioni_cliente luo_condizioni_cliente
datastore lds_data
declare cu_optional dynamic cursor for sqlsa;


setnull(ls_cod_tipo_det_ven_add)

choose case fs_tipo_gestione
	case "ORD_VEN"
		
		ls_tabella_det = "det_ord_ven"
		ls_progressivo = "prog_riga_ord_ven"
		ls_quantita  	= " quan_ordine"

		select  cod_tipo_ord_ven, 
				 cod_tipo_listino_prodotto,
				 cod_valuta,
				 data_consegna,
				 data_registrazione,
				 cod_cliente,
				 cod_agente_1,
				 cod_agente_2
		into   	 :ls_cod_tipo_ord_ven,  
				 :ls_cod_tipo_listino_prodotto,
				 :ls_cod_valuta,
				 :ldt_data_consegna,
				 :ldt_data_registrazione,
				 :ls_cod_cliente,
				 :ls_cod_agente_1,
				 :ls_cod_agente_2
		from   tes_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :fl_anno_documento and
				 num_registrazione = :fl_num_documento;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca  testata " + fs_tipo_gestione + ". Ordine = " + string(fl_anno_documento) + "/" + string(fl_num_documento) + "~r~nDettaglio errore = " + sqlca.sqlerrtext
			return -1
		end if

		ldt_data_riferimento = ldt_data_consegna
		// se non c'è data consegna prendo per buona la data di registrazione
		if isnull(ldt_data_consegna) or ldt_data_consegna <= datetime(date("01/01/1900"),00:00:00) then
			ldt_data_riferimento = ldt_data_registrazione
		end if
		
//		select 	cod_tipo_det_ven_addizionali
//		into   		:ls_cod_tipo_det_ven_add
//		from   	tab_flags_conf_tipi_ord_ven
//		where  	cod_azienda = :s_cs_xx.cod_azienda and
//				 	cod_modello = :str_conf_prodotto.cod_modello and
//					cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
//					
//		if sqlca.sqlcode <> 0 then
//			setnull(ls_cod_tipo_det_ven_add)
//		end if
		

case "OFF_VEN"

		ls_tabella_det = "det_off_ven"
		ls_progressivo = "prog_riga_off_ven"
		ls_quantita    = "quan_offerta"

		select cod_tipo_listino_prodotto,
				 cod_valuta,
				 data_consegna,
				 data_registrazione,
				 cod_cliente,
				 cod_agente_1,
				 cod_agente_2
		into   :ls_cod_tipo_listino_prodotto,
				 :ls_cod_valuta,
				 :ldt_data_consegna,
				 :ldt_data_registrazione,
				 :ls_cod_cliente,
				 :ls_cod_agente_1,
				 :ls_cod_agente_2
		from   tes_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :fl_anno_documento and
				 num_registrazione = :fl_num_documento;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca  testata " + fs_tipo_gestione + ". Offerta = " + string(fl_anno_documento) + "/" + string(fl_num_documento) + "~r~nDettaglio errore = " + sqlca.sqlerrtext
			return -1
		end if

		ldt_data_riferimento = ldt_data_consegna
		// se non c'è data consegna prendo per buona la data di registrazione
		if isnull(ldt_data_consegna) or ldt_data_consegna <= datetime(date("01/01/1900"),00:00:00) then
			ldt_data_riferimento = ldt_data_registrazione
		end if

end choose

//if isnull(ls_cod_tipo_det_ven_add) then
	
select 	cod_tipo_det_ven_addizionali
into   	:ls_cod_tipo_det_ven_add
from  	tab_flags_configuratore
where  	cod_azienda = :s_cs_xx.cod_azienda and
			cod_modello = :str_conf_prodotto.cod_modello;
		 
if sqlca.sqlcode = 100 then
	fs_messaggio = "Non esistono i parametri configuratore per il modello prodotto " + str_conf_prodotto.cod_modello
	return -1
elseif sqlca.sqlcode = -1 then
	fs_messaggio = "Errore grave in lettura parametri configuratore " + sqlca.sqlerrtext
	return -1
end if

if not isnull(ls_cod_tipo_det_ven_add) then
	ll_ind ++
	ls_cod_tipo_det_ven_addizionali[ll_ind] = ls_cod_tipo_det_ven_add
end if

ls_sql_ds = "select distinct cod_tipo_det_ven_addizionali from tab_flags_conf_tipi_ord_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_modello = '" + str_conf_prodotto.cod_modello + "' "

ll_rows = guo_functions.uof_crea_datastore( lds_data, ls_sql_ds)

for ll_i = 1 to ll_rows
	if not isnull( lds_data.getitemstring(ll_i,1) ) then
		ll_ind ++
		ls_cod_tipo_det_ven_addizionali[ll_ind] = lds_data.getitemstring(ll_i,1)
	end if
next

ls_cod_tipo_det_ven_add = "'" + g_str.implode( ls_cod_tipo_det_ven_addizionali[] , "','") + "'"
//end if


guo_functions.uof_get_parametro_azienda( "OCF", ls_cod_cat_mer_optionals_config)

if isnull(ls_cod_cat_mer_optionals_config) then ls_cod_cat_mer_optionals_config = ""


ll_i = 0
ls_sql = "SELECT " + ls_progressivo+ ", cod_prodotto, cod_tipo_det_ven, des_prodotto, " + ls_quantita + ", prezzo_vendita, sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, provvigione_1, provvigione_2, cod_iva, nota_dettaglio, dim_x, dim_y, flag_prezzo_bloccato " + & 
			"FROM " + ls_tabella_det + "  " + &
			"WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' AND " + &
			" anno_registrazione = " + string(fl_anno_documento) + " AND  " + &
			" num_registrazione = " +  string(fl_num_documento) + " AND  " + &
			" num_riga_appartenenza = " +  string(fl_prog_riga_documento) + " AND " + &
			" cod_tipo_det_ven not in (" + ls_cod_tipo_det_ven_add + ") "
prepare sqlsa from :ls_sql;
	

open dynamic cu_optional;
if sqlca.sqlcode = -1 then
	fs_messaggio = "Errore in OPEN cursore di lettura optional dalla gestione " + fs_tipo_gestione + ". Dettaglio errore = " + sqlca.sqlerrtext
	return -1
end if

do while  true
	fetch cu_optional into :ll_prog_riga, :ls_cod_prodotto, :ls_cod_tipo_det_ven, :ls_des_prodotto, :ld_quan_vendita, :ld_prezzo_vendita, :ld_sconto_1, :ld_sconto_2, :ld_sconto_3, :ld_sconto_4, :ld_sconto_5, :ld_sconto_6, :ld_sconto_7, :ld_sconto_8, :ld_sconto_9, :ld_sconto_10, :ld_provvigione_1, :ld_provvigione_2, :ls_cod_iva, :ls_nota_dettaglio, :ld_dim_x, :ld_dim_y, :ls_flag_prezzo_bloccato;
	if sqlca.sqlcode = -1 then
		fs_messaggio = "Errore in lettura optional dalla gestione " + fs_tipo_gestione + ". Dettaglio errore = " + sqlca.sqlerrtext
		return -1
	end if
	if sqlca.sqlcode = 100 then exit
	ll_i ++
	
	if isnull(ld_dim_x) then ld_dim_x = 0
	if isnull(ld_dim_y) then ld_dim_y = 0
	fstr_optional[ll_i].cod_tipo_det_ven = ls_cod_tipo_det_ven
	fstr_optional[ll_i].cod_prodotto = ls_cod_prodotto
	fstr_optional[ll_i].des_prodotto = ls_des_prodotto
	fstr_optional[ll_i].quan_vendita = ld_quan_vendita
	fstr_optional[ll_i].sconto_1 = ld_sconto_1
	fstr_optional[ll_i].sconto_2 = ld_sconto_2
	fstr_optional[ll_i].sconto_3 = ld_sconto_3
	fstr_optional[ll_i].sconto_4 = ld_sconto_4
	fstr_optional[ll_i].sconto_5 = ld_sconto_5
	fstr_optional[ll_i].sconto_6 = ld_sconto_6
	fstr_optional[ll_i].sconto_7 = ld_sconto_7
	fstr_optional[ll_i].sconto_8 = ld_sconto_8
	fstr_optional[ll_i].sconto_9 = ld_sconto_9
	fstr_optional[ll_i].sconto_10 = ld_sconto_10
	fstr_optional[ll_i].provvigione_1 = ld_provvigione_1
	fstr_optional[ll_i].provvigione_2 = ld_provvigione_2
	fstr_optional[ll_i].prezzo_vendita = ld_prezzo_vendita
	fstr_optional[ll_i].note = ls_nota_dettaglio
	fstr_optional[ll_i].cod_iva = ls_cod_iva
	fstr_optional[ll_i].dim_x = ld_dim_x
	fstr_optional[ll_i].dim_y = ld_dim_y
	setnull(ls_cod_misura)
	
	if not isnull(ls_cod_prodotto) then
		
		select 	cod_misura_mag, 
					cod_iva,
					cod_cat_mer
		into   		:ls_cod_misura, 
					:ls_cod_iva,
					:ls_cod_cat_mer
		from   	anag_prodotti
		where  	cod_azienda = :s_cs_xx.cod_azienda and
		       		cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in lettura anagrafica prodotti (uof_leggi_optional) " + fs_tipo_gestione + ". Dettaglio errore = " + sqlca.sqlerrtext
			return -1
		end if
		if not isnull(ls_cod_iva) then fstr_optional[ll_i].cod_iva = ls_cod_iva
		fstr_optional[ll_i].cod_misura = ls_cod_misura
		
		if ls_cod_cat_mer = ls_cod_cat_mer_optionals_config then
			fstr_optional[ll_i].flag_dimensioni = "S"
		else
			fstr_optional[ll_i].flag_dimensioni = "N"
		end if
	end if
	
	// ******************** 26/5/2003 :: chiesto da Marco Mallozzi e Antonella Bellin **********************************
	//	eventuali provvigioni scritte a mano nella riga d'ordine devono essere sovrascritte dalle condizioni
	// impostate nei listini; il prezzo invece resta invariato.
	//
	//  EnMe 20/09/2012: Marco Brunazzetto aggiunge eccezione; la provvigione va sovrascritta solo se il prezzo non è bloccato.
	if ls_flag_prezzo_bloccato <> "S" then
		luo_condizioni_cliente = CREATE uo_condizioni_cliente
		// meglio fare la create ogni giro perchè così si puliscono le variabili di istanza 
		
		luo_condizioni_cliente.str_parametri.ldw_oggetto = pcca.null_object
		luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
		luo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
		luo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_riferimento
		luo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
		luo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
		luo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
		luo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
		luo_condizioni_cliente.str_parametri.quantita = ld_quan_vendita
		luo_condizioni_cliente.str_parametri.valore = 0
		luo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
		luo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
		luo_condizioni_cliente.str_parametri.colonna_quantita = ""
		luo_condizioni_cliente.str_parametri.colonna_prezzo = ""
		luo_condizioni_cliente.ib_setitem = false
		luo_condizioni_cliente.ib_setitem_provvigioni = false
		luo_condizioni_cliente.wf_condizioni_cliente()
		
		fstr_optional[ll_i].provvigione_1 = luo_condizioni_cliente.str_output.provvigione_1
		fstr_optional[ll_i].provvigione_2 = luo_condizioni_cliente.str_output.provvigione_2
		
		destroy luo_condizioni_cliente
	end if
	
	// ************************************ fine modifica **************************************************************	
loop

close cu_optional;
return 0

end function

on uo_optional.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_optional.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

