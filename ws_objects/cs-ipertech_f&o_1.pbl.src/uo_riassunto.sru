﻿$PBExportHeader$uo_riassunto.sru
$PBExportComments$UO aggiornamento riassunto
forward
global type uo_riassunto from nonvisualobject
end type
end forward

global type uo_riassunto from nonvisualobject
end type
global uo_riassunto uo_riassunto

type variables

end variables

forward prototypes
public function string uof_riassunto (datawindow fdw_dw_inizio, datawindow fdw_fine)
public function integer uof_riassunto_formule (uo_conf_varianti auo_conf_varianti, ref datawindow adw_fine)
end prototypes

public function string uof_riassunto (datawindow fdw_dw_inizio, datawindow fdw_fine);//	Funzione che mi compone la stringa di riassunto visualizzata sulla DW di Fine estraendo
//		il modello e le dimensioni dalla DW inizio e lanciando la funzione wf_riassunto_passo
//		per le altre DW
//
// nome: wf_riassunto
// tipo: none
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							
//		Creata il 31-08-98 
//		Autore Mauro Bordin (PAJASSO)
//		
//    modificata il 31/08/1998 ENRICO
//		modificato il 17/11/99   ENRICO

string ls_riassunto, ls_descrizione
long   ll_i, ll_num_mp

/// Modello ///
if not isnull(str_conf_prodotto.cod_modello) and str_conf_prodotto.cod_modello <> "" then
	select des_prodotto
	 into :ls_descrizione
	 from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :str_conf_prodotto.cod_modello;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore di Prodotto", "Errore nell'Estrazione del Modello")
		return ""
	end if
end if
long ll_riga
fdw_fine.reset()
fdw_fine.SetRowFocusIndicator (off!)

ll_riga = fdw_fine.insertrow(0)
fdw_fine.setitem(ll_riga,"rs_testata","MODELLO DI PRODOTTO")
ls_riassunto = str_conf_prodotto.cod_modello + " - " + ls_descrizione
ll_riga = fdw_fine.insertrow(0)
fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)

		// versione della distnita
ls_riassunto = fdw_dw_inizio.getitemstring(1, "rs_cod_versione")
select des_versione
into   :ls_descrizione
from   distinta_padri
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :str_conf_prodotto.cod_modello and
		 cod_versione = :ls_riassunto;
if sqlca.sqlcode <> 0 then
	ls_descrizione = "Versione inesistente: verificare !!!"
end if
if isnull(ls_descrizione) then ls_descrizione = ""
ls_riassunto = "Versione Prodotto: " + ls_riassunto + " - " + ls_descrizione
ll_riga = fdw_fine.insertrow(0)
fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)

		// quantità di prodotto finito
if int(fdw_dw_inizio.getitemnumber(1, "rn_quan_vendita")) <> fdw_dw_inizio.getitemnumber(1, "rn_quan_vendita") then
	ls_riassunto = "Quantità Ordine: " + string(fdw_dw_inizio.getitemnumber(1, "rn_quan_vendita"),"##,###,##0.0###")
else
	ls_riassunto = "Quantità Ordine: " + string(fdw_dw_inizio.getitemnumber(1, "rn_quan_vendita"),"##,###,##0")
end if
ll_riga = fdw_fine.insertrow(0)
fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)

ll_riga = fdw_fine.insertrow(0)
fdw_fine.setitem(ll_riga,"rs_testata","DIMENSIONI DEL PRODOTTO")
LS_RIASSUNTO = ""
if fdw_dw_inizio.object.rd_dimensione_1_t.text <> "Dimensione 1:" then
	ls_riassunto = ls_riassunto + fdw_dw_inizio.object.rd_dimensione_1_t.text + " " + string(fdw_dw_inizio.getitemnumber(1, "rd_dimensione_1")) + "; "
end if
if fdw_dw_inizio.object.rd_dimensione_2_t.text <> "Dimensione 2:" then
	ls_riassunto = ls_riassunto + fdw_dw_inizio.object.rd_dimensione_2_t.text + " " + string(fdw_dw_inizio.getitemnumber(1, "rd_dimensione_2")) + "; "
end if
if fdw_dw_inizio.object.rd_dimensione_3_t.text <> "Dimensione 3:" then
	ls_riassunto = ls_riassunto + fdw_dw_inizio.object.rd_dimensione_3_t.text + " " + string(fdw_dw_inizio.getitemnumber(1, "rd_dimensione_3")) + "; "
end if
if fdw_dw_inizio.object.rd_dimensione_4_t.text <> "Dimensione 4:" then
	ls_riassunto = ls_riassunto + fdw_dw_inizio.object.rd_dimensione_4_t.text + " " + string(fdw_dw_inizio.getitemnumber(1, "rd_dimensione_4")) + "; "
end if
if fdw_dw_inizio.object.rd_dimensione_5_t.text <> "Dimensione 5:" then
	ls_riassunto = ls_riassunto + fdw_dw_inizio.object.rd_dimensione_5_t.text + " " + string(fdw_dw_inizio.getitemnumber(1, "rd_dimensione_5")) + "; "
end if
if fdw_dw_inizio.object.rd_dimensione_6_t.text <> "Dimensione 6:" then
	ls_riassunto = ls_riassunto + fdw_dw_inizio.object.rd_dimensione_6_t.text + " " + string(fdw_dw_inizio.getitemnumber(1, "rd_dimensione_6")) + "; "
end if
if fdw_dw_inizio.object.rd_dimensione_7_t.text <> "Dimensione 7:" then
	ls_riassunto = ls_riassunto + fdw_dw_inizio.object.rd_dimensione_7_t.text + " " + string(fdw_dw_inizio.getitemnumber(1, "rd_dimensione_7")) + "; "
end if
if fdw_dw_inizio.object.rd_dimensione_8_t.text <> "Dimensione 8:" then
	ls_riassunto = ls_riassunto + fdw_dw_inizio.object.rd_dimensione_8_t.text + " " + string(fdw_dw_inizio.getitemnumber(1, "rd_dimensione_8")) + "; "
end if
if fdw_dw_inizio.object.rd_dimensione_9_t.text <> "Dimensione 9:" then
	ls_riassunto = ls_riassunto + fdw_dw_inizio.object.rd_dimensione_9_t.text + " " + string(fdw_dw_inizio.getitemnumber(1, "rd_dimensione_9")) + "; "
end if
ll_riga = fdw_fine.insertrow(0)
fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)

/// Flag Allegati //-------------
if w_configuratore.istr_riepilogo.flag_allegati = "S" then
	ll_riga = fdw_fine.insertrow(0)
	fdw_fine.setitem(ll_riga,"rs_dettaglio","PRESENZA DI ALLEGATI TECNICO-DESCRITTIVI ALL'ORDINE")
end if

/// Flag Misura Luce o Finita ///
if w_configuratore.istr_flags.flag_luce_finita = "S" then
	if not isnull(w_configuratore.istr_riepilogo.flag_misura_luce_finita) and w_configuratore.istr_riepilogo.flag_misura_luce_finita <> "" then
		if w_configuratore.istr_riepilogo.flag_misura_luce_finita = "L" then 
			ls_riassunto = "Misura: Luce"
		elseif w_configuratore.istr_riepilogo.flag_misura_luce_finita = "F" then 
			ls_riassunto = "Misura: Finita"
		end if
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if

/// Tessuto ///
if w_configuratore.istr_flags.flag_tessuto = "S" then
	ll_riga = fdw_fine.insertrow(0)
	fdw_fine.setitem(ll_riga,"rs_testata","CONFIGURAZIONE TESSUTO")
	if not isnull(w_configuratore.istr_riepilogo.cod_tessuto) and w_configuratore.istr_riepilogo.cod_tessuto <> "" then
		select des_prodotto
		 into :ls_descrizione
		 from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :w_configuratore.istr_riepilogo.cod_tessuto;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore nell'Estrazione Tessuto")
			return ""
		end if
		ls_riassunto = w_configuratore.istr_riepilogo.cod_tessuto + " - " + ls_descrizione
		if not isnull(w_configuratore.istr_riepilogo.cod_non_a_magazzino) then
			ls_riassunto = ls_riassunto + " TESSUTO:" + w_configuratore.istr_riepilogo.cod_non_a_magazzino
		end if
		if w_configuratore.istr_riepilogo.flag_addizionale_tessuto = "S" then ls_riassunto = ls_riassunto +" (ADDIZIONALE)"
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if

if w_configuratore.istr_flags.flag_tessuto_mantovana = "S" then
	ll_riga = fdw_fine.insertrow(0)
	fdw_fine.setitem(ll_riga,"rs_testata","MANTOVANA")
	if not isnull(w_configuratore.istr_riepilogo.cod_tessuto_mant) and w_configuratore.istr_riepilogo.cod_tessuto_mant <> "" then
		select des_prodotto
		 into :ls_descrizione
		 from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :w_configuratore.istr_riepilogo.cod_tessuto_mant;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore nell'Estrazione Tessuto della Mantovana")
			return ""
		end if
		ls_riassunto = w_configuratore.istr_riepilogo.cod_tessuto_mant + " - " + ls_descrizione
		if not isnull(w_configuratore.istr_riepilogo.cod_mant_non_a_magazzino) then
			ls_riassunto = ls_riassunto + " TESSUTO:" + w_configuratore.istr_riepilogo.cod_mant_non_a_magazzino
		end if
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if

if w_configuratore.istr_flags.flag_tipo_mantovana = "S" then
	if not isnull(w_configuratore.istr_riepilogo.flag_tipo_mantovana) and w_configuratore.istr_riepilogo.flag_tipo_mantovana <> "" then
		ls_riassunto = "Mantovana di tipo: " + w_configuratore.istr_riepilogo.flag_tipo_mantovana
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if

if w_configuratore.istr_flags.flag_altezza_mantovana = "S" then
	if not isnull(w_configuratore.istr_riepilogo.alt_mantovana) and w_configuratore.istr_riepilogo.alt_mantovana <> 0 then
		ls_riassunto = "Altezza Mantovana: " + string(w_configuratore.istr_riepilogo.alt_mantovana)
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if

if not isnull(w_configuratore.istr_riepilogo.colore_cordolo) and w_configuratore.istr_riepilogo.colore_cordolo <> "" then
	ls_riassunto = "Colore Cordolo: " + w_configuratore.istr_riepilogo.colore_cordolo
	ll_riga = fdw_fine.insertrow(0)
	fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
end if

if not isnull(w_configuratore.istr_riepilogo.colore_passamaneria) and w_configuratore.istr_riepilogo.colore_passamaneria <> "" then
	ls_riassunto = "Colore Passamaneria:" + w_configuratore.istr_riepilogo.colore_passamaneria
	ll_riga = fdw_fine.insertrow(0)
	fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
end if

/// Verniciatura ///
if w_configuratore.istr_flags.flag_verniciatura = "S" then
	ll_riga = fdw_fine.insertrow(0)
	fdw_fine.setitem(ll_riga,"rs_testata","CONFIGURAZIONE VERNICIATURE")
	if not isnull(w_configuratore.istr_riepilogo.cod_verniciatura) and w_configuratore.istr_riepilogo.cod_verniciatura <> "" then
		select des_prodotto
		 into :ls_descrizione
		 from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :w_configuratore.istr_riepilogo.cod_verniciatura;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore nell'Estrazione Verniciatura")
			return ""
		end if
		ls_riassunto = "Vernice: " + w_configuratore.istr_riepilogo.cod_verniciatura + " - " + ls_descrizione 
		if w_configuratore.istr_riepilogo.flag_addizionale_vernic = "S" then ls_riassunto = ls_riassunto +" (ADDIZIONALE)"
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if

/// Comando ///
if w_configuratore.istr_flags.flag_comando = "S" then
	ll_riga = fdw_fine.insertrow(0)
	fdw_fine.setitem(ll_riga,"rs_testata","CONFIGURAZIONE COMANDI")
	if not isnull(w_configuratore.istr_riepilogo.cod_comando) and w_configuratore.istr_riepilogo.cod_comando <> "" then
		select des_prodotto
		 into :ls_descrizione
		 from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :w_configuratore.istr_riepilogo.cod_comando;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore nell'Estrazione Comando")
			return ""
		end if
		ls_riassunto = "Tipo comando: " + w_configuratore.istr_riepilogo.cod_comando + " - " + ls_descrizione
		if w_configuratore.istr_riepilogo.flag_addizionale_comando_1 = "S" then ls_riassunto = ls_riassunto +" (ADDIZIONALE)"
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if
if w_configuratore.istr_flags.flag_pos_comando = "S" then
	if not isnull(w_configuratore.istr_riepilogo.pos_comando) and w_configuratore.istr_riepilogo.pos_comando <> "" then 
		ls_riassunto = "Posizione Comando: " + w_configuratore.istr_riepilogo.pos_comando
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if
if w_configuratore.istr_flags.flag_alt_asta = "S" then
	if not isnull(w_configuratore.istr_riepilogo.alt_asta) and w_configuratore.istr_riepilogo.alt_asta <> 0 then 
		ls_riassunto = "Altezza Comando: " + string(w_configuratore.istr_riepilogo.alt_asta)
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if

// secondo comando
if w_configuratore.istr_flags.flag_secondo_comando = "S" then
	ll_riga = fdw_fine.insertrow(0)
	fdw_fine.setitem(ll_riga,"rs_testata","CONFIGURAZIONE SECONDO COMANDO")
	if not isnull(w_configuratore.istr_riepilogo.cod_comando_2) and w_configuratore.istr_riepilogo.cod_comando_2 <> "" then
		select des_prodotto
		 into :ls_descrizione
		 from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :w_configuratore.istr_riepilogo.cod_comando_2;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore nell'Estrazione del Comando secondario")
			return ""
		end if
		ls_riassunto = "Secondo Comando: " + w_configuratore.istr_riepilogo.cod_comando_2 + " - " + ls_descrizione
		if w_configuratore.istr_riepilogo.flag_addizionale_comando_2 = "S" then ls_riassunto = ls_riassunto +" (ADDIZIONALE)"
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if

/// Colore Lastra ///
if w_configuratore.istr_flags.flag_colore_lastra = "S" then
	ll_riga = fdw_fine.insertrow(0)
	fdw_fine.setitem(ll_riga,"rs_testata","CONFIGURAZIONE LASTRE")
	if not isnull(w_configuratore.istr_riepilogo.cod_colore_lastra) and w_configuratore.istr_riepilogo.cod_colore_lastra <> "" then
		select des_prodotto
		 into :ls_descrizione
		 from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :w_configuratore.istr_riepilogo.cod_colore_lastra;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore nell'Estrazione Colore Lastra")
			return ""
		end if
		ls_riassunto = "Colore Lastra: " + w_configuratore.istr_riepilogo.cod_colore_lastra + " - " + ls_descrizione
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if

/// Tipo di Lastra ///
if w_configuratore.istr_flags.flag_tipo_lastra = "S" then
	if not isnull(w_configuratore.istr_riepilogo.cod_tipo_lastra) and w_configuratore.istr_riepilogo.cod_tipo_lastra <> "" then
		select des_tipo_lastra
		 into :ls_descrizione
		 from tab_tipi_lastre
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_lastra = :w_configuratore.istr_riepilogo.cod_tipo_lastra and
				cod_modello = :str_conf_prodotto.cod_modello;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore nell'Estrazione Tipo Lastra")
			return ""
		end if
		ls_riassunto = "Tipo Lastra: " + w_configuratore.istr_riepilogo.cod_tipo_lastra + " - " + ls_descrizione
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if

/// Spessore Lastra ///
if w_configuratore.istr_flags.flag_spessore_lastra = "S" then
	if not isnull(w_configuratore.istr_riepilogo.spes_lastra) and w_configuratore.istr_riepilogo.spes_lastra <> "" then
		ls_riassunto = "Spessore Lastra: " + w_configuratore.istr_riepilogo.spes_lastra
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if

/// Tipo Supporto ///
if w_configuratore.istr_flags.flag_supporto = "S" then
	ll_riga = fdw_fine.insertrow(0)
	fdw_fine.setitem(ll_riga,"rs_testata","CONFIGURAZIONE SUPPORTI")
	if not isnull(w_configuratore.istr_riepilogo.cod_tipo_supporto) and w_configuratore.istr_riepilogo.cod_tipo_supporto <> "" then
		select des_prodotto
		 into :ls_descrizione
		 from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :w_configuratore.istr_riepilogo.cod_tipo_supporto;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore nell'Estrazione Supporto")
			return ""
		end if
		ls_riassunto = "Supporto: " + w_configuratore.istr_riepilogo.cod_tipo_supporto + " - " + ls_descrizione
		if w_configuratore.istr_riepilogo.flag_addizionale_supporto = "S" then ls_riassunto = ls_riassunto +" (ADDIZIONALE)"
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if

/// Flag Cappotta Frontale ///
if not isnull(w_configuratore.istr_riepilogo.flag_cappotta_frontale) and w_configuratore.istr_riepilogo.flag_cappotta_frontale <> "" then
	if w_configuratore.istr_riepilogo.flag_cappotta_frontale = "S" then
		select des_prodotto
		 into :ls_descrizione
		 from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :w_configuratore.istr_flags.cod_cappotta_frontale;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore nell'Estrazione Cappotta Frontale")
			return ""
		end if
		ls_riassunto = "Cappotta Frontale: " + w_configuratore.istr_flags.cod_cappotta_frontale + " - " + ls_descrizione
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_testata","CONFIGURAZIONE CAPPOTTA FRONTALE")
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if

/// Flag Rollò ///
if not isnull(w_configuratore.istr_riepilogo.flag_rollo) and w_configuratore.istr_riepilogo.flag_rollo <> "" then
	if w_configuratore.istr_riepilogo.flag_rollo = "S" then
		select des_prodotto
		 into :ls_descrizione
		 from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :w_configuratore.istr_flags.cod_rollo;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore nell'Estrazione Rollò")
			return ""
		end if
		ls_riassunto = "Rollò: " + w_configuratore.istr_flags.cod_rollo + " - " + ls_descrizione
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_testata","CONFIGURAZIONE ROLLO'")
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if

end if

/// Numero Gambe ///
if not isnull(w_configuratore.istr_riepilogo.num_gambe) and w_configuratore.istr_riepilogo.num_gambe <> 0 then
	ls_riassunto = "Numero Gambe: " + string(w_configuratore.istr_riepilogo.num_gambe)
	ll_riga = fdw_fine.insertrow(0)
	fdw_fine.setitem(ll_riga,"rs_testata",ls_riassunto)
end if

/// Numero Campate ///
if not isnull(w_configuratore.istr_riepilogo.num_campate) and w_configuratore.istr_riepilogo.num_campate <> 0 then
	ls_riassunto = ls_riassunto + "Numero Campate: " + string(w_configuratore.istr_riepilogo.num_campate)
	ll_riga = fdw_fine.insertrow(0)
	fdw_fine.setitem(ll_riga,"rs_testata",ls_riassunto)
end if

/// Flag Kit Laterale ///
if not isnull(w_configuratore.istr_riepilogo.flag_kit_laterale) and w_configuratore.istr_riepilogo.flag_kit_laterale <> "" then
	if w_configuratore.istr_riepilogo.flag_kit_laterale = "S" then ls_riassunto = ls_riassunto + "Kit Laterale: SI"
	ll_riga = fdw_fine.insertrow(0)
	fdw_fine.setitem(ll_riga,"rs_testata",ls_riassunto)
end if


if w_configuratore.istr_flags.flag_plisse = "S" then
	ll_riga = fdw_fine.insertrow(0)
	fdw_fine.setitem(ll_riga,"rs_testata","CONFIGURAZIONE PLISSE'")
	ls_riassunto = ""
	/// Flag AutoBlocco ///
	if not isnull(w_configuratore.istr_riepilogo.flag_autoblocco) and w_configuratore.istr_riepilogo.flag_autoblocco <> "" then
		if w_configuratore.istr_riepilogo.flag_autoblocco = "S" then
			ls_riassunto = ls_riassunto + "Auto Blocco SI"
		else
			ls_riassunto = ls_riassunto + "Auto Blocco NO"
		end if
	end if
	/// Numero Fili ///
	if not isnull(w_configuratore.istr_riepilogo.num_fili_plisse) and w_configuratore.istr_riepilogo.num_fili_plisse <> 0 then
		ls_riassunto = ls_riassunto + ", Fili: " + string(w_configuratore.istr_riepilogo.num_fili_plisse)
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
	/// Numero Boccole ///
	if not isnull(w_configuratore.istr_riepilogo.num_boccole_plisse) and w_configuratore.istr_riepilogo.num_boccole_plisse <> 0 then
		ls_riassunto = ls_riassunto + ", Boccole: " + string(w_configuratore.istr_riepilogo.num_boccole_plisse)
	end if
	if len(ls_riassunto) > 0 then
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
		ls_riassunto = ""
	end if
	/// Intervallo Punzoni ///
	if not isnull(w_configuratore.istr_riepilogo.intervallo_punzoni_plisse) and w_configuratore.istr_riepilogo.intervallo_punzoni_plisse <> 0 then
		ls_riassunto = ls_riassunto + "Intervallo Punzoni: " + string(w_configuratore.istr_riepilogo.intervallo_punzoni_plisse)
	end if
	/// Numero Pieghe ///
	if not isnull(w_configuratore.istr_riepilogo.num_pieghe_plisse) and w_configuratore.istr_riepilogo.num_pieghe_plisse <> 0 then
		ls_riassunto = ls_riassunto + ", Pieghe: " + string(w_configuratore.istr_riepilogo.num_pieghe_plisse)
	end if
	if len(ls_riassunto) > 0 then
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
		ls_riassunto = ""
	end if
	/// Profilo Inferiore ///
	if not isnull(w_configuratore.istr_riepilogo.prof_inf_plisse) and w_configuratore.istr_riepilogo.prof_inf_plisse <> 0 then
		ls_riassunto = ls_riassunto + "Prof. Inf.: " + string(w_configuratore.istr_riepilogo.prof_inf_plisse)
	end if
	/// Profilo Superiore ///
	if not isnull(w_configuratore.istr_riepilogo.prof_sup_plisse) and w_configuratore.istr_riepilogo.prof_sup_plisse <> 0 then
		ls_riassunto = ls_riassunto + ", Prof. Sup.: " + string(w_configuratore.istr_riepilogo.prof_sup_plisse)
	end if
	if len(ls_riassunto) > 0 then
		ll_riga = fdw_fine.insertrow(0)
		fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)
	end if
end if

// ------------------------  inizio materie prime automatiche -------------------------------------------
ll_riga = fdw_fine.insertrow(0)
fdw_fine.setitem(ll_riga,"rs_testata","MATERIE PRIME AUTOMATICHE")
ls_riassunto = ""

ll_num_mp = upperbound(w_configuratore.istr_riepilogo.mp_automatiche)
for ll_i = 1 to ll_num_mp
	if not isnull(w_configuratore.istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica) and w_configuratore.istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica <> "" then
		select des_prodotto
		 into :ls_descrizione
		 from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :w_configuratore.istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore nell'Estrazione Materia Prima Automatica ")
			return ""
		end if
		ls_riassunto = ls_riassunto + "Materia Prima: " + w_configuratore.istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica + " - " + ls_descrizione + "~n "
	end if
next

if len(ls_riassunto) < 0 or isnull(ls_riassunto) then ls_riassunto = "Nessun Optional presente"
ll_riga = fdw_fine.insertrow(0)
fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)


//// ------------------------  riepilogo su esito calcolo formule prodotto figlio -------------------------------------------
//if w_configuratore.istr_riepilogo.riepilogo_formule <>"" then
//	ls_riassunto = "~r~nWARNINGS SU CALCOLO FORMULE PROD.FIGLIO:~r~n" + w_configuratore.istr_riepilogo.riepilogo_formule
//else
//	ls_riassunto = "~r~nWARNINGS SU CALCOLO FORMULE PROD.FIGLIO: Nessuno"
//end if
//
//ll_riga = fdw_fine.insertrow(0)
//fdw_fine.setitem(ll_riga,"rs_dettaglio",ls_riassunto)


// ------------------------------------------------- fine di tutto ---------------------------------------
fdw_fine.resetupdate()
return ""

end function

public function integer uof_riassunto_formule (uo_conf_varianti auo_conf_varianti, ref datawindow adw_fine);//questa funzione deve essere chiamata dopo la creazione della varianti (funzione uof_crea_varianti dell'oggetto conf_varianti)
//attualmente nel pulsante fine del configuratore
//in sostanza la creazione delle varianti elabora anche delle formule sui rami di distinta (su figlio, su quantità utilizzo e tecnica)
//quindi vengono valorizzate delle variabili nella struttura auo_conf_varianti con il logo delle formule calcolate


long					ll_riga, ll_index


//###################################################################
ll_riga = adw_fine.insertrow(0)
adw_fine.setitem(ll_riga,"rs_dettaglio", "-----------------------------------------------------------------")
ll_riga = adw_fine.insertrow(0)
adw_fine.setitem(ll_riga,"rs_dettaglio", string(datetime(today(),now()), "dd/mm/yy hh:mm:ss") + " - INIZIO ELABORAZIONE FORMULE DISTINTA BASE")
ll_riga = adw_fine.insertrow(0)
adw_fine.setitem(ll_riga,"rs_dettaglio", "-")
ll_riga = adw_fine.insertrow(0)
adw_fine.setitem(ll_riga,"rs_dettaglio", "Su Prodotto Figlio:")

if upperbound(auo_conf_varianti.is_elaborazione_formule[]) + upperbound(auo_conf_varianti.is_warning_formule[]) > 0 then

	for ll_index=1 to upperbound(auo_conf_varianti.is_elaborazione_formule[])
		ll_riga = adw_fine.insertrow(0)
		adw_fine.setitem(ll_riga,"rs_dettaglio",  auo_conf_varianti.is_elaborazione_formule[ll_index])
	next
	
	for ll_index=1 to upperbound(auo_conf_varianti.is_warning_formule[])
		ll_riga = adw_fine.insertrow(0)
		adw_fine.setitem(ll_riga,"rs_dettaglio",  auo_conf_varianti.is_warning_formule[ll_index])
		adw_fine.setitem(ll_riga,"colore", 255)
	next
	
else
	ll_riga = adw_fine.insertrow(0)
	adw_fine.setitem(ll_riga,"rs_dettaglio",  "Nessuna formula su prodotto figlio da elaborare!")
end if

adw_fine.scrolltorow(ll_riga)


ll_riga = adw_fine.insertrow(0)
adw_fine.setitem(ll_riga,"rs_dettaglio", "-")
ll_riga = adw_fine.insertrow(0)
adw_fine.setitem(ll_riga,"rs_dettaglio", "Su Q.tà Utilizzo:")

if upperbound(auo_conf_varianti.is_elaborazione_formule_qta[]) > 0 then

	for ll_index=1 to upperbound(auo_conf_varianti.is_elaborazione_formule_qta[])
		ll_riga = adw_fine.insertrow(0)
		adw_fine.setitem(ll_riga,"rs_dettaglio",  auo_conf_varianti.is_elaborazione_formule_qta[ll_index])
	next
	
else
	ll_riga = adw_fine.insertrow(0)
	adw_fine.setitem(ll_riga,"rs_dettaglio",  "Nessuna formula su quantità utilizzo da elaborare!")
end if

adw_fine.scrolltorow(ll_riga)

ll_riga = adw_fine.insertrow(0)
adw_fine.setitem(ll_riga,"rs_dettaglio", "-")
ll_riga = adw_fine.insertrow(0)
adw_fine.setitem(ll_riga,"rs_dettaglio", "Su Q.tà Tecnica:")

if upperbound(auo_conf_varianti.is_elaborazione_formule_qtatec[]) > 0 then

	for ll_index=1 to upperbound(auo_conf_varianti.is_elaborazione_formule_qtatec[])
		ll_riga = adw_fine.insertrow(0)
		adw_fine.setitem(ll_riga,"rs_dettaglio",  auo_conf_varianti.is_elaborazione_formule_qtatec[ll_index])
	next
else
	ll_riga = adw_fine.insertrow(0)
	adw_fine.setitem(ll_riga,"rs_dettaglio",  "Nessuna formula su quantità tecnica da elaborare!")
end if

ll_riga = adw_fine.insertrow(0)
adw_fine.setitem(ll_riga,"rs_dettaglio", "-")
ll_riga = adw_fine.insertrow(0)
adw_fine.setitem(ll_riga,"rs_dettaglio", string(datetime(today(),now()), "dd/mm/yy hh:mm:ss") + " - FINE ELABORAZIONE FORMULE DISTINTA BASE")
ll_riga = adw_fine.insertrow(0)
adw_fine.setitem(ll_riga,"rs_dettaglio", "-----------------------------------------------------------------")
adw_fine.scrolltorow(ll_riga)


return 0
end function

on uo_riassunto.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_riassunto.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

