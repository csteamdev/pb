﻿$PBExportHeader$uo_rowfocuschanged.sru
$PBExportComments$UO gestione Evento RowfocusChanged del configuratore
forward
global type uo_rowfocuschanged from nonvisualobject
end type
end forward

global type uo_rowfocuschanged from nonvisualobject
end type
global uo_rowfocuschanged uo_rowfocuschanged

forward prototypes
public function integer uof_rowfocuschanged_optional (datawindow fdw_passo, long fl_currentrow, ref string fs_messaggio)
end prototypes

public function integer uof_rowfocuschanged_optional (datawindow fdw_passo, long fl_currentrow, ref string fs_messaggio);string ls_modify, ls_flag_tipo_det_ven, ls_null, ls_cod_tipo_det_ven

if fl_currentrow > 0 then
	ls_cod_tipo_det_ven = fdw_passo.getitemstring(fl_currentrow,"rs_cod_tipo_det_ven")
	if isnull(ls_cod_tipo_det_ven) then return 0
	
	select flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Attenzione ! Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita."
		return - 1
	end if
	
	if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" then
		ls_modify = "rs_cod_prodotto.protect='0'~t"
		ls_modify = ls_modify + "rs_cod_prodotto.background.color='16777215'~t"
		fdw_passo.modify(ls_modify)
	else
		ls_modify = "rs_cod_prodotto.protect='1'~t"
		ls_modify = ls_modify + "rs_cod_prodotto.background.color='12632256'~t"
		fdw_passo.modify(ls_modify)
	end if
end if
return 0
end function

on uo_rowfocuschanged.create
TriggerEvent( this, "constructor" )
end on

on uo_rowfocuschanged.destroy
TriggerEvent( this, "destructor" )
end on

