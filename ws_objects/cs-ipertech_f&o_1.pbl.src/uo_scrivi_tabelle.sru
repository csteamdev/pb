﻿$PBExportHeader$uo_scrivi_tabelle.sru
$PBExportComments$UO scrittura tabelle effettive
forward
global type uo_scrivi_tabelle from nonvisualobject
end type
end forward

global type uo_scrivi_tabelle from nonvisualobject
end type
global uo_scrivi_tabelle uo_scrivi_tabelle

type variables
uo_array iuo_array



private:
	boolean		ib_ripristina_proforma = false
	
public:
	datastore	ids_data_proforma

end variables

forward prototypes
public function integer uof_calcola_val_riga (string fs_cod_tipo_det_ven, string fs_cod_valuta, double fd_quan_documento, double fd_prezzo_vendita, double fd_sconti[], double fd_sconto_testata, double fd_sconto_pagamento, ref double fd_valore_riga, ref string fs_messaggio)
public function integer uof_crea_comp_det ()
public function integer uof_aggiungi_conf_variabile (string as_colonna, string as_tabella, datetime adt_valore_datetime, ref string as_errore)
public function integer uof_aggiungi_conf_variabile (string as_colonna, string as_tabella, decimal ad_valore_numero, ref string as_errore)
public function integer uo_elimina_righe_riferite (string fs_tipo_gestione, long fl_anno_registrazione, long fl_num_registrazione, long fl_riga_riferimento, ref string fs_messaggio)
public subroutine uof_set_ripristino_proforma (boolean ab_ripristina_proforma)
public function integer uof_imposta_ds_proforma (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore)
public function boolean uof_get_ripristino_proforma ()
public function integer uof_aggiungi_conf_variabile (string as_colonna, string as_tabella, string as_flag_tipo_dato, string as_valore_stringa, decimal ad_valore_numero, datetime adt_valore_datetime, string as_des_valore, ref string as_errore)
public function integer uof_aggiungi_conf_variabile (string as_colonna, string as_tabella, string as_valore_stringa, string as_des_valore, ref string as_errore)
public function string uof_get_descrizione (string f_nome_tabella, string f_where, string f_colonna_des)
end prototypes

public function integer uof_calcola_val_riga (string fs_cod_tipo_det_ven, string fs_cod_valuta, double fd_quan_documento, double fd_prezzo_vendita, double fd_sconti[], double fd_sconto_testata, double fd_sconto_pagamento, ref double fd_valore_riga, ref string fs_messaggio);string ls_flag_tipo_det_ven, ls_flag_sola_iva, ls_cod_valuta_lire
long ll_i, ll_sconti
dec{4} ld_val_riga, ld_prezzo_scontato, ld_sconto_tot
      
if isnull(fd_sconto_testata) then fd_sconto_testata = 0
if isnull(fd_sconto_pagamento) then fd_sconto_pagamento = 0

select flag_tipo_det_ven,
		 flag_sola_iva
into   :ls_flag_tipo_det_ven,
		 :ls_flag_sola_iva
from   tab_tipi_det_ven
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_tipo_det_ven = :fs_cod_tipo_det_ven;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento.",  exclamation!, ok!)
	return -1
end if

if ls_flag_sola_iva = 'N' then
	uo_condizioni_cliente luo_condizioni_cliente
	luo_condizioni_cliente = create uo_condizioni_cliente
	luo_condizioni_cliente.uof_calcola_valore_sconto(fd_sconti[],fd_prezzo_vendita, ld_sconto_tot, ld_prezzo_scontato)
	destroy luo_condizioni_cliente
//	f_calcola_sconto_decimal(fd_sconti[],fd_prezzo_vendita, ld_sconto_tot, ld_prezzo_scontato)
	ld_val_riga = fd_quan_documento * ld_prezzo_scontato
	
	select stringa
	into   :ls_cod_valuta_lire
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'CVL';
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in ricerca valuta lire in parametri aziendali. Dettaglio errore " + sqlca.sqlerrtext
		return -1
	end if
	
	if fs_cod_valuta = ls_cod_valuta_lire then
		ld_val_riga = round(ld_val_riga, 0)
	else
		ld_val_riga = round(ld_val_riga, 2)
	end if   

	if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" or ls_flag_tipo_det_ven = "N" then
//		ld_val_riga = ld_val_riga - ((ld_val_riga * fd_sconto_testata) / 100)
//		ld_val_riga = ld_val_riga - ((ld_val_riga * fd_sconto_pagamento) / 100)
	elseif ls_flag_tipo_det_ven = "S" then
		ld_val_riga = ld_val_riga * -1
	end if
	fd_valore_riga = ld_val_riga
end if
return 0
end function

public function integer uof_crea_comp_det ();/*Funzione che crea la riga in comp_det_[ord]_ven in chiusura di configuratore

 nome: wf_crea_comp
 tipo: integer
  
Variabili passate: 		nome					 tipo				passaggio per			commento
							
Creata il 31-08-98 

EnMe il 18/1/02
		per nota dettaglio del prodotto finito

Enrico Menegotto 19/03/2004 
		Modificato la procedura per la gestione della produzione;
  		La riga del prodotto finito non viene più cancellata e reinserita, ma viene fatto
  		un update su tutte le colonne.
		  
Enrico Menegotto 20/03/2004
		Aggiunto cancellazione eventuali dati di carico dei reparti
		
EnMe 04/02/2009
		Implementazione del configuratore nelle offerte
		
EnMe 22/04/2012
		Gestione di 3 verniciature

EnMe 08/05/2012
		Tipi dettaglio prodotto, addizionali, optionals gestite come eccezioni in tabella specifica
		
EnMe 22/05/2014
		Gestione delle nuove tabelle con le variabili (det_off_ven_conf_variabili, det_ord_ven_conf_variabili)
		aggiunta funzione if uof_aggiungi_conf_variabile()
		
EnMe 28/02/2019
		Nuove colonne per Alusistemi
EnMe 22/07/2019
		Per Paride ripristinato gestione colore lastra e tipo lastra
*/
boolean	lb_alusistemi

string ls_sql_del, ls_tabella_comp, ls_progressivo, ls_sql_ins, ls_tabella_det, ls_tabella_mp_comp, ls_dim_1, ls_dim_2, ls_dim_3, &
		ls_dim_4, ls_cod_tessuto, ls_cod_non_a_magazzino, ls_cod_tessuto_mant, ls_cod_mant_non_a_magazzino, ls_flag_tipo_mantovana, &
		ls_cod_verniciatura, ls_cod_comando, ls_pos_comando, ls_colore_cordolo, ls_colore_passamaneria, ls_cod_colore_lastra, &
		ls_cod_tipo_lastra, ls_spes_lastra, ls_cod_tipo_supporto, ls_flag_cappotta_frontale, ls_flag_rollo, ls_flag_kit_laterale, &
		ls_flag_misura_luce_finita, ls_cod_tipo_stampo, ls_cod_mat_sis_stampaggio, ls_rev_stampo, ls_note, ls_flag_autoblocco, &
		ls_flag_tipo_det_ven, ls_cod_misura_mag, ls_cod_misura, ls_cod_tipo_det_ven, ls_data_consegna, ls_cod_iva, ls_des_prodotto, &
		ls_flag_decimali, ls_cod_iva_esente, ls_nota_prodotto, ls_nota_dettaglio, ls_des_vernice_fc, ls_messaggio, ls_sql, ls_errore, &
		ls_tabella_note, ls_tabella_corrispondenze, ls_cod_tipo_det_ven_prodotto, ls_cod_tipo_det_ven_addizionali, ls_flag_allegati, &
		ls_des_comando_1, ls_des_comando_2, ls_cod_comando_2, ls_flag_doc_suc_or, ls_flag_st_note_or, ls_flag_gen_commessa, &
		ls_flag_presso_cg, ls_flag_presso_pt, ls_flag_trasporta_nota_ddt,ls_cod_prodotto_raggruppato, ls_quantita, ls_cod_iva_det_ven, &
		ls_cod_verniciatura_2,ls_cod_verniciatura_3, ls_des_vernice_fc_2, ls_des_vernice_fc_3, ls_cod_tipo_ord_ven, ls_cod_modello_telo_finito,&
		ls_des_valore, ls_test, ls_flag_mantovana
		
long  ll_num_mp, ll_i, ll_riga_originale, ll_ret

dec{4} ld_quan_prodotto, ld_quan_prodotto_precedente, ld_quan_raggruppo, ld_altezzA_tessuto

dec{5} ld_fat_conversione

datetime ldt_data_esenzione_iva, ldt_data_registrazione, ldt_data_consegna

datastore	lds_data

iuo_array = create uo_array



guo_functions.uof_get_parametro_azienda("ALU", lb_alusistemi)
ld_quan_prodotto = w_configuratore.istr_riepilogo.quan_vendita

choose case str_conf_prodotto.tipo_gestione
		
	case "ORD_VEN"
		ls_tabella_note 		= "det_ord_ven_note"
		ls_tabella_corrispondenze = "det_ord_ven_corrispondenze"
		ls_tabella_det 		= "det_ord_ven"
		ls_tabella_comp 		= "comp_det_ord_ven"
		ls_tabella_mp_comp 	= "tab_mp_non_ric_comp_det"
		ls_progressivo 		= "prog_riga_ord_ven"
		ls_quantita    		= "quan_ordine"
		
	case "OFF_VEN"
		ls_tabella_note 		= "det_off_ven_note"
		ls_tabella_corrispondenze = "det_off_ven_corrispondenze"
		ls_tabella_det 		= "det_off_ven"
		ls_tabella_comp 		= "comp_det_off_ven"
		ls_tabella_mp_comp 	= "tab_mp_non_ric_comp_det_off"
		ls_progressivo 		= "prog_riga_off_ven"
		ls_quantita    		= "quan_offerta"
		
end choose

ls_sql = "select cod_variabile, nome_campo_database from tab_variabili_formule where cod_azienda = '" + s_cs_xx.cod_azienda + "' and nome_tabella_database = 'comp_det_ord_ven' "

ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql)

for ll_i = 1 to ll_ret
	iuo_array.set(lds_data.getitemstring(ll_i, 2), lds_data.getitemstring(ll_i, 1) )
next

destroy lds_data

ls_sql_del ="delete from " + ls_tabella_mp_comp + &
				" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
				" anno_registrazione = " + string(str_conf_prodotto.anno_documento) + " and " + &
				" num_registrazione = " + string(str_conf_prodotto.num_documento) + " and " + &
				ls_progressivo + " = " + string(str_conf_prodotto.prog_riga_documento)

execute immediate :ls_sql_del;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nella Cancellazione delle Materie Prime Automatiche", exclamation!, ok!)
	rollback;
	return -1
end if

ls_sql_del ="delete from " + ls_tabella_comp + &
				" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
				" anno_registrazione = " + string(str_conf_prodotto.anno_documento) + " and " + &
				" num_registrazione = " + string(str_conf_prodotto.num_documento) + " and " + &
				ls_progressivo + " = " + string(str_conf_prodotto.prog_riga_documento)

execute immediate :ls_sql_del;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nella Cancellazione della Composizione", &
				  exclamation!, ok!)
	rollback;
	return -1
end if

if ls_tabella_comp = "comp_det_off_ven" then
	ls_sql_del ="delete from det_off_ven_conf_variabili "
elseif ls_tabella_comp = "comp_det_ord_ven" then
	ls_sql_del ="delete from det_ord_ven_conf_variabili "
end if
ls_sql_del += 	" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					" anno_registrazione = " + string(str_conf_prodotto.anno_documento) + " and " + &
					" num_registrazione = " + string(str_conf_prodotto.num_documento) + " and " + &
					ls_progressivo + " = " + string(str_conf_prodotto.prog_riga_documento)

execute immediate :ls_sql_del;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nella Cancellazione della Composizione", &
				  exclamation!, ok!)
	rollback;
	return -1
end if

// ----------------- verifico se la riga esiste già per eventuale ripristino quan_impegnata -----------------
// Enrico 05/01/2000: memorizzo la riga originale in "ll_riga_originale" perchè, in caso di modifica, 
//                    vado a reinserire allo stesso posto. 
//
choose case str_conf_prodotto.tipo_gestione
	case "ORD_VEN"
		
	ll_riga_originale = 0
	
	select cod_tipo_det_ven, quan_ordine
	into   :ls_cod_tipo_det_ven, :ld_quan_prodotto_precedente
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :str_conf_prodotto.anno_documento and
			 num_registrazione = :str_conf_prodotto.num_documento and
			 prog_riga_ord_ven = :str_conf_prodotto.prog_riga_documento;
			 
	if sqlca.sqlcode = 0 then
		
		ll_riga_originale = str_conf_prodotto.prog_riga_documento
		
		select flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_det_ven = :ls_cod_tipo_det_ven;
				 
		if sqlca.sqlcode = 0 then
			
			if ls_flag_tipo_det_ven = "M" then
				update anag_prodotti  
					set quan_impegnata = quan_impegnata - :ld_quan_prodotto_precedente
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :str_conf_prodotto.cod_modello;
		
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
					return -1
				end if
				
				// enme 08/1/2006 gestione prodotto raggruppato
				setnull(ls_cod_prodotto_raggruppato)
				
				select cod_prodotto_raggruppato
				into   :ls_cod_prodotto_raggruppato
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :str_conf_prodotto.cod_modello;
						 
				if not isnull(ls_cod_prodotto_raggruppato) then
		
					ld_quan_raggruppo = f_converti_qta_raggruppo(str_conf_prodotto.cod_modello, 	ld_quan_prodotto_precedente, "M")

					update anag_prodotti  
						set quan_impegnata = quan_impegnata - :ld_quan_raggruppo
					 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
							 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
			
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
						return -1
					end if
					
				end if
				
			end if
		end if
		// cancello le righe riferite
		if uo_elimina_righe_riferite(str_conf_prodotto.tipo_gestione,  str_conf_prodotto.anno_documento, str_conf_prodotto.num_documento, str_conf_prodotto.prog_riga_documento, ls_messaggio) <> 0 then
			g_mb.messagebox("Attenzione", ls_messaggio, exclamation!, ok!)
			return -1
		end if	
		
		// cancello eventuali distinte di taglio già calcolate
		delete from distinte_taglio_calcolate
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :str_conf_prodotto.anno_documento and
				num_registrazione = :str_conf_prodotto.num_documento  and
				prog_riga_ord_ven = :str_conf_prodotto.prog_riga_documento;
		
		//commentato da DONATO 05/04/2012: non serve in quanto è solo una tabella di appoggio per il report carico reparti
		//e comunque non c'è neanche la FK quindi era praticamente inutileed inoltre il commebntu sulle distinte taglio non centra una beata m....
		//-----------------------------------------------------------------------------------------------------------------
//		// cancello eventuali distinte di taglio già calcolate
//		delete tab_carico_reparti
//		where cod_azienda = :s_cs_xx.cod_azienda and
//				anno_registrazione = :str_conf_prodotto.anno_documento and
//				num_registrazione = :str_conf_prodotto.num_documento  and
//				prog_riga_ord_ven = :str_conf_prodotto.prog_riga_documento;
		//FINE MODIFICA ----------------------------------------------------------------------------------------------
		
	end if
	
case "OFF_VEN"
	
	ll_riga_originale = 0
	
	select cod_tipo_det_ven
	into   :ls_cod_tipo_det_ven
	from   det_off_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :str_conf_prodotto.anno_documento and
			 num_registrazione = :str_conf_prodotto.num_documento and
			 prog_riga_off_ven = :str_conf_prodotto.prog_riga_documento;
			 
	if sqlca.sqlcode = 0 then
		ll_riga_originale = str_conf_prodotto.prog_riga_documento
			// cancello le righe riferite
		if uo_elimina_righe_riferite(str_conf_prodotto.tipo_gestione,  str_conf_prodotto.anno_documento, str_conf_prodotto.num_documento, str_conf_prodotto.prog_riga_documento, ls_messaggio) <> 0 then
			g_mb.messagebox("Attenzione", ls_messaggio, exclamation!, ok!)
			return -1
		end if				
		
	end if
	
end choose


choose case str_conf_prodotto.tipo_gestione
		
	case "ORD_VEN"
		if ll_riga_originale <> 0 then
			str_conf_prodotto.prog_riga_documento = ll_riga_originale
		else
			select max(prog_riga_ord_ven)
			into   :str_conf_prodotto.prog_riga_documento
			from   det_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :str_conf_prodotto.anno_documento and
					 num_registrazione = :str_conf_prodotto.num_documento ;
			if isnull(str_conf_prodotto.prog_riga_documento) or (str_conf_prodotto.prog_riga_documento < 1) then
				str_conf_prodotto.prog_riga_documento = 10
			else
				str_conf_prodotto.prog_riga_documento = (truncate(str_conf_prodotto.prog_riga_documento/10,0) + 1) * 10
			end if
		end if
		str_conf_prodotto.ultima_riga_inserita = str_conf_prodotto.prog_riga_documento
		
	case "OFF_VEN"
		if ll_riga_originale <> 0 then
			str_conf_prodotto.prog_riga_documento = ll_riga_originale
		else
			select max(prog_riga_off_ven)
			into   :str_conf_prodotto.prog_riga_documento
			from   det_off_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :str_conf_prodotto.anno_documento and
					 num_registrazione = :str_conf_prodotto.num_documento ;
			if isnull(str_conf_prodotto.prog_riga_documento) or (str_conf_prodotto.prog_riga_documento < 1) then
				str_conf_prodotto.prog_riga_documento = 10
			else
				str_conf_prodotto.prog_riga_documento = (truncate(str_conf_prodotto.prog_riga_documento/10,0) + 1) * 10
			end if
		end if
		str_conf_prodotto.ultima_riga_inserita = str_conf_prodotto.prog_riga_documento
		
end choose

select cod_misura_mag,
		 cod_misura_ven,
		 fat_conversione_ven,
		 flag_decimali,
		 cod_iva,
		 des_prodotto
into   :ls_cod_misura_mag,
		 :ls_cod_misura,
		 :ld_fat_conversione,
		 :ls_flag_decimali,
		 :ls_cod_iva,
		 :ls_des_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_prodotto = :str_conf_prodotto.cod_modello;
		 
if sqlca.sqlcode = 100 then
	g_mb.messagebox("Configuratore di Prodotto","Prodotto "+ str_conf_prodotto.cod_modello +" non trovato in anagrafica prodotti.~r~n(uo_scrivi_tabelle.uof_crea_comp_det).",stopsign!)
	rollback;
	return -1
elseif sqlca.sqlcode < 0 then
	g_mb.messagebox("Configuratore di Prodotto","Errore imprevisto durante la ricercadel prodotto "+ str_conf_prodotto.cod_modello +" in anagrafica prodotti.~r~n(uo_scrivi_tabelle.uof_crea_comp_det).~r~n"+sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
else
	// EnMe 4-6-2010
	if not isnull(str_conf_prodotto.des_prodotto) and len(str_conf_prodotto.des_prodotto) > 0 then
		ls_des_prodotto = str_conf_prodotto.des_prodotto
	end if
end if



if not isnull(str_conf_prodotto.cod_contatto) then
	select cod_iva,
			 data_esenzione_iva
	into   	:ls_cod_iva_esente,
			 :ldt_data_esenzione_iva
	from   anag_contatti
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_contatto = :str_conf_prodotto.cod_contatto;
	if sqlca.sqlcode = 0 then
		if ls_cod_iva_esente <> "" and not isnull(ls_cod_iva_esente) and (ldt_data_esenzione_iva <= str_conf_prodotto.data_documento or isnull(ldt_data_esenzione_iva)) then
			ls_cod_iva = ls_cod_iva_esente
		end if
	end if
elseif not isnull(str_conf_prodotto.cod_cliente) then
	select cod_iva,
			 data_esenzione_iva
	into   :ls_cod_iva_esente,
			 :ldt_data_esenzione_iva
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_cliente = :str_conf_prodotto.cod_cliente;
	if sqlca.sqlcode = 0 then
		if ls_cod_iva_esente <> "" and not isnull(ls_cod_iva_esente) and (ldt_data_esenzione_iva <= str_conf_prodotto.data_documento or isnull(ldt_data_esenzione_iva)) then
			ls_cod_iva = ls_cod_iva_esente
		end if
	end if
end if



if ls_flag_decimali = "N" and &
   ld_quan_prodotto - int(ld_quan_prodotto) > 0 then
   ld_quan_prodotto = ceiling(ld_quan_prodotto)
end if

if isnull(w_configuratore.istr_riepilogo.nota_dettaglio) then
	ls_nota_dettaglio = " null"
else
	ls_nota_dettaglio = "'" + f_cazzillo(w_configuratore.istr_riepilogo.nota_dettaglio) + "'"
end if

if isnull(w_configuratore.istr_riepilogo.nota_prodotto) then
	ls_nota_prodotto = " null"
else
	ls_nota_prodotto = "'" + f_cazzillo(w_configuratore.istr_riepilogo.nota_prodotto) + "'"
end if

if isnull(w_configuratore.istr_riepilogo.flag_trasporta_nota_ddt) or w_configuratore.istr_riepilogo.flag_trasporta_nota_ddt = "" then
	ls_flag_trasporta_nota_ddt = "N"
else
	ls_flag_trasporta_nota_ddt = w_configuratore.istr_riepilogo.flag_trasporta_nota_ddt
end if

choose case str_conf_prodotto.tipo_gestione
		
	case "ORD_VEN"

		select data_consegna
		into   :ldt_data_consegna
		from   tes_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :str_conf_prodotto.anno_documento and
				 num_registrazione = :str_conf_prodotto.num_documento ;
		if sqlca.sqlcode <> 0 then
			setnull(ldt_data_consegna)
			setnull(str_conf_prodotto.data_consegna)
		else
			str_conf_prodotto.data_consegna = ldt_data_consegna
		end if

	case "OFF_VEN"

		select data_consegna
		into   :ldt_data_consegna
		from   tes_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :str_conf_prodotto.anno_documento and
				 num_registrazione = :str_conf_prodotto.num_documento ;
		if sqlca.sqlcode <> 0 then
			setnull(ldt_data_consegna)
			setnull(str_conf_prodotto.data_consegna)
		else
			str_conf_prodotto.data_consegna = ldt_data_consegna
		end if

end choose

// trovo il tipo dettaglio opportuno da usare per le addizionali

setnull(ls_cod_tipo_det_ven_prodotto)
setnull(ls_cod_tipo_det_ven_addizionali)
setnull(ls_cod_tipo_ord_ven)

if str_conf_prodotto.tipo_gestione="ORD_VEN" then
	select cod_tipo_ord_ven
	into   :ls_cod_tipo_ord_ven
	from   tes_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :str_conf_prodotto.anno_documento and
			 num_registrazione  = :str_conf_prodotto.num_documento;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore in ricerca tipi ordini dalla testata. Dettaglio errore "+sqlca.sqlerrtext)
		return -1
	end if
	
	if isnull(ls_cod_tipo_ord_ven) then
		g_mb.messagebox("Configuratore", "Tipo Ordine non impostato nella testata ordine." )
		return -1
	end if
	
	select cod_tipo_det_ven_prodotto,
			cod_tipo_det_ven_addizionali
	into 	 :ls_cod_tipo_det_ven_prodotto, 
			 :ls_cod_tipo_det_ven_addizionali
	from 	tab_flags_conf_tipi_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_modello = :str_conf_prodotto.cod_modello and 
			 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Configuratore","Errore in ricerca tipo dettaglio in tabella parametri del configuratore! Dettaglio ~r~n" + sqlca.sqlerrtext )
		return -1
	end if
end if

if isnull(ls_cod_tipo_det_ven_prodotto) then
	select cod_tipo_det_ven_prodotto
	into   :ls_cod_tipo_det_ven_prodotto
	from   tab_flags_configuratore
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_modello = :str_conf_prodotto.cod_modello;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Configuratore","Errore in ricerca tipo dettaglio in tabella parametri del configuratore! Dettaglio ~r~n" + sqlca.sqlerrtext )
		return -1
	end if
	if isnull(ls_cod_tipo_det_ven_prodotto) then
		g_mb.messagebox("Configuratore","Il tipo dettaglio vendita del prodotto non è stato indicato nella tabella parametri configuratore")
		return -1
	end if
end if
				 
if isnull(ls_cod_tipo_det_ven_addizionali) then
	select cod_tipo_det_ven_addizionali
	into   :ls_cod_tipo_det_ven_addizionali
	from   tab_flags_configuratore
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_modello = :str_conf_prodotto.cod_modello;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Configuratore","Errore in ricerca tipo dettaglio in tabella parametri del configuratore! Dettaglio ~r~n" + sqlca.sqlerrtext )
		return -1
	end if
	if isnull(ls_cod_tipo_det_ven_addizionali) then
		g_mb.messagebox("Configuratore","Il tipo dettaglio vendita del prodotto non è stato indicato nella tabella parametri configuratore")
		return -1
	end if
end if
				 

select flag_doc_suc_or, 
		flag_st_note_or,
		cod_iva
into   :ls_flag_doc_suc_or, 
		:ls_flag_st_note_or,
		:ls_cod_iva_det_ven
from   tab_tipi_det_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_det_ven = :ls_cod_tipo_det_ven_prodotto;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Configuratore","Errore in ricerca flag_note in tipo dettaglio prodotto. Dettaglio errore "+sqlca.sqlerrtext)
	ROLLBACK;
	return -1
end if


// Se l'aliquota iva è vuota allora la prendo dal tipo dettaglio
if isnull(ls_cod_iva) then
	ls_cod_iva = ls_cod_iva_det_ven
end if

if isnull(ls_cod_iva) then
	g_mb.messagebox("Configuratore di Prodotto","Codice IVA non impostato per questo prodotto.~nVerificare i dati del prodotto in anagrafica_prodotti.~nOperazione interrotta.",stopsign!)
	rollback;
	return -1
end if



if isnull(str_conf_prodotto.data_consegna) then
	ls_data_consegna = " null"
else
	ls_data_consegna = "'" + string(str_conf_prodotto.data_consegna, s_cs_xx.db_funzioni.formato_data) + "'"
end if

ls_flag_gen_commessa = "S"
ls_flag_presso_cg = "N"
ls_flag_presso_pt = "N"

uo_default_prodotto luo_default_prodotto
luo_default_prodotto = CREATE uo_default_prodotto
if luo_default_prodotto.uof_flag_gen_commessa(str_conf_prodotto.cod_modello) = 0 then
	ls_flag_gen_commessa = luo_default_prodotto.is_flag_gen_commessa
	ls_flag_presso_cg = luo_default_prodotto.is_flag_presso_cg
	ls_flag_presso_pt = luo_default_prodotto.is_flag_presso_pt
else
	ls_flag_gen_commessa = "S"
	ls_flag_presso_cg = "N"
	ls_flag_presso_pt = "N"
end if
destroy luo_default_prodotto

//									IMPORTANTE !!!!!!
//
// IL CONTROLLO DEL MINIMALE DI SUPERFICIE ED IL CALCOLO DEL PREZZO AL MQ SONO NELL'EVENTO CLICKED DEL
// PULSANTE CB_FINE DELLA W_CONFIGURATORE
if str_conf_prodotto.stato = "N" then
	ls_sql_ins = "insert into " + ls_tabella_det + &
				  " ( cod_azienda, " + &
				  " anno_registrazione, " + &  
				  " num_registrazione, " + &  
				  ls_progressivo + ", " + &  
				  " cod_prodotto, " + &  
				  " cod_tipo_det_ven, " + &  
				  " cod_misura, " + &  
				  " des_prodotto, " + &  
				  ls_quantita + ", " + &  
				  " prezzo_vendita, " + &  
				  " fat_conversione_ven, " + &  
				  " sconto_1, " + &  
				  " sconto_2, " + &  
				  " provvigione_1, " + &  
				  " provvigione_2, " + &  
				  " cod_iva, " + &  
				  " data_consegna, " + &  
				  " nota_dettaglio, " + &  
				  " cod_centro_costo, " + &  
				  " sconto_3, " + &  
				  " sconto_4, " + &  
				  " sconto_5, " + &  
				  " sconto_6, " + &  
				  " sconto_7, " + &  
				  " sconto_8, " + &  
				  " sconto_9, " + &  
				  " sconto_10, " + &  
				  " cod_versione, " + &  
				  " flag_presso_ptenda, " + &  
				  " flag_presso_cgibus, " + &  
				  " flag_gen_commessa, " + &  
				  " flag_doc_suc_det, " + &  
				  " flag_st_note_det, " + &  
				  " num_riga_appartenenza, " + &  
				  " num_confezioni, " + &  
				  " num_pezzi_confezione "
				  
		if str_conf_prodotto.tipo_gestione = "ORD_VEN" then
			ls_sql_ins += ", " + &  
				  			  " flag_blocco, " + &  
				  			  " flag_trasporta_nota_ddt, " + &  
				 			  " nota_prodotto, " + &  
				  			  " flag_evasione, " + &   
							  " val_riga, " + &  
							  " quan_in_evasione, " + &  
							  " quan_evasa, " + &  
							  " anno_registrazione_off, " + &  
							  " num_registrazione_off, " + &  
							  " prog_riga_off_ven, " + &  
							  " anno_commessa, " + &  
							  " num_commessa) "
		else
			ls_sql_ins += " ) "
		end if
				  
		ls_sql_ins += " VALUES ( '" + s_cs_xx.cod_azienda + "', "  + &  
				  string(str_conf_prodotto.anno_documento) + ", " + &  
				  string(str_conf_prodotto.num_documento) + ", " + &  
				  string(str_conf_prodotto.prog_riga_documento) + ", " + &  
				  "'" + str_conf_prodotto.cod_modello + "', " + &  
				  "'" + ls_cod_tipo_det_ven_prodotto + "', " + &  
				  "'" + ls_cod_misura + "', " + &  
				  "'" + f_cazzillo(ls_des_prodotto) + "', " + &  
				  f_double_to_string(w_configuratore.istr_riepilogo.quan_vendita) + ", " + &
				  " 0, " + &  
				  f_double_to_string(ld_fat_conversione) + ", " + &
				  " 0," + &   
				  " 0, " + &  
				  " 0, " + &  
				  " 0, " + &  
				  "'" + ls_cod_iva + "', " + &  
				  ls_data_consegna + ", " + &
				  ls_nota_dettaglio + "," + &
				  " null,   " + &
				  " 0,   " + &
				  " 0,   " + &
				  " 0,   " + &
				  " 0,   " + &
				  " 0,   " + &
				  " 0,   " + &
				  " 0,   " + &
				  " 0,   " + &
				  "'" + str_conf_prodotto.cod_versione + "', " + &
				  "'" + ls_flag_presso_pt + "', " + &  
				  "'" + ls_flag_presso_cg + "', " + &  
				  "'" + ls_flag_gen_commessa + "', " + &  
				  "'" + ls_flag_doc_suc_or + "', " + &  
				  "'" + ls_flag_st_note_or + "', " + &  
				  " 0,   " + &
				  " 0,   " + &
				  " 0 "
				  
		if str_conf_prodotto.tipo_gestione = "ORD_VEN" then
			ls_sql_ins +=  ", " + &
					" 'N',   " + &
				  "'" + ls_flag_trasporta_nota_ddt + "', " + &  
				  ls_nota_prodotto + ", " + &  
				  " 'A',   " + &
				  " 0,  " + & 
				  " 0, " + &  
				  " 0,  " + & 
				  " null,   " + &
				  " null,   " + &
				  " null,   " + &
				  " null,   " + &
				  " null)  "
		else
			ls_sql_ins += " ) "
		end if
	
else
	ls_sql_ins = "update " + ls_tabella_det + " set " + &
				  " cod_prodotto = '" + str_conf_prodotto.cod_modello +"', " + &  
				  " cod_tipo_det_ven = '" + ls_cod_tipo_det_ven_prodotto +"', " + &  
				  " cod_misura = '" + ls_cod_misura +"', " + &  
				  " des_prodotto = '" + f_cazzillo(ls_des_prodotto) + "', " + &  
				  ls_quantita + " = " + f_double_to_string(w_configuratore.istr_riepilogo.quan_vendita) + ", " + &  
				  " prezzo_vendita = 0 , " + &  
				  " fat_conversione_ven = " + f_double_to_string(ld_fat_conversione) + ", " + &  
				  " sconto_1 = 0, " + &  
				  " sconto_2 = 0, " + &  
				  " provvigione_1 = 0, " + &  
				  " provvigione_2 = 0, " + &  
				  " cod_iva = '" + ls_cod_iva + "', " + &  
				  " data_consegna = " + ls_data_consegna + ", " + &  
				  " val_riga = 0, " + &  
				  " nota_dettaglio = " + ls_nota_dettaglio + ", " + &  
				  " cod_centro_costo = null, " + &  
				  " sconto_3 = 0 , " + &  
				  " sconto_4 = 0 , " + &  
				  " sconto_5 = 0 , " + &  
				  " sconto_6 = 0 , " + &  
				  " sconto_7 = 0 , " + &  
				  " sconto_8 = 0 , " + &  
				  " sconto_9 = 0 , " + &  
				  " sconto_10 = 0 , " + &  
				  " cod_versione = '" + str_conf_prodotto.cod_versione + "', " + &  
				  " flag_presso_ptenda = '" + ls_flag_presso_pt + "', " + &  
				  " flag_presso_cgibus = '" + ls_flag_presso_pt + "', " + &  
				  " flag_gen_commessa = '" + ls_flag_gen_commessa + "', " + &  
				  " flag_doc_suc_det = '" + ls_flag_doc_suc_or + "', " + &  
				  " flag_st_note_det = '" + ls_flag_st_note_or + "', " + &  
				  " num_riga_appartenenza = 0, " + &  
				  " num_confezioni = 0, " + &  
				  " num_pezzi_confezione = 0 "
				  
					if str_conf_prodotto.tipo_gestione = "ORD_VEN" then
						ls_sql_ins += ",  " + &
					   " nota_prodotto = " + ls_nota_prodotto + ", " + &  
					   " flag_trasporta_nota_ddt = '" + ls_flag_trasporta_nota_ddt + "', " + &  
				  		" flag_blocco = 'N', " + &  
				  		" flag_evasione = 'A' , " + &   
  				  		" quan_in_evasione = 0, " + &  
				  		" quan_evasa = 0, " + &  
					  	" anno_registrazione_off = null , " + &  
					  	" num_registrazione_off = null , " + &  
					  	" prog_riga_off_ven = null , " + &  
					  	" anno_commessa = null, " + &  
					  	" num_commessa = null "
					end if


				  ls_sql_ins += &
				  " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
				  "       anno_registrazione = " + string(str_conf_prodotto.anno_documento) + " and " + &
				  "       num_registrazione  = " + string(str_conf_prodotto.num_documento) + " and " + &
				  			 ls_progressivo+ "  = " + string(str_conf_prodotto.prog_riga_documento)
end if

execute immediate :ls_sql_ins;
if sqlca.sqlcode = -1 then
	if str_conf_prodotto.stato = "N" then
		g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
	else
		g_mb.messagebox("Configuratore di Prodotto", "Errore nell' aggiornamento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
	end if		
	rollback;
	return -1
end if

// Registro le variabili
ls_des_valore = uof_get_descrizione("anag_prodotti", "cod_prodotto = '" + w_configuratore.istr_riepilogo.cod_tessuto + "'", "des_prodotto")
if uof_aggiungi_conf_variabile( "cod_tessuto", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_tessuto, ls_des_valore, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.cod_tessuto = "" or isnull(w_configuratore.istr_riepilogo.cod_tessuto) then
	ls_cod_tessuto = "null"
else
	ls_cod_tessuto = "'" + w_configuratore.istr_riepilogo.cod_tessuto + "'"
end if

ls_des_valore = uof_get_descrizione("tab_colori_tessuti", "cod_colore_tessuto = '" + w_configuratore.istr_riepilogo.cod_non_a_magazzino + "'", "des_colore_tessuto")
if sqlca.sqlcode <> 0 then ls_des_valore = ""
if uof_aggiungi_conf_variabile( "cod_non_a_magazzino", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_non_a_magazzino , ls_des_valore, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.cod_non_a_magazzino = "" or isnull(w_configuratore.istr_riepilogo.cod_non_a_magazzino) then
	ls_cod_non_a_magazzino = "null"
else
	ls_cod_non_a_magazzino = "'" + w_configuratore.istr_riepilogo.cod_non_a_magazzino + "'"
end if

ls_des_valore = uof_get_descrizione("anag_prodotti", "cod_prodotto = '" + w_configuratore.istr_riepilogo.cod_tessuto_mant + "'", "des_prodotto")
if uof_aggiungi_conf_variabile( "cod_tessuto_mant", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_tessuto_mant, ls_des_valore, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.cod_tessuto_mant = "" or isnull(w_configuratore.istr_riepilogo.cod_tessuto_mant) then
	ls_cod_tessuto_mant = "null"
else
	ls_cod_tessuto_mant = "'" + w_configuratore.istr_riepilogo.cod_tessuto_mant + "'"
end if
//
ls_des_valore = uof_get_descrizione("tab_colori_tessuti", "cod_colore_tessuto = '" + w_configuratore.istr_riepilogo.cod_mant_non_a_magazzino + "'", "des_colore_tessuto")
if uof_aggiungi_conf_variabile( "cod_mant_non_a_magazzino", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_mant_non_a_magazzino, ls_des_valore, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.cod_mant_non_a_magazzino = "" or isnull(w_configuratore.istr_riepilogo.cod_mant_non_a_magazzino) then
	ls_cod_mant_non_a_magazzino = "null"
else
	ls_cod_mant_non_a_magazzino = "'" + w_configuratore.istr_riepilogo.cod_mant_non_a_magazzino + "'"
end if

if lb_alusistemi then
	if w_configuratore.istr_riepilogo.flag_tipo_mantovana = "G" then
		ls_des_valore = "SPECIALE"
	else
		ls_des_valore =""
	end if
end if
if uof_aggiungi_conf_variabile( "flag_tipo_mantovana", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_tipo_mantovana, ls_des_valore, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_tipo_mantovana = "" or isnull(w_configuratore.istr_riepilogo.flag_tipo_mantovana) then
	ls_flag_tipo_mantovana = "null"
else
	ls_flag_tipo_mantovana = "'" + w_configuratore.istr_riepilogo.flag_tipo_mantovana + "'"
end if

if uof_aggiungi_conf_variabile( "colore_cordolo", ls_tabella_comp, w_configuratore.istr_riepilogo.colore_cordolo, "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.colore_cordolo = "" or isnull(w_configuratore.istr_riepilogo.colore_cordolo) then
	ls_colore_cordolo = "null"
else
	ls_colore_cordolo = "'" + w_configuratore.istr_riepilogo.colore_cordolo + "'"
end if

if uof_aggiungi_conf_variabile( "colore_passamaneria", ls_tabella_comp, w_configuratore.istr_riepilogo.colore_passamaneria, "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.colore_passamaneria = "" or isnull(w_configuratore.istr_riepilogo.colore_passamaneria) then
	ls_colore_passamaneria = "null"
else
	ls_colore_passamaneria = "'" + w_configuratore.istr_riepilogo.colore_passamaneria + "'"
end if

ls_des_valore = uof_get_descrizione("anag_prodotti", "cod_prodotto = '" + w_configuratore.istr_riepilogo.cod_verniciatura + "'", "des_prodotto")
if uof_aggiungi_conf_variabile( "cod_verniciatura", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_verniciatura, ls_des_valore, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.cod_verniciatura = "" or isnull(w_configuratore.istr_riepilogo.cod_verniciatura) then
	ls_cod_verniciatura = "null"
else
	ls_cod_verniciatura = "'" + w_configuratore.istr_riepilogo.cod_verniciatura + "'"
end if

ls_des_valore = uof_get_descrizione("anag_prodotti", "cod_prodotto = '" + w_configuratore.istr_riepilogo.cod_verniciatura_2 + "'", "des_prodotto")
if uof_aggiungi_conf_variabile( "cod_verniciatura_2", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_verniciatura_2, ls_des_valore, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.cod_verniciatura_2 = "" or isnull(w_configuratore.istr_riepilogo.cod_verniciatura_2) then
	ls_cod_verniciatura_2 = "null"
else
	ls_cod_verniciatura_2 = "'" + w_configuratore.istr_riepilogo.cod_verniciatura_2 + "'"
end if

ls_des_valore = uof_get_descrizione("anag_prodotti", "cod_prodotto = '" + w_configuratore.istr_riepilogo.cod_verniciatura_3 + "'", "des_prodotto")
if uof_aggiungi_conf_variabile( "cod_verniciatura_3", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_verniciatura_3, ls_des_valore, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.cod_verniciatura_3 = "" or isnull(w_configuratore.istr_riepilogo.cod_verniciatura_3) then
	ls_cod_verniciatura_3 = "null"
else
	ls_cod_verniciatura_3 = "'" + w_configuratore.istr_riepilogo.cod_verniciatura_3 + "'"
end if

if uof_aggiungi_conf_variabile( "des_vernice_fc", ls_tabella_comp, w_configuratore.istr_riepilogo.des_vernice_fc, "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.des_vernice_fc = "" or isnull(w_configuratore.istr_riepilogo.des_vernice_fc) then
	ls_des_vernice_fc = "null"
else
	ls_des_vernice_fc = "'" + w_configuratore.istr_riepilogo.des_vernice_fc + "'"
end if

if uof_aggiungi_conf_variabile( "des_vernice_fc_2", ls_tabella_comp, w_configuratore.istr_riepilogo.des_vernice_fc_2, "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.des_vernice_fc_2 = "" or isnull(w_configuratore.istr_riepilogo.des_vernice_fc_2) then
	ls_des_vernice_fc_2 = "null"
else
	ls_des_vernice_fc_2 = "'" + w_configuratore.istr_riepilogo.des_vernice_fc_2 + "'"
end if

if uof_aggiungi_conf_variabile( "des_vernice_fc_3", ls_tabella_comp,  w_configuratore.istr_riepilogo.des_vernice_fc_3, "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.des_vernice_fc_3 = "" or isnull(w_configuratore.istr_riepilogo.des_vernice_fc_3) then
	ls_des_vernice_fc_3 = "null"
else
	ls_des_vernice_fc_3 = "'" + w_configuratore.istr_riepilogo.des_vernice_fc_3 + "'"
end if

ls_des_valore = uof_get_descrizione("anag_prodotti", "cod_prodotto = '" + w_configuratore.istr_riepilogo.cod_comando + "'", "des_prodotto")
if uof_aggiungi_conf_variabile( "cod_comando", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_comando ,ls_des_valore,ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.cod_comando = "" or isnull(w_configuratore.istr_riepilogo.cod_comando) then
	ls_cod_comando = "null"
else
	ls_cod_comando = "'" + w_configuratore.istr_riepilogo.cod_comando + "'" 
end if

if uof_aggiungi_conf_variabile( "pos_comando", ls_tabella_comp, w_configuratore.istr_riepilogo.pos_comando, "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.pos_comando = "" or isnull(w_configuratore.istr_riepilogo.pos_comando) then
	ls_pos_comando = "null"
else
	ls_pos_comando = "'" + w_configuratore.istr_riepilogo.pos_comando + "'" 
end if

ls_des_valore = uof_get_descrizione("anag_prodotti", "cod_prodotto = '" + w_configuratore.istr_riepilogo.cod_tipo_supporto + "'", "des_prodotto")
if uof_aggiungi_conf_variabile( "cod_tipo_supporto", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_tipo_supporto ,ls_des_valore, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.cod_tipo_supporto = "" or isnull(w_configuratore.istr_riepilogo.cod_tipo_supporto) then
	ls_cod_tipo_supporto = "null"
else
	ls_cod_tipo_supporto = "'" + w_configuratore.istr_riepilogo.cod_tipo_supporto + "'"
end if

if uof_aggiungi_conf_variabile( "flag_misura_luce_finita", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_misura_luce_finita , "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_misura_luce_finita = "" or isnull(w_configuratore.istr_riepilogo.flag_misura_luce_finita) then
	ls_flag_misura_luce_finita = "'L'"
else
	ls_flag_misura_luce_finita = "'" + w_configuratore.istr_riepilogo.flag_misura_luce_finita + "'"
end if

if uof_aggiungi_conf_variabile( "flag_allegati", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_allegati, "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_allegati = "" or isnull(w_configuratore.istr_riepilogo.flag_allegati) then
	ls_flag_allegati = "'N'"
else
	ls_flag_allegati = "'" + w_configuratore.istr_riepilogo.flag_allegati + "'"
end if

if uof_aggiungi_conf_variabile( "note", ls_tabella_comp, w_configuratore.istr_riepilogo.note , "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.note = "" or isnull(w_configuratore.istr_riepilogo.note) then
	ls_note = "null"
else
	ls_note = "'" + f_cazzillo(w_configuratore.istr_riepilogo.note) + "'"
end if

if uof_aggiungi_conf_variabile( "des_comando_1", ls_tabella_comp, w_configuratore.istr_riepilogo.des_comando_1, "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if isnull(w_configuratore.istr_riepilogo.des_comando_1) or w_configuratore.istr_riepilogo.des_comando_1 = "" then
	ls_des_comando_1 = "null"
else
	ls_des_comando_1 = "'" + f_cazzillo(w_configuratore.istr_riepilogo.des_comando_1) + "'"
end if

if uof_aggiungi_conf_variabile( "des_comando_2", ls_tabella_comp, w_configuratore.istr_riepilogo.des_comando_2 , "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if isnull(w_configuratore.istr_riepilogo.des_comando_2) or w_configuratore.istr_riepilogo.des_comando_2 = "" then
	ls_des_comando_2 = "null"
else
	ls_des_comando_2 = "'" + f_cazzillo(w_configuratore.istr_riepilogo.des_comando_2) + "'"
end if

ls_des_valore = uof_get_descrizione("anag_prodotti", "cod_prodotto = '" + w_configuratore.istr_riepilogo.cod_comando_2 + "'", "des_prodotto")
if uof_aggiungi_conf_variabile( "cod_comando_2", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_comando_2 , ls_des_valore, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if isnull(w_configuratore.istr_riepilogo.cod_comando_2) or w_configuratore.istr_riepilogo.cod_comando_2 = "" then
	ls_cod_comando_2 = "null"
else
	ls_cod_comando_2 = "'" + w_configuratore.istr_riepilogo.cod_comando_2 + "'"
end if

if uof_aggiungi_conf_variabile( "flag_addizionale_comando_1", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_addizionale_comando_1 , "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_addizionale_comando_1 = "" or isnull(w_configuratore.istr_riepilogo.flag_addizionale_comando_1) then
	w_configuratore.istr_riepilogo.flag_addizionale_comando_1 = "N"
end if

if uof_aggiungi_conf_variabile( "flag_addizionale_comando_2", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_addizionale_comando_2 , "",  ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_addizionale_comando_2 = "" or isnull(w_configuratore.istr_riepilogo.flag_addizionale_comando_2) then
	w_configuratore.istr_riepilogo.flag_addizionale_comando_2 = "N"
end if

if uof_aggiungi_conf_variabile( "flag_addizionale_supporto", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_addizionale_supporto, "",  ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_addizionale_supporto = "" or isnull(w_configuratore.istr_riepilogo.flag_addizionale_supporto) then
	w_configuratore.istr_riepilogo.flag_addizionale_supporto = "N"
end if

if uof_aggiungi_conf_variabile( "flag_addizionale_tessuto", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_addizionale_tessuto, "",  ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_addizionale_tessuto = "" or isnull(w_configuratore.istr_riepilogo.flag_addizionale_tessuto) then
	w_configuratore.istr_riepilogo.flag_addizionale_tessuto = "N"
end if

if uof_aggiungi_conf_variabile( "flag_addizionale_vernic", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_addizionale_vernic ,"",  ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_addizionale_vernic = "" or isnull(w_configuratore.istr_riepilogo.flag_addizionale_vernic) then
	w_configuratore.istr_riepilogo.flag_addizionale_vernic = "N"
end if

if uof_aggiungi_conf_variabile( "flag_addizionale_vernic_2", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_addizionale_vernic_2 ,"", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_addizionale_vernic_2 = "" or isnull(w_configuratore.istr_riepilogo.flag_addizionale_vernic_2) then
	w_configuratore.istr_riepilogo.flag_addizionale_vernic_2 = "N"
end if

if uof_aggiungi_conf_variabile( "flag_addizionale_vernic_3", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_addizionale_vernic_3,"", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_addizionale_vernic_3 = "" or isnull(w_configuratore.istr_riepilogo.flag_addizionale_vernic_3) then
	w_configuratore.istr_riepilogo.flag_addizionale_vernic_3 = "N"
end if

if uof_aggiungi_conf_variabile( "flag_fc_comando_1", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_fc_comando_1 ,"", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_fc_comando_1 = "" or isnull(w_configuratore.istr_riepilogo.flag_fc_comando_1) then
	w_configuratore.istr_riepilogo.flag_fc_comando_1 = "N"
end if

if uof_aggiungi_conf_variabile( "flag_fc_comando_2", ls_tabella_comp,  w_configuratore.istr_riepilogo.flag_fc_comando_2,"", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_fc_comando_2 = "" or isnull(w_configuratore.istr_riepilogo.flag_fc_comando_2) then
	w_configuratore.istr_riepilogo.flag_fc_comando_2 = "N"
end if

if uof_aggiungi_conf_variabile( "flag_fc_supporto", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_fc_supporto,"", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_fc_supporto = "" or isnull(w_configuratore.istr_riepilogo.flag_fc_supporto) then
	w_configuratore.istr_riepilogo.flag_fc_supporto = "N"
end if

if uof_aggiungi_conf_variabile( "flag_fc_tessuto", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_fc_tessuto,"", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_fc_tessuto = "" or isnull(w_configuratore.istr_riepilogo.flag_fc_tessuto) then
	w_configuratore.istr_riepilogo.flag_fc_tessuto = "N"
end if

if uof_aggiungi_conf_variabile( "flag_fc_vernic", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_fc_vernic, "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_fc_vernic = "" or isnull(w_configuratore.istr_riepilogo.flag_fc_vernic) then
	w_configuratore.istr_riepilogo.flag_fc_vernic = "N"
end if

if uof_aggiungi_conf_variabile( "flag_fc_vernic_2", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_fc_vernic_2,"", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_fc_vernic_2 = "" or isnull(w_configuratore.istr_riepilogo.flag_fc_vernic_2) then
	w_configuratore.istr_riepilogo.flag_fc_vernic_2 = "N"
end if

if uof_aggiungi_conf_variabile( "flag_fc_vernic_3", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_fc_vernic_3,"", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_fc_vernic_3 = "" or isnull(w_configuratore.istr_riepilogo.flag_fc_vernic_3) then
	w_configuratore.istr_riepilogo.flag_fc_vernic_3 = "N"
end if

if uof_aggiungi_conf_variabile( "flag_tessuto_cliente", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_tessuto_cliente, "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_tessuto_cliente = "" or isnull(w_configuratore.istr_riepilogo.flag_tessuto_cliente) then
	w_configuratore.istr_riepilogo.flag_tessuto_cliente = "N"
end if

if uof_aggiungi_conf_variabile( "flag_tessuto_mant_cliente", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_tessuto_mant_cliente, "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.flag_tessuto_mant_cliente = "" or isnull(w_configuratore.istr_riepilogo.flag_tessuto_mant_cliente) then
	w_configuratore.istr_riepilogo.flag_tessuto_mant_cliente = "N"
end if

if uof_aggiungi_conf_variabile( "cod_modello_telo_finito", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_modello_telo_finito,"", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if w_configuratore.istr_riepilogo.cod_modello_telo_finito = "" or isnull(w_configuratore.istr_riepilogo.cod_modello_telo_finito) then
	ls_cod_modello_telo_finito = "null"
else
	ls_cod_modello_telo_finito = "'" + w_configuratore.istr_riepilogo.cod_modello_telo_finito + "'" 
end if

if uof_aggiungi_conf_variabile( "alt_mantovana", ls_tabella_comp, w_configuratore.istr_riepilogo.alt_mantovana, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if uof_aggiungi_conf_variabile( "alt_asta", ls_tabella_comp, w_configuratore.istr_riepilogo.alt_asta, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

if uof_aggiungi_conf_variabile( "dim_x", ls_tabella_comp, w_configuratore.istr_riepilogo.dimensione_1, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if uof_aggiungi_conf_variabile( "dim_y", ls_tabella_comp, w_configuratore.istr_riepilogo.dimensione_2, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if uof_aggiungi_conf_variabile( "dim_z", ls_tabella_comp, w_configuratore.istr_riepilogo.dimensione_3, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if uof_aggiungi_conf_variabile( "dim_t", ls_tabella_comp, w_configuratore.istr_riepilogo.dimensione_4, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if uof_aggiungi_conf_variabile( "dim_5", ls_tabella_comp, w_configuratore.istr_riepilogo.dimensione_5, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if uof_aggiungi_conf_variabile( "dim_6", ls_tabella_comp, w_configuratore.istr_riepilogo.dimensione_6, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if uof_aggiungi_conf_variabile( "dim_7", ls_tabella_comp, w_configuratore.istr_riepilogo.dimensione_7, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if uof_aggiungi_conf_variabile( "dim_8", ls_tabella_comp, w_configuratore.istr_riepilogo.dimensione_8, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
if uof_aggiungi_conf_variabile( "dim_9", ls_tabella_comp, w_configuratore.istr_riepilogo.dimensione_9, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

if uof_aggiungi_conf_variabile( "diam_tubetto_telo_sup", ls_tabella_comp, w_configuratore.istr_riepilogo.diam_tubetto_telo_sup, "",ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

if uof_aggiungi_conf_variabile( "diam_tubetto_telo_inf", ls_tabella_comp, w_configuratore.istr_riepilogo.diam_tubetto_telo_inf, "", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

if uof_aggiungi_conf_variabile( "diam_tubetto_mant", ls_tabella_comp, w_configuratore.istr_riepilogo.diam_tubetto_mant,"", ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

ls_des_valore = uof_get_descrizione("tab_tipi_unione_tessuti", "cod_tipo_unione_tessuto = '" + w_configuratore.istr_riepilogo.flag_tipo_unione + "'", "des_tipo_unione_tessuto")
if uof_aggiungi_conf_variabile( "flag_tipo_unione", ls_tabella_comp,  w_configuratore.istr_riepilogo.flag_tipo_unione,ls_des_valore, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

choose case w_configuratore.istr_riepilogo.flag_orientamento_unione
	case "O"
		ls_des_valore = "Orizzontale"
	case "V"
		ls_des_valore = "Verticale"
	case "N"
		ls_des_valore = "---"
end choose		
if uof_aggiungi_conf_variabile( "flag_orientamento_unione", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_orientamento_unione, ls_des_valore,ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

ls_des_valore = uof_get_descrizione("tab_flags_config_tipo_tasca", "cod_modello= '" + str_conf_prodotto.cod_modello + "' and cod_tipo_tasca='" + w_configuratore.istr_riepilogo.cod_tipo_tasca +"' ", "des_tipo_tasca")
if uof_aggiungi_conf_variabile( "cod_tipo_tasca", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_tipo_tasca, ls_des_valore,ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

ls_des_valore = uof_get_descrizione("tab_flags_config_tipo_tasca", "cod_modello= '" + str_conf_prodotto.cod_modello + "' and cod_tipo_tasca='" + w_configuratore.istr_riepilogo.cod_tipo_tasca_1 +"' ", "des_tipo_tasca")
if uof_aggiungi_conf_variabile( "cod_tipo_tasca_2", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_tipo_tasca_1, ls_des_valore,ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

ls_des_valore = uof_get_descrizione("tab_flags_config_tipo_tasca", "cod_modello= '" + str_conf_prodotto.cod_modello + "' and cod_tipo_tasca='" + w_configuratore.istr_riepilogo.cod_tipo_tasca_2 +"' ", "des_tipo_tasca")
if uof_aggiungi_conf_variabile( "cod_tipo_tasca_3", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_tipo_tasca_2, ls_des_valore,ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

ls_des_valore = uof_get_descrizione("anag_reparti", "cod_reparto_telo= '" + w_configuratore.istr_riepilogo.cod_reparto_telo +"' ", "des_reparto")
if uof_aggiungi_conf_variabile( "cod_reparto_telo", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_reparto_telo, ls_des_valore,ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

choose case w_configuratore.istr_riepilogo.flag_mantovana
	case "S"
		ls_des_valore = "SI"
	case "N"
		ls_des_valore = "NO"
	case else
		ls_des_valore = "SOLO"
end choose		
if uof_aggiungi_conf_variabile( "flag_mantovana", ls_tabella_comp, w_configuratore.istr_riepilogo.flag_mantovana, ls_des_valore,ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

if uof_aggiungi_conf_variabile( "rapporto_mantovana", ls_tabella_comp, w_configuratore.istr_riepilogo.rapporto_mantovana, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

if uof_aggiungi_conf_variabile( "num_rinforzi", ls_tabella_comp, w_configuratore.istr_riepilogo.num_rinforzi, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

if uof_aggiungi_conf_variabile( "dimensione_rinforzo", ls_tabella_comp, w_configuratore.istr_riepilogo.dimensione_rinforzo, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

ls_des_valore = uof_get_descrizione("anag_prodotti", "cod_prodotto = '" + str_conf_prodotto.cod_prodotto_finito + "'", "des_prodotto")
if uof_aggiungi_conf_variabile( "cod_prodotto_finito", ls_tabella_comp, str_conf_prodotto.cod_prodotto_finito,ls_des_valore, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if
ls_des_valore = uof_get_descrizione("anag_prodotti", "cod_prodotto = '" + str_conf_prodotto.cod_modello + "'", "des_prodotto")
if uof_aggiungi_conf_variabile( "cod_prodotto", ls_tabella_comp,  str_conf_prodotto.cod_modello,ls_des_valore, ls_errore ) = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
end if

// Caso particolare gestito per Carraro per avere a disposizione l'altezza del tessuto.
select cod_variabile
into	:ls_test
from	tab_des_variabili 
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :str_conf_prodotto.cod_modello and
		cod_variabile = 'ALTTES';
if sqlca.sqlcode = 0 and len(w_configuratore.istr_riepilogo.cod_tessuto) > 0 and len(w_configuratore.istr_riepilogo.cod_non_a_magazzino) > 0 then
	select alt_tessuto
	into	:ld_altezza_tessuto
	from	tab_tessuti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tessuto = :w_configuratore.istr_riepilogo.cod_tessuto and
			cod_non_a_magazzino = :w_configuratore.istr_riepilogo.cod_non_a_magazzino;
	if sqlca.sqlcode = 0 then
		if uof_aggiungi_conf_variabile( "alt_tessuto", ls_tabella_comp , ld_altezza_tessuto, ls_errore) = -1 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
		end if
	end if
end if

if w_configuratore.istr_riepilogo.cod_colore_lastra = "" or isnull(w_configuratore.istr_riepilogo.cod_colore_lastra) then
	ls_cod_colore_lastra = "null"
else
	ls_des_valore = uof_get_descrizione("anag_prodotti", "cod_prodotto = '" + w_configuratore.istr_riepilogo.cod_colore_lastra + "'", "des_prodotto")
	if uof_aggiungi_conf_variabile( "cod_colore_lastra", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_colore_lastra,ls_des_valore, ls_errore ) = -1 then
		g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
	end if
	ls_cod_colore_lastra = "'" + w_configuratore.istr_riepilogo.cod_colore_lastra + "'"
end if

if w_configuratore.istr_riepilogo.cod_tipo_lastra = "" or isnull(w_configuratore.istr_riepilogo.cod_tipo_lastra) then
	ls_cod_tipo_lastra = "null"
else
	ls_des_valore = uof_get_descrizione("tab_tipi_lastre", g_str.format("cod_modello = '$1' and cod_tipo_lastra = '$2' ", str_conf_prodotto.cod_modello, w_configuratore.istr_riepilogo.cod_tipo_lastra), "des_tipo_lastra")
	if uof_aggiungi_conf_variabile( "cod_tipo_lastra", ls_tabella_comp, w_configuratore.istr_riepilogo.cod_tipo_lastra,ls_des_valore, ls_errore ) = -1 then
		g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della riga ordine. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
	end if
	ls_cod_tipo_lastra = "'" + w_configuratore.istr_riepilogo.cod_tipo_lastra + "'"
end if

// deprecate

if w_configuratore.istr_riepilogo.spes_lastra = "" or isnull(w_configuratore.istr_riepilogo.spes_lastra) then
	ls_spes_lastra = "null"
else
	ls_spes_lastra = "'" + w_configuratore.istr_riepilogo.spes_lastra + "'"
end if
if w_configuratore.istr_riepilogo.flag_cappotta_frontale = "" or isnull(w_configuratore.istr_riepilogo.flag_cappotta_frontale) then
	ls_flag_cappotta_frontale = "'N'"
else
	ls_flag_cappotta_frontale = "'" + w_configuratore.istr_riepilogo.flag_cappotta_frontale + "'"
end if
if w_configuratore.istr_riepilogo.flag_rollo = "" or isnull(w_configuratore.istr_riepilogo.flag_rollo) then
	ls_flag_rollo = "'N'"
else
	ls_flag_rollo = "'" + w_configuratore.istr_riepilogo.flag_rollo + "'"
end if
if w_configuratore.istr_riepilogo.flag_kit_laterale = "" or isnull(w_configuratore.istr_riepilogo.flag_kit_laterale) then
	ls_flag_kit_laterale = "'N'"
else
	ls_flag_kit_laterale = "'" + w_configuratore.istr_riepilogo.flag_kit_laterale + "'"
end if
if w_configuratore.istr_riepilogo.cod_tipo_stampo = "" or isnull(w_configuratore.istr_riepilogo.cod_tipo_stampo) then
	ls_cod_tipo_stampo = "null"
else
	ls_cod_tipo_stampo = "'" + w_configuratore.istr_riepilogo.cod_tipo_stampo + "'"
end if
if w_configuratore.istr_riepilogo.cod_mat_sis_stampaggio = "" or isnull(w_configuratore.istr_riepilogo.cod_mat_sis_stampaggio) then
	ls_cod_mat_sis_stampaggio = "null"
else
	ls_cod_mat_sis_stampaggio = "'" + w_configuratore.istr_riepilogo.cod_mat_sis_stampaggio + "'"
end if
if w_configuratore.istr_riepilogo.rev_stampo = "" or isnull(w_configuratore.istr_riepilogo.rev_stampo) then
	ls_rev_stampo = "null"
else
	ls_rev_stampo = "'" + w_configuratore.istr_riepilogo.rev_stampo + "'"
end if
if w_configuratore.istr_riepilogo.flag_autoblocco = "" or isnull(w_configuratore.istr_riepilogo.flag_autoblocco) then
	ls_flag_autoblocco = "'N'"
else
	ls_flag_autoblocco = "'" + w_configuratore.istr_riepilogo.flag_autoblocco + "'"
end if
if w_configuratore.istr_riepilogo.flag_mantovana = "" or isnull(w_configuratore.istr_riepilogo.flag_mantovana) then
	ls_flag_mantovana = "null"
else
	ls_flag_mantovana = "'" + w_configuratore.istr_riepilogo.flag_mantovana + "'"
end if





ls_sql_ins =	"insert into " + ls_tabella_comp + &
						" (cod_azienda," + &
						"anno_registrazione," + &
						"num_registrazione," + &
						ls_progressivo + "," + &
						"cod_prod_finito," + &
						"cod_modello," + &
						"dim_x," + &
						"dim_y," + &
						"dim_z," + &
						"dim_t," + &
						"dim_05," + &
						"dim_06," + &
						"dim_07," + &
						"dim_08," + &
						"dim_09," + &
						"cod_tessuto," + &
						"cod_non_a_magazzino," + &
						"cod_tessuto_mant," + &
						"cod_mant_non_a_magazzino," + &
						"flag_tipo_mantovana," + &
						"alt_mantovana," + &
						"flag_cordolo," + &
						"flag_passamaneria," + &
						"cod_verniciatura," + &
						"cod_comando," + &
						"pos_comando," + &
						"alt_asta," + &
						"cod_comando_tenda_verticale," + &
						"cod_colore_lastra," + &
						"cod_tipo_lastra," + &
						"spes_lastra," + &
						"cod_tipo_supporto," + &
						"flag_cappotta_frontale," + &
						"flag_rollo," + &
						"flag_kit_laterale," + &
						"flag_misura_luce_finita," + &
						"cod_tipo_stampo," + &
						"cod_mat_sis_stampaggio," + &
						"rev_stampo," + &  
						"num_gambe," + &
						"num_campate, " + &
						"note," + &
						"note_esterne, " + &
						"flag_autoblocco," + &
						"num_fili_plisse," + &
						"num_boccole_plisse," + &
						"intervallo_punzoni_plisse," + &
						"num_pieghe_plisse," + &
						"prof_inf_plisse," + &
						"prof_sup_plisse," + &
						"colore_passamaneria," + &
						"colore_cordolo," + &
						"num_punzoni_plisse, " + &
						"des_comando_1," + &
						"cod_comando_2," + &
						"des_comando_2," + &
						"flag_addizionale_comando_1," + &
						"flag_addizionale_comando_2," + &
						"flag_addizionale_supporto," + &
						"flag_addizionale_tessuto," + &
						"flag_addizionale_vernic," + &
						"flag_fc_comando_1," + &
						"flag_fc_comando_2," + &
						"flag_fc_supporto," + &
						"flag_fc_tessuto," + &
						"flag_fc_vernic," + &
						"flag_allegati," + &
						"des_vernice_fc, " + &
						"flag_tessuto_cliente," + &
						"flag_tessuto_mant_cliente," + &
						"cod_verniciatura_2," + &
						"cod_verniciatura_3," + &
						"des_vernic_fc_2," + &
						"des_vernic_fc_3," + &
						"flag_addizionale_vernic_2," + &
						"flag_addizionale_vernic_3," + &
						"flag_fc_vernic_2," + &
						"flag_fc_vernic_3," + &
						"flag_azzera_dt, " + &
						"cod_modello_telo_finito, " + &
						"diam_tubetto_telo_sup, " + &
						"diam_tubetto_telo_inf, " + &
						"diam_tubetto_mant, " + &
						"flag_tipo_unione, " + &
						"flag_orientamento_unione, " + &
						"cod_tipo_tasca, " + &
						"cod_tipo_tasca_2, " + &
						"cod_tipo_tasca_3, " + &
						"flag_mantovana, " + &
						"rapporto_mantovana, " + &
						"num_rinforzi, " + &
						"dimensione_rinforzo," + &
						"cod_reparto" + &
						") " + &
					" values ('" + s_cs_xx.cod_azienda + "', " + &
						string(str_conf_prodotto.anno_documento) + ", " + &
						string(str_conf_prodotto.num_documento) + ", " + &
						string(str_conf_prodotto.prog_riga_documento) + ", " + &
						"'" + str_conf_prodotto.cod_prodotto_finito + "', " + &
						"'" + str_conf_prodotto.cod_modello + "', " + &
						f_double_to_string(w_configuratore.istr_riepilogo.dimensione_1) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.dimensione_2) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.dimensione_3) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.dimensione_4) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.dimensione_5) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.dimensione_6) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.dimensione_7) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.dimensione_8) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.dimensione_9) + ", " + &
						ls_cod_tessuto + ", " + &
						ls_cod_non_a_magazzino + ", " + &
						ls_cod_tessuto_mant + ", " + &
						ls_cod_mant_non_a_magazzino + ", " + &
						ls_flag_tipo_mantovana + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.alt_mantovana) + ", " + &
						"'N'" + ", " + &
						"'N'" + ", " + &
						ls_cod_verniciatura + ", " + &
						ls_cod_comando + ", " + &
						ls_pos_comando + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.alt_asta) + ", " + &
						"null, " + &
						ls_cod_colore_lastra + ", " + &
						ls_cod_tipo_lastra + ", " + &
						ls_spes_lastra + ", " + &
						ls_cod_tipo_supporto + ", " + &
						ls_flag_cappotta_frontale + ", " + &
						ls_flag_rollo + ", " + &
						ls_flag_kit_laterale + ", " + &
						ls_flag_misura_luce_finita + ", " + &
						ls_cod_tipo_stampo + ", " + &
						ls_cod_mat_sis_stampaggio + ", " + &
						ls_rev_stampo + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.num_gambe) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.num_campate) + ", " + &
						ls_note + ", " + &
						"null, " + &
						ls_flag_autoblocco + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.num_fili_plisse) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.num_boccole_plisse) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.intervallo_punzoni_plisse) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.num_pieghe_plisse) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.prof_inf_plisse) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.prof_sup_plisse) + ", " + &
						ls_colore_passamaneria + ", " + &
						ls_colore_cordolo + ", " + &
						"0, " + &
						ls_des_comando_1 + ", " + &
						ls_cod_comando_2 + ", " + &
						ls_des_comando_2 + ", " + &
						"'" + w_configuratore.istr_riepilogo.flag_addizionale_comando_1 + "', " + &
						"'" + w_configuratore.istr_riepilogo.flag_addizionale_comando_2 + "', " + &
						"'" + w_configuratore.istr_riepilogo.flag_addizionale_supporto + "', " + &
						"'" + w_configuratore.istr_riepilogo.flag_addizionale_tessuto + "', " + &
						"'" + w_configuratore.istr_riepilogo.flag_addizionale_vernic + "', " + &
						"'" + w_configuratore.istr_riepilogo.flag_fc_comando_1 + "', " + &
						"'" + w_configuratore.istr_riepilogo.flag_fc_comando_2 + "', " + &
						"'" + w_configuratore.istr_riepilogo.flag_fc_supporto + "', " + &
						"'" + w_configuratore.istr_riepilogo.flag_fc_tessuto + "', " + &
						"'" + w_configuratore.istr_riepilogo.flag_fc_vernic + "', " + &
						ls_flag_allegati + ", " + &
						ls_des_vernice_fc +  ", " + &
						"'" + w_configuratore.istr_riepilogo.flag_tessuto_cliente + "', " + &
						"'" + w_configuratore.istr_riepilogo.flag_tessuto_mant_cliente + "', " + &
						ls_cod_verniciatura_2 + ", " + &
						ls_cod_verniciatura_3 + ", " + &
						ls_des_vernice_fc_2 +  ", " + &
						ls_des_vernice_fc_3 +  ", " + &
						"'" + w_configuratore.istr_riepilogo.flag_addizionale_vernic_2 + "', " + &
						"'" + w_configuratore.istr_riepilogo.flag_addizionale_vernic_3 + "', " + &
						"'" + w_configuratore.istr_riepilogo.flag_fc_vernic_2 + "', " + &
						"'" + w_configuratore.istr_riepilogo.flag_fc_vernic_3 + "',"+ &
						"'" + w_configuratore.istr_riepilogo.flag_azzera_dt + "', "+ &
						ls_cod_modello_telo_finito +  ", " + &
						"'" + w_configuratore.istr_riepilogo.diam_tubetto_telo_sup  + "', " + &
						"'" + w_configuratore.istr_riepilogo.diam_tubetto_telo_inf  + "', " + &
						"'" + w_configuratore.istr_riepilogo.diam_tubetto_mant  + "', " + &
						"'" + w_configuratore.istr_riepilogo.flag_tipo_unione + "', " + &
						"'" + w_configuratore.istr_riepilogo.flag_orientamento_unione + "', " + &
						"'" + w_configuratore.istr_riepilogo.cod_tipo_tasca  + "', " + &
						"'" + w_configuratore.istr_riepilogo.cod_tipo_tasca_1  + "', " + &
						"'" + w_configuratore.istr_riepilogo.cod_tipo_tasca_2  + "', " + &
						ls_flag_mantovana  + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.rapporto_mantovana) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.num_rinforzi) + ", " + &
						f_double_to_string(w_configuratore.istr_riepilogo.dimensione_rinforzo) + ", " + &
						"'" + w_configuratore.istr_riepilogo.cod_reparto_telo  + "' " +" )"

execute immediate :ls_sql_ins;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della Composizione. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
	rollback;
	return -1
end if

choose case str_conf_prodotto.tipo_gestione
	case "ORD_VEN"
		
	if not isnull(w_configuratore.istr_riepilogo.note_esterne) then
		updateblob comp_det_ord_ven
		set        note_esterne 		= :w_configuratore.istr_riepilogo.note_esterne
		where 	  cod_azienda 			= :s_cs_xx.cod_azienda and
					  anno_registrazione = :str_conf_prodotto.anno_documento and
					  num_registrazione 	= :str_conf_prodotto.num_documento and
					  prog_riga_ord_ven 	= :str_conf_prodotto.prog_riga_documento;
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della Composizione. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
			rollback;
			return -1
		end if
	end if

	// --------------------------------- aggiornamento impegnato -------------------------------------
	select flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven_prodotto;
	if sqlca.sqlcode = 0 then
		
		if ls_flag_tipo_det_ven = "M" then
			
			update anag_prodotti  
				set quan_impegnata = quan_impegnata + :ld_quan_prodotto
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :str_conf_prodotto.cod_modello;
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento quantità impegnata in magazzino.", exclamation!, ok!)
				return -1
			end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :str_conf_prodotto.cod_modello;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
	
				ld_quan_raggruppo = f_converti_qta_raggruppo(str_conf_prodotto.cod_modello, ld_quan_prodotto, "M")

				update anag_prodotti  
					set quan_impegnata = quan_impegnata + :ld_quan_raggruppo
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento quantità impegnata in magazzino.", exclamation!, ok!)
					return -1
				end if
				
			end if
			
		end if
	end if
	// -----------------------------------------------------------------------------------------------
end choose


ll_num_mp = upperbound(w_configuratore.istr_riepilogo.mp_automatiche)
for ll_i = 1 to ll_num_mp
	if not isnull(w_configuratore.istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica) and w_configuratore.istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica <> "" and &
	       not isnull(w_configuratore.istr_riepilogo.mp_automatiche[ll_i].prog_mp_automatica) and w_configuratore.istr_riepilogo.mp_automatiche[ll_i].prog_mp_automatica <> 0 then
		ls_sql_ins =	"insert into " + ls_tabella_mp_comp + &
								" (cod_azienda," + &
								"anno_registrazione," + &
								"num_registrazione," + &
								ls_progressivo + "," + &
								"cod_mp_non_richiesta," + &
								"prog_mp_non_richiesta)" + &  
								" values ('" + s_cs_xx.cod_azienda + "', " + &
								string(str_conf_prodotto.anno_documento) + ", " + &
								string(str_conf_prodotto.num_documento) + ", " + &
								string(str_conf_prodotto.prog_riga_documento) + ", '" + &
								w_configuratore.istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica + "', " + &
								string(w_configuratore.istr_riepilogo.mp_automatiche[ll_i].prog_mp_automatica) + ")"
		execute immediate :ls_sql_ins;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Configuratore di Prodotto", "Errore nell' Inserimento della Materia Prima Automatica " + w_configuratore.istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica + ". Dettaglio " + sqlca.sqlerrtext,  exclamation!, ok!)
			rollback;
			return -1
		end if
	end if
next


return 0
end function

public function integer uof_aggiungi_conf_variabile (string as_colonna, string as_tabella, datetime adt_valore_datetime, ref string as_errore);string ls_valore_stringa
dec{4} ld_valore_numero

setnull(ls_valore_stringa)
setnull(ld_valore_numero)

return uof_aggiungi_conf_variabile( as_colonna, as_tabella, "D", ls_valore_stringa, ld_valore_numero, adt_valore_datetime, "",ref as_errore)

end function

public function integer uof_aggiungi_conf_variabile (string as_colonna, string as_tabella, decimal ad_valore_numero, ref string as_errore);string ls_valore_stringa
datetime ldt_valore_datetime

setnull(ls_valore_stringa)
setnull(ldt_valore_datetime)

return uof_aggiungi_conf_variabile( as_colonna, as_tabella, "N", ls_valore_stringa, ad_valore_numero, ldt_valore_datetime, "",ref as_errore)



end function

public function integer uo_elimina_righe_riferite (string fs_tipo_gestione, long fl_anno_registrazione, long fl_num_registrazione, long fl_riga_riferimento, ref string fs_messaggio);string ls_sql, ls_prog_riga, ls_cod_tipo_det_ven, ls_cod_prodotto, ls_progressivo, ls_tabella_det, ls_tabella_note, &
       ls_tabella_corrispondenze, ls_flag_tipo_det_ven,ls_cod_prodotto_raggruppato, ls_tabella_varianti, ls_tabella_variabili
long ll_prog_riga
dec{4} ld_quan_ordine, ld_quan_raggruppo

choose case fs_tipo_gestione
		
	case "OFF_VEN"
		ls_sql = "select prog_riga_off_ven, cod_prodotto, cod_tipo_det_ven, quan_offerta from det_off_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(fl_anno_registrazione) + " and num_registrazione = " + string(fl_num_registrazione) + " and num_riga_appartenenza = " + string(fl_riga_riferimento)
		ls_progressivo = "prog_riga_off_ven"
		ls_tabella_det = "det_off_ven"
		ls_tabella_note = "det_off_ven_note"
		ls_tabella_corrispondenze = "det_off_ven_corrispondenze"
		ls_tabella_varianti = "varianti_det_off_ven"
		ls_tabella_variabili = "det_off_ven_conf_variabili"
		
	case "ORD_VEN"
		ls_sql = "select prog_riga_ord_ven, cod_prodotto, cod_tipo_det_ven, quan_ordine from det_ord_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(fl_anno_registrazione) + " and num_registrazione = " + string(fl_num_registrazione) + " and num_riga_appartenenza = " + string(fl_riga_riferimento)
		ls_progressivo = "prog_riga_ord_ven"
		ls_tabella_det = "det_ord_ven"
		ls_tabella_note = "det_ord_ven_note"
		ls_tabella_corrispondenze = "det_ord_ven_corrispondenze"
		ls_tabella_varianti = "varianti_det_ord_ven"
		ls_tabella_variabili = "det_ord_ven_conf_variabili"
		
end choose

declare cu_riga dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_riga;
do while true
   fetch cu_riga into :ll_prog_riga, :ls_cod_prodotto, :ls_cod_tipo_det_ven, :ld_quan_ordine;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then exit
	
		// CONTROLLO SE PRODOTTO A MAGAZZINO PER RIPRISTINANRE LA QUANTITA' IMPEGNATA
		select flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_det_ven = :ls_cod_tipo_det_ven;
				 
		if sqlca.sqlcode = 0 then
			
			if fs_tipo_gestione = "ORD_VEN" then
				// nel caso si tratti di ordine devo sistemare la quantita impegnata
			
				if ls_flag_tipo_det_ven = "M" then
					
					update anag_prodotti  
						set quan_impegnata = quan_impegnata - :ld_quan_ordine
					 where cod_azienda = :s_cs_xx.cod_azienda and  
							 cod_prodotto = :ls_cod_prodotto;
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento quantità impegnata del prodotto "+ ls_cod_prodotto+ ":PROSEGUO LO STESSO" , exclamation!, ok!)
					end if
					
					// enme 08/1/2006 gestione prodotto raggruppato
					setnull(ls_cod_prodotto_raggruppato)
					
					select cod_prodotto_raggruppato
					into   :ls_cod_prodotto_raggruppato
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_prodotto;
							 
					if not isnull(ls_cod_prodotto_raggruppato) then
						
						ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, 	ld_quan_ordine, "M")
			
						update anag_prodotti  
							set quan_impegnata = quan_impegnata - :ld_quan_raggruppo
						 where cod_azienda = :s_cs_xx.cod_azienda and  
								 cod_prodotto = :ls_cod_prodotto_raggruppato;
						if sqlca.sqlcode = -1 then
							g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento quantità impegnata del prodotto "+ ls_cod_prodotto+ ":PROSEGUO LO STESSO" , exclamation!, ok!)
						end if
						
					end if
					
				end if
				
			end if
			
		end if
		

		//	cancello variabili ipertech
		ls_sql ="delete from " + ls_tabella_variabili + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + " anno_registrazione = " + string(fl_anno_registrazione) + " and " + " num_registrazione = " + string(fl_num_registrazione) + " and " + ls_progressivo + " = " + string(ll_prog_riga)
		execute immediate :ls_sql;
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore in cancellazione tabella "+ ls_tabella_variabili+" : "+sqlca.sqlerrtext
			rollback;
			return -1
		end if		
		
		//	cancello eventuali note della riga
		ls_sql ="delete from " + ls_tabella_note + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + " anno_registrazione = " + string(fl_anno_registrazione) + " and " + " num_registrazione = " + string(fl_num_registrazione) + " and " + ls_progressivo + " = " + string(ll_prog_riga)
		execute immediate :ls_sql;
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore in cancellazione tabella " + ls_tabella_note + " : "+sqlca.sqlerrtext
			rollback;
			return -1
		end if		
		
		// cancello eventuali corrispondenze
		ls_sql ="delete from " + ls_tabella_corrispondenze + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + " anno_registrazione = " + string(fl_anno_registrazione) + " and " + " num_registrazione = " + string(fl_num_registrazione) + " and " + ls_progressivo + " = " + string(ll_prog_riga)
		execute immediate :ls_sql;
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore in cancellazione tabella " + ls_tabella_corrispondenze + " : "+sqlca.sqlerrtext
			rollback;
			return -1
		end if		
		
		//se contesto ORDINE allora cancello eventuali righe di carico produzione
		if fs_tipo_gestione = "ORD_VEN" then
			
			ls_sql ="delete from det_ord_ven_prod where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + " anno_registrazione = " + string(fl_anno_registrazione) + " and " + " num_registrazione = " + string(fl_num_registrazione) + " and " + ls_progressivo + " = " + string(ll_prog_riga)
			execute immediate :ls_sql;
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore in cancellazione tabella det_ord_ven_prod : "+sqlca.sqlerrtext
				rollback;
				return -1
			end if
		end if
		
		//	cancello eventuali note della riga
		ls_sql ="delete from " + ls_tabella_varianti + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + " anno_registrazione = " + string(fl_anno_registrazione) + " and " + " num_registrazione = " + string(fl_num_registrazione) + " and " + ls_progressivo + " = " + string(ll_prog_riga)
		execute immediate :ls_sql;
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore in cancellazione " + ls_tabella_varianti + " : "+sqlca.sqlerrtext
			rollback;
			return -1
		end if		
		
		
		// cancello la riga
		if fs_tipo_gestione = "ORD_VEN" then
			//controllo ed eventuale salvataggio del collegamento della riga ordine a fattura proforma
			if uof_imposta_ds_proforma(fl_anno_registrazione, fl_num_registrazione, ll_prog_riga, fs_messaggio) < 0 then
				rollback;
				return -1
			end if
		end if
		
		ls_sql ="delete from " + ls_tabella_det + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + " anno_registrazione = " + string(fl_anno_registrazione) + " and " + " num_registrazione = " + string(fl_num_registrazione) + " and " + ls_progressivo + " = " + string(ll_prog_riga)
		execute immediate :ls_sql;
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore in cancellazione " + ls_tabella_det + " : "+sqlca.sqlerrtext
			rollback;
			return -1
		end if		
loop
close cu_riga;

return 0
end function

public subroutine uof_set_ripristino_proforma (boolean ab_ripristina_proforma);

ib_ripristina_proforma = ab_ripristina_proforma
end subroutine

public function integer uof_imposta_ds_proforma (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore);string			ls_sql, ls_errore, ls_sql_ds_istanza
datastore	lds_data
long			ll_ret, ll_start

//funzione che salva in un datastore di istanza i collegamenti righe ordini (solo le righe collegate a quella principale) a righe proforma
//per evitare errore di FK si effettua il salvataggio dei collegamenti alle proforma, si fa il set null, 
//quindi a re-inserimento di riga ordine riferita avvenuto si ripristina il collegamento a proforma


ls_sql = 	"select 	od.anno_registrazione, od.num_registrazione, od.prog_riga_ord_ven,"+&
						"fd.anno_registrazione, fd.num_registrazione, fd.prog_riga_fat_ven "+&
			"from det_fat_ven as fd "+&
			"join tes_fat_ven as ft on ft.cod_azienda=fd.cod_azienda and "+&
								"ft.anno_registrazione=fd.anno_registrazione and "+&
								"ft.num_registrazione=fd.num_registrazione "+&
			"join det_ord_ven as od on 	od.cod_azienda=fd.cod_azienda and "+&
												"od.anno_registrazione=fd.anno_reg_ord_ven and "+&
												"od.num_registrazione=fd.num_reg_ord_ven and "+&
												"od.prog_riga_ord_ven=fd.prog_riga_ord_ven "
												
ls_sql_ds_istanza = ls_sql + "where od.cod_azienda='INESISTENTE' "
												
ls_sql += "where od.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"od.anno_registrazione="+string(ai_anno_ordine)+" and "+&
					"od.num_registrazione="+string(al_num_ordine)+" and "+&
					"od.prog_riga_ord_ven="+string(al_riga_ordine)+" and "+&
					"ft.cod_tipo_fat_ven in (select cod_tipo_fat_ven "+&
												  "from tab_tipi_fat_ven "+&
												  "where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
															"flag_tipo_fat_ven in ('P','F'))"

ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_ret > 0 then
	
	//la riga ordine è collegata ad una riga di fattura proforma
	if not ib_ripristina_proforma then
		//crea il datastore di istanza
		ll_ret = guo_functions.uof_crea_datastore(ids_data_proforma, ls_sql_ds_istanza, ls_errore)
		if ll_ret < 0 then
			as_errore = "Errore creazione ds di collegamento a righe fatture proforma (riga ordine" + string(ai_anno_ordine)+"/"+string(al_num_ordine)+"/"+string(al_riga_ordine)+") : " + ls_errore
			return -1
		end if
		
		//imposto semaforo per attivarepoi successivamente il ripristino del collegamento alle righe proforma
		uof_set_ripristino_proforma(true)
	end if
	
	//inserisco nel datastore di istanza quanto richiesto
	ll_start = ids_data_proforma.rowcount() + 1
	ll_ret = lds_data.rowscopy(1, lds_data.rowcount(), primary!, ids_data_proforma, ll_start, primary!)
	
	if ll_ret < 0 then
		as_errore = "Errore in salvataggio collegamento a righe fatture proforma (riga ordine" + string(ai_anno_ordine)+"/"+string(al_num_ordine)+"/"+string(al_riga_ordine)+")"
		return -1
	end if
	
	//faccio set null del riferimento in riga proforma
	if guo_functions.uof_setnull_fatt_proforma(ai_anno_ordine, al_num_ordine, al_riga_ordine, ls_errore) < 0 then
		return -1
	end if
	//----------------------------------------------------------------------------------------------
	
else
	//no proforma collegate
	return 0
end if

return 0
end function

public function boolean uof_get_ripristino_proforma ();

return ib_ripristina_proforma
end function

public function integer uof_aggiungi_conf_variabile (string as_colonna, string as_tabella, string as_flag_tipo_dato, string as_valore_stringa, decimal ad_valore_numero, datetime adt_valore_datetime, string as_des_valore, ref string as_errore);string ls_cod_variabile
long ll_prog_variabile, ll_index

setnull(ls_cod_variabile)
ls_cod_variabile = string( iuo_array.get(as_colonna, ref ll_index) )

if not isnull(ls_cod_variabile) and len(ls_cod_variabile) > 0 and ll_index > 0 then

	choose case lower(as_tabella)
		case "comp_det_off_ven"
			
			INSERT INTO det_off_ven_conf_variabili  
					( cod_azienda,   
					  anno_registrazione,   
					  num_registrazione,   
					  prog_riga_off_ven,   
					  prog_variabile,   
					  cod_variabile,   
					  flag_tipo_dato,   
					  valore_stringa,   
					  valore_numero,   
					  valore_datetime,
					  des_valore)  
			VALUES ( :s_cs_xx.cod_azienda,   
					:str_conf_prodotto.anno_documento,
					:str_conf_prodotto.num_documento,
					:str_conf_prodotto.prog_riga_documento,
					:ll_index,   
					:ls_cod_variabile,
					:as_flag_tipo_dato,   
					:as_valore_stringa,
					:ad_valore_numero,
					:adt_valore_datetime,
					:as_des_valore)  ;
			if sqlca.sqlcode < 0 then
				as_errore = "Errore in scrittura variabile " + ls_cod_variabile + " nella tabella " + as_tabella + ". " + sqlca.sqlerrtext
				return -1
			end if

		case "comp_det_ord_ven"
			
//			select max(prog_variabile)
//			into	:ll_prog_variabile
//			from	det_ord_ven_conf_variabili
//			where cod_azienda =  :s_cs_xx.cod_azienda,   
//					anno_registrazione = :str_conf_prodotto.anno_documento,
//					num_registrazione = :str_conf_prodotto.num_documento,
//					prog_riga_ord_ven = :str_conf_prodotto.prog_riga_documento;
//			if isnull(ll_prog_variabile) or ll_prog_variabile < 1 then
//				ll_prog_variabile = 1
//			else
//				ll_prog_variabile ++
//			end if
			
			INSERT INTO det_ord_ven_conf_variabili  
					( cod_azienda,   
					  anno_registrazione,   
					  num_registrazione,   
					  prog_riga_ord_ven,   
					  prog_variabile,   
					  cod_variabile,   
					  flag_tipo_dato,   
					  valore_stringa,   
					  valore_numero,   
					  valore_datetime,
					  des_valore,
					  flag_visible_in_stampa)  
			VALUES ( :s_cs_xx.cod_azienda,   
					:str_conf_prodotto.anno_documento,
					:str_conf_prodotto.num_documento,
					:str_conf_prodotto.prog_riga_documento,
					:ll_index,   
					:ls_cod_variabile,
					:as_flag_tipo_dato,   
					:as_valore_stringa,
					:ad_valore_numero,
					:adt_valore_datetime,
					:as_des_valore,
					'S')  ;
			if sqlca.sqlcode < 0 then
				as_errore = "Errore in scrittura variabile " + ls_cod_variabile + " nella tabella " + as_tabella + ". " + sqlca.sqlerrtext
				return -1
			end if

	end choose

end if

return 0
end function

public function integer uof_aggiungi_conf_variabile (string as_colonna, string as_tabella, string as_valore_stringa, string as_des_valore, ref string as_errore);dec{4} ld_numero
datetime ldt_valore_datetime

setnull(ld_numero)
setnull(ldt_valore_datetime)

return uof_aggiungi_conf_variabile( as_colonna, as_tabella, "S", as_valore_stringa, ld_numero, ldt_valore_datetime, as_des_valore, ref as_errore)

end function

public function string uof_get_descrizione (string f_nome_tabella, string f_where, string f_colonna_des);long   ll_return, ll_pos
string ls_sql, ls_return1, ls_return2, ls_null,ls_profilo_corrente, ls_db

setnull(ls_null)
if not isnull(f_where) then
	
	if not isnull(s_cs_xx.cod_lingua_applicazione) and not s_cs_xx.admin  then
		string ls_nome_pk_1, ls_nome_pk_2
		
		select nome_pk_1, nome_pk_2
		into   :ls_nome_pk_1, :ls_nome_pk_2
		from   tab_tabelle_lingue
		where  nome_tabella = :f_nome_tabella;
	
		if sqlca.sqlcode = 0 then
			ls_sql =  " SELECT "+ f_nome_tabella +"." + f_colonna_des +", tab_tabelle_lingue_des.descrizione_lingua " + &
			          " FROM   "+f_nome_tabella+" LEFT OUTER JOIN tab_tabelle_lingue_des ON " +  &
						 " tab_tabelle_lingue_des.cod_azienda = " + f_nome_tabella + ".cod_azienda "
							if not isnull(ls_nome_pk_1) and len(ls_nome_pk_1) > 0 then
								ls_sql += " and tab_tabelle_lingue_des.chiave_str_1 = " + f_nome_tabella + "." + ls_nome_pk_1
							end if
							
							if not isnull(ls_nome_pk_2) and len(ls_nome_pk_2) > 0 then
								ls_sql += " and tab_tabelle_lingue_des.chiave_str_2 = " + f_nome_tabella + "." + ls_nome_pk_2
							end if
							ls_sql += " and tab_tabelle_lingue_des.cod_lingua = '" + s_cs_xx.cod_lingua_applicazione + "' "
							ls_sql += " WHERE " + f_where + " AND " + f_nome_tabella + ".cod_azienda = '" + s_cs_xx.cod_azienda + "'"
						 
		else
			ls_sql = "SELECT " + f_colonna_des + ", " + f_colonna_des + " FROM " + f_nome_tabella + " WHERE " + f_where + " AND cod_azienda = '" + s_cs_xx.cod_azienda + "'"
			
		end if
		
	else
		ls_sql = "SELECT " + f_colonna_des + ", " + f_colonna_des + " FROM " + f_nome_tabella + " WHERE " + f_where + " AND cod_azienda = '" + s_cs_xx.cod_azienda + "'"
	end if
	// Verifico tipo di database
	
	ll_return = Registryget(s_cs_xx.chiave_root + "database_" +  s_cs_xx.profilocorrente, "enginetype", ls_db)
	
	if ll_return = -1 then
		g_mb.messagebox("f_des_tabella","Errore nella lettura del motore database dal registro")
		return ""
	end if

	// Verifico se si tratta di ORACLE
	if ls_db = "ORACLE" then
		do
			ll_pos = pos(ls_sql,"+",1)
			if ll_pos > 0 then
				ls_sql = replace(ls_sql,ll_pos,1,"||")
			end if	
		loop while ll_pos > 0
	end if	
	
	
	DECLARE cursore DYNAMIC CURSOR FOR SQLSA ;
	PREPARE SQLSA FROM :ls_sql ;
	OPEN DYNAMIC cursore ;
	FETCH cursore INTO :ls_return1, :ls_return2 ;
	if sqlca.sqlcode = 100 then
		ls_return1 = "< --- >"
		ls_return2 = "< --- >"
	end if
	CLOSE cursore ;
		
	if not isnull(ls_return2) and len(trim(ls_return2)) > 0 then 
		return ls_return2 
	else
		return ls_return1
	end if
else
	return ls_null
end if

return ls_null
end function

on uo_scrivi_tabelle.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_scrivi_tabelle.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

