﻿$PBExportHeader$uo_set_default.sru
$PBExportComments$UO impostazione default passi configuratore
forward
global type uo_set_default from nonvisualobject
end type
end forward

global type uo_set_default from nonvisualobject
end type
global uo_set_default uo_set_default

forward prototypes
public subroutine uof_set_default (datawindow fdw_datawindow, datawindow fdw_inizio, string fs_linea_prodotto, string fs_classe_merc, ref string fs_cod_prodotto_finito, string fs_cod_modello)
public function integer uof_init_codice_prodotto (string as_cod_modello_prodotto, decimal ad_dimensione_1, string as_colore_tessuto, string as_cod_tessuto, string as_cod_comando, string as_cod_verniciatura, ref string as_cod_prodotto_composto, ref string as_errore)
end prototypes

public subroutine uof_set_default (datawindow fdw_datawindow, datawindow fdw_inizio, string fs_linea_prodotto, string fs_classe_merc, ref string fs_cod_prodotto_finito, string fs_cod_modello);//	Funzione che inizializza ai valori di default (flag_default delle rispettive tabelle) i campi
//		della datawindow passata come parametro
//		viene lanciata ogni volta che si passa di Dw in DW
//
// nome: wf_set_default
// tipo: none
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							
//									fdw_datawindow	 DataWindow		valore
//
//		Creata il 31-08-98 
//		Autore Enrico Menegotto
//    	modificata il 31/08/1998
//
//		EnMe 30-03-2012: gestione 3 verniciature
//
//		EnMe 31/03/2020: Alusistemi; gestione tubetti

boolean lb_test
string ls_cod_veloce, ls_linea_prodotto, ls_classe_merceolo, ls_null,ls_vecchio_comando,&
		ls_cod_tessuto_default, ls_tess_fuori_campionario, ls_tess_addizionale, ls_cod_non_a_magazzino, ls_cod_tessuto_mantovana, ls_cod_colore_mantovana, &
		ls_colore_passamaneria, ls_colore_cordolo, ls_cod_verniciatura, ls_vern_fuori_campionario, ls_vern_addizionale, ls_cod_tipo_tasca, ls_diam_tubetto_telo_sup, ls_diam_tubetto_telo_inf, ls_diam_tubetto_mant
long   ll_cont, ll_i
dec{4} ld_hmantovana
datastore lds_data


setnull(ls_null)
ls_linea_prodotto = "%" + fs_linea_prodotto + "%"
ls_classe_merceolo = "%" + fs_classe_merc + "%"
choose case fdw_datawindow.dataobject
	case "d_ext_tessuto", "d_ext_tessuto_dk", "d_ext_tessuto_alu"
		if w_configuratore.istr_flags.flag_tessuto = "N" then
			fdw_datawindow.object.rs_cod_tessuto_manuale_t.color = 8421504
			fdw_datawindow.object.rs_cod_non_a_magazzino_t.color = 8421504
			fdw_datawindow.object.rs_tess_addizionale_t.color = 8421504
			fdw_datawindow.object.rs_tess_fuori_campionario_t.color = 8421504
			fdw_datawindow.object.rs_cod_tessuto.background.color = 79741120
			fdw_datawindow.object.rs_cod_tessuto.protect = 1
			fdw_datawindow.object.rs_cod_non_a_magazzino.background.color = 79741120
			fdw_datawindow.object.rs_cod_non_a_magazzino.protect = 1
			fdw_datawindow.object.rs_tess_addizionale.background.color = 79741120
			fdw_datawindow.object.rs_tess_addizionale.protect = 1
			fdw_datawindow.object.rs_tess_fuori_campionario.background.color = 79741120
			fdw_datawindow.object.rs_tess_fuori_campionario.protect = 1
			fdw_datawindow.object.rs_flag_tessuto_cliente.background.color = 79741120
			fdw_datawindow.object.rs_flag_tessuto_cliente.protect = 1
			fdw_datawindow.object.rs_flag_tessuto_mant_cliente.background.color = 79741120
			fdw_datawindow.object.rs_flag_tessuto_mant_cliente.protect = 1
			
			fdw_datawindow.object.rn_diam_tubetto_telo_sup.background.color = 79741120
			fdw_datawindow.object.rn_diam_tubetto_telo_sup.protect = 1
			fdw_datawindow.object.rn_diam_tubetto_telo_inf.background.color = 79741120
			fdw_datawindow.object.rn_diam_tubetto_telo_inf.protect = 1
			fdw_datawindow.object.rn_diam_tubetto_telo_mant.background.color = 79741120
			fdw_datawindow.object.rn_diam_tubetto_telo_mant.protect = 1
			fdw_datawindow.object.rs_flag_tipo_unione.background.color = 79741120
			fdw_datawindow.object.rs_flag_tipo_unione.protect = 1
			fdw_datawindow.object.rs_flag_orientamento_unione.background.color = 79741120
			fdw_datawindow.object.rs_flag_orientamento_unione.protect = 1
			fdw_datawindow.object.rs_flag_tasca.background.color = 79741120
			fdw_datawindow.object.rs_flag_tasca.protect = 1
			fdw_datawindow.object.rs_cod_tipo_tasca_1.background.color = 79741120
			fdw_datawindow.object.rs_cod_tipo_tasca_1.protect = 1
			fdw_datawindow.object.rs_cod_tipo_tasca_2.background.color = 79741120
			fdw_datawindow.object.rs_cod_tipo_tasca_2.protect = 1
			fdw_datawindow.object.rn_num_rinforzi.background.color = 79741120
			fdw_datawindow.object.rn_num_rinforzi.protect = 1
			fdw_datawindow.object.rn_dimensione_rinforzo.background.color = 79741120
			fdw_datawindow.object.rn_dimensione_rinforzo.protect = 1
			
		elseif isnull(fdw_datawindow.getitemstring(1, "rs_cod_tessuto")) then 
					 
			select cod_tessuto,
					 cod_non_a_magazzino,
					 cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale,
					 cod_tessuto_mantovana,
				    cod_colore_mantovana,
					 colore_passamaneria,
					 colore_cordolo
			 into :ls_cod_tessuto_default,
					:ls_cod_non_a_magazzino,
					:ls_cod_veloce,
					:ls_tess_fuori_campionario,
					:ls_tess_addizionale,
					:ls_cod_tessuto_mantovana,
				   :ls_cod_colore_mantovana,
				 	:ls_colore_passamaneria,
					:ls_colore_cordolo
			 from tab_tessuti  
			where cod_azienda = :s_cs_xx.cod_azienda and
					(cod_linea_prodotto like :ls_linea_prodotto or
					cod_linea_prodotto is null or
					cod_linea_prodotto = '') and
					flag_default = 'S';
	
			if SQLCA.sqlcode = 0 then
				w_configuratore.istr_riepilogo.cod_tessuto = ls_cod_tessuto_default
				w_configuratore.istr_riepilogo.cod_non_a_magazzino = ls_cod_non_a_magazzino
				fdw_datawindow.setitem(1, "rs_cod_tessuto", ls_cod_tessuto_default)
				fdw_datawindow.setitem(1, "rs_cod_tessuto_manuale", ls_cod_tessuto_default)
				fdw_datawindow.setitem(1, "rs_cod_non_a_magazzino", ls_cod_non_a_magazzino)
				fdw_datawindow.setitem(1, "rs_tess_fuori_campionario", ls_tess_fuori_campionario)
				fdw_datawindow.setitem(1, "rs_tess_addizionale", ls_tess_addizionale)
				fs_cod_prodotto_finito = Replace(fs_cod_prodotto_finito, 5, 1, ls_cod_veloce)
				if w_configuratore.istr_flags.flag_tessuto_mantovana = "S" then
					fdw_datawindow.setitem(1, "rs_cod_tessuto_mant", ls_cod_tessuto_mantovana)
					fdw_datawindow.setitem(1, "rs_cod_tessuto_mant_manuale", ls_cod_tessuto_mantovana)
					fdw_datawindow.setitem(1, "rs_cod_mant_non_a_magazzino", ls_cod_colore_mantovana)
					w_configuratore.istr_riepilogo.cod_tessuto_mant = ls_cod_tessuto_mantovana
					w_configuratore.istr_riepilogo.cod_mant_non_a_magazzino = ls_cod_colore_mantovana
				end if
				if w_configuratore.istr_flags.flag_cordolo = "S" then					
					fdw_datawindow.setitem(1, "rs_colore_cordolo", ls_colore_cordolo)
					w_configuratore.istr_riepilogo.colore_cordolo = ls_colore_cordolo
				end if
				if w_configuratore.istr_flags.flag_passamaneria = "S" then
					fdw_datawindow.setitem(1, "rs_colore_passamaneria", ls_colore_passamaneria)
					w_configuratore.istr_riepilogo.colore_passamaneria = ls_colore_passamaneria
				end if
				if w_configuratore.istr_flags.flag_tipo_mantovana = "S" then
					w_configuratore.istr_riepilogo.flag_tipo_mantovana = "C"
				end if
				
		elseif SQLCA.sqlcode = 100 then
				w_configuratore.istr_riepilogo.cod_tessuto = ""
				w_configuratore.istr_riepilogo.cod_non_a_magazzino = ""
				fdw_datawindow.setitem(1, "rs_cod_tessuto", "")
				fdw_datawindow.setitem(1, "rs_cod_tessuto_manuale", "")
				fdw_datawindow.setitem(1, "rs_cod_non_a_magazzino", "")				
				fdw_datawindow.setitem(1, "rs_tess_fuori_campionario", "N")
				fdw_datawindow.setitem(1, "rs_tess_addizionale", "N")		
			else
				g_mb.messagebox("Estrazione Tessuto di Default", "Errore durante l'Estrazione")
			end if
		end if
		if w_configuratore.istr_flags.flag_tessuto_mantovana = "N" then
			fdw_datawindow.object.rs_cod_tessuto_mant_manuale_t.color = 8421504
			fdw_datawindow.object.rs_cod_mant_non_a_magazzino_t.color = 8421504
			fdw_datawindow.object.rs_cod_tessuto_mant.background.color = 79741120
			fdw_datawindow.object.rs_cod_tessuto_mant.protect = 1
			fdw_datawindow.object.rs_cod_mant_non_a_magazzino.background.color = 79741120
			fdw_datawindow.object.rs_cod_mant_non_a_magazzino.protect = 1

			fdw_datawindow.object.rs_flag_mantovana.background.color = 79741120
			fdw_datawindow.object.rs_flag_mantovana.protect = 1
			fdw_datawindow.object.rn_rapporto_mantovana.background.color = 79741120
			fdw_datawindow.object.rn_rapporto_mantovana.protect = 1
			
		elseif not isnull(fdw_datawindow.getitemstring(1, "rs_cod_tessuto")) and isnull(fdw_datawindow.getitemstring(1, "rs_cod_tessuto_mant")) and str_conf_prodotto.stato <> "M" then 
			if w_configuratore.istr_riepilogo.flag_mantovana = "S" then
				w_configuratore.istr_riepilogo.cod_tessuto_mant = fdw_datawindow.getitemstring(1, "rs_cod_tessuto")
				w_configuratore.istr_riepilogo.cod_mant_non_a_magazzino = fdw_datawindow.getitemstring(1, "rs_cod_non_a_magazzino")
				fdw_datawindow.setitem(1, "rs_cod_tessuto_mant_manuale", fdw_datawindow.getitemstring(1, "rs_cod_tessuto"))
				fdw_datawindow.setitem(1, "rs_cod_tessuto_mant", fdw_datawindow.getitemstring(1, "rs_cod_tessuto"))
				fdw_datawindow.setitem(1, "rs_cod_mant_non_a_magazzino", fdw_datawindow.getitemstring(1, "rs_cod_non_a_magazzino"))
			end if
			// richiesto da Beatrice 01/04/2010
			select numero
			into   :ld_hmantovana
			from   parametri_azienda
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_parametro = 'HMN';
			if sqlca.sqlcode = 0 then
				if isnull(ld_hmantovana) then ld_hmantovana = 0
				fdw_datawindow.setitem(1, "rn_alt_mantovana", ld_hmantovana)
			else
				fdw_datawindow.setitem(1, "rn_alt_mantovana", 25)
			end if
		end if
		if w_configuratore.istr_flags.flag_altezza_mantovana = "N" then
			fdw_datawindow.object.rn_alt_mantovana_t.color = 8421504
			fdw_datawindow.object.rn_alt_mantovana.background.color = 79741120
			fdw_datawindow.object.rn_alt_mantovana.protect = 1
		end if
		if w_configuratore.istr_flags.flag_tipo_mantovana = "N" then
			fdw_datawindow.object.rs_flag_tipo_mantovana_t.color = 8421504
			fdw_datawindow.object.rs_flag_tipo_mantovana.background.color = 79741120
			fdw_datawindow.object.rs_flag_tipo_mantovana.protect = 1
		end if
		if w_configuratore.istr_flags.flag_cordolo = "N" then
			fdw_datawindow.object.rs_flag_cordolo_t.color = 8421504
			fdw_datawindow.object.rs_colore_cordolo.background.color = 79741120
			fdw_datawindow.object.rs_colore_cordolo.protect = 1
		end if
		if w_configuratore.istr_flags.flag_passamaneria = "N" then
			fdw_datawindow.object.rs_flag_passamaneria_t.color = 8421504
			fdw_datawindow.object.rs_colore_passamaneria.background.color = 79741120
			fdw_datawindow.object.rs_colore_passamaneria.protect = 1
		end if
		
		if fdw_datawindow.dataobject =  "d_ext_tessuto_alu" and str_conf_prodotto.stato <> "M" then
			
			// default tasca 1
			if len( fdw_datawindow.getitemstring(1,"rs_flag_tasca") ) <= 0 then
				select cod_tipo_tasca
				into	:ls_cod_tipo_tasca 
				from	tab_flags_config_tipo_tasca
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_modello = :fs_cod_modello and
						flag_default = 'S' ;
				if sqlca.sqlcode = 0 then
					fdw_datawindow.setitem(1, "rs_flag_tasca", ls_cod_tipo_tasca)
				end if
			end if
			
			if len( fdw_datawindow.getitemstring(1,"rs_cod_tipo_tasca_1") ) <= 0 then
				// default tasca 2
				select cod_tipo_tasca
				into	:ls_cod_tipo_tasca 
				from	tab_flags_config_tipo_tasca
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_modello = :fs_cod_modello and
						flag_default_1 = 'S' ;
				if sqlca.sqlcode = 0 then
					fdw_datawindow.setitem(1, "rs_cod_tipo_tasca_1", ls_cod_tipo_tasca)
				end if
			end if 
			
			if len( fdw_datawindow.getitemstring(1,"rs_cod_tipo_tasca_2") ) <= 0 then
				// default tasca 3
				select cod_tipo_tasca
				into	:ls_cod_tipo_tasca 
				from	tab_flags_config_tipo_tasca
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_modello = :fs_cod_modello and
						flag_default_2 = 'S' ;
				if sqlca.sqlcode = 0 then
					fdw_datawindow.setitem(1, "rs_cod_tipo_tasca_2", ls_cod_tipo_tasca)
				end if
			end if
			
			// carico valore default tubetto
			
			select diam_tubetto_telo_sup, 
					diam_tubetto_telo_inf, 
					diam_tubetto_mant
			into	:ls_diam_tubetto_telo_sup, 
					:ls_diam_tubetto_telo_inf, 
					:ls_diam_tubetto_mant
			from	tab_flags_configuratore
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_modello = :fs_cod_modello;
			if sqlca.sqlcode = 0 then
				if fdw_datawindow.getitemstring(1,"rn_diam_tubetto_telo_sup") = "0" or len(fdw_datawindow.getitemstring(1,"rn_diam_tubetto_telo_sup")) <= 0 then fdw_datawindow.setitem(1, "rn_diam_tubetto_telo_sup", ls_diam_tubetto_telo_sup)
				if fdw_datawindow.getitemstring(1,"rn_diam_tubetto_telo_inf") = "0" or len(fdw_datawindow.getitemstring(1,"rn_diam_tubetto_telo_inf")) >=0 then fdw_datawindow.setitem(1, "rn_diam_tubetto_telo_inf", ls_diam_tubetto_telo_inf)
				if fdw_datawindow.getitemstring(1,"rn_diam_tubetto_mant") = "0" or len(fdw_datawindow.getitemstring(1,"rn_diam_tubetto_mant")) >=0 then fdw_datawindow.setitem(1, "rn_diam_tubetto_mant", ls_diam_tubetto_mant)
			end if
			
			// carico valore dei rinforzi
			ll_i = guo_functions.uof_crea_datastore( lds_data, g_str.format("select dim_x, num_rinforzi, dimensione_rinforzo from tab_flags_config_rinforzi where cod_azienda = '$1' and cod_modello = '$2' order by dim_x ",s_cs_xx.cod_azienda,fs_cod_modello ))
			if ll_i > 0 then
				for ll_i = 1 to lds_data.rowcount()
					if w_configuratore.istr_riepilogo.dimensione_1 <= lds_data.getitemnumber(ll_i,1) then
						if fdw_datawindow.getitemnumber(1,"rn_num_rinforzi") = 0 then fdw_datawindow.setitem(1, "rn_num_rinforzi", lds_data.getitemnumber(ll_i,2))
						if fdw_datawindow.getitemnumber(1,"rn_dimensione_rinforzo") = 0 then fdw_datawindow.setitem(1, "rn_dimensione_rinforzo", lds_data.getitemnumber(ll_i,3))
						exit
					end if						
				next
			end if
			destroy lds_data
		end if
		

	case "d_ext_vern_lastra"

		if w_configuratore.istr_flags.flag_verniciatura = "N" then
			fdw_datawindow.object.rs_cod_verniciatura_manuale_t.color = 8421504
			fdw_datawindow.object.rs_cod_verniciatura_t.color = 8421504
			fdw_datawindow.object.rs_vern_addizionale_t.color = 8421504
			fdw_datawindow.object.rs_vern_fuori_campionario_t.color = 8421504
			fdw_datawindow.object.rs_cod_verniciatura_manuale.background.color = 79741120
			fdw_datawindow.object.rs_cod_verniciatura_manuale.protect = 1
			fdw_datawindow.object.rs_cod_verniciatura.background.color = 79741120
			fdw_datawindow.object.rs_cod_verniciatura.protect = 1
			fdw_datawindow.object.rs_vern_addizionale.background.color = 79741120
			fdw_datawindow.object.rs_vern_addizionale.protect = 1
			fdw_datawindow.object.rs_vern_fuori_campionario.background.color = 79741120
			fdw_datawindow.object.rs_vern_fuori_campionario.protect = 1
		else
		   // aggiunto da EnMe x spec_varie_5 funz. 11  (21/1/02)
			// carico la DDDW verniciature sempre e solo con le vern.possibili per il modello
			f_PO_LoadDDDW_DW(fdw_datawindow,"rs_cod_verniciatura",sqlca,&
								  "anag_prodotti","cod_prodotto","des_prodotto",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_verniciatura from tab_modelli_verniciature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_modello = '" + fs_cod_modello + "' and num_verniciatura=1 and flag_blocco = 'N')")
			
			if isnull(fdw_datawindow.getitemstring(1, "rs_cod_verniciatura")) then
				
				
				lb_test = false
				select count(*)
				into   :ll_cont 
				from   tab_modelli_verniciature  
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_modello = :fs_cod_modello and  
						 num_verniciatura = 1 and
						 flag_default = 'S' and
						 flag_blocco  = 'N';
				if ll_cont > 1 then
					g_mb.messagebox("Ricerca verniciatura","Attenzione! Per questo modello è stata specificata più di una verniciatura di default !", Information!)
					lb_test = false
				elseif ll_cont < 1 then
					lb_test = false
				else
					f_PO_LoadDDDW_DW(fdw_datawindow,"rs_cod_verniciatura",sqlca,&
										  "anag_prodotti","cod_prodotto","des_prodotto",&
										  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_verniciatura from tab_modelli_verniciature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_modello = '" + fs_cod_modello + "' and num_verniciatura=1 and flag_blocco = 'N')")
					select cod_verniciatura  
					into   :ls_cod_verniciatura  
					from   tab_modelli_verniciature  
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_modello = :fs_cod_modello and  
							 num_verniciatura=1 and
							 flag_default = 'S' and
							 flag_blocco  = 'N';
					if sqlca.sqlcode = 0 then			// esiste verniciatura di default
						select cod_veloce,
								 flag_fuori_campionario,
								 flag_addizionale
						into   :ls_cod_veloce,
								 :ls_vern_fuori_campionario,
								 :ls_vern_addizionale
						from   tab_verniciatura
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_verniciatura = :ls_cod_verniciatura;
						if sqlca.sqlcode = 0 then
							lb_test = true
						else
							g_mb.messagebox("Estrazione Verniciatura di Default", "Dettaglio errore: " + sqlca.sqlerrtext)
						end if
					end if
				end if		
				if lb_test then
					w_configuratore.istr_riepilogo.cod_verniciatura = ls_cod_verniciatura
					fdw_datawindow.setitem(1, "rs_cod_verniciatura", ls_cod_verniciatura)
					fdw_datawindow.setitem(1, "rs_cod_verniciatura_manuale", ls_cod_verniciatura)
					fdw_datawindow.setitem(1, "rs_vern_fuori_campionario", ls_vern_fuori_campionario)
					fdw_datawindow.setitem(1, "rs_vern_addizionale", ls_vern_addizionale)
					if ls_vern_fuori_campionario = "S" then
						fdw_datawindow.object.rs_des_vernice_fc_t.color = 8388608
						fdw_datawindow.object.rs_des_vernice_fc.background.color = 16777215
						fdw_datawindow.object.rs_des_vernice_fc.protect = 0
					else
						fdw_datawindow.object.rs_des_vernice_fc_t.color = 8421504
						fdw_datawindow.object.rs_des_vernice_fc.background.color = 79741120
						fdw_datawindow.object.rs_des_vernice_fc.protect = 1
					end if
					fs_cod_prodotto_finito = Replace(fs_cod_prodotto_finito, 6, 1, ls_cod_veloce)
				else
					w_configuratore.istr_riepilogo.cod_verniciatura = ""
					fdw_datawindow.setitem(1, "rs_cod_verniciatura", "")
					fdw_datawindow.setitem(1, "rs_cod_verniciatura_manuale", "")
					fdw_datawindow.setitem(1, "rs_vern_fuori_campionario", "N")
					fdw_datawindow.setitem(1, "rs_vern_addizionale", "N")		
					fdw_datawindow.object.rs_des_vernice_fc_t.color = 8421504
					fdw_datawindow.object.rs_des_vernice_fc.background.color = 79741120
					fdw_datawindow.object.rs_des_vernice_fc.protect = 1
				end if
			end if
		end if
		
		if w_configuratore.istr_flags.flag_colore_lastra = "N" then
			fdw_datawindow.object.rs_cod_colore_lastra_manuale_t.color = 8421504
			fdw_datawindow.object.rs_cod_colore_lastra_t.color = 8421504
			fdw_datawindow.object.rs_cod_colore_lastra_manuale.background.color = 79741120
			fdw_datawindow.object.rs_cod_colore_lastra_manuale.protect = 1
			fdw_datawindow.object.rs_cod_colore_lastra.background.color = 79741120
			fdw_datawindow.object.rs_cod_colore_lastra.protect = 1
			fdw_datawindow.object.rs_col_addizionale_t.color = 8421504
			fdw_datawindow.object.rs_col_fuori_campionario_t.color = 8421504
			fdw_datawindow.object.rs_col_addizionale.background.color = 79741120
			fdw_datawindow.object.rs_col_addizionale.protect = 1
			fdw_datawindow.object.rs_col_fuori_campionario.background.color = 79741120
			fdw_datawindow.object.rs_col_fuori_campionario.protect = 1
		elseif isnull(fdw_datawindow.getitemstring(1, "rs_cod_colore_lastra")) then
			string ls_cod_colore_lastra_default, ls_col_fuori_campionario, ls_col_addizionale					 

			select cod_colore_lastra,
					 cod_veloce,
					 flag_fuori_campionario,
					 flag_addizionale
			 into :ls_cod_colore_lastra_default,
			 		:ls_cod_veloce,
					:ls_col_fuori_campionario,
					:ls_col_addizionale
			 from tab_colori_lastre
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_modello = :fs_cod_prodotto_finito and
					flag_default = 'S';
	
			if SQLCA.sqlcode = 0 then
				w_configuratore.istr_riepilogo.cod_colore_lastra = ls_cod_colore_lastra_default
				fdw_datawindow.setitem(1, "rs_cod_colore_lastra", ls_cod_colore_lastra_default)
				fdw_datawindow.setitem(1, "rs_cod_colore_lastra_manuale", ls_cod_colore_lastra_default)
				fdw_datawindow.setitem(1, "rs_col_fuori_campionario", ls_col_fuori_campionario)
				fdw_datawindow.setitem(1, "rs_col_addizionale", ls_col_addizionale)
				fs_cod_prodotto_finito = Replace(fs_cod_prodotto_finito, 7, 1, ls_cod_veloce)
			elseif SQLCA.sqlcode = 100 then
				w_configuratore.istr_riepilogo.cod_colore_lastra = ""
				fdw_datawindow.setitem(1, "rs_cod_colore_lastra", "")
				fdw_datawindow.setitem(1, "rs_cod_colore_lastra_manuale", "")
				fdw_datawindow.setitem(1, "rs_col_fuori_campionario", "N")
				fdw_datawindow.setitem(1, "rs_col_addizionale", "N")		
			else
				g_mb.messagebox("Estrazione Colore Lastra di Default", "Errore durante l'Estrazione")
			end if
		end if

		if w_configuratore.istr_flags.flag_tipo_lastra = "N" then
			fdw_datawindow.object.rs_cod_tipo_lastra_manuale_t.color = 8421504
			fdw_datawindow.object.rs_cod_tipo_lastra_t.color = 8421504
			fdw_datawindow.object.rs_cod_tipo_lastra_manuale.background.color = 79741120
			fdw_datawindow.object.rs_cod_tipo_lastra_manuale.protect = 1
			fdw_datawindow.object.rs_cod_tipo_lastra.background.color = 79741120
			fdw_datawindow.object.rs_cod_tipo_lastra.protect = 1
			fdw_datawindow.object.rs_tipo_addizionale_t.color = 8421504
			fdw_datawindow.object.rs_tipo_fuori_campionario_t.color = 8421504
			fdw_datawindow.object.rs_tipo_addizionale.background.color = 79741120
			fdw_datawindow.object.rs_tipo_addizionale.protect = 1
			fdw_datawindow.object.rs_tipo_fuori_campionario.background.color = 79741120
			fdw_datawindow.object.rs_tipo_fuori_campionario.protect = 1
		elseif isnull(fdw_datawindow.getitemstring(1, "rs_cod_tipo_lastra")) then
			string ls_cod_tipo_lastra_default, ls_tipo_addizionale, ls_tipo_fuori_campionario
					 
			select cod_tipo_lastra,
					 flag_fuori_campionario,
					 flag_addizionale
			 into :ls_cod_tipo_lastra_default,
					:ls_tipo_fuori_campionario,
					:ls_tipo_addizionale
			 from tab_tipi_lastre
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_modello =  :fs_cod_prodotto_finito and
					flag_default = 'S';
	
			if SQLCA.sqlcode = 0 then
				w_configuratore.istr_riepilogo.cod_tipo_lastra = ls_cod_tipo_lastra_default
				fdw_datawindow.setitem(1, "rs_cod_tipo_lastra", ls_cod_tipo_lastra_default)
				fdw_datawindow.setitem(1, "rs_cod_tipo_lastra_manuale", ls_cod_tipo_lastra_default)
				fdw_datawindow.setitem(1, "rs_tipo_fuori_campionario", ls_tipo_fuori_campionario)
				fdw_datawindow.setitem(1, "rs_tipo_addizionale", ls_tipo_addizionale)
				fs_cod_prodotto_finito = Replace(fs_cod_prodotto_finito, 8, 1, ls_cod_veloce)
			elseif SQLCA.sqlcode = 100 then
				w_configuratore.istr_riepilogo.cod_tipo_lastra = ""
				fdw_datawindow.setitem(1, "rs_cod_tipo_lastra", "")
				fdw_datawindow.setitem(1, "rs_cod_tipo_lastra_manuale", "")
				fdw_datawindow.setitem(1, "rs_tipo_fuori_campionario", "N")
				fdw_datawindow.setitem(1, "rs_tipo_addizionale", "N")		
			else
				g_mb.messagebox("Estrazione Tipo Lastra di Default", "Errore durante l'Estrazione")
			end if
		end if

		
		if w_configuratore.istr_flags.flag_spessore_lastra = "N" then
			fdw_datawindow.object.rs_spessore_lastra_t.color = 8421504
			fdw_datawindow.object.rs_spessore_lastra.background.color = 79741120
			fdw_datawindow.object.rs_spessore_lastra.protect = 1
		end if


	// ********************
	// gestione 3  VERNICIATURE
	// ********************
	case "d_ext_verniciature"

		if w_configuratore.istr_flags.flag_verniciatura = "N" then
			fdw_datawindow.object.rs_cod_verniciatura.background.color = 79741120
			fdw_datawindow.object.rs_cod_verniciatura.protect = 1
			fdw_datawindow.object.rs_vern_addizionale.background.color = 79741120
			fdw_datawindow.object.rs_vern_addizionale.protect = 1
			fdw_datawindow.object.rs_vern_fuori_campionario.background.color = 79741120
			fdw_datawindow.object.rs_vern_fuori_campionario.protect = 1
			fdw_datawindow.object.rs_des_vernice_fc.protect = 1
			fdw_datawindow.object.rs_des_vernice_fc.background.color = 79741120
		else
		   // aggiunto da EnMe x spec_varie_5 funz. 11  (21/1/02)
			// carico la DDDW verniciature sempre e solo con le vern.possibili per il modello
			f_PO_LoadDDDW_DW(fdw_datawindow,"rs_cod_verniciatura",sqlca,&
								  "anag_prodotti","cod_prodotto","des_prodotto",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_verniciatura from tab_modelli_verniciature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_modello = '" + fs_cod_modello + "' and num_verniciatura=1 and flag_blocco = 'N')")
			
			if isnull(fdw_datawindow.getitemstring(1, "rs_cod_verniciatura")) then
				
				
				lb_test = false
				select count(*)
				into   :ll_cont 
				from   tab_modelli_verniciature  
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_modello = :fs_cod_modello and  
						 num_verniciatura = 1 and
						 flag_default = 'S' and
						 flag_blocco  = 'N';
				if ll_cont > 1 then
					g_mb.messagebox("Ricerca verniciatura","Attenzione! Per questo modello è stata specificata più di una verniciatura di default !", Information!)
					lb_test = false
				elseif ll_cont < 1 then
					lb_test = false
				else
					f_PO_LoadDDDW_DW(fdw_datawindow,"rs_cod_verniciatura",sqlca,&
										  "anag_prodotti","cod_prodotto","des_prodotto",&
										  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_verniciatura from tab_modelli_verniciature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_modello = '" + fs_cod_modello + "' and num_verniciatura=1 and flag_blocco = 'N')")
					select cod_verniciatura  
					into   :ls_cod_verniciatura  
					from   tab_modelli_verniciature  
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_modello = :fs_cod_modello and  
							 num_verniciatura=1 and
							 flag_default = 'S' and
							 flag_blocco  = 'N';
					if sqlca.sqlcode = 0 then			// esiste verniciatura di default
						select cod_veloce,
								 flag_fuori_campionario,
								 flag_addizionale
						into   :ls_cod_veloce,
								 :ls_vern_fuori_campionario,
								 :ls_vern_addizionale
						from   tab_verniciatura
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_verniciatura = :ls_cod_verniciatura;
						if sqlca.sqlcode = 0 then
							lb_test = true
						else
							g_mb.messagebox("Estrazione Verniciatura di Default", "Dettaglio errore: " + sqlca.sqlerrtext)
						end if
					end if
				end if		
				if lb_test then
					w_configuratore.istr_riepilogo.cod_verniciatura = ls_cod_verniciatura
					fdw_datawindow.setitem(1, "rs_cod_verniciatura", ls_cod_verniciatura)
					fdw_datawindow.setitem(1, "rs_cod_verniciatura_manuale", ls_cod_verniciatura)
					fdw_datawindow.setitem(1, "rs_vern_fuori_campionario", ls_vern_fuori_campionario)
					fdw_datawindow.setitem(1, "rs_vern_addizionale", ls_vern_addizionale)
					if ls_vern_fuori_campionario = "S" then
						fdw_datawindow.object.rs_des_vernice_fc.background.color = 16777215
						fdw_datawindow.object.rs_des_vernice_fc.protect = 0
					else
						fdw_datawindow.object.rs_des_vernice_fc.background.color = 79741120
						fdw_datawindow.object.rs_des_vernice_fc.protect = 1
					end if
					fs_cod_prodotto_finito = Replace(fs_cod_prodotto_finito, 6, 1, ls_cod_veloce)
				else
					w_configuratore.istr_riepilogo.cod_verniciatura = ""
					fdw_datawindow.setitem(1, "rs_cod_verniciatura", "")
					fdw_datawindow.setitem(1, "rs_cod_verniciatura_manuale", "")
					fdw_datawindow.setitem(1, "rs_vern_fuori_campionario", "N")
					fdw_datawindow.setitem(1, "rs_vern_addizionale", "N")		
					fdw_datawindow.object.rs_des_vernice_fc.background.color = 79741120
					fdw_datawindow.object.rs_des_vernice_fc.protect = 1
				end if
			end if
		end if
		
		//****** SECONDA VERNICIATURA *******
		
		if w_configuratore.istr_flags.flag_verniciatura_2 = "N" then
//			fdw_datawindow.object.rs_cod_verniciatura_manuale_2.background.color = 79741120
//			fdw_datawindow.object.rs_cod_verniciatura_manuale_2.protect = 1
			fdw_datawindow.object.rs_cod_verniciatura_2.background.color = 79741120
			fdw_datawindow.object.rs_cod_verniciatura_2.protect = 1
			fdw_datawindow.object.rs_vern_addizionale_2.background.color = 79741120
			fdw_datawindow.object.rs_vern_addizionale_2.protect = 1
			fdw_datawindow.object.rs_vern_fuori_campionario_2.background.color = 79741120
			fdw_datawindow.object.rs_vern_fuori_campionario_2.protect = 1
			fdw_datawindow.object.rs_des_vernice_fc_2.protect = 1
			fdw_datawindow.object.rs_des_vernice_fc_2.background.color = 79741120
		else
		   // aggiunto da EnMe x spec_varie_5 funz. 11  (21/1/02)
			// carico la DDDW verniciature sempre e solo con le vern.possibili per il modello
			f_PO_LoadDDDW_DW(fdw_datawindow,"rs_cod_verniciatura_2",sqlca,&
								  "anag_prodotti","cod_prodotto","des_prodotto",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_verniciatura from tab_modelli_verniciature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_modello = '" + fs_cod_modello + "' and num_verniciatura=2 and flag_blocco = 'N')")
			
			if isnull(fdw_datawindow.getitemstring(1, "rs_cod_verniciatura_2")) then
				lb_test = false
				select count(*)
				into   :ll_cont 
				from   tab_modelli_verniciature  
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_modello = :fs_cod_modello and  
						 num_verniciatura = 2 and
						 flag_default = 'S' and
						 flag_blocco  = 'N';
				if ll_cont > 1 then
					g_mb.messagebox("Ricerca verniciatura","Attenzione! Per questo modello è stata specificata più di una verniciatura di default !", Information!)
					lb_test = false
				elseif ll_cont < 1 then
					lb_test = false
				else
					f_PO_LoadDDDW_DW(fdw_datawindow,"rs_cod_verniciatura_2",sqlca,&
										  "anag_prodotti","cod_prodotto","des_prodotto",&
										  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_verniciatura from tab_modelli_verniciature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_modello = '" + fs_cod_modello + "' and num_verniciatura=2 and flag_blocco = 'N')")
					select cod_verniciatura  
					into   :ls_cod_verniciatura  
					from   tab_modelli_verniciature  
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_modello = :fs_cod_modello and  
							 num_verniciatura=2 and
							 flag_default = 'S' and
							 flag_blocco  = 'N';
							 
					if sqlca.sqlcode = 0 then			// esiste verniciatura di default
						select cod_veloce,
								 flag_fuori_campionario,
								 flag_addizionale
						into   :ls_cod_veloce,
								 :ls_vern_fuori_campionario,
								 :ls_vern_addizionale
						from   tab_verniciatura
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_verniciatura = :ls_cod_verniciatura;
						if sqlca.sqlcode = 0 then
							lb_test = true
						else
							g_mb.messagebox("Estrazione Verniciatura di Default", "Dettaglio errore: " + sqlca.sqlerrtext)
						end if
					end if
				end if		
				
				if lb_test then
					w_configuratore.istr_riepilogo.cod_verniciatura_2 = ls_cod_verniciatura
					fdw_datawindow.setitem(1, "rs_cod_verniciatura_2", ls_cod_verniciatura)
					fdw_datawindow.setitem(1, "rs_cod_verniciatura_manuale_2", ls_cod_verniciatura)
					fdw_datawindow.setitem(1, "rs_vern_fuori_campionario_2", ls_vern_fuori_campionario)
					fdw_datawindow.setitem(1, "rs_vern_addizionale_2", ls_vern_addizionale)
					if ls_vern_fuori_campionario = "S" then
						fdw_datawindow.object.rs_des_vernice_fc_2.background.color = 16777215
						fdw_datawindow.object.rs_des_vernice_fc_2.protect = 0
					else
						fdw_datawindow.object.rs_des_vernice_fc_2.background.color = 79741120
						fdw_datawindow.object.rs_des_vernice_fc_2.protect = 1
					end if
					fs_cod_prodotto_finito = Replace(fs_cod_prodotto_finito, 6, 1, ls_cod_veloce)
				else
					w_configuratore.istr_riepilogo.cod_verniciatura_2 = ""
					fdw_datawindow.setitem(1, "rs_cod_verniciatura_2", "")
					fdw_datawindow.setitem(1, "rs_cod_verniciatura_manuale_2", "")
					fdw_datawindow.setitem(1, "rs_vern_fuori_campionario_2", "N")
					fdw_datawindow.setitem(1, "rs_vern_addizionale_2", "N")		
					fdw_datawindow.object.rs_des_vernice_fc_2.background.color = 79741120
					fdw_datawindow.object.rs_des_vernice_fc_2.protect = 1
				end if
			end if
		end if
		
		//****** TERZA VERNICIATURA *******
		
		if w_configuratore.istr_flags.flag_verniciatura_3 = "N" then
//			fdw_datawindow.object.rs_cod_verniciatura_manuale_3.background.color = 79741120
//			fdw_datawindow.object.rs_cod_verniciatura_manuale_3.protect = 1
			fdw_datawindow.object.rs_cod_verniciatura_3.background.color = 79741120
			fdw_datawindow.object.rs_cod_verniciatura_3.protect = 1
			fdw_datawindow.object.rs_vern_addizionale_3.background.color = 79741120
			fdw_datawindow.object.rs_vern_addizionale_3.protect = 1
			fdw_datawindow.object.rs_vern_fuori_campionario_3.background.color = 79741120
			fdw_datawindow.object.rs_vern_fuori_campionario_3.protect = 1
			fdw_datawindow.object.rs_des_vernice_fc_3.protect = 1
			fdw_datawindow.object.rs_des_vernice_fc_3.background.color = 79741120
		else
		   // aggiunto da EnMe x spec_varie_5 funz. 11  (21/1/02)
			// carico la DDDW verniciature sempre e solo con le vern.possibili per il modello
			f_PO_LoadDDDW_DW(fdw_datawindow,"rs_cod_verniciatura_3",sqlca,&
								  "anag_prodotti","cod_prodotto","des_prodotto",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_verniciatura from tab_modelli_verniciature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_modello = '" + fs_cod_modello + "' and num_verniciatura=3 and flag_blocco = 'N')")
			
			if isnull(fdw_datawindow.getitemstring(1, "rs_cod_verniciatura_3")) then
				lb_test = false
				select count(*)
				into   :ll_cont 
				from   tab_modelli_verniciature  
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_modello = :fs_cod_modello and  
						 num_verniciatura = 3 and
						 flag_default = 'S' and
						 flag_blocco  = 'N';
				if ll_cont > 1 then
					g_mb.messagebox("Ricerca verniciatura","Attenzione! Per questo modello è stata specificata più di una verniciatura di default !", Information!)
					lb_test = false
				elseif ll_cont < 1 then
					lb_test = false
				else
					f_PO_LoadDDDW_DW(fdw_datawindow,"rs_cod_verniciatura_3",sqlca,&
										  "anag_prodotti","cod_prodotto","des_prodotto",&
										  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_verniciatura from tab_modelli_verniciature where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_modello = '" + fs_cod_modello + "' and num_verniciatura=3 and flag_blocco = 'N')")
					select cod_verniciatura  
					into   :ls_cod_verniciatura  
					from   tab_modelli_verniciature  
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_modello = :fs_cod_modello and  
							 num_verniciatura=3 and
							 flag_default = 'S' and
							 flag_blocco  = 'N';
							 
					if sqlca.sqlcode = 0 then			// esiste verniciatura di default
						select cod_veloce,
								 flag_fuori_campionario,
								 flag_addizionale
						into   :ls_cod_veloce,
								 :ls_vern_fuori_campionario,
								 :ls_vern_addizionale
						from   tab_verniciatura
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_verniciatura = :ls_cod_verniciatura;
						if sqlca.sqlcode = 0 then
							lb_test = true
						else
							g_mb.messagebox("Estrazione Verniciatura di Default", "Dettaglio errore: " + sqlca.sqlerrtext)
						end if
					end if
				end if		
				
				if lb_test then
					w_configuratore.istr_riepilogo.cod_verniciatura_3 = ls_cod_verniciatura
					fdw_datawindow.setitem(1, "rs_cod_verniciatura_3", ls_cod_verniciatura)
					fdw_datawindow.setitem(1, "rs_cod_verniciatura_manuale_3", ls_cod_verniciatura)
					fdw_datawindow.setitem(1, "rs_vern_fuori_campionario_3", ls_vern_fuori_campionario)
					fdw_datawindow.setitem(1, "rs_vern_addizionale_3", ls_vern_addizionale)
					if ls_vern_fuori_campionario = "S" then
						fdw_datawindow.object.rs_des_vernice_fc_3.background.color = 16777215
						fdw_datawindow.object.rs_des_vernice_fc_3.protect = 0
					else
						fdw_datawindow.object.rs_des_vernice_fc_3.background.color = 79741120
						fdw_datawindow.object.rs_des_vernice_fc_3.protect = 1
					end if
					fs_cod_prodotto_finito = Replace(fs_cod_prodotto_finito, 6, 1, ls_cod_veloce)
				else
					w_configuratore.istr_riepilogo.cod_verniciatura_3 = ""
					fdw_datawindow.setitem(1, "rs_cod_verniciatura_3", "")
					fdw_datawindow.setitem(1, "rs_cod_verniciatura_manuale_3", "")
					fdw_datawindow.setitem(1, "rs_vern_fuori_campionario_3", "N")
					fdw_datawindow.setitem(1, "rs_vern_addizionale_3", "N")		
					fdw_datawindow.object.rs_des_vernice_fc_3.background.color = 79741120
					fdw_datawindow.object.rs_des_vernice_fc_3.protect = 1
				end if
			end if
		end if


	case "d_ext_comandi"
		string ls_cod_comando_default, ls_com_fuori_campionario, ls_com_addizionale, ls_dimensioni
		dec{4} ld_dimensione_1, ld_dimensione_2
		if w_configuratore.istr_flags.flag_comando = "S" then
			// **** spec_varie_5  24/1/2002  (EnMe); se in un passo precedente era stata specificata una
			// verniciatura, reimposto la DDDW dei comandi filtrando con la verniciatura.
			
			// EnMe 31/1/2007 specifica distinta_base_varianti; visualizzo solo i comandi previsti dale misure della tenda;
			ls_dimensioni = " and tab_limiti_comando.lim_inf_dim_1 <= " + f_double_to_string(w_configuratore.istr_riepilogo.dimensione_1) + " and tab_limiti_comando.lim_sup_dim_1 >= " +  f_double_to_string(w_configuratore.istr_riepilogo.dimensione_1) + &
								 " and tab_limiti_comando.lim_inf_dim_2 <= " + f_double_to_string(w_configuratore.istr_riepilogo.dimensione_2) + " and tab_limiti_comando.lim_sup_dim_2 >= " +  f_double_to_string(w_configuratore.istr_riepilogo.dimensione_2)		 
							 
			
			if not isnull(w_configuratore.istr_riepilogo.cod_verniciatura) then
				f_PO_LoadDDDW_DW(fdw_datawindow,"rs_cod_comando",sqlca,&
				  "anag_prodotti","cod_prodotto","des_prodotto",&
				  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in (SELECT tab_limiti_comando.cod_comando  " + &
					" FROM tab_limiti_comando,   tab_limiti_comando_vern  " + &
					" WHERE ( tab_limiti_comando_vern.cod_azienda = tab_limiti_comando.cod_azienda ) and    " + &
					" ( tab_limiti_comando_vern.cod_comando = tab_limiti_comando.cod_comando ) and    " + &
					" ( tab_limiti_comando_vern.prog_limite_comando = tab_limiti_comando.prog_limite_comando ) and    " + &
					" ( ( tab_limiti_comando.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND    " + &
					" ( tab_limiti_comando_vern.cod_verniciatura = '"+w_configuratore.istr_riepilogo.cod_verniciatura+"' ) AND    " + &
					" ( tab_limiti_comando.cod_modello_tenda = '" + str_conf_prodotto.cod_modello + "' "+ ls_dimensioni +"))) ")
			end if

			ls_vecchio_comando = fdw_datawindow.getitemstring(1, "rs_cod_comando")
			
			if not isnull(ls_vecchio_comando) and len(ls_vecchio_comando) > 0 then
				// siccome potrebbe essere cambiata la verniciatura, verifico se il comando precedente (supposto che ci sia)
				// è ancora valido; se non è valido segnalo la cosa e propongo quello nuovo. [Enrico in accordo Beatrice 25/1/2002]
				ll_cont = 0
				SELECT count(*)
				INTO :ll_cont
				FROM tab_limiti_comando,   tab_limiti_comando_vern 
				WHERE ( tab_limiti_comando_vern.cod_azienda = tab_limiti_comando.cod_azienda ) and 
				( tab_limiti_comando_vern.cod_comando = tab_limiti_comando.cod_comando ) and  
				( tab_limiti_comando_vern.prog_limite_comando = tab_limiti_comando.prog_limite_comando ) and  
				( ( tab_limiti_comando.cod_azienda = :s_cs_xx.cod_azienda) AND 
				  ( tab_limiti_comando_vern.cod_verniciatura = :w_configuratore.istr_riepilogo.cod_verniciatura) AND 
				  ( tab_limiti_comando.flag_secondo_comando = 'N') and
				  ( tab_limiti_comando.cod_modello_tenda = :str_conf_prodotto.cod_modello) and
				  ( tab_limiti_comando.cod_comando = :ls_vecchio_comando));				
				if ll_cont = 0 or isnull(ll_cont) then
					// cerco il comando di default per la nuova verniciatura
					ld_dimensione_1 = fdw_inizio.getitemnumber(1, "rd_dimensione_1")
					ld_dimensione_2 = fdw_inizio.getitemnumber(1, "rd_dimensione_2")				
					
					if ld_dimensione_1 = 0 and ld_dimensione_2 = 0 then
						select tab_limiti_comando.cod_comando,
								 tab_limiti_comando.flag_fuori_campionario,
								 tab_limiti_comando.flag_addizionale
						  into :ls_cod_comando_default,
								 :ls_com_fuori_campionario,
								 :ls_com_addizionale
						  from tab_limiti_comando, tab_limiti_comando_vern
						 where ( tab_limiti_comando_vern.cod_azienda = tab_limiti_comando.cod_azienda ) and  
								 ( tab_limiti_comando_vern.cod_comando = tab_limiti_comando.cod_comando ) and 
								 ( tab_limiti_comando_vern.prog_limite_comando = tab_limiti_comando.prog_limite_comando ) AND
								 (( tab_limiti_comando.cod_azienda = :s_cs_xx.cod_azienda ) and
								  ( tab_limiti_comando.flag_default = 'S') and
								  ( tab_limiti_comando.flag_secondo_comando = 'N') and
								  ( tab_limiti_comando.cod_modello_tenda = :fs_cod_modello) AND
								  ( tab_limiti_comando_vern.cod_verniciatura = :w_configuratore.istr_riepilogo.cod_verniciatura)) ;					
					else
						select tab_limiti_comando.cod_comando,
								 tab_limiti_comando.flag_fuori_campionario,
								 tab_limiti_comando.flag_addizionale
						  into :ls_cod_comando_default,
								 :ls_com_fuori_campionario,
								 :ls_com_addizionale
						  from tab_limiti_comando, tab_limiti_comando_vern
						 where ( tab_limiti_comando_vern.cod_azienda = tab_limiti_comando.cod_azienda ) and  
								 ( tab_limiti_comando_vern.cod_comando = tab_limiti_comando.cod_comando ) and 
								 ( tab_limiti_comando_vern.prog_limite_comando = tab_limiti_comando.prog_limite_comando ) AND
								 (( tab_limiti_comando.cod_azienda = :s_cs_xx.cod_azienda ) and
								  ( tab_limiti_comando.flag_default = 'S') and
								  ( tab_limiti_comando.flag_secondo_comando = 'N') and
								  ( tab_limiti_comando.cod_modello_tenda = :fs_cod_modello) AND
								  ( tab_limiti_comando_vern.cod_verniciatura = :w_configuratore.istr_riepilogo.cod_verniciatura) AND				
								  ( (:ld_dimensione_1 between lim_inf_dim_1 and lim_sup_dim_1) and (:ld_dimensione_2 between lim_inf_dim_2 and lim_sup_dim_2)));
					end if	
					if SQLCA.sqlcode = 0 then
						select cod_veloce
						 into :ls_cod_veloce
						 from tab_comandi
						where cod_azienda = :s_cs_xx.cod_azienda and
								cod_comando = :ls_cod_comando_default;
					end if
					if SQLCA.sqlcode = 0 then
						w_configuratore.istr_riepilogo.cod_comando = ls_cod_comando_default
						fdw_datawindow.setitem(1, "rs_cod_comando", ls_cod_comando_default)
						fdw_datawindow.setitem(1, "rs_cod_comando_manuale", ls_cod_comando_default)
						fdw_datawindow.setitem(1, "rs_com_flag_fuori_campionario", ls_com_fuori_campionario)
						fdw_datawindow.setitem(1, "rs_com_flag_addizionale", ls_com_addizionale)
						fs_cod_prodotto_finito = Replace(fs_cod_prodotto_finito, 7, 2, ls_cod_veloce)
						f_PO_LoadDDDW_DW(fdw_datawindow,"rs_pos_comando",sqlca, &
											  "tab_comandi_posizioni","posizione","des_posizione",&
											  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_comando = '" +ls_cod_comando_default + "'")
						
					elseif SQLCA.sqlcode = 100 then
						w_configuratore.istr_riepilogo.cod_comando = ""
						fdw_datawindow.setitem(1, "rs_cod_comando", ls_null)
						fdw_datawindow.setitem(1, "rs_cod_comando_manuale", ls_null)
						fdw_datawindow.setitem(1, "rs_com_flag_fuori_campionario", "N")
						fdw_datawindow.setitem(1, "rs_com_flag_addizionale", "N")
					else
						g_mb.messagebox("Estrazione Comando di Default", "Errore durante l'Estrazione del comando." +sqlca.sqlerrtext)
					end if
					if isnull(ls_vecchio_comando) or ls_vecchio_comando = "" then ls_vecchio_comando = "<NESSUN COMANDO>"
					if isnull(ls_cod_comando_default) or ls_cod_comando_default = "" then ls_cod_comando_default = "<NESSUN COMANDO>"
					g_mb.messagebox("Configuratore","ATTENZIONE E' STATO VARIATO IL COMANDO ~r~nDAL CODICE " + ls_vecchio_comando + " AL CODICE " + ls_cod_comando_default + "~r~nVERIFICARE CHE SIA CORRETTO !!!! ")
				end if

			else
				
				ld_dimensione_1 = fdw_inizio.getitemnumber(1, "rd_dimensione_1")
				ld_dimensione_2 = fdw_inizio.getitemnumber(1, "rd_dimensione_2")				

				if ld_dimensione_1 = 0 and ld_dimensione_2 = 0 then
					select tab_limiti_comando.cod_comando,
							 tab_limiti_comando.flag_fuori_campionario,
							 tab_limiti_comando.flag_addizionale
					  into :ls_cod_comando_default,
							 :ls_com_fuori_campionario,
							 :ls_com_addizionale
					  from tab_limiti_comando, tab_limiti_comando_vern
					 where ( tab_limiti_comando_vern.cod_azienda = tab_limiti_comando.cod_azienda ) and  
							 ( tab_limiti_comando_vern.cod_comando = tab_limiti_comando.cod_comando ) and 
							 ( tab_limiti_comando_vern.prog_limite_comando = tab_limiti_comando.prog_limite_comando ) AND
							 (( tab_limiti_comando.cod_azienda = :s_cs_xx.cod_azienda ) and
							  ( tab_limiti_comando.flag_default = 'S') and
							  ( tab_limiti_comando.flag_secondo_comando = 'N') and
					   	  ( tab_limiti_comando.cod_modello_tenda = :fs_cod_modello) AND
							  ( tab_limiti_comando_vern.cod_verniciatura = :w_configuratore.istr_riepilogo.cod_verniciatura)) ;					
				else
					select tab_limiti_comando.cod_comando,
							 tab_limiti_comando.flag_fuori_campionario,
							 tab_limiti_comando.flag_addizionale
					  into :ls_cod_comando_default,
							 :ls_com_fuori_campionario,
							 :ls_com_addizionale
					  from tab_limiti_comando, tab_limiti_comando_vern
					 where ( tab_limiti_comando_vern.cod_azienda = tab_limiti_comando.cod_azienda ) and  
							 ( tab_limiti_comando_vern.cod_comando = tab_limiti_comando.cod_comando ) and 
							 ( tab_limiti_comando_vern.prog_limite_comando = tab_limiti_comando.prog_limite_comando ) AND
							 (( tab_limiti_comando.cod_azienda = :s_cs_xx.cod_azienda ) and
							  ( tab_limiti_comando.flag_default = 'S') and
							  ( tab_limiti_comando.flag_secondo_comando = 'N') and
					   	  ( tab_limiti_comando.cod_modello_tenda = :fs_cod_modello) AND
							  ( tab_limiti_comando_vern.cod_verniciatura = :w_configuratore.istr_riepilogo.cod_verniciatura) AND				
							  ( (:ld_dimensione_1 between lim_inf_dim_1 and lim_sup_dim_1) and (:ld_dimensione_2 between lim_inf_dim_2 and lim_sup_dim_2)));
				end if	

				if SQLCA.sqlcode = 0 then
					select cod_veloce
					 into :ls_cod_veloce
					 from tab_comandi
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_comando = :ls_cod_comando_default;
				end if
				

				if SQLCA.sqlcode = 0 then
					w_configuratore.istr_riepilogo.cod_comando = ls_cod_comando_default
					fdw_datawindow.setitem(1, "rs_cod_comando", ls_cod_comando_default)
					fdw_datawindow.setitem(1, "rs_cod_comando_manuale", ls_cod_comando_default)
					fdw_datawindow.setitem(1, "rs_com_flag_fuori_campionario", ls_com_fuori_campionario)
					fdw_datawindow.setitem(1, "rs_com_flag_addizionale", ls_com_addizionale)
					fs_cod_prodotto_finito = Replace(fs_cod_prodotto_finito, 7, 2, ls_cod_veloce)
					f_PO_LoadDDDW_DW(fdw_datawindow,"rs_pos_comando",sqlca, &
										  "tab_comandi_posizioni","posizione","des_posizione",&
										  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_comando = '" +ls_cod_comando_default + "'")
					
				elseif SQLCA.sqlcode = 100 then
					w_configuratore.istr_riepilogo.cod_comando = ""
					fdw_datawindow.setitem(1, "rs_cod_comando", ls_null)
					fdw_datawindow.setitem(1, "rs_cod_comando_manuale", ls_null)
					fdw_datawindow.setitem(1, "rs_com_flag_fuori_campionario", "N")
					fdw_datawindow.setitem(1, "rs_com_flag_addizionale", "N")
				else
					g_mb.messagebox("Estrazione Comando di Default", "Errore durante l'Estrazione del comando." +sqlca.sqlerrtext)
				end if
//			else
			end if
			if not isnull(fdw_datawindow.getitemstring(1, "rs_cod_comando")) then
				ls_cod_comando_default = fdw_datawindow.getitemstring(1, "rs_cod_comando")
				f_PO_LoadDDDW_DW(fdw_datawindow,"rs_pos_comando",sqlca,"tab_comandi_posizioni","posizione","des_posizione",&
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_comando = '" + ls_cod_comando_default + "'")
			end if
		else
			fdw_datawindow.object.rs_cod_comando_t.color = 8421504
			fdw_datawindow.object.rs_cod_comando.background.color = 79741120
			fdw_datawindow.object.rs_cod_comando.protect = 1
			fdw_datawindow.object.rs_cod_comando_manuale_t.color = 8421504
			fdw_datawindow.object.rs_cod_comando_manuale.background.color = 79741120
			fdw_datawindow.object.rs_cod_comando_manuale.protect = 1
			fdw_datawindow.object.rs_com_flag_addizionale_t.color = 8421504
			fdw_datawindow.object.rs_com_flag_addizionale.background.color = 79741120
			fdw_datawindow.object.rs_com_flag_addizionale.protect = 1
			fdw_datawindow.object.rs_com_flag_fuori_campionario_t.color = 8421504
			fdw_datawindow.object.rs_com_flag_fuori_campionario.background.color = 79741120
			fdw_datawindow.object.rs_com_flag_fuori_campionario.protect = 1
			fdw_datawindow.object.nota_comando_1_t.color = 8421504
			fdw_datawindow.object.nota_comando_1.background.color = 79741120
			fdw_datawindow.object.nota_comando_1.protect = 1
		end if
		if w_configuratore.istr_flags.flag_alt_asta = "N" then
			fdw_datawindow.object.rn_alt_asta_t.color = 8421504
			fdw_datawindow.object.rn_alt_asta.background.color = 79741120
			fdw_datawindow.object.rn_alt_asta.protect = 1
		end if
		if w_configuratore.istr_flags.flag_pos_comando = "N" then
			fdw_datawindow.object.rs_pos_comando_t.color = 8421504
			fdw_datawindow.object.rs_pos_comando.background.color = 79741120
			fdw_datawindow.object.rs_pos_comando.protect = 1
		end if
		
		// ----------------------------------  SECONDO COMANDO ------------------------------------------------ //
		
		ls_cod_comando_default=""
		ls_com_fuori_campionario = ""
		ls_com_addizionale = ""
		ls_cod_veloce = ""
		
		if w_configuratore.istr_flags.flag_secondo_comando = "S" then
			
//			if isnull(fdw_datawindow.getitemstring(1, "rs_cod_comando_2")) then
//				ld_dimensione_1 = fdw_inizio.getitemnumber(1, "rd_dimensione_1")
//				ld_dimensione_2 = fdw_inizio.getitemnumber(1, "rd_dimensione_2")				
//
//				Le regole per il secondo comando devono essere guali a quelle del promi comando. [12/07/2013 EnMe]
//				if ld_dimensione_1 = 0 and ld_dimensione_2 = 0 then
//					select cod_comando,
//							 flag_fuori_campionario,
//							 flag_addizionale
//					  into :ls_cod_comando_default,
//							 :ls_com_fuori_campionario,
//							 :ls_com_addizionale
//					  from tab_limiti_comando
//					 where cod_azienda = :s_cs_xx.cod_azienda and
//							 flag_default = 'S' and
//							 flag_secondo_comando = 'S' and
//					   	 cod_modello_tenda = :fs_cod_modello;					
//				else
//					select cod_comando,
//							 flag_fuori_campionario,
//							 flag_addizionale
//					  into :ls_cod_comando_default,
//							 :ls_com_fuori_campionario,
//							 :ls_com_addizionale
//					  from tab_limiti_comando
//					 where cod_azienda = :s_cs_xx.cod_azienda and
//							 cod_modello_tenda = :fs_cod_modello and
//							 flag_default = 'S' and 
//							 flag_secondo_comando = 'S' and
//							 (:ld_dimensione_1 between lim_inf_dim_1 and lim_sup_dim_1 and
//							 :ld_dimensione_2 between lim_inf_dim_2 and lim_sup_dim_2);
//				end if	

//**********************

			ls_vecchio_comando = fdw_datawindow.getitemstring(1, "rs_cod_comando_2")
			
			if not isnull(ls_vecchio_comando) and len(ls_vecchio_comando) > 0 then
				// siccome potrebbe essere cambiata la verniciatura, verifico se il comando precedente (supposto che ci sia)
				// è ancora valido; se non è valido segnalo la cosa e propongo quello nuovo. [Enrico in accordo Beatrice 25/1/2002]
				ll_cont = 0
				
				SELECT 		count(*)
				INTO 			:ll_cont
				FROM 		tab_limiti_comando,   tab_limiti_comando_vern 
				WHERE 		( tab_limiti_comando_vern.cod_azienda = tab_limiti_comando.cod_azienda ) and 
								( tab_limiti_comando_vern.cod_comando = tab_limiti_comando.cod_comando ) and  
								( tab_limiti_comando_vern.prog_limite_comando = tab_limiti_comando.prog_limite_comando ) and  
								( ( tab_limiti_comando.cod_azienda = :s_cs_xx.cod_azienda) AND 
								( tab_limiti_comando_vern.cod_verniciatura = :w_configuratore.istr_riepilogo.cod_verniciatura) AND 
								( tab_limiti_comando.flag_secondo_comando = 'S') and
								( tab_limiti_comando.cod_modello_tenda = :str_conf_prodotto.cod_modello) and
								( tab_limiti_comando.cod_comando = :ls_vecchio_comando));			
								
				if ll_cont = 0 or isnull(ll_cont) then
					// cerco il comando di default per la nuova verniciatura
					ld_dimensione_1 = fdw_inizio.getitemnumber(1, "rd_dimensione_1")
					ld_dimensione_2 = fdw_inizio.getitemnumber(1, "rd_dimensione_2")				
					
					if ld_dimensione_1 = 0 and ld_dimensione_2 = 0 then
						select tab_limiti_comando.cod_comando,
								 tab_limiti_comando.flag_fuori_campionario,
								 tab_limiti_comando.flag_addizionale
						  into :ls_cod_comando_default,
								 :ls_com_fuori_campionario,
								 :ls_com_addizionale
						  from tab_limiti_comando, tab_limiti_comando_vern
						 where ( tab_limiti_comando_vern.cod_azienda = tab_limiti_comando.cod_azienda ) and  
								 ( tab_limiti_comando_vern.cod_comando = tab_limiti_comando.cod_comando ) and 
								 ( tab_limiti_comando_vern.prog_limite_comando = tab_limiti_comando.prog_limite_comando ) AND
								 (( tab_limiti_comando.cod_azienda = :s_cs_xx.cod_azienda ) and
								  ( tab_limiti_comando.flag_default = 'S') and
								  ( tab_limiti_comando.flag_secondo_comando = 'S') and
								  ( tab_limiti_comando.cod_modello_tenda = :fs_cod_modello) AND
								  ( tab_limiti_comando_vern.cod_verniciatura = :w_configuratore.istr_riepilogo.cod_verniciatura)) ;					
					else
						select tab_limiti_comando.cod_comando,
								 tab_limiti_comando.flag_fuori_campionario,
								 tab_limiti_comando.flag_addizionale
						  into :ls_cod_comando_default,
								 :ls_com_fuori_campionario,
								 :ls_com_addizionale
						  from tab_limiti_comando, tab_limiti_comando_vern
						 where ( tab_limiti_comando_vern.cod_azienda = tab_limiti_comando.cod_azienda ) and  
								 ( tab_limiti_comando_vern.cod_comando = tab_limiti_comando.cod_comando ) and 
								 ( tab_limiti_comando_vern.prog_limite_comando = tab_limiti_comando.prog_limite_comando ) AND
								 (( tab_limiti_comando.cod_azienda = :s_cs_xx.cod_azienda ) and
								  ( tab_limiti_comando.flag_default = 'S') and
								  ( tab_limiti_comando.flag_secondo_comando = 'S') and
								  ( tab_limiti_comando.cod_modello_tenda = :fs_cod_modello) AND
								  ( tab_limiti_comando_vern.cod_verniciatura = :w_configuratore.istr_riepilogo.cod_verniciatura) AND				
								  ( (:ld_dimensione_1 between lim_inf_dim_1 and lim_sup_dim_1) and (:ld_dimensione_2 between lim_inf_dim_2 and lim_sup_dim_2)));
					end if	
					
					if sqlca.sqlcode = 0 then
						select cod_veloce
						 into :ls_cod_veloce
						 from tab_comandi
						where cod_azienda = :s_cs_xx.cod_azienda and
								cod_comando = :ls_cod_comando_default;

						w_configuratore.istr_riepilogo.cod_comando_2 = ls_cod_comando_default
						fdw_datawindow.setitem(1, "rs_cod_comando_2_manuale", ls_cod_comando_default)
						fdw_datawindow.setitem(1, "rs_cod_comando_2", ls_cod_comando_default)
						fdw_datawindow.setitem(1, "rs_com_2_flag_fuori_campionario", ls_com_fuori_campionario)
						fdw_datawindow.setitem(1, "rs_com_2_flag_addizionale", ls_com_addizionale)
						
					elseif sqlca.sqlcode = 100 then
						w_configuratore.istr_riepilogo.cod_comando_2 = ""
						fdw_datawindow.setitem(1, "rs_cod_comando_2_manuale", ls_cod_comando_default)
						fdw_datawindow.setitem(1, "rs_cod_comando_2", ls_null)
						fdw_datawindow.setitem(1, "rs_com_2_flag_fuori_campionario", "N")
						fdw_datawindow.setitem(1, "rs_com_2_flag_addizionale", "N")
					else		// errore sql non previsto
						g_mb.messagebox("Estrazione Comando di Default", "Errore durante l'Estrazione del secondo comando." +sqlca.sqlerrtext)
					end if
					if isnull(ls_vecchio_comando) or ls_vecchio_comando = "" then ls_vecchio_comando = "<NESSUN COMANDO>"
					if isnull(ls_cod_comando_default) or ls_cod_comando_default = "" then ls_cod_comando_default = "<NESSUN COMANDO>"
					g_mb.messagebox("Configuratore","ATTENZIONE E' STATO VARIATO IL SECONDO COMANDO ~r~nDAL CODICE " + ls_vecchio_comando + " AL CODICE " + ls_cod_comando_default + "~r~nVERIFICARE CHE SIA CORRETTO !!!! ")
				end if

			else
				
				ld_dimensione_1 = fdw_inizio.getitemnumber(1, "rd_dimensione_1")
				ld_dimensione_2 = fdw_inizio.getitemnumber(1, "rd_dimensione_2")				

				if ld_dimensione_1 = 0 and ld_dimensione_2 = 0 then
					select tab_limiti_comando.cod_comando,
							 tab_limiti_comando.flag_fuori_campionario,
							 tab_limiti_comando.flag_addizionale
					  into :ls_cod_comando_default,
							 :ls_com_fuori_campionario,
							 :ls_com_addizionale
					  from tab_limiti_comando, tab_limiti_comando_vern
					 where ( tab_limiti_comando_vern.cod_azienda = tab_limiti_comando.cod_azienda ) and  
							 ( tab_limiti_comando_vern.cod_comando = tab_limiti_comando.cod_comando ) and 
							 ( tab_limiti_comando_vern.prog_limite_comando = tab_limiti_comando.prog_limite_comando ) AND
							 (( tab_limiti_comando.cod_azienda = :s_cs_xx.cod_azienda ) and
							  ( tab_limiti_comando.flag_default = 'S') and
							  ( tab_limiti_comando.flag_secondo_comando = 'S') and
					   	  ( tab_limiti_comando.cod_modello_tenda = :fs_cod_modello) AND
							  ( tab_limiti_comando_vern.cod_verniciatura = :w_configuratore.istr_riepilogo.cod_verniciatura)) ;					
				else
					select tab_limiti_comando.cod_comando,
							 tab_limiti_comando.flag_fuori_campionario,
							 tab_limiti_comando.flag_addizionale
					  into :ls_cod_comando_default,
							 :ls_com_fuori_campionario,
							 :ls_com_addizionale
					  from tab_limiti_comando, tab_limiti_comando_vern
					 where ( tab_limiti_comando_vern.cod_azienda = tab_limiti_comando.cod_azienda ) and  
							 ( tab_limiti_comando_vern.cod_comando = tab_limiti_comando.cod_comando ) and 
							 ( tab_limiti_comando_vern.prog_limite_comando = tab_limiti_comando.prog_limite_comando ) AND
							 (( tab_limiti_comando.cod_azienda = :s_cs_xx.cod_azienda ) and
							  ( tab_limiti_comando.flag_default = 'S') and
							  ( tab_limiti_comando.flag_secondo_comando = 'S') and
					   	  ( tab_limiti_comando.cod_modello_tenda = :fs_cod_modello) AND
							  ( tab_limiti_comando_vern.cod_verniciatura = :w_configuratore.istr_riepilogo.cod_verniciatura) AND				
							  ( (:ld_dimensione_1 between lim_inf_dim_1 and lim_sup_dim_1) and (:ld_dimensione_2 between lim_inf_dim_2 and lim_sup_dim_2)));
				end if	

				if SQLCA.sqlcode = 0 then
					select cod_veloce
					 into :ls_cod_veloce
					 from tab_comandi
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_comando = :ls_cod_comando_default;

					w_configuratore.istr_riepilogo.cod_comando_2 = ls_cod_comando_default
					fdw_datawindow.setitem(1, "rs_cod_comando_2_manuale", ls_cod_comando_default)
					fdw_datawindow.setitem(1, "rs_cod_comando_2", ls_cod_comando_default)
					fdw_datawindow.setitem(1, "rs_com_2_flag_fuori_campionario", ls_com_fuori_campionario)
					fdw_datawindow.setitem(1, "rs_com_2_flag_addizionale", ls_com_addizionale)
					
				elseif SQLCA.sqlcode = 100 then
					w_configuratore.istr_riepilogo.cod_comando = ""
					fdw_datawindow.setitem(1, "rs_cod_comando_2_manuale", ls_cod_comando_default)
					fdw_datawindow.setitem(1, "rs_cod_comando_2", ls_null)
					fdw_datawindow.setitem(1, "rs_com_2_flag_fuori_campionario", "N")
					fdw_datawindow.setitem(1, "rs_com_2_flag_addizionale", "N")
				else  // errore SQL non previsto
					g_mb.messagebox("Estrazione Comando di Default", "Errore durante l'Estrazione del comando." +sqlca.sqlerrtext)
				end if
				
			end if
//**********************
		else
			fdw_datawindow.object.rs_cod_comando_2_t.color = 8421504
			fdw_datawindow.object.rs_cod_comando_2.background.color = 79741120
			fdw_datawindow.object.rs_cod_comando_2.protect = 1
			fdw_datawindow.object.rs_cod_comando_2_manuale_t.color = 8421504
			fdw_datawindow.object.rs_cod_comando_2_manuale.background.color = 79741120
			fdw_datawindow.object.rs_cod_comando_2_manuale.protect = 1
			fdw_datawindow.object.rs_com_2_flag_addizionale_t.color = 8421504
			fdw_datawindow.object.rs_com_2_flag_addizionale.background.color = 79741120
			fdw_datawindow.object.rs_com_2_flag_addizionale.protect = 1
			fdw_datawindow.object.rs_com_2_flag_fuori_campionario_t.color = 8421504
			fdw_datawindow.object.rs_com_2_flag_fuori_campionario.background.color = 79741120
			fdw_datawindow.object.rs_com_2_flag_fuori_campionario.protect = 1
			fdw_datawindow.object.nota_comando_2_t.color = 8421504
			fdw_datawindow.object.nota_comando_2.background.color = 79741120
			fdw_datawindow.object.nota_comando_2.protect = 1
		end if

	case "d_ext_supporto_flags"
		string ls_cod_supporto_default, ls_sup_fuori_campionario, ls_sup_addizionale, ls_sup_verniciatura
		
		ls_sup_verniciatura = w_configuratore.istr_riepilogo.cod_verniciatura
		lb_test = false
		if w_configuratore.istr_flags.flag_supporto = "S"  then
			if isnull(fdw_datawindow.getitemstring(1, "rs_cod_supporto")) and not isnull(ls_sup_verniciatura) then
				select count(*)
				into   :ll_cont
				from   tab_modelli_tipi_supp_vernic
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_modello = :fs_cod_modello and
						 cod_verniciatura = :ls_sup_verniciatura and
						 flag_default = 'S';
				if ll_cont <> 1 then
					//g_mb.messagebox("Ricerca tipo supporto","Attenzione! Per questo modello/verniciatura non esistono tipi supporti associate oppure è stata specificata più di un supporto di default", Information!)
					lb_test = false
				else
					f_PO_LoadDDDW_DW(fdw_datawindow,"rs_cod_supporto",sqlca,&
										  "anag_prodotti","cod_prodotto","des_prodotto",&
										  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_tipo_supporto from tab_modelli_tipi_supp_vernic where cod_azienda = '" + &
										  s_cs_xx.cod_azienda + "' and cod_modello = '" + fs_cod_modello + "' and cod_verniciatura = '" + ls_sup_verniciatura + "')")
					
					select cod_tipo_supporto,
							 flag_fuori_campionario,
							 flag_addizionale
					into   :ls_cod_supporto_default,
							 :ls_sup_fuori_campionario,
							 :ls_sup_addizionale
					from   tab_modelli_tipi_supp_vernic
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_modello = :fs_cod_modello and
							 cod_verniciatura = :ls_sup_verniciatura and
							 flag_default = 'S';
					if sqlca.sqlcode = 0 then
						lb_test = true
					else
						g_mb.messagebox("Estrazione tipo supporto di Default", "Dettaglio errore: " + sqlca.sqlerrtext)
					end if
				end if
				if lb_test then
					w_configuratore.istr_riepilogo.cod_tipo_supporto = ls_cod_supporto_default
					fdw_datawindow.setitem(1, "rs_cod_supporto", ls_cod_supporto_default)
					fdw_datawindow.setitem(1, "rs_cod_supporto_manuale", ls_cod_supporto_default)
					fdw_datawindow.setitem(1, "rs_flag_fuori_campionario", ls_sup_fuori_campionario)
					fdw_datawindow.setitem(1, "rs_flag_addizionale", ls_sup_addizionale)
				else
					if isnull(ls_sup_verniciatura) then
						g_mb.messagebox("Proposta supporto","Non essendo stata impostata nessuna verniciatura di default non è possibile proporre un supporto", Information!)
					end if
					w_configuratore.istr_riepilogo.cod_tipo_supporto = ""
					fdw_datawindow.setitem(1, "rs_cod_supporto", "")
					fdw_datawindow.setitem(1, "rs_cod_supporto_manuale", "")
					fdw_datawindow.setitem(1, "rs_flag_fuori_campionario", "N")
					fdw_datawindow.setitem(1, "rs_flag_addizionale", "N")
				end if
			end if
		else
			fdw_datawindow.object.rs_cod_supporto_manuale_t.color = 8421504
			fdw_datawindow.object.rs_cod_supporto_t.color = 8421504
			fdw_datawindow.object.rs_flag_addizionale_t.color = 8421504
			fdw_datawindow.object.rs_flag_fuori_campionario_t.color = 8421504
			fdw_datawindow.object.rs_cod_supporto_manuale.background.color = 79741120
			fdw_datawindow.object.rs_cod_supporto_manuale.protect = 1
			fdw_datawindow.object.rs_cod_supporto.background.color = 79741120
			fdw_datawindow.object.rs_cod_supporto.protect = 1
			fdw_datawindow.object.rs_flag_addizionale.background.color = 79741120
			fdw_datawindow.object.rs_flag_addizionale.protect = 1
			fdw_datawindow.object.rs_flag_fuori_campionario.background.color = 79741120
			fdw_datawindow.object.rs_flag_fuori_campionario.protect = 1
		end if
		if w_configuratore.istr_flags.flag_rollo = "N" then
			fdw_datawindow.object.rs_flag_rollo_t.color = 8421504
			fdw_datawindow.object.rs_flag_rollo.background.color = 79741120
			fdw_datawindow.object.rs_flag_rollo.protect = 1
		end if
		if w_configuratore.istr_flags.flag_kit_laterale = "N" then
			fdw_datawindow.object.rs_flag_kit_laterale_t.color = 8421504
			fdw_datawindow.object.rs_flag_kit_laterale.background.color = 79741120
			fdw_datawindow.object.rs_flag_kit_laterale.protect = 1
		end if
		if w_configuratore.istr_flags.flag_cappotta_frontale = "N" then
			fdw_datawindow.object.rs_flag_cappotta_frontale_t.color = 8421504
			fdw_datawindow.object.rs_flag_cappotta_frontale.background.color = 79741120
			fdw_datawindow.object.rs_flag_cappotta_frontale.protect = 1
		end if
		if w_configuratore.istr_flags.flag_num_gambe_campate = "N" then
			fdw_datawindow.object.rn_num_gambe_t.color = 8421504
			fdw_datawindow.object.rn_num_gambe.background.color = 79741120
			fdw_datawindow.object.rn_num_gambe.protect = 1
			fdw_datawindow.object.rn_num_campate_t.color = 8421504
			fdw_datawindow.object.rn_num_campate.background.color = 79741120
			fdw_datawindow.object.rn_num_campate.protect = 1
		end if
	case "d_ext_plisse"
		if w_configuratore.istr_flags.flag_plisse = "N" then
			fdw_datawindow.object.rs_flag_autoblocco_t.color = 8421504
			fdw_datawindow.object.rs_flag_autoblocco.background.color = 79741120
			fdw_datawindow.object.rs_flag_autoblocco.protect = 1
			fdw_datawindow.object.rn_num_fili_plisse_t.color = 8421504
			fdw_datawindow.object.rn_num_fili_plisse.background.color = 79741120
			fdw_datawindow.object.rn_num_fili_plisse.protect = 1
			fdw_datawindow.object.rn_num_boccole_plisse_t.color = 8421504
			fdw_datawindow.object.rn_num_boccole_plisse.background.color = 79741120
			fdw_datawindow.object.rn_num_boccole_plisse.protect = 1
			fdw_datawindow.object.rn_intervallo_punzoni_plisse_t.color = 8421504
			fdw_datawindow.object.rn_intervallo_punzoni_plisse.background.color = 79741120
			fdw_datawindow.object.rn_intervallo_punzoni_plisse.protect = 1
			fdw_datawindow.object.rn_num_pieghe_plisse_t.color = 8421504
			fdw_datawindow.object.rn_num_pieghe_plisse.background.color = 79741120
			fdw_datawindow.object.rn_num_pieghe_plisse.protect = 1
			fdw_datawindow.object.rn_prof_inf_plisse_t.color = 8421504
			fdw_datawindow.object.rn_prof_inf_plisse.background.color = 79741120
			fdw_datawindow.object.rn_prof_inf_plisse.protect = 1
			fdw_datawindow.object.rn_prof_sup_plisse_t.color = 8421504
			fdw_datawindow.object.rn_prof_sup_plisse.background.color = 79741120
			fdw_datawindow.object.rn_prof_sup_plisse.protect = 1
		else
			g_mb.messagebox("autoblocco",w_configuratore.istr_riepilogo.flag_autoblocco)
			g_mb.messagebox("num_fili",string(w_configuratore.istr_riepilogo.num_fili_plisse))
			g_mb.messagebox("num_boccole",string(w_configuratore.istr_riepilogo.num_boccole_plisse))
			g_mb.messagebox("intervallo",string(w_configuratore.istr_riepilogo.intervallo_punzoni_plisse))
			g_mb.messagebox("num_pieghe",string(w_configuratore.istr_riepilogo.num_pieghe_plisse))
			g_mb.messagebox("prof_inf_plisse",string(w_configuratore.istr_riepilogo.prof_inf_plisse))
			g_mb.messagebox("prof_sup_plisse",string(w_configuratore.istr_riepilogo.prof_sup_plisse))
			if w_configuratore.istr_riepilogo.flag_autoblocco <> "" and not isnull(w_configuratore.istr_riepilogo.flag_autoblocco) then
				fdw_datawindow.setitem(1, "rs_flag_autoblocco", w_configuratore.istr_riepilogo.flag_autoblocco)
			end if
			if w_configuratore.istr_riepilogo.num_fili_plisse <> 0 and not isnull(w_configuratore.istr_riepilogo.num_fili_plisse) then
				fdw_datawindow.setitem(1, "rn_num_fili_plisse", w_configuratore.istr_riepilogo.num_fili_plisse)
			end if
			if w_configuratore.istr_riepilogo.num_boccole_plisse <> 0 and not isnull(w_configuratore.istr_riepilogo.num_boccole_plisse) then
				fdw_datawindow.setitem(1, "rn_num_boccole_plisse", w_configuratore.istr_riepilogo.num_boccole_plisse)
			end if
			if w_configuratore.istr_riepilogo.intervallo_punzoni_plisse <> 0 and not isnull(w_configuratore.istr_riepilogo.intervallo_punzoni_plisse) then
				fdw_datawindow.setitem(1, "rn_intervallo_punzoni_plisse", w_configuratore.istr_riepilogo.intervallo_punzoni_plisse)
			end if
			if w_configuratore.istr_riepilogo.num_pieghe_plisse <> 0 and not isnull(w_configuratore.istr_riepilogo.num_pieghe_plisse) then
				fdw_datawindow.setitem(1, "rn_num_pieghe_plisse", w_configuratore.istr_riepilogo.num_pieghe_plisse)
			end if
			if w_configuratore.istr_riepilogo.prof_inf_plisse <> 0 and not isnull(w_configuratore.istr_riepilogo.prof_inf_plisse) then
				fdw_datawindow.setitem(1, "rn_prof_inf_plisse", w_configuratore.istr_riepilogo.prof_inf_plisse)
			end if
			if w_configuratore.istr_riepilogo.prof_sup_plisse <> 0 and not isnull(w_configuratore.istr_riepilogo.prof_sup_plisse) then
				fdw_datawindow.setitem(1, "rn_prof_sup_plisse", w_configuratore.istr_riepilogo.prof_sup_plisse)
			end if
		end if
	case "d_ext_fine"
	case "d_ext_optional"
	case "d_ext_nota_prodotto_finito"
		if not isnull(w_configuratore.istr_riepilogo.nota_dettaglio)  and len(w_configuratore.istr_riepilogo.nota_dettaglio) > 0 then
			fdw_datawindow.setitem(1, "nota_dettaglio", w_configuratore.istr_riepilogo.nota_dettaglio)
		end if
		if not isnull(w_configuratore.istr_riepilogo.nota_prodotto)  and len(w_configuratore.istr_riepilogo.nota_prodotto) > 0 then
			fdw_datawindow.setitem(1, "nota_prodotto_riga", w_configuratore.istr_riepilogo.nota_prodotto)
		end if
		fdw_datawindow.setitem(1, "flag_trasporta_nota_ddt", w_configuratore.istr_riepilogo.flag_trasporta_nota_ddt)
		
end choose

end subroutine

public function integer uof_init_codice_prodotto (string as_cod_modello_prodotto, decimal ad_dimensione_1, string as_colore_tessuto, string as_cod_tessuto, string as_cod_comando, string as_cod_verniciatura, ref string as_cod_prodotto_composto, ref string as_errore);// --------------------------------------------------------------------------------------------------------------------------------
//	Funzione che partendo dal modello passato come parametro mi inizializza le variabili di istanza
//		quali la classe merceologica, la linea del prodotto, il codice prodotto finito
//		restituisce il codice prodotto finito
//
// nome: wf_init_cod_prodotto
// tipo: string
//  
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							
//									as_cod_modello_prodotto	    String			valore
//
// ----------------------------------------------------------------------------------------------------------------------------------

boolean lb_cac=false
string ls_cod_veloce, ls_linea_prodotto, ls_classe_merceologica

if isnull( as_cod_modello_prodotto ) then return 0

guo_functions. uof_get_parametro_azienda( "GAC", lb_cac)

if lb_cac then

	ls_linea_prodotto = left(as_cod_modello_prodotto, 1)
	ls_classe_merceologica = mid(as_cod_modello_prodotto, 2, 1)
	
	
	as_cod_prodotto_composto = as_cod_modello_prodotto + "     "
	
	// --------------------------------------  inizio ---------------------------------------------
	
	if ls_classe_merceologica <> "0" and ls_classe_merceologica <> "S" then
		if ad_dimensione_1 > 999 then
			as_cod_prodotto_composto = replace(as_cod_prodotto_composto, 7, 3, "---")
		else
			as_cod_prodotto_composto = replace(as_cod_prodotto_composto, 7, 3, fill("0", 3 - len(trim(string(integer(ad_dimensione_1))))) + trim(string(integer(ad_dimensione_1))))
		end if
	end if
	
	// ----------------------------------------  tessuto ---------------------------------------------
	if not isnull(as_colore_tessuto) and len(as_colore_tessuto) > 0 and not isnull(as_cod_tessuto) and len(as_cod_tessuto) > 0  then
		choose case ls_linea_prodotto
			case "A", "B","C","D","E","F","G","H","I","L","M","N","O","P","Q","R","S","T","U","V","Z"
				select cod_veloce
				 into :ls_cod_veloce
				 from tab_tessuti
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_non_a_magazzino = :as_colore_tessuto and
						cod_tessuto = :as_cod_tessuto;
				if SQLCA.sqlcode = 100 then
					as_cod_prodotto_composto = Replace(as_cod_prodotto_composto, 5, 1, "-")
					return 0
				elseif SQLCA.sqlcode <> 0 then
					as_errore = "Errore durante la ricerca proprietà tessuto in tabella tessuti. Dettaglio errore " + sqlca.sqlerrtext
					return -1
				end if
				as_cod_prodotto_composto = Replace(as_cod_prodotto_composto, 5, 1, ls_cod_veloce)
		end choose
	end if
	//
	
	// ------------------------------------------ comandi --------------------------------------------------------
	if not isnull(as_cod_comando) and len(as_cod_comando) > 0 then
		choose case ls_linea_prodotto
			case "A", "B","C","D","E","F","G","H","I","L","M","N","O","P","Q","R","S","T","U","V","Z"
				select cod_veloce
				 into :ls_cod_veloce
				 from tab_comandi
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_comando = :as_cod_comando;
				if sqlca.sqlcode = 100 then
					as_cod_prodotto_composto = Replace(as_cod_prodotto_composto, 7, 2, "-")
					return 0
				elseif sqlca.sqlcode = -1 then
					as_errore = "Errore durante l'Estrazione del codice veloce dal tipo comando"
					return -1
				end if
				if isnull(ls_cod_veloce) or len(ls_cod_veloce) < 1 then
					as_errore = "Al comando " + as_cod_comando + " non è stato associato alcun codice veloce: verificare"
					return -1
				end if
				as_cod_prodotto_composto = Replace(as_cod_prodotto_composto, 7, 2, ls_cod_veloce)
		end choose
	end if
	// ----------------------------------- lastre / verniciature ---------------------------------------------
	
	if not isnull(as_cod_verniciatura) and len(as_cod_verniciatura) > 0 then
		select cod_veloce
		into  :ls_cod_veloce
		from  tab_verniciatura
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_verniciatura = :as_cod_verniciatura;
		if sqlca.sqlcode = 100 then
			as_errore = "Autocomposizione codice: codice verniciatura " + as_cod_verniciatura + " non trovato in tabella verniciature"
			return -1
		elseif SQLCA.sqlcode <> 0 then
			as_errore = "Errore durante ricerca codice verniciatura in tabella verniciature.~r~nDettaglio errore: " + sqlca.sqlerrtext
			return -1
		end if
		if isnull(ls_cod_veloce) or len(ls_cod_veloce) < 1 then
			as_errore = "Alla verniciatura " + as_cod_verniciatura + " non è stato associato alcun codice veloce: verificare"
			return -1
		end if
		as_cod_prodotto_composto = Replace(as_cod_prodotto_composto, 6, 1, ls_cod_veloce)
	end if
	
	// -----------------------------------  visualizzo il codicie finito ----------------------------------------
	//st_cod_prodotto_finito.text = " Codice Prodotto :  " + as_cod_prodotto_composto
else
	as_cod_prodotto_composto = as_cod_modello_prodotto
end if

return 0



end function

on uo_set_default.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_set_default.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

