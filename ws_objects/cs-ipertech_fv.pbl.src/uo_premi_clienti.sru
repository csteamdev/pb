﻿$PBExportHeader$uo_premi_clienti.sru
forward
global type uo_premi_clienti from nonvisualobject
end type
end forward

global type uo_premi_clienti from nonvisualobject
end type
global uo_premi_clienti uo_premi_clienti

type variables
string is_clienti[], is_prodotti_premio_pezzo[]
end variables

forward prototypes
public function integer uof_calcolo_premio_riga (decimal fd_imponibile, decimal fd_provvigione, string fs_flag_premio_pezzi, decimal fd_pezzi, ref string fs_errore, ref decimal fd_punti)
public function integer uof_calcolo_premio_fattura (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_errore)
public function integer uof_stampa_premio_fattura (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio)
end prototypes

public function integer uof_calcolo_premio_riga (decimal fd_imponibile, decimal fd_provvigione, string fs_flag_premio_pezzi, decimal fd_pezzi, ref string fs_errore, ref decimal fd_punti);long ll_rows, ll_i
dec{4} ld_premio, ld_premio_aggiuntivo
datastore lds_punti

fd_punti = 0
if fd_provvigione = 0 then return 0

// trovo scaglione premio in base alla provvigione
lds_punti = CREATE datastore
lds_punti.dataobject = 'd_ds_punti'
lds_punti.settransobject(sqlca)
ll_rows = lds_punti.retrieve()

for ll_i = 1 to ll_rows
	if lds_punti.getitemnumber(ll_i,"scaglione") >= fd_provvigione then
		ld_premio = lds_punti.getitemnumber(ll_i,"premio")
		exit
	end if
next
destroy lds_punti

fd_punti = (fd_imponibile / 1000) * ld_premio

fd_punti = round(fd_punti, 2)

if fs_flag_premio_pezzi = "S" then
	// trovo scaglione premio in base alla provvigione
	lds_punti = CREATE datastore
	lds_punti.dataobject = 'd_ds_punti_prezzi'
	lds_punti.settransobject(sqlca)
	ll_rows = lds_punti.retrieve()
	
	for ll_i = 1 to ll_rows
		if lds_punti.getitemnumber(ll_i,"scaglione") >= fd_provvigione then
			ld_premio_aggiuntivo = lds_punti.getitemnumber(ll_i,"premio_pezzo")
			exit
		end if
	next
	destroy lds_punti
	
	ld_premio_aggiuntivo = ld_premio_aggiuntivo * fd_pezzi
	
	ld_premio_aggiuntivo = round(ld_premio_aggiuntivo, 2)
	
	fd_punti = fd_punti + ld_premio_aggiuntivo
	
end if	

return 0
end function

public function integer uof_calcolo_premio_fattura (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_errore);string ls_cod_cliente, ls_cod_agente_1, ls_flag_premio_pezzo,ls_cod_tipo_fat_ven,ls_flag_tipo_fat_ven
long   ll_i,ll_rows, ll_y, ll_ret, ll_prog_riga_fat_ven
dec{4} ld_provvigione_1, ld_imponibile_iva, ld_punti,ld_quan_fatturata, ld_moltiplicatore
datastore lds_det_fat_ven


select cod_cliente, cod_agente_1, cod_tipo_fat_ven
into   :ls_cod_cliente, :ls_cod_agente_1, :ls_cod_tipo_fat_ven
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
       num_registrazione = :fl_num_registrazione;
if sqlca.sqlcode = -1 then
	fs_errore = "Errore in ricerca clietne su tes_fat_ven~r~n"+sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_agente_1) then return 0

if sqlca.sqlcode = 100 then
	fs_errore = "cliente inesistente " + ls_cod_cliente
	return -1
end if

select flag_tipo_fat_ven
into   :ls_flag_tipo_fat_ven
from   tab_tipi_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca tipo fattura~r~n"+sqlca.sqlerrtext
	return -1
end if

if ls_flag_tipo_fat_ven = "N" then
	ld_moltiplicatore = -1
else
	ld_moltiplicatore = 1
end if
		 
// pulisco i premi in fattura
update det_fat_ven
set    valore_premio = 0
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;
if sqlca.sqlcode = -1 then
	fs_errore = "Errore in memorizzazione premio in riga fattura~r~n" + sqlca.sqlerrtext
	rollback;
	destroy lds_det_fat_ven
	return -1
end if

commit;

// controllo se cliente escluso
for ll_i = 1 to upperbound(is_clienti[])
	
	if ls_cod_cliente = is_clienti[ll_i] then
		exit		// trovato! è un cliente incluso nel giro
	end if
	
	if ll_i = upperbound(is_clienti[]) then
		return 0		// non trovato! è un cliente escluso dal calcolo
	end if
	
next

lds_det_fat_ven = CREATE datastore
lds_det_fat_ven.dataobject = 'd_det_fat_ven_lista'
lds_det_fat_ven.settransobject(SQLCA)

ll_rows = lds_det_fat_ven.retrieve(s_cs_xx.cod_azienda, fl_anno_registrazione, fl_num_registrazione)

for ll_i = 1 to ll_rows
	ls_flag_premio_pezzo = "N"
	if not isnull(lds_det_fat_ven.getitemstring(ll_i,"cod_prodotto")) then
		// controllo premio pezzi
		//ls_flag_premio_pezzo = "N"
		for ll_y = 1 to upperbound(is_prodotti_premio_pezzo[])
			if is_prodotti_premio_pezzo[ll_y] = lds_det_fat_ven.getitemstring(ll_i,"cod_prodotto") then
				ls_flag_premio_pezzo ="S"
				exit
			end if
		next
	end if
	
	ll_prog_riga_fat_ven = lds_det_fat_ven.getitemnumber(ll_i,"prog_riga_fat_ven")
	ld_imponibile_iva = lds_det_fat_ven.getitemnumber(ll_i,"imponibile_iva")
	ld_provvigione_1 = lds_det_fat_ven.getitemnumber(ll_i,"provvigione_1")
	ld_quan_fatturata = lds_det_fat_ven.getitemnumber(ll_i,"quan_fatturata")
	
	ll_ret = uof_calcolo_premio_riga(ld_imponibile_iva, ld_provvigione_1, ls_flag_premio_pezzo, ld_quan_fatturata, ref fs_errore, ref ld_punti)
	
	if ll_ret = 0 then
		
		ld_punti = ld_punti * ld_moltiplicatore
		
		update det_fat_ven
		set    valore_premio = :ld_punti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :fl_anno_registrazione and
				 num_registrazione = :fl_num_registrazione and
				 prog_riga_fat_ven = :ll_prog_riga_fat_ven;
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in memorizzazione premio in riga fattura~r~n" + sqlca.sqlerrtext
			rollback;
			destroy lds_det_fat_ven
			return -1
		end if
		
		commit;
	else
		destroy lds_det_fat_ven
		return -1
	end if
next

commit;
destroy lds_det_fat_ven
return 0
end function

public function integer uof_stampa_premio_fattura (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio);string ls_flag_stampa,ls_cod_cliente, ls_des_mese,ls_cod_documento, ls_numeratore_documento
long   ll_mese,ll_anno_documento,ll_num_documento
dec{4} ld_premio_fattura, ld_premio_totale
datetime ldt_data_inizio, ldt_data_fattura

// Parametro Stampa Calcolo Premio in fattura
select flag
into   :ls_flag_stampa
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'SCP';
if sqlca.sqlcode <> 0 then
	fs_messaggio = ""
	return 0
end if

if ls_flag_stampa = "N" or isnull(ls_flag_stampa) or len(ls_flag_stampa) < 1 then
		fs_messaggio = ""
	return 0
end if

// Parametro Data Inizio Calcolo premi clienti
select data
into   :ldt_data_inizio
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'DIC';
if sqlca.sqlcode <> 0 then
	fs_messaggio = ""
	return 0
end if

select cod_cliente, data_fattura, cod_documento, numeratore_documento, anno_documento, num_documento
into   :ls_cod_cliente, :ldt_data_fattura,:ls_cod_documento, :ls_numeratore_documento, :ll_anno_documento, :ll_num_documento
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca data fattura.~r~n" + sqlca.sqlerrtext
	return -1
end if

if ldt_data_fattura < ldt_data_inizio then
	fs_messaggio = ""
	return 0
end if

ll_mese = month(date(ldt_data_fattura))

choose case ll_mese
	case 1
		ls_des_mese = "Gennaio"
	case 2
		ls_des_mese = "Febbraio"
	case 3
		ls_des_mese = "Marzo"
	case 4
		ls_des_mese = "Aprile"
	case 5
		ls_des_mese = "Maggio"
	case 6
		ls_des_mese = "Giugno"
	case 7
		ls_des_mese = "Luglio"
	case 8
		ls_des_mese = "Agosto"
	case 9
		ls_des_mese = "Settembre"
	case 10
		ls_des_mese = "Ottobre"
	case 11
		ls_des_mese = "Novembre"
	case 12
		ls_des_mese = "Dicembre"
end choose

select sum(valore_premio)
into   :ld_premio_fattura
from   det_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda  and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in calcolo premio fattura.~r~n" + sqlca.sqlerrtext
	return -1
end if


select sum(valore_premio)
into   :ld_premio_totale
from   det_fat_ven, tes_fat_ven
where  (det_fat_ven.cod_azienda = tes_fat_ven.cod_azienda and
       det_fat_ven.anno_registrazione = tes_fat_ven.anno_registrazione and
		 det_fat_ven.num_registrazione = tes_fat_ven.num_registrazione ) and
       det_fat_ven.cod_azienda = :s_cs_xx.cod_azienda  and
       tes_fat_ven.cod_cliente = :ls_cod_cliente and
		 tes_fat_ven.cod_documento is not null and
		 tes_fat_ven.data_fattura >= :ldt_data_inizio and
		 tes_fat_ven.data_fattura <= :ldt_data_fattura and
		 tes_fat_ven.cod_documento = :ls_cod_documento and
		 tes_fat_ven.numeratore_documento = :ls_numeratore_documento and
		 tes_fat_ven.anno_documento <= :ll_anno_documento and
		 tes_fat_ven.num_documento  <= :ll_num_documento;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in calcolo premio fattura.~r~n" + sqlca.sqlerrtext
	return -1
end if

if ld_premio_totale = 0 then
	fs_messaggio = ""
else
	fs_messaggio = "TOTALE PUNTI FATTURA " + string(ld_premio_fattura, "###,##0.00") + " ---- TOTALE PUNTI ACCUMULATI DAL " + string(ldt_data_inizio,"dd/mm/yyyy") + "  " + string(ld_premio_totale, "###,##0.00")
end if
//fs_messaggio = "Premio al " + string(ldt_data_fattura,"dd/mm/yyyy") + " = " + string(ld_premio_totale, "###,##0") + " punti; totale punti mese " + ls_des_mese + " = " + string(ld_premio_totale, "###,##0") + " punti"

return 0
end function

on uo_premi_clienti.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_premi_clienti.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

