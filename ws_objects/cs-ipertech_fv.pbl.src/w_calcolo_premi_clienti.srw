﻿$PBExportHeader$w_calcolo_premi_clienti.srw
forward
global type w_calcolo_premi_clienti from w_cs_xx_principale
end type
type st_1 from statictext within w_calcolo_premi_clienti
end type
type pb_2 from picturebutton within w_calcolo_premi_clienti
end type
type pb_1 from picturebutton within w_calcolo_premi_clienti
end type
type dw_selezione from uo_cs_xx_dw within w_calcolo_premi_clienti
end type
type cb_calcola from commandbutton within w_calcolo_premi_clienti
end type
end forward

global type w_calcolo_premi_clienti from w_cs_xx_principale
integer width = 3209
integer height = 864
string title = "Elaborazione Premi Clienti"
st_1 st_1
pb_2 pb_2
pb_1 pb_1
dw_selezione dw_selezione
cb_calcola cb_calcola
end type
global w_calcolo_premi_clienti w_calcolo_premi_clienti

on w_calcolo_premi_clienti.create
int iCurrent
call super::create
this.st_1=create st_1
this.pb_2=create pb_2
this.pb_1=create pb_1
this.dw_selezione=create dw_selezione
this.cb_calcola=create cb_calcola
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.pb_2
this.Control[iCurrent+3]=this.pb_1
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.cb_calcola
end on

on w_calcolo_premi_clienti.destroy
call super::destroy
destroy(this.st_1)
destroy(this.pb_2)
destroy(this.pb_1)
destroy(this.dw_selezione)
destroy(this.cb_calcola)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)



pb_1.picturename = s_cs_xx.risorse + "\cartella_apri.bmp"
pb_1.disabledname = s_cs_xx.risorse + "\cartella_apri.bmp"

pb_2.picturename = s_cs_xx.risorse + "\cartella_apri.bmp"
pb_2.disabledname = s_cs_xx.risorse + "\cartella_apri.bmp"

end event

type st_1 from statictext within w_calcolo_premi_clienti
integer x = 14
integer y = 636
integer width = 2743
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Pronto !"
alignment alignment = center!
boolean focusrectangle = false
end type

type pb_2 from picturebutton within w_calcolo_premi_clienti
integer x = 2862
integer y = 440
integer width = 101
integer height = 88
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string picturename = "C:\CS_90\framework\RISORSE\cartella_apri.bmp"
string disabledname = "C:\CS_90\framework\RISORSE\cartella_apri.bmp"
end type

event clicked;string docname, named
integer value,ll_pos,ll_pos_old, LL_I

value = GetFileOpenName("Selezione", + docname, named, "*.*", "Tutti i tipi (*.*),*.*")
if value < 1 then return

dw_selezione.setitem(dw_selezione.getrow(),"path_file_prodotti",docname)

//
//ll_pos = 0
//ll_pos_old = 1
//do while true
//	ll_pos = pos(docname,"\",ll_pos_old)
//	if ll_pos < 1 then exit
//	ll_pos_old = ll_pos + 1
//loop
//		
//if ll_pos_old > 0 then
//	dw_selezione.setitem(dw_selezione.getrow(),"path_file_prodotti",left(docname, ll_pos_old - 1))
//end if		
end event

type pb_1 from picturebutton within w_calcolo_premi_clienti
integer x = 2862
integer y = 340
integer width = 101
integer height = 88
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean originalsize = true
string picturename = "C:\CS_90\framework\RISORSE\cartella_apri.bmp"
string disabledname = "C:\CS_90\framework\RISORSE\cartella_apri.bmp"
end type

event clicked;string docname, named
integer value,ll_pos,ll_pos_old, LL_I

value = GetFileOpenName("Selezione", + docname, named, "*.*", "Tutti i tipi (*.*),*.*")
if value < 1 then return

dw_selezione.setitem(dw_selezione.getrow(),"path_file_categorie",docname)


//ll_pos = 0
//ll_pos_old = 1
//do while true
//	ll_pos = pos(docname,"\",ll_pos_old)
//	if ll_pos < 1 then exit
//	ll_pos_old = ll_pos + 1
//loop
//		
//if ll_pos_old > 0 then
//	dw_selezione.setitem(dw_selezione.getrow(),"path_file_categorie",left(docname, ll_pos_old - 1))
//end if		
end event

type dw_selezione from uo_cs_xx_dw within w_calcolo_premi_clienti
integer x = 18
integer y = 24
integer width = 3127
integer height = 596
integer taborder = 10
string dataobject = "d_calcolo_premi_cliente_selezione"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
end choose
end event

type cb_calcola from commandbutton within w_calcolo_premi_clienti
integer x = 2784
integer y = 636
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Calcola"
end type

event clicked;string ls_errore, ls_sql,ls_cod_cliente,ls_path_categorie,ls_path_file_prodotti, ls_str
long ll_anno_fattura, ll_num_fattura,ll_ret,ll_anno_registrazione,ll_num_registrazione, ll_file, ll_i, &
     ll_anno_reg[],ll_num_reg[]
datetime ldt_data_inizio, ldt_data_fine
uo_premi_clienti luo_premi_clienti

dw_selezione.accepttext()

luo_premi_clienti = create uo_premi_clienti

declare cu_tes_fat_ven dynamic cursor for sqlsa;

st_1.text = "Elaborazione in Corso..."

//ls_sql = "select anno_registrazione, num_registrazione from tes_fat_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_documento is not null "
ls_sql = "select anno_registrazione, num_registrazione from tes_fat_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' "

ll_anno_registrazione = dw_selezione.getitemnumber(dw_selezione.getrow(),"anno_registrazione")
ll_num_registrazione = dw_selezione.getitemnumber(dw_selezione.getrow(),"num_registrazione")
ls_cod_cliente = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_cliente")
ldt_data_inizio = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_inizio_calcolo")
ldt_data_fine   = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_fine_calcolo")
ls_path_categorie = dw_selezione.getitemstring(dw_selezione.getrow(),"path_file_categorie")
ls_path_file_prodotti = dw_selezione.getitemstring(dw_selezione.getrow(),"path_file_prodotti")

if not isnull(ll_anno_registrazione) and ll_anno_registrazione > 0 then
	ls_sql += " and anno_registrazione = " + string(ll_anno_registrazione)
end if

if not isnull(ll_num_registrazione) and ll_num_registrazione > 0 then
	ls_sql += " and num_registrazione = " + string(ll_num_registrazione)
end if

if not isnull(ls_cod_cliente) and len(ls_cod_cliente) > 0 then
	ls_sql += " and cod_cliente = '" + ls_cod_cliente + "' "
end if

if not isnull(ldt_data_inizio) and ldt_data_inizio > datetime(date("01/01/1900"), 00:00:00) then
	ls_sql += " and data_registrazione >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "'"
//	ls_sql += " and data_fattura >= '" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "'"
end if
	
if not isnull(ldt_data_fine) and ldt_data_fine > datetime(date("01/01/1900"), 00:00:00) then
	ls_sql += " and data_registrazione <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "'"
//	ls_sql += " and data_fattura <= '" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "'"
end if
	
prepare sqlsa from :ls_sql;

open cu_tes_fat_ven;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella OPEN cursor di cu_tes_fat_ven~r~n"+sqlca.sqlerrtext)
	rollback;
	st_1.text = "Pronto"
	return
end if

// carico userobject i clienti esclusi.
if not isnull(ls_path_categorie) and len(ls_path_categorie) > 0 then
	ll_file = fileopen(ls_path_categorie)
	
	ll_i = 0
	
	do while true
		ll_ret = fileread(ll_file, ls_str)
		if ll_ret = -100 then exit
		if ll_ret = -1 then
			g_mb.messagebox("APICE","Errore in lettura file " + ls_path_categorie + "~r~nProcedura interrotta")
			close cu_tes_fat_ven;
			destroy luo_premi_clienti
			rollback;
			st_1.text = "Pronto"
			return
		end if
		ll_i ++
		luo_premi_clienti.is_clienti[ll_i] = ls_str
		
		st_1.text = "Lettura file clienti" + ls_str
		
	loop	
	fileclose(ll_file)
end if

// carico userobject prodotti a premio aggiuntivo.
if not isnull(ls_path_file_prodotti) and len(ls_path_file_prodotti) > 0 then
	ll_file = fileopen(ls_path_file_prodotti)
	
	ll_i = 0
	
	do while true
		ll_ret = fileread(ll_file, ls_str)
		if ll_ret = -100 then exit
		if ll_ret = -1 then
			g_mb.messagebox("APICE","Errore in lettura file " + ls_path_categorie + "~r~nProcedura interrotta")
			close cu_tes_fat_ven;
			destroy luo_premi_clienti
			rollback;
			st_1.text = "Pronto"
			return
		end if
		ll_i ++
		luo_premi_clienti.is_prodotti_premio_pezzo[ll_i] = ls_str
	
		st_1.text = "Lettura file prodotti " +ls_str
		
	loop	
	fileclose(ll_file)
end if

// ciclo di elaborazione premi.
ll_i = 0
do while true
	fetch cu_tes_fat_ven into :ll_anno_fattura, :ll_num_fattura;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then
		rollback;
		exit
	end if
	
	// devo metterlo in un array, altrimenti un commit qualsiasi potrebbe chiuedere il cursore
	
	ll_i ++
	ll_anno_reg[ll_i] = ll_anno_fattura
	ll_num_reg[ll_i]  = ll_num_fattura
loop

close cu_tes_fat_ven;

for ll_i = 1 to upperbound(ll_anno_reg)
	st_1.text = "Elaborazione fattura " +string(ll_anno_reg[ll_i]) + " / " + string(ll_num_reg[ll_i])

	ll_ret = luo_premi_clienti.uof_calcolo_premio_fattura(ll_anno_reg[ll_i], ll_num_reg[ll_i], ref ls_errore)
	if ll_ret <> 0 then
		g_mb.messagebox("APICE",ls_errore)
		st_1.text = "Pronto"
		return 
	end if
next
commit;

st_1.text = "Pronto"

destroy luo_premi_clienti

g_mb.messagebox("APICE","Elaborazione Conclusa")

end event

