﻿$PBExportHeader$w_det_fat_ven_ptenda.srw
forward
global type w_det_fat_ven_ptenda from w_det_fat_ven
end type
type dw_vis_comp_det_ord_ven from datawindow within w_det_fat_ven_ptenda
end type
type cb_1 from commandbutton within w_det_fat_ven_ptenda
end type
end forward

global type w_det_fat_ven_ptenda from w_det_fat_ven
integer height = 2684
dw_vis_comp_det_ord_ven dw_vis_comp_det_ord_ven
cb_1 cb_1
end type
global w_det_fat_ven_ptenda w_det_fat_ven_ptenda

type variables

end variables

forward prototypes
public subroutine wf_vis_comp_det_ord_ven (long fn_anno_registrazione, long fn_num_registrazione, long fn_prog_riga_ord_ven)
end prototypes

public subroutine wf_vis_comp_det_ord_ven (long fn_anno_registrazione, long fn_num_registrazione, long fn_prog_riga_ord_ven);string ls_cod_prodotto_finito, ls_cod_tessuto, ls_colore_tessuto, ls_cod_tessuto_mant, ls_cod_colore_mant,   &
       ls_cod_verniciatura, ls_cod_comando_1, ls_note, ls_des_vernice_fc, ls_des_comando_1, ls_cod_comando_2, &
		 ls_des_comando_2, ls_flag_add_comando_1, ls_flag_add_comando_2, ls_flag_add_tessuto, ls_flag_add_verniciatura  
long   ll_riga		 
double ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_alt_mantovana 

dw_vis_comp_det_ord_ven.reset()
dw_vis_comp_det_ord_ven.setredraw(false)

SELECT cod_prod_finito,   
		dim_x,   
		dim_y,   
		dim_z,   
		dim_t,   
		cod_tessuto,   
		cod_non_a_magazzino,   
		cod_tessuto_mant,   
		cod_mant_non_a_magazzino,   
		alt_mantovana,   
		cod_verniciatura,   
		cod_comando,   
		note,   
		des_vernice_fc,   
		des_comando_1,   
		cod_comando_2,   
		des_comando_2,   
		flag_addizionale_comando_1,   
		flag_addizionale_comando_2,   
		flag_addizionale_tessuto,   
		flag_addizionale_vernic  
 INTO :ls_cod_prodotto_finito,   
		:ld_dim_x,   
		:ld_dim_y,   
		:ld_dim_z,   
		:ld_dim_t,   
		:ls_cod_tessuto,   
		:ls_colore_tessuto,   
		:ls_cod_tessuto_mant,   
		:ls_cod_colore_mant,   
		:ld_alt_mantovana,   
		:ls_cod_verniciatura,   
		:ls_cod_comando_1,   
		:ls_note,   
		:ls_des_vernice_fc,   
		:ls_des_comando_1,   
		:ls_cod_comando_2,   
		:ls_des_comando_2,   
		:ls_flag_add_comando_1,   
		:ls_flag_add_comando_2,   
		:ls_flag_add_tessuto,   
		:ls_flag_add_verniciatura  
 FROM comp_det_ord_ven  
WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( anno_registrazione = :fn_anno_registrazione ) AND  
		( num_registrazione = :fn_num_registrazione ) AND  
		( prog_riga_ord_ven = :fn_prog_riga_ord_ven )   ;
if sqlca.sqlcode = 100 then
	return
end if

ll_riga = dw_vis_comp_det_ord_ven.insertrow(0)

dw_vis_comp_det_ord_ven.setitem(ll_riga,"cod_prod_finito", ls_cod_prodotto_finito)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"dim_x",ld_dim_x)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"dim_y",ld_dim_y)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"dim_z",ld_dim_z)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"dim_t",ld_dim_t)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"cod_tessuto",ls_cod_tessuto)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"cod_non_a_magazzino",ls_colore_tessuto)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"cod_verniciatura",ls_cod_verniciatura)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"des_vernice_fc",ls_des_vernice_fc)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"cod_comando",ls_cod_comando_1)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"cod_comando_2",ls_cod_comando_2)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"flag_addizionale_comando_1",ls_flag_add_comando_1)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"flag_addizionale_comando_2",ls_flag_add_comando_2)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"flag_addizionale_tessuto",ls_flag_add_tessuto)
dw_vis_comp_det_ord_ven.setitem(ll_riga,"flag_addizionale_vernic",ls_flag_add_verniciatura)

dw_vis_comp_det_ord_ven.resetupdate()
dw_vis_comp_det_ord_ven.setredraw(true)

return
end subroutine

on w_det_fat_ven_ptenda.create
int iCurrent
call super::create
this.dw_vis_comp_det_ord_ven=create dw_vis_comp_det_ord_ven
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_vis_comp_det_ord_ven
this.Control[iCurrent+2]=this.cb_1
end on

on w_det_fat_ven_ptenda.destroy
call super::destroy
destroy(this.dw_vis_comp_det_ord_ven)
destroy(this.cb_1)
end on

type cb_prodotti_note_ricerca from w_det_fat_ven`cb_prodotti_note_ricerca within w_det_fat_ven_ptenda
end type

type cb_sconti from w_det_fat_ven`cb_sconti within w_det_fat_ven_ptenda
integer x = 1193
integer y = 1768
end type

type cb_c_industriale from w_det_fat_ven`cb_c_industriale within w_det_fat_ven_ptenda
integer x = 805
integer y = 1768
end type

type cb_des_mov from w_det_fat_ven`cb_des_mov within w_det_fat_ven_ptenda
integer x = 416
integer y = 1768
end type

type uo_1 from w_det_fat_ven`uo_1 within w_det_fat_ven_ptenda
end type

type cb_corrispondenze from w_det_fat_ven`cb_corrispondenze within w_det_fat_ven_ptenda
integer x = 27
integer y = 1768
end type

type cb_stock from w_det_fat_ven`cb_stock within w_det_fat_ven_ptenda
end type

type dw_det_fat_ven_cc from w_det_fat_ven`dw_det_fat_ven_cc within w_det_fat_ven_ptenda
end type

type dw_folder from w_det_fat_ven`dw_folder within w_det_fat_ven_ptenda
end type

type dw_documenti from w_det_fat_ven`dw_documenti within w_det_fat_ven_ptenda
end type

type dw_det_fat_ven_det_1 from w_det_fat_ven`dw_det_fat_ven_det_1 within w_det_fat_ven_ptenda
integer height = 1304
boolean vscrollbar = false
boolean livescroll = false
end type

event dw_det_fat_ven_det_1::rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	dw_det_fat_ven_lista.setrow(getrow())
end if
end event

type dw_det_fat_ven_lista from w_det_fat_ven`dw_det_fat_ven_lista within w_det_fat_ven_ptenda
event ue_blocca_colonne_lista ( )
event ue_blocca_colonne_dett ( )
end type

event dw_det_fat_ven_lista::ue_blocca_colonne_lista();string ls_modify, ls_colonne[], ls_err
long ll_i

ls_colonne[1] = "cod_prodotto"
ls_colonne[2] = "des_prodotto"
ls_colonne[3] = "cod_versione"
ls_colonne[4] = "quan_fatturata"
ls_colonne[5] = "sconto_1"
ls_colonne[6] = "sconto_2"
ls_colonne[7] = "prezzo_vendita"
ls_modify = ""

dw_det_fat_ven_lista.setredraw(false)
for ll_i = 1 to upperbound(ls_colonne)
	ls_modify = ls_colonne[ll_i] +".protect='1'"
	ls_err = dw_det_fat_ven_lista.modify(ls_modify)
	if ls_err <> "" then g_mb.messagebox("APICE",ls_err)
	ls_modify = ls_colonne[ll_i] +".background.color='12632256'"
	ls_err = dw_det_fat_ven_lista.modify(ls_modify)
	if ls_err <> "" then g_mb.messagebox("APICE",ls_err)
next
dw_det_fat_ven_lista.setredraw(true)
//dw_det_fat_ven_lista.object.cod_prodotto.protect= 1
//dw_det_fat_ven_lista.object.cod_prodotto.background.color=12632256

end event

event dw_det_fat_ven_lista::ue_blocca_colonne_dett();string ls_modify, ls_colonne[], ls_err
long ll_i

ls_colonne[1] = "cod_prodotto"
ls_colonne[2] = "des_prodotto"
ls_colonne[3] = "cod_versione"
ls_colonne[4] = "quan_fatturata"
ls_colonne[5] = "sconto_1"
ls_colonne[6] = "sconto_2"
ls_colonne[7] = "prezzo_vendita"
ls_colonne[8] = "cod_misura"
ls_colonne[9] = "fat_conversione_ven"
ls_colonne[10] = "quantita_um"
ls_colonne[11] = "prezzo_um"
ls_colonne[12] = "cod_iva"
ls_colonne[13] = "cod_centro_costo"
ls_colonne[14] = "num_confezioni"
ls_colonne[15] = "num_pezzi_confezione"
ls_colonne[16] = "flag_st_note_det"
ls_colonne[16] = "nota_dettaglio"

ls_modify = ""
dw_det_fat_ven_lista.setredraw(false)
for ll_i = 1 to upperbound(ls_colonne)
	ls_modify = ls_colonne[ll_i] +".protect='1'"
	ls_err = dw_det_fat_ven_det_1.modify(ls_modify)
	if ls_err <> "" then g_mb.messagebox("APICE",ls_err)
	ls_modify = ls_colonne[ll_i] +".background.color='12632256'"
	ls_err= dw_det_fat_ven_det_1.modify(ls_modify)
	if ls_err <> "" then g_mb.messagebox("APICE",ls_err)
next
dw_det_fat_ven_lista.setredraw(true)
dw_det_fat_ven_det_1.modify(ls_modify)

end event

event dw_det_fat_ven_lista::rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	// stefanop: 12/01/2012 solo se ci sono righe
	if this.getrow() > 0 then
		long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven
		
		ll_anno_registrazione = this.getitemnumber(this.getrow(),"anno_reg_ord_ven")
		ll_num_registrazione = this.getitemnumber(this.getrow(),"num_reg_ord_ven")
		ll_prog_riga_ord_ven = this.getitemnumber(this.getrow(),"prog_riga_ord_ven")
		wf_vis_comp_det_ord_ven(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven)
		
		if ib_provvigioni then
			postevent("ue_blocca_colonne_lista")
			postevent("ue_blocca_colonne_det")
		end if
	end if
end if
end event

type dw_vis_comp_det_ord_ven from datawindow within w_det_fat_ven_ptenda
integer x = 23
integer y = 1876
integer width = 3474
integer height = 680
integer taborder = 110
boolean bringtotop = true
string title = "none"
string dataobject = "w_vis_comp_det_ord_ven"
boolean livescroll = true
end type

type cb_1 from commandbutton within w_det_fat_ven_ptenda
integer x = 1582
integer y = 1768
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Provvig."
end type

event clicked;if ib_provvigioni then
	ib_provvigioni = false
	this.text = "Provvig."
	parent.postevent("pc_save")
else
	ib_provvigioni = true
	
	dw_det_fat_ven_lista.triggerevent("pcd_modify")
	
	dw_det_fat_ven_lista.postevent("ue_blocca_colonne_lista")
	dw_det_fat_ven_lista.postevent("ue_blocca_colonne_dett")
	this.text = "Salva Provv."
end if
end event

