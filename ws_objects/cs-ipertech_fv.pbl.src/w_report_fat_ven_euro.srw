﻿$PBExportHeader$w_report_fat_ven_euro.srw
forward
global type w_report_fat_ven_euro from w_cs_xx_principale
end type
type cbx_nomenclatura from checkbox within w_report_fat_ven_euro
end type
type cb_5 from commandbutton within w_report_fat_ven_euro
end type
type dw_report_fat_ven from uo_cs_xx_dw within w_report_fat_ven_euro
end type
type cb_4 from commandbutton within w_report_fat_ven_euro
end type
type cb_singola from commandbutton within w_report_fat_ven_euro
end type
type cb_2 from commandbutton within w_report_fat_ven_euro
end type
type cb_3 from commandbutton within w_report_fat_ven_euro
end type
type cb_1 from commandbutton within w_report_fat_ven_euro
end type
end forward

global type w_report_fat_ven_euro from w_cs_xx_principale
integer width = 3931
integer height = 3560
string title = "Stampa Fattura"
boolean minbox = false
event ue_close ( )
event ue_avviso_spedizione ( )
cbx_nomenclatura cbx_nomenclatura
cb_5 cb_5
dw_report_fat_ven dw_report_fat_ven
cb_4 cb_4
cb_singola cb_singola
cb_2 cb_2
cb_3 cb_3
cb_1 cb_1
end type
global w_report_fat_ven_euro w_report_fat_ven_euro

type variables
boolean ib_modifica=false, ib_nuovo=false, ib_nr_nr, ib_stampando = false, ib_email=false
boolean ib_estero
long il_anno_registrazione, il_num_registrazione, il_anno[], il_num[], il_corrente = 0
long il_num_copie
string is_flag_tipo_fat_ven

// ------------- dichiarate per stampa da pagina a pagina -----------
boolean ib_stampa = true
string is_flag_email[], is_email_amministrazione[], is_email_utente[]
long    il_totale_pagine, il_pagina_corrente

// ------------- discalimer per invio fatture tramite email ----------
constant string is_disclaimer=""

// -- stefanop: progetto 128, parametro Visualizza Stampa Fatture
private:
	boolean ib_vsf = false
	
// stefanop 17/02/2012: percorso file per filigrana
private:
	string is_path_filigrana = ""
	string is_cod_cliente
	string is_cod_lingua
	boolean	ib_MAST = false
end variables

forward prototypes
public subroutine wf_leggi_scadenze (long fl_anno_registrazione, long fl_num_registrazione, ref datetime fdt_data_scadenze[], ref decimal fd_importo_rata[])
public function integer wf_impostazioni ()
public function integer wf_report ()
public subroutine wf_stampa (long copie)
public function integer wf_email ()
public function integer wf_memorizza_blob (long fl_anno_registrazione, long fl_num_registrazione, string fs_path)
public function boolean wf_scadenze_fattura (ref string as_scad_fat)
public function integer wf_replace_marker (ref string as_source, string as_find, string as_replace, boolean ab_case_sensitive)
public function integer wf_imposta_barcode ()
public function integer wf_testo_mail (string fs_rag_soc_1, long fl_anno, long fl_numero, datetime fdt_data, string fs_lingua, ref string fs_testo)
public function integer wf_profilo_registrazione (string as_profilo_impresa, ref long al_numero_registro, ref string as_errore)
end prototypes

event ue_close();close(this)
end event

event ue_avviso_spedizione();uo_avviso_spedizioni luo_avviso_sped

string ls_tipo_gestione, ls_messaggio, ls_path_logo_intestazione
long ll_ret

//solo se non hai stampato da numero a numero
if not ib_nr_nr then
	if upperbound(il_anno)>0 and upperbound(il_num)>0 then
		if il_anno[1]>0 and il_num[1]>0 then
		
			luo_avviso_sped = create uo_avviso_spedizioni
		
			ls_tipo_gestione = "fat_ven"
			
			//passaggio path file dell'intestazione report ---------------------------------------
			try
				ls_path_logo_intestazione = dw_report_fat_ven.Describe ("intestazione.filename") 
			catch (throwable err)
				ls_path_logo_intestazione = ""
			end try
			
			luo_avviso_sped.is_path_logo_intestazione = ls_path_logo_intestazione
			//-----------------------------------------------------------------------------------------
			
			//passa l'oggetto dw del report
			luo_avviso_sped.idw_report = dw_report_fat_ven
			
			ll_ret = luo_avviso_sped.uof_avviso_spedizione(il_anno[1], il_num[1], ls_tipo_gestione, ls_messaggio)
	
			if ll_ret<0 then
				g_mb.error(ls_messaggio)
				rollback;
				return
			end if
		
			commit;
			
			destroy luo_avviso_sped
		else
			g_mb.error("Nessun documento presente (>0)!")
			rollback;
			return
		end if
	else
		g_mb.error("Nessun documento presente (ubound)!")
		rollback;
		return
	end if
end if
end event

public subroutine wf_leggi_scadenze (long fl_anno_registrazione, long fl_num_registrazione, ref datetime fdt_data_scadenze[], ref decimal fd_importo_rata[]);long ll_i

declare cu_scadenze cursor for 
	select   scad_fat_ven.data_scadenza, 
				scad_fat_ven.imp_rata_valuta
	from     scad_fat_ven 
	where    scad_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				scad_fat_ven.anno_registrazione = :fl_anno_registrazione and 
				scad_fat_ven.num_registrazione = :fl_num_registrazione;

open cu_scadenze;

ll_i = 0
do while 0 = 0
	ll_i = ll_i + 1
   fetch cu_scadenze into :fdt_data_scadenze[ll_i], 
							     :fd_importo_rata[ll_i];

   if sqlca.sqlcode <> 0 then exit
loop

close cu_scadenze;
return

end subroutine

public function integer wf_impostazioni ();string ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven, ls_cod_cliente, ls_flag_tipo_cliente, ls_path_logo_1, &
       ls_path_logo_2, ls_modify, ls_dataobject, ls_logo_testata, ls_cod_lingua

long   ll_copie, ll_copie_cee, ll_copie_extra_cee



save_on_close(c_socnosave)

ib_estero = false
il_num_copie = 3
setnull(ls_logo_testata)

select cod_cliente,
       	cod_tipo_fat_ven
into   	:ls_cod_cliente,
		:ls_cod_tipo_fat_ven
from   tes_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and
       	anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;
		 
select flag_tipo_cliente,
		 cod_lingua
into   :ls_flag_tipo_cliente,
		 :ls_cod_lingua
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :ls_cod_cliente;
		 
if sqlca.sqlcode = 0 and not isnull(ls_cod_cliente) then

	// *** Modifica Michela 04/12/2007: specifica report fatture di centro gibus rev.02 del 03/12/2007
	//												controllo se esistono per quel cliente particolari dw. altrimenti controllo se esistono particolari dw per 
	//												la sola lingua, altrimenti prendo il dataobject nel tipo fattura
	if not isnull( ls_cod_lingua) and ls_cod_lingua <> "" then
		
		select  dataobject, 
				 logo_testata
		into	 :ls_dataobject, 
				 :ls_logo_testata
		from	 tab_tipi_fat_ven_lingue_dw
		where	 cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven and
				 cod_lingua = :ls_cod_lingua and
				 cod_cliente = :ls_cod_cliente;
				 
		if sqlca.sqlcode <> 0 then
			
			// *** non esiste per quel cliente un modulo specifico; controllo se esiste a livello di lingua
			select dataobject,
					logo_testata
			into	 :ls_dataobject,
					:ls_logo_testata
			from	 tab_tipi_fat_ven_lingue_dw
			where	 cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven and
					 cod_lingua = :ls_cod_lingua and
					 ( cod_cliente IS NULL or cod_cliente = '' );
			
		end if
			
	end if
	
end if


// se il dataobject è vuoto, lo prendo dal tipo fattura; altrimenti tengo quello di default
if isnull(ls_dataobject) or len(ls_dataobject) < 1 then
	select dataobject
	into	 :ls_dataobject
	from	 tab_tipi_fat_ven
	where	 cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
			 
end if
				 
if not isnull(ls_dataobject) and len(ls_dataobject) > 0  then
	dw_report_fat_ven.dataobject = ls_dataobject
end if




// determino quante copie stampare della fattura
select num_copie,
		 num_copie_cee,
		 num_copie_extra_cee
into     :ll_copie,
		 :ll_copie_cee,
		 :ll_copie_extra_cee
from   tab_tipi_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
if sqlca.sqlcode = 0 then
	choose case ls_flag_tipo_cliente
		case "C"
			ib_estero = true
			il_num_copie = ll_copie_cee
		case "E"
			ib_estero = true
			il_num_copie = ll_copie_extra_cee
		case else
			ib_estero = false
			il_num_copie = ll_copie
	end choose
end if
				

if isnull(ls_logo_testata) or len(ls_logo_testata) < 1 then

	select parametri_azienda.stringa
	into   :ls_path_logo_1
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LO3';
	
	ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
	dw_report_fat_ven.modify(ls_modify)

else
	
	ls_modify = "intestazione.filename='" + s_cs_xx.volume + s_cs_xx.risorse + ls_logo_testata + "'~t"
	dw_report_fat_ven.modify(ls_modify)
	
end if

if isnull(il_num_copie) or il_num_copie < 0 then
	il_num_copie = 1
end if

is_cod_cliente = ls_cod_cliente
is_cod_lingua = ls_cod_lingua
cb_1.text = "STAMPA " + string(il_num_copie) + " COPIE"

return 0
end function

public function integer wf_report ();boolean	lb_prima_riga, lb_flag_prodotto_cliente, lb_flag_nota_dettaglio, lb_flag_nota_piede, &
         	lb_riga_premio=false, lb_nomenclatura=false

string 	ls_stringa, ls_stringa_euro, ls_cod_tipo_fat_ven, ls_cod_banca_clien_for, ls_cod_cliente, &
		 	ls_cod_valuta, ls_cod_pagamento, ls_num_ord_cliente, ls_cod_porto, ls_cod_resa, ls_null, &
		 	ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
		 	ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
		 	ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, &
		 	ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli, &
		 	ls_partita_iva, ls_cod_fiscale, ls_cod_lingua, ls_flag_tipo_cliente, &
		 	ls_des_tipo_fat_ven, ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, &
  		 	ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua, ls_des_tipo_det_ven,&
		 	ls_des_prodotto_anag, ls_flag_stampa_fattura, ls_cod_tipo_det_ven, ls_flag_tipo_fat_ven, &
		 	ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_cod_iva, ls_des_iva, ls_causale_trasporto, &
		 	ls_cod_vettore, ls_vettore_rag_soc_1, ls_vettore_rag_soc_2, ls_vettore_indirizzo, &
		 	ls_vettore_cap, ls_vettore_localita, ls_vettore_provincia, ls_des_esenzione_iva[], &
		 	ls_cod_documento, ls_numeratore_doc, ls_cod_banca, ls_flag_tipo_pagamento, ls_cod_abi, ls_cod_cab, &
		 	ls_cod_des_cliente, ls_telefono_cliente, ls_fax_cliente, ls_telefono_des, ls_fax_des, &
		 	ls_des_valuta, ls_des_valuta_lingua, ls_dicitura_std, ls_cod_agente_1, ls_cod_agente_2,&
		 	ls_anag_agenti_rag_soc_1, ls_anag_agenti_rag_soc_2, ls_cod_fornitore, ls_cod_des_fornitore, &
		 	ls_cod_des_fattura, ls_rag_soc_1_dest_fat, ls_rag_soc_2_dest_fat, ls_indirizzo_dest_fat, &
		 	ls_localita_dest_fat, ls_frazione_dest_fat, ls_cap_dest_fat, ls_provincia_dest_fat, ls_des_causale, &
		 	ls_cod_prodotto_finito, ls_cod_non_a_magazzino, ls_cod_verniciatura, ls_cod_tessuto, ls_des_gruppo_tessuto, &
		 	ls_descrizione_riga, ls_descrizione_riga_lingua, ls_des_verniciatura, ls_codici_iva[], ls_messaggio_premio, &
		 	ls_flag_st_note_tes, ls_flag_st_note_pie, ls_flag_st_note_det, ls_cod_misura_mag, ls_formato, &
			ls_flag_tipo_det, ls_flag_tipo_fat,ls_cod_cin, ls_cod_iban, ls_conto_corrente, ls_des_pagamento_estesa, &
			ls_cod_inoltro, ls_inoltro_rag_soc_1, ls_inoltro_rag_soc_2, ls_inoltro_indirizzo, ls_cod_contatto, &
		 	ls_inoltro_cap, ls_inoltro_localita, ls_inoltro_provincia, ls_cod_mezzo, ls_des_mezzo, ls_des_mezzo_lingua, &
			ls_des_imballo, ls_cod_imballo, ls_parametro_colli, ls_scad_fat, ls_vsf, ls_cod_deposito, ls_des_deposito, &
			ls_provincia_dep, ls_cap_dep, ls_indirizzo_dep, ls_telefono_dep, ls_fax_dep, ls_swift, ls_des_misura_lingua, &
			ls_cod_misura_um, ls_colore_passamaneria, ls_flag_tipo_mantovana, ls_des_colore_tessuto, ls_localita_dep, &
			ls_nome_file, ls_condizioni, ls_cod_nazione, ls_des_nazione, ls_stampa_nota_testata, ls_stampa_nota_piede, &
			ls_stampa_nota_video,ls_profilo_impresa, ls_des_estesa[], ls_stato_cli, ls_cod_iban_cliente, ls_errore, ls_tipo_destinazione, &
			ls_intestazione, ls_destinazione, ls_des_breve_tessuto, ls_flag_tipo_lista_bolle, ls_variabili, ls_cod_nomenclatura, &
			ls_stampa_nota_testata_prod, ls_stampa_nota_piede_prod, ls_stampa_nota_video_prod
			 
long   	ll_errore, ll_num_colli, ll_num_aliquote, ll_anno_documento, ll_num_documento, li_filenum, &
       		ll_anno_registrazione_ord_ven, ll_num_registrazione_ord_ven, ll_prog_riga_ord_ven, ll_ret, li_risp, ll_riga, &
			ll_numero_registro, ll_anno_registrazione_bol_ven, ll_num_registrazione_bol_ven, ll_prog_riga_bol_ven

dec{4} 	ld_tot_fattura, ld_sconto_tes, ld_quan_fatturata, ld_prezzo_vendita, ld_sconto_1, &
	    		ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7,&
		 	ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_tot, ld_imponibile_riga, &
		 	ld_val_riga, ld_sconto_pagamento, ld_cambio_ven, ld_prezzo_um, ld_quantita_um, ld_val_sconto_riga, &
		   	ld_imponibili_valuta[], ld_iva_valuta[],ld_aliquote_iva[] ,ld_rate_scadenze[], ld_imponibile_iva_valuta, &
		   	ld_importo_iva_valuta, ld_perc_iva, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_tot_merci, ld_tot_cassa, &
			ld_tot_sconti_commerciali, ld_tot_fattura_euro, ld_precisione, ld_valore_parziale, ld_peso_lordo, ld_peso_netto, &
			ld_dim_x_riga, ld_dim_y_riga,ld_imponibile_iva, ld_importo_iva, ld_val_gtot
			
dec{5}   ld_fat_conversione_ven

datetime ldt_data_ord_cliente, ldt_data_registrazione, ldt_scadenze[], ldt_data_fattura, ldt_data_inizio_trasporto, &
			ldt_ora_inizio_trasporto, ldt_data_decorrenza_pagamento

uo_calcola_documento_euro luo_doc
uo_premi_clienti luo_premi_clienti
uo_stampa_dati_prodotto luo_stampa_dati_prodotto

setnull(ls_null)
lb_nomenclatura = cbx_nomenclatura.checked

setredraw(false)

//---------------------------------------------------------------------------------
select cod_cliente
into   :ls_cod_cliente
from   tes_fat_ven  
where  tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_fat_ven.anno_registrazione = :il_anno_registrazione and 
		 tes_fat_ven.num_registrazione = :il_num_registrazione;
//---------------------------------------------------------------------------------

if not isnull(ls_cod_cliente) and ls_cod_cliente<>"" then

	luo_premi_clienti = CREATE uo_premi_clienti
	ll_ret = luo_premi_clienti.uof_stampa_premio_fattura(il_anno_registrazione,il_num_registrazione, ref ls_messaggio_premio)
	if ll_ret = 0 and len(ls_messaggio_premio) > 0 then
		lb_riga_premio = true
	elseif ll_ret = -1 then
		g_mb.messagebox("APICE",ls_messaggio_premio,Stopsign!)
	end if
	DESTROY luo_premi_clienti
	
end if

ls_flag_tipo_lista_bolle = "N"

Select flag_tipo_lista_bolle
into 	:ls_flag_tipo_lista_bolle
from 	con_vendite
where cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then
	ls_flag_tipo_lista_bolle = "N"
end if

dw_report_fat_ven.reset()

wf_imposta_barcode()

select parametri_azienda.stringa  
into  :ls_stringa  
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'CVL';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

select parametri_azienda.stringa  
into  :ls_stringa_euro
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'EUR';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa_euro)
end if

// EnMe 25-2-2009; stabilisce se stampare oppure no il numero colli nei doc fiscali
select flag  
into  :ls_parametro_colli
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'F' and  
      parametri_azienda.cod_parametro = 'SCL';

if sqlca.sqlcode <> 0 then
	ls_parametro_colli = "N"
end if

// -- stefanop 04/03/2010: Progetto 128
// Carico parametro VSF
select flag
into	:ls_vsf
from	parametri_azienda
where
	cod_azienda = :s_cs_xx.cod_azienda and
	flag_parametro = 'F' and
	cod_parametro = 'VSF';
	
if sqlca.sqlcode = 0 and ls_vsf = 'S' then
	ib_vsf = true
else
	ib_vsf = false
end if

if ib_vsf then
	if not wf_scadenze_fattura(ls_scad_fat) then
		return -1
	end if
else
	ls_scad_fat = ""
end if
// ----


select cod_tipo_fat_ven,   
		 data_registrazione,   
		 cod_cliente,   
		 cod_fornitore,
		 cod_contatto,
		 cod_valuta,   
		 cod_pagamento,   
		 sconto,   
		 cod_banca_clien_for,   
		 cod_banca,   
		 num_ord_cliente,   
		 data_ord_cliente,   
		 cod_porto,   
		 cod_resa,   
		 nota_testata,   
		 nota_piede,   
		 imponibile_iva_valuta,
		 importo_iva_valuta,
		 imponibile_iva,
		 importo_iva,
		 tot_fattura_valuta,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia,
 		 rag_soc_1_dest_fat,   
		 rag_soc_2_dest_fat,   
		 indirizzo_dest_fat,   
		 localita_dest_fat,   
		 frazione_dest_fat,   
		 cap_dest_fat,   
		 provincia_dest_fat,
		 cambio_ven,
		 num_colli,
		 cod_vettore,
		 cod_causale,
		 cod_documento,
		 numeratore_documento,
		 anno_documento,
		 num_documento,
		 data_fattura,
		 cod_agente_1,
		 cod_agente_2,
		 cod_des_cliente,
		 cod_des_fornitore,
		 tot_merci,
		 tot_sconto_cassa,
		 tot_sconti_commerciali,
		 flag_st_note_tes,
		 flag_st_note_pie,
		 tot_fattura,
		 cod_inoltro,
		 peso_lordo,
		 peso_netto,
		 data_inizio_trasporto,
		 ora_inizio_trasporto,
		 cod_mezzo,
		 cod_imballo,
		 cod_deposito,
		 data_decorrenza_pagamento
into   :ls_cod_tipo_fat_ven,   
	 	 :ldt_data_registrazione,   
	 	 :ls_cod_cliente,   
		 :ls_cod_fornitore,
		 :ls_cod_contatto,
		 :ls_cod_valuta,   
		 :ls_cod_pagamento,   
		 :ld_sconto_tes,
		 :ls_cod_banca_clien_for,   
		 :ls_cod_banca,
		 :ls_num_ord_cliente,   
		 :ldt_data_ord_cliente,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_imponibile_iva_valuta,
		 :ld_importo_iva_valuta,
		 :ld_imponibile_iva,
		 :ld_importo_iva,
		 :ld_tot_fattura,   
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,
		 :ls_rag_soc_1_dest_fat,   
		 :ls_rag_soc_2_dest_fat,   
		 :ls_indirizzo_dest_fat,   
		 :ls_localita_dest_fat,   
		 :ls_frazione_dest_fat,   
		 :ls_cap_dest_fat,   
		 :ls_provincia_dest_fat,		 
		 :ld_cambio_ven,
		 :ll_num_colli,
		 :ls_cod_vettore,
		 :ls_causale_trasporto,
		 :ls_cod_documento,
		 :ls_numeratore_doc,
		 :ll_anno_documento,
		 :ll_num_documento,
		 :ldt_data_fattura,
		 :ls_cod_agente_1,
		 :ls_cod_agente_2,
		 :ls_cod_des_cliente,
		 :ls_cod_des_fornitore,
		 :ld_tot_merci,
		 :ld_tot_cassa,
		 :ld_tot_sconti_commerciali,
		 :ls_flag_st_note_tes,
		 :ls_flag_st_note_pie,
		 :ld_tot_fattura_euro,
		 :ls_cod_inoltro,
		 :ld_peso_lordo,
		 :ld_peso_netto,
		 :ldt_data_inizio_trasporto,
		 :ldt_ora_inizio_trasporto,
		 :ls_cod_mezzo,
		 :ls_cod_imballo,
		 :ls_cod_deposito,
		 :ldt_data_decorrenza_pagamento
from   tes_fat_ven  
where  tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_fat_ven.anno_registrazione = :il_anno_registrazione and 
		 tes_fat_ven.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca testata fattura: stampa interrotta", Stopsign!)
	return -1
end if

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return -1
end if


// stefanop: 19/12/2011: aggiunto deposito
select 
	localita,
	provincia,
	cap,
	indirizzo,
	telefono,
	fax,
	localita
into 
	:ls_des_deposito,
	:ls_provincia_dep,
	:ls_cap_dep,
	:ls_indirizzo_dep,
	:ls_telefono_dep,
	:ls_fax_dep,
	:ls_localita_dep
from anag_depositi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_deposito = :ls_cod_deposito;

if isnull(ls_des_deposito) then ls_des_deposito = ""
if isnull(ls_provincia_dep) then ls_provincia_dep = ""
if isnull(ls_cap_dep) then ls_cap_dep = ""
if isnull(ls_localita_dep) then ls_localita_dep = ""
if isnull(ls_indirizzo_dep) then ls_indirizzo_dep = ""
if isnull(ls_telefono_dep) then ls_telefono_dep = ""
if isnull(ls_fax_dep) then ls_fax_dep = ""
// ----

//-------------------------------------- Modifica Nicola -------------------------------------------------------
if ls_flag_st_note_tes = 'I' then   //nota di testata
	select flag_st_note
	  into :ls_flag_st_note_tes
	  from tab_tipi_fat_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
end if		

if ls_flag_st_note_tes = 'N' then
	ls_nota_testata = ""
end if			


if ls_flag_st_note_pie = 'I' then 	//nota di piede
	select flag_st_note
	  into :ls_flag_st_note_pie
	  from tab_tipi_fat_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
end if			

if ls_flag_st_note_pie = 'N' then
	ls_nota_piede = ""
end if	

if lb_riga_premio then
	if isnull(ls_nota_piede) or len(ls_nota_piede) < 1 then
		ls_nota_piede = ls_messaggio_premio
	else
		ls_nota_piede += "~r~n" + ls_messaggio_premio
	end if
end if
//--------------------------------------- Fine Modifica --------------------------------------------------------		


select des_tipo_fat_ven,
       flag_tipo_fat_ven,
		 profilo_impresa
into   :ls_des_tipo_fat_ven,
       :ls_flag_tipo_fat_ven,
		 :ls_profilo_impresa
from   tab_tipi_fat_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_fat_ven)
end if

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca flag_tipo_fat_ven in tabella tab_tipi_fat_ven: stampa interrotta", Stopsign!)
	return -1
end if

choose case ls_flag_tipo_fat_ven
	case "F", "P"
		ll_numero_registro = 0
	case else

		if s_cs_xx.parametri.impresa then
			
			if isnull(ls_profilo_impresa) or len(ls_profilo_impresa) < 1 then
				g_mb.error("Attenzione; nel tipo fattura non è stato indicato il profilo di riferimento" )
				return -1
			end if
			
			if wf_profilo_registrazione(ls_profilo_impresa, ll_numero_registro, ls_errore) < 0 then
				g_mb.error(ls_errore)
				return -1
			end if
			
//			select numero
//			into :ll_numero_registro
//			from prof_registrazione
//			join mod_registro on prof_registrazione.id_mod_registro = mod_registro.id_mod_registro
//			where prof_registrazione.codice = :ls_profilo_impresa
//			using sqlci;
//			
//			if sqlci.sqlcode <> 0 then
//				g_mb.error("Errore in ricerca numero registro iva con il profilo " + ls_profilo_impresa )
//				return -1
//			end if
			
			if isnull(ll_numero_registro)  then
				g_mb.error("Attenzione; nel profilo di impresa non è indicato il registro iva di riferimento." )
				return -1
			end if
			
		else
			ll_numero_registro = 1
//			g_mb.warning("ATTENZIONE: collegamento con impresa disattivato; impossibile stampare correttamente il numero fattura")
//			return -1
		end if
end choose
	
if ls_flag_tipo_fat_ven <> "F" then
	//occhio, se proforma cliente potrebbe
	if not isnull(ls_cod_cliente) then
	
		select rag_soc_1,   
				 rag_soc_2,   
				 indirizzo,   
				 localita,   
				 frazione,   
				 cap,   
				 provincia,   
				 partita_iva,   
				 cod_fiscale,   
				 cod_lingua,   
				 telefono,   
				 telex,   
				 flag_tipo_cliente,
				 cod_nazione,
				 stato,
				 iban
		into   :ls_rag_soc_1_cli,   
				 :ls_rag_soc_2_cli,   
				 :ls_indirizzo_cli,   
				 :ls_localita_cli,   
				 :ls_frazione_cli,   
				 :ls_cap_cli,   
				 :ls_provincia_cli,   
				 :ls_partita_iva,   
				 :ls_cod_fiscale,   
				 :ls_cod_lingua,  
				 :ls_telefono_cliente,
				 :ls_fax_cliente,
				 :ls_flag_tipo_cliente,
				 :ls_cod_nazione,
				 :ls_stato_cli,
				 :ls_cod_iban_cliente
		from   anag_clienti  
		where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
				 anag_clienti.cod_cliente = :ls_cod_cliente;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca anagrafica cliente: stampa interrotta", Stopsign!)
			return -1
		end if
		
	else
		
		setnull(ls_stato_cli)
		setnull(ls_cod_iban_cliente)
		
		select rag_soc_1,   
			 rag_soc_2,   
			 indirizzo,   
			 localita,   
			 frazione,   
			 cap,   
			 provincia,   
			 partita_iva,   
			 cod_fiscale,   
			 cod_lingua,   
			 telefono,   
			 telex,   
			 flag_tipo_cliente,
			 cod_nazione
	into   :ls_rag_soc_1_cli,   
			 :ls_rag_soc_2_cli,   
			 :ls_indirizzo_cli,   
			 :ls_localita_cli,   
			 :ls_frazione_cli,   
			 :ls_cap_cli,   
			 :ls_provincia_cli,   
			 :ls_partita_iva,   
			 :ls_cod_fiscale,   
			 :ls_cod_lingua,  
			 :ls_telefono_cliente,
			 :ls_fax_cliente,
			 :ls_flag_tipo_cliente,
			 :ls_cod_nazione
	from   anag_contatti  
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_contatto = :ls_cod_contatto;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca anagrafica contatti: stampa interrotta", Stopsign!)
		return -1
	end if
		
	end if
		
else
	select rag_soc_1,   
			 rag_soc_2,   
			 indirizzo,   
			 localita,   
			 frazione,   
			 cap,   
			 provincia,   
			 partita_iva,   
			 cod_fiscale,   
			 cod_lingua,   
			 telefono,   
			 telex,   
			 flag_tipo_fornitore,
			 stato,
			 null,
			 iban
	into   :ls_rag_soc_1_cli,   
			 :ls_rag_soc_2_cli,   
			 :ls_indirizzo_cli,   
			 :ls_localita_cli,   
			 :ls_frazione_cli,   
			 :ls_cap_cli,   
			 :ls_provincia_cli,   
			 :ls_partita_iva,   
			 :ls_cod_fiscale,   
			 :ls_cod_lingua,  
			 :ls_telefono_cliente,
			 :ls_fax_cliente,
			 :ls_flag_tipo_cliente,
			 :ls_stato_cli,
			 :ls_cod_nazione,
			 :ls_cod_iban_cliente
	from   anag_fornitori  
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_fornitore = :ls_cod_fornitore;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca anagrafica fornitori: stampa interrotta", Stopsign!)
		return -1
	end if
end if

if not isnull(ls_stato_cli) and ls_stato_cli<>"" then
	if not isnull(ls_localita_cli) and ls_localita_cli<>"" then
		ls_localita_cli += " - " + ls_stato_cli
	else
		ls_localita_cli = ls_stato_cli
	end if
end if


if ls_flag_tipo_fat_ven = "I" and len(ls_rag_soc_1) > 0 and not isnull(ls_rag_soc_1)  then
	dw_report_fat_ven.object.t_1.text = "DESTINAZIONE MERCE"
else
	if ls_flag_tipo_cliente = "E" or ls_flag_tipo_cliente = "C" then
		// se è estero lascio come scritto nel modulo
	else
		dw_report_fat_ven.object.t_1.text = "SPETTABILE"
	end if
end if


// vettore
select rag_soc_1,
       indirizzo,   
       localita,   
       cap,   
       provincia  
into   :ls_vettore_rag_soc_1,   
       :ls_vettore_indirizzo,   
       :ls_vettore_localita,   
       :ls_vettore_cap,   
       :ls_vettore_provincia
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and
	    cod_vettore = :ls_cod_vettore;

if sqlca.sqlcode <> 0 then
   setnull(ls_vettore_rag_soc_1)   
   setnull(ls_vettore_indirizzo)   
   setnull(ls_vettore_localita)   
   setnull(ls_vettore_cap)   
   setnull(ls_vettore_provincia)
end if

// inoltro
select rag_soc_1,
       indirizzo,   
       localita,   
       cap,   
       provincia  
into   :ls_inoltro_rag_soc_1,   
       :ls_inoltro_indirizzo,   
       :ls_inoltro_localita,   
       :ls_inoltro_cap,   
       :ls_inoltro_provincia
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and
	    cod_vettore = :ls_cod_inoltro;

if sqlca.sqlcode <> 0 then
   setnull(ls_inoltro_rag_soc_1)   
   setnull(ls_inoltro_indirizzo)   
   setnull(ls_inoltro_localita)   
   setnull(ls_inoltro_cap)   
   setnull(ls_inoltro_provincia)
end if


if ls_flag_tipo_cliente = 'E' or ls_flag_tipo_cliente = 'C' then
	ls_partita_iva = ls_cod_fiscale
end if

select des_pagamento,   
       sconto,
       flag_tipo_pagamento,
		 des_pagamento_2
into   :ls_des_pagamento,   
       :ld_sconto_pagamento,
       :ls_flag_tipo_pagamento,
		 :ls_des_pagamento_estesa
from   tab_pagamenti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
	setnull(ls_des_pagamento_estesa)
   	ls_flag_tipo_pagamento = "D"
else
	if not isnull(ldt_data_fattura) then
		if not isnull(ldt_data_decorrenza_pagamento) and ldt_data_fattura <> ldt_data_decorrenza_pagamento then
			ls_des_pagamento += " (DECORRENZA " + string(ldt_data_decorrenza_pagamento,"dd/mm/yyyy")
		end if 
	end if
end if

select des_pagamento
into   :ls_des_pagamento_lingua
from   tab_pagamenti_lingue  
where  cod_azienda   = :s_cs_xx.cod_azienda and 
       cod_pagamento = :ls_cod_pagamento and
       cod_lingua    = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
else
	if not isnull(ldt_data_fattura) then
		if not isnull(ldt_data_decorrenza_pagamento) and ldt_data_fattura <> ldt_data_decorrenza_pagamento then
			ls_des_pagamento_lingua += " (DECORRENZA " + string(ldt_data_decorrenza_pagamento,"dd/mm/yyyy")
		end if 
	end if
end if

if not isnull(ls_cod_nazione) then
	select des_nazione
	into :ls_des_nazione
	from tab_nazioni
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_nazione = :ls_cod_nazione;
	if sqlca.sqlcode <> 0 then
		setnull(ls_cod_nazione)
		setnull(ls_des_nazione)
	end if
else
	setnull(ls_des_nazione)
end if

setnull(ls_cod_iban)
setnull(ls_conto_corrente)

if ls_flag_tipo_pagamento <> "R" or (ls_flag_tipo_pagamento = "R" and ls_flag_tipo_fat_ven="F") then
	//if not isnull(ls_cod_banca) then
	// se diverso da riba visualizzo la banca nostra; oppure se pro-forma fornitore e pagamento RIBA mostro banca nostra
	select des_banca,
	       cod_abi,
			 cod_cab,
			 cin,
			 iban,
			 conto_corrente,
			 swift
	into   :ls_des_banca,
			 :ls_cod_abi,
			 :ls_cod_cab,
			 :ls_cod_cin,
			 :ls_cod_iban,
			 :ls_conto_corrente,
			 :ls_swift
	from   anag_banche
	where  anag_banche.cod_azienda = :s_cs_xx.cod_azienda and 
			 anag_banche.cod_banca = :ls_cod_banca;
elseif not isnull(ls_cod_banca_clien_for) then
	// carico banca del cliente
	setnull(ls_swift)
	ls_cod_iban = ls_cod_iban_cliente
	select des_banca,
			 cod_abi,
			 cod_cab,
			 cin
	into   :ls_des_banca,
			 :ls_cod_abi,
			 :ls_cod_cab,
			 :ls_cod_cin
	from   anag_banche_clien_for  
	where  anag_banche_clien_for.cod_azienda = :s_cs_xx.cod_azienda and 
			 anag_banche_clien_for.cod_banca_clien_for = :ls_cod_banca_clien_for;	
end if			 

if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca)
	setnull(ls_cod_abi)
	setnull(ls_cod_cab)
	setnull(ls_cod_cin)
	setnull(ls_cod_iban)
end if


select des_imballo
into   :ls_des_imballo
from   tab_imballi
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_imballo = :ls_cod_imballo;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballo)
end if

select des_mezzo 
into   :ls_des_mezzo  
from   tab_mezzi  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_mezzo = :ls_cod_mezzo;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo)
end if

select des_mezzo  
into   :ls_des_mezzo_lingua  
from   tab_mezzi_lingue 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_mezzo = :ls_cod_mezzo and
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo_lingua)
end if


select des_porto  
into   :ls_des_porto  
from   tab_porti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_porto = :ls_cod_porto;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_porto = :ls_cod_porto and
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select des_resa
into   :ls_des_resa  
from   tab_rese  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_resa = :ls_cod_resa;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_resa = :ls_cod_resa and  
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

select des_valuta,
		 precisione
into   :ls_des_valuta,
		 :ld_precisione
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta = :ls_cod_valuta;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta)
end if

select des_valuta
into   :ls_des_valuta_lingua  
from   tab_valute_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_valuta = :ls_cod_valuta and  
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta_lingua)
end if

if not isnull(ls_cod_agente_1) then
	select rag_soc_1
	into   :ls_anag_agenti_rag_soc_1
	from   anag_agenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_agente = :ls_cod_agente_1;
	if sqlca.sqlcode <> 0 then setnull(ls_anag_agenti_rag_soc_1)
end if

if not isnull(ls_cod_agente_2) then
	select rag_soc_1
	into   :ls_anag_agenti_rag_soc_2
	from   anag_agenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_agente = :ls_cod_agente_2;
	if sqlca.sqlcode <> 0 then setnull(ls_anag_agenti_rag_soc_2)
end if

if not isnull(ls_cod_des_cliente) then
	if ls_flag_tipo_fat_ven <> "F" then	
		select telefono,   
				 telex  
		into   :ls_telefono_des,   
				 :ls_fax_des  
		from   anag_des_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente and
				 cod_des_cliente = :ls_cod_des_cliente ;
		if sqlca.sqlcode <> 0 then
			setnull(ls_telefono_des)
			setnull(ls_fax_des)
		end if
	else
		select telefono,   
				 telex  
		into   :ls_telefono_des,   
				 :ls_fax_des  
		from   anag_des_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore and
				 cod_des_fornitore = :ls_cod_des_fornitore ;
		if sqlca.sqlcode <> 0 then
			setnull(ls_telefono_des)
			setnull(ls_fax_des)
		end if
	end if
end if

//Donato 15/02/2013
//creata la funzione leggi iva centralizzata in uo_functions, in base alla gestione ed alla lingua del cliente/fornitore
//se la lingua non è gestita passare stringa vuota nel relativo argomento
guo_functions.uof_leggi_iva(il_anno_registrazione, il_num_registrazione, ls_cod_lingua, "iva_fat_ven", 	ls_codici_iva[], &
																																	ld_aliquote_iva[], &
																																	ld_imponibili_valuta[], &
																																	ld_iva_valuta[], &
																																	ls_des_esenzione_iva[], &
																																	ls_des_estesa[])
//--------------------------------------------------------------------------------------------------------------------------------

wf_leggi_scadenze(il_anno_registrazione, il_num_registrazione, ldt_scadenze[], ld_rate_scadenze[])
						
						
// -----------------------------  modifica Enrico 21/01/2003 per stampa note fisse solo nei documenti -----------------
ls_stampa_nota_testata = ""
ls_stampa_nota_piede = ""
ls_stampa_nota_video = ""

if ls_flag_tipo_fat_ven = "F" then // Fornitore
	if guo_functions.uof_get_note_fisse(ls_null, ls_cod_fornitore, ls_null,"STAMPA_FAT_VEN", ls_cod_tipo_fat_ven, ldt_data_registrazione, ref ls_stampa_nota_testata, ref ls_stampa_nota_piede, ref ls_stampa_nota_video) < 0 then
		g_mb.error(ls_stampa_nota_testata)
	else
		if len(ls_stampa_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_stampa_nota_video)
	end if
else // Cliente
	if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null,"STAMPA_FAT_VEN", ls_cod_tipo_fat_ven, ldt_data_registrazione, ref ls_stampa_nota_testata, ref ls_stampa_nota_piede, ref ls_stampa_nota_video) < 0 then
		g_mb.error(ls_stampa_nota_testata)
	else
		if len(ls_stampa_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_stampa_nota_video)
	end if
end if
	
if not isnull(ls_stampa_nota_testata) and len(ls_stampa_nota_testata) > 0 then
	if isnull(ls_nota_testata) or len(ls_nota_testata) = 0 then
		ls_nota_testata = ls_stampa_nota_testata
	else
		ls_nota_testata = ls_stampa_nota_testata + "~r~n" + ls_nota_testata
	end if
end if

if not isnull(ls_stampa_nota_piede) and len(ls_stampa_nota_piede) > 0 then
	if isnull(ls_nota_piede) or len(ls_nota_piede) = 0 then
		ls_nota_piede = ls_stampa_nota_piede
	else
		ls_nota_piede =  ls_nota_piede + "~r~n" + ls_stampa_nota_piede
	end if
end if

if not isnull(ls_stampa_nota_video) and len(ls_stampa_nota_video) > 0 then
	g_mb.messagebox("NOTA PER L'OPERATORE",ls_stampa_nota_video + "~r~nPREMERE INVIO PER PROSEGUIRE",information!)
end if
// --------------------------------------------------------------------------------------------------------------------
						
					
declare cu_dettagli cursor for 
	select   anno_reg_ord_ven,
				num_reg_ord_ven,
				prog_riga_ord_ven,
				cod_tipo_det_ven, 
				cod_misura, 
				quan_fatturata, 
				prezzo_vendita, 
				sconto_1, 
				sconto_2, 
				sconto_3, 
				sconto_4, 
				sconto_5, 
				sconto_6, 
				sconto_7, 
				sconto_8, 
				sconto_9, 
				sconto_10, 
				nota_dettaglio, 
				cod_prodotto, 
				des_prodotto,
				fat_conversione_ven,
				cod_iva,
				flag_st_note_det,
				quantita_um,
				prezzo_um,
				imponibile_iva_valuta,
				anno_registrazione_bol_ven,
				num_registrazione_bol_ven,
				prog_riga_bol_ven
	from     det_fat_ven 
	where    cod_azienda = :s_cs_xx.cod_azienda and 
				anno_registrazione = :il_anno_registrazione and 
				num_registrazione = :il_num_registrazione
	order by cod_azienda, 
				anno_registrazione, 
				num_registrazione,
				ordinamento, //stefanop 09/05/2010: progetto 140: ordinamento righe dettaglio
				prog_riga_fat_ven;

open cu_dettagli;

if not isnull(ls_nota_testata) and len(trim(ls_nota_testata)) > 0 then lb_prima_riga = true

lb_flag_prodotto_cliente = false
lb_flag_nota_dettaglio = false
lb_flag_nota_piede = false


//##################################################################
//per MAST
if ib_MAST then
	//sx											dx
	//campo dest								campo intestazione
	
	if g_str.isnotempty(ls_rag_soc_1_dest_fat) then
		//destinazione FATTURA (diversa)  						-> a sx (come CLIENTE) vanno i dati del cliente a dx (come SPETTABILE) i dati della destinazione fattura
		ls_tipo_destinazione = "fattura"																										//così è stato chiesto da Angela Marchese
		
	elseif g_str.isnotempty(ls_rag_soc_1) then
		//destinazione MERCE (diversa)							-> a sx (come DESTINAZIONE MERCE) va la destinazione merce a dx (come CLIENTE) i dati del cliente
		ls_tipo_destinazione = "merce"																											//così è stato chiesto da Angela Marchese
		
	else
		//no destinazione merce e no destinazione fattura 	-> a sx (come SPETTABILE) e a dx (come CLIENTE) vanno i dati del cliente
		ls_tipo_destinazione = ""																													//così è stato chiesto da Angela Marchese
	end if
	
	try
		//ls_tipo_destinazione può essere
		//		""					non impostata nè destinazione merce nè destinazione fattura
		//		"merce"			impostata destinazione merce
		//		"fattura"			impostata destinazione fattura
		
		//a sx la destinazione documento    						a dx la intestazione documento
		
		choose case ls_tipo_destinazione
			case ""
				//a sx (come SPETTABILE) vanno i dati del cliente (che però in tal caso saranno coincidenti con i rispettivi campi di tes_fat_ven)
				dw_report_fat_ven.object.titolo_destinazione_t.text = "SPETTABILE"
				
				ls_destinazione = "~r~n" + ls_rag_soc_1_cli
				if g_str.isnotempty(ls_rag_soc_2_cli) then ls_destinazione += " " + ls_rag_soc_2_cli
				ls_destinazione += "~r~n"
				if g_str.isnotempty(ls_indirizzo_cli) then ls_destinazione += ls_indirizzo_cli + "~r~n"
				if g_str.isnotempty(ls_cap_cli) then ls_destinazione += ls_cap_cli + " "
				if g_str.isnotempty(ls_localita_cli) then ls_destinazione += ls_localita_cli + " "
				if g_str.isnotempty(ls_provincia_cli) then ls_destinazione += "("+ls_provincia_cli + ")"
				ls_destinazione += "~r~n"
				if g_str.isnotempty(ls_cod_nazione) then ls_destinazione += f_des_tabella("tab_nazioni", "cod_nazione='" +  ls_cod_nazione +"'", "des_nazione")
				
				
				//a dx (come CLIENTE) vanno i dati del cliente
				dw_report_fat_ven.object.titolo_intestazione_t.text = "CLIENTE: "
				
				ls_intestazione = "                    "+ ls_cod_cliente + "~r~n" + ls_rag_soc_1_cli
				if g_str.isnotempty(ls_rag_soc_2_cli) then ls_intestazione += " " + ls_rag_soc_2_cli
				ls_intestazione += "~r~n"
				if g_str.isnotempty(ls_indirizzo_cli) then ls_intestazione += ls_indirizzo_cli + "~r~n"
				if g_str.isnotempty(ls_cap_cli) then ls_intestazione += ls_cap_cli + " "
				if g_str.isnotempty(ls_localita_cli) then ls_intestazione += ls_localita_cli + " "
				if g_str.isnotempty(ls_provincia_cli) then ls_intestazione += "(" +ls_provincia_cli + ")"
				ls_intestazione += "~r~n"
				if g_str.isnotempty(ls_partita_iva) then ls_intestazione += "P.I.: " + ls_partita_iva + "   "
				if g_str.isnotempty(ls_cod_fiscale) then ls_intestazione += "C.F.: " + ls_cod_fiscale
				
			case "merce"
				//a sx va la destinazione merce (come DESTINAZIONE MERCE)
				dw_report_fat_ven.object.titolo_destinazione_t.text = "DESTINAZIONE MERCE"
				
				ls_destinazione = "~r~n" + ls_rag_soc_1
				if g_str.isnotempty(ls_rag_soc_2) then ls_destinazione += " " + ls_rag_soc_2
				ls_destinazione += "~r~n"
				if g_str.isnotempty(ls_indirizzo) then ls_destinazione += ls_indirizzo + "~r~n"
				if g_str.isnotempty(ls_cap) then ls_destinazione += ls_cap + " "
				if g_str.isnotempty(ls_localita) then ls_destinazione += ls_localita + " "
				if g_str.isnotempty(ls_provincia) then ls_destinazione += "("+ls_provincia + ")"
				ls_destinazione += "~r~n"
				if g_str.isnotempty(ls_cod_nazione) then ls_destinazione += f_des_tabella("tab_nazioni", "cod_nazione='" +  ls_cod_nazione +"'", "des_nazione")
				
				//a dx (come CLIENTE) i dati del cliente 
				dw_report_fat_ven.object.titolo_intestazione_t.text = "CLIENTE: "
				
				ls_intestazione = "                    "+ ls_cod_cliente + "~r~n" + ls_rag_soc_1_cli
				if g_str.isnotempty(ls_rag_soc_2_cli) then ls_intestazione += " " + ls_rag_soc_2_cli
				ls_intestazione += "~r~n"
				if g_str.isnotempty(ls_indirizzo_cli) then ls_intestazione += ls_indirizzo_cli + "~r~n"
				if g_str.isnotempty(ls_cap_cli) then ls_intestazione += ls_cap_cli + " "
				if g_str.isnotempty(ls_localita_cli) then ls_intestazione += ls_localita_cli + " "
				if g_str.isnotempty(ls_provincia_cli) then ls_intestazione += "(" +ls_provincia_cli + ")"
				ls_intestazione += "~r~n"
				if g_str.isnotempty(ls_partita_iva) then ls_intestazione += "P.I.: " + ls_partita_iva + "   "
				if g_str.isnotempty(ls_cod_fiscale) then ls_intestazione += "C.F.: " + ls_cod_fiscale
				
				
			case "fattura"
				//a sx (come CLIENTE) vanno i dati del cliente
				dw_report_fat_ven.object.titolo_destinazione_t.text = "CLIENTE: "
				
				ls_destinazione = "                    " + ls_cod_cliente + "~r~n" + ls_rag_soc_1_cli
				if g_str.isnotempty(ls_rag_soc_2_cli) then ls_destinazione += " " + ls_rag_soc_2_cli
				ls_destinazione += "~r~n"
				if g_str.isnotempty(ls_indirizzo_cli) then ls_destinazione += ls_indirizzo_cli + "~r~n"
				if g_str.isnotempty(ls_cap_cli) then ls_destinazione += ls_cap_cli + " "
				if g_str.isnotempty(ls_localita_cli) then ls_destinazione += ls_localita_cli + " "
				if g_str.isnotempty(ls_provincia_cli) then ls_destinazione += "("+ls_provincia_cli + ")"
				ls_destinazione += "~r~n"
				if g_str.isnotempty(ls_partita_iva) then ls_destinazione += "P.I.: " + ls_partita_iva + "   "
				if g_str.isnotempty(ls_cod_fiscale) then ls_destinazione += "C.F.: " + ls_cod_fiscale
				
				//a dx (come SPETTABILE) i dati della destinazione fattura
				dw_report_fat_ven.object.titolo_intestazione_t.text = "SPETTABILE"
				
				ls_intestazione = "~r~n" + ls_rag_soc_1_dest_fat
				if g_str.isnotempty(ls_rag_soc_2_dest_fat) then ls_intestazione += " " + ls_rag_soc_2_dest_fat
				ls_intestazione += "~r~n"
				if g_str.isnotempty(ls_indirizzo_dest_fat) then ls_intestazione += ls_indirizzo_dest_fat + "~r~n"
				if g_str.isnotempty(ls_cap_dest_fat) then ls_intestazione += ls_cap_dest_fat + " "
				if g_str.isnotempty(ls_localita_dest_fat) then ls_intestazione += ls_localita_dest_fat + " "
				if g_str.isnotempty(ls_provincia_dest_fat) then ls_intestazione += "("+ls_provincia_dest_fat + ")"
				ls_intestazione += "~r~n"
				if g_str.isnotempty(ls_cod_nazione) then ls_intestazione += f_des_tabella("tab_nazioni", "cod_nazione='" +  ls_cod_nazione +"'", "des_nazione")

		end choose
		
		ls_destinazione = g_str.replace(ls_destinazione, "'", "~'")
		ls_destinazione = g_str.replace(ls_destinazione, '"', "~'")
		
		ls_intestazione = g_str.replace(ls_intestazione, "'", "~'")
		ls_intestazione = g_str.replace(ls_intestazione, '"', "~'")
		
		try
			dw_report_fat_ven.object.destinazione_t.text = ls_destinazione		//a sx
		catch (RuntimeError err_mast2)
		end try
		
		try
			dw_report_fat_ven.object.intestazione_t.text = ls_intestazione			//a dx
		catch (RuntimeError err_mast3)
		end try
		
	catch (RuntimeError err_mast)
	end try
	
end if
//##################################################################
luo_stampa_dati_prodotto = create uo_stampa_dati_prodotto


do while 0 = 0
	if not(lb_prima_riga) and not(lb_flag_prodotto_cliente) and not(lb_flag_nota_dettaglio) then
		fetch cu_dettagli into :ll_anno_registrazione_ord_ven,
									  :ll_num_registrazione_ord_ven,
									  :ll_prog_riga_ord_ven,
									  :ls_cod_tipo_det_ven, 
									  :ls_cod_misura, 
									  :ld_quan_fatturata, 
									  :ld_prezzo_vendita,   
									  :ld_sconto_1, 
									  :ld_sconto_2, 
									  :ld_sconto_3, 
									  :ld_sconto_4, 
									  :ld_sconto_5, 
									  :ld_sconto_6, 
									  :ld_sconto_7, 
									  :ld_sconto_8, 
									  :ld_sconto_9, 
									  :ld_sconto_10, 
									  :ls_nota_dettaglio, 
									  :ls_cod_prodotto, 
									  :ls_des_prodotto,
									  :ld_fat_conversione_ven,
									  :ls_cod_iva,
								  	  :ls_flag_st_note_det,
									  :ld_quantita_um,
									  :ld_prezzo_um,
									  :ld_imponibile_riga,
									  :ll_anno_registrazione_bol_ven,
									  :ll_num_registrazione_bol_ven,
									  :ll_prog_riga_bol_ven;

		if sqlca.sqlcode <> 0 then
			if not isnull(ls_nota_piede) and len(trim(ls_nota_piede)) > 0 then
				lb_flag_nota_piede = true
			else
				exit
			end if
		end if
		
		setnull(ls_cod_misura_mag)
		//////////////////////////////// Calcolo Valore Riga PT /////////////////////////////////////////
		
		luo_doc = create uo_calcola_documento_euro
		
		ld_imponibile_riga = ld_quan_fatturata * ld_prezzo_vendita
		luo_doc.uof_arrotonda(ld_imponibile_riga,ld_precisione,"euro")
		
		ld_valore_parziale = ld_imponibile_riga
		
		ld_sconto_tot = 0
		
		if ld_sconto_1 <> 0 and not isnull(ld_sconto_1) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_1)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_1)
		end if
		
		if ld_sconto_2 <> 0 and not isnull(ld_sconto_2) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_2)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_2)
		end if
		
		if ld_sconto_3 <> 0 and not isnull(ld_sconto_3) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_3)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_3)
		end if
		
		if ld_sconto_4 <> 0 and not isnull(ld_sconto_4) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_4)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_4)
		end if
		
		if ld_sconto_5 <> 0 and not isnull(ld_sconto_5) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_5)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_5)
		end if
		
		if ld_sconto_6 <> 0 and not isnull(ld_sconto_6) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_6)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_6)
		end if
		
		if ld_sconto_7 <> 0 and not isnull(ld_sconto_7) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_7)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_7)
		end if
		
		if ld_sconto_8 <> 0 and not isnull(ld_sconto_8) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_8)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_8)
		end if
		
		if ld_sconto_9 <> 0 and not isnull(ld_sconto_9) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_9)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_9)
		end if
		
		if ld_sconto_10 <> 0 and not isnull(ld_sconto_10) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_10)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_10)
		end if
		
		if ld_sconto_tes <> 0 and not isnull(ld_sconto_tes) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_tes)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_tes)
		end if
		
		luo_doc.uof_arrotonda(ld_sconto_tot,ld_precisione,"euro")
		
		ld_imponibile_riga = ld_imponibile_riga - ld_sconto_tot
		
		destroy luo_doc
		
		/////////////////////////////////////////////////////////////////////////////////////////////////
		
//------------------------------------------------- Modifica Nicola ---------------------------------------------
		if ls_flag_st_note_det = 'I' then //nota dettaglio
			select flag_st_note_ft
			  into :ls_flag_st_note_det
			  from tab_tipi_det_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		end if		
		
		if ls_flag_st_note_det = 'N' then
			ls_nota_dettaglio = ""
		end if				
//-------------------------------------------------- Fine Modifica ----------------------------------------------				
		
	end if
   	
	select flag_stampa_fattura,
	       	des_tipo_det_ven
	into   	:ls_flag_stampa_fattura,
	       	:ls_des_tipo_det_ven
	from   tab_tipi_det_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if sqlca.sqlcode <> 0 then
		setnull(ls_flag_stampa_fattura)
		setnull(ls_des_tipo_det_ven)
	end if
	
	//Donato 15/12/2011
	//commentato perchè mandava in loop la costruzione del report
	//quando arrivava alla fine delle righe, quindi sqlca.sqlcode=100 e nota_piede non vuota
	//impostava la variabile lb_flag_nota_piede a TRUE per permettere l'inserimento di una nuova riga con questo contenuto nel report
	//ma quando arrivava a questa istruzione il "continue" lo faceva tornare su e cosi via all'infinito
	//per questo ho aggiunto anche la condizione "and not lb_flag_nota_piede"
	
	//if ls_flag_stampa_fattura<>"S" then continue
	
	//se non devi stampare la riga e se non sei alla fine (abilitato nota piede con nota piede non vuota)
	if ls_flag_stampa_fattura<>"S" and not lb_flag_nota_piede then continue
	
	//fine modifica ------------------------------------------------
	
		
	ll_riga = dw_report_fat_ven.insertrow(0)
	dw_report_fat_ven.setrow(ll_riga)
	
	dw_report_fat_ven.setitem(ll_riga, "formato", ls_formato)

	/*
	select tab_tipi_det_ven.flag_stampa_fattura,
	       tab_tipi_det_ven.des_tipo_det_ven
	into   :ls_flag_stampa_fattura,
	       :ls_des_tipo_det_ven
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if sqlca.sqlcode <> 0 then
		setnull(ls_flag_stampa_fattura)
		setnull(ls_des_tipo_det_ven)
	end if
	*/

	if lb_prima_riga then
		dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_des_prodotto", ls_nota_testata)
	elseif lb_flag_prodotto_cliente then
		dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_cod_prodotto", "Vs Codice")
		dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_des_prodotto", ls_cod_prod_cliente)
		lb_flag_prodotto_cliente = false
	elseif lb_flag_nota_dettaglio then
		dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_des_prodotto", ls_nota_dettaglio)
		lb_flag_nota_dettaglio = false		
	elseif lb_flag_nota_piede then
		dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_des_prodotto", ls_nota_piede)
	else
		
		if ls_flag_stampa_fattura = 'S' then
			select des_prodotto, 
					cod_misura_mag, 
					cod_nomenclatura
			into   	:ls_des_prodotto_anag, 
					:ls_cod_misura_mag, 
					:ls_cod_nomenclatura
			from   anag_prodotti  
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_anag)
				setnull(ls_cod_misura_mag)
				setnull(ls_cod_nomenclatura)
			end if
			

			select des_prodotto  
			into   :ls_des_prodotto_lingua  
			from   anag_prodotti_lingue  
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto and
					 cod_lingua = :ls_cod_lingua;
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_lingua)
			end if
				

			select tab_prod_clienti.cod_prod_cliente  
			into   :ls_cod_prod_cliente  
			from   tab_prod_clienti  
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto and   
					 cod_cliente = :ls_cod_cliente;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_cod_prod_cliente)
			end if
			
			select tab_ive.aliquota
			into   :ld_perc_iva
			from   tab_ive
			where  tab_ive.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_ive.cod_iva = :ls_cod_iva;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_iva)
			else
				ls_des_iva = string(ld_perc_iva,"###")
			end if
			
			select flag_tipo_det_ven
			into   :ls_flag_tipo_det
			from   tab_tipi_det_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
					 
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore in controllo tipo dettaglio.~nErrore nella select di tab_tipi_det_ven: " + sqlca.sqlerrtext)
				setnull(ls_flag_tipo_det)
			elseif sqlca.sqlcode = 100 then
				g_mb.messagebox("APICE","Errore in controllo tipo dettaglio.~nErrore nella select di tab_tipi_det_ven: tipo dettaglio non trovato")
				setnull(ls_flag_tipo_det)
			end if
			
			
			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_cod_misura", ls_cod_misura)
			
			// *** aggiunti  per estero
//			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_quantita_um", ld_quantita_um)
			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_quantita_um", ld_quan_fatturata)
//			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_prezzo_um", ld_prezzo_um)
			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_prezzo_um", ld_prezzo_vendita)
			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_quan_ordine", ld_quan_fatturata)
			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_prezzo_vendita", ld_prezzo_vendita)
			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_cod_misura_um", ls_cod_misura_mag)
			// **** fine estero	
			
			if not isnull(ls_cod_misura_mag) and ls_cod_misura_mag <> ls_cod_misura then
				dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_quan_ordine", ld_quantita_um)
				if ls_flag_tipo_det = 'S' then
					dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_prezzo_vendita", ld_prezzo_um * -1)
				else			
					dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_prezzo_vendita", ld_prezzo_um)
				end if				
			else
				dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_quan_ordine", ld_quan_fatturata)
				if ls_flag_tipo_det = 'S' then
					dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_prezzo_vendita", ld_prezzo_vendita * -1)
				else			
					dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_prezzo_vendita", ld_prezzo_vendita)
				end if				
			end if
			
			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_val_riga", ld_imponibile_riga)
			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_sconto_1", ld_sconto_1)
			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_sconto_2", ld_sconto_2)
			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_sconto_tot", ld_sconto_tot)			
			//dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_cod_iva", ls_des_iva)
			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_cod_iva", ls_cod_iva)
			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_nota_dettaglio", ls_nota_dettaglio)
			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_cod_prodotto", ls_cod_prodotto)
	
			// Normale descrizione di riga
			if not isnull(ls_des_prodotto) then
				ls_descrizione_riga = ls_des_prodotto
			elseif not isnull(ls_des_prodotto_anag) then
				ls_descrizione_riga = ls_des_prodotto_anag
			else		
				ls_descrizione_riga = ls_des_tipo_det_ven
			end if
			
			// inizio la predisposizione della descrizione in lingua se il cliente ha un codice lingua associato
			ls_descrizione_riga_lingua = ""
			if not isnull(ls_cod_lingua)  then
				if not isnull(ls_des_prodotto_lingua) then
					ls_descrizione_riga_lingua = ls_des_prodotto_lingua
				elseif not isnull(ls_des_prodotto) then
					//ls_descrizione_riga_lingua = ls_des_prodotto
				elseif not isnull(ls_des_prodotto_anag) then
					//ls_descrizione_riga_lingua = ls_des_prodotto_anag
				else		
					//ls_descrizione_riga_lingua = ls_des_tipo_det_ven
				end if
			end if
			
			
			// EnMe 01-08-2017 nuova gestione nota dettaglio prodotto
			if not isnull(ls_cod_prodotto) then
				if guo_functions.uof_get_note_fisse(ls_null, ls_null, ls_cod_prodotto, "STAMPA_FAT_VEN", ls_null, ldt_data_registrazione, ref ls_stampa_nota_testata_prod, ref ls_stampa_nota_piede_prod, ref ls_stampa_nota_video_prod) < 0 then
					g_mb.error(ls_stampa_nota_testata)
				else
					if len(ls_stampa_nota_testata_prod) > 0 then ls_nota_dettaglio = g_str.implode({ls_nota_dettaglio,ls_stampa_nota_testata_prod},"~r~n")
					if len(ls_stampa_nota_piede_prod) > 0 then ls_nota_dettaglio = g_str.implode({ls_nota_dettaglio,ls_stampa_nota_piede_prod},"~r~n")
					if len(ls_stampa_nota_video_prod) > 0 then g_mb.warning( "NOTA FISSA", ls_stampa_nota_video_prod)
				end if
			end if	
			
			if not isnull(ls_cod_prod_cliente) and len(trim(ls_cod_prod_cliente)) > 0 then
				lb_flag_prodotto_cliente = true
			end if
			if not isnull(ls_nota_dettaglio) and len(trim(ls_nota_dettaglio)) > 0 then
				lb_flag_nota_dettaglio = true
			end if
			
			// Dettagli complementari
			if ll_anno_registrazione_ord_ven > 0 and not isnull(ll_anno_registrazione_ord_ven) then
				
				ls_variabili = ""
				luo_stampa_dati_prodotto.uof_stampa_variabili( ls_cod_prodotto, "ORD_VEN", "FAT_VEN",  ls_cod_tipo_fat_ven, ll_anno_registrazione_ord_ven, ll_num_registrazione_ord_ven, ll_prog_riga_ord_ven, ls_variabili)
				if len(ls_variabili) > 0 then
					ls_descrizione_riga= ls_des_prodotto + "~r~n" + ls_variabili
				end if

				// EnMe 10/10/2017 Richiesto da Lupo con specifica NOMENCLATURA_UE del 28-08-2017
				// Se presente codice nomenclatura lo aggiungo alla descrizione del prodotto
				if lb_nomenclatura and not isnull(ls_cod_nomenclatura) then
					ls_descrizione_riga += "~r~n" + "HS.CODENUMBER:" + ls_cod_nomenclatura
				end if

				select cod_prod_finito, dim_x, dim_y, dim_z, dim_t, cod_non_a_magazzino, cod_verniciatura, cod_tessuto, flag_tipo_mantovana, colore_passamaneria
				into   :ls_cod_prodotto_finito, :ld_dim_x, :ld_dim_y, :ld_dim_z, :ld_dim_t, :ls_cod_non_a_magazzino, :ls_cod_verniciatura, :ls_cod_tessuto, :ls_flag_tipo_mantovana, :ls_colore_passamaneria
				from   comp_det_ord_ven  
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione_ord_ven and
						 num_registrazione = :ll_num_registrazione_ord_ven and  
						 prog_riga_ord_ven = :ll_prog_riga_ord_ven ;
				if sqlca.sqlcode = 0 then	
				end if
			else 
				// non c'è un ordine collegato
				
				// EnMe 10/10/2017 Richiesto da Lupo con specifica NOMENCLATURA_UE del 28-08-2017
				// Se presente codice nomenclatura lo aggiungo alla descrizione del prodotto
				if lb_nomenclatura and not isnull(ls_cod_nomenclatura) then
					ls_descrizione_riga += "~r~n" + "HS.CODENUMBER:" + ls_cod_nomenclatura
				end if
				
				if not isnull(ll_anno_registrazione_bol_ven) and ll_anno_registrazione_bol_ven > 0 then
					if  ls_flag_tipo_lista_bolle =  "X" then		// lista e dettaglio analitico con dimensioni
						select dim_x, dim_y
						into 	:ld_dim_x_riga, :ld_dim_y_riga
						from	det_bol_ven
						where cod_azienda = :s_cs_xx.cod_azienda and
								anno_registrazione = :ll_anno_registrazione_bol_ven and
								num_registrazione = :ll_num_registrazione_bol_ven and
								prog_riga_bol_ven = :ll_prog_riga_bol_ven;
						if sqlca.sqlcode < 0 then
							g_mb.messagebox("APICE","Errore in ricerca dimensioni prodotto nell DDT" + sqlca.sqlerrtext)
						end if
					
						if ld_dim_x_riga > 0 then
							dw_report_fat_ven.setitem(ll_riga,"comp_det_dim_x", ld_dim_x_riga)
						end if
						
						if ld_dim_y_riga > 0 then
							dw_report_fat_ven.setitem(ll_riga,"comp_det_dim_y", ld_dim_y_riga)
						end if	
					end if 
				end if			
			end if
			
			// stefanop: 05/08/2014: risolto problema del doppio codice
			if not isnull(ls_cod_lingua) and not isnull(ls_descrizione_riga_lingua) and ls_cod_lingua <> "ITA" then ls_descrizione_riga += "~r~n" + ls_descrizione_riga_lingua
			dw_report_fat_ven.setitem(ll_riga, "det_fat_ven_des_prodotto", ls_descrizione_riga)

		end if
	end if	
	
	dw_report_fat_ven.setitem(ll_riga, "parametri_azienda_stringa", ls_stringa)
	dw_report_fat_ven.setitem(ll_riga, "parametri_azienda_stringa_euro", ls_stringa_euro)
	if isnull(ls_cod_documento) then
		dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_cod_documento", ls_cod_documento)
//		dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_anno_registrazione", il_anno_registrazione)
//		dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_num_registrazione", il_num_registrazione)
	else
		dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_cod_documento", ls_cod_documento)
		dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_numeratore_documento", ls_numeratore_doc)
		dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_anno_documento", ll_anno_documento)
		dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_num_documento", ll_num_documento)
		dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_data_fattura", ldt_data_fattura)
		dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_num_registro_iva_impresa", ll_numero_registro)
		
	end if
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_anno_registrazione", il_anno_registrazione)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_num_registrazione", il_num_registrazione)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_cod_tipo_fat_ven", ls_cod_tipo_fat_ven)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_data_registrazione", ldt_data_registrazione)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_cod_cliente", ls_cod_cliente)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_cod_valuta", ls_cod_valuta)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_cod_pagamento", ls_cod_pagamento)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_sconto", ld_sconto_pagamento)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_cod_banca_clien_for", ls_cod_banca_clien_for)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_num_ord_cliente", ls_num_ord_cliente)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_data_ord_cliente", ldt_data_ord_cliente)
	
	// stefanop: 10/12/2011: deposito
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_des_deposito", ls_des_deposito)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_provincia_dep", ls_provincia_dep)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_cap_dep", ls_cap_dep)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_localita_dep", ls_localita_dep)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_indirizzo_dep", ls_indirizzo_dep)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_telefono_dep", ls_telefono_dep)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_fax_dep", ls_fax_dep)
	// ---
	
	// --- nazione: richiesto da Gibus 11-04-2012
	dw_report_fat_ven.setitem(ll_riga, "anag_clienti_cod_nazione", ls_cod_nazione)
	dw_report_fat_ven.setitem(ll_riga, "tab_nazioni_des_nazione", ls_des_nazione)
	
	//---------------------------------- Modifica Nicola ---------------------------------------

	if (isnull(ls_rag_soc_1_dest_fat) or len(ls_rag_soc_1_dest_fat) < 1) and ls_flag_tipo_fat_ven = "D" then
		ls_rag_soc_1 = ls_rag_soc_1_cli
		ls_rag_soc_2 = ls_rag_soc_2_cli
		ls_indirizzo = ls_indirizzo_cli
		ls_cap = ls_cap_cli
		ls_localita = ls_localita_cli
		ls_provincia = ls_provincia_cli	
	end if		
	
	if (not isnull(ls_rag_soc_1_dest_fat) or len(ls_rag_soc_1_dest_fat) > 0) and ls_flag_tipo_fat_ven <> "I" then
		
		ls_rag_soc_1 = ls_rag_soc_1_dest_fat
		ls_rag_soc_2 = ls_rag_soc_2_dest_fat
		ls_indirizzo = ls_indirizzo_dest_fat
		ls_cap = ls_cap_dest_fat
		ls_localita = ls_localita_dest_fat
		ls_provincia = ls_provincia_dest_fat
	end if 	
	
	if (isnull(ls_rag_soc_1) or len(ls_rag_soc_1) < 1) and &
		(isnull(ls_rag_soc_1_dest_fat) or len(ls_rag_soc_1_dest_fat) < 1) then
		
		ls_rag_soc_1 = ls_rag_soc_1_cli
		ls_rag_soc_2 = ls_rag_soc_2_cli
		ls_indirizzo = ls_indirizzo_cli
		ls_cap = ls_cap_cli
		ls_localita = ls_localita_cli
		ls_provincia = ls_provincia_cli	
	end if	
	
//----------------------------------- Fine Modifica ----------------------------------------	
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_rag_soc_1", ls_rag_soc_1)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_rag_soc_2", ls_rag_soc_2)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_indirizzo", ls_indirizzo)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_localita", ls_localita)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_frazione", ls_frazione)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_cap", ls_cap)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_provincia", ls_provincia)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_nota_testata", ls_nota_testata)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_nota_piede", ls_nota_piede)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_cod_porto", ls_cod_porto)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_cod_resa", ls_cod_resa)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_cod_agente_1", ls_cod_agente_1)
	dw_report_fat_ven.setitem(ll_riga, "anag_agenti_rag_soc_1", ls_anag_agenti_rag_soc_1)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_cod_agente_2", ls_cod_agente_2)
	dw_report_fat_ven.setitem(ll_riga, "anag_agenti_rag_soc_2", ls_anag_agenti_rag_soc_2)
	
	select flag_tipo_fat_ven
	into   :ls_flag_tipo_fat
	from   tab_tipi_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in controllo tipo fattura.~nErrore nella select di tab_tipi_fat_ven: " + sqlca.sqlerrtext)
		setnull(ls_flag_tipo_fat)
	elseif sqlca.sqlcode = 100 then
		g_mb.messagebox("APICE","Errore in controllo tipo fattura.~nErrore nella select di tab_tipi_fat_ven: tipo fattura non trovato")
		setnull(ls_flag_tipo_fat)
	end if
	
	if ls_flag_tipo_fat = 'N' then
		dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_tot_fattura_euro", ld_tot_fattura_euro * -1)
		dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_tot_fattura_valuta", ld_tot_fattura * -1)
	else
		dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_tot_fattura_euro", ld_tot_fattura_euro)
		dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_tot_fattura_valuta", ld_tot_fattura)
	end if
	
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_imponibile_iva_valuta", 0)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_iva_valuta", 0)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_imponibile_iva", 0)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_iva", 0)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_cod_vettore", ls_cod_vettore)
	
	// -- stefanop 01/03/2010: Inserisco il numero di colli
	try
		dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_stampa_colli", ls_parametro_colli)
		dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_num_colli", ll_num_colli)
	catch (RuntimeError ex)
	end try
	// ----


//---------------------------------------- Modifica Nicola -----------------------------------------------------
	select des_causale
	  into :ls_des_causale
	  from tab_causali_trasp
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and cod_causale = :ls_causale_trasporto;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in ricerca causale trasporto: stampa interrotta", Stopsign!)
		return -1
	elseif sqlca.sqlcode = 100 then
		ls_des_causale = ""
	end if	
//------------------------------------------ Fine Modifica ----------------------------------------------------- 	
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_causale_trasporto", ls_des_causale)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_telefono", ls_telefono_cliente)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_fax", ls_fax_cliente)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_cambio_ven", ld_cambio_ven)
	dw_report_fat_ven.setitem(ll_riga, "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
	dw_report_fat_ven.setitem(ll_riga, "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
	dw_report_fat_ven.setitem(ll_riga, "anag_clienti_indirizzo", ls_indirizzo_cli)
	dw_report_fat_ven.setitem(ll_riga, "anag_clienti_localita", ls_localita_cli)
	dw_report_fat_ven.setitem(ll_riga, "anag_clienti_frazione", ls_frazione_cli)
	dw_report_fat_ven.setitem(ll_riga, "anag_clienti_cap", ls_cap_cli)
	dw_report_fat_ven.setitem(ll_riga, "anag_clienti_provincia", ls_provincia_cli)
	dw_report_fat_ven.setitem(ll_riga, "anag_clienti_partita_iva", ls_partita_iva)
	dw_report_fat_ven.setitem(ll_riga, "anag_clienti_telefono", ls_telefono_des)
	dw_report_fat_ven.setitem(ll_riga, "anag_clienti_fax", ls_fax_des)
	dw_report_fat_ven.setitem(ll_riga, "anag_clienti_cod_fiscale", ls_cod_fiscale)
	dw_report_fat_ven.setitem(ll_riga, "tab_tipi_fat_ven_des_tipo_fat_ven", ls_des_tipo_fat_ven)
	dw_report_fat_ven.setitem(ll_riga, "tab_pagamenti_des_pagamento", ls_des_pagamento)
	dw_report_fat_ven.setitem(ll_riga, "tab_pagamenti_des_estesa", ls_des_pagamento_estesa)
	dw_report_fat_ven.setitem(ll_riga, "tab_pagamenti_sconto", ld_sconto_pagamento)
	dw_report_fat_ven.setitem(ll_riga, "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
	if ls_flag_tipo_pagamento = "R" then
		dw_report_fat_ven.setitem(ll_riga, "anag_banche_des_banca", ls_null)
		dw_report_fat_ven.setitem(ll_riga, "anag_banche_clien_for_des_banca", ls_des_banca)
		dw_report_fat_ven.setitem(ll_riga, "anag_banche_clien_for_cod_abi", ls_cod_abi)
		dw_report_fat_ven.setitem(ll_riga, "anag_banche_clien_for_cod_cab", ls_cod_cab)
		dw_report_fat_ven.setitem(ll_riga, "anag_banche_clien_for_cin", ls_cod_cin)
		dw_report_fat_ven.setitem(ll_riga, "anag_banche_iban", ls_cod_iban)
	else
		dw_report_fat_ven.setitem(ll_riga, "anag_banche_clien_for_des_banca", ls_null)
		dw_report_fat_ven.setitem(ll_riga, "anag_banche_des_banca", ls_des_banca)
		dw_report_fat_ven.setitem(ll_riga, "anag_banche_cod_abi", ls_cod_abi)
		dw_report_fat_ven.setitem(ll_riga, "anag_banche_cod_cab", ls_cod_cab)
		dw_report_fat_ven.setitem(ll_riga, "anag_banche_cin", ls_cod_cin)
		dw_report_fat_ven.setitem(ll_riga, "anag_banche_iban", ls_cod_iban)
		dw_report_fat_ven.setitem(ll_riga, "anag_banche_conto_corrente", ls_conto_corrente)
		dw_report_fat_ven.setitem(ll_riga, "anag_banche_swift", ls_swift)
	end if
	dw_report_fat_ven.setitem(ll_riga, "tab_valute_des_valuta", ls_des_valuta)
	dw_report_fat_ven.setitem(ll_riga, "tab_valute_lingue_des_valuta", ls_des_valuta_lingua)
	dw_report_fat_ven.setitem(ll_riga, "tab_porti_des_porto", ls_des_porto)
	dw_report_fat_ven.setitem(ll_riga, "tab_porti_lingue_des_porto", ls_des_porto_lingua)
	dw_report_fat_ven.setitem(ll_riga, "tab_rese_des_resa", ls_des_resa)
	dw_report_fat_ven.setitem(ll_riga, "tab_rese_lingue_des_resa", ls_des_resa_lingua)
	dw_report_fat_ven.setitem(ll_riga, "anag_vettori_rag_soc_1", ls_vettore_rag_soc_1)
	dw_report_fat_ven.setitem(ll_riga, "anag_vettori_indirizzo", ls_vettore_indirizzo)
	dw_report_fat_ven.setitem(ll_riga, "anag_vettori_cap", ls_vettore_cap)
	dw_report_fat_ven.setitem(ll_riga, "anag_vettori_localita", ls_vettore_localita)
	dw_report_fat_ven.setitem(ll_riga, "anag_vettori_provincia", ls_vettore_provincia)

	dw_report_fat_ven.setitem(ll_riga, "anag_inoltro_cod_inoltro", ls_cod_inoltro)
	dw_report_fat_ven.setitem(ll_riga, "anag_inoltro_rag_soc_1", ls_inoltro_rag_soc_1)
	dw_report_fat_ven.setitem(ll_riga, "anag_inoltro_indirizzo", ls_inoltro_indirizzo)
	dw_report_fat_ven.setitem(ll_riga, "anag_inoltro_cap", ls_inoltro_cap)
	dw_report_fat_ven.setitem(ll_riga, "anag_inoltro_localita", ls_inoltro_localita)
	dw_report_fat_ven.setitem(ll_riga, "anag_inoltro_provincia", ls_inoltro_provincia)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_peso_lordo", ld_peso_lordo)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_peso_netto", ld_peso_netto)
	dw_report_fat_ven.setitem(ll_riga, "tab_mezzi_des_mezzo", ls_des_mezzo)
	dw_report_fat_ven.setitem(ll_riga, "tab_imballi_des_imballo", ls_des_imballo)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_data_inizio_trasporto", ldt_data_inizio_trasporto)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_ora_inizio_trasporto", ldt_ora_inizio_trasporto)

	//######################################################################
	//gestione importi iva
	//------------------------------------------------------------------------------------
	ll_num_aliquote = upperbound(ld_aliquote_iva) - 1
	if ll_num_aliquote > 0 then
		if ld_aliquote_iva[1] > 0 and not isnull(ld_aliquote_iva[1]) then
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_imponibile_iva_1", ld_imponibili_valuta[1])
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_importo_iva_1", ld_iva_valuta[1])
		else
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_imponibile_iva_1", ld_imponibili_valuta[1])
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_des_esenzione_1", ls_des_estesa[1])
			//dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_des_esenzione_1", ls_des_esenzione_iva[1])
		end if
		dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_cod_iva_1", ls_codici_iva[1])
	end if
	//------------------------------------------------------------------------------------
	if ll_num_aliquote > 1 then
		if ld_aliquote_iva[2] > 0 and not isnull(ld_aliquote_iva[2]) then
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_imponibile_iva_2", ld_imponibili_valuta[2])
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_importo_iva_2", ld_iva_valuta[2])
		else
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_imponibile_iva_2", ld_imponibili_valuta[2])
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_des_esenzione_2", ls_des_estesa[2])
			//dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_des_esenzione_2", ls_des_esenzione_iva[2])
		end if
		dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_cod_iva_2", ls_codici_iva[2])
	end if
	//------------------------------------------------------------------------------------
	if ll_num_aliquote > 2 then
		if ld_aliquote_iva[3] > 0 and not isnull(ld_aliquote_iva[3]) then
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_imponibile_iva_3", ld_imponibili_valuta[3])
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_importo_iva_3", ld_iva_valuta[3])
		else
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_imponibile_iva_3", ld_imponibili_valuta[3])
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_des_esenzione_3", ls_des_estesa[3])
			//dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_des_esenzione_3", ls_des_esenzione_iva[3])
		end if
		dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_cod_iva_3", ls_codici_iva[3])					
	end if
	//------------------------------------------------------------------------------------
	if ll_num_aliquote > 3 then
		if ld_aliquote_iva[4] > 0 and not isnull(ld_aliquote_iva[4]) then
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_imponibile_iva_4", ld_imponibili_valuta[4])
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_importo_iva_4", ld_iva_valuta[4])
		else
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_imponibile_iva_4", ld_imponibili_valuta[4])
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_des_esenzione_4", ls_des_estesa[4])
			//dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_des_esenzione_4", ls_des_esenzione_iva[4])
		end if
		dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_cod_iva_4", ls_codici_iva[4])
	end if
	//------------------------------------------------------------------------------------
	if ll_num_aliquote > 4 then
		if ld_aliquote_iva[5] > 0 and not isnull(ld_aliquote_iva[5]) then
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_imponibile_iva_5", ld_imponibili_valuta[5])
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_importo_iva_5", ld_iva_valuta[5])
		else
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_imponibile_iva_5", ld_imponibili_valuta[5])
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_des_esenzione_5", ls_des_estesa[5])
			//dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_des_esenzione_5", ls_des_esenzione_iva[5])
		end if
		dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_cod_iva_5", ls_codici_iva[5])
	end if
	//------------------------------------------------------------------------------------
	if ll_num_aliquote > 5 then
		if ld_aliquote_iva[6] > 0 and not isnull(ld_aliquote_iva[6]) then
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_imponibile_iva_6", ld_imponibili_valuta[6])
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_importo_iva_6", ld_iva_valuta[6])
		else
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_imponibile_iva_6", ld_imponibili_valuta[6])
			dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_des_esenzione_6", ls_des_estesa[6])
			//dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_des_esenzione_6", ls_des_esenzione_iva[6])
		end if
		dw_report_fat_ven.setitem(ll_riga, "iva_fat_ven_cod_iva_6", ls_codici_iva[6])
	end if
	//------------------------------------------------------------------------------------
	//######################################################################

	// stefanop 05/03/2010: provo a commentare questo codice che forse non serve!
	/*
	if upperbound(ldt_scadenze) > 0 then
		if date(ldt_scadenze[1]) > date("01/01/1900") and not isnull(ldt_scadenze[1]) then
			dw_report_fat_ven.setitem(ll_riga, "scad_fat_ven_scadenza_1", ldt_scadenze[1])
			dw_report_fat_ven.setitem(ll_riga, "scad_fat_ven_rata_1", ld_rate_scadenze[1])
		end if
	end if
	if upperbound(ldt_scadenze) > 1 then
		if date(ldt_scadenze[2]) > date("01/01/1900") and not isnull(ldt_scadenze[2]) then
			dw_report_fat_ven.setitem(ll_riga, "scad_fat_ven_scadenza_2", ldt_scadenze[2])
			dw_report_fat_ven.setitem(ll_riga, "scad_fat_ven_rata_2", ld_rate_scadenze[2])
		end if
	end if
	if upperbound(ldt_scadenze) > 2 then
		if date(ldt_scadenze[3]) > date("01/01/1900") and not isnull(ldt_scadenze[3]) then
			dw_report_fat_ven.setitem(ll_riga, "scad_fat_ven_scadenza_3", ldt_scadenze[3])
			dw_report_fat_ven.setitem(ll_riga, "scad_fat_ven_rata_3", ld_rate_scadenze[3])
		end if
	end if
	if upperbound(ldt_scadenze) > 3 then
		if date(ldt_scadenze[4]) > date("01/01/1900") and not isnull(ldt_scadenze[4]) then
			dw_report_fat_ven.setitem(ll_riga, "scad_fat_ven_scadenza_4", ldt_scadenze[4])
			dw_report_fat_ven.setitem(ll_riga, "scad_fat_ven_rata_4", ld_rate_scadenze[4])
		end if
	end if
	if upperbound(ldt_scadenze) > 4 then
		if date(ldt_scadenze[5]) > date("01/01/1900") and not isnull(ldt_scadenze[5]) then
			dw_report_fat_ven.setitem(ll_riga, "scad_fat_ven_scadenza_5", ldt_scadenze[5])
			dw_report_fat_ven.setitem(ll_riga, "scad_fat_ven_rata_5", ld_rate_scadenze[5])
		end if
	end if
	if upperbound(ldt_scadenze) > 5 then
		if date(ldt_scadenze[6]) > date("01/01/1900") and not isnull(ldt_scadenze[6]) then
			dw_report_fat_ven.setitem(ll_riga, "scad_fat_ven_scadenza_6", ldt_scadenze[6])
			dw_report_fat_ven.setitem(ll_riga, "scad_fat_ven_rata_6", ld_rate_scadenze[6])
		end if
	end if
	*/
	
	dw_report_fat_ven.setitem(ll_riga, "scad_fat_ven", ls_scad_fat)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_imponibile_iva_valuta", ld_imponibile_iva_valuta)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_iva_valuta", ld_importo_iva_valuta)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_imponibile_iva", ld_imponibile_iva)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_iva", ld_importo_iva)
	lb_prima_riga = false
	if lb_flag_nota_piede then exit
loop

destroy luo_stampa_dati_prodotto
close cu_dettagli;

ld_tot_merci = ld_tot_merci - ld_tot_sconti_commerciali

if ll_riga > 0 then
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_tot_merci", ld_tot_merci)
	dw_report_fat_ven.setitem(ll_riga, "tes_fat_ven_importo_sconto", ld_tot_cassa)
end if

if not isnull(ls_cod_lingua) then
	
	ls_nome_file = s_cs_xx.volume + s_cs_xx.risorse + "condizioni_" + ls_cod_lingua + ".txt"
	
	li_FileNum = FileOpen(ls_nome_file, StreamMode!)
	if li_FileNum > 0 then
		if fileread(li_FileNum, ls_condizioni) > 0 then
			try
				dw_report_fat_ven.object.condizioni_commerciali_t.text = ls_condizioni
			catch (runtimeerror e)
			end try
		end if
		
		FileClose(li_FileNum)
	end if
end if


	
dw_report_fat_ven.reset_dw_modified(c_resetchildren)
dw_report_fat_ven.change_dw_current()


dw_report_fat_ven.object.datawindow.print.preview = 'Yes'

setredraw(true)
return 0
end function

public subroutine wf_stampa (long copie);string ls_spf, ls_error
long ll_i,ll_zoom


// il parametro ZFV determina lo zoom con cui le fatture devono essere stampate
select numero
into   :ll_zoom
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'ZFV';
		 
if sqlca.sqlcode <> 0 then ll_zoom = 100

if isnull(ll_zoom) then ll_zoom = 100


// il parametro SPF Stampa Prezzi Fattura determina se i prezzi devono essere stampati oppure no nella fattura accompagnatoria
// il parametro è una stringa del tipo "1001" dove 0=non stampa 1=stampa; la posizione del caratterere indica se stampare o no i prezzi
select stringa
into   :ls_spf
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'SPF';
		 
if sqlca.sqlcode <> 0 then ls_spf = "111111"

if isnull(ll_zoom) then ls_spf = "111111"


il_totale_pagine = 1

dw_report_fat_ven.object.datawindow.zoom = ll_zoom

// stefanop: Filigrana
ls_error = dw_report_fat_ven.modify("DataWindow.Picture.File='" + is_path_filigrana + "'")
if not isnull(ls_error) and ls_error <> "" then g_mb.error("File", ls_error)

ls_error = dw_report_fat_ven.modify("DataWindow.Picture.Mode=1")
if not isnull(ls_error) and ls_error <> "" then g_mb.error("Mode", ls_error)

ls_error = dw_report_fat_ven.modify("DataWindow.Picture.Transparency=80")
if not isnull(ls_error) and ls_error <> "" then g_mb.error("Transparency",ls_error)

ls_error = dw_report_fat_ven.modify("DataWindow.Print.Background	='Yes'")
if not isnull(ls_error) and ls_error <> "" then g_mb.error("Background", ls_error)
// ----

if ib_email then
	// processo di invio tramite EMAIL

	// invio la mail solo della prima copia
	if is_flag_email[il_corrente] = "S" then 
		wf_email()
	end if

else

	for il_pagina_corrente = 1 to il_totale_pagine
		
		for ll_i = 1 to copie
			/* Il cliente della fattura attuale accetta la fattura via mail,
				quindi NON invio la prima copia del destinatario              */
			if is_flag_email[il_corrente] = "S" and ll_i = 1 then continue
		
			// processo di normale stampa delle copie previste per il cliente
			
			choose case ll_i
				case 1 
					dw_report_fat_ven.object.st_copia.text = "Copia Destinatario"
				case 2
					dw_report_fat_ven.object.st_copia.text = "Copia Vettore / Raccolta Firme"
				case 3
					dw_report_fat_ven.object.st_copia.text = "Copia Mittente"
				case else  //  dalla quarta in poi.
					dw_report_fat_ven.object.st_copia.text = "Copia Interna"
			end choose
			
			ls_error = dw_report_fat_ven.modify("DataWindow.brushmode=0")
			
			// Aggiunto su richiesta di Claudia 11/01/2012: oscuramento prezzi nelle pagine dopo la prima.
			if is_flag_tipo_fat_ven = "I" then
				
				if mid(ls_spf, ll_i, 1) = "0" then
					// nascondi prezzi in fattura
					dw_report_fat_ven.object.det_fat_ven_cod_iva.visible = 0
					dw_report_fat_ven.object.det_fat_ven_val_riga.visible = 0
					dw_report_fat_ven.object.det_fat_ven_sconto_2.visible = 0
					dw_report_fat_ven.object.det_fat_ven_sconto_1.visible = 0
					dw_report_fat_ven.object.compute_4.visible = 0
					dw_report_fat_ven.object.scad_fat_ven.visible = 0
					dw_report_fat_ven.object.tes_fat_ven_sconto.visible = 0
					dw_report_fat_ven.object.tes_fat_ven_importo_sconto.visible = 0
					dw_report_fat_ven.object.tes_fat_ven_tot_fattura_euro.visible = 0
					dw_report_fat_ven.object.tes_fat_ven_tot_fattura_valuta.visible = 0
					dw_report_fat_ven.object.tes_fat_ven_imponibile_iva_valuta.visible = 0
					dw_report_fat_ven.object.tes_fat_ven_iva_valuta.visible = 0
					
					dw_report_fat_ven.object.iva_fat_ven_cod_iva_1.visible	= 0
					dw_report_fat_ven.object.iva_fat_ven_cod_iva_2.visible	= 0
					try
						dw_report_fat_ven.object.iva_fat_ven_cod_iva_3.visible	= 0
						dw_report_fat_ven.object.iva_fat_ven_cod_iva_4.visible	= 0
					catch (runtimeerror err)
					end try
					
					dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_1.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_importo_iva_1.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_des_esenzione_1.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_2.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_importo_iva_2.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_des_esenzione_2.visible = 0
					try
						dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_3.visible = 0
						dw_report_fat_ven.object.iva_fat_ven_importo_iva_3.visible = 0
						dw_report_fat_ven.object.iva_fat_ven_des_esenzione_3.visible = 0
						dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_4.visible = 0
						dw_report_fat_ven.object.iva_fat_ven_importo_iva_4.visible = 0
						dw_report_fat_ven.object.iva_fat_ven_des_esenzione_4.visible = 0
					catch (runtimeerror err2)
					end try

					
					// Visualizzo Filigrana
					ls_error = dw_report_fat_ven.modify("DataWindow.brushmode=6")
					if not isnull(ls_error) and ls_error <> "" then g_mb.error("Brushmode", ls_error)
					
				else
					dw_report_fat_ven.object.det_fat_ven_cod_iva.visible 					= "0~tif((det_fat_ven_val_riga <> 0), 1, 0)"
					dw_report_fat_ven.object.det_fat_ven_val_riga.visible 					= "0~tif ( det_fat_ven_val_riga <> 0 , 1 , 0 )"
					
					try
						dw_report_fat_ven.object.det_fat_ven_sconto_2.visible 					= 1
						dw_report_fat_ven.object.det_fat_ven_sconto_1.visible 					= 1
					catch (runtimeerror err_sconto)
					end try
					
					try
						dw_report_fat_ven.object.compute_4.visible 								= "0~tif( det_fat_ven_prezzo_vendita <> 0,1,0)"
					catch (runtimeerror err_compute_4)
					end try
					
					try
						dw_report_fat_ven.object.scad_fat_ven.visible 							= "0~tif(len( scad_fat_ven ) > 1 AND  page() = pagecount(), 1, 0)"
						dw_report_fat_ven.object.tes_fat_ven_sconto.visible 					= "0~tif (tes_fat_ven_sconto > 0, 1, 0)"
						dw_report_fat_ven.object.tes_fat_ven_importo_sconto.visible 			= "0~tif (tes_fat_ven_importo_sconto > 0, 1, 0)"
						dw_report_fat_ven.object.tes_fat_ven_tot_fattura_euro.visible 		= "0~tif(page() = pagecount(),1,0)"
						dw_report_fat_ven.object.tes_fat_ven_tot_fattura_valuta.visible		= "0~tif(parametri_azienda_stringa_euro <>  tes_fat_ven_cod_valuta and tes_fat_ven_tot_fattura_valuta <> 0 and page() = pagecount(),1,0)"
						dw_report_fat_ven.object.tes_fat_ven_imponibile_iva_valuta.visible 	= "0~tif(tes_fat_ven_tot_fattura_valuta <> 0 and page() = pagecount(),1,0)"
						dw_report_fat_ven.object.tes_fat_ven_iva_valuta.visible 				= "0~tif(tes_fat_ven_tot_fattura_valuta <> 0 and page() = pagecount(),1,0)"
					catch (runtimeerror err_a)
					end try
					
					try
						dw_report_fat_ven.object.iva_fat_ven_cod_iva_1.visible 				= "0~tif(page() = pagecount(),1,0)"
						dw_report_fat_ven.object.iva_fat_ven_cod_iva_2.visible 				= "0~tif(page() = pagecount(),1,0)"
					catch (runtimeerror err_cod_iva_12)
					end try
					
					try
						dw_report_fat_ven.object.iva_fat_ven_cod_iva_3.visible 				= "0~tif(page() = pagecount(),1,0)"
						dw_report_fat_ven.object.iva_fat_ven_cod_iva_4.visible 				= "0~tif(page() = pagecount(),1,0)"
					catch (runtimeerror err_cod_iva_34)
					end try
					
					try
						dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_1.visible 		= "0~tif(iva_fat_ven_imponibile_iva_1 <> 0 and page() = pagecount(),1,0)"
						dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_2.visible 		= "0~tif(iva_fat_ven_imponibile_iva_2 <> 0 and page() = pagecount(),1,0)"
						dw_report_fat_ven.object.iva_fat_ven_importo_iva_1.visible			= "0~tif(iva_fat_ven_importo_iva_1 <> 0 and page() = pagecount(),1,0)"
						dw_report_fat_ven.object.iva_fat_ven_importo_iva_2.visible 			= "0~tif(iva_fat_ven_importo_iva_2 <> 0 and page() = pagecount(),1,0)"
						dw_report_fat_ven.object.iva_fat_ven_des_esenzione_1.visible 		= "0~tif((isnull(iva_fat_ven_importo_iva_1) or iva_fat_ven_importo_iva_1 = 0) and page() = pagecount(),1,0)"
						dw_report_fat_ven.object.iva_fat_ven_des_esenzione_2.visible 		= "0~tif((isnull(iva_fat_ven_importo_iva_2) or iva_fat_ven_importo_iva_2 = 0) and page() = pagecount(),1,0)"
					catch (runtimeerror err_b)
					end try
					
					try
						dw_report_fat_ven.object.iva_fat_ven_importo_iva_3.visible 			= "0~tif(iva_fat_ven_importo_iva_3 <> 0 and page() = pagecount(),1,0)"
						dw_report_fat_ven.object.iva_fat_ven_importo_iva_4.visible 			= "0~tif(iva_fat_ven_importo_iva_4 <> 0 and page() = pagecount(),1,0)"
						dw_report_fat_ven.object.iva_fat_ven_des_esenzione_3.visible 		= "0~tif((isnull(iva_fat_ven_importo_iva_3) or iva_fat_ven_importo_iva_3 = 0) and page() = pagecount(),1,0)"
						dw_report_fat_ven.object.iva_fat_ven_des_esenzione_4.visible 		= "0~tif((isnull(iva_fat_ven_importo_iva_4) or iva_fat_ven_importo_iva_4 = 0) and page() = pagecount(),1,0)"
						dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_3.visible 		= "0~tif(iva_fat_ven_imponibile_iva_3 <> 0 and page() = pagecount(),1,0)"
						dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_4.visible 		= "0~tif(iva_fat_ven_imponibile_iva_4 <> 0 and page() = pagecount(),1,0)"
					catch (runtimeerror err4)
					end try
				end if		
				
			end if
			
			dw_report_fat_ven.triggerevent("pcd_print")
				
		next
		
		this.postevent("ue_avviso_spedizione")
	
	next
	
end if

dw_report_fat_ven.object.st_copia.text = ""

end subroutine

public function integer wf_email ();// funzione che provvedere all'invio MAIL della fattura corrente

integer 	li_ret
long    	ll_anno_documento, ll_num_documento, ll_prog_mimytype
string 	ls_path, ls_messaggio, ls_destinatari[], ls_allegati[], ls_subject, ls_message, &
			ls_rag_soc_1,ls_cod_cliente, ls_nota, ls_azienda, ls_cod_lingua, ls_cc[]
datetime ldt_data_fattura
uo_outlook      luo_outlook
uo_archivia_pdf luo_pdf

luo_pdf = CREATE uo_archivia_pdf
luo_outlook = CREATE uo_outlook

if ib_nr_nr then
	luo_outlook.ib_silent_mode = true
end if

ls_path = luo_pdf.uof_crea_pdf_path(dw_report_fat_ven)

if ls_path = "errore" then
	g_mb.messagebox ("Errore","Errore nella creazione del file pdf.")
	return -1
end if

ls_destinatari[1] = is_email_amministrazione[il_corrente]
ls_allegati[1]    = ls_path

if upperbound(is_email_utente) > 0 then
	if not isnull(is_email_utente[il_corrente]) then ls_cc[1] = is_email_utente[il_corrente]
end if

select anno_documento,
       num_documento,
		 data_fattura, 
		 cod_cliente
into   :ll_anno_documento,
		 :ll_num_documento,
		 :ldt_data_fattura,
	    :ls_cod_cliente
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and	
       anno_registrazione = :il_anno[il_corrente] and
		 num_registrazione = :il_num[il_corrente];

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca dati fattura (wf_email)~r~n" + sqlca.sqlerrtext)
end if


select rag_soc_1,
		cod_lingua
into   :ls_rag_soc_1,
		:ls_cod_lingua
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca dati cliente (wf_email)~r~n" + sqlca.sqlerrtext)
end if

select rag_soc_1
into :ls_azienda
from aziende
where cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ls_azienda) then ls_azienda=""

ls_subject = ls_azienda+" - Invio Fattura nr. " + string(ll_anno_documento) + "/" + string(ll_num_documento) + " del " + string(ldt_data_fattura,"dd/mm/yyyy")


wf_testo_mail(ls_rag_soc_1, ll_anno_documento, ll_num_documento, ldt_data_fattura, ls_cod_lingua, ref ls_message)

luo_outlook.ib_html = false

if not luo_outlook.uof_invio_sendmail( ls_destinatari[], ls_cc[], ls_subject, ls_message, ls_allegati[], ref ls_messaggio) then
	g_mb.messagebox("APICE", "Errore in fase di invio fattura~r~n" + ls_messaggio)
else
	// tutto bene; memorizzo pure in tes_fat_ven l'invio
	update tes_fat_ven
	set    flag_email_inviata = 'S'
	where  cod_azienda = :s_cs_xx.cod_azienda and	
			 anno_registrazione = :il_anno[il_corrente] and
			 num_registrazione = :il_num[il_corrente];
	
	wf_memorizza_blob(il_anno[il_corrente], il_num[il_corrente], ls_path)
	
	commit;

	if not ib_email then
		g_mb.success("Mail inviata con successo")
	end if
	
end if

	
destroy luo_outlook

filedelete(ls_path)

return 0
end function

public function integer wf_memorizza_blob (long fl_anno_registrazione, long fl_num_registrazione, string fs_path);long ll_prog_mimytype,ll_ret
blob l_blob
string ls_note, ls_nota, ls_cod_nota

ll_ret = f_file_to_blob(fs_path, l_blob)
if ll_ret < 0 then return -1

ll_ret = len(l_blob)


SELECT prog_mimetype  
INTO   :ll_prog_mimytype  
FROM   tab_mimetype  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and
		 estensione = 'PDF'  OR estensione = 'pdf';
		 
		 
if sqlca.sqlcode = 100 then
	g_mb.messagebox("APICE","Impossibile archiviare il documento: impostare il mimetype")
	return 0
end if

setnull(ls_cod_nota)

select max(cod_nota)
into   :ls_cod_nota
from   tes_fat_ven_note
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 cod_nota like 'F%';
		 
if isnull(ls_cod_nota) or len(ls_cod_nota) < 1 then
	ls_cod_nota = "F01"
else
	ls_cod_nota = right(ls_cod_nota, 2)
	ll_ret = long(ls_cod_nota)
	ll_ret ++
	ls_cod_nota = "F" + string(ll_ret, "00")
end if

ls_nota = "Invio fattura PDF tramite e-mail"
ls_note = "Fattura Inviata dall'utente " + s_cs_xx.cod_utente + "~r~n" + &
          "Data invio:" + string(today(), "dd/mm/yyyy") + "  Ora invio:" + string(now(), "hh:mm:ss")

INSERT INTO tes_fat_ven_note  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  cod_nota,   
		  des_nota, 
		  note,
		  prog_mimetype)  
VALUES ( :s_cs_xx.cod_azienda,   
		  :fl_anno_registrazione,   
		  :fl_num_registrazione,   
		  :ls_cod_nota,   
		  :ls_nota,   
		  :ls_note,
		  :ll_prog_mimytype )  ;

updateblob tes_fat_ven_note
set        note_esterne = :l_blob
where      cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :fl_anno_registrazione and
			  num_registrazione = :fl_num_registrazione and
			  cod_nota = :ls_cod_nota;

end function

public function boolean wf_scadenze_fattura (ref string as_scad_fat);string ls_sql, ls_cod_tipo_pagamento, ls_des_tipo_pagamento, ls_flag_tipo_pagamento
int		li_scad_per_riga
long ll_rows, ll_row
datastore lds_store

select 	flag_tipo_pagamento 
into		:ls_flag_tipo_pagamento
from	 	tes_fat_ven
join		tab_pagamenti on tes_fat_ven.cod_azienda = tab_pagamenti.cod_azienda and tes_fat_ven.cod_pagamento = tab_pagamenti.cod_pagamento
where 	tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and
			tes_fat_ven.anno_registrazione = :il_anno_registrazione and
			tes_fat_ven.num_registrazione = :il_num_registrazione;
if sqlca.sqlcode <> 0 then 
	ls_flag_tipo_pagamento = ""
end if
			
ls_sql = "SELECT data_scadenza, imp_rata_valuta, cod_tipo_pagamento FROM scad_fat_ven WHERE"
ls_sql += " cod_azienda='" + s_cs_xx.cod_azienda+"' AND "
ls_sql += "anno_registrazione=" + string(il_anno_registrazione) + " AND "
ls_sql += "num_registrazione=" + string(il_num_registrazione)

if not f_crea_datastore(lds_store, ls_sql) then 
	g_mb.messagebox("APICE", "Errore durante la creazione del datastore per le scadenze")
	return false
end if

li_scad_per_riga = 4
as_scad_fat = ""
ll_rows = lds_store.retrieve()

if ll_rows > 0 then
	for ll_row = 1 to ll_rows
		// aggiunto su richiesta di Daniele Viropa che se lo era dimenticato
		if s_cs_xx.parametri.impresa then		
			ls_cod_tipo_pagamento = lds_store.getitemstring(ll_row, 3)
		else
			choose case ls_flag_tipo_pagamento
				case "R"
					ls_cod_tipo_pagamento = "RB"
				case else
					ls_cod_tipo_pagamento = "BB"
			end choose
		end if
//		if isvalid(sqlci) then
//			
//			if not isnull(ls_cod_tipo_pagamento) then
//				
//				setnull(ls_des_tipo_pagamento)
//				
//				select descrizione
//				into   :ls_des_tipo_pagamento
//				from   tipo_pagamento
//				where  codice = :ls_cod_tipo_pagamento
//				using  sqlci;
//					
//			end if
//		end if
		// fine modifica richiesta da Daniele Viropa
		
		if not isnull(ls_des_tipo_pagamento) then
			as_scad_fat += ls_cod_tipo_pagamento + " al " + string(lds_store.getitemdatetime(ll_row, 1), "dd/mm/yyyy") + " - " + string(round(lds_store.getitemnumber(ll_row, 2), 2))
		else	
			as_scad_fat += string(lds_store.getitemdatetime(ll_row, 1), "dd/mm/yyyy") + " - " + string(round(lds_store.getitemnumber(ll_row, 2), 2))
		end if
		
		if mod(ll_row, li_scad_per_riga) = 0 then
			as_scad_fat += "~n"
		else
			as_scad_fat += "     "
		end if
	next	
end if

return true
end function

public function integer wf_replace_marker (ref string as_source, string as_find, string as_replace, boolean ab_case_sensitive);/*
Function fof_replace_text
Created by: Michele
Creation Date: 13/12/2007
Comment: Given a string <as_source>, replaces all occurencies of <as_find> with <as_replace>

as_source                                                          Reference to the string to process
as_find                                                               The text we wish to change
as_replace                                                        The new text to be put in place of <as_find>

Return Values
Value                                                                   Comments
 1                                                                                           Everything OK
-1                                                                                          Some error occured
*/
long		ll_pos

 if isnull(as_source) or as_source = "" then
	return -1
end if

if isnull(as_find) or as_find = "" then
	return -1
end if

 ll_pos = 1

do
	 if ab_case_sensitive then
		ll_pos = pos(as_source,as_find,ll_pos)
	 else
		ll_pos = pos(lower(as_source),lower(as_find),ll_pos)
	 end if

	 if ll_pos = 0 then
		continue
	 end if
	 
	 as_source = replace(as_source,ll_pos,len(as_find),as_replace)	 
	 ll_pos += len(as_replace)

loop while ll_pos > 0

return 1


end function

public function integer wf_imposta_barcode ();string ls_bfo, ls_errore
int li_risposta, li_bco

li_risposta = f_font_barcode(ls_bfo,li_bco,ls_errore)
                              
if li_risposta < 0 then
                messagebox("SEP","Report 1 tende da sole: " + ls_errore,stopsign!)
                return -1
end if
                              
//dw_report.Modify("barcode.text='*"+is_CSB+ string(ll_progr_det_produzione) +is_CSB+ "*'")
dw_report_fat_ven.Modify("cf_barcode.Font.Face='" + ls_bfo + "'")
dw_report_fat_ven.Modify("cf_barcode.Font.Height= -" + string(li_bco) )
//dw_report_fat_ven.Modify("cf_barcode.Font.Height= -" + string(14) )
end function

public function integer wf_testo_mail (string fs_rag_soc_1, long fl_anno, long fl_numero, datetime fdt_data, string fs_lingua, ref string fs_testo);string 			ls_default, ls_path, ls_nome_file, ls_testo, ls_mark_ragsoc, ls_mark_numero, ls_mark_data

integer			li_FileNum

long				ll_pos_ragsoc, ll_pos_numero, ll_pos_data

string				ls_rag_soc_azienda, ls_tel, ls_fax

//parametri
ls_nome_file = "testo_mail_fatture"
ls_mark_ragsoc = "[RAGIONESOCIALE]"
ls_mark_numero = "[NUMERO]"
ls_mark_data = "[DATA]"

//il percorso contiene anche il backslash finale
if isnull(fs_lingua) then
	ls_path = s_cs_xx.volume + s_cs_xx.risorse +"11.5\" + ls_nome_file + ".txt"
else
	ls_path = s_cs_xx.volume + s_cs_xx.risorse +"11.5\" + ls_nome_file + "_" + fs_lingua + ".txt"
end if

ls_default = "Gentile Cliente " + fs_rag_soc_1 + ", " + &
					"con la presente Le trasmettiamo in allegato la nostra fattura N°" +  string(fl_anno) + "/" + &
					string(fl_numero)
if not isnull(fdt_data) then ls_default+=  " del " + string(fdt_data,"dd/mm/yyyy") + "."
	
ls_default += "~r~n"
ls_default += "Per qualunque chiarimento siamo a Vostra disposizione.~r~n~r~n" + &
					"Cordiali Saluti~r~n"

select rag_soc_1, telefono, fax
into :ls_rag_soc_azienda, :ls_tel, :ls_fax
from aziende
where cod_azienda=:s_cs_xx.cod_azienda;

if ls_rag_soc_azienda<>"" and not isnull(ls_rag_soc_azienda) then ls_default += ls_rag_soc_azienda + "~r~n"
if ls_tel<>"" and not isnull(ls_tel) then ls_default += "Tel. "+ls_tel + "~r~n"
if ls_fax<>"" and not isnull(ls_fax) then ls_default += "Fax. "+ls_fax + "~r~n"

ls_default += is_disclaimer

if not FileExists(ls_path) then
//	g_mb.messagebox("APICE","Il file di configurazione del testo del messaggio e-mail '"+ls_path+&
//												"' non esiste oppure è stato spostato!~n~r"+&
//												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

//lego dal file il testo personalizzato
li_FileNum = FileOpen(ls_path, TextMode!)
FileReadEx(li_FileNum, ls_testo)
FileClose(li_FileNum)


//sostituire i marcatori con i valori opportuni
//[RAGIONESOCIALE]
//[NUMERO]
//[DATA]

ll_pos_ragsoc = pos(ls_testo, ls_mark_ragsoc)
ll_pos_numero = pos(ls_testo, ls_mark_numero)
ll_pos_data = pos(ls_testo, ls_mark_data)

if ll_pos_ragsoc>0 then
else
	g_mb.messagebox("APICE","Marcatore "+ls_mark_ragsoc+" non trovato nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

if ll_pos_numero>0 then
else
	g_mb.messagebox("APICE","Marcatore "+ls_mark_numero+" non trovato nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

if ll_pos_data>0 then
else
	g_mb.messagebox("APICE","Marcatore "+ls_mark_data+" non trovato nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

fs_testo = ls_testo

//ragione sociale
if wf_replace_marker(fs_testo, ls_mark_ragsoc, fs_rag_soc_1, false)=1 then
else
	g_mb.messagebox("APICE","Errore in sostituzione marcatore "+ls_mark_ragsoc+" nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

//  anno/numero
if wf_replace_marker(fs_testo, ls_mark_numero, string(fl_anno)+"/"+string(fl_numero), false)=1 then
else
	g_mb.messagebox("APICE","Errore in sostituzione marcatore "+ls_mark_numero+" nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

//data
if wf_replace_marker(fs_testo, ls_mark_data, string(fdt_data,"dd/mm/yyyy"), false)=1 then
else
	g_mb.messagebox("APICE","Errore in sostituzione marcatore "+ls_mark_numero+" nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

return 1
end function

public function integer wf_profilo_registrazione (string as_profilo_impresa, ref long al_numero_registro, ref string as_errore);

select		numero
into		:al_numero_registro
from prof_registrazione
join mod_registro on prof_registrazione.id_mod_registro = mod_registro.id_mod_registro
where prof_registrazione.codice = :as_profilo_impresa
using sqlci;

if sqlci.sqlcode <> 0 then
	as_errore = "Errore in ricerca numero registro iva con il profilo " + as_profilo_impresa
	return -1
end if

return 0
end function

event pc_setwindow;call super::pc_setwindow;boolean lb_invio_singolo_mail=false

long    ll_i, ll_anno, ll_num, ll_num_documento, ll_ret, ll_righe_importo_zero

string  ls_cod_documento,ls_cod_tipo_fat_ven,ls_dataobject, ls_flag_email, ls_email_amministrazione, &
		  ls_cod_cliente, ls_flag_email_inviata, ls_email_utente

dw_report_fat_ven.ib_dw_report=true
iuo_dw_main = dw_report_fat_ven


guo_functions.uof_get_parametro("MST", ib_MAST)
if isnull(ib_MAST) then ib_MAST=false



set_w_options(c_noresizewin)
il_anno_registrazione = s_cs_xx.parametri.parametro_d_1
il_num_registrazione  = s_cs_xx.parametri.parametro_d_2

if il_corrente = 0 then 
	ib_nr_nr = s_cs_xx.parametri.parametro_b_1
	ib_email = s_cs_xx.parametri.parametro_b_2
end if

// stefanop 17/02/2012: leggo il percorso dell'immagine per la filigrana
select stringa
into :is_path_filigrana
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'LO6' and
		 flag_parametro = 'S';
		 
if sqlca.sqlcode <> 0 or is_path_filigrana = ""  then 
	setnull(is_path_filigrana)
else
	is_path_filigrana  = s_cs_xx.volume + is_path_filigrana
end if
// -------

// carico il dataobject a seconda che la fattura sia immediata o differita
// Enrico 1/10/2007 per viropa
setnull(ls_dataobject)

if not ib_nr_nr  then
	
	ll_righe_importo_zero = 0
	
	select count(*)
	into :ll_righe_importo_zero
	from det_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione and
			 cod_prodotto is not null and
			 (imponibile_iva = 0 or imponibile_iva is null) ;
	
	if ll_righe_importo_zero > 0 and not isnull(ll_righe_importo_zero) then
		if g_mb.messagebox("Apice", "Vi sono alcune righe di prodotti con importo a ZERO: proseguo lo stesso ?", Question!, YesNo!, 2) = 1 then
			f_scrivi_log( "Fattura " + string(il_anno_registrazione) + "/" + string(il_num_registrazione) + " presenta alcune righe di imponibile a ZERO: l'utente " + s_cs_xx.cod_utente + " ha scelto di proseguire lo stesso " )
		else
			return
		end if
	end if

	select cod_tipo_fat_ven,
			 cod_cliente,
			 flag_email_inviata
	into   :ls_cod_tipo_fat_ven,
			 :ls_cod_cliente,
			 :ls_flag_email_inviata
	from   tes_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Fattura richiesta non trovata~r~n" + sqlca.sqlerrtext)
		return
	end if
	
	if isnull(ls_cod_cliente) or ls_cod_cliente="" then
		ls_flag_email = "N"
		ls_email_amministrazione = "N"
		
	else
		select flag_accetta_mail,
				 email_amministrazione
		into   :ls_flag_email,
				 :ls_email_amministrazione
		from   anag_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Cliente " + ls_cod_cliente + " inesistente in anagrafica~r~n" + sqlca.sqlerrtext)
			return
		end if
		
	end if
	

	select dataobject,
			flag_tipo_fat_ven
	into   :ls_dataobject,
			:is_flag_tipo_fat_ven
	from   tab_tipi_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if isnull(ls_dataobject) or len(ls_dataobject) < 1 then
		g_mb.messagebox("APICE","Manca il dataobject nella tabella TIPI FATTURE~r~nViene applicato il report di default",stopsign!)
	else
		dw_report_fat_ven.dataobject = ls_dataobject
	end if
	
end if

// ------------------------------ fine modifica 1/10/2007  --------------------------
dw_report_fat_ven.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
						   c_disablecc, &
						   c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)
		
		

if il_corrente = 0 then
	
	ib_nr_nr = s_cs_xx.parametri.parametro_b_1
	s_cs_xx.parametri.parametro_b_1 = false

	ib_email = s_cs_xx.parametri.parametro_b_2
	s_cs_xx.parametri.parametro_b_2 = false

	if ib_nr_nr then
		
		declare stampa cursor for
		select   anno_registrazione,
					num_registrazione,
					flag_email,
					email_amministrazione,
					email_utente
		from     stampa_fat_ven
		where    cod_azienda = :s_cs_xx.cod_azienda and
					cod_utente = :s_cs_xx.cod_utente
		order by anno_registrazione ASC,
					num_registrazione ASC;
					
		open stampa;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore nella open del cursore stampa: " + sqlca.sqlerrtext)
			return
		end if
		
		ll_i = 1
		
		do while true
			
			fetch stampa
			into  :ll_anno,
					:ll_num,
					:ls_flag_email,
					:ls_email_amministrazione,
					:ls_email_utente;
					
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore nella fetch del cursore stampa: " + sqlca.sqlerrtext)
				close stampa;
				return
			elseif sqlca.sqlcode = 100 then
				close stampa;
				exit
			end if
			
			il_anno[ll_i] = ll_anno
			il_num[ll_i] = ll_num
			is_flag_email[ll_i] = ls_flag_email
			is_email_amministrazione[ll_i] = ls_email_amministrazione
			is_email_utente[ll_i] = ls_email_utente
			ll_i++
				
		loop
		
	else
		
		ib_email = false
		is_flag_email[1] = "N"
		is_email_amministrazione[1] = ""
		
		il_anno[1] = s_cs_xx.parametri.parametro_d_1
		il_num[1]  = s_cs_xx.parametri.parametro_d_2
		
	end if
	
end if

for il_corrente = 1 to upperbound(il_num[])
	
	il_anno_registrazione = il_anno[il_corrente]
	il_num_registrazione = il_num[il_corrente]
	
	wf_impostazioni()
	
	wf_report()
	
	if ib_nr_nr then
		triggerevent("pc_print")
		dw_report_fat_ven.reset()
		
	else
		// l'attuale cliente prevede l'invio della fattura tramite mail
		if ls_flag_email = "S" and not isnull(ls_flag_email) then
			
			ll_ret = g_mb.messagebox("APICE","Il cliente selezionato prevede l'invio fattura tramite E-Mail: procedo con l'invio?",Question!, YesNo!,2) 
			
			if ll_ret = 1 then
			
				is_flag_email[1] = "S"
				is_email_amministrazione[1] = ls_email_amministrazione
				
				update tes_fat_ven
				set flag_email_inviata = 'S'
				where cod_azienda = :s_cs_xx.cod_azienda and
				      anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
						num_registrazione = :s_cs_xx.parametri.parametro_d_2; 
						
				cb_singola.event clicked()
				
				// il commit avviene dentro al pulsante cb_singola perchè la funzione provvede alla 
				// memorizzazione del documento nel database.
				
			else
				
				update tes_fat_ven
				set flag_email_inviata = 'N'
				where cod_azienda = :s_cs_xx.cod_azienda and
				      anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
						num_registrazione = :s_cs_xx.parametri.parametro_d_2; 
						
				ib_email = false
				
			end if
			
		end if
		
	end if
	
next

il_corrente --

dw_report_fat_ven.change_dw_current()

// stefanop: 10/07/2012: ridimensiono la finestra per occupare tutta l'altezza dell' MDI
guo_functions.uof_resize_max_mdi(w_report_fat_ven_euro, 1)

commit;




	

end event

on w_report_fat_ven_euro.create
int iCurrent
call super::create
this.cbx_nomenclatura=create cbx_nomenclatura
this.cb_5=create cb_5
this.dw_report_fat_ven=create dw_report_fat_ven
this.cb_4=create cb_4
this.cb_singola=create cb_singola
this.cb_2=create cb_2
this.cb_3=create cb_3
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_nomenclatura
this.Control[iCurrent+2]=this.cb_5
this.Control[iCurrent+3]=this.dw_report_fat_ven
this.Control[iCurrent+4]=this.cb_4
this.Control[iCurrent+5]=this.cb_singola
this.Control[iCurrent+6]=this.cb_2
this.Control[iCurrent+7]=this.cb_3
this.Control[iCurrent+8]=this.cb_1
end on

on w_report_fat_ven_euro.destroy
call super::destroy
destroy(this.cbx_nomenclatura)
destroy(this.cb_5)
destroy(this.dw_report_fat_ven)
destroy(this.cb_4)
destroy(this.cb_singola)
destroy(this.cb_2)
destroy(this.cb_3)
destroy(this.cb_1)
end on

event pc_print;//IN QUESTO EVENTO IL FLAG EXTEND ANCESTOR DEVE ESSERE DISATTIVATO
cb_1.triggerevent("clicked")
end event

event open;call super::open;if ib_nr_nr then
	postevent("ue_close")
end if
end event

event close;call super::close;if ib_nr_nr then
	
	delete
	from   stampa_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_utente = :s_cs_xx.cod_utente;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella delete di stampa_fat_ven: " + sqlca.sqlerrtext)
		rollback;
	else
		commit;
	end if
	
	g_mb.messagebox("APICE","Processo di stampa completato!")
	
end if
end event

event resize;cb_1.move(20,20)

cb_3.move(newwidth - cb_3.width - 20, 20)
cb_2.move(cb_3.x - cb_2.width - 20, 20)

dw_report_fat_ven.move(20, cb_1.y + cb_1.height + 40)
dw_report_fat_ven.resize(newwidth - 40, newheight - dw_report_fat_ven.y - 20)
end event

type cbx_nomenclatura from checkbox within w_report_fat_ven_euro
integer x = 571
integer y = 40
integer width = 640
integer height = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 553648127
string text = "Nomenclature UE"
end type

event clicked;dw_report_fat_ven.postevent("pcd_retrieve")
end event

type cb_5 from commandbutton within w_report_fat_ven_euro
integer x = 2299
integer y = 24
integer width = 402
integer height = 104
integer taborder = 21
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "ARCHIVIA"
end type

event clicked;string ls_path
uo_archivia_pdf luo_pdf
uo_upload_documenti luo_upload

luo_pdf = create uo_archivia_pdf

ls_path = luo_pdf.uof_crea_pdf_path(dw_report_fat_ven)

if ls_path = "errore" then
	g_mb.messagebox ("Errore","Errore nella creazione del file pdf.")
	return -1
end if


try 
	luo_upload = create uo_upload_documenti
	luo_upload.uof_archivio_fattura_vendita(il_anno_registrazione, il_num_registrazione, ls_path)
catch (uo_upload_exception e)
	g_mb.error(e.getMessage())
end try

g_mb.success("Documento archiviato con successo.")

return 0
end event

type dw_report_fat_ven from uo_cs_xx_dw within w_report_fat_ven_euro
integer x = 23
integer y = 140
integer width = 3817
integer height = 3300
integer taborder = 30
string dataobject = "d_report_fat_ven_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_first;call super::pcd_first;//wf_report()
end event

event pcd_last;call super::pcd_last;//wf_report()
end event

event pcd_next;call super::pcd_next;//wf_report()
end event

event pcd_previous;call super::pcd_previous;//wf_report()
end event

event pcd_retrieve;call super::pcd_retrieve;
wf_report()
end event

event printend;call super::printend;ib_stampando = true
end event

event printpage;call super::printpage;// stefanop: 17/02/2012: devo controlla se sto stampando per l'invo della mail o se è per la stampante
// Nel caso della mail non devo fare nessun controllo sul numero di pagina altrimenti il saveas della datawindow
// mi ritorna sempre una foglio bianco!
if ib_email then return 0
// -------------------------------------------------------------

if ib_stampa and pagenumber <> il_pagina_corrente then
	return 1
end if
end event

event printstart;call super::printstart;il_totale_pagine = pagesmax
end event

event ue_anteprima_pdf;//LASCIARE CHECK ANCESTOR SCRIPT DISATTIVATO
//la variabile ib_mail viene messa a true prima di creare il pdf e poi rimessa a false
//per evitare di generare un pdf con foglio bianco ....
//il problema lo si evince dallo script dell'evento printpage

long ll_return, ll_elemento
string  ls_messaggio, ls_path, ls_to[], ls_allegati[], ls_error, ls_subject
uo_outlook luo_outlook
//oleobject iole_outlook, iole_item, iole_attach

ll_elemento = 0

setpointer(hourglass!)

s_cs_xx.parametri.parametro_dw_1 = this
s_cs_xx.parametri.parametro_s_1 = pcca.window_current.title

uo_archivia_pdf luo_pdf
luo_pdf = create uo_archivia_pdf

//#############################################
ib_email = true
//#############################################

ls_path = luo_pdf.uof_crea_pdf_path(this)

//#############################################
ib_email = false
//#############################################

if ls_path = "errore" then
	g_mb.messagebox ("Errore","Errore nella creazione del file pdf.")
	return -1
end if

ls_allegati[1] = ls_path

ls_subject = "Fattura " + string(il_anno_registrazione) + "/" + string(il_num_registrazione)

luo_outlook = create uo_outlook
if luo_outlook.uof_invio_sendmail(ls_to[], ls_subject, ls_subject, ls_allegati, ls_error) then
	g_mb.success("Email inviata con successo")
else
	g_mb.warning(ls_error)
end if

destroy luo_outlook

//invio pdf
//iole_outlook = create oleobject
//
//ll_return = iole_outlook.connecttonewobject("outlook.application")
//
//if ll_return <> 0 THEN
//	ls_messaggio = "Impossibile connettersi ad Outlook. Codice errore: " + string(ll_return)
//	g_mb.messagebox ("Errore",ls_messaggio)
//	
//	destroy iole_outlook
//	return -1
//end if
//
//// Viene creato un nuovo elemento del tipo specificato
//
//iole_item = iole_outlook.createitem(ll_elemento)
////visualizza la mail da inviare
//iole_item.display
//
//iole_attach = iole_item.attachments
//	
//// Il file specificato viene allegato solo se esiste al percorso indicato
//		
//if fileexists(ls_path) then
//	iole_attach.add(ls_path)
//else
//	ls_messaggio = "allegato inesistente" + string(ll_return)
//	g_mb.messagebox ("Errore",ls_messaggio)
//	destroy iole_attach
//	destroy iole_outlook
//	return -1
//end if
//		
//destroy iole_item		
//destroy iole_attach
filedelete(ls_path)
end event

event pcd_saverowsas;//LASCIARE CHECK ANCESTOR SCRIPT DISATTIVATO
//la variabile ib_mail viene messa a true prima di creare il pdf e poi rimessa a false
//per evitare di generare un pdf con foglio bianco ....
//il problema lo si evince dallo script dell'evento printpage


//################################################
ib_email = true
//################################################

saveas()

//################################################
ib_email = false
//################################################
end event

type cb_4 from commandbutton within w_report_fat_ven_euro
integer x = 1778
integer y = 20
integer width = 503
integer height = 108
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "STAMPA 1 COPIA"
end type

event clicked;wf_stampa(1)
end event

type cb_singola from commandbutton within w_report_fat_ven_euro
integer x = 1257
integer y = 20
integer width = 503
integer height = 108
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "EMAIL"
end type

event clicked;string ls_cod_cliente, ls_flag_accetta_mail,ls_email_amministrazione

il_corrente = 1

select cod_cliente
into   :ls_cod_cliente
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione and
		 num_registrazione =  :il_num_registrazione;
		 
select flag_accetta_mail,
       email_amministrazione
into   :ls_flag_accetta_mail,
       :ls_email_amministrazione
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente;
		 

if ls_flag_accetta_mail = "S" then 
	ib_email = true
else
	return
end if

if isnull(ls_email_amministrazione) or len(ls_email_amministrazione) < 1 or pos(ls_email_amministrazione, "@")< 1 then
	g_mb.messagebox("APICE","Impossibile inviare la mail: verificare le impostazione in anagrafica cliente")
	return
end if

is_flag_email[1] = "S"
is_email_amministrazione[1] = ls_email_amministrazione

wf_stampa(1)

ib_email = false
end event

type cb_2 from commandbutton within w_report_fat_ven_euro
integer x = 2830
integer y = 20
integer width = 503
integer height = 108
integer taborder = 11
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Pag. Prec."
end type

event clicked;dw_report_fat_ven.scrollpriorpage()

end event

type cb_3 from commandbutton within w_report_fat_ven_euro
integer x = 3333
integer y = 20
integer width = 503
integer height = 108
integer taborder = 11
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Pag. Succ."
end type

event clicked;dw_report_fat_ven.scrollnextpage()
end event

type cb_1 from commandbutton within w_report_fat_ven_euro
integer x = 32
integer y = 20
integer width = 503
integer height = 108
integer taborder = 1
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "STAMPA COPIE"
end type

event clicked;ib_stampa = true
wf_stampa(il_num_copie)


/* STEFANOP */
string ls_error
uo_moduli_stampa luo_moduli_stampa
luo_moduli_stampa = create uo_moduli_stampa
luo_moduli_stampa.uof_stampa_moduli(dw_report_fat_ven, "FATVEN", is_cod_lingua, is_cod_cliente, il_num_copie, ls_error)
destroy luo_moduli_stampa
/*---------------------*/
end event

