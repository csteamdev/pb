﻿$PBExportHeader$uo_cancella_mov_magazzino.sru
forward
global type uo_cancella_mov_magazzino from uo_cs_xx_base
end type
type str_fk from structure within uo_cancella_mov_magazzino
end type
end forward

type str_fk from structure
	string		tabella
	string		colonna_anno_reg
	string		colonna_numero_reg
end type

global type uo_cancella_mov_magazzino from uo_cs_xx_base
end type
global uo_cancella_mov_magazzino uo_cancella_mov_magazzino

type variables
private str_fk lstr_fk[]
private long il_indice
end variables

forward prototypes
public function integer uof_sql_update (string tabella, string colonna_anno_registrazione, string colonna_num_registrazione, long fl_anno_registrazione, long fl_num_registrazione)
public function integer uof_cancella_mov_magazzino (long fl_anno_registrazione, long fl_num_registrazione)
public function integer uof_rinumera_movimento (long fl_anno_registrazione, long fl_num_registrazione, long fl_nuovo_numero)
end prototypes

public function integer uof_sql_update (string tabella, string colonna_anno_registrazione, string colonna_num_registrazione, long fl_anno_registrazione, long fl_num_registrazione);string  ls_sql, ls_errore
uo_scrivi_log luo_scrivi_log

luo_scrivi_log = CREATE uo_scrivi_log

ls_sql="update " + tabella + &
       " set "   + colonna_anno_registrazione + "= null, " + &
		           + colonna_num_registrazione  + "= null " + &
		"where cod_azienda ='" + s_cs_xx.cod_azienda + "' and " + &
             colonna_anno_registrazione + " = " + string(fl_anno_registrazione) + " and " +  &
             colonna_num_registrazione  + " = " + string(fl_num_registrazione)


EXECUTE IMMEDIATE :ls_sql USING tran_loc;

if tran_loc.sqlcode = 0 then
	destroy luo_scrivi_log
else
	ls_errore = "Errore in cancllazione FK su tabella " + tabella+ &
	            "~r~n" + colonna_anno_registrazione + "=" + string(fl_anno_registrazione) + &
	            "~r~n" + colonna_num_registrazione + "=" + string(fl_num_registrazione)
	luo_scrivi_log.uo_log_cancella_movimenti_mag(s_cs_xx.cod_utente, '2',tran_loc.sqlcode, tran_loc.sqlerrtext, ls_errore)
	destroy luo_scrivi_log
	return -1
end if

return 0
end function

public function integer uof_cancella_mov_magazzino (long fl_anno_registrazione, long fl_num_registrazione);// ------------------------------------------------------------------------------------------
//			Funzione di cancellazione movimento di magazzino anche se referenziato;
//			vado in cerca in tutte le tabelle referenziate di un possibile legame con il
//			il movimento e metto a null la FK.
//			Poi vado a cancellare il movimento; eventuali errori vengono storicizzati nella
//			tabella LOG_SISTEMA per poi essere analizzati.
//			Questa funzione lavora su una transazione separata in modo da poter fare ROLLBACK
//			e COMMIT quando serve e non creare troopi LOCK.
//
//	enrico 26/6/2002
//	--------------------------------------------------------------------------------------------
integer li_ret
long ll_prog_mov,ll_anno_registrazione,ll_num_registrazione, ll_i
uo_scrivi_log luo_scrivi_log

luo_scrivi_log = CREATE uo_scrivi_log

select prog_mov
into   :ll_prog_mov
from   mov_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione
using tran_loc;
if tran_loc.sqlcode <> 0 then
	luo_scrivi_log.uo_log_cancella_movimenti_mag(s_cs_xx.cod_utente, '2',tran_loc.sqlcode, tran_loc.sqlerrtext,"Errore in select prog_mov from mov_magazzino where where  anno_registrazione = "+string(fl_anno_registrazione) + " and num_registrazione = " + string(fl_num_registrazione))
	return -1
end if

declare cu_mov_magazzino cursor for
select anno_registrazione, num_registrazione
from   mov_magazzino
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :fl_anno_registrazione and
      prog_mov = :ll_prog_mov
using tran_loc;

open cu_mov_magazzino;
if tran_loc.sqlcode <> 0 then
	luo_scrivi_log.uo_log_cancella_movimenti_mag(s_cs_xx.cod_utente, '2',tran_loc.sqlcode, tran_loc.sqlerrtext,"Errore in open cursore cu_mov_magazzino")
	return -1
end if

//*********************  vado a cancellare possibili FK ****************************************
do while true
	fetch cu_mov_magazzino into :ll_anno_registrazione, :ll_num_registrazione;
	if tran_loc.sqlcode = 100 then exit
	if tran_loc.sqlcode <> 0 then
		luo_scrivi_log.uo_log_cancella_movimenti_mag(s_cs_xx.cod_utente, '2',tran_loc.sqlcode, tran_loc.sqlerrtext,"Errore in fect cursore cu_mov_magazzino")
		return -1
	end if
	
	for ll_i = 1 to il_indice
		li_ret = uof_sql_update(lstr_fk[ll_i].tabella, lstr_fk[ll_i].colonna_anno_reg, lstr_fk[ll_i].colonna_numero_reg, ll_anno_registrazione,ll_num_registrazione)
		if li_ret <> 0 then exit
	next
	if li_ret <> 0 then exit
loop
// *******************  eseguo la delete del record e di eventuali record concatenati **********
if li_ret <> 0 then
	close cu_mov_magazzino;
	rollback using tran_loc;
	return -1
else
	close cu_mov_magazzino;
	commit using tran_loc;
end if

delete from mov_magazzino
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :fl_anno_registrazione and
      prog_mov = :ll_prog_mov
using tran_loc;
if tran_loc.sqlcode <> 0 then
	luo_scrivi_log.uo_log_cancella_movimenti_mag(s_cs_xx.cod_utente, '2',tran_loc.sqlcode, tran_loc.sqlerrtext,"Errore in cancellazione movimento magazzino (anno_prog_mov)" + string(fl_anno_registrazione) + "/" + string(ll_prog_mov))
	rollback using tran_loc;
	return -1
end if
luo_scrivi_log.uo_log_cancella_movimenti_mag(s_cs_xx.cod_utente, '1',tran_loc.sqlcode, tran_loc.sqlerrtext,"Cancellazione eseguita con successo del movimento (anno/prog_mov)" + string(fl_anno_registrazione) + "/" + string(ll_prog_mov))
commit using tran_loc;

return 0
end function

public function integer uof_rinumera_movimento (long fl_anno_registrazione, long fl_num_registrazione, long fl_nuovo_numero);string ls_sql

long   ll_i, ll_count

uo_scrivi_log luo_log


luo_log = create uo_scrivi_log

insert 
into   mov_magazzino  
       (cod_azienda,   
       anno_registrazione,   
       num_registrazione,   
       data_registrazione,   
       cod_tipo_movimento,   
       cod_prodotto,   
       cod_deposito,   
       cod_ubicazione,   
       cod_lotto,   
       quan_movimento,   
       val_movimento,   
       num_documento,   
       data_documento,   
       referenza,   
       flag_manuale,   
       flag_storico,   
       data_stock,   
       prog_stock,   
       prog_mov,   
       cod_tipo_mov_det,   
       cod_cliente,   
       cod_fornitore )  
select cod_azienda,   
       anno_registrazione,   
       :fl_nuovo_numero,   
       data_registrazione,   
       cod_tipo_movimento,   
       cod_prodotto,   
       cod_deposito,   
       cod_ubicazione,   
       cod_lotto,   
       quan_movimento,   
       val_movimento,   
       num_documento,   
       data_documento,   
       referenza,   
       flag_manuale,   
       flag_storico,   
       data_stock,   
       prog_stock,   
       prog_mov,   
       cod_tipo_mov_det,   
       cod_cliente,   
       cod_fornitore  
from   mov_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione
using  tran_loc;

if tran_loc.sqlcode <> 0 then
	luo_log.uo_log_cancella_movimenti_mag(s_cs_xx.cod_utente,'2',tran_loc.sqlcode,tran_loc.sqlerrtext,"Errore in inserimento movimento " + string(fl_anno_registrazione) + "/" + string(fl_nuovo_numero))
	rollback using tran_loc;
	return -1
end if

for ll_i = 1 to upperbound(lstr_fk)
	
	ls_sql = "update " + lstr_fk[ll_i].tabella + " set " + lstr_fk[ll_i].colonna_numero_reg + " = " + &
				string(fl_nuovo_numero) + " where " + lstr_fk[ll_i].colonna_anno_reg + " = " + &
				string(fl_anno_registrazione) + " and " + lstr_fk[ll_i].colonna_numero_reg + " = " + &
				string(fl_num_registrazione)
				
	execute immediate :ls_sql using tran_loc;
	
	if tran_loc.sqlcode <> 0 then
		luo_log.uo_log_cancella_movimenti_mag(s_cs_xx.cod_utente,'2',tran_loc.sqlcode,tran_loc.sqlerrtext,"Errore spostamento FK in " + lstr_fk[ll_i].tabella + " da " + string(fl_anno_registrazione) + "/" + string(fl_num_registrazione) + &
														  " a " + string(fl_anno_registrazione) + "/" + string(fl_nuovo_numero))
		rollback using tran_loc;
		return -1
	end if	
	
next

delete
from  mov_magazzino
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :fl_anno_registrazione and
		num_registrazione = :fl_num_registrazione
using tran_loc;
		
if tran_loc.sqlcode <> 0 then
	luo_log.uo_log_cancella_movimenti_mag(s_cs_xx.cod_utente,'2',tran_loc.sqlcode,tran_loc.sqlerrtext,"Errore in cancellazione movimento " + string(fl_anno_registrazione) + "/" + string(fl_num_registrazione))
	rollback using tran_loc;
	return -1
end if

luo_log.uo_log_cancella_movimenti_mag(s_cs_xx.cod_utente,'1',tran_loc.sqlcode,tran_loc.sqlerrtext,"Il movimento " + string(fl_anno_registrazione) + "/" + string(fl_num_registrazione) + &
												  " è stato rinumerato in " + string(fl_anno_registrazione) + "/" + string(fl_nuovo_numero))

commit using tran_loc;

destroy luo_log

return 0
end function

on uo_cancella_mov_magazzino.create
call super::create
end on

on uo_cancella_mov_magazzino.destroy
call super::destroy
end on

event constructor;call super::constructor;lstr_fk[1].tabella = "det_bol_ven"
lstr_fk[1].colonna_anno_reg = "anno_registrazione_mov_mag"
lstr_fk[1].colonna_numero_reg = "num_registrazione_mov_mag"

lstr_fk[2].tabella = "det_fat_ven"
lstr_fk[2].colonna_anno_reg = "anno_registrazione_mov_mag"
lstr_fk[2].colonna_numero_reg = "num_registrazione_mov_mag"

lstr_fk[3].tabella = "det_bol_acq"
lstr_fk[3].colonna_anno_reg = "anno_registrazione_mov_mag"
lstr_fk[3].colonna_numero_reg = "num_registrazione_mov_mag"

lstr_fk[4].tabella = "det_fat_acq"
lstr_fk[4].colonna_anno_reg = "anno_registrazione_mov_mag"
lstr_fk[4].colonna_numero_reg = "num_registrazione_mov_mag"

lstr_fk[5].tabella = "avan_produzione_com"
lstr_fk[5].colonna_anno_reg = "anno_reg_reso"
lstr_fk[5].colonna_numero_reg = "num_reg_reso"

lstr_fk[6].tabella = "avan_produzione_com"
lstr_fk[6].colonna_anno_reg = "anno_reg_sfrido"
lstr_fk[6].colonna_numero_reg = "num_reg_sfrido"

lstr_fk[7].tabella = "avan_produzione_com"
lstr_fk[7].colonna_anno_reg = "anno_reg_sl"
lstr_fk[7].colonna_numero_reg = "num_reg_sl"

lstr_fk[8].tabella = "det_anag_comm_anticipi"
lstr_fk[8].colonna_anno_reg = "anno_reg_mov_mag"
lstr_fk[8].colonna_numero_reg = "num_reg_mov_mag"

lstr_fk[9].tabella = "det_anag_commesse"
lstr_fk[9].colonna_anno_reg = "anno_registrazione"
lstr_fk[9].colonna_numero_reg = "num_registrazione"

lstr_fk[10].tabella = "det_anag_commesse"
lstr_fk[10].colonna_anno_reg = "anno_reg_anticipo"
lstr_fk[10].colonna_numero_reg = "num_reg_anticipo"

lstr_fk[11].tabella = "mat_prime_commessa"
lstr_fk[11].colonna_anno_reg = "anno_reg_reso"
lstr_fk[11].colonna_numero_reg = "num_reg_reso"

lstr_fk[12].tabella = "mat_prime_commessa"
lstr_fk[12].colonna_anno_reg = "anno_reg_sfrido"
lstr_fk[12].colonna_numero_reg = "num_reg_sfrido"

lstr_fk[13].tabella = "mat_prime_commessa_stock"
lstr_fk[13].colonna_anno_reg = "anno_registrazione"
lstr_fk[13].colonna_numero_reg = "num_registrazione"

lstr_fk[14].tabella = "det_fat_acq"
lstr_fk[14].colonna_anno_reg = "anno_reg_mov_mag_ret"
lstr_fk[14].colonna_numero_reg = "num_reg_mov_mag_ret"

//lstr_fk[15].tabella = "mp_com_stock_det_orari"
//lstr_fk[15].colonna_anno_reg = "anno_registrazione"
//lstr_fk[15].colonna_numero_reg = "num_registrazione"

//lstr_fk[15].tabella = "manutenzioni"
//lstr_fk[15].colonna_anno_reg = "anno_reg_mov_mag"
//lstr_fk[15].colonna_numero_reg = "num_reg_mov_mag"

// tenere sempre aggiornato con il numero delle tabelle

il_indice = 14

end event

