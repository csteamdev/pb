﻿$PBExportHeader$uo_cs_xx_base.sru
$PBExportComments$Oggetto Base che Apre un transazione indipendente al suo interno.
forward
global type uo_cs_xx_base from nonvisualobject
end type
end forward

global type uo_cs_xx_base from nonvisualobject
end type
global uo_cs_xx_base uo_cs_xx_base

type variables
string is_errore
integer ii_stato
transaction tran_loc
end variables

event constructor;integer li_errore, li_num_utenti, li_risposta
long     ll_ret
string  ls_parametri, ls_cod_utente, ls_password, ls_pass, ls_db, ls_logpass, &
		  ls_cod_azienda, ls_valore,ls_placca,ls_ini_section,fs_errore, ls_chiave_root

ls_ini_section = "database_" +  s_cs_xx.profilocorrente
ls_chiave_root = s_cs_xx.chiave_root

tran_loc = CREATE transaction

li_risposta = registryget(ls_chiave_root + ls_ini_section, "servername", tran_loc.ServerName)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazione del database sul registro."
	return -1
end if

li_risposta = registryget(ls_chiave_root + ls_ini_section, "logid", tran_loc.LogId)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazione del database sul registro."
	return -1
end if

li_risposta = registryget(ls_chiave_root + ls_ini_section, "logpass", ls_logpass)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazione del database sul registro."
	return -1
end if

li_risposta = registryget(ls_chiave_root + ls_ini_section, "dbms", tran_loc.DBMS)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazione del database sul registro."
	return -1
end if

li_risposta = registryget(ls_chiave_root + ls_ini_section, "database", tran_loc.Database)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazione del database sul registro."
	return -1
end if

li_risposta = registryget(ls_chiave_root + ls_ini_section, "userid", tran_loc.UserId)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazione del database sul registro."
	return -1
end if

li_risposta = registryget(ls_chiave_root + ls_ini_section, "dbpass", tran_loc.DBPass)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazione del database sul registro."
	return -1
end if

li_risposta = registryget(ls_chiave_root + ls_ini_section, "dbparm", tran_loc.DBParm)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazione del database sul registro."
	return -1
end if

li_risposta = registryget(ls_chiave_root + ls_ini_section, "lock", tran_loc.Lock)
if li_risposta = -1 then
	fs_errore = "Mancano le impostazione del database sul registro."
	return -1
end if

if tran_loc.DBMS <> "ODBC" then
	n_cst_crypto lnv_crypt
	lnv_crypt = CREATE n_cst_crypto
	
	if isnull(ls_logpass) or ls_logpass="" then
		
		g_mb.messagebox("Attenzione", "Manca la password per l'accesso al database è necessario impostarla ora altrimenti non è possibile accedere al sistema.", &
              Stopsign!)
				  				  
		s_cs_xx.parametri.parametro_b_1 = false
	
		window_open(w_imposta_pwd,0)
	
		if s_cs_xx.parametri.parametro_b_1 = true then
			ls_logpass = s_cs_xx.parametri.parametro_s_1
			li_risposta = RegistrySet(ls_chiave_root + ls_ini_section, "logpass", ls_logpass)

			if li_risposta < 0 then
				g_mb.messagebox("Apice","Attenzione! Non si hanno i privilegi per scrivere sul registro di windows. Contattare l'amministratore della macchina e riprovare.",stopsign!)
				DESTROY lnv_crypt
				halt close
			end if

		else
			DESTROY lnv_crypt
			halt close
			
		end if
		
	end if
	
	ll_ret = lnv_crypt.DecryptData(ls_logpass, ref ls_logpass)
	if ll_ret <> 0 then
		g_mb.messagebox("Apice","Attenzione! Errore in decrypt password.",stopsign!)
		DESTROY lnv_crypt
		halt close
	end if

	tran_loc.LogPass = ls_logpass
	
	DESTROY lnv_crypt

end if

connect using tran_loc;

if tran_loc.sqlcode <> 0 then
	fs_errore ="Errore durante la connessione con il DB del profilo " +  s_cs_xx.profilocorrente + ". descrizione errore: " + tran_loc.sqlerrtext
	return -1
end if
ii_stato = 0
is_errore = "ok!"

end event

on uo_cs_xx_base.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_cs_xx_base.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event destructor;disconnect using tran_loc;
destroy tran_loc

end event

