﻿$PBExportHeader$uo_scrivi_log.sru
forward
global type uo_scrivi_log from uo_cs_xx_base
end type
type str_tabella from structure within uo_scrivi_log
end type
end forward

type str_tabella from structure
	string		elenco_fk[]
end type

global type uo_scrivi_log from uo_cs_xx_base
end type
global uo_scrivi_log uo_scrivi_log

forward prototypes
public function integer uof_scrivi_log_txt (string fs_messaggio)
public function integer uo_log_cancella_movimenti_mag (string fs_utente, string fs_flag_tipo_log, decimal fd_sqlcode, string fs_sqlerrtext, string fs_messaggio)
end prototypes

public function integer uof_scrivi_log_txt (string fs_messaggio);integer li_file,li_risposta
string ls_volume, ls_nome_file, ls_messaggio

select stringa
into   :ls_nome_file
from   parametri
where  cod_parametro = 'LOG';

if sqlca.sqlcode <> 0  then
	g_mb.messagebox("File LOG","Manca parametro multiaziendale LOG indicante nome del file")
	return -1
end if

li_risposta = Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "LOG", ls_volume)

if li_risposta = -1 then
	g_mb.messagebox("File LOG","Manca parametro LOG (registry HKLM) indicante volume condiviso documenti.")
	return -1
end if

li_file = fileopen(ls_volume + ls_nome_file, linemode!, Write!, LockWrite!, Append!)
if li_file = -1 then
	g_mb.messagebox("LOG File","Errore durante l'apertura del file LOG; verificare le connessioni delle unità di rete")
	return -1
end if

ls_messaggio = s_cs_xx.cod_utente + "~t" + string(today(),"dd/mm/yyyy") + "~t" + string(now(),"hh:mm:ss") +  "~t" + fs_messaggio
li_risposta = filewrite(li_file, ls_messaggio)
fileclose(li_file)

return 0

end function

public function integer uo_log_cancella_movimenti_mag (string fs_utente, string fs_flag_tipo_log, decimal fd_sqlcode, string fs_sqlerrtext, string fs_messaggio);datetime ldt_data, ldt_ora

ldt_data = datetime(today(),00:00:00)
ldt_ora  = datetime(date("01/01/1900"),now())

INSERT INTO log_sistema  
		( data_registrazione,   
		  ora_registrazione,   
		  utente,   
		  flag_tipo_log,   
		  db_sqlcode,   
		  db_sqlerrtext,   
		  messaggio )  
VALUES (:ldt_data,   
		  :ldt_ora,   
		  :fs_utente,   
		  :fs_flag_tipo_log,   
		  :fd_sqlcode,   
		  :fs_sqlerrtext,   
		  :fs_messaggio )
using tran_loc;

//if sqlca.sqlcode <> 0 then
if tran_loc.sqlcode <> 0 then
	rollback using tran_loc;
	
	// se fa errore qui scrivo il log su file TXT
	uof_scrivi_log_txt("Errore in insert su tabella LOG_SYSTEMA~r~n"+sqlca.sqlerrtext)
	return -1
end if

commit using tran_loc;
return 0
end function

on uo_scrivi_log.create
call super::create
end on

on uo_scrivi_log.destroy
call super::destroy
end on

