﻿$PBExportHeader$w_elimina_reg_mov_mag.srw
forward
global type w_elimina_reg_mov_mag from window
end type
type cbx_zero from checkbox within w_elimina_reg_mov_mag
end type
type st_8 from statictext within w_elimina_reg_mov_mag
end type
type st_7 from statictext within w_elimina_reg_mov_mag
end type
type st_6 from statictext within w_elimina_reg_mov_mag
end type
type st_4 from statictext within w_elimina_reg_mov_mag
end type
type st_3 from statictext within w_elimina_reg_mov_mag
end type
type cb_1 from commandbutton within w_elimina_reg_mov_mag
end type
type st_5 from statictext within w_elimina_reg_mov_mag
end type
type dw_movimenti from datawindow within w_elimina_reg_mov_mag
end type
type cb_elimina from commandbutton within w_elimina_reg_mov_mag
end type
type em_data_fine from editmask within w_elimina_reg_mov_mag
end type
type st_2 from statictext within w_elimina_reg_mov_mag
end type
type st_1 from statictext within w_elimina_reg_mov_mag
end type
type em_data_inizio from editmask within w_elimina_reg_mov_mag
end type
type dw_selezione_elimina_movimenti from datawindow within w_elimina_reg_mov_mag
end type
end forward

global type w_elimina_reg_mov_mag from window
integer width = 2112
integer height = 2532
boolean titlebar = true
string title = "Elimina Movimenti Magazzino"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
long backcolor = 12632256
cbx_zero cbx_zero
st_8 st_8
st_7 st_7
st_6 st_6
st_4 st_4
st_3 st_3
cb_1 cb_1
st_5 st_5
dw_movimenti dw_movimenti
cb_elimina cb_elimina
em_data_fine em_data_fine
st_2 st_2
st_1 st_1
em_data_inizio em_data_inizio
dw_selezione_elimina_movimenti dw_selezione_elimina_movimenti
end type
global w_elimina_reg_mov_mag w_elimina_reg_mov_mag

event open;string ls_cod_tipo_movimento, ls_des_tipo_movimento
long ll_riga

em_data_inizio.text = string("01/01/2001")
em_data_fine.text = string("31/12/2001")

dw_selezione_elimina_movimenti.reset()

DECLARE cu_tipi_mov CURSOR FOR  
SELECT cod_tipo_movimento,   
		des_tipo_movimento  
 FROM tab_tipi_movimenti  
WHERE cod_azienda = :s_cs_xx.cod_azienda   
ORDER BY cod_azienda ASC,   
         cod_tipo_movimento ASC  ;

open cu_tipi_mov;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in OPEN del cursore cu_tipi_mov")
	return
end if

do while true
	fetch cu_tipi_mov into :ls_cod_tipo_movimento, :ls_des_tipo_movimento;
	if sqlca.sqlcode <> 0 then exit
	
	ll_riga = dw_selezione_elimina_movimenti.insertrow(0)
	dw_selezione_elimina_movimenti.setitem(ll_riga,"cod_tipo_movimento",ls_cod_tipo_movimento)
	dw_selezione_elimina_movimenti.setitem(ll_riga,"des_tipo_movimento",ls_des_tipo_movimento)
	dw_selezione_elimina_movimenti.setitem(ll_riga,"flag_elimina","N")
loop

commit;
end event

on w_elimina_reg_mov_mag.create
this.cbx_zero=create cbx_zero
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.st_4=create st_4
this.st_3=create st_3
this.cb_1=create cb_1
this.st_5=create st_5
this.dw_movimenti=create dw_movimenti
this.cb_elimina=create cb_elimina
this.em_data_fine=create em_data_fine
this.st_2=create st_2
this.st_1=create st_1
this.em_data_inizio=create em_data_inizio
this.dw_selezione_elimina_movimenti=create dw_selezione_elimina_movimenti
this.Control[]={this.cbx_zero,&
this.st_8,&
this.st_7,&
this.st_6,&
this.st_4,&
this.st_3,&
this.cb_1,&
this.st_5,&
this.dw_movimenti,&
this.cb_elimina,&
this.em_data_fine,&
this.st_2,&
this.st_1,&
this.em_data_inizio,&
this.dw_selezione_elimina_movimenti}
end on

on w_elimina_reg_mov_mag.destroy
destroy(this.cbx_zero)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.cb_1)
destroy(this.st_5)
destroy(this.dw_movimenti)
destroy(this.cb_elimina)
destroy(this.em_data_fine)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.em_data_inizio)
destroy(this.dw_selezione_elimina_movimenti)
end on

type cbx_zero from checkbox within w_elimina_reg_mov_mag
integer x = 1006
integer y = 992
integer width = 960
integer height = 72
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 0
string text = "Solo Movimenti con quantità = 0"
boolean lefttext = true
end type

type st_8 from statictext within w_elimina_reg_mov_mag
integer x = 18
integer y = 2128
integer width = 1957
integer height = 72
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type st_7 from statictext within w_elimina_reg_mov_mag
integer x = 599
integer y = 2332
integer width = 928
integer height = 72
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_6 from statictext within w_elimina_reg_mov_mag
integer x = 599
integer y = 2224
integer width = 928
integer height = 80
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 16777215
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_4 from statictext within w_elimina_reg_mov_mag
integer x = 18
integer y = 2336
integer width = 549
integer height = 76
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "DATA / ORA FINE"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_3 from statictext within w_elimina_reg_mov_mag
integer x = 18
integer y = 2232
integer width = 549
integer height = 76
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "DATA / ORA INIZIO"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_elimina_reg_mov_mag
integer x = 1600
integer y = 2032
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elimina"
end type

event clicked;long ll_i, ll_rowcount, ll_ret, ll_seconds
time lt_1, lt_now
uo_cancella_mov_magazzino luo_cancella

ll_rowcount = dw_movimenti.rowcount()
luo_cancella = CREATE uo_cancella_mov_magazzino

st_6.text = string(date(today()), "dd/mm/yyyy") + " : " + string( now(), "hh:mm" )
lt_1 = now()

for ll_i = 1 to ll_rowcount
	yield()
	st_5.text = "IN CANCELLAZIONE " + string(dw_movimenti.getitemnumber(ll_i,"anno_registrazione")) + "-" + string(dw_movimenti.getitemnumber(ll_i,"num_registrazione"))
	ll_ret = luo_cancella.uof_cancella_mov_magazzino(dw_movimenti.getitemnumber(ll_i,"anno_registrazione"), dw_movimenti.getitemnumber(ll_i,"num_registrazione"))
	if ll_ret = 0 then
		dw_movimenti.setitem(ll_i,"flag_eliminato","S")
	end if
	lt_now = now()
	ll_seconds = (secondsafter(lt_1, lt_now)) / ll_i
	
	st_8.text = string(ll_seconds) + "/sec movimento (" + string(ll_i) + "-" + string(ll_rowcount) + ")"
next

destroy luo_cancella

st_7.text = string(date(today()), "dd/mm/yyyy") + " : " + string( now(), "hh:mm" )

g_mb.messagebox("APICE", "Elaborazione Eseguita!")

end event

type st_5 from statictext within w_elimina_reg_mov_mag
integer x = 23
integer y = 2032
integer width = 1554
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_movimenti from datawindow within w_elimina_reg_mov_mag
integer x = 27
integer y = 1088
integer width = 1943
integer height = 920
string title = "none"
string dataobject = "d_ext_movimenti_da_cancellare"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type cb_elimina from commandbutton within w_elimina_reg_mov_mag
integer x = 1600
integer y = 900
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;string ls_tipi_movimenti, ls_sql
long ll_i, ll_riga, ll_anno_registrazione, ll_num_registrazione
decimal ld_quan_movimento
datetime ldt_data_inizio, ldt_data_fine, ldt_data_registrazione

ldt_data_inizio = datetime(date(em_data_inizio.text),00:00:00)
ldt_data_fine = datetime(date(em_data_fine.text),00:00:00)
//ll_anno_registrazione = long(em_anno_registrazione.text)
//ll_num_registrazione = long(em_num_registrazione.text)

ls_tipi_movimenti =""
dw_movimenti.reset()

for ll_i = 1 to dw_selezione_elimina_movimenti.rowcount()
	if dw_selezione_elimina_movimenti.getitemstring(ll_i,"flag_elimina") = "S" then
		ls_tipi_movimenti = ls_tipi_movimenti + "'" + dw_selezione_elimina_movimenti.getitemstring(ll_i,"cod_tipo_movimento") + "',"
	end if
next
//if ls_tipi_movimenti <> "" then
//	ls_tipi_movimenti = left(ls_tipi_movimenti,len(ls_tipi_movimenti) - 1)
//else
//	ls_tipi_movimenti = "xxx"
//end if


ls_sql = " select anno_registrazione, num_registrazione,quan_movimento,data_registrazione from mov_magazzino " +&
         " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and data_registrazione >= '" + string(ldt_data_inizio,"dd-mmm-yyyy") + "' and data_registrazione <= '" + string(ldt_data_fine,"dd-mmm-yyyy") + "'"

if cbx_zero.checked then
	ls_sql = ls_sql + " and quan_movimento = 0 "
end if 

if ls_tipi_movimenti <> "" then
	ls_tipi_movimenti = left(ls_tipi_movimenti,len(ls_tipi_movimenti) - 1)
	ls_sql = ls_sql + " and cod_tipo_movimento in (" + ls_tipi_movimenti + ")  "
end if
ls_sql = ls_sql + " order by anno_registrazione, num_registrazione "
declare cu_del_mov DYNAMIC CURSOR FOR SQLSA ;
PREPARE SQLSA FROM :ls_sql;
open DYNAMIC cu_del_mov;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","errore in open cu_del_mov;~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

do while true
	fetch cu_del_mov into :ll_anno_registrazione, :ll_num_registrazione, :ld_quan_movimento, :ldt_data_registrazione;
	if sqlca.sqlcode <> 0 then exit
	yield()
	ll_riga = dw_movimenti.insertrow(0)
	dw_movimenti.setitem(ll_riga,"anno_registrazione", ll_anno_registrazione)
	dw_movimenti.setitem(ll_riga,"num_registrazione", ll_num_registrazione)
	dw_movimenti.setitem(ll_riga,"data_registrazione", ldt_data_registrazione)
	dw_movimenti.setitem(ll_riga,"quan_movimento", ld_quan_movimento)
	dw_movimenti.setitem(ll_riga,"flag_eliminato", "N")
	
	st_5.text = string(ll_anno_registrazione) + "-" + string(ll_num_registrazione)
loop

commit;

close cu_del_mov;

st_5.text = "Letti " + string(dw_movimenti.rowcount()) + " movimenti da cancellare"
end event

type em_data_fine from editmask within w_elimina_reg_mov_mag
integer x = 1166
integer y = 900
integer width = 389
integer height = 60
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

type st_2 from statictext within w_elimina_reg_mov_mag
integer x = 869
integer y = 900
integer width = 274
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "A Data:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_elimina_reg_mov_mag
integer x = 46
integer y = 900
integer width = 274
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Da Data:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_data_inizio from editmask within w_elimina_reg_mov_mag
integer x = 366
integer y = 900
integer width = 389
integer height = 60
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean border = false
alignment alignment = center!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
end type

type dw_selezione_elimina_movimenti from datawindow within w_elimina_reg_mov_mag
integer x = 23
integer width = 1966
integer height = 880
integer taborder = 10
string title = "none"
string dataobject = "d_selezione_elimina_movimenti"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

