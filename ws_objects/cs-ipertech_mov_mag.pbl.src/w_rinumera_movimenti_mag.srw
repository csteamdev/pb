﻿$PBExportHeader$w_rinumera_movimenti_mag.srw
forward
global type w_rinumera_movimenti_mag from window
end type
type cb_2 from commandbutton within w_rinumera_movimenti_mag
end type
type cb_1 from commandbutton within w_rinumera_movimenti_mag
end type
type dw_sel_rinumera_movimenti_mag from datawindow within w_rinumera_movimenti_mag
end type
type dw_rinumera_movimenti_mag from datawindow within w_rinumera_movimenti_mag
end type
end forward

global type w_rinumera_movimenti_mag from window
integer width = 2368
integer height = 1584
boolean titlebar = true
string title = "Rinumerazione movimenti di magazzino"
boolean controlmenu = true
boolean minbox = true
long backcolor = 67108864
cb_2 cb_2
cb_1 cb_1
dw_sel_rinumera_movimenti_mag dw_sel_rinumera_movimenti_mag
dw_rinumera_movimenti_mag dw_rinumera_movimenti_mag
end type
global w_rinumera_movimenti_mag w_rinumera_movimenti_mag

on w_rinumera_movimenti_mag.create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_sel_rinumera_movimenti_mag=create dw_sel_rinumera_movimenti_mag
this.dw_rinumera_movimenti_mag=create dw_rinumera_movimenti_mag
this.Control[]={this.cb_2,&
this.cb_1,&
this.dw_sel_rinumera_movimenti_mag,&
this.dw_rinumera_movimenti_mag}
end on

on w_rinumera_movimenti_mag.destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_sel_rinumera_movimenti_mag)
destroy(this.dw_rinumera_movimenti_mag)
end on

event open;dw_sel_rinumera_movimenti_mag.insertrow(0)
end event

type cb_2 from commandbutton within w_rinumera_movimenti_mag
integer x = 23
integer y = 1360
integer width = 2309
integer height = 120
integer taborder = 30
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "AVVIA RINUMERAZIONE MOVIMENTI DI MAGAZZINO"
end type

event clicked;long     ll_i, ll_nuovo, ll_anno, ll_num

uo_cancella_mov_magazzino luo_rinumera


setpointer(hourglass!)

enabled = false
cb_1.enabled = false

ll_nuovo = 0

for ll_i = 1 to dw_rinumera_movimenti_mag.rowcount()
	
	dw_rinumera_movimenti_mag.setrow(ll_i)
	
	dw_rinumera_movimenti_mag.scrolltorow(ll_i)
	
	ll_nuovo ++
	
	ll_anno = dw_rinumera_movimenti_mag.getitemnumber(ll_i,"anno")
	
	ll_num = dw_rinumera_movimenti_mag.getitemnumber(ll_i,"numero")
	
	dw_rinumera_movimenti_mag.setitem(ll_i,"esaminato","S")
	
	if ll_num = ll_nuovo then
		dw_rinumera_movimenti_mag.setitem(ll_i,"nuovo",ll_num)
		continue
	end if
	
	luo_rinumera = create uo_cancella_mov_magazzino
	
	if luo_rinumera.uof_rinumera_movimento(ll_anno,ll_num,ll_nuovo) <> 0 then
		g_mb.messagebox("APICE","Errore in rinumerazione movimento " + string(ll_anno) + "/" + string(ll_num),stopsign!)
		destroy luo_rinumera
		setpointer(arrow!)
		enabled = true
		cb_1.enabled = true
		return -1
	end if
	
	destroy luo_rinumera
	
	dw_rinumera_movimenti_mag.setitem(ll_i,"nuovo", ll_nuovo)
	dw_rinumera_movimenti_mag.setitem(ll_i,"rinumerato", "S")
	
next

setpointer(arrow!)

enabled = true

cb_1.enabled = true

g_mb.messagebox("APICE","Rinumerazione movimenti completata con successo!",information!)

dw_rinumera_movimenti_mag.setrow(1)
	
dw_rinumera_movimenti_mag.scrolltorow(1)
end event

type cb_1 from commandbutton within w_rinumera_movimenti_mag
integer x = 1920
integer y = 60
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "CERCA"
end type

event clicked;datetime ldt_data

long     ll_i, ll_anno, ll_num, ll_trovati


dw_sel_rinumera_movimenti_mag.accepttext()

ll_anno = dw_sel_rinumera_movimenti_mag.getitemnumber(1,"anno")

if isnull(ll_anno) then
	g_mb.messagebox("APICE","Impostare l'anno su cui eseguire la rinumerazione!",stopsign!)
	return -1
end if

ll_num = dw_sel_rinumera_movimenti_mag.getitemnumber(1,"numero")

setpointer(hourglass!)

declare movimenti cursor for
select   anno_registrazione,
		   num_registrazione,
		   data_registrazione
from     mov_magazzino
where    cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno and
			num_registrazione <= :ll_num
order by num_registrazione;

open movimenti;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella open del cursore movimenti: " + sqlca.sqlerrtext,stopsign!)
	setpointer(arrow!)
	return -1
end if

enabled = false
cb_2.enabled = false

ll_trovati = 0

dw_sel_rinumera_movimenti_mag.setitem(1,"trovati",ll_trovati)

dw_rinumera_movimenti_mag.reset()

do while true
	
	fetch movimenti
	into  :ll_anno,
	      :ll_num,
			:ldt_data;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore nella fetch del cursore movimenti: " + sqlca.sqlerrtext,stopsign!)
		close movimenti;
		setpointer(arrow!)
		enabled = true
		cb_2.enabled = false
		dw_rinumera_movimenti_mag.reset()
		return -1
	elseif sqlca.sqlcode = 100 then
		exit
	end if
	
	ll_trovati ++
	
	dw_sel_rinumera_movimenti_mag.setitem(1,"trovati",ll_trovati)
	
	ll_i = dw_rinumera_movimenti_mag.insertrow(0)
	
	dw_rinumera_movimenti_mag.setitem(ll_i,"anno",ll_anno)
	dw_rinumera_movimenti_mag.setitem(ll_i,"numero",ll_num)
	dw_rinumera_movimenti_mag.setitem(ll_i,"data_mov",ldt_data)
	dw_rinumera_movimenti_mag.setitem(ll_i,"nuovo",0)
	dw_rinumera_movimenti_mag.setitem(ll_i,"esaminato","N")
	dw_rinumera_movimenti_mag.setitem(ll_i,"rinumerato","N")	
	
loop

close movimenti;

setpointer(arrow!)

enabled = true

if dw_rinumera_movimenti_mag.rowcount() > 0 then
	cb_2.enabled = true
else
	cb_2.enabled = false
end if
end event

type dw_sel_rinumera_movimenti_mag from datawindow within w_rinumera_movimenti_mag
integer x = 23
integer y = 20
integer width = 2309
integer height = 160
integer taborder = 40
string dataobject = "d_sel_rinumera_movimenti_mag"
boolean livescroll = true
end type

type dw_rinumera_movimenti_mag from datawindow within w_rinumera_movimenti_mag
integer x = 23
integer y = 200
integer width = 2309
integer height = 1140
string dataobject = "d_rinumera_movimenti_mag"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

