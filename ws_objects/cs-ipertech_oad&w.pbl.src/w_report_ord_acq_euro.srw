﻿$PBExportHeader$w_report_ord_acq_euro.srw
$PBExportComments$Report Ordine Acquisto
forward
global type w_report_ord_acq_euro from w_cs_xx_principale
end type
type dw_report_ord_acq from uo_cs_xx_dw within w_report_ord_acq_euro
end type
end forward

global type w_report_ord_acq_euro from w_cs_xx_principale
integer width = 3872
integer height = 4772
string title = "Stampa Ordini Acquisto"
boolean minbox = false
boolean hscrollbar = true
boolean vscrollbar = true
event ue_setfocus ( )
dw_report_ord_acq dw_report_ord_acq
end type
global w_report_ord_acq_euro w_report_ord_acq_euro

type variables
boolean ib_stampa_residuo
long il_anno_registrazione, il_num_registrazione
string is_title
end variables

forward prototypes
public function integer wf_report ()
public function integer wf_imposta_barcode ()
end prototypes

event ue_setfocus();dw_report_ord_acq.change_dw_current()
end event

public function integer wf_report ();string 	ls_stringa, ls_cod_tipo_ord_acq, ls_cod_banca_clien_for, ls_cod_fornitore, ls_cod_mezzo, ls_des_mezzo, ls_des_mezzo_lingua, &
		 	ls_cod_valuta, ls_cod_pagamento, ls_num_ord_fornitore, ls_cod_porto, ls_cod_resa, ls_nota_testata, ls_nota_piede, ls_rag_soc_1, &
		 	ls_rag_soc_2, ls_indirizzo,  ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, ls_cod_prodotto, &
		 	ls_des_prodotto, ls_rag_soc_1_for, ls_rag_soc_2_for, ls_indirizzo_for, ls_localita_for, ls_frazione_for, ls_cap_for, &
		 	ls_provincia_for, ls_partita_iva_for, ls_cod_fiscale_for, ls_cod_lingua, ls_flag_tipo_fornitore, ls_des_tipo_ord_acq, &
		 	ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua , &
		 	ls_des_prodotto_anag, ls_flag_stampa_ordine, ls_cod_tipo_det_acq, ls_des_prodotto_lingua, ls_cod_prod_fornitore, &
		 	ls_cod_fil_fornitore, ls_rag_soc_1_fil, ls_rag_soc_2_fil, ls_indirizzo_fil, ls_localita_fil, ls_frazione_fil, ls_cap_fil, &
		 	ls_provincia_fil, ls_cod_operatore, ls_des_operatore, ls_des_prodotto_fornitore, ls_flag_saldo, ls_rif_interno, ls_formato, &
			ls_cod_vettore, ls_des_vettore, ls_iban, ls_flag, ls_des_specifica, ls_cod_deposito, ls_cod_deposito_transito, ls_cod_des_fornitore, &
			ls_tes_ord_acq_riferimento, ls_des_deposito_origine, ls_indirizzo_deposito_origine, ls_localita_deposito_origine, ls_frazione_deposito_origine,   &
			ls_cap_deposito_origine, ls_provincia_deposito_origine, ls_telefono_deposito_origine, ls_fax_deposito_origine, ls_telex_deposito_origine,&
			ls_cod_deposito_operatore, ls_telefono_deposito_operatore, ls_label_des_specifica, ls_mail_destinatario, ls_visible, ls_null, &
			ls_stampa_nota_testata, ls_stampa_nota_piede, ls_stampa_nota_video

long   	ll_errore, ll_riga, ll_num_reg_rda, ll_prog_riga_rda

integer	li_anno_reg_rda,li_anno_reg_ord_ven

dec{4} 	ld_tot_val_ordine, ld_sconto, ld_quan_ordinata, ld_prezzo_acquisto, ld_sconto_1, ld_sconto_2, ld_val_riga, &
			ld_sconto_pagamento, ld_quan_arrivata, ld_tot_imponibile, ld_tot_iva, ld_quan_totale, &
			ld_imponibile_iva_valuta, ld_importo_iva_valuta, ld_tot_val_ordine_valuta		 
			
dec{5}   ld_fat_conversione

datetime ldt_data_ord_fornitore, ldt_data_registrazione, ldt_data_consegna

//Donato 29-10-2008 aggiunto campo Banca Nostra, telefono e fax del Fornitore sul report ----------
string ls_cod_banca_nostra, ls_des_banca_nostra, ls_iban_banca_nostra, ls_tel_fornitore, ls_fax_fornitore
//----------------------------------------------------------------------------------------------------

dw_report_ord_acq.setredraw(false)
dw_report_ord_acq.reset()
wf_imposta_barcode()
setnull(ls_null)

// carico la valuta di base del sistema

select parametri_azienda.stringa  
into  :ls_stringa  
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua

select flag
into	 :ls_flag
from	 parametri_azienda
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'DTL';
		 
if sqlca.sqlcode = 0 and not isnull(ls_flag) and ls_flag = "S" then
	ls_flag = "S" 
else
	ls_flag = "N"
end if

select cod_tipo_ord_acq,   
		 data_registrazione,   
		 cod_fornitore,   
		 cod_fil_fornitore,
		 cod_valuta,   
		 cod_pagamento,   
		 sconto,   
		 cod_banca_clien_for,   
		 num_ord_fornitore,   
		 data_ord_fornitore,   
		 cod_porto,   
		 cod_resa,   
		 cod_mezzo,   
		 nota_testata,   
		 nota_piede,
		 imponibile_iva,
		 importo_iva,
		 tot_val_ordine,
		 rag_soc_1,
		 rag_soc_2,
		 indirizzo,
		 localita,
		 frazione,
		 cap,
		 provincia,
		 cod_operatore,
		 cod_vettore,
		 cod_banca,
		 cod_deposito,
		 cod_deposito_trasf,
		 cod_des_fornitore,
		 imponibile_iva_valuta,
		 importo_iva_valuta,
		 tot_val_ordine_valuta
into   :ls_cod_tipo_ord_acq,   
	 	 :ldt_data_registrazione,   
	 	 :ls_cod_fornitore,   
		 :ls_cod_fil_fornitore,
		 :ls_cod_valuta,   
		 :ls_cod_pagamento,   
		 :ld_sconto,   
		 :ls_cod_banca_clien_for,   
		 :ls_num_ord_fornitore,   
		 :ldt_data_ord_fornitore,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_cod_mezzo,
		 :ls_nota_testata,   
		 :ls_nota_piede,
		 :ld_tot_imponibile,
		 :ld_tot_iva,
		 :ld_tot_val_ordine,
		 :ls_rag_soc_1,
		 :ls_rag_soc_2,
		 :ls_indirizzo,
		 :ls_localita,
		 :ls_frazione,
		 :ls_cap,
		 :ls_provincia, 
		 :ls_cod_operatore,
		 :ls_cod_vettore,
		 :ls_cod_banca_nostra,
		 :ls_cod_deposito,
		 :ls_cod_deposito_transito,
		 :ls_cod_des_fornitore,
		 :ld_imponibile_iva_valuta,
		 :ld_importo_iva_valuta,
		 :ld_tot_val_ordine_valuta		 
from   tes_ord_acq  
where  tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_ord_acq.anno_registrazione = :il_anno_registrazione and 
		 tes_ord_acq.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_ord_acq)
	setnull(ldt_data_registrazione)
	setnull(ls_cod_fornitore)
	setnull(ls_cod_fil_fornitore)
	setnull(ls_cod_valuta)
	setnull(ls_cod_pagamento)
	setnull(ld_sconto)
	setnull(ls_cod_banca_clien_for)
	setnull(ls_num_ord_fornitore)
	setnull(ldt_data_ord_fornitore)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ld_tot_imponibile)
	setnull(ld_tot_iva)
	setnull(ld_tot_val_ordine)
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
	setnull(ls_indirizzo)
	setnull(ls_localita)
	setnull(ls_frazione)
	setnull(ls_cap)
	setnull(ls_provincia)
	setnull(ls_cod_vettore)
	//Donato 29-10-2008 aggiunto campo Banca Nostra sul report ----------
	setnull(ls_cod_banca_nostra)
	setnull(ld_imponibile_iva_valuta)
	setnull(ld_importo_iva_valuta)
	setnull(ld_tot_val_ordine_valuta)
	//------------------------------------------------------------------------
end if

if not isnull(ls_cod_vettore) then
	
	select rag_soc_1
	into   :ls_des_vettore
	from   anag_vettori
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_vettore = :ls_cod_vettore;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella select di anag_vettori: " + sqlca.sqlerrtext)
		setnull(ls_cod_vettore)
		setnull(ls_des_vettore)
	end if
	
end if

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return -1
end if

select anag_fornitori.rag_soc_1,   
       anag_fornitori.rag_soc_2,   
       anag_fornitori.indirizzo,   
       anag_fornitori.localita,   
       anag_fornitori.frazione,   
       anag_fornitori.cap,   
       anag_fornitori.provincia,   
       anag_fornitori.partita_iva,   
       anag_fornitori.cod_fiscale,   
       anag_fornitori.cod_lingua,   
       anag_fornitori.flag_tipo_fornitore,
	  anag_fornitori.rif_interno,
	  //Donato 29-10-2008 aggiunti i campi telefono e fax del fornitore sul report ----------
	  anag_fornitori.telefono,
	  anag_fornitori.fax,
	  anag_fornitori.casella_mail
	  //-------------------------------------------------------------------------------------
into   :ls_rag_soc_1_for,   
       :ls_rag_soc_2_for,   
       :ls_indirizzo_for,   
       :ls_localita_for,   
       :ls_frazione_for,   
       :ls_cap_for,   
       :ls_provincia_for,   
       :ls_partita_iva_for,   
       :ls_cod_fiscale_for,   
       :ls_cod_lingua,   
       :ls_flag_tipo_fornitore,
	  :ls_rif_interno,
	   //Donato 29-10-2008 aggiunti i campi telefono e fax del fornitore sul report ----------
	  :ls_tel_fornitore,
	  :ls_fax_fornitore,
	  :ls_mail_destinatario
	  //-------------------------------------------------------------------------------------
from   anag_fornitori  
where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and
       anag_fornitori.cod_fornitore = :ls_cod_fornitore;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_for)
	setnull(ls_rag_soc_2_for)
	setnull(ls_indirizzo_for)
	setnull(ls_localita_for)
	setnull(ls_frazione_for)
	setnull(ls_cap_for)
	setnull(ls_provincia_for)
	setnull(ls_partita_iva_for)
	setnull(ls_cod_fiscale_for)
	setnull(ls_cod_lingua)
	setnull(ls_flag_tipo_fornitore)
	//Donato 29-10-2008 aggiunti i campi telefono e fax del fornitore sul report ----------
	setnull(ls_tel_fornitore)
	setnull(ls_fax_fornitore)
	//-------------------------------------------------------------------------------------
else
	dw_report_ord_acq.is_mail_destinatario = ls_mail_destinatario
end if

if ls_flag_tipo_fornitore = 'E' then
	ls_partita_iva_for = ls_cod_fiscale_for
end if

select anag_fil_fornitori.rag_soc_1,   
       anag_fil_fornitori.rag_soc_2,   
       anag_fil_fornitori.indirizzo,   
       anag_fil_fornitori.localita,   
       anag_fil_fornitori.frazione,   
       anag_fil_fornitori.cap,   
       anag_fil_fornitori.provincia	 
into   :ls_rag_soc_1_fil,   
       :ls_rag_soc_2_fil,   
       :ls_indirizzo_fil,   
       :ls_localita_fil,   
       :ls_frazione_fil,   
       :ls_cap_fil,   
       :ls_provincia_fil	  
from   anag_fil_fornitori
where  anag_fil_fornitori.cod_azienda = :s_cs_xx.cod_azienda and
       anag_fil_fornitori.cod_fornitore = :ls_cod_fornitore and
		 anag_fil_fornitori.cod_fil_fornitore = :ls_cod_fil_fornitore;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_fil)
	setnull(ls_rag_soc_2_fil)
	setnull(ls_indirizzo_fil)
	setnull(ls_localita_fil)
	setnull(ls_frazione_fil)
	setnull(ls_cap_fil)
	setnull(ls_provincia_fil)	
end if

select 	des_deposito
into 		:ls_tes_ord_acq_riferimento
from 		anag_depositi
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_deposito = :ls_cod_deposito;
if sqlca.sqlcode <> 0 then
	if sqlca.sqlcode = 100 then sqlca.sqlerrtext = "Deposito transito inesistente in anagrafica depositi"
	g_mb.messagebox("Apice", "Errore in lettura deposito di transito~r~n" + sqlca.sqlerrtext)
end if

// carico dati del deposito che ha generato l'ordine

  SELECT des_deposito,   
         indirizzo,   
         localita,   
         frazione,   
         cap,   
         provincia,   
         telefono,   
         fax,   
         telex  
    INTO :ls_des_deposito_origine,   
         :ls_indirizzo_deposito_origine,   
         :ls_localita_deposito_origine,   
         :ls_frazione_deposito_origine,   
         :ls_cap_deposito_origine,   
         :ls_provincia_deposito_origine,   
         :ls_telefono_deposito_origine,   
         :ls_fax_deposito_origine,   
         :ls_telex_deposito_origine  
    FROM anag_depositi  
	 where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_deposito = :ls_cod_deposito;
if sqlca.sqlcode <> 0 then
	if sqlca.sqlcode = 100 then sqlca.sqlerrtext = "Deposito emittente ordine inesistente in anagrafica depositi"
	g_mb.messagebox("Apice", "Errore in lettura deposito di transito~r~n" + sqlca.sqlerrtext)
end if

// carico il numero di telefono dell'operatore che ha creato l'ordine
if not isnull(ls_cod_operatore) then
	
	select cod_deposito
	into 	:ls_cod_deposito_operatore
	from 	tab_operatori_utenti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_operatore = :ls_cod_operatore;
			
	if sqlca.sqlcode=0 and not isnull(ls_cod_deposito_operatore) then
		select telefono
		into :ls_telefono_deposito_operatore
		from anag_depositi
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_deposito = :ls_cod_deposito_operatore;
	end if

end if


// vado a verificare le altre destinazioni solo se non è caricata la destinazione manualmente nella testata ordine.
if len(ls_rag_soc_1) < 1 or isnull(ls_rag_soc_1) then

	if len(ls_cod_deposito_transito) > 0 and not isnull(ls_cod_deposito_transito) then
	// deposito di transito
		select 	des_deposito,   
        				indirizzo,   
        				localita,   
         			frazione,   
         			cap,   
         			provincia  
		into 		:ls_rag_soc_1,
		 			:ls_indirizzo,
		 			:ls_localita,
		 			:ls_frazione,
		 			:ls_cap,
		 			:ls_provincia
		from		anag_depositi 
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_deposito = :ls_cod_deposito_transito;
		if sqlca.sqlcode <> 0 then
			if sqlca.sqlcode = 100 then sqlca.sqlerrtext = "Deposito transito inesistente in anagrafica depositi"
			g_mb.messagebox("Apice", "Errore in lettura deposito di transito~r~n" + sqlca.sqlerrtext)
		end if
		
	elseif len(ls_cod_des_fornitore) > 0 or not isnull(ls_cod_des_fornitore) then
		// destinazione fornitore
		select 	rag_soc_1,   
        				indirizzo,   
        				localita,   
         			frazione,   
         			cap,   
         			provincia  
		into 		:ls_rag_soc_1,
		 			:ls_indirizzo,
		 			:ls_localita,
		 			:ls_frazione,
		 			:ls_cap,
		 			:ls_provincia  
		from		anag_des_fornitori
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_des_fornitore = :ls_cod_des_fornitore and
					cod_fornitore = :ls_cod_fornitore;
		if sqlca.sqlcode <> 0 then
			if sqlca.sqlcode = 100 then sqlca.sqlerrtext = "Destinazione fornitore inesistente in anagrafica depositi"
			g_mb.messagebox("Apice", "Errore in lettura deposito di transito~r~n" + sqlca.sqlerrtext)
		end if
		
	elseif len(ls_cod_deposito) > 0 and not isnull(ls_cod_deposito) then
	// Imposto la destinazione merce in base al deposito di carico merce
		select 	des_deposito,   
        				indirizzo,   
        				localita,   
         			frazione,   
         			cap,   
         			provincia  
		into 		:ls_rag_soc_1,
		 			:ls_indirizzo,
		 			:ls_localita,
		 			:ls_frazione,
		 			:ls_cap,
		 			:ls_provincia  
		from		anag_depositi 
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_deposito = :ls_cod_deposito;
		if sqlca.sqlcode <> 0 then
			if sqlca.sqlcode = 100 then sqlca.sqlerrtext = "Deposito inesistente in anagrafica depositi"
			g_mb.messagebox("Apice", "Errore in lettura deposito di transito~r~n" + sqlca.sqlerrtext)
		end if
	end if
end if

// fine gestione depositi e trasferimenti

select tab_tipi_ord_acq.des_tipo_ord_acq  
into   :ls_des_tipo_ord_acq  
from   tab_tipi_ord_acq  
where  tab_tipi_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and
       tab_tipi_ord_acq.cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_ord_acq)
end if

select tab_pagamenti.des_pagamento,   
       tab_pagamenti.sconto  
into   :ls_des_pagamento,   
       :ld_sconto_pagamento  
from   tab_pagamenti  
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
end if

select tab_pagamenti_lingue.des_pagamento  
into   :ls_des_pagamento_lingua  
from   tab_pagamenti_lingue  
where  tab_pagamenti_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_pagamenti_lingue.cod_pagamento = :ls_cod_pagamento and
       tab_pagamenti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

select anag_banche_clien_for.des_banca,
			iban
into   :ls_des_banca,
		 :ls_iban
from   anag_banche_clien_for  
where  anag_banche_clien_for.cod_azienda = :s_cs_xx.cod_azienda and 
       anag_banche_clien_for.cod_banca_clien_for = :ls_cod_banca_clien_for;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca)
end if

//Donato 29-10-2008 aggiunto campo Banca Nostra sul report: recuperare l'iban -----------
select anag_banche.des_banca,
			iban
into   :ls_des_banca_nostra,
		 :ls_iban_banca_nostra
from   anag_banche  
where  anag_banche.cod_azienda = :s_cs_xx.cod_azienda and 
       anag_banche.cod_banca = :ls_cod_banca_nostra;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca_nostra)
end if
//--------------------------------------------------------------------------------------------

select tab_porti.des_porto  
into   :ls_des_porto  
from   tab_porti  
where  tab_porti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti.cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select tab_porti_lingue.des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  tab_porti_lingue.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti_lingue.cod_porto = :ls_cod_porto and
       tab_porti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  tab_rese.cod_azienda = :s_cs_xx.cod_azienda and
       tab_rese.cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select tab_rese_lingue.des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  tab_rese_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_rese_lingue.cod_resa = :ls_cod_resa and  
       tab_rese_lingue.cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

select des_mezzo
into   :ls_des_mezzo
from   tab_mezzi  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_mezzo   = :ls_cod_mezzo;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo)
end if

select des_mezzo
into   :ls_des_mezzo_lingua  
from   tab_mezzi_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_mezzo   = :ls_cod_mezzo and  
       cod_lingua  = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo_lingua)
end if

// -----------------------------  modifica Enrico 01-08-2017 per stampa note fisse solo nei documenti -----------------

if guo_functions.uof_get_note_fisse(ls_null, ls_cod_fornitore, ls_null,"STAMPA_ORD_ACQ", ls_null, ldt_data_registrazione, ref ls_stampa_nota_testata, ref ls_stampa_nota_piede, ref ls_stampa_nota_video) < 0 then
	g_mb.error(ls_stampa_nota_testata)
else
	if len(ls_stampa_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_stampa_nota_video)
end if
	
if not isnull(ls_stampa_nota_testata) and len(ls_stampa_nota_testata) > 0 then
	if isnull(ls_nota_testata) or len(ls_nota_testata) = 0 then
		ls_nota_testata = ls_stampa_nota_testata
	else
		ls_nota_testata = ls_stampa_nota_testata + "~r~n" + ls_nota_testata
	end if
end if

if not isnull(ls_stampa_nota_piede) and len(ls_stampa_nota_piede) > 0 then
	if isnull(ls_nota_piede) or len(ls_nota_piede) = 0 then
		ls_nota_piede = ls_stampa_nota_piede
	else
		ls_nota_piede =  ls_nota_piede + "~r~n" + ls_stampa_nota_piede
	end if
end if

// --------------------------------------------------------------------------------------------------------------------


declare cu_dettagli cursor for 
	select   cod_tipo_det_acq, 
				cod_misura, 
				quan_ordinata, 
				prezzo_acquisto, 
				sconto_1, 
				sconto_2, 
				imponibile_iva_valuta, 
				data_consegna, 
				nota_dettaglio, 
				cod_prodotto, 
				des_prodotto,
				cod_prod_fornitore,
				fat_conversione,
				flag_saldo,
				quan_arrivata,
				des_specifica,
				anno_reg_rda,
				num_reg_rda,
				prog_riga_rda
	from     det_ord_acq 
	where    cod_azienda = :s_cs_xx.cod_azienda and 
				anno_registrazione = :il_anno_registrazione and 
				num_registrazione = :il_num_registrazione
	order by cod_azienda, 
				anno_registrazione, 
				num_registrazione,
				prog_riga_ordine_acq;

open cu_dettagli;
ld_quan_totale = 0
ll_riga = 0

do while true
   fetch cu_dettagli into :ls_cod_tipo_det_acq, 
								  :ls_cod_misura, 
								  :ld_quan_ordinata, 
								  :ld_prezzo_acquisto,   
								  :ld_sconto_1, 
								  :ld_sconto_2, 
								  :ld_val_riga, 
								  :ldt_data_consegna, 
								  :ls_nota_dettaglio, 
								  :ls_cod_prodotto, 
								  :ls_des_prodotto,
								  :ls_cod_prod_fornitore,
								  :ld_fat_conversione,
								  :ls_flag_saldo,
								  :ld_quan_arrivata,
								  :ls_des_specifica,
								  :li_anno_reg_rda,
								  :ll_num_reg_rda,
								  :ll_prog_riga_rda;

   if sqlca.sqlcode < 0 then 
		g_mb.error("Errore durante la lettura delle righe di dettaglio.", sqlca)
		exit
	elseif sqlca.sqlcode = 100 then
		exit
	end if
	
	
	if not(ib_stampa_residuo) or (ls_flag_saldo ="N" and ib_stampa_residuo and ld_quan_ordinata > ld_quan_arrivata) then

		ll_riga = dw_report_ord_acq.insertrow(0)
		dw_report_ord_acq.setrow(ll_riga)
	
		dw_report_ord_acq.setitem(ll_riga, "parametri_azienda_stringa", ls_stringa)
		dw_report_ord_acq.setitem(ll_riga, "formato", ls_formato)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_anno_registrazione", il_anno_registrazione)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_num_registrazione", il_num_registrazione)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_cod_tipo_ord_acq", ls_cod_tipo_ord_acq)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_data_registrazione", ldt_data_registrazione)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_cod_fornitore", ls_cod_fornitore)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_cod_fil_fornitore", ls_cod_fil_fornitore)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_cod_valuta", ls_cod_valuta)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_cod_pagamento", ls_cod_pagamento)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_sconto", ld_sconto)
		
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_cod_banca_clien_for", ls_cod_banca_clien_for)
		//Donato 29-10-2008 aggiunto campo Banca Nostra sul report ------
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_cod_banca", ls_cod_banca_nostra)
		//---------------------------------------------------------------------
		
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_num_ord_fornitore", ls_num_ord_fornitore)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_data_ord_fornitore", ldt_data_ord_fornitore)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_nota_testata", ls_nota_testata)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_nota_piede", ls_nota_piede)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_cod_porto", ls_cod_porto)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_cod_resa", ls_cod_resa)
		
		// enme: 27/06/2014: per fornitore comunitario, l'ordine viene inserito con iva, e quindi devo evidenziare solo imponibile
		if ls_flag_tipo_fornitore = "C" then
			dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_tot_imponibile", ld_tot_imponibile)
			dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_tot_iva", 0)
			dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_tot_val_ordine", ld_tot_imponibile)
			
			//---- EnMe x Deko0ra 07-02-2014 (aggiunta totali in valuta) 
			dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_tot_imponibile_valuta", ld_imponibile_iva_valuta)
			dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_tot_iva_valuta", 0)
			dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_tot_val_ordine_valuta", ld_imponibile_iva_valuta)
		else
			dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_tot_imponibile", ld_tot_imponibile)
			dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_tot_iva", ld_tot_iva)
			dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_tot_val_ordine", ld_tot_val_ordine)
			
			//---- EnMe x Deko0ra 07-02-2014 (aggiunta totali in valuta) 
			dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_tot_imponibile_valuta", ld_imponibile_iva_valuta)
			dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_tot_iva_valuta", ld_importo_iva_valuta)
			dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_tot_val_ordine_valuta", ld_tot_val_ordine_valuta)
		end if
		
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_rag_soc_1", ls_rag_soc_1)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_rag_soc_2", ls_rag_soc_2)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_indirizzo", ls_indirizzo)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_localita", ls_localita)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_frazione", ls_frazione)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_cap", ls_cap)
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_provincia", ls_provincia)
		
		dw_report_ord_acq.setitem(ll_riga, "tes_ord_acq_riferimento", ls_tes_ord_acq_riferimento)
	
		dw_report_ord_acq.setitem(ll_riga, "anag_fornitori_rag_soc_1", ls_rag_soc_1_for)
		dw_report_ord_acq.setitem(ll_riga, "anag_fornitori_rag_soc_2", ls_rag_soc_2_for)
		dw_report_ord_acq.setitem(ll_riga, "anag_fornitori_indirizzo", ls_indirizzo_for)
		dw_report_ord_acq.setitem(ll_riga, "anag_fornitori_localita", ls_localita_for)
		dw_report_ord_acq.setitem(ll_riga, "anag_fornitori_frazione", ls_frazione_for)
		dw_report_ord_acq.setitem(ll_riga, "anag_fornitori_cap", ls_cap_for)
		dw_report_ord_acq.setitem(ll_riga, "anag_fornitori_provincia", ls_provincia_for)
		dw_report_ord_acq.setitem(ll_riga, "anag_fornitori_partita_iva", ls_partita_iva_for)
		dw_report_ord_acq.setitem(ll_riga, "anag_fornitori_rif_interno", ls_rif_interno)
		
		//Donato 29-10-2008 aggiunti i campi telefono e fax del fornitore sul report ----------
		dw_report_ord_acq.setitem(ll_riga, "anag_fornitori_tel", ls_tel_fornitore)
		dw_report_ord_acq.setitem(ll_riga, "anag_fornitori_fax", ls_fax_fornitore)
		//------------------------------------------------------------------------------------
		
		dw_report_ord_acq.setitem(ll_riga, "anag_fil_fornitori_rag_soc_1", ls_rag_soc_1_fil)
		dw_report_ord_acq.setitem(ll_riga, "anag_fil_fornitori_rag_soc_2", ls_rag_soc_2_fil)
		dw_report_ord_acq.setitem(ll_riga, "anag_fil_fornitori_indirizzo", ls_indirizzo_fil)
		dw_report_ord_acq.setitem(ll_riga, "anag_fil_fornitori_localita", ls_localita_fil)
		dw_report_ord_acq.setitem(ll_riga, "anag_fil_fornitori_frazione", ls_frazione_fil)
		dw_report_ord_acq.setitem(ll_riga, "anag_fil_fornitori_cap", ls_cap_fil)
		dw_report_ord_acq.setitem(ll_riga, "anag_fil_fornitori_provincia", ls_provincia_fil)
	
		dw_report_ord_acq.setitem(ll_riga, "tab_tipi_ord_acq_des_tipo_ord_acq", ls_des_tipo_ord_acq)
		
		dw_report_ord_acq.setitem(ll_riga, "tab_pagamenti_des_pagamento", ls_des_pagamento)
		dw_report_ord_acq.setitem(ll_riga, "tab_pagamenti_sconto", ld_sconto_pagamento)
		
		dw_report_ord_acq.setitem(ll_riga, "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
		
		dw_report_ord_acq.setitem(ll_riga, "anag_banche_clien_for_des_banca", ls_des_banca)
		dw_report_ord_acq.setitem(ll_riga, "anag_banche_clien_for_iban", ls_iban)
		//Donato 29-10-2008 aggiunto campo Banca Nostra sul report: campo des_banca e relativo iban ------
		dw_report_ord_acq.setitem(ll_riga, "anag_banche_des_banca", ls_des_banca_nostra)
		dw_report_ord_acq.setitem(ll_riga, "anag_banche_iban", ls_iban_banca_nostra)
		//------------------------------------------------------------------------------------------------------
		
		// --- EnMe --- Deposito emittente ordine
		dw_report_ord_acq.setitem(ll_riga, "anag_depositi_rag_soc_1", ls_des_deposito_origine)
		dw_report_ord_acq.setitem(ll_riga, "anag_depositi_indirizzo", ls_indirizzo_deposito_origine )
		dw_report_ord_acq.setitem(ll_riga, "anag_depositi_cap",ls_cap_deposito_origine )
		dw_report_ord_acq.setitem(ll_riga, "anag_depositi_localita", ls_localita_deposito_origine)
		dw_report_ord_acq.setitem(ll_riga, "anag_depositi_frazione", ls_frazione_deposito_origine)
		dw_report_ord_acq.setitem(ll_riga, "anag_depositi_provincia", ls_provincia_deposito_origine)
		dw_report_ord_acq.setitem(ll_riga, "anag_depositi_telefono", ls_telefono_deposito_origine)
		dw_report_ord_acq.setitem(ll_riga, "anag_depositi_fax", ls_fax_deposito_origine)
		dw_report_ord_acq.setitem(ll_riga, "anag_depositi_email", ls_telex_deposito_origine)

	
	
		select tab_tipi_det_acq.flag_stampa_ordine
		into   :ls_flag_stampa_ordine  
		from   tab_tipi_det_acq
		where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;
	
		if sqlca.sqlcode <> 0 then
			setnull(ls_flag_stampa_ordine)
		end if
	
		if ls_flag_stampa_ordine = 'S' then
			select anag_prodotti.des_prodotto  
			into   :ls_des_prodotto_anag  
			from   anag_prodotti  
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_anag)
			end if
			

			// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua
			
			if ls_flag = "S" then
				setnull(ls_des_prodotto_lingua)
			else			
		
				select anag_prodotti_lingue.des_prodotto  
				into   :ls_des_prodotto_lingua  
				from   anag_prodotti_lingue  
				where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
						 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
				if sqlca.sqlcode <> 0 then
					setnull(ls_des_prodotto_lingua)
				end if
			end if
	
			dw_report_ord_acq.setitem(ll_riga, "det_ord_acq_cod_misura", ls_cod_misura)
			
			if ib_stampa_residuo then ld_quan_ordinata = ld_quan_ordinata - ld_quan_arrivata
			
			ld_quan_totale += ld_quan_ordinata * ld_fat_conversione
			dw_report_ord_acq.setitem(ll_riga, "det_ord_acq_quan_ordinata", ld_quan_ordinata * ld_fat_conversione)
			dw_report_ord_acq.setitem(ll_riga, "det_ord_acq_prezzo_acquisto", ld_prezzo_acquisto / ld_fat_conversione)
			dw_report_ord_acq.setitem(ll_riga, "det_ord_acq_sconto_1", ld_sconto_1)
			dw_report_ord_acq.setitem(ll_riga, "det_ord_acq_sconto_2", ld_sconto_2)
			
			if not(ib_stampa_residuo) then 
				dw_report_ord_acq.setitem(ll_riga, "det_ord_acq_val_riga", ld_val_riga)
			else
				dw_report_ord_acq.setitem(ll_riga, "det_ord_acq_val_riga", 0)
			end if
			
			dw_report_ord_acq.setitem(ll_riga, "det_ord_acq_data_consegna", ldt_data_consegna)
			
			setnull(ls_des_prodotto_fornitore)
			
			select des_prod_fornitore
			  into :ls_des_prodotto_fornitore
			  from tab_prod_fornitori
			 where cod_azienda   = :s_cs_xx.cod_azienda
				and cod_prodotto  = :ls_cod_prodotto
				and cod_fornitore = :ls_cod_fornitore;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_fornitore)
			end if	
			
			
			if not isnull(ls_des_specifica) and len(ls_des_specifica) > 0 then
				
				ls_label_des_specifica = "COD.COLORE:"
				setnull(li_anno_reg_ord_ven)
				
				//se l'ordine di acquisto è generato da RDA e nella riga RDA riferita c'è il riferimento alla riga ordine vendita
				//allora in des_specifica c'è anno-numero ordine di vendita, quindi inietta 'NR.ORDINE:' al posto di 'COD.COLORE:'
				
				if li_anno_reg_rda>0 and ll_num_reg_rda>0 and ll_prog_riga_rda>0 then
					//la riga ordine acquisto è stata generata da RDA
					
					select anno_reg_ord_ven
					into :li_anno_reg_ord_ven
					from det_rda
					where 	cod_azienda=:s_cs_xx.cod_azienda and
								anno_registrazione=:li_anno_reg_rda and
								num_registrazione=:ll_num_reg_rda and
								prog_riga_rda=:ll_prog_riga_rda;
					
					if li_anno_reg_ord_ven>0 then 
						ls_label_des_specifica = "NR.ORDINE:"
					end if
				end if
			
				if not isnull(ls_nota_dettaglio) and len(ls_nota_dettaglio) > 0 then
					ls_nota_dettaglio = ls_label_des_specifica + ls_des_specifica + "~r~n" + ls_nota_dettaglio
				else
					ls_nota_dettaglio = ls_label_des_specifica + ls_des_specifica
				end if
			end if
			
			// EnMe 01-08-2017 nuova gestione nota dettaglio prodotto
			if not isnull(ls_cod_prodotto) then
				if guo_functions.uof_get_note_fisse(ls_null, ls_null, ls_cod_prodotto, "STAMPA_ORD_ACQ", ls_null, ldt_data_registrazione, ref ls_stampa_nota_testata, ref ls_stampa_nota_piede, ref ls_stampa_nota_video) < 0 then
					g_mb.error(ls_stampa_nota_testata)
				else
					if len(ls_stampa_nota_testata) > 0 then ls_nota_dettaglio = g_str.implode({ls_nota_dettaglio,ls_stampa_nota_testata},"~r~n")
					if len(ls_stampa_nota_piede) > 0 then ls_nota_dettaglio = g_str.implode({ls_nota_dettaglio,ls_stampa_nota_piede},"~r~n")
					if len(ls_stampa_nota_video) > 0 then g_mb.warning( "NOTA FISSA", ls_stampa_nota_video)
				end if
			end if				

			ls_visible = dw_report_ord_acq.Describe("det_ord_acq_cod_prod_fornitore.Visible")
			if not isnull(ls_cod_prod_fornitore) and not isnull(ls_nota_dettaglio) then
				if ls_visible <> "1" then ls_nota_dettaglio = "Vs.Codice : " + ls_cod_prod_fornitore + "~r~n" + ls_nota_dettaglio
			elseif not isnull(ls_cod_prod_fornitore) and isnull(ls_nota_dettaglio) then
				if ls_visible <> "1" then ls_nota_dettaglio = "Vs.Codice: " + ls_cod_prod_fornitore
			elseif isnull(ls_cod_prod_fornitore) and not isnull(ls_nota_dettaglio) then			
				ls_nota_dettaglio = ls_nota_dettaglio			
			elseif (isnull(ls_cod_prod_fornitore) or ls_cod_prod_fornitore = "") and (isnull(ls_nota_dettaglio) or ls_nota_dettaglio = "") then	
				setnull(ls_nota_dettaglio)
			end if	
			
			dw_report_ord_acq.setitem(ll_riga, "det_ord_acq_cod_prod_fornitore", ls_cod_prod_fornitore)
			
			dw_report_ord_acq.setitem(ll_riga, "det_ord_acq_nota_dettaglio", ls_nota_dettaglio)
			
			dw_report_ord_acq.setitem(ll_riga, "det_ord_acq_cod_prodotto", ls_cod_prodotto)
			// stefanop 09/07/2010: cambiate condizioni da OR a AND su richiesta di ENRICO
			if not isnull(ls_des_prodotto) AND len(ls_des_prodotto) > 0 then
				dw_report_ord_acq.setitem(ll_riga, "det_ord_acq_des_prodotto", ls_des_prodotto)
			elseif not isnull(ls_des_prodotto_fornitore) AND len(ls_des_prodotto_fornitore) > 0 then
				dw_report_ord_acq.setitem(ll_riga, "det_ord_acq_des_prodotto", ls_des_prodotto_fornitore)
			elseif not isnull(ls_des_prodotto_lingua) AND len(ls_des_prodotto_lingua) > 0 then
				dw_report_ord_acq.setitem(ll_riga, "det_ord_acq_des_prodotto", ls_des_prodotto_lingua)
			elseif not isnull(ls_des_prodotto_anag) AND len(ls_des_prodotto_anag) > 0 then
				dw_report_ord_acq.setitem(ll_riga, "det_ord_acq_des_prodotto", ls_des_prodotto_anag)
			end if
			// ----
		end if
		
		select des_operatore 
		  into :ls_des_operatore
		  from tab_operatori
		 where cod_azienda   = :s_cs_xx.cod_azienda
			and cod_operatore = :ls_cod_operatore;
			
		if not isnull(ls_telefono_deposito_operatore) and len(ls_telefono_deposito_operatore) > 0 then
			ls_des_operatore += "~r~nTel: " + ls_telefono_deposito_operatore
		end if
			
		if sqlca.sqlcode <> 0 then setnull(ls_cod_operatore)
		dw_report_ord_acq.setitem(ll_riga, "tab_operatori_des_operatore", ls_des_operatore)
		dw_report_ord_acq.setitem(ll_riga, "tab_porti_des_porto", ls_des_porto)
		dw_report_ord_acq.setitem(ll_riga, "tab_porti_lingue_des_porto", ls_des_porto_lingua)
		dw_report_ord_acq.setitem(ll_riga, "tab_rese_des_resa", ls_des_resa)
		dw_report_ord_acq.setitem(ll_riga, "tab_rese_lingue_des_resa", ls_des_resa_lingua)
		dw_report_ord_acq.setitem(ll_riga, "tab_mezzi_des_mezzo", ls_des_mezzo)
		dw_report_ord_acq.setitem(ll_riga, "tab_mezzi_lingue_des_mezzo", ls_des_mezzo_lingua)
		dw_report_ord_acq.setitem(ll_riga, "cod_vettore", ls_cod_vettore)
		dw_report_ord_acq.setitem(ll_riga, "des_vettore", ls_des_vettore)
	end if
loop

close cu_dettagli;

//dw_report_ord_acq.reset_dw_modified(c_resetchildren)
dw_report_ord_acq.setredraw(true)
title = is_title + " - Quan.Ordinata: " + string(ld_quan_totale)
return 0
end function

public function integer wf_imposta_barcode ();string ls_bfo, ls_errore
int li_risposta, li_bco

li_risposta = f_font_barcode(ls_bfo,li_bco,ls_errore)
                              
if li_risposta < 0 then
	g_mb.error("Controllare parametri barcode." + ls_errore)
	return -1
end if
                              
dw_report_ord_acq.Modify("cf_barcode.Font.Face='" + ls_bfo + "'")
dw_report_ord_acq.Modify("cf_barcode.Font.Height= -" + string(li_bco) )
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo_1, ls_path_logo_2, ls_modify, ls_path_firma,ls_cod_oggetto

s_report_ordine ls_report_ordine

//pcca.window_current=this

ls_report_ordine = message.powerobjectparm
ib_stampa_residuo = ls_report_ordine.ab_flag_residuo
il_anno_registrazione = ls_report_ordine.al_anno_registrazione
il_num_registrazione = ls_report_ordine.al_num_registrazione

title += " - " + string(il_anno_registrazione) + "/" + string(il_num_registrazione)
dw_report_ord_acq.set_document_name("Ordine di Acquisto " + string(il_anno_registrazione) + "/" + string(il_num_registrazione))

set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_NoEnablePopup)

dw_report_ord_acq.ib_dw_report=true
dw_report_ord_acq.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
                                          c_disablecc, &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)

iuo_dw_main = dw_report_ord_acq
								
select parametri_azienda.stringa
into   :ls_path_logo_1
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO1';

select parametri_azienda.stringa
into   :ls_path_logo_2
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO2';

select parametri_azienda.stringa
into   :ls_cod_oggetto
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'FR1';

if sqlca.sqlcode = 0 then
	select path_oggetto
	into   :ls_path_firma
	from   utenti_oggetti
	where  cod_azienda = :s_cs_xx.cod_azienda and
   	    cod_utente = :s_cs_xx.cod_utente and 
      	 cod_oggetto = :ls_cod_oggetto;

	ls_modify = "firma.filename='" + ls_path_firma + "'~t"
	dw_report_ord_acq.modify(ls_modify)
end if

ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
dw_report_ord_acq.modify(ls_modify)

ls_modify = "piede.filename='" + s_cs_xx.volume + ls_path_logo_2 + "'~t"
dw_report_ord_acq.modify(ls_modify)

//ib_stampa_residuo = s_cs_xx.parametri.parametro_b_1
if ib_stampa_residuo then
	g_mb.messagebox("APICE","Durante la stampa del residuo ordine non verrà visualizzato il valore riga",Information!)
end if

postevent("ue_setfocus")

is_title = title


end event

on w_report_ord_acq_euro.create
int iCurrent
call super::create
this.dw_report_ord_acq=create dw_report_ord_acq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_ord_acq
end on

on w_report_ord_acq_euro.destroy
call super::destroy
destroy(this.dw_report_ord_acq)
end on

type dw_report_ord_acq from uo_cs_xx_dw within w_report_ord_acq_euro
event ue_anteprima_pdf pbm_custom02
integer x = 23
integer y = 20
integer width = 3657
integer height = 4400
integer taborder = 20
string dataobject = "d_report_ord_acq_euro"
boolean border = false
end type

event ue_anteprima_pdf;call super::ue_anteprima_pdf;////
////		OVERRIDE ANCESTOR SCRIPT
////
//long ll_return, ll_elemento, ll_anno, ll_num
//string  ls_messaggio, ls_path
//
//oleobject iole_outlook, iole_item, iole_attach
//
//ll_elemento = 0
//
//setpointer(hourglass!)
//
//s_cs_xx.parametri.parametro_dw_1 = this
////if (pcca.window_current.title) then
//s_cs_xx.parametri.parametro_s_1 = pcca.window_current.title
////end if
//
//uo_archivia_pdf luo_pdf
//luo_pdf = create uo_archivia_pdf
//
//// stefanop 26/07/2010: cambio il nome di default: richiesto da BEATRICE: ticket 2010/213
//ll_anno = getitemnumber(1, "tes_ord_acq_anno_registrazione")
//ll_num = getitemnumber(1, "tes_ord_acq_num_registrazione")
//ls_path = luo_pdf.uof_crea_pdf_path(this, "Ord Acquisto " + string(ll_anno)+"-"+ string(ll_num))
//
////ls_path = luo_pdf.uof_crea_pdf_path(this)
//// ----
//
//if ls_path = "errore" then
//	g_mb.messagebox ("Errore","Errore nella creazione del file pdf.")
//end if
//
////invio pdf
//iole_outlook = create oleobject
//
//ll_return = iole_outlook.connecttonewobject("outlook.application")
//
//if ll_return <> 0 THEN
//	ls_messaggio = "Impossibile connettersi ad Outlook. Codice errore: " + string(ll_return)
//	g_mb.messagebox ("Errore",ls_messaggio)
//	
//	destroy iole_outlook
//end if
//
//// Viene creato un nuovo elemento del tipo specificato
//
//iole_item = iole_outlook.createitem(ll_elemento)
////visualizza la mail da inviare
//iole_item.display
//
//iole_attach = iole_item.attachments
//	
//// Il file specificato viene allegato solo se esiste al percorso indicato
//		
//if fileexists(ls_path) then
//	iole_attach.add(ls_path)
//else
//	ls_messaggio = "allegato inesistente" + string(ll_return)
//	g_mb.messagebox ("Errore",ls_messaggio)
//	destroy iole_attach
//	destroy iole_outlook
//end if
//		
//destroy iole_item		
//destroy iole_attach
//filedelete(ls_path)
//
end event

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

