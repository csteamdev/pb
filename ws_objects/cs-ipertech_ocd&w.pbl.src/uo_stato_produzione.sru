﻿$PBExportHeader$uo_stato_produzione.sru
forward
global type uo_stato_produzione from userobject
end type
type r_1 from rectangle within uo_stato_produzione
end type
type st_stato from statictext within uo_stato_produzione
end type
type st_stato_label from statictext within uo_stato_produzione
end type
type ov_stato_v from oval within uo_stato_produzione
end type
type ov_stato_g from oval within uo_stato_produzione
end type
type ov_stato_r from oval within uo_stato_produzione
end type
end forward

global type uo_stato_produzione from userobject
integer width = 1518
integer height = 92
long backcolor = 12632256
string text = "none"
long tabtextcolor = 33554432
long tabbackcolor = 1073741824
long picturemaskcolor = 536870912
r_1 r_1
st_stato st_stato
st_stato_label st_stato_label
ov_stato_v ov_stato_v
ov_stato_g ov_stato_g
ov_stato_r ov_stato_r
end type
global uo_stato_produzione uo_stato_produzione

type variables
/*
1.	silver = 12632256
2.	red = 255
3.	green = 65408
4.	yellow = 65535
*/
public long state_colors[] = {	12632256, &
										255, &	
										65408, &
										65535}

public string state_text[] = {	"nessun lancio produzione", &
										"NIENTE", &
										"TUTTO", &
										"PARZIALE"}

//stato colori spento (Rosso, Giallo, Verde)
private long state_colors_off[] = {		8421504, &
												8421504, &
												8421504}
										
public long index_background = 1

end variables

forward prototypes
public function integer uof_set_color (long fl_stato)
public subroutine uof_spegni_semaforo ()
end prototypes

public function integer uof_set_color (long fl_stato);
if fl_stato > upperbound(state_colors) then
	//semaforo spento
	fl_stato = 1
end if

/*
 1		spento
 2		rosso
 3		verde
 4		giallo

*/

choose case fl_stato
	case 1		//spento
		uof_spegni_semaforo()
		
	case 2		//rosso
		ov_stato_r.fillcolor = state_colors[fl_stato]
		ov_stato_g.fillcolor = state_colors_off[2]
		ov_stato_v.fillcolor = state_colors_off[3]
	
	case 3		//verde
		ov_stato_r.fillcolor = state_colors_off[1]
		ov_stato_g.fillcolor = state_colors_off[3]
		ov_stato_v.fillcolor = state_colors[fl_stato]
		
	case 4		//giallo
		ov_stato_r.fillcolor = state_colors_off[1]
		ov_stato_g.fillcolor = state_colors[fl_stato]
		ov_stato_v.fillcolor = state_colors_off[2]
		
	case else
		uof_spegni_semaforo()
		
end choose

st_stato.text = state_text[fl_stato]

return 1
end function

public subroutine uof_spegni_semaforo ();ov_stato_r.fillcolor = state_colors_off[1]
ov_stato_g.fillcolor = state_colors_off[2]
ov_stato_v.fillcolor = state_colors_off[3]
end subroutine

on uo_stato_produzione.create
this.r_1=create r_1
this.st_stato=create st_stato
this.st_stato_label=create st_stato_label
this.ov_stato_v=create ov_stato_v
this.ov_stato_g=create ov_stato_g
this.ov_stato_r=create ov_stato_r
this.Control[]={this.r_1,&
this.st_stato,&
this.st_stato_label,&
this.ov_stato_v,&
this.ov_stato_g,&
this.ov_stato_r}
end on

on uo_stato_produzione.destroy
destroy(this.r_1)
destroy(this.st_stato)
destroy(this.st_stato_label)
destroy(this.ov_stato_v)
destroy(this.ov_stato_g)
destroy(this.ov_stato_r)
end on

event constructor;this.backcolor = state_colors[index_background]

st_stato_label.backcolor = state_colors[index_background]

uof_spegni_semaforo()




end event

type r_1 from rectangle within uo_stato_produzione
integer linethickness = 4
long fillcolor = 33554432
integer x = 544
integer y = 16
integer width = 169
integer height = 64
end type

type st_stato from statictext within uo_stato_produzione
integer x = 736
integer y = 12
integer width = 763
integer height = 72
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_stato_label from statictext within uo_stato_produzione
integer x = 32
integer y = 12
integer width = 517
integer height = 72
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Stato Produzione:"
boolean focusrectangle = false
end type

type ov_stato_v from oval within uo_stato_produzione
integer linethickness = 5
long fillcolor = 1073741824
integer x = 654
integer y = 24
integer width = 50
integer height = 45
end type

type ov_stato_g from oval within uo_stato_produzione
integer linethickness = 5
long fillcolor = 1073741824
integer x = 603
integer y = 24
integer width = 50
integer height = 44
end type

type ov_stato_r from oval within uo_stato_produzione
integer linethickness = 5
long fillcolor = 1073741824
integer x = 553
integer y = 24
integer width = 50
integer height = 45
end type

