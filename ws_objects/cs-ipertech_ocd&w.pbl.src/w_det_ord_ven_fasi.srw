﻿$PBExportHeader$w_det_ord_ven_fasi.srw
forward
global type w_det_ord_ven_fasi from w_cs_xx_principale
end type
type st_2 from statictext within w_det_ord_ven_fasi
end type
type dw_sospensioni from datawindow within w_det_ord_ven_fasi
end type
type cb_sessioni from commandbutton within w_det_ord_ven_fasi
end type
type dw_det_ord_ven_fasi from uo_cs_xx_dw within w_det_ord_ven_fasi
end type
type dw_det_ord_ven_fasi_comp from datawindow within w_det_ord_ven_fasi
end type
type dw_det_ord_ven_fasi_comp2 from datawindow within w_det_ord_ven_fasi
end type
type st_1 from statictext within w_det_ord_ven_fasi
end type
end forward

global type w_det_ord_ven_fasi from w_cs_xx_principale
integer width = 4018
integer height = 1828
string title = "Fasi - Ordine"
boolean maxbox = false
boolean resizable = false
st_2 st_2
dw_sospensioni dw_sospensioni
cb_sessioni cb_sessioni
dw_det_ord_ven_fasi dw_det_ord_ven_fasi
dw_det_ord_ven_fasi_comp dw_det_ord_ven_fasi_comp
dw_det_ord_ven_fasi_comp2 dw_det_ord_ven_fasi_comp2
st_1 st_1
end type
global w_det_ord_ven_fasi w_det_ord_ven_fasi

type variables
long il_anno, il_num, il_riga
boolean ib_abilita_reset=false
end variables

forward prototypes
public function integer wf_reset_produzione_fase (ref string fs_errore)
end prototypes

public function integer wf_reset_produzione_fase (ref string fs_errore);long				ll_barcode, ll_riga
string				ls_cod_tipo_ord_ven, ls_tipo_stampa, ls_errore
integer			li_ret
uo_produzione luo_produzione

fs_errore = ""
if not ib_abilita_reset then return 0

ll_riga = dw_det_ord_ven_fasi.getrow()
if ll_riga>0 then
else
	return 0
end if

select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:il_anno
and    num_registrazione=:il_num;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore lettura tipo ordine: " + sqlca.sqlerrtext
	return -1
end if

select flag_tipo_bcl
into   :ls_tipo_stampa
from   tab_tipi_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore in lettura flag_tipo_bcl " + sqlca.sqlerrtext
	return -1
end if

choose case ls_tipo_stampa
	case "A"
		ll_barcode = dw_det_ord_ven_fasi.getitemnumber(ll_riga, "progr_det_produzione")
		
	case "B","C"
		ll_barcode = dw_det_ord_ven_fasi.getitemnumber(ll_riga, "progr_tes_produzione")
		
	case else
		fs_errore = "Tipo stampa ordine '"+ls_tipo_stampa+"' non previsto!"
		return -1
end choose

if ll_barcode>0 then
else
	return 0
end if

if g_mb.confirm("Eseguire il reset della fase di produzione per il barcode "+string(ll_barcode)) then
else
	fs_errore = "Operazione annullta dall'utente!"
	return 0
end if

luo_produzione = create uo_produzione
li_ret = luo_produzione.uof_reset_produzione(ll_barcode, fs_errore)
destroy luo_produzione

return li_ret
end function

event pc_setwindow;call super::pc_setwindow;
string ls_flag_supervisore

iuo_dw_main = dw_det_ord_ven_fasi

ib_abilita_reset = false

dw_det_ord_ven_fasi.change_dw_current()
dw_det_ord_ven_fasi.set_dw_key("cod_azienda")
dw_det_ord_ven_fasi.set_dw_key("progr_det_produzione_azienda")
dw_det_ord_ven_fasi.set_dw_options(sqlca, &
											  i_openparm, &
											  c_nonew + c_nomodify + c_nodelete + c_scrollparent, &
											  c_default)

if s_cs_xx.cod_utente<>"CS_SYSTEM" then
	select flag_supervisore
	into :ls_flag_supervisore
	from utenti
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente;
			
	if ls_flag_supervisore="S" then ib_abilita_reset = true
else
	ib_abilita_reset = true
end if

dw_det_ord_ven_fasi.object.b_reset_prod.visible = ib_abilita_reset
dw_sospensioni.object.b_cancella.visible = ib_abilita_reset

end event

on w_det_ord_ven_fasi.create
int iCurrent
call super::create
this.st_2=create st_2
this.dw_sospensioni=create dw_sospensioni
this.cb_sessioni=create cb_sessioni
this.dw_det_ord_ven_fasi=create dw_det_ord_ven_fasi
this.dw_det_ord_ven_fasi_comp=create dw_det_ord_ven_fasi_comp
this.dw_det_ord_ven_fasi_comp2=create dw_det_ord_ven_fasi_comp2
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_2
this.Control[iCurrent+2]=this.dw_sospensioni
this.Control[iCurrent+3]=this.cb_sessioni
this.Control[iCurrent+4]=this.dw_det_ord_ven_fasi
this.Control[iCurrent+5]=this.dw_det_ord_ven_fasi_comp
this.Control[iCurrent+6]=this.dw_det_ord_ven_fasi_comp2
this.Control[iCurrent+7]=this.st_1
end on

on w_det_ord_ven_fasi.destroy
call super::destroy
destroy(this.st_2)
destroy(this.dw_sospensioni)
destroy(this.cb_sessioni)
destroy(this.dw_det_ord_ven_fasi)
destroy(this.dw_det_ord_ven_fasi_comp)
destroy(this.dw_det_ord_ven_fasi_comp2)
destroy(this.st_1)
end on

type st_2 from statictext within w_det_ord_ven_fasi
integer x = 23
integer y = 568
integer width = 1166
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Sospensioni riga ordine:"
boolean focusrectangle = false
end type

type dw_sospensioni from datawindow within w_det_ord_ven_fasi
integer x = 23
integer y = 636
integer width = 3950
integer height = 512
integer taborder = 20
string title = "none"
string dataobject = "d_det_ord_ven_sospensioni"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event buttonclicked;string					ls_msg, ls_cod_reparto
long					ll_barcode, ll_progressivo

if row>0 then
else
	return
end if

choose case dwo.name
	case "b_cancella"
		
		ll_barcode = dw_sospensioni.getitemnumber(row, "barcode")
		ll_progressivo = dw_sospensioni.getitemnumber(row, "progressivo")
		
		ls_cod_reparto = dw_sospensioni.object.cf_reparto[row]
		if ls_cod_reparto<>"" and not isnull(ls_cod_reparto) then
			ls_msg = "Eliminare la sospensione selezionata del reparto "+ls_cod_reparto+"?"
		else
			ls_msg = "Eliminare la sospensione selezionata?"
		end if
		
		if g_mb.confirm(ls_msg) then
		
			delete from tab_sosp_produzione
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						barcode=:ll_barcode and
						progressivo=:ll_progressivo;
						
			if sqlca.sqlcode<0 then
				rollback;
				g_mb.error("Errore in cancellazione sospensione: "+sqlca.sqlerrtext)
				return
			else
				//tutto ok, fai commit
				commit;
				g_mb.success("Cancellazione sospensione avvenuta con successo!")
				
				//riscatena la retrieve ....
				dw_sospensioni.retrieve(s_cs_xx.cod_azienda, il_anno,il_num, il_riga)
				
				return
			end if
			
		end if
		
end choose
end event

type cb_sessioni from commandbutton within w_det_ord_ven_fasi
integer x = 3607
integer y = 1656
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sessioni"
end type

event clicked;long ll_row, ll_prog


ll_row = dw_det_ord_ven_fasi.getrow()

if isnull(ll_row) or ll_row = 0 then
	g_mb.messagebox("APICE","Selezionare una fase prima di continuare!",stopsign!)
	return -1
end if

ll_prog = dw_det_ord_ven_fasi.getitemnumber(ll_row,"progr_det_produzione")

if isnull(ll_prog) or ll_prog = 0 then
	g_mb.messagebox("APICE","Selezionare una fase prima di continuare!",stopsign!)
	return -1
end if

window_open_parm(w_det_ord_ven_sessioni,-1,dw_det_ord_ven_fasi)
end event

type dw_det_ord_ven_fasi from uo_cs_xx_dw within w_det_ord_ven_fasi
integer x = 23
integer y = 20
integer width = 3950
integer height = 516
integer taborder = 10
string dataobject = "d_det_ord_ven_fasi"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;il_anno = i_parentdw.getitemnumber(i_parentdw.getrow(),"anno_registrazione")

il_num = i_parentdw.getitemnumber(i_parentdw.getrow(),"num_registrazione")

il_riga = i_parentdw.getitemnumber(i_parentdw.getrow(),"prog_riga_ord_ven")

parent.title = "Fasi - Ordine " + string(il_anno) + "/" + string(il_num) + " Riga " + string(il_riga)

retrieve(s_cs_xx.cod_azienda,il_anno,il_num,il_riga)



dw_sospensioni.retrieve(s_cs_xx.cod_azienda, il_anno,il_num,il_riga)
end event

event rowfocuschanged;call super::rowfocuschanged;long ll_prog_tes_produzione, ll_prog_det_produzione

//Donato 09/05/2011
//per gli ordini tipo A progr_tes_produzione in det_ordini_produzione è NULL
//quindi questa cosa qui non ha mai funzionato

if not isnull(currentrow) and currentrow > 0 then
	
	ll_prog_tes_produzione = getitemnumber(currentrow,"progr_tes_produzione")
	ll_prog_det_produzione = getitemnumber(currentrow,"progr_det_produzione")
	
	if isnull(ll_prog_tes_produzione) or ll_prog_tes_produzione<=0 then
		ll_prog_tes_produzione = -1
		dw_det_ord_ven_fasi_comp.visible = false
		dw_det_ord_ven_fasi_comp2.visible = true
		
		st_1.text = "Sessioni:"
	else
		ll_prog_det_produzione = -1
		dw_det_ord_ven_fasi_comp.visible = true
		dw_det_ord_ven_fasi_comp2.visible = false
		
		st_1.text = "Stato avanzamento complessivo del reparto:"
	end if
	
	dw_det_ord_ven_fasi_comp.retrieve(s_cs_xx.cod_azienda,ll_prog_tes_produzione)
	dw_det_ord_ven_fasi_comp2.retrieve(s_cs_xx.cod_azienda,ll_prog_det_produzione)
end if

//commento il codice originario

//if not isnull(currentrow) and currentrow > 0 then
//	dw_det_ord_ven_fasi_comp.retrieve(s_cs_xx.cod_azienda,getitemnumber(currentrow,"progr_tes_produzione"))
//end if

//fine modifica del cazzo -------------




























end event

event buttonclicked;call super::buttonclicked;string ls_errore
integer li_ret

if row>0 then
else
	return
end if

choose case dwo.name
	case "b_reset_prod"
		li_ret = wf_reset_produzione_fase(ls_errore)
		
		if li_ret<0 then
			//rollback + messaggio di errore
			rollback;
			g_mb.error(ls_errore)
			return
		else
			//tutto ok, fai commit e se c'è qualche messaggio lo dai
			commit;
			
			if ls_errore<>"" and not isnull(ls_errore) then
				g_mb.success(ls_errore)
			end if
			
			//riscatena la retrieve ....
			dw_det_ord_ven_fasi.triggerevent("pcd_retrieve")
			
			//riseleziona la prima riga
			this.event trigger rowfocuschanged(1)
			
			return
			
		end if
		
end choose
end event

event doubleclicked;call super::doubleclicked;long		l_x, l_y

if row>0 then
	choose case dwo.name
		case "progr_det_produzione","progr_tes_produzione"
			l_x=getitemnumber(row, "progr_tes_produzione")
			l_y=getitemnumber(row, "progr_det_produzione")
			messagebox("",f_get_posizione_colli( l_x, l_y))
	end choose
end if
end event

type dw_det_ord_ven_fasi_comp from datawindow within w_det_ord_ven_fasi
integer x = 23
integer y = 1256
integer width = 3950
integer height = 384
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_det_ord_ven_fasi_comp"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;dw_det_ord_ven_fasi_comp.settransobject(sqlca)
dw_sospensioni.settransobject(sqlca)
end event

type dw_det_ord_ven_fasi_comp2 from datawindow within w_det_ord_ven_fasi
integer x = 23
integer y = 1256
integer width = 3950
integer height = 384
integer taborder = 20
string dataobject = "d_det_ord_ven_sessioni"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event constructor;dw_det_ord_ven_fasi_comp2.settransobject(sqlca)
end event

type st_1 from statictext within w_det_ord_ven_fasi
integer x = 23
integer y = 1184
integer width = 1166
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Stato avanzamento complessivo del reparto:"
boolean focusrectangle = false
end type

