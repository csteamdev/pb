﻿$PBExportHeader$w_det_ord_ven_ologramma.srw
forward
global type w_det_ord_ven_ologramma from w_cs_xx_principale
end type
type cb_assegna from commandbutton within w_det_ord_ven_ologramma
end type
type dw_operaio from uo_dw_search within w_det_ord_ven_ologramma
end type
type st_1 from statictext within w_det_ord_ven_ologramma
end type
type rb_verifica from radiobutton within w_det_ord_ven_ologramma
end type
type rb_assegna_iniziale from radiobutton within w_det_ord_ven_ologramma
end type
type dw_det_ord_ven_ologramma from uo_cs_xx_dw within w_det_ord_ven_ologramma
end type
end forward

global type w_det_ord_ven_ologramma from w_cs_xx_principale
integer width = 4105
integer height = 1736
string title = "Ologrammi Riga"
boolean maxbox = false
boolean resizable = false
cb_assegna cb_assegna
dw_operaio dw_operaio
st_1 st_1
rb_verifica rb_verifica
rb_assegna_iniziale rb_assegna_iniziale
dw_det_ord_ven_ologramma dw_det_ord_ven_ologramma
end type
global w_det_ord_ven_ologramma w_det_ord_ven_ologramma

type variables
long il_anno, il_num, il_riga
boolean ib_abilita_gestione=false
end variables

forward prototypes
public function integer wf_prima_assegnazione (string as_cod_operaio, ref string as_errore)
public function integer wf_verifica_assegnazione ()
end prototypes

public function integer wf_prima_assegnazione (string as_cod_operaio, ref string as_errore);long			ll_barcode
string			ls_cod_operaio
integer		li_risposta
uo_produzione	luo_prod
		
setnull(ll_barcode)

luo_prod = create uo_produzione
li_risposta = luo_prod.uof_ass_iniziale_ologramma(true, ll_barcode, il_anno, il_num, il_riga, as_cod_operaio, as_errore)
destroy luo_prod

choose case li_risposta
	case is <0
		//in as_errore il messaggio
		return -1
		
	case 1, 2
		//1: assegnazione non prevista OPPURE assegnazione ologrammi prevista ma è stata già effettuata la prima assegnazione
		//2: probabile annullamento da parte dell'utente (in tal caso non dare messaggi, ci ha già pensato la finestra di gestione ologrammi); 
		return li_risposta
		
	case else
		//assegnazione iniziale effettuata con successo: fare COMMIT
		return 0
		
end choose


end function

public function integer wf_verifica_assegnazione ();//
//
////#############################################################
////Donato 08/10/2012 controlla se devo fare la verifica/assegnazione ologrammi, 
////ma solo se ho indicato una quantità prodotta > 0
//
//if ld_quan_prodotta>0 then
//
//	li_risposta = uof_trova_tipo(fl_barcode,&
//										  ls_tipo_stampa,&
//										  ls_errore)
//	if ls_tipo_stampa="A" then
//		
//		li_risposta = uof_controlla_ologrammi(false, fl_barcode, li_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, fs_errore)
//		if li_risposta = -1 then
//			//in fs_errore il messaggio
//			return -1
//			
//		elseif li_risposta=-2 then
//			//assegnazione non prevista, vai avanti
//			
//	//	elseif li_risposta > 0 then
//	//		//assegnazione ologrammi prevista ma è stata già effettuata la prima assegnazione
//			
//		else
//			
//			//solo se ci sono ancora ologrammi da verificare -----------
//			select count(*)
//			into :li_risposta
//			from det_ord_ven_ologramma
//			where 	cod_azienda=:s_cs_xx.cod_azienda and
//						anno_registrazione=:li_anno_registrazione and
//						num_registrazione=:ll_num_registrazione and
//						prog_riga_ord_ven=:ll_prog_riga_ord_ven and
//						flag_verificato<>'S';
//			//----------------------------------------------------------------
//			
//			if sqlca.sqlcode<0 then
//				fs_errore = "Errore lettura ologrammi da verificare: "+sqlca.sqlerrtext
//				return -1
//				
//			elseif sqlca.sqlcode=100 or isnull(li_risposta) or li_risposta<=0 then
//				//o sono tutti a S oppure la tabella è vuota
//				
//				//c'è da fare l'assegnazione iniziale? (tabella vuota)
//				select count(*)
//				into :li_risposta
//				from det_ord_ven_ologramma
//				where 	cod_azienda=:s_cs_xx.cod_azienda and
//							anno_registrazione=:li_anno_registrazione and
//							num_registrazione=:ll_num_registrazione and
//							prog_riga_ord_ven=:ll_prog_riga_ord_ven;
//				//----------------------------------------------------------------
//				if li_risposta>0 then
//					//Non risultano Ologrammi da verificare (tutti ad S) e l'assegnazione iniziale è stata anche fatta!,
//					
//				else
//					//=(tabella ologrammi vuota)
//					//Non risultano Ologrammi da verificare in quanto la tabella è vuota
//					//pertanto procedi all'assegnazione iniziale + verifica contemporanea
//					lb_procedi = true
//					
//				end if
//		
//			else
//				//ci sono ologrammi da verificare (o sono tutti da verificare o solo parte di essi), procedi alla verifica
//				lb_procedi = true
//			end if
//			
//			
//			//assegnazione ologrammi prevista e prima assegnazione NON ancora effettuata
//			//OPPURE assegnazione ologrammi prevista ma è stata già effettuata la prima assegnazione
//			if lb_procedi then
//				//in ogni caso faccio una attività di verifica ----
//				lstr_parametri.parametro_s_1 = "V"
//				lstr_parametri.parametro_s_2 = "O"
//				lstr_parametri.parametro_s_3 = fs_cod_operaio
//				lstr_parametri.parametro_d_1_a[1] = li_anno_registrazione
//				lstr_parametri.parametro_d_1_a[2] = ll_num_registrazione
//				lstr_parametri.parametro_d_1_a[3] = ll_prog_riga_ord_ven
//				
//				//devo assegnare un numero di ologrammi pari alla quantità prodotta
//				lstr_parametri.parametro_d_1_a[4] = ld_quan_prodotta
//				
//				openwithparm(w_gestione_ologrammi, lstr_parametri)
//				
//				lstr_parametri = message.powerobjectparm
//				//se hai terminatro il processo confermando l' assegnazione iniziale, il COMMIT alla fine confermerà tutto
//				//altrimenti, non avendo fatto alcun update, ci sarà un nulla di fatto, anche con il COMMIT finale
//				if lstr_parametri.parametro_b_1 then
//				else
//					fs_errore = "Operazione annullata dall'utente! Fase non chiusa!"
//					return -1
//				end if
//				
//				 lb_ologrammi = true
//			end if
//			
//		end if
//	end if
//end if
////#############################################################

return 0
end function

event pc_setwindow;call super::pc_setwindow;
string ls_flag_supervisore

iuo_dw_main = dw_det_ord_ven_ologramma

ib_abilita_gestione = false

dw_det_ord_ven_ologramma.change_dw_current()
dw_det_ord_ven_ologramma.set_dw_key("cod_azienda")
dw_det_ord_ven_ologramma.set_dw_key("cod_ologramma")


if s_cs_xx.cod_utente<>"CS_SYSTEM" then
	select flag_supervisore
	into :ls_flag_supervisore
	from utenti
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente;
			
	if ls_flag_supervisore="S" then ib_abilita_gestione = true
else
	ib_abilita_gestione = true
end if

if ib_abilita_gestione then
	dw_det_ord_ven_ologramma.set_dw_options(	sqlca, &
																i_openparm, &
																c_scrollparent, &
																c_default)
else
	dw_det_ord_ven_ologramma.set_dw_options(	sqlca, &
																i_openparm, &
																c_nonew + c_nomodify + c_nodelete + c_scrollparent, &
																c_default)
end if
end event

on w_det_ord_ven_ologramma.create
int iCurrent
call super::create
this.cb_assegna=create cb_assegna
this.dw_operaio=create dw_operaio
this.st_1=create st_1
this.rb_verifica=create rb_verifica
this.rb_assegna_iniziale=create rb_assegna_iniziale
this.dw_det_ord_ven_ologramma=create dw_det_ord_ven_ologramma
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_assegna
this.Control[iCurrent+2]=this.dw_operaio
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.rb_verifica
this.Control[iCurrent+5]=this.rb_assegna_iniziale
this.Control[iCurrent+6]=this.dw_det_ord_ven_ologramma
end on

on w_det_ord_ven_ologramma.destroy
call super::destroy
destroy(this.cb_assegna)
destroy(this.dw_operaio)
destroy(this.st_1)
destroy(this.rb_verifica)
destroy(this.rb_assegna_iniziale)
destroy(this.dw_det_ord_ven_ologramma)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det_ord_ven_ologramma, &
                			 "operaio_assegnazione", &
                			sqlca, &
                			"anag_operai", &
                 			"cod_operaio", &
                 			"nome + ' ' + cognome", &
                 			"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
								  
f_po_loaddddw_dw(dw_det_ord_ven_ologramma, &
                			 "operaio_verifica", &
                			sqlca, &
                			"anag_operai", &
                 			"cod_operaio", &
                 			"nome + ' ' + cognome", &
                 			"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
								  
f_po_loaddddw_dw(dw_operaio, &
                 "cod_operaio", &
                 sqlca, &
                 "anag_operai", &
                 "cod_operaio", &
                 "cognome + ' ' + nome", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
end event

type cb_assegna from commandbutton within w_det_ord_ven_ologramma
integer x = 2597
integer y = 32
integer width = 370
integer height = 84
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Assegna"
end type

event clicked;string ls_errore, ls_cod_operaio
integer li_ret

dw_operaio.accepttext()

ls_cod_operaio = dw_operaio.getitemstring(dw_operaio.getrow(), "cod_operaio")

if ls_cod_operaio <>"" and not isnull(ls_cod_operaio) then
	li_ret = wf_prima_assegnazione(ls_cod_operaio, ls_errore)

	if li_ret < 0 then
		rollback;
		g_mb.error(ls_errore)
		
	elseif li_ret = 1 then
		rollback;
		g_mb.warning(ls_errore)
	
	elseif li_ret = 2 then
		//non dare messaggi, ci ha già pensato la finestra gestione ologrammi
		rollback;
		//g_mb.warning(ls_errore)
		
	else
		//0:  tutto OK
		commit;
		g_mb.success("Assegnazione Iniziale effettuata con successo!")
		
		//esegui retrieve
		dw_det_ord_ven_ologramma.postevent("pcd_retrieve")
	end if
	
	
end if

end event

type dw_operaio from uo_dw_search within w_det_ord_ven_ologramma
integer x = 41
integer y = 24
integer width = 2606
integer height = 140
integer taborder = 20
string dataobject = "d_operaio_assegna_ologramma"
boolean border = false
end type

type st_1 from statictext within w_det_ord_ven_ologramma
boolean visible = false
integer x = 2423
integer y = 36
integer width = 1641
integer height = 136
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 255
string text = "La Verifica non chiude nessuna fase di produzione (Utilizzare per operazioni di sostituzione ologrammi da verificare)"
boolean focusrectangle = false
end type

type rb_verifica from radiobutton within w_det_ord_ven_ologramma
boolean visible = false
integer x = 2053
integer y = 96
integer width = 1687
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 255
string text = "Verifica (solo per gli ologrammi NON ancora verificati!)"
end type

type rb_assegna_iniziale from radiobutton within w_det_ord_ven_ologramma
boolean visible = false
integer x = 2057
integer y = 28
integer width = 727
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 255
string text = "Assegnazione Iniziale"
boolean checked = true
end type

type dw_det_ord_ven_ologramma from uo_cs_xx_dw within w_det_ord_ven_ologramma
integer x = 23
integer y = 180
integer width = 4046
integer height = 1448
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_det_ord_ven_ologramma"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;
il_anno = i_parentdw.getitemnumber(i_parentdw.getrow(),"anno_registrazione")
il_num = i_parentdw.getitemnumber(i_parentdw.getrow(),"num_registrazione")
il_riga = i_parentdw.getitemnumber(i_parentdw.getrow(),"prog_riga_ord_ven")

parent.title = "Ologrammi - Riga Ordine " + string(il_anno) + "/" + string(il_num) + "/" + string(il_riga)

retrieve(s_cs_xx.cod_azienda,il_anno,il_num,il_riga)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_riga, ll_progressivo


ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
ll_riga = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "prog_riga_ord_ven")

for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i, "cod_azienda")) then
		setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if isnull(getitemnumber(ll_i, "anno_registrazione")) or getitemnumber(ll_i, "anno_registrazione") = 0 then
		setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
	end if
	
	if isnull(getitemnumber(ll_i, "num_registrazione")) or getitemnumber(ll_i, "num_registrazione") = 0 then
		setitem(ll_i, "num_registrazione", ll_num_registrazione)
	end if
	
	if isnull(getitemnumber(ll_i, "prog_riga_ord_ven")) or getitemnumber(ll_i, "prog_riga_ord_ven") = 0 then
		setitem(ll_i, "prog_riga_ord_ven", ll_riga)
	end if
	
	ll_progressivo = getitemnumber(ll_i, "progressivo")
	if isnull(ll_progressivo) or ll_progressivo<=0 then
		select max(progressivo)
		into :ll_progressivo
		from det_ord_ven_ologramma
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ll_anno_registrazione and
					num_registrazione=:ll_num_registrazione and
					prog_riga_ord_ven=:ll_riga;
		
		if isnull(ll_progressivo) then ll_progressivo=0
		ll_progressivo += 1
		
		setitem(ll_i, "progressivo", ll_progressivo)
		
	end if
	
next

end event

