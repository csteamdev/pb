﻿$PBExportHeader$w_det_ord_ven_sessioni.srw
forward
global type w_det_ord_ven_sessioni from w_cs_xx_principale
end type
type dw_det_ord_ven_sessioni from uo_cs_xx_dw within w_det_ord_ven_sessioni
end type
end forward

global type w_det_ord_ven_sessioni from w_cs_xx_principale
integer width = 3310
integer height = 1428
string title = "Sessioni - Fase"
boolean maxbox = false
boolean resizable = false
dw_det_ord_ven_sessioni dw_det_ord_ven_sessioni
end type
global w_det_ord_ven_sessioni w_det_ord_ven_sessioni

type variables
long il_prog
end variables

event pc_setwindow;call super::pc_setwindow;iuo_dw_main = dw_det_ord_ven_sessioni

dw_det_ord_ven_sessioni.change_dw_current()

dw_det_ord_ven_sessioni.set_dw_options(sqlca, &
											  i_openparm, &
											  c_nonew + c_nomodify + c_nodelete + c_scrollparent, &
											  c_default)
end event

on w_det_ord_ven_sessioni.create
int iCurrent
call super::create
this.dw_det_ord_ven_sessioni=create dw_det_ord_ven_sessioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_ord_ven_sessioni
end on

on w_det_ord_ven_sessioni.destroy
call super::destroy
destroy(this.dw_det_ord_ven_sessioni)
end on

type dw_det_ord_ven_sessioni from uo_cs_xx_dw within w_det_ord_ven_sessioni
integer x = 23
integer y = 20
integer width = 3246
integer height = 1300
integer taborder = 10
string dataobject = "d_det_ord_ven_sessioni"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_reparto

long 	 ll_anno, ll_num, ll_riga


ll_anno = i_parentdw.getitemnumber(i_parentdw.getrow(),"anno_registrazione")

ll_num = i_parentdw.getitemnumber(i_parentdw.getrow(),"num_registrazione")

ll_riga = i_parentdw.getitemnumber(i_parentdw.getrow(),"prog_riga_ord_ven")

ls_reparto = i_parentdw.getitemstring(i_parentdw.getrow(),"cod_reparto")

parent.title = "Sessioni - Ordine " + string(ll_anno) + "/" + string(ll_num) + " Riga " + string(ll_riga) + " Reparto " + ls_reparto

il_prog = i_parentdw.getitemnumber(i_parentdw.getrow(),"progr_det_produzione")

retrieve(s_cs_xx.cod_azienda,il_prog)
end event

