﻿$PBExportHeader$w_det_ord_ven_stato.srw
forward
global type w_det_ord_ven_stato from w_cs_xx_principale
end type
type dw_documenti from datawindow within w_det_ord_ven_stato
end type
type cb_fasi from commandbutton within w_det_ord_ven_stato
end type
type st_stato_produzione from statictext within w_det_ord_ven_stato
end type
type st_1 from statictext within w_det_ord_ven_stato
end type
type dw_det_ord_ven_stato from uo_cs_xx_dw within w_det_ord_ven_stato
end type
end forward

global type w_det_ord_ven_stato from w_cs_xx_principale
integer width = 3840
integer height = 1432
string title = "Stato Preparazione - Ordine"
boolean minbox = false
boolean maxbox = false
dw_documenti dw_documenti
cb_fasi cb_fasi
st_stato_produzione st_stato_produzione
st_1 st_1
dw_det_ord_ven_stato dw_det_ord_ven_stato
end type
global w_det_ord_ven_stato w_det_ord_ven_stato

type variables
long il_anno, il_num

uo_produzione iuo_produzione
end variables

forward prototypes
public function integer wf_mostra_documento (long al_row, ref string as_errore)
end prototypes

public function integer wf_mostra_documento (long al_row, ref string as_errore);
integer					li_anno
long						ll_numero, ll_prog_riga, ll_prog_mimetype, ll_len
string					ls_cod_nota, ls_describe, ls_estensione, ls_collegato,ls_temp_dir, ls_rnd, ls_file_name
blob						lb_blob
uo_shellexecute 		luo_run


if al_row > 0 then
	
	li_anno = dw_documenti.getitemnumber(al_row, "anno_registrazione")
	ll_numero = dw_documenti.getitemnumber(al_row, "num_registrazione")
	ll_prog_riga = dw_documenti.getitemnumber(al_row, "prog_riga")
	ls_cod_nota = dw_documenti.getitemstring(al_row, "cod_nota")
	ll_prog_mimetype = dw_documenti.getitemnumber(al_row, "prog_mimetype")
	
	ls_describe = dw_documenti.Describe("collegato.visible")
	choose case ls_describe
		case "1","0"
			ls_collegato = dw_documenti.getitemstring(al_row, "collegato")
			
		case else
			ls_collegato = "N"
	end choose
	
	if ls_collegato="S" then
		//leggi dalle offerte di vendita
		selectblob 	note_esterne
		into 			:lb_blob
		from det_off_ven_note
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno and
				num_registrazione = :ll_numero and
				prog_riga_off_ven = :ll_prog_riga and
				cod_nota = :ls_cod_nota;
	else
		selectblob 	note_esterne
		into 			:lb_blob
		from det_ord_ven_note
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno and
				num_registrazione = :ll_numero and
				prog_riga_ord_ven = :ll_prog_riga and
				cod_nota = :ls_cod_nota;
	
	end if
	
		
	if sqlca.sqlcode<0 then
		as_errore = "Errore in lettura documento dalla tabella (sqlcode -1): "+sqlca.sqlerrtext
		return -1
	
	elseif sqlca.sqlcode = 100 then
		as_errore = "Documento non trovato! Potrebbe essere stato cancellato da un altro utente!"
		return -1
		
	elseif sqlca.sqlcode <> 0 then
		as_errore = "Errore in lettura documento dalla tabella (sqlcode <>-1,<>0 e <>100)"
		return -1
		
	end if
		
	
	ll_len = lenA(lb_blob)
	
	
	select estensione
	into :ls_estensione
	from tab_mimetype
	where cod_azienda = :s_cs_xx.cod_azienda and
			prog_mimetype = :ll_prog_mimetype;
	
	ls_temp_dir = guo_functions.uof_get_user_temp_folder( )
	
	ls_rnd = string( hour(now()),"00") +"_" + string( minute(now()),"00") +"_" + string( second(now()),"00") 
	ls_file_name =  ls_temp_dir + ls_rnd + "." + ls_estensione
	guo_functions.uof_blob_to_file( lb_blob, ls_file_name)
	
	
	luo_run = create uo_shellexecute
	luo_run.uof_run( handle(this), ls_file_name )
	destroy(luo_run)
		
end if

return 0
end function

on w_det_ord_ven_stato.create
int iCurrent
call super::create
this.dw_documenti=create dw_documenti
this.cb_fasi=create cb_fasi
this.st_stato_produzione=create st_stato_produzione
this.st_1=create st_1
this.dw_det_ord_ven_stato=create dw_det_ord_ven_stato
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_documenti
this.Control[iCurrent+2]=this.cb_fasi
this.Control[iCurrent+3]=this.st_stato_produzione
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.dw_det_ord_ven_stato
end on

on w_det_ord_ven_stato.destroy
call super::destroy
destroy(this.dw_documenti)
destroy(this.cb_fasi)
destroy(this.st_stato_produzione)
destroy(this.st_1)
destroy(this.dw_det_ord_ven_stato)
end on

event close;call super::close;destroy iuo_produzione
end event

event pc_setwindow;call super::pc_setwindow;il_anno = s_cs_xx.parametri.parametro_d_1

setnull(s_cs_xx.parametri.parametro_d_1)

il_num = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_2)


dw_documenti.settransobject(sqlca)
dw_documenti.object.b_del.visible = false

iuo_dw_main = dw_det_ord_ven_stato

dw_det_ord_ven_stato.change_dw_current()

dw_det_ord_ven_stato.set_dw_options(sqlca, &
												pcca.null_object, &
												c_nonew + c_nomodify + c_nodelete, &
												c_default)
												

end event

type dw_documenti from datawindow within w_det_ord_ven_stato
integer x = 2889
integer y = 20
integer width = 896
integer height = 1276
integer taborder = 20
string title = "none"
string dataobject = "d_det_ord_ven_note_blob"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

event doubleclicked;string					ls_errore
integer					li_ret


if row>0 then
else
	return
end if

li_ret = wf_mostra_documento(row, ls_errore)

if li_ret<0 then
	g_mb.error(ls_errore)
end if
end event

type cb_fasi from commandbutton within w_det_ord_ven_stato
integer x = 2491
integer y = 1220
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Fasi"
end type

event clicked;long ll_row, ll_anno, ll_num, ll_riga


ll_row = dw_det_ord_ven_stato.getrow()

if isnull(ll_row) or ll_row = 0 then
	g_mb.messagebox("APICE","Selezionare una riga di dettaglio prima di continuare!",stopsign!)
	return -1
end if

ll_anno = dw_det_ord_ven_stato.getitemnumber(ll_row,"anno_registrazione")

ll_num = dw_det_ord_ven_stato.getitemnumber(ll_row,"num_registrazione")

ll_riga = dw_det_ord_ven_stato.getitemnumber(ll_row,"prog_riga_ord_ven")

if isnull(ll_anno) or ll_anno = 0 or isnull(ll_num) or ll_num = 0 or isnull(ll_riga) or ll_riga = 0 then
	g_mb.messagebox("APICE","Selezionare una riga di dettaglio prima di continuare!",stopsign!)
	return -1
end if

window_open_parm(w_det_ord_ven_fasi,-1,dw_det_ord_ven_stato)
end event

type st_stato_produzione from statictext within w_det_ord_ven_stato
integer x = 2126
integer y = 1220
integer width = 343
integer height = 80
boolean bringtotop = true
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type st_1 from statictext within w_det_ord_ven_stato
integer x = 1531
integer y = 1232
integer width = 571
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Stato di produzione:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_det_ord_ven_stato from uo_cs_xx_dw within w_det_ord_ven_stato
integer x = 23
integer y = 20
integer width = 2834
integer height = 1180
integer taborder = 10
string dataobject = "d_det_ord_ven_stato"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;parent.title = "Stato Preparazione - Ordine " + string(il_anno) + "/" + string(il_num)

retrieve(s_cs_xx.cod_azienda,il_anno,il_num)
end event

event rowfocuschanged;call super::rowfocuschanged;if getrow() > 0 then
	
		string ls_messaggio
		
		long 	 		ll_num, ll_riga, ll_return
		
		integer		li_anno
		
		li_anno = getitemnumber(getrow(),"anno_registrazione")
		
		ll_num = getitemnumber(getrow(),"num_registrazione")
		
		ll_riga = getitemnumber(getrow(),"prog_riga_ord_ven")
		
		
		dw_documenti.retrieve(s_cs_xx.cod_azienda, li_anno, ll_num, ll_riga)
		
		
		if not isvalid(iuo_produzione) then
			iuo_produzione = create uo_produzione
		end if
		
		ll_return = iuo_produzione.uof_stato_prod_det_ord_ven(li_anno, ll_num, ll_riga, ls_messaggio)
		
		choose case ll_return
			case 0
				st_stato_produzione.text = "Niente"
				st_stato_produzione.backcolor = rgb(255,0,0)
				st_stato_produzione.show()
				cb_fasi.show()
			case 1
				st_stato_produzione.text = "Tutto"
				st_stato_produzione.backcolor = rgb(0,255,0)
				st_stato_produzione.show()
				cb_fasi.show()
			case 2
				st_stato_produzione.text = "Parziale"
				st_stato_produzione.backcolor = rgb(255,255,0)
				st_stato_produzione.show()
				cb_fasi.show()
			case else
				st_stato_produzione.text = ""
				st_stato_produzione.backcolor = 12632256
				st_stato_produzione.hide()
				cb_fasi.hide()
		end choose
		
	else
		
		st_stato_produzione.text = ""
		st_stato_produzione.backcolor = 12632256
		st_stato_produzione.hide()
		cb_fasi.hide()
		
	end if
end event

