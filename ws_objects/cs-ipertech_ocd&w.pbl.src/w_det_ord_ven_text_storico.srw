﻿$PBExportHeader$w_det_ord_ven_text_storico.srw
forward
global type w_det_ord_ven_text_storico from window
end type
type cb_1 from commandbutton within w_det_ord_ven_text_storico
end type
type mle_1 from multilineedit within w_det_ord_ven_text_storico
end type
end forward

global type w_det_ord_ven_text_storico from window
integer width = 2537
integer height = 1508
boolean titlebar = true
string title = "Testo Esclusioni"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cb_1 cb_1
mle_1 mle_1
end type
global w_det_ord_ven_text_storico w_det_ord_ven_text_storico

event open;mle_1.text = s_cs_xx.parametri.parametro_s_10

end event

on w_det_ord_ven_text_storico.create
this.cb_1=create cb_1
this.mle_1=create mle_1
this.Control[]={this.cb_1,&
this.mle_1}
end on

on w_det_ord_ven_text_storico.destroy
destroy(this.cb_1)
destroy(this.mle_1)
end on

type cb_1 from commandbutton within w_det_ord_ven_text_storico
integer x = 2117
integer y = 1312
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long Job

job = printsetup()
if job < 1 then return
Job = PrintOpen( )
if job < 1 then return

Print( Job,s_cs_xx.parametri.parametro_s_10)

PrintClose(Job)


end event

type mle_1 from multilineedit within w_det_ord_ven_text_storico
integer x = 18
integer y = 16
integer width = 2464
integer height = 1280
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean border = false
boolean autohscroll = true
boolean autovscroll = true
boolean displayonly = true
end type

