﻿$PBExportHeader$w_det_ord_ven_tv.srw
$PBExportComments$Finestra Gestione  Dettaglio Ordini Vendita
forward
global type w_det_ord_ven_tv from w_cs_xx_principale
end type
type dw_det_ord_ven_det_conf_variabili from uo_std_dw within w_det_ord_ven_tv
end type
type st_margine from statictext within w_det_ord_ven_tv
end type
type cb_unisci_doc from commandbutton within w_det_ord_ven_tv
end type
type cb_1 from commandbutton within w_det_ord_ven_tv
end type
type dw_trasferimenti from datawindow within w_det_ord_ven_tv
end type
type dw_documenti from uo_dw_drag_doc_acq_ven within w_det_ord_ven_tv
end type
type cb_ologrammi from commandbutton within w_det_ord_ven_tv
end type
type cb_lancio_prod_riga from commandbutton within w_det_ord_ven_tv
end type
type uo_stato_prod from uo_stato_produzione within w_det_ord_ven_tv
end type
type cb_fasi from commandbutton within w_det_ord_ven_tv
end type
type uo_1 from uo_situazione_prodotto within w_det_ord_ven_tv
end type
type cb_assegnazione from commandbutton within w_det_ord_ven_tv
end type
type cb_azzera from commandbutton within w_det_ord_ven_tv
end type
type cb_blocca from commandbutton within w_det_ord_ven_tv
end type
type cb_corrispondenze from commandbutton within w_det_ord_ven_tv
end type
type cb_prezzo from commandbutton within w_det_ord_ven_tv
end type
type cb_sblocca from commandbutton within w_det_ord_ven_tv
end type
type cb_sconti from commandbutton within w_det_ord_ven_tv
end type
type cb_configuratore from commandbutton within w_det_ord_ven_tv
end type
type cb_c_industriale from commandbutton within w_det_ord_ven_tv
end type
type cb_ricalcola_configurazione from commandbutton within w_det_ord_ven_tv
end type
type st_avanzamento from statictext within w_det_ord_ven_tv
end type
type cb_cancella_commessa from commandbutton within w_det_ord_ven_tv
end type
type dw_det_ord_ven_det_1 from uo_cs_xx_dw within w_det_ord_ven_tv
end type
type dw_comp_det_ord_ven from uo_cs_xx_dw within w_det_ord_ven_tv
end type
type dw_folder from u_folder within w_det_ord_ven_tv
end type
type dw_det_ord_ven_lista from uo_cs_xx_dw within w_det_ord_ven_tv
end type
end forward

global type w_det_ord_ven_tv from w_cs_xx_principale
integer width = 5083
integer height = 2560
string title = "Righe.Ordini"
boolean minbox = false
event ue_posiziona_riga ( )
event ue_text_storico ( )
event ue_post_configuratore ( )
dw_det_ord_ven_det_conf_variabili dw_det_ord_ven_det_conf_variabili
st_margine st_margine
cb_unisci_doc cb_unisci_doc
cb_1 cb_1
dw_trasferimenti dw_trasferimenti
dw_documenti dw_documenti
cb_ologrammi cb_ologrammi
cb_lancio_prod_riga cb_lancio_prod_riga
uo_stato_prod uo_stato_prod
cb_fasi cb_fasi
uo_1 uo_1
cb_assegnazione cb_assegnazione
cb_azzera cb_azzera
cb_blocca cb_blocca
cb_corrispondenze cb_corrispondenze
cb_prezzo cb_prezzo
cb_sblocca cb_sblocca
cb_sconti cb_sconti
cb_configuratore cb_configuratore
cb_c_industriale cb_c_industriale
cb_ricalcola_configurazione cb_ricalcola_configurazione
st_avanzamento st_avanzamento
cb_cancella_commessa cb_cancella_commessa
dw_det_ord_ven_det_1 dw_det_ord_ven_det_1
dw_comp_det_ord_ven dw_comp_det_ord_ven
dw_folder dw_folder
dw_det_ord_ven_lista dw_det_ord_ven_lista
end type
global w_det_ord_ven_tv w_det_ord_ven_tv

type prototypes

end prototypes

type variables
boolean							ib_esposizione=false
long								il_indice_esp, il_num_riga, il_riga_riferimento, il_riga_selezionata
string								is_text_storico, is_cod_cat_mer_optional_conf
datastore						ids_esposizione
uo_condizioni_cliente			iuo_condizioni_cliente
uo_gestione_conversioni		iuo_gestione_conversioni
uo_produzione					iuo_produzione
datetime							idt_data_partenza, idt_date_reparti[]
string								is_reparti[], is_cod_valuta

integer							ii_anno_ordine
long								il_num_ordine, il_CCU

dec{4}							id_cambio_ven_ds, id_precisione_valuta

end variables

forward prototypes
public subroutine wf_tipo_det_ven (datawindow fdw_datawindow, commandbutton flc_ricerca_prodotti, picturebutton flp_prod_view, string fs_cod_tipo_det_ven, string fs_cod_agente_1, string fs_cod_agente_2)
public subroutine wf_tipo_det_ven_det (datawindow fdw_datawindow, commandbutton fbo_ricerca_prodotti, picturebutton fpb_listview, string fs_cod_tipo_det_ven)
public subroutine wf_tipo_det_ven_lista (datawindow fdw_datawindow, string fs_cod_tipo_det_ven)
public function integer wf_ricerca_condizioni (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_errore)
public function integer wf_cod_prodotto_finito (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_cod_prodotto_finito, ref string fs_messaggio)
public subroutine wf_proteggi_colonne (double fd_quan_evasa, double fd_quan_in_evasione, long fl_anno_commessa)
public function integer wf_ricalcola ()
public function integer wf_duplica_riga (long fl_anno, long fl_numero, long fl_prog, ref string fs_msg)
public function integer wf_duplica_altre_tabelle (long fl_anno, long fl_numero, long fl_prog, long fl_prog_new, ref string fs_msg)
public function integer wf_check_prodotto (string fs_cod_prodotto, ref string fs_errore)
public function longlong wf_get_prog_sessione ()
public subroutine wf_controlla_ddt_collegati ()
public function boolean wf_controlla_ologrammi (integer fi_anno, long fl_numero, long fl_riga)
public function integer wf_aggiorna_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore)
end prototypes

event ue_posiziona_riga();dw_det_ord_ven_lista.setredraw(false)
dw_det_ord_ven_lista.postevent("pcd_last")
//dw_det_ord_ven_lista.setrowfocusindicator(hand!,3960)
dw_det_ord_ven_lista.setrowfocusindicator(hand!)
dw_det_ord_ven_lista.setredraw(true)

end event

event ue_text_storico();if s_cs_xx.parametri.parametro_s_10 <> "" then
	string ls_cod_cliente
	long   ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_ord_ven
	datetime ldt_data_registrazione
	if is_text_storico = "" then
		
		ll_anno_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione")
		ls_cod_cliente = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
		ldt_data_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
		is_text_storico  = "ORDINE N." + string(ll_num_registrazione) + " DEL " + string(ldt_data_registrazione,"dd/mm/yyyy") + " CLIENTE: " + ls_cod_cliente + "~r~n"
		
	end if
	
	ll_prog_riga_ord_ven = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven")
	is_text_storico += "RIGA: " + string(ll_prog_riga_ord_ven) + " PRODOTTO FINITO: " + dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(),"cod_prodotto") + "~r~n" + s_cs_xx.parametri.parametro_s_10
end if


end event

event ue_post_configuratore();//gestione cambio data consegna dell'ordine ------------------------------------------------
long						ll_num_ordine, ll_i, ll_ret
string						ls_messaggio
datetime					ldt_nuova_data_consegna
integer					li_anno_ordine


ll_ret = dw_det_ord_ven_lista.getrow()

//integrazione gestione cambio data consegna
if not isnull(s_cs_xx.parametri.parametro_data_4) and ll_ret>0 then
	
	li_anno_ordine = dw_det_ord_ven_lista.getitemnumber(ll_ret, "anno_registrazione")
	ll_num_ordine = dw_det_ord_ven_lista.getitemnumber(ll_ret, "num_registrazione")

	if li_anno_ordine>0 then
		//al ritorno dal configuratore bisogna aggiornare alla nuova data consegna
		//presente nella variabile s_cs_xx.parametri.parametro_data_4
		//deleghiamo questo all'evento ue_post_configuratore della testata ordine
		
		
		if isvalid(s_cs_xx.parametri.parametro_w_ord_ven) then
			s_cs_xx.parametri.parametro_w_ord_ven.triggerevent("ue_post_configuratore")
		end if
		
		//aggiorna anche la lista delle righe di dettaglio
		dw_det_ord_ven_lista.change_dw_current()
		dw_det_ord_ven_lista.postevent("pcd_retrieve")
		
	end if
	
else
	//fai tutto come prima
	//fai quello che facevi prima
	dw_det_ord_ven_lista.change_dw_current()
	dw_det_ord_ven_lista.postevent("pcd_retrieve")
end if

setnull(s_cs_xx.parametri.parametro_data_4)
end event

public subroutine wf_tipo_det_ven (datawindow fdw_datawindow, commandbutton flc_ricerca_prodotti, picturebutton flp_prod_view, string fs_cod_tipo_det_ven, string fs_cod_agente_1, string fs_cod_agente_2);string ls_modify, ls_flag_tipo_det_ven, ls_null, ls_messaggio
double ld_prov_agente_1, ld_prov_agente_2, ld_provv_riga_1, ld_provv_riga_2


setnull(ls_null)
select tab_tipi_det_ven.flag_tipo_det_ven
into   :ls_flag_tipo_det_ven
from   tab_tipi_det_ven
where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_tipi_det_ven.cod_tipo_det_ven = :fs_cod_tipo_det_ven;

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita.", &
              exclamation!, ok!)
   return
end if

if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" then
//   flc_ricerca_prodotti.enabled = true
//   flp_prod_view.enabled = true
   ls_modify = "cod_prodotto.protect='0'~t"
   ls_modify = ls_modify + "cod_prodotto.background.color='16777215'~t"
   ls_modify = ls_modify + "cod_versione.protect='0'~t"
   ls_modify = ls_modify + "cod_versione.background.color='16777215'~t"
   fdw_datawindow.modify(ls_modify)
else
//   flc_ricerca_prodotti.enabled = false
//   flp_prod_view.enabled = false
   if not isnull(fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_prodotto")) then
      fdw_datawindow.setitem(fdw_datawindow.getrow(),"cod_prodotto",ls_null)
   end if
   ls_modify = "cod_prodotto.protect='1'~t"
   ls_modify = ls_modify + "cod_prodotto.background.color='12632256'~t"
   ls_modify = ls_modify + "cod_versione.protect='1'~t"
   ls_modify = ls_modify + "cod_versione.background.color='12632256'~t"
   fdw_datawindow.modify(ls_modify)
end if

if ls_flag_tipo_det_ven <> "M" and ls_flag_tipo_det_ven <> "C" and &
   ls_flag_tipo_det_ven <> "N" and ls_flag_tipo_det_ven <> "S" then
   if fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"provvigione_1") > 0 then
      fdw_datawindow.setitem(fdw_datawindow.getrow(),"provvigione_1",0)
   end if
   if fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"provvigione_2") > 0 then
      fdw_datawindow.setitem(fdw_datawindow.getrow(),"provvigione_2",0)
   end if
else
   select anag_agenti.prov_agente
   into   :ld_prov_agente_1
   from   anag_agenti
   where  anag_agenti.cod_azienda = :s_cs_xx.cod_azienda and 
          anag_agenti.cod_agente = :fs_cod_agente_1;

	ld_provv_riga_1 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"provvigione_1")
   if sqlca.sqlcode = 0 and ld_provv_riga_1 = 0 then
      fdw_datawindow.setitem(fdw_datawindow.getrow(), "provvigione_1", ld_prov_agente_1)
	elseif sqlca.sqlcode <> 0 then
      fdw_datawindow.setitem(fdw_datawindow.getrow(), "provvigione_1", 0)
	end if

   select anag_agenti.prov_agente
   into   :ld_prov_agente_2
   from   anag_agenti
   where  anag_agenti.cod_azienda = :s_cs_xx.cod_azienda and 
          anag_agenti.cod_agente = :fs_cod_agente_2;

	ld_provv_riga_2 = fdw_datawindow.getitemnumber(fdw_datawindow.getrow(),"provvigione_2")
   if sqlca.sqlcode = 0 and ld_provv_riga_2 = 0 then
      fdw_datawindow.setitem(fdw_datawindow.getrow(), "provvigione_2", ld_prov_agente_2)
	elseif sqlca.sqlcode <> 0 then
      fdw_datawindow.setitem(fdw_datawindow.getrow(), "provvigione_2", 0)
   end if
end if
end subroutine

public subroutine wf_tipo_det_ven_det (datawindow fdw_datawindow, commandbutton fbo_ricerca_prodotti, picturebutton fpb_listview, string fs_cod_tipo_det_ven);string ls_modify, ls_flag_tipo_det_ven, ls_null, ls_messaggio

setnull(ls_null)
if not isnull(fs_cod_tipo_det_ven) then
	select tab_tipi_det_ven.flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_ven.cod_tipo_det_ven = :fs_cod_tipo_det_ven;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita.", &
					  exclamation!, ok!)
		return
	end if
	
	if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" then
//		fbo_ricerca_prodotti.enabled = true
//		fpb_listview.enabled = true
		ls_modify = "cod_prodotto.protect='0'~t"
		ls_modify = ls_modify + "cod_prodotto.background.color='16777215'~t"
		ls_modify = ls_modify + "cod_versione.protect='0'~t"
		ls_modify = ls_modify + "cod_versione.background.color='16777215'~t"
		fdw_datawindow.modify(ls_modify)
	else
//		fbo_ricerca_prodotti.enabled = false
//		fpb_listview.enabled = false
		if not isnull(fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_prodotto")) then
			fdw_datawindow.setitem(fdw_datawindow.getrow(),"cod_prodotto",ls_null)
		end if
		ls_modify = "cod_prodotto.protect='1'~t"
		ls_modify = ls_modify + "cod_prodotto.background.color='12632256'~t"
		ls_modify = ls_modify + "cod_versione.protect='1'~t"
		ls_modify = ls_modify + "cod_versione.background.color='12632256'~t"
		fdw_datawindow.modify(ls_modify)
	end if
end if
end subroutine

public subroutine wf_tipo_det_ven_lista (datawindow fdw_datawindow, string fs_cod_tipo_det_ven);string ls_modify, ls_flag_tipo_det_ven, ls_null, ls_messaggio
double ld_prov_agente_1, ld_prov_agente_2, ld_provv_riga_1, ld_provv_riga_2


setnull(ls_null)
if not isnull(fs_cod_tipo_det_ven) then
	select tab_tipi_det_ven.flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_ven.cod_tipo_det_ven = :fs_cod_tipo_det_ven;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita.", &
					  exclamation!, ok!)
		return
	end if
	
	if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" then
		ls_modify = "cod_prodotto.protect='0'~t"
		ls_modify = ls_modify + "cod_prodotto.background.color='16777215'~t"
		fdw_datawindow.modify(ls_modify)
	else
		if not isnull(fdw_datawindow.getitemstring(fdw_datawindow.getrow(),"cod_prodotto")) then
			fdw_datawindow.setitem(fdw_datawindow.getrow(),"cod_prodotto",ls_null)
		end if
		ls_modify = "cod_prodotto.protect='1'~t"
		ls_modify = ls_modify + "cod_prodotto.background.color='12632256'~t"
		fdw_datawindow.modify(ls_modify)
	end if
end if
end subroutine

public function integer wf_ricerca_condizioni (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_errore);// *****************************************************************************************************************
//
// FUNZIONE DI CALCOLO DEL PREZZO DEL PRODOTTO FINITO DEL CONFIGURATORE
// SENZA PASSARE DENTRO LA MASCHERA DEL CONFIGURATORE
//
// AUTORE: EnMe 29/1/2002
//
//
// ----------------------------------  calcolo del prezzo  --------------------------------------------------- //
boolean lb_test
string ls_valuta_lire, ls_cod_valuta, ls_sql, ls_prezzo_vendita, ls_valore_riga, ls_fat_conversione, ls_cod_tessuto, ls_errore,&
       ls_sconto, ls_provvigione_1, ls_provvigione_2, ls_quantita_um, ls_prezzo_um, ls_null,ls_flag_prezzo_superficie, &
		 ls_cod_prodotto,ls_cod_gruppo_var_quan_distinta,ls_tabella_det_doc, ls_progressivo,ls_cod_tipo_det_ven,&
		 ls_flag_uso_quan_distinta, ls_tabella_varianti, ls_messaggio
long ll_i, ll_y
dec{4} ld_variazioni[], ld_gruppi_sconti[], ld_provv_1, ld_provv_2, ld_valore_riga, ld_dimensione_1, ld_dimensione_2, &
       ld_dimensione_3, ld_quan_ordinata, ld_provvigione_1, ld_provvigione_2, &
		 ld_prezzo_um, ld_quantita_um,ld_quan_tessuto, ld_dim_x, ld_dim_y, ld_dim_z, ld_prezzo_vendita
dec{5} ld_fat_conversione
uo_gruppi_sconti luo_gruppi_sconti
uo_condizioni_cliente luo_condizioni_cliente

setnull(ls_null)
ls_tabella_det_doc = "det_ord_ven"
ls_progressivo = "prog_riga_ord_ven"
ls_tabella_varianti = "varianti_det_ord_ven"

luo_condizioni_cliente = CREATE uo_condizioni_cliente
s_cs_xx.listino_db.flag_calcola = "S"
s_cs_xx.listino_db.anno_documento = fl_anno_registrazione
s_cs_xx.listino_db.num_documento = fl_num_registrazione
s_cs_xx.listino_db.prog_riga = fl_prog_riga_ord_ven
s_cs_xx.listino_db.tipo_gestione = "ORD_VEN"

select cod_versione,
       quan_ordine
into   :s_cs_xx.listino_db.cod_versione,
		 :s_cs_xx.listino_db.quan_prodotto_finito
from   det_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :fl_anno_registrazione and
      num_registrazione = :fl_num_registrazione and
		prog_riga_ord_ven = :fl_prog_riga_ord_ven;

select cod_cliente, 
       cod_valuta, 
		 cod_tipo_listino_prodotto, 
		 data_registrazione,
		 cod_agente_1
into  :s_cs_xx.listino_db.cod_cliente, 
      :s_cs_xx.listino_db.cod_valuta, 
		:s_cs_xx.listino_db.cod_tipo_listino_prodotto, 
		:s_cs_xx.listino_db.data_riferimento,
		:luo_condizioni_cliente.str_parametri.cod_agente_1
from  tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :fl_anno_registrazione and
      num_registrazione = :fl_num_registrazione ;

select cod_modello,
       dim_x,
       dim_y,
		 dim_z
into   :ls_cod_prodotto,
       :ld_dim_x,
       :ld_dim_y,
		 :ld_dim_z
from   comp_det_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :fl_anno_registrazione and
      num_registrazione = :fl_num_registrazione and
		prog_riga_ord_ven = :fl_prog_riga_ord_ven;


luo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
luo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
luo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
luo_condizioni_cliente.str_parametri.ldw_oggetto = dw_det_ord_ven_lista
luo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = s_cs_xx.listino_db.cod_tipo_listino_prodotto
luo_condizioni_cliente.str_parametri.cod_valuta = s_cs_xx.listino_db.cod_valuta
luo_condizioni_cliente.str_parametri.data_riferimento = s_cs_xx.listino_db.data_riferimento
luo_condizioni_cliente.str_parametri.cod_cliente = s_cs_xx.listino_db.cod_cliente


luo_condizioni_cliente.str_parametri.quantita = s_cs_xx.listino_db.quan_prodotto_finito
luo_condizioni_cliente.str_parametri.valore = 0
luo_condizioni_cliente.str_parametri.cod_agente_2 = ls_null
luo_condizioni_cliente.str_parametri.colonna_quantita = ""
luo_condizioni_cliente.str_parametri.colonna_prezzo = ""
luo_condizioni_cliente.ib_setitem = false
luo_condizioni_cliente.ib_setitem_provvigioni = false
luo_condizioni_cliente.wf_condizioni_cliente()

//for ll_i = 1 to 10
//	if str_conf_prodotto.stato = "N" and not str_conf_prodotto.flag_cambio_modello then
//										//            ^^ aggiunto per spec_varie_5; funzione 14
//		if upperbound(luo_condizioni_cliente.str_output.sconti) >= ll_i then
//			istr_riepilogo.sconti[ll_i] = luo_condizioni_cliente.str_output.sconti[ll_i]
//		else
//			istr_riepilogo.sconti[ll_i] = 0
//		end if
//	else
//		istr_riepilogo.sconti[ll_i] = str_conf_prodotto.sconti[ll_i]
//	end if
//next
//
//if str_conf_prodotto.stato = "N" then
	ld_provvigione_1 = luo_condizioni_cliente.str_output.provvigione_1
	ld_provvigione_2 = luo_condizioni_cliente.str_output.provvigione_2
//else
//	ld_provvigione_1 = str_conf_prodotto.provvigione_1
//	ld_provvigione_2 = str_conf_prodotto.provvigione_2
//end if
if isnull(ld_provvigione_1) then ld_provvigione_1 = 0
if isnull(ld_provvigione_2) then ld_provvigione_2 = 0
	
if upperbound(luo_condizioni_cliente.str_output.variazioni) < 1 or isnull(upperbound(luo_condizioni_cliente.str_output.variazioni)) then
	ld_prezzo_vendita = 0
	ld_fat_conversione = 1
else
	ld_fat_conversione = 1
	ld_prezzo_vendita = luo_condizioni_cliente.str_output.variazioni[upperbound(luo_condizioni_cliente.str_output.variazioni)]
end if
	// calcolo del valore netto riga //
select cod_tipo_det_ven_prodotto,
       flag_prezzo_superficie,
		 flag_uso_quan_distinta,
		 cod_gruppo_var_quan_distinta
into   :ls_cod_tipo_det_ven,
       :ls_flag_prezzo_superficie,
		 :ls_flag_uso_quan_distinta,
		 :ls_cod_gruppo_var_quan_distinta
from   tab_flags_configuratore
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_modello = :ls_cod_prodotto;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca tipi dettaglio nella tabella parametri configuratore. Dettaglio errore "+sqlca.sqlerrtext
	return -1
end if
if isnull(ls_cod_tipo_det_ven) then
	fs_errore = "Il tipo dettaglio vendita del prodotto non è stato indicato nella tabella flags configuratore"
	return -1
end if


// ----------------------- gestione del prezzo per metro quadarato con il fattore di conversione ------------------------

if ls_flag_prezzo_superficie = "S" then
	if ld_dim_x > 0 then
		if luo_condizioni_cliente.str_output.min_fat_larghezza > 0 and not isnull(luo_condizioni_cliente.str_output.min_fat_larghezza) and ld_dim_x < luo_condizioni_cliente.str_output.min_fat_larghezza then
			ld_dimensione_1 = luo_condizioni_cliente.str_output.min_fat_larghezza
		else
			ld_dimensione_1 = ld_dim_x
		end if
	end if
	if ld_dim_y > 0 then
		if luo_condizioni_cliente.str_output.min_fat_altezza > 0 and not isnull(luo_condizioni_cliente.str_output.min_fat_altezza) and ld_dim_y < luo_condizioni_cliente.str_output.min_fat_altezza then
			ld_dimensione_2 = luo_condizioni_cliente.str_output.min_fat_altezza
		else
			ld_dimensione_2 = ld_dim_y
		end if
	end if
	if ld_dim_z > 0 then
		if luo_condizioni_cliente.str_output.min_fat_profondita > 0 and not isnull(luo_condizioni_cliente.str_output.min_fat_profondita) and ld_dim_z < luo_condizioni_cliente.str_output.min_fat_profondita then
			ld_dimensione_3 = luo_condizioni_cliente.str_output.min_fat_profondita
		else
			ld_dimensione_3 = ld_dim_x
		end if
	end if
	
	// --------------------- nuova gestione vuoto per pieno -------------------------
	//                         nicola ferrari 23/05/2000
	if ls_flag_uso_quan_distinta = "N" then
		ld_fat_conversione = round((ld_dimensione_1 * ld_dimensione_2) / 10000,2)
		if ld_fat_conversione <= 0 or isnull(ld_fat_conversione) then
			fs_errore = "Attenzione la quantità risultante dal calcolo DIM1 x DIM2 è pari a zero o è nulla; verrà FORZATA 1, ma è necessario il controllo dell'operatore!"
			ld_fat_conversione = 1
		end if

		uo_conf_varianti uo_ins_varianti
		uo_ins_varianti = CREATE uo_conf_varianti
		if uo_ins_varianti.uof_ricerca_quan_mp(ls_cod_prodotto,  &
														s_cs_xx.listino_db.cod_versione, &
														s_cs_xx.listino_db.quan_prodotto_finito, &
														ls_cod_gruppo_var_quan_distinta, &
														fl_anno_registrazione, &
														fl_num_registrazione, &
														fl_prog_riga_ord_ven, &
														ls_tabella_varianti, &
														ls_cod_tessuto, &
														ld_quan_tessuto,&
														ls_errore) = -1 then
			fs_errore = "Errore nella ricerca quantità tessuto in disntita base. Dettaglio errore: " + ls_errore
			rollback;
			return -1
		end if

		if ld_quan_tessuto <= 0 or isnull(ld_quan_tessuto) then
			fs_errore = "Attenzione la quantità trovata nel gruppo variante " + ls_cod_gruppo_var_quan_distinta + " è pari a zero o è nulla; verrà FORZATA 1, ma è necessario il controllo dell'operatore!"
			ld_quan_tessuto = 1
		end if
		destroy uo_ins_varianti
		ld_fat_conversione = round(ld_quan_tessuto,2)
	end if
//
	// ---------------------- fine vuoto per pieno ----------------------------------
	// controllo il minimale di superficie
	if luo_condizioni_cliente.str_output.min_fat_superficie > 0 and not isnull(luo_condizioni_cliente.str_output.min_fat_superficie) and ld_fat_conversione < luo_condizioni_cliente.str_output.min_fat_superficie then
		ld_fat_conversione = luo_condizioni_cliente.str_output.min_fat_superficie
	end if
	ld_quantita_um = s_cs_xx.listino_db.quan_prodotto_finito * ld_fat_conversione
	ld_prezzo_um = ld_prezzo_vendita
	ld_prezzo_vendita = ld_prezzo_vendita * ld_fat_conversione
end if	

if luo_condizioni_cliente.uof_arrotonda_prezzo(ld_prezzo_vendita, str_conf_prodotto.cod_valuta, ld_prezzo_vendita, ls_messaggio) = -1 then
	fs_errore = "Errore durante l'arrotondamento del prezzo di vendita: " + ls_messaggio
	rollback;
	return -1
end if

ls_cod_valuta = s_cs_xx.listino_db.cod_valuta
ld_quan_ordinata = s_cs_xx.listino_db.quan_prodotto_finito

//	------------------------------// SQL aggiornamento riga dettaglio ///--------------------------------------------

if ld_provvigione_1 = 0 or isnull(ld_provvigione_1) then 
	ls_provvigione_1 = "0"
else
	ls_provvigione_1 = f_double_to_string(ld_provvigione_1)
	if isnull(ls_provvigione_1) then ls_provvigione_1 = "0"
end if
if ld_provvigione_2 = 0 or isnull(ld_provvigione_2) then 
	ls_provvigione_2 = "0"
else
	ls_provvigione_2 = f_double_to_string(ld_provvigione_2)
	if isnull(ls_provvigione_2) then ls_provvigione_2 = "0"
end if

if ld_prezzo_vendita = 0 or isnull(ld_prezzo_vendita) then 
	ls_prezzo_vendita = "0"
else
	ls_prezzo_vendita = f_double_to_string(ld_prezzo_vendita)
	if isnull(ls_prezzo_vendita) then ls_prezzo_vendita = "0"
end if

if ld_valore_riga = 0 or isnull(ld_valore_riga) then 
	ls_valore_riga = "0"
else
	ls_valore_riga = f_double_to_string(ld_valore_riga)
	if isnull(ls_valore_riga) then ls_valore_riga = "0"
end if

if ld_fat_conversione = 0 or isnull(ld_fat_conversione) then 
	ls_fat_conversione = "0"
else
	ls_fat_conversione = f_double_to_string(ld_fat_conversione)
	if isnull(ls_fat_conversione) then ls_fat_conversione = "1"
end if

if ld_prezzo_um = 0 or isnull(ld_prezzo_um) then 
	ls_prezzo_um = "0"
else
	ls_prezzo_um = f_double_to_string(ld_prezzo_um)
	if isnull(ls_prezzo_um) then ls_prezzo_um = "0"
end if

if ld_quantita_um = 0 or isnull(ld_quantita_um) then 
	ls_quantita_um = "0"
else
	ls_quantita_um = f_double_to_string(ld_quantita_um)
	if isnull(ls_quantita_um) then ls_quantita_um = "0"
end if

ls_sql =" update " + ls_tabella_det_doc
ls_sql = ls_sql + " set prezzo_vendita = " + ls_prezzo_vendita
ls_sql = ls_sql + ", prezzo_um = " + ls_prezzo_um
ls_sql = ls_sql + ", quantita_um = " + ls_quantita_um
ls_sql = ls_sql + ", fat_conversione_ven = " + ls_fat_conversione
for ll_i = 1 to upperbound(luo_condizioni_cliente.str_output.sconti[])
	if ll_i > 10 then exit
	if luo_condizioni_cliente.str_output.sconti[ll_i] = 0 or isnull(luo_condizioni_cliente.str_output.sconti[ll_i]) then 
		ls_sconto = "0"
	else
		ls_sconto = f_double_to_string(luo_condizioni_cliente.str_output.sconti[ll_i])
		if isnull(ls_sconto) then ls_sconto = "0"
	end if
	ls_sql = ls_sql + ", sconto_" + string(ll_i) + " = " + ls_sconto
next

ls_sql = ls_sql + ", provvigione_1 = " + ls_provvigione_1 + ", provvigione_2 = " + ls_provvigione_2
ls_sql =ls_sql + " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  " anno_registrazione = " + string(fl_anno_registrazione) + " and " + &
					  " num_registrazione = " + string(fl_num_registrazione) + " and " + &
					    ls_progressivo + " = " + string(fl_prog_riga_ord_ven)

execute immediate :ls_sql;
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Configuratore di Prodotto", "Errore nell'aggiornamento del prezzo di vendita. Dettaglio " + sqlca.sqlerrtext,   exclamation!, ok!)
	rollback;
	return -1
end if
commit;
return 0

end function

public function integer wf_cod_prodotto_finito (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_cod_prodotto_finito, ref string fs_messaggio);//	Funzione che leggendo i dati di configurazione restituisce il codice prodotto finito
//
// nome: wf_init_cod_prodotto
// 
//	Autore: EnMe 29/1/2002

string ls_cod_veloce, ls_cod_modello, ls_cod_tessuto, ls_cod_non_a_magazzino,ls_cod_comando,ls_cod_verniciatura,ls_linea_prodotto, &
       ls_classe_merceolo

dec{4} ld_dim_x, ld_dim_y

select cod_modello,
       cod_tessuto,
		 cod_non_a_magazzino,
		 cod_comando,
		 cod_verniciatura,
		 dim_x,
		 dim_y
into   :ls_cod_modello,
       :ls_cod_tessuto,
		 :ls_cod_non_a_magazzino,
		 :ls_cod_comando,
		 :ls_cod_verniciatura,
		 :ld_dim_x,
		 :ld_dim_y
 from  comp_det_ord_ven
 where cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 prog_riga_ord_ven = :fl_prog_riga_ord_ven;
		 



if isnull(ls_cod_modello) then return 0
ls_linea_prodotto = left(ls_cod_modello, 1)
ls_classe_merceolo = mid(ls_cod_modello, 2, 1)

fs_cod_prodotto_finito = ls_cod_modello + "     "

// --------------------------------------  inizio ---------------------------------------------

if ls_classe_merceolo <> "0" and ls_classe_merceolo <> "S" then
	if ld_dim_x > 999 then
		fs_messaggio = "Dimensione Troppo Grande (Max 999)"
		return -1
	else
		fs_cod_prodotto_finito = replace(fs_cod_prodotto_finito, 7, 3, fill("0", 3 - len(trim(string(integer(ld_dim_x))))) + trim(string(integer(ld_dim_y))))
	end if
end if

// ----------------------------------------  tessuto ---------------------------------------------
if not isnull(ls_cod_non_a_magazzino) and len(ls_cod_non_a_magazzino) > 0 and not isnull(ls_cod_tessuto) and len(ls_cod_tessuto) > 0  then
	choose case ls_linea_prodotto
		case "A", "B","C","D","E","F","G","H","I","L","M","N","O","P","Q","R","S","T","U","V","Z"
			select cod_veloce
			 into :ls_cod_veloce
			 from tab_tessuti
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_non_a_magazzino = :ls_cod_non_a_magazzino and
					cod_tessuto = :ls_cod_tessuto;
			if SQLCA.sqlcode = 100 then
				fs_messaggio = "Codice Tessuto non Valido"
				return -1
			elseif SQLCA.sqlcode <> 0 then
				fs_messaggio = "Errore durante la ricerca proprietà tessuto in tabella tessuti. Dettaglio errore " + sqlca.sqlerrtext
				return -1
			end if
			fs_cod_prodotto_finito = Replace(fs_cod_prodotto_finito, 5, 1, ls_cod_veloce)
	end choose
end if
//

// ------------------------------------------ comandi --------------------------------------------------------
if not isnull(ls_cod_comando) and len(ls_cod_comando) > 0 then
	choose case ls_linea_prodotto
		case "A", "B","C","D","E","F","G","H","I","L","M","N","O","P","Q","R","S","T","U","V","Z"
			select cod_veloce
			 into :ls_cod_veloce
			 from tab_comandi
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_comando = :ls_cod_comando;
			if sqlca.sqlcode = 100 then
				fs_messaggio = "Codice comando non Valido"
				return -1
			elseif sqlca.sqlcode = -1 then
				fs_messaggio = "Errore durante l'Estrazione del codice veloce dal tipo comando"
				return -1
			end if
			if isnull(ls_cod_veloce) or len(ls_cod_veloce) < 1 then
				fs_messaggio = "Al comando " + ls_cod_comando + " non è stato associato alcun codice veloce: verificare"
				return -1
			end if
			fs_cod_prodotto_finito = Replace(fs_cod_prodotto_finito, 7, 2, ls_cod_veloce)
	end choose
end if
// ----------------------------------- lastre / verniciature ---------------------------------------------

if not isnull(ls_cod_verniciatura) and len(ls_cod_verniciatura) > 0 then
	select cod_veloce
	into  :ls_cod_veloce
	from  tab_verniciatura
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_verniciatura = :ls_cod_verniciatura;
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Autocomposizione codice: codice verniciatura " + ls_cod_verniciatura + " non trovato in tabella verniciature"
		return -1
	elseif SQLCA.sqlcode <> 0 then
		fs_messaggio = "Errore durante ricerca codice verniciatura in tabella verniciature.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
	if isnull(ls_cod_veloce) or len(ls_cod_veloce) < 1 then
		fs_messaggio = "Alla verniciatura " + ls_cod_verniciatura + " non è stato associato alcun codice veloce: verificare"
		return -1
	end if
	fs_cod_prodotto_finito = Replace(fs_cod_prodotto_finito, 6, 1, ls_cod_veloce)
end if

return 0
end function

public subroutine wf_proteggi_colonne (double fd_quan_evasa, double fd_quan_in_evasione, long fl_anno_commessa);string ls_nome_colonna, ls_num_col, ls_protect
long ll_i, ll_err,ll_num_col

dw_det_ord_ven_lista.setredraw(false)
dw_det_ord_ven_det_1.setredraw(false)
dw_comp_det_ord_ven.setredraw(false)
if fd_quan_evasa > 0 or fd_quan_in_evasione > 0 or fl_anno_commessa > 0 or not isnull(fl_anno_commessa) then
	dw_det_ord_ven_lista.object.quan_ordine.background.color=12632256
	dw_det_ord_ven_lista.object.quan_ordine.protect = 1
	
	dw_det_ord_ven_lista.object.cod_prodotto.background.color=12632256
	dw_det_ord_ven_lista.object.cod_prodotto.protect = 1
	
	dw_det_ord_ven_det_1.object.cod_prodotto.background.color=12632256
	dw_det_ord_ven_det_1.object.cod_prodotto.protect = 1
	
	dw_det_ord_ven_det_1.object.quan_ordine.background.color=12632256
	dw_det_ord_ven_det_1.object.quan_ordine.protect = 1

	dw_det_ord_ven_det_1.object.cod_versione.background.color=12632256
	dw_det_ord_ven_det_1.object.cod_versione.protect = 1
	
	dw_det_ord_ven_det_1.object.data_consegna.background.color=12632256
	dw_det_ord_ven_det_1.object.data_consegna.protect = 1
	
	dw_det_ord_ven_det_1.object.num_confezioni.background.color=12632256
	dw_det_ord_ven_det_1.object.num_confezioni.protect = 1
	
	dw_det_ord_ven_det_1.object.num_pezzi_confezione.background.color=12632256
	dw_det_ord_ven_det_1.object.num_pezzi_confezione.protect = 1
else
	dw_det_ord_ven_lista.object.quan_ordine.background.color=16777215
	dw_det_ord_ven_lista.object.quan_ordine.protect = 0
	
	dw_det_ord_ven_lista.object.cod_prodotto.background.color=16777215
	dw_det_ord_ven_lista.object.cod_prodotto.protect = 0
	
	dw_det_ord_ven_det_1.object.cod_prodotto.background.color=16777215
	dw_det_ord_ven_det_1.object.cod_prodotto.protect = 0
	
	dw_det_ord_ven_det_1.object.quan_ordine.background.color=16777215
	dw_det_ord_ven_det_1.object.quan_ordine.protect = 0

	dw_det_ord_ven_det_1.object.cod_versione.background.color=16777215
	dw_det_ord_ven_det_1.object.cod_versione.protect = 0

	dw_det_ord_ven_det_1.object.data_consegna.background.color=16777215
	dw_det_ord_ven_det_1.object.data_consegna.protect = 0

	dw_det_ord_ven_det_1.object.num_confezioni.background.color=16777215
	dw_det_ord_ven_det_1.object.num_confezioni.protect = 0

	dw_det_ord_ven_det_1.object.num_pezzi_confezione.background.color=16777215
	dw_det_ord_ven_det_1.object.num_pezzi_confezione.protect = 0
	
end if
// aggiunto per spec_varie_5; non posso accedere ai campi del dettaglio complmentare in modifica se
// alla riga è collegata una commessa
if fl_anno_commessa > 0 or not isnull(fl_anno_commessa) then
	ls_protect = "1"
else
	ls_protect = "0"
end if
ls_num_col = dw_comp_det_ord_ven.Object.DataWindow.Column.Count
ll_num_col = long(ls_num_col)
for ll_i = 1 to ll_num_col
	ll_err = dw_comp_det_ord_ven.setcolumn(ll_i)
	if ll_err > 0 then
		ls_nome_colonna = dw_comp_det_ord_ven.getcolumnname()
		dw_comp_det_ord_ven.Modify(ls_nome_colonna+".Protect="+ls_protect)
	end if
next
dw_det_ord_ven_lista.setredraw(true)
dw_det_ord_ven_det_1.setredraw(true)
dw_comp_det_ord_ven.setredraw(true)
return
end subroutine

public function integer wf_ricalcola ();long   ll_i

string ls_parametro, ls_prodotto, ls_misura_mag, ls_misura_ven

dec{4} ld_quan_mag, ld_prezzo_mag, ld_quan_ven, ld_prezzo_ven, ld_fat_conversione, ld_prec_mag, ld_prec_ven

uo_calcola_documento_euro luo_calcolo


select stringa
into   :ls_parametro
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'TRD';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura parametro TRD da parametri_azienda: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_parametro) or (ls_parametro <> "M" and ls_parametro <> "V") then
	return 0
end if

select precisione_prezzo_mag,
		 precisione_prezzo_ven
into   :ld_prec_mag,
		 :ld_prec_ven
from   con_vendite
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in lettura precisione prezzi da parametri vendite: " + sqlca.sqlerrtext)
	return -1
end if

if isnull(ld_prec_mag) or ld_prec_mag = 0 then
	g_mb.messagebox("APICE","Impostare la precisione del prezzo di magazzino in PARAMETRI VENDITE")
	return -1
end if

if isnull(ld_prec_ven) or ld_prec_ven = 0 then
	g_mb.messagebox("APICE","Impostare la precisione del prezzo di vendita in PARAMETRI VENDITE")
	return -1
end if

for ll_i = 1 to dw_det_ord_ven_lista.rowcount()
	
	ls_prodotto = dw_det_ord_ven_lista.getitemstring(ll_i,"cod_prodotto")
	
	if isnull(ls_prodotto) then
		continue
	end if
	
	ls_misura_ven = dw_det_ord_ven_lista.getitemstring(ll_i,"cod_misura")
	
	select cod_misura_mag
	into   :ls_misura_mag
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_prodotto;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in lettura dati prodotto da anag_prodotti: " + sqlca.sqlerrtext)
		return -1
	end if
	
	if ls_misura_ven = ls_misura_mag then
		continue
	end if
	
	ld_quan_mag = dw_det_ord_ven_lista.getitemnumber(ll_i,"quan_ordine")
	
	ld_prezzo_mag = dw_det_ord_ven_lista.getitemnumber(ll_i,"prezzo_vendita")
	
	ld_quan_ven = dw_det_ord_ven_lista.getitemnumber(ll_i,"quantita_um")
	
	ld_prezzo_ven = dw_det_ord_ven_lista.getitemnumber(ll_i,"prezzo_um")
	
	ld_fat_conversione = dw_det_ord_ven_lista.getitemnumber(ll_i,"fat_conversione_ven")
	
	choose case ls_parametro
			
		case "M"
			
			ld_quan_ven = round(ld_quan_mag * ld_fat_conversione , 4)
			
			ld_prezzo_ven = ld_prezzo_mag / ld_fat_conversione
			
			luo_calcolo = create uo_calcola_documento_euro
			
			luo_calcolo.uof_arrotonda(ld_prezzo_ven,ld_prec_ven,"round")
			
			destroy luo_calcolo
			
		case "V"
			
			ld_quan_mag = round(ld_quan_ven / ld_fat_conversione , 4)
			
			ld_prezzo_mag = ld_prezzo_ven * ld_fat_conversione
			
			luo_calcolo = create uo_calcola_documento_euro
			
			luo_calcolo.uof_arrotonda(ld_prezzo_mag,ld_prec_mag,"round")
			
			destroy luo_calcolo
			
	end choose
	
	dw_det_ord_ven_lista.setitem(ll_i,"quan_ordine",ld_quan_mag)
	
	dw_det_ord_ven_lista.setitem(ll_i,"prezzo_vendita",ld_prezzo_mag)
	
	dw_det_ord_ven_lista.setitem(ll_i,"quantita_um",ld_quan_ven)
	
	dw_det_ord_ven_lista.setitem(ll_i,"prezzo_um",ld_prezzo_ven)
	
next

return 0
end function

public function integer wf_duplica_riga (long fl_anno, long fl_numero, long fl_prog, ref string fs_msg);long 				ll_count, ll_prog_riga, ll_anno_reg_cu, ll_num_reg_cu, ll_prog_riga_ord_ven_cu, ll_i, ll_ret, &
					ll_prog_riga_hold, ll_anno_commessa, ll_num_commessa
string 			ls_sql, ls_flag_stampata_bcl, ls_cod_prodotto, ls_cod_prodotto_raggruppato, ls_cod_tipo_det_ven, &
					ls_flag_tipo_det_ven, ls_flag_evasione, ls_flag_blocco
integer 			li_risposta
dec{4}			ld_quan_ordine,ld_quan_raggruppo, ldd_quan_evasa, ldd_quan_in_evasione
uo_spese_trasporto luo_spese_trasp

if fl_anno>0 and fl_numero>0 and fl_prog>0 then
else
	return 1
end if

select count(*)
into :ll_count
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno and
			num_registrazione=:fl_numero and
			prog_riga_ord_ven=:fl_prog;
	
if ll_count>0 then
else
	//ordine inesistente o non ancora salvato
	fs_msg=""
	return 0
end if

//controlla che la riga non sia evasa e che non abbia commesse ----------------------------------------
select 	flag_evasione,
			flag_blocco,
			quan_evasa,
			quan_in_evasione,
			anno_commessa,
			num_commessa
into		:ls_flag_evasione,
			:ls_flag_blocco,
			:ldd_quan_evasa,
			:ldd_quan_in_evasione,
			:ll_anno_commessa,
			:ll_num_commessa
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno and
			num_registrazione=:fl_numero and
			prog_riga_ord_ven=:fl_prog;

if sqlca.sqlcode=0 then
else
	fs_msg="Errore in lettura dati di controllo riga ordine da duplicare"
	if not isnull(sqlca.sqlerrtext) then fs_msg+= ": "+sqlca.sqlerrtext	
	return -1
end if

if ls_flag_evasione <> "A" then
	fs_msg = "Non è possibile effettuare duplicazioni di righe già evase o parzialmente evase"
	return -1
end if

if ldd_quan_evasa > 0 or ldd_quan_in_evasione > 0  then
	fs_msg = "Non è possibile effettuare duplicazioni di righe con quantità già evasa o in evasione"
	return -1
end if

if ls_flag_blocco = 'S' then
	fs_msg = "Non puoi duplicare una riga d'ordine bloccata."
	return -1
end if

setnull(ls_flag_blocco)

select flag_blocco
into   :ls_flag_blocco
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno and
		 num_registrazione = :fl_numero;
		 
if sqlca.sqlcode=0 then
else
	fs_msg="Errore in lettura dati di controllo testata ordine"
	if not isnull(sqlca.sqlerrtext) then fs_msg+= ": "+sqlca.sqlerrtext	
	return -1
end if
		 
if ls_flag_blocco = 'S' then
	fs_msg = "Non puoi duplicare una riga da un ordine bloccato."
	return -1
end if

if ll_anno_commessa>0 and ll_num_commessa>0 then
	fs_msg = "Non puoi duplicare una riga con una commessa associata."
	return -1
end if

//-----------------------------------------------------------------------------------------------------------------

if g_mb.messagebox("APICE","Duplicare la riga "+string(fl_prog)+"?", Question!, YesNo!,1)=2 then
	fs_msg=""
	return 0
end if

//calcolo max prog riga
select max(prog_riga_ord_ven)
into :ll_prog_riga
from det_ord_ven
where cod_azienda=:s_cs_xx.cod_azienda and
	anno_registrazione=:fl_anno and num_registrazione=:fl_numero;
	
if sqlca.sqlcode=0 then
else
	fs_msg="Errore in calcolo max progressivo riga"
	if not isnull(sqlca.sqlerrtext) then fs_msg+= ": "+sqlca.sqlerrtext
	
	return -1
end if

ll_prog_riga = int(ll_prog_riga / 10)
if ll_prog_riga = 0 then
	ll_prog_riga = 10
else
	ll_prog_riga = ll_prog_riga * 10 + 10
end if

//inserisci la riga duplicata -------------------------------------------------------------------

  INSERT INTO det_ord_ven  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           prog_riga_ord_ven,   
           cod_prodotto,   
           cod_tipo_det_ven,   
           cod_misura,   
           des_prodotto,   
           quan_ordine,   
           prezzo_vendita,   
           fat_conversione_ven,   
           sconto_1,   
           sconto_2,   
           provvigione_1,   
           provvigione_2,   
           cod_iva,   
           data_consegna,   
           val_riga,   
           quan_in_evasione,   
           quan_evasa,   
           flag_evasione,   
           flag_blocco,   
           nota_dettaglio,   
           anno_registrazione_off,   
           num_registrazione_off,   
           prog_riga_off_ven,   
           anno_commessa,   
           num_commessa,   
           cod_centro_costo,   
           sconto_3,   
           sconto_4,   
           sconto_5,   
           sconto_6,   
           sconto_7,   
           sconto_8,   
           sconto_9,   
           sconto_10,   
           cod_versione,   
           flag_presso_ptenda,   
           flag_presso_cgibus,   
           num_confezioni,   
           num_pezzi_confezione,   
           num_riga_appartenenza,   
           flag_gen_commessa,   
           flag_doc_suc_det,   
           flag_st_note_det,   
           quantita_um,   
           prezzo_um,   
           imponibile_iva,   
           imponibile_iva_valuta,   
           settimana_consegna,   
           flag_stampa_settimana,   
           nota_prodotto,   
           flag_trasporta_nota_ddt,   
           flag_riga_sospesa,   
           cod_tipo_causa_sospensione,   
           flag_urgente,   
           anno_reg_ord_acq,   
           num_reg_ord_acq,   
           prog_riga_ord_acq,   
           flag_elaborato_rda,
		  flag_prezzo_bloccato,
		  dim_x,
		  dim_y,
		  peso_netto,
		  cod_reparto_sfuso,
		  data_pronto_sfuso,
		  cod_reparto_arrivo,
		  data_arrivo)  
     SELECT cod_azienda,   
            anno_registrazione,   
            num_registrazione,   
            :ll_prog_riga,   
            cod_prodotto,   
            cod_tipo_det_ven,   
            cod_misura,   
            des_prodotto,   
            quan_ordine,   
            prezzo_vendita,   
            fat_conversione_ven,   
            sconto_1,   
            sconto_2,   
            provvigione_1,   
            provvigione_2,   
            cod_iva,   
            data_consegna,   
            val_riga,   
            quan_in_evasione,   
            quan_evasa,   
            flag_evasione,   
            flag_blocco,   
            nota_dettaglio,   
            anno_registrazione_off,   
            num_registrazione_off,   
            prog_riga_off_ven,   
            null,   
            null,   
            cod_centro_costo,   
            sconto_3,   
            sconto_4,   
            sconto_5,   
            sconto_6,   
            sconto_7,   
            sconto_8,   
            sconto_9,   
            sconto_10,   
            cod_versione,   
            flag_presso_ptenda,   
            flag_presso_cgibus,   
            num_confezioni,   
            num_pezzi_confezione,   
            num_riga_appartenenza,   
            flag_gen_commessa,   
            flag_doc_suc_det,   
            flag_st_note_det,   
            quantita_um,   
            prezzo_um,   
            imponibile_iva,   
            imponibile_iva_valuta,   
            settimana_consegna,   
            flag_stampa_settimana,   
            nota_prodotto,   
            flag_trasporta_nota_ddt,   
            flag_riga_sospesa,   
            cod_tipo_causa_sospensione,   
            flag_urgente,   
            anno_reg_ord_acq,   
            num_reg_ord_acq,   
            prog_riga_ord_acq,   
            'N' ,
		  flag_prezzo_bloccato,
		  dim_x,
		  dim_y,
		  peso_netto,
		  cod_reparto_sfuso,
		  data_pronto_sfuso,
		  cod_reparto_arrivo,
		  data_arrivo
       FROM det_ord_ven 
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fl_anno and 
				num_registrazione=:fl_numero and
				prog_riga_ord_ven=:fl_prog;

if sqlca.sqlcode=0 then
else
	fs_msg="Errore in inserimento duplicato riga "+string(fl_prog)
	if not isnull(sqlca.sqlerrtext) then fs_msg+= ": "+sqlca.sqlerrtext
	
	return -1
end if

ll_prog_riga_hold = ll_prog_riga

//AGGIORNAMENTO IMPEGNATO RIGA -------------------------------------------------
select 	cod_tipo_det_ven,
			cod_prodotto,
			quan_ordine
into		:ls_cod_tipo_det_ven,
			:ls_cod_prodotto,
			:ld_quan_ordine
from		det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno and 
			num_registrazione=:fl_numero and
			prog_riga_ord_ven=:ll_prog_riga;
			
if sqlca.sqlcode<0 then
	fs_msg="Errore in lettura dati riga duplicata "+string(ll_prog_riga) +" "+sqlca.sqlerrtext	
	return -1
end if

select 	flag_tipo_det_ven 
into   		:ls_flag_tipo_det_ven
from   	tab_tipi_det_ven
where  	cod_azienda = :s_cs_xx.cod_azienda and 
		 	cod_tipo_det_ven = :ls_cod_tipo_det_ven;
if sqlca.sqlcode <> 0  then
	fs_msg = "Si è verificato un errore in fase di lettura tipi dettaglio." + sqlca.sqlerrtext
	return -1
end if

 if ls_flag_tipo_det_ven = "M" and not isnull(ls_cod_prodotto) and ls_cod_prodotto<>""then
	
	update anag_prodotti  
		set quan_impegnata = quan_impegnata + :ld_quan_ordine  
	 where 	cod_azienda = :s_cs_xx.cod_azienda and  
				cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode <0 then
		fs_msg= "Si è verificato un errore in fase di aggiornamento magazzino." +sqlca.sqlerrtext
		return -1
	end if
	
	// enme 08/1/2006 gestione prodotto raggruppato
	setnull(ls_cod_prodotto_raggruppato)
	
	select cod_prodotto_raggruppato
	into   :ls_cod_prodotto_raggruppato
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if not isnull(ls_cod_prodotto_raggruppato) then
		
		ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordine, "M")
		
		update anag_prodotti  
			set quan_impegnata = quan_impegnata + :ld_quan_raggruppo  
		 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
		if sqlca.sqlcode = -1 then
			fs_msg =  "Si è verificato un errore in fase di aggiornamento magazzino."
			return -1
		end if
	end if
end if

//FINE AGGIORNAMENTO IMPEGNATO RIGA --------------------------------------------------------------------

//inserisci nella comp_det_ord_ven
insert into comp_det_ord_ven
	(	cod_azienda,   
         anno_registrazione,   
         num_registrazione,   
         prog_riga_ord_ven,   
         cod_prod_finito,   
         cod_modello,   
         dim_x,   
         dim_y,   
         dim_z,   
         dim_t,   
         cod_tessuto,   
         cod_non_a_magazzino,   
         cod_tessuto_mant,   
         cod_mant_non_a_magazzino,   
         flag_tipo_mantovana,   
         alt_mantovana,   
         flag_cordolo,   
         flag_passamaneria,   
         cod_verniciatura,   
         cod_comando,   
         pos_comando,   
         alt_asta,   
         cod_comando_tenda_verticale,   
         cod_colore_lastra,   
         cod_tipo_lastra,   
         spes_lastra,   
         cod_tipo_supporto,   
         flag_cappotta_frontale,   
         flag_rollo,   
         flag_kit_laterale,   
         flag_misura_luce_finita,   
         cod_tipo_stampo,   
         cod_mat_sis_stampaggio,   
         rev_stampo,   
         num_gambe,   
         num_campate,   
         note,   
		flag_autoblocco,   
         num_fili_plisse,   
         num_boccole_plisse,   
         intervallo_punzoni_plisse,   
         num_pieghe_plisse,   
         prof_inf_plisse,   
         prof_sup_plisse,   
         colore_passamaneria,   
         colore_cordolo,   
         num_punzoni_plisse,   
         des_vernice_fc,   
         des_comando_1,   
         cod_comando_2,   
         des_comando_2,   
         flag_addizionale_comando_1,   
         flag_addizionale_comando_2,   
         flag_addizionale_supporto,   
         flag_addizionale_tessuto,   
         flag_addizionale_vernic,   
         flag_fc_comando_1,   
         flag_fc_comando_2,   
         flag_fc_supporto,   
         flag_fc_tessuto,   
         flag_fc_vernic,   
         flag_allegati,   
         data_ora_ar,   
         flag_tessuto_cliente,   
         flag_tessuto_mant_cliente,
		cod_verniciatura_2,
		flag_addizionale_vernic_2,
		flag_fc_vernic_2,
		des_vernic_fc_2,
		cod_verniciatura_3,
		flag_addizionale_vernic_3,
		flag_fc_vernic_3,
		des_vernic_fc_3,
		flag_azzera_dt,
		cod_modello_telo_finito)
  select cod_azienda,   
         anno_registrazione,   
         num_registrazione,   
         :ll_prog_riga,   
         cod_prod_finito,   
         cod_modello,   
         dim_x,   
         dim_y,   
         dim_z,   
         dim_t,   
         cod_tessuto,   
         cod_non_a_magazzino,   
         cod_tessuto_mant,   
         cod_mant_non_a_magazzino,   
         flag_tipo_mantovana,   
         alt_mantovana,   
         flag_cordolo,   
         flag_passamaneria,   
         cod_verniciatura,   
         cod_comando,   
         pos_comando,   
         alt_asta,   
         cod_comando_tenda_verticale,   
         cod_colore_lastra,   
         cod_tipo_lastra,   
         spes_lastra,   
         cod_tipo_supporto,   
         flag_cappotta_frontale,   
         flag_rollo,   
         flag_kit_laterale,   
         flag_misura_luce_finita,   
         cod_tipo_stampo,   
         cod_mat_sis_stampaggio,   
         rev_stampo,   
         num_gambe,   
         num_campate,   
         note,   
		flag_autoblocco,   
         num_fili_plisse,   
         num_boccole_plisse,   
         intervallo_punzoni_plisse,   
         num_pieghe_plisse,   
         prof_inf_plisse,   
         prof_sup_plisse,   
         colore_passamaneria,   
         colore_cordolo,   
         num_punzoni_plisse,   
         des_vernice_fc,   
         des_comando_1,   
         cod_comando_2,   
         des_comando_2,   
         flag_addizionale_comando_1,   
         flag_addizionale_comando_2,   
         flag_addizionale_supporto,   
         flag_addizionale_tessuto,   
         flag_addizionale_vernic,   
         flag_fc_comando_1,   
         flag_fc_comando_2,   
         flag_fc_supporto,   
         flag_fc_tessuto,   
         flag_fc_vernic,   
         flag_allegati,   
         data_ora_ar,   
         flag_tessuto_cliente,   
         flag_tessuto_mant_cliente,
		cod_verniciatura_2,
		flag_addizionale_vernic_2,
		flag_fc_vernic_2,
		des_vernic_fc_2,
		cod_verniciatura_3,
		flag_addizionale_vernic_3,
		flag_fc_vernic_3,
		des_vernic_fc_3,
		flag_azzera_dt,
		cod_modello_telo_finito
    from comp_det_ord_ven  
   where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno and 
			num_registrazione=:fl_numero and
			prog_riga_ord_ven=:fl_prog;

if sqlca.sqlcode=0 then
else
	fs_msg="Errore in inserimento duplicato riga "+string(fl_prog) + " (comp_det_ord_ven)"
	if not isnull(sqlca.sqlerrtext) then fs_msg+= ": "+sqlca.sqlerrtext
	
	return -1
end if

// duplica variabili configuratore
// duplico variabili configurazione
INSERT INTO det_ord_ven_conf_variabili  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  prog_riga_ord_ven,   
			  prog_variabile,   
			  cod_variabile,   
			  flag_tipo_dato,   
			  valore_stringa,   
			  valore_numero,   
			  valore_datetime )  
	select cod_azienda,   
			anno_registrazione,   
			num_registrazione,   
			:ll_prog_riga,   
			prog_variabile,   
			cod_variabile,   
			flag_tipo_dato,   
			valore_stringa,   
			valore_numero,   
			valore_datetime  
			FROM det_ord_ven_conf_variabili 
			where cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:fl_anno and 
					num_registrazione=:fl_numero and
					prog_riga_ord_ven=:fl_prog;
if sqlca.sqlcode < 0 then
	fs_msg = "Errore durante la copia della tabella conf_variabili." + sqlca.sqlerrtext
	return -1
end if

//aggiorna altre tabelle referenziate tipo 1-n
ll_ret = wf_duplica_altre_tabelle(fl_anno, fl_numero, fl_prog, ll_prog_riga, fs_msg)
if ll_ret=1 then
	//vai avanti
else
	//messaggio già memorizzato in fs_msg
	return -1
end if
//------------------------------------------------------------

//adesso duplica le eventuali righe collegate
ls_sql = "select anno_registrazione, num_registrazione, prog_riga_ord_ven from det_ord_ven " + &
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and anno_registrazione="+string(fl_anno)+" and "+&
						"num_registrazione="+string(fl_numero)+" and num_riga_appartenenza="+string(fl_prog)

declare cu_cursore dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_cursore;

if sqlca.sqlcode <> 0 then 
	fs_msg = "Errore nella open del cursore delle righe collegate. Dettaglio = " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_cursore
	into :ll_anno_reg_cu, 
		   :ll_num_reg_cu, 
		   :ll_prog_riga_ord_ven_cu;
	
	if sqlca.sqlcode < 0 then
		fs_msg = "Errore nella fetch del cursore delle righe collegate. Dettaglio = " + sqlca.sqlerrtext
		close cu_cursore;
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then
		exit
	end if	
	
	ll_prog_riga += 1
	
	//duplica la riga collegata
	insert into det_ord_ven
	(	cod_azienda,   
         anno_registrazione, 
         num_registrazione,   
         prog_riga_ord_ven,   
         cod_prodotto,   
         cod_tipo_det_ven,   
         cod_misura,   
         des_prodotto,   
         quan_ordine,   
         prezzo_vendita,   
         fat_conversione_ven,   
         sconto_1,   
         sconto_2,   
         provvigione_1,   
         provvigione_2,   
         cod_iva,   
         data_consegna,   
         val_riga,   
         quan_in_evasione,   
         quan_evasa,   
         flag_evasione,   
         flag_blocco,   
         nota_dettaglio,   
         anno_registrazione_off,   
         num_registrazione_off,   
         prog_riga_off_ven,   
         anno_commessa,   
         num_commessa,   
         cod_centro_costo,   
         sconto_3,   
         sconto_4,   
         sconto_5,   
         sconto_6,   
         sconto_7,   
         sconto_8,   
         sconto_9,   
         sconto_10,   
         cod_versione,   
         flag_presso_ptenda,   
         flag_presso_cgibus,   
         num_confezioni,   
         num_pezzi_confezione,   
         num_riga_appartenenza,   
         flag_gen_commessa,   
         flag_doc_suc_det,   
         flag_st_note_det,   
         quantita_um,   
         prezzo_um,   
         imponibile_iva,   
         imponibile_iva_valuta,   
         settimana_consegna,   
         flag_stampa_settimana,   
         nota_prodotto,   
         flag_trasporta_nota_ddt,   
         flag_riga_sospesa,   
         cod_tipo_causa_sospensione,   
         flag_urgente,   
         anno_reg_ord_acq,   
         num_reg_ord_acq,   
         prog_riga_ord_acq,
	  	flag_elaborato_rda,
		flag_prezzo_bloccato,
		dim_x,
		dim_y,
		peso_netto,
		cod_reparto_sfuso,
		data_pronto_sfuso,
		cod_reparto_arrivo,
		data_arrivo)
 	select cod_azienda,   
         anno_registrazione, 
         num_registrazione,   
         :ll_prog_riga,   
         cod_prodotto,   
         cod_tipo_det_ven,   
         cod_misura,   
         des_prodotto,   
         quan_ordine,   
         prezzo_vendita,   
         fat_conversione_ven,   
         sconto_1,   
         sconto_2,   
         provvigione_1,   
         provvigione_2,   
         cod_iva,   
         data_consegna,   
         val_riga,   
         quan_in_evasione,   
         quan_evasa,   
         flag_evasione,   
         flag_blocco,   
         nota_dettaglio,   
         anno_registrazione_off,   
         num_registrazione_off,   
         prog_riga_off_ven,   
         null, //det_ord_ven.anno_commessa,   
         null, //det_ord_ven.num_commessa,   
         cod_centro_costo,   
         sconto_3,   
         sconto_4,   
         sconto_5,   
         sconto_6,   
         sconto_7,   
         sconto_8,   
         sconto_9,   
         sconto_10,   
         cod_versione,   
         flag_presso_ptenda,   
         flag_presso_cgibus,   
         num_confezioni,   
         num_pezzi_confezione,   
         :ll_prog_riga_hold,   
         flag_gen_commessa,   
         flag_doc_suc_det,   
         flag_st_note_det,   
         quantita_um,   
         prezzo_um,   
         imponibile_iva,   
         imponibile_iva_valuta,   
         settimana_consegna,   
         flag_stampa_settimana,   
         nota_prodotto,   
         flag_trasporta_nota_ddt,   
         flag_riga_sospesa,   
         cod_tipo_causa_sospensione,   
         flag_urgente,   
         anno_reg_ord_acq,   
         num_reg_ord_acq,   
         prog_riga_ord_acq,
	  	'N',
		flag_prezzo_bloccato,
		dim_x,
		dim_y,
		peso_netto,
		cod_reparto_sfuso,
		data_pronto_sfuso,
		cod_reparto_arrivo,
		data_arrivo
from 	det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_reg_cu and 
			num_registrazione=:ll_num_reg_cu and
			prog_riga_ord_ven=:ll_prog_riga_ord_ven_cu;

	if sqlca.sqlcode=0 then
	else
		fs_msg="Errore in inserimento duplicato riga collegata "+string(ll_prog_riga_ord_ven_cu)
		if not isnull(sqlca.sqlerrtext) then fs_msg+= ": "+sqlca.sqlerrtext
		
		return -1
	end if

	//AGGIORNAMENTO IMPEGNATO RIGA COLLEGATA -------------------------------------------------
	select 	cod_tipo_det_ven,
				cod_prodotto,
				quan_ordine
	into		:ls_cod_tipo_det_ven,
				:ls_cod_prodotto,
				:ld_quan_ordine
	from		det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fl_anno and 
				num_registrazione=:fl_numero and
				prog_riga_ord_ven=:ll_prog_riga;
				
	if sqlca.sqlcode<0 then
		fs_msg="Errore in lettura dati riga collegata e duplicata"+string(ll_prog_riga) +" "+sqlca.sqlerrtext	
		return -1
	end if
	
	select 	flag_tipo_det_ven 
	into   		:ls_flag_tipo_det_ven
	from   	tab_tipi_det_ven
	where  	cod_azienda = :s_cs_xx.cod_azienda and 
				cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	if sqlca.sqlcode <> 0  then
		fs_msg = "Si è verificato un errore in fase di lettura tipi dettaglio." + sqlca.sqlerrtext
		return -1
	end if
	
	 if ls_flag_tipo_det_ven = "M" and not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
		
		update anag_prodotti  
			set quan_impegnata = quan_impegnata + :ld_quan_ordine  
		 where 	cod_azienda = :s_cs_xx.cod_azienda and  
					cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <0 then
			fs_msg= "Si è verificato un errore in fase di aggiornamento magazzino." +sqlca.sqlerrtext
			return -1
		end if
		
		// enme 08/1/2006 gestione prodotto raggruppato
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then
			
			ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordine, "M")
			
			update anag_prodotti  
				set quan_impegnata = quan_impegnata + :ld_quan_raggruppo  
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
			if sqlca.sqlcode = -1 then
				fs_msg =  "Si è verificato un errore in fase di aggiornamento magazzino."
				return -1
			end if
		end if
	end if
	//FINE AGGIORNAMENTO IMPEGNATO RIGA COLLEGATA --------------------------------------------------------------------
	
loop

close cu_cursore;


if not isvalid(s_cs_xx.parametri.parametro_w_ord_ven) then
	fs_msg = "Impossibile calcolare il documento. Aprire la testata del documento ed eseguire manualmente il calcolo!"
	return 1
end if

// stefanop: 17/03/2014: spese trasporto
luo_spese_trasp = create uo_spese_trasporto
if luo_spese_trasp.uof_imposta_spese_ord_ven(fl_anno, fl_numero, ll_prog_riga_hold, true, fs_msg) < 0 then
	destroy luo_spese_trasp
	return -1
end if
destroy luo_spese_trasp

//nuova chiamata
s_cs_xx.parametri.parametro_w_ord_ven.postevent("ue_calcola")

fs_msg = "Operazione terminata con successo!"

return 1
end function

public function integer wf_duplica_altre_tabelle (long fl_anno, long fl_numero, long fl_prog, long fl_prog_new, ref string fs_msg);

//integrazioni_det_ord_ven ----------------------------------------------------------------
insert into integrazioni_det_ord_ven
(				cod_azienda,   
				anno_registrazione,   
				num_registrazione,   
				prog_riga_ord_ven, 
				progressivo,   
				cod_prodotto_padre,   
				num_sequenza,   
				cod_prodotto_figlio,   
				cod_misura,   
				quan_tecnica,   
				quan_utilizzo,   
				dim_x,   
				dim_y,   
				dim_z,   
				dim_t,   
				coef_calcolo,   
				des_estesa,   
				cod_formula_quan_utilizzo,   
				flag_escludibile,   
				flag_materia_prima,   
				flag_arrotonda,   
				cod_versione_padre,   
				cod_versione_figlio,   
				lead_time)
  select 	cod_azienda,   
				anno_registrazione,   
				num_registrazione,   
				:fl_prog_new, 
				progressivo,   
				cod_prodotto_padre,   
				num_sequenza,   
				cod_prodotto_figlio,   
				cod_misura,   
				quan_tecnica,   
				quan_utilizzo,   
				dim_x,   
				dim_y,   
				dim_z,   
				dim_t,   
				coef_calcolo,   
				des_estesa,   
				cod_formula_quan_utilizzo,   
				flag_escludibile,   
				flag_materia_prima,   
				flag_arrotonda,   
				cod_versione_padre,   
				cod_versione_figlio,   
				lead_time
	from integrazioni_det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fl_anno and
				num_registrazione=:fl_numero and
				prog_riga_ord_ven=:fl_prog;

if sqlca.sqlcode=0 then
else
	fs_msg="Errore in inserimento integrazioni_det_ord_ven  riga "+string(fl_prog_new)
	if not isnull(sqlca.sqlerrtext) then fs_msg+= ": "+sqlca.sqlerrtext
	
	return -1
end if
//fine integrazioni_det_ord_ven -------------------------------------------------------------------------------------------------

//det_ord_ven_note --------------------------------------------------------------------------------------------------------------
insert into det_ord_ven_note
(				cod_azienda,   
				anno_registrazione,   
				num_registrazione,   
				prog_riga_ord_ven,
				cod_nota,   
				des_nota,   
				note)
  	select 	cod_azienda,   
				anno_registrazione,   
				num_registrazione,   
				:fl_prog_new,
				cod_nota,   
				des_nota,   
				note
	from det_ord_ven_note
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fl_anno and
				num_registrazione=:fl_numero and
				prog_riga_ord_ven=:fl_prog;

if sqlca.sqlcode=0 then
else
	fs_msg="Errore in inserimento det_ord_ven_note  riga "+string(fl_prog_new)
	if not isnull(sqlca.sqlerrtext) then fs_msg+= ": "+sqlca.sqlerrtext
	
	return -1
end if
//fine det_ord_ven_note ------------------------------------------------------------------------------------------------------

//det_ord_ven_corrispondenze ------------------------------------------------------------------------------------------------
insert into det_ord_ven_corrispondenze
(			cod_azienda,   
         anno_registrazione,   
         num_registrazione,   
         prog_riga_ord_ven,   
         cod_corrispondenza,   
         data_corrispondenza,   
         ora_corrispondenza,   
         prog_corrispondenza,   
         intestatario,   
         data_pros_corrispondenza,   
         ora_pros_corrispondenza,   
         oggetto,   
         num_reg_lista,   
         num_versione,   
         num_edizione,   
         num_reg_lista_comp,   
         prog_liste_con_comp,   
         flag_tempo,   
         tempo_speso,   
         note,   
	    	cod_tipo_agenda,   
         cod_utente,            
         ora_agenda,   
         costo,   
         cod_operaio,
	    det_ord_ven_corrispondenze.data_agenda)
	select
			cod_azienda,   
         anno_registrazione,   
         num_registrazione,   
         :fl_prog_new,   
         cod_corrispondenza,   
         data_corrispondenza,   
         ora_corrispondenza,   
         prog_corrispondenza,   
         intestatario,   
         data_pros_corrispondenza,   
         ora_pros_corrispondenza,   
         oggetto,   
         num_reg_lista,   
         num_versione,   
         num_edizione,   
         num_reg_lista_comp,   
         prog_liste_con_comp,   
         flag_tempo,   
         tempo_speso,   
         note,   
	    	cod_tipo_agenda,   
         cod_utente,            
         ora_agenda,   
         costo,   
         cod_operaio,
	    	data_agenda
	from det_ord_ven_corrispondenze
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fl_anno and
				num_registrazione=:fl_numero and
				prog_riga_ord_ven=:fl_prog;
				
if sqlca.sqlcode=0 then
else
	fs_msg="Errore in inserimento det_ord_ven_corrispondenze  riga "+string(fl_prog_new)
	if not isnull(sqlca.sqlerrtext) then fs_msg+= ": "+sqlca.sqlerrtext
	
	return -1
end if
//fine det_ord_ven_corrispondenze ------------------------------------------------------------------------------------------

//varianti_det_ord_ven -------------------------------------------------------------------------------------------------------------
insert into varianti_det_ord_ven
(			cod_azienda,   
         anno_registrazione,   
         num_registrazione,   
         prog_riga_ord_ven,   
         cod_prodotto_padre,   
         num_sequenza,   
         cod_prodotto_figlio,   
         cod_versione,   
         cod_versione_figlio,   
         cod_prodotto,   
         cod_versione_variante,   
         cod_misura,   
         quan_tecnica,   
         quan_utilizzo,   
         dim_x,   
         dim_y,   
         dim_z,   
         dim_t,   
         coef_calcolo,   
         flag_esclusione,   
         formula_tempo,   
         des_estesa,   
         flag_materia_prima,   
         lead_time,
		cod_deposito_produzione)
	select
	    	cod_azienda,   
         anno_registrazione,   
         num_registrazione,   
         :fl_prog_new,   
         cod_prodotto_padre,   
         num_sequenza,   
         cod_prodotto_figlio,   
         cod_versione,   
         cod_versione_figlio,   
         cod_prodotto,   
         cod_versione_variante,   
         cod_misura,   
         quan_tecnica,   
         quan_utilizzo,   
         dim_x,   
         dim_y,   
         dim_z,   
         dim_t,   
         coef_calcolo,   
         flag_esclusione,   
         formula_tempo,   
         des_estesa,   
         flag_materia_prima,   
         lead_time,
		cod_deposito_produzione
	from varianti_det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fl_anno and
				num_registrazione=:fl_numero and
				prog_riga_ord_ven=:fl_prog;
				
if sqlca.sqlcode=0 then
else
	fs_msg="Errore in inserimento varianti_det_ord_ven  riga "+string(fl_prog_new)
	if not isnull(sqlca.sqlerrtext) then fs_msg+= ": "+sqlca.sqlerrtext
	
	return -1
end if
//fine varianti_det_ord_ven ------------------------------------------------------------------------------------------------------

//varianti_det_ord_ven_prod -------------------------------------------------------------------------------------------------------------
insert into varianti_det_ord_ven_prod
(		cod_azienda,   
         anno_registrazione,   
         num_registrazione,   
         prog_riga_ord_ven,   
         cod_prodotto_padre,   
         num_sequenza,   
         cod_prodotto_figlio,   
         cod_versione,   
         cod_versione_figlio,   
		cod_reparto,
		data_pronto,
		cod_reparto_arrivo,
		data_arrivo)
	select
	    	cod_azienda,   
         anno_registrazione,   
         num_registrazione,   
         :fl_prog_new,   
         cod_prodotto_padre,   
         num_sequenza,   
         cod_prodotto_figlio,   
         cod_versione,   
         cod_versione_figlio,   
		cod_reparto,
		data_pronto,
		cod_reparto_arrivo,
		data_arrivo
	from varianti_det_ord_ven_prod
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fl_anno and
				num_registrazione=:fl_numero and
				prog_riga_ord_ven=:fl_prog;
				
if sqlca.sqlcode=0 then
else
	fs_msg="Errore in inserimento varianti_det_ord_ven_prod  riga "+string(fl_prog_new)
	if not isnull(sqlca.sqlerrtext) then fs_msg+= ": "+sqlca.sqlerrtext
	
	return -1
end if
//fine varianti_det_ord_ven_prod ------------------------------------------------------------------------------------------------------

return 1
end function

public function integer wf_check_prodotto (string fs_cod_prodotto, ref string fs_errore);//controlla se il prodotto è configurato
//se si segnala all'operatore che ciò potrebbe causare problemi nell'assegnazione dei reparti di produzione e nella stampa ordini produzione
//infatti dalla finestra di dettaglio non si dovrebbe inserire/modificare prodotti soggetti a configurazione: occorre usare il configuratore

long ll_cont

if isnull(fs_cod_prodotto) or fs_cod_prodotto="" then return 0

//stabilisco se è uno sfuso o un modello
select count(*)
into   :ll_cont
from   tab_flags_configuratore
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_modello = :fs_cod_prodotto;
		 
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in ricerca in tab_flags_configuratore (prodotto "+fs_cod_prodotto+")~r~n" + sqlca.sqlerrtext
	return -1
end if

if ll_cont>0 then
	fs_errore = "Attenzione: il prodotto"+fs_cod_prodotto+" è soggetto a configurazione. Ciò potrebbe causare problemi in assegnazione reparti o ordini stampa produzione!"
	return 1
end if

return 0
end function

public function longlong wf_get_prog_sessione ();longlong			ll_id
string				ls_temp

//numero 		es.			2012011109452235 (fino ai millesecondi con due cifre)
ls_temp = string(today(), "yyyymmdd") + string(now(), "hhmmssff")
ll_id = longlong(ls_temp)

return ll_id

end function

public subroutine wf_controlla_ddt_collegati ();/**
 * stefanop
 * 27/07/2012
 *
 * Al cambiamento del prezzo controllo se ci sono DDT collegati alla riga e avviso l'operatore
 * che il cambio provoca un disallineamento nel DDT e dei movimenti di magazzino.
 **/
 
string ls_sql, ls_error, ls_ddt
int li_anno_registrazione, li_count, li_i
long ll_num_registrazione, ll_prog_riga_ord_ven
datastore lds_store

li_anno_registrazione = dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(), "num_registrazione")
ll_prog_riga_ord_ven = dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(), "prog_riga_ord_ven")


ls_sql = "SELECT anno_registrazione, num_registrazione, prog_riga_bol_ven FROM det_bol_ven " + &
			"WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND anno_registrazione_ord_ven=" + string(li_anno_registrazione) + &
			" AND num_registrazione_ord_ven=" + string(ll_num_registrazione) + " AND prog_riga_ord_ven=" + string(ll_prog_riga_ord_ven)
			
li_count = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)

if li_count < 0 then
	g_mb.warning(ls_error)
	return
elseif li_count = 0 then
	// Nessun DDT collegato
	return
end if

ls_ddt = ""

for li_i = 1 to li_count
	
	ls_ddt += g_str.format("- $1/$2/$3~r~n", lds_store.getitemnumber(li_i, 1), lds_store.getitemnumber(li_i, 2), lds_store.getitemnumber(li_i, 3))
	
next

g_mb.warning("Attenzione il cambio prezzo della riga è stato variato, tale riga è referenziata nei seguenti DDT:~r~n" + ls_ddt + "Procedere MANUALMENTE alla variazione nei documenti collegati.")
end subroutine

public function boolean wf_controlla_ologrammi (integer fi_anno, long fl_numero, long fl_riga);uo_produzione		luo_prod
integer				li_risposta, li_anno
long					ll_null
string					ls_errore, ls_cod_tipo_ord_ven,ls_tipo_stampa


//solo per ordini tipo A
select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno
and    num_registrazione=:fl_numero;

select flag_tipo_bcl
into   :ls_tipo_stampa
from   tab_tipi_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;

if ls_tipo_stampa<>"A" then return false


setnull(ll_null)

luo_prod = create uo_produzione
li_risposta = luo_prod.uof_controlla_ologrammi(false, ll_null, fi_anno, fl_numero, fl_riga, ls_errore)

if li_risposta>=0 then
	//gestione ologrammi prevista
	return true
else
	//gestione ologrammi non prevista o errore
	return false
end if

end function

public function integer wf_aggiorna_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore);string									ls_dep_par[], ls_rep_par[], ls_dep_arr[], ls_rep_arr[], ls_vuoto[], ls_cod_prod, ls_riga_ordine
date									ldt_dtp_rep_par[], ldt_dtp_rep_arr[], ldd_vuoto[]
integer								li_ret, ll_index
uo_produzione						luo_prod
uo_calendario_prod_new			luo_cal_prod
string									ls_cod_prodotto[]
long									ll_righe_ordine[], ll_temp
integer								li_i
datetime								ldt_vuoto[], ldt_date_reparti[]


if al_riga_ordine>0  then
	//solo riga corrente
	ll_righe_ordine[1] = al_riga_ordine
	
	select cod_prodotto
	into :ls_cod_prodotto[1]
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine and
				prog_riga_ord_ven=:ll_righe_ordine[1];
	
else
	//il ricalcolo riguarda tutto l'ordine
	declare cu_righe cursor for  
	select prog_riga_ord_ven, cod_prodotto
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine and
				cod_prodotto is not null ;

	open cu_righe;

	if sqlca.sqlcode < 0 then
		as_errore = "Errore in OPEN cursore cu_righe (wf_aggiorna_calendario): " + sqlca.sqlerrtext
		return -1
	end if

	do while true
		
		fetch cu_righe into :ll_temp, :ls_cod_prod;
	
		if sqlca.sqlcode < 0 then
			as_errore = "Errore in fetch cursore cu_righe  (wf_aggiorna_calendario):" + sqlca.sqlerrtext
			close cu_righe;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then exit
		
		li_i += 1
		ll_righe_ordine[li_i] = ll_temp
		ls_cod_prodotto[li_i] = ls_cod_prod
		
	loop
	close cu_righe;
end if
//----------------------------------------------------------------------

//caricati in ll_righe_ordine[] ls_cod_prodotto[] i dati da processare ...
//potrebbero essere quelli di una riga singola o di tutto l'ordine


luo_prod = create uo_produzione
luo_cal_prod = create uo_calendario_prod_new

//controlla se hai deciso di spostare la data partenza dell'intero ordine
if not isnull(idt_data_partenza) and year(date(idt_data_partenza))>1950 then
	luo_prod.idt_data_partenza = idt_data_partenza
end if


for li_i=1 to upperbound(ll_righe_ordine[])
	
	ls_riga_ordine = "Riga Ordine "+string(ai_anno_ordine)+"/"+string(al_num_ordine)+"/"+string(ll_righe_ordine[li_i])+"~r~n~r~n"
	
	ls_dep_par[] = ls_vuoto[]
	ls_rep_par[] = ls_vuoto[]
	ls_dep_arr[] = ls_vuoto[]
	ls_rep_arr[] = ls_vuoto[]
	
	ldt_dtp_rep_par[] = ldd_vuoto[]
	ldt_dtp_rep_arr[] = ldd_vuoto[]
	
	//ricalcolo le date prima del salvataggio definitivo ------
	li_ret = luo_prod.uof_cal_trasferimenti(	ai_anno_ordine, al_num_ordine, &
														ls_cod_prodotto[li_i], "", &
														ls_dep_par[], ls_rep_par[], ldt_dtp_rep_par[], ls_dep_arr[], ls_rep_arr[], ldt_dtp_rep_arr[], as_errore)
	
	//se non ci sono errori salvo -------------------------------
	//se ci sono errori, in questa fase segnalo ma vado avanti nel salvataggio del resto
	//probabilmente non hai trovato navette o eventi
	if li_ret = 0 then
		//se torna ZERO sicuramente ho le date e i reparti
		
		ldt_date_reparti[] =  ldt_vuoto[]
		
		for ll_index = 1 to upperbound(ls_rep_par[])
			
			if ls_rep_arr[ll_index]="" then setnull(ls_rep_arr[ll_index])
			
			update det_ord_ven
			set		cod_reparto_sfuso = :ls_rep_par[ll_index],
					data_pronto_sfuso = :ldt_dtp_rep_par[ll_index],
					cod_reparto_arrivo = :ls_rep_arr[ll_index],
					data_arrivo = :ldt_dtp_rep_arr[ll_index]
			where 	anno_registrazione = :ai_anno_ordine and 
						num_registrazione = :al_num_ordine and
						prog_riga_ord_ven = :ll_righe_ordine[li_i];
			
			if sqlca.sqlcode < 0 then
				as_errore = "Errore in update det_ord_ven (cod_reparto_sfuso-data_pronto_sfuso).~r~n" + sqlca.sqlerrtext
				
				destroy luo_prod
				destroy uo_calendario_prod_new
				return -1
			end if
			
			ldt_date_reparti[ll_index] = datetime(ldt_dtp_rep_par[ll_index] ,00:00:00)
			
		next
	
		//se arrivi fin qui aggiorna l'impegno in calendario
		if upperbound(ls_rep_par[])>0 then
			li_ret = luo_cal_prod. uof_salva_in_calendario(	ai_anno_ordine,al_num_ordine, ll_righe_ordine[li_i], &
																	ls_rep_par[], ldt_date_reparti[], as_errore)
			
			if li_ret < 0 then
				destroy luo_prod
				destroy uo_calendario_prod_new
				return -1
			end if
		end if
		
	else
		as_errore = ls_riga_ordine + as_errore
		destroy luo_prod
		destroy uo_calendario_prod_new
		return 1
	end if
next

destroy luo_prod
destroy uo_calendario_prod_new


return 0
end function

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det_ord_ven_det_1, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco<>'S'")
f_po_loaddddw_dw(dw_det_ord_ven_lista, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco<>'S'")
f_po_loaddddw_dw(dw_det_ord_ven_det_1, &
                 "cod_misura", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_ord_ven_det_1, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_ord_ven_det_1, &
                 "cod_tipo_causa_sospensione", &
                 sqlca, &
                 "tab_tipi_cause_sospensione", &
                 "cod_tipo_causa_sospensione", &
                 "des_tipo_causa_sospensione", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;string									ls_flag, ls_path
windowobject						lw_oggetti[]


guo_functions.uof_get_parametro_azienda( "OCF", is_cod_cat_mer_optional_conf)
if isnull(is_cod_cat_mer_optional_conf) then is_cod_cat_mer_optional_conf = ""


dw_det_ord_ven_lista.set_dw_key("cod_azienda")
dw_det_ord_ven_lista.set_dw_key("anno_registrazione")
dw_det_ord_ven_lista.set_dw_key("num_registrazione")

dw_det_ord_ven_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + &
                                    c_nohighlightselected + c_ViewModeBorderUnchanged + c_NoCursorRowFocusRect)
												
dw_det_ord_ven_det_1.set_dw_options(sqlca, &
                                    dw_det_ord_ven_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
dw_comp_det_ord_ven.set_dw_options(sqlca, &
                                   dw_det_ord_ven_lista, &
											  c_scrollparent+c_nonew+c_nodelete, &
											  c_default)

lw_oggetti[1] = dw_det_ord_ven_lista
lw_oggetti[2] = uo_1
lw_oggetti[3] = uo_stato_prod
lw_oggetti[4] = cb_fasi
dw_folder.fu_assigntab(1, "Lista", lw_oggetti[])

lw_oggetti[1] = dw_det_ord_ven_det_1
lw_oggetti[2] = uo_1
lw_oggetti[3] = uo_stato_prod
lw_oggetti[4] = cb_fasi
lw_oggetti[5] = dw_documenti
dw_folder.fu_assigntab(2, "Dettaglio", lw_oggetti[])

lw_oggetti[1] = dw_comp_det_ord_ven
lw_oggetti[2] = cb_cancella_commessa
lw_oggetti[3] = cb_ricalcola_configurazione
lw_oggetti[4] = st_avanzamento
lw_oggetti[5] = cb_lancio_prod_riga
lw_oggetti[6] = dw_det_ord_ven_det_conf_variabili
dw_folder.fu_assigntab(3, "Complementi", lw_oggetti[])

dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

dw_documenti.uof_set_management("ORDVEN", dw_det_ord_ven_lista)
dw_documenti.settransobject(sqlca)
dw_documenti.object.p_collegato.FileName = s_cs_xx.volume + s_cs_xx.risorse + + "menu\indietro.png"
dw_documenti.uof_enabled_delete_blob()

iuo_dw_main = dw_det_ord_ven_lista
cb_prezzo.enabled=false
cb_c_industriale.enabled = false
cb_blocca.enabled = false
cb_sblocca.enabled = false
cb_assegnazione.enabled = false
cb_azzera.enabled = false
cb_corrispondenze.enabled = false

ls_flag = "N"
select flag
into   :ls_flag
from   parametri_azienda	
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and
		 parametri_azienda.cod_parametro = 'VPR';
if ls_flag = 'N' then
	dw_det_ord_ven_det_1.object.provvigione_1.visible = 0
	dw_det_ord_ven_det_1.object.provvigione_1_t.visible = 0
	dw_det_ord_ven_det_1.object.provvigione_2.visible = 0
	dw_det_ord_ven_det_1.object.provvigione_2_t.visible = 0
end if
//uo_1.hide()

// mi posizioni sull'ultima riga
postevent("ue_posiziona_riga")


//------------------------------------------------------------------------------------
ls_path = s_cs_xx.volume + s_cs_xx.risorse + "11.5\forklift.png"
dw_trasferimenti.object.p_forklift.FileName = ls_path

ls_path = s_cs_xx.volume + s_cs_xx.risorse + "11.5\truck.png"
dw_trasferimenti.object.p_truck.FileName = ls_path

ls_path = s_cs_xx.volume + s_cs_xx.risorse + "menu\icona_utente.png"
dw_trasferimenti.object.p_cliente.FileName = ls_path
//------------------------------------------------------------------------------------

try
	dw_det_ord_ven_det_1.object.p_commessa.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
catch (runtimeerror err)
end try

guo_functions.uof_get_parametro( "CCU", il_CCU)
if isnull(il_CCU) or il_CCU<0 then il_CCU = 0

dw_det_ord_ven_lista.is_cod_parametro_blocco_prodotto="POC"
dw_det_ord_ven_det_1.is_cod_parametro_blocco_prodotto="POC"



end event

on w_det_ord_ven_tv.create
int iCurrent
call super::create
this.dw_det_ord_ven_det_conf_variabili=create dw_det_ord_ven_det_conf_variabili
this.st_margine=create st_margine
this.cb_unisci_doc=create cb_unisci_doc
this.cb_1=create cb_1
this.dw_trasferimenti=create dw_trasferimenti
this.dw_documenti=create dw_documenti
this.cb_ologrammi=create cb_ologrammi
this.cb_lancio_prod_riga=create cb_lancio_prod_riga
this.uo_stato_prod=create uo_stato_prod
this.cb_fasi=create cb_fasi
this.uo_1=create uo_1
this.cb_assegnazione=create cb_assegnazione
this.cb_azzera=create cb_azzera
this.cb_blocca=create cb_blocca
this.cb_corrispondenze=create cb_corrispondenze
this.cb_prezzo=create cb_prezzo
this.cb_sblocca=create cb_sblocca
this.cb_sconti=create cb_sconti
this.cb_configuratore=create cb_configuratore
this.cb_c_industriale=create cb_c_industriale
this.cb_ricalcola_configurazione=create cb_ricalcola_configurazione
this.st_avanzamento=create st_avanzamento
this.cb_cancella_commessa=create cb_cancella_commessa
this.dw_det_ord_ven_det_1=create dw_det_ord_ven_det_1
this.dw_comp_det_ord_ven=create dw_comp_det_ord_ven
this.dw_folder=create dw_folder
this.dw_det_ord_ven_lista=create dw_det_ord_ven_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_det_ord_ven_det_conf_variabili
this.Control[iCurrent+2]=this.st_margine
this.Control[iCurrent+3]=this.cb_unisci_doc
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.dw_trasferimenti
this.Control[iCurrent+6]=this.dw_documenti
this.Control[iCurrent+7]=this.cb_ologrammi
this.Control[iCurrent+8]=this.cb_lancio_prod_riga
this.Control[iCurrent+9]=this.uo_stato_prod
this.Control[iCurrent+10]=this.cb_fasi
this.Control[iCurrent+11]=this.uo_1
this.Control[iCurrent+12]=this.cb_assegnazione
this.Control[iCurrent+13]=this.cb_azzera
this.Control[iCurrent+14]=this.cb_blocca
this.Control[iCurrent+15]=this.cb_corrispondenze
this.Control[iCurrent+16]=this.cb_prezzo
this.Control[iCurrent+17]=this.cb_sblocca
this.Control[iCurrent+18]=this.cb_sconti
this.Control[iCurrent+19]=this.cb_configuratore
this.Control[iCurrent+20]=this.cb_c_industriale
this.Control[iCurrent+21]=this.cb_ricalcola_configurazione
this.Control[iCurrent+22]=this.st_avanzamento
this.Control[iCurrent+23]=this.cb_cancella_commessa
this.Control[iCurrent+24]=this.dw_det_ord_ven_det_1
this.Control[iCurrent+25]=this.dw_comp_det_ord_ven
this.Control[iCurrent+26]=this.dw_folder
this.Control[iCurrent+27]=this.dw_det_ord_ven_lista
end on

on w_det_ord_ven_tv.destroy
call super::destroy
destroy(this.dw_det_ord_ven_det_conf_variabili)
destroy(this.st_margine)
destroy(this.cb_unisci_doc)
destroy(this.cb_1)
destroy(this.dw_trasferimenti)
destroy(this.dw_documenti)
destroy(this.cb_ologrammi)
destroy(this.cb_lancio_prod_riga)
destroy(this.uo_stato_prod)
destroy(this.cb_fasi)
destroy(this.uo_1)
destroy(this.cb_assegnazione)
destroy(this.cb_azzera)
destroy(this.cb_blocca)
destroy(this.cb_corrispondenze)
destroy(this.cb_prezzo)
destroy(this.cb_sblocca)
destroy(this.cb_sconti)
destroy(this.cb_configuratore)
destroy(this.cb_c_industriale)
destroy(this.cb_ricalcola_configurazione)
destroy(this.st_avanzamento)
destroy(this.cb_cancella_commessa)
destroy(this.dw_det_ord_ven_det_1)
destroy(this.dw_comp_det_ord_ven)
destroy(this.dw_folder)
destroy(this.dw_det_ord_ven_lista)
end on

event pc_delete;call super::pc_delete;long ll_i

for ll_i = 1 to dw_det_ord_ven_lista.deletedcount()
	if dw_det_ord_ven_lista.getitemstring(ll_i, "flag_blocco", delete!, true) = "S" then
		g_mb.messagebox("Attenzione", "Ordine non cancellabile! E' stato bloccato.", exclamation!, ok!)
		dw_det_ord_ven_lista.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	if dw_det_ord_ven_lista.getitemnumber(ll_i, "anno_commessa", delete!, true) > 0 or not isnull(dw_det_ord_ven_lista.getitemnumber(ll_i, "anno_commessa", delete!, true)) then
		g_mb.messagebox("Attenzione", "Riga d'ordine non cancellabile! E' collegata ad una commessa di produzione.", exclamation!, ok!)
		dw_det_ord_ven_lista.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	
next

cb_c_industriale.enabled = false
cb_blocca.enabled = false
cb_sblocca.enabled = false
cb_assegnazione.enabled = false
cb_azzera.enabled = false
cb_corrispondenze.enabled = false
cb_cancella_commessa.enabled = false
cb_ricalcola_configurazione.enabled = false

end event

event pc_modify;call super::pc_modify;if dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Ordine non modificabile! E' stato bloccato.", &
              exclamation!, ok!)
   dw_det_ord_ven_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Ordine non modificabile! E' stato bloccato.", &
              exclamation!, ok!)
   dw_det_ord_ven_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

end event

event pc_new;boolean lb_riga_configurata = false
string ls_cod_prodotto
long ll_getrow, ll_row, ll_max_riga, ll_anno_registrazione, ll_num_registrazione, ll_max, ll_cont

// ************************************************************************************************************
// 
// 
//     ATTENZIONE !!!!!!!!!!!!!!   IN QUESTO EVENTO DEVE ESSERE DISATTIVATO L'INDICATIVO EXTEND ANCESTOR SCRIPT
//
//
// *************************************************************************************************************

if dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Ordine non modificabile! E' stato bloccato.",  exclamation!, ok!)
   dw_det_ord_ven_lista.set_dw_view(c_ignorechanges)
   dw_det_ord_ven_det_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if


if dw_det_ord_ven_lista.getrow() > 0 then
	ls_cod_prodotto = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_prodotto")
	if not isnull(ls_cod_prodotto) then
		ll_cont = 0
		select count(*)
		into   :ll_cont
		from   tab_flags_configuratore
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_modello = :ls_cod_prodotto;
		if ll_cont > 0 then
			lb_riga_configurata = true
		end if
	end if
end if

if (dw_det_ord_ven_lista.getrow() = dw_det_ord_ven_lista.rowcount() or dw_det_ord_ven_lista.getrow() < 1) and not(lb_riga_configurata) then
	il_num_riga = 0
	il_riga_riferimento = 0
else
	ll_anno_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "num_registrazione")
	ll_getrow = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"prog_riga_ord_ven")
	ll_max = ceiling((ll_getrow + 1) / 10) * 10
	ll_max_riga = 0
	select max(prog_riga_ord_ven)
	into   :ll_max_riga
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 prog_riga_ord_ven >= :ll_getrow and
			 prog_riga_ord_ven < :ll_max;
	if ll_max_riga = 0 or isnull(ll_max_riga) then
		il_num_riga = 0
		il_riga_riferimento = 0
	else
		il_num_riga = ll_max_riga + 1
		il_riga_riferimento = ll_getrow
	end if
end if

if isvalid(iuo_dw_main) then
   iuo_dw_main.change_dw_current()
end if

call super::pc_new
end event

event close;call super::close;if len(is_text_storico) > 0 and not isnull(is_text_storico) then
	
	if g_mb.messagebox("APICE", "Stampo il testo ESCLUSIONI della storicizzazione?",Question!,YesNo!,1) = 1 then
		
		long Job
		
		job = printsetup()
		
		if job < 1 then return
		
		Job = PrintOpen( )
		
		if job < 1 then return
		
		Print( Job,is_text_storico)
		
		PrintClose(Job)

	end if
	
end if

destroy iuo_condizioni_cliente
destroy iuo_gestione_conversioni
destroy iuo_produzione

end event

event open;call super::open;iuo_condizioni_cliente=create uo_condizioni_cliente
iuo_gestione_conversioni = create uo_gestione_conversioni

is_text_storico = ""
end event

event activate;call super::activate;dw_det_ord_ven_lista.setrowfocusindicator(hand!)

end event

type dw_det_ord_ven_det_conf_variabili from uo_std_dw within w_det_ord_ven_tv
integer x = 2976
integer y = 136
integer width = 1307
integer height = 1820
integer taborder = 30
string dataobject = "d_det_ord_ven_det_conf_variabili"
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

type st_margine from statictext within w_det_ord_ven_tv
integer x = 2811
integer y = 2344
integer width = 1495
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_unisci_doc from commandbutton within w_det_ord_ven_tv
integer x = 1202
integer y = 2340
integer width = 366
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Unisci Doc."
end type

event clicked;long										ll_row, ll_num, ll_riga
integer									li_anno, li_ret
uo_service_doc_ord_ven			luo_doc
string									ls_errore


ll_row = dw_det_ord_ven_lista.getrow()

if isnull(ll_row) or ll_row = 0 then
	g_mb.warning("APICE","Selezionare una riga di dettaglio prima di continuare!")
	return
end if

li_anno = dw_det_ord_ven_lista.getitemnumber(ll_row,"anno_registrazione")
ll_num = dw_det_ord_ven_lista.getitemnumber(ll_row,"num_registrazione")
ll_riga = dw_det_ord_ven_lista.getitemnumber(ll_row,"prog_riga_ord_ven")

if isnull(li_anno) or li_anno = 0 or isnull(ll_num) or ll_num = 0 or isnull(ll_riga) or ll_riga = 0 then
	g_mb.warning("APICE","Selezionare una riga di dettaglio prima di continuare!")
	return
end if

if not g_mb.confirm("Procedere all'unione dei documenti della riga per tipologia (P-T-O)?") then return

setpointer(Hourglass!)

luo_doc = create uo_service_doc_ord_ven
li_ret = luo_doc.uof_unisci_documenti(li_anno, ll_num, ll_riga, ls_errore)
destroy luo_doc

setpointer(Arrow!)

if li_ret < 0 then
	//rollback già fatto
	g_mb.error(ls_errore)
	
else
	//commit già fatto, in verità per ogni riga/tipologia
	g_mb.success("Operazione terminata!")
	
end if

dw_documenti.uof_retrieve_blob(ll_row)




end event

type cb_1 from commandbutton within w_det_ord_ven_tv
integer x = 818
integer y = 2340
integer width = 366
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "MRP"
end type

event clicked;string ls_errore, ls_null
long ll_lead_time_cumulato, ll_ret
datetime ldt_null
uo_mrp luo_mrp
s_fabbisogno_commessa lstr_fabbisogno[]

setnull(ls_null)
setnull(ldt_null)

luo_mrp = create uo_mrp
luo_mrp.ib_data_fabbisogno_da_varianti = true
luo_mrp.ib_verifica_avanz_fasi = false

ll_ret = luo_mrp.uof_calcolo_fabbisogni_comm( false, &
														"varianti_det_ord_ven", &
														dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"anno_registrazione"), &
														dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"num_registrazione"), &
														dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"prog_riga_ord_ven"), &
														dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(),"cod_prodotto"), &
														dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(),"cod_versione"), &
														dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"quan_ordine"), &
														0,  &
														ls_null,&
														ldt_null,&
														ref ll_lead_time_cumulato, &
														ref lstr_fabbisogno[], &
														ref ls_errore)
														
if ll_ret < 0 then
	g_mb.error(ls_errore)
end if
rollback;

end event

type dw_trasferimenti from datawindow within w_det_ord_ven_tv
integer x = 4325
integer y = 20
integer width = 686
integer height = 2412
integer taborder = 140
string title = "none"
string dataobject = "d_ext_trasferimenti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;boolean								lb_ricalcola, lb_esiste_riga
datetime								ldt_data_partenza
long									ll_index, ll_count, ll_riga_ordine, ll_num_ordine, ll_temp
uo_calendario_prod_new			luo_cal_prod
string									ls_errore, ls_cod_prodotto, ls_flag_tipo_bcl, ls_cod_tipo_ord_ven
integer								li_anno_ordine, li_ret


lb_ricalcola = false
lb_esiste_riga = false
ll_riga_ordine = -1

if ii_anno_ordine>0 and il_num_ordine>0 then
else
	return
end if

ls_cod_tipo_ord_ven = f_des_tabella ("tes_ord_ven", "anno_registrazione="+string(ii_anno_ordine)+ " and num_registrazione="+string(il_num_ordine), "cod_tipo_ord_ven" )

//ls_flag_tipo_bcl = A - B - C
select flag_tipo_bcl
into :ls_flag_tipo_bcl
from tab_tipi_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;
			
if ls_flag_tipo_bcl<>"B" then return
			

ll_index = dw_det_ord_ven_lista.getrow()
if ll_index>0 then
	ll_riga_ordine = dw_det_ord_ven_lista.getitemnumber(ll_index, "prog_riga_ord_ven")
	
	if ll_riga_ordine>0 then
		
		setnull(ll_temp)
		
		select prog_riga_ord_ven
		into :ll_temp
		from det_ord_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ii_anno_ordine and
					num_registrazione=:il_num_ordine and
					prog_riga_ord_ven=:ll_riga_ordine;
		
		
		if ll_temp>0 then
			//riga esistente
		else
			ll_riga_ordine = 0
		end if
		
	else
		ll_riga_ordine = 0
	end if
	
else
	return
end if


//
//if s_cs_xx.admin then
//	if ll_index > 0 and ll_riga_ordine>0 and rowcount()>0 then
//		lb_ricalcola = g_mb.confirm("Premere SI per effettuare il ricalcolo del log dei trasferimenti sulla riga corrente, NO per continuare e visualizzare il calendario produzione!")
//	else
//		lb_ricalcola = true
//	end if
//	
//	if lb_ricalcola then
//		
//		if ii_anno_ordine>0 and il_num_ordine>0 and ll_riga_ordine>0 then
//			lb_esiste_riga = true
//		end if
//		
//		if not lb_esiste_riga then
//			ls_cod_prodotto = dw_det_ord_ven_lista.getitemstring(ll_index, "cod_prodotto")
//			
//			if ls_cod_prodotto="" or isnull(ls_cod_prodotto) then return
//			
//		end if
//		
//		dw_trasferimenti.object.testo_t.text = "DEBUG TRASF."
//		
//		luo_cal_prod = create uo_calendario_prod_new
//		luo_cal_prod.uof_dw_cal_produzione(		lb_esiste_riga, ii_anno_ordine, il_num_ordine, ll_riga_ordine, &
//																ls_cod_prodotto, "", idt_data_partenza, is_reparti[], idt_date_reparti[], dw_trasferimenti, ls_errore)
//		
//		destroy luo_cal_prod
//	
//		return
//	end if
////end if


if row>0 then
else
	return
end if


//se arrivi fin qui visualizza calendario
	
//leggo attuale data partenza dell'ordine, oppure se è stata GIà CAMBIATA PASSO QUELLA ...
if not isnull(idt_data_partenza) and year(date(idt_data_partenza))>1950 then
	ldt_data_partenza = idt_data_partenza
	
else
	select data_consegna
	into :ldt_data_partenza
	from tes_ord_ven
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ii_anno_ordine and
			num_registrazione=:il_num_ordine;

	if sqlca.sqlcode<0 then
		g_mb.error("Errore in lettura data partenza dell'ordine: "+sqlca.sqlerrtext)
		return
	end if

end if

ll_count = 0

s_cs_xx.parametri.parametro_s_4_a[] = is_reparti[]
s_cs_xx.parametri.parametro_data_1_a[] = idt_date_reparti[]
s_cs_xx.parametri.parametro_data_4 = ldt_data_partenza
setnull(ldt_data_partenza)

window_open(w_cal_produzione_reparti, 0)


if s_cs_xx.parametri.parametro_b_2 then
	//data partenza cambiata
	idt_data_partenza = s_cs_xx.parametri.parametro_data_4
	g_mb.success("La Data Partenza sarà cambiata in "+string(idt_data_partenza, "dd/mm/yyyy") + " al salvataggio.")
	dw_det_ord_ven_lista.event trigger ue_cal_trasferimenti()
	
	//##########################################################
	//se sei in visualizza vai avanti
	if dw_det_ord_ven_lista.ib_stato_visualizzazione then
		//salva tutto e subito
		
		li_ret = wf_aggiorna_calendario(ii_anno_ordine, il_num_ordine, 0, ls_errore)
		
		if li_ret<0 then
			s_cs_xx.parametri.parametro_b_2 = false
			setnull(idt_data_partenza)
			g_mb.error("Attenzione", ls_errore)
			rollback;
			return
			
		elseif ls_errore<>"" and li_ret=1 then
			//mostro nel log
			s_cs_xx.parametri.parametro_b_2 = false
			setnull(idt_data_partenza)
			dw_trasferimenti.reset()
			li_ret = dw_trasferimenti.insertrow(0)
			dw_trasferimenti.setitem(li_ret, "testo", ls_errore)
			rollback;
			return
		
		else
			//********************************************************
			
			commit;
			
			if isvalid(s_cs_xx.parametri.parametro_w_ord_ven)  then
	
				//se è stata cambiata la data partenza (ex consegna) allora aggiorna su tutte le righe ed in testata ordine
				//questo, come dopo l'uscita dal configuratore è delegato all'evento ue_cambia_data_partenza della tes_ord_ven
				//attivi questo solo per lo sfuso
				
				if not isnull(idt_data_partenza) and year(date(idt_data_partenza))>=1950 then
					s_cs_xx.parametri.parametro_data_4 = idt_data_partenza
					setnull(idt_data_partenza)
					
					if ii_anno_ordine>0 and il_num_ordine>0 then
						s_cs_xx.parametri.parametro_d_4_a[1] = ii_anno_ordine
						s_cs_xx.parametri.parametro_d_4_a[2] = il_num_ordine
						s_cs_xx.parametri.parametro_w_ord_ven.postevent("ue_cambia_data_partenza")
					end if
				end if
			end if
			//********************************************************
			
		end if
	end if
	//##########################################################
	
	
elseif not isnull(idt_data_partenza) and year(date(idt_data_partenza)) > 1950 then
	//vuol dire che era stata cambiata in un precedente tentativo
	//quindi lascia inalterata idt_data_partenza

else
	s_cs_xx.parametri.parametro_b_2 = false
	setnull(s_cs_xx.parametri.parametro_data_4)
	
end if


return

end event

type dw_documenti from uo_dw_drag_doc_acq_ven within w_det_ord_ven_tv
integer x = 3479
integer y = 140
integer width = 795
integer height = 1812
integer taborder = 20
string dataobject = "d_det_ord_ven_note_blob"
boolean vscrollbar = true
end type

type cb_ologrammi from commandbutton within w_det_ord_ven_tv
integer x = 2354
integer y = 2244
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ologrammi"
end type

event clicked;long ll_row, ll_anno, ll_num, ll_riga


ll_row = dw_det_ord_ven_lista.getrow()

if isnull(ll_row) or ll_row = 0 then
	g_mb.messagebox("APICE","Selezionare una riga di dettaglio prima di continuare!",stopsign!)
	return -1
end if

ll_anno = dw_det_ord_ven_lista.getitemnumber(ll_row,"anno_registrazione")
ll_num = dw_det_ord_ven_lista.getitemnumber(ll_row,"num_registrazione")
ll_riga = dw_det_ord_ven_lista.getitemnumber(ll_row,"prog_riga_ord_ven")

if isnull(ll_anno) or ll_anno = 0 or isnull(ll_num) or ll_num = 0 or isnull(ll_riga) or ll_riga = 0 then
	g_mb.messagebox("APICE","Selezionare una riga di dettaglio prima di continuare!",stopsign!)
	return -1
end if

window_open_parm(w_det_ord_ven_ologramma,-1,dw_det_ord_ven_lista)
end event

type cb_lancio_prod_riga from commandbutton within w_det_ord_ven_tv
integer x = 2638
integer y = 2104
integer width = 366
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Lanc.Prod."
end type

event clicked;long				ll_num_registrazione,ll_riga,ll_selected[],ll_prog_riga_ord_ven,ll_num_reparti,ll_prog_riga_comanda, ll_tot, &
					ll_anno_reg_lancio_produzione, ll_num_reg_lancio_produzione
integer			li_anno_registrazione,li_risposta,li_r, ll_cont

string			ls_cod_prodotto,ls_cod_tipo_det_ven,ls_cod_versione,ls_cod_reparto_test,ls_tipo_stampa,& 
		 			ls_cod_tipo_ord_ven,ls_errore,ls_cod_reparto,ls_cod_reparti_trovati[], ls_selezionato, ls_flag_tipo_bcl
					 
datetime		ldt_data_consegna,ldt_data_consegna_riga

decimal{4} 	ld_quan_ordine
uo_produzione luo_produzione


if not(g_mb.confirm(g_str.format("Eseguo lancio di produzione della riga $1?",ll_prog_riga_ord_ven )) ) then return

setpointer(hourglass!)

luo_produzione = create uo_produzione

li_anno_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow() ,"anno_registrazione")
ll_num_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow() ,"num_registrazione")
ll_prog_riga_ord_ven = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"prog_riga_ord_ven")

select 	cod_tipo_ord_ven, 
			data_consegna
into 		:ls_cod_tipo_ord_ven,
			:ldt_data_consegna
from 		tes_ord_ven
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :li_anno_registrazione and
			num_registrazione = :ll_num_registrazione;

if sqlca.sqlcode <> 0 then
	g_mb.warning("Errore in ricerca testata ordine di vendita~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

select 	flag_tipo_bcl
into		:ls_flag_tipo_bcl
from		tab_tipi_ord_ven
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
if sqlca.sqlcode <> 0 then
	g_mb.warning("Errore in ricerca tipo ordine di vendita~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

if isnull(ls_flag_tipo_bcl) or ls_flag_tipo_bcl <> "A" then
	g_mb.error("Il lancio di produzione della singola riga può essere effettuato SOLO per prodotti confezionati con report produzione tipo A ")
	rollback;
	return
end if
			

select 	count(*) 
into 		:ll_cont
from 		comp_det_ord_ven
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :li_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga_ord_ven;

if sqlca.sqlcode < 0 then
	g_mb.error("Attenzione; questa procedura può essere eseguita solo per prodotti soggetti a configurazione.")
	rollback;
	return
end if

li_risposta = luo_produzione.uof_controlla_produzione(ll_num_registrazione,&
																		li_anno_registrazione,&
																		ll_prog_riga_ord_ven, &
																		ls_errore)

choose case li_risposta
	case is < 0
		
		if pos(ls_errore, "Procedure has not been executed or has no results") > 0 then
			ls_errore = "Problema lettura righe selezionate: riprova a cliccare sul pulsante LANCIO PRODUZIONE"
		end if
		
		messagebox("SEP",ls_errore,stopsign!)
		destroy luo_produzione;
		
		rollback;
		return
	
	case 1 // non è un errore ma significa che esiste almeno una sessione di lavoro
		li_r = messagebox("SEP","Attenzione! Nell'ordine " + string(ll_num_registrazione) + "/" + & 
		string(li_anno_registrazione) + " esistono già delle rilevazioni di produzione." + &  
		" Se si procede con il lancio di produzione, tali rilevazioni andranno perdute." + &  
		" Vuoi procedere con il lancio di produzione per questo ordine?",question!,yesnocancel!,1)

		choose case li_r
			case 1
				// non fa niente e continua il processo normalmente
				
			case 2
				// skippa all'ordine successivo
				rollback;
				return
				
			case 3
				// esce dalla procedure e annulla le modifiche
				messagebox("SEP","Processo annullato dall'operatore, tutte le modifiche sono state annullate",stopsign!)
				destroy luo_produzione;
				rollback;
				return
				
		end choose
	

	case 2,3 //  se 2 non è un errore ma significa che almeno un record in det_ordini_produzione
				// se 3 non è un errore ma significa che almeno un record in tes_ordini_produzione
		li_r = messagebox("SEP","Attenzione! La produzione per l'ordine " + string(ll_num_registrazione) + "/" + & 
		string(li_anno_registrazione) + " è già stata lanciata." + &  
		" Si può comunque procedere alla ripetizione del lancio di produzione. In questo modo se ci sono" + &  
		" state delle modifiche alla struttura del prodotto saranno riprocessate e reinserite in produzione." + &
		" Vuoi procedere con il lancio di produzione per questo ordine?",question!,yesnocancel!,1)

		choose case li_r
			case 1
				// non fa niente e continua il processo normalmente
				
			case 2
				// skippa all'ordine successivo
				rollback;
				return
				
			case 3
				// esce dalla procedure e annulla le modifiche
				messagebox("SEP","Processo annullato dall'operatore, tutte le modifiche sono state annullate",stopsign!)
				destroy luo_produzione;
				rollback;
				return
				
		end choose


	case 0 // procede poichè significa che non vi è alcun record nelle tabelle di produzione
		// procede regolarmente 
	
end choose


li_risposta = luo_produzione.uof_elimina_produzione_riga(ll_num_registrazione,&
																	 li_anno_registrazione,&
																	 ll_prog_riga_ord_ven, &
																	 "D", &
																	 ls_errore)
															 
if li_risposta < 0 then
	messagebox("SEP",ls_errore,stopsign!)
	destroy luo_produzione;
	rollback;
	return
end if


li_risposta = luo_produzione.uof_lancia_produzione(	ll_num_registrazione, &
																	li_anno_registrazione, &
																	ll_prog_riga_ord_ven, &
																	ls_cod_tipo_ord_ven, &
																	ldt_data_consegna, &
																	ls_errore)
																	

if li_risposta < 0 then
	messagebox("SEP",ls_errore,stopsign!)
	destroy luo_produzione;
	rollback;
	return
end if

destroy luo_produzione;

commit;
setpointer(arrow!)
g_mb.warning(g_str.format("Processo di lancio di produzione della riga $1 completato; creato lancio produzione $2/$3.",ll_prog_riga_ord_ven,ll_anno_reg_lancio_produzione,ll_num_reg_lancio_produzione ))


end event

type uo_stato_prod from uo_stato_produzione within w_det_ord_ven_tv
event destroy ( )
integer x = 2775
integer y = 2240
integer width = 1527
integer taborder = 100
end type

on uo_stato_prod.destroy
call uo_stato_produzione::destroy
end on

type cb_fasi from commandbutton within w_det_ord_ven_tv
integer x = 1970
integer y = 2244
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Fasi"
end type

event clicked;long ll_row, ll_anno, ll_num, ll_riga


ll_row = dw_det_ord_ven_lista.getrow()

if isnull(ll_row) or ll_row = 0 then
	g_mb.messagebox("APICE","Selezionare una riga di dettaglio prima di continuare!",stopsign!)
	return -1
end if

ll_anno = dw_det_ord_ven_lista.getitemnumber(ll_row,"anno_registrazione")

ll_num = dw_det_ord_ven_lista.getitemnumber(ll_row,"num_registrazione")

ll_riga = dw_det_ord_ven_lista.getitemnumber(ll_row,"prog_riga_ord_ven")

if isnull(ll_anno) or ll_anno = 0 or isnull(ll_num) or ll_num = 0 or isnull(ll_riga) or ll_riga = 0 then
	g_mb.messagebox("APICE","Selezionare una riga di dettaglio prima di continuare!",stopsign!)
	return -1
end if

window_open_parm(w_det_ord_ven_fasi,-1,dw_det_ord_ven_lista)
end event

type uo_1 from uo_situazione_prodotto within w_det_ord_ven_tv
integer x = 46
integer y = 1956
integer width = 4210
integer height = 252
integer taborder = 50
boolean border = false
end type

on uo_1.destroy
call uo_situazione_prodotto::destroy
end on

type cb_assegnazione from commandbutton within w_det_ord_ven_tv
event clicked pbm_bnclicked
integer x = 425
integer y = 2340
integer width = 366
integer height = 80
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Assegna"
end type

event clicked;integer				li_return

long					ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_anno_commessa, &
						ll_num_commessa, ll_riga
						
string					ls_cod_tipo_det_ven, ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_flag_tipo_det_ven,ls_cod_tipo_causa_sospensione,ls_des_tipo_causa_sospensione, ls_cod_giro_consegna, ls_cod_dep_giro

double				ld_quan_evasa, ld_quan_in_evasione, ld_quan_commessa

uo_produzione		luo_prod
uo_log_sistema		luo_log



if dw_det_ord_ven_lista.getrow() > 0 then
	if dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_fuori_fido") = "N" then

		if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_evasione") <> "E" and &
		   		dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_blocco") = "N" and &
				dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_in_evasione") = 0 then

			ll_anno_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")
			ll_num_registrazione  = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione")
			ll_anno_commessa  = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_commessa")
			ll_num_commessa   = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_commessa")
			ls_cod_deposito   = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_deposito")
			ls_cod_giro_consegna   = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_giro_consegna")
			
			
			ll_prog_riga_ord_ven = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven")
			
			//----------------------------------------------------------------------------------
			//se l'ordine ha un giro consegna con deposito applicato e diverso da quello origine ordine, allora il deposito da assegnare deve essere questo
			luo_prod = create uo_produzione
			ls_cod_dep_giro = luo_prod.uof_deposito_giro_consegna(ls_cod_giro_consegna)
			destroy luo_prod
			
			if g_str.isnotempty(ls_cod_dep_giro) and ls_cod_deposito<>ls_cod_dep_giro then
				ls_cod_deposito = ls_cod_dep_giro
				
				luo_log = create uo_log_sistema
				luo_log.uof_write_log_sistema_not_sqlca("DEP.CONS.ASSEGN", "Applicato deposito codice "+ls_cod_dep_giro+" del  giro consegna dell'ordine "+string(ll_anno_registrazione)+"/" + string(ll_num_registrazione)+&
																								" al posto del deposito origine "+ls_cod_deposito+" in assegnazione riga ordine n. " + string(ll_prog_riga_ord_ven) + &
																								" da finestra dettaglio ordine vendita.")
				destroy luo_log
			end if
			//----------------------------------------------------------------------------------
			
			
			
			ls_cod_ubicazione = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_ubicazione")
			ls_cod_tipo_det_ven  = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_tipo_det_ven")
			ls_cod_prodotto = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_prodotto") 
			s_cs_xx.parametri.parametro_d_1 = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_ordine") - &
														  dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_evasa")
			ld_quan_evasa = 0
			
			
			if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_riga_sospesa") = "S" then
				
				ls_cod_tipo_causa_sospensione = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_tipo_causa_sospensione")
				
				select des_tipo_causa_sospensione
				into :ls_des_tipo_causa_sospensione
				from tab_tipi_cause_sospensione
				where cod_azienda = :s_cs_xx.cod_azienda and
							cod_tipo_causa_sospensione = :ls_cod_tipo_causa_sospensione;
							
				if g_mb.messagebox("APICE", "La riga d'ordine risulta sospesa per la seguente causa: " + ls_des_tipo_causa_sospensione + ".~r~nPROSEGUO ?", Question!, YesNo!, 2) = 2 then return
			end if

			if ll_anno_commessa <> 0 and not isnull(ll_anno_commessa) then
				s_cs_xx.parametri.parametro_d_2 = ll_anno_commessa
				s_cs_xx.parametri.parametro_d_3 = ll_num_commessa
			else
				setnull(s_cs_xx.parametri.parametro_d_2)
				setnull(s_cs_xx.parametri.parametro_d_3)
			end if

			select tab_tipi_det_ven.flag_tipo_det_ven
			into   :ls_flag_tipo_det_ven
			from   tab_tipi_det_ven
			where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettagli.", &
							  exclamation!, ok!)
				return
			end if
			
			s_cs_xx.parametri.parametro_s_4 = ls_cod_tipo_det_ven
			s_cs_xx.parametri.parametro_ul_1 = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")
			s_cs_xx.parametri.parametro_ul_2 = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione")
			s_cs_xx.parametri.parametro_ul_3 = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven")
			s_cs_xx.parametri.parametro_s_1 = ls_cod_prodotto
			s_cs_xx.parametri.parametro_s_2 = ls_cod_deposito
			s_cs_xx.parametri.parametro_s_15 = "OC"
			if isnull(ls_cod_ubicazione) or ls_cod_ubicazione = "" then
				s_cs_xx.parametri.parametro_s_3 = "%"
			else
				s_cs_xx.parametri.parametro_s_3 = ls_cod_ubicazione
			end if

			window_open(w_ass_stock, 0)
			
			ll_riga = dw_det_ord_ven_lista.getrow()
			dw_det_ord_ven_lista.triggerevent("pcd_retrieve")
			dw_det_ord_ven_lista.scrolltorow(ll_riga)
		else
			g_mb.messagebox("APICE","Riga già evasa, bloccata o in corso di evasione: impossibile assegnare")	
		end if
	end if

end if

commit;

end event

type cb_azzera from commandbutton within w_det_ord_ven_tv
event clicked pbm_bnclicked
integer x = 37
integer y = 2340
integer width = 366
integer height = 80
integer taborder = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A&zzera Ass"
end type

event clicked;integer								li_return
long									ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven
string									ls_messaggio, ls_fuori_fido, ls_flag_evasione, ls_flag_blocco
uo_generazione_documenti		luo_gen_doc

if dw_det_ord_ven_lista.getrow() > 0 then
	
	ls_fuori_fido = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_fuori_fido")
	ls_flag_evasione = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_evasione")
	ls_flag_blocco = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_blocco")
	
	if ls_fuori_fido = "S" then
		g_mb.warning("Attenzione", "Azzeramento Assegnazione non avvenuta: Testata Ordine Fuori Fido!")
		return
		
	elseif ls_flag_evasione="E" then
		g_mb.warning("Attenzione", "Azzeramento Assegnazione non avvenuta: Riga già Evasa!")
		return
		
	elseif ls_flag_blocco="S" then
		g_mb.warning("Attenzione", "Azzeramento Assegnazione non avvenuta: Riga Bloccata!")
		return
		
	end if
	
	//se arrivi fin qui, prosegui ...

	ll_anno_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione")
	ll_prog_riga_ord_ven = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven")
	setnull(ls_messaggio)
	
	luo_gen_doc = create uo_generazione_documenti
	li_return = luo_gen_doc.uof_azz_ass_ord_ven_sessione( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven,0, ls_messaggio)
	destroy luo_gen_doc
	
	if not isnull(ls_messaggio) then g_mb.messagebox("Azzeramento assegnazione",ls_messaggio, information!)

	if li_return <> 0 then
		dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "quan_in_evasione", 0)
		dw_det_ord_ven_det_1.setitemstatus(dw_det_ord_ven_det_1.getrow(), "quan_in_evasione", primary!, notmodified!)
	end if
	
	if li_return = 0 then
		g_mb.warning("Attenzione", "Azzeramento Assegnazione non avvenuta.")
		return
	end if

	g_mb.success("Informazione", "Azzeramento Assegnazione avvenuta con successo.")
	
end if
end event

type cb_blocca from commandbutton within w_det_ord_ven_tv
event clicked pbm_bnclicked
boolean visible = false
integer x = 498
integer y = 2480
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Blocca"
end type

event clicked;//donato 25/01/2012
//commentato perchè il pulsante è stato reso invisibile su richiesta di Beatrice
//###########################################################


//string ls_cod_tipo_det_ven, ls_cod_prodotto, ls_flag_tipo_det_ven,ls_cod_prodotto_raggruppato
//dec{4} ld_quan_ordine, ld_quan_in_evasione, ld_quan_evasa, ld_quantita, ld_quan_raggruppo
//long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_i, ll_blocco
//
//
//if dw_det_ord_ven_lista.getrow() > 0 then
//	if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_blocco") = "N" then
//		if dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_in_evasione") > 0 then
//			g_mb.messagebox("Attenzione", "Dettaglio non chiudibile. E' presente della quantità in evasione.", exclamation!, ok!)
//			return
//		end if
//		
//		ls_cod_tipo_det_ven = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_tipo_det_ven")
//		ls_cod_prodotto = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_prodotto")
//		ld_quan_ordine = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_ordine")
//		ld_quan_evasa = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_evasa")
//		
//		select tab_tipi_det_ven.flag_tipo_det_ven
//		into   :ls_flag_tipo_det_ven
//		from   tab_tipi_det_ven
//		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
//				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
//		
//		if sqlca.sqlcode = -1 then
//			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", exclamation!, ok!)
//			return
//		end if
//		
//		if ls_flag_tipo_det_ven = "M" and ld_quan_evasa < ld_quan_ordine then
//			update anag_prodotti  
//				set quan_impegnata = quan_impegnata - (:ld_quan_ordine - :ld_quan_evasa)
//			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
//					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
//		
//			if sqlca.sqlcode = -1 then
//				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
//				rollback;
//				return
//			end if
//			
//			// enme 08/1/2006 gestione prodotto raggruppato
//			setnull(ls_cod_prodotto_raggruppato)
//			
//			select cod_prodotto_raggruppato
//			into   :ls_cod_prodotto_raggruppato
//			from   anag_prodotti
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//					 cod_prodotto = :ls_cod_prodotto;
//					 
//			if not isnull(ls_cod_prodotto_raggruppato) then
//				
//				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordine - ld_quan_evasa), "M")
//				
//				update anag_prodotti  
//					set quan_impegnata = quan_impegnata - :ld_quan_raggruppo
//				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
//						 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
//			
//				if sqlca.sqlcode = -1 then
//					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", exclamation!, ok!)
//					rollback;
//					return
//				end if
//			end if
//			
//			
//			
//		end if
//
//		ll_anno_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")
//		ll_num_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione")
//		ll_prog_riga_ord_ven = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven")
//
//		update det_ord_ven
//			set flag_blocco = 'S'
//		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//				 det_ord_ven.anno_registrazione = :ll_anno_registrazione and  
//				 det_ord_ven.num_registrazione = :ll_num_registrazione and
//				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
//
//		if sqlca.sqlcode = -1 then
//			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio ordine.", &
//						  exclamation!, ok!)
//			rollback;
//			return
//		end if
//
//		dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "flag_blocco", "S")
//		dw_det_ord_ven_det_1.setitemstatus(dw_det_ord_ven_det_1.getrow(), "flag_blocco", primary!, notmodified!)
//
//		ll_blocco = 0
//
//		for ll_i = 1 to dw_det_ord_ven_det_1.rowcount()
//			if dw_det_ord_ven_lista.getitemstring(ll_i, "flag_blocco") = "S" then
//				ll_blocco ++
//			end if
//		next
//
//		if ll_blocco = dw_det_ord_ven_det_1.rowcount() then
//			update tes_ord_ven
//			set flag_blocco = 'S'
//			where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//					tes_ord_ven.anno_registrazione = :ll_anno_registrazione and  
//					tes_ord_ven.num_registrazione = :ll_num_registrazione;
//
//			if sqlca.sqlcode = -1 then
//				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata ordine.", exclamation!, ok!)
//				dw_det_ord_ven_lista.i_parentdw.setitem(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco", "N")
//				dw_det_ord_ven_lista.i_parentdw.setitemstatus(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco", primary!, notmodified!)
//				rollback;
//				return
//			end if
//			dw_det_ord_ven_lista.i_parentdw.setitem(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco", "S")
//			dw_det_ord_ven_lista.i_parentdw.setitemstatus(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco", primary!, notmodified!)
//		end if
//		commit;
//	end if
//end if
end event

type cb_corrispondenze from commandbutton within w_det_ord_ven_tv
event clicked pbm_bnclicked
integer x = 37
integer y = 2244
integer width = 366
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corrispond."
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "Ord_Ven"
window_open_parm(w_det_acq_ven_corr, -1, dw_det_ord_ven_det_1)

end event

type cb_prezzo from commandbutton within w_det_ord_ven_tv
event clicked pbm_bnclicked
integer x = 1202
integer y = 2244
integer width = 366
integer height = 80
integer taborder = 150
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Prezzo"
end type

event clicked;dw_det_ord_ven_det_1.setcolumn(6)
s_cs_xx.parametri.parametro_s_1 = dw_det_ord_ven_det_1.gettext()
dw_det_ord_ven_det_1.setcolumn(10)
s_cs_xx.parametri.parametro_d_1 = double(dw_det_ord_ven_det_1.gettext())
dw_det_ord_ven_det_1.setcolumn(11)
s_cs_xx.parametri.parametro_d_2 = double(dw_det_ord_ven_det_1.gettext())
dw_det_ord_ven_det_1.setcolumn(9)
s_cs_xx.parametri.parametro_d_3 = double(dw_det_ord_ven_det_1.gettext())

window_open(w_prezzo_um, 0)

if s_cs_xx.parametri.parametro_d_1 <> 0 then
   dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "prezzo_vendita", s_cs_xx.parametri.parametro_d_1)
end if

if s_cs_xx.parametri.parametro_d_3 <> 0 then
   dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "quan_ordine", s_cs_xx.parametri.parametro_d_3)
end if
end event

type cb_sblocca from commandbutton within w_det_ord_ven_tv
event clicked pbm_bnclicked
boolean visible = false
integer x = 91
integer y = 2476
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sbl&occa"
end type

event clicked;//donato 25/01/2012
//commentato perchè il pulsante è stato reso invisibile su richiesta di Beatrice
//###########################################################


//string ls_cod_tipo_det_ven, ls_cod_prodotto, ls_flag_tipo_det_ven
//double ld_quan_ordine, ld_quan_evasa, ld_quantita
//long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven
//
//
//if dw_det_ord_ven_lista.getrow() > 0 then
//	if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_blocco") = "S" then
//		ls_cod_tipo_det_ven = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_tipo_det_ven")
//		ls_cod_prodotto = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_prodotto")
//		ld_quan_ordine = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_ordine")
//		ld_quan_evasa = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_evasa")
//
//		select tab_tipi_det_ven.flag_tipo_det_ven
//		into   :ls_flag_tipo_det_ven
//		from   tab_tipi_det_ven
//		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
//				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
//		
//		if sqlca.sqlcode = -1 then
//			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
//						  exclamation!, ok!)
//			return
//		end if
//		
//		if ls_flag_tipo_det_ven = "M" and ld_quan_evasa < ld_quan_ordine then
//			update anag_prodotti  
//				set quan_impegnata = quan_impegnata + (:ld_quan_ordine - :ld_quan_evasa)
//			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
//					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
//		
//			if sqlca.sqlcode = -1 then
//				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", &
//							  exclamation!, ok!)
//				rollback;
//				return
//			end if
//		end if
//
//		ll_anno_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")
//		ll_num_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione")
//		ll_prog_riga_ord_ven = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven")
//
//		update det_ord_ven
//			set flag_blocco = 'N'
//		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//				 det_ord_ven.anno_registrazione = :ll_anno_registrazione and  
//				 det_ord_ven.num_registrazione = :ll_num_registrazione and
//				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
//
//		if sqlca.sqlcode = -1 then
//			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio ordine.", &
//						  exclamation!, ok!)
//			rollback;
//			return
//		end if
//
//		update tes_ord_ven
//		set flag_blocco = 'N'
//		where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//				tes_ord_ven.anno_registrazione = :ll_anno_registrazione and  
//				tes_ord_ven.num_registrazione = :ll_num_registrazione;
//
//		if sqlca.sqlcode = -1 then
//			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata ordine.", &
//						  exclamation!, ok!)
//			rollback;
//			return
//		end if
//
//		dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "flag_blocco", "N")
//		dw_det_ord_ven_det_1.setitemstatus(dw_det_ord_ven_det_1.getrow(), "flag_blocco", primary!, notmodified!)
//		dw_det_ord_ven_lista.i_parentdw.setitem(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco", "N")
//		dw_det_ord_ven_lista.i_parentdw.setitemstatus(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco", primary!, notmodified!)
//		commit;
//	end if
//end if
end event

type cb_sconti from commandbutton within w_det_ord_ven_tv
integer x = 1586
integer y = 2244
integer width = 366
integer height = 80
integer taborder = 160
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Sconti"
end type

event clicked;s_cs_xx.parametri.parametro_uo_dw_1 = dw_det_ord_ven_lista
window_open(w_sconti, 0)

end event

type cb_configuratore from commandbutton within w_det_ord_ven_tv
integer x = 425
integer y = 2244
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Configurat."
end type

event clicked;string ls_cod_pagamento
long ll_i, ll_anno_commessa, ll_num_commessa, ll_cont, ll_num_riga_appartenenza, ll_prog_riga_ord_ven, ll_ret
double ld_sconto

ll_anno_commessa = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"anno_commessa")
ll_num_commessa = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"num_commessa")
if not isnull(ll_anno_commessa) or ll_anno_commessa > 0 then
	g_mb.messagebox("APICE","Impossibile entrare nel configuratore: è già stata creata una commessa")
	return
end if


// EnMe 23-07-2010 per Beatrice; anche se qualche riga riferita ha creato la commessa, impedisco l'accesso
// finchè la commessa non sia stata eliminata; altrimenti restano delle commesse tronche in giro.
ll_prog_riga_ord_ven = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"prog_riga_ord_ven")

for ll_i = 1 to dw_det_ord_ven_lista.rowcount( )
	
	ll_num_riga_appartenenza = dw_det_ord_ven_lista.getitemnumber(ll_i,"num_riga_appartenenza")
	
	if ll_num_riga_appartenenza = ll_prog_riga_ord_ven then
	
		ll_anno_commessa = dw_det_ord_ven_lista.getitemnumber(ll_i,"anno_commessa")
		if not isnull(ll_anno_commessa) or ll_anno_commessa > 0 then
			g_mb.messagebox("APICE","Impossibile entrare nel configuratore: qualche riga RIFERITA ha generato una commessa")
			return
		end if
	
	end if
	
next
// fine modifica EnMe 23-07-2010

str_conf_prodotto.anno_documento = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"anno_registrazione")
str_conf_prodotto.num_documento = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"num_registrazione")
str_conf_prodotto.prog_riga_documento = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"prog_riga_ord_ven")

select count(*)
into   :ll_cont
from   comp_det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :str_conf_prodotto.anno_documento and
		 num_registrazione = :str_conf_prodotto.num_documento and
		 prog_riga_ord_ven = :str_conf_prodotto.prog_riga_documento;
if isnull(ll_cont) or ll_cont = 0 then
	g_mb.messagebox("APICE","Impossibile entrare nel configuratore: la riga non era stata generata dal configuratore")
	return
end if

setnull(str_conf_prodotto.cod_contatto)
str_conf_prodotto.cod_cliente = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.getrow(),"cod_cliente")
str_conf_prodotto.cod_agente_1 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.getrow(),"cod_agente_1")
str_conf_prodotto.cod_tipo_listino_prodotto = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.getrow(),"cod_tipo_listino_prodotto")
str_conf_prodotto.cod_valuta = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.getrow(),"cod_valuta")
str_conf_prodotto.data_documento = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.getrow(),"data_registrazione")
str_conf_prodotto.data_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.getrow(),"data_registrazione")
str_conf_prodotto.sconto_testata = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.getrow(),"sconto")
str_conf_prodotto.tipo_gestione = "ORD_VEN"
str_conf_prodotto.cod_modello = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(),"cod_prodotto")
str_conf_prodotto.cod_versione = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(),"cod_versione")
str_conf_prodotto.data_consegna = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.getrow(),"data_consegna")
str_conf_prodotto.cambio_ven = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.getrow(),"cambio_ven")
ls_cod_pagamento = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.getrow(),"cod_pagamento")

if isnull(ls_cod_pagamento) then
	str_conf_prodotto.sconto_pagamento = 0
else
	select sconto
	into   :ld_sconto
	from   tab_pagamenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_pagamento = :ls_cod_pagamento;
	if sqlca.sqlcode = 0 and not isnull(ld_sconto) then
		str_conf_prodotto.sconto_pagamento = ld_sconto
	else
		str_conf_prodotto.sconto_pagamento = 0
	end if
end if
for ll_i = 1 to 10 
	str_conf_prodotto.sconti[ll_i] = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"sconto_" + string(ll_i))
	if isnull(str_conf_prodotto.sconti[ll_i]) then str_conf_prodotto.sconti[ll_i] = 0
next

str_conf_prodotto.provvigione_1 = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"provvigione_1")
if isnull(str_conf_prodotto.provvigione_1) then str_conf_prodotto.provvigione_1 = 0
str_conf_prodotto.provvigione_2 = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"provvigione_2")
if isnull(str_conf_prodotto.provvigione_2) then str_conf_prodotto.provvigione_2 = 0

str_conf_prodotto.nota = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(),"nota_dettaglio")
setnull(str_conf_prodotto.cod_prodotto_finito)
str_conf_prodotto.stato = "M"
str_conf_prodotto.flag_cambio_modello = false	

// Enme 4-6-2010
str_conf_prodotto.des_prodotto = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(),"des_prodotto")

//window_open(w_configuratore, 0)
openwithparm(w_configuratore, parent)
end event

type cb_c_industriale from commandbutton within w_det_ord_ven_tv
integer x = 818
integer y = 2244
integer width = 366
integer height = 80
integer taborder = 130
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Elimina"
end type

event clicked;string					ls_errore
long					ll_riga_corrente, ll_ret, ll_numero, ll_riga
integer				li_anno
uo_produzione		luo_produzione


ll_riga_corrente = dw_det_ord_ven_lista.getrow()

if ll_riga_corrente>0 then
else
	g_mb.warning("Nessuna riga selezionata!")
	return
end if

li_anno = dw_det_ord_ven_lista.getitemnumber(ll_riga_corrente, "anno_registrazione")
ll_numero = dw_det_ord_ven_lista.getitemnumber(ll_riga_corrente, "num_registrazione")
ll_riga = dw_det_ord_ven_lista.getitemnumber(ll_riga_corrente, "prog_riga_ord_ven")

if li_anno>0 and ll_numero>0 and ll_riga>0 then
	if not g_mb.confirm("Eliminare la riga ordine n° "+string(li_anno)+"/"+string(ll_numero)+"/"+string(ll_riga)) then
		return
	end if
else
	g_mb.warning("Nessuna riga ordine selezionata!")
	return
end if

//------------------------------------------------------------------------------------------------------
//pulizia tabella evasione da dati eventuali per evitare errore di FK
delete from evas_ord_ven
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :li_anno and
			num_registrazione  = :ll_numero and
			prog_riga_ord_ven  =: ll_riga;
if sqlca.sqlcode < 0 then
	ls_errore = "Errore durante pulizia tabella evas_ord_ven dalla riga ordine: "+sqlca.sqlerrtext
	rollback;
	g_mb.error(ls_errore)
	return
end if
//------------------------------------------------------------------------------------------------------

luo_produzione = create uo_produzione
ll_ret = luo_produzione.uof_elimina_riga_ordine( 	li_anno, ll_numero, ll_riga, ls_errore)
destroy luo_produzione


if ll_ret = 0 then
	commit;
	if len(ls_errore) > 0 then
		g_mb.warning(ls_errore)
	end if
else
	rollback;
	g_mb.error(ls_errore)
	return
end if

g_mb.success("Operazione effettuata!")


ll_riga_corrente = dw_det_ord_ven_lista.getrow()
dw_det_ord_ven_lista.change_dw_current()
dw_det_ord_ven_lista.triggerevent("pcd_retrieve")

if dw_det_ord_ven_lista.rowcount() > 0 then
	dw_det_ord_ven_lista.change_dw_current()
	if ll_riga_corrente > dw_det_ord_ven_lista.rowcount() then ll_riga_corrente = dw_det_ord_ven_lista.rowcount()
	dw_det_ord_ven_lista.setrow(ll_riga_corrente)
end if

end event

type cb_ricalcola_configurazione from commandbutton within w_det_ord_ven_tv
integer x = 3026
integer y = 2100
integer width = 366
integer height = 80
integer taborder = 31
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ricalcola"
end type

event clicked;boolean lb_apri_configuratore
string ls_flag_tessuto, ls_flag_mantovana, ls_flag_altezza_mantovana,  ls_flag_tessuto_mantovana, ls_flag_cordolo, ls_flag_passamaneria, &
       ls_flag_verniciatura, ls_flag_comando, ls_flag_pos_comando, ls_flag_alt_asta, ls_flag_supporto, ls_flag_luce_finita,   &
		 ld_limite_max_dim_x, ld_limite_max_dim_y, ld_limite_max_dim_z, ld_limite_max_dim_t, ld_limite_min_dim_x, ld_limite_min_dim_y,   &
		 ld_limite_min_dim_z, ld_limite_min_dim_t, ls_flag_secondo_comando, ls_flag_tessuto_1, ls_flag_mantovana_1, ls_flag_altezza_mantovana_1, &
		 ls_flag_tessuto_mantovana_1, ls_flag_cordolo_1, ls_flag_passamaneria_1, ls_flag_verniciatura_1, ls_flag_comando_1, ls_flag_pos_comando_1,&
		 ls_flag_alt_asta_1, ls_flag_supporto_1, ls_flag_luce_finita_1, ld_limite_max_dim_x_1, ld_limite_max_dim_y_1, ld_limite_max_dim_z_1, &
		 ld_limite_max_dim_t_1, ld_limite_min_dim_x_1, ld_limite_min_dim_y_1, ld_limite_min_dim_z_1, ld_limite_min_dim_t_1, &
		 ls_flag_secondo_comando_1, ls_cod_modello, ls_cod_prodotto_origine, ld_des_dim_x,ld_des_dim_y,ld_des_dim_z,ld_des_dim_t, &
		 ls_messaggio_modifica, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_cod_prodotto_finito, ls_messaggio,ls_cod_tipo_ord_ven, &
		 ls_des_prodotto_mag, ls_mp_escluse[], ls_testo_esclusioni, ls_cod_prodotto_storico,ls_cod_prodotto_raggruppato, ls_null

integer li_anno_commessa,li_risposta

long   ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven,ll_num_commessa, ll_null, ll_righe_escluse[], ll_ret

dec{4} ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t, ld_quan_ordine, ld_quan_raggruppo

datetime ldt_adesso

uo_funzioni_1 luo_funzioni_1

uo_storicizzazione luo_storicizzazione

uo_gestione_conversioni luo_gestione_conversioni

uo_calcola_documento_euro luo_calcola_documento_euro

setnull(ls_null)
ll_anno_registrazione = dw_comp_det_ord_ven.getitemnumber(dw_comp_det_ord_ven.getrow(),"anno_registrazione")
ll_num_registrazione = dw_comp_det_ord_ven.getitemnumber(dw_comp_det_ord_ven.getrow(),"num_registrazione")

st_avanzamento.text = "ELABORAZIONE DI STORICIZZAZIONE...ATTENDERE !!!"

// ------------------------------- per sicurezza procedo con il ri-calcolo del documento ---------------------------

luo_calcola_documento_euro = create uo_calcola_documento_euro

if luo_calcola_documento_euro.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"ord_ven",ls_messaggio) <> 0 then
	g_mb.messagebox("APICE",ls_messaggio)
	rollback;
else
	commit;
end if

destroy luo_calcola_documento_euro

// ------------------------------------- fine calcolo documento --------------------------------------------------
ll_prog_riga_ord_ven = dw_comp_det_ord_ven.getitemnumber(dw_comp_det_ord_ven.getrow(),"prog_riga_ord_ven")
ld_dim_x = dw_comp_det_ord_ven.getitemnumber(dw_comp_det_ord_ven.getrow(),"dim_x")
ld_dim_y = dw_comp_det_ord_ven.getitemnumber(dw_comp_det_ord_ven.getrow(),"dim_y")
ld_dim_z = dw_comp_det_ord_ven.getitemnumber(dw_comp_det_ord_ven.getrow(),"dim_z")
ld_dim_t = dw_comp_det_ord_ven.getitemnumber(dw_comp_det_ord_ven.getrow(),"dim_t")

ls_cod_modello = dw_comp_det_ord_ven.getitemstring(dw_comp_det_ord_ven.getrow(),"cod_modello")
ls_cod_prodotto_origine = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(),"cod_prodotto")
ls_cod_tipo_det_ven = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(),"cod_tipo_det_ven")
ld_quan_ordine = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"quan_ordine")
if ls_cod_modello = ls_cod_prodotto_origine then
	g_mb.messagebox("Configuratore","Ricalcolo INUTILE perchè non è stato cambiato il modello di prodotto",Stopsign!)
	return
end if

// visualizzo finestra con richiesta del valore da storicizzare
s_cs_xx.parametri.parametro_d_1 = -1

open(w_det_ord_ven_val_storico)

if s_cs_xx.parametri.parametro_d_1 < 0 then 
	return
end if

string ls_materia_prima[], ls_errore, ls_cod_versione, ls_versione_mat_prima[]
double ld_quantita_utilizzo[]

// trovo il codice prodotto di cui eseguire la storicizzazione
ls_cod_versione = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_versione")

choose case right(ls_cod_modello,1)
	case "Y"
		ls_cod_prodotto_storico = ls_cod_prodotto_origine + "X"
		
	case "X"
		ls_cod_prodotto_storico = ls_cod_prodotto_origine + "Y"
		
	case else
		g_mb.messagebox("APICE","Il prodotto su cui eseguire AR deve avere la lettera finale X oppure Y, altrimenti non si riesce a storicizzare correttamente!")
		rollback;
		return
		
end choose

// trovo le materie prime

luo_funzioni_1 = CREATE uo_funzioni_1

ll_ret = luo_funzioni_1.uof_trova_mat_prime_varianti  (	ls_cod_prodotto_storico, &
                           													 ls_cod_versione, &
																			ls_null, &
																			 ref ls_materia_prima[], &
																			 ref ls_versione_mat_prima[], &
																			 ref ld_quantita_utilizzo[], &
																			 ld_quan_ordine, &
																			 dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione"), &
																			 dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione"), &
																			 dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven"), &
																			 "varianti_det_ord_ven", &
																			ls_null,&
									 										ref ls_errore   )
																			
if ll_ret < 0 then
	destroy luo_funzioni_1
	rollback;
	g_mb.messagebox("Configuratore", "Storicizzazione AR~r~n" + "Errore in estrazione materie prime~r~n" + sqlca.sqlerrtext)
	return
end if

destroy luo_funzioni_1

luo_storicizzazione = create uo_storicizzazione

// imposto alcuni parametri di istanza per passare i dati necessari
luo_storicizzazione.is_flag_ar = "S"
luo_storicizzazione.is_cod_prodotto_origine = ls_cod_prodotto_origine
luo_storicizzazione.is_cod_prodotto_ar      = ls_cod_prodotto_storico
luo_storicizzazione.is_cod_materia_prima[] = ls_materia_prima[]
luo_storicizzazione.id_quan_utilizzo[] = ld_quantita_utilizzo[]
luo_storicizzazione.id_valore_storico = s_cs_xx.parametri.parametro_d_1

ll_ret = luo_storicizzazione.uof_storicizza(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, true, ls_mp_escluse[], ll_righe_escluse[], ls_testo_esclusioni, ls_messaggio )

if ll_ret = 1 then
	g_mb.messagebox("Configuratore", "Storicizzazione AR~r~n" + ls_messaggio + "~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

st_avanzamento.text = "ELABORAZIONE DI RICOLCOLO IN CORSO....ATTENDERE !!!"
SELECT flag_tessuto,   
		flag_tipo_mantovana,   
		flag_altezza_mantovana,   
		flag_tessuto_mantovana,   
		flag_cordolo,   
		flag_passamaneria,   
		flag_verniciatura,   
		flag_comando,   
		flag_pos_comando,   
		flag_alt_asta,   
		flag_supporto,   
		flag_luce_finita,   
		limite_max_dim_x,   
		limite_max_dim_y,   
		limite_max_dim_z,   
		limite_max_dim_t,   
		limite_min_dim_x,   
		limite_min_dim_y,   
		limite_min_dim_z,   
		limite_min_dim_t,   
		flag_secondo_comando  
 INTO :ls_flag_tessuto,   
		:ls_flag_mantovana,   
		:ls_flag_altezza_mantovana,   
		:ls_flag_tessuto_mantovana,   
		:ls_flag_cordolo,   
		:ls_flag_passamaneria,   
		:ls_flag_verniciatura,   
		:ls_flag_comando,   
		:ls_flag_pos_comando,   
		:ls_flag_alt_asta,   
		:ls_flag_supporto,   
		:ls_flag_luce_finita,   
		:ld_limite_max_dim_x,   
		:ld_limite_max_dim_y,   
		:ld_limite_max_dim_z,   
		:ld_limite_max_dim_t,   
		:ld_limite_min_dim_x,   
		:ld_limite_min_dim_y,   
		:ld_limite_min_dim_z,   
		:ld_limite_min_dim_t,   
		:ls_flag_secondo_comando  
 FROM tab_flags_configuratore
 WHERE cod_azienda = :s_cs_xx.cod_azienda and
       cod_modello = :ls_cod_modello;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Configuratore", "Errore in ricerca parametri configuratore.~r~nDettaglio: " + sqlca.sqlerrtext)
	rollback;
	return
end if

// *********************  pulisco i passi che adesso non sono più validi ****************************************

if ls_flag_luce_finita = "N" or isnull(ls_flag_luce_finita) then
	update comp_det_ord_ven
	set    flag_misura_luce_finita = 'L'
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
	       prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

if ls_flag_tessuto = "N" or isnull(ls_flag_tessuto) then
	update comp_det_ord_ven
	set    cod_tessuto = null, cod_non_a_magazzino = null, flag_fc_tessuto = 'N', flag_addizionale_tessuto = 'N'
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
	       prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

if ls_flag_tessuto_mantovana = "N" or isnull(ls_flag_tessuto_mantovana) then
	update comp_det_ord_ven
	set    cod_tessuto_mant = null, cod_mant_non_a_magazzino = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
	       prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

if ls_flag_altezza_mantovana = "N" or isnull(ls_flag_altezza_mantovana) then
	update comp_det_ord_ven
	set    alt_mantovana = 0
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
	       prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

if ls_flag_mantovana = "N" or isnull(ls_flag_mantovana) then
	update comp_det_ord_ven
	set    flag_tipo_mantovana = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
	       prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

if ls_flag_cordolo = "N" or isnull(ls_flag_cordolo) then
	update comp_det_ord_ven
	set    flag_cordolo = 'N', colore_cordolo = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
	       prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

if ls_flag_passamaneria = "N" or isnull(ls_flag_passamaneria) then
	update comp_det_ord_ven
	set    flag_passamaneria = 'N', colore_passamaneria = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
	       prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

if ls_flag_verniciatura = "N" or isnull(ls_flag_verniciatura) then
	update comp_det_ord_ven
	set    cod_verniciatura = null, flag_fc_vernic = 'N', des_vernice_fc = null, flag_addizionale_vernic = 'N'
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
	       prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

if ls_flag_comando = "N" or isnull(ls_flag_comando) then
	update comp_det_ord_ven
	set    cod_comando = null, flag_addizionale_comando_1 ='N', flag_fc_comando_1 = 'N', des_comando_1 = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
	       prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

if ls_flag_pos_comando = "N" or isnull(ls_flag_pos_comando) then
	update comp_det_ord_ven
	set    pos_comando = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
	       prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

if ls_flag_alt_asta = "N" or isnull(ls_flag_alt_asta) then
	update comp_det_ord_ven
	set    alt_asta = 0
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
	       prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

if ls_flag_secondo_comando = "N" or isnull(ls_flag_secondo_comando) then
	update comp_det_ord_ven
	set    cod_comando_2 = null, flag_addizionale_comando_2 ='N', flag_fc_comando_2 = 'N', des_comando_2 = null
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
	       prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

if ls_flag_supporto = "N" or isnull(ls_flag_supporto) then
	update comp_det_ord_ven
	set    cod_tipo_supporto = null, flag_addizionale_supporto = 'N', flag_fc_supporto ='N'
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
	       prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

// *********************  controllo se devo aprire il configuratore per chiedere dati aggiuntivi ********************************

SELECT flag_tessuto,   
		flag_tipo_mantovana,   
		flag_altezza_mantovana,   
		flag_tessuto_mantovana,   
		flag_cordolo,   
		flag_passamaneria,   
		flag_verniciatura,   
		flag_comando,   
		flag_pos_comando,   
		flag_alt_asta,   
		flag_supporto,   
		flag_luce_finita,   
		limite_max_dim_x,   
		limite_max_dim_y,   
		limite_max_dim_z,   
		limite_max_dim_t,   
		limite_min_dim_x,   
		limite_min_dim_y,   
		limite_min_dim_z,   
		limite_min_dim_t,   
		flag_secondo_comando  
 INTO :ls_flag_tessuto_1,   
		:ls_flag_mantovana_1,   
		:ls_flag_altezza_mantovana_1,   
		:ls_flag_tessuto_mantovana_1,   
		:ls_flag_cordolo_1,   
		:ls_flag_passamaneria_1,   
		:ls_flag_verniciatura_1,   
		:ls_flag_comando_1,   
		:ls_flag_pos_comando_1,   
		:ls_flag_alt_asta_1,   
		:ls_flag_supporto_1,   
		:ls_flag_luce_finita_1,   
		:ld_limite_max_dim_x_1,   
		:ld_limite_max_dim_y_1,   
		:ld_limite_max_dim_z_1,   
		:ld_limite_max_dim_t_1,   
		:ld_limite_min_dim_x_1,   
		:ld_limite_min_dim_y_1,   
		:ld_limite_min_dim_z_1,   
		:ld_limite_min_dim_t_1,   
		:ls_flag_secondo_comando_1
 FROM tab_flags_configuratore
 WHERE cod_azienda = :s_cs_xx.cod_azienda and
       cod_modello = :ls_cod_prodotto_origine;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Configuratore", "Errore in ricerca parametri configuratore del prodotto di origine.~r~nDettaglio: " + sqlca.sqlerrtext)
	rollback;
	return
end if

lb_apri_configuratore = false
ls_messaggio_modifica = "Aggiungere i seguenti dati nel configuratore:"
if ls_flag_luce_finita_1 = "N" and ls_flag_luce_finita ="S" then 
	lb_apri_configuratore = true
	ls_messaggio_modifica = ls_messaggio_modifica + "~r~nMISURA LUCE"
end if
if ls_flag_tessuto_1 = "N" and ls_flag_tessuto ="S" then 
	lb_apri_configuratore = true
	ls_messaggio_modifica = ls_messaggio_modifica + "~r~nCODICE TESSUTO"
end if
if ls_flag_mantovana_1 = "N" and ls_flag_mantovana ="S" then
	lb_apri_configuratore = true
	ls_messaggio_modifica = ls_messaggio_modifica + "~r~nTIPO MANTOVANA"
end if
if ls_flag_cordolo_1 = "N" and ls_flag_cordolo ="S" then 
	lb_apri_configuratore = true
	ls_messaggio_modifica = ls_messaggio_modifica + "~r~nCOLORE CORDOLO"
end if
if ls_flag_passamaneria_1 = "N" and ls_flag_passamaneria ="S" then 
	lb_apri_configuratore = true
	ls_messaggio_modifica = ls_messaggio_modifica + "~r~nCOLORE PASSAMANERIA"
end if
if ls_flag_altezza_mantovana_1 = "N" and ls_flag_altezza_mantovana ="S" then 
	lb_apri_configuratore = true
	ls_messaggio_modifica = ls_messaggio_modifica + "~r~nALTEZZA MANTOVANA"
end if
if ls_flag_tessuto_mantovana_1 = "N" and ls_flag_tessuto_mantovana ="S" then 
	lb_apri_configuratore = true
	ls_messaggio_modifica = ls_messaggio_modifica + "~r~nCODICE TESSUTO MANTOVANA"
end if
if ls_flag_verniciatura_1 = "N" and ls_flag_verniciatura ="S" then 
	lb_apri_configuratore = true
	ls_messaggio_modifica = ls_messaggio_modifica + "~r~nCODICE VERNICIATURA"
end if
if ls_flag_comando_1 = "N" and ls_flag_comando ="S" then 
	lb_apri_configuratore = true
	ls_messaggio_modifica = ls_messaggio_modifica + "~r~nCODICE PRIMO COMANDO"
end if
if ls_flag_alt_asta_1 = "N" and ls_flag_alt_asta ="S" then 
	lb_apri_configuratore = true
	ls_messaggio_modifica = ls_messaggio_modifica + "~r~nCODICE SECONDO"
end if
if ls_flag_supporto_1 = "N" and ls_flag_supporto ="S" then 
	lb_apri_configuratore = true
	ls_messaggio_modifica = ls_messaggio_modifica + "~r~nCODICE SUPPORTO"
end if

// ***********************************************  ESEGUO UN COMMIT PER LIBERARE EVENTUALI LOCK **************************

COMMIT;

destroy luo_storicizzazione

// ========================================================================================================================

if lb_apri_configuratore then
	str_conf_prodotto.flag_cambio_modello = true
	str_conf_prodotto.messaggio_modifiche_modello = ls_messaggio_modifica
	st_avanzamento.text = "ELABORAZIONE DI RICOLCOLO TERMINATA. APERTURA DEL CONFIGURATORE IN CORSO"
	cb_configuratore.triggerevent("clicked")
else
	st_avanzamento.text = "ELABORAZIONE DI RICOLCOLO TERMINATA. AGGIORNAMENTO DEL MAGAZZINO IN CORSO !"
	str_conf_prodotto.flag_cambio_modello = false
	setnull(str_conf_prodotto.messaggio_modifiche_modello)
	
	select des_prodotto
	into   :ls_des_prodotto_mag
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_modello;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante ricerca della descrizione del prodotto "+ls_cod_modello+".~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	update det_ord_ven
	set cod_prodotto = :ls_cod_modello,
	    des_prodotto = :ls_des_prodotto_mag
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
			 prog_riga_ord_ven= :ll_prog_riga_ord_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante cambio prodotto nella riga di dettaglio.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	// <<------------------- sistemo la quantità impegnata ---------------- >>
	// ovviamente sistemo la quantità impegnata solo se non entro nel configuratore; quando entro nel configuratore ci pensa
	// lui a sistemare la quantità impegnata
	select flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	if sqlca.sqlcode = 0 then
		if ls_flag_tipo_det_ven = "M" then
			update anag_prodotti  
				set quan_impegnata = quan_impegnata - :ld_quan_ordine
			 where cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto_origine;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento impegnato magazzino.~r~n" + sqlca.sqlerrtext, exclamation!, ok!)
				return
			end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto_origine;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
				
				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto_origine,ld_quan_ordine,"M")
				
				update anag_prodotti  
					set quan_impegnata = quan_impegnata - :ld_quan_raggruppo
				 where cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_prodotto = :ls_cod_prodotto_raggruppato;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento impegnato magazzino.~r~n" + sqlca.sqlerrtext, exclamation!, ok!)
					return
				end if
			end if
			
			
			update anag_prodotti  
				set quan_impegnata = quan_impegnata + :ld_quan_ordine
			 where cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_modello;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento impegnato magazzino.~r~n" + sqlca.sqlerrtext, exclamation!, ok!)
				return
			end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_modello;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then

				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_modello, ld_quan_ordine, "M")

				update anag_prodotti  
					set quan_impegnata = quan_impegnata + :ld_quan_raggruppo
				 where cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_prodotto = :ls_cod_prodotto_raggruppato;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento impegnato magazzino.~r~n" + sqlca.sqlerrtext, exclamation!, ok!)
					return
				end if
			end if
			
			
		end if
	end if
	// << ----------------------  ricalcolo del codice autocomposto  ------------------>>
	
	st_avanzamento.text = "ELABORAZIONE DI RICOLCOLO TERMINATA. RICALCOLO CODICE AUTOCOMPOSTO"
	if wf_cod_prodotto_finito(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_prodotto_finito, ls_messaggio) = -1 then
		g_mb.messagebox("Configuratore","RICALCOLO CODICE PRODOTTO FINITO: " + ls_messaggio)
		rollback;
		return
	end if
	
	update comp_det_ord_ven
	set    cod_prod_finito = :ls_cod_prodotto_finito
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione = :ll_num_registrazione and
	       prog_riga_ord_ven = :ll_prog_riga_ord_ven;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	
	/* << ----------------------  ricalcolo del prezzo del prodotto finito ------------>>
	
	Eliminato questo pezzo di codice a seguito della specifica "storico ordini"; ora il prezzo
	del prodotto finito viene scritto dall'operatore al momento del lancio della AR
	
	st_avanzamento.text = "ELABORAZIONE DI RICOLCOLO TERMINATA. RICALCOLO PREZZO VENDITA"
	if wf_ricerca_condizioni(ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,ls_messaggio) = -1 then
		messagebox("Configuratore","Errore durante calcolo prezzo.~r~n"+ls_messaggio)
		rollback;
		return
	end if
	*/
	
	// <<-------------  fine sistemazione quantità impegnata ------>>
	
	
	// << ------------   GENERAZIONE E CHIUSURA DELLA COMMESSA --------------------------->>
	st_avanzamento.text = "ELABORAZIONE DI RICOLCOLO TERMINATA. GENERAZIONE COMMESSA IN CORSO"
	luo_funzioni_1 = CREATE uo_funzioni_1
	
	select cod_tipo_ord_ven
	into   :ls_cod_tipo_ord_ven
	from   tes_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione  = :ll_num_registrazione;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Creazione commessa","Errore in ricerca ordine: creazione commessa interrotta",stopsign!)
		rollback using sqlca;
		return
	end if
	
	setnull(ll_null)	
	setnull(ls_null)
	
	if luo_funzioni_1.uof_genera_commesse(ls_cod_tipo_ord_ven, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_null, li_anno_commessa, ll_num_commessa, ls_messaggio) = 0 then
		st_avanzamento.text = "ELABORAZIONE DI RICOLCOLO TERMINATA. CHIUSURA COMMESSA IN CORSO"
		if luo_funzioni_1.uof_avanza_commessa(li_anno_commessa, ll_num_commessa, ls_null,  ls_messaggio) = -1 then
			g_mb.messagebox("Configuratore","Errore in chiusura della commessa.~r~n" + ls_messaggio)
			rollback using sqlca;
			destroy luo_funzioni_1
			return 
		end if
	else
		g_mb.messagebox("Configuratore","Errore in generazione commessa.~r~n" + ls_messaggio)
		rollback using sqlca;
		destroy luo_funzioni_1
		return
	end if
	destroy luo_funzioni_1
	st_avanzamento.text = "RICOLCOLO TERMINATO. GENERATO E CHIUSO commessa " + string(li_anno_commessa) + "-" + string(ll_num_commessa) + " CON SUCCESSO !!"
end if	

// ************ procedura per la cancellazione/allineamento produzione A/R

// imposta la data a/r in comp_det_ord_ven

ldt_adesso = datetime(today(),now())

update comp_det_ord_ven
set    data_ora_ar =:ldt_adesso
where  cod_azienda = :s_cs_xx.cod_azienda and
	    anno_registrazione = :ll_anno_registrazione and
	    num_registrazione = :ll_num_registrazione and
	    prog_riga_ord_ven = :ll_prog_riga_ord_ven;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Configuratore","Errore durante aggiornamento dettaglio complementare ordine.~r~n"+sqlca.sqlerrtext)
	rollback;
	return
end if

//  funzione (in iuo_produzione) che elimina dalla produzione tutte le righe di 
// det_ordini_in_produzione e le sue sessioni che appartengono a reparti diversi da quelli del nuovo prodotto 
// appena scelto come A/R.

if not isvalid(iuo_produzione) then
	iuo_produzione = create uo_produzione
end if

li_risposta = iuo_produzione.uof_cancella_ar(integer(ll_anno_registrazione),& 
															ll_num_registrazione,& 
															ll_prog_riga_ord_ven,& 
															ls_messaggio)


if li_risposta < 0 then
	g_mb.messagebox("Configuratore", ls_messaggio,stopsign!)
	rollback;
	return 
end if

// ************ fine procedura per la cancellazione/allineamento produzione A/R

commit;


end event

type st_avanzamento from statictext within w_det_ord_ven_tv
integer x = 69
integer y = 1968
integer width = 2551
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type cb_cancella_commessa from commandbutton within w_det_ord_ven_tv
integer x = 3415
integer y = 2100
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Canc.Comm."
end type

event clicked;boolean lb_cancella_produzione=false, lb_cancella_commesse_riferite=false

string ls_messaggio,ls_flag_tipo_avanzamento, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_cod_prodotto, &
		ls_cod_prodotto_raggruppato, ls_mess_comm_eliminate
		
long 	ll_anno_commessa, ll_num_commessa, ll_anno_ord, ll_num_ord, ll_riga_ord,ll_righe, ll_i, &
	  	ll_num_riga_appartenenza, ll_prog_riga_ord_ven, ll_anno_comm_riferita, ll_num_comm_riferita, &
		ll_prog_riga_ord_rif, ll_ret

dec{4} ld_quan_ordine, ld_quan_raggruppo

uo_produzione luo_prod
uo_funzioni_1 luo_funzioni_commesse


ll_anno_commessa = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_commessa")
ll_num_commessa = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_commessa")

ll_anno_ord = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")

ll_num_ord = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"num_registrazione")

ll_riga_ord = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"prog_riga_ord_ven")
ll_prog_riga_ord_ven = ll_riga_ord

if g_mb.messagebox("Sep","SEI SICURO DI VOLER CANCELLARE LA COMMESSA NR." + string(ll_anno_commessa) + "/" + string(ll_num_commessa),Question!,YesNo!,1) = 2 then return

if g_mb.messagebox("APICE","Cancellare anche le commesse delle righe RIFERITE ?",question!,yesno!,2) = 1 then
	lb_cancella_commesse_riferite = true
else
	lb_cancella_commesse_riferite = false
end if

if g_mb.messagebox("APICE","Cancellare anche gli eventuali dati di avanzamento produzione?",question!,yesno!,2) = 1 then
	lb_cancella_produzione = true
else
	lb_cancella_produzione = false
end if

setpointer(hourglass!)

luo_prod = create uo_produzione
ll_ret = luo_prod.uof_elimina_commessa(ll_anno_ord, ll_num_ord, ll_prog_riga_ord_ven, lb_cancella_commesse_riferite, lb_cancella_produzione, ref ls_messaggio)
if ll_ret < 0 then
	rollback;
	g_mb.error( ls_messaggio )
	setpointer(arrow!)
	destroy luo_prod
	return
end if
// Se va tutto bene allora mi passo il messaggio che elenca le commesse eliminate.
ls_mess_comm_eliminate = ls_messaggio


/* da cancellare

// 8/2/2007 aggiunto su richiesta di Beatrice: impossibile eliminare una riga se commessa chiusa.
if not isnull(ll_anno_commessa) then
	select flag_tipo_avanzamento
	into   :ls_flag_tipo_avanzamento
	from   anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_commessa = :ll_anno_commessa and
			 num_commessa = :ll_num_commessa;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in cancellazione commessa " + string(ll_anno_commessa) + "/" + string(ll_num_commessa) + "~r~n"+sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	if ls_flag_tipo_avanzamento = "7" then
		g_mb.messagebox("APICE","Commessa già chiusa: impossibile eliminare.")
		rollback;
		return
	end if
		
end if

setpointer(hourglass!)

luo_funzioni_commesse = CREATE uo_funzioni_1

//log in log_sistema della richiesta di avanzamento ############################################################################
f_log_sistema("***Richiesta CANC.com: uof_avanza_commessa("+string(ll_anno_commessa) +"/"+string(ll_num_commessa)+"'NULL')","AVANZCOMdet_ord")
//############################################################################################################

if luo_funzioni_commesse.uof_cancella_commessa(ll_anno_commessa,ll_num_commessa, ls_messaggio) = -1 then
	g_mb.messagebox("Cancellazione Commessa", ls_messaggio)
	destroy luo_funzioni_commesse
	rollback;
	setpointer(arrow!)
	return
end if


delete impegno_mat_prime_commessa
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_commessa = :ll_anno_commessa and
		num_commessa = :ll_num_commessa;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Cancellazione Commessa", "Errore durante la cancellazione da impegno_mat_prime_commessa. Dettaglio=" + sqlca.sqlerrtext)
	destroy luo_funzioni_commesse
	rollback;
	setpointer(arrow!)
	return
end if

delete anag_commesse
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_commessa = :ll_anno_commessa and
		num_commessa = :ll_num_commessa;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Cancellazione Commessa", "Errore durante la cancellazione da anag_commesse. Dettaglio=" + sqlca.sqlerrtext)
	destroy luo_funzioni_commesse
	rollback;
	setpointer(arrow!)
	return
end if

// CANCELLAZIONE ANCHE DELLE COMMESSE DELLE RIGHE RIFERITE
//	Richiesto da Beatrice 16/06/2008

if lb_cancella_commesse_riferite then

	ll_righe = dw_det_ord_ven_lista.rowcount()
	
	for ll_i = 1 to ll_righe
		
		ll_num_riga_appartenenza = dw_det_ord_ven_lista.getitemnumber(ll_i, "num_riga_appartenenza")
		
		if (ll_num_riga_appartenenza = ll_prog_riga_ord_ven and not isnull(ll_num_riga_appartenenza) and ll_num_riga_appartenenza <> 0) or ll_i = ll_prog_riga_ord_ven then
		
			// cancello la commessa delle righe riferite  [EnMe 22/4/2003]
			
			ll_anno_comm_riferita = dw_det_ord_ven_lista.getitemnumber(ll_i, "anno_commessa")
			ll_num_comm_riferita = dw_det_ord_ven_lista.getitemnumber(ll_i, "num_commessa")
			ll_prog_riga_ord_rif = dw_det_ord_ven_lista.getitemnumber(ll_i, "prog_riga_ord_ven")
			
			if not isnull(ll_anno_comm_riferita) then
				
				if luo_funzioni_commesse.uof_cancella_commessa(ll_anno_comm_riferita,ll_num_comm_riferita, ls_messaggio) = -1 then
					g_mb.messagebox("Cancellazione Commessa", ls_messaggio)
					destroy luo_funzioni_commesse
					rollback;
					setpointer(arrow!)
					return
				end if
				
				delete from anag_commesse
				where  cod_azienda   = :s_cs_xx.cod_azienda and
						 anno_commessa = :ll_anno_comm_riferita and
						 num_commessa  = :ll_num_comm_riferita;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Apice","Errore durante la cancellazione della commessa: " + sqlca.sqlerrtext,stopsign!)
					rollback;
					return
				else
					ls_mess_comm_eliminate += "~r~nCOMMESSA (di riga riferita) N° " + string(ll_anno_comm_riferita) + "/" + string(ll_num_comm_riferita)
				end if
				
				if lb_cancella_produzione then
					
					luo_prod = create uo_produzione
					
					if luo_prod.uof_elimina_prod_det(ll_anno_ord,ll_num_ord,ll_prog_riga_ord_rif,ls_messaggio) <> 0 then
						g_mb.messagebox("APICE",ls_messaggio,stopsign!)
						rollback;
						setpointer(arrow!)
						destroy luo_prod
						return -1
					end if
					
					destroy luo_prod
					
				end if
				
			end if
			
		end if
		
	next

end if

// --------------------------------------------------------

luo_prod = create uo_produzione

if lb_cancella_produzione then

	if luo_prod.uof_elimina_prod_det(ll_anno_ord,ll_num_ord,ll_riga_ord,ls_messaggio) <> 0 then
		g_mb.messagebox("APICE",ls_messaggio,stopsign!)
		rollback;
		setpointer(arrow!)
		destroy luo_prod
		return -1
	end if
	
end if

commit;

destroy luo_funzioni_commesse

*/

setpointer(arrow!)

g_mb.messagebox("Sep","COMMESSA NR." + string(ll_anno_commessa) + "/" + string(ll_num_commessa) + " CANCELLATA !!!~r~n" + ls_mess_comm_eliminate)

destroy luo_prod

il_riga_selezionata = dw_det_ord_ven_lista.getrow()
parent.triggerevent("pc_retrieve")
dw_det_ord_ven_lista.postevent("ue_posiziona_riga")
end event

type dw_det_ord_ven_det_1 from uo_cs_xx_dw within w_det_ord_ven_tv
event ue_key pbm_dwnkey
event ue_ricalcola_colonne ( )
event ue_abilita_pulsanti ( )
event ue_prezzo_bloccato ( )
integer x = 46
integer y = 140
integer width = 3438
integer height = 1704
integer taborder = 170
string dataobject = "d_det_ord_ven_det_1"
boolean border = false
end type

event ue_key;call super::ue_key;string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, &
       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_listino, ls_messaggio
long ll_sconti[], ll_maggiorazioni[], ll_i, ll_y
double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo
datetime ldt_data_registrazione


choose case this.getcolumnname()

	case "nota_dettaglio"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_1 = dw_det_ord_ven_det_1.getitemstring(this.getrow(),"cod_prodotto")
			dw_det_ord_ven_det_1.change_dw_current()
			setnull(s_cs_xx.parametri.parametro_s_2)
			window_open(w_prod_note_ricerca, 0)
			if not isnull(s_cs_xx.parametri.parametro_s_2) then
				this.setcolumn("nota_dettaglio")
				this.settext(this.gettext() + "~r~n" + s_cs_xx.parametri.parametro_s_2)
			end if
		end if

	//Giulio: 03/11/2011 modifica ricerca prodotti
	case "des_prodotto", "cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_ord_ven_det_1, "cod_prodotto")
		end if
end choose


end event

event ue_ricalcola_colonne;iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "prezzo_doc", &
																dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quan_ordine"), &
																dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_vendita"), &
																dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quantita_um"), &
																dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_um"), &
																dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"fat_conversione_ven"), &
																dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta"))

end event

event ue_abilita_pulsanti();if ib_stato_nuovo or ib_stato_modifica then
	
	this.object.b_canc_commessa.enabled = 'No'
	this.object.b_ricerca_prodotto.enabled = 'Yes'
	this.object.b_varianti.enabled = 'No'
	
else
	
	this.object.b_canc_commessa.enabled = 'Yes'
	this.object.b_ricerca_prodotto.enabled = 'No'
	this.object.b_varianti.enabled = 'Yes'
	
end if
end event

event ue_prezzo_bloccato();string ls_modify, ls_ret

ls_modify = "prezzo_vendita.Background.Color='0~tIf(flag_prezzo_bloccato <> ~"N~", 50431, "+ string(s_themes.theme[s_themes.current_theme].dw_column_viewmode_backcolor)  +")'" 
ls_ret = Modify( ls_modify )

ls_modify = "sconto_1.Background.Color='0~tIf(flag_prezzo_bloccato <> ~"N~", 50431, "+ string(s_themes.theme[s_themes.current_theme].dw_column_viewmode_backcolor)  +")'" 
ls_ret = Modify( ls_modify )

ls_modify = "sconto_2.Background.Color='0~tIf(flag_prezzo_bloccato <> ~"N~", 50431, "+ string(s_themes.theme[s_themes.current_theme].dw_column_viewmode_backcolor)  +")'" 
ls_ret = Modify( ls_modify )

ls_modify = "provvigione_1.Background.Color='0~tIf(flag_prezzo_bloccato <> ~"N~", 50431, "+ string(s_themes.theme[s_themes.current_theme].dw_column_viewmode_backcolor)  +")'" 
ls_ret = Modify( ls_modify )

ls_modify = "provvigione_2.Background.Color='0~tIf(flag_prezzo_bloccato <> ~"N~", 50431, "+ string(s_themes.theme[s_themes.current_theme].dw_column_viewmode_backcolor)  +")'" 
ls_ret = Modify( ls_modify )

ls_ret = ""
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	uo_default_prodotto luo_default_prodotto
	uo_gruppi_sconti luo_gruppi_sconti
	datawindow ld_datawindow
	commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
	boolean lb_test
	string ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_colonna_sconto, ls_errore, &
			 ls_cod_tipo_listino_prodotto, ls_cod_cliente, ls_cod_valuta, &
			 ls_flag_tipo_det_ven, ls_modify, ls_null, ls_cod_agente_1, ls_des_prodotto, &
			 ls_cod_agente_2, ls_cod_tipo_det_ven, ls_flag_decimali, ls_messaggio, &
			 ls_cod_misura_mag, ls_nota_prodotto, ls_cod_versione, ls_flag_prezzo, ls_flag_blocco, &
			 ls_flag_prezzo_bloccato, ls_nota_testata, ls_stampa_piede, ls_nota_video
	double ld_fat_conversione, ld_quantita, ld_cambio_ven, ld_variazioni[], ld_num_confezioni, ld_num_pezzi, &
			 ld_ultimo_prezzo, ll_sconti[], ll_maggiorazioni[], ld_gruppi_sconti[], ld_provv_1, ld_provv_2
	dec{4} ld_dim_x, ld_dim_y, ld_pezzi_collo
	datetime ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva
	long ll_riga_origine, ll_i, ll_y, ll_riga_appartenenza, ll_anno_registrazione, ll_num_registrazione
	integer li_ret

	setnull(ls_null)
	
	ls_cod_tipo_listino_prodotto = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
	ldt_data_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
	ls_cod_cliente = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
	ls_cod_valuta = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
	ld_cambio_ven = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
	ls_cod_agente_1 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
	ls_cod_agente_2 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
	ls_cod_tipo_det_ven = this.getitemstring(this.getrow(),"cod_tipo_det_ven")
	ls_flag_prezzo_bloccato = this.getitemstring(this.getrow(),"flag_prezzo_bloccato")
	ld_dim_x = 0
	ld_dim_y = 0
	//EnMe 10/12/2012 Valorizzazione prezzi optional configurabili anche con opzioni da distinta
	s_cs_xx.listino_db.flag_calcola ="S"
   
  	choose case i_colname
			
		case "prog_riga_ord_ven"
			ll_riga_origine = this.getrow()
			for ll_i = 1 to this.rowcount()
				if ll_i <> ll_riga_origine and &
					long(i_coltext) = this.getitemnumber(ll_i, "prog_riga_ord_ven") then
					g_mb.messagebox("Attenzione", "Progressivo riga già utilizzato.", &
									exclamation!, ok!)
					i_coltext = string(this.getitemnumber(ll_riga_origine, "prog_riga_ord_ven", primary!,true))
					this.setitem(ll_riga_origine, "prog_riga_ord_ven", double(i_coltext))
					this.settext(i_coltext)
					return 2
					pcca.error = c_fatal
				end if
			next
			
			
		case "cod_tipo_det_ven"
			ls_modify = "cod_prodotto.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_prodotto.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_versione.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_versione.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "des_prodotto.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "des_prodotto.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_misura.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_misura.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "fat_conversione_ven.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "fat_conversione_ven.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "quan_ordine.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "quan_ordine.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "prezzo_vendita.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "prezzo_vendita.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_1.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_1.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_2.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_2.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_iva.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_iva.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_1.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_1.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_2.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_2.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "data_consegna.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "data_consegna.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_centro_costo.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_centro_costo.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
	
			ld_datawindow = dw_det_ord_ven_det_1
			setnull(lc_prodotti_ricerca)
			wf_tipo_det_ven(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext, ls_cod_agente_1, ls_cod_agente_2)
			ld_datawindow = dw_det_ord_ven_lista
			wf_tipo_det_ven_lista(ld_datawindow, i_coltext)

			select 	cod_iva  
			into   		:ls_cod_iva  
			from   	tab_tipi_det_ven  
			where  	cod_azienda = :s_cs_xx.cod_azienda and  
					 	cod_tipo_det_ven = :i_coltext;
					 
			if sqlca.sqlcode = 0 then
				this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
			end if
			
			//------------------------------------------------------------------------------------------------
			//se il cliente è iva esente imposta l'iva in maniera corretta
			if not isnull(ls_cod_cliente) then
				select cod_iva,
						 data_esenzione_iva
				into   	:ls_cod_iva,
						 :ldt_data_esenzione_iva
				from   anag_clienti
				where  cod_azienda = :s_cs_xx.cod_azienda and 
						 cod_cliente = :ls_cod_cliente;
			end if

			if sqlca.sqlcode = 0 then
				if ls_cod_iva <> "" and not isnull(ls_cod_iva) and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
					this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
				end if
			end if
			//------------------------------------------------------------------------------------------------
			
			
			
			setitem(getrow(), "nota_dettaglio", ls_null)
			setitem(getrow(), "dim_x", 0)
			setitem(getrow(), "dim_y", 0)
		
		
		case "cod_prodotto"
			if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1

			select cod_misura_mag,
					 cod_misura_ven,
					 fat_conversione_ven,
					 flag_decimali,
					 cod_iva,
					 des_prodotto,
					 flag_blocco
			into   :ls_cod_misura_mag,
					 :ls_cod_misura,
					 :ld_fat_conversione,
					 :ls_flag_decimali,
					 :ls_cod_iva,
					 :ls_des_prodotto,
					 :ls_flag_blocco
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_prodotti.cod_prodotto = :i_coltext;
   
			if sqlca.sqlcode = 0 then
				
				//Donato 13/03/2012 gestione alert se gestisco da qui un prodotto che in realtà andrebbe configurato
				li_ret = wf_check_prodotto(i_coltext, ls_errore)
				if li_ret < 0 then
					g_mb.error("APICE",ls_errore)
				elseif li_ret = 1 then
					//dai alert
					g_mb.show("APICE",ls_errore)
				end if
				//----------------------------------------------------------------------------------------------------------------------
				if guo_functions.uof_get_note_fisse(ls_null, ls_null, i_coltext, "ORD_VEN", ls_null, ldt_data_registrazione, ls_nota_testata, ls_stampa_piede, ls_nota_video) < 0 then
					g_mb.error(ls_nota_testata)
				else
					if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
				end if
				
				this.setitem(i_rownbr, "cod_misura", ls_cod_misura)
				this.setitem(i_rownbr, "des_prodotto", ls_des_prodotto)

				dw_det_ord_ven_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
				dw_det_ord_ven_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")
				dw_det_ord_ven_det_1.modify("prezzo_ordine_t.text='Prezzo " + ls_cod_misura + ":'")
				dw_det_ord_ven_det_1.modify("quan_ordine_t.text='Quantità " + ls_cod_misura + ":'")

				if ld_fat_conversione <> 0 then
					this.setitem(i_rownbr, "fat_conversione_ven", ld_fat_conversione)
				else
					ld_fat_conversione = 1
              		this.setitem(i_rownbr, "fat_conversione_ven", 1)
				end if
				if not isnull(ls_cod_iva) then
	           		this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
				end if
					
				uo_1.uof_aggiorna(i_coltext)
				uo_1.uof_ultimo_prezzo(ls_cod_cliente, i_coltext)

				dw_det_ord_ven_lista.event post ue_costo_ultimo(i_rownbr)

			else
				
				/*uo_1.hide()
				dw_det_ord_ven_det_1.change_dw_current()
				s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
				s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
				s_cs_xx.parametri.parametro_pos_ricerca = i_coltext
				s_cs_xx.parametri.parametro_tipo_ricerca = 1
				if not isvalid(w_prodotti_ricerca) then
					window_open(w_prodotti_ricerca, 0)
				end if
				w_prodotti_ricerca.show() 
				*/
				this.change_dw_current()
				guo_ricerca.uof_ricerca_prodotto(dw_det_ord_ven_det_1, "cod_prodotto")
				
				return
			end if
			
			dw_det_ord_ven_lista.postevent("ue_postopen")

			setitem(getrow(), "dim_x", 0)
			setitem(getrow(), "dim_y", 0)

			if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_ord_ven_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(ld_fat_conversione)) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
			else
				dw_det_ord_ven_det_1.modify("st_fattore_conv.text=''")
			end if
			
			if not isnull(ls_cod_cliente) then
				
				select 	cod_iva,
						 	data_esenzione_iva
				into   		:ls_cod_iva,
						 	:ldt_data_esenzione_iva
				from   	anag_clienti
				where  	cod_azienda = :s_cs_xx.cod_azienda and 
						 	cod_cliente = :ls_cod_cliente;

				if sqlca.sqlcode = 0 then
					if ls_cod_iva <> "" and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
						this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
					end if
				end if
				
			end if

			ls_cod_prodotto = i_coltext
			ld_quantita = this.getitemnumber(i_rownbr, "quan_ordine")
			
			if not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
				ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")
			else
				ldt_data_consegna = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
			end if
			
			if ls_flag_decimali = "N" and &
				ld_quantita - int(ld_quantita) > 0 then
				ld_quantita = ceiling(ld_quantita)
				this.setitem(i_rownbr, "quan_ordine", ld_quantita)
			end if
			
			if mid(f_flag_controllo(),1,1) = "S" and ls_flag_prezzo_bloccato="N" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			if f_leggi_nota_prodotto(ls_cod_tipo_det_ven, ls_cod_prodotto, ls_nota_prodotto) = 0 then
				dw_det_ord_ven_det_1.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
				dw_det_ord_ven_det_1.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if

			f_PO_LoadDDDW_DW(dw_det_ord_ven_det_1,"cod_versione",sqlca,&
                 	  "distinta_padri","cod_versione","des_versione",&
	  		           "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + i_coltext + "'")
		
			luo_default_prodotto = CREATE uo_default_prodotto
			if luo_default_prodotto.uof_flag_gen_commessa(i_coltext) = 0 then
				setitem(getrow(),"cod_versione",luo_default_prodotto.is_cod_versione)
				setitem(getrow(),"flag_gen_commessa",luo_default_prodotto.is_flag_gen_commessa)
				setitem(getrow(),"flag_presso_cgibus",luo_default_prodotto.is_flag_presso_cg)
				setitem(getrow(),"flag_presso_ptenda",luo_default_prodotto.is_flag_presso_pt)
			end if
			destroy luo_default_prodotto
			
			if ls_cod_misura_mag <> ls_cod_misura then postevent("ue_ricalcola_colonne")

			//verifica lo stato dei reparti e visualizza il semaforo
			if data<>"" then dw_det_ord_ven_lista.event post ue_cal_trasferimenti()
			//wf_semaforo_produzione(this, data, i_rownbr)

		case "cod_misura"
			dw_det_ord_ven_lista.postevent("ue_postopen")
			
			
		case "fat_conversione_ven"
			ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			select anag_prodotti.cod_misura_mag
			into   :ls_cod_misura_mag
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> this.getitemstring(i_rownbr, "cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_ord_ven_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + i_coltext + " " + this.getitemstring(i_rownbr, "cod_misura") + ")'")
			else
				dw_det_ord_ven_det_1.modify("st_fattore_conv.text=''")
			end if
			
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "fattore", &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quan_ordine"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_vendita"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_um"), &
			              												double(i_coltext), &
																			ls_cod_valuta)
			
			
		case "quan_ordine"
			ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			select anag_prodotti.flag_decimali
			into   :ls_flag_decimali
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
			if sqlca.sqlcode = 0 then
				if ls_flag_decimali = "N" and &
					(double(i_coltext) - int(double(i_coltext))) > 0 then
					i_coltext = string(ceiling(double(i_coltext)))
					this.setitem(i_rownbr, "quan_ordine", double(i_coltext))
					this.settext(i_coltext)
					return 2
				end if
			end if

         	ld_quantita = double(i_coltext)
			ld_dim_x = getitemnumber(i_rownbr, "dim_x")
			ld_dim_y = getitemnumber(i_rownbr, "dim_y")
			
			if mid(f_flag_controllo(),2,1) = "S"  and ls_flag_prezzo_bloccato="N" then
				if not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
					ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")
				else
					ldt_data_consegna = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
				end if
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "quan_doc", &
			              												ld_quantita, &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_vendita"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
			dw_det_ord_ven_lista.event post ue_costo_ultimo(i_rownbr)
			

		case "data_consegna"
			if mid(f_flag_controllo(),4,1) = "S" then
				ld_dim_x = getitemnumber(i_rownbr, "dim_x")
				ld_dim_y = getitemnumber(i_rownbr, "dim_y")
				ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
				ld_quantita = this.getitemnumber(i_rownbr, "quan_ordine")
				if not isnull(i_coltext) then
					ldt_data_consegna = datetime(date(i_coltext))
				else
					ldt_data_consegna = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
				end if
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			
		case "sconto_1", "sconto_2", "sconto_3", "sconto_4", "sconto_5", "sconto_6", "sconto_7", "sconto_8", "sconto_9", "sconto_10"
			if mid(f_flag_controllo(),9,1) = "S"  and ls_flag_prezzo_bloccato="N" then
				ld_dim_x = getitemnumber(i_rownbr, "dim_x")
				ld_dim_y = getitemnumber(i_rownbr, "dim_y")
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.prezzo_listino = getitemnumber(getrow(),"prezzo_vendita")
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_ordine")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			
		case "num_confezioni"
         	ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			ld_quantita = this.getitemnumber(this.getrow(),"quan_ordine")
			ld_num_confezioni = double(i_coltext)
			ld_num_pezzi  = this.getitemnumber(this.getrow(),"num_pezzi_confezione")
			ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")
			ld_dim_x = getitemnumber(i_rownbr, "dim_x")
			ld_dim_y = getitemnumber(i_rownbr, "dim_y")
			select flag_prezzo
			into   :ls_flag_prezzo
			from   tab_misure
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_misura = :ls_cod_misura;
			if sqlca.sqlcode = 0 then
				if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 then
					if ls_flag_prezzo = 'S' then
						if ld_num_pezzi > 0 and not isnull(ld_num_pezzi) then
							ld_quantita = ld_num_pezzi * ld_num_confezioni
							this.setitem(i_rownbr, "quan_ordine", ld_quantita)
						else
							if not isnull(ls_cod_prodotto) then
								// vado a vedere se ci sono pezzi per collo impostati in anagrafica prodotti
								select 	pezzi_collo
								into		:ld_pezzi_collo
								from 		anag_prodotti
								where 	cod_azienda = :s_cs_xx.cod_azienda and
											cod_prodotto = :ls_cod_prodotto;
								if isnull(ld_pezzi_collo) then ld_pezzi_collo = 1
							else
								ld_pezzi_collo = 1
							end if
							this.setitem(i_rownbr, "num_pezzi_confezione", ld_pezzi_collo)
							ld_quantita = ld_pezzi_collo * ld_num_confezioni
							this.setitem(i_rownbr, "quan_ordine", ld_quantita)
						end if					
					else
						ld_quantita = ld_num_confezioni
						this.setitem(i_rownbr, "quan_ordine", ld_quantita)
					end if
				end if
			else
				g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
				return 1
			end if		
			
			if mid(f_flag_controllo(),6,1) = "S" and ls_flag_prezzo_bloccato="N"  then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			
		case "num_pezzi_confezione"
         	ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			ld_quantita = this.getitemnumber(this.getrow(),"quan_ordine")
			ld_num_pezzi = double(i_coltext)
			ld_num_confezioni = this.getitemnumber(this.getrow(),"num_confezioni")
			ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")
			ld_dim_x = getitemnumber(i_rownbr, "dim_x")
			ld_dim_y = getitemnumber(i_rownbr, "dim_y")
			select flag_prezzo
			into   :ls_flag_prezzo
			from   tab_misure
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_misura = :ls_cod_misura;
					 
			if sqlca.sqlcode = 0 then
				if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
					if ls_flag_prezzo = 'S' then
						ld_quantita = ld_num_pezzi * ld_num_confezioni
						this.setitem(i_rownbr, "quan_ordine", ld_quantita)
					else
						ld_quantita = ld_num_confezioni
						this.setitem(i_rownbr, "quan_ordine", ld_quantita)
					end if
				end if
			else
				g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
				return 1
			end if				

			if mid(f_flag_controllo(),5,1) = "S" and ls_flag_prezzo_bloccato="N"  then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			
		case "prezzo_vendita"
			if mid(f_flag_controllo(),3,1) = "S"  and ls_flag_prezzo_bloccato="N" then
				ld_dim_x = getitemnumber(i_rownbr, "dim_x")
				ld_dim_y = getitemnumber(i_rownbr, "dim_y")
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.prezzo_listino = double(i_coltext)
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_ordine")
				iuo_condizioni_cliente.ib_prezzi = false
				
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "prezzo_doc", &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quan_ordine"), &
			              												double(i_coltext), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
			dw_det_ord_ven_lista.event post ue_costo_ultimo(i_rownbr)
			
																			
		case "quantita_um"  
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "quan_um", &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quan_ordine"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_vendita"), &
			              												double(i_coltext), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
																			
																			
		case "prezzo_um" 
			
			wf_controlla_ddt_collegati()
			
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "prezzo_um", &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quan_ordine"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_vendita"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quantita_um"), &
			              												double(i_coltext), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
																			
			
		case "num_riga_appartenenza"
			ll_riga_appartenenza = double(data)
			if ll_riga_appartenenza > 0 then
				ll_anno_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "anno_registrazione")
				ll_num_registrazione  = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "num_registrazione")
				select prog_riga_ord_ven
				into  :ll_i
				from  det_ord_ven
				where cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :ll_anno_registrazione and
						num_registrazione = :ll_num_registrazione and
						prog_riga_ord_ven = :ll_riga_appartenenza;
				if sqlca.sqlcode = 0 then
					return 0
				else
					g_mb.messagebox("APICE","Indicare una riga di riferimento valida")
					return 1
				end if
			end if
			

		case "flag_riga_sospesa"
			if i_coltext = "S" then
				dw_det_ord_ven_det_1.object.cod_tipo_causa_sospensione.visible = 1
			else
				setitem(getrow(),"cod_tipo_causa_sospensione", ls_null)
				dw_det_ord_ven_det_1.object.cod_tipo_causa_sospensione.visible = 0
			end if
		
		
		case "sconto_1","sconto_2","sconto_3","sconto_4","sconto_5","sconto_6","sconto_7","sconto_8","sconto_9","sconto_10"
			dw_det_ord_ven_lista.event post ue_costo_ultimo(i_rownbr)
		
	end choose
end if
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_modify, ls_flag_tipo_det_ven, ls_cod_tipo_det_ven, &
       ls_null, ls_match, ls_match_1


setnull(ls_null)
if i_rownbr > 0 then
   ls_cod_tipo_det_ven = this.getitemstring(i_rownbr,"cod_tipo_det_ven")

   select tab_tipi_det_ven.flag_tipo_det_ven
   into   :ls_flag_tipo_det_ven
   from   tab_tipi_det_ven
   where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita.", &
                 exclamation!, ok!)
      return
   end if

   if i_insave > 0 then
      if ls_flag_tipo_det_ven = "M" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("cod_misura")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Unità di misura obbligatoria.", exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("quan_ordine")
         if double(get_col_data(i_rownbr, i_colnbr)) = 0 then
          	g_mb.messagebox("Attenzione", "Quantità ordine obbligatoria.", exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      elseif ls_flag_tipo_det_ven = "C" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      end if
   end if
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	dw_documenti.uof_retrieve_blob(getrow())
	dw_det_ord_ven_lista.setrow(getrow())
	
end if

setnull(idt_data_partenza)
end event

event buttonclicked;call super::buttonclicked;string											ls_nota_dettaglio, ls_flag_tipo_avanzamento,ls_errore, ls_null, ls_cod_dep_giro, ls_cod_dep_origine
s_cs_xx_parametri 						lstr_parametri
uo_calendario_prod_new				luo_cal_prod
long											ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_numero, ll_num_commessa
integer										li_anno_commessa,li_risposta
uo_funzioni_1								luo_funzioni_1
uo_produzione								luo_prod
uo_log_sistema								luo_log


if isvalid(dwo) then
choose case dwo.name
	case "b_varianti"
		
		select parametri.numero  
		into   :ll_numero  
		from   parametri  
		where (parametri.flag_parametro = 'N' ) and
				(parametri.cod_parametro = 'GVA' )   ;
				
		if ll_numero > 0 and dw_det_ord_ven_lista.getrow() > 0 then
			//s_cs_xx.parametri.parametro_dw_1 = dw_det_ord_ven_lista
			//window_open(w_varianti_ordini,-1)
			
			//attivazione modifica della nota dettaglio --------------------------------
			lstr_parametri.parametro_b_1 = true
			
			ls_nota_dettaglio = dw_det_ord_ven_det_1.getitemstring(dw_det_ord_ven_det_1.getrow(), "nota_dettaglio")
			lstr_parametri.parametro_s_1_a[1] = ls_nota_dettaglio
			lstr_parametri.parametro_dw_1 = dw_det_ord_ven_lista
			
			window_open_parm(w_varianti_ordini, -1, lstr_parametri)
			//----------------------------------------------------------------------------
			
		end if		
		
	// Giulio: 03/11/2011 modifica ricerca prodotti
	case "b_ricerca_prodotto"
		dw_det_ord_ven_det_1.change_dw_current()
		guo_ricerca.uof_ricerca_prodotto(dw_det_ord_ven_det_1, "cod_prodotto")
		
	case "b_canc_commessa"			//in realtà è CHIUSURA COMMESSA
		
		setnull(ls_null)
		li_anno_commessa = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_commessa")
		ll_num_commessa = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_commessa")
		
		ll_anno_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione")
		
		if li_anno_commessa = 0 or isnull(li_anno_commessa) then 
			g_mb.messagebox("Apice","Attenzione! Non esiste alcuna commessa associata al dettaglio ordine. Generare prima la commessa!",stopsign!)
			return
		end if
		
		select flag_tipo_avanzamento
		into   :ls_flag_tipo_avanzamento
		from   anag_commesse
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_commessa=:li_anno_commessa
		and    num_commessa=:ll_num_commessa;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Attenzione! Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return
		end if
		
		if ls_flag_tipo_avanzamento = "9" or ls_flag_tipo_avanzamento = "8" or ls_flag_tipo_avanzamento = "7" or &
			ls_flag_tipo_avanzamento = "1" or ls_flag_tipo_avanzamento = "2" then
			g_mb.messagebox("Apice","Attenzione! La commessa si trova in uno stato per il quale non è più possibile effettuare un avanzamento automatico.",stopsign!)
			return
		else
						
			luo_funzioni_1 = create uo_funzioni_1
			luo_funzioni_1.uof_set_posticipato(true)
			
			
			//----------------------------------------------------------------------------------
			//se l'ordine ha un giro consegna con deposito applicato e diverso da quello oriogine ordine, allora il deposito di chiusura della commessa deve essere questo
			//quindi le MP saranno prelevate da questo deposito e il PF verrà caricato in questo deposito!!
			luo_prod = create uo_produzione
			ls_cod_dep_giro = luo_prod.uof_deposito_giro_consegna(ll_anno_registrazione, ll_num_registrazione)
			destroy luo_prod
			
			if g_str.isnotempty(ls_cod_dep_giro) then
				//leggo il deposito origine ordine
				select cod_deposito
				into :ls_cod_dep_origine
				from tes_ord_ven
				where 	cod_azienda = :s_cs_xx.cod_azienda and
							anno_registrazione = :ll_anno_registrazione and
							num_registrazione = :ll_num_registrazione;
				
				if g_str.isnotempty(ls_cod_dep_origine) and ls_cod_dep_origine<>ls_cod_dep_giro then
					
					luo_funzioni_1.ib_forza_deposito_scarico_mp = true
					luo_funzioni_1.is_cod_deposito_scarico_mp = ls_cod_dep_giro
					
					luo_log = create uo_log_sistema
					luo_log.uof_write_log_sistema_not_sqlca("DEP.CONS.COMM", "Applicato deposito codice "+ls_cod_dep_giro+" del giro consegna dell'ordine "+string(ll_anno_registrazione)+"/" + string(ll_num_registrazione)+&
																									" al posto del deposito origine "+ls_cod_dep_origine+" in chiusura commessa n. "+string(li_anno_commessa)+"/" + string(ll_num_commessa) + &
																									" da riga ordine vendita.")
					destroy luo_log
				end if
			end if
			//----------------------------------------------------------------------------------
			
			
			li_risposta = luo_funzioni_1.uof_avanza_commessa(li_anno_commessa,ll_num_commessa,ls_null, ls_errore)
			
			if li_risposta < 0 then 
				li_risposta = f_scrivi_log ("Errore sulla commessa anno " + string(li_anno_commessa) +" numero " + string(ll_num_commessa) + ". Dettaglio errore: " + ls_errore)
				g_mb.messagebox("Sep","Si è manifestato un errore durante la chisusura commessa: consultare il file di log.",stopsign!) 
				if li_risposta= -1 then
					g_mb.messagebox("Sep","Si è manifestato un errore durante la chisusura commessa ma non è stato possibile scrivere il file di log. Questo errore non è bloccante (riguarda esclusivamente la creazione del file log molto probabilmente manca il parametro log).",stopsign!) 
				end if	
				
				rollback;
				destroy luo_funzioni_1
				return
				
			end if
			
			destroy luo_funzioni_1
			
			
			//pulizia tabella det_ord_ven_prod, MA PRIMA AGGIORNA LA TAB_CAL_PRODUZIONE -----------------------------------------
			luo_cal_prod = create uo_calendario_prod_new
			
			ll_anno_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "anno_registrazione")
			ll_num_registrazione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "num_registrazione")
			ll_prog_riga_ord_ven = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "prog_riga_ord_ven")
			
			li_risposta = luo_cal_prod.uof_disimpegna_calendario(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_errore)
			destroy luo_cal_prod;
			
			if li_risposta<0 then
				//messaggio ma non blocco
				//g_mb.warning(ls_errore)
				
			else
				delete det_ord_ven_prod
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    anno_registrazione = :ll_anno_registrazione
				and    num_registrazione  = :ll_num_registrazione
				and    prog_riga_ord_ven  =: ll_prog_riga_ord_ven;
				
			end if
			//--------------------------------------------------------------------------------------------------------------------------------------------
			
			commit;
			g_mb.messagebox("Sep","Impostazione Chiusura commessa avvenuta con successo!",information!) 
		end if 		
		
end choose
end if
end event

event clicked;call super::clicked;long										ll_num_commessa, ll_row
integer									li_anno_commessa
s_cs_xx_parametri						lstr_param
w_commesse_produzione_tv		lw_window


choose case dwo.name
	case "p_commessa"
		//apri la window delle commesse produzione
		ll_row = dw_det_ord_ven_det_1.getrow()
		
		if ll_row>0 then
			li_anno_commessa = dw_det_ord_ven_det_1.getitemnumber(ll_row, "anno_commessa")
			ll_num_commessa = dw_det_ord_ven_det_1.getitemnumber(ll_row, "num_commessa")
				
			if li_anno_commessa>0 and ll_num_commessa>0 then
				lstr_param.parametro_ul_1 = li_anno_commessa
				lstr_param.parametro_ul_2 = ll_num_commessa
				
				lstr_param.parametro_s_1 = "Commessa da riga ordine vendita n. "+&
														string(dw_det_ord_ven_det_1.getitemnumber(ll_row, "anno_registrazione")) + " / "+&
														string(dw_det_ord_ven_det_1.getitemnumber(ll_row, "num_registrazione")) + " / " + &
														string(dw_det_ord_ven_det_1.getitemnumber(ll_row, "prog_riga_ord_ven"))
				
				opensheetwithparm(lw_window, lstr_param, PCCA.MDI_Frame, 6,  Original!)
				//openwithparm(w_commesse_produzione_tv, lstr_param)
			else
				//nessuna commessa associata
				return
			end if
		end if
		
end choose
end event

type dw_comp_det_ord_ven from uo_cs_xx_dw within w_det_ord_ven_tv
integer x = 69
integer y = 140
integer width = 2912
integer height = 1816
integer taborder = 10
string dataobject = "d_comp_det_ord_ven"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven
	
ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")
ll_prog_riga_ord_ven = i_parentdw.getitemnumber(i_parentdw.getrow(), "prog_riga_ord_ven")
retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven)

dw_det_ord_ven_det_conf_variabili.settransobject(sqlca)
dw_det_ord_ven_det_conf_variabili.retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven)


end event

event itemchanged;call super::itemchanged;if i_extendmode then
	choose case i_colname
		case "cod_modello"
			if left(i_coltext,4) <> left(getitemstring(getrow(),"cod_modello"),4) then
				g_mb.messagebox("APICE","Non è possibile cambiare i primi 4 caratteri del modello")
				return 1
			end if
	end choose
end if
end event

type dw_folder from u_folder within w_det_ord_ven_tv
integer x = 23
integer y = 20
integer width = 4279
integer height = 2200
integer taborder = 60
end type

type dw_det_ord_ven_lista from uo_cs_xx_dw within w_det_ord_ven_tv
event ue_key pbm_dwnkey
event ue_postopen ( )
event ue_riallinea_riga ( )
event ue_posiziona_riga ( )
event ue_storicizzazione_totale ( )
event ue_storicizzazione_parziale ( )
event ue_cal_trasferimenti ( )
event ue_costo_ultimo ( long al_row )
event ue_elimina_commesse ( )
integer x = 50
integer y = 140
integer width = 4224
integer height = 1784
integer taborder = 40
boolean bringtotop = true
string dataobject = "d_det_ord_ven_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;call super::ue_key;boolean lb_riallinea_dettaglio

integer li_anno_commessa

string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, &
       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_listino, ls_messaggio, &
		 ls_cod_tipo_det_ven_addizionali, ls_mp_escluse[], ls_testo_esclusioni, ls_title, &
		 ls_parametro, ls_flag_tipo_det_ven,ls_flag_gen_commessa, ls_errore,ls_cod_tipo_ord_ven, &
		 ls_cod_prodotto_raggruppato

long   ll_num_riga_appartenenza, ll_anno_registrazione,ll_num_registrazione, ll_prog_riga_ord_ven, ll_ret, &
       ll_righe_escluse[], ll_riga_corrente,ll_num_commessa, ll_null

dec{4} ll_sconti[], ll_maggiorazioni[], ll_i, ll_y, ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, &
       ld_ultimo_prezzo, ldd_quan_evasa, ldd_quan_in_evasione, ld_quan_ordine, ld_quan_evasa, ld_quan_raggruppo

datetime ldt_data_registrazione

uo_storicizzazione luo_storicizzazione

uo_funzioni_1 luo_funzioni_1



select stringa
into   :ls_parametro
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_parametro = 'CSO';
		 
if sqlca.sqlcode = 0 then
	
	if pos(ls_parametro, s_cs_xx.cod_utente) > 0 then
	// specifica "storico ordini"
		if keyflags = 3 and key = KeyF9! and len(is_text_storico) > 0 then
			
			s_cs_xx.parametri.parametro_s_10 = is_text_storico
			window_open(w_det_ord_ven_text_storico,0)
			return
			
		end if
			
		// storicizzazione totale prodotto
		if keyflags = 3 and key = KeyF8! then
			triggerevent("ue_storicizzazione_totale")
		end if
		
		// storicizzazione parziale del prodotto finito
		if keyflags = 3 and key = KeyF7! then
			triggerevent("ue_storicizzazione_parziale")
		end if
	end if

end if

// Enrico 20/03/2006 specifica "modifica_etichette" rev 1 del 7/3/2006
if keyflags = 2 and key = KeyM! then
	if g_mb.messagebox("APICE","Procedo con svincolo riga?",Question!,YesNo!,2) = 2 then return
	
	il_riga_selezionata = this.getrow()
	ll_anno_registrazione = getitemnumber(getrow(),"anno_registrazione")
	ll_num_registrazione = getitemnumber(getrow(),"num_registrazione")
	ll_prog_riga_ord_ven = getitemnumber(getrow(),"prog_riga_ord_ven")
	ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
	ls_cod_tipo_det_ven = getitemstring(getrow(),"cod_tipo_det_ven")
	ld_quan_ordine = getitemnumber(getrow(),"quan_ordine")
	ld_quan_evasa = getitemnumber(getrow(),"quan_evasa")
	ls_flag_gen_commessa = getitemstring(getrow(),"flag_gen_commessa")
	li_anno_commessa = getitemnumber(getrow(),"anno_commessa")
	ll_num_commessa = getitemnumber(getrow(),"num_commessa")
	
	if not isnull(li_anno_commessa) or not isnull(ll_num_commessa) then
		g_mb.messagebox("APICE","La riga ha già generato una commessa: impossibile procedere, CANCELLARE LA COMMESSA E RIPETERE IL COMANDO.",Stopsign!)
		return
	end if
	
	if ls_cod_tipo_det_ven = "MAG" then
		g_mb.messagebox("APICE","La riga è già di tipo MAG. Impossibile procedere!",Stopsign!)
		return
	end if
	
	if ld_quan_evasa > 0 then
		g_mb.messagebox("APICE","La riga è già stata evasa o parzialmente evasa. Impossibile procedere!",Stopsign!)
		return
	end if
	
	select flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			 
	if ls_flag_tipo_det_ven <> "M" then
		update anag_prodotti
		set    quan_impegnata = quan_impegnata + :ld_quan_ordine
		where cod_azienda = :s_cs_xx.cod_azienda and
		      cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode < 0 then			
			g_mb.messagebox("APICE","Errore in svincolo riga durante agg. prodotto.~r~n"+sqlca.sqlerrtext,stopsign!)
			rollback;
			return
		end if
		
		// enme 08/1/2006 gestione prodotto raggruppato
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then
			
			ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordine, "M")
			
			update anag_prodotti
			set    quan_impegnata = quan_impegnata + :ld_quan_raggruppo
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_cod_prodotto_raggruppato;
			if sqlca.sqlcode < 0 then			
				g_mb.messagebox("APICE","Errore in svincolo riga durante agg. prodotto.~r~n"+sqlca.sqlerrtext,stopsign!)
				rollback;
				return
			end if
		end if
		
		
	end if
	
	update det_ord_ven
	set num_riga_appartenenza = 0, 
	    cod_tipo_det_ven = 'MAG'
	where cod_azienda = :s_cs_xx.cod_azienda and
	      anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode < 0 then			
		g_mb.messagebox("APICE","Errore in svincolo riga in aggiormanento riga ord.~r~n"+sqlca.sqlerrtext,stopsign!)
		rollback;
		return
	end if
	
	// ------------------------------------- genero anche la commessa e se richiesto la chiudo ------------------------------------
	if ls_flag_gen_commessa = "S" then
		
		luo_funzioni_1 = CREATE uo_funzioni_1
		select cod_tipo_ord_ven
		into   :ls_cod_tipo_ord_ven
		from   tes_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione  = :ll_num_registrazione;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Svincolo Riga - Creaz. Commessa","Errore in ricerca ordine: procedura interrotta",stopsign!)
			rollback;
			return
		end if
		setnull(ll_null)	
		
		if luo_funzioni_1.uof_genera_commesse(ls_cod_tipo_ord_ven, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_null, ref li_anno_commessa, ref ll_num_commessa, ref ls_errore) = 0 then
			g_mb.messagebox("Svincolo Riga", "Generata Commessa:" + string(li_anno_commessa, "####") + "-" + string(ll_num_commessa,"######"))
			commit;
		else
			g_mb.messagebox("Svincolo Riga - Creaz.Commessa","Errore in generazione commessa.~r~n" + ls_errore)
			rollback;
			return
		end if
	end if	
	destroy luo_funzioni_1
	
	commit;
	
	parent.triggerevent("pc_retrieve")
	postevent("ue_posiziona_riga")
	
end if


// 14/01/2020 EnMe x Alusistemi. Cancella commessa e produzione con CTRL-P
if keyflags = 2 and key = KeyP! then
	event ue_elimina_commesse()
end if


choose case this.getcolumnname()

	case "sconto_1"
		if key = keyenter! then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		end if
		
		//Donato 18/10/2012 su richiesta di Alberto/Beatrice attivo il salva+nuovo al TAB su dim_y, pertanto commento questo
//		if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
//			this.triggerevent("pcd_save")
//			this.postevent("pcd_new")
//		end if
		//fine modifica ------------------------------------------------------------------------------------
	
	//Donato 18/10/2012 su richiesta di Alberto/Beatrice attivo il salva+nuovo al TAB su dim_y
	case "dim_y"
		if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		end if
	//fine modifica ------------------------------------------------------------------------------------
				
	case "prezzo_vendita"
		if key = keyenter! then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		else
			if (key = keyF1! or key = keyF2! or key = keyF3! or key = keyF4! or key = keyF5!) and keyflags = 1 then
				setpointer(hourglass!)
				ldt_data_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
				ls_cod_cliente = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
				ls_cod_valuta = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
				ls_cod_agente_1 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
				ls_cod_agente_2 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
				ld_cambio_ven = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
				ls_cod_tipo_det_ven = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_tipo_det_ven")
				ls_cod_prodotto = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_prodotto")
				ld_quantita = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_fatturata")
				choose case key
					case keyF1!
						ls_listino = 'LI1'
					case keyF2!
						ls_listino = 'LI2'
					case keyF3!
						ls_listino = 'LI3'
					case keyF4!
						ls_listino = 'LI4'
					case keyF5!
						ls_listino = 'LI5'
					case else
						return
				end choose
				if ls_listino <> "LI5" then
					select stringa
					into  :ls_stringa
					from  parametri_azienda
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_parametro = :ls_listino ;
					if sqlca.sqlcode = 0 then
						f_ricerca_provvigioni(ls_stringa, ls_cod_cliente, ls_cod_valuta, ld_cambio_ven, ls_cod_prodotto, ld_quantita, ldt_data_registrazione, ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven)
			
						iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
						iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_stringa
						iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
						iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
						iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
						iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
						iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
						iuo_condizioni_cliente.str_parametri.dim_1 = 0
						iuo_condizioni_cliente.str_parametri.dim_2 = 0
						iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
						iuo_condizioni_cliente.str_parametri.valore = 0
						iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
						iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
						iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
						iuo_condizioni_cliente.wf_condizioni_cliente()
					end if			
					setpointer(arrow!)
				else
					select prezzo_acquisto
					into   :ld_prezzo_acquisto
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
					       cod_prodotto = :ls_cod_prodotto;
					dw_det_ord_ven_lista.setitem(i_rownbr, "prezzo_vendita", ld_prezzo_acquisto)
				end if					 
				setpointer(arrow!)
			end if
		end if
		
	//Giulio: 03/11/2011 modifica ricerca prodotti
	case "cod_prodotto", "des_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_ord_ven_lista,"cod_prodotto")		
		end if

end choose

end event

event ue_postopen();if this.getrow() > 0 then
	datawindow ld_datawindow
	
	uo_gestione_conversioni iuo_oggetto
	iuo_oggetto = create uo_gestione_conversioni
	ld_datawindow  = dw_det_ord_ven_det_1
	iuo_oggetto.uof_visualizza_um(ld_datawindow, "ord_ven")
	if ib_stato_nuovo or ib_stato_modifica then 	iuo_gestione_conversioni.uof_blocca_colonne(ld_datawindow,"ord_ven")
	destroy iuo_oggetto
	
end if
end event

event ue_riallinea_riga;uo_gestione_conversioni iuo_oggetto
iuo_oggetto = create uo_gestione_conversioni
iuo_oggetto.uof_visualizza_um(dw_det_ord_ven_det_1, "ord_ven")
destroy iuo_oggetto

end event

event ue_posiziona_riga();this.change_dw_current()
this.setrow(il_riga_selezionata)

end event

event ue_storicizzazione_totale();boolean lb_riallinea_dettaglio
string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, &
       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_listino, ls_messaggio, &
		 ls_cod_tipo_det_ven_addizionali, ls_mp_escluse[], ls_testo_esclusioni, ls_title, &
		 ls_parametro
long   ll_num_riga_appartenenza, ll_anno_registrazione,ll_num_registrazione, ll_prog_riga_ord_ven, ll_ret, &
       ll_righe_escluse[], ll_riga_corrente
double ll_sconti[], ll_maggiorazioni[], ll_i, ll_y
double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo, &
       ldd_quan_evasa, ldd_quan_in_evasione
datetime ldt_data_registrazione
uo_storicizzazione luo_storicizzazione


if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_evasione") <> "A" then
	g_mb.messagebox("APICE","Non è possibile effettuare storicizzazioni di righe già evase o parzialmente evase")
	return
end if

ldd_quan_evasa = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_evasa")
ldd_quan_in_evasione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_in_evasione")
if ldd_quan_evasa > 0 or ldd_quan_in_evasione > 0  then
	g_mb.messagebox("APICE","Non è possibile effettuare storicizzazioni di righe con quantità già evasa o in evasione")
	return
end if

if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_blocco") = 'S' then
	g_mb.messagebox("APICE","Non puoi storicizzare una riga d'ordine bloccata !!!")
	return
end if

lb_riallinea_dettaglio = false


if this.getrow() > 0 then
	
	ll_anno_registrazione = getitemnumber(getrow(),"anno_registrazione")
	ll_num_registrazione = getitemnumber(getrow(),"num_registrazione")
	ll_prog_riga_ord_ven = getitemnumber(getrow(),"prog_riga_ord_ven")
	ll_num_riga_appartenenza = getitemnumber(getrow(),"num_riga_appartenenza")
	
	if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then
		// è una riga di prodotto finito, non addizionale optional o altro del genere
		if g_mb.messagebox("STORICIZZAZIONE","Confermi la cancellazione e storicizzazione TOTALE ?",Question!,YesNo!,2) = 1 then
			
			// funzione di storicizzazione riga prodotto finito
			luo_storicizzazione = create uo_storicizzazione
			
			ls_title = parent.title
			parent.title = ls_title + " STORICIZZAZIONE IN CORSO..."
			
			ll_ret = luo_storicizzazione.uof_storicizza(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, true, ls_mp_escluse[], ll_righe_escluse[], ref ls_testo_esclusioni, ref ls_messaggio )
			
			parent.title = ls_title
			
			if ll_ret < 0 then
				g_mb.messagebox("APICE","Errore nella storicizzazione.~r~n"+ls_messaggio)
				rollback;
				destroy luo_storicizzazione
				return
			end if
			
			commit;
			destroy luo_storicizzazione
			lb_riallinea_dettaglio = true
		
		end if
		
	else	
		// controllo se si tratta di una riga di addizionale optional o altro del genere
		ls_cod_tipo_det_ven = getitemstring(getrow(), "cod_tipo_det_ven")
		
		select cod_tipo_det_ven_addizionali
		into   :ls_cod_tipo_det_ven_addizionali
		from   tab_flags_configuratore
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_modello in (select cod_prodotto
									  from   det_ord_ven
									  where  cod_azienda = :s_cs_xx.cod_azienda and
												anno_registrazione = :ll_anno_registrazione and
												num_registrazione = :ll_num_registrazione and
												prog_riga_ord_ven = :ll_num_riga_appartenenza);
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Storicizzazione", "Errore in ricerca tipo dettaglio addizionali in tabella flags configuratore~r~n"+ sqlca.sqlerrtext)
			return
		end if
		

		// se si tratta di un'addizionale devo mettere un messaggio speciale
		if ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_addizionali then
			
			if g_mb.messagebox("STORICIZZAZIONE","ATTENZIONE: E' UNA ADDIZIONALE. PROCEDO ?",Question!,YesNo!,2) = 2 then return
			
		end if
		
		if g_mb.messagebox("STORICIZZAZIONE","Confermi la cancellazione e storicizzazione TOTALE ?",Question!,YesNo!,2) = 1 then
			
			// funzione di storicizzazione riga non prodotto finito
			luo_storicizzazione = create uo_storicizzazione

			ls_title = parent.title
			parent.title = ls_title + " STORICIZZAZIONE IN CORSO..."
			
			ll_ret = luo_storicizzazione.uof_storicizza(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, false, ls_mp_escluse[], ll_righe_escluse[], ref ls_testo_esclusioni, ref ls_messaggio )
			
			parent.title = ls_title
			
			if ll_ret < 0 then
				g_mb.messagebox("APICE","Errore nella storicizzazione.~r~n"+ls_messaggio)
				rollback;
				destroy luo_storicizzazione
				return
			end if
			
			commit;
			destroy luo_storicizzazione
			lb_riallinea_dettaglio = true
			
		end if
			
	end if
	
end if


if lb_riallinea_dettaglio then
	
	ll_riga_corrente = dw_det_ord_ven_lista.getrow()
	change_dw_current()
	parent.triggerevent("pc_retrieve")
	
	if rowcount() > 0 then
		change_dw_current()
		if ll_riga_corrente > rowcount() then ll_riga_corrente = rowcount()
		setrow(ll_riga_corrente)
	end if
	
	parent.title = ls_title
	
end if
			

end event

event ue_storicizzazione_parziale();boolean lb_riallinea_dettaglio
string ls_mp_escluse[], ls_testo_esclusioni, ls_title, ls_parametro, ls_cod_versione, ls_cod_prodotto
long   ll_num_riga_appartenenza, ll_anno_registrazione,ll_num_registrazione, ll_prog_riga_ord_ven, ll_ret, &
       ll_righe_escluse[], ll_riga_corrente, ll_cont
double ldd_quan_evasa, ldd_quan_in_evasione
datetime ldt_data_registrazione
uo_storicizzazione luo_storicizzazione

if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_evasione") <> "A" then
	g_mb.messagebox("APICE","Non è possibile effettuare storicizzazioni di righe già evase o parzialmente evase")
	return
end if

ldd_quan_evasa = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_evasa")
ldd_quan_in_evasione = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(), "quan_in_evasione")
if ldd_quan_evasa > 0 or ldd_quan_in_evasione > 0  then
	g_mb.messagebox("APICE","Non è possibile effettuare storicizzazioni di righe con quantità già evasa o in evasione")
	return
end if

if dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "flag_blocco") = 'S' then
	g_mb.messagebox("APICE","Non puoi storicizzare una riga d'ordine bloccata !!!")
	return
end if

lb_riallinea_dettaglio = false


if this.getrow() > 0 then
	
	ll_anno_registrazione = getitemnumber(getrow(),"anno_registrazione")
	ll_num_registrazione = getitemnumber(getrow(),"num_registrazione")
	ll_prog_riga_ord_ven = getitemnumber(getrow(),"prog_riga_ord_ven")
	ll_num_riga_appartenenza = getitemnumber(getrow(),"num_riga_appartenenza")
	ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
	ls_cod_versione = getitemstring(getrow(),"cod_versione")
	
	if isnull(ls_cod_prodotto) or len(ls_cod_prodotto) < 1 then
		g_mb.messagebox("STORICIZZAZIONE","La storicizzazione è consentita solo per prodotti codificati!",stopsign!)
		return
	end if
	
	if isnull(ls_cod_versione) or len(ls_cod_versione) < 1 then
		g_mb.messagebox("STORICIZZAZIONE","La storicizzazione è consentita solo per prodotti con distinta base!",stopsign!)
		return
	end if
	
	select count(*)
	into   :ll_cont
	from   distinta_padri
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto and
			 cod_versione = :ls_cod_versione;
			 
	if ll_cont < 1 or isnull(ll_cont) then
		g_mb.messagebox("STORICIZZAZIONE","La storicizzazione è consentita solo per prodotti con distinta base!",stopsign!)
		return
	end if
	
	if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then
		// è una riga di prodotto finito, non addizionale optional o altro del genere
		if g_mb.messagebox("STORICIZZAZIONE","Confermi la cancellazione e storicizzazione PARZIALE ?",Question!,YesNo!,2) = 1 then
			
			s_cs_xx.parametri.parametro_dw_1 = dw_det_ord_ven_lista
			s_cs_xx.parametri.parametro_s_10 = ""
			//window_open_parm(w_varianti_storico, -1, dw_det_ord_ven_lista)
			
		end if
	end if
end if



if lb_riallinea_dettaglio then
	
	ll_riga_corrente = dw_det_ord_ven_lista.getrow()
	change_dw_current()
	parent.triggerevent("pc_retrieve")
	
	if rowcount() > 0 then
		change_dw_current()
		if ll_riga_corrente > rowcount() then ll_riga_corrente = rowcount()
		setrow(ll_riga_corrente)
	end if
	
	parent.title = ls_title
	
end if
			

end event

event ue_cal_trasferimenti();
string				ls_cod_prodotto_finito, ls_cod_versione_finito, ls_errore, ls_cod_prodotto, ls_cod_tipo_ord_ven,ls_flag_tipo_bcl
long				ll_row, ll_riga_ordine, ll_temp
boolean			lb_esiste_riga
uo_calendario_prod_new			luo_cal_prod



if ii_anno_ordine>0 and il_num_ordine>0 then
else
	return
end if

ls_cod_tipo_ord_ven = f_des_tabella ("tes_ord_ven", "anno_registrazione="+string(ii_anno_ordine)+ " and num_registrazione="+string(il_num_ordine), "cod_tipo_ord_ven" )

//ls_flag_tipo_bcl = A - B - C
select flag_tipo_bcl
into :ls_flag_tipo_bcl
from tab_tipi_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;
			
if ls_flag_tipo_bcl<>"B" then return


ll_row = getrow()

lb_esiste_riga = false

if ll_row>0 then
	ll_riga_ordine = getitemnumber(ll_row, "prog_riga_ord_ven")
	
	if ll_riga_ordine>0 then
		
		setnull(ll_temp)
		
		select prog_riga_ord_ven
		into :ll_temp
		from det_ord_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ii_anno_ordine and
					num_registrazione=:il_num_ordine and
					prog_riga_ord_ven=:ll_riga_ordine;
		
		if ll_temp>0 then
			//riga esistente
			lb_esiste_riga = true
		else
			ll_riga_ordine = 0
			lb_esiste_riga = false
			
			ls_cod_prodotto = getitemstring(ll_row, "cod_prodotto")
		
			if isnull(ls_cod_prodotto) or ls_cod_prodotto="" then
				return
			end if
			
		end if
		
	else
		ll_riga_ordine = -1
		
		ls_cod_prodotto = getitemstring(ll_row, "cod_prodotto")
		
		if isnull(ls_cod_prodotto) or ls_cod_prodotto="" then
			return
		end if
		
	end if
	
else
	return
end if


luo_cal_prod = create uo_calendario_prod_new
luo_cal_prod.uof_dw_cal_produzione(	lb_esiste_riga, ii_anno_ordine, il_num_ordine, ll_riga_ordine, &
													ls_cod_prodotto, "", idt_data_partenza, &
													is_reparti[], idt_date_reparti[], dw_trasferimenti, ls_errore)

destroy	luo_cal_prod






end event

event ue_costo_ultimo(long al_row);uo_trova_prezzi							luo_prezzi
uo_calcola_documento_euro			luo_calcola_doc
dec{4}										ld_costo_acquisto, ld_null[], ld_imponibile_riga, ld_imponibile_riga_valuta, ld_quantita, ld_prezzo
datetime										ldt_data_fine
string											ls_cod_prodotto, ls_errore
integer										li_ret


if il_CCU>0 and al_row>0 then
else
	st_margine.text = ""
	return
end if

ldt_data_fine = datetime(today(), 00:00:00)
ls_cod_prodotto = dw_det_ord_ven_lista.getitemstring(al_row, "cod_prodotto")

if g_str.isempty(ls_cod_prodotto) then 
	st_margine.text = ""
	return
end if

luo_prezzi = create uo_trova_prezzi
li_ret = luo_prezzi.uof_ultimo_acquisto("F", ldt_data_fine, il_CCU, ls_cod_prodotto, "", ld_costo_acquisto, ls_errore)
destroy luo_prezzi

if li_ret < 0 then
	st_margine.text = ""
	return
end if


if isnull(ld_costo_acquisto) then ld_costo_acquisto = 0

if ld_costo_acquisto = 0 then
	
	//prendi il costo standard
	select costo_standard
	into :ld_costo_acquisto
	from anag_prodotti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:ls_cod_prodotto;
	
	if isnull(ld_costo_acquisto) then ld_costo_acquisto = 0
				
	st_margine.text = "Costo Standard = "
else
	st_margine.text = "Costo Ultimo = "
end if

st_margine.text += string(ld_costo_acquisto, "#####0.0000")

ld_prezzo = dw_det_ord_ven_lista.getitemnumber(al_row, "prezzo_vendita")
ld_quantita = dw_det_ord_ven_lista.getitemnumber(al_row, "quan_ordine")

if isnull(ld_prezzo) then ld_prezzo = 0
if isnull(ld_quantita) then ld_quantita = 0

if ld_prezzo>0 then
	luo_calcola_doc = create uo_calcola_documento_euro
	
	luo_calcola_doc.il_anno_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "anno_registrazione")
	luo_calcola_doc.il_num_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "num_registrazione")
	luo_calcola_doc.is_tipo_documento = "ord_ven"
	luo_calcola_doc.is_cod_valuta = is_cod_valuta
	luo_calcola_doc.id_cambio = id_cambio_ven_ds
	luo_calcola_doc.is_colonna_quantita = "quan_ordine"
	luo_calcola_doc.is_colonna_prog_riga = "prog_riga_ord_ven"
	luo_calcola_doc.id_precisione = id_precisione_valuta
	
	li_ret = luo_calcola_doc.uof_get_imponibile_riga(	"dw", al_row, dw_det_ord_ven_lista, "", 0, 0, ld_null[], 0, "", ld_imponibile_riga, ld_imponibile_riga_valuta, ls_errore)
	destroy luo_calcola_doc
	
	if li_ret < 0 then
		return
	end if
	
	st_margine.text += "    -    Margine = " + string(ld_imponibile_riga - ld_costo_acquisto * ld_quantita, "#####0.0000")
	
else
	st_margine.text += "    -    margine = 0.0000"
end if



return
end event

event ue_elimina_commesse();boolean lb_cancella_produzione=false, lb_cancella_commesse_riferite=false

string ls_messaggio,ls_flag_tipo_avanzamento, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_cod_prodotto, &
		ls_cod_prodotto_raggruppato, ls_mess_comm_eliminate
		
long 	ll_anno_commessa, ll_num_commessa, ll_anno_ord, ll_num_ord, ll_riga_ord,ll_righe, ll_i, &
	  	ll_num_riga_appartenenza, ll_prog_riga_ord_ven, ll_anno_comm_riferita, ll_num_comm_riferita, &
		ll_prog_riga_ord_rif, ll_ret


uo_produzione luo_prod


ll_anno_commessa = getitemnumber(getrow(), "anno_commessa")
ll_num_commessa = getitemnumber(getrow(), "num_commessa")

ll_anno_ord = getitemnumber(getrow(), "anno_registrazione")

ll_num_ord = getitemnumber(getrow(),"num_registrazione")

ll_riga_ord = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"prog_riga_ord_ven")
ll_prog_riga_ord_ven = ll_riga_ord

lb_cancella_commesse_riferite = true

lb_cancella_produzione = true

setpointer(hourglass!)

luo_prod = create uo_produzione
ll_ret = luo_prod.uof_elimina_commessa(ll_anno_ord, ll_num_ord, ll_prog_riga_ord_ven, lb_cancella_commesse_riferite, lb_cancella_produzione, ref ls_messaggio)
if ll_ret < 0 then
	rollback;
	g_mb.error( ls_messaggio )
	setpointer(arrow!)
	destroy luo_prod
	return
end if
// Se va tutto bene allora mi passo il messaggio che elenca le commesse eliminate.
ls_mess_comm_eliminate = ls_messaggio

setpointer(arrow!)

g_mb.messagebox("Sep","COMMESSA NR." + string(ll_anno_commessa) + "/" + string(ll_num_commessa) + " CANCELLATA !!!~r~n" + ls_mess_comm_eliminate)

destroy luo_prod

il_riga_selezionata = dw_det_ord_ven_lista.getrow()
parent.triggerevent("pc_retrieve")
dw_det_ord_ven_lista.postevent("ue_posiziona_riga")
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	datawindow ld_datawindow
	commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
	string ls_cod_tipo_det_ven, ls_cod_tipo_ord_ven, ls_flag_tipo_det_ven, ls_modify, ls_null, ls_cod_agente_1, ls_cod_agente_2, ls_cod_cliente, &
	ls_cod_iva
	long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven
	double ld_quan_proposta
	datetime ldt_data_consegna, ldt_data_esenzione_iva, ldt_data_registrazione

	setnull(ls_null)
	
	ll_anno_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "num_registrazione")
	ldt_data_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
	ls_cod_cliente = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
	ldt_data_consegna = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_consegna")

	// stefano 04/05/2010: ticket 113: Ricalcolo il progressivo riga
	// il codice è stato copiato da parent.pc_new
	string ls_cod_prodotto
	long ll_max, ll_getrow, ll_max_riga,ll_cont
	boolean lb_riga_configurata
	
	if dw_det_ord_ven_lista.getrow() > 0 then
		ls_cod_prodotto = dw_det_ord_ven_lista.getitemstring(dw_det_ord_ven_lista.getrow(), "cod_prodotto")
		if not isnull(ls_cod_prodotto) then
			ll_cont = 0
			select count(*)
			into   :ll_cont
			from   tab_flags_configuratore
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_modello = :ls_cod_prodotto;
			if ll_cont > 0 then
				lb_riga_configurata = true
			end if
		end if
	end if

	if (dw_det_ord_ven_lista.getrow() = dw_det_ord_ven_lista.rowcount() or dw_det_ord_ven_lista.getrow() < 1) and not(lb_riga_configurata) then
		il_num_riga = 0
		il_riga_riferimento = 0
	else
		ll_getrow = dw_det_ord_ven_lista.getitemnumber(dw_det_ord_ven_lista.getrow(),"prog_riga_ord_ven")
		ll_max = ceiling((ll_getrow + 1) / 10) * 10
		ll_max_riga = 0
		select max(prog_riga_ord_ven)
		into   :ll_max_riga
		from   det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven >= :ll_getrow and
				 prog_riga_ord_ven < :ll_max;
		
		if ll_max_riga = 0 or isnull(ll_max_riga) then
			il_num_riga = 0
			il_riga_riferimento = 0
		else
			il_num_riga = ll_max_riga + 1
			il_riga_riferimento = ll_getrow
		end if
	end if
	
	// Imposto il focus sulla riga per il TAB
	this.setfocus()
	// ----
	
	if il_num_riga = 0 then
		select max(det_ord_ven.prog_riga_ord_ven)
		into   :ll_prog_riga_ord_ven
		from   det_ord_ven
		where  det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				 det_ord_ven.anno_registrazione = :ll_anno_registrazione and
				 det_ord_ven.num_registrazione = :ll_num_registrazione;
	
		if isnull(ll_prog_riga_ord_ven) then
			dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "prog_riga_ord_ven", 10)
		else
			dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "prog_riga_ord_ven", ll_prog_riga_ord_ven + 10)
		end if
	else
		dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "prog_riga_ord_ven", il_num_riga)
		dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "num_riga_appartenenza", il_riga_riferimento)
	end if

   ls_cod_tipo_ord_ven = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_ord_ven")

   select tab_tipi_ord_ven.cod_tipo_det_ven
   into   :ls_cod_tipo_det_ven
   from   tab_tipi_ord_ven
   where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
   
   if sqlca.sqlcode = 0 then
      dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "cod_tipo_det_ven", ls_cod_tipo_det_ven)
      if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
         select tab_tipi_det_ven.cod_iva  
         into   :ls_cod_iva  
         from   tab_tipi_det_ven  
         where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
                tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
         end if
      end if

   else
      ls_cod_tipo_det_ven = ls_null
   end if
	
	select quan_default
	  into :ld_quan_proposta
	  from tab_tipi_det_ven
	 where cod_azienda = :s_cs_xx.cod_azienda 
	   and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati da tabella tab_tipi_ord_ven " + sqlca.sqlerrtext)
		return 0
	end if
	
	if isnull(ld_quan_proposta) then ld_quan_proposta = 0

   dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "data_consegna", ldt_data_consegna)
   
   if not isnull(ls_cod_cliente) then
      select cod_iva,
             data_esenzione_iva
      into   :ls_cod_iva,
             :ldt_data_esenzione_iva
      from   anag_clienti
      where  cod_azienda = :s_cs_xx.cod_azienda and 
             cod_cliente = :ls_cod_cliente;
   end if
   if sqlca.sqlcode = 0 then
      if ls_cod_iva <> "" and not isnull(ls_cod_iva) and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
         this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
      end if
   end if
   
	ls_modify = "cod_tipo_det_ven.protect='0'~t"
	dw_det_ord_ven_det_1.modify(ls_modify)
	ls_modify = "cod_tipo_det_ven.background.color='16777215'~t"
	dw_det_ord_ven_det_1.modify(ls_modify)

	if not isnull(ls_cod_tipo_det_ven) then
		dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "cod_tipo_det_ven", ls_cod_tipo_det_ven)
		ld_datawindow = dw_det_ord_ven_det_1
		ls_cod_agente_1 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
		ls_cod_agente_2 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
		wf_tipo_det_ven(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_ven, ls_cod_agente_1, ls_cod_agente_2)
	else
		ls_modify = "cod_prodotto.protect='1'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "cod_prodotto.background.color='12632256'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "des_prodotto.protect='1'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "des_prodotto.background.color='12632256'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "cod_misura.protect='1'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "cod_misura.background.color='12632256'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "fat_conversione_ven.protect='1'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "fat_conversione_ven.background.color='12632256'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "quan_ordine.protect='1'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "quan_ordine.background.color='12632256'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "prezzo_vendita.protect='1'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "prezzo_vendita.background.color='12632256'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "sconto_1.protect='1'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "sconto_1.background.color='12632256'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "sconto_2.protect='1'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "sconto_2.background.color='12632256'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "cod_iva.protect='1'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "cod_iva.background.color='12632256'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "provvigione_1.protect='1'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "provvigione_1.background.color='12632256'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "provvigione_2.protect='1'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "provvigione_2.background.color='12632256'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "data_consegna.protect='1'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "data_consegna.background.color='12632256'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "cod_centro_costo.protect='1'~t"
		dw_det_ord_ven_det_1.modify(ls_modify)
		ls_modify = "cod_centro_costo.background.color='12632256'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
	end if

	cb_sconti.enabled = true
	cb_c_industriale.enabled = false
	cb_blocca.enabled = false
	cb_sblocca.enabled = false
	cb_assegnazione.enabled = false
	cb_azzera.enabled = false
	cb_corrispondenze.enabled = false
	cb_configuratore.enabled = false
	cb_cancella_commessa.enabled = false	
	cb_ricalcola_configurazione.enabled = false
	
	dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_lista.getrow(), "flag_doc_suc_det", "I")
	dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_lista.getrow(), "flag_st_note_det", "I")	
	
	dw_trasferimenti.enabled = true
	
	cb_unisci_doc.enabled = false
	
end if
dw_det_ord_ven_det_1.postevent("ue_abilita_pulsanti")
this.Object.dim_x.Background.Color = "0~tIf(flag_dimensioni = 'S',rgb(255,255,255), rgb(192,192,192) )"
this.Object.dim_y.Background.Color = "0~tIf(flag_dimensioni = 'S',rgb(255,255,255), rgb(192,192,192) )"


end event

event pcd_view;call super::pcd_view;if i_extendmode then
   	cb_sconti.enabled = false
	cb_blocca.enabled = true
	cb_sblocca.enabled = true

	if this.getrow() > 0 then
		if this.getitemnumber(this.getrow(), "anno_registrazione") > 0 then
			cb_c_industriale.enabled=true
			cb_corrispondenze.enabled = true
		end if
	else
		cb_c_industriale.enabled=false
		cb_corrispondenze.enabled = false
	end if

	cb_assegnazione.enabled = true
	cb_azzera.enabled = true
	cb_configuratore.enabled = true
	cb_cancella_commessa.enabled = true
	cb_ricalcola_configurazione.enabled = true
	
	dw_det_ord_ven_det_1.postevent("ue_abilita_pulsanti")
	
	dw_trasferimenti.enabled = true
	
	cb_unisci_doc.enabled = true

end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	datawindow ld_datawindow
	commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
	string ls_cod_cliente, ls_cod_iva, ls_cod_tipo_det_ven, ls_modify, ls_cod_prodotto, ls_cod_misura_mag, ls_cod_misura_ven
	datetime ldt_data_registrazione

	ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")
	ldt_data_registrazione = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_registrazione")

	setnull(idt_data_partenza)

	if this.getrow() > 0 then
		ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")		
		ls_cod_misura_ven = this.getitemstring(this.getrow(),"cod_misura")
		
		select 	cod_misura_mag
		into   	:ls_cod_misura_mag
		from   	anag_prodotti
		where  	cod_azienda = :s_cs_xx.cod_azienda and 
					cod_prodotto = :ls_cod_prodotto;

		uo_1.uof_aggiorna(ls_cod_prodotto)
		
		event trigger ue_costo_ultimo(this.getrow())

		f_PO_LoadDDDW_DW(dw_det_ord_ven_det_1,"cod_versione",sqlca,&
		                   "distinta_padri","cod_versione","des_versione",&
				             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + getitemstring(getrow(),"cod_prodotto") + "'")

		f_po_loaddddw_dw(dw_det_ord_ven_det_1,  "cod_iva",  sqlca, &
					  "tab_ive", &
					  "cod_iva", &
					  "des_iva", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

		if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
			len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
			len(trim(ls_cod_misura_mag)) <> 0 then
			dw_det_ord_ven_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione_ven"))) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
		else
			dw_det_ord_ven_det_1.modify("st_fattore_conv.text=''")		
		end if
		
		
		if getitemstring(getrow(),"flag_riga_sospesa") = "S" then
			dw_det_ord_ven_det_1.object.cod_tipo_causa_sospensione.visible = 1
		else
			dw_det_ord_ven_det_1.object.cod_tipo_causa_sospensione.visible = 0
		end if
		
		dw_documenti.uof_retrieve_blob(getrow())
		
	end if
 
	postevent("ue_riallinea_riga")
	
   if ib_stato_modifica or ib_stato_nuovo then
      ls_cod_tipo_det_ven = dw_det_ord_ven_det_1.getitemstring(dw_det_ord_ven_det_1.getrow(), "cod_tipo_det_ven")
      ld_datawindow       = dw_det_ord_ven_det_1
      wf_tipo_det_ven_det(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_ven)
      ld_datawindow       = dw_det_ord_ven_lista
      wf_tipo_det_ven_lista(ld_datawindow, ls_cod_tipo_det_ven)
		if getrow() > 0 then wf_proteggi_colonne(getitemnumber(getrow(),"quan_evasa"), getitemnumber(getrow(),"quan_in_evasione"), getitemnumber(getrow(),"anno_commessa"))
		
		uo_gestione_conversioni iuo_oggetto
		iuo_oggetto = create uo_gestione_conversioni
		ld_datawindow  = dw_det_ord_ven_det_1
		iuo_oggetto.uof_blocca_colonne(ld_datawindow, "ord_ven")
		destroy iuo_oggetto
	end if

   if ib_stato_nuovo then
      ls_modify = "cod_tipo_det_ven.protect='0~tif(isrownew(),0,1)'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
      ls_modify = "cod_tipo_det_ven.background.color='16777215~tif(isrownew(),16777215,12632256)'~t"
      dw_det_ord_ven_det_1.modify(ls_modify)
   end if
	
	// ---- Visualizzazione stato produzione - Michele 07/01/2004 ----
	
	if getrow() > 0 then
	
		string ls_messaggio
		
		long 	 ll_anno, ll_num, ll_riga, ll_return
		
		ll_anno = getitemnumber(getrow(),"anno_registrazione")
		
		ll_num = getitemnumber(getrow(),"num_registrazione")
		
		ll_riga = getitemnumber(getrow(),"prog_riga_ord_ven")
		
		if not isvalid(iuo_produzione) then
			iuo_produzione = create uo_produzione
		end if
		
		// stefanop 28/06/2012: se non ci sono righe azzero lo stato del semaforo
		if isnull(ll_riga) or ll_riga < 1 then
			ll_return = -1
		else
			ll_return = iuo_produzione.uof_stato_prod_det_ord_ven(ll_anno,ll_num,ll_riga,ls_messaggio)
		end if
		// -----
		
		choose case ll_return
			case 0
				//st_stato_produzione.text = "Niente"
				//st_stato_produzione.backcolor = rgb(255,0,0)
				//st_stato_produzione.border = true
				uo_stato_prod.uof_set_color(2)
				cb_fasi.enabled = true
			case 1
				//st_stato_produzione.text = "Tutto"
				//st_stato_produzione.backcolor = rgb(0,255,0)
				//st_stato_produzione.border = true
				uo_stato_prod.uof_set_color(3)
				cb_fasi.enabled = true
			case 2
				//st_stato_produzione.text = "Parziale"
				//st_stato_produzione.backcolor = rgb(255,255,0)
				//st_stato_produzione.border = true
				uo_stato_prod.uof_set_color(4)
				cb_fasi.enabled = true
			case else
				//st_stato_produzione.text = ""
				//st_stato_produzione.backcolor = 12632256
				//st_stato_produzione.border = false
				uo_stato_prod.uof_set_color(1)
				cb_fasi.enabled = false
		end choose
		
	else
		
		//st_stato_produzione.text = ""
		//st_stato_produzione.backcolor = 12632256
		//st_stato_produzione.border = false
		uo_stato_prod.uof_set_color(1)
		cb_fasi.enabled = false
		
	end if
	
	// ---------------------------------------------------------------
	
	dw_det_ord_ven_det_1.postevent("ue_abilita_pulsanti")
	dw_det_ord_ven_det_1.postevent("ue_prezzo_bloccato")
	
	if getrow() > 0 then
		//gestione abilitazione pulsante ologrammi
		cb_ologrammi.enabled = wf_controlla_ologrammi(ll_anno, ll_num, ll_riga)
	else
		cb_ologrammi.enabled = false
	end if
	
end if

end event

event updatestart;call super::updatestart;if i_extendmode then
	integer 	li_risposta
	string 	ls_cod_pagamento, ls_cod_valuta, ls_tabella, ls_quan_documento, ls_cod_tipo_ord_ven, ls_flag_tipo_bcl, & 
			 	ls_tot_val_documento, ls_nome_prog, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, &
			 	ls_cod_prodotto_old, ls_cod_prodotto, ls_aspetto_beni, ls_des_imballo,ls_flag_stampata_bcl, &
			 	ls_messaggio, ls_flag_inviato,ls_cod_prodotto_raggruppato, ls_flag_prezzo_bloccato, ls_errore
	
	long 		ll_anno_registrazione, ll_num_registrazione, ll_i, ll_prog_riga_ord_ven, ll_i2, &
		  		ll_i3, ll_controllo, ll_i4, ll_errore
 
	 dec{4} 	ld_sconto_testata, ld_quan_ordine_old, ld_quan_ordine, ld_quan_raggruppo, ld_imponibile_ordine, &
				ld_valore_riga
	
	uo_calendario_prod_new luo_cal_prod
	uo_produzione luo_prod

	
	// 16/02/2004 Michela: se l'ordine è stato inviato devo avvertire per le modifiche
   ls_flag_inviato = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_inviato")
	If ls_flag_inviato = "S" then
		g_mb.messagebox("APICE", "Attenzione: questo ordine è già stato esportato. Le modifiche dovranno essere notificate al ricevente manualmente.")
	end if
	// fine modifiche	
	
	
	ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	ld_sconto_testata = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "sconto")
	ls_cod_pagamento = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_pagamento")
	ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
	ll_prog_riga_ord_ven = getitemnumber(getrow(),"prog_riga_ord_ven")
	ls_aspetto_beni = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "aspetto_beni")
	ls_flag_stampata_bcl = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "flag_stampata_bcl")
		
	if ls_flag_stampata_bcl = "S" then
		li_risposta = g_mb.messagebox("Apice", "Devono essere ristampati per la produzione?", Question!, YesNo!, 2)
		
		if li_risposta = 1 then
			update tes_ord_ven
			set    flag_stampata_bcl ='N'
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_registrazione=:ll_anno_registrazione
			and    num_registrazione=:ll_num_registrazione;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice","Errore sul DB in fase di aggiornamento flag stampa bcl: " + sqlca.sqlerrtext,stopsign!)
				return 1
			end if
			
		end if	
		
	end if

	ls_tabella = "tes_ord_ven"
	ls_quan_documento = "quan_ordine"
	ls_tot_val_documento = "tot_val_ordine"
	
	dw_det_ord_ven_lista.change_dw_current()
	cb_sconti.enabled = false
	cb_prezzo.enabled=false
	cb_blocca.enabled = true
	cb_sblocca.enabled = true

	for ll_i2 = 1 to this.rowcount()
		
		ls_flag_prezzo_bloccato = getitemstring(ll_i2, "flag_prezzo_bloccato") 
		if ls_flag_prezzo_bloccato <> getitemstring(ll_i2, "flag_prezzo_bloccato", primary!, true) and ( isnull( getitemnumber(ll_i2, "num_riga_appartenenza")) or  getitemnumber(ll_i2, "num_riga_appartenenza") = 0 )  then
			for ll_i4 = 1 to rowcount()
				if getitemnumber(ll_i4, "num_riga_appartenenza") = getitemnumber(ll_i2, "prog_riga_ord_ven") then
					setitem(ll_i4, "flag_prezzo_bloccato", ls_flag_prezzo_bloccato)
				end if
			next
			g_mb.warning("PREZZO BLOCCATO aggiornato nelle righe di appartenenza optionals/addizionali")		
		end if

		
		ls_cod_prodotto = this.getitemstring(ll_i2, "cod_prodotto")
		select des_imballo
		 into  :ls_des_imballo
		 from  tab_imballi
		 where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_imballo = (select cod_imballo 
				 					 from	  anag_prodotti
									 where  cod_azienda = :s_cs_xx.cod_azienda and
									 		  cod_prodotto = :ls_cod_prodotto);

		if sqlca.sqlcode = -1 then
			g_mb.error("Errore durante l'Estrazione Descrizione Imballo: "+sqlca.sqlerrtext)
			return 1
		end if

		if (pos(ls_aspetto_beni, ls_des_imballo) = 0 or isnull(ls_aspetto_beni) or ls_aspetto_beni = "") and &
			ls_des_imballo <> "" and not isnull(ls_des_imballo) then
			if len(ls_aspetto_beni) > 0 then
				ls_aspetto_beni = ls_aspetto_beni + " - " + ls_des_imballo
			else
				ls_aspetto_beni = ls_des_imballo
			end if
		end if
		
		ls_cod_tipo_det_ven = this.getitemstring(ll_i2, "cod_tipo_det_ven")

		select flag_tipo_det_ven 
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		if sqlca.sqlcode <> 0  then
			g_mb.error("Errore in fase di lettura tipi dettaglio: " + sqlca.sqlerrtext)
			return 1
		end if

   		if ls_flag_tipo_det_ven = "M" then
			ll_prog_riga_ord_ven = this.getitemnumber(ll_i2, "prog_riga_ord_ven")

			select prog_riga_ord_ven
			into :ll_controllo
			from det_ord_ven
			where cod_azienda = :s_cs_xx.cod_azienda and 
				   anno_registrazione = :ll_anno_registrazione and 
			 		num_registrazione = :ll_num_registrazione and 
			 		prog_riga_ord_ven = :ll_prog_riga_ord_ven;
			if sqlca.sqlcode = 0 then			// ripristino la quantità del vecchio prodotto
				ls_cod_prodotto_old = this.getitemstring(ll_i2, "cod_prodotto", primary!, true)
				ld_quan_ordine_old = this.getitemnumber(ll_i2, "quan_ordine", primary!, true)

				update anag_prodotti  
				   set quan_impegnata = quan_impegnata - :ld_quan_ordine_old  
			    where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
			          anag_prodotti.cod_prodotto = :ls_cod_prodotto_old;
			   if sqlca.sqlcode = -1 then
			      g_mb.error("Errore in aggiornamento q.impegnata: "+sqlca.sqlerrtext)
		   	   return 1
			   end if
				
				// enme 08/1/2006 gestione prodotto raggruppato
				setnull(ls_cod_prodotto_raggruppato)
				
				select cod_prodotto_raggruppato
				into   :ls_cod_prodotto_raggruppato
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto_old;
						 
				if not isnull(ls_cod_prodotto_raggruppato) then
					
					ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto_old, ld_quan_ordine_old, "M")
					
					update anag_prodotti  
						set quan_impegnata = quan_impegnata - :ld_quan_raggruppo  
					 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
							 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
					if sqlca.sqlcode = -1 then
						g_mb.error("Errore in aggiornamento q.impegnata codice raggruppo: "+sqlca.sqlerrtext)
						return 1
					end if
				end if
				
			end if
			// aggiorno quantità impegnata su nuovo prodotto
			ls_cod_prodotto = this.getitemstring(ll_i2, "cod_prodotto")
			ld_quan_ordine = this.getitemnumber(ll_i2, "quan_ordine")

			update anag_prodotti  
			   set quan_impegnata = quan_impegnata + :ld_quan_ordine  
		    where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
		          anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		   if sqlca.sqlcode = -1 then
		      g_mb.error("Errore in aggiornamento q.impegnata nuovo codice: "+sqlca.sqlerrtext)
	   	   return 1
		   end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
				
				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordine, "M")
				
				update anag_prodotti  
					set quan_impegnata = quan_impegnata + :ld_quan_raggruppo  
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
				if sqlca.sqlcode = -1 then
					g_mb.error("Errore in aggiornamento q.impegnata nuovo codice raggruppo: "+sqlca.sqlerrtext)
					return 1
				end if
			end if
		end if
		
		// calcolo imponibile riga per controllo fido
/*		
		ld_valore_riga = f_calcola_riga ( getitemstring(ll_i2,"cod_tipo_det_ven"), &
											  getitemnumber(ll_i2,"quan_ordine"), &
											  getitemnumber(ll_i2,"prezzo_vendita"), &
											  i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta"), &
											  i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "cambio_ven"), &
											  getitemnumber(ll_i2,"sconto_1"), &
											  getitemnumber(ll_i2,"sconto_2"), &
											  getitemnumber(ll_i2,"sconto_3"), &
											  getitemnumber(ll_i2,"sconto_4"), &
											  getitemnumber(ll_i2,"sconto_5"), &
											  getitemnumber(ll_i2,"sconto_6"), &
											  getitemnumber(ll_i2,"sconto_7"), &
											  getitemnumber(ll_i2,"sconto_8"), &
											  getitemnumber(ll_i2,"sconto_9"), &
											  getitemnumber(ll_i2,"sconto_10") )
					*/						  
		if isnull( ld_valore_riga ) then ld_valore_riga = 0
		
		ld_imponibile_ordine += ld_valore_riga
	next

	for ll_i3 = 1 to deletedcount()
		ls_cod_tipo_det_ven = this.getitemstring(ll_i3, "cod_tipo_det_ven", delete!, true)

	   select flag_tipo_det_ven
	   into   :ls_flag_tipo_det_ven
	   from   tab_tipi_det_ven
	   where  cod_azienda = :s_cs_xx.cod_azienda and 
	          cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	   if sqlca.sqlcode <> 0 then
	      g_mb.error("Errore in fase di lettura tipi dettaglio:" + sqlca.sqlerrtext)
   	   return 1
	   end if

   		if ls_flag_tipo_det_ven = "M" then
			ls_cod_prodotto = this.getitemstring(ll_i3, "cod_prodotto", delete!, true)
			ld_quan_ordine = this.getitemnumber(ll_i3, "quan_ordine", delete!, true)
	
			update anag_prodotti  
			   set quan_impegnata = quan_impegnata - :ld_quan_ordine  
		    where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
		          anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		   if sqlca.sqlcode = -1 then
		      g_mb.error("Errore in aggiornamento q.impegnata: "+sqlca.sqlerrtext)
				return 1
		   end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then

				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordine, "M")

				update anag_prodotti  
					set quan_impegnata = quan_impegnata - :ld_quan_raggruppo  
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
				if sqlca.sqlcode = -1 then
					g_mb.error("Errore in aggiornamento q.impegnata codice raggruppo: "+sqlca.sqlerrtext)
					return 1
				end if
			end if
		end if
	next

	update tes_ord_ven
	set	 aspetto_beni = :ls_aspetto_beni
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
			
	if sqlca.sqlcode = -1 then
		g_mb.error("Errore durante l'Aggiornamento Imballo Ordine: "+sqlca.sqlerrtext)
		return 1
	end if

	ls_tabella = "det_ord_ven_stat"
	ls_nome_prog = "prog_riga_ord_ven"
	
   for ll_i = 1 to this.deletedcount()
		ll_prog_riga_ord_ven = this.getitemnumber(ll_i, "prog_riga_ord_ven", delete!, true)		
     	if f_elimina_det_stat(ls_tabella, ll_anno_registrazione, ll_num_registrazione, ls_nome_prog, ll_prog_riga_ord_ven) = -1 then
			return 1
		end if

		delete varianti_det_ord_ven_prod
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione
		and    prog_riga_ord_ven =: ll_prog_riga_ord_ven;

		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione varianti produzione riga ordine: "+sqlca.sqlerrtext)
			return 1
		end if
		
		delete varianti_det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione
		and    prog_riga_ord_ven =: ll_prog_riga_ord_ven;

		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione varianti riga ordine: "+sqlca.sqlerrtext)
			return 1
		end if
		
		delete tab_optionals_comp_det
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione
		and    prog_riga_ord_ven =: ll_prog_riga_ord_ven;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione tab_optionals_comp_det: "+sqlca.sqlerrtext)
			return 1
		end if
		
		delete tab_mp_non_ric_comp_det
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione
		and    prog_riga_ord_ven =: ll_prog_riga_ord_ven;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione tabella MP automatiche riga ordine: "+sqlca.sqlerrtext)
			return 1
		end if

		delete comp_det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione
		and    prog_riga_ord_ven =: ll_prog_riga_ord_ven;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione dettagli complementari riga ordine: "+sqlca.sqlerrtext)
			return 1
		end if
		
		delete det_ord_ven_note		
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione
		and    prog_riga_ord_ven =: ll_prog_riga_ord_ven;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione tabella note riga ordine: "+sqlca.sqlerrtext)
			return 1
		end if
		
		delete det_ord_ven_corrispondenze
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione
		and    prog_riga_ord_ven  =: ll_prog_riga_ord_ven;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione tabella corrispondenze riga ordine: "+sqlca.sqlerrtext)
			return 1
		end if
		
		delete det_ord_ven_conf_variabili
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione
		and    prog_riga_ord_ven  =: ll_prog_riga_ord_ven;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione variabili configuratore: "+sqlca.sqlerrtext)
			return 1
		end if
		
		//---------- Michele 28/01/2004 Cancellazione dati produzione ------------
		
		luo_prod = create uo_produzione
		
		if luo_prod.uof_elimina_prod_det(ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,ls_messaggio) <> 0 then
			g_mb.error(ls_messaggio)
			destroy luo_prod
			return 1
		end if
		
		destroy luo_prod
		
		//------------------------------------------------------------------------
		
		//pulizia tabella det_ord_ven_prod e disimpegno del calendario produzione -----------------------------------------
		luo_cal_prod = create uo_calendario_prod_new
		li_risposta = luo_cal_prod.uof_disimpegna_calendario(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_messaggio)
		destroy luo_cal_prod;
		
		if li_risposta<0 then
			g_mb.error(ls_messaggio)
			return 1
		end if
		
		delete det_ord_ven_prod
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione
		and    prog_riga_ord_ven  =: ll_prog_riga_ord_ven;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante cancellazione dettaglio ordine prod: "+sqlca.sqlerrtext)
			return 1
		end if
		//--------------------------------------------------------------------------------------------------------------------------------------------
		
		delete from distinte_taglio_calcolate
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione  = :ll_num_registrazione and
				 prog_riga_ord_ven  = :ll_prog_riga_ord_ven;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in ancellazione distinte taglio calcolate: "+sqlca.sqlerrtext)
			return 1
		end if
		
		//elimino eventuali ologrammi
		luo_prod = create uo_produzione
		ll_errore = luo_prod.uof_elimina_ologrammi(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_messaggio)
		destroy luo_prod
		
		if ll_errore < 0 then
			//in fs_messaggio l'errore
			g_mb.error("Apice",ls_messaggio)
			return 1
		elseif ll_errore=1 then
			//operazione annullata
			g_mb.warning("Apice",ls_messaggio)
			return 1
		end if
		
		//pulizia tabella evasione da dati eventuali per evitare errore di FK
		delete from evas_ord_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione and
					num_registrazione  = :ll_num_registrazione and
					prog_riga_ord_ven  =: ll_prog_riga_ord_ven;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante pulizia tabella evas_ord_ven dalla riga ordine: "+sqlca.sqlerrtext)
			return 1
		end if
		
		
		
		//Donato 28/01/2013 ----------------------------------------------------------------------
		//Beatrice ha chiesto di mettere a NULL il collegamento con la riga di fattura proforma
		//quando cancello una riga di ordine di vendita
		if guo_functions.uof_setnull_fatt_proforma(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_messaggio) < 0 then
			g_mb.error("Apice",ls_messaggio)
			return 1
		end if
		//----------------------------------------------------------------------------------------------
		
		
   next
end if


/* ------------------ calcolo dell'esposizione cliente ----------------------------
donato 27/01/2009*/

uo_fido_cliente l_uo_fido_cliente
long ll_return, ll_index, ll_rows
string ls_cod_cliente
dec{4} ld_valore_ordine_origine

l_uo_fido_cliente = create uo_fido_cliente

ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")

select 	sum(imponibile_iva)
into   	:ld_valore_ordine_origine
from   	det_ord_ven
where  	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
		
if isnull(ld_valore_ordine_origine) then ld_valore_ordine_origine = 0

l_uo_fido_cliente.id_importo = ld_imponibile_ordine - ld_valore_ordine_origine

l_uo_fido_cliente.is_flag_autorizza_sblocco = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "flag_aut_sblocco_fido")

ll_return = l_uo_fido_cliente.uof_check_cliente(ls_cod_cliente,datetime(today(),00:00:00),ls_messaggio)

destroy l_uo_fido_cliente

if ll_return = -1 then
	g_mb.error("Errore in verifica esposizione cliente.~n" + ls_messaggio)
	return 1
end if

//Donato 05-11-2008 Modifica per specifica cliente PTENDA_ gestione fidi

if ll_return = 2 then
	//blocca perchè non sono autorizzato
	return 1
end if
end event

event updateend;call super::updateend;//this.postevent("pcd_view")
long					ll_i, ll_prog_riga_ord_ven, ll_anno_registrazione, ll_num_registrazione, ll_riga_per_peso
string					ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_cod_prodotto, ls_messaggio, ls_cod_tipo_ord_ven, ls_flag_tipo_bcl
uo_funzioni_1		luo_funzioni_1
dec{5}				ld_peso_netto_unitario
integer				li_ret
uo_spese_trasporto		luo_spese_trasporto


ls_cod_tipo_ord_ven = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_ord_ven")

//ls_flag_tipo_bcl = A - B - C
select flag_tipo_bcl
into :ls_flag_tipo_bcl
from tab_tipi_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;


//gestione aggiornamento calendario reparti e calcolo peso netto unitario (se sfuso)
for ll_i = 1 to this.rowcount()

	ls_cod_tipo_det_ven = this.getitemstring(ll_i, "cod_tipo_det_ven")

	select flag_tipo_det_ven 
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if sqlca.sqlcode <> 0  then
		g_mb.error("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.")
		return 1
	end if

	ll_anno_registrazione = this.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = this.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_ord_ven = this.getitemnumber(ll_i, "prog_riga_ord_ven")
	ls_cod_prodotto = this.getitemstring(ll_i, "cod_prodotto")
	
	if isnull(ls_cod_prodotto) or ls_cod_prodotto="" then continue
	
	
	if ls_flag_tipo_bcl="B" then		//solo per lo sfuso
		//aggiorna det_ord_ven, det_ord_ven_prod e tab_cal_produzione
		
		if not isnull(idt_data_partenza) and year(date(idt_data_partenza))>1950 then
			//devo ricalcolare date e reparti per tutte le righe dell'ordine
			li_ret = wf_aggiorna_calendario(ll_anno_registrazione, ll_num_registrazione, 0, ls_messaggio)
		else
			//solo riga corrente
			li_ret = wf_aggiorna_calendario(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_messaggio)
		end if
		
		if li_ret<0 then
			g_mb.error("Attenzione", ls_messaggio)
			return 1
			
		elseif ls_messaggio<>"" and li_ret=1 then
			//mostro nel log ma vai avanti lo stesso
			dw_trasferimenti.reset()
			li_ret = dw_trasferimenti.insertrow(0)
			dw_trasferimenti.setitem(li_ret, "testo", ls_messaggio)
			
		end if
		//---------------------------------------------------------------------------------------------------------------------------------	
	end if
next

luo_spese_trasporto = create uo_spese_trasporto

for ll_i = 1 to this.rowcount()
	//donato 24/01/2012
	//calcolo peso netto unitario della riga (solo per ordini di sfuso), per gli altri questo viene fatto alla fine del configuratore
	//20/02/2014: inserito script per l'impostazione dei valori spesa trasporto sulle righe di sfuso
	//################################################
	
	if ls_flag_tipo_bcl="B" then		//solo per lo sfuso
	
		luo_funzioni_1 = create uo_funzioni_1
		
		ll_anno_registrazione = this.getitemnumber(ll_i, "anno_registrazione")
		ll_num_registrazione = this.getitemnumber(ll_i, "num_registrazione")
		ll_riga_per_peso = this.getitemnumber(ll_i, "prog_riga_ord_ven")
		
		ld_peso_netto_unitario  = 0
		li_ret = luo_funzioni_1.uof_calcola_peso_riga_ordine(ll_anno_registrazione, ll_num_registrazione, ll_riga_per_peso, ld_peso_netto_unitario, ls_messaggio)
		destroy luo_funzioni_1
		
		if li_ret<0 then
			rollback;
			g_mb.error("Attenzione", "Si è verificato un errore in fase di calcolo peso netto unitario della riga:" + ls_messaggio)
			return 1
		else
			//se ho ottenuto zero lascio il valore originale, altrimenti piazzo il valore e resetto itemstatus
			if ld_peso_netto_unitario>0 then
				
				update det_ord_ven
				set peso_netto=:ld_peso_netto_unitario
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							anno_registrazione=:ll_anno_registrazione and
							num_registrazione=:ll_num_registrazione and
							prog_riga_ord_ven=:ll_riga_per_peso;
				
				setitem(ll_i,"peso_netto", ld_peso_netto_unitario)
				setitemstatus(ll_i, "peso_netto", primary!, NotModified!)
			end if
		end if
		
		//--------------------------------------------
		// stefanop 19/02/2014
		li_ret = luo_spese_trasporto.uof_imposta_spese_ord_ven(ll_anno_registrazione, ll_num_registrazione, ll_riga_per_peso, false, ref ls_messaggio)
		if li_ret < 0 then
			rollback;
			g_mb.error("Attenzione", ls_messaggio)
			destroy luo_spese_trasporto;
			return 1
		end if
		
	end if
	//##############################################
next

destroy luo_spese_trasporto;

commit;


if isvalid(s_cs_xx.parametri.parametro_w_ord_ven)  then
	
	//se è stata cambiata la data partenza (ex consegna) allora aggiorna su tutte le righe ed in testata ordine
	//questo, come dopo l'uscita dal configuratore è delegato all'evento ue_cambia_data_partenza della tes_ord_ven
	//attivi questo solo per lo sfuso
	
	if not isnull(idt_data_partenza) and year(date(idt_data_partenza))>=1950 and ls_flag_tipo_bcl="B" then
		s_cs_xx.parametri.parametro_data_4 = idt_data_partenza
		setnull(idt_data_partenza)
		
		if ll_anno_registrazione>0 and ll_num_registrazione>0 then
			s_cs_xx.parametri.parametro_d_4_a[1] = ll_anno_registrazione
			s_cs_xx.parametri.parametro_d_4_a[2] = ll_num_registrazione
			s_cs_xx.parametri.parametro_w_ord_ven.postevent("ue_cambia_data_partenza")
		end if
		
	else
		//tutto come prima
		s_cs_xx.parametri.parametro_w_ord_ven.postevent("ue_calcola")
	end if
end if

return 0




end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if
next

end on

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_cliente, ls_rag_soc_1, ls_cod_prodotto, ls_cod_cat_mer
long ll_row, ll_anno_registrazione, ll_num_registrazione, ll_i

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")

is_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
id_cambio_ven_ds = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "cambio_ven")
select precisione
into   :id_precisione_valuta
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :is_cod_valuta;
 

ii_anno_ordine = ll_anno_registrazione
il_num_ordine = ll_num_registrazione

select rag_soc_1
into   :ls_rag_soc_1
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :ls_cod_cliente ;
		 
if sqlca.sqlcode = 0 then
	parent.title = "Ordine " + &
						string(ll_anno_registrazione,"####") + "/" + &
						string(ll_num_registrazione) + "  Cliente: " + ls_rag_soc_1
end if

setnull(idt_data_partenza)

ll_row = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)

if ll_row < 0 then
   pcca.error = c_fatal
	
elseif ll_row = 0 then
	
	// stefanop 28/06/2012: se non ci sono righe resetto lo stato del semaforo
	uo_stato_prod.uof_set_color(1)
	cb_fasi.enabled = false
	cb_ologrammi.enabled = false
	
else
	// EnMe 20/09/12 per specifica Optionals Configurabili
	for ll_i = 1 to ll_row
		ls_cod_prodotto = getitemstring(ll_i,"cod_prodotto")
		if not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then
			select cod_cat_mer
			into    :ls_cod_cat_mer
			from 	anag_prodotti
			where cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :ls_cod_prodotto;
			if ls_cod_cat_mer = is_cod_cat_mer_optional_conf then
				setitem(ll_i, "flag_dimensioni","S")
				setitemstatus(ll_i, "flag_dimensioni",primary!,NotModified!)
			end if
		end if
	next
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_ven, ls_modify, ls_cod_agente_1, ls_cod_agente_2


	dw_det_ord_ven_lista.object.cod_tipo_det_ven.protect = 1
	dw_det_ord_ven_lista.object.cod_tipo_det_ven.background.color='12632256'
	ls_cod_tipo_det_ven = dw_det_ord_ven_det_1.getitemstring(dw_det_ord_ven_det_1.getrow(), "cod_tipo_det_ven")
	
	ld_datawindow = dw_det_ord_ven_det_1
	ls_cod_agente_1 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
	ls_cod_agente_2 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
	wf_tipo_det_ven(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_ven, ls_cod_agente_1, ls_cod_agente_2)

	cb_sconti.enabled = true
	cb_c_industriale.enabled = false
	cb_blocca.enabled = false
	cb_sblocca.enabled = false
	cb_assegnazione.enabled = false
	cb_azzera.enabled = false
	cb_corrispondenze.enabled = false
	cb_configuratore.enabled = false
	cb_cancella_commessa.enabled = false
	cb_ricalcola_configurazione.enabled = false
	wf_proteggi_colonne(getitemnumber(getrow(),"quan_evasa"), getitemnumber(getrow(),"quan_in_evasione"), getitemnumber(getrow(),"anno_commessa"))
	
	uo_gestione_conversioni iuo_oggetto
	iuo_oggetto = create uo_gestione_conversioni
	ld_datawindow  = dw_det_ord_ven_det_1
	iuo_oggetto.uof_blocca_colonne(ld_datawindow, "ord_ven")
	iuo_oggetto.uof_visualizza_um(ld_datawindow, "ord_ven")
	destroy iuo_oggetto
	
	
	dw_trasferimenti.enabled = true
	
	cb_unisci_doc.enabled = false
	
end if
dw_det_ord_ven_det_1.postevent("ue_abilita_pulsanti")
dw_det_ord_ven_det_1.postevent("ue_prezzo_bloccato")
this.Object.dim_x.Background.Color = "0~tIf(flag_dimensioni = 'S',rgb(255,255,255), rgb(192,192,192) )"
this.Object.dim_y.Background.Color = "0~tIf(flag_dimensioni = 'S',rgb(255,255,255), rgb(192,192,192) )"

end event

event pcd_save;call super::pcd_save;if i_extendmode then
	long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_i1,ll_prog_riga_ord_ven
	string  ls_cod_tipo_analisi,ls_cod_prodotto,ls_cod_tipo_ord_ven,ls_cod_tipo_det_ven,ls_test
	integer li_risposta

	ll_i = i_parentdw.i_selectedrows[1]
	ll_i1 = getrow()

	ll_anno_registrazione = i_parentdw.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_ord_ven = getitemnumber(ll_i1, "prog_riga_ord_ven")
	ls_cod_tipo_det_ven = getitemstring(ll_i1, "cod_tipo_det_ven")
	ls_cod_tipo_ord_ven = i_parentdw.getitemstring(ll_i, "cod_tipo_ord_ven")
	
	SELECT cod_tipo_analisi
	INTO   :ls_cod_tipo_analisi  
	FROM   tab_tipi_ord_ven
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND    cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

	select cod_azienda
	into   :ls_test
	from 	 det_ord_ven_stat
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_registrazione
	and    num_registrazione=:ll_num_registrazione
	and    prog_riga_ord_ven=:ll_prog_riga_ord_ven;

	if sqlca.sqlcode=100 then
		li_risposta = f_crea_distribuzione(ls_cod_tipo_analisi,ls_cod_prodotto,ls_cod_tipo_det_ven,ll_num_registrazione,ll_anno_registrazione,ll_prog_riga_ord_ven,4)
		if li_risposta=-1 then
			g_mb.messagebox("Apice","Attenzione! Si è verificato un errore durante la creazione dei dettagli statistici.",exclamation!)
			pcca.error = c_fatal
		end if
	end if
	dw_det_ord_ven_det_1.postevent("ue_abilita_pulsanti")
end if
end event

event pcd_validaterow;call super::pcd_validaterow;if this.getitemstatus(this.getrow(), 0, primary!) = datamodified! then
	if this.getitemstring(this.getrow(), "flag_blocco") = "S" then
		g_mb.messagebox("Attenzione", "Ordine non modificabile! E' stato bloccato.", &
					  exclamation!, ok!)
		this.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return 1
	end if
end if
if this.getitemstatus(this.getrow(), 0, primary!) = New! or this.getitemstatus(this.getrow(), 0, primary!) = NewModified!  then
		// 16/02/2004 Michela: se l'ordine è stato inviato non posso creare nuove righe.
	If dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "flag_inviato") = "S" then
		g_mb.messagebox("APICE", "Attenzione: l'ordine risulta già esportato, quindi risulta impossibile creare nuove righe!", &
					  exclamation!, ok!)
		this.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return 1
	end if
	// fine modifiche
end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	uo_gruppi_sconti luo_gruppi_sconti
	uo_default_prodotto luo_default_prodotto
	datawindow ld_datawindow
	commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
	boolean lb_test
	string ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_colonna_sconto, ls_errore, &
			 ls_cod_tipo_listino_prodotto, ls_cod_cliente, ls_cod_valuta, &
			 ls_flag_tipo_det_ven, ls_modify, ls_null, ls_cod_agente_1, ls_des_prodotto, &
			 ls_cod_agente_2, ls_cod_tipo_det_ven, ls_flag_decimali, ls_messaggio, &
			 ls_cod_misura_mag, ls_nota_prodotto, ls_cod_versione, ls_flag_prezzo, ls_flag_blocco, &
			 ls_flag_prezzo_bloccato, ls_nota_testata, ls_stampa_piede, ls_nota_video
	double ld_fat_conversione, ld_quantita, ld_cambio_ven, ld_variazioni[], ld_num_confezioni, ld_num_pezzi, &
			 ld_ultimo_prezzo, ll_sconti[], ll_y, ll_maggiorazioni[], ld_provv_1, ld_provv_2, ld_gruppi_sconti[]
	dec{4} ld_dim_x, ld_dim_y, ld_pezzi_collo
	datetime ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva
	long ll_riga_origine, ll_i, ll_prog_riga_ord_ven ,ll_num_riga_appartenenza
	integer li_ret
	
	setnull(ls_null)
	lb_test = false
	ls_cod_tipo_listino_prodotto = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
	ldt_data_registrazione = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
	ls_cod_cliente = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
	ls_cod_valuta = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
	ld_cambio_ven = dw_det_ord_ven_lista.i_parentdw.getitemnumber(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
	ls_cod_agente_1 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
	ls_cod_agente_2 = dw_det_ord_ven_lista.i_parentdw.getitemstring(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
	ls_cod_tipo_det_ven = this.getitemstring(this.getrow(),"cod_tipo_det_ven")
	ls_cod_tipo_det_ven = this.getitemstring(this.getrow(),"cod_tipo_det_ven")
	ls_flag_prezzo_bloccato = this.getitemstring(this.getrow(),"flag_prezzo_bloccato")
	ld_dim_x = 0
	ld_dim_y = 0
	//EnMe 10/12/2012 Valorizzazione prezzi optional configurabili anche con opzioni da distinta
	s_cs_xx.listino_db.flag_calcola ="S"

   
	choose case i_colname
			
		case "prog_riga_ord_ven"
			ll_riga_origine = this.getrow()
			for ll_i = 1 to this.rowcount()
				if ll_i <> ll_riga_origine and &
					long(i_coltext) = this.getitemnumber(ll_i, "prog_riga_ord_ven") then
	            g_mb.messagebox("Attenzione", "Progressivo riga già utilizzato.", &
   	                     exclamation!, ok!)
               i_coltext = string(this.getitemnumber(ll_riga_origine, "prog_riga_ord_ven", primary!,true))
               this.setitem(ll_riga_origine, "prog_riga_ord_ven", double(i_coltext))
               this.settext(i_coltext)
               return 2
					pcca.error = c_fatal
				end if
			next
			
			
		case "cod_tipo_det_ven"
			ls_modify = "cod_prodotto.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_prodotto.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_versione.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_versione.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "des_prodotto.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "des_prodotto.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_misura.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_misura.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "fat_conversione_ven.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "fat_conversione_ven.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "quan_ordine.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "quan_ordine.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "prezzo_vendita.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "prezzo_vendita.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_1.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_1.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_2.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_2.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_iva.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_iva.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_1.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_1.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_2.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_2.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "data_consegna.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "data_consegna.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_centro_costo.protect='0'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
			ls_modify = "cod_centro_costo.background.color='16777215'~t"
			dw_det_ord_ven_det_1.modify(ls_modify)
	
			ld_datawindow = dw_det_ord_ven_det_1
			setnull(lc_prodotti_ricerca)
			f_tipo_dettaglio_ven(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext, ls_cod_agente_1, ls_cod_agente_2)
			ld_datawindow = dw_det_ord_ven_lista
			f_tipo_dettaglio_ven_lista(ld_datawindow, i_coltext)

			select tab_tipi_det_ven.cod_iva  
			into   :ls_cod_iva  
			from   tab_tipi_det_ven  
			where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_tipi_det_ven.cod_tipo_det_ven = :i_coltext;
			if sqlca.sqlcode = 0 then
				this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
			end if
			
			//------------------------------------------------------------------------------------------------
			//se il cliente è iva esente imposta l'iva in maniera corretta
			if not isnull(ls_cod_cliente) then
				select cod_iva,
						 data_esenzione_iva
				into   	:ls_cod_iva,
						 :ldt_data_esenzione_iva
				from   anag_clienti
				where  cod_azienda = :s_cs_xx.cod_azienda and 
						 cod_cliente = :ls_cod_cliente;
			end if

			if sqlca.sqlcode = 0 then
				if ls_cod_iva <> "" and not isnull(ls_cod_iva) and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
					this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
				end if
			end if
			//------------------------------------------------------------------------------------------------
			
			
			dw_det_ord_ven_det_1.setitem(dw_det_ord_ven_det_1.getrow(), "nota_dettaglio", ls_null)
			
			this.setitem(i_rownbr, "dim_x", 0)
			this.setitem(i_rownbr, "dim_y", 0)
	
	
		case "cod_prodotto"
			if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1

			select cod_misura_mag,
					 cod_misura_ven,
					 fat_conversione_ven,
					 flag_decimali,
					 cod_iva,
					 des_prodotto,
					 flag_blocco
			into   :ls_cod_misura_mag,
					 :ls_cod_misura,
					 :ld_fat_conversione,
					 :ls_flag_decimali,
					 :ls_cod_iva,
					 :ls_des_prodotto,
					 :ls_flag_blocco
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_prodotti.cod_prodotto = :i_coltext;
   
			if sqlca.sqlcode = 0 then
				
				//Donato 13/03/2012 gestione alert se gestisco da qui un prodotto che in realtà andrebbe configurato
				li_ret = wf_check_prodotto(i_coltext, ls_errore)
				if li_ret < 0 then
					g_mb.error("APICE",ls_errore)
				elseif li_ret = 1 then
					//dai solo alert
					g_mb.show("APICE",ls_errore)
				end if
				//----------------------------------------------------------------------------------------------------------------------
				if guo_functions.uof_get_note_fisse(ls_null, ls_null, i_coltext, "ORD_VEN", ls_null, ldt_data_registrazione, ls_nota_testata, ls_stampa_piede, ls_nota_video) < 0 then
					g_mb.error(ls_nota_testata)
				else
					if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
				end if
			
				this.setitem(i_rownbr, "cod_misura", ls_cod_misura)
				this.setitem(i_rownbr, "des_prodotto", ls_des_prodotto)

				dw_det_ord_ven_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
				dw_det_ord_ven_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")
				dw_det_ord_ven_det_1.modify("prezzo_ordine_t.text='Prezzo " + ls_cod_misura + ":'")
				dw_det_ord_ven_det_1.modify("quan_ordine_t.text='Quantità " + ls_cod_misura + ":'")
	
				if ld_fat_conversione <> 0 then
					this.setitem(i_rownbr, "fat_conversione_ven", ld_fat_conversione)
				else
					ld_fat_conversione = 1
					this.setitem(i_rownbr, "fat_conversione_ven", 1)
				end if
				
				if not isnull(ls_cod_iva) then
					this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
				end if
						
				//f_situazione_prodotto(i_coltext, uo_1)
				uo_1.uof_aggiorna(i_coltext)
				uo_1.uof_ultimo_prezzo(ls_cod_cliente, i_coltext)
				
				this.setitem(i_rownbr, "dim_x", 0)
				this.setitem(i_rownbr, "dim_y", 0)
				
				event post ue_costo_ultimo(i_rownbr)
				
			else
				dw_det_ord_ven_det_1.modify("st_prezzo.text='Prezzo:'")
				dw_det_ord_ven_det_1.modify("st_quantita.text='Quantità:'")
				dw_det_ord_ven_det_1.modify("prezzo_ordine_t.text='Prezzo:'")
				dw_det_ord_ven_det_1.modify("quan_ordine_t.text='Quantità:'")
				this.change_dw_current()
				guo_ricerca.uof_ricerca_prodotto(dw_det_ord_ven_lista, "cod_prodotto")
				
				return
			end if

			dw_det_ord_ven_lista.postevent("ue_postopen")

			if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_ord_ven_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(ld_fat_conversione)) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
			else
				dw_det_ord_ven_det_1.modify("st_fattore_conv.text=''")
			end if
			
			if not isnull(ls_cod_cliente) then
				select cod_iva,
						 data_esenzione_iva
				into   	:ls_cod_iva,
						 :ldt_data_esenzione_iva
				from   anag_clienti
				where  cod_azienda = :s_cs_xx.cod_azienda and 
						 cod_cliente = :ls_cod_cliente;
			end if

			if sqlca.sqlcode = 0 then
				if ls_cod_iva <> "" and &
					(ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
					this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
				end if
			end if

			ls_cod_prodotto = i_coltext
			ld_quantita = this.getitemnumber(i_rownbr, "quan_ordine")
			if not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
				ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")
			else
				ldt_data_consegna = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
			end if
			
			if ls_flag_decimali = "N" and &
				ld_quantita - int(ld_quantita) > 0 then
				ld_quantita = ceiling(ld_quantita)
				this.setitem(i_rownbr, "quan_ordine", ld_quantita)
			end if

			if mid(f_flag_controllo(),1,1) = "S" and ls_flag_prezzo_bloccato = "N" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			if f_leggi_nota_prodotto(ls_cod_tipo_det_ven, ls_cod_prodotto, ls_nota_prodotto) = 0 then
				dw_det_ord_ven_det_1.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
				dw_det_ord_ven_det_1.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if

			f_PO_LoadDDDW_DW(dw_det_ord_ven_det_1,"cod_versione",sqlca,&
						  "distinta_padri","cod_versione","des_versione",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + i_coltext + "'")
	
			luo_default_prodotto = CREATE uo_default_prodotto
			if luo_default_prodotto.uof_flag_gen_commessa(i_coltext) = 0 then
				setitem(getrow(),"cod_versione",luo_default_prodotto.is_cod_versione)
				setitem(getrow(),"flag_gen_commessa",luo_default_prodotto.is_flag_gen_commessa)
				setitem(getrow(),"flag_presso_cgibus",luo_default_prodotto.is_flag_presso_cg)
				setitem(getrow(),"flag_presso_ptenda",luo_default_prodotto.is_flag_presso_pt)
			end if
			destroy luo_default_prodotto
			
			if ls_cod_misura_mag <> ls_cod_misura then dw_det_ord_ven_det_1.postevent("ue_ricalcola_colonne")
			
			//verifica lo stato dei reparti e visualizza il semaforo
			//wf_semaforo_produzione(this, data, i_rownbr)
			if data<>"" then dw_det_ord_ven_lista.event post ue_cal_trasferimenti()
			
			
		case "quan_ordine"
			
			ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			select flag_decimali, cod_misura_mag
			into   :ls_flag_decimali, :ls_cod_misura_mag
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode = 0 then
				if ls_flag_decimali = "N" and &
					(double(i_coltext) - int(double(i_coltext))) > 0 then
					i_coltext = string(ceiling(double(i_coltext)))
					this.setitem(i_rownbr, "quan_ordine", double(i_coltext))
					this.settext(i_coltext)
					return 2
				end if
			end if
			
			ld_dim_x = getitemnumber(i_rownbr, "dim_x")
			ld_dim_y = getitemnumber(i_rownbr, "dim_y")

			ld_quantita = double(i_coltext)
			if mid(f_flag_controllo(),2,1) = "S" and ls_flag_prezzo_bloccato="N" then
				if not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
					ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")
				else
					ldt_data_consegna = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
				end if
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "quan_doc", &
			              												ld_quantita, &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_vendita"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
			
			event post ue_costo_ultimo(i_rownbr)
			
			
		case "data_consegna"
			
			ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			ld_quantita = this.getitemnumber(i_rownbr, "quan_ordine")
			ld_dim_x = getitemnumber(i_rownbr, "dim_x")
			ld_dim_y = getitemnumber(i_rownbr, "dim_y")
			if not isnull(i_coltext) then
				ldt_data_consegna = datetime(date(i_coltext))
			else
				ldt_data_consegna = dw_det_ord_ven_lista.i_parentdw.getitemdatetime(dw_det_ord_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
			end if
			if mid(f_flag_controllo(),4,1) = "S" and ls_flag_prezzo_bloccato="N" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			
		case "sconto_1", "sconto_2", "sconto_3", "sconto_4", "sconto_5", "sconto_6", "sconto_7", "sconto_8", "sconto_9", "sconto_10"
			if mid(f_flag_controllo(),9,1) = "S" and ls_flag_prezzo_bloccato="N" then
				ld_dim_x = getitemnumber(i_rownbr, "dim_x")
				ld_dim_y = getitemnumber(i_rownbr, "dim_y")
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.prezzo_listino = getitemnumber(getrow(),"prezzo_vendita")
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_ordine")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			event post ue_costo_ultimo(i_rownbr)
			
			
		case "data_consegna"
			if mid(f_flag_controllo(),4,1) = "S" and ls_flag_prezzo_bloccato="N" then
				ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
				ld_quantita = this.getitemnumber(i_rownbr, "quan_ordine")
				ld_dim_x = getitemnumber(i_rownbr, "dim_x")
				ld_dim_y = getitemnumber(i_rownbr, "dim_y")
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = datetime(date(i_coltext))
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			
		case "prezzo_vendita"
			if mid(f_flag_controllo(),3,1) = "S" and ls_flag_prezzo_bloccato ="N" then
				ld_dim_x = getitemnumber(i_rownbr, "dim_x")
				ld_dim_y = getitemnumber(i_rownbr, "dim_y")
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.prezzo_listino = double(i_coltext)
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_ordine")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_ord_ven_det_1, "ord_ven", "prezzo_doc", &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quan_ordine"), &
			              												double(i_coltext), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_ord_ven_det_1.getitemnumber(dw_det_ord_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
			event post ue_costo_ultimo(i_rownbr)
			
			

		case "flag_urgente"
			ll_prog_riga_ord_ven = getitemnumber(getrow(),"prog_riga_ord_ven")
			ll_num_riga_appartenenza = getitemnumber(getrow(),"num_riga_appartenenza")
			if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then
				setredraw(false)
				for ll_i = 1 to rowcount()
					if getitemnumber(ll_i,"num_riga_appartenenza") = ll_prog_riga_ord_ven then
						setitem(ll_i, "flag_urgente", i_coltext)
					end if
				next
				setredraw(true)
			else
				messagebox("APICE", "il FLAG URGENTE non è impostabile sulle righe di appartenenza")
				return 2
			end if
			
		case "dim_x"
			if ls_flag_prezzo_bloccato="N" then
				ld_dim_x = dec(data)
				ld_dim_y = getitemnumber(i_rownbr, "dim_y")
				ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
				ld_quantita = this.getitemnumber(i_rownbr, "quan_ordine")
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
		case "dim_y"
			if ls_flag_prezzo_bloccato="N" then
				ld_dim_x = getitemnumber(i_rownbr, "dim_x")
				ld_dim_y = dec(data)
				ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
				ld_quantita = this.getitemnumber(i_rownbr, "quan_ordine")
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			case "num_confezioni"
				ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
				ld_quantita = this.getitemnumber(this.getrow(),"quan_ordine")
				ld_num_confezioni = double(i_coltext)
				ld_num_pezzi  = this.getitemnumber(this.getrow(),"num_pezzi_confezione")
				ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")
				ld_dim_x = getitemnumber(i_rownbr, "dim_x")
				ld_dim_y = getitemnumber(i_rownbr, "dim_y")
				select flag_prezzo
				into   :ls_flag_prezzo
				from   tab_misure
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_misura = :ls_cod_misura;
				if sqlca.sqlcode = 0 then
					if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 then
						if ls_flag_prezzo = 'S' then
							if ld_num_pezzi > 0 and not isnull(ld_num_pezzi) then
								ld_quantita = ld_num_pezzi * ld_num_confezioni
								this.setitem(i_rownbr, "quan_ordine", ld_quantita)
							else
								if not isnull(ls_cod_prodotto) then
									// vado a vedere se ci sono pezzi per collo impostati in anagrafica prodotti
									select 	pezzi_collo
									into		:ld_pezzi_collo
									from 		anag_prodotti
									where 	cod_azienda = :s_cs_xx.cod_azienda and
												cod_prodotto = :ls_cod_prodotto;
									if isnull(ld_pezzi_collo) then ld_pezzi_collo = 1
								else
									ld_pezzi_collo = 1
								end if
								this.setitem(i_rownbr, "num_pezzi_confezione", ld_pezzi_collo)
								ld_quantita = ld_pezzi_collo * ld_num_confezioni
								this.setitem(i_rownbr, "quan_ordine", ld_quantita)
							end if					
						else
							ld_quantita = ld_num_confezioni
							this.setitem(i_rownbr, "quan_ordine", ld_quantita)
						end if
					end if
				else
					g_mb.messagebox("Dettagli Ordine","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
					return 1
				end if		
				
				if mid(f_flag_controllo(),6,1) = "S" and ls_flag_prezzo_bloccato="N"  then
					iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
					iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
					iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
					iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
					iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
					iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
					iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
					iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
					iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
					iuo_condizioni_cliente.str_parametri.valore = 0
					iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
					iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
					iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
					iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
					iuo_condizioni_cliente.wf_condizioni_cliente()
				end if
				
			
			case "num_pezzi_confezione"
				ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
				ld_quantita = this.getitemnumber(this.getrow(),"quan_ordine")
				ld_num_pezzi = double(i_coltext)
				ld_num_confezioni = this.getitemnumber(this.getrow(),"num_confezioni")
				ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")
				ld_dim_x = getitemnumber(i_rownbr, "dim_x")
				ld_dim_y = getitemnumber(i_rownbr, "dim_y")
				select flag_prezzo
				into   :ls_flag_prezzo
				from   tab_misure
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_misura = :ls_cod_misura;
						 
				if sqlca.sqlcode = 0 then
					if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
						if ls_flag_prezzo = 'S' then
							ld_quantita = ld_num_pezzi * ld_num_confezioni
							this.setitem(i_rownbr, "quan_ordine", ld_quantita)
						else
							ld_quantita = ld_num_confezioni
							this.setitem(i_rownbr, "quan_ordine", ld_quantita)
						end if
					end if
				else
					g_mb.messagebox("Dettagli Ordine","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
					return 1
				end if				
	
				if mid(f_flag_controllo(),5,1) = "S" and ls_flag_prezzo_bloccato="N"  then
					iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
					iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
					iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
					iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
					iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
					iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
					iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
					iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
					iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
					iuo_condizioni_cliente.str_parametri.valore = 0
					iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
					iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
					iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_ordine"
					iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
					iuo_condizioni_cliente.wf_condizioni_cliente()
				end if
			
			
	end choose
end if
end event

event buttonclicked;call super::buttonclicked;long ll_anno, ll_num, ll_riga, ll_ret
string ls_msg, ls_flag_stampata_bcl
integer li_risposta

if row > 0 then
	choose case dwo.name
			//Donato 24/05/2010: gestione duplica riga ordine
			//duplica la riga calcolando il nuovo progressivo
			//duplica le eventuali righe collegate
			//inoltre considera anche la comp_det_ord_ven
		case "b_duplica"
			ll_anno = getitemnumber(getrow(),"anno_registrazione")
			ll_num = getitemnumber(getrow(),"num_registrazione")
			ll_riga = getitemnumber(getrow(),"prog_riga_ord_ven")
			
			if ll_anno>0 and ll_num>0 and ll_riga>0 then
				setpointer(HourGlass!)
				
				ll_ret = wf_duplica_riga(ll_anno, ll_num, ll_riga, ls_msg)
				
				choose case ll_ret
					case 1
						//ok fai la commit e dai il messaggio di duplicazione avvenuta
						commit;
						setpointer(Arrow!)
						g_mb.messagebox("APICE",ls_msg, Information!)						
						dw_det_ord_ven_lista.change_dw_current()
						parent.postevent("pc_retrieve")
						return
						
					case 0
						//nessun messaggio e fai rollback per sicurezza
						//es. ordine inesistente o nopn ancora salvato o risposta no alla conferma del duplica
						setpointer(Arrow!)
						rollback;
						return
						
					case else
						//errore, dai il messaggio e fai rollback
						setpointer(Arrow!)
						g_mb.messagebox("APICE",ls_msg,StopSign!)
						rollback;
						return
						
				end choose
				
			end if
	end choose
end if
end event

