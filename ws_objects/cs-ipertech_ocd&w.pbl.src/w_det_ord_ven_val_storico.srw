﻿$PBExportHeader$w_det_ord_ven_val_storico.srw
forward
global type w_det_ord_ven_val_storico from window
end type
type cb_2 from commandbutton within w_det_ord_ven_val_storico
end type
type cb_1 from commandbutton within w_det_ord_ven_val_storico
end type
type dw_1 from datawindow within w_det_ord_ven_val_storico
end type
end forward

global type w_det_ord_ven_val_storico from window
integer width = 1929
integer height = 432
windowtype windowtype = response!
long backcolor = 16777215
string icon = "AppIcon!"
boolean center = true
cb_2 cb_2
cb_1 cb_1
dw_1 dw_1
end type
global w_det_ord_ven_val_storico w_det_ord_ven_val_storico

on w_det_ord_ven_val_storico.create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_1=create dw_1
this.Control[]={this.cb_2,&
this.cb_1,&
this.dw_1}
end on

on w_det_ord_ven_val_storico.destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_1)
end on

event open;dw_1.insertrow(0)

end event

type cb_2 from commandbutton within w_det_ord_ven_val_storico
integer x = 379
integer y = 256
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;dw_1.accepttext()

if g_mb.messagebox("APICE","Confermi l'annullo dell'operazione AR ?",Question!,YesNo!,2) = 1 then
	s_cs_xx.parametri.parametro_d_1 = -1
	close(parent)
end if
	
end event

type cb_1 from commandbutton within w_det_ord_ven_val_storico
integer x = 1120
integer y = 256
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;dw_1.accepttext()

if g_mb.messagebox("APICE","Confermi il valore inserito ?",Question!,YesNo!,2) = 2 then
	return
end if

s_cs_xx.parametri.parametro_d_1 = dw_1.getitemnumber(1,"valore_storico")

if s_cs_xx.parametri.parametro_d_1 = 0 then
	if g_mb.messagebox("APICE","Il valore è ZERO; sei sicuro?",Question!,YesNo!,2) = 2 then
		return
	end if
end if

close(parent)
end event

type dw_1 from datawindow within w_det_ord_ven_val_storico
integer x = 123
integer y = 48
integer width = 1742
integer height = 128
integer taborder = 10
string title = "none"
string dataobject = "d_det_ord_ven_val_storico_ext"
boolean border = false
boolean livescroll = true
end type

