﻿$PBExportHeader$w_evas_ordini_barcode.srw
forward
global type w_evas_ordini_barcode from w_cs_xx_risposta
end type
type dw_storico from datawindow within w_evas_ordini_barcode
end type
type tab_1 from tab within w_evas_ordini_barcode
end type
type tabpage_1 from userobject within tab_1
end type
type dw_lista_bolle_pronte from datawindow within tabpage_1
end type
type cb_nuova_bolla from commandbutton within tabpage_1
end type
type cb_5 from commandbutton within tabpage_1
end type
type cb_aggiungi_a_bolla from commandbutton within tabpage_1
end type
type dw_ext_dati_bolla from datawindow within tabpage_1
end type
type tabpage_1 from userobject within tab_1
dw_lista_bolle_pronte dw_lista_bolle_pronte
cb_nuova_bolla cb_nuova_bolla
cb_5 cb_5
cb_aggiungi_a_bolla cb_aggiungi_a_bolla
dw_ext_dati_bolla dw_ext_dati_bolla
end type
type tabpage_2 from userobject within tab_1
end type
type dw_lista_fatture_pronte from datawindow within tabpage_2
end type
type cb_nuova_fattura from commandbutton within tabpage_2
end type
type cb_3 from commandbutton within tabpage_2
end type
type cb_aggiungi_a_fattura from commandbutton within tabpage_2
end type
type dw_ext_dati_fattura from datawindow within tabpage_2
end type
type tabpage_2 from userobject within tab_1
dw_lista_fatture_pronte dw_lista_fatture_pronte
cb_nuova_fattura cb_nuova_fattura
cb_3 cb_3
cb_aggiungi_a_fattura cb_aggiungi_a_fattura
dw_ext_dati_fattura dw_ext_dati_fattura
end type
type tabpage_3 from userobject within tab_1
end type
type dw_barcode from datawindow within tabpage_3
end type
type st_1 from statictext within tabpage_3
end type
type cb_1 from commandbutton within tabpage_3
end type
type cb_conferma from commandbutton within tabpage_3
end type
type cb_deseleziona from commandbutton within tabpage_3
end type
type cb_seleziona from commandbutton within tabpage_3
end type
type dw_righe_ordine from datawindow within tabpage_3
end type
type tabpage_3 from userobject within tab_1
dw_barcode dw_barcode
st_1 st_1
cb_1 cb_1
cb_conferma cb_conferma
cb_deseleziona cb_deseleziona
cb_seleziona cb_seleziona
dw_righe_ordine dw_righe_ordine
end type
type tab_1 from tab within w_evas_ordini_barcode
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
end type
end forward

global type w_evas_ordini_barcode from w_cs_xx_risposta
integer x = 0
integer y = 200
integer width = 4855
integer height = 2100
string title = "Creazione Bolle e Fatture da Ordini"
boolean minbox = true
boolean maxbox = true
windowtype windowtype = main!
event ue_seleziona_tab ( )
event ue_show ( )
dw_storico dw_storico
tab_1 tab_1
end type
global w_evas_ordini_barcode w_evas_ordini_barcode

type variables
string						is_flag_bol_fat, is_cod_cliente, is_cod_tipo_bol_ven, is_cod_tipo_fat_ven, is_deposito_consegna

boolean					ib_anticipo, ib_evasione_parziale
long						il_anno_registrazione[], il_num_registrazione[]
uo_storicizzazione		iuo_storicizzazione

datawindow				idw_righe_ordini_spedizioni
boolean					ib_da_spedizione = false

longlong					il_prog_sessione

uo_fido_cliente			iuo_fido
end variables

forward prototypes
public function integer wf_storicizza_riga (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_messaggio)
public function integer wf_aggiungi_riga (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_messaggio)
public function integer wf_carica_riga_ordine (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_messaggio)
public subroutine wf_seleziona_tab ()
public function integer wf_memorizza_riga (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, decimal ad_quan_evasione, ref string as_messaggio)
public function boolean wf_check_riga (long al_anno_reg, long al_num_reg, long al_prog_riga)
public function integer wf_chiudi_commessa (long fl_anno_reg, long fl_num_reg, long fl_num_riga)
public function integer wf_carica_da_spedizioni ()
public function integer wf_update_spedizione (long fl_anno_documento, long fl_num_documento, string fs_tipo_documento, ref string fs_msg)
public function integer wf_aggiungi_righe_ordine (long al_anno_registrazione, long al_num_registrazione)
public function integer wf_controlla_cliente_e_tipo_doc (integer al_anno_registrazione, long al_num_registrazione, ref string as_messaggio)
public function longlong wf_get_prog_sessione ()
end prototypes

event ue_show();w_report_fat_ven_euro.SHOW()
end event

public function integer wf_storicizza_riga (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_messaggio);string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, &
		 ls_listino, ls_messaggio, ls_cod_tipo_det_ven_addizionali, ls_mp_escluse[], ls_testo_esclusioni, ls_title, ls_parametro,ls_flag_evasione,&
		 ls_flag_blocco
long   ll_num_riga_appartenenza, ll_ret, &
       ll_righe_escluse[], ll_riga_corrente
double ll_sconti[], ll_maggiorazioni[], ll_i, ll_y
double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo, &
       ldd_quan_evasa, ldd_quan_in_evasione
datetime ldt_data_registrazione



select flag_evasione,quan_evasa,quan_in_evasione, flag_blocco, num_riga_appartenenza, cod_tipo_det_ven
into   :ls_flag_evasione,:ldd_quan_evasa,:ldd_quan_in_evasione, :ls_flag_blocco, :ll_num_riga_appartenenza, :ls_cod_tipo_det_ven
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
 		 anno_registrazione = :al_anno_registrazione and
		  num_registrazione = :al_num_registrazione and
		  prog_riga_ord_ven = :al_prog_riga_ord_ven;

if sqlca.sqlcode = 100 then
	as_messaggio = "Riga ordine " + string(al_prog_riga_ord_ven) + " non trovata "
	return -1
end if

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in ricerca riga ordine " + string(al_prog_riga_ord_ven) + "~r~n" + sqlca.sqlerrtext
	return -1
end if

if ls_flag_evasione <> "A" then
	as_messaggio = "Non è possibile effettuare storicizzazioni di righe già evase o parzialmente evase"
	return -1
end if

if ldd_quan_evasa > 0 or ldd_quan_in_evasione > 0  then
	as_messaggio = "Non è possibile effettuare storicizzazioni di righe con quantità già evasa o in evasione"
	return -1
end if

if ls_flag_blocco = 'S' then
	as_messaggio = "Non puoi storicizzare una riga d'ordine bloccata !!!"
	return -1
end if


if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then
	// è una riga di prodotto finito, non addizionale optional o altro del genere
	// funzione di storicizzazione riga prodotto finito
	
	ll_ret = iuo_storicizzazione.uof_storicizza(al_anno_registrazione, al_num_registrazione, al_prog_riga_ord_ven, true, ls_mp_escluse[], ll_righe_escluse[], ref ls_testo_esclusioni, ref ls_messaggio )
	
	if ll_ret < 0 then
		as_messaggio = "Errore nella storicizzazione.~r~n"+ls_messaggio
		return -1
	end if
	
	
else	
	// controllo se si tratta di una riga di addizionale optional o altro del genere
	select cod_tipo_det_ven_addizionali
	into   :ls_cod_tipo_det_ven_addizionali
	from   tab_flags_configuratore
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_modello in (select cod_prodotto
								  from   det_ord_ven
								  where  cod_azienda = :s_cs_xx.cod_azienda and
											anno_registrazione = :al_anno_registrazione and
											num_registrazione = :al_num_registrazione and
											prog_riga_ord_ven = :ll_num_riga_appartenenza);
	
	if sqlca.sqlcode <> 0 then
		as_messaggio = "Errore in ricerca tipo dettaglio addizionali in tabella flags configuratore~r~n"+ sqlca.sqlerrtext
		return -1
	end if
	

	// se si tratta di un'addizionale devo mettere un messaggio speciale
	if ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_addizionali then
		//if g_mb.messagebox("STORICIZZAZIONE","ATTENZIONE: E' UNA ADDIZIONALE. PROCEDO ?",Question!,YesNo!,2) = 2 then return
	end if
	
	// funzione di storicizzazione riga non prodotto finito

	ll_ret = iuo_storicizzazione.uof_storicizza(al_anno_registrazione, al_num_registrazione, al_prog_riga_ord_ven, false, ls_mp_escluse[], ll_righe_escluse[], ref ls_testo_esclusioni, ref ls_messaggio )
	
	if ll_ret < 0 then
		as_messaggio = "Errore nella storicizzazione.~r~n"+ls_messaggio
		return -1
	end if
		
end if

return 0
end function

public function integer wf_aggiungi_riga (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_messaggio);long					ll_ret, ll_find

string					ls_null, ls_cod_cliente, ls_flag_tipo_bol_fat, ls_cod_parametro_blocco, ls_messaggio, ls_cod_tipo_ord_ven
		
datetime				ldt_oggi

integer				li_ret



ldt_oggi = datetime(today(), 00:00:00)
setnull(ls_null)

//13/04/2012 Donato: spostato in una function il controllo
li_ret = wf_controlla_cliente_e_tipo_doc( al_anno_registrazione, al_num_registrazione, as_messaggio )
if li_ret < 0 then
	//in as_messaggio il messaggio di errore, sia SQL che incongruenze varie, cliente e/o tipo documento
	return -1
end if


//*******************************************************************************
//controlla blocchi cliente (se sono presenti già righe dello stesso ordine, in caso di warning NON ridare il messaggio, in caso di blocco si!)
select cod_cliente, cod_tipo_ord_ven
into	:ls_cod_cliente, :ls_cod_tipo_ord_ven
from tes_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:al_anno_registrazione and
			num_registrazione=:al_num_registrazione;

select flag_tipo_bol_fat
into :ls_flag_tipo_bol_fat
from tab_tipi_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

if ls_flag_tipo_bol_fat="B" then
	//segue ddt
	ls_cod_parametro_blocco = "GBV"
else
	//segue fattura
	ls_cod_parametro_blocco = "GFV"
end if

ll_ret = iuo_fido.uof_get_blocco_cliente( ls_cod_cliente, ldt_oggi, ls_cod_parametro_blocco, ls_messaggio)
if ll_ret=2 then
	//blocco
	//in as_messaggio il messaggio
	as_messaggio = ls_messaggio
	return -1
	
elseif ll_ret=1 then
	//solo warning
	//se una riga dello stesso ordine è già presente allora evita di ridare il warning
	ll_find  = tab_1.tabpage_3.dw_righe_ordine.find(	"anno_registrazione= "+string(al_anno_registrazione)+" and num_registrazione="+string(al_num_registrazione), &
												1, tab_1.tabpage_3.dw_righe_ordine.rowcount())
	if ll_find>0 then
		//già presente, inutile ridare il warning
	else
		g_mb.warning(ls_messaggio)
	end if
end if
//*******************************************************************************


ll_ret = wf_carica_riga_ordine(al_anno_registrazione, al_num_registrazione, al_prog_riga_ord_ven, ref as_messaggio)
if ll_ret < 0 then
	return -1
end if
		  
return 0
end function

public function integer wf_carica_riga_ordine (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_messaggio);string ls_cod_prodotto, ls_cod_tipo_det_ven, ls_cod_misura, ls_des_prodotto, ls_cod_misura_mag, ls_flag_urgente
long ll_num_commessa, ll_anno_commessa, ll_riga, ll_riga_appartenenza, ll_prog_riga_ord_ven
dec{4} ld_quan_ordine, ld_prezzo_vendita, ld_val_riga, ld_quan_in_evasione, ld_quan_evasa, ld_quantita_um, ld_prezzo_um, ld_sconto_1, ld_sconto_2
datetime ldt_data_consegna

DECLARE cu_righe_ordini CURSOR FOR  
SELECT 	cod_prodotto,  cod_tipo_det_ven, cod_misura, des_prodotto, quan_ordine, prezzo_vendita, sconto_1, sconto_2, &
			data_consegna, val_riga, quan_in_evasione, quan_evasa, anno_commessa, &
			num_commessa, quantita_um, prezzo_um, flag_urgente, num_riga_appartenenza, prog_riga_ord_ven
 FROM det_ord_ven  
WHERE	cod_azienda = :s_cs_xx.cod_azienda AND  
			anno_registrazione = : al_anno_registrazione AND  
			num_registrazione = :al_num_registrazione AND  
			(prog_riga_ord_ven = :al_prog_riga_ord_ven or num_riga_appartenenza = :al_prog_riga_ord_ven ) AND  
			(quan_ordine - quan_in_evasione - quan_evasa) > 0
ORDER BY prog_riga_ord_ven ASC;

open cu_righe_ordini;

if sqlca.sqlcode = -1 then
	as_messaggio = "Errore in open del cursore cu_righe_ordini " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_righe_ordini into 	:ls_cod_prodotto, :ls_cod_tipo_det_ven, :ls_cod_misura, :ls_des_prodotto, :ld_quan_ordine, :ld_prezzo_vendita, :ld_sconto_1, :ld_sconto_2, &
										:ldt_data_consegna, :ld_val_riga, :ld_quan_in_evasione, :ld_quan_evasa, :ll_anno_commessa, &
										:ll_num_commessa, :ld_quantita_um, :ld_prezzo_um, :ls_flag_urgente, :ll_riga_appartenenza, :ll_prog_riga_ord_ven;
	if sqlca.sqlcode = -1 then
		as_messaggio = "Errore in lettura righe ordini " + sqlca.sqlerrtext
		close cu_righe_ordini;
		return -1
	end if
	if sqlca.sqlcode = 100 then exit
	
	if not isnull(ls_cod_prodotto) then
		select cod_misura_mag
		into   :ls_cod_misura_mag
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
	end if	
	
	ll_riga = tab_1.tabpage_3.dw_righe_ordine.insertrow(0)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "anno_registrazione", al_anno_registrazione)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "num_registrazione", al_num_registrazione)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "prog_riga_ord_ven", ll_prog_riga_ord_ven)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "cod_tipo_det_ven", ls_cod_tipo_det_ven)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "des_prodotto", ls_des_prodotto)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "cod_misura_mag", ls_cod_misura_mag)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "cod_misura_ven", ls_cod_misura)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quan_ordine", ld_quan_ordine)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quantita_um", ld_quantita_um)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "prezzo_vendita", ld_prezzo_vendita)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "sconto_1", ld_sconto_1)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "sconto_2", ld_sconto_2)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "prezzo_um", ld_prezzo_um)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quan_in_evasione", ld_quan_in_evasione)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quan_evasa", ld_quan_evasa)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "val_riga", ld_val_riga)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "anno_commessa", ll_anno_commessa)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "num_commessa", ll_num_commessa)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "data_consegna", ldt_data_consegna)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quan_residua", ld_quan_ordine - ld_quan_in_evasione - ld_quan_evasa)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "quan_da_evadere", ld_quan_ordine - ld_quan_in_evasione - ld_quan_evasa)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "flag_evasione_totale", "S")
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "flag_urgente", ls_flag_urgente)
	tab_1.tabpage_3.dw_righe_ordine.setitem(ll_riga, "num_riga_appartenenza", ll_riga_appartenenza)
loop

close cu_righe_ordini;

tab_1.selecttab(3)

return 0
end function

public subroutine wf_seleziona_tab ();long				ll_riga, ll_i
string				ls_cod_des_cliente, ls_cod_causale, ls_cod_porto, ls_cod_mezzo, ls_cod_resa, ls_cod_vettore, ls_cod_inoltro, ls_nota_piede, &
					ls_aspetto_beni, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cap, ls_localita,ls_provincia, ls_null, ls_num_ord_cliente, &
					ls_nota_testata, ls_nota_video, ls_cod_tipo_ord_ven, ls_cod_tipo_bol_ven, ls_cod_imballo,ls_flag_bol_fat,ls_cod_tipo_fat_ven, ls_temp, ls_db
double			ld_num_colli, ld_peso_lordo, ld_peso_netto, ld_num_colli_sum
datetime			ldt_oggi, ldt_data_ord_cliente

select cod_des_cliente, cod_porto, cod_mezzo, cod_resa, cod_vettore, cod_inoltro, aspetto_beni, nota_piede,
		 rag_soc_1, rag_soc_2, indirizzo, frazione, cap, localita, provincia, num_colli, peso_lordo, peso_netto, cod_causale, num_ord_cliente, data_ord_cliente, cod_tipo_ord_ven, cod_imballo
into	:ls_cod_des_cliente, :ls_cod_porto, :ls_cod_mezzo, :ls_cod_resa, :ls_cod_vettore, :ls_cod_inoltro, :ls_aspetto_beni, :ls_nota_piede,
		:ls_rag_soc_1, :ls_rag_soc_2, :ls_indirizzo, :ls_frazione, :ls_cap, :ls_localita,:ls_provincia, :ld_num_colli, :ld_peso_lordo, :ld_peso_netto, :ls_cod_causale, :ls_num_ord_cliente, :ldt_data_ord_cliente, :ls_cod_tipo_ord_ven, :ls_cod_imballo
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :il_anno_registrazione[1] and
		 num_registrazione = :il_num_registrazione[1];
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca ordine " + string(il_anno_registrazione[1]) + "/" + string(il_num_registrazione[1])+ ". Impossibile proseguire", stopsign!)
	return
end if

// stefanop 16/02/2012
// devo ciclare tutte le righe di testata per calcolare il nuovo numero di colli, altrimenti mi mostra solo i colli del primo ordine
// TODO: Il calcolo dei colli non deve essere fatto dalla testa, ma dalla singola riga in base che sia un REPORT 1 o 2-3
if upperbound(il_anno_registrazione) > 1 then
	
	for ll_i = 2 to upperbound(il_anno_registrazione) 
		
		select num_colli, nota_piede
		into :ld_num_colli_sum, :ls_temp
		from tes_ord_ven
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :il_anno_registrazione[ll_i] and
					num_registrazione = :il_num_registrazione[ll_i];
					
		ld_num_colli += ld_num_colli_sum
		
		if trim(ls_temp)<>"" and not isnull(ls_temp) then ls_nota_piede += "~r~n" + ls_temp
		
	next
end if

// ----- controllo lunghezza note per ASE
Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
if (upper(ls_db) = "SYBASE_ASE" or upper(ls_db) = "MSSQL") and len(ls_nota_piede) > 255 then
	ls_nota_piede = left(ls_nota_piede, 255)
end if
// -----  fine controllo lunghezza note per ASE


choose case is_flag_bol_fat
	case "B"
		
		tab_1.tabpage_1.enabled = true
		tab_1.tabpage_2.enabled = false
		tab_1.SelectTab(1)			
		tab_1.tabpage_1.dw_lista_bolle_pronte.setredraw(false)
		tab_1.tabpage_1.dw_ext_dati_bolla.setredraw(false)
		tab_1.tabpage_1.dw_lista_bolle_pronte.retrieve(s_cs_xx.cod_azienda, is_cod_cliente)
		tab_1.tabpage_1.dw_ext_dati_bolla.reset()
		ll_riga = tab_1.tabpage_1.dw_ext_dati_bolla.insertrow(0)
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_destinazione", &
							  sqlca, &
							  "anag_des_clienti", &
							  "cod_des_cliente", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + is_cod_cliente + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_imballo", &
							  sqlca, &
							  "tab_imballi", &
							  "cod_imballo", &
							  "des_imballo", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_vettore", &
							  sqlca, &
							  "anag_vettori", &
							  "cod_vettore", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_inoltro", &
							  sqlca, &
							  "anag_vettori", &
							  "cod_vettore", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_mezzo", &
							  sqlca, &
							  "tab_mezzi", &
							  "cod_mezzo", &
							  "des_mezzo", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_porto", &
							  sqlca, &
							  "tab_porti", &
							  "cod_porto", &
							  "des_porto", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_resa", &
							  sqlca, &
							  "tab_rese", &
							  "cod_resa", &
							  "des_resa", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_1.dw_ext_dati_bolla, &
							  "cod_causale", &
							  sqlca, &
							  "tab_causali_trasp", &
							  "cod_causale", &
							  "des_causale", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_tipo_bolla", &
							  sqlca, &
							  "tab_tipi_bol_ven", &
							  "cod_tipo_bol_ven", &
							  "des_tipo_bol_ven", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' " )
				
				
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_cliente", is_cod_cliente)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_destinazione", ls_cod_des_cliente)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_mezzo", ls_cod_mezzo)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_causale", ls_cod_causale)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_porto", ls_cod_porto)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_resa", ls_cod_resa)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_vettore", ls_cod_vettore)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_inoltro", ls_cod_inoltro)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_imballo", ls_cod_imballo)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "aspetto_beni", ls_aspetto_beni)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "rag_soc_1", ls_rag_soc_1)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "rag_soc_2", ls_rag_soc_2)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "indirizzo", ls_indirizzo)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "frazione", ls_frazione)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "localita", ls_localita)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cap", ls_cap)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "provincia", ls_provincia)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "num_colli", ld_num_colli)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "peso_netto", ld_peso_netto)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "peso_lordo", ld_peso_lordo)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "num_ord_cliente", ls_num_ord_cliente)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "data_ord_cliente", ldt_data_ord_cliente)
		tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "note_piede", ls_nota_piede)
		tab_1.tabpage_1.dw_lista_bolle_pronte.setredraw(true)
		tab_1.tabpage_1.dw_ext_dati_bolla.setredraw(true)

		if guo_functions.uof_get_note_fisse(is_cod_cliente, ls_null, ls_null, "BOL_VEN", ls_null, ldt_oggi, ls_nota_testata,ls_nota_piede, ls_nota_video) < 0 then
			g_mb.error(ls_nota_testata)
		else
			if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA", ls_nota_video)
		end if
		
		if isnull(ls_cod_causale) then
			select cod_tipo_bol_ven
			into   :ls_cod_tipo_bol_ven
			from   tab_tipi_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
			if not isnull(ls_cod_tipo_bol_ven) then
				select cod_causale
				into   :ls_cod_causale
				from   tab_tipi_bol_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
				if not isnull(ls_cod_causale) then
					tab_1.tabpage_1.dw_ext_dati_bolla.setitem(ll_riga, "cod_causale", ls_cod_causale)
				end if
			end if
		end if
		
	case "F"
		tab_1.tabpage_1.enabled = false
		tab_1.tabpage_2.enabled = true
		tab_1.SelectTab(2)			
		tab_1.tabpage_2.dw_lista_fatture_pronte.setredraw(false)
		tab_1.tabpage_2.dw_ext_dati_fattura.setredraw(false)
		tab_1.tabpage_2.dw_lista_fatture_pronte.retrieve(s_cs_xx.cod_azienda, is_cod_cliente)
		tab_1.tabpage_2.dw_ext_dati_fattura.reset()
		ll_riga = tab_1.tabpage_2.dw_ext_dati_fattura.insertrow(0)
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_destinazione", &
							  sqlca, &
							  "anag_des_clienti", &
							  "cod_des_cliente", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + is_cod_cliente + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_imballo", &
							  sqlca, &
							  "tab_imballi", &
							  "cod_imballo", &
							  "des_imballo", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_vettore", &
							  sqlca, &
							  "anag_vettori", &
							  "cod_vettore", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_inoltro", &
							  sqlca, &
							  "anag_vettori", &
							  "cod_vettore", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_mezzo", &
							  sqlca, &
							  "tab_mezzi", &
							  "cod_mezzo", &
							  "des_mezzo", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_porto", &
							  sqlca, &
							  "tab_porti", &
							  "cod_porto", &
							  "des_porto", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_resa", &
							  sqlca, &
							  "tab_rese", &
							  "cod_resa", &
							  "des_resa", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_causale", &
							  sqlca, &
							  "tab_causali_trasp", &
							  "cod_causale", &
							  "des_causale", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		f_po_loaddddw_dw(tab_1.tabpage_2.dw_ext_dati_fattura, &
							  "cod_tipo_bolla", &
							  sqlca, &
							  "tab_tipi_fat_ven", &
							  "cod_tipo_fat_ven", &
							  "des_tipo_fat_ven", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' " )
				
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_cliente", is_cod_cliente)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_destinazione", ls_cod_des_cliente)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_mezzo", ls_cod_mezzo)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_causale", ls_cod_causale)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_porto", ls_cod_porto)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_resa", ls_cod_resa)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_vettore", ls_cod_vettore)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_inoltro", ls_cod_inoltro)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_imballo", ls_cod_imballo)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "aspetto_beni", ls_aspetto_beni)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_cliente", is_cod_cliente)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "rag_soc_1", ls_rag_soc_1)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "rag_soc_2", ls_rag_soc_2)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "indirizzo", ls_indirizzo)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "frazione", ls_frazione)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "localita", ls_localita)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cap", ls_cap)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "provincia", ls_provincia)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "num_colli", ld_num_colli)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "peso_netto", ld_peso_netto)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "peso_lordo", ld_peso_lordo)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "num_ord_cliente", ls_num_ord_cliente)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "data_ord_cliente", ldt_data_ord_cliente)
		tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "note_piede", ls_nota_piede)

		if guo_functions.uof_get_note_fisse(is_cod_cliente, ls_null, ls_null, "FAT_VEN", ls_null, ldt_oggi, ls_nota_testata,ls_nota_piede, ls_nota_video) < 0 then
			g_mb.error(ls_nota_testata)
		else
			if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA", ls_nota_video)
		end if
		
		if isnull(ls_cod_causale) then
			select cod_tipo_fat_ven
			into   :ls_cod_tipo_fat_ven
			from   tab_tipi_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
			if not isnull(ls_cod_tipo_fat_ven) then
				select cod_causale
				into   :ls_cod_causale
				from   tab_tipi_fat_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
				if not isnull(ls_cod_causale) then
					tab_1.tabpage_2.dw_ext_dati_fattura.setitem(ll_riga, "cod_causale", ls_cod_causale)
				end if
			end if
		end if
		tab_1.tabpage_2.dw_lista_fatture_pronte.setredraw(true)
		tab_1.tabpage_2.dw_ext_dati_fattura.setredraw(true)
end choose

end subroutine

public function integer wf_memorizza_riga (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, decimal ad_quan_evasione, ref string as_messaggio);string ls_cod_prodotto, ls_cod_misura, ls_des_prodotto,ls_rag_soc_1,ls_rag_soc_2,ls_indirizzo,ls_cap,ls_localita,ls_provincia, &
       ls_cod_tessuto, ls_colore_tessuto,ls_des_tessuto, ls_telefono, ls_telex

long 	 ll_riga

dec{4} ld_prezzo_vendita,ld_sconto_1,ld_sconto_2,ld_quantita_um,ld_prezzo_um,ld_imponibile_iva, ld_dim_x, ld_dim_y, ld_dim_z

datetime ldt_data_registrazione

select data_registrazione
into   :ldt_data_registrazione
from   tes_ord_ven
WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( anno_registrazione = : al_anno_registrazione) AND  
		( num_registrazione = :al_num_registrazione );
if sqlca.sqlcode <> 0 then
	as_messaggio = sqlca.sqlerrtext
	return -1
end if
		
select rag_soc_1,
		 rag_soc_2,
		 indirizzo,
		 cap,
		 localita,
		 provincia,
		 telefono,
		 telex
into   :ls_rag_soc_1,
		 :ls_rag_soc_2,
		 :ls_indirizzo,
		 :ls_cap,
		 :ls_localita,
		 :ls_provincia,
		 :ls_telefono,
		 :ls_telex
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :is_cod_cliente;
if sqlca.sqlcode <> 0 then
	as_messaggio = sqlca.sqlerrtext
	return -1
end if
		 
SELECT cod_prodotto,  
       cod_misura, 
		 des_prodotto, 
		 prezzo_vendita, 
		 sconto_1, 
		 sconto_2, 
		 quantita_um, 
		 prezzo_um, 
		 imponibile_iva
into   :ls_cod_prodotto,  
       :ls_cod_misura, 
		 :ls_des_prodotto, 
		 :ld_prezzo_vendita, 
		 :ld_sconto_1, 
		 :ld_sconto_2, 
		 :ld_quantita_um, 
		 :ld_prezzo_um, 
		 :ld_imponibile_iva
 FROM  det_ord_ven  
WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( anno_registrazione = : al_anno_registrazione) AND  
		( num_registrazione = :al_num_registrazione ) AND  
		( prog_riga_ord_ven = :al_prog_riga_ord_ven );
if sqlca.sqlcode <> 0 then
	as_messaggio = sqlca.sqlerrtext
	return -1
end if

SELECT dim_x,  
       dim_y,
		 dim_z,
		 cod_tessuto,
		 cod_non_a_magazzino
into   :ld_dim_x,
		 :ld_dim_y,
		 :ld_dim_z,
		 :ls_cod_tessuto,
		 :ls_colore_tessuto
 FROM  comp_det_ord_ven  
WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( anno_registrazione = : al_anno_registrazione) AND  
		( num_registrazione = :al_num_registrazione ) AND  
		( prog_riga_ord_ven = :al_prog_riga_ord_ven );
if sqlca.sqlcode < 0 then
	as_messaggio = sqlca.sqlerrtext
	return -1
end if

if not isnull(ls_cod_tessuto) and len(ls_cod_tessuto) > 0 then
	select des_prodotto
	into   :ls_des_tessuto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_tessuto;
	if sqlca.sqlcode = 0 then
		ls_des_prodotto = ls_des_prodotto + "~r~n" + ls_cod_tessuto + " " + ls_des_tessuto + " - " + ls_colore_tessuto
	end if
end if

ll_riga = dw_storico.insertrow(0)

dw_storico.setitem(ll_riga, "tes_ord_ven_cod_cliente", is_cod_cliente )
dw_storico.setitem(ll_riga, "tes_ord_ven_rag_soc_1", ls_rag_soc_1)
dw_storico.setitem(ll_riga, "tes_ord_ven_rag_soc_2", ls_rag_soc_2)
dw_storico.setitem(ll_riga, "tes_ord_ven_indirizzo", ls_indirizzo)
dw_storico.setitem(ll_riga, "tes_ord_ven_localita", ls_localita)
dw_storico.setitem(ll_riga, "tes_ord_ven_cap", ls_cap)
dw_storico.setitem(ll_riga, "tes_ord_ven_provincia", ls_provincia)
dw_storico.setitem(ll_riga, "tes_ord_ven_telefono", ls_telefono)
dw_storico.setitem(ll_riga, "tes_ord_ven_cellulare", ls_telex)
dw_storico.setitem(ll_riga, "det_ord_ven_cod_prodotto", ls_cod_prodotto)
dw_storico.setitem(ll_riga, "det_ord_ven_des_prodotto", ls_des_prodotto)
dw_storico.setitem(ll_riga, "det_ord_ven_quan_consegna", ad_quan_evasione)
dw_storico.setitem(ll_riga, "det_ord_ven_cod_misura", ls_cod_misura)
dw_storico.setitem(ll_riga, "det_ord_ven_prezzo", ld_prezzo_vendita)
dw_storico.setitem(ll_riga, "det_ord_ven_sconto_1", ld_sconto_1)
dw_storico.setitem(ll_riga, "det_ord_ven_sconto_2", ld_sconto_2)
dw_storico.setitem(ll_riga, "det_ord_ven_importo", ld_imponibile_iva)
dw_storico.setitem(ll_riga, "tes_ord_ven_anno_registrazione", al_anno_registrazione)
dw_storico.setitem(ll_riga, "tes_ord_ven_num_registrazione", al_num_registrazione)
dw_storico.setitem(ll_riga, "tes_ord_ven_data_registrazione", ldt_data_registrazione)
dw_storico.setitem(ll_riga, "det_ord_ven_prog_riga_ord_ven", al_prog_riga_ord_ven)
dw_storico.setitem(ll_riga, "comp_det_ord_ven_dim_x", ld_dim_x)
dw_storico.setitem(ll_riga, "comp_det_ord_ven_dim_y", ld_dim_y)
dw_storico.setitem(ll_riga, "comp_det_ord_ven_dim_z", ld_dim_z)


return 0

end function

public function boolean wf_check_riga (long al_anno_reg, long al_num_reg, long al_prog_riga);long		ll_i, ll_anno, ll_num, ll_riga


for ll_i = 1 to tab_1.tabpage_3.dw_righe_ordine.rowcount()
	
	ll_anno = tab_1.tabpage_3.dw_righe_ordine.getitemnumber(ll_i,"anno_registrazione")
	ll_num = tab_1.tabpage_3.dw_righe_ordine.getitemnumber(ll_i,"num_registrazione")
	ll_riga = tab_1.tabpage_3.dw_righe_ordine.getitemnumber(ll_i,"prog_riga_ord_ven")
	
	if ll_anno = al_anno_reg and ll_num = al_num_reg and ll_riga = al_prog_riga then
		return false
	end if
	
next

return true
end function

public function integer wf_chiudi_commessa (long fl_anno_reg, long fl_num_reg, long fl_num_riga);//Donato 03-12-2008
//Questa funzione esegue la chiusura di una eventuale commessa associata alla riga di dettaglio dell'ordine
//Valore ritorno funzione
// 1 	tutto è a posto
//-1 	c'è un errore
// 0 	on deve fare la chiusura (nessuna commessa associata)

integer					li_anno_commessa, li_risposta
long						ll_num_commessa
string						ls_flag_tipo_avanzamento, ls_errore, ls_null, ls_cod_dep_giro, ls_cod_dep_origine
uo_funzioni_1			luo_funzioni_1
uo_produzione			luo_prod
uo_log_sistema			luo_log

setnull(ls_null)

select det_ord_ven.anno_commessa, det_ord_ven.num_commessa, tes_ord_ven.cod_deposito
into   :li_anno_commessa, :ll_num_commessa, :ls_cod_dep_origine
from   det_ord_ven
left outer join tes_ord_ven on 	tes_ord_ven.cod_azienda=det_ord_ven.cod_azienda and
										tes_ord_ven.anno_registrazione=det_ord_ven.anno_registrazione and
										tes_ord_ven.num_registrazione=det_ord_ven.num_registrazione
where	det_ord_ven.cod_azienda        = :s_cs_xx.cod_azienda and 
			det_ord_ven.anno_registrazione = :fl_anno_reg and
			det_ord_ven.num_registrazione  = :fl_num_reg and 
			det_ord_ven.prog_riga_ord_ven = :fl_num_riga;
		 
if sqlca.sqlcode < 0 then
	//errore
	g_mb.messagebox("Apice","Attenzione! Errore durante la lettura della commessa associata all'ordine " + &
											string(fl_anno_reg)+"/"+string(fl_num_reg)+" riga "+string(fl_num_riga) + &
											sqlca.sqlerrtext,stopsign!)
	return -1
end if

if li_anno_commessa = 0 or isnull(li_anno_commessa) then
	//nessuna commessa associata
	return 0
end if

select flag_tipo_avanzamento
into   :ls_flag_tipo_avanzamento
from   anag_commesse
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_commessa=:li_anno_commessa
and    num_commessa=:ll_num_commessa;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Attenzione! Errore durante la lettura del flag tipo avanzamento della commessa: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if ls_flag_tipo_avanzamento = "9" or ls_flag_tipo_avanzamento = "8" or ls_flag_tipo_avanzamento = "7" or &
	ls_flag_tipo_avanzamento = "1" or ls_flag_tipo_avanzamento = "2" then
	
	//chiesto da Alberto e Beatrcice il 30/10/2014
	//se la commessa è già chiusa non dare più messaggio di alert, basta che venga saltata!
	
	/*
	g_mb.messagebox("Apice","Attenzione! La commessa " + string(li_anno_commessa) + "/" + string(ll_num_commessa) + &
				" relativa all'ordine " + string(fl_anno_reg)+"/"+string(fl_num_reg)+" riga "+string(fl_num_riga) + &
				" si trova in uno stato per il quale non è più possibile effettuare un avanzamento automatico.",exclamation!)
	*/
	
	return 0
else
	
	
	luo_funzioni_1 = create uo_funzioni_1
	luo_funzioni_1.uof_set_posticipato(true)
	
	//----------------------------------------------------------------------------------
	//se l'ordine ha un giro consegna con deposito applicato e diverso da quello oriogine ordine, allora il deposito di chiusura della commessa deve essere questo
	//quindi le MP saranno prelevate da questo deposito e il PF verrà caricato in questo deposito!!
	luo_prod = create uo_produzione
	ls_cod_dep_giro = luo_prod.uof_deposito_giro_consegna(fl_anno_reg, fl_num_reg)
	destroy luo_prod
	
	if g_str.isnotempty(ls_cod_dep_giro) and ls_cod_dep_origine<>ls_cod_dep_giro then
		luo_funzioni_1.ib_forza_deposito_scarico_mp = true
		luo_funzioni_1.is_cod_deposito_scarico_mp = ls_cod_dep_giro
		
		luo_log = create uo_log_sistema
		luo_log.uof_write_log_sistema_not_sqlca("DEP.CONS.COMM", "Applicato deposito codice "+ls_cod_dep_giro+" del  giro consegna dell'ordine "+string(fl_anno_reg)+"/" + string(fl_num_reg)+&
																						" al posto del deposito origine "+ls_cod_dep_origine+" in chiusura commessa n. "+string(li_anno_commessa)+"/" + string(ll_num_commessa) + &
																						" da finestra evasione con barcode.")
		destroy luo_log
		
	else
		setnull(luo_funzioni_1.is_cod_deposito_scarico_mp)
	end if
	//----------------------------------------------------------------------------------
	
	
	li_risposta = luo_funzioni_1.uof_avanza_commessa(li_anno_commessa,ll_num_commessa, ls_null,ls_errore)
	
	if li_risposta < 0 then 
		li_risposta = f_scrivi_log ("Errore sulla commessa anno " + string(li_anno_commessa) +" numero " + string(ll_num_commessa) + &
											" relativa all'ordine " + string(fl_anno_reg)+"/"+string(fl_num_reg)+" riga "+string(fl_num_riga) + &
											". Dettaglio errore: " + ls_errore)
		g_mb.messagebox("Sep","Si è manifestato un errore durante la chiusura commessa: consultare il file di log."+char(13)+&
											"Commessa: anno " + string(li_anno_commessa) +" numero " + string(ll_num_commessa) + &
											" relativa all'ordine " + string(fl_anno_reg)+"/"+string(fl_num_reg)+" riga "+string(fl_num_riga), &
											stopsign!)		
		
		if li_risposta= -1 then
			g_mb.messagebox("Sep","Si è manifestato un errore durante la chiusura commessa ma non è stato possibile scrivere il file di log. "+&
												"Commessa: anno " + string(li_anno_commessa) +" numero " + string(ll_num_commessa) + &
												" relativa all'ordine " + string(fl_anno_reg)+"/"+string(fl_num_reg)+" riga "+string(fl_num_riga) + &
												"Questo errore non è bloccante (riguarda esclusivamente la creazione del file log "+&
												"molto probabilmente manca il parametro log).",stopsign!) 
		end if	
		
		rollback;
		destroy luo_funzioni_1
		return -1
		
	end if
	
	commit;
	destroy luo_funzioni_1
	return 1
end if 

end function

public function integer wf_carica_da_spedizioni ();long 			ll_index, ll_rows, ll_anno_reg, ll_num_reg, ll_prog_riga, ll_prog_riga_temp, ll_ret, &
				ll_index2, ll_new
string 		ls_msg, ls_sql
datastore 	lds_ordini

ll_rows = idw_righe_ordini_spedizioni.rowcount()

for ll_index=1 to ll_rows
	ll_anno_reg = idw_righe_ordini_spedizioni.getitemnumber(ll_index, "anno_registrazione")
	ll_num_reg = idw_righe_ordini_spedizioni.getitemnumber(ll_index, "num_registrazione")
	ll_prog_riga =  idw_righe_ordini_spedizioni.getitemnumber(ll_index, "prog_riga_ord_ven")
	
	
	ll_ret = wf_aggiungi_riga(ll_anno_reg, ll_num_reg, ll_prog_riga, ls_msg)
		
	if ll_ret < 0 then
		g_mb.error(ls_msg)
		//continua a processare il resto delle righe
		
//		rollback;
//		return -1
	end if
	
next

return 1


end function

public function integer wf_update_spedizione (long fl_anno_documento, long fl_num_documento, string fs_tipo_documento, ref string fs_msg);long ll_index, ll_tot, ll_progressivo
string ls_barcode

if not ib_da_spedizione then return 1
//if not isvalid(idw_righe_ordini_spedizioni_update) then return 1

ll_tot = idw_righe_ordini_spedizioni.rowcount()
for ll_index=1 to ll_tot

	ls_barcode = idw_righe_ordini_spedizioni.getitemstring(ll_index, "barcode")
	ll_progressivo = idw_righe_ordini_spedizioni.getitemnumber(ll_index, "progressivo")
	
	update tab_lista_carico_spedizioni
	set 	anno_documento=:fl_anno_documento,
			num_documento=:fl_num_documento,
			tipo_documento=:fs_tipo_documento
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				barcode=:ls_barcode and
				progressivo=:ll_progressivo;
				
	if sqlca.sqlcode<0 then
		fs_msg = "Errore in aggiornamento anno e numero documento fiscale sulla relativa spedizione ("+ls_barcode+")"
		return -1
	end if
next

return 1
end function

public function integer wf_aggiungi_righe_ordine (long al_anno_registrazione, long al_num_registrazione);/**
 * stefnaop
 * 05/01/2012
 *
 * Carico tutte le righe dell'ordine tramite il barcode della testata
 **/
 
string					ls_sql, ls_errore, ls_messaggio, ls_cod_cliente, ls_cod_tipo_ord_ven, ls_flag_tipo_bol_fat, ls_cod_parametro_blocco
long					ll_rows, ll_i, ll_prog_riga_ord_ven, ll_ret
datastore			lds_store
datetime				ldt_oggi


//*******************************************************************************
//controllo parametri blocco
select cod_cliente, cod_tipo_ord_ven
into	:ls_cod_cliente, :ls_cod_tipo_ord_ven
from tes_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:al_anno_registrazione and
			num_registrazione=:al_num_registrazione;

ldt_oggi = datetime(today(), 00:00:00)

select flag_tipo_bol_fat
into :ls_flag_tipo_bol_fat
from tab_tipi_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

if ls_flag_tipo_bol_fat="B" then
	//segue ddt
	ls_cod_parametro_blocco = "GBV"
else
	//segue fattura
	ls_cod_parametro_blocco = "GFV"
end if

ll_ret = iuo_fido.uof_get_blocco_cliente( ls_cod_cliente, ldt_oggi, ls_cod_parametro_blocco, ls_messaggio)
if ll_ret=2 then
	//blocco
	g_mb.error(ls_messaggio)
	return -1
elseif ll_ret=1 then
	//solo warning
	g_mb.warning(ls_messaggio)
end if
//*******************************************************************************



//Donato 12/02/2014
//escludo a-priori le righe collegate (optionals, addizionale, ecc...) perchè la funzione wf_aggiungi_riga(.) chiamata dopo per tutte le righe dell'ordine ne tiene già conto.
ls_sql = "SELECT prog_riga_ord_ven FROM det_ord_ven WHERE cod_azienda='"+ s_cs_xx.cod_azienda + "' "
ls_sql += " AND anno_registrazione=" + string(al_anno_registrazione) + " AND num_registrazione=" + string(al_num_registrazione)
ls_sql += " AND (num_riga_appartenenza is null or num_riga_appartenenza= 0) "
ls_sql += " ORDER BY prog_riga_ord_ven"

ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_errore)

if ll_rows < 0 then
	g_mb.error(ls_errore)
	return -1
elseif ll_rows = 0 then
	return 0
end if

for ll_i = 1 to ll_rows
	
	ll_prog_riga_ord_ven = lds_store.getitemnumber(ll_i, 1)
	
	// in teoria il controllo sulle righe non serve in quanto un ordine non può avere righe doppie
	if not wf_check_riga(al_anno_registrazione, al_num_registrazione,ll_prog_riga_ord_ven) then
		g_mb.messagebox("APICE","Questa riga d'ordine (" + string(al_anno_registrazione) + "/" + string(al_num_registrazione) + "-" + string(ll_prog_riga_ord_ven) + ") è già stata processata!",exclamation!,ok!,false,sqlca,true,this,false,false)
		//return -1
		continue
	else
		ll_ret = wf_aggiungi_riga(al_anno_registrazione, al_num_registrazione, ll_prog_riga_ord_ven, ref ls_messaggio)
		if ll_ret < 0 then
			g_mb.error(ls_messaggio)
			return -1
		end if		
	end if
	
	
next

return ll_rows
end function

public function integer wf_controlla_cliente_e_tipo_doc (integer al_anno_registrazione, long al_num_registrazione, ref string as_messaggio);string					ls_cod_cliente, ls_cod_tipo_ord_ven, ls_flag_bol_fat, ls_cod_tipo_bol_ven, ls_cod_tipo_fat_ven, ls_cod_giro, ls_cod_deposito_consegna, ls_temp
uo_produzione		luo_prod


select cod_cliente, cod_tipo_ord_ven, cod_deposito, cod_giro_consegna
into   :ls_cod_cliente, :ls_cod_tipo_ord_ven, :ls_cod_deposito_consegna, :ls_cod_giro
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in ricerca ordine " + string(al_anno_registrazione) + "/" + string(al_num_registrazione)+ ". Impossibile proseguire: "+sqlca.sqlerrtext
	return -1
end if

if g_str.isnotempty(ls_cod_giro) then
	luo_prod = create uo_produzione
	ls_temp = luo_prod.uof_deposito_giro_consegna(ls_cod_giro)
	destroy luo_prod
	
	if ls_temp<>"" then ls_cod_deposito_consegna = ls_temp
end if


//--------------------------
select flag_tipo_bol_fat, cod_tipo_bol_ven, cod_tipo_fat_ven
into   :ls_flag_bol_fat, :ls_cod_tipo_bol_ven, :ls_cod_tipo_fat_ven
from   tab_tipi_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in ricerca flag_tipo_bol_fat in tabella tipi ordini. "+sqlca.sqlerrtext
	return -1
end if

//if tab_1.tabpage_3.dw_righe_ordine.rowcount() > 0 then
if is_cod_cliente<>"" and not isnull(is_cod_cliente) then
	
	//le variabili di istanza sono state valorizzate, verifica che siano congruenti con quelle lette dall'ordine scansionato
else
	//no controlli, sei all'inizio
	//salva nelle variabili di istanza -----------------------------
	is_cod_cliente = ls_cod_cliente
	is_flag_bol_fat = ls_flag_bol_fat
	is_deposito_consegna = ls_cod_deposito_consegna
	
	if is_flag_bol_fat="F" then
		is_cod_tipo_fat_ven = ls_cod_tipo_fat_ven
		setnull(is_cod_tipo_bol_ven)
	else
		is_cod_tipo_bol_ven = ls_cod_tipo_bol_ven
		setnull(is_cod_tipo_fat_ven)
	end if
	//----------------------------------------------------------------
	
	//quindi esci dal controllo
	return 0
end if

if ls_cod_cliente <> is_cod_cliente then
	as_messaggio = "ATTENZIONE: si può evadere gli ordini di un cliente alla volta!"
	return -1
end if

if is_flag_bol_fat <> ls_flag_bol_fat then
	as_messaggio = "ATTENZIONE: il documento di destinazione '"+ls_flag_bol_fat+"' non è congruo con l'evasione corrente '"+is_flag_bol_fat+"'"
	return -1
end if

if is_flag_bol_fat="F" then
	//fattura: verifica il tipo fattura
	if is_cod_tipo_fat_ven <> ls_cod_tipo_fat_ven then
		as_messaggio = "ATTENZIONE: il tipo fattura di destinazione '"+ls_cod_tipo_fat_ven+"' non è congruo con quello dell'evasione corrente '"+is_cod_tipo_fat_ven+"'"
		return -1
	end if
else
	//bolla: verifica il tipo bolla
	if is_cod_tipo_bol_ven <> ls_cod_tipo_bol_ven then
		as_messaggio = "ATTENZIONE: il tipo bolla di destinazione '"+ls_cod_tipo_bol_ven+"' non è congruo con quello dell'evasione corrente '"+is_cod_tipo_bol_ven+"'"
		return -1
	end if
end if

//controllo congruità del deposito consegna
if is_deposito_consegna <> ls_cod_deposito_consegna then
	as_messaggio = 	"ATTENZIONE: il deposito consegna dell'ordine n° "+string(al_anno_registrazione)+"/"+string(al_num_registrazione)+" è il "+ls_cod_deposito_consegna+ &
							" e risulta differente da quello delle righe ordine selezionate finora (codice deposito "+is_deposito_consegna+") "
	return -1
end if



//se arrivi fin qui vuol dire che è tutto a posto
return 0
end function

public function longlong wf_get_prog_sessione ();longlong			ll_id
string				ls_temp

//numero 		es.			2012011109452235 (fino ai millesecondi con due cifre)
ls_temp = string(today(), "yyyymmdd") + string(now(), "hhmmssff")
ll_id = longlong(ls_temp)

return ll_id
end function

on w_evas_ordini_barcode.create
int iCurrent
call super::create
this.dw_storico=create dw_storico
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_storico
this.Control[iCurrent+2]=this.tab_1
end on

on w_evas_ordini_barcode.destroy
call super::destroy
destroy(this.dw_storico)
destroy(this.tab_1)
end on

event pc_setwindow;string ls_cod_cliente, ls_cod_tipo_ord_ven

tab_1.tabpage_1.dw_lista_bolle_pronte.settransobject(sqlca)
tab_1.tabpage_1.dw_lista_bolle_pronte.setrowfocusindicator(FocusRect!)

tab_1.tabpage_2.dw_lista_fatture_pronte.settransobject(sqlca)
tab_1.tabpage_2.dw_lista_fatture_pronte.setrowfocusindicator(FocusRect!)


iuo_fido = create uo_fido_cliente

// ---------------  in base al documento da creare attivo il folder appropriato ---------------

tab_1.tabpage_3.dw_barcode.insertrow(0)

tab_1.selecttab("tabpage_3")

long ll_ret

if s_cs_xx.parametri.parametro_b_2 then
	s_cs_xx.parametri.parametro_b_2 = false
	idw_righe_ordini_spedizioni = s_cs_xx.parametri.parametro_dw_2
	
	ll_ret = idw_righe_ordini_spedizioni.rowcount()
	
	//idw_righe_ordini_spedizioni.reset()
	
	ll_ret = s_cs_xx.parametri.parametro_dw_2.rowcount()
	
	//s_cs_xx.parametri.parametro_dw_2.RowsCopy(1, ll_ret, Primary!, idw_righe_ordini_spedizioni, 1, Primary!)
	
	//ll_ret = s_cs_xx.parametri.parametro_dw_2.rowcount()
	//ll_ret = idw_righe_ordini_spedizioni.rowcount()
	
	setnull(s_cs_xx.parametri.parametro_dw_2)
	
	//chiama una funzione di precaricamento dati righe ordini barcode
	wf_carica_da_spedizioni()
	
	ib_da_spedizione = true
end if

				

end event

event close;call super::close;

destroy iuo_fido
end event

type dw_storico from datawindow within w_evas_ordini_barcode
boolean visible = false
integer x = 3886
integer y = 2440
integer width = 686
integer height = 400
integer taborder = 70
string title = "none"
string dataobject = "d_evas_ord_ven_report_storico"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type tab_1 from tab within w_evas_ordini_barcode
integer x = 23
integer y = 24
integer width = 4773
integer height = 1952
integer taborder = 10
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
alignment alignment = center!
integer selectedtab = 3
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
end on

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4736
integer height = 1824
long backcolor = 12632256
string text = "Bolle"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_lista_bolle_pronte dw_lista_bolle_pronte
cb_nuova_bolla cb_nuova_bolla
cb_5 cb_5
cb_aggiungi_a_bolla cb_aggiungi_a_bolla
dw_ext_dati_bolla dw_ext_dati_bolla
end type

on tabpage_1.create
this.dw_lista_bolle_pronte=create dw_lista_bolle_pronte
this.cb_nuova_bolla=create cb_nuova_bolla
this.cb_5=create cb_5
this.cb_aggiungi_a_bolla=create cb_aggiungi_a_bolla
this.dw_ext_dati_bolla=create dw_ext_dati_bolla
this.Control[]={this.dw_lista_bolle_pronte,&
this.cb_nuova_bolla,&
this.cb_5,&
this.cb_aggiungi_a_bolla,&
this.dw_ext_dati_bolla}
end on

on tabpage_1.destroy
destroy(this.dw_lista_bolle_pronte)
destroy(this.cb_nuova_bolla)
destroy(this.cb_5)
destroy(this.cb_aggiungi_a_bolla)
destroy(this.dw_ext_dati_bolla)
end on

type dw_lista_bolle_pronte from datawindow within tabpage_1
integer width = 4608
integer height = 480
integer taborder = 20
string title = "none"
string dataobject = "d_lista_bolle_pronte"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type cb_nuova_bolla from commandbutton within tabpage_1
integer x = 9
integer y = 1588
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Nuovo DDT"
end type

event clicked;if g_mb.messagebox("APICE","Sei sicuro di voler creare una nuova bolla per tutte le righe assegnate di questo ordine?", Question!, YEsNo!, 2) = 1 then
	string ls_errore, ls_cod_cliente
	long   ll_anno_bolla, ll_num_bolla, ll_i, ll_ret
	uo_generazione_documenti luo_generazione_documenti
	uo_spese_trasporto luo_spese_trasporto
	
	dw_ext_dati_bolla.accepttext()
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_aspetto_beni = dw_ext_dati_bolla.getitemstring(1,"aspetto_beni")
	luo_generazione_documenti.is_cod_mezzo = dw_ext_dati_bolla.getitemstring(1,"cod_mezzo")
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_cod_porto = dw_ext_dati_bolla.getitemstring(1,"cod_porto")
	luo_generazione_documenti.is_cod_resa = dw_ext_dati_bolla.getitemstring(1,"cod_resa")
	luo_generazione_documenti.is_cod_vettore = dw_ext_dati_bolla.getitemstring(1,"cod_vettore")
	luo_generazione_documenti.is_cod_inoltro = dw_ext_dati_bolla.getitemstring(1,"cod_inoltro")
	luo_generazione_documenti.is_cod_imballo = dw_ext_dati_bolla.getitemstring(1,"cod_imballo")
	luo_generazione_documenti.is_destinazione = dw_ext_dati_bolla.getitemstring(1,"cod_destinazione")
	luo_generazione_documenti.id_num_colli = dw_ext_dati_bolla.getitemnumber(1,"num_colli")
	luo_generazione_documenti.id_peso_netto = dw_ext_dati_bolla.getitemnumber(1,"peso_netto")
	luo_generazione_documenti.id_peso_lordo = dw_ext_dati_bolla.getitemnumber(1,"peso_lordo")
	luo_generazione_documenti.idt_data_inizio_trasporto = dw_ext_dati_bolla.getitemdatetime(1,"data_inizio_trasp")
	luo_generazione_documenti.it_ora_inizio_trasporto = dw_ext_dati_bolla.getitemtime(1,"ora_inizio_trasp")
	luo_generazione_documenti.is_rag_soc_1 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_1")
	luo_generazione_documenti.is_rag_soc_2 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_2")
	luo_generazione_documenti.is_indirizzo = dw_ext_dati_bolla.getitemstring(1,"indirizzo")
	luo_generazione_documenti.is_localita = dw_ext_dati_bolla.getitemstring(1,"localita")
	luo_generazione_documenti.is_frazione = dw_ext_dati_bolla.getitemstring(1,"frazione")
	luo_generazione_documenti.is_provincia = dw_ext_dati_bolla.getitemstring(1,"provincia")
	luo_generazione_documenti.is_cap = dw_ext_dati_bolla.getitemstring(1,"cap")
	luo_generazione_documenti.is_nota_piede = dw_ext_dati_bolla.getitemstring(1,"note_piede")
	luo_generazione_documenti.is_num_ord_cliente = dw_ext_dati_bolla.getitemstring(1,"num_ord_cliente")
	luo_generazione_documenti.idt_data_ord_cliente = dw_ext_dati_bolla.getitemdatetime(1,"data_ord_cliente")
	luo_generazione_documenti.ib_visualizza_spese_trasp = false
	ll_anno_bolla = 0
	ll_num_bolla = 0
	
	if len(luo_generazione_documenti.is_cod_causale) < 1 then setnull(luo_generazione_documenti.is_cod_causale)
	if len(luo_generazione_documenti.is_cod_mezzo) < 1 then setnull(luo_generazione_documenti.is_cod_mezzo)
	if len(luo_generazione_documenti.is_cod_porto) < 1 then setnull(luo_generazione_documenti.is_cod_porto)
	if len(luo_generazione_documenti.is_cod_resa) < 1 then setnull(luo_generazione_documenti.is_cod_resa)
	if len(luo_generazione_documenti.is_cod_vettore) < 1 then setnull(luo_generazione_documenti.is_cod_vettore)
	if len(luo_generazione_documenti.is_cod_inoltro) < 1 then setnull(luo_generazione_documenti.is_cod_inoltro)
	if len(luo_generazione_documenti.is_cod_imballo) < 1 then setnull(luo_generazione_documenti.is_cod_imballo)
	if len(luo_generazione_documenti.is_destinazione) < 1 then setnull(luo_generazione_documenti.is_destinazione)

	if s_cs_xx.parametri.parametro_s_5 = "PACKLIST" then luo_generazione_documenti.is_rif_packing_list = "Rif.Packing List " + s_cs_xx.parametri.parametro_s_6
	
	//*** MICHELA 14/12/2005: SPECIFICA EUGANEA PANNELLI REPORT DDT FATTURE
	//                        solo se esiste il parametro flag aziendale 'EUG' a si allora procedo con l'ordinamento
	//                        delle righe in base alla specifica
		
	string ls_flag, ls_vuoto[]
		
	select flag
	into   :ls_flag
	from   parametri_azienda 
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'EUG';
				 
	if sqlca.sqlcode = 0  and not isnull(ls_flag) and ls_flag = 'S' and UpperBound(s_cs_xx.parametri.parametro_s_1_a) > 0 then
	
		ll_ret = luo_generazione_documenti.uof_crea_aggiungi_bolla_pack(s_cs_xx.parametri.parametro_s_1_a, il_anno_registrazione, il_num_registrazione, ref ll_anno_bolla, ref ll_num_bolla, ref ls_errore)	
		s_cs_xx.parametri.parametro_s_1_a = ls_vuoto
		
	else
			
		// 4-7-2005: aggiunta la possibilità di creare bolla da più di 1 ordine per evasione da packing list
		for ll_i = 1 to upperbound(il_anno_registrazione)
			
			//################################################################################################################
			//Donato 13/04/2012 lettura spedizioni per prog sessione e poi non viene ripulita evas_ord_ven ma messo un flag spedito a S
			ll_ret = luo_generazione_documenti.uof_crea_aggiungi_bolla_sessione(		il_anno_registrazione[ll_i], il_num_registrazione[ll_i], &
																											ref ll_anno_bolla, ref ll_num_bolla, il_prog_sessione, ref ls_errore)
			//ll_ret = luo_generazione_documenti.uof_crea_aggiungi_bolla(il_anno_registrazione[ll_i], il_num_registrazione[ll_i], ref ll_anno_bolla, ref ll_num_bolla, ref ls_errore)
			//fine modifica #########################################################################################################

			if ll_ret <> 0 then exit
		next
		
			
	end if

	//*************** fine modifica
	
	// verifico se l'uscita è dovuta ad un errore e al limite faccio rollback di tutto.
	if ll_ret <> 0 then
		rollback;
		g_mb.messagebox("APICE","Errore durante la generazione della bolla. " + ls_errore)
		setnull(s_cs_xx.parametri.parametro_d_1)
		setnull(s_cs_xx.parametri.parametro_d_2)
	else
		
		//25-03/2011 ----------------------------------------------------------------------------------------------------------------------------
		//controlla se hai generato il documento a partire da una spedizione
		if wf_update_spedizione(ll_anno_bolla, ll_num_bolla, "B", ls_errore) < 0 then
			rollback;
			g_mb.messagebox("APICE","Errore durante l'aggiornamento delle righe in spedizione (update con anno e numero bolla). " + ls_errore)
			setnull(s_cs_xx.parametri.parametro_d_1)
			setnull(s_cs_xx.parametri.parametro_d_2)
		else
		
			commit;
			
			// stefanop 04/03/2014: Visualizzo finestra dello storico spese
			luo_spese_trasporto = create uo_spese_trasporto
			
			//valori di ritorno della funzione
			//0		SPESE TRASPORTO CONFERMATE (o non abilitate dal parametro SSP), quindi non fare null'altro
			//1		SPESE TRASPORTO ANNULLATE, quindi chiama la funzione che toglie le righe trasporto e che ricalcola il documento
			//2		SPESE TRASPORTO DA SOLE PERCENTUALI IMPONIBILE RIGHE ORDINE. quindi chiama funzione opportuna
			//NOTA BENE nei casi di ritorno 1 o 2	urge fare il commit
			ll_ret = luo_spese_trasporto.uof_apri_storico(ll_anno_bolla, ll_num_bolla, "BOLVEN")
			
			if ll_ret = 0 then
			else
				if ll_ret = 1 then
					//--------------------------------------------------------------------------------------------------------------------------------------------
					//ANNULLAMENTO SPESE TRASPORTO
					ll_ret = luo_spese_trasporto.uof_azzera_trasporto_documento(ll_anno_bolla, ll_num_bolla, "BOLVEN", ls_errore)
					//--------------------------------------------------------------------------------------------------------------------------------------------
				elseif ll_ret = 2 then
					//--------------------------------------------------------------------------------------------------------------------------------------------
					//CALCOLO SPESE TRASPORTO DA SOLE PERCENTUALI IMPONIBILE RIGHE ORDINE
					ll_ret = luo_spese_trasporto.uof_imposta_trasporto_solo_percentuale(ll_anno_bolla, ll_num_bolla, "BOLVEN", ls_errore)
					//--------------------------------------------------------------------------------------------------------------------------------------------
				end if
				
				if ll_ret<0 then
					//relativamente alla sola eliminazione delle spese di trasporto o ricalcolo su sole percentuali
					rollback;
					g_mb.error(ls_errore)
					//comunque vai avanti
				else
					commit;
				end if
			end if
			
			destroy luo_spese_trasporto
			// -----------------------------
			
			g_mb.messagebox("APICE","E' stata generata la bolla " + string(ll_anno_bolla) + "-" + string(ll_num_bolla))
		end if
		
		// *** michela: se i flag di stampa rif interscambio e rif vostro cliente sono a si creo due righe
		s_cs_xx.parametri.parametro_d_1 = ll_anno_bolla
		s_cs_xx.parametri.parametro_d_2 = ll_num_bolla
		// ***		
	end if
	destroy luo_generazione_documenti
//	tab_1.postevent("clicked")

	tab_1.tabpage_1.dw_lista_bolle_pronte.retrieve(s_cs_xx.cod_azienda, is_cod_cliente)
	
	cb_5.enabled=true
end if
end event

type cb_5 from commandbutton within tabpage_1
integer x = 3845
integer y = 1588
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa Bol."
end type

event clicked;long     ll_anno_reg_mov, ll_num_reg_mov, ll_progr_stock, ll_prog_registrazione, ll_anno_registrazione, ll_num_registrazione

string   ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_nome_tabella, ls_cod_valuta, ls_test, ls_cod_documento, &
         ls_messaggio, ls_window = "w_report_bol_ven_euro"

datetime ldt_data_stock

window   lw_window

uo_calcola_documento_euro luo_calcola_documento_euro


if dw_lista_bolle_pronte.rowcount() < 1 or dw_lista_bolle_pronte.getrow() < 1 then
	g_mb.messagebox("APICE","Nessuna bolla selezionata!")
	return -1
end if

ll_anno_registrazione = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(), "tes_bol_ven_anno_registrazione")
ll_num_registrazione = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(), "tes_bol_ven_num_registrazione")

	declare 	cu_det_bol_ven cursor for
	select 	anno_reg_des_mov,
		  		num_reg_des_mov
	from 		det_bol_ven
	where 	cod_azienda = :s_cs_xx.cod_azienda and
	 			anno_registrazione = :ll_anno_registrazione and
	 			num_registrazione = :ll_num_registrazione;

open cu_det_bol_ven;

do while true
	fetch cu_det_bol_ven into :ll_anno_reg_mov, :ll_num_reg_mov;
   
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore", "Anno registrazione " + string(ll_anno_registrazione) + "  num_registrazione " + string(ll_num_registrazione) + " non esistono in DET_BOL_VEV")
		exit
	end if	

	if sqlca.sqlcode = 0 then
		declare cu_des_mov_magazzino cursor for
		 select cod_deposito,
				  cod_ubicazione,
				  cod_lotto,
				  data_stock,
				  prog_stock,
				  prog_registrazione
			from dest_mov_magazzino	  
		  where cod_azienda = :s_cs_xx.cod_azienda
			 and anno_registrazione = :ll_anno_reg_mov
			 and num_registrazione = :ll_num_reg_mov;
			 
		open cu_des_mov_magazzino;
		
		do while true
			fetch cu_des_mov_magazzino into :ls_cod_deposito, :ls_cod_ubicazione, :ls_cod_lotto, :ldt_data_stock, :ll_progr_stock, :ll_prog_registrazione;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Errore", "Non è stato trovato nessun movimento di destinazione magazzino in corrispondenza dell'anno registrazione " + string(ll_anno_reg_mov) + " e del numero registrazione " + string(ll_num_reg_mov))
				exit
			end if
			if sqlca.sqlcode = 0 then
				if isnull(ls_cod_deposito) or isnull(ls_cod_ubicazione) or isnull(ls_cod_lotto) or &
					isnull(ldt_data_stock) or isnull(ll_progr_stock) then
					g_mb.messagebox("Attenzione", "Attenzione nella tabella DES_MOV_MAGAZZINO i dati dello stock corrispondenti all'anno registrazione " + string(ll_anno_reg_mov) + ", numero registrazione " + string(ll_num_reg_mov) + " e prog. registrazione " + string(ll_prog_registrazione) + " sono incompleti")
				end if
			end if	
		loop
		
		close cu_des_mov_magazzino;
	end if
loop
close cu_det_bol_ven;

select 	cod_documento
into 		:ls_cod_documento
from 		tes_bol_ven
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore in ricerca testata bolla " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

s_cs_xx.parametri.parametro_i_1 = 0
if isnull( ls_cod_documento ) then
	window_open(w_tipo_stampa_bol_ven, 0)
	if s_cs_xx.parametri.parametro_i_1 = -1 then
		rollback;
		return -1
	end if
end if

luo_calcola_documento_euro = create uo_calcola_documento_euro
	
if luo_calcola_documento_euro.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_ven",ls_messaggio) <> 0 then
	g_mb.messagebox("APICE",ls_messaggio)
	destroy luo_calcola_documento_euro
	rollback;
	return -1
else
	commit;
end if	
	
destroy luo_calcola_documento_euro


select stringa
into   :ls_test
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'BVE';
				 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore nella select di parametri_azienda: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 then		
	s_cs_xx.parametri.parametro_d_1 = ll_anno_registrazione
	s_cs_xx.parametri.parametro_d_2 = ll_num_registrazione
	window_open(w_report_bol_ven, -1)
elseif sqlca.sqlcode = 0 then		
	s_cs_xx.parametri.parametro_d_1 = ll_anno_registrazione
	s_cs_xx.parametri.parametro_d_2 = ll_num_registrazione
	window_type_open(lw_window,ls_window, -1)
end if

setnull(s_cs_xx.parametri.parametro_d_1)
setnull(s_cs_xx.parametri.parametro_d_2)

this.enabled=false
end event

type cb_aggiungi_a_bolla from commandbutton within tabpage_1
integer x = 4233
integer y = 1588
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiungi"
end type

event clicked;if g_mb.messagebox("APICE","Sei sicuro di voler evadere tutte le righe asssegnate per questo ordine aggiungendole ad una bolla già esistente?", Question!, YEsNo!, 2) = 1 then
	
	string ls_errore, ls_des_ord, ls_des_bol, ls_rag_soc_ord, ls_ind_ord, ls_cap_ord, ls_loc_ord, ls_prov_ord,&
			 ls_rag_soc_bol, ls_ind_bol, ls_cap_bol, ls_loc_bol, ls_prov_bol
			 
	long   ll_anno_bolla, ll_num_bolla, ll_i, ll_ret
	
	uo_generazione_documenti luo_generazione_documenti
	
	
	dw_ext_dati_bolla.accepttext()
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_aspetto_beni = dw_ext_dati_bolla.getitemstring(1,"aspetto_beni")
	luo_generazione_documenti.is_cod_mezzo = dw_ext_dati_bolla.getitemstring(1,"cod_mezzo")
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_bolla.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_cod_porto = dw_ext_dati_bolla.getitemstring(1,"cod_porto")
	luo_generazione_documenti.is_cod_resa = dw_ext_dati_bolla.getitemstring(1,"cod_resa")
	luo_generazione_documenti.is_cod_vettore = dw_ext_dati_bolla.getitemstring(1,"cod_vettore")
	luo_generazione_documenti.is_cod_inoltro = dw_ext_dati_bolla.getitemstring(1,"cod_inoltro")
	luo_generazione_documenti.is_cod_imballo = dw_ext_dati_bolla.getitemstring(1,"cod_imballo")
	luo_generazione_documenti.is_destinazione = dw_ext_dati_bolla.getitemstring(1,"cod_destinazione")
	luo_generazione_documenti.id_num_colli = dw_ext_dati_bolla.getitemnumber(1,"num_colli")
	luo_generazione_documenti.id_peso_netto = dw_ext_dati_bolla.getitemnumber(1,"peso_netto")
	luo_generazione_documenti.id_peso_lordo = dw_ext_dati_bolla.getitemnumber(1,"peso_lordo")
	luo_generazione_documenti.idt_data_inizio_trasporto = dw_ext_dati_bolla.getitemdatetime(1,"data_inizio_trasp")
	luo_generazione_documenti.it_ora_inizio_trasporto = dw_ext_dati_bolla.getitemtime(1,"ora_inizio_trasp")
	luo_generazione_documenti.is_rag_soc_1 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_1")
	luo_generazione_documenti.is_rag_soc_2 = dw_ext_dati_bolla.getitemstring(1,"rag_soc_2")
	luo_generazione_documenti.is_indirizzo = dw_ext_dati_bolla.getitemstring(1,"indirizzo")
	luo_generazione_documenti.is_localita = dw_ext_dati_bolla.getitemstring(1,"localita")
	luo_generazione_documenti.is_frazione = dw_ext_dati_bolla.getitemstring(1,"frazione")
	luo_generazione_documenti.is_provincia = dw_ext_dati_bolla.getitemstring(1,"provincia")
	luo_generazione_documenti.is_cap = dw_ext_dati_bolla.getitemstring(1,"cap")
	luo_generazione_documenti.is_nota_piede = dw_ext_dati_bolla.getitemstring(1,"note_piede")
	luo_generazione_documenti.is_num_ord_cliente = dw_ext_dati_bolla.getitemstring(1,"num_ord_cliente")
	luo_generazione_documenti.idt_data_ord_cliente = dw_ext_dati_bolla.getitemdatetime(1,"data_ord_cliente")
	
	if len(luo_generazione_documenti.is_cod_causale) < 1 then setnull(luo_generazione_documenti.is_cod_causale)
	if len(luo_generazione_documenti.is_cod_mezzo) < 1 then setnull(luo_generazione_documenti.is_cod_mezzo)
	if len(luo_generazione_documenti.is_cod_porto) < 1 then setnull(luo_generazione_documenti.is_cod_porto)
	if len(luo_generazione_documenti.is_cod_resa) < 1 then setnull(luo_generazione_documenti.is_cod_resa)
	if len(luo_generazione_documenti.is_cod_vettore) < 1 then setnull(luo_generazione_documenti.is_cod_vettore)
	if len(luo_generazione_documenti.is_cod_inoltro) < 1 then setnull(luo_generazione_documenti.is_cod_inoltro)
	if len(luo_generazione_documenti.is_cod_imballo) < 1 then setnull(luo_generazione_documenti.is_cod_imballo)
	if len(luo_generazione_documenti.is_destinazione) < 1 then setnull(luo_generazione_documenti.is_destinazione)

	if dw_lista_bolle_pronte.getrow() > 0 then
		ll_anno_bolla = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(),"tes_bol_ven_anno_registrazione")
		ll_num_bolla  = dw_lista_bolle_pronte.getitemnumber(dw_lista_bolle_pronte.getrow(),"tes_bol_ven_num_registrazione")
	else
		g_mb.messagebox("APICE","Selezionare una bolla fra la lista di quelle pronte")
		destroy luo_generazione_documenti
		return
	end if
	
	select rag_soc_1,
			 indirizzo,
			 cap,
			 localita,
			 provincia
	into   :ls_rag_soc_ord,
			 :ls_ind_ord,
			 :ls_cap_ord,
			 :ls_loc_ord,
			 :ls_prov_ord
	from   tes_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_registrazione[1] and
			 num_registrazione = :il_num_registrazione[1];
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella verifica delle destinazioni~nErrore nella select di tes_ord_ven: " + sqlca.sqlerrtext)
		destroy luo_generazione_documenti
		return
	end if
	
	select rag_soc_1,
			 indirizzo,
			 cap,
			 localita,
			 provincia
	into   :ls_rag_soc_bol,
			 :ls_ind_bol,
			 :ls_cap_bol,
			 :ls_loc_bol,
			 :ls_prov_bol
	from   tes_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_bolla and
			 num_registrazione = :ll_num_bolla;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella verifica delle destinazioni~nErrore nella select di tes_bol_ven: " + sqlca.sqlerrtext)
		destroy luo_generazione_documenti
		return
	end if
	
//********************* Modifica Michele per sistemazione PTENDA 24/01/2003 *****************************
	
//	if not( &
//	(isnull(ls_rag_soc_ord) or ls_rag_soc_ord = "") and &
//	(isnull(ls_ind_ord) or ls_ind_ord = "") and &
//	(isnull(ls_cap_ord) or ls_cap_ord = "") and &
//	(isnull(ls_loc_ord) or ls_loc_ord = "") and &
//	(isnull(ls_prov_ord) or ls_prov_ord = "") &			
//	) then
	
		if (ls_rag_soc_ord <> ls_rag_soc_bol) or &  
			(isnull(ls_rag_soc_bol) and not isnull(ls_rag_soc_ord)) or &
			(not isnull(ls_rag_soc_bol) and isnull(ls_rag_soc_ord)) or &
			(ls_ind_ord <> ls_ind_bol) or &
			(isnull(ls_ind_bol) and not isnull(ls_ind_ord)) or &
			(not isnull(ls_ind_bol) and isnull(ls_ind_ord)) or &
			(ls_cap_ord <> ls_cap_bol) or &
			(isnull(ls_cap_bol) and not isnull(ls_cap_ord)) or &
			(not isnull(ls_cap_bol) and isnull(ls_cap_ord)) or &
			(ls_loc_ord <> ls_loc_bol) or &
			(isnull(ls_loc_bol) and not isnull(ls_loc_ord)) or &
			(not isnull(ls_loc_bol) and isnull(ls_loc_ord)) or &
			(ls_prov_ord <> ls_prov_bol) or &
			(isnull(ls_prov_bol) and not isnull(ls_prov_ord)) or &
			(not isnull(ls_prov_bol) and isnull(ls_prov_ord)) then
			
			if g_mb.messagebox("APICE","ATTENZIONE: la destinazione della bolla selezionata non corrisponde alla " + &
							  "destinazione dell'ordine~nSI DESIDERA CONTINUARE UGUALMENTE?",question!,yesno!,2) = 2	then
				destroy luo_generazione_documenti
				return
			end if
			
		end if
		
//	end if

//********************************* Fine Modifica Michele *********************************************


	// 4-7-2005: aggiunta la possibilità di creare bolla da più di 1 ordine per evasione da packing list
	if s_cs_xx.parametri.parametro_s_5 = "PACKLIST" then luo_generazione_documenti.is_rif_packing_list = "Rif.Packing List " + s_cs_xx.parametri.parametro_s_6

	//*** MICHELA 14/12/2005: SPECIFICA EUGANEA PANNELLI REPORT DDT FATTURE
	//                        solo se esiste il parametro flag aziendale 'EUG' a si allora procedo con l'ordinamento
	//                        delle righe in base alla specifica
		
	string ls_flag, ls_vuoto[]
		
	select flag
	into   :ls_flag
	from   parametri_azienda 
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'EUG';
				 
	if sqlca.sqlcode = 0  and not isnull(ls_flag) and ls_flag = 'S' and UpperBound(s_cs_xx.parametri.parametro_s_1_a) > 0 then
	
		ll_ret = luo_generazione_documenti.uof_crea_aggiungi_bolla_pack(s_cs_xx.parametri.parametro_s_1_a, il_anno_registrazione, il_num_registrazione, ref ll_anno_bolla, ref ll_num_bolla, ref ls_errore)	
		s_cs_xx.parametri.parametro_s_1_a = ls_vuoto
	else

		for ll_i = 1 to upperbound(il_anno_registrazione)
			
			//################################################################################################################
			//Donato 13/04/2012 lettura spedizioni per prog sessione e poi non viene ripulita evas_ord_ven ma messo un flag spedito a S
			ll_ret = luo_generazione_documenti.uof_crea_aggiungi_bolla_sessione(		il_anno_registrazione[ll_i], il_num_registrazione[ll_i], &
																											ref ll_anno_bolla, ref ll_num_bolla, il_prog_sessione, ref ls_errore)
			//ll_ret = luo_generazione_documenti.uof_crea_aggiungi_bolla(il_anno_registrazione[ll_i], il_num_registrazione[ll_i], ll_anno_bolla, ll_num_bolla, ls_errore)
			//fine modifica #########################################################################################################
			
			if ll_ret <> 0 then exit
		next
	end if
	// fine modifica 
	
	// verifico se l'uscita è per errore e al limite eseguo rollback di tutto
	if ll_ret <> 0 then
		g_mb.messagebox("APICE","Errore durante la generazione della bolla. " + ls_errore)
		destroy luo_generazione_documenti		
		rollback;
		setnull(s_cs_xx.parametri.parametro_d_1)
		setnull(s_cs_xx.parametri.parametro_d_2)		
		return
	else
		g_mb.messagebox("APICE","E' stata generata la bolla " + string(ll_anno_bolla) + "-" + string(ll_num_bolla))
		destroy luo_generazione_documenti
		commit;
		// *** michela: se i flag di stampa rif interscambio e rif vostro cliente sono a si creo due righe
		s_cs_xx.parametri.parametro_d_1 = ll_anno_bolla
		s_cs_xx.parametri.parametro_d_2 = ll_num_bolla
		// ***		
		cb_5.enabled=true
		return
	end if
	
	cb_5.enabled=true
	
end if

end event

type dw_ext_dati_bolla from datawindow within tabpage_1
integer x = 5
integer y = 488
integer width = 4603
integer height = 1080
integer taborder = 30
string title = "none"
string dataobject = "d_ext_dati_bolla_assegnazione"
boolean border = false
boolean livescroll = true
end type

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4736
integer height = 1824
long backcolor = 12632256
string text = "Fatture"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_lista_fatture_pronte dw_lista_fatture_pronte
cb_nuova_fattura cb_nuova_fattura
cb_3 cb_3
cb_aggiungi_a_fattura cb_aggiungi_a_fattura
dw_ext_dati_fattura dw_ext_dati_fattura
end type

on tabpage_2.create
this.dw_lista_fatture_pronte=create dw_lista_fatture_pronte
this.cb_nuova_fattura=create cb_nuova_fattura
this.cb_3=create cb_3
this.cb_aggiungi_a_fattura=create cb_aggiungi_a_fattura
this.dw_ext_dati_fattura=create dw_ext_dati_fattura
this.Control[]={this.dw_lista_fatture_pronte,&
this.cb_nuova_fattura,&
this.cb_3,&
this.cb_aggiungi_a_fattura,&
this.dw_ext_dati_fattura}
end on

on tabpage_2.destroy
destroy(this.dw_lista_fatture_pronte)
destroy(this.cb_nuova_fattura)
destroy(this.cb_3)
destroy(this.cb_aggiungi_a_fattura)
destroy(this.dw_ext_dati_fattura)
end on

type dw_lista_fatture_pronte from datawindow within tabpage_2
integer y = 4
integer width = 4594
integer height = 472
integer taborder = 20
string title = "none"
string dataobject = "d_lista_fatture_pronte"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type cb_nuova_fattura from commandbutton within tabpage_2
integer x = 9
integer y = 1588
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Nuova Fat."
end type

event clicked;if g_mb.messagebox("APICE","Sei sicuro di voler creare una nuova fattura per tutte le righe assegnate di questo ordine?", Question!, YEsNo!, 2) = 1 then
	string ls_errore, ls_cod_cliente
	long   ll_anno_fattura, ll_num_fattura, ll_i, ll_ret
	uo_generazione_documenti luo_generazione_documenti
	uo_spese_trasporto luo_spese_trasporto
	
	dw_ext_dati_fattura.accepttext()
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_fattura.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_aspetto_beni = dw_ext_dati_fattura.getitemstring(1,"aspetto_beni")
	luo_generazione_documenti.is_cod_mezzo = dw_ext_dati_fattura.getitemstring(1,"cod_mezzo")
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_fattura.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_cod_porto = dw_ext_dati_fattura.getitemstring(1,"cod_porto")
	luo_generazione_documenti.is_cod_resa = dw_ext_dati_fattura.getitemstring(1,"cod_resa")
	luo_generazione_documenti.is_cod_vettore = dw_ext_dati_fattura.getitemstring(1,"cod_vettore")
	luo_generazione_documenti.is_cod_inoltro = dw_ext_dati_fattura.getitemstring(1,"cod_inoltro")
	luo_generazione_documenti.is_cod_imballo = dw_ext_dati_fattura.getitemstring(1,"cod_imballo")
	luo_generazione_documenti.is_destinazione = dw_ext_dati_fattura.getitemstring(1,"cod_destinazione")
	luo_generazione_documenti.id_num_colli = dw_ext_dati_fattura.getitemnumber(1,"num_colli")
	luo_generazione_documenti.id_peso_netto = dw_ext_dati_fattura.getitemnumber(1,"peso_netto")
	luo_generazione_documenti.id_peso_lordo = dw_ext_dati_fattura.getitemnumber(1,"peso_lordo")
	luo_generazione_documenti.idt_data_inizio_trasporto = dw_ext_dati_fattura.getitemdatetime(1,"data_inizio_trasp")
	luo_generazione_documenti.it_ora_inizio_trasporto = dw_ext_dati_fattura.getitemtime(1,"ora_inizio_trasp")
	luo_generazione_documenti.is_rag_soc_1 = dw_ext_dati_fattura.getitemstring(1,"rag_soc_1")
	luo_generazione_documenti.is_rag_soc_2 = dw_ext_dati_fattura.getitemstring(1,"rag_soc_2")
	luo_generazione_documenti.is_indirizzo = dw_ext_dati_fattura.getitemstring(1,"indirizzo")
	luo_generazione_documenti.is_localita = dw_ext_dati_fattura.getitemstring(1,"localita")
	luo_generazione_documenti.is_frazione = dw_ext_dati_fattura.getitemstring(1,"frazione")
	luo_generazione_documenti.is_provincia = dw_ext_dati_fattura.getitemstring(1,"provincia")
	luo_generazione_documenti.is_cap = dw_ext_dati_fattura.getitemstring(1,"cap")
	luo_generazione_documenti.is_nota_piede = dw_ext_dati_fattura.getitemstring(1,"note_piede")
	luo_generazione_documenti.is_num_ord_cliente = dw_ext_dati_fattura.getitemstring(1,"num_ord_cliente")
	luo_generazione_documenti.idt_data_ord_cliente = dw_ext_dati_fattura.getitemdatetime(1,"data_ord_cliente")
	luo_generazione_documenti.ib_visualizza_spese_trasp = false
	ll_anno_fattura = 0
	ll_num_fattura = 0
	
	if len(luo_generazione_documenti.is_cod_causale) < 1 then setnull(luo_generazione_documenti.is_cod_causale)
	if len(luo_generazione_documenti.is_cod_mezzo) < 1 then setnull(luo_generazione_documenti.is_cod_mezzo)
	if len(luo_generazione_documenti.is_cod_porto) < 1 then setnull(luo_generazione_documenti.is_cod_porto)
	if len(luo_generazione_documenti.is_cod_resa) < 1 then setnull(luo_generazione_documenti.is_cod_resa)
	if len(luo_generazione_documenti.is_cod_vettore) < 1 then setnull(luo_generazione_documenti.is_cod_vettore)
	if len(luo_generazione_documenti.is_cod_inoltro) < 1 then setnull(luo_generazione_documenti.is_cod_inoltro)
	if len(luo_generazione_documenti.is_cod_imballo) < 1 then setnull(luo_generazione_documenti.is_cod_imballo)
	if len(luo_generazione_documenti.is_destinazione) < 1 then setnull(luo_generazione_documenti.is_destinazione)
	
	// 4-7-2005: aggiunta la possibilità di creare bolla da più di 1 ordine per evasione da packing list
	for ll_i = 1 to upperbound(il_anno_registrazione)
		
		//################################################################################################################
		//Donato 13/04/2012 lettura spedizioni per prog sessione e poi non viene ripulita evas_ord_ven ma messo un flag spedito a S
		ll_ret = luo_generazione_documenti.uof_crea_aggiungi_fattura_sessione(	il_anno_registrazione[ll_i], il_num_registrazione[ll_i], &
																										ref ll_anno_fattura, ref ll_num_fattura, il_prog_sessione, ref ls_errore)
		//ll_ret = luo_generazione_documenti.uof_crea_aggiungi_fattura(il_anno_registrazione[ll_i], il_num_registrazione[ll_i], ref ll_anno_fattura, ref ll_num_fattura, ref ls_errore)
		//fine modifica #########################################################################################################
		
		// stefanop 16/02/2012: Il numero colli che vedo nella maschera è il numero colli totale della testa ordine.
		// agli N giri successivi dello stesso ordine non devo sommare ancora quel valore
		luo_generazione_documenti.id_num_colli = 0
		// -----
		
		
		if ll_ret <> 0 then exit
	next
	// fine modifica
	
	// verifico se l'uscita è per errore
	if ll_ret <> 0 then
		rollback;
		g_mb.messagebox("APICE","Errore durante la generazione della fattura. " + ls_errore)
	else
		
		//25-03/2011 ----------------------------------------------------------------------------------------------------------------------------
		//controlla se hai generato il documento a partire da una spedizione
		if wf_update_spedizione(ll_anno_fattura, ll_num_fattura, "F", ls_errore) < 0 then
			rollback;
			g_mb.messagebox("APICE","Errore durante l'aggiornamento delle righe in spedizione (update con anno e numero fattura). " + ls_errore)
			
		else
			commit;
			
			// stefanop 04/03/2014: Visualizzo finestra dello storico spese
			luo_spese_trasporto = create uo_spese_trasporto
			
			//valori di ritorno della funzione
			//0		SPESE TRASPORTO CONFERMATE (o non abilitate dal parametro SSP), quindi non fare null'altro
			//1		SPESE TRASPORTO ANNULLATE, quindi chiama la funzione che toglie le righe trasporto e che ricalcola il documento
			//2		SPESE TRASPORTO DA SOLE PERCENTUALI IMPONIBILE RIGHE ORDINE. quindi chiama funzione opportuna
			//NOTA BENE nei casi di ritorno 1 o 2	urge fare il commit
			ll_ret = luo_spese_trasporto.uof_apri_storico(ll_anno_fattura, ll_num_fattura, "FATVEN")
			
			if ll_ret = 0 then
			else
				if ll_ret = 1 then
					//--------------------------------------------------------------------------------------------------------------------------------------------
					//ANNULLAMENTO SPESE TRASPORTO
					ll_ret = luo_spese_trasporto.uof_azzera_trasporto_documento(ll_anno_fattura, ll_num_fattura, "FATVEN", ls_errore)
					//--------------------------------------------------------------------------------------------------------------------------------------------
				elseif ll_ret = 2 then
					//--------------------------------------------------------------------------------------------------------------------------------------------
					//CALCOLO SPESE TRASPORTO DA SOLE PERCENTUALI IMPONIBILE RIGHE ORDINE
					ll_ret = luo_spese_trasporto.uof_imposta_trasporto_solo_percentuale(ll_anno_fattura, ll_num_fattura, "FATVEN", ls_errore)
					//--------------------------------------------------------------------------------------------------------------------------------------------
				end if
				
				if ll_ret<0 then
					//relativamente alla sola eliminazione delle spese di trasporto o ricalcolo su sole percentuali
					rollback;
					g_mb.error(ls_errore)
					//comunque vai avanti
				else
					commit;
				end if
			end if
			
			destroy luo_spese_trasporto
			// -----------------------------
			
			g_mb.messagebox("APICE","E' stata generata la fattura " + string(ll_anno_fattura) + "-" + string(ll_num_fattura))
		end if
		
	end if
	
	destroy luo_generazione_documenti
	
	tab_1.tabpage_2.dw_lista_fatture_pronte.retrieve(s_cs_xx.cod_azienda, is_cod_cliente)
	
	cb_3.enabled=true

end if
end event

type cb_3 from commandbutton within tabpage_2
integer x = 3835
integer y = 1588
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa Fat."
end type

event clicked;string ls_messaggio,ls_cod_cliente,ls_cod_documento
long ll_anno_registrazione,ll_num_registrazione,ll_num_documento,ll_count

uo_calcola_documento_euro luo_calcola_documento_euro

if dw_lista_fatture_pronte.rowcount() < 1 or dw_lista_fatture_pronte.getrow() < 1 then
	g_mb.messagebox("APICE","Nessuna fattura selezionata!")
	return -1
end if

s_cs_xx.parametri.parametro_d_1 = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_anno_registrazione")
s_cs_xx.parametri.parametro_d_2  = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_num_registrazione")

s_cs_xx.parametri.parametro_i_1 = 0
s_cs_xx.parametri.parametro_b_1 = FALSE

// Eventualmente richiedo le spese di trasporto se previsto
w_cs_xx_mdi.setmicrohelp("Stampa fattura in corso.....")

if dw_lista_fatture_pronte.getrow() > 0 then
	ll_anno_registrazione = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_anno_registrazione")
	ll_num_registrazione = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_num_registrazione")
	ls_cod_cliente = dw_lista_fatture_pronte.getitemstring(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_cod_cliente")
	
	select cod_documento,
			 num_documento
	into   :ls_cod_documento,
			 :ll_num_documento
	from   tes_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione;
	
	if sqlca.sqlcode = 0 then
		
		// *** controllo se ci sono addebiti
		
		select count(*)
		into   :ll_count
		from   anag_clienti_addebiti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_cliente = :ls_cod_cliente and
				 flag_mod_valore_fatture = 'S';
				 
		if not isnull(ll_count) and ll_count > 0 then		
		
			if isnull(ls_cod_documento) and ( isnull(ll_num_documento) or ll_num_documento = 0) then
				window_open(w_mod_importi_fat_ven, 0)
		
			end if	
		end if

		if isnull(ls_cod_documento) then
			window_open(w_tipo_stampa_fat_ven, 0)
			if s_cs_xx.parametri.parametro_i_1 = -1 then
				rollback;
				return -1
			end if
		end if

		luo_calcola_documento_euro = create uo_calcola_documento_euro
			
		if luo_calcola_documento_euro.uof_calcola_documento(ll_anno_registrazione ,ll_num_registrazione,"fat_ven",ls_messaggio) <> 0 then
			g_mb.messagebox("APICE",ls_messaggio)
			destroy luo_calcola_documento_euro
			rollback;
			return -1
		else
			commit;		
		end if
	
		destroy luo_calcola_documento_euro

		s_cs_xx.parametri.parametro_d_1 = ll_anno_registrazione
		s_cs_xx.parametri.parametro_d_2 = ll_num_registrazione
		
		window_open(w_report_fat_ven_euro, 0)
		
	end if
end if


parent.postevent("ue_show")

this.enabled=false
end event

type cb_aggiungi_a_fattura from commandbutton within tabpage_2
integer x = 4224
integer y = 1588
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Aggiungi"
end type

event clicked;if g_mb.messagebox("APICE","Sei sicuro di voler evadere tutte le righe asssegnate per questo ordine aggiungendole ad una fattura già esistente?", Question!, YEsNo!, 2) = 1 then
	string ls_errore
	long   ll_anno_fattura, ll_num_fattura, ll_ret, ll_i
	uo_generazione_documenti luo_generazione_documenti
	
	dw_ext_dati_fattura.accepttext()
	luo_generazione_documenti = CREATE uo_generazione_documenti
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_fattura.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_aspetto_beni = dw_ext_dati_fattura.getitemstring(1,"aspetto_beni")
	luo_generazione_documenti.is_cod_mezzo = dw_ext_dati_fattura.getitemstring(1,"cod_mezzo")
	luo_generazione_documenti.is_cod_causale = dw_ext_dati_fattura.getitemstring(1,"cod_causale")
	luo_generazione_documenti.is_cod_porto = dw_ext_dati_fattura.getitemstring(1,"cod_porto")
	luo_generazione_documenti.is_cod_resa = dw_ext_dati_fattura.getitemstring(1,"cod_resa")
	luo_generazione_documenti.is_cod_vettore = dw_ext_dati_fattura.getitemstring(1,"cod_vettore")
	luo_generazione_documenti.is_cod_inoltro = dw_ext_dati_fattura.getitemstring(1,"cod_inoltro")
	luo_generazione_documenti.is_cod_imballo = dw_ext_dati_fattura.getitemstring(1,"cod_imballo")
	luo_generazione_documenti.is_destinazione = dw_ext_dati_fattura.getitemstring(1,"cod_destinazione")
	luo_generazione_documenti.id_num_colli = dw_ext_dati_fattura.getitemnumber(1,"num_colli")
	luo_generazione_documenti.id_peso_netto = dw_ext_dati_fattura.getitemnumber(1,"peso_netto")
	luo_generazione_documenti.id_peso_lordo = dw_ext_dati_fattura.getitemnumber(1,"peso_lordo")
	luo_generazione_documenti.idt_data_inizio_trasporto = dw_ext_dati_fattura.getitemdatetime(1,"data_inizio_trasp")
	luo_generazione_documenti.it_ora_inizio_trasporto = dw_ext_dati_fattura.getitemtime(1,"ora_inizio_trasp")
	luo_generazione_documenti.is_rag_soc_1 = dw_ext_dati_fattura.getitemstring(1,"rag_soc_1")
	luo_generazione_documenti.is_rag_soc_2 = dw_ext_dati_fattura.getitemstring(1,"rag_soc_2")
	luo_generazione_documenti.is_indirizzo = dw_ext_dati_fattura.getitemstring(1,"indirizzo")
	luo_generazione_documenti.is_localita = dw_ext_dati_fattura.getitemstring(1,"localita")
	luo_generazione_documenti.is_frazione = dw_ext_dati_fattura.getitemstring(1,"frazione")
	luo_generazione_documenti.is_provincia = dw_ext_dati_fattura.getitemstring(1,"provincia")
	luo_generazione_documenti.is_cap = dw_ext_dati_fattura.getitemstring(1,"cap")
	luo_generazione_documenti.is_nota_piede = dw_ext_dati_fattura.getitemstring(1,"note_piede")
	luo_generazione_documenti.is_num_ord_cliente = dw_ext_dati_fattura.getitemstring(1,"num_ord_cliente")
	luo_generazione_documenti.idt_data_ord_cliente = dw_ext_dati_fattura.getitemdatetime(1,"data_ord_cliente")
	
	if len(luo_generazione_documenti.is_cod_causale) < 1 then setnull(luo_generazione_documenti.is_cod_causale)
	if len(luo_generazione_documenti.is_cod_mezzo) < 1 then setnull(luo_generazione_documenti.is_cod_mezzo)
	if len(luo_generazione_documenti.is_cod_porto) < 1 then setnull(luo_generazione_documenti.is_cod_porto)
	if len(luo_generazione_documenti.is_cod_resa) < 1 then setnull(luo_generazione_documenti.is_cod_resa)
	if len(luo_generazione_documenti.is_cod_vettore) < 1 then setnull(luo_generazione_documenti.is_cod_vettore)
	if len(luo_generazione_documenti.is_cod_inoltro) < 1 then setnull(luo_generazione_documenti.is_cod_inoltro)
	if len(luo_generazione_documenti.is_cod_imballo) < 1 then setnull(luo_generazione_documenti.is_cod_imballo)
	if len(luo_generazione_documenti.is_destinazione) < 1 then setnull(luo_generazione_documenti.is_destinazione)

	if dw_lista_fatture_pronte.getrow() > 0 then
		ll_anno_fattura = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_anno_registrazione")
		ll_num_fattura  = dw_lista_fatture_pronte.getitemnumber(dw_lista_fatture_pronte.getrow(),"tes_fat_ven_num_registrazione")
	else
		g_mb.messagebox("APICE","Selezionare una fattura fra la lista di quelle pronte")
	end if
	
	// 4-7-2005: aggiunta la possibilità di creare bolla da più di 1 ordine per evasione da packing list
	for ll_i = 1 to upperbound(il_anno_registrazione)
		
		//################################################################################################################
		//Donato 13/04/2012 lettura spedizioni per prog sessione e poi non viene ripulita evas_ord_ven ma messo un flag spedito a S
		ll_ret = luo_generazione_documenti.uof_crea_aggiungi_fattura_sessione(	il_anno_registrazione[ll_i], il_num_registrazione[ll_i], &
																										ref ll_anno_fattura, ref ll_num_fattura, il_prog_sessione, ref ls_errore)
																										
		//ll_ret = luo_generazione_documenti.uof_crea_aggiungi_fattura(il_anno_registrazione[ll_i], il_num_registrazione[ll_i], ref ll_anno_fattura, ref ll_num_fattura, ref ls_errore)
		//fine modifica #########################################################################################################
		
		// stefanop 16/02/2012: Il numero colli che vedo nella maschera è il numero colli totale della testa ordine.
		// agli N giri successivi dello stesso ordine non devo sommare ancora quel valore
		luo_generazione_documenti.id_num_colli = 0
		// -----
		
		if ll_ret <> 0 then exit
	next
	// fine modifica
	
	// verifico se l'uscita è per errore e al limite faccio rollback di tutto.
	if ll_ret <> 0 then
		g_mb.messagebox("APICE","Errore durante la generazione della fattura. " + ls_errore)
		destroy luo_generazione_documenti
		rollback;
		return
	else
		g_mb.messagebox("APICE","E' stata generata la fattura " + string(ll_anno_fattura) + "-" + string(ll_num_fattura))
		destroy luo_generazione_documenti
		commit;
		cb_3.enabled=true
		return
	end if
	
	cb_3.enabled=true
	
end if


end event

type dw_ext_dati_fattura from datawindow within tabpage_2
integer x = 5
integer y = 488
integer width = 4594
integer height = 1080
integer taborder = 20
string title = "none"
string dataobject = "d_ext_dati_bolla_assegnazione"
boolean border = false
boolean livescroll = true
end type

type tabpage_3 from userobject within tab_1
event ue_carica_righe_ordine ( )
integer x = 18
integer y = 112
integer width = 4736
integer height = 1824
long backcolor = 12632256
string text = "Righe Ordine"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_barcode dw_barcode
st_1 st_1
cb_1 cb_1
cb_conferma cb_conferma
cb_deseleziona cb_deseleziona
cb_seleziona cb_seleziona
dw_righe_ordine dw_righe_ordine
end type

on tabpage_3.create
this.dw_barcode=create dw_barcode
this.st_1=create st_1
this.cb_1=create cb_1
this.cb_conferma=create cb_conferma
this.cb_deseleziona=create cb_deseleziona
this.cb_seleziona=create cb_seleziona
this.dw_righe_ordine=create dw_righe_ordine
this.Control[]={this.dw_barcode,&
this.st_1,&
this.cb_1,&
this.cb_conferma,&
this.cb_deseleziona,&
this.cb_seleziona,&
this.dw_righe_ordine}
end on

on tabpage_3.destroy
destroy(this.dw_barcode)
destroy(this.st_1)
destroy(this.cb_1)
destroy(this.cb_conferma)
destroy(this.cb_deseleziona)
destroy(this.cb_seleziona)
destroy(this.dw_righe_ordine)
end on

type dw_barcode from datawindow within tabpage_3
event ue_key pbm_dwnkey
integer x = 837
integer y = 1608
integer width = 1257
integer height = 108
integer taborder = 70
string title = "none"
string dataobject = "d_evas_ord_ven_barcode"
boolean border = false
boolean livescroll = true
end type

event ue_key;choose case key
	case keyenter!
		string ls_barcode,ls_anno_registrazione,ls_num_registrazione,ls_prog_riga_ord_ven,ls_messaggio
		long   ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven, ll_ret
		
		ls_barcode = gettext( )
		ls_barcode = upper(ls_barcode)
		
		// stefanop: 05/01/2012: ora il barcode può essere sparato anche solo per la testa dell'ordine
		// questo permette di poter caricare tutte le righe in un sol colpo
		if len(ls_barcode) < 11 then
		//if len(ls_barcode) <> 15 then
			g_mb.messagebox("APICE","Formato BARCODE errato")
			dw_barcode.setfocus()
			dw_barcode.SelectText(1,999999)
			return
		end if
		
		ls_anno_registrazione = mid(ls_barcode,2,4)
		ls_num_registrazione  = mid(ls_barcode,6,6)
		if len(ls_barcode) > 11 then
			ls_prog_riga_ord_ven  = mid(ls_barcode,12,4)
		else
			ls_prog_riga_ord_ven = "0"
		end if
			
		
		ll_anno_registrazione = long(ls_anno_registrazione)
		ll_num_registrazione = long(ls_num_registrazione)
		ll_prog_riga_ord_ven = long(ls_prog_riga_ord_ven)
		
		if len(ls_barcode) = 11 then
			
			// carico tutte le righe dell'ordine
			
			// attenzione, pulisco le righe lette in precedenza perchè sto caricando tutte le righe lette da una testa
			// Pulze 14-2-2012 : commentato su richiesta di Gibus
			// tab_1.tabpage_3.dw_righe_ordine.reset()
			
			wf_aggiungi_righe_ordine(ll_anno_registrazione, ll_num_registrazione)
			
			dw_barcode.setfocus()
			dw_barcode.SelectText(1,999999)
			
		else
			// aggiungo solo la riga letta: funzionamento solito
			if not wf_check_riga(ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven) then
				g_mb.messagebox("APICE","Questa riga d'ordine (" + ls_anno_registrazione + "/" + ls_num_registrazione + "-" + ls_prog_riga_ord_ven + ") è già stata processata!",exclamation!,ok!,false,sqlca,true,this,false,false)
				this.setfocus()
				this.selecttext(1,999999)
				return
			end if
			
			ll_ret = wf_aggiungi_riga(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ref ls_messaggio)
			if ll_ret < 0 then
				g_mb.messagebox("APICE",ls_messaggio)
				dw_barcode.setfocus()
				dw_barcode.SelectText(1,999999)
				rollback;
				return
			end if
			dw_barcode.setfocus()
			dw_barcode.SelectText(1,999999)
		end if
		
end choose

tab_1.tabpage_3.dw_righe_ordine.event post ue_posiziona_ultima_riga()
end event

type st_1 from statictext within tabpage_3
integer x = 2135
integer y = 1624
integer width = 1678
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "SHIFT-F1 = SELEZIONA TUTTO     SHIFT-F2 = DESELEZIONA TUTTO"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within tabpage_3
boolean visible = false
integer x = 393
integer y = 1608
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla Sped."
end type

event clicked;//messo invisibile questo pulsante che fa solo casini
//Se si vuole annullare una spedizione, chiudere la finestra, e faere azzera assegnazione dalla finestra dell'ordine/i
//
//integer li_return
//long ll_i
//string ls_perc, ls_messaggio
//uo_generazione_documenti luo_gen_doc
//
//for ll_i = 1 to dw_righe_ordine.rowcount()
//	ls_perc = string(dw_righe_ordine.getitemnumber(ll_i, "prog_riga_ord_ven"))
//	setnull(ls_messaggio)
//	
//	luo_gen_doc = create uo_generazione_documenti
//	li_return = luo_gen_doc.uof_azz_ass_ord_ven(il_anno_registrazione[1], il_num_registrazione[1], ls_perc, ls_messaggio)
//	destroy luo_gen_doc
//
//	
//	if not isnull(ls_messaggio) then 
//		g_mb.messagebox("Azzeramento assegnazione",ls_messaggio, information!)
//	end if
//	
//	if li_return = 0 then
//		g_mb.messagebox("Attenzione", "Azzeramento Assegnazione non avvenuta.", exclamation!)
//		rollback;
//		return
//	end if
//	g_mb.messagebox("Informazione", "Azzeramento Assegnazione avvenuta con successo.", information!)
//next
//commit;
//
end event

type cb_conferma from commandbutton within tabpage_3
integer x = 5
integer y = 1608
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Spedisci"
end type

event clicked;string					ls_cod_tipo_bol_ven, ls_cod_deposito, ls_cod_ubicazione, ls_cod_tipo_det_ven, ls_cod_tipo_ord_ven, ls_null,&
						ls_cod_cliente, ls_parametro, ls_messaggio,ls_flag_urgente, ls_nota_testata, ls_nota_piede, ls_nota_video, &
						ls_cod_tipo_fat_ven, ls_des_prodotto, ls_cod_fornitore, ls_cod_cliente_old, ls_flag_aut_sblocco,ls_flag_tipo_bol_fat, ls_cod_dep_giro
			
integer				li_return

long					ll_return, ll_i, ll_prog_riga_ord_ven, ll_ret, ll_cont, ll_ind, ll_anno_registrazione, ll_num_registrazione, &
						ll_vuoto[],ll_canc[], ll_storico, ll_num_riga_appartenenza
			
decimal				ld_quan_commessa, ld_quan_evasa, ld_quan_in_evasione

datetime				ldt_data_registrazione
uo_fido_cliente		l_uo_fido_cliente
uo_generazione_documenti luo_gen_doc
uo_produzione		luo_prod
uo_log_sistema		luo_log


ldt_data_registrazione = datetime(today(),00:00:00)
setnull(ls_cod_fornitore)
setnull(ls_null)

dw_righe_ordine.accepttext()

//genero un progressivo sessione univoco #########################
il_prog_sessione = wf_get_prog_sessione()
//#################################################

ll_return = g_mb.messagebox("APICE","Vuoi assegnare le righe selezionate ?", Question!, YesNo!, 1)
if ll_return = 2 then return

dw_righe_ordine.setsort("anno_registrazione, num_registrazione, prog_riga_ord_ven")
dw_righe_ordine.sort()

ll_cont = 0
il_anno_registrazione[] = ll_vuoto[]
il_num_registrazione[]  = ll_vuoto[]

//Donato 19-11-2008 prima di fare tutto, verifica l'esposizione e lo scaduto del cliente

l_uo_fido_cliente = create uo_fido_cliente
ls_cod_cliente_old = ""

for ll_i = 1 to dw_righe_ordine.rowcount()
	ll_anno_registrazione = dw_righe_ordine.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione  = dw_righe_ordine.getitemnumber(ll_i, "num_registrazione")
	
	select cod_cliente,flag_aut_sblocco_fido,cod_tipo_ord_ven
	into :ls_cod_cliente, :ls_flag_aut_sblocco, :ls_cod_tipo_ord_ven
	from tes_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda
		and anno_registrazione = :ll_anno_registrazione and num_registrazione = :ll_num_registrazione;
		
	if sqlca.sqlcode = 100 then
		g_mb.messagebox("APICE","Ordine non trovato:" + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) )
	end if
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in ricerca ordine:" + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "~r~n" + sqlca.sqlerrtext )
	end if
	
	if ls_cod_cliente <> ls_cod_cliente_old then
	
		// visualizzo la nota fissa VIDEO se presente
		select flag_tipo_bol_fat, 
				cod_tipo_bol_ven,
				cod_tipo_fat_ven
		into  	:ls_flag_tipo_bol_fat, 
				:ls_cod_tipo_bol_ven,
				:ls_cod_tipo_fat_ven				
		from   tab_tipi_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		if sqlca.sqlcode = 100 then
			g_mb.messagebox("APICE","Tipo Ordine non trovato:" + ls_cod_tipo_ord_ven)
		end if
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore in ricerca tipo ordine:" + ls_cod_tipo_ord_ven + "~r~n" + sqlca.sqlerrtext)
		end if
		
		choose case ls_flag_tipo_bol_fat
			case "F"
				if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "FAT_VEN", ls_cod_tipo_fat_ven, ldt_data_registrazione, ls_nota_testata,ls_nota_piede, ls_nota_video) < 0 then
					g_mb.error(ls_nota_testata)
				else
					if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA", ls_nota_video)
				end if
				
				if not isnull(ls_nota_video) and len(ls_nota_video) > 0 then
					g_mb.warning(ls_nota_video)
				end if
				
				ls_cod_cliente_old = ls_cod_cliente
				
			case "B"
		
				if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "BOL_VEN", ls_cod_tipo_bol_ven, ldt_data_registrazione, ls_nota_testata,ls_nota_piede, ls_nota_video) < 0 then
					g_mb.error(ls_nota_testata)
				else
					if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA", ls_nota_video)
				end if

				if not isnull(ls_nota_video) and len(ls_nota_video) > 0 then
					g_mb.warning(ls_nota_video)
				end if

				ls_cod_cliente_old = ls_cod_cliente
		end choose
		
	end if

	
	if ls_cod_cliente <> "" and not isnull(ls_cod_cliente) then
		l_uo_fido_cliente.is_flag_autorizza_sblocco = ls_flag_aut_sblocco
		
		ll_return = l_uo_fido_cliente.uof_check_cliente(ls_cod_cliente,datetime(today(),00:00:00),ls_messaggio)
		
		if ll_return = -1 then
			g_mb.messagebox("APICE","Errore in verifica esposizione cliente.~n" + ls_messaggio,exclamation!)
			return
		end if
		
		if ll_return = 2 then
			//blocca perchè non sono autorizzato
			g_mb.messagebox("APICE","L'ordine bloccante è il numero: "+string(ll_anno_registrazione)+"/"+string(ll_num_registrazione),Information!)
			return
		end if
	end if
next

destroy l_uo_fido_cliente
//fine modifica ---------------------------

// ri-allineamento flag_urgente fra maschera evasione e det_ord_ven
// {
	
for ll_i = 1 to dw_righe_ordine.rowcount()
	
	ll_anno_registrazione = dw_righe_ordine.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione  = dw_righe_ordine.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_ord_ven = dw_righe_ordine.getitemnumber(ll_i, "prog_riga_ord_ven")
	ls_flag_urgente		= dw_righe_ordine.getitemstring(ll_i, "flag_urgente")
	ll_num_riga_appartenenza = dw_righe_ordine.getitemnumber(ll_i, "num_riga_appartenenza")
	
	if ls_flag_urgente = "S" then
		
		// se si storicizza una riga "padre", le sue righe di riferimento vengono slegate in
		// modo che possano essere storicizzate in modo indipendente
		
		update
			det_ord_ven
		set
			num_riga_appartenenza = null
		where
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			num_riga_appartenenza = :ll_prog_riga_ord_ven;
			
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in allineamento riga appartenenza in riga ordine",stopsign!,ok!,true,sqlca,true,this,false,false)
			rollback;
			return
		end if
		
	end if
	
	// il flag urgente deve essere indipendente per ogni riga, quindi non si allineano più
	// anche le righe di riferimento, ma solo la riga corrente
	
	update
		det_ord_ven
	set
		flag_urgente = :ls_flag_urgente
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione and
		prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in allineamento flag_urgente in riga ordine",stopsign!,ok!,true,sqlca,true,this,false,false)
		rollback;
		return
	end if
	
next
	
// }

for ll_i = 1 to dw_righe_ordine.rowcount()
	if ll_cont = 0 then
		ll_cont ++
		il_anno_registrazione[ll_cont] = dw_righe_ordine.getitemnumber(ll_i, "anno_registrazione")
		il_num_registrazione[ll_cont] = dw_righe_ordine.getitemnumber(ll_i, "num_registrazione")
	else
		if il_anno_registrazione[ll_cont] <> dw_righe_ordine.getitemnumber(ll_i, "anno_registrazione") or il_num_registrazione[ll_cont] <> dw_righe_ordine.getitemnumber(ll_i, "num_registrazione") then
			ll_cont ++
			il_anno_registrazione[ll_cont] = dw_righe_ordine.getitemnumber(ll_i, "anno_registrazione")
			il_num_registrazione[ll_cont] = dw_righe_ordine.getitemnumber(ll_i, "num_registrazione")
			
			select cod_deposito
			into   :ls_cod_deposito
			from   tes_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :il_anno_registrazione[ll_cont] and
					 num_registrazione = :il_num_registrazione[ll_cont];
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in ricerca codice deposito in testata ordine. " + string(il_anno_registrazione[ll_cont]) + "/" + string(il_num_registrazione[ll_cont]) + "~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
		end if
	end if
next

setnull(ls_cod_ubicazione)

// *************************  INIZIO CON STORICIZZAZIONE DELLE RIGHE URGENTI *****************************
// cancello dalla lista delle evasioni le righe storicizzate
//for ll_i = 1 to upperbound(ll_canc)
//	dw_righe_ordine.deleterow(ll_canc[ll_i])
//next

// rimetto in ordine le righe rimanenti.
dw_righe_ordine.setsort("anno_registrazione, num_registrazione, prog_riga_ord_ven")
dw_righe_ordine.sort()

ll_cont = 0
il_anno_registrazione[] = ll_vuoto[]
il_num_registrazione[]  = ll_vuoto[]

for ll_i = 1 to dw_righe_ordine.rowcount()
	if ll_cont = 0 then
		ll_cont ++
		il_anno_registrazione[ll_cont] = dw_righe_ordine.getitemnumber(ll_i, "anno_registrazione")
		il_num_registrazione[ll_cont] = dw_righe_ordine.getitemnumber(ll_i, "num_registrazione")
	else
		if il_anno_registrazione[ll_cont] <> dw_righe_ordine.getitemnumber(ll_i, "anno_registrazione") or il_num_registrazione[ll_cont] <> dw_righe_ordine.getitemnumber(ll_i, "num_registrazione") then
			ll_cont ++
			il_anno_registrazione[ll_cont] = dw_righe_ordine.getitemnumber(ll_i, "anno_registrazione")
			il_num_registrazione[ll_cont] = dw_righe_ordine.getitemnumber(ll_i, "num_registrazione")
		end if
	end if
next

// ************************** CONTINUO CON L'ASSEGNAZIONE DELLE RIMANENTI  ***************************************


//Donato 13/04/2012
//COMMENTATO PERCHè GESTIAMO TUTTO MEDIANTE UN ID SESSIONE UNIVOCO GENERATO AL CLICKED DEL PULSANTE SPEDISCI

////Donato 05/04/2012
////puizia tabella evas_ord_ven. prima di inziare la spedizione (solo righe eventualmente "marcate" dall'utente in precedenti operazioni interrotte...)
//delete from evas_ord_ven
//where 	cod_azienda=:s_cs_xx.cod_azienda and
//			cod_utente=:s_cs_xx.cod_utente;
//
//if sqlca.sqlcode<0 then
//	g_mb.error("APICE","Errore in pulizia tabella spedizioni per l'utente: " + sqlca.sqlerrtext)
//	rollback;
//	return
//end if
//-----------------------------------------------------------------------------------------------------------------------------------

luo_gen_doc = create uo_generazione_documenti

for ll_i = 1 to dw_righe_ordine.rowcount()
	
	if dw_righe_ordine.getitemstring(ll_i, "flag_urgente") = "S" then continue
	
	if  dw_righe_ordine.getitemnumber(ll_i, "quan_da_evadere") > 0 then	
		ld_quan_evasa = 0
		ld_quan_commessa = 0
		ls_cod_tipo_det_ven   = dw_righe_ordine.getitemstring(ll_i, "cod_tipo_det_ven")
		ll_anno_registrazione = dw_righe_ordine.getitemnumber(ll_i, "anno_registrazione")
		ll_num_registrazione  = dw_righe_ordine.getitemnumber(ll_i, "num_registrazione")
		ll_prog_riga_ord_ven  = dw_righe_ordine.getitemnumber(ll_i, "prog_riga_ord_ven")
		
		
		select cod_deposito
		into   :ls_cod_deposito
		from   tes_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode <> 0 or isnull(ls_cod_deposito) then
			g_mb.messagebox("APICE","Errore in ricerca codice deposito in testata ordine. " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "~r~n" + sqlca.sqlerrtext)
			destroy luo_gen_doc;
			rollback;
			return
		end if
		
		
		//----------------------------------------------------------------------------------
		//se l'ordine ha un giro consegna con deposito applicato e diverso da quello oriogine ordine, allora il deposito da assegnare deve essere questo
		luo_prod = create uo_produzione
		ls_cod_dep_giro = luo_prod.uof_deposito_giro_consegna(ll_anno_registrazione, ll_num_registrazione)
		destroy luo_prod
		
		if g_str.isnotempty(ls_cod_dep_giro) and ls_cod_deposito<>ls_cod_dep_giro then
			ls_cod_deposito = ls_cod_dep_giro
			
			luo_log = create uo_log_sistema
			luo_log.uof_write_log_sistema_not_sqlca("DEP.CONS.ASSEGN", "Applicato deposito codice "+ls_cod_dep_giro+" del  giro consegna dell'ordine "+string(ll_anno_registrazione)+"/" + string(ll_num_registrazione)+&
																							" al posto del deposito origine "+ls_cod_deposito+" in assegnazione riga ordine n. "+string(ll_prog_riga_ord_ven) + &
																							" da pulsante SPEDISCI finestra evasione con barcode.")
			destroy luo_log
		end if
		//----------------------------------------------------------------------------------
		
		
		li_return = luo_gen_doc.uof_ass_ord_ven(	ll_anno_registrazione, &
																ll_num_registrazione, &
																ll_prog_riga_ord_ven, &
																ls_cod_tipo_det_ven, &
																ls_cod_deposito, &
																ls_cod_ubicazione, &
																dw_righe_ordine.getitemstring(ll_i, "cod_prodotto"), &
																dw_righe_ordine.getitemnumber(ll_i, "quan_da_evadere"), &
																0, &
																0, &
																il_prog_sessione)

		
		//*******************STEFANO P. 05/08/08 MODIFICATO LL_NUM_REGISTRAZIONE DA IL_NUM_REGISTRAZION[1] ERRATO
		if li_return = 3 then
			g_mb.messagebox("APICE","Ordine n. " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " riga nr." + string(ll_prog_riga_ord_ven) + "~r~nNon è stato trovata suffiente quantità negli STOCK per evadere l'ordine: operazione totalmente annullata!", stopsign! )
			destroy luo_gen_doc;
			rollback;
			return
		end if
		//*************05/08/08 STEFANO P. FINE MODIFICA
		
		if li_return = 200 then
			//messaggio già dato, esci e blocca tutto: possibili doppie righe in fattura
			destroy luo_gen_doc;
			rollback;
			return
		end if
		
		//Donato 03-12-2008 chiusura commessa associata alla riga di dettaglio dell'ordine
		li_return = wf_chiudi_commessa(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven)
		if li_return = -1 then
			//messaggio già dato dalla funzione
			destroy luo_gen_doc;
			rollback;
			return
		end if
		//fine modifica ----------------------------------------------------------------------------------------
	end if
next

destroy luo_gen_doc;

commit;

// attenzione: il destroy deve avvenire DOPO del COMMIT, altrimenti perdo i dati
destroy iuo_storicizzazione

g_mb.messagebox("APICE","Assegnazione delle righe selezionate eseguita con successo!")

// ----------------------------- riaggiorno righe ordine ------------------------------------------------------

tab_1.tabpage_3.dw_righe_ordine.reset()

// ----------------------------- attivo la maschera di input dati bolla o fattura -----------------------------

wf_seleziona_tab()

return
end event

type cb_deseleziona from commandbutton within tabpage_3
integer x = 3845
integer y = 1612
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Deseleziona"
end type

event clicked;long ll_i

for ll_i = 1 to dw_righe_ordine.rowcount()
	dw_righe_ordine.setitem(ll_i, "flag_evasione_totale", "N")
	dw_righe_ordine.setitem(ll_i, "quan_da_evadere", 0)
next
end event

type cb_seleziona from commandbutton within tabpage_3
integer x = 4233
integer y = 1612
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Sel.Tutto"
end type

event clicked;long ll_i

for ll_i = 1 to dw_righe_ordine.rowcount()
	dw_righe_ordine.setitem(ll_i, "flag_evasione_totale", "S")
	dw_righe_ordine.setitem(ll_i, "quan_da_evadere", dw_righe_ordine.getitemnumber(ll_i, "quan_residua"))
next
dw_righe_ordine.accepttext()
end event

type dw_righe_ordine from datawindow within tabpage_3
event ue_key pbm_dwnkey
event ue_flag_urgente ( long al_prog_riga,  string as_flag )
event ue_posiziona_ultima_riga ( )
integer y = 8
integer width = 4603
integer height = 1580
integer taborder = 10
string dataobject = "d_lista_righe_ordine_barcode"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_key;if key = keyf1! and keyflags = 1 then
	cb_seleziona.triggerevent("Clicked")
end if
if key = keyF2! and keyflags = 1 then
	cb_deseleziona.triggerevent("Clicked")
end if

			
end event

event ue_flag_urgente(long al_prog_riga, string as_flag);long ll_i
			
for ll_i = 1 to rowcount()
	if getitemnumber(ll_i,"num_riga_appartenenza") = al_prog_riga then
		setitem(ll_i,"flag_urgente",as_flag)
	end if
next

end event

event ue_posiziona_ultima_riga();scrolltorow( rowcount() )
end event

event itemchanged;if isnull(dwo.name) then return
choose case dwo.name
	case "flag_evasione_totale"
		if data = "S" then
			setitem(row, "quan_da_evadere", getitemnumber(row,"quan_residua"))
		else
			setitem(row, "quan_da_evadere", 0)
		end if
	case "quan_da_evadere"
		if dec(data) < getitemnumber(row,"quan_residua") then
			setitem(row,"flag_evasione_totale","N")
		else
			setitem(row,"flag_evasione_totale","S")
		end if
	case "flag_urgente"
		
		// *** TUTTO DISABILITATO: il flag_urgente è ora indipendente per ogni riga (Viropa 23/11/2007)
//		if getitemnumber(row,"num_riga_appartenenza") > 0 then
//			g_mb.messagebox("APICE","Non è possibile modificare il flag urgente per una riga di riferimento!",exclamation!,ok!,false,sqlca,true,this,false,false)
//			return 1
//		else
//			
//			// le righe di appartenenza devono seguire il valore di flag_urgente della riga "padre"
//			event post ue_flag_urgente(getitemnumber(row,"prog_riga_ord_ven"),data)
//			
//		end if
		
end choose
end event

