﻿$PBExportHeader$w_evas_ordini_barcode_trasf.srw
forward
global type w_evas_ordini_barcode_trasf from w_cs_xx_principale
end type
type tab_1 from tab within w_evas_ordini_barcode_trasf
end type
type det_1 from userobject within tab_1
end type
type dw_1 from uo_std_dw within det_1
end type
type det_1 from userobject within tab_1
dw_1 dw_1
end type
type det_2 from userobject within tab_1
end type
type cb_passo_2 from commandbutton within det_2
end type
type dw_barcode from uo_std_dw within det_2
end type
type dw_righe_ordine from uo_std_dw within det_2
end type
type det_2 from userobject within tab_1
cb_passo_2 cb_passo_2
dw_barcode dw_barcode
dw_righe_ordine dw_righe_ordine
end type
type det_3 from userobject within tab_1
end type
type cb_genera_ddt from commandbutton within det_3
end type
type dw_dati_bolla from uo_std_dw within det_3
end type
type det_3 from userobject within tab_1
cb_genera_ddt cb_genera_ddt
dw_dati_bolla dw_dati_bolla
end type
type tab_1 from tab within w_evas_ordini_barcode_trasf
det_1 det_1
det_2 det_2
det_3 det_3
end type
end forward

global type w_evas_ordini_barcode_trasf from w_cs_xx_principale
integer width = 4558
integer height = 2064
string title = "Creazione Bolle Trasferimento da Ordini"
event ue_filtra_deposito_partenza ( )
tab_1 tab_1
end type
global w_evas_ordini_barcode_trasf w_evas_ordini_barcode_trasf

type variables
CONSTANT long COLORE_ERRORE = 11184895
CONSTANT long COLORE_ELABORAZIONE = 16765864
CONSTANT long COLORE_NEUTRO = 16777215

private:
	string is_cod_deposito_partenza	// il deposito dell'utente collegato
	string is_cod_reparti_partenza[] 	// reparto dell'utente collegato
	string is_cod_deposito_arrivo		// deposito dove traferire la mercie
	integer ii_current_step = -1
	
	boolean ib_da_spedizione
	
	string is_cod_cliente

end variables

forward prototypes
public subroutine wf_leggi_barcode (string as_barcode)
public function integer wf_carica_riga_ordine (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_error)
public function integer wf_controlla_deposito (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_cod_prodotto, string as_cod_versione, string as_cod_deposito_ord_ven, ref string as_error)
public function boolean wf_step (integer ai_step, boolean ab_fire_event)
public function boolean wf_step (integer ai_step)
public function integer wf_get_reparti_deposito (string as_cod_deposito, ref string as_cod_reparti[], ref string as_errore)
public subroutine wf_step_3 ()
public function integer wf_genera_ddt ()
public subroutine wf_log_barcode (long al_colore, string as_messaggio)
public function integer wf_carica_ordine (long al_anno_registrazione, long al_num_registrazione, ref string as_messaggio)
public function boolean wf_check_riga (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga, string as_cod_prodotto)
public function integer wf_carica_righe_collegate (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ordine, string as_cod_prodotto, ref string as_errore)
public function integer wf_stampa_ddt (long al_anno_bolla, long al_num_bolla, ref string as_messaggio)
end prototypes

event ue_filtra_deposito_partenza();/**
 * stefanop
 * 27/01/2012
 *
 * Filtro il deposito di partenza in abase al deposito dell'utente collegato
 **/
 
// recupero stabilimento e deposito
string ls_error, ls_cod_deposito_utente, ls_filter
DataWindowChild ldw_child

guo_functions.uof_get_stabilimento_utente(ls_cod_deposito_utente, ls_error)
if isnull(ls_cod_deposito_utente) or len(ls_cod_deposito_utente) < 1 then
	return
end if

// filtro solo depositi dell'utente
ls_filter = "data_column like '" + left(ls_cod_deposito_utente, 1) + "%'"
tab_1.det_1.dw_1.getchild("cod_deposito_partenza", ref ldw_child)

if ldw_child.setfilter(ls_filter) < 1 then g_mb.error("Errore durante l'impostazione del filtro")
if ldw_child.filter() < 1 then g_mb.error("Errore drante il filter della dw")

// nascondo i filtri dell'utente
ls_filter = "data_column not like '" + left(ls_cod_deposito_utente, 1) + "%'"
tab_1.det_1.dw_1.getchild("cod_deposito_finale", ref ldw_child)

if ldw_child.setfilter(ls_filter) < 1 then g_mb.error("Errore durante l'impostazione del filtro")
if ldw_child.filter() < 1 then g_mb.error("Errore drante il filter della dw")

tab_1.det_1.dw_1.setitem(1, "cod_deposito_partenza", ls_cod_deposito_utente)


end event

public subroutine wf_leggi_barcode (string as_barcode);/**
 * stefnaop
 * 25/01/2012
 *
 * Processo il barcode selezionato
 **/
 
string ls_error
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_result

wf_log_barcode(COLORE_NEUTRO, "")

if len(as_barcode) < 11 then
	wf_log_barcode(COLORE_ERRORE, "Formato BARCODE errato")
	tab_1.det_2.dw_barcode.setfocus()
	tab_1.det_2.dw_barcode.SelectText(1,999999)
	return
end if

ll_anno_registrazione = long(mid(as_barcode,2,4))
ll_num_registrazione = long(mid(as_barcode,6,6))

if len(as_barcode) > 11 then
	ll_prog_riga_ord_ven  = long(mid(as_barcode,12,4))
	
	// carico la singola riga
	ll_result = wf_carica_riga_ordine(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_error)
else
	// DISABILITATA PER IL MOMENTO
	wf_log_barcode(COLORE_ELABORAZIONE, "Funzione momentaneamente disabilitata.~r~nProcessare una riga alla volta.")
	tab_1.det_2.dw_barcode.setfocus()
	tab_1.det_2.dw_barcode.SelectText(1,999999)
	return 
	// ----
	
	ll_prog_riga_ord_ven = 0
	tab_1.det_2.dw_righe_ordine.reset()
	
	// carico tutto l'ordine
	ll_result = wf_carica_ordine(ll_anno_registrazione, ll_num_registrazione, ls_error)
end if

if ll_result = -1 then
	wf_log_barcode(COLORE_ERRORE, ls_error)
else
	wf_log_barcode(COLORE_NEUTRO, "")
end if

tab_1.det_2.dw_barcode.setfocus()
tab_1.det_2.dw_barcode.SelectText(1,999999)

tab_1.det_2.dw_righe_ordine.event post ue_posiziona_ultima_riga()

end subroutine

public function integer wf_carica_riga_ordine (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_error);string					ls_cod_prodotto, ls_cod_tipo_det_ven, ls_cod_misura, ls_des_prodotto, ls_cod_misura_mag, ls_flag_urgente, ls_cod_versione, &
						ls_cod_deposito_ord_ven, ls_cod_cliente, ls_cod_semilavorato, ls_cod_tipo_ord_ven, ls_flag_tipo_stampa, ls_flag_gen_commessa, &
						ls_cod_deposito_giro, ls_cod_deposito_commerciale
						
long					ll_num_commessa, ll_anno_commessa, ll_riga, ll_riga_appartenenza, ll_result, ll_i, ll_num_sequenza_finale, ll_ret, ll_count

dec{4}				ld_quan_ordine, ld_prezzo_vendita, ld_val_riga, ld_quan_in_evasione, ld_quan_evasa, ld_quantita_um, ld_prezzo_um, &
						ld_sconto_1, ld_sconto_2
						
datetime				ldt_data_consegna

integer				li_anno_bolla_esistente
long					ll_num_bolla_esistente, ll_prog_riga_bolla_esistente
string					ls_cod_tipo_bolla_esistente, ls_bolla_esistente

datastore			lds_fasi_lavorazione

uo_produzione		luo_prod


setnull(as_error)

// recupero il deposito di orgine dell'ordine
select cod_deposito, cod_cliente, cod_tipo_ord_ven
into :ls_cod_deposito_ord_ven, :ls_cod_cliente, :ls_cod_tipo_ord_ven
from tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione;
		 
if sqlca.sqlcode = -1 then
	as_error = "Errore in lettura della testata ordine.~r~n" + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_error = "Ordine non trovato."
	return -1
end if
// ----

//se c'è un giro con deposito partenza considera questo al posto del deposito origine ordine

//prima mi salvo il deposito commerciale, mi serve per un setfilter su un datastore che va sulle fasi di lavorazioni, più in basso
ls_cod_deposito_commerciale = ls_cod_deposito_ord_ven

luo_prod = create uo_produzione
ls_cod_deposito_giro = luo_prod.uof_deposito_giro_consegna(al_anno_registrazione, al_num_registrazione)
destroy luo_prod
if g_str.isnotempty(ls_cod_deposito_giro) and ls_cod_deposito_giro<>ls_cod_deposito_ord_ven then ls_cod_deposito_ord_ven=ls_cod_deposito_giro


// Stefanop 27/03/2012: controllo se è uno sfuso o no
select flag_tipo_bcl
into :ls_flag_tipo_stampa
from tab_tipi_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		 
if sqlca.sqlcode <> 0 or isnull(ls_cod_tipo_ord_ven) then
	as_error = "Errore durante il recupero del Tipo BCL!"
	return -1
end if

SELECT 	cod_prodotto, cod_tipo_det_ven, cod_misura, des_prodotto, quan_ordine, prezzo_vendita, sconto_1, sconto_2, 
			data_consegna, val_riga, quan_in_evasione, quan_evasa, anno_commessa, num_commessa, quantita_um, prezzo_um,
			flag_urgente, num_riga_appartenenza, cod_versione, flag_gen_commessa
 INTO 	:ls_cod_prodotto, :ls_cod_tipo_det_ven, :ls_cod_misura, :ls_des_prodotto, :ld_quan_ordine, :ld_prezzo_vendita, 
			:ld_sconto_1, :ld_sconto_2, :ldt_data_consegna, :ld_val_riga, :ld_quan_in_evasione, :ld_quan_evasa, 
			:ll_anno_commessa, :ll_num_commessa, :ld_quantita_um, :ld_prezzo_um, :ls_flag_urgente, :ll_riga_appartenenza,
			:ls_cod_versione, :ls_flag_gen_commessa
FROM det_ord_ven  
WHERE	cod_azienda = :s_cs_xx.cod_azienda AND  
			anno_registrazione = :al_anno_registrazione AND  
			num_registrazione = :al_num_registrazione AND  
			prog_riga_ord_ven = :al_prog_riga_ord_ven;
		
if sqlca.sqlcode = -1 then
	as_error = "Errore in lettura righe ordini " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_error = "Riga ordine non trovato."
	return -1
end if

//Donato 06/07/2012 Controlla se questa riga di ordine è stata già trasferita ----------------------
select count(*)
into :ll_count
from det_bol_ven as d
join tes_bol_ven as t on 	t.cod_azienda=t.cod_azienda and
								t.anno_registrazione=d.anno_registrazione and
								t.num_registrazione=d.num_registrazione
where 	d.cod_azienda=:s_cs_xx.cod_azienda and
			d.anno_registrazione_ord_ven=:al_anno_registrazione and
			d.num_registrazione_ord_ven=:al_num_registrazione and
			d.prog_riga_ord_ven=:al_prog_riga_ord_ven and
			t.cod_deposito=:is_cod_deposito_partenza and
			t.cod_deposito_tras=:is_cod_deposito_arrivo and
			t.cod_tipo_bol_ven in (select cod_tipo_bol_ven
										from tab_tipi_bol_ven
										where cod_azienda=:s_cs_xx.cod_azienda and
												flag_tipo_bol_ven='T');

if sqlca.sqlcode<0 then
	as_error = "Errore in controllo esistenza della riga ordine in altro D.d.T. di trasferimento: "+sqlca.sqlerrtext
	return -1
end if
// fine modifica --------------------------------------------------------------------------------------------

if ll_count>0 then
	//riga già trasferita dal deposito di partenza al deposito di arrivo
	
	select d.anno_registrazione,d.num_registrazione,d.prog_riga_bol_ven,t.cod_tipo_bol_ven
	into :li_anno_bolla_esistente, :ll_num_bolla_esistente, :ll_prog_riga_bolla_esistente, :ls_cod_tipo_bolla_esistente
	from det_bol_ven as d
	join tes_bol_ven as t on 	t.cod_azienda=t.cod_azienda and
									t.anno_registrazione=d.anno_registrazione and
									t.num_registrazione=d.num_registrazione
	where 	d.cod_azienda=:s_cs_xx.cod_azienda and
				d.anno_registrazione_ord_ven=:al_anno_registrazione and
				d.num_registrazione_ord_ven=:al_num_registrazione and
				d.prog_riga_ord_ven=:al_prog_riga_ord_ven and
				t.cod_deposito=:is_cod_deposito_partenza and
				t.cod_deposito_tras=:is_cod_deposito_arrivo and
				t.cod_tipo_bol_ven in (select cod_tipo_bol_ven
										from tab_tipi_bol_ven
										where cod_azienda=:s_cs_xx.cod_azienda and
												flag_tipo_bol_ven='T');
	
	if sqlca.sqlcode<0 then
		ls_bolla_esistente = ""
	else
		 ls_bolla_esistente = "(" + ls_cod_tipo_bolla_esistente + " " + string(li_anno_bolla_esistente) +"/"+&
		 								string(ll_num_bolla_esistente)+"/"+string(ll_prog_riga_bolla_esistente) + ")"
	end if
	
	as_error = "La riga "+string(al_anno_registrazione)+"/"+string(al_num_registrazione)+"/"+string(al_prog_riga_ord_ven)+ &
				" è già presente altro D.d.T. di trasferimento da dep. "+is_cod_deposito_partenza+" a dep. "+is_cod_deposito_arrivo+&
				" "+ls_bolla_esistente+" !"
	return -1
end if

// controllo reparti
ll_result = wf_controlla_deposito(al_anno_registrazione, al_num_registrazione, al_prog_riga_ord_ven, ls_cod_prodotto, ls_cod_versione, ls_cod_deposito_ord_ven, as_error) 

if ll_result < 0 then
	// errore
	return -1
elseif ll_result = 0 then
	// non sono nello stack quindi salto riga
	as_error = "I reparti del deposito di partenza non"
	return -1
end if
// ----

if ls_flag_tipo_stampa <> "B" then
	// recupero stato fasi lavorazioni
	uo_funzioni_1 luo_funzioni_1
	luo_funzioni_1 = create uo_funzioni_1
	
	if luo_funzioni_1.uof_trova_fasi_lavorazioni(ls_cod_prodotto, ls_cod_versione, ld_quan_ordine, al_anno_registrazione, al_num_registrazione, al_prog_riga_ord_ven, "varianti_det_ord_ven", lds_fasi_lavorazione, as_error) < 0 then
		return -1
	end if
	
	destroy luo_funzioni_1
	
	if lds_fasi_lavorazione.rowcount() = 0 then
		as_error = "Non sono state trovate fasi di lavorazione per la riga ordine: "+&
						string(al_anno_registrazione)+"/"+string(al_num_registrazione)+"/"+string(al_prog_riga_ord_ven) + " (prodotto:"+ls_cod_prodotto+")"
		return -1
	end if
	
	
	// recupero il numero di sequenza massimo, che mi indica il termine della lavorazione
	string ls_cod_semilavorati[], ls_empty[]
	long ll_pos_semilavorato
	boolean lb_invia_semilavorato
	
	lb_invia_semilavorato = false
	ll_num_sequenza_finale = lds_fasi_lavorazione.getitemnumber(lds_fasi_lavorazione.rowcount(), "num_sequenza")
	
	//INIZIO MODIFICA -----------------------------------------
	//questo lo faccio sul deposito commerciale dell'ordine, a prescindere da quale è il depopsito del giro consegna
	//ecco perchè in precedenza mi son o salvato nella variabile "ls_cod_deposito_commerciale" il deposito in testata ordine di vendita
	lds_fasi_lavorazione.setfilter("cod_deposito_commerciale='" + ls_cod_deposito_commerciale + "'")
	
	//Questo lo commento in quanto la variabile "ls_cod_deposito_ord_ven" potrebbe essere stata sovrascritta dal deposito del giro consegna
	//lds_fasi_lavorazione.setfilter("cod_deposito_commerciale='" + ls_cod_deposito_ord_ven + "'")
	//FINE MODIFICA ------------------------------------------
	
	lds_fasi_lavorazione.filter()
	for ll_i = 1 to lds_fasi_lavorazione.rowcount()
		
		// lo deve fare il mio deposito?
		if is_cod_deposito_partenza = lds_fasi_lavorazione.getitemstring(ll_i, "cod_deposito_produzione") then
			ls_cod_semilavorati[upperbound(ls_cod_semilavorati) + 1] = lds_fasi_lavorazione.getitemstring(ll_i, "cod_semilavorato")
			ls_cod_semilavorato = lds_fasi_lavorazione.getitemstring(ll_i, "cod_semilavorato")
			ll_pos_semilavorato = ll_i
		end if
		
	next
	
	// controllo se ci sono altre fasi
	for ll_i = ll_pos_semilavorato to lds_fasi_lavorazione.rowcount()
		if is_cod_deposito_partenza <> lds_fasi_lavorazione.getitemstring(ll_i, "cod_deposito_produzione") then
			lb_invia_semilavorato = true
		end if
	next
	
	
	if not lb_invia_semilavorato then
		ls_cod_semilavorati = ls_empty
		ls_cod_semilavorati[1] = ls_cod_prodotto
	end if
	// ----
else
	// E' uno sfuso
	ls_cod_semilavorati = ls_empty
	ls_cod_semilavorati[1] = ls_cod_prodotto
end if


// ciclo tutti i semilavorati che devo inviare
for ll_i = 1 to upperbound(ls_cod_semilavorati) 
	
	ls_cod_semilavorato = ls_cod_semilavorati[ll_i]
		
	// verifico che il prodotto non sia già stato messo in spedizione, posso spedire un solo tipo di prodotto
	if wf_check_riga(al_anno_registrazione, al_num_registrazione, al_prog_riga_ord_ven, ls_cod_semilavorato)  then
		return 0
	end if
	
	// se è un semilavorato allora recuper descrizione e quantità
	if lb_invia_semilavorato then
		ls_des_prodotto = f_des_tabella("anag_prodotti", "cod_prodotto='"+ls_cod_semilavorato+"'", "des_prodotto")
		ld_quan_ordine = lds_fasi_lavorazione.getitemnumber(ll_pos_semilavorato, "quan_utilizzo")
	end if
	
	ll_riga = tab_1.det_2.dw_righe_ordine.insertrow(0)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "anno_registrazione", al_anno_registrazione)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "num_registrazione", al_num_registrazione)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "prog_riga_ord_ven", al_prog_riga_ord_ven)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "cod_tipo_det_ven", ls_cod_tipo_det_ven)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "cod_prodotto", ls_cod_semilavorato)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "des_prodotto", ls_des_prodotto)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "cod_misura_ven", ls_cod_misura)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "quan_ordine", ld_quan_ordine)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "quantita_um", ld_quantita_um)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "prezzo_vendita", ld_prezzo_vendita)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "sconto_1", ld_sconto_1)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "sconto_2", ld_sconto_2)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "prezzo_um", ld_prezzo_um)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "quan_in_evasione", ld_quan_in_evasione)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "quan_evasa", ld_quan_evasa)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "val_riga", ld_val_riga)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "anno_commessa", ll_anno_commessa)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "num_commessa", ll_num_commessa)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "data_consegna", ldt_data_consegna)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "quan_residua", ld_quan_ordine - ld_quan_in_evasione - ld_quan_evasa)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "quan_da_evadere", ld_quan_ordine - ld_quan_in_evasione - ld_quan_evasa)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "flag_evasione_totale", "S")
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "flag_urgente", ls_flag_urgente)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "num_riga_appartenenza", ll_riga_appartenenza)
			
next

// se devo spedire un prodotto finito carico le righe collegate
if not lb_invia_semilavorato then
	return wf_carica_righe_collegate(al_anno_registrazione, al_num_registrazione, al_prog_riga_ord_ven, ls_cod_prodotto, as_error)
end if

return 0
end function

public function integer wf_controlla_deposito (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_cod_prodotto, string as_cod_versione, string as_cod_deposito_ord_ven, ref string as_error);/**
 * stefanop
 * 25/01/2012
 *
 * Controllo lo stabilimento.
 * 1. Il mio stabilimento deve essere presente nello stack di produzione
 * 2. Lo stabilimento successivo al mio deve essere quello impostato nel filtro o..
 * 3. Lo stabilimento di destinazione deve essere diverso dal mio
 *
 * Ritorno:
 * -1 Errore
 *  0 Il mio deposito non è nello stack, quindi salto
 *  1 Riga da prendere in considerazione
 **/

string ls_cod_reparto[], ls_cod_deposito_riga, ls_flag_tipo_stampa, ls_cod_tipo_ord_ven, ls_cod_deposito_prod_tes_sfuso, ls_null_string
long ll_pos_rep, ll_i
boolean lb_salta_riga = true, lb_controlla_dep_testata_ord
uo_funzioni_1 luo_funzioni_1
		
lb_salta_riga = true
lb_controlla_dep_testata_ord = true // indica se devo confrontare il deposito che sta nella testa dell'ordine
luo_funzioni_1 = create uo_funzioni_1

// Stefanop 27/03/2012: controllo se è uno sfuso o no
select cod_tipo_ord_ven, cod_deposito_prod
into :ls_cod_tipo_ord_ven, :ls_cod_deposito_prod_tes_sfuso
from tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione;
		 
if sqlca.sqlcode <> 0 or isnull(ls_cod_tipo_ord_ven) then
	as_error = "Errore durante il recupero del tipo ordine!"
	return -1
end if
		 
select flag_tipo_bcl
into :ls_flag_tipo_stampa
from tab_tipi_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		 
if sqlca.sqlcode <> 0 or isnull(ls_cod_tipo_ord_ven) then
	as_error = "Errore durante il recupero del Tipo BCL!"
	return -1
end if

// stefanop: in base al tipo d'ordine decido da dove prendere i reparti (sfuso o altro)
if ls_flag_tipo_stampa = "B" then
	
	//08/03/2012 Donato
	//----------------------------------------------------------------------------------------------------------------
	//Annotazione mia, che se aspetto che Stefanin commenti il suo codice merdoso stiamo freschi ...
	//----------------------------------------------------------------------------------------------------------------
	//ls_cod_dep_filtro 	è il deposito ORIGINE dell'ordine selezionato 		-> DESTINAZIONE
	//ls_cod_deposito_ut 	è il deposito PRODUZIONE selezionato 				-> PARTENZA
		
	//se NON specificato il deposito produzione allora vuol dire che coincide con quello di origine e quindi non devo trasferire niente: SALTO LA RIGA
	//se E' stato specificato ed è DIVERSO da quello origine e COINCIDENTE con quello dell'utente (di PRODUZIONE) allora elaboro
	if ls_cod_deposito_prod_tes_sfuso="" or isnull(ls_cod_deposito_prod_tes_sfuso)  then
		return 0
	end if
//	if ls_cod_deposito_prod_tes_sfuso=ls_cod_dep_filtro or ls_cod_deposito_prod_tes_sfuso<>ls_cod_deposito_ut or &
//			ls_cod_dep_filtro<>ls_cod_deposito_origine then
	if ls_cod_deposito_prod_tes_sfuso = is_cod_deposito_arrivo or ls_cod_deposito_prod_tes_sfuso<>is_cod_deposito_partenza then
//	or is_cod_deposito_arrivo<>is_cod_deposito_arrivo then
		return 0
	end if
	//-------------------------------------------------------------------------------------
	
	setnull(ls_null_string)
	
	//Questa funzione recupera i reparti del deposito produzione della riga ordine
	if luo_funzioni_1.uof_trova_reparti_sfuso(as_cod_prodotto, ls_cod_deposito_prod_tes_sfuso, ls_null_string, ls_cod_reparto, as_error) < 0 then
		destroy luo_funzioni_1
		return -1
	end if
	
	// se l'array è vuoto non devo produrre io il prodotto
	if upperbound(ls_cod_reparto) < 1 then return 0
	
else

	// stefanop: recupero reparti
	if luo_funzioni_1.uof_trova_reparti(true, al_anno_registrazione, al_num_registrazione, al_prog_riga_ord_ven, as_cod_prodotto, as_cod_versione, ref ls_cod_reparto[], ref as_error ) < 0 then
		return 0
	end if
	
	//controllo se almeno un reparto della lista repart previsti è nella lista dei reparti (stack di produzione) del deposito di partenza  (mah!!! a che cazzo servirà sta cosa....)
	//la funzione torna la posizione, che sarà diversa da zero se presente
	if not guo_functions.uof_in_array(is_cod_reparti_partenza, ls_cod_reparto, ll_pos_rep) then
		return 0
	else
		// il mio reparto è presente nello stack di produzione, devo controllare lo stabilimento successivo al mio.
		if ll_pos_rep = upperbound(ls_cod_reparto) then
			// il mio reparto è presente ma è all'ultima posizione quindi controllo la testata dell'ordine
			lb_controlla_dep_testata_ord = true
		else
			for ll_i = ll_pos_rep + 1  to upperbound(ls_cod_reparto)
				
				ls_cod_deposito_riga = f_des_tabella("anag_reparti", "cod_reparto='" + ls_cod_reparto[ll_i] + "'", "cod_deposito")
				
				// controllo se ci sono altri reparto nello stack.
				if ls_cod_deposito_riga <> is_cod_deposito_partenza then
					lb_controlla_dep_testata_ord = false
				end if
				
				if ls_cod_deposito_riga <> is_cod_deposito_partenza and (isnull(is_cod_deposito_arrivo) or is_cod_deposito_arrivo = ls_cod_deposito_riga) then
					lb_salta_riga = false
					exit
				end if
			next
		end if
		
		if lb_controlla_dep_testata_ord then
			// la riga è da saltare per qunto riguarda i reparti coinvolti ma controllo il deposito origine dell'ordine.
			if as_cod_deposito_ord_ven <> is_cod_deposito_partenza and (isnull(is_cod_deposito_arrivo) or is_cod_deposito_arrivo = as_cod_deposito_ord_ven) then
				lb_salta_riga = false
			else
				return 0
			end if
		end if
		
		if lb_salta_riga then
			return 0
		end if			
		// --
	end if
	//----
end if

return 1
end function

public function boolean wf_step (integer ai_step, boolean ab_fire_event);/**
 * stefanop
 * 25/01/2012
 *
 * Abilitito i tab in base al numero di step passato
 **/
 
string ls_error

choose case ai_step
		
	case 1
		tab_1.det_1.enabled = true
		tab_1.det_2.enabled = false
		tab_1.det_3.enabled = false
		
		tab_1.det_2.dw_righe_ordine.reset()
		tab_1.det_3.cb_genera_ddt.enabled = true
		
	case 2
		// controllo tipo bolla
		if isnull(tab_1.det_1.dw_1.getitemstring(1, "cod_tipo_bol_ven")) or len(tab_1.det_1.dw_1.getitemstring(1, "cod_tipo_bol_ven")) < 1 then
			g_mb.warning("Impostare il tipo bolla")
			tab_1.det_1.dw_1.setcolumn("cod_tipo_bol_ven")
			return false
		end if
		
		// Controllo deposito di partenza e relativi reparti
		is_cod_deposito_partenza = tab_1.det_1.dw_1.getitemstring(1, "cod_deposito_partenza")
		
		if isnull(is_cod_deposito_partenza) or len(is_cod_deposito_partenza) < 1 then
			g_mb.warning("Selezionare un deposito di partenza.")
			tab_1.det_1.dw_1.setcolumn("cod_deposito_partenza")
			return false
		end if
		
		wf_get_reparti_deposito(is_cod_deposito_partenza, is_cod_reparti_partenza[], ls_error)

		if upperbound(is_cod_reparti_partenza) < 1 then
			g_mb.error("Lo stabilimento " + is_cod_deposito_partenza + " non ha nessun reparto associato")
			return false
		end if

		// Controllo deposito di arrivo
		is_cod_deposito_arrivo = tab_1.det_1.dw_1.getitemstring(1, "cod_deposito_finale")
		if isnull(is_cod_deposito_arrivo) or len(is_cod_deposito_arrivo) < 1 then 
			g_mb.warning("Selezionare un deposito di arrivo.")
			tab_1.det_1.dw_1.setcolumn("cod_deposito_finale")
			return false
		end if
		
		tab_1.det_1.enabled = true
		tab_1.det_2.enabled = true
		tab_1.det_3.enabled = false
		
		tab_1.det_2.dw_barcode.setitem(1, "barcode", "")
		tab_1.det_2.dw_barcode.setfocus()
		tab_1.det_2.dw_barcode.setcolumn("barcode")
	case 3
		tab_1.det_3.dw_dati_bolla.reset()
		tab_1.det_3.dw_dati_bolla.insertrow(0)
		wf_step_3()
		
		tab_1.det_1.enabled = true
		tab_1.det_2.enabled = true
		tab_1.det_3.enabled = true
		
end choose

ii_current_step = ai_step

if ab_fire_event then tab_1.selecttab(ai_step)

return true
end function

public function boolean wf_step (integer ai_step);/**
 * stefanop
 * 25/01/2012
 *
 * Abilitito i tab in base al numero di step passato
 **/
 
return wf_step(ai_step, true)
end function

public function integer wf_get_reparti_deposito (string as_cod_deposito, ref string as_cod_reparti[], ref string as_errore);/**
 * stefanop
 * 11/10/2012
 *
 * Dato un deposito mi recupero tutti i reparti associati
 **/
 
 
string ls_sql
long ll_rows, ll_i
datastore lds_store

ls_sql = "SELECT cod_reparto FROM anag_reparti WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND cod_deposito='" + as_cod_deposito + "'"

ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_errore)

for ll_i = 1 to ll_rows
	
	as_cod_reparti[ll_i] = lds_store.getitemstring(ll_i, 1)
	
next

return ll_rows
end function

public subroutine wf_step_3 ();string ls_cod_causale, ls_cod_porto, ls_cod_mezzo, ls_cod_resa, ls_cod_vettore, ls_cod_inoltro, ls_nota_piede, &
		ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cap, ls_localita,ls_provincia, ls_null, &
		ls_nota_testata, ls_nota_video, ls_cod_tipo_bol_ven, ls_cod_imballo, ls_cod_cliente
double ld_num_colli, ld_peso_lordo, ld_peso_netto
datetime ldt_oggi, ldt_data_ord_cliente

setnull(ls_null)
ls_cod_tipo_bol_ven = tab_1.det_1.dw_1.getitemstring(1, "cod_tipo_bol_ven")

//recupero cliente per fare la bolla dai parametri vendite
select cod_cliente_bol_trasf
into :ls_cod_cliente
from con_vendite;

if sqlca.sqlcode <> 0 or isnull(ls_cod_cliente) or len(ls_cod_cliente) < 1 then
	g_mb.error("Non è impostato nessun cliente per la generazione di bolle di traferimento.~r~nControllare i Parametri Vendita")
end if
// ----
		
select  cod_porto, cod_mezzo, cod_resa, cod_vettore, cod_inoltro,
		 rag_soc_1, rag_soc_2, indirizzo, frazione, cap, localita, provincia, cod_imballo
into   :ls_cod_porto, :ls_cod_mezzo, :ls_cod_resa, :ls_cod_vettore, :ls_cod_inoltro,
		 :ls_rag_soc_1, :ls_rag_soc_2, :ls_indirizzo, :ls_frazione, :ls_cap, :ls_localita,:ls_provincia, :ls_cod_imballo
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :ls_cod_cliente;
		 
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante la lettura delle informazioni del cliente")
	return
end if

select cod_causale
into :ls_cod_causale
from tab_tipi_bol_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;

f_po_loaddddw_dw(tab_1.det_3.dw_dati_bolla, &
					  "cod_destinazione", &
					  sqlca, &
					  "anag_des_clienti", &
					  "cod_des_cliente", &
					  "rag_soc_1", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + ls_cod_cliente + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_1.det_3.dw_dati_bolla, &
					  "cod_imballo", &
					  sqlca, &
					  "tab_imballi", &
					  "cod_imballo", &
					  "des_imballo", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_1.det_3.dw_dati_bolla, &
					  "cod_vettore", &
					  sqlca, &
					  "anag_vettori", &
					  "cod_vettore", &
					  "rag_soc_1", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_1.det_3.dw_dati_bolla, &
					  "cod_inoltro", &
					  sqlca, &
					  "anag_vettori", &
					  "cod_vettore", &
					  "rag_soc_1", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_1.det_3.dw_dati_bolla, &
					  "cod_mezzo", &
					  sqlca, &
					  "tab_mezzi", &
					  "cod_mezzo", &
					  "des_mezzo", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_1.det_3.dw_dati_bolla, &
					  "cod_porto", &
					  sqlca, &
					  "tab_porti", &
					  "cod_porto", &
					  "des_porto", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_1.det_3.dw_dati_bolla, &
					  "cod_resa", &
					  sqlca, &
					  "tab_rese", &
					  "cod_resa", &
					  "des_resa", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_1.det_3.dw_dati_bolla, &
					  "cod_causale", &
					  sqlca, &
					  "tab_causali_trasp", &
					  "cod_causale", &
					  "des_causale", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_1.det_3.dw_dati_bolla, &
					  "cod_tipo_bolla", &
					  sqlca, &
					  "tab_tipi_bol_ven", &
					  "cod_tipo_bol_ven", &
					  "des_tipo_bol_ven", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' " )
		
				
tab_1.det_3.dw_dati_bolla.setitem(1, "cod_cliente", ls_cod_cliente)
tab_1.det_3.dw_dati_bolla.setitem(1, "cod_mezzo", ls_cod_mezzo)
tab_1.det_3.dw_dati_bolla.setitem(1, "cod_causale", ls_cod_causale)
tab_1.det_3.dw_dati_bolla.setitem(1, "cod_porto", ls_cod_porto)
tab_1.det_3.dw_dati_bolla.setitem(1, "cod_resa", ls_cod_resa)
tab_1.det_3.dw_dati_bolla.setitem(1, "cod_vettore", ls_cod_vettore)
tab_1.det_3.dw_dati_bolla.setitem(1, "cod_inoltro", ls_cod_inoltro)
tab_1.det_3.dw_dati_bolla.setitem(1, "cod_imballo", ls_cod_imballo)
tab_1.det_3.dw_dati_bolla.setitem(1, "rag_soc_1", ls_rag_soc_1)
tab_1.det_3.dw_dati_bolla.setitem(1, "rag_soc_2", ls_rag_soc_2)
tab_1.det_3.dw_dati_bolla.setitem(1, "indirizzo", ls_indirizzo)
tab_1.det_3.dw_dati_bolla.setitem(1, "frazione", ls_frazione)
tab_1.det_3.dw_dati_bolla.setitem(1, "localita", ls_localita)
tab_1.det_3.dw_dati_bolla.setitem(1, "cap", ls_cap)
tab_1.det_3.dw_dati_bolla.setitem(1, "provincia", ls_provincia)
tab_1.det_3.dw_dati_bolla.setitem(1, "num_colli", ld_num_colli)
tab_1.det_3.dw_dati_bolla.setitem(1, "peso_netto", ld_peso_netto)
tab_1.det_3.dw_dati_bolla.setitem(1, "peso_lordo", ld_peso_lordo)
tab_1.det_3.dw_dati_bolla.setitem(1, "data_ord_cliente", ldt_data_ord_cliente)

tab_1.det_3.dw_dati_bolla.setitem(1, "cod_tipo_bolla", ls_cod_tipo_bol_ven)

if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "BOL_VEN", ls_null, ldt_oggi, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
	g_mb.error(ls_nota_testata)
else
	if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
end if

end subroutine

public function integer wf_genera_ddt ();
//boolean ib_prezzo_sl=false   <<<----- QUESTA VARIABILE NON SERVE +, CREDO

string					ls_cod_cliente, ls_cod_prodotto_spedizione, ls_cod_prodotto_finito,ls_null, ls_errore,ls_flag_gen_commessa, &
						ls_flag_chiudi_commessa, ls_errore_rif_commessa,ls_errore_rif_ordine,ls_errore_email,ls_allegati[]
						
long					ll_anno_bolla, ll_num_bolla, ll_i, ll_ret, ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_ord_ven, &
						ll_anno_commessa, ll_num_commessa,ll_num_riga_appartenenza
						
dec{4}								ld_prezzo, ld_sconto_sbt
uo_generazione_documenti		luo_generazione_documenti
s_det_riga							lstr_det_righe[]
uo_funzioni_1						luo_funzioni_commesse
uo_storicizzazione					luo_storicizzazione
uo_outlook							luo_mail


setnull(ls_null)

if not g_mb.confirm("Sei sicuro di voler creare una nuova bolla per tutte le righe assegnate di questo ordine?") then
	return 0
end if

// Parametro SBT
guo_functions.uof_get_parametro_azienda("SBT", ld_sconto_sbt)
if isnull(ld_sconto_sbt) or ld_sconto_sbt < 0 then ld_sconto_sbt = 0
// ----
	
tab_1.det_3.dw_dati_bolla.accepttext()

luo_funzioni_commesse 			= CREATE uo_funzioni_1

luo_generazione_documenti 	= CREATE uo_generazione_documenti
luo_generazione_documenti.is_cod_tipo_bol_ven = tab_1.det_3.dw_dati_bolla.getitemstring(1,"cod_tipo_bolla")

luo_generazione_documenti.is_cod_causale = tab_1.det_3.dw_dati_bolla.getitemstring(1,"cod_causale")
luo_generazione_documenti.is_aspetto_beni = tab_1.det_3.dw_dati_bolla.getitemstring(1,"aspetto_beni")
luo_generazione_documenti.is_cod_mezzo = tab_1.det_3.dw_dati_bolla.getitemstring(1,"cod_mezzo")
luo_generazione_documenti.is_cod_causale = tab_1.det_3.dw_dati_bolla.getitemstring(1,"cod_causale")
luo_generazione_documenti.is_cod_porto = tab_1.det_3.dw_dati_bolla.getitemstring(1,"cod_porto")
luo_generazione_documenti.is_cod_resa = tab_1.det_3.dw_dati_bolla.getitemstring(1,"cod_resa")
luo_generazione_documenti.is_cod_vettore = tab_1.det_3.dw_dati_bolla.getitemstring(1,"cod_vettore")
luo_generazione_documenti.is_cod_inoltro = tab_1.det_3.dw_dati_bolla.getitemstring(1,"cod_inoltro")
luo_generazione_documenti.is_cod_imballo = tab_1.det_3.dw_dati_bolla.getitemstring(1,"cod_imballo")
luo_generazione_documenti.is_destinazione = tab_1.det_3.dw_dati_bolla.getitemstring(1,"cod_destinazione")
luo_generazione_documenti.id_num_colli = tab_1.det_3.dw_dati_bolla.getitemnumber(1,"num_colli")
luo_generazione_documenti.id_peso_netto = tab_1.det_3.dw_dati_bolla.getitemnumber(1,"peso_netto")
luo_generazione_documenti.id_peso_lordo = tab_1.det_3.dw_dati_bolla.getitemnumber(1,"peso_lordo")
luo_generazione_documenti.idt_data_inizio_trasporto = tab_1.det_3.dw_dati_bolla.getitemdatetime(1,"data_inizio_trasp")
luo_generazione_documenti.it_ora_inizio_trasporto = tab_1.det_3.dw_dati_bolla.getitemtime(1,"ora_inizio_trasp")
luo_generazione_documenti.is_rag_soc_1 = tab_1.det_3.dw_dati_bolla.getitemstring(1,"rag_soc_1")
luo_generazione_documenti.is_rag_soc_2 = tab_1.det_3.dw_dati_bolla.getitemstring(1,"rag_soc_2")
luo_generazione_documenti.is_indirizzo = tab_1.det_3.dw_dati_bolla.getitemstring(1,"indirizzo")
luo_generazione_documenti.is_localita = tab_1.det_3.dw_dati_bolla.getitemstring(1,"localita")
luo_generazione_documenti.is_frazione = tab_1.det_3.dw_dati_bolla.getitemstring(1,"frazione")
luo_generazione_documenti.is_provincia = tab_1.det_3.dw_dati_bolla.getitemstring(1,"provincia")
luo_generazione_documenti.is_cap = tab_1.det_3.dw_dati_bolla.getitemstring(1,"cap")
luo_generazione_documenti.is_nota_piede = tab_1.det_3.dw_dati_bolla.getitemstring(1,"note_piede")
luo_generazione_documenti.is_num_ord_cliente = tab_1.det_3.dw_dati_bolla.getitemstring(1,"num_ord_cliente")
luo_generazione_documenti.idt_data_ord_cliente = tab_1.det_3.dw_dati_bolla.getitemdatetime(1,"data_ord_cliente")
	
ll_anno_bolla = 0
ll_num_bolla = 0

if len(luo_generazione_documenti.is_cod_causale) < 1 then setnull(luo_generazione_documenti.is_cod_causale)
if len(luo_generazione_documenti.is_cod_mezzo) < 1 then setnull(luo_generazione_documenti.is_cod_mezzo)
if len(luo_generazione_documenti.is_cod_porto) < 1 then setnull(luo_generazione_documenti.is_cod_porto)
if len(luo_generazione_documenti.is_cod_resa) < 1 then setnull(luo_generazione_documenti.is_cod_resa)
if len(luo_generazione_documenti.is_cod_vettore) < 1 then setnull(luo_generazione_documenti.is_cod_vettore)
if len(luo_generazione_documenti.is_cod_inoltro) < 1 then setnull(luo_generazione_documenti.is_cod_inoltro)
if len(luo_generazione_documenti.is_cod_imballo) < 1 then setnull(luo_generazione_documenti.is_cod_imballo)
if len(luo_generazione_documenti.is_destinazione) < 1 then setnull(luo_generazione_documenti.is_destinazione)



//#################################################################
//Sistemazione per anomalia di commesse che non venivano avanzate in fase di trasferimenti
//14/09/2012 Donato 
//effettuo un primo ciclo per la valorizzazione eventuale dei semilavorati
//cioè carico la struttura limitatamente al seguente campo lstr_det_righe[ll_i].prezzo_vendita 
//con il prezzo della valorizzazione se è un SL o con il prezzo della riga ordine altrimenti
//Le clausole ROLLBACK lanciate in tale fase non influenzeranno gli avanzamenti commessa, che andrò a fare in un ciclo successivo

//*********************************************************************
//CICLO N°1  valorizzazione prezzi SL
for ll_i = 1 to tab_1.det_2.dw_righe_ordine.rowcount()
	
	ls_cod_prodotto_spedizione = tab_1.det_2.dw_righe_ordine.getitemstring(ll_i, "cod_prodotto")
	ll_anno_registrazione = tab_1.det_2.dw_righe_ordine.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = tab_1.det_2.dw_righe_ordine.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_ord_ven = tab_1.det_2.dw_righe_ordine.getitemnumber(ll_i, "prog_riga_ord_ven")
	ls_flag_chiudi_commessa = tab_1.det_2.dw_righe_ordine.getitemstring(ll_i, "flag_chiudi_commessa")
	
	//leggo il prezzo della riga ordine
	ld_prezzo = tab_1.det_2.dw_righe_ordine.getitemnumber(ll_i, "prezzo_vendita")
	
	
	ls_errore_rif_ordine = "RIGA ORDINE:" + string(ll_anno_registrazione) + "-" + string(ll_num_registrazione) + "-" + string(ll_prog_riga_ord_ven)
	
	select cod_prodotto, 
			anno_commessa, 
			num_commessa,
			num_riga_appartenenza,
			flag_gen_commessa
	into 	:ls_cod_prodotto_finito, 
			:ll_anno_commessa, 
			:ll_num_commessa,
			:ll_num_riga_appartenenza,
			:ls_flag_gen_commessa
	from 	det_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
	if sqlca.sqlcode <> 0 then
		rollback;
		if isnull(sqlca.sqlerrtext) then sqlca.sqlerrtext = ""
		g_mb.error("Errore in ricerca della riga ordine di origine." + ls_errore_rif_ordine + " Dettaglio "+ sqlca.sqlerrtext)
		return -1
	end if
	

	if ls_flag_gen_commessa = "S" and ( ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza)) and ls_cod_prodotto_spedizione <> ls_cod_prodotto_finito then

		//sono in fase di spedizione di un semilavorato di un prodotto finito: eseguo la valorizzazione separata
		// calcolo il prezzo del semilavorato (usando il sistema della vecchia procedura A.R.
		// tale procedura in realtà fa degli update nella riga d'ordine, ma poi il sistema fa dei ROLLBACK in modo da riportare tutto all'origine
		luo_storicizzazione = CREATE uo_storicizzazione
		ld_prezzo = 0
		ll_ret = luo_storicizzazione.uof_valorizza_semilavorato( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_prodotto_spedizione, ref ld_prezzo, ls_errore)
		if ll_ret < 0 then
			destroy luo_storicizzazione
			g_mb.error("ERRORE IN FASE DI CALCOLO DEL PREZZO DEL SEMILAVORATO "+ls_cod_prodotto_spedizione+".~r~n" + ls_errore)
			rollback;
			return -1
		end if
		destroy luo_storicizzazione
		
		if isnull(ld_prezzo) then ld_prezzo=0
		
		// Come regola, i semilavorati (es. assieme telo) vanno valorizzati togliendo sconto 50 e un ulteriore sconto previsto nel parametro SBT
		ld_prezzo = (ld_prezzo / 100) * 50
		
//		****  commentato perchè lo socnto SBT viene già tolto durante la fase di creazione bolla (luo_generazione_documenti.uof_crea_bolla_trasferimento)
//		if ld_sconto_sbt > 0 then
//			ld_prezzo = ld_prezzo - ( (ld_prezzo / 100) * ld_sconto_sbt)
//		end if
// 		arrotondo
//		ld_prezzo = round( ld_prezzo,2 )
//		-----------------------------------------------------------------------------------------------------------------
		
		if ld_prezzo = 0 then
			// Se prezzo = 0 allora manda una mail di avviso
			luo_mail = create uo_outlook
			luo_mail.uof_invio_sendmail("A", &
												g_str.format("Avviso Anomalia Valore Trasferimento Semilavorato "+ls_cod_prodotto_spedizione+" ordine $1 / $2 riga $3", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven), &
												"Salve,~r~nL'elaborazione della riga ordine in oggetto restituisce prezzo 0.~r~nE' possibile che per il semilavorato "+ls_cod_prodotto_spedizione+" non esiste un listino predefinito." ,&
												"COMDDT", "TRASF", ls_allegati[], ref ls_errore_email)
			destroy luo_mail
		end if
	end if
	
	//valorizzazione con prezzo STORICIZZATO con sconto 50 + 20 oppure con prezzo riga ordine
	//a seconda dei casi ...
	lstr_det_righe[ll_i].prezzo_vendita = ld_prezzo
	
	//CARICO IL RESTO DELLE PROPRIETà NELLA STRUTTURA
	lstr_det_righe[ll_i].anno_registrazione = tab_1.det_2.dw_righe_ordine.getitemnumber(ll_i, "anno_registrazione")
	lstr_det_righe[ll_i].num_registrazione = tab_1.det_2.dw_righe_ordine.getitemnumber(ll_i, "num_registrazione")
	lstr_det_righe[ll_i].prog_riga = tab_1.det_2.dw_righe_ordine.getitemnumber(ll_i, "prog_riga_ord_ven")
	lstr_det_righe[ll_i].cod_prodotto = tab_1.det_2.dw_righe_ordine.getitemstring(ll_i, "cod_prodotto")
	lstr_det_righe[ll_i].quan_ordine = tab_1.det_2.dw_righe_ordine.getitemnumber(ll_i, "quan_ordine")
	
next
//*********************************************************************



//*********************************************************************
//CICLO N°2 Ciclo dedicato al solo Avanzamento Commesse
for ll_i = 1 to tab_1.det_2.dw_righe_ordine.rowcount()

	// -------------- EnMe 10/02/2012: eseguo l'avanzamento della commessa mettendo il flag_scarico_parziale sulla variante specifica.
	ls_cod_prodotto_spedizione = tab_1.det_2.dw_righe_ordine.getitemstring(ll_i, "cod_prodotto")
	ll_anno_registrazione = tab_1.det_2.dw_righe_ordine.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = tab_1.det_2.dw_righe_ordine.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_ord_ven = tab_1.det_2.dw_righe_ordine.getitemnumber(ll_i, "prog_riga_ord_ven")
	ls_flag_chiudi_commessa = tab_1.det_2.dw_righe_ordine.getitemstring(ll_i, "flag_chiudi_commessa")
	
	ls_errore_rif_ordine = "RIGA ORDINE:" + string(ll_anno_registrazione) + "-" + string(ll_num_registrazione) + "-" + string(ll_prog_riga_ord_ven)
	
	select cod_prodotto, 
			anno_commessa, 
			num_commessa,
			num_riga_appartenenza,
			flag_gen_commessa
	into 	:ls_cod_prodotto_finito, 
			:ll_anno_commessa, 
			:ll_num_commessa,
			:ll_num_riga_appartenenza,
			:ls_flag_gen_commessa
	from 	det_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		
	if sqlca.sqlcode <> 0 then
		rollback;
		if isnull(sqlca.sqlerrtext) then sqlca.sqlerrtext = ""
		g_mb.error("Errore in ricerca della riga ordine di origine." + ls_errore_rif_ordine + " Dettaglio "+ sqlca.sqlerrtext)
		return -1
	end if	
	
	if not isnull(ll_anno_commessa) then
		ls_errore_rif_commessa = "COMMESSA: " + string(ll_anno_commessa) + "-" + string(ll_num_commessa)
	end if
	
	if ls_flag_gen_commessa = "S" and ( ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza)) then
				
		if ls_cod_prodotto_spedizione <> ls_cod_prodotto_finito then
			// sono in fase di spedizione di un semilavorato di un prodotto finito
			if isnull(ll_anno_commessa) then
				if g_mb.messagebox("SEP","ATTENZIONE: il prodotto in spedizione è senza una commessa associata. Questo potrebbe poi provocare errori negli scarichi di magazzino; in particolare non verranno gestiti gli scarichi pariziali per stabilimento del semilavorato " + ls_errore_rif_ordine,Question!, YesNo!, 2) = 2 then
					rollback;
					return -1
				end if
			end if
		
			
			//  configurato e quindi eseguo l'avanzamento parziale della commessa 
	
			luo_funzioni_commesse.ib_forza_deposito_scarico_mp=false
			setnull(luo_funzioni_commesse.is_cod_deposito_scarico_mp)
			luo_funzioni_commesse.uof_set_posticipato(true)
			
			//serve per bypassare il controllo esistenza ddt di trasferimento
			//se si accorge che manca il ddt di trasferimento, ma è proprio quello che sto creando io, allora non mandare l'alert di avviso ...
			luo_funzioni_commesse.uof_set_from_ddt_trasf(true, is_cod_deposito_partenza, is_cod_deposito_arrivo, ll_anno_commessa, ll_num_commessa)
			
			ll_ret = luo_funzioni_commesse.uof_avanza_commessa (ll_anno_commessa, ll_num_commessa, ls_cod_prodotto_spedizione, ref ls_errore )	
			
			if ll_ret < 0 then
				rollback;
				g_mb.error("Errore in fase di avanzamento parziale commessa. " + ls_errore_rif_commessa + "; " + ls_errore )
				return -1
			end if
			
		else
			
			if isnull(ll_anno_commessa) then
				if g_mb.messagebox("SEP","ATTENZIONE: il prodotto in spedizione è senza una commessa associata. Questo potrebbe poi provocare errori negli scarichi di magazzino; in particolare non verranno gestiti gli scarichi pariziali per stabilimento del semilavorato " + ls_errore_rif_ordine,Question!, YesNo!, 2) = 2 then
					rollback;
					return -1
				end if
			end if
			// sono in fase di spedizione di un prodotto finito e quindi eseguo l'avanzamento della commessa 
			
			// ATTENZIONE: in questo caso vado a fare una forzatura in modo che tutti gli scarichi avvengano sul deposito che sta eseguendo
			// la spedizione e non nel deposito indicato nella commessa.
			
			luo_funzioni_commesse.ib_forza_deposito_scarico_mp=true
			luo_funzioni_commesse.is_cod_deposito_scarico_mp =  tab_1.det_1.dw_1.getitemstring( tab_1.det_1.dw_1.getrow(),"cod_deposito_partenza")
			luo_funzioni_commesse.uof_set_posticipato(true)
			
			//serve per bypassare il controllo esistenza ddt di trasferimento
			//se si accorge che manca il ddt di trasferimento, ma è proprio quello che sto creando io, allora non mandare l'alert di avviso ...
			luo_funzioni_commesse.uof_set_from_ddt_trasf(true, is_cod_deposito_partenza, is_cod_deposito_arrivo, ll_anno_commessa, ll_num_commessa)
	
			ll_ret = luo_funzioni_commesse.uof_avanza_commessa (ll_anno_commessa, ll_num_commessa, ls_null, ref ls_errore )	
			
			if ll_ret < 0 then
				rollback;
				g_mb.error("Errore in fase di avanzamento parziale commessa. " + ls_errore_rif_commessa + " " + ls_errore)
				return -1
			end if
		end if
	elseif ls_flag_gen_commessa = "S" and ls_flag_chiudi_commessa = "S"  then
		// stefanop 25/05/2012: chiudo commessa dei prodotti OPTIONALS
		// ATTENZIONE: in questo caso vado a fare una forzatura in modo che tutti gli scarichi avvengano sul deposito che sta eseguendo
		// la spedizione e non nel deposito indicato nella commessa.
		
		luo_funzioni_commesse.ib_forza_deposito_scarico_mp = true
		luo_funzioni_commesse.is_cod_deposito_scarico_mp =  tab_1.det_1.dw_1.getitemstring( tab_1.det_1.dw_1.getrow(),"cod_deposito_partenza")
		luo_funzioni_commesse.uof_set_posticipato(true)
		
		//serve per bypassare il controllo esistenza ddt di trasferimento
		//se si accorge che manca il ddt di trasferimento, ma è proprio quello che sto creando io, allora non mandare l'alert di avviso ...
		luo_funzioni_commesse.uof_set_from_ddt_trasf(true, is_cod_deposito_partenza, is_cod_deposito_arrivo, ll_anno_commessa, ll_num_commessa)

		if luo_funzioni_commesse.uof_avanza_commessa (ll_anno_commessa, ll_num_commessa, ls_null, ref ls_errore) < 0 then
			rollback;
			g_mb.error("Errore in fase di avanzamento parziale commessa. " + ls_errore_rif_commessa + " " + ls_errore)
			return -1
		end if
			
			
	end if	
	// -------------- Fine EnMe 10/2/2012 -----------------------
	
	
next
//*********************************************************************

		
ll_ret = luo_generazione_documenti.uof_crea_bolla_trasferimento(is_cod_deposito_partenza, is_cod_deposito_arrivo, lstr_det_righe, ref ll_anno_bolla,ref ll_num_bolla, ref ls_errore)

// verifico se l'uscita è dovuta ad un errore e al limite faccio rollback di tutto.
if ll_ret <> 0 then
	rollback;
	g_mb.messagebox("APICE","Errore durante la generazione della bolla.~r~n" + ls_errore)
else
	commit;
	if g_mb.confirm("APICE","E' stata generata la bolla " + string(ll_anno_bolla) + "-" + string(ll_num_bolla) + ": PROCEDO CON LA STAMPA?" ) then
	
	// procedo con la stampa  del DDT
	wf_stampa_ddt(ll_anno_bolla, ll_num_bolla, ls_errore)
	// ******************* fine stampa bolla	
	end if

end if

destroy luo_generazione_documenti
destroy luo_funzioni_commesse

return 1
end function

public subroutine wf_log_barcode (long al_colore, string as_messaggio);/**
 * stefanop
 * 27/01/2012
 *
 * Imposto il colore di backgroud del barcode e se necessario visualizzo un messaggio di log
 **/
 
tab_1.det_2.dw_barcode.Modify("barcode.Background.Color='" + string(al_colore) + "'")
tab_1.det_2.dw_barcode.setitem(1, "log", as_messaggio)
end subroutine

public function integer wf_carica_ordine (long al_anno_registrazione, long al_num_registrazione, ref string as_messaggio);/**
 * stefnaop
 * 05/01/2012
 *
 * Carico tutte le righe dell'ordine tramite il barcode della testata
 **/
 
string ls_sql, ls_errore
long ll_rows, ll_i, ll_prog_riga_ord_ven, ll_ret
datastore lds_store

setnull(as_messaggio)

ls_sql = "SELECT prog_riga_ord_ven FROM det_ord_ven WHERE cod_azienda='"+ s_cs_xx.cod_azienda + "' "
ls_sql += " AND anno_registrazione=" + string(al_anno_registrazione) + " AND num_registrazione=" + string(al_num_registrazione)
ls_sql += " ORDER BY prog_riga_ord_ven"

ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_errore)

if ll_rows < 0 then
	g_mb.error(ls_errore)
	return -1
elseif ll_rows = 0 then
	return 0
end if

for ll_i = 1 to ll_rows
	
	ll_prog_riga_ord_ven = lds_store.getitemnumber(ll_i, 1)
	
	ll_ret = wf_carica_riga_ordine(al_anno_registrazione, al_num_registrazione, ll_prog_riga_ord_ven, ref as_messaggio)
	if ll_ret < 0 then
		return -1
	end if		
next

return ll_rows
end function

public function boolean wf_check_riga (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga, string as_cod_prodotto);string ls_cod_prodotto
long		ll_i, ll_anno, ll_num, ll_riga


for ll_i = 1 to tab_1.det_2.dw_righe_ordine.rowcount()
	
	ll_anno = tab_1.det_2.dw_righe_ordine.getitemnumber(ll_i,"anno_registrazione")
	ll_num = tab_1.det_2.dw_righe_ordine.getitemnumber(ll_i,"num_registrazione")
	ll_riga = tab_1.det_2.dw_righe_ordine.getitemnumber(ll_i,"prog_riga_ord_ven")
	ls_cod_prodotto = tab_1.det_2.dw_righe_ordine.getitemstring(ll_i,"cod_prodotto")
	
	if ll_anno = al_anno_registrazione and ll_num = al_num_registrazione and ll_riga = al_prog_riga and ls_cod_prodotto = as_cod_prodotto  then
		return true
	end if
	
next

return false
end function

public function integer wf_carica_righe_collegate (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ordine, string as_cod_prodotto, ref string as_errore);string ls_sql, ls_tipo_det_ven_add, ls_cod_tipo_ord_ven
long ll_rows, ll_riga, ll_i
datastore lds_store

// recupero il tipo dettaglio vendita Addizionali per escluderle dall'importazione
select cod_tipo_ord_ven
into 	:ls_cod_tipo_ord_ven
from	tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione;

select cod_tipo_det_ven_addizionali
into 	:ls_tipo_det_ven_add
from	tab_flags_conf_tipi_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_modello = :as_cod_prodotto and
		 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven ;

if sqlca.sqlcode = 100 then

	select cod_tipo_det_ven_addizionali
	into :ls_tipo_det_ven_add
	from tab_flags_configuratore
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_modello = :as_cod_prodotto;
			 
	if sqlca.sqlcode < 0 then
		as_errore = "Errore durante il recupero del tipo dettaglio vendita Addizionali.~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
elseif sqlca.sqlcode = -1 then
			 
	if sqlca.sqlcode < 0 then
		as_errore = "Errore durante il recupero del tipo dettaglio vendita Addizionali.~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
end if	
	
//if isnull(ls_tipo_det_ven_add) or len(ls_tipo_det_ven_add) < 1 then ls_tipo_det_ven_add= ""
		 
ls_sql = "SELECT prog_riga_ord_ven, " + &
			"cod_tipo_det_ven, " + &
			"cod_prodotto, " + &
			"des_prodotto, " + &
			"quan_ordine, " + &
			"quantita_um, " + &
			"prezzo_vendita, " + &
			"sconto_1, " + &
			"sconto_2, " + &
			"prezzo_um, " + &
			"quan_in_evasione " + &
			"FROM det_ord_ven WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND " + &
			"anno_registrazione=" + string(al_anno_registrazione) + " AND num_registrazione=" + string(al_num_registrazione) + &
			" AND num_riga_appartenenza=" + string(al_prog_riga_ordine)
	
// Escludo le righe addizionali
if not isnull(ls_tipo_det_ven_add) and ls_tipo_det_ven_add <> "" then
	ls_sql += " AND cod_tipo_det_ven <> '" + ls_tipo_det_ven_add + "' "
end if

ls_sql += " ORDER BY prog_riga_ord_ven"
			

ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_errore)

if ll_rows < 0 then
	return -1
end if

for ll_i = 1 to ll_rows
	
	// è un addizionale? allora salto
	// calcolo spostato direttamente nella query.
	//if ls_tipo_det_ven_add = lds_store.getitemstring(ll_i, "cod_tipo_det_ven") then continue
	
	ll_riga = tab_1.det_2.dw_righe_ordine.insertrow(0)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "anno_registrazione", al_anno_registrazione)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "num_registrazione", al_num_registrazione)
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "prog_riga_ord_ven", lds_store.getitemnumber(ll_i, "prog_riga_ord_ven"))
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "cod_tipo_det_ven", lds_store.getitemstring(ll_i, "cod_tipo_det_ven"))
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "cod_prodotto",  lds_store.getitemstring(ll_i, "cod_prodotto"))
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "des_prodotto", lds_store.getitemstring(ll_i, "des_prodotto"))

	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "quan_ordine",  lds_store.getitemnumber(ll_i, "quan_ordine"))
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "quantita_um", lds_store.getitemnumber(ll_i, "quantita_um"))
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "prezzo_vendita", lds_store.getitemnumber(ll_i, "prezzo_vendita"))
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "sconto_1", lds_store.getitemnumber(ll_i, "sconto_1"))
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "sconto_2", lds_store.getitemnumber(ll_i, "sconto_2"))
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "prezzo_um", lds_store.getitemnumber(ll_i, "prezzo_um"))
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "quan_in_evasione", lds_store.getitemnumber(ll_i, "quan_in_evasione"))
	tab_1.det_2.dw_righe_ordine.setitem(ll_riga, "flag_chiudi_commessa", "S")
	
	
next

return 1
end function

public function integer wf_stampa_ddt (long al_anno_bolla, long al_num_bolla, ref string as_messaggio);long     ll_anno_reg_mov, ll_num_reg_mov, ll_progr_stock, ll_prog_registrazione, ll_anno_registrazione, ll_num_registrazione, ll_num_documento, ll_count

string   ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_cod_valuta, ls_test, ls_cod_documento, ls_cod_cliente
string	  ls_window = "w_report_bol_ven_euro"

datetime ldt_data_stock

window   lw_window

s_cs_xx.parametri.parametro_i_1 = 0
s_cs_xx.parametri.parametro_d_1 = al_anno_bolla
s_cs_xx.parametri.parametro_d_2 = al_num_bolla

declare cu_det_bol_ven cursor for
 select anno_reg_des_mov,
	     num_reg_des_mov
   from det_bol_ven
  where cod_azienda = :s_cs_xx.cod_azienda
    and anno_registrazione = :s_cs_xx.parametri.parametro_d_1
	 and num_registrazione = :s_cs_xx.parametri.parametro_d_2;

open cu_det_bol_ven;

do while true
	fetch cu_det_bol_ven into :ll_anno_reg_mov, :ll_num_reg_mov;
   
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Errore", "Anno registrazione " + string(s_cs_xx.parametri.parametro_d_1) + "  num_registrazione " + string(s_cs_xx.parametri.parametro_d_2) + " non esistono in DET_BOL_VEV")
		exit
	end if	

	if sqlca.sqlcode = 0 then
		declare cu_des_mov_magazzino cursor for
		 select cod_deposito,
				  cod_ubicazione,
				  cod_lotto,
				  data_stock,
				  prog_stock,
				  prog_registrazione
			from dest_mov_magazzino	  
		  where cod_azienda = :s_cs_xx.cod_azienda
			 and anno_registrazione = :ll_anno_reg_mov
			 and num_registrazione = :ll_num_reg_mov;
			 
		open cu_des_mov_magazzino;
		
		do while true
			fetch cu_des_mov_magazzino into :ls_cod_deposito, :ls_cod_ubicazione, :ls_cod_lotto, :ldt_data_stock, :ll_progr_stock, :ll_prog_registrazione;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Errore", "Non è stato trovato nessun movimento di destinazione magazzino in corrispondenza dell'anno registrazione " + string(ll_anno_reg_mov) + " e del numero registrazione " + string(ll_num_reg_mov))
				exit
			end if
			if sqlca.sqlcode = 0 then
				if isnull(ls_cod_deposito) or isnull(ls_cod_ubicazione) or isnull(ls_cod_lotto) or &
					isnull(ldt_data_stock) or isnull(ll_progr_stock) then
					g_mb.messagebox("Attenzione", "Attenzione nella tabella DES_MOV_MAGAZZINO i dati dello stock corrispondenti all'anno registrazione " + string(ll_anno_reg_mov) + ", numero registrazione " + string(ll_num_reg_mov) + " e prog. registrazione " + string(ll_prog_registrazione) + " sono incompleti")
				end if
			end if	
		loop
		
		close cu_des_mov_magazzino;
	end if
loop
close cu_det_bol_ven;

// il calcolo del DDT non serve in quanto già eseguito in fase gi generazione del documento.
	
ll_anno_registrazione = al_anno_bolla
ll_num_registrazione = al_num_bolla

select cod_documento,
		 num_documento,
		 cod_cliente
into   :ls_cod_documento,
		 :ll_num_documento,
		 :ls_cod_cliente
from   tes_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_registrazione and
		 num_registrazione = :ll_num_registrazione;

if sqlca.sqlcode = 0 then
	
	// *** controllo se ci sono addebiti
	
	select count(*)
	into   :ll_count
	from   anag_clienti_addebiti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_cliente = :ls_cod_cliente and
			 flag_mod_valore_bolle = 'S';
			 
	if not isnull(ll_count) and ll_count > 0 then
	
		if isnull(ls_cod_documento) and ( isnull(ll_num_documento) or ll_num_documento = 0) then
			s_cs_xx.parametri.parametro_d_1 = ll_anno_registrazione
			s_cs_xx.parametri.parametro_d_2 = ll_num_registrazione	
			window_open(w_mod_importi_bol_ven, 0)
			setnull(s_cs_xx.parametri.parametro_d_1) 
			setnull(s_cs_xx.parametri.parametro_d_2)
		end if	
	end if
	
end if		


if isnull(ls_cod_documento) then
	window_open(w_tipo_stampa_bol_ven, 0)
end if

if s_cs_xx.parametri.parametro_i_1 <> -1 then
	
	select stringa
	into   :ls_test
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_parametro = 'BVE';
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore nella select di parametri_azienda: " + sqlca.sqlerrtext)
		return -1
	elseif sqlca.sqlcode = 100 then		
		window_open(w_report_bol_ven, -1)
	elseif sqlca.sqlcode = 0 then			
		window_type_open(lw_window,ls_window, -1)
	end if
	
end if
end function

on w_evas_ordini_barcode_trasf.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_evas_ordini_barcode_trasf.destroy
call super::destroy
destroy(this.tab_1)
end on

event pc_setwindow;call super::pc_setwindow;// Posiziono oggetti
tab_1.move(20,20)
tab_1.det_1.dw_1.move(20,20)
tab_1.det_2.dw_righe_ordine.move(0,0)
tab_1.det_2.dw_barcode.x = 20
// ---

tab_1.det_1.dw_1.insertrow(0)
tab_1.det_2.dw_barcode.insertrow(0)
tab_1.det_3.dw_dati_bolla.insertrow(0)

wf_step(1)


event post ue_filtra_deposito_partenza()
end event

event resize;/* TOLTO ANCESTOR SCRIPT */
tab_1.resize(newwidth - 40, newheight - 40)

setredraw(true)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(tab_1.det_1.dw_1, &
							  "cod_deposito_partenza", &
							  sqlca, &
							  "anag_depositi", &
							  "cod_deposito", &
							  "des_deposito", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(tab_1.det_1.dw_1, &
							  "cod_deposito_finale", &
							  sqlca, &
							  "anag_depositi", &
							  "cod_deposito", &
							  "des_deposito", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
							  
f_po_loaddddw_dw(tab_1.det_1.dw_1, &
							  "cod_tipo_bol_ven", &
							  sqlca, &
							  "tab_tipi_bol_ven", &
							  "cod_tipo_bol_ven", &
							  "des_tipo_bol_ven", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
		
end event

type tab_1 from tab within w_evas_ordini_barcode_trasf
event create ( )
event destroy ( )
event ue_resize pbm_size
integer x = 23
integer y = 20
integer width = 4480
integer height = 1912
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean fixedwidth = true
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
det_1 det_1
det_2 det_2
det_3 det_3
end type

on tab_1.create
this.det_1=create det_1
this.det_2=create det_2
this.det_3=create det_3
this.Control[]={this.det_1,&
this.det_2,&
this.det_3}
end on

on tab_1.destroy
destroy(this.det_1)
destroy(this.det_2)
destroy(this.det_3)
end on

event ue_resize;//det_1.cb_passo_1.move(det_1.width - det_1.cb_passo_1.width - 40, det_1.height - det_1.cb_passo_1.height - 40)

det_2.dw_barcode.y = det_2.height - det_2.dw_barcode.height - 20
det_2.dw_righe_ordine.resize(det_2.width, det_2.dw_barcode.y - 40)
det_2.cb_passo_2.move(det_2.width - det_2.cb_passo_2.width - 40, det_2.dw_barcode.y)
det_2.dw_barcode.width = det_2.cb_passo_2.x - 40
det_3.cb_genera_ddt.move(det_2.cb_passo_2.x, det_2.cb_passo_2.y)

//dw_righe_ordine.setredraw(true)
end event

event selectionchanging;if not wf_step(newindex, false) then
	return 1
end if
end event

type det_1 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4443
integer height = 1784
long backcolor = 12632256
string text = "Passo 1"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_1 dw_1
end type

on det_1.create
this.dw_1=create dw_1
this.Control[]={this.dw_1}
end on

on det_1.destroy
destroy(this.dw_1)
end on

type dw_1 from uo_std_dw within det_1
integer x = -14
integer y = 8
integer width = 2103
integer height = 460
integer taborder = 20
string dataobject = "d_evas_ordini_barcode_trasf_1"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_avanti"
		tab_1.selecttab(2)
		
end choose
end event

type det_2 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4443
integer height = 1784
long backcolor = 12632256
string text = "Passo 2"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_passo_2 cb_passo_2
dw_barcode dw_barcode
dw_righe_ordine dw_righe_ordine
end type

on det_2.create
this.cb_passo_2=create cb_passo_2
this.dw_barcode=create dw_barcode
this.dw_righe_ordine=create dw_righe_ordine
this.Control[]={this.cb_passo_2,&
this.dw_barcode,&
this.dw_righe_ordine}
end on

on det_2.destroy
destroy(this.cb_passo_2)
destroy(this.dw_barcode)
destroy(this.dw_righe_ordine)
end on

type cb_passo_2 from commandbutton within det_2
integer x = 4050
integer y = 1676
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Avanti"
end type

event clicked;if dw_righe_ordine.rowcount() > 0 then
	tab_1.selecttab(3)
end if


end event

type dw_barcode from uo_std_dw within det_2
event ue_key pbm_dwnkey
integer x = 9
integer y = 1628
integer width = 3109
integer height = 120
integer taborder = 20
string dataobject = "d_evas_ordini_barcode_trasf_2_1"
boolean border = false
end type

event ue_key;choose case key
	case keyenter!
		string ls_barcode,ls_anno_registrazione,ls_num_registrazione,ls_prog_riga_ord_ven,ls_messaggio
		long   ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven, ll_ret
		
		ls_barcode = gettext( )
		ls_barcode = upper(ls_barcode)
		
		// processo il barcode
		wf_leggi_barcode(ls_barcode)
		
end choose
end event

type dw_righe_ordine from uo_std_dw within det_2
event ue_posiziona_ultima_riga ( )
integer width = 4411
integer height = 1500
integer taborder = 20
string dataobject = "d_evas_ordini_barcode_trasf_2"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event ue_posiziona_ultima_riga();scrolltorow( rowcount() )
end event

type det_3 from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4443
integer height = 1784
long backcolor = 12632256
string text = "Passo 3"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
cb_genera_ddt cb_genera_ddt
dw_dati_bolla dw_dati_bolla
end type

on det_3.create
this.cb_genera_ddt=create cb_genera_ddt
this.dw_dati_bolla=create dw_dati_bolla
this.Control[]={this.cb_genera_ddt,&
this.dw_dati_bolla}
end on

on det_3.destroy
destroy(this.cb_genera_ddt)
destroy(this.dw_dati_bolla)
end on

type cb_genera_ddt from commandbutton within det_3
integer x = 3895
integer y = 1652
integer width = 393
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Genera DDT"
end type

event clicked;if wf_genera_ddt() = 1 then
	
	// ho generato correttamente il DDT; non posso più usare il pulsante
	enabled = false
	
end if
end event

type dw_dati_bolla from uo_std_dw within det_3
integer width = 3589
integer height = 1292
integer taborder = 20
string dataobject = "d_evas_ordini_barcode_trasf_3"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_cod_causale

choose case dwo.name
	case "cod_tipo_bolla"
		if not isnull(data) and len(data) > 0 then
			
			select cod_causale
			into :ls_cod_causale
			from tab_tipi_bol_ven
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_bol_ven = :data;
			
			setitem(1, "cod_causale", ls_cod_causale)
			
		end if
end choose
end event

