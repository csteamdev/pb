﻿$PBExportHeader$w_imposta_logo_etichetta.srw
forward
global type w_imposta_logo_etichetta from window
end type
type mle_1 from multilineedit within w_imposta_logo_etichetta
end type
type st_logo from statictext within w_imposta_logo_etichetta
end type
type cb_annulla from commandbutton within w_imposta_logo_etichetta
end type
type cb_conferma from commandbutton within w_imposta_logo_etichetta
end type
type p_logo from picture within w_imposta_logo_etichetta
end type
type ddlb_1 from dropdownlistbox within w_imposta_logo_etichetta
end type
end forward

global type w_imposta_logo_etichetta from window
integer width = 1573
integer height = 1028
boolean titlebar = true
string title = "Imposta Logo Etichette"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
mle_1 mle_1
st_logo st_logo
cb_annulla cb_annulla
cb_conferma cb_conferma
p_logo p_logo
ddlb_1 ddlb_1
end type
global w_imposta_logo_etichetta w_imposta_logo_etichetta

type variables
string				is_logostandard = "<LOGO STANDARD>", is_path_cliente = "", is_path_logo_standard="", is_logo_corrente=""

integer			ii_anno
long				il_numero
end variables

forward prototypes
public subroutine wf_preview_immagine ()
public function integer wf_imposta_ddlb (ref string as_errore)
end prototypes

public subroutine wf_preview_immagine ();string			ls_file

setpointer(Hourglass!)

ls_file = ddlb_1.text
if ls_file=is_logostandard or g_str.isempty(ls_file) then
	//path del logo standard
	ls_file = is_path_logo_standard
	
else
	ls_file = is_path_cliente + ls_file
end if



if fileExists(ls_file) then
	p_logo.picturename = ls_file
	mle_1.text = ""
else
	p_logo.picturename = ""
	mle_1.text = "File inesistente in " + ls_file
end if

setpointer(Arrow!)


return
end subroutine

public function integer wf_imposta_ddlb (ref string as_errore);string			ls_condizione, ls_files_trovati[]
long			ll_index, ll_tot

//leggo la lista dei fle presenti nel perocrso


ls_condizione = "*.*"
//ls_condizione = auo_params.uof_get_string(3, "*.*")


ll_tot = guo_functions.uof_estrai_files(is_path_cliente, ls_condizione, ls_files_trovati[], as_errore)

if ll_tot < 0 then
	as_errore = "Errore durante l'estrazione dei files: " + as_errore
	return -1
elseif ll_tot = 0 then
	//nessun file presente
end if


ddlb_1.additem(is_logostandard)

for ll_index=1 to ll_tot
	ddlb_1.additem(ls_files_trovati[ll_index])
next

ddlb_1.text = is_logo_corrente

return 0
end function

on w_imposta_logo_etichetta.create
this.mle_1=create mle_1
this.st_logo=create st_logo
this.cb_annulla=create cb_annulla
this.cb_conferma=create cb_conferma
this.p_logo=create p_logo
this.ddlb_1=create ddlb_1
this.Control[]={this.mle_1,&
this.st_logo,&
this.cb_annulla,&
this.cb_conferma,&
this.p_logo,&
this.ddlb_1}
end on

on w_imposta_logo_etichetta.destroy
destroy(this.mle_1)
destroy(this.st_logo)
destroy(this.cb_annulla)
destroy(this.cb_conferma)
destroy(this.p_logo)
destroy(this.ddlb_1)
end on

event open;
s_cs_xx_parametri					lstr_parametri
string									ls_logo, ls_cod_cliente, ls_LET, ls_errore



lstr_parametri = message.powerobjectparm

ii_anno = lstr_parametri.parametro_ul_1
il_numero = lstr_parametri.parametro_ul_2

select logo_etichetta, cod_cliente
into :is_logo_corrente, :ls_cod_cliente
from tes_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ii_anno and
			num_registrazione=:il_numero;

if g_str.isempty(is_logo_corrente) then is_logo_corrente = is_logostandard

is_path_cliente = s_cs_xx.volume + s_cs_xx.risorse + "loghi_clienti\"+ls_cod_cliente+"\"

//path del logo standard
guo_functions.uof_get_parametro_azienda("LET", ls_LET)

if g_str.isempty(ls_LET) then
	is_path_logo_standard = ""
else
	is_path_logo_standard = s_cs_xx.volume + ls_LET
end if

if wf_imposta_ddlb(ls_errore) < 0 then
	g_mb.error(ls_errore)
end if

wf_preview_immagine()

end event

type mle_1 from multilineedit within w_imposta_logo_etichetta
integer x = 50
integer y = 544
integer width = 1495
integer height = 376
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean vscrollbar = true
boolean autovscroll = true
end type

type st_logo from statictext within w_imposta_logo_etichetta
integer x = 50
integer y = 32
integer width = 1435
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Seleziona l~'etichetta da utilizzare:"
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_imposta_logo_etichetta
integer x = 1001
integer y = 336
integer width = 357
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;s_cs_xx_parametri				lstr_param

lstr_param.parametro_b_1 = false
closewithreturn(parent, lstr_param)
end event

type cb_conferma from commandbutton within w_imposta_logo_etichetta
integer x = 590
integer y = 336
integer width = 357
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;s_cs_xx_parametri				lstr_param
string								ls_logo_etichette, ls_errore, ls_path_control


ls_logo_etichette = ddlb_1.text

if ls_logo_etichette = is_logostandard then
	ls_path_control = is_path_logo_standard
	ls_logo_etichette = ""
else
	ls_path_control = is_path_cliente + ls_logo_etichette
end if

//------------------------------------------------------------------------------------------------
if fileExists(ls_path_control) then
	//il file esiste: OK
else
	//il file non esiste, avvisa prima di continuare
	if not g_mb.confirm("Il file selezionato non esiste nel percorso stabilito! Sicuro di voler salvare?") then return
end if

//------------------------------------------------------------------------------------------------
//aggiorna
update tes_ord_ven
set logo_etichetta = :ls_logo_etichette
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ii_anno and
			num_registrazione = :il_numero;

if sqlca.sqlcode < 0 then
	ls_errore = sqlca.sqlerrtext
	rollback;
	g_mb.error("Errore in aggiornamento campo etichetta: "+ls_errore)
	
	return
end if

commit;

lstr_param.parametro_b_1 = true
lstr_param.parametro_s_1 = ls_logo_etichette

closewithreturn(parent, lstr_param)

end event

type p_logo from picture within w_imposta_logo_etichetta
integer x = 59
integer y = 260
integer width = 439
integer height = 244
boolean focusrectangle = false
end type

type ddlb_1 from dropdownlistbox within w_imposta_logo_etichetta
integer x = 32
integer y = 124
integer width = 1477
integer height = 732
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;
wf_preview_immagine()
end event

