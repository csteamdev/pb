﻿$PBExportHeader$w_packing_list_gibus.srw
forward
global type w_packing_list_gibus from w_packing_list
end type
end forward

global type w_packing_list_gibus from w_packing_list
end type
global w_packing_list_gibus w_packing_list_gibus

on w_packing_list_gibus.create
call super::create
end on

on w_packing_list_gibus.destroy
call super::destroy
end on

event ue_imposta_dw();call super::ue_imposta_dw;if s_cs_xx.parametri.parametro_b_1 then
	dw_selezione_pl.setitem(dw_selezione_pl.getrow(),"anno_registrazione",s_cs_xx.parametri.parametro_d_1)
	dw_selezione_pl.setitem(dw_selezione_pl.getrow(),"num_registrazione",s_cs_xx.parametri.parametro_d_2)
	cb_aggiorna.postevent("clicked")
	f_PO_LoadDDDW_DW(dw_selezione_pl,"num_pl",sqlca,"tes_ord_ven_pack_list","num_pack_list",&
						  "num_pack_list","cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " &
						  + string(s_cs_xx.parametri.parametro_d_1) + " and num_registrazione = " + &
						  + string(s_cs_xx.parametri.parametro_d_2))
end if

setnull(s_cs_xx.parametri.parametro_b_1)
setnull(s_cs_xx.parametri.parametro_d_1)
setnull(s_cs_xx.parametri.parametro_d_2)
end event

type cb_aggiorna from w_packing_list`cb_aggiorna within w_packing_list_gibus
end type

type dw_selezione_pl from w_packing_list`dw_selezione_pl within w_packing_list_gibus
end type

type cb_cancella from w_packing_list`cb_cancella within w_packing_list_gibus
end type

type dw_packing_list from w_packing_list`dw_packing_list within w_packing_list_gibus
end type

type dw_colli from w_packing_list`dw_colli within w_packing_list_gibus
end type

type dw_report_pl from w_packing_list`dw_report_pl within w_packing_list_gibus
end type

type cb_avanti from w_packing_list`cb_avanti within w_packing_list_gibus
end type

type st_1 from w_packing_list`st_1 within w_packing_list_gibus
end type

type dw_eti_colli from w_packing_list`dw_eti_colli within w_packing_list_gibus
end type

type cb_salva from w_packing_list`cb_salva within w_packing_list_gibus
end type

type cb_ritorna from w_packing_list`cb_ritorna within w_packing_list_gibus
end type

type cb_modifica from w_packing_list`cb_modifica within w_packing_list_gibus
end type

type cb_indietro from w_packing_list`cb_indietro within w_packing_list_gibus
end type

type cb_stampa from w_packing_list`cb_stampa within w_packing_list_gibus
end type

type cb_elimina from w_packing_list`cb_elimina within w_packing_list_gibus
end type

type cb_stampa_pl from w_packing_list`cb_stampa_pl within w_packing_list_gibus
end type

event cb_stampa_pl::clicked;long 	  ll_num_pl, ll_anno_registrazione, ll_num_registrazione, ll_num_collo, ll_prog_riga,&
	  	  ll_riga, ll_num_collo_prec[], ll_i, ll_j
	  
dec{4}  ld_quan_pl, ld_altezza, ld_larghezza, ld_profondita, ld_peso, ld_tot_volume, ld_tot_peso, &
        ld_dim_x, ld_dim_y, ld_dim_z
		 
string  ls_cod_cliente, ls_rag_soc, ls_indirizzo, ls_localita, ls_cap, ls_provincia, ls_nazione,&
        ls_cod_prodotto, ls_des_prodotto, ls_misura, ls_rag_soc_dest, ls_indirizzo_dest, ls_localita_dest, &
		  ls_cap_dest, ls_provincia_dest, ls_dimensioni, ls_nota_dettaglio
		 
boolean lb_fatto


ll_num_pl = dw_selezione_pl.getitemnumber(1,"num_pl")
is_stampa = "pl"
ld_tot_volume = 0
ld_tot_peso = 0
ll_i = 1

dw_report_pl.reset()

select anno_registrazione, num_registrazione
into :ll_anno_registrazione, :ll_num_registrazione
from tes_ord_ven_pack_list
where cod_azienda = :s_cs_xx.cod_azienda and
		num_pack_list = :ll_num_pl;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di tes_ord_ven_pack_list. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

select cod_cliente, rag_soc_1, indirizzo, localita, cap, provincia
into :ls_cod_cliente, :ls_rag_soc_dest, :ls_indirizzo_dest, :ls_localita_dest, :ls_cap_dest, :ls_provincia_dest
from tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di tes_ord_ven. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

select rag_soc_1, indirizzo, localita, cap, provincia
into :ls_rag_soc, :ls_indirizzo, :ls_localita, :ls_cap, :ls_provincia
from anag_clienti
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di anag_clienti. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

select des_nazione
into :ls_nazione
from tab_nazioni
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_nazione = (select cod_nazione from anag_clienti where cod_azienda = :s_cs_xx.cod_azienda and cod_cliente = :ls_cod_cliente);

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di tab_nazioni. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

if (ls_rag_soc_dest = "" or isnull(ls_rag_soc_dest)) and &
	(ls_indirizzo_dest = "" or isnull(ls_indirizzo_dest)) and &
	(ls_localita_dest = "" or isnull(ls_localita_dest)) and &
	(ls_cap_dest = "" or isnull(ls_cap_dest)) and &
	(ls_provincia_dest = "" or isnull(ls_provincia_dest)) then
	ls_rag_soc_dest = ls_rag_soc
	ls_indirizzo_dest = ls_indirizzo
	ls_localita_dest = ls_localita
	ls_cap_dest = ls_cap
	ls_provincia_dest = ls_provincia
end if	

declare report_pl cursor for
select quan_pack_list, num_collo, prog_riga_ord_ven
from det_ord_ven_pack_list
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione and
		num_pack_list = :ll_num_pl
order by num_collo;

open report_pl;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella open del cursore report_pl. Dettaglio = " + sqlca.sqlerrtext)
	return -1
end if

do while true

	lb_fatto = false
	
	fetch report_pl into :ld_quan_pl, :ll_num_collo, :ll_prog_riga;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella fetch del cursore report_pl. Dettaglio = " + sqlca.sqlerrtext)
		close report_pl;
		rollback;
		return -1
	elseif sqlca.sqlcode = 100 then
		close report_pl;
		exit
	end if
	
	select cod_prodotto, des_prodotto, nota_dettaglio
	into :ls_cod_prodotto, :ls_des_prodotto, :ls_nota_dettaglio
	from det_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di det_ord_ven. Dettaglio = " + sqlca.sqlerrtext)
		close report_pl;
		return -1
	end if
	
	select dim_x, dim_y, dim_z
	into   :ld_dim_x, :ld_dim_y, :ld_dim_z
	from   comp_det_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di comp_det_ord_ven. Dettaglio = " + sqlca.sqlerrtext)
		close report_pl;
		return -1
	end if
	if sqlca.sqlcode = 100 then
		ld_dim_x =0
		ld_dim_y =0
		ld_dim_z =0
	end if
	
	ls_dimensioni = ""
	if ld_dim_x > 0 then
		ls_dimensioni = ls_dimensioni + string(int(ld_dim_x))
	end if
	if len(ls_dimensioni) > 0 and ld_dim_y > 0 then ls_dimensioni = ls_dimensioni +"x"
	if ld_dim_y > 0 then
		ls_dimensioni = ls_dimensioni + string(int(ld_dim_y))
	end if
	if len(ls_dimensioni) > 0 and ld_dim_z > 0 then ls_dimensioni = ls_dimensioni +"x"
	if ld_dim_z > 0 then
		ls_dimensioni = ls_dimensioni + string(int(ld_dim_z))
	end if
		
	
	select cod_misura_mag
	into :ls_misura
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di anag_prodotti. Dettaglio = " + sqlca.sqlerrtext)
		close report_pl;
		return -1
	end if

	select larghezza, altezza, profondita, peso
	into :ld_larghezza, :ld_altezza, :ld_profondita, :ld_peso
	from tes_ord_ven_pack_list_colli
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			num_pack_list = :ll_num_pl and
			num_collo = :ll_num_collo;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di tes_ord_ven_pack_list_colli. Dettaglio = " + sqlca.sqlerrtext)
		close report_pl;
		return -1
	end if
	
	for ll_j = 1 to upperbound(ll_num_collo_prec)
		if ll_num_collo = ll_num_collo_prec[ll_j] then
			lb_fatto = true
		end if
	next	

	if lb_fatto = false then
		ld_tot_volume = ld_tot_volume + (ld_altezza*ld_larghezza*ld_profondita/1000000)
		ld_tot_peso = ld_tot_peso + ld_peso
		ll_num_collo_prec[ll_i] = ll_num_collo
		ll_i++
	end if	

	if len(trim(ls_dimensioni)) > 0 then ls_des_prodotto = ls_des_prodotto + "  " + ls_dimensioni
	
	if len(trim(ls_nota_dettaglio)) > 0 then ls_des_prodotto = ls_des_prodotto + "~r~n" + ls_nota_dettaglio

	ll_riga = dw_report_pl.insertrow(0)
	dw_report_pl.setitem(ll_riga,"rag_soc",ls_rag_soc)
	dw_report_pl.setitem(ll_riga,"indirizzo",ls_indirizzo)
	dw_report_pl.setitem(ll_riga,"indirizzo_2",ls_cap + " "+ ls_localita + " (" + ls_provincia + ")")
	dw_report_pl.setitem(ll_riga,"nazione",ls_nazione)
	dw_report_pl.setitem(ll_riga,"rag_soc_dest",ls_rag_soc_dest)
	dw_report_pl.setitem(ll_riga,"indirizzo_dest",ls_indirizzo_dest)
	dw_report_pl.setitem(ll_riga,"indirizzo_2_dest",ls_cap_dest + " "+ ls_localita_dest + " (" + ls_provincia_dest + ")")
	dw_report_pl.setitem(ll_riga,"nazione_dest",ls_nazione)
	dw_report_pl.setitem(ll_riga,"num_ordine",ll_num_registrazione)
	dw_report_pl.setitem(ll_riga,"num_collo",ll_num_collo)
	dw_report_pl.setitem(ll_riga,"altezza",ld_altezza)
	dw_report_pl.setitem(ll_riga,"larghezza",ld_larghezza)
	dw_report_pl.setitem(ll_riga,"profondita",ld_profondita)
	dw_report_pl.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)
	dw_report_pl.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
	dw_report_pl.setitem(ll_riga,"misura",ls_misura)
	dw_report_pl.setitem(ll_riga,"quantita",ld_quan_pl)
	dw_report_pl.setitem(ll_riga,"peso",ld_peso)
	dw_report_pl.setitem(ll_riga,"tot_volume",ld_tot_volume)
	dw_report_pl.setitem(ll_riga,"tot_peso",ld_tot_peso)	
	dw_report_pl.setitem(ll_riga,"num_pl",ll_num_pl)	
	
loop	

close report_pl;

cb_salva.hide()
cb_indietro.hide()
cb_stampa_eti.hide()
cb_stampa_pl.hide()
dw_report_pl.change_dw_current()
dw_report_pl.setfocus()
dw_report_pl.object.datawindow.print.preview = 'Yes'
dw_report_pl.object.datawindow.print.preview.rulers = 'Yes'
dw_report_pl.show()
cb_ritorna.show()
cb_stampa.show()
end event

type cb_etichette from w_packing_list`cb_etichette within w_packing_list_gibus
end type

type cb_stampa_eti from w_packing_list`cb_stampa_eti within w_packing_list_gibus
end type

type em_num_eti from w_packing_list`em_num_eti within w_packing_list_gibus
end type

