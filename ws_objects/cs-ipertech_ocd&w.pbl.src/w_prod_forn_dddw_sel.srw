﻿$PBExportHeader$w_prod_forn_dddw_sel.srw
forward
global type w_prod_forn_dddw_sel from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_prod_forn_dddw_sel
end type
type cb_conferma from commandbutton within w_prod_forn_dddw_sel
end type
type dw_lista from datawindow within w_prod_forn_dddw_sel
end type
end forward

global type w_prod_forn_dddw_sel from w_cs_xx_risposta
integer width = 3118
integer height = 2272
string title = "Seleziona Fornitori Multipli per i seguenti prodotti"
boolean controlmenu = false
cb_annulla cb_annulla
cb_conferma cb_conferma
dw_lista dw_lista
end type
global w_prod_forn_dddw_sel w_prod_forn_dddw_sel

forward prototypes
public function boolean wf_controlla_fornitore (ref string as_errore)
public subroutine wf_filtra_fornitori (long al_row)
end prototypes

public function boolean wf_controlla_fornitore (ref string as_errore);long			ll_index



for ll_index=1 to dw_lista.rowcount()
	if dw_lista.getitemstring(ll_index, "cod_fornitore")="" or isnull(dw_lista.getitemstring(ll_index, "cod_fornitore")) then
		
		as_errore = 	"Riga "+string(ll_index)+" : Non hai selezionato il fornitore da utilizzare per il prodotto "+&
							dw_lista.getitemstring(ll_index, "cod_prodotto")
							
		dw_lista.scrolltorow(ll_index)
		
		return false
		
	end if
next


return true
end function

public subroutine wf_filtra_fornitori (long al_row);
string				ls_cod_prodotto


if al_row>0 then
else
	return
end if

ls_cod_prodotto = dw_lista.getitemstring(al_row, "cod_prodotto")
	
if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
	
	f_po_loaddddw_dw(dw_lista, &
					  "cod_fornitore", &
					  sqlca, &
					  "tab_prod_fornitori", &
					  "cod_fornitore", &
					  "cod_fornitore", &
					  "cod_prodotto='"+ls_cod_prodotto+"'")
					  
end if
end subroutine

on w_prod_forn_dddw_sel.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_conferma=create cb_conferma
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_conferma
this.Control[iCurrent+3]=this.dw_lista
end on

on w_prod_forn_dddw_sel.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_conferma)
destroy(this.dw_lista)
end on

event pc_setddlb;call super::pc_setddlb;

f_po_loaddddw_dw(dw_lista, &
                 "cod_fornitore", &
                 sqlca, &
                 "tab_prod_fornitori", &
                 "cod_fornitore", &
                 "cod_fornitore", &
                 "1=1")
end event

event pc_setwindow;call super::pc_setwindow;


s_cs_xx_parametri			ls_cs_xx_parametri
string							ls_errore, ls_cod_prodotto
long								ll_index, ll_new


ls_cs_xx_parametri = message.powerobjectparm

for ll_index=1 to ls_cs_xx_parametri.parametro_ds_1.rowcount()
	ls_cod_prodotto = ls_cs_xx_parametri.parametro_ds_1.getitemstring(ll_index, 1)		//cod_prodotto
	
	ll_new = dw_lista.insertrow(0)
	dw_lista.setitem(ll_new, "cod_prodotto", ls_cod_prodotto)
	
next


dw_lista.postevent("ue_filtra_fornitori")
end event

type cb_annulla from commandbutton within w_prod_forn_dddw_sel
integer x = 2286
integer y = 16
integer width = 402
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;s_cs_xx_parametri			ls_cs_xx_parametri



ls_cs_xx_parametri.parametro_b_1 = false
ls_cs_xx_parametri.parametro_s_1 = "E' obbligatorio definire il fornitore per gli articoli che ne prevedono più di uno!"

closewithreturn(parent, ls_cs_xx_parametri)
end event

type cb_conferma from commandbutton within w_prod_forn_dddw_sel
integer x = 1833
integer y = 16
integer width = 402
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;s_cs_xx_parametri			ls_cs_xx_parametri
string							ls_errore
datastore						lds_data


if not wf_controlla_fornitore(ls_errore) then
	g_mb.error(ls_errore)
	return
end if


dw_lista.accepttext()

lds_data = create datastore
lds_data.dataobject = dw_lista.dataobject

dw_lista.rowscopy(1, dw_lista.rowcount(), Primary!, lds_data, 1, Primary!)

ls_cs_xx_parametri.parametro_b_1 = true
ls_cs_xx_parametri.parametro_ds_1 = lds_data

//destroy lds_data

closewithreturn(parent, ls_cs_xx_parametri)
end event

type dw_lista from datawindow within w_prod_forn_dddw_sel
event ue_filtra_fornitori ( )
event scroll_next ( long al_row )
integer x = 37
integer y = 120
integer width = 3013
integer height = 2024
integer taborder = 10
string title = "none"
string dataobject = "d_prod_forn_dddw_sel"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_filtra_fornitori();
if rowcount()>0 then wf_filtra_fornitori(1)

return
end event

event scroll_next(long al_row);

if al_row < rowcount() then
	
	al_row += 1
	setrow(al_row)
	scrolltorow(al_row)
	
end if

return
end event

event rowfocuschanged;string			ls_cod_prodotto

if currentrow>0 then
	
	wf_filtra_fornitori(currentrow)
	
end if
end event

event itemchanged;choose case dwo.name
	case "cod_fornitore"
		if data<>"" then
			this.event post scroll_next(row)
		end if
		
end choose
end event

