﻿$PBExportHeader$w_report_cartellini_ean.srw
forward
global type w_report_cartellini_ean from w_cs_xx_principale
end type
type cb_stampa from commandbutton within w_report_cartellini_ean
end type
type dw_report from uo_cs_xx_dw within w_report_cartellini_ean
end type
type dw_folder from u_folder within w_report_cartellini_ean
end type
type dw_ricerca from u_dw_search within w_report_cartellini_ean
end type
end forward

global type w_report_cartellini_ean from w_cs_xx_principale
integer width = 4320
integer height = 2460
string title = "Stampa cartellini EAN"
cb_stampa cb_stampa
dw_report dw_report
dw_folder dw_folder
dw_ricerca dw_ricerca
end type
global w_report_cartellini_ean w_report_cartellini_ean

type variables
string			is_vern_old[], is_vern_new[]
end variables

forward prototypes
public function integer wf_report (ref string as_errore)
public function integer wf_get_verniciatura (string as_cod_prodotto, ref string as_cod_verniciatura, ref string as_des_verniciatura, ref string as_errore)
public function integer wf_verifica_sostituzione (string as_cod_veloce, string as_like, ref string as_verniciatura_new, ref string as_errore)
public function integer wf_cod_prod_fornitore (string as_cod_prodotto, string as_cod_deposito, long al_progressivo, ref string as_cod_prod_fornitore, ref string as_errore)
public function integer wf_cod_prod_fornitore (string as_cod_prodotto, string as_cod_fornitore, ref string as_cod_prod_fornitore, ref string as_errore)
public function integer wf_report_2 (ref string as_errore)
end prototypes

public function integer wf_report (ref string as_errore);
string		ls_cod_prodotto_inizio, ls_cod_prodotto_fine, ls_cod_deposito, ls_cod_reparto, ls_sql, ls_elab, &
				ls_cod_prodotto_ds, ls_des_prodotto_ds, ls_deposito_ds, ls_reparto_ds, ls_path_img, ls_filename, ls_logo, &
				ls_cod_verniciatura, ls_des_verniciatura, ls_vuoto[], ls_matrice, ls_des_progressivo	//,ls_where
				
dec{4}		ld_riordino, ld_stock

datastore	lds_data

long 			ll_tot, ll_index, ll_new, ll_k, ll_progressivo



dw_ricerca.accepttext()
dw_report.reset()

is_vern_old[] = ls_vuoto[]
is_vern_new[] = ls_vuoto[]

dw_ricerca.setitem(1, "flag_errore", "N")
dw_ricerca.setitem(1, "errore", "Elaborazione in corso ...")

ls_cod_prodotto_inizio	 = dw_ricerca.getitemstring(1, "cod_prodotto_inizio")
ls_cod_prodotto_fine	 = dw_ricerca.getitemstring(1, "cod_prodotto_fine")
ls_cod_deposito			 = dw_ricerca.getitemstring(1, "cod_deposito")
ls_cod_reparto			 = dw_ricerca.getitemstring(1, "cod_reparto")
ll_progressivo 			 = dw_ricerca.getitemnumber(1, "progressivo")
ls_des_progressivo		 = dw_ricerca.getitemstring(1, "des_progressivo")

if isnull(ls_cod_prodotto_inizio) or ls_cod_prodotto_inizio="" then
	as_errore = "Selezionare un prodotto inizio!"
	return -1
end if

if isnull(ls_cod_prodotto_fine) or ls_cod_prodotto_fine="" then
	as_errore = "Selezionare un prodotto fine!"
	return -1
end if

//ls_where = "p.cod_prodotto>='"+ls_cod_prodotto_inizio+"' and p.cod_prodotto<='"+ls_cod_prodotto_fine+"' and " + &
//				"((p.flag_blocco <> 'S') or (p.flag_blocco = 'S' and p.data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) "

if isnull(ls_cod_deposito) or ls_cod_deposito="" then
	as_errore = "Selezionare un deposito!"
	return -1
end if

if isnull(ls_cod_reparto) or ls_cod_reparto="" then
	as_errore = "Selezionare un reparto!"
	return -1
end if

if isnull(ll_progressivo) or ll_progressivo<=0 then
	as_errore = "Selezionare un progressivo da stampare!"
	return -1
end if

if isnull(ls_des_progressivo) or ls_des_progressivo="" then
	if not g_mb.confirm("Non hai digitato la stringa da stampare relativa al progressivo indicato. Continuare?") then
		as_errore = "Annullato dall'operatore!"
		return -1
	end if
end if


ls_logo = ""
if right(ls_logo, 1) <> "\" then ls_logo += "\"

ls_logo = s_cs_xx.volume + s_cs_xx.risorse
if right(ls_logo, 1) <> "\" then ls_logo += "\"
ls_logo += "gibus_4.jpg"

select stringa
into :ls_path_img
from parametri_azienda
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_parametro='PIP';
			
if ls_path_img="" or isnull(ls_path_img) or sqlca.sqlcode=100 then
	as_errore = "Manca l'impostazione del parametro aziendale PIP (Percorso file immagini articoli)!"
	return -1
end if

//percorso in cui cercare le immagini degli articoli
//ls_path_img = s_cs_xx.volume + ls_path_img
if right(ls_path_img, 1) <> "\" then ls_path_img += "\"


select des_deposito
into :ls_deposito_ds
from anag_depositi
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_deposito=:ls_cod_deposito;
			
select des_reparto
into :ls_reparto_ds
from anag_reparti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_reparto=:ls_cod_reparto;


ls_sql = "select 	ean.cod_prodotto,"+&
						"p.des_breve,"+&
						"p.des_prodotto,"+&
						"ean.stock,"+&
						"ean.riordino "+&
			"from anag_prodotti_cartellini as ean "+&
			"join anag_prodotti as p on p.cod_azienda=ean.cod_azienda and "+&
													"p.cod_prodotto=ean.cod_prodotto "+&
			"where ean.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"((p.flag_blocco <> 'S') or (p.flag_blocco = 'S' and p.data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
						"ean.cod_prodotto>='"+ls_cod_prodotto_inizio+"' and "+&
						"ean.cod_prodotto<='"+ls_cod_prodotto_fine+"' and "+&
						"ean.cod_deposito='"+ls_cod_deposito+"' and "+&
						"ean.progressivo="+string(ll_progressivo) + " "

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)    

if ll_tot<0 then
	return -1
	
elseif ll_tot = 0 then
	destroy lds_data
	as_errore = "Nessun dato presente in tabella anag_prodotti_cartellini rispetto al filtro specificato!"
	return -1
	
end if

////preparo i dati con gli articoli con più fornitori
//if wf_set_ds_prod_forn_multipli(ls_where, as_errore)<0 then
//	destroy lds_data
//	return -1
//end if
//alla fine avrò definito ogni prodotto con ciascun codice fornitore


dw_report.modify("p_logo_1.filename='"+ls_logo+"'")
dw_report.modify("p_logo_2.filename='"+ls_logo+"'")
dw_report.modify("p_logo_3.filename='"+ls_logo+"'")
dw_report.modify("p_logo_4.filename='"+ls_logo+"'")

dw_report.modify("des_progressivo_1_t.text='"+ls_des_progressivo+"'")
dw_report.modify("des_progressivo_2_t.text='"+ls_des_progressivo+"'")
dw_report.modify("des_progressivo_3_t.text='"+ls_des_progressivo+"'")
dw_report.modify("des_progressivo_4_t.text='"+ls_des_progressivo+"'")

for ll_index=1 to ll_tot step 4
	
	ls_cod_prodotto_ds		= lds_data.getitemstring(ll_index, 1)
	ls_des_prodotto_ds		= lds_data.getitemstring(ll_index, 2)
	
	if isnull(ls_des_prodotto_ds) or ls_des_prodotto_ds="" then
		ls_des_prodotto_ds	= lds_data.getitemstring(ll_index, 3)
	end if
	
	ld_stock							= lds_data.getitemnumber(ll_index, 4)
	ld_riordino						= lds_data.getitemnumber(ll_index, 5)
	
	ls_elab = string( ((ll_index / ll_tot) * 100) , "##0") + " % : Elaborazione prodotto " + ls_cod_prodotto_ds
	dw_ricerca.setitem(1, "errore", ls_elab)
	
	//inserimento di una riga ogni 4 elaborati dal ciclo for ...
	ll_new = dw_report.insertrow(0)
	
	dw_report.setitem(ll_new, "stabilimento", ls_deposito_ds)
	dw_report.setitem(ll_new, "reparto", ls_reparto_ds)
	
	//campo 1 ------------------------------------
	dw_report.setitem(ll_new, "prodotto_1", ls_cod_prodotto_ds)
	dw_report.setitem(ll_new, "des_prodotto_1", ls_des_prodotto_ds)
	dw_report.setitem(ll_new, "stock_1", ld_stock)
	dw_report.setitem(ll_new, "riordino_1", ld_riordino)
	
	//matrice fornitore ---------------------------
	if wf_cod_prod_fornitore(ls_cod_prodotto_ds, ls_cod_deposito, ll_progressivo, ls_matrice, as_errore) < 0 then
		destroy lds_data
		return -1
	end if
	
	dw_report.setitem(ll_new, "matrice_1", ls_matrice)


	//verniciatura ------------------------------
	if wf_get_verniciatura(ls_cod_prodotto_ds, ls_cod_verniciatura, ls_des_verniciatura, as_errore) < 0 then
		destroy lds_data
		return -1
	end if
	
	dw_report.setitem(ll_new, "des_verniciatura_1", ls_des_verniciatura)
	
	
	//path\+ primi5caratetricodice + _ + .jpg
	ls_filename = ls_path_img + left(ls_cod_prodotto_ds, 5) + "_.jpg"
	

	if fileexists(ls_filename) then
		dw_report.setitem(ll_new, "img_1", "S")
		dw_report.setitem(ll_new, "path_img_1", ls_filename)
	end if
	//---------------------------------------------
	
	
	for ll_k=1 to 3
		//campi 2,3,4 ---------------------------------------
		//ma solo se ci sono dati ...
		if ll_index + ll_k <= ll_tot then
			ls_cod_prodotto_ds		= lds_data.getitemstring(		ll_index + ll_k, 1)
			ls_des_prodotto_ds		= lds_data.getitemstring(		ll_index + ll_k, 2)
			
			if isnull(ls_des_prodotto_ds) or ls_des_prodotto_ds="" then
				ls_des_prodotto_ds	= lds_data.getitemstring(		ll_index + ll_k, 3)
			end if
			
			ld_stock							= lds_data.getitemnumber(	ll_index + ll_k, 4)
			ld_riordino						= lds_data.getitemnumber(	ll_index + ll_k, 5)
			
			dw_report.setitem(ll_new, "prodotto_" + string(ll_k + 1), 			ls_cod_prodotto_ds)
			dw_report.setitem(ll_new, "des_prodotto_" + string(ll_k + 1), 		ls_des_prodotto_ds)
			dw_report.setitem(ll_new, "stock_" + string(ll_k + 1), 					ld_stock)
			dw_report.setitem(ll_new, "riordino_" + string(ll_k + 1), 				ld_riordino)
			
			
			//matrice fornitore ---------------------------
			ls_matrice = ""
			if wf_cod_prod_fornitore(ls_cod_prodotto_ds, ls_cod_deposito, ll_progressivo, ls_matrice, as_errore) < 0 then
				destroy lds_data
				return -1
			end if
			
			dw_report.setitem(ll_new, "matrice_"+string(ll_k + 1), ls_matrice)
			
			
			//verniciatura ------------------------------
			ls_cod_verniciatura = ""
			ls_des_verniciatura = ""
			if wf_get_verniciatura(ls_cod_prodotto_ds, ls_cod_verniciatura, ls_des_verniciatura, as_errore) < 0 then
				destroy lds_data
				return -1
			end if
			
			dw_report.setitem(ll_new, "des_verniciatura_"+ string(ll_k + 1), ls_des_verniciatura)
			
			
			ls_filename = ls_path_img + left(ls_cod_prodotto_ds, 5) + "_.jpg"
			
			if fileexists(ls_filename) then
				dw_report.setitem(ll_new, "img_" + string(ll_k + 1), "S")
				dw_report.setitem(ll_new, "path_img_"+string(ll_k + 1), ls_filename)
			end if
			
		end if
		//------------------------------------------------------
		
	next
	
	
next



return 0
end function

public function integer wf_get_verniciatura (string as_cod_prodotto, ref string as_cod_verniciatura, ref string as_des_verniciatura, ref string as_errore);
string				ls_cod_classe_merceol, ls_cod_veloce, ls_like

long					ll_pos_verniciatura, ll_count

date					ldt_oggi

integer				li_ret

//s_cs_xx_parametri		ls_cs_xx_parametri



//posizione caratere che identifica la verniciatura
ll_pos_verniciatura = 6

ls_cod_classe_merceol = mid(as_cod_prodotto, 2, 1)

as_cod_verniciatura = ""
as_des_verniciatura = ""
as_errore = ""

if as_cod_prodotto="" or isnull(as_cod_prodotto) then
	setnull(as_cod_verniciatura)
	as_des_verniciatura = ""
	return 0
	
else
	ls_cod_veloce = mid(as_cod_prodotto, ll_pos_verniciatura, 1)
	
	if isnull(ls_cod_veloce) or ls_cod_veloce="" then
		setnull(as_cod_verniciatura)
		as_des_verniciatura = ""
		return 0
		
	elseif ls_cod_veloce="0" then
		setnull(as_cod_verniciatura)
		as_des_verniciatura = "(grezzo)"
		return 0
		
	else
		//se c'è + di una verniciatura apro un pop-up per permettere all'utente di scegliere
		ls_like = "%" + ls_cod_classe_merceol + "%"
		ldt_oggi = today()
		
		select count(*)
		into :ll_count
		from tab_verniciatura
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_veloce=:ls_cod_veloce and
					cod_classe_merceolo like :ls_like and
					((flag_blocco<>'S') or (flag_blocco = 'S' and data_blocco>:ldt_oggi));
		
		if ll_count=1 then
			//una sola verniciatura, metto quella
			as_cod_verniciatura = f_des_tabella("tab_verniciatura","cod_veloce='" +  ls_cod_veloce +"' and "+&
																						"cod_classe_merceolo like '%"+ls_cod_classe_merceol+"%'", "cod_verniciatura")
			
		elseif ll_count>1 then
			
			//verifica se per caso hai già scelto per una sostituzione
			//in ogni caso torna con la verniciatura già sostituita e/o tieni traccia della scelta fatta
			li_ret = wf_verifica_sostituzione(ls_cod_veloce, ls_like, as_cod_verniciatura, as_errore)
			if li_ret<0 then
				return -1
			end if
			
//			//più verniciature corrispondenti, apro pop-up per far scegliere ...
//			ls_cs_xx_parametri.parametro_s_1_a[1] = ls_cod_veloce
//			ls_cs_xx_parametri.parametro_s_1_a[2] = ls_like
//			openwithparm(w_sel_verniciature_ord_acq, ls_cs_xx_parametri)
//			as_cod_verniciatura = message.stringparm
			
			
		else
			as_cod_verniciatura = ""
		end if
		
		if isnull(as_cod_verniciatura) or as_cod_verniciatura="" then
			as_des_verniciatura = ""
			setnull( as_cod_verniciatura )
		else
			as_des_verniciatura = f_des_tabella("anag_prodotti","cod_prodotto = '" +  as_cod_verniciatura +"'", "des_breve")
			if isnull(as_des_verniciatura) then as_des_verniciatura=""
			
		end if
		
	end if
end if


return 0
end function

public function integer wf_verifica_sostituzione (string as_cod_veloce, string as_like, ref string as_verniciatura_new, ref string as_errore);string				ls_sql
datastore			lds_data
datetime			ldt_oggi
integer				li_ret, li_index, li_index2, li_index3
s_cs_xx_parametri		ls_cs_xx_parametri


ldt_oggi = datetime(today(), now())

ls_sql = "select cod_verniciatura "+&
			"from tab_verniciatura "+&
			"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"cod_veloce='"+as_cod_veloce+"' and "+&
					"cod_classe_merceolo like '"+as_like +"'and "+&
					"((flag_blocco<>'S') or (flag_blocco='S' and data_blocco>'"+string(ldt_oggi, s_cs_xx.db_funzioni.formato_data)+"')) "
		
li_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)		
if li_ret<0 then
	destroy lds_data
	return -1
end if

for li_index=1 to upperbound(is_vern_old[])
	
	for li_index2=1 to li_ret
		if is_vern_old[li_index] = lds_data.getitemstring(li_index2, 1) then
			//trovata verniciatura che va sostituita
			as_verniciatura_new = is_vern_new[li_index]
			destroy lds_data
			return 0
		end if
		
	next
	
next


//se arrivi fin qui non hai trovato alcuna sostituzione, cioè nessuna delle verniciature che soddisfano il criterio
//è presente nell'array delle verniciature old
//quindi caricale tutte nell'array e all'uscita aggiorna l'array new con la sostituzione
li_index = upperbound(is_vern_old[])

//salvo l'index iniziale
li_index3 = li_index

for li_index2=1 to li_ret
	li_index += 1
	is_vern_old[li_index] =  lds_data.getitemstring(li_index2, 1)
next


//più verniciature corrispondenti, apro pop-up per far scegliere ...
ls_cs_xx_parametri.parametro_s_1_a[1] = as_cod_veloce
ls_cs_xx_parametri.parametro_s_1_a[2] = as_like

//carica le verniciature old

openwithparm(w_sel_verniciature_ord_acq, ls_cs_xx_parametri)

as_verniciatura_new = message.stringparm

if as_verniciatura_new="" or isnull(as_verniciatura_new) then
	destroy lds_data
	as_errore = "Devi optare per una verniciatura da sostituire!"
	return -1
end if

li_index = upperbound(is_vern_new[])
for li_index2=1 to li_ret
	li_index += 1
	is_vern_new[li_index] =  as_verniciatura_new
next

return 0
end function

public function integer wf_cod_prod_fornitore (string as_cod_prodotto, string as_cod_deposito, long al_progressivo, ref string as_cod_prod_fornitore, ref string as_errore);
long				ll_count, ll_pos, ll_pos2
string			ls_cod_fornitore, ls_cod_fornitore_2, ls_temp


as_cod_prod_fornitore = ""
setnull(ls_cod_fornitore)
setnull(ls_cod_fornitore_2)

select cod_fornitore, cod_fornitore_2
into	:ls_cod_fornitore, :ls_cod_fornitore_2
from anag_prodotti_cartellini
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:as_cod_prodotto and
			cod_deposito=:as_cod_deposito and
			progressivo=:al_progressivo;

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura fornitori da anag_prodotti_cartellini per prodotto '"+as_cod_prodotto+"': "+sqlca.sqlerrtext
	return -1
end if

if (ls_cod_fornitore="" or isnull(ls_cod_fornitore)) and (ls_cod_fornitore_2="" or isnull(ls_cod_fornitore_2)) then
	//nessun fornitore specificato in anag_prodotti_cartellini
	//torna stringa vuota per la matrice
	return 0
end if

if ls_cod_fornitore<>"" and not isnull(ls_cod_fornitore) then
	if wf_cod_prod_fornitore(as_cod_prodotto, ls_cod_fornitore, ls_temp, as_errore) < 0 then
		return -1
	end if
	
	if ls_temp<>"" then
		as_cod_prod_fornitore = ls_temp
	end if
end if



if ls_cod_fornitore_2<>"" and not isnull(ls_cod_fornitore_2) then
	if wf_cod_prod_fornitore(as_cod_prodotto, ls_cod_fornitore_2, ls_temp, as_errore) < 0 then
		return -1
	end if
	
	if ls_temp<>"" then
		if as_cod_prod_fornitore<>"" then
			as_cod_prod_fornitore += "~r~n"
		end if
		
		as_cod_prod_fornitore += ls_temp
	end if
	
end if




return 0
end function

public function integer wf_cod_prod_fornitore (string as_cod_prodotto, string as_cod_fornitore, ref string as_cod_prod_fornitore, ref string as_errore);
long					ll_pos, ll_pos2

select cod_prod_fornitore
into :as_cod_prod_fornitore
from tab_prod_fornitori
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:as_cod_prodotto and
			cod_fornitore=:as_cod_fornitore;

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura cod_prod_fornitore da tab_prod_fornitori per prodotto '"+as_cod_prodotto+"': "+sqlca.sqlerrtext
	return -1
end if
	
if not isnull(as_cod_prod_fornitore) and as_cod_prod_fornitore<>"" then
	ll_pos = pos(as_cod_prod_fornitore, "L=")
	ll_pos2 = pos(as_cod_prod_fornitore, "L =")

	if ll_pos>0 then
		as_cod_prod_fornitore = left(as_cod_prod_fornitore, ll_pos - 1)
		as_cod_prod_fornitore = trim(as_cod_prod_fornitore)
		
	elseif ll_pos2>0 then
		as_cod_prod_fornitore = left(as_cod_prod_fornitore, ll_pos2 - 1)
		as_cod_prod_fornitore = trim(as_cod_prod_fornitore)
		
	else
		//torna con tutto quello che c'è
		return 0
	end if

else
	as_cod_prod_fornitore = ""
end if

return 0
end function

public function integer wf_report_2 (ref string as_errore);
string		ls_cod_prodotto_inizio, ls_cod_prodotto_fine, ls_cod_deposito, ls_cod_reparto, ls_sql, ls_elab, &
				ls_cod_prodotto_ds, ls_des_prodotto_ds, ls_deposito_ds, ls_reparto_ds, ls_path_img, ls_filename, ls_logo, &
				ls_cod_verniciatura, ls_des_verniciatura, ls_vuoto[], ls_matrice, ls_des_progressivo	//,ls_where
				
dec{4}		ld_riordino, ld_stock

datastore	lds_data

long 			ll_tot, ll_index, ll_new, ll_k, ll_progressivo



dw_ricerca.accepttext()
dw_report.reset()

is_vern_old[] = ls_vuoto[]
is_vern_new[] = ls_vuoto[]

dw_ricerca.setitem(1, "flag_errore", "N")
dw_ricerca.setitem(1, "errore", "Elaborazione in corso ...")

ls_cod_prodotto_inizio	 = dw_ricerca.getitemstring(1, "cod_prodotto_inizio")
ls_cod_prodotto_fine	 = dw_ricerca.getitemstring(1, "cod_prodotto_fine")
ls_cod_deposito			 = dw_ricerca.getitemstring(1, "cod_deposito")
ls_cod_reparto			 = dw_ricerca.getitemstring(1, "cod_reparto")
ll_progressivo 			 = dw_ricerca.getitemnumber(1, "progressivo")
ls_des_progressivo		 = dw_ricerca.getitemstring(1, "des_progressivo")

if isnull(ls_cod_prodotto_inizio) or ls_cod_prodotto_inizio="" then
	as_errore = "Selezionare un prodotto inizio!"
	return -1
end if

if isnull(ls_cod_prodotto_fine) or ls_cod_prodotto_fine="" then
	as_errore = "Selezionare un prodotto fine!"
	return -1
end if

//ls_where = "p.cod_prodotto>='"+ls_cod_prodotto_inizio+"' and p.cod_prodotto<='"+ls_cod_prodotto_fine+"' and " + &
//				"((p.flag_blocco <> 'S') or (p.flag_blocco = 'S' and p.data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) "

if isnull(ls_cod_deposito) or ls_cod_deposito="" then
	as_errore = "Selezionare un deposito!"
	return -1
end if

if isnull(ls_cod_reparto) or ls_cod_reparto="" then
	as_errore = "Selezionare un reparto!"
	return -1
end if

if isnull(ll_progressivo) or ll_progressivo<=0 then
	as_errore = "Selezionare un progressivo da stampare!"
	return -1
end if

if isnull(ls_des_progressivo) or ls_des_progressivo="" then
	if not g_mb.confirm("Non hai digitato la stringa da stampare relativa al progressivo indicato. Continuare?") then
		as_errore = "Annullato dall'operatore!"
		return -1
	end if
end if


ls_logo = ""
if right(ls_logo, 1) <> "\" then ls_logo += "\"

ls_logo = s_cs_xx.volume + s_cs_xx.risorse
if right(ls_logo, 1) <> "\" then ls_logo += "\"
ls_logo += "gibus_4.jpg"

select stringa
into :ls_path_img
from parametri_azienda
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_parametro='PIP';
			
if ls_path_img="" or isnull(ls_path_img) or sqlca.sqlcode=100 then
	as_errore = "Manca l'impostazione del parametro aziendale PIP (Percorso file immagini articoli)!"
	return -1
end if

//percorso in cui cercare le immagini degli articoli
//ls_path_img = s_cs_xx.volume + ls_path_img
if right(ls_path_img, 1) <> "\" then ls_path_img += "\"


select des_deposito
into :ls_deposito_ds
from anag_depositi
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_deposito=:ls_cod_deposito;
			
select des_reparto
into :ls_reparto_ds
from anag_reparti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_reparto=:ls_cod_reparto;


ls_sql = "select 	ean.cod_prodotto,"+&
						"p.des_breve,"+&
						"p.des_prodotto,"+&
						"ean.stock,"+&
						"ean.riordino "+&
			"from anag_prodotti_cartellini as ean "+&
			"join anag_prodotti as p on p.cod_azienda=ean.cod_azienda and "+&
													"p.cod_prodotto=ean.cod_prodotto "+&
			"where ean.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"((p.flag_blocco <> 'S') or (p.flag_blocco = 'S' and p.data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
						"ean.cod_prodotto>='"+ls_cod_prodotto_inizio+"' and "+&
						"ean.cod_prodotto<='"+ls_cod_prodotto_fine+"' and "+&
						"ean.cod_deposito='"+ls_cod_deposito+"' and "+&
						"ean.progressivo="+string(ll_progressivo) + " "

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)    

if ll_tot<0 then
	return -1
	
elseif ll_tot = 0 then
	destroy lds_data
	as_errore = "Nessun dato presente in tabella anag_prodotti_cartellini rispetto al filtro specificato!"
	return -1
	
end if

dw_report.modify("p_logo_1.filename='"+ls_logo+"'")
//dw_report.modify("des_progressivo_1_t.text='"+ls_des_progressivo+"'")


for ll_index=1 to ll_tot
	
	ls_cod_prodotto_ds		= lds_data.getitemstring(ll_index, 1)
	ls_des_prodotto_ds		= lds_data.getitemstring(ll_index, 2)
	
	if isnull(ls_des_prodotto_ds) or ls_des_prodotto_ds="" then
		ls_des_prodotto_ds	= lds_data.getitemstring(ll_index, 3)
	end if
	
	//ld_stock							= lds_data.getitemnumber(ll_index, 4)
	//ld_riordino						= lds_data.getitemnumber(ll_index, 5)
	
	ls_elab = string( ((ll_index / ll_tot) * 100) , "##0") + " % : Elaborazione prodotto " + ls_cod_prodotto_ds
	dw_ricerca.setitem(1, "errore", ls_elab)
	
	//inserimento di una riga ogni 4 elaborati dal ciclo for ...
	ll_new = dw_report.insertrow(0)
	
	dw_report.setitem(ll_new, "stabilimento", ls_deposito_ds)
	dw_report.setitem(ll_new, "reparto", ls_reparto_ds)
	
	//campo 1 ------------------------------------
	dw_report.setitem(ll_new, "prodotto_1", ls_cod_prodotto_ds)
	dw_report.setitem(ll_new, "des_prodotto_1", ls_des_prodotto_ds)
	//dw_report.setitem(ll_new, "stock_1", ld_stock)
	//dw_report.setitem(ll_new, "riordino_1", ld_riordino)
	
//	//matrice fornitore ---------------------------
//	if wf_cod_prod_fornitore(ls_cod_prodotto_ds, ls_cod_deposito, ll_progressivo, ls_matrice, as_errore) < 0 then
//		destroy lds_data
//		return -1
//	end if
//	
//	dw_report.setitem(ll_new, "matrice_1", ls_matrice)


	//verniciatura ------------------------------
	if wf_get_verniciatura(ls_cod_prodotto_ds, ls_cod_verniciatura, ls_des_verniciatura, as_errore) < 0 then
		destroy lds_data
		return -1
	end if
	
	dw_report.setitem(ll_new, "des_verniciatura_1", ls_des_verniciatura)
	
	
	//path\+ primi5caratetricodice + _ + .jpg
	ls_filename = ls_path_img + left(ls_cod_prodotto_ds, 5) + "_.jpg"
	

	if fileexists(ls_filename) then
		dw_report.setitem(ll_new, "img_1", "S")
		dw_report.setitem(ll_new, "path_img_1", ls_filename)
	end if
	//---------------------------------------------
	
	
//	for ll_k=1 to 3
//		//campi 2,3,4 ---------------------------------------
//		//ma solo se ci sono dati ...
//		if ll_index + ll_k <= ll_tot then
//			ls_cod_prodotto_ds		= lds_data.getitemstring(		ll_index + ll_k, 1)
//			ls_des_prodotto_ds		= lds_data.getitemstring(		ll_index + ll_k, 2)
//			
//			if isnull(ls_des_prodotto_ds) or ls_des_prodotto_ds="" then
//				ls_des_prodotto_ds	= lds_data.getitemstring(		ll_index + ll_k, 3)
//			end if
//			
//			ld_stock							= lds_data.getitemnumber(	ll_index + ll_k, 4)
//			ld_riordino						= lds_data.getitemnumber(	ll_index + ll_k, 5)
//			
//			dw_report.setitem(ll_new, "prodotto_" + string(ll_k + 1), 			ls_cod_prodotto_ds)
//			dw_report.setitem(ll_new, "des_prodotto_" + string(ll_k + 1), 		ls_des_prodotto_ds)
//			dw_report.setitem(ll_new, "stock_" + string(ll_k + 1), 					ld_stock)
//			dw_report.setitem(ll_new, "riordino_" + string(ll_k + 1), 				ld_riordino)
//			
//			
//			//matrice fornitore ---------------------------
//			ls_matrice = ""
//			if wf_cod_prod_fornitore(ls_cod_prodotto_ds, ls_cod_deposito, ll_progressivo, ls_matrice, as_errore) < 0 then
//				destroy lds_data
//				return -1
//			end if
//			
//			dw_report.setitem(ll_new, "matrice_"+string(ll_k + 1), ls_matrice)
//			
//			
//			//verniciatura ------------------------------
//			ls_cod_verniciatura = ""
//			ls_des_verniciatura = ""
//			if wf_get_verniciatura(ls_cod_prodotto_ds, ls_cod_verniciatura, ls_des_verniciatura, as_errore) < 0 then
//				destroy lds_data
//				return -1
//			end if
//			
//			dw_report.setitem(ll_new, "des_verniciatura_"+ string(ll_k + 1), ls_des_verniciatura)
//			
//			
//			ls_filename = ls_path_img + left(ls_cod_prodotto_ds, 5) + "_.jpg"
//			
//			if fileexists(ls_filename) then
//				dw_report.setitem(ll_new, "img_" + string(ll_k + 1), "S")
//				dw_report.setitem(ll_new, "path_img_"+string(ll_k + 1), ls_filename)
//			end if
//			
//		end if
//		//------------------------------------------------------
//		
//	next
//	
	
next



return 0
end function

on w_report_cartellini_ean.create
int iCurrent
call super::create
this.cb_stampa=create cb_stampa
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_ricerca
end on

on w_report_cartellini_ean.destroy
call super::destroy
destroy(this.cb_stampa)
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[], lw_vuoto[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.ib_dw_report=true

											
dw_report.set_dw_options(		sqlca, &
												pcca.null_object, &
												c_nonew + &
												c_nomodify + &
												c_nodelete + &
												c_noenablenewonopen + &
												c_noenablemodifyonopen + &
												c_scrollparent + &
												c_disablecc, &
												c_noresizedw + &
												c_nohighlightselected + &
												c_nocursorrowfocusrect + &
												c_nocursorrowpointer)

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_ricerca
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_report
lw_oggetti[2] = cb_stampa
dw_folder.fu_assigntab(2, "Cartellini", lw_oggetti[])

dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)

dw_report.object.datawindow.print.preview = "yes"






end event

event pc_setddlb;call super::pc_setddlb;
string ls_where_depositi


ls_where_depositi = 	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
							"((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
							"isnumeric(cod_deposito)=1 and "+&
							"right('000' + cod_deposito, 2) = '00'"


f_po_loaddddw_dw(dw_ricerca, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 ls_where_depositi)
					  
f_po_loaddddw_dw(dw_ricerca, &
                 "cod_reparto", &
                 sqlca, &
                 "anag_reparti", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
							"((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end event

type cb_stampa from commandbutton within w_report_cartellini_ean
integer x = 3840
integer y = 160
integer width = 334
integer height = 84
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;dw_report.print(true, true)
end event

type dw_report from uo_cs_xx_dw within w_report_cartellini_ean
integer x = 41
integer y = 248
integer width = 4197
integer height = 2092
integer taborder = 30
string dataobject = "d_report_cartellini_ean"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type dw_folder from u_folder within w_report_cartellini_ean
integer x = 18
integer y = 20
integer width = 4242
integer height = 2328
integer taborder = 10
end type

type dw_ricerca from u_dw_search within w_report_cartellini_ean
integer x = 41
integer y = 144
integer width = 2798
integer height = 2008
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_report_cartellini_ean_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string		ls_errore, ls_flag_tipo_stampa
integer		li_ret

choose case dwo.name
		
	case "b_prodotto_inizio"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto_inizio")
		
		
	case "b_prodotto_fine"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto_fine")
		
		
	case "b_report"
		
		ls_flag_tipo_stampa = dw_ricerca.getitemstring(1, "flag_tipo_stampa")
		
		setpointer(Hourglass!)
		
		if ls_flag_tipo_stampa="B" then
			dw_report.dataobject = "d_report_cartellini_ean_2"
			li_ret = wf_report_2(ls_errore)
		else
			dw_report.dataobject = "d_report_cartellini_ean"
			li_ret = wf_report(ls_errore)
		end if
		setpointer(Arrow!)
		
		if li_ret < 0 then
			dw_ricerca.setitem(1, "flag_errore", "S")
			dw_ricerca.setitem(1, "errore", ls_errore)
		else
			dw_ricerca.setitem(1, "flag_errore", "N")
			dw_ricerca.setitem(1, "errore", "Pronto!")
			dw_folder.fu_selecttab(2)
		end if
		
		
		
end choose
end event

event itemchanged;call super::itemchanged;string				ls_where_add, ls_null


choose case dwo.name
	case "cod_deposito"
		if data="" then
			ls_where_add = ""
		else
			ls_where_add = " and cod_deposito='"+data+"'"
		end if
		
		setnull(ls_null)
		dw_ricerca.setitem(1, "cod_reparto", ls_null)
		
		f_po_loaddddw_dw(dw_ricerca, &
                 "cod_reparto", &
                 sqlca, &
                 "anag_reparti", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
							"((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) " + &
				ls_where_add)
		
		
end choose
end event

