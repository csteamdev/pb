﻿$PBExportHeader$w_report_conferma_ordine.srw
forward
global type w_report_conferma_ordine from w_cs_xx_principale
end type
type cbx_1 from checkbox within w_report_conferma_ordine
end type
type cbx_invia_mittente from checkbox within w_report_conferma_ordine
end type
type dw_report_fat_ven from uo_cs_xx_dw within w_report_conferma_ordine
end type
type cb_stampa from commandbutton within w_report_conferma_ordine
end type
type cb_invia_mail from commandbutton within w_report_conferma_ordine
end type
end forward

global type w_report_conferma_ordine from w_cs_xx_principale
integer width = 4005
integer height = 4796
string title = "Stampa Conferma"
boolean minbox = false
boolean hscrollbar = true
boolean vscrollbar = true
event ue_close ( )
event ue_reset_lingua_window ( )
cbx_1 cbx_1
cbx_invia_mittente cbx_invia_mittente
dw_report_fat_ven dw_report_fat_ven
cb_stampa cb_stampa
cb_invia_mail cb_invia_mail
end type
global w_report_conferma_ordine w_report_conferma_ordine

type variables
boolean ib_modifica=false, ib_nuovo=false, ib_stampando = false, ib_email=false
boolean ib_estero
long il_anno_registrazione[], il_num_registrazione[], il_corrente = 0
long il_num_copie
string is_flag_tipo_fat_ven

// ------------- dichiarate per stampa da pagina a pagina -----------
boolean ib_stampa = true
string is_flag_email[], is_email_amministrazione[]
long    il_totale_pagine, il_pagina_corrente

// ------------- discalimer per invio fatture tramite email ----------
constant string is_disclaimer=""

// stefanop 17/02/2012: percorso file per filigrana
private:
	string is_path_filigrana = ""
end variables

forward prototypes
public subroutine wf_leggi_iva (long fl_anno_registrazione, long fl_num_registrazione, ref decimal fd_aliquote_iva[], ref decimal fd_imponibile_iva_valuta[], ref decimal fd_imposta_iva_valuta[], ref string fs_des_esenzione[], ref string fs_cod_iva[])
public function integer wf_impostazioni ()
public function integer wf_report ()
public subroutine wf_stampa (long copie)
public function integer wf_email ()
public function integer wf_replace_marker (ref string as_source, string as_find, string as_replace, boolean ab_case_sensitive)
public function integer wf_imposta_barcode ()
public function integer wf_testo_mail (string fs_rag_soc_1, long fl_anno, long fl_numero, datetime fdt_data, string fs_lingua, ref string fs_testo)
public function integer wf_calcola_ordini ()
public function integer wf_memorizza_blob (long fl_anno_registrazione, long fl_num_registrazione, string fs_path, transaction at_tran)
public function integer wf_crea_transaction_docs (ref transaction at_tran, ref string as_errore)
end prototypes

event ue_close();close(this)
end event

event ue_reset_lingua_window();setnull(s_cs_xx.cod_lingua_window)
end event

public subroutine wf_leggi_iva (long fl_anno_registrazione, long fl_num_registrazione, ref decimal fd_aliquote_iva[], ref decimal fd_imponibile_iva_valuta[], ref decimal fd_imposta_iva_valuta[], ref string fs_des_esenzione[], ref string fs_cod_iva[]);boolean lb_trovato= false
string ls_cod_iva, ls_aliquote_iva
dec{4} ld_imponibile_iva_valuta, ld_imposta_iva_valuta, ld_aliquote_iva
long  ll_y

declare cu_iva cursor for 
	select   cod_iva, 
				imponibile_valuta_iva, 
				importo_valuta_iva,
				perc_iva
	from     iva_ord_ven 
	where    cod_azienda = :s_cs_xx.cod_azienda and 
				anno_registrazione = :fl_anno_registrazione and 
				num_registrazione = :fl_num_registrazione;

open cu_iva;

do while true
   fetch cu_iva into :ls_aliquote_iva, :ld_imponibile_iva_valuta, :ld_imposta_iva_valuta, :ld_aliquote_iva;
	
   if sqlca.sqlcode <> 0 then exit
	
	lb_trovato= false
	
	for ll_y = 1 to upperbound(fd_aliquote_iva[]) 
		if fd_aliquote_iva[ll_y] = ld_aliquote_iva then
			fd_imponibile_iva_valuta[ll_y] += ld_imponibile_iva_valuta
			fd_imposta_iva_valuta[ll_y]    += ld_imposta_iva_valuta
			lb_trovato = true
		end if
	next
	
	if not lb_trovato then
		ll_y = upperbound(fd_aliquote_iva[])
		ll_y ++ 
		fd_aliquote_iva[ll_y] = ld_aliquote_iva
		fd_imponibile_iva_valuta[ll_y] = ld_imponibile_iva_valuta
		fd_imposta_iva_valuta[ll_y]    = ld_imposta_iva_valuta
		fs_cod_iva[ll_y] = ls_aliquote_iva
		
		if ld_aliquote_iva = 0 then
			select  des_iva
			into    :fs_des_esenzione[ll_y]
			from   tab_ive
			where  cod_azienda = :s_cs_xx.cod_azienda and
					  cod_iva = :ls_aliquote_iva;
		end if
	end if
loop

close cu_iva;
return
end subroutine

public function integer wf_impostazioni ();string ls_cod_tipo_ord_ven, ls_flag_tipo_ord_ven, ls_cod_cliente, ls_flag_tipo_cliente, ls_path_logo_1, &
       ls_path_logo_2, ls_modify, ls_dataobject, ls_logo_testata, ls_cod_lingua

long   ll_copie, ll_copie_cee, ll_copie_extra_cee, ll_i

save_on_close(c_socnosave)
ib_estero = false
il_num_copie = 3
setnull(ls_logo_testata)

// Stampa ed invio pdf
if upperbound(il_anno_registrazione) > 1 then
	dw_report_fat_ven.set_document_name("Conferme Ordini")
	dw_report_fat_ven.is_mail_message = "Ordini";
	
	for ll_i = 1 to upperbound(il_anno_registrazione)
		dw_report_fat_ven.is_mail_message += "~r~nOrdine " + string(il_anno_registrazione[ll_i]) + "/" + string(il_num_registrazione[ll_i])
	next
else
	dw_report_fat_ven.set_document_name("Conferma Ordine " + string(il_anno_registrazione[1]) + "/" + string(il_num_registrazione[1]))
	setnull(dw_report_fat_ven.is_mail_message)
end if


select cod_cliente,
       	cod_tipo_ord_ven
into   	:ls_cod_cliente,
		:ls_cod_tipo_ord_ven
from   tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
       	anno_registrazione = :il_anno_registrazione[1] and
		num_registrazione = :il_num_registrazione[1];

select flag_tipo_cliente,
		 cod_lingua
into   :ls_flag_tipo_cliente,
		 :ls_cod_lingua
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :ls_cod_cliente;
	 
if sqlca.sqlcode = 0 and not isnull(ls_cod_cliente) then

	if not isnull( ls_cod_lingua) and ls_cod_lingua <> "" then
		
		select  	dataobject, 
					 logo_testata
		into	 	:ls_dataobject, 
				 	:ls_logo_testata
		from	 	tab_tipi_ord_ven_lingue_dw
		where	 cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven and
					 cod_lingua = :ls_cod_lingua and
					 cod_cliente = :ls_cod_cliente;
				 
		if sqlca.sqlcode <> 0 then
			
			// *** non esiste per quel cliente un modulo specifico; controllo se esiste a livello di lingua
			select dataobject,
					logo_testata
			into	 :ls_dataobject,
					:ls_logo_testata
			from	 tab_tipi_ord_ven_lingue_dw
			where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven and
					 cod_lingua = :ls_cod_lingua and
					 ( cod_cliente IS NULL or cod_cliente = '' );
					 
			if sqlca.sqlcode = 0 and not isnull(ls_dataobject) and ls_dataobject <> "" then
				// *** esiste un particolare dataobject per quel tipo di fattura per quella lingua
				dw_report_fat_ven.dataobject = ls_dataobject					
			else 
				
				// *** non esiste per quel cliente/lingua un modulo specifico; controllo se esiste a livello di tipo ordine
				select dataobject,
						logo_testata
				into	 :ls_dataobject,
						:ls_logo_testata
				from	 tab_tipi_ord_ven_lingue_dw
				where cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven and
						 cod_lingua is null and
						 ( cod_cliente IS NULL or cod_cliente = '' );
						 
				if sqlca.sqlcode = 0 and not isnull(ls_dataobject) and ls_dataobject <> "" then
					// *** esiste un particolare dataobject per quel tipo di fattura per quella lingua
					dw_report_fat_ven.dataobject = ls_dataobject					
				else 
					// *** altrimenti lascio stare tutto com'è
				end if		
				
			end if		
			
		end if
			
	end if
	
end if

if isnull(ls_logo_testata) or len(ls_logo_testata) < 1 then

	select parametri_azienda.stringa
	into   :ls_path_logo_1
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LO3';
	
	ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
	dw_report_fat_ven.modify(ls_modify)

else
	
	ls_modify = "intestazione.filename='" + s_cs_xx.volume + s_cs_xx.risorse + ls_logo_testata + "'~t"
	dw_report_fat_ven.modify(ls_modify)
	
end if

return 0
end function

public function integer wf_report ();boolean	lb_prima_riga, lb_flag_prodotto_cliente, lb_flag_nota_dettaglio, lb_flag_nota_piede

string 	ls_stringa, ls_stringa_euro, ls_cod_tipo_ord_ven, ls_cod_banca_clien_for, ls_cod_cliente, &
		 	ls_cod_valuta, ls_cod_pagamento, ls_num_ord_cliente, ls_cod_porto, ls_cod_resa, ls_null, &
		 	ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
		 	ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
		 	ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, &
		 	ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli, &
		 	ls_partita_iva, ls_cod_fiscale, ls_cod_lingua, ls_flag_tipo_cliente, &
		 	ls_des_tipo_fat_ven, ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, &
  		 	ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua, ls_des_tipo_det_ven,&
		 	ls_des_prodotto_anag, ls_flag_stampa_ordine, ls_cod_tipo_det_ven, ls_flag_tipo_fat_ven, &
		 	ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_cod_iva, ls_des_iva, ls_causale_trasporto, &
		 	ls_cod_vettore, ls_vettore_rag_soc_1, ls_vettore_rag_soc_2, ls_vettore_indirizzo, &
		 	ls_vettore_cap, ls_vettore_localita, ls_vettore_provincia, ls_des_esenzione_iva[], &
		 	ls_cod_documento, ls_numeratore_doc, ls_cod_banca, ls_flag_tipo_pagamento, ls_cod_abi, ls_cod_cab, &
		 	ls_cod_des_cliente, ls_telefono_cliente, ls_fax_cliente, ls_telefono_des, ls_fax_des, &
		 	ls_des_valuta, ls_des_valuta_lingua, ls_dicitura_std, ls_cod_agente_1, ls_cod_agente_2,&
		 	ls_anag_agenti_rag_soc_1, ls_anag_agenti_rag_soc_2, ls_cod_fornitore, ls_cod_des_fattura, ls_des_causale, &
		 	ls_cod_prodotto_finito, ls_cod_non_a_magazzino, ls_cod_verniciatura, ls_cod_tessuto, ls_des_gruppo_tessuto, &
		 	ls_descrizione_riga, ls_descrizione_riga_lingua, ls_des_verniciatura, ls_codici_iva[],  &
		 	ls_flag_st_note_tes, ls_flag_st_note_pie, ls_flag_st_note_det, ls_cod_misura_mag, ls_formato, &
			ls_flag_tipo_det, ls_flag_tipo_fat,ls_cod_cin, ls_cod_iban, ls_conto_corrente, ls_des_pagamento_estesa, &
			ls_cod_inoltro, ls_inoltro_rag_soc_1, ls_inoltro_rag_soc_2, ls_inoltro_indirizzo, &
		 	ls_inoltro_cap, ls_inoltro_localita, ls_inoltro_provincia, ls_cod_mezzo, ls_des_mezzo, ls_des_mezzo_lingua, &
			ls_des_imballo, ls_cod_imballo, ls_parametro_colli,  ls_cod_deposito, ls_des_deposito, &
			ls_provincia_dep, ls_cap_dep, ls_indirizzo_dep, ls_telefono_dep, ls_fax_dep, ls_swift, ls_des_misura_lingua, &
			ls_cod_misura_um, ls_colore_passamaneria, ls_flag_tipo_mantovana, ls_des_colore_tessuto, ls_localita_dep, &
			ls_nome_file, ls_condizioni, ls_cod_nazione, ls_des_nazione, ls_elenco_ordini, ls_rif_ordine_cliente, ls_tab_ive_des_iva, ls_stato_cli, &
			ls_des_imballo_lingua, ls_variabili, ls_stampa_nota_testata,  ls_stampa_nota_piede, ls_stampa_nota_video
			 
long   	ll_errore, ll_num_colli, ll_num_aliquote, ll_anno_documento, ll_num_documento, li_filenum, ll_prog_riga_ord_ven, ll_ret, ll_i, ll_settimana

dec{4} 	ld_tot_val_ordine_euro, ld_sconto_tes, ld_quan_fatturata, ld_prezzo_vendita, ld_sconto_1, &
	    		ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7,&
		 	ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_tot, ld_imponibile_riga, &
		 	ld_val_riga, ld_sconto_pagamento, ld_cambio_ven, ld_prezzo_um, ld_quantita_um, ld_val_sconto_riga, &
		   	ld_imponibili_valuta[], ld_iva_valuta[],ld_aliquote_iva[] ,ld_rate_scadenze[], ld_imponibile_iva_valuta, &
		   	ld_importo_iva_valuta, ld_perc_iva, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_tot_merci, ld_tot_cassa, &
			ld_tot_sconti_commerciali, ld_tot_fattura_euro, ld_precisione, ld_valore_parziale, ld_peso_lordo, ld_peso_netto, &
			ld_sum_imponibile_iva_valuta, ld_sum_importo_iva_valuta, ld_sum_val_ordine_euro, ld_sum_merci, ld_sum_cassa, &
			ld_sum_sconti_commerciali,ld_dim_x_riga, ld_dim_y_riga, ld_sum_val_ordine_valuta, ld_tot_val_ordine_valuta

			
dec{5}   ld_fat_conversione_ven

datetime ldt_data_ord_cliente, ldt_data_registrazione, ldt_data_consegna,  ldt_data_consegna_max

uo_calcola_documento_euro luo_doc
uo_premi_clienti luo_premi_clienti
uo_stampa_dati_prodotto luo_stampa_dati_prodotto


setnull(ls_null)
ls_elenco_ordini = ""

dw_report_fat_ven.reset()

wf_imposta_barcode()

select parametri_azienda.stringa  
into  :ls_stringa  
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'CVL';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

select parametri_azienda.stringa  
into  :ls_stringa_euro
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'EUR';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa_euro)
end if

// EnMe 25-2-2009; stabilisce se stampare oppure no il numero colli nei doc fiscali
select flag  
into  :ls_parametro_colli
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'F' and  
      parametri_azienda.cod_parametro = 'SCL';

if sqlca.sqlcode <> 0 then
	ls_parametro_colli = "N"
end if


select cod_tipo_ord_ven,   
		 data_registrazione,   
		 cod_cliente,   
		 cod_valuta,   
		 cod_pagamento,   
		 sconto,   
		 cod_banca_clien_for,   
		 cod_banca,   
		 num_ord_cliente,   
		 data_ord_cliente,   
		 cod_porto,   
		 cod_resa,   
		 nota_testata,   
		 nota_piede,   
		 imponibile_iva_valuta,
		 importo_iva_valuta,
		 tot_val_ordine,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia,
		 cambio_ven,
		 cod_vettore,
		 cod_causale,
		 cod_agente_1,
		 cod_agente_2,
		 cod_des_cliente,
		 tot_merci,
		 tot_sconto_cassa,
		 tot_sconti_commerciali,
		 flag_st_note_tes,
		 flag_st_note_pie,
		 cod_inoltro,
		 peso_lordo,
		 peso_netto,
		 cod_mezzo,
		 cod_imballo,
		 cod_deposito
into   :ls_cod_tipo_ord_ven,   
	 	 :ldt_data_registrazione,   
	 	 :ls_cod_cliente,   
		 :ls_cod_valuta,   
		 :ls_cod_pagamento,   
		 :ld_sconto_tes,
		 :ls_cod_banca_clien_for,   
		 :ls_cod_banca,
		 :ls_num_ord_cliente,   
		 :ldt_data_ord_cliente,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_imponibile_iva_valuta,
		 :ld_importo_iva_valuta,
		 :ld_tot_val_ordine_euro,   
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,
		 :ld_cambio_ven,
		 :ls_cod_vettore,
		 :ls_causale_trasporto,
		 :ls_cod_agente_1,
		 :ls_cod_agente_2,
		 :ls_cod_des_cliente,
		 :ld_tot_merci,
		 :ld_tot_cassa,
		 :ld_tot_sconti_commerciali,
		 :ls_flag_st_note_tes,
		 :ls_flag_st_note_pie,
		 :ls_cod_inoltro,
		 :ld_peso_lordo,
		 :ld_peso_netto,
		 :ls_cod_mezzo,
		 :ls_cod_imballo,
		 :ls_cod_deposito
from   tes_ord_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :il_anno_registrazione[1] and 
		 num_registrazione = :il_num_registrazione[1];

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca testata fattura: stampa interrotta", Stopsign!)
	return -1
end if

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return -1
end if


// stefanop: 19/12/2011: aggiunto deposito
select 
	des_deposito,
	provincia,
	cap,
	indirizzo,
	telefono,
	fax,
	localita
into 
	:ls_des_deposito,
	:ls_provincia_dep,
	:ls_cap_dep,
	:ls_indirizzo_dep,
	:ls_telefono_dep,
	:ls_fax_dep,
	:ls_localita_dep
from anag_depositi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_deposito = :ls_cod_deposito;

if isnull(ls_des_deposito) then ls_des_deposito = ""
if isnull(ls_provincia_dep) then ls_provincia_dep = ""
if isnull(ls_cap_dep) then ls_cap_dep = ""
if isnull(ls_localita_dep) then ls_localita_dep = ""
if isnull(ls_indirizzo_dep) then ls_indirizzo_dep = ""
if isnull(ls_telefono_dep) then ls_telefono_dep = ""
if isnull(ls_fax_dep) then ls_fax_dep = ""
// ----

//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_st_note_tes = 'I' then   //nota di testata
		select flag_st_note
		  into :ls_flag_st_note_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_st_note_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_st_note_pie = 'I' then 	//nota di piede
		select flag_st_note
		  into :ls_flag_st_note_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_st_note_pie = 'N' then
		ls_nota_piede = ""
	end if	
	

//--------------------------------------- Fine Modifica --------------------------------------------------------		

	
select rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia,   
		 partita_iva,   
		 cod_fiscale,   
		 cod_lingua,   
		 telefono,   
		 telex,   
		 flag_tipo_cliente,
		 cod_nazione,
		 stato
into   :ls_rag_soc_1_cli,   
		 :ls_rag_soc_2_cli,   
		 :ls_indirizzo_cli,   
		 :ls_localita_cli,   
		 :ls_frazione_cli,   
		 :ls_cap_cli,   
		 :ls_provincia_cli,   
		 :ls_partita_iva,   
		 :ls_cod_fiscale,   
		 :ls_cod_lingua,  
		 :ls_telefono_cliente,
		 :ls_fax_cliente,
		 :ls_flag_tipo_cliente,
		 :ls_cod_nazione,
		 :ls_stato_cli
from   anag_clienti  
where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
		 anag_clienti.cod_cliente = :ls_cod_cliente;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca anagrafica cliente: stampa interrotta", Stopsign!)
	return -1
end if

if not isnull(ls_stato_cli) and ls_stato_cli<>"" then
	if not isnull(ls_localita_cli) and ls_localita_cli<>"" then
		ls_localita_cli += " - " + ls_stato_cli
	else
		ls_localita_cli = ls_stato_cli
	end if
end if


// vettore
select rag_soc_1,
       indirizzo,   
       localita,   
       cap,   
       provincia  
into   :ls_vettore_rag_soc_1,   
       :ls_vettore_indirizzo,   
       :ls_vettore_localita,   
       :ls_vettore_cap,   
       :ls_vettore_provincia
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and
	    cod_vettore = :ls_cod_vettore;

if sqlca.sqlcode <> 0 then
   setnull(ls_vettore_rag_soc_1)   
   setnull(ls_vettore_indirizzo)   
   setnull(ls_vettore_localita)   
   setnull(ls_vettore_cap)   
   setnull(ls_vettore_provincia)
end if

// inoltro
select rag_soc_1,
       indirizzo,   
       localita,   
       cap,   
       provincia  
into   :ls_inoltro_rag_soc_1,   
       :ls_inoltro_indirizzo,   
       :ls_inoltro_localita,   
       :ls_inoltro_cap,   
       :ls_inoltro_provincia
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and
	    cod_vettore = :ls_cod_inoltro;

if sqlca.sqlcode <> 0 then
   setnull(ls_inoltro_rag_soc_1)   
   setnull(ls_inoltro_indirizzo)   
   setnull(ls_inoltro_localita)   
   setnull(ls_inoltro_cap)   
   setnull(ls_inoltro_provincia)
end if


if ls_flag_tipo_cliente = 'E' or ls_flag_tipo_cliente = 'C' then
	ls_partita_iva = ls_cod_fiscale
end if

select des_pagamento,   
       	sconto,
       	flag_tipo_pagamento,
		 des_pagamento_2
into   :ls_des_pagamento,   
       	:ld_sconto_pagamento,
       	:ls_flag_tipo_pagamento,
		 :ls_des_pagamento_estesa
from   tab_pagamenti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       	cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
	setnull(ls_des_pagamento_estesa)
   	ls_flag_tipo_pagamento = "D"
end if

select des_pagamento
into   :ls_des_pagamento_lingua
from   tab_pagamenti_lingue  
where  cod_azienda   = :s_cs_xx.cod_azienda and 
       cod_pagamento = :ls_cod_pagamento and
       cod_lingua    = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

if not isnull(ls_cod_nazione) then
	select des_nazione
	into :ls_des_nazione
	from tab_nazioni
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_nazione = :ls_cod_nazione;
	if sqlca.sqlcode <> 0 then
		setnull(ls_cod_nazione)
		setnull(ls_des_nazione)
	end if
else
	setnull(ls_des_nazione)
end if

setnull(ls_cod_iban)
setnull(ls_conto_corrente)

if not isnull(ls_cod_banca) then
	select des_banca,
	       cod_abi,
			 cod_cab,
			 cin,
			 iban,
			 conto_corrente,
			 swift
	into   :ls_des_banca,
			 :ls_cod_abi,
			 :ls_cod_cab,
			 :ls_cod_cin,
			 :ls_cod_iban,
			 :ls_conto_corrente,
			 :ls_swift
	from   anag_banche
	where  anag_banche.cod_azienda = :s_cs_xx.cod_azienda and 
			 anag_banche.cod_banca = :ls_cod_banca;
elseif not isnull(ls_cod_banca_clien_for) then
	setnull(ls_swift)
	setnull(ls_cod_iban)
	select des_banca,
			 cod_abi,
			 cod_cab,
			 cin
	into   :ls_des_banca,
			 :ls_cod_abi,
			 :ls_cod_cab,
			 :ls_cod_cin
	from   anag_banche_clien_for  
	where  anag_banche_clien_for.cod_azienda = :s_cs_xx.cod_azienda and 
			 anag_banche_clien_for.cod_banca_clien_for = :ls_cod_banca_clien_for;	
end if			 

if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca)
	setnull(ls_cod_abi)
	setnull(ls_cod_cab)
	setnull(ls_cod_cin)
	setnull(ls_cod_iban)
end if


select des_imballo
into   :ls_des_imballo
from   tab_imballi
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_imballo = :ls_cod_imballo;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballo)
end if

select des_imballi
into   :ls_des_imballo_lingua
from   tab_imballi_lingue
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_imballo = :ls_cod_imballo and
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballo_lingua)
end if

select des_mezzo 
into   :ls_des_mezzo  
from   tab_mezzi  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_mezzo = :ls_cod_mezzo;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo)
end if

select des_mezzo  
into   :ls_des_mezzo_lingua  
from   tab_mezzi_lingue 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_mezzo = :ls_cod_mezzo and
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo_lingua)
end if


select des_porto  
into   :ls_des_porto  
from   tab_porti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_porto = :ls_cod_porto;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_porto = :ls_cod_porto and
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select des_resa
into   :ls_des_resa  
from   tab_rese  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_resa = :ls_cod_resa;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_resa = :ls_cod_resa and  
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

select des_valuta,
		 precisione
into   :ls_des_valuta,
		 :ld_precisione
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta = :ls_cod_valuta;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta)
end if

select des_valuta
into   :ls_des_valuta_lingua  
from   tab_valute_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_valuta = :ls_cod_valuta and  
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta_lingua)
end if

if not isnull(ls_cod_agente_1) then
	select rag_soc_1
	into   :ls_anag_agenti_rag_soc_1
	from   anag_agenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_agente = :ls_cod_agente_1;
	if sqlca.sqlcode <> 0 then setnull(ls_anag_agenti_rag_soc_1)
end if

if not isnull(ls_cod_agente_2) then
	select rag_soc_1
	into   :ls_anag_agenti_rag_soc_2
	from   anag_agenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_agente = :ls_cod_agente_2;
	if sqlca.sqlcode <> 0 then setnull(ls_anag_agenti_rag_soc_2)
end if

if not isnull(ls_cod_des_cliente) then
		select telefono,   
				 telex  
		into   :ls_telefono_des,   
				 :ls_fax_des  
		from   anag_des_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente and
				 cod_des_cliente = :ls_cod_des_cliente ;
		if sqlca.sqlcode <> 0 then
			setnull(ls_telefono_des)
			setnull(ls_fax_des)
		end if
end if

ld_sum_imponibile_iva_valuta = 0
ld_sum_importo_iva_valuta = 0
ld_sum_val_ordine_euro = 0
ld_sum_val_ordine_valuta = 0
ld_sum_merci = 0
ld_sum_cassa = 0
ld_sum_sconti_commerciali = 0


for ll_i = 1 to upperbound(il_anno_registrazione) 
	
	if len(ls_elenco_ordini) > 0 then ls_elenco_ordini += " - "
	ls_elenco_ordini += string(il_anno_registrazione[ll_i]) + "/" + string(il_num_registrazione[ll_i])
	
	wf_leggi_iva(il_anno_registrazione[ll_i], &
					il_num_registrazione[ll_i], &
					ld_aliquote_iva[], &
					ld_imponibili_valuta[], &
					ld_iva_valuta[], &
					ls_des_esenzione_iva[], &
					ls_codici_iva[])	
					
	select  data_consegna,
			 imponibile_iva_valuta,
			 importo_iva_valuta,
			 tot_val_ordine,
			 tot_merci,
			 tot_sconto_cassa,
			 tot_sconti_commerciali,
			 num_ord_cliente,
			 tot_val_ordine_valuta
	into     :ldt_data_consegna,
			 :ld_imponibile_iva_valuta,
			 :ld_importo_iva_valuta,
			 :ld_tot_val_ordine_euro,   
			 :ld_tot_merci,
			 :ld_tot_cassa,
			 :ld_tot_sconti_commerciali,
			 :ls_rif_ordine_cliente,
			 :ld_tot_val_ordine_valuta
	from   tes_ord_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_registrazione = :il_anno_registrazione[ll_i] and 
			 num_registrazione = :il_num_registrazione[ll_i];		
			 
	ld_sum_imponibile_iva_valuta += ld_imponibile_iva_valuta
	ld_sum_importo_iva_valuta += ld_importo_iva_valuta
	ld_sum_val_ordine_euro += ld_tot_val_ordine_euro
	ld_sum_val_ordine_valuta += ld_tot_val_ordine_valuta
	ld_sum_merci +=ld_tot_merci
	ld_sum_cassa += ld_tot_cassa
	ld_sum_sconti_commerciali += ld_tot_sconti_commerciali
	
	// Aggiungo il rif.ordine cliente alla numerazione "Rif.Vs-Ord"
	// Richiesto da Angela e Approvato da Betty 30.05.2012
	if not isnull(ls_rif_ordine_cliente) and len(ls_rif_ordine_cliente) > 0 then
		ls_elenco_ordini += " (" + ls_rif_ordine_cliente + ") "
	end if
	
	// prendo sempre la max data consegna dalla testata ordine (Beatrice)
	if isnull(ldt_data_consegna_max) then
		ldt_data_consegna_max = ldt_data_consegna
	else
		if ldt_data_consegna_max < ldt_data_consegna then
			ldt_data_consegna_max = ldt_data_consegna
		end if
	end if
	
	
	declare cu_dettagli cursor for 
		select     prog_riga_ord_ven,	
					cod_tipo_det_ven, 
					cod_misura, 
					quan_ordine, 
					prezzo_vendita, 
					sconto_1, 
					sconto_2, 
					sconto_3, 
					sconto_4, 
					sconto_5, 
					sconto_6, 
					sconto_7, 
					sconto_8, 
					sconto_9, 
					sconto_10, 
					nota_dettaglio, 
					cod_prodotto, 
					des_prodotto,
					fat_conversione_ven,
					cod_iva,
					flag_st_note_det,
					quantita_um,
					prezzo_um,
					imponibile_iva_valuta,
					dim_x,
					dim_y
		from      det_ord_ven 
		where    cod_azienda = :s_cs_xx.cod_azienda and 
					anno_registrazione = :il_anno_registrazione[ll_i] and 
					num_registrazione = :il_num_registrazione[ll_i]
		order by cod_azienda, 
					anno_registrazione, 
					num_registrazione,
					prog_riga_ord_ven;
	
	open cu_dettagli;
	
	if not isnull(ls_nota_testata) and len(trim(ls_nota_testata)) > 0 then lb_prima_riga = true
	
	lb_flag_prodotto_cliente = false
	lb_flag_nota_dettaglio = false
	lb_flag_nota_piede = false
	luo_stampa_dati_prodotto = create uo_stampa_dati_prodotto
	
	do while true
		if not(lb_prima_riga) and not(lb_flag_prodotto_cliente) and not(lb_flag_nota_dettaglio) then
			fetch cu_dettagli into 	  :ll_prog_riga_ord_ven,
										  :ls_cod_tipo_det_ven, 
										  :ls_cod_misura, 
										  :ld_quan_fatturata, 
										  :ld_prezzo_vendita,   
										  :ld_sconto_1, 
										  :ld_sconto_2, 
										  :ld_sconto_3, 
										  :ld_sconto_4, 
										  :ld_sconto_5, 
										  :ld_sconto_6, 
										  :ld_sconto_7, 
										  :ld_sconto_8, 
										  :ld_sconto_9, 
										  :ld_sconto_10, 
										  :ls_nota_dettaglio, 
										  :ls_cod_prodotto, 
										  :ls_des_prodotto,
										  :ld_fat_conversione_ven,
										  :ls_cod_iva,
										  :ls_flag_st_note_det,
										  :ld_quantita_um,
										  :ld_prezzo_um,
										  :ld_imponibile_riga,
										  :ld_dim_x_riga,
										  :ld_dim_y_riga;
										  
			if sqlca.sqlcode <> 0 then
				if not isnull(ls_nota_piede) and len(trim(ls_nota_piede)) > 0 then
					lb_flag_nota_piede = true
				else
					exit
				end if
			end if
			
			setnull(ls_cod_misura_mag)
			//////////////////////////////// Calcolo Valore Riga PT /////////////////////////////////////////
			
			luo_doc = create uo_calcola_documento_euro
			
			ld_imponibile_riga = ld_quan_fatturata * ld_prezzo_vendita
			luo_doc.uof_arrotonda(ld_imponibile_riga,ld_precisione,"euro")
			
			ld_valore_parziale = ld_imponibile_riga
			
			ld_sconto_tot = 0
			
			if ld_sconto_1 <> 0 and not isnull(ld_sconto_1) then
				ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_1)
				ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_1)
			end if
			
			if ld_sconto_2 <> 0 and not isnull(ld_sconto_2) then
				ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_2)
				ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_2)
			end if
			
			if ld_sconto_3 <> 0 and not isnull(ld_sconto_3) then
				ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_3)
				ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_3)
			end if
			
			if ld_sconto_4 <> 0 and not isnull(ld_sconto_4) then
				ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_4)
				ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_4)
			end if
			
			if ld_sconto_5 <> 0 and not isnull(ld_sconto_5) then
				ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_5)
				ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_5)
			end if
			
			if ld_sconto_6 <> 0 and not isnull(ld_sconto_6) then
				ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_6)
				ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_6)
			end if
			
			if ld_sconto_7 <> 0 and not isnull(ld_sconto_7) then
				ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_7)
				ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_7)
			end if
			
			if ld_sconto_8 <> 0 and not isnull(ld_sconto_8) then
				ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_8)
				ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_8)
			end if
			
			if ld_sconto_9 <> 0 and not isnull(ld_sconto_9) then
				ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_9)
				ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_9)
			end if
			
			if ld_sconto_10 <> 0 and not isnull(ld_sconto_10) then
				ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_10)
				ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_10)
			end if
			
			if ld_sconto_tes <> 0 and not isnull(ld_sconto_tes) then
				ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_tes)
				ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_tes)
			end if
			
			luo_doc.uof_arrotonda(ld_sconto_tot,ld_precisione,"euro")
			
			ld_imponibile_riga = ld_imponibile_riga - ld_sconto_tot
			
			destroy luo_doc
			
			/////////////////////////////////////////////////////////////////////////////////////////////////
			
	//------------------------------------------------- Modifica Nicola ---------------------------------------------
			if ls_flag_st_note_det = 'I' then //nota dettaglio
				select flag_st_note_ft
				  into :ls_flag_st_note_det
				  from tab_tipi_det_ven
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			end if		
			
			if ls_flag_st_note_det = 'N' then
				ls_nota_dettaglio = ""
			end if				
	//-------------------------------------------------- Fine Modifica ----------------------------------------------				
			
		end if
			
		select	 flag_stampa_ordine,
					 des_tipo_det_ven
		into   	:ls_flag_stampa_ordine,
					:ls_des_tipo_det_ven
		from   tab_tipi_det_ven
		where cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
		if sqlca.sqlcode <> 0 then
			setnull(ls_flag_stampa_ordine)
			setnull(ls_des_tipo_det_ven)
		end if
		
		//Donato 15/12/2011
		//commentato perchè mandava in loop la costruzione del report
		//quando arrivavav alla fine delle righe, quindi sqlca.sqlcode=100 e nota_piede non vuota
		//impostava la variabile lb_flag_nota_piede a TRUE per permettere l'inserimento di una nuova riga con questo contenuto nel report
		//ma quando arrivava a questa istruzione il "continue" lo faceva tornare su e cosi via all'infinito
		//per questo ho aggiunto anche la condizione "and not lb_flag_nota_piede"
		
		
		//se non devi stampare la riga e se non sei alla fine (abilitato nota piede con nota piede non vuota)
		if ls_flag_stampa_ordine<>"S" and not lb_flag_nota_piede then continue
		
		//fine modifica ------------------------------------------------
		
			
		dw_report_fat_ven.insertrow(0)
		dw_report_fat_ven.setrow(dw_report_fat_ven.rowcount())
		
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "formato", ls_formato)
	
		/*
		select 	flag_stampa_fattura,
						tab_tipi_det_ven.des_tipo_det_ven
		into   		:ls_flag_stampa_ordine,
						:ls_des_tipo_det_ven
		from   	tab_tipi_det_ven
		where  	cod_azienda = :s_cs_xx.cod_azienda and  
					cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
		if sqlca.sqlcode <> 0 then
			setnull(ls_flag_stampa_ordine)
			setnull(ls_des_tipo_det_ven)
		end if
		*/
	
		if lb_prima_riga then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_nota_testata)
		elseif lb_flag_prodotto_cliente then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_prodotto", "Vs Codice")
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_cod_prod_cliente)
			lb_flag_prodotto_cliente = false
		elseif lb_flag_nota_dettaglio then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_nota_dettaglio)
			lb_flag_nota_dettaglio = false		
		elseif lb_flag_nota_piede then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_nota_piede)
		else
			
			if ls_flag_stampa_ordine = 'S' then
				select 	des_prodotto, 
							cod_misura_mag
				into   		:ls_des_prodotto_anag, 
							:ls_cod_misura_mag
				from   	anag_prodotti  
				where  	cod_azienda = :s_cs_xx.cod_azienda and  
							cod_prodotto = :ls_cod_prodotto;
				if sqlca.sqlcode <> 0 then
					setnull(ls_des_prodotto_anag)
					setnull(ls_cod_misura_mag)
				end if
				
	
				select 	des_prodotto  
				into   		:ls_des_prodotto_lingua  
				from    	anag_prodotti_lingue  
				where  	cod_azienda = :s_cs_xx.cod_azienda and  
							cod_prodotto = :ls_cod_prodotto and
							cod_lingua = :ls_cod_lingua;
				
				if sqlca.sqlcode <> 0 then
					setnull(ls_des_prodotto_lingua)
				end if
					
				select tab_prod_clienti.cod_prod_cliente  
				into   :ls_cod_prod_cliente  
				from   tab_prod_clienti  
				where  tab_prod_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
						 tab_prod_clienti.cod_prodotto = :ls_cod_prodotto and   
						 tab_prod_clienti.cod_cliente = :ls_cod_cliente;
		
				if sqlca.sqlcode <> 0 then
					setnull(ls_cod_prod_cliente)
				end if
				
				select aliquota, des_iva
				into   :ld_perc_iva, :ls_tab_ive_des_iva
				from   tab_ive
				where  tab_ive.cod_azienda = :s_cs_xx.cod_azienda and  
						 tab_ive.cod_iva = :ls_cod_iva;
		
				if sqlca.sqlcode <> 0 then
					setnull(ls_des_iva)
				else
					ls_des_iva = string(ld_perc_iva,"###")
				end if
				
				select flag_tipo_det_ven
				into   :ls_flag_tipo_det
				from   tab_tipi_det_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
						 
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("APICE","Errore in controllo tipo dettaglio.~nErrore nella select di tab_tipi_det_ven: " + sqlca.sqlerrtext)
					setnull(ls_flag_tipo_det)
				elseif sqlca.sqlcode = 100 then
					g_mb.messagebox("APICE","Errore in controllo tipo dettaglio.~nErrore nella select di tab_tipi_det_ven: tipo dettaglio non trovato")
					setnull(ls_flag_tipo_det)
				end if
				
				
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_misura", ls_cod_misura)
				
				// *** aggiunti  per estero
	//			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_quantita_um", ld_quantita_um)
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_quantita_um", ld_quan_fatturata)
	//			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_prezzo_um", ld_prezzo_um)
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_prezzo_um", ld_prezzo_vendita)
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_quan_ordine", ld_quan_fatturata)
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_prezzo_vendita", ld_prezzo_vendita)
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_misura_um", ls_cod_misura_mag)
				// **** fine estero	
				
				if not isnull(ls_cod_misura_mag) and ls_cod_misura_mag <> ls_cod_misura then
					dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_quan_ordine", ld_quantita_um)
					if ls_flag_tipo_det = 'S' then
						dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_prezzo_vendita", ld_prezzo_um * -1)
					else			
						dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_prezzo_vendita", ld_prezzo_um)
					end if				
				else
					dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_quan_ordine", ld_quan_fatturata)
					if ls_flag_tipo_det = 'S' then
						dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_prezzo_vendita", ld_prezzo_vendita * -1)
					else			
						dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_prezzo_vendita", ld_prezzo_vendita)
					end if				
				end if
				
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_val_riga", ld_imponibile_riga)
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_sconto_1", ld_sconto_1)
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_sconto_2", ld_sconto_2)
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_sconto_tot", ld_sconto_tot)			
				//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_iva", ls_des_iva)
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_iva", ls_cod_iva)
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_nota_dettaglio", ls_nota_dettaglio)
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_prodotto", ls_cod_prodotto)
		
				// Normale descrizione di riga
				if not isnull(ls_des_prodotto) then
					ls_descrizione_riga = ls_des_prodotto
				elseif not isnull(ls_des_prodotto_anag) then
					ls_descrizione_riga = ls_des_prodotto_anag
				else		
					ls_descrizione_riga = ls_des_tipo_det_ven
				end if
				
				// inizio la predisposizione della descrizione in lingua se il cliente ha un codice lingua associato
				ls_descrizione_riga_lingua = ""
				if not isnull(ls_cod_lingua)  then
					if not isnull(ls_des_prodotto_lingua) then
						ls_descrizione_riga_lingua = ls_des_prodotto_lingua
					elseif not isnull(ls_des_prodotto) then
						//ls_descrizione_riga_lingua = ls_des_prodotto
					elseif not isnull(ls_des_prodotto_anag) then
						//ls_descrizione_riga_lingua = ls_des_prodotto_anag
					else		
						//ls_descrizione_riga_lingua = ls_des_tipo_det_ven
					end if
				end if
				
				// EnMe 01-08-2017 nuova gestione nota dettaglio prodotto
				if not isnull(ls_cod_prodotto) then
					if guo_functions.uof_get_note_fisse(ls_null, ls_null, ls_cod_prodotto, "STAMPA_CONF_ORD_VEN", ls_null, ldt_data_registrazione, ref ls_stampa_nota_testata, ref ls_stampa_nota_piede, ref ls_stampa_nota_video) < 0 then
						g_mb.error(ls_stampa_nota_testata)
					else
						if len(ls_stampa_nota_testata) > 0 then ls_nota_dettaglio = g_str.implode({ls_nota_dettaglio,ls_stampa_nota_testata},"~r~n")
						if len(ls_stampa_nota_piede) > 0 then ls_nota_dettaglio = g_str.implode({ls_nota_dettaglio,ls_stampa_nota_piede},"~r~n")
						if len(ls_stampa_nota_video) > 0 then g_mb.warning( "NOTA FISSA", ls_stampa_nota_video)
					end if
				end if	
			
				if not isnull(ls_cod_prod_cliente) and len(trim(ls_cod_prod_cliente)) > 0 then
					lb_flag_prodotto_cliente = true
				end if
				if not isnull(ls_nota_dettaglio) and len(trim(ls_nota_dettaglio)) > 0 then
					lb_flag_nota_dettaglio = true
				end if
				
				// Dettagli complementari
				if il_anno_registrazione[ll_i] > 0 and not isnull(il_anno_registrazione[ll_i]) then
					ls_variabili = ""
					luo_stampa_dati_prodotto.uof_stampa_variabili( ls_cod_prodotto, "ORD_VEN", "ORD_VEN", ls_cod_tipo_ord_ven, il_anno_registrazione[ll_i], il_num_registrazione[ll_i], ll_prog_riga_ord_ven, ls_variabili)
					if len(ls_variabili) > 0 then
						ls_descrizione_riga= ls_des_prodotto + "~r~n" + ls_variabili
					else
						ls_descrizione_riga= ls_des_prodotto
					end if
					
					select cod_prod_finito, dim_x, dim_y, dim_z, dim_t, cod_non_a_magazzino, cod_verniciatura, cod_tessuto, flag_tipo_mantovana, colore_passamaneria
					into   :ls_cod_prodotto_finito, :ld_dim_x, :ld_dim_y, :ld_dim_z, :ld_dim_t, :ls_cod_non_a_magazzino, :ls_cod_verniciatura, :ls_cod_tessuto, :ls_flag_tipo_mantovana, :ls_colore_passamaneria
					from   comp_det_ord_ven  
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 anno_registrazione = :il_anno_registrazione[ll_i] and
							 num_registrazione = :il_num_registrazione[ll_i] and  
							 prog_riga_ord_ven = :ll_prog_riga_ord_ven ;
					if sqlca.sqlcode = 0 then	
						/*
						// GRUPPO TESSUTO
						if not isnull(ls_cod_tessuto) and ls_cod_tessuto <> "" then
							select des_prodotto
							into   :ls_des_gruppo_tessuto
							from   anag_prodotti
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_prodotto = :ls_cod_tessuto;
							if sqlca.sqlcode = 0 then
								ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_gruppo_tessuto
							end if		
							// descrizione in lingua del gruppo tessuto
							if not isnull(ls_cod_lingua)  then
								select des_prodotto
								into   :ls_des_gruppo_tessuto
								from   anag_prodotti_lingue
								where  cod_azienda = :s_cs_xx.cod_azienda and
										 cod_prodotto = :ls_cod_tessuto and
										 cod_lingua = :ls_cod_lingua;
								if sqlca.sqlcode = 0 then
									ls_descrizione_riga_lingua +=" " + ls_des_gruppo_tessuto
								end if		
							end if
						end if
						
						// COLORE  TESSUTO
						if not isnull(ls_cod_non_a_magazzino) and ls_cod_non_a_magazzino <> "" then
							ls_descrizione_riga = ls_descrizione_riga + " COL='" + ls_cod_non_a_magazzino + "' "
							ls_descrizione_riga_lingua = ls_descrizione_riga_lingua + " COL='" + ls_cod_non_a_magazzino + "' "
						end if
						
						//TIPO MANTOVANA
						if not isnull(ls_flag_tipo_mantovana) and len(ls_flag_tipo_mantovana) > 0 then
							ls_descrizione_riga = ls_descrizione_riga + " MANT='" + ls_flag_tipo_mantovana + "' "
							ls_descrizione_riga_lingua = ls_descrizione_riga_lingua + " MANT='" + ls_flag_tipo_mantovana + "' "
						end if			
						
						//COLORE PASSAMANERIA
						if not isnull(ls_colore_passamaneria) and len(ls_colore_passamaneria) > 0 then
							ls_descrizione_riga = ls_descrizione_riga + " PASS='" + ls_colore_passamaneria + "' "
							ls_descrizione_riga_lingua = ls_descrizione_riga_lingua + " PASS='" + ls_colore_passamaneria + "' "
						end if			
						
	
						// VERNICIATURA
						if not isnull(ls_cod_verniciatura) and ls_cod_verniciatura <> "" then
							select des_prodotto
							into   :ls_des_verniciatura
							from   anag_prodotti
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_prodotto = :ls_cod_verniciatura;
							if sqlca.sqlcode = 0 then
								ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_verniciatura
							end if
							// descrizione in lingua della verniciatura
							if not isnull(ls_cod_lingua)  then
								select des_prodotto
								into   :ls_des_verniciatura
								from   anag_prodotti_lingue
								where  cod_azienda = :s_cs_xx.cod_azienda and
										 cod_prodotto = :ls_cod_verniciatura and
										 cod_lingua = :ls_cod_lingua;
								if sqlca.sqlcode = 0 then
									ls_descrizione_riga_lingua +=" " + ls_des_verniciatura
								end if		
							end if
						end if
						
						// DIMENSIONI
						if ld_dim_x > 0 then
							dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(),"comp_det_dim_x", ld_dim_x)
						end if
						if ld_dim_y > 0 then
							dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(),"comp_det_dim_y", ld_dim_y)
						end if
						if ld_dim_z > 0 then
							dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(),"comp_det_dim_z", ld_dim_z)
						end if
						if ld_dim_t > 0 then
							dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(),"comp_det_dim_t", ld_dim_t)
						end if
						if not isnull(ls_cod_prodotto_finito) and len(ls_cod_prodotto_finito) > 0 then
							dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_prodotto", ls_cod_prodotto_finito)
						end if
						*/
					else
						// non è un prodotto configurato
						if ld_dim_x_riga > 0 then
							dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(),"comp_det_dim_x", ld_dim_x_riga)
						end if
						if ld_dim_y_riga > 0 then
							dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(),"comp_det_dim_y", ld_dim_y_riga)
						end if
						
					end if
					//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_descrizione_riga)
				end if
				
				if not isnull(ls_cod_lingua) and not isnull(ls_descrizione_riga_lingua) then ls_descrizione_riga += "~r~n" + ls_descrizione_riga_lingua
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_descrizione_riga)
	
			end if
		end if	
		
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_ive_des_iva", ls_tab_ive_des_iva)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "parametri_azienda_stringa", ls_stringa)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "parametri_azienda_stringa_euro", ls_stringa_euro)
		
//		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_anno_registrazione", il_anno_registrazione[ll_i])
//		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_num_registrazione", il_num_registrazione[ll_i])
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_tipo_fat_ven", ls_cod_tipo_ord_ven)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_data_registrazione", ldt_data_registrazione)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_cliente", ls_cod_cliente)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_valuta", ls_cod_valuta)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_pagamento", ls_cod_pagamento)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_sconto", ld_sconto_pagamento)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_banca_clien_for", ls_cod_banca_clien_for)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_num_ord_cliente", ls_num_ord_cliente)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_data_ord_cliente", ldt_data_ord_cliente)
		
		// stefanop: 10/12/2011: deposito
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_des_deposito", ls_des_deposito)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_provincia_dep", ls_provincia_dep)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cap_dep", ls_cap_dep)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_localita_dep", ls_localita_dep)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_indirizzo_dep", ls_indirizzo_dep)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_telefono_dep", ls_telefono_dep)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_fax_dep", ls_fax_dep)
		// ---
		
		// --- nazione: richiesto da Gibus 11-04-2012
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_cod_nazione", ls_cod_nazione)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_nazioni_des_nazione", ls_des_nazione)
		
	//---------------------------------- Modifica Nicola ---------------------------------------
	
		ls_rag_soc_1 = ls_rag_soc_1_cli
		ls_rag_soc_2 = ls_rag_soc_2_cli
		ls_indirizzo = ls_indirizzo_cli
		ls_cap = ls_cap_cli
		ls_localita = ls_localita_cli
		ls_provincia = ls_provincia_cli	
		

	//----------------------------------- Fine Modifica ----------------------------------------	
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_rag_soc_1", ls_rag_soc_1)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_rag_soc_2", ls_rag_soc_2)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_indirizzo", ls_indirizzo)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_localita", ls_localita)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_frazione", ls_frazione)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cap", ls_cap)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_provincia", ls_provincia)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_nota_testata", ls_nota_testata)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_nota_piede", ls_nota_piede)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_porto", ls_cod_porto)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_resa", ls_cod_resa)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_agente_1", ls_cod_agente_1)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_agenti_rag_soc_1", ls_anag_agenti_rag_soc_1)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_agente_2", ls_cod_agente_2)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_agenti_rag_soc_2", ls_anag_agenti_rag_soc_2)
		
		
//		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_tot_fattura_euro", ld_tot_val_ordine_euro)
		
//		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_imponibile_iva_valuta", 0)
//		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_iva_valuta", 0)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_vettore", ls_cod_vettore)
		
	
	//------------------------------------------ Fine Modifica ----------------------------------------------------- 	
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_causale_trasporto", ls_des_causale)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_telefono", ls_telefono_cliente)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_fax", ls_fax_cliente)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cambio_ven", ld_cambio_ven)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_indirizzo", ls_indirizzo_cli)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_localita", ls_localita_cli)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_frazione", ls_frazione_cli)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_cap", ls_cap_cli)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_provincia", ls_provincia_cli)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_partita_iva", ls_partita_iva)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_telefono", ls_telefono_des)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_fax", ls_fax_des)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_cod_fiscale", ls_cod_fiscale)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_pagamenti_des_estesa", ls_des_pagamento_estesa)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_pagamenti_sconto", ld_sconto_pagamento)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
		if ls_flag_tipo_pagamento = "R" then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_des_banca", ls_null)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_clien_for_des_banca", ls_des_banca)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_clien_for_cod_abi", ls_cod_abi)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_clien_for_cod_cab", ls_cod_cab)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_clien_for_cin", ls_cod_cin)
		else
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_clien_for_des_banca", ls_null)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_des_banca", ls_des_banca)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_cod_abi", ls_cod_abi)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_cod_cab", ls_cod_cab)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_cin", ls_cod_cin)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_iban", ls_cod_iban)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_conto_corrente", ls_conto_corrente)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_swift", ls_swift)
		end if
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_valute_des_valuta", ls_des_valuta)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_valute_lingue_des_valuta", ls_des_valuta_lingua)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_porti_des_porto", ls_des_porto)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_rese_des_resa", ls_des_resa)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_rag_soc_1", ls_vettore_rag_soc_1)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_indirizzo", ls_vettore_indirizzo)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_cap", ls_vettore_cap)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_localita", ls_vettore_localita)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_provincia", ls_vettore_provincia)
	
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_inoltro_cod_inoltro", ls_cod_inoltro)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_inoltro_rag_soc_1", ls_inoltro_rag_soc_1)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_inoltro_indirizzo", ls_inoltro_indirizzo)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_inoltro_cap", ls_inoltro_cap)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_inoltro_localita", ls_inoltro_localita)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_inoltro_provincia", ls_inoltro_provincia)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_peso_lordo", ld_peso_lordo)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_peso_netto", ld_peso_netto)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_mezzi_des_mezzo", ls_des_mezzo)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_imballi_des_imballo", ls_des_imballo)
		
		try
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_mezzi_lingue_des_mezzo", ls_des_mezzo_lingua)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_imballi_lingue_des_imballo", ls_des_imballo_lingua)
		catch(RuntimeError e)	
		end try
		ll_num_aliquote = upperbound(ld_aliquote_iva)
		if ll_num_aliquote > 0 then
			if ld_aliquote_iva[1] > 0 and not isnull(ld_aliquote_iva[1]) then
				//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_1", ld_aliquote_iva[1])
				//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_1", ls_codici_iva[1])
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_1", ld_imponibili_valuta[1])
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_1", ld_iva_valuta[1])
			else
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_1", ld_imponibili_valuta[1])
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_1", ls_des_esenzione_iva[1])
			end if
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_1", ls_codici_iva[1])
		end if
		if ll_num_aliquote > 1 then
			if ld_aliquote_iva[2] > 0 and not isnull(ld_aliquote_iva[2]) then
				//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_2", ld_aliquote_iva[2])
				//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_2", ls_codici_iva[2])			
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_2", ld_imponibili_valuta[2])
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_2", ld_iva_valuta[2])
			else
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_2", ld_imponibili_valuta[2])
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_2", ls_des_esenzione_iva[2])
			end if
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_2", ls_codici_iva[2])
		end if
		if ll_num_aliquote > 2 then
			if ld_aliquote_iva[3] > 0 and not isnull(ld_aliquote_iva[3]) then
				//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_3", ld_aliquote_iva[3])
				//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_3", ls_codici_iva[3])			
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_3", ld_imponibili_valuta[3])
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_3", ld_iva_valuta[3])
			else
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_3", ld_imponibili_valuta[3])
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_3", ls_des_esenzione_iva[3])
			end if
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_3", ls_codici_iva[3])					
		end if
		if ll_num_aliquote > 3 then
			if ld_aliquote_iva[4] > 0 and not isnull(ld_aliquote_iva[4]) then
				//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_4", ld_aliquote_iva[4])
				//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_4", ls_codici_iva[4])			
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_4", ld_imponibili_valuta[4])
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_4", ld_iva_valuta[4])
			else
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_4", ld_imponibili_valuta[4])
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_4", ls_des_esenzione_iva[4])
			end if
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_4", ls_codici_iva[4])
		end if
		if ll_num_aliquote > 4 then
			if ld_aliquote_iva[5] > 0 and not isnull(ld_aliquote_iva[5]) then
				//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_5", ld_aliquote_iva[5])
				//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_5", ls_codici_iva[5])			
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_5", ld_imponibili_valuta[5])
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_5", ld_iva_valuta[5])
			else
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_5", ld_imponibili_valuta[5])
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_5", ls_des_esenzione_iva[5])
			end if
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_5", ls_codici_iva[5])
		end if
		if ll_num_aliquote > 5 then
			if ld_aliquote_iva[6] > 0 and not isnull(ld_aliquote_iva[6]) then
				//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_6", ld_aliquote_iva[6])
				//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_6", ls_codici_iva[6])			
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_6", ld_imponibili_valuta[6])
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_6", ld_iva_valuta[6])
			else
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_6", ld_imponibili_valuta[6])
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_6", ls_des_esenzione_iva[6])
			end if
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_6", ls_codici_iva[6])
		end if
	
		
//		dw_report_fat_ven.setitem(1, "tes_fat_ven_imponibile_iva_valuta", ld_sum_imponibile_iva_valuta)
//		dw_report_fat_ven.setitem(1, "tes_fat_ven_iva_valuta", ld_sum_importo_iva_valuta)
		lb_prima_riga = false
		
		if lb_flag_nota_piede then exit
		
	loop
	close cu_dettagli;
	luo_stampa_dati_prodotto = create uo_stampa_dati_prodotto


next

ld_sum_merci = ld_sum_merci - ld_sum_sconti_commerciali

for ll_i = 1 to dw_report_fat_ven.getrow()
	dw_report_fat_ven.setitem(ll_i, "tes_fat_ven_tot_merci", ld_sum_merci)
	dw_report_fat_ven.setitem(ll_i, "tes_fat_ven_importo_sconto", ld_sum_cassa)
	
	dw_report_fat_ven.setitem(ll_i, "tes_fat_ven_tot_fattura_euro", ld_sum_val_ordine_euro)
	dw_report_fat_ven.setitem(ll_i, "tes_fat_ven_tot_fattura_valuta", ld_sum_val_ordine_valuta)
	dw_report_fat_ven.setitem(ll_i, "tes_fat_ven_imponibile_iva_valuta", ld_sum_imponibile_iva_valuta)
	dw_report_fat_ven.setitem(ll_i, "tes_fat_ven_iva_valuta", ld_sum_importo_iva_valuta)
	dw_report_fat_ven.setitem(ll_i, "tes_fat_ven_data_consegna", ldt_data_consegna_max)
	ll_settimana = guo_functions.uof_get_week_number(date(ldt_data_consegna_max))
	dw_report_fat_ven.setitem(ll_i, "tes_fat_ven_settimana_consegna", string(ll_settimana) )
	dw_report_fat_ven.setitem(ll_i, "tes_fat_ven_elenco_ordini", ls_elenco_ordini)	
	next

if not isnull(ls_cod_lingua) then
	
	ls_nome_file = s_cs_xx.volume + s_cs_xx.risorse + "condizioni_" + ls_cod_lingua + ".txt"
	
	li_FileNum = FileOpen(ls_nome_file, StreamMode!)
	if li_FileNum > 0 then
		if fileread(li_FileNum, ls_condizioni) > 0 then
			dw_report_fat_ven.object.condizioni_commerciali_t.text = ls_condizioni
		end if
	end if
end if

dw_report_fat_ven.reset_dw_modified(c_resetchildren)
dw_report_fat_ven.change_dw_current()
return 0
end function

public subroutine wf_stampa (long copie);string ls_error
long ll_i


dw_report_fat_ven.Object.DataWindow.Print.DocumentName="Report_Conferma_Ordine " + string(now())

il_totale_pagine = 1
copie = 1
// stefanop: Filigrana
//ls_error = dw_report_fat_ven.modify("DataWindow.Picture.File='" + is_path_filigrana + "'")
//if not isnull(ls_error) and ls_error <> "" then g_mb.error("File", ls_error)
//
//ls_error = dw_report_fat_ven.modify("DataWindow.Picture.Mode=1")
//if not isnull(ls_error) and ls_error <> "" then g_mb.error("Mode", ls_error)
//
//ls_error = dw_report_fat_ven.modify("DataWindow.Picture.Transparency=80")
//if not isnull(ls_error) and ls_error <> "" then g_mb.error("Transparency",ls_error)
//
//ls_error = dw_report_fat_ven.modify("DataWindow.Print.Background	='Yes'")
//if not isnull(ls_error) and ls_error <> "" then g_mb.error("Background", ls_error)
// ----

if ib_email then
	// processo di invio tramite EMAIL

	// invio la mail solo della prima copia
	if is_flag_email[il_corrente] = "S" then 
		wf_email()
	end if

else

	for il_pagina_corrente = 1 to il_totale_pagine
		
		for ll_i = 1 to copie
			/* Il cliente della fattura attuale accetta la fattura via mail,
				quindi NON invio la prima copia del destinatario              */
//			if is_flag_email[il_corrente] = "S" and ll_i = 1 then continue
		
//			// processo di normale stampa delle copie previste per il cliente
//			
//			choose case ll_i
//				case 1 
//					dw_report_fat_ven.object.st_copia.text = "Copia Destinatario"
//				case 2
//					dw_report_fat_ven.object.st_copia.text = "Copia Vettore / Raccolta Firme"
//				case 3
//					dw_report_fat_ven.object.st_copia.text = "Copia Mittente"
//				case else  //  dalla quarta in poi.
//					dw_report_fat_ven.object.st_copia.text = "Copia Interna Gibus"
//			end choose
			
//			ls_error = dw_report_fat_ven.modify("DataWindow.brushmode=0")
			

			
			dw_report_fat_ven.triggerevent("pcd_print")
				
		next
		
	next
	
end if

//dw_report_fat_ven.object.st_copia.text = ""

end subroutine

public function integer wf_email ();// funzione che provvedere all'invio MAIL

integer				li_ret
long					ll_prog_mimytype
string					ls_path, ls_messaggio, ls_destinatari[], ls_allegati[], ls_subject, ls_message, &
						ls_rag_soc_1,ls_cod_cliente, ls_nota, ls_azienda, ls_cod_lingua, ls_user_email
					
datetime				ldt_data_registrazione
uo_outlook			luo_outlook
uo_archivia_pdf	luo_pdf
transaction			lt_tran_docs
string					ls_errore


if isnull(is_email_amministrazione[il_corrente]) or len(is_email_amministrazione[il_corrente]) < 1 then
	g_mb.error("Invio Mail","Attenzione! Manca l''indirizzo destinatario 'email doc amministrativi' in anagrafica clienti.")
	return -1
end if
	
luo_pdf = CREATE uo_archivia_pdf
luo_outlook = CREATE uo_outlook

ls_path = luo_pdf.uof_crea_pdf_path(dw_report_fat_ven)

if ls_path = "errore" then
	g_mb.messagebox ("Invio Mail","Errore nella creazione del file pdf.")
	return -1
end if


ls_destinatari[1] = is_email_amministrazione[il_corrente]
ls_allegati[1]    = ls_path

// se rischiesto ne mando una copia all'utente
if cbx_invia_mittente.checked then
	select		e_mail
	into		:ls_user_email
	from 		utenti
	where	cod_utente  = :s_cs_xx.cod_utente;
		
	if sqlca.sqlcode = 0 and not isnull(ls_user_email) and len(ls_user_email) > 0 then
		ls_destinatari[2] = ls_user_email
	end if
end if


select  data_registrazione, 
		 cod_cliente
into     :ldt_data_registrazione,
	     :ls_cod_cliente
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and	
       anno_registrazione = :il_anno_registrazione[il_corrente] and
		 num_registrazione = :il_num_registrazione[il_corrente];

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca dati fattura (wf_email)~r~n" + sqlca.sqlerrtext)
end if

select  rag_soc_1, 
		 cod_lingua
into     :ls_rag_soc_1,
		 :ls_cod_lingua
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
       	  cod_cliente = :ls_cod_cliente;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca dati cliente (wf_email)~r~n" + sqlca.sqlerrtext)
end if

select rag_soc_1
into :ls_azienda
from aziende
where cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ls_azienda) then ls_azienda=""

if not isnull(ls_cod_lingua) then
	ls_subject = ls_azienda+" - Order Confirmation. " + string(il_anno_registrazione[il_corrente]) + "/" + string(il_num_registrazione[il_corrente]) + "  " + string(ldt_data_registrazione,"dd/mm/yyyy")
else
	ls_subject = ls_azienda+" - Conferma D'ordine nr. " + string(il_anno_registrazione[il_corrente]) + "/" + string(il_num_registrazione[il_corrente]) + " del " + string(ldt_data_registrazione,"dd/mm/yyyy")
end if

wf_testo_mail(ls_rag_soc_1, il_anno_registrazione[il_corrente], il_num_registrazione[il_corrente], ldt_data_registrazione, ls_cod_lingua, ls_message)
luo_outlook.ib_html = false

if not luo_outlook.uof_invio_sendmail( ls_destinatari[], ls_subject, ls_message, ls_allegati[], ref ls_messaggio) then
	g_mb.messagebox("APICE", "Errore in fase di invio Conferma Ordine: " + ls_messaggio)
else
	
	//creo transazione per i documenti
	li_ret = wf_crea_transaction_docs(lt_tran_docs, ls_errore)
	if li_ret<0 then
		//problemi in creazione transazione documenti, non archiviare il PDF
		g_mb.error(ls_errore)
		g_mb.warning("A causa di un errore in creazione transazione non è stato possibile archiviare il PDF della conferma d'ordine, ma l'email è stata inviata correttamente!")
		
	else
		wf_memorizza_blob(il_anno_registrazione[il_corrente], il_num_registrazione[il_corrente], ls_path, lt_tran_docs)
		commit;
		commit using lt_tran_docs;
		
	end if
	
	g_mb.success("Mail inviata con successo")
end if
	
destroy luo_outlook

filedelete(ls_path)

return 0
end function

public function integer wf_replace_marker (ref string as_source, string as_find, string as_replace, boolean ab_case_sensitive);/*
Function fof_replace_text
Created by: Michele
Creation Date: 13/12/2007
Comment: Given a string <as_source>, replaces all occurencies of <as_find> with <as_replace>

as_source                                                          Reference to the string to process
as_find                                                               The text we wish to change
as_replace                                                        The new text to be put in place of <as_find>

Return Values
Value                                                                   Comments
 1                                                                                           Everything OK
-1                                                                                          Some error occured
*/
long		ll_pos

 if isnull(as_source) or as_source = "" then
	return -1
end if

if isnull(as_find) or as_find = "" then
	return -1
end if

 ll_pos = 1

do
	 if ab_case_sensitive then
		ll_pos = pos(as_source,as_find,ll_pos)
	 else
		ll_pos = pos(lower(as_source),lower(as_find),ll_pos)
	 end if

	 if ll_pos = 0 then
		continue
	 end if
	 
	 as_source = replace(as_source,ll_pos,len(as_find),as_replace)	 
	 ll_pos += len(as_replace)

loop while ll_pos > 0

return 1


end function

public function integer wf_imposta_barcode ();string ls_bfo, ls_errore
int li_risposta, li_bco

li_risposta = f_font_barcode(ls_bfo,li_bco,ls_errore)
                              
if li_risposta < 0 then
                messagebox("SEP","Report 1 tende da sole: " + ls_errore,stopsign!)
                return -1
end if
                              
//dw_report.Modify("barcode.text='*"+is_CSB+ string(ll_progr_det_produzione) +is_CSB+ "*'")
dw_report_fat_ven.Modify("cf_barcode.Font.Face='" + ls_bfo + "'")
dw_report_fat_ven.Modify("cf_barcode.Font.Height= -" + string(li_bco) )
//dw_report_fat_ven.Modify("cf_barcode.Font.Height= -" + string(14) )
end function

public function integer wf_testo_mail (string fs_rag_soc_1, long fl_anno, long fl_numero, datetime fdt_data, string fs_lingua, ref string fs_testo);string 			ls_default, ls_path, ls_nome_file, ls_testo, ls_mark_ragsoc, ls_mark_numero, ls_mark_data

integer			li_FileNum

long				ll_pos_ragsoc, ll_pos_numero, ll_pos_data

string				ls_rag_soc_azienda, ls_tel, ls_fax

//parametri
ls_nome_file = "testo_conferma_ordine"
ls_mark_ragsoc = "[RAGIONESOCIALE]"
ls_mark_numero = "[NUMERO]"
ls_mark_data = "[DATA]"

//il percorso contiene anche il backslash finale
if isnull(fs_lingua) then
	ls_path = s_cs_xx.volume + s_cs_xx.risorse +"11.5\" + ls_nome_file + ".txt"
else
	ls_path = s_cs_xx.volume + s_cs_xx.risorse +"11.5\" + ls_nome_file + "_" + fs_lingua + ".txt"
end if

ls_default = "Gentile Cliente " + fs_rag_soc_1 + ", " + &
					"con la presente Le trasmettiamo in allegato la conferma d'ordine."
	
ls_default += "~r~n"
ls_default += "Per qualunque chiarimento siamo a Vostra disposizione.~r~n~r~n" + &
					"Cordiali Saluti~r~n"

select rag_soc_1, telefono, fax
into :ls_rag_soc_azienda, :ls_tel, :ls_fax
from aziende
where cod_azienda=:s_cs_xx.cod_azienda;

if ls_rag_soc_azienda<>"" and not isnull(ls_rag_soc_azienda) then ls_default += ls_rag_soc_azienda + "~r~n"
if ls_tel<>"" and not isnull(ls_tel) then ls_default += "Tel. "+ls_tel + "~r~n"
if ls_fax<>"" and not isnull(ls_fax) then ls_default += "Fax. "+ls_fax + "~r~n"

ls_default += is_disclaimer

if not FileExists(ls_path) then
	g_mb.messagebox("APICE","Il file di configurazione del testo del messaggio e-mail '"+ls_path+"' non esiste oppure è stato spostato!~n~r"+"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

//leggo dal file il testo personalizzato
li_FileNum = FileOpen(ls_path, TextMode!)
FileReadEx(li_FileNum, ls_testo)
FileClose(li_FileNum)


//sostituire i marcatori con i valori opportuni
//[RAGIONESOCIALE]
//[NUMERO]
//[DATA]

//ll_pos_ragsoc = pos(ls_testo, ls_mark_ragsoc)
//ll_pos_numero = pos(ls_testo, ls_mark_numero)
//ll_pos_data = pos(ls_testo, ls_mark_data)
//
//if ll_pos_ragsoc>0 then
//else
//	g_mb.messagebox("APICE","Marcatore "+ls_mark_ragsoc+" non trovato nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+ "Verrà usato un testo di default!",Exclamation!)
//	fs_testo = ls_default
//	return 1
//end if
//
//if ll_pos_numero>0 then
//else
//	g_mb.messagebox("APICE","Marcatore "+ls_mark_numero+" non trovato nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+	"Verrà usato un testo di default!",Exclamation!)
//	fs_testo = ls_default
//	return 1
//end if
//
//if ll_pos_data>0 then
//else
//	g_mb.messagebox("APICE","Marcatore "+ls_mark_data+" non trovato nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+	"Verrà usato un testo di default!",Exclamation!)
//	fs_testo = ls_default
//	return 1
//end if

fs_testo = ls_testo

//ragione sociale
 wf_replace_marker(fs_testo, ls_mark_ragsoc, fs_rag_soc_1, false)

//  anno/numero
wf_replace_marker(fs_testo, ls_mark_numero, string(fl_anno)+"/"+string(fl_numero), false)

//data
wf_replace_marker(fs_testo, ls_mark_data, string(fdt_data,"dd/mm/yyyy"), false)

return 1
end function

public function integer wf_calcola_ordini ();string ls_messaggio
long ll_i
uo_calcola_documento_euro luo_calcola_documento_euro

luo_calcola_documento_euro = create uo_calcola_documento_euro

luo_calcola_documento_euro.ib_salta_esposiz_cliente = true

for ll_i = 1 to upperbound(il_anno_registrazione)
	w_cs_xx_mdi.setmicrohelp(g_str.format("Calcolo ordine $1/$2", il_anno_registrazione[ll_i], il_num_registrazione[ll_i]))
	if luo_calcola_documento_euro.uof_calcola_documento(il_anno_registrazione[ll_i], il_num_registrazione[ll_i],"ord_ven",ls_messaggio) <> 0 then
		if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.messagebox("APICE",ls_messaggio)
		rollback;
	else
		commit;
	end if
next

destroy luo_calcola_documento_euro

return 0

end function

public function integer wf_memorizza_blob (long fl_anno_registrazione, long fl_num_registrazione, string fs_path, transaction at_tran);long ll_prog_mimytype,ll_ret
blob l_blob
string ls_note, ls_nota, ls_cod_nota

ll_ret = f_file_to_blob(fs_path, l_blob)
if ll_ret < 0 then return -1

ll_ret = len(l_blob)


SELECT prog_mimetype  
INTO   :ll_prog_mimytype  
FROM   tab_mimetype  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and
		 estensione = 'PDF'  OR estensione = 'pdf';
		 
		 
if sqlca.sqlcode = 100 then
	g_mb.messagebox("APICE","Impossibile archiviare il documento: impostare il mimetype")
	return 0
end if

setnull(ls_cod_nota)

select max(cod_nota)
into   :ls_cod_nota
from   tes_ord_ven_note
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 cod_nota like 'C%';
		 
if isnull(ls_cod_nota) or len(ls_cod_nota) < 1 then
	ls_cod_nota = "C01"
else
	ls_cod_nota = right(ls_cod_nota, 2)
	ll_ret = long(ls_cod_nota)
	ll_ret ++
	ls_cod_nota = "C" + string(ll_ret, "00")
end if

ls_nota = "Invio Conferma Ordine tramite e-mail"
ls_note = "Conferma d'ordine inviata dall'utente " + s_cs_xx.cod_utente + "~r~n" + &
          "Data invio:" + string(today(), "dd/mm/yyyy") + "  Ora invio:" + string(now(), "hh:mm:ss")

INSERT INTO tes_ord_ven_note  
		( cod_azienda,
		  anno_registrazione,   
		  num_registrazione,   
		  cod_nota,   
		  des_nota, 
		  note,
		  prog_mimetype)  
VALUES ( :s_cs_xx.cod_azienda,   
		  :fl_anno_registrazione,   
		  :fl_num_registrazione,   
		  :ls_cod_nota,   
		  :ls_nota,   
		  :ls_note,
		  :ll_prog_mimytype )
using at_tran;

updateblob tes_ord_ven_note
set       	  note_esterne = :l_blob
where      cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :fl_anno_registrazione and
			  num_registrazione = :fl_num_registrazione and
			  cod_nota = :ls_cod_nota
using at_tran;



end function

public function integer wf_crea_transaction_docs (ref transaction at_tran, ref string as_errore);string				ls_databasedocs, ls_servernamedocs, ls_err


ls_databasedocs = ""
ls_servernamedocs = ""
Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "databasedocs", ls_databasedocs)
Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "servernamedocs", ls_servernamedocs)


if not isnull(ls_servernamedocs) and len(ls_servernamedocs) > 0 and not isnull(ls_databasedocs) and len(ls_databasedocs) > 0 then
	if not guo_functions.uof_create_transaction_from( sqlca, at_tran, ls_servernamedocs, ls_databasedocs, ls_err)  then
		as_errore = "Errore in creazione transazione: " + ls_err
		return -1
	end if
else
	if not guo_functions.uof_create_transaction_from( sqlca, at_tran, ls_err)  then
		as_errore = "Errore in creazione transazione: " + ls_err
		return -1
	end if
end if


return 0

end function

event pc_setwindow;call super::pc_setwindow;boolean lb_invio_singolo_mail=false

long    ll_i, ll_anno, ll_num, ll_num_documento, ll_ret, ll_righe_importo_zero

string  ls_cod_documento,ls_cod_tipo_fat_ven,ls_dataobject, ls_flag_email, ls_email_amministrazione, &
		  ls_cod_cliente, ls_flag_email_inviata, ls_cod_lingua_cliente

dw_report_fat_ven.ib_dw_report=true

set_w_options(c_noresizewin)
il_anno_registrazione = s_cs_xx.parametri.parametro_d_1_a
il_num_registrazione  = s_cs_xx.parametri.parametro_d_2_a

if il_corrente = 0 then 
	ib_email = s_cs_xx.parametri.parametro_b_2
end if

dw_report_fat_ven.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
						   	 c_disablecc, &
						      c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)

s_cs_xx.parametri.parametro_b_1 = false

ib_email = s_cs_xx.parametri.parametro_b_2
s_cs_xx.parametri.parametro_b_2 = false

ib_email = false
is_flag_email[1] = "N"
is_email_amministrazione[1] = ""

// imposto il layout corretto
wf_impostazioni()

// eseguo calcolo documenti
wf_calcola_ordini()

// visualizzo il report
wf_report()

il_corrente --

event post ue_reset_lingua_window()
end event

on w_report_conferma_ordine.create
int iCurrent
call super::create
this.cbx_1=create cbx_1
this.cbx_invia_mittente=create cbx_invia_mittente
this.dw_report_fat_ven=create dw_report_fat_ven
this.cb_stampa=create cb_stampa
this.cb_invia_mail=create cb_invia_mail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_1
this.Control[iCurrent+2]=this.cbx_invia_mittente
this.Control[iCurrent+3]=this.dw_report_fat_ven
this.Control[iCurrent+4]=this.cb_stampa
this.Control[iCurrent+5]=this.cb_invia_mail
end on

on w_report_conferma_ordine.destroy
call super::destroy
destroy(this.cbx_1)
destroy(this.cbx_invia_mittente)
destroy(this.dw_report_fat_ven)
destroy(this.cb_stampa)
destroy(this.cb_invia_mail)
end on

event pc_print;//IN QUESTO EVENTO IL FLAG EXTEND ANCESTOR DEVE ESSERE DISATTIVATO
cb_stampa.triggerevent("clicked")
end event

type cbx_1 from checkbox within w_report_conferma_ordine
integer x = 2080
integer y = 20
integer width = 1166
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "Nascondi indicazioni data consegna"
end type

event clicked;if checked then
	dw_report_fat_ven.object.t_21.visible=0
	dw_report_fat_ven.object.t_22.visible=0
	dw_report_fat_ven.object.tes_fat_ven_settimana_consegna.visible=0
else
	dw_report_fat_ven.object.t_21.visible=1
	dw_report_fat_ven.object.t_22.visible=1
	dw_report_fat_ven.object.tes_fat_ven_settimana_consegna.visible=1
end if
end event

type cbx_invia_mittente from checkbox within w_report_conferma_ordine
integer x = 791
integer y = 20
integer width = 1065
integer height = 68
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "Invia una copia email al mittente"
boolean checked = true
end type

type dw_report_fat_ven from uo_cs_xx_dw within w_report_conferma_ordine
integer x = 23
integer y = 104
integer width = 3817
integer height = 4460
integer taborder = 30
string dataobject = "d_report_conferma_ordine_gibus_ita"
end type

event pcd_first;call super::pcd_first;wf_report()
end event

event pcd_last;call super::pcd_last;wf_report()
end event

event pcd_next;call super::pcd_next;wf_report()
end event

event pcd_previous;call super::pcd_previous;wf_report()
end event

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

event printend;call super::printend;ib_stampando = true
end event

event printpage;call super::printpage;// stefanop: 17/02/2012: devo controlla se sto stampando per l'invo della mail o se è per la stampante
// Nel caso della mail non devo fare nessun controllo sul numero di pagina altrimenti il saveas della datawindow
// mi ritorna sempre una foglio bianco!
if ib_email then return 0
// -------------------------------------------------------------

if ib_stampa and pagenumber <> il_pagina_corrente then
	return 1
end if
end event

event printstart;call super::printstart;il_totale_pagine = pagesmax
end event

event ue_anteprima_pdf;// Attenzione tolto Ancestor Script
ib_email = true

super::event ue_anteprima_pdf(wparam, lparam)

ib_email = false
end event

type cb_stampa from commandbutton within w_report_conferma_ordine
integer x = 23
integer y = 12
integer width = 366
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;wf_stampa(1)
end event

type cb_invia_mail from commandbutton within w_report_conferma_ordine
integer x = 402
integer y = 12
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Invia Mail"
end type

event clicked;string ls_cod_cliente, ls_flag_accetta_mail,ls_email_amministrazione

il_corrente = 1

select cod_cliente
into   :ls_cod_cliente
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione[1] and
		 num_registrazione =  :il_num_registrazione[1];
		 
select flag_accetta_mail,
       email_amministrazione
into   :ls_flag_accetta_mail,
       :ls_email_amministrazione
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente;
		 

if ls_flag_accetta_mail = "S" then 
	ib_email = true
else
	return
end if

if isnull(ls_email_amministrazione) or len(ls_email_amministrazione) < 1 or pos(ls_email_amministrazione, "@")< 1 then
	g_mb.messagebox("APICE","Impossibile inviare la mail: verificare le impostazione in anagrafica cliente")
	return
end if

is_flag_email[1] = "S"
is_email_amministrazione[1] = ls_email_amministrazione

wf_stampa(1)

ib_email = false
end event

