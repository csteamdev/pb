﻿$PBExportHeader$w_report_taglio_profili.srw
forward
global type w_report_taglio_profili from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_taglio_profili
end type
type dw_folder from u_folder within w_report_taglio_profili
end type
type dw_report_2 from uo_cs_xx_dw within w_report_taglio_profili
end type
type dw_lista from uo_cs_xx_dw within w_report_taglio_profili
end type
type dw_ricerca from uo_cs_xx_dw within w_report_taglio_profili
end type
type dw_colonne_dinamiche from uo_cs_xx_dw within w_report_taglio_profili
end type
type hpb_1 from hprogressbar within w_report_taglio_profili
end type
type cb_stampa from commandbutton within w_report_taglio_profili
end type
type cb_stampa_2 from commandbutton within w_report_taglio_profili
end type
end forward

global type w_report_taglio_profili from w_cs_xx_principale
integer width = 3886
integer height = 2712
string title = "Stampa Etichette per Taglio Profili"
event ue_stabilimento_operatore ( )
dw_report dw_report
dw_folder dw_folder
dw_report_2 dw_report_2
dw_lista dw_lista
dw_ricerca dw_ricerca
dw_colonne_dinamiche dw_colonne_dinamiche
hpb_1 hpb_1
cb_stampa cb_stampa
cb_stampa_2 cb_stampa_2
end type
global w_report_taglio_profili w_report_taglio_profili

type variables
string 				is_flag_supervisore = "N"


datastore			ids_data

//string is_cod_colonna_dinamica_profili = "DDTAGL"
end variables

forward prototypes
public function integer wf_ricerca (ref string fs_errore)
public function integer wf_annulla ()
public function integer wf_filtra_valori (string fs_cod_colonna_dinamica)
public function long wf_leggi_colonna_dinamica (string fs_cod_prodotto, string fs_cod_colonna_dinamica, ref string fs_des_valore_lista, ref string fs_errore)
public function long wf_check_colonne_dinamiche (string fs_cod_prodotto, ref string fs_cod_colonna_dinamica, ref string fs_errore)
public function integer wf_create_ds ()
public subroutine wf_da_barcode ()
protected function integer wf_distinte_taglio_calcolate (long fl_anno_reg, long fl_num_reg, long fl_prog_riga, string fs_flag_stampate, ref string fs_cod_prodotto_padre[], ref long fl_num_sequenza[], ref string fs_cod_prodotto_figlio[], ref string fs_cod_versione[], ref string fs_cod_versione_figlio[], ref long fl_progressivo[], ref long fl_prog_calcolo[], ref decimal fd_misure[], ref integer fl_quantita[], ref string fs_um[], ref string fs_valore_colonna_dinamica[], ref string fs_errore)
public function long wf_assegna_posizione (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref datastore ads_data)
public function integer wf_crea_sessione (long al_progr_det_produzione, string as_cod_operaio, ref string as_errore)
public function boolean wf_check_operaio ()
public function integer wf_post_stampa ()
public subroutine wf_carica_sql_barcode (ref long al_barcode[], string as_dtaglio[], ref string as_cod_reparto)
public function long wf_verifica_distinte_taglio (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine)
end prototypes

event ue_stabilimento_operatore();integer				li_ret
string				ls_cod_deposito_operatore, ls_errore

li_ret = guo_functions.uof_get_stabilimento_utente(ls_cod_deposito_operatore, ls_errore)

if li_ret<0 then
	g_mb.error("Attenzione!", ls_errore)
	return
end if

dw_ricerca.setitem(1, "cod_deposito", ls_cod_deposito_operatore)

if is_flag_supervisore="S" then
else
	//blocca il campo deposito
	dw_ricerca.object.cod_deposito.protect = 1
end if

//popola i reparti per deposito (se non vuoto)
if ls_cod_deposito_operatore<>"" and not isnull(ls_cod_deposito_operatore) then
	f_po_loaddddw_dw(dw_ricerca, &
							  "cod_reparto", &
							  sqlca, &
							  "anag_reparti", &
							  "cod_reparto", &
							  "des_reparto", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
							  "cod_deposito = '"+ls_cod_deposito_operatore+"' ")
end if


return
end event

public function integer wf_ricerca (ref string fs_errore);string			ls_cod_deposito, ls_cod_reparto, ls_sql, ls_ordine, ls_cod_prodotto_figlio[], ls_vuoto[], ls_um[], ls_valori_col_din[],ls_tipo_ordinamento, ls_appo, &
					ls_flag_blocco, ls_flag_stampate, ls_flag_pagebreak, ls_des_reparto, ls_flag_DT
				
datetime		ldt_data_pronto_da, ldt_data_pronto_a, ldt_data_reg_da, ldt_data_reg_a

datastore		lds_data, lds_temp

long				ll_index, ll_tot, ll_new, ll_index_2, ll_tot_2, ll_index_3, ll_tot_3, ll_quan_ordinata, ll_index_qta, ll_anno_reg, ll_num_reg, ll_flag_stato_produzione, ll_ret_prod, &
					ll_riga_reg, ll_barcodes[], ll_pos
				
decimal			ld_misure[], ld_vuoto[]

integer			li_quantita[], li_vuoto[]

boolean			lb_continue = false, lb_ok

uo_produzione luo_prod

string 			ls_cod_prodotto_padre[], ls_cod_versione[], ls_cod_versione_figlio[], ls_operatore, ls_dtaglio[]
long 				ll_num_sequenza[], ll_vuoto[], ll_prog[], ll_prog_calcolo[]


//variabili per il datastore -----------------------------------------------------------------
long				ll_anno_registrazione_ds, ll_num_registrazione_ds, ll_prog_riga_ord_ven_ds, ll_ordinamento_lp_ds, ll_progr_det_produzione_ds
string			ls_cod_cliente_ds, ls_rag_soc_1_ds, ls_rag_soc_abbreviata_ds, ls_cod_comando_ds, ls_des_comando, ls_cod_verniciatura_ds, ls_cod_comodo_vern, &
					ls_cod_prodotto_figlio_ds, ls_des_prodotto_figlio, ls_progressivo, ls_cod_tenda_ds, ls_des_tenda_ds, ls_cod_linea_ds, ls_des_linea_prodotto
datetime		ldt_data_consegna_ds, ldt_data_pronto_ds
decimal			ld_dim_x_ds, ld_dim_y_ds, ld_dim_z_ds, ld_dim_t_ds, ld_misura_ds, ld_quan_ordinata_ds
//-------------------------------------------------------------------------------------------------

dw_ricerca.accepttext()
dw_colonne_dinamiche.accepttext()

dw_ricerca.Modify("log_t.text='Elaborazione in corso ...'")

dw_lista.reset()
dw_report.reset()
dw_report_2.reset()


if dw_ricerca.object.cf_cod_operaio[1] <>"" and not isnull(dw_ricerca.object.cf_cod_operaio[1]) then
	ls_operatore = dw_ricerca.object.cf_cod_operaio[1]
else
	ls_operatore = ""
end if

//controlla di aver specificato almeno una colonna dinamica
for ll_index=1 to dw_colonne_dinamiche.rowcount()
	ls_appo = dw_colonne_dinamiche.getitemstring(ll_index, "cod_colonna_dinamica")
	if ls_appo<>"" and not isnull(ls_appo) then
		lb_continue = true
		exit
	end if
next

if not lb_continue then
	fs_errore = "Specificare almeno una colonna dinamica!"
	return -1
end if

//ordinamento etichette
ls_tipo_ordinamento = dw_ricerca.getitemstring(1, "flag_tipo_ordinamento")
if ls_tipo_ordinamento="" or isnull(ls_tipo_ordinamento) then ls_tipo_ordinamento = "M"
//------------------------

ls_sql = 	"select distinct "+&
							"T.anno_registrazione,"+&
							"T.num_registrazione,"+&
							"D.prog_riga_ord_ven,"+&
							"T.cod_cliente,"+&
							"C.rag_soc_1,"+&
							"C.rag_soc_abbreviata,"+&
							"T.data_consegna,"+&
							"DP.data_pronto,"+&
							"COMP.dim_x,"+&
							"COMP.dim_y,"+&
							"COMP.dim_z,"+&
							"COMP.dim_t,"+&
							"COMP.cod_comando,"+&
							"COMP.cod_verniciatura,"+&
							"D.quan_ordine,"+&
							"D.cod_prodotto,"+&
							"D.des_prodotto"
							
//if ls_tipo_ordinamento = "L" then
	ls_sql += 	","+&
					"isnull(LP.ordinamento, 999999999) as ordinamento_lp,isnull(LP.descrizione,'linea prod. NON assegnata>') as des_linea_prodotto,"+&
					"isnull(VERN.cod_linea_prodotto, '') as cod_linea"
//else
//	ls_sql += 	","+&
//					"0 as ordinamento_lp,"+&
//					"'' as cod_linea"
//end if

ls_sql += ",DP.progr_det_produzione"

ls_sql +=    " "+&
				"from det_ord_ven D "+&
				"join varianti_det_ord_ven VD on 	VD.cod_azienda=D.cod_azienda and "+&
													"VD.anno_registrazione=D.anno_registrazione and "+&
													"VD.num_registrazione=D.num_registrazione and "+&
													"VD.prog_riga_ord_ven=D.prog_riga_ord_ven "+&
				"join varianti_det_ord_ven_prod VP on 	VP.cod_azienda=VD.cod_azienda and "+&
													"VP.anno_registrazione=VD.anno_registrazione and "+&
													"VP.num_registrazione=VD.num_registrazione and "+&
													"VP.prog_riga_ord_ven=VD.prog_riga_ord_ven "+&
				"join comp_det_ord_ven COMP on 	COMP.cod_azienda=D.cod_azienda and "+&
													"COMP.anno_registrazione=D.anno_registrazione and "+&
													"COMP.num_registrazione=D.num_registrazione and "+&
													"COMP.prog_riga_ord_ven=D.prog_riga_ord_ven "+&
				"join det_ordini_produzione DP on 	DP.cod_azienda=D.cod_azienda and "+&
													"DP.anno_registrazione=D.anno_registrazione and "+&
													"DP.num_registrazione=D.num_registrazione and "+&
													"DP.prog_riga_ord_ven=D.prog_riga_ord_ven "+&
				"left outer join distinte_taglio_calcolate DT on 	DT.cod_azienda=D.cod_azienda and "+&
													"DT.anno_registrazione=D.anno_registrazione and "+&
													"DT.num_registrazione=D.num_registrazione and "+&
													"DT.prog_riga_ord_ven=D.prog_riga_ord_ven "+&
				"join tes_ord_ven T on 	T.cod_azienda=D.cod_azienda and "+&
													"T.anno_registrazione=D.anno_registrazione and "+&
													"T.num_registrazione=D.num_registrazione "+&
				"join anag_clienti C on C.cod_azienda=T.cod_azienda and "+&
													"C.cod_cliente=T.cod_cliente "
													
//if ls_tipo_ordinamento = "L" then
	ls_sql += "left join tab_des_linea_prodotto LP on LP.cod_azienda=D.cod_azienda and "+&
													"LP.cod_prodotto=D.cod_prodotto "
	ls_sql += "left join tab_verniciatura VERN on VERN.cod_azienda=COMP.cod_azienda and "+&
													"VERN.cod_verniciatura=COMP.cod_verniciatura "
//end if
													
													
//ls_sql += " where 	D.cod_azienda='"+s_cs_xx.cod_azienda+"' and DT.misura>0 and DT.quantita>0 and D.flag_evasione<>'E' "
ls_sql += " where 	D.cod_azienda='"+s_cs_xx.cod_azienda+"' and D.flag_evasione<>'E' "

ls_flag_stampate = dw_ricerca.getitemstring(1, "flag_stampate")
ls_flag_pagebreak = dw_ricerca.getitemstring(1, "flag_pagebreak")
if isnull(ls_flag_pagebreak) or ls_flag_pagebreak="" then  ls_flag_pagebreak = "N"

//###############################################################################################
if ids_data.rowcount() > 0 then
	//comandano i barcodes
	wf_carica_sql_barcode(ll_barcodes[], ls_dtaglio[], ls_cod_reparto)
	
	//popolo il datastore ma faccio in modo che sia vuoto
	ls_appo = ls_sql + " and D.cod_azienda='CODICEINESISTENTE'"
	ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_appo, fs_errore)
	if ll_tot<0 then
		//in fs_errore il messaggio di errore
		return -1
	end if
	
	
	select des_reparto
	into	:ls_des_reparto
	from anag_reparti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_reparto=:ls_cod_reparto;
				
	
	ll_tot_2 = upperbound(ll_barcodes[])
	
	for ll_index=1 to ll_tot_2
		Yield()
		ls_appo = ls_sql + "and  DP.progr_det_produzione=" + string(ll_barcodes[ll_index])
		
		dw_ricerca.Modify("log_t.text='"+string((ll_index / ll_tot_2) * 100, "##0") + " % "+" Elaborazione in corso barcode "+string(ll_barcodes[ll_index])+" ...'")
		
		ll_tot = guo_functions.uof_crea_datastore(lds_temp, ls_appo, fs_errore)
		if ll_tot < 0 then
			//in fs_errore il messaggio di errore
			destroy lds_data
			return -1
			
		elseif ll_tot>0 then
			//copio la prima (in realtà me ne aspetto sempre una sola) ...
			//ll_tot_2 += 1
			lds_temp.rowscopy(1, 1, Primary!, lds_data, 1, Primary!)
		end if
	next
	
else

	ls_cod_deposito = dw_ricerca.getitemstring(1, "cod_deposito")
	ls_cod_reparto = dw_ricerca.getitemstring(1, "cod_reparto")
	
	//deposito e reparto produzione sono obbligatori -----------------
	if isnull(ls_cod_deposito) or ls_cod_deposito="" then
		fs_errore = "Specificare il deposito di produzione!"
		return -1
	end if
	
	if isnull(ls_cod_reparto) or ls_cod_reparto="" then
		fs_errore = "Specificare il reparto di produzione!"
		return -1
	end if
	
	select des_reparto
	into	:ls_des_reparto
	from anag_reparti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_reparto=:ls_cod_reparto;
	
	ls_sql += " and VD.cod_deposito_produzione='"+ls_cod_deposito + "' "
	ls_sql += " and VP.cod_reparto='"+ls_cod_reparto+"' and DP.cod_reparto='"+ls_cod_reparto+"' "
	
	//---------------------------------------------------------------------------------------------------
	//almeno una di queste tre deve essere usata
	//		data pronto 
	//		data registrazione
	//		anno/numero/riga
	
	lb_ok = false
	
	ldt_data_pronto_da = dw_ricerca.getitemdatetime(1, "data_pronto_da")
	ldt_data_pronto_a = dw_ricerca.getitemdatetime(1, "data_pronto_a")
	
	ldt_data_reg_da = dw_ricerca.getitemdatetime(1, "data_reg_da")
	ldt_data_reg_a = dw_ricerca.getitemdatetime(1, "data_reg_a")
	
	ll_anno_reg = dw_ricerca.getitemnumber(1, "anno_registrazione")
	ll_num_reg = dw_ricerca.getitemnumber(1, "num_registrazione")
	ll_riga_reg = dw_ricerca.getitemnumber(1, "prog_riga_ord_ven")
	
	//data pronto
	if not isnull(ldt_data_pronto_da) and year(date(ldt_data_pronto_da))>1950 then
		
		ls_sql += " and DP.data_pronto>='"+string(ldt_data_pronto_da, s_cs_xx.db_funzioni.formato_data)+"' "
			
		if not isnull(ldt_data_pronto_a) and year(date(ldt_data_pronto_a))>1950 then
			ls_sql += " and DP.data_pronto<='"+string(ldt_data_pronto_a, s_cs_xx.db_funzioni.formato_data)+"' "
		else
			fs_errore = "Specificare la data pronto di fine periodo per la ricerca"
			return -1
		end if
		
		lb_ok = true
	end if
	
	//data registrazione
	if not isnull(ldt_data_reg_da) and year(date(ldt_data_reg_da))>1950 then
		ls_sql += " and T.data_registrazione>='"+string(ldt_data_reg_da, s_cs_xx.db_funzioni.formato_data)+"' "
		
		if not isnull(ldt_data_reg_a) and year(date(ldt_data_reg_a))>1950 then
			ls_sql += " and T.data_registrazione<='"+string(ldt_data_reg_a, s_cs_xx.db_funzioni.formato_data)+"' "
		else
			fs_errore = "Specificare la data registrazione di fine periodo per la ricerca"
			return -1
		end if
		
		lb_ok = true
	end if
	
	//anno/numero/riga
	if ll_anno_reg>0 then
		ls_sql += " and T.anno_registrazione="+string(ll_anno_reg)+" "
		
		if ll_num_reg>0 then
			ls_sql += " and T.num_registrazione="+string(ll_num_reg)+" "
		else
			fs_errore = "Specificare il numero ordine per la ricerca"
			return -1
		end if
		
		if ll_riga_reg>0 then
			ls_sql += " and D.prog_riga_ord_ven="+string(ll_riga_reg)+" "
		else
			fs_errore = "Specificare il numero riga ordine per la ricerca"
			return -1
		end if
		
		lb_ok = true
		
	end if
	
	if not lb_ok then
		fs_errore = "Specificare almeno una opzione tra range data pronto, range data registrazione e anno/numero/riga ordine per la ricerca"
		return -1
	end if
	//--------------------------------------------
	
	//flag blocco in testata
	ls_flag_blocco = dw_ricerca.getitemstring(1, "flag_blocco")
	if ls_flag_blocco<>"T" then ls_sql += " and T.flag_blocco='"+ls_flag_blocco+"' "
	
	
	ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, fs_errore)
	if ll_tot<0 then
		//in fs_errore il messaggio
		return -1
	end if
	
end if
//###############################################################################################

////stato produzione
//// 0		aperte, cioè NON INIZIATE
//// 1		chiuse, PRODUZIONE CHIUSA
//// 2		parziali, cioè PRODUZIONE INIZIATA
////999		indifferente
//ll_flag_stato_produzione = dw_ricerca.getitemnumber(1, "flag_stato_produzione")


ll_tot = lds_data.rowcount()

hpb_1.maxposition = ll_tot

dw_ricerca.Modify("log_t.text=''")

//mi serve un datastore per memorizzare numero assegnato alla riga e la riga ordine riga
ls_sql = 	"select 100 as pos, anno_registrazione, num_registrazione, prog_riga_ord_ven "+&
			"from det_ord_ven "+&
			"where cod_azienda='AZIENDAINESISTENTE'"

guo_functions.uof_crea_datastore( lds_temp, ls_sql, fs_errore)

for ll_index=1 to ll_tot
	Yield()
	
	hpb_1.stepit( )
	
	ll_anno_registrazione_ds	= lds_data.getitemnumber(ll_index, 1)
	ll_num_registrazione_ds	= lds_data.getitemnumber(ll_index, 2)
	ll_prog_riga_ord_ven_ds	= lds_data.getitemnumber(ll_index, 3)
	
	ls_ordine = string((ll_index / ll_tot) * 100, "##0") + " % Elaboraz. ordine " + string(ll_anno_registrazione_ds) + "/" + string(ll_num_registrazione_ds) + " - riga " + string(ll_prog_riga_ord_ven_ds) + " in corso ..."
	dw_ricerca.Modify("log_t.text='"+ls_ordine+"'")
	
	ls_cod_cliente_ds				= lds_data.getitemstring(ll_index, 4)
	ls_rag_soc_1_ds				= lds_data.getitemstring(ll_index, 5)
	ls_rag_soc_abbreviata_ds	= lds_data.getitemstring(ll_index, 6)
	ldt_data_consegna_ds		= lds_data.getitemdatetime(ll_index, 7)
	ldt_data_pronto_ds			= lds_data.getitemdatetime(ll_index, 8)
	
	ld_dim_x_ds					= lds_data.getitemdecimal(ll_index, 9)
	ld_dim_y_ds					= lds_data.getitemdecimal(ll_index, 10)
	ld_dim_z_ds						= lds_data.getitemdecimal(ll_index, 11)
	ld_dim_t_ds						= lds_data.getitemdecimal(ll_index, 12)
	ls_cod_comando_ds			= lds_data.getitemstring(ll_index, 13)
	
	ls_cod_verniciatura_ds		= lds_data.getitemstring(ll_index, 14)
	
	ld_quan_ordinata_ds			=  lds_data.getitemdecimal(ll_index, 15)

	ls_cod_tenda_ds				= lds_data.getitemstring(ll_index, 16)
	ls_des_tenda_ds				= lds_data.getitemstring(ll_index, 17)

	ll_ordinamento_lp_ds		= lds_data.getitemnumber(ll_index, 18)
	ls_des_linea_prodotto		= lds_data.getitemstring(ll_index, 19)
	ls_cod_linea_ds				= lds_data.getitemstring(ll_index, 20)
	
	ll_progr_det_produzione_ds = lds_data.getitemnumber(ll_index, 21)

	ll_quan_ordinata				= long(ld_quan_ordinata_ds )
	
//	if ll_flag_stato_produzione=999 then
//		//lo stato produzione è indifferente ...
//	else
//		luo_prod = create uo_produzione
//		ll_ret_prod = luo_prod.uof_stato_prod_det_ord_ven_reparto(ll_anno_registrazione_ds, ll_num_registrazione_ds, ll_prog_riga_ord_ven_ds, ls_cod_reparto, fs_errore)
//		destroy luo_prod;
//	end if
	
	ll_new = dw_lista.insertrow(0)
	//--------------------------------------
	dw_lista.setitem(ll_new, "anno_registrazione", ll_anno_registrazione_ds)
	dw_lista.setitem(ll_new, "num_registrazione", ll_num_registrazione_ds)
	dw_lista.setitem(ll_new, "prog_riga_ord_ven", ll_prog_riga_ord_ven_ds)

	dw_lista.setitem(ll_new, "cod_cliente", ls_cod_cliente_ds)
	dw_lista.setitem(ll_new, "rag_soc_1", ls_rag_soc_1_ds)
	
	dw_lista.setitem(ll_new, "data_consegna", ldt_data_consegna_ds)
	dw_lista.setitem(ll_new, "quan_ordinata",ld_quan_ordinata_ds)
	
	dw_lista.setitem(ll_new, "cod_prodotto",ls_cod_tenda_ds)
	
	ls_des_tenda_ds = ""
	if ls_des_tenda_ds="" or isnull(ls_des_tenda_ds) then
		select des_prodotto
		into :ls_des_tenda_ds
		from anag_prodotti
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto=:ls_cod_tenda_ds; 
	end if
	dw_lista.setitem(ll_new, "des_prodotto",ls_des_tenda_ds)
	
	
	//inserimento dati nella etichetta/e #################################
	ls_cod_prodotto_padre[] 		= ls_vuoto[]
	ll_num_sequenza[] 				= ll_vuoto[]
	ls_cod_prodotto_figlio[] 			= ls_vuoto[]
	ls_cod_versione[] 					= ls_vuoto[]
	ls_cod_versione_figlio[] 			= ls_vuoto[]
	ll_prog[] 								= ll_vuoto[]
	ll_prog_calcolo[] 					= ll_vuoto[]
	
	ld_misure[] 							= ld_vuoto[]
	li_quantita[] 						= li_vuoto[]
	ls_um[] 								= ls_vuoto[]

	if wf_distinte_taglio_calcolate(	ll_anno_registrazione_ds, ll_num_registrazione_ds, ll_prog_riga_ord_ven_ds, ls_flag_stampate, &
											ls_cod_prodotto_padre[], ll_num_sequenza[], ls_cod_prodotto_figlio[], ls_cod_versione[], ls_cod_versione_figlio[], &
											ll_prog[], ll_prog_calcolo[], &
											ld_misure[], li_quantita[], ls_um[], ls_valori_col_din[], fs_errore) < 0 then
		//in fs_errore il messaggio
		return -1
	end if

	if upperbound(ls_cod_prodotto_figlio[])>0 then
	else
//		//elimina la riga inserita in dw_lista e poi continua
//		dw_lista.deleterow(ll_new)

		dw_lista.setitem(ll_new, "dtaglio", "N")

		continue
	end if
	
	//inserisco nel datastore assegnadone il numero
	ll_pos = wf_assegna_posizione(ll_anno_registrazione_ds, ll_num_registrazione_ds, ll_prog_riga_ord_ven_ds, lds_temp)
	
	
	//in base alla quantità ordinata
	for ll_index_qta=1 to 1		//ll_quan_ordinata
	
		ll_tot_2 = upperbound(ls_cod_prodotto_figlio[])
		//in base al numero di distinte taglio (stessa dimensione per ls_cod_prodotto_figlio[], ld_misure[], li_quantita[], ls_um[] e ls_valori_col_din[])
		for ll_index_2=1 to ll_tot_2
			
			ls_cod_prodotto_figlio_ds = ls_cod_prodotto_figlio[ll_index_2]
			ld_misura_ds = ld_misure[ll_index_2]
			
			
			ls_ordine = "Elaboraz. ordine " + string(ll_anno_registrazione_ds) + "/" + string(ll_num_registrazione_ds) + " - riga " + string(ll_prog_riga_ord_ven_ds) + "  (DT "+ls_cod_prodotto_figlio_ds+") in corso ..."
			
			
			//una etichetta per quantità riportata in distinte taglio calcolate
			ll_tot_3 = li_quantita[ll_index_2]
			
			for ll_index_3=1 to ll_tot_3
				//REPORT ETICHETTE °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
				ll_new = dw_report.insertrow(0)
				dw_report.setitem(ll_new, "anno_registrazione", ll_anno_registrazione_ds)
				dw_report.setitem(ll_new, "num_registrazione", ll_num_registrazione_ds)
				dw_report.setitem(ll_new, "prog_riga_ord_ven", ll_prog_riga_ord_ven_ds)
			
				dw_report.setitem(ll_new, "cod_cliente", ls_cod_cliente_ds)
				
				if ls_rag_soc_abbreviata_ds<>"" and not isnull(ls_rag_soc_abbreviata_ds) then
					dw_report.setitem(ll_new, "rag_soc_1", ls_rag_soc_abbreviata_ds)
				else
					dw_report.setitem(ll_new, "rag_soc_1", ls_rag_soc_1_ds)
				end if
			
				dw_report.setitem(ll_new, "des_tenda",ls_des_tenda_ds)
			
				dw_report.setitem(ll_new, "data_consegna", ldt_data_consegna_ds)
				
				if ld_dim_x_ds>0 then dw_report.setitem(ll_new, "dim_x", ld_dim_x_ds)
				if ld_dim_y_ds>0 then dw_report.setitem(ll_new, "dim_y", ld_dim_y_ds)
				if ld_dim_z_ds>0 then dw_report.setitem(ll_new, "dim_z", ld_dim_z_ds)
				if ld_dim_t_ds>0 then dw_report.setitem(ll_new, "dim_t", ld_dim_t_ds)
				
				
				//----------------------------------------------------------------------------------------
				ls_des_comando = ""
				if ls_cod_comando_ds<>"" and not isnull(ls_cod_comando_ds) then
					select des_prodotto
					into :ls_des_comando
					from anag_prodotti
					where 	cod_azienda=:s_cs_xx.cod_azienda and
								cod_prodotto=:ls_cod_comando_ds;
				end if
				
				ls_cod_comodo_vern = ""
				if ls_cod_verniciatura_ds<>"" and not isnull(ls_cod_verniciatura_ds) then
					select cod_comodo
					into :ls_cod_comodo_vern
					from anag_prodotti
					where 	cod_azienda=:s_cs_xx.cod_azienda and
								cod_prodotto=:ls_cod_verniciatura_ds;
				end if
				
				select des_prodotto
				into :ls_des_prodotto_figlio
				from anag_prodotti
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							cod_prodotto=:ls_cod_prodotto_figlio_ds;
				//----------------------------------------------------------------------------------------
				
				
				if ls_des_comando<>"" and not isnull(ls_des_comando) then
					dw_report.setitem(ll_new, "des_comando_1", ls_des_comando)
				end if
				
				if ls_cod_comodo_vern<>"" and not isnull(ls_cod_comodo_vern) then
					dw_report.setitem(ll_new, "verniciatura", ls_cod_comodo_vern)
				end if
				
				dw_report.setitem(ll_new, "prodotto_figlio", ls_des_prodotto_figlio)
				dw_report.setitem(ll_new, "cod_prodotto_figlio", ls_cod_prodotto_figlio_ds)
				
				dw_report.setitem(ll_new, "cod_prodotto_padre", 	ls_cod_prodotto_padre[ll_index_2])
				dw_report.setitem(ll_new, "num_sequenza", 			ll_num_sequenza[ll_index_2])
				dw_report.setitem(ll_new, "cod_versione", 				ls_cod_versione[ll_index_2])
				dw_report.setitem(ll_new, "cod_versione_figlio", 		ls_cod_versione_figlio[ll_index_2])
				dw_report.setitem(ll_new, "prog", 						ll_prog[ll_index_2])
				dw_report.setitem(ll_new, "prog_calcolo", 				ll_prog_calcolo[ll_index_2])
				
								
				dw_report.setitem(ll_new, "misura_dt", ld_misura_ds)
				
				dw_report.setitem(ll_new, "um", ls_um[ll_index_2])
				
				dw_report.setitem(ll_new, "des_valore_profilo", ls_valori_col_din[ll_index_2])
				
				dw_report.setitem(ll_new, "ordinamento_lp", ll_ordinamento_lp_ds)
				dw_report.setitem(ll_new, "cod_linea", ls_cod_linea_ds)
				
				//if ls_tipo_ordinamento = "L" then
					dw_report.setitem(ll_new, "data_pronto", ldt_data_pronto_ds)
				//end if
				
				dw_report.setitem(ll_new, "progr_det_produzione", ll_progr_det_produzione_ds)
				
				//°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
				//REPORT A4 °°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
				ll_new = dw_report_2.insertrow(0)
				
				dw_report_2.setitem(ll_new, "cod_reparto", ls_cod_reparto)
				dw_report_2.setitem(ll_new, "des_reparto", ls_des_reparto)
				
				dw_report_2.setitem(ll_new, "operatore", ls_operatore)
				
				dw_report_2.setitem(ll_new, "pos", ll_pos)
				
				dw_report_2.setitem(ll_new, "anno_registrazione", ll_anno_registrazione_ds)
				dw_report_2.setitem(ll_new, "num_registrazione", ll_num_registrazione_ds)
				dw_report_2.setitem(ll_new, "prog_riga_ord_ven", ll_prog_riga_ord_ven_ds)
			
				dw_report_2.setitem(ll_new, "data_pronto", ldt_data_pronto_ds)
				
				if ld_dim_x_ds>0 then dw_report_2.setitem(ll_new, "dim_x", ld_dim_x_ds)
	
				ls_des_comando = ""
				select abbreviazione_comando
				into :ls_des_comando
				from tab_comandi
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							cod_comando=:ls_cod_comando_ds;
				
				if ls_des_comando<>"" and not isnull(ls_des_comando) then
					dw_report_2.setitem(ll_new, "abbreviazione_comando", ls_des_comando)
				end if
				
				
				ls_cod_comodo_vern = ""
				ls_des_comando = ""
				if ls_cod_verniciatura_ds<>"" and not isnull(ls_cod_verniciatura_ds) then
					select des_prodotto
					into :ls_cod_comodo_vern
					from anag_prodotti
					where 	cod_azienda=:s_cs_xx.cod_azienda and
								cod_prodotto=:ls_cod_verniciatura_ds;
				end if
				
				//uso la stessa variabile per non definirne altre
				ls_des_comando = ""
				select des_vernice_fc
				into :ls_des_comando
				from comp_det_ord_ven
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							anno_registrazione=:ll_anno_registrazione_ds and
							num_registrazione=:ll_num_registrazione_ds and
							prog_riga_ord_ven=:ll_prog_riga_ord_ven_ds;
				
				if ls_des_comando<>"" and not isnull(ls_des_comando) then ls_cod_comodo_vern += " " + ls_des_comando
				
				if ls_cod_comodo_vern<>"" and not isnull(ls_cod_comodo_vern) then
					dw_report_2.setitem(ll_new, "verniciatura", ls_cod_comodo_vern)
				end if
				
				dw_report_2.setitem(ll_new, "prodotto_figlio",			ls_des_prodotto_figlio)
				dw_report_2.setitem(ll_new, "cod_prodotto_figlio",	ls_cod_prodotto_figlio_ds)
								
				dw_report_2.setitem(ll_new, "misura_dt",				ld_misura_ds)
				
				dw_report_2.setitem(ll_new, "des_valore_profilo",	ls_valori_col_din[ll_index_2])
				
				dw_report_2.setitem(ll_new, "ordinamento_lp",		ll_ordinamento_lp_ds)
				dw_report_2.setitem(ll_new, "des_linea_profilo",		ls_des_linea_prodotto)
				
				if ls_flag_pagebreak="S" then
					dw_report_2.setitem(ll_new, "page_break",			ll_ordinamento_lp_ds)
				else
					dw_report_2.setitem(ll_new, "page_break",			0)
				end if
				
				dw_report_2.setitem(ll_new, "cod_linea",				ls_cod_linea_ds)
				
			
				//°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
			next
		next
	next
	//#####################################################
next

if ls_tipo_ordinamento = "M" then
	dw_report.setsort("des_valore_profilo asc, misura_dt desc, cod_prodotto_figlio asc")
	dw_report_2.setsort("des_valore_profilo asc, misura_dt desc, cod_prodotto_figlio asc")
	
elseif ls_tipo_ordinamento = "P" then
	dw_report.setsort("des_valore_profilo asc, cod_prodotto_figlio asc, misura_dt desc")
	dw_report_2.setsort("des_valore_profilo asc, cod_prodotto_figlio asc, misura_dt desc")
else
	//L
	dw_report.setsort("ordinamento_lp asc, des_valore_profilo asc, cod_linea asc, dim_x desc, data_pronto asc")
	dw_report_2.setsort("ordinamento_lp asc, des_valore_profilo asc, cod_linea asc, dim_x desc, data_pronto asc")
end if

dw_report.sort()

dw_report_2.sort()
dw_report_2.groupcalc()

dw_ricerca.Modify("log_t.text='Pronto!'")

dw_lista.change_dw_current()
dw_folder.fu_selecttab(2)



return 1
end function

public function integer wf_annulla ();string				ls_null
datetime			ldt_null
long				ll_null

setnull(ls_null)
setnull(ldt_null)
setnull(ll_null)

if is_flag_supervisore="S" then dw_ricerca.setitem(1, "cod_deposito", ls_null)

dw_ricerca.setitem(1, "cod_reparto", ls_null)
dw_ricerca.setitem(1, "data_pronto_da", ldt_null)
dw_ricerca.setitem(1, "data_pronto_a", ldt_null)
dw_ricerca.setitem(1, "flag_tipo_ordinamento", "M")
dw_ricerca.setitem(1, "flag_stato_produzione", 0)
dw_ricerca.setitem(1, "flag_blocco", "N")
dw_ricerca.setitem(1, "data_reg_da", ldt_null)
dw_ricerca.setitem(1, "data_reg_a", ldt_null)

dw_ricerca.setitem(1, "anno_registrazione", ll_null)
dw_ricerca.setitem(1, "num_registrazione", ll_null)
dw_ricerca.setitem(1, "prog_riga_ord_ven", ll_null)

dw_ricerca.setitem(1, "flag_stampate", "N")

dw_ricerca.setitem(1, "flag_pagebreak", "N")

dw_colonne_dinamiche.reset()

return 1
end function

public function integer wf_filtra_valori (string fs_cod_colonna_dinamica);datawindowchild  		ldwc_child
integer					li_child
string						ls_filter

li_child = dw_colonne_dinamiche.GetChild("progressivo", ldwc_child)


if fs_cod_colonna_dinamica="" or isnull(fs_cod_colonna_dinamica) then
	//fai vedere tutto
	ls_filter = ""
else
	//filtra
	ls_filter = "cod_colonna_dinamica='"+fs_cod_colonna_dinamica+"'"
end if

ldwc_child.setfilter(ls_filter)
ldwc_child.filter()

return 1
end function

public function long wf_leggi_colonna_dinamica (string fs_cod_prodotto, string fs_cod_colonna_dinamica, ref string fs_des_valore_lista, ref string fs_errore);long				ll_tot, ll_count, ll_id_valore_lista
string				ls_sql
datastore		lds_data

ls_sql = 	"select a.cod_colonna_dinamica, a.id_valore_lista, b.des_valore "+&
			"from anag_prodotti_col_din as a "+&
			"join tab_colonne_dinamiche_valori as b on 	b.cod_azienda=a.cod_azienda and "+&
																	"b.progressivo=a.id_valore_lista "+&
			"where 	a.cod_azienda='"+s_cs_xx.cod_azienda + "' and "+&
						"a.cod_colonna_dinamica='"+fs_cod_colonna_dinamica+"' and "+&
						"a.cod_prodotto='"+fs_cod_prodotto+"' "

ll_tot = guo_functions.uof_crea_datastore( lds_data, ls_sql, fs_errore)

if ll_tot<0 then
	//in fs_errore il messaggio
	return -1
end if

fs_des_valore_lista = ""

if ll_tot>0 then
	//OK condizione verificata, prendo il primo valore
	
	ll_id_valore_lista = lds_data.getitemnumber(1, 2)
	
	fs_des_valore_lista = lds_data.getitemstring(1, 3)
	
	fs_des_valore_lista = right("0000" + string(ll_id_valore_lista), 4) + " "+ fs_des_valore_lista
	
	if isnull(fs_des_valore_lista) then fs_des_valore_lista=""
	
else
	destroy lds_data;
	return 0
end if

destroy lds_data;
return 0
end function

public function long wf_check_colonne_dinamiche (string fs_cod_prodotto, ref string fs_cod_colonna_dinamica, ref string fs_errore);long				ll_index, ll_tot, ll_count, ll_id_valore_lista
string				ls_sql, ls_cod_colonna_dinamica_profili, ls_where_colonne
datastore		lds_data

//ritorna 
//		1		se il prodotto soddisfa tutti i criteri impostati per colonne dinamiche
//		0		se invece NON lo soddisfa (anche solo una condizione)
//		-1		in caso di errore

ll_tot = dw_colonne_dinamiche.rowcount()

ls_where_colonne = ""
for ll_index=1 to ll_tot
	ls_cod_colonna_dinamica_profili = dw_colonne_dinamiche.getitemstring(ll_index, "cod_colonna_dinamica")
	
	if ls_cod_colonna_dinamica_profili="" or isnull(ls_cod_colonna_dinamica_profili) then continue
	
	if ls_where_colonne<>"" then ls_where_colonne += ","
	ls_where_colonne += "'" + ls_cod_colonna_dinamica_profili + "'"
	
next

ls_sql = 	"select a.cod_colonna_dinamica "+&
			"from anag_prodotti_col_din as a "+&
			"where 	a.cod_azienda='"+s_cs_xx.cod_azienda + "' and "+&
						"a.cod_colonna_dinamica in ("+ls_where_colonne+") and "+&
						"a.cod_prodotto='"+fs_cod_prodotto+"' "

ll_tot = guo_functions.uof_crea_datastore( lds_data, ls_sql, fs_errore)

fs_cod_colonna_dinamica = ""

if ll_tot<0 then
	//in fs_errore il messaggio
	return -1
end if

if ll_tot>0 then
	//OK condizione verificata
	fs_cod_colonna_dinamica = lds_data.getitemstring(1, 1 )
	
	destroy lds_data;
	return 1
else
	//condizione non verificata
	
	destroy lds_data;
	return 0
end if

return 0
end function

public function integer wf_create_ds ();

destroy ids_data

ids_data = create datastore
ids_data.dataobject = "d_taglio_profili_sel_barcode"


return 0
end function

public subroutine wf_da_barcode ();s_cs_xx_parametri			lstr_parametri
long								ll_index, ll_new
string								ls_flag_da_barcode


lstr_parametri.parametro_ds_1 = ids_data

openwithparm(w_report_taglio_profili_barcode, lstr_parametri)

lstr_parametri = message.powerobjectparm

ids_data = lstr_parametri.parametro_ds_1

ls_flag_da_barcode= "N"
if ids_data.rowcount( )>0 then ls_flag_da_barcode= "S"

dw_ricerca.setitem(1, "flag_da_barcode", ls_flag_da_barcode)

return
end subroutine

protected function integer wf_distinte_taglio_calcolate (long fl_anno_reg, long fl_num_reg, long fl_prog_riga, string fs_flag_stampate, ref string fs_cod_prodotto_padre[], ref long fl_num_sequenza[], ref string fs_cod_prodotto_figlio[], ref string fs_cod_versione[], ref string fs_cod_versione_figlio[], ref long fl_progressivo[], ref long fl_prog_calcolo[], ref decimal fd_misure[], ref integer fl_quantita[], ref string fs_um[], ref string fs_valore_colonna_dinamica[], ref string fs_errore);string			ls_cod_prodotto, ls_cod_prodotto_padre, ls_cod_versione_padre, ls_cod_versione, ls_cod_um, ls_des_valore_col_din, ls_cod_colonna_dinamica, &
					ls_flag_stampate_cu, ls_flag_azzera_dt_cu
					
decimal			ld_quantita, ld_misura

long				ll_index, ll_num_sequenza, ll_progressivo, ll_ret_col_din, ll_prog_calcolo




declare cu_distinte_taglio cursor for  
 select	DT.cod_prodotto_padre,
 			DT.num_sequenza,
 			DT.cod_prodotto_figlio,
			DT.cod_versione_figlio,
			DT.cod_versione,
			DT.progressivo,
			DT.prog_calcolo,
			DT.quantita,
        		DT.misura,
			DT.flag_stampate,
			COMP.flag_azzera_dt
from distinte_taglio_calcolate as DT
join comp_det_ord_ven as COMP on COMP.cod_azienda=DT.cod_azienda and
													COMP.anno_registrazione=DT.anno_registrazione and
													COMP.num_registrazione=DT.num_registrazione and
													COMP.prog_riga_ord_ven=DT.prog_riga_ord_ven
where 	DT.cod_azienda=:s_cs_xx.cod_azienda and
			DT.anno_registrazione=:fl_anno_reg and
			DT.num_registrazione=:fl_num_reg and
			DT.prog_riga_ord_ven=:fl_prog_riga and
			DT.misura>0 and DT.quantita>0;

open cu_distinte_taglio;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in apertura cursore 'cu_distinte_taglio'~r~n" + sqlca.sqlerrtext
	return -1
end if

ll_index = 0
do while true

	fetch cu_distinte_taglio into 	:ls_cod_prodotto_padre,
											:ll_num_sequenza,
											:ls_cod_prodotto,
											:ls_cod_versione,
											:ls_cod_versione_padre,
											:ll_progressivo,
											:ll_prog_calcolo,
											:ld_quantita,
											:ld_misura,
											:ls_flag_stampate_cu,
											:ls_flag_azzera_dt_cu;
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in fetch cursore 'cu_distinte_taglio'~r~n" + sqlca.sqlerrtext
		close cu_distinte_taglio;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
		
		//verifica se hai chiesto di visualizzare solo quelle NON ancora stampate oppure quelle gia stampate
		if fs_flag_stampate<>ls_flag_stampate_cu then continue
		
		//verifica in base al filtro sulle colonne dinamiche -------------------------------------------------------------------
		ll_ret_col_din = wf_check_colonne_dinamiche(ls_cod_prodotto, ls_cod_colonna_dinamica, fs_errore)
		choose case ll_ret_col_din
			case is < 0
				//in fs_errore il messaggio
				close cu_distinte_taglio;
				return -1
				
			case 0
				//non soddifa il filtro delle colonne dinamiche dei prodotti, salta il prodotto
				continue
				
			case else
				//ok, continua
				//in ls_cod_colonna_dinamica il codice della colonna dinamica associata
				
		end choose
		//----------------------------------------------------------------------------------------------------------------------------
		
		
		//vado a recuperare l'unità di misura dalla tab_distinte_taglio
		select um_misura
		into :ls_cod_um
		from tab_distinte_taglio
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto_padre=:ls_cod_prodotto_padre and
					num_sequenza=:ll_num_sequenza and
					cod_prodotto_figlio=:ls_cod_prodotto and
					cod_versione_figlio=:ls_cod_versione and
					cod_versione=:ls_cod_versione_padre and
					progressivo=:ll_progressivo;

		if sqlca.sqlcode<0 then
			fs_errore = "Errore lettura da tab_distinte_taglio: "+sqlca.sqlerrtext
			close cu_distinte_taglio;
			return -1
		end if
		
		ll_index += 1
		
		fs_cod_prodotto_figlio[ll_index] = ls_cod_prodotto
		if isnull(ld_misura) then ld_misura=0
		if isnull(ls_cod_um) then ls_cod_um=""
		
		
		if ls_flag_azzera_dt_cu="S" then
			setnull(ld_misura)
		end if
		
		fd_misure[ll_index] = ld_misura
		fl_quantita[ll_index] = ld_quantita
		fs_um[ll_index] = ls_cod_um
		
		
		if wf_leggi_colonna_dinamica(ls_cod_prodotto, ls_cod_colonna_dinamica, ls_des_valore_col_din, fs_errore) < 0 then
			//in fs_errore il messaggio
			close cu_distinte_taglio;
			return -1
		end if
		fs_valore_colonna_dinamica[ll_index] = ls_des_valore_col_din
		
		fs_cod_prodotto_padre[ll_index] 	= ls_cod_prodotto_padre
		fl_num_sequenza[ll_index] 			= ll_num_sequenza
		fs_cod_prodotto_figlio[ll_index] 	= ls_cod_prodotto
		fs_cod_versione[ll_index]			= ls_cod_versione_padre
		fs_cod_versione_figlio[ll_index]	= ls_cod_versione
		fl_progressivo[ll_index]				= ll_progressivo
		fl_prog_calcolo[ll_index]				= ll_prog_calcolo
		
	end if

loop

close cu_distinte_taglio;

return 1
end function

public function long wf_assegna_posizione (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref datastore ads_data);long			ll_index, ll_max_pos

ll_max_pos = 0

for ll_index=1 to ads_data.rowcount()
	if 		ai_anno_ordine=ads_data.getitemnumber(ll_index, 2) and &
			al_num_ordine=ads_data.getitemnumber(ll_index, 3) and &
			al_riga_ordine=ads_data.getitemnumber(ll_index, 4) then
		
		//riga già presente nel datastore
		return ads_data.getitemnumber(ll_index, 1)
	end if
	
	if ads_data.getitemnumber(ll_index, 1) > ll_max_pos then ll_max_pos = ads_data.getitemnumber(ll_index, 1)

next

ll_max_pos += 1

//se arrivo fin qui la riga deve essere inserita nel datastore, con la sua posizione
ll_index = ads_data.insertrow(0)
ads_data.setitem(ll_index, 1, ll_max_pos)
ads_data.setitem(ll_index, 2, ai_anno_ordine)
ads_data.setitem(ll_index, 3, al_num_ordine)
ads_data.setitem(ll_index, 4, al_riga_ordine)

return ll_max_pos



end function

public function integer wf_crea_sessione (long al_progr_det_produzione, string as_cod_operaio, ref string as_errore);
string				ls_flag_fine_fase
long					ll_progr_sessione
uo_produzione	luo_prod
integer				li_risposta


select flag_fine_fase
into   :ls_flag_fine_fase
from   det_ordini_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr_det_produzione=:al_progr_det_produzione;
		
if sqlca.sqlcode < 0 then 
	as_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if
		
if sqlca.sqlcode = 100 then
	as_errore = "Attenzione! La produzione per questo ordine non è ancora stata lanciata. Verificare e rilanciare la produzione."
	return -1
end if
		
if ls_flag_fine_fase='S' then
	as_errore = "Attenzione! La fase risulta già chiusa, pertanto non è possibile iniziare nuove sessioni di lavoro."
	return -1
end if


setnull(ll_progr_sessione)

select count(*)
into   :ll_progr_sessione
from   sessioni_lavoro
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr_det_produzione=:al_progr_det_produzione;

if sqlca.sqlcode < 0 then 
	as_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if
		
if sqlca.sqlcode = 100 or isnull(ll_progr_sessione) or ll_progr_sessione=0 then
	ll_progr_sessione = 1
	
	luo_prod = create uo_produzione
	li_risposta = luo_prod.uof_nuova_sessione(al_progr_det_produzione, ll_progr_sessione, as_cod_operaio, as_errore)
	
	luo_prod.uof_mrp_disimpegna_ordine(al_progr_det_produzione, as_errore)
	
	destroy luo_prod
	
	if li_risposta < 0 then
		return -1
	end if

else
	//ci sono già sessioni
	return -1
end if


return 0
end function

public function boolean wf_check_operaio ();string			ls_cod_operaio

ls_cod_operaio = dw_ricerca.getitemstring(1, "cod_operaio")

if ls_cod_operaio="" or isnull(ls_cod_operaio) then
	//se rispondo SI al messaggio seguente (TRUE) vuol dire che voglio comunque proseguire ....
	return  g_mb.confirm(	"Non hai selezionato il codice operaio quindi NON sarà possibile effettuare l'eventuale apertura delle fasi reparto. "+&
									"Sicuro di voler procedere?")
end if

//se arrivi qui vuol dire che l'operaio esiste, e quindi continua pure
return true
end function

public function integer wf_post_stampa ();long			ll_index, ll_tot, ll_num_registrazione, ll_prog_riga_ord_ven, ll_num_sequenza, ll_prog, ll_prog_calcolo, ll_progr_det_produzione

integer		li_anno_registrazione, li_ret

string		ls_cod_prodotto_padre, ls_cod_prodotto_figlio, ls_cod_versione, ls_cod_versione_figlio, ls_flag_stampate, ls_cod_operaio, ls_errore


//reset del datastore
ids_data.reset()
dw_ricerca.setitem(1, "flag_da_barcode", "N")


ls_flag_stampate = dw_ricerca.getitemstring(1, "flag_stampate")
ls_cod_operaio = dw_ricerca.getitemstring(1, "cod_operaio")

//se non devi salvare lo stato stampate e non c'è il codice operaio esci subito
//in quanto non devi nè salvare lo stato stampate nè puoi aprire la sessione reparto
if ls_flag_stampate="S" and (ls_cod_operaio="" or not isnull(ls_cod_operaio)) then return 0


ll_tot = dw_report.rowcount()

for ll_index=1 to ll_tot
	
	//---------------------------------------------------------------------------------------------------------------------------------------------
	if ls_flag_stampate<>"S" then
	
		li_anno_registrazione			= dw_report.getitemnumber(ll_index, "anno_registrazione")
		ll_num_registrazione			= dw_report.getitemnumber(ll_index, "num_registrazione")
		ll_prog_riga_ord_ven			= dw_report.getitemnumber(ll_index, "prog_riga_ord_ven")
		
		ls_cod_prodotto_padre		= dw_report.getitemstring(ll_index, "cod_prodotto_padre")
		ll_num_sequenza				= dw_report.getitemnumber(ll_index, "num_sequenza")
		ls_cod_prodotto_figlio			= dw_report.getitemstring(ll_index, "cod_prodotto_figlio")
		ls_cod_versione					= dw_report.getitemstring(ll_index, "cod_versione")
		ls_cod_versione_figlio			= dw_report.getitemstring(ll_index, "cod_versione_figlio")
		ll_prog								= dw_report.getitemnumber(ll_index, "prog")
		ll_prog_calcolo					= dw_report.getitemnumber(ll_index, "prog_calcolo")
		
		pcca.mdi_frame.setmicrohelp(string((ll_index / ll_tot) * 100, "##0") + " % Aggiornamento flag stampata su distinta taglio  in corso ...")
	
		update distinte_taglio_calcolate
		set 	flag_stampate='S'
		where 	cod_azienda				= :s_cs_xx.cod_azienda and
					anno_registrazione		= :li_anno_registrazione and
					num_registrazione		= :ll_num_registrazione and
					prog_riga_ord_ven		= :ll_prog_riga_ord_ven and
					cod_prodotto_padre	= :ls_cod_prodotto_padre and
					num_sequenza			= :ll_num_sequenza and
					cod_prodotto_figlio		= :ls_cod_prodotto_figlio and
					cod_versione				= :ls_cod_versione and
					cod_versione_figlio		= :ls_cod_versione_figlio and
					progressivo				= :ll_prog and
					prog_calcolo				= :ll_prog_calcolo;
		
		//faccio un commit per ogni update, che forse è meglio
		if sqlca.sqlcode=0 then
			commit;
		else
			rollback;
			return -1
		end if
		
	end if
	//---------------------------------------------------------------------------------------------------------------------------------------------

	//---------------------------------------------------------------------------------------------------------------------------------------------
	if ls_cod_operaio<>"" and not isnull(ls_cod_operaio) then
		//se non ci sono sessioni aperte apro sessione reparto
		ll_progr_det_produzione		= dw_report.getitemnumber(ll_index, "progr_det_produzione")

		pcca.mdi_frame.setmicrohelp(	string((ll_index / ll_tot) * 100, "##0") + &
													" % Controllo e Apertura sessione per reparto con Barcode "+string(ll_progr_det_produzione)+"  in corso ...")

		li_ret = wf_crea_sessione(ll_progr_det_produzione, ls_cod_operaio, ls_errore)
		if li_ret<0 then
			rollback;
		else
			commit;
		end if
	end if
	//---------------------------------------------------------------------------------------------------------------------------------------------
	
next


return 0











end function

public subroutine wf_carica_sql_barcode (ref long al_barcode[], string as_dtaglio[], ref string as_cod_reparto);
long				ll_index, ll_tot

ll_tot = ids_data.rowcount()


for ll_index=1 to ll_tot
	
	if ll_index=1 then
		as_cod_reparto = ids_data.getitemstring(ll_index, "cod_reparto")
	end if
	
	al_barcode[ll_index] = ids_data.getitemnumber(ll_index, "progr_det_produzione")	
	as_dtaglio[ll_index] = ids_data.getitemstring(ll_index, "dtaglio")
	
next

return
end subroutine

public function long wf_verifica_distinte_taglio (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine);long		ll_count


select count(*)
into :ll_count
from distinte_taglio_calcolate
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_ordine and
			num_registrazione=:al_num_ordine and
			prog_riga_ord_ven=:al_riga_ordine and
			misura>0 and quantita>0;

if isnull(ll_count) then ll_count=0

return ll_count

end function

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[], lw_vuoto[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.ib_dw_report=true
dw_report_2.ib_dw_report=true
//set_w_options(c_noenablepopup)

dw_ricerca.set_dw_options(		sqlca, &
												pcca.null_object,&
												c_newonopen, &
												c_default)

dw_lista.set_dw_options(			sqlca, &
												pcca.null_object, &
												c_nonew + &
												c_nomodify + &
												c_nodelete + &
												c_noenablenewonopen + &
												c_noenablemodifyonopen + &
												c_scrollparent + &
												c_disablecc, &
												c_noresizedw + &
												c_nohighlightselected + &
												c_nocursorrowfocusrect + &
												c_nocursorrowpointer)
											
dw_report.set_dw_options(		sqlca, &
												pcca.null_object, &
												c_nonew + &
												c_nomodify + &
												c_nodelete + &
												c_noenablenewonopen + &
												c_noenablemodifyonopen + &
												c_scrollparent + &
												c_disablecc, &
												c_noresizedw + &
												c_nohighlightselected + &
												c_nocursorrowfocusrect + &
												c_nocursorrowpointer)
												
dw_report_2.set_dw_options(		sqlca, &
												pcca.null_object, &
												c_nonew + &
												c_nomodify + &
												c_nodelete + &
												c_noenablenewonopen + &
												c_noenablemodifyonopen + &
												c_scrollparent + &
												c_disablecc, &
												c_noresizedw + &
												c_nohighlightselected + &
												c_nocursorrowfocusrect + &
												c_nocursorrowpointer)

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_colonne_dinamiche
lw_oggetti[2] = hpb_1
lw_oggetti[3] = dw_ricerca
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_lista
dw_folder.fu_assigntab(2, "Lista", lw_oggetti[])

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_report
lw_oggetti[2] = cb_stampa
dw_folder.fu_assigntab(3, "Etichette", lw_oggetti[])

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_report_2
lw_oggetti[2] = cb_stampa_2
dw_folder.fu_assigntab(4, "Report A4", lw_oggetti[])

dw_folder.fu_foldercreate(4, 4)
dw_folder.fu_selecttab(1)

//creo la struttura del datastore
wf_create_ds()


//gestione supervisore ---------------------------------------------
if s_cs_xx.cod_utente<>"CS_SYSTEM" then
	select flag_supervisore
	into :is_flag_supervisore
	from utenti
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente;
			
	if is_flag_supervisore<>"S" then is_flag_supervisore="N"
else
	is_flag_supervisore = "S"
end if

postevent("ue_stabilimento_operatore")

dw_colonne_dinamiche.object.p_add.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\add.png"
dw_colonne_dinamiche.object.p_del.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\delete.png"

dw_report_2.object.datawindow.print.preview = "yes"

dw_ricerca.postevent("ue_set_operaio")





end event

on w_report_taglio_profili.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.dw_folder=create dw_folder
this.dw_report_2=create dw_report_2
this.dw_lista=create dw_lista
this.dw_ricerca=create dw_ricerca
this.dw_colonne_dinamiche=create dw_colonne_dinamiche
this.hpb_1=create hpb_1
this.cb_stampa=create cb_stampa
this.cb_stampa_2=create cb_stampa_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.dw_folder
this.Control[iCurrent+3]=this.dw_report_2
this.Control[iCurrent+4]=this.dw_lista
this.Control[iCurrent+5]=this.dw_ricerca
this.Control[iCurrent+6]=this.dw_colonne_dinamiche
this.Control[iCurrent+7]=this.hpb_1
this.Control[iCurrent+8]=this.cb_stampa
this.Control[iCurrent+9]=this.cb_stampa_2
end on

on w_report_taglio_profili.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.dw_folder)
destroy(this.dw_report_2)
destroy(this.dw_lista)
destroy(this.dw_ricerca)
destroy(this.dw_colonne_dinamiche)
destroy(this.hpb_1)
destroy(this.cb_stampa)
destroy(this.cb_stampa_2)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ricerca, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

//caricare solo le colonne dinamiche di tipo lista e presenti in tabella chievi dei prodotti
f_po_loaddddw_dw(	dw_colonne_dinamiche, &
							"cod_colonna_dinamica", &
							sqlca, &
							"tab_colonne_dinamiche", &
							"cod_colonna_dinamica", &
							"des_colonna_dinamica", &
							"cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_colonna='L' and "+ &
							"cod_colonna_dinamica in (select cod_colonna_dinamica "+&
																"from anag_prodotti_col_din "+&
																"where cod_azienda='"+s_cs_xx.cod_azienda+"' )")
																
end event

event close;call super::close;

destroy ids_data
end event

type dw_report from uo_cs_xx_dw within w_report_taglio_profili
integer x = 69
integer y = 240
integer width = 3749
integer height = 2312
integer taborder = 40
string dataobject = "d_label_taglio_profili"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event printend;call super::printend;

wf_post_stampa()

pcca.mdi_frame.setmicrohelp("Finito!")

end event

type dw_folder from u_folder within w_report_taglio_profili
integer x = 27
integer y = 20
integer width = 3822
integer height = 2576
integer taborder = 10
boolean border = false
end type

type dw_report_2 from uo_cs_xx_dw within w_report_taglio_profili
integer x = 69
integer y = 240
integer width = 3749
integer height = 2312
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_report_taglio_profili"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event printend;call super::printend;

wf_post_stampa()

pcca.mdi_frame.setmicrohelp("Finito!")
end event

type dw_lista from uo_cs_xx_dw within w_report_taglio_profili
integer x = 69
integer y = 136
integer width = 3689
integer height = 2412
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_taglio_profili_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "b_stampa"
		if rowcount()>0 then
		else
			g_mb.warning("Apice", "nessuna riga da stampare!")
			return
		end if
		
		this.print( true, true)
		
end choose
end event

type dw_ricerca from uo_cs_xx_dw within w_report_taglio_profili
event ue_set_operaio ( )
integer x = 64
integer y = 780
integer width = 3026
integer height = 1572
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_taglio_profili_sel"
end type

event ue_set_operaio();string			ls_cod_operaio


select cod_operaio
into :ls_cod_operaio
from anag_operai
where	cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente;

if ls_cod_operaio<>"" and not isnull(ls_cod_operaio) then
	dw_ricerca.setitem(1, "cod_operaio", ls_cod_operaio)
end if
end event

event buttonclicked;call super::buttonclicked;string			ls_errore
long				ll_return

if row>0 then
else
	return
end if

choose case dwo.name
	case "b_annulla"
		wf_annulla()
		
	case "b_cerca"
		hpb_1.position = 0
		ll_return = wf_ricerca(ls_errore)
		hpb_1.position = 0
		
		if ll_return<0 then
			g_mb.error(ls_errore)
			dw_ricerca.Modify("log_t.text='Elaborazione terminata con errori!'")
			rollback;
			
			return
		else
			dw_ricerca.Modify("log_t.text='Pronto!'")
		end if
	
	
	case "b_barcode"
		wf_da_barcode()
		
		
	case "b_reset_barcode"
		if ids_data.rowcount()>0 then
			
			if g_mb.confirm("Vuoi resettare il buffer dei codici barcodes scansionati?") then
				ids_data.reset()
				dw_ricerca.setitem(1, "flag_da_barcode", "N")
			end if
			
		end if
		
end choose
end event

event itemchanged;call super::itemchanged;datetime			ldt_null
string				ls_null

if row>0 then
else
	return
end if

setnull(ldt_null)
setnull(ls_null)

choose case dwo.name	
	case "data_pronto_da"
		if isnull(data) then
			//allora pulisci anche l'altro
			setitem(1, "data_pronto_a", ldt_null)
		end if
	
	case "data_pronto_a"
		if isnull(data) then
			//allora pulisci anche l'altro
			setitem(1, "data_pronto_da", ldt_null)
		end if
	
	case "cod_deposito"
		if data<>"" then
			f_po_loaddddw_dw(dw_ricerca, &
						  "cod_reparto", &
						  sqlca, &
						  "anag_reparti", &
						  "cod_reparto", &
						  "des_reparto", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
						  "cod_deposito = '"+data+"' ")
			
			//pulisco il campo reparto
			setitem(1, "cod_reparto", ls_null)
	
		else
			f_po_loaddddw_dw(dw_ricerca, &
						  "cod_reparto", &
						  sqlca, &
						  "anag_reparti", &
						  "cod_reparto", &
						  "des_reparto", &
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		end if

end choose
end event

type dw_colonne_dinamiche from uo_cs_xx_dw within w_report_taglio_profili
integer x = 64
integer y = 136
integer width = 3026
integer height = 624
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_taglio_profili_col_din_sel"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event clicked;call super::clicked;
choose case dwo.name
	case "p_add"
		insertrow(0)
		
	case "p_del"
		if row>0 then
			deleterow(row)
		else
			return
		end if

end choose
end event

event itemchanged;call super::itemchanged;//
//
//if row > 0 then
//else
//	return
//end if
//
//choose case dwo.name
//	case "cod_colonna_dinamica"
//		wf_filtra_valori(data)
//		
//end choose
//
end event

type hpb_1 from hprogressbar within w_report_taglio_profili
integer x = 64
integer y = 2364
integer width = 3026
integer height = 68
boolean bringtotop = true
unsignedinteger maxposition = 100
integer setstep = 1
end type

type cb_stampa from commandbutton within w_report_taglio_profili
integer x = 3387
integer y = 136
integer width = 402
integer height = 84
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;
if not wf_check_operaio() then return

dw_report.print( true, true)
end event

type cb_stampa_2 from commandbutton within w_report_taglio_profili
integer x = 3346
integer y = 136
integer width = 402
integer height = 84
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;
if not wf_check_operaio() then return

dw_report_2.print( true, true)
end event

