﻿$PBExportHeader$w_report_taglio_profili_barcode.srw
forward
global type w_report_taglio_profili_barcode from window
end type
type cb_annulla from commandbutton within w_report_taglio_profili_barcode
end type
type cb_reset from commandbutton within w_report_taglio_profili_barcode
end type
type cb_fine from commandbutton within w_report_taglio_profili_barcode
end type
type st_messaggio from statictext within w_report_taglio_profili_barcode
end type
type dw_lista from datawindow within w_report_taglio_profili_barcode
end type
type dw_buffer from datawindow within w_report_taglio_profili_barcode
end type
end forward

global type w_report_taglio_profili_barcode from window
integer width = 2633
integer height = 1956
boolean titlebar = true
string title = "Lettura codici da stampare"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cb_annulla cb_annulla
cb_reset cb_reset
cb_fine cb_fine
st_messaggio st_messaggio
dw_lista dw_lista
dw_buffer dw_buffer
end type
global w_report_taglio_profili_barcode w_report_taglio_profili_barcode

type variables
datastore			ids_data
end variables

forward prototypes
public subroutine wf_reset_dw_buffer ()
public function long wf_verifica_se_presente (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine)
public function long wf_verifica_dtaglio (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine)
end prototypes

public subroutine wf_reset_dw_buffer ();

dw_buffer.reset()
dw_buffer.insertrow(0)
dw_buffer.setfocus()

return
end subroutine

public function long wf_verifica_se_presente (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine);//torna il numero riga in cui la riga è presente, altrimenti torn a ZERO

long			ll_index, ll_numero, ll_riga
integer		li_anno
string		ls_cod_reparto


for ll_index=1 to dw_lista.rowcount()
	li_anno = dw_lista.getitemnumber(ll_index, "anno_registrazione")
	ll_numero = dw_lista.getitemnumber(ll_index, "num_registrazione")
	ll_riga = dw_lista.getitemnumber(ll_index, "prog_riga_ord_ven")

	if li_anno=ai_anno_ordine and ll_numero=al_num_ordine and ll_riga=al_riga_ordine then
		return ll_index
	end if

next

//se arrivi fin qui vuol dire che non è presente in lista
return 0


end function

public function long wf_verifica_dtaglio (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine);long		ll_count


select count(*)
into :ll_count
from distinte_taglio_calcolate
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_ordine and
			num_registrazione=:al_num_ordine and
			prog_riga_ord_ven=:al_riga_ordine and
			misura>0 and quantita>0;

if isnull(ll_count) then ll_count=0

return ll_count

end function

on w_report_taglio_profili_barcode.create
this.cb_annulla=create cb_annulla
this.cb_reset=create cb_reset
this.cb_fine=create cb_fine
this.st_messaggio=create st_messaggio
this.dw_lista=create dw_lista
this.dw_buffer=create dw_buffer
this.Control[]={this.cb_annulla,&
this.cb_reset,&
this.cb_fine,&
this.st_messaggio,&
this.dw_lista,&
this.dw_buffer}
end on

on w_report_taglio_profili_barcode.destroy
destroy(this.cb_annulla)
destroy(this.cb_reset)
destroy(this.cb_fine)
destroy(this.st_messaggio)
destroy(this.dw_lista)
destroy(this.dw_buffer)
end on

event open;
s_cs_xx_parametri			lstr_parametri
long								ll_index, ll_new


lstr_parametri = message.powerobjectparm

ids_data = lstr_parametri.parametro_ds_1

ids_data.rowscopy(1, ids_data.rowcount(), Primary!, dw_lista, 1 , Primary!)



wf_reset_dw_buffer()
end event

type cb_annulla from commandbutton within w_report_taglio_profili_barcode
integer x = 2194
integer y = 28
integer width = 402
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;
s_cs_xx_parametri			lstr_parametri

lstr_parametri.parametro_b_1 = false

//poi lascia il datastore intatto
lstr_parametri.parametro_ds_1 = ids_data

closewithreturn(parent, lstr_parametri)
end event

type cb_reset from commandbutton within w_report_taglio_profili_barcode
integer x = 1134
integer y = 28
integer width = 402
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Reset"
end type

event clicked;

if g_mb.confirm( "Sicuro di voler effettuare il rest della lista dei codici barcode scansionati finora (anche quelli precedentemente impostati)?") then
	dw_lista.reset()
	ids_data.reset()
end if
end event

type cb_fine from commandbutton within w_report_taglio_profili_barcode
integer x = 722
integer y = 28
integer width = 402
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Finito"
end type

event clicked;
s_cs_xx_parametri			lstr_parametri



//reset del datastore
ids_data.reset()


//se la dw ha righe popolo il datastore, altrimenti tornerà vuoto
if dw_lista.rowcount()>0 then

	dw_lista.rowscopy(1, dw_lista.rowcount(), Primary!, ids_data, 1 , Primary!)

	//torno con il datastore popolato
	lstr_parametri.parametro_b_1 = true
	lstr_parametri.parametro_ds_1 = ids_data

else
	lstr_parametri.parametro_b_1 = false
end if

closewithreturn(parent, lstr_parametri)



end event

type st_messaggio from statictext within w_report_taglio_profili_barcode
integer x = 37
integer y = 160
integer width = 2537
integer height = 164
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean focusrectangle = false
end type

type dw_lista from datawindow within w_report_taglio_profili_barcode
integer x = 27
integer y = 360
integer width = 2555
integer height = 1464
integer taborder = 20
string title = "none"
string dataobject = "d_taglio_profili_sel_barcode"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event rowfocuschanged;

if currentrow>0 then
	selectrow(0, false)
	selectrow(currentrow, true)
end if
end event

type dw_buffer from datawindow within w_report_taglio_profili_barcode
integer x = 27
integer y = 36
integer width = 635
integer height = 100
integer taborder = 10
string title = "none"
string dataobject = "d_buffer"
boolean border = false
boolean livescroll = true
end type

event itemchanged;string						ls_lettura, ls_msg, ls_cod_reparto, ls_flag_distinte_taglio
long						ll_barcode, ll_num_ordine, ll_riga_ordine, ll_new
integer					li_anno_ordine, li_ret
uo_produzione			luo_prod


choose case dwo.name
	//#####################################################################################
	case "buffer"
		
		st_messaggio.text = ""
		
		if data="" or isnull(data) then
			st_messaggio.text = "Il dato deve essere numerico!"
			wf_reset_dw_buffer()
			return
		end if
			
		ls_lettura = data
		if f_converti_barcode(ls_lettura, ls_msg) < 0 then
			st_messaggio.text = ls_msg
			wf_reset_dw_buffer()
			return
		end if			
		
		if not isnumber(data) then
			st_messaggio.text = "Il dato deve essere numerico!"
			wf_reset_dw_buffer()
			return
		end if
		
		
		ll_barcode = long (ls_lettura)
		
		//verifica che il barcode esista --------------------------------------------------------------------
		setnull(ll_riga_ordine)
		
		luo_prod = create uo_produzione
		li_ret = luo_prod.uof_barcode_anno_reg(ll_barcode, li_anno_ordine, ll_num_ordine, ll_riga_ordine, ls_cod_reparto, ls_msg)
		destroy luo_prod
		
		if li_ret<0 then
			st_messaggio.text = "Codice a barre " + string(ll_barcode) + " : " + ls_msg
			wf_reset_dw_buffer()
			return
		end if
		
		if ll_riga_ordine>0 then
		else
			st_messaggio.text = 	"Il codice a barre " + string(ll_barcode) + " è stato trovato ma sembra non si riferisca "+&
										"ad alcun ordine di tipo A (L'ordine risulta "+string(li_anno_ordine)+"/"+string(ll_num_ordine)+" ) "
			wf_reset_dw_buffer()
			return
		end if
		
		//verifica caso mai hai già scansionato ----------------------------------------------------------
		li_ret = wf_verifica_se_presente(li_anno_ordine, ll_num_ordine, ll_riga_ordine)
		if li_ret>0 then
			st_messaggio.text = 	"Il codice a barre " + string(ll_barcode) + " è già presente in lista e si riferisce alla riga ordine " + &
										string(li_anno_ordine)+"/"+string(ll_num_ordine)+"/"+string(ll_riga_ordine)
			
			dw_lista.scrolltorow(li_ret)
			
			wf_reset_dw_buffer()
			return
		end if
		
		//verifica su reparto
		if dw_lista.rowcount()>0 then
			if ls_cod_reparto<>dw_lista.getitemstring(1, "cod_reparto") then
				st_messaggio.text = 	"Il codice a barre " + string(ll_barcode) + " si riferisce ad un reparto ("+ls_cod_reparto+") che risulta diverso "+&
												"da quello già selezionato dai barcodes già presenti in questa lista ("+dw_lista.getitemstring(1, "cod_reparto")+")"
			
			dw_lista.scrolltorow(li_ret)
			
			wf_reset_dw_buffer()
			return
			end if
		end if
		
		//controllo che esistano distinte tagli calcolate per la riga ordine del codice a barre
		ls_flag_distinte_taglio = "S"
		if wf_verifica_dtaglio(li_anno_ordine, ll_num_ordine, ll_riga_ordine) = 0 then
			ls_flag_distinte_taglio = "N"

			//dai un alert a video
			st_messaggio.text = 	"La riga ordine " + string(li_anno_ordine)+"/"+string(ll_num_ordine)+"/"+string(ll_riga_ordine) + &
										" relativa alla codice a barre " + string(ll_barcode) + &
										" non ha nessuna distinta di taglio calcolata!"
		end if

		//se arrivi fin qui è tutto OK
		ll_new = dw_lista.insertrow(0)
		dw_lista.setitem(ll_new, "progr_det_produzione", ll_barcode)
		dw_lista.setitem(ll_new, "anno_registrazione", li_anno_ordine)
		dw_lista.setitem(ll_new, "num_registrazione", ll_num_ordine)
		dw_lista.setitem(ll_new, "prog_riga_ord_ven", ll_riga_ordine)
		dw_lista.setitem(ll_new, "cod_reparto", ls_cod_reparto)
		dw_lista.setitem(ll_new, "dtaglio", ls_flag_distinte_taglio)

		dw_lista.scrolltorow(ll_new)

		wf_reset_dw_buffer()
		//#####################################################################################

end choose
end event

