﻿$PBExportHeader$w_tes_ord_ven_reparti.srw
forward
global type w_tes_ord_ven_reparti from w_cs_xx_risposta
end type
type cb_reparti from commandbutton within w_tes_ord_ven_reparti
end type
type dw_tes_ord_ven_reparti from uo_cs_xx_dw within w_tes_ord_ven_reparti
end type
type cb_modifica from commandbutton within w_tes_ord_ven_reparti
end type
type cb_salva from commandbutton within w_tes_ord_ven_reparti
end type
end forward

global type w_tes_ord_ven_reparti from w_cs_xx_risposta
integer width = 3072
integer height = 1324
string title = "Gestione del Cambio Reparto"
cb_reparti cb_reparti
dw_tes_ord_ven_reparti dw_tes_ord_ven_reparti
cb_modifica cb_modifica
cb_salva cb_salva
end type
global w_tes_ord_ven_reparti w_tes_ord_ven_reparti

event pc_setwindow;call super::pc_setwindow;dw_tes_ord_ven_reparti.set_dw_key("cod_azienda")
dw_tes_ord_ven_reparti.set_dw_key("anno_registrazione")
dw_tes_ord_ven_reparti.set_dw_key("num_registrazione")

dw_tes_ord_ven_reparti.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_default, &
                                    c_default + &
                                    c_nohighlightselected + c_ViewModeBorderUnchanged + c_NoCursorRowFocusRect)


end event

on w_tes_ord_ven_reparti.create
int iCurrent
call super::create
this.cb_reparti=create cb_reparti
this.dw_tes_ord_ven_reparti=create dw_tes_ord_ven_reparti
this.cb_modifica=create cb_modifica
this.cb_salva=create cb_salva
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_reparti
this.Control[iCurrent+2]=this.dw_tes_ord_ven_reparti
this.Control[iCurrent+3]=this.cb_modifica
this.Control[iCurrent+4]=this.cb_salva
end on

on w_tes_ord_ven_reparti.destroy
call super::destroy
destroy(this.cb_reparti)
destroy(this.dw_tes_ord_ven_reparti)
destroy(this.cb_modifica)
destroy(this.cb_salva)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tes_ord_ven_reparti, &
                 "cod_reparto_sostituito", &
                 sqlca, &
                 "anag_reparti", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_reparto = 'E' ")

f_po_loaddddw_dw(dw_tes_ord_ven_reparti, &
                 "cod_reparto_sostituente", &
                 sqlca, &
                 "anag_reparti", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_reparto = 'E' ")

end event

type cb_reparti from commandbutton within w_tes_ord_ven_reparti
integer x = 2633
integer y = 1124
integer width = 389
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Carica Rep."
end type

event clicked;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_cont,ll_prog_riga_ord_ven, ll_ret
string ls_tipo_trasmissione, ls_cod_reparto, ls_email, ls_messaggio,ls_flag_tipo_riga
uo_passaggio_ordini luo_passaggio_ordini

ll_anno_registrazione = dw_tes_ord_ven_reparti.i_parentdw.getitemnumber(dw_tes_ord_ven_reparti.i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_reparti.i_parentdw.getitemnumber(dw_tes_ord_ven_reparti.i_parentdw.i_selectedrows[1], "num_registrazione")

select count(*)
into   :ll_cont
from   tes_ord_ven_reparti
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_registrazione AND
       num_registrazione = :ll_num_registrazione;

if ll_cont > 0 then
	if g_mb.messagebox("APICE","Attenzione: vi sono già dei reparti caricati nella maschera; ricarico i reparti esterni estraendoli dalle righe d'ordine?",Question!,yesno!,2) = 2 then return
end if


luo_passaggio_ordini = CREATE uo_passaggio_ordini

 DECLARE cu_prog_riga_ord_ven CURSOR FOR  
  SELECT det_ord_ven.prog_riga_ord_ven  
    FROM det_ord_ven  
   WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
         anno_registrazione = :ll_anno_registrazione AND  
         num_registrazione = :ll_num_registrazione and
			( num_riga_appartenenza = 0 or num_riga_appartenenza is null)
ORDER BY det_ord_ven.prog_riga_ord_ven ASC  ;

open cu_prog_riga_ord_ven;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore durante la open del cursore di lettura delle righe ordine.~r~nDettaglio: " + sqlca.sqlerrtext,Stopsign!)
	rollback;
	return
end if

do while true
	fetch cu_prog_riga_ord_ven into :ll_prog_riga_ord_ven;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore durante la fetch del cursore di lettura delle righe ordine.~r~nDettaglio: " + sqlca.sqlerrtext,Stopsign!)
		rollback;
		return
	end if
	
	ll_ret = luo_passaggio_ordini.uof_get_rep_det_ord_ven(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ref ls_flag_tipo_riga, ref ls_tipo_trasmissione, ref ls_cod_reparto, ref ls_email, ref ls_messaggio)
	if ll_ret < 0 then
		// errore
		g_mb.messagebox("APICE", "Errore in verifica reparto riga ~n" + ls_messaggio)
		return -1
	elseif ll_ret = 100 then
		// la riga non è collegata ad alcun reparto.
		continue
	end if
	
	INSERT INTO tes_ord_ven_reparti  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  cod_reparto_sostituito,   
			  cod_reparto_sostituente )  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,   
			  :ls_cod_reparto,   
			  null )  ;
loop

destroy luo_passaggio_ordini
close cu_prog_riga_ord_ven;
commit;

dw_tes_ord_ven_reparti.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type dw_tes_ord_ven_reparti from uo_cs_xx_dw within w_tes_ord_ven_reparti
integer x = 9
integer y = 12
integer width = 3013
integer height = 1092
integer taborder = 10
string dataobject = "d_tes_ord_ven_reparti"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_anno_registrazione, ll_num_registrazione


ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")


ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if
next

end event

event pcd_modify;call super::pcd_modify;cb_modifica.visible = false
cb_salva.visible = true
cb_reparti.enabled = false
end event

event pcd_view;call super::pcd_view;cb_modifica.visible = true
cb_salva.visible = false
cb_reparti.enabled = true
end event

type cb_modifica from commandbutton within w_tes_ord_ven_reparti
integer x = 23
integer y = 1120
integer width = 389
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Modifica Rep."
end type

event clicked;parent.triggerevent("pc_modify")
end event

type cb_salva from commandbutton within w_tes_ord_ven_reparti
integer x = 23
integer y = 1120
integer width = 389
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva Mod."
end type

event clicked;parent.triggerevent("pc_save")
end event

