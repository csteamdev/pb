﻿$PBExportHeader$w_tes_ord_ven_tv.srw
$PBExportComments$Finestra Gestione Testata Ordini di Vendita
forward
global type w_tes_ord_ven_tv from w_cs_xx_principale
end type
type dw_documenti from uo_dw_drag_doc_acq_ven within w_tes_ord_ven_tv
end type
type cb_grafico from commandbutton within w_tes_ord_ven_tv
end type
type dw_folder from u_folder within w_tes_ord_ven_tv
end type
type dw_tes_ord_ven_det_4 from uo_cs_xx_dw within w_tes_ord_ven_tv
end type
type dw_tes_ord_ven_det_2 from uo_cs_xx_dw within w_tes_ord_ven_tv
end type
type cb_duplica_doc from commandbutton within w_tes_ord_ven_tv
end type
type dw_tes_off_ven from datawindow within w_tes_ord_ven_tv
end type
type dw_tes_ord_ven_det_3 from uo_cs_xx_dw within w_tes_ord_ven_tv
end type
type tv_1 from treeview within w_tes_ord_ven_tv
end type
type dw_tes_ord_ven_det_1 from uo_cs_xx_dw within w_tes_ord_ven_tv
end type
type uo_stato_prod from uo_stato_produzione within w_tes_ord_ven_tv
end type
type dw_tes_ord_ven_ricerca from uo_std_dw within w_tes_ord_ven_tv
end type
type dw_folder_tv from u_folder within w_tes_ord_ven_tv
end type
end forward

global type w_tes_ord_ven_tv from w_cs_xx_principale
integer width = 4562
integer height = 3012
string title = "Ordini Clienti"
event ue_post_configuratore ( )
event ue_riapri_configuratore ( )
event ue_assegnazione ( )
event ue_azzera ( )
event ue_blocca ( )
event ue_calcola ( )
event ue_cambio_reparti ( )
event ue_chiudi_commessa ( )
event ue_configuratore ( )
event ue_corrispondenze ( )
event ue_dettaglio ( )
event ue_genera_bolla ( )
event ue_evas_parziale ( )
event ue_proforma ( )
event ue_note ( )
event ue_salda ( )
event ue_sblocca ( )
event ue_stampa ( )
event ue_aggiungi_conferma_ordine ( )
event ue_stampa_conferma_ordine ( )
event ue_reset_conferma_ordine ( )
event ue_cambia_data_partenza ( )
event ue_unisci_doc ( )
event ue_menu_azzera_spese_trasporto ( )
event ue_menu_abilita_spese_trasporto ( )
event ue_visualizza_spese_trasporto ( )
event ue_stampa_rapida_conferma ( )
event ue_documenti_fiscali ( )
event ue_importa_excel ( )
event ue_marginalita ( )
event ue_annulla_evasione_alusistemi ( )
dw_documenti dw_documenti
cb_grafico cb_grafico
dw_folder dw_folder
dw_tes_ord_ven_det_4 dw_tes_ord_ven_det_4
dw_tes_ord_ven_det_2 dw_tes_ord_ven_det_2
cb_duplica_doc cb_duplica_doc
dw_tes_off_ven dw_tes_off_ven
dw_tes_ord_ven_det_3 dw_tes_ord_ven_det_3
tv_1 tv_1
dw_tes_ord_ven_det_1 dw_tes_ord_ven_det_1
uo_stato_prod uo_stato_prod
dw_tes_ord_ven_ricerca dw_tes_ord_ven_ricerca
dw_folder_tv dw_folder_tv
end type
global w_tes_ord_ven_tv w_tes_ord_ven_tv

type variables
boolean 					ib_esiste_padre=TRUE, ib_new
long 						il_anno_registrazione, il_num_registrazione, il_num_row = 1
string 					is_sql_filtro=""
string						is_join_det_ord_ven = " join det_ord_ven on det_ord_ven.cod_azienda=tes_ord_ven.cod_azienda and "+&
																					"det_ord_ven.anno_registrazione=tes_ord_ven.anno_registrazione and "+&
																					"det_ord_ven.num_registrazione=tes_ord_ven.num_registrazione "

uo_produzione 			iuo_produzione

integer 					ii_protect_autorizza_sblocco = 1 //cioèprotect

boolean 					ib_tree_deleting = false, ib_retrieve=true, ib_nuova_data_consegna, ib_supervisore = false
long 						il_livello_corrente, il_numero_riga, il_handle, il_handle_modificato, il_handle_cancellato

//								Aggiunto per conferma ordine
long							il_anno_conferma[], il_num_conferma[]

// spese trasporto
uo_spese_trasporto iuo_spese_trasporto

uo_fido_cliente			iuo_fido_cliente
string						is_cod_parametro_blocco = "IOC"
end variables

forward prototypes
public function boolean wf_controlla_commesse_aperte (long fl_anno_ordine, long fl_num_ordine, ref string fs_msg)
public subroutine wf_cancella_treeview ()
public subroutine wf_imposta_treeview ()
public subroutine wf_ricerca ()
public subroutine wf_annulla ()
public function integer wf_inserisci_singolo_ordine (long fl_handle, long fl_anno, long fl_numero)
public function integer wf_leggi_parent (long fl_handle, ref string fs_sql)
public subroutine wf_imposta_immagini_tv ()
public function integer wf_inserisci_depositi (long fl_handle)
public function integer wf_inserisci_anni (long fl_handle)
public function integer wf_inserisci_clienti (long fl_handle)
public function integer wf_inserisci_data_consegna (long fl_handle)
public function integer wf_inserisci_data_registrazione (long fl_handle)
public function integer wf_inserisci_operatori (long fl_handle)
public function integer wf_inserisci_ordini (long fl_handle)
public function integer wf_inserisci_tipi_ordini (long fl_handle)
public function integer wf_inserisci_prodotti (long fl_handle)
public subroutine wf_memorizza_filtro ()
public subroutine wf_leggi_filtro ()
public subroutine wf_predefinito ()
public subroutine wf_imposta_predefinito ()
public function boolean wf_liv_prodotto_is_present ()
public function integer wf_retrieve_offerte_vendita (long fl_anno_reg_ordine, long fl_num_reg_ordine)
public function integer wf_apri_offerta (long fl_anno_offerta, long fl_num_offerta)
public function longlong wf_get_prog_sessione ()
public function integer wf_riapri_offerta (integer fi_anno_reg_ord_ven, long fl_num_reg_ord_ven, ref string fs_errore)
public function boolean wf_conta_ologrammi (integer fi_anno_ord_ven, long fl_num_ord_ven, ref integer fi_anno_ordini[], ref long fl_numero_ordini[], ref string fs_evasione[])
public subroutine wf_inserisci_ordini_ologramma (long fl_handle, integer fi_anno, long fl_numero, integer fi_anno_ordine_olo[], long fl_num_ordine_olo[], string fs_evasione_olo[])
public subroutine wf_inserisci_singolo_ordine_ologramma (string fs_ologramma)
public function integer wf_cambia_data_partenza ()
public subroutine wf_supervisore ()
public function integer wf_controlla_pianificazione (integer ai_anno_commessa, long al_num_commessa, string as_cod_semilavorato, ref string as_errore)
public function integer wf_logo_etichette ()
end prototypes

event ue_post_configuratore();string						ls_messaggio
long						ll_num_ordine, ll_ret
datetime					ldt_nuova_data_consegna
integer					li_anno_ordine


ll_ret = dw_tes_ord_ven_det_1.getrow()

//integrazione gestione cambio data consegna (semaforo produzione)
if not isnull(s_cs_xx.parametri.parametro_data_4) and ll_ret>0 then
	//al ritorno dal configuratore bisogna aggiornare alla nuova data consegna
	//presente nella variabile s_cs_xx.parametri.parametro_data_4
	
	//gestione cambio data consegna dell'ordine ------------------------------------------------
	li_anno_ordine = dw_tes_ord_ven_det_1.getitemnumber(ll_ret,"anno_registrazione")
	ll_num_ordine = dw_tes_ord_ven_det_1.getitemnumber(ll_ret,"num_registrazione")
	
	s_cs_xx.parametri.parametro_d_4_a[1] = li_anno_ordine
	s_cs_xx.parametri.parametro_d_4_a[2] = ll_num_ordine
	
	if li_anno_ordine>0 and ll_num_ordine>0 then
		event trigger ue_cambia_data_partenza()
	end if
	
	setnull(s_cs_xx.parametri.parametro_d_4_a[1])
	setnull(s_cs_xx.parametri.parametro_d_4_a[2])
	
	//*******************************************************
end if

setnull(s_cs_xx.parametri.parametro_data_4)

if s_cs_xx.parametri.parametro_s_7= "ETICHETTA" and ll_ret>0 then
	dw_tes_ord_ven_det_1.setitem(ll_ret, "logo_etichetta", s_cs_xx.parametri.parametro_s_6)
	dw_tes_ord_ven_det_1.setitemstatus(ll_ret, "logo_etichetta", Primary!, NotModified!)
end if
s_cs_xx.parametri.parametro_s_6 = ""
s_cs_xx.parametri.parametro_s_7 = ""


//dw_tes_ord_ven_det_1.postevent("ue_allinea_ordine")
this.triggerevent("ue_calcola")

w_cs_xx_mdi.setredraw(true) 

if str_conf_prodotto.apri_configuratore then
	postevent("ue_riapri_configuratore")
end if
end event

event ue_riapri_configuratore();this.postevent("ue_configuratore")
end event

event ue_assegnazione();integer			li_return[], li_return_1, li_return_2, li_return_3

long				ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_i, ll_i1, ll_anno_commessa, ll_num_commessa

string				ls_cod_tipo_det_ven, ls_cod_prodotto, ls_sql_stringa, ls_cod_deposito, &
					ls_cod_ubicazione, ls_cod_giro_consegna, ls_cod_dep_giro
					
dec{4}			ld_quan_ordine, ld_quan_evasa, ld_quan_prodotta, ld_quan_commessa

longlong			ll_prog_sessione

uo_generazione_documenti	luo_gen_doc
uo_produzione					luo_prod
uo_log_sistema					luo_log


// 30.07.2013 tolto controllo sul flag_fuori_fido. (EnMe su richiesta di Beatrice)
// Quindi ora l'assegnazione può essere fatto anche per ordini fuori fido
if dw_tes_ord_ven_det_1.getrow() > 0 then

	ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")

	if ll_anno_registrazione>0 and ll_num_registrazione>0 then
	else
		return
	end if

	if dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_fuori_fido") = "N" and dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_evasione") <> "E" and dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_blocco") = "N" then

		ls_cod_deposito = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "cod_deposito")
		ls_cod_giro_consegna = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "cod_giro_consegna")
		ls_cod_ubicazione = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "cod_ubicazione")
		
		//----------------------------------------------------------------------------------
		//se l'ordine ha un giro consegna con deposito applicato e diverso da quello origine ordine, allora il deposito da assegnare deve essere questo
		luo_prod = create uo_produzione
		ls_cod_dep_giro = luo_prod.uof_deposito_giro_consegna(ls_cod_giro_consegna)
		destroy luo_prod

		if g_str.isnotempty(ls_cod_dep_giro) and ls_cod_deposito<>ls_cod_dep_giro then
			ls_cod_deposito = ls_cod_dep_giro
			
			luo_log = create uo_log_sistema
			luo_log.uof_write_log_sistema_not_sqlca("DEP.CONS.ASSEGN", "Applicato deposito codice "+ls_cod_dep_giro+" del  giro consegna dell'ordine "+string(ll_anno_registrazione)+"/" + string(ll_num_registrazione)+&
																							" al posto del deposito origine "+ls_cod_deposito+" in assegnazione intero ordine" + &
																							" da finestra testat ordine vendita.")
			destroy luo_log
		end if
		//----------------------------------------------------------------------------------
//		//Donato 05/04/2012
//		//puizia tabella evas_ord_ven. prima di inziare la spedizione (solo righe eventualmente "marcate" dall'utente in precedenti operazioni interrotte...)
//		delete from evas_ord_ven
//		where 	cod_azienda=:s_cs_xx.cod_azienda and
//					cod_utente=:s_cs_xx.cod_utente;
//		
//		if sqlca.sqlcode<0 then
//			g_mb.error("APICE","Errore in pulizia tabella spedizioni per l'utente: " + sqlca.sqlerrtext)
//			rollback;
//			return
//		end if
//		//-----------------------------------------------------------------------------------------------------------------------------------
		luo_gen_doc = create uo_generazione_documenti
		
		//genero un progressivo sessione univoco #########################
		ll_prog_sessione = wf_get_prog_sessione()
		s_cs_xx.parametri.parametro_s_10 = string(ll_prog_sessione)
		//#################################################
		
		declare cu_dettagli dynamic cursor for sqlsa;

		ls_sql_stringa = "select det_ord_ven.cod_tipo_det_ven, " + &
							  "det_ord_ven.prog_riga_ord_ven, " + &
							  "det_ord_ven.cod_prodotto, " + &
							  "det_ord_ven.quan_ordine, " + &
							  "det_ord_ven.quan_evasa, " + &
							  "det_ord_ven.anno_commessa, " + &
							  "det_ord_ven.num_commessa " + &
							  "from det_ord_ven " + &
							  "where det_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_ord_ven.anno_registrazione = " + string(ll_anno_registrazione) + " and det_ord_ven.num_registrazione = " + string(ll_num_registrazione) + " and det_ord_ven.flag_blocco = 'N' and det_ord_ven.flag_evasione <> 'E' and det_ord_ven.quan_in_evasione = 0 order by det_ord_ven.prog_riga_ord_ven"

		prepare sqlsa from :ls_sql_stringa;

		open dynamic cu_dettagli;

		ll_i = 0
		do while 0 = 0
		   fetch cu_dettagli into :ls_cod_tipo_det_ven, 
										  :ll_prog_riga_ord_ven, 
										  :ls_cod_prodotto, 
										  :ld_quan_ordine, 
										  :ld_quan_evasa,
										  :ll_anno_commessa,
										  :ll_num_commessa;

			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si  è verificato un errore in fase di lettura per aggiornamento magazzino.", &
							  exclamation!, ok!)
				close cu_dettagli;
				destroy luo_gen_doc;
				rollback;
				return
			end if

			if sqlca.sqlcode <> 0 then exit
			
			if not isnull(ll_anno_commessa) and not isnull(ll_num_commessa) then
				select anag_commesse.quan_prodotta
				into   :ld_quan_prodotta
				from   anag_commesse
				where  cod_azienda   = :s_cs_xx.cod_azienda and
						 anno_commessa = :ll_anno_commessa  and  
						 num_commessa  = :ll_num_commessa   ;
				if sqlca.sqlcode = 0 then
					ld_quan_commessa = ld_quan_prodotta		// assegno la quantità prodotta dall'ordine
				else
					ld_quan_commessa = 0							// vado avanti con la quantità dell'ordine
				end if
			else
				ld_quan_commessa = 0
			end if			
			ll_i ++
			
		
			li_return[ll_i] =  luo_gen_doc.uof_ass_ord_ven( 	ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, &
																			ls_cod_tipo_det_ven, ls_cod_deposito, ls_cod_ubicazione, ls_cod_prodotto, &
																			ld_quan_ordine, ld_quan_commessa, ld_quan_evasa, ll_prog_sessione)
							
		loop
		close cu_dettagli;
		destroy luo_gen_doc;
		
		commit;
	end if

	li_return_1 = 3
	li_return_2 = 3
	li_return_3 = 3
	
	for ll_i1 = 1 to upperbound(li_return[])
		choose case li_return[ll_i1]
			case 0
				li_return_1 = 0
			case 1
				li_return_2 = 1
			case 2
				li_return_3 = 2
		end choose
	next

	if li_return_1 = 3 and li_return_2 = 1 and li_return_3 = 3 then
		g_mb.messagebox("Informazione", "Assegnazione avvenuta con successo.",  information!)
		return
	end if

	if li_return_1 = 0 and li_return_2 = 3 and li_return_3 = 3 then
		g_mb.messagebox("Attenzione", "Nessun dettaglio è stato assegnato; "+&
									"è probabile che non vi siano le quantità disponibili a magazzino.~r~n"+&
									"Verificare che il deposito di prelievo indicato in testata ordini sia corretto", exclamation!)
		s_cs_xx.parametri.parametro_s_10 = ""
		return
	end if
	s_cs_xx.parametri.parametro_s_10 = ""
	g_mb.messagebox("Attenzione", "Non tutte le righe ordine sono state assegnate; entrare nei dettagli e veriricare.", exclamation!)
	
else
	return		
end if
end event

event ue_azzera();
window_open_parm ( w_tes_ord_ven_sessioni_evas_ord_ven, -1, dw_tes_ord_ven_det_1 )

/*
integer								li_return
long									ll_anno_registrazione, ll_num_registrazione
string									ls_perc, ls_messaggio, ls_flag_evasione, ls_flag_blocco
uo_generazione_documenti		luo_gen_doc


ls_perc = "%"

if dw_tes_ord_ven_det_1.getrow() > 0 then
	
	ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")
	
	if ll_anno_registrazione>0 and ll_num_registrazione>0 then
	else
		return
	end if
	
	ls_flag_evasione = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_evasione")
	ls_flag_blocco = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_blocco")
	
	if ls_flag_evasione="E" then
		g_mb.warning("Attenzione", "Azzeramento Assegnazione non avvenuta: Testata Ordine già Evasa!")
		return
		
	elseif ls_flag_blocco="S" then
		g_mb.warning("Attenzione", "Azzeramento Assegnazione non avvenuta: Testata Ordine Bloccata!")
		return
		
	end if
	
	//se arrivi fin qui continua ...
	luo_gen_doc = create uo_generazione_documenti
	li_return = luo_gen_doc.uof_azz_ass_ord_ven( ll_anno_registrazione, ll_num_registrazione, ls_perc, ls_messaggio)
	destroy luo_gen_doc;
	
	if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.messagebox("Azzeramento assegnazione",ls_messaggio)

	if li_return = 0 then
		g_mb.error("Attenzione", "Azzeramento Assegnazione non avvenuta!")
		return
	end if

	g_mb.success("Informazione", "Azzeramento Assegnazione avvenuta con successo!")
	
else
	
	return
end if
*/
end event

event ue_blocca();long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven

string ls_cod_tipo_det_ven, ls_cod_prodotto, ls_sql_stringa, ls_flag_tipo_det_ven, &
		 ls_flag_blocco,ls_cod_prodotto_raggruppato
		 
dec{4} ld_quan_ordine, ld_quan_evasa, ld_quan_in_evasione, ld_quan_raggruppo



if dw_tes_ord_ven_det_1.getrow() > 0 then
	if dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_blocco") = "N" then
		ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")

		if ll_anno_registrazione>0 and ll_num_registrazione>0 then
		else
			return
		end if
		
		//se la testata ordine non ha righe collegate non deve essere possibile bloccare (serve per AXIOMA)
		setnull(ll_i)
		
		select count(*)
		into :ll_i
		from det_ord_ven
		where 	cod_azienda=: s_cs_xx.cod_azienda and
					anno_registrazione=:ll_anno_registrazione and
					num_registrazione=:ll_num_registrazione;
		
		if isnull(ll_i) or ll_i=0 then
			g_mb.warning("Non è possibile bloccare la testata di un ordine senza righe di dettaglio!")
			return
		end if
		//-------------------------------------------------------------------------------------------------------
		

		declare cu_dettagli dynamic cursor for sqlsa;

		ls_sql_stringa = "select cod_tipo_det_ven, prog_riga_ord_ven, cod_prodotto, "+&
									"quan_ordine, quan_in_evasione, quan_evasa, flag_blocco "+&
							"from det_ord_ven "+&
							"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
										"anno_registrazione = " + string(ll_anno_registrazione) + " and "+&
										"num_registrazione = " + string(ll_num_registrazione)

		prepare sqlsa from :ls_sql_stringa;

		open dynamic cu_dettagli;

		ll_i = 0
		do while 0 = 0
			ll_i ++
			fetch cu_dettagli into :ls_cod_tipo_det_ven, :ll_prog_riga_ord_ven, :ls_cod_prodotto, :ld_quan_ordine, :ld_quan_in_evasione, :ld_quan_evasa, :ls_flag_blocco;
			
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore in fetch cursore cu_dettagli: " + sqlca.sqlerrtext)
				close cu_dettagli;
				rollback;
				return
			end if

			if sqlca.sqlcode <> 0 then exit

			select flag_tipo_det_ven
			into   :ls_flag_tipo_det_ven
			from   tab_tipi_det_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
					 
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore in lettura lettura flag tipo dettaglo da tab_tipi_det_ven:" + sqlca.sqlerrtext)
				close cu_dettagli;
				rollback;
			   return
			end if

			if ld_quan_in_evasione <> 0 then
				g_mb.warning("Attenzione", "Vi sono dettagli con quantità in evasione diversa da 0. Impossibile continuare.")
				close cu_dettagli;
				rollback;
				return
			end if
			
			if ls_flag_tipo_det_ven = "M" and ld_quan_evasa < ld_quan_ordine and ls_flag_blocco = "N" then
				update anag_prodotti  
					set quan_impegnata = quan_impegnata - (:ld_quan_ordine - :ld_quan_evasa)
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
				if sqlca.sqlcode = -1 then
					g_mb.error("Errore in aggiornamento impegnato in tabella anagrafica prodotti:"+sqlca.sqlerrtext)
					rollback;
					close cu_dettagli;
					rollback;
					return
				end if
				
				// enme 08/1/2006 gestione prodotto raggruppato
				setnull(ls_cod_prodotto_raggruppato)
				
				select cod_prodotto_raggruppato
				into   :ls_cod_prodotto_raggruppato
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
						 
				if not isnull(ls_cod_prodotto_raggruppato) then

					ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordine - ld_quan_evasa), "M")

					update anag_prodotti  
						set quan_impegnata = quan_impegnata - (:ld_quan_ordine - :ld_quan_evasa)
					 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
							 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
							 
					if sqlca.sqlcode = -1 then
						g_mb.error("Errore in aggiornamento impegnato del raggruppato in tabella anagrafica prodotti:"+sqlca.sqlerrtext)
						rollback;
						close cu_dettagli;
						rollback;
						return
					end if
				end if
				
			end if				
			
			update det_ord_ven
				set flag_blocco = 'S'
			 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					 det_ord_ven.anno_registrazione = :ll_anno_registrazione and  
					 det_ord_ven.num_registrazione = :ll_num_registrazione and
					 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;

			if sqlca.sqlcode = -1 then
				g_mb.error("Errore in aggiornamento flag blocco riga dettaglio ordine:"+sqlca.sqlerrtext)
				rollback;
				close cu_dettagli;
				return
			end if

		loop
		close cu_dettagli;
		
		//sblocco testata
		update tes_ord_ven
		set flag_blocco = 'S'
		where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				tes_ord_ven.anno_registrazione = :ll_anno_registrazione and  
				tes_ord_ven.num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode = -1 then
			g_mb.error("Errore in aggiornamento flag blocco testata ordine:"+sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		dw_tes_ord_ven_det_1.setitem(dw_tes_ord_ven_det_1.getrow(), "flag_blocco", "S")
		dw_tes_ord_ven_det_1.setitemstatus(dw_tes_ord_ven_det_1.getrow(), "flag_blocco", primary!, notmodified!)
		commit;
	end if
else
	return
end if
end event

event ue_calcola();string									ls_messaggio
long									ll_num_registrazione, ll_anno_registrazione, ll_ret, ll_return
uo_calcola_documento_euro	luo_calcola_documento_euro
dec{5}								ld_peso_netto
uo_funzioni_1						luo_funzioni_1
integer								li_ret

il_num_row = dw_tes_ord_ven_det_1.getrow()

ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(il_num_row,"anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(il_num_row,"num_registrazione")


if il_num_row > 0 and ll_anno_registrazione>0 and ll_num_registrazione>0 then
else
	g_mb.messagebox("Ordini di vendita", "nessun ordine selezionato: calcolo bloccato")
	return
end if

luo_calcola_documento_euro = create uo_calcola_documento_euro

//Donato 06-11-2008 metto a true per non fare di nuovo il controllo sull'esposizione cliente
luo_calcola_documento_euro.ib_salta_esposiz_cliente = true
//-----------------

if luo_calcola_documento_euro.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"ord_ven",ls_messaggio) <> 0 then
	//Donato 05-11-2008 dare msg solo se c'
	if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.messagebox("APICE",ls_messaggio)
	//g_mb.messagebox("APICE",ls_messaggio)
	rollback;
else
	commit;
end if
destroy luo_calcola_documento_euro


//Donato 22/01/2013: se il tipo ordine lo prevede, aggiorna il campo peso netto in testata ---------------------------
luo_funzioni_1 = create uo_funzioni_1
li_ret = luo_funzioni_1.uof_calcola_peso_testata_ordine(ll_anno_registrazione, ll_num_registrazione, ls_messaggio)
destroy luo_funzioni_1

if li_ret=0 then
	//la funzione ha calcolato il totale e fatto update in tes_ord_ven
	//necessita COMMIT
	commit;
	
elseif li_ret=1 then
	//la funzione non ha calcolato perchè non era previsto
	//non fare niente e vai avanti
else
	//errore, segnala
	g_mb.messagebox("APICE", ls_messaggio)
	rollback;
end if
//-------------------------------------------------------------------------------------------------------------------------------


dw_tes_ord_ven_det_1.change_dw_current()

long ll_anno_reg, ll_num_reg, ll_i

ll_anno_reg = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"anno_registrazione")
ll_num_reg = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"num_registrazione")


dw_tes_ord_ven_det_1.triggerevent("pcd_retrieve")

choose case dw_folder.i_selectedtab
	case 1
		dw_tes_ord_ven_det_1.setfocus()
	case 2
		dw_tes_ord_ven_det_2.setfocus()
	case 3
		dw_tes_ord_ven_det_3.setfocus()
	case 4
		dw_tes_ord_ven_det_4.setfocus()
//	case 5
//		dw_tes_ord_ven_lista.setfocus()
end choose


end event

event ue_cambio_reparti();long ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")

if ll_anno_registrazione>0 and ll_num_registrazione>0 then
	window_open_parm(w_tes_ord_ven_reparti,0,dw_tes_ord_ven_det_1)
else
	return
end if
end event

event ue_chiudi_commessa();integer 				li_anno_registrazione, li_anno_commessa[], li_flag_errore, li_risposta, li_anno_commessa_1, li_anno_c, li_errore_log
long 					ll_num_registrazione,ll_num_commessa[], ll_prog_riga_ord_ven, ll_i, ll_t, ll_num_commessa_1, ll_num_c, &
						ll_prog_riga_ordine[], ll_cont_depositi
string 				ls_flag_tipo_avanzamento,ls_errore, ls_messaggio, ls_null, ls_cod_deposito, ls_cod_deposito_prod, ls_cod_dep_giro, ls_cod_dep_origine
uo_funzioni_1 		luo_funzioni_1
uo_calendario_prod_new				luo_cal_prod
uo_produzione		luo_prod
uo_log_sistema		luo_log
boolean				lb_cambia_deposito = false


ls_messaggio = "Commesse con Errori: "
setnull(ls_null)

//leggo anno e numero ordine vendita
li_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")
ls_cod_deposito = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "cod_deposito")
ls_cod_deposito_prod = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "cod_deposito_prod")

if isnull(ls_cod_deposito_prod) then
	ls_cod_deposito_prod = ls_cod_deposito
end if
	
if li_anno_registrazione>0 and ll_num_registrazione>0 then
else
	return
end if

ll_i = 0
li_flag_errore = 0

// Verifico se per  caso la produzione avviene in depositi esterni
ll_cont_depositi = 0

select 	count(*)
into 		:ll_cont_depositi
from 		varianti_det_ord_ven
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :li_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			cod_deposito_produzione <> :ls_cod_deposito_prod;
			
if ll_cont_depositi > 0 and not isnull(ll_cont_depositi) then
	if not g_mb.confirm("ATTENZIONE: alcune righe dell'ordine sono soggette a produzione in ALTRO DEPOSITO.~r~nChiudendo la commessa da questo punto potrebbero sorgere anomalie negli scarichi.~r~nPROCEDO LO STESSO ? ") then
		return 
	else
		// Scrivo nel LOG la scelta dell'utente di andare avanti lo stesso.
		f_scrivi_log ( "Ordine " + string(li_anno_registrazione) + "-" + string(ll_num_registrazione) + " chisura commessa forzata da testata, anche con presenza di righe ordine soggette a produzione su altro deposito" )
	end if
end if


//----------------------------------------------------------------------------------
//se l'ordine ha un giro consegna con deposito applicato e diverso da quello oriogine ordine, allora il deposito di chiusura della commessa deve essere questo
//quindi le MP saranno prelevate da questo deposito e il PF verrà caricato in questo deposito!!
luo_prod = create uo_produzione
ls_cod_dep_giro = luo_prod.uof_deposito_giro_consegna(li_anno_registrazione, ll_num_registrazione)
destroy luo_prod

if g_str.isnotempty(ls_cod_dep_giro) then
	//leggo il deposito origine ordine
	select cod_deposito
	into :ls_cod_dep_origine
	from tes_ord_ven
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_registrazione and
				num_registrazione = :ll_num_registrazione;
	
	if g_str.isnotempty(ls_cod_dep_origine) and ls_cod_dep_origine<>ls_cod_dep_giro then
		lb_cambia_deposito=true
	end if
end if
//----------------------------------------------------------------------------------


//cursore delle commessa associata alla riga e recupero prog_riga_ord_ven
declare righe_det_ord_ven cursor for
select anno_commessa,
		 num_commessa,
		 prog_riga_ord_ven
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:li_anno_registrazione
and    num_registrazione=:ll_num_registrazione;

open righe_det_ord_ven;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return
end if

do while 1=1
	fetch righe_det_ord_ven
	into  	:li_anno_c,
			:ll_num_c,
			:ll_prog_riga_ord_ven;
			
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
		exit
	end if
	
	if sqlca.sqlcode = 100 then exit

	if not isnull(li_anno_c)  and not isnull(ll_num_c) then 
		ll_i++
		li_anno_commessa[ll_i]= li_anno_c
		ll_num_commessa[ll_i]=ll_num_c
		ll_prog_riga_ordine[ll_i]=ll_prog_riga_ord_ven
	end if
	
loop

close righe_det_ord_ven;


for ll_t = 1 to upperbound(li_anno_commessa[])
	
	li_anno_commessa_1 = li_anno_commessa[ll_t]
	ll_num_commessa_1 = ll_num_commessa[ll_t]
	
	select flag_tipo_avanzamento
	into   :ls_flag_tipo_avanzamento
	from   anag_commesse
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_commessa=:li_anno_commessa_1
	and    num_commessa=:ll_num_commessa_1;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice","Attenzione! Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return
	end if
			
	
	if ls_flag_tipo_avanzamento = "9" or ls_flag_tipo_avanzamento = "8" or ls_flag_tipo_avanzamento = "7" or ls_flag_tipo_avanzamento = "1" or ls_flag_tipo_avanzamento = "2" then
		continue
	else
		
		luo_funzioni_1 = create uo_funzioni_1
		
		luo_funzioni_1.uof_set_posticipato(true)
		
		//----------------------------------------------------------------------------------
		//se l'ordine ha un giro consegna con deposito applicato e diverso da quello oriogine ordine, allora il deposito di chiusura della commessa deve essere questo
		//quindi le MP saranno prelevate da questo deposito e il PF verrà caricato in questo deposito!!
		if lb_cambia_deposito then
			luo_funzioni_1.ib_forza_deposito_scarico_mp = true
			luo_funzioni_1.is_cod_deposito_scarico_mp = ls_cod_dep_giro
			
			luo_log = create uo_log_sistema
			luo_log.uof_write_log_sistema_not_sqlca("DEP.CONS.COMM", "Applicato deposito codice "+ls_cod_dep_giro+" del  giro consegna dell'ordine "+string(li_anno_registrazione)+"/" + string(ll_num_registrazione)+&
																							" al posto del deposito origine "+ls_cod_dep_origine+" in chiusura commessa n. "+string(li_anno_commessa_1)+"/" + string(ll_num_commessa_1) + &
																							" da testata ordine vendita.")
			destroy luo_log
		else
			luo_funzioni_1.ib_forza_deposito_scarico_mp = false
			setnull(luo_funzioni_1.is_cod_deposito_scarico_mp)
		end if
		//----------------------------------------------------------------------------------
		
		
		
		li_risposta = luo_funzioni_1.uof_avanza_commessa(li_anno_commessa_1,ll_num_commessa_1, ls_null, ls_errore)
		
		if li_risposta < 0 then 
			li_errore_log = f_scrivi_log ("Errore sulla commessa anno " + string(li_anno_commessa) +" numero " + string(ll_num_commessa) + ". Dettaglio errore: " + ls_errore)

			li_flag_errore = - 1
			ls_messaggio = ls_messaggio + string(li_anno_commessa_1) + "/" + string(ll_num_commessa_1) + " - "
			
			if li_errore_log = -1 then
				
				if g_mb.messagebox("Sep","Non è stato possibile scrivere il file di log. Tuttavia questo errore non è bloccante (riguarda esclusivamente la creazione del file log molto probabilmente manca il parametro log.), la produzione può continuare normalmente, vuoi proseguire?",question!,YesNo!,1) = 2 then
					rollback;
					destroy luo_funzioni_1
					return	
					
				end if
				
			end if		
			
			rollback;
			destroy luo_funzioni_1
			continue
			
		else
			//se sei riuscito a chiudere la commessa allora
			//fai pulizia tabella det_ord_ven_prod, MA PRIMA AGGIORNA LA TAB_CAL_PRODUZIONE -----------------------------------------
			luo_cal_prod = create uo_calendario_prod_new
			
			li_risposta = luo_cal_prod.uof_disimpegna_calendario(li_anno_registrazione, ll_num_registrazione, ll_prog_riga_ordine[ll_i], ls_errore)
			destroy luo_cal_prod;
			
			if li_risposta<0 then
				//messaggio ma non blocco
				//g_mb.warning(ls_errore)
				
			else
				/*
				delete det_ord_ven_prod
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    anno_registrazione = :li_anno_registrazione
				and    num_registrazione  = :ll_num_registrazione
				and    prog_riga_ord_ven  =: ll_prog_riga_ordine[ll_i];
				*/
				update det_ord_ven_prod
				set flag_attivo = 'N'
				where  cod_azienda = :s_cs_xx.cod_azienda
				and    anno_registrazione = :li_anno_registrazione
				and    num_registrazione  = :ll_num_registrazione
				and    prog_riga_ord_ven  =: ll_prog_riga_ordine[ll_i];

			end if
			
			//--------------------------------------------------------------------------------------------------------------------------------------------
		end if
		
		commit;
		destroy luo_funzioni_1
		
	end if 
next

if li_flag_errore = 0 then
	g_mb.messagebox("Apice","Impostazione Chiusura Commesse avvenuta con successo",information!)
else
	g_mb.messagebox("Apice","La chiusura automatica commesse è stata eseguita ma qualche commessa ha avuto dei problemi. Consultare il file di log. " + ls_messaggio ,stopsign!)
end if
end event

event ue_configuratore();string ls_cod_pagamento
double ld_sconto
string ls_messaggio
long ll_anno_registrazione, ll_num_registrazione, ll_ret

str_conf_prodotto.apri_configuratore = false
if isnull(dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"anno_registrazione")) or dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"anno_registrazione") = 0 or dw_tes_ord_ven_det_1.getrow() < 1 then return
	
//do
	str_conf_prodotto.anno_documento = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"anno_registrazione")
	str_conf_prodotto.num_documento = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"num_registrazione")
	str_conf_prodotto.prog_riga_documento = 0
	str_conf_prodotto.tipo_gestione = "ORD_VEN"
	setnull(str_conf_prodotto.cod_contatto)
	str_conf_prodotto.cod_cliente = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"cod_cliente")
	str_conf_prodotto.cod_agente_1 = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"cod_agente_1")
	str_conf_prodotto.cod_agente_2 = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"cod_agente_2")
	str_conf_prodotto.cod_tipo_listino_prodotto = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"cod_tipo_listino_prodotto")
	str_conf_prodotto.cod_valuta = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"cod_valuta")
	str_conf_prodotto.data_documento = dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(),"data_registrazione")
	str_conf_prodotto.data_registrazione = dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(),"data_registrazione")
	str_conf_prodotto.sconto_testata = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"sconto")
	str_conf_prodotto.data_consegna = dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(),"data_consegna")
	
	ls_cod_pagamento = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"cod_pagamento")
	if isnull(ls_cod_pagamento) then
		str_conf_prodotto.sconto_pagamento = 0
	else
		select sconto
		into   :ld_sconto
		from   tab_pagamenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_pagamento = :ls_cod_pagamento;
		if sqlca.sqlcode = 0 and not isnull(ld_sconto) then
			str_conf_prodotto.sconto_pagamento = ld_sconto
		else
			str_conf_prodotto.sconto_pagamento = 0
		end if
	end if
	setnull(str_conf_prodotto.cod_modello)
	setnull(str_conf_prodotto.cod_versione)
	setnull(str_conf_prodotto.cod_prodotto_finito)
	str_conf_prodotto.quan_vendita = 0
	str_conf_prodotto.prezzo_vendita = 0
	str_conf_prodotto.sconti[1] = 0
	str_conf_prodotto.sconti[2] = 0
	str_conf_prodotto.sconti[3] = 0
	str_conf_prodotto.sconti[4] = 0
	str_conf_prodotto.sconti[5] = 0
	str_conf_prodotto.sconti[6] = 0
	str_conf_prodotto.sconti[7] = 0
	str_conf_prodotto.sconti[8] = 0
	str_conf_prodotto.sconti[9] = 0
	str_conf_prodotto.sconti[10] = 0
	str_conf_prodotto.provvigione_1 = 0
	str_conf_prodotto.provvigione_2 = 0
	setnull(str_conf_prodotto.nota)
	setnull(str_conf_prodotto.cod_prodotto_finito)
	str_conf_prodotto.stato = "N"
	str_conf_prodotto.flag_cambio_modello = false	
	
	//Donato 03/10/2008 modifica su richiesta di marco -------------------------
	s_cs_xx.parametri.parametro_w_ord_ven = this
	//-----------------------------------------------------------------------------
	
	// Enme 04-06-2010
	setnull(str_conf_prodotto.des_prodotto)
	

	// EnMe 04-05-11: sostituito il window_open con openwithparm
	openwithparm(w_configuratore, this)
	//window_open(w_configuratore, 0 )
	
//loop until not(str_conf_prodotto.apri_configuratore)

end event

event ue_corrispondenze();long ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")

if ll_anno_registrazione>0 and ll_num_registrazione>0 then
	s_cs_xx.parametri.parametro_s_1 = "Ord_Ven"
	window_open_parm(w_acq_ven_corr, -1, dw_tes_ord_ven_det_1)
else
	return
end if

end event

event ue_dettaglio();long ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")

if ll_anno_registrazione>0 and ll_num_registrazione>0 then
else
	return
end if

s_cs_xx.parametri.parametro_w_ord_ven = this

window_open_parm(w_det_ord_ven_tv, -1, dw_tes_ord_ven_det_1)

end event

event ue_genera_bolla();long					ll_anno_bolla, ll_num_bolla, ll_max
string					ls_rif_interscambio, ls_num_ord_cliente, ls_flag_stampa_rif_intersc, ls_flag_stampa_num_ordine, ls_cod_tipo_fat_ven, &
						ls_cod_tipo_bol_ven, ls_cod_tipo_det_ven_rif_ord_ven, ls_cod_cliente, ls_cod_iva_riga, ls_cod_iva, ls_cod_tipo_ord_ven, ls_flag_tipo_bol_fat
datetime				ldt_data_registrazione, ldt_data_esenzione_iva, ldt_oggi
long					ll_anno_ordine, ll_num_ordine
string					ls_messaggio, ls_cod_parametro_blocco, ls_pvc_prefix


//Donato 04/05/2010 controlla se ci sono nelle righe dell'ordine commesse aperte
ll_anno_ordine = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"anno_registrazione")
ll_num_ordine = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"num_registrazione")

// stefanop_ 27/08/2014: aggiunge un prefisso alla riga RIF "vostro ordine"
guo_functions.uof_get_parametro_azienda("PVC", ls_pvc_prefix)
if isnull(ls_pvc_prefix) then
	ls_pvc_prefix = ""
end if
// ------------------------

if ll_anno_ordine>0 and ll_num_ordine>0 then
	if not wf_controlla_commesse_aperte(ll_anno_ordine, ll_num_ordine, ls_messaggio) then
		g_mb.error(ls_messaggio)
		return
	end if
else
	return
end if
//-----------------------------------------------------------------------------------------------------

//Donato 19-11-2008 prima di fare tutto verifica l'esposizione e lo scaduto del cliente
uo_fido_cliente l_uo_fido_cliente
long ll_return

l_uo_fido_cliente = create uo_fido_cliente

ls_cod_cliente = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "cod_cliente")
if ls_cod_cliente = "" or isnull(ls_cod_cliente) then return


//*******************************************************************************
//controllo parametri blocco
ldt_oggi = datetime(today(), 00:00:00)
ls_cod_tipo_ord_ven = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "cod_tipo_ord_ven")

select flag_tipo_bol_fat
into :ls_flag_tipo_bol_fat
from tab_tipi_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

if ls_flag_tipo_bol_fat="B" then
	//segue ddt
	ls_cod_parametro_blocco = "GBV"
else
	//segue fattura
	ls_cod_parametro_blocco = "GFV"
end if

ll_return = l_uo_fido_cliente.uof_get_blocco_cliente( ls_cod_cliente, ldt_oggi, ls_cod_parametro_blocco, ls_messaggio)
if ll_return=2 then
	//blocco
	g_mb.error(ls_messaggio)
	return
elseif ll_return=1 then
	//solo warning
	g_mb.warning(ls_messaggio)
end if
//*******************************************************************************


l_uo_fido_cliente.is_flag_autorizza_sblocco = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_aut_sblocco_fido")

ll_return = l_uo_fido_cliente.uof_check_cliente(ls_cod_cliente,datetime(today(),00:00:00),ls_messaggio)
destroy l_uo_fido_cliente

if ll_return = -1 then
	g_mb.messagebox("APICE","Errore in verifica esposizione cliente.~n" + ls_messaggio,exclamation!)
	return
end if

//Donato 05-11-2008 Modifica per specifica cliente PTENDA_ gestione fidi
if ll_return = 2 then
	//blocca perchè non sono autorizzato
	return
end if
//fine modifica ---------------------------


// ------------------------------------------------------------------------------------------------------
// procedura di assegnazione automatica 
//
// nuovo pulsante di assegnazione, evasione, creazione bolla; specifica \gibus\progetti\SEMPLIFICAZIONI
// enrico 6/4/2001
// -------------------------------------------------------------------------------------------------------

// ---------------------- assegno tutte le righe dell'ordine --------------------

this.triggerevent("ue_assegnazione")

//in s_cs_xx.parametri.parametro_s_10 c', convertito in stringa, il prog sessione della spedizione

// ---------------------- apro finestra di richiesta dati bolla -----------------

s_cs_xx.parametri.parametro_s_5 = "TOTALE"
s_cs_xx.parametri.parametro_ul_1 = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"anno_registrazione")
s_cs_xx.parametri.parametro_ul_2 = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"num_registrazione")
setnull(s_cs_xx.parametri.parametro_d_1)
setnull(s_cs_xx.parametri.parametro_d_2)
window_open(w_richiesta_dati_bolla, 0)

ll_anno_bolla = s_cs_xx.parametri.parametro_d_1
ll_num_bolla = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_1)
setnull(s_cs_xx.parametri.parametro_d_2)

if isnull(ll_anno_bolla) or isnull(ll_num_bolla) or ls_flag_tipo_bol_fat<>"B" then return 

ls_rif_interscambio = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"rif_interscambio")
ls_num_ord_cliente = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"num_ord_cliente")
ls_cod_cliente = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"cod_cliente")
if isnull(dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(),"data_consegna")) then
	ldt_data_registrazione = dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(),"data_registrazione")
else
	ldt_data_registrazione = dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(),"data_consegna")
end if


if ls_rif_interscambio = "" then setnull(ls_rif_interscambio)
if ls_num_ord_cliente = "" then setnull(ls_num_ord_cliente)

ls_flag_stampa_rif_intersc = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"flag_stampa_rif_intersc")
ls_flag_stampa_num_ordine = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"flag_stampa_num_ordine")

if ls_flag_stampa_rif_intersc <> "S" and ls_flag_stampa_num_ordine <> "S" then return
	
select cod_tipo_bol_ven
into   :ls_cod_tipo_bol_ven
from   tes_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_bolla and
		 num_registrazione = :ll_num_bolla;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE", "Errore durante la ricerca det tipo di bolla: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 then
	g_mb.messagebox("APICE", "Attenzione bolla inesistente. " + string(ll_anno_bolla) + "/" + string(ll_num_bolla))
	return	
end if		       
			
select cod_tipo_det_ven_rif_ord_ven
into   :ls_cod_tipo_det_ven_rif_ord_ven
from   tab_tipi_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
			 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE", "Errore durante la ricerca det tipo di riferimento della bolla: " + sqlca.sqlerrtext)
	return	
elseif sqlca.sqlcode = 100 then
	g_mb.messagebox("APICE", "Attenzione controllare il seguente tipo di bolla: " + ls_cod_tipo_bol_ven)
	return		
end if							 

// ----------------------->> cerco l'aliquota iva appropriata  << ------------------------------

setnull(ls_cod_iva_riga)

if not isnull(ls_cod_cliente) then
	select cod_iva,
			 data_esenzione_iva
	into   :ls_cod_iva,
			 :ldt_data_esenzione_iva
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_cliente = :ls_cod_cliente;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE", "Errore durante la ricerca del cliente. " + sqlca.sqlerrtext)
		return	
	elseif sqlca.sqlcode = 100 then
		if isnull(ls_cod_cliente) then ls_cod_cliente = "<null>"
		g_mb.messagebox("APICE", "Attenzione cliente "+ls_cod_cliente+" inesistente ")
		return		
	end if							 
end if

if sqlca.sqlcode = 0 then
	if ls_cod_iva <> "" and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
		ls_cod_iva_riga = ls_cod_iva
	end if
end if

if isnull(ls_cod_iva_riga) then
	select cod_iva  
	into   :ls_cod_iva  
	from   tab_tipi_det_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven_rif_ord_ven;
			 
	if sqlca.sqlcode = 0 then 		ls_cod_iva_riga = ls_cod_iva
	
end if

// ----------------------------------------------------------------------------------------------

select max(prog_riga_bol_ven)
into   :ll_max
from   det_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_bolla and
		 num_registrazione = :ll_num_bolla;
				 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE", "Errore durante la ricerca del progressivo massimo della bolla: " + sqlca.sqlerrtext)
	return
end if			

if isnull(ll_max) then ll_max = 0
ll_max = ll_max + 10

if ls_flag_stampa_rif_intersc = "S" and not isnull(ls_rif_interscambio) then
	insert into det_bol_ven(cod_azienda,
	                        anno_registrazione,
									num_registrazione,
									prog_riga_bol_ven,
									des_prodotto,
									cod_tipo_det_ven,
									cod_iva)
					values     (:s_cs_xx.cod_azienda,
					            :ll_anno_bolla,
									:ll_num_bolla,
									:ll_max,
									:ls_rif_interscambio,
									:ls_cod_tipo_det_ven_rif_ord_ven,
									:ls_cod_iva_riga);
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE", "Errore durante l'inserimento di R.I.: " + sqlca.sqlerrtext)
		rollback;
		return	
	end if	
	ll_max = ll_max + 10
end if	

if ls_flag_stampa_num_ordine = "S" and not isnull(ls_num_ord_cliente) then
	ls_num_ord_cliente = ls_pvc_prefix + ls_num_ord_cliente
	
	insert into det_bol_ven(cod_azienda,
	                        anno_registrazione,
									num_registrazione,
									prog_riga_bol_ven,
									des_prodotto,
									cod_tipo_det_ven,
									cod_iva)
					values     (:s_cs_xx.cod_azienda,
					            :ll_anno_bolla,
									:ll_num_bolla,
									:ll_max,
									:ls_num_ord_cliente,
									:ls_cod_tipo_det_ven_rif_ord_ven,
									:ls_cod_iva_riga);
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE", "Errore durante l'inserimento di Rif. Vs. Ordine: " + sqlca.sqlerrtext)
		rollback;
		return		
	end if	
end if

commit;
end event

event ue_evas_parziale();//Donato 19-11-2008 prima di fare tutto verifica l'esposizione e lo scaduto del cliente
uo_fido_cliente					l_uo_fido_cliente
long								ll_return, ll_anno_bolla, ll_num_bolla, ll_max
string								ls_messaggio, ls_cliente, ls_cod_tipo_ord_ven, ls_flag_tipo_bol_fat, ls_cod_parametro_blocco

datetime							ldt_oggi, ldt_data_registrazione, ldt_data_esenzione_iva
string								ls_rif_interscambio, ls_num_ord_cliente, ls_flag_stampa_rif_intersc, ls_flag_stampa_num_ordine, &
									ls_cod_tipo_bol_ven, ls_cod_tipo_det_ven_rif_ord_ven, ls_cod_cliente, ls_cod_iva_riga, ls_cod_iva


l_uo_fido_cliente = create uo_fido_cliente

ls_cliente = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "cod_cliente")
if ls_cliente = "" or isnull(ls_cliente) then return


//*******************************************************************************
//controllo parametri blocco
ldt_oggi = datetime(today(), 00:00:00)
ls_cod_tipo_ord_ven = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "cod_tipo_ord_ven")

select flag_tipo_bol_fat
into :ls_flag_tipo_bol_fat
from tab_tipi_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

if ls_flag_tipo_bol_fat="B" then
	//segue ddt
	ls_cod_parametro_blocco = "GBV"
else
	//segue fattura
	ls_cod_parametro_blocco = "GFV"
end if

ll_return = l_uo_fido_cliente.uof_get_blocco_cliente( ls_cliente, ldt_oggi, ls_cod_parametro_blocco, ls_messaggio)
if ll_return=2 then
	//blocco
	g_mb.error(ls_messaggio)
	return
elseif ll_return=1 then
	//solo warning
	g_mb.warning(ls_messaggio)
end if
//*******************************************************************************



l_uo_fido_cliente.is_flag_autorizza_sblocco = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_aut_sblocco_fido")

ll_return = l_uo_fido_cliente.uof_check_cliente(ls_cliente,datetime(today(),00:00:00),ls_messaggio)
destroy l_uo_fido_cliente

if ll_return = -1 then
	g_mb.messagebox("APICE","Errore in verifica esposizione cliente.~n" + ls_messaggio,exclamation!)
	return
end if

//Donato 05-11-2008 Modifica per specifica cliente PTENDA_ gestione fidi
if ll_return = 2 then
	//blocca perchè non sono autorizzato
	return
end if
//fine modifica ---------------------------


// ------------------------------------------------------------------------------------------------------
// procedura di evasione parziale ordine
//
// nuovo pulsante di assegnazione, evasione, creazione bolla (PARZIALE); specifica \gibus\progetti\SEMPLIFICAZIONI
// enrico 6/4/2001
// -------------------------------------------------------------------------------------------------------


// ---------------------- apro finestra di richiesta righe ordine -----------------

s_cs_xx.parametri.parametro_s_5 = "PARZIALE"
s_cs_xx.parametri.parametro_ul_1 = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"anno_registrazione")
s_cs_xx.parametri.parametro_ul_2 = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"num_registrazione")
setnull(s_cs_xx.parametri.parametro_d_1)
setnull(s_cs_xx.parametri.parametro_d_2)
window_open(w_richiesta_dati_bolla, 0)


ll_anno_bolla = s_cs_xx.parametri.parametro_d_1
ll_num_bolla = s_cs_xx.parametri.parametro_d_2

setnull(s_cs_xx.parametri.parametro_d_1)
setnull(s_cs_xx.parametri.parametro_d_2)

if isnull(ll_anno_bolla) or isnull(ll_num_bolla) or ls_flag_tipo_bol_fat<>"B" then return 

ls_rif_interscambio = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"rif_interscambio")
ls_num_ord_cliente = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"num_ord_cliente")
ls_cod_cliente = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"cod_cliente")
if isnull(dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(),"data_consegna")) then
	ldt_data_registrazione = dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(),"data_registrazione")
else
	ldt_data_registrazione = dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(),"data_consegna")
end if


if ls_rif_interscambio = "" then setnull(ls_rif_interscambio)
if ls_num_ord_cliente = "" then setnull(ls_num_ord_cliente)

ls_flag_stampa_rif_intersc = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"flag_stampa_rif_intersc")
ls_flag_stampa_num_ordine = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"flag_stampa_num_ordine")

if ls_flag_stampa_rif_intersc <> "S" and ls_flag_stampa_num_ordine <> "S" then return
	
select cod_tipo_bol_ven
into   :ls_cod_tipo_bol_ven
from   tes_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_bolla and
		 num_registrazione = :ll_num_bolla;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE", "Errore durante la ricerca det tipo di bolla: " + sqlca.sqlerrtext)
	return
elseif sqlca.sqlcode = 100 then
	g_mb.messagebox("APICE", "Attenzione bolla inesistente. " + string(ll_anno_bolla) + "/" + string(ll_num_bolla))
	return	
end if		       
			
select cod_tipo_det_ven_rif_ord_ven
into   :ls_cod_tipo_det_ven_rif_ord_ven
from   tab_tipi_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
			 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE", "Errore durante la ricerca det tipo di riferimento della bolla: " + sqlca.sqlerrtext)
	return	
elseif sqlca.sqlcode = 100 then
	g_mb.messagebox("APICE", "Attenzione controllare il seguente tipo di bolla: " + ls_cod_tipo_bol_ven)
	return		
end if	

// ----------------------->> cerco l'aliquota iva appropriata  << ------------------------------

setnull(ls_cod_iva_riga)

if not isnull(ls_cod_cliente) then
	select cod_iva,
			 data_esenzione_iva
	into   :ls_cod_iva,
			 :ldt_data_esenzione_iva
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_cliente = :ls_cod_cliente;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE", "Errore durante la ricerca del cliente. " + sqlca.sqlerrtext)
		return	
	elseif sqlca.sqlcode = 100 then
		if isnull(ls_cod_cliente) then ls_cod_cliente = "<null>"
		g_mb.messagebox("APICE", "Attenzione cliente "+ls_cod_cliente+" inesistente ")
		return		
	end if							 
end if

if sqlca.sqlcode = 0 then
	if ls_cod_iva <> "" and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
		ls_cod_iva_riga = ls_cod_iva
	end if
end if

if isnull(ls_cod_iva_riga) then
	select cod_iva  
	into   :ls_cod_iva  
	from   tab_tipi_det_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven_rif_ord_ven;
			 
	if sqlca.sqlcode = 0 then 		ls_cod_iva_riga = ls_cod_iva
	
end if

// ----------------------------------------------------------------------------------------------

					 			
select max(prog_riga_bol_ven)
into   :ll_max
from   det_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_bolla and
		 num_registrazione = :ll_num_bolla;
				 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE", "Errore durante la ricerca del progressivo massimo della bolla: " + sqlca.sqlerrtext)
	return
end if			

if isnull(ll_max) then ll_max = 0
ll_max = ll_max + 10

if ls_flag_stampa_rif_intersc = "S" and not isnull(ls_rif_interscambio) then
	insert into det_bol_ven(cod_azienda,
	                        anno_registrazione,
									num_registrazione,
									prog_riga_bol_ven,
									des_prodotto,
									cod_tipo_det_ven)
					values     (:s_cs_xx.cod_azienda,
					            :ll_anno_bolla,
									:ll_num_bolla,
									:ll_max,
									:ls_rif_interscambio,
									:ls_cod_tipo_det_ven_rif_ord_ven);
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE", "Errore durante l'inserimento di R.I.: " + sqlca.sqlerrtext)
		rollback;
		return	
	end if	
	ll_max = ll_max + 10
end if	

if ls_flag_stampa_num_ordine = "S" and not isnull(ls_num_ord_cliente) then
	insert into det_bol_ven(cod_azienda,
	                        anno_registrazione,
									num_registrazione,
									prog_riga_bol_ven,
									des_prodotto,
									cod_tipo_det_ven)
					values     (:s_cs_xx.cod_azienda,
					            :ll_anno_bolla,
									:ll_num_bolla,
									:ll_max,
									:ls_num_ord_cliente,
									:ls_cod_tipo_det_ven_rif_ord_ven);
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE", "Errore durante l'inserimento di Rif. Vs. Ordine: " + sqlca.sqlerrtext)
		rollback;
		return		
	end if	
end if

commit;
end event

event ue_proforma();string ls_errore
long ll_anno_registrazione, ll_num_registrazione, ll_anno_fattura,ll_num_fattura
uo_generazione_documenti luo_generazione_documenti

ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"num_registrazione")

if ll_anno_registrazione>0 and ll_num_registrazione>0 then
else
	return
end if

if g_mb.messagebox("APICE","Sei sicuro di voler creare la fattura PROFORMA partendo da questo ordine?",Question!,YesNo!,2) = 1 then
	luo_generazione_documenti = create uo_generazione_documenti
	if luo_generazione_documenti.uof_crea_fattura_proforma(ll_anno_registrazione, ll_num_registrazione, ref ll_anno_fattura, ref ll_num_fattura, ref ls_errore) = -1 then
		rollback;
		g_mb.messagebox("APICE","Errore in generazione fatture PROFORMA.~r~n" + ls_errore)
	else
		commit;
		g_mb.messagebox("APICE","Generata con successo la fattura proforma " + string(ll_anno_fattura) + "/" + string(ll_num_fattura))
	end if
	destroy luo_generazione_documenti
end if
return

end event

event ue_note();long ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")

if ll_anno_registrazione>0 and ll_num_registrazione>0 then
	s_cs_xx.parametri.parametro_s_1 = "Ord_Ven"
	window_open_parm(w_acq_ven_note, -1, dw_tes_ord_ven_det_1)
else
	return
end if

end event

event ue_salda();long  ll_anno_registrazione, ll_num_registrazione

string  ls_errore

uo_calcola_documento_euro luo_calcola_doc


if dw_tes_ord_ven_det_1.getrow() > 0 then
	
	ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")
	
	if ll_anno_registrazione>0 and ll_num_registrazione>0 then
	else
		return
	end if
	
	if dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_evasione") <> "E" then
		
		if g_mb.messagebox("APICE","Sei sicuro di voler SALDARE l'ordine visualizzato?",Question!,YesNo!,2) = 2 then return
		
		luo_calcola_doc = create uo_calcola_documento_euro
		if luo_calcola_doc.uof_salda_ordine( ll_anno_registrazione, ll_num_registrazione, ls_errore) = -1 then
			g_mb.error(ls_errore)
			rollback;
			return
		end if
		destroy luo_calcola_doc
		
		dw_tes_ord_ven_det_1.setitem(dw_tes_ord_ven_det_1.getrow(), "flag_evasione", "E")
		dw_tes_ord_ven_det_1.setitemstatus(dw_tes_ord_ven_det_1.getrow(), "flag_evasione", primary!, notmodified!)
		commit;
	end if
else
	return
end if

g_mb.messagebox("APICE","Ordine saldato forzatamente: RICORDARSI di cancellare le eventuali commesse delle righe con residuo > 0")


end event

event ue_sblocca();long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven

string ls_cod_tipo_det_ven, ls_cod_prodotto, ls_sql_stringa, ls_flag_tipo_det_ven, &
		 ls_flag_blocco,ls_cod_prodotto_raggruppato
		 
dec{4} ld_quan_ordine, ld_quan_in_evasione, ld_quan_evasa, ld_quan_raggruppo



if dw_tes_ord_ven_det_1.getrow() > 0 then
	ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")
	
	if ll_anno_registrazione>0 and ll_num_registrazione>0 then
	else
		return
	end if
	
	declare cu_dettagli dynamic cursor for sqlsa;

	ls_sql_stringa = "select cod_tipo_det_ven, prog_riga_ord_ven, cod_prodotto, "+&
									"quan_ordine, quan_in_evasione, quan_evasa, flag_blocco "+&
						"from det_ord_ven "+&
						"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
								"anno_registrazione = " + string(ll_anno_registrazione) + " and "+&
								"num_registrazione = " + string(ll_num_registrazione)

	prepare sqlsa from :ls_sql_stringa;
	open dynamic cu_dettagli;

	ll_i = 0
	do while 0 = 0
		ll_i ++
		fetch cu_dettagli into :ls_cod_tipo_det_ven, :ll_prog_riga_ord_ven, :ls_cod_prodotto, :ld_quan_ordine, :ld_quan_in_evasione, :ld_quan_evasa, :ls_flag_blocco;
			if sqlca.sqlcode = -1 then
			g_mb.error("Errore in fase di fetch cursore cu_dettagli: "+sqlca.sqlerrtext)
			close cu_dettagli;
			rollback;
			return
		end if

		if sqlca.sqlcode <> 0 then exit

		select flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode = -1 then
			g_mb.error("Errore in fase di lettura flag_tipo_det_ven da tabella tab_tipi_det_ven:"+sqlca.sqlerrtext)
			close cu_dettagli;
			rollback;
			return
		end if

		if ls_flag_tipo_det_ven = "M" and ls_flag_blocco = 'S' and ld_quan_evasa < ld_quan_ordine then
			update anag_prodotti  
				set quan_impegnata = quan_impegnata + (:ld_quan_ordine - :ld_quan_evasa)
				where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode = -1 then
				g_mb.error("Errore in fase di aggiornamento impegnato in tabella anagrafica prodotti:"+sqlca.sqlerrtext)
				rollback;
				close cu_dettagli;
				return
			end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
				
				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordine - ld_quan_evasa), "M")
				
				update anag_prodotti  
					set quan_impegnata = quan_impegnata + :ld_quan_raggruppo
					where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
		
				if sqlca.sqlcode = -1 then
					g_mb.error("Errore in fase di aggiornamento impegnato del raggruppato in tabella anagrafica prodotti:"+sqlca.sqlerrtext)
					rollback;
					close cu_dettagli;
					return
				end if
			end if
			
		end if				
		
		update det_ord_ven
			set flag_blocco = 'N'
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :ll_anno_registrazione and  
				 det_ord_ven.num_registrazione = :ll_num_registrazione and
				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;

		if sqlca.sqlcode = -1 then
			g_mb.error("Errore in fase di aggiornamento flag blocco in tabbella dettaglio riga ordine:"+sqlca.sqlerrtext)
			rollback;
			close cu_dettagli;
			return
		end if

//		update tes_ord_ven
//		set flag_blocco = 'N'
//		where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//				tes_ord_ven.anno_registrazione = :ll_anno_registrazione and  
//				tes_ord_ven.num_registrazione = :ll_num_registrazione;
//
//		if sqlca.sqlcode = -1 then
//			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata ordine.", &
//						  exclamation!, ok!)
//			rollback;
//			close cu_dettagli;
//			return
//		end if
	loop
	
	update tes_ord_ven
	set flag_blocco = 'N'
	where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			tes_ord_ven.anno_registrazione = :ll_anno_registrazione and  
			tes_ord_ven.num_registrazione = :ll_num_registrazione;

	if sqlca.sqlcode = -1 then
		g_mb.error("Errore in fase di aggiornamento flag blocco in tabella testata ordine:"+sqlca.sqlerrtext)
		rollback;
		return
	end if

	commit;
	close cu_dettagli;
	dw_tes_ord_ven_det_1.setitem(dw_tes_ord_ven_det_1.getrow(), "flag_blocco", "N")
	dw_tes_ord_ven_det_1.setitemstatus(dw_tes_ord_ven_det_1.getrow(), "flag_blocco", primary!, notmodified!)
	
else
	return
end if
end event

event ue_stampa();long				ll_righe_dett, ll_anno_reg, ll_num_reg
string				ls_cod_cliente, ls_messaggio
integer			li_ret


if dw_tes_ord_ven_det_1.getrow() > 0 then
	
	ll_anno_reg = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"anno_registrazione")
	ll_num_reg = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(),"num_registrazione")
	
	if ll_anno_reg>0 and ll_num_reg>0 then
	else
		return
	end if
	
	this.triggerevent("ue_calcola")
	
	ls_cod_cliente = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"cod_cliente")
		
	//il controllo su blocco/warning azione lo faccio solo la prima volta
	//in quanto tutti gli ordini saranno dello stesso cliente ...
	li_ret = iuo_fido_cliente.uof_get_blocco_cliente(	ls_cod_cliente,&
																	datetime(today(), 00:00:00), "SOC", ls_messaggio)
	if li_ret=2 then
		//blocco
		g_mb.warning(ls_messaggio)
		return
	elseif li_ret=1 then
		//solo warning
		g_mb.warning(ls_messaggio)
	end if
	
	
	s_cs_xx.parametri.parametro_d_1 = ll_anno_reg
	s_cs_xx.parametri.parametro_d_2 = ll_num_reg
	
	select count(*)
	into   :ll_righe_dett
	from   det_ord_ven
	where  cod_azienda        = :s_cs_xx.cod_azienda and
			 anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
			 num_registrazione  = :s_cs_xx.parametri.parametro_d_2;
			 
	if ll_righe_dett = 0 or isnull(ll_righe_dett) then
		g_mb.messagebox("Stampa Ordine Vendita","Nessun dettaglio presente in questo ordine: impossibile stampare!",StopSign!)
		return
	end if
	
	window_open(w_report_ord_ven_euro, -1)
	
end if

end event

event ue_aggiungi_conferma_ordine();treeviewitem		ltv_item

ss_record			ls_record

string					ls_cod_cliente, ls_cod_deposito, ls_cod_porto, ls_cod_mezzo, ls_cod_resa, ls_cod_causale, &
						ls_cod_cliente1, ls_cod_deposito1, ls_cod_porto1, ls_cod_mezzo1, ls_cod_resa1, ls_cod_causale1, ls_messaggio
						
long					ll_i, ll_anno_ordine, ll_num_ordine

integer				li_ret



tv_1.getitem(il_handle,ltv_item)
ls_record = ltv_item.data

ll_anno_ordine = long(ls_record.anno_registrazione)
ll_num_ordine = long(ls_record.num_registrazione)

ll_i = upperbound(il_anno_conferma)

if ll_i < 1 or isnull(ll_i) then ll_i = 0

ll_i ++

// controllo congruenza ordine con altri ordini inseriti; deve essere omogeneo per Cliente - Deposito - Pagamento - Porto - Mezzo - Resa - Causale
if ll_i > 1 then
	// leggo il primo ordine
	select cod_cliente, cod_deposito, cod_porto, cod_mezzo, cod_resa, cod_causale
	into 	:ls_cod_cliente, :ls_cod_deposito, :ls_cod_porto, :ls_cod_mezzo, :ls_cod_resa, :ls_cod_causale
	from 	tes_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_conferma[1] and
			 num_registrazione = :il_num_conferma[1] ;
			 
	//l'ordine che ho scelto di aggiungere
	select cod_cliente, cod_deposito, cod_porto, cod_mezzo, cod_resa, cod_causale
	into 	:ls_cod_cliente1, :ls_cod_deposito1, :ls_cod_porto1, :ls_cod_mezzo1, :ls_cod_resa1, :ls_cod_causale1
	from 	tes_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_ordine and
			 num_registrazione = :ll_num_ordine ;
			 
	if ls_cod_cliente <> ls_cod_cliente1 or ls_cod_deposito <> ls_cod_deposito1 or ls_cod_porto <> ls_cod_porto1 or ls_cod_mezzo <> ls_cod_mezzo1 or ls_cod_resa <> ls_cod_resa1 or ls_cod_causale <> ls_cod_causale1 then
		g_mb.warning("Impossibile aggiungere l'ordine perchè il cliente, deposito, porto, mezzo, resa o causale sono diversi dagli altri ordini!")
		return
	end if
	
else
	select cod_cliente
	into :ls_cod_cliente
	from tes_ord_ven
	where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_ordine and
				num_registrazione = :ll_num_ordine ;
	
	//il controllo su blocco/warning azione lo faccio solo la prima volta
	//in quanto tutti gli ordini saranno dello stesso cliente ...
	li_ret = iuo_fido_cliente.uof_get_blocco_cliente(	ls_cod_cliente,&
																	datetime(today(), 00:00:00), "SCO", ls_messaggio)
	if li_ret=2 then
		//blocco
		g_mb.warning(ls_messaggio)
		return
	elseif li_ret=1 then
		//solo warning
		g_mb.warning(ls_messaggio)
	end if
	
end if


il_anno_conferma[ll_i] = ll_anno_ordine
il_num_conferma[ll_i] = ll_num_ordine

end event

event ue_stampa_conferma_ordine();string ls_cod_cliente, ls_cod_lingua_cliente

s_cs_xx.parametri.parametro_d_1_a[] = il_anno_conferma[]
s_cs_xx.parametri.parametro_d_2_a[]  = il_num_conferma[]

this.triggerevent("ue_calcola")

select cod_cliente
into :ls_cod_cliente
from tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :il_anno_conferma[1] and
		num_registrazione =  :il_num_conferma[1] ;
if sqlca.sqlcode <>0 then
	g_mb.error("Impossibile trovare la testata ordine dell'ordine selezionato!")
	return
end if

select cod_lingua
into :ls_cod_lingua_cliente
from anag_clienti
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_cliente = :ls_cod_cliente ;
if sqlca.sqlcode <>0 then
	g_mb.error("Impossibile trovare l'anagrafica del cliente nel documento selezionato!")
	return
end if

s_cs_xx.cod_lingua_window = ls_cod_lingua_cliente

window_open(w_report_conferma_ordine, -1)


end event

event ue_reset_conferma_ordine();if g_mb.confirm("Eseguo il reset delle conferme?") then

	long ll_null[]
	
	il_anno_conferma[] = ll_null[]
	il_num_conferma[] = ll_null[]

end if
end event

event ue_cambia_data_partenza();datetime			ldt_nuova_data_consegna
string				ls_messaggio
long				ll_num_ordine
integer			li_anno_ordine


li_anno_ordine = s_cs_xx.parametri.parametro_d_4_a[1]
ll_num_ordine = s_cs_xx.parametri.parametro_d_4_a[2]

setnull( s_cs_xx.parametri.parametro_d_4_a[1])
setnull( s_cs_xx.parametri.parametro_d_4_a[2])

if li_anno_ordine>0 then

	ldt_nuova_data_consegna = s_cs_xx.parametri.parametro_data_4
	setnull(s_cs_xx.parametri.parametro_data_4)
	
	//prevengo eventuali date farlocche tipo 01/01/1900
	if isnull(ldt_nuova_data_consegna) or year(date(ldt_nuova_data_consegna)) <=1950 then return
	
	
	//modifica la data consegna delle righe
	update det_ord_ven
	set data_consegna=:ldt_nuova_data_consegna
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_ordine and
				num_registrazione=:ll_num_ordine;
				
	if sqlca.sqlcode<0 then
		ls_messaggio = "Errore in aggiornamento data consegna righe dettagli ordine!"+ sqlca.sqlerrtext
		g_mb.error(ls_messaggio)
		rollback;
	end if
	
	//e della testata ----
	update tes_ord_ven
	set data_consegna=:ldt_nuova_data_consegna
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_ordine and
				num_registrazione=:ll_num_ordine ;
			
	if sqlca.sqlcode<0 then
		ls_messaggio = "Errore in aggiornamento data consegna righe dettagli ordine!"+ sqlca.sqlerrtext
		g_mb.error(ls_messaggio)
		rollback;
	end if
	
	commit;
end if
//*******************************************************
end event

event ue_unisci_doc();integer									li_anno_registrazione,li_ret

long										ll_num_registrazione, ll_row

string									ls_errore

uo_service_doc_ord_ven			luo_doc



ll_row = dw_tes_ord_ven_det_1.getrow()
if ll_row>0 then
else
	g_mb.warning("APICE","Selezionare un ordine!")
	return

end if

li_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(ll_row, "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(ll_row, "num_registrazione")

if isnull(li_anno_registrazione) or li_anno_registrazione = 0 or isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
	g_mb.warning("APICE","Selezionare un ordine!")
	return
end if

if not g_mb.confirm("Procedere all'unione dei documenti di tutte le righe dell'ordine per tipologia (P-T-O) ed eventuali documenti di testata (T)?") then return

setpointer(Hourglass!)

luo_doc = create uo_service_doc_ord_ven
li_ret = luo_doc.uof_unisci_documenti(li_anno_registrazione, ll_num_registrazione, ls_errore)
destroy luo_doc

setpointer(Arrow!)

if li_ret < 0 then
	//rollback già fatto
	g_mb.error(ls_errore)
	
else
	//commit già fatto, in verità per ogni riga/tipologia
	//retrieve docs
	dw_documenti.uof_retrieve_blob(ll_row)
	g_mb.success("Operazione terminata!")
	
end if


end event

event ue_menu_azzera_spese_trasporto();/**
 * stefanop
 * 18/02/2014
 *
 * Azzero le spese di trasporto dei dettagli
 **/
 
string ls_message
int li_anno, li_ret, li_row
long ll_numero

li_row = dw_tes_ord_ven_det_1.getrow()
li_anno = dw_tes_ord_ven_det_1.getitemnumber(li_row, "anno_registrazione")
ll_numero = dw_tes_ord_ven_det_1.getitemnumber(li_row, "num_registrazione")

if g_mb.confirm("Azzerare le spese di trasporto per l'ordine " + string(li_anno) + "/" + string(ll_numero) + "?") then
 
	li_ret = iuo_spese_trasporto.uof_azzera_spese_ordine(li_anno, ll_numero, "S", ref ls_message)
	
	if li_ret = 0 then
		dw_tes_ord_ven_det_1.setitem(li_row, "flag_azzera_spese_trasp", "S")
		dw_tes_ord_ven_det_1.setitemstatus(li_row, "flag_azzera_spese_trasp", primary!, NotModified!)
		//dw_tes_ord_ven_det_1.resetupdate()
	elseif li_ret = 1 then
		g_mb.warning(ls_message)
	else
		g_mb.error(ls_message)
	end if
	
end if
end event

event ue_menu_abilita_spese_trasporto();/**
 * stefanop
 * 18/02/2014
 *
 * Azzero le spese di trasporto dei dettagli
 **/
 
string ls_message
int li_anno, li_ret, li_row
long ll_numero

li_row = dw_tes_ord_ven_det_1.getrow()
li_anno = dw_tes_ord_ven_det_1.getitemnumber(li_row, "anno_registrazione")
ll_numero = dw_tes_ord_ven_det_1.getitemnumber(li_row, "num_registrazione")

if g_mb.confirm("Abilito le spese di trasporto per l'ordine " + string(li_anno) + "/" + string(ll_numero) + "?") then
 
	li_ret = iuo_spese_trasporto.uof_azzera_spese_ordine(li_anno, ll_numero, "N", ref ls_message)
	
	if li_ret = 0 then
		dw_tes_ord_ven_det_1.setitem(li_row, "flag_azzera_spese_trasp", "N")
		dw_tes_ord_ven_det_1.setitemstatus(li_row, "flag_azzera_spese_trasp", primary!, NotModified!)

	elseif li_ret = 1 then
		g_mb.warning(ls_message)
	else
		g_mb.error(ls_message)
	end if
	
end if
end event

event ue_visualizza_spese_trasporto();integer		li_anno_registrazione
long			ll_num_registrazione

li_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")
	
if li_anno_registrazione>0 and ll_num_registrazione>0 then
else
	return
end if

window_open_parm(w_det_ord_ven_spese_trasp, -1, dw_tes_ord_ven_det_1)

return
end event

event ue_stampa_rapida_conferma();// stampa rapida conferma
long ll_null[]

il_anno_conferma[] = ll_null[]
il_num_conferma[] = ll_null[]

event ue_aggiungi_conferma_ordine()

event ue_stampa_conferma_ordine()

il_anno_conferma[] = ll_null[]
il_num_conferma[] = ll_null[]

return

end event

event ue_documenti_fiscali();integer			li_anno_ordine
long				ll_num_ordine

li_anno_ordine = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_ordine = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")

if li_anno_ordine>0 and ll_num_ordine>0 then
else
	return
end if

window_open_parm(w_doc_fiscali_ordine_vendita, -1, dw_tes_ord_ven_det_1)

return
end event

event ue_importa_excel();/**
 * stefanop
 * 11/06/2014
 *
 * Esegue importazione di un foglio excel all'interno dell'ordine.
 **/

string ls_filepath, ls_error
int li_return
long ll_anno, ll_numero

uo_importa_excel_ord_ven luo_importa

luo_importa = create uo_importa_excel_ord_ven
if luo_importa.uof_seleziona_file(ref ls_filepath) = 1 then
	
	ll_anno = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
	ll_numero = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione") 

	li_return = luo_importa.uof_importa_in_ordine(ll_anno, ll_numero, ls_filepath, ref ls_error)
	
	if  li_return= 1 then
		commit;
		g_mb.success("Importazione eseguita con successo")
	else
		rollback;
		
		if li_return = 0 then
			g_mb.warning(ls_error)
		else
			g_mb.error(ls_error)
		end if
		
	end if
	
end if

destroy luo_importa
end event

event ue_marginalita();integer											li_anno
long 												ll_numero
s_cs_xx_parametri								lstr_parametri
w_report_margine_ordini_sfuso			lw_window


li_anno = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_numero = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")

if li_anno>0 and ll_numero>0 then
else
	return
end if

lstr_parametri.parametro_d_1 = li_anno
lstr_parametri.parametro_d_2 = ll_numero
lstr_parametri.parametro_s_1 = "O"

opensheetwithparm(lw_window, lstr_parametri, PCCA.MDI_Frame, 6,  Original!)


return
end event

event ue_annulla_evasione_alusistemi();string	ls_nota_piede
long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven
uo_generazione_documenti luo_gen

luo_gen = CREATE uo_generazione_documenti

if not g_mb.confirm( "Ripristino Evasione", "Sei sicuro di voler ripristina l'ordine evaso?", 2)  then return


ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")

update det_ord_ven
set 	quan_evasa = 0, 
		quan_in_evasione=0,
		flag_evasione ='A'
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione;
if sqlca.sqlcode = -1 then
	g_mb.error( g_str.format("Errore in fase di evasione riga ordine $1-$2-$3.~r~n$4", ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,sqlca.sqlerrtext ))
	rollback;
	destroy luo_gen
	return
end if

luo_gen.uof_calcola_stato_ordine(ll_anno_registrazione, ll_num_registrazione)

ls_nota_piede = g_str.format("~r~nAnnullata spedizione in data:$1 ora:$2", string(today(),"dd/mm/yyyy"), string(now(),"hh:mm"))

update tes_ord_ven
set nota_piede = nota_piede + :ls_nota_piede
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione;
if sqlca.sqlcode = -1 then
	g_mb.error( g_str.format("Errore in fase di memo nota piede $1-$2~r~n$4", ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,sqlca.sqlerrtext ) )
	rollback;
	destroy luo_gen
	return
end if


commit;
destroy luo_gen

g_mb.show( g_str.format( "Ordine $1/$2 ripristinato", ll_anno_registrazione,ll_num_registrazione) )

return
end event

public function boolean wf_controlla_commesse_aperte (long fl_anno_ordine, long fl_num_ordine, ref string fs_msg);datastore		lds_data
string				ls_sql, ls_cod_prodotto, ls_flag_tipo_avanzamento, ls_null
long				ll_rows, ll_index, ll_prog_riga, ll_anno_commessa, ll_num_commessa
boolean			lb_PCC
integer			li_ret



ls_sql = "select prog_riga_ord_ven, anno_commessa, num_commessa,"+&
				"cod_prodotto "+&
			"from det_ord_ven "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
				"anno_registrazione="+string(fl_anno_ordine) +" and "+&
				"num_registrazione="+string(fl_num_ordine) + " and "+&
				"anno_commessa is not null and anno_commessa>0 and "+&
				"num_commessa is not null and num_commessa>0 "

if not f_crea_datastore(lds_data, ls_sql) then
	fs_msg = "Errore di creazione datastore controllo commesse!"
	return false
end if

ll_rows = lds_data.retrieve()
for ll_index=1 to ll_rows
	ll_prog_riga = lds_data.getitemnumber(ll_index, 1)
	ll_anno_commessa = lds_data.getitemnumber(ll_index, 2)
	ll_num_commessa = lds_data.getitemnumber(ll_index, 3)
	ls_cod_prodotto = lds_data.getitemstring(ll_index, 4)
	
	select flag_tipo_avanzamento
	into :ls_flag_tipo_avanzamento
	from anag_commesse
	where cod_azienda=:s_cs_xx.cod_azienda and
		anno_commessa=:ll_anno_commessa and num_commessa=:ll_num_commessa;
	
	if sqlca.sqlcode<0 then
		fs_msg = "Errore di lettura avanzamento commesse! "
		fs_msg += "Riga ordine "+string(ll_prog_riga)
		fs_msg += "~r~nProdotto "+ls_cod_prodotto
		fs_msg += "~r~nCommessa "+string(ll_anno_commessa)+"/"+string(ll_num_commessa)+"~r~n"
		fs_msg += sqlca.sqlerrtext
		return false
	elseif sqlca.sqlcode=100 then
		fs_msg = "Commessa non trovata! "
		fs_msg += "Riga ordine "+string(ll_prog_riga)
		fs_msg += "~r~nProdotto "+ls_cod_prodotto
		fs_msg += "~r~nCommessa "+string(ll_anno_commessa)+"/"+string(ll_num_commessa)
		return false
	end if
	
	/*
	9 CHIUSA ASSEGN
	8 CHIUSA ATTIV
	7 CHIUSA AUTOM
	*/
	if ls_flag_tipo_avanzamento<> "9" and ls_flag_tipo_avanzamento<>"8" and ls_flag_tipo_avanzamento<>"7" then
		
		
		//se la commessa è ancora aperta ma è attivo il sistema di posticipazione chiusura
		//ed esiste una pianificazione del nostro tipo (anno, numero NULL) allora non dare il messaggio ma fai andare avanti
		//nella generazione del documento
		guo_functions.uof_get_parametro_azienda( "PCC", lb_PCC)

		if lb_PCC then
			//è attiva l'elaborazione posticipata delle commesse
			setnull(ls_null)
			
			//	-1	errore critico
			//	0	esiste pianificazione di chiusura, quindi vai pure avanti
			//	1	NON esiste pianificazione di chiusura, quindi non permetetre di andare avanti
			li_ret = wf_controlla_pianificazione(ll_anno_commessa, ll_num_commessa, ls_null, fs_msg)
			if li_ret<0 then
				return false
				
			elseif li_ret = 0 then
				return true
				
			else
				fs_msg += "Riga ordine "+string(ll_prog_riga)
				fs_msg += "~r~nProdotto "+ls_cod_prodotto
				fs_msg += "~r~nCommessa "+string(ll_anno_commessa)+"/"+string(ll_num_commessa)
				return false
			end if
			
		else
			//commessa ancora aperta: avvisa e blocca
			fs_msg = "Esiste ancora una commessa associata ad una riga dell'ordine che risulta aperta! "
			fs_msg += "Riga ordine "+string(ll_prog_riga)
			fs_msg += "~r~nProdotto "+ls_cod_prodotto
			fs_msg += "~r~nCommessa "+string(ll_anno_commessa)+"/"+string(ll_num_commessa)
			return false
		end if
		
	end if
	
next

return true
end function

public subroutine wf_cancella_treeview ();ib_tree_deleting = true
tv_1.deleteitem(0)
ib_tree_deleting = false
end subroutine

public subroutine wf_imposta_treeview ();string				ls_null, ls_ologramma
long				ll_risposta, ll_1, ll_2, ll_3, ll_i, ll_tvi, ll_root, ll_anno_registrazione, ll_num_registrazione
ss_record		lstr_record
treeviewitem	ltvi_campo

setnull(ls_null)

ltvi_campo.expanded = false
ltvi_campo.selected = false
ltvi_campo.children = false	//altrimenti mostrava il segno di + anche accanto agli elementi senza sottoelementi

ll_i = 0
setpointer(HourGlass!)

tv_1.setredraw(false)

ll_anno_registrazione = dw_tes_ord_ven_ricerca.getitemnumber(1,"anno_registrazione")
ll_num_registrazione 	= dw_tes_ord_ven_ricerca.getitemnumber(1,"num_registrazione")
ls_ologramma 			= dw_tes_ord_ven_ricerca.getitemstring(1,"ologramma")

//se è stata richiesto uno specifico OLOGRAMMA procedo solo per questo, ignorando altri tipi di filtri   ------------------
if ls_ologramma<>"" and not isnull(ls_ologramma) then
	//è stata richiesto uno specifico OLOGRAMMA procedo solo per questo, ignorando altri tipi di filtri   ------------------
	ib_retrieve=true
	wf_inserisci_singolo_ordine_ologramma(ls_ologramma )
	tv_1.setredraw(true)
	return

elseif ll_anno_registrazione > 0 and not isnull(ll_anno_registrazione) and &
		ll_num_registrazione > 0 and not isnull(ll_num_registrazione) then
	//è stata richiesta una specifica registrazione propongo solo quella e niente altro --------------------------------------
	
	ib_retrieve=true
	wf_inserisci_singolo_ordine(0, ll_anno_registrazione, ll_num_registrazione )
	tv_1.setredraw(true)
	return
	
end if

il_livello_corrente = 0

// controlla cosa c'è al livello successivo
choose case dw_tes_ord_ven_ricerca.getitemstring(1,"flag_tipo_livello_" + string(il_livello_corrente + 1))
	case "T"
		//caricamento tipo ordine
		wf_inserisci_tipi_ordini(0)
		
	case "A"
		//caricamento anni
		wf_inserisci_anni(0)

	case "C"
		//caricamento clienti
		wf_inserisci_clienti(0)
		
	case "O"
		//caricamento operatori
		wf_inserisci_operatori(0)
		
	case "D"
		//caricamento depositi
		wf_inserisci_depositi(0)
		
	case "P"
		//caricamento prodotti
		wf_inserisci_prodotti(0)
		
	case "R" 
		//caricamento date registrazioni
		wf_inserisci_data_registrazione(0)
		
	case "S" 
		//caricamento date consegna
		wf_inserisci_data_consegna(0)
		
	case "N"
		wf_inserisci_ordini(0)

end choose

tv_1.setredraw(true)

setpointer(arrow!)
return
end subroutine

public subroutine wf_ricerca ();long			ll_anno_registrazione, ll_num_registrazione
datetime	ldt_data_registrazione_inizio, ldt_data_registrazione_fine, ldt_data_consegna_inizio, ldt_data_consegna_fine
string		ls_tipo_ordine, ls_cod_valuta, ls_cod_cliente, ls_cod_operatore, ls_cod_resa, ls_rif_interscambio, ls_num_ord_cliente, &
				ls_cod_deposito, ls_cod_agente_1, ls_flag_evasione, ls_cod_prodotto, ls_flag_blocco

dw_tes_ord_ven_ricerca.accepttext()

ll_anno_registrazione 				= dw_tes_ord_ven_ricerca.getitemnumber(1,		 "anno_registrazione")
ll_num_registrazione 				= dw_tes_ord_ven_ricerca.getitemnumber(1, 		"num_registrazione")
ldt_data_registrazione_inizio	= dw_tes_ord_ven_ricerca.getitemdatetime(1, 	"data_registrazione_d")
ldt_data_registrazione_fine 		= dw_tes_ord_ven_ricerca.getitemdatetime(1, 	"data_registrazione_a")
ldt_data_consegna_inizio 		= dw_tes_ord_ven_ricerca.getitemdatetime(1, 	"data_consegna_d")
ldt_data_consegna_fine 			= dw_tes_ord_ven_ricerca.getitemdatetime(1, 	"data_consegna_a")
//ldt_data_pronto 						= dw_tes_ord_ven_ricerca.getitemdatetime(1, 	"data_pronto_d")
ls_tipo_ordine						= dw_tes_ord_ven_ricerca.getitemstring(1, 		"cod_tipo_ord_ven")
ls_cod_valuta							= dw_tes_ord_ven_ricerca.getitemstring(1, 		"cod_valuta")
ls_cod_cliente							= dw_tes_ord_ven_ricerca.getitemstring(1, 		"cod_cliente")
ls_cod_prodotto						= dw_tes_ord_ven_ricerca.getitemstring(1, 		"cod_prodotto")
ls_cod_operatore					= dw_tes_ord_ven_ricerca.getitemstring(1,			"cod_operatore")
ls_cod_resa							= dw_tes_ord_ven_ricerca.getitemstring(1, 		"cod_resa")
ls_rif_interscambio					= dw_tes_ord_ven_ricerca.getitemstring(1, 		"rif_interscambio")
ls_num_ord_cliente					= dw_tes_ord_ven_ricerca.getitemstring(1, 		"num_ord_cliente")
ls_cod_deposito						= dw_tes_ord_ven_ricerca.getitemstring(1, 		"cod_deposito")
ls_cod_agente_1					= dw_tes_ord_ven_ricerca.getitemstring(1, 		"cod_agente_1")
ls_flag_evasione						= dw_tes_ord_ven_ricerca.getitemstring(1, 		"flag_evasione")
ls_flag_blocco = dw_tes_ord_ven_ricerca.getitemstring(1, "flag_blocco")

// -------------  compongo SQL con filtri ---------------------
is_sql_filtro = ""

if not isnull(ll_anno_registrazione) and ll_anno_registrazione>0 then
	is_sql_filtro += " and tes_ord_ven.anno_registrazione = " + string(ll_anno_registrazione) + " "
end if
if not isnull(ll_num_registrazione) and ll_num_registrazione>0 then
	is_sql_filtro += " and tes_ord_ven.num_registrazione = " + string(ll_num_registrazione) + " "
end if
if not isnull(ls_cod_cliente) and ls_cod_cliente<>"" then
	is_sql_filtro += " and tes_ord_ven.cod_cliente = '" + ls_cod_cliente + "' "
end if
if not isnull(ls_tipo_ordine) and ls_tipo_ordine<>"" then
	is_sql_filtro += " and tes_ord_ven.cod_tipo_ord_ven = '" + ls_tipo_ordine + "' "
end if
if not isnull(ls_cod_valuta) and ls_cod_valuta<>"" then
	is_sql_filtro += " and tes_ord_ven.cod_valuta = '" + ls_cod_valuta + "' "
end if
if not isnull(ldt_data_registrazione_inizio) then
	is_sql_filtro += " and tes_ord_ven.data_registrazione >= '" + string(ldt_data_registrazione_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_data_registrazione_fine) then
	is_sql_filtro += " and tes_ord_ven.data_registrazione <= '" + string(ldt_data_registrazione_fine, s_cs_xx.db_funzioni.formato_data) + "' "
end if
//if not isnull(ldt_data_pronto) then
//	is_sql_filtro += " and tes_ord_ven.data_pronto = '" + string(ldt_data_pronto, s_cs_xx.db_funzioni.formato_data) + "' "
//end if
if not isnull(ldt_data_consegna_inizio) then
	is_sql_filtro += " and tes_ord_ven.data_consegna >= '" + string(ldt_data_consegna_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_data_consegna_fine) then
	is_sql_filtro += " and tes_ord_ven.data_consegna <= '" + string(ldt_data_consegna_fine, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ls_cod_operatore) and ls_cod_operatore<>"" then
	is_sql_filtro += " and tes_ord_ven.cod_operatore = '" + ls_cod_operatore + "' "
end if
if not isnull(ls_cod_resa) and ls_cod_resa<>"" then
	is_sql_filtro += " and tes_ord_ven.cod_resa = '" + ls_cod_resa + "' "
end if
if not isnull(ls_rif_interscambio) and ls_rif_interscambio<>"" then
	is_sql_filtro += " and tes_ord_ven.rif_interscambio like '" + ls_rif_interscambio + "' "
end if
if not isnull(ls_cod_deposito) and ls_cod_deposito<>"" then
	is_sql_filtro += " and tes_ord_ven.cod_deposito = '" + ls_cod_deposito + "' "
end if
if not isnull(ls_cod_agente_1) and ls_cod_agente_1<>"" then
	is_sql_filtro += " and tes_ord_ven.cod_agente_1 = '" + ls_cod_agente_1 + "' "
end if
if not isnull(ls_num_ord_cliente) and ls_num_ord_cliente<>"" then
	is_sql_filtro += " and tes_ord_ven.num_ord_cliente like '" + ls_num_ord_cliente + "' "
end if
if not isnull(ls_flag_blocco) and ls_flag_blocco <> "X" then
	is_sql_filtro += " and tes_ord_ven.flag_blocco='" + ls_flag_blocco + "' "
end if

choose case ls_flag_evasione
	case "A"
		is_sql_filtro += " and tes_ord_ven.flag_evasione = 'A' "
	case "P"
		is_sql_filtro += " and tes_ord_ven.flag_evasione = 'P' "
	case "E"
		is_sql_filtro += " and tes_ord_ven.flag_evasione = 'E' "
	case "R"
		is_sql_filtro += " and (tes_ord_ven.flag_evasione = 'A' or tes_ord_ven.flag_evasione = 'P') "
end choose

if not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
	//NOTA: mettere nella select la join con la det_ord_ven
	is_sql_filtro += " and det_ord_ven.cod_prodotto = '" + ls_cod_prodotto + "' "
end if

// ------------------------------------------------------------

wf_cancella_treeview()
wf_imposta_treeview()

tv_1.setfocus()

dw_folder_tv.fu_SelectTab(2)
dw_folder.fu_SelectTab(1)

dw_tes_ord_ven_det_1.change_dw_current()

end subroutine

public subroutine wf_annulla ();string				ls_null
long				ll_null
datetime			ldt_null

setnull(ls_null)
setnull(ll_null)
setnull(ldt_null)

dw_tes_ord_ven_ricerca.setitem(1, 		"ologramma",			"")

dw_tes_ord_ven_ricerca.setitem(1,		 "anno_registrazione",		ll_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"num_registrazione",			ll_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"data_registrazione_d",		ldt_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"data_registrazione_a",		ldt_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"data_consegna_d",			ldt_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"data_consegna_a",			ldt_null)
//dw_tes_ord_ven_ricerca.setitem(1, 		"data_pronto_d",				ldt_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"cod_tipo_ord_ven",			ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"cod_valuta",					ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"cod_cliente",					ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"cod_prodotto",					ls_null)
dw_tes_ord_ven_ricerca.setitem(1,		"cod_operatore",				ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"cod_resa",						ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"rif_interscambio",				ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"num_ord_cliente",			ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"cod_deposito",					ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"cod_agente_1",				ls_null)
dw_tes_ord_ven_ricerca.setitem(1, 		"flag_evasione",				"A")
dw_tes_ord_ven_ricerca.setitem(1, 		"flag_visualizza_offerte",		"N")

dw_tes_ord_ven_ricerca.setitem(1, 		"flag_tipo_livello_1",			"N")
dw_tes_ord_ven_ricerca.setitem(1, 		"flag_tipo_livello_2",			"N")
dw_tes_ord_ven_ricerca.setitem(1, 		"flag_tipo_livello_3",			"N")

return
end subroutine

public function integer wf_inserisci_singolo_ordine (long fl_handle, long fl_anno, long fl_numero);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento singolo ordine
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

treeviewitem	ltvi_campo
string				ls_temp, ls_flag_visualizza_offerte, ls_flag_evasione, ls_cod_deposito, ls_messaggio, ls_flag_tipo_bcl, ls_evasione_olo[], ls_vuoto[]
ss_record		lstr_record
long				ll_risposta, ll_index, ll_num_ordine_olo[], ll_vuoto[]
integer			li_anno_ordine_olo[], li_vuoto[]


ls_flag_visualizza_offerte = dw_tes_ord_ven_ricerca.getitemstring(dw_tes_ord_ven_ricerca.getrow(), "flag_visualizza_offerte")

//aggiungere nella where il deposito del filtro, se impostato
ls_cod_deposito = dw_tes_ord_ven_ricerca.getitemstring(1, "cod_deposito")

if ls_cod_deposito<>"" and not isnull(ls_cod_deposito) then
	
	select 	tes_ord_ven.cod_azienda, tes_ord_ven.flag_evasione, tab_tipi_ord_ven.flag_tipo_bcl
	into   		:ls_temp, :ls_flag_evasione, :ls_flag_tipo_bcl
	from   tes_ord_ven
	join tab_tipi_ord_ven on tab_tipi_ord_ven.cod_azienda=tes_ord_ven.cod_azienda and
										   tab_tipi_ord_ven.cod_tipo_ord_ven=tes_ord_ven.cod_tipo_ord_ven
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
			 tes_ord_ven.anno_registrazione = :fl_anno and
			 tes_ord_ven.num_registrazione = :fl_numero and
			 tes_ord_ven.cod_deposito=:ls_cod_deposito;
	
	//eventuale messaggio se la select da 100
	ls_messaggio = "Ordine inesistente oppure appartenente ad un altro stabilimento!"
else
	
	select 	tes_ord_ven.cod_azienda, tes_ord_ven.flag_evasione, tab_tipi_ord_ven.flag_tipo_bcl
	into   		:ls_temp, :ls_flag_evasione, :ls_flag_tipo_bcl
	from   tes_ord_ven
	join tab_tipi_ord_ven on tab_tipi_ord_ven.cod_azienda=tes_ord_ven.cod_azienda and
										   tab_tipi_ord_ven.cod_tipo_ord_ven=tes_ord_ven.cod_tipo_ord_ven
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
			 tes_ord_ven.anno_registrazione = :fl_anno and
			 tes_ord_ven.num_registrazione = :fl_numero;
			 
	//eventuale messaggio se la select da 100
	ls_messaggio = "Ordine inesistente!"
end if

if sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA", ls_messaggio)
	return 0
end if

lstr_record.anno_registrazione = fl_anno
lstr_record.num_registrazione = fl_numero
lstr_record.codice = ""		
lstr_record.descrizione = ""
lstr_record.livello = 100
lstr_record.tipo_livello = "X"

ltvi_campo.label = 	string(fl_anno) + "/"+ string(fl_numero)

choose case ls_flag_evasione
	case "E"
		ll_index = 10
	case "P"
		ll_index = 9
	case else
		ll_index = 8
end choose

ltvi_campo.pictureindex = ll_index
ltvi_campo.selectedpictureindex = ll_index
ltvi_campo.overlaypictureindex = ll_index
ltvi_campo.data = lstr_record
ltvi_campo.selected = false

li_anno_ordine_olo[] = li_vuoto[]
ll_num_ordine_olo[] = ll_vuoto[]
ls_evasione_olo[] = ls_vuoto[]

//verifica se abbiamo ordini collegati tramite ologramma (prima o dopo nel tempo)
if ls_flag_tipo_bcl="A" then
	ltvi_campo.children = wf_conta_ologrammi(fl_anno, fl_numero, li_anno_ordine_olo[], ll_num_ordine_olo[], ls_evasione_olo[])
	ltvi_campo.expandedonce = true
else
	ltvi_campo.children = false
end if

ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)

if upperbound(ll_num_ordine_olo[]) > 0 then
	wf_inserisci_ordini_ologramma(ll_risposta, fl_anno, fl_numero, li_anno_ordine_olo[], ll_num_ordine_olo[], ls_evasione_olo[])
end if



return 0
end function

public function integer wf_leggi_parent (long fl_handle, ref string fs_sql);ss_record lstr_item
long	ll_item
treeviewitem ltv_item

ll_item = fl_handle

if ll_item = 0 then
	return 0
end if

do
	tv_1.getitem(ll_item,ltv_item)
	lstr_item = ltv_item.data
	
	choose case lstr_item.tipo_livello		
		case "T"
			fs_sql += " and tes_ord_ven.cod_tipo_ord_ven = '" + lstr_item.codice + "' "
		case "A"
			fs_sql += " and tes_ord_ven.anno_registrazione = " + lstr_item.codice + " "
		case "C"
			fs_sql += " and tes_ord_ven.cod_cliente = '" + lstr_item.codice + "' "	
		case "P"
			fs_sql += " and det_ord_ven.cod_prodotto = '" + lstr_item.codice + "' "	
		case "O"
			fs_sql += " and tes_ord_ven.cod_operatore = '" + lstr_item.codice + "' "	
		case "D"
			fs_sql += " and tes_ord_ven.cod_deposito = '" + lstr_item.codice + "' "	
		case "R"
			fs_sql += " and tes_ord_ven.data_registrazione = '" + lstr_item.codice + "' "	
		case "S"
			fs_sql += " and tes_ord_ven.data_consegna = '" + lstr_item.codice + "' "	
			
		case "N"
			
	end choose
	
	ll_item = tv_1.finditem(parenttreeitem!,ll_item)
	
loop while ll_item <> -1

return 0
end function

public subroutine wf_imposta_immagini_tv ();string ls_path

//datawindow offerte vendita ##############################################
ls_path = s_cs_xx.volume + s_cs_xx.risorse + "\treeview\documento_grigio.png"
dw_tes_off_ven.object.p_tes_off_ven.FileName = ls_path

ls_path = s_cs_xx.volume + s_cs_xx.risorse + "\treeview\prodotto.png"
dw_tes_off_ven.object.p_det_off_ven.FileName = ls_path


//treeview ordini vendita #################################################
//immagini tree
tv_1.deletepictures()

//1 cliente
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\cliente.png")

//2 operatore
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\operatore.png")

//3 deposito
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\deposito.png")

//4 anno registrazione
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\anno.png")

//5 data registrazione
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\data_1.png")

//6 data consegna
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\data_2.png")

//7 tipo ordine
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\tipo_documento.png")

//8,9,10 ordine aperto, parziale, evaso
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\documento_verde.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\documento_giallo.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\documento_rosso.png")

//11 offerte di vendita di un item ordine
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\documento_grigio.png")

//12 prodotto
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\prodotto.png")


//13, 14, 15, 16, 17, 18 ordine aperto, parziale, evaso COLLEGATO MEDIANTE OLOGRAMMA
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\olo_verde_p.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\olo_giallo_p.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\olo_rosso_p.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\olo_verde_s.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\olo_giallo_s.png")
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "treeview\olo_rosso_s.png")





end subroutine

public function integer wf_inserisci_depositi (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello depositi
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean  			lb_dati = false
string    			ls_sql, ls_cod_deposito, ls_des_deposito, ls_sql_livello
long      			ll_risposta, ll_livello
ss_record 		lstr_record
treeviewitem	ltvi_campo

setpointer(Hourglass!)

ll_livello = il_livello_corrente + 1

ls_sql = "select distinct tes_ord_ven.cod_deposito, anag_depositi.des_deposito "+&
			"from tes_ord_ven "+&
			"join anag_depositi on anag_depositi.cod_azienda=tes_ord_ven.cod_azienda and "+&
										"anag_depositi.cod_deposito=tes_ord_ven.cod_deposito "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if

ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "


if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by tes_ord_ven.cod_deposito asc "

declare cu_depositi dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_depositi;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_depositi (wf_inserisci_depositi): " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	setpointer(Arrow!)
	close cu_depositi;
	return 0
end if

do while 1=1
   fetch cu_depositi into :ls_cod_deposito, 
							:ls_des_deposito;
									
   if (sqlca.sqlcode = 100) then
		close cu_depositi;
		if lb_dati then
			setpointer(Arrow!)
			return 0
		end if
		setpointer(Arrow!)
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_depositi (wf_inserisci_depositi)~r~n" + sqlca.sqlerrtext)
		setpointer(Arrow!)
		close cu_depositi;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_deposito
	lstr_record.descrizione = ls_des_deposito
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "D"
	ltvi_campo.data = lstr_record
	ltvi_campo.label = ls_cod_deposito + " - " + ls_des_deposito

	ltvi_campo.pictureindex = 3
	ltvi_campo.selectedpictureindex = 3
	ltvi_campo.overlaypictureindex = 3
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento deposito nel TREEVIEW al livello Depositi")
		setpointer(Arrow!)
		close cu_depositi;
		return 0
	end if

loop
close cu_depositi;
setpointer(Arrow!)

return 0
end function

public function integer wf_inserisci_anni (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello anno fattura vendita
//		return -1 = errore
//		return  1 = tutto OK dati caricati
//		return  0 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean   lb_dati = false
string    ls_sql, ls_sql_livello
long      ll_risposta, ll_livello, ll_anno
ss_record lstr_record
treeviewitem ltvi_campo


ll_livello = il_livello_corrente + 1

ls_sql = "select distinct tes_ord_ven.anno_registrazione "+&
			"from tes_ord_ven "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if
			
ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by tes_ord_ven.anno_registrazione desc "

declare cu_anni dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_anni;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_anni (wf_inserisci_anni): " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_anni;
	return -1
end if

do while 1=1
   fetch cu_anni into :ll_anno;
									
   if (sqlca.sqlcode = 100) then
		close cu_anni;
		if lb_dati then return 1
		return 0
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_anni (wf_inserisci_anni)~r~n" + sqlca.sqlerrtext)
		close cu_anni;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = string(ll_anno)
	lstr_record.descrizione = ""
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "A"
	
	ltvi_campo.data = lstr_record
	ltvi_campo.label = "Anno "+string(ll_anno)

	ltvi_campo.pictureindex = 4
	ltvi_campo.selectedpictureindex = 4
	ltvi_campo.overlaypictureindex = 4
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento anno nel TREEVIEW al livello ANNO ORDINE VENDITA")
		close cu_anni;
		return -1
	end if

loop
close cu_anni;

return 1

end function

public function integer wf_inserisci_clienti (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello clienti
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean  			lb_dati = false
string    			ls_sql, ls_cod_cliente, ls_des_cliente, ls_sql_livello
long      			ll_risposta, ll_livello
ss_record 		lstr_record
treeviewitem	ltvi_campo

setpointer(Hourglass!)

ll_livello = il_livello_corrente + 1

ls_sql = "select distinct tes_ord_ven.cod_cliente, anag_clienti.rag_soc_1 "+&
			"from tes_ord_ven "+&
			"join anag_clienti on anag_clienti.cod_azienda=tes_ord_ven.cod_azienda and "+&
										"anag_clienti.cod_cliente=tes_ord_ven.cod_cliente "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if

ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by anag_clienti.rag_soc_1 asc "

declare cu_clienti dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_clienti;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_clienti (wf_inserisci_clienti): " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	setpointer(Arrow!)
	close cu_clienti;
	return 0
end if

do while 1=1
   fetch cu_clienti into :ls_cod_cliente, 
							:ls_des_cliente;
									
   if (sqlca.sqlcode = 100) then
		close cu_clienti;
		if lb_dati then
			setpointer(Arrow!)
			return 0
		end if
		setpointer(Arrow!)
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_clienti (wf_inserisci_clienti)~r~n" + sqlca.sqlerrtext)
		setpointer(Arrow!)
		close cu_clienti;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_cliente
	lstr_record.descrizione = ls_des_cliente
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "C"
	ltvi_campo.data = lstr_record
	ltvi_campo.label = ls_cod_cliente + " - " + ls_des_cliente

	ltvi_campo.pictureindex = 1
	ltvi_campo.selectedpictureindex = 1
	ltvi_campo.overlaypictureindex = 1
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento cliente nel TREEVIEW al livello CLIENTI")
		setpointer(Arrow!)
		close cu_clienti;
		return 0
	end if

loop
close cu_clienti;
setpointer(Arrow!)

return 0
end function

public function integer wf_inserisci_data_consegna (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello data consegna ordine
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean   lb_dati = false
string    ls_sql, ls_sql_livello, ls_data_consegna
long      ll_risposta, ll_livello
datetime ldt_data_consegna
ss_record lstr_record
treeviewitem ltvi_campo


ll_livello = il_livello_corrente + 1

ls_sql = "select distinct tes_ord_ven.data_consegna "+&
			"from tes_ord_ven "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if

ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by tes_ord_ven.data_consegna desc "

declare cu_date dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_date;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_date (wf_inserisci_data_consegna): " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_date;
	return 0
end if

do while 1=1
   fetch cu_date into :ldt_data_consegna;
									
   if (sqlca.sqlcode = 100) then
		close cu_date;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_date (wf_inserisci_data_consegna)~r~n" + sqlca.sqlerrtext)
		close cu_date;
		return -1
	end if
	
	ls_data_consegna = string(ldt_data_consegna, s_cs_xx.db_funzioni.formato_data)
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_data_consegna
	lstr_record.descrizione = ""
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "S"
	
	ltvi_campo.data = lstr_record
	ltvi_campo.label = "Data Part. "+string(ldt_data_consegna, "dd/mm/yyyy")

	ltvi_campo.pictureindex = 6
	ltvi_campo.selectedpictureindex = 6
	ltvi_campo.overlaypictureindex = 6
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.error("Errore","Errore in inserimento data consegna nel TREEVIEW al livello DATA CONS. ORDINE VENDITA")
		close cu_date;
		return 0
	end if

loop
close cu_date;

return 0

end function

public function integer wf_inserisci_data_registrazione (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello data registrazione ordine
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean   lb_dati = false
string    ls_sql, ls_sql_livello, ls_data_registrazione
long      ll_risposta, ll_livello
datetime ldt_data_registrazione
ss_record lstr_record
treeviewitem ltvi_campo


ll_livello = il_livello_corrente + 1

ls_sql = "select distinct tes_ord_ven.data_registrazione "+&
			"from tes_ord_ven "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if
			
ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by tes_ord_ven.data_registrazione desc "

declare cu_date dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_date;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_date (wf_inserisci_data_registrazione): " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_date;
	return 0
end if

do while 1=1
   fetch cu_date into :ldt_data_registrazione;
									
   if (sqlca.sqlcode = 100) then
		close cu_date;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_date (wf_inserisci_data_registrazione)~r~n" + sqlca.sqlerrtext)
		close cu_date;
		return -1
	end if
	
	ls_data_registrazione = string(ldt_data_registrazione, s_cs_xx.db_funzioni.formato_data)
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_data_registrazione
	lstr_record.descrizione = ""
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "R"
	
	ltvi_campo.data = lstr_record
	ltvi_campo.label = "Data Reg. "+string(ldt_data_registrazione, "dd/mm/yyyy")

	ltvi_campo.pictureindex = 5
	ltvi_campo.selectedpictureindex = 5
	ltvi_campo.overlaypictureindex = 5
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.error("Errore","Errore in inserimento data registrazione nel TREEVIEW al livello DATA REG. ORDINE VENDITA")
		close cu_date;
		return 0
	end if

loop
close cu_date;

return 0

end function

public function integer wf_inserisci_operatori (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello operatori
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean  			lb_dati = false
string    			ls_sql, ls_cod_operatore, ls_des_operatore, ls_sql_livello
long      			ll_risposta, ll_livello
ss_record 		lstr_record
treeviewitem	ltvi_campo

setpointer(Hourglass!)

ll_livello = il_livello_corrente + 1

ls_sql = "select distinct tes_ord_ven.cod_operatore, tab_operatori.des_operatore "+&
			"from tes_ord_ven "+&
			"join tab_operatori on tab_operatori.cod_azienda=tes_ord_ven.cod_azienda and "+&
										"tab_operatori.cod_operatore=tes_ord_ven.cod_operatore "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if
										
ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by tab_operatori.des_operatore asc "

declare cu_operatori dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_operatori;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_operatori (wf_inserisci_operatori): " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	setpointer(Arrow!)
	close cu_operatori;
	return 0
end if

do while 1=1
   fetch cu_operatori into :ls_cod_operatore, 
							:ls_des_operatore;
									
   if (sqlca.sqlcode = 100) then
		close cu_operatori;
		if lb_dati then
			setpointer(Arrow!)
			return 0
		end if
		setpointer(Arrow!)
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_operatori (wf_inserisci_operatori)~r~n" + sqlca.sqlerrtext)
		setpointer(Arrow!)
		close cu_operatori;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_operatore
	lstr_record.descrizione = ls_des_operatore
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "O"
	ltvi_campo.data = lstr_record
	ltvi_campo.label = ls_cod_operatore + " - " + ls_des_operatore

	ltvi_campo.pictureindex = 2
	ltvi_campo.selectedpictureindex = 2
	ltvi_campo.overlaypictureindex = 2
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento operatore nel TREEVIEW al livello Operatori")
		setpointer(Arrow!)
		close cu_operatori;
		return 0
	end if

loop
close cu_operatori;
setpointer(Arrow!)

return 0
end function

public function integer wf_inserisci_ordini (long fl_handle);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento ordini sul tree
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean			ib_max, lb_dati = false
string			ls_sql, ls_sql_livello, ls_flag_visualizza_offerte, ls_flag_evasione, ls_flag_tipo_bcl, ls_evasione_olo[], ls_vuoto[], ls_cod_valuta, ls_cod_cliente, ls_rag_soc_1
integer			li_anno_ordine_olo[], li_vuoto[]
long				ll_livello, ll_count, ll_max_count, ll_anno_registrazione, ll_num_registrazione, ll_risposta, ll_index, ll_ret_offerte, ll_num_ordine_olo[], ll_vuoto[], &
					ll_index2
dec{4}			ld_imponibile_iva_valuta
ss_record		lstr_record
treeviewitem	ltvi_campo


ll_livello = il_livello_corrente
ll_count = 0
ll_max_count = 500
ib_max = false
ls_flag_visualizza_offerte = dw_tes_ord_ven_ricerca.getitemstring(dw_tes_ord_ven_ricerca.getrow(), "flag_visualizza_offerte")

setpointer(Hourglass!)

declare cu_items dynamic cursor for sqlsa;
ls_sql = "select distinct 	 tes_ord_ven.anno_registrazione," +&
         						"tes_ord_ven.num_registrazione,"+&
								"tes_ord_ven.flag_evasione,"+&
								"tes_ord_ven.imponibile_iva_valuta,"+&
								"tes_ord_ven.cod_valuta,"+&
								"tes_ord_ven.cod_cliente,"+&
								"tp.flag_tipo_bcl, "+&
								"cl.rag_soc_1 "+&
			"from   tes_ord_ven "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if

//aggiungo questo pezzo per verificare successivamente se sono del tipo Report 1
//Specifica Ologrammi
ls_sql += " join tab_tipi_ord_ven as tp on tp.cod_azienda=tes_ord_ven.cod_azienda and "+&
												" tp.cod_tipo_ord_ven=tes_ord_ven.cod_tipo_ord_ven "
			
ls_sql += " join anag_clienti as cl on cl.cod_azienda=tes_ord_ven.cod_azienda and "+&
												" cl.cod_cliente=tes_ord_ven.cod_cliente "

ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

if len(trim(ls_sql_livello)) > 0 then
	ls_sql +=ls_sql_livello
end if

ls_sql = ls_sql + " order by tes_ord_ven.anno_registrazione, tes_ord_ven.num_registrazione "

prepare sqlsa from :ls_sql;
open dynamic cu_items;

if sqlca.sqlcode <> 0 then
	g_mb.error("OMNIA","Errore in OPEN cursore cu_items (wf_inserisci_ordini)~r~n" + sqlca.sqlerrtext)
	close cu_items;
	setpointer(Arrow!)
	return -1
end if

do while true
	
	fetch cu_items into :ll_anno_registrazione, 
	                            :ll_num_registrazione,
							 :ls_flag_evasione,
							 :ld_imponibile_iva_valuta,
							 :ls_cod_valuta,
							 :ls_cod_cliente,
							 :ls_flag_tipo_bcl,
							 :ls_rag_soc_1;

   if (sqlca.sqlcode = 100) then
		close cu_items;
		setpointer(Arrow!)
		if lb_dati then return 0
		return 1
	end if
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_items (wf_inserisci_ordini)~r~n" + sqlca.sqlerrtext)
		close cu_items;
		setpointer(Arrow!)
		return -1
	end if
	
	ll_count += 1
	lb_dati = true
		
	lstr_record.anno_registrazione = ll_anno_registrazione	
	lstr_record.num_registrazione = ll_num_registrazione
	lstr_record.codice = ""		
	lstr_record.descrizione = ""
	lstr_record.livello = 100
	lstr_record.tipo_livello = "X"	
	ltvi_campo.data = lstr_record
	
	if isnull(ld_imponibile_iva_valuta) then ld_imponibile_iva_valuta = 0
	if isnull(ls_rag_soc_1) then ls_rag_soc_1 = ""
	if isnull(ls_cod_valuta) then ls_cod_valuta = ""
	
	ltvi_campo.label = string(ll_anno_registrazione) + "/"+ string(ll_num_registrazione) + "  " + ls_cod_valuta + " " + string(ld_imponibile_iva_valuta,"###,###,##0.00##") + " (" + ls_rag_soc_1 + ")"
	
	choose case ls_flag_evasione
		case "E"
			ll_index = 10
		case "P"
			ll_index = 9
		case else
			ll_index = 8
	end choose
	
	ltvi_campo.pictureindex = ll_index
	ltvi_campo.selectedpictureindex = ll_index
	ltvi_campo.overlaypictureindex = ll_index
	ltvi_campo.selected = false
	
	
	li_anno_ordine_olo[] = li_vuoto[]
	ll_num_ordine_olo[] = ll_vuoto[]
	ls_evasione_olo[] = ls_vuoto[]
	
	//verifica se abbiamo ordini collegati tramite ologramma (prima o dopo nel tempo)
	if ls_flag_tipo_bcl = "A" then
		ltvi_campo.children = wf_conta_ologrammi(ll_anno_registrazione, ll_num_registrazione,  li_anno_ordine_olo[], ll_num_ordine_olo[], ls_evasione_olo[])
		ltvi_campo.expandedonce = true
	else
		ltvi_campo.children = false
	end if
	
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	
	if upperbound(ll_num_ordine_olo[]) > 0 then
		wf_inserisci_ordini_ologramma(ll_risposta, ll_anno_registrazione, ll_num_registrazione, li_anno_ordine_olo[], ll_num_ordine_olo[], ls_evasione_olo[])
	end if

	if ll_count > 500 and not ib_max then
		ib_max = true
		if g_mb.messagebox("APICE","Attenzione troppi dati caricati nell'albero degli ordini. Continuare?" + & 
					  "(Si consiglia di caricare annullare scegliendo NO, impostare qualche altro elemento nel filtro e riprovare.)",Question!, YesNo!,2)=2 then
			close cu_items;
			setpointer(Arrow!)
			return 0
		end if
	end if
loop

close cu_items;
setpointer(Arrow!)
return 0
end function

public function integer wf_inserisci_tipi_ordini (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello tipi ordine
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean					lb_dati = false
string						ls_sql, ls_cod_tipo_ord_ven, ls_des_tipo_ord_ven, ls_sql_livello
ss_record				lstr_record
treeviewitem			ltvi_campo
long						ll_livello,ll_risposta

ll_livello = il_livello_corrente + 1

ls_sql =  "select distinct tes_ord_ven.cod_tipo_ord_ven, tab_tipi_ord_ven.des_tipo_ord_ven "+&
			"from tes_ord_ven "+&
			"join tab_tipi_ord_ven on tab_tipi_ord_ven.cod_azienda=tes_ord_ven.cod_azienda and "+&
										"tab_tipi_ord_ven.cod_tipo_ord_ven=tes_ord_ven.cod_tipo_ord_ven "

ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if pos(is_sql_filtro, "det_ord_ven.cod_prodotto")>0 or pos(ls_sql_livello, "det_ord_ven.cod_prodotto")>0 or &
		wf_liv_prodotto_is_present() then
		
	ls_sql += is_join_det_ord_ven
end if
										
ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by tes_ord_ven.cod_tipo_ord_ven asc "

declare cu_tipi_ord_ven dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_tipi_ord_ven;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_tipi_ord_ven: " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_tipi_ord_ven;
	return 0
end if

do while 1=1
   fetch cu_tipi_ord_ven into 	:ls_cod_tipo_ord_ven, 
								   		:ls_des_tipo_ord_ven;
									
   if (sqlca.sqlcode = 100) then
		close cu_tipi_ord_ven;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_tipi_ord_ven (wf_inserisci_tipi_ordini)~r~n" + sqlca.sqlerrtext)
		close cu_tipi_ord_ven;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_tipo_ord_ven
	lstr_record.descrizione = ls_des_tipo_ord_ven
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "T"
	ltvi_campo.data = lstr_record
	ltvi_campo.label = ls_cod_tipo_ord_ven + " -  " +  ls_des_tipo_ord_ven

	ltvi_campo.pictureindex = 7
	ltvi_campo.selectedpictureindex = 7
	ltvi_campo.overlaypictureindex = 7
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento tipo ordine vendita nel TREEVIEW al livello TIPO ORDINE VENDITA")
		close cu_tipi_ord_ven;
		return 0
	end if

loop
close cu_tipi_ord_ven;

ll_risposta = tv_1.FindItem(RootTreeItem! , 0)
if ll_risposta > 0 then tv_1.ExpandItem(ll_risposta)

return 0

end function

public function integer wf_inserisci_prodotti (long fl_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello prodotti
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean  			lb_dati = false
string    			ls_sql, ls_cod_prodotto, ls_des_prodotto, ls_sql_livello
long      			ll_risposta, ll_livello
ss_record 		lstr_record
treeviewitem	ltvi_campo

setpointer(Hourglass!)

ll_livello = il_livello_corrente + 1

ls_sql = "select distinct det_ord_ven.cod_prodotto, anag_prodotti.des_prodotto "+&
			"from tes_ord_ven "

//metto sempre la join con la det_ord_ven
ls_sql += is_join_det_ord_ven
			
ls_sql += "join anag_prodotti on anag_prodotti.cod_azienda=tes_ord_ven.cod_azienda and "+&
										"anag_prodotti.cod_prodotto=det_ord_ven.cod_prodotto "

ls_sql += "where tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "
			
ls_sql_livello = ""
wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += ls_sql_livello
end if

if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if

ls_sql = ls_sql + "order by det_ord_ven.cod_prodotto asc "

declare cu_prodotti dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_prodotti;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_prodotti (wf_inserisci_prodotti): " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	setpointer(Arrow!)
	close cu_prodotti;
	return 0
end if

do while 1=1
   fetch cu_prodotti into :ls_cod_prodotto, 
							:ls_des_prodotto;
									
   if (sqlca.sqlcode = 100) then
		close cu_prodotti;
		if lb_dati then
			setpointer(Arrow!)
			return 0
		end if
		setpointer(Arrow!)
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_prodotti (wf_inserisci_prodotti)~r~n" + sqlca.sqlerrtext)
		setpointer(Arrow!)
		close cu_prodotti;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_prodotto
	lstr_record.descrizione = ls_des_prodotto
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	lstr_record.tipo_livello = "P"
	ltvi_campo.data = lstr_record
	ltvi_campo.label = ls_cod_prodotto + " - " + ls_des_prodotto

	ltvi_campo.pictureindex = 12
	ltvi_campo.selectedpictureindex = 12
	ltvi_campo.overlaypictureindex = 12
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento prodotti nel TREEVIEW al livello Prodotti")
		setpointer(Arrow!)
		close cu_prodotti;
		return 0
	end if

loop
close cu_prodotti;
setpointer(Arrow!)

return 0
end function

public subroutine wf_memorizza_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero,ll_cont
datetime ldt_data

if g_mb.messagebox("Omnia","Memorizza l'attuale impostazione dei filtro come predefinito?",Question!,YesNo!,2) = 2 then return

ls_colcount = dw_tes_ord_ven_ricerca.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""
for ll_i = 1 to ll_colonne
	ls_nome_colonna = dw_tes_ord_ven_ricerca.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_tes_ord_ven_ricerca.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		ldt_data = dw_tes_ord_ven_ricerca.getitemdatetime(dw_tes_ord_ven_ricerca.getrow(), ls_nome_colonna)
		if isnull(ldt_data) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ldt_data, "dd/mm/yyyy") + "~t"
		end if
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		ls_stringa = dw_tes_ord_ven_ricerca.getitemstring(dw_tes_ord_ven_ricerca.getrow(), ls_nome_colonna)
		if isnull(ls_stringa) then
			ls_memo += "NULL~t"
		else
			ls_memo += ls_stringa + "~t"
		end if
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		ll_numero = dw_tes_ord_ven_ricerca.getitemnumber(dw_tes_ord_ven_ricerca.getrow(), ls_nome_colonna)
		if isnull(ll_numero) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ll_numero) + "~t"
		end if
	end if

next

select count(*)
into   :ll_cont
from   filtri_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'ORDVEN';
		
if ll_cont > 0 then
	update filtri_manutenzioni
	set filtri = :ls_memo
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente =  :ls_cod_utente and
		   tipo_filtro = 'ORDVEN';
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
else
	insert into filtri_manutenzioni
		(cod_azienda,
		 cod_utente,
		 filtri,
		 tipo_filtro)
	 values
		 (:s_cs_xx.cod_azienda,
		  :ls_cod_utente,
		  :ls_memo,
		  'ORDVEN');
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

commit;

return
end subroutine

public subroutine wf_leggi_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero
datetime ldt_data

ls_colcount = dw_tes_ord_ven_ricerca.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""

select filtri 
into   :ls_memo
from   filtri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'ORDVEN';
if sqlca.sqlcode = 100 then
	//g_mb.messagebox("OMNIA", "Nessun filtro memorizzato.")
	return
end if
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in lettura impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
	return
end if

for ll_i = 1 to ll_colonne
	
	ls_stringa = mid(ls_memo,1, pos(ls_memo, "~t") - 1)
	ls_memo = mid(ls_memo, pos(ls_memo, "~t") + 1)
	
	ls_nome_colonna = dw_tes_ord_ven_ricerca.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_tes_ord_ven_ricerca.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		if ls_stringa = "NULL" then
			setnull(ldt_data)
		else
			ldt_data = datetime(date(ls_stringa), 00:00:00)
		end if
		dw_tes_ord_ven_ricerca.setitem(dw_tes_ord_ven_ricerca.getrow(),ls_nome_colonna, ldt_data)
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		if ls_stringa = "NULL" then
			setnull(ls_stringa)
		end if
		dw_tes_ord_ven_ricerca.setitem(dw_tes_ord_ven_ricerca.getrow(),ls_nome_colonna, ls_stringa)
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		if ls_stringa = "NULL" then
			setnull(ll_numero)
		else
			ll_numero = long(ls_stringa)
		end if
		dw_tes_ord_ven_ricerca.setitem(dw_tes_ord_ven_ricerca.getrow(),ls_nome_colonna, ll_numero)
	end if

next

return
end subroutine

public subroutine wf_predefinito ();wf_leggi_filtro()
end subroutine

public subroutine wf_imposta_predefinito ();
dw_tes_ord_ven_ricerca.accepttext()
wf_memorizza_filtro()
end subroutine

public function boolean wf_liv_prodotto_is_present ();long ll_index, ll_max_index
string ls_flag_tipo_livello

ll_max_index = 3

for ll_index = 1 to ll_max_index
	ls_flag_tipo_livello = ""
	ls_flag_tipo_livello = dw_tes_ord_ven_ricerca.getitemstring(dw_tes_ord_ven_ricerca.getrow(), "flag_tipo_livello_"+string(ll_index))
	
	if ls_flag_tipo_livello= "P" then
		//è stato specificato uno dei livelli come Prodotto
		return true
	end if
	
next

return false
end function

public function integer wf_retrieve_offerte_vendita (long fl_anno_reg_ordine, long fl_num_reg_ordine);long ll_tot
string ls_flag_visualizza_offerte

ls_flag_visualizza_offerte = dw_tes_ord_ven_ricerca.getitemstring(dw_tes_ord_ven_ricerca.getrow(), "flag_visualizza_offerte")

if ls_flag_visualizza_offerte<> "S" then return 0

ll_tot = dw_tes_off_ven.retrieve(s_cs_xx.cod_azienda, fl_anno_reg_ordine, fl_num_reg_ordine)

return ll_tot
end function

public function integer wf_apri_offerta (long fl_anno_offerta, long fl_num_offerta);long 	 ll_righe_dett

s_cs_xx.parametri.parametro_d_1 = fl_anno_offerta
s_cs_xx.parametri.parametro_d_2 = fl_num_offerta

select count(*)
into   :ll_righe_dett
from   det_off_ven
where  cod_azienda        = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_offerta and
		 num_registrazione  = :fl_num_offerta;

if ll_righe_dett = 0 or isnull(ll_righe_dett) then
	g_mb.messagebox("Stampa Offerta Vendita","Nessun dettaglio presente in questa offerta: impossibile stampare!",StopSign!)
	return 0
end if

window_open(w_report_off_ven_euro,-1)

return 1
end function

public function longlong wf_get_prog_sessione ();longlong			ll_id
string				ls_temp

//numero 		es.			2012011109452235 (fino ai millesecondi con due cifre)
ls_temp = string(today(), "yyyymmdd") + string(now(), "hhmmssff")
ll_id = longlong(ls_temp)

return ll_id
end function

public function integer wf_riapri_offerta (integer fi_anno_reg_ord_ven, long fl_num_reg_ord_ven, ref string fs_errore);//Donato 23/05/2012
//chiesto da Beatrice

//questa funzione viene chiamata in cancellazione della testata di un ordine
//se le righe dell'ordine hanno un riferimento ad un'oferta allora devo riaprire l'offerta

integer li_anno_offerta
long ll_num_offerta
string ls_flag_evasione
	

// ciclo le righe dell'ordine che sto cancellando per verificare se c'è qualche offerta collegata
//che quindi verrà riaperta
declare 	cu_righe_x_offerta cursor for  
select distinct		anno_registrazione_off,
						num_registrazione_off
from 	det_ord_ven
where  	cod_azienda = :s_cs_xx.cod_azienda  and  
			anno_registrazione = :fi_anno_reg_ord_ven  and  
			num_registrazione = :fl_num_reg_ord_ven;

open cu_righe_x_offerta;
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in OPEN cursore 'cu_righe cu_righe_x_offerta'~r~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_righe_x_offerta into 	:li_anno_offerta,
											:ll_num_offerta;
	if sqlca.sqlcode < 0 then
		close cu_righe_x_offerta;
		fs_errore = "Errore in FETCH cursore 'cu_righe_x_offerta'~r~n" + sqlca.sqlerrtext
		return -1
	end if
	if sqlca.sqlcode = 100 then exit
	
	//leggo lo stato evasione dell'offerta collegata
	select flag_evasione
	into :ls_flag_evasione
	from tes_off_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_offerta and
				num_registrazione=:ll_num_offerta;
	
	if ls_flag_evasione<>"A" then
		
		//riapro l'offerta
		update tes_off_ven
		set flag_evasione='A'
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:li_anno_offerta and
					num_registrazione=:ll_num_offerta;
		
		if sqlca.sqlcode < 0 then
			close cu_righe_x_offerta;
			fs_errore = "Errore in riapertura offerta "+string(li_anno_offerta)+"/"+string(ll_num_offerta)+": " + sqlca.sqlerrtext
			return -1
		end if
		
	end if
	
loop

close cu_righe_x_offerta;
//--------------------------------------------------------------------------------------------------------------------------------------------

return 0
end function

public function boolean wf_conta_ologrammi (integer fi_anno_ord_ven, long fl_num_ord_ven, ref integer fi_anno_ordini[], ref long fl_numero_ordini[], ref string fs_evasione[]);long			ll_count, ll_numero_cu
string			ls_ordine, ls_flag_evasione_cu
integer		li_anno_cu

ls_ordine = string(fi_anno_ord_ven) + right("000000" + string(fl_num_ord_ven) , 6)

//vedo se ci sono ordini con ologramma uguale ad almeno una riga dell'ordine corrente (escludendo ovviamente l'ordine corrente)
select count(*)
into :ll_count
from det_ord_ven_ologramma
join tes_ord_ven on	tes_ord_ven.cod_azienda=det_ord_ven_ologramma.cod_azienda and
							tes_ord_ven.anno_registrazione=det_ord_ven_ologramma.anno_registrazione and
							tes_ord_ven.num_registrazione=det_ord_ven_ologramma.num_registrazione
where det_ord_ven_ologramma.cod_azienda=:s_cs_xx.cod_azienda and
		upper(det_ord_ven_ologramma.cod_ologramma) in (	select upper(cod_ologramma)
																	from det_ord_ven_ologramma
																	where 	cod_azienda=:s_cs_xx.cod_azienda and
																				anno_registrazione=:fi_anno_ord_ven and
																				num_registrazione=:fl_num_ord_ven
																) and
		cast(tes_ord_ven.anno_registrazione as varchar(4)) + right('000000' + cast(tes_ord_ven.num_registrazione as varchar(6)) , 6) <> :ls_ordine;

if ll_count>0 then
else
	return false
end if


//------------------------------------------------
declare cu_ologramma cursor for
	select distinct tes_ord_ven.anno_registrazione, tes_ord_ven.num_registrazione, tes_ord_ven.flag_evasione
	from det_ord_ven_ologramma
	join tes_ord_ven on	tes_ord_ven.cod_azienda=det_ord_ven_ologramma.cod_azienda and
								tes_ord_ven.anno_registrazione=det_ord_ven_ologramma.anno_registrazione and
								tes_ord_ven.num_registrazione=det_ord_ven_ologramma.num_registrazione
	where det_ord_ven_ologramma.cod_azienda=:s_cs_xx.cod_azienda and
			upper(det_ord_ven_ologramma.cod_ologramma) in (	select upper(cod_ologramma)
																		from det_ord_ven_ologramma
																		where 	cod_azienda=:s_cs_xx.cod_azienda and
																					anno_registrazione=:fi_anno_ord_ven and
																					num_registrazione=:fl_num_ord_ven
																	) and
			cast(tes_ord_ven.anno_registrazione as varchar(4)) + right('000000' + cast(tes_ord_ven.num_registrazione as varchar(6)) , 6) <> :ls_ordine;

//------------------------------------------------
open cu_ologramma;

if sqlca.sqlcode < 0 then
	return false
end if

ll_count = 0
//-----------------------------------------------
do while true
	
	fetch cu_ologramma into :li_anno_cu, :ll_numero_cu, :ls_flag_evasione_cu;

	if sqlca.sqlcode < 0 then
		close cu_ologramma;
		return false
	end if
	
	if sqlca.sqlcode = 100 then exit

	//procedi
	if ll_numero_cu>0 then
		ll_count +=1
		
		fi_anno_ordini[ll_count]			= li_anno_cu
		fl_numero_ordini[ll_count]		= ll_numero_cu
		fs_evasione[ll_count]				= ls_flag_evasione_cu
		
	end if

loop

close cu_ologramma;


return true
end function

public subroutine wf_inserisci_ordini_ologramma (long fl_handle, integer fi_anno, long fl_numero, integer fi_anno_ordine_olo[], long fl_num_ordine_olo[], string fs_evasione_olo[]);long				ll_index2
boolean			lb_precedente

//---------------------------------------------------------------------------------------------------------------------------
//inserisci eventuali altri nodi di ordini legati da stesso codice ologramma (prima o dopo nel tempo)
for ll_index2=1 to upperbound(fl_num_ordine_olo[])
	
	//variabili -----------------------------------
	ss_record			lstr_record
	long					ll_index
	treeviewitem		ltvi_campo
	//--------------------------------------------
	
	
	lstr_record.anno_registrazione = fi_anno_ordine_olo[ll_index2]
	lstr_record.num_registrazione = fl_num_ordine_olo[ll_index2]
	lstr_record.codice = ""		
	lstr_record.descrizione = ""
	lstr_record.livello = 100
	lstr_record.tipo_livello = "X"	
	ltvi_campo.data = lstr_record
	
	ltvi_campo.label = "(Olo) " + string(fi_anno_ordine_olo[ll_index2]) + "/"+ string(fl_num_ordine_olo[ll_index2])
	
	if fi_anno > fi_anno_ordine_olo[ll_index2] then
		//si tratta di un ordine PRECEDENTE collegato da ologramma
		lb_precedente = true
		
	elseif fi_anno < fi_anno_ordine_olo[ll_index2] then
		//si tratta di un ordine SUCCESSIVO collegato da ologramma
		lb_precedente = false
		
	else
		//verifica per numero ordine
		if fl_numero > fl_num_ordine_olo[ll_index2] then
			//si tratta di un ordine PRECEDENTE collegato da ologramma
			lb_precedente = true
			
		else
			//si tratta di un ordine SUCCESSIVO collegato da ologramma
			lb_precedente = false
		end if
	end if
	
	choose case fs_evasione_olo[ll_index2]
		case "E"
			ll_index = 15
		case "P"
			ll_index = 14
		case else
			ll_index = 13
	end choose
	
	if lb_precedente = false then
		ll_index += 3
	end if
	
	ltvi_campo.pictureindex = ll_index
	ltvi_campo.selectedpictureindex = ll_index
	ltvi_campo.overlaypictureindex = ll_index
	ltvi_campo.selected = false
	ltvi_campo.children = false
	
	tv_1.insertitemlast(fl_handle, ltvi_campo)
	
next
//---------------------------------------------------------------------------------------------------------------------------
end subroutine

public subroutine wf_inserisci_singolo_ordine_ologramma (string fs_ologramma);

ss_record	lstr_record
long			ll_count, ll_numero_cu, ll_num_ordine_olo[], ll_index, ll_risposta, ll_new, ll_num_ordine_principale, ll_progr_det_produzione
string			ls_flag_evasione_cu, ls_evasione_olo[], ls_cod_deposito, ls_errore_inesistente, ls_flag_evasione_principale
integer		li_anno_cu, li_anno_ordine_olo[], li_anno_ordine_principale
treeviewitem	ltvi_campo
boolean		lb_primo


ls_cod_deposito = dw_tes_ord_ven_ricerca.getitemstring(1, "cod_deposito")
ll_new = 0
lb_primo = true

//vedo se ci sono ordini con ologramma specificato

if ls_cod_deposito<>"" and not isnull(ls_cod_deposito) then

	select count(*)
	into :ll_count
	from det_ord_ven_ologramma
	join tes_ord_ven on	tes_ord_ven.cod_azienda=det_ord_ven_ologramma.cod_azienda and
								tes_ord_ven.anno_registrazione=det_ord_ven_ologramma.anno_registrazione and
								tes_ord_ven.num_registrazione=det_ord_ven_ologramma.num_registrazione
	where det_ord_ven_ologramma.cod_azienda=:s_cs_xx.cod_azienda and
			upper(det_ord_ven_ologramma.cod_ologramma) = upper(:fs_ologramma) and
			tes_ord_ven.cod_deposito=:ls_cod_deposito;
	
	ls_errore_inesistente = "Nessun ordine trovato in corrispondenza del codice ologramma '"+fs_ologramma+"' oppure appartenente ad un altro stabilimento"
else
	
	select count(*)
	into :ll_count
	from det_ord_ven_ologramma
	join tes_ord_ven on	tes_ord_ven.cod_azienda=det_ord_ven_ologramma.cod_azienda and
								tes_ord_ven.anno_registrazione=det_ord_ven_ologramma.anno_registrazione and
								tes_ord_ven.num_registrazione=det_ord_ven_ologramma.num_registrazione
	where det_ord_ven_ologramma.cod_azienda=:s_cs_xx.cod_azienda and
			upper(det_ord_ven_ologramma.cod_ologramma) = upper(:fs_ologramma);
	
	ls_errore_inesistente = "Nessun ordine trovato in corrispondenza del codice ologramma '"+fs_ologramma+"'"
	
end if

if ll_count>0 then

	declare cu_ologramma cursor for
		select distinct tes_ord_ven.anno_registrazione, tes_ord_ven.num_registrazione, tes_ord_ven.flag_evasione
		from det_ord_ven_ologramma
		join tes_ord_ven on	tes_ord_ven.cod_azienda=det_ord_ven_ologramma.cod_azienda and
									tes_ord_ven.anno_registrazione=det_ord_ven_ologramma.anno_registrazione and
									tes_ord_ven.num_registrazione=det_ord_ven_ologramma.num_registrazione
		where det_ord_ven_ologramma.cod_azienda=:s_cs_xx.cod_azienda and
				upper(det_ord_ven_ologramma.cod_ologramma) = upper(:fs_ologramma)
		order by tes_ord_ven.anno_registrazione asc, tes_ord_ven.num_registrazione asc;
	
	open cu_ologramma;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore apertura cursore cu_ologramma: "+sqlca.sqlerrtext)
		return
	end if
	
	//-----------------------------------------------
	do while true
		//Il cursore lo lego solo una volta, prendendo l'ordine con numero + basso
		fetch cu_ologramma into :li_anno_cu, :ll_numero_cu, :ls_flag_evasione_cu;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore nella fetch del cursore cu_ologramma: "+sqlca.sqlerrtext)
			close cu_ologramma;
			return
		end if
	
		if sqlca.sqlcode=100 then exit
		
		if ll_numero_cu>0 then
			if not lb_primo then
				ll_new += 1
				li_anno_ordine_olo[ll_new] = li_anno_cu
				ll_num_ordine_olo[ll_new] = ll_numero_cu
				ls_evasione_olo[ll_new] = ls_flag_evasione_cu
			else
				//il primo ordine lo metti qui, gli eventuali altri finiranno nell'array
				li_anno_ordine_principale = li_anno_cu
				ll_num_ordine_principale = ll_numero_cu
				ls_flag_evasione_principale = ls_flag_evasione_cu
				
				lb_primo = false
			end if
		end if
		
	loop
	
	close cu_ologramma;
else
	// non c'è ologramma provo a vedere se c'è un barcode di produzione
	ll_progr_det_produzione = long(fs_ologramma)
	select 	tes_ord_ven.anno_registrazione, 
				tes_ord_ven.num_registrazione,
				tes_ord_ven.flag_evasione
	into		:li_anno_cu, 
				:ll_numero_cu,
				:ls_flag_evasione_cu
	from		det_ordini_produzione
				join tes_ord_ven on	tes_ord_ven.cod_azienda=det_ordini_produzione.cod_azienda and
									tes_ord_ven.anno_registrazione=det_ordini_produzione.anno_registrazione and
									tes_ord_ven.num_registrazione=det_ordini_produzione.num_registrazione
	where	det_ordini_produzione.cod_azienda = :s_cs_xx.cod_azienda and
				det_ordini_produzione.progr_det_produzione  = :ll_progr_det_produzione;
	if sqlca.sqlcode = 0 then
		li_anno_ordine_principale = li_anno_cu
		ll_num_ordine_principale = ll_numero_cu
		ls_flag_evasione_principale = ls_flag_evasione_cu
	else				
	
		g_mb.warning(ls_errore_inesistente)
		return
	end if
end if

if ll_num_ordine_principale>0 then
else
	return
end if


//procedi (hai almeno un ordine negli array) -------------------------------------------------------------------------------
lstr_record.anno_registrazione = li_anno_ordine_principale
lstr_record.num_registrazione = ll_num_ordine_principale
lstr_record.codice = ""		
lstr_record.descrizione = ""
lstr_record.livello = 100
lstr_record.tipo_livello = "X"

ltvi_campo.label = 	string(li_anno_ordine_principale) + "/"+ string(ll_num_ordine_principale)

choose case ls_flag_evasione_principale
	case "E"
		ll_index = 10
	case "P"
		ll_index = 9
	case else
		ll_index = 8
end choose

ltvi_campo.pictureindex = ll_index
ltvi_campo.selectedpictureindex = ll_index
ltvi_campo.overlaypictureindex = ll_index
ltvi_campo.data = lstr_record
ltvi_campo.selected = false

//li_anno_ordine_olo[] = li_vuoto[]
//ll_num_ordine_olo[] = ll_vuoto[]
//ls_evasione_olo[] = ls_vuoto[]


if ll_new > 0 then
	ltvi_campo.children = true
else
	ltvi_campo.children = false
end if
ltvi_campo.expandedonce = true


ll_risposta = tv_1.insertitemlast(0, ltvi_campo)

if ll_new > 0 then
	wf_inserisci_ordini_ologramma(ll_risposta, li_anno_cu, ll_numero_cu, li_anno_ordine_olo[], ll_num_ordine_olo[], ls_evasione_olo[])
end if

return
end subroutine

public function integer wf_cambia_data_partenza ();long							ll_row, ll_num_ordine
integer						li_anno_ordine
datetime						ldt_data_partenza
//s_cs_xx_parametri		lstr_parametri


ll_row = dw_tes_ord_ven_det_1.getrow()

if ll_row>0 then
else
	return 0
end if

li_anno_ordine = dw_tes_ord_ven_det_1.getitemnumber(ll_row, "anno_registrazione")
ll_num_ordine = dw_tes_ord_ven_det_1.getitemnumber(ll_row, "num_registrazione")

if li_anno_ordine>0 and ll_num_ordine>0 then
else
	return 0
end if

ldt_data_partenza = dw_tes_ord_ven_det_1.getitemdatetime(ll_row, "data_consegna")

//apri finestra per gestione cambio data partenza


//lstr_parametri.parametro_d_1_a[1] = li_anno_ordine
//lstr_parametri.parametro_d_1_a[2] = ll_num_ordine
//lstr_parametri.parametro_data_1 = ldt_data_partenza
s_cs_xx.parametri.parametro_d_1_a[1] = li_anno_ordine
s_cs_xx.parametri.parametro_d_1_a[2] = ll_num_ordine
s_cs_xx.parametri.parametro_data_1 = ldt_data_partenza

//questa la lasciamo globale
s_cs_xx.parametri.parametro_w_ord_ven = this

//openwithparm(w_verifica_data_partenza, lstr_parametri)
window_open(w_verifica_data_partenza, -1)

return 0
end function

public subroutine wf_supervisore ();string				ls_flag_supervisore


//-------------------------------------------------------------
if s_cs_xx.cod_utente<>"CS_SYSTEM" then
	select flag_supervisore
	into :ls_flag_supervisore
	from utenti
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente;
else
	ls_flag_supervisore="S"
end if

if ls_flag_supervisore="S" then
	ib_supervisore = true
else
	ib_supervisore = false
end if

//-------------------------------------------------------------
end subroutine

public function integer wf_controlla_pianificazione (integer ai_anno_commessa, long al_num_commessa, string as_cod_semilavorato, ref string as_errore);
long			ll_count


setnull(ll_count)

if isnull(as_cod_semilavorato) or as_cod_semilavorato="" then
	select count(*)
	into :ll_count
	from buffer_chiusura_commesse
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_commessa=:ai_anno_commessa and
				num_commessa=:al_num_commessa and
				cod_semilavorato is null;
else
	select count(*)
	into :ll_count
	from buffer_chiusura_commesse
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_commessa=:ai_anno_commessa and
				num_commessa=:al_num_commessa and
				cod_semilavorato=:as_cod_semilavorato;
end if

if sqlca.sqlcode<0 then
	//errore critico
	as_errore = "Errore controllo esistenza pianificazione commessa: "+sqlca.sqlerrtext
	return -1
	
elseif ll_count>0 then
	//esiste pianificazione
	return 0
	
else
	//NON esiste pianificazione
	//commessa ancora aperta: avvisa e blocca
	as_errore = "Esiste ancora la commessa che risulta aperta e non pianificata n°"+string(ai_anno_commessa)+"/"+string(al_num_commessa)+" associata ad una riga dell'ordine! "
	return 1
end if


end function

public function integer wf_logo_etichette ();integer					li_anno
long						ll_numero, ll_row
s_cs_xx_parametri		lstr_param


ll_row = dw_tes_ord_ven_det_1.getrow()

if ll_row > 0 then
else
	return 0
end if

li_anno = dw_tes_ord_ven_det_1.getitemnumber(ll_row, "anno_registrazione")
ll_numero = dw_tes_ord_ven_det_1.getitemnumber(ll_row, "num_registrazione")
	
if li_anno>0 and ll_numero>0 then
else
	return 0
end if

lstr_param.parametro_ul_1 = li_anno
lstr_param.parametro_ul_2 = ll_numero
openwithparm(w_imposta_logo_etichetta, lstr_param)

lstr_param = message.powerobjectparm

if lstr_param.parametro_b_1 then
	dw_tes_ord_ven_det_1.setitem(ll_row, "logo_etichetta", lstr_param.parametro_s_1)
	dw_tes_ord_ven_det_1.setitemstatus(ll_row, "logo_etichetta", Primary!, NotModified!)
end if


return 0
end function

event pc_setddlb;call super::pc_setddlb;string ls_select_operatori

if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + ")) and cod_operatore in (select cod_operatore from tab_operatori_utenti where cod_utente = '" + &
						  s_cs_xx.cod_utente + "')"
						  
else
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + "))"

end if

// dw_tes_ord_ven_det_1 -------------------------------------------------
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_operatore", &
                 sqlca, &
                 "tab_operatori", &
                 "cod_operatore", &
                 "des_operatore", &
					  ls_select_operatori)
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_tipo_ord_ven", &
                 sqlca, &
                 "tab_tipi_ord_ven", &
                 "cod_tipo_ord_ven", &
                 "des_tipo_ord_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_pagamento", &
                 sqlca, &
                 "tab_pagamenti", &
                 "cod_pagamento", &
                 "des_pagamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_agente_1", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_agente_2", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_banca", &
                 sqlca, &
                 "anag_banche", &
                 "cod_banca", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_banca_clien_for", &
                 sqlca, &
                 "anag_banche_clien_for", &
                 "cod_banca_clien_for", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                 "cod_deposito_prod", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

//dw_tes_ord_ven_det_3 -----------------------------------------------------------------
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_imballo", &
                 sqlca, &
                 "tab_imballi", &
                 "cod_imballo", &
                 "des_imballo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_vettore", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_inoltro", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_mezzo", &
                 sqlca, &
                 "tab_mezzi", &
                 "cod_mezzo", &
                 "des_mezzo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_porto", &
                 sqlca, &
                 "tab_porti", &
                 "cod_porto", &
                 "des_porto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_resa", &
                 sqlca, &
                 "tab_rese", &
                 "cod_resa", &
                 "des_resa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_causale", &
                 sqlca, &
                 "tab_causali_trasp", &
                 "cod_causale", &
                 "des_causale", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
                 "cod_giro_consegna", &
                 sqlca, &
                 "tes_giri_consegne", &
                 "cod_giro_consegna", &
                 "des_giro_consegna", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_det_3, &
	"cod_natura_intra", &
	sqlca, &
	"tab_natura_intra", &
	"cod_natura_intra", &
	"descrizione", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")


// dw_tes_ord_ven_ricerca ---------------------------------------------------
f_po_loaddddw_dw(dw_tes_ord_ven_ricerca, &
				"cod_tipo_ord_ven", &
				sqlca, &
                  "tab_tipi_ord_ven", &
			    "cod_tipo_ord_ven", &
				"des_tipo_ord_ven", &
				"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tes_ord_ven_ricerca, &
				"cod_agente_1", &
				sqlca, &
                  "anag_agenti", &
				"cod_agente", &
				"rag_soc_1", &
				"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))" )
f_po_loaddddw_dw(dw_tes_ord_ven_ricerca, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_ord_ven_ricerca, &
				"cod_valuta", &
				sqlca, &
				"tab_valute", &
                  "cod_valuta", &
				"des_valuta", &
				"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))" )
f_po_loaddddw_dw(dw_tes_ord_ven_ricerca, &
				"cod_operatore", &
				sqlca, &
				"tab_operatori", &
                 	"cod_operatore", &
				"des_operatore", &
				"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))" )
f_po_loaddddw_dw(dw_tes_ord_ven_ricerca, &
				"cod_resa", &
				sqlca, &
				"tab_rese", &
                 	"cod_resa", &
				"des_resa", &
				"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))" )

					  
end event

event pc_setwindow;call super::pc_setwindow;string				l_criteriacolumn[], l_searchtable[], l_searchcolumn[], ls_data, ls_expression, ls_postit, ls_file, &
					ls_flag_aut_pagamenti, ls_path, ls_profilo
boolean			ib_apertura
unsignedlong	lul_modalita		
s_open_parm	lstr_parm


lstr_parm = message.powerobjectparm

iuo_spese_trasporto = create uo_spese_trasporto
iuo_fido_cliente = create uo_fido_cliente

												
dw_tes_ord_ven_det_1.set_dw_key("cod_azienda")
dw_tes_ord_ven_det_1.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_noretrieveonopen + c_sharedata + c_scrollparent, &
                                    c_default)
dw_tes_ord_ven_det_2.set_dw_options(sqlca, &
                                    dw_tes_ord_ven_det_1, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
dw_tes_ord_ven_det_3.set_dw_options(sqlca, &
                                    dw_tes_ord_ven_det_1, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
dw_tes_ord_ven_det_4.set_dw_options(sqlca, &
                                    dw_tes_ord_ven_det_1, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

dw_tes_ord_ven_ricerca.insertrow(0)
			
windowobject lw_oggetti[], lw_vuoto[]

lw_oggetti[1] = dw_tes_ord_ven_det_1
lw_oggetti[2] = uo_stato_prod
lw_oggetti[3] = dw_tes_off_ven
lw_oggetti[4] = dw_documenti
dw_folder.fu_assigntab(1, "Testata", lw_oggetti[])

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_tes_ord_ven_det_2
lw_oggetti[2] = uo_stato_prod
lw_oggetti[3] = dw_tes_off_ven
lw_oggetti[4] = dw_documenti
dw_folder.fu_assigntab(2, "Cliente/Destinazione", lw_oggetti[])

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_tes_ord_ven_det_3
lw_oggetti[2] = cb_grafico
lw_oggetti[3] = uo_stato_prod
lw_oggetti[4] = dw_tes_off_ven
lw_oggetti[5] = dw_documenti
dw_folder.fu_assigntab(3, "Trasporto", lw_oggetti[])

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_tes_ord_ven_det_4
lw_oggetti[2] = uo_stato_prod
lw_oggetti[3] = dw_tes_off_ven
lw_oggetti[4] = dw_documenti
dw_folder.fu_assigntab(4, "Totali", lw_oggetti[])

dw_folder.fu_foldercreate(4, 4)

dw_folder.fu_selecttab(1)

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_tes_ord_ven_ricerca
dw_folder_tv.fu_assigntab(1, "Ricerca", lw_oggetti[])

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = tv_1
dw_folder_tv.fu_assigntab(2, "Ordini", lw_oggetti[])

dw_folder_tv.fu_foldercreate(2, 2)
dw_folder_tv.fu_selecttab(1)

iuo_dw_main = dw_tes_ord_ven_det_1


dw_documenti.uof_set_management("TESORDVEN", dw_tes_ord_ven_det_1)
dw_documenti.settransobject(sqlca)
//dw_documenti.object.p_collegato.FileName = s_cs_xx.volume + s_cs_xx.risorse + + "menu\indietro.png"

//Icona Attrezzatura ---------------------------------------------------------------------------------------------------------------------
//Distinzione tra Post-it e file
ls_postit		= s_cs_xx.volume + s_cs_xx.risorse + "treeview\olo_giallo_p.png"
ls_file			= s_cs_xx.volume + s_cs_xx.risorse + "treeview\table.png"
ls_expression = "case (flag_postit when 'S' then '"+ls_postit+"' else '"+ls_file+"') "
ls_expression = "bitmap("+ls_expression+")"
dw_documenti.object.cf_bitmap.expression = ls_expression

dw_documenti.uof_enabled_delete_blob()


if s_cs_xx.parametri.parametro_i_1 > 0 and s_cs_xx.parametri.parametro_ul_1 > 0 then
	dw_tes_ord_ven_ricerca.setitem(1, "anno_registrazione", s_cs_xx.parametri.parametro_i_1)
	dw_tes_ord_ven_ricerca.setitem(1, "num_registrazione", s_cs_xx.parametri.parametro_ul_1)
	setnull(s_cs_xx.parametri.parametro_i_1)
	setnull(s_cs_xx.parametri.parametro_ul_1)
end if

dw_tes_ord_ven_ricerca.setitem(1, "anno_registrazione", f_anno_esercizio())
dw_tes_ord_ven_ricerca.setitem(1, "flag_evasione", "A")

ib_prima_riga = false

dw_tes_ord_ven_det_1.object.b_ricerca_banca_for.enabled = false
dw_tes_ord_ven_det_1.object.b_ricerca_cliente.enabled = false
dw_tes_ord_ven_det_3.object.b_aspetto_beni.enabled = false
cb_duplica_doc.enabled = false

//imposta una variabile booleana di istanza se l'utente collegato è un SUPERVISORE
wf_supervisore()
//----------------------------------------------------------------------------------------------


wf_imposta_immagini_tv()

//Donato 19-11-2008 gestione fidi PTENDA -----------------------------
if s_cs_xx.cod_utente = "CS_SYSTEM" then
	ii_protect_autorizza_sblocco = 0 //cioè not protect
else
	select flag_pagamenti
	into :ls_flag_aut_pagamenti
	from utenti
	where cod_azienda = :s_cs_xx.cod_azienda
		and cod_utente = :s_cs_xx.cod_utente;
		
	if sqlca.sqlcode <> 0  then
		g_mb.messagebox("APICE","Errore durante la lettura del flag autorizza sblocco dalla tabella utenti!",StopSign!)	
	else
		if ls_flag_aut_pagamenti = "S" then ii_protect_autorizza_sblocco = 0 //cioè not protect
	end if
end if
//------------------------------------------------------------------------------------

ls_path = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
dw_tes_ord_ven_det_1.object.p_cambia_partenza.FileName = ls_path

wf_predefinito()

dw_tes_off_ven.settransobject(sqlca)

if isvalid(lstr_parm) and not isnull(lstr_parm) then
	if lstr_parm.anno_registrazione > 0 then
		dw_tes_ord_ven_ricerca.setitem(dw_tes_ord_ven_ricerca.getrow(), "anno_registrazione", lstr_parm.anno_registrazione)
		dw_tes_ord_ven_ricerca.setitem(dw_tes_ord_ven_ricerca.getrow(), "num_registrazione", lstr_parm.num_registrazione)
		wf_ricerca()
	end if
end if



end event

on w_tes_ord_ven_tv.create
int iCurrent
call super::create
this.dw_documenti=create dw_documenti
this.cb_grafico=create cb_grafico
this.dw_folder=create dw_folder
this.dw_tes_ord_ven_det_4=create dw_tes_ord_ven_det_4
this.dw_tes_ord_ven_det_2=create dw_tes_ord_ven_det_2
this.cb_duplica_doc=create cb_duplica_doc
this.dw_tes_off_ven=create dw_tes_off_ven
this.dw_tes_ord_ven_det_3=create dw_tes_ord_ven_det_3
this.tv_1=create tv_1
this.dw_tes_ord_ven_det_1=create dw_tes_ord_ven_det_1
this.uo_stato_prod=create uo_stato_prod
this.dw_tes_ord_ven_ricerca=create dw_tes_ord_ven_ricerca
this.dw_folder_tv=create dw_folder_tv
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_documenti
this.Control[iCurrent+2]=this.cb_grafico
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_tes_ord_ven_det_4
this.Control[iCurrent+5]=this.dw_tes_ord_ven_det_2
this.Control[iCurrent+6]=this.cb_duplica_doc
this.Control[iCurrent+7]=this.dw_tes_off_ven
this.Control[iCurrent+8]=this.dw_tes_ord_ven_det_3
this.Control[iCurrent+9]=this.tv_1
this.Control[iCurrent+10]=this.dw_tes_ord_ven_det_1
this.Control[iCurrent+11]=this.uo_stato_prod
this.Control[iCurrent+12]=this.dw_tes_ord_ven_ricerca
this.Control[iCurrent+13]=this.dw_folder_tv
end on

on w_tes_ord_ven_tv.destroy
call super::destroy
destroy(this.dw_documenti)
destroy(this.cb_grafico)
destroy(this.dw_folder)
destroy(this.dw_tes_ord_ven_det_4)
destroy(this.dw_tes_ord_ven_det_2)
destroy(this.cb_duplica_doc)
destroy(this.dw_tes_off_ven)
destroy(this.dw_tes_ord_ven_det_3)
destroy(this.tv_1)
destroy(this.dw_tes_ord_ven_det_1)
destroy(this.uo_stato_prod)
destroy(this.dw_tes_ord_ven_ricerca)
destroy(this.dw_folder_tv)
end on

event pc_modify;call super::pc_modify;string ls_tabella
long ll_anno_registrazione, ll_num_registrazione 

if dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_evasione") <> "A" then
   g_mb.messagebox("Attenzione", "Ordine non modificabile! Ha già subito un'evasione.", &
              exclamation!, ok!)
   dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

if dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Ordine non modificabile! E' Bloccato.", &
              exclamation!, ok!)
   dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")

if f_controlla_quan_in_evasione(ll_anno_registrazione, ll_num_registrazione) then
   g_mb.messagebox("Attenzione", "Ordine non modificabile! Vi sono dettagli con quantità in evasione.", &
              exclamation!, ok!)
   dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if


//---------------------------------------------------------------------------------
if ib_supervisore then
	dw_tes_ord_ven_det_1.object.data_consegna.protect = 0
else
	dw_tes_ord_ven_det_1.object.data_consegna.protect = 1
end if
//---------------------------------------------------------------------------------




// TOLTO QUESTO CONTROLLO DA MODIFICA TESTATA ENRICO 24/07/98
//ls_tabella = "det_ord_ven"
//if f_controlla_coll_commesse(ls_tabella, ll_anno_registrazione, ll_num_registrazione) then
//	messagebox("Attenzione", "Ordine non modificabile! Vi sono dettagli collegati con le commesse.", &
//			  exclamation!, ok!)
//	dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
//	pcca.error = c_fatal
//	return
//end if


end event

event close;call super::close;destroy iuo_produzione
destroy iuo_spese_trasporto
destroy iuo_fido_cliente
end event

event pc_new;call super::pc_new;

//---------------------------------------------------------------------------------
dw_tes_ord_ven_det_1.object.data_consegna.protect = 0
//---------------------------------------------------------------------------------
end event

type dw_documenti from uo_dw_drag_doc_acq_ven within w_tes_ord_ven_tv
integer x = 3063
integer y = 1820
integer width = 1390
integer height = 1028
integer taborder = 160
string dataobject = "d_tes_ord_ven_note"
boolean vscrollbar = true
borderstyle borderstyle = stylebox!
boolean ib_drag_from_dw = true
end type

type cb_grafico from commandbutton within w_tes_ord_ven_tv
integer x = 3771
integer y = 1432
integer width = 375
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Copia Nota"
end type

event clicked;string ls_nota_piede, ls_nota_dettaglio
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_cod_azienda, ll_righe_aggiornate

ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(dw_tes_ord_ven_det_1.getrow(), "num_registrazione")

if ll_anno_registrazione>0 and ll_num_registrazione>0 then
else
	return
end if

select nota_piede
  into :ls_nota_piede
  from tes_ord_ven
 where cod_azienda = :s_cs_xx.cod_azienda
   and anno_registrazione = :ll_anno_registrazione
	and num_registrazione = :ll_num_registrazione;
	
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Errore in lettura dati dalla tabella TES_ORD_VEN " + sqlca.sqlerrtext)
	return
end if

ll_righe_aggiornate = 0

if not isnull(ls_nota_piede) and len(ls_nota_piede) > 0 then
	declare cu_det_ord_ven cursor for
	 select prog_riga_ord_ven,
	        nota_dettaglio
	   from det_ord_ven
	  where cod_azienda = :s_cs_xx.cod_azienda
	    and anno_registrazione = :ll_anno_registrazione
		 and num_registrazione = :ll_num_registrazione;
	
	open cu_det_ord_ven;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in apertura cursore CU_DET_ORD_VEN " + sqlca.sqlerrtext)
		return
	end if			
	
	do while 1 = 1 
		fetch cu_det_ord_ven into :ll_prog_riga_ord_ven, :ls_nota_dettaglio;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice", "Errore in lettura dati dalla tabella TES_ORD_VEN " + sqlca.sqlerrtext)
			return
		end if		
		
		if sqlca.sqlcode = 100 then exit
		
		select count(cod_azienda)
		  into :ll_cod_azienda 
		  from comp_det_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
			and anno_registrazione = :ll_anno_registrazione 
			and num_registrazione = :ll_num_registrazione
			and prog_riga_ord_ven = :ll_prog_riga_ord_ven;
			
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice", "Errore in lettura dati dalla tabella comp_det_ord_ven " + sqlca.sqlerrtext)
			return
		end if					
			
		if ll_cod_azienda > 0 then
			if not isnull(ls_nota_dettaglio) and len(ls_nota_dettaglio) > 0 then
				ls_nota_dettaglio = ls_nota_dettaglio + "~n~r" + ls_nota_piede
			else	
				ls_nota_dettaglio = ls_nota_piede
			end if	
			
			update det_ord_ven
			   set nota_dettaglio = :ls_nota_dettaglio
			 where cod_azienda = :s_cs_xx.cod_azienda
				and anno_registrazione = :ll_anno_registrazione
				and num_registrazione = :ll_num_registrazione
				and prog_riga_ord_ven = :ll_prog_riga_ord_ven;
				
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Apice", "Errore in aggiornamento dati in tabella det_ord_ven " + sqlca.sqlerrtext)
				return
			end if									
			
			ll_righe_aggiornate = ll_righe_aggiornate + 1
		
		end if
	loop
	
	close cu_det_ord_ven;
end if	

setnull(ls_nota_piede)

update tes_ord_ven
	set nota_piede = :ls_nota_piede
 where cod_azienda = :s_cs_xx.cod_azienda
	and anno_registrazione = :ll_anno_registrazione
	and num_registrazione = :ll_num_registrazione;
		
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Errore in aggiornamento dati in tabella tes_ord_ven " + sqlca.sqlerrtext)
	return
end if		

commit;

g_mb.messagebox("Apice", "Sono state aggiornate " + string(ll_righe_aggiornate) + " righe.")

//wf_ricerca()
dw_tes_ord_ven_det_1.triggerevent( "pcd_retrieve")

end event

event getfocus;call super::getfocus;dw_tes_ord_ven_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_banca_clien_for"
end event

type dw_folder from u_folder within w_tes_ord_ven_tv
integer x = 1257
integer y = 20
integer width = 3223
integer height = 2852
integer taborder = 0
end type

event po_tabclicked;call super::po_tabclicked;dw_tes_ord_ven_det_3.settaborder("cod_imballo",5)


end event

type dw_tes_ord_ven_det_4 from uo_cs_xx_dw within w_tes_ord_ven_tv
integer x = 1307
integer y = 132
integer width = 3109
integer height = 932
integer taborder = 10
string dataobject = "d_tes_ord_ven_det_4"
end type

event rowfocuschanged;call super::rowfocuschanged;dw_tes_ord_ven_det_1.setrow(currentrow)
dw_tes_ord_ven_det_1.scrolltorow(currentrow)

end event

type dw_tes_ord_ven_det_2 from uo_cs_xx_dw within w_tes_ord_ven_tv
integer x = 1330
integer y = 172
integer width = 2857
integer height = 1360
integer taborder = 200
string dataobject = "d_tes_ord_ven_det_2"
boolean border = false
end type

event rowfocuschanged;call super::rowfocuschanged;dw_tes_ord_ven_det_1.setrow(currentrow)
dw_tes_ord_ven_det_1.scrolltorow(currentrow)

end event

type cb_duplica_doc from commandbutton within w_tes_ord_ven_tv
event clicked pbm_bnclicked
boolean visible = false
integer x = 864
integer y = 1636
integer width = 361
integer height = 80
integer taborder = 140
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica Doc"
end type

type dw_tes_off_ven from datawindow within w_tes_ord_ven_tv
integer x = 1307
integer y = 1820
integer width = 1751
integer height = 1028
integer taborder = 150
boolean bringtotop = true
string title = "none"
string dataobject = "d_tes_ord_ven_offerte_tv"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event buttonclicked;long ll_anno_reg_off, ll_num_reg_off

if row>0 then
	choose case dwo.name
		case "b_offerta"
			
			ll_anno_reg_off = dw_tes_off_ven.getitemnumber(row, "det_off_ven_anno_registrazione")
			ll_num_reg_off = dw_tes_off_ven.getitemnumber(row, "det_off_ven_num_registrazione")
			
			wf_apri_offerta(ll_anno_reg_off, ll_num_reg_off)
			
	end choose
end if
end event

type dw_tes_ord_ven_det_3 from uo_cs_xx_dw within w_tes_ord_ven_tv
integer x = 1330
integer y = 132
integer width = 3109
integer height = 1556
integer taborder = 210
boolean bringtotop = true
string dataobject = "d_tes_ord_ven_det_3"
boolean border = false
end type

event ue_key;call super::ue_key;string ls_nota_fissa

choose case this.getcolumnname()
	
	case "nota_testata"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_1 = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"cod_cliente")
			s_cs_xx.parametri.parametro_s_2 = "%"
			s_cs_xx.parametri.parametro_s_3 = "%"
			s_cs_xx.parametri.parametro_s_4 = "S"
			s_cs_xx.parametri.parametro_s_5 = "%"
			s_cs_xx.parametri.parametro_s_6 = "%"
			s_cs_xx.parametri.parametro_s_7 = "%"
			s_cs_xx.parametri.parametro_s_8 = "%"
			s_cs_xx.parametri.parametro_s_9 = "T"
			s_cs_xx.parametri.parametro_data_1 = dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(),"data_registrazione")
			
			
			dw_tes_ord_ven_det_3.change_dw_current()
			setnull(s_cs_xx.parametri.parametro_s_11)
			window_open(w_ricerca_note_fisse, 0)
			if not isnull(s_cs_xx.parametri.parametro_s_11) then
				
				select nota_fissa
				  into :ls_nota_fissa
				  from tab_note_fisse
				 where cod_azienda = :s_cs_xx.cod_azienda
				   and cod_nota_fissa = :s_cs_xx.parametri.parametro_s_11;
				
				if not isnull(ls_nota_fissa) then
					if len(this.gettext()) > 0 then
						this.setcolumn("nota_testata")
						this.settext(this.gettext() + "~r~n" + ls_nota_fissa)
					else
						this.setcolumn("nota_testata")
						this.settext(ls_nota_fissa)
					end if	
				end if	
			end if
		end if
		
	case "nota_piede"
		if key = keyF1!  and keyflags = 1 then		
			s_cs_xx.parametri.parametro_s_1 = dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(),"cod_cliente")
			s_cs_xx.parametri.parametro_s_2 = "%"
			s_cs_xx.parametri.parametro_s_3 = "%"
			s_cs_xx.parametri.parametro_s_4 = "S"
			s_cs_xx.parametri.parametro_s_5 = "%"
			s_cs_xx.parametri.parametro_s_6 = "%"
			s_cs_xx.parametri.parametro_s_7 = "%"
			s_cs_xx.parametri.parametro_s_8 = "%"
			s_cs_xx.parametri.parametro_s_9 = "P"
			s_cs_xx.parametri.parametro_data_1 = dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(),"data_registrazione")
			
			
			dw_tes_ord_ven_det_3.change_dw_current()
			setnull(s_cs_xx.parametri.parametro_s_11)
			window_open(w_ricerca_note_fisse, 0)
			if not isnull(s_cs_xx.parametri.parametro_s_11) then
				
				select nota_fissa
				  into :ls_nota_fissa
				  from tab_note_fisse
				 where cod_azienda = :s_cs_xx.cod_azienda
				   and cod_nota_fissa = :s_cs_xx.parametri.parametro_s_11;
				
				if not isnull(ls_nota_fissa) then	
					if len(this.gettext()) > 0 then					
						this.setcolumn("nota_piede")
						this.settext(this.gettext() + "~r~n" + ls_nota_fissa)
					else	
						this.setcolumn("nota_piede")
						this.settext(ls_nota_fissa)
					end if	
				end if	
			end if
		end if	
		
end choose

end event

event rowfocuschanged;call super::rowfocuschanged;dw_tes_ord_ven_det_1.setrow(currentrow)
dw_tes_ord_ven_det_1.scrolltorow(currentrow)

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_aspetto_beni"
		setnull(s_cs_xx.parametri.parametro_s_1)
		window_open(w_aspetto_beni, 0)
		setitem(row, "aspetto_beni", s_cs_xx.parametri.parametro_s_1)
		
end choose
end event

type tv_1 from treeview within w_tes_ord_ven_tv
event ue_rightclicked ( long handle )
integer x = 59
integer y = 144
integer width = 1161
integer height = 2720
integer taborder = 210
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
boolean linesatroot = true
boolean hideselection = false
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

event ue_rightclicked(long handle);treeviewitem ltv_item
ss_record ls_record
string ls_tipo_nodo, ls_numero_ordine, ls_str, ls_spese_trasp
long ll_i

if handle < 1 then return 

pcca.window_current = parent

getitem(handle,ltv_item)
ls_record = ltv_item.data
ls_tipo_nodo = ls_record.tipo_livello

if ls_record.tipo_livello = "X" then
	//continua
	ls_numero_ordine = "O " + string(ls_record.anno_registrazione)+"/"+string(ls_record.num_registrazione)
else
	//non sono su un elemento di tipo ordine vendita, esci
	return
end if

m_pop_ordini lm_menu
lm_menu = create m_pop_ordini

lm_menu.m_ordinen.text = ls_numero_ordine
ls_spese_trasp = dw_tes_ord_ven_det_1.getitemstring(1, "flag_azzera_spese_trasp")

if upperbound(il_anno_conferma) > 0 then
	lm_menu.m_confermaaggiungi.enabled = true
	lm_menu.m_confermastampa.enabled = true
	ls_str = ""
	for ll_i = 1 to upperbound(il_anno_conferma)
		ls_str += "~r~n" + string(il_anno_conferma[ll_i]) + " - " + string(il_num_conferma[ll_i])
	next
	lm_menu.m_confermaaggiungi.text += ls_str
else
	lm_menu.m_confermastampa.enabled = false
end if

// spese trasp
lm_menu.m_azzeraspesetrasporto.visible = ls_spese_trasp = "N"
lm_menu.m_abilitaspesetrasporto.visible = ls_spese_trasp = "S"

if not dw_tes_ord_ven_det_1.ib_stato_nuovo and not dw_tes_ord_ven_det_1.ib_stato_modifica then 
	//non sono i modifica o nuovo, disabilita quello che c'è da disabilitare
	lm_menu.m_assegna.enabled = true
	lm_menu.m_azzeraassegnazione.enabled = true
	lm_menu.m_bloccaordine.enabled = true
	lm_menu.m_calcolaordine.enabled = true
	lm_menu.m_cambioreparto.enabled = true
	lm_menu.m_chiudicommessa.enabled = true
	lm_menu.m_configuratore.enabled = true
	lm_menu.m_corrispondenze.enabled = true
	lm_menu.m_dettaglio.enabled = true
	lm_menu.m_evasioneparziale.enabled = true
	lm_menu.m_fatturaproforma.enabled = true
	lm_menu.m_generabolla.enabled = true
	lm_menu.m_note.enabled = true
	lm_menu.m_salda.enabled = true
	lm_menu.m_sbloccaordine.enabled = true
	lm_menu.m_stampaordine.enabled = true
else
	//sono in stato modifica o nuovo, disabilita quello che c'è da disabilitare
	lm_menu.m_assegna.enabled = false
	lm_menu.m_azzeraassegnazione.enabled = false
	lm_menu.m_bloccaordine.enabled = false
	lm_menu.m_calcolaordine.enabled = false
	lm_menu.m_cambioreparto.enabled = false
	lm_menu.m_chiudicommessa.enabled = false
	lm_menu.m_configuratore.enabled = false
	lm_menu.m_corrispondenze.enabled = false
	lm_menu.m_dettaglio.enabled = false
	lm_menu.m_evasioneparziale.enabled = false
	lm_menu.m_fatturaproforma.enabled = false
	lm_menu.m_generabolla.enabled = false
	lm_menu.m_note.enabled = false
	lm_menu.m_salda.enabled = false
	lm_menu.m_sbloccaordine.enabled = false
	lm_menu.m_stampaordine.enabled = false
end if

pcca.window_current  = parent

lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
destroy lm_menu



end event

event selectionchanged;treeviewitem ltv_item
ss_record lws_record

if isnull(newhandle) or newhandle <= 0 or ib_tree_deleting then
	return 0
end if

pcca.window_current = parent

il_handle = newhandle
getitem(newhandle,ltv_item)
lws_record = ltv_item.data

dw_tes_ord_ven_det_1.change_dw_current()
parent.postevent("pc_retrieve")
end event

event itempopulate;string ls_null
long ll_livello = 0
treeviewitem ltv_item
ss_record ls_record

pcca.window_current = parent

setnull(ls_null)

if isnull(handle) or handle <= 0 or ib_tree_deleting then
	return 0
end if

getitem(handle,ltv_item)

ls_record = ltv_item.data
ll_livello = ls_record.livello

il_livello_corrente = ll_livello

setpointer(HourGlass!)

if ll_livello < 3 then
	// controlla cosa c'è al livello successivo ???
	choose case dw_tes_ord_ven_ricerca.getitemstring(dw_tes_ord_ven_ricerca.getrow(),"flag_tipo_livello_" + string(ll_livello + 1))
		case "T"
			// caricamento tipi ordini vendita
			if wf_inserisci_tipi_ordini(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "A"
			// caricamento anni registrazione
			if wf_inserisci_anni(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "C"
			// caricamento clienti
			if wf_inserisci_clienti(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "O"
			// caricamento operatori
			if wf_inserisci_operatori(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "D"
			// caricamento depositi
			if wf_inserisci_depositi(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "P"
			//caricamento prodotti
			if wf_inserisci_prodotti(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "R"
			// caricamento date registrazioni
			if wf_inserisci_data_registrazione(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "S"
			// caricamento date consegna
			if wf_inserisci_data_consegna(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "N" 
			if wf_inserisci_ordini(handle) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
		
		case else
			ltv_item.children = false
			setitem(handle, ltv_item)
			
	end choose
else
	// inserisco le registrazioni
	if wf_inserisci_ordini(handle) = 1 then
		ltv_item.children = false
		tv_1.setitem(handle, ltv_item)
	end if
	
end if

setpointer(Arrow!)
end event

event rightclicked;event post ue_rightclicked(handle)
end event

event key;long	ll_handle
string ls_messaggio, ls_sql
treeviewitem ltv_item
ss_record lws_record

if key = KeyY! and keyflags=3 then

	string							ls_riga_ordine, ls_dep_par[], ls_rep_par[], ls_dep_arr[], ls_rep_arr[], ls_vuoto[], ls_cod_prodotto, ls_cod_versione, ls_errore, &
									ls_cod_tipo_ord_ven,ls_flag_tipo_bcl
	long							ll_index, ll_riga_ordine, ll_i_rep
	integer									li_ret
	datetime						ldt_data_partenza, ldt_date_reparti[], ldt_vuoto[]
	date							ldd_dtp_rep_par[], ldd_dtp_rep_arr[], ldd_vuoto[]
	s_cs_xx_parametri			lstr_parametri
	uo_produzione				luo_prod
	uo_calendario_prod_new luo_cal_prod
	datastore 								lds_data
		
	ll_handle = il_handle
	
	setpointer(HourGlass!)
	
	do while true
		
		if ll_handle <= 0 or isnull(ll_handle) then exit
		tv_1.getitem(ll_handle, ltv_item)
		
		lws_record = ltv_item.data
		
		if lws_record.anno_registrazione > 0 and lws_record.num_registrazione > 0 then

			ls_sql = g_str.format("select T.data_consegna, D.prog_riga_ord_ven, D.cod_prodotto, D.cod_versione, T.cod_tipo_ord_ven from tes_ord_ven T left join det_ord_ven D on T.cod_azienda = D.cod_azienda and T.anno_registrazione = D.anno_registrazione and T.num_registrazione=D.num_registrazione where T.cod_azienda='$1'  and T.anno_registrazione=$2 and T.num_registrazione=$3 ",s_cs_xx.cod_azienda, lws_record.anno_registrazione, lws_record.num_registrazione)
			
			li_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql)
			
			luo_prod = create uo_produzione
			luo_cal_prod = create uo_calendario_prod_new
			
			
			for ll_index=1 to lds_data.rowcount()
				ldt_data_partenza = lds_data.getitemdatetime(ll_index, 1)
				luo_prod.idt_data_partenza = ldt_data_partenza
				
				ll_riga_ordine = lds_data.getitemnumber(ll_index, 2)
				
				ls_cod_prodotto = lds_data.getitemstring(ll_index, 3)
				ls_cod_versione = lds_data.getitemstring(ll_index, 4)
				ls_cod_tipo_ord_ven = lds_data.getitemstring(ll_index, 5)
				
				w_cs_xx_mdi.setmicrohelp("ricalcolo Calendario Ordine " + string(lws_record.anno_registrazione) + "-"  + string(lws_record.num_registrazione))
				Yield()

				
				select flag_tipo_bcl
				into :ls_flag_tipo_bcl
				from tab_tipi_ord_ven
				where cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
				
				
				ls_dep_par[] = ls_vuoto[]
				ls_rep_par[] = ls_vuoto[]
				ls_dep_arr[] = ls_vuoto[]
				ls_rep_arr[] = ls_vuoto[]
				
				ldd_dtp_rep_par[] = ldd_vuoto[]
				ldd_dtp_rep_arr[] = ldd_vuoto[]
				
				//ricalcolo le date prima del salvataggio definitivo ------
				luo_prod.il_riga_ordine = ll_riga_ordine
				li_ret = luo_prod.uof_cal_trasferimenti(	lws_record.anno_registrazione, lws_record.num_registrazione, &
																	ls_cod_prodotto, ls_cod_versione, &
																	ls_dep_par[], ls_rep_par[], ldd_dtp_rep_par[], ls_dep_arr[], ls_rep_arr[], ldd_dtp_rep_arr[], ls_errore)
				
				//se non ci sono errori salvo -------------------------------
				//se ci sono errori, in questa fase me ne frego ... o meglio inutile che vado avanti
				//probabilmente non hai trovato navette o eventi
				if li_ret = 0 then
					//se torna ZERO sicuramente ho le date e i reparti
			
					ldt_date_reparti[] =  ldt_vuoto[]
			
					
					for ll_i_rep = 1 to upperbound(ls_rep_par[])
						ls_errore = ""
						
						if ls_rep_arr[ll_i_rep]="" then setnull(ls_rep_arr[ll_i_rep])
						
						if ls_flag_tipo_bcl="B" or ls_flag_tipo_bcl="C" then
							//sfuso o tecnica
							
							update det_ord_ven
							set		cod_reparto_sfuso = :ls_rep_par[ll_i_rep],
									data_pronto_sfuso = :ldd_dtp_rep_par[ll_i_rep],
									cod_reparto_arrivo = :ls_rep_arr[ll_i_rep],
									data_arrivo = :ldd_dtp_rep_arr[ll_i_rep]
							where 	anno_registrazione = :lws_record.anno_registrazione and 
										num_registrazione = :lws_record.num_registrazione and
										prog_riga_ord_ven = :ll_riga_ordine;
							
							if sqlca.sqlcode < 0 then
								ls_errore = "Errore in update det_ord_ven (cod_reparto_sfuso-data_pronto_sfuso).~r~n" + sqlca.sqlerrtext
								li_ret = -1
							end if
							
						else
							//tenda da sole
							
							update varianti_det_ord_ven_prod
							set		data_pronto = :ldd_dtp_rep_par[ll_i_rep],
										data_arrivo = :ldd_dtp_rep_arr[ll_i_rep],
										cod_reparto_arrivo = :ls_rep_arr[ll_i_rep]
							where 	anno_registrazione = :lws_record.anno_registrazione and 
										num_registrazione = :lws_record.num_registrazione and
										prog_riga_ord_ven = :ll_riga_ordine and
										cod_reparto = :ls_rep_par[ll_i_rep];
							
							if sqlca.sqlcode < 0 then
								ls_errore = "Errore in UPDATE tabella 'varianti_det_ord_ven_prod' con data_pronto.~r~n" + sqlca.sqlerrtext
								li_ret = -1
							end if
							
						end if
							
						if li_ret<0 then
							destroy luo_prod
							destroy uo_calendario_prod_new
							rollback;
							g_mb.error(ls_errore)
							return
						end if
						
						//ricopio in un datetime
						ldt_date_reparti[ll_i_rep] = datetime(ldd_dtp_rep_par[ll_i_rep] ,00:00:00)
						
					next
						
					//se arrivi fin qui aggiorna l'impegno in calendario
					if upperbound(ls_rep_par[])>0 then
						li_ret = luo_cal_prod. uof_salva_in_calendario(	lws_record.anno_registrazione, lws_record.num_registrazione, ll_riga_ordine, &
																						ls_rep_par[], ldt_date_reparti[], ls_errore)
						if li_ret < 0 then
							destroy luo_prod
							destroy uo_calendario_prod_new
							rollback;
							g_mb.error(ls_errore)
							return
						end if
					end if
					
				else
					continue
				end if
		
			next
	
			destroy luo_prod
			destroy uo_calendario_prod_new

		end if
		
		ll_handle = tv_1.finditem( NextTreeItem! ,ll_handle )
		commit;
		
	loop
	commit;
end if

if key = KeyX! and keyflags=3 then
	
	if g_mb.confirm("Ricalcolare tutti gli ordini nella lista?") then
		uo_calcola_documento_euro luo_calcola_documento_euro
		
		ll_handle = il_handle
		
		setpointer(HourGlass!)
		
		do while true
			
			if ll_handle <= 0 or isnull(ll_handle) then exit
			tv_1.getitem(ll_handle, ltv_item)
			
			lws_record = ltv_item.data
			
			if lws_record.anno_registrazione > 0 and lws_record.num_registrazione > 0 then
			
				luo_calcola_documento_euro = create uo_calcola_documento_euro
				
				luo_calcola_documento_euro.ib_salta_esposiz_cliente = true
				
				w_cs_xx_mdi.setmicrohelp("Calcolo Ordine " + string(lws_record.anno_registrazione) + "-"  + string(lws_record.num_registrazione))
				Yield()
				
				if luo_calcola_documento_euro.uof_calcola_documento(lws_record.anno_registrazione, lws_record.num_registrazione,"ord_ven",ls_messaggio) <> 0 then
					if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.messagebox("APICE",ls_messaggio)
					rollback;
				else
					commit;
				end if
				destroy luo_calcola_documento_euro
			end if
			
			ll_handle = tv_1.finditem( NextTreeItem! ,ll_handle )

		loop
		w_cs_xx_mdi.setmicrohelp("Elaborazione Eseguita Correttamente; calcolo terminato!")
		setpointer(Arrow!)

	end if
end if


if keyflags = 1 then
	choose case key
		case KeyC!		// accesso configuratore
				if isvalid(pcca.window_currentdw) then
						triggerevent(pcca.window_current, "ue_configuratore")
				end if
		case KeyD!			// accesso dettaglio
				if isvalid(pcca.window_currentdw) then
						triggerevent(pcca.window_current, "ue_dettaglio")
				end if
		case KeyO!			// stampa ordine cliente
				if isvalid(pcca.window_currentdw) then
						triggerevent(pcca.window_current, "ue_stampa")
				end if			
		case KeyS!			// stampa rapida conferma ordine
				if isvalid(pcca.window_currentdw) then
						triggerevent(pcca.window_current, "ue_stampa_rapida_conferma")
				end if			
		case KeyA!			// Aggiungi a conferma ordine
				if isvalid(pcca.window_currentdw) then
						triggerevent(pcca.window_current, "ue_aggiungi_conferma_ordine")
				end if			
		case KeyZ!			// Reset report conferma ordine
				if isvalid(pcca.window_currentdw) then
						triggerevent(pcca.window_current, "ue_reset_conferma_ordine")
				end if			
		case KeyR!			// Stampa conferma ordine
				if isvalid(pcca.window_currentdw) then
						triggerevent(pcca.window_current, "ue_stampa_conferma_ordine")
				end if		
		case KeyE!			// Aggiungi a conferma ordine
				if isvalid(pcca.window_currentdw) then
						triggerevent(pcca.window_current, "ue_annulla_evasione_alusistemi")
				end if		
	end choose
end if

				
				
end event

type dw_tes_ord_ven_det_1 from uo_cs_xx_dw within w_tes_ord_ven_tv
event ue_posiziona ( )
event ue_allinea_ordine ( )
integer x = 1376
integer y = 132
integer width = 3040
integer height = 1560
integer taborder = 190
boolean bringtotop = true
string dataobject = "d_tes_ord_ven_det_1"
boolean border = false
end type

event ue_posiziona();//this.scrolltorow(il_num_row)

dw_tes_ord_ven_det_1.setfocus()

dw_tes_ord_ven_det_1.setcolumn(79)

end event

event ue_allinea_ordine();
wf_annulla()

pcca.window_current = parent

dw_tes_ord_ven_ricerca.setitem(1, "anno_registrazione", getitemnumber(getrow(),"anno_registrazione"))
dw_tes_ord_ven_ricerca.setitem(1, "num_registrazione", getitemnumber(getrow(),"num_registrazione"))

wf_ricerca()

end event

event itemchanged;call super::itemchanged;if i_extendmode then
   string				ls_null, ls_cod_cliente, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
						ls_frazione, ls_cap, ls_localita, ls_provincia, ls_telefono, ls_cod_banca,&
						ls_fax, ls_telex, ls_partita_iva, ls_cod_fiscale, ls_cod_deposito, &
						ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
						ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca_clien_for, &
						ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, &
						ls_cod_porto, ls_cod_resa, ls_flag_fuori_fido, ls_flag_riep_boll, ls_flag_tipo_bcl, &
						ls_flag_riep_fatt, ls_nota_testata, ls_nota_piede, ls_nota_video, ls_cod_dep_giro_cons, &
						ls_messaggio,ls_cod_giro_consegna, ls_flag_blocco, ls_cod_deposito_operatore, ls_cod_tipo_ord_ven,&
						ls_cod_deposito_tipo_ord
						
	long				ll_anno_registrazione, ll_num_registrazione, ll_cont
	double			ld_sconto
	datetime			ldt_data_registrazione, ldt_data_blocco,ldt_oggi
	integer			li_ret

   setnull(ls_null)

   choose case i_colname
	
	case "cod_deposito"
		if data<>"" then
			//se l'ordine è sfuso allora imposta il deposito produzione con il deposito del giro
			//se questo ne ha uno e se è diverso dal deposito origine ordine
			ls_cod_tipo_ord_ven = getitemstring(getrow(),"cod_tipo_ord_ven")
			
			select flag_tipo_bcl
			into :ls_flag_tipo_bcl
			from tab_tipi_ord_ven
			where 	cod_azienda = : s_cs_xx.cod_azienda and
						cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
			
			if ls_flag_tipo_bcl = "B" then
				//è uno sfuso
				ls_cod_giro_consegna = getitemstring(getrow(),"cod_giro_consegna")
				
				select cod_deposito
				into   :ls_cod_dep_giro_cons
				from tes_giri_consegne
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_giro_consegna = :ls_cod_giro_consegna;
				
				if g_str.isnotempty(ls_cod_dep_giro_cons) and ls_cod_dep_giro_cons<>data then
					setitem(getrow(),"cod_deposito_prod", ls_cod_dep_giro_cons)
				end if
				
			else
				//non è uno sfuso, per sicurezza pulisci il campo deposito produzione dello sfuso
				dw_tes_ord_ven_det_3.setitem(dw_tes_ord_ven_det_3.getrow(),"cod_deposito_prod", ls_null)
			end if
		end if
	
      case "data_registrazione"
         ls_cod_valuta = this.getitemstring(i_rownbr, "cod_valuta")
         f_cambio_ven(ls_cod_valuta, datetime(date(i_coltext)))
		
		
		li_ret = iuo_fido_cliente.uof_get_blocco_cliente( getitemstring(i_rownbr, "cod_cliente"),&
																		datetime(date(i_coltext), 00:00:00), is_cod_parametro_blocco, ls_messaggio)
		if li_ret=2 then
			//blocco
			g_mb.error(ls_messaggio)
			return 1
		elseif li_ret=1 then
			//solo warning
			g_mb.warning(ls_messaggio)
		end if
		
			
	case "cod_operatore"
		if not isnull(data)  and not isnull(s_cs_xx.cod_utente) and s_cs_xx.cod_utente <> "CS_SYSTEM" then
			
			select cod_deposito
			into :ls_cod_deposito_operatore
			from tab_operatori_utenti
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_utente = :s_cs_xx.cod_utente and
					cod_operatore = :data;
			if sqlca.sqlcode = 0 then
				
				setitem(getrow(),"cod_deposito", ls_cod_deposito_operatore)
			
				f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
									  "cod_tipo_ord_ven", &
									  sqlca, &
									  "tab_tipi_ord_ven", &
									  "cod_tipo_ord_ven", &
									  "des_tipo_ord_ven", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_tipo_ord_ven like '" + left(ls_cod_deposito_operatore,1) + "%'" )
			end if
		end if
			
      case "cod_cliente"
			ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
			
			li_ret = iuo_fido_cliente.uof_get_blocco_cliente( data, ldt_data_registrazione, is_cod_parametro_blocco, ls_messaggio)
			if li_ret=2 then
				//blocco
				g_mb.error(ls_messaggio)
				this.setitem(i_rownbr, i_colname, ls_null)
				this.SetColumn ( i_colname )
				return 1
			elseif li_ret=1 then
				//solo warning
				g_mb.warning(ls_messaggio)
			end if
			
			
         select 	anag_clienti.rag_soc_1,
					rag_soc_2,
					indirizzo,
					frazione,
					cap,
					localita,
					provincia,
					telefono,
					fax,
					telex,
					partita_iva,
					cod_fiscale,
					cod_deposito,
					cod_valuta,
					cod_tipo_listino_prodotto,
					cod_pagamento,
					sconto,
					cod_agente_1,
					cod_agente_2,
					cod_banca,
					cod_banca_clien_for,
					cod_imballo,
					cod_vettore,
					cod_inoltro,
					cod_mezzo,
					cod_porto,
					cod_resa,
					flag_fuori_fido,
					flag_riep_boll,
					flag_riep_fatt,
					flag_blocco,
					anag_clienti.data_blocco					 
         into   	:ls_rag_soc_1,    
					:ls_rag_soc_2,
					:ls_indirizzo,    
					:ls_frazione,
					:ls_cap,
					:ls_localita,
					:ls_provincia,
					:ls_telefono,
					:ls_fax,
					:ls_telex,
					:ls_partita_iva,
					:ls_cod_fiscale,
					:ls_cod_deposito,
					:ls_cod_valuta,
					:ls_cod_tipo_listino_prodotto,
					:ls_cod_pagamento,
					:ld_sconto,
					:ls_cod_agente_1,
					:ls_cod_agente_2,
					:ls_cod_banca,
					:ls_cod_banca_clien_for,
					:ls_cod_imballo,
					:ls_cod_vettore,
					:ls_cod_inoltro,
					:ls_cod_mezzo,
					:ls_cod_porto,
					:ls_cod_resa,
					:ls_flag_fuori_fido,
					:ls_flag_riep_boll,
					:ls_flag_riep_fatt,
					:ls_flag_blocco,
					:ldt_data_blocco					 
         from   	anag_clienti
         where  	cod_azienda = :s_cs_xx.cod_azienda and 
                		cod_cliente = :i_coltext;
 
         if sqlca.sqlcode = 0 then 
			if ldt_data_blocco < ldt_data_registrazione and ls_flag_blocco = "S" then
				g_mb.messagebox("Apice","Attenzione cliente bloccato!")
				this.setitem(i_rownbr, i_colname, ls_null)
				this.SetColumn ( i_colname )
				return 1
			end if				
			
			if isnull( getitemstring(getrow(),"cod_deposito") ) then
				setitem(getrow(),"cod_deposito", ls_cod_deposito)
			end if
				
            this.setitem(i_rownbr, "cod_valuta", ls_cod_valuta)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
            this.setitem(i_rownbr, "cod_pagamento", ls_cod_pagamento)
            this.setitem(i_rownbr, "sconto", ld_sconto)
            this.setitem(i_rownbr, "cod_agente_1", ls_cod_agente_1)
            this.setitem(i_rownbr, "cod_agente_2", ls_cod_agente_2)
            this.setitem(i_rownbr, "cod_banca", ls_cod_banca)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_cod_banca_clien_for)
            this.setitem(i_rownbr, "flag_fuori_fido", ls_flag_fuori_fido)
            this.setitem(i_rownbr, "flag_riep_bol", ls_flag_riep_boll)
            this.setitem(i_rownbr, "flag_riep_fat", ls_flag_riep_fatt)
            if not isnull(i_coltext) then
               dw_tes_ord_ven_det_2.modify("st_cod_cliente.text='" + i_coltext + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_cod_cliente.text=''")
            end if
            if not isnull(ls_rag_soc_1) then
               dw_tes_ord_ven_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_rag_soc_1.text=''")
            end if
            if not isnull(ls_rag_soc_2) then
               dw_tes_ord_ven_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_rag_soc_2.text=''")
            end if
            if not isnull(ls_indirizzo) then
               dw_tes_ord_ven_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_indirizzo.text=''")
            end if
            if not isnull(ls_frazione) then
               dw_tes_ord_ven_det_2.modify("st_frazione.text='" + ls_frazione + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_frazione.text=''")
            end if
            if not isnull(ls_cap) then
               dw_tes_ord_ven_det_2.modify("st_cap.text='" + ls_cap + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_cap.text=''")
            end if
            if not isnull(ls_localita) then
               dw_tes_ord_ven_det_2.modify("st_localita.text='" + ls_localita + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_localita.text=''")
            end if
            if not isnull(ls_provincia) then
               dw_tes_ord_ven_det_2.modify("st_provincia.text='" + ls_provincia + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_provincia.text=''")
            end if
            if not isnull(ls_telefono) then
               dw_tes_ord_ven_det_2.modify("st_telefono.text='" + ls_telefono + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_telefono.text=''")
            end if
            if not isnull(ls_fax) then
               dw_tes_ord_ven_det_2.modify("st_fax.text='" + ls_fax + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_fax.text=''")
            end if
            if not isnull(ls_telex) then
               dw_tes_ord_ven_det_2.modify("st_telex.text='" + ls_telex + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_telex.text=''")
            end if
            if not isnull(ls_partita_iva) then
               dw_tes_ord_ven_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_partita_iva.text=''")
            end if
            if not isnull(ls_cod_fiscale) then
               dw_tes_ord_ven_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
            else
               dw_tes_ord_ven_det_2.modify("st_cod_fiscale.text=''")
            end if
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_imballo", ls_cod_imballo)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_vettore", ls_cod_vettore)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_inoltro", ls_cod_inoltro)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_mezzo", ls_cod_mezzo)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_porto", ls_cod_porto)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_resa", ls_cod_resa)
   
            f_po_loaddddw_dw(this, &
                             "cod_des_cliente", &
                             sqlca, &
                             "anag_des_clienti", &
                             "cod_des_cliente", &
                             "rag_soc_1", &
                             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + i_coltext + "' and flag_dest_merce_fat = 'S' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
    
            this.setitem(i_rownbr, "cod_des_cliente", ls_null)
            ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
            f_cambio_ven(ls_cod_valuta, ldt_data_registrazione)
				
            if f_cerca_destinazione(dw_tes_ord_ven_det_2, i_rownbr, i_coltext, "C", ls_null, ls_messaggio) = -1 then
					g_mb.messagebox("APICE","Errore in ricerca destinazione diversa. ~r~nDettaglio: " +ls_messaggio)
				end if
				
				ldt_oggi = datetime(today(),00:00:00)
				
				select count(*)
				into   :ll_cont
				from   det_giri_consegne, tes_giri_consegne
				where  det_giri_consegne.cod_azienda = tes_giri_consegne.cod_azienda and
						 det_giri_consegne.cod_giro_consegna = tes_giri_consegne.cod_giro_consegna and
						 det_giri_consegne.cod_azienda = :s_cs_xx.cod_azienda and
				       cod_cliente = :i_coltext and
						 ( (flag_blocco <> 'S') or 
						   (flag_blocco = 'S' and data_blocco > :ldt_oggi));
							
				if ll_cont = 0 then
					g_mb.messagebox("APICE","Attenzione questo cliente non appartiene ad alcun giro")
					
				elseif ll_cont = 1 then
					
					select det_giri_consegne.cod_giro_consegna, tes_giri_consegne.cod_deposito
					into   :ls_cod_giro_consegna, :ls_cod_dep_giro_cons
					from   det_giri_consegne
					join tes_giri_consegne on	tes_giri_consegne.cod_azienda= det_giri_consegne.cod_azienda and
														tes_giri_consegne.cod_giro_consegna = det_giri_consegne.cod_giro_consegna
					where det_giri_consegne.cod_azienda = :s_cs_xx.cod_azienda and
							 det_giri_consegne.cod_cliente = :i_coltext and
							 (tes_giri_consegne.flag_blocco <> 'S' or (tes_giri_consegne.flag_blocco = 'S' and tes_giri_consegne.data_blocco>:ldt_oggi));
							 
					dw_tes_ord_ven_det_3.setitem(dw_tes_ord_ven_det_3.getrow(),"cod_giro_consegna",ls_cod_giro_consegna)
					
					//--------------------------------------------------------------------------------------------------------------------------------------------------
					//se l'ordine è di tipo sfuso imposta il deposito produzione con il deposito giro consegna, se ce l'ha e se diverso dal deposito origine ordine
					ls_cod_tipo_ord_ven = getitemstring(getrow(),"cod_tipo_ord_ven")
					ls_cod_deposito = getitemstring(getrow(),"cod_deposito")
					
					select flag_tipo_bcl
					into :ls_flag_tipo_bcl
					from tab_tipi_ord_ven
					where 	cod_azienda = : s_cs_xx.cod_azienda and
								cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
					
					if ls_flag_tipo_bcl = "B" then
						if g_str.isnotempty(ls_cod_dep_giro_cons) and ls_cod_dep_giro_cons<>ls_cod_deposito then
							setitem(getrow(),"cod_deposito_prod", ls_cod_dep_giro_cons)
						end if
						
					else
						//non è uno sfuso, per sicurezza pulisci il campo deposito produzione dello sfuso
						setitem(getrow(),"cod_deposito_prod", ls_null)
					end if
					//--------------------------------------------------------------------------------------------------------------------------------------------------
					
				else
					g_mb.messagebox("APICE","Attenzione questo cliente appartiene a più giri: impostare manualmente")
				end if		
				
			else
            this.setitem(i_rownbr, "cod_deposito", ls_null)
            this.setitem(i_rownbr, "cod_valuta", ls_null)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_null)
            this.setitem(i_rownbr, "cod_pagamento", ls_null)
            this.setitem(i_rownbr, "sconto", ls_null)
            this.setitem(i_rownbr, "cod_agente_1", ls_null)
            this.setitem(i_rownbr, "cod_agente_2", ls_null)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_null)
            this.setitem(i_rownbr, "flag_fuori_fido", ls_null)
            this.setitem(i_rownbr, "flag_riep_bol", ls_null)
            this.setitem(i_rownbr, "flag_riep_fat", ls_null)
            dw_tes_ord_ven_det_2.modify("st_cod_cliente.text=''")
            dw_tes_ord_ven_det_2.modify("st_rag_soc_1.text=''")
            dw_tes_ord_ven_det_2.modify("st_rag_soc_2.text=''")
            dw_tes_ord_ven_det_2.modify("st_indirizzo.text=''")
            dw_tes_ord_ven_det_2.modify("st_frazione.text=''")
            dw_tes_ord_ven_det_2.modify("st_cap.text=''")
            dw_tes_ord_ven_det_2.modify("st_localita.text=''")
            dw_tes_ord_ven_det_2.modify("st_provincia.text=''")
            dw_tes_ord_ven_det_2.modify("st_telefono.text=''")
            dw_tes_ord_ven_det_2.modify("st_fax.text=''")
            dw_tes_ord_ven_det_2.modify("st_telex.text=''")
            dw_tes_ord_ven_det_2.modify("st_partita_iva.text=''")
            dw_tes_ord_ven_det_2.modify("st_cod_fiscale.text=''")
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_imballo", ls_null)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_vettore", ls_null)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_inoltro", ls_null)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_mezzo", ls_null)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_porto", ls_null)
            dw_tes_ord_ven_det_3.setitem(i_rownbr, "cod_resa", ls_null)
				
            this.setitem(i_rownbr, "cod_des_cliente", ls_null)
			g_mb.messagebox("Ordini Clienti","Indicare un codice cliente valido.",StopSign!)
			//return 1
         end if
			setnull(ls_null)
			ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
			if guo_functions.uof_get_note_fisse(data, ls_null, ls_null,"ORD_VEN", ls_null,ldt_data_registrazione, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
				g_mb.error(ls_nota_testata)
			else
				if len(ls_nota_video) > 0 then g_mb.messagebox("Nota Fissa", ls_nota_video, information!)
			end if				
			dw_tes_ord_ven_det_1.postevent("ue_posiziona")		
			
	//--------------------------------------------------------------------------------------------------------------------------------
	case "cod_giro_consegna"
		if data <>"" then
			//se hai cambiato giro di consegna e l'ordine è sfuso allora imposta il deposito produzione con il deposito del giro
			//se questo ne ha uno e se è diverso dal deposito origine ordine
			ls_cod_tipo_ord_ven = getitemstring(getrow(),"cod_tipo_ord_ven")
			
			select flag_tipo_bcl
			into :ls_flag_tipo_bcl
			from tab_tipi_ord_ven
			where 	cod_azienda = : s_cs_xx.cod_azienda and
						cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
			
			if ls_flag_tipo_bcl = "B" then
				//è uno sfuso
				ls_cod_deposito = getitemstring(getrow(),"cod_deposito")
				
				select cod_deposito
				into   :ls_cod_dep_giro_cons
				from tes_giri_consegne
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_giro_consegna = :i_coltext;
				
				if g_str.isnotempty(ls_cod_dep_giro_cons) and ls_cod_dep_giro_cons<>ls_cod_deposito then
					dw_tes_ord_ven_det_3.setitem(dw_tes_ord_ven_det_3.getrow(),"cod_deposito_prod", ls_cod_dep_giro_cons)
				end if
				
			else
				//non è uno sfuso, per sicurezza pulisci il campo deposito produzione dello sfuso
				dw_tes_ord_ven_det_3.setitem(dw_tes_ord_ven_det_3.getrow(),"cod_deposito_prod", ls_null)
			end if
		end if
	//--------------------------------------------------------------------------------------------------------------------------------
	
	
	case "cod_des_cliente"
         ls_cod_cliente = this.getitemstring(i_rownbr, "cod_cliente")
			if f_cerca_destinazione(dw_tes_ord_ven_det_2, i_rownbr, ls_cod_cliente, "C", i_coltext, ls_messaggio) = -1 then
				g_mb.messagebox("APICE","Errore in ricerca destinazione diversa. ~r~nDettaglio: " +ls_messaggio)
			end if
			
			
      case "cod_valuta"
         ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
         f_cambio_ven(i_coltext, ldt_data_registrazione)
			
			
		case "cod_tipo_ord_ven"
			ls_cod_tipo_ord_ven = i_coltext
			if not isnull(ls_cod_tipo_ord_ven) and len(ls_cod_tipo_ord_ven) > 0 and not isnull(data) and len(data) > 0 and getitemnumber(getrow(),"num_registrazione") > 0 then
				
				ll_anno_registrazione = getitemnumber(getrow(),"anno_registrazione")
				ll_num_registrazione = getitemnumber(getrow(),"num_registrazione")
				
				select count(*)
				into :ll_cont
				from comp_det_ord_ven
				where cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :ll_anno_registrazione and
						num_registrazione = :ll_num_registrazione;
				if ll_cont > 0 then
					g_mb.messagebox("APICE","Attenzione: cambiando il tipo ordine è necessario ripassare tutte le righe d'ordine nel configuratore!")
				end if
			end if
			
			select 	cod_deposito
			into		:ls_cod_deposito_tipo_ord
			from		tab_tipi_ord_ven
			where	cod_azienda = :s_cs_xx.cod_azienda and
						cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
			if sqlca.sqlcode = 0 then
				setitem(getrow(),"cod_deposito", ls_cod_deposito_tipo_ord)
			end if
			
		case "data_consegna"
			string   ls_str
			long     ll_giorno, ll_anno_reg, ll_num_reg
			datetime ldt_data_consegna, ldt_data_consegna_salvata		//, ldt_data_pronto

			ls_str = left(i_coltext, 10)
			ldt_data_consegna = datetime(date(ls_str))
			ldt_data_registrazione = this.getitemdatetime(this.getrow(),"data_registrazione")
			if ldt_data_consegna < ldt_data_registrazione then
				g_mb.error("APICE","Inserire una data maggiore o uguale alla data di registrazione !")
				return 1
			end if
			if ldt_data_consegna > datetime(relativedate(today(),30),00:00:00) then
				g_mb.warning("APICE","Attenzione: La data partenza risulta 30 gg oltre quella odierna!")
				//return 1
			end if
			
			//verifica se la data consegna è cambiata --------------------------------------------------
			ll_anno_reg = getitemnumber(getrow(),"anno_registrazione")
			ll_num_reg = getitemnumber(getrow(),"num_registrazione")
			
			if ll_anno_reg>0 and ll_num_reg>0 then
				select data_consegna
				into :ldt_data_consegna_salvata
				from tes_ord_ven
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							anno_registrazione=:ll_anno_reg and num_registrazione=:ll_num_reg;
				
				if not isnull(ldt_data_consegna_salvata) and year(date(ldt_data_consegna_salvata))>=1950 then
					//data consegna salvata in testata non è NULL
					
					if ldt_data_consegna_salvata <> ldt_data_consegna then
						//è diversa da quella immessa
						ib_nuova_data_consegna = true
					else
						//è uguale a quella immessa
						ib_nuova_data_consegna = false
					end if
				else
					//la data consegna salvata in testata è NULL
					
					if not isnull(ldt_data_consegna) and year(date(ldt_data_consegna))>=1950 then
						//vuoi mettere un vaore NOT NULL
						ib_nuova_data_consegna = true
					else
						//inutile fare update perchè la data NULL era prima e NULL vuoi mettere adesso
						ib_nuova_data_consegna = false
					end if
					//ib_nuova_data_consegna = false
					
				end if
			else
				//probabile sei in nuovo, quindi non essendoci righe di dettaglio inutile lanciare l'update ...
				ib_nuova_data_consegna = false
			end if
			//----------------------------------------------------------------------------------------------
			
   end choose
end if

end event

event pcd_view;call super::pcd_view;if i_extendmode then
	dw_tes_ord_ven_det_1.object.b_ricerca_cliente.enabled = false
	dw_tes_ord_ven_det_3.object.b_aspetto_beni.enabled = false
	dw_tes_ord_ven_det_1.object.b_ricerca_banca_for.enabled = false
	cb_grafico.enabled = true
	
	if this.getrow() > 0 then
		if this.getitemnumber(this.getrow(), "num_registrazione") > 0 then
			cb_duplica_doc.enabled = true
		else
			cb_duplica_doc.enabled = false
		end if
	end if
	
	try
		dw_tes_ord_ven_det_1.object.b_logo_etichette.enabled = true
	catch (runtimeerror err)
	end try
	
	ib_new = false
	
	dw_tes_ord_ven_det_1.object.p_cambia_partenza.enabled = true
end if

end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_anno_registrazione, ll_num_registrazione, ll_return
string ls_messaggio

//// ---- Visualizzazione stato produzione - Michele 07/01/2004 ----
////st_stato_produzione.text = ""
////st_stato_produzione.backcolor = 12632256
////ov_stato_produzione.fillcolor = 12632256
//uo_stato_prod.uof_set_color(1)
////st_stato_produzione.border = false
//// ---------------------------------------------------------------


long  l_Error
ss_record lstr_record
treeviewitem ltvi_campo

if tv_1.getitem(il_handle, ltvi_campo) = -1 then
	return 
end if

lstr_record = ltvi_campo.data

if not(lstr_record.anno_registrazione > 0 and lstr_record.num_registrazione > 0 and lstr_record.tipo_livello = "X")  then
	setnull(lstr_record.anno_registrazione)
	setnull(lstr_record.num_registrazione)
end if

l_Error = Retrieve(s_cs_xx.cod_azienda, lstr_record.anno_registrazione, lstr_record.num_registrazione)
if l_Error < 0 then
	PCCA.Error = c_Fatal
end if

//retrieve offerte
wf_retrieve_offerte_vendita(lstr_record.anno_registrazione, lstr_record.num_registrazione)

il_anno_registrazione = lstr_record.anno_registrazione
il_num_registrazione = lstr_record.num_registrazione

//Stato Produzione -------------------------------------------------------------------
if dw_tes_ord_ven_det_1.getrow() > 0 then
	
	//retrieve docs
	dw_documenti.uof_retrieve_blob(dw_tes_ord_ven_det_1.getrow())
	
	if not isvalid(iuo_produzione) then
		iuo_produzione = create uo_produzione
	end if
	
	ll_return = iuo_produzione.uof_stato_prod_tes_ord_ven(il_anno_registrazione, il_num_registrazione, ls_messaggio)
	
	choose case ll_return
		case 0
			//st_stato_produzione.text = "Niente"
			//st_stato_produzione.backcolor = rgb(255,0,0)
			//ov_stato_produzione.fillcolor = rgb(255,0,0)
			uo_stato_prod.uof_set_color(2)
			//st_stato_produzione.border = true
		case 1
			//st_stato_produzione.text = "Tutto"
			//st_stato_produzione.backcolor = rgb(0,255,0)
			//ov_stato_produzione.fillcolor = rgb(0,255,0)
			uo_stato_prod.uof_set_color(3)
			//st_stato_produzione.border = true
		case 2
			//st_stato_produzione.text = "Parziale"
			//st_stato_produzione.backcolor = rgb(255,255,0)
			//ov_stato_produzione.fillcolor = rgb(255,255,0)
			uo_stato_prod.uof_set_color(4)
			//st_stato_produzione.border = true
		case else
			//st_stato_produzione.text = ""
			//st_stato_produzione.backcolor = 12632256
			//ov_stato_produzione.fillcolor = 12632256
			uo_stato_prod.uof_set_color(1)
			//st_stato_produzione.border = false
	end choose
	
else
	//st_stato_produzione.text = ""
	//st_stato_produzione.backcolor = 12632256
	//ov_stato_produzione.fillcolor = 12632256
	uo_stato_prod.uof_set_color(1)
	//st_stato_produzione.border = false
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	string ls_cod_tipo_ord_ven, ls_cod_operatore, ls_cod_deposito_operatore
	long	ll_gg_consegna, ll_ret
	date	ld_data
	datetime	ldt_data_consegna
	
	dw_folder_tv.fu_selecttab(2)
	
	this.setitem(this.getrow(), "data_registrazione", datetime(today()))
	this.setitem(this.getrow(), "ora_registrazione", now())
	
	select cod_operatore, 
			cod_deposito
	into 	:ls_cod_operatore,
			:ls_cod_deposito_operatore
	from tab_operatori_utenti
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_utente = :s_cs_xx.cod_utente and
			 flag_default = 'S';
			 
   if sqlca.sqlcode = -1 then
		g_mb.messagebox("Estrazione Operatori", "Errore durante l'Estrazione dell'Operatore di Default")
		pcca.error = c_fatal
		return
	elseif sqlca.sqlcode = 0 then
		this.setitem(this.getrow(), "cod_operatore", ls_cod_operatore)
		
		if not isnull(ls_cod_deposito_operatore) and len(ls_cod_deposito_operatore) > 0 then
			setitem(getrow(),"cod_deposito", ls_cod_deposito_operatore)
		end if
	end if
	
	guo_functions.uof_get_parametro_azienda( "GDC", ll_gg_consegna)
	if not isnull(ll_gg_consegna) then
		ld_data = relativedate(today(), ll_gg_consegna)
		ldt_data_consegna = datetime(ld_data, 00:00:00)
		setitem(this.getrow(), "data_consegna", ldt_data_consegna)
	end if

	dw_tes_ord_ven_det_1.object.b_ricerca_cliente.enabled = true
	dw_tes_ord_ven_det_3.object.b_aspetto_beni.enabled = true
	cb_duplica_doc.enabled = false
	dw_tes_ord_ven_det_1.object.b_ricerca_banca_for.enabled = true
	cb_grafico.enabled = false
	ib_new = true
	
	try
		dw_tes_ord_ven_det_1.object.b_logo_etichette.enabled = false
	catch (runtimeerror err)
	end try
	
	dw_tes_ord_ven_det_3.setitem(dw_tes_ord_ven_det_1.getrow(), "flag_doc_suc_tes", "I")
	dw_tes_ord_ven_det_3.setitem(dw_tes_ord_ven_det_1.getrow(), "flag_st_note_tes", "I")
	dw_tes_ord_ven_det_3.setitem(dw_tes_ord_ven_det_1.getrow(), "flag_doc_suc_pie", "I")
	dw_tes_ord_ven_det_3.setitem(dw_tes_ord_ven_det_1.getrow(), "flag_st_note_pie", "I")	
	
	dw_tes_ord_ven_det_1.object.p_cambia_partenza.enabled = false

end if

//Donato 19-11-2008 gestione fidi
this.object.flag_aut_sblocco_fido.protect = ii_protect_autorizza_sblocco
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	dw_tes_ord_ven_det_1.object.b_ricerca_cliente.enabled = true
	dw_tes_ord_ven_det_3.object.b_aspetto_beni.enabled = true
	cb_duplica_doc.enabled = false
	
	dw_tes_ord_ven_det_1.object.b_ricerca_banca_for.enabled = true
	cb_grafico.enabled = false
	
	dw_tes_ord_ven_det_1.object.p_cambia_partenza.enabled = false
	
	try
		dw_tes_ord_ven_det_1.object.b_logo_etichette.enabled = false
	catch (runtimeerror err)
	end try
end if

//Donato 19-11-2008 gestione fidi
this.object.flag_aut_sblocco_fido.protect = ii_protect_autorizza_sblocco
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_cliente

if isnull(dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(), "data_consegna")) then
	g_mb.messagebox("Apice", "Data consegna obbligatoria")
	pcca.error = c_fatal
end if	

//if isnull(dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(), "data_pronto")) and not isnull(dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(), "data_consegna")) then
	//g_mb.messagebox("Apice", "Data pronto obbligatoria")
	//pcca.error = c_fatal
//end if		
	
ls_cod_cliente = this.getitemstring(this.getrow(), "cod_cliente")
if isnull(ls_cod_cliente) or len(ls_cod_cliente) < 1 then
	g_mb.error("Il codice cliente è un dato OBBLIGATORIO !!!")
	pcca.error = c_fatal
end if
	

end event

event pcd_validatecol;call super::pcd_validatecol;//if isnull(dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(), "data_consegna")) then
//	messagebox("Apice", "Data consegna obbligatoria")
//	return
//end if	
//
//if isnull(dw_tes_ord_ven_det_1.getitemdatetime(dw_tes_ord_ven_det_1.getrow(), "data_pronto")) then
//	messagebox("Apice", "Data pronto obbligatoria")
//	return
//end if	
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
   string ls_cod_cliente, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, &
          ls_cap, ls_localita, ls_provincia, ls_telefono, ls_fax, ls_telex, &
          ls_partita_iva, ls_cod_fiscale


   if this.getrow() > 0 then
      ls_cod_cliente = this.getitemstring(this.getrow(), "cod_cliente")
      select anag_clienti.rag_soc_1,
             anag_clienti.rag_soc_2,
             anag_clienti.indirizzo,
             anag_clienti.frazione,
             anag_clienti.cap,
             anag_clienti.localita,
             anag_clienti.provincia,
             anag_clienti.telefono,
             anag_clienti.fax,
             anag_clienti.telex,
             anag_clienti.partita_iva,
             anag_clienti.cod_fiscale
      into   :ls_rag_soc_1,    
             :ls_rag_soc_2,
             :ls_indirizzo,    
             :ls_frazione,
             :ls_cap,
             :ls_localita,
             :ls_provincia,
             :ls_telefono,
             :ls_fax,
             :ls_telex,
             :ls_partita_iva,
             :ls_cod_fiscale
      from   anag_clienti
      where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
             anag_clienti.cod_cliente = :ls_cod_cliente;

      if sqlca.sqlcode = 0 then
         if not isnull(ls_cod_cliente) then
            dw_tes_ord_ven_det_2.modify("st_cod_cliente.text='" + ls_cod_cliente + "'")
         else
            ls_cod_cliente = ""
            dw_tes_ord_ven_det_2.modify("st_cod_cliente.text=''")
         end if
         if not isnull(ls_rag_soc_1) then
            dw_tes_ord_ven_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_rag_soc_1.text=''")
         end if
         if not isnull(ls_rag_soc_2) then
            dw_tes_ord_ven_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_rag_soc_2.text=''")
         end if
         if not isnull(ls_indirizzo) then
            dw_tes_ord_ven_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_indirizzo.text=''")
         end if
         if not isnull(ls_frazione) then
            dw_tes_ord_ven_det_2.modify("st_frazione.text='" + ls_frazione + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_frazione.text=''")
         end if
         if not isnull(ls_cap) then
            dw_tes_ord_ven_det_2.modify("st_cap.text='" + ls_cap + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_cap.text=''")
         end if
         if not isnull(ls_localita) then
            dw_tes_ord_ven_det_2.modify("st_localita.text='" + ls_localita + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_localita.text=''")
         end if
         if not isnull(ls_provincia) then
            dw_tes_ord_ven_det_2.modify("st_provincia.text='" + ls_provincia + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_provincia.text=''")
         end if
         if not isnull(ls_telefono) then
            dw_tes_ord_ven_det_2.modify("st_telefono.text='" + ls_telefono + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_telefono.text=''")
         end if
         if not isnull(ls_fax) then
            dw_tes_ord_ven_det_2.modify("st_fax.text='" + ls_fax + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_fax.text=''")
         end if
         if not isnull(ls_telex) then
            dw_tes_ord_ven_det_2.modify("st_telex.text='" + ls_telex + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_telex.text=''")
         end if
         if not isnull(ls_partita_iva) then
            dw_tes_ord_ven_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_partita_iva.text=''")
         end if
         if not isnull(ls_cod_fiscale) then
            dw_tes_ord_ven_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
         else
            dw_tes_ord_ven_det_2.modify("st_cod_fiscale.text=''")
         end if

         f_po_loaddddw_dw(dw_tes_ord_ven_det_1, &
                          "cod_des_cliente", &
                          sqlca, &
                          "anag_des_clienti", &
                          "cod_des_cliente", &
                          "rag_soc_1", &
                          "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + ls_cod_cliente + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
      else
         dw_tes_ord_ven_det_2.modify("st_cod_cliente.text=''")
			dw_tes_ord_ven_det_2.modify("st_rag_soc_1.text=''")
			dw_tes_ord_ven_det_2.modify("st_rag_soc_2.text=''")
			dw_tes_ord_ven_det_2.modify("st_indirizzo.text=''")
			dw_tes_ord_ven_det_2.modify("st_frazione.text=''")
			dw_tes_ord_ven_det_2.modify("st_cap.text=''")
			dw_tes_ord_ven_det_2.modify("st_localita.text=''")
			dw_tes_ord_ven_det_2.modify("st_provincia.text=''")
			dw_tes_ord_ven_det_2.modify("st_telefono.text=''")
			dw_tes_ord_ven_det_2.modify("st_fax.text=''")
			dw_tes_ord_ven_det_2.modify("st_telex.text=''")
			dw_tes_ord_ven_det_2.modify("st_partita_iva.text=''")
			dw_tes_ord_ven_det_2.modify("st_cod_fiscale.text=''")
		end if
		
		dw_documenti.uof_retrieve_blob(dw_tes_ord_ven_det_1.getrow())
		
   end if
	
end if

end event

event pcd_save;call super::pcd_save;if i_extendmode then
	
   if pcca.error = c_success then
		dw_tes_ord_ven_det_1.object.b_ricerca_cliente.enabled = false
		dw_tes_ord_ven_det_3.object.b_aspetto_beni.enabled = false
		dw_tes_ord_ven_det_1.object.b_ricerca_banca_for.enabled = false
		cb_grafico.enabled = true

		try
			dw_tes_ord_ven_det_1.object.b_logo_etichette.enabled = true
		catch (runtimeerror err)
		end try

     	if this.getrow() > 0 and this.getitemnumber(this.getrow(), "num_registrazione") > 0 then
         	cb_duplica_doc.enabled = true
		else
			cb_duplica_doc.enabled = false
		end if
		
		ib_new = false
   end if
	
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_max_registrazione
string		ls_null

setnull(ls_null)

for ll_i = 1 to this.rowcount()
	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if this.getitemnumber(ll_i, "anno_registrazione") = 0 or &
		isnull(this.getitemnumber(ll_i, "anno_registrazione")) then
	
		ll_anno_registrazione = f_anno_esercizio()
		if ll_anno_registrazione > 0 then
			this.setitem(this.getrow(), "anno_registrazione", int(ll_anno_registrazione))
		else
			g_mb.messagebox("Ordini Clienti","Impostare l'anno di esercizio in parametri aziendali")
		end if
	
//		ll_num_registrazione = f_gen_num_ordine()
		ll_num_registrazione = guo_functions.uof_gen_num_ordine( today() )
		this.setitem(this.getrow(), "num_registrazione", ll_num_registrazione)
		
	 end if
	 
	if this.getitemstring(ll_i, "cod_giro_consegna") = "" then this.setitem(ll_i, "cod_giro_consegna", ls_null)
	 
next      
end event

event updateend;call super::updateend;integer				li_ret
 treeviewitem		ltv_item
 long					ll_row, ll_risposta
 ss_record			ls_record


// Stefano 04/05/2010: ticket 2010/113: commento l'allinemanento ordine per non perdere
// i filtri di ricerca
 //postevent("ue_allinea_ordine")
 
//inserimento elemento sull'albero se nuovo
ll_row = this.getrow()

choose case this.getitemstatus(ll_row, 0, Primary!)
	case NewModified!	
		
		ls_record.anno_registrazione = this.getitemnumber( ll_row, "anno_registrazione")
		ls_record.num_registrazione = this.getitemnumber( ll_row, "num_registrazione")
		ls_record.codice = ""
		ls_record.descrizione = ""
		ls_record.livello = 100
		ls_record.tipo_livello = "X"
		
		ltv_item.data = ls_record
		ltv_item.label = "NEW! " + string(ls_record.anno_registrazione) + "/" + string(ls_record.num_registrazione)
		
		ltv_item.pictureindex = 8
		ltv_item.selectedpictureindex = 8
		ltv_item.overlaypictureindex = 8
					
		ltv_item.children = false
		ltv_item.selected = false
	
		ll_risposta = tv_1.insertitemLast(0, ltv_item)
		tv_1.SelectItem(ll_risposta)
		
	case New!
		
end choose
end event

event updatestart;call super::updatestart;if i_extendmode then
	integer				li_i, li_risposta
	
	string					ls_tabella, /*ls_tot_documento,*/ ls_nome_prog, ls_cod_cliente, &
			 				ls_nota_testata, ls_nota_piede, ls_nota_video, ls_nota_old, ls_null, ls_flag_stampata_bcl
							 
	datetime				ldt_data_registrazione, ldt_data_consegna

	long					ll_anno_registrazione, ll_num_registrazione
	
	string					ls_messaggio, ls_flag_inviato
	
	uo_produzione 							luo_prod
	uo_calendario_prod_new			luo_cal_prod
	uo_log_sistema							luo_log
	
	// 16/02/2004 Michela: se un ordine è già esportato allora avverto l'utente.
	ls_flag_inviato = getitemstring(getrow(), "flag_inviato")
	if ls_flag_inviato = "S" then
		g_mb.messagebox("APICE","Attenzione: questo ordine è già stato esportato. Le modifiche dovranno essere notificate al ricevente manualmente.")
	end if
	// fine modifiche	
	
//	ls_cod_cliente = this.getitemstring(this.getrow(), "cod_cliente")
//	if isnull(ls_cod_cliente) or len(ls_cod_cliente) < 1 then
//		g_mb.error("Il codice cliente è un dato OBBLIGATORIO !!!")
//		return 1
//	end if
	
	ls_flag_stampata_bcl = getitemstring(getrow(), "flag_stampata_bcl")
		
	if ls_flag_stampata_bcl = 'S' then
		li_risposta = g_mb.messagebox("Apice", "Devono essere ristampati per la produzione?", Question!, YesNo!, 2)
		
		ll_anno_registrazione = getitemnumber(getrow(), "anno_registrazione")
		ll_num_registrazione = getitemnumber(getrow(), "num_registrazione")
		
   		if li_risposta = 1 then
			update tes_ord_ven
			set    flag_stampata_bcl ='N'
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_registrazione=:ll_anno_registrazione
			and    num_registrazione=:ll_num_registrazione;
			
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore in fase di aggiornamento flag stampa bcl a NO: " + sqlca.sqlerrtext)
				return 1
			end if
		end if	
	end if
	
	for li_i = 1 to this.deletedcount()
		ll_anno_registrazione = this.getitemnumber(li_i, "anno_registrazione", delete!, true)
		ll_num_registrazione = this.getitemnumber(li_i, "num_registrazione", delete!, true)
		
		luo_log = create uo_log_sistema
		luo_log.uof_write_log_sistema_not_sqlca( "ORD", g_str.format("Cancellazione Testata Ordine Cliente $1-$2",ll_anno_registrazione,ll_num_registrazione))
		destroy luo_log
		
		ls_tabella = "det_ord_ven_stat"
		ls_nome_prog = ""

     	if f_elimina_det_stat(ls_tabella, ll_anno_registrazione, ll_num_registrazione, ls_nome_prog, 0) = -1 then
			return 1
		end if
			
		delete varianti_det_ord_ven_prod
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione varianti reparti (varianti_det_ord_ven_prod): "+sqlca.sqlerrtext)
			return 1
		end if

		delete varianti_det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante cancellazione varianti ordini: "+sqlca.sqlerrtext)
			return 1
		end if

      	ls_tabella = "det_ord_ven"

      	if f_riag_quan_impegnata(ll_anno_registrazione, ll_num_registrazione) = -1 then
			return 1
		end if

		delete tab_optionals_comp_det
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione tabella tab_optionals_comp_det: "+sqlca.sqlerrtext)
			return 1
		end if
		
		delete tab_mp_non_ric_comp_det
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione tabella MP automatiche (tab_mp_non_ric_comp_det): "+sqlca.sqlerrtext)
			return 1
		end if

		delete comp_det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione complementi ordine: "+sqlca.sqlerrtext)
			return 1
		end if
		
		delete det_ord_ven_note		
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione tabella note dettagli: "+sqlca.sqlerrtext)
			return 1
		end if
		
		delete det_ord_ven_corrispondenze
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione corrispondenze dettagli ordine: "+sqlca.sqlerrtext)
			return 1
		end if
      
		delete iva_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione dati iva dell'ordine: " + sqlca.sqlerrtext)
			return 1
		end if
		
		delete tes_ord_ven_corrispondenze
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Apice","Errore in cancellazione corrispondenze ordine: " + sqlca.sqlerrtext)
			return 1
		end if
		
		delete tes_ord_ven_note
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione tabella note testata ordine: " + sqlca.sqlerrtext)
			return 1
		end if
      
		delete scad_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione dati scadenze dell'ordine: " + sqlca.sqlerrtext)
			return 1
		end if
		
		delete det_ord_ven_conf_variabili
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in cancellazione variabili configuratore: "+sqlca.sqlerrtext)
			return 1
		end if
		
		//---------- Michele 28/01/2004 Cancellazione dati produzione ------------

		luo_prod = create uo_produzione
		
		if luo_prod.uof_elimina_produzione(ll_num_registrazione, ll_anno_registrazione,"X", ls_messaggio) <> 0 then
			g_mb.error(ls_messaggio)
			destroy luo_prod
			return 1
		end if
		
		destroy luo_prod
		//------------------------------------------------------------------------
		
		
		//pulizia tabella det_ord_ven_prod, MA PRIMA AGGIORNA LA TAB_CAL_PRODUZIONE -----------------------------------------
		luo_cal_prod = create uo_calendario_prod_new
		li_risposta = luo_cal_prod.uof_disimpegna_calendario(ll_anno_registrazione, ll_num_registrazione, ls_messaggio)
		destroy luo_cal_prod;
		
		if li_risposta<0 then
			g_mb.error(ls_messaggio)
			return 1
		end if
		
		delete det_ord_ven_prod
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione;
		
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante cancellazione dettaglio ordine prod: "+sqlca.sqlerrtext)
			return 1
		end if
		
		
		long ll_prog_riga_ord_ven
		
		// Cursore per det_ord_ven_prod ---------------------------------------------
		DECLARE 	cu_righe CURSOR FOR  
		SELECT 	prog_riga_ord_ven
		 FROM 	det_ord_ven
		WHERE  	cod_azienda = :s_cs_xx.cod_azienda  AND  
					anno_registrazione = :ll_anno_registrazione  AND  
					num_registrazione = :ll_num_registrazione
		order by prog_riga_ord_ven;
		
		open cu_righe;
		if sqlca.sqlcode < 0 then
			ls_messaggio = "Errore in OPEN cursore 'cu_righe dettaglio'~r~n" + sqlca.sqlerrtext
			g_mb.error(ls_messaggio)
			return 1
		end if
		
		do while true
			fetch cu_righe into :ll_prog_riga_ord_ven;
			
			if sqlca.sqlcode < 0 then
				ls_messaggio = "Errore in FETCH cursore 'cu_righe dettaglio'~r~n" + sqlca.sqlerrtext
				close cu_righe;
				g_mb.error(ls_messaggio)
				return 1
			end if
			if sqlca.sqlcode = 100 then exit
			
			
			delete from distinte_taglio_calcolate
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione and
					 num_registrazione  = :ll_num_registrazione and
					 prog_riga_ord_ven  = :ll_prog_riga_ord_ven;
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore in cancellazione distinte taglio calcolate: "+sqlca.sqlerrtext)
				close cu_righe;
				return 1
			end if
			
			//elimino eventuali ologrammi
			luo_prod = create uo_produzione
			li_risposta = luo_prod.uof_elimina_ologrammi(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_messaggio)
			destroy luo_prod
			
			if li_risposta < 0 then
				//in fs_messaggio l'errore
				g_mb.error("Apice",ls_messaggio)
				close cu_righe;
				return 1
			elseif li_risposta=1 then
				//operazione annullata
				g_mb.warning("Apice",ls_messaggio)
				close cu_righe;
				return 1
			end if

		loop
		
		close cu_righe;
		//--------------------------------------------------------------------------------------------------------------------------------------------
		
		
		//------------------------------------------------------------------------------------------------------
		//pulizia tabella evasione da dati eventuali per evitare errore di FK
		delete from evas_ord_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione and
					num_registrazione  = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			ls_messaggio = "Errore durante pulizia tabella evas_ord_ven dell'ordine: "+sqlca.sqlerrtext
			g_mb.error(ls_messaggio)
			return 1
		end if
		
		//pulizia tabella stampa ordini altri stabilimenti per evitare, qualora presente
		//che arrivino mail di avviso su ordine oramai cancellato
		//non controllo l'errore che mi pare superfluo ....
		delete from tes_stampa_ord_ven_depositi
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione and
					num_registrazione  = :ll_num_registrazione;
		//------------------------------------------------------------------------------------------------------
		
		
		//Donato 23/05/2012
		//chiesto da Beatrice
		//prima di eliminare i dettagli verifico se devo riaprire offerte di vendita eventuali
		if wf_riapri_offerta( ll_anno_registrazione, ll_num_registrazione, ls_messaggio) < 0 then
			g_mb.error("Apice","Errore in riapertura offerta collegata: "+ls_messaggio)
			return 1
		end if
		//---------------------------------------------------------
		
		
		//Donato 28/01/2013 ----------------------------------------------------------------------
		//Beatrice ha chiesto di mettere a NULL il collegamento con la riga di fattura proforma
		//quando cancello una riga di ordine di vendita
		if guo_functions.uof_setnull_fatt_proforma(ll_anno_registrazione, ll_num_registrazione, ls_messaggio) < 0 then
			g_mb.error("Apice",ls_messaggio)
			return 1
		end if
		//----------------------------------------------------------------------------------------------
		
		
		if f_del_det(ls_tabella, ll_anno_registrazione, ll_num_registrazione) = -1 then
			return 1
		end if
		//fine iterata su righe cancellate

   next

   if this.getrow() > 0 then
		
		//----------------------------------------------------------------------------------------------------------------------
		//memorizzo il primo valore assegnato alla data consegna in data pronto, campo invisibile
		dwItemStatus l_status
		
		l_status = this.GetItemStatus(this.GetRow(), 0, Primary!)
		
		//non mettere or l_status = New!
		if l_status = NotModified!  or l_status = NewModified! then
			this.setitem(this.GetRow(), "data_pronto", this.getitemdatetime(this.getrow(), "data_consegna"))
		end if
		//---------------------------------------------------------------------------------------------------------------------

		ll_anno_registrazione = this.getitemnumber(this.getrow(), "anno_registrazione")
		ll_num_registrazione = this.getitemnumber(this.getrow(), "num_registrazione")
		
		if ll_anno_registrazione <> 0 then
			
			ls_tabella = "det_ord_ven"

			if dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_evasione") <> "A" then
				g_mb.messagebox("Attenzione", "Ordine non modificabile! Ha già subito un'evasione.", &
							  exclamation!, ok!)
				dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
				pcca.error = c_fatal
				return 1
			end if
			
			if dw_tes_ord_ven_det_1.getitemstring(dw_tes_ord_ven_det_1.getrow(), "flag_blocco") = "E" then
				g_mb.messagebox("Attenzione", "Ordine non modificabile! E' Bloccato.", &
							  exclamation!, ok!)
				dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
				pcca.error = c_fatal
				return 1
			end if
			
			if f_controlla_quan_in_evasione(ll_anno_registrazione, ll_num_registrazione) then
				g_mb.messagebox("Attenzione", "Ordine non modificabile! Vi sono dettagli con quantità in evasione.", &
							  exclamation!, ok!)
				dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
				pcca.error = c_fatal
				return 1
			end if


			//se la data consegna è stata cambiata, impostala anche su tutte le eventuali righe dettaglio
			l_status = this.GetItemStatus(this.GetRow(), "data_consegna", Primary!)
			if l_status = DataModified!  or l_status = NewModified! then
				ldt_data_consegna = this.getitemdatetime(this.getrow(), "data_consegna")
				
				update det_ord_ven
				set data_consegna=:ldt_data_consegna
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							anno_registrazione=:ll_anno_registrazione and
							num_registrazione=:ll_num_registrazione;
			end if
			

			if ib_new then
				setnull(ls_null)
				ls_cod_cliente = this.getitemstring(this.getrow(), "cod_cliente")
				ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
			
				if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "ORD_VEN", ls_null, ldt_data_registrazione, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
					g_mb.error(ls_nota_testata)
				else
					ls_nota_old = this.getitemstring(this.getrow(), "nota_testata")
					if isnull(ls_nota_old) then ls_nota_old = ""
					this.setitem(this.getrow(), "nota_testata", ls_nota_testata + ls_nota_old )
				
					ls_nota_old = this.getitemstring(this.getrow(), "nota_piede")
					if isnull(ls_nota_old) then ls_nota_old = ""
					this.setitem(this.getrow(), "nota_piede", ls_nota_piede + ls_nota_old )
				end if
			
			end if
		
		end if
		
   end if

end if
end event

event pcd_delete;call super::pcd_delete;string ls_tabella,ls_cod_cliente,ls_rag_soc_1
long ll_anno_registrazione, ll_num_registrazione
integer li_i


for li_i = 1 to dw_tes_ord_ven_det_1.deletedcount()
	if dw_tes_ord_ven_det_1.getitemstring(li_i, "flag_evasione", delete!, true) <> "A" then
	   g_mb.messagebox("Attenzione", "Ordine non cancellabile! Ha già subito un'evasione.", &
	              exclamation!, ok!)
	   dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return 0
	end if

	if dw_tes_ord_ven_det_1.getitemstring(li_i, "flag_blocco", delete!, true) = "S" then
	   g_mb.messagebox("Attenzione", "Ordine non cancellabile! E' Bloccato.", &
	              exclamation!, ok!)
	   dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return 0
	end if

	ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(li_i, "anno_registrazione", delete!, true)
	ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(li_i, "num_registrazione", delete!, true)
	if f_controlla_quan_in_evasione(ll_anno_registrazione, ll_num_registrazione) then
	   g_mb.messagebox("Attenzione", "Ordine non cancellabile! Vi sono dettagli con quantità in evasione.", &
	              exclamation!, ok!)
	   dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return 0
	end if

	ls_tabella = "det_ord_ven"
	if f_controlla_coll_commesse(ls_tabella, ll_anno_registrazione, ll_num_registrazione) then
	   g_mb.messagebox("Attenzione", "Ordine non cancellabile! Vi sono dettagli collegati con le commesse.", &
	              exclamation!, ok!)
	   dw_tes_ord_ven_det_1.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return 0
	end if
	
	ls_cod_cliente = dw_tes_ord_ven_det_1.getitemstring(li_i,"cod_cliente", delete!, true)
	
	select rag_soc_1
 	into   :ls_rag_soc_1
	from   anag_clienti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cliente=:ls_cod_cliente;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		pcca.error = c_fatal
	   return 0
	end if

	g_mb.messagebox("Apice","L'ordine " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " del cliente " + ls_rag_soc_1 +  " è in stato di cancellazione, al successivo salvataggio delle righe sarà eliminato.", information!)
 
	
next

dw_tes_ord_ven_det_1.object.b_ricerca_banca_for.enabled = false
dw_tes_ord_ven_det_1.object.b_ricerca_cliente.enabled = false
dw_tes_ord_ven_det_3.object.b_aspetto_beni.enabled = false
cb_duplica_doc.enabled = false

dw_tes_ord_ven_det_1.object.p_cambia_partenza.enabled = false

treeviewitem ltv_item
ss_record ls_record

il_handle_modificato = 0
il_handle_cancellato = 0

if deletedcount() > 0 then
	il_handle_cancellato = tv_1.finditem(CurrentTreeItem!, 0)
	
	if il_handle_cancellato>0 then
		tv_1.getitem(il_handle_cancellato,ltv_item)
		ls_record = ltv_item.data
		
		if ls_record.tipo_livello = "X" then
			//è un documento, rimuovilo
			tv_1.deleteitem(il_handle_cancellato)
		end if
		
	end if
	
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tes_ord_ven_det_1,"cod_cliente")
		
	case "b_ricerca_banca_for"
		guo_ricerca.uof_ricerca_prodotto(dw_tes_ord_ven_det_1,"cod_banca_clien_for")
		
	case "b_logo_etichette"
		wf_logo_etichette()
		
	case "b_cancella_produzione"
		if row > 0 then
			string	ls_errore
			long	ll_anno_registrazione, ll_num_registrazione
			uo_produzione luo_prod
			
			ll_anno_registrazione = dw_tes_ord_ven_det_1.getitemnumber(row,"anno_registrazione")
			ll_num_registrazione = dw_tes_ord_ven_det_1.getitemnumber(row,"num_registrazione")
			if ll_anno_registrazione > 0 and not isnull(ll_anno_registrazione) then
				if g_mb.confirm("Procedo con la cancellazione dei dati della produzione?") then
					luo_prod=create uo_produzione
					if luo_prod.uof_elimina_produzione( ll_num_registrazione, ll_anno_registrazione, "X", ref ls_errore) < 0 then
						rollback;
						g_mb.error(ls_errore)
					else
						commit;
						g_mb.show("Cancellazione eseguita con successo!")
					end if
					destroy luo_prod
				end if
			end if
		end if
end choose
end event

event clicked;call super::clicked;
if row>0 then
else
	return
end if

choose case dwo.name
		
	case "p_cambia_partenza"
		wf_cambia_data_partenza()
		
end choose
end event

type uo_stato_prod from uo_stato_produzione within w_tes_ord_ven_tv
integer x = 1330
integer y = 1688
integer taborder = 150
boolean bringtotop = true
end type

on uo_stato_prod.destroy
call uo_stato_produzione::destroy
end on

type dw_tes_ord_ven_ricerca from uo_std_dw within w_tes_ord_ven_tv
event ue_keydown pbm_dwnkey
integer x = 46
integer y = 140
integer width = 1143
integer height = 2716
integer taborder = 30
string dataobject = "d_tes_ord_ven_ricerca_tv"
boolean border = false
boolean livescroll = false
end type

event ue_keydown;if key = KeyEnter! then
	wf_ricerca()
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tes_ord_ven_ricerca,"cod_cliente")
	
	case "b_ricerca_prodotto"
		//selezione prodotto per ricerca -------------------------------------------------------
		guo_ricerca.uof_ricerca_prodotto(dw_tes_ord_ven_ricerca,"cod_prodotto")	
		
	case "b_cerca"
		//ricerca -------------------------------------------------------------------------------------
		wf_ricerca()
		
	case "b_annulla"
		//reset dei campi ----------------------------------------------------------------------------
		wf_annulla()
		
	case "b_predefinito"
		//carica filtro impostato ----------------------------------------------------------------------
		wf_predefinito()
		
	case "b_imposta_predefinito"
		//salva filtro impostato sulla dw -----------------------------------------------------------
		wf_imposta_predefinito()
		
end choose
end event

event itemchanged;call super::itemchanged;long ll_tab_order

ll_tab_order = 500

choose case this.getcolumnname()

	case "flag_tipo_livello_1"
		setitem(row, "flag_tipo_livello_2", "N")
		setitem(row, "flag_tipo_livello_3", "N")
		settaborder("flag_tipo_livello_2", 0)
		settaborder("flag_tipo_livello_3", 0)
		if data <> "N" then
			settaborder("flag_tipo_livello_2", ll_tab_order)
		end if
			
	case "flag_tipo_livello_2"
		setitem(row, "flag_tipo_livello_3", "N")
		settaborder("flag_tipo_livello_3", 0)
		if data <> "N" then
			settaborder("flag_tipo_livello_3", ll_tab_order + 10)
		end if		
		
	case "data_registrazione_d"
		if isnull( getitemdatetime(row,"data_registrazione_a")) then setitem(row, "data_registrazione_a", datetime(data))

	case "data_consegna_d"
		if isnull( getitemdatetime(row,"data_consegna_a")) then setitem(row, "data_consegna_a", datetime(data))
end choose
end event

type dw_folder_tv from u_folder within w_tes_ord_ven_tv
integer x = 23
integer y = 20
integer height = 2856
integer taborder = 10
end type

