﻿$PBExportHeader$w_verifica_data_partenza.srw
$PBExportComments$Finestra Gestione Testata Ordini di Vendita
forward
global type w_verifica_data_partenza from w_cs_xx_principale
end type
type st_11 from statictext within w_verifica_data_partenza
end type
type st_10 from statictext within w_verifica_data_partenza
end type
type st_9 from statictext within w_verifica_data_partenza
end type
type r_1 from rectangle within w_verifica_data_partenza
end type
type st_8 from statictext within w_verifica_data_partenza
end type
type st_7 from statictext within w_verifica_data_partenza
end type
type st_6 from statictext within w_verifica_data_partenza
end type
type st_legneda_semaforo from statictext within w_verifica_data_partenza
end type
type st_log from statictext within w_verifica_data_partenza
end type
type hpb_1 from hprogressbar within w_verifica_data_partenza
end type
type st_1 from statictext within w_verifica_data_partenza
end type
type cb_eventi from commandbutton within w_verifica_data_partenza
end type
type cb_navette from commandbutton within w_verifica_data_partenza
end type
type dw_trasf from datawindow within w_verifica_data_partenza
end type
type dw_lista from datawindow within w_verifica_data_partenza
end type
type st_data_partenza from statictext within w_verifica_data_partenza
end type
type cb_annulla from commandbutton within w_verifica_data_partenza
end type
type cb_ok from commandbutton within w_verifica_data_partenza
end type
type cb_verifica from commandbutton within w_verifica_data_partenza
end type
type em_data_partenza from editmask within w_verifica_data_partenza
end type
type ov_1 from oval within w_verifica_data_partenza
end type
type ov_2 from oval within w_verifica_data_partenza
end type
type ov_3 from oval within w_verifica_data_partenza
end type
type ov_4 from oval within w_verifica_data_partenza
end type
type st_5 from statictext within w_verifica_data_partenza
end type
type r_3 from rectangle within w_verifica_data_partenza
end type
end forward

global type w_verifica_data_partenza from w_cs_xx_principale
integer width = 4334
integer height = 2856
string title = "Verifica Data Partenza"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
string icon = "AppIcon!"
boolean center = true
event ue_post_open ( )
st_11 st_11
st_10 st_10
st_9 st_9
r_1 r_1
st_8 st_8
st_7 st_7
st_6 st_6
st_legneda_semaforo st_legneda_semaforo
st_log st_log
hpb_1 hpb_1
st_1 st_1
cb_eventi cb_eventi
cb_navette cb_navette
dw_trasf dw_trasf
dw_lista dw_lista
st_data_partenza st_data_partenza
cb_annulla cb_annulla
cb_ok cb_ok
cb_verifica cb_verifica
em_data_partenza em_data_partenza
ov_1 ov_1
ov_2 ov_2
ov_3 ov_3
ov_4 ov_4
st_5 st_5
r_3 r_3
end type
global w_verifica_data_partenza w_verifica_data_partenza

type variables
integer		ii_anno_ordine
long			il_num_ordine
string			is_flag_tipo_bcl

//memorizzo la data partenza dell'ordine originale
datetime		idt_data_partenza_iniziale

end variables

forward prototypes
public subroutine wf_verifica ()
public subroutine wf_set_row (long al_row)
end prototypes

event ue_post_open();wf_verifica()
end event

public subroutine wf_verifica ();string								ls_reparti[], ls_vuoto[], ls_errore, ls_temp
integer							li_ret, ll_k, li_stato
datetime							ldt_data_partenza, ldt_date_reparti[], ldt_vuoto[]
uo_calendario_prod_new		luo_cal_prod
long								ll_riga_ordine, ll_index, ll_tot


ldt_data_partenza = datetime(date(em_data_partenza.text), 00:00:00)

setpointer(hourglass!)

luo_cal_prod = create uo_calendario_prod_new

dw_trasf.setredraw(false)

st_log.text = "Elaborazione in corso ..."
hpb_1.position = 0

ll_tot = dw_lista.rowcount()

hpb_1.maxposition = ll_tot



for ll_index=1 to ll_tot
	dw_lista.setitem(ll_index, "stato", "-")
	dw_lista.setitem(ll_index, "flag_errore", "N")
	dw_lista.setitem(ll_index, "errore", "")
next

for ll_index=1 to ll_tot
	
	ll_riga_ordine = dw_lista.getitemnumber(ll_index, "prog_riga_ord_ven")
	
	st_log.text = "Elaborazione in corso ... riga "+string(ii_anno_ordine)+"/"+string(il_num_ordine)+"/"+string(ll_riga_ordine)
	hpb_1.stepit()
	
	
	ls_reparti[] = ls_vuoto[]
	ldt_date_reparti[] = ldt_vuoto[]
	setnull(li_stato)
	
	luo_cal_prod.ib_rielaborato = true
	li_ret = luo_cal_prod.uof_dw_cal_produzione(	true, ii_anno_ordine, il_num_ordine, ll_riga_ordine, &
																"", "", ldt_data_partenza, ls_reparti[], ldt_date_reparti[], dw_trasf, ls_errore)
	if luo_cal_prod.ib_rielaborato then
		dw_lista.setitem(ll_index, "rielaborato", "S")
	else
		dw_lista.setitem(ll_index, "rielaborato", "N")
	end if
	
	//arrivato qui hai qualche riga nella dw
	//se la colonna testo non è vuota c'è qualche errore ...
	ls_temp = dw_trasf.getitemstring(dw_trasf.getrow(), "testo")
	if ls_temp<>"" and not isnull(ls_temp) then
		dw_lista.setitem(ll_index, "flag_errore", "S")
		dw_lista.setitem(ll_index, "errore", ls_temp)
		
		continue
		
	end if
	
	//dw_lista.setitem(ll_index, "flag_errore", "N")
	//dw_lista.setitem(ll_index, "errore", "")
	
	
	for ll_k=1 to dw_trasf.rowcount()
		 ls_temp = dw_trasf.getitemstring(ll_k, "semaforo")
		 
		if ls_temp="V" and (isnull(li_stato)) then li_stato = 0
		if ls_temp="G" and (isnull(li_stato) or li_stato<=0) then li_stato = 1
		if ls_temp="R" and (isnull(li_stato) or li_stato<=1) then li_stato = 2
		
		if li_stato=2 then exit
		 
	next
	
	choose case li_stato
		case 0
			dw_lista.setitem(ll_index, "stato", "V")
			
		case 1
			dw_lista.setitem(ll_index, "stato", "G")
			
		case 2
			dw_lista.setitem(ll_index, "stato", "R")
			
		case else
			//lascia spento
			
	end choose
	
next

destroy luo_cal_prod

st_log.text = "Pronto!"

dw_trasf.setredraw(true)

dw_lista.setrow(1)
wf_set_row(1)

setpointer(arrow!)


end subroutine

public subroutine wf_set_row (long al_row);
long								ll_tot, ll_index, ll_riga_ordine
datetime							ldt_data_partenza, ldt_date_reparti[]
uo_calendario_prod_new		luo_cal_prod
string								ls_reparti[], ls_errore
integer							li_ret
														

if al_row>0 then
else
	return
end if

ldt_data_partenza = datetime(date(em_data_partenza.text), 00:00:00)

ll_riga_ordine = dw_lista.getitemnumber(al_row, "prog_riga_ord_ven")

if ll_riga_ordine>0 then
else
	return
end if

setpointer(hourglass!)

luo_cal_prod = create uo_calendario_prod_new
luo_cal_prod.uof_dw_cal_produzione(	true, ii_anno_ordine, il_num_ordine, ll_riga_ordine, &
														"", "", ldt_data_partenza, ls_reparti[], ldt_date_reparti[], dw_trasf, ls_errore)
destroy luo_cal_prod

setpointer(arrow!)

return
end subroutine

on w_verifica_data_partenza.create
int iCurrent
call super::create
this.st_11=create st_11
this.st_10=create st_10
this.st_9=create st_9
this.r_1=create r_1
this.st_8=create st_8
this.st_7=create st_7
this.st_6=create st_6
this.st_legneda_semaforo=create st_legneda_semaforo
this.st_log=create st_log
this.hpb_1=create hpb_1
this.st_1=create st_1
this.cb_eventi=create cb_eventi
this.cb_navette=create cb_navette
this.dw_trasf=create dw_trasf
this.dw_lista=create dw_lista
this.st_data_partenza=create st_data_partenza
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.cb_verifica=create cb_verifica
this.em_data_partenza=create em_data_partenza
this.ov_1=create ov_1
this.ov_2=create ov_2
this.ov_3=create ov_3
this.ov_4=create ov_4
this.st_5=create st_5
this.r_3=create r_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_11
this.Control[iCurrent+2]=this.st_10
this.Control[iCurrent+3]=this.st_9
this.Control[iCurrent+4]=this.r_1
this.Control[iCurrent+5]=this.st_8
this.Control[iCurrent+6]=this.st_7
this.Control[iCurrent+7]=this.st_6
this.Control[iCurrent+8]=this.st_legneda_semaforo
this.Control[iCurrent+9]=this.st_log
this.Control[iCurrent+10]=this.hpb_1
this.Control[iCurrent+11]=this.st_1
this.Control[iCurrent+12]=this.cb_eventi
this.Control[iCurrent+13]=this.cb_navette
this.Control[iCurrent+14]=this.dw_trasf
this.Control[iCurrent+15]=this.dw_lista
this.Control[iCurrent+16]=this.st_data_partenza
this.Control[iCurrent+17]=this.cb_annulla
this.Control[iCurrent+18]=this.cb_ok
this.Control[iCurrent+19]=this.cb_verifica
this.Control[iCurrent+20]=this.em_data_partenza
this.Control[iCurrent+21]=this.ov_1
this.Control[iCurrent+22]=this.ov_2
this.Control[iCurrent+23]=this.ov_3
this.Control[iCurrent+24]=this.ov_4
this.Control[iCurrent+25]=this.st_5
this.Control[iCurrent+26]=this.r_3
end on

on w_verifica_data_partenza.destroy
call super::destroy
destroy(this.st_11)
destroy(this.st_10)
destroy(this.st_9)
destroy(this.r_1)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.st_6)
destroy(this.st_legneda_semaforo)
destroy(this.st_log)
destroy(this.hpb_1)
destroy(this.st_1)
destroy(this.cb_eventi)
destroy(this.cb_navette)
destroy(this.dw_trasf)
destroy(this.dw_lista)
destroy(this.st_data_partenza)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.cb_verifica)
destroy(this.em_data_partenza)
destroy(this.ov_1)
destroy(this.ov_2)
destroy(this.ov_3)
destroy(this.ov_4)
destroy(this.st_5)
destroy(this.r_3)
end on

event pc_setwindow;call super::pc_setwindow;//s_cs_xx_parametri			lstr_parametri
long								ll_tot, ll_index, ll_riga_ordine
datetime							ldt_data_partenza, ldt_date_reparti[], ldt_vuoto[]
uo_calendario_prod_new		luo_cal_prod
string								ls_cod_tipo_ord_ven, ls_reparti[], ls_vuoto[], ls_errore, ls_temp, ls_path, ls_flag_supervisore
integer							li_ret, ll_k, li_stato
boolean							lb_supervisore
														

//lstr_parametri = message.powerobjectparm
//
//ii_anno_ordine = lstr_parametri.parametro_d_1_a[1]
//il_num_ordine = lstr_parametri.parametro_d_1_a[2]
//ldt_data_partenza = lstr_parametri.parametro_data_1
ii_anno_ordine = s_cs_xx.parametri.parametro_d_1_a[1]
il_num_ordine = s_cs_xx.parametri.parametro_d_1_a[2]
ldt_data_partenza = s_cs_xx.parametri.parametro_data_1

setnull(s_cs_xx.parametri.parametro_d_1_a[1])
setnull(s_cs_xx.parametri.parametro_d_1_a[2])
setnull(s_cs_xx.parametri.parametro_data_1)



//memorizzo la data partenza dell'ordine originale
idt_data_partenza_iniziale = ldt_data_partenza


ls_cod_tipo_ord_ven = f_des_tabella ("tes_ord_ven", "anno_registrazione="+string(ii_anno_ordine)+ " and num_registrazione="+string(il_num_ordine), "cod_tipo_ord_ven" )

//ls_flag_tipo_bcl = A - B - C
select flag_tipo_bcl
into :is_flag_tipo_bcl
from tab_tipi_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;
			
em_data_partenza.text = string(ldt_data_partenza, "dd/mm/yyyy")

dw_lista.SetRowFocusIndicator(Hand!)
dw_lista.settransobject(sqlca)

ll_tot = dw_lista.retrieve(s_cs_xx.cod_azienda, ii_anno_ordine, il_num_ordine)

if ll_tot=0 then
	setpointer(arrow!)
	g_mb.warning("nessuna riga da visualizzare!")
	cb_annulla.postevent(clicked!)
	return
end if


//------------------------------------------------------------------------------------
ls_path = s_cs_xx.volume + s_cs_xx.risorse + "11.5\forklift.png"
dw_trasf.object.p_forklift.FileName = ls_path

ls_path = s_cs_xx.volume + s_cs_xx.risorse + "11.5\truck.png"
dw_trasf.object.p_truck.FileName = ls_path

ls_path = s_cs_xx.volume + s_cs_xx.risorse + "menu\icona_utente.png"
dw_trasf.object.p_cliente.FileName = ls_path
//------------------------------------------------------------------------------------

event post ue_post_open()


lb_supervisore = false

if s_cs_xx.cod_utente<>"CS_SYSTEM" then
	select flag_supervisore
	into :ls_flag_supervisore
	from utenti
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente;
			
	if ls_flag_supervisore="S" then lb_supervisore = true
else
	lb_supervisore = true
end if

cb_eventi.visible = lb_supervisore
cb_navette.visible = lb_supervisore



end event

type st_11 from statictext within w_verifica_data_partenza
integer x = 2811
integer y = 2560
integer width = 1445
integer height = 184
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "In presenza di questo simbolo esiste un problema per i reparti coinvolti in questa riga. seleziona la riga oppure passa il mouse sul simbolo per leggere il dettaglio del problema"
boolean focusrectangle = false
end type

type st_10 from statictext within w_verifica_data_partenza
integer x = 2729
integer y = 2556
integer width = 82
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "!"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_9 from statictext within w_verifica_data_partenza
integer x = 2747
integer y = 2492
integer width = 626
integer height = 56
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Legenda colonna ~"Err~""
alignment alignment = center!
boolean focusrectangle = false
end type

type r_1 from rectangle within w_verifica_data_partenza
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 32
integer y = 2508
integer width = 2601
integer height = 252
end type

type st_8 from statictext within w_verifica_data_partenza
integer x = 1326
integer y = 2648
integer width = 1303
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "il reparto in quella data non risulta pianificato"
boolean focusrectangle = false
end type

type st_7 from statictext within w_verifica_data_partenza
integer x = 1326
integer y = 2560
integer width = 1303
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "il reparto in quella data è oltre soglia max ma in tolleranza"
boolean focusrectangle = false
end type

type st_6 from statictext within w_verifica_data_partenza
integer x = 224
integer y = 2648
integer width = 946
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "il reparto in quella data è oltre soglia max"
boolean focusrectangle = false
end type

type st_legneda_semaforo from statictext within w_verifica_data_partenza
integer x = 101
integer y = 2480
integer width = 782
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Legenda Semaforo reparto"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_log from statictext within w_verifica_data_partenza
integer x = 2482
integer y = 112
integer width = 1806
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 12632256
string text = "Pronto!"
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within w_verifica_data_partenza
integer x = 2482
integer y = 32
integer width = 1815
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 1
end type

type st_1 from statictext within w_verifica_data_partenza
integer x = 37
integer y = 32
integer width = 2382
integer height = 72
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Non chiudere la finestra di testata ordini vendita prima di aver chiuso questa finestra!"
boolean focusrectangle = false
end type

type cb_eventi from commandbutton within w_verifica_data_partenza
integer x = 3429
integer y = 184
integer width = 398
integer height = 88
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Eventi Trasf."
end type

event clicked;window_open(w_eventi_trasferimento, -1)
end event

type cb_navette from commandbutton within w_verifica_data_partenza
integer x = 3826
integer y = 184
integer width = 480
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Calend. Navette"
end type

event clicked;window_open(w_tab_cal_trasferimenti, -1)
end event

type dw_trasf from datawindow within w_verifica_data_partenza
integer x = 3438
integer y = 272
integer width = 869
integer height = 2196
integer taborder = 30
string title = "none"
string dataobject = "d_ext_trasferimenti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;boolean								lb_esiste_riga
datetime								ldt_data_partenza, ldt_date_reparti[]
long									ll_index, ll_riga_ordine, ll_cur_row
uo_calendario_prod_new			luo_cal_prod
string									ls_errore, ls_reparti[]
integer								li_ret


lb_esiste_riga = true

ll_cur_row = dw_lista.getrow()

if row>0 and ll_cur_row>0 then
else
	return
end if

	
//leggo attuale data partenza dell'ordine, oppure se è stata GIà AMBIATA PASSO QUELLA ...
ldt_data_partenza = datetime(date(em_data_partenza.text), 00:00:00)

dw_trasf.setredraw(false)

//recupero reparti ed eventuali date partenza dai reparti della riga corrente, mi serve per la finestra del calendario
ll_riga_ordine = dw_lista.getitemnumber(ll_cur_row, "prog_riga_ord_ven")

luo_cal_prod = create uo_calendario_prod_new
li_ret = luo_cal_prod.uof_dw_cal_produzione(	true, ii_anno_ordine, il_num_ordine, ll_riga_ordine, &
																"", "", ldt_data_partenza, ls_reparti[], ldt_date_reparti[], dw_trasf, ls_errore)
destroy luo_cal_prod

dw_trasf.setredraw(true)

s_cs_xx.parametri.parametro_s_4_a[] = ls_reparti[]
s_cs_xx.parametri.parametro_data_1_a[] = ldt_date_reparti[]
s_cs_xx.parametri.parametro_data_4 = ldt_data_partenza
setnull(ldt_data_partenza)

window_open(w_cal_produzione_reparti, 0)


if s_cs_xx.parametri.parametro_b_2 then
	//data partenza cambiata, rifai l'elaborazione
	ldt_data_partenza = s_cs_xx.parametri.parametro_data_4
	em_data_partenza.text = string(ldt_data_partenza, "dd/mm/yyyy")
	wf_verifica()

else
	//non fare nulla
	
end if

setnull(s_cs_xx.parametri.parametro_data_4)
s_cs_xx.parametri.parametro_b_2 = false




end event

type dw_lista from datawindow within w_verifica_data_partenza
integer x = 23
integer y = 272
integer width = 3406
integer height = 2196
integer taborder = 20
string title = "none"
string dataobject = "d_verifica_data_partenza"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event rowfocuschanged;											

if currentrow>0 then
else
	return
end if

wf_set_row(currentrow)
end event

type st_data_partenza from statictext within w_verifica_data_partenza
integer x = 9
integer y = 168
integer width = 507
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Data Partenza:"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_verifica_data_partenza
integer x = 2043
integer y = 152
integer width = 343
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;
close(parent)
end event

type cb_ok from commandbutton within w_verifica_data_partenza
event wf_get_r ( )
integer x = 1687
integer y = 152
integer width = 343
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Accetta"
end type

event clicked;datetime									ldt_data_partenza, ldt_date_reparti[], ldt_vuoto[]

s_cs_xx_parametri					lstr_parametri

long										ll_index, ll_riga_ordine, ll_i_rep

string										ls_riga_ordine, ls_dep_par[], ls_rep_par[], ls_dep_arr[], ls_rep_arr[], ls_vuoto[], ls_cod_prodotto, ls_cod_versione, ls_errore

date										ldd_dtp_rep_par[], ldd_dtp_rep_arr[], ldd_vuoto[]

uo_produzione							luo_prod
uo_calendario_prod_new				luo_cal_prod

integer									li_ret




//leggo la data impostata
ldt_data_partenza = datetime(date(em_data_partenza.text), 00:00:00)


if ldt_data_partenza=idt_data_partenza_iniziale then
	
	if not g_mb.confirm("La data partenza non è stata cambiata. Vuoi comunque forzare il ricalcolo? Se rispondi NO, nessuna elaborazione verrà effettuata!") then
		//hai scelto di non fare niente
		close(parent)
		return
	end if
	
end if


if not g_mb.confirm("Reimpostare la data di partenza per tutte le righe dell'ordine?") then return


luo_prod = create uo_produzione
luo_cal_prod = create uo_calendario_prod_new

luo_prod.idt_data_partenza = ldt_data_partenza

for ll_index=1 to dw_lista.rowcount()
	ll_riga_ordine = dw_lista.getitemnumber(ll_index, "prog_riga_ord_ven")
	ls_riga_ordine = "Riga Ordine "+string(ii_anno_ordine)+"/"+string(il_num_ordine)+"/"+string(ll_riga_ordine)+"~r~n~r~n"
	
	ls_cod_prodotto = dw_lista.getitemstring(ll_index, "cod_prodotto")
	ls_cod_versione = dw_lista.getitemstring(ll_index, "cod_versione")
	
	
	ls_dep_par[] = ls_vuoto[]
	ls_rep_par[] = ls_vuoto[]
	ls_dep_arr[] = ls_vuoto[]
	ls_rep_arr[] = ls_vuoto[]
	
	ldd_dtp_rep_par[] = ldd_vuoto[]
	ldd_dtp_rep_arr[] = ldd_vuoto[]
	
	//ricalcolo le date prima del salvataggio definitivo ------
	luo_prod.il_riga_ordine = ll_riga_ordine
	li_ret = luo_prod.uof_cal_trasferimenti(	ii_anno_ordine, il_num_ordine, &
														ls_cod_prodotto, ls_cod_versione, &
														ls_dep_par[], ls_rep_par[], ldd_dtp_rep_par[], ls_dep_arr[], ls_rep_arr[], ldd_dtp_rep_arr[], ls_errore)
	
	//se non ci sono errori salvo -------------------------------
	//se ci sono errori, in questa fase me ne frego ... o meglio inutile che vado avanti
	//probabilmente non hai trovato navette o eventi
	if li_ret = 0 then
		//se torna ZERO sicuramente ho le date e i reparti

		ldt_date_reparti[] =  ldt_vuoto[]

		
		for ll_i_rep = 1 to upperbound(ls_rep_par[])
			ls_errore = ""
			
			if ls_rep_arr[ll_i_rep]="" then setnull(ls_rep_arr[ll_i_rep])
			
			if is_flag_tipo_bcl="B" or is_flag_tipo_bcl="C" then
				//sfuso o tecnica
				
				update det_ord_ven
				set		cod_reparto_sfuso = :ls_rep_par[ll_i_rep],
						data_pronto_sfuso = :ldd_dtp_rep_par[ll_i_rep],
						cod_reparto_arrivo = :ls_rep_arr[ll_i_rep],
						data_arrivo = :ldd_dtp_rep_arr[ll_i_rep]
				where 	anno_registrazione = :ii_anno_ordine and 
							num_registrazione = :il_num_ordine and
							prog_riga_ord_ven = :ll_riga_ordine;
				
				if sqlca.sqlcode < 0 then
					ls_errore = "Errore in update det_ord_ven (cod_reparto_sfuso-data_pronto_sfuso).~r~n" + sqlca.sqlerrtext
					li_ret = -1
				end if
				
			else
				//tenda da sole
				
				update varianti_det_ord_ven_prod
				set		data_pronto = :ldd_dtp_rep_par[ll_i_rep],
							data_arrivo = :ldd_dtp_rep_arr[ll_i_rep],
							cod_reparto_arrivo = :ls_rep_arr[ll_i_rep]
				where 	anno_registrazione = :ii_anno_ordine and 
							num_registrazione = :il_num_ordine and
							prog_riga_ord_ven = :ll_riga_ordine and
							cod_reparto = :ls_rep_par[ll_i_rep];
				
				if sqlca.sqlcode < 0 then
					ls_errore = "Errore in UPDATE tabella 'varianti_det_ord_ven_prod' con data_pronto.~r~n" + sqlca.sqlerrtext
					li_ret = -1
				end if

			end if
				
			if li_ret<0 then
				destroy luo_prod
				destroy uo_calendario_prod_new
				rollback;
				g_mb.error(ls_errore)
				return
			end if
			
			//ricopio in un datetime
			ldt_date_reparti[ll_i_rep] = datetime(ldd_dtp_rep_par[ll_i_rep] ,00:00:00)
			
		next
			
		//se arrivi fin qui aggiorna l'impegno in calendario
		if upperbound(ls_rep_par[])>0 then
			li_ret = luo_cal_prod. uof_salva_in_calendario(	ii_anno_ordine, il_num_ordine, ll_riga_ordine, &
																			ls_rep_par[], ldt_date_reparti[], ls_errore)
			if li_ret < 0 then
				destroy luo_prod
				destroy uo_calendario_prod_new
				rollback;
				g_mb.error(ls_errore)
				return
			end if
		end if
		
	else
		//ls_errore = ls_riga_ordine + ls_errore
		//destroy luo_prod
		//destroy uo_calendario_prod_new
		continue
	end if

next

destroy luo_prod
destroy uo_calendario_prod_new


//commit;

//non mi resta che fare l'update delle date consegna
//lo rimando alla w_tes_ord_ven_tv
if isvalid(s_cs_xx.parametri.parametro_w_ord_ven)  then
	
	s_cs_xx.parametri.parametro_data_4 = ldt_data_partenza
	s_cs_xx.parametri.parametro_d_4_a[1] = ii_anno_ordine
	s_cs_xx.parametri.parametro_d_4_a[2] = il_num_ordine
	s_cs_xx.parametri.parametro_w_ord_ven.postevent("ue_cambia_data_partenza")
	s_cs_xx.parametri.parametro_w_ord_ven.postevent("ue_calcola")

end if

close(parent)
return
end event

type cb_verifica from commandbutton within w_verifica_data_partenza
integer x = 1029
integer y = 152
integer width = 343
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Verifica"
end type

event clicked;

wf_verifica()
end event

type em_data_partenza from editmask within w_verifica_data_partenza
integer x = 539
integer y = 152
integer width = 462
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = center!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean dropdowncalendar = true
end type

type ov_1 from oval within w_verifica_data_partenza
long linecolor = 134217745
integer linethickness = 6
long fillcolor = 65280
integer x = 101
integer y = 2556
integer width = 82
integer height = 68
end type

type ov_2 from oval within w_verifica_data_partenza
long linecolor = 134217745
integer linethickness = 6
long fillcolor = 255
integer x = 101
integer y = 2644
integer width = 82
integer height = 68
end type

type ov_3 from oval within w_verifica_data_partenza
long linecolor = 134217745
integer linethickness = 6
long fillcolor = 65535
integer x = 1202
integer y = 2556
integer width = 82
integer height = 68
end type

type ov_4 from oval within w_verifica_data_partenza
long linecolor = 134217745
integer linethickness = 6
long fillcolor = 12632256
integer x = 1202
integer y = 2644
integer width = 82
integer height = 68
end type

type st_5 from statictext within w_verifica_data_partenza
integer x = 233
integer y = 2560
integer width = 946
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "il reparto in quella data è sotto soglia"
boolean focusrectangle = false
end type

type r_3 from rectangle within w_verifica_data_partenza
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 2711
integer y = 2508
integer width = 1605
integer height = 252
end type

