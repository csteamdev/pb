﻿$PBExportHeader$w_det_off_ven_ptenda.srw
forward
global type w_det_off_ven_ptenda from w_det_off_ven
end type
type cb_configuratore from commandbutton within w_det_off_ven_ptenda
end type
type st_margine from statictext within w_det_off_ven_ptenda
end type
end forward

global type w_det_off_ven_ptenda from w_det_off_ven
integer width = 5038
integer height = 1852
event ue_posiziona_riga ( )
event ue_post_configuratore ( )
cb_configuratore cb_configuratore
st_margine st_margine
end type
global w_det_off_ven_ptenda w_det_off_ven_ptenda

type variables


dec{4}							id_cambio_ven_ds, id_precisione_valuta
long								il_CCU
string								is_cod_valuta
end variables

forward prototypes
public function boolean wf_duplica (integer ai_row)
public function boolean wf_duplica_righe (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_off_ven_orig, long al_prog_riga_off_ven)
end prototypes

event ue_posiziona_riga();dw_det_off_ven_lista.setredraw(false)
dw_det_off_ven_lista.postevent("pcd_last")
dw_det_off_ven_lista.setrowfocusindicator(hand!)
dw_det_off_ven_lista.setredraw(true)

end event

event ue_post_configuratore();
dw_det_off_ven_lista.change_dw_current()
dw_det_off_ven_lista.postevent("pcd_retrieve")
end event

public function boolean wf_duplica (integer ai_row);/**
**/

string ls_error
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_ven_orig,  ll_prog_riga_off_ven_dupl

setpointer(HourGlass!)


ll_anno_registrazione = dw_det_off_ven_lista.getitemnumber(ai_row, "anno_registrazione")
ll_num_registrazione = dw_det_off_ven_lista.getitemnumber(ai_row, "num_registrazione")
ll_prog_riga_off_ven_orig  = dw_det_off_ven_lista.getitemnumber(ai_row, "prog_riga_off_ven")

// calcolo nuova riga
select max(prog_riga_off_ven)
into :ll_prog_riga_off_ven_dupl
from det_off_ven
where    	
	cod_azienda = :s_cs_xx.cod_azienda and  
	anno_registrazione = :ll_anno_registrazione and  
	num_registrazione = :ll_num_registrazione;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il calcolo del progressivo della nuova riga.", sqlca)
	return false
elseif sqlca.sqlcode = 100 or isnull(ll_prog_riga_off_ven_dupl) or ll_prog_riga_off_ven_dupl < 1 then
	ll_prog_riga_off_ven_dupl = 0
//else
//	ll_prog_riga_off_ven_dupl++
end if

ll_prog_riga_off_ven_dupl = int(ll_prog_riga_off_ven_dupl / 10)
if ll_prog_riga_off_ven_dupl = 0 then
	ll_prog_riga_off_ven_dupl = 10
else
	ll_prog_riga_off_ven_dupl = ll_prog_riga_off_ven_dupl * 10 + 10
end if

// Duplico riga dettaglio
insert into det_off_ven (
	cod_azienda,   
	anno_registrazione,   
	num_registrazione,   
	prog_riga_off_ven,   
	cod_tipo_det_ven,   
	cod_misura,   
	cod_prodotto,   
	des_prodotto,   
	quan_offerta,   
	prezzo_vendita,   
	fat_conversione_ven,   
	sconto_1,   
	sconto_2,   
	provvigione_1,   
	provvigione_2,   
	val_riga,   
	cod_iva,   
	data_consegna,   
	nota_dettaglio,   
	sconto_3,   
	sconto_4,   
	sconto_5,   
	sconto_6,   
	sconto_7,   
	sconto_8,   
	sconto_9,   
	sconto_10,   
	cod_centro_costo,
	anno_reg_richiesta,
	num_reg_richiesta,
	dim_x,
	dim_y,
	flag_gen_commessa)  
select 
		det_off_ven.cod_azienda,   
		:ll_anno_registrazione,   
		:ll_num_registrazione,   
		:ll_prog_riga_off_ven_dupl,   
		det_off_ven.cod_tipo_det_ven,   
		det_off_ven.cod_misura,   
		det_off_ven.cod_prodotto,   
		det_off_ven.des_prodotto,   
		det_off_ven.quan_offerta,   
		det_off_ven.prezzo_vendita,   
		det_off_ven.fat_conversione_ven,   
		det_off_ven.sconto_1,   
		det_off_ven.sconto_2,   
		det_off_ven.provvigione_1,   
		det_off_ven.provvigione_2,   
		det_off_ven.val_riga,   
		det_off_ven.cod_iva,   
		det_off_ven.data_consegna,   
		det_off_ven.nota_dettaglio,   
		det_off_ven.sconto_3,   
		det_off_ven.sconto_4,   
		det_off_ven.sconto_5,   
		det_off_ven.sconto_6,   
		det_off_ven.sconto_7,   
		det_off_ven.sconto_8,   
		det_off_ven.sconto_9,   
		det_off_ven.sconto_10,   
		det_off_ven.cod_centro_costo,
		det_off_ven.anno_reg_richiesta,
		det_off_ven.num_reg_richiesta,
		dim_x,
		dim_y,
		flag_gen_commessa
	from det_off_ven
	where
		det_off_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		det_off_ven.anno_registrazione = :ll_anno_registrazione and  
		det_off_ven.num_registrazione = :ll_num_registrazione and
		det_off_ven.prog_riga_off_ven = :ll_prog_riga_off_ven_orig;

if sqlca.sqlcode <> 0 then
	g_mb.error("Si è verificato un errore in fase di duplicazione dettagli offerte.", sqlca)
	rollback;
	return false
end if

if wf_duplica_altre_tabelle(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_ven_orig, ll_prog_riga_off_ven_dupl, ls_error) < 0 then
	g_mb.error(ls_error, sqlca)
	rollback;
	return false
end if

INSERT INTO comp_det_off_ven (
		cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		prog_riga_off_ven,   
		cod_prod_finito,   
		cod_modello,   
		dim_x,   
		dim_y,   
		dim_z,   
		dim_t,   
		cod_tessuto,   
		cod_non_a_magazzino,   
		cod_tessuto_mant,   
		cod_mant_non_a_magazzino,   
		flag_tipo_mantovana,   
		alt_mantovana,   
		flag_cordolo,   
		flag_passamaneria,   
		cod_verniciatura,   
		cod_comando,   
		pos_comando,   
		alt_asta,   
		cod_comando_tenda_verticale,   
		cod_colore_lastra,   
		cod_tipo_lastra,   
		spes_lastra,   
		cod_tipo_supporto,   
		flag_cappotta_frontale,   
		flag_rollo,   
		flag_kit_laterale,   
		flag_misura_luce_finita,   
		cod_tipo_stampo,   
		cod_mat_sis_stampaggio,   
		rev_stampo,   
		num_gambe,   
		num_campate,   
		flag_autoblocco,   
		num_fili_plisse,   
		num_boccole_plisse,   
		intervallo_punzoni_plisse,   
		num_pieghe_plisse,   
		prof_inf_plisse,   
		prof_sup_plisse,   
		colore_passamaneria,   
		colore_cordolo,   
		num_punzoni_plisse,   
		des_vernice_fc,   
		des_comando_1,   
		cod_comando_2,   
		des_comando_2,   
		flag_addizionale_comando_1,   
		flag_addizionale_comando_2,   
		flag_addizionale_supporto,   
		flag_addizionale_tessuto,   
		flag_addizionale_vernic,   
		flag_fc_comando_1,   
		flag_fc_comando_2,   
		flag_fc_supporto,   
		flag_fc_tessuto,   
		flag_fc_vernic,   
		flag_allegati,   
		data_ora_ar,   
		flag_tessuto_cliente,   
		flag_tessuto_mant_cliente,
		cod_verniciatura_2,
		flag_addizionale_vernic_2,
		flag_fc_vernic_2,
		des_vernic_fc_2,
		cod_verniciatura_3,
		flag_addizionale_vernic_3,
		flag_fc_vernic_3,
		des_vernic_fc_3,
		flag_azzera_dt,
		cod_modello_telo_finito)  
	SELECT
		:s_cs_xx.cod_azienda,   
		:ll_anno_registrazione,   
		:ll_num_registrazione,   	
		:ll_prog_riga_off_ven_dupl, 
		cod_prod_finito,  
		cod_modello,   
		dim_x,   
		dim_y,   
		dim_z,   
		dim_t,
		cod_tessuto,
		cod_non_a_magazzino,   
		cod_tessuto_mant,      
		cod_mant_non_a_magazzino,
		flag_tipo_mantovana,   
		alt_mantovana,   
		flag_cordolo,   
		flag_passamaneria,   
		cod_verniciatura,   
		cod_comando,   
		pos_comando,   
		alt_asta,   
		cod_comando_tenda_verticale,   
		cod_colore_lastra,   
		cod_tipo_lastra,   
		spes_lastra,   
		cod_tipo_supporto,   
		flag_cappotta_frontale,   
		flag_rollo,   
		flag_kit_laterale,   
		flag_misura_luce_finita,   
		cod_tipo_stampo,   
		cod_mat_sis_stampaggio,   
		rev_stampo,   
		num_gambe,   
		num_campate,   
		flag_autoblocco,   
		num_fili_plisse,   
		num_boccole_plisse,   
		intervallo_punzoni_plisse,   
		num_pieghe_plisse,   
		prof_inf_plisse,   
		prof_sup_plisse,   
		colore_passamaneria,   
		colore_cordolo,   
		num_punzoni_plisse,   
		des_vernice_fc,   
		des_comando_1,   
		cod_comando_2,   
		des_comando_2,   
		flag_addizionale_comando_1,   
		flag_addizionale_comando_2,   
		flag_addizionale_supporto,   
		flag_addizionale_tessuto,   
		flag_addizionale_vernic,   
		flag_fc_comando_1,   
		flag_fc_comando_2,   
		flag_fc_supporto,   
		flag_fc_tessuto,   
		flag_fc_vernic,   
		flag_allegati,   
		data_ora_ar,   
		flag_tessuto_cliente,   
		flag_tessuto_mant_cliente,
		cod_verniciatura_2,
		flag_addizionale_vernic_2,
		flag_fc_vernic_2,
		des_vernic_fc_2,
		cod_verniciatura_3,
		flag_addizionale_vernic_3,
		flag_fc_vernic_3,
		des_vernic_fc_3,
		flag_azzera_dt,
		cod_modello_telo_finito
	FROM comp_det_off_ven
	WHERE
			cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_registrazione and
			num_registrazione=:ll_num_registrazione and
			prog_riga_off_ven=:ll_prog_riga_off_ven_orig;
	
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante la copia della tabella comp_det_off_ven", sqlca)
	rollback;
	return false
end if

// duplico variabili configurazione
INSERT INTO det_off_ven_conf_variabili  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  prog_riga_off_ven,   
			  prog_variabile,   
			  cod_variabile,   
			  flag_tipo_dato,   
			  valore_stringa,   
			  valore_numero,   
			  valore_datetime )  
  SELECT :s_cs_xx.cod_azienda,   
			:ll_anno_registrazione,   
			:ll_num_registrazione,   	
			:ll_prog_riga_off_ven_dupl, 
			prog_variabile,   
			cod_variabile,   
			flag_tipo_dato,   
			valore_stringa,   
			valore_numero,   
			valore_datetime  
			FROM det_off_ven_conf_variabili 
			WHERE
					cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ll_anno_registrazione and
					num_registrazione=:ll_num_registrazione and
					prog_riga_off_ven=:ll_prog_riga_off_ven_orig;

if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante la copia della tabella conf_variabili", sqlca)
	rollback;
	return false
end if



// Duplico riga dettaglio statico
insert into det_off_ven_stat (
	cod_azienda,   
	anno_registrazione,   
	num_registrazione,   
	prog_riga_off_ven,   
	cod_tipo_analisi,   
	progressivo,   
	cod_statistico_1,   
	cod_statistico_2,   
	cod_statistico_3,   
	cod_statistico_4,   
	cod_statistico_5,   
	cod_statistico_6,   
	cod_statistico_7,   
	cod_statistico_8,   
	cod_statistico_9,   
	cod_statistico_10,   
	percentuale,   
	flag_trasferito)  
	select
		det_off_ven_stat.cod_azienda,
		:ll_anno_registrazione,
		:ll_num_registrazione,
		:ll_prog_riga_off_ven_dupl,
		det_off_ven_stat.cod_tipo_analisi,
		det_off_ven_stat.progressivo,
		det_off_ven_stat.cod_statistico_1,
		det_off_ven_stat.cod_statistico_2,
		det_off_ven_stat.cod_statistico_3,
		det_off_ven_stat.cod_statistico_4,
		det_off_ven_stat.cod_statistico_5,
		det_off_ven_stat.cod_statistico_6,
		det_off_ven_stat.cod_statistico_7,
		det_off_ven_stat.cod_statistico_8,   
		det_off_ven_stat.cod_statistico_9,
		det_off_ven_stat.cod_statistico_10,
		det_off_ven_stat.percentuale,   
		'N'  
	from det_off_ven_stat  
	where
		det_off_ven_stat.cod_azienda = :s_cs_xx.cod_azienda and   
		det_off_ven_stat.anno_registrazione = :ll_anno_registrazione and   
		det_off_ven_stat.num_registrazione = :ll_num_registrazione and
		det_off_ven_stat.prog_riga_off_ven = :ll_prog_riga_off_ven_orig;
		
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante la copia della tabella det_off_ven_stat", sqlca)
	rollback;
	return false
end if

if not wf_duplica_righe(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_ven_orig, ll_prog_riga_off_ven_dupl) then
	rollback;
	return false
end if

if sqlca.sqlcode <> 0 then
	g_mb.error("Si è verificato un errore in fase di duplicazione dettagli statistici.", sqlca)
	rollback;
	return false
end if

return true
end function

public function boolean wf_duplica_righe (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_off_ven_orig, long al_prog_riga_off_ven);string ls_sql, ls_error
long ll_anno_reg_cu, ll_num_reg_cu, ll_prog_riga_ord_ven_cu, ll_prog_riga

ls_sql = "select anno_registrazione, num_registrazione, prog_riga_off_ven from det_off_ven " + &
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and anno_registrazione="+string(al_anno_registrazione)+" and "+&
						"num_registrazione="+string(al_num_registrazione)+" and num_riga_appartenenza="+string(al_prog_riga_off_ven_orig)

declare cu_cursore dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_cursore;

if sqlca.sqlcode <> 0 then 
	g_mb.error("Errore nella open del cursore delle righe collegate.", sqlca)
	return false
end if

ll_prog_riga = al_prog_riga_off_ven

do while true
	fetch cu_cursore
	into :ll_anno_reg_cu, :ll_num_reg_cu, :ll_prog_riga_ord_ven_cu;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore nella fetch del cursore delle righe collegate.", sqlca)
		close cu_cursore;
		return false
	elseif sqlca.sqlcode = 100 then
		exit
	end if	
	
	ll_prog_riga += 1
	
	//duplica la riga collegata
	// Duplico riga dettaglio
	insert into det_off_ven (
		cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		prog_riga_off_ven,   
		cod_tipo_det_ven,   
		cod_misura,   
		cod_prodotto,   
		des_prodotto,   
		quan_offerta,   
		prezzo_vendita,   
		fat_conversione_ven,   
		sconto_1,   
		sconto_2,   
		provvigione_1,   
		provvigione_2,   
		val_riga,   
		cod_iva,   
		data_consegna,   
		nota_dettaglio,   
		sconto_3,   
		sconto_4,   
		sconto_5,   
		sconto_6,   
		sconto_7,   
		sconto_8,   
		sconto_9,   
		sconto_10,   
		cod_centro_costo,
		anno_reg_richiesta,
		num_reg_richiesta,
		num_riga_appartenenza,
		dim_x,
		dim_y)  
	select 
		det_off_ven.cod_azienda,   
		:al_anno_registrazione,   
		:al_num_registrazione,   
		:ll_prog_riga,   
		det_off_ven.cod_tipo_det_ven,   
		det_off_ven.cod_misura,   
		det_off_ven.cod_prodotto,   
		det_off_ven.des_prodotto,   
		det_off_ven.quan_offerta,   
		det_off_ven.prezzo_vendita,   
		det_off_ven.fat_conversione_ven,   
		det_off_ven.sconto_1,   
		det_off_ven.sconto_2,   
		det_off_ven.provvigione_1,   
		det_off_ven.provvigione_2,   
		det_off_ven.val_riga,   
		det_off_ven.cod_iva,   
		det_off_ven.data_consegna,   
		det_off_ven.nota_dettaglio,   
		det_off_ven.sconto_3,   
		det_off_ven.sconto_4,   
		det_off_ven.sconto_5,   
		det_off_ven.sconto_6,   
		det_off_ven.sconto_7,   
		det_off_ven.sconto_8,   
		det_off_ven.sconto_9,   
		det_off_ven.sconto_10,   
		det_off_ven.cod_centro_costo,
		det_off_ven.anno_reg_richiesta,
		det_off_ven.num_reg_richiesta,
		:al_prog_riga_off_ven,
		dim_x,
		dim_y
	from det_off_ven
	where
		det_off_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		det_off_ven.anno_registrazione = :ll_anno_reg_cu and  
		det_off_ven.num_registrazione = :ll_num_reg_cu and
		det_off_ven.prog_riga_off_ven = :ll_prog_riga_ord_ven_cu;

	if sqlca.sqlcode=0 then
	else
		g_mb.error("Errore in inserimento duplicato riga collegata "+string(ll_prog_riga_ord_ven_cu), sqlca)	
		close cu_cursore;
		return false
	end if
	
	if wf_duplica_altre_tabelle(ll_anno_reg_cu, ll_num_reg_cu, ll_prog_riga_ord_ven_cu, ll_prog_riga, ls_error) < 0 then
		g_mb.error(ls_error, sqlca)
		rollback;
		return false
	end if

	// Duplico riga dettaglio statico
	insert into det_off_ven_stat (
		cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		prog_riga_off_ven,   
		cod_tipo_analisi,   
		progressivo,   
		cod_statistico_1,   
		cod_statistico_2,   
		cod_statistico_3,   
		cod_statistico_4,   
		cod_statistico_5,   
		cod_statistico_6,   
		cod_statistico_7,   
		cod_statistico_8,   
		cod_statistico_9,   
		cod_statistico_10,   
		percentuale,   
		flag_trasferito)  
		select
			det_off_ven_stat.cod_azienda,
			:al_anno_registrazione,
			:al_num_registrazione,
			:ll_prog_riga,
			det_off_ven_stat.cod_tipo_analisi,
			det_off_ven_stat.progressivo,
			det_off_ven_stat.cod_statistico_1,
			det_off_ven_stat.cod_statistico_2,
			det_off_ven_stat.cod_statistico_3,
			det_off_ven_stat.cod_statistico_4,
			det_off_ven_stat.cod_statistico_5,
			det_off_ven_stat.cod_statistico_6,
			det_off_ven_stat.cod_statistico_7,
			det_off_ven_stat.cod_statistico_8,   
			det_off_ven_stat.cod_statistico_9,
			det_off_ven_stat.cod_statistico_10,
			det_off_ven_stat.percentuale,   
			'N'  
		from det_off_ven_stat  
		where
			det_off_ven_stat.cod_azienda = :s_cs_xx.cod_azienda and   
			det_off_ven_stat.anno_registrazione = :al_anno_registrazione and   
			det_off_ven_stat.num_registrazione = :al_num_registrazione and
			det_off_ven_stat.prog_riga_off_ven = :ll_prog_riga_ord_ven_cu;
loop

close cu_cursore;

return true
end function

on w_det_off_ven_ptenda.create
int iCurrent
call super::create
this.cb_configuratore=create cb_configuratore
this.st_margine=create st_margine
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_configuratore
this.Control[iCurrent+2]=this.st_margine
end on

on w_det_off_ven_ptenda.destroy
call super::destroy
destroy(this.cb_configuratore)
destroy(this.st_margine)
end on

event activate;call super::activate;dw_det_off_ven_lista.setrowfocusindicator(hand!)
end event

event pc_setwindow;call super::pc_setwindow;// mi posizioni sull'ultima riga
postevent("ue_posiziona_riga")



guo_functions.uof_get_parametro( "CCU", il_CCU)
if isnull(il_CCU) or il_CCU<0 then il_CCU = 0

end event

type cb_sconti from w_det_off_ven`cb_sconti within w_det_off_ven_ptenda
end type

type uo_1 from w_det_off_ven`uo_1 within w_det_off_ven_ptenda
end type

type cb_c_industriale from w_det_off_ven`cb_c_industriale within w_det_off_ven_ptenda
end type

type cb_corrispondenze from w_det_off_ven`cb_corrispondenze within w_det_off_ven_ptenda
end type

type dw_documenti from w_det_off_ven`dw_documenti within w_det_off_ven_ptenda
end type

type dw_det_off_ven_det_1 from w_det_off_ven`dw_det_off_ven_det_1 within w_det_off_ven_ptenda
event ue_prezzo_bloccato ( )
end type

event dw_det_off_ven_det_1::itemchanged;call super::itemchanged;

if row>0 then
else
	return
end if


choose case dwo.name
	case "cod_prodotto"
		if data<>"" then
			dw_det_off_ven_lista.event post ue_costo_ultimo(row)
		end if
	
	case "prezzo_vendita"
		dw_det_off_ven_lista.event post ue_costo_ultimo(row)
		
	case "quan_offerta"
		dw_det_off_ven_lista.event post ue_costo_ultimo(row)
		
	case "sconto_1","sconto_2","sconto_3","sconto_4","sconto_5","sconto_6","sconto_7","sconto_8","sconto_9","sconto_10"
		dw_det_off_ven_lista.event post ue_costo_ultimo(row)
		
end choose
end event

type dw_det_off_ven_lista from w_det_off_ven`dw_det_off_ven_lista within w_det_off_ven_ptenda
event ue_costo_ultimo ( long al_row )
event ue_blocco_prodotto ( datetime data_registrazione,  long row,  dwobject dwo,  string data )
integer width = 4864
end type

event dw_det_off_ven_lista::ue_costo_ultimo(long al_row);uo_trova_prezzi							luo_prezzi
uo_calcola_documento_euro			luo_calcola_doc
dec{4}										ld_costo_acquisto, ld_null[], ld_imponibile_riga, ld_imponibile_riga_valuta, ld_quantita, ld_prezzo
datetime										ldt_data_fine
string											ls_cod_prodotto, ls_errore
integer										li_ret


if il_CCU>0 and al_row>0 then
else
	st_margine.text = ""
	return
end if

ldt_data_fine = datetime(today(), 00:00:00)
ls_cod_prodotto = dw_det_off_ven_lista.getitemstring(al_row, "cod_prodotto")

if g_str.isempty(ls_cod_prodotto) then 
	st_margine.text = ""
	return
end if

luo_prezzi = create uo_trova_prezzi
li_ret = luo_prezzi.uof_ultimo_acquisto("F", ldt_data_fine, il_CCU, ls_cod_prodotto, "", ld_costo_acquisto, ls_errore)
destroy luo_prezzi

if li_ret < 0 then
	st_margine.text = ""
	return
end if


if isnull(ld_costo_acquisto) then ld_costo_acquisto = 0

if ld_costo_acquisto = 0 then
	
	//prendi il costo standard
	select costo_standard
	into :ld_costo_acquisto
	from anag_prodotti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:ls_cod_prodotto;
	
	if isnull(ld_costo_acquisto) then ld_costo_acquisto = 0
				
	st_margine.text = "Costo Standard = "
else
	st_margine.text = "Costo Ultimo = "
end if

st_margine.text += string(ld_costo_acquisto, "#####0.0000")

ld_prezzo = dw_det_off_ven_lista.getitemnumber(al_row, "prezzo_vendita")
ld_quantita = dw_det_off_ven_lista.getitemnumber(al_row, "quan_offerta")

if isnull(ld_prezzo) then ld_prezzo = 0
if isnull(ld_quantita) then ld_quantita = 0

if ld_prezzo>0 then
	luo_calcola_doc = create uo_calcola_documento_euro
	
	luo_calcola_doc.il_anno_registrazione = dw_det_off_ven_lista.i_parentdw.getitemnumber(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "anno_registrazione")
	luo_calcola_doc.il_num_registrazione = dw_det_off_ven_lista.i_parentdw.getitemnumber(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "num_registrazione")
	luo_calcola_doc.is_tipo_documento = "off_ven"
	luo_calcola_doc.is_cod_valuta = is_cod_valuta
	luo_calcola_doc.id_cambio = id_cambio_ven_ds
	luo_calcola_doc.is_colonna_quantita = "quan_offerta"
	luo_calcola_doc.is_colonna_prog_riga = "prog_riga_off_ven"
	luo_calcola_doc.id_precisione = id_precisione_valuta
	
	li_ret = luo_calcola_doc.uof_get_imponibile_riga(	"dw", al_row, dw_det_off_ven_lista, "", 0, 0, ld_null[], 0, "", ld_imponibile_riga, ld_imponibile_riga_valuta, ls_errore)
	destroy luo_calcola_doc
	
	if li_ret < 0 then
		return
	end if
	
	st_margine.text += "    -    Margine = " + string(ld_imponibile_riga - ld_costo_acquisto * ld_quantita, "#####0.0000")
	
else
	st_margine.text += "    -    margine = 0.0000"
end if



return
end event

event dw_det_off_ven_lista::ue_blocco_prodotto(datetime data_registrazione, long row, dwobject dwo, string data);string ls_messaggio, ls_null
long li_ret
uo_fido_cliente luo_fido_cliente

setnull(ls_null)

luo_fido_cliente = create uo_fido_cliente
luo_fido_cliente = create uo_fido_cliente
li_ret = luo_fido_cliente.uof_get_blocco_prodotto( data, data_registrazione, is_cod_parametro_blocco_prodotto, ls_messaggio)
if li_ret=2 then
	//blocco
	g_mb.error(ls_messaggio)
	this.setitem(row, "cod_prodotto", ls_null)
	this.SetColumn ( "cod_prodotto" )
	destroy luo_fido_cliente
	return
elseif li_ret=1 then
	//solo warning
	g_mb.warning(ls_messaggio)
end if
destroy luo_fido_cliente
return
end event

event dw_det_off_ven_lista::updatestart;//OCIO: MANTERNERE DESELEZIONATO il flag "EXTEND ANCESTOR SCRIPT"

if i_extendmode then
   long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_prog_riga_off_ven, ll_i4
   double ld_sconto_testata
   string ls_cod_pagamento, ls_cod_valuta, ls_tabella, ls_quan_documento, ls_cod_prodotto, & 
			 ls_tot_val_documento, ls_nome_prog, ls_aspetto_beni, ls_des_imballo, ls_flag_prezzo_bloccato


	ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	ld_sconto_testata = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "sconto")
	ls_cod_pagamento = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_pagamento")
	ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
	ls_aspetto_beni = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "aspetto_beni")
	ll_prog_riga_off_ven = getitemnumber(getrow(),"prog_riga_off_ven")
	ls_tabella = "tes_off_ven"
	ls_quan_documento = "quan_offerta"
	ls_tot_val_documento = "tot_val_offerta"

	dw_det_off_ven_lista.change_dw_current()
	
//	cb_prodotti_ricerca.enabled = false
	cb_sconti.enabled = false

	for ll_i = 1 to this.rowcount()
		ls_cod_prodotto = this.getitemstring(ll_i, "cod_prodotto")
		select des_imballo
		 into  :ls_des_imballo
		 from  tab_imballi
		 where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_imballo = (select cod_imballo 
				 					 from	  anag_prodotti
									 where  cod_azienda = :s_cs_xx.cod_azienda and
									 		  cod_prodotto = :ls_cod_prodotto);

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Errore Durante l'Estrazione Descrizione Imballo.", &
							exclamation!, ok!)
			return 1
		end if

		if (pos(ls_aspetto_beni, ls_des_imballo) = 0 or isnull(ls_aspetto_beni) or ls_aspetto_beni = "") and &
			ls_des_imballo <> "" and not isnull(ls_des_imballo) then
			if len(ls_aspetto_beni) > 0 then
				ls_aspetto_beni = ls_aspetto_beni + " - " + ls_des_imballo
			else
				ls_aspetto_beni = ls_des_imballo
			end if
		end if
		
		
		ls_flag_prezzo_bloccato = getitemstring(ll_i, "flag_prezzo_bloccato") 
		if ls_flag_prezzo_bloccato <> getitemstring(ll_i, "flag_prezzo_bloccato", primary!, true) and ( isnull( getitemnumber(ll_i, "num_riga_appartenenza")) or  getitemnumber(ll_i, "num_riga_appartenenza") = 0 )  then
			for ll_i4 = 1 to rowcount()
				if getitemnumber(ll_i4, "num_riga_appartenenza") = getitemnumber(ll_i, "prog_riga_off_ven") then
					setitem(ll_i4, "flag_prezzo_bloccato", ls_flag_prezzo_bloccato)
				end if
			next
			g_mb.warning("PREZZO BLOCCATO aggiornato nelle righe di appartenenza optionals/addizionali")		
		end if
		
	next

	update tes_off_ven
	set	 aspetto_beni = :ls_aspetto_beni
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Attenzione", "Errore Durante l'Aggiornamento Imballo Offerta.", exclamation!, ok!)
		return 1
	end if

	ls_tabella = "det_off_ven_stat"
	ls_nome_prog = "prog_riga_off_ven"
	
   for ll_i = 1 to this.deletedcount()
		ll_prog_riga_off_ven = this.getitemnumber(ll_i, "prog_riga_off_ven", delete!, true)		
      if f_elimina_det_stat(ls_tabella, ll_anno_registrazione, ll_num_registrazione,ls_nome_prog,ll_prog_riga_off_ven) = -1 then
			return 1
		end if
		
		delete varianti_det_off_ven
		where  cod_azienda=:s_cs_xx.cod_azienda and
			    anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_off_ven =: ll_prog_riga_off_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione varianti offerte",stopsign!)
			return 1
		end if
		
		
		delete tab_mp_non_ric_comp_det_off
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione
		and    prog_riga_off_ven =: ll_prog_riga_off_ven;
		if sqlca.sqlcode < 0 then
			g_mb.error("Apice","Errore durante cancellazione tabella MP automatiche offerte")
			return 1
		end if
		
		delete tab_optionals_comp_det_off
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione
		and    prog_riga_off_ven =: ll_prog_riga_off_ven;
		if sqlca.sqlcode < 0 then
			g_mb.error("Apice","Errore durante cancellazione tabella tab_optionals_comp_det_off")
			return 1
		end if
	
		delete comp_det_off_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione
		and    prog_riga_off_ven =: ll_prog_riga_off_ven;
		if sqlca.sqlcode < 0 then
			g_mb.error("Apice","Errore durante cancellazione dettagli complementari offerte")
			return 1
		end if
		
		delete det_off_ven_conf_variabili
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione
		and    prog_riga_off_ven =: ll_prog_riga_off_ven;
		if sqlca.sqlcode < 0 then
			g_mb.error("Apice","Errore durante cancellazione variabili del configuratore")
			return 1
		end if
		
		delete det_ord_ven_note		
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione = :ll_num_registrazione
		and    prog_riga_ord_ven =: ll_prog_riga_off_ven;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione note dettaglio offerte")
			return 1
		end if
		
		delete det_off_ven_corrispondenze
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :ll_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione
		and    prog_riga_off_ven  =: ll_prog_riga_off_ven;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione corrispondenze dettaglio offerte")
			return 1
		end if	
		
   next
end if

wf_ricalcola()

end event

event dw_det_off_ven_lista::getfocus;call super::getfocus;dw_det_off_ven_lista.setrowfocusindicator(hand!)
end event

event dw_det_off_ven_lista::itemchanged;//	*****************************************************************************
//
//
//			ATTENZIONE: IN QUESTO EVENTO TENERE DISATTIVATO IL CHECK EXTEND ANCESTOR SCRIPT
//
//
//	*****************************************************************************

if i_extendmode then
	
	datawindow ld_datawindow
	
	commandbutton lc_prodotti_ricerca
	
	picturebutton lp_prod_view
	
	string	ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_des_prodotto, ls_cod_tipo_listino_prodotto, ls_cod_cliente, ls_cod_valuta, &
			ls_flag_tipo_det_ven, ls_modify, ls_null, ls_cod_agente_1, ls_messaggio, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_flag_decimali, ls_cod_contatto, &
			ls_cod_misura_mag, ls_nota_prodotto, ls_cod_versione, ls_colonna_sconto, ls_flag_esplodi_in_doc, ls_flag_prezzo
			
	double	ld_fat_conversione, ld_quantita, ld_cambio_ven, ld_variazioni[], ld_ultimo_prezzo, ll_sconti[], ll_maggiorazioni[], &
				ld_min_fat_altezza,ld_min_fat_larghezza,ld_min_fat_profondita,ld_min_fat_superficie,ld_min_fat_volume, ld_quan_proposta
				
	dec{4}	ld_dim_x, ld_dim_y, ld_num_confezioni, ld_num_pezzi
	
	datetime ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva
	
	long	ll_riga_origine, ll_i, ll_y, ll_z, ll_num_confezione, ll_num_pezzi_confezioni, li_ret


	setnull(ls_null)


	if row < 1 then
		return
	end if

	ls_cod_tipo_listino_prodotto = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
	ldt_data_registrazione = dw_det_off_ven_lista.i_parentdw.getitemdatetime(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
	ls_cod_cliente = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
	ls_cod_contatto = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_contatto")
	ls_cod_valuta = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
	ld_cambio_ven = dw_det_off_ven_lista.i_parentdw.getitemnumber(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
	ls_cod_agente_1 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
	ls_cod_agente_2 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
	
	
	ls_cod_tipo_det_ven = this.getitemstring(this.getrow(),"cod_tipo_det_ven")
	if not isnull(this.getitemdatetime(row, "data_consegna")) then
		ldt_data_consegna = this.getitemdatetime(row, "data_consegna")
	else
		ldt_data_consegna = dw_det_off_ven_lista.i_parentdw.getitemdatetime(dw_det_off_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
	end if
	
   
	choose case dwo.name
		//#############################################################################
		case "prog_riga_off_ven"
			ll_riga_origine = this.getrow()
			
			for ll_i = 1 to this.rowcount()
				
				if ll_i <> ll_riga_origine and long(data) = this.getitemnumber(ll_i, "prog_riga_off_ven") then
					g_mb.messagebox("Attenzione", "Progressivo riga già utilizzato.", exclamation!, ok!)
               		data = string(this.getitemnumber(ll_riga_origine, "prog_riga_off_ven", primary!,true))
               		this.setitem(ll_riga_origine, "prog_riga_off_ven", double(data))
               		this.settext(data)
               		return 2
					pcca.error = c_fatal
				end if
			next
		
		
		//#############################################################################
		case "cod_tipo_det_ven"
			ls_modify = "cod_prodotto.protect='0'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "cod_prodotto.background.color='16777215'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "cod_versione.protect='0'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "cod_versione.background.color='16777215'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "des_prodotto.protect='0'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "des_prodotto.background.color='16777215'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "cod_misura.protect='0'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "cod_misura.background.color='16777215'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "fat_conversione_ven.protect='0'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "fat_conversione_ven.background.color='16777215'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "quan_offerta.protect='0'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "quan_offerta.background.color='16777215'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "prezzo_vendita.protect='0'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "prezzo_vendita.background.color='16777215'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_1.protect='0'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_1.background.color='16777215'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_2.protect='0'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_2.background.color='16777215'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "cod_iva.protect='0'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "cod_iva.background.color='16777215'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_1.protect='0'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_1.background.color='16777215'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_2.protect='0'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_2.background.color='16777215'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "data_consegna.protect='0'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "data_consegna.background.color='16777215'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "cod_centro_costo.protect='0'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
			ls_modify = "cod_centro_costo.background.color='16777215'~t"
			dw_det_off_ven_det_1.modify(ls_modify)
   
			ld_datawindow = dw_det_off_ven_det_1
			setnull(lc_prodotti_ricerca)
			setnull(lp_prod_view)
			f_tipo_dettaglio_ven(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, data, ls_cod_agente_1, ls_cod_agente_2)
			ld_datawindow = dw_det_off_ven_lista
			f_tipo_dettaglio_ven_lista(this, data)


			if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
				
				select tab_tipi_det_ven.cod_iva  
				into   :ls_cod_iva  
				from   tab_tipi_det_ven  
				where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_tipi_det_ven.cod_tipo_det_ven = :data;
					 
				if sqlca.sqlcode = 0 then
					this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
				end if
				
			end if
			
			dw_det_off_ven_det_1.setitem(dw_det_off_ven_det_1.getrow(), "nota_dettaglio", ls_null)
			
			select quan_default
			into :ld_quan_proposta
			from tab_tipi_det_ven
			where cod_azienda = :s_cs_xx.cod_azienda 
			and cod_tipo_det_ven = :data;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice", "Errore in lettura dati da tabella tab_tipi_det_ven " + sqlca.sqlerrtext)
				return
			end if
		
			if isnull(ld_quan_proposta) then
				ld_quan_proposta = 0
			end if
			
			setitem(getrow(), "quan_offerta", ld_quan_proposta)			
			
			setitem(getrow(), "dim_x", 0)			
			setitem(getrow(), "dim_y", 0)
			
		
		//#############################################################################
		case "cod_prodotto"
			
			if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1
			
			select cod_misura_mag,
					cod_misura_ven,
					fat_conversione_ven,
					flag_decimali,
					cod_iva,
					pezzi_collo,
					des_prodotto
			into   		:ls_cod_misura_mag,
						:ls_cod_misura,
						:ld_fat_conversione,
						:ls_flag_decimali,
						:ls_cod_iva,
						:ll_num_pezzi_confezioni,
						:ls_des_prodotto
			from   	anag_prodotti
			where  	cod_azienda = :s_cs_xx.cod_azienda and 
						cod_prodotto = :data;
   
			if sqlca.sqlcode = 0 then
				
				this.setitem(row, "cod_misura", ls_cod_misura)
				this.setitem(row, "des_prodotto", ls_des_prodotto)
				
				if ld_fat_conversione <> 0 then
					this.setitem(row, "fat_conversione_ven", ld_fat_conversione)
				else
					ld_fat_conversione = 1
					this.setitem(row, "fat_conversione_ven", 1)
				end if
				
				if not isnull(ls_cod_iva) then
					this.setitem(row, "cod_iva", ls_cod_iva)
				end if
				
				ls_cod_prodotto = data
				uo_1.uof_aggiorna(ls_cod_prodotto)
				uo_1.uof_ultimo_prezzo(ls_cod_cliente, ls_cod_prodotto)
				
				event post ue_costo_ultimo(row)
			else
				//cioè sqlca.sqlcode <>0 ....
				this.change_dw_current()
				guo_ricerca.uof_ricerca_prodotto( dw_det_off_ven_lista, "cod_prodotto")
				return
			end if

			
			dw_det_off_ven_lista.postevent("ue_postopen")
			
			if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				
				dw_det_off_ven_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(ld_fat_conversione)) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
			else
				dw_det_off_ven_det_1.modify("st_fattore_conv.text=''")
			end if


			if not isnull(ls_cod_cliente) then
				select anag_clienti.cod_iva,
						 anag_clienti.data_esenzione_iva
				into   :ls_cod_iva,
						 :ldt_data_esenzione_iva
				from   anag_clienti
				where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
						 anag_clienti.cod_cliente = :ls_cod_cliente;
			else
				select anag_contatti.cod_iva,
						 anag_contatti.data_esenzione_iva
				into   :ls_cod_iva,
						 :ldt_data_esenzione_iva
				from   anag_contatti
				where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
						 anag_contatti.cod_contatto = :ls_cod_contatto;
			end if
			
			//sqlcode dell'ultima select da anag_clienti o da anag_contatti
			if sqlca.sqlcode = 0 then
				if ls_cod_iva <> "" and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
					this.setitem(row, "cod_iva", ls_cod_iva)
				end if
			end if


			ls_cod_prodotto = data
			ld_quantita = this.getitemnumber(row, "quan_offerta")
				
			if ls_flag_decimali = "N" and ld_quantita - int(ld_quantita) > 0 then
				ld_quantita = ceiling(ld_quantita)
				this.setitem(row, "quan_offerta", ld_quantita)
			end if
			

			if mid(f_flag_controllo(),1,1) = "S"  and getitemstring(row, "flag_prezzo_bloccato") = "N" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if

			if f_leggi_nota_prodotto(ls_cod_tipo_det_ven, ls_cod_prodotto, ls_nota_prodotto) = 0 then
				dw_det_off_ven_det_1.setitem(row, "nota_dettaglio", ls_nota_prodotto)
			else
				dw_det_off_ven_det_1.setitem(row, "nota_dettaglio", ls_null)
			end if
	
			f_PO_LoadDDDW_DW(dw_det_off_ven_det_1,"cod_versione",sqlca,&
								  "distinta_padri","cod_versione","des_versione",&
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + data + "'")

			uo_default_prodotto luo_default_prodotto
			luo_default_prodotto = CREATE uo_default_prodotto
			
			if luo_default_prodotto.uof_flag_gen_commessa(data) = 0 then
				setitem(getrow(),"cod_versione",luo_default_prodotto.is_cod_versione)
				setitem(getrow(),"flag_gen_commessa",luo_default_prodotto.is_flag_gen_commessa)
			end if
			
			destroy luo_default_prodotto
		
			dw_det_off_ven_det_1.postevent("ue_ricalcola_colonne")
			
			setitem(getrow(), "dim_x", 0)			
			setitem(getrow(), "dim_y", 0)

		
		//#############################################################################
		case "quan_offerta"
			
			ls_cod_prodotto = this.getitemstring(row, "cod_prodotto")
			ld_dim_x = this.getitemnumber(row, "dim_x")
			ld_dim_y = this.getitemnumber(row, "dim_y")
			
			select anag_prodotti.flag_decimali
			into   :ls_flag_decimali
			from   anag_prodotti
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode = 0 then
				if ls_flag_decimali = "N" and (double(data) - int(double(data))) > 0 then
					data = string(ceiling(double(data)))
					this.setitem(row, "quan_offerta", double(data))
					this.settext(data)
					return 2
				end if
			end if
			
			ld_quantita = double(data)
				
			if mid(f_flag_controllo(),2,1) = "S" and getitemstring(row, "flag_prezzo_bloccato") = "N"  then
				
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
				
			end if
			
			try
				iuo_gestione_conversioni.uof_ricalcola_quantita(	dw_det_off_ven_det_1, "off_ven", "quan_doc", &
																				ld_quantita, &
																				dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"prezzo_vendita"), &
																				dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"quantita_um"), &
																				dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"prezzo_um"), &
																				dw_det_off_ven_det_1.getitemnumber(dw_det_off_ven_det_1.getrow(),"fat_conversione_ven"), &
																				ls_cod_valuta)
			catch (RuntimeError ex)
			end try
			
			event post ue_costo_ultimo(this.getrow())
			
		//#############################################################################
		
		case "data_consegna"
			
			if mid(f_flag_controllo(),4,1) = "S"  and getitemstring(row, "flag_prezzo_bloccato") = "N"  then
				ls_cod_prodotto = this.getitemstring(row, "cod_prodotto")
				ld_quantita = this.getitemnumber(row, "quan_offerta")
				ld_dim_x = this.getitemnumber(row, "dim_x")
				ld_dim_y = this.getitemnumber(row, "dim_y")
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = datetime(data)
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
		
		//#############################################################################
		case "sconto_1", "sconto_2", "sconto_3", "sconto_4", "sconto_5", "sconto_6", "sconto_7", "sconto_8", "sconto_9", "sconto_10"
			
			if mid(f_flag_controllo(),9,1) = "S"  and getitemstring(row, "flag_prezzo_bloccato") = "N" then
				ld_dim_x = this.getitemnumber(row, "dim_x")
				ld_dim_y = this.getitemnumber(row, "dim_y")
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.prezzo_listino = getitemnumber(getrow(),"prezzo_vendita")
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_offerta")
				iuo_condizioni_cliente.ib_prezzi = false
				
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(data)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
				
			end if
			
			event post ue_costo_ultimo(this.getrow())
			
		
		//#############################################################################
		case "prezzo_vendita"
			
			if mid(f_flag_controllo(),3,1) = "S" and getitemstring(row, "flag_prezzo_bloccato") = "N" then
				ld_dim_x = this.getitemnumber(row, "dim_x")
				ld_dim_y = this.getitemnumber(row, "dim_y")
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.prezzo_listino = double(data)
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_offerta")
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.ib_prezzi = false
				
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(data)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
				
			end if
			
			event post ue_costo_ultimo(row)
		
		
		//#############################################################################
		case "cod_versione"

			// Gestione del KIT prodotto.
			if not isnull(data) and len(data) > 0 then
			
				ls_cod_prodotto = this.getitemstring(row, "cod_prodotto")
				ls_flag_esplodi_in_doc = "N"
				
				select flag_esplodi_in_doc
				into   :ls_flag_esplodi_in_doc
				from   distinta_padri
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto and
						 cod_versione = :data and
						 flag_blocco = 'N';
				
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("APICE","Errore in ricerca ed esplosione KIT prodotti", Information!)
				end if
				
				if sqlca.sqlcode = 0 and ls_flag_esplodi_in_doc ="S" then
					// si tratta di un KIT di prodotti e quindi mi fermo per inserire il KIT
					postevent("ue_inserisci_kit")
					return 0
				end if
				
			end if
		
		
		//#############################################################################
		case "dim_x"
			
			if this.getitemstring(row, "flag_prezzo_bloccato") = "N" then
				ls_cod_prodotto = this.getitemstring(row, "cod_prodotto")
				ld_quantita = this.getitemnumber(row, "quan_offerta")
				ld_dim_x = dec(data)
				ld_dim_y = this.getitemnumber(row, "dim_y")
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = datetime(data)
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
		
		
		//#############################################################################
		case "dim_y"
			
			if this.getitemstring(row, "flag_prezzo_bloccato") = "N" then
				ls_cod_prodotto = this.getitemstring(row, "cod_prodotto")
				ld_quantita = this.getitemnumber(row, "quan_offerta")
				ld_dim_x = this.getitemnumber(row, "dim_x")
				ld_dim_y = dec(data)
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				if mid(f_flag_controllo(),7,1) = "S" then
					iuo_condizioni_cliente.str_parametri.data_riferimento = datetime(data)
				else
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				end if
				
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
		case "num_confezioni"
				ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
				ld_quantita = this.getitemnumber(this.getrow(),"quan_offerta")
				ld_num_confezioni = dec(i_coltext)
				ld_num_pezzi  = this.getitemnumber(this.getrow(),"num_pezzi_confezione")
				ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")
				select flag_prezzo
				into   :ls_flag_prezzo
				from   tab_misure
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_misura = :ls_cod_misura;
				if sqlca.sqlcode = 0 then
					if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
						if ls_flag_prezzo = 'S' then
							ld_quantita = ld_num_pezzi * ld_num_confezioni
							this.setitem(i_rownbr, "quan_offerta", ld_quantita)
						else
							ld_quantita = ld_num_confezioni
							this.setitem(i_rownbr, "quan_offerta", ld_quantita)
						end if
					end if
				else
					g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
					return 1
				end if		
				if mid(f_flag_controllo(),6,1) = "S" then
					iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
					iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
					iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
					iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
					if mid(f_flag_controllo(),7,1) = "S" then
						iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
					else
						iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
					end if
					iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
					iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
					iuo_condizioni_cliente.str_parametri.dim_1 = 0
					iuo_condizioni_cliente.str_parametri.dim_2 = 0
					iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
					iuo_condizioni_cliente.str_parametri.valore = 0
					iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
					iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
					iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
					iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
					iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
					iuo_condizioni_cliente.wf_condizioni_cliente()
				end if
				
		case "num_pezzi_confezione"
				ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
				ld_quantita = this.getitemnumber(this.getrow(),"quan_offerta")
				ld_num_pezzi = double(i_coltext)
				ld_num_confezioni = this.getitemnumber(this.getrow(),"num_confezioni")
				ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")
				select flag_prezzo
				into   :ls_flag_prezzo
				from   tab_misure
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_misura = :ls_cod_misura;
				if sqlca.sqlcode = 0 then
					if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
						if ls_flag_prezzo = 'S' then
							ld_quantita = ld_num_pezzi * ld_num_confezioni
							this.setitem(i_rownbr, "quan_offerta", ld_quantita)
						else
							ld_quantita = ld_num_confezioni
							this.setitem(i_rownbr, "quan_offerta", ld_quantita)
						end if
					end if
				else
					g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
					return 1
				end if				
				if mid(f_flag_controllo(),5,1) = "S" then
					iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
					iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
					iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
					iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
					if mid(f_flag_controllo(),7,1) = "S" then
						iuo_condizioni_cliente.str_parametri.data_riferimento = getitemdatetime(getrow(),"data_consegna")
					else
						iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
					end if
					iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
					iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
					iuo_condizioni_cliente.str_parametri.dim_1 = 0
					iuo_condizioni_cliente.str_parametri.dim_2 = 0
					iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
					iuo_condizioni_cliente.str_parametri.valore = 0
					iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
					iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
					iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
					iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_offerta"
					iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
					iuo_condizioni_cliente.wf_condizioni_cliente()
				end if
	end choose
end if

end event

event dw_det_off_ven_lista::rowfocuschanged;call super::rowfocuschanged;dw_det_off_ven_det_1.postevent("ue_prezzo_bloccato")


if this.getrow() > 0 then
	event trigger ue_costo_ultimo(this.getrow())
end if
end event

event dw_det_off_ven_lista::pcd_retrieve;call super::pcd_retrieve;


is_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
id_cambio_ven_ds = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "cambio_ven")
select precisione
into   :id_precisione_valuta
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :is_cod_valuta;
end event

type dw_folder from w_det_off_ven`dw_folder within w_det_off_ven_ptenda
integer width = 4928
end type

type cb_configuratore from commandbutton within w_det_off_ven_ptenda
integer x = 2962
integer y = 1544
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Configurat."
end type

event clicked;string ls_cod_pagamento
long ll_i, ll_anno_commessa, ll_num_commessa, ll_cont
double ld_sconto

str_conf_prodotto.anno_documento = dw_det_off_ven_lista.getitemnumber(dw_det_off_ven_lista.getrow(),"anno_registrazione")
str_conf_prodotto.num_documento = dw_det_off_ven_lista.getitemnumber(dw_det_off_ven_lista.getrow(),"num_registrazione")
str_conf_prodotto.prog_riga_documento = dw_det_off_ven_lista.getitemnumber(dw_det_off_ven_lista.getrow(),"prog_riga_off_ven")

select count(*)
into   :ll_cont
from   comp_det_off_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :str_conf_prodotto.anno_documento and
		num_registrazione = :str_conf_prodotto.num_documento and
		prog_riga_off_ven = :str_conf_prodotto.prog_riga_documento;
if isnull(ll_cont) or ll_cont = 0 then
	g_mb.messagebox("APICE","Impossibile entrare nel configuratore: la riga non era stata generata dal configuratore")
	return
end if

str_conf_prodotto.cod_cliente = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.getrow(),"cod_cliente")
str_conf_prodotto.cod_contatto = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.getrow(),"cod_contatto")
str_conf_prodotto.cod_agente_1 = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.getrow(),"cod_agente_1")
str_conf_prodotto.cod_tipo_listino_prodotto = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.getrow(),"cod_tipo_listino_prodotto")
str_conf_prodotto.cod_valuta = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.getrow(),"cod_valuta")
str_conf_prodotto.data_documento = dw_det_off_ven_lista.i_parentdw.getitemdatetime(dw_det_off_ven_lista.i_parentdw.getrow(),"data_registrazione")
str_conf_prodotto.data_registrazione = dw_det_off_ven_lista.i_parentdw.getitemdatetime(dw_det_off_ven_lista.i_parentdw.getrow(),"data_registrazione")
str_conf_prodotto.sconto_testata = dw_det_off_ven_lista.i_parentdw.getitemnumber(dw_det_off_ven_lista.i_parentdw.getrow(),"sconto")
str_conf_prodotto.tipo_gestione = "OFF_VEN"
str_conf_prodotto.cod_modello = dw_det_off_ven_lista.getitemstring(dw_det_off_ven_lista.getrow(),"cod_prodotto")
str_conf_prodotto.cod_versione = dw_det_off_ven_lista.getitemstring(dw_det_off_ven_lista.getrow(),"cod_versione")
str_conf_prodotto.data_consegna = dw_det_off_ven_lista.i_parentdw.getitemdatetime(dw_det_off_ven_lista.i_parentdw.getrow(),"data_consegna")
ls_cod_pagamento = dw_det_off_ven_lista.i_parentdw.getitemstring(dw_det_off_ven_lista.i_parentdw.getrow(),"cod_pagamento")
if isnull(ls_cod_pagamento) then
	str_conf_prodotto.sconto_pagamento = 0
else
	select sconto
	into   :ld_sconto
	from   tab_pagamenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	      cod_pagamento = :ls_cod_pagamento;
	if sqlca.sqlcode = 0 and not isnull(ld_sconto) then
		str_conf_prodotto.sconto_pagamento = ld_sconto
	else
		str_conf_prodotto.sconto_pagamento = 0
	end if
end if
for ll_i = 1 to 10 
	str_conf_prodotto.sconti[ll_i] = dw_det_off_ven_lista.getitemnumber(dw_det_off_ven_lista.getrow(),"sconto_" + string(ll_i))
	if isnull(str_conf_prodotto.sconti[ll_i]) then str_conf_prodotto.sconti[ll_i] = 0
next

str_conf_prodotto.provvigione_1 = dw_det_off_ven_lista.getitemnumber(dw_det_off_ven_lista.getrow(),"provvigione_1")
if isnull(str_conf_prodotto.provvigione_1) then str_conf_prodotto.provvigione_1 = 0
str_conf_prodotto.provvigione_2 = dw_det_off_ven_lista.getitemnumber(dw_det_off_ven_lista.getrow(),"provvigione_2")
if isnull(str_conf_prodotto.provvigione_2) then str_conf_prodotto.provvigione_2 = 0

str_conf_prodotto.nota = dw_det_off_ven_lista.getitemstring(dw_det_off_ven_lista.getrow(),"nota_dettaglio")
setnull(str_conf_prodotto.cod_prodotto_finito)
str_conf_prodotto.stato = "M"
str_conf_prodotto.flag_cambio_modello = false	

openwithparm(w_configuratore, parent)


end event

type st_margine from statictext within w_det_off_ven_ptenda
integer x = 114
integer y = 1540
integer width = 2843
integer height = 64
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean focusrectangle = false
end type

