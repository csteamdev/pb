﻿$PBExportHeader$w_report_off_ven_euro.srw
$PBExportComments$Finestra Report Offerte Vendita
forward
global type w_report_off_ven_euro from w_cs_xx_principale
end type
type dw_report_off_ven from uo_cs_xx_dw within w_report_off_ven_euro
end type
end forward

global type w_report_off_ven_euro from w_cs_xx_principale
integer width = 5056
integer height = 2376
string title = ""
boolean center = true
event ue_close ( )
dw_report_off_ven dw_report_off_ven
end type
global w_report_off_ven_euro w_report_off_ven_euro

type variables
boolean ib_modifica=false, ib_nuovo=false
boolean ib_estero
long il_anno_registrazione, il_num_registrazione,il_num_copie

end variables

forward prototypes
public subroutine wf_leggi_iva (long fl_anno_registrazione, long fl_num_registrazione, ref decimal fd_aliquote_iva[], ref decimal fd_imponibile_iva_valuta[], ref decimal fd_imposta_iva_valuta[], ref string fs_des_esenzione[], ref string fs_cod_iva[])
public function integer wf_impostazioni ()
public subroutine wf_stampa (long copie)
public function integer wf_report_ptenda ()
end prototypes

event ue_close();close(this)
end event

public subroutine wf_leggi_iva (long fl_anno_registrazione, long fl_num_registrazione, ref decimal fd_aliquote_iva[], ref decimal fd_imponibile_iva_valuta[], ref decimal fd_imposta_iva_valuta[], ref string fs_des_esenzione[], ref string fs_cod_iva[]);string ls_cod_iva
long ll_i

declare cu_iva cursor for 
	select   cod_iva, 
				imponibile_valuta_iva, 
				importo_valuta_iva,
				perc_iva
	from     iva_off_ven 
	where    cod_azienda = :s_cs_xx.cod_azienda and 
				anno_registrazione = :fl_anno_registrazione and 
				num_registrazione = :fl_num_registrazione;

open cu_iva;

ll_i = 0
do while true
	ll_i = ll_i + 1
   fetch cu_iva into :fs_cod_iva[ll_i], 
							:fd_imponibile_iva_valuta[ll_i], 
							:fd_imposta_iva_valuta[ll_i],
							:fd_aliquote_iva[ll_i];

   if sqlca.sqlcode <> 0 then exit
	if fd_aliquote_iva[ll_i] = 0 then
		select des_iva
		into   :fs_des_esenzione[ll_i]
		from   tab_ive
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_iva = :fs_cod_iva[ll_i];
	end if
loop

close cu_iva;

return

end subroutine

public function integer wf_impostazioni ();boolean 	ib_found=false
string		ls_cod_tipo_off_ven, ls_flag_tipo_off_ven, ls_cod_cliente, ls_flag_tipo_cliente, ls_path_logo_1, &
       			ls_path_logo_2, ls_modify, ls_dataobject, ls_cod_contatto, ls_logo_testata, ls_cod_lingua, ls_ret
long   		ll_copie, ll_copie_cee, ll_copie_extra_cee



save_on_close(c_socnosave)

ib_estero = false
il_num_copie = 3
setnull(ls_logo_testata)

select 	cod_cliente,
       		cod_tipo_off_ven,
			cod_contatto
into   	:ls_cod_cliente,
		 	:ls_cod_tipo_off_ven,
			:ls_cod_contatto
from   	tes_off_ven
where  	cod_azienda = :s_cs_xx.cod_azienda and
       		anno_registrazione = :il_anno_registrazione and
		 	num_registrazione = :il_num_registrazione;
if sqlca.sqlcode <> 0 then
	g_mb.error("Errore in caricamento dati ordine~r~n" + sqlca.sqlerrtext)
	return  -1
end if

if not isnull(ls_cod_cliente) then
	select 	flag_tipo_cliente,
				cod_lingua
	into   	:ls_flag_tipo_cliente,
				:ls_cod_lingua
	from   	anag_clienti
	where  	cod_azienda = :s_cs_xx.cod_azienda and
				cod_cliente = :ls_cod_cliente;
elseif not isnull(ls_cod_contatto) then
	select 	flag_tipo_cliente,
				cod_lingua
	into   	:ls_flag_tipo_cliente,
				:ls_cod_lingua
	from   	anag_contatti
	where  	cod_azienda = :s_cs_xx.cod_azienda and
				cod_contatto = :ls_cod_contatto;
else
	g_mb.error( uo_str.format( "Nell'offerta $1/$2 non è presente nè il cliente nè il contatto",il_anno_registrazione, il_num_registrazione) )
	return -1
end if	
				
// determino quante copie stampare
select num_copie,
		 num_copie_cee,
		 num_copie_extra_cee
into   :ll_copie,
		 :ll_copie_cee,
		 :ll_copie_extra_cee
from   tab_tipi_off_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_off_ven = :ls_cod_tipo_off_ven;
if sqlca.sqlcode = 0 then
	choose case ls_flag_tipo_cliente
		case "C"
			il_num_copie = ll_copie_cee
		case "E"
			il_num_copie = ll_copie_extra_cee
		case else
			il_num_copie = ll_copie
	end choose
end if
	

// carico il dataobject ed il logo corretto
if not isnull(ls_cod_cliente) and not isnull(ls_cod_lingua) then
	// cerco modulo per tipo offerta/ cliente / lingua
	select  	dataobject, 
				 logo_testata
	into	 	:ls_dataobject, 
				:ls_logo_testata
	from	 	tab_tipi_off_ven_lingue_dw
	where	 cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_off_ven = :ls_cod_tipo_off_ven and
				 cod_lingua = :ls_cod_lingua and
				 cod_cliente = :ls_cod_cliente;
	if sqlca.sqlcode = 0 then ib_found=true
end if


// se non trovato proseguo nella ricerca per tipo offerta / lingua
if not ib_found then
	
	// *** non esiste per quel cliente un modulo specifico; controllo se esiste a livello di lingua
	select dataobject,
			logo_testata
	into	 :ls_dataobject,
			:ls_logo_testata
	from	 tab_tipi_off_ven_lingue_dw
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_off_ven = :ls_cod_tipo_off_ven and
			 cod_lingua = :ls_cod_lingua and
			 ( cod_cliente IS NULL or cod_cliente = '' );
			 
	if sqlca.sqlcode = 0 and not isnull(ls_dataobject) and ls_dataobject <> "" then
		// *** esiste un particolare dataobject per quel tipo di offerta per quella lingua
		dw_report_off_ven.dataobject = ls_dataobject					
	else 
		
		// *** non esiste per quel cliente/lingua un modulo specifico; controllo se esiste a livello di tipo ordine
		select dataobject,
				logo_testata
		into	 :ls_dataobject,
				:ls_logo_testata
		from	 tab_tipi_off_ven_lingue_dw
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_off_ven = :ls_cod_tipo_off_ven and
				 cod_lingua  is null and
				 ( cod_cliente is null or cod_cliente = '' );
				 
		if sqlca.sqlcode = 0 and not isnull(ls_dataobject) and ls_dataobject <> "" then
			// *** esiste un particolare dataobject per quel tipo di fattura per quella lingua
			dw_report_off_ven.dataobject = ls_dataobject					
		else 
			// *** altrimenti lascio stare tutto com'è
		end if		
		
	end if		
	
end if


if isnull(ls_logo_testata) or len(ls_logo_testata) < 1 then
	
	select 	stringa
	into   	:ls_logo_testata
	from   	parametri_azienda
	where  	cod_azienda = :s_cs_xx.cod_azienda and
			 	flag_parametro = 'S' and
			 	cod_parametro = 'LO3';
	if sqlca.sqlcode <> 0 then
		g_mb.warning("Attenzione: il parametro LO3 relativo al logo di testata offerta non è stato impostato")
	end if
	ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_logo_testata + "'~t"

else
	ls_modify = "intestazione.filename='" + s_cs_xx.volume + s_cs_xx.risorse +  ls_logo_testata + "'~t"
end if

ls_ret = dw_report_off_ven.modify(ls_modify)


return 0
end function

public subroutine wf_stampa (long copie);long ll_i,ll_zoom

select numero
into   :ll_zoom
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'ZFV';
if sqlca.sqlcode <> 0 then ll_zoom = 100
if isnull(ll_zoom) then ll_zoom = 100

dw_report_off_ven.object.datawindow.zoom = ll_zoom


end subroutine

public function integer wf_report_ptenda ();boolean	lb_prima_riga, lb_flag_prodotto_cliente, lb_flag_nota_dettaglio, lb_flag_nota_piede, &
         lb_riga_premio=false

string 	ls_stringa, ls_stringa_euro, ls_cod_tipo_off_ven, ls_cod_banca_clien_for, ls_cod_cliente, &
		 	ls_cod_valuta, ls_cod_pagamento, ls_num_ord_cliente, ls_cod_porto, ls_cod_resa, &
		 	ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
		 	ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
		 	ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, &
		 	ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli, &
		 	ls_partita_iva, ls_cod_fiscale, ls_cod_lingua, ls_flag_tipo_cliente, &
		 	ls_des_tipo_off_ven, ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, &
  		 	ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua, ls_des_tipo_det_ven,&
		 	ls_des_prodotto_anag, ls_flag_stampa_offerta, ls_cod_tipo_det_ven, ls_flag_tipo_off_ven, &
		 	ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_cod_iva, ls_des_iva, ls_causale_trasporto, &
		 	ls_cod_vettore, ls_vettore_rag_soc_1, ls_vettore_rag_soc_2, ls_vettore_indirizzo, &
		 	ls_vettore_cap, ls_vettore_localita, ls_vettore_provincia, ls_des_esenzione_iva[], &
		 	ls_cod_banca, ls_flag_tipo_pagamento, ls_cod_abi, ls_cod_cab, &
		 	ls_cod_des_cliente, ls_telefono_cliente, ls_fax_cliente, ls_telefono_des, ls_fax_des, &
		 	ls_des_valuta, ls_des_valuta_lingua, ls_dicitura_std, ls_cod_agente_1, ls_cod_agente_2,&
		 	ls_anag_agenti_rag_soc_1, ls_anag_agenti_rag_soc_2, ls_cod_contatto, &
		 	ls_cod_des_fattura, ls_rag_soc_1_dest_fat, ls_rag_soc_2_dest_fat, ls_indirizzo_dest_fat, &
		 	ls_localita_dest_fat, ls_frazione_dest_fat, ls_cap_dest_fat, ls_provincia_dest_fat, ls_des_causale, &
		 	ls_cod_prodotto_finito, ls_cod_non_a_magazzino, ls_cod_verniciatura, ls_cod_tessuto, ls_des_gruppo_tessuto, &
		 	ls_descrizione_riga, ls_des_verniciatura, ls_codici_iva[], ls_messaggio_premio, &
		 	ls_flag_st_note_tes, ls_flag_st_note_pie, ls_flag_st_note_det, ls_cod_misura_mag, ls_formato, &
			ls_flag_tipo_det, ls_flag_tipo_fat,ls_cod_cin, ls_cod_iban, ls_conto_corrente, ls_null,&
			ls_cod_inoltro, ls_inoltro_rag_soc_1, ls_inoltro_rag_soc_2, ls_inoltro_indirizzo, &
		 	ls_inoltro_cap, ls_inoltro_localita, ls_inoltro_provincia, ls_cod_mezzo, ls_des_mezzo, ls_des_mezzo_lingua, &
			ls_des_imballo, ls_cod_imballo, ls_path_logo_1, ls_modify, ls_num_documento, ls_flag_reset_subtot, &
			ls_flag_tipo_det_ven, ls_cod_deposito, ls_des_deposito, ls_indirizzo_dep, ls_localita_dep, ls_cap_dep, &
			ls_provincia_dep, ls_telefono_dep, ls_fax_dep, ls_telex_dep, ls_tes_off_ven_firma, ls_cod_operatore_approva, &
			ls_richiedente_nome, ls_des_validita, ls_des_tempi_consegna, ls_mezzo_ricezione, ls_num_ric_cliente, &
			ls_descrizione_riga_lingua,ls_flag_tipo_mantovana, ls_colore_passamaneria, ls_cod_nazione_cliente, &
			ls_des_nazione_cliente, ls_stato_cli, ls_variabili, ls_stampa_nota_testata, ls_stampa_nota_piede, ls_stampa_nota_video, &
			ls_stampa_nota_testata_prod, ls_stampa_nota_piede_prod, ls_stampa_nota_video_prod
			 
long   	ll_errore, ll_num_colli, ll_num_aliquote, ll_prog_riga_off_ven, ll_ret, ll_num_revisione, ll_null, ll_anno_documento

dec{4} 	ld_tot_val_offerta_valuta, ld_sconto_tes, ld_quan_offerta, ld_prezzo_vendita, ld_sconto_1, &
	    		ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7,&
		 	ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_tot, ld_imponibile_riga, &
		 	ld_val_riga, ld_sconto_pagamento, ld_cambio_ven, ld_prezzo_um, ld_quantita_um, ld_val_sconto_riga, &
		   	ld_imponibili_valuta[], ld_iva_valuta[],ld_aliquote_iva[] ,ld_rate_scadenze[], ld_imponibile_iva_valuta, &
		   	ld_importo_iva_valuta, ld_perc_iva, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_tot_merci, ld_tot_cassa, &
			ld_tot_sconti_commerciali, ld_tot_val_offerta_euro, ld_precisione, ld_valore_parziale, ld_peso_lordo, ld_subtot, ld_subtot_reset, &
			ld_tot_sconto_cassa, ld_importo_iva,ld_dim_x_riga, ld_dim_y_riga
			
dec{5}   ld_fat_conversione_ven

datetime ldt_data_registrazione, ldt_data_ric_cliente

uo_calcola_documento_euro luo_doc
uo_stampa_dati_prodotto luo_stampa_dati_prodotto

setnull(ll_null)
setnull(ls_null)

dw_report_off_ven.reset()

select parametri_azienda.stringa  
into  :ls_stringa  
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'CVL';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

select parametri_azienda.stringa  
into  :ls_stringa_euro
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'EUR';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa_euro)
end if

select	 cod_tipo_off_ven,   
		 data_registrazione,   
		 cod_cliente,   
		 cod_contatto,
		 cod_valuta,   
		 cod_pagamento,   
		 sconto,   
		 cod_banca_clien_for,   
		 cod_banca,   
		 cod_porto,   
		 cod_resa,   
		 nota_testata,   
		 nota_piede,   
		 imponibile_iva_valuta,
		 importo_iva_valuta,
		 tot_val_offerta_valuta,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia,
		 cambio_ven,
		 num_colli,
		 cod_vettore,
		 cod_causale,
		 cod_agente_1,
		 cod_agente_2,
		 cod_des_cliente,
		 flag_st_note_tes,
		 flag_st_note_pie,
		 tot_val_offerta,
		 cod_inoltro,
		 peso_lordo,
		 cod_mezzo,
		 cod_imballo,
		 anno_documento,
		 num_documento,
		 num_revisione,
		 tot_sconto_cassa,
		 importo_iva,
		 cod_deposito,
		 cod_operatore_approva,
		 richiedente_nome,
		 des_validita,
		 des_tempi_consegna,
		 num_ric_cliente,
		 data_ric_cliente,
		 mezzo_ricezione
into   :ls_cod_tipo_off_ven,   
	 	 :ldt_data_registrazione,   
	 	 :ls_cod_cliente, 
		 :ls_cod_contatto,
		 :ls_cod_valuta,   
		 :ls_cod_pagamento,   
		 :ld_sconto_tes,
		 :ls_cod_banca_clien_for,   
		 :ls_cod_banca,
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_imponibile_iva_valuta,
		 :ld_importo_iva_valuta,
		 :ld_tot_val_offerta_valuta,   
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,
		 :ld_cambio_ven,
		 :ll_num_colli,
		 :ls_cod_vettore,
		 :ls_causale_trasporto,
		 :ls_cod_agente_1,
		 :ls_cod_agente_2,
		 :ls_cod_des_cliente,
		 :ls_flag_st_note_tes,
		 :ls_flag_st_note_pie,
		 :ld_tot_val_offerta_euro,
		 :ls_cod_inoltro,
		 :ld_peso_lordo,
		 :ls_cod_mezzo,
		 :ls_cod_imballo,
		 :ll_anno_documento,
		 :ls_num_documento,
		 :ll_num_revisione,
		 :ld_tot_sconto_cassa,
		 :ld_importo_iva,
		 :ls_cod_deposito,
		 :ls_cod_operatore_approva,
		 :ls_richiedente_nome,
		 :ls_des_validita,
		 :ls_des_tempi_consegna,
		 :ls_num_ric_cliente,
		 :ldt_data_ric_cliente,
		 :ls_mezzo_ricezione
from   tes_off_ven  
where  tes_off_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_off_ven.anno_registrazione = :il_anno_registrazione and 
		 tes_off_ven.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca testata fattura: stampa interrotta", Stopsign!)
	return -1
end if

// Firma approvazione
if isnull(ls_cod_operatore_approva) or ls_cod_operatore_approva = "" then
	ls_tes_off_ven_firma = "(Firma)"
else
	ls_tes_off_ven_firma = "(" + f_des_tabella("tab_operatori", "cod_operatore='" + ls_cod_operatore_approva + "'", "des_operatore") + ")"
end if
// ---

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return -1
end if


// stefanop 14/10/2011: carico deposito
if not isnull(ls_cod_deposito) and ls_cod_deposito <> "" then
	
	select 
		des_deposito,
		indirizzo,
		localita,
		cap,
		provincia,
		telefono,
		fax,
		telex
	into
		:ls_des_deposito,
		:ls_indirizzo_dep,
		:ls_localita_dep,
		:ls_cap_dep,
		:ls_provincia_dep,
		:ls_telefono_dep,
		:ls_fax_dep,
		:ls_telex_dep
	from anag_depositi
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_deposito = :ls_cod_deposito;

end if
// ----

//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_st_note_tes = 'I' then   //nota di testata
		select flag_st_note
		  into :ls_flag_st_note_tes
		  from tab_tipi_off_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_off_ven = :ls_cod_tipo_off_ven;
	end if		

	if ls_flag_st_note_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_st_note_pie = 'I' then 	//nota di piede
		select flag_st_note
		  into :ls_flag_st_note_pie
		  from tab_tipi_off_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_off_ven = :ls_cod_tipo_off_ven;
	end if			
	
	if ls_flag_st_note_pie = 'N' then
		ls_nota_piede = ""
	end if	
	
	if lb_riga_premio then
		if isnull(ls_nota_piede) or len(ls_nota_piede) < 1 then
			ls_nota_piede = ls_messaggio_premio
		else
			ls_nota_piede += "~r~n" + ls_messaggio_premio
		end if
	end if
//--------------------------------------- Fine Modifica --------------------------------------------------------		


select des_tipo_off_ven
into   :ls_des_tipo_off_ven
from   tab_tipi_off_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_off_ven = :ls_cod_tipo_off_ven;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_off_ven)
end if

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca flag_tipo_off_ven in tabella tab_tipi_off_ven: stampa interrotta", Stopsign!)
	return -1
end if
	
if not isnull(ls_cod_cliente) then
	select rag_soc_1,   
			 rag_soc_2,   
			 indirizzo,   
			 localita,   
			 frazione,   
			 cap,   
			 provincia,   
			 partita_iva,   
			 cod_fiscale,   
			 cod_lingua,   
			 telefono,   
			 telex,   
			 flag_tipo_cliente,
			 cod_nazione,
			 stato
	into   :ls_rag_soc_1_cli,   
			 :ls_rag_soc_2_cli,   
			 :ls_indirizzo_cli,   
			 :ls_localita_cli,   
			 :ls_frazione_cli,   
			 :ls_cap_cli,   
			 :ls_provincia_cli,   
			 :ls_partita_iva,   
			 :ls_cod_fiscale,   
			 :ls_cod_lingua,  
			 :ls_telefono_cliente,
			 :ls_fax_cliente,
			 :ls_flag_tipo_cliente,
			 :ls_cod_nazione_cliente,
			 :ls_stato_cli
	from   anag_clienti  
	where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
			 anag_clienti.cod_cliente = :ls_cod_cliente;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca anagrafica cliente: stampa interrotta", Stopsign!)
		return -1
	end if
	
	if not isnull(ls_stato_cli) and ls_stato_cli<>"" then
		if not isnull(ls_localita_cli) and ls_localita_cli<>"" then
			ls_localita_cli += " - " + ls_stato_cli
		else
			ls_localita_cli = ls_stato_cli
		end if
	end if
	

else
	
	select rag_soc_1,   
			 rag_soc_2,   
			 indirizzo,   
			 localita,   
			 frazione,   
			 cap,   
			 provincia,   
			 partita_iva,   
			 cod_fiscale,   
			 cod_lingua,   
			 telefono,   
			 telex,   
			 flag_tipo_cliente,
			 cod_nazione
	into   :ls_rag_soc_1_cli,   
			 :ls_rag_soc_2_cli,   
			 :ls_indirizzo_cli,   
			 :ls_localita_cli,   
			 :ls_frazione_cli,   
			 :ls_cap_cli,   
			 :ls_provincia_cli,   
			 :ls_partita_iva,   
			 :ls_cod_fiscale,   
			 :ls_cod_lingua,  
			 :ls_telefono_cliente,
			 :ls_fax_cliente,
			 :ls_flag_tipo_cliente,
			 :ls_cod_nazione_cliente
	from   anag_contatti  
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_contatto = :ls_cod_contatto;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca anagrafica cliente: stampa interrotta", Stopsign!)
		return -1
	end if
	
end if
// vettore
select rag_soc_1,
       indirizzo,   
       localita,   
       cap,   
       provincia  
into   :ls_vettore_rag_soc_1,   
       :ls_vettore_indirizzo,   
       :ls_vettore_localita,   
       :ls_vettore_cap,   
       :ls_vettore_provincia
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and
	    cod_vettore = :ls_cod_vettore;

if sqlca.sqlcode <> 0 then
   setnull(ls_vettore_rag_soc_1)   
   setnull(ls_vettore_indirizzo)   
   setnull(ls_vettore_localita)   
   setnull(ls_vettore_cap)   
   setnull(ls_vettore_provincia)
end if

// inoltro
select rag_soc_1,
       indirizzo,   
       localita,   
       cap,   
       provincia  
into   :ls_inoltro_rag_soc_1,   
       :ls_inoltro_indirizzo,   
       :ls_inoltro_localita,   
       :ls_inoltro_cap,   
       :ls_inoltro_provincia
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and
	    cod_vettore = :ls_cod_inoltro;

if sqlca.sqlcode <> 0 then
   setnull(ls_inoltro_rag_soc_1)   
   setnull(ls_inoltro_indirizzo)   
   setnull(ls_inoltro_localita)   
   setnull(ls_inoltro_cap)   
   setnull(ls_inoltro_provincia)
end if


if ls_flag_tipo_cliente = 'E' or ls_flag_tipo_cliente = 'C' then
	ls_partita_iva = ls_cod_fiscale
end if

select des_pagamento,   
       sconto,
       flag_tipo_pagamento
into   :ls_des_pagamento,   
       :ld_sconto_pagamento,
       :ls_flag_tipo_pagamento
from   tab_pagamenti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
   ls_flag_tipo_pagamento = "D"
end if

select des_pagamento
into   :ls_des_pagamento_lingua
from   tab_pagamenti_lingue  
where  cod_azienda   = :s_cs_xx.cod_azienda and 
       cod_pagamento = :ls_cod_pagamento and
       cod_lingua    = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

setnull(ls_cod_iban)
setnull(ls_conto_corrente)

if not isnull(ls_cod_banca) then
	
	select des_banca,
	       cod_abi,
			 cod_cab,
			 cin,
			 iban,
			 conto_corrente
	into   :ls_des_banca,
			 :ls_cod_abi,
			 :ls_cod_cab,
			 :ls_cod_cin,
			 :ls_cod_iban,
			 :ls_conto_corrente
	from   anag_banche
	where  anag_banche.cod_azienda = :s_cs_xx.cod_azienda and 
			 anag_banche.cod_banca = :ls_cod_banca;
			 
elseif not isnull(ls_cod_banca_clien_for) then
	
	select des_banca,
			 cod_abi,
			 cod_cab,
			 cin
	into   :ls_des_banca,
			 :ls_cod_abi,
			 :ls_cod_cab,
			 :ls_cod_cin
	from   anag_banche_clien_for  
	where  anag_banche_clien_for.cod_azienda = :s_cs_xx.cod_azienda and 
			 anag_banche_clien_for.cod_banca_clien_for = :ls_cod_banca_clien_for;	
			 
end if			 

if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca)
	setnull(ls_cod_abi)
	setnull(ls_cod_cab)
	setnull(ls_cod_cin)
	setnull(ls_cod_iban)
end if


select des_imballo
into   :ls_des_imballo
from   tab_imballi
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_imballo = :ls_cod_imballo;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballo)
end if

select des_mezzo 
into   :ls_des_mezzo  
from   tab_mezzi  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_mezzo = :ls_cod_mezzo;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo)
end if

select des_mezzo  
into   :ls_des_mezzo_lingua  
from   tab_mezzi_lingue 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_mezzo = :ls_cod_mezzo and
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo_lingua)
end if


select des_porto  
into   :ls_des_porto  
from   tab_porti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_porto = :ls_cod_porto;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_porto = :ls_cod_porto and
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select des_resa
into   :ls_des_resa  
from   tab_rese  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_resa = :ls_cod_resa;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_resa = :ls_cod_resa and  
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

select des_valuta,
		 precisione
into   :ls_des_valuta,
		 :ld_precisione
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta = :ls_cod_valuta;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta)
end if

select des_valuta
into   :ls_des_valuta_lingua  
from   tab_valute_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_valuta = :ls_cod_valuta and  
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta_lingua)
end if

if not isnull(ls_cod_agente_1) then
	select rag_soc_1
	into   :ls_anag_agenti_rag_soc_1
	from   anag_agenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_agente = :ls_cod_agente_1;
	if sqlca.sqlcode <> 0 then setnull(ls_anag_agenti_rag_soc_1)
end if

if not isnull(ls_cod_agente_2) then
	select rag_soc_1
	into   :ls_anag_agenti_rag_soc_2
	from   anag_agenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_agente = :ls_cod_agente_2;
	if sqlca.sqlcode <> 0 then setnull(ls_anag_agenti_rag_soc_2)
end if


if not isnull(ls_cod_cliente) then
	select telefono,   
			 telex  
	into   :ls_telefono_des,   
			 :ls_fax_des  
	from   anag_des_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_cliente = :ls_cod_cliente and
			 cod_des_cliente = :ls_cod_des_cliente ;
	if sqlca.sqlcode <> 0 then
		setnull(ls_telefono_des)
		setnull(ls_fax_des)
	end if
end if

// -----------------------------  modifica Enrico 01-08-2017 per stampa note fisse solo nei documenti -----------------
if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null,"STAMPA_OFF_VEN", ls_null, ldt_data_registrazione, ref ls_stampa_nota_testata, ref ls_stampa_nota_piede, ref ls_stampa_nota_video) < 0 then
	g_mb.error(ls_stampa_nota_testata)
else
	if len(ls_stampa_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_stampa_nota_video)
end if
	
/*
if not isnull(ls_stampa_nota_testata) and len(ls_stampa_nota_testata) > 0 then
	if isnull(ls_nota_testata) or len(ls_nota_testata) = 0 then
		ls_nota_testata = ls_stampa_nota_testata
	else
		ls_nota_testata = ls_stampa_nota_testata + "~r~n" + ls_nota_testata
	end if
end if

if not isnull(ls_stampa_nota_piede) and len(ls_stampa_nota_piede) > 0 then
	if isnull(ls_nota_piede) or len(ls_nota_piede) = 0 then
		ls_nota_piede = ls_stampa_nota_piede
	else
		ls_nota_piede =  ls_nota_piede + "~r~n" + ls_stampa_nota_piede
	end if
end if
*/

// --------------------------------------------------------------------------------------------------------------------

wf_leggi_iva(il_anno_registrazione, &
             il_num_registrazione, &
				 ld_aliquote_iva[], &
				 ld_imponibili_valuta[], &
				 ld_iva_valuta[], &
				 ls_des_esenzione_iva[], &
             ls_codici_iva[])

declare cu_dettagli cursor for 
	select   prog_riga_off_ven,
				cod_tipo_det_ven, 
				cod_misura, 
				quan_offerta, 
				prezzo_vendita, 
				sconto_1, 
				sconto_2, 
				sconto_3, 
				sconto_4, 
				sconto_5, 
				sconto_6, 
				sconto_7, 
				sconto_8, 
				sconto_9, 
				sconto_10, 
				nota_dettaglio, 
				cod_prodotto, 
				des_prodotto,
				fat_conversione_ven,
				cod_iva,
				flag_st_note_det,
				quantita_um,
				prezzo_um,
				imponibile_iva_valuta,
				dim_x,
				dim_y
	from     	det_off_ven 
	where    cod_azienda = :s_cs_xx.cod_azienda and 
				anno_registrazione = :il_anno_registrazione and 
				num_registrazione = :il_num_registrazione
	order by cod_azienda, 
				anno_registrazione, 
				num_registrazione,
				prog_riga_off_ven;
				
if g_str.isnotempty(ls_cod_nazione_cliente) then
	select des_nazione
	into :ls_des_nazione_cliente
	from tab_nazioni
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_nazione = :ls_cod_nazione_cliente;
else
	setnull(ls_des_nazione_cliente)
end if			

open cu_dettagli;

lb_flag_nota_piede = false
lb_prima_riga = false

if (not isnull(ls_nota_testata) and len(trim(ls_nota_testata)) > 0) or (not isnull(ls_stampa_nota_testata) and len(trim(ls_stampa_nota_testata)) > 0) then lb_prima_riga = true

lb_flag_prodotto_cliente = false
lb_flag_nota_dettaglio = false
luo_stampa_dati_prodotto = create uo_stampa_dati_prodotto

do while 0 = 0
	if not(lb_prima_riga) and not(lb_flag_prodotto_cliente) and not(lb_flag_nota_dettaglio) then
		fetch cu_dettagli into :ll_prog_riga_off_ven,
									  :ls_cod_tipo_det_ven, 
									  :ls_cod_misura, 
									  :ld_quan_offerta, 
									  :ld_prezzo_vendita,   
									  :ld_sconto_1, 
									  :ld_sconto_2, 
									  :ld_sconto_3, 
									  :ld_sconto_4, 
									  :ld_sconto_5, 
									  :ld_sconto_6, 
									  :ld_sconto_7, 
									  :ld_sconto_8, 
									  :ld_sconto_9, 
									  :ld_sconto_10, 
									  :ls_nota_dettaglio, 
									  :ls_cod_prodotto, 
									  :ls_des_prodotto,
									  :ld_fat_conversione_ven,
									  :ls_cod_iva,
								  	  :ls_flag_st_note_det,
									  :ld_quantita_um,
									  :ld_prezzo_um,
									  :ld_imponibile_riga,
									  :ld_dim_x_riga,
									  :ld_dim_y_riga;
									  
		if sqlca.sqlcode <> 0 then
			if (not isnull(ls_nota_piede) and len(trim(ls_nota_piede)) > 0) or (not isnull(ls_stampa_nota_piede) and len(trim(ls_stampa_nota_piede)) > 0) then
				// sono alla fine e c'è una nota, quindi attivo la stampa nota e faccio ancora un giro
				lb_flag_nota_piede = true
			else
				exit
			end if
		end if
		
		//////////////////////////////// Calcolo Valore Riga PT /////////////////////////////////////////
		
		luo_doc = create uo_calcola_documento_euro
		
		ld_imponibile_riga = ld_quan_offerta * ld_prezzo_vendita
		luo_doc.uof_arrotonda(ld_imponibile_riga,ld_precisione,"euro")
		
		ld_valore_parziale = ld_imponibile_riga
		
		ld_sconto_tot = 0
		
		if ld_sconto_1 <> 0 and not isnull(ld_sconto_1) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_1)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_1)
		end if
		
		if ld_sconto_2 <> 0 and not isnull(ld_sconto_2) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_2)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_2)
		end if
		
		if ld_sconto_3 <> 0 and not isnull(ld_sconto_3) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_3)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_3)
		end if
		
		if ld_sconto_4 <> 0 and not isnull(ld_sconto_4) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_4)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_4)
		end if
		
		if ld_sconto_5 <> 0 and not isnull(ld_sconto_5) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_5)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_5)
		end if
		
		if ld_sconto_6 <> 0 and not isnull(ld_sconto_6) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_6)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_6)
		end if
		
		if ld_sconto_7 <> 0 and not isnull(ld_sconto_7) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_7)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_7)
		end if
		
		if ld_sconto_8 <> 0 and not isnull(ld_sconto_8) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_8)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_8)
		end if
		
		if ld_sconto_9 <> 0 and not isnull(ld_sconto_9) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_9)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_9)
		end if
		
		if ld_sconto_10 <> 0 and not isnull(ld_sconto_10) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_10)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_10)
		end if
		
		if ld_sconto_tes <> 0 and not isnull(ld_sconto_tes) then
			ld_sconto_tot = ld_sconto_tot + (ld_valore_parziale / 100 * ld_sconto_tes)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_tes)
		end if
		
		luo_doc.uof_arrotonda(ld_sconto_tot,ld_precisione,"euro")
		
		ld_imponibile_riga = ld_imponibile_riga - ld_sconto_tot
		
		destroy luo_doc
		
		/////////////////////////////////////////////////////////////////////////////////////////////////
		
//------------------------------------------------- Modifica Nicola ---------------------------------------------
		if ls_flag_st_note_det = 'I' then //nota dettaglio
			select flag_st_note_ft
			  into :ls_flag_st_note_det
			  from tab_tipi_det_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		end if		
		
		if ls_flag_st_note_det = 'N' then
			ls_nota_dettaglio = ""
		end if				
//-------------------------------------------------- Fine Modifica ----------------------------------------------				
		
	end if
   	
	dw_report_off_ven.insertrow(0)
	dw_report_off_ven.setrow(dw_report_off_ven.rowcount())
	
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "formato", ls_formato)

	select tab_tipi_det_ven.flag_stampa_offerta,
	       tab_tipi_det_ven.des_tipo_det_ven
	into   :ls_flag_stampa_offerta,
	       :ls_des_tipo_det_ven
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if sqlca.sqlcode <> 0 then
		setnull(ls_flag_stampa_offerta)
		setnull(ls_des_tipo_det_ven)
	end if

	if lb_prima_riga then
		if len(ls_stampa_nota_testata) > 0 then dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_des_prodotto", ls_stampa_nota_testata)
	elseif lb_flag_prodotto_cliente then
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_cod_prodotto", "Vs Codice")
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_des_prodotto", ls_cod_prod_cliente)
		lb_flag_prodotto_cliente = false
	elseif lb_flag_nota_dettaglio then
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_des_prodotto", ls_nota_dettaglio)
		lb_flag_nota_dettaglio = false		
	elseif lb_flag_nota_piede then
		if len(ls_nota_piede) > 0 then dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_nota_piede", ls_nota_piede)
		
		if len(ls_nota_piede) > 0 and len(ls_stampa_nota_piede) > 0 then
			dw_report_off_ven.insertrow(0)
			dw_report_off_ven.setrow(dw_report_off_ven.rowcount())
		end if

		if  len(ls_stampa_nota_piede) > 0 then dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_des_prodotto", ls_stampa_nota_piede)
		
	else
		
		if ls_flag_stampa_offerta = 'S' then
			select des_prodotto, cod_misura_mag
			into   :ls_des_prodotto_anag, :ls_cod_misura_mag
			from   anag_prodotti  
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_anag)
				setnull(ls_cod_misura_mag)
			end if
			

			select anag_prodotti_lingue.des_prodotto  
			into   :ls_des_prodotto_lingua  
			from   anag_prodotti_lingue  
			where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
					 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_lingua)
			end if
	
			setnull(ls_cod_prod_cliente)
			
			if not isnull(ls_cod_cliente) then
				select tab_prod_clienti.cod_prod_cliente  
				into   :ls_cod_prod_cliente  
				from   tab_prod_clienti  
				where  tab_prod_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
						 tab_prod_clienti.cod_prodotto = :ls_cod_prodotto and   
						 tab_prod_clienti.cod_cliente = :ls_cod_cliente;
		
				if sqlca.sqlcode <> 0 then
					setnull(ls_cod_prod_cliente)
				end if
				
			end if
			
			select tab_ive.aliquota
			into   :ld_perc_iva
			from   tab_ive
			where  tab_ive.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_ive.cod_iva = :ls_cod_iva;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_iva)
			else
				ls_des_iva = string(ld_perc_iva,"###")
			end if
			
			select flag_tipo_det_ven, flag_reset_subtot, flag_tipo_det_ven
			into   :ls_flag_tipo_det, :ls_flag_reset_subtot, :ls_flag_tipo_det_ven
			from   tab_tipi_det_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
					 
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore in controllo tipo dettaglio.~nErrore nella select di tab_tipi_det_ven: " + sqlca.sqlerrtext)
				setnull(ls_flag_tipo_det)
			elseif sqlca.sqlcode = 100 then
				g_mb.messagebox("APICE","Errore in controllo tipo dettaglio.~nErrore nella select di tab_tipi_det_ven: tipo dettaglio non trovato")
				setnull(ls_flag_tipo_det)
			end if
			
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_cod_misura", ls_cod_misura)
			
			if not isnull(ls_cod_misura_mag) and ls_cod_misura_mag <> ls_cod_misura then
				dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_quan_ordine", ld_quantita_um)
				if ls_flag_tipo_det = 'S' then
					dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_prezzo_vendita", ld_prezzo_um * -1)
				else			
					dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_prezzo_vendita", ld_prezzo_um)
				end if				
			else
				dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_quan_ordine", ld_quan_offerta)
				if ls_flag_tipo_det = 'S' then
					dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_prezzo_vendita", ld_prezzo_vendita * -1)
				else			
					dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_prezzo_vendita", ld_prezzo_vendita)
				end if				
			end if
			
			// è un valore negativo?
			if ls_flag_tipo_det = 'S' then ld_imponibile_riga *= -1
			// ---
			
			ld_subtot += ld_imponibile_riga
		
				//subtotali
				if ls_flag_tipo_det_ven = "X" then	
					ld_imponibile_riga = ld_subtot
				end if
				
				if ls_flag_tipo_det_ven = "X" and ls_flag_reset_subtot = "S" then
					ld_subtot = 0
				end if
				// ----
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_flag_tipo_dett_ven", ls_flag_tipo_det_ven)
	
			
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_val_riga", ld_imponibile_riga)
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_sconto_1", ld_sconto_1)
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_sconto_2", ld_sconto_2)
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_sconto_tot", ld_sconto_tot)			
			//dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_cod_iva", ls_des_iva)
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_cod_iva", ls_cod_iva)
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_nota_dettaglio", ls_nota_dettaglio)
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_cod_prodotto", ls_cod_prodotto)
			
			// Normale descrizione di riga
			if not isnull(ls_des_prodotto) then
				ls_descrizione_riga = ls_des_prodotto
			elseif not isnull(ls_des_prodotto_anag) then
				ls_descrizione_riga = ls_des_prodotto_anag
			else		
				ls_descrizione_riga = ls_des_tipo_det_ven
			end if
				
			// inizio la predisposizione della descrizione in lingua se il cliente ha un codice lingua associato
			ls_descrizione_riga_lingua = ""
			if not isnull(ls_cod_lingua)  then
				if not isnull(ls_des_prodotto_lingua) then
					ls_descrizione_riga_lingua = ls_des_prodotto_lingua
				end if
			end if
				
			// EnMe 01-08-2017 nuova gestione nota dettaglio prodotto
			if not isnull(ls_cod_prodotto) then
				if guo_functions.uof_get_note_fisse(ls_null, ls_null, ls_cod_prodotto, "STAMPA_OFF_VEN", ls_null, ldt_data_registrazione, ref ls_stampa_nota_testata_prod, ref ls_stampa_nota_piede_prod, ref ls_stampa_nota_video_prod) < 0 then
					g_mb.error(ls_stampa_nota_testata_prod)
				else
					if len(ls_stampa_nota_testata_prod) > 0 then ls_nota_dettaglio = g_str.implode({ls_nota_dettaglio,ls_stampa_nota_testata_prod},"~r~n")
					if len(ls_stampa_nota_piede_prod) > 0 then ls_nota_dettaglio = g_str.implode({ls_nota_dettaglio,ls_stampa_nota_piede_prod},"~r~n")
					if len(ls_stampa_nota_video_prod) > 0 then g_mb.warning( "NOTA FISSA", ls_stampa_nota_video_prod)
				end if
			end if	
			
			if not isnull(ls_cod_prod_cliente) and len(trim(ls_cod_prod_cliente)) > 0 then
				lb_flag_prodotto_cliente = true
			end if
			if not isnull(ls_nota_dettaglio) and len(trim(ls_nota_dettaglio)) > 0 then
				lb_flag_nota_dettaglio = true
			end if
			
			// select dettagli complementari
			if il_anno_registrazione > 0 and not isnull(il_anno_registrazione) then
				
				ls_variabili = ""
				luo_stampa_dati_prodotto.uof_stampa_variabili( ls_cod_prodotto, "OFF_VEN", "OFF_VEN", ls_cod_tipo_off_ven, il_anno_registrazione, il_num_registrazione, ll_prog_riga_off_ven, ls_variabili)
				if len(ls_variabili) > 0 then
					ls_descrizione_riga= ls_des_prodotto + "~r~n" + ls_variabili
				else
					ls_descrizione_riga= ls_des_prodotto
				end if

				select cod_prod_finito, dim_x, dim_y, dim_z, dim_t, cod_non_a_magazzino, cod_verniciatura, cod_tessuto, flag_tipo_mantovana, colore_passamaneria
				into   :ls_cod_prodotto_finito, :ld_dim_x, :ld_dim_y, :ld_dim_z, :ld_dim_t, :ls_cod_non_a_magazzino, :ls_cod_verniciatura, :ls_cod_tessuto, :ls_flag_tipo_mantovana, :ls_colore_passamaneria
				from   comp_det_off_ven  
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :il_anno_registrazione and
						 num_registrazione = :il_num_registrazione and  
						 prog_riga_off_ven = :ll_prog_riga_off_ven ;
				if sqlca.sqlcode = 0 then	
					/*
					// stampo dettagli complementari
					if not isnull(ls_cod_tessuto) and ls_cod_tessuto <> "" then
						select des_prodotto
						into   :ls_des_gruppo_tessuto
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_tessuto;
						if sqlca.sqlcode = 0 then
							ls_descrizione_riga = ls_descrizione_riga + ", " + ls_des_gruppo_tessuto
						end if		
						// descrizione in lingua del gruppo tessuto
						if not isnull(ls_cod_lingua)  then
							select des_prodotto
							into   :ls_des_gruppo_tessuto
							from   anag_prodotti_lingue
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_prodotto = :ls_cod_tessuto and
									 cod_lingua = :ls_cod_lingua;
							if sqlca.sqlcode = 0 then
								ls_descrizione_riga_lingua +=" " + ls_des_gruppo_tessuto
							end if		
						end if
					end if
						
					// COLORE  TESSUTO
					if not isnull(ls_cod_non_a_magazzino) and ls_cod_non_a_magazzino <> "" then
						ls_descrizione_riga = ls_descrizione_riga + " COL='" + ls_cod_non_a_magazzino + "' "
						//ls_descrizione_riga_lingua = ls_descrizione_riga_lingua + " COL='" + ls_cod_non_a_magazzino + "' "
					end if
					
					//TIPO MANTOVANA
					if not isnull(ls_flag_tipo_mantovana) and len(ls_flag_tipo_mantovana) > 0 then
						ls_descrizione_riga = ls_descrizione_riga + " MANT='" + ls_flag_tipo_mantovana + "' "
						//ls_descrizione_riga_lingua = ls_descrizione_riga_lingua + " MANT='" + ls_flag_tipo_mantovana + "' "
					end if			
					
					//COLORE PASSAMANERIA
					if not isnull(ls_colore_passamaneria) and len(ls_colore_passamaneria) > 0 then
						ls_descrizione_riga = ls_descrizione_riga + " PASS='" + ls_colore_passamaneria + "' "
						//ls_descrizione_riga_lingua = ls_descrizione_riga_lingua + " PASS='" + ls_colore_passamaneria + "' "
					end if			
						
					if not isnull(ls_cod_verniciatura) and ls_cod_verniciatura <> "" then
						select des_prodotto
						into   :ls_des_verniciatura
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_verniciatura;
						if sqlca.sqlcode = 0 then
							ls_descrizione_riga = ls_descrizione_riga + ", " + ls_des_verniciatura
						end if
						// descrizione in lingua della verniciatura
						if not isnull(ls_cod_lingua)  then
							select des_prodotto
							into   :ls_des_verniciatura
							from   anag_prodotti_lingue
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_prodotto = :ls_cod_verniciatura and
									 cod_lingua = :ls_cod_lingua;
							if sqlca.sqlcode = 0 then
								ls_descrizione_riga_lingua +=" " + ls_des_verniciatura
							end if		
						end if
					end if
					
					// DIMENSIONI
					if ld_dim_x > 0 then
						dw_report_off_ven.setitem(dw_report_off_ven.getrow(),"comp_det_dim_x", ld_dim_x)
					end if
					if ld_dim_y > 0 then
						dw_report_off_ven.setitem(dw_report_off_ven.getrow(),"comp_det_dim_y", ld_dim_y)
					end if
					if ld_dim_z > 0 then
						dw_report_off_ven.setitem(dw_report_off_ven.getrow(),"comp_det_dim_z", ld_dim_z)
					end if
					if ld_dim_t > 0 then
						dw_report_off_ven.setitem(dw_report_off_ven.getrow(),"comp_det_dim_t", ld_dim_t)
					end if
					if not isnull(ls_cod_prodotto_finito) and len(ls_cod_prodotto_finito) > 0 then
						dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_cod_prodotto", ls_cod_prodotto_finito)
					end if
				else
					// non è un prodotto configurato
					if ld_dim_x_riga > 0 then
						dw_report_off_ven.setitem(dw_report_off_ven.getrow(),"comp_det_dim_x", ld_dim_x_riga)
					end if
					if ld_dim_y_riga > 0 then
						dw_report_off_ven.setitem(dw_report_off_ven.getrow(),"comp_det_dim_y", ld_dim_y_riga)
					end if
					*/
				end if
				
				//dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_des_prodotto", ls_descrizione_riga)
			end if
			// stefanop: 06/08/2014: rimossa duplicazione COL='
			if not isnull(ls_cod_lingua) and not isnull(ls_descrizione_riga_lingua) and ls_cod_lingua <> "ITA" then ls_descrizione_riga += "~r~n" + ls_descrizione_riga_lingua
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "det_off_ven_des_prodotto", ls_descrizione_riga)

		end if
	end if	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "parametri_azienda_stringa", ls_stringa)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "parametri_azienda_stringa_euro", ls_stringa_euro)
	
	if len(ls_num_documento) < 1 then setnull( ls_num_documento )
	
	if isnull(ls_num_documento) then
		
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_anno_documento", il_anno_registrazione)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_num_documento", il_num_registrazione)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_data_fattura", ldt_data_registrazione)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_documento", ls_num_documento )
		
	else
		
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_anno_documento", ll_null)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_num_documento",  ll_null)
		
		if ll_num_revisione > 0 then
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_documento", string(ll_anno_documento,"0000") + " / " + ls_num_documento + "  REV.: " + string(ll_num_revisione))
		else
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_documento", string(ll_anno_documento,"0000") + " / " + ls_num_documento)
		end if
		
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_data_fattura", ldt_data_registrazione)
		
	end if
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_tipo_off_ven", ls_cod_tipo_off_ven)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_data_registrazione", ldt_data_registrazione)
	if not isnull(ls_cod_cliente) then
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_cliente", ls_cod_cliente)
	else
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_cliente", ls_cod_contatto)
	end if
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_valuta", ls_cod_valuta)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_pagamento", ls_cod_pagamento)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_sconto", ld_sconto_pagamento)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_banca_clien_for", ls_cod_banca_clien_for)
//---------------------------------- Modifica Nicola ---------------------------------------

	if (isnull(ls_rag_soc_1_dest_fat) or len(ls_rag_soc_1_dest_fat) < 1) and ls_flag_tipo_off_ven = "D" then
		ls_rag_soc_1 = ls_rag_soc_1_cli
		ls_rag_soc_2 = ls_rag_soc_2_cli
		ls_indirizzo = ls_indirizzo_cli
		ls_cap = ls_cap_cli
		ls_localita = ls_localita_cli
		ls_provincia = ls_provincia_cli	
	end if		
	
	if (not isnull(ls_rag_soc_1_dest_fat) or len(ls_rag_soc_1_dest_fat) > 0) and ls_flag_tipo_off_ven <> "I" then
		ls_rag_soc_1 = ls_rag_soc_1_dest_fat
		ls_rag_soc_2 = ls_rag_soc_2_dest_fat
		ls_indirizzo = ls_indirizzo_dest_fat
		ls_cap = ls_cap_dest_fat
		ls_localita = ls_localita_dest_fat
		ls_provincia = ls_provincia_dest_fat
	end if 	
	
	if (isnull(ls_rag_soc_1) or len(ls_rag_soc_1) < 1) and &
		(isnull(ls_rag_soc_1_dest_fat) or len(ls_rag_soc_1_dest_fat) < 1) then
		ls_rag_soc_1 = ls_rag_soc_1_cli
		ls_rag_soc_2 = ls_rag_soc_2_cli
		ls_indirizzo = ls_indirizzo_cli
		ls_cap = ls_cap_cli
		ls_localita = ls_localita_cli
		ls_provincia = ls_provincia_cli	
	end if	
	
//----------------------------------- Fine Modifica ----------------------------------------	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_rag_soc_1", ls_rag_soc_1)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_rag_soc_2", ls_rag_soc_2)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_indirizzo", ls_indirizzo)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_localita", ls_localita)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_frazione", ls_frazione)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cap", ls_cap)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_provincia", ls_provincia)
	
	//  EnMe: nota testat e piede devono restare così perchè c'è la gestione con RTF
//	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_nota_testata", ls_nota_testata)
//	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_nota_piede", ls_nota_piede)
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_porto", ls_cod_porto)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_resa", ls_cod_resa)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_agente_1", ls_cod_agente_1)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_agenti_rag_soc_1", ls_anag_agenti_rag_soc_1)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_agente_2", ls_cod_agente_2)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_agenti_rag_soc_2", ls_anag_agenti_rag_soc_2)
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_tot_fattura_euro", ld_tot_val_offerta_euro)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_tot_fattura_valuta", ld_tot_val_offerta_valuta)
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_imponibile_iva_valuta", 0)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_iva_valuta", 0)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cod_vettore", ls_cod_vettore)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_num_colli", ll_num_colli)

//---------------------------------------- Modifica Nicola -----------------------------------------------------
	select des_causale
	  into :ls_des_causale
	  from tab_causali_trasp
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and cod_causale = :ls_causale_trasporto;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in ricerca causale trasporto: stampa interrotta", Stopsign!)
		return -1
	elseif sqlca.sqlcode = 100 then
		ls_des_causale = ""
	end if	
//------------------------------------------ Fine Modifica ----------------------------------------------------- 	
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_firma", ls_tes_off_ven_firma)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_causale_trasporto", ls_des_causale)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_telefono", ls_telefono_cliente)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_fax", ls_fax_cliente)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_cambio_ven", ld_cambio_ven)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_indirizzo", ls_indirizzo_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_localita", ls_localita_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_frazione", ls_frazione_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_cap", ls_cap_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_provincia", ls_provincia_cli)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_partita_iva", ls_partita_iva)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_telefono", ls_telefono_des)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_fax", ls_fax_des)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_cod_fiscale", ls_cod_fiscale)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_tipi_off_ven_des_tipo_off_ven", ls_des_tipo_off_ven)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_pagamenti_sconto", ld_sconto_pagamento)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
	if ls_flag_tipo_pagamento = "R" then
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_banche_clien_for_des_banca", ls_des_banca)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_banche_clien_for_cod_abi", ls_cod_abi)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_banche_clien_for_cod_cab", ls_cod_cab)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_banche_clien_for_cin", ls_cod_cin)
	else
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_banche_des_banca", ls_des_banca)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_banche_cod_abi", ls_cod_abi)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_banche_cod_cab", ls_cod_cab)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_banche_cin", ls_cod_cin)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_banche_iban", ls_cod_iban)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_banche_conto_corrente", ls_conto_corrente)
	end if
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_valute_des_valuta", ls_des_valuta)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_valute_lingue_des_valuta", ls_des_valuta_lingua)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_porti_des_porto", ls_des_porto)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_rese_des_resa", ls_des_resa)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_vettori_rag_soc_1", ls_vettore_rag_soc_1)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_vettori_indirizzo", ls_vettore_indirizzo)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_vettori_cap", ls_vettore_cap)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_vettori_localita", ls_vettore_localita)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_vettori_provincia", ls_vettore_provincia)

	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_inoltro_cod_inoltro", ls_cod_inoltro)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_inoltro_rag_soc_1", ls_inoltro_rag_soc_1)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_inoltro_indirizzo", ls_inoltro_indirizzo)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_inoltro_cap", ls_inoltro_cap)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_inoltro_localita", ls_inoltro_localita)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_inoltro_provincia", ls_inoltro_provincia)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_peso_lordo", ld_peso_lordo)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_mezzi_des_mezzo", ls_des_mezzo)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_imballi_des_imballo", ls_des_imballo)

	ll_num_aliquote = upperbound(ld_aliquote_iva) - 1
	if ll_num_aliquote > 0 then
		if ld_aliquote_iva[1] > 0 and not isnull(ld_aliquote_iva[1]) then
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_imponibile_iva_1", ld_imponibili_valuta[1])
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_importo_iva_1", ld_iva_valuta[1])
		else
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_imponibile_iva_1", ld_imponibili_valuta[1])
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_des_esenzione_1", ls_des_esenzione_iva[1])
		end if
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_cod_iva_1", ls_codici_iva[1])
	end if
	if ll_num_aliquote > 1 then
		if ld_aliquote_iva[2] > 0 and not isnull(ld_aliquote_iva[2]) then
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_imponibile_iva_2", ld_imponibili_valuta[2])
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_importo_iva_2", ld_iva_valuta[2])
		else
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_imponibile_iva_2", ld_imponibili_valuta[2])
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_des_esenzione_2", ls_des_esenzione_iva[2])
		end if
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_cod_iva_2", ls_codici_iva[2])
	end if
	if ll_num_aliquote > 2 then
		if ld_aliquote_iva[3] > 0 and not isnull(ld_aliquote_iva[3]) then
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_imponibile_iva_3", ld_imponibili_valuta[3])
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_importo_iva_3", ld_iva_valuta[3])
		else
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_imponibile_iva_3", ld_imponibili_valuta[3])
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_des_esenzione_3", ls_des_esenzione_iva[3])
		end if
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_cod_iva_3", ls_codici_iva[3])					
	end if
	if ll_num_aliquote > 3 then
		if ld_aliquote_iva[4] > 0 and not isnull(ld_aliquote_iva[4]) then
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_imponibile_iva_4", ld_imponibili_valuta[4])
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_importo_iva_4", ld_iva_valuta[4])
		else
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_imponibile_iva_4", ld_imponibili_valuta[4])
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_des_esenzione_4", ls_des_esenzione_iva[4])
		end if
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_cod_iva_4", ls_codici_iva[4])
	end if
	if ll_num_aliquote > 4 then
		if ld_aliquote_iva[5] > 0 and not isnull(ld_aliquote_iva[5]) then
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_imponibile_iva_5", ld_imponibili_valuta[5])
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_importo_iva_5", ld_iva_valuta[5])
		else
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_imponibile_iva_5", ld_imponibili_valuta[5])
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_des_esenzione_5", ls_des_esenzione_iva[5])
		end if
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_cod_iva_5", ls_codici_iva[5])
	end if
	if ll_num_aliquote > 5 then
		if ld_aliquote_iva[6] > 0 and not isnull(ld_aliquote_iva[6]) then
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_imponibile_iva_6", ld_imponibili_valuta[6])
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_importo_iva_6", ld_iva_valuta[6])
		else
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_imponibile_iva_6", ld_imponibili_valuta[6])
			dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_des_esenzione_6", ls_des_esenzione_iva[6])
		end if
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "iva_off_ven_cod_iva_6", ls_codici_iva[6])
	end if

	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_imponibile_iva_valuta", ld_imponibile_iva_valuta)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_iva_valuta", ld_importo_iva_valuta)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_tot_sconto_cassa", ld_tot_sconto_cassa)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_importo_iva", ld_importo_iva)
	
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_richiedente_nome", ls_richiedente_nome)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_des_validita", ls_des_validita)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_tempi_consegna", ls_des_tempi_consegna)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_num_richiesta", ls_num_ric_cliente)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_data_richiesta", ldt_data_ric_cliente)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_mezzo_ricezione", ls_mezzo_ricezione)

	// deposito
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_deposito_cod_deposito", ls_cod_deposito)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_deposito_des_deposito", ls_des_deposito)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_deposito_indrizzo", ls_indirizzo_dep)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_deposito_localita", ls_localita_dep)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_deposito_cap", ls_cap_dep)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_deposito_provincia", ls_provincia_dep)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_deposito_telefono", ls_telefono_dep)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_deposito_fax", ls_fax_dep)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_deposito_email", ls_telex_dep)
	dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_deposito_pec", "")
	// ---
	
	// nuove colonne che danno problemi con la libreria personalizzata
	try
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "anag_clienti_cod_nazione", ls_cod_nazione_cliente)
		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tab_nazioni_des_nazione", ls_des_nazione_cliente)
	catch(RuntimeError e)
	end try
	
	if lb_prima_riga and len(ls_nota_testata) > 0 then
		if len(ls_stampa_nota_testata) > 0 then
			dw_report_off_ven.insertrow(0)
			dw_report_off_ven.setrow(dw_report_off_ven.rowcount())
		end if
 		dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_nota_testata", ls_nota_testata)
	end if
	
	
	lb_prima_riga = false
	if lb_flag_nota_piede then exit
loop
close cu_dettagli;
destroy luo_stampa_dati_prodotto

ld_tot_merci = ld_tot_merci - ld_tot_sconti_commerciali
dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_tot_merci", ld_tot_merci)
dw_report_off_ven.setitem(dw_report_off_ven.getrow(), "tes_off_ven_importo_sconto", ld_tot_cassa)
/*
select parametri_azienda.stringa
into   :ls_path_logo_1
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO3';

ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
dw_report_off_ven.modify(ls_modify)
*/
dw_report_off_ven.reset_dw_modified(c_resetchildren)
dw_report_off_ven.change_dw_current()
return 0
end function

event pc_setwindow;call super::pc_setwindow;string  ls_cod_documento,ls_cod_tipo_fat_ven,ls_dataobject, ls_numeratore_documento
long    ll_i, ll_anno, ll_num, ll_num_documento, ll_anno_documento
datetime ldt_data_offerta


set_w_options(c_noresizewin)
il_anno_registrazione = s_cs_xx.parametri.parametro_d_1
il_num_registrazione  = s_cs_xx.parametri.parametro_d_2

if wf_impostazioni() < 0 then return

select cod_documento,
		numeratore_documento,
		anno_documento,
		num_documento,
		data_offerta
into	:ls_cod_documento,
		:ls_numeratore_documento,
		:ll_anno_documento,
		:ll_num_documento,
		:ldt_data_offerta
from	tes_off_ven
where		cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :il_anno_registrazione and
			num_registrazione = :il_num_registrazione;
if sqlca.sqlcode = 0 and not isnull(ls_cod_documento) then
	title =  g_str.format("Offerta numero $1-$2 / $3-$4 del $5", ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento, string(ldt_data_offerta,"dd/mm/yyyy"))
	dw_report_off_ven.set_document_name(title)
else
	title = "Offerta di Vendita " + string(il_anno_registrazione) + "/" + string(il_num_registrazione)
	dw_report_off_ven.set_document_name("Offerta di Vendita " + string(il_anno_registrazione) + "/" + string(il_num_registrazione))
end if

dw_report_off_ven.ib_dw_report = true
dw_report_off_ven.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
											c_disablecc, &
											c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)
		
		



// ************ metto la DW in anteprima **************************
dw_report_off_ven.object.datawindow.print.preview = 'Yes'
dw_report_off_ven.object.datawindow.print.preview.rulers = 'Yes'

dw_report_off_ven.move(20,20)

end event

on w_report_off_ven_euro.create
int iCurrent
call super::create
this.dw_report_off_ven=create dw_report_off_ven
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_off_ven
end on

on w_report_off_ven_euro.destroy
call super::destroy
destroy(this.dw_report_off_ven)
end on

event resize;dw_report_off_ven.resize(newwidth - 40, newheight - 40)
end event

type dw_report_off_ven from uo_cs_xx_dw within w_report_off_ven_euro
integer x = 27
integer y = 20
integer width = 4965
integer height = 2236
integer taborder = 20
string dataobject = "d_report_off_ven_euro_ptenda"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report_ptenda()

end event

