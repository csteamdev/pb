﻿$PBExportHeader$w_tes_off_ven_ptenda.srw
forward
global type w_tes_off_ven_ptenda from w_tes_off_ven
end type
end forward

global type w_tes_off_ven_ptenda from w_tes_off_ven
event ue_set_current_window ( )
end type
global w_tes_off_ven_ptenda w_tes_off_ven_ptenda

type variables
boolean 					ib_nuova_data_consegna = false
end variables

forward prototypes
public function boolean wf_duplica_altre_tabelle (long al_anno_registrazione_orig, long al_num_registrazione_orig, long al_anno_registrazione_dupl, long al_num_registrazione_dupl, ref string as_errore)
end prototypes

event ue_set_current_window();pcca.window_current = this
end event

public function boolean wf_duplica_altre_tabelle (long al_anno_registrazione_orig, long al_num_registrazione_orig, long al_anno_registrazione_dupl, long al_num_registrazione_dupl, ref string as_errore);// COMP_DET_OFF_VEN
INSERT INTO comp_det_off_ven  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  prog_riga_off_ven,   
		  cod_modello,   
		  cod_tessuto,   
		  cod_tessuto_mant,   
		  cod_mant_non_a_magazzino,   
		  cod_non_a_magazzino,   
		  cod_prod_finito,   
		  dim_x,   
		  dim_y,   
		  dim_z,   
		  dim_t,   
		  flag_tipo_mantovana,   
		  alt_mantovana,   
		  flag_cordolo,   
		  flag_passamaneria,   
		  cod_verniciatura,   
		  cod_comando,   
		  pos_comando,   
		  alt_asta,   
		  cod_comando_tenda_verticale,   
		  cod_colore_lastra,   
		  cod_tipo_lastra,   
		  spes_lastra,   
		  cod_tipo_supporto,   
		  flag_cappotta_frontale,   
		  flag_rollo,   
		  flag_kit_laterale,   
		  flag_misura_luce_finita,   
		  cod_tipo_stampo,   
		  cod_mat_sis_stampaggio,   
		  rev_stampo,   
		  num_gambe,   
		  num_campate,   
		  note,   
		  note_esterne,   
		  flag_autoblocco,   
		  num_fili_plisse,   
		  num_boccole_plisse,   
		  intervallo_punzoni_plisse,   
		  num_pieghe_plisse,   
		  prof_inf_plisse,   
		  prof_sup_plisse,   
		  colore_passamaneria,   
		  colore_cordolo,   
		  num_punzoni_plisse,   
		  des_vernice_fc,   
		  des_comando_1,   
		  cod_comando_2,   
		  des_comando_2,   
		  flag_addizionale_comando_1,   
		  flag_addizionale_comando_2,   
		  flag_addizionale_supporto,   
		  flag_addizionale_tessuto,   
		  flag_addizionale_vernic,   
		  flag_fc_comando_1,   
		  flag_fc_comando_2,   
		  flag_fc_supporto,   
		  flag_fc_tessuto,   
		  flag_fc_vernic,   
		  flag_allegati,   
		  data_ora_ar,   
		  flag_tessuto_cliente,   
		  flag_tessuto_mant_cliente ) 
SELECT
			cod_azienda,   
		  :al_anno_registrazione_dupl,   
		  :al_num_registrazione_dupl,   
		  prog_riga_off_ven,   
		  cod_modello,   
		  cod_tessuto,   
		  cod_tessuto_mant,   
		  cod_mant_non_a_magazzino,   
		  cod_non_a_magazzino,   
		  cod_prod_finito,   
		  dim_x,   
		  dim_y,   
		  dim_z,   
		  dim_t,   
		  flag_tipo_mantovana,   
		  alt_mantovana,   
		  flag_cordolo,   
		  flag_passamaneria,   
		  cod_verniciatura,   
		  cod_comando,   
		  pos_comando,   
		  alt_asta,   
		  cod_comando_tenda_verticale,   
		  cod_colore_lastra,   
		  cod_tipo_lastra,   
		  spes_lastra,   
		  cod_tipo_supporto,   
		  flag_cappotta_frontale,   
		  flag_rollo,   
		  flag_kit_laterale,   
		  flag_misura_luce_finita,   
		  cod_tipo_stampo,   
		  cod_mat_sis_stampaggio,   
		  rev_stampo,   
		  num_gambe,   
		  num_campate,   
		  note,   
		  note_esterne,   
		  flag_autoblocco,   
		  num_fili_plisse,   
		  num_boccole_plisse,   
		  intervallo_punzoni_plisse,   
		  num_pieghe_plisse,   
		  prof_inf_plisse,   
		  prof_sup_plisse,   
		  colore_passamaneria,   
		  colore_cordolo,   
		  num_punzoni_plisse,   
		  des_vernice_fc,   
		  des_comando_1,   
		  cod_comando_2,   
		  des_comando_2,   
		  flag_addizionale_comando_1,   
		  flag_addizionale_comando_2,   
		  flag_addizionale_supporto,   
		  flag_addizionale_tessuto,   
		  flag_addizionale_vernic,   
		  flag_fc_comando_1,   
		  flag_fc_comando_2,   
		  flag_fc_supporto,   
		  flag_fc_tessuto,   
		  flag_fc_vernic,   
		  flag_allegati,   
		  data_ora_ar,   
		  flag_tessuto_cliente,   
		  flag_tessuto_mant_cliente
FROM comp_det_off_ven
WHERE
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :al_anno_registrazione_orig and
	num_registrazione = :al_num_registrazione_orig;

if sqlca.sqlcode < 0 then
	as_errore = sqlca.sqlerrtext
	return false
end if


INSERT INTO det_off_ven_conf_variabili  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  prog_riga_off_ven,   
			  prog_variabile,   
			  cod_variabile,   
			  flag_tipo_dato,   
			  valore_stringa,   
			  valore_numero,   
			  valore_datetime )  
  SELECT cod_azienda,   
			:al_anno_registrazione_dupl,   
			:al_num_registrazione_dupl,   
			prog_riga_off_ven,   
			prog_variabile,   
			cod_variabile,   
			flag_tipo_dato,   
			valore_stringa,   
			valore_numero,   
			valore_datetime  
			FROM det_off_ven_conf_variabili 
			WHERE	cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :al_anno_registrazione_orig and
						num_registrazione = :al_num_registrazione_orig;

if sqlca.sqlcode < 0 then
	as_errore = sqlca.sqlerrtext
	return false
end if

setnull(as_errore)
return true


end function

on w_tes_off_ven_ptenda.create
call super::create
end on

on w_tes_off_ven_ptenda.destroy
call super::destroy
end on

event pc_menu_configuratore;string ls_cod_pagamento
double ld_sconto

str_conf_prodotto.apri_configuratore = false
if isnull(dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"anno_registrazione")) or dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"anno_registrazione") = 0 or dw_tes_off_ven_det_1.getrow() < 1 then return
	
//do
	str_conf_prodotto.anno_documento = dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"anno_registrazione")
	str_conf_prodotto.num_documento 	= dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"num_registrazione")
	str_conf_prodotto.prog_riga_documento = 0
	str_conf_prodotto.tipo_gestione 	= "OFF_VEN"
	str_conf_prodotto.cod_cliente 		= dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(),"cod_cliente")
	str_conf_prodotto.cod_contatto 	= dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(),"cod_contatto")
	str_conf_prodotto.cod_agente_1 	= dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(),"cod_agente_1")
	str_conf_prodotto.cod_tipo_listino_prodotto = dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(),"cod_tipo_listino_prodotto")
	str_conf_prodotto.cod_valuta 		= dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(),"cod_valuta")
	str_conf_prodotto.data_documento = dw_tes_off_ven_det_1.getitemdatetime(dw_tes_off_ven_det_1.getrow(),"data_registrazione")
	str_conf_prodotto.data_registrazione = dw_tes_off_ven_det_1.getitemdatetime(dw_tes_off_ven_det_1.getrow(),"data_registrazione")
	str_conf_prodotto.sconto_testata = dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"sconto")
	str_conf_prodotto.data_consegna 	= dw_tes_off_ven_det_1.getitemdatetime(dw_tes_off_ven_det_1.getrow(),"data_consegna")
	str_conf_prodotto.cambio_ven =  dw_tes_off_ven_det_1.getitemnumber(dw_tes_off_ven_det_1.getrow(),"cambio_ven")
	
	ls_cod_pagamento = dw_tes_off_ven_det_1.getitemstring(dw_tes_off_ven_det_1.getrow(),"cod_pagamento")
	if isnull(ls_cod_pagamento) then
		str_conf_prodotto.sconto_pagamento = 0
	else
		select sconto
		into   :ld_sconto
		from   tab_pagamenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_pagamento = :ls_cod_pagamento;
		if sqlca.sqlcode = 0 and not isnull(ld_sconto) then
			str_conf_prodotto.sconto_pagamento = ld_sconto
		else
			str_conf_prodotto.sconto_pagamento = 0
		end if
	end if
	setnull(str_conf_prodotto.cod_modello)
	setnull(str_conf_prodotto.cod_versione)
	setnull(str_conf_prodotto.cod_prodotto_finito)
	str_conf_prodotto.quan_vendita = 0
	str_conf_prodotto.prezzo_vendita = 0
	str_conf_prodotto.sconti[1] = 0
	str_conf_prodotto.sconti[2] = 0
	str_conf_prodotto.sconti[3] = 0
	str_conf_prodotto.sconti[4] = 0
	str_conf_prodotto.sconti[5] = 0
	str_conf_prodotto.sconti[6] = 0
	str_conf_prodotto.sconti[7] = 0
	str_conf_prodotto.sconti[8] = 0
	str_conf_prodotto.sconti[9] = 0
	str_conf_prodotto.sconti[10] = 0
	str_conf_prodotto.provvigione_1 = 0
	str_conf_prodotto.provvigione_2 = 0
	setnull(str_conf_prodotto.nota)
	setnull(str_conf_prodotto.cod_prodotto_finito)
	str_conf_prodotto.stato = "N"
	str_conf_prodotto.flag_cambio_modello = false	
	
	//Donato 03/10/2008 modifica su richiesta di marco -------------------------
	s_cs_xx.parametri.parametro_w_ord_ven = this
	//-----------------------------------------------------------------------------
	// Enme 04-06-2010
	setnull(str_conf_prodotto.des_prodotto)
	
	openwithparm(w_configuratore, this)
	
	// ripristino la pcca.current_window
	this.postevent("ue_set_current_window")
	
//	window_open(w_configuratore, 0)
//loop until not(str_conf_prodotto.apri_configuratore)
end event

event pc_setwindow;call super::pc_setwindow;ib_menu_configuratore = true
end event

event pc_menu_dettagli;/** TOLTO ANCESTOR SCRIPT **/

s_cs_xx.parametri.parametro_w_off_ven = this
window_open_parm(w_det_off_ven_ptenda, -1, dw_tes_off_ven_det_1)
end event

type dw_folder from w_tes_off_ven`dw_folder within w_tes_off_ven_ptenda
end type

type dw_folder_search from w_tes_off_ven`dw_folder_search within w_tes_off_ven_ptenda
end type

type tv_1 from w_tes_off_ven`tv_1 within w_tes_off_ven_ptenda
end type

type dw_filtro from w_tes_off_ven`dw_filtro within w_tes_off_ven_ptenda
end type

type dw_tes_off_ven_det_3 from w_tes_off_ven`dw_tes_off_ven_det_3 within w_tes_off_ven_ptenda
end type

type dw_tes_off_ven_det_4 from w_tes_off_ven`dw_tes_off_ven_det_4 within w_tes_off_ven_ptenda
end type

type dw_tes_off_ven_det_1 from w_tes_off_ven`dw_tes_off_ven_det_1 within w_tes_off_ven_ptenda
end type

event dw_tes_off_ven_det_1::itemchanged;call super::itemchanged;string   ls_str
long     ll_giorno, ll_anno_reg, ll_num_reg
datetime ldt_data_consegna, ldt_data_consegna_salvata, ldt_data_registrazione

if i_extendmode then
	choose case i_colname
	
		case "data_consegna"
				
				//leggo la data immessa
				ls_str = left(i_coltext, 10)
				ldt_data_consegna = datetime(date(ls_str))
				
				//verifica se la data consegna è cambiata --------------------------------------------------
				ll_anno_reg = getitemnumber(getrow(),"anno_registrazione")
				ll_num_reg = getitemnumber(getrow(),"num_registrazione")
				
				if ll_anno_reg>0 and ll_num_reg>0 then
					select data_consegna
					into :ldt_data_consegna_salvata
					from tes_off_ven
					where 	cod_azienda=:s_cs_xx.cod_azienda and
								anno_registrazione=:ll_anno_reg and num_registrazione=:ll_num_reg;
					
					if not isnull(ldt_data_consegna_salvata) and year(date(ldt_data_consegna_salvata))>=1950 then
						//data consegna salvata in testata non è NULL
						
						if ldt_data_consegna_salvata <> ldt_data_consegna then
							//è diversa da quella immessa
							ib_nuova_data_consegna = true
						else
							//è uguale a quella immessa
							ib_nuova_data_consegna = false
						end if
					else
						//la data consegna salvata in testata è NULL
						
						if not isnull(ldt_data_consegna) and year(date(ldt_data_consegna))>=1950 then
							//vuoi mettere un vaore NOT NULL
							ib_nuova_data_consegna = true
						else
							//inutile fare update perchè la data NULL era prima e NULL vuoi mettere adesso
							ib_nuova_data_consegna = false
						end if
						//ib_nuova_data_consegna = false
						
					end if
				else
					//probabile sei in nuovo, quindi non essendoci righe di dettaglio inutile lanciare l'update ...
					ib_nuova_data_consegna = false
				end if
				//----------------------------------------------------------------------------------------------
				
	end choose
end if
end event

event dw_tes_off_ven_det_1::updateend;call super::updateend;long				ll_row, ll_num_registrazione
datetime			ldt_data_consegna
integer			li_anno_registrazione

ll_row = getrow()

if ll_row>0 then
else
	return
end if

//se hai cambiato data consegna aggiorna le righe dell'offerta
//if this.getitemstatus(ll_row, "data_consegna", Primary!) <> NotModified! and ib_nuova_data_consegna then
if ib_nuova_data_consegna then
	
	ib_nuova_data_consegna = false
	
	li_anno_registrazione = this.getitemnumber(ll_row, "anno_registrazione")
	ll_num_registrazione = this.getitemnumber(ll_row, "num_registrazione")
	ldt_data_consegna =  this.getitemdatetime(ll_row, "data_consegna")
	
	//aggiorna la data consegna su tutte le righe ----------------------------
	update det_off_ven
	set data_consegna=:ldt_data_consegna
	where cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_registrazione and
				num_registrazione=:ll_num_registrazione;

	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durantel' aggiornamento della data consegna sulle righe offerta.", sqlca)
		return
	end if
	
	//se arrivi fin qui fai commit
	commit;
	
end if
end event

type dw_tes_off_ven_det_2 from w_tes_off_ven`dw_tes_off_ven_det_2 within w_tes_off_ven_ptenda
end type

