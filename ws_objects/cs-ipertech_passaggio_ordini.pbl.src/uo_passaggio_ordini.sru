﻿$PBExportHeader$uo_passaggio_ordini.sru
forward
global type uo_passaggio_ordini from nonvisualobject
end type
end forward

global type uo_passaggio_ordini from nonvisualobject
end type
global uo_passaggio_ordini uo_passaggio_ordini

type variables
long il_tran = 0

str_tran istr_tran[]

str_mail istr_mail[]

wstr_riepilogo istr_riepilogo

uo_condizioni_cliente iuo_condizioni_cliente

end variables

forward prototypes
public function string uof_dt_str (datetime adt_datetime, string as_format, long al_len)
public function string uof_num_str (decimal ad_num, string as_format, long al_len)
public function string uof_str_str (string as_str, long al_len)
public function integer uof_imposta_tran (long al_num_profilo, ref string as_messaggio)
public function integer uof_tipo_det_ord_ven (long al_anno_ord_ven, long al_num_ord_ven, long al_riga_ord_ven, ref string as_messaggio)
public function integer uof_connetti_db (string as_cod_reparto_tx, ref string as_messaggio)
public function integer uof_dati_reparto (string as_cod_reparto, ref string as_tipo_reparto, ref string as_tipo_trasmissione, ref string as_email[], ref string as_messaggio)
public function integer uof_init_cod_prodotto (string as_cod_prodotto_modello, n_tran at_tran, decimal ad_dim_x, string as_cod_tessuto, string as_cod_colore_tessuto, string as_cod_comando, string as_cod_verniciatura, ref string as_cod_prod_finito, ref string as_messaggio)
public function integer uof_disconnetti_db (long al_tran)
public function integer uof_disconnetti_db (long al_tran, ref string as_messaggio)
public function integer uof_rollback (ref string as_messaggio)
public function integer uof_descrizione_ar (long al_anno_ord_ven, long al_num_ord_ven, long al_riga_ord_ven, ref string as_descrizione_ar, ref string as_messaggio)
public function integer uof_commit (ref string as_messaggio)
public function integer uof_stato_buffer_ord_ven (string as_cod_reparto, long al_anno_ord_ven, long al_num_ord_ven, ref string as_resoconto, ref string as_messaggio)
public function integer uof_invia_mail (ref string as_messaggio)
public function integer uof_get_rep_det_ord_ven (long al_anno_ord_ven, long al_num_ord_ven, long al_riga_ord_ven, ref string as_tipo_riga, ref string as_tipo_trasmissione, ref string as_cod_reparto, ref string as_email, ref string as_messaggio)
public function integer uof_riassunto_giornaliero (datetime adt_data, string as_cod_reparto, boolean ab_null, ref string as_messaggio)
public function integer uof_cambio_reparto (long al_anno_ord_ven, long al_num_ord_ven, ref string as_cod_reparto, ref string as_messaggio)
public function integer uof_export_ord_ven (long al_anno_ord_ven, long al_num_ord_ven, boolean ab_esegui, ref string as_messaggio)
public function integer uof_import_ord_ven (string as_cod_reparto, long al_anno_ord_ven, long al_num_ord_ven, boolean ab_respingi, ref string as_messaggio)
end prototypes

public function string uof_dt_str (datetime adt_datetime, string as_format, long al_len);/*
	Funzione che data una variabile datetime ritorna una stringa della lunghezza e del formato specificati
*/

string ls_str


if al_len <= 0 then
	return ""
end if

if isnull(as_format) or trim(as_format) = "" then
	ls_str = ""
else
	ls_str = string(adt_datetime,as_format)
end if

if isnull(ls_str) then
	ls_str = ""
end if

if len(ls_str) > al_len then
	ls_str = left(ls_str,al_len)
else
	ls_str = ls_str + fill(" ",al_len - len(ls_str))
end if

return ls_str
end function

public function string uof_num_str (decimal ad_num, string as_format, long al_len);/*
	Funzione che data una variabile numerica ritorna una stringa della lunghezza e del formato specificati
*/

string ls_str


if al_len <= 0 then
	return ""
end if

if isnull(as_format) or trim(as_format) = "" then
	ls_str = ""
else
	ls_str = string(ad_num,as_format)
end if

if isnull(ls_str) then
	ls_str = ""
end if

if len(ls_str) > al_len then
	ls_str = left(ls_str,al_len)
else
	ls_str = fill("0",al_len - len(ls_str)) + ls_str
end if

return ls_str
end function

public function string uof_str_str (string as_str, long al_len);/*
	Funzione che data una stringa la ritorna della lunghezza specificata, eventualmente riempiendola con spazi
*/

string ls_str


if al_len <= 0 then
	return ""
end if

ls_str = as_str

if isnull(ls_str) then
	ls_str = ""
end if

if len(ls_str) > al_len then
	ls_str = left(ls_str,al_len)
else
	ls_str = ls_str + fill(" ",al_len - len(ls_str))
end if

return ls_str
end function

public function integer uof_imposta_tran (long al_num_profilo, ref string as_messaggio);/*
	Funzione che legge i parametri di connessione dal profilo di registro indicato in AL_NUM_PROFILO.
	La transazione locale viene impsotata con i parametri ottenuti.
	Valori di ritorno:
		 0, lettura parametri effettuata con esito positivo
		-1, errore in lettura dal registro (specificato in AS_MESSAGGIO)
*/

string ls_profilo, ls_valori[], ls_servername, ls_logid, ls_logpass, ls_dbms, ls_database, ls_dbparm, ls_lock


//Impostazione della chiave di registro corrispondente al profilo indicato
ls_profilo = s_cs_xx.chiave_root + "database_" + string(al_num_profilo)

//Verifica della chiave di registro individuata
if registryvalues(ls_profilo,ls_valori) = -1 then
	as_messaggio = "Errore in lettura chiave di registro:~r~n" + ls_profilo
	return -1
end if

if upperbound(ls_valori) < 1 then
	as_messaggio = "Chiave di registro non impostata:~r~n" + ls_profilo
	return -1
end if

//Lettura parametri di connessione dalla chiave di registro individuata
if registryget(ls_profilo,"servername",ls_servername) = -1 then
	as_messaggio = "Errore in lettura parametro SERVERNAME da:~r~n" + ls_profilo
	return -1
end if

if registryget(ls_profilo,"logid",ls_logid) = -1 then
	as_messaggio = "Errore in lettura parametro SERVERNAME da:~r~n" + ls_profilo
	return -1
end if

if registryget(ls_profilo,"logpass",ls_logpass) = -1 then
	as_messaggio = "Errore in lettura parametro SERVERNAME da:~r~n" + ls_profilo
	return -1
end if

if registryget(ls_profilo,"dbms",ls_dbms) = -1 then
	as_messaggio = "Errore in lettura parametro SERVERNAME da:~r~n" + ls_profilo
	return -1
end if

if registryget(ls_profilo,"database",ls_database) = -1 then
	as_messaggio = "Errore in lettura parametro SERVERNAME da:~r~n" + ls_profilo
	return -1
end if

if registryget(ls_profilo,"dbparm",ls_dbparm) = -1 then
	as_messaggio = "Errore in lettura parametro SERVERNAME da:~r~n" + ls_profilo
	return -1
end if

if registryget(ls_profilo,"lock",ls_lock) = -1 then
	as_messaggio = "Errore in lettura parametro SERVERNAME da:~r~n" + ls_profilo
	return -1
end if

// decipto la password se necessario
if upper(ls_dbms) <> "ODBC" then
	
	n_cst_crypto lnv_crypt
	lnv_crypt = CREATE n_cst_crypto
	
	if lnv_crypt.DecryptData(ls_logpass, ls_logpass) < 0 then
		ls_logpass = ""
	end if
	
	DESTROY lnv_crypt
	
end if

//Impostazione delle proprietà della transazione interna
istr_tran[il_tran].tran.servername = ls_servername
istr_tran[il_tran].tran.logid = ls_logid
istr_tran[il_tran].tran.logpass = ls_logpass
istr_tran[il_tran].tran.dbms = ls_dbms
istr_tran[il_tran].tran.database = ls_database
istr_tran[il_tran].tran.dbparm = ls_dbparm
istr_tran[il_tran].tran.lock = ls_lock

return 0
end function

public function integer uof_tipo_det_ord_ven (long al_anno_ord_ven, long al_num_ord_ven, long al_riga_ord_ven, ref string as_messaggio);/*
	Funzione che data una riga d'ordine di vendita verifica se si tratta di sfuso / tecniche / tende da sole
	Valori di ritorno:
		 0, la riga non appartiene a nessuno dei 3 tipi
	    1, la riga è di tipo SFUSO
		 2, la riga è di tipo TENDE TECNICHE
		 3, la riga è di tipo TENDE DA SOLE
		-1, errore della procedura (specificato in AS_MESSAGGIO)
*/

long 	 ll_return, ll_num_riga_appartenenza

string ls_flag_tipo_report, ls_cod_prodotto


//Lettura del codice prodotto e verifica della presenza di una riga di appartenenza
select
	cod_prodotto,
	num_riga_appartenenza
into
	:ls_cod_prodotto,
	:ll_num_riga_appartenenza
from
	det_ord_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :al_anno_ord_ven and
	num_registrazione = :al_num_ord_ven and
	prog_riga_ord_ven = :al_riga_ord_ven;
	
if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura dati riga:~n" + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_messaggio = "Errore in lettura dati riga:~nRiga indicata non trovata"
	return -1
end if

//Verifica che la riga abbia impostato il codice prodotto
if isnull(ls_cod_prodotto) then
	as_messaggio = "La riga indicata non ha il codice prodotto"
	return -1
end if

//Se esiste la riga di appartenenza allora vale il tipo della riga padre
if not isnull(ll_num_riga_appartenenza) and ll_num_riga_appartenenza > 0 then
	
	ll_return = uof_tipo_det_ord_ven(al_anno_ord_ven,al_num_ord_ven,al_riga_ord_ven,as_messaggio)
	
	if ll_return < 0 then
		as_messaggio = "Errore in verifica reparto riga padre:~n" + as_messaggio
		return -1
	else
		return ll_return
	end if

else
	
	//Verifica flags configuratore per il prodotto indicato nella riga
	select
		flag_tipo_report
	into
   	:ls_flag_tipo_report
	from
   	tab_flags_configuratore
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_modello = :ls_cod_prodotto;
		
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in lettura flags configuratore:~n" + sqlca.sqlerrtext
		return -1
	elseif sqlca.sqlcode = 100 then
		//Il prodotto non è un modello quindi si tratta di sfuso
		return 1
	elseif sqlca.sqlcode = 0 then
		choose case ls_flag_tipo_report
			case "C"
				//Il prodotto è un modello e il tipo report è C, si tratta di tende tecniche
				return 2
			case "A"
				//Il prodotto è un modello e il tipo report è A, si tratta di tende da sole
				return 3
			case else
				//Tipo riga non identificato
				return 0
		end choose
	end if
	
end if

return 0
end function

public function integer uof_connetti_db (string as_cod_reparto_tx, ref string as_messaggio);/*
	Funzione che connette la transazione i_tran al DB di un reparto esterno.
	Il reparto a cui ci si connette è quello indicato nel parametro AS_COD_REPARTO_TX.
	La funzione restituisce inoltre nei parametri AS_COD_REPARTO_RX e AS_COD_CLIENTE il codice del reparto e
	il codice del cliente che corrispondono al trasmittente nel database del ricevente.
	Valori di ritorno:
		   0, connessione effettuata con esito positivo
		  -1, errore di qualsiasi genere (specificato in dettaglio in AS_MESSAGGIO)
*/

boolean lb_trovato

long	  ll_i, ll_num_profilo

string  ls_tipo_reparto, ls_tipo_trasmissione, ls_user, ls_password, ls_cod_reparto_rx, ls_cod_cliente


lb_trovato = false

for ll_i = 1 to upperbound(istr_tran)
	
	if istr_tran[ll_i].cod_reparto_tx = as_cod_reparto_tx then
		il_tran = ll_i
		lb_trovato = true
		exit
	end if
	
next

if lb_trovato then
	return 0
end if

il_tran = upperbound(istr_tran) + 1

istr_tran[il_tran].tran = create n_tran

//Lettura delle impostazioni del reparto indicato
select
	flag_tipo_reparto,
	flag_tipo_trasmissione,
	num_profilo_registry,
	user_tx,
	password_tx
into
	:ls_tipo_reparto,
	:ls_tipo_trasmissione,
	:ll_num_profilo,
	:ls_user,
	:ls_password
from
	anag_reparti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_reparto = :as_cod_reparto_tx;
	
if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura dati reparto:~r~n" + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura dati reparto:~r~nReparto indicato non trovato"
	return -1
end if

//Il reparto deve essere codificato come (E)STERNO, altrimenti non è possibile effettuare la connessione
if isnull(ls_tipo_reparto) or ls_tipo_reparto <> "E" then
	as_messaggio = "Il reparto indicato non è un reparto esterno"
	return -1
end if

//Il reparto esterno deve essere impostato per trasmettere tramite la connessione diretta al DB remoto
if isnull(ls_tipo_trasmissione) or ls_tipo_trasmissione <> "C" then
	as_messaggio = "Il reparto esterno indicato non è configurato per la connessione diretta"
	return -1
end if

//Nei parametri di connessione deve essere indicato il profilo del registro con cui colegarsi al DB remoto
if isnull(ll_num_profilo) or ll_num_profilo < 1 then
	as_messaggio = "Il reparto esterno indicato non è collegato ad alcun profilo di connessione"
	return -1
end if

//Se la transazione era già collegata precedentemente, non è necessario connettersi
if not istr_tran[il_tran].connected then
	
	//Viene richiamata la funzione per l'impostazione dei parametri di connessione della transazione interna
	if uof_imposta_tran(ll_num_profilo,as_messaggio) <> 0 then
		as_messaggio = "Errore in impostazione transazione esterna.~r~n" + as_messaggio
		return -1
	end if
	
	//Connessione al DB remoto
	connect using istr_tran[il_tran].tran;
	
	if istr_tran[il_tran].tran.sqlcode <> 0 then
		as_messaggio = "Errore in connessione al DB del reparto:~r~n" + istr_tran[il_tran].tran.sqlerrtext
		return -1
	end if
	
end if

//In base ai dati di conessione si effettua la "login" del trasmittente come reparto del ricevente
select
	cod_reparto,
	cod_cliente
into
	:ls_cod_reparto_rx,
	:ls_cod_cliente
from
	anag_reparti
where
	flag_tipo_reparto = "E" and
	user_rx = :ls_user and
	password_rx = :ls_password
using
	istr_tran[il_tran].tran;

if istr_tran[il_tran].tran.sqlcode < 0 then
	as_messaggio = "Errore durante login come reparto del ricevente:~n" + istr_tran[il_tran].tran.sqlerrtext
	uof_disconnetti_db(il_tran)
	return -1
elseif istr_tran[il_tran].tran.sqlcode = 100 then
	as_messaggio = "Errore durante login come reparto del ricevente:~nNessun reparto del ricevente corrisponde alla login indicata"
	uof_disconnetti_db(il_tran)
	return -1
end if

//Si imposta l'identità di connessione del ricevente per la connessione all'attuale ricevente
istr_tran[il_tran].cod_reparto_tx = as_cod_reparto_tx
istr_tran[il_tran].cod_reparto_rx = ls_cod_reparto_rx
istr_tran[il_tran].cod_cliente = ls_cod_cliente

//Si memorizza lo stato di connessione
istr_tran[il_tran].connected = true

return 0
end function

public function integer uof_dati_reparto (string as_cod_reparto, ref string as_tipo_reparto, ref string as_tipo_trasmissione, ref string as_email[], ref string as_messaggio);/*
	Funzione che dato un reparto ne estrae i parametri di configurazione
	Valori di ritorno:
		 0, dati reparto letti correttamente
	  	-1, errore della procedura (specificato in AS_MESSAGGIO)
*/

string ls_email_1,ls_email_2,ls_email_3,ls_email_4, ls_tipo_reparto, ls_tipo_trasmissione


select
	flag_tipo_reparto,
	flag_tipo_trasmissione,
	email_esportazione,
	email_variazioni,
	email_riassunto,
	email_ascii
into
	:ls_tipo_reparto,
	:ls_tipo_trasmissione,
	:ls_email_1,
	:ls_email_2,
	:ls_email_3,
	:ls_email_4
from
	anag_reparti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_reparto = :as_cod_reparto;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura dati reparto:~n" + sqlca.sqlerrtext
	setnull(as_tipo_reparto)
	setnull(as_tipo_trasmissione)
	setnull(as_email[1])
	setnull(as_email[2])
	setnull(as_email[3])
	setnull(as_email[4])
	return -1
elseif sqlca.sqlcode = 100 then
	as_messaggio = "Errore in lettura dati reparto:~nReparto indicato non trovato"
	setnull(as_tipo_reparto)
	setnull(as_tipo_trasmissione)
	setnull(as_email[1])
	setnull(as_email[2])
	setnull(as_email[3])
	setnull(as_email[4])
	return -1
end if

as_tipo_reparto = ls_tipo_reparto
as_tipo_trasmissione = ls_tipo_trasmissione
as_email[1] = ls_email_1
as_email[2] = ls_email_2
as_email[3] = ls_email_3
as_email[4] = ls_email_4

return 0
end function

public function integer uof_init_cod_prodotto (string as_cod_prodotto_modello, n_tran at_tran, decimal ad_dim_x, string as_cod_tessuto, string as_cod_colore_tessuto, string as_cod_comando, string as_cod_verniciatura, ref string as_cod_prod_finito, ref string as_messaggio);//		Funzione che partendo dal modello passato come parametro restituisce il codice prodotto finito
//
// nome: uof_init_cod_prodotto
// tipo: string
//  
//	Variabili passate: 		nome					 				tipo				passaggio per			commento
//							
//									uof_cod_prodotto_modello	    String			valore
//									as_cod_tessuto						string	
//									as_cod_colore_tessuto			string
//
//

string ls_cod_veloce,ls_linea_prodotto,ls_classe_merceologica

ls_linea_prodotto = left(as_cod_prodotto_modello, 1)
ls_classe_merceologica = mid(as_cod_prodotto_modello, 2, 1)

as_cod_prod_finito = as_cod_prodotto_modello + "     "

// --------------------------------------  inizio ---------------------------------------------

if ls_classe_merceologica <> "0" and ls_classe_merceologica <> "S" then
	if ad_dim_x > 999 then
		as_cod_prod_finito = replace(as_cod_prod_finito, 7, 3, "---")
	else
		as_cod_prod_finito = replace(as_cod_prod_finito, 7, 3, fill("0", 3 - len(trim(string(integer(ad_dim_x))))) + trim(string(integer(ad_dim_x))))
	end if
end if

// ----------------------------------------  tessuto ---------------------------------------------
if not isnull(as_cod_colore_tessuto) and len(as_cod_colore_tessuto) > 0 and not isnull(as_cod_tessuto) and len(as_cod_tessuto) > 0  then
	choose case ls_linea_prodotto
		case "A", "B","C","D","E","F","G","H","I","L","M","N","O","P","Q","R","S","T","U","V","Z"
			select cod_veloce
			 into :ls_cod_veloce
			 from tab_tessuti
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_non_a_magazzino = :as_cod_colore_tessuto and
					cod_tessuto = :as_cod_tessuto
			using at_tran;
			if at_tran.sqlcode = 100 then
				// se il codice del tessuto manca non c'è verrà fuori un errore nella maschera di importazione
			elseif SQLCA.sqlcode <> 0 then
				as_messaggio = "Errore durante la ricerca 'codice veloce' in tabella tessuti. Dettaglio errore " + sqlca.sqlerrtext
				return -1
			end if
			as_cod_prod_finito = Replace(as_cod_prod_finito, 5, 1, ls_cod_veloce)
	end choose
end if
//

// ------------------------------------------ comandi --------------------------------------------------------
if not isnull(as_cod_comando) and len(as_cod_comando) > 0 then
	choose case ls_linea_prodotto
		case "A", "B","C","D","E","F","G","H","I","L","M","N","O","P","Q","R","S","T","U","V","Z"
			select cod_veloce
			 into :ls_cod_veloce
			 from tab_comandi
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_comando = :as_cod_comando
			using at_tran;
			if at_tran.sqlcode = 100 then
				// se non esiste non mi interessa perchè poi verrà specificato nella maschera di importazione
			elseif sqlca.sqlcode < 0 then
				as_messaggio = "Errore durante del codice veloce dalla tabella tipi comando"
				return -1
			end if
			as_cod_prod_finito = Replace(as_cod_prod_finito, 7, 2, ls_cod_veloce)
	end choose
end if

// ----------------------------------- lastre / verniciature ---------------------------------------------

if not isnull(as_cod_verniciatura) and len(as_cod_verniciatura) > 0 then
	select cod_veloce
	into  :ls_cod_veloce
	from  tab_verniciatura
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_verniciatura = :as_cod_verniciatura;
	if sqlca.sqlcode = 100 then
				// se non esiste non mi interessa perchè poi verrà specificato nella maschera di importazione
	elseif SQLCA.sqlcode < 0 then
		as_messaggio = "Errore durante ricerca codice verniciatura in tabella verniciature.~r~nDettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
	as_cod_prod_finito = Replace(as_cod_prod_finito, 6, 1, ls_cod_veloce)
end if

// -----------------------------------  visualizzo il codicie finito ----------------------------------------

return 0



end function

public function integer uof_disconnetti_db (long al_tran);/*
	Funzione che effettua la disconnessione da un DB remoto in modalità "quiet".
	Non gestisce eventuali messaggi di errore
	Valori di ritorno:
		 0, disconnesso
		-1, errore durante disconnessione
*/

string ls_null


//Viene richiamata la funzione di disconnessione ignorando il messaggio di ritorno
if uof_disconnetti_db(al_tran,ls_null) <> 0 then
	return -1
end if

return 0
end function

public function integer uof_disconnetti_db (long al_tran, ref string as_messaggio);/*
	Funzione che effettua la disconnessione da un DB remoto.
	Gestisce eventuali messaggi di errore.
	Valori di ritorno:
		 0, disconnesso
		-1, errore durante disconnessione (specificato in AS_MESSAGGIO)
*/

//Disconnessione dal DB remoto
disconnect using istr_tran[al_tran].tran;

if istr_tran[al_tran].tran.sqlcode <> 0 then
	as_messaggio = "Errore in disconnessione DB:~r~n" + istr_tran[al_tran].tran.sqlerrtext
	return -1
end if

//Si resetta l'identità di connessione del ricevente per la connessione all'attuale ricevente
setnull(istr_tran[al_tran].cod_reparto_tx)
setnull(istr_tran[al_tran].cod_reparto_rx)
setnull(istr_tran[al_tran].cod_cliente)

//Aggiornamento dello stato di connessione
istr_tran[al_tran].connected = false

destroy istr_tran[al_tran].tran

return 0
end function

public function integer uof_rollback (ref string as_messaggio);/*
	Funzione che esegue il rollback delle transazioni interne
	Valori di ritorno:
		 0, rollback eseguito con successo
		-1, errore (specificato in AS_MESSAGGIO)
*/

long ll_i


il_tran = 0

for ll_i = 1 to upperbound(istr_tran)
	
	if not istr_tran[ll_i].connected then
		continue
	end if
	
	if not isvalid(istr_tran[ll_i].tran) then
		continue
	end if
	
	rollback using istr_tran[ll_i].tran;
	
	if istr_tran[ll_i].tran.sqlcode <> 0 then
		as_messaggio = "Errore in rollback reparto " + istr_tran[ll_i].cod_reparto_tx + ":~n" + istr_tran[ll_i].tran.sqlerrtext
		return -1
	end if
	
	if uof_disconnetti_db(ll_i,as_messaggio) <> 0 then
		return -1
	end if
	
next

return 0
end function

public function integer uof_descrizione_ar (long al_anno_ord_ven, long al_num_ord_ven, long al_riga_ord_ven, ref string as_descrizione_ar, ref string as_messaggio);long	  ll_pos

boolean lb_leggi_des

string  ls_col_des_ar, ls_tab_des_ar, ls_sql, ls_cod_des_ar, ls_des_des_ar


select
	nome_colonna_descrizione_ar
into
	:ls_col_des_ar
from
	det_ord_ven a,
	tab_flags_configuratore b
where
	b.cod_azienda = a.cod_azienda and
	b.cod_modello = a.cod_prodotto and
	a.cod_azienda = :s_cs_xx.cod_azienda and
	a.anno_registrazione = :al_anno_ord_ven and
	a.num_registrazione = :al_num_ord_ven and
	a.prog_riga_ord_ven = :al_riga_ord_ven;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura colonna descrizione AR: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	setnull(ls_col_des_ar)
end if

if isnull(ls_col_des_ar) then
	setnull(as_descrizione_ar)
	return 0
end if

ll_pos = pos(ls_col_des_ar,".",1)

if isnull(ll_pos) or ll_pos < 1 then
	as_messaggio = "Impossibile individuare la colonna e la tabella della descrizione AR"
	return -1
end if

ls_tab_des_ar = left(ls_col_des_ar,ll_pos - 1)

ls_col_des_ar = right(ls_col_des_ar,len(ls_col_des_ar) - ll_pos)

ls_sql = "select " + ls_col_des_ar + " from " + ls_tab_des_ar + " where cod_azienda = '" + s_cs_xx.cod_azienda + &
			"' and anno_registrazione = " + string(al_anno_ord_ven) + " and num_registrazione = " + &
			string(al_num_ord_ven) + " and prog_riga_ord_ven = " + string(al_riga_ord_ven)

declare ar dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic ar using descriptor sqlda;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in open cursore ar: " + sqlca.sqlerrtext
	return -1
end if

fetch ar using descriptor sqlda;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in fetch cursore ar: " + sqlca.sqlerrtext
	close ar;
	return -1
elseif sqlca.sqlcode = 100 then
	as_messaggio = "Errore in fetch cursore ar: riga d'ordine indicata non trovata"
	close ar;
	return -1
end if

close ar;

choose case sqlda.outparmtype[1]
	case typestring!
		ls_cod_des_ar = sqlda.getdynamicstring(1)
	case typedecimal!,typedouble!,typeinteger!,typelong!,typereal!
		ls_cod_des_ar = string(sqlda.getdynamicnumber(1))
	case else
		as_messaggio = "Tipo dato non riconosciuto"
		return -1
end choose

if isnull(ls_cod_des_ar) then
	setnull(as_descrizione_ar)
	return 0
end if

as_descrizione_ar = ls_cod_des_ar

choose case ls_col_des_ar
	case "cod_non_a_magazzino","cod_mant_non_a_magazzino"
		ls_sql = "select des_colore_tessuto from tab_colori_tessuti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_colore_tessuto = '" + ls_cod_des_ar + "'"
	case "cod_prodotto","cod_tessuto","cod_tessuto_mant","cod_verniciatura","cod_comando","cod_comando_2","cod_tipo_supporto"
		ls_sql = "select des_prodotto from anag_prodotti where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + ls_cod_des_ar + "'"
	case "cod_misura"
		ls_sql = "select des_misura from tab_misure where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_misura = '" + ls_cod_des_ar + "'"
	case else
		return 0
end choose

prepare sqlsa from :ls_sql;

open dynamic ar;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in open cursore ar: " + sqlca.sqlerrtext
	return -1
end if

fetch ar into :ls_des_des_ar;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in fetch cursore ar: " + sqlca.sqlerrtext
	close ar;
	return -1
elseif sqlca.sqlcode = 100 then
	as_messaggio = "Errore in fetch cursore ar: dato indicato non trovato"
	close ar;
	return -1
end if

close ar;

if not isnull(ls_des_des_ar) then
	as_descrizione_ar += " " + ls_des_des_ar
end if

return 0
end function

public function integer uof_commit (ref string as_messaggio);/*
	Funzione che esegue il commit delle transazioni interne
	Valori di ritorno:
		 0, commit eseguito con successo
		-1, errore (specificato in AS_MESSAGGIO)
*/

long ll_i


il_tran = 0

for ll_i = 1 to upperbound(istr_tran)
	
	if not istr_tran[ll_i].connected then
		continue
	end if
	
	if not isvalid(istr_tran[ll_i].tran) then
		continue
	end if
	
	commit using istr_tran[ll_i].tran;
	
	if istr_tran[ll_i].tran.sqlcode <> 0 then
		as_messaggio = "Errore in commit reparto " + istr_tran[ll_i].cod_reparto_tx + ":~n" + istr_tran[ll_i].tran.sqlerrtext
		return -1
	end if
	
	if uof_disconnetti_db(ll_i,as_messaggio) <> 0 then
		return -1
	end if
	
next

return 0
end function

public function integer uof_stato_buffer_ord_ven (string as_cod_reparto, long al_anno_ord_ven, long al_num_ord_ven, ref string as_resoconto, ref string as_messaggio);/*
	Funzione che restituisce una stringa contenente lo stato dell'ordine indicato nelle tabelle
	di appoggio e l'elenco delle eventuali modifiche ai dati ricevuti apportate dal ricevente.
	Valori di ritorno:
		 0, resoconto elaborato correttamente (contenuto in AS_RESOCONTO)
	  	-1, errore della procedura (specificato in AS_MESSAGGIO)
*/

long	 	ll_riga

datetime ldt_data_cons_old, ldt_data_cons_new

string 	ls_resoconto, ls_mod_tes, ls_mod_det, ls_mod_riga

dec{4} 	ld_quan_ord_old, ld_quan_ord_new, ld_quan_um_old, ld_quan_um_new, ld_dim_x_old, ld_dim_x_new, &
		 	ld_dim_y_old, ld_dim_y_new, ld_dim_z_old, ld_dim_z_new, ld_alt_asta_old, ld_alt_asta_new, &
		 	ld_alt_mant_old, ld_alt_mant_new

string 	ls_flag_esito, ls_note_esito, ls_prod_old, ls_prod_new, ls_misura_old, ls_misura_new, ls_tessuto_old, &
		 	ls_tessuto_new, ls_non_mag_old, ls_non_mag_new, ls_pos_com_old, ls_pos_com_new, ls_comando_old, &
		 	ls_comando_new, ls_tess_mant_old, ls_tess_mant_new, ls_mant_non_mag_old, ls_mant_non_mag_new, &
		 	ls_supporto_old, ls_supporto_new, ls_verniciatura_old, ls_verniciatura_new, ls_comando_2_old, &
		 	ls_comando_2_new, ls_misura_l_f_old, ls_misura_l_f_new, ls_allegati_old, ls_allegati_new, &
		 	ls_colore_pass_old, ls_colore_pass_new, ls_colore_cord_old, ls_colore_cord_new, &
			ls_tipo_mant_old, ls_tipo_mant_new


setnull(as_resoconto)

//Verifica che siano stati passati dei valori validi per la chiave della tabella di appoggio
if isnull(as_cod_reparto) or isnull(al_anno_ord_ven) or isnull(al_num_ord_ven) then
	as_messaggio = "Indicare la numerazione dell'ordine e il reparto trasmittente"
	return -1
end if

//Inizializzazione della stringa contenente il resoconto
ls_resoconto = "Resoconto situazione ordine " + string(al_anno_ord_ven) + "/" + string(al_num_ord_ven)

//Lettura dei dati di testata dalle tabelle di appoggio
select
	b.flag_esito_import,
	b.note_esito,
	a.data_consegna,
	b.data_consegna
into
	:ls_flag_esito,
	:ls_note_esito,
	:ldt_data_cons_old,
	:ldt_data_cons_new
from
	buffer_ord_ven a,
	buffer_ord_ven b
where
	a.cod_reparto = b.cod_reparto and
	a.anno_registrazione = b.anno_registrazione and
	a.num_registrazione = b.num_registrazione and
	a.flag_originale = 'S' and
	b.flag_originale = 'N' and
	a.cod_reparto = :as_cod_reparto and
	a.anno_registrazione = :al_anno_ord_ven and
	a.num_registrazione = :al_num_ord_ven;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura testata tabella di appoggio: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_messaggio = "Errore in lettura testata tabella di appoggio: ordine indicato non trovato"
	return -1
end if

//Impostazione messaggio sull'esito dell'ordine
choose case ls_flag_esito
	case "S"
		ls_resoconto += "~n~nL'ordine è attualmente in attesa di essere elaborato dal ricevente"
	case "A"
		ls_resoconto += "~n~nL'ordine è stato ACCETTATO"
	case "R"
		ls_resoconto += "~n~nL'ordine è stato RESPINTO"
end choose

//Aggiunta eventuali note del ricevente
if not isnull(ls_note_esito) then
	ls_resoconto += "~n~nNote del ricevente:~n" + ls_note_esito
end if

//Indicazione sulla modifica della data di consegna
if ldt_data_cons_new <> ldt_data_cons_old then
	ls_mod_tes = "~n~t~tdata consenga modificata da " + string(date(ldt_data_cons_old)) + " in " + string(date(ldt_data_cons_new))
elseif isnull(ldt_data_cons_new) and not isnull(ldt_data_cons_old) then
	ls_mod_tes = "~n~t~tdata consenga azzerata"
elseif not isnull(ldt_data_cons_new) and isnull(ldt_data_cons_old) then
	ls_mod_tes = "~n~t~tdata consenga impostata a " + string(date(ldt_data_cons_new))
else
	ls_mod_tes = ""
end if

if len(ls_mod_tes) > 0 then
	ls_mod_tes = "~n~n~tTestata ordine" + ls_mod_tes
end if

//Lettura dati di dettaglio dalle tabelle di appoggio
declare righe cursor for
select
	a.prog_riga_ord_ven,
	a.cod_prodotto,
	b.cod_prodotto,
	a.cod_misura,
	b.cod_misura,
	a.cod_tessuto,
	b.cod_tessuto,
	a.cod_non_a_magazzino,
	b.cod_non_a_magazzino,
	a.pos_comando,
	b.pos_comando,
	a.cod_comando,
	b.cod_comando,
	a.cod_tessuto_mant,
	b.cod_tessuto_mant,
	a.cod_mant_non_a_magazzino,
	b.cod_mant_non_a_magazzino,
	a.cod_tipo_supporto,
	b.cod_tipo_supporto,
	a.cod_verniciatura,
	b.cod_verniciatura,
	a.cod_comando_2,
	b.cod_comando_2,
	a.quan_ordine,
	b.quan_ordine,
	a.quantita_um,
	b.quantita_um,
	a.dim_x,
	b.dim_x,
	a.dim_y,
	b.dim_y,
	a.dim_z,
	b.dim_z,
	a.flag_misura_luce_finita,
	b.flag_misura_luce_finita,
	a.alt_asta,
	b.alt_asta,
	a.alt_mantovana,
	b.alt_mantovana,
	a.flag_allegati,
	b.flag_allegati,
	a.colore_passamaneria,
	b.colore_passamaneria,
	a.colore_cordolo,
	b.colore_cordolo,
	a.flag_tipo_mantovana,
	b.flag_tipo_mantovana
from
	buffer_ord_ven_det a,
	buffer_ord_ven_det b
where
	a.cod_reparto = b.cod_reparto and
	a.anno_registrazione = b.anno_registrazione and
	a.num_registrazione = b.num_registrazione and
	a.prog_riga_ord_ven = b.prog_riga_ord_ven and
	a.flag_originale = 'S' and
	b.flag_originale = 'N' and
	a.cod_reparto = :as_cod_reparto and
	a.anno_registrazione = :al_anno_ord_ven and
	a.num_registrazione = :al_num_ord_ven;

open righe;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in open cursore righe: " + sqlca.sqlerrtext
	return -1
end if

ls_mod_det = ""

//Si scorrono tutte le righe dell'ordine per verificare le differenze fra i dati esportati e quelli importati
do while true
	
	fetch
		righe
	into
		:ll_riga,
		:ls_prod_old,
		:ls_prod_new,
		:ls_misura_old,
		:ls_misura_new,
		:ls_tessuto_old,
		:ls_tessuto_new,
		:ls_non_mag_old,
		:ls_non_mag_new,
		:ls_pos_com_old,
		:ls_pos_com_new,
		:ls_comando_old,
		:ls_comando_new,
		:ls_tess_mant_old,
		:ls_tess_mant_new,
		:ls_mant_non_mag_old,
		:ls_mant_non_mag_new,
		:ls_supporto_old,
		:ls_supporto_new,
		:ls_verniciatura_old,
		:ls_verniciatura_new,
		:ls_comando_2_old,
		:ls_comando_2_new,
		:ld_quan_ord_old,
		:ld_quan_ord_new,
		:ld_quan_um_old,
		:ld_quan_um_new,
		:ld_dim_x_old,
		:ld_dim_x_new,
		:ld_dim_y_old,
		:ld_dim_y_new,
		:ld_dim_z_old,
		:ld_dim_z_new,
		:ls_misura_l_f_old,
		:ls_misura_l_f_new,
		:ld_alt_asta_old,
		:ld_alt_asta_new,
		:ld_alt_mant_old,
		:ld_alt_mant_new,
		:ls_allegati_old,
		:ls_allegati_new,
		:ls_colore_pass_old,
		:ls_colore_pass_new,
		:ls_colore_cord_old,
		:ls_colore_cord_new,
		:ls_tipo_mant_old,
		:ls_tipo_mant_new;
	
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in fetch cursore righe: " + sqlca.sqlerrtext
		close righe;
		return -1
	elseif sqlca.sqlcode = 100 then
		close righe;
		exit
	end if
	
	ls_mod_riga = ""
	
	//Verifica eventuali modifiche apportate ai dati di dettaglio nelle tabelle di appoggio
	
	if ls_misura_new <> ls_misura_old then
		ls_mod_riga += "~n~t~t~tUnità di misura modificata da " + ls_misura_old + " a " + ls_misura_new
	elseif isnull(ls_misura_new) and not isnull(ls_misura_old) then
		ls_mod_riga += "~n~t~t~tUnità di misura azzerata"
	elseif not isnull(ls_misura_new) and isnull(ls_misura_old) then
		ls_mod_riga += "~n~t~t~tUnità di misura impostata a " + ls_misura_new
	end if
	
	if ls_tessuto_new <> ls_tessuto_old then
		ls_mod_riga += "~n~t~t~tTessuto modificato da " + ls_tessuto_old + " a " + ls_tessuto_new
	elseif isnull(ls_tessuto_new) and not isnull(ls_tessuto_old) then
		ls_mod_riga += "~n~t~t~tTessuto azzerato"
	elseif not isnull(ls_tessuto_new) and isnull(ls_tessuto_old) then
		ls_mod_riga += "~n~t~t~tTessuto impostat a " + ls_tessuto_new
	end if
	
	if ls_non_mag_new <> ls_non_mag_old then
		ls_mod_riga += "~n~t~t~tColore tessuto modificato da " + ls_non_mag_old + " a " + ls_non_mag_new
	elseif isnull(ls_non_mag_new) and not isnull(ls_non_mag_old) then
		ls_mod_riga += "~n~t~t~tColore tessuto azzerato"
	elseif not isnull(ls_non_mag_new) and isnull(ls_non_mag_old) then
		ls_mod_riga += "~n~t~t~tColore tessuto impostato a " + ls_non_mag_new
	end if
	
	if ls_pos_com_new <> ls_pos_com_old then
		ls_mod_riga += "~n~t~t~tPosizione comando modificata da " + ls_pos_com_old + " a " + ls_pos_com_new
	elseif isnull(ls_pos_com_new) and not isnull(ls_pos_com_old) then
		ls_mod_riga += "~n~t~t~tPosizione comando azzerata"
	elseif not isnull(ls_pos_com_new) and isnull(ls_pos_com_old) then
		ls_mod_riga += "~n~t~t~tPosizione comando impostata a " + ls_pos_com_new
	end if
	
	if ls_comando_new <> ls_comando_old then
		ls_mod_riga += "~n~t~t~tComando modificato da " + ls_comando_old + " a " + ls_comando_new
	elseif isnull(ls_comando_new) and not isnull(ls_comando_old) then
		ls_mod_riga += "~n~t~t~tComando azzerato"
	elseif not isnull(ls_comando_new) and isnull(ls_comando_old) then
		ls_mod_riga += "~n~t~t~tComando impostato a " + ls_comando_new
	end if
	
	if ls_tess_mant_new <> ls_tess_mant_old then
		ls_mod_riga += "~n~t~t~tTessuto mantovana modificato da " + ls_tess_mant_old + " a " + ls_tess_mant_new
	elseif isnull(ls_tess_mant_new) and not isnull(ls_tess_mant_old) then
		ls_mod_riga += "~n~t~t~tTessuto mantovana azzerato"
	elseif not isnull(ls_tess_mant_new) and isnull(ls_tess_mant_old) then
		ls_mod_riga += "~n~t~t~tTessuto mantovana impostato a " + ls_tess_mant_new
	end if
	
	if ls_mant_non_mag_new <> ls_mant_non_mag_old then
		ls_mod_riga += "~n~t~t~tColore tessuto mantovana modificato da " + ls_mant_non_mag_old + " a " + ls_mant_non_mag_new
	elseif isnull(ls_mant_non_mag_new) and not isnull(ls_mant_non_mag_old) then
		ls_mod_riga += "~n~t~t~tColore tessuto mantovana azzerato"
	elseif not isnull(ls_mant_non_mag_new) and isnull(ls_mant_non_mag_old) then
		ls_mod_riga += "~n~t~t~tColore tessuto mantovana impostato a " + ls_mant_non_mag_new
	end if
	
	if ls_supporto_new <> ls_supporto_old then
		ls_mod_riga += "~n~t~t~tTipo supporto modificato da " + ls_supporto_old + " a " + ls_supporto_new
	elseif isnull(ls_supporto_new) and not isnull(ls_supporto_old) then
		ls_mod_riga += "~n~t~t~tTipo supporto azzerato"
	elseif not isnull(ls_supporto_new) and isnull(ls_supporto_old) then
		ls_mod_riga += "~n~t~t~tTipo supporto impostato a " + ls_supporto_new
	end if
	
	if ls_verniciatura_new <> ls_verniciatura_old then
		ls_mod_riga += "~n~t~t~tVerniciatura modificata da " + ls_verniciatura_old + " a " + ls_verniciatura_new
	elseif isnull(ls_verniciatura_new) and not isnull(ls_verniciatura_old) then
		ls_mod_riga += "~n~t~t~tVerniciatura azzerata"
	elseif not isnull(ls_verniciatura_new) and isnull(ls_verniciatura_old) then
		ls_mod_riga += "~n~t~t~tVerniciatura impostata a " + ls_verniciatura_new
	end if
	
	if ls_comando_2_new <> ls_comando_2_old then
		ls_mod_riga += "~n~t~t~tGuide modificate da " + ls_comando_2_old + " a " + ls_comando_2_new
	elseif isnull(ls_comando_2_new) and not isnull(ls_comando_2_old) then
		ls_mod_riga += "~n~t~t~tGuide azzerate"
	elseif not isnull(ls_comando_2_new) and isnull(ls_comando_2_old) then
		ls_mod_riga += "~n~t~t~tGuide impostate a " + ls_comando_2_new
	end if
	
	if ls_misura_l_f_new <> ls_misura_l_f_old then
		ls_mod_riga += "~n~t~t~tFlag misura luce finita modificato da " + ls_misura_l_f_old + " a " + ls_misura_l_f_new
	elseif isnull(ls_misura_l_f_new) and not isnull(ls_misura_l_f_old) then
		ls_mod_riga += "~n~t~t~tFlag misura luce finita azzerato"
	elseif not isnull(ls_misura_l_f_new) and isnull(ls_misura_l_f_old) then
		ls_mod_riga += "~n~t~t~tFlag misura luce finita impostato a " + ls_misura_l_f_new
	end if
	
	if ls_allegati_new <> ls_allegati_old then
		ls_mod_riga += "~n~t~t~tFlag allegati modificato da " + ls_allegati_old + " a " + ls_allegati_new
	elseif isnull(ls_allegati_new) and not isnull(ls_allegati_old) then
		ls_mod_riga += "~n~t~t~tFlag allegati azzerato"
	elseif not isnull(ls_allegati_new) and isnull(ls_allegati_old) then
		ls_mod_riga += "~n~t~t~tFlag allegati impostato a " + ls_allegati_new
	end if
	
	if ls_colore_pass_new <> ls_colore_pass_old then
		ls_mod_riga += "~n~t~t~tColore passamaneria modificato da " + ls_colore_pass_old + " a " + ls_colore_pass_new
	elseif isnull(ls_colore_pass_new) and not isnull(ls_colore_pass_old) then
		ls_mod_riga += "~n~t~t~tColore passamaneria azzerato"
	elseif not isnull(ls_colore_pass_new) and isnull(ls_colore_pass_old) then
		ls_mod_riga += "~n~t~t~tColore passamaneria impostato a " + ls_colore_pass_new
	end if
	
	if ls_colore_cord_new <> ls_colore_cord_old then
		ls_mod_riga += "~n~t~t~tColore cordolo modificato da " + ls_colore_cord_old + " a " + ls_colore_cord_new
	elseif isnull(ls_colore_cord_new) and not isnull(ls_colore_cord_old) then
		ls_mod_riga += "~n~t~t~tColore cordolo azzerato"
	elseif not isnull(ls_colore_cord_new) and isnull(ls_colore_cord_old) then
		ls_mod_riga += "~n~t~t~tColore cordolo impostato a " + ls_colore_cord_new
	end if
	
	if ls_tipo_mant_new <> ls_tipo_mant_old then
		ls_mod_riga += "~n~t~t~tTipo mantovana modificato da " + ls_tipo_mant_old + " a " + ls_tipo_mant_new
	elseif isnull(ls_tipo_mant_new) and not isnull(ls_tipo_mant_old) then
		ls_mod_riga += "~n~t~t~tTipo mantovana azzerato"
	elseif not isnull(ls_tipo_mant_new) and isnull(ls_tipo_mant_old) then
		ls_mod_riga += "~n~t~t~tTipo mantovana impostato a " + ls_tipo_mant_new
	end if
	
	if ld_quan_ord_new <> ld_quan_ord_old then
		ls_mod_riga += "~n~t~t~tQuantità ordine modificata da " + string(ld_quan_ord_old) + " a " + string(ld_quan_ord_new)
	elseif isnull(ld_quan_ord_new) and not isnull(ld_quan_ord_old) then
		ls_mod_riga += "~n~t~t~tQuantità ordine azzerata"
	elseif not isnull(ld_quan_ord_new) and isnull(ld_quan_ord_old) then
		ls_mod_riga += "~n~t~t~tQuantità ordine impostata a " + string(ld_quan_ord_new)
	end if
	
	if ld_quan_um_new <> ld_quan_um_old then
		ls_mod_riga += "~n~t~t~tQuantità UM modificata da " + string(ld_quan_um_old) + " a " + string(ld_quan_um_new)
	elseif isnull(ld_quan_um_new) and not isnull(ld_quan_um_old) then
		ls_mod_riga += "~n~t~t~tQuantità UM azzerata"
	elseif not isnull(ld_quan_um_new) and isnull(ld_quan_um_old) then
		ls_mod_riga += "~n~t~t~tQuantità UM impostata a " + string(ld_quan_um_new)
	end if
	
	if ld_dim_x_new <> ld_dim_x_old then
		ls_mod_riga += "~n~t~t~tDimensioni X modificate da " + string(ld_dim_x_old) + " a " + string(ld_dim_x_new)
	elseif isnull(ld_dim_x_new) and not isnull(ld_dim_x_old) then
		ls_mod_riga += "~n~t~t~tDimensioni X azzerate"
	elseif not isnull(ld_dim_x_new) and isnull(ld_dim_x_old) then
		ls_mod_riga += "~n~t~t~tDimensioni X impostate a " + string(ld_dim_x_new)
	end if
	
	if ld_dim_y_new <> ld_dim_y_old then
		ls_mod_riga += "~n~t~t~tDimensioni Y modificate da " + string(ld_dim_y_old) + " a " + string(ld_dim_y_new)
	elseif isnull(ld_dim_y_new) and not isnull(ld_dim_y_old) then
		ls_mod_riga += "~n~t~t~tDimensioni Y azzerate"
	elseif not isnull(ld_dim_y_new) and isnull(ld_dim_y_old) then
		ls_mod_riga += "~n~t~t~tDimensioni Y impostate a " + string(ld_dim_y_new)
	end if
	
	if ld_dim_z_new <> ld_dim_z_old then
		ls_mod_riga += "~n~t~t~tDimensioni Z modificate da " + string(ld_dim_z_old) + " a " + string(ld_dim_z_new)
	elseif isnull(ld_dim_z_new) and not isnull(ld_dim_z_old) then
		ls_mod_riga += "~n~t~t~tDimensioni Z azzerate"
	elseif not isnull(ld_dim_z_new) and isnull(ld_dim_z_old) then
		ls_mod_riga += "~n~t~t~tDimensioni Z impostate a " + string(ld_dim_z_new)
	end if
	
	if ld_alt_asta_new <> ld_alt_asta_old then
		ls_mod_riga += "~n~t~t~tAltezza asta modificata da " + string(ld_alt_asta_old) + " a " + string(ld_alt_asta_new)
	elseif isnull(ld_alt_asta_new) and not isnull(ld_alt_asta_old) then
		ls_mod_riga += "~n~t~t~tAltezza asta azzerata"
	elseif not isnull(ld_alt_asta_new) and isnull(ld_alt_asta_old) then
		ls_mod_riga += "~n~t~t~tAltezza asta impostata a " + string(ld_alt_asta_new)
	end if
	
	if ld_alt_mant_new <> ld_alt_mant_old then
		ls_mod_riga += "~n~t~t~tAltezza mantovana modificata da " + string(ld_alt_mant_old) + " a " + string(ld_alt_mant_new)
	elseif isnull(ld_alt_mant_new) and not isnull(ld_alt_mant_old) then
		ls_mod_riga += "~n~t~t~tAltezza mantovana azzerata"
	elseif not isnull(ld_alt_mant_new) and isnull(ld_alt_mant_old) then
		ls_mod_riga += "~n~t~t~tAltezza mantovana impostata a " + string(ld_alt_mant_new)
	end if
	
	if len(ls_mod_riga) > 0 then
		ls_mod_det += "~n~t~tRiga " + string(ll_riga) + "~n" + ls_mod_riga
	end if
	
loop

if len(ls_mod_det) > 0 then
	ls_mod_det = "~n~n~tRighe ordine~n" + ls_mod_det
end if

//Aggiornamento stringa del resoconto
if len(ls_mod_tes) > 0 or len(ls_mod_det) > 0 then
	ls_resoconto += "~n~nModifiche effettuate dal ricevente:" + ls_mod_tes + ls_mod_det
end if

ls_resoconto += "~n~n------------------------------------------------------------------~n~n"

as_resoconto = ls_resoconto

return 0
end function

public function integer uof_invia_mail (ref string as_messaggio);/*
	Funzione che invia i messaggi email ai destinatari impostati durante l'esportazione degli ordini
	Valori di ritorno:
		 0, invio messaggi completato con successo
		-1, errore della procedura (specificato in AS_MESSAGGIO)
*/

long 	 ll_i, ll_j

string ls_destinatari[], ls_tipo_invio, ls_oggetto, ls_corpo, ls_allegati[]

uo_outlook luo_outlook


//Si scorrono i destinatari individuati durante l'esportazione
for ll_i = 1 to upperbound(istr_mail)
	
	//Per ogni destinatario si legge l'email, se l'indirizzo è vuoto si disattiva l'invio automatico
	if isnull(istr_mail[ll_i].email) or len(trim(istr_mail[ll_i].email)) = 0 then
		as_messaggio = "Destinatario non impostato. Invio messaggio interrotto."
		return -1
	end if
	
	ls_destinatari[1] = istr_mail[ll_i].email
	
	ls_tipo_invio = "A"
	
	ls_oggetto = istr_mail[ll_i].oggetto
	
	ls_corpo = ""
	
	//Si scorrono i messaggi associati al destinatario corrente (X es. l'elenco degli ordini inviati)
	for ll_j = 1 to upperbound(istr_mail[ll_i].messaggi)
		
		//Se è presente un testo del messaggio, viene aggiunto al corpo dell'email
		if not isnull(istr_mail[ll_i].messaggi[ll_j].testo) then
			ls_corpo += "~r~n" + istr_mail[ll_i].messaggi[ll_j].testo
		end if
		
		//Se è presente un file, viene allegato all'email
		if not isnull(istr_mail[ll_i].messaggi[ll_j].allegato) then
			ls_allegati[upperbound(ls_allegati) + 1] = istr_mail[ll_i].messaggi[ll_j].allegato
		end if
		
	next
	
	//Si richiama l'oggetto UO_OUTLOOK con la funzione per l'invio di un messaggio email
	luo_outlook = create uo_outlook
	
	if luo_outlook.uof_outlook(0,ls_tipo_invio,ls_oggetto,ls_corpo,ls_destinatari,ls_allegati,false,as_messaggio) <> 0 then
		destroy luo_outlook
		return -1
	end if
	
	destroy luo_outlook
	
	//Si riscorrono i messaggi del destinatario corrente per eliminare i file temporanei degli allegati
	for ll_j = 1 to upperbound(istr_mail[ll_i].messaggi)
		
		//Se è presente un file, viene allegato all'email
		if not isnull(istr_mail[ll_i].messaggi[ll_j].allegato) then
			if not filedelete(istr_mail[ll_i].messaggi[ll_j].allegato) then
				as_messaggio = "Errore in cancellazione file ASCII"
				return -1
			end if
		end if
		
	next
	
next

return 0
end function

public function integer uof_get_rep_det_ord_ven (long al_anno_ord_ven, long al_num_ord_ven, long al_riga_ord_ven, ref string as_tipo_riga, ref string as_tipo_trasmissione, ref string as_cod_reparto, ref string as_email, ref string as_messaggio);/*
	Funzione che data una riga d'ordine di vendita legge l'eventuale reparto esterno associato
	Valori di ritorno:
		 0, lettura eseguita con successo (il reparto esterno è indicato in AS_COD_REPARTO)
     100, la riga non è collegata ad alcun reparto esterno
		-1, errore della procedura (specificato in AS_MESSAGGIO)
*/

long 	 ll_num_riga_appartenenza, ll_return, ll_i

string ls_cod_prodotto, ls_cod_versione, ls_tipo_reparto, ls_tipo_trasmissione, ls_reparti[], ls_email[]

uo_funzioni_1 luo_funzioni

//Si verifica se il dettaglio ha una riga di appartenenza
select
	cod_prodotto,
	cod_versione,
	num_riga_appartenenza
into
	:ls_cod_prodotto,
	:ls_cod_versione,
	:ll_num_riga_appartenenza
from
	det_ord_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :al_anno_ord_ven and
	num_registrazione = :al_num_ord_ven and
	prog_riga_ord_ven = :al_riga_ord_ven;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura dati riga:~n" + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_messaggio = "Errore in lettura dati riga:~nRiga indicata non trovata"
	return -1
end if

//Verifica che la riga abbia impostato il codice prodotto
if isnull(ls_cod_prodotto) then
	as_messaggio = "La riga indicata non ha il codice prodotto"
	return -1
end if

//Se esiste la riga di appartenenza allora vale il reparto della riga padre
if not isnull(ll_num_riga_appartenenza) and ll_num_riga_appartenenza > 0 then
	
	ll_return = uof_get_rep_det_ord_ven(al_anno_ord_ven,al_num_ord_ven,ll_num_riga_appartenenza,as_tipo_riga,as_tipo_trasmissione,as_cod_reparto,as_email,as_messaggio)
	
	if ll_return < 0 then
		as_messaggio = "Errore in verifica reparto riga padre:~n" + as_messaggio
		return -1
	elseif ll_return = 0 and not isnull(as_cod_reparto) then
		return 0
	else
		setnull(as_cod_reparto)
		setnull(as_email)
		return 100
	end if
	
end if

//Si richiama la funzione che verifica il tipo della riga (sfuso / tecniche / tende da sole)
ll_return = uof_tipo_det_ord_ven(al_anno_ord_ven,al_num_ord_ven,al_riga_ord_ven,as_messaggio)

//Si imposta la variabile per restituire assieme al reparto esterno il tipo di riga
choose case ll_return
		
	case is < 0
		
		as_messaggio = "Errore in verifica tipo riga:~n" + as_messaggio
		
		setnull(as_tipo_riga)
		
		return -1
		
	case 0
		
		//Se non è stato possibile definire il tipo riga non si può determinare neanche il reparto
		as_messaggio = "Tipo riga sconosciuto"
		
		setnull(as_tipo_riga)
		
		return -1
		
	case 1
		
		as_tipo_riga = "D"
		
	case 2
		
		as_tipo_riga = "C"
		
	case 3
		
		as_tipo_riga = "A"
		
end choose

//Se si verifica un errore o il tipo riga non è riconosciuto, si interrompe la funzione
//Altrimenti in base al tipo riga viene letto l'eventuale reparto esterno associato
choose case as_tipo_riga
		
	case "D"
		
		//RIGHE DI SFUSO
		
		//Lettura del reparto associato al prodotto in anagrafica
		select
			cod_reparto
		into
			:as_cod_reparto
		from
			anag_prodotti
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			as_messaggio = "Errore in lettura reparto associato in anagrafica:~n" + sqlca.sqlerrtext
			return -1
		elseif sqlca.sqlcode = 100 then
			as_messaggio = "Errore in lettura reparto associato in anagrafica:~Prodotto indicato non trovato"
			return -1
		end if
		
		//Se al prodotto non è associato un reparto la funzione termina
		if isnull(as_cod_reparto) then
			return 100
		end if
		
		//Gestione cambio reparto
		if uof_cambio_reparto(al_anno_ord_ven,al_num_ord_ven,as_cod_reparto,as_messaggio) <> 0 then
			return -1
		end if
		
		//Se il reparto del prodotto esiste bisogna verificare se si tratta di un reparto esterno
		if uof_dati_reparto(as_cod_reparto,ls_tipo_reparto,as_tipo_trasmissione,ls_email[],as_messaggio) <> 0 then
			return -1
		end if
		
		//Se il reparto non è esterno la funzione si comporta come se non avesse trovato nulla
		if ls_tipo_reparto = "E" then
			
			//Impostazione dell'indirizzo email da usare in base al tipo di trasmissione
			if as_tipo_trasmissione = "C" then
				as_email = ls_email[1] //email per notifica esportazione
			elseif as_tipo_trasmissione = "F" then
				as_email = ls_email[4] //email per invio file ASCII
			else
				setnull(as_email)
			end if
			
			return 0
			
		else
			
			setnull(as_cod_reparto)
			
			setnull(as_email)
			
			as_tipo_trasmissione = "N"
			
			return 100
			
		end if
		
	case "A","C"
		
		//RIGHE DI TENDE TECNICHE / DA SOLE
		
		//Verifica che la riga abbia impostato il codice versione
		if isnull(ls_cod_versione) then
			as_messaggio = "La riga indicata non ha il codice versione"
			return -1
		end if
		
		//Donato 01/02/2012 Spostata funzione globale in user object oggetto
		luo_funzioni = create uo_funzioni_1
		if luo_funzioni.uof_trova_reparti(	true, al_anno_ord_ven, al_num_ord_ven, al_riga_ord_ven, &
													ls_cod_prodotto, ls_cod_versione, ls_reparti, as_messaggio) <> 0 then
			as_messaggio = "Errore in lettura reparti dalla distinta base:~n" + as_messaggio
			return -1
		end if
		destroy luo_funzioni
		
//		if f_trova_reparti(al_anno_ord_ven,al_num_ord_ven,al_riga_ord_ven,ls_cod_prodotto,ls_cod_versione,ls_reparti,as_messaggio) <> 0 then
//			as_messaggio = "Errore in lettura reparti dalla distinta base:~n" + as_messaggio
//			return -1
//		end if
		//fine modifica ------------------------
		
		setnull(as_cod_reparto)
		
		//Per ogni reparto trovato si verifica se si tratta di un reparto esterno
		for ll_i = 1 to upperbound(ls_reparti)
			
			//Gestione cambio reparto
			if uof_cambio_reparto(al_anno_ord_ven,al_num_ord_ven,ls_reparti[ll_i],as_messaggio) <> 0 then
				return -1
			end if
			
			if uof_dati_reparto(ls_reparti[ll_i],ls_tipo_reparto,ls_tipo_trasmissione,ls_email[],as_messaggio) <> 0 then
				return -1
			end if
			
			//Ogni prodotto può essere collegato ad un solo reparto esterno
			if ls_tipo_reparto = "E" then
				
				if not isnull(as_cod_reparto) and ls_reparti[ll_i] <> as_cod_reparto then
					as_messaggio = "Esistono più reparti esterni nella distinta base del prodotto"
					setnull(as_cod_reparto)
					setnull(as_email)
					as_tipo_trasmissione = "N"
					return -1
				end if
				
				as_cod_reparto = ls_reparti[ll_i]
				
				//Impostazione dell'indirizzo email da usare in base al tipo di trasmissione
				if ls_tipo_trasmissione = "C" then
					as_email = ls_email[1] //email per notifica esportazione
					as_tipo_trasmissione = ls_tipo_trasmissione
				elseif ls_tipo_trasmissione = "F" then
					as_email = ls_email[4] //email per invio file ASCII
					as_tipo_trasmissione = ls_tipo_trasmissione
				else
					setnull(as_email)
					as_tipo_trasmissione = "N"
				end if
				
			end if
			
		next
		
		//Si verifica se almeno uno dei reparti era un reparto esterno
		if isnull(as_cod_reparto) then
			setnull(as_email)
			as_tipo_trasmissione = "N"
			return 100
		else
			return 0
		end if
		
	case else
		
		//In teoria non si dovrebbe mai avere questo caso, per sicurezza si esce come se non esistesse il reparto
		setnull(as_cod_reparto)
		setnull(as_email)
		as_tipo_trasmissione = "N"
		return 100
		
end choose
end function

public function integer uof_riassunto_giornaliero (datetime adt_data, string as_cod_reparto, boolean ab_null, ref string as_messaggio);/*
	Funzione che genera un messaggio email contenente l'elenco degli ordini acettati/respinti/inviati
	da e verso un certo reparto esterno in una certa data.
	Valori di ritorno:
		 0, riassunto giornaliero generato ed inviato correttamente
	  	-1, errore della procedura (specificato in AS_MESSAGGIO)
*/

long	 	ll_anno, ll_num

datetime ldt_inizio, ldt_fine

string 	ls_riassunto, ls_stato_ordine, ls_tipo_reparto, ls_tipo_trasmissione, ls_email[], &
			ls_oggetto, ls_destinatari[], ls_allegati[]

uo_outlook luo_outlook


//Lettura dati reparto
if uof_dati_reparto(as_cod_reparto,ls_tipo_reparto,ls_tipo_trasmissione,ls_email,as_messaggio) <> 0 then
	return -1
end if

//Il reparto destinatario del riassunto deve essere un reparto esterno
if ls_tipo_reparto <> "E" then
	as_messaggio = "Il reparto indicato non è un reparto esterno"
	return -1
end if

ldt_inizio = datetime(date(adt_data),00:00:00.000000)

ldt_fine = datetime(date(adt_data),23:59:59.999999)

ls_riassunto = ""

//Lettura di tutti gli oridni ricevuti dal reparto indicato e confermati nella data indicata
declare ricevuti cursor for
select
	anno_registrazione,
	num_registrazione
from
	buffer_ord_ven
where
	cod_reparto = :as_cod_reparto and
	flag_originale = 'N' and
	((flag_importato = 'S' and
	data_ora_import >= :ldt_inizio and
	data_ora_import <= :ldt_fine) or
	(flag_importato = 'N' and
	flag_esito_import = 'S'))
order by
	flag_importato DESC,
	flag_esito_import ASC,
	anno_registrazione ASC,
	num_registrazione ASC;

open ricevuti;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in open cursore ricevuti: " + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch
		ricevuti
	into
		:ll_anno,
		:ll_num;
	
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in fetch cursore ricevuti: " + sqlca.sqlerrtext
		close ricevuti;
		return -1
	elseif sqlca.sqlcode = 100 then
		close ricevuti;
		exit
	end if
	
	//Lettura resoconto stato ordine
	if uof_stato_buffer_ord_ven(as_cod_reparto,ll_anno,ll_num,ls_stato_ordine,as_messaggio) <> 0 then
		as_messaggio = "Errore in verifica stato ordine " + string(ll_anno) + "/" + string(ll_num) + ":~n" + as_messaggio
		return -1
	end if
	
	//Aggiunta dello stato ordine al testo del riassunto
	if not isnull(ls_stato_ordine) then
		ls_riassunto += ls_stato_ordine
	end if
	
loop

//Lettura di tutti gli oridni spediti al reparto indicato nella data indicata
declare inviati cursor for
select
	anno_registrazione,
	num_registrazione
from
	tes_ord_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	flag_inviato = 'S' and
	data_ora_invio >= :ldt_inizio and
	data_ora_invio <= :ldt_fine and
	cod_reparto_invio = :as_cod_reparto
order by
	anno_registrazione ASC,
	num_registrazione ASC;

open inviati;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in open cursore inviati: " + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch
		inviati
	into
		:ll_anno,
		:ll_num;
	
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in fetch cursore inviati: " + sqlca.sqlerrtext
		close inviati;
		return -1
	elseif sqlca.sqlcode = 100 then
		close inviati;
		exit
	end if
	
	//Aggiunta al testo del riassunto del riferimento all'ordine
	ls_riassunto += "~n~nInviato ordine " + string(ll_anno) + "/" + string(ll_num)
	
loop

if isnull(ls_riassunto) or len(trim(ls_riassunto)) < 1 then
	if ab_null then
		ls_riassunto = "Nessun ordine inviato o ricevuto"
	else
		return 0
	end if
end if

//Impostazione dati messaggio email
ls_oggetto = "Riassunto giornaliero - " + string(date(adt_data),"dd/mm/yyyy")

ls_destinatari[1] = ls_email[3]

//Si richiama l'oggetto UO_OUTLOOK con la funzione per l'invio di un messaggio email
luo_outlook = create uo_outlook

if luo_outlook.uof_outlook(0,"A",ls_oggetto,ls_riassunto,ls_destinatari,ls_allegati,false,as_messaggio) <> 0 then
	destroy luo_outlook
	return -1
end if

destroy luo_outlook

return 0
end function

public function integer uof_cambio_reparto (long al_anno_ord_ven, long al_num_ord_ven, ref string as_cod_reparto, ref string as_messaggio);string ls_rep_sost


select
	cod_reparto_sostituente
into
	:ls_rep_sost
from
	tes_ord_ven_reparti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :al_anno_ord_ven and
	num_registrazione = :al_num_ord_ven and
	cod_reparto_sostituito = :as_cod_reparto;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in verifica cambio reparto: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	setnull(ls_rep_sost)
end if

if not isnull(ls_rep_sost) then
	as_cod_reparto = ls_rep_sost
end if

return 0
end function

public function integer uof_export_ord_ven (long al_anno_ord_ven, long al_num_ord_ven, boolean ab_esegui, ref string as_messaggio);/*
	Funzione che dato un ordine di vendita legge tutte le righe di dettaglio che hanno un codice prodotto.
	Per ogni riga verifica se è collegata ad un reparto esterno e quindi da esportare.
	Quandoi una riga è da esportare, allora se AB_ESEGUI è a TRUE esegue l'esportazione, altrimenti si limita
	a tenere aggiornato il totale di righe da esportare rilevate.
	Valori di ritorno:
		 0, non è stato rilevato alcun dettaglio da esportare
	  > 0, il valore restituito rappresenta il totale delle righe esportate/rilevate da esportare
		-1, errore della procedura (specificato in AS_MESSAGGIO)
*/

dec{6}	ld_fat_conversione_ven

boolean  lb_complementi, lb_trovato

datetime ldt_data_ord_cliente, ldt_data_consegna, ldt_data_ora_ar, ldt_now

long	 	ll_i, ll_riga_ord_ven, ll_return, ll_count, ll_num_riga_appartenenza, ll_originale, ll_file,&
			ll_dest, ll_mess, ll_num_exp

dec{4}	ld_quan_ordine, ld_quantita_um, ld_num_confezioni, ld_num_pezzi_confezione, ld_dim_x, ld_dim_y, &
			ld_dim_z, ld_dim_t, ld_alt_mantovana, ld_alt_asta, ld_num_gambe, ld_num_campate, &
			ld_num_fili_plisse, ld_num_boccole_plisse, ld_num_intervallo_punzoni_plisse, ld_num_pieghe_plisse, &
			ld_prof_inf_plisse, ld_prof_sup_plisse, ld_num_punzoni_plisse, ld_intervallo_punzoni_plisse

string 	ls_originale[2], ls_inviato, ls_respinto, ls_cod_reparto, ls_tipo_trasmissione, ls_rep_old, &
		 	ls_cod_tipo_ord_ven, ls_num_ord_cliente, ls_alias_cliente, ls_flag_lavorazioni_speciali, &
			ls_cod_tipo_det_ven, ls_cod_misura, ls_cod_prodotto, ls_des_prodotto, ls_nota_dettaglio, &
			ls_cod_prod_finito, ls_cod_modello, ls_cod_tessuto, ls_cod_non_a_magazzino, ls_cod_tessuto_mant, &
			ls_cod_mant_non_a_magazzino, ls_flag_tipo_mantovana, ls_flag_cordolo, ls_flag_passamaneria, &
			ls_cod_verniciatura, ls_cod_comando, ls_pos_comando, ls_cod_comando_tenda_verticale, &
			ls_cod_colore_lastra, ls_cod_tipo_lastra, ls_cod_tipo_supporto, ls_flag_cappotta_frontale, &
			ls_flag_rollo, ls_flag_kit_laterale, ls_flag_misura_luce_finita, ls_cod_tipo_stampo, &
			ls_cod_mat_sis_stampaggio, ls_rev_stampo, ls_note, ls_flag_autoblocco, &
			ls_colore_passamaneria, ls_colore_cordolo, ls_des_vernice_fc, ls_des_comando_1, ls_cod_comando_1, &
			ls_cod_comando_2, ls_des_comando_2, ls_flag_addizionale_comando_1, ls_flag_addizionale_comando_2, &
			ls_flag_addizionale_supporto, ls_flag_addizionale_tessuto, ls_flag_addizionale_vernic, &
			ls_flag_fc_comando_1, ls_flag_fc_comando_2, ls_flag_fc_supporto, ls_flag_fc_tessuto, &
			ls_flag_fc_vernic, ls_flag_allegati, ls_spes_lastra, ls_email, ls_file, ls_txt, ls_testo, ls_tipo_riga, &
			ls_cod_tipo_det_ven_ricevente, ls_cod_tipo_ord_ven_ricevente,ls_cod_prodotto_ricevente,ls_cod_misura_ven, &
			ls_cod_tessuto_cliente, ls_cod_colore_tessuto_cliente, ls_flag_tessuto_cliente, ls_flag_tessuto_mant_cliente, &
			ls_cod_cliente, ls_rag_soc_abbreviata, ls_descrizione_ar, ls_flag_tassativo, ls_nota_prodotto, ls_flag_nota_ddt

datastore lds_righe

uo_outlook luo_outlook


//Lettura dati ordine
select
	cod_cliente,
	flag_inviato,
	flag_respinto,
	cod_tipo_ord_ven,
	num_ord_cliente,
	data_ord_cliente,
	data_pronto,
	alias_cliente,
	flag_lavorazioni_speciali,
	flag_tassativo
into
	:ls_cod_cliente,
	:ls_inviato,
	:ls_respinto,
	:ls_cod_tipo_ord_ven,
	:ls_num_ord_cliente,
	:ldt_data_ord_cliente,
	:ldt_data_consegna,
	:ls_alias_cliente,
	:ls_flag_lavorazioni_speciali,
	:ls_flag_tassativo
from
	tes_ord_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :al_anno_ord_ven and
	num_registrazione = :al_num_ord_ven;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in verifica stato ordine:~n" + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_messaggio = "Errore in verifica stato ordine:~nOrdine indicato non trovato"
	return -1
end if

if ls_respinto = "S" then
	as_messaggio = "L'ordine indicato è stato respinto in seguito all'ultimo invio"
	return -1
end if

if ls_inviato = "S" then
	as_messaggio = "L'ordine indicato è già stato inviato"
	return -1
end if

select
	rag_soc_abbreviata
into
	:ls_rag_soc_abbreviata
from
	anag_clienti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura dati cliente:~n" + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	setnull(ls_rag_soc_abbreviata)
end if

//Creazione del datastore ed impostazione della transazione e del dataobject
lds_righe = create datastore

lds_righe.dataobject = "d_ds_export_ord_ven"

if lds_righe.settransobject(sqlca) = -1 then
	as_messaggio = "Errore in impostazione transazione"
	destroy lds_righe
	return -1
end if

//Lettura delle righe d'ordine tramite il datastore
if lds_righe.retrieve(s_cs_xx.cod_azienda,al_anno_ord_ven,al_num_ord_ven) = -1 then
	as_messaggio = "Errore in lettura dati"
	destroy lds_righe
	return -1
end if

ls_txt = ""

ll_num_exp = 0

setnull(ls_rep_old)

//Si scorrono le righe d'ordine e per ognuna si richiama la funzione che restituisce l'eventuale reparto esterno
for ll_i = 1 to lds_righe.rowcount()
	
	//Lettura dei dati della riga d'ordine e dei relativi complementi
	ll_riga_ord_ven = lds_righe.getitemnumber(ll_i,"prog_riga_ord_ven")
	
	ls_cod_tipo_det_ven = lds_righe.getitemstring(ll_i,"cod_tipo_det_ven")
	
	ls_cod_misura = lds_righe.getitemstring(ll_i,"cod_misura")
	
	ls_cod_prodotto = lds_righe.getitemstring(ll_i,"cod_prodotto")
	
	ls_des_prodotto = lds_righe.getitemstring(ll_i,"des_prodotto")
	
	ls_nota_dettaglio = lds_righe.getitemstring(ll_i,"nota_dettaglio")
	
	ld_quan_ordine = lds_righe.getitemnumber(ll_i,"quan_ordine")
	
	ld_quantita_um = lds_righe.getitemnumber(ll_i,"quantita_um")
	
	ld_fat_conversione_ven = lds_righe.getitemnumber(ll_i,"fat_conversione_ven")
	
	ld_num_confezioni = lds_righe.getitemnumber(ll_i,"num_confezioni")
	
	ld_num_pezzi_confezione = lds_righe.getitemnumber(ll_i,"num_pezzi_confezione")
	
	ll_num_riga_appartenenza = lds_righe.getitemnumber(ll_i,"num_riga_appartenenza")
	
	ls_nota_prodotto = lds_righe.getitemstring(ll_i,"nota_prodotto")
	
	ls_flag_nota_ddt = lds_righe.getitemstring(ll_i,"flag_trasporta_nota_ddt")
	
	//Viene richiamata la funzione che restituisce l'eventuale reparto esterno collegato alla riga
	ll_return = uof_get_rep_det_ord_ven(al_anno_ord_ven,al_num_ord_ven,ll_riga_ord_ven,ls_tipo_riga,ls_tipo_trasmissione,ls_cod_reparto,ls_email,as_messaggio)
	
	//Se si verifica un errore nella lettura del reparto la funzione si interrompe
	//Altrimenti se la funzione non ha rilevato alcun reparto esterno si può passare alla riga successiva
	if ll_return < 0 then
		as_messaggio = "Errore in verifica reparto riga " + string(ll_riga_ord_ven) + ":~n" + as_messaggio
		destroy lds_righe
		return -1
	elseif ll_return = 100 or isnull(ls_cod_reparto) then
		continue
	end if
	
	//Si annota che è stata rilevata una riga da esportare
	ll_num_exp ++
	
	//La funzione è stata richiamata in modalità di verifica, pertanto non si deve eseguire l'esportazione
	if not ab_esegui then
		continue
	end if
	
	//Si verifica che il reparto esterno di questa riga non sia diverso da quelli già trovati nell'ordine
	if not isnull(ls_rep_old) and ls_cod_reparto <> ls_rep_old then
		as_messaggio = "L'ordine contiene al suo interno riferimenti a più reparti esterni"
		destroy lds_righe
		return -1
	end if
	
	//Si annota il reparto esterno trovato nell'ordine
	ls_rep_old = ls_cod_reparto
	
	//Se il tipo trasmissione è N (Nessuna trasmissione) si salta la riga
	if ls_tipo_trasmissione = "N" then
		continue
	end if
	
	//Verifica della presenza di eventuali complementi della riga di dettaglio
	select
		cod_prod_finito,
		cod_modello,
		dim_x,
		dim_y,
		dim_z,
		dim_t,
		cod_tessuto,
		cod_non_a_magazzino,
		cod_tessuto_mant,
		cod_mant_non_a_magazzino,
		flag_tipo_mantovana,
		alt_mantovana,
		flag_cordolo,
		flag_passamaneria,
		cod_verniciatura,
		cod_comando,
		pos_comando,
		alt_asta,
		cod_comando_tenda_verticale,
		cod_colore_lastra,
		cod_tipo_lastra,
		spes_lastra,
		cod_tipo_supporto,
		flag_cappotta_frontale,
		flag_rollo,
		flag_kit_laterale,
		flag_misura_luce_finita,
		cod_tipo_stampo,
		cod_mat_sis_stampaggio,
		rev_stampo,
		num_gambe,
		num_campate,
		note,
		flag_autoblocco,
		num_fili_plisse,
		num_boccole_plisse,
		intervallo_punzoni_plisse,
		num_pieghe_plisse,
		prof_inf_plisse,
		prof_sup_plisse,
		colore_passamaneria,
		colore_cordolo,
		num_punzoni_plisse,
		des_vernice_fc,
		des_comando_1,
		cod_comando_2,
		des_comando_2,
		flag_addizionale_comando_1,
		flag_addizionale_comando_2,
		flag_addizionale_supporto,
		flag_addizionale_tessuto,
		flag_addizionale_vernic,
		flag_fc_comando_1,
		flag_fc_comando_2,
		flag_fc_supporto,
		flag_fc_tessuto,
		flag_fc_vernic,
		flag_allegati,
		data_ora_ar,
		flag_tessuto_cliente,
		flag_tessuto_mant_cliente
	into
		:ls_cod_prod_finito,
		:ls_cod_modello,
		:ld_dim_x,
		:ld_dim_y,
		:ld_dim_z,
		:ld_dim_t,
		:ls_cod_tessuto,
		:ls_cod_non_a_magazzino,
		:ls_cod_tessuto_mant,
		:ls_cod_mant_non_a_magazzino,
		:ls_flag_tipo_mantovana,
		:ld_alt_mantovana,
		:ls_flag_cordolo,
		:ls_flag_passamaneria,
		:ls_cod_verniciatura,
		:ls_cod_comando,
		:ls_pos_comando,
		:ld_alt_asta,
		:ls_cod_comando_tenda_verticale,
		:ls_cod_colore_lastra,
		:ls_cod_tipo_lastra,
		:ls_spes_lastra,
		:ls_cod_tipo_supporto,
		:ls_flag_cappotta_frontale,
		:ls_flag_rollo,
		:ls_flag_kit_laterale,
		:ls_flag_misura_luce_finita,
		:ls_cod_tipo_stampo,
		:ls_cod_mat_sis_stampaggio,
		:ls_rev_stampo,
		:ld_num_gambe,
		:ld_num_campate,
		:ls_note,
		:ls_flag_autoblocco,
		:ld_num_fili_plisse,
		:ld_num_boccole_plisse,
		:ld_intervallo_punzoni_plisse,
		:ld_num_pieghe_plisse,
		:ld_prof_inf_plisse,
		:ld_prof_sup_plisse,
		:ls_colore_passamaneria,
		:ls_colore_cordolo,
		:ld_num_punzoni_plisse,
		:ls_des_vernice_fc,
		:ls_des_comando_1,
		:ls_cod_comando_2,
		:ls_des_comando_2,
		:ls_flag_addizionale_comando_1,
		:ls_flag_addizionale_comando_2,
		:ls_flag_addizionale_supporto,
		:ls_flag_addizionale_tessuto,
		:ls_flag_addizionale_vernic,
		:ls_flag_fc_comando_1,
		:ls_flag_fc_comando_2,
		:ls_flag_fc_supporto,
		:ls_flag_fc_tessuto,
		:ls_flag_fc_vernic,
		:ls_flag_allegati,
		:ldt_data_ora_ar,
		:ls_flag_tessuto_cliente,
		:ls_flag_tessuto_mant_cliente
	from
		comp_det_ord_ven
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :al_anno_ord_ven and
		num_registrazione = :al_num_ord_ven and
		prog_riga_ord_ven = :ll_riga_ord_ven;
	
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in controllo esistenza complementi riga ordine: " + sqlca.sqlerrtext
		return -1
	elseif sqlca.sqlcode = 100 then
		lb_complementi = false
		setnull(ls_cod_prod_finito)
		setnull(ls_cod_modello)
		setnull(ld_dim_x)
		setnull(ld_dim_y)
		setnull(ld_dim_z)
		setnull(ld_dim_t)
		setnull(ls_cod_tessuto)
		setnull(ls_cod_non_a_magazzino)
		setnull(ls_cod_tessuto_mant)
		setnull(ls_cod_mant_non_a_magazzino)
		setnull(ls_flag_tipo_mantovana)
		setnull(ld_alt_mantovana)
		setnull(ls_flag_cordolo)
		setnull(ls_flag_passamaneria)
		setnull(ls_cod_verniciatura)
		setnull(ls_cod_comando)
		setnull(ls_pos_comando)
		setnull(ld_alt_asta)
		setnull(ls_cod_comando_tenda_verticale)
		setnull(ls_cod_colore_lastra)
		setnull(ls_cod_tipo_lastra)
		setnull(ls_spes_lastra)
		setnull(ls_cod_tipo_supporto)
		setnull(ls_flag_cappotta_frontale)
		setnull(ls_flag_rollo)
		setnull(ls_flag_kit_laterale)
		setnull(ls_flag_misura_luce_finita)
		setnull(ls_cod_tipo_stampo)
		setnull(ls_cod_mat_sis_stampaggio)
		setnull(ls_rev_stampo)
		setnull(ld_num_gambe)
		setnull(ld_num_campate)
		setnull(ls_note)
		setnull(ls_flag_autoblocco)
		setnull(ld_num_fili_plisse)
		setnull(ld_num_boccole_plisse)
		setnull(ld_intervallo_punzoni_plisse)
		setnull(ld_num_pieghe_plisse)
		setnull(ld_prof_inf_plisse)
		setnull(ld_prof_sup_plisse)
		setnull(ls_colore_passamaneria)
		setnull(ls_colore_cordolo)
		setnull(ld_num_punzoni_plisse)
		setnull(ls_des_vernice_fc)
		setnull(ls_des_comando_1)
		setnull(ls_cod_comando_2)
		setnull(ls_des_comando_2)
		setnull(ls_flag_addizionale_comando_1)
		setnull(ls_flag_addizionale_comando_2)
		setnull(ls_flag_addizionale_supporto)
		setnull(ls_flag_addizionale_tessuto)
		setnull(ls_flag_addizionale_vernic)
		setnull(ls_flag_fc_comando_1)
		setnull(ls_flag_fc_comando_2)
		setnull(ls_flag_fc_supporto)
		setnull(ls_flag_fc_tessuto)
		setnull(ls_flag_fc_vernic)
		setnull(ls_flag_allegati)
		setnull(ldt_data_ora_ar)
		setnull(ls_flag_tessuto_cliente)
		setnull(ls_flag_tessuto_mant_cliente)
	else
		lb_complementi = true
	end if
	
	//Si effettua l'esportazione in base al tipo di trasmissione indicato nel reparto collegato alla riga:
	//C corrisponde alla connessione diretta e quindi si esporta l'ordine nel database del reparto esterno
	//F corrisponde all'invio di un file ASCII e quindi si crea una stringa di testo con i dati dell'ordine
	choose case ls_tipo_trasmissione
			
		case "C"
			
			//Si imposta il testo del messaggio email
			ls_testo = "Si avvisa che è stato inviato l'ordine " + string(al_anno_ord_ven) + "/" + string(al_num_ord_ven)
			
			if ls_flag_lavorazioni_speciali = "S" then
				ls_testo += " ***(LS)***"
			end if
			
			//Si azzera il nome del file da allegare al messaggio email
			setnull(ls_file)
			
			ls_originale[1] = "S"
			ls_originale[2] = "N"
			
			//Connessione al DB del reparto esterno
			if uof_connetti_db(ls_cod_reparto,as_messaggio) <> 0 then
				as_messaggio = "Errore in connessione al DB del reparto " + ls_cod_reparto + ":~n" + as_messaggio
				return -1
			end if
			
			//FUNZIONE DI CAMBIO TESSUTO
			if ls_flag_tessuto_cliente = "S" and not isnull(ls_flag_tessuto_cliente) then
				select cod_tessuto_cliente,   
						 cod_colore_tessuto_cliente  
				into  :ls_cod_tessuto_cliente,   
						:ls_cod_colore_tessuto_cliente  
				from  tab_tessuti  
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_tessuto = :ls_cod_tessuto and
						cod_non_a_magazzino = :ls_cod_non_a_magazzino
				using istr_tran[il_tran].tran;
				if sqlca.sqlcode = 0 then
					ls_cod_tessuto = ls_cod_tessuto_cliente
					ls_cod_non_a_magazzino = ls_cod_colore_tessuto_cliente 
				end if
			end if
			
			//FUNZIONE DI CAMBIO TESSUTO (mantovana)
			if ls_flag_tessuto_mant_cliente = "S" and not isnull(ls_flag_tessuto_mant_cliente) then
				select cod_tessuto_cliente,   
						 cod_colore_tessuto_cliente  
				into  :ls_cod_tessuto_cliente,   
						:ls_cod_colore_tessuto_cliente  
				from  tab_tessuti  
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_tessuto = :ls_cod_tessuto_mant and
						cod_non_a_magazzino = :ls_cod_mant_non_a_magazzino
				using istr_tran[il_tran].tran;
				if sqlca.sqlcode = 0 then
					ls_cod_tessuto_mant = ls_cod_tessuto_cliente
					ls_cod_mant_non_a_magazzino = ls_cod_colore_tessuto_cliente 
				end if
			end if
			

			//Trasformazione prodotti in base alle specifiche A.R. del cliente. /ENRICO
			select cod_prodotto_ricevente
			into   :ls_cod_prodotto_ricevente
			from   tab_trasformazione_ar
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_reparto = :ls_cod_reparto and
					 cod_prodotto_trasmittente = :ls_cod_prodotto;
			if sqlca.sqlcode = 0 then		// se lo trovo e non ci sono errori
				uo_ricalcola_configurazione_ar luo_ar
				luo_ar = CREATE uo_ricalcola_configurazione_ar
				
				luo_ar.i_tran_ricevente = istr_tran[il_tran].tran
				luo_ar.i_tran_trasmittente = sqlca
				
				luo_ar.is_cod_prodotto_origine = ls_cod_prodotto
				luo_ar.is_cod_modello_finale = ls_cod_prodotto_ricevente
				
				luo_ar.istr_riepilogo.dimensione_1 = ld_dim_x
				luo_ar.istr_riepilogo.dimensione_2 = ld_dim_y
				luo_ar.istr_riepilogo.dimensione_3 = ld_dim_z
				luo_ar.istr_riepilogo.dimensione_4 = ld_dim_t
				luo_ar.istr_riepilogo.cod_tessuto = ls_cod_tessuto
				luo_ar.istr_riepilogo.cod_non_a_magazzino = ls_cod_non_a_magazzino
				luo_ar.istr_riepilogo.cod_tessuto_mant = ls_cod_tessuto_mant
				luo_ar.istr_riepilogo.cod_mant_non_a_magazzino = ls_cod_mant_non_a_magazzino
				luo_ar.istr_riepilogo.flag_tipo_mantovana = ls_flag_tipo_mantovana
				luo_ar.istr_riepilogo.alt_mantovana = ld_alt_mantovana
				luo_ar.istr_riepilogo.flag_cordolo = ls_flag_cordolo
				luo_ar.istr_riepilogo.flag_passamaneria = ls_flag_passamaneria
				luo_ar.istr_riepilogo.cod_verniciatura = ls_cod_verniciatura
				luo_ar.istr_riepilogo.cod_comando = ls_cod_comando
				luo_ar.istr_riepilogo.pos_comando = ls_pos_comando
				luo_ar.istr_riepilogo.alt_asta = ld_alt_asta
				luo_ar.istr_riepilogo.cod_colore_lastra = ls_cod_colore_lastra
				luo_ar.istr_riepilogo.cod_tipo_lastra = ls_cod_tipo_lastra
				luo_ar.istr_riepilogo.spes_lastra = ls_spes_lastra
				luo_ar.istr_riepilogo.cod_tipo_supporto = ls_cod_tipo_supporto
				luo_ar.istr_riepilogo.flag_cappotta_frontale = ls_flag_cappotta_frontale
				luo_ar.istr_riepilogo.flag_rollo = ls_flag_rollo
				luo_ar.istr_riepilogo.flag_kit_laterale = ls_flag_kit_laterale
				luo_ar.istr_riepilogo.flag_misura_luce_finita = ls_flag_misura_luce_finita
				luo_ar.istr_riepilogo.cod_tipo_stampo = ls_cod_tipo_stampo
				luo_ar.istr_riepilogo.cod_mat_sis_stampaggio = ls_cod_mat_sis_stampaggio
				luo_ar.istr_riepilogo.rev_stampo = ls_rev_stampo
				luo_ar.istr_riepilogo.num_gambe = ld_num_gambe
				luo_ar.istr_riepilogo.num_campate = ld_num_campate
				luo_ar.istr_riepilogo.note = ls_note
				luo_ar.istr_riepilogo.flag_autoblocco = ls_flag_autoblocco
				luo_ar.istr_riepilogo.num_fili_plisse = ld_num_fili_plisse
				luo_ar.istr_riepilogo.num_boccole_plisse = ld_num_boccole_plisse
				luo_ar.istr_riepilogo.intervallo_punzoni_plisse = ld_intervallo_punzoni_plisse
				luo_ar.istr_riepilogo.num_pieghe_plisse = ld_num_pieghe_plisse
				luo_ar.istr_riepilogo.prof_inf_plisse = ld_prof_inf_plisse
				luo_ar.istr_riepilogo.prof_sup_plisse = ld_prof_sup_plisse
				luo_ar.istr_riepilogo.colore_passamaneria = ls_colore_passamaneria
				luo_ar.istr_riepilogo.colore_cordolo = ls_colore_cordolo
				luo_ar.istr_riepilogo.num_punzoni_plisse = ld_num_punzoni_plisse
				luo_ar.istr_riepilogo.des_vernice_fc = ls_des_vernice_fc
				luo_ar.istr_riepilogo.des_comando_1 = ls_des_comando_1
				luo_ar.istr_riepilogo.cod_comando_2 = ls_cod_comando_2
				luo_ar.istr_riepilogo.des_comando_2 = ls_des_comando_2
				luo_ar.istr_riepilogo.flag_addizionale_comando_1 = ls_flag_addizionale_comando_1
				luo_ar.istr_riepilogo.flag_addizionale_comando_2 = ls_flag_addizionale_comando_2
				luo_ar.istr_riepilogo.flag_addizionale_supporto = ls_flag_addizionale_supporto
				luo_ar.istr_riepilogo.flag_addizionale_tessuto = ls_flag_addizionale_tessuto
				luo_ar.istr_riepilogo.flag_addizionale_vernic = ls_flag_addizionale_vernic
				luo_ar.istr_riepilogo.flag_fc_comando_1 = ls_flag_fc_comando_1
				luo_ar.istr_riepilogo.flag_fc_comando_2 = ls_flag_fc_comando_2
				luo_ar.istr_riepilogo.flag_fc_supporto = ls_flag_fc_supporto
				luo_ar.istr_riepilogo.flag_fc_tessuto = ls_flag_fc_tessuto
				luo_ar.istr_riepilogo.flag_fc_vernic = ls_flag_fc_vernic
				luo_ar.istr_riepilogo.flag_allegati = ls_flag_allegati
	
				if luo_ar.uof_trasforma_prodotto() < 0 then
					as_messaggio = luo_ar.is_messaggio_errore
					destroy luo_ar
					return -1
				end if 
				// rileggo i dati della configurazione dopo la trasformazione
				ls_cod_prodotto = luo_ar.is_cod_modello_finale
				ls_des_prodotto = luo_ar.is_des_prodotto
				ld_dim_x = luo_ar.istr_riepilogo.dimensione_1 
				ld_dim_y = luo_ar.istr_riepilogo.dimensione_2 
				ld_dim_z = luo_ar.istr_riepilogo.dimensione_3 
				ld_dim_t = luo_ar.istr_riepilogo.dimensione_4 
				ls_cod_tessuto = luo_ar.istr_riepilogo.cod_tessuto 
				ls_cod_non_a_magazzino = luo_ar.istr_riepilogo.cod_non_a_magazzino 
				ls_cod_tessuto_mant = luo_ar.istr_riepilogo.cod_tessuto_mant 
				ls_cod_mant_non_a_magazzino = luo_ar.istr_riepilogo.cod_mant_non_a_magazzino 
				ls_flag_tipo_mantovana = luo_ar.istr_riepilogo.flag_tipo_mantovana 
				ld_alt_mantovana = luo_ar.istr_riepilogo.alt_mantovana 
				ls_flag_cordolo = luo_ar.istr_riepilogo.flag_cordolo 
				ls_flag_passamaneria = luo_ar.istr_riepilogo.flag_passamaneria 
				ls_cod_verniciatura = luo_ar.istr_riepilogo.cod_verniciatura 
				ls_cod_comando = luo_ar.istr_riepilogo.cod_comando 
				ls_pos_comando = luo_ar.istr_riepilogo.pos_comando 
				ld_alt_asta = luo_ar.istr_riepilogo.alt_asta 
				ls_cod_colore_lastra = luo_ar.istr_riepilogo.cod_colore_lastra 
				ls_cod_tipo_lastra = luo_ar.istr_riepilogo.cod_tipo_lastra 
				ls_spes_lastra = luo_ar.istr_riepilogo.spes_lastra 
				ls_cod_tipo_supporto = luo_ar.istr_riepilogo.cod_tipo_supporto 
				ls_flag_cappotta_frontale = luo_ar.istr_riepilogo.flag_cappotta_frontale 
				ls_flag_rollo = luo_ar.istr_riepilogo.flag_rollo 
				ls_flag_kit_laterale = luo_ar.istr_riepilogo.flag_kit_laterale 
				ls_flag_misura_luce_finita = luo_ar.istr_riepilogo.flag_misura_luce_finita 
				ls_cod_tipo_stampo = luo_ar.istr_riepilogo.cod_tipo_stampo 
				ls_cod_mat_sis_stampaggio = luo_ar.istr_riepilogo.cod_mat_sis_stampaggio
				ls_rev_stampo = luo_ar.istr_riepilogo.rev_stampo 
				ld_num_gambe = luo_ar.istr_riepilogo.num_gambe 
				ld_num_campate = luo_ar.istr_riepilogo.num_campate 
				//ls_note = luo_ar.istr_riepilogo.note 
				ls_flag_autoblocco = luo_ar.istr_riepilogo.flag_autoblocco 
				ld_num_fili_plisse = luo_ar.istr_riepilogo.num_fili_plisse 
				ld_num_boccole_plisse = luo_ar.istr_riepilogo.num_boccole_plisse 
				ld_intervallo_punzoni_plisse = luo_ar.istr_riepilogo.intervallo_punzoni_plisse 
				ld_num_pieghe_plisse = luo_ar.istr_riepilogo.num_pieghe_plisse 
				ld_prof_inf_plisse = luo_ar.istr_riepilogo.prof_inf_plisse 
				ld_prof_sup_plisse = luo_ar.istr_riepilogo.prof_sup_plisse 
				ls_colore_passamaneria = luo_ar.istr_riepilogo.colore_passamaneria 
				ls_colore_cordolo = luo_ar.istr_riepilogo.colore_cordolo 
				ld_num_punzoni_plisse = luo_ar.istr_riepilogo.num_punzoni_plisse 
				ls_des_vernice_fc = luo_ar.istr_riepilogo.des_vernice_fc 
				ls_des_comando_1 = luo_ar.istr_riepilogo.des_comando_1 
				ls_cod_comando_2 = luo_ar.istr_riepilogo.cod_comando_2 
				ls_des_comando_2 = luo_ar.istr_riepilogo.des_comando_2 
				ls_flag_addizionale_comando_1 = luo_ar.istr_riepilogo.flag_addizionale_comando_1 
				ls_flag_addizionale_comando_2 = luo_ar.istr_riepilogo.flag_addizionale_comando_2 
				ls_flag_addizionale_supporto = luo_ar.istr_riepilogo.flag_addizionale_supporto 
				ls_flag_addizionale_tessuto = luo_ar.istr_riepilogo.flag_addizionale_tessuto 
				ls_flag_addizionale_vernic = luo_ar.istr_riepilogo.flag_addizionale_vernic 
				ls_flag_fc_comando_1 = luo_ar.istr_riepilogo.flag_fc_comando_1 
				ls_flag_fc_comando_2 = luo_ar.istr_riepilogo.flag_fc_comando_2 
				ls_flag_fc_supporto = luo_ar.istr_riepilogo.flag_fc_supporto 
				ls_flag_fc_tessuto = luo_ar.istr_riepilogo.flag_fc_tessuto 
				ls_flag_fc_vernic = luo_ar.istr_riepilogo.flag_fc_vernic 
				ls_flag_allegati = luo_ar.istr_riepilogo.flag_allegati 
				
				if uof_descrizione_ar(al_anno_ord_ven,al_num_ord_ven,ll_riga_ord_ven,ls_descrizione_ar,as_messaggio) <> 0 then
					return -1
				end if
				
				if not isnull(ls_descrizione_ar) then
					if isnull(ls_nota_dettaglio) then
						ls_nota_dettaglio = ls_descrizione_ar
					else
						ls_nota_dettaglio += "~n" + ls_descrizione_ar
					end if
				end if
				
				select cod_misura_ven
				into   :ls_cod_misura_ven
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_prodotto = :ls_cod_prodotto
				using  istr_tran[il_tran].tran;
				if istr_tran[il_tran].tran.sqlcode = 0 then
					ls_cod_misura = ls_cod_misura_ven
				end if
				
				// ---------------  ricalcolo del codice del prodotto finito autocomposto ---------------------
				
				if uof_init_cod_prodotto(ls_cod_prodotto, &
				                     istr_tran[il_tran].tran, &
											ld_dim_x,&
											ls_cod_tessuto,&
											ls_cod_non_a_magazzino,&
											ls_cod_comando,&
											ls_cod_verniciatura,&
											ref ls_cod_prod_finito,&
											ref as_messaggio) < 0 then
					if isnull(as_messaggio) then as_messaggio = ""
					as_messaggio = "Errore in fase di calcolo codice prodotto finito~r~n"+as_messaggio+":~n" + sqlca.sqlerrtext
					return -1
				end if
					
				
				// ----------------------- fine ricalcolo autocomposto -----------------------------------------
			end if
			
			//Sostituzione del tipo dettaglio e del tipo ordine di vendita /ENRICO
			select cod_tipo_det_ven_ricevente
			into   :ls_cod_tipo_det_ven_ricevente
			from   tab_tipi_det_ven_ricevente
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_tipo_det_ven = :ls_cod_tipo_det_ven and
					 cod_reparto = :ls_cod_reparto;
			if sqlca.sqlcode = 0 then
				ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_ricevente
			elseif sqlca.sqlcode < 0 then
				as_messaggio = "Errore in ricerca del tipo dettaglio vendite" + ls_cod_tipo_det_ven + " del reparto " + ls_cod_reparto + ":~n" + sqlca.sqlerrtext
				return -1
			end if
			
			select cod_tipo_ord_ven_ricevente
			into   :ls_cod_tipo_ord_ven_ricevente
			from   tab_tipi_ord_ven_ricevente
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_tipo_ord_ven = :ls_cod_tipo_ord_ven and
					 cod_reparto = :ls_cod_reparto;
			if sqlca.sqlcode = 0 then
				ls_cod_tipo_ord_ven = ls_cod_tipo_ord_ven_ricevente
			elseif sqlca.sqlcode < 0 then
				as_messaggio = "Errore in ricerca del tipo ordine di vendite" + ls_cod_tipo_det_ven + " del reparto " + ls_cod_reparto + ":~n" + sqlca.sqlerrtext
				return -1
			end if
			
			//Se è la prima riga da esportare allora si eliminano eventuali versioni precedenti dello stesso ordine
			if ll_num_exp = 1 then
				
				delete from
					buffer_ord_ven_det
				where
					cod_reparto = :istr_tran[il_tran].cod_reparto_rx and
					anno_registrazione = :al_anno_ord_ven and
					num_registrazione = :al_num_ord_ven
				using
					istr_tran[il_tran].tran;
				
				if istr_tran[il_tran].tran.sqlcode <> 0 then
					as_messaggio = "Errore in cancellazione versioni precedenti:~n" + istr_tran[il_tran].tran.sqlerrtext
					return -1
				end if
				
				delete from
					buffer_ord_ven
				where
					cod_reparto = :istr_tran[il_tran].cod_reparto_rx and
					anno_registrazione = :al_anno_ord_ven and
					num_registrazione = :al_num_ord_ven
				using
					istr_tran[il_tran].tran;
				
				if istr_tran[il_tran].tran.sqlcode <> 0 then
					as_messaggio = "Errore in cancellazione versioni precedenti:~n" + istr_tran[il_tran].tran.sqlerrtext
					return -1
				end if
				
			end if
			
			//Se la testata dell'ordine non è ancora stata esportata nella tabella di appoggio del
			//ricevente, allora è necessario esportarla prima di poter esportare la riga di dettaglio
			select
				count(*)
			into
				:ll_count
			from
				buffer_ord_ven
			where
				cod_reparto = :istr_tran[il_tran].cod_reparto_rx and
				anno_registrazione = :al_anno_ord_ven and
				num_registrazione = :al_num_ord_ven and
				flag_originale = 'S'
			using
				istr_tran[il_tran].tran;
			
			if istr_tran[il_tran].tran.sqlcode < 0 then
				as_messaggio = "Errore in controllo esistenza testata ordine:~n" + istr_tran[il_tran].tran.sqlerrtext
				return -1
			elseif istr_tran[il_tran].tran.sqlcode = 100 or isnull(ll_count) or ll_count < 1 then
				
				//Inserimento testata ordine
				for ll_originale = 1 to 2
				
					insert into
						buffer_ord_ven
						(cod_reparto,
						anno_registrazione,
						num_registrazione,
						flag_originale,
						cod_cliente,
						cod_tipo_ord_ven,
						num_ord_cliente,
						data_ord_cliente,
						data_consegna,
						rag_soc_abbreviata,
						alias_cliente,
						flag_lavorazioni_speciali,
						flag_esito_import,
						data_ora_import,
						flag_importato,
						flag_tipo_ordine,
						cod_utente_import,
						flag_tassativo)
					values
						(:istr_tran[il_tran].cod_reparto_rx,
						:al_anno_ord_ven,
						:al_num_ord_ven,
						:ls_originale[ll_originale],
						:istr_tran[il_tran].cod_cliente,
						:ls_cod_tipo_ord_ven,
						:ls_num_ord_cliente,
						:ldt_data_ord_cliente,
						:ldt_data_consegna,
						:ls_rag_soc_abbreviata,
						:ls_alias_cliente,
						:ls_flag_lavorazioni_speciali,
						'S',
						null,
						'N',
						:ls_tipo_riga,
						null,
						:ls_flag_tassativo)
					using
						istr_tran[il_tran].tran;
					
					if istr_tran[il_tran].tran.sqlcode <> 0 then
						as_messaggio = "Errore in esportazione testata ordine:~n" + istr_tran[il_tran].tran.sqlerrtext
						return -1
					end if
					
				next
				
			end if
			
			//Inserimento della riga di dettaglio e degli eventuali complementi
			for ll_originale = 1 to 2
			
				insert into
					buffer_ord_ven_det
					(cod_reparto,
					anno_registrazione,
					num_registrazione,
					flag_originale,
					prog_riga_ord_ven,
					cod_tipo_det_ven,
					cod_misura,
					cod_prodotto,
					des_prodotto,
					nota_dettaglio,
					quan_ordine,
					quantita_um,
					fat_conversione_ven,
					num_confezioni,
					num_pezzi_confezione,
					num_riga_appartenenza,
					data_consegna,
					flag_tipo_ordine,
					cod_prod_finito,
					cod_modello,
					dim_x,
					dim_y,
					dim_z,
					dim_t,
					cod_tessuto,
					cod_non_a_magazzino,
					cod_tessuto_mant,
					cod_mant_non_a_magazzino,
					flag_tipo_mantovana,
					alt_mantovana,
					flag_cordolo,
					flag_passamaneria,
					cod_verniciatura,
					cod_comando,
					pos_comando,
					alt_asta,
					cod_comando_tenda_verticale,
					cod_colore_lastra,
					cod_tipo_lastra,
					spes_lastra,
					cod_tipo_supporto,
					flag_cappotta_frontale,
					flag_rollo,
					flag_kit_laterale,
					flag_misura_luce_finita,
					cod_tipo_stampo,
					cod_mat_sis_stampaggio,
					rev_stampo,
					num_gambe,
					num_campate,
					note,
					flag_autoblocco,
					num_fili_plisse,
					num_boccole_plisse,
					intervallo_punzoni_plisse,
					num_pieghe_plisse,
					prof_inf_plisse,
					prof_sup_plisse,
					colore_passamaneria,
					colore_cordolo,
					num_punzoni_plisse,
					des_vernice_fc,
					des_comando_1,
					cod_comando_2,
					des_comando_2,
					flag_addizionale_comando_1,
					flag_addizionale_comando_2,
					flag_addizionale_supporto,
					flag_addizionale_tessuto,
					flag_addizionale_vernic,
					flag_fc_comando_1,
					flag_fc_comando_2,
					flag_fc_supporto,
					flag_fc_tessuto,
					flag_fc_vernic,
					flag_allegati,
					data_ora_ar,
					nota_prodotto,
					flag_trasporta_nota_ddt)
				values
					(:istr_tran[il_tran].cod_reparto_rx,
					:al_anno_ord_ven,
					:al_num_ord_ven,
					:ls_originale[ll_originale],
					:ll_riga_ord_ven,
					:ls_cod_tipo_det_ven,
					:ls_cod_misura,
					:ls_cod_prodotto,
					:ls_des_prodotto,
					:ls_nota_dettaglio,
					:ld_quan_ordine,
					:ld_quantita_um,
					:ld_fat_conversione_ven,
					:ld_num_confezioni,
					:ld_num_pezzi_confezione,
					:ll_num_riga_appartenenza,
					:ldt_data_consegna,
					:ls_tipo_riga,
					:ls_cod_prod_finito,
					:ls_cod_modello,
					:ld_dim_x,
					:ld_dim_y,
					:ld_dim_z,
					:ld_dim_t,
					:ls_cod_tessuto,
					:ls_cod_non_a_magazzino,
					:ls_cod_tessuto_mant,
					:ls_cod_mant_non_a_magazzino,
					:ls_flag_tipo_mantovana,
					:ld_alt_mantovana,
					:ls_flag_cordolo,
					:ls_flag_passamaneria,
					:ls_cod_verniciatura,
					:ls_cod_comando,
					:ls_pos_comando,
					:ld_alt_asta,
					:ls_cod_comando_tenda_verticale,
					:ls_cod_colore_lastra,
					:ls_cod_tipo_lastra,
					:ls_spes_lastra,
					:ls_cod_tipo_supporto,
					:ls_flag_cappotta_frontale,
					:ls_flag_rollo,
					:ls_flag_kit_laterale,
					:ls_flag_misura_luce_finita,
					:ls_cod_tipo_stampo,
					:ls_cod_mat_sis_stampaggio,
					:ls_rev_stampo,
					:ld_num_gambe,
					:ld_num_campate,
					:ls_note,
					:ls_flag_autoblocco,
					:ld_num_fili_plisse,
					:ld_num_boccole_plisse,
					:ld_intervallo_punzoni_plisse,
					:ld_num_pieghe_plisse,
					:ld_prof_inf_plisse,
					:ld_prof_sup_plisse,
					:ls_colore_passamaneria,
					:ls_colore_cordolo,
					:ld_num_punzoni_plisse,
					:ls_des_vernice_fc,
					:ls_des_comando_1,
					:ls_cod_comando_2,
					:ls_des_comando_2,
					:ls_flag_addizionale_comando_1,
					:ls_flag_addizionale_comando_2,
					:ls_flag_addizionale_supporto,
					:ls_flag_addizionale_tessuto,
					:ls_flag_addizionale_vernic,
					:ls_flag_fc_comando_1,
					:ls_flag_fc_comando_2,
					:ls_flag_fc_supporto,
					:ls_flag_fc_tessuto,
					:ls_flag_fc_vernic,
					:ls_flag_allegati,
					:ldt_data_ora_ar,
					:ls_nota_prodotto,
					:ls_flag_nota_ddt)
				using
					istr_tran[il_tran].tran;
				
				if istr_tran[il_tran].tran.sqlcode <> 0 then
					as_messaggio = "Errore in esportazione riga ordine:~n" + istr_tran[il_tran].tran.sqlerrtext
					return -1
				end if
				
			next
			
		case "F"
			
			//Si imposta il testo del messaggio email
			ls_testo = "Si allega il file di esportazione dell'ordine " + string(al_anno_ord_ven) + "/" + string(al_num_ord_ven)
			
			//Si impostano il path e il nome del file di testo da creare
			ls_file = s_cs_xx.volume + s_cs_xx.risorse + string(al_anno_ord_ven) + "_" + string(al_num_ord_ven) + ".txt"
			
			//Se è la prima riga da esportare allora è necessario inserire nel file la testata dell'ordine
			if ll_num_exp = 1 then
				ls_txt = "TES"
				ls_txt += uof_num_str(al_anno_ord_ven,"0000",4)
				ls_txt += uof_num_str(al_num_ord_ven,"000000",6)
				ls_txt += uof_str_str(ls_num_ord_cliente,20)
				ls_txt += uof_dt_str(ldt_data_ord_cliente,"dd/mm/yyyy",10)
				ls_txt += uof_dt_str(ldt_data_consegna,"dd/mm/yyyy",10)
				ls_txt += uof_str_str(ls_alias_cliente,40)
				ls_txt += uof_str_str(ls_flag_lavorazioni_speciali,1)
			end if
			
			//Inserimento della riga d'ordine
			ls_txt += "~r~nDET"
			ls_txt += uof_num_str(al_anno_ord_ven,"0000",4)
			ls_txt += uof_num_str(al_num_ord_ven,"000000",6)
			ls_txt += uof_num_str(ll_riga_ord_ven,"000000",6)
			ls_txt += uof_str_str(ls_cod_misura,3)
			ls_txt += uof_str_str(ls_cod_prodotto,15)
			ls_txt += uof_str_str(ls_des_prodotto,40)
			ls_txt += uof_str_str(ls_nota_dettaglio,255)
			ls_txt += uof_num_str(ld_quan_ordine,"000000000000.0000",17)
			ls_txt += uof_num_str(ld_quantita_um,"000000000000.0000",17)
			ls_txt += uof_num_str(ld_fat_conversione_ven,"00000.00000",11)
			ls_txt += uof_num_str(ld_num_confezioni,"000000000000.0000",17)
			ls_txt += uof_num_str(ld_num_pezzi_confezione,"000000000000.0000",17)
			ls_txt += uof_num_str(ll_num_riga_appartenenza,"000000",6)
			ls_txt += uof_dt_str(ldt_data_consegna,"dd/mm/yyyy",10)
			
			//Inserimento eventuali complementi
			if lb_complementi then
				ls_txt += "~r~nCOM"
				ls_txt += uof_num_str(al_anno_ord_ven,"0000",4)
				ls_txt += uof_num_str(al_num_ord_ven,"000000",6)
				ls_txt += uof_num_str(ll_riga_ord_ven,"000000",6)
				ls_txt += uof_str_str(ls_cod_prod_finito,15)
				ls_txt += uof_str_str(ls_cod_modello,15)
				ls_txt += uof_num_str(ld_dim_x,"000000000000.0000",17)
				ls_txt += uof_num_str(ld_dim_y,"000000000000.0000",17)
				ls_txt += uof_num_str(ld_dim_z,"000000000000.0000",17)
				ls_txt += uof_num_str(ld_dim_t,"000000000000.0000",17)
				ls_txt += uof_str_str(ls_cod_tessuto,15)
				ls_txt += uof_str_str(ls_cod_non_a_magazzino,15)
				ls_txt += uof_str_str(ls_cod_tessuto_mant,15)
				ls_txt += uof_str_str(ls_cod_mant_non_a_magazzino,15)
				ls_txt += uof_str_str(ls_flag_tipo_mantovana,1)
				ls_txt += uof_num_str(ld_alt_mantovana,"000000000000.0000",17)
				ls_txt += uof_str_str(ls_flag_cordolo,1)
				ls_txt += uof_str_str(ls_flag_passamaneria,1)
				ls_txt += uof_str_str(ls_cod_verniciatura,15)
				ls_txt += uof_str_str(ls_cod_comando,15)
				ls_txt += uof_str_str(ls_pos_comando,40)
				ls_txt += uof_num_str(ld_alt_asta,"000000000000.0000",17)
				ls_txt += uof_str_str(ls_cod_comando_tenda_verticale,1)
				ls_txt += uof_str_str(ls_cod_colore_lastra,15)
				ls_txt += uof_str_str(ls_cod_tipo_lastra,1)
				ls_txt += uof_str_str(ls_spes_lastra,1)
				ls_txt += uof_str_str(ls_cod_tipo_supporto,15)
				ls_txt += uof_str_str(ls_flag_cappotta_frontale,1)
				ls_txt += uof_str_str(ls_flag_rollo,1)
				ls_txt += uof_str_str(ls_flag_kit_laterale,1)
				ls_txt += uof_str_str(ls_flag_misura_luce_finita,1)
				ls_txt += uof_str_str(ls_cod_tipo_stampo,15)
				ls_txt += uof_str_str(ls_cod_mat_sis_stampaggio,15)
				ls_txt += uof_str_str(ls_rev_stampo,1)
				ls_txt += uof_num_str(ld_num_gambe,"00",2)
				ls_txt += uof_num_str(ld_num_campate,"00",2)
				ls_txt += uof_str_str(ls_note,255)
				ls_txt += uof_str_str(ls_flag_autoblocco,1)
				ls_txt += uof_num_str(ld_num_fili_plisse,"000",3)
				ls_txt += uof_num_str(ld_num_boccole_plisse,"000",3)
				ls_txt += uof_num_str(ld_intervallo_punzoni_plisse,"000000000000.0000",17)
				ls_txt += uof_num_str(ld_num_pieghe_plisse,"000",3)
				ls_txt += uof_num_str(ld_prof_inf_plisse,"000000000000.0000",17)
				ls_txt += uof_num_str(ld_prof_sup_plisse,"000000000000.0000",17)
				ls_txt += uof_str_str(ls_colore_passamaneria,40)
				ls_txt += uof_str_str(ls_colore_cordolo,40)
				ls_txt += uof_num_str(ld_num_punzoni_plisse,"00",2)
				ls_txt += uof_str_str(ls_des_vernice_fc,40)
				ls_txt += uof_str_str(ls_des_comando_1,40)
				ls_txt += uof_str_str(ls_cod_comando_2,15)
				ls_txt += uof_str_str(ls_des_comando_2,40)
				ls_txt += uof_str_str(ls_flag_addizionale_comando_1,1)
				ls_txt += uof_str_str(ls_flag_addizionale_comando_2,1)
				ls_txt += uof_str_str(ls_flag_addizionale_supporto,1)
				ls_txt += uof_str_str(ls_flag_addizionale_tessuto,1)
				ls_txt += uof_str_str(ls_flag_addizionale_vernic,1)
				ls_txt += uof_str_str(ls_flag_fc_comando_1,1)
				ls_txt += uof_str_str(ls_flag_fc_comando_2,1)
				ls_txt += uof_str_str(ls_flag_fc_supporto,1)
				ls_txt += uof_str_str(ls_flag_fc_tessuto,1)
				ls_txt += uof_str_str(ls_flag_fc_vernic,1)
				ls_txt += uof_str_str(ls_flag_allegati,1)
				ls_txt += uof_dt_str(ldt_data_ora_ar,"dd/mm/yyyy hh:mm:ss",19)
			end if
			
			//Creazione del file di testo
			ll_file = fileopen(ls_file,linemode!,write!,lockreadwrite!,append!)
			
			if ll_file = -1 then
				as_messaggio = "Errore in apertura file ASCII"
				return -1
			end if
			
			//Scrittura nel file di testo della stringa con i dati dell'ordine precedentemente composta
			if filewrite(ll_file,ls_txt) = -1 then
				as_messaggio = "Errore in scrittura su file ASCII"
				return -1
			end if
			
			//Chiusura del file di testo
			if fileclose(ll_file) = -1 then
				as_messaggio = "Errore in chiusura file ASCII"
				return -1
			end if
			
	end choose
	
next

//Se la funzione è stata richiamata in modalità di esecuzione allora alla fine delle righe si completano
//le operazioni di esportazione/invio
if ab_esegui then
	
	//Viene aggiornato l'elenco dei destinatari delle mail di notifica/invio file ASCII
	lb_trovato = false
	
	//Per ogni destinatario si verifica se è già presente nell'elenco
	for ll_i = 1 to upperbound(istr_mail)
		if not isnull(istr_mail[ll_i].email) and trim(istr_mail[ll_i].email) = ls_email then
			ll_dest = ll_i
			lb_trovato = true
			exit
		end if
	next
	
	//Se il destinatario è nuovo lo si aggiunge all'elenco
	if not lb_trovato then
		ll_dest = upperbound(istr_mail) + 1
		istr_mail[ll_dest].email = ls_email
		istr_mail[ll_dest].oggetto = "Invio ordini di vendita"
	end if
	
	//Si aggiunge al destinatario il relativo messaggio
	ll_mess = upperbound(istr_mail[ll_dest].messaggi) + 1
	istr_mail[ll_dest].messaggi[ll_mess].testo = ls_testo
	istr_mail[ll_dest].messaggi[ll_mess].allegato = ls_file
	
	//Una volta conclusa l'esportazione si aggiorna il flag INVIATO in testata dell'ordine del trasmittente
	ldt_now = datetime(today(),now())
	
	update
		tes_ord_ven
	set
		flag_inviato = 'S',
		data_ora_invio = :ldt_now,
		cod_utente_invio = :s_cs_xx.cod_utente,
		cod_reparto_invio = :istr_tran[il_tran].cod_reparto_tx
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :al_anno_ord_ven and
		num_registrazione = :al_num_ord_ven;
	
	if sqlca.sqlcode <> 0 then
		as_messaggio = "Errore in aggiornamento stato invio ordine:~n" + sqlca.sqlerrtext
		return -1
	end if
	
end if

//Sono finite tutte le righe, si esce restituendo il conteggio delle righe esportate/rilevate
destroy lds_righe

return ll_num_exp
end function

public function integer uof_import_ord_ven (string as_cod_reparto, long al_anno_ord_ven, long al_num_ord_ven, boolean ab_respingi, ref string as_messaggio);/*
	Funzione che esegue la conferma degli ordini ricevuti notificandone l'esito al trasmittente.
	Se AB_RESPINGI è TRUE l'ordine originale viene segnato come respinto e al trasmittente vengono notificate
	le eventuali note del ricevente sul motivo del rifiuto dell'ordine.
	Se AB_RESPINGI è FALSE nel database del ricevente viene generato un nuovo ordine a partire dai dati contenuti
	nelle tabelle di appoggio, successivamente al trasmittente vengono notificate le eventuali modifiche sui dati
	trasmessi apportate dal ricevente prima della conferma.
	Valori di ritorno:
		 0, conferma eseguita correttamente
	  	-1, errore della procedura (specificato in AS_MESSAGGIO)
*/

boolean lb_presenza_distinta,lb_processi_configuratore

string ls_cod_cliente, ls_cod_tipo_ord_ven, ls_num_ord_cliente, ls_alias_cliente, ls_flag_lavorazioni_speciali, &
       ls_cod_operatore, ls_cod_deposito, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
		 ls_cod_agente_1,ls_cod_agente_2,ls_cod_banca_clien_for,ls_cod_imballo,ls_cod_vettore,ls_cod_inoltro,&
		 ls_cod_mezzo,ls_cod_porto,ls_cod_resa,ls_flag_fuori_fido,ls_flag_riep_boll,ls_flag_riep_fatt,ls_flag_blocco, &
		 ls_cod_ubicazione, ls_cod_giro_consegna,ls_cod_banca, ls_nota_testata, ls_nota_piede, ls_nota_video, ls_null, &
		 ls_cod_tipo_det_ven, ls_cod_misura_ven, ls_cod_prodotto, ls_des_prodotto, ls_nota_dettaglio, &
		 ls_cod_prod_finito, ls_cod_modello,ls_cod_tessuto, ls_cod_non_a_magazzino, ls_flag_tipo_mantovana, &
		 ls_flag_cordolo, ls_flag_passamaneria, ls_cod_verniciatura, ls_pos_comando, ls_cod_tipo_supoporto, &
		 ls_flag_misura_luce_finita, ls_colore_passamaneria, ls_colore_cordolo, ls_des_vernice_fc, ls_des_comando_1, &
		 ls_cod_comando_2, ls_des_comando_2, ls_flag_addizionale_comando_1, ls_flag_addizionale_comando_2, ls_flag_addizionale_supporto, &
		 ls_flag_addizionale_tessuto, ls_flag_addizionale_vernic, ls_flag_fc_comando_1, ls_flag_fc_comando_2, ls_flag_fc_supporto, &
		 ls_flag_fc_tessuto, ls_flag_fc_vernic, ls_flag_allegati, ls_cod_tessuto_mant, ls_cod_mant_non_a_magazzino, ls_cod_comando, &
		 ls_cod_tipo_supporto, ls_cod_versione, ls_flag_gen_commessa, ls_flag_presso_cgibus, ls_flag_presso_ptenda, &
		 ls_cod_iva_det_ven, ls_cod_iva_prodotto, ls_cod_iva_cliente, ls_flag_doc_suc, ls_flag_stampa_note, ls_cod_nota_prodotto, &
		 ls_anag_prod_note, ls_cod_iva, ls_flag_trasporta_nota_ddt, ls_note_comp_det_ord_ven,ls_flag_tipo_det_ven, &
		 ls_flag_verniciatura, ls_asta,ls_flag_prezzo_superficie,ls_flag_uso_quan_distinta, ls_cod_gruppo_var_quan_distinta, &
		 ls_rag_soc_abbreviata, ls_rif_interscambio, ls_flag_tassativo, ls_nota_prodotto, ls_cod_prodotto_raggruppato

long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven,ll_giorno, ll_num_confezioni, &
     ll_num_pezzi_confezione, ll_num_riga_appartenenza, ll_cont

dec{4} ld_sconto, ld_cambio_ven, ld_quan_ordine, ld_quantita_um, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, &
       ld_alt_mantovana,ld_alt_asta, ld_quan_raggruppo

dec{5} ld_fat_conversione_ven

datetime ldt_today, ldt_data_ord_cliente, ldt_data_consegna, ldt_oggi,ldt_data_blocco,ldt_data_pronto, &
         ldt_data_ora_ar,ldt_data_esenzione_iva

boolean  lb_trovato

long		ll_for_mess, ll_dest, ll_mess

string	ls_tipo_reparto, ls_tipo_trasmissione, ls_email[], ls_testo


ldt_oggi = datetime(today(),00:00:00)
ldt_today = datetime(today(),now())
setnull(ls_null)

//Lettura dati reparto
if uof_dati_reparto(as_cod_reparto,ls_tipo_reparto,ls_tipo_trasmissione,ls_email,as_messaggio) <> 0 then
	return -1
end if

//Il reparto corrispondente al trasmittente deve essere configurato come esterno
if ls_tipo_reparto <> "E" then
	as_messaggio = "Il reparto indicato non è un reparto esterno"
	return -1
end if

choose case ab_respingi
		
	case true
		
		//Il reparto corrispondente al trasmittente deve essere configurato per la connesione al DB
		if ls_tipo_trasmissione <> "C" then
			as_messaggio = "Il reparto esterno indicato non è configurato per la connessione al database remoto"
			return -1
		end if
		
		//Connessione al DB del reparto esterno
		if uof_connetti_db(as_cod_reparto,as_messaggio) <> 0 then
			as_messaggio = "Errore in connessione al DB del reparto " + as_cod_reparto + ":~n" + as_messaggio
			return -1
		end if
		
		//Se l'ordine è stato respinto è sufficiente impostare l'apposito flag in testata del trasmittente
		update
			tes_ord_ven
		set
			flag_respinto = 'S'
		where
			anno_registrazione = :al_anno_ord_ven and
			num_registrazione = :al_num_ord_ven
		using
			istr_tran[il_tran].tran;
		
		if istr_tran[il_tran].tran.sqlcode <> 0 then
			as_messaggio = "Errore in impostazione flag respinto:~n" + istr_tran[il_tran].tran.sqlerrtext
			return -1
		end if
		
	case false
		
		// non è respinto e quindi parto con l'importazione dell'ordine.
		
		// carico anno di esercizio corrente
		ll_anno_registrazione = f_anno_esercizio()
		
		// carico il numero ordine nella modalità random prevista
		ll_num_registrazione = f_gen_num_ordine()
		
		// carico codice utente che sta importando
		select cod_operatore
		  into :ls_cod_operatore
		  from tab_operatori_utenti
		 where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_utente = :s_cs_xx.cod_utente and
				 flag_default = 'S';
		if sqlca.sqlcode < 0 then
			setnull(ls_cod_operatore)
		end if

		
		select cod_cliente,   
				cod_tipo_ord_ven,   
				num_ord_cliente,   
				data_ord_cliente,   
				data_consegna,   
				alias_cliente,   
				flag_lavorazioni_speciali,
				rag_soc_abbreviata,
				flag_tassativo
		 into :ls_cod_cliente,   
				:ls_cod_tipo_ord_ven,   
				:ls_num_ord_cliente,   
				:ldt_data_ord_cliente,   
				:ldt_data_consegna,   
				:ls_alias_cliente,   
				:ls_flag_lavorazioni_speciali,
				:ls_rag_soc_abbreviata,
				:ls_flag_tassativo
		 from buffer_ord_ven  
		where cod_reparto = :as_cod_reparto and
				anno_registrazione = :al_anno_ord_ven and
				num_registrazione = :al_num_ord_ven and
				flag_originale = 'N';
		if sqlca.sqlcode <> 0 then
			as_messaggio = "Errore in ricerca dell'ordine selezionato per l'importazione~n" + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ls_rag_soc_abbreviata) then
			ls_rag_soc_abbreviata = ""
		else
			ls_rag_soc_abbreviata = " " + ls_rag_soc_abbreviata
		end if
		
		ls_rif_interscambio = string(al_anno_ord_ven) + "/" + string(al_num_ord_ven) + ls_rag_soc_abbreviata
		
		select cod_deposito,
				 cod_valuta,
				 cod_tipo_listino_prodotto,
				 cod_pagamento,
				 sconto,
				 cod_agente_1,
				 cod_agente_2,
				 cod_banca_clien_for,
				 cod_imballo,
				 cod_vettore,
				 cod_inoltro,
				 cod_mezzo,
				 cod_porto,
				 cod_resa,
				 flag_fuori_fido,
				 flag_riep_boll,
				 flag_riep_fatt,
				 cod_banca,
				 flag_blocco,
				 data_blocco
		into   :ls_cod_deposito,
				 :ls_cod_valuta,
				 :ls_cod_tipo_listino_prodotto,
				 :ls_cod_pagamento,
				 :ld_sconto,
				 :ls_cod_agente_1,
				 :ls_cod_agente_2,
				 :ls_cod_banca_clien_for,
				 :ls_cod_imballo,
				 :ls_cod_vettore,
				 :ls_cod_inoltro,
				 :ls_cod_mezzo,
				 :ls_cod_porto,
				 :ls_cod_resa,
				 :ls_flag_fuori_fido,
				 :ls_flag_riep_boll,
				 :ls_flag_riep_fatt,
				 :ls_cod_banca,
				 :ls_flag_blocco,
				 :ldt_data_blocco					 
		from   anag_clienti
		where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
				 anag_clienti.cod_cliente = :ls_cod_cliente;
	
	
		ld_cambio_ven = dec(f_cambio_val_ven(ls_cod_valuta, ldt_oggi))
	
		// ***********  in base alla specifica la destinazione merce viene caricata vuota *****
		
		// ***********  carico note testata e note di piede *****************
		if f_crea_nota_fissa(ls_cod_cliente, ls_null, "ORD_VEN", ldt_oggi, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) = -1 then
			as_messaggio = "Errore in estrazione della nota fissa di testata e di piede dell'ordine"
			return -1
		end if
		
		// ***********  caricare giro di consegna merce *****************
		
		select cod_giro_consegna
		into   :ls_cod_giro_consegna
		from   det_giri_consegne
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente;
		if sqlca.sqlcode < 0 then
			as_messaggio = "Errore in ricerca del giro di consegna del cliente~n" + sqlca.sqlerrtext
			return -1
		end if
			
		// ***********  calcolo della data pronto  *****************
		ll_giorno = daynumber(date(ldt_data_consegna))
		choose case ll_giorno
			case 1    // domenica
				ldt_data_pronto = datetime(relativedate(date(ldt_data_consegna), -2), 00:00:00)
			case 2	 // lunedì
				ldt_data_pronto = datetime(relativedate(date(ldt_data_consegna), -4), 00:00:00)
			case 3	// martedì
				ldt_data_pronto = datetime(relativedate(date(ldt_data_consegna), -4), 00:00:00)
			case 4	// mercoledì
				ldt_data_pronto = datetime(relativedate(date(ldt_data_consegna), -2), 00:00:00)
			case 5	// giovedì
				ldt_data_pronto = datetime(relativedate(date(ldt_data_consegna), -2), 00:00:00)
			case 6	// venerdì
				ldt_data_pronto = datetime(relativedate(date(ldt_data_consegna), -2), 00:00:00)
			case 7	// sabato 
				ldt_data_pronto = datetime(relativedate(date(ldt_data_consegna), -2), 00:00:00)
		end choose
		
		// ***********  valorizzare il rif_interscambio
		//*****************
		
		INSERT INTO tes_ord_ven  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  data_registrazione,   
				  ora_registrazione,   
				  cod_operatore,   
				  cod_tipo_ord_ven,   
				  cod_cliente,   
				  cod_des_cliente,   
				  rag_soc_1,   
				  rag_soc_2,   
				  indirizzo,   
				  localita,   
				  frazione,   
				  cap,   
				  provincia,   
				  cod_deposito,   
				  cod_ubicazione,   
				  cod_valuta,   
				  cambio_ven,   
				  cod_tipo_listino_prodotto,   
				  cod_pagamento,   
				  sconto,   
				  cod_agente_1,   
				  cod_agente_2,   
				  cod_banca,   
				  num_ord_cliente,   
				  data_ord_cliente,   
				  cod_imballo,   
				  aspetto_beni,   
				  cod_vettore,   
				  cod_inoltro,   
				  cod_mezzo,   
				  causale_trasporto,   
				  cod_porto,   
				  cod_resa,   
				  nota_testata,   
				  nota_piede,   
				  flag_evasione,   
				  flag_riep_bol,   
				  flag_riep_fat,   
				  data_consegna,   
				  cod_banca_clien_for,   
				  cod_causale,   
				  flag_stampato,   
				  cod_giro_consegna,   
				  flag_controllato,   
				  data_pronto,   
				  flag_stampata_bcl,   
				  flag_doc_suc_tes,   
				  flag_doc_suc_pie,   
				  flag_st_note_tes,   
				  flag_st_note_pie,   
				  flag_tassativo,   
				  alias_cliente,   
				  flag_inviato,   
				  flag_respinto,   
				  rif_interscambio,   
				  flag_stampa_rif_intersc,   
				  flag_stampa_num_ordine,   
				  flag_lavorazioni_speciali )  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :ll_anno_registrazione,   
				  :ll_num_registrazione,   
				  :ldt_oggi,   
				  :ldt_today,
				  :ls_cod_operatore,   
				  :ls_cod_tipo_ord_ven,   
				  :ls_cod_cliente,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  :ls_cod_deposito,   
				  :ls_cod_ubicazione,   
				  :ls_cod_valuta,   
				  :ld_cambio_ven,   
				  :ls_cod_tipo_listino_prodotto,   
				  :ls_cod_pagamento,   
				  :ld_sconto,   
				  :ls_cod_agente_1,   
				  :ls_cod_agente_2,   
				  :ls_cod_banca,   
				  :ls_num_ord_cliente,   
				  :ldt_data_ord_cliente,   
				  :ls_cod_imballo,   
				  null,   
				  :ls_cod_vettore,   
				  :ls_cod_inoltro,   
				  :ls_cod_mezzo,   
				  null,   
				  :ls_cod_porto,   
				  :ls_cod_resa,   
				  :ls_nota_testata,   
				  :ls_nota_piede,   
				  'A',   
				  :ls_flag_riep_boll,   
				  :ls_flag_riep_fatt,   
				  :ldt_data_consegna,   
				  :ls_cod_banca_clien_for,   
				  null,   
				  'N',   
				  :ls_cod_giro_consegna,   
				  'N',   
				  :ldt_data_pronto,   
				  'N',   
				  'I',   
				  'I',   
				  'I',   
				  'I',   
				  :ls_flag_tassativo,   
				  :ls_alias_cliente,   
				  'N',   
				  'N',   
				  :ls_rif_interscambio,   
				  'S',   
				  'S',   
				  :ls_flag_lavorazioni_speciali);
			if sqlca.sqlcode < 0 then
				as_messaggio = "Errore in inserimento testata ordine di vendita~r~nORDINE RICEVUTO " + string(al_anno_ord_ven) + "/" + string(al_num_ord_ven) + "~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
			declare cu_buffer_ord_ven_det cursor for  
			  select prog_riga_ord_ven,   
						cod_tipo_det_ven,   
						cod_misura,   
						cod_prodotto,   
						des_prodotto,   
						nota_dettaglio,   
						quan_ordine,   
						quantita_um,   
						fat_conversione_ven,   
						num_confezioni,   
						num_pezzi_confezione,   
						num_riga_appartenenza,   
						data_consegna,   
						cod_prod_finito,   
						cod_modello,   
						dim_x,   
						dim_y,   
						dim_z,   
						dim_t,   
						cod_tessuto,   
						cod_non_a_magazzino,   
						cod_tessuto_mant,   
						cod_mant_non_a_magazzino,   
						flag_tipo_mantovana,   
						alt_mantovana,   
						flag_cordolo,   
						flag_passamaneria,   
						cod_verniciatura,   
						cod_comando,   
						pos_comando,   
						alt_asta,   
						cod_tipo_supporto,   
						flag_misura_luce_finita,   
						colore_passamaneria,   
						colore_cordolo,   
						des_vernice_fc,   
						des_comando_1,   
						cod_comando_2,   
						des_comando_2,   
						flag_addizionale_comando_1,   
						flag_addizionale_comando_2,   
						flag_addizionale_supporto,   
						flag_addizionale_tessuto,   
						flag_addizionale_vernic,   
						flag_fc_comando_1,   
						flag_fc_comando_2,   
						flag_fc_supporto,   
						flag_fc_tessuto,   
						flag_fc_vernic,   
						flag_allegati,   
						data_ora_ar,
						note,
						nota_prodotto,
						flag_trasporta_nota_ddt
				 from buffer_ord_ven_det  
				where cod_reparto = :as_cod_reparto and  
						anno_registrazione = :al_anno_ord_ven and  
						num_registrazione = :al_num_ord_ven and
						flag_originale = 'N';
						
			open cu_buffer_ord_ven_det;
			if sqlca.sqlcode < 0 then
				as_messaggio = "Errore nella open del cursore cu_buffer_ord_ven_det~r~n" + sqlca.sqlerrtext
				return -1
			end if		
			
			do while true
					fetch cu_buffer_ord_ven_det into 
						:ll_prog_riga_ord_ven,   
						:ls_cod_tipo_det_ven,   
						:ls_cod_misura_ven,   
						:ls_cod_prodotto,   
						:ls_des_prodotto,   
						:ls_nota_dettaglio,   
						:ld_quan_ordine,   
						:ld_quantita_um,   
						:ld_fat_conversione_ven,   
						:ll_num_confezioni,   
						:ll_num_pezzi_confezione,   
						:ll_num_riga_appartenenza,   
						:ldt_data_consegna,   
						:ls_cod_prod_finito,   
						:ls_cod_modello,   
						:ld_dim_x,   
						:ld_dim_y,   
						:ld_dim_z,   
						:ld_dim_t,   
						:ls_cod_tessuto,   
						:ls_cod_non_a_magazzino,   
						:ls_cod_tessuto_mant,   
						:ls_cod_mant_non_a_magazzino,   
						:ls_flag_tipo_mantovana,   
						:ld_alt_mantovana,   
						:ls_flag_cordolo,   
						:ls_flag_passamaneria,   
						:ls_cod_verniciatura,   
						:ls_cod_comando,   
						:ls_pos_comando,   
						:ld_alt_asta,   
						:ls_cod_tipo_supporto,   
						:ls_flag_misura_luce_finita,   
						:ls_colore_passamaneria,   
						:ls_colore_cordolo,   
						:ls_des_vernice_fc,   
						:ls_des_comando_1,   
						:ls_cod_comando_2,   
						:ls_des_comando_2,   
						:ls_flag_addizionale_comando_1,   
						:ls_flag_addizionale_comando_2,   
						:ls_flag_addizionale_supporto,   
						:ls_flag_addizionale_tessuto,   
						:ls_flag_addizionale_vernic,   
						:ls_flag_fc_comando_1,   
						:ls_flag_fc_comando_2,   
						:ls_flag_fc_supporto,   
						:ls_flag_fc_tessuto,   
						:ls_flag_fc_vernic,   
						:ls_flag_allegati,   
						:ldt_data_ora_ar,
						:ls_note_comp_det_ord_ven,
						:ls_nota_prodotto,
						:ls_flag_trasporta_nota_ddt;
				if sqlca.sqlcode = 100 then
					close cu_buffer_ord_ven_det;
					exit
				elseif sqlca.sqlcode = -1 then
					as_messaggio = "Errore in fetch del cursore cu_buffer_ord_ven_det~r~n" + sqlca.sqlerrtext
					close cu_buffer_ord_ven_det;
					return -1
				end if
				
				istr_riepilogo.dimensione_1 = ld_dim_x
				istr_riepilogo.dimensione_2 = ld_dim_y
				istr_riepilogo.dimensione_3 = ld_dim_z
				istr_riepilogo.dimensione_4 = ld_dim_t
				
				// ***************  ricerca della versione predefinita  *********************************
				// verifico se esiste una distinta
				if not isnull(ls_cod_prodotto) then
					ll_cont = 0
					lb_presenza_distinta = true
					
					select count(*)
					into   :ll_cont
					from   distinta_padri
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_prodotto and
							 flag_blocco = 'N';
									
					if ll_cont < 1 or isnull(ll_cont) then 
						lb_presenza_distinta = false
					else	
						select cod_versione
						into   :ls_cod_versione
						from   distinta_padri
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto and
								 flag_predefinita = 'S' and
								 flag_blocco = 'N';
						if sqlca.sqlcode <> 0 then
							setnull(ls_cod_versione)
							lb_presenza_distinta = false
						end if
					end if
				else
					setnull(ls_cod_versione)
					lb_presenza_distinta = false
				end if
				
				// *************** CERCO NEL TIPO DETTAGLIO I FLAG PER LE NOTE / IVA / GEN_COMMESSA
				
				uo_default_prodotto luo_default_prodotto
				luo_default_prodotto = CREATE uo_default_prodotto
				if luo_default_prodotto.uof_flag_gen_commessa(ls_cod_prodotto) = 0 then
					ls_flag_gen_commessa   = luo_default_prodotto.is_flag_gen_commessa
					ls_flag_presso_cgibus  = luo_default_prodotto.is_flag_presso_cg
					ls_flag_presso_ptenda = luo_default_prodotto.is_flag_presso_pt
				end if
				destroy luo_default_prodotto
				
				SELECT cod_iva,   
						 cod_nota_prodotto,   
						 flag_doc_suc_or,   
						 flag_st_note_or  
				 INTO :ls_cod_iva_det_ven,   
						:ls_cod_nota_prodotto,   
						:ls_flag_doc_suc,   
						:ls_flag_stampa_note  
				 FROM tab_tipi_det_ven  
				WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
						cod_tipo_det_ven = :ls_cod_tipo_det_ven ;
				if sqlca.sqlcode <> 0 then
					as_messaggio = "Errore in ricerca nella tabella tipi dettagli di vendita del tipo dettaglio di vendita con codice" + ls_cod_tipo_det_ven + "~r~n" + sqlca.sqlerrtext
					close cu_buffer_ord_ven_det;
					return -1
				end if
				
				if not isnull(ls_cod_nota_prodotto) and len(ls_cod_nota_prodotto) > 0 then
					select nota_prodotto
					into   :ls_anag_prod_note
					from   anag_prodotti_note
					where  cod_azienda = :s_cs_xx.cod_azienda and
					       cod_prodotto = :ls_cod_prodotto and
							 cod_nota_prodotto = :ls_cod_nota_prodotto;
					if sqlca.sqlcode = 0 then
						ls_nota_dettaglio += "~r~n" + ls_anag_prod_note
					end if
				end if
				
				// *************  trovo l'aliquouta iva giusta ************************
				if not isnull(ls_cod_cliente) then
					select cod_iva,
							 data_esenzione_iva
					into   :ls_cod_iva_cliente,
							 :ldt_data_esenzione_iva
					from   anag_clienti
					where  cod_azienda = :s_cs_xx.cod_azienda and 
							 cod_cliente = :ls_cod_cliente;
				end if
	
				if sqlca.sqlcode = 0 then
					if ls_cod_iva_cliente <> "" and not isnull(ls_cod_iva_cliente) and (ldt_data_esenzione_iva <= ldt_oggi or isnull(ldt_data_esenzione_iva)) then
						ls_cod_iva = ls_cod_iva_cliente
					else
						select cod_iva
						into   :ls_cod_iva_prodotto
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
						       cod_prodotto = :ls_cod_prodotto;
						if sqlca.sqlcode = 0 and not isnull(ls_cod_iva_prodotto) then
							ls_cod_iva = ls_cod_iva_prodotto
						else
							ls_cod_iva = ls_cod_iva_det_ven
						end if
					end if
				end if
				
				// ***************  insert nella det_ord_ven
				INSERT INTO det_ord_ven  
						( cod_azienda,   
						  anno_registrazione,   
						  num_registrazione,   
						  prog_riga_ord_ven,   
						  cod_prodotto,   
						  cod_tipo_det_ven,   
						  cod_misura,   
						  des_prodotto,   
						  quan_ordine,   
						  prezzo_vendita,   
						  fat_conversione_ven,   
						  sconto_1,   
						  sconto_2,   
						  provvigione_1,   
						  provvigione_2,   
						  cod_iva,   
						  data_consegna,   
						  val_riga,   
						  quan_in_evasione,   
						  quan_evasa,   
						  flag_evasione,   
						  flag_blocco,   
						  nota_dettaglio,   
						  anno_registrazione_off,   
						  num_registrazione_off,   
						  prog_riga_off_ven,   
						  anno_commessa,   
						  num_commessa,   
						  cod_centro_costo,   
						  sconto_3,   
						  sconto_4,   
						  sconto_5,   
						  sconto_6,   
						  sconto_7,   
						  sconto_8,   
						  sconto_9,   
						  sconto_10,   
						  cod_versione,   
						  flag_presso_ptenda,   
						  flag_presso_cgibus,   
						  num_confezioni,   
						  num_pezzi_confezione,   
						  num_riga_appartenenza,   
						  flag_gen_commessa,   
						  flag_doc_suc_det,   
						  flag_st_note_det,   
						  quantita_um,   
						  prezzo_um,   
						  imponibile_iva,   
						  imponibile_iva_valuta,   
						  nota_prodotto,   
						  flag_trasporta_nota_ddt,   
						  settimana_consegna,   
						  flag_stampa_settimana)  
				VALUES ( :s_cs_xx.cod_azienda,   
						  :ll_anno_registrazione,   
						  :ll_num_registrazione,   
						  :ll_prog_riga_ord_ven,   
						  :ls_cod_prodotto,   
						  :ls_cod_tipo_det_ven,   
						  :ls_cod_misura_ven,   
						  :ls_des_prodotto,   
						  :ld_quan_ordine,   
						  0,   
						  :ld_fat_conversione_ven,   
						  0,   
						  0,   
						  0,   
						  0,   
						  :ls_cod_iva,   
						  :ldt_data_consegna,   
						  0,   
						  0,   
						  0,   
						  'A',   
						  'N',   
						  :ls_nota_dettaglio,   
						  null,   
						  null,   
						  null,   
						  null,   
						  null,   
						  null,   
						  0,   
						  0,   
						  0,   
						  0,   
						  0,   
						  0,   
						  0,   
						  0,   
						  :ls_cod_versione,   
						  :ls_flag_presso_ptenda,   
						  :ls_flag_presso_cgibus,   
						  0,   
						  0,   
						  :ll_num_riga_appartenenza,   
						  :ls_flag_gen_commessa,   
						  :ls_flag_doc_suc,   
						  :ls_flag_stampa_note,   
						  :ld_quantita_um,   
						  0,   
						  0,   
						  0,   
						  :ls_nota_prodotto,   
						  :ls_flag_trasporta_nota_ddt,   
						  null,   
						  'N' )  ;
				if sqlca.sqlcode < 0 then
					as_messaggio = "Errore in inserimento dettaglio ordine di vendita. Ordine " + string(al_anno_ord_ven) + "/" + string(al_num_ord_ven) + " riga nr." + string(ll_prog_riga_ord_ven) + " Prodotto " + ls_cod_prodotto + "~r~nErrore:" +sqlca.sqlerrtext
					close cu_buffer_ord_ven_det;
					return -1
				end if
				
				// *********************  verifico de è un prodotto da configurare *********************
				
				if ll_num_riga_appartenenza > 0 and not isnull(ll_num_riga_appartenenza) then
					lb_processi_configuratore = false
				else
					
					lb_processi_configuratore = false
					
					select flag_verniciatura,
					       flag_prezzo_superficie, 
							 flag_uso_quan_distinta, 
							 cod_gruppo_var_quan_distinta
					into   :ls_flag_verniciatura,
					       :ls_flag_prezzo_superficie, 
							 :ls_flag_uso_quan_distinta, 
							 :ls_cod_gruppo_var_quan_distinta
					from   tab_flags_configuratore
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_modello = :ls_cod_prodotto;
							 
					if sqlca.sqlcode < 0 then
						as_messaggio = "Errore in lettura parametri configuratore. ~r~n" + sqlca.sqlerrtext
						close cu_buffer_ord_ven_det;
						return -1
						
					elseif sqlca.sqlcode = 0 and lb_presenza_distinta then
						
						lb_processi_configuratore = true
						
					end if
				end if
				
				if lb_processi_configuratore then
				// ***************  insert nella comp_det_ord_ven
					INSERT INTO comp_det_ord_ven  
							( cod_azienda,   
							  anno_registrazione,   
							  num_registrazione,   
							  prog_riga_ord_ven,   
							  cod_prod_finito,   
							  cod_modello,   
							  dim_x,   
							  dim_y,   
							  dim_z,   
							  dim_t,   
							  cod_tessuto,   
							  cod_non_a_magazzino,   
							  cod_tessuto_mant,   
							  cod_mant_non_a_magazzino,   
							  flag_tipo_mantovana,   
							  alt_mantovana,   
							  flag_cordolo,   
							  flag_passamaneria,   
							  cod_verniciatura,   
							  cod_comando,   
							  pos_comando,   
							  alt_asta,   
							  cod_tipo_supporto,   
							  flag_misura_luce_finita,   
							  note,   
							  colore_passamaneria,   
							  colore_cordolo,   
							  des_vernice_fc,   
							  des_comando_1,   
							  cod_comando_2,   
							  des_comando_2,   
							  flag_addizionale_comando_1,   
							  flag_addizionale_comando_2,   
							  flag_addizionale_supporto,   
							  flag_addizionale_tessuto,   
							  flag_addizionale_vernic,   
							  flag_fc_comando_1,   
							  flag_fc_comando_2,   
							  flag_fc_supporto,   
							  flag_fc_tessuto,   
							  flag_fc_vernic,   
							  flag_allegati,   
							  data_ora_ar,   
							  flag_tessuto_cliente,   
							  flag_tessuto_mant_cliente,
							  cod_comando_tenda_verticale,
							  num_gambe,
							  num_campate,
							  num_fili_plisse,
							  num_boccole_plisse,
							  intervallo_punzoni_plisse,
							  num_pieghe_plisse,
							  prof_inf_plisse,
							  prof_sup_plisse,
							  num_punzoni_plisse)  
					VALUES ( :s_cs_xx.cod_azienda,   
							  :ll_anno_registrazione,   
							  :ll_num_registrazione,   
							  :ll_prog_riga_ord_ven,   
							  :ls_cod_prod_finito,   
							  :ls_cod_prodotto,   
							  :ld_dim_x,   
							  :ld_dim_y,   
							  :ld_dim_z,   
							  :ld_dim_t,   
							  :ls_cod_tessuto,   
							  :ls_cod_non_a_magazzino,   
							  :ls_cod_tessuto_mant,   
							  :ls_cod_mant_non_a_magazzino,   
							  :ls_flag_tipo_mantovana,   
							  :ld_alt_mantovana,   
							  :ls_flag_cordolo,   
							  :ls_flag_passamaneria,   
							  :ls_cod_verniciatura,   
							  :ls_cod_comando,   
							  :ls_pos_comando,   
							  :ld_alt_asta,   
							  :ls_cod_tipo_supporto,   
							  :ls_flag_misura_luce_finita,   
							  :ls_note_comp_det_ord_ven,   
							  :ls_colore_passamaneria,   
							  :ls_colore_cordolo,   
							  :ls_des_vernice_fc,   
							  :ls_des_comando_1,   
							  :ls_cod_comando_2,   
							  :ls_des_comando_2,   
							  :ls_flag_addizionale_comando_1,   
							  :ls_flag_addizionale_comando_2,   
							  :ls_flag_addizionale_supporto,   
							  :ls_flag_addizionale_tessuto,   
							  :ls_flag_addizionale_vernic,   
							  :ls_flag_fc_comando_1,   
							  :ls_flag_fc_comando_2,   
							  :ls_flag_fc_supporto,   
							  :ls_flag_fc_tessuto,   
							  :ls_flag_fc_vernic,   
							  :ls_flag_allegati,   
							  :ldt_data_ora_ar,   
							  'N',   
							  'N',
							  null,
							  0,
							  0,
							  0,
							  0,
							  0,
							  0,
							  0,
							  0,
							  0)  ;
					if sqlca.sqlcode < 0 then
						as_messaggio = "Errore in inserimento dettaglio complementare ordine di vendita. Ordine " + string(al_anno_ord_ven) + "/" + string(al_num_ord_ven) + " riga nr." + string(ll_prog_riga_ord_ven) + " Prodotto " + ls_cod_prodotto + "~r~nErrore:" +sqlca.sqlerrtext
						close cu_buffer_ord_ven_det;
						return -1
					end if
	
					// ***************  carico la struttura delle materie prime automatiche *****************
					string ls_cod_mp_non_richiesta
					long ll_prog_mp_non_richiesta, ll_num_mp, ll_i
					str_mp_automatiche lstr_mp_automatiche[]
	
					istr_riepilogo.mp_automatiche = lstr_mp_automatiche
					
					if ls_flag_verniciatura = "S" then
						// se non c'era la verniciatura prendo quella di default
						if isnull(ls_cod_verniciatura) or len(ls_cod_verniciatura) < 1 then
							select cod_verniciatura  
							into   :ls_cod_verniciatura  
							from   tab_modelli_verniciature  
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_modello = :ls_cod_prodotto and  
									 flag_default = 'S' ;
							if sqlca.sqlcode <> 0 then
								ls_cod_verniciatura = ""
							end if
						end if 
		
						if  len(ls_cod_verniciatura) > 0 and not isnull(ls_cod_verniciatura) then
							declare cur_mp_non_richieste_vern cursor for
							SELECT tab_mp_non_richieste.cod_mp_non_richiesta,   
									tab_mp_non_richieste.prog_mp_non_richiesta
							 FROM tab_mp_non_richieste,   
									tab_mp_automatiche_mod_vern  
							WHERE ( tab_mp_automatiche_mod_vern.cod_azienda = tab_mp_non_richieste.cod_azienda ) and  
									( tab_mp_automatiche_mod_vern.cod_mp_non_richiesta = tab_mp_non_richieste.cod_mp_non_richiesta ) and  
									( tab_mp_automatiche_mod_vern.prog_mp_non_richiesta = tab_mp_non_richieste.prog_mp_non_richiesta ) and  
									( ( tab_mp_non_richieste.cod_azienda = :s_cs_xx.cod_azienda ) AND  
									( :ld_dim_x between lim_inf_dim_1 and lim_sup_dim_1 ) AND  
									( :ld_dim_y between lim_inf_dim_2 and lim_sup_dim_2 ) AND
									(tab_mp_non_richieste.flag_asta = 'N') AND
									(tab_mp_non_richieste.flag_blocco = 'N' OR  
									(tab_mp_non_richieste.flag_blocco = 'S' AND  
									tab_mp_non_richieste.data_blocco > :ldt_oggi)) AND  
									tab_mp_automatiche_mod_vern.cod_modello = :str_conf_prodotto.cod_modello AND  
									tab_mp_automatiche_mod_vern.cod_verniciatura = :ls_cod_verniciatura )   ;
							open cur_mp_non_richieste_vern;
							if sqlca.sqlcode < 0 then
								as_messaggio = "Errore in caricamento MP automatiche per il modello/verniciatura.~r~nDETTAGLIO: " + sqlca.sqlerrtext
								close cu_buffer_ord_ven_det;
								return -1
							end if
						end if
						
					else		// non c'è verniciatura su questo prodotto
						
						declare cur_mp_non_richieste cursor for
					  select cod_mp_non_richiesta,
								prog_mp_non_richiesta
						 from tab_mp_non_richieste
						where cod_azienda = :s_cs_xx.cod_azienda and
								cod_modello_tenda = :str_conf_prodotto.cod_modello and
								(:ld_dim_x between lim_inf_dim_1 and lim_sup_dim_1 or
								lim_inf_dim_1 = 0 and lim_sup_dim_1 = 0 ) and
								(:ld_dim_y between lim_inf_dim_2 and lim_sup_dim_2 or
								lim_inf_dim_2 = 0 and lim_sup_dim_2 = 0 ) and
								(flag_asta = 'N') and
								(flag_blocco = 'N' or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
						open cur_mp_non_richieste;
						if sqlca.sqlcode < 0 then
							as_messaggio = "Errore in caricamento MP automatiche~r~nDETTAGLIO: " + sqlca.sqlerrtext
							close cu_buffer_ord_ven_det;
							return -1
						end if
					end if
	
					if ls_flag_verniciatura = "S" then
						if  len(ls_cod_verniciatura) > 0 and not isnull(ls_cod_verniciatura) then
							fetch cur_mp_non_richieste_vern into :ls_cod_mp_non_richiesta, :ll_prog_mp_non_richiesta;
						end if
					else
						fetch cur_mp_non_richieste into :ls_cod_mp_non_richiesta, :ll_prog_mp_non_richiesta;
					end if
					if sqlca.sqlcode = -1 then
						as_messaggio = "Errore durante l'estrazione delle materie prime.~r~nDETTAGLIO: " + sqlca.sqlerrtext
						if ls_flag_verniciatura = "N" then
							close cur_mp_non_richieste;
						else
							close cur_mp_non_richieste_vern;
						end if
						close cu_buffer_ord_ven_det;
						return -1
					end if
	
					do while sqlca.sqlcode = 0
						ll_cont ++
						istr_riepilogo.mp_automatiche[ll_cont].cod_mp_automatica = ls_cod_mp_non_richiesta
						istr_riepilogo.mp_automatiche[ll_cont].prog_mp_automatica = ll_prog_mp_non_richiesta
						if ls_flag_verniciatura = "S" then
							fetch cur_mp_non_richieste_vern into :ls_cod_mp_non_richiesta, :ll_prog_mp_non_richiesta;
						else
							fetch cur_mp_non_richieste into :ls_cod_mp_non_richiesta, :ll_prog_mp_non_richiesta;
						end if
					loop
	
					if ls_flag_verniciatura = "S" then
						close cur_mp_non_richieste_vern;
					else
						close cur_mp_non_richieste;
					end if	
	
					// ---------------------  selezione dell'asta in funzione dell'altezza comando -------------------------------------
	
					if ld_alt_asta > 0 then
						if ls_flag_verniciatura = "S" then
							declare cu_asta_vern cursor for
							SELECT tab_mp_non_richieste.cod_mp_non_richiesta,   
									tab_mp_non_richieste.prog_mp_non_richiesta
							 FROM tab_mp_non_richieste,   
									tab_mp_automatiche_mod_vern  
							WHERE ( tab_mp_automatiche_mod_vern.cod_azienda = tab_mp_non_richieste.cod_azienda ) and  
									( tab_mp_automatiche_mod_vern.cod_mp_non_richiesta = tab_mp_non_richieste.cod_mp_non_richiesta ) and  
									( tab_mp_automatiche_mod_vern.prog_mp_non_richiesta = tab_mp_non_richieste.prog_mp_non_richiesta ) and  
									( ( tab_mp_non_richieste.cod_azienda = :s_cs_xx.cod_azienda ) AND  
									(tab_mp_non_richieste.flag_asta = 'S') AND
									(tab_mp_non_richieste.altezza_comando = :ld_alt_asta) AND
									(tab_mp_non_richieste.flag_blocco = 'N' OR  
									(tab_mp_non_richieste.flag_blocco = 'S' AND  
									tab_mp_non_richieste.data_blocco > :ldt_oggi)) AND  
									tab_mp_automatiche_mod_vern.cod_modello = :str_conf_prodotto.cod_modello AND  
									tab_mp_automatiche_mod_vern.cod_verniciatura = :ls_cod_verniciatura )   ;
							open cu_asta_vern;
							if sqlca.sqlcode < 0 then
								as_messaggio = "Errore in apertura ricerca asta comando/verniciature~r~n DETTAGLIO: " + sqlca.sqlerrtext
								close cu_buffer_ord_ven_det;
								return -1
							end if
						else
							declare cu_asta cursor for
							SELECT cod_mp_non_richiesta,   
									 prog_mp_non_richiesta
							 FROM  tab_mp_non_richieste
							WHERE  ( ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
									 ( tab_mp_non_richieste.flag_asta = 'S') AND
									 ( tab_mp_non_richieste.altezza_comando = :ld_alt_asta ) AND
									 ( tab_mp_non_richieste.flag_blocco = 'N' OR  
									 ( tab_mp_non_richieste.flag_blocco = 'S' AND  
									 tab_mp_non_richieste.data_blocco > :ldt_oggi ) ) )   ;
							open cu_asta;
							if sqlca.sqlcode < 0 then
								as_messaggio = "Errore in apertura ricerca asta comando ~r~n DETTAGLIO: " + sqlca.sqlerrtext
								close cu_buffer_ord_ven_det;
								return -1
							end if
						end if
						do
							if ls_flag_verniciatura = "S" then
								fetch cu_asta_vern into :ls_cod_mp_non_richiesta, :ll_prog_mp_non_richiesta;
							else
								fetch cu_asta into :ls_cod_mp_non_richiesta, :ll_prog_mp_non_richiesta;
							end if
							if sqlca.sqlcode <> 0 then exit
							ll_cont ++
							istr_riepilogo.mp_automatiche[ll_cont].cod_mp_automatica = ls_cod_mp_non_richiesta
							istr_riepilogo.mp_automatiche[ll_cont].prog_mp_automatica = ll_prog_mp_non_richiesta
						loop 	while true
						if ls_flag_verniciatura = "S" then
							close cu_asta_vern;
						else
							close cu_asta;
						end if
					end if
	
					
					// ***************  creazione delle varianti e altre cose relative al configuratore******
					
					
					string ls_cod_gruppo_variante
					integer li_return, ll_index
					dec{4} ld_null, ld_quan_utilizzo, ld_quan_tecnica
					uo_conf_varianti uo_ins_varianti
					
					uo_ins_varianti = CREATE uo_conf_varianti
					uo_ins_varianti.is_gestione = "ORD_VEN"
					uo_ins_varianti.il_anno_registrazione = ll_anno_registrazione
					uo_ins_varianti.il_num_registrazione = ll_num_registrazione
					uo_ins_varianti.il_prog_riga = ll_prog_riga_ord_ven
					uo_ins_varianti.is_cod_modello = ls_cod_prodotto
					
					setnull(ld_null)
					ll_index = 0
					
					// **********************************
					// INIZIO Materie Prime Automatiche
					// **********************************
					//
					
					ll_num_mp = upperbound(istr_riepilogo.mp_automatiche)
					for ll_i = 1 to ll_num_mp
						if not(isnull(istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica)) and istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica <> "" and &
							not(isnull(istr_riepilogo.mp_automatiche[ll_i].prog_mp_automatica)) and istr_riepilogo.mp_automatiche[ll_i].prog_mp_automatica <> 0 then
							select cod_gruppo_variante,
									 quan_utilizzo,
									 quan_tecnica
							into   :ls_cod_gruppo_variante,
									 :ld_quan_utilizzo,
									 :ld_quan_tecnica
							from   tab_mp_non_richieste
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_mp_non_richiesta = :istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica and
									 prog_mp_non_richiesta = :istr_riepilogo.mp_automatiche[ll_i].prog_mp_automatica ;
						
							if sqlca.sqlcode <> 0 then
								as_messaggio = "Errore in ricerca materia prima automatica " + istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica + " non presente in tabella MP automatiche"
								destroy uo_ins_varianti
								close cu_buffer_ord_ven_det;
								return -1				
							end if
							
							if not(isnull(ls_cod_gruppo_variante)) and ls_cod_gruppo_variante <> "" then
								ll_index ++
								uo_ins_varianti.str_varianti[ll_index].cod_versione = ls_cod_versione
								uo_ins_varianti.str_varianti[ll_index].cod_gruppo_variante = ls_cod_gruppo_variante
								uo_ins_varianti.str_varianti[ll_index].cod_mp_variante = istr_riepilogo.mp_automatiche[ll_i].cod_mp_automatica
								uo_ins_varianti.str_varianti[ll_index].quan_utilizzo = ld_quan_utilizzo
								uo_ins_varianti.str_varianti[ll_index].quan_tecnica  = ld_quan_tecnica
								
							end if
						end if
					next
					
					// **********************************
					// INIZIO Comando
					// **********************************
					
					if not isnull(ls_cod_comando) and len(ls_cod_comando) > 0 then
						select cod_gruppo_variante
						into   :ls_cod_gruppo_variante
						from   tab_comandi
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_comando = :ls_cod_comando;
				
						if sqlca.sqlcode <> 0 then
							as_messaggio = "Errore in ricerca comando " + ls_cod_comando + "~r~n" + sqlca.sqlerrtext
							destroy uo_ins_varianti
							close cu_buffer_ord_ven_det;
							return -1				
						end if
				
						ll_index ++
						uo_ins_varianti.str_varianti[ll_index].cod_versione = ls_cod_versione
						uo_ins_varianti.str_varianti[ll_index].cod_gruppo_variante = ls_cod_gruppo_variante
						uo_ins_varianti.str_varianti[ll_index].cod_mp_variante = ls_cod_comando
						uo_ins_varianti.str_varianti[ll_index].quan_utilizzo = ld_null
						uo_ins_varianti.str_varianti[ll_index].quan_tecnica  = ld_null
					
					end if
					
					// **********************************
					// INIZIO Tessuto
					// **********************************
					
					if not(isnull(ls_cod_tessuto)) and ls_cod_tessuto <> "" and not(isnull(ls_cod_non_a_magazzino)) and ls_cod_non_a_magazzino <> "" then
						select cod_gruppo_variante
						into   :ls_cod_gruppo_variante
						from   tab_tessuti
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_tessuto = :ls_cod_tessuto and
								 cod_non_a_magazzino = :ls_cod_non_a_magazzino;
				
						if sqlca.sqlcode <> 0 then
							as_messaggio = "Errore in ricerca del gruppo variante tessuto in tabella tessuti"
							destroy uo_ins_varianti
							close cu_buffer_ord_ven_det;
							return -1				
						end if
						ll_index ++
						uo_ins_varianti.str_varianti[ll_index].cod_versione = ls_cod_versione
						uo_ins_varianti.str_varianti[ll_index].cod_gruppo_variante = ls_cod_gruppo_variante
						uo_ins_varianti.str_varianti[ll_index].cod_mp_variante = ls_cod_tessuto
						uo_ins_varianti.str_varianti[ll_index].quan_utilizzo = ld_null
						uo_ins_varianti.str_varianti[ll_index].quan_tecnica  = ld_null
						
					end if
					
					// **********************************
					// INIZIO Verniciatura
					// **********************************
					
					if not(isnull(ls_cod_verniciatura)) and ls_cod_verniciatura <> "" then
						select cod_gruppo_variante
						into   :ls_cod_gruppo_variante
						from   tab_verniciatura
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_verniciatura = :ls_cod_verniciatura;
				
						if sqlca.sqlcode <> 0 then
							as_messaggio = "Errore in ricerca del gruppo variante verniciatura in tabella verniciature"
							destroy uo_ins_varianti
							close cu_buffer_ord_ven_det;
							return -1
						end if
						
						ll_index ++
						uo_ins_varianti.str_varianti[ll_index].cod_versione = ls_cod_versione
						uo_ins_varianti.str_varianti[ll_index].cod_gruppo_variante = ls_cod_gruppo_variante
						uo_ins_varianti.str_varianti[ll_index].cod_mp_variante = ls_cod_verniciatura
						uo_ins_varianti.str_varianti[ll_index].quan_utilizzo = ld_null
						uo_ins_varianti.str_varianti[ll_index].quan_tecnica  = ld_null
					end if
					
					// **********************************
					// INIZIO Supporto
					// **********************************
					
					if not(isnull(ls_cod_tipo_supporto)) and ls_cod_tipo_supporto <> "" then
						select cod_gruppo_variante
						into   :ls_cod_gruppo_variante
						from   tab_tipi_supporto
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_tipo_supporto = :ls_cod_tipo_supporto;
					
						if sqlca.sqlcode <> 0 then
							as_messaggio = "Errore in ricerca del gruppo variante supporto in tabella supporti"
							destroy uo_ins_varianti
							close cu_buffer_ord_ven_det;
							return -1				
						end if
						ll_index ++
						uo_ins_varianti.str_varianti[ll_index].cod_versione = ls_cod_versione
						uo_ins_varianti.str_varianti[ll_index].cod_gruppo_variante = ls_cod_gruppo_variante
						uo_ins_varianti.str_varianti[ll_index].cod_mp_variante = ls_cod_tipo_supporto
						uo_ins_varianti.str_varianti[ll_index].quan_utilizzo = ld_null
						uo_ins_varianti.str_varianti[ll_index].quan_tecnica  = ld_null
					
					end if		
					
					// **********************************
					// FINE Supporto
					// **********************************
					
					// -------------------------------------------------------------------------------------------------------------
					// ELABORAZIONE DELLE VARIANTI con creazione delle righe in varianti_det_ord_ven
					// -------------------------------------------------------------------------------------------------------------
					
					if uo_ins_varianti.uof_ins_varianti(ls_cod_prodotto, ls_cod_versione, as_messaggio) <> 0 then
						close cu_buffer_ord_ven_det;
						return -1
					end if
					
					// -------------------------------------------------------------------------------------------------------------
					// VALORIZZO LE FORMULE NELLA DISTINTA NEI RAMMI SENZA VARIANTE
					// -------------------------------------------------------------------------------------------------------------
					string ls_errore_varianti
					
					li_return = uo_ins_varianti.uof_valorizza_formule(ls_cod_prodotto, ls_cod_versione, ref ls_errore_varianti)
					if li_return = -1 then
						as_messaggio = "Errore durante la valorizzazione delle formule della distinta base (uof_valorizza_formule)"
						destroy uo_ins_varianti
						close cu_buffer_ord_ven_det;
						return -1				
					end if
					
					destroy uo_ins_varianti
					
				end if				
				
				
				// ***************  aggiornamento della quantità impegnata del prodotto finito in base al tipo dettaglio
				select flag_tipo_det_ven
				into   :ls_flag_tipo_det_ven
				from   tab_tipi_det_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
				if sqlca.sqlcode = 0 then
					if ls_flag_tipo_det_ven = "M" then
						
						update anag_prodotti  
							set quan_impegnata = quan_impegnata + :ld_quan_ordine
						 where cod_azienda = :s_cs_xx.cod_azienda and  
								 cod_prodotto = :str_conf_prodotto.cod_modello;
						if sqlca.sqlcode < 0 then
							as_messaggio = "Errore in fase di aggiornamento quantità impegnata del prodotto " +str_conf_prodotto.cod_modello + "~r~n" + sqlca.sqlerrtext
							close cu_buffer_ord_ven_det;
							return -1
						end if
						
						// enme 08/1/2006 gestione prodotto raggruppato
						setnull(ls_cod_prodotto_raggruppato)
						
						select cod_prodotto_raggruppato
						into   :ls_cod_prodotto_raggruppato
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :str_conf_prodotto.cod_modello;
								 
						if not isnull(ls_cod_prodotto_raggruppato) then
							
							ld_quan_raggruppo = f_converti_qta_raggruppo(str_conf_prodotto.cod_modello, ld_quan_ordine, "M")
				
							update anag_prodotti  
								set quan_impegnata = quan_impegnata + :ld_quan_raggruppo
							 where cod_azienda = :s_cs_xx.cod_azienda and  
									 cod_prodotto = :ls_cod_prodotto_raggruppato;
							if sqlca.sqlcode < 0 then
								as_messaggio = "Errore in fase di aggiornamento quantità impegnata del prodotto " +ls_cod_prodotto_raggruppato + "~r~n" + sqlca.sqlerrtext
								close cu_buffer_ord_ven_det;
								return -1
							end if
							
						end if
						
					end if
				end if
				
				// ***************  calcolo del prezzo  e minimali  (devo aver già inserito la riga in det_ord_ven e devo aver già creato le varianti)
				string ls_tabella
				dec{4} ld_provvigione_1,ld_provvigione_2
				double ld_prezzo_vendita,ld_prezzo_um
				ls_tabella = "varianti_det_ord_ven"

				s_cs_xx.listino_db.flag_calcola = "S"
				s_cs_xx.listino_db.cod_versione = ls_cod_versione
				s_cs_xx.listino_db.quan_prodotto_finito = ld_quan_ordine
				s_cs_xx.listino_db.anno_documento = ll_anno_registrazione
				s_cs_xx.listino_db.num_documento = ll_num_registrazione
				s_cs_xx.listino_db.prog_riga = ll_prog_riga_ord_ven
				s_cs_xx.listino_db.tipo_gestione = ls_tabella
				
				s_cs_xx.listino_db.cod_cliente = ls_cod_cliente
				s_cs_xx.listino_db.cod_valuta = ls_cod_valuta
				s_cs_xx.listino_db.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				s_cs_xx.listino_db.data_riferimento = ldt_oggi
				
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = s_cs_xx.listino_db.cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = s_cs_xx.listino_db.cod_valuta
				iuo_condizioni_cliente.str_parametri.data_riferimento = s_cs_xx.listino_db.data_riferimento
				iuo_condizioni_cliente.str_parametri.cod_cliente = s_cs_xx.listino_db.cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = ld_dim_x
				iuo_condizioni_cliente.str_parametri.dim_2 = ld_dim_y
				iuo_condizioni_cliente.str_parametri.quantita = s_cs_xx.listino_db.quan_prodotto_finito
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.colonna_quantita = ""
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = ""
				iuo_condizioni_cliente.ib_setitem = false
				iuo_condizioni_cliente.ib_setitem_provvigioni = false
				iuo_condizioni_cliente.wf_condizioni_cliente()
				
				for ll_i = 1 to 10
					if upperbound(iuo_condizioni_cliente.str_output.sconti) >= ll_i then
						istr_riepilogo.sconti[ll_i] = iuo_condizioni_cliente.str_output.sconti[ll_i]
					else
						istr_riepilogo.sconti[ll_i] = 0
					end if
				next
				
				ld_provvigione_1 = iuo_condizioni_cliente.str_output.provvigione_1
				ld_provvigione_2 = iuo_condizioni_cliente.str_output.provvigione_2
				if isnull(ld_provvigione_1) then ld_provvigione_1 = 0
				if isnull(ld_provvigione_2) then ld_provvigione_2 = 0
					
				
				if upperbound(iuo_condizioni_cliente.str_output.variazioni) < 1 or isnull(upperbound(iuo_condizioni_cliente.str_output.variazioni)) then
					ld_prezzo_vendita = 0
				else
					ld_prezzo_vendita = iuo_condizioni_cliente.str_output.variazioni[upperbound(iuo_condizioni_cliente.str_output.variazioni)]
				end if
				
				// ----------------------- gestione del prezzo per metro quadarato con il fattore di conversione ------------------------
				
				if lb_processi_configuratore then
				
					if ls_flag_prezzo_superficie = "S" then
						if ld_dim_x > 0 then
							if iuo_condizioni_cliente.str_output.min_fat_larghezza > 0 and not isnull(iuo_condizioni_cliente.str_output.min_fat_larghezza) and istr_riepilogo.dimensione_1 < iuo_condizioni_cliente.str_output.min_fat_larghezza then
								ld_dim_x = iuo_condizioni_cliente.str_output.min_fat_larghezza
							else
								ld_dim_x = istr_riepilogo.dimensione_1
							end if
						end if
						if ld_dim_y > 0 then
							if iuo_condizioni_cliente.str_output.min_fat_altezza > 0 and not isnull(iuo_condizioni_cliente.str_output.min_fat_altezza) and istr_riepilogo.dimensione_2 < iuo_condizioni_cliente.str_output.min_fat_altezza then
								ld_dim_y = iuo_condizioni_cliente.str_output.min_fat_altezza
							else
								ld_dim_y = istr_riepilogo.dimensione_2
							end if
						end if
						if ld_dim_z > 0 then
							if iuo_condizioni_cliente.str_output.min_fat_profondita > 0 and not isnull(iuo_condizioni_cliente.str_output.min_fat_profondita) and istr_riepilogo.dimensione_3 < iuo_condizioni_cliente.str_output.min_fat_profondita then
								ld_dim_z = iuo_condizioni_cliente.str_output.min_fat_profondita
							else
								ld_dim_z = istr_riepilogo.dimensione_3
							end if
						end if
						
						// --------------------- nuova gestione vuoto per pieno -------------------------
						//                         nicola ferrari 23/05/2000
						if ls_flag_uso_quan_distinta = "N" then
							ld_fat_conversione_ven = round((ld_dim_x * ld_dim_y) / 10000,2)
							if ld_fat_conversione_ven <= 0 or isnull(ld_fat_conversione_ven) then
								as_messaggio = "Attenzione la quantità risultante dal calcolo DIM1 x DIM2 è pari a zero o è nulla; verrà FORZATA 1, ma è necessario il controllo dell'operatore!"
								ld_fat_conversione_ven = 1
							end if
						else
							string ls_cod_prodotto_variante, ls_errore
							dec{4} ld_quan_prodotto_variante, ld_prezzo_temp
							
							uo_ins_varianti = CREATE uo_conf_varianti
							if uo_ins_varianti.uof_ricerca_quan_mp(ls_cod_prodotto,  &
																			ls_cod_versione, &
																			ld_quan_ordine, &
																			ls_cod_gruppo_var_quan_distinta, &
																			ll_anno_registrazione, &
																			ll_num_registrazione, &
																			ll_prog_riga_ord_ven, &
																			ls_tabella, &
																			ls_cod_prodotto_variante, &
																			ld_quan_prodotto_variante,&
																			ls_errore) = -1 then
								as_messaggio = "Errore nella ricerca quantità tessuto in distinta base. Dettaglio errore: " + ls_errore
								close cu_buffer_ord_ven_det;
								return -1
							end if
					
							if ld_quan_prodotto_variante <= 0 or isnull(ld_quan_prodotto_variante) then ld_quan_prodotto_variante = 1
							
							destroy uo_ins_varianti
							
							ld_fat_conversione_ven = round(ld_quan_prodotto_variante,2)
							
						end if
					
					//
						// ---------------------- fine vuoto per pieno ----------------------------------
						// controllo il minimale di superficie
						if iuo_condizioni_cliente.str_output.min_fat_superficie > 0 and not isnull(iuo_condizioni_cliente.str_output.min_fat_superficie) and ld_fat_conversione_ven < iuo_condizioni_cliente.str_output.min_fat_superficie then
							ld_fat_conversione_ven = iuo_condizioni_cliente.str_output.min_fat_superficie
						end if
						ld_quantita_um = ld_quan_ordine * ld_fat_conversione_ven
						ld_prezzo_um = ld_prezzo_vendita
						ld_prezzo_vendita = ld_prezzo_vendita * ld_fat_conversione_ven
					end if	
				
					if iuo_condizioni_cliente.uof_arrotonda_prezzo(ld_prezzo_vendita, ls_cod_valuta, ld_prezzo_vendita, as_messaggio) = -1 then
						close cu_buffer_ord_ven_det;
						return -1
					end if
					
				else // se il prodotto non è configurato allineo semplicemente il prezzo_um in base al fattore di conversione.
					dec{4} ld_prezzo_tondo,ld_precisione_prezzo_mag
					ld_prezzo_um = ld_prezzo_vendita / ld_fat_conversione_ven
					
					select precisione_prezzo_mag
					into   :ld_precisione_prezzo_mag
					from   con_vendite
					where  cod_azienda = :s_cs_xx.cod_azienda;

					
					ld_prezzo_tondo = ld_prezzo_um
					
					if ld_precisione_prezzo_mag <> 0 then
						// arrotondamento per difetto
						ld_prezzo_tondo = (truncate((ld_prezzo_um/ld_precisione_prezzo_mag), 0) * ld_precisione_prezzo_mag)
						// arrotondamento per eccesso
						if (ld_prezzo_um - ld_prezzo_tondo) > (ld_precisione_prezzo_mag / 2) then
							ld_prezzo_tondo = ld_prezzo_tondo + ld_precisione_prezzo_mag
						end if	
					end if	
					ld_prezzo_um = ld_prezzo_tondo
					
				end if				


				// --------------------------  eseguo update sulla riga di dettaglio con i valori ricalcolati --------------------
				
				for ll_i = 1 to 10
					if upperbound(istr_riepilogo.sconti) < ll_i then
						istr_riepilogo.sconti[ll_i] = 0
					end if
				next
				
				update det_ord_ven
				set 	prezzo_vendita = :ld_prezzo_vendita,
						prezzo_um = :ld_prezzo_um,
						quantita_um = :ld_quantita_um,
						fat_conversione_ven = :ld_fat_conversione_ven,
						sconto_1 = :istr_riepilogo.sconti[1] ,
						sconto_2 = :istr_riepilogo.sconti[2] ,
						sconto_3 = :istr_riepilogo.sconti[3] ,
						sconto_4 = :istr_riepilogo.sconti[4] ,
						sconto_5 = :istr_riepilogo.sconti[5] ,
						sconto_6 = :istr_riepilogo.sconti[6] ,
						sconto_7 = :istr_riepilogo.sconti[7] ,
						sconto_8 = :istr_riepilogo.sconti[8] ,
						sconto_9 = :istr_riepilogo.sconti[9] ,
						sconto_10 = :istr_riepilogo.sconti[10] ,
						provvigione_1 = :ld_provvigione_1,	 
						provvigione_2 = :ld_provvigione_2
				where cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :ll_anno_registrazione and
						num_registrazione = :ll_num_registrazione and
						prog_riga_ord_ven = :ll_prog_riga_ord_ven;
				if sqlca.sqlcode < 0 then
					as_messaggio = "Errore in update dati commerciali ordine " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " riga:" + string(ll_prog_riga_ord_ven) + "~r~n" + sqlca.sqlerrtext
					close cu_buffer_ord_ven_det;
					return -1
				end if
				
			loop
			
end choose

//Lettura stato ordine nelle tabelle di appoggio
if uof_stato_buffer_ord_ven(as_cod_reparto,al_anno_ord_ven,al_num_ord_ven,ls_testo,as_messaggio) <> 0 then
	as_messaggio = "Errore in lettura stato ordine in tabelle appoggio:~n" + as_messaggio
	return -1
end if

//Viene aggiornato l'elenco dei destinatari delle mail di notifica importazione
lb_trovato = false

//Per ogni destinatario si verifica se è già presente nell'elenco
for ll_for_mess = 1 to upperbound(istr_mail)
	if not isnull(istr_mail[ll_for_mess].email) and trim(istr_mail[ll_for_mess].email) = ls_email[2] then
		ll_dest = ll_for_mess
		lb_trovato = true
		exit
	end if
next

//Se il destinatario è nuovo lo si aggiunge all'elenco
if not lb_trovato then
	ll_dest = upperbound(istr_mail) + 1
	istr_mail[ll_dest].email = ls_email[2]
end if

//Si aggiunge al destinatario il relativo messaggio
ll_mess = upperbound(istr_mail[ll_dest].messaggi) + 1
istr_mail[ll_dest].messaggi[ll_mess].testo = ls_testo
setnull(istr_mail[ll_dest].messaggi[ll_mess].allegato)

update
	buffer_ord_ven
set
	flag_importato = 'S',
	data_ora_import = :ldt_today,
	cod_utente_import = :s_cs_xx.cod_utente
where
	cod_reparto = :as_cod_reparto and
	anno_registrazione = :al_anno_ord_ven and
	num_registrazione = :al_num_ord_ven;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento stato importazione su tabelle appoggio:~n" + sqlca.sqlerrtext
	return -1
end if

return 0
end function

on uo_passaggio_ordini.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_passaggio_ordini.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event destructor;long ll_i

destroy iuo_condizioni_cliente

//Prima di distruggere l'oggetto ci si assicura di essere scollegati da tutti i DB esterni
for ll_i = 1 to upperbound(istr_tran)
	if istr_tran[ll_i].connected then
		uof_disconnetti_db(ll_i)
	end if
next
end event

event constructor;iuo_condizioni_cliente = create uo_condizioni_cliente
end event

