﻿$PBExportHeader$uo_ricalcola_configurazione_ar.sru
forward
global type uo_ricalcola_configurazione_ar from nonvisualobject
end type
end forward

global type uo_ricalcola_configurazione_ar from nonvisualobject
end type
global uo_ricalcola_configurazione_ar uo_ricalcola_configurazione_ar

type variables
public n_tran i_tran_trasmittente, i_tran_ricevente

wstr_riepilogo istr_riepilogo
boolean ib_configurazione_incompleta
string is_messaggio_configurazione

// variabili che gestiscono il cambio di modello 
string is_cod_modello_finale,is_cod_prodotto_origine, is_des_prodotto

// variabili che gestiscono la presenza delle caratterische o meno del prodotto di DESTINAZIONE
private string is_flag_tessuto, is_flag_mantovana, is_flag_altezza_mantovana, is_flag_tessuto_mantovana, &
       is_flag_cordolo, is_flag_passamaneria, is_flag_verniciatura, is_flag_comando, is_flag_pos_comando, &
		 is_flag_alt_asta, is_flag_supporto, is_flag_luce_finita, is_flag_secondo_comando  

private dec{4} id_dim_x, id_dim_y, id_dim_z, id_dim_t, id_limite_max_dim_x, id_limite_max_dim_y, id_limite_max_dim_z, id_limite_max_dim_t, &
       id_limite_min_dim_x, id_limite_min_dim_y, id_limite_min_dim_z, id_limite_min_dim_t  

// variabili che gestiscono la presenza delle caratterische o meno del prodotto di ORIGINE
private string is_flag_tessuto_1,is_flag_mantovana_1,is_flag_altezza_mantovana_1,is_flag_tessuto_mantovana_1, &
       is_flag_cordolo_1,  is_flag_passamaneria_1, is_flag_verniciatura_1,is_flag_comando_1, is_flag_pos_comando_1,  &
		 is_flag_alt_asta_1, is_flag_supporto_1,  is_flag_luce_finita_1, is_flag_secondo_comando_1
private dec{4} id_limite_max_dim_x_1, id_limite_max_dim_y_1,id_limite_max_dim_z_1, id_limite_max_dim_t_1, id_limite_min_dim_x_1, &
       id_limite_min_dim_y_1, id_limite_min_dim_z_1, id_limite_min_dim_t_1

string is_messaggio_errore
end variables

forward prototypes
public function integer uof_carica_parametri_configurazione ()
public function integer uof_trasforma_prodotto ()
end prototypes

public function integer uof_carica_parametri_configurazione ();SELECT flag_tessuto,   
		flag_tipo_mantovana,   
		flag_altezza_mantovana,   
		flag_tessuto_mantovana,   
		flag_cordolo,   
		flag_passamaneria,   
		flag_verniciatura,   
		flag_comando,   
		flag_pos_comando,   
		flag_alt_asta,   
		flag_supporto,   
		flag_luce_finita,   
		limite_max_dim_x,   
		limite_max_dim_y,   
		limite_max_dim_z,   
		limite_max_dim_t,   
		limite_min_dim_x,   
		limite_min_dim_y,   
		limite_min_dim_z,   
		limite_min_dim_t,   
		flag_secondo_comando  
 INTO :is_flag_tessuto,   
		:is_flag_mantovana,   
		:is_flag_altezza_mantovana,   
		:is_flag_tessuto_mantovana,   
		:is_flag_cordolo,   
		:is_flag_passamaneria,   
		:is_flag_verniciatura,   
		:is_flag_comando,   
		:is_flag_pos_comando,   
		:is_flag_alt_asta,   
		:is_flag_supporto,   
		:is_flag_luce_finita,   
		:id_limite_max_dim_x,   
		:id_limite_max_dim_y,   
		:id_limite_max_dim_z,   
		:id_limite_max_dim_t,   
		:id_limite_min_dim_x,   
		:id_limite_min_dim_y,   
		:id_limite_min_dim_z,   
		:id_limite_min_dim_t,   
		:is_flag_secondo_comando  
 FROM tab_flags_configuratore
 WHERE cod_azienda = :s_cs_xx.cod_azienda and
       cod_modello = :is_cod_prodotto_origine
USING i_tran_trasmittente;

if i_tran_trasmittente.sqlcode <> 0 then
	is_messaggio_errore = "Errore in ricerca parametri configuratore.~r~nDettaglio: " + i_tran_trasmittente.sqlerrtext
	return -1
end if

SELECT flag_tessuto,   
		flag_tipo_mantovana,   
		flag_altezza_mantovana,   
		flag_tessuto_mantovana,   
		flag_cordolo,   
		flag_passamaneria,   
		flag_verniciatura,   
		flag_comando,   
		flag_pos_comando,   
		flag_alt_asta,   
		flag_supporto,   
		flag_luce_finita,   
		limite_max_dim_x,   
		limite_max_dim_y,   
		limite_max_dim_z,   
		limite_max_dim_t,   
		limite_min_dim_x,   
		limite_min_dim_y,   
		limite_min_dim_z,   
		limite_min_dim_t,   
		flag_secondo_comando  
 INTO :is_flag_tessuto_1,   
		:is_flag_mantovana_1,   
		:is_flag_altezza_mantovana_1,   
		:is_flag_tessuto_mantovana_1,   
		:is_flag_cordolo_1,   
		:is_flag_passamaneria_1,   
		:is_flag_verniciatura_1,   
		:is_flag_comando_1,   
		:is_flag_pos_comando_1,   
		:is_flag_alt_asta_1,   
		:is_flag_supporto_1,   
		:is_flag_luce_finita_1,   
		:id_limite_max_dim_x_1,   
		:id_limite_max_dim_y_1,   
		:id_limite_max_dim_z_1,   
		:id_limite_max_dim_t_1,   
		:id_limite_min_dim_x_1,   
		:id_limite_min_dim_y_1,   
		:id_limite_min_dim_z_1,   
		:id_limite_min_dim_t_1,   
		:is_flag_secondo_comando_1
 FROM tab_flags_configuratore
 WHERE cod_azienda = :s_cs_xx.cod_azienda and
       cod_modello = :is_cod_modello_finale
USING i_tran_ricevente;
if i_tran_ricevente.sqlcode <> 0 then
	is_messaggio_errore = "Errore in ricerca parametri configuratore del prodotto di origine.~r~nDettaglio: " + i_tran_ricevente.sqlerrtext
	return -1
end if

select des_prodotto
into   :is_des_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :is_cod_modello_finale
using  i_tran_ricevente;
if i_tran_ricevente.sqlcode <> 0 then
	is_messaggio_errore = "Errore in ricerca descrizione prodotto ricevente.~r~nDettaglio: " + i_tran_ricevente.sqlerrtext
	return -1
end if

return 0
end function

public function integer uof_trasforma_prodotto ();if is_cod_modello_finale = is_cod_prodotto_origine then
	return 0
end if

if uof_carica_parametri_configurazione() < 0 then
	return -1
end if

//
//// *********************  pulisco i passi che adesso non sono più validi ****************************************

if is_flag_luce_finita_1 = "N" or isnull(is_flag_luce_finita_1) then
	istr_riepilogo.flag_misura_luce_finita = 'F'
end if

if is_flag_tessuto_1 = "N" or isnull(is_flag_tessuto_1) then
	setnull(istr_riepilogo.cod_tessuto)
	setnull(istr_riepilogo.cod_non_a_magazzino)
	istr_riepilogo.flag_fc_tessuto = "N"
	istr_riepilogo.flag_addizionale_tessuto = "N"
	istr_riepilogo.flag_tessuto_cliente = "N"
	istr_riepilogo.flag_tessuto_mant_cliente = "N"
end if

if is_flag_tessuto_mantovana_1 = "N" or isnull(is_flag_tessuto_mantovana_1) then
	setnull(istr_riepilogo.cod_tessuto_mant)
	setnull(istr_riepilogo.cod_mant_non_a_magazzino)
end if

if is_flag_altezza_mantovana_1 = "N" or isnull(is_flag_altezza_mantovana_1) then
	istr_riepilogo.alt_mantovana = 0
end if

if is_flag_mantovana_1 = "N" or isnull(is_flag_mantovana_1) then
	setnull(istr_riepilogo.flag_tipo_mantovana)
end if

if is_flag_cordolo_1 = "N" or isnull(is_flag_cordolo_1) then
	istr_riepilogo.flag_cordolo = "N"
	setnull(istr_riepilogo.colore_cordolo)
end if

if is_flag_passamaneria_1 = "N" or isnull(is_flag_passamaneria_1) then
	istr_riepilogo.flag_passamaneria = "N"
	setnull(istr_riepilogo.colore_passamaneria)
	
end if

if is_flag_verniciatura_1 = "N" or isnull(is_flag_verniciatura_1) then
	setnull(istr_riepilogo.cod_verniciatura)
	setnull(istr_riepilogo.des_vernice_fc)
	istr_riepilogo.flag_fc_vernic = "N"
	istr_riepilogo.flag_addizionale_vernic = "N"
end if

if is_flag_comando_1 = "N" or isnull(is_flag_comando_1) then
	setnull(istr_riepilogo.cod_comando)
	setnull(istr_riepilogo.des_comando_1)
	istr_riepilogo.flag_addizionale_comando_1 = "N"
	istr_riepilogo.flag_fc_comando_1 = "N"
end if

if is_flag_pos_comando_1 = "N" or isnull(is_flag_pos_comando_1) then
	setnull(istr_riepilogo.pos_comando)
end if

if is_flag_alt_asta_1 = "N" or isnull(is_flag_alt_asta_1) then
	istr_riepilogo.alt_asta = 0
end if

if is_flag_secondo_comando_1 = "N" or isnull(is_flag_secondo_comando_1) then
	setnull(istr_riepilogo.cod_comando_2)
	setnull(istr_riepilogo.des_comando_2)
	istr_riepilogo.flag_addizionale_comando_2 = "N"
	istr_riepilogo.flag_fc_comando_2 = "N"
end if

if is_flag_supporto_1 = "N" or isnull(is_flag_supporto_1) then
	setnull(istr_riepilogo.cod_tipo_supporto)
	istr_riepilogo.flag_addizionale_supporto = "N"
	istr_riepilogo.flag_fc_supporto = "N"
end if

// *********************  controllo se la configurazione è incompleta ********************************


ib_configurazione_incompleta = false
is_messaggio_configurazione = "Aggiungere i seguenti dati nel configuratore:"
if is_flag_luce_finita_1 = "N" and is_flag_luce_finita ="S" then 
	ib_configurazione_incompleta = true
	is_messaggio_configurazione = is_messaggio_configurazione + "~r~nMISURA LUCE"
end if
if is_flag_tessuto_1 = "N" and is_flag_tessuto ="S" then 
	ib_configurazione_incompleta = true
	is_messaggio_configurazione = is_messaggio_configurazione + "~r~nCODICE TESSUTO"
end if
if is_flag_mantovana_1 = "N" and is_flag_mantovana ="S" then
	ib_configurazione_incompleta = true
	is_messaggio_configurazione = is_messaggio_configurazione + "~r~nTIPO MANTOVANA"
end if
if is_flag_cordolo_1 = "N" and is_flag_cordolo ="S" then 
	ib_configurazione_incompleta = true
	is_messaggio_configurazione = is_messaggio_configurazione + "~r~nCOLORE CORDOLO"
end if
if is_flag_passamaneria_1 = "N" and is_flag_passamaneria ="S" then 
	ib_configurazione_incompleta = true
	is_messaggio_configurazione = is_messaggio_configurazione + "~r~nCOLORE PASSAMANERIA"
end if
if is_flag_altezza_mantovana_1 = "N" and is_flag_altezza_mantovana ="S" then 
	ib_configurazione_incompleta = true
	is_messaggio_configurazione = is_messaggio_configurazione + "~r~nALTEZZA MANTOVANA"
end if
if is_flag_tessuto_mantovana_1 = "N" and is_flag_tessuto_mantovana ="S" then 
	ib_configurazione_incompleta = true
	is_messaggio_configurazione = is_messaggio_configurazione + "~r~nCODICE TESSUTO MANTOVANA"
end if
if is_flag_verniciatura_1 = "N" and is_flag_verniciatura ="S" then 
	ib_configurazione_incompleta = true
	is_messaggio_configurazione = is_messaggio_configurazione + "~r~nCODICE VERNICIATURA"
end if
if is_flag_comando_1 = "N" and is_flag_comando ="S" then 
	ib_configurazione_incompleta = true
	is_messaggio_configurazione = is_messaggio_configurazione + "~r~nCODICE PRIMO COMANDO"
end if
if is_flag_alt_asta_1 = "N" and is_flag_alt_asta ="S" then 
	ib_configurazione_incompleta = true
	is_messaggio_configurazione = is_messaggio_configurazione + "~r~nCODICE SECONDO"
end if
if is_flag_supporto_1 = "N" and is_flag_supporto ="S" then 
	ib_configurazione_incompleta = true
	is_messaggio_configurazione = is_messaggio_configurazione + "~r~nCODICE SUPPORTO"
end if

return 0
end function

on uo_ricalcola_configurazione_ar.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_ricalcola_configurazione_ar.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;//  		oggetto che permette di trasformare i prodotti da un modello ad un altro
//
//		Realizzata da Enrico 16/3/2004
//
//
//		Note sull'uso dell'oggetto.
//		1) caricare la variabile is_cod_modello_origine con il valore appropriato
//		2) caricare la variabile is_cod_modello_finale con il valore appropriato
//		3) caricare la struttura istr_riepilogo con tutte le variabili di configurazione necessarie
//		4) chiamare la funzione uof_trasforma_prodotto
//		5) testare il valore di ritorno della funzione
//				0 = tutto OK
//			  -1 = errore
//		6) Leggere le variabili	  
//		7) destroy oggetto
//			
//			
//			
end event

