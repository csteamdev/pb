﻿$PBExportHeader$w_exp_ord_ven.srw
$PBExportComments$Finestra Testata Generazione Commesse
forward
global type w_exp_ord_ven from w_cs_xx_principale
end type
type tab_folder from tab within w_exp_ord_ven
end type
type tabpage_ricerca from userobject within tab_folder
end type
type tabpage_ricerca from userobject within tab_folder
end type
type tabpage_elenco from userobject within tab_folder
end type
type tabpage_elenco from userobject within tab_folder
end type
type tab_folder from tab within w_exp_ord_ven
tabpage_ricerca tabpage_ricerca
tabpage_elenco tabpage_elenco
end type
type gb_stato_ric from groupbox within w_exp_ord_ven
end type
type st_stato_ric from statictext within w_exp_ord_ven
end type
type hpb_stato_ric from hprogressbar within w_exp_ord_ven
end type
type gb_ricerca from groupbox within w_exp_ord_ven
end type
type cb_annulla from commandbutton within w_exp_ord_ven
end type
type gb_operatori from groupbox within w_exp_ord_ven
end type
type st_ord_sel_num from statictext within w_exp_ord_ven
end type
type st_ord_sel from statictext within w_exp_ord_ven
end type
type gb_ordini from groupbox within w_exp_ord_ven
end type
type dw_exp_ord_ven_lista from datawindow within w_exp_ord_ven
end type
type cb_azzera_ord from commandbutton within w_exp_ord_ven
end type
type cb_tutti_ord from commandbutton within w_exp_ord_ven
end type
type cb_azzera_op from commandbutton within w_exp_ord_ven
end type
type cb_tutti_op from commandbutton within w_exp_ord_ven
end type
type gb_export from groupbox within w_exp_ord_ven
end type
type cb_export from commandbutton within w_exp_ord_ven
end type
type gb_stato_exp from groupbox within w_exp_ord_ven
end type
type st_stato_exp from statictext within w_exp_ord_ven
end type
type hpb_stato_exp from hprogressbar within w_exp_ord_ven
end type
type dw_exp_ord_ven_op from datawindow within w_exp_ord_ven
end type
type dw_exp_ord_ven_sel from datawindow within w_exp_ord_ven
end type
type cb_cerca from commandbutton within w_exp_ord_ven
end type
end forward

global type w_exp_ord_ven from w_cs_xx_principale
integer width = 3968
integer height = 1908
string title = "Esportazione Ordini di Vendita"
boolean maxbox = false
boolean resizable = false
tab_folder tab_folder
gb_stato_ric gb_stato_ric
st_stato_ric st_stato_ric
hpb_stato_ric hpb_stato_ric
gb_ricerca gb_ricerca
cb_annulla cb_annulla
gb_operatori gb_operatori
st_ord_sel_num st_ord_sel_num
st_ord_sel st_ord_sel
gb_ordini gb_ordini
dw_exp_ord_ven_lista dw_exp_ord_ven_lista
cb_azzera_ord cb_azzera_ord
cb_tutti_ord cb_tutti_ord
cb_azzera_op cb_azzera_op
cb_tutti_op cb_tutti_op
gb_export gb_export
cb_export cb_export
gb_stato_exp gb_stato_exp
st_stato_exp st_stato_exp
hpb_stato_exp hpb_stato_exp
dw_exp_ord_ven_op dw_exp_ord_ven_op
dw_exp_ord_ven_sel dw_exp_ord_ven_sel
cb_cerca cb_cerca
end type
global w_exp_ord_ven w_exp_ord_ven

type variables

end variables

forward prototypes
public function integer wf_carica_ordini ()
public function integer wf_carica_operatori ()
public function integer wf_export_ordini ()
end prototypes

public function integer wf_carica_ordini ();datetime ldt_cons_da, ldt_cons_a, ldt_reg_da, ldt_reg_a, ldt_cons, ldt_reg, ldt_pronto

long		ll_anno_reg, ll_num_reg_da, ll_num_reg_a, ll_i, ll_anno, ll_num, ll_riga, ll_return

string 	ls_sql, ls_errore, ls_cod_cliente, ls_flag_operatori, ls_elenco_op, ls_flag_blocco, &
			ls_rag_soc_1, ls_cod_tipo_ord_ven, ls_messaggio

datastore lds_lista_ordini

uo_passaggio_ordini luo_passaggio


dw_exp_ord_ven_sel.accepttext()

ll_anno_reg = dw_exp_ord_ven_sel.getitemnumber(1,"anno_registrazione")

ll_num_reg_da = dw_exp_ord_ven_sel.getitemnumber(1,"num_registrazione_inizio")

ll_num_reg_a = dw_exp_ord_ven_sel.getitemnumber(1,"num_registrazione_fine")

ldt_cons_da = dw_exp_ord_ven_sel.getitemdatetime(1,"data_consegna_inizio")

ldt_cons_a = dw_exp_ord_ven_sel.getitemdatetime(1,"data_consegna_fine")

ldt_reg_da = dw_exp_ord_ven_sel.getitemdatetime(1,"data_registrazione_inizio")

ldt_reg_a = dw_exp_ord_ven_sel.getitemdatetime(1,"data_registrazione_fine")

ls_cod_cliente = dw_exp_ord_ven_sel.getitemstring(1,"cod_cliente")

ls_flag_operatori = dw_exp_ord_ven_sel.getitemstring(1,"flag_operatori")

ls_flag_blocco = dw_exp_ord_ven_sel.getitemstring(1,"flag_blocco")

ls_sql = &
"select " + &
"	anno_registrazione, " + &
"	num_registrazione, " + &
"	data_registrazione, " + &
"	data_consegna, " + &
"	data_pronto, " + &
"	cod_cliente, " + &
"	cod_tipo_ord_ven " + &
"from " + &
"	tes_ord_ven " + &
"where " + &
"	cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"	flag_evasione = 'A' and " + &
"	flag_inviato = 'N' "

if not isnull(ll_anno_reg) then
	ls_sql += " and anno_registrazione = " + string(ll_anno_reg) + " "
end if

if not isnull(ll_num_reg_da) then
	ls_sql += " and num_registrazione >= " + string(ll_num_reg_da) + " "
end if

if not isnull(ll_num_reg_a) then
	ls_sql += " and num_registrazione <= " + string(ll_num_reg_a) + " "
end if

if not isnull(ldt_cons_da) then
	ls_sql += " and data_consegna >= '" + string(ldt_cons_da,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_cons_a) then
	ls_sql += " and data_consegna <= '" + string(ldt_cons_a,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_reg_da) then
	ls_sql += " and data_registrazione >= '" + string(ldt_reg_da,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_reg_a) then
	ls_sql += " and data_registrazione <= '" + string(ldt_reg_a,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ls_cod_cliente) then
	ls_sql += " and cod_cliente = '" + ls_cod_cliente + "' "
end if

if ls_flag_blocco <> "I" then
	ls_sql += " and flag_blocco = '" + ls_flag_blocco + "' "
end if

if ls_flag_operatori = "S" then
	
	ls_elenco_op = ""
	
	for ll_i = 1 to dw_exp_ord_ven_op.rowcount()
		if dw_exp_ord_ven_op.getitemstring(ll_i,"flag_selezionato") = "S" then
			if ls_elenco_op <> "" then
				ls_elenco_op += ","
			end if
			ls_elenco_op += "'" + dw_exp_ord_ven_op.getitemstring(ll_i,"cod_operatore") + "'"
		end if
	next
	
	ls_sql += " and cod_operatore in (" + ls_elenco_op + ") "
	
end if

ls_sql += " order by anno_registrazione ASC, num_registrazione ASC "

setnull(ls_errore)

ls_sql = sqlca.syntaxfromsql(ls_sql,"style(type=grid)",ls_errore)

rollback;

if ls_sql = "" and not isnull(ls_errore) then
	g_mb.messagebox("APICE",ls_errore,stopsign!)
	return -1
end if

lds_lista_ordini = create datastore

if lds_lista_ordini.create(ls_sql,ls_errore) = -1 then
	g_mb.messagebox("APICE",ls_errore,stopsign!)
	destroy lds_lista_ordini
	return -1
end if

if lds_lista_ordini.settransobject(sqlca) = -1 then
	g_mb.messagebox("APICE","Errore in impostazione transazione",stopsign!)
	destroy lds_lista_ordini
	return -1
end if

st_stato_ric.text = "lettura dati..."

hpb_stato_ric.position = 0

st_stato_exp.text = ""

hpb_stato_exp.position = 0

if lds_lista_ordini.retrieve() = -1 then
	g_mb.messagebox("APICE","Errore in lettura dati",stopsign!)
	destroy lds_lista_ordini
	return -1
end if

dw_exp_ord_ven_lista.reset()

luo_passaggio = create uo_passaggio_ordini

for ll_i = 1 to lds_lista_ordini.rowcount()
	
	ll_anno = lds_lista_ordini.getitemnumber(ll_i,"anno_registrazione")
	
	ll_num = lds_lista_ordini.getitemnumber(ll_i,"num_registrazione")
	
	st_stato_ric.text = string(ll_anno) + "/" + string(ll_num)
	
	hpb_stato_ric.position = round( ll_i * 100 / lds_lista_ordini.rowcount() , 0 )
	
	ldt_reg = lds_lista_ordini.getitemdatetime(ll_i,"data_registrazione")
	
	ldt_pronto = lds_lista_ordini.getitemdatetime(ll_i,"data_pronto")
	
	ldt_cons = lds_lista_ordini.getitemdatetime(ll_i,"data_consegna")
	
	ls_cod_cliente = lds_lista_ordini.getitemstring(ll_i,"cod_cliente")
	
	ls_cod_tipo_ord_ven = lds_lista_ordini.getitemstring(ll_i,"cod_tipo_ord_ven")
	
	ls_rag_soc_1 = f_des_tabella("anag_clienti","cod_cliente = '" + ls_cod_cliente + "'","rag_soc_1")
	
	ll_return =  luo_passaggio.uof_export_ord_ven(ll_anno,ll_num,false,ls_messaggio)
	
	choose case	ll_return
		case is < 0
			g_mb.messagebox("APICE","Errore in verifica reparti esterni ordine " + string(ll_anno) + "/" + &
						  string(ll_num) + ":~n" + ls_messaggio,stopsign!)
			destroy luo_passaggio
			destroy lds_lista_ordini
			return -1
		case 0
			continue
	end choose
	
	ll_riga = dw_exp_ord_ven_lista.insertrow(0)
	
	dw_exp_ord_ven_lista.setitem(ll_riga,"anno_registrazione",ll_anno)
	dw_exp_ord_ven_lista.setitem(ll_riga,"num_registrazione",ll_num)
	dw_exp_ord_ven_lista.setitem(ll_riga,"flag_conferma","S")
	dw_exp_ord_ven_lista.setitem(ll_riga,"data_registrazione",ldt_reg)
	dw_exp_ord_ven_lista.setitem(ll_riga,"data_consegna",ldt_cons)
	dw_exp_ord_ven_lista.setitem(ll_riga,"data_pronto",ldt_pronto)
	dw_exp_ord_ven_lista.setitem(ll_riga,"cod_cliente",ls_cod_cliente)
	dw_exp_ord_ven_lista.setitem(ll_riga,"rag_soc_1",ls_rag_soc_1)
	dw_exp_ord_ven_lista.setitem(ll_riga,"cod_tipo_ord_ven",ls_cod_tipo_ord_ven)
	dw_exp_ord_ven_lista.setitem(ll_riga,"flag_inviato","N")
	
next

destroy luo_passaggio

destroy lds_lista_ordini

st_stato_ric.text = string(dw_exp_ord_ven_lista.rowcount())

if st_stato_ric.text = "1" then
	st_stato_ric.text += " ordine trovato"
else
	st_stato_ric.text += " ordini trovati"
end if

st_ord_sel_num.text = string(dw_exp_ord_ven_lista.rowcount())

hpb_stato_ric.position = 0

return dw_exp_ord_ven_lista.rowcount()
end function

public function integer wf_carica_operatori ();long	 ll_riga

string ls_cod_operatore, ls_des_operatore


declare operatori cursor for
select
	cod_operatore,
	des_operatore
from
	tab_operatori
where
	cod_azienda = :s_cs_xx.cod_azienda;

open operatori;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in open cursore operatori: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

do while true
	
	fetch
		operatori
	into
		:ls_cod_operatore,
		:ls_des_operatore;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in fetch cursore operatori: " + sqlca.sqlerrtext,stopsign!)
		close operatori;
		return -1
	elseif sqlca.sqlcode = 100 then
		close operatori;
		exit
	end if
	
	ll_riga = dw_exp_ord_ven_op.insertrow(0)
	
	dw_exp_ord_ven_op.setitem(ll_riga,"cod_operatore",ls_cod_operatore)
	dw_exp_ord_ven_op.setitem(ll_riga,"des_operatore",ls_des_operatore)
	dw_exp_ord_ven_op.setitem(ll_riga,"flag_selezionato","I")

loop

return 0
end function

public function integer wf_export_ordini ();string ls_messaggio

long 	 ll_i, ll_anno, ll_num, ll_return

datetime ldt_oggi, ldt_30ggdopo

uo_passaggio_ordini luo_passaggio


ldt_oggi = datetime(today(), 00:00:00)
ldt_30ggdopo = datetime(relativedate(today(), 30), 00:00:00)

st_stato_exp.text = ""

hpb_stato_exp.position = 0

luo_passaggio = create uo_passaggio_ordini

for ll_i = 1 to dw_exp_ord_ven_lista.rowcount()
	
	if dw_exp_ord_ven_lista.getitemstring(ll_i,"flag_conferma") <> "S" then
		continue
	end if
	
	if dw_exp_ord_ven_lista.getitemdatetime(ll_i,"data_pronto") > ldt_30ggdopo or dw_exp_ord_ven_lista.getitemdatetime(ll_i,"data_pronto") < ldt_oggi then
		continue
	end if
	
	ll_anno = dw_exp_ord_ven_lista.getitemnumber(ll_i,"anno_registrazione")
	
	ll_num = dw_exp_ord_ven_lista.getitemnumber(ll_i,"num_registrazione")
	
	st_stato_exp.text = string(ll_anno) + "/" + string(ll_num)
	
	hpb_stato_exp.position = round( ll_i * 100 / dw_exp_ord_ven_lista.rowcount() , 0 )
	
	if luo_passaggio.uof_export_ord_ven(ll_anno,ll_num,true,ls_messaggio) < 0 then
		g_mb.messagebox("APICE","Errore in esportazione ordine " + string(ll_anno) + "/" + &
						  string(ll_num) + ":~n" + ls_messaggio,stopsign!)
		rollback;
		luo_passaggio.uof_rollback(ls_messaggio)
		destroy luo_passaggio
		return -1
	end if
	
	dw_exp_ord_ven_lista.setitem(ll_i,"flag_inviato","S")
	
next

if luo_passaggio.uof_invia_mail(ls_messaggio) <> 0 then
	g_mb.messagebox("APICE","Errore in invio notifica esportazione:~n" + ls_messaggio,stopsign!)
	rollback;
	luo_passaggio.uof_rollback(ls_messaggio)
	destroy luo_passaggio
	return -1
end if

if luo_passaggio.uof_commit(ls_messaggio) <> 0 then
	g_mb.messagebox("APICE","Errore in commit modifiche:~n" + ls_messaggio,stopsign!)
	rollback;
	luo_passaggio.uof_rollback(ls_messaggio)
	return -1
end if

commit;

destroy luo_passaggio

st_stato_exp.text = ""

hpb_stato_exp.position = 0

return 0
end function

on w_exp_ord_ven.create
int iCurrent
call super::create
this.tab_folder=create tab_folder
this.gb_stato_ric=create gb_stato_ric
this.st_stato_ric=create st_stato_ric
this.hpb_stato_ric=create hpb_stato_ric
this.gb_ricerca=create gb_ricerca
this.cb_annulla=create cb_annulla
this.gb_operatori=create gb_operatori
this.st_ord_sel_num=create st_ord_sel_num
this.st_ord_sel=create st_ord_sel
this.gb_ordini=create gb_ordini
this.dw_exp_ord_ven_lista=create dw_exp_ord_ven_lista
this.cb_azzera_ord=create cb_azzera_ord
this.cb_tutti_ord=create cb_tutti_ord
this.cb_azzera_op=create cb_azzera_op
this.cb_tutti_op=create cb_tutti_op
this.gb_export=create gb_export
this.cb_export=create cb_export
this.gb_stato_exp=create gb_stato_exp
this.st_stato_exp=create st_stato_exp
this.hpb_stato_exp=create hpb_stato_exp
this.dw_exp_ord_ven_op=create dw_exp_ord_ven_op
this.dw_exp_ord_ven_sel=create dw_exp_ord_ven_sel
this.cb_cerca=create cb_cerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_folder
this.Control[iCurrent+2]=this.gb_stato_ric
this.Control[iCurrent+3]=this.st_stato_ric
this.Control[iCurrent+4]=this.hpb_stato_ric
this.Control[iCurrent+5]=this.gb_ricerca
this.Control[iCurrent+6]=this.cb_annulla
this.Control[iCurrent+7]=this.gb_operatori
this.Control[iCurrent+8]=this.st_ord_sel_num
this.Control[iCurrent+9]=this.st_ord_sel
this.Control[iCurrent+10]=this.gb_ordini
this.Control[iCurrent+11]=this.dw_exp_ord_ven_lista
this.Control[iCurrent+12]=this.cb_azzera_ord
this.Control[iCurrent+13]=this.cb_tutti_ord
this.Control[iCurrent+14]=this.cb_azzera_op
this.Control[iCurrent+15]=this.cb_tutti_op
this.Control[iCurrent+16]=this.gb_export
this.Control[iCurrent+17]=this.cb_export
this.Control[iCurrent+18]=this.gb_stato_exp
this.Control[iCurrent+19]=this.st_stato_exp
this.Control[iCurrent+20]=this.hpb_stato_exp
this.Control[iCurrent+21]=this.dw_exp_ord_ven_op
this.Control[iCurrent+22]=this.dw_exp_ord_ven_sel
this.Control[iCurrent+23]=this.cb_cerca
end on

on w_exp_ord_ven.destroy
call super::destroy
destroy(this.tab_folder)
destroy(this.gb_stato_ric)
destroy(this.st_stato_ric)
destroy(this.hpb_stato_ric)
destroy(this.gb_ricerca)
destroy(this.cb_annulla)
destroy(this.gb_operatori)
destroy(this.st_ord_sel_num)
destroy(this.st_ord_sel)
destroy(this.gb_ordini)
destroy(this.dw_exp_ord_ven_lista)
destroy(this.cb_azzera_ord)
destroy(this.cb_tutti_ord)
destroy(this.cb_azzera_op)
destroy(this.cb_tutti_op)
destroy(this.gb_export)
destroy(this.cb_export)
destroy(this.gb_stato_exp)
destroy(this.st_stato_exp)
destroy(this.hpb_stato_exp)
destroy(this.dw_exp_ord_ven_op)
destroy(this.dw_exp_ord_ven_sel)
destroy(this.cb_cerca)
end on

event pc_setwindow;call super::pc_setwindow;long ll_row

ll_row = dw_exp_ord_ven_sel.insertrow(0)

setpointer(hourglass!)

if wf_carica_operatori() <> 0 then
	postevent("close")
end if

dw_exp_ord_ven_sel.triggerevent("ue_imposta_data_inizio")
dw_exp_ord_ven_sel.triggerevent("ue_imposta_data_30gg")

setpointer(arrow!)
end event

type tab_folder from tab within w_exp_ord_ven
integer x = 23
integer y = 20
integer width = 3895
integer height = 1784
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean focusonbuttondown = true
boolean boldselectedtext = true
alignment alignment = center!
integer selectedtab = 1
tabpage_ricerca tabpage_ricerca
tabpage_elenco tabpage_elenco
end type

on tab_folder.create
this.tabpage_ricerca=create tabpage_ricerca
this.tabpage_elenco=create tabpage_elenco
this.Control[]={this.tabpage_ricerca,&
this.tabpage_elenco}
end on

on tab_folder.destroy
destroy(this.tabpage_ricerca)
destroy(this.tabpage_elenco)
end on

event selectionchanged;choose case newindex
		
	case 1
		
		dw_exp_ord_ven_sel.show()
		dw_exp_ord_ven_sel.object.b_ricerca_cliente.visible=true
		dw_exp_ord_ven_op.show()
		gb_operatori.show()
		cb_tutti_op.show()
		cb_azzera_op.show()
		gb_ricerca.show()
		cb_annulla.show()
		cb_cerca.show()
		gb_stato_ric.show()
		st_stato_ric.show()
		hpb_stato_ric.show()
		
		dw_exp_ord_ven_lista.hide()
		gb_ordini.hide()
		cb_azzera_ord.hide()
		cb_tutti_ord.hide()
		st_ord_sel.hide()
		st_ord_sel_num.hide()
		gb_export.hide()
		cb_export.hide()
		gb_stato_exp.hide()
		st_stato_exp.hide()
		hpb_stato_exp.hide()
		
	case 2
		
		dw_exp_ord_ven_sel.hide()
dw_exp_ord_ven_sel.object.b_ricerca_cliente.visible=false
		dw_exp_ord_ven_op.hide()
		gb_operatori.hide()
		cb_tutti_op.hide()
		cb_azzera_op.hide()
		gb_ricerca.hide()
		cb_annulla.hide()
		cb_cerca.hide()
		gb_stato_ric.hide()
		st_stato_ric.hide()
		hpb_stato_ric.hide()
		
		dw_exp_ord_ven_lista.show()
		gb_ordini.show()
		cb_azzera_ord.show()
		cb_tutti_ord.show()
		st_ord_sel.show()
		st_ord_sel_num.show()
		gb_export.show()
		cb_export.show()
		gb_stato_exp.show()
		st_stato_exp.show()
		hpb_stato_exp.show()
		
end choose
end event

type tabpage_ricerca from userobject within tab_folder
integer x = 18
integer y = 108
integer width = 3858
integer height = 1660
long backcolor = 12632256
string text = "Ricerca"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type tabpage_elenco from userobject within tab_folder
integer x = 18
integer y = 108
integer width = 3858
integer height = 1660
boolean enabled = false
long backcolor = 12632256
string text = "Elenco Ordini"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type gb_stato_ric from groupbox within w_exp_ord_ven
integer x = 1783
integer y = 1580
integer width = 549
integer height = 200
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Stato Ricerca"
end type

type st_stato_ric from statictext within w_exp_ord_ven
integer x = 1806
integer y = 1652
integer width = 503
integer height = 44
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean focusrectangle = false
end type

type hpb_stato_ric from hprogressbar within w_exp_ord_ven
integer x = 1806
integer y = 1700
integer width = 503
integer height = 40
unsignedinteger maxposition = 100
integer setstep = 1
end type

type gb_ricerca from groupbox within w_exp_ord_ven
integer x = 914
integer y = 1580
integer width = 846
integer height = 200
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ricerca Ordini"
end type

type cb_annulla from commandbutton within w_exp_ord_ven
integer x = 960
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string 	ls_null

datetime ldt_null

long 		ll_i, ll_null


setnull(ll_null)

setnull(ls_null)

setnull(ldt_null)

dw_exp_ord_ven_sel.setitem(1,"anno_registrazione",ll_null)
dw_exp_ord_ven_sel.setitem(1,"num_registrazione_inizio",ll_null)
dw_exp_ord_ven_sel.setitem(1,"num_registrazione_fine",ll_null)
dw_exp_ord_ven_sel.setitem(1,"data_consegna_inizio",ldt_null)
dw_exp_ord_ven_sel.setitem(1,"data_consegna_fine",ldt_null)
dw_exp_ord_ven_sel.setitem(1,"data_registrazione_inizio",ldt_null)
dw_exp_ord_ven_sel.setitem(1,"data_registrazione_fine",ldt_null)
dw_exp_ord_ven_sel.setitem(1,"cod_cliente",ls_null)
dw_exp_ord_ven_sel.setitem(1,"flag_operatori","N")
dw_exp_ord_ven_sel.setitem(1,"flag_blocco","N")

cb_tutti_op.enabled = false

cb_azzera_op.enabled = false

for ll_i = 1 to dw_exp_ord_ven_op.rowcount()
	dw_exp_ord_ven_op.setitem(ll_i,"flag_selezionato","I")
next
end event

type gb_operatori from groupbox within w_exp_ord_ven
integer x = 46
integer y = 1580
integer width = 846
integer height = 200
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Elenco Operatori"
end type

type st_ord_sel_num from statictext within w_exp_ord_ven
integer x = 1189
integer y = 1672
integer width = 389
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type st_ord_sel from statictext within w_exp_ord_ven
integer x = 869
integer y = 1672
integer width = 343
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Selezionati:"
alignment alignment = center!
boolean focusrectangle = false
end type

type gb_ordini from groupbox within w_exp_ord_ven
integer x = 46
integer y = 1580
integer width = 1577
integer height = 200
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Elenco Ordini"
end type

type dw_exp_ord_ven_lista from datawindow within w_exp_ord_ven
integer x = 46
integer y = 140
integer width = 3840
integer height = 1420
integer taborder = 50
string dataobject = "d_exp_ord_ven_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;if dwo.name = "flag_conferma" then
	if data = "S" then
		st_ord_sel_num.text = string(long(st_ord_sel_num.text) + 1)
	else
		st_ord_sel_num.text = string(long(st_ord_sel_num.text) - 1)
	end if
end if
end event

type cb_azzera_ord from commandbutton within w_exp_ord_ven
integer x = 480
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Azzera Sel. "
end type

event clicked;long ll_i


for ll_i = 1 to dw_exp_ord_ven_lista.rowcount()
	dw_exp_ord_ven_lista.setitem(ll_i,"flag_conferma","N")
next

st_ord_sel_num.text = "0"
end event

type cb_tutti_ord from commandbutton within w_exp_ord_ven
integer x = 91
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sel. Tutti"
end type

event clicked;long ll_i


for ll_i = 1 to dw_exp_ord_ven_lista.rowcount()
	dw_exp_ord_ven_lista.setitem(ll_i,"flag_conferma","S")
next

st_ord_sel_num.text = string(dw_exp_ord_ven_lista.rowcount())
end event

type cb_azzera_op from commandbutton within w_exp_ord_ven
integer x = 480
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Azzera Sel."
end type

event clicked;long ll_i


for ll_i = 1 to dw_exp_ord_ven_op.rowcount()
	dw_exp_ord_ven_op.setitem(ll_i,"flag_selezionato","N")
next
end event

type cb_tutti_op from commandbutton within w_exp_ord_ven
integer x = 91
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Sel. Tutti"
end type

event clicked;long ll_i


for ll_i = 1 to dw_exp_ord_ven_op.rowcount()
	dw_exp_ord_ven_op.setitem(ll_i,"flag_selezionato","S")
next
end event

type gb_export from groupbox within w_exp_ord_ven
integer x = 1646
integer y = 1580
integer width = 457
integer height = 200
integer taborder = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Esportazione"
end type

type cb_export from commandbutton within w_exp_ord_ven
integer x = 1691
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esporta"
end type

event clicked;long ll_i


setpointer(hourglass!)

if wf_export_ordini() = 0 then
	for ll_i = dw_exp_ord_ven_lista.rowcount() to 1 step -1
		if dw_exp_ord_ven_lista.getitemstring(ll_i,"flag_inviato") = "S" then
			dw_exp_ord_ven_lista.deleterow(ll_i)
		end if
	next
else
	for ll_i = dw_exp_ord_ven_lista.rowcount() to 1 step -1
		if dw_exp_ord_ven_lista.getitemstring(ll_i,"flag_inviato") = "S" then
			dw_exp_ord_ven_lista.setitem(ll_i,"flag_inviato","N")
		end if
	next
end if

setpointer(arrow!)
end event

type gb_stato_exp from groupbox within w_exp_ord_ven
integer x = 2126
integer y = 1580
integer width = 1760
integer height = 200
integer taborder = 70
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Stato Esportazione"
end type

type st_stato_exp from statictext within w_exp_ord_ven
integer x = 2149
integer y = 1656
integer width = 1714
integer height = 44
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean focusrectangle = false
end type

type hpb_stato_exp from hprogressbar within w_exp_ord_ven
integer x = 2149
integer y = 1700
integer width = 1714
integer height = 40
unsignedinteger maxposition = 100
integer setstep = 1
end type

type dw_exp_ord_ven_op from datawindow within w_exp_ord_ven
integer x = 46
integer y = 520
integer width = 2286
integer height = 1040
integer taborder = 40
string dataobject = "d_exp_ord_ven_op"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event clicked;if dwo.name = "flag_selezionato" then
	choose case getitemstring(row,"flag_selezionato")
		case "S"
			setitem(row,"flag_selezionato","N")
		case "N"
			setitem(row,"flag_selezionato","S")
		case "I"
			return 0
	end choose
end if
end event

type dw_exp_ord_ven_sel from datawindow within w_exp_ord_ven
event ue_imposta_data_inizio ( )
event ue_imposta_data_30gg ( )
integer x = 46
integer y = 140
integer width = 2286
integer height = 360
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_exp_ord_ven_sel"
boolean livescroll = true
end type

event ue_imposta_data_inizio();// aggiunto da EnMe per ptenda specifica distinta_base_varianti 15/2/2007
datetime ldt_data_oggi

ldt_data_oggi = datetime(today(), 00:00:00)

setitem(getrow(), "data_registrazione_inizio", ldt_data_oggi)

end event

event ue_imposta_data_30gg();// aggiunto da EnMe per ptenda specifica distinta_base_varianti 15/2/2007
datetime ldt_data_oggi, ldt_data_30gg

ldt_data_oggi = datetime(today(), 00:00:00)
ldt_data_30gg = datetime(relativedate(date(ldt_data_oggi), 30), 00:00:00)

setitem(getrow(), "data_registrazione_fine", ldt_data_30gg)


end event

event itemchanged;datetime ldt_data_oggi, ldt_data_30gg, ldt_data
date     ld_data
			
if isvalid(dwo) then
	choose case dwo.name
		case "flag_operatori"
	
			if data = "S" then
				
				cb_tutti_op.enabled = true
				
				cb_azzera_op.enabled = true
				
				cb_tutti_op.triggerevent("clicked")
				
			else
				
				long ll_i
				
				cb_tutti_op.enabled = false
				
				cb_azzera_op.enabled = false
				
				for ll_i = 1 to dw_exp_ord_ven_op.rowcount()
					dw_exp_ord_ven_op.setitem(ll_i,"flag_selezionato","I")
				next
				
			end if
			
//		case "data_registrazione_inizio"
//			
//			// aggiunto da EnMe per ptenda specifica distinta_base_varianti 15/2/2007
//
//			ldt_data_oggi = datetime(today(), 00:00:00)
//			ldt_data_30gg = datetime(relativedate(date(ldt_data_oggi), 30), 00:00:00)
//
//			ld_data = date(left(data,10))
//			
//			ldt_data = datetime(ld_data, 00:00:00)
//			
//			if ldt_data < ldt_data_oggi then 
//				postevent("ue_imposta_data_inizio")
//				messagebox("APICE","Data Registrazione Inizio non può essere antecedente ad oggi",Stopsign!)
//				return 0
//			end if
//			
//			if ldt_data > ldt_data_30gg then
//				postevent("ue_imposta_data_inizio")
//				messagebox("APICE","Data Registrazione Inizio non può essere oltre 30gg da oggi.",Stopsign!)
//				return 0
//			end if
//			
//		case "data_registrazione_fine"
//			
//			// aggiunto da EnMe per ptenda specifica distinta_base_varianti 15/2/2007
//
//			ldt_data_oggi = datetime(today(), 00:00:00)
//			ldt_data_30gg = datetime(relativedate(date(ldt_data_oggi), 30), 00:00:00)
//
//			ld_data = date(left(data,10))
//			
//			ldt_data = datetime(ld_data, 00:00:00)
//			
//			if ldt_data < ldt_data_oggi then 
//				postevent("ue_imposta_data_30gg")
//				messagebox("APICE","Data Registrazione Fine non può essere antecedente ad oggi",Stopsign!)
//				return 0
//			end if
//			
//			if ldt_data > ldt_data_30gg then
//				postevent("ue_imposta_data_30gg")
//				messagebox("APICE","Data Registrazione Fine non può essere oltre 30gg da oggi.",Stopsign!)
//				return 0
//			end if
			
	end choose
end if
end event

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
setnull(s_cs_xx.parametri.parametro_uo_dw_search)
s_cs_xx.parametri.parametro_dw_1 = dw_exp_ord_ven_sel
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
s_cs_xx.parametri.parametro_tipo_ricerca = 2
end event

event buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_exp_ord_ven_sel,"cod_cliente")
end choose
end event

type cb_cerca from commandbutton within w_exp_ord_ven
integer x = 1349
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;setpointer(hourglass!)

tab_folder.tabpage_elenco.enabled = false

dw_exp_ord_ven_lista.setredraw(false)

if wf_carica_ordini() > 0 then
	tab_folder.tabpage_elenco.enabled = true
	tab_folder.selecttab(2)
end if

dw_exp_ord_ven_lista.setredraw(true)

setpointer(arrow!)
end event

