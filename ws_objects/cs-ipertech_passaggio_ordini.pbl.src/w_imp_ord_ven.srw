﻿$PBExportHeader$w_imp_ord_ven.srw
$PBExportComments$Finestra Testata Generazione Commesse
forward
global type w_imp_ord_ven from w_cs_xx_principale
end type
type dw_imp_ord_ven_note from uo_cs_xx_dw within w_imp_ord_ven
end type
type cb_aggiorna from commandbutton within w_imp_ord_ven
end type
type dw_folder from u_folder within w_imp_ord_ven
end type
type dw_imp_ord_ven_lista from uo_cs_xx_dw within w_imp_ord_ven
end type
type dw_imp_ord_ven_filtri from datawindow within w_imp_ord_ven
end type
type dw_imp_ord_ven_sole from uo_cs_xx_dw within w_imp_ord_ven
end type
type dw_imp_ord_ven_tecniche from uo_cs_xx_dw within w_imp_ord_ven
end type
type dw_imp_ord_ven_sfuso from uo_cs_xx_dw within w_imp_ord_ven
end type
type cb_conferma from commandbutton within w_imp_ord_ven
end type
end forward

global type w_imp_ord_ven from w_cs_xx_principale
integer x = 0
integer y = 0
integer width = 3346
integer height = 2180
string title = "Importazione Ordini di Vendita"
windowstate windowstate = maximized!
event ue_imposta ( )
dw_imp_ord_ven_note dw_imp_ord_ven_note
cb_aggiorna cb_aggiorna
dw_folder dw_folder
dw_imp_ord_ven_lista dw_imp_ord_ven_lista
dw_imp_ord_ven_filtri dw_imp_ord_ven_filtri
dw_imp_ord_ven_sole dw_imp_ord_ven_sole
dw_imp_ord_ven_tecniche dw_imp_ord_ven_tecniche
dw_imp_ord_ven_sfuso dw_imp_ord_ven_sfuso
cb_conferma cb_conferma
end type
global w_imp_ord_ven w_imp_ord_ven

forward prototypes
public function integer wf_imposta_colore (datawindow adw_dw)
public function integer wf_controllo_codici (datawindow adw_dw)
public function integer wf_conferma ()
end prototypes

event ue_imposta();dw_imp_ord_ven_filtri.move(23,20)

dw_imp_ord_ven_filtri.resize(1966,120)

cb_aggiorna.move(2011,40)

cb_conferma.move(2400,40)

dw_imp_ord_ven_lista.move(23,160)

dw_imp_ord_ven_lista.resize(width - 64,(height - 160 - 20 - 64) / 2 - 420)

dw_imp_ord_ven_note.move(23,dw_imp_ord_ven_lista.y + dw_imp_ord_ven_lista.height + 20)

dw_imp_ord_ven_note.resize(width - 64,400)

dw_imp_ord_ven_note.object.note_esito.width = dw_imp_ord_ven_note.width - 40

dw_folder.move(23,dw_imp_ord_ven_note.y + dw_imp_ord_ven_note.height + 20)

//dw_folder.resize(width - 64,(height - 160 - 20 - 64) / 2)
dw_folder.resize(width - 64,((height - 160 - 20 - 64) / 2)-50)

dw_imp_ord_ven_sfuso.move(46,dw_folder.y + 120)

dw_imp_ord_ven_sfuso.resize(dw_folder.width - 46,dw_folder.height - 140)

dw_imp_ord_ven_sole.move(46,dw_folder.y + 120)

dw_imp_ord_ven_sole.resize(dw_folder.width - 46,dw_folder.height - 140)

dw_imp_ord_ven_tecniche.move(46,dw_folder.y + 120)

dw_imp_ord_ven_tecniche.resize(dw_folder.width - 46,dw_folder.height - 140)
end event

public function integer wf_imposta_colore (datawindow adw_dw);long	 ll_i

string ls_campo, ls_modify, ls_error, ls_exp


for ll_i = 1 to long(adw_dw.object.datawindow.column.count)
		
	if adw_dw.describe("#" + string(ll_i) + ".visible") = "0" then
		continue
	end if
	
	ls_campo = adw_dw.describe("#" + string(ll_i) + ".name")
	
	if adw_dw <> dw_imp_ord_ven_lista then
	
		choose case ls_campo
	
			case "cod_prodotto","cod_tessuto","cod_tessuto_mant","cod_non_a_magazzino", &
				  "cod_mant_non_a_magazzino","pos_comando","cod_comando","cod_comando_2", &
				  "cod_tipo_supporto","cod_verniciatura","cod_misura"
				
				ls_modify = ls_campo + ".background.mode='0'"
				
				ls_error = adw_dw.modify(ls_modify)
				
				if len(ls_error) > 0 then
					g_mb.messagebox("APICE","Errore in modifica modalità sfondo " + ls_campo + ":~n" + ls_error)
					return -1
				end if
				
				ls_exp = "12632256 ~t " + ls_campo + "_color"
				
			case else
				
				continue
			
		end choose
		
	else
		
		if ls_campo = "anno_registrazione" or ls_campo = "num_registrazione" then
			continue
		end if
		
		ls_exp = "12632256 ~t if ( flag_lavorazioni_speciali = ~~'S~~' , rgb(255,0,255) , 12632256 )"
		
	end if
	
	ls_modify = ls_campo + ".background.color='" + ls_exp + "'"
	
	ls_error = adw_dw.modify(ls_modify)
			
	if len(ls_error) > 0 then
		g_mb.messagebox("APICE","Errore in modifica colore sfondo " + ls_campo + ":~n" + ls_error)
		return -1
	end if
	
next

return 0
end function

public function integer wf_controllo_codici (datawindow adw_dw);boolean lb_errori

string  ls_campo, ls_codice_dw, ls_codice_db, ls_sql, ls_where

long	  ll_i, ll_j, ll_color_det, ll_color_com, ll_color_err, ll_color_div, ll_color_ok, ll_color_set


lb_errori = false

ll_color_det = rgb(255,0,0)

ll_color_com = rgb(255,160,0)

ll_color_div = rgb(255,255,0)

ll_color_ok = 12632256

ls_where = "where cod_azienda = '" + s_cs_xx.cod_azienda + "'"

declare codice dynamic cursor for sqlsa;

for ll_i = 1 to adw_dw.rowcount()
	
	for ll_j = 1 to long(adw_dw.object.datawindow.column.count)
		
		if adw_dw.describe("#" + string(ll_j) + ".visible") = "0" then
			continue
		end if
		
		ls_campo = adw_dw.describe("#" + string(ll_j) + ".name")
		
		choose case ls_campo
	
			case "cod_prodotto","cod_tessuto","cod_tessuto_mant","cod_non_a_magazzino", &
				  "cod_mant_non_a_magazzino","pos_comando","cod_comando","cod_comando_2", &
				  "cod_tipo_supporto","cod_verniciatura","cod_misura"
				
				ls_codice_dw = adw_dw.getitemstring(ll_i,ll_j)
		
				if isnull(ls_codice_dw) then
					continue
				end if
				
			case else
				
				continue
			
		end choose
		
		choose case ls_campo
	
			case "cod_prodotto"
				
				ls_sql = "select des_prodotto from anag_prodotti " + ls_where + " and cod_prodotto = '" + ls_codice_dw + "'"
				
				ls_codice_dw = adw_dw.getitemstring(ll_i,"des_prodotto")
				
				ll_color_err = ll_color_det
				
			case "cod_tessuto","cod_tessuto_mant"

				ls_sql = "select cod_prodotto from anag_prodotti " + ls_where + " and cod_prodotto = '" + ls_codice_dw + "'"
				
				ll_color_err = ll_color_com
				
			case "cod_non_a_magazzino","cod_mant_non_a_magazzino"
				
				ls_sql = "select cod_colore_tessuto from tab_colori_tessuti " + ls_where + " and cod_colore_tessuto = '" + ls_codice_dw + "'"
				
				ll_color_err = ll_color_com
				
			case "pos_comando"
				
				ls_sql = "select posizione from tab_comandi_posizioni " + ls_where + " and cod_comando = "
				
				if isnull(adw_dw.getitemstring(ll_i,"cod_comando")) then
					ls_sql += "'XXXXXXXXXXXXXXXXX'"
				else
					ls_sql += "'" + adw_dw.getitemstring(ll_i,"cod_comando") + "'"
				end if
				
				ls_sql += " and posizione = '" + ls_codice_dw + "'"
				
				ll_color_err = ll_color_com
				
			case "cod_comando","cod_comando_2"
				
				ls_sql = "select cod_comando from tab_comandi " + ls_where + " and cod_comando = '" + ls_codice_dw + "'"
				
				ll_color_err = ll_color_com
				
			case "cod_tipo_supporto"
				
				ls_sql = "select cod_tipo_supporto from tab_tipi_supporto " + ls_where + " and cod_tipo_supporto = '" + ls_codice_dw + "'"
				
				ll_color_err = ll_color_com
				
			case "cod_verniciatura"
				
				ls_sql = "select cod_verniciatura from tab_verniciatura " + ls_where + " and cod_verniciatura = '" + ls_codice_dw + "'"
				
				ll_color_err = ll_color_com
				
			case "cod_misura"
				
				ls_sql = "select cod_misura from tab_misure " + ls_where + " and cod_misura = '" + ls_codice_dw + "'"
				
				ll_color_err = ll_color_det
			
		end choose
		
		prepare sqlsa from :ls_sql;
		
		open dynamic codice;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in open cursore codice: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		fetch codice into :ls_codice_db;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore in fetch cursore codice: " + sqlca.sqlerrtext,stopsign!)
			close codice;
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ls_codice_db) then
			lb_errori = true
			ll_color_set = ll_color_err
		elseif ls_codice_db <> ls_codice_dw then
			ll_color_set = ll_color_div
		else
			ll_color_set = ll_color_ok
		end if
		
		close codice;
		
		adw_dw.setitem(ll_i,ls_campo + "_color",ll_color_set)
		
		adw_dw.setitemstatus(ll_i,ls_campo + "_color",primary!,notmodified!)
		
	next
	
next

if lb_errori then
	return 100
else
	return 0
end if
end function

public function integer wf_conferma ();long 	 ll_i, ll_anno, ll_num

string ls_cod_reparto, ls_esito, ls_messaggio

uo_passaggio_ordini luo_passaggio


if g_mb.messagebox("APICE","Confermare tutti gli ordini in stato ACCETTATO o RESPINTO?~nATTENZIONE: " + &
				  "per gli ordini RESPINTI è necessaria la connesisone al reparto esterno",question!,yesno!,2) = 2 then
	return -1
end if

luo_passaggio = create uo_passaggio_ordini

for ll_i = 1 to dw_imp_ord_ven_lista.rowcount()
	
	dw_imp_ord_ven_lista.setrow(ll_i)
	
	dw_imp_ord_ven_lista.scrolltorow(ll_i)
	
	ls_cod_reparto = dw_imp_ord_ven_lista.getitemstring(ll_i,"cod_reparto")
	
	ll_anno = dw_imp_ord_ven_lista.getitemnumber(ll_i,"anno_registrazione")
	
	ll_num = dw_imp_ord_ven_lista.getitemnumber(ll_i,"num_registrazione")
	
	ls_esito = dw_imp_ord_ven_lista.getitemstring(ll_i,"flag_esito_import")
	
	choose case ls_esito
			
		case "S"
			
			continue
			
		case "R"
			
			if luo_passaggio.uof_import_ord_ven(ls_cod_reparto,ll_anno,ll_num,true,ls_messaggio) < 0 then
				g_mb.messagebox("APICE","Errore in rifiuto ordine " + string(ll_anno) + "/" + &
								  string(ll_num) + ":~n" + ls_messaggio,stopsign!)
				rollback;
				luo_passaggio.uof_rollback(ls_messaggio)
				destroy luo_passaggio
				return -1
			end if
			
		case "A"
			
			if dw_imp_ord_ven_lista.getitemstring(ll_i,"flag_errori") = "S" then
				if g_mb.messagebox("APICE","L'ordine alla riga " + string(ll_i) + " presenta delle incongruenze:~n" + &
							  "verificare i dati di dettaglio prima di procedere all'importazione!~n" + &
							  "Continuare ad elaborare gli altri ordini?",exclamation!,yesno!,2) = 2 then
					rollback;
					luo_passaggio.uof_rollback(ls_messaggio)
					destroy luo_passaggio
					return -1
				else
					continue
				end if
			end if
			
			if luo_passaggio.uof_import_ord_ven(ls_cod_reparto,ll_anno,ll_num,false,ls_messaggio) < 0 then
				g_mb.messagebox("APICE","Errore in importazione ordine " + string(ll_anno) + "/" + &
								  string(ll_num) + ":~n" + ls_messaggio,stopsign!)
				rollback;
				luo_passaggio.uof_rollback(ls_messaggio)
				destroy luo_passaggio
				return -1
			end if
			
	end choose
	
next

if luo_passaggio.uof_invia_mail(ls_messaggio) <> 0 then
	g_mb.messagebox("APICE","Errore in invio notifica importazione:~n" + ls_messaggio,stopsign!)
	rollback;
	luo_passaggio.uof_rollback(ls_messaggio)
	destroy luo_passaggio
	return -1
end if

if luo_passaggio.uof_commit(ls_messaggio) <> 0 then
	g_mb.messagebox("APICE","Errore in commit modifiche:~n" + ls_messaggio,stopsign!)
	rollback;
	luo_passaggio.uof_rollback(ls_messaggio)
	destroy luo_passaggio
	return -1
end if

commit;

destroy luo_passaggio

return 0
end function

on w_imp_ord_ven.create
int iCurrent
call super::create
this.dw_imp_ord_ven_note=create dw_imp_ord_ven_note
this.cb_aggiorna=create cb_aggiorna
this.dw_folder=create dw_folder
this.dw_imp_ord_ven_lista=create dw_imp_ord_ven_lista
this.dw_imp_ord_ven_filtri=create dw_imp_ord_ven_filtri
this.dw_imp_ord_ven_sole=create dw_imp_ord_ven_sole
this.dw_imp_ord_ven_tecniche=create dw_imp_ord_ven_tecniche
this.dw_imp_ord_ven_sfuso=create dw_imp_ord_ven_sfuso
this.cb_conferma=create cb_conferma
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_imp_ord_ven_note
this.Control[iCurrent+2]=this.cb_aggiorna
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_imp_ord_ven_lista
this.Control[iCurrent+5]=this.dw_imp_ord_ven_filtri
this.Control[iCurrent+6]=this.dw_imp_ord_ven_sole
this.Control[iCurrent+7]=this.dw_imp_ord_ven_tecniche
this.Control[iCurrent+8]=this.dw_imp_ord_ven_sfuso
this.Control[iCurrent+9]=this.cb_conferma
end on

on w_imp_ord_ven.destroy
call super::destroy
destroy(this.dw_imp_ord_ven_note)
destroy(this.cb_aggiorna)
destroy(this.dw_folder)
destroy(this.dw_imp_ord_ven_lista)
destroy(this.dw_imp_ord_ven_filtri)
destroy(this.dw_imp_ord_ven_sole)
destroy(this.dw_imp_ord_ven_tecniche)
destroy(this.dw_imp_ord_ven_sfuso)
destroy(this.cb_conferma)
end on

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]


set_w_options(c_noresizewin + c_noautominimize + c_noautosize + c_noautoposition)

event post ue_imposta()

dw_imp_ord_ven_filtri.insertrow(0)

iuo_dw_main = dw_imp_ord_ven_lista

wf_imposta_colore(dw_imp_ord_ven_lista)

wf_imposta_colore(dw_imp_ord_ven_sfuso)

wf_imposta_colore(dw_imp_ord_ven_sole)

wf_imposta_colore(dw_imp_ord_ven_tecniche)

dw_imp_ord_ven_lista.set_dw_options(sqlca, &
												pcca.null_object, &
												c_nonew + &
												c_nodelete, &
												c_nohighlightselected + &
												c_nocursorrowfocusrect + &
												c_viewmodecolorunchanged + &
												c_viewmodeborderunchanged + &
												c_inactivedwcolorunchanged + &
												c_noshowempty)

dw_imp_ord_ven_note.set_dw_options(sqlca, &
												dw_imp_ord_ven_lista,&
												c_nonew + &
												c_nodelete + &
												c_sharedata + c_scrollparent, &
												c_nohighlightselected + &
												c_nocursorrowfocusrect + &
												c_viewmodeborderunchanged + &
												c_inactivedwcolorunchanged + &
												c_noshowempty)

dw_imp_ord_ven_sfuso.set_dw_options(sqlca, &
												dw_imp_ord_ven_lista,&
												c_nonew + &
												c_nodelete + &
												c_scrollparent, &
												c_nohighlightselected + &
												c_nocursorrowfocusrect + &
												c_viewmodeborderunchanged + &
												c_viewmodecolorunchanged + &
												c_inactivedwcolorunchanged + &
												c_noshowempty)

dw_imp_ord_ven_sole.set_dw_options(sqlca, &
											  dw_imp_ord_ven_lista, &
											  c_nonew + &
											  c_nodelete + &
											  c_scrollparent, &
											  c_nohighlightselected + &
											  c_nocursorrowfocusrect + &
											  c_viewmodeborderunchanged + &
											  c_viewmodecolorunchanged + &
											  c_inactivedwcolorunchanged + &
											  c_noshowempty)

dw_imp_ord_ven_tecniche.set_dw_options(sqlca, &
													dw_imp_ord_ven_lista, &
													c_nonew + &
													c_nodelete + &
													c_scrollparent, &
													c_nohighlightselected + &
													c_nocursorrowfocusrect + &
													c_viewmodeborderunchanged + &
													c_viewmodecolorunchanged + &
													c_inactivedwcolorunchanged + &
													c_noshowempty)

l_objects[1] = dw_imp_ord_ven_sfuso
dw_folder.fu_assigntab(1,"Sfuso",l_objects[])

l_objects[1] = dw_imp_ord_ven_sole
dw_folder.fu_assigntab(2,"Tende da sole",l_objects[])

l_objects[1] = dw_imp_ord_ven_tecniche
dw_folder.fu_assigntab(3,"Tende tecniche",l_objects[])

dw_folder.fu_foldercreate(3,3)

dw_folder.fu_selecttab(1)
end event

event resize;call super::resize;event post ue_imposta()
end event

type dw_imp_ord_ven_note from uo_cs_xx_dw within w_imp_ord_ven
integer x = 23
integer y = 560
integer width = 3246
integer height = 400
integer taborder = 30
string dataobject = "d_imp_ord_ven_note"
end type

event retrieveend;call super::retrieveend;event post rowfocuschanged(getrow())
end event

event rowfocuschanged;call super::rowfocuschanged;if isnull(currentrow) or currentrow < 1 then
	return -1
end if

choose case getitemstring(currentrow,"flag_tipo_ordine")
	case "A"
		dw_folder.fu_selecttab(2)
	case "C"
		dw_folder.fu_selecttab(3)
	case "D"
		dw_folder.fu_selecttab(1)
end choose
end event

event pcd_modify;call super::pcd_modify;cb_conferma.enabled = false
end event

event pcd_new;call super::pcd_new;cb_conferma.enabled = false
end event

event pcd_retrieve;call super::pcd_retrieve;string ls_tipo_ord, ls_stato_ord


ls_tipo_ord = dw_imp_ord_ven_filtri.getitemstring(1,"tipo_ordine")

ls_stato_ord = dw_imp_ord_ven_filtri.getitemstring(1,"esito_import")

retrieve(ls_tipo_ord,ls_stato_ord)
end event

event pcd_save;call super::pcd_save;cb_conferma.enabled = true
end event

event pcd_view;call super::pcd_view;cb_conferma.enabled = true
end event

event ue_key;call super::ue_key;return -1
end event

type cb_aggiorna from commandbutton within w_imp_ord_ven
integer x = 2011
integer y = 40
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna"
end type

event clicked;dw_imp_ord_ven_lista.change_dw_current()

parent.postevent("pc_retrieve")
end event

type dw_folder from u_folder within w_imp_ord_ven
integer x = 23
integer y = 980
integer width = 3246
integer height = 1060
integer taborder = 30
end type

type dw_imp_ord_ven_lista from uo_cs_xx_dw within w_imp_ord_ven
integer x = 23
integer y = 160
integer width = 3246
integer height = 380
integer taborder = 20
string dataobject = "d_imp_ord_ven_lista"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanged;call super::rowfocuschanged;if isnull(currentrow) or currentrow < 1 then
	return -1
end if

choose case getitemstring(currentrow,"flag_tipo_ordine")
	case "A"
		dw_folder.fu_selecttab(2)
	case "C"
		dw_folder.fu_selecttab(3)
	case "D"
		dw_folder.fu_selecttab(1)
end choose
end event

event pcd_retrieve;call super::pcd_retrieve;string ls_tipo_ord, ls_stato_ord


ls_tipo_ord = dw_imp_ord_ven_filtri.getitemstring(1,"tipo_ordine")

ls_stato_ord = dw_imp_ord_ven_filtri.getitemstring(1,"esito_import")

retrieve(ls_tipo_ord,ls_stato_ord)
end event

event retrieveend;call super::retrieveend;event post rowfocuschanged(getrow())
end event

event pcd_modify;call super::pcd_modify;cb_conferma.enabled = false
end event

event pcd_new;call super::pcd_new;cb_conferma.enabled = false
end event

event pcd_view;call super::pcd_view;cb_conferma.enabled = true
end event

event pcd_save;call super::pcd_save;cb_conferma.enabled = true
end event

event itemchanged;call super::itemchanged;string		ls_str

long			ll_giorno

datetime	ldt_data_consegna, ldt_data_registrazione


choose case i_colname
		
	case "data_consegna"
		
		ls_str = left( i_coltext, 10)
		
		ldt_data_consegna = datetime( date( ls_str), 00:00:00)
		
		ldt_data_registrazione = this.getitemdatetime( this.getrow(), "data_ord_cliente")
		
		if ldt_data_consegna < ldt_data_registrazione then
			
			g_mb.messagebox("APICE","Inserire una data consegna maggiore o uguale alla data di registrazione !", stopsign!)
			
			return 1
			
		end if
		
		if ldt_data_consegna < datetime(today(),00:00:00) then
			
			g_mb.messagebox("APICE","Non è possibile inserire una data consegna retroattiva !", stopsign!)
			
			return 1
			
		end if
		
		if ldt_data_consegna > datetime(relativedate(today(),30),00:00:00) then
			
			g_mb.messagebox("APICE","ATTENZIONE: DATA NON CONGRUA; VERIFICARE!")
			
		end if
		
end choose
end event

type dw_imp_ord_ven_filtri from datawindow within w_imp_ord_ven
integer x = 23
integer y = 20
integer width = 1966
integer height = 120
integer taborder = 10
string dataobject = "d_imp_ord_ven_filtri"
boolean livescroll = true
end type

event itemchanged;parent.postevent("pc_retrieve")
end event

type dw_imp_ord_ven_sole from uo_cs_xx_dw within w_imp_ord_ven
boolean visible = false
integer x = 46
integer y = 1100
integer width = 3200
integer height = 920
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_imp_ord_ven_sole"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_reparto

long	 ll_anno, ll_num


ls_reparto = dw_imp_ord_ven_lista.getitemstring(dw_imp_ord_ven_lista.getrow(),"cod_reparto")

ll_anno = dw_imp_ord_ven_lista.getitemnumber(dw_imp_ord_ven_lista.getrow(),"anno_registrazione")

ll_num = dw_imp_ord_ven_lista.getitemnumber(dw_imp_ord_ven_lista.getrow(),"num_registrazione")

retrieve(ls_reparto,ll_anno,ll_num)
end event

event retrieveend;call super::retrieveend;if rowcount > 0 then
	if wf_controllo_codici(this) = 0 then
		dw_imp_ord_ven_lista.setitem(dw_imp_ord_ven_lista.getrow(),"flag_errori","N")
		dw_imp_ord_ven_lista.setitemstatus(dw_imp_ord_ven_lista.getrow(),"flag_errori",primary!,notmodified!)
	else
		dw_imp_ord_ven_lista.setitem(dw_imp_ord_ven_lista.getrow(),"flag_errori","S")
		dw_imp_ord_ven_lista.setitemstatus(dw_imp_ord_ven_lista.getrow(),"flag_errori",primary!,notmodified!)
	end if
end if
end event

type dw_imp_ord_ven_tecniche from uo_cs_xx_dw within w_imp_ord_ven
boolean visible = false
integer x = 46
integer y = 1100
integer width = 3200
integer height = 920
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_imp_ord_ven_tecniche"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_reparto

long	 ll_anno, ll_num


ls_reparto = dw_imp_ord_ven_lista.getitemstring(dw_imp_ord_ven_lista.getrow(),"cod_reparto")

ll_anno = dw_imp_ord_ven_lista.getitemnumber(dw_imp_ord_ven_lista.getrow(),"anno_registrazione")

ll_num = dw_imp_ord_ven_lista.getitemnumber(dw_imp_ord_ven_lista.getrow(),"num_registrazione")

retrieve(ls_reparto,ll_anno,ll_num)
end event

event retrieveend;call super::retrieveend;if rowcount > 0 then
	if wf_controllo_codici(this) = 0 then
		dw_imp_ord_ven_lista.setitem(dw_imp_ord_ven_lista.getrow(),"flag_errori","N")
		dw_imp_ord_ven_lista.setitemstatus(dw_imp_ord_ven_lista.getrow(),"flag_errori",primary!,notmodified!)
	else
		dw_imp_ord_ven_lista.setitem(dw_imp_ord_ven_lista.getrow(),"flag_errori","S")
		dw_imp_ord_ven_lista.setitemstatus(dw_imp_ord_ven_lista.getrow(),"flag_errori",primary!,notmodified!)
	end if
end if
end event

type dw_imp_ord_ven_sfuso from uo_cs_xx_dw within w_imp_ord_ven
boolean visible = false
integer x = 46
integer y = 1100
integer width = 3200
integer height = 920
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_imp_ord_ven_sfuso"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_reparto

long	 ll_anno, ll_num


ls_reparto = dw_imp_ord_ven_lista.getitemstring(dw_imp_ord_ven_lista.getrow(),"cod_reparto")

ll_anno = dw_imp_ord_ven_lista.getitemnumber(dw_imp_ord_ven_lista.getrow(),"anno_registrazione")

ll_num = dw_imp_ord_ven_lista.getitemnumber(dw_imp_ord_ven_lista.getrow(),"num_registrazione")

retrieve(ls_reparto,ll_anno,ll_num)
end event

event retrieveend;call super::retrieveend;if rowcount > 0 then
	if wf_controllo_codici(this) = 0 then
		dw_imp_ord_ven_lista.setitem(dw_imp_ord_ven_lista.getrow(),"flag_errori","N")
		dw_imp_ord_ven_lista.setitemstatus(dw_imp_ord_ven_lista.getrow(),"flag_errori",primary!,notmodified!)
	else
		dw_imp_ord_ven_lista.setitem(dw_imp_ord_ven_lista.getrow(),"flag_errori","S")
		dw_imp_ord_ven_lista.setitemstatus(dw_imp_ord_ven_lista.getrow(),"flag_errori",primary!,notmodified!)
	end if
end if
end event

type cb_conferma from commandbutton within w_imp_ord_ven
integer x = 2400
integer y = 40
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;setpointer(hourglass!)

if wf_conferma() = 0 then
	g_mb.messagebox("APICE","Operazione completata",information!)
else
	g_mb.messagebox("APICE","Operazione interrotta a causa di errori",stopsign!)
end if

cb_aggiorna.triggerevent("clicked")

setpointer(arrow!)
end event

