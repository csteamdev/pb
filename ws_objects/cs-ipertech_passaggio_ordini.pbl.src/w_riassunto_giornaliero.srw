﻿$PBExportHeader$w_riassunto_giornaliero.srw
forward
global type w_riassunto_giornaliero from w_cs_xx_principale
end type
type cb_azzera from commandbutton within w_riassunto_giornaliero
end type
type cb_tutti from commandbutton within w_riassunto_giornaliero
end type
type cb_chiudi from commandbutton within w_riassunto_giornaliero
end type
type dw_reparti from datawindow within w_riassunto_giornaliero
end type
type dw_sel from datawindow within w_riassunto_giornaliero
end type
type cb_ok from commandbutton within w_riassunto_giornaliero
end type
end forward

global type w_riassunto_giornaliero from w_cs_xx_principale
integer width = 1874
integer height = 1628
string title = "Riassunto Giornaliero"
boolean maxbox = false
boolean resizable = false
cb_azzera cb_azzera
cb_tutti cb_tutti
cb_chiudi cb_chiudi
dw_reparti dw_reparti
dw_sel dw_sel
cb_ok cb_ok
end type
global w_riassunto_giornaliero w_riassunto_giornaliero

on w_riassunto_giornaliero.create
int iCurrent
call super::create
this.cb_azzera=create cb_azzera
this.cb_tutti=create cb_tutti
this.cb_chiudi=create cb_chiudi
this.dw_reparti=create dw_reparti
this.dw_sel=create dw_sel
this.cb_ok=create cb_ok
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_azzera
this.Control[iCurrent+2]=this.cb_tutti
this.Control[iCurrent+3]=this.cb_chiudi
this.Control[iCurrent+4]=this.dw_reparti
this.Control[iCurrent+5]=this.dw_sel
this.Control[iCurrent+6]=this.cb_ok
end on

on w_riassunto_giornaliero.destroy
call super::destroy
destroy(this.cb_azzera)
destroy(this.cb_tutti)
destroy(this.cb_chiudi)
destroy(this.dw_reparti)
destroy(this.dw_sel)
destroy(this.cb_ok)
end on

event pc_setwindow;call super::pc_setwindow;dw_sel.insertrow(0)

dw_sel.setitem(1,"data_rif",datetime(today(),00:00:00))

dw_reparti.settransobject(sqlca)

dw_reparti.retrieve(s_cs_xx.cod_azienda)
end event

type cb_azzera from commandbutton within w_riassunto_giornaliero
integer x = 411
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Azzera"
end type

event clicked;long		ll_i

for ll_i = 1 to dw_reparti.rowcount()
	dw_reparti.setitem(ll_i,"selezione","N")
next
end event

type cb_tutti from commandbutton within w_riassunto_giornaliero
integer x = 23
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Tutti"
end type

event clicked;long		ll_i

for ll_i = 1 to dw_reparti.rowcount()
	dw_reparti.setitem(ll_i,"selezione","S")
next
end event

type cb_chiudi from commandbutton within w_riassunto_giornaliero
integer x = 1051
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi"
end type

event clicked;close(parent)
end event

type dw_reparti from datawindow within w_riassunto_giornaliero
integer x = 23
integer y = 160
integer width = 1783
integer height = 1240
integer taborder = 10
string dataobject = "d_riassunto_reparti"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_sel from datawindow within w_riassunto_giornaliero
integer x = 23
integer y = 20
integer width = 1783
integer height = 120
integer taborder = 30
string dataobject = "d_riassunto_sel"
boolean livescroll = true
end type

type cb_ok from commandbutton within w_riassunto_giornaliero
integer x = 1440
integer y = 1420
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;long		ll_i

datetime ldt_rif

boolean	lb_nulli

string	ls_cod_reparto, ls_messaggio

uo_passaggio_ordini luo_riassunto


ldt_rif = dw_sel.getitemdatetime(1,"data_rif")

if isnull(ldt_rif) then
	g_mb.messagebox("APICE","Impostare la data di riferimento!",stopsign!)
	return -1
end if

if dw_sel.getitemstring(1,"nulli") = "S" then
	lb_nulli = true
else
	lb_nulli = false
end if

setpointer(hourglass!)

luo_riassunto = create uo_passaggio_ordini

for ll_i = 1 to dw_reparti.rowcount()
	
	if dw_reparti.getitemstring(ll_i,"selezione") <> "S" then
		continue
	end if
	
	ls_cod_reparto = dw_reparti.getitemstring(ll_i,"cod_reparto")
	
	if luo_riassunto.uof_riassunto_giornaliero(ldt_rif,ls_cod_reparto,lb_nulli,ls_messaggio) <> 0 then
		g_mb.messagebox("APICE",ls_messaggio,stopsign!)
		exit
	end if
	
next

destroy luo_riassunto

setpointer(arrow!)

return 0
end event

