﻿$PBExportHeader$w_sblocca_ord_respinti.srw
forward
global type w_sblocca_ord_respinti from w_cs_xx_principale
end type
type rb_inviati from radiobutton within w_sblocca_ord_respinti
end type
type rb_respinti from radiobutton within w_sblocca_ord_respinti
end type
type st_1 from statictext within w_sblocca_ord_respinti
end type
type cb_sblocca from commandbutton within w_sblocca_ord_respinti
end type
type dw_sblocca_ord_respinti from uo_cs_xx_dw within w_sblocca_ord_respinti
end type
end forward

global type w_sblocca_ord_respinti from w_cs_xx_principale
integer width = 2715
integer height = 1448
string title = "Gestione Ordini Respinti"
boolean maxbox = false
boolean resizable = false
rb_inviati rb_inviati
rb_respinti rb_respinti
st_1 st_1
cb_sblocca cb_sblocca
dw_sblocca_ord_respinti dw_sblocca_ord_respinti
end type
global w_sblocca_ord_respinti w_sblocca_ord_respinti

type variables
string is_flag_respinto = "S"
end variables

on w_sblocca_ord_respinti.create
int iCurrent
call super::create
this.rb_inviati=create rb_inviati
this.rb_respinti=create rb_respinti
this.st_1=create st_1
this.cb_sblocca=create cb_sblocca
this.dw_sblocca_ord_respinti=create dw_sblocca_ord_respinti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.rb_inviati
this.Control[iCurrent+2]=this.rb_respinti
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.cb_sblocca
this.Control[iCurrent+5]=this.dw_sblocca_ord_respinti
end on

on w_sblocca_ord_respinti.destroy
call super::destroy
destroy(this.rb_inviati)
destroy(this.rb_respinti)
destroy(this.st_1)
destroy(this.cb_sblocca)
destroy(this.dw_sblocca_ord_respinti)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noenablepopup + c_closenosave)

iuo_dw_main = dw_sblocca_ord_respinti

dw_sblocca_ord_respinti.change_dw_current()

dw_sblocca_ord_respinti.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nodelete + c_nomodify,c_default)
end event

type rb_inviati from radiobutton within w_sblocca_ord_respinti
integer x = 1006
integer y = 20
integer width = 549
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "tutti gli ordini inviati"
boolean lefttext = true
borderstyle borderstyle = stylelowered!
end type

event clicked;is_flag_respinto = "%"

parent.postevent("pc_retrieve")
end event

type rb_respinti from radiobutton within w_sblocca_ord_respinti
integer x = 343
integer y = 20
integer width = 617
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "solo gli ordini respinti"
boolean checked = true
boolean lefttext = true
borderstyle borderstyle = stylelowered!
end type

event clicked;is_flag_respinto = "S"

parent.postevent("pc_retrieve")
end event

type st_1 from statictext within w_sblocca_ord_respinti
integer x = 23
integer y = 20
integer width = 297
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Visualizza:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_sblocca from commandbutton within w_sblocca_ord_respinti
integer x = 2309
integer y = 1260
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sblocca"
end type

event clicked;long ll_row, ll_anno, ll_num


ll_row = dw_sblocca_ord_respinti.getrow()

if isnull(ll_row) or ll_row < 1 then
	g_mb.messagebox("APICE","Selezionare un ordine prima di continuare!",exclamation!)
	return -1
end if

ll_anno = dw_sblocca_ord_respinti.getitemnumber(ll_row,"tes_ord_ven_anno_registrazione")

ll_num = dw_sblocca_ord_respinti.getitemnumber(ll_row,"tes_ord_ven_num_registrazione")

if isnull(ll_anno) or ll_anno < 1 or isnull(ll_num) or ll_num < 1 then
	g_mb.messagebox("APICE","Selezionare un ordine prima di continuare!",exclamation!)
	return -1
end if

if g_mb.messagebox("APICE","Sbloccare l'ordine selezionato e renderlo nuovamente disponibile per l'esportazione?",question!,yesno!,2) = 2 then
	return -1
end if

update
	tes_ord_ven
set
	flag_inviato = 'N',
	flag_respinto = 'N',
	data_ora_invio = null,
	cod_utente_invio = null,
	cod_reparto_invio = null
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno and
	num_registrazione = :ll_num;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in aggiornamento ordine: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

commit;

parent.postevent("pc_retrieve")
end event

type dw_sblocca_ord_respinti from uo_cs_xx_dw within w_sblocca_ord_respinti
integer x = 23
integer y = 100
integer width = 2651
integer height = 1140
integer taborder = 10
string dataobject = "d_sblocca_ord_respinti"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda,is_flag_respinto)
end event

