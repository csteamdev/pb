﻿$PBExportHeader$w_tab_trasformazione_ar.srw
forward
global type w_tab_trasformazione_ar from w_cs_xx_principale
end type
type dw_tab_trasformazione_ar from uo_cs_xx_dw within w_tab_trasformazione_ar
end type
end forward

global type w_tab_trasformazione_ar from w_cs_xx_principale
integer x = 667
integer y = 468
integer width = 3072
integer height = 1224
string title = "Trasformazione Prodotti"
dw_tab_trasformazione_ar dw_tab_trasformazione_ar
end type
global w_tab_trasformazione_ar w_tab_trasformazione_ar

event pc_setwindow;call super::pc_setwindow;dw_tab_trasformazione_ar.set_dw_key("cod_azienda")

dw_tab_trasformazione_ar.set_dw_options(sqlca, &
                                    pcca.null_object,&
                                    c_default, &
                                    c_default)


end event

on w_tab_trasformazione_ar.create
int iCurrent
call super::create
this.dw_tab_trasformazione_ar=create dw_tab_trasformazione_ar
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_trasformazione_ar
end on

on w_tab_trasformazione_ar.destroy
call super::destroy
destroy(this.dw_tab_trasformazione_ar)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tab_trasformazione_ar, &
                 "cod_prodotto_trasmittente", &
                 sqlca, &
                 "anag_prodotti", &
                 "cod_prodotto", &
                 "des_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_modelli')")


f_po_loaddddw_dw(dw_tab_trasformazione_ar, &
                 "cod_prodotto_ricevente", &
                 sqlca, &
                 "anag_prodotti", &
                 "cod_prodotto", &
                 "des_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
					  							s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_modelli')")

f_po_loaddddw_dw(dw_tab_trasformazione_ar, &
                 "cod_reparto", &
                 sqlca, &
                 "anag_reparti", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_reparto = 'E' ")

end event

type dw_tab_trasformazione_ar from uo_cs_xx_dw within w_tab_trasformazione_ar
integer x = 9
integer y = 12
integer width = 3013
integer height = 1092
integer taborder = 10
string dataobject = "d_tab_trasformazione_ar"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_anno_registrazione, ll_num_registrazione


ll_errore = retrieve(s_cs_xx.cod_azienda)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next

end event

event itemchanged;call super::itemchanged;string ls_null, ls_tipo_reparto, ls_tipo_trasmissione, ls_messaggio

setnull(ls_null)

choose case i_colname
	case "cod_reparto"
		if not isnull(i_coltext) and len(i_coltext) > 0 then
			
			// mi connetto al DB del reparto selezionato.
			
			select flag_tipo_reparto,
					 flag_tipo_trasmissione
			into   :ls_tipo_reparto,
					 :ls_tipo_trasmissione
			from   anag_reparti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_reparto = :i_coltext;
			
			if ls_tipo_reparto = "E" and ls_tipo_trasmissione = "C" then
				// se è stato selezionato un reparto esterno e se la connessione è diretta allora carico la DROP con i
				// tipi ordini del ricevente.
				uo_passaggio_ordini luo_passaggio_ordini
				luo_passaggio_ordini = CREATE uo_passaggio_ordini
				if luo_passaggio_ordini.uof_connetti_db(i_coltext, ref ls_messaggio) < 0 then
					g_mb.messagebox("APICE", "Impossibile connettersi con il database del ricevente~n" + ls_messaggio)
					return
				end if
			
				f_po_loaddddw_dw(dw_tab_trasformazione_ar, &
									  "cod_prodotto_ricevente", &
									  luo_passaggio_ordini.istr_tran[luo_passaggio_ordini.il_tran].tran, &
									  "anag_prodotti", &
									  "cod_prodotto", &
									  "des_prodotto", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cat_mer in (select cod_cat_mer from tab_cat_mer_tabelle where cod_azienda = '" +&
																s_cs_xx.cod_azienda + "' and nome_tabella = 'tab_modelli')")
				

				luo_passaggio_ordini.uof_disconnetti_db(luo_passaggio_ordini.il_tran)
				destroy luo_passaggio_ordini
			end if
		else
			setitem(getrow(),"cod_tipo_ord_ven_ricevente", ls_null)
		end if
end choose
end event

