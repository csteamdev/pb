﻿$PBExportHeader$uo_calendario_prod_new.sru
$PBExportComments$Motore di produzione di Ptenda
forward
global type uo_calendario_prod_new from nonvisualobject
end type
end forward

global type uo_calendario_prod_new from nonvisualobject
end type
global uo_calendario_prod_new uo_calendario_prod_new

type variables
private:
	dec{4}				id_minuti_sfuso = 0
	uo_log				iuo_log
	string					is_tipo_log_sistema = "CALPROD"
	
public:
	integer				ii_log_level = 3
	string					is_path_log = ""
	boolean				ib_log = false
	boolean				ib_rielaborato = true
	

end variables

forward prototypes
public function integer uof_trova_quan_variante (string as_cod_prodotto, string as_cod_versione, string as_cod_gruppo_variante, integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_cod_prodotto_variante, ref decimal ad_quan_prodotto_variante, ref string as_errore)
public function integer uof_riga_modello (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore)
public function integer uof_riga_sfuso (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore)
public function integer uof_elaborazione_righe_modello (datetime adt_data_reg_inizio, datetime adt_data_reg_fine, ref string as_errore)
public function integer uof_elaborazione_righe_sfuso (datetime adt_data_reg_inizio, datetime adt_data_reg_fine, ref string as_errore)
public function integer uof_aggiorna_data_elab (ref string as_errore)
public function integer uof_controlla_fine_fase (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_reparto, ref string as_errore)
public function integer uof_disimpegna_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore)
public function integer uof_get_data_pronto_reparto (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_reparto, ref datetime adt_data_pronto, ref string as_errore)
public function integer uof_disimpegna_calendario (integer ai_anno_ordine, long al_num_ordine, ref string as_errore)
public function integer uof_sposta_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_reparto, datetime adt_data_pronto_new, ref string as_errore)
public function integer uof_sposta_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore)
public function integer uof_sposta_calendario (integer ai_anno_ordine, long al_num_ordine, ref string as_errore)
public function integer uof_disimpegna_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_reparto, ref string as_errore)
public function integer uof_aggiorna_detordven_prod (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_reparto, decimal ad_minuti_reparto, decimal ad_minuti_effettivi, decimal ad_minuti_giro_rc, datetime adt_data_pronto, ref string as_errore)
public function integer uof_aggiorna_tabella_calendario (string as_cod_reparto, datetime adt_data_pronto, decimal ad_minuti_effettivi, decimal ad_minuti_giro_rc, ref string as_errore)
public function integer uof_prepara_tabella_temporanea (datetime adt_data_reg_inizio, datetime adt_data_reg_fine, ref string as_errore)
public function integer uof_salva_in_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_reparti[], datetime adt_date_pronto[], ref string as_errore)
public function integer uof_get_minuti_reparto (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_reparto, datetime adt_data_pronto, ref decimal ad_minuti_effettivi, ref decimal ad_minuti_giro_rc, ref string as_errore)
public subroutine uof_log (string as_tipo, string as_testo)
public function integer uof_tempo_reparto (string as_cod_reparto, string as_cod_prodotto, string as_cod_versione, ref decimal ad_tempo_reparto, ref boolean ab_quan_variante, ref string as_cod_gruppo_variante, ref string as_errore)
public function integer uof_tempo_reparto (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_reparti[], ref datetime ad_date_pronto[], ref decimal ad_tempo_reparto[], ref decimal ad_tempo_reparto_rc[], ref string as_errore)
public subroutine uof_log_sistema (string as_tipo_log, string as_testo)
public function integer uof_inizio ()
public subroutine uof_data_pronto_tradizionale (integer ai_anno_ordine, long al_num_ordine, ref datetime adt_data)
public function boolean uof_controlla_pianificazione (string as_riga, string as_cod_reparto, datetime adt_data_pronto)
public function integer uof_controlla_carico_reparto (string as_cod_reparto, datetime adt_data_pronto, ref string as_colore, ref string as_errore)
public function string uof_get_giorno_settimana ()
public subroutine uof_get_reparto_sfuso (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_cod_reparto, ref string as_cod_prodotto)
public function integer uof_reparti_da_fasi (integer ai_anno_ordine, long al_numero_ordine, long al_riga_ordine, ref string as_reparti[], ref datetime adt_date_pronto[], ref string as_errore)
public function integer uof_reparti_da_distinta (integer ai_anno_ordine, long al_numero_ordine, long al_riga_ordine, ref string as_reparti[], ref datetime adt_date_pronto[], ref string as_errore)
public function integer uof_cancella (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore)
public function integer uof_cancella (integer ai_anno_ordine, long al_num_ordine, ref string as_errore)
public function datetime uof_relativedatetime_cal (long al_gg, datetime adt_data_valore)
public function date uof_relativedate_cal (long al_gg, date add_data_valore)
public function datetime uof_relativedatetime_cal (string as_cod_reparto, long al_gg, datetime adt_data_valore)
public function date uof_relativedate_cal (string as_cod_reparto, long al_gg, date add_data_valore)
public function integer uof_dw_cal_produzione (boolean ab_esiste_riga_documento, integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_prodotto, string as_cod_versione, datetime adt_data_partenza, ref string as_rep_par[], ref datetime adt_date_part[], ref datawindow adw_trasferimenti, ref string as_errore)
public function integer uof_salva_in_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore)
public function string uof_leggi_ultima_elab ()
public function integer uof_imposta_varianti_prod_da_backup (long al_anno_documento, long al_num_documento, long al_prog_riga_documento, ref datastore ads_data_backup)
public function integer uof_aggiorna_varianti_e_calendario (datetime adt_data_partenza, long al_anno_documento, long al_num_documento, long al_singola_riga, ref string as_reparti[], ref string as_errore)
end prototypes

public function integer uof_trova_quan_variante (string as_cod_prodotto, string as_cod_versione, string as_cod_gruppo_variante, integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_cod_prodotto_variante, ref decimal ad_quan_prodotto_variante, ref string as_errore);uo_conf_varianti		luo_ins_varianti

luo_ins_varianti = CREATE uo_conf_varianti
if luo_ins_varianti.uof_ricerca_quan_mp(	as_cod_prodotto,  &
														as_cod_versione, &
														1, &
														as_cod_gruppo_variante, &
														ai_anno_ordine, &
														al_num_ordine, &
														al_riga_ordine, &
														"varianti_det_ord_ven", &
														as_cod_prodotto_variante, &
														ad_quan_prodotto_variante, &
														as_errore) = -1 						then
	destroy luo_ins_varianti
	return -1
end if


return 0
end function

public function integer uof_riga_modello (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore);dec{4}			ld_tempi_reparti[], ld_tempi_reparti_rc[], ld_minuti_reparto_previsti
integer			li_index
string				ls_reparti[], ls_riga
datetime			ldt_date_pronto[]


li_index = uof_tempo_reparto(ai_anno_ordine, al_num_ordine, al_riga_ordine, ls_reparti[], ldt_date_pronto[], ld_tempi_reparti[], ld_tempi_reparti_rc[], as_errore)
if li_index<0 then
	return -1
	
elseif li_index=1 then
	//riga saltata ....
	return 1
	
end if


for li_index = 1 to upperbound(ld_tempi_reparti)
	
	ls_riga = string(ai_anno_ordine)+"/"+string(al_num_ordine)+"/"+string(al_riga_ordine)
	
	//NOTA ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//se nella data pronto il reparto non è pianificato in ore/minuti previsti
	//allora salta il salvataggio del calendario e in det_ord_ven_prod e logga
	if not uof_controlla_pianificazione(ls_riga, ls_reparti[li_index], ldt_date_pronto[li_index]) then
		//salta il reparto. Log già scritto
		as_errore = ""
	
		continue
	end if
	
	//aggiornamento tab_cal_produzione -------------------------
	if uof_aggiorna_tabella_calendario(ls_reparti[li_index], ldt_date_pronto[li_index], ld_tempi_reparti[li_index], ld_tempi_reparti_rc[li_index], as_errore) < 0 then
		return -1
	end if
	
	//leggo i minuti totali pianificati per il reparto nel calendario produzione
	setnull(ld_minuti_reparto_previsti)
	
	select minuti_reparto
	into :ld_minuti_reparto_previsti
	from tab_cal_produzione
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_reparto = :ls_reparti[li_index] and
			data_giorno = :ldt_date_pronto[li_index];
	if isnull(ld_minuti_reparto_previsti) then ld_minuti_reparto_previsti = 0


	//aggiornamento det_ord_ven_prod ---------------------------
	if uof_aggiorna_detordven_prod(	ai_anno_ordine, al_num_ordine, al_riga_ordine, ls_reparti[li_index], &
												ld_minuti_reparto_previsti, ld_tempi_reparti[li_index], ld_tempi_reparti_rc[li_index], ldt_date_pronto[li_index], &
												as_errore) < 0  then
		return -1
	end if
next


return 0
end function

public function integer uof_riga_sfuso (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore);dec{4}			ld_tempo_reparto_rc, ld_minuti_reparto_previsti
integer			li_index, li_ret
string				ls_cod_reparto, ls_cod_giro_consegna, ls_flag_rc, ls_riga, ls_cod_prodotto
datetime			ldt_data_pronto


ls_riga = string(ai_anno_ordine)+""+string(al_num_ordine)+""+string(al_riga_ordine)

//per lo sfuso il tempo riga è fissato dal parametro aziendale MOS (minuti necessari riga ordine sfuso: id_minuti_sfuso)


//leggo il/i reparto/i associato/i alla riga ordine di sfuso
select d.cod_reparto_sfuso, d.data_pronto_sfuso, t.cod_giro_consegna
into	:ls_cod_reparto, :ldt_data_pronto, :ls_cod_giro_consegna
from det_ord_ven as d
join tes_ord_ven as t on t.cod_azienda=d.cod_azienda and
								t.anno_registrazione=d.anno_registrazione and
								t.num_registrazione=d.num_registrazione
where 	d.cod_azienda=:s_cs_xx.cod_azienda and
			d.anno_registrazione=:ai_anno_ordine and
			d.num_registrazione=:al_num_ordine and
			d.prog_riga_ord_ven=:al_riga_ordine;

if sqlca.sqlcode<0 then
	as_errore = "Errore lettura reparto e data pronto riga sfuso: "+sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_reparto) or ls_cod_reparto="" then
	as_errore = "Reparto non trovato in tabella det_ord_ven: provo a recuperare il reparto dalla tabella anagrafica prodotti"
	
	//--------------------------------------------------------------
	uof_log("warn", as_errore)
	as_errore = ""
	
	uof_get_reparto_sfuso(ai_anno_ordine, al_num_ordine, al_riga_ordine, ls_cod_reparto, ls_cod_prodotto)
	
	if isnull(ls_cod_reparto) or ls_cod_reparto="" then
		//salta riga
		as_errore = "Reparto non trovato neanche da anagrafica prodotti (riga "+ls_riga+" - prodotto: "+ls_cod_prodotto+")!"
		
		//poichè a ciascuna riga ordine può corrispondere un solo reparto, allora la riga ordine viene saltata dall'elaborazione
		return 1
	end if
	
	//--------------------------------------------------------------
//	//salta riga
//	return 1
end if


//a regime commenta questa chiamata o metti return nella funzione uof_data_pronto_tradizionale
//#################################################################
//uof_data_pronto_tradizionale(ai_anno_ordine, al_num_ordine, ldt_data_pronto)
//#################################################################

if isnull(ldt_data_pronto) or year(date(ldt_data_pronto))< 1950 then
	as_errore = "Data pronto reparto non trovata in tabella det_ord_ven!"
	//riga saltata
	return 1
end if


//se nella data pronto, il reparto non è pianificato salto
	
//NOTA ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//se nella data pronto il reparto non è pianificato in ore/minuti previti
//allora salta il salvataggio del calendario e in det_ord_ven_prod e logga
if not uof_controlla_pianificazione(ls_riga, ls_cod_reparto, ldt_data_pronto) then
	//salta il reparto. Log già scritto
	as_errore = ""
	
	//riga saltata
	return 1
end if




uof_log("info", "Elaborazione Reparto " + ls_cod_reparto)

ls_flag_rc = "N"
if not isnull(ls_cod_giro_consegna) and ls_cod_giro_consegna<>"" then
	select flag_rc
	into   :ls_flag_rc
	from   tes_giri_consegne
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_giro_consegna = :ls_cod_giro_consegna;
	if sqlca.sqlcode <> 0 then ls_flag_rc ="N"
end if

// se l'ordine appartiene ad giro RC allora incremento anche il tempo di RC
if ls_flag_rc = "S" then 
	ld_tempo_reparto_rc = id_minuti_sfuso
else
	ld_tempo_reparto_rc = 0
end if

//se per il reparto in questione esiste il lancio produzione ed il flag fine sessione o pronto complessivo è S allora escludi dal calcolo
li_ret = uof_controlla_fine_fase(ai_anno_ordine, al_num_ordine, al_riga_ordine, ls_cod_reparto, as_errore)
if li_ret=1 then
	//riga saltata
	return 1
end if


//aggiornamento tab_cal_produzione -------------------------
if uof_aggiorna_tabella_calendario(ls_cod_reparto, ldt_data_pronto, id_minuti_sfuso, ld_tempo_reparto_rc, as_errore) < 0 then
	return -1
end if


//leggo i minuti totali pianificati per il reparto nel calendario produzione
setnull(ld_minuti_reparto_previsti)

select minuti_reparto
into :ld_minuti_reparto_previsti
from tab_cal_produzione
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_reparto = :ls_cod_reparto and
		data_giorno = :ldt_data_pronto;
if isnull(ld_minuti_reparto_previsti) then ld_minuti_reparto_previsti = 0


//aggiornamento det_ord_ven_prod ---------------------------
if uof_aggiorna_detordven_prod(	ai_anno_ordine, al_num_ordine, al_riga_ordine, ls_cod_reparto, &
											ld_minuti_reparto_previsti, id_minuti_sfuso, ld_tempo_reparto_rc, ldt_data_pronto, &
											as_errore) < 0  then
	return -1
end if


return 0
end function

public function integer uof_elaborazione_righe_modello (datetime adt_data_reg_inizio, datetime adt_data_reg_fine, ref string as_errore);
integer			li_anno_ordine, li_ret
long				ll_num_ordine, ll_riga_ordine
string 			ls_riga_ordine


uof_log("warn", "Inizio preparazione cursore righe modelli ##################################################")


declare cu_modelli cursor for
	select 	d.anno_registrazione,
				d.num_registrazione,   
				d.prog_riga_ord_ven
	from 	det_ord_ven as d
	join tes_ord_ven as t on d.cod_azienda = t.cod_azienda and 
									d.anno_registrazione = t.anno_registrazione and
									d.num_registrazione = t.num_registrazione
	where  	d.cod_azienda=:s_cs_xx.cod_azienda and t.flag_blocco<>'S'  and d.flag_blocco<>'S'  and
				t.flag_evasione<>'E'  and d.flag_evasione<>'E' and d.cod_prodotto is not null and
				t.cod_tipo_ord_ven in ( 	select cod_tipo_ord_ven
												from tab_tipi_ord_ven
												where cod_azienda=:s_cs_xx.cod_azienda and 
												flag_usa_per_cal_prod='X' and
												flag_tipo_bcl in ('A','C')) and
				t.data_registrazione>=:adt_data_reg_inizio and t.data_registrazione<=:adt_data_reg_fine and d.num_riga_appartenenza=0
	order by d.anno_registrazione, d.num_registrazione, d.prog_riga_ord_ven;

open cu_modelli;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore 'cu_modelli'~r~n" + sqlca.sqlerrtext
	return -1
end if

uof_log("warn", "Inizio elaborazione cursore righe modelli")

do while true
	fetch cu_modelli into	:li_anno_ordine,
								:ll_num_ordine,
								:ll_riga_ordine;
	
	ls_riga_ordine = string(li_anno_ordine)+"/"+string(ll_num_ordine)+"/"+string(ll_riga_ordine)
	uof_log("log", "Elaborazione riga " + ls_riga_ordine)
	as_errore = ""
	//---------------------------------------------------------------------------------------------------------
	li_ret = uof_riga_modello(li_anno_ordine, ll_num_ordine, ll_riga_ordine, as_errore)
	//---------------------------------------------------------------------------------------------------------
	
	if li_ret<0 then
		//l'elaborazione della riga ha dato errore ... segnalo nel log ma continuo
		uof_log("error", as_errore)
		rollback;
		continue
		
	elseif li_ret = 1 then
		if as_errore<>"" and not isnull(as_errore) then
			uof_log("warn", "Riga "+ls_riga_ordine +" saltata: " + as_errore)
		end if
		rollback;
		continue
		
	else 
		//riga elaborata con successo
		commit;
		uof_log("log", "COMMIT e fine elaborazione riga " + ls_riga_ordine)
	end if
loop

close cu_modelli;

return 0

end function

public function integer uof_elaborazione_righe_sfuso (datetime adt_data_reg_inizio, datetime adt_data_reg_fine, ref string as_errore);
integer			li_anno_ordine, li_ret
long				ll_num_ordine, ll_riga_ordine
string				ls_riga_ordine


uof_log("warn", "Inizio preparazione cursore righe sfusi #####################################################")

declare cu_sfusi cursor for  
	select 		d.anno_registrazione,
					d.num_registrazione,
					d.prog_riga_ord_ven
	from 	det_ord_ven as d
	join tes_ord_ven as t on d.cod_azienda = t.cod_azienda and 
									d.anno_registrazione = t.anno_registrazione and 
									d.num_registrazione = t.num_registrazione
	where  	d.cod_azienda=:s_cs_xx.cod_azienda and t.flag_blocco='N'  and d.flag_blocco='N' and
				t.flag_evasione<>'E'  and d.flag_evasione<>'E'  and 
				t.cod_tipo_ord_ven in (	select cod_tipo_ord_ven
												from tab_tipi_ord_ven
												where 	cod_azienda=:s_cs_xx.cod_azienda and
															flag_usa_per_cal_prod='S' and
															flag_tipo_bcl ='B') and
				t.data_registrazione>=:adt_data_reg_inizio and t.data_registrazione<=:adt_data_reg_fine
	order by d.anno_registrazione, d.num_registrazione, d.prog_riga_ord_ven;
//------------------------------------------------------------------------------------------------------------------------------------------


open cu_sfusi;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore 'cu_sfusi'~r~n" + sqlca.sqlerrtext
	return -1
end if

uof_log("warn", "Inizio elaborazione cursore righe sfusi")

do while true
	fetch cu_sfusi into	:li_anno_ordine,
							:ll_num_ordine,
							:ll_riga_ordine;

	ls_riga_ordine = string(li_anno_ordine)+"/"+string(ll_num_ordine)+"/"+string(ll_riga_ordine)
	uof_log("log", "Elaborazione riga " + ls_riga_ordine)
	as_errore = ""
	//---------------------------------------------------------------------------------------------------------
	li_ret = uof_riga_sfuso(li_anno_ordine, ll_num_ordine, ll_riga_ordine, as_errore)
	//---------------------------------------------------------------------------------------------------------
	
	if li_ret<0 then
		//l'elaborazione della riga ha dato errore ... segnalo nel log ma continuo
		uof_log("error", as_errore)
		rollback;
		continue
		
	elseif li_ret = 1 then
		if as_errore<>"" and not isnull(as_errore) then
			uof_log("warn", "Riga "+ls_riga_ordine +" saltata: " + as_errore)
		end if
		rollback;
		continue
		
	else 
		//riga elaborata con successo
		commit;
		uof_log("log", "COMMIT e fine elaborazione riga " + ls_riga_ordine)
	end if
loop

close cu_sfusi;

return 0

end function

public function integer uof_aggiorna_data_elab (ref string as_errore);integer li_count
datetime ldt_data_ultima_elab, ldt_ora_ultima_elab

ldt_data_ultima_elab = datetime(today(), 00:00:00)
ldt_ora_ultima_elab = datetime(date(1900, 1, 1), now())

select count(*)
into :li_count
from tab_cal_produzione_elab
where cod_azienda=:s_cs_xx.cod_azienda;

if li_count>0 then
	
	update tab_cal_produzione_elab
		set 	data_ultima_elab = :ldt_data_ultima_elab,
				ora_ultima_elab = :ldt_ora_ultima_elab,
				log_level = :ii_log_level
	where cod_azienda=:s_cs_xx.cod_azienda;
	
else
	
	insert into tab_cal_produzione_elab
				(	cod_azienda, 
					data_ultima_elab, 
					ora_ultima_elab,
					log_level)
		values (	:s_cs_xx.cod_azienda,
					:ldt_data_ultima_elab,
					:ldt_ora_ultima_elab,
					:ii_log_level);

end if

if sqlca.sqlcode<0 then
	as_errore = "Problema durante l'aggiornamento della data esecuzione elaborazione calendario produzione: "+sqlca.sqlerrtext
	uof_log("error", as_errore)
	return -1
end if

uof_log("log", "Aggiornata data elaborazione al "+string(ldt_data_ultima_elab, "dd/mm/yyyy")+" alle ore "+string(ldt_ora_ultima_elab, "hh:mm:ss"))

return 0
end function

public function integer uof_controlla_fine_fase (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_reparto, ref string as_errore);
string 			ls_flag_fine_fase, ls_flag_pronto_complessivo
datetime			ldt_fine_preparazione


// Verifico se la produzione è terminata; se è terminata allora "scarto" l'ordine.
select flag_fine_fase
into   :ls_flag_fine_fase
from   det_ordini_produzione
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ai_anno_ordine and
		 num_registrazione = :al_num_ordine and
		 prog_riga_ord_ven = :al_riga_ordine and
		 cod_reparto = :as_cod_reparto;
		
choose case sqlca.sqlcode 
	case 0
		// se la fase è conclusa allora salto
		if ls_flag_fine_fase = "S" then
			//as_errore = "Fine Fase per il reparto "+as_cod_reparto
			return 1
		end if
				
	case 100
		// non trovato in det_ordini_produzione, provo a controllare in tes_ordini_produzione
		select fine_preparazione, flag_pronto_complessivo
		into   :ldt_fine_preparazione, :ls_flag_pronto_complessivo
		from   tes_ordini_produzione
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ai_anno_ordine and
				 num_registrazione = :al_num_ordine and
				 cod_reparto = :as_cod_reparto;

		choose case sqlca.sqlcode
			case 0
				// se la fase è conclusa allora salto
				if not isnull(ldt_fine_preparazione) and ls_flag_pronto_complessivo = "S" then
					//as_errore = "Fine Fase per il reparto "+as_cod_reparto
					return 1
				end if
				
			case else
				//richiesto da Daniele 24/05/2011 ------------------
				//non segnalare alcun errore, vai avanti ed elabora l'ordine
				return 0
				
		end choose
		
	case else
		//richiesto da Daniele 24/05/2011 ------------------
		//non segnalare alcun errore, vai avanti ed elabora l'ordine
		return 0
	
end choose

return 0
end function

public function integer uof_disimpegna_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore);//elimina il contributo al calendario produzione della riga ordine nelle rispettive date
//utile quando l'ordine o la riga ordine deve essere eliminata .....

string					ls_cod_reparto
datetime				ldt_data_pronto
integer				li_ret


//NOTA: se la det_ord_ven_prod è vuota, questa funzione non fa nulla


declare cu_reparti cursor for  
	select cod_reparto, data_pronto
	from det_ord_ven_prod
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine and
				prog_riga_ord_ven=:al_riga_ordine;

open cu_reparti;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore 'cu_reparti' (uof_disimpegna_calendario): " + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_reparti into :ls_cod_reparto, :ldt_data_pronto;

	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore in fetch cursore 'cu_reparti'  (uof_disimpegna_calendario):" + sqlca.sqlerrtext
		close cu_reparti;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	if isnull(ldt_data_pronto) or year(date(ldt_data_pronto)) < 1950 then
		//salta reparto
		continue
		
	end if
	
	//disimpegno calendario
	li_ret = uof_disimpegna_calendario(ai_anno_ordine, al_num_ordine, al_riga_ordine, ls_cod_reparto, as_errore)
	
	if li_ret < 0 then
		close cu_reparti;
		return -1
	end if
loop

close cu_reparti;

return 0
end function

public function integer uof_get_data_pronto_reparto (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_reparto, ref datetime adt_data_pronto, ref string as_errore);//trova la data pronto aggiornata del reparto: se ordine tende da sole o tecniche cerca nelle varianti, altrimenti nella det_ord_ven

string				ls_flag_tipo_bcl

select tp.flag_tipo_bcl
into :ls_flag_tipo_bcl
from tab_tipi_ord_ven as tp
join tes_ord_ven as t on t.cod_azienda=tp.cod_azienda and
								t.cod_tipo_ord_ven=tp.cod_tipo_ord_ven
where 	tp.cod_azienda=:s_cs_xx.cod_azienda and
			t.anno_registrazione=:ai_anno_ordine and
			t.num_registrazione=:al_num_ordine;

if sqlca.sqlcode<0 then
	as_errore = "Errore lettura flag tipo ordine: "+sqlca.sqlerrtext
	return -1
end if

if ls_flag_tipo_bcl="A" then
	
	//cerca tra le varianti
	select data_pronto
	into :adt_data_pronto
	from varianti_det_ord_ven_prod
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine and
				prog_riga_ord_ven=:al_riga_ordine and
				cod_reparto is not null and
				cod_reparto=:as_cod_reparto;
	
elseif ls_flag_tipo_bcl="B" or ls_flag_tipo_bcl="C" then
	
	//cerca nella riga ordine (non è necessario specificare il reparto, può essere uno solo)
	select data_pronto_sfuso
	into :adt_data_pronto
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine and
				prog_riga_ord_ven=:al_riga_ordine;
	
else
	as_errore = "Flag tipo ordine sconosciuto!"
	return 1
end if

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura data pronto reparto: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 or isnull(adt_data_pronto) or year(date(adt_data_pronto))<1950 then
	as_errore = "Data pronto o reparto non trovato!"
	return 1
end if

return 0





end function

public function integer uof_disimpegna_calendario (integer ai_anno_ordine, long al_num_ordine, ref string as_errore);//elimina il contributo al calendario produzione dell'ordine

long					ll_riga_ordine
integer				li_ret


declare cu_righe cursor for  
	select prog_riga_ord_ven
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine;

open cu_righe;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore 'cu_righe' (uof_disimpegna_calendario): " + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_righe into :ll_riga_ordine;

	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore in fetch cursore 'cu_righe'  (uof_disimpegna_calendario):" + sqlca.sqlerrtext
		close cu_righe;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	li_ret = uof_disimpegna_calendario(ai_anno_ordine, al_num_ordine, ll_riga_ordine, as_errore)
	
	if li_ret < 0 then
		close cu_righe;
		return -1
	end if
loop

close cu_righe;

return 0
end function

public function integer uof_sposta_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_reparto, datetime adt_data_pronto_new, ref string as_errore);//elimina il contributo al calendario produzione del reparto della riga ordine nella vecchia data
//ed esegue l'impegno delr eparto nella nuova data


dec{4}				ld_minuti_reparto, ld_minuti_effettivi, ld_minuti_effettivi_rc, ld_min_eff_cal, ld_min_eff_rc_cal
dec{4}				ld_minuti_effettivi_1, ld_minuti_effettivi_rc_1
string					ls_temp
datetime				ldt_data_pronto_old


//leggo il contributo
select 	minuti_reparto, minuti_effettivi, minuti_giro_rc, data_pronto
into 		:ld_minuti_reparto, :ld_minuti_effettivi, :ld_minuti_effettivi_rc, :ldt_data_pronto_old
from		det_ord_ven_prod
where		cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_ordine and
			num_registrazione=:al_num_ordine and
			prog_riga_ord_ven=:al_riga_ordine and
			cod_reparto=:as_cod_reparto;
			
if sqlca.sqlcode = 0 and ld_minuti_effettivi>0 then
	//se esiste il contributo allora procedo
	
	//salvo i valori in variabili di appoggio --------------------------------------------------------------------------------------------------------------------------------
	ld_minuti_effettivi_1 = ld_minuti_effettivi
	ld_minuti_effettivi_rc_1 = ld_minuti_effettivi_rc
	
	
	//controllo anche se nella data pronto precedente le quantità sono sufficienti, altrimenti le metterò a zero --------------------------------------------------
	select minuti_effettivi, minuti_giro_rc
	into :ld_min_eff_cal, :ld_min_eff_rc_cal
	from tab_cal_produzione
	where		cod_azienda=:s_cs_xx.cod_azienda and
				cod_reparto=:as_cod_reparto and
				data_giorno=:ldt_data_pronto_old;
	
	if sqlca.sqlcode=0 then

		if isnull(ld_min_eff_cal) then ld_min_eff_cal=0
		if isnull(ld_min_eff_rc_cal) then ld_min_eff_rc_cal=0

		//controllo quantità --------------------------------------------------------------------------------------------------------------------------------------------------
		if ld_min_eff_cal < ld_minuti_effettivi then
			//la quantità da disimpegnare non sarebbe sufficiente, 
			//quindi limito la quantità a quella disponibile per ottenere ZERO e non un valore negativo
			ld_minuti_effettivi = ld_min_eff_cal
		end if
		if ld_min_eff_rc_cal < ld_minuti_effettivi_rc then
			//la quantità da disimpegnare non sarebbe sufficiente, 
			//quindi limito la quantità a quella disponibile per ottenere ZERO e non un valore negativo
			ld_minuti_effettivi_rc = ld_min_eff_rc_cal
		end if
	
		//disimpegno il calendario nella vecchia data pronto del reparto ------------------------------------------------------------------------------------------------
		update tab_cal_produzione
		set 	minuti_effettivi= minuti_effettivi - :ld_minuti_effettivi,
				minuti_giro_rc= minuti_giro_rc - :ld_minuti_effettivi_rc
		where		cod_azienda=:s_cs_xx.cod_azienda and
				cod_reparto=:as_cod_reparto and
				data_giorno=:ldt_data_pronto_old;
		
		//ripristino i valori, qualora modificati/limitati -----------------------------------------------------------------------------------------------------------------------
		ld_minuti_effettivi = ld_minuti_effettivi_1
		ld_minuti_effettivi_rc = ld_minuti_effettivi_rc_1
		
	else
		//riga non trovata, quindi non mi faccio problemi di alcun tipo e non disimpegno il calendario
	end if
	
	
	//procedo con l'impegno del calendario nella nuova data ---------------------------------------------------------------------------------------------------------------
	select cod_azienda
	into :ls_temp
	from tab_cal_produzione
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_reparto=:as_cod_reparto and
				data_giorno=:adt_data_pronto_new;
	
	if sqlca.sqlcode=100 or ls_temp="" or isnull(ls_temp) then
		//inserisci il record
		insert into tab_cal_produzione
		(	cod_azienda,
			cod_reparto,
			data_giorno,
			minuti_reparto,
			minuti_effettivi,
			minuti_giro_rc)
		values	(	:s_cs_xx.cod_azienda,
						:as_cod_reparto,
						:adt_data_pronto_new,
						:ld_minuti_reparto,
						:ld_minuti_effettivi,
						:ld_minuti_effettivi_rc);
	else
		//aggiorna il record
		update tab_cal_produzione
		set 	minuti_effettivi = minuti_effettivi + :ld_minuti_reparto,
				minuti_giro_rc = minuti_giro_rc + :ld_minuti_effettivi_rc
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_reparto=:as_cod_reparto and
					data_giorno=:adt_data_pronto_new;
				
	end if
	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore aggiornamento cella calendario data "+string(adt_data_pronto_new,"dd/mm/yyyy") + " - reparto "+as_cod_reparto + " : "+sqlca.sqlerrtext
		return -1
	end if
	
	
	//aggiorno con la nuova data pronto --------------------------------------------------------------------------------------------------------------------------------------
	update det_ord_ven_prod
	set data_pronto = :adt_data_pronto_new
	where		cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine and
				prog_riga_ord_ven=:al_riga_ordine and
				cod_reparto=:as_cod_reparto;
	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore aggiornamento nuova data pronto in det_ord_ven_prod per il reparto "+as_cod_reparto + " : "+sqlca.sqlerrtext
		return -1
	end if
	
end if


return 0
end function

public function integer uof_sposta_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore);//elimina il contributo al calendario produzione della riga ordine nella vecchia data (memorizzata in det_ord_ven_prod)
//ed esegue l'impegno della riga ordine nella nuova data (memorizzata nelle varianti_det_ord_ven_prod oppure direttamente in det_ord_ven, in base al tipo ordine)
//poi salva la nuova data pronto anche in det_ord_ven_prod

string					ls_cod_reparto
integer				li_ret
datetime				ldt_data_pronto_new

//NOTA BENE
//in assenza di dati nella det_ord_ven_prod, questa funzione non effettuerà nulla

declare cu_reparti cursor for  
	select cod_reparto
	from det_ord_ven_prod
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine and
				prog_riga_ord_ven=:al_riga_ordine;

open cu_reparti;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore 'cu_reparti' (uof_sposta_calendario): " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_reparti into :ls_cod_reparto;

	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore in fetch cursore 'cu_reparti'  (uof_sposta_calendario):" + sqlca.sqlerrtext
		close cu_reparti;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	if li_ret < 0 then
		close cu_reparti;
		return -1
		
	elseif li_ret = 1 then
		//salta reparto
		continue
		
	end if
	
	//leggo la nuova data pronto del reparto (quella vecchia è memorizzata in det_ord_ven_prod)
	//(o varianti_det_ord_ven_prod oppure det_ord_ven, a seconda che sia un ordine di tende da sole/tecniche oppure di sfuso)
	li_ret = uof_get_data_pronto_reparto(ai_anno_ordine, al_num_ordine, al_riga_ordine, ls_cod_reparto, ldt_data_pronto_new, as_errore)
	
	if li_ret < 0 then
		
	elseif li_ret = 1 then
		//salta
		continue
	end if
	
	//spostamento impegno nel calendario produzione
	li_ret = uof_sposta_calendario(ai_anno_ordine, al_num_ordine, al_riga_ordine, ls_cod_reparto, ldt_data_pronto_new, as_errore)
	
	if li_ret < 0 then
		close cu_reparti;
		return -1
	end if
loop

close cu_reparti;

return 0

end function

public function integer uof_sposta_calendario (integer ai_anno_ordine, long al_num_ordine, ref string as_errore);//elimina il contributo al calendario produzione dell'intero ordine nella vecchia data (memorizzata in det_ord_ven_prod)
//ed esegue l'impegno dell'intero ordine nella nuova data (memorizzata nelle varianti_det_ord_ven_prod oppure direttamente in det_ord_ven, in base al tipo ordine)
//poi salva la nuova data pronto anche in det_ord_ven_prod

long					ll_prog_riga_ord_ven
integer				li_ret
datetime				ldt_data_pronto_new


declare cu_righe cursor for  
	select prog_riga_ord_ven
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine;

open cu_righe;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore 'cu_righe' (uof_sposta_calendario): " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_righe into :ll_prog_riga_ord_ven;

	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore in fetch cursore 'cu_righe'  (uof_sposta_calendario):" + sqlca.sqlerrtext
		close cu_righe;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	if li_ret < 0 then
		close cu_righe;
		return -1
		
	elseif li_ret = 1 then
		//salta reparto
		continue
		
	end if
	
	
	//spostamento impegno nel calendario produzione
	li_ret = uof_sposta_calendario(ai_anno_ordine, al_num_ordine, ll_prog_riga_ord_ven, as_errore)
	
	if li_ret < 0 then
		close cu_righe;
		return -1
		
	elseif li_ret = 1 then
		//salta riga
		continue
		
	end if
	
loop

close cu_righe;

return 0

end function

public function integer uof_disimpegna_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_reparto, ref string as_errore);//elimina il contributo al calendario produzione del reparto della riga ordine

dec{4}				ld_minuti_reparto, ld_minuti_effettivi, ld_minuti_effettivi_rc, ld_min_eff_cal, ld_min_eff_rc_cal
datetime				ldt_data_pronto


//leggo il contributo
select 	minuti_reparto, minuti_effettivi, minuti_giro_rc, data_pronto
into 		:ld_minuti_reparto, :ld_minuti_effettivi, :ld_minuti_effettivi_rc, :ldt_data_pronto
from		det_ord_ven_prod
where		cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_ordine and
			num_registrazione=:al_num_ordine and
			prog_riga_ord_ven=:al_riga_ordine and
			cod_reparto=:as_cod_reparto;
			
if sqlca.sqlcode = 0 and ld_minuti_effettivi>0 and not isnull(ldt_data_pronto) and year(date(ldt_data_pronto))>1950 then
	//se esiste il contributo allora procedo
	
	//controllo anche se nella data pronto precedente le quantità sono sufficienti, altrimenti le metterò a zero --------------------------------------------------
	select minuti_effettivi, minuti_giro_rc
	into :ld_min_eff_cal, :ld_min_eff_rc_cal
	from tab_cal_produzione
	where		cod_azienda=:s_cs_xx.cod_azienda and
				cod_reparto=:as_cod_reparto and
				data_giorno=:ldt_data_pronto;
	
	if sqlca.sqlcode=0 then

		if isnull(ld_min_eff_cal) then ld_min_eff_cal=0
		if isnull(ld_min_eff_rc_cal) then ld_min_eff_rc_cal=0

		//controllo quantità --------------------------------------------------------------------------------------------------------------------------------------------------
		if ld_min_eff_cal < ld_minuti_effettivi then
			//la quantità da disimpegnare non sarebbe sufficiente, 
			//quindi limito la quantità a quella disponibile per ottenere ZERO e non un valore negativo
			ld_minuti_effettivi = ld_min_eff_cal
		end if
		if ld_min_eff_rc_cal < ld_minuti_effettivi_rc then
			//la quantità da disimpegnare non sarebbe sufficiente, 
			//quindi limito la quantità a quella disponibile per ottenere ZERO e non un valore negativo
			ld_minuti_effettivi_rc = ld_min_eff_rc_cal
		end if
	
		//disimpegno il calendario nella vecchia data pronto del reparto ------------------------------------------------------------------------------------------------
		update tab_cal_produzione
		set 	minuti_effettivi= minuti_effettivi - :ld_minuti_effettivi,
				minuti_giro_rc= minuti_giro_rc - :ld_minuti_effettivi_rc
		where		cod_azienda=:s_cs_xx.cod_azienda and
				cod_reparto=:as_cod_reparto and
				data_giorno=:ldt_data_pronto;

	else
		//riga non trovata, quindi non mi faccio problemi di alcun tipo e non disimpegno il calendario
	end if
	
else
	//salto
	return 1
end if


return 0
end function

public function integer uof_aggiorna_detordven_prod (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_reparto, decimal ad_minuti_reparto, decimal ad_minuti_effettivi, decimal ad_minuti_giro_rc, datetime adt_data_pronto, ref string as_errore);dec{4} ld_appo_eff, ld_appo_giro_rc


if isnull(adt_data_pronto) or year(date(adt_data_pronto)) < 1950 then return 1


//cancella e re-inserisce nella det_ord_ven_prod
delete from det_ord_ven_prod
where		cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ai_anno_ordine and 
			num_registrazione = :al_num_ordine and
			prog_riga_ord_ven = :al_riga_ordine and
			cod_reparto = :as_reparto;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in reset det_ord_ven_prod: " + sqlca.sqlerrtext
	return -1
end if

insert into det_ord_ven_prod
(	cod_azienda,
	anno_registrazione,
	num_registrazione,
	prog_riga_ord_ven,
	cod_reparto,
	flag_attivo,
	minuti_reparto,
	minuti_effettivi,
	minuti_giro_rc,
	data_pronto)
values	(	:s_cs_xx.cod_azienda,
				:ai_anno_ordine,
				:al_num_ordine,
				:al_riga_ordine,
				:as_reparto,
				'S',
				:ad_minuti_reparto,
				:ad_minuti_effettivi,
				:ad_minuti_giro_rc,
				:adt_data_pronto);

if sqlca.sqlcode < 0 then
	as_errore = "Errore in aggiornamento det_ord_ven_prod: " + sqlca.sqlerrtext
	return -1
end if

return 0

end function

public function integer uof_aggiorna_tabella_calendario (string as_cod_reparto, datetime adt_data_pronto, decimal ad_minuti_effettivi, decimal ad_minuti_giro_rc, ref string as_errore);dec{4}			ld_minuti_reparto
string				ls_temp


if isnull(adt_data_pronto) or year(date(adt_data_pronto)) < 1950 then return 1

//select cod_azienda
//into :ls_temp
//from tab_cal_produzione
//where 	cod_azienda = :s_cs_xx.cod_azienda and
//			cod_reparto = :as_cod_reparto and
//			data_giorno = :adt_data_pronto;


//if sqlca.sqlcode=100 or isnull(ls_temp) or ls_temp="" then
//	//fai inserimento, ma considera che vuol dire che non era stato pianificato il reparto ...
//	
//	ld_minuti_reparto = 0
//	
//	insert into tab_cal_produzione
//	(	cod_azienda,
//		cod_reparto,
//		data_giorno,
//		minuti_reparto,
//		minuti_effettivi,
//		minuti_giro_rc)
//	values	(	:s_cs_xx.cod_azienda,
//					:as_cod_reparto,
//					:adt_data_pronto,
//					:ld_minuti_reparto,
//					:ad_minuti_effettivi,
//					:ad_minuti_giro_rc);
//	
//else
	//fai aggiornamento
	update tab_cal_produzione
	set minuti_effettivi		= minuti_effettivi + :ad_minuti_effettivi,
		 minuti_giro_rc		= minuti_giro_rc   + :ad_minuti_giro_rc
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_reparto = :as_cod_reparto and
			data_giorno = :adt_data_pronto;
	
//end if


if sqlca.sqlcode < 0 then
	as_errore = "Errore in update tabella tab_cal_produzione~r~n" + sqlca.sqlerrtext
	return -1
end if

return 0
end function

public function integer uof_prepara_tabella_temporanea (datetime adt_data_reg_inizio, datetime adt_data_reg_fine, ref string as_errore);
integer				li_anno_ordine
long					ll_num_ordine, ll_riga_ordine, ll_indice
string					ls_riga_ordine


uof_log("warn", "Inizio preparazione cursore righe modelli per inserimento in tabella temporanea ########################################")

ll_indice = 0

declare cu_modelli cursor for  
	select 	d.anno_registrazione,
				d.num_registrazione,   
				d.prog_riga_ord_ven
	from 	det_ord_ven as d
	join tes_ord_ven as t on d.cod_azienda = t.cod_azienda and 
									d.anno_registrazione = t.anno_registrazione and
									d.num_registrazione = t.num_registrazione
	where  	d.cod_azienda=:s_cs_xx.cod_azienda and t.flag_blocco<>'S'  and d.flag_blocco<>'S'  and
				t.flag_evasione<>'E'  and d.flag_evasione<>'E' and 
				d.cod_prodotto is not null and d.cod_versione is not null and d.cod_versione<>'' and d.quan_ordine>0 and
				t.cod_tipo_ord_ven in ( 	select cod_tipo_ord_ven
												from tab_tipi_ord_ven
												where cod_azienda=:s_cs_xx.cod_azienda and 
												flag_usa_per_cal_prod='X' and
												flag_tipo_bcl in ('A','C')) and
				t.data_registrazione>=:adt_data_reg_inizio and t.data_registrazione<=:adt_data_reg_fine and d.num_riga_appartenenza=0
	order by d.anno_registrazione, d.num_registrazione, d.prog_riga_ord_ven;

open cu_modelli;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore cu_modelli: " + sqlca.sqlerrtext
	return -1
end if

uof_log("warn", "Inizio elaborazione cursore righe modelli per inserimento in tabella temporanea")

do while true
	fetch cu_modelli into	:li_anno_ordine,
								:ll_num_ordine,
								:ll_riga_ordine;
	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore in fetch cursore cu_modelli: " + sqlca.sqlerrtext
		close cu_modelli;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	ls_riga_ordine = string(li_anno_ordine)+"/"+string(ll_num_ordine)+"/"+string(ll_riga_ordine)
	ll_indice += 1
	
	insert into tab_cal_produzione_temp
	(	cod_azienda,
		progressivo,
		anno_registrazione,
		num_registrazione,
		prog_riga_ord_ven,
		tipo_ordine)
	values	(	:s_cs_xx.cod_azienda,
					:ll_indice,
					:li_anno_ordine,
					:ll_num_ordine,
					:ll_riga_ordine,
					1);
	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore inserimento Tab. temp Riga modello " + ls_riga_ordine + " " + sqlca.sqlerrtext
		close cu_modelli;
		return -1
	end if
	
loop

commit;
close cu_modelli;


//###############################################################################

uof_log("warn", "Inizio preparazione cursore righe sfusi per inserimento in tabella temporanea #########################################")

declare cu_sfusi cursor for  
	select 		d.anno_registrazione,
					d.num_registrazione,
					d.prog_riga_ord_ven
	from 	det_ord_ven as d
	join tes_ord_ven as t on d.cod_azienda = t.cod_azienda and 
									d.anno_registrazione = t.anno_registrazione and 
									d.num_registrazione = t.num_registrazione
	where  	d.cod_azienda=:s_cs_xx.cod_azienda and t.flag_blocco='N'  and d.flag_blocco='N' and
				t.flag_evasione<>'E'  and d.flag_evasione<>'E'  and 
				d.cod_prodotto is not null and  d.quan_ordine>0 and
				t.cod_tipo_ord_ven in (	select cod_tipo_ord_ven
												from tab_tipi_ord_ven
												where 	cod_azienda=:s_cs_xx.cod_azienda and
															flag_usa_per_cal_prod='S' and
															flag_tipo_bcl ='B') and
				t.data_registrazione>=:adt_data_reg_inizio and t.data_registrazione<=:adt_data_reg_fine
	order by d.anno_registrazione, d.num_registrazione, d.prog_riga_ord_ven;
//------------------------------------------------------------------------------------------------------------------------------------------


open cu_sfusi;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore cu_sfusi:" + sqlca.sqlerrtext
	return -1
end if

uof_log("warn", "Inizio elaborazione cursore righe sfusi per inserimento in tabella temporanea")

do while true
	fetch cu_sfusi into	:li_anno_ordine,
							:ll_num_ordine,
							:ll_riga_ordine;

	if sqlca.sqlcode < 0 then
		as_errore = "Errore in fetch cursore cu_sfusi: " + sqlca.sqlerrtext
		close cu_sfusi;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	ls_riga_ordine = string(li_anno_ordine)+"/"+string(ll_num_ordine)+"/"+string(ll_riga_ordine)
	as_errore = ""
	
	ll_indice += 1
	
	insert into tab_cal_produzione_temp
	(	cod_azienda,
		progressivo,
		anno_registrazione,
		num_registrazione,
		prog_riga_ord_ven,
		tipo_ordine)
	values	(	:s_cs_xx.cod_azienda,
					:ll_indice,
					:li_anno_ordine,
					:ll_num_ordine,
					:ll_riga_ordine,
					0);
	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore inserimento Tab. temp Riga sfuso " + ls_riga_ordine + " " + sqlca.sqlerrtext
		close cu_modelli;
		return -1
	end if
	
loop

commit;
close cu_sfusi;

uof_log("warn", "Fine preparazione tabella temporanea ####################################################")


return 0
end function

public function integer uof_salva_in_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_reparti[], datetime adt_date_pronto[], ref string as_errore);//questa funzione va chiamata per inserire i dati nel calendario e in det_ord_ven_prod su base riga ordine
//se necessario pulisce dati esistenti, disimpegnando il calendario nelle date precedenti e re-inserendo i nuovi dati, in giorni eventualmente diversi ...

integer			li_ret, li_index
dec{4}			ld_minuti_reparto, ld_minuti_effettivi, ld_minuti_giro_rc
string				ls_temp, ls_riga


//pulizia eventuale dalle tabelle calendario e det_ord_ven_prod ------------------------------------------------------------------------------
li_ret = uof_disimpegna_calendario(ai_anno_ordine, al_num_ordine, al_riga_ordine, as_errore)
if li_ret < 0 then
	return -1
end if

delete from det_ord_ven_prod
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_ordine and
			num_registrazione=:al_num_ordine and
			prog_riga_ord_ven=:al_riga_ordine;

if sqlca.sqlcode<0 then
	as_errore = "Errore in cancellazione dati da tabella det_ord_ven_prod: "+sqlca.sqlerrtext
	return -1
end if

//per ogni reparto
//			calcola i minuti necessari alla preparazione
//			aggiorna il calendario nella data pronto per il reparto
//			aggiorna la det_ord_ven_prod relativamente al reparto e data pronto
for li_index=1 to upperbound(as_reparti[])
	
	ls_riga = string(ai_anno_ordine)+"/"+string(al_num_ordine)+"/"+string(al_riga_ordine)
	
	//NOTA ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//se nella data pronto il reparto non è pianificato in ore/minuti previti
	//allora salta il salvataggio del calendario e in det_ord_ven_prod e logga
	if not uof_controlla_pianificazione(ls_riga, as_reparti[li_index], adt_date_pronto[li_index]) then
		//salta il reparto. Log già scritto
		continue
	end if
	
	
	//calcola i dati relativi a minuti ----------------------------------------------------------------------------------------------------------------------------------------------
	//NOTA
	//se non trova la combinazione prodotto-reparto o prodotto-reparto-gruppo variante
	//salta il salvataggio del calendario e in det_ord_ven_prod e logga
	li_ret = uof_get_minuti_reparto(	ai_anno_ordine, al_num_ordine, al_riga_ordine, as_reparti[li_index], adt_date_pronto[li_index], &
												ld_minuti_effettivi, ld_minuti_giro_rc, as_errore)
	if li_ret<0 then
		return -1
		
	elseif li_ret = 1 then
		//minuti non indicati in tabella tempi calendario
		if not ib_log then
			uof_log_sistema("", as_errore)
		else
			uof_log("warn", as_errore)
		end if
		
		continue
	end if
	
	
	//aggiorna calendario ---------------------------------------------------------------------------------------------------------------------------------------------------------
	li_ret = uof_aggiorna_tabella_calendario(as_reparti[li_index], adt_date_pronto[li_index], ld_minuti_effettivi, ld_minuti_giro_rc, as_errore)
	if li_ret<0 then
		return -1
	end if
	
	
	//leggo i minuti totali pianificati per il reparto nel calendario produzione ------------------------------------------------------------------------------------------------
	setnull(ld_minuti_reparto)
	
	select minuti_reparto
	into :ld_minuti_reparto
	from tab_cal_produzione
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_reparto = :as_reparti[li_index] and
			data_giorno = :adt_date_pronto[li_index];
	if isnull(ld_minuti_reparto) then ld_minuti_reparto = 0
	
	
	//aggiorna det_ord_ven_prod con il reparto corrente -----------------------------------------------------------------------------------------------------------------------
	li_ret = uof_aggiorna_detordven_prod(	ai_anno_ordine, al_num_ordine, al_riga_ordine, &
														as_reparti[li_index], ld_minuti_reparto, ld_minuti_effettivi, ld_minuti_giro_rc, adt_date_pronto[li_index], as_errore)
	if li_ret<0 then
		return -1
	end if
	
next



return 0
end function

public function integer uof_get_minuti_reparto (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_reparto, datetime adt_data_pronto, ref decimal ad_minuti_effettivi, ref decimal ad_minuti_giro_rc, ref string as_errore);
string			ls_flag_tipo_bcl, ls_cod_giro_consegna, ls_flag_rc, ls_cod_gruppo_variante, ls_cod_prodotto, ls_cod_versione, ls_cod_prodotto_variante
boolean		lb_modello, lb_quan_variante
dec{4}		ld_quan_ordine,ld_tempo_reparto, ld_quan_prodotto_variante
integer		li_ret


//leggi il tipo ordine ------------------------------------------------------------------------------------------------------
select tp.flag_tipo_bcl, t.cod_giro_consegna
into :ls_flag_tipo_bcl, :ls_cod_giro_consegna
from tab_tipi_ord_ven as tp
join tes_ord_ven as t on t.cod_azienda=tp.cod_azienda and
								t.cod_tipo_ord_ven=tp.cod_tipo_ord_ven
where tp.cod_azienda=:s_cs_xx.cod_azienda and
		t.anno_registrazione=:ai_anno_ordine and
		t.num_registrazione=:al_num_ordine;

if ls_flag_tipo_bcl="A" or ls_flag_tipo_bcl="C" then
	lb_modello = true
elseif ls_flag_tipo_bcl="B" then
	lb_modello = false
else
	as_errore = "Tipo ordine sconosciuto (nè A, nè B, nè C)"
	return 1
end if


//valuta i minuti reparto in base al tipo ordine ----------------------------------------------------------------------------
if lb_modello then
	//si tratta di un modello ######################################
	
	lb_quan_variante = false
	setnull(ls_cod_gruppo_variante)
	
	//leggo info che mi servono --------------------------
	select d.cod_prodotto, d.cod_versione, d.quan_ordine
	into :ls_cod_prodotto, :ls_cod_versione, :ld_quan_ordine
	from det_ord_ven as d
	join tes_ord_ven as t on t.cod_azienda=d.cod_azienda and
									t.anno_registrazione=d.anno_registrazione and
									t.num_registrazione=d.num_registrazione
	where 	d.cod_azienda=:s_cs_xx.cod_azienda and
				d.anno_registrazione=:ai_anno_ordine and
				d.num_registrazione=:al_num_ordine and
				d.prog_riga_ord_ven=:al_riga_ordine;
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore lettura dati riga ordine (uof_get_minuti_reparto): "+sqlca.sqlerrtext
		return -1
	end if
	
	//leggo i tempi che occorrono al reparto per produrre -----------------------------
	li_ret = uof_tempo_reparto(as_cod_reparto, ls_cod_prodotto, ls_cod_versione, ld_tempo_reparto, lb_quan_variante, ls_cod_gruppo_variante, as_errore)
	if li_ret<0 then
		return -1
	elseif li_ret=1 then
		//riga saltata: in as_errore il messaggio
		return 1
	end if
	
	//moltiplico sempre e comunque per la quantità ordinata
	ld_tempo_reparto = ld_tempo_reparto * ld_quan_ordine
	
	//-----------------------------------------------------------------------------------------------------------
	if lb_quan_variante then
		li_ret = uof_trova_quan_variante(	ls_cod_prodotto, ls_cod_versione, ls_cod_gruppo_variante, &
													ai_anno_ordine, al_num_ordine, al_riga_ordine, &
													ls_cod_prodotto_variante, ld_quan_prodotto_variante, as_errore)
		if li_ret<0 then
			return -1
		end if
		
		ld_tempo_reparto = ld_tempo_reparto * ld_quan_prodotto_variante										
	end if
	
	ad_minuti_effettivi = ld_tempo_reparto
	//####################################################
	
else
	//si tratta di uno sfuso
	select numero
	into   :ad_minuti_effettivi
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_parametro = 'MOS';
	
	if sqlca.sqlcode <> 0 or ad_minuti_effettivi<=0 or isnull(ad_minuti_effettivi) then
		ad_minuti_effettivi = 0
	end if
end if


// se l'ordine appartiene ad giro RC allora incremento anche il tempo di RC ----------------------------------------
ls_flag_rc = "N"
if not isnull(ls_cod_giro_consegna) and ls_cod_giro_consegna<>"" then
	select flag_rc
	into   :ls_flag_rc
	from   tes_giri_consegne
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_giro_consegna = :ls_cod_giro_consegna;
	if sqlca.sqlcode <> 0 then ls_flag_rc ="N"
end if

if ls_flag_rc = "S" then 
	ad_minuti_giro_rc = ad_minuti_effettivi
else
	ad_minuti_giro_rc = 0
end if




return 0
end function

public subroutine uof_log (string as_tipo, string as_testo);

if not ib_log then return

choose case as_tipo
	case "error"
		iuo_log.error(as_testo)
		
	case "warn"
		iuo_log.warn(as_testo)
		
	case "log"
		iuo_log.log(as_testo)
		
	case "info"
		iuo_log.info(as_testo)
		
	case "debug"
		iuo_log.debug(as_testo)
	
end choose

return
end subroutine

public function integer uof_tempo_reparto (string as_cod_reparto, string as_cod_prodotto, string as_cod_versione, ref decimal ad_tempo_reparto, ref boolean ab_quan_variante, ref string as_cod_gruppo_variante, ref string as_errore);
integer			li_count, li_ret
string				ls_messaggio


ab_quan_variante = false

//controllo in tab_distinta_reparti_tempi l'esistenza di tempi del reparto/prodotto per cod_gruppo_variante not null
select count(cod_gruppo_variante), cod_gruppo_variante
into   :li_count, :as_cod_gruppo_variante
from   tab_distinta_reparti_tempi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto_distinta = :as_cod_prodotto and
		 cod_reparto = :as_cod_reparto and
		 cod_gruppo_variante is not null
group by cod_gruppo_variante;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in select in tab_distinta_reparti_tempi (1)~r~n" + sqlca.sqlerrtext
	return -1
end if

if li_count > 0 then
	//ho trovato l'associazione    prodotto/versione/reparto/gruppo_variante;
	
	// se esiste più di una associazione distinta/reparto/gruppo_variante segnalo la cosa, ma vado avanti lo stesso
	if li_count > 1 then
		ls_messaggio = "ATTENZIONE: l'associazione prodotto/reparto (" + as_cod_prodotto +"-" +  as_cod_versione +"-" + as_cod_reparto +  ")  è presente con più ricorrenze per gruppo_variante; non è corretto, ma vado avanti lo stesso prendendo il gruppo variante " + as_cod_gruppo_variante
		if not ib_log then
			uof_log_sistema("", ls_messaggio)
		else
			uof_log("info", ls_messaggio)
		end if
	end if
	
	select tempo_produzione
	into   :ad_tempo_reparto
	from   tab_distinta_reparti_tempi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto_distinta = :as_cod_prodotto and
			 cod_reparto = :as_cod_reparto and
			 cod_gruppo_variante = :as_cod_gruppo_variante;
			 
	if sqlca.sqlcode <> 0 then
		// segnalo errore anche se non lo trovo; il record DEVE esserci!
		as_errore = "(1) Errore in select tempo_produzione in tab_distinta_reparti_tempi. "+&
						"cod_prodotto_distinta='"+as_cod_prodotto+"' - cod_versione='"+as_cod_versione+"' - cod_reparto='"+as_cod_reparto+"' "+&
						" - cod_gruppo_variante='"+as_cod_gruppo_variante+"'~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ad_tempo_reparto) then ad_tempo_reparto = 0
	if ad_tempo_reparto=0 then
		//no tempi produzione impostati
		as_errore = "Trovata associazione distinta/reparto/gruppo_variante ("+as_cod_prodotto+"-"+as_cod_versione+"-" +as_cod_reparto+"-"+as_cod_gruppo_variante+&
						") ma con tempi produzione impostati pari a ZERO"
		return 1
	end if

	// cerca la quantità della variante nella DB
	ab_quan_variante = true
	
else
	// se non trovo i tempi per il gruppo_variante,  prendo il tempo per prodotto/versione/reparto
	select tempo_produzione
	into   :ad_tempo_reparto
	from   tab_distinta_reparti_tempi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto_distinta = :as_cod_prodotto and
			cod_reparto 	= :as_cod_reparto and
			 cod_gruppo_variante is null;
			 
	if sqlca.sqlcode < 0 then
		as_errore = "(2) Errore in select tempo_produzione in tab_distinta_reparti_tempi. "+&
					"cod_prodotto_distinta='"+as_cod_prodotto+"' - cod_versione='"+as_cod_versione+"' - cod_reparto='"+as_cod_reparto+"' "+&
					" - cod_gruppo_variante is null~r~n" + sqlca.sqlerrtext
		rollback;
		return -1
	end if
	
	// non ci sono tempi caricati per questo reparto, quindi salto al reparto successivo
	if sqlca.sqlcode = 100 then
		as_errore = "Non ci sono tempi produzione impostati per prodotto-reparto-gruppo variante e neppure per prodotto-reparto ("+as_cod_prodotto+" - "+as_cod_versione+" - "+as_cod_reparto+")"
		//ls_log = uof_add_log(ls_log,"NoTempiImpostati per "+fs_cod_prodotto+"-"+ls_reparti_distinta[ll_i])
		//continue
		return 1
	end if
	
	//->->->->->-> la moltiplicazione la faccio all'uscita di questa funzione
	//ad_tempo_reparto = ad_tempo_reparto * ad_quan_ordine
	
end if

return 0
end function

public function integer uof_tempo_reparto (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_reparti[], ref datetime ad_date_pronto[], ref decimal ad_tempo_reparto[], ref decimal ad_tempo_reparto_rc[], ref string as_errore);//questa funzione va chiamata per le righe ordine di tipo 1 e 3
//restituisce in arrays i reparti produzione con relativi tempi e date pronto, 
//per incrementare all'uscita della presente funzione la cella del calendario nella data pronto relativa ed aggiornare la tabella det_ord_ven_prod

dec{4}			ld_quan_ordine, ld_quan_prodotto_variante, ld_tempo_reparto, ld_tempo_reparto_rc, ld_vuoto[]
integer			li_ret, li_index, ll_counter, li_ret2
string				ls_cod_prodotto, ls_cod_versione, ls_cod_gruppo_variante, ls_cod_prodotto_variante, ls_cod_giro_consegna, ls_flag_rc, ls_sql, ls_cod_reparto, ls_vuoto[], &
					ls_reparti[], ls_riga, ls_messaggio, ls_flag_tipo_bcl
boolean			lb_quan_variante, lb_almeno_1_elaborato
datastore		lds_reparti
datetime			ldt_data_pronto, ldt_date_pronto[]
long				ll_new
uo_produzione	luo_prod



as_reparti[] = ls_vuoto[]
ad_tempo_reparto[] = ld_vuoto[]
ad_tempo_reparto_rc[] = ld_vuoto[]
ll_new = 0
ls_riga = string(ai_anno_ordine) +"/"+ string(al_num_ordine) +"/"+ string(al_riga_ordine)

luo_prod = create uo_produzione
luo_prod.uof_trova_tipo(ai_anno_ordine, al_num_ordine, ls_flag_tipo_bcl, as_errore)
as_errore = ""
destroy luo_prod


lb_almeno_1_elaborato = false

if ls_flag_tipo_bcl="A" then
	//estrazione reparti produzione e data pronto a partire dalla riga ordine
	//li leggo dalle varianti
//	ls_sql = g_str.format("select distinct cod_reparto, data_pronto from varianti_det_ord_ven_prod where cod_azienda='$1' and anno_registrazione=$2 and num_registrazione=$3 and prog_riga_ord_ven=$4 and cod_reparto is not null", s_cs_xx.cod_azienda,ai_anno_ordine,al_num_ordine,al_riga_ordine)
//	li_ret = guo_functions.uof_crea_datastore(lds_reparti, ls_sql, as_errore)
	select count(*)
	into	:li_ret
	from varianti_det_ord_ven_prod 
	where cod_azienda=:s_cs_xx.cod_azienda and 
			anno_registrazione=:ai_anno_ordine and 
			num_registrazione=:al_num_ordine and 
			prog_riga_ord_ven=:al_riga_ordine and 
			cod_reparto is not null
	group by cod_reparto, data_pronto;

	ll_counter = 0
	
	if li_ret<0 then
		as_errore = "Errore in conteggio reparti [uof_tempo_reparto]"
		return -1
		
	elseif li_ret=0 then
		//nessun reparto produzione trovato nella tabella varianti_det_ord_ven_prod
		//prova a leggerli da det_ordini_produzione (se è stato fatto il lancio di produzione) altrimenti recuperali dalla distinta base
		
		uof_log("warn", "Nessun Reparto trovato in varianti det. ord. ven per la riga ordine "+ls_riga+". Provo a leggerli dalle fasi di produzione ...")
		
		li_ret = uof_reparti_da_fasi(ai_anno_ordine, al_num_ordine, al_riga_ordine, ls_reparti[], ldt_date_pronto[], as_errore)
		if li_ret<0 then
			return -1
			
		end if
			
		if li_ret=0 then
			//prova allora dalla distinta base ...
			uof_log("warn", "Nessun Reparto trovato nelle fasi di produzione (forse non è stato fatto il lancio prod. riga ordine "+ls_riga+"). Provo a leggerli dalla distinta base ...")
			li_ret = uof_reparti_da_distinta(ai_anno_ordine, al_num_ordine, al_riga_ordine, ls_reparti[], ldt_date_pronto[], as_errore)
			
			if li_ret<0 then
				return -1
				
			elseif li_ret=0 then
				as_errore = "Nessun Reparto trovato neanche dalla distinta base per la riga ordine "+ls_riga+". Questa riga sarà saltata!"
				return 1
			end if
			
		end if
		
	else
		
		declare cu_reparti dynamic cursor for sqlsa;
		
		ls_sql = g_str.format("select distinct cod_reparto, data_pronto from varianti_det_ord_ven_prod where cod_azienda='$1' and anno_registrazione=$2 and num_registrazione=$3 and prog_riga_ord_ven=$4 and cod_reparto is not null", s_cs_xx.cod_azienda, ai_anno_ordine, al_num_ordine,al_riga_ordine)
				
		prepare sqlsa from :ls_sql;
		
		open cu_reparti;
		if sqlca.sqlcode < 0 then 
			close cu_reparti;
			as_errore = "Errore in OPEN cursore [uof_salva_in_calendario()]" + sqlca.sqlerrtext
			return -1
		end if
		
		li_index = 0
		do while true
			fetch cu_reparti into  :ls_cod_reparto, :ldt_data_pronto;
			if sqlca.sqlcode < 0 then 
				close cu_reparti;
				as_errore = "Errore in FETCH  cursore [uof_salva_in_calendario()]" + sqlca.sqlerrtext
				return -1
			end if
			if sqlca.sqlcode = 100 then exit
			
			//carico l'array a partire dal datastore
			ll_new += 1
			ls_reparti[ll_new] = ls_cod_reparto
			ldt_date_pronto[ll_new] = ldt_data_pronto
		loop
		close cu_reparti;
/*		
		for li_index=1 to li_ret
			ll_new += 1
			ls_reparti[ll_new] = lds_reparti.getitemstring(li_index, 1)
			ldt_date_pronto[ll_new] = datetime(date(lds_reparti.getitemdatetime(li_index, 2)), 00:00:00)
		next
*/
	end if
else
	
	select cod_reparto_sfuso, data_pronto_sfuso
	into	:ls_reparti[1], :ldt_date_pronto[1]
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine and
				prog_riga_ord_ven=:al_riga_ordine;
	
	if ls_reparti[1]="" or isnull(ls_reparti[1]) then
		uof_log("warn", "Nessun Reparto trovato in det_ord_ven (tenda tecnica) per la riga ordine "+ls_riga+". Questa riga sarà saltata!")
		return 1
	end if
	
end if



li_ret = upperbound(ls_reparti[])
for li_index=1 to li_ret
	
	ls_cod_reparto = ls_reparti[li_index]
	ldt_data_pronto = ldt_date_pronto[li_index]
	
	//a regime commenta questa chiamata o metti return nella funzione uof_data_pronto_tradizionale
	//#################################################################
	uof_data_pronto_tradizionale(ai_anno_ordine, al_num_ordine, ldt_data_pronto)
	//#################################################################
	
	if isnull(ldt_data_pronto) or year(date(ldt_data_pronto))<1950 then
		uof_log("warn", "Data pronto reparto "+ls_cod_reparto+" non trovata in tabella varianti_det_ord_ven_prod: reparto saltato!")
		continue
	end if
	
	uof_log("info", "Elaborazione Reparto " + ls_cod_reparto)
	
	//se per il reparto in questione esiste il lancio produzione ed il flag fine sessione è S allora escludi dal calcolo
	li_ret2 = uof_controlla_fine_fase(ai_anno_ordine, al_num_ordine, al_riga_ordine, ls_cod_reparto, as_errore)
	if li_ret2=1 then
		//reparto saltato perchè ha finito la produzione
		uof_log("warn", "Reparto " + ls_cod_reparto + " saltato perchè la produzione è terminata!")
		continue
	end if
	
	lb_quan_variante = false
	setnull(ls_cod_gruppo_variante)
	
	select d.cod_prodotto, d.cod_versione, d.quan_ordine, t.cod_giro_consegna
	into :ls_cod_prodotto, :ls_cod_versione, :ld_quan_ordine, :ls_cod_giro_consegna
	from det_ord_ven as d
	join tes_ord_ven as t on t.cod_azienda=d.cod_azienda and
									t.anno_registrazione=d.anno_registrazione and
									t.num_registrazione=d.num_registrazione
	where 	d.cod_azienda=:s_cs_xx.cod_azienda and
				d.anno_registrazione=:ai_anno_ordine and
				d.num_registrazione=:al_num_ordine and
				d.prog_riga_ord_ven=:al_riga_ordine;
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore lettura dati riga ordine (uof_tempo_reparto): "+sqlca.sqlerrtext
		return -1
	end if
	
	ls_flag_rc = "N"
	
	if not isnull(ls_cod_giro_consegna) and ls_cod_giro_consegna<>"" then
		//verifico se il giro è un ritiro a cura del cliente
		select flag_rc
		into   :ls_flag_rc
		from   tes_giri_consegne
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_giro_consegna = :ls_cod_giro_consegna;
	end if
	
	//-----------------------------------------------------------------------------------------------------------
	li_ret2 = uof_tempo_reparto(ls_cod_reparto, ls_cod_prodotto, ls_cod_versione, ld_tempo_reparto, lb_quan_variante, ls_cod_gruppo_variante, as_errore)
	if li_ret2<0 then
		return -1
	elseif li_ret2=1 then
		
		//reparto saltato perchè probabilmente non ci sono tempi di preparazione impostati
		ls_messaggio = "Reparto "+ls_cod_reparto+" saltato: " + as_errore
		
		if not ib_log then
			uof_log_sistema("", ls_messaggio)
		else
			uof_log("warn", ls_messaggio)
		end if
		
		as_errore = ""
		continue
	end if
	
	//moltiplico sempre e comunque per la quantità ordinata
	uof_log("info", "Tempo Reparto (t x qta_ordine) " + string(ld_tempo_reparto) + " x " + string(ld_quan_ordine) +" = " + string(ld_tempo_reparto * ld_quan_ordine) )
	ld_tempo_reparto = ld_tempo_reparto * ld_quan_ordine
	
	//-----------------------------------------------------------------------------------------------------------
	if lb_quan_variante then
		li_ret2 = uof_trova_quan_variante(	ls_cod_prodotto, ls_cod_versione, ls_cod_gruppo_variante, &
													ai_anno_ordine, al_num_ordine, al_riga_ordine, &
													ref ls_cod_prodotto_variante, ref ld_quan_prodotto_variante, ref as_errore)
		if li_ret2<0 then
			return -1
		end if
		
		uof_log("info", "Trovata quantita variante, moltiplico il tempo anche per questa quantità: " + string(ld_tempo_reparto) + " x " + string(ld_quan_prodotto_variante) +" = " + string(ld_tempo_reparto * ld_quan_prodotto_variante) )
		ld_tempo_reparto = ld_tempo_reparto * ld_quan_prodotto_variante										
	end if
	
	// se l'ordine appartiene ad giro RC allora incremento anche il tempo di RC
	if ls_flag_rc = "S" then 
		ld_tempo_reparto_rc = ld_tempo_reparto
	else
		ld_tempo_reparto_rc = 0
	end if
	
	ll_counter += 1
	as_reparti[ll_counter] = ls_cod_reparto
	ad_date_pronto[ll_counter] = ldt_data_pronto
	ad_tempo_reparto[ll_counter] = ld_tempo_reparto
	ad_tempo_reparto_rc[ll_counter] = ld_tempo_reparto_rc
	
	uof_log("log", "Reparto: " + as_reparti[ll_counter] + " Data Pronto: " + string(ad_date_pronto[ll_counter], "dd/mm/yyyy") + &
					" Tempo Reparto: "+string(ad_tempo_reparto[ll_counter], "######0.00") + &
					" Tempo Reparto RC: "+string(ad_tempo_reparto_rc[ll_counter], "######0.00"))
	
	lb_almeno_1_elaborato = true
next

if not lb_almeno_1_elaborato then
	uof_log("log", "Tot. reparti:"+string(li_ret) + ": Nessuno elaborato!")
end if

return 0
end function

public subroutine uof_log_sistema (string as_tipo_log, string as_testo);
if ib_log then return

uo_log_sistema			luo_log

luo_log = create uo_log_sistema

if isnull(as_tipo_log) or as_tipo_log="" then as_tipo_log = is_tipo_log_sistema

luo_log.uof_write_log_sistema_not_sqlca(as_tipo_log, as_testo)

destroy uo_log_sistema

return
end subroutine

public function integer uof_inizio ();date				ldd_data
long				ll_numero, ll_count, ll_index, ll_num_ordine, ll_riga_ordine, li_tipo, ll_perc
datetime			ldt_data_reg_fine, ldt_data_reg_inizio
integer			li_ret, li_anno_ordine
string				ls_ordine, ls_errore, ls_sep


ls_sep = "log"

//impostazioni log
iuo_log.set_log_level(ii_log_level)

if ib_log then
	is_path_log = s_cs_xx.volume + s_cs_xx.risorse + "logs\"+uof_get_giorno_settimana()+"_cal_prod.log"
	
	//prepara il file per la scrittura, pulendo prima eventuali dati già presenti
	iuo_log.set_file( is_path_log, true)
end if


//MOS: numero minuti preparazione riga ordine sfuso ---------------------------------------------------------------------------------------
setnull(id_minuti_sfuso)

select numero
into   :id_minuti_sfuso
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'MOS';

if sqlca.sqlcode <> 0 or id_minuti_sfuso<=0 or isnull(id_minuti_sfuso) then
	ls_errore = "Attenzione; è necessario impostare il parametro aziendale MOS: numero dei minuti necessari per preparare una riga di ordine di sfuso!"
	uof_log("error", ls_errore)
	return -1
end if

uof_log("log", "Letto parametro MOS: "+string(id_minuti_sfuso))


//GVP: predisponi gli intervalli di elaborazione all'interno dei GG di retroattività -----------------------------------------------------------
select numero
into   :ll_numero
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'GVP';

if sqlca.sqlcode < 0 then
	ls_errore = "Errore in lettura parametro GVP: "+sqlca.sqlerrtext
	uof_log("error", ls_errore)
	return -1
	
elseif sqlca.sqlcode=100 or ll_numero<=0 or isnull(ll_numero) then
	ls_errore = "Attenzione; è necessario impostare il parametro GVP: numero di giorni di retroattività visione produzione!"
	uof_log("error", ls_errore)
	return -1
	
end if

uof_log("log", "Letto parametro GVP: "+string(ll_numero) + " giorni di retroattività")


//azzero la tabella di appoggio per l'elaborazione
delete from tab_cal_produzione_temp
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	ls_errore = "Errore in azzeramento tabella calendario produzione temporanea: " + sqlca.sqlerrtext
	uof_log("error", ls_errore)
	rollback;
	return -1
end if

commit;

uof_log("log", "Azzeramento tabella tab_cal_produzione_temp effettuata!")


//calcolo periodo di elaborazione ------------------------------------------------------------------------------------------------------------------
//							la procedura elabora le righe ordine con data_registrazione a partire 
//							dalla data odierna (di sistema) fini a N giorni indietro: parametro aziendale GVP
ldd_data = today()
ldt_data_reg_fine = datetime(ldd_data, 00:00:00)
ldd_data = relativedate(ldd_data, -ll_numero)
ldt_data_reg_inizio = datetime(ldd_data, 00:00:00)
uof_log("log", "Impostato periodo di elaborazione dal "+string(ldt_data_reg_inizio, "dd/mm/yyyy")+" al "+string(ldt_data_reg_fine, "dd/mm/yyyy"))


if uof_prepara_tabella_temporanea(ldt_data_reg_inizio, ldt_data_reg_fine, ls_errore) < 0 then
	//errore critico: interrompi tutto!
	uof_log("error", ls_errore)
	rollback;
	return -1
end if


//conta quante righe ci sono da elaborare --------------------------------------------------------------------------------------------------------
select count(*)
into :ll_count
from tab_cal_produzione_temp
where cod_azienda= :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	ls_errore = "Errore in conteggio righe ordini da elaborare: "+sqlca.sqlerrtext
	uof_log("error", ls_errore)
	return 0
end if

if ll_count=0 or isnull(ll_count) then
	ls_errore = "Nessuna riga ordini da elaborare!"
	uof_log("error", ls_errore)
	return 0
end if


//azzero i valori dei tempi nella tabella calendario ----------------------------------------------------------------------------------------------
update tab_cal_produzione
set    minuti_effettivi = 0, minuti_giro_rc = 0
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	ls_errore = "Errore in azzeramento tabella calendario produzione: " + sqlca.sqlerrtext
	uof_log("error", ls_errore)
	rollback;
	return -1
end if

uof_log("log", "Azzeramento tabella tab_cal_produzione effettuata!")


//azzero la tabella di dettaglio ordini produzione ------------------------------------------------------------------------------------------------
delete from det_ord_ven_prod
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	ls_errore = "Errore in azzeramento tabella det_ord_ven_prod'~r~n" + sqlca.sqlerrtext
	uof_log("error", ls_errore)
	rollback;
	return -1
end if

//se arrivi fin qui fai un commit prima di iniziare il processo di elaborazione ------------------------------------------------------------------
commit;

uof_log("log", "Azzeramento tabella det_ord_ven_prod effettuata e fatto COMMIT!")


//durante il processo, viene fatta un commit ad ogni riga elaborata ...
for ll_index = 1 to ll_count
	
	select 	anno_registrazione,
				num_registrazione,
				prog_riga_ord_ven,
				tipo_ordine
	into			:li_anno_ordine,
					:ll_num_ordine,
					:ll_riga_ordine,
					:li_tipo
	from 	tab_cal_produzione_temp
	where  	cod_azienda=:s_cs_xx.cod_azienda and progressivo=:ll_index;
	
	if sqlca.sqlcode<0 then
		ls_errore = "Errore in lettura da tabella temporanea progressivo "+string(ll_index) + " : "+sqlca.sqlerrtext
		uof_log("error", ls_errore)
		rollback;
		return -1
	end if
	
	ls_errore = ""
	
	ls_ordine = string(li_anno_ordine)+"/"+string(ll_num_ordine)+"/"+string(ll_riga_ordine)
	
	uof_log(ls_sep, "----------------------------------------------------------------------------------------------------------------------------")
	if li_tipo=1 then
		//modello
		uof_log(ls_sep, "Elaborazione Riga ordine Modello "+ls_ordine)
		li_ret =  uof_riga_modello(li_anno_ordine, ll_num_ordine, ll_riga_ordine, ls_errore)
		
	elseif li_tipo=0 then
		//sfuso
		uof_log(ls_sep, "Elaborazione Riga ordine Sfuso "+ls_ordine)
		li_ret = uof_riga_sfuso(li_anno_ordine, ll_num_ordine, ll_riga_ordine, ls_errore)
		
	end if
	
	if li_ret < 0 then
		//errore critico: interrompi tutto!
		uof_log("error", "Riga ordine " + ls_ordine + " : " + ls_errore)
		rollback;
		return -1
		
	elseif li_ret = 1 then
		//riga saltata
		if ls_errore<>"" and not isnull(ls_errore) then uof_log("warn", ls_errore)
		
		uof_log("warn", "Riga ordine "+ls_ordine + " saltata!")
	else
		commit;
		ll_perc = (ll_index / ll_count) * 100
		uof_log(ls_sep, string(ll_perc) + "% Riga ordine "+ls_ordine+" elaborata!")
	end if
next


//aggiorna la data utlima elaborazione del calendario -----------------------------------------------------------------------------------------
ls_errore = ""
if uof_aggiorna_data_elab(ls_errore) < 0 then
	//in tal caso il rollback riguarda solo l'aggiornamento della data elaborazione ... 
	uof_log("error", ls_errore)
	rollback;
	return -1
end if


// fine: il commit riguarda solo l'aggiornameto della data elaborazione (il resto è stato già "COMMITTATO") ----------------------------
commit;
uof_log("warn", "Fine processo elaborazione e fatto COMMIT finale.")


return 0

end function

public subroutine uof_data_pronto_tradizionale (integer ai_anno_ordine, long al_num_ordine, ref datetime adt_data);date				ldd_appoggio
long				ll_gg

if isnull(adt_data) or year(date(adt_data))<1950 then

	select data_consegna
	into :adt_data
	from tes_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine;
	
	if sqlca.sqlcode <> 0 then
		setnull(adt_data)
		return
	end if

	//predisponi la data pronto come 1 GG prima della data consegna
	ll_gg = -1
	adt_data = uof_relativedatetime_cal(ll_gg, adt_data)
	
end if

return
end subroutine

public function boolean uof_controlla_pianificazione (string as_riga, string as_cod_reparto, datetime adt_data_pronto);string					ls_temp
dec{4}				ld_minuti_pianificati

//se nella data pronto il reparto non è pianificato in ore/minuti previti
//allora salta il salvataggio del calendario e in det_ord_ven_prod e logga

//setnull(ls_temp)
//
//select cod_azienda
//into :ls_temp
//from tab_cal_produzione
//where 	cod_azienda = :s_cs_xx.cod_azienda and
//			cod_reparto = :as_cod_reparto and
//			data_giorno = :adt_data_pronto;

select minuti_reparto
into :ld_minuti_pianificati
from tab_cal_produzione
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_reparto = :as_cod_reparto and
			data_giorno = :adt_data_pronto;


//if sqlca.sqlcode=100 or isnull(ls_temp) or ls_temp="" then
if sqlca.sqlcode=100 or isnull(ld_minuti_pianificati) or ld_minuti_pianificati<=0 then
	//reparto non pianificato
	ls_temp = as_cod_reparto + " non pianificato in data " + string(adt_data_pronto, "dd/mm/yyyy")+ ". "+&
					"Il calendario non verrà aggiornato per questo reparto, relativamente a questa riga ordine: rif.ordine "+as_riga
	if not ib_log then
		uof_log_sistema("", ls_temp)
	else
		uof_log("warn", ls_temp)
	end if
	
	return false
end if

return true
end function

public function integer uof_controlla_carico_reparto (string as_cod_reparto, datetime adt_data_pronto, ref string as_colore, ref string as_errore);
dec{4}			ld_minuti_reparto, ld_minuti_eff_reparto, ld_minuti_giro_rc, ld_soglia


adt_data_pronto = datetime(date(adt_data_pronto), 00:00:00)

select	minuti_reparto, 
		minuti_effettivi, 
		minuti_giro_rc
into   	:ld_minuti_reparto, 
		:ld_minuti_eff_reparto, 
		:ld_minuti_giro_rc
from 	tab_cal_produzione
where 	cod_azienda = :s_cs_xx.cod_azienda and  
			cod_reparto = :as_cod_reparto and
			data_giorno = :adt_data_pronto;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in lettura tabella tab_cal_produzione~r~n" + sqlca.sqlerrtext
	return -1
end if

if isnull(ld_minuti_reparto) then ld_minuti_reparto = 0

if ld_minuti_reparto = 0 then
	//REPARTO NON PIANIFICATO NEL GIORNO
	as_colore = ""
	return 0
end if


if isnull(ld_minuti_eff_reparto) then ld_minuti_eff_reparto = 0
if isnull(ld_minuti_giro_rc) then ld_minuti_giro_rc = 0

//leggo soglia reparto (% del potenziale oltre quale segnalare fuori soglia)

//                |                               |   soglia    |
//		---------------------------------------------------------------------------------------
//				0					          P              P* = P + (P * soglia) / 100

select 	soglia_carico
into		:ld_soglia
from   	anag_reparti
where  	cod_azienda = :s_cs_xx.cod_azienda and
			cod_reparto = :as_cod_reparto;
if isnull(ld_soglia) then ld_soglia = 0


ld_soglia = (ld_minuti_reparto * ld_soglia) / 100

ld_soglia = round(ld_soglia / 60, 0)
ld_minuti_eff_reparto =  round(ld_minuti_eff_reparto / 60, 0)
ld_minuti_reparto = round(ld_minuti_reparto / 60, 0)
ld_minuti_giro_rc = round(ld_minuti_giro_rc / 60, 0)


if ld_minuti_eff_reparto > ld_minuti_reparto + ld_soglia then
	//FUORI SOGLIA
	as_colore = "R"
elseif ld_minuti_eff_reparto >= ld_minuti_reparto then
	//IN SOGLIA
	as_colore = "G"
else
	//SOTTO SOGLIA
	as_colore = "V"
end if

return 0
end function

public function string uof_get_giorno_settimana ();choose case daynumber(today())
	case 1
		return "DOM"
	case 2
		return "LUN"
	case 3
		return "MAR"
	case 4
		return "MER"
	case 5
		return "GIO"
	case 6
		return "VEN"
	case else
		return "SAB"
end choose
end function

public subroutine uof_get_reparto_sfuso (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_cod_reparto, ref string as_cod_prodotto);string						ls_cod_reparti_trovati[], ls_errore

setnull(as_cod_prodotto)
setnull(as_cod_reparto)

select cod_prodotto
into :as_cod_prodotto
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_ordine and
			num_registrazione=:al_num_ordine and
			prog_riga_ord_ven=:al_riga_ordine;

if isnull(as_cod_prodotto) or as_cod_prodotto="" then
	as_cod_prodotto = "<null>"
	return
end if

					
//legge il reparto associato in anagrafica prodotti ---------
select cod_reparto
into   :as_cod_reparto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:as_cod_prodotto;

//nessuna gestione dell'errore ....
if sqlca.sqlcode < 0 then
	setnull(as_cod_reparto)
	return
end if
if isnull(as_cod_reparto) or as_cod_reparto="" then
	setnull(as_cod_reparto)
	return
end if


//verifica se esiste eccezione
if f_reparti_stabilimenti(	ai_anno_ordine, al_num_ordine, as_cod_prodotto, "", as_cod_reparto, "", "", ls_cod_reparti_trovati[], ls_errore)<0 then
	setnull(as_cod_reparto)
	return
end if

if upperbound(ls_cod_reparti_trovati[]) > 0 then
else
	return
end if

as_cod_reparto = ls_cod_reparti_trovati[1]

return
end subroutine

public function integer uof_reparti_da_fasi (integer ai_anno_ordine, long al_numero_ordine, long al_riga_ordine, ref string as_reparti[], ref datetime adt_date_pronto[], ref string as_errore);string		ls_cod_reparto
long		ll_index
integer	li_ret
datetime	ldt_null

ll_index = 0
li_ret = 0
setnull(ldt_null)

declare cu_fasi_reparto cursor for  
select cod_reparto
from det_ordini_produzione
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ai_anno_ordine and
			num_registrazione = :al_numero_ordine and
			prog_riga_ord_ven = :al_riga_ordine
order by progr_det_produzione asc;
			
open cu_fasi_reparto;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in apertura cursore cu_fasi_reparto: " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_fasi_reparto into :ls_cod_reparto;

	if sqlca.sqlcode < 0 then
		as_errore = "Errore in fetch cursore cu_fasi_reparto: " + sqlca.sqlerrtext
		close cu_fasi_reparto;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	if ls_cod_reparto<>"" and not isnull(ls_cod_reparto) then
		ll_index += 1
		as_reparti[ll_index] = ls_cod_reparto
		
		//metto null nell'array delle date pronto
		//successivamente metterò data_consegna - 1GG lavorativo
		adt_date_pronto[ll_index] = ldt_null
		
	end if

loop

close cu_fasi_reparto;

return upperbound(as_reparti[])
 
 
 
end function

public function integer uof_reparti_da_distinta (integer ai_anno_ordine, long al_numero_ordine, long al_riga_ordine, ref string as_reparti[], ref datetime adt_date_pronto[], ref string as_errore);string					ls_cod_prodotto, ls_cod_versione
uo_funzioni_1		luo_funzioni_1
integer				li_ret
datetime				ldt_null

setnull(ldt_null)

select cod_prodotto, cod_versione
into :ls_cod_prodotto, :ls_cod_versione
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_ordine and
			num_registrazione=:al_numero_ordine and
			prog_riga_ord_ven=:al_riga_ordine;
			
if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura da det_ord_ven (uof_reparti_da_distinta) : "+sqlca.sqlerrtext
	return -1
end if

luo_funzioni_1 = create uo_funzioni_1
li_ret = luo_funzioni_1.uof_trova_reparti(true, ai_anno_ordine, al_numero_ordine, al_riga_ordine, ls_cod_prodotto, ls_cod_versione, &
														 as_reparti[], as_errore)
destroy luo_funzioni_1

if li_ret < 0 then
	as_errore = "Errore in ricerca reparti (uof_reparti_da_distinta:uof_trova_reparti): " + as_errore
	return -1
end if


//metto null nell'array delle date pronto
//successivamente metterò data_consegna - 1GG lavorativo
for li_ret=1 to upperbound(as_reparti[])
	adt_date_pronto[li_ret] = ldt_null
next


return upperbound(as_reparti[])
 
 
 
end function

public function integer uof_cancella (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore);//utile in fase di cancellazione riga ordine -------------------------------------------------------------

//elimina il contributo al calendario produzione della riga ordine nelle rispettive date
//e successivamente cancella le rispettive righe dalla det_ord_ven_prod

integer				li_ret

//disimpegna il calendario --------
li_ret = uof_disimpegna_calendario(ai_anno_ordine, al_num_ordine, al_riga_ordine, as_errore)
if li_ret < 0 then
	return -1
end if

//elimina da det_ord_ven_prod
delete from det_ord_ven_prod
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_ordine and
			num_registrazione=:al_num_ordine and
			prog_riga_ord_ven=:al_riga_ordine;


if sqlca.sqlcode<0 then
	return -1
end if


return 0
end function

public function integer uof_cancella (integer ai_anno_ordine, long al_num_ordine, ref string as_errore);//utile in fase di cancellazione testata ordine -------------------------------------------------------------

//elimina il contributo al calendario produzione di tutte le righe dell'ordine
//e successivamente cancella le rispettive righe dalla det_ord_ven_prod

integer				li_ret
long					ll_riga_ordine


declare cu_righe cursor for  
	select prog_riga_ord_ven
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine;

open cu_righe;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore cu_righe (uof_cancella): " + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_righe into :ll_riga_ordine;
	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore in fetch cursore cu_righe  (uof_cancella):" + sqlca.sqlerrtext
		close cu_righe;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	li_ret = uof_cancella(ai_anno_ordine, al_num_ordine, ll_riga_ordine, as_errore)
	
	if li_ret < 0 then
		close cu_righe;
		return -1
	end if
loop

close cu_righe;

return 0
end function

public function datetime uof_relativedatetime_cal (long al_gg, datetime adt_data_valore);string ls_cod_reparto

setnull(ls_cod_reparto)

return uof_relativedatetime_cal(ls_cod_reparto, al_gg, adt_data_valore)

end function

public function date uof_relativedate_cal (long al_gg, date add_data_valore);string	ls_cod_reparto


setnull(ls_cod_reparto)

return date( uof_relativedatetime_cal(ls_cod_reparto, al_gg,  datetime(add_data_valore,00:00:00)))
end function

public function datetime uof_relativedatetime_cal (string as_cod_reparto, long al_gg, datetime adt_data_valore);
date				ldt_data
integer			li_count, li_step
dec{4}			ld_minuti_reparto
datetime			ldt_appo


//sposta di N giorni una data, in avanti o indietro.
//Se capita di sabato o di domenica o è una festività comandata allora continuo a spostare, senza considerare questo giorno

if al_gg = 0 then
	//non modificare la data ...
	return adt_data_valore
end if

//-1 oppure +1
li_step = sign(al_gg)

ldt_data = date(adt_data_valore)

do while true
	
	//eseguo spostamento di 1 GG (in avanti o indietro)
	ldt_data = relativedate(ldt_data, li_step) 
	
	//verifica se il giorno in esame è presente come festività o se è una DOMENICA ...
	setnull(li_count)
	
	select count(*)
	into :li_count
	from tab_cal_ferie
	where cod_azienda=:s_cs_xx.cod_azienda and
			data_giorno=:ldt_data and
			flag_tipo_giorno = 'F';
	
	//######################################################################################
	if sqlca.sqlcode=100 or isnull(li_count) or li_count>0 or daynumber(ldt_data) = 1 then
		//presente nelle festività oppure trattasi di Domenica, quindi non conteggiarlo ma continua a spostare
		continue
	
	//######################################################################################
	elseif daynumber(ldt_data) = 7 then
		//si tratta di un sabato -------------------------------------
		//non conteggiarlo ma continua a spostare
		continue

	//######################################################################################
	else
		//nè festività nè sabato nè domenica
		//quindi conteggia lo sostamento, il loop verificherà se hai terminato il conteggio
		
		//se devo andare avanti di 2 giorni, li_step sarà positivo ma lo sarà inpartenza anche al_gg (+2)
		//quindi ad esempio  								al_gg = 2 - (+1) = 1
		//al successivo passaggio avrò					al_gg = 1 - (+1) = 0			e avrò finito ...
		//--------------------------------------------------------------------------------------------------------------------
		//se devo andare indietro di 2 giorni, li_step sarà negativo ma lo sarà inpartenza anche al_gg (-2)
		//quindi ad esempio  								al_gg = -2 - (-1) = -1
		//al successivo passaggio avrò					al_gg = -1 - (-1) = 0			e avrò finito ...
		al_gg = al_gg - li_step
	end if
	
	if al_gg = 0 then
		exit
	end if
	
loop

//torno la data ottenuta
adt_data_valore = datetime(date(ldt_data), 00:00:00)


return adt_data_valore




//date				ldt_data
//integer			li_count, li_step
//dec{4}			ld_minuti_reparto
//datetime			ldt_appo
//
//
////sposta di N giorni una data, in avanti o indietro.
////Se capita di sabato o di domenica o è una festività comandata allora continuo a spostare
//
////se di sabato e l'argomento reparto non è NULL, prima di saltare il sabato verifica se il reparto
////quel sabato per il reparto è pianificato come lavorativo
//
//if al_gg = 0 then
//	//non modificare la data ...
//	return adt_data_valore
//end if
//
////-1 oppure +1
//li_step = sign(al_gg)
//
//ldt_data = date(adt_data_valore)
//
////eseguo spostamento
//ldt_data = relativedate(ldt_data, al_gg) 
//
//
////verifico se cade di sabato, domenica o festivi (tab_cal_ferie)
//do while true
//	
//	
//	//verifica se il giorno in esame è presente come festività o se è una DOMENICA ...
//	setnull(li_count)
//	
//	select count(*)
//	into :li_count
//	from tab_cal_ferie
//	where cod_azienda=:s_cs_xx.cod_azienda and
//			data_giorno=:ldt_data and
//			flag_tipo_giorno = 'F';
//	
//	//######################################################################################
//	if sqlca.sqlcode=100 or isnull(li_count) or li_count>0 or daynumber(ldt_data) = 1 then
//		//presente nelle festività oppure Domenica sposta in avanti o indietro di 1 GG -----------------------------------
//		ldt_data = relativedate(ldt_data, li_step)
//	
//	//######################################################################################
//	elseif daynumber(ldt_data) = 7 then
//		//si tratta di un sabato -------------------------------------
//		
//		//chiesto da Alberto il 28/05/2013
//		ldt_data = relativedate(ldt_data, li_step)
//		
////		//se ho passato alla funzione il reparto, verifico se per caso il reparto è pianificato di sabato
////		//se SI allora accetto il giorno
////		if isnull(as_cod_reparto) or as_cod_reparto="" then
////			//NON ho passato il reparto, quindi il sabato viene saltato a-priori
////			ldt_data = relativedate(ldt_data, li_step)
////			
////		else
////			//verifica se il reparto è pianificato per caso di sabato
////			ldt_appo = datetime(ldt_data, 00:00:00)
////			
////			select minuti_reparto
////			into :ld_minuti_reparto
////			from tab_cal_produzione
////			where 	cod_azienda=:s_cs_xx.cod_azienda and
////						cod_reparto=:as_cod_reparto and
////						data_giorno=:ldt_appo;
////			
////			if sqlca.sqlcode<>0 or ld_minuti_reparto<=0 or isnull(ld_minuti_reparto) then
////				//il reparto non è pianificato in questo sabato ... quindi salta
////				ldt_data = relativedate(ldt_data, li_step)
////			else
////				//risulta pianificato
////				//accetta la data
////				exit
////			end if
////			
////		end if
//	
//	//######################################################################################
//	else
//		//nè festività nè sabato nè domenica
//		//accetta la data
//		exit
//	end if
//loop
//
////torno la data ottenuta
//adt_data_valore = datetime(date(ldt_data), 00:00:00)
//
//
//return adt_data_valore
end function

public function date uof_relativedate_cal (string as_cod_reparto, long al_gg, date add_data_valore);
return date( uof_relativedatetime_cal(as_cod_reparto, al_gg,  datetime(add_data_valore,00:00:00)  )  )



end function

public function integer uof_dw_cal_produzione (boolean ab_esiste_riga_documento, integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_cod_prodotto, string as_cod_versione, datetime adt_data_partenza, ref string as_rep_par[], ref datetime adt_date_part[], ref datawindow adw_trasferimenti, ref string as_errore);uo_produzione					luo_prod
string								ls_dep_par[], ls_dep_arr[], ls_rep_arr[], ls_str, ls_colore
date								ldt_dtp_rep_par[], ldt_dtp_rep_arr[]
integer							li_ret
datetime							ldt_vuoto[]
long								ll_i, ll_row



luo_prod = create uo_produzione

if not isnull(adt_data_partenza) and year(date(adt_data_partenza))>2000 then
	luo_prod.idt_data_partenza = adt_data_partenza
end if

//if ab_esiste_riga_documento then
//	li_ret = luo_prod.uof_cal_trasferimenti(	ai_anno_ordine, al_num_ordine, al_riga_ordine, &
//														ls_dep_par[], as_rep_par[], ldt_dtp_rep_par[], ls_dep_arr[], ls_rep_arr[], ldt_dtp_rep_arr[], as_errore)
//														
//else
//	li_ret = luo_prod.uof_cal_trasferimenti(	ai_anno_ordine, al_num_ordine, as_cod_prodotto, as_cod_versione, &
//														ls_dep_par[], as_rep_par[],ldt_dtp_rep_par[],ls_dep_arr[],ls_rep_arr[],ldt_dtp_rep_arr[], as_errore)
//
//end if
if ab_esiste_riga_documento then
	luo_prod.il_riga_ordine = al_riga_ordine
	
	if isnull(as_cod_prodotto) or as_cod_prodotto="" then
	
		select 	cod_prodotto,
					cod_versione
		into		:as_cod_prodotto,
					:as_cod_versione
		from		det_ord_ven
		where		cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ai_anno_ordine and
					num_registrazione = :al_num_ordine and
					prog_riga_ord_ven = :al_riga_ordine;
	end if
	
end if
li_ret = luo_prod.uof_cal_trasferimenti(	ai_anno_ordine, al_num_ordine, as_cod_prodotto, as_cod_versione, &
														ls_dep_par[], as_rep_par[],ldt_dtp_rep_par[],ls_dep_arr[],ls_rep_arr[],ldt_dtp_rep_arr[], as_errore)	





//se luo_prod.ib_rielaborato è FALSE vuol dire che la funziona ha rielaboreto tutto, quindi i dati non sono letti dalle varianti o dalla det_ord_ven
ib_rielaborato = luo_prod.ib_rielaborato

destroy luo_prod


adw_trasferimenti.reset()

adt_date_part[] = ldt_vuoto[]
for ll_i = 1 to upperbound(ldt_dtp_rep_par[])
	adt_date_part[ll_i] = datetime(ldt_dtp_rep_par[ll_i], 00:00:00)
next

if li_ret < 0 then
	ll_row = adw_trasferimenti.insertrow(0)
	adw_trasferimenti.setitem(ll_row, "testo", as_errore)
else
	//for ll_i = 1 to upperbound(ls_dep_par[])
	for ll_i = upperbound(ls_dep_par[]) to 1 step -1

//		ls_str = 	"PARTENZA:~nDEPOSITO=" + ls_dep_par[ll_i] + "~rREPARTO=" +  + as_rep_par[ll_i] + "~nDATA=" + string(ldt_dtp_rep_par[ll_i], "dd/mm/yy") + &
//					"~nARRIVO:~nDEPOSITO=" + ls_dep_arr[ll_i] + "~rREPARTO=" +  + ls_rep_arr[ll_i] + "~nDATA=" + string(ldt_dtp_rep_arr[ll_i], "dd/mm/yy") 

		ll_row = adw_trasferimenti.insertrow(0)
		adw_trasferimenti.setitem(ll_row, "reparto_partenza", as_rep_par[ll_i])
		adw_trasferimenti.setitem(ll_row, "reparto_arrivo",  ls_rep_arr[ll_i])
		
		
		//adw_trasferimenti.setitem(ll_row, "deposito_partenza", ls_dep_par[ll_i])
		select left(des_deposito, 3)
		into :ls_str
		from anag_depositi
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_deposito=:ls_dep_par[ll_i];
		adw_trasferimenti.setitem(ll_row, "deposito_partenza", ls_str)
		
		//adw_trasferimenti.setitem(ll_row, "deposito_arrivo", ls_dep_arr[ll_i])
		select left(des_deposito, 3)
		into :ls_str
		from anag_depositi
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_deposito=:ls_dep_arr[ll_i];
					adw_trasferimenti.setitem(ll_row, "deposito_arrivo", ls_str)
		
		adw_trasferimenti.setitem(ll_row, "data_partenza", date(ldt_dtp_rep_par[ll_i]))
		adw_trasferimenti.setitem(ll_row, "data_arrivo", date(ldt_dtp_rep_arr[ll_i]))
		
		//------------------------------------------------------------------------------------------------
		//Donato 24/04/2013
		//visualizzo semaforo produzione
		li_ret = uof_controlla_carico_reparto(as_rep_par[ll_i], datetime(ldt_dtp_rep_par[ll_i], 00:00:00), ls_colore, as_errore)
		
		if li_ret < 0 then
			g_mb.warning(as_errore)
		else
			adw_trasferimenti.setitem(ll_row, "semaforo", ls_colore)
		end if
		//------------------------------------------------------------------------------------------------
	next
	
	adw_trasferimenti.sort()
	
end if


return 0

end function

public function integer uof_salva_in_calendario (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_errore);//questa funzione va chiamata per inserire i dati nel calendario e in det_ord_ven_prod su base riga ordine
//se necessario pulisce dati esistenti, disimpegnando il calendario nelle date precedenti e re-inserendo i nuovi dati, in giorni eventualmente diversi ...
//utile quando non si conoscono i reparti e le date pronto, in quanto questa funzione se li va a leggere da sola dalle varianti_det_ord_ven_prod ...

//PERTANTO CHIAMARE QUESTA FUNZIONE SOLO SE SI TRATTA DI UNA RIGA DI ORDINE TENDA DA SOLE

long				li_ret, li_index, li_new
dec{4}			ld_minuti_reparto, ld_minuti_effettivi, ld_minuti_giro_rc
string				ls_temp, ls_riga, ls_reparti[], ls_sql, ls_cod_tipo_ord_ven, ls_flag_tipo_bcl, ls_cod_reparto
datetime			ldt_date_pronto[], ldt_data_pronto
datastore		lds_reparti



//recupero reparti e relative date pronto (varianti_det_ord_ven_prod oppure det_ord_ven)
select cod_tipo_ord_ven
into :ls_cod_tipo_ord_ven
from tes_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and 
			anno_registrazione=:ai_anno_ordine and 
			num_registrazione=:al_num_ordine;
			
select flag_tipo_bcl
into :ls_flag_tipo_bcl
from tab_tipi_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and 
			cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;


if ls_flag_tipo_bcl="A" then
	//TENDA da SOLE  ##############################################################
	//estrazione reparti produzione e data pronto a partire dalla riga ordine
	//li leggo dalle varianti
	/*
	ls_sql = g_str.format("select distinct cod_reparto, data_pronto from varianti_det_ord_ven_prod where cod_azienda='$1' and anno_registrazione=$2 and num_registrazione=$3 and prog_riga_ord_ven=$4 and cod_reparto is not null", s_cs_xx.cod_azienda, ai_anno_ordine, al_num_ordine,al_riga_ordine)
	li_ret = guo_functions.uof_crea_datastore(lds_reparti, ls_sql, as_errore)
	if li_ret<0 then
		return -1
		
	elseif  li_ret=0 then
		//Non ho trovato niente in varianti_det_ord_ven_prod: provo a leggerli dalle fasi di produzione
		as_errore = "Nessun reparto trovato in det_ord_ven_prod!"
		return -1
		
	else
		//carico l'array a partire dal datastore
		for li_index=1 to li_ret
			li_new += 1
			ls_reparti[li_new] = lds_reparti.getitemstring(li_index, 1)
			ldt_date_pronto[li_new] = datetime(date(lds_reparti.getitemdatetime(li_index, 2)), 00:00:00)
		next
		destroy lds_reparti
		
	end if
	*/
	declare cu_reparti dynamic cursor for sqlsa;
	
	ls_sql = g_str.format("select distinct cod_reparto, data_pronto from varianti_det_ord_ven_prod where cod_azienda='$1' and anno_registrazione=$2 and num_registrazione=$3 and prog_riga_ord_ven=$4 and cod_reparto is not null", s_cs_xx.cod_azienda, ai_anno_ordine, al_num_ordine,al_riga_ordine)
			
	prepare sqlsa from :ls_sql;
	
	open cu_reparti;
	if sqlca.sqlcode < 0 then 
		close cu_reparti;
		as_errore = "Errore in OPEN cursore [uof_salva_in_calendario()]" + sqlca.sqlerrtext
		return -1
	end if
	
	li_index = 0
	do while true
		fetch cu_reparti into  :ls_cod_reparto, :ldt_data_pronto;
		if sqlca.sqlcode < 0 then 
			close cu_reparti;
			as_errore = "Errore in FETCH  cursore [uof_salva_in_calendario()]" + sqlca.sqlerrtext
			return -1
		end if
		if sqlca.sqlcode = 100 then exit
		
		//carico l'array a partire dal datastore
		li_new += 1
		ls_reparti[li_new] = ls_cod_reparto
		ldt_date_pronto[li_new] = ldt_data_pronto
	loop
	close cu_reparti;
else
	as_errore = "Tipo ordine non tra quelli previsti (solo tipo A)!"
	return -1
end if



//pulizia eventuale dalle tabelle calendario e det_ord_ven_prod ------------------------------------------------------------------------------
li_ret = uof_disimpegna_calendario(ai_anno_ordine, al_num_ordine, al_riga_ordine, as_errore)
if li_ret < 0 then
	return -1
end if

delete from det_ord_ven_prod
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_ordine and
			num_registrazione=:al_num_ordine and
			prog_riga_ord_ven=:al_riga_ordine;

if sqlca.sqlcode<0 then
	as_errore = "Errore in cancellazione dati da tabella det_ord_ven_prod: "+sqlca.sqlerrtext
	return -1
end if

//per ogni reparto
//			calcola i minuti necessari alla preparazione
//			aggiorna il calendario nella data pronto per il reparto
//			aggiorna la det_ord_ven_prod relativamente al reparto e data pronto
for li_index=1 to upperbound(ls_reparti[])
	
	ls_riga = string(ai_anno_ordine)+"/"+string(al_num_ordine)+"/"+string(al_riga_ordine)
	
	//NOTA ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//se nella data pronto il reparto non è pianificato in ore/minuti previti
	//allora salta il salvataggio del calendario e in det_ord_ven_prod e logga
	if not uof_controlla_pianificazione(ls_riga, ls_reparti[li_index], ldt_date_pronto[li_index]) then
		//salta il reparto. Log già scritto
		continue
	end if
	
	
	//calcola i dati relativi a minuti ----------------------------------------------------------------------------------------------------------------------------------------------
	//NOTA
	//se non trova la combinazione prodotto-reparto o prodotto-reparto-gruppo variante
	//salta il salvataggio del calendario e in det_ord_ven_prod e logga
	li_ret = uof_get_minuti_reparto(	ai_anno_ordine, al_num_ordine, al_riga_ordine, ls_reparti[li_index], ldt_date_pronto[li_index], &
												ld_minuti_effettivi, ld_minuti_giro_rc, as_errore)
	if li_ret<0 then
		return -1
		
	elseif li_ret = 1 then
		//minuti non indicati in tabella tempi calendario
		if not ib_log then
			uof_log_sistema("", as_errore)
		else
			uof_log("warn", as_errore)
		end if
		
		continue
	end if
	
	
	//aggiorna calendario ---------------------------------------------------------------------------------------------------------------------------------------------------------
	li_ret = uof_aggiorna_tabella_calendario(ls_reparti[li_index], ldt_date_pronto[li_index], ld_minuti_effettivi, ld_minuti_giro_rc, as_errore)
	if li_ret<0 then
		return -1
	end if
	
	
	//leggo i minuti totali pianificati per il reparto nel calendario produzione ------------------------------------------------------------------------------------------------
	setnull(ld_minuti_reparto)
	
	select minuti_reparto
	into :ld_minuti_reparto
	from tab_cal_produzione
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_reparto = :ls_reparti[li_index] and
			data_giorno = :ldt_date_pronto[li_index];
	if isnull(ld_minuti_reparto) then ld_minuti_reparto = 0
	
	
	//aggiorna det_ord_ven_prod con il reparto corrente -----------------------------------------------------------------------------------------------------------------------
	li_ret = uof_aggiorna_detordven_prod(	ai_anno_ordine, al_num_ordine, al_riga_ordine, &
														ls_reparti[li_index], ld_minuti_reparto, ld_minuti_effettivi, ld_minuti_giro_rc, ldt_date_pronto[li_index], as_errore)
	if li_ret<0 then
		return -1
	end if
	
next



return 0
end function

public function string uof_leggi_ultima_elab ();string  ls_return
datetime ldt_data_ultima_elab, ldt_ora_ultima_elab

ldt_data_ultima_elab = datetime(today(), 00:00:00)
ldt_ora_ultima_elab = datetime(date(1900, 1, 1), now())

select 	data_ultima_elab, ora_ultima_elab
into 		:ldt_data_ultima_elab, :ldt_ora_ultima_elab
from tab_cal_produzione_elab
where cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode = 0 and not isnull(ldt_data_ultima_elab) and year(date(ldt_data_ultima_elab)) > 1950 and not isnull(ldt_ora_ultima_elab) then
	ls_return = "il " + string(date(ldt_data_ultima_elab), "dd/mm/yyyy")
	ls_return += " alle "
	ls_return += string(time(ldt_ora_ultima_elab), "HH:MM")
else
	return "Mai"
end if

return ls_return

end function

public function integer uof_imposta_varianti_prod_da_backup (long al_anno_documento, long al_num_documento, long al_prog_riga_documento, ref datastore ads_data_backup);/*
Questa funzione viene chiamata, quando si vuole tentare di re-impostare la tabella varianti_det_ord_ven_prod allo stato che aveva
prima dell'inizio della configurazione. E' un tentativo che andrà a buon fine solo se la tabella varianti_det_ord_ven non è cvambiata
Cioè se è stata creata manualmente una variante dalla finestra delle varianti e poi si rientra nel configuratore sicuramente
questa funzione non ce la farà e quindi verranno re-impostate lòe varianti lette dalla distinta base e non quelle salvate prima dell'inizio della configurazione
*/




return 0

end function

public function integer uof_aggiorna_varianti_e_calendario (datetime adt_data_partenza, long al_anno_documento, long al_num_documento, long al_singola_riga, ref string as_reparti[], ref string as_errore);string									ls_dep_par[], ls_rep_par[], ls_dep_arr[], ls_rep_arr[], ls_vuoto[], ls_cod_prod, ls_cod_vers, ls_riga_ordine
date									ldt_dtp_rep_par[], ldt_dtp_rep_arr[], ldd_vuoto[]
integer								li_ret, ll_index, ll_i
uo_produzione						luo_prod
uo_calendario_prod_new			luo_cal_prod
string									ls_cod_prodotto[], ls_cod_versione[], ls_flag_tipo_bcl
long									ll_righe_ordine[], ll_temp
integer								li_i
datetime								ldt_vuoto[], ldt_date_reparti[]
datastore							ids_backup_vardetordven_prod



if al_singola_riga>0  then
	//solo riga corrente
	ll_righe_ordine[1] = al_singola_riga
	
	select cod_prodotto, cod_versione
	into :ls_cod_prodotto[1], :ls_cod_versione[1]
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:al_anno_documento and
				num_registrazione=:al_num_documento and
				prog_riga_ord_ven=:ll_righe_ordine[1];
	
else
	//il ricalcolo riguarda tutto l'ordine
	
	declare cu_righe cursor for  
	select prog_riga_ord_ven, cod_prodotto, cod_versione
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:al_anno_documento and
				num_registrazione=:al_num_documento and
				cod_prodotto is not null and cod_versione is not null and cod_versione <>'' and
				(num_riga_appartenenza=0 or num_riga_appartenenza is null);

	open cu_righe;

	if sqlca.sqlcode < 0 then
		as_errore = "Errore in OPEN cursore cu_righe (wf_aggiorna_varianti_e_calendario): " + sqlca.sqlerrtext
		return -1
	end if

	do while true
		
		fetch cu_righe into :ll_temp, :ls_cod_prod, :ls_cod_vers;
	
		if sqlca.sqlcode < 0 then
			as_errore = "Errore in fetch cursore cu_righe  (wf_aggiorna_varianti_e_calendario):" + sqlca.sqlerrtext
			close cu_righe;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then exit
		
		li_i += 1
		ll_righe_ordine[li_i] = ll_temp
		ls_cod_prodotto[li_i] = ls_cod_prod
		ls_cod_versione[li_i] = ls_cod_vers
		
	loop
	close cu_righe;
end if
//----------------------------------------------------------------------


luo_prod = create uo_produzione
luo_cal_prod = create uo_calendario_prod_new


luo_prod.uof_trova_tipo(integer(al_anno_documento), al_num_documento, ls_flag_tipo_bcl, as_errore)
as_errore = ""

//controlla se hai deciso di spostare la data partenza dell'intero ordine
if not isnull(adt_data_partenza) and year(date(adt_data_partenza))>1950 then
	luo_prod.idt_data_partenza = adt_data_partenza
end if

as_reparti[] = ls_vuoto[]

for li_i=1 to upperbound(ll_righe_ordine[])
	ls_riga_ordine = "Riga Ordine "+string(al_anno_documento)+"/"+string(al_num_documento)+"/"+string(ll_righe_ordine[li_i])+"~r~n~r~n"

	ls_dep_par[] = ls_vuoto[]
	ls_rep_par[] = ls_vuoto[]
	ls_dep_arr[] = ls_vuoto[]
	ls_rep_arr[] = ls_vuoto[]
	
	ldt_dtp_rep_par[] = ldd_vuoto[]
	ldt_dtp_rep_arr[] = ldd_vuoto[]
	
	luo_prod.il_riga_ordine = -1
	
	li_ret = luo_prod.uof_cal_trasferimenti(	al_anno_documento, al_num_documento, &
														ls_cod_prodotto[li_i], ls_cod_versione[li_i], &
														ls_dep_par[], ls_rep_par[], ldt_dtp_rep_par[], ls_dep_arr[], ls_rep_arr[], ldt_dtp_rep_arr[], as_errore)
	
	if ll_righe_ordine[li_i]=str_conf_prodotto.prog_riga_documento then
		as_reparti[] = ls_rep_par[]
	end if
	
	
	//se non ci sono errori salvo -------------------------------
	//se ci sono errori, in questa fase me ne frego ... o meglio inutile che vado avanti
	//probabilmente non hai trovato navette o eventi
	if li_ret = 0 then
		//se torna ZERO sicuramente ho le date e i reparti
		
		ldt_date_reparti[] =  ldt_vuoto[]
		
		for ll_index = 1 to upperbound(ls_rep_par[])
			
			if ls_rep_arr[ll_index]="" then setnull(ls_rep_arr[ll_index])
			
			if ls_flag_tipo_bcl = "A" then
/*				
				update varianti_det_ord_ven_prod
				set		data_pronto = :ldt_dtp_rep_par[ll_index],
							data_arrivo = :ldt_dtp_rep_arr[ll_index],
							cod_reparto_arrivo = :ls_rep_arr[ll_index]
				where 	anno_registrazione = :al_anno_documento and 
							num_registrazione = :al_num_documento and
							prog_riga_ord_ven = :ll_righe_ordine[li_i] and
							cod_reparto = :ls_rep_par[ll_index];
*/
				
				update varianti_det_ord_ven_prod
				set		data_pronto = :ldt_dtp_rep_par[ll_index],
							data_arrivo = :adt_data_partenza,
							cod_reparto_arrivo = :ls_rep_arr[ll_index]
				where 	anno_registrazione = :al_anno_documento and 
							num_registrazione = :al_num_documento and
							prog_riga_ord_ven = :ll_righe_ordine[li_i] and
							cod_reparto = :ls_rep_par[ll_index];
				
				if sqlca.sqlcode < 0 then
					as_errore = "Errore in UPDATE tabella 'varianti_det_ord_ven_prod' con data_pronto.~r~n" + sqlca.sqlerrtext
					
					destroy luo_prod
					destroy uo_calendario_prod_new
					return -1
				end if
				
			else
				//tenda tecnica
				update det_ord_ven
				set		cod_reparto_sfuso = :ls_rep_par[ll_index],
						data_pronto_sfuso = :ldt_dtp_rep_par[ll_index],
						cod_reparto_arrivo = :ls_rep_arr[ll_index],
						data_arrivo = :ldt_dtp_rep_arr[ll_index]
				where 	anno_registrazione = :al_anno_documento and 
							num_registrazione = :al_num_documento and
							prog_riga_ord_ven = :ll_righe_ordine[li_i];
				
				if sqlca.sqlcode < 0 then
					as_errore = "Errore in update det_ord_ven (cod_reparto-data_pronto).~r~n" + sqlca.sqlerrtext
					
					destroy luo_prod
					destroy uo_calendario_prod_new
					return -1
				end if
				
			end if
			ldt_date_reparti[ll_index] = adt_data_partenza
//			ldt_date_reparti[ll_index] = datetime(ldt_dtp_rep_par[ll_index] ,00:00:00)
			
		next
	
		//se arrivi fin qui aggiorna l'impegno in calendario
		if upperbound(ls_rep_par[])>0 then
			li_ret = luo_cal_prod.uof_salva_in_calendario(	al_anno_documento, al_num_documento, ll_righe_ordine[li_i], &
																	ls_rep_par[], ldt_date_reparti[], as_errore)
			
			if li_ret < 0 then
				destroy luo_prod
				destroy uo_calendario_prod_new
				return -1
			end if
		end if
		
	else
		as_errore = ls_riga_ordine + as_errore
		destroy luo_prod
		destroy uo_calendario_prod_new
		return 1
	end if

next

destroy luo_prod
destroy uo_calendario_prod_new


return 0
end function

on uo_calendario_prod_new.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_calendario_prod_new.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;iuo_log = create uo_log

setnull(ii_log_level)

select log_level
into :ii_log_level
from tab_cal_produzione_elab
where cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ii_log_level) or ii_log_level<=0 then ii_log_level = 3
if ii_log_level>5 then ii_log_level=5
end event

event destructor;destroy iuo_log
end event

