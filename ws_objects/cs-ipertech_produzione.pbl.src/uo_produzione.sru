﻿$PBExportHeader$uo_produzione.sru
$PBExportComments$Motore di produzione di Ptenda
forward
global type uo_produzione from nonvisualobject
end type
end forward

global type uo_produzione from nonvisualobject
end type
global uo_produzione uo_produzione

type variables
long il_barcode_minimo = 9999
longlong il_sessione_tes_stampa = 0

//valorizzata dopo il create se hasi scelto di cambiare data partenza dell'ordine
datetime	idt_data_partenza

boolean	ib_rielaborato = true
boolean	ib_altro_stabilimento = false

long		il_riga_ordine = -1

boolean	ib_varianti_ok = true
end variables

forward prototypes
public function integer uof_calcola_tempo_fase (long fl_barcode, ref long fl_tempo_totale, ref string fs_errore)
public function integer uof_duplica_sessione (long fl_barcode, long fl_progr_sessione, ref string fs_errore)
public function integer uof_nuova_sessione (long fl_barcode, long fl_progr_sessione, string fs_cod_operaio, ref string fs_errore)
public function integer uof_stato_prod_tes_ord_ven (long al_anno_ord_ven, long al_num_ord_ven, ref string as_messaggio)
public function integer uof_trova_max_barcode (ref long fl_barcode, ref string fs_errore)
public function integer uof_trova_tipo (long fl_barcode, ref string fs_tipo_stampa, ref string fs_errore)
public function integer uof_stato_prod_det_ord_ven (long al_anno_ord_ven, long al_num_ord_ven, long al_riga_ord_ven, ref string as_messaggio)
public function integer uof_stato_prod_sessione (long al_progr_det_produzione, long al_progr_sessione, ref string as_messaggio)
public function integer uof_stato_prod_fase (long al_progr_det_produzione, ref string as_messaggio)
public function integer uof_reparti (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_versione, ref string fs_cod_reparti_trovati[], ref string fs_errore)
public function integer uof_cancella_ar (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_errore)
public function integer uof_elimina_prod_det (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_messaggio)
public function integer uof_elimina_prod_comm (long al_anno_commessa, long al_num_commessa, ref string as_messaggio)
public function integer uof_stato_prod_fase_comp (long al_progr_tes_produzione, ref string as_messaggio)
public function integer uof_inserisci_tes (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_reparti_trovati[], ref string fs_errore)
public function integer uof_aggiorna_colli_tes (long al_progr_tes_produzione, ref string as_messaggio)
public function integer uof_barcode_anno_reg (ref long fl_barcode, ref integer fi_anno_registrazione, ref long fl_num_registrazione, ref long fl_prog_riga_ord_ven, ref string fs_cod_reparto, ref string fs_errore)
public function integer uof_etichette (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_errore)
public function integer uof_aggiorna_colli_det (long al_progr_det_produzione, ref string as_messaggio)
public function integer uof_fine_sessione (long fl_barcode, long fl_progr_sessione, string fs_cod_operaio, ref string fs_errore)
public function integer uof_pronto_complessivo (long fl_barcode, ref string fs_errore)
public function integer uof_pronto_parziale (long fl_barcode, string fs_cod_operaio, ref string fs_errore)
public function integer uof_rileva_produzione (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_reparto, string fs_cod_operaio, ref string fs_errore)
public function integer uof_fine_produzione (long fl_barcode, ref string fs_errore)
public function integer uof_etichette_apertura (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_errore)
public function string uof_trasforma_data (date ld_data)
public function integer uof_stato_prod_det_ord_ven_reparto (long al_anno_ord_ven, long al_num_ord_ven, long al_riga_ord_ven, string as_cod_reparto, ref string as_messaggio)
public function integer uof_stato_prod_det_ord_ven_reparti (long al_anno_ord_ven, long al_num_ord_ven, long al_riga_ord_ven, ref string as_cod_reparto[], ref long al_stato_reparto[], ref string as_messaggio)
public function integer uof_etichette_tecniche (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, ref string fs_errore)
public function integer uof_etichette_produzione_cg (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_errore)
public function integer uof_etichette_cg (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_errore)
public function integer uof_barcode_anno_reg_sfuso (ref long fl_barcode, ref integer fi_anno_registrazione, ref long fl_num_registrazione, ref string fs_errore)
public function integer uof_reimposta_colli (long fl_barcode, ref string fs_errore)
public function integer uof_leggi_colli_ordine (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_colli[], ref string fs_msg)
public function integer uof_carica_colli_barcode (long fl_barcode, ref string fs_colli[], ref string fs_cod_reparto, ref string fs_msg)
public function integer uof_crea_barcode_colli (long fl_barcode, long fl_tot_colli, boolean fb_reimposta_colli, ref string fs_msg)
public function integer uof_etichette_scheda_prodotto (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_reparto, ref string fs_errore)
public function boolean uof_controlla_reparto (long al_anno_registrazione, long al_num_registrazione, string as_cod_deposito, ref string as_errore)
public subroutine uof_controlla_data_pronto (datetime fdt_data_consegna, ref datetime fdt_data_pronto)
public function string uof_get_riga_su_tot_righe (integer fl_anno_reg, long fl_num_reg, long fl_prog_riga)
public function integer uof_reset_produzione (long fl_barcode, ref string fs_errore)
public function integer uof_stato_prod_det_ord_ven_reparti (long al_anno_ord_ven, long al_num_ord_ven, long al_riga_ord_ven, string as_cod_deposito_partenza, ref string as_cod_reparto[], ref long al_stato_reparto[], ref string as_messaggio)
public function integer uof_richiesta_trasf_componenti (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, long fl_barcode, boolean fb_stampa_subito, ref datawindow fdw_report, ref string fs_errore)
public function integer uof_get_ordine_reparti_2 (integer fi_anno_reg, long fl_num_reg, long fl_prog_riga, string fs_reparto_corrente, ref string fs_lista_reparti, ref string fs_stab_rep_prossimo, ref string fs_reparto_successivo)
public function integer uof_get_ordine_reparti (integer fi_anno_reg, long fl_num_reg, long fl_prog_riga, string fs_reparto_corrente, ref string fs_lista_reparti, ref string fs_stab_rep_prossimo, ref string fs_reparto_successivo, ref boolean fb_alert)
public function boolean uof_reparto_successivo_esterno (string fs_reparti_trovati[], long fl_pos_reparto_corrente, string fs_cod_deposito_origine)
public function integer uof_stato_prod_det_ord_ven_trasf (long al_anno_ord_ven, long al_num_ord_ven, long al_prog_riga_ord_ven, string as_deposito_commerciale, string as_reparto_produzione, integer ai_stato_reparto_produzione, ref string as_flag_stato, ref string as_errore)
public function integer uof_controlla_produzione (long fl_num_registrazione, integer fi_anno_registrazione, integer fi_prog_riga_ord_ven, ref string fs_errore)
public function integer uof_get_tipo_det_ven_add (long fl_anno_ordine, long fl_num_ordine, long fl_prog_riga_ord_ven, ref string fs_cod_tipo_det_ven_addizionali, ref string fs_errore)
public function integer uof_commesse_esterne_ordine (integer fi_anno_ordine, long fl_num_ordine, ref string fs_errore)
public function integer uof_commesse_esterne_riga_ordine (boolean fb_da_ordine, integer fi_anno_ordine, long fl_num_ordine, integer fl_riga_ordine, ref string fs_errore)
public function integer uof_elimina_ologrammi (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_messaggio)
public function integer uof_controlla_ologrammi (boolean fb_check, long fl_barcode, ref integer fi_anno_registrazione, ref long fl_num_registrazione, ref long fl_prog_riga_ord_ven, ref string fs_messaggio)
public function integer uof_check_ologramma_altra_riga (string as_codice, integer ai_anno, long al_numero, long al_riga, ref string as_errore)
public function integer uof_check_ologramma_riga (string as_codice, integer ai_anno, long al_numero, long al_riga, ref string as_errore)
public function integer uof_ass_iniziale_ologramma (boolean fb_check, long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_operaio, ref string fs_errore)
public function integer uof_get_riga_ordine_da_ologramma (string as_ologramma, ref integer ai_anno_ordine[], ref long al_num_ordine[], ref long al_riga_ordine[], ref string as_errore)
public function integer uof_stampa_scheda_prodotto (string fs_ologramma, integer fi_anno_registrazione, long fl_num_registrazione, integer fl_prog_riga_ord_ven, ref string fs_errore)
public subroutine uof_get_data_fatt_e_ddt_riga_ordine (integer fi_anno, long fl_numero, long fl_riga, ref datetime fdt_data_fatt, ref datetime fdt_data_ddt)
public function integer uof_get_colli_reparti_prec (long al_barcode, ref string as_reparto_corrente, ref datastore ads_colli, ref string as_errore)
public function integer uof_elimina_colli_reparti_prec (datastore ads_colli, ref string as_errore)
public function integer uof_sospensione (string fs_azione, long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga, string fs_cod_operaio, ref string fs_errore)
public function integer uof_controlla_se_sospesa (long al_barcode, string as_cod_operaio)
public function integer uof_get_sospensioni_reparto (string as_cod_reparto, integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga, ref string as_sigla_sospensione)
public function integer uof_get_sospensioni_riga_ordine (integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga)
public function integer uof_elimina_riga_ordine (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_errore)
public function integer uof_riapri_offerta (long fi_anno_ord_ven, long fl_num_ord_ven, long fl_riga_ord_ven, ref string fs_errore)
public function integer uof_cal_trasferimenti_navetta (date ad_data_arrivo_richiesta, string as_reparto_partenza, string as_reparto_arrivo, string as_deposito_arrivo, ref date ad_data_partenza, ref string as_message)
public function integer uof_cal_trasferimenti (long al_anno_ord_ven, long al_num_ord_ven, string as_cod_prodotto_finito, string as_cod_versione_finito, ref string as_dep_par[], ref string as_rep_par[], ref date add_dtp_rep_par[], ref string as_dep_arr[], ref string as_rep_arr[], ref date add_dtp_rep_arr[], ref string as_message)
public function integer uof_cal_trasferimenti (long al_anno_ord_ven, long al_num_ord_ven, long al_prog_riga_ord_ven, ref string as_dep_par[], ref string as_rep_par[], ref date add_dtp_rep_par[], ref string as_dep_arr[], ref string as_rep_arr[], ref date add_dtp_rep_arr[], ref string as_message)
public function integer uof_get_data_pronto_reparto (long al_barcode, ref datetime adt_data_pronto)
public function integer uof_trova_tipo (integer ai_anno_ordine, long al_num_ordine, ref string as_tipo_stampa, ref string as_errore)
public function integer uof_get_varianti_det_ord_ven_prod (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_dep_par[], ref string as_rep_par[], ref date add_dtp_par[], ref string as_dep_arr[], ref string as_rep_arr[], ref date add_dtp_arr[], ref string as_errore)
public function integer uof_mrp_disimpegna_ordine (long al_barcode, ref string as_errore)
public function integer uof_mrp_disimpegna_ordine (integer al_anno_reg, long al_num_reg, long al_prog_riga_reg, string as_cod_reparto, ref string as_errore)
public subroutine uof_scrivi_log_sistema (string as_flag_log, string as_valore)
public function integer uof_get_posizione (integer ai_anno, long al_numero, long al_riga, string as_cod_reparto, ref string as_posizione, ref string as_errore)
public function boolean uof_check_posizione (integer ai_anno, long al_numero, long al_riga, string as_posizione)
public function integer uof_trova_cliente (long fl_barcode, ref string fs_cod_cliente, ref string fs_errore)
public function string uof_deposito_giro_consegna (string as_cod_giro_consegna)
public function string uof_deposito_giro_consegna (integer ai_anno_ordine, long al_num_ordine)
public function integer uof_allega_docs_produzione (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga, ref string fs_errore)
public function integer uof_inserisci_det (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_reparti_trovati[], decimal fd_quan_ordine, ref string fs_errore)
public function integer uof_lancia_produzione (long fl_num_registrazione, integer fi_anno_registrazione, integer fi_prog_riga_ord_ven, string fs_cod_tipo_ord_ven, datetime fdt_data_consegna, ref string fs_errore)
public function integer uof_elimina_produzione (long fl_num_registrazione, integer fi_anno_registrazione, string fs_origine_comando, ref string fs_errore)
public function integer uof_stato_prod_tes_lancio_prod (long al_id_tes_lancio_prod, ref string as_messaggio)
public function integer uof_crea_matricola (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, decimal al_quan_ordine, ref string as_errore)
public function string uof_genera_matricola (date adt_date, long al_progr_produzione, long al_num_tenda)
public function integer uof_elimina_produzione_riga (long fl_num_registrazione, integer fi_anno_registrazione, integer fi_prog_riga_ord_ven, string fs_origine_comando, ref string fs_errore)
public function integer uof_elimina_commessa (long fl_anno_reg_ordine, long fl_num_reg_ordine, long fl_prog_riga_ord_ven, boolean fb_flag_elimina_commesse_righe_riferite, boolean fb_flag_elimina_dati_produzione, ref string fs_errore)
end prototypes

public function integer uof_calcola_tempo_fase (long fl_barcode, ref long fl_tempo_totale, ref string fs_errore);// Funzione che calcola il tempo totale impiegato per una fase (det_ordini_produzione)
// nome: uof_calcola_tempo_fase
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 09-12-2003
//		Autore Diego Ferrari

datetime ldt_inizio_sessione,ldt_fine_sessione
date ldd_data_inizio,ldd_data_fine
time lt_ora_inizio,lt_ora_fine
long ll_minuti,ll_giorni

declare r_sessioni_lavoro cursor for
select inizio_sessione,
		 fine_sessione
from   sessioni_lavoro
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr_det_produzione=:fl_barcode;

open r_sessioni_lavoro;

do while 1=1
	fetch r_sessioni_lavoro
	into  :ldt_inizio_sessione,
			:ldt_fine_sessione;
			
	if sqlca.sqlcode = 100 then exit
			
	if sqlca.sqlcode < 0 then 
		fs_errore = "Procedura di calcolo tempo fase. Errore sul DB: " + sqlca.sqlerrtext
		close r_sessioni_lavoro;
		return -1
	end if	
	
	ldd_data_inizio = date(ldt_inizio_sessione)
	ldd_data_fine = date(ldt_fine_sessione)
	
	lt_ora_inizio = time(ldt_inizio_sessione)
	lt_ora_fine = time(ldt_fine_sessione)
	
	ll_giorni = ll_giorni + DaysAfter(ldd_data_inizio, ldd_data_fine)
	ll_minuti = ll_minuti + SecondsAfter(lt_ora_inizio, lt_ora_fine)/60
	
loop

close r_sessioni_lavoro;

if isnull(ll_giorni ) then ll_giorni = 0

ll_minuti = ll_minuti + ll_giorni*24*60

fl_tempo_totale = ll_minuti

return 0
end function

public function integer uof_duplica_sessione (long fl_barcode, long fl_progr_sessione, ref string fs_errore);// Funzione che duplica una sessione già esistente lasciandola aperta
// nome: uof_duplica_sessione
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 09-12-2003
//		Autore Diego Ferrari

datetime ldt_dataora
string ls_cod_operaio
long ll_progr_sessione

select cod_operaio_fine,
		 fine_sessione
into   :ls_cod_operaio,
       :ldt_dataora
from   sessioni_lavoro
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr_det_produzione=:fl_barcode
and    progr_sessione=:fl_progr_sessione;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

ll_progr_sessione = fl_progr_sessione + 1


insert into sessioni_lavoro
(cod_azienda,
 progr_det_produzione,
 progr_sessione,
 progr_tes_produzione,
 cod_operaio_inizio,
 cod_operaio_fine,
 inizio_sessione,
 fine_sessione,
 quan_prodotta,
 flag_fine_sessione,
 num_colli)
 values
 (:s_cs_xx.cod_azienda,
  :fl_barcode,
  :ll_progr_sessione,
  null,
  :ls_cod_operaio,
  null,
  :ldt_dataora,
  null,
  0,
  'N',
  0);
  
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

public function integer uof_nuova_sessione (long fl_barcode, long fl_progr_sessione, string fs_cod_operaio, ref string fs_errore);// Funzione che inserisce una nuova sessione di lavoro
// nome: uof_nuova_sessione
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 05-12-2003
//		Autore Diego Ferrari

datetime ldt_adesso

ldt_adesso = datetime(today(),now())

insert into sessioni_lavoro
(cod_azienda,
 progr_det_produzione,
 progr_sessione,
 progr_tes_produzione,
 cod_operaio_inizio,
 cod_operaio_fine,
 inizio_sessione,
 fine_sessione,
 quan_prodotta,
 flag_fine_sessione,
 num_colli)
 values
 (:s_cs_xx.cod_azienda,
  :fl_barcode,
  :fl_progr_sessione,
  null,
  :fs_cod_operaio,
  null,
  :ldt_adesso,
  null,
  0,
  'N',
  0);
  
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

public function integer uof_stato_prod_tes_ord_ven (long al_anno_ord_ven, long al_num_ord_ven, ref string as_messaggio);string  ls_cod_tipo_det_ven, ls_cod_tipo_det_ven_addizionali, ls_cod_tipo_ord_ven, ls_tipo_stampa
long	 ll_aperte, ll_parziali, ll_chiuse, ll_totale, ll_riga_ord_ven, ll_return


ll_aperte = 0

ll_parziali = 0

ll_chiuse = 0

ll_totale = 0

declare det_ord_ven cursor for
select
	prog_riga_ord_ven,
	cod_tipo_det_ven
from
	det_ord_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :al_anno_ord_ven and
	num_registrazione = :al_num_ord_ven and
	cod_prodotto is not null and
	num_riga_appartenenza = 0 and
	flag_blocco = 'N' 
order by
	prog_riga_ord_ven ASC;
	
open det_ord_ven;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in open cursore det_ord_ven:~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch
		det_ord_ven
	into
		:ll_riga_ord_ven,
		:ls_cod_tipo_det_ven;
		
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in fetch cursore det_ord_ven:~n" + sqlca.sqlerrtext
		close det_ord_ven;
		return -1
	elseif sqlca.sqlcode = 100 then
		close det_ord_ven;
		exit
	end if
	
	//###########################################################################################
	//Donato 28/06/2012 se l'ordine è di tipo report 2 (sfuso) salta il controllo sul tipo det ven addizionali, altrimenti il semaforo produzione
	//in testata ordine non funziona
	select cod_tipo_ord_ven
	into   :ls_cod_tipo_ord_ven
	from   tes_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:al_anno_ord_ven
	and    num_registrazione=:al_num_ord_ven;
	
	if sqlca.sqlcode < 0 then 
		as_messaggio = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	select flag_tipo_bcl
	into   :ls_tipo_stampa
	from   tab_tipi_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;
	
	if sqlca.sqlcode < 0 then 
		as_messaggio = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	if ls_tipo_stampa="B" then
	else
		if uof_get_tipo_det_ven_add(al_anno_ord_ven, al_num_ord_ven, ll_riga_ord_ven, 	ls_cod_tipo_det_ven_addizionali, as_messaggio) < 0 then
			close det_ord_ven;
			return -1
		end if
		if ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_addizionali then continue
	end if
	
	//vecchio codice commentato .....
//	if uof_get_tipo_det_ven_add(al_anno_ord_ven, al_num_ord_ven, ll_riga_ord_ven, 	ls_cod_tipo_det_ven_addizionali, as_messaggio) < 0 then
//		close det_ord_ven;
//		return -1
//	end if
//	if ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_addizionali then continue
	
	//fine modifica
	//###########################################################################################
	
	
	ll_return = uof_stato_prod_det_ord_ven(al_anno_ord_ven,al_num_ord_ven,ll_riga_ord_ven,as_messaggio)
	
	choose case ll_return
		case -1
			as_messaggio = "Errore in controllo stato produzione riga " + string(ll_riga_ord_ven) + ":~n" + as_messaggio
			return -1
		case -100
			continue
		case 0
			ll_aperte ++
		case 1
			ll_chiuse ++
		case 2
			ll_parziali ++
		case else
			as_messaggio = "Errore in controllo stato produzione riga " + string(ll_riga_ord_ven) + ":~nValore non previsto"
			return -1
	end choose
	
	ll_totale ++
	
loop

if ll_totale = 0 then
	as_messaggio = "Non esiste alcuna riga in produzione per l'ordine indicato"
	return -100
end if
	
if ll_chiuse = 0 and ll_parziali = 0 then
	as_messaggio = "Niente"
	return 0
else
	if ll_chiuse = ll_totale then
		as_messaggio = "Tutto"
		return 1
	else
		as_messaggio = "Parziale"
		return 2
	end if
end if
end function

public function integer uof_trova_max_barcode (ref long fl_barcode, ref string fs_errore);// Funzione che trova il massimo attuale tra progr_det_produzione e progr_tes_produzione
// nome: uof_trova_max_barcode
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 10-12-2003
//		Autore Diego Ferrari

long ll_progr_det_produzione,ll_progr_tes_produzione

select max(progr_det_produzione)
into  :ll_progr_det_produzione
from  det_ordini_produzione
where cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore in fase di lettura max progressivo di det_stampa_ordini.Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

if isnull(ll_progr_det_produzione) or ll_progr_det_produzione=0 then
	ll_progr_det_produzione=0
end if

select max(progr_tes_produzione)
into  :ll_progr_tes_produzione
from  tes_ordini_produzione
where cod_azienda=:s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore in fase di lettura max progressivo di det_stampa_ordini.Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

if isnull(ll_progr_tes_produzione) or ll_progr_tes_produzione=0 then
	ll_progr_tes_produzione=0
end if

if ll_progr_tes_produzione >= ll_progr_det_produzione then
	fl_barcode = ll_progr_tes_produzione
else
	fl_barcode = ll_progr_det_produzione
end if

//se viene fuori un barcode più piccolo di quello minimo, assegna quello minimo (che poi viene incrementato di uno)
if fl_barcode<il_barcode_minimo then
	fl_barcode = il_barcode_minimo
end if
//-------------------------------------------------------------------------------------------

return 0
end function

public function integer uof_trova_tipo (long fl_barcode, ref string fs_tipo_stampa, ref string fs_errore);// Funzione che trova il tipo ordine A = tende da sole, B = Sfuso, C = tende tecniche
// nome: uof_trova_tipo
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 10-12-2003
//		Autore Diego Ferrari


integer li_risposta, li_anno_registrazione
long ll_num_registrazione
string ls_tipo_stampa,ls_cod_tipo_ord_ven

select anno_registrazione,
	    num_registrazione
into   :li_anno_registrazione,
		 :ll_num_registrazione
from   det_ordini_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr_det_produzione=:fl_barcode;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

if sqlca.sqlcode = 100 then
	select anno_registrazione,
	    	 num_registrazione
	into   :li_anno_registrazione,
			 :ll_num_registrazione
	from   tes_ordini_produzione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    progr_tes_produzione=:fl_barcode;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	if sqlca.sqlcode = 100 then
		fs_errore = "Attenzione! Non è stato trovato alcun ordine con questo codice a barre. Verificare di aver letto il codice a barre corretto presente nella stampa di produzione in basso a sinistra o di aver inserito correttamente i dati in modalità manuale."
		return -1	
	end if
	
end if


if uof_trova_tipo(li_anno_registrazione, ll_num_registrazione, fs_tipo_stampa, fs_errore) < 0 then
	return -1	
end if


//select cod_tipo_ord_ven
//into   :ls_cod_tipo_ord_ven
//from   tes_ord_ven
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    anno_registrazione=:li_anno_registrazione
//and    num_registrazione=:ll_num_registrazione;
//
//if sqlca.sqlcode < 0 then 
//	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
//	return -1
//end if
//
//select flag_tipo_bcl
//into   :ls_tipo_stampa
//from   tab_tipi_ord_ven
//where  cod_azienda=:s_cs_xx.cod_azienda
//and    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;
//
//if sqlca.sqlcode < 0 then 
//	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
//	return -1
//end if
//
//fs_tipo_stampa = ls_tipo_stampa


return 0
end function

public function integer uof_stato_prod_det_ord_ven (long al_anno_ord_ven, long al_num_ord_ven, long al_riga_ord_ven, ref string as_messaggio);long ll_progr_det_produzione, ll_return, ll_aperte, ll_parziali, ll_chiuse, ll_totale


ll_aperte = 0

ll_parziali = 0

ll_chiuse = 0

ll_totale = 0

declare fasi cursor for
select
	progr_det_produzione
from
	det_ordini_produzione
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :al_anno_ord_ven and
	num_registrazione = :al_num_ord_ven and
	prog_riga_ord_ven = :al_riga_ord_ven
order by
	progr_det_produzione ASC;

open fasi;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in open cursore fasi:~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch
		fasi
	into
		:ll_progr_det_produzione;
		
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in fetch cursore fasi:~n" + sqlca.sqlerrtext
		close fasi;
		return -1
	elseif sqlca.sqlcode = 100 then
		close fasi;
		exit
	end if
	
	ll_return = uof_stato_prod_fase(ll_progr_det_produzione,as_messaggio)
	
	choose case ll_return
		case -1
			as_messaggio = "Errore in controllo stato produzione fase " + string(ll_progr_det_produzione) + ":~n" + as_messaggio
			return -1
		case 0
			ll_aperte ++
		case 1
			ll_chiuse ++
		case 2
			ll_parziali ++
		case else
			as_messaggio = "Errore in controllo stato produzione fase " + string(ll_progr_det_produzione) + ":~nValore non previsto"
			return -1
	end choose
	
	ll_totale ++
	
loop

if ll_totale = 0 then
	as_messaggio = "Non esiste alcuna fase in produzione per la riga d'ordine indicata"
	return -100
end if

if ll_chiuse = 0 and ll_parziali = 0 then
	as_messaggio = "Niente"
	return 0
else
	if ll_chiuse = ll_totale then
		as_messaggio = "Tutto"
		return 1
	else
		as_messaggio = "Parziale"
		return 2
	end if
end if
end function

public function integer uof_stato_prod_sessione (long al_progr_det_produzione, long al_progr_sessione, ref string as_messaggio);string 	ls_fine_sessione

datetime ldt_inizio_sessione


select
	flag_fine_sessione,
	inizio_sessione
into
	:ls_fine_sessione,
	:ldt_inizio_sessione
from
	sessioni_lavoro
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_det_produzione = :al_progr_det_produzione and
	progr_sessione = :al_progr_sessione;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in controllo flag fine sessione: " + sqlca.sqlerrtext
	return -1
end if

if ls_fine_sessione = "S" then
	as_messaggio = "Tutto"
	return 1
elseif not isnull(ldt_inizio_sessione) then
	as_messaggio = "Parziale"
	return 2
else
	as_messaggio = "Niente"
	return 0
end if
end function

public function integer uof_stato_prod_fase (long al_progr_det_produzione, ref string as_messaggio);string ls_fine_fase

long   ll_progr_sessione, ll_return, ll_aperte, ll_parziali, ll_chiuse, ll_totale, ll_progr_tes_produzione


select
	progr_tes_produzione,
	flag_fine_fase
into
	:ll_progr_tes_produzione,
	:ls_fine_fase
from
	det_ordini_produzione
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_det_produzione = :al_progr_det_produzione;
	
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in controllo flag fine fase: " + sqlca.sqlerrtext
	return -1
end if

if ls_fine_fase = "S" then
	as_messaggio = "Tutto"
	return 1
end if

ll_aperte = 0

ll_parziali = 0

ll_chiuse = 0

ll_totale = 0

declare sessioni cursor for
select
	progr_sessione
from
	sessioni_lavoro
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_det_produzione = :al_progr_det_produzione
order by
	progr_sessione ASC;

open sessioni;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in open cursore sessioni:~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch
		sessioni
	into
		:ll_progr_sessione;
		
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in fetch cursore sessioni:~n" + sqlca.sqlerrtext
		close sessioni;
		return -1
	elseif sqlca.sqlcode = 100 then
		close sessioni;
		exit
	end if
	
	ll_return = uof_stato_prod_sessione(al_progr_det_produzione,ll_progr_sessione,as_messaggio)
	
	choose case ll_return
		case -1
			as_messaggio = "Errore in controllo stato produzione sessione " + string(ll_progr_sessione) + ":~n" + as_messaggio
			return -1
		case 0
			ll_aperte ++
		case 1
			ll_chiuse ++
		case 2
			ll_parziali ++
		case else
			as_messaggio = "Errore in controllo stato produzione sessione " + string(ll_progr_sessione) + ":~nValore non previsto"
			return -1
	end choose
	
	ll_totale ++
	
loop

if ll_totale = 0 then
	if isnull(ll_progr_tes_produzione) then
		as_messaggio = "Niente"
		return 0
	else
		
		ll_return = uof_stato_prod_fase_comp(ll_progr_tes_produzione,as_messaggio)
		
		choose case ll_return
			case -1
				as_messaggio = "Errore in controllo stato produzione fase complessiva " + string(ll_progr_tes_produzione) + ":~n" + as_messaggio
				return -1
			case 0
				as_messaggio = "Niente"
				return 0
			case 1
				as_messaggio = "Tutto"
				return 1
			case 2
				as_messaggio = "Parziale"
				return 2
			case else
				as_messaggio = "Errore in controllo stato produzione fase complessiva " + string(ll_progr_tes_produzione) + ":~nValore non previsto"
				return -1
		end choose
		
	end if
end if

if ll_chiuse = 0 and ll_parziali = 0 then
	as_messaggio = "Niente"
	return 0
else
	as_messaggio = "Parziale"
	return 2
end if
end function

public function integer uof_reparti (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_versione, ref string fs_cod_reparti_trovati[], ref string fs_errore);// Funzione che verifica la corretta assegnazione dei reparti e carica i reparti stessi
// nome: uof_reparti
// tipo: intero
// 		 0 passed
//			-1 failed e si arresta
//       -2 failed ma può continuare
//  
//		Creata il 21-11-2003
//		Autore Diego Ferrari

string ls_test,ls_errore,ls_cod_reparti_trovati[],ls_cod_reparto,ls_flag_stampa_fase
integer li_risposta
uo_funzioni_1 luo_funzioni

f_null_array(ls_cod_reparti_trovati[])           //azzero contenuto array dei reparti per la funzione trova reparti
			
declare test_padre cursor for
select cod_azienda
from   distinta_padri
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:fs_cod_prodotto;			 //test se prodotto è un prodotto con distinta base
														 //devo aprire un cursore poichè possono esserci più versioni
open test_padre;

fetch test_padre into :ls_test;

if sqlca.sqlcode < 0 then 
	fs_errore = "Lancio di produzione, caso tende da sole, lettura (per test) distinta_padri.Errore sul DB: " + sqlca.sqlerrtext
	close test_padre;
	return -1
end if

if sqlca.sqlcode = 100 then	//se il prodotto non ha distinta base è un errore di compilazione 
									//dell'ordine poichè nei report_1 i prodotti che non sono optional 
									//o addizionali devono avere distinta base
	f_scrivi_log("Lancio di produzione GIBUS-->>tende da sole. Nell'ordine anno:" + string(fi_anno_registrazione) + & 
					 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + fs_cod_prodotto + & 
					 " che non essendo ne optional ne addizionale DEVE avere la distinta base poichè la stampa di questo ordine per la produzione è di tipo Report 1. Verificare.")
	
	close test_padre;
	return -2 //continue //esce dall'ordine corrente e scansiona il prodotto successivo

else
// se il prodotto ha DB allora gli 'N' reparti vengono trovati tramite una funzione
// che carica is_cod_reparto[] con i reparti dalla distinta base 
// per le fasi di lavoro con flag_stampa fase ='S'

	//Donato 01/02/2012 Spostata funzione globale in user object oggetto
	luo_funzioni = create uo_funzioni_1
	
	li_risposta = luo_funzioni.uof_trova_reparti(true,  fi_anno_registrazione,fl_num_registrazione, fl_prog_riga_ord_ven, &
											fs_cod_prodotto,fs_cod_versione, ls_cod_reparti_trovati[],ls_errore)
	
//	li_risposta = luo_funzioni.uof_trova_reparti(		true, fi_anno_registrazione,fl_num_registrazione, fl_prog_riga_ord_ven, &
//																fs_cod_prodotto,fs_cod_versione, ls_cod_reparti_trovati[],ls_errore)
	destroy luo_funzioni
	
	//li_risposta=f_trova_reparti(fi_anno_registrazione,fl_num_registrazione, & 
	//									 fl_prog_riga_ord_ven,fs_cod_prodotto,fs_cod_versione, &
	//									 ls_cod_reparti_trovati[],ls_errore)
	
	//fine modifica -----------------------------------------------------------------------------------
	
	if li_risposta = -1 then
		f_scrivi_log("Lancio di produzione GIBUS-->>tende da sole. Nell'ordine anno:" + string(fi_anno_registrazione) + & 
		 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + fs_cod_prodotto + & 
		 " che durante la scansione della distinta base/fasi lavoro per trovare i reparti ha provocato questo errore:" + & 
		 ls_errore + " Verificare.")
	
		close test_padre;
	return -2 //	continue //esce dalla riga ordine corrente e scansiona il prodotto successivo

	end if

	if upperbound(ls_cod_reparti_trovati[]) = 0 then		//nel caso non venga trovato alcun reparto scansionando la distinta
		SELECT cod_reparto,											//viene assegnato il reparto della fase di lavoro del  PF quando era 		
				 flag_stampa_fase										//un SL in un'altra distinta
		INTO   :ls_cod_reparto,
				 :ls_flag_stampa_fase
		FROM   tes_fasi_lavorazione
		WHERE  cod_azienda = :s_cs_xx.cod_azienda  
		AND    cod_prodotto = :fs_cod_prodotto;
	
		if sqlca.sqlcode=100 then
			f_scrivi_log("Lancio di produzione GIBUS-->>tende da sole. Nell'ordine anno:" + string(fi_anno_registrazione) + & 
			 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + fs_cod_prodotto + & 
			 " che non ha una fase di lavoro con un reparto assegnato. Verificare.")
			close test_padre;
			return -2 //		continue
		end if
	  
		if ls_flag_stampa_fase = 'N' then
			f_scrivi_log("Lancio di produzione GIBUS-->>tende da sole. Nell'ordine anno:" + string(fi_anno_registrazione) + & 
			 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + fs_cod_prodotto + & 
			 " che ha una fase di lavoro con un reparto assegnato ma il flag_stampa_fase non è impostato. Verificare.")
			
			close test_padre;
			return -2 //		continue
			
		end if																						
	end if

end if

close test_padre;

fs_cod_reparti_trovati[] = ls_cod_reparti_trovati[]

return 0
end function

public function integer uof_cancella_ar (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_errore);// Funzione che elimina dalla produzione tutte le righe di det_ordini_in_produzione e le sue sessioni 
// che appartengono a reparti diversi da quelli del nuovo prodotto appena scelto come A/R.

// nome: uof_cancella_ar
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 21-01-2004
//		Autore Diego Ferrari


integer li_risposta
long ll_t,ll_i,ll_progr_det_produzione
string ls_cod_reparto,ls_cod_prodotto,ls_cod_versione,ls_errore,ls_cod_reparti[],ls_cod_reparti_canc[]
boolean lb_test

select cod_prodotto,
		 cod_versione
into   :ls_cod_prodotto,
		 :ls_cod_versione
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione
and    prog_riga_ord_ven=:fl_prog_riga_ord_ven;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

li_risposta=uof_reparti(fi_anno_registrazione, & 
								fl_num_registrazione, & 
								fl_prog_riga_ord_ven,&
								ls_cod_prodotto,&
								ls_cod_versione, &
								ls_cod_reparti[],&
								ls_errore)

choose case li_risposta
	case -1
		fs_errore=ls_errore
		return -1
		
	case -2
		fs_errore="Errore durante la procedura di eliminazione dati dei reparti nel ricalcolo A/R, consultare il log."
		return -1
	
	case 0	// se ok procede con l'eliminazione dei record in det_ordini_produzione con reparto diverso da quello appena caricato
		declare r_det_ord_prod cursor for
		select  cod_reparto
		from    det_ordini_produzione
		where   cod_azienda=:s_cs_xx.cod_azienda
		and     anno_registrazione=:fi_anno_registrazione
		and     num_registrazione=:fl_num_registrazione
		and     prog_riga_ord_ven=:fl_prog_riga_ord_ven;
		
		open r_det_ord_prod;
		
		do while 1 = 1
			fetch r_det_ord_prod
			into  :ls_cod_reparto;
			
			if sqlca.sqlcode = 100 then exit
					
			if sqlca.sqlcode < 0 then 
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				close r_det_ord_prod;
				return -1
			end if
			
			lb_test = false
			for ll_t = 1 to upperbound(ls_cod_reparti[])
				if ls_cod_reparti[ll_t] = ls_cod_reparto then lb_test= true		
			next
			
			if lb_test = false then //se il reparto non appartiene a quelli del prodotto della a/r allora lo mette nell'elenco 
											//di quelli da eliminare
				ll_i++
				ls_cod_reparti_canc[ll_i] = ls_cod_reparto
			end if
			
		loop
		
		close r_det_ord_prod;

		for ll_i = 1 to upperbound(ls_cod_reparti_canc[])  // esegue le cancellazioni
			select progr_det_produzione
			into   :ll_progr_det_produzione
			from   det_ordini_produzione
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_registrazione=:fi_anno_registrazione
			and    num_registrazione=:fl_num_registrazione
			and    prog_riga_ord_ven=:fl_prog_riga_ord_ven
			and    cod_reparto = :ls_cod_reparti_canc[ll_i];
				
			if sqlca.sqlcode < 0 then 
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if
			
			delete sessioni_lavoro
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    progr_det_produzione=:ll_progr_det_produzione;
			
			if sqlca.sqlcode < 0 then 
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if
			
			//Donato 11/03/2011
			//pulisci eventuali dati dei colli riferiti al barcode dettaglio --------------------------------------------------
			delete from tab_ord_ven_colli
			where  cod_azienda = :s_cs_xx.cod_azienda and
				barcode = :ll_progr_det_produzione;
			
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore cancellazione tab_ord_ven_colli.~r~n" + sqlca.sqlerrtext
				return -1
			end if
			//----------------------------------------------------------------------------------------------------------------------------------------
			
			//donato 21/01/2013: elimino eventuali righe di sospensione produzione --------------------------------
			delete from tab_sosp_produzione
			where cod_azienda=:s_cs_xx.cod_azienda and
					 progr_det_produzione=:ll_progr_det_produzione;
			
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore cancellazione tab_sosp_produzione.~r~n" + sqlca.sqlerrtext
				return -1
			end if
			//-----------------------------------------------------------------------------------------------------------------
			
			delete det_ordini_produzione
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    progr_det_produzione=:ll_progr_det_produzione;
			
			if sqlca.sqlcode < 0 then 
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if
			
		next
end choose			

return 0


end function

public function integer uof_elimina_prod_det (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_messaggio);delete from
	sessioni_lavoro
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_det_produzione in (select
										progr_det_produzione 
									 from
									 	det_ordini_produzione 
									 where
									 	cod_azienda = :s_cs_xx.cod_azienda and
										anno_registrazione = :al_anno_registrazione and
										num_registrazione = :al_num_registrazione and
										prog_riga_ord_ven = :al_prog_riga_ord_ven);

if sqlca.sqlcode <> 0 then 
	as_messaggio = "Errore in cancellazione sessioni: " + sqlca.sqlerrtext
	return -1
end if


//donato 21/01/2013: elimino eventuali righe di sospensione produzione --------------------------------
delete from tab_sosp_produzione
where cod_azienda=:s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno_registrazione and
		num_registrazione  = :al_num_registrazione and
		(prog_riga_ord_ven = :al_prog_riga_ord_ven or prog_riga_ord_ven is null);
		
if sqlca.sqlcode < 0 then
	as_messaggio = "Errore cancellazione tab_sosp_produzione della riga ordine.~r~n" + sqlca.sqlerrtext
	return -1
end if
//-----------------------------------------------------------------------------------------------------------------


delete from 	det_ordini_produzione
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :al_anno_registrazione and
			num_registrazione = :al_num_registrazione and
			prog_riga_ord_ven = :al_prog_riga_ord_ven;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in cancellazione fasi: " + sqlca.sqlerrtext
	return -1
end if


//pulisci eventuali dati in colli --------------------------------------
delete from tab_ord_ven_colli
where		cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :al_anno_registrazione and
			num_registrazione = :al_num_registrazione and
			prog_riga_ord_ven = :al_prog_riga_ord_ven;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore cancellazione tab_ord_ven_colli.~r~n" + sqlca.sqlerrtext
	return -1
end if
//---------------------------------------------------------------------


return 0
end function

public function integer uof_elimina_prod_comm (long al_anno_commessa, long al_num_commessa, ref string as_messaggio);long ll_anno_ord, ll_num_ord, ll_riga_ord


declare righe_ord_ven cursor for
select distinct
	anno_registrazione,
	num_registrazione,
	prog_riga_ord_ven
from
	det_ord_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_commessa = :al_anno_commessa and
	num_commessa = :al_num_commessa;
	
open righe_ord_ven;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in open cursore righe_ord_ven: " + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch
		righe_ord_ven
	into
		:ll_anno_ord,
		:ll_num_ord,
		:ll_riga_ord;
		
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in fetch cursore righe_ord_ven: " + sqlca.sqlerrtext
		close righe_ord_ven;
		return -1
	elseif sqlca.sqlcode = 100 then
		close righe_ord_ven;
		exit
	end if
		
	if uof_elimina_prod_det(ll_anno_ord,ll_num_ord,ll_riga_ord,as_messaggio) <> 0 then
		as_messaggio = "Errore in cancellazione righe ordine collegate.~n" + as_messaggio
		close righe_ord_ven;
		return -1
	end if
	
loop

return 0
end function

public function integer uof_stato_prod_fase_comp (long al_progr_tes_produzione, ref string as_messaggio);string 	ls_pronto_complessivo

datetime ldt_inizio_preparazione

long   	ll_aperte, ll_parziali, ll_chiuse, ll_totale


select
	flag_pronto_complessivo,
	inizio_preparazione
into
	:ls_pronto_complessivo,
	:ldt_inizio_preparazione
from
	tes_ordini_produzione
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_tes_produzione = :al_progr_tes_produzione;
	
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in controllo flag pronto complessivo: " + sqlca.sqlerrtext
	return -1
end if

if ls_pronto_complessivo = "S" then
	as_messaggio = "Tutto"
	return 1
elseif not isnull(ldt_inizio_preparazione) then
	as_messaggio = "Parziale"
	return 2
else
	as_messaggio = "Niente"
	return 0
end if
end function

public function integer uof_inserisci_tes (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_reparti_trovati[], ref string fs_errore);// Funzione che esegue l'inserimento in tes_ordini_produzione
// nome: uof_inserisci_tes
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 24-11-2003
//		Autore Diego Ferrari

long ll_progr_tes_produzione,ll_num_reparti
integer li_risposta
string ls_errore

li_risposta = uof_trova_max_barcode(ll_progr_tes_produzione, ls_errore)
												
												
if li_risposta < 0 then 
	fs_errore = ls_errore
	return -1
end if


if isnull(ll_progr_tes_produzione) or ll_progr_tes_produzione=0 then
	ll_progr_tes_produzione=1
else
	ll_progr_tes_produzione++
end if

for ll_num_reparti = 1 to upperbound(fs_cod_reparti_trovati[])
	
	insert into tes_ordini_produzione		
	(cod_azienda,
	 progr_tes_produzione,
	 anno_registrazione,
	 num_registrazione,
	 cod_reparto,
	 cod_operaio_inizio,
	 cod_operaio_fine,
	 inizio_preparazione,
	 fine_preparazione,
	 flag_pronto_complessivo,
	 num_colli)
	values
	(:s_cs_xx.cod_azienda,
	 :ll_progr_tes_produzione,
	 :fi_anno_registrazione,
	 :fl_num_registrazione,
	 :fs_cod_reparti_trovati[ll_num_reparti],
	 null,
	 null,
	 null,
	 null,
	 'N',
	 0);
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "errore in fase di inserimento in tes_ordini_produzione. Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	// impone il progr_tes_produzione anche sui det_ordini_produzione appena creati
	
	update det_ordini_produzione
	set    progr_tes_produzione =:ll_progr_tes_produzione
	where cod_azienda=:s_cs_xx.cod_azienda
	and   anno_registrazione=:fi_anno_registrazione
	and   num_registrazione=:fl_num_registrazione
	and   cod_reparto=:fs_cod_reparti_trovati[ll_num_reparti];
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "errore in fase di update in det_ordini_produzione. Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	ll_progr_tes_produzione++
next


return 0
end function

public function integer uof_aggiorna_colli_tes (long al_progr_tes_produzione, ref string as_messaggio);long ll_anno_ord_ven, ll_num_ord_ven, ll_num_colli


select
	anno_registrazione,
	num_registrazione,
	num_colli
into
	:ll_anno_ord_ven,
	:ll_num_ord_ven,
	:ll_num_colli
from
	tes_ordini_produzione
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_tes_produzione = :al_progr_tes_produzione;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura totale colli: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_messaggio = "Errore in lettura totale colli: progressivo ordine produzione non trovato"
	return -1
end if

if isnull(ll_anno_ord_ven) or isnull(ll_num_ord_ven) or ll_anno_ord_ven = 0 or ll_num_ord_ven = 0 then
	as_messaggio = "Errore in lettura ordine di vendita collegato: dati numerazione nulli o incompleti"
	return -1
end if

if isnull(ll_num_colli) then
	ll_num_colli = 0
end if


//Donato 07/04/2010 ----------------------------------------------
//Se un ordine tipo B,C (esempio uno sfuso) ha prodotti su + reparti (come può accadere da Viropa secondo Fabio) potrei avere in tes_ordini_produzioni altri barcode (gli altri reparti)
//che hanno già prodotto qualcosa. Quindi per il corretto numero colli totale faccio questo sum

select sum(num_colli)
into :ll_num_colli
from tes_ordini_produzione
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_ord_ven and
			num_registrazione = :ll_num_ord_ven;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura totale colli ordine da tes_ordini_produzione: " + sqlca.sqlerrtext
	return -1
end if

if isnull(ll_num_colli) then
	ll_num_colli = 0
end if
//fine modifica -----------------------------------------------------------



update
	tes_ord_ven
set
	num_colli = :ll_num_colli
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno_ord_ven and
	num_registrazione = :ll_num_ord_ven;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento colli ordine di vendita: " + sqlca.sqlerrtext
	return -1
end if

as_messaggio = "Ordine di vendita " + string(ll_anno_ord_ven) + "/" + string(ll_num_ord_ven) + &
					" aggiornato a " + string(ll_num_colli) + " collo/i totali"

return 0
end function

public function integer uof_barcode_anno_reg (ref long fl_barcode, ref integer fi_anno_registrazione, ref long fl_num_registrazione, ref long fl_prog_riga_ord_ven, ref string fs_cod_reparto, ref string fs_errore);// Funzione che esegue dato il codice a barre restituisce anno, numero, progressivo e reparto di det_ordini_produzione
//          e viceversa
// nome: uof_barcode_anno_reg
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 09-12-2003
//		Autore Diego Ferrari


integer li_anno_registrazione
long ll_num_registrazione,ll_prog_riga_ord_ven,ll_barcode,ll_barcode_2
string ls_cod_reparto

if not isnull(fl_barcode) then

	select anno_registrazione,
			 num_registrazione,
			 prog_riga_ord_ven,
			 cod_reparto
	into   :li_anno_registrazione,
			 :ll_num_registrazione,
			 :ll_prog_riga_ord_ven,
			 :ls_cod_reparto
	from   det_ordini_produzione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    progr_det_produzione =:fl_barcode;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	else
		
		if sqlca.sqlcode = 100 then
			select anno_registrazione,
					 num_registrazione,
					 cod_reparto
			into   :li_anno_registrazione,
					 :ll_num_registrazione,
					 :ls_cod_reparto
			from   tes_ordini_produzione
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    progr_tes_produzione =:fl_barcode;	
		
			if sqlca.sqlcode < 0 then 
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if		
		
			if sqlca.sqlcode = 100 then
				fs_errore = "Non è stato trovato alcun lancio di produzione con questo codice. Verificare."
				return -1
			end if	
			fi_anno_registrazione = li_anno_registrazione
			fl_num_registrazione = ll_num_registrazione
			fs_cod_reparto = ls_cod_reparto
			
		else
			fi_anno_registrazione = li_anno_registrazione
			fl_num_registrazione = ll_num_registrazione
			fl_prog_riga_ord_ven = ll_prog_riga_ord_ven
			fs_cod_reparto = ls_cod_reparto
		end if
		
		return 0
	end if
else
	select progr_det_produzione,
			 progr_tes_produzione
	into   :ll_barcode,
	       :ll_barcode_2
	from   det_ordini_produzione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:fi_anno_registrazione
	and	 num_registrazione=:fl_num_registrazione
	and	 prog_riga_ord_ven=:fl_prog_riga_ord_ven
	and 	 cod_reparto=:fs_cod_reparto;
			 

	choose case sqlca.sqlcode
			
		case is < 0
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
			
		case 0
			if not isnull(ll_barcode_2) then
				fl_barcode = ll_barcode_2
				return 0
			end if
			
			fl_barcode = ll_barcode
			return 0
	
		case 100
			select progr_tes_produzione
			into   :ll_barcode	
			from   tes_ordini_produzione
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_registrazione=:fi_anno_registrazione
			and	 num_registrazione=:fl_num_registrazione
			and 	 cod_reparto=:fs_cod_reparto;
			
			if sqlca.sqlcode < 0 then 
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if
			
			if sqlca.sqlcode = 100 then
				fs_errore = "Attenzione! Non è stato trovato alcun ordine con questo codice a barre. Verificare di aver letto il codice a barre corretto presente nella stampa di produzione in basso a sinistra o di aver inserito correttamente i dati in modalità manuale."
				return -1
			end if

			if sqlca.sqlcode = 0 then
				fl_barcode = ll_barcode
				return 0
			end if
		
	end choose
	
	
end if


end function

public function integer uof_etichette (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_errore);// Funzione che esegue la stampa delle etichette di produzione per PROGETTOTENDA SPA
// nome: uof_etichette
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 11-02-2004
//		Autore Diego Ferrari

boolean			lb_alusistem

string				ls_errore,ls_tipo_stampa,ls_cod_reparto,ls_cod_cliente,ls_rag_soc_1_div,ls_rag_soc_2_div,ls_indirizzo_div,ls_localita_div, &
					ls_frazione_div,ls_cap_div,ls_cod_giro_consegna,ls_alias_cliente,ls_provincia_div,ls_rag_soc_1,ls_rag_soc_2, ls_sql, & 
					ls_indirizzo,ls_localita,ls_frazione,ls_cap,ls_provincia,ls_des_giro_consegna,ls_cod_prodotto,ls_des_prodotto, &
					ls_num_ord_cliente,ls_rif_interscambio,ls_nota_prodotto,ls_cgibus, ls_CSB, ls_lista_reparti_coinvolti, ls_stabilim_prossimo, &
					ls_cod_reparto_prossimo, ls_rag_soc_abbreviata, ls_cod_deposito_origine, ls_cod_nazione, ls_temp, ls_colore_tessuto, ls_cod_verniciatura, &
					ls_des_verniciatura, ls_cod_comodo

					
long				ll_num_colli,ll_barcode,ll_num_colli_testata,ll_num_tende_totali,ll_riga,ll_num_colli_riga, ll_prog_riga_ord_ven_tecnica

integer			li_risposta

decimal			ld_quan_ordine,ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t

datetime			ldt_data_consegna

datastore		lds_det_ordini_prod

boolean			lb_alert = false		//se TRUE allora il reparto successivo appartiene ad uno stabilimento diverso
											//oppure sei all'ultimo reparto e lo stabilimento origine dell'ordine è esterno --->>> visulaizza immaginetta alert su etichetta imballo

boolean			lb_RIE = false

guo_functions.uof_get_parametro("RIE", lb_RIE)
if isnull(lb_RIE) then lb_RIE = false

//lettura parametro aziendale Carattere Speciale barcode (specifica Carico ordini con palmare)
select stringa
into   :ls_CSB
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='CSB' and 
		flag_parametro='S';

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in ricerca del parametro CSB in tabella parametri azienda: " + sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 or isnull(ls_CSB) or ls_CSB = "" then	
	ls_CSB = ""
end if
//------------------------------------------------------------------------------------------------------------------------------------


// parametro che identifica l'applicazione per centro gibus
select flag
into   :ls_cgibus
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='CGH';

if sqlca.sqlcode < 0 then
	ls_cgibus = "N"
end if

// per centro gibus c'è un dataobject particolare.
if ls_cgibus = "S"  then
	// funzione disabilitata per chi è Centro Gibus (parametro aziendale CGH='S')
	return 0
end if



if not isnull(fl_barcode) then 
	li_risposta = uof_barcode_anno_reg(fl_barcode,&
												  fi_anno_registrazione, &
												  fl_num_registrazione,&
												  fl_prog_riga_ord_ven,&
												  ls_cod_reparto,&
												  ls_errore)
	
	if li_risposta < 0 then
		fs_errore = ls_errore
		return -1
	end if
end if

s_cs_xx.parametri.parametro_ds_1 = create datastore


select cod_cliente,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
	 	 cap,   
		 provincia,   
		 cod_giro_consegna,   
		 alias_cliente,   
		 num_colli,
		 num_ord_cliente,
		 rif_interscambio,
		 data_consegna,
		 cod_deposito
 into :ls_cod_cliente,   
		:ls_rag_soc_1_div,   
		:ls_rag_soc_2_div,   
		:ls_indirizzo_div,   
		:ls_localita_div,   
		:ls_frazione_div,   
		:ls_cap_div,   
		:ls_provincia_div,   
		:ls_cod_giro_consegna,   
		:ls_alias_cliente,   
		:ll_num_colli_testata,
		:ls_num_ord_cliente,
		:ls_rif_interscambio,
		:ldt_data_consegna,
		:ls_cod_deposito_origine
from  tes_ord_ven 
where cod_azienda=:s_cs_xx.cod_azienda
and   anno_registrazione=:fi_anno_registrazione
and   num_registrazione=:fl_num_registrazione;
 
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_rag_soc_1_div) then ls_rag_soc_1_div = ""
if isnull(ls_rag_soc_2_div) then ls_rag_soc_2_div = ""
if isnull(ls_indirizzo_div) then ls_indirizzo_div = ""
if isnull(ls_localita_div) then ls_localita_div = ""
if isnull(ls_frazione_div) then ls_frazione_div = ""
if isnull(ls_cap_div) then ls_cap_div = ""
if isnull(ls_provincia_div) then ls_provincia_div = ""
if isnull(ls_cod_giro_consegna) then ls_cod_giro_consegna = ""
if isnull(ls_alias_cliente) then ls_alias_cliente = ""
if isnull(ll_num_colli_testata) then ll_num_colli_testata =0
if isnull(ls_num_ord_cliente) then ls_num_ord_cliente = ""
if isnull(ls_rif_interscambio) then ls_rif_interscambio = ""

//if isnull(ls_rag_soc_1_div) or ls_rag_soc_1 = "" then
	
	select rag_soc_1,
	       rag_soc_2,
			 indirizzo,   
			 localita,   
			 frazione,   
			 cap,   
			 provincia,
			 rag_soc_abbreviata,
			 cod_nazione
	into     :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,
			 :ls_rag_soc_abbreviata,
			 :ls_cod_nazione
	from   anag_clienti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cliente=:ls_cod_cliente; 

	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if

//end if


if isnull(ls_rag_soc_1) then ls_rag_soc_1 =""
if isnull(ls_rag_soc_2) then ls_rag_soc_2 =""
if isnull(ls_indirizzo) then ls_indirizzo =""
if isnull(ls_localita) then ls_localita =""
if isnull(ls_frazione) then ls_frazione =""
if isnull(ls_cap) then ls_cap =""
if isnull(ls_provincia) then ls_provincia =""

if isnull(ls_cod_nazione) then ls_cod_nazione =""
if ls_cod_nazione<>"" then ls_cod_nazione = "(" + ls_cod_nazione + ")"


//17/01/2012 Beatrice ha chiesto che se c'è la rag. soc. abbreviata visualizza quella
if not isnull(ls_rag_soc_abbreviata) and ls_rag_soc_abbreviata<>"" then ls_rag_soc_1=ls_rag_soc_abbreviata


select des_giro_consegna
into   :ls_des_giro_consegna
from   tes_giri_consegne
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_giro_consegna=:ls_cod_giro_consegna;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if


//controlla il tipo ordine (tipo stampa) se A= tende da sole, B=sfuso o C=tende tecniche
li_risposta = uof_trova_tipo(fl_barcode,&
									  ls_tipo_stampa,&
									  ls_errore)

if li_risposta < 0 then
	fs_errore = ls_errore
	return -1
end if


choose case ls_tipo_stampa
	case 'A'  // tende da sole
		lb_alusistem = false
		guo_functions.uof_get_parametro_azienda( "ALU", lb_alusistem)
		if lb_alusistem then
			s_cs_xx.parametri.parametro_ds_1.dataobject = "d_label_tende_sole_alusistemi"
		else
			s_cs_xx.parametri.parametro_ds_1.dataobject = "d_label_tende_sole"
		end if
			
		s_cs_xx.parametri.parametro_s_1 = "A"
		
	case 'B'  // sfuso
		s_cs_xx.parametri.parametro_ds_1.dataobject = "d_label_tende_tecniche_sfuso"
		s_cs_xx.parametri.parametro_s_1 = "B"
		
	case 'C' // tende tecniche
		s_cs_xx.parametri.parametro_ds_1.dataobject = "d_label_tende_tecniche_sfuso"
		s_cs_xx.parametri.parametro_s_1 = "C"
		
end choose


// il seguente inserimento va bene per tutti e tre i tipi di etichetta.
ll_riga = s_cs_xx.parametri.parametro_ds_1.insertrow(0)
s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"num_registrazione",string(fl_num_registrazione))
s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"anno_registrazione",string(fi_anno_registrazione))
s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"giro_consegna",ls_des_giro_consegna)

if lb_RIE and g_str.isnotempty(ls_rif_interscambio) then
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rag_soc",ls_rif_interscambio)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"alias",ls_rif_interscambio)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rag_soc_diversa",ls_rif_interscambio)
else
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rag_soc",ls_rag_soc_1)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"alias",ls_alias_cliente)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rag_soc_diversa",ls_rag_soc_1_div)
end if


s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_1",ls_indirizzo)
s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_2",ls_cap+" "+ls_frazione+" "+ls_localita+" "+ ls_provincia + " " + ls_cod_nazione)

s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_1_diversa",ls_indirizzo_div)
s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_2_diversa",ls_cap_div+" "+ls_frazione_div+" "+ls_localita_div+" "+ ls_provincia_div)
s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rif_1",ls_rif_interscambio)
s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rif_2",ls_num_ord_cliente)

try
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"barcode","*"+ls_CSB + string(fl_barcode) + ls_CSB+"*")
catch (runtimeerror err)	
end try

choose case ls_tipo_stampa
	case 'A'  // tende da sole
		
		select cod_prodotto,   
				 quan_ordine,
				 nota_prodotto
		into   :ls_cod_prodotto,   
				 :ld_quan_ordine,
				 :ls_nota_prodotto
		from   det_ord_ven
		where  cod_azienda =: s_cs_xx.cod_azienda and
				 anno_registrazione = :fi_anno_registrazione and
				 num_registrazione  = :fl_num_registrazione and
				 prog_riga_ord_ven  = :fl_prog_riga_ord_ven;

		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		select dim_x,   
             dim_y,   
             dim_z,   
             dim_t,
			cod_non_a_magazzino,
			cod_verniciatura
      into   :ld_dim_x,   
             :ld_dim_y,   
             :ld_dim_z,   
             :ld_dim_t,
			:ls_colore_tessuto,
			:ls_cod_verniciatura
		 from  comp_det_ord_ven 
		 where cod_azienda=:s_cs_xx.cod_azienda
		 and   anno_registrazione=:fi_anno_registrazione
		 and   num_registrazione=:fl_num_registrazione
		 and   prog_riga_ord_ven=:fl_prog_riga_ord_ven;

		 if sqlca.sqlcode < 0 then 
		   fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		 end if
		 
		 ls_des_verniciatura = f_des_tabella("anag_prodotti","cod_prodotto='" + ls_cod_verniciatura + "'", "des_prodotto")
		 
		select des_prodotto,
				cod_comodo
		into   :ls_des_prodotto,
				:ls_cod_comodo
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then 
		   fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
	   end if
		
		select count(*)
		into   :ll_num_tende_totali
		from   det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:fi_anno_registrazione
		and    num_registrazione=:fl_num_registrazione
		and    num_riga_appartenenza=0;
		
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"data_consegna",uof_trasforma_data(date(ldt_data_consegna)))
		
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"cod_prodotto_mod",ls_cod_prodotto)
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"des_prodotto_mod",ls_des_prodotto)
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"nota",ls_nota_prodotto)
		
		if ld_dim_x <> 0 then
			s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_x",string(round(ld_dim_x,2)))
		end if
		
		if ld_dim_y <> 0 then
			s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_y",string(round(ld_dim_y,2)))
		end if
		
		if ld_dim_z <> 0 then
			s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_z",string(round(ld_dim_z,2)))
		end if
		
		if ld_dim_t <> 0 then
	 	   s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_t",string(round(ld_dim_t,2)))
		end if
		
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"quan_ordine",string(ld_quan_ordine))
		
		if lb_alusistem then
			s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"des_prodotto_mod",ls_cod_comodo)
			s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"tessuto",ls_colore_tessuto)
			s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"verniciatura",ls_des_verniciatura)
		end if
		
		
		select sum(num_colli)
		into   :ll_num_colli_riga
		from   sessioni_lavoro
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    progr_det_produzione=:fl_barcode;
		
		
		if sqlca.sqlcode < 0 then 
		   fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
	   	end if
		
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"num_colli",string(ll_num_colli_riga))
		
		//visualizza lo stabilimento successivo / stabilimento origine ordine (se ultimo reparto)
		s_cs_xx.parametri.parametro_s_14 = ""
		uof_get_ordine_reparti(	fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ls_cod_reparto, &
										ls_lista_reparti_coinvolti, ls_stabilim_prossimo, ls_cod_reparto_prossimo, lb_alert)
		s_cs_xx.parametri.parametro_s_14 = ls_cod_reparto_prossimo
		
		if lb_alert then
			s_cs_xx.parametri.parametro_dec4_5 = 1
		else
			s_cs_xx.parametri.parametro_dec4_5 = 0
		end if
		//------------------------------

		s_cs_xx.parametri.parametro_ul_1 = fl_barcode
		
		
	case 'B','C'  // sfuso e tende tecniche
		select num_colli
		into   :ll_num_colli_testata
		from   tes_ordini_produzione
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    progr_tes_produzione=:fl_barcode;
			
		if sqlca.sqlcode < 0 then 
		   fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
	   end if
		
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"num_colli",string(ll_num_colli_testata))
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"data_consegna",uof_trasforma_data(date(ldt_data_consegna)))
		
		if ls_tipo_stampa="C" then
			//nel caso di tende tecniche il barcode è quello della tes_ordini_produzione
			//pertanto con questo barcode faccio una select nella det_ordini_produzione e mi prendo un prog_riga_ord_ven qualsiasi
			//che miserve per chiamare la funzione che trova il reparto/stabilimento successivi
			ls_sql = 	"select prog_riga_ord_ven "+&
						"from det_ordini_produzione "+&
						"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
									"progr_tes_produzione=" + string(fl_barcode)+ " "
			
			s_cs_xx.parametri.parametro_s_14 = ""
			if guo_functions.uof_crea_datastore(lds_det_ordini_prod, ls_sql)>0 then
				//prendo la prima
				ll_prog_riga_ord_ven_tecnica = lds_det_ordini_prod.getitemnumber(1, "prog_riga_ord_ven")
				
				uof_get_ordine_reparti(fi_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven_tecnica, &
												ls_cod_reparto, ls_lista_reparti_coinvolti, ls_stabilim_prossimo, ls_cod_reparto_prossimo, lb_alert)
				
				s_cs_xx.parametri.parametro_s_14 = ls_cod_reparto_prossimo
				
				if lb_alert then
					s_cs_xx.parametri.parametro_dec4_5 = 1
				else
					s_cs_xx.parametri.parametro_dec4_5 = 0
				end if
			end if
		
		elseif ls_tipo_stampa="B" then
			//11/04/2012: Beatrice, caso sfuso
			//visualizza sull'etichetta di spedizione le prime 3 lettere del deposito origine dell'ordine
			s_cs_xx.parametri.parametro_s_14 = left(f_des_tabella("anag_depositi", "cod_deposito='" +  ls_cod_deposito_origine + "'", "des_deposito" ), 3)
			
			//deposito del reparto corrente
			ls_temp = f_des_tabella("anag_reparti", "cod_reparto='" +  ls_cod_reparto + "'", "cod_deposito" )
			
			if ls_cod_deposito_origine<>"" and not isnull(ls_cod_deposito_origine) and ls_temp<>"" and not isnull(ls_temp) then
				
				if ls_temp<>ls_cod_deposito_origine then
					s_cs_xx.parametri.parametro_dec4_5 = 1
				else
					s_cs_xx.parametri.parametro_dec4_5 = 0
				end if
			end if
		end if				
								
		s_cs_xx.parametri.parametro_ul_1 = fl_barcode
	
end choose


window_open(w_stampa_etichette_spedizione,0)

if s_cs_xx.parametri.parametro_i_1 = 0 then 
	fs_errore = s_cs_xx.parametri.parametro_s_2
	return -1
end if



return 0
end function

public function integer uof_aggiorna_colli_det (long al_progr_det_produzione, ref string as_messaggio);long ll_anno_ord_ven, ll_num_ord_ven, ll_num_colli


select
	anno_registrazione,
	num_registrazione
into
	:ll_anno_ord_ven,
	:ll_num_ord_ven
from
	det_ordini_produzione
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_det_produzione = :al_progr_det_produzione;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura ordine collegato: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_messaggio = "Errore in lettura ordine collegato: progressivo ordine produzione non trovato"
	return -1
end if

select
	sum(num_colli)
into
	:ll_num_colli
from
	sessioni_lavoro
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_det_produzione in (select
										progr_det_produzione
									from
										det_ordini_produzione
									where
										cod_azienda = :s_cs_xx.cod_azienda and
										anno_registrazione = :ll_anno_ord_ven and
										num_registrazione = :ll_num_ord_ven);

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura totale colli: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_num_colli) then
	ll_num_colli = 0
end if

update
	tes_ord_ven
set
	num_colli = :ll_num_colli
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ll_anno_ord_ven and
	num_registrazione = :ll_num_ord_ven;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in aggiornamento colli ordine di vendita: " + sqlca.sqlerrtext
	return -1
end if

as_messaggio = "Ordine di vendita " + string(ll_anno_ord_ven) + "/" + string(ll_num_ord_ven) + &
					" aggiornato a " + string(ll_num_colli) + " collo/i totali"

return 0
end function

public function integer uof_fine_sessione (long fl_barcode, long fl_progr_sessione, string fs_cod_operaio, ref string fs_errore);// Funzione che finisce una sessione di lavoro iniziata
// nome: uof_fine_sessione
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 05-12-2003
//		Autore Diego Ferrari

boolean					lb_ologrammi, lb_procedi, lb_reset_colli, lb_chiedi_colli_quantita
string						ls_errore, ls_colli_v[], ls_cod_reparto, ls_tipo_stampa, ls_posizione, sigla_posizione_automatica, ls_flag_quan_colli_automatico
integer					li_risposta, li_anno_registrazione
long						ll_num_colli,ll_tempo_totale, ll_num_registrazione, ll_prog_riga_ord_ven, ll_tot_colli_reparti_prec, ll_cont
decimal					ld_quan_prodotta,ld_somma_quan_prodotta, ld_quan_da_produrre
decimal					ld_Q_DA_PRODURRE, ld_Q_GIA_PRODOTTA, ld_Q_RESIDUA, ld_Q_DIGITATA, ld_QUAN_OLOGRAMMA
datetime					ldt_adesso
s_cs_xx_parametri		lstr_parametri
datastore				lds_colli_reparti_prec



 lb_ologrammi = false
 lb_procedi = false

select quan_da_produrre, quan_prodotta
into   :ld_Q_DA_PRODURRE, :ld_Q_GIA_PRODOTTA
from   det_ordini_produzione
where  cod_azienda =:s_cs_xx.cod_azienda
and    progr_det_produzione=:fl_barcode;
 
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if


//CALCOLO IL RESIDUO CHE RESTA DA PRODURRE
ld_quan_da_produrre = ld_Q_DA_PRODURRE - ld_Q_GIA_PRODOTTA
ld_Q_RESIDUA = ld_quan_da_produrre


s_cs_xx.parametri.parametro_d_3 = ld_quan_da_produrre


ll_tot_colli_reparti_prec = uof_get_colli_reparti_prec(fl_barcode, ls_cod_reparto, lds_colli_reparti_prec, fs_errore)
if ll_tot_colli_reparti_prec<0 then
	//in fs_errore il messaggio
	return -1
end if

s_cs_xx.parametri.parametro_s_13 = ls_cod_reparto
s_cs_xx.parametri.parametro_ds_1 = lds_colli_reparti_prec
s_cs_xx.parametri.parametro_d_5 = dec(ll_tot_colli_reparti_prec)
setnull(ls_flag_quan_colli_automatico)
lb_chiedi_colli_quantita = true

select flag_quan_colli_automatico
into	:ls_flag_quan_colli_automatico
from  anag_reparti
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_reparto = :ls_cod_reparto;
if sqlca.sqlcode <> 0 then
	fs_errore = "uof_fine_sessione(): errore SQL." + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_flag_quan_colli_automatico) then ls_flag_quan_colli_automatico = "N"

// cerco il numero riga ordone
select anno_registrazione,
		num_registrazione,
		prog_riga_ord_ven
into	:li_anno_registrazione,
		:ll_num_registrazione,
		:ll_prog_riga_ord_ven
from	det_ordini_produzione
where cod_azienda = :s_cs_xx.cod_azienda and
		progr_det_produzione = :fl_barcode;
if sqlca.sqlcode <> 0 then
	fs_errore = "uof_fine_sessione(): errore SQL." + sqlca.sqlerrtext
	return -1
end if
		
// vedo se ci sono altri reparti aperti per questo ordine, escluso il reparto corrente
select count(*)
into	:ll_cont
from	det_ordini_produzione
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :li_anno_registrazione and
		num_registrazione = :ll_num_registrazione and
		prog_riga_ord_ven = :ll_prog_riga_ord_ven and
		flag_fine_fase ='N' and 
		cod_reparto <> :ls_cod_reparto;
if sqlca.sqlcode <> 0 then
	fs_errore = "uof_fine_sessione(): errore SQL." + sqlca.sqlerrtext
	return -1
end if

if ll_cont <> 0 and ls_flag_quan_colli_automatico = "S" then  lb_chiedi_colli_quantita=false

if lb_chiedi_colli_quantita then
	s_cs_xx.parametri.parametro_s_12 = string(fl_barcode)
	
	window_open(w_quantita_sessioni,0)
	
	lb_reset_colli = s_cs_xx.parametri.parametro_b_2
	
	setnull(s_cs_xx.parametri.parametro_s_13)
	setnull(s_cs_xx.parametri.parametro_d_5)
	setnull(s_cs_xx.parametri.parametro_ds_1)
	s_cs_xx.parametri.parametro_b_2 = false
	
	if s_cs_xx.parametri.parametro_i_1 = 0 then 
		fs_errore = "Processo annullato dall'operatore"
		return -1
	end if
	
	ld_quan_prodotta = round(s_cs_xx.parametri.parametro_d_1, 4)
	ll_num_colli = s_cs_xx.parametri.parametro_d_2
	ls_cod_reparto = ""
	ldt_adesso = datetime(today(),now())
	ls_posizione = s_cs_xx.parametri.parametro_s_12
else
	ld_quan_prodotta = ld_quan_da_produrre
	ll_num_colli = 1
	ldt_adesso = datetime(today(),now())
	setnull(s_cs_xx.parametri.parametro_s_13)
	setnull(s_cs_xx.parametri.parametro_d_5)
	setnull(s_cs_xx.parametri.parametro_ds_1)
	s_cs_xx.parametri.parametro_b_2 = false
	
	SELECT sigla_posizione_automatica
	into	:sigla_posizione_automatica
	from anag_reparti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_reparto = :ls_cod_reparto;
	if sqlca.sqlcode <> 0 then
		fs_errore = "uof_fine_sessione(): errore in ricerca reparto."
		return -1
	end if
	
	if len(sigla_posizione_automatica) > 0 then
		ls_posizione = sigla_posizione_automatica + string( day(today()),"00" ) + string( month(today()),"00" )
	else
		ls_posizione = ""
	end if
	ls_cod_reparto = ""
end if

s_cs_xx.parametri.parametro_s_12 = ""
if isnull(ls_posizione) then ls_posizione = ""

//#############################################################
//Donato 08/10/2012 controlla se devo fare la verifica/assegnazione ologrammi, 
//ma solo se ho indicato una quantità prodotta > 0

ld_Q_DIGITATA = ld_quan_prodotta
ld_QUAN_OLOGRAMMA = ld_Q_DIGITATA + (ld_Q_GIA_PRODOTTA - int(ld_Q_GIA_PRODOTTA) )

if ld_QUAN_OLOGRAMMA >= 1 THEN

	li_risposta = uof_trova_tipo(fl_barcode,&
										  ls_tipo_stampa,&
										  ls_errore)
	if ls_tipo_stampa="A" then
		
		li_risposta = uof_controlla_ologrammi(false, fl_barcode, li_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, fs_errore)
		if li_risposta = -1 then
			//in fs_errore il messaggio
			return -1
			
		elseif li_risposta=-2 then
			//assegnazione non prevista, vai avanti
			
	//	elseif li_risposta > 0 then
	//		//assegnazione ologrammi prevista ma è stata già effettuata la prima assegnazione
			
		else
			
			//solo se ci sono ancora ologrammi da verificare -----------
			select count(*)
			into :li_risposta
			from det_ord_ven_ologramma
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						anno_registrazione=:li_anno_registrazione and
						num_registrazione=:ll_num_registrazione and
						prog_riga_ord_ven=:ll_prog_riga_ord_ven and
						flag_verificato<>'S';
			//----------------------------------------------------------------
			
			if sqlca.sqlcode<0 then
				fs_errore = "Errore lettura ologrammi da verificare: "+sqlca.sqlerrtext
				return -1
				
			elseif sqlca.sqlcode=100 or isnull(li_risposta) or li_risposta<=0 then
				//o sono tutti a S oppure la tabella è vuota
				
				//c'è da fare l'assegnazione iniziale? (tabella vuota)
				select count(*)
				into :li_risposta
				from det_ord_ven_ologramma
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							anno_registrazione=:li_anno_registrazione and
							num_registrazione=:ll_num_registrazione and
							prog_riga_ord_ven=:ll_prog_riga_ord_ven;
				//----------------------------------------------------------------
				if li_risposta>0 then
					//Non risultano Ologrammi da verificare (tutti ad S) e l'assegnazione iniziale è stata anche fatta!,
					
				else
					//=(tabella ologrammi vuota)
					//Non risultano Ologrammi da verificare in quanto la tabella è vuota
					//pertanto procedi all'assegnazione iniziale + verifica contemporanea
					lb_procedi = true
					
				end if
		
			else
				//ci sono ologrammi da verificare (o sono tutti da verificare o solo parte di essi), procedi alla verifica
				lb_procedi = true
			end if
			
			
			//assegnazione ologrammi prevista e prima assegnazione NON ancora effettuata
			//OPPURE assegnazione ologrammi prevista ma è stata già effettuata la prima assegnazione
			if lb_procedi then
				//in ogni caso faccio una attività di verifica ----
				lstr_parametri.parametro_s_1 = "V"
				lstr_parametri.parametro_s_2 = "O"
				lstr_parametri.parametro_s_3 = fs_cod_operaio
				lstr_parametri.parametro_d_1_a[1] = li_anno_registrazione
				lstr_parametri.parametro_d_1_a[2] = ll_num_registrazione
				lstr_parametri.parametro_d_1_a[3] = ll_prog_riga_ord_ven
				
				//devo assegnare un numero di ologrammi pari alla quantità prodotta
				//lstr_parametri.parametro_d_1_a[4] = ld_quan_prodotta
				lstr_parametri.parametro_d_1_a[4] = ld_QUAN_OLOGRAMMA
				
				openwithparm(w_gestione_ologrammi, lstr_parametri)
				
				lstr_parametri = message.powerobjectparm
				//se hai terminatro il processo confermando l' assegnazione iniziale, il COMMIT alla fine confermerà tutto
				//altrimenti, non avendo fatto alcun update, ci sarà un nulla di fatto, anche con il COMMIT finale
				if lstr_parametri.parametro_b_1 then
				else
					fs_errore = "Operazione annullata dall'utente! Fase non chiusa!"
					return -1
				end if
				
				 lb_ologrammi = true
			end if
			
		end if
	end if
end if
//#############################################################


//se devo resettare i colli dei reparti dello stabilimento precedente, lo faccio
if lb_reset_colli and ll_tot_colli_reparti_prec>0 then
	//elimina i colli dei reparti degli stabilimenti precedenti, successivamente poi i conteggi dei colli saranno ricalcolati
	//e si avranno i totali corretti in tes_ord_ven
	if uof_elimina_colli_reparti_prec(lds_colli_reparti_prec, fs_errore) < 0 then
		//in fs_errore c'è il messaggio
	return -1
	end if
end if


update sessioni_lavoro
set cod_operaio_fine = :fs_cod_operaio,
    fine_sessione = :ldt_adesso,
	 quan_prodotta = :ld_quan_prodotta,
	 flag_fine_sessione='S',
	 num_colli=:ll_num_colli
where cod_azienda =:s_cs_xx.cod_azienda
and progr_det_produzione=:fl_barcode
and progr_sessione= :fl_progr_sessione;
 
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

//16/02/2011 Donato ---------------------------------------------------------------------
//aggiorna la tabella tab_ord_ven_colli
//incrementa con altri ll_num_colli (fb_reimposta_colli=FALSE)
if uof_crea_barcode_colli(fl_barcode, ll_num_colli, false, fs_errore) < 0 then
	//in fs_errore c'è il messaggio
	return -1
end if
//fine modifica --------------------------------------------------------------------------


// registra quan prodotta anche in det ordini produzione (che sarebbero le fasi di lavoro)
update det_ordini_produzione
set 	quan_prodotta = quan_prodotta + :ld_quan_prodotta,
		posizione = :ls_posizione
where cod_azienda =:s_cs_xx.cod_azienda
and progr_det_produzione=:fl_barcode;
 
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

// controllo per il fine fase di reparto e complessivo

select sum (quan_prodotta)
into   :ld_somma_quan_prodotta
from sessioni_lavoro
where cod_azienda=:s_cs_xx.cod_azienda
and   progr_det_produzione=:fl_barcode;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

select quan_da_produrre
into   :ld_quan_da_produrre
from   det_ordini_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr_det_produzione =:fl_barcode;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if


if ld_somma_quan_prodotta >= ld_quan_da_produrre then
	
	//se esistono sospensioni, nell'ambito del reparto, chiedi se si vuole sbloccare 
	//sia in caso di risposta afefrmatiova che negativa la fase viene comunque chiusa)
	//###########################################################
	uof_controlla_se_sospesa(fl_barcode, fs_cod_operaio)
	//###########################################################
	
	
	//fase reparto chiusa, se hai verificato/assegnato ologrammi, elimina quelli ancora in stato NON verificato
	//###########################################
	if  lb_ologrammi then
		
		delete from det_ord_ven_ologramma
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:li_anno_registrazione and 
					num_registrazione=:ll_num_registrazione and 
					prog_riga_ord_ven=:ll_prog_riga_ord_ven and
					flag_verificato<>'S';
		
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore in pulizia tabella ologrammi post chiusura fase:" + sqlca.sqlerrtext
			return -1
		end if
		
	end if
	//###########################################
	
	li_risposta = uof_calcola_tempo_fase(fl_barcode,&
													 ll_tempo_totale,&
												 	 ls_errore)
												 
	if li_risposta = -1 then
		fs_errore = ls_errore
		return -1
	end if
	
	
	
	update det_ordini_produzione
	set flag_fine_fase = 'S',
		 data_fine_fase = :ldt_adesso,
		 tempo_totale_impiegato=:ll_tempo_totale
	where cod_azienda =:s_cs_xx.cod_azienda
	and progr_det_produzione=:fl_barcode;
	 
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if


	li_risposta = uof_fine_produzione(fl_barcode,&
												 ls_errore)
												 
	if li_risposta = -1 then
		fs_errore = ls_errore
		return -1
	end if

else
	
	//fase non chiusa: se ci sono colli proponi quali stampare
	uof_carica_colli_barcode(fl_barcode, ls_colli_v[], ls_cod_reparto, ls_errore)
	if upperbound(ls_colli_v[]) >0 then
	else
		//no colli da stampare
		return 0

	end if
	
	s_cs_xx.parametri.parametro_s_11 = "RISTAMPA"
	li_risposta = uof_etichette(fl_barcode,&
										0,&
										0,&
										0,&
										ls_errore)
	s_cs_xx.parametri.parametro_s_11 = ""
	
	
end if



return 0
end function

public function integer uof_pronto_complessivo (long fl_barcode, ref string fs_errore);// Funzione che effettua il pronto complessivo su det_ordini_produzione e su sessioni_lavoro
// nome: uof_pronto_complessivo
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 17-12-2003
//		Autore Diego Ferrari

integer li_anno_registrazione,li_risposta
long ll_num_registrazione,ll_progr_det_produzione[],ll_t,ll_i,ll_progr_sessione
string ls_cod_reparto,ls_cod_operaio_inizio,ls_cod_operaio_fine,ls_errore
datetime ldt_inizio_preparazione,ldt_fine_preparazione
decimal ld_quan_da_produrre[]


select anno_registrazione,
       num_registrazione,
		 cod_reparto,
		 cod_operaio_inizio,
		 cod_operaio_fine,
		 inizio_preparazione,
		 fine_preparazione
into   :li_anno_registrazione,
       :ll_num_registrazione,
		 :ls_cod_reparto,
		 :ls_cod_operaio_inizio,
		 :ls_cod_operaio_fine,
		 :ldt_inizio_preparazione,
		 :ldt_fine_preparazione
from   tes_ordini_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr_tes_produzione=:fl_barcode;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if


ll_t = 1

declare r_det_ordini_produzione cursor for
select  progr_det_produzione,
		  quan_da_produrre
from    det_ordini_produzione
where   cod_azienda=:s_cs_xx.cod_azienda
and     anno_registrazione=:li_anno_registrazione
and     num_registrazione=:ll_num_registrazione
and     cod_reparto=:ls_cod_reparto
and     flag_fine_fase='N';

open r_det_ordini_produzione;

do while 1 = 1
	fetch r_det_ordini_produzione
	into  :ll_progr_det_produzione[ll_t],
	      :ld_quan_da_produrre[ll_t];
	
	if sqlca.sqlcode = 100 then exit
			
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close r_det_ordini_produzione;
		return -1
	end if
	
	ll_t++

loop

close r_det_ordini_produzione;


for ll_i = 1 to ll_t -1
	
	update det_ordini_produzione
	set    quan_prodotta = quan_da_produrre,
	       flag_fine_fase = 'S',
			 data_fine_fase =:ldt_fine_preparazione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    progr_det_produzione=:ll_progr_det_produzione[ll_i];
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if

	select max(progr_sessione)
	into   :ll_progr_sessione
	from   sessioni_lavoro
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    progr_det_produzione=:ll_progr_det_produzione[ll_i];
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	if sqlca.sqlcode = 100 or isnull(ll_progr_sessione) then
		ll_progr_sessione = 1
	else
		ll_progr_sessione++
	end if

	insert into sessioni_lavoro
	(cod_azienda,
	 progr_det_produzione,
	 progr_sessione,
	 progr_tes_produzione,
	 cod_operaio_inizio,
	 cod_operaio_fine,
	 inizio_sessione,
	 fine_sessione,
	 quan_prodotta,
	 flag_fine_sessione,
	 num_colli)
	 values
	 (:s_cs_xx.cod_azienda,
	  :ll_progr_det_produzione[ll_i],
	  :ll_progr_sessione,
	  :fl_barcode,
	  :ls_cod_operaio_inizio,
	  :ls_cod_operaio_fine,
	  :ldt_inizio_preparazione,
	  :ldt_fine_preparazione,
	  :ld_quan_da_produrre[ll_i],
	  'S',
	  0);
	  
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	//qui verifico se tutte i det_ordini_produzione di tutti i reparti sono stati già chiusi.

	li_risposta = uof_fine_produzione(ll_progr_det_produzione[ll_i],&
												 ls_errore)
													 
	if li_risposta = -1 then
		fs_errore = ls_errore
		return -1
	end if

next

//se esistono sospensioni, nell'ambito del reparto, chiedi se si vuole sbloccare 
//sia in caso di risposta afefrmatiova che negativa la fase viene comunque chiusa)
//###########################################################
uof_controlla_se_sospesa(fl_barcode, ls_cod_operaio_fine)
//###########################################################

// richiama la procedura di stampa etichette
li_risposta = uof_etichette(fl_barcode,&
											0,&
											0,&
											0,&
											ls_errore)
if li_risposta < 0 then
	fs_errore = ls_errore
	return -1
end if

return 0
end function

public function integer uof_pronto_parziale (long fl_barcode, string fs_cod_operaio, ref string fs_errore);// Funzione che effettua il pronto pronto parziale
// nome: uof_pronto_parziale
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 22-12-2003
//		Autore Diego Ferrari

integer li_anno_registrazione,li_risposta
long ll_num_registrazione,ll_t,ll_i,ll_riga,ll_prog_riga_ord_ven,ll_progr_det_produzione,ll_progr_sessione,ll_tempo_totale
string ls_cod_reparto,ls_cod_operaio_inizio,ls_cod_operaio_fine,ls_cod_operaio_fine_sessione_prec,ls_errore,ls_cod_prodotto, & 
		 ls_des_prodotto
datetime ldt_inizio_preparazione,ldt_fine_preparazione,ldt_fine_sessione_prec,ldt_adesso
decimal ld_quan_prodotta,ld_num_colli,ld_quan_da_produrre,ld_somma_quan_prodotta,ld_somma_num_colli

select		anno_registrazione,
			num_registrazione,
			cod_reparto,
			cod_operaio_inizio,
			cod_operaio_fine,
			inizio_preparazione,
			fine_preparazione
into	:li_anno_registrazione,
		:ll_num_registrazione,
		:ls_cod_reparto,
		:ls_cod_operaio_inizio,
		:ls_cod_operaio_fine,
		:ldt_inizio_preparazione,
		:ldt_fine_preparazione
from   tes_ordini_produzione
where		cod_azienda=:s_cs_xx.cod_azienda and
			progr_tes_produzione=:fl_barcode;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

s_cs_xx.parametri.parametro_ul_1 = li_anno_registrazione
s_cs_xx.parametri.parametro_ul_2 = ll_num_registrazione
s_cs_xx.parametri.parametro_s_1 = ls_cod_reparto

s_cs_xx.parametri.parametro_ds_1 = create datastore
s_cs_xx.parametri.parametro_ds_1.dataobject = "d_elenco_prodotti_produzione"

// **** carica i valori nel datastore

declare r_det_ordini_produzione cursor for
select		cod_prodotto,
			quan_da_produrre - quan_prodotta,
			prog_riga_ord_ven,
			progr_det_produzione
from	det_ordini_produzione
where		cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:li_anno_registrazione and
			num_registrazione=:ll_num_registrazione and
			cod_reparto=:ls_cod_reparto and
			flag_fine_fase='N'
order by prog_riga_ord_ven;

open r_det_ordini_produzione;

do while 1 = 1
	fetch r_det_ordini_produzione
	into			:ls_cod_prodotto,
					:ld_quan_da_produrre,
					:ll_prog_riga_ord_ven,
					:ll_progr_det_produzione;
	
	if sqlca.sqlcode = 100 then exit
			
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;

	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if

	ll_riga = s_cs_xx.parametri.parametro_ds_1.insertrow(0)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"quan_da_produrre",ld_quan_da_produrre)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"prog_riga_ord_ven",ll_prog_riga_ord_ven)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"progr_det_produzione",ll_progr_det_produzione)
loop

close r_det_ordini_produzione;

// **** fine carica datastore

window_open(w_elenco_prodotti_ordine,0)

if s_cs_xx.parametri.parametro_i_1 = 0 then 
	fs_errore = s_cs_xx.parametri.parametro_s_2
	return -1
end if

ldt_adesso = datetime(today(),now())

for ll_t = 1 to s_cs_xx.parametri.parametro_ds_1.rowcount()
	ll_progr_det_produzione = s_cs_xx.parametri.parametro_ds_1.getitemnumber(ll_t,"progr_det_produzione")
	ll_prog_riga_ord_ven= s_cs_xx.parametri.parametro_ds_1.getitemnumber(ll_t,"prog_riga_ord_ven")
	ld_quan_prodotta = s_cs_xx.parametri.parametro_ds_1.getitemnumber(ll_t,"quan_prodotta")
	ld_num_colli = s_cs_xx.parametri.parametro_ds_1.getitemnumber(ll_t,"num_colli")
	
	// come prima cosa controlla che non ci siano altre sessioni per questo progr_det_produzione
	// se non ci sono la crea nuova usando data inizio e operaio inizio di tes_ordini_produzione
	// se c'è crea una nuova sessione impostando data inizio e operaio inizio uguali a data fine e operaio fine
	// della sessione precedente.
	
	if ld_quan_prodotta = 0 then continue
	
	select max(progr_sessione)
	into   :ll_progr_sessione
	from   sessioni_lavoro
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    progr_det_produzione=:ll_progr_det_produzione;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	if sqlca.sqlcode = 100 or isnull(ll_progr_sessione) then
		
		ll_progr_sessione = 1
		
		insert into sessioni_lavoro
		(cod_azienda,
		 progr_det_produzione,
		 progr_sessione,
		 progr_tes_produzione,
		 cod_operaio_inizio,
		 cod_operaio_fine,
		 inizio_sessione,
		 fine_sessione,
		 quan_prodotta,
		 flag_fine_sessione,
		 num_colli)
		 values
		 (:s_cs_xx.cod_azienda,
		  :ll_progr_det_produzione,
		  :ll_progr_sessione,
		  :fl_barcode,
		  :ls_cod_operaio_inizio,
		  :fs_cod_operaio,
		  :ldt_inizio_preparazione,
		  :ldt_adesso,
		  :ld_quan_prodotta,
		  'S',
		  :ld_num_colli);
		  
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
			

	else
	
		select cod_operaio_fine,
				 fine_sessione
		into   :ls_cod_operaio_fine_sessione_prec,
				 :ldt_fine_sessione_prec
		from   sessioni_lavoro
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    progr_det_produzione=:ll_progr_det_produzione
		and    progr_sessione=:ll_progr_sessione;
		
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		ll_progr_sessione++
		
		insert into sessioni_lavoro
		(cod_azienda,
		 progr_det_produzione,
		 progr_sessione,
		 progr_tes_produzione,
		 cod_operaio_inizio,
		 cod_operaio_fine,
		 inizio_sessione,
		 fine_sessione,
		 quan_prodotta,
		 flag_fine_sessione,
		 num_colli)
		 values
		 (:s_cs_xx.cod_azienda,
		  :ll_progr_det_produzione,
		  :ll_progr_sessione,
		  :fl_barcode,
		  :ls_cod_operaio_fine_sessione_prec,
		  :fs_cod_operaio,
		  :ldt_fine_sessione_prec,
		  :ldt_adesso,
		  :ld_quan_prodotta,
		  'S',
		  :ld_num_colli);
		  
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
			
	end if				 


	// registra quan prodotta anche in det ordini produzione (che sarebbero le fasi di lavoro)
	
	update det_ordini_produzione
	set quan_prodotta = quan_prodotta + :ld_quan_prodotta
	where cod_azienda =:s_cs_xx.cod_azienda
	and progr_det_produzione=:ll_progr_det_produzione;
	 
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	
	// controllo per il fine fase di reparto e complessivo
	
	select sum (quan_prodotta)
	into   :ld_somma_quan_prodotta
	from sessioni_lavoro
	where cod_azienda=:s_cs_xx.cod_azienda
	and   progr_det_produzione=:ll_progr_det_produzione;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	select quan_da_produrre
	into   :ld_quan_da_produrre
	from   det_ordini_produzione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    progr_det_produzione =:ll_progr_det_produzione;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	
	if ld_somma_quan_prodotta >= ld_quan_da_produrre then
		//chiudere la fase di produzione del reparto
		
		li_risposta = uof_calcola_tempo_fase(ll_progr_det_produzione,&
														 ll_tempo_totale,&
														 ls_errore)
													 
		if li_risposta = -1 then
			fs_errore = ls_errore
			return -1
		end if
		
		update det_ordini_produzione
		set flag_fine_fase = 'S',
			 data_fine_fase = :ldt_adesso,
			 tempo_totale_impiegato=:ll_tempo_totale
		where cod_azienda =:s_cs_xx.cod_azienda
		and progr_det_produzione=:ll_progr_det_produzione;
		 
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		li_risposta = uof_fine_produzione(ll_progr_det_produzione,&
													 ls_errore)
													 
		if li_risposta = -1 then
			fs_errore = ls_errore
			return -1
		end if
		
	end if
	
	//14/03/2011 Donato ---------------------------------------------------------------------
	//aggiorna la tabella tab_ord_ven_colli
	//incrementa con altri ll_num_colli (fb_reimposta_colli=FALSE)
	if uof_crea_barcode_colli(fl_barcode, ld_num_colli, false, fs_errore) < 0 then
		//in fs_errore c'è il messaggio
		rollback;
		return -1
	end if
	//fine modifica --------------------------------------------------------------------------
	
next

// controllo se tutti i det_ordini_produzione relativi allo stesso tes_ordini_produzione sono stati chiusi


declare r_det_ordini_produzione_1 cursor for
select  progr_det_produzione
from    det_ordini_produzione
where   cod_azienda=:s_cs_xx.cod_azienda
and     anno_registrazione=:li_anno_registrazione
and     num_registrazione=:ll_num_registrazione
and     cod_reparto=:ls_cod_reparto
and     flag_fine_fase='N';

open r_det_ordini_produzione_1;

fetch r_det_ordini_produzione_1
into  :ll_progr_det_produzione;

if sqlca.sqlcode = 100 then // significa che tutti i det_ordini_produzione sono chiusi e quindi chiude anche tes_ordini_produzione
	
	update tes_ordini_produzione
	set    cod_operaio_fine=:fs_cod_operaio,
			 fine_preparazione=:ldt_adesso,
			 flag_pronto_complessivo = 'S'			
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    progr_tes_produzione=:fl_barcode;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	//se esistono sospensioni, nell'ambito del reparto, chiedi se si vuole sbloccare 
	//sia in caso di risposta afefrmatiova che negativa la fase viene comunque chiusa)
	//###########################################################
	uof_controlla_se_sospesa(fl_barcode, fs_cod_operaio)
	//###########################################################
	
	
	// chiama la procedura per la stampa delle etichette
	li_risposta = uof_etichette(fl_barcode,&
											0,&
											0,&
											0,&
											ls_errore)
	if li_risposta < 0 then
		fs_errore = ls_errore
		return -1
	end if
	
	
	
end if

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if
	
close r_det_ordini_produzione_1;


// AGGIORNA NUMERO COLLI SU TES_ORDINI_PRODUZIONE OGNI VOLTA CHE VIENE ESEGUITO UN PRONTO PARZIALE
select sum(num_colli)
into   :ld_somma_num_colli
from   sessioni_lavoro
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr_tes_produzione=:fl_barcode;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

update tes_ordini_produzione
set    num_colli=:ld_somma_num_colli			
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr_tes_produzione=:fl_barcode;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

public function integer uof_rileva_produzione (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_reparto, string fs_cod_operaio, ref string fs_errore);// Funzione che esegue la rilevazione di produzione per PROGETTOTENDA SPA
// nome: uof_rileva_produzione
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 05-12-2003
//		Autore Diego Ferrari

string			ls_flag_fine_sessione, ls_errore, ls_flag_fine_fase, ls_tipo_stampa, ls_flag_etichetta_sp, ls_posizione, ls_cod_cliente
long			ll_progr_sessione, ll_num_colli, ll_barcode
integer		li_risposta
datetime		ldt_inizio_preparazione_test, ldt_fine_preparazione_test, ldt_inizio_preparazione, ldt_fine_preparazione
s_cs_xx_parametri lstr_parametri
dec{4}		ld_quan_ordine

//controlla il tipo ordine (tipo stampa) se A= tende da sole, B=sfuso o C=tende tecniche

if isnull(fl_barcode) then
	
	setnull(ll_barcode)
	
	li_risposta = uof_barcode_anno_reg(ll_barcode,&
												  fi_anno_registrazione, &
												  fl_num_registrazione,&
												  fl_prog_riga_ord_ven,&
												  fs_cod_reparto,&
												  ls_errore)

	if li_risposta < 0 then
		fs_errore = ls_errore
		return -1
	end if
	
	fl_barcode = ll_barcode
	
end if

li_risposta = uof_trova_tipo(fl_barcode,&
									  ls_tipo_stampa,&
									  ls_errore)

if li_risposta < 0 then
	fs_errore = ls_errore
	rollback; // stefanop 09/03/2010
	return -1
end if

choose case ls_tipo_stampa
	case 'A'

		select flag_fine_fase
		into   :ls_flag_fine_fase
		from   det_ordini_produzione
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    progr_det_produzione=:fl_barcode;
		
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		if sqlca.sqlcode = 100 then
			fs_errore = "Attenzione! La produzione per questo ordine non è ancora stata lanciata. Verificare e rilanciare la produzione."
			return -1
		end if
		
		if ls_flag_fine_fase='S' then
			fs_errore = "Attenzione! La fase risulta già chiusa, pertanto non è possibile iniziare nuove sessioni di lavoro."
			return -1
		end if
		
		select max(progr_sessione)
		into   :ll_progr_sessione
		from   sessioni_lavoro
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    progr_det_produzione=:fl_barcode;
		
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		if sqlca.sqlcode = 100 or isnull(ll_progr_sessione) then
			ll_progr_sessione = 1
			
			li_risposta = uof_nuova_sessione(fl_barcode, &
														ll_progr_sessione,&
														fs_cod_operaio,&
														ls_errore)
															
			if li_risposta < 0 then
				fs_errore = ls_errore
				rollback; // stefanop 09/03/2010
				return -1
			end if
			
			
			if uof_mrp_disimpegna_ordine(fl_barcode, fs_errore) < 0 then
				rollback;
				return -1
			end if
			
			
			// aggiunto per progettotenda. mdg modifica_etichette rev 1 (enme 20/03/2006)
			li_risposta = uof_etichette_apertura (fl_barcode,&
			                        fi_anno_registrazione, &
											fl_num_registrazione, &
											fl_prog_riga_ord_ven,&
											ref fs_errore )			
											
			if li_risposta < 0 then
				fs_errore = ls_errore
				return -1
			end if
			// fine modifica enme 20/03/2006
			
			//Donato 03/01/2012: solo per tende tipo A
			//se il reparto lo prevede chiedi se vuoi stampare l'etichetta scheda prodotto
			
			if isnull(fs_cod_reparto) or fs_cod_reparto="" then
				select cod_reparto
				into :fs_cod_reparto
				from   det_ordini_produzione
				where	cod_azienda=:s_cs_xx.cod_azienda and
							progr_det_produzione =:fl_barcode;
			end if
			
			select flag_etichetta_sp
			into   :ls_flag_etichetta_sp
			from   anag_reparti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_reparto = :fs_cod_reparto;
			if sqlca.sqlcode < 0 then 
				fs_errore = "Errore in ricerca flag_chiusura in anagrafica reparti~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
			if ls_flag_etichetta_sp = "S" then
				if g_mb.confirm("Attenzione!", "Il reparto "+fs_cod_reparto+" prevede la stampa etichetta Scheda Prodotto. Vuoi stamparla?", 1) then
				
					li_risposta = uof_etichette_scheda_prodotto(fl_barcode, fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, fs_cod_reparto, fs_errore)
					if li_risposta < 0 then
						//in fs_errore il messaggio
						return -1
					end if
					
				end if
			end if
			//------------------------------------------------------------------------------------
			
			li_risposta = uof_ass_iniziale_ologramma(false, fl_barcode, fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, fs_cod_operaio, fs_errore)
			
			choose case li_risposta
				case is <0
					//in fs_errore il messaggio
					return -1
					
				case 1
					//assegnazione non prevista, vai avanti OPPURE assegnazione ologrammi prevista ma è stata già effettuata la prima assegnazione
					//in questo punto non dare alcun messaggio, vai semplicemente avanti (anche se fs_errore ha un valore non vuoto)
					
				case 2
					//probabile annullamento da parte dell'utente
					//non dare alcun messaggio, chi ha già pensato la finestra di assegnazione/verifica ologrammi
					
				case else
					//assegnazione iniziale effettuata con successo
					//il COMMIT alla fine confermerà tutto
					//altrimenti, non avendo fatto alcun update, ci sarà un nulla di fatto, anche con il COMMIT finale
				
			end choose
			
			
		else
			select flag_fine_sessione
			into   :ls_flag_fine_sessione
			from   sessioni_lavoro
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    progr_det_produzione=:fl_barcode
			and    progr_sessione = :ll_progr_sessione;
			
			if sqlca.sqlcode < 0 then 
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if
			
			if ls_flag_fine_sessione = 'S' then
				
				// (si veda spec.req. "avanzamento di produzione rev.7 funzione 8) 
				// Si assume che le rilevazioni di sessione successive alla prima siano solo di fine sessione.
				//	In pratica se una tenda da sole è prodotta in un reparto in più sessioni, la rilevazione 
				//	dell'inizio è fatta solo per la prima sessione. La seconda sessione, ad esempio, avrà come inizio i 
				//	dati riguardanti la fine della prima sessione vale a dire data/ora e operaio.
				//	Sostanzialmente la procedura rileverà solo i fine sessione se il numero di sessioni è superiore a uno.
				//	Si osserva che in questo modo, il tempo totale di fase non sarà corretto
				
				// duplica sessione già esistente
				li_risposta = uof_duplica_sessione(fl_barcode, &
															  ll_progr_sessione,&
															  ls_errore)
															
				if li_risposta < 0 then
					fs_errore = ls_errore
					rollback; // stefanop 09/03/2010
					return -1
				end if
				
				ll_progr_sessione++  // questo serve poichè la sessione appena creata dalla uof_duplica_sessione è nuova
				
				// chiude la sessione appena duplicata
				li_risposta = uof_fine_sessione(fl_barcode, &
															ll_progr_sessione,&
															fs_cod_operaio,&
															ls_errore)
															
				if li_risposta < 0 then
					fs_errore = ls_errore
					rollback; // stefanop 09/03/2010
					return -1
				end if
				
				if uof_aggiorna_colli_det(fl_barcode,fs_errore) <> 0 then
					rollback; // stefanop 09/03/2010
					return -1
				end if
				
			else
				// significa che c'è una sessione già aperta
				li_risposta = uof_fine_sessione(fl_barcode, &
															ll_progr_sessione,&
															fs_cod_operaio,&
															ls_errore)
															
				if li_risposta < 0 then
					fs_errore = ls_errore
					rollback; // stefanop 09/03/2010
					return -1
				else
					commit;
				end if
				
				if uof_aggiorna_colli_det(fl_barcode,fs_errore) <> 0 then
					return -1
				end if
				
				// aggiunto per centro gibus. mdg modifica_etichette rev 1 (enme 25/02/2008)
				if uof_etichette_produzione_cg(fl_barcode,&
			                        fi_anno_registrazione, &
											fl_num_registrazione, &
											fl_prog_riga_ord_ven,&
											ref fs_errore ) <> 0 then
					return -1
				end if
				// fine aggiunta 
											
			end if
		
		end if
	
	// tende tecniche o sfuso
	case 'B','C'
		
		// controlla se la fase in tes_ordini_produzione è stata già iniziata/finita
		
		select inizio_preparazione,
				 fine_preparazione
		into   :ldt_inizio_preparazione_test,
		  		 :ldt_fine_preparazione_test
		from   tes_ordini_produzione
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    progr_tes_produzione=:fl_barcode;
		
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		if not isnull(ldt_fine_preparazione_test) then		
			fs_errore = "La fase risulta già essere stata chiusa pertanto non è possibile procedere con alcuna operazione."
			return -1
		end if
		
		if isnull(ldt_inizio_preparazione_test) then
			
			// se è nulla allora inizia la preparazione complessiva
			
			ldt_inizio_preparazione = datetime(today(),now())
			
			update tes_ordini_produzione
			set    cod_operaio_inizio = :fs_cod_operaio,
					 inizio_preparazione = :ldt_inizio_preparazione
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    progr_tes_produzione=:fl_barcode;
		
			if sqlca.sqlcode < 0 then 
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				rollback; // stefanop 09/03/2010
				return -1
			end if
			
			
			if uof_mrp_disimpegna_ordine(fl_barcode, fs_errore) < 0 then
				rollback;
				return -1
			end if
			
		else
			// se è già stata iniziata chiede se si tratta di pronto complessivo o meno
			
			// ****************************    ATTENZIONE!   ******************************
			//  qui bisognerà decidere se vale la pena non fare una msgbox per poter rendere l'oggetto assolutamente non
			//  grafico.....nel caso, ad esempio, di volerlo riutilizzare per la intranet o affini.........
			//  per ora lasciamo la messagebox (prevista anche dalla specifica)
			
			li_risposta = messagebox("SEP","Pronto Complessivo?",question!,yesnocancel!,1)
			
			choose case li_risposta

				case 1   //caso pronto complessivo
					// chiede il numero complessivo di colli, aggiorna record in det_ordini_produzione, crea in 
					// automatico una sessione per ogni det_ordine_produzione
					
					s_cs_xx.parametri.parametro_s_12 = string(fl_barcode)
					window_open(w_num_colli,0)

					if s_cs_xx.parametri.parametro_i_1 = 0 then 
						fs_errore = "Processo annullato dall'operatore"
						return -1
					end if
				
					ll_num_colli = s_cs_xx.parametri.parametro_d_2
					ldt_fine_preparazione = datetime(today(),now())
					
					ls_posizione = s_cs_xx.parametri.parametro_s_12
					
					update tes_ordini_produzione
					set    cod_operaio_fine=:fs_cod_operaio,
							 fine_preparazione=:ldt_fine_preparazione,
							 flag_pronto_complessivo = 'S',
							 num_colli = num_colli + :ll_num_colli,
							 posizione = :ls_posizione
					where  cod_azienda=:s_cs_xx.cod_azienda
					and    progr_tes_produzione=:fl_barcode;
					
					if sqlca.sqlcode < 0 then 
						fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
						rollback; // stefanop 09/03/2010
						return -1
					end if
					
					//14/03/2011 Donato ---------------------------------------------------------------------
					//aggiorna la tabella tab_ord_ven_colli
					//incrementa con altri ll_num_colli (fb_reimposta_colli=FALSE)
					if uof_crea_barcode_colli(fl_barcode, ll_num_colli, false, fs_errore) < 0 then
						//in fs_errore c'è il messaggio
						rollback;
						return -1
					end if
					//fine modifica --------------------------------------------------------------------------
					
					
					li_risposta = uof_pronto_complessivo(fl_barcode, &
																	 ls_errore)
															
					if li_risposta < 0 then
						fs_errore = ls_errore
						rollback; // stefanop 09/03/2010
						return -1
					end if
					
					if uof_aggiorna_colli_tes(fl_barcode,fs_errore) <> 0 then
						rollback; // stefanop 09/03/2010
						return -1
					end if
					
					// rimane da controllare se tutte le fasi in tes ordini produzione relative allo stesso ordine sono state chiuse
					//rimane da decidere se scatenare la chiusura commessa per ogni singola riga d'ordine oppure solo quando tutto l'ordine viene finito.

					
				case 2   // caso manuale
					li_risposta = uof_pronto_parziale(fl_barcode, &
																 fs_cod_operaio,&
																 ls_errore)
															
					if li_risposta < 0 then
						fs_errore = ls_errore
						rollback; // stefanop 09/03/2010
						return -1
					end if
					
					if uof_aggiorna_colli_tes(fl_barcode,fs_errore) <> 0 then
						rollback; // stefanop 09/03/2010
						return -1
					end if
					
				case 3  // caso operazione annullata
					
					fs_errore = "Processo annullata dall'operatore"
					return -1
					
			end choose
			
			//passo questo param alla finestra per far capire che devo stampare le etichettine relative al barcode univoco dei colli
			//perchè provengo dalla rilevazione colli
			s_cs_xx.parametri.parametro_s_11 = "COLLI"
			
			// stampa etichette se necessario (aggiunto per Centro Gibus 24/1/2008)
			li_risposta = uof_etichette_tecniche (fl_barcode,&
			                        fi_anno_registrazione, &
											fl_num_registrazione, &
											ref fs_errore )			
			
			//reset del semaforo
			s_cs_xx.parametri.parametro_s_11 = ""
			
			
			if li_risposta < 0 then
				fs_errore = ls_errore
				rollback; // stefanop 09/03/2010
				return -1
			end if
			// fine modifica enme 24/1/08
			
		end if
	
end choose

return 0
end function

public function integer uof_fine_produzione (long fl_barcode, ref string fs_errore);/* Funzione che controlla se tutte le fasi di una riga d'ordine sono chiuse, se si cihude la produzione di quella riga d'ordine
	nome: uof_fine_produzione
	tipo: intero
			 0 passed
			-1 failed
	
	
		Creata il 09-12-2003 - Autore Diego Ferrari
		
		07-10-2008 Enrico: modificato per aggiungere parametrro "CAC - chiusura automatica commessa" che 
		permette di chiudere SEMPRE la commessa alla fine della produzione indipendemente dal valore del 
		flag_chiusura_automatica presente di distinta_padri
*/

boolean						lb_test

string							ls_cod_reparto,ls_errore,ls_flag_fine_fase,ls_cod_prodotto,ls_flag_chiusura_automatica,ls_cod_versione, &
								ls_flag_tipo_avanzamento, ls_flag_etichetta_chiusura, ls_flag, ls_tipo_stampa, ls_null, ls_colli_v[], ls_tipo_ordine

integer						li_risposta, li_anno_registrazione,li_anno_commessa

long							ll_num_registrazione,ll_prog_riga_ord_ven,ll_num_commessa, ll_colli_prod, ll_colli_tes,ll_test

uo_funzioni_1				luo_funzioni_1

boolean						lb_sab_dom = false
uo_calendario_prod_new 	luo_cal_prod


li_risposta = uof_barcode_anno_reg(fl_barcode,&
											  li_anno_registrazione, &
											  ll_num_registrazione,&
											  ll_prog_riga_ord_ven,&
											  ls_cod_reparto,&
											  ls_errore)

if li_risposta < 0 then
	fs_errore = ls_errore
	return -1
end if

lb_test = false
setnull(ls_null)


// verifico se ci sono ancora fasi aperte
declare r_det_ordini_produzione cursor for
select  flag_fine_fase
from    det_ordini_produzione
where   cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione =:li_anno_registrazione and
			num_registrazione=:ll_num_registrazione and
			prog_riga_ord_ven=:ll_prog_riga_ord_ven;

open r_det_ordini_produzione;

do while true
	fetch r_det_ordini_produzione
	into  :ls_flag_fine_fase;
	
	if sqlca.sqlcode = 100 then exit
			
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close r_det_ordini_produzione;
		return -1
	end if

	if ls_flag_fine_fase ='N' then lb_test = true
	
loop
	
close r_det_ordini_produzione;

// se non ci sono fasi aperte entro in questo IF e faccio le operazioni finali.
if not lb_test then
	select cod_prodotto,
			 cod_versione,
			 anno_commessa,
			 num_commessa
	into   :ls_cod_prodotto,
			 :ls_cod_versione,
	       :li_anno_commessa,
			 :ll_num_commessa
	from   det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione =:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione
	and    prog_riga_ord_ven=:ll_prog_riga_ord_ven;

	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if

	// se esiste la commessa allora controlla flag di chiusura automatica commesse in distinta padri ####################################
	if not isnull(ll_num_commessa) and not isnull(li_anno_commessa) then
		
		// modifica EnMe 07-10-2008
		setnull(ls_flag)
		
		select flag
		into   :ls_flag
		from   parametri_azienda
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_parametro = 'CAC';
		if sqlca.sqlcode <> 0 then setnull(ls_flag)
		
		if isnull(ls_flag) then ls_flag = "N"
		
		select flag_chiusura_automatica
		into   :ls_flag_chiusura_automatica
		from   distinta_padri
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto
		and    cod_versione=:ls_cod_versione;
	
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		if ls_flag_chiusura_automatica = "S" or ls_flag = "S" then 

		// ESEGUO LA PROCEDURA DI CHIUSURA COMMESSA
	
			select flag_tipo_avanzamento
			into   :ls_flag_tipo_avanzamento
			from   anag_commesse
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_commessa=:li_anno_commessa
			and    num_commessa=:ll_num_commessa;
			
			if sqlca.sqlcode < 0 then 
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if
			
			if ls_flag_tipo_avanzamento = "9" or ls_flag_tipo_avanzamento = "8" or ls_flag_tipo_avanzamento = "7" or &
				ls_flag_tipo_avanzamento = "1" or ls_flag_tipo_avanzamento = "2" then
				
				//questo messaggio non serve ad una emerita minkia !!!!!
				//se la commessa è già chiusa tanto meglio, non scassare la minkia all'operatore che altrimenti non riuscirebbe a chiudere la sua fase ....
				
//				fs_errore = "Attenzione! La commessa si trova in uno stato per il quale non è più possibile effettuare un avanzamento automatico."
//				return -1
				
			else
				luo_funzioni_1 = create uo_funzioni_1
				li_risposta = luo_funzioni_1.uof_avanza_commessa(li_anno_commessa,ll_num_commessa,ls_null,ls_errore)
				
				if li_risposta < 0 then 
					fs_errore = "durante l'avanzamento di produzione in chiusura automatica commessa si è verificato un errore sulla commessa anno " + string(li_anno_commessa) +" numero " + string(ll_num_commessa) + ". Dettaglio errore: " + ls_errore
					destroy luo_funzioni_1
					return -1
				end if
				
				commit;
				
				destroy luo_funzioni_1
				
				
			end if 
	
		//  FINE procedura di chiusura della commessa
	
		end if

	end if
	//#########################################################################################################
	
	//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//Donato 15/11/2011 spostato qui lo script perchè deve essere eseguito a prescindere che ci sia una commessa sulla riga oppure no
	//pulizia tabella det_ord_ven_prod, MA PRIMA AGGIORNA LA TAB_CAL_PRODUZIONE
	
	luo_cal_prod = create uo_calendario_prod_new
	li_risposta = luo_cal_prod.uof_disimpegna_calendario(li_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, fs_errore)
	destroy luo_cal_prod;
	
	if li_risposta<0 then
		//messaggio ma non blocco
		//g_mb.warning(ls_errore)
		
	else
		/*
		delete det_ord_ven_prod
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :li_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione
		and    prog_riga_ord_ven  =: ll_prog_riga_ord_ven;
		*/
		update det_ord_ven_prod
		set flag_attivo = 'N'
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_registrazione = :li_anno_registrazione
		and    num_registrazione  = :ll_num_registrazione
		and    prog_riga_ord_ven  =: ll_prog_riga_ord_ven;
	end if
	
	//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	// se si tratta di tende da sole chiama la procedura per la stampa delle etichette
	
	select progr_tes_produzione
	into   :ll_test
	from   det_ordini_produzione
	where  cod_azienda=:s_cs_xx.cod_azienda and
			 progr_det_produzione=:fl_barcode;
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore in select tabella det_ordini_produzione~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	select flag_etichetta_chiusura
	into   :ls_flag_etichetta_chiusura
	from   anag_reparti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_reparto = :ls_cod_reparto;
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in ricerca flag_chiusura in anagrafica reparti~r~n" + sqlca.sqlerrtext
		return -1
	end if
		
	// se il reparto prevede l'etichetta di chiusura
	if isnull(ll_test) or ll_test = 0 then
		// se il reparto prevede l'etichetta di chiusura
		if ls_flag_etichetta_chiusura = "S" then
		
			li_risposta = uof_etichette(fl_barcode, 0, 0, 0, ls_errore)
			if li_risposta < 0 then
				fs_errore = ls_errore
				return -1
			end if
			
		end if
	end if
	
/*
// EnMe (25/03/2019) Essendo una cosa richiesta da GIBUS allora la commento perchè è fuori controllo dai flag del reparto.
else
	
	//09/03/2012 Beatrice e Daniele hanno chiesto questo
	//anche in presenza di altre fasi aperte, se ci sono colli associati al barcode proponi di stamparli
	//NOTA BENE solo caso report 1
	
	uof_trova_tipo(fl_barcode, ls_tipo_ordine, ls_errore)
	
	if ls_tipo_ordine = "A" then
		uof_carica_colli_barcode(fl_barcode, ls_colli_v[], ls_cod_reparto, ls_errore)
		
		if upperbound(ls_colli_v[]) >0 then
			//ci sono colli da stampare, apri la finestra di stampa e chiedi quali colli stampare
			s_cs_xx.parametri.parametro_s_11 = "RISTAMPA"
			li_risposta = uof_etichette(fl_barcode,&
											0,&
											0,&
											0,&
											ls_errore)
			s_cs_xx.parametri.parametro_s_11 = ""
		end if
		
	end if
	//fine modifica ---------------------------------------------------------
*/
end if

return 0
end function

public function integer uof_etichette_apertura (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_errore);// Funzione che esegue la stampa delle etichette di aprtura per PROGETTOTENDA SPA
//	vedi specifica requisiti "Modifica_etichette" rev. 1 del 7/3/2006
//	richiesta da Beatrice
//
// nome: uof_etichette_apertura
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 20-03-2006
//		Autore Enrico Menegotto

string				ls_errore,ls_tipo_stampa,ls_cod_reparto,ls_cod_cliente,ls_rag_soc_1_div,ls_rag_soc_2_div,ls_indirizzo_div,ls_localita_div, &
					ls_frazione_div,ls_cap_div,ls_cod_giro_consegna,ls_alias_cliente,ls_provincia_div,ls_rag_soc_1,ls_rag_soc_2, & 
					ls_indirizzo,ls_localita,ls_frazione,ls_cap,ls_provincia,ls_des_giro_consegna,ls_cod_prodotto,ls_des_prodotto, &
					ls_num_ord_cliente,ls_rif_interscambio,ls_nota_prodotto, ls_flag_etichetta_apertura, ls_flag_etichetta_produzione, &
					ls_cod_colore_tessuto, ls_des_colore_tessuto, ls_cod_colore_mant, ls_des_colore_mant, ls_cod_deposito_reparto, ls_cod_deposito_origine, &
					ls_flag_tipo_mant, ls_colore_passamaneria,ls_cgibus, ls_lista_reparti_coinvolti, ls_stabilim_prossimo, ls_cod_reparto_prossimo
					
long				ll_num_colli,ll_barcode,ll_num_colli_testata,ll_num_tende_totali,ll_riga,ll_num_colli_riga, ll_altezza_mantovana

integer			li_risposta

decimal			ld_quan_ordine,ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t,ld_quan_etichetta_produzione

datetime			ldt_data_consegna, ldt_data_pronto, ldt_data_pronto_reparto_corrente

string				ls_CSB

boolean			lb_alert = false		//se TRUE allora il reparto successivo appartiene ad uno stabilimento diverso
											//oppure sei all'ultimo reparto e lo stabilimento origine dell'ordine è esterno --->>> visualizza immaginetta alert su etichetta imballo
											//in questa funziona non verrà usato perchè trattasi di etichette di apertura
boolean			lb_RIE = false


guo_functions.uof_get_parametro("RIE", lb_RIE)
if isnull(lb_RIE) then lb_RIE = false


//lettura parametro aziendale Carattere Speciale barcode (specifica Carico ordini con palmare)
select stringa
into   :ls_CSB
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='CSB' and 
		flag_parametro='S';

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in ricerca del parametro CSB in tabella parametri azienda: " + sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 or isnull(ls_CSB) or ls_CSB = "" then	
	ls_CSB = ""
end if
//------------------------------------------------------------------------------------------------------------------------------------

// parametro che identifica l'applicazione per centro gibus
select flag
into   :ls_cgibus
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='CGH';

if sqlca.sqlcode < 0 then
	ls_cgibus = "N"
end if

// per centro gibus c'è un dataobject particolare.
if ls_cgibus = "S"  then
	// funzione disabilitata per chi non è Centro Gibus
	return 0
end if


if not isnull(fl_barcode) then 
	li_risposta = uof_barcode_anno_reg(fl_barcode,&
												  fi_anno_registrazione, &
												  fl_num_registrazione,&
												  fl_prog_riga_ord_ven,&
												  ls_cod_reparto,&
												  ls_errore)
	
	if li_risposta < 0 then
		fs_errore = ls_errore
		return -1
	end if
end if

select flag_etichetta_apertura, 
       flag_etichetta_produzione,
		 quan_etichetta_produzione
into  :ls_flag_etichetta_apertura, 
      :ls_flag_etichetta_produzione,
		:ld_quan_etichetta_produzione
from  anag_reparti
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_reparto = :ls_cod_reparto;
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if
if sqlca.sqlcode = 100 then 
	fs_errore = "Attenzione: il reparto "+ls_cod_reparto+" è inesistente "
	return -1
end if

if (ls_flag_etichetta_apertura = "N" or isnull(ls_flag_etichetta_apertura)) and (ls_flag_etichetta_produzione = "N" or isnull(ls_flag_etichetta_produzione)) then
	// niente da stampare esco...
	return 0
end if


s_cs_xx.parametri.parametro_ds_1 = create datastore


select cod_cliente,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
	 	 cap,   
		 provincia,   
		 cod_giro_consegna,   
		 alias_cliente,   
		 num_colli,
		 num_ord_cliente,
		 rif_interscambio,
		 data_consegna,
		 data_pronto,
		 cod_deposito
 into :ls_cod_cliente,   
		:ls_rag_soc_1_div,   
		:ls_rag_soc_2_div,   
		:ls_indirizzo_div,   
		:ls_localita_div,   
		:ls_frazione_div,   
		:ls_cap_div,   
		:ls_provincia_div,   
		:ls_cod_giro_consegna,   
		:ls_alias_cliente,   
		:ll_num_colli_testata,
		:ls_num_ord_cliente,
		:ls_rif_interscambio,
		:ldt_data_consegna,
		:ldt_data_pronto,
		:ls_cod_deposito_origine
from  tes_ord_ven 
where cod_azienda=:s_cs_xx.cod_azienda
and   anno_registrazione=:fi_anno_registrazione
and   num_registrazione=:fl_num_registrazione;
 
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_rag_soc_1_div) then ls_rag_soc_1_div = ""
if isnull(ls_rag_soc_2_div) then ls_rag_soc_2_div = ""
if isnull(ls_indirizzo_div) then ls_indirizzo_div = ""
if isnull(ls_localita_div) then ls_localita_div = ""
if isnull(ls_frazione_div) then ls_frazione_div = ""
if isnull(ls_cap_div) then ls_cap_div = ""
if isnull(ls_provincia_div) then ls_provincia_div = ""
if isnull(ls_cod_giro_consegna) then ls_cod_giro_consegna = ""
if isnull(ls_alias_cliente) then ls_alias_cliente = ""
if isnull(ll_num_colli_testata) then ll_num_colli_testata =0
if isnull(ls_num_ord_cliente) then ls_num_ord_cliente = ""
if isnull(ls_rif_interscambio) then ls_rif_interscambio = ""

	
select rag_soc_1,
		 rag_soc_2,
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia
into   :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente; 

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if



if isnull(ls_rag_soc_1) then ls_rag_soc_1 =""
if isnull(ls_rag_soc_2) then ls_rag_soc_2 =""
if isnull(ls_indirizzo) then ls_indirizzo =""
if isnull(ls_localita) then ls_localita =""
if isnull(ls_frazione) then ls_frazione =""
if isnull(ls_cap) then ls_cap =""
if isnull(ls_provincia) then ls_provincia =""


select des_giro_consegna
into   :ls_des_giro_consegna
from   tes_giri_consegne
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_giro_consegna=:ls_cod_giro_consegna;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if


//controlla il tipo ordine (tipo stampa) se A= tende da sole, B=sfuso o C=tende tecniche
li_risposta = uof_trova_tipo(fl_barcode,&
									  ls_tipo_stampa,&
									  ls_errore)

if li_risposta < 0 then
	fs_errore = ls_errore
	return -1
end if


if ls_flag_etichetta_apertura = "S" OR ls_flag_etichetta_produzione = "S" then

	s_cs_xx.parametri.parametro_ds_1.dataobject = "d_label_apertura"
	s_cs_xx.parametri.parametro_s_1 = ls_tipo_stampa
	
	ll_riga = s_cs_xx.parametri.parametro_ds_1.insertrow(0)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"num_registrazione",string(fl_num_registrazione))
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"anno_registrazione",string(fi_anno_registrazione))
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"giro_consegna",ls_des_giro_consegna)
	
	/*--------------------------------------------------
	if not isnull(ldt_data_consegna) and year(date(ldt_data_consegna))>1950 then
		//verifica se il deposito (stabilimento) del reparto di stampa e diverso dal deposito di origine dell'ordine (tes_ord_ven.cod_deposito)
		//Se SI allora sposta la data consegna a (-6) GG lavorativi rispetto alla data consegna
		//N.B tenere conto della domenica, del sabato e di eventuali festività in tabella
		
		select cod_deposito
		into :ls_cod_deposito_reparto
		from anag_reparti
		where cod_azienda=:s_cs_xx.cod_azienda and
					cod_reparto=:ls_cod_reparto;
					
		if not isnull(ls_cod_deposito_reparto) and ls_cod_deposito_reparto<>"" and not isnull(ls_cod_deposito_origine) and ls_cod_deposito_origine<>"" and ls_cod_deposito_reparto<> ls_cod_deposito_origine then
			uof_controlla_data_pronto(ldt_data_consegna, ldt_data_pronto)
		end if
	end if
	-------------------------------------------------- */
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"data_pronto",uof_trasforma_data(date(ldt_data_pronto)))
	
	
	if lb_RIE and g_str.isnotempty(ls_rif_interscambio) then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rag_soc",ls_rif_interscambio)
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rag_soc_diversa",ls_rif_interscambio)
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"alias",ls_rif_interscambio)
	else
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rag_soc",ls_rag_soc_1)
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rag_soc_diversa",ls_rag_soc_1_div)
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"alias",ls_alias_cliente)
	end if
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_1",ls_indirizzo)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_2",ls_cap+" "+ls_frazione+" "+ls_localita+" "+ ls_provincia)
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_1_diversa",ls_indirizzo_div)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_2_diversa",ls_cap_div+" "+ls_frazione_div+" "+ls_localita_div+" "+ ls_provincia_div)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rif_1",ls_rif_interscambio)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rif_2",ls_num_ord_cliente)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"cod_reparto",ls_cod_reparto)

	try
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"barcode","*"+ls_CSB + string(fl_barcode) + ls_CSB+"*")
	catch (runtimeerror err)
		
	end try
		
	select cod_prodotto,   
			 quan_ordine,
			 nota_dettaglio
	into   :ls_cod_prodotto,   
			 :ld_quan_ordine,
			 :ls_nota_prodotto
	from   det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:fi_anno_registrazione
	and    num_registrazione=:fl_num_registrazione
	and    prog_riga_ord_ven=:fl_prog_riga_ord_ven;

	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
		
	select dim_x,   
			 dim_y,   
			 dim_z,   
			 dim_t,
			 cod_non_a_magazzino,
			 cod_mant_non_a_magazzino,
			 flag_tipo_mantovana,
			 alt_mantovana,
			 colore_passamaneria
	into   :ld_dim_x,   
			 :ld_dim_y,   
			 :ld_dim_z,   
			 :ld_dim_t,
			 :ls_cod_colore_tessuto,
			 :ls_cod_colore_mant,
			 :ls_flag_tipo_mant,
			 :ll_altezza_mantovana,
			 :ls_colore_passamaneria
	 from  comp_det_ord_ven 
	 where cod_azienda=:s_cs_xx.cod_azienda and
	       anno_registrazione=:fi_anno_registrazione and
			 num_registrazione=:fl_num_registrazione 	 and
			 prog_riga_ord_ven=:fl_prog_riga_ord_ven;

	 if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	 end if
	 
		 
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	select des_colore_tessuto
	into   :ls_des_colore_tessuto
	from   tab_colori_tessuti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_colore_tessuto = :ls_cod_colore_tessuto;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if

	select des_colore_tessuto
	into   :ls_des_colore_mant
	from   tab_colori_tessuti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_colore_tessuto = :ls_cod_colore_mant;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if

	
	select count(*)
	into   :ll_num_tende_totali
	from   det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:fi_anno_registrazione
	and    num_registrazione=:fl_num_registrazione
	and    num_riga_appartenenza=0;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"cod_colore_tessuto", ls_cod_colore_tessuto)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"des_colore_tessuto", ls_des_colore_tessuto)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"cod_colore_mant", ls_cod_colore_mant)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"des_colore_mant", ls_des_colore_mant)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"flag_tipo_mant", ls_flag_tipo_mant)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"colore_passamaneria", ls_colore_passamaneria)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"altezza_mant", string(ll_altezza_mantovana))

	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"data_consegna",uof_trasforma_data(date(ldt_data_consegna)))
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"cod_prodotto_mod",ls_cod_prodotto)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"des_prodotto_mod",ls_des_prodotto)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"nota",ls_nota_prodotto)
	
	if ld_dim_x <> 0 then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_x",string(round(ld_dim_x,2)))
	end if
	
	if ld_dim_y <> 0 then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_y",string(round(ld_dim_y,2)))
	end if
	
	if ld_dim_z <> 0 then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_z",string(round(ld_dim_z,2)))
	end if
	
	if ld_dim_t <> 0 then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_t",string(round(ld_dim_t,2)))
	end if
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"quan_ordine",string(ld_quan_ordine))
	
	
	select sum(num_colli)
	into   :ll_num_colli_riga
	from   sessioni_lavoro
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    progr_det_produzione=:fl_barcode;
	
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"num_colli",string(ll_num_colli_riga))
	
	s_cs_xx.parametri.parametro_ul_1 = fl_barcode
	
	//mettiamo qui il codice per lo stabilimento/reparto successivo
	//visualizza lo stabilimento successivo / stabilimento origine ordine (se ultimo reparto)
	s_cs_xx.parametri.parametro_s_14 = ""
	uof_get_ordine_reparti(	fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ls_cod_reparto, &
									ls_lista_reparti_coinvolti, ls_stabilim_prossimo, ls_cod_reparto_prossimo, lb_alert)
									
									
	// 10-5-2013 Specifica "Calendario Trasferimenti"
	// la data pronto viene presa ora in maniera specifica per il reparto dalle tabelle della produzione.
	setnull(ldt_data_pronto_reparto_corrente)

	select 	data_pronto
	into		:ldt_data_pronto_reparto_corrente
	from 		det_ordini_produzione
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				progr_det_produzione = :fl_barcode;
	if sqlca.sqlcode <> 0 then 
		fs_errore = "Errore in ricerca data pronto del reparto~r~n " + sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ldt_data_pronto_reparto_corrente) then
		//s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"data_consegna", "??/??/??" )
	else
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"data_pronto", string( ldt_data_pronto_reparto_corrente,"dd/mm/yy" ))
	end if
									
	s_cs_xx.parametri.parametro_s_14 = ls_cod_reparto_prossimo
	
	s_cs_xx.parametri.parametro_s_1 = "A"		//solo per ordini tipo A, report 1
	//------------------------------
	

	window_open(w_stampa_etichette_apertura, 0)

	if s_cs_xx.parametri.parametro_i_1 = 0 then 
		fs_errore = s_cs_xx.parametri.parametro_s_2
		return -1
	end if
	
	destroy s_cs_xx.parametri.parametro_ds_1
	
end if

return 0
end function

public function string uof_trasforma_data (date ld_data);// funzione che trasforma la data nel formato stringa
// con la descrizione del giorno.

integer li_giorno
string  ls_string

li_giorno = daynumber(ld_data)

choose case li_giorno
	case 1 
		ls_string = "DOM"
	case 2 
		ls_string = "LUN"
	case 3 
		ls_string = "MAR"
	case 4 
		ls_string = "MER"
	case 5 
		ls_string = "GIO"
	case 6 
		ls_string = "VEN"
	case 7 
		ls_string = "SAB"
	case else 
		ls_string = " "
		
		
end choose

ls_string += " " + string(ld_data, "dd/mm/yy")

return ls_string
end function

public function integer uof_stato_prod_det_ord_ven_reparto (long al_anno_ord_ven, long al_num_ord_ven, long al_riga_ord_ven, string as_cod_reparto, ref string as_messaggio);long ll_progr_det_produzione, ll_return, ll_aperte, ll_parziali, ll_chiuse, ll_totale


ll_aperte = 0

ll_parziali = 0

ll_chiuse = 0

ll_totale = 0

declare fasi cursor for
select
	progr_det_produzione
from
	det_ordini_produzione
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :al_anno_ord_ven and
	num_registrazione = :al_num_ord_ven and
	prog_riga_ord_ven = :al_riga_ord_ven and
	cod_reparto = :as_cod_reparto
order by
	progr_det_produzione ASC;

open fasi;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in open cursore fasi:~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch
		fasi
	into
		:ll_progr_det_produzione;
		
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in fetch cursore fasi:~n" + sqlca.sqlerrtext
		close fasi;
		return -1
	elseif sqlca.sqlcode = 100 then
		close fasi;
		exit
	end if
	
	ll_return = uof_stato_prod_fase(ll_progr_det_produzione,as_messaggio)
	
	choose case ll_return
		case -1
			as_messaggio = "Errore in controllo stato produzione fase " + string(ll_progr_det_produzione) + ":~n" + as_messaggio
			close fasi;
			return -1
		case 0
			ll_aperte ++
		case 1
			ll_chiuse ++
		case 2
			ll_parziali ++
		case else
			as_messaggio = "Errore in controllo stato produzione fase " + string(ll_progr_det_produzione) + ":~nValore non previsto"
			return -1
	end choose
	
	ll_totale ++
	
loop

if ll_totale = 0 then
	as_messaggio = "Non esiste alcuna fase in produzione per la riga d'ordine indicata"
	close fasi;
	return -100
end if

if ll_chiuse = 0 and ll_parziali = 0 then
	as_messaggio = "Niente"
	close fasi;
	return 0
else
	if ll_chiuse = ll_totale then
		as_messaggio = "Tutto"
		close fasi;
		return 1
	else
		as_messaggio = "Parziale"
		close fasi;
		return 2
	end if
end if

close fasi;

end function

public function integer uof_stato_prod_det_ord_ven_reparti (long al_anno_ord_ven, long al_num_ord_ven, long al_riga_ord_ven, ref string as_cod_reparto[], ref long al_stato_reparto[], ref string as_messaggio);long ll_index, ll_return, ll_aperti=0, ll_parziali=0, ll_chiusi=0, ll_totale=0, ll_num_sequenza, ll_ordinamento
string ls_cod_reparto
string ls_reparti_distint[] // contiene l'array dei reparti utilizzati in modo da non avere doppioni

declare fasi_rep cursor for
select distinct det_ordini_produzione.cod_reparto, distinta.num_ordinamento_dt, varianti_det_ord_ven_prod.num_sequenza
from det_ordini_produzione
left outer join varianti_det_ord_ven_prod on
	varianti_det_ord_ven_prod.cod_azienda = det_ordini_produzione.cod_azienda and
	varianti_det_ord_ven_prod.anno_registrazione = det_ordini_produzione.anno_registrazione and
	varianti_det_ord_ven_prod.num_registrazione = det_ordini_produzione.num_registrazione and
	varianti_det_ord_ven_prod.prog_riga_ord_ven = det_ordini_produzione.prog_riga_ord_ven and
	varianti_det_ord_ven_prod.cod_reparto = det_ordini_produzione.cod_reparto
left join distinta on
	distinta.cod_azienda = varianti_det_ord_ven_prod.cod_azienda and
	distinta.cod_prodotto_padre = varianti_det_ord_ven_prod.cod_prodotto_padre and
	distinta.num_sequenza = varianti_det_ord_ven_prod.num_sequenza and
	distinta.cod_prodotto_figlio = varianti_det_ord_ven_prod.cod_prodotto_figlio and
	distinta.cod_versione = varianti_det_ord_ven_prod.cod_versione and
	distinta.cod_versione_figlio = varianti_det_ord_ven_prod.cod_versione_figlio
where det_ordini_produzione.cod_azienda = :s_cs_xx.cod_azienda and
		det_ordini_produzione.anno_registrazione = :al_anno_ord_ven and
		det_ordini_produzione.num_registrazione = :al_num_ord_ven and
		det_ordini_produzione.prog_riga_ord_ven = :al_riga_ord_ven
order by distinta.num_ordinamento_dt, varianti_det_ord_ven_prod.num_sequenza;
// -----------------------------------------------------

open fasi_rep;
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in OPEN cursore fasi_rep~r~n" + sqlca.sqlerrtext
	return -1
end if


ll_index = 0

do while true
	//fetch fasi_rep into :ls_cod_reparto;
	fetch fasi_rep into :ls_cod_reparto , :ll_ordinamento, :ll_num_sequenza;
	if sqlca.sqlcode = 100 then exit 
	if sqlca.sqlcode <> 0 then
		as_messaggio = "Errore in FETCH cursore fasi_rep~r~n" + sqlca.sqlerrtext
		close fasi_rep;
		return -1
	end if
	
	// Stefanop 21/06/2013: agginugo controllo perchè in alcuni casi la select ritorna
	// due codici di reparti uguali (perchè cambia il num_sequenza e l'order by fa salatare
	// il distinct)
	if guo_functions.uof_in_array(ls_cod_reparto, ls_reparti_distint) then
		// Se già presente allora lo salto
		continue
	else
		// altrimenti lo aggiungo alla cache
		ls_reparti_distint[upperbound(ls_reparti_distint) + 1] = ls_cod_reparto
	end if
	
	ll_return = uof_stato_prod_det_ord_ven_reparto(al_anno_ord_ven, al_num_ord_ven, al_riga_ord_ven, ls_cod_reparto, ref as_messaggio)	

	ll_index ++
	
	as_cod_reparto[ll_index] = ls_cod_reparto
	ll_totale ++
	choose case ll_return
		case -1
			close fasi_rep;
			return -1
		case -100
			// nessuna fase presente
			al_stato_reparto[ll_index] = ll_return
			
		case 0
			// tutto da iniziare
			al_stato_reparto[ll_index] = ll_return
			ll_aperti ++
		case 1
			// reparto completamente chiuso
			al_stato_reparto[ll_index] = ll_return
			ll_chiusi ++
		case 2
			// parzialmente chiuso
			al_stato_reparto[ll_index] = ll_return
			ll_parziali ++
	end choose
loop

if ll_chiusi = 0 and ll_parziali = 0 then
	as_messaggio = "Niente"
	close fasi_rep;
	return 0
else
	if ll_chiusi = ll_totale then
		as_messaggio = "Tutto"
		close fasi_rep;
		return 1
	else
		as_messaggio = "Parziale"
		close fasi_rep;
		return 2
	end if
end if

close fasi_rep;
return 0
end function

public function integer uof_etichette_tecniche (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, ref string fs_errore);// Funzione che esegue la stampa delle etichette di aprtura per PROGETTOTENDA SPA
//	vedi specifica requisiti "Modifica_etichette" rev. 1 del 7/3/2006
//	richiesta da Beatrice
//
// nome: uof_etichette_apertura
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 20-03-2006
//		Autore Enrico Menegotto

string ls_errore,ls_tipo_stampa,ls_cod_reparto,ls_cod_cliente,ls_rag_soc_1_div,ls_rag_soc_2_div,ls_indirizzo_div,ls_localita_div, &
		 ls_frazione_div,ls_cap_div,ls_cod_giro_consegna,ls_alias_cliente,ls_provincia_div,ls_rag_soc_1,ls_rag_soc_2, & 
		 ls_indirizzo,ls_localita,ls_frazione,ls_cap,ls_provincia,ls_des_giro_consegna,ls_cod_prodotto,ls_des_prodotto, &
		 ls_num_ord_cliente,ls_rif_interscambio,ls_nota_prodotto, ls_flag_etichetta_produzione, &
		 ls_cod_colore_tessuto, ls_des_colore_tessuto, ls_cod_colore_mant, ls_des_colore_mant, &
		 ls_flag_tipo_mant, ls_colore_passamaneria
long ll_num_colli,ll_barcode,ll_num_colli_testata,ll_num_tende_totali,ll_riga,ll_num_colli_riga, ll_altezza_mantovana, ll_prog_riga_ord_ven
integer li_risposta
decimal ld_quan_ordine,ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t,ld_quan_etichetta_produzione
datetime ldt_data_consegna, ldt_data_pronto

if not isnull(fl_barcode) then 
	ll_prog_riga_ord_ven = 9999
	li_risposta = uof_barcode_anno_reg(fl_barcode, ref fi_anno_registrazione, ref fl_num_registrazione, ref ll_prog_riga_ord_ven, ref ls_cod_reparto, ref ls_errore)
	
	if li_risposta < 0 then
		fs_errore = ls_errore
		return -1
	end if
end if

select flag_etichetta_produzione,
		 quan_etichetta_produzione
into  :ls_flag_etichetta_produzione,
		:ld_quan_etichetta_produzione
from  anag_reparti
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_reparto = :ls_cod_reparto;
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if
if sqlca.sqlcode = 100 then 
	fs_errore = "Attenzione: il reparto "+ls_cod_reparto+" è inesistente "
	return -1
end if

if ls_flag_etichetta_produzione = "N" or isnull(ls_flag_etichetta_produzione) then
	// niente da stampare esco...
	return 0
end if


s_cs_xx.parametri.parametro_ds_1 = create datastore


select cod_cliente,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
	 	 cap,   
		 provincia,   
		 cod_giro_consegna,   
		 alias_cliente,   
		 num_colli,
		 num_ord_cliente,
		 rif_interscambio,
		 data_consegna,
		 data_pronto
 into :ls_cod_cliente,   
		:ls_rag_soc_1_div,   
		:ls_rag_soc_2_div,   
		:ls_indirizzo_div,   
		:ls_localita_div,   
		:ls_frazione_div,   
		:ls_cap_div,   
		:ls_provincia_div,   
		:ls_cod_giro_consegna,   
		:ls_alias_cliente,   
		:ll_num_colli_testata,
		:ls_num_ord_cliente,
		:ls_rif_interscambio,
		:ldt_data_consegna,
		:ldt_data_pronto
from  tes_ord_ven 
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:fi_anno_registrazione and
		num_registrazione=:fl_num_registrazione;
 
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore in ricerca testata ordine di vendita~r~n " + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_rag_soc_1_div) then ls_rag_soc_1_div = ""
if isnull(ls_rag_soc_2_div) then ls_rag_soc_2_div = ""
if isnull(ls_indirizzo_div) then ls_indirizzo_div = ""
if isnull(ls_localita_div) then ls_localita_div = ""
if isnull(ls_frazione_div) then ls_frazione_div = ""
if isnull(ls_cap_div) then ls_cap_div = ""
if isnull(ls_provincia_div) then ls_provincia_div = ""
if isnull(ls_cod_giro_consegna) then ls_cod_giro_consegna = ""
if isnull(ls_alias_cliente) then ls_alias_cliente = ""
if isnull(ll_num_colli_testata) then ll_num_colli_testata =0
if isnull(ls_num_ord_cliente) then ls_num_ord_cliente = ""
if isnull(ls_rif_interscambio) then ls_rif_interscambio = ""

	
select rag_soc_1,
		 rag_soc_2,
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia
into   :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente; 

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if



if isnull(ls_rag_soc_1) then ls_rag_soc_1 =""
if isnull(ls_rag_soc_2) then ls_rag_soc_2 =""
if isnull(ls_indirizzo) then ls_indirizzo =""
if isnull(ls_localita) then ls_localita =""
if isnull(ls_frazione) then ls_frazione =""
if isnull(ls_cap) then ls_cap =""
if isnull(ls_provincia) then ls_provincia =""


select des_giro_consegna
into   :ls_des_giro_consegna
from   tes_giri_consegne
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_giro_consegna=:ls_cod_giro_consegna;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if


//controlla il tipo ordine (tipo stampa) se A= tende da sole, B=sfuso o C=tende tecniche
li_risposta = uof_trova_tipo(fl_barcode,&
									  ls_tipo_stampa,&
									  ls_errore)

if li_risposta < 0 then
	fs_errore = ls_errore
	return -1
end if


if ls_flag_etichetta_produzione = "S" then

	s_cs_xx.parametri.parametro_ds_1.dataobject = "d_label_produzione_tecniche"
	s_cs_xx.parametri.parametro_s_1 = ls_tipo_stampa
	s_cs_xx.parametri.parametro_ul_1 = fl_barcode
	
	s_cs_xx.parametri.parametro_ds_1.reset()
	
	ll_riga = s_cs_xx.parametri.parametro_ds_1.insertrow(0)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"num_registrazione",string(fl_num_registrazione))
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"anno_registrazione",string(fi_anno_registrazione))
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"giro_consegna",ls_des_giro_consegna)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"data_pronto",uof_trasforma_data(date(ldt_data_pronto)))
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rag_soc",ls_rag_soc_1)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_1",ls_indirizzo)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_2",ls_cap+" "+ls_frazione+" "+ls_localita+" "+ ls_provincia)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"alias",ls_alias_cliente)
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rag_soc_diversa",ls_rag_soc_1_div)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_1_diversa",ls_indirizzo_div)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_2_diversa",ls_cap_div+" "+ls_frazione_div+" "+ls_localita_div+" "+ ls_provincia_div)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rif_1",ls_rif_interscambio)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rif_2",ls_num_ord_cliente)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"cod_reparto",ls_cod_reparto)
	
	// aggiunto su richiesta di Valerio 27/02/2008
	// se il riferimento interscambio è vuoto, carico il riferimento vs ordine
	if isnull(ls_rif_interscambio) or len(ls_rif_interscambio) < 1 then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rif_1",ls_num_ord_cliente)
	end if

	window_open(w_stampa_etichette_tecniche, 0)

	if s_cs_xx.parametri.parametro_i_1 = 0 then 
		fs_errore = s_cs_xx.parametri.parametro_s_2
		return -1
	end if

	destroy s_cs_xx.parametri.parametro_ds_1

end if

return 0
end function

public function integer uof_etichette_produzione_cg (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_errore);// Funzione che esegue la stampa delle etichette di aprtura per PROGETTOTENDA SPA
//	vedi specifica requisiti "Modifica_etichette" rev. 1 del 7/3/2006
//	richiesta da Beatrice
//
// nome: uof_etichette_apertura
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 20-03-2006
//		Autore Enrico Menegotto

string ls_errore,ls_tipo_stampa,ls_cod_reparto,ls_cod_cliente,ls_rag_soc_1_div,ls_rag_soc_2_div,ls_indirizzo_div,ls_localita_div, &
		 ls_frazione_div,ls_cap_div,ls_cod_giro_consegna,ls_alias_cliente,ls_provincia_div,ls_rag_soc_1,ls_rag_soc_2, & 
		 ls_indirizzo,ls_localita,ls_frazione,ls_cap,ls_provincia,ls_des_giro_consegna,ls_cod_prodotto,ls_des_prodotto, &
		 ls_num_ord_cliente,ls_rif_interscambio,ls_nota_prodotto, ls_flag_etichetta_produzione, &
		 ls_cod_colore_tessuto, ls_des_colore_tessuto, ls_cod_colore_mant, ls_des_colore_mant, &
		 ls_flag_tipo_mant, ls_colore_passamaneria, ls_cgibus, ls_nota_dettaglio
long ll_num_colli,ll_barcode,ll_num_colli_testata,ll_num_tende_totali,ll_riga,ll_num_colli_riga, ll_altezza_mantovana
integer li_risposta
decimal ld_quan_ordine,ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t,ld_quan_etichetta_produzione
datetime ldt_data_consegna, ldt_data_pronto


// parametro che identifica l'applicazione per centro gibus
select flag
into   :ls_cgibus
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='CGH';

if sqlca.sqlcode < 0 then
	ls_cgibus = "N"
end if

// per centro gibus c'è un dataobject particolare.
if ls_cgibus = "N" or isnull(ls_cgibus) then
	// funzione disabilitata per chi non è Centro Gibus
	return 0
end if

if not isnull(fl_barcode) then 
	li_risposta = uof_barcode_anno_reg(fl_barcode,&
												  fi_anno_registrazione, &
												  fl_num_registrazione,&
												  fl_prog_riga_ord_ven,&
												  ls_cod_reparto,&
												  ls_errore)
	
	if li_risposta < 0 then
		fs_errore = ls_errore
		return -1
	end if
end if

select flag_etichetta_produzione,
		 quan_etichetta_produzione
into  :ls_flag_etichetta_produzione,
		:ld_quan_etichetta_produzione
from  anag_reparti
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_reparto = :ls_cod_reparto;
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if
if sqlca.sqlcode = 100 then 
	fs_errore = "Attenzione: il reparto "+ls_cod_reparto+" è inesistente "
	return -1
end if

if ls_flag_etichetta_produzione = "N" or isnull(ls_flag_etichetta_produzione) then
	// niente da stampare esco...
	return 0
end if


s_cs_xx.parametri.parametro_ds_1 = create datastore


select cod_cliente,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
	 	 cap,   
		 provincia,   
		 cod_giro_consegna,   
		 alias_cliente,   
		 num_colli,
		 num_ord_cliente,
		 rif_interscambio,
		 data_consegna,
		 data_pronto
 into :ls_cod_cliente,   
		:ls_rag_soc_1_div,   
		:ls_rag_soc_2_div,   
		:ls_indirizzo_div,   
		:ls_localita_div,   
		:ls_frazione_div,   
		:ls_cap_div,   
		:ls_provincia_div,   
		:ls_cod_giro_consegna,   
		:ls_alias_cliente,   
		:ll_num_colli_testata,
		:ls_num_ord_cliente,
		:ls_rif_interscambio,
		:ldt_data_consegna,
		:ldt_data_pronto
from  tes_ord_ven 
where cod_azienda=:s_cs_xx.cod_azienda
and   anno_registrazione=:fi_anno_registrazione
and   num_registrazione=:fl_num_registrazione;
 
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_rag_soc_1_div) then ls_rag_soc_1_div = ""
if isnull(ls_rag_soc_2_div) then ls_rag_soc_2_div = ""
if isnull(ls_indirizzo_div) then ls_indirizzo_div = ""
if isnull(ls_localita_div) then ls_localita_div = ""
if isnull(ls_frazione_div) then ls_frazione_div = ""
if isnull(ls_cap_div) then ls_cap_div = ""
if isnull(ls_provincia_div) then ls_provincia_div = ""
if isnull(ls_cod_giro_consegna) then ls_cod_giro_consegna = ""
if isnull(ls_alias_cliente) then ls_alias_cliente = ""
if isnull(ll_num_colli_testata) then ll_num_colli_testata =0
if isnull(ls_num_ord_cliente) then ls_num_ord_cliente = ""
if isnull(ls_rif_interscambio) then ls_rif_interscambio = ""

	
select rag_soc_1,
		 rag_soc_2,
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia
into   :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente; 

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if



if isnull(ls_rag_soc_1) then ls_rag_soc_1 =""
if isnull(ls_rag_soc_2) then ls_rag_soc_2 =""
if isnull(ls_indirizzo) then ls_indirizzo =""
if isnull(ls_localita) then ls_localita =""
if isnull(ls_frazione) then ls_frazione =""
if isnull(ls_cap) then ls_cap =""
if isnull(ls_provincia) then ls_provincia =""


select des_giro_consegna
into   :ls_des_giro_consegna
from   tes_giri_consegne
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_giro_consegna=:ls_cod_giro_consegna;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if


//controlla il tipo ordine (tipo stampa) se A= tende da sole, B=sfuso o C=tende tecniche
li_risposta = uof_trova_tipo(fl_barcode,&
									  ls_tipo_stampa,&
									  ls_errore)

if li_risposta < 0 then
	fs_errore = ls_errore
	return -1
end if


if ls_flag_etichetta_produzione = "S" then

	s_cs_xx.parametri.parametro_ds_1.dataobject = "d_label_apertura"
	s_cs_xx.parametri.parametro_s_1 = ls_tipo_stampa
	
	ll_riga = s_cs_xx.parametri.parametro_ds_1.insertrow(0)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"num_registrazione",string(fl_num_registrazione))
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"anno_registrazione",string(fi_anno_registrazione))
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"giro_consegna",ls_des_giro_consegna)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"data_pronto",uof_trasforma_data(date(ldt_data_pronto)))
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rag_soc",ls_rag_soc_1)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_1",ls_indirizzo)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_2",ls_cap+" "+ls_frazione+" "+ls_localita+" "+ ls_provincia)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"alias",ls_alias_cliente)
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rag_soc_diversa",ls_rag_soc_1_div)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_1_diversa",ls_indirizzo_div)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_2_diversa",ls_cap_div+" "+ls_frazione_div+" "+ls_localita_div+" "+ ls_provincia_div)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rif_1",ls_rif_interscambio)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rif_2",ls_num_ord_cliente)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"cod_reparto",ls_cod_reparto)
	
	// aggiunto su richiesta di Valerio 27/02/2008
	// se il riferimento interscambio è vuoto, carico il riferimento vs ordine
	if isnull(ls_rif_interscambio) or len(ls_rif_interscambio) < 1 then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rif_1",ls_num_ord_cliente)
	end if
		
	select cod_prodotto,   
			 quan_ordine,
			 nota_dettaglio,
			 nota_prodotto
	into   :ls_cod_prodotto,   
			 :ld_quan_ordine,
			 :ls_nota_dettaglio,
			 :ls_nota_prodotto
	from   det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:fi_anno_registrazione
	and    num_registrazione=:fl_num_registrazione
	and    prog_riga_ord_ven=:fl_prog_riga_ord_ven;

	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
		
	select dim_x,   
			 dim_y,   
			 dim_z,   
			 dim_t,
			 cod_non_a_magazzino,
			 cod_mant_non_a_magazzino,
			 flag_tipo_mantovana,
			 alt_mantovana,
			 colore_passamaneria
	into   :ld_dim_x,   
			 :ld_dim_y,   
			 :ld_dim_z,   
			 :ld_dim_t,
			 :ls_cod_colore_tessuto,
			 :ls_cod_colore_mant,
			 :ls_flag_tipo_mant,
			 :ll_altezza_mantovana,
			 :ls_colore_passamaneria
	 from  comp_det_ord_ven 
	 where cod_azienda=:s_cs_xx.cod_azienda and
	       anno_registrazione=:fi_anno_registrazione and
			 num_registrazione=:fl_num_registrazione 	 and
			 prog_riga_ord_ven=:fl_prog_riga_ord_ven;

	 if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	 end if
	 
		 
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	select des_colore_tessuto
	into   :ls_des_colore_tessuto
	from   tab_colori_tessuti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_colore_tessuto = :ls_cod_colore_tessuto;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if

	select des_colore_tessuto
	into   :ls_des_colore_mant
	from   tab_colori_tessuti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_colore_tessuto = :ls_cod_colore_mant;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if

	
	select count(*)
	into   :ll_num_tende_totali
	from   det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:fi_anno_registrazione
	and    num_registrazione=:fl_num_registrazione
	and    num_riga_appartenenza=0;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"cod_colore_tessuto", ls_cod_colore_tessuto)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"des_colore_tessuto", ls_des_colore_tessuto)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"cod_colore_mant", ls_cod_colore_mant)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"des_colore_mant", ls_des_colore_mant)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"flag_tipo_mant", ls_flag_tipo_mant)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"colore_passamaneria", ls_colore_passamaneria)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"altezza_mant", string(ll_altezza_mantovana))

	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"data_consegna",uof_trasforma_data(date(ldt_data_consegna)))
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"cod_prodotto_mod",ls_cod_prodotto)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"des_prodotto_mod",ls_des_prodotto)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"nota",ls_nota_prodotto)
	
	if ld_dim_x <> 0 then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_x",string(round(ld_dim_x,2)))
	end if
	
	if ld_dim_y <> 0 then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_y",string(round(ld_dim_y,2)))
	end if
	
	if ld_dim_z <> 0 then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_z",string(round(ld_dim_z,2)))
	end if
	
	if ld_dim_t <> 0 then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_t",string(round(ld_dim_t,2)))
	end if
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"quan_ordine",string(ld_quan_ordine))
	
	
	select sum(num_colli)
	into   :ll_num_colli_riga
	from   sessioni_lavoro
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    progr_det_produzione=:fl_barcode;
	
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"num_colli",string(ll_num_colli_riga))
	
	s_cs_xx.parametri.parametro_ul_1 = fl_barcode
	
	window_open(w_stampa_etichette_produzione_cg, 0)
	
	if s_cs_xx.parametri.parametro_i_1 = 0 then 
		fs_errore = s_cs_xx.parametri.parametro_s_2
		return -1
	end if
	
	destroy s_cs_xx.parametri.parametro_ds_1
	
end if

return 0
end function

public function integer uof_etichette_cg (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_errore);// Funzione che esegue la stampa delle etichette di produzione per CENTRO GIBUS
// nome: uof_etichette
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 26-02-2008
//		Autore Enrico Menegotto

long   li_risposta
string ls_tipo_stampa,ls_errore, ls_cod_reparto

if not isnull(fl_barcode) then 
	li_risposta = uof_barcode_anno_reg(fl_barcode,&
												  fi_anno_registrazione, &
												  fl_num_registrazione,&
												  fl_prog_riga_ord_ven,&
												  ls_cod_reparto,&
												  ls_errore)
	
	if li_risposta < 0 then
		fs_errore = ls_errore
		return -1
	end if
end if

//controlla il tipo ordine (tipo stampa) se A= tende da sole, B=sfuso o C=tende tecniche
li_risposta = uof_trova_tipo(fl_barcode,&
									  ls_tipo_stampa,&
									  ls_errore)

if li_risposta < 0 then
	fs_errore = ls_errore
	return -1
end if


choose case ls_tipo_stampa
	case 'A'  // tende da sole
		s_cs_xx.parametri.parametro_s_11 = "RISTAMPA"
		uof_etichette_produzione_cg(fl_barcode, fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ls_errore)
		s_cs_xx.parametri.parametro_s_11 = ""
	case 'B'  // sfuso
		uof_etichette_tecniche(fl_barcode, 0, 0, ls_errore)
	case 'C' // tende tecniche
		uof_etichette_tecniche(fl_barcode, 0, 0, ls_errore)
end choose

return 0
end function

public function integer uof_barcode_anno_reg_sfuso (ref long fl_barcode, ref integer fi_anno_registrazione, ref long fl_num_registrazione, ref string fs_errore);/**
 * Stefano
 * 17/05/2010
 *
 * Modificata per la specifica Etichette Sfuso CGibus
 **/
 
integer li_anno_registrazione
long ll_num_registrazione,ll_prog_riga_ord_ven,ll_barcode,ll_barcode_2, ll_count
string ls_cod_reparto

if not isnull(fl_barcode) then

	select anno_registrazione, num_registrazione
	into :li_anno_registrazione, :ll_num_registrazione
	from det_ordini_produzione
	where 
		cod_azienda=:s_cs_xx.cod_azienda and
		progr_det_produzione =:fl_barcode;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	else
		
		if sqlca.sqlcode = 100 then
			select anno_registrazione, num_registrazione
			into :li_anno_registrazione, :ll_num_registrazione
			from tes_ordini_produzione
			where 
				cod_azienda=:s_cs_xx.cod_azienda and
				progr_tes_produzione =:fl_barcode;	
		
			if sqlca.sqlcode < 0 then 
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if		
		
			if sqlca.sqlcode = 100 then
				fs_errore = "Non è stato trovato alcun lancio di produzione con questo codice. Verificare."
				return -1
			end if	
			
			fi_anno_registrazione = li_anno_registrazione
			fl_num_registrazione = ll_num_registrazione
			
		else
			fi_anno_registrazione = li_anno_registrazione
			fl_num_registrazione = ll_num_registrazione
		end if
		
		return 0
	end if
else
	
	select count(cod_azienda)
	into :ll_count
	from det_ordini_produzione
	where
		cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:fi_anno_registrazione and
		num_registrazione=:fl_num_registrazione;
					 
	choose case sqlca.sqlcode
			
		case is < 0
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
			
		case 0
			if isnull(ll_count) or ll_count < 1 then
				fs_errore = "Attenzione! Non è stato trovato alcun ordine con questo codice a barre. Verificare di aver letto il codice a barre corretto presente nella stampa di produzione in basso a sinistra o di aver inserito correttamente i dati in modalità manuale."
				return -1
			else
				return 0
			end if
	
		case 100
			select count(cod_azienda)
			into :ll_count	
			from tes_ordini_produzione
			where 
				cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fi_anno_registrazione and
				num_registrazione=:fl_num_registrazione;
			
			if sqlca.sqlcode < 0 then 
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				return -1
			end if
			
			if sqlca.sqlcode = 100 then
				fs_errore = "Attenzione! Non è stato trovato alcun ordine con questo codice a barre. Verificare di aver letto il codice a barre corretto presente nella stampa di produzione in basso a sinistra o di aver inserito correttamente i dati in modalità manuale."
				return -1
			end if

			if sqlca.sqlcode = 0 then
				if isnull(ll_count) or ll_count < 1 then
					return -1
				else
					return 0
				end if
			end if
		
	end choose
	
	
end if


end function

public function integer uof_reimposta_colli (long fl_barcode, ref string fs_errore);// Funzione che esegue il reset-reimposta colli di un ordine/barcode



integer				li_risposta, li_anno_reg
long					ll_num_reg, ll_prog_riga, ll_num_colli_new, ll_count, ll_ret_produzione, ll_max_sessione, ll_colli_sessione, ll_num_colli
string					ls_cod_reparto, ls_tipo_stampa, ls_ordine,ls_flag_fine_fase, ls_posizione
datetime				ldt_fine_prep

//verifico prima se per questo barcode c'è già qualcosa --------------------------------------
select count(*)
into :ll_count
from tab_ord_ven_colli
where 	cod_azienda=:s_cs_xx.cod_azienda and
			barcode=:fl_barcode;
			
if sqlca.sqlcode<0 then
	fs_errore = "Errore in conteggio colli esistenti per il barcode "+string(fl_barcode)+" - "+sqlca.sqlerrtext
	return -1
end if

//if isnull(ll_count) or ll_count=0 then
//	fs_errore = "Per questo barcode non esistono colli prodotti. Impossibile re-impostare!"
//	return -1
//end if

//leggo le info sull'ordine associato al barcode -----------------------------------
li_risposta = uof_barcode_anno_reg(fl_barcode,&
												  li_anno_reg, &
												  ll_num_reg,&
												  ll_prog_riga,&
												  ls_cod_reparto,&
												  fs_errore)
	
if li_risposta < 0 then
	//in fs_errore c'è il messaggio di errore
	return -1
end if
ls_ordine = string(li_anno_reg) + "/" + string(ll_num_reg)

//---------------------------------------------------------------------
//verifica se le fasi sono chiuse, controllo diverso a seconda del tipo ordine
//solo a fase chiusa è possibile re-impostare i colli ...
li_risposta = uof_trova_tipo(fl_barcode,&
									  ls_tipo_stampa,&
									  fs_errore)
if li_risposta < 0 then
	//in fs_errore il messaggio di errore
	return -1
end if

choose case ls_tipo_stampa
	case "A"
		select flag_fine_fase, posizione
		into   :ls_flag_fine_fase, :ls_posizione
		from   det_ordini_produzione
		where	cod_azienda=:s_cs_xx.cod_azienda and
				progr_det_produzione=:fl_barcode;
		//--
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		//--
		if sqlca.sqlcode = 100 then
			fs_errore = "Attenzione! La produzione per questo ordine non è ancora stata lanciata. Verificare e rilanciare la produzione."
			return -1
		end if
		//--
		if ls_flag_fine_fase='S' then
			//la fase risulta già chiusa    OK
		else
			fs_errore = "La fase per questo barcode risulta ancora aperta. Impossibile re-impostare i colli!"
			return -1
		end if			
	
	
		
	case "B", "C"
		select fine_preparazione, posizione
		into   :ldt_fine_prep, :ls_posizione
		from   tes_ordini_produzione
		where  cod_azienda=:s_cs_xx.cod_azienda and
			    progr_tes_produzione=:fl_barcode;
		
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		if sqlca.sqlcode = 100 then
			fs_errore = "Attenzione! La produzione per questo ordine non è ancora stata lanciata. Verificare e rilanciare la produzione."
			return -1
		end if
				
		if not isnull(ldt_fine_prep) and year(date(ldt_fine_prep))>1950 then
			//La fase risulta già chiusa    OK
		else
			fs_errore = "La fase per questo barcode risulta ancora aperta. Impossibile re-impostare i colli!"
			return -1
		end if
		
end choose
//--------------------------------------------------------------------


//apri una window di re-impostazione e metto in ll_num_colli_new ------------------------------------------------
s_cs_xx.parametri.parametro_s_5 = string(fl_barcode)

if isnull(ls_posizione) then ls_posizione=""
s_cs_xx.parametri.parametro_s_12 = ls_posizione

window_open(w_reimposta_colli, 0)

ls_posizione = s_cs_xx.parametri.parametro_s_12

//in s_cs_xx.parametri.parametro_s_5 c'è la stringa numerica dei colli reimpostati

if s_cs_xx.parametri.parametro_s_5<>"" and not isnull(s_cs_xx.parametri.parametro_s_5) then
	ll_num_colli_new = long(s_cs_xx.parametri.parametro_s_5)
	
//	//ulteriore controllo per scrupolo
//	if ll_num_colli_new>0 then
//		//continua .....
//	else
//		fs_errore = "Numero colli da impostare NULLO: Operazione annullata!"
//		return -1
//	end if
else
	//probabilmente hai annullato tutto: esci
	fs_errore = "Operazione annullata!"
	return -1
end if

//pulizia variabili globali
setnull(s_cs_xx.parametri.parametro_s_5)
setnull(s_cs_xx.parametri.parametro_s_12)

//aggiorno la posizione
choose case ls_tipo_stampa
	case "A"
		update det_ordini_produzione
		set posizione=:ls_posizione
		where	cod_azienda=:s_cs_xx.cod_azienda and
					progr_det_produzione=:fl_barcode;
		
	case "B", "C"
		update tes_ordini_produzione
		set posizione=:ls_posizione
		where	cod_azienda=:s_cs_xx.cod_azienda and
					progr_tes_produzione=:fl_barcode;
					
end choose

//*******************************************
//in ll_count i colli pre-esistenti
//in ll_num_colli_new quelli da impostare
//*******************************************

//re-imposto i colli in tab_ord_ven_colli (cancello e re-inserisco)
//reset e re-impostazione con ll_num_colli (fb_reimposta_colli=TRUE)
if uof_crea_barcode_colli(fl_barcode, ll_num_colli_new, true, fs_errore) < 0 then
	//in fs_errore l'errore
	return -1
end if


//allineo la sessioni_lavoro se barcode di dettaglio -----------------------------------------------
//e successivamente ri-allineo pure i num_colli in tes_ord_ven in base al sum di tutte le sessioni di lavoro collegate
if ll_prog_riga>0 then
	//barcode di dettaglio
	//azzero i colli presenti nelle sessioni lavoro del barcode -----------------------------
	update sessioni_lavoro
	set num_colli = 0
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				progr_det_produzione=:fl_barcode;
				
//azzero solo i colli relativi al barcode scansionato e non per i barcode della stessa riga !!!!!!!!!!!!!!!!!
//										in (select progr_det_produzione
//												from det_ordini_produzione
//												where 	cod_azienda = :s_cs_xx.cod_azienda and
//															anno_registrazione = :li_anno_reg and
//															num_registrazione = :ll_num_reg and
//															prog_riga_ord_ven=:ll_prog_riga);

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in azzeramento colli in sessione lavoro (ordine "+string(li_anno_reg)+"/"+string(ll_num_reg)+"/"+string(ll_prog_riga)+") : " + sqlca.sqlerrtext
		return -1
	end if
	//------------------------------------------------------------------------------------------------------------
	
	//leggo il max in sessioni lavoro relativo al barcode per recuperare la PK e fare un update SUCCESSIVO dei colli voluti
	select max(progr_sessione)
	into :ll_max_sessione
	from sessioni_lavoro
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				progr_det_produzione=:fl_barcode;
				
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in lettura max progr_sessione in tabella sessioni_lavoro: "+sqlca.sqlerrtext
		return -1
	end if
	
	//aggiorno una riga di sessioni lavoro (gli altri, se ce ne sono, resteranno a num colli pari a 0) -----------------------------------------
	update sessioni_lavoro
	set num_colli = :ll_num_colli_new
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				progr_det_produzione=:fl_barcode and
				progr_sessione = :ll_max_sessione;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in aggiornamento colli in sessioni_lavoro ordine "+ls_ordine+" "+sqlca.sqlerrtext
		return -1
	end if
	//---------------------------------------------------------------------------------------------------------------------------------------------------------------
	//fine aggiornamento sessioni_lavoro
	
	//Allineamento in tes_ord_ven
	//riallineamento num_colli in testata ordine in base al sum colli in tutte le sessioni lavoro
	if uof_aggiorna_colli_det(fl_barcode, fs_errore) < 0 then
		//in fs_errore il messaggio di errore
		return -1
	end if

else
	//caso barcode di testata
	//faccio solo la re-impostazione della tes_ord_ven
	//ce ne erano ll_count, ce ne devono essere ll_num_colli_new
	/*es. ce ne sono 3 ne devono essere 5:     Dx= 5 - 3 = +2  ----> incrementare la tes_ord_ven.num_colli  di 2
		es. ce ne sono 3 ne devono essere 2:     Dx= 2 - 3 = -1  ----> decrementare la tes_ord_ven.num_colli  di 1
	*/
	
	//aggiorno la tes_ordini_produzione
	update tes_ordini_produzione
	set num_colli = :ll_num_colli_new
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				progr_tes_produzione=:fl_barcode;
				
	
	ll_num_colli = ll_num_colli_new - ll_count
	
	//verifico quanto diventerà il num_colli
	select num_colli
	into :ll_ret_produzione
	from tes_ord_ven
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_reg and
				num_registrazione = :ll_num_reg;
				
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in lettura colli testata ordine "+ls_ordine+" - "+sqlca.sqlerrtext
		return -1
	end if
	
	//quanto diventerebbe la colonna tes_ord_ven.num_colli dopo l'update?
	ll_ret_produzione = ll_ret_produzione + ll_num_colli
	
	if ll_ret_produzione<0 then
		//evito l'errore di mettere un numero di colli negativo
		update tes_ord_ven
		set num_colli = 0
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :li_anno_reg and
					num_registrazione = :ll_num_reg;
	else
		//fai tranquillamente la somma algebrica
		update tes_ord_ven
		set num_colli = num_colli + :ll_num_colli
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :li_anno_reg and
					num_registrazione = :ll_num_reg;
	end if
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in aggiornamento testata ordine "+ls_ordine+" colonna num_colli: "+sqlca.sqlerrtext
		return -1
	end if
	
end if

//aggiornamento colli effettuato correttamente!
return 1

end function

public function integer uof_leggi_colli_ordine (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_colli[], ref string fs_msg);

long ll_progressivo, ll_barcode, ll_tot, ll_index
string ls_sql, ls_cod_tipo_ord_ven, ls_tipo_stampa, ls_cod_prodotto, ls_cod_reparto
datastore lds_data

//Questa funzione viene utilizzata dalla finestra lista di carico w_lista_carico_sped, per recuperare i codici collo relativi ad una riga ordine

select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda and
		    anno_registrazione=:fl_anno_registrazione and
		    num_registrazione=:fl_num_registrazione;
			 
if sqlca.sqlcode<0 then
	fs_msg = "Errore in lettura cod_tipo_ord_ven - "+sqlca.sqlerrtext
	return -1
end if

select flag_tipo_bcl
into   :ls_tipo_stampa
from   tab_tipi_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda and
		    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;
			 
if sqlca.sqlcode<0 then
	fs_msg = "Errore in lettura flag_tipo_bcl - "+sqlca.sqlerrtext
	return -1
end if

choose case ls_tipo_stampa
	//---------------------------------------
	case "A"
		ls_sql = 	"select barcode, progressivo "+&
					"from tab_ord_ven_colli  "+&
					"where cod_azienda = '"+s_cs_xx.cod_azienda+"' and  "+&
						  "anno_registrazione = "+string(fl_anno_registrazione)+" and  "+&
						  "num_registrazione = "+string(fl_num_registrazione)+" and  "+&
						  "prog_riga_ord_ven = "+string(fl_prog_riga_ord_ven)+" "
	
	//---------------------------------------
	case 'B','C'
		
		//leggo il prodotto della riga
		select cod_prodotto
		into :ls_cod_prodotto
		from det_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and  
				  anno_registrazione = :fl_anno_registrazione and  
				  num_registrazione = :fl_num_registrazione and  
				  prog_riga_ord_ven = :fl_prog_riga_ord_ven;
		
		if ls_cod_prodotto="" or isnull(ls_cod_prodotto) then return 0
		
		//considero il reparto del prodotto della riga e filtro per tale reparto
		select cod_reparto
		into :ls_cod_reparto
		from anag_prodotti
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_cod_prodotto;
					
		if ls_cod_reparto="" or isnull(ls_cod_reparto) then return 0
		
		//leggo filtrato per reparto
		ls_sql = 	"select barcode, progressivo "+&
					"from tab_ord_ven_colli  "+&
					"where cod_azienda = '"+s_cs_xx.cod_azienda+"' and  "+&
						  "anno_registrazione = "+string(fl_anno_registrazione)+" and  "+&
						  "num_registrazione = "+string(fl_num_registrazione)+" and  "+&
						  "cod_reparto = '"+ls_cod_reparto+"' "
		
		
		
	case else
		fs_msg = "Tipo ordine per "+string(fl_anno_registrazione)+"/"+string(fl_num_registrazione)+"/"+string(fl_prog_riga_ord_ven)+" non previsto!"
		return -1
		
end choose


//order by
ls_sql += "order by barcode, progressivo"

//prosegui con la creazione del datastore
if not f_crea_datastore(lds_data, ls_sql) then
	//messaggio errore già dato
	return -1
end if

ll_tot = lds_data.retrieve()

for ll_index = 1 to ll_tot
	ll_barcode = lds_data.getitemnumber(ll_index, 1)
	ll_progressivo = lds_data.getitemnumber(ll_index, 2)
	
	fs_colli[ll_index] = string(ll_barcode) + "-" + string(ll_progressivo)
next

return 1
end function

public function integer uof_carica_colli_barcode (long fl_barcode, ref string fs_colli[], ref string fs_cod_reparto, ref string fs_msg);//Questa funzione restituisce un array di stringhe rappresentative dei colli associati ad un barcode
//ad esempio se il barcode 25478 ha 4 colli prodotti allora
//fs_colli[] sarà 		[1] -> 25478-1
//						[2] -> 25478-2
//						[3] -> 25478-3
//						[4] -> 25478-4
//ed inoltre la funzione tornerà 4
//-1 se c'è un errore

long ll_count, ll_index
string ls_tipo, ls_vuoto[], ls_CSB, ls_temp

//azzero l'array dei colli
fs_colli[] = ls_vuoto[]

//lettura parametro aziendale Carattere Speciale barcode (specifica Carico ordini con palmare)
select stringa
into   :ls_CSB
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='CSB' and 
		flag_parametro='S';

if sqlca.sqlcode < 0 then
	fs_msg = "Errore in ricerca del parametro CSB in tabella parametri azienda: " + sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 or isnull(ls_CSB) or ls_CSB = "" then	
	ls_CSB = ""
end if
//------------------------------------------------------------------------------------------------------------------------------------

//cerca prima in det_ordini_produzione
select cod_azienda
into :ls_temp
from det_ordini_produzione
where 	cod_azienda=:s_cs_xx.cod_azienda and
			progr_det_produzione=:fl_barcode;

if sqlca.sqlcode<0 then
	fs_msg = "Errore in lettura uof_carica_barcode_colli (det_ordini_produzione): "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode = 100 then
	//prova in tes_ordini_produzione
	select cod_azienda
	into :ls_temp
	from tes_ordini_produzione
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				progr_tes_produzione=:fl_barcode;
				
	if sqlca.sqlcode<0 then
		fs_msg = "Errore in lettura uof_carica_barcode_colli (tes_ordini_produzione): "+sqlca.sqlerrtext
		return -1
		
	elseif sqlca.sqlcode=100 then
		//nessun lancio di produzione
		fs_msg = "Nessun lancio di produzione per il barcode "+string(fl_barcode)
		return -1
	else
		//trovato in tes_ordini_produzione
		ls_tipo = "T"

	end if
	
else
	//trovato in det_ordini_produzione
	ls_tipo = "D"
	
end if

//apri un cursore per caricare tutti i codici barcode dei colli
ll_count = 0

datastore lds_reparto
string ls_sql


select count(*)
into :ll_count
from tab_ord_ven_colli
where 	cod_azienda = :s_cs_xx.cod_azienda and
			barcode = :fl_barcode;

if sqlca.sqlcode < 0 then
	fs_msg = "Errore in lettura codice colli (tipologia "+ls_tipo+")~r~n" + sqlca.sqlerrtext
	rollback;
	return -1
end if

if isnull(ll_count) then
	ll_count = 0
	return 0
end if

for ll_index=1 to ll_count
	fs_colli[ll_index] = "*"+ls_CSB + string(fl_barcode) + "-" + string(ll_index) + ls_CSB+"*"
next

//recupero il reparto -------------------------------------------
ls_sql = 	"select cod_reparto "+&
			"from tab_ord_ven_colli "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"barcode="+string(fl_barcode)
					
if not f_crea_datastore(lds_reparto, ls_sql) then
	rollback;
	return -1
end if

ll_count = lds_reparto.retrieve()

if ll_count>0 then
	//prendo solo la prima riga
	fs_cod_reparto = lds_reparto.getitemstring(1, 1)
else
	fs_cod_reparto = "-"
end if
//------------------------------------------------------------------

return upperbound(fs_colli[])

return 0
end function

public function integer uof_crea_barcode_colli (long fl_barcode, long fl_tot_colli, boolean fb_reimposta_colli, ref string fs_msg);//Donato 16/02/2011
//funzione che prepara la tabella da cui si ricavano i barcode univoci dei colli
//fl_tot_colli contiene il numero dei colli da impostare
//SE fb_reimposta_colli è true allora fai sempre prima la cancellazione dei colli del barcode e poi il re-inserimento
//SE INVECE   fb_reimposta_colli è false allora vai sempre in inserimento


long		ll_count_colli, ll_num_colli, ll_index, ll_num_registrazione, ll_prog_riga_ord_ven, ll_progressivo
long		ll_barcode_det, ll_barcode_tes
integer	li_anno_registrazione
string		ls_azione, ls_cod_reparto
datetime ldt_oggi, ldt_adesso

ldt_oggi = datetime(today(), 00:00:00)
ldt_adesso = datetime(date(1900, 1, 1), now())


//leggi le info sull'ordine associato al barcode
if uof_barcode_anno_reg (fl_barcode, li_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_reparto, fs_msg) < 0 then
	//in fs_msg il messaggio di errore
	return -1
end if

if ll_prog_riga_ord_ven>0 then
else
	setnull(ll_prog_riga_ord_ven)
end if
if ls_cod_reparto="" then
	setnull(ls_cod_reparto)
end if


if not fb_reimposta_colli then
	//non faccio la pulizia, perchè è un avanzamento produzione ... sommo ai colli già eventualmente presenti
else
	//si proviene dalla funzione di re-impostazione numero colli relativi ad un barcode
	//prima pulizia per sicurezza (SOLO PER IL BARCODE CORRENTE) ---------------
	delete from tab_ord_ven_colli
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				barcode=:fl_barcode;
				
	if sqlca.sqlcode<0 then
		fs_msg = "Errore in pulizia colli (tab_ord_ven_colli)~r~n"+sqlca.sqlerrtext
		return -1
	end if
	
end if

//poi inserisco --------------------------------------------------------------------------------------
for ll_index = 1 to fl_tot_colli
	
	//valuto ilmax+1 per la PK
	select max(progressivo)
	into :ll_progressivo
	from tab_ord_ven_colli
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				barcode = :fl_barcode;
	
	//se già ho qualcosa già da ll_index=1 vuol dire che sono nel caso in cui precedentemenmte si è fatto un pronto parziale
	//vuol dire che ll_progressivo= ad es. 1 (1 colli prodotti nel pronto parziale e ora voglio inserire ad es. altri 2)
	
	if isnull(ll_progressivo) then ll_progressivo = 0
	ll_progressivo += 1
	
	insert into tab_ord_ven_colli
		(	cod_azienda,
			barcode,
			progressivo,
			anno_registrazione,
			num_registrazione,
			prog_riga_ord_ven,
			data_creazione,
			ora_creazione,
			cod_operaio,
			cod_reparto)
	values	(	:s_cs_xx.cod_azienda,
					:fl_barcode,
					:ll_progressivo,
					:li_anno_registrazione,
					:ll_num_registrazione,
					:ll_prog_riga_ord_ven,
					:ldt_oggi,
					:ldt_adesso,
					null,
					:ls_cod_reparto);
					
	if sqlca.sqlcode<0 then
		fs_msg = "Errore in creazione barcode colli (tab_ord_ven_colli), ordine "+string(li_anno_registrazione)+"/"+string(ll_num_registrazione)
		
		if ll_prog_riga_ord_ven>0 then fs_msg += "/"+string(ll_prog_riga_ord_ven)
		
		fs_msg += " ~r~n"+sqlca.sqlerrtext
		
		return -1
	end if
next

return 1
end function

public function integer uof_etichette_scheda_prodotto (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_reparto, ref string fs_errore);//Funzione che controlla se l'ordine è OC (se il reparto prevede la stampa dell'etichetta scheda prodotto è stato già controllato)

//string							ls_flag_bcl,ls_cod_cliente, ls_rag_soc_1_div, ls_rag_soc_2_div, ls_indirizzo_div, &
//								ls_localita_div, ls_frazione_div, ls_cap_div, ls_provincia_div, ls_cod_giro_consegna, ls_alias_cliente, &
//								ls_num_ord_cliente, ls_rif_interscambio, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
//		 						ls_cap, ls_provincia, ls_cod_prodotto,ls_des_prodotto, ls_flag_etichetta_sp, ls_cod_comando, ls_cod_tessuto, &
//								ls_cod_verniciatura, ls_des_comando, ls_des_tessuto, ls_des_verniciatura, ls_piva, ls_cf, ls_piva_cf, ls_cod_nazione
//
//integer						li_risposta, li_anno_registrazione,li_anno_commessa
//
//long							ll_num_colli_testata, ll_riga,ll_num_max_tenda, ll_prog_riga_ord_ven_cfr, &
//								ll_num_tenda, ll_barcode
//
//datetime						ldt_data_consegna
//
//decimal						ld_quan_ordine,ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t


integer						li_risposta
long							ll_barcode
string							ls_flag_bcl
string							ls_flag_etichetta_sp


if not isnull(fl_barcode) then
	
	li_risposta = uof_barcode_anno_reg(fl_barcode,&
												  fi_anno_registrazione, &
												  fl_num_registrazione,&
												  fl_prog_riga_ord_ven,&
												  fs_cod_reparto,&
												  fs_errore)
	
	if li_risposta < 0 then
		//in fs_errore il messaggio
		return -1
	end if

else
	
	setnull(ll_barcode)
	li_risposta = uof_barcode_anno_reg(ll_barcode,&
												  fi_anno_registrazione, &
												  fl_num_registrazione,&
												  fl_prog_riga_ord_ven,&
												  fs_cod_reparto,&
												  fs_errore)

	if li_risposta < 0 then
		//in fs_errore il messaggio
		return -1
	end if
	
	fl_barcode = ll_barcode
end if

//verifico se l'ordine prevede la stampa (solo tipo A)
if uof_trova_tipo(fl_barcode, ls_flag_bcl, fs_errore) < 0 then
	//in fs_errore il messaggio
	return -1
end if

if ls_flag_bcl<>"A" then
	fs_errore = "Stampa etichetta Scheda Prodotto prevista solo per ordini tipo A!"
	return -1
end if

select flag_etichetta_sp
into   :ls_flag_etichetta_sp
from   anag_reparti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_reparto = :fs_cod_reparto;
		 
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore in ricerca flag_chiusura in anagrafica reparti~r~n" + sqlca.sqlerrtext
	return -1
end if

if ls_flag_etichetta_sp<>"S" then
	fs_errore = "Il reparto "+fs_cod_reparto+" non prevede la stampa dell'etichetta Scheda Prodotto!"
	return -1
end if


if uof_stampa_scheda_prodotto("", fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, fs_errore)<0 then
	//in fs_errore
	return -1
end if

return 0

end function

public function boolean uof_controlla_reparto (long al_anno_registrazione, long al_num_registrazione, string as_cod_deposito, ref string as_errore);/**
 * stefanop
 * 10/01/2012
 *
 * Controllo se per un ordine di vendita il reparto passato per argomento verrà
 * interessato nel processo di produzione;
 * In caso affermativo ritorno TRUE altrimenti FALSE
 **/
 
 
string ls_sql
long ll_rows
datastore lds_store

ls_sql = "SELECT count(det_ordini_produzione.cod_azienda) " + &
			" FROM det_ordini_produzione " + &
			" LEFT JOIN anag_reparti on " + &
			"	anag_reparti.cod_azienda = det_ordini_produzione.cod_azienda and " + &
			"	anag_reparti.cod_reparto = det_ordini_produzione.cod_reparto " + &
			" WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' " + &
			" 	AND det_ordini_produzione.anno_registrazione=" + string(al_anno_registrazione) + &
			"	AND det_ordini_produzione.num_registrazione=" + string(al_num_registrazione) + &
			" 	AND anag_reparti.cod_deposito='" + as_cod_deposito + "'"
			
ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_errore)
			
return ll_rows > 0
end function

public subroutine uof_controlla_data_pronto (datetime fdt_data_consegna, ref datetime fdt_data_pronto);
//verifica se il deposito (stabilimento) del reparto di stampa e diverso dal deposito di origine dell'ordine (tes_ord_ven.cod_deposito)
//Se SI allora sposta la data consegna a (-6) GG lavorativi rispetto alla data consegna

//NOTA CORRETTIVA PROVVISORIA
//per ora mettiamo -4

//N.B tenere conto della domenica, del sabato e di eventuali festività in tabella
//inoltre tale funzione viene chiamata solo in caso tipo ordine A (infatti viene chiamata solo da f_report_1(...) )

long			ll_gg_da_decrementare, ll_counter, ll_count
date			ldt_data_pronto
datetime		ldtm_pronto
string			ls_giorno
integer		li_num_giorno

ll_gg_da_decrementare = 4			//6
ll_counter = 0

//imposto come data consegna, poi andrò a ritroso per i giorni impostati da "ll_gg_da_decrementare"
fdt_data_pronto = fdt_data_consegna
ldt_data_pronto = date(fdt_data_pronto)

do while ll_counter<ll_gg_da_decrementare
	
	//torno indietro di un giorno
	ldt_data_pronto = relativedate(ldt_data_pronto, -1)
	
	//verifico se si tratta di un sabato o di una domenica o di una festività comandata
	ldtm_pronto = datetime(ldt_data_pronto, 00:00:00)
	
	//-------------------------------------------------------------------------------------
	//f_giorno_settimana(datetime(ldtm_pronto), ls_giorno)
	
	li_num_giorno = DayNumber(date(ldtm_pronto))

	choose case li_num_giorno
		case 1
			ls_giorno="Dom"
		case 2
			ls_giorno="Lun"
		case 3
			ls_giorno="Mar"
		case 4
			ls_giorno="Mer"
		case 5
			ls_giorno="Gio"
		case 6
			ls_giorno="Ven"
		case 7
			ls_giorno="Sab"
	end choose
	//---------------------------------------------------------------------
	
	select count(*)
	into :ll_count
	from tab_cal_ferie
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				flag_tipo_giorno='F' and
				data_giorno =:ldtm_pronto;
	
	if ls_giorno="Sab" or ls_giorno="Dom" or ll_count>0 then
		//non contare
	else
		//conta
		ll_counter += 1
	end if
	
loop

//modifica la data pronto solo se sei entrato nel ciclo
fdt_data_pronto = ldtm_pronto

return
end subroutine

public function string uof_get_riga_su_tot_righe (integer fl_anno_reg, long fl_num_reg, long fl_prog_riga);long ll_tot, ll_num_riga

select count(*)
into :ll_tot
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno_reg and
			num_registrazione=:fl_num_reg and
			(num_riga_appartenenza=0 or num_riga_appartenenza is null);	// and
			//flag_blocco<>'S';

if isnull(ll_tot) then ll_tot=0

select count(*)
into :ll_num_riga
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno_reg and
			num_registrazione=:fl_num_reg and
			(num_riga_appartenenza=0 or num_riga_appartenenza is null) and
			prog_riga_ord_ven<=:fl_prog_riga;	// and
			//flag_blocco<>'S';
			
if isnull(ll_num_riga) then ll_num_riga=0

return string(ll_num_riga) + "/" + string(ll_tot)
end function

public function integer uof_reset_produzione (long fl_barcode, ref string fs_errore);/*
Donato 30/01/2012
Questa funzione esegue il reset della produzione registrata su una fase, eliminando le sessioni_lavoro ed i colli registrati sulla fase
NON VIENE RESETTATO IL NUMERO DI BARCODE, QUINDI NON VIENE RILANCIATA LA PRODUZIONE MA SOLO RESETTATA

N.B. il minkione che chiama questa funzione, al'uscita deve effettuare COMMIT (se torna 0) o ROLLBACK (se torna -1)
*/


integer			li_anno_reg, li_ret
long				ll_num_reg, ll_prog_riga, ll_count
string				ls_cod_reparto, ls_tipo_stampa, ls_sql_where, ls_sql, ls_msg_aggiuntivo
boolean			lb_aggiorna_colli = false

if fl_barcode>0 then
else
	fs_errore = "Specificare un barcode numerico!"
	return -1
end if

if uof_barcode_anno_reg(fl_barcode, li_anno_reg, ll_num_reg, ll_prog_riga, ls_cod_reparto, fs_errore) < 0 then
	//in fs_errore il messaggio
	return -1
end if

if uof_trova_tipo(fl_barcode, ls_tipo_stampa, fs_errore) < 0 then
	//in fs_errore il messaggio
	return -1
end if

choose case ls_tipo_stampa
		
	case "B", "C"		//sfuso, tecniche: report 2, report 3
		ls_sql_where = " and progr_tes_produzione="+string(fl_barcode) + " "
		
	case "A"				//tende da sole: report 1
		ls_sql_where = " and progr_det_produzione="+string(fl_barcode) + " "
		
	case else
		fs_errore = "Tipo stampa '"+ls_tipo_stampa+"' non previsto!"
		return -1
		
end choose

//elimino colli eventuali --------------------------------------------------------------------------------------

//prima verifico se effettivamente ce ne sono (mi servirà dopo)
select count(*)
into :ll_count
from tab_ord_ven_colli
where 	cod_azienda=:s_cs_xx.cod_azienda and
			barcode=:fl_barcode;

if sqlca.sqlcode<0 then
	fs_errore = "Errore conteggio colli da tab_ord_ven_colli:~r~n" + sqlca.sqlerrtext
	return -1
end if

if ll_count>0 then
	lb_aggiorna_colli =  true
end if

if lb_aggiorna_colli then
	delete from tab_ord_ven_colli
	where cod_azienda=:s_cs_xx.cod_azienda and barcode=:fl_barcode;
		
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore cancellazione colli:~r~n" + sqlca.sqlerrtext
		return -1
	end if
end if

//elimino sessioni_lavoro --------------------------------------------------------------------------------------
ls_sql = "delete from sessioni_lavoro "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' " + ls_sql_where
execute immediate :ls_sql;
	
if sqlca.sqlcode < 0 then
	fs_errore = "Errore cancellazione sessioni lavoro:~r~n" + sqlca.sqlerrtext
	return -1
end if	

//aggiorno det_ordini_produzione --------------------------------------------------------------------------
ls_sql = "update det_ordini_produzione "+&
				"set tempo_totale_impiegato=0, "+&
					 "quan_prodotta=0, "+&
					 "flag_fine_fase='N', "+&
					 "data_fine_fase=null,"+&
					 "posizione='' "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' " + ls_sql_where
execute immediate :ls_sql;
	
if sqlca.sqlcode < 0 then
	fs_errore = "Errore aggiornamento det. ordini produzione:~r~n" + sqlca.sqlerrtext
	return -1
end if	

//aggiorno tes_ordini_produzione ----------------------------------------------------------------------------
if ls_tipo_stampa<>"A" then
	//nel caso di ordini con tipo report = 1 la tes_ordini_produzione è vuota!!!
	
	ls_sql = "update tes_ordini_produzione "+&
					"set cod_operaio_inizio=null, "+&
						  "cod_operaio_fine=null, "+&
						 "inizio_preparazione=null, "+&
						 "fine_preparazione=null, "+&
						 "flag_pronto_complessivo='N', "+&
						 "num_colli=0,"+&
						 "posizione='' "+&
				"where cod_azienda='"+s_cs_xx.cod_azienda+"' " + ls_sql_where
	execute immediate :ls_sql;
		
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore aggiornamento tes. ordini produzione:~r~n" + sqlca.sqlerrtext
		return -1
	end if	
	
end if

//tutto OK fin qui. Ora provvedi al ricalcolo del numero colli (solo se effettivamente ce ne è bisogno)
fs_errore = ""
ls_msg_aggiuntivo = ""
if lb_aggiorna_colli then
	
	if ls_tipo_stampa="A" then
		li_ret = uof_aggiorna_colli_det(fl_barcode, fs_errore)
	else
		//B, C
		li_ret = uof_aggiorna_colli_tes(fl_barcode, fs_errore)
	end if
	
	if li_ret < 0 then
		//in fs_errore il messaggio
		return -1
	end if
	
	//carica il messaggio di avviso di chiudere la finestra degfli ordini ....
	ls_msg_aggiuntivo = "Chiudere la finestra degli ordini (se aperta) oppure aggiornarli (nuova Ricerca), per permettere l'allineamento del numero colli totale in testata ordine!"

end if

fs_errore = "Operazione effettuata con successo! "+ls_msg_aggiuntivo

return 0
end function

public function integer uof_stato_prod_det_ord_ven_reparti (long al_anno_ord_ven, long al_num_ord_ven, long al_riga_ord_ven, string as_cod_deposito_partenza, ref string as_cod_reparto[], ref long al_stato_reparto[], ref string as_messaggio);/**
 * stefanop
 * 16/02/2012
 *
 * Recupero lo stato di produzione dei reparti legati al deposito passato
 **/
 
string ls_cod_reparto, ls_sql, ls_reparti
long ll_index, ll_return, ll_aperti=0, ll_parziali=0, ll_chiusi=0, ll_totale=0, ll_rows, ll_i
datastore lds_store

if isnull(as_cod_deposito_partenza) or as_cod_deposito_partenza = "" then
	
	// chiamo la funzione originale
	return uof_stato_prod_det_ord_ven_reparti(al_anno_ord_ven, al_num_ord_ven, al_riga_ord_ven, as_cod_reparto, al_stato_reparto, as_messaggio)
	
end if

// recupero i reparti del deposito
ls_sql = "SELECT DISTINCT cod_reparto " + &
			"FROM det_ordini_produzione " +  &
			"WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' AND " + &
			"anno_registrazione = " + string(al_anno_ord_ven) + " AND " + &
			"num_registrazione = " + string(al_num_ord_ven) + " AND " + &
			"prog_riga_ord_ven = " + string(al_riga_ord_ven) + " AND " + &
			"cod_reparto IN ( "+ &
			"SELECT cod_reparto FROM anag_reparti WHERE cod_azienda='" + s_cs_xx.cod_azienda +"' AND cod_deposito='" + as_cod_deposito_partenza + "') "
			
ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_messaggio)

if ll_rows < 0 then
	return -1
end if

for ll_i = 1 to ll_rows
	
	ls_cod_reparto = lds_store.getitemstring(ll_i, 1)
	
	ll_return = uof_stato_prod_det_ord_ven_reparto(al_anno_ord_ven, al_num_ord_ven, al_riga_ord_ven, ls_cod_reparto, ref as_messaggio)	
	
	as_cod_reparto[ll_i] = ls_cod_reparto
	ll_totale ++
	
	choose case ll_return
		case -1
			destroy lds_store
			return -1
		case -100
			// nessuna fase presente
			al_stato_reparto[ll_i] = ll_return
			
		case 0
			// tutto da iniziare
			al_stato_reparto[ll_i] = ll_return
			ll_aperti ++
		case 1
			// reparto completamente chiuso
			al_stato_reparto[ll_i] = ll_return
			ll_chiusi ++
		case 2
			// parzialmente chiuso
			al_stato_reparto[ll_i] = ll_return
			ll_parziali ++
	end choose
	
next

if ll_chiusi = 0 and ll_parziali = 0 then
	as_messaggio = "Niente"
	destroy lds_store
	return 0
else
	if ll_chiusi = ll_totale then
		as_messaggio = "Tutto"
		destroy lds_store
		return 1
	else
		as_messaggio = "Parziale"
		destroy lds_store
		return 2
	end if
end if

destroy lds_store
return 0
end function

public function integer uof_richiesta_trasf_componenti (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, long fl_barcode, boolean fb_stampa_subito, ref datawindow fdw_report, ref string fs_errore);long								ll_numero, ll_anno_commessa, ll_num_commessa, ll_riga, ll_max_livelli, ll_count

string								ls_cod_reparto, ls_cod_prodotto, ls_cod_versione, ls_des_prodotto, &
									ls_flag_note_prodotto, ls_flag_ignora_mp, ls_flag_des_fasi, ls_barcode_riga
									
integer							li_risposta					

datetime							ldt_data_consegna, ldt_data_pronto

decimal							ldd_quan_ordinata

uo_imposta_tv_varianti		luo_carica_report


//SE ATTIVAZIONE GESTIONE VARIANTI è "SI" ...
select parametri.numero  
into   :ll_numero  
from   parametri  
where (parametri.flag_parametro = 'N' ) and
		(parametri.cod_parametro = 'GVA' )   ;
		
if ll_numero > 0 then
else
	return 0
end if

//se passi il barcode produzione recupera anno e numero registrazione
if not isnull(fl_barcode) then
	li_risposta = uof_barcode_anno_reg(fl_barcode,&
												  fi_anno_registrazione, &
												  fl_num_registrazione,&
												  fl_prog_riga_ord_ven,&
												  ls_cod_reparto,&
												  fs_errore)
	if li_risposta < 0 then
		//in fs_errore il messaggio di errore
		return -1
	end if
else
	fl_barcode = 0
end if


//Donato: 08/04/2014, chiesto da Alberto
select count(*)
into :ll_count
from tes_rdt
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_ordine=:fi_anno_registrazione and
			num_ordine=:fl_num_registrazione and
			riga_ordine=:fl_prog_riga_ord_ven;

if ll_count>0 then
	if not g_mb.confirm("Sembra siano già presenti richieste di trasferimento per questa riga ordine. Vuoi continuare lo stesso?") then return 0
end if



//---------------------------------------------------------------------
select data_consegna, data_pronto
into :ldt_data_consegna, :ldt_data_pronto
from tes_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_registrazione and
			num_registrazione=:fl_num_registrazione;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura dati testata ordine: " + sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_errore = "Attenzione: Ordine inesistente!"
	return -1
end if

//---------------------------------------------------------------------
select cod_prodotto, cod_versione, anno_commessa, num_commessa, quan_ordine
into :ls_cod_prodotto, :ls_cod_versione, :ll_anno_commessa, :ll_num_commessa, :ldd_quan_ordinata
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_registrazione and
			num_registrazione=:fl_num_registrazione and
			prog_riga_ord_ven=:fl_prog_riga_ord_ven;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura dati riga ordine: " + sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_errore = "Attenzione: Ordine/riga inesistente!"
	return -1
end if

if isnull(ls_cod_prodotto) or ls_cod_prodotto ="" then return 0
if isnull(ldd_quan_ordinata) or ldd_quan_ordinata=0 then ldd_quan_ordinata = 1

//---------------------------------------------------------------------
select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_prodotto = :ls_cod_prodotto;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore in lettura descrizione prodotto: " + sqlca.sqlerrtext
	return -1
end if

//---------------------------------------------------------------------
ll_max_livelli = 2

//se il codice prodotto della riga ordine è un semilavorato (cioè ha un padre con relativa distinta base)
//allora imposta il max livello di espansione a 1 (al posto di due)
//es. B004X  --->   ha B004: quindi proponi per la scelta i componenti di primo livello della disitinta di B004X
//						che poi sarebbero i componenti di 2° livello della distinta di B004 ...
select count(*)
into :ll_count
from distinta
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto_figlio=:ls_cod_prodotto and
			cod_versione_figlio=:ls_cod_versione;

if ll_count>0 then
	ll_max_livelli = 1
end if
//---------------------------------------------------

ls_flag_note_prodotto = "N"
ls_flag_ignora_mp = "N"
ls_flag_des_fasi = "N"


fdw_report.reset()
fdw_report.Modify("cod_prodotto.text='" + ls_cod_prodotto + "'")
fdw_report.Modify("des_prodotto_t.text='" + ls_des_prodotto + "'")
fdw_report.Modify("ordine_t.text='" + "Ordine " + string(fi_anno_registrazione) + " / " +  string(fl_num_registrazione) + " / " +  string(fl_prog_riga_ord_ven) + "'")

ls_barcode_riga = "*O"+&
						string(fi_anno_registrazione)+&
						right("000000"+string(fl_num_registrazione),6)+&
						right("0000"+string(fl_prog_riga_ord_ven),4)+&
						"*"
fdw_report.Modify("barcode_riga_t.text='" + ls_barcode_riga + "'")

fdw_report.Modify("data_consegna_t.text='" + string(ldt_data_consegna,"dd/mm/yyyy") + "'")
fdw_report.Modify("data_pronto_t.text='" + string(ldt_data_pronto,"dd/mm/yyyy") + "'")

if ll_anno_commessa>0 and ll_num_commessa>0 then &
		fdw_report.Modify("commessa_t.text='Commessa: " + string(ll_anno_commessa)+"/"+string(ll_num_commessa) + "'")

fdw_report.Modify("quan_ordinata_t.text='Quantità: " + string(ldd_quan_ordinata,"###,###,##0.00##") + "'")

if fl_barcode>0 then fdw_report.Modify("barcode_t.text='*" + string(fl_barcode) + "*'")

ll_riga = fdw_report.insertrow(0)	
fdw_report.setitem( ll_riga, "tipo_riga", 2)

luo_carica_report = create uo_imposta_tv_varianti

luo_carica_report.is_tipo_gestione = "varianti_ordini"
luo_carica_report.il_anno_registrazione = fi_anno_registrazione
luo_carica_report.il_num_registrazione = fl_num_registrazione
luo_carica_report.il_prog_riga_registrazione = fl_prog_riga_ord_ven

//if dw_ricerca.getitemstring(dw_ricerca.getrow(),"flag_vis_mp") = "N" then
//	uo_carica_report.ib_salta_materie_prime=true
//else
	luo_carica_report.ib_salta_materie_prime=false
//end if

//Donato 14/03/2012 Controllo sul livello di espansione
//se il livello impostato è nullo o ZERO ignora il controllo livelli, cioè tutto come prima
luo_carica_report.il_livello_max = ll_max_livelli
luo_carica_report.ib_trasf_componenti = true
luo_carica_report.il_barcode = fl_barcode
luo_carica_report.id_qta_ordine = ldd_quan_ordinata
//------------------------------------------------------------------

luo_carica_report.uof_imposta_report(fdw_report, ls_cod_prodotto, ls_cod_versione, 1, fs_errore)

destroy luo_carica_report
commit;

if fb_stampa_subito then
	fdw_report.print()
else
	s_cs_xx.parametri.parametro_dw_2 = fdw_report
	s_cs_xx.parametri.parametro_s_1_a[1] = ls_cod_prodotto
	s_cs_xx.parametri.parametro_s_1_a[2] = ls_des_prodotto
	s_cs_xx.parametri.parametro_d_1_a[1] = fi_anno_registrazione
	s_cs_xx.parametri.parametro_d_1_a[2] = fl_num_registrazione
	s_cs_xx.parametri.parametro_d_1_a[3] = fl_prog_riga_ord_ven
	s_cs_xx.parametri.parametro_d_1_a[4] = ll_max_livelli
	s_cs_xx.parametri.parametro_d_1_a[5] = fl_barcode
	s_cs_xx.parametri.parametro_d_1_a[6] = ldd_quan_ordinata
	s_cs_xx.parametri.parametro_d_1_a[7] = ll_anno_commessa
	s_cs_xx.parametri.parametro_d_1_a[8] = ll_num_commessa
	s_cs_xx.parametri.parametro_data_3 = ldt_data_consegna
	s_cs_xx.parametri.parametro_data_4 = ldt_data_pronto
	
	window_open(w_report_db_varianti_componenti, -1)
end if

return 0
end function

public function integer uof_get_ordine_reparti_2 (integer fi_anno_reg, long fl_num_reg, long fl_prog_riga, string fs_reparto_corrente, ref string fs_lista_reparti, ref string fs_stab_rep_prossimo, ref string fs_reparto_successivo);string					ls_cod_tipo_ord_ven, ls_cod_deposito_origine, ls_cod_prodotto, ls_cod_versione, ls_cod_reparti_trovati[], ls_errore, ls_sql, &
						ls_cod_reparto_anagrafica, ls_tipo_stampa, ls_appo, ls_cod_dep_corrente, ls_flag_tipo_log, ls_messaggio, ls_parametri, &
						ls_cod_deposito_giro, ls_dep_succ
						
long					ll_ret, ll_index_2, ll_new

datastore			lds_data

boolean				lb_piu_di_uno=false

uo_produzione		luo_prod

integer				li_ret


//torna				-1 in caso di errore
//						0 se tutto OK ed il reparto corrente non è l'ultimo
//						1 se tutto OK ed il reparto è l'utlimo della lista (o se c'è un solo reparto, che quindi è anche l'ultimo)


ib_altro_stabilimento = false
fs_lista_reparti = "ERR"
fs_stab_rep_prossimo = "ERR"

//------------------------------------------------------------------------------------------
ls_flag_tipo_log = "LISTA_REP"

if isnull(fi_anno_reg) then
	ls_parametri = "anno=NULL"
else
	ls_parametri = "anno="+string(fi_anno_reg)
end if
if isnull(fl_num_reg) then
	ls_parametri += ",numero=NULL"
else
	ls_parametri += ",numero="+string(fl_num_reg)
end if
if isnull(fl_prog_riga) then
	ls_parametri += ",riga=NULL"
else
	ls_parametri += ",riga="+string(fl_prog_riga)
end if
if isnull(fs_reparto_corrente) then
	ls_parametri += ",rep.corrente=NULL"
else
	ls_parametri += ",rep.corrente='"+fs_reparto_corrente+"'"
end if

ls_parametri =  "uof_get_ordine_reparti_2("+ls_parametri+")"

	

//leggo tipo ordine e deposito acquisizione ordine
select cod_tipo_ord_ven, cod_deposito
into :ls_cod_tipo_ord_ven, :ls_cod_deposito_origine
from tes_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_reg and
			num_registrazione=:fl_num_reg;

if sqlca.sqlcode<0 then
	//Errore in lettura tipo ordine e deposito
	ls_messaggio = ls_parametri + " : (tipo ord./dep.origine) " + sqlca.sqlerrtext
	uof_scrivi_log_sistema(ls_flag_tipo_log, ls_messaggio)
	
	return -1
end if

select		flag_tipo_bcl
into		:ls_tipo_stampa
from		tab_tipi_ord_ven
where  	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;

if sqlca.sqlcode < 0 then 
	//Errore lettura flag_tipo_bcl
	
	ls_messaggio = ls_parametri + " : (flag_tipo_bcl) " + sqlca.sqlerrtext
	uof_scrivi_log_sistema(ls_flag_tipo_log, ls_messaggio)
	
	return -1
end if



choose case ls_tipo_stampa
	case "A"		//tende da sole
		ls_sql = "select distinct cod_reparto, ordine from tes_stampa_ordini "+&
					"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"anno_registrazione="+string(fi_anno_reg)+" and "+&
							"num_registrazione="+string(fl_num_reg)+" and "+&
							"prog_riga_comanda = " + string(fl_prog_riga) + " and "+&
							"prog_sessione="+string(il_sessione_tes_stampa) + " "
		
	case "B"			//sfuso
		//non previsto, comunque per sicurezza faccio questa cosa qui
		fs_lista_reparti = ""
		fs_stab_rep_prossimo = ""
		return 0
		
	case "C"		//tecniche
		ls_sql = "select distinct cod_reparto, ordine from tes_stampa_ordini "+&
					"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"anno_registrazione="+string(fi_anno_reg)+" and "+&
							"num_registrazione="+string(fl_num_reg)+ " and "+&
							"prog_sessione="+string(il_sessione_tes_stampa) + " "

	case else
		
		ls_messaggio = ls_parametri + " : TIPO ORDINE NON PREVISTO (A,B,C)"
		uof_scrivi_log_sistema(ls_flag_tipo_log, ls_messaggio)
		
		return -1
		
end choose

ls_sql += " order by ordine "
ll_new = 0

ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_ret > 0 then
	for ll_index_2=1 to ll_ret
		ls_appo = lds_data.getitemstring(ll_index_2, "cod_reparto")
		
		if ls_appo<>"" and not isnull(ls_appo) then
			ll_new += 1
			ls_cod_reparti_trovati[ll_new] = ls_appo
		end if
	next
	
elseif ll_ret=0 then
	ls_messaggio = ls_parametri + " : (ds) No righe dalla tabella lancio produzione "
	if isnull(il_sessione_tes_stampa) then
		ls_messaggio += "; IDsessione=NULL"
	else
		ls_messaggio += "; IDsessione="+string(il_sessione_tes_stampa)
	end if
	
	uof_scrivi_log_sistema(ls_flag_tipo_log, ls_messaggio)
	
	destroy lds_data;
	
	return -1
else
	ls_messaggio = ls_parametri + " : (ds) " + ls_errore
	uof_scrivi_log_sistema(ls_flag_tipo_log, ls_messaggio)
	
	destroy lds_data;
	
	return -1
end if

destroy lds_data;
//-----------------------------------------------------------------------------------------------------------------


select cod_deposito
into :ls_cod_dep_corrente
from anag_reparti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_reparto=:fs_reparto_corrente;

if sqlca.sqlcode<0 then
	ls_messaggio = ls_parametri + " : (dep.corrente) " + sqlca.sqlerrtext
	uof_scrivi_log_sistema(ls_flag_tipo_log, ls_messaggio)
	
	return -1
end if


//se il giro consegna esiste ed ha un deposito assegnato, considero quello al posto del deposito origine ordine
//se la testata ordine ha un giro consegna e questo ha un deposito usare questo al posto del deposito origine ordine
//se esiste un giro di consegna e questo ha un deposito, considera questo al posto del deposito testata ordine vendita
ls_cod_deposito_giro = uof_deposito_giro_consegna(fi_anno_reg, fl_num_reg)
if g_str.isnotempty(ls_cod_deposito_giro) and ls_cod_deposito_giro<>ls_cod_deposito_origine then ls_cod_deposito_origine = ls_cod_deposito_giro


//alla fine ciclo ls_cod_reparti_trovati[]: contiene i reparti in ordine di num_sequenza
choose case upperbound(ls_cod_reparti_trovati[])
	case is > 1
		lb_piu_di_uno=true
		
	case 0
		fs_lista_reparti = ""
		fs_stab_rep_prossimo = ""
		return 0
		
	case else  // 1 solo reparto
		lb_piu_di_uno=false
		
end choose

if not lb_piu_di_uno then
	//C'E' UN SOLO REPARTO
	
	//visualizza stabilimento origine ordine (primi 3 caratteri della descrizione)
	fs_stab_rep_prossimo = left(f_des_tabella("anag_depositi", "cod_deposito='" +  ls_cod_deposito_origine + "'", "des_deposito" ), 3)
	
	fs_lista_reparti = ls_cod_reparti_trovati[1]
	
	//come reparto successivo segnaliamo sempre le prime 3 lettere della descrizione dello stabilim. origine ordine
	//va in etichetta spedizione/imballo
	fs_reparto_successivo = fs_stab_rep_prossimo
	
	//se il deposito successivo + diverso dal deposito del reparto corrente segnalalo
	if ls_cod_deposito_origine <> ls_cod_dep_corrente then
		ib_altro_stabilimento = true
	else
		ib_altro_stabilimento = false
	end if
	//------------------------------------------------------------------------------------------
	
	//l'unico reparto è dunque anche l'ultimo
	return 1
	
else
	//CASO PIU' REPARTI COINVOLTI
	
	fs_lista_reparti = ""
	
	for ll_ret=1 to upperbound(ls_cod_reparti_trovati[])
		
		//se non sei all'inizio vai prima a capo
		if ll_ret>1 then fs_lista_reparti += "~r~n"
		
		if ls_cod_reparti_trovati[ll_ret] = fs_reparto_corrente then
			//siamo sul reparto relativo all'ordine di lavoro corrente
			//di regola entra una volta sola in questo IF nell'ambito di questo ciclo for
			
			//marca la stringa in modo da evidenziare che si tratta del reparto corrente (>> REPXX <<)
			fs_lista_reparti += ">> " + ls_cod_reparti_trovati[ll_ret] + " <<"
			
			if ll_ret = upperbound(ls_cod_reparti_trovati[]) then
				//il reparto relativo all'ordine di lavoro corrente è anche l'ultimo previsto
				//visualizza stabilimento origine dell'ordine (ovvero quello del giro consegna)
				fs_stab_rep_prossimo = left(f_des_tabella("anag_depositi", "cod_deposito='" +  ls_cod_deposito_origine + "'", "des_deposito" ), 3)
				
				//come reparto successivo segnaliamo sempre le prime 3 lettere della descrizione dello stabilim. origine ordine
				//va in etichetta spedizione/imballo
				fs_reparto_successivo = fs_stab_rep_prossimo
				
				//se il deposito successivo + diverso dal deposito del reparto corrente segnalalo
				if ls_cod_deposito_origine <> ls_cod_dep_corrente then
					ib_altro_stabilimento = true
				else
					ib_altro_stabilimento = false
				end if
				//------------------------------------------------------------------------------------------
				
				//il reparto corrente è l'ultimo reparto della lista
				li_ret = 1
				
			else
				
				//il reparto relativo all'ordine di lavoro corrente NON è l'ultimo previsto
				//visualizza le prime tre lettere della descrizione del deposito del reparto successivo
				fs_reparto_successivo = 		ls_cod_reparti_trovati[ll_ret + 1]
				ls_dep_succ = f_des_tabella("anag_reparti", "cod_reparto='" +  fs_reparto_successivo + "'", "cod_deposito" )
				fs_stab_rep_prossimo = left(f_des_tabella("anag_depositi", "cod_deposito='" +  ls_dep_succ + "'", "des_deposito" ), 3)
				
				//se il deposito successivo è diverso dal deposito del reparto corrente (che stai stampando) segnalalo
				if ls_dep_succ <> ls_cod_dep_corrente then
					ib_altro_stabilimento = true
				else
					ib_altro_stabilimento = false
				end if
				//------------------------------------------------------------------------------------------
				
				//il reparto corrente NON è l'ultimo reparto della lista
				li_ret = 0
				
			end if
		
		else
			//trattasi di reparto non corrente
			fs_lista_reparti += ls_cod_reparti_trovati[ll_ret]
		end if
		
	next
	
end if


return li_ret
end function

public function integer uof_get_ordine_reparti (integer fi_anno_reg, long fl_num_reg, long fl_prog_riga, string fs_reparto_corrente, ref string fs_lista_reparti, ref string fs_stab_rep_prossimo, ref string fs_reparto_successivo, ref boolean fb_alert);string					ls_cod_tipo_ord_ven, ls_cod_deposito_origine, ls_cod_prodotto, ls_cod_versione, ls_cod_reparti_trovati[], ls_errore, ls_sql, &
						ls_cod_reparto_anagrafica, ls_tipo_stampa, ls_cod_deposito_giro
						
long					ll_ret, ll_index_2, ll_pos_rep_corrente

datastore			lds_data

boolean				lb_piu_di_uno=false

uo_produzione		luo_prod


fs_lista_reparti = "ERR"
fs_stab_rep_prossimo = "ERR"

//leggo tipo ordine e deposito acquisizione ordine
select cod_tipo_ord_ven, cod_deposito
into :ls_cod_tipo_ord_ven, :ls_cod_deposito_origine
from tes_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_reg and
			num_registrazione=:fl_num_reg;

if sqlca.sqlcode<0 then
	//Errore in lettura tipo ordine e deposito
	return -1
end if

select		flag_tipo_bcl
into		:ls_tipo_stampa
from		tab_tipi_ord_ven
where  	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;

if sqlca.sqlcode < 0 then 
	//Errore lettura flag_tipo_bcl
	return -1
end if

choose case ls_tipo_stampa
	case "A", "C"		//tende da sole e tecniche
		ls_sql = "select cod_reparto from det_ordini_produzione "+&
					"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"anno_registrazione="+string(fi_anno_reg)+" and "+&
							"num_registrazione="+string(fl_num_reg)+" and "+&
							"prog_riga_ord_ven = " + string(fl_prog_riga) + " "
		ls_sql += " order by progr_det_produzione "
		
		ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql)
		
	case "B"
		//non previsto, comunque per sicurezza faccio questa cosa qui
		fs_lista_reparti = ""
		fs_stab_rep_prossimo = ""
		
		return 0
		
	case else
		return -1
		
end choose

if ll_ret > 0 then
	for ll_index_2=1 to ll_ret
		ls_cod_reparti_trovati[ll_index_2] = lds_data.getitemstring(ll_index_2, "cod_reparto")
	next
else
	destroy lds_data;
	
	return -1
end if

destroy lds_data;
//-----------------------------------------------------------------------------------------------------------------

/*
1.						A
2.					>>B<<
3.						C
origine				S1
*/

//alla fine ciclo ls_cod_reparti_trovati[]: contiene i reparti in ordine di num_sequenza
choose case upperbound(ls_cod_reparti_trovati[])
	case is > 1
		lb_piu_di_uno=true
		
	case 0
		fs_lista_reparti = ""
		fs_stab_rep_prossimo = ""
		return 0
		
	case else  // 1 solo reparto
		lb_piu_di_uno=false
		
end choose


//se la testata ordine ha un giro consegna e questo ha un deposito usare questo al posto del deposito origine ordine
//se esiste un giro di consegna e questo ha un deposito, considera questo al posto del deposito testata ordine vendita
ls_cod_deposito_giro = uof_deposito_giro_consegna(fi_anno_reg, fl_num_reg)
if g_str.isnotempty(ls_cod_deposito_giro) and ls_cod_deposito_giro<>ls_cod_deposito_origine then ls_cod_deposito_origine = ls_cod_deposito_giro


if not lb_piu_di_uno then
	//visualizza stabilimento origine ordine (primi 3 caratteri della descrizione)
	fs_stab_rep_prossimo = left(f_des_tabella("anag_depositi", "cod_deposito='" +  ls_cod_deposito_origine + "'", "des_deposito" ), 3)
	
	fs_lista_reparti = ls_cod_reparti_trovati[1]
	
	//come reparto successivo segnaliamo sempre le prime 3 lettere della descrizione dello stabilim. origine ordine
	//va in etichetta spedizione/imballo
	fs_reparto_successivo = fs_stab_rep_prossimo
	
	//immagine alert su etichetta imballo
	fb_alert = uof_reparto_successivo_esterno(ls_cod_reparti_trovati[], 1, ls_cod_deposito_origine)
	
	return 0
	
else
	//caso più reparti coinvolti
	
	fs_lista_reparti = ""
	
	for ll_ret=1 to upperbound(ls_cod_reparti_trovati[])
		
		//se non sei all'inizio vai prima a capo
		if ll_ret>1 then fs_lista_reparti += "~r~n"
		
		if ls_cod_reparti_trovati[ll_ret] = fs_reparto_corrente then
			//siamo sul reparto relativo all'ordine di lavoro corrente
			//di regola entra una volta sola in questo IF nell'ambito di questo ciclo for
			
			//marca la stringa in modo da evidenziare che si tratta del reparto corrente (>> REPXX <<)
			fs_lista_reparti += ">> " + ls_cod_reparti_trovati[ll_ret] + " <<"
			ll_pos_rep_corrente = ll_ret
			
			if ll_ret = upperbound(ls_cod_reparti_trovati[]) then
				//il reparto relativo all'ordine di lavoro corrente è anche l'ultimo previsto
				//visualizza stabilimento origine dell'ordine
				fs_stab_rep_prossimo = left(f_des_tabella("anag_depositi", "cod_deposito='" +  ls_cod_deposito_origine + "'", "des_deposito" ), 3)
				
				//come reparto successivo segnaliamo sempre le prime 3 lettere della descrizione dello stabilim. origine ordine
				//va in etichetta spedizione/imballo
				fs_reparto_successivo = fs_stab_rep_prossimo
				
			else
				
				//il reparto relativo all'ordine di lavoro corrente NON è l'ultimo previsto
				//visualizza sempre CODREPsucc/STABorigine ovvero CODREPsucc/STABgiro
				fs_reparto_successivo = 		ls_cod_reparti_trovati[ll_ret + 1] + "/" + &
													left(f_des_tabella("anag_depositi", "cod_deposito='" +  ls_cod_deposito_origine + "'", "des_deposito" ), 3)
				fs_stab_rep_prossimo = left(f_des_tabella("anag_depositi", "cod_deposito='" +  ls_cod_deposito_origine + "'", "des_deposito" ), 3)
				
			end if
		
		else
			//trattasi di reparto non corrente
			fs_lista_reparti += ls_cod_reparti_trovati[ll_ret]
		end if
		
	next
	
	//immagine alert su etichetta imballo
	fb_alert = uof_reparto_successivo_esterno(ls_cod_reparti_trovati[], ll_pos_rep_corrente, ls_cod_deposito_origine)
	
end if


return 1
end function

public function boolean uof_reparto_successivo_esterno (string fs_reparti_trovati[], long fl_pos_reparto_corrente, string fs_cod_deposito_origine);long ll_index, ll_tot
string ls_cod_deposito_corrente, ls_cod_deposito_succ

select cod_deposito
into :ls_cod_deposito_corrente
from anag_reparti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_reparto=:fs_reparti_trovati[fl_pos_reparto_corrente];


ll_tot = upperbound(fs_reparti_trovati[])

for ll_index=fl_pos_reparto_corrente+1 to ll_tot
	
	select cod_deposito
	into :ls_cod_deposito_succ
	from anag_reparti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_reparto=:fs_reparti_trovati[ll_index];
	
	if ls_cod_deposito_succ<> ls_cod_deposito_corrente then
		//beccato
		return true
	end if
	
next

//se ancora non hai rilevato depositi esterni, verifica se per caso lo stabilimento origine e diverso dallo stabilimento del deposito corrente
if fs_cod_deposito_origine<> ls_cod_deposito_corrente then
	//beccato
	return true
end if

return false
end function

public function integer uof_stato_prod_det_ord_ven_trasf (long al_anno_ord_ven, long al_num_ord_ven, long al_prog_riga_ord_ven, string as_deposito_commerciale, string as_reparto_produzione, integer ai_stato_reparto_produzione, ref string as_flag_stato, ref string as_errore);/**
 * stefanop
 * 11/04/2012
 *
 * Recupera lo stato della riga dell'ordine.
 * Risultati variabile byref :as_flag_stato
 * 1. [S,V,P,C] = Se non esiste una bolla che è collegata con la riga d'ordine e lo stato di produzione del reparto è iniziato				valore ritorno: 0
 * 2. [T] = Se la riga è presente in una bolla e la bolla NON è confermata, quindi in trasferimento											valore ritorno: 1
 * 3. [S,V,P,C] = La bolla è stata confermata e viene indicata la lettera della stabilimento di destinazione della bolla						valore_ritorno: 2
 **/

string ls_cod_deposito_produzione, ls_sql, ls_flag_movimenti, ls_cod_deposito_tras, ls_des_deposito
int li_count
datastore lds_store

setnull(as_errore)
setnull(as_flag_stato)

select cod_deposito
into :ls_cod_deposito_produzione
from anag_reparti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_reparto = :as_reparto_produzione;

if sqlca.sqlcode < 0 then
	as_errore = "Errore durante la lettura del deposito di produzione. " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_cod_deposito_produzione) or ls_cod_deposito_produzione = "" then
	as_errore = "Il reparto " + as_reparto_produzione + " non ha il deposito associato."
	return -1
end if

// Recupero eventuali bolle
ls_sql = "SELECT tes_bol_ven.flag_movimenti, tes_bol_ven.cod_deposito_tras " + &
			"FROM det_bol_ven " + &
			"JOIN tes_bol_ven on " + &
			"	tes_bol_ven.cod_azienda = det_bol_ven.cod_azienda AND " + &
			"	tes_bol_ven.anno_registrazione = det_bol_ven.anno_registrazione AND " + &
			"	tes_bol_ven.num_registrazione  = det_bol_ven.num_registrazione " + &
			"WHERE det_bol_ven.cod_azienda ='" + s_cs_xx.cod_azienda + "' AND " + &
			"	det_bol_ven.anno_registrazione_ord_ven =" + string(al_anno_ord_ven) +" AND " + &
			"	det_bol_ven.num_registrazione_ord_ven =" + string(al_num_ord_ven ) + " AND " + &
			"	det_bol_ven.prog_riga_ord_ven =" + string(al_prog_riga_ord_ven) + " AND " + &
			"	tes_bol_ven.cod_deposito ='" + ls_cod_deposito_produzione + "' " + &
			"ORDER BY tes_bol_ven.data_registrazione DESC"
			
li_count = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_errore)

if li_count < 0 then 
	return -1
elseif li_count = 0 then
	// 1. Iniziali del reparto di produzione
	if ai_stato_reparto_produzione > 0 then
		as_flag_stato = left(as_reparto_produzione, 1)
	end if
	
	return 0
end if

// 2. C'è una bolla
as_flag_stato = "T"
ls_flag_movimenti = lds_store.getitemstring(1, 1)
ls_cod_deposito_tras = lds_store.getitemstring(1, 2)

if ls_flag_movimenti = "S" then
	
	select des_deposito
	into :ls_des_deposito
	from anag_depositi
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_deposito = :ls_cod_deposito_tras;
			 
			 
	// 3. La bolla è confermata, faccio vedere l'iniziale del deposito di trasferimento
	as_flag_stato = left(ls_des_deposito, 1)
	
	return 2
	
end if

return 1

end function

public function integer uof_controlla_produzione (long fl_num_registrazione, integer fi_anno_registrazione, integer fi_prog_riga_ord_ven, ref string fs_errore);/* Funzione che verifica se nelle tabelle di produzione esistono già i record relativi all'ordine corrente, se esistono
 controlla se è già stato fatto almeno una registrazione di avanzamento di produzione. In tutti e due i casi avverte
 l'operatore
 nome: uof_controlla_produzione
 tipo: intero
 		 0 passed
			-1 failed
 
  
		Creata il 30-12-2003
		Autore Diego Ferrari

		18/052/2012 (EnMe): modificata la funzione in modo che si possa usare ANCHE a livello di singola riga

*/
string ls_test, ls_sql

// per prima cosa controlla se ci sono delle sessioni di lavoro

declare r_test_sessioni dynamic cursor for sqlsa;

ls_sql = " select cod_azienda from   sessioni_lavoro where  progr_det_produzione in " + &
			" (select progr_det_produzione from   det_ordini_produzione where  cod_azienda='" + s_cs_xx.cod_azienda + "' " + &
			"   and  anno_registrazione= " + string(fi_anno_registrazione) + &
			"   and    num_registrazione= " + string(fl_num_registrazione)
			if fi_prog_riga_ord_ven > 0 and not isnull(fi_prog_riga_ord_ven) then
				ls_sql += "   and prog_riga_ord_ven = " + string(fi_prog_riga_ord_ven)
			end if
			ls_sql += " ) " 
		
prepare sqlsa from :ls_sql;

open r_test_sessioni;

if sqlca.sqlcode < 0 then 
	close r_test_sessioni;
	fs_errore = "Errore in OPEN cursore r_test_sessioni (uof_controlla_produzione)~r~n" + sqlca.sqlerrtext
	return -1
end if

fetch r_test_sessioni
into  :ls_test;

if sqlca.sqlcode < 0 then 
	close r_test_sessioni;
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

if sqlca.sqlcode =100 then  // se non c'è alcuna sessione procede con il controllo del solo lancio di produzione

	close r_test_sessioni;
	
	declare r_test_sessioni_1 dynamic cursor for sqlsa;
	
	ls_sql =	" select cod_azienda from det_ordini_produzione where  cod_azienda='" + s_cs_xx.cod_azienda + "' " + &
				" and    anno_registrazione= " + string(fi_anno_registrazione) + &
				" and    num_registrazione= " + string(fl_num_registrazione)
				if fi_prog_riga_ord_ven > 0 and not isnull(fi_prog_riga_ord_ven) then
					ls_sql += "   and prog_riga_ord_ven = " + string(fi_prog_riga_ord_ven)
				end if

	prepare sqlsa from :ls_sql;
	
	open dynamic r_test_sessioni_1;
	
	if sqlca.sqlcode < 0 then 
		close r_test_sessioni_1;
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	fetch r_test_sessioni_1
	into  :ls_test;
	
	if sqlca.sqlcode < 0 then 
		close r_test_sessioni_1;
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode = 100 then // controlla se esiste un record in tes_ordini_produzione
		close r_test_sessioni_1;
		
		declare 	r_test_sessioni_2 cursor for
		select 	cod_azienda
		from   	tes_ordini_produzione
		where  	cod_azienda=:s_cs_xx.cod_azienda and 
					anno_registrazione=:fi_anno_registrazione and 
					num_registrazione=:fl_num_registrazione;
	
		open r_test_sessioni_2;
		
		if sqlca.sqlcode < 0 then 
			close r_test_sessioni_2;
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		fetch r_test_sessioni_2
		into  :ls_test;
		
		if sqlca.sqlcode < 0 then 
			close r_test_sessioni_2;
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		if sqlca.sqlcode = 0 then
			close r_test_sessioni_2;
			fs_errore = ""
			return 3 // non è un errore ma significa che almeno un record in tes_ordini_produzione
		end if
		
		
		if sqlca.sqlcode = 100 then // controlla se esiste un record in tes_ordini_produzione
			close r_test_sessioni_2;
			return 0 // significa che non vi è alcun record nelle tabelle di produzione
		end if
		
	end if
	
	if sqlca.sqlcode = 0 then
		close r_test_sessioni_1;
		fs_errore = ""
		return 2 // non è un errore ma significa che almeno un record in det_ordini_produzione
	end if
	
end if


if not isnull(ls_test) then
	close r_test_sessioni;
	fs_errore = ""
	return 1 // non è un errore ma significa che esiste almeno una sessione di lavoro
end if

close r_test_sessioni;

return 0
end function

public function integer uof_get_tipo_det_ven_add (long fl_anno_ordine, long fl_num_ordine, long fl_prog_riga_ord_ven, ref string fs_cod_tipo_det_ven_addizionali, ref string fs_errore);// funzione che partendo dalla riga ordine, restituisce il tipo dettaglio addizionale corretto
string ls_cod_tipo_ord_ven, ls_cod_prodotto


select 	cod_tipo_ord_ven
into 		:ls_cod_tipo_ord_ven
from 		tes_ord_ven
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :fl_anno_ordine and
			num_registrazione = :fl_num_ordine;
		
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in lettura testata ordine di vendita - uo_produzione.uof_get_tipo_det_ven_add " + sqlca.sqlerrtext
	return -1
end if

if sqlca.sqlcode = 100 then
	fs_errore = "Errore in lettura testata ordine di vendita - (uo_produzione.uof_get_tipo_det_ven_add); l'ordine è inesistente " + string(fl_anno_ordine) + "/" + string(fl_num_ordine)
	return -1
end if

		
select 	cod_prodotto
into 		:ls_cod_prodotto
from 		det_ord_ven
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :fl_anno_ordine and
			num_registrazione = :fl_num_ordine and
			prog_riga_ord_ven = :fl_prog_riga_ord_ven;
		
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in lettura riga ordine di vendita - uo_produzione.uof_get_tipo_det_ven_add " + sqlca.sqlerrtext
	return -1
end if
if sqlca.sqlcode = 100 then
	fs_errore = "Errore in lettura riga ordine di vendita  - (uo_produzione.uof_get_tipo_det_ven_add); la riga ordine è inesistente " + string(fl_anno_ordine) + "/" + string(fl_num_ordine) + "/" + string(fl_prog_riga_ord_ven)
	return -1
end if

select 	cod_tipo_det_ven_addizionali
into		:fs_cod_tipo_det_ven_addizionali
from		tab_flags_conf_tipi_ord_ven
where 	cod_azienda =:s_cs_xx.cod_azienda and
			cod_modello = :ls_cod_prodotto and
			cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in parametri configuratore - uo_produzione.uof_get_tipo_det_ven_add " + sqlca.sqlerrtext
	return -1
end if
if sqlca.sqlcode = 100 then
	select 	cod_tipo_det_ven_addizionali
	into		:fs_cod_tipo_det_ven_addizionali
	from		tab_flags_configuratore
	where 	cod_azienda =:s_cs_xx.cod_azienda and
				cod_modello = :ls_cod_prodotto ;
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in parametri configuratore - uo_produzione.uof_get_tipo_det_ven_add " + sqlca.sqlerrtext
		return -1
	end if
	if sqlca.sqlcode = 100 then
		fs_errore = "Errore in parametri configuratore - uo_produzione.uof_get_tipo_det_ven_add; attenzione; manca il tipo dettaglio addizionali nei parametri del configuratore"
		return -1
	end if

end if

return 0
end function

public function integer uof_commesse_esterne_ordine (integer fi_anno_ordine, long fl_num_ordine, ref string fs_errore);//Donato 06/07/2012 prima di dare messaggio bloccante verifica se per caso i DDT di trasferimento ci sono
	//es.  ordine del deposito 100
	//		riga 10   B020		B020Y		S_TS
	//								B020X	V_TS
	//    se l'operatore di saccolongo cerca di chiudere la commessa dell'ordine bisogna che sia presente nel sistema il ddt di trasf da veggiano a saccolongo
	//questo per ogni riga di ordine
	
long		ll_riga_ordine
integer	li_ret
string		ls_flag_tipo_bcl, ls_cod_deposito, ls_cod_deposito_produzione

select tipi.flag_tipo_bcl, tes.cod_deposito, tes.cod_deposito_prod
into :ls_flag_tipo_bcl, :ls_cod_deposito, :ls_cod_deposito_produzione
from tes_ord_ven as tes
join tab_tipi_ord_ven as tipi on	tes.cod_azienda=tipi.cod_azienda and
										tes.cod_tipo_ord_ven=tipi.cod_tipo_ord_ven
where 	tes.cod_azienda=:s_cs_xx.cod_azienda and
			tes.anno_registrazione=:fi_anno_ordine and
			tes.num_registrazione=:fl_num_ordine;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura tipo ordine (uof_commesse_esterne_ordine): "+sqlca.sqlerrtext
	return -1
end if

if ls_flag_tipo_bcl="B" then
	//tipo ordine sfuso
	if ls_cod_deposito=ls_cod_deposito_produzione then return 0
else
	//no sfuso
end if

declare cu_dettagli cursor for  
	select prog_riga_ord_ven
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fi_anno_ordine and
				num_registrazione=:fl_num_ordine and
				num_commessa > 0
	order by prog_riga_ord_ven;

open cu_dettagli;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in OPEN cursore 'cu_CdC'~r~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_dettagli into :ll_riga_ordine;

	if sqlca.sqlcode < 0 then
		close cu_dettagli;
		fs_errore = "Errore in FETCH cursore 'cu_dettagli' :" + sqlca.sqlerrtext
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	//procedi
	li_ret = uof_commesse_esterne_riga_ordine(true, fi_anno_ordine, fl_num_ordine, ll_riga_ordine, fs_errore)
	if li_ret<0 then
		//in fs_errore il messaggio
		return -1
	elseif  li_ret = 1 then
		//in fs_errore il messaggio sulla riga che presenta fasi esterne con ddt di trasferimento non ancora presenti
		return 1
	end if
loop

close cu_dettagli;

	
return 0
end function

public function integer uof_commesse_esterne_riga_ordine (boolean fb_da_ordine, integer fi_anno_ordine, long fl_num_ordine, integer fl_riga_ordine, ref string fs_errore);//Donato 06/07/2012 prima di dare messaggio bloccante verifica se per caso i DDT di trasferimento ci sono
	//es.  ordine del deposito 100
	//		riga 10   B020		B020Y		S_TS
	//								B020X	V_TS
	//    se l'operatore di saccolongo cerca di chiudere la commessa dell'ordine bisogna che sia presente nel sistema il ddt di trasf da veggiano a saccolongo

	
return 0
end function

public function integer uof_elimina_ologrammi (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_messaggio);integer li_count

//cancellazione di tutti gli ologrammi associati alla riga ordine

select count(*)
into :li_count
from det_ord_ven_ologramma
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:al_anno_registrazione and
			num_registrazione=:al_num_registrazione and
			prog_riga_ord_ven=:al_prog_riga_ord_ven;

if isnull(li_count) or li_count<=0 then
	//nulla da cancellare
	return 0
else
	//ci sono ologrammi
end if

if not g_mb.confirm( "Attenzione", "Ci sono ologrammi assegnati alla riga ordine "+string(al_anno_registrazione)+"/"+string(al_num_registrazione)+"/"+string(al_prog_riga_ord_ven)+"! "+&
											"Procedo alla cancellazione?") then
											
	as_messaggio = "Operazione annullata dall'utente!"
	return 1
end if


delete from det_ord_ven_ologramma
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:al_anno_registrazione and
			num_registrazione=:al_num_registrazione and
			prog_riga_ord_ven=:al_prog_riga_ord_ven;

if sqlca.sqlcode<0 then 
	as_messaggio = "Errore in cancellazione ologrammi: " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

public function integer uof_controlla_ologrammi (boolean fb_check, long fl_barcode, ref integer fi_anno_registrazione, ref long fl_num_registrazione, ref long fl_prog_riga_ord_ven, ref string fs_messaggio);string							ls_cod_reparto, ls_flag_ologramma, ls_cod_prodotto
long							ll_count
integer						li_ret
s_cs_xx_parametri		lstr_parametri


//se l'argomento fb_check è a FALSE, tutti i messaggi che riguardano controlli NON SUPERATI
//circa il fatto che il reparto ed il prodotto NON sono abilitati all'etichettatura OLOGRAMMI
//non vengono dati all'utente, ma semplicemente si esce dalla funzione ...


if not isnull(fl_barcode) and fl_barcode>0 then
	
	//si tratta sempre di ordini tipo report 1 quindi leggo le info dalla det_ordini_produzione
	select anno_registrazione,
			 num_registrazione,
			 prog_riga_ord_ven,
			 cod_reparto
	into	:fi_anno_registrazione,
			:fl_num_registrazione,
			:fl_prog_riga_ord_ven,
			:ls_cod_reparto
	from   det_ordini_produzione
	where		cod_azienda=:s_cs_xx.cod_azienda and
				progr_det_produzione =:fl_barcode;
	
	if sqlca.sqlcode < 0 then 
		fs_messaggio = "Errore lettura info riga ordine per barcode "+string(fl_barcode)+" :" + sqlca.sqlerrtext
		return -1
	
	elseif sqlca.sqlcode = 100 then
		fs_messaggio = "Nessuna corrispondenza tra le righe ordini vendita per il  barcode "+string(fl_barcode)+"!"
		return -1
		
	end if
	
	//verifico reparto
	select flag_ologramma
	into :ls_flag_ologramma
	from anag_reparti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_reparto=:ls_cod_reparto;
	
	if sqlca.sqlcode < 0 then 
		fs_messaggio = "Errore in controllo abilitazione ologrammi per il reparto associato alla riga ordine "+&
								string(fi_anno_registrazione)+"/"+string(fl_num_registrazione)+"/"+string(fl_prog_riga_ord_ven)+" : " + sqlca.sqlerrtext
		return -1
	
	elseif sqlca.sqlcode = 100 then
		fs_messaggio = "Reparto non trovato (barcode "+string(fl_barcode)+")"
		return -1
	end if
	
	if ls_flag_ologramma="S" then
	else
		if fb_check then
			fs_messaggio = "Il reparto di produzione "+ls_cod_reparto+" NON è abilitato all'etichettatura Ologrammi!"
			return -1
		else
			return -2
		end if
	end if
	
else
	//verifico se almeno un reparto della riga ordine ammette l'ologramma
	select count(*)
	into :ll_count
	from det_ordini_produzione
	join anag_reparti on 	anag_reparti.cod_azienda=det_ordini_produzione.cod_azienda and
								anag_reparti.cod_reparto=det_ordini_produzione.cod_reparto
	where 	det_ordini_produzione.cod_azienda=:s_cs_xx.cod_azienda and
				det_ordini_produzione.anno_registrazione=:fi_anno_registrazione and
				det_ordini_produzione.num_registrazione=:fl_num_registrazione and
				det_ordini_produzione.prog_riga_ord_ven=:fl_prog_riga_ord_ven and
				anag_reparti.flag_ologramma='S';
	
	if sqlca.sqlcode<0 then
		fs_messaggio = "Errore in controllo abilitazione ologrammi per i reparti associati alla riga ordine "+&
								string(fi_anno_registrazione)+"/"+string(fl_num_registrazione)+"/"+string(fl_prog_riga_ord_ven)+" : " + sqlca.sqlerrtext
		return -1
	end if
	
	if ll_count>0 then
	else
		if fb_check then
			fs_messaggio = "Non ci sono reparti di produzione abilitati all'etichettatura Ologrammi per la riga ordine "+&
									string(fi_anno_registrazione)+"/"+string(fl_num_registrazione)+"/"+string(fl_prog_riga_ord_ven)+" : " + sqlca.sqlerrtext
			return -1
		else
			return -2
		end if
	end if
				
end if

//recupero prodotto e quantità ordine --------------------------------------------------------------
select cod_prodotto
into :ls_cod_prodotto
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_registrazione and
			num_registrazione=:fl_num_registrazione and
			prog_riga_ord_ven=:fl_prog_riga_ord_ven;

if sqlca.sqlcode<0 then
	fs_messaggio = "Errore lettura prodotto e quantità riga ordine: "+sqlca.sqlerrtext
	return -1
end if

//verifico tabella flags configuratore
setnull(ls_flag_ologramma)

select flag_scritta
into :ls_flag_ologramma
from tab_flags_configuratore
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_modello=:ls_cod_prodotto;

if sqlca.sqlcode<0 then
	fs_messaggio = "Errore lettura tabella tab_flags_configuratore: "+sqlca.sqlerrtext
	return -1
end if

if ls_flag_ologramma="S" then
else
	if fb_check then
		fs_messaggio = "Il prodotto "+ls_cod_prodotto+" NON è abilitato all'etichettatura Ologrammi!"
		return -1
	else
		return -2
	end if
end if


//se arrivi fin qui vuol dire che puoi procedere all'assegnazione/verifica/visualizzazione etichettatura Ologrammi

//leggi se c'è stata una prima assegnazione
select count(*)
into :ll_count
from det_ord_ven_ologramma
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_registrazione and
			num_registrazione=:fl_num_registrazione and
			prog_riga_ord_ven=:fl_prog_riga_ord_ven;

if sqlca.sqlcode<0 then
	fs_messaggio = "Errore in conteggio ologrammi assegnati per la riga ordine "+&
						string(fi_anno_registrazione)+"/"+string(fl_num_registrazione)+"/"+string(fl_prog_riga_ord_ven)+" : " + sqlca.sqlerrtext
	return -1
end if

if isnull(ll_count) then ll_count=0


return ll_count
end function

public function integer uof_check_ologramma_altra_riga (string as_codice, integer ai_anno, long al_numero, long al_riga, ref string as_errore);string			ls_sql
datastore	lds_data
long			ll_ret, ll_num_ordine, ll_riga_ordine
integer		li_anno_ordine


ls_sql = 	"select 	anno_registrazione,"+&
						"num_registrazione,"+&
						"prog_riga_ord_ven "+&
			"from det_ord_ven_ologramma "+&
			"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"upper(cod_ologramma)='"+upper(as_codice)+"' and "+&
					"cast(anno_registrazione as varchar(4)) + right('000000' + cast(num_registrazione as varchar(6)) , 6) + right('0000' + cast(prog_riga_ord_ven as varchar(4)) , 4) <> "+&
							"'" + string(ai_anno) + "'+" + "right('000000'+'"+ string(al_numero)+"' , 6) + right('0000'+'"+string(al_riga)+"' , 4) "+&
			"order by anno_registrazione asc, num_registrazione asc, prog_riga_ord_ven asc "

ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, sqlca, as_errore)
if ll_ret<0 then
	return -1
end if

if ll_ret>0 then
	//codice già assegnato ad altra riga ordine
	//leggo la prima, che dovrebbe essere anche la prima temporalmente (n° ordine più basso)
	li_anno_ordine = lds_data.getitemnumber(1, 1)
	ll_num_ordine = lds_data.getitemnumber(1, 2)
	ll_riga_ordine = lds_data.getitemnumber(1, 3)
	
	if g_mb.confirm("Codice "+as_codice+" già assegnato alla riga ordine "+string(li_anno_ordine)+"/"+string(ll_num_ordine)+"/"+string(ll_riga_ordine)+"!" +&
						" Si tratta di un'ordine di riparazione?") then
		//l'operatore dice che si tratta di una riparazione ...
		return 1
	else
		//no riparazione
		
		as_errore = "Impossibile assegnare questo codice ologramma perchè già assegnato all'ordine n° "+string(li_anno_ordine)+"/"+string(ll_num_ordine)+"/"+string(ll_riga_ordine)
		return 2
	end if
	
else
	//codice non ancora assegnato
	return 0
end if
end function

public function integer uof_check_ologramma_riga (string as_codice, integer ai_anno, long al_numero, long al_riga, ref string as_errore);string			ls_flag_verificato
datastore	lds_data
long			ll_ret, ll_num_ordine, ll_riga_ordine
integer		li_anno_ordine

select flag_verificato
into :ls_flag_verificato
from det_ord_ven_ologramma
where 	cod_azienda=:s_cs_xx.cod_azienda and
			upper(cod_ologramma)=upper(:as_codice) and
			anno_registrazione=:ai_anno and num_registrazione=:al_numero and prog_riga_ord_ven=:al_riga;

if sqlca.sqlcode<0 then
	as_errore = "Errore in controllo associazione ologramma '"+as_codice+"' su riga ordine: "+string(ai_anno)+"/"+string(al_numero)+"/"+string(al_riga)+ " :"+sqlca.sqlerrtext
	return -1
end if

if sqlca.sqlcode=100 then
	//codice NON presente nella riga ordine
	
	//verifica se il codice è assegnato ad altra riga ordine
	ll_ret = uof_check_ologramma_altra_riga(as_codice, ai_anno, al_numero, al_riga, as_errore)
	//POSSIBILI VALORI DI RITORNO ...............
	//se -1 è un errore critico
	//se li_ret è 1, allora vuol dire che l'operatore dice che si tratta di una riparazione, ok va avanti ma segna che è riparazione ...
	//se vale 2 allora si cerca di assegnare un codice già assegnato ma non è una riparazione: impediscilo
	//se invece vale 0 allora tutto normale
	
	
	if ll_ret = 0 then
		//tutto OK: NUOVO CODICE: assegna previa conferma dell'operatore
		
		if g_mb.confirm("Nuovo numero ologramma: "+as_codice+" - Procedo?", 2) then
			//conferma
			return 3
		else
			//nuovo codice annullato
			as_errore = "Nuovo numero ologramma annullato dall'operatore!"
			return 2
		end if
		
	else
		//tutti gli altri casi torna il rispettivo valore (-1:errore critico   1:riparazione confermata    2:codice già assegnato ma non riparazione: impedisci)
		return ll_ret
	end if
	

elseif sqlca.sqlcode=0 then
	//codice presente nella riga ordine
	
	if ls_flag_verificato="S" then
		as_errore = "Il codice '"+as_codice+"' è stato già verificato per la riga ordine "+string(ai_anno)+"/"+string(al_numero)+"/"+string(al_riga)
		return 2
	else
		//tutto OK, verifica il codice
		return 0
	end if
	
end if
end function

public function integer uof_ass_iniziale_ologramma (boolean fb_check, long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_operaio, ref string fs_errore);integer					li_risposta
dec{4}					ld_quan_ordine
s_cs_xx_parametri	lstr_parametri
string						ls_cod_tipo_ord_ven,ls_tipo_stampa, ls_cod_reparto


//se anno/numero/riga arriva vuoto recuperali
if isnull(fi_anno_registrazione) or fi_anno_registrazione<=0 then
	
	if uof_barcode_anno_reg(fl_barcode, fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ls_cod_reparto, fs_errore) < 0 then
		fs_errore = "Errore lettura anno/numero/riga ordine per barcode "+string(fl_barcode)+": " + sqlca.sqlerrtext
		return -1
	end if
	
end if

//solo per ordini tipo A
select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione;

select flag_tipo_bcl
into   :ls_tipo_stampa
from   tab_tipi_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;

if ls_tipo_stampa<>"A" then
	fs_errore = "Assegnazione Ologrammi NON prevista per questa tipologia ordine!"
	return 1
end if


//#############################################################
//Donato 08/10/2012 controlla se devo fare l'assegnazione ologrammi ---
li_risposta = uof_controlla_ologrammi(fb_check, fl_barcode, fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, fs_errore)
if li_risposta = -1 then
	//in fs_errore il messaggio
	return -1
	
elseif li_risposta=-2 then
	//assegnazione non prevista, vai avanti
	fs_errore = "Assegnazione Ologrammi NON prevista!"
	return 1
	
elseif li_risposta > 0 then
	//assegnazione ologrammi prevista ma è stata già effettuata la prima assegnazione
	fs_errore = "Assegnazione Iniziale Ologrammi GIA' effettuata in precedenza!"
	return 1
	
else
	//assegnazione ologrammi prevista e prima assegnazione NON ancora effettuata
	
	select quan_ordine
	into :ld_quan_ordine
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fi_anno_registrazione and
				num_registrazione=:fl_num_registrazione and
				prog_riga_ord_ven=:fl_prog_riga_ord_ven;
	
	
	lstr_parametri.parametro_s_1 = "A"
	lstr_parametri.parametro_s_2 = "O"
	lstr_parametri.parametro_s_3 = fs_cod_operaio
	lstr_parametri.parametro_d_1_a[1] = fi_anno_registrazione
	lstr_parametri.parametro_d_1_a[2] = fl_num_registrazione
	lstr_parametri.parametro_d_1_a[3] = fl_prog_riga_ord_ven
	
	//devo assegnare un numero di ologrammi pari alla quantità ordine
	lstr_parametri.parametro_d_1_a[4] = ld_quan_ordine
	
	openwithparm(w_gestione_ologrammi, lstr_parametri)
	
	lstr_parametri = message.powerobjectparm
	
	if lstr_parametri.parametro_b_1 then
		return 0
	else
		//probabile operazione annullata
		return 2
	end if
end if
//#############################################################
end function

public function integer uof_get_riga_ordine_da_ologramma (string as_ologramma, ref integer ai_anno_ordine[], ref long al_num_ordine[], ref long al_riga_ordine[], ref string as_errore);datastore			lds_data
string					ls_sql
integer				li_count, li_index, ll_new, li_anno_ds, ll_index2
long					ll_num_ds, ll_riga_ds
boolean				lb_trovata

//ordinati per numerazione discendente, prima (si presume) le eventuali riparazioni, poi la riga d'ordine principale -------------------
ls_sql = 	"select distinct anno_registrazione, num_registrazione, prog_riga_ord_ven "+&
			"from det_ord_ven_ologramma "+&
			"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"upper(cod_ologramma)='"+upper(as_ologramma)+"' "+&
			"order by anno_registrazione desc, num_registrazione desc "
			
li_count = guo_functions.uof_crea_datastore( lds_data, ls_sql, as_errore)
if li_count<0 then
	//in as_errore il messaggio eventuale
	return -1
	
elseif li_count =0 then
	as_errore = "Nessuna riga di ordine corrisponde al codice ologramma '"+as_ologramma+"'"
	return 0
end if

ll_new = 0

for li_index=1 to li_count
	//lb_trovata = false
	//se nell'array la riga ordine è già caricata (caso in cui viene passata la variabile byref già riempita
	//			es: dalla stampa di + schede prodotto collegate da stesso numero ologramma)
	//allora non inserirla:
	
	//leggo la riga ordine
	li_anno_ds = lds_data.getitemnumber(li_index, 1)
	ll_num_ds = lds_data.getitemnumber(li_index, 2)
	ll_riga_ds = lds_data.getitemnumber(li_index, 3)
	
//	//controllo se per caso è già presente nell'array
//	for ll_index2=1 to upperbound(ai_anno_ordine[])
//		if 	ai_anno_ordine[ll_index2] = li_anno_ds and &
//			al_num_ordine[ll_index2] = ll_num_ds and &
//			al_riga_ordine[ll_index2] = ll_riga_ds				then
//			
//				lb_trovata = true
//				exit
//				
//		end if
//	next
	
	//if not lb_trovata then
		ll_new += 1
		ai_anno_ordine[ll_new] 	= li_anno_ds
		al_num_ordine[ll_new] 	= ll_num_ds
		al_riga_ordine[ll_new] 	= ll_riga_ds
		
	//else
		//continue
	//end if
	
next

return ll_new
end function

public function integer uof_stampa_scheda_prodotto (string fs_ologramma, integer fi_anno_registrazione, long fl_num_registrazione, integer fl_prog_riga_ord_ven, ref string fs_errore);
string					ls_cod_cliente, ls_rag_soc_1_div,ls_rag_soc_2_div,  ls_indirizzo_div, ls_localita_div, ls_frazione_div, ls_cap_div, ls_provincia_div, ls_cod_giro_consegna, &
						ls_alias_cliente, ls_num_ord_cliente,ls_rif_interscambio, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_piva, ls_cf, &
						ls_cod_nazione, ls_piva_cf, ls_cod_prodotto, ls_des_prodotto, ls_cod_comando, ls_cod_tessuto, ls_cod_verniciatura, ls_des_comando, ls_des_tessuto, &
						ls_des_verniciatura, ls_cod_verniciatura_2, ls_cod_verniciatura_3, ls_des_verniciatura_2, ls_des_verniciatura_3, ls_data_fattura, ls_data_ddt, ls_flag_misura_luce_finita,&
						ls_cod_non_a_magazzino, ls_cod_lingua, ls_des_breve_tessuto, ls_logo_etichetta

long					ll_num_colli_testata, ll_riga, ll_num_max_tenda, ll_num_tenda, ll_prog_riga_ord_ven_cfr, ll_num_reg_v[], ll_riga_ord_ven_v[]

integer				 li_anno_reg_v[], ll_index

datetime				ldt_data_consegna, ldt_data_fattura, ldt_data_ddt

dec{4}				ld_quan_ordine, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t, ld_val_gtot

boolean				fb_stampa_da_ologramma, lb_RIE=false


s_cs_xx.parametri.parametro_ds_1 = create datastore

//NOTA: solo per report tipo A
s_cs_xx.parametri.parametro_ds_1.dataobject = "d_label_scheda_prodotto"
s_cs_xx.parametri.parametro_s_1 = "A"
fb_stampa_da_ologramma = false
ls_data_fattura = ""
ls_data_ddt = ""

if not isnull(fl_num_registrazione) and fl_num_registrazione>0 then
	li_anno_reg_v[1] = fi_anno_registrazione
	ll_num_reg_v[1] = fl_num_registrazione
	ll_riga_ord_ven_v[1] = fl_prog_riga_ord_ven
	
else
	
	if fs_ologramma<>"" and not isnull(fs_ologramma) then
		
		if pos(fs_ologramma, "*") > 0 then
			fs_errore = "Non è possibile stampare Schede Prodotto con Codici Ologramma contenti l'asterisco!"
			return -1
		end if
		
		
		//carico gli altri ordini nell'array
		ll_index = uof_get_riga_ordine_da_ologramma(fs_ologramma, li_anno_reg_v[], ll_num_reg_v[], ll_riga_ord_ven_v[], fs_errore)
		
		if ll_index<=0 then
			//in fs_errore il messaggio
			return -1
		end if
		
		//ALTRIMENTI tutto OK, esegui la stampa elaborando l'array delle righe ordine
		fb_stampa_da_ologramma = true
	end if
	
end if


guo_functions.uof_get_parametro("RIE", lb_RIE)
if isnull(lb_RIE) then lb_RIE = false

for ll_index=1 to upperbound(li_anno_reg_v[])

	select cod_cliente,   
			 rag_soc_1,   
			 rag_soc_2,   
			 indirizzo,   
			 localita,   
			 frazione,   
			 cap,   
			 provincia,   
			 cod_giro_consegna,   
			 alias_cliente,   
			 num_colli,
			 num_ord_cliente,
			 rif_interscambio,
			 data_consegna,
			 logo_etichetta
	 into   :ls_cod_cliente,   
			:ls_rag_soc_1_div,   
			:ls_rag_soc_2_div,   
			:ls_indirizzo_div,   
			:ls_localita_div,   
			:ls_frazione_div,   
			:ls_cap_div,   
			:ls_provincia_div,   
			:ls_cod_giro_consegna,   
			:ls_alias_cliente,   
			:ll_num_colli_testata,
			:ls_num_ord_cliente,
			:ls_rif_interscambio,
			:ldt_data_consegna,
			:ls_logo_etichetta
	from  tes_ord_ven 
	where cod_azienda=:s_cs_xx.cod_azienda
	and   anno_registrazione=:li_anno_reg_v[ll_index]
	and   num_registrazione=:ll_num_reg_v[ll_index];
	 
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ls_rag_soc_1_div) then ls_rag_soc_1_div = ""
	if isnull(ls_rag_soc_2_div) then ls_rag_soc_2_div = ""
	if isnull(ls_indirizzo_div) then ls_indirizzo_div = ""
	if isnull(ls_localita_div) then ls_localita_div = ""
	if isnull(ls_frazione_div) then ls_frazione_div = ""
	if isnull(ls_cap_div) then ls_cap_div = ""
	if isnull(ls_provincia_div) then ls_provincia_div = ""
	if isnull(ls_cod_giro_consegna) then ls_cod_giro_consegna = ""
	if isnull(ls_alias_cliente) then ls_alias_cliente = ""
	if isnull(ll_num_colli_testata) then ll_num_colli_testata =0
	if isnull(ls_num_ord_cliente) then ls_num_ord_cliente = ""
	if isnull(ls_rif_interscambio) then ls_rif_interscambio = ""
		
	select rag_soc_1,
			 rag_soc_2,
			 indirizzo,   
			 localita,   
			 frazione,   
			 cap,   
			 provincia,
			 partita_iva,
			 cod_fiscale,
			 cod_nazione,
			 cod_lingua
	into   :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,
			 :ls_piva,
			 :ls_cf,
			 :ls_cod_nazione,
			 :ls_cod_lingua
	from   anag_clienti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_cliente=:ls_cod_cliente; 
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	
	if isnull(ls_rag_soc_1) then ls_rag_soc_1 =""
	if isnull(ls_rag_soc_2) then ls_rag_soc_2 =""
	if isnull(ls_indirizzo) then ls_indirizzo =""
	if isnull(ls_localita) then ls_localita =""
	if isnull(ls_frazione) then ls_frazione =""
	if isnull(ls_cap) then ls_cap =""
	if isnull(ls_provincia) then ls_provincia =""
	
	if isnull(ls_cod_nazione) then ls_cod_nazione =""
	if ls_cod_nazione<>"" then ls_cod_nazione = "(" + ls_cod_nazione + ")"
	
	ls_piva_cf = ""
	
	//se c'è la partita iva visualizza quella altrimenti prova con il codice fiscale
	if ls_piva<>"" and not isnull(ls_piva) then
		ls_piva_cf = "P.I.  " + ls_piva
	elseif ls_cf<>"" and not isnull(ls_cf) then
		ls_piva_cf = "C.F.  "+ ls_cf
	end if
	
	

	//faccio insert row
	ll_riga = s_cs_xx.parametri.parametro_ds_1.insertrow(0)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"anno_registrazione",string(li_anno_reg_v[ll_index]))
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"num_registrazione",string(ll_num_reg_v[ll_index]))
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"prog_riga_ord_ven",ll_riga_ord_ven_v[ll_index])
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"cod_cliente",ls_cod_cliente)
	
	
	if lb_RIE and g_str.isnotempty(ls_rif_interscambio) then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rag_soc",ls_rif_interscambio)
	else
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"rag_soc",ls_rag_soc_1)
	end if
	
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_1",ls_indirizzo)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"indirizzo_2",ls_cap+" "+ls_frazione+" "+ls_localita+" "+ ls_provincia + " " + ls_cod_nazione)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"piva_cf", ls_piva_cf)
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"logo_etichetta", ls_logo_etichetta)
	
	if ls_cod_lingua<>"" and not isnull(ls_cod_lingua) then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"lingua", "(" + ls_cod_lingua + ")")
	else
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"lingua", "")
	end if
	
	//recupero info riga N/M in etichetta --------------------------------------------
	select count(*)
	into   :ll_num_max_tenda
	from   det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_reg_v[ll_index]
	and    num_registrazione=:ll_num_reg_v[ll_index]
	and    num_riga_appartenenza=0
	and    flag_blocco='N';
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	declare  righe_det_ord_ven_1 cursor for
	select   prog_riga_ord_ven
	from     det_ord_ven
	where    cod_azienda = :s_cs_xx.cod_azienda and      
				anno_registrazione = :li_anno_reg_v[ll_index] and      
				num_registrazione = :ll_num_reg_v[ll_index] and      
				num_riga_appartenenza = 0 and      
				flag_blocco = 'N'
	order by prog_riga_ord_ven;
	
	open righe_det_ord_ven_1;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore apertura cursore: " + sqlca.sqlerrtext
		return -1
	end if
	
	ll_num_tenda = 0
	do while 1 = 1			
		
		fetch righe_det_ord_ven_1 into  :ll_prog_riga_ord_ven_cfr;
				
		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			close righe_det_ord_ven_1;
			return -1
		end if
		
		ll_num_tenda += 1				
	
		if ll_riga_ord_ven_v[ll_index] = ll_prog_riga_ord_ven_cfr then exit
		
	loop
	close righe_det_ord_ven_1;
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"num_riga_num_tot", string(string(ll_num_tenda)) +"/" + string(ll_num_max_tenda))
	
	select  cod_prodotto,
			 quan_ordine,
			 des_prodotto
	into     :ls_cod_prodotto,   
			 :ld_quan_ordine,
			 :ls_des_prodotto
	from   det_ord_ven
	where  cod_azienda =: s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_reg_v[ll_index] and
			 num_registrazione  = :ll_num_reg_v[ll_index] and
			 prog_riga_ord_ven  = :ll_riga_ord_ven_v[ll_index];
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
			
	select dim_x,   
			dim_y,   
			dim_z,   
			dim_t,
			cod_comando,
			cod_tessuto,
			cod_verniciatura,
			cod_verniciatura_2,
			cod_verniciatura_3,
			flag_misura_luce_finita,
			cod_non_a_magazzino
	into   	:ld_dim_x,   
			:ld_dim_y,   
			:ld_dim_z,   
			:ld_dim_t,
			:ls_cod_comando,
			:ls_cod_tessuto,
			:ls_cod_verniciatura,
			:ls_cod_verniciatura_2,
			:ls_cod_verniciatura_3,
			:ls_flag_misura_luce_finita,
			:ls_cod_non_a_magazzino
	 from  comp_det_ord_ven 
	 where cod_azienda=:s_cs_xx.cod_azienda
	 and   anno_registrazione=:li_anno_reg_v[ll_index]
	 and   num_registrazione=:ll_num_reg_v[ll_index]
	 and   prog_riga_ord_ven=:ll_riga_ord_ven_v[ll_index];
	
	 if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	 end if
	
	//se la descrizione del prodotto in det_ord_ven è vuota, leggila da anag_prodotti
	if isnull(ls_des_prodotto) or ls_des_prodotto="" then
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then 
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
	end if
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"data_consegna",uof_trasforma_data(date(ldt_data_consegna)))
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"des_prodotto_mod",ls_des_prodotto)
	
	if ld_dim_x <> 0 then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_x",string(round(ld_dim_x,2)))
	end if
	
	if ld_dim_y <> 0 then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_y",string(round(ld_dim_y,2)))
	end if
	
	if ld_dim_z <> 0 then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_z",string(round(ld_dim_z,2)))
	end if
	
	if ld_dim_t <> 0 then
		s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"dim_t",string(round(ld_dim_t,2)))
	end if
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"quan_ordine",string(ld_quan_ordine))
	
	//campi COMANDO, Tipo Tessuto, COLORE in Etichetta Scheda Prodotto
	if ls_cod_comando<>"" and not isnull(ls_cod_comando) then
		//prendo la des. abbreviata in tabella tab_comandi
		//ls_des_comando = f_des_tabella ( "tab_comandi", "cod_comando = '" +  ls_cod_comando + "'", "abbreviazione_comando" )
		ls_des_comando = f_des_tabella ( "anag_prodotti", "cod_prodotto = '" +  ls_cod_comando + "'", "des_prodotto" )
	end if

	if ls_cod_tessuto<>"" and not isnull(ls_cod_tessuto) then
		ls_des_tessuto = f_des_tabella ( "anag_prodotti", "cod_prodotto = '" +  ls_cod_tessuto + "'", "des_prodotto" )
	end if
	
	if ls_cod_verniciatura<>"" and not isnull(ls_cod_verniciatura) then
		ls_des_verniciatura = f_des_tabella ( "anag_prodotti", "cod_prodotto = '" +  ls_cod_verniciatura + "'", "des_prodotto" )
	else
		ls_des_verniciatura = ""
	end if
	
	if ls_cod_verniciatura_2<>"" and not isnull(ls_cod_verniciatura_2) then
		ls_des_verniciatura_2 = f_des_tabella ( "anag_prodotti", "cod_prodotto = '" +  ls_cod_verniciatura_2 + "'", "des_prodotto" )
	else
		ls_des_verniciatura_2 = ""
	end if
	
	if ls_cod_verniciatura_3<>"" and not isnull(ls_cod_verniciatura_3) then
		ls_des_verniciatura_3 = f_des_tabella ( "anag_prodotti", "cod_prodotto = '" +  ls_cod_verniciatura_3 + "'", "des_prodotto" )
	else
		ls_des_verniciatura_3 = ""
	end if
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"comando", ls_des_comando)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"tipo_tessuto", ls_des_tessuto)
	
	ls_des_verniciatura = ls_des_verniciatura + " / " + ls_des_verniciatura_2 + " / " + ls_des_verniciatura_3
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"colore", ls_des_verniciatura)
	//---------------------------------------------------------------------------------
	
	//identifica il numero di riga: importante uando stampo più righe (righe collegate dallo stesso numero ologramma)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"numero", ll_riga)
	
	//-------------------------------------------------------------------------------------------------
	if ls_cod_non_a_magazzino<>"" and not isnull(ls_cod_non_a_magazzino) then
	//visualizza val_gtot
		select des_breve_tessuto, val_gtot
		into :ls_des_breve_tessuto, :ld_val_gtot
		from tab_colori_tessuti
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_colore_tessuto=:ls_cod_non_a_magazzino;
		
		if isnull(ld_val_gtot) then ld_val_gtot = 0
		
	else
		ls_des_breve_tessuto = ""
		ld_val_gtot = 0
	end if
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"val_gtot", ld_val_gtot)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"des_breve_tessuto", ls_des_breve_tessuto)
	//---------------------------------------------------------------------------------------------------
	
	//stampa data fattura e data ddt --------------------------------------------------------------------
	if fb_stampa_da_ologramma then
		uof_get_data_fatt_e_ddt_riga_ordine(li_anno_reg_v[ll_index], ll_num_reg_v[ll_index], ll_riga_ord_ven_v[ll_index], ldt_data_fattura, ldt_data_ddt)
		
		if not isnull(ldt_data_fattura) and year(date(ldt_data_fattura))>1950 then
			ls_data_fattura = "Data Fattura:~r~n" + string(ldt_data_fattura, "dd/mm/yyyy")
		end if
		if not isnull(ldt_data_ddt) and year(date(ldt_data_ddt))>1950 then
			ls_data_ddt = "Data D.d.T.:~r~n" + string(ldt_data_ddt, "dd/mm/yyyy")
		end if
	end if
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"data_fattura", ls_data_fattura)
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"data_ddt", ls_data_ddt)
	//--------------------------------------------------------------------------------------------------------
	
	s_cs_xx.parametri.parametro_ds_1.setitem(ll_riga,"flag_misura_luce_finita", ls_flag_misura_luce_finita)
	
next

if fb_stampa_da_ologramma then
	//mi serve per segnalare alla finestra w_stampa_etichette_schedaprodotto che provengo da codice ologramma
	s_cs_xx.parametri.parametro_d_9 = 1
else
	s_cs_xx.parametri.parametro_d_9 = 0
end if
	
//apertura finestra di stampa scheda prodotto
window_open(w_stampa_etichette_schedaprodotto, 0)

if s_cs_xx.parametri.parametro_i_1 = 0 then 
	fs_errore = s_cs_xx.parametri.parametro_s_2
	return -1
end if


return 1
end function

public subroutine uof_get_data_fatt_e_ddt_riga_ordine (integer fi_anno, long fl_numero, long fl_riga, ref datetime fdt_data_fatt, ref datetime fdt_data_ddt);
select max(isnull(tes_fat_ven.data_fattura, tes_fat_ven.data_registrazione))
into :fdt_data_fatt
from tes_fat_ven
join det_fat_ven on 	det_fat_ven.cod_azienda=tes_fat_ven.cod_azienda and
							det_fat_ven.anno_registrazione=tes_fat_ven.anno_registrazione and
							det_fat_ven.num_registrazione=tes_fat_ven.num_registrazione
join det_ord_ven on 	det_ord_ven.cod_azienda=det_fat_ven.cod_azienda and
							det_ord_ven.anno_registrazione=det_fat_ven.anno_reg_ord_ven and
							det_ord_ven.num_registrazione=det_fat_ven.num_reg_ord_ven and
							det_ord_ven.prog_riga_ord_ven=det_fat_ven.prog_riga_ord_ven
where 	tes_fat_ven.cod_azienda=:s_cs_xx.cod_azienda and
			det_fat_ven.anno_reg_ord_ven = :fi_anno and
			det_fat_ven.num_reg_ord_ven = :fl_numero and
			det_fat_ven.prog_riga_ord_ven = :fl_riga and
			tes_fat_ven.cod_tipo_fat_ven not in (	select cod_tipo_fat_ven
															from tab_tipi_fat_ven
															where 	cod_azienda=:s_cs_xx.cod_azienda and
																		flag_tipo_fat_ven in ('N', 'P', 'F')
															);

select max(isnull(tes_bol_ven.data_bolla, tes_bol_ven.data_registrazione))
into :fdt_data_ddt
from tes_bol_ven
join det_bol_ven on 	det_bol_ven.cod_azienda=tes_bol_ven.cod_azienda and
							det_bol_ven.anno_registrazione=tes_bol_ven.anno_registrazione and
							det_bol_ven.num_registrazione=tes_bol_ven.num_registrazione
join det_ord_ven on 	det_ord_ven.cod_azienda=det_bol_ven.cod_azienda and
							det_ord_ven.anno_registrazione=det_bol_ven.anno_registrazione_ord_ven and
							det_ord_ven.num_registrazione=det_bol_ven.num_registrazione_ord_ven and
							det_ord_ven.prog_riga_ord_ven=det_bol_ven.prog_riga_ord_ven
where 	tes_bol_ven.cod_azienda=:s_cs_xx.cod_azienda and
			det_bol_ven.anno_registrazione_ord_ven = :fi_anno and
			det_bol_ven.num_registrazione_ord_ven = :fl_numero and
			det_bol_ven.prog_riga_ord_ven = :fl_riga and
			tes_bol_ven.cod_tipo_bol_ven not in (	select cod_tipo_bol_ven
															from tab_tipi_bol_ven
															where 	cod_azienda=:s_cs_xx.cod_azienda and
																		flag_tipo_bol_ven in ('R', 'T', 'C')
															);
			

return
end subroutine

public function integer uof_get_colli_reparti_prec (long al_barcode, ref string as_reparto_corrente, ref datastore ads_colli, ref string as_errore);long ll_num_registrazione, ll_prog_riga_ord_ven
integer li_anno_registrazione, li_ret
string ls_tipo, ls_sql, ls_colonna, ls_where_ordine, ls_cod_deposito_corrente


//recupero anno, numero e riga ordine
select anno_registrazione, num_registrazione, prog_riga_ord_ven
into :li_anno_registrazione, :ll_num_registrazione, :ll_prog_riga_ord_ven
from det_ordini_produzione
where 	cod_azienda=:s_cs_xx.cod_azienda and
			progr_det_produzione=:al_barcode;

if sqlca.sqlcode<0 then
	as_errore = "uof_get_colli_reparti_prec: Errore in lettura det_ordini_produzione: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode = 0 then
	//è un ordine tipo report 1
	ls_tipo = "A"
	ls_colonna = "progr_det_produzione"
	ls_where_ordine = " a.anno_registrazione="+string(li_anno_registrazione)+" and a.num_registrazione="+string(ll_num_registrazione)+" and a.prog_riga_ord_ven="+string(ll_prog_riga_ord_ven)+" "
	
	//leggo il reparto corrente
	select cod_reparto
	into :as_reparto_corrente
	from det_ordini_produzione
	where cod_azienda=:s_cs_xx.cod_azienda and progr_det_produzione=:al_barcode;
	
	
else
	//caso non gestito: Beatrice ed Alberto mi hanno confermato che il reset colli di stabilimenti precedenti diversi non
	//si ha nel caso di tende tecniche o sfuso, quindi commento tutto
	return 0
	
//	//cerca nella tes_ordini_produzione
//	setnull(ll_prog_riga_ord_ven)
//	
//	select anno_registrazione, num_registrazione
//	into :li_anno_registrazione, :ll_num_registrazione
//	from tes_ordini_produzione
//	where 	cod_azienda=:s_cs_xx.cod_azienda and
//				progr_tes_produzione=:al_barcode;
//	
//	if sqlca.sqlcode<0 then
//		as_errore = "uof_get_colli_reparti_prec: Errore in lettura tes_ordini_produzione: "+sqlca.sqlerrtext
//		return -1
//		
//	elseif sqlca.sqlcode = 0 then
//		//è un ordine tipo report 2 o 3
//		ls_tipo = "BC"
//		ls_colonna = "progr_tes_produzione"
//		ls_where_ordine = " a.anno_registrazione="+string(li_anno_registrazione)+" and a.num_registrazione="+string(ll_num_registrazione)+" "
//		
//		//leggo il reparto corrente
//		select cod_reparto
//		into :as_reparto_corrente
//		from tes_ordini_produzione
//		where cod_azienda=:s_cs_xx.cod_azienda and progr_tes_produzione=:al_barcode;
//		
//		
//	else
//		//trovato niente per questo barcode
//		as_errore = "uof_get_colli_reparti_prec: nessun ordine in corrispondenza di questo barcode: "+sqlca.sqlerrtext
//		return -1
//	end if
	
end if

//leggo il deposito del reparto corrente
select cod_deposito
into :ls_cod_deposito_corrente
from anag_reparti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_reparto=:as_reparto_corrente;

ls_sql = "select c.cod_azienda, c.barcode, c.progressivo, c.cod_reparto, c.data_creazione, c.ora_creazione "+&
			"from det_ordini_produzione as a "+&
			"join tab_ord_ven_colli as c on c.cod_azienda=a.cod_azienda and "+&
											"c.barcode=a." +ls_colonna+ " " + &
			"join anag_reparti as r on r.cod_azienda=c.cod_azienda and "+&
											"r.cod_reparto=c.cod_reparto "+&
			"where a.cod_azienda='"+s_cs_xx.cod_azienda+"' and r.cod_deposito <> '"+ls_cod_deposito_corrente+"' and "+&
					"a.flag_fine_fase='S' and  a."+ls_colonna+" < "+string(al_barcode)+" and "+ls_where_ordine
					
li_ret = guo_functions.uof_crea_datastore(ads_colli, ls_sql, as_errore)

return li_ret

//in li_ret il totale colli dei reparti precedenti che hanno già chiuso
//in ads_colli il set di dati della tabella tab_ord_ven_colli
//----------------------------------------------------------------------------------------------------
//cod_azienda | barcode | progressivo | cod_reparto | data_creazione | ora_creazione











end function

public function integer uof_elimina_colli_reparti_prec (datastore ads_colli, ref string as_errore);long	ll_barcode, ll_progressivo, ll_index
string ls_collo


//in ads_colli il set di dati della tabella tab_ord_ven_colli
//----------------------------------------------------------------------------------------------------
//       1                2                 3                  4                    5                        6
//cod_azienda | barcode | progressivo | cod_reparto | data_creazione | ora_creazione

for ll_index=1 to ads_colli.rowcount()
	ll_barcode = ads_colli.getitemnumber(ll_index, 2)
	ll_progressivo = ads_colli.getitemnumber(ll_index, 3)
	
	ls_collo = string(ll_barcode)+"-"+string(ll_progressivo)
	
	delete from tab_ord_ven_colli
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				barcode=:ll_barcode and
				progressivo=:ll_progressivo;
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore in eliminazione collo "+ls_collo + " : "+sqlca.sqlerrtext
		return -1
	end if
	
	//azzero anche il numero colli in sessioni_lavoro rispettive
	update sessioni_lavoro
	set num_colli=0
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				progr_det_produzione=:ll_barcode;
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore in azzeramento colli in sessioni_lavoro: "+ls_collo + " : "+sqlca.sqlerrtext
		return -1
	end if
		
	//log in log_sistema della cancellazione colli, per eventuali controlli di corretta operatività
	f_log_sistema("Cancellazione collo di reparto deposito precedente: "+ls_collo, "cancella_colli_p")
	//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
		

	
next


return 0
end function

public function integer uof_sospensione (string fs_azione, long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga, string fs_cod_operaio, ref string fs_errore);long						ll_progressivo
string						ls_tipo, ls_cod_operaio, ls_cod_tipo_causa_sospensione, ls_sql, ls_temp
s_cs_xx_parametri	lstr_parametri
boolean					lb_insert = false
datetime					ldt_oggi



//fs_azione può assumenre i seguenti valori: "SOSP"  oppure "DESOSP"

//in base al tipo ordine (A, B, C) il barcode letto corrisponde a progr_det_produzione (tipo A) oppure progr_tes_produzione (tipo B, C)
if uof_trova_tipo(fl_barcode, ls_tipo, fs_errore) < 0 then
	return -1
end if

if ls_tipo = "B" or ls_tipo = "C" then
	//caso B, C
	setnull(fl_prog_riga)
end if

//prima di far aprire la finestra di scelta operatore e causa sospensione, verifico se l'operazione è congruente
//esempio ho richiesto di sospendere, e non sono in presenza di sospensioni attive, oppure
//ho richiesto di togliere la sospensione, ma non esistono sospensioni attive!

//leggo il progressivo del record in tabella che risulta sospeso ... (se esiste)

select progressivo
into :ll_progressivo
from tab_sosp_produzione
where 	cod_azienda=:s_cs_xx.cod_azienda and
			barcode=:fl_barcode and
			data_desosp is null;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in controllo sospensioni attive: "+sqlca.sqlerrtext
	return -1
end if

if fs_azione = "SOSP" then
	ls_temp = "Tentativo Sospensione"
	
	//richiesta sospensione, verifico che non ci siano sospensioni ancora attive
	if ll_progressivo>0 then
		fs_errore = "Impossibile procedere: esistono ancora sospensioni attive nel reparto!"
		uof_scrivi_log_sistema("SOSP", ls_temp+" barcode "+string(fl_barcode)+" : "+fs_errore)
		return -1
		
	else
		//OK procedi all'inserimento della sospensione
		
		//recupero allora il progressivo più alto
		setnull(ll_progressivo)
		
		select max(progressivo)
		into :ll_progressivo
		from tab_sosp_produzione
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					barcode=:fl_barcode;
		
		if isnull(ll_progressivo) then ll_progressivo = 0
		ll_progressivo += 1
		
		lb_insert = true
		
	end if
else
	//richiesta DEsospensione, verifico che ci sia la sospensioni attiva da sbloccare
	ls_temp = "Tentativo De-Sospensione"
	if ll_progressivo>0 then
		//OK procederai successivamente all'update con questo progressivo
		lb_insert = false
	else
		fs_errore = "Impossibile procedere: NON esistono sospensioni attive nel reparto!"
		uof_scrivi_log_sistema("SOSP", ls_temp+" barcode "+string(fl_barcode)+" : "+fs_errore)
		return -1
	end if
end if


lstr_parametri.parametro_s_1_a[1] = fs_azione
lstr_parametri.parametro_s_1_a[2] = fs_cod_operaio
openwithparm(w_sospensione_produzione, lstr_parametri)

lstr_parametri = message.powerobjectparm

if lstr_parametri.parametro_b_1 then
	ls_cod_operaio = lstr_parametri.parametro_s_1_a[1]
	ls_cod_tipo_causa_sospensione = lstr_parametri.parametro_s_1_a[2]
	
else
	fs_errore = "Operazione annullata dall'operatore!"
	uof_scrivi_log_sistema("SOSP", ls_temp+" barcode "+string(fl_barcode)+" : "+fs_errore)
	return -1
end if

ldt_oggi = datetime(today(), now())

if lb_insert then
	//inserisco la sospensione
	insert into tab_sosp_produzione
		(	cod_azienda,
			barcode,
			progressivo,
			anno_registrazione,
			num_registrazione,
			prog_riga_ord_ven,
			cod_operaio_sosp,
			data_sospensione,
			cod_tipo_causa_sospensione,
			cod_operaio_desosp,
			data_desosp)
	values	(	:s_cs_xx.cod_azienda,
					:fl_barcode,
					:ll_progressivo,
					:fi_anno_registrazione,
					:fl_num_registrazione,
					:fl_prog_riga,
					:ls_cod_operaio,
					:ldt_oggi,
					:ls_cod_tipo_causa_sospensione,
					null,
					null);
	
else
	
	//sblocco la sospensione
	update tab_sosp_produzione
		set 	cod_operaio_desosp = :ls_cod_operaio,
				data_desosp = :ldt_oggi
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				barcode = :fl_barcode and
				progressivo = :ll_progressivo;
end if


if sqlca.sqlcode < 0 then
	if fs_azione="SOSP" then
		fs_errore = "Errore in fase di sospensione: "+ sqlca.sqlerrtext
	else
		fs_errore = "Errore in fase di sblocco sospensione: "+ sqlca.sqlerrtext
	end if
	uof_scrivi_log_sistema("SOSP", fs_errore)
	
	return -1
end if

if fs_azione="SOSP" then
	uof_scrivi_log_sistema("SOSP", ls_temp+" barcode "+string(fl_barcode)+"(causale "+ls_cod_tipo_causa_sospensione+") effettuata con successo!")
else
	uof_scrivi_log_sistema("SOSP", ls_temp+" barcode "+string(fl_barcode)+" effettuata con successo!")
end if


return 0
end function

public function integer uof_controlla_se_sospesa (long al_barcode, string as_cod_operaio);//questa funzione viene chiamata in fase di chiusura della fase produzione del reparto
//cioè quando la quantità prodotta raggiunge la quantià da produrre
//quindi verifica se ci sono fasi reparto sospese, e se si, chiede all'operatore se vuole procedere al relativo sblocco

integer li_progressivo
datetime ldt_oggi

//se esiste qualcosa di sospeso (cod_operaio_desosp NULL), sarà univoco per progr_det_produzione, in caso contrario c'è qualche errore ...
select progressivo
into :li_progressivo
from tab_sosp_produzione
where 	cod_azienda=:s_cs_xx.cod_azienda and
			barcode=:al_barcode and
			cod_operaio_desosp is null;

if sqlca.sqlcode<0 then
	//segnala l'errore ma non bloccare
	g_mb.warning("Errore in controllo se fase sospesa! Errore non bloccante, la fase sarà comunque chiusa! "+sqlca.sqlerrtext)
	return -1
end if

if li_progressivo>0 then

	if g_mb.confirm( "La fase del reparto risulta sospesa. Vuoi sbloccarla? (La fase verrà in ogni caso chiusa...)", 1) then
		
		ldt_oggi = datetime(today(), now())
		
		update tab_sosp_produzione
			set 	cod_operaio_desosp=:as_cod_operaio,
					data_desosp=:ldt_oggi
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					barcode=:al_barcode and
					progressivo=:li_progressivo;
		
		if sqlca.sqlcode<0 then
			//segnala l'errore ma non bloccare
			g_mb.warning("Errore in sblocco sospensione fase! Errore non bloccante, la fase sarà comunque chiusa! "+sqlca.sqlerrtext)
			return -1
		end if
		
	end if

end if

return 0
end function

public function integer uof_get_sospensioni_reparto (string as_cod_reparto, integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga, ref string as_sigla_sospensione);
//se esiste una sospensione nel reparto, ritorna la sigla della sospensione applicata

if al_prog_riga> 0 then
	//caso ordini tipo report 1
	
	select isnull(a.sigla, '-')
	into :as_sigla_sospensione
	from tab_sosp_produzione as b
	join tab_tipi_cause_sospensione as a on a.cod_azienda=b.cod_azienda and
														 b.cod_tipo_causa_sospensione=a.cod_tipo_causa_sospensione
	join det_ordini_produzione as d on d.cod_azienda=b.cod_azienda and
												d.anno_registrazione = b.anno_registrazione and
												d.num_registrazione = b.num_registrazione and
												d.prog_riga_ord_ven = b.prog_riga_ord_ven and
												d.progr_det_produzione = b.barcode
	where		b.cod_azienda=:s_cs_xx.cod_azienda and 
				b.cod_operaio_desosp is null and
				b.anno_registrazione=:ai_anno_registrazione and
				b.num_registrazione=:al_num_registrazione and
				b.prog_riga_ord_ven=:al_prog_riga and
				d.cod_reparto=:as_cod_reparto;
	
else
	//caso ordini tipo report 2 o report 3
	
	select  isnull(a.sigla, '-')
	into :as_sigla_sospensione
	from tab_sosp_produzione as b
	join tab_tipi_cause_sospensione as a on a.cod_azienda=b.cod_azienda and
														 b.cod_tipo_causa_sospensione=a.cod_tipo_causa_sospensione
	join tes_ordini_produzione as t on t.cod_azienda=b.cod_azienda and
												t.anno_registrazione = b.anno_registrazione and
												t.num_registrazione = b.num_registrazione and
												t.progr_tes_produzione = b.barcode
	where		b.cod_azienda=:s_cs_xx.cod_azienda and 
				b.cod_operaio_desosp is null and
				b.anno_registrazione=:ai_anno_registrazione and
				b.num_registrazione=:al_num_registrazione and
				t.cod_reparto=:as_cod_reparto;
	
end if

if isnull(as_sigla_sospensione) then as_sigla_sospensione = ""

return 0
end function

public function integer uof_get_sospensioni_riga_ordine (integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga);integer				li_count


//se esiste una sospensione nel reparto, ritorna la sigla della sospensione applicata

if al_prog_riga> 0 then
	//caso ordini tipo report 1
	
	select count(*)
	into :li_count
	from tab_sosp_produzione as b
	join tab_tipi_cause_sospensione as a on a.cod_azienda=b.cod_azienda and
														 b.cod_tipo_causa_sospensione=a.cod_tipo_causa_sospensione
	join det_ordini_produzione as d on d.cod_azienda=b.cod_azienda and
												d.anno_registrazione = b.anno_registrazione and
												d.num_registrazione = b.num_registrazione and
												d.prog_riga_ord_ven = b.prog_riga_ord_ven and
												d.progr_det_produzione = b.barcode
	where		b.cod_azienda=:s_cs_xx.cod_azienda and 
				b.cod_operaio_desosp is null and
				b.anno_registrazione=:ai_anno_registrazione and
				b.num_registrazione=:al_num_registrazione and
				b.prog_riga_ord_ven=:al_prog_riga;
	
else
	//caso ordini tipo report 2 o report 3
	
	select count(*)
	into :li_count
	from tab_sosp_produzione as b
	join tab_tipi_cause_sospensione as a on a.cod_azienda=b.cod_azienda and
														 b.cod_tipo_causa_sospensione=a.cod_tipo_causa_sospensione
	join tes_ordini_produzione as t on t.cod_azienda=b.cod_azienda and
												t.anno_registrazione = b.anno_registrazione and
												t.num_registrazione = b.num_registrazione and
												t.progr_tes_produzione = b.barcode
	where		b.cod_azienda=:s_cs_xx.cod_azienda and 
				b.cod_operaio_desosp is null and
				b.anno_registrazione=:ai_anno_registrazione and
				b.num_registrazione=:al_num_registrazione;
	
end if

if isnull(li_count) then li_count = 0

return li_count
end function

public function integer uof_elimina_riga_ordine (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, ref string as_errore);string ls_cod_prodotto, ls_cod_versione, ls_flag_evasione, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_flag_blocco, &
       ls_messaggio, ls_mess_comm_eliminate,ls_cod_prodotto_raggruppato,ls_flag_tipo_avanzamento, ls_sql
		 
long 	ll_anno_commessa, ll_num_commessa, ll_null, ll_i, ll_righe, ll_riga_ordine, ll_num_riga_appartenenza, ll_riga_corrente, ll_anno_comm_riferita,ll_num_comm_riferita, &
	  	ll_errore
	  
dec{4} ldd_quan_in_produzione, ldd_quan_assegnata, ldd_quan_prodotta, ldd_quan_in_ordine, ldd_quan_impegnata, &
       ldd_quan_evasa, ldd_quan_in_evasione, ld_quan_ordine,ld_quan_raggruppo
		 
uo_funzioni_1 luo_funzioni_commesse
datastore lds_righe_ordine
uo_calendario_prod_new luo_cal_prod
integer li_risposta

setnull(ll_null)

select flag_evasione,
		quan_evasa,
		quan_in_evasione,
		anno_commessa,
		num_commessa,
		flag_blocco
into 	:ls_flag_evasione,
		:ldd_quan_evasa,
		:ldd_quan_in_evasione,
		:ll_anno_commessa,
		:ll_num_commessa,
		:ls_flag_blocco
from det_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :al_anno_registrazione and
		num_registrazione = :al_num_Registrazione and
		prog_riga_ord_ven = :al_prog_riga_ord_ven;

if ls_flag_evasione <> "A" then
	as_errore = "Non è possibile effettuare eliminazioni di righe già evase o parzialmente evase"
	return -1
end if

if ldd_quan_evasa > 0 or ldd_quan_in_evasione > 0  then
	as_errore = "Non è possibile effettuare eliminazioni di righe con quantità già evasa o in evasione"
	return -1
end if

if ls_flag_blocco = 'S' then
	as_errore = "Non puoi eliminare una riga d'ordine bloccata !!!"
	return -1
end if

select flag_blocco
into   :ls_flag_blocco
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione;
if ls_flag_blocco = 'S' then
	as_errore = "Non puoi eliminare una riga da un ordine bloccato !!!"
	return -1
end if

// 8/2/2007 aggiunto su richiesta di Beatrice: impossibile eliminare una riga se commessa chiusa.
if not isnull(ll_anno_commessa) then
	select flag_tipo_avanzamento
	into   :ls_flag_tipo_avanzamento
	from   anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_commessa = :ll_anno_commessa and
			 num_commessa = :ll_num_commessa;
	if sqlca.sqlcode <> 0 then
		as_errore = "Errore in cancellazione commessa " + string(ll_anno_commessa) + "/" + string(ll_num_commessa) + "~r~n"+sqlca.sqlerrtext
		return -1
	end if
	
	if ls_flag_tipo_avanzamento = "7" then
		as_errore = "Commessa già chiusa: impossibile eliminare."
		return -1
	end if
		
end if

// 13/04/2004 Enrico
//  cancellazione avanzamento di produzione su richiesta beatrice

ll_errore = uof_elimina_prod_det(al_anno_registrazione, al_num_registrazione, al_prog_riga_ord_ven, ref ls_messaggio)
if ll_errore < 0 then
	as_errore = "Cancellazione produzione: " +  ls_messaggio
	return -1
end if

// ------------------


if not isnull(ll_anno_commessa) then
	luo_funzioni_commesse = CREATE uo_funzioni_1
	if luo_funzioni_commesse.uof_cancella_commessa(ll_anno_commessa,ll_num_commessa, ls_messaggio) = -1 then
		g_mb.messagebox("Cancellazione Commessa", ls_messaggio)
		destroy luo_funzioni_commesse
		return -1
	end if
	
	delete from anag_commesse
	where  cod_azienda   = :s_cs_xx.cod_azienda and
			 anno_commessa = :ll_anno_commessa and
			 num_commessa  = :ll_num_commessa;
	if sqlca.sqlcode <> 0 then
		as_errore = "Errore durante la cancellazione della commessa: " + sqlca.sqlerrtext
		return -1
	else
		ls_mess_comm_eliminate = "ELIMINATA LA COMMESSA N° " + string(ll_anno_commessa) + "/" + string(ll_num_commessa)
	end if
end if

delete from varianti_det_ord_ven_prod
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione and
		 prog_riga_ord_ven = :al_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	as_errore = "Errore cancellazione delle varianti"
	return -1
end if

delete from varianti_det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione and
		 prog_riga_ord_ven = :al_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	as_errore = "Errore cancellazione delle varianti"
	return -1
end if


delete from tab_optionals_comp_det
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione and
		 prog_riga_ord_ven = :al_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	as_errore = "Errore cancellazione optionals del dettaglio complementare (tab_optionals_comp_det)"
	return -1
end if



delete from tab_optionals_comp_det
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione and
		 prog_riga_ord_ven = :al_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	as_errore = "Errore cancellazione optionals del dettaglio complementare (tab_optionals_comp_det)"
	return -1
end if

delete from tab_mp_non_ric_comp_det
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione and
		 prog_riga_ord_ven = :al_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	as_errore = "Errore cancellazione del dettaglio complementare (tab_mp_non_ric_comp_det)"
	return -1
end if


delete from comp_det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione and
		 prog_riga_ord_ven = :al_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	as_errore = "Errore cancellazione del dettaglio complementare"
	return -1
end if


delete from det_ord_ven_conf_variabili
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione and
		 prog_riga_ord_ven = :al_prog_riga_ord_ven;


//pulizia tabella det_ord_ven_prod, MA PRIMA AGGIORNA LA TAB_CAL_PRODUZIONE -----------------------------------------
luo_cal_prod = create uo_calendario_prod_new
li_risposta = luo_cal_prod.uof_disimpegna_calendario(al_anno_registrazione, al_num_registrazione, al_prog_riga_ord_ven, ls_messaggio)
destroy luo_cal_prod;

if li_risposta<0 then
	//messaggio ma non blocco
	//g_mb.warning(ls_errore)
	
else
	delete det_ord_ven_prod
	where  cod_azienda = :s_cs_xx.cod_azienda
	and    anno_registrazione = :al_anno_registrazione
	and    num_registrazione  = :al_num_registrazione
	and    prog_riga_ord_ven  =: al_prog_riga_ord_ven;
	
end if
//--------------------------------------------------------------------------------------------------------------------------------------------

ls_sql = "select num_riga_appartenenza, cod_tipo_det_ven, cod_prodotto, quan_ordine,prog_riga_ord_ven, anno_commessa, num_commessa from det_ord_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(al_anno_registrazione) + " and num_registrazione = " + string(al_num_registrazione) + " and num_riga_appartenenza = " + string(al_prog_riga_ord_ven) + " order by prog_riga_ord_ven "

guo_functions.uof_crea_datastore( lds_righe_ordine, ls_sql)

ll_righe = lds_righe_ordine.rowcount()
for ll_i = 1 to ll_righe
//	ll_num_riga_appartenenza = lds_righe_ordine.getitemnumber(ll_i, "num_riga_appartenenza")
	
//	if (ll_num_riga_appartenenza = al_prog_riga_ord_ven and not isnull(ll_num_riga_appartenenza) and ll_num_riga_appartenenza <> 0) or ll_i = al_prog_riga_ord_ven then
	
		ls_cod_tipo_det_ven = lds_righe_ordine.getitemstring(ll_i, "cod_tipo_det_ven")
		select tab_tipi_det_ven.flag_tipo_det_ven
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
		if sqlca.sqlcode = -1 then
			as_errore =  "Si è verificato un errore in fase di lettura tipi dettaglio di vendita."
			return -1
		end if
	
		if ls_flag_tipo_det_ven = "M" then
			ls_cod_prodotto = lds_righe_ordine.getitemstring(ll_i, "cod_prodotto")
			ld_quan_ordine = lds_righe_ordine.getitemnumber(ll_i, "quan_ordine")
	
			update anag_prodotti  
				set quan_impegnata = quan_impegnata - :ld_quan_ordine  
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode = -1 then
				as_errore =  "Si è verificato un errore in fase di aggiornamento magazzino."
				return -1
			end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
				
				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordine, "M")
				
				update anag_prodotti  
					set quan_impegnata = quan_impegnata - :ld_quan_raggruppo  
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
		
				if sqlca.sqlcode = -1 then
					as_errore =  "Si è verificato un errore in fase di aggiornamento magazzino."
					return -1
				end if
			end if
			
		end if
		ll_riga_ordine  = lds_righe_ordine.getitemnumber(ll_i, "prog_riga_ord_ven")
		
		// cancello la commessa delle righe riferite  [EnMe 22/4/2003]
		ll_anno_comm_riferita = lds_righe_ordine.getitemnumber(ll_i, "anno_commessa")
		ll_num_comm_riferita = lds_righe_ordine.getitemnumber(ll_i, "num_commessa")
		
		if not isnull(ll_anno_comm_riferita) then
			luo_funzioni_commesse = CREATE uo_funzioni_1
			if luo_funzioni_commesse.uof_cancella_commessa(ll_anno_comm_riferita,ll_num_comm_riferita, ls_messaggio) = -1 then
				as_errore = "Cancellazione Commessa: " + ls_messaggio
				destroy luo_funzioni_commesse
				return -1
			end if
			
			delete from anag_commesse
			where  cod_azienda   = :s_cs_xx.cod_azienda and
					 anno_commessa = :ll_anno_comm_riferita and
					 num_commessa  = :ll_num_comm_riferita;
			if sqlca.sqlcode <> 0 then
				as_errore = "Errore durante la cancellazione della commessa: "
				return -1
			else
				ls_mess_comm_eliminate += "~r~nCOMMESSA (di riga riferita) N° " + string(ll_anno_commessa) + "/" + string(ll_num_commessa)
			end if
		end if
		// -----------------------------------------------------------
		delete from det_ord_ven_note
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno_registrazione and
				 num_registrazione  = :al_num_registrazione and
				 prog_riga_ord_ven  = :ll_riga_ordine;
		if sqlca.sqlcode <> 0 then
			as_errore = "Errore cancellazione delle note della riga ordine"
			return -1
		end if

		delete from det_ord_ven_corrispondenze
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno_registrazione and
				 num_registrazione  = :al_num_registrazione and
				 prog_riga_ord_ven  = :ll_riga_ordine;
		if sqlca.sqlcode <> 0 then
			as_errore = "Errore cancellazione delle corrispondenze riga ordine"
			return -1
		end if
		
		//Donato 28/01/2013 ----------------------------------------------------------------------
		//Beatrice ha chiesto di mettere a NULL il collegamento con la riga di fattura proforma
		//quando cancello una riga di ordine di vendita
		if guo_functions.uof_setnull_fatt_proforma(al_anno_registrazione, al_num_registrazione, ll_riga_ordine, ls_messaggio) < 0 then
			as_errore = ls_messaggio
			return -1
		end if
		//----------------------------------------------------------------------------------------------
		
		
		
		//riapertura eventuale offerta vendita collegata
		delete from varianti_det_ord_ven_prod
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno_registrazione and
				 num_registrazione = :al_num_registrazione and
				 prog_riga_ord_ven = :ll_riga_ordine;
		if sqlca.sqlcode <> 0 then
			as_errore = "Errore cancellazione delle varianti"
			return -1
		end if

		delete from varianti_det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno_registrazione and
				 num_registrazione  = :al_num_registrazione and
				 prog_riga_ord_ven  = :ll_riga_ordine;
		if sqlca.sqlcode <> 0 then
			as_errore = "Errore cancellazione del riga ordine"
			return -1
		end if
		
		
		delete from tab_optionals_comp_det
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno_registrazione and
				 num_registrazione = :al_num_registrazione and
				 prog_riga_ord_ven = :ll_riga_ordine;
		if sqlca.sqlcode <> 0 then
			as_errore = "Errore cancellazione optionals del dettaglio complementare (tab_optionals_comp_det)"
			return -1
		end if
		
		delete from tab_mp_non_ric_comp_det
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno_registrazione and
				 num_registrazione = :al_num_registrazione and
				 prog_riga_ord_ven = :ll_riga_ordine;
		if sqlca.sqlcode <> 0 then
			as_errore = "Errore cancellazione del dettaglio complementare (tab_mp_non_ric_comp_det)"
			return -1
		end if
		
		
		delete from comp_det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno_registrazione and
				 num_registrazione = :al_num_registrazione and
				 prog_riga_ord_ven = :ll_riga_ordine;
		if sqlca.sqlcode <> 0 then
			as_errore = "Errore cancellazione del dettaglio complementare"
			return -1
		end if


		delete from varianti_det_ord_ven_prod
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno_registrazione and
				 num_registrazione = :al_num_registrazione and
				 prog_riga_ord_ven = :ll_riga_ordine;
		if sqlca.sqlcode <> 0 then
			as_errore = "Errore cancellazione delle varianti"
			return -1
		end if

		delete from det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno_registrazione and
				 num_registrazione  = :al_num_registrazione and
				 prog_riga_ord_ven  = :ll_riga_ordine;
		if sqlca.sqlcode <> 0 then
			as_errore = "Errore cancellazione della riga ordine"
			return -1
		end if
//	end if
next

// -------------------------------- cancello la riga principale  ------------------------------------------------

select cod_tipo_det_ven,
		cod_prodotto,
		quan_ordine
into 	:ls_cod_tipo_det_ven,
		:ls_cod_prodotto,
		:ld_quan_ordine
from 	det_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :al_anno_registrazione and
		num_registrazione = :al_num_Registrazione and
		prog_riga_ord_ven = :al_prog_riga_ord_ven;
if sqlca.sqlcode = -1 then
	as_errore =  "Si è verificato un errore in fase di lettura riga ordine." + sqlca.sqlerrtext
	return -1
end if

select flag_tipo_det_ven
into   :ls_flag_tipo_det_ven
from   tab_tipi_det_ven
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_tipo_det_ven = :ls_cod_tipo_det_ven;

if sqlca.sqlcode = -1 then
	as_errore =  "Si è verificato un errore in fase di lettura tipi dettaglio di vendita." + sqlca.sqlerrtext
	return -1
end if

if ls_flag_tipo_det_ven = "M" then

	update anag_prodotti  
		set quan_impegnata = quan_impegnata - :ld_quan_ordine  
	 where cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode = -1 then
		as_errore =  "Si è verificato un errore in fase di aggiornamento magazzino."
		return -1
	end if
	
	// enme 08/1/2006 gestione prodotto raggruppato
	setnull(ls_cod_prodotto_raggruppato)
	
	select cod_prodotto_raggruppato
	into   :ls_cod_prodotto_raggruppato
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if not isnull(ls_cod_prodotto_raggruppato) then
		
		ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordine, "M")
		
		update anag_prodotti  
			set quan_impegnata = quan_impegnata - :ld_quan_raggruppo  
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_prodotto = :ls_cod_prodotto_raggruppato;
		if sqlca.sqlcode = -1 then
			as_errore =  "Si è verificato un errore in fase di aggiornamento magazzino."
			return -1
		end if
	end if
	
end if

delete from det_ord_ven_note
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno_registrazione and
		 num_registrazione  = :al_num_registrazione and
		 prog_riga_ord_ven  = :al_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	as_errore = "Errore cancellazione delle note della riga ordine"
	return -1
end if

delete from det_ord_ven_corrispondenze
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno_registrazione and
		 num_registrazione  = :al_num_registrazione and
		 prog_riga_ord_ven  = :al_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	as_errore = "Errore cancellazione delle corrispondenze riga ordine"
	return -1
end if

delete from distinte_taglio_calcolate
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno_registrazione and
		 num_registrazione  = :al_num_registrazione and
		 prog_riga_ord_ven  = :al_prog_riga_ord_ven;
if sqlca.sqlcode < 0 then
	as_errore = "Errore cancellazione distinta taglio calcolata: "+sqlca.sqlerrtext
	return -1
end if

//----------------------------------------------------------------------------------------------
//Donato 23/05/2012
//Beatrice ha chiesto di riaprire l'offerta se elimino le righe
if uof_riapri_offerta(al_anno_registrazione, al_num_registrazione, al_prog_riga_ord_ven, ls_messaggio)<0 then
	as_errore = ls_messaggio
	return -1
end if
//----------------------------------------------------------------------------------------------


//elimino eventuali ologrammi
ll_errore = uof_elimina_ologrammi(al_anno_registrazione, al_num_registrazione, al_prog_riga_ord_ven, ls_messaggio)
if ll_errore < 0 then
	//in fs_messaggio l'errore
	as_errore = ls_messaggio
	return -1
elseif ll_errore = 1 then
	//operazione annullata
	as_errore = ls_messaggio
	return -1
end if


//Donato 28/01/2013 ----------------------------------------------------------------------
//Beatrice ha chiesto di mettere a NULL il collegamento con la riga di fattura proforma
//quando cancello una riga di ordine di vendita
if guo_functions.uof_setnull_fatt_proforma(al_anno_registrazione, al_num_registrazione, al_prog_riga_ord_ven, ls_messaggio) < 0 then
	as_errore = ls_messaggio
	return -1
end if
//----------------------------------------------------------------------------------------------

delete from det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno_registrazione and
		 num_registrazione  = :al_num_registrazione and
		 prog_riga_ord_ven  = :al_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	as_errore = "Errore cancellazione della riga ordine"
	return -1
end if
	
commit;

if len(trim(ls_mess_comm_eliminate)) > 0 then
	as_errore =  ls_mess_comm_eliminate
end if

return 0
end function

public function integer uof_riapri_offerta (long fi_anno_ord_ven, long fl_num_ord_ven, long fl_riga_ord_ven, ref string fs_errore);//Donato 23/05/2012
//chiesto da Beatrice

//questa funzione viene chiamata in cancellazione di una riga ordine
//se la riga dell'ordine ha un riferimento ad un'offerta allora devo riaprire l'offerta

integer li_anno_offerta
long ll_num_offerta, ll_count
string ls_flag_evasione

//leggo se esiste una riga di offerta collegata alla riga ordine che si vuole cancellare
select 		anno_registrazione_off,
				num_registrazione_off
into			:li_anno_offerta,
				:ll_num_offerta
from 	det_ord_ven
where  	cod_azienda = :s_cs_xx.cod_azienda  and  
			anno_registrazione = :fi_anno_ord_ven  and  
			num_registrazione = :fl_num_ord_ven and
			prog_riga_ord_ven=:fl_riga_ord_ven;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in lettura riga offerta: " + sqlca.sqlerrtext
	return -1
end if

if li_anno_offerta>0 and ll_num_offerta>0 then
	//esiste un'offerta
	
	//verifico se ci sono altre righe dell'ordine (a parte quella che sto per cancellare) con la stessa offerta collegata
	//se si allora lasso stare, altrimenti riapro l'offerta
	select count(*)
	into :ll_count
	from det_ord_ven
	where  	cod_azienda = :s_cs_xx.cod_azienda  and  
				anno_registrazione = :fi_anno_ord_ven  and  
				num_registrazione = :fl_num_ord_ven and
				anno_registrazione_off = :li_anno_offerta and
				num_registrazione_off = :ll_num_offerta and
				prog_riga_ord_ven<>:fl_riga_ord_ven;
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in controllo esistenza altre righe ordine con stessa offerta: " + sqlca.sqlerrtext
		return -1
	end if
	
	if ll_count>0 then
		//OK, lasso stare, non riapro niente
	else
		
		//leggo lo stato evasione dell'offerta collegata
		select flag_evasione
		into :ls_flag_evasione
		from tes_off_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:li_anno_offerta and
					num_registrazione=:ll_num_offerta;
		
		//riapro l'offerta
		if ls_flag_evasione<>"A" then
		
			//riapro l'offerta
			update tes_off_ven
			set flag_evasione='A'
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						anno_registrazione=:li_anno_offerta and
						num_registrazione=:ll_num_offerta;
			
			if sqlca.sqlcode < 0 then
				fs_errore = "Errore in riapertura offerta "+string(li_anno_offerta)+"/"+string(ll_num_offerta)+": " + sqlca.sqlerrtext
				return -1
			end if
			
		end if
		
	end if
	
end if

return 0

end function

public function integer uof_cal_trasferimenti_navetta (date ad_data_arrivo_richiesta, string as_reparto_partenza, string as_reparto_arrivo, string as_deposito_arrivo, ref date ad_data_partenza, ref string as_message);string ls_sql, ls_deposito_partenza
long ll_ret, ll_i
datastore lds_navetta

setnull(ad_data_partenza)
ls_deposito_partenza = f_des_tabella("anag_reparti", "cod_reparto='" + as_reparto_partenza + "'", "cod_deposito")
if isnull(as_deposito_arrivo) then
	as_deposito_arrivo = f_des_tabella("anag_reparti", "cod_reparto='" + as_reparto_arrivo + "'", "cod_deposito")
end if

ls_sql = " select data_partenza from tab_cal_trasferimenti where cod_azienda='" + s_cs_xx.cod_azienda +  "' and " + &
			" cod_deposito_partenza='" + ls_deposito_partenza + "' and " + &
			" cod_deposito_arrivo='" + as_deposito_arrivo + "' and " + &
			" data_arrivo <'" + string(ad_data_arrivo_richiesta, s_cs_xx.db_funzioni.formato_data) + "' and " + &
			" data_partenza >'" + string(today(), s_cs_xx.db_funzioni.formato_data) + "' " + &
			" order by data_partenza desc"
			
ll_ret = guo_functions.uof_crea_datastore(ref lds_navetta, ls_sql)

if ll_ret < 0 then
	as_message = "Errore risoluzione SQL nel datastore lds_navetta - (uof_cal_trasferimenti_navetta)"
	return -1
elseif ll_ret = 0 then
	as_message = "Nessuna soluzione di navetta trovata nel calendario trasferimenti~r~n~r~n"+"dal deposito "+ls_deposito_partenza + "~r~n" + &
					"dopo il "+string(today(), "dd/mm/yyyy") + "~r~n~r~n" + &
					"al deposito " + as_deposito_arrivo + "~r~n" + "prima del "+string(ad_data_arrivo_richiesta, "dd/mm/yyyy") + " !~r~n~r~n"+&
					"Verificare il calendario navette!"
	return -1
else
	ad_data_partenza = date( lds_navetta.getitemdatetime(1,1) )
end if

return 0
end function

public function integer uof_cal_trasferimenti (long al_anno_ord_ven, long al_num_ord_ven, string as_cod_prodotto_finito, string as_cod_versione_finito, ref string as_dep_par[], ref string as_rep_par[], ref date add_dtp_rep_par[], ref string as_dep_arr[], ref string as_rep_arr[], ref date add_dtp_rep_arr[], ref string as_message);/*
FUNZIONE DI CACOLO DEL CALENDARIO TRASFERIMENTI 
Vedi Specificadei requisiti Calendario trasferimenti  rev 6 del 25.03.2013
EnMe
*/

string			ls_flag_tipo_bcl, ls_reparti[], ls_cod_deposito_commerciale,ls_cod_reparto_arrivo, ls_cod_deposito_giro
string			ls_reparti_par[], ls_depositi_par[], ls_reparti_arr[], ls_depositi_arr[], ls_null[], ls_null_string,ls_cod_reparto_sfuso
long			ll_ind,ll_detrazione_gg_arrivo,ll_detrazione_gg_partenza, ll_num_evento
date			ldd_data_partenza, ldd_dtp_rep_arr[], ldd_dtp_rep_par[], ldd_null[], ldd_date_null
datetime 	ldt_data_partenza

uo_funzioni_1 luo_f1
uo_calendario_prod_new luo_cal_prod 

setnull(ls_null_string)
setnull(ldd_date_null)

as_dep_par[] = ls_null[]
as_rep_par[] = ls_null[]
add_dtp_rep_par[] = ldd_null[]
as_dep_arr[] = ls_null[]
as_rep_arr[] = ls_null[]
add_dtp_rep_arr[] = ldd_null[]


luo_f1 = create uo_funzioni_1
luo_cal_prod = create uo_calendario_prod_new

ll_ind = 0


if isnull(as_cod_prodotto_finito) then return 0

select 	tes_ord_ven.data_consegna, 
			tab_tipi_ord_ven.flag_tipo_bcl,
			tes_ord_ven.cod_deposito
into 		:ldt_data_partenza, 
			:ls_flag_tipo_bcl,
			:ls_cod_deposito_commerciale
from 		tes_ord_ven
left outer join		tab_tipi_ord_ven on 	tab_tipi_ord_ven.cod_azienda = tes_ord_ven.cod_azienda   and 
											tab_tipi_ord_ven.cod_tipo_ord_ven  = tes_ord_ven.cod_tipo_ord_ven 
where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
			tes_ord_ven.anno_registrazione = :al_anno_ord_ven and
			tes_ord_ven.num_registrazione = :al_num_ord_ven;
if sqlca.sqlcode <> 0 then
	as_message = g_str.format("Errore in ricerca data partenza in testata ordine $1/$2. $4", al_anno_ord_ven, al_num_ord_ven,  sqlca.sqlerrtext)
	return -1
end if

if isnull(ls_flag_tipo_bcl) or ls_flag_tipo_bcl = "" then
	as_message = "FLAG tipo bolla lavoro non specificato in tipo ordine: procedura interrotta"
	return -1
end if

//se esiste un giro di consegna e questo ha un deposito, considera questo al posto del deposito testata ordine vendita
ls_cod_deposito_giro = uof_deposito_giro_consegna(al_anno_ord_ven, al_num_ord_ven)
if g_str.isnotempty(ls_cod_deposito_giro) and ls_cod_deposito_giro<>ls_cod_deposito_commerciale then ls_cod_deposito_commerciale = ls_cod_deposito_giro


if not isnull(idt_data_partenza) and year(date(idt_data_partenza)) > 2000 then
	//vuol dire che in precedenza hai scelto di cambiare data partenza dell'ordine
	//poichè non è stata ancora salvata nella testata, ignora quello in tabella e considera questa
	ldt_data_partenza = idt_data_partenza
end if


if isnull(il_riga_ordine) or il_riga_ordine<=0 then il_riga_ordine = -1

ldd_data_partenza = date(ldt_data_partenza)

// Carico sequenza dei reparti e stabilimenti
choose case ls_flag_tipo_bcl
	case "A", "C"		// TS + TT
		if uof_reparti( al_anno_ord_ven, al_num_ord_ven, il_riga_ordine, as_cod_prodotto_finito, as_cod_versione_finito, ref ls_reparti[],  ref as_message) < 0 then
			as_message = "Caso: TS+TT, Chiamata funzione uo_produzione.uof_reparti()~r~n" + as_message
			goto End4Error
		end if
	case "B"				// Sfuso
		
			//legge il reparto associato in anagrafica prodotti ---------
			select cod_reparto
			into   :ls_cod_reparto_sfuso
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:as_cod_prodotto_finito;
			
			//Donato 19/12/2011
			//verifica se esiste eccezione
			ls_reparti[] = ls_null[]
			if f_reparti_stabilimenti(al_anno_ord_ven, al_num_ord_ven, as_cod_prodotto_finito, "", 	ls_cod_reparto_sfuso, "", "",  ref ls_reparti[], as_message)<0 then
				as_message = "Caso:SFUSO, Chiamata funzionef_reparti_stabilimenti~r~n" + as_message
				goto End4Error
			end if
		
end choose

if upperbound(ls_reparti) < 1 then
	as_message = "La composizione corrente non prevede alcuna fase di lavoro con reparto associato; nessun calcolo possibile sul calendario trasferimenti. "
	goto End4error
end if

// inverto l'array
guo_functions.as_reverse_array (ls_reparti[], ref ls_reparti[])


do while true
	
	ll_ind ++
	
	if ll_ind = 1 then
		// sono al primo (cioè l'ultimo nella sequenza di produzione) reparto che è quello che deve eseguire la consegna
		ls_reparti_par[ll_ind] = ls_reparti[ll_ind]
		ls_depositi_par[ll_ind] = f_des_tabella ( "anag_reparti", "cod_reparto = '" + ls_reparti_par[ll_ind] + "'" , "cod_deposito" )
		ls_depositi_arr[ll_ind] = ls_cod_deposito_commerciale
		
		select 	detrazione_gg_arrivo,
					detrazione_gg_partenza
		into		:ll_detrazione_gg_arrivo,
					:ll_detrazione_gg_partenza
		from 		tab_eventi_trasferimento
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					num_evento = 3 and
					cod_reparto_partenza = :ls_reparti_par[ll_ind] and
					cod_deposito_arrivo = :ls_depositi_arr[ll_ind];
					
		if sqlca.sqlcode <> 0 then
			as_message = g_str.format("Ricerca fallita: EVENTO 3, stabilimento arrivo = $1, reparto partenza = $2", ls_cod_deposito_commerciale, ls_reparti_par[ll_ind])
			if sqlca.sqlcode = -1 then as_message = as_message + "~r~nDettaglio:" + sqlca.sqlerrtext
			goto End4Error
		end if
		
		// verifico se lo stabilimento del reparto di arrivo corrisponde allo stabilimento del reparto di partenza
		if ls_depositi_par[ll_ind] = ls_depositi_arr[ll_ind] then
			
			// il deposito dell'ultimo reparto è anche il deposito che spedisce al cliente
			ldd_dtp_rep_arr[ll_ind] = luo_cal_prod.uof_relativedate_cal( - ll_detrazione_gg_arrivo, ldd_data_partenza ) 
			
			ldd_dtp_rep_par[ll_ind] = luo_cal_prod.uof_relativedate_cal( - ll_detrazione_gg_partenza, ldd_dtp_rep_arr[ll_ind] )
			ls_reparti_arr[ll_ind] = ls_reparti_par[ll_ind]
			continue
			
		else 			// il deposito dell'ultimo reparto NON è il deposito che spedisce al cliente; devo trasferire
			
			ldd_dtp_rep_arr[ll_ind] = luo_cal_prod.uof_relativedate_cal( - ll_detrazione_gg_arrivo,ldd_data_partenza )
			ls_reparti_arr[ll_ind] = ""
			// cerca navetta compatibile
			if uof_cal_trasferimenti_navetta(ldd_dtp_rep_arr[ll_ind], ls_reparti_par[ll_ind], ls_null_string, ls_depositi_arr[ll_ind],ref ldd_dtp_rep_par[ll_ind], ref as_message) < 0 then
				// navetta non trovata oppure errore sql
				goto End4Error
			else
				ldd_dtp_rep_par[ll_ind] =  luo_cal_prod.uof_relativedate_cal( - ll_detrazione_gg_partenza, ldd_dtp_rep_par[ll_ind] )
			end if
			
		end if
					
	else
		if ll_ind > upperbound(ls_reparti) then exit
		// NON sono al primo (cioè l'ultimo nella sequenza di produzione) reparto.
		
		ls_reparti_arr[ll_ind] = ls_reparti_par[ll_ind - 1]
		ls_reparti_par[ll_ind] =  ls_reparti[ll_ind]
		ls_depositi_par[ll_ind] = f_des_tabella ( "anag_reparti", "cod_reparto = '" + ls_reparti_par[ll_ind] + "'" , "cod_deposito" )
		ls_depositi_arr[ll_ind] = f_des_tabella ( "anag_reparti", "cod_reparto = '" +  ls_reparti_arr[ll_ind] + "'" , "cod_deposito" )
		
		select 	detrazione_gg_arrivo,
					detrazione_gg_partenza,
					num_evento
		into		:ll_detrazione_gg_arrivo,
					:ll_detrazione_gg_partenza,
					:ll_num_evento
		from 		tab_eventi_trasferimento
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					num_evento in (1,2) and
					cod_reparto_partenza = :ls_reparti_par[ll_ind] and
					cod_reparto_arrivo = :ls_reparti_arr[ll_ind];
					
		if sqlca.sqlcode <> 0 then
			as_message = g_str.format("Ricerca fallita: EVENTO 1 oppure 2 , stabilimento arrivo = $1, reparto partenza = $2", ls_cod_deposito_commerciale, ls_reparti_par[ll_ind])
			if sqlca.sqlcode = -1 then as_message = as_message + "~r~nDettaglio:" + sqlca.sqlerrtext
			goto End4Error
		end if

		// la data arrivo del reparto = data partenza del precedente meno i tempi di lavorazione
		ldd_dtp_rep_arr[ll_ind] = luo_cal_prod.uof_relativedate_cal( - ll_detrazione_gg_arrivo, ldd_dtp_rep_par[ll_ind - 1])
		
		if ll_num_evento = 1 then
			// reparti dello steso stabilimento, non serve nessun trasferimento
			ldd_dtp_rep_par[ll_ind] = ldd_dtp_rep_arr[ll_ind]
			ldd_dtp_rep_par[ll_ind] = luo_cal_prod.uof_relativedate_cal( - ll_detrazione_gg_partenza, ldd_dtp_rep_par[ll_ind])
			
		else
			// reparto di stabilimenti diversi, procedo con trasferimento
			if uof_cal_trasferimenti_navetta(ldd_dtp_rep_arr[ll_ind], ls_reparti_par[ll_ind], ls_reparti_arr[ll_ind], ls_null_string, ref ldd_dtp_rep_par[ll_ind], ref as_message) < 0 then
				// navetta non trovata oppure errore sql
				goto End4Error
			else
				ldd_dtp_rep_par[ll_ind] = luo_cal_prod.uof_relativedate_cal( - ll_detrazione_gg_partenza, ldd_dtp_rep_par[ll_ind] )
			end if
			
		end if
	
	end if
loop

destroy luo_f1
as_dep_par[] = ls_depositi_par[]
as_rep_par[] = ls_reparti_par[]
add_dtp_rep_par[] = ldd_dtp_rep_par[]
as_dep_arr[] =ls_depositi_arr[]
as_rep_arr[] = ls_reparti_arr[]
add_dtp_rep_arr[] = ldd_dtp_rep_arr[]
return 0

End4Error:
as_dep_par[] = ls_null[]

//ALMENO I REPARTI ME LI DEVI TORNARE ECCHECCACCHIO!!!!
as_rep_par[] = ls_reparti[]
//as_rep_par[] = ls_null[]

//POI MI TORNI ANCHE UN ARRAY DI DATE VUOTE
for ll_ind=1 to upperbound(ls_reparti[])
	add_dtp_rep_par[ll_ind] = ldd_date_null
next
//add_dtp_rep_par[] = ldd_null[]


as_dep_arr[] = ls_null[]
as_rep_arr[] = ls_null[]
add_dtp_rep_arr[] = ldd_null[]
destroy luo_f1
destroy luo_cal_prod
return -1
end function

public function integer uof_cal_trasferimenti (long al_anno_ord_ven, long al_num_ord_ven, long al_prog_riga_ord_ven, ref string as_dep_par[], ref string as_rep_par[], ref date add_dtp_rep_par[], ref string as_dep_arr[], ref string as_rep_arr[], ref date add_dtp_rep_arr[], ref string as_message);string						ls_cod_prodotto_finito, ls_cod_versione_finito, ls_cod_tipo_ord_ven, ls_vuoto[]
//long						ll_ret
datetime					ldt_data_partenza
boolean					lb_ok
date						ldd_vuoto[]
integer					li_ret


as_dep_par[] = ls_vuoto[]
as_rep_par[] = ls_vuoto[]
as_dep_arr[] = ls_vuoto[]
as_rep_arr[] = ls_vuoto[]
add_dtp_rep_par[] = ldd_vuoto[]
add_dtp_rep_arr[] = ldd_vuoto[]

select 	d.cod_prodotto,
			d.cod_versione,
			t.data_consegna,
			t.cod_tipo_ord_ven
into		:ls_cod_prodotto_finito,
			:ls_cod_versione_finito,
			:ldt_data_partenza,
			:ls_cod_tipo_ord_ven
from		det_ord_ven as d
join 	tes_ord_ven as t on 	t.cod_azienda=d.cod_azienda and
									t.anno_registrazione=d.anno_registrazione and
									t.num_registrazione = d.num_registrazione
where		d.cod_azienda = :s_cs_xx.cod_azienda and
			d.anno_registrazione = :al_anno_ord_ven and
			d.num_registrazione = :al_num_ord_ven and
			d.prog_riga_ord_ven = :al_prog_riga_ord_ven;
			

if sqlca.sqlcode <> 0 then
	as_message = g_str.format("Errore in ricerca riga ordine ordine $1/$2/$3. $4", al_anno_ord_ven, al_num_ord_ven, al_prog_riga_ord_ven,  sqlca.sqlerrtext)
	return -1
end if


select flag_tipo_bcl
into	:ls_cod_tipo_ord_ven
from tab_tipi_ord_ven
where		cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;


//prima di rifare tutta l'elaborazione, SE LA DATA PARTENZA NON è STATA CAMBIATA, prova a leggere la configurazione reparti/date dalle varianti o dalla det_ord_ven (se sfuso)
lb_ok = false

if isnull(idt_data_partenza) or year(date(idt_data_partenza))<=2000 or ldt_data_partenza=idt_data_partenza then
	
	if ls_cod_tipo_ord_ven="B" or ls_cod_tipo_ord_ven="C" then
		//leggi da det_ord_ven ------------------------------------------------------------------
		
		select		cod_reparto_sfuso,
					data_pronto_sfuso,
					cod_reparto_arrivo,
					data_arrivo
		into	:as_rep_par[1],
				:add_dtp_rep_par[1],
				:as_rep_arr[1],
				:add_dtp_rep_arr[1]
		from det_ord_ven
		where		cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :al_anno_ord_ven and
					num_registrazione = :al_num_ord_ven and
					prog_riga_ord_ven = :al_prog_riga_ord_ven;
		
		//dell'eventuale errore, per ora non me ne frega niente ...
		
		if sqlca.sqlcode=0 and as_rep_par[1]<>"" and not isnull(as_rep_par[1]) and as_rep_arr[1]<>"" and not isnull(as_rep_arr[1]) and &
					not isnull(add_dtp_rep_par[1]) and year(add_dtp_rep_par[1])>1950 and not isnull(add_dtp_rep_arr[1]) and year(add_dtp_rep_arr[1])>1950 then
		
			as_dep_par[1] = f_des_tabella ( "anag_reparti", "cod_reparto = '" + as_rep_par[1] + "'" , "cod_deposito" )
			as_dep_arr[1] = f_des_tabella ( "anag_reparti", "cod_reparto = '" +  as_rep_arr[1] + "'" , "cod_deposito" )
			lb_ok = true
		else
			lb_ok = false
		end if
		
	else
		//leggi da varianti_det_ord_ven_prod ---------------------------------------------------
		li_ret = uof_get_varianti_det_ord_ven_prod(	al_anno_ord_ven, al_num_ord_ven, al_prog_riga_ord_ven, &
																	ref as_dep_par[], ref as_rep_par[], ref add_dtp_rep_par[], ref as_dep_arr[], ref as_rep_arr[], ref add_dtp_rep_arr[], ref as_message)
		
		as_message = ""		//dell'errore, per ora non me nefrega niente ...
		if li_ret=0 then
			lb_ok = true
		else
			lb_ok = false
		end if
		
	end if	
end if

//se lb_ok è FALSE vuol dire che la funziona rielaborerà tutto, quindi i dati non sono letti dalle varianti o dalla det_ord_ven
ib_rielaborato = not lb_ok


if not lb_ok then
	//ripulisco le variabili ... e ricalcolo tutto ...
	//as_dep_par[] = ls_vuoto[]
	as_rep_par[] = ls_vuoto[]
	as_dep_arr[] = ls_vuoto[]
	as_rep_arr[] = ls_vuoto[]
	add_dtp_rep_par[] = ldd_vuoto[]
	add_dtp_rep_arr[] = ldd_vuoto[]
	
	li_ret =uof_cal_trasferimenti(	al_anno_ord_ven, al_num_ord_ven, ls_cod_prodotto_finito, ls_cod_versione_finito, &
											ref as_dep_par[], ref as_rep_par[], ref add_dtp_rep_par[], ref as_dep_arr[], ref as_rep_arr[], ref add_dtp_rep_arr[], ref as_message )
end if

return li_ret
end function

public function integer uof_get_data_pronto_reparto (long al_barcode, ref datetime adt_data_pronto);//TODO
//passo il barcode di produzione
//legge la data pronto del relativo reparto
//deve valere per tutti i tipi di ordini (A,B,C)

long				ll_progr_det_produzione, ll_ret
string				ls_sql, ls_errore
datastore		lds_data


setnull(adt_data_pronto)

select data_pronto
into   :adt_data_pronto
from   det_ordini_produzione
where  cod_azienda=:s_cs_xx.cod_azienda and
			progr_det_produzione=:al_barcode;

if sqlca.sqlcode < 0 then 
	return -1
end if

if sqlca.sqlcode = 100 then
	//
	ls_sql = 	"select data_pronto "+&
				"from   det_ordini_produzione "+&
				"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"progr_tes_produzione="+string(al_barcode)
	
	ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
	
	if ll_ret>0 then
		//leggo il dato solo dalla prima riga
		adt_data_pronto = lds_data.getitemdatetime(1, "data_pronto")
	else
		//Non è stato trovato alcun ordine con questo codice a barre
		return -1	
	end if
end if

if isnull(adt_data_pronto) or year(date(adt_data_pronto))>1950 then return -1

return 0
end function

public function integer uof_trova_tipo (integer ai_anno_ordine, long al_num_ordine, ref string as_tipo_stampa, ref string as_errore);// Funzione che trova il tipo ordine A = tende da sole, B = Sfuso, C = tende tecniche
// nome: uof_trova_tipo
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 10-12-2003
//		Autore Diego Ferrari

string			ls_cod_tipo_ord_ven, ls_tipo_stampa


select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:ai_anno_ordine
and    num_registrazione=:al_num_ordine;

if sqlca.sqlcode < 0 then 
	as_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

select flag_tipo_bcl
into   :ls_tipo_stampa
from   tab_tipi_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;

if sqlca.sqlcode < 0 then 
	as_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

as_tipo_stampa = ls_tipo_stampa


return 0
end function

public function integer uof_get_varianti_det_ord_ven_prod (integer ai_anno_ordine, long al_num_ordine, long al_riga_ordine, ref string as_dep_par[], ref string as_rep_par[], ref date add_dtp_par[], ref string as_dep_arr[], ref string as_rep_arr[], ref date add_dtp_arr[], ref string as_errore);string				ls_cod_reparto_par, ls_cod_reparto_arr, ls_riga
date				ldd_data_partenza, ldd_data_arrivo
integer			li_new

//argomento ab_ottimistico
//è un booleano: se FALSE allora verifica che siano valorizzati tutti i campi della varianti_produzione
//se TRUE verifica solo che sia almeno presente il reparto princvipale (varianti_det_ord_ven_prod.cod_reparto)


declare cu_var_prod cursor for  
	select 	cod_reparto,
				data_pronto,
				cod_reparto_arrivo,
				data_arrivo
	from varianti_det_ord_ven_prod
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_ordine and
				num_registrazione=:al_num_ordine and
				prog_riga_ord_ven=:al_riga_ordine
	order by data_pronto desc;

open cu_var_prod;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore cu_var_prod: " + sqlca.sqlerrtext
	return -1
end if


li_new = 0
ls_riga = string(ai_anno_ordine) + "/" + string(al_num_ordine) + "/" + string(al_riga_ordine)

ib_varianti_ok = true

do while true
	
	fetch cu_var_prod into :ls_cod_reparto_par, :ldd_data_partenza, :ls_cod_reparto_arr, :ldd_data_arrivo;

	if sqlca.sqlcode < 0 then
		as_errore = "Errore in FETCH cursore cu_var_prod: " + sqlca.sqlerrtext
		close cu_var_prod;
		return -1
	end if

	if sqlca.sqlcode = 100 then exit

	if ls_cod_reparto_par="" or isnull(ls_cod_reparto_par) then
		close cu_var_prod;
		as_errore = "(1) No configurazione Reparto/Data pronto in varianti_det_ord_ven_prod: riga " + ls_riga
		return -1
	end if
	
	//verifica anche gli altri campi
	if ls_cod_reparto_arr="" or isnull(ls_cod_reparto_arr) then
		setnull(ls_cod_reparto_arr)
		ib_varianti_ok = false
	end if
	
	if isnull(ldd_data_partenza) or year(ldd_data_partenza)>1950 then
		setnull(ldd_data_partenza)
		ib_varianti_ok = false
	end if
	
	if isnull(ldd_data_arrivo) or year(ldd_data_arrivo)>1950 then
		setnull(ldd_data_arrivo)
		ib_varianti_ok = false
	end if

	//se arrivi fin qui procedi
	li_new += 1
	as_rep_par[li_new] = ls_cod_reparto_par
	add_dtp_par[li_new] = ldd_data_partenza
	as_rep_arr[li_new] = ls_cod_reparto_arr
	add_dtp_arr[li_new] = ldd_data_arrivo
	
	as_dep_par[li_new] = f_des_tabella ( "anag_reparti", "cod_reparto = '" + as_rep_par[li_new] + "'" , "cod_deposito" )
	as_dep_arr[li_new] = f_des_tabella ( "anag_reparti", "cod_reparto = '" +  as_rep_arr[li_new] + "'" , "cod_deposito" )
		
loop

close cu_var_prod;

if upperbound(as_rep_par[]) > 0 then
	return 0
else
	as_errore = " (3) No configurazione Reparto/Data pronto in varianti_det_ord_ven_prod: riga " + ls_riga
	return -1
end if

end function

public function integer uof_mrp_disimpegna_ordine (long al_barcode, ref string as_errore);integer			li_anno_reg, li_ret
long				ll_num_reg, ll_prog_riga_reg, ll_tot, ll_index
string			ls_cod_reparto, ls_sql
datastore		lds_righe

setnull(li_anno_reg)
uof_barcode_anno_reg(al_barcode, li_anno_reg, ll_num_reg, ll_prog_riga_reg, ls_cod_reparto, as_errore)

if isnull(ls_cod_reparto) or ls_cod_reparto="" then return 0

if ll_prog_riga_reg>0 then
	//tipo A
	li_ret = uof_mrp_disimpegna_ordine(li_anno_reg, ll_num_reg, ll_prog_riga_reg, ls_cod_reparto, as_errore)
	
	if li_ret<0 then
		return -1
	end if
	
else
	//tipo B o C
	ls_sql = "select distinct dp.anno_registrazione,dp.num_registrazione,dp.prog_riga_ord_ven "+&
				"from det_ordini_produzione as dp "+&
				"join det_ord_ven as d on d.cod_azienda=dp.cod_azienda and "+&
													"d.anno_registrazione=dp.anno_registrazione and "+&
													"d.num_registrazione=dp.num_registrazione and "+&
													"d.prog_riga_ord_ven=dp.prog_riga_ord_ven "+&
				"join tes_ord_ven as t on t.cod_azienda=d.cod_azienda and  "+&
													"t.anno_registrazione=d.anno_registrazione and "+&
													"t.num_registrazione=d.num_registrazione "+&
				"where dp.cod_azienda='"+s_cs_xx.cod_azienda+"' and dp.progr_tes_produzione="+string(al_barcode)+" and "+&
    							"d.flag_evasione <> 'E' and d.cod_prodotto is not null and "+&
        						"(d.quan_ordine - d.quan_evasa - d.quan_in_evasione) > 0 and "+&
							"dp.cod_reparto='"+ls_cod_reparto+"' "
	
	ll_tot = guo_functions.uof_crea_datastore( lds_righe, ls_sql, as_errore)
	
	if ll_tot<0 then
		destroy lds_righe
		return -1
		
	elseif ll_tot=0 then
		//no righe corrispondenti: esci
		return 0
		
	end if
	
	for ll_index=1 to ll_tot
		li_anno_reg			= lds_righe.getitemnumber(ll_index, 1)
		ll_num_reg				= lds_righe.getitemnumber(ll_index, 2)
		ll_prog_riga_reg		= lds_righe.getitemnumber(ll_index, 3)
		
		li_ret = uof_mrp_disimpegna_ordine(li_anno_reg, ll_num_reg, ll_prog_riga_reg, ls_cod_reparto, as_errore)
	
		if li_ret<0 then
			destroy lds_righe
			return -1
		end if
		
	next
	
	destroy lds_righe
	
end if


return 0
end function

public function integer uof_mrp_disimpegna_ordine (integer al_anno_reg, long al_num_reg, long al_prog_riga_reg, string as_cod_reparto, ref string as_errore);string		ls_flag_mrp


//se il reparto non prevede la gestione MRP esci
select flag_mrp
into :ls_flag_mrp
from anag_reparti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_reparto=:as_cod_reparto;

if ls_flag_mrp<>'S' then return 0


//disimpegno
update mov_mrp
set flag_elaborato='S'
where 	cod_azienda=:s_cs_xx.cod_azienda and
			tipo_doc_origine='ORD_VEN' and
			anno_doc_origine=:al_anno_reg and
			num_doc_origine=:al_num_reg and
			prog_riga_doc_origine=:al_prog_riga_reg and
			cod_reparto=:as_cod_reparto;

if sqlca.sqlcode<0 then
	as_errore = "Errore in disimpegno riga ordine "+string(al_anno_reg)+"/"+string(al_num_reg)+"/"+string(al_prog_riga_reg)+&
						" e reparto "+as_cod_reparto+" in tabella mov_mrp: "+sqlca.sqlerrtext
	return -1
end if


return 0
end function

public subroutine uof_scrivi_log_sistema (string as_flag_log, string as_valore);uo_log_sistema				luo_log

luo_log = create uo_log_sistema
luo_log.uof_write_log_sistema_not_sqlca(as_flag_log, as_valore)
destroy luo_log

return
end subroutine

public function integer uof_get_posizione (integer ai_anno, long al_numero, long al_riga, string as_cod_reparto, ref string as_posizione, ref string as_errore);long			ll_barcode_tes
string			ls_temp


as_posizione = ""

//cerca prima nella det_ordini_produzione (ordine tipo A)
select posizione, progr_tes_produzione
into   :ls_temp, :ll_barcode_tes
from   det_ordini_produzione
where	cod_azienda=:s_cs_xx.cod_azienda and 	anno_registrazione=:ai_anno and
			num_registrazione=:al_numero and prog_riga_ord_ven=:al_riga and cod_reparto=:as_cod_reparto;
			 

choose case sqlca.sqlcode
		
	case is < 0
		as_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
		
	case 0
		//hai trovato il record nella det_ordini produzione, ma potrebbe essere comunque di tipo B,C
		if not isnull(ll_barcode_tes) then
			//allora è di tipo B o C, perchè il progr_tes_produzione non è nullo ...
			select posizione
			into   :ls_temp	
			from   tes_ordini_produzione
			where  cod_azienda=:s_cs_xx.cod_azienda and progr_tes_produzione=:ll_barcode_tes;
			
		end if
		
		if isnull(ls_temp) then ls_temp = ""
		as_posizione = ls_temp
	
		return 0

	case 100
		//nessun record in det_ordini_produzione, quindi sicuramente è di tipo B,C
		//capita se lancio questa funzione con l'argomento prog_riga nullo
		select posizione
		into   :ls_temp	
		from   tes_ordini_produzione
		where	cod_azienda=:s_cs_xx.cod_azienda and  anno_registrazione=:ai_anno and
					num_registrazione=:al_numero and  cod_reparto=:as_cod_reparto;
		
		if sqlca.sqlcode < 0 then 
			as_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		if sqlca.sqlcode = 100 then
			as_errore = "Non è stato trovato alcun ordine!"
			return -1
		end if

		if isnull(ls_temp) then ls_temp = ""
		as_posizione = ls_temp
		return 0
	
end choose

return 0
end function

public function boolean uof_check_posizione (integer ai_anno, long al_numero, long al_riga, string as_posizione);long			ll_count
string			ls_tipo_stampa, ls_errore


if isnull(as_posizione) or as_posizione="" then return true

uof_trova_tipo(ai_anno, al_numero, ls_tipo_stampa, ls_errore)

setnull(ll_count)

choose case ls_tipo_stampa
	case "A"
		select count(*)
		into   :ll_count
		from   det_ordini_produzione
		where	cod_azienda=:s_cs_xx.cod_azienda and 	anno_registrazione=:ai_anno and
					num_registrazione=:al_numero and prog_riga_ord_ven=:al_riga and posizione=:as_posizione;
		
	case "B","C"
		select count(*)
		into   :ll_count
		from   tes_ordini_produzione
		where	cod_azienda=:s_cs_xx.cod_azienda and 	anno_registrazione=:ai_anno and
					num_registrazione=:al_numero and posizione=:as_posizione;
	
	case else
		return false
		
end choose

if ll_count>0 then
	//presente
	return true
end if


return false
end function

public function integer uof_trova_cliente (long fl_barcode, ref string fs_cod_cliente, ref string fs_errore);

integer li_risposta, li_anno_registrazione
long ll_num_registrazione
string ls_tipo_stampa,ls_cod_tipo_ord_ven


select anno_registrazione, num_registrazione
into   :li_anno_registrazione, :ll_num_registrazione
from   det_ordini_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr_det_produzione=:fl_barcode;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1

elseif sqlca.sqlcode = 100 then
	select anno_registrazione, num_registrazione
	into   :li_anno_registrazione, :ll_num_registrazione
	from   tes_ordini_produzione
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    progr_tes_produzione=:fl_barcode;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1

	elseif sqlca.sqlcode = 100 then
		fs_errore = "Attenzione! Non è stato trovato alcun ordine con questo codice a barre. Verificare di aver letto il codice a barre corretto presente nella stampa di produzione in basso a sinistra o di aver inserito correttamente i dati in modalità manuale."
		return -1	
	end if

end if


select cod_cliente
into :fs_cod_cliente
from tes_ord_ven
where	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:li_anno_registrazione and
			num_registrazione=:ll_num_registrazione;

return 0



end function

public function string uof_deposito_giro_consegna (string as_cod_giro_consegna);string				ls_cod_deposito_giro

//torna il codice del deposito del giro consegna (se non è NULL, altrimenti torna "")
if g_str.isnotempty(as_cod_giro_consegna) then
	select cod_deposito
	into :ls_cod_deposito_giro
	from tes_giri_consegne
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_giro_consegna=:as_cod_giro_consegna;
	
	if g_str.isnotempty(ls_cod_deposito_giro) then return ls_cod_deposito_giro
end if

return ""
end function

public function string uof_deposito_giro_consegna (integer ai_anno_ordine, long al_num_ordine);string				ls_cod_deposito_giro

//torna il codice del deposito del giro consegna della testata ordine (se non è NULL, altrimenti torna "")
select tes_giri_consegne.cod_deposito
into :ls_cod_deposito_giro
from tes_ord_ven
join tes_giri_consegne on	tes_giri_consegne.cod_azienda=tes_ord_ven.cod_azienda and
									tes_giri_consegne.cod_giro_consegna=tes_ord_ven.cod_giro_consegna						
where 	tes_ord_ven.cod_azienda=:s_cs_xx.cod_azienda and
			tes_ord_ven.anno_registrazione=:ai_anno_ordine and
			tes_ord_ven.num_registrazione=:al_num_ordine;
	
if g_str.isnotempty(ls_cod_deposito_giro) then return ls_cod_deposito_giro

return ""

end function

public function integer uof_allega_docs_produzione (long fl_barcode, integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga, ref string fs_errore);
string						ls_tipo, ls_sql, ls_tabella_gestione, ls_cod_nota, ls_filename, ls_dir, ls_file, ls_ext, ls_cont, ls_docname[], ls_desktop, &
							ls_databasedocs, ls_servernamedocs, ls_ordine
							
datastore				lds_data

long						ll_cont, ll_len, ll_maxKB, ll_prog_mimetype

blob						lbl_documento

transaction				lt_tran_docs

integer					li_ret


ls_desktop = guo_functions.uof_get_user_desktop_folder()

//selezione file immagine in "ls_filename"
if GetFileOpenName("Select File", ls_filename, ls_docname[], "", "Images,*.bmp;*.gif;*.jpg;*.jpeg", ls_desktop) = 1 then
else
	return 0
end if


//in base al tipo ordine (A, B, C) il barcode letto corrisponde a progr_det_produzione (tipo A) oppure progr_tes_produzione (tipo B, C)
li_ret = uof_trova_tipo(fl_barcode, ls_tipo, fs_errore)
if li_ret < 0 then
	return -1
end if

ls_tabella_gestione = "det_ord_ven_note"

if ls_tipo = "B" or ls_tipo = "C" then
	//caso B, C: allega alla testata ordine vendita
	setnull(fl_prog_riga)
	ls_tabella_gestione = "tes_ord_ven_note"
	ls_ordine = "testata ordine vendita n." + string(fi_anno_registrazione) + "/" + string(fl_num_registrazione)
else
	ls_ordine = "riga ordine vendita n." + string(fi_anno_registrazione) + "/" + string(fl_num_registrazione) + "/" + string(fl_prog_riga)
end if


ls_sql = "select max(cod_nota) from " + ls_tabella_gestione + " " + &
			"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " +&
						"anno_registrazione = " + string(fi_anno_registrazione) + " and " + &
						"num_registrazione = " + string(fl_num_registrazione) + " and " + &
						"isnumeric(cod_nota)=1"

if ls_tipo="A" then
	ls_sql += " and prog_riga_ord_ven = " + string(fl_prog_riga)
end if

ll_cont = guo_functions.uof_crea_datastore( lds_data, ls_sql, fs_errore)

if ll_cont< 0 then
	destroy lds_data
	fs_errore = "Errore creazione datastore conteggio: " + fs_errore
	return -1
elseif ll_cont = 0 or isnull(ll_cont) then
	ll_cont = 1
else
	ls_cont = lds_data.getitemstring(1, 1)
	if isnull(ls_cont) or ls_cont="" then ls_cont = "0"
	ll_cont = long(ls_cont)
	ll_cont += 1
end if
destroy lds_data


ls_cod_nota = string(ll_cont, "000")
guo_functions.uof_file_to_blob(ls_filename, lbl_documento)

//lunghezza in BYTE del documento trascinato
ll_len = lenA(lbl_documento)


guo_functions.uof_get_parametro("MLD", ll_maxKB)
if not isnull(ll_maxKB) and ll_maxKB>0 then
	if ll_len > ll_maxKB * 1024 then
		//documento oltre la grandezza massima pre-stabilita
		fs_errore = "Attenzione: grandezza file (" + string(ll_len) + " BYTE) superiore a quella prestabilita ("+string(ll_maxKB * 1024)+" KB) come da parametro multiazienda MLD"
		return -1
	end if
end if

guo_functions.uof_get_file_info( ls_filename, ls_dir, ls_file, ls_ext)
ls_ext = lower(ls_ext)

select prog_mimetype
into :ll_prog_mimetype
from tab_mimetype
where cod_azienda = :s_cs_xx.cod_azienda and
		estensione = :ls_ext;
if sqlca.sqlcode <> 0 then
	fs_errore = "Attenzione: estensione file " + ls_ext + " non prevista da sistema."
	return -1
end if

//--------------------------------------------------------------------------------------
//INSERIMENTO
//creazione transazione
ls_databasedocs = ""
ls_servernamedocs = ""
Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "databasedocs",ls_databasedocs)
Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "servernamedocs",ls_servernamedocs)


if g_str.isnotempty(ls_servernamedocs) and g_str.isnotempty(ls_databasedocs) then
	if not guo_functions.uof_create_transaction_from( sqlca, lt_tran_docs, ls_servernamedocs, ls_databasedocs, fs_errore)  then
		fs_errore = "Errore in creazione transazione: " + fs_errore
		return -1
	end if
else
	if not guo_functions.uof_create_transaction_from( sqlca, lt_tran_docs, fs_errore)  then
		fs_errore = "Errore in creazione transazione: " + fs_errore
		return -1
	end if
end if


//-------------------------------------------------------------------------------------
//preparazione inserimento
guo_functions.uof_replace_string(ls_filename, "'", "")
guo_functions.uof_replace_string(ls_filename, '"', "")

ls_sql = "insert into " + ls_tabella_gestione + " "+&  
				"( 	   cod_azienda,"+&
					 "anno_registrazione,"+&
					  "num_registrazione,"
					  
if ls_tipo="A" then
	ls_sql += "prog_riga_ord_ven,"
end if
					
ls_sql +=			  "cod_nota,"+&
					  "des_nota,"+&
					  "note,"+&
					  "prog_mimetype) "+&  
		"values ('" + s_cs_xx.cod_azienda + "', "+&
				  string(fi_anno_registrazione)+","+&
				  string(fl_num_registrazione)+","
				  
if ls_tipo="A" then			  
	ls_sql +=  string(fl_prog_riga)+","
end if

ls_sql += 	  "'"+ls_cod_nota+"',"+&
				  "'"+ls_docname[1]+"',"+&
				  "null,"+&
				  string(ll_prog_mimetype) +")"

execute immediate :ls_sql using lt_tran_docs;

//----------------------------------------------------------------------------------
//updateblob
if lt_tran_docs.sqlcode = 0 then
	
	if ls_tipo="A" then
		
		updateblob det_ord_ven_note
		set note_esterne = :lbl_documento
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :fi_anno_registrazione and
				num_registrazione = :fl_num_registrazione and
				prog_riga_ord_ven = :fl_prog_riga and
				cod_nota = :ls_cod_nota
		using lt_tran_docs;
		
	else
		updateblob tes_ord_ven_note
		set note_esterne = :lbl_documento
		where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :fi_anno_registrazione and
					num_registrazione = :fl_num_registrazione and
					cod_nota = :ls_cod_nota
		using lt_tran_docs;
		
	end if
	
	if lt_tran_docs.sqlcode <> 0 then
		fs_errore = "Errore nell'inserimento del documento digitale: " + lt_tran_docs.sqlerrtext
		rollback using lt_tran_docs;
		disconnect using lt_tran_docs;
		destroy lt_tran_docs;
		return -1
	end if
	
else
	if lt_tran_docs.sqlcode <> 0 then
		fs_errore = "Errore nell'inserimento del documento digital: " + lt_tran_docs.sqlerrtext
		rollback using lt_tran_docs;
		disconnect using lt_tran_docs;
		destroy lt_tran_docs;
		return -1
	end if
end if

//se arrivi fin qui fai commit della transazione documenti
commit using lt_tran_docs;
disconnect using lt_tran_docs;
destroy lt_tran_docs;

uof_scrivi_log_sistema("ALLEGAPROD", "Allegato documento "+ls_filename+" su "+ls_ordine)


return 0
end function

public function integer uof_inserisci_det (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_reparti_trovati[], decimal fd_quan_ordine, ref string fs_errore);// Funzione che esegue l'inserimento in det_ordini_produzione
// nome: uof_inserisci_det
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 21-11-2003
//		Autore Diego Ferrari

long					ll_progr_det_produzione, ll_num_reparti, ll_prog_riga_lancio, ll_barcode_produzione, ll_max_progressivo
integer				li_risposta
string					ls_errore, ls_sql, ls_cod_reparto_arrivo,ls_tipo_stampa
datastore			lds_data
datetime				ldt_data_pronto, ldt_data_arrivo


li_risposta = uof_trova_max_barcode(ll_max_progressivo,  ls_errore)
																						
if li_risposta < 0 then 
	fs_errore = ls_errore
	return -1
end if

if isnull(ll_max_progressivo) or ll_max_progressivo=0 then
	ll_max_progressivo=0
end if

for ll_num_reparti = 1 to upperbound(fs_cod_reparti_trovati[])
	
	select		progr_det_produzione
	into		:ll_barcode_produzione
	from		varianti_det_ord_ven_prod
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione= :fi_anno_registrazione and
				num_registrazione= :fl_num_registrazione  and
				prog_riga_ord_ven= :fl_prog_riga_ord_ven and
				cod_reparto= :fs_cod_reparti_trovati[ll_num_reparti] ;
	if sqlca.sqlcode = 0 then
		if not isnull(ll_barcode_produzione) and ll_barcode_produzione > 0 then
				ll_progr_det_produzione = ll_barcode_produzione
		else
			ll_max_progressivo ++
			ll_progr_det_produzione = ll_max_progressivo
		end if
	else
		ll_max_progressivo ++
		ll_progr_det_produzione = ll_max_progressivo
	end if
	
	
	insert into det_ordini_produzione		
	(cod_azienda,
	 progr_det_produzione,
	 progr_tes_produzione,
	 anno_registrazione,
	 num_registrazione,
	 prog_riga_ord_ven,
	 cod_prodotto,
	 cod_reparto,
	 tempo_totale_impiegato,
	 quan_da_produrre,
	 quan_prodotta,
	 flag_fine_fase,
	 data_fine_fase)
	values
	(:s_cs_xx.cod_azienda,
	 :ll_progr_det_produzione,
	 null,
	 :fi_anno_registrazione,
	 :fl_num_registrazione,
	 :fl_prog_riga_ord_ven,
	 :fs_cod_prodotto,
	 :fs_cod_reparti_trovati[ll_num_reparti],
	 0,
	 :fd_quan_ordine,
	 0,
	 'N',
	 null);
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "errore in fase di inserimento in det_stampa_ordini.Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	//###################################################################################################
	//Donato 07/05/2013
	//aggiorno con le eventuali dati pronto, reparto arrivo e data arrivo delle variani_det_ord_ven_prod
	
	if uof_trova_tipo(ll_progr_det_produzione, ls_tipo_stampa, ls_errore)=0 then
	
		if ls_tipo_stampa = "A" then
			//tipo A  --------------------------------------------------------------------------------------------------------------------------
			
			//leggo da varianti_det_ord_ven_prod -------------------------------
			ls_sql = 	"select distinct 	data_pronto,"+&
								"cod_reparto_arrivo,"+&
								"data_arrivo "+&
						"from varianti_det_ord_ven_prod "+&
						"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
								"anno_registrazione="+string(fi_anno_registrazione)+" and "+&
								"num_registrazione="+string(fl_num_registrazione) +" and "+&
								"prog_riga_ord_ven="+string(fl_prog_riga_ord_ven)+" and "+&
								"cod_reparto='"+fs_cod_reparti_trovati[ll_num_reparti]+"' "
			
			//mi basta una sola riga, se c'è
			li_risposta = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
			
			if li_risposta>0 then
				ldt_data_pronto = lds_data.getitemdatetime(1, "data_pronto")
				ls_cod_reparto_arrivo = lds_data.getitemstring(1, "cod_reparto_arrivo")
				ldt_data_arrivo = lds_data.getitemdatetime(1, "data_arrivo")
				
				// memorizzo il numero di ordine in modo che se rilancio la produzione o ristampo, riprendo lo stesso numero (Alusistemi 27/5/2019)
				update 	varianti_det_ord_ven_prod
				set 		progr_det_produzione = :ll_progr_det_produzione
				where 	cod_azienda = :s_cs_xx.cod_azienda and
							anno_registrazione= :fi_anno_registrazione and
							num_registrazione= :fl_num_registrazione  and
							prog_riga_ord_ven= :fl_prog_riga_ord_ven and
							cod_reparto= :fs_cod_reparti_trovati[ll_num_reparti] ;
				if sqlca.sqlcode < 0 then 
					fs_errore = "errore in memorizzazione barcode nella riga variante del reparto.Errore sul DB: " + sqlca.sqlerrtext
					return -1
				end if
					
			end if
			
			destroy lds_data
			
		else
			//tipo B o C ------------------------------------------------------------------------------------------------------------------------------
			
			//leggo da det_ord_ven -----------------------------------
			select 	data_pronto_sfuso,
						cod_reparto_arrivo,
						data_arrivo
			into	:ldt_data_pronto,
					:ls_cod_reparto_arrivo,
					:ldt_data_arrivo
			from det_ord_ven
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						anno_registrazione=:fi_anno_registrazione and
						num_registrazione=:fl_num_registrazione and
						prog_riga_ord_ven=:fl_prog_riga_ord_ven;
						
			if sqlca.sqlcode=0 then
				li_risposta=1
			else
				li_risposta=-1
			end if
			
		end if
		
		//se tutto OK ***************************************************************
		//aggiornamento in det_ordini_produzione
		if li_risposta>0 then
			
			if ls_cod_reparto_arrivo="" then setnull(ls_cod_reparto_arrivo)
			
			update det_ordini_produzione
				set 	data_pronto=:ldt_data_pronto,
						cod_reparto_arrivo=:ls_cod_reparto_arrivo,
						data_arrivo=:ldt_data_arrivo
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						progr_det_produzione=:ll_progr_det_produzione;
			
		end if
		//*************************************************************************
		
	end if
	
	ls_errore = ""
	//###################################################################################################
	
next


return 0
end function

public function integer uof_lancia_produzione (long fl_num_registrazione, integer fi_anno_registrazione, integer fi_prog_riga_ord_ven, string fs_cod_tipo_ord_ven, datetime fdt_data_consegna, ref string fs_errore);/* Funzione che esegue il lancio di produzione per PROGETTOTENDA SPA
 nome: uof_lancia_produzione
 tipo: intero
 		 0 passed
			-1 failed
 
  
		Creata il 20-11-2003
		Autore Diego Ferrari
		
		18/05/2012 (EnMe): modificata la funzione in modo da accettare il lancio di produzione di una singola riga d'ordine
		18/04/2016 (EnMe): modificata la funzione per gestire il raggruppamanento ordini per singolo lancio produzione; quindi ora vengono passati anno/num lancio produzione che 
		se passati NULL indicano che è necessario generare un nuovo lancio produzione.
*/

long			ll_riga, ll_selected[], ll_prog_riga_ord_ven, ll_num_reparti, ll_prog_riga_comanda, ll_progr_det_produzione, ll_t, ll_i, ll_r

integer		li_risposta

string			ls_cod_prodotto, ls_cod_tipo_det_ven, ls_cod_versione, ls_cod_reparto_test, ls_tipo_stampa, ls_flag_stampa_fase, ls_sql, ls_cod_tipo_det_ven_addizionali,& 
		 		ls_cod_tipo_ord_ven, ls_errore, ls_cod_reparto, ls_cod_reparti_trovati[], ls_test, ls_cod_reparti_trovati_tes[], ls_reparti_letti[], ls_vuoto[]
				 
datetime		ldt_data_consegna_riga

decimal{4}	ld_quan_ordine

boolean		lb_test


select 	flag_tipo_bcl
into   		:ls_tipo_stampa
from  		tab_tipi_ord_ven
where  	cod_azienda = :s_cs_xx.cod_azienda and 	cod_tipo_ord_ven = :fs_cod_tipo_ord_ven;

if sqlca.sqlcode < 0 then 
	ls_errore = "Errore in ricerca tipo stampa ordine in tabella tab_tipi_ord_ven~r~n " + sqlca.sqlerrtext
	return -1
end if

	

// verifico il tipo di report 'A'=Report_1 o tende da sole, 'B'=Report_2 o sfuso , 'C'=Report_3 o tende tecniche
choose case ls_tipo_stampa

	case 'A'								//************** Inizio Case TENDE DA SOLE
		
		declare  righe_det_ord_ven_1 dynamic cursor for sqlsa;
		
		ls_sql = 	" select   cod_prodotto, cod_tipo_det_ven, prog_riga_ord_ven, cod_versione, data_consegna, quan_ordine from det_ord_ven where cod_azienda='" + s_cs_xx.cod_azienda + "' " + &
					" and  anno_registrazione= " + string(fi_anno_registrazione ) + &
					" and  num_registrazione= " + string(fl_num_registrazione)
					
		if fi_prog_riga_ord_ven > 0 and not isnull(fi_prog_riga_ord_ven) then
			ls_sql += " and prog_riga_ord_ven = " + string(fi_prog_riga_ord_ven)
		end if
		
		ls_sql += " and num_riga_appartenenza=0 and flag_blocco='N' order by prog_riga_ord_ven "
		
		prepare sqlsa from :ls_sql;
		
		open dynamic righe_det_ord_ven_1;
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in OPEN del cursore righe_det_ord_ven_1 (uof_lancia_produzione).~r~n" + sqlca.sqlerrtext + "~r~nCONTATTARE IL SERVIZIO DI ASSISTENZA"
			f_scrivi_log(fs_errore)
			rollback;
			return -1
		end if
		
		do while true														//  scorre tutti i dettagli dell'ordine
			fetch righe_det_ord_ven_1
			into  	:ls_cod_prodotto,
					:ls_cod_tipo_det_ven,
					:ll_prog_riga_ord_ven,
					:ls_cod_versione,
					:ldt_data_consegna_riga,
					:ld_quan_ordine;

					
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode < 0 then 
				fs_errore = "Caso Report_1, lettura cursore su det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext
				close righe_det_ord_ven_1;
				return -1
			end if
			
			
			li_risposta=uof_reparti(fi_anno_registrazione, & 
											fl_num_registrazione, & 
											ll_prog_riga_ord_ven,&
											ls_cod_prodotto,&
											ls_cod_versione, &
											ls_cod_reparti_trovati[],&
											ls_errore)

			choose case li_risposta
				case -1
					close righe_det_ord_ven_1;
					fs_errore=ls_errore
					return -1
				
				case -2
					continue
				
				case 0	
				
					li_risposta =uof_inserisci_det (fi_anno_registrazione, &
															  fl_num_registrazione, &
															  ll_prog_riga_ord_ven, &
															  ls_cod_prodotto, &
															  ls_cod_reparti_trovati[], &
															  ld_quan_ordine,&
															  ls_errore )
																
					if li_risposta = -1 then
						fs_errore=ls_errore
						return -1
					end if
					
			end choose		
			
			if uof_crea_matricola (fi_anno_registrazione,  fl_num_registrazione, ll_prog_riga_ord_ven, ld_quan_ordine, ref ls_errore ) < 0 then
				fs_errore = "Funzione uof_lancia_produzione(): " + ls_errore
				return -1
			end if
			
		loop
		
		close righe_det_ord_ven_1;
		
		
										//************** Fine Case TENDE DA SOLE **************************



	case 'B'		                  //************** Inizio Case  SFUSO
		
		ll_i = 1
		f_null_array(ls_cod_reparti_trovati_tes[]) 
		
		declare righe_det_ord_ven_3 dynamic cursor for sqlsa;
		
		ls_sql = 	" select  cod_prodotto,  cod_tipo_det_ven, prog_riga_ord_ven, cod_versione, quan_ordine from det_ord_ven where cod_azienda= '" + s_cs_xx.cod_azienda + "' " + &
					" and  anno_registrazione= " + string(fi_anno_registrazione ) + &
					" and  num_registrazione= " + string(fl_num_registrazione)

		if fi_prog_riga_ord_ven > 0 and not isnull(fi_prog_riga_ord_ven) then
			ls_sql += " and prog_riga_ord_ven = " + string(fi_prog_riga_ord_ven)
		end if
		
		ls_sql += " and  num_riga_appartenenza=0 and  flag_blocco='N' and  cod_tipo_det_ven <> 'ADD' "
		
		prepare sqlsa from :ls_sql;
	
		open dynamic righe_det_ord_ven_3;
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in OPEN del cursore righe_det_ord_ven_3 (uof_lancia_produzione).~r~n" + sqlca.sqlerrtext + "~r~nCONTATTARE IL SERVIZIO DI ASSISTENZA"
			f_scrivi_log(fs_errore)
			rollback;
			return -1
		end if
		
		do while true															//  scorre tutti i dettagli dell'ordine
			fetch righe_det_ord_ven_3
			into  :ls_cod_prodotto,
					:ls_cod_tipo_det_ven,
					:ll_prog_riga_ord_ven,
					:ls_cod_versione,
					:ld_quan_ordine;
					
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode < 0 then 
				fs_errore = "Caso Report_2 sfuso, errore in fase di lettura cursore su det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext
				close righe_det_ord_ven_3;
				rollback;
				return -1
			end if
			
			setnull(ls_cod_reparto)
								
			//legge il reparto associato in anagrafica prodotti ---------
			select cod_reparto
			into   :ls_cod_reparto
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;
			
			if sqlca.sqlcode < 0 then 
				f_scrivi_log("Lancio di produzione PTENDA-->>Caso Report_2 Sfuso. Nell'ordine anno:" + string(fi_anno_registrazione) + & 
								 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + ls_cod_prodotto + & 
								 " che non ha il reparto associato nell'anagrafica prodotti. Verificare.")
				
				continue
			end if
			
			if isnull(ls_cod_reparto) then
				f_scrivi_log("Lancio di produzione PTENDA-->>Caso Report_2 Sfuso. Nell'ordine anno:" + string(fi_anno_registrazione) + & 
								 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + ls_cod_prodotto + & 
								 " che non ha il reparto associato nell'anagrafica prodotti. Verificare.")
				
				continue

			end if
			
			//Donato 19/12/2011
			//verifica se esiste eccezione
			ls_reparti_letti[] = ls_vuoto[]
			ls_cod_reparti_trovati[] = ls_vuoto[]
			if f_reparti_stabilimenti(		fi_anno_registrazione, fl_num_registrazione, ls_cod_prodotto, "", 	ls_cod_reparto, "", "", ls_reparti_letti[], fs_errore)<0 then
				//in fs_errore il messaggio
				close righe_det_ord_ven_3;
				rollback;
				return -1
			end if
		
			//merge con l'array by ref principale
			guo_functions.uof_merge_arrays(ls_cod_reparti_trovati[], ls_reparti_letti[])
			
			//###############################################################################
			//donato 14/03/2012 modifica per prevenire errori di configurazione prodotti in ordine sfuso (no-reparti)
			//dare messaggio all'utente e chiudere il cursore
			if upperbound(ls_cod_reparti_trovati[]) > 0 then
			else
				fs_errore = "Nessun reparto per il prodotto "+ls_cod_prodotto+"  nell'ordine tipo Report 2 "&
									+string(fi_anno_registrazione)+"/"+string(fl_num_registrazione)+"/"+string(ll_prog_riga_ord_ven)
				close righe_det_ord_ven_3;
				
				return -1		
			end if
			//###############################################################################
			
			
			//in ls_cod_reparti_trovati[] ci sono i reparti del prodotto sfuso
			//in realtà sarà sempre UNO solo
			
			//faccio questa porkeria qua per trovarmi bene poi più sotto con la funzione uof_inserisci_tes(...)
			//caso SFUSO: considero sempre e solo la prima componente del vettore reparti
			ls_cod_reparto = ls_cod_reparti_trovati[1]
			ls_cod_reparti_trovati[] = ls_vuoto[]
			ls_cod_reparti_trovati[1] = ls_cod_reparto
			
		
			li_risposta =uof_inserisci_det (fi_anno_registrazione, &
													  fl_num_registrazione, &
													  ll_prog_riga_ord_ven, &
													  ls_cod_prodotto, &
													  ls_cod_reparti_trovati[], &
													  ld_quan_ordine,&
													  ls_errore )
																
			if li_risposta = -1 then
				fs_errore=ls_errore
				return -1
			end if
	
	
			if ll_i = 1 then
				ls_cod_reparti_trovati_tes[1] = ls_cod_reparto
			else
				if ls_cod_reparti_trovati_tes[1] <> ls_cod_reparto then
					lb_test = false
					for ll_r = 1 to upperbound(ls_cod_reparti_trovati_tes[])
						if ls_cod_reparti_trovati_tes[ll_r] = ls_cod_reparto then lb_test = true
					next
					if lb_test = false then
						ll_num_reparti = upperbound(ls_cod_reparti_trovati_tes[])
						ls_cod_reparti_trovati_tes[ll_num_reparti+1] = ls_cod_reparto
					end if
				end if
				
			end if
		
			ll_i++
		
		loop
		
		close righe_det_ord_ven_3;
		
		// inserisce in tabella tes_ordini_produzione
		
//		if upperbound(ls_cod_reparti_trovati_tes[]) >1 then       // controllo che verifica gli ordini di tipo sfuso
//																					 // in modo che i prodotti in ordine appartengano tutti allo stesso reparto
//			fs_errore="Attenzione! Nell'ordine, di tipo sfuso, anno:" + string(fi_anno_registrazione) + & 
//						 " Num:" + string(fl_num_registrazione) + ", i prodotti inseriti non appartengono tutti allo stesso unico reparto "+ & 
//						 " pertanto è necessario verificare che i prodotti inseriti appartengano tutti allo stesso reparto." + &
//						 " Si ricorda che per i prodotti di tipo sfuso il reparto è quello associato in anagrafica prodotti. Il lancio di produzione viene annullato."
//			return -1
//		end if	

		// ENME Con la specifica Viropa_integrazioni Rev 2 questo controllo non è più bloccante, ma è diventata una segnalazione.

		if upperbound(ls_cod_reparti_trovati_tes[]) >1 then       // controllo che verifica gli ordini di tipo sfuso
																					 // in modo che i prodotti in ordine appartengano tutti allo stesso reparto
			if messagebox("SEP","Attenzione! Nell'ordine, di tipo sfuso, anno:" + string(fi_anno_registrazione) + & 
						 " Num:" + string(fl_num_registrazione) + ", i prodotti inseriti non appartengono tutti allo stesso unico reparto "+ & 
						 " pertanto è necessario verificare che i prodotti inseriti appartengano tutti allo stesso reparto." + &
						 " Si ricorda che per i prodotti di tipo sfuso il reparto è quello associato in anagrafica prodotti.~r~nPROSEGUO CON IL LANCIO DI PRODUZIONE ?.",question!,YesNo!,2) = 2 then
				fs_errore= "Lancio di produzione annullato dall'operatore."
				return -1
			end if
			
		end if		

		
		li_risposta =uof_inserisci_tes (fi_anno_registrazione, &
												  fl_num_registrazione, &
												  ls_cod_reparti_trovati_tes[], &
												  ls_errore )
													
			
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if		

		if uof_crea_matricola (fi_anno_registrazione,  fl_num_registrazione, ll_prog_riga_ord_ven, ld_quan_ordine, ref ls_errore ) < 0 then
			fs_errore = "Funzione uof_lancia_produzione(): " + ls_errore
			return -1
		end if
//************** Fine Case SFUSO	

	f_null_array(ls_cod_reparti_trovati[]) 
	f_null_array(ls_cod_reparti_trovati_tes[]) 


	case 'C'                      //************** Inizio Case     TENDE TECNICHE
					
		declare righe_det_ord_ven_2 dynamic cursor for sqlsa;
		
		ls_sql = "select  cod_prodotto, cod_tipo_det_ven, prog_riga_ord_ven, cod_versione, quan_ordine from det_ord_ven where   cod_azienda= '" + s_cs_xx.cod_azienda + "' " + &
					" and  anno_registrazione= " + string(fi_anno_registrazione ) + &
					" and  num_registrazione= " + string(fl_num_registrazione)

		if fi_prog_riga_ord_ven > 0 and not isnull(fi_prog_riga_ord_ven) then
			ls_sql += " and prog_riga_ord_ven = " + string(fi_prog_riga_ord_ven)
		end if
		
		ls_sql += " and num_riga_appartenenza=0 and flag_blocco='N' "
		
		prepare sqlsa from :ls_sql;
		
		open dynamic righe_det_ord_ven_2;
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in OPEN del cursore righe_det_ord_ven_2 (uof_lancia_produzione).~r~n" + sqlca.sqlerrtext + "~r~nCONTATTARE IL SERVIZIO DI ASSISTENZA"
			f_scrivi_log(fs_errore)
			rollback;
			return -1
		end if
		
		do while true														//  scorre tutti i dettagli dell'ordine
			fetch righe_det_ord_ven_2
			into  :ls_cod_prodotto,
					:ls_cod_tipo_det_ven,
					:ll_prog_riga_ord_ven,
					:ls_cod_versione,
					:ld_quan_ordine;
					
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode < 0 then 
				fs_errore = "Caso Report_3 tende tecniche, errore in fase di lettura cursore su det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext
				close righe_det_ord_ven_2;
				rollback;
				return -1
			end if
			
			// Donato 18/06/2012: controllo se si tratta di riga addizionale; in questo caso salto
			if uof_get_tipo_det_ven_add( 	fi_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven, 	ls_cod_tipo_det_ven_addizionali, fs_errore) < 0 then
				fs_errore = "Caso Report_3 tende tecniche, " + fs_errore
				close righe_det_ord_ven_2;
				rollback;
				return -1
			end if
			if ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_addizionali then continue
			
								
			li_risposta=uof_reparti(fi_anno_registrazione, & 
											fl_num_registrazione, & 
											ll_prog_riga_ord_ven,&
											ls_cod_prodotto,&
											ls_cod_versione, &
											ls_cod_reparti_trovati[],&
											ls_errore)

			choose case li_risposta
				case -1
					close righe_det_ord_ven_1;
					fs_errore=ls_errore
					return -1
				
				case -2		//se la configurazione del prodotto corrente non è corretta allora passa al successivo
					continue
				
				case 0	//inserisce i record in det_ordini_produzione
				
					li_risposta =uof_inserisci_det (fi_anno_registrazione, &
															  fl_num_registrazione, &
															  ll_prog_riga_ord_ven, &
															  ls_cod_prodotto, &
															  ls_cod_reparti_trovati[], &
															  ld_quan_ordine,&
															  ls_errore )
																
					if li_risposta = -1 then
						fs_errore=ls_errore
						return -1
					end if
					
					// prepara e aggiorna array che contiene tutti i reparti diversi presenti in tutti i prodotti dell'ordine
					
					if upperbound(ls_cod_reparti_trovati_tes[]) = 0 then
						for ll_t= 1 to  upperbound(ls_cod_reparti_trovati[])
							ls_cod_reparti_trovati_tes[ll_t] = ls_cod_reparti_trovati[ll_t]
						next
						
					else			
						for ll_t= 1 to  upperbound(ls_cod_reparti_trovati[])
							if ls_cod_reparti_trovati_tes[1] <> ls_cod_reparti_trovati[ll_t] then
								lb_test = false
								for ll_r = 1 to upperbound(ls_cod_reparti_trovati_tes[])
									if ls_cod_reparti_trovati_tes[ll_r] = ls_cod_reparti_trovati[ll_t] then lb_test = true
								next
								if lb_test = false then
									ll_num_reparti = upperbound(ls_cod_reparti_trovati_tes[])
									ls_cod_reparti_trovati_tes[ll_num_reparti+1] = ls_cod_reparti_trovati[ll_t]
								end if
							end if
						next	
					
				   end if	
					
			end choose			
			
			
		loop
		
		close righe_det_ord_ven_2;
		
		// inserisce in tabella tes_ordini_produzione
		
	
		li_risposta =uof_inserisci_tes (fi_anno_registrazione, &
												  fl_num_registrazione, &
												  ls_cod_reparti_trovati_tes[], &
												  ls_errore )
													
			
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
		
	
		
		
//************** Fine Case TENDE TECNICHE

	
		
end choose


return 0
end function

public function integer uof_elimina_produzione (long fl_num_registrazione, integer fi_anno_registrazione, string fs_origine_comando, ref string fs_errore);// Funzione che esegue la cancellazione delle tabelle di produzione per PROGETTOTENDA SPA
// nome: uof_elimina_produzione
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 21-11-2003  Diego Ferrari
//
//		3-8-2016	EnMe: aggiunto parametro fs_origine_comando che può avere i seguenti valori: 	T=eseguito dal treeview della finestra stampa ordini produzione
//																															X=eseguito da altre procedure
string		ls_sql
long		ll_rows, ll_i, ll_id
datastore lds_data

delete sessioni_lavoro
where cod_azienda=:s_cs_xx.cod_azienda
and   progr_det_produzione in (select progr_det_produzione 
										 from det_ordini_produzione 
										 where cod_azienda=:s_cs_xx.cod_azienda
										 and   anno_registrazione=:fi_anno_registrazione
										 and   num_registrazione=:fl_num_registrazione);
										 
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

//donato 21/01/2013: elimino eventuali righe di sospensione produzione --------------------------------
delete from tab_sosp_produzione
where cod_azienda=:s_cs_xx.cod_azienda and
		 anno_registrazione = :fi_anno_registrazione and
		num_registrazione  = :fl_num_registrazione;
		
if sqlca.sqlcode < 0 then
	fs_errore = "Errore cancellazione tab_sosp_produzione della riga ordine.~r~n" + sqlca.sqlerrtext
	return -1
end if
//-----------------------------------------------------------------------------------------------------------------


delete det_ordini_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

delete tes_ordini_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

//pulisci eventuali dati in colli --------------------------------------
delete from tab_ord_ven_colli
where  cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :fi_anno_registrazione and
		 	num_registrazione  = :fl_num_registrazione;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore cancellazione tab_ord_ven_colli.~r~n" + sqlca.sqlerrtext
	return -1
end if

if fs_origine_comando <> "T" then
	
	ls_sql = g_str.format("select id_tes_lancio_prod from det_lancio_prod where cod_azienda ='$1' and anno_reg_ord_ven = $2 and num_reg_ord_ven = $3",s_cs_xx.cod_azienda,fi_anno_registrazione,fl_num_registrazione)
	
	ll_rows = guo_functions.uof_crea_datastore(lds_data, ls_sql)
	
	for ll_i = 1 to ll_rows
		ll_id = lds_data.getitemnumber(ll_i,1)
		
		//pulisci tabelle lancio di produzione e ottimizzazione (EnMe 18-04-2016)
		delete 	det_lancio_prod_ottimiz
		where		id_tes_lancio_prod = :ll_id;
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore cancellazione tabella ottimizzazioni.~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		delete 	det_lancio_prod
		where		id_tes_lancio_prod = :ll_id;
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore cancellazione det_lancio_prod.~r~n" + sqlca.sqlerrtext
			return -1
		end if
	next
end if
return 0
end function

public function integer uof_stato_prod_tes_lancio_prod (long al_id_tes_lancio_prod, ref string as_messaggio);string  ls_cod_tipo_det_ven, ls_cod_tipo_det_ven_addizionali, ls_cod_tipo_ord_ven, ls_tipo_stampa, ls_sql
long	 ll_aperte, ll_parziali, ll_chiuse, ll_totale, ll_riga_ord_ven, ll_return, ll_rows, ll_i, ll_anno_ord_ven, ll_num_ord_ven, &
		ll_cont_sessioni_tot, ll_cont_sessioni_chiuse
datastore lds_data


ll_aperte = 0

ll_parziali = 0

ll_chiuse = 0

ll_totale = 0

/*select count(*)
into	:ll_cont_sessioni_tot
from sessioni_lavoro
left outer join det_ordini_produzione on sessioni_lavoro.cod_azienda = det_ordini_produzione.cod_azienda and sessioni_lavoro.progr_det_produzione  = det_ordini_produzione.progr_det_produzione
left outer join det_lancio_prod on det_lancio_prod.cod_azienda = det_ordini_produzione.cod_azienda and det_lancio_prod.anno_reg_ord_ven  = det_ordini_produzione.anno_registrazione  and det_lancio_prod.num_reg_ord_ven  = det_ordini_produzione.num_registrazione  and det_lancio_prod.prog_riga_ord_ven  = det_ordini_produzione.prog_riga_ord_ven
where det_lancio_prod.id_tes_lancio_prod=:al_id_tes_lancio_prod;
*/
select count(*)
into	:ll_cont_sessioni_tot
from det_ordini_produzione 
left outer join det_lancio_prod on det_lancio_prod.cod_azienda = det_ordini_produzione.cod_azienda and det_lancio_prod.anno_reg_ord_ven  = det_ordini_produzione.anno_registrazione  and det_lancio_prod.num_reg_ord_ven  = det_ordini_produzione.num_registrazione  and det_lancio_prod.prog_riga_ord_ven  = det_ordini_produzione.prog_riga_ord_ven
where det_lancio_prod.id_tes_lancio_prod=:al_id_tes_lancio_prod;
	
if sqlca.sqlcode < 0 then 
	as_messaggio = g_str.format( "uo_produzione.uof_stato_prod_tes_lancioprod(). Conteggio sessioni. Errore SQL $1 ",sqlca.sqlerrtext )
	return -1
end if

// non c'è alcun avanzamento
if ll_cont_sessioni_tot = 0 then
	as_messaggio = "Niente"
	return 0
end if

select count(*)
into	:ll_cont_sessioni_chiuse
from  det_ordini_produzione 
left outer join det_lancio_prod on det_lancio_prod.cod_azienda = det_ordini_produzione.cod_azienda and det_lancio_prod.anno_reg_ord_ven  = det_ordini_produzione.anno_registrazione  and det_lancio_prod.num_reg_ord_ven  = det_ordini_produzione.num_registrazione  and det_lancio_prod.prog_riga_ord_ven  = det_ordini_produzione.prog_riga_ord_ven
where det_lancio_prod.id_tes_lancio_prod=:al_id_tes_lancio_prod and det_ordini_produzione.flag_fine_fase='S' ;

if sqlca.sqlcode < 0 then 
	as_messaggio = g_str.format( "uo_produzione.uof_stato_prod_tes_lancioprod(). Conteggio sessioni chiuse. Errore SQL $1 ",sqlca.sqlerrtext )
	return -1
end if

if ll_cont_sessioni_tot > ll_cont_sessioni_chiuse then
	// parziale perchè ci sono delle sessioni, ma non tutte chiuse
	as_messaggio = "Parziale"
	return 2
else
	as_messaggio = "Tutto"
	return 1
end if

/*

ls_sql =  " select det_ord_ven.anno_registrazione,det_ord_ven.num_registrazione,det_ord_ven.prog_riga_ord_ven,det_ord_ven.cod_tipo_det_ven from det_lancio_prod " + &
			" join det_ord_ven on det_lancio_prod.cod_azienda = det_ord_ven.cod_azienda and det_lancio_prod.anno_reg_ord_ven = det_ord_ven.anno_registrazione and det_lancio_prod.num_reg_ord_ven = det_ord_ven.num_registrazione and det_lancio_prod.prog_riga_ord_ven = det_ord_ven.prog_riga_ord_ven " + &
			g_str.format(" where det_lancio_prod.cod_azienda = '$1' and det_ord_ven.cod_prodotto is not null and det_ord_ven.num_riga_appartenenza = 0 and det_ord_ven.flag_blocco = 'N' and det_lancio_prod.id_tes_lancio_prod= $2 ",s_cs_xx.cod_azienda,al_id_tes_lancio_prod) + &
			" order by det_ord_ven.anno_registrazione, det_ord_ven.num_registrazione,det_ord_ven.prog_riga_ord_ven"

ll_rows = guo_functions.uof_crea_datastore(lds_data, ls_sql)

for ll_i = 1 to ll_rows
	
	
	ll_anno_ord_ven = lds_data.getitemnumber(ll_i,1)
	ll_num_ord_ven = lds_data.getitemnumber(ll_i,2)
	ll_riga_ord_ven = lds_data.getitemnumber(ll_i,3)
	ls_cod_tipo_det_ven = lds_data.getitemstring(ll_i,4)
	
	//###########################################################################################
	//Donato 28/06/2012 se l'ordine è di tipo report 2 (sfuso) salta il controllo sul tipo det ven addizionali, altrimenti il semaforo produzione
	//in testata ordine non funziona
	select cod_tipo_ord_ven
	into   :ls_cod_tipo_ord_ven
	from   tes_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_ord_ven
	and    num_registrazione=:ll_num_ord_ven;
	
	if sqlca.sqlcode < 0 then 
		as_messaggio = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	select flag_tipo_bcl
	into   :ls_tipo_stampa
	from   tab_tipi_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;
	
	if sqlca.sqlcode < 0 then 
		as_messaggio = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	if ls_tipo_stampa="B" then
	else
		if uof_get_tipo_det_ven_add(ll_anno_ord_ven, ll_num_ord_ven, ll_riga_ord_ven,	ls_cod_tipo_det_ven_addizionali, as_messaggio) < 0 then
			return -1
		end if
		if ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_addizionali then continue
	end if
	
	select count(*)
	from sessioni_lavoro
	left outer join det_ordini_produzione on sessioni_lavoro.cod_azienda = det_ordini_produzione.cod_azienda and sessioni_lavoro.progr_det_produzione  = det_ordini_produzione.progr_det_produzione
	left outer join det_lancio_prod on det_lancio_prod.cod_azienda = det_ordini_produzione.cod_azienda and det_lancio_prod.anno_reg_ord_ven  = det_ordini_produzione.anno_registrazione  and det_lancio_prod.num_reg_ord_ven  = det_ordini_produzione.num_registrazione  and det_lancio_prod.prog_riga_ord_ven  = det_ordini_produzione.prog_riga_ord_ven
	where det_lancio_prod.id_tes_lancio_prod=1 and 
	
	

	ll_return = uof_stato_prod_det_ord_ven(ll_anno_ord_ven,ll_num_ord_ven,ll_riga_ord_ven,as_messaggio)
	
	choose case ll_return
		case -1
			as_messaggio = "Errore in controllo stato produzione riga " + string(ll_riga_ord_ven) + ":~n" + as_messaggio
			return -1
		case -100
			continue
		case 0
			ll_aperte ++
		case 1
			ll_chiuse ++
		case 2
			ll_parziali ++
		case else
			as_messaggio = "Errore in controllo stato produzione riga " + string(ll_riga_ord_ven) + ":~nValore non previsto"
			return -1
	end choose
	
	ll_totale ++
	
next

if ll_totale = 0 then
	as_messaggio = "Non esiste alcuna riga in produzione per l'ordine indicato"
	return -100
end if
	
if ll_chiuse = 0 and ll_parziali = 0 then
	as_messaggio = "Niente"
	return 0
else
	if ll_chiuse = ll_totale then
		as_messaggio = "Tutto"
		return 1
	else
		as_messaggio = "Parziale"
		return 2
	end if
end if
*/
end function

public function integer uof_crea_matricola (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, decimal al_quan_ordine, ref string as_errore);// Enrico 18/10/2016
// provvede alla scrittira del numero matricola nella tabella dedicata
string		ls_matricola
long		ll_i, ll_progr_det_produzione
datetime	ldt_today

ldt_today = datetime(today())

// cancello eventuali vecchie matricole
delete det_ord_ven_ologramma
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :al_anno_registrazione and 
			num_registrazione = :al_num_registrazione and 
			prog_riga_ord_ven = :al_prog_riga_ord_ven;
			
select min(progr_det_produzione)
into	:ll_progr_det_produzione
from	det_ordini_produzione
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :al_anno_registrazione and
		num_registrazione = :al_num_registrazione and
		prog_riga_ord_ven = :al_prog_riga_ord_ven;
if sqlca.sqlcode = 100 then
	as_errore = g_str.format("Errore Creazione Matricola; l'ordine $1-$2-$3 non ha i dati di produzione.", al_anno_registrazione ,al_num_registrazione  , al_prog_riga_ord_ven )
	return -1
end if

for ll_i = 1 to al_quan_ordine
	ls_matricola = uof_genera_matricola( today(), ll_progr_det_produzione, ll_i)
	
	INSERT INTO det_ord_ven_ologramma  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           prog_riga_ord_ven,   
           progressivo,   
           cod_ologramma,   
           data_assegnazione,   
           operaio_assegnazione,   
           data_verifica,   
           operaio_verifica,   
           flag_riparazione,   
           flag_verificato )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :al_anno_registrazione,   
           :al_num_registrazione,   
           :al_prog_riga_ord_ven,   
           :ll_i,   
           :ls_matricola,   
           :ldt_today,   
           null,   
           null,   
           null,   
           'N',   
           'N' )  ;
	if sqlca.sqlcode < 0 then
		as_errore = g_str.format("Funcion uof_crea_matricola(), errore in insert SQL in tabella det_ord_ven_ologramma.~r~n $1",sqlca.sqlerrtext)
		return -1
	end if
next
	
return 0
end function

public function string uof_genera_matricola (date adt_date, long al_progr_produzione, long al_num_tenda);/**
 * enrico
 * 18/10/2016
 *
Calcolo codice matricola come  AASSGPPPPX
AA= due cifre anno esercizio
SS= settimana dell'anno
G=Giorno della settimana
PPPP=Riga ordine tenda
X=numero tenda (nel caso siano più di 1 nella stessa riga)
 **/
 
string		ls_num_matricola, ls_anno_esercizio
int 		li_week, li_week_day
long 		ll_min, ll_max, ll_cont
date 		ld_januaryfirst

ls_anno_esercizio = string(f_anno_esercizio())
ls_anno_esercizio = right(ls_anno_esercizio,2)

li_week_day = daynumber(adt_date) - 1
if li_week_day = 0 then li_week_day = 7
li_week = guo_functions.uof_get_week_number(adt_date)

ls_num_matricola = ls_anno_esercizio + string(li_week,"00") + string(li_week_day,"0") + string(al_progr_produzione) + string(al_num_tenda,"0")
 
return ls_num_matricola
end function

public function integer uof_elimina_produzione_riga (long fl_num_registrazione, integer fi_anno_registrazione, integer fi_prog_riga_ord_ven, string fs_origine_comando, ref string fs_errore);// Funzione che esegue la cancellazione delle tabelle di produzione per PROGETTOTENDA SPA
// nome: uof_elimina_produzione
// tipo: intero
// 		 0 passed
//			-1 failed
// 
//  
//		Creata il 21-11-2003
//		Autore Diego Ferrari
string			ls_sql
long			ll_rows, ll_i, ll_id
datastore	lds_data

delete sessioni_lavoro
where cod_azienda=:s_cs_xx.cod_azienda
and   progr_det_produzione in (select 	progr_det_produzione 
										 from 		det_ordini_produzione 
										 where 	cod_azienda=:s_cs_xx.cod_azienda and
													anno_registrazione=:fi_anno_registrazione and
										 			num_registrazione=:fl_num_registrazione and
													prog_riga_ord_ven = :fi_prog_riga_ord_ven );
										 
if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if


//donato 21/01/2013: elimino eventuali righe di sospensione produzione --------------------------------
delete from tab_sosp_produzione
where cod_azienda=:s_cs_xx.cod_azienda and
		 anno_registrazione = :fi_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione and
		 (prog_riga_ord_ven = :fi_prog_riga_ord_ven or prog_riga_ord_ven is null);
		 
if sqlca.sqlcode < 0 then
	fs_errore = "Errore cancellazione tab_sosp_produzione della riga ordine.~r~n" + sqlca.sqlerrtext
	return -1
end if
//-----------------------------------------------------------------------------------------------------------------

delete 	det_ordini_produzione
where  	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :fi_anno_registrazione and
			num_registrazione = :fl_num_registrazione and
			prog_riga_ord_ven = :fi_prog_riga_ord_ven;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

//pulisci eventuali dati in colli --------------------------------------
delete 	tab_ord_ven_colli
where  	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :fi_anno_registrazione and
		 	num_registrazione  = :fl_num_registrazione and
			prog_riga_ord_ven = :fi_prog_riga_ord_ven;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore cancellazione tab_ord_ven_colli.~r~n" + sqlca.sqlerrtext
	return -1
end if
//---------------------------------------------------------------------
if fs_origine_comando <> "T" then
	
	ls_sql = g_str.format("select id_tes_lancio_prod from det_lancio_prod where cod_azienda ='$1' and anno_reg_ord_ven = $2 and num_reg_ord_ven = $3 and prog_riga_ord_ven = $4",s_cs_xx.cod_azienda,fi_anno_registrazione,fl_num_registrazione, fi_prog_riga_ord_ven)
	
	ll_rows = guo_functions.uof_crea_datastore(lds_data, ls_sql)
	
	for ll_i = 1 to ll_rows
		ll_id = lds_data.getitemnumber(ll_i,1)
		
		//pulisci tabelle lancio di produzione e ottimizzazione (EnMe 18-04-2016)
		delete 	det_lancio_prod_ottimiz
		where		id_tes_lancio_prod = :ll_id;
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore cancellazione tabella ottimizzazioni.~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		delete 	det_lancio_prod
		where		id_tes_lancio_prod = :ll_id;
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore cancellazione det_lancio_prod.~r~n" + sqlca.sqlerrtext
			return -1
		end if
	next
end if

return 0
end function

public function integer uof_elimina_commessa (long fl_anno_reg_ordine, long fl_num_reg_ordine, long fl_prog_riga_ord_ven, boolean fb_flag_elimina_commesse_righe_riferite, boolean fb_flag_elimina_dati_produzione, ref string fs_errore);/*
QUESTA FUNZIONE ELIMINA LA COMMESSA PARTENDO DALLA RIGA ORDINE.
PARAMETRI IN INGRESSO		fl_anno_reg_ordine
										fl_num_reg_ordine
										fl_prog_riga_ord_ven
										fb_flag_elimina_commesse_righe_riferite			stabilisce se cancellare anche le commesse delle righe riferite; valore S/N
										fb_flag_elimina_dati_produzione						stabilisce se cancellare contestualmente anche tutti i dati della produzione
										
PARAMETRI USCITA				fs_errore														eventuale messaggio di errore

RETURN								0=OK		-1=ERRORE

*/

string ls_messaggio,ls_flag_tipo_avanzamento, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_cod_prodotto, &
		ls_cod_prodotto_raggruppato, ls_mess_comm_eliminate, ls_sql, ls_errore
		
long 	ll_anno_commessa, ll_num_commessa, ll_anno_ord, ll_num_ord, ll_righe, ll_i, &
	  	ll_num_riga_appartenenza, ll_prog_riga_ord_ven, ll_anno_comm_riferita, ll_num_comm_riferita, &
		ll_prog_riga_ord_rif

dec{4} ld_quan_ordine, ld_quan_raggruppo

uo_funzioni_1 luo_funzioni_commesse
datastore	lds_data


select anno_commessa,
		num_commessa
into	:ll_anno_commessa,
		:ll_num_commessa
from	det_ord_ven
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :fl_anno_reg_ordine and
			num_registrazione = :fl_num_reg_ordine and
			prog_riga_ord_ven = :fl_prog_riga_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_errore = g_str.format("Errore SQL in ricerca anno e numer ocommessa della riga indicata. $1 ",sqlca.sqlerrtext)
	return -1
end if

if isnull(ll_anno_commessa) then
	fs_errore = g_str.format("Alla riga ordine $1-$2-$3 non è associata alcuna commessa.", fl_anno_reg_ordine, fl_num_reg_ordine, fl_prog_riga_ord_ven)
	return -1
end if

// 8/2/2007 aggiunto su richiesta di Beatrice: impossibile eliminare una riga se commessa chiusa.
if not isnull(ll_anno_commessa) then
	select flag_tipo_avanzamento
	into   :ls_flag_tipo_avanzamento
	from   anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_commessa = :ll_anno_commessa and
			 num_commessa = :ll_num_commessa;
	if sqlca.sqlcode <> 0 then
		fs_errore ="Errore in cancellazione commessa " + string(ll_anno_commessa) + "/" + string(ll_num_commessa) + "~r~n"+sqlca.sqlerrtext
		return -1
	end if
	
	if ls_flag_tipo_avanzamento = "7" then
		fs_errore = g_str.format("Commessa $1-$2 già chiusa: impossibile eliminare.",ll_anno_commessa, ll_num_commessa)
		return -1
	end if
		
end if

luo_funzioni_commesse = CREATE uo_funzioni_1

//log in log_sistema della richiesta di avanzamento ############################################################################
f_log_sistema("***Richiesta CANC.com: uof_avanza_commessa("+string(ll_anno_commessa) +"/"+string(ll_num_commessa)+"'NULL')","AVANZCOMdet_ord")
//############################################################################################################

if luo_funzioni_commesse.uof_cancella_commessa(ll_anno_commessa,ll_num_commessa, ls_messaggio) = -1 then
	fs_errore = ls_messaggio
	destroy luo_funzioni_commesse
	return -1
end if

delete impegno_mat_prime_commessa
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_commessa = :ll_anno_commessa and
		num_commessa = :ll_num_commessa;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore SQL durante la cancellazione dati impegno materie prime (impegno_mat_prime_commessa). " + sqlca.sqlerrtext
	destroy luo_funzioni_commesse
	return -1
end if

delete anag_commesse
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_commessa = :ll_anno_commessa and
		num_commessa = :ll_num_commessa;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore SQL durante la cancellazione commessa (anag_commesse). " + sqlca.sqlerrtext
	destroy luo_funzioni_commesse
	return -1
end if

// CANCELLAZIONE ANCHE DELLE COMMESSE DELLE RIGHE RIFERITE
//	Richiesto da Beatrice 16/06/2008

if fb_flag_elimina_commesse_righe_riferite then
	
	ls_sql = g_str.format("select prog_riga_ord_ven, anno_commessa, num_commessa from det_ord_ven where cod_azienda='$1' and anno_registrazione = $2 and num_registrazione =$3 and num_riga_appartenenza = $4 and num_riga_appartenenza is not null order by 1", s_cs_xx.cod_azienda, fl_anno_reg_ordine, fl_num_reg_ordine, fl_prog_riga_ord_ven)
	ll_righe = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_errore)
	if ll_righe < 0 then
		fs_errore = "Errore in ricerca righe riferite alla riga principale. " + ls_errore
		return -1
	end if

	//ll_righe = dw_det_ord_ven_lista.rowcount()
	
	for ll_i = 1 to ll_righe
		
//		ll_num_riga_appartenenza = dw_det_ord_ven_lista.getitemnumber(ll_i, "num_riga_appartenenza")
		
//		if (ll_num_riga_appartenenza = ll_prog_riga_ord_ven and not isnull(ll_num_riga_appartenenza) and ll_num_riga_appartenenza <> 0) or ll_i = ll_prog_riga_ord_ven then
		
			// cancello la commessa delle righe riferite  [EnMe 22/4/2003]
//			ll_anno_comm_riferita = dw_det_ord_ven_lista.getitemnumber(ll_i, "anno_commessa")
	//		ll_num_comm_riferita = dw_det_ord_ven_lista.getitemnumber(ll_i, "num_commessa")
		//	ll_prog_riga_ord_rif = dw_det_ord_ven_lista.getitemnumber(ll_i, "prog_riga_ord_ven")
			
			ll_anno_comm_riferita = lds_data.getitemnumber(ll_i, 2)
			ll_num_comm_riferita = lds_data.getitemnumber(ll_i, 3)
			ll_prog_riga_ord_rif = lds_data.getitemnumber(ll_i, 1)
			
			if not isnull(ll_anno_comm_riferita) then
				
				if luo_funzioni_commesse.uof_cancella_commessa(ll_anno_comm_riferita,ll_num_comm_riferita, ls_messaggio) = -1 then
					fs_errore = g_str.format("Errore in cancellazione commessa $1-$2 riga riferita $3 . ls_messaggio", ll_anno_comm_riferita, ll_num_comm_riferita,ll_prog_riga_ord_rif)
					destroy luo_funzioni_commesse
					return -1
				end if
				
				delete from anag_commesse
				where  cod_azienda   = :s_cs_xx.cod_azienda and
						 anno_commessa = :ll_anno_comm_riferita and
						 num_commessa  = :ll_num_comm_riferita;
				if sqlca.sqlcode <> 0 then
					fs_errore = g_str.format("Errore SQL durante la cancellazione della commessa $1-$2. ",ll_anno_comm_riferita,ll_num_comm_riferita, sqlca.sqlerrtext)
					return -1
				else
					ls_mess_comm_eliminate += "~r~nCOMMESSA (di riga riferita) N° " + string(ll_anno_comm_riferita) + "/" + string(ll_num_comm_riferita)
				end if
				
				if fb_flag_elimina_dati_produzione then
					
					if uof_elimina_prod_det(ll_anno_ord,ll_num_ord,ll_prog_riga_ord_rif,ls_messaggio) <> 0 then
						fs_errore = g_str.format("Errore durante cancellazione dati produzione riga ordine $1-$2-$3. $4", ll_anno_ord, ll_num_ord, ll_prog_riga_ord_rif, ls_messaggio)
						return -1
					end if
				end if
				
			end if
//		end if
	next
end if

// --------------------------------------------------------

if fb_flag_elimina_dati_produzione then

	if uof_elimina_prod_det(ll_anno_ord,ll_num_ord,fl_prog_riga_ord_ven,ls_messaggio) <> 0 then
		fs_errore = g_str.format("Errore durante cancellazione dati produzione riga ordine $1-$2-$3. $4", ll_anno_ord,ll_num_ord,fl_prog_riga_ord_ven,ls_messaggio)
		return -1
	end if
	
end if

destroy luo_funzioni_commesse

fs_errore = ls_mess_comm_eliminate

return 0
end function

on uo_produzione.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_produzione.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

