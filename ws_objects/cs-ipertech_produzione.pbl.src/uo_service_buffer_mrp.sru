﻿$PBExportHeader$uo_service_buffer_mrp.sru
$PBExportComments$Motore di produzione di Ptenda
forward
global type uo_service_buffer_mrp from uo_service_base
end type
end forward

global type uo_service_buffer_mrp from uo_service_base
end type
global uo_service_buffer_mrp uo_service_buffer_mrp

type variables
string			is_qualsiasi_reparto = "NOREP"
end variables

forward prototypes
public function integer dispatch (uo_commandparm_service auo_params)
end prototypes

public function integer dispatch (uo_commandparm_service auo_params);date									ldt_today
time									ltm_now
string								ls_path_log, ls_sql, ls_errore, ls_cod_reparto, ls_testo, ls_tipo_doc_origine, ls_flag_elaborato, ls_inventario
long									ll_index, ll_tot, ll_ok, ll_skipped, ll_num_doc_origine, ll_prog_riga_doc_origine
datastore							lds_data
integer								li_ret, li_anno_doc_origine


uo_service_mov_mrp			luo_mrp


//variabili per il datastore
long				ll_id_buffer_ds, ll_progressivo_ds
string			ls_cod_prodotto_ds, ls_cod_reparto_ds, ls_valore_file_ds, ls_flag_letto_ds, ls_cod_tipo_mov_mrp_ds
dec{4}			ld_quantita_ds, ld_valore_unitario

integer			li_anno_mov_mrp
long				ll_num_mov_mrp
datetime		ldt_data_rif_mrp


/*
ERROR_LEVEL = 1
WARN_LEVEL = 2
LOG_LEVEL = 3
INFO_LEVEL = 4
DEBUG_LEVEL = 5
*/

//##########################################################################################
//nota
//il processo cs_team.exe va lanciato così				cs_team.exe S=buffer_mrp,A01,REP,INVS,n
//																			dove REP rappresenta il reparto (passare NOREP per ignorare, ma il reparto deve trovarsi nella tabella buffer)
//																					INVS oppure INVN se vuoi elaborare oppure no gli inventari
//																					n è un numero che indica la verbosità del log (min 1, max 5, default 2)
//##########################################################################################

//NOTA: passando il reparto nella commandparm, questo, e solo questo, sarà considerato come oggetto del movimento !!!!
//			se invece non passo nulla, la procedura cercherà di leggere il reparto scritto nella riga della tabella buffer ...
//				se non lo trova, la riga verrà saltata e scritta nel log

//-----------------------------------------------------------------------------------------------------------------------------------------------------
//imposto l'azienda (se non passo niente metto A01) ed il livello di logging (1..5 se non passo niente metto 2)
uof_set_s_cs_xx(auo_params.uof_get_string(1,"A01"))

//reparto
ls_cod_reparto = auo_params.uof_get_string(2, is_qualsiasi_reparto)

//reparto
ls_inventario = auo_params.uof_get_string(3, ls_inventario)
if upper(ls_inventario) = "INVS" then
	ls_inventario = "S"
else
	ls_inventario = "N"
end if

//verbosity
iuo_log.set_log_level(auo_params.uof_get_int(4, 2))

//-----------------------------------------------------------------------------------------------------------------------------------------------------
//assegno il nome al file di log    bufferMRP_YYYYMMDD.log
ldt_today = today()

ldt_data_rif_mrp = datetime(ldt_today, 00:00:00)

ltm_now = now()
ls_path_log = s_cs_xx.volume + s_cs_xx.risorse + "logs\bufferMRP_" + &
								string(year(ldt_today)) + "-" + right("00" + string(month(ldt_today)), 2) + "-" + right("00" + string(day(ldt_today)), 2) + ".log"

//fai l'append nel file del giorno
iuo_log.set_file( ls_path_log, false)
iuo_log.warn("### INIZIO ELABORAZIONE BUFFER MRP #######################")

if isnull(ls_cod_reparto) or ls_cod_reparto = is_qualsiasi_reparto then
	ls_cod_reparto = ""
	iuo_log.warn("Tra i parametri non è stato passato alcun reparto, quindi solo le righe del buffer con reparto saranno movimentate, le altre saranno saltate!")
	
else
	//controlla che il reparto esista
	select count(*)
	into :ll_tot
	from anag_reparti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_reparto=:ls_cod_reparto;
				
	if sqlca.sqlcode<0 then
		iuo_log.error("Errore in controllo parametro reparto: "+sqlca.sqlerrtext)
		return -1
	end if
	
	if sqlca.sqlcode=0 and ll_tot=1 then
		iuo_log.warn("Valore reparto passato come parametro '"+ls_cod_reparto+"' VALIDO: !")
	else
		iuo_log.warn("Valore reparto passato come parametro '"+ls_cod_reparto+"' NON risulta valido, pertanto non verrà considerato nell'elaborazione e quindi solo le righe del buffer con reparto saranno movimentate, le altre saranno saltate!")
		ls_cod_reparto = ""
	end if
	
end if


ls_sql = "select id_buffer,"+&
					"progressivo,"+&
					"cod_prodotto,"+&
					"quantita,"+&
					"valore_file,"+&
					"cod_tipo_mov_mrp,"+&
					"cod_reparto,"+&
					"flag_letto "+&
			"from buffer_prodotti_qta "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"flag_letto<>'S' "+&			
			"order by id_buffer asc, progressivo asc"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_tot<0 then
	iuo_log.error(ls_errore)
	return -1
end if

iuo_log.warn("Tot. righe da elaborare: " + string(ll_tot))

ll_ok = 0
ll_skipped = 0
 
setnull(li_anno_doc_origine)
setnull(ll_num_doc_origine)
setnull(ll_prog_riga_doc_origine)

//-----------------------------------------------------------------------------------------------------------------------------------------------------
//se arrivi fin qui vuol dire che ci sono righe da elaborare
luo_mrp = create uo_service_mov_mrp

for ll_index=1 to ll_tot
	//lettura dati dal datastore ----------------------------------------------------
	ll_id_buffer_ds 					= lds_data.getitemnumber(ll_index, "id_buffer")
	ll_progressivo_ds 				= lds_data.getitemnumber(ll_index, "progressivo")
	ls_cod_prodotto_ds			= lds_data.getitemstring(ll_index, "cod_prodotto")
	ld_quantita_ds					= lds_data.getitemnumber(ll_index, "quantita")
	ls_valore_file_ds					= lds_data.getitemstring(ll_index, "valore_file")
	ls_cod_tipo_mov_mrp_ds		= lds_data.getitemstring(ll_index, "cod_tipo_mov_mrp")
	ls_cod_reparto_ds				= lds_data.getitemstring(ll_index, "cod_reparto")
	ls_flag_letto_ds					= lds_data.getitemstring(ll_index, "flag_letto")
	
	
	ld_valore_unitario = 0
	
	ls_testo = "(ID:"+string(ll_id_buffer_ds)+" Prog:"+string(ll_progressivo_ds)+") Prodotto: "+ls_cod_prodotto_ds+&
						" - Q.tà: "+string(ld_quantita_ds, "###,###,##0.00")+" - Mov: "+ls_cod_tipo_mov_mrp_ds+" - Rep: "
	
	
	//verifica sul codice reparto -------------------------------------------------
	if ls_cod_reparto="" then
		//parametro reparto non impostato, posso procedere solo se nella riga del buffer c'è il reparto
		if ls_cod_reparto_ds="" or isnull(ls_cod_reparto_ds) then
			ls_testo += " <null>"
			ll_skipped += 1
			
			iuo_log.warn("Reparto non impostato nella riga buffer e neanche nel parametro processo~r~n"+ls_testo+" -->riga saltata!~r~n~r~n")
			continue
		else
			ls_testo += ls_cod_reparto_ds
		end if
	else
		//parametro reparto impostato, vado a sovrascrivere, e lo segnalo nel log
		if ls_cod_reparto_ds="" or isnull(ls_cod_reparto_ds) then
			ls_testo += ls_cod_reparto + "(valore originario nel buffer <null>)"
		else
			ls_testo += ls_cod_reparto + "(valore originario nel buffer "+ls_cod_reparto_ds+")"
		end if
		
		//sovrascrivi il reparto
		ls_cod_reparto_ds = ls_cod_reparto
	end if
	
	
	//salto gli inventari, se richiesto ---------------------------------------------------------
	if upper(ls_cod_tipo_mov_mrp_ds)="INV" and ls_inventario="N" then
		ll_skipped += 1
		iuo_log.warn("Movimento id_buffer/progressivo "+string(ll_id_buffer_ds)+"/"+string(ll_progressivo_ds)+" di tipo INVENTARIO: verrà saltato dall'elaborazione -->riga saltata!~r~n~r~n")
		continue
	end if
	
	
	//elaborazione movimento ------------------------------------------------
	iuo_log.log("Elaborazione ... "+ls_testo)
	
	ls_tipo_doc_origine = ""
	ls_flag_elaborato = "N"
	
	luo_mrp.uof_insert_data( 	ldt_data_rif_mrp, &
										ls_cod_tipo_mov_mrp_ds, &
										ls_cod_prodotto_ds, &
										ld_quantita_ds, &
										ld_quantita_ds, &
										ld_valore_unitario, &
										ls_tipo_doc_origine, &
										li_anno_doc_origine, &
										ll_num_doc_origine, &
										ll_prog_riga_doc_origine, &
										ls_cod_reparto_ds, &
										ls_flag_elaborato, &
										li_anno_mov_mrp, &
										ll_num_mov_mrp, &
										ls_errore)
	
	if li_ret<0 then
		//si è verificato un errore
		uo_log.error("Elaborazione terminata con Errori: "+ ls_errore)
		rollback;
		destroy luo_mrp
		return -1
	end if


	//aggiorno il flag letto nel buffer ------------------------------------------
	update buffer_prodotti_qta
	set flag_letto='S'
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				id_buffer=:ll_id_buffer_ds and
				progressivo=:ll_progressivo_ds;
				
	if sqlca.sqlcode<0 then
		//si è verificato un errore in update
		uo_log.error("Errore in update flag_letto: "+ sqlca.sqlerrtext)
		rollback;
		destroy luo_mrp
		return -1
	end if
	
	//se arrivi fin qui committa il singolo movimento
	commit;
	iuo_log.log("Elaborazione id_buffer/progressivo "+string(ll_id_buffer_ds)+"/"+string(ll_progressivo_ds)+" terminata con successo: creato movimento "+string(li_anno_mov_mrp)+"/"+string(ll_num_mov_mrp))
	ll_ok += 1
	
next

destroy luo_mrp

iuo_log.warn("Elaborazione terminata, Tot.Righe Elaborate: " + string(ll_tot) + "  -  Con Successo: " + string(ll_ok) + "  -  Saltate: " + string(ll_skipped))
iuo_log.warn("### FINE ELABORAZIONE BUFFER MR ########################")

return 0

end function

on uo_service_buffer_mrp.create
call super::create
end on

on uo_service_buffer_mrp.destroy
call super::destroy
end on

