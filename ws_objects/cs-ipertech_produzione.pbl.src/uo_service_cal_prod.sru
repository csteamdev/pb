﻿$PBExportHeader$uo_service_cal_prod.sru
$PBExportComments$Motore di produzione di Ptenda
forward
global type uo_service_cal_prod from uo_service_base
end type
end forward

global type uo_service_cal_prod from uo_service_base
end type
global uo_service_cal_prod uo_service_cal_prod

forward prototypes
public function integer dispatch (uo_commandparm_service auo_params)
end prototypes

public function integer dispatch (uo_commandparm_service auo_params);

//##########################################################################################
//nota
//il processo cs_team.exe va lanciato così				cs_team.exe S=cal_prod,A01
//														
//##########################################################################################

uo_calendario_prod_new				luo_cal_prod

//il log è integrato nell'oggetto uo_calendario_prod_new, di cui tale servizio fa uso ...
//quindi nessuna chiamata all'oggetto uo_log è permessa in questo contesto
destroy iuo_log


//imposto l'azienda (se non passo niente metto A01) ed il livello di logging (1..5 se non passo niente metto 2)
uof_set_s_cs_xx(auo_params.uof_get_string(1,"A01"))

luo_cal_prod = create uo_calendario_prod_new
luo_cal_prod.ib_log = true

luo_cal_prod.uof_inizio()

destroy luo_cal_prod

return 0
end function

on uo_service_cal_prod.create
call super::create
end on

on uo_service_cal_prod.destroy
call super::destroy
end on

