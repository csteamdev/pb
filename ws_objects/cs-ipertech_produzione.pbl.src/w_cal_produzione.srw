﻿$PBExportHeader$w_cal_produzione.srw
forward
global type w_cal_produzione from w_cs_xx_principale
end type
type cb_reparti from commandbutton within w_cal_produzione
end type
type em_timer from editmask within w_cal_produzione
end type
type cbx_timer from checkbox within w_cal_produzione
end type
type mle_log from multilineedit within w_cal_produzione
end type
type cbx_domenica from checkbox within w_cal_produzione
end type
type pb_colore_4 from picturebutton within w_cal_produzione
end type
type pb_colore_3 from picturebutton within w_cal_produzione
end type
type pb_colore_2 from picturebutton within w_cal_produzione
end type
type pb_colore_1 from picturebutton within w_cal_produzione
end type
type st_zoom from statictext within w_cal_produzione
end type
type st_log from statictext within w_cal_produzione
end type
type ddlb_zoom from dropdownlistbox within w_cal_produzione
end type
type cbx_log from checkbox within w_cal_produzione
end type
type cb_print from commandbutton within w_cal_produzione
end type
type cb_plan from commandbutton within w_cal_produzione
end type
type cb_6 from commandbutton within w_cal_produzione
end type
type cb_view_plan from commandbutton within w_cal_produzione
end type
type em_fine from editmask within w_cal_produzione
end type
type em_inizio from editmask within w_cal_produzione
end type
type mle_1 from multilineedit within w_cal_produzione
end type
type dw_1 from uo_std_dw within w_cal_produzione
end type
end forward

global type w_cal_produzione from w_cs_xx_principale
integer width = 3712
integer height = 2552
string title = "Pianificazione Produzione Reparti"
event ue_posiziona_window ( )
event ue_ridimensiona ( )
cb_reparti cb_reparti
em_timer em_timer
cbx_timer cbx_timer
mle_log mle_log
cbx_domenica cbx_domenica
pb_colore_4 pb_colore_4
pb_colore_3 pb_colore_3
pb_colore_2 pb_colore_2
pb_colore_1 pb_colore_1
st_zoom st_zoom
st_log st_log
ddlb_zoom ddlb_zoom
cbx_log cbx_log
cb_print cb_print
cb_plan cb_plan
cb_6 cb_6
cb_view_plan cb_view_plan
em_fine em_fine
em_inizio em_inizio
mle_1 mle_1
dw_1 dw_1
end type
global w_cal_produzione w_cal_produzione

type variables
string is_header, is_column, is_objects, is_footer

string is_colonne_dinamiche, is_oggetti_dinamici

// array che memorizza la data relativa ad una certa colonna
date idd_date_colonne[], idd_date_null[]

// larghezza della colonna
long il_larghezza_colonna=182

// numero del file di log
long il_file

string is_reparti_calendario[]

//31/01/2011 Daniele ha richiesto di visualizzare i dati dei due reparti (REPTS e REPTS3)
//in un unico reparto REPTS, sommando i valori
string		is_repts1 = "REPTS"
string		is_repts2 = "REPTS3"
//-------------------------------------------------------------------------------------------

//parametro che separa le funzioni amministrative da quelle utente
//contiene i codici utente amministratori
//se questo parametro non esiste oppure è vuoto 
//allora tutte le funzionalità saranno disponibili per qualsiasi utente
string is_ACP
boolean ib_amministra = true


end variables

forward prototypes
public subroutine wf_imposta_struttura ()
public function integer wf_crea_colonna (long fl_numero_colonna, string fs_testo, ref string fs_columns, ref string fs_objects)
public function string wf_nome_giorno (long fl_daynumber)
public subroutine wf_crea_calendario (datetime fdt_data_inizio, datetime fdt_data_fine)
public function long wf_calcola_ore_produzione ()
public function integer wf_trova_quan_variante (string fs_cod_prodotto_finito, string fs_cod_versione, string fs_cod_gruppo_variante, long fl_anno_ordine, long fl_num_ordine, long fl_prog_riga_ord_ven, ref string fs_cod_prodotto_variante, ref decimal fd_quan_prodotto_variante, ref string fs_errore)
public function string wf_add_log (string fs_log, string fs_string)
public function string wf_add_log (string fs_log, datetime fs_datetime)
public function string wf_add_log (string fs_log, decimal fs_decimal)
public function string wf_add_log (string fs_log, long fs_long)
public function boolean wf_check_reparto (string fs_cod_reparto)
public subroutine wf_leggi_filtro_reparti ()
public subroutine wf_parse_reparti (string fs_stringa, string fs_parser)
public function integer wf_amministrazione ()
end prototypes

event ue_posiziona_window();move(1,1)

this.width = w_cs_xx_mdi.mdi_1.width - 60
this.height = w_cs_xx_mdi.mdi_1.height - 40


end event

event ue_ridimensiona();// Sposta gli oggetti all'interno della finestra in modo relativo alla sua dimensione

long ll_relative=470


dw_1.move(1,1)
dw_1.height = height - 170
dw_1.width 	= width - 60 - ll_relative

em_inizio.x = width - ll_relative
//em_inizio.y = 20

em_fine.x = width - ll_relative
//em_fine.y = 120

cb_view_plan.x = width - ll_relative
//cb_view_plan.y = 220

//cb_generate.x = width - ll_relative
//cb_generate.y = 389

cb_plan.x = width - ll_relative
//cb_plan.y = 1052

cb_print.x = width - ll_relative
//cb_print.y = 1152

cbx_log.x = width - ll_relative
//cbx_log.y = 472

cbx_domenica.x = width - ll_relative
//cbx_domenica.y = 552

st_zoom.x = width - ll_relative
//st_zoom.y = 684

ddlb_zoom.x = width - ll_relative
//ddlb_zoom.y = 760

st_log.x = width - ll_relative
//st_log.y = 864

pb_colore_1.x = width - ll_relative
//pb_colore_1.y = 1312

pb_colore_2.x = width - ll_relative
//pb_colore_2.y = 1464

pb_colore_3.x = width - ll_relative
//pb_colore_3.y = 1616

pb_colore_4.x = width - ll_relative
//pb_colore_4.y = 1768

cb_reparti.x=width - ll_relative

em_timer.x =  width - ll_relative
cbx_timer.x =  width - ll_relative

end event

public subroutine wf_imposta_struttura ();
f_imposta_struttura_cal_prod(is_header, is_column, is_objects, is_footer, pb_colore_4.backcolor)

//is_header = "release 10.5;" + "~n" + &
//				"datawindow(units=0 timer_interval=0 color=16777215 processing=1 HTMLDW=no print.printername=~"~" print.documentname=~"~" print.orientation = 0 print.margin.left = 110 print.margin.right = 110 print.margin.top = 96 print.margin.bottom = 96 print.paper.source = 0 print.paper.size = 0 print.canusedefaultprinter=yes print.prompt=no print.buttons=no print.preview.buttons=no print.cliptext=no print.overrideprintjob=no print.collate=yes print.preview.outline=yes hidegrayline=no grid.lines=0 )" + "~n" + &
//				"header(height=202 color=~""+ string( pb_colore_4.backcolor ) +"~" )" + "~n" + &
//				"summary(height=0 color=~""+ string( pb_colore_4.backcolor ) +"~" )" + "~n" + &
//				"footer(height=0 color=~""+ string( pb_colore_4.backcolor ) +"~" )" + "~n" + &
//				"detail(height=96 color=~""+ string( pb_colore_4.backcolor ) +"~" )"
//
//is_column = "table(column=(type=char(6) updatewhereclause=no name=cod_reparto dbname=~"cod_reparto~" )" + "~n" + &
//				" column=(type=char(40) updatewhereclause=no name=des_reparto dbname=~"des_reparto~" )" + "~n" + &
//				" column=(type=number updatewhereclause=no name=soglia_carico dbname=~"soglia_carico~" )" + "~n" + &
//				" column=(type=char(1) updatewhereclause=no name=flag_tipo_ore dbname=~"flag_tipo_ore~" )"
//
//is_objects = "text(band=header alignment=~"2~" text=~"Reparto~" border=~"2~" color=~"33554432~" x=~"5~" y=~"4~" height=~"300~" width=~"1200~" html.valueishtml=~"0~"  name=t_reparto visible=~"1~"  font.face=~"Tahoma~" font.height=~"-8~" font.weight=~"400~"  font.family=~"2~" font.pitch=~"2~" font.charset=~"0~" background.mode=~"1~" background.color=~"553648127~" )" + "~n" + &
//				"compute(band=detail alignment=~"0~" expression=~" cod_reparto + ' ' +  des_reparto ~"border=~"2~" color=~"0~" x=~"5~" y=~"4~" height=~"80~" width=~"1200~" format=~"[GENERAL]~" html.valueishtml=~"0~"  name=cf_des_reparto visible=~"1~"  font.face=~"Tahoma~" font.height=~"-8~" font.weight=~"400~"  font.family=~"2~" font.pitch=~"2~" font.charset=~"0~" background.mode=~"1~" background.color=~""+ string( pb_colore_4.backcolor ) +"~" )"
////				"text(band=header alignment=~"2~" text=~"text~" border=~"2~" color=~"33554432~" x=~"718~" y=~"4~" height=~"300~" width=~"174~" html.valueishtml=~"0~"  name=t_2 visible=~"1~"  font.face=~"Tahoma~" font.height=~"-8~" font.weight=~"400~"  font.family=~"2~" font.pitch=~"2~" font.charset=~"0~" background.mode=~"1~" background.color=~"553648127~" )" + "~n" + &
//
//is_footer = "htmltable(border=~"1~" )" + "~n" + &
//				"htmlgen(clientevents=~"1~" clientvalidation=~"1~" clientcomputedfields=~"1~" clientformatting=~"0~" clientscriptable=~"0~" generatejavascript=~"1~" encodeselflinkargs=~"1~" netscapelayers=~"0~" pagingmethod=0 generatedddwframes=~"1~" ) " + "~n" + &
//				"xhtmlgen() cssgen(sessionspecific=~"0~" ) " + "~n" + &
//				"xmlgen(inline=~"0~" ) " + "~n" + &
//				"xsltgen() " + "~n" + &
//				"jsgen() " + "~n" + &
//				"export.xml(headgroups=~"1~" includewhitespace=~"0~" metadatatype=0 savemetadata=0 ) " + "~n" + &
//				"import.xml() " + "~n" + &
//				"export.pdf(method=0 distill.custompostscript=~"0~" xslfop.print=~"0~" ) " + "~n" + &
//				"export.xhtml()"
//
end subroutine

public function integer wf_crea_colonna (long fl_numero_colonna, string fs_testo, ref string fs_columns, ref string fs_objects);string ls_colonne, ls_oggetti, ls_testo_aggiuntivo
long   ll_posizione_colonna

//imposta il grassetto nella colonna relativa alla data consegna dell'ordine corrente
//serve solo quando si usa tale funzione per generare l'impegno reparti di un ordine
//(doppio clic sul semaforo) e non in generazione calendario generale, quindi qui passiamo false
ls_testo_aggiuntivo = ""

f_crea_colonna_cal_prod(fl_numero_colonna, fs_testo, fs_columns, fs_objects, il_larghezza_colonna, &
								pb_colore_3.backcolor, pb_colore_2.backcolor, pb_colore_1.backcolor, ls_testo_aggiuntivo)
return 0

//ll_posizione_colonna = 723 + (il_larghezza_colonna * (fl_numero_colonna - 1) ) + (14 * (fl_numero_colonna - 1) )
//
//ls_colonne = " column=(type=char(10) updatewhereclause=yes name=giorno_"+string(fl_numero_colonna)+" dbname=~"giorno_"+string(fl_numero_colonna)+"~" ) " + "~n" + &
// 				 " column=(type=number updatewhereclause=yes name=soglia_"+string(fl_numero_colonna)+" dbname=~"soglia_"+string(fl_numero_colonna)+"~" ) " + "~n" + &
// 				 " column=(type=number updatewhereclause=yes name=repore_"+string(fl_numero_colonna)+" dbname=~"repore_"+string(fl_numero_colonna)+"~" ) " + "~n" + &
// 				 " column=(type=number updatewhereclause=yes name=repore_confronto_"+string(fl_numero_colonna)+" dbname=~"repore_confronto_"+string(fl_numero_colonna)+"~" ) " + "~n" + &
// 				 " column=(type=number updatewhereclause=yes name=ore_"+string(fl_numero_colonna)+" dbname=~"ore_"+string(fl_numero_colonna)+"~" )"
//				  
//
//
//ls_oggetti = "text(band=header alignment=~"0~" text=~"" + fs_testo + "~" border=~"2~" color=~"33554432~" x=~"" + string(ll_posizione_colonna) + "~" y=~"1~" height=~"200~" width=~"" + string(il_larghezza_colonna) + "~" html.valueishtml=~"0~"  name=t_" + string(fl_numero_colonna) + " visible=~"1~"  font.face=~"Tahoma~" font.height=~"-8~" font.weight=~"400~"  font.family=~"2~" font.pitch=~"2~" font.charset=~"0~"  background.mode=~"1~" background.color=~"553648127~" ) " + "~n" + &
//				 "compute(band=detail alignment=~"2~" expression=~" ore_" + string(fl_numero_colonna) + " + '/' + repore_" + string(fl_numero_colonna) + " ~"border=~"2~" color=~"0~" x=~"" + string(ll_posizione_colonna) + "~" y=~"4~" height=~"80~" width=~"" + string(il_larghezza_colonna) + "~" format=~"[GENERAL]~" html.valueishtml=~"0~"  name=compute_" + string(fl_numero_colonna) + " visible=~"1~"  font.face=~"Tahoma~" font.height=~"-8~" font.weight=~"400~"  font.family=~"2~" font.pitch=~"2~" font.charset=~"0~" background.mode=~"0~" background.color=~"16777215~~tif (repore_confronto_" + string(fl_numero_colonna) + " > (ore_" + string(fl_numero_colonna) + " + soglia_" + string(fl_numero_colonna) + ") , " + string(pb_colore_3.backcolor) + ",  if (repore_confronto_" + string(fl_numero_colonna) + " >= ore_" + string(fl_numero_colonna) + ", " + string(pb_colore_2.backcolor) + ", " + string(pb_colore_1.backcolor) + " ) )~" )"
//
////				 "compute(band=detail alignment=~"2~" expression=~" ore_" + string(fl_numero_colonna) + " + '/' + repore_" + string(fl_numero_colonna) + " ~"border=~"2~" color=~"0~" x=~"" + string(ll_posizione_colonna) + "~" y=~"4~" height=~"80~" width=~"" + string(il_larghezza_colonna) + "~" format=~"[GENERAL]~" html.valueishtml=~"0~"  name=compute_" + string(fl_numero_colonna) + " visible=~"1~"  font.face=~"Tahoma~" font.height=~"-8~" font.weight=~"400~"  font.family=~"2~" font.pitch=~"2~" font.charset=~"0~" background.mode=~"0~" background.color=~"16777215~~tif (ore_" + string(fl_numero_colonna) + " <  soglia_" + string(fl_numero_colonna) + " , 65280,  if (ore_" + string(fl_numero_colonna) + " < 20, 65535, 255  ) )~" )"
//
//
//fs_columns += ls_colonne + "~n"
//fs_objects += ls_oggetti + "~n"
//
//return 0
////~tif (ore_" + string(fl_numero_colonna) + " <  soglia_carico , rgb(0,255,0),  if (ore_" + string(fl_numero_colonna) + " < 100, rgb(255,255,0), rgb(255,0,0)  ) )
end function

public function string wf_nome_giorno (long fl_daynumber);//string ls_ret

return f_nome_giorno_cal_prod(fl_daynumber)

//choose case fl_daynumber
//	case 1
//		ls_ret =  "Dom"
//	case 2
//		ls_ret =  "Lun"
//	case 3
//		ls_ret =  "Mar"
//	case 4
//		ls_ret =  "Mer"
//	case 5
//		ls_ret =  "Gio"
//	case 6
//		ls_ret =  "Ven"
//	case 7
//		ls_ret =  "Sab"
//	case else
//		ls_ret =  "???"
//end choose
//
//return ls_ret
end function

public subroutine wf_crea_calendario (datetime fdt_data_inizio, datetime fdt_data_fine);string	ls_des_giorno, ls_columns, ls_objects,ls_dw_syntax, ls_error, ls_cod_reparto, ls_des_reparto, ls_path, ls_file

long 		ll_giorni, ll_i, ll_daynumber, ll_y, ll_row_1, ll_row_2, ll_carico, ll_ret

dec{4} 	ld_tot_minuti_reparto, ld_tot_minuti_effettivi, ld_tot_minuti_giro_rc, ld_soglia_carico, ld_soglia_effettiva

date 		ldd_inizio, ldd_fine

datetime	ldt_oggi

ldt_oggi = datetime(today(), 00:00:00)

declare 	cu_reparti cursor for
select 	cod_reparto, des_reparto, soglia_carico
from   	anag_reparti
where  	cod_azienda = :s_cs_xx.cod_azienda and
  			flag_tipo_reparto = 'I' and
			flag_blocco = 'N';


wf_imposta_struttura()

ldd_inizio = date(fdt_data_inizio)
ldd_fine   = date(fdt_data_fine)

ll_giorni = daysafter(ldd_inizio, ldd_fine) + 1

// Creo una prima colonna con il totale cumulato alla data precendente rispetto alla data inizio
idd_date_colonne[]  = idd_date_null[]
idd_date_colonne[1] = relativedate(ldd_inizio, - 1)

ls_des_giorno = "TOT.AL" +"~n" + string(idd_date_colonne[1],"dd/mm")

wf_crea_colonna(1, ls_des_giorno, ref ls_columns, ref ls_objects)


// Crea tante colonne quante sono le date richieste 

for ll_i = 1 to ll_giorni
	
	//ll_y = ll_i + 1
	if ll_i = 1 then
		ll_y += ll_i + 1
	else
		ll_y += 1
	end if
	
	idd_date_colonne[ll_y] = relativedate(ldd_inizio, ll_i - 1)

	ll_daynumber  = daynumber( idd_date_colonne[ll_y] )
	
	ls_des_giorno = wf_nome_giorno(ll_daynumber) + "~n" + string(idd_date_colonne[ll_y],"dd/mm")
	
	wf_crea_colonna(ll_y, ls_des_giorno, ref ls_columns, ref ls_objects)
	
	//poi crea un altra colonna sommatoria -----------------------------------------------------------------
	ll_y += 1
	
	//ls_des_giorno = "TOT.AL " + string(ll_y)
	ls_des_giorno = "TOT.AL" +"~n" + string(idd_date_colonne[ll_y - 1],"dd/mm")
	wf_crea_colonna(ll_y, ls_des_giorno, ref ls_columns, ref ls_objects)
	//----------------------------------------------------------------------------------------------------------------------------
	
next

// crea la DW
ls_dw_syntax = IS_HEADER + "~n" + &
					IS_COLUMN  + "~n" + &
					ls_columns + &
					" ) "  + "~n" + &
					IS_OBJECTS  + "~n" + &
					ls_objects + &
					IS_FOOTER
					
					
dw_1.create(ls_dw_syntax, ls_error)

mle_1.text = ls_dw_syntax

if len(ls_error) > 0 then
	g_mb.messagebox("ERRORE", ls_error)
end if

// Caricamento della DW con i dati effettivi del calendario

//// -----------------  caricamento dati da TAB_CAL_PRODUZIONE e visualizzazione ---------
/*	Per ogni reparto devono essere caricate 3 righe:
RIGA 1: le ore inserite nel calendario teorico di produzione
RIGA 2: le ore di produzione effettive escluse quelle provenienti da ordini con giro tipo Ritiro cliente
RIGA 3: le ore di produzione effettive SOLO provenienti da ordini con giro tipo Ritiro cliente
*/
	
open cu_reparti;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("SEP", "Errore in OPEN cursore CU_REPARTI~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

do while true
	// mi passo tutti i reparti dell'azienda e per ogni reparto carico i dati
	fetch cu_reparti into :ls_cod_reparto, :ls_des_reparto, :ld_soglia_carico;
	
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("SEP", "Errore in FETCH cursore CU_REPARTI~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	//salta il reparto se escluso dalla visualizzazione
	if not wf_check_reparto(ls_cod_reparto) then continue
	
	if ls_cod_reparto = is_repts2 then
		//salta perchè questi dati sono riportati nel reparto ls_repts1
		continue
	end if
	
	if isnull(ld_soglia_carico) then ld_soglia_carico = 0

	// per ogni reparto inserisco 2 righe;
	// ora inserisco la prima che è il totale delle ore potenziali in rapporto con le ore totalizzate dagli ordini
	ll_row_1 = dw_1.insertrow(0)
	dw_1.setitem(ll_row_1, "cod_reparto", ls_cod_reparto )
	dw_1.setitem(ll_row_1, "des_reparto", ls_des_reparto + "-GIRO")
	
	ll_row_2 = dw_1.insertrow(0)
	dw_1.setitem(ll_row_2, "cod_reparto", ls_cod_reparto)
	dw_1.setitem(ll_row_2, "des_reparto", ls_des_reparto + "-RC")
	
	//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	for ll_i = 1 to upperbound(idd_date_colonne)
		
		if year(idd_date_colonne[ll_i]) > 1950 then
		else
			continue
		end if
		
		//°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
		if ll_i = 1 then
			
			if ls_cod_reparto = is_repts1 then
				//includi anche REPTS3
				// somma del totale alle date precedenti
				
				//11/03/2011 Donato
				//su richiesta di Daniele Riello effettuiamo la somma a partire dalla data odierna di sistema
				
				//04/04/2011 Il potenziale Reparti deve essere calcolato a partire dalla data di sistema
				//					Invece l'impegno deve andare a guardare anche prima (eventuali ordini in ritardo)
				
//				select 	sum(isnull(minuti_reparto, 0)),
//							sum(isnull(minuti_effettivi, 0)), 
//							sum(isnull(minuti_giro_rc, 0))
//				into   		:ld_tot_minuti_reparto,	
//							:ld_tot_minuti_effettivi, 
//							:ld_tot_minuti_giro_rc
//				from 		tab_cal_produzione
//				where 	cod_azienda = :s_cs_xx.cod_azienda and  
//							(cod_reparto = :ls_cod_reparto or cod_reparto = :is_repts2) and
//							data_giorno <= :idd_date_colonne[ll_i] and
//							data_giorno >= :ldt_oggi;
				
				//potenziale reparto
				select 	sum(isnull(minuti_reparto, 0))
				into   		:ld_tot_minuti_reparto
				from 		tab_cal_produzione
				where 	cod_azienda = :s_cs_xx.cod_azienda and  
							(cod_reparto = :ls_cod_reparto or cod_reparto = :is_repts2) and
							data_giorno <= :idd_date_colonne[ll_i] and
							data_giorno >= :ldt_oggi;
				
				if isnull(ld_tot_minuti_reparto) then ld_tot_minuti_reparto = 0
				
				//impegno impegno
				select 	sum(isnull(minuti_effettivi, 0)), 
							sum(isnull(minuti_giro_rc, 0))
				into   		:ld_tot_minuti_effettivi, 
							:ld_tot_minuti_giro_rc
				from 		tab_cal_produzione
				where 	cod_azienda = :s_cs_xx.cod_azienda and  
							(cod_reparto = :ls_cod_reparto or cod_reparto = :is_repts2) and
							data_giorno <= :idd_date_colonne[ll_i];
							
							
			else
				
				//04/04/2011 Il potenziale Reparti deve essere calcolato a partire dalla data di sistema
				//					Invece l'impegno deve andare a guardare anche prima (eventuali ordini in ritardo)
				
				// somma del totale alle date precedenti
//				select 	sum(isnull(minuti_reparto, 0)),
//							sum(isnull(minuti_effettivi, 0)), 
//							sum(isnull(minuti_giro_rc, 0))
//				into   		:ld_tot_minuti_reparto,	
//							:ld_tot_minuti_effettivi, 
//							:ld_tot_minuti_giro_rc
//				from 		tab_cal_produzione
//				where 	cod_azienda = :s_cs_xx.cod_azienda and  
//							cod_reparto = :ls_cod_reparto and
//							data_giorno <= :idd_date_colonne[ll_i] and
//							data_giorno >= :ldt_oggi;		
				
				//potenziale reparto
				select 	sum(isnull(minuti_reparto, 0))
				into   		:ld_tot_minuti_reparto
				from 		tab_cal_produzione
				where 	cod_azienda = :s_cs_xx.cod_azienda and  
							cod_reparto = :ls_cod_reparto and
							data_giorno <= :idd_date_colonne[ll_i] and
							data_giorno >= :ldt_oggi;
				
				if isnull(ld_tot_minuti_reparto) then ld_tot_minuti_reparto = 0
				
				//impegno reparto
				select 	sum(isnull(minuti_effettivi, 0)), 
							sum(isnull(minuti_giro_rc, 0))
				into   		:ld_tot_minuti_effettivi, 
							:ld_tot_minuti_giro_rc
				from 		tab_cal_produzione
				where 	cod_azienda = :s_cs_xx.cod_azienda and  
							cod_reparto = :ls_cod_reparto and
							data_giorno <= :idd_date_colonne[ll_i];
				
			end if
			
			if isnull(ld_tot_minuti_effettivi) then ld_tot_minuti_effettivi = 0
			if isnull(ld_tot_minuti_giro_rc) then ld_tot_minuti_giro_rc = 0
			if isnull(ld_soglia_carico) then ld_soglia_carico = 0
			
			ld_soglia_effettiva = ld_tot_minuti_effettivi - (ld_tot_minuti_effettivi * ld_soglia_carico) / 100
			
			dw_1.setitem(ll_row_1, "giorno_" + string(ll_i), "PREC")
			dw_1.setitem(ll_row_1, "soglia_" + string(ll_i), 0)
			dw_1.setitem(ll_row_1, "ore_" + string(ll_i), round(ld_tot_minuti_reparto / 60, 0) )
			dw_1.setitem(ll_row_1, "repore_" + string(ll_i), round(ld_tot_minuti_effettivi / 60, 0) )
			dw_1.setitem(ll_row_1, "repore_confronto_" + string(ll_i), round(ld_tot_minuti_effettivi / 60, 0) )

			dw_1.setitem(ll_row_1, "giorno_" + string(ll_i), "PREC")
			dw_1.setitem(ll_row_2, "soglia_" + string(ll_i), 0)
			dw_1.setitem(ll_row_2, "ore_" + string(ll_i), round(ld_tot_minuti_reparto / 60, 0) )
			dw_1.setitem(ll_row_1, "repore_confronto_" + string(ll_i), round(ld_tot_minuti_effettivi / 60, 0) )
			
			//°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
		else
			//####################################################
			if ls_cod_reparto = is_repts1 then
				//includi anche REPTS3
				// situazione precisa alla data
				select 	sum(minuti_reparto), 
							sum(minuti_effettivi), 
							sum(minuti_giro_rc)
				into   		:ld_tot_minuti_reparto, 
							:ld_tot_minuti_effettivi, 
							:ld_tot_minuti_giro_rc
				from 		tab_cal_produzione
				where 	cod_azienda = :s_cs_xx.cod_azienda and  
							(cod_reparto = :ls_cod_reparto or cod_reparto = :is_repts2) and
							data_giorno = :idd_date_colonne[ll_i];
				
				if isnull(ld_tot_minuti_reparto) then ld_tot_minuti_reparto = 0
				
			else
				
				// situazione precisa alla data
				select 	sum(minuti_reparto), 
							sum(minuti_effettivi), 
							sum(minuti_giro_rc)
				into   		:ld_tot_minuti_reparto, 
							:ld_tot_minuti_effettivi, 
							:ld_tot_minuti_giro_rc
				from 		tab_cal_produzione
				where 	cod_azienda = :s_cs_xx.cod_azienda and  
							cod_reparto = :ls_cod_reparto and
							data_giorno = :idd_date_colonne[ll_i];
							
			end if
			
			if isnull(ld_tot_minuti_effettivi) then ld_tot_minuti_effettivi = 0
			if isnull(ld_tot_minuti_giro_rc) then ld_tot_minuti_giro_rc = 0
			if isnull(ld_soglia_carico) then ld_soglia_carico = 0
			
			ld_soglia_effettiva = (ld_tot_minuti_reparto * ld_soglia_carico) / 100

			dw_1.setitem(ll_row_1, "giorno_" + string(ll_i), string( idd_date_colonne[ll_i], "dd/mm/yyyy" ))
			dw_1.setitem(ll_row_1, "soglia_" + string(ll_i), round(ld_soglia_effettiva   / 60,0) )
			dw_1.setitem(ll_row_1, "ore_"    + string(ll_i), round(ld_tot_minuti_reparto / 60, 0) )
			dw_1.setitem(ll_row_1, "repore_" + string(ll_i), round(ld_tot_minuti_effettivi / 60, 0) )
			dw_1.setitem(ll_row_1, "repore_confronto_" + string(ll_i), round(ld_tot_minuti_effettivi / 60, 0) )

			dw_1.setitem(ll_row_2, "giorno_" + string(ll_i), string( idd_date_colonne[ll_i], "dd/mm/yyyy" ))
			dw_1.setitem(ll_row_2, "soglia_" + string(ll_i), round(ld_soglia_effettiva   / 60,0) )
			dw_1.setitem(ll_row_2, "ore_"    + string(ll_i), round(ld_tot_minuti_reparto / 60, 0) )
			dw_1.setitem(ll_row_2, "repore_" + string(ll_i), round(ld_tot_minuti_giro_rc / 60, 0) )
			dw_1.setitem(ll_row_2, "repore_confronto_" + string(ll_i), round(ld_tot_minuti_effettivi / 60, 0) )
			
			//####################################################
			
	
			
			//........................................................................................................................
			//inoltre scrivi nella colonna adiacente la sommatoria
			
			if ls_cod_reparto = is_repts1 then
				//includi anche REPTS3
				// somma del totale alle date precedenti
				
				//04/04/2011 Il potenziale Reparti deve essere calcolato a partire dalla data di sistema
				//					Invece l'impegno deve andare a guardare anche prima (eventuali ordini in ritardo)
				
//				select 	sum(isnull(minuti_reparto, 0)),
//							sum(isnull(minuti_effettivi, 0)), 
//							sum(isnull(minuti_giro_rc, 0))
//				into   		:ld_tot_minuti_reparto,	
//							:ld_tot_minuti_effettivi, 
//							:ld_tot_minuti_giro_rc
//				from 		tab_cal_produzione
//				where 	cod_azienda = :s_cs_xx.cod_azienda and  
//							(cod_reparto = :ls_cod_reparto or cod_reparto = :is_repts2) and
//							data_giorno <= :idd_date_colonne[ll_i] and
//							data_giorno >= :ldt_oggi;
				
				//potenziale reparto
				select 	sum(isnull(minuti_reparto, 0))
				into   		:ld_tot_minuti_reparto
				from 		tab_cal_produzione
				where 	cod_azienda = :s_cs_xx.cod_azienda and  
							(cod_reparto = :ls_cod_reparto or cod_reparto = :is_repts2) and
							data_giorno <= :idd_date_colonne[ll_i] and
							data_giorno >= :ldt_oggi;
				
				if isnull(ld_tot_minuti_reparto) then ld_tot_minuti_reparto = 0
				
				//impegno reparto
				select 	sum(isnull(minuti_effettivi, 0)), 
							sum(isnull(minuti_giro_rc, 0))
				into   		:ld_tot_minuti_effettivi, 
							:ld_tot_minuti_giro_rc
				from 		tab_cal_produzione
				where 	cod_azienda = :s_cs_xx.cod_azienda and  
							(cod_reparto = :ls_cod_reparto or cod_reparto = :is_repts2) and
							data_giorno <= :idd_date_colonne[ll_i] ;
							
			else
				
				//04/04/2011 Il potenziale Reparti deve essere calcolato a partire dalla data di sistema
				//					Invece l'impegno deve andare a guardare anche prima (eventuali ordini in ritardo)
				
				// somma del totale alle date precedenti
//				select 	sum(isnull(minuti_reparto, 0)),
//							sum(isnull(minuti_effettivi, 0)), 
//							sum(isnull(minuti_giro_rc, 0))
//				into   		:ld_tot_minuti_reparto,	
//							:ld_tot_minuti_effettivi, 
//							:ld_tot_minuti_giro_rc
//				from 		tab_cal_produzione
//				where 	cod_azienda = :s_cs_xx.cod_azienda and  
//							cod_reparto = :ls_cod_reparto and
//							data_giorno <= :idd_date_colonne[ll_i] and
//							data_giorno >= :ldt_oggi;

				//ptenziale reparto
				select 	sum(isnull(minuti_reparto, 0))
				into   		:ld_tot_minuti_reparto
				from 		tab_cal_produzione
				where 	cod_azienda = :s_cs_xx.cod_azienda and  
							cod_reparto = :ls_cod_reparto and
							data_giorno <= :idd_date_colonne[ll_i] and
							data_giorno >= :ldt_oggi;
				
				//impegno reparto
				select 	sum(isnull(minuti_effettivi, 0)), 
							sum(isnull(minuti_giro_rc, 0))
				into   		:ld_tot_minuti_effettivi, 
							:ld_tot_minuti_giro_rc
				from 		tab_cal_produzione
				where 	cod_azienda = :s_cs_xx.cod_azienda and  
							cod_reparto = :ls_cod_reparto and
							data_giorno <= :idd_date_colonne[ll_i];
				
			end if
			
			if isnull(ld_tot_minuti_effettivi) then ld_tot_minuti_effettivi = 0
			if isnull(ld_tot_minuti_giro_rc) then ld_tot_minuti_giro_rc = 0
			if isnull(ld_soglia_carico) then ld_soglia_carico = 0
			
			ld_soglia_effettiva = ld_tot_minuti_effettivi - (ld_tot_minuti_effettivi * ld_soglia_carico) / 100
			
			dw_1.setitem(ll_row_1, "giorno_" + string(ll_i + 1), "PREC")
			dw_1.setitem(ll_row_1, "soglia_" + string(ll_i + 1), 0)
			dw_1.setitem(ll_row_1, "ore_" + string(ll_i + 1), round(ld_tot_minuti_reparto / 60, 0) )
			dw_1.setitem(ll_row_1, "repore_" + string(ll_i + 1), round(ld_tot_minuti_effettivi / 60, 0) )
			dw_1.setitem(ll_row_1, "repore_confronto_" + string(ll_i + 1), round(ld_tot_minuti_effettivi / 60, 0) )

			dw_1.setitem(ll_row_1, "giorno_" + string(ll_i + 1), "PREC")
			dw_1.setitem(ll_row_2, "soglia_" + string(ll_i + 1), 0)
			dw_1.setitem(ll_row_2, "ore_" + string(ll_i + 1), round(ld_tot_minuti_reparto / 60, 0) )
			dw_1.setitem(ll_row_2, "repore_confronto_" + string(ll_i + 1), round(ld_tot_minuti_effettivi / 60, 0) )
			
			//........................................................................................................................
			
		end if
			
	next	

loop

close cu_reparti;



end subroutine

public function long wf_calcola_ore_produzione ();// funzione che legge gli ordini clienti, calcola le ore di lavoro ed aggiorna la tabella di appoggio tab_cal_produzione

string 	ls_tipi_ordini_sfuso, ls_cod_prodotto, ls_cod_tipo_det_ven, ls_cod_versione, ls_cod_reparto_tempi[], &
			ls_reparti_distinta[], ls_errore, ls_cod_gruppo_variante, ls_cod_gruppo_variante_tempi[], ls_cod_reparto, &
			ls_cod_prodotto_variante, ls_vuoto[], ls_cod_giro_consegna, ls_flag_rc, ls_log, ls_tipi_ordini_confezioni, &
			ls_flag_fine_fase, ls_flag_pronto_complessivo

long 		ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_cont, ll_ret, ll_i, ll_index, ll_numero

dec{4} 	ld_minuti_sfuso, ld_quan_ordine, ld_lead_time, ld_tempo_riga_ordine, ld_tempo_reparto[], ld_tempo_produzione, &
			ld_quan_prodotto_variante, ld_vuoto[], ld_tempo_reparto_rc[], ld_percentuale

date		ldd_data, ldd_data_produzione

datetime ldt_data_consegna, ldt_data_produzione, ldt_data_start, ldt_fine_preparazione
uo_funzioni_1 luo_funzioni


// Cursore per SFUSO
DECLARE 	cu_sfusi CURSOR FOR  
SELECT 	tes_ord_ven.anno_registrazione,   
			tes_ord_ven.num_registrazione,   
			det_ord_ven.prog_riga_ord_ven,   
			tes_ord_ven.data_consegna,
			tes_ord_ven.cod_giro_consegna,
			det_ord_ven.cod_prodotto,   
			det_ord_ven.cod_tipo_det_ven,   
			det_ord_ven.cod_versione,   
			det_ord_ven.quan_ordine  
 FROM 	det_ord_ven RIGHT OUTER JOIN tes_ord_ven ON det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda AND det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione AND det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione
WHERE  	tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda  AND  
		 	tes_ord_ven.flag_blocco = 'N'  AND  
		 	det_ord_ven.flag_blocco = 'N'  AND  
		 	tes_ord_ven.flag_evasione    IN ('A', 'P')  AND  
		 	det_ord_ven.flag_evasione    IN ('A', 'P')  AND
		 	tes_ord_ven.cod_tipo_ord_ven IN ( :ls_tipi_ordini_sfuso) and
			tes_ord_ven.data_registrazione >= :ldt_data_start 
ORDER BY tes_ord_ven.anno_registrazione,   
			tes_ord_ven.num_registrazione,   
			det_ord_ven.prog_riga_ord_ven
;

// Cursore per prodotti confezionati
DECLARE 	cu_det_ord_ven_prod CURSOR FOR  
SELECT 	tes_ord_ven.anno_registrazione,   
			tes_ord_ven.num_registrazione,   
			det_ord_ven.prog_riga_ord_ven,   
			tes_ord_ven.data_consegna,
			tes_ord_ven.cod_giro_consegna,
			det_ord_ven.cod_prodotto,   
			det_ord_ven.cod_tipo_det_ven,   
			det_ord_ven.cod_versione,   
			det_ord_ven.quan_ordine  
 FROM 	det_ord_ven RIGHT OUTER JOIN tes_ord_ven ON det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda AND det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione AND det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione  
WHERE  	tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda  AND  
		 	tes_ord_ven.flag_blocco = 'N'  AND  
		 	tes_ord_ven.flag_evasione in ('A', 'P')  AND  
		 	tes_ord_ven.cod_tipo_ord_ven in (:ls_tipi_ordini_confezioni) AND  
		 	det_ord_ven.flag_blocco = 'N'  AND  
		 	det_ord_ven.flag_evasione in ('A', 'P')  and 
			tes_ord_ven.data_registrazione >= :ldt_data_start  
ORDER BY tes_ord_ven.anno_registrazione,   
			tes_ord_ven.num_registrazione,   
			det_ord_ven.prog_riga_ord_ven
;

//-------------------------------------------------------------------------------------------------------------------

select numero
into   :ll_numero
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'GVP';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("SEP","Attenzione; è necessario create il parametro GVP indicante i giorni di retroattività visione produzione.~r~n")
	return -1
end if

ldd_data = today()
ll_numero = ll_numero * -1
ldd_data = relativedate(ldd_data, ll_numero)
ldt_data_start = datetime(ldd_data, 00:00:00)


select stringa
into   :ls_tipi_ordini_confezioni
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'COC';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("SEP","Attenzione; è necessario create il parametro COC con l'elenco separato da virgola degli ordini di sfuso.~r~n" + &
								 "Ad Esempio 'AAA','BBB'")
	return -1
end if

select stringa
into   :ls_tipi_ordini_sfuso
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'COS';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("SEP","Attenzione; è necessario create il parametro COS con l'elenco separato da virgola degli ordini di sfuso.~r~n" + &
								 "Ad Esempio 'AAA','BBB'")
	return -1
end if

select numero
into   :ld_minuti_sfuso
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'MOS';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("SEP","Attenzione; è necessario create il parametro MOS con il numero dei minuti necessari per eseguire 1 ordine di sfuso")
	return -1
end if

// azzero i valori dei tempi nella tabella calendario

update tab_cal_produzione
set    minuti_effettivi = 0, minuti_giro_rc = 0
where cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("SEP","Errore in OPEN cursore 'cu_det_ord_ven_prod'~r~n" + sqlca.sqlerrtext)
	rollback;
	return -1
end if

// procedo con esame di ogni ordine che sia di prodotti finiti

open cu_det_ord_ven_prod;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("SEP","Errore in OPEN cursore 'cu_det_ord_ven_prod'~r~n" + sqlca.sqlerrtext)
	rollback;
	return -1
end if

do while true
	fetch cu_det_ord_ven_prod into :ll_anno_registrazione, :ll_num_registrazione, :ll_prog_riga_ord_ven, :ldt_data_consegna, :ls_cod_giro_consegna,
											 :ls_cod_prodotto, 		 :ls_cod_tipo_det_ven,  :ls_cod_versione,      :ld_quan_ordine;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("SEP","Errore in FETCH cursore 'cu_det_ord_ven_prod'~r~n" + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	st_log.text = string(ll_anno_registrazione) + "-" + &
						string(ll_num_registrazione) + "-" + &
						string(ll_prog_riga_ord_ven)
	Yield()
	
	
	ls_log = ""
	
	ls_log = wf_add_log(ls_log, ll_anno_registrazione)
	ls_log = wf_add_log(ls_log, ll_num_registrazione)
	ls_log = wf_add_log(ls_log, ll_prog_riga_ord_ven)
	ls_log = wf_add_log(ls_log, ldt_data_consegna)
	ls_log = wf_add_log(ls_log, ls_cod_giro_consegna)
	ls_log = wf_add_log(ls_log, ls_cod_prodotto)
	ls_log = wf_add_log(ls_log, ls_cod_tipo_det_ven)
	ls_log = wf_add_log(ls_log, ls_cod_versione)
	ls_log = wf_add_log(ls_log, ld_quan_ordine)
	
		
	if isnull(ls_cod_prodotto) then continue
	
	// Calcolo la data di produzione, che deve essere 1 giorno prima della data consegna
	ldd_data = date(ldt_data_consegna)
	ldd_data = relativedate(ldd_data, -1)
	ldt_data_produzione = datetime(ldd_data, 00:00:00)
	
	ls_log = wf_add_log(ls_log, ldt_data_produzione)

	// cerifico se il giro di consegna è un ritiro cliente
	ls_flag_rc += "N"
	
	if not isnull(ls_cod_giro_consegna) then
		
		select flag_rc
		into   :ls_flag_rc
		from   tes_giri_consegne
		where  cod_azienda = :s_cs_xx.cod_azienda and
		 		 cod_giro_consegna = :ls_cod_giro_consegna;
		if sqlca.sqlcode <> 0 then ls_flag_rc ="N"
		
	end if
	
	ls_log = wf_add_log(ls_log, ls_flag_rc)
			
	// azzero variabili buffer
 	ll_cont = 0
	ls_cod_reparto_tempi[] 	= ls_vuoto[]
	ls_reparti_distinta[]  	= ls_vuoto[]
	ls_cod_reparto_tempi[]	= ls_vuoto[]
	ls_cod_gruppo_variante_tempi[]	= ls_vuoto[]
	ld_tempo_reparto[] 		= ld_vuoto[]
	ld_tempo_reparto_rc[]	= ld_vuoto[]
	
	select count(*)
	into   :ll_cont
	from   tab_flags_configuratore
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_modello = :ls_cod_prodotto;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("SEP","Errore in ricerca modello prodotto in tab_flags_configuratore~r~n" + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	// il prodotto non ha distinta oppure non è un modello
	if isnull(ls_cod_versione) or ll_cont = 0 then
		
		ls_log = wf_add_log(ls_log, "N")

		select lead_time, cod_reparto
		into   :ld_lead_time, :ls_cod_reparto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("SEP","Errore in ricerca prodotto in anag_prodotti~r~n" + sqlca.sqlerrtext)
			rollback;
			return -1
		end if

		ld_tempo_riga_ordine = ld_lead_time * ld_quan_ordine

		ls_log = wf_add_log(ls_log, ld_lead_time)
		ls_log = wf_add_log(ls_log, ls_cod_reparto)
		ls_log = wf_add_log(ls_log, ld_tempo_riga_ordine)
		ls_log = wf_add_log(ls_log, "Caso Prodotto senza distinta o configuratore: potrebbe essere un ADD/OPT legato ad un prodotto finito.")
		
	else
		// prodotto che ha distinta base e che è un modello configuratore
		ls_log = wf_add_log(ls_log, "S")
		
		// 1) estraggo tutti i reparti di questo prodotto dalla distinta base
		
		//Donato 01/02/2012 Spostata funzione globale in user object oggetto
		luo_funzioni = create uo_funzioni_1
		ll_ret = luo_funzioni.uof_trova_reparti(	true, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, &
															ls_cod_prodotto, ls_cod_versione, ref ls_reparti_distinta[], ref ls_errore)
		destroy luo_funzioni
		//ll_ret = f_trova_reparti(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_prodotto, ls_cod_versione, ref ls_reparti_distinta[], ref ls_errore)
		//fine modifica --------------------------
		
		if ll_ret < 0 then
			g_mb.messagebox("SEP","Errore in ricerca reparti~r~n" + ls_errore)
			rollback;
			return -1
		end if
		
		ll_index = 0
		
		for ll_i = 1 to upperbound(ls_reparti_distinta)

			setnull(ls_cod_gruppo_variante)
			setnull(ls_cod_reparto)
			ll_cont = 0
			
			// EnMe 28/05/2010 richiesto da Daniele Riello.
			// Verifico se la produzione è terminata; se è terminata allora "scarto" l'ordine.
			
			select flag_fine_fase
			into   :ls_flag_fine_fase
			from   det_ordini_produzione
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and
					 prog_riga_ord_ven = :ll_prog_riga_ord_ven and
					 cod_reparto = :ls_reparti_distinta[ll_i];
			
			choose case sqlca.sqlcode 
				case 0
					// se la fase è conclusa allora salto
					if ls_flag_fine_fase = "S" then continue
					
				case 100
					// non trovato in det_ordini_produzione, provo a controllare in tes_ordini_produzione
					select fine_preparazione, flag_pronto_complessivo
					into   :ldt_fine_preparazione, :ls_flag_pronto_complessivo
					from   tes_ordini_produzione
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 anno_registrazione = :ll_anno_registrazione and
							 num_registrazione = :ll_num_registrazione and
							 cod_reparto = :ls_reparti_distinta[ll_i];
							 
					choose case sqlca.sqlcode
							
						case 0
							// se la fase è conclusa allora salto
							if not isnull(ldt_fine_preparazione) and ls_flag_pronto_complessivo = "S" then continue 
							
						case else
							g_mb.messagebox("SEP","Errore durante la verifica dello stato di avazamento produzione su tes_ordini_produzione (wf_calcola_ore_produzione)~r~nRiga Ordine "+string(ll_anno_registrazione)+ "/" + string(ll_num_registrazione) + "/" + string(ll_prog_riga_ord_ven) + "PRENDERE NOTA, VERIFICARE E PROSEGUIRE~r~n" + ls_errore)
							//rollback;
							//return -1
							continue
					end choose
					
				case else
					g_mb.messagebox("SEP","Errore durante la verifica dello stato di avazamento produzione su det_ordini_produzione (wf_calcola_ore_produzione)~r~nRiga Ordine "+string(ll_anno_registrazione)+ "/" + string(ll_num_registrazione) + "/" + string(ll_prog_riga_ord_ven) + "PRENDERE NOTA, VERIFICARE E PROSEGUIRE~r~n" + ls_errore)
					//rollback;
					//return -1
					continue		
			end choose
			
			// ------- fine modifica 28/05/2010
			
			
			select count(cod_gruppo_variante), cod_gruppo_variante
			into   :ll_cont, :ls_cod_gruppo_variante
			from   tab_distinta_reparti_tempi
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto_distinta = :ls_cod_prodotto and
					 cod_versione = :ls_cod_versione and
					 cod_reparto = :ls_reparti_distinta[ll_i] and
					 cod_gruppo_variante is not null
			group by cod_gruppo_variante;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("SEP","Errore in select in tab_distinta_reparti_tempi~r~n" + sqlca.sqlerrtext)
				rollback;
				return -1
			end if

			ls_log = wf_add_log(ls_log, ls_reparti_distinta[ll_i])
	
			// verifico se ho trovato l'associazione    prodotto/versione/reparto/gruppo_variante;
			if ll_cont > 0 then
				
				ll_index ++
				ls_cod_reparto_tempi[ll_index] = ls_reparti_distinta[ll_i]
				ls_cod_gruppo_variante_tempi[ll_index] = ls_cod_gruppo_variante

				ls_log = wf_add_log(ls_log, ls_cod_gruppo_variante)
				// se esiste più di una associazione distinta/reparto/gruppo_variante segnalo la cosa, ma vado avanti lo stesso
				if ll_cont > 1 then
					g_mb.messagebox("SEP","ATTENIONE: l'associazione prodotto/versione/reparto (" + ls_cod_prodotto +"-" +  ls_cod_versione +"-" + ls_cod_reparto +  ")  è presente con più ricorrenze per gruppo_variante; non è corretto, ma vado avanti lo stesso prendendo il gruppo variante " + ls_cod_gruppo_variante)
				end if
				
				select tempo_produzione
				into   :ld_tempo_reparto[ll_index]
				from   tab_distinta_reparti_tempi
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto_distinta = :ls_cod_prodotto and
						 cod_versione = :ls_cod_versione and
						 cod_reparto = :ls_cod_reparto_tempi[ll_index] and
						 cod_gruppo_variante = :ls_cod_gruppo_variante_tempi[ll_index];
				if sqlca.sqlcode <> 0 then
					// segnalo errore anche se non lo trovo; il record DEVE esserci!
					g_mb.messagebox("SEP","Errore in select tempo_produzione in tab_distinta_reparti_tempi~r~n" + sqlca.sqlerrtext)
					rollback;
					return -1
				end if
				
				if isnull(ld_tempo_reparto[ll_index]) then ld_tempo_reparto[ll_index] = 0
				
				// cerca la quantità della variante nella DB
				ll_ret = wf_trova_quan_variante(ls_cod_prodotto, ls_cod_versione, ls_cod_gruppo_variante_tempi[ll_index], ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ref ls_cod_prodotto_variante, ref ld_quan_prodotto_variante, ref ls_errore)
				
				// moltiplica la quantità trovata per il tempo_produzione
				
				if len(ls_cod_prodotto_variante) > 0 and not isnull(ls_cod_prodotto_variante) then
					
					if ld_quan_prodotto_variante <> 0 and not isnull(ld_quan_prodotto_variante) then
						ld_tempo_reparto[ll_index] = (ld_tempo_reparto[ll_index] * ld_quan_prodotto_variante) * ld_quan_ordine
						ld_tempo_produzione = ld_tempo_reparto[ll_index]
					else
						//g_mb.messagebox("SEP","ATTENZIONE: la quantità della variante " + ls_cod_gruppo_variante_tempi[ll_index] + " è zero, quindi considero il tempo unitario")
						ld_tempo_produzione = ld_tempo_reparto[ll_index] * ld_quan_ordine
					end if
					
					ls_log = wf_add_log(ls_log, ld_tempo_reparto[ll_index])
					ls_log = wf_add_log(ls_log, ls_cod_prodotto_variante)
					ls_log = wf_add_log(ls_log, ld_quan_prodotto_variante)
					ls_log = wf_add_log(ls_log, ld_tempo_produzione)
					ls_log = wf_add_log(ls_log, "Tempo Rep-Var:Variante Trovata")
								 
				else  // prodotto variante non trovato
					ld_tempo_produzione = ld_tempo_reparto[ll_index] * ld_quan_ordine
					
					ls_log = wf_add_log(ls_log, ld_tempo_reparto[ll_index])
					ls_log = wf_add_log(ls_log, ls_cod_prodotto_variante)
					ls_log = wf_add_log(ls_log, ld_quan_prodotto_variante)
					ls_log = wf_add_log(ls_log, ld_tempo_produzione)
					ls_log = wf_add_log(ls_log, "Tempo Rep-Var:Variante Trovata")

				end if
				
				
				// se l'ordine appartiene ad giro RC allora incremento anche il tempo di RC
				if ls_flag_rc = "S" then 
					ld_tempo_reparto_rc[ll_index] = ld_tempo_reparto[ll_index]
				else
					ld_tempo_reparto_rc[ll_index] = 0
				end if

			
			else // se non trovo i tempi per il gruppo_variante,  prendo il tempo per prodotto/versione/reparto
				
				select tempo_produzione
				into   :ld_tempo_produzione
				from   tab_distinta_reparti_tempi
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto_distinta = :ls_cod_prodotto and
						 cod_versione 	= :ls_cod_versione and
						 cod_reparto 	= :ls_reparti_distinta[ll_i] and
						 cod_gruppo_variante is null;
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("SEP","Errore in select tempo_produzione in tab_distinta_reparti_tempi~r~n" + sqlca.sqlerrtext)
					rollback;
					return -1
				end if
				
				// non ci sono tempi caricati per questo reparto, quindi salto al reparto successivo
				if sqlca.sqlcode = 100 then continue
				
				ll_index ++
				ls_cod_reparto_tempi[ll_index] = ls_reparti_distinta[ll_i]
				ld_tempo_reparto[ll_index] = ld_tempo_produzione * ld_quan_ordine
				
				ls_log = wf_add_log(ls_log, ld_tempo_reparto[ll_index])
				ls_log = wf_add_log(ls_log, "Tempo Rep.")


				// se l'ordine appartiene ad giro RC allora incremento anche il tempo di RC
				if ls_flag_rc = "S" then 
					ld_tempo_reparto_rc[ll_index] = ld_tempo_reparto[ll_index]
				else
					ld_tempo_reparto_rc[ll_index] = 0
				end if
				
			end if
			
		next
	
	end if
	
	for ll_i = 1 to upperbound(ld_tempo_reparto)
		
		if not cbx_domenica.checked then
			// verifico se si tratta di un sabato o domenica
			
			if daynumber( date(ldt_data_produzione) ) = 1 then
				// in questo caso incremento ore sul venerdì
				ldd_data_produzione = date(ldt_data_produzione)
				ldd_data_produzione = relativedate(ldd_data_produzione, -2)
				ldt_data_produzione = datetime(ldd_data_produzione,00:00:00)
			
			end if
			
			if daynumber( date(ldt_data_produzione) ) = 7 then
				// in questo caso incremento ore sul venerdì
				ldd_data_produzione = date(ldt_data_produzione)
				ldd_data_produzione = relativedate(ldd_data_produzione, -1)
				ldt_data_produzione = datetime(ldd_data_produzione,00:00:00)
			
			end if
		end if

		update tab_cal_produzione
		set minuti_effettivi = minuti_effettivi + :ld_tempo_reparto[ll_i],
		    minuti_giro_rc   = minuti_giro_rc   + :ld_tempo_reparto_rc[ll_i]
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_reparto = :ls_cod_reparto_tempi[ll_i] and
				data_giorno = :ldt_data_produzione;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("SEP","Errore in update tabella tab_cal_produzione~r~n" + sqlca.sqlerrtext)
			rollback;
			return -1
		end if
		
	next
	
	if cbx_log.checked then
		FileWriteEx(il_file, ls_log )
	end if
			
	ld_tempo_riga_ordine = 0

loop

close cu_det_ord_ven_prod;

// --------------------  inizio elaborazione per gli ordini SFUSI ---------------------------------

open cu_sfusi;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("SEP","Errore in OPEN cursore 'cu_det_ord_ven_prod'~r~n" + sqlca.sqlerrtext)
	rollback;
	return -1
end if

do while true
	fetch cu_sfusi into :ll_anno_registrazione, :ll_num_registrazione, :ll_prog_riga_ord_ven, :ldt_data_consegna, :ls_cod_giro_consegna,
											 :ls_cod_prodotto, 		 :ls_cod_tipo_det_ven,  :ls_cod_versione,      :ld_quan_ordine;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("SEP","Errore in FETCH cursore 'cu_det_ord_ven_prod'~r~n" + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	ll_index = 0
	
	st_log.text = string(ll_anno_registrazione) + "-" + &
						string(ll_num_registrazione) + "-" + &
						string(ll_prog_riga_ord_ven)
	Yield()
	
	ls_log = ""
	
	ls_log = wf_add_log(ls_log, ll_anno_registrazione)
	ls_log = wf_add_log(ls_log, ll_num_registrazione)
	ls_log = wf_add_log(ls_log, ll_prog_riga_ord_ven)
	ls_log = wf_add_log(ls_log, ldt_data_consegna)
	ls_log = wf_add_log(ls_log, ls_cod_giro_consegna)
	
	if isnull(ls_cod_prodotto) then continue
	
	ls_log = wf_add_log(ls_log, ls_cod_prodotto)
	
	// Calcolo la data di produzione, che deve essere 1 giorno prima della data consegna
	ldd_data = date(ldt_data_consegna)
	ldd_data = relativedate(ldd_data, -1)
	ldt_data_produzione = datetime(ldd_data, 00:00:00)
	
	ls_log = wf_add_log(ls_log, ldt_data_produzione)

	// Verifico se il giro di consegna è un ritiro cliente
	ls_flag_rc += "N"
	
	if not isnull(ls_cod_giro_consegna) then
		
		select flag_rc
		into   :ls_flag_rc
		from   tes_giri_consegne
		where  cod_azienda = :s_cs_xx.cod_azienda and
		 		 cod_giro_consegna = :ls_cod_giro_consegna;
		if sqlca.sqlcode <> 0 then ls_flag_rc ="N"
		
	end if
	
	ls_log = wf_add_log(ls_log, ls_flag_rc)
	
	select cod_reparto
	into   :ls_cod_reparto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("SEP","Prodotto " + ls_cod_prodotto + " non trovato in anagrafica~r~n" + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	if not cbx_domenica.checked then
		// verifico se si tratta di un sabato o domenica
		
		if daynumber( date(ldt_data_produzione) ) = 1 then
			// in questo caso incremento ore sul venerdì
			ldd_data_produzione = date(ldt_data_produzione)
			ldd_data_produzione = relativedate(ldd_data_produzione, -2)
			ldt_data_produzione = datetime(ldd_data_produzione,00:00:00)
		
		end if
		
		if daynumber( date(ldt_data_produzione) ) = 7 then
			// in questo caso incremento ore sul venerdì
			ldd_data_produzione = date(ldt_data_produzione)
			ldd_data_produzione = relativedate(ldd_data_produzione, -1)
			ldt_data_produzione = datetime(ldd_data_produzione,00:00:00)
		
		end if
	end if

	
	choose case ls_flag_rc
			
		case "S"		// ritiro cliente
	
			update tab_cal_produzione
			set 	 minuti_effettivi = minuti_effettivi + :ld_minuti_sfuso
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_reparto = :ls_cod_reparto and
					data_giorno = :ldt_data_produzione;
					
		case "N"
			
			update tab_cal_produzione
			set 	 minuti_giro_rc   = minuti_giro_rc   + :ld_minuti_sfuso
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_reparto = :ls_cod_reparto and
					data_giorno = :ldt_data_produzione;
					
	end choose
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("SEP","Errore in update tabella tab_cal_produzione~r~n" + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	ls_log = wf_add_log(ls_log, ld_minuti_sfuso)
	ls_log = wf_add_log(ls_log, "Tempo Sfuso")
	
	if cbx_log.checked then
		FileWriteEx(il_file, ls_log )
	end if

loop

close cu_sfusi;

// fine: consolido tutto nel DB

commit;

st_log.text = "Pronto!"

return 0














end function

public function integer wf_trova_quan_variante (string fs_cod_prodotto_finito, string fs_cod_versione, string fs_cod_gruppo_variante, long fl_anno_ordine, long fl_num_ordine, long fl_prog_riga_ord_ven, ref string fs_cod_prodotto_variante, ref decimal fd_quan_prodotto_variante, ref string fs_errore);string ls_cod_prodotto, ls_errore
dec{4} ld_quan_variante
uo_conf_varianti luo_ins_varianti

luo_ins_varianti = CREATE uo_conf_varianti
if luo_ins_varianti.uof_ricerca_quan_mp(fs_cod_prodotto_finito,  &
												fs_cod_versione, &
												1, &
												fs_cod_gruppo_variante, &
												fl_anno_ordine, &
												fl_num_ordine, &
												fl_prog_riga_ord_ven, &
												"varianti_det_ord_ven", &
												ls_cod_prodotto, &
												ld_quan_variante,&
												ls_errore) = -1 then
	fs_errore = ls_errore
	destroy luo_ins_varianti
	return -1
end if

fs_cod_prodotto_variante = ls_cod_prodotto
fd_quan_prodotto_variante = ld_quan_variante

return 0
end function

public function string wf_add_log (string fs_log, string fs_string);if isnull(fs_string) then
	fs_log = fs_log + "~t"
else
	fs_log = fs_log + fs_string  + "~t"
end if

return fs_log
end function

public function string wf_add_log (string fs_log, datetime fs_datetime);if isnull(fs_datetime) then
	fs_log = fs_log + "~t"
else
	fs_log = fs_log + string(fs_datetime, "dd/mm/yyyy")  + "~t"
end if

return fs_log
end function

public function string wf_add_log (string fs_log, decimal fs_decimal);if isnull(fs_decimal) then
	fs_log = fs_log + "~t"
else
	fs_log = fs_log + string(fs_decimal, "########0.0000")  + "~t"
end if

return fs_log
end function

public function string wf_add_log (string fs_log, long fs_long);if isnull(fs_long) then
	fs_log = fs_log + "~t"
else
	fs_log = fs_log + string(fs_long, "########0")  + "~t"
end if

return fs_log
end function

public function boolean wf_check_reparto (string fs_cod_reparto);long ll_count, ll_index

ll_count = upperbound(is_reparti_calendario)

//se vuoto carica tutti i reparti 
if ll_count <= 0 then return true

for ll_index = 1 to ll_count
	if fs_cod_reparto = is_reparti_calendario[ll_index] then
		//reparto trovato: fallo vedere
		return true
	end if
next

return false
end function

public subroutine wf_leggi_filtro_reparti ();
string ls_memo, ls_cod_utente, ls_parser


ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""

select filtri 
into   :ls_memo
from   filtri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'CALEN';
		 
if sqlca.sqlcode = 100 then
	//nessun filtro memorizzato: metti tutti a Selezionato (è il valore di default quindi fai semplicemente return)
	//g_mb.messagebox("OMNIA", "Nessun filtro reparti memorizzato.")
	return
end if
if sqlca.sqlcode <> 0 then
	g_mb.error("OMNIA", "Errore in lettura impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
	return
end if

if trim(ls_memo)="" or isnull(ls_memo) then return

ls_parser = "~t"
wf_parse_reparti(ls_memo, ls_parser)


return
end subroutine

public subroutine wf_parse_reparti (string fs_stringa, string fs_parser);string ls_tmp, ls_vuoto[]
long ll_pos, ll_pos2, ll_index

ll_pos = 1
ll_index = 0
is_reparti_calendario = ls_vuoto

do while true
	if len(fs_stringa) > 0 and not isnull(fs_stringa) then
		ll_pos2 = pos(fs_stringa, fs_parser, ll_pos)
		
		ls_tmp = mid(fs_stringa, ll_pos, ll_pos2 - ll_pos)
		
		if isnull(ls_tmp) or trim(ls_tmp)="" then exit
		
		ll_index += 1
		is_reparti_calendario[ll_index] = ls_tmp
		
		ll_pos = ll_pos2 + 1
	end if
loop
end subroutine

public function integer wf_amministrazione ();//questa funzione abilita/disabilita alcune funzionalità della finestra in base al parametro aziendale ACP
string ls_utente_collegato

select stringa
into :is_ACP
from parametri_azienda
where cod_azienda=:s_cs_xx.cod_azienda and
		flag_parametro='S' and
		cod_parametro='ACP';

if is_ACP<>"" and not isnull(is_ACP) then
	//aggiungi l'eventuale utente Consulting&Software come amministratore
	is_ACP +=",'CS_SYSTEM'"
else
	//lascia abilitato tutto
	ib_amministra = true
	
	return 1
end if

ls_utente_collegato = "'"+s_cs_xx.cod_utente+"'"

if pos(is_ACP, ls_utente_collegato) > 0 then
	//OK lascia tutto abilitato
	ib_amministra = true
else
	//no GRANT, disabilita quello che devi disabilitare
	ib_amministra = false
end if

return 1
end function

on w_cal_produzione.create
int iCurrent
call super::create
this.cb_reparti=create cb_reparti
this.em_timer=create em_timer
this.cbx_timer=create cbx_timer
this.mle_log=create mle_log
this.cbx_domenica=create cbx_domenica
this.pb_colore_4=create pb_colore_4
this.pb_colore_3=create pb_colore_3
this.pb_colore_2=create pb_colore_2
this.pb_colore_1=create pb_colore_1
this.st_zoom=create st_zoom
this.st_log=create st_log
this.ddlb_zoom=create ddlb_zoom
this.cbx_log=create cbx_log
this.cb_print=create cb_print
this.cb_plan=create cb_plan
this.cb_6=create cb_6
this.cb_view_plan=create cb_view_plan
this.em_fine=create em_fine
this.em_inizio=create em_inizio
this.mle_1=create mle_1
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_reparti
this.Control[iCurrent+2]=this.em_timer
this.Control[iCurrent+3]=this.cbx_timer
this.Control[iCurrent+4]=this.mle_log
this.Control[iCurrent+5]=this.cbx_domenica
this.Control[iCurrent+6]=this.pb_colore_4
this.Control[iCurrent+7]=this.pb_colore_3
this.Control[iCurrent+8]=this.pb_colore_2
this.Control[iCurrent+9]=this.pb_colore_1
this.Control[iCurrent+10]=this.st_zoom
this.Control[iCurrent+11]=this.st_log
this.Control[iCurrent+12]=this.ddlb_zoom
this.Control[iCurrent+13]=this.cbx_log
this.Control[iCurrent+14]=this.cb_print
this.Control[iCurrent+15]=this.cb_plan
this.Control[iCurrent+16]=this.cb_6
this.Control[iCurrent+17]=this.cb_view_plan
this.Control[iCurrent+18]=this.em_fine
this.Control[iCurrent+19]=this.em_inizio
this.Control[iCurrent+20]=this.mle_1
this.Control[iCurrent+21]=this.dw_1
end on

on w_cal_produzione.destroy
call super::destroy
destroy(this.cb_reparti)
destroy(this.em_timer)
destroy(this.cbx_timer)
destroy(this.mle_log)
destroy(this.cbx_domenica)
destroy(this.pb_colore_4)
destroy(this.pb_colore_3)
destroy(this.pb_colore_2)
destroy(this.pb_colore_1)
destroy(this.st_zoom)
destroy(this.st_log)
destroy(this.ddlb_zoom)
destroy(this.cbx_log)
destroy(this.cb_print)
destroy(this.cb_plan)
destroy(this.cb_6)
destroy(this.cb_view_plan)
destroy(this.em_fine)
destroy(this.em_inizio)
destroy(this.mle_1)
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;string ls_str
date ldd_today

ldd_today = today()

em_inizio.text = string(ldd_today, "dd/mm/yyyy")

ldd_today = relativedate(ldd_today, 30)

em_fine.text = string(ldd_today, "dd/mm/yyyy")

set_w_options(c_CloseNoSave + c_noenablepopup + c_NoResizeWin)

save_on_close(c_socnosave)

event post ue_posiziona_window()

event post ue_ridimensiona()

// carico codici dei colori
select stringa
into   :ls_str
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
  		 cod_parametro = 'CC1';
if sqlca.sqlcode <> 0 or isnull(ls_str) or len(ls_str) < 1 then
	pb_colore_1.backcolor = 8053814
else
	pb_colore_1.backcolor = long(ls_str)
end if

	
select stringa
into   :ls_str
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
  		 cod_parametro = 'CC2';
if sqlca.sqlcode <> 0 or isnull(ls_str) or len(ls_str) < 1 then
	pb_colore_2.backcolor = 8442861
else
	pb_colore_2.backcolor = long(ls_str)
end if

select stringa
into   :ls_str
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
  		 cod_parametro = 'CC3';
if sqlca.sqlcode <> 0 or isnull(ls_str) or len(ls_str) < 1 then
	pb_colore_3.backcolor = 1149386
else
	pb_colore_3.backcolor = long(ls_str)
end if

select stringa
into   :ls_str
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
  		 cod_parametro = 'CC4';
if sqlca.sqlcode <> 0 or isnull(ls_str) or len(ls_str) < 1 then
	//pb_colore_4.backcolor = 536870912
	pb_colore_4.backcolor = 16777215
else
	pb_colore_4.backcolor = long(ls_str)
end if


//visualizza ultima elaborazione
uo_calendario_prod_new luo_cal

luo_cal = create uo_calendario_prod_new

this.title = "Pianificazione Produzione Reparti ->  Ultima elaborazione: " + luo_cal.uof_leggi_ultima_elab()

destroy luo_cal;

wf_leggi_filtro_reparti()

wf_amministrazione()
end event

event resize;event ue_ridimensiona()
end event

event ue_anteprima;call super::ue_anteprima;messagebox("","anteprima")
end event

event timer;call super::timer;//cb_generate.triggerevent(clicked!)
cb_view_plan.postevent(clicked!)
end event

type cb_reparti from commandbutton within w_cal_produzione
integer x = 3269
integer y = 1412
integer width = 389
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Reparti"
end type

event clicked;string ls_vuoto[]


//s_cs_xx.parametri.parametro_s_4_a = is_reparti_calendario

open(w_cal_produzione_sel_reparti)

is_reparti_calendario = s_cs_xx.parametri.parametro_s_4_a[]

s_cs_xx.parametri.parametro_s_4_a = ls_vuoto

end event

type em_timer from editmask within w_cal_produzione
integer x = 3282
integer y = 220
integer width = 352
integer height = 80
integer taborder = 50
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "5"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##"
boolean spin = true
double increment = 1
string minmax = "1~~20"
end type

type cbx_timer from checkbox within w_cal_produzione
integer x = 3282
integer y = 312
integer width = 361
integer height = 76
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Timer"
end type

event clicked;long ll_timer

if this.checked then
	ll_timer = long(em_timer.text)
	if ll_timer>0 then
		ll_timer = ll_timer * 60
		timer(ll_timer)
	else
		g_mb.show("Inserire i minuti di ricalcolo automatico!")
		timer(0)
		return
	end if
else
	timer(0)
	return
end if
end event

type mle_log from multilineedit within w_cal_produzione
boolean visible = false
integer x = 2743
integer y = 2036
integer width = 882
integer height = 344
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean hscrollbar = true
boolean vscrollbar = true
boolean autohscroll = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type cbx_domenica from checkbox within w_cal_produzione
boolean visible = false
integer x = 3282
integer y = 644
integer width = 361
integer height = 80
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Sab./Dom."
end type

type pb_colore_4 from picturebutton within w_cal_produzione
integer x = 3264
integer y = 1896
integer width = 402
integer height = 108
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sfondo"
boolean originalsize = true
alignment htextalign = left!
end type

event clicked;string ls_color

//se non sei amministratore
if not ib_amministra then
	g_mb.error("Operazione non autorizzata per l'utente corrente!")
	return
end if

s_cs_xx.parametri.parametro_d_1 = backcolor

window_open(w_scegli_colori, 0)

backcolor = s_cs_xx.parametri.parametro_d_1

delete parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and cod_parametro = 'CC4';

ls_color = string( long(s_cs_xx.parametri.parametro_d_1) )

insert into parametri_azienda
	(cod_azienda,
	 flag_parametro,
	 cod_parametro,
	 stringa)
values
	 (:s_cs_xx.cod_azienda,
	  'S',
	  'CC4',
	  :ls_color);

commit;

end event

type pb_colore_3 from picturebutton within w_cal_produzione
integer x = 3269
integer y = 1780
integer width = 402
integer height = 108
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Fuori Soglia"
boolean originalsize = true
alignment htextalign = left!
end type

event clicked;string ls_color

//se non sei amministratore
if not ib_amministra then
	g_mb.error("Operazione non autorizzata per l'utente corrente!")
	return
end if

s_cs_xx.parametri.parametro_d_1 = backcolor

window_open(w_scegli_colori, 0)

backcolor = s_cs_xx.parametri.parametro_d_1

delete parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and cod_parametro = 'CC3';

ls_color = string( long(s_cs_xx.parametri.parametro_d_1) )

insert into parametri_azienda
	(cod_azienda,
	 flag_parametro,
	 cod_parametro,
	 stringa)
values
	 (:s_cs_xx.cod_azienda,
	  'S',
	  'CC3',
	  :ls_color);

commit;

end event

type pb_colore_2 from picturebutton within w_cal_produzione
integer x = 3264
integer y = 1664
integer width = 402
integer height = 108
integer taborder = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Soglia"
boolean originalsize = true
alignment htextalign = left!
end type

event clicked;string ls_color

//se non sei amministratore
if not ib_amministra then
	g_mb.error("Operazione non autorizzata per l'utente corrente!")
	return
end if

s_cs_xx.parametri.parametro_d_1 = backcolor

window_open(w_scegli_colori, 0)

backcolor = s_cs_xx.parametri.parametro_d_1

delete parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and cod_parametro = 'CC2';

ls_color = string( long(s_cs_xx.parametri.parametro_d_1) )

insert into parametri_azienda
	(cod_azienda,
	 flag_parametro,
	 cod_parametro,
	 stringa)
values
	 (:s_cs_xx.cod_azienda,
	  'S',
	  'CC2',
	  :ls_color);

commit;

end event

type pb_colore_1 from picturebutton within w_cal_produzione
integer x = 3264
integer y = 1548
integer width = 402
integer height = 108
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Disponibile"
boolean originalsize = true
alignment htextalign = left!
end type

event clicked;string ls_color

//se non sei amministratore
if not ib_amministra then
	g_mb.error("Operazione non autorizzata per l'utente corrente!")
	return
end if

s_cs_xx.parametri.parametro_d_1 = backcolor

window_open(w_scegli_colori, 0)

backcolor = s_cs_xx.parametri.parametro_d_1

delete parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and cod_parametro = 'CC1';

ls_color = string( long(s_cs_xx.parametri.parametro_d_1) )

insert into parametri_azienda
	(cod_azienda,
	 flag_parametro,
	 cod_parametro,
	 stringa)
values
	 (:s_cs_xx.cod_azienda,
	  'S',
	  'CC1',
	  :ls_color);

commit;

end event

type st_zoom from statictext within w_cal_produzione
integer x = 3273
integer y = 776
integer width = 270
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "ZOOM:"
boolean focusrectangle = false
end type

type st_log from statictext within w_cal_produzione
integer x = 3269
integer y = 956
integer width = 389
integer height = 80
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type ddlb_zoom from dropdownlistbox within w_cal_produzione
integer x = 3269
integer y = 852
integer width = 389
integer height = 452
integer taborder = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "100"
boolean allowedit = true
boolean border = false
boolean sorted = false
boolean vscrollbar = true
string item[] = {"5","10","25","50","75","90","100","125","150","200"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;long ll_zoom

ll_zoom = long( ddlb_zoom.text )
dw_1.Object.DataWindow.Zoom = ll_zoom

end event

event modified;long ll_zoom

ll_zoom = long( ddlb_zoom.text )
dw_1.Object.DataWindow.Zoom = ll_zoom

end event

type cbx_log from checkbox within w_cal_produzione
boolean visible = false
integer x = 3282
integer y = 564
integer width = 366
integer height = 80
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Log"
end type

type cb_print from commandbutton within w_cal_produzione
integer x = 3269
integer y = 1324
integer width = 389
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long ll_ret

ll_ret = printsetup()
if ll_ret < 0 then return

dw_1.Object.DataWindow.Print.Orientation = 1
ll_ret = dw_1.print( )
end event

type cb_plan from commandbutton within w_cal_produzione
integer x = 3269
integer y = 1236
integer width = 389
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Pianifica"
end type

event clicked;//se non sei amministratore
if not ib_amministra then
	g_mb.error("Operazione non autorizzata per l'utente corrente!")
	return
end if

window_open(w_carica_cal_produzione, 0)
end event

type cb_6 from commandbutton within w_cal_produzione
boolean visible = false
integer x = 2674
integer y = 2240
integer width = 402
integer height = 112
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "none"
end type

event clicked;STRING LS_ERRORS

mle_1.text = SQLCA.SyntaxFromSQL("select cod_prodotto, des_prodotto, cod_cat_mer, cod_misura_mag from anag_prodotti", "style(type=grid)", LS_ERRORS)

MESSAGEBOX("",LS_ERRORS)
end event

type cb_view_plan from commandbutton within w_cal_produzione
integer x = 3269
integer y = 400
integer width = 389
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Visualizza"
end type

event clicked;long     ll_zoom
datetime ldt_data_inizio, ldt_data_fine

ldt_data_inizio = datetime(date(em_inizio.text), 00:00:00)
ldt_data_fine = datetime(date(em_fine.text), 00:00:00)

dw_1.setredraw(false)

setpointer(Hourglass!)
wf_crea_calendario(ldt_data_inizio, ldt_data_fine)
setpointer(Arrow!)

dw_1.hscrollbar = true
dw_1.vscrollbar = true
dw_1.livescroll = true

dw_1.Object.DataWindow.Print.Preview.Rulers = 'No'
dw_1.Object.DataWindow.Print.Preview = 'No'

ll_zoom = long( ddlb_zoom.text )
dw_1.Object.DataWindow.Zoom = ll_zoom

dw_1.setredraw(true)


end event

type em_fine from editmask within w_cal_produzione
integer x = 3269
integer y = 120
integer width = 389
integer height = 80
integer taborder = 40
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "10/10/2008"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean dropdowncalendar = true
end type

type em_inizio from editmask within w_cal_produzione
integer x = 3269
integer y = 20
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "05/10/2008"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean dropdowncalendar = true
end type

type mle_1 from multilineedit within w_cal_produzione
boolean visible = false
integer x = 23
integer y = 1700
integer width = 2606
integer height = 680
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean autohscroll = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_1 from uo_std_dw within w_cal_produzione
integer x = 5
integer y = 4
integer width = 3241
integer height = 2436
integer taborder = 10
string dataobject = "d_dddw_string"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylebox!
end type

event doubleclicked;call super::doubleclicked;if isvalid(dwo) then
	string ls_name, ls_describe, ls_ret, ls_data
	
	decimal ld_ore, ld_repore, ld_soglia
	
	ls_name = dwo.name
		
	long ll_pos, ll_col
	date ldd_data
	datetime ldt_data_calendario
	string ls_cod_reparto
	string ls_runtime
	
	if row>0 then
		ls_cod_reparto = getitemstring(row, "cod_reparto")
		ls_runtime = "riga:"+string(row) + " - " + "oggetto:"+ls_name + " - " + "reparto:"+ls_cod_reparto
		
		ll_pos = pos(ls_name, "_", 1)
		ll_col = long(right(ls_name, len(ls_name) - ll_pos))
		
		try
			ls_data = getitemstring(row, "giorno_"+string(ll_col))
			if isdate(ls_data) then
				ldd_data = date(ls_data)
				ldt_data_calendario = datetime(ldd_data, 00:00:00)
				ls_runtime += " - Data:"+string(day(ldd_data))+"/"+string(month(ldd_data))+"/"+string(year(ldd_data))
			else
				ls_runtime += " - Data non riconosciuta: "+ls_data
				setnull(ldt_data_calendario)
			end if
			
		catch (throwable err)
			ls_runtime += " - Data non riconosciuta"
			setnull(ldt_data_calendario)
		end try
		
		
		if isnull(ldt_data_calendario) then
			g_mb.show("Data non riconosciuta!")
			return
		else
			s_cs_xx.parametri.parametro_s_4_a[1] = ls_cod_reparto
			s_cs_xx.parametri.parametro_data_4 = ldt_data_calendario
			s_cs_xx.parametri.parametro_b_2 = false	//considera sab o domenica come giorni utili
			
			//gestione fusione REPTS e REPTS3
			if ls_cod_reparto = is_repts1 then
				s_cs_xx.parametri.parametro_s_4_a[2] = is_repts2
			end if
			
			window_open(w_cal_produzione_dettaglio, -1)
		end if
		
	end if

end if
end event

