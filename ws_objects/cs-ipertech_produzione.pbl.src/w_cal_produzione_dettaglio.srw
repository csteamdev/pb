﻿$PBExportHeader$w_cal_produzione_dettaglio.srw
forward
global type w_cal_produzione_dettaglio from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_cal_produzione_dettaglio
end type
type cb_stampa from commandbutton within w_cal_produzione_dettaglio
end type
type dw_report from uo_std_dw within w_cal_produzione_dettaglio
end type
end forward

global type w_cal_produzione_dettaglio from w_cs_xx_principale
integer width = 3515
integer height = 1848
string title = "Dettaglio Reparto"
event ue_posiziona_window ( )
event ue_ridimensiona ( )
cb_1 cb_1
cb_stampa cb_stampa
dw_report dw_report
end type
global w_cal_produzione_dettaglio w_cal_produzione_dettaglio

type variables
boolean ib_alusistemi
end variables

forward prototypes
public function integer wf_stato_produzione_riga (long fl_row)
end prototypes

event ue_posiziona_window();move(1,1)

this.width = w_cs_xx_mdi.mdi_1.width - 60
this.height = w_cs_xx_mdi.mdi_1.height - 40
end event

event ue_ridimensiona();long ll_relative=0	//470

//dw_report.move(10,10)
dw_report.height = height - 20 - cb_stampa.height
dw_report.width 	= width - 30 - ll_relative
end event

public function integer wf_stato_produzione_riga (long fl_row);long ll_return, ll_anno, ll_num, ll_riga, ll_reparti_stato[], ll_i, ll_cont
uo_produzione luo_prod
string ls_messaggio, ls_reparti[]

ll_anno = dw_report.getitemnumber(fl_row, "anno_registrazione")
ll_num = dw_report.getitemnumber(fl_row, "num_registrazione")
ll_riga = dw_report.getitemnumber(fl_row, "prog_riga_ord_ven")

luo_prod = create uo_produzione


if ib_alusistemi then
	ll_return = luo_prod.uof_stato_prod_det_ord_ven(ll_anno,ll_num,ll_riga,ls_messaggio)
	luo_prod.uof_stato_prod_det_ord_ven_reparti( ll_anno,ll_num,ll_riga,ls_reparti[], ll_reparti_stato[], ls_messaggio)
	
	select count(*)
	into	:ll_cont
	from 	det_lancio_prod 
	where anno_reg_ord_ven=:ll_anno  and 
			num_reg_ord_ven=:ll_num  and 
			prog_riga_ord_ven=:ll_riga;
	
	// solo 2 reparti MAX
	for ll_i = upperbound(ll_reparti_stato[]) to 1 step -1
		if ll_i > 2 then continue // solo 2 reparti
		choose case ll_reparti_stato[ll_i]
			case 0,2
				dw_report.setitem(fl_row, "rep_"+string(ll_i),  ls_reparti[ll_i])
				if ll_cont > 0 then 
					dw_report.setitem(fl_row, "resp_"+string(ll_i) + "_stato",  "2")
				else
					dw_report.setitem(fl_row, "resp_"+string(ll_i) + "_stato",  "0")
				end if				
			case 1
				dw_report.setitem(fl_row, "rep_"+string(ll_i), "#"+ls_reparti[ll_i])
				dw_report.setitem(fl_row, "resp_"+string(ll_i) + "_stato",  "0")
				
			case else
				dw_report.setitem(fl_row, "cf_stato_prod", "-")
				
		end choose
		
	next
	
else
	ll_return = luo_prod.uof_stato_prod_det_ord_ven(ll_anno,ll_num,ll_riga,ls_messaggio)
	destroy luo_prod;
		
	choose case ll_return
		case 0
			dw_report.setitem(fl_row, "cf_stato_prod", "N")
			
		case 1
			dw_report.setitem(fl_row, "cf_stato_prod", "T")
			
		case 2
			dw_report.setitem(fl_row, "cf_stato_prod", "P")
			
		case else
			dw_report.setitem(fl_row, "cf_stato_prod", "-")
			
	end choose
end if

return 1
end function

on w_cal_produzione_dettaglio.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_stampa=create cb_stampa
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_stampa
this.Control[iCurrent+3]=this.dw_report
end on

on w_cal_produzione_dettaglio.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_stampa)
destroy(this.dw_report)
end on

event pc_setwindow;call super::pc_setwindow;string ls_cod_reparto, ls_vuoto[], ls_cod_reparto2
datetime ldt_data_consegna, ldt_data_produzione
date ldd_data_appo
boolean lb_sab_dom
decimal ld_tot_minuti_reparto
long ll_index

set_w_options(c_CloseNoSave + c_noenablepopup + c_NoResizeWin)
save_on_close(c_socnosave)

guo_functions.uof_get_parametro_azienda("ALU", ib_alusistemi)
if ib_alusistemi then dw_report.dataobject = "d_tab_cal_produzione_dettaglio_alu"

ls_cod_reparto = s_cs_xx.parametri.parametro_s_4_a[1]

ldt_data_produzione = s_cs_xx.parametri.parametro_data_4

ldt_data_produzione = datetime(date(ldt_data_produzione), 00:00:00)

//leggi la potenzialita del reparto
select 	sum(minuti_reparto)
into   	:ld_tot_minuti_reparto
from 		tab_cal_produzione
where 	cod_azienda = :s_cs_xx.cod_azienda and  
			cod_reparto = :ls_cod_reparto and
			data_giorno = :ldt_data_produzione;

if isnull(ld_tot_minuti_reparto) then ld_tot_minuti_reparto = 0

dw_report.object.t_minuti_potenziali.text = string(ld_tot_minuti_reparto, "###,###,##0")
dw_report.object.t_ore_potenziali.text = string(round(ld_tot_minuti_reparto / 60, 0))



lb_sab_dom = s_cs_xx.parametri.parametro_b_2

s_cs_xx.parametri.parametro_s_4_a = ls_vuoto[]
setnull(s_cs_xx.parametri.parametro_data_4)
s_cs_xx.parametri.parametro_b_2 = false

event post ue_posiziona_window()
event post ue_ridimensiona()



////la data passata qui va trattata, perchè si tratta di una data produzione e occorre "convertirla" in una data consegna
//
////la data produzione è un giorno prima della consegna, ma se cade di venerdi occorre ragionare un po
//ldd_data_appo = date(ldt_data_produzione)
//
//if daynumber(ldd_data_appo) = 6 and not lb_sab_dom then	//se è un venerdi e  inoltre sabato e domenica vanno saltati
//	//allora la data consegna deve essere per forza al lunedi
//	ldd_data_appo = relativedate(ldd_data_appo, 3)
//else
//	//considera normalmente che il giorno dopo è la consegna
//	ldd_data_appo = relativedate(ldd_data_appo, 1)
//end if
//ldt_data_consegna = datetime(ldd_data_appo, 00:00:00)
//-----------------------------------------------------------------------------------------------------------------------------------------

dw_report.settransobject(sqlca)

//if isnull(ls_cod_reparto2) then
//	dw_report.retrieve(s_cs_xx.cod_azienda, ls_cod_reparto, ldt_data_consegna)
//else
//	dw_report.retrieve(s_cs_xx.cod_azienda, ls_cod_reparto, ls_cod_reparto2, ldt_data_consegna)
//end if
dw_report.retrieve(s_cs_xx.cod_azienda, ls_cod_reparto, ldt_data_produzione)


//stato produzione riga
for ll_index=1 to dw_report.rowcount()
	wf_stato_produzione_riga(ll_index)
next

dw_report.object.t_data_produzione.text = string(ldt_data_produzione, "dd/MM/yyyy")
dw_report.object.datawindow.print.preview = "Yes"
end event

type cb_1 from commandbutton within w_cal_produzione_dettaglio
integer x = 2592
integer y = 24
integer width = 402
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Excel"
end type

event clicked;if dw_report.saveas("", Excel!, true) = 1 then
	g_mb.show("Esportazione avvenuta con successo!")
else
	g_mb.error("Esportazione non effettuata!")
end if
end event

type cb_stampa from commandbutton within w_cal_produzione_dettaglio
integer x = 32
integer y = 24
integer width = 402
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long ll_ret

ll_ret = printsetup()
if ll_ret < 0 then return

dw_report.Object.DataWindow.Print.Orientation = 1
ll_ret = dw_report.print( )
end event

type dw_report from uo_std_dw within w_cal_produzione_dettaglio
integer x = 27
integer y = 124
integer width = 3415
integer height = 1604
integer taborder = 10
string dataobject = "d_tab_cal_produzione_dettaglio_new"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylebox!
end type

