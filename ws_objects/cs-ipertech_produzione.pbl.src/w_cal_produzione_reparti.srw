﻿$PBExportHeader$w_cal_produzione_reparti.srw
forward
global type w_cal_produzione_reparti from w_cs_xx_principale
end type
type st_4 from statictext within w_cal_produzione_reparti
end type
type st_3 from statictext within w_cal_produzione_reparti
end type
type st_2 from statictext within w_cal_produzione_reparti
end type
type st_1 from statictext within w_cal_produzione_reparti
end type
type pb_colore_1 from picturebutton within w_cal_produzione_reparti
end type
type pb_colore_2 from picturebutton within w_cal_produzione_reparti
end type
type pb_colore_3 from picturebutton within w_cal_produzione_reparti
end type
type pb_colore_4 from picturebutton within w_cal_produzione_reparti
end type
type dw_1 from datawindow within w_cal_produzione_reparti
end type
end forward

global type w_cal_produzione_reparti from w_cs_xx_principale
integer width = 4951
integer height = 1740
string title = "Stato Reparti"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
st_4 st_4
st_3 st_3
st_2 st_2
st_1 st_1
pb_colore_1 pb_colore_1
pb_colore_2 pb_colore_2
pb_colore_3 pb_colore_3
pb_colore_4 pb_colore_4
dw_1 dw_1
end type
global w_cal_produzione_reparti w_cal_produzione_reparti

type variables
string				is_header, is_column, is_objects, is_footer, is_colonne_dinamiche, is_oggetti_dinamici

// array che memorizza la data relativa ad una certa colonna
date				idd_date_colonne[], idd_date_null[]

// larghezza della colonna
long				il_larghezza_colonna=182//, il_anno_reg_ordine, il_num_reg_ordine

datetime			idt_data_partenza, idt_date_pronto[]
//string				is_cod_prodotto, is_cod_versione
string				is_reparti[]
end variables

forward prototypes
public function integer wf_trova_reparti (ref string fs_des_reparti[], ref decimal fd_soglie_reparti[], ref string fs_msg)
public subroutine wf_crea_calendario ()
public subroutine wf_crea_calendario_reparti (datetime fd_inizio, datetime fd_fine)
public function datetime wf_min_data_pronto ()
public function boolean wf_check_se_produzione (date adt_data, ref string as_testo)
end prototypes

public function integer wf_trova_reparti (ref string fs_des_reparti[], ref decimal fd_soglie_reparti[], ref string fs_msg);long ll_cont


for ll_cont=1 to upperbound(is_reparti[])
	
	select des_reparto, soglia_carico
	into :fs_des_reparti[ll_cont], :fd_soglie_reparti[ll_cont]
	from anag_reparti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_reparto=:is_reparti[ll_cont];
	
	if sqlca.sqlcode < 0 then
		fs_msg = "Errore in ricerca dati reparto~r~n" + sqlca.sqlerrtext
		rollback;
		return -1
	end if
	
	if isnull(fs_des_reparti[ll_cont]) then fs_des_reparti[ll_cont]=""
	if isnull(fd_soglie_reparti[ll_cont]) then fd_soglie_reparti[ll_cont]=0
	
next




return 1
end function

public subroutine wf_crea_calendario ();long						ll_zoom, ll_gg_prima, ll_gg_dopo
datetime					ldt_data_inizio, ldt_data_fine
date						ldd_data

//imposta un periodo che comprenda la data partenza e la più piccola data pronto reparti
//e poi aggiungi qualche giorno agli estremi

//5 GG .... |  MIN  .........   partenza | .... 15 GG

ldt_data_inizio = wf_min_data_pronto()


ll_gg_prima = 5
ll_gg_dopo = 15

ldd_data = date(ldt_data_inizio)
ldd_data = relativedate(ldd_data, -ll_gg_prima)
ldt_data_inizio = datetime(ldd_data, 00:00:00)

ldd_data = date(idt_data_partenza)
ldd_data = relativedate(ldd_data, ll_gg_dopo)
ldt_data_fine = datetime(ldd_data, 00:00:00)

dw_1.setredraw(false)

wf_crea_calendario_reparti(ldt_data_inizio, ldt_data_fine)

dw_1.hscrollbar = true
dw_1.vscrollbar = true
dw_1.livescroll = true

dw_1.Object.DataWindow.Print.Preview.Rulers = 'No'
dw_1.Object.DataWindow.Print.Preview = 'No'

ll_zoom = long( "100" )
dw_1.Object.DataWindow.Zoom = ll_zoom

dw_1.setredraw(true)
end subroutine

public subroutine wf_crea_calendario_reparti (datetime fd_inizio, datetime fd_fine);string		ls_des_giorno, ls_columns, ls_objects,ls_dw_syntax, ls_error, ls_cod_reparto, ls_des_reparto, ls_sql,&
			ls_des_reparti[], ls_msg, ls_testo_aggiuntivo
long 		ll_giorni, ll_i, ll_daynumber, ll_y, ll_row_1, ll_row_2, ll_carico, ll_index
dec{4} 	ld_tot_minuti_reparto, ld_tot_minuti_effettivi, ld_tot_minuti_giro_rc, ld_soglia_carico, ld_soglia_effettiva, ld_soglie_reparti[]
date 		ldd_inizio, ldd_fine
boolean	lb_sab_dom
datetime ldt_data_produzione

//imposta il grassetto nella colonna relativa alla data partenza dell'ordine corrente
//serve solo quando si usa tale funzione per generare l'impegno reparti di un ordine
//(doppio clic sul semaforo) e non in generazione calendario generale

//inizialmente la variabile è vuota
ls_testo_aggiuntivo = ""
lb_sab_dom = false


f_imposta_struttura_cal_prod(is_header, is_column, is_objects, is_footer, pb_colore_4.backcolor)

ldd_inizio = date(fd_inizio)
ldd_fine   = date(fd_fine)

ll_giorni = daysafter(ldd_inizio, ldd_fine) + 1

// Creo una prima colonna con il totale cumulato alla data precendente rispetto alla data inizio
idd_date_colonne[]  = idd_date_null[]
idd_date_colonne[1] = relativedate(ldd_inizio, - 1)

ls_des_giorno = "TOT.AL" +"~n" + string(idd_date_colonne[1],"dd/mm")

//wf_crea_colonna(1, ls_des_giorno, ref ls_columns, ref ls_objects)
f_crea_colonna_cal_prod(1, ls_des_giorno, ref ls_columns, ref ls_objects, il_larghezza_colonna, &
								pb_colore_3.backcolor, pb_colore_2.backcolor, pb_colore_1.backcolor, ls_testo_aggiuntivo)

// Crea tante colonne quante sono le date richieste 

for ll_i = 1 to ll_giorni
	
	ll_y = ll_i + 1
	
	idd_date_colonne[ll_y] = relativedate(ldd_inizio, ll_i - 1)

	ll_daynumber  = daynumber( idd_date_colonne[ll_y] )
	
	ls_des_giorno = f_nome_giorno_cal_prod(ll_daynumber) + "~n" + string(idd_date_colonne[ll_y],"dd/mm")

	ls_testo_aggiuntivo = ""
	if idd_date_colonne[ll_y] = date(idt_data_partenza) then
		ls_testo_aggiuntivo = "PART."
		
	//elseif idd_date_colonne[ll_y]=date(ldt_data_produzione) then
	elseif wf_check_se_produzione(idd_date_colonne[ll_y], ls_testo_aggiuntivo) then
		//allora ls_testo_aggiountivo è stato modificato con il codice del reparto
		
	else
		ls_testo_aggiuntivo = ""
	end if
	f_crea_colonna_cal_prod(ll_y, ls_des_giorno, ref ls_columns, ref ls_objects, il_larghezza_colonna, &
									pb_colore_3.backcolor, pb_colore_2.backcolor, pb_colore_1.backcolor, ls_testo_aggiuntivo)
	
next

// crea la DW
ls_dw_syntax = IS_HEADER + "~n" + &
					IS_COLUMN  + "~n" + &
					ls_columns + &
					" ) "  + "~n" + &
					IS_OBJECTS  + "~n" + &
					ls_objects + &
					IS_FOOTER
					
					
dw_1.create(ls_dw_syntax, ls_error)


if len(ls_error) > 0 then
	g_mb.show("ERRORE", ls_error)
	rollback;
	return
end if

// Caricamento della DW con i dati effettivi del calendario

//// -----------------  caricamento dati da TAB_CAL_PRODUZIONE e visualizzazione ---------
/*	Per ogni reparto devono essere caricate 3 righe:
RIGA 1: le ore inserite nel calendario teorico di produzione
RIGA 2: le ore di produzione effettive escluse quelle provenienti da ordini con giro tipo Ritiro cliente
RIGA 3: le ore di produzione effettive SOLO provenienti da ordini con giro tipo Ritiro cliente
*/

if wf_trova_reparti(ls_des_reparti[], ld_soglie_reparti[], ls_msg) < 0 then
	g_mb.show("ERRORE", ls_msg)
	rollback;
	return
end if

for ll_index=1 to upperbound(is_reparti[])
	// mi passo tutti i reparti dell'azienda e per ogni reparto carico i dati
	ls_cod_reparto = is_reparti[ll_index]
	ls_des_reparto = ls_des_reparti[ll_index]
	ld_soglia_carico = ld_soglie_reparti[ll_index]
	
	if isnull(ld_soglia_carico) then ld_soglia_carico = 0

	// per ogni reparto inserisco 2 righe;
	// ora inserisco la prima che è il totale delle ore potenziali in rapporto con le ore totalizzate dagli ordini
	ll_row_1 = dw_1.insertrow(0)
	dw_1.setitem(ll_row_1, "cod_reparto", ls_cod_reparto )
	dw_1.setitem(ll_row_1, "des_reparto", ls_des_reparto + "-GIRO")
	
	ll_row_2 = dw_1.insertrow(0)
	dw_1.setitem(ll_row_2, "cod_reparto", ls_cod_reparto)
	dw_1.setitem(ll_row_2, "des_reparto", ls_des_reparto + "-RC")
	
	for ll_i = 1 to upperbound(idd_date_colonne)
		
		if ll_i = 1 then
			// somma del totale alle date precedenti
			select 	sum(isnull(minuti_reparto, 0)),
						sum(isnull(minuti_effettivi, 0)), 
						sum(isnull(minuti_giro_rc, 0))
			into   		:ld_tot_minuti_reparto,
						:ld_tot_minuti_effettivi, 
						:ld_tot_minuti_giro_rc
			from 		tab_cal_produzione
			where 	cod_azienda = :s_cs_xx.cod_azienda and  
						cod_reparto = :ls_cod_reparto and
						data_giorno <= :idd_date_colonne[ll_i];
			
			if isnull(ld_tot_minuti_effettivi) then ld_tot_minuti_effettivi = 0
			if isnull(ld_tot_minuti_giro_rc) then ld_tot_minuti_giro_rc = 0
			if isnull(ld_soglia_carico) then ld_soglia_carico = 0
			
			ld_soglia_effettiva = ld_tot_minuti_effettivi - (ld_tot_minuti_effettivi * ld_soglia_carico) / 100
			
			dw_1.setitem(ll_row_1, "giorno_" + string(ll_i), "PREC")
			dw_1.setitem(ll_row_1, "soglia_" + string(ll_i), 0)
			dw_1.setitem(ll_row_1, "ore_" + string(ll_i), round(ld_tot_minuti_reparto / 60, 0) )
			dw_1.setitem(ll_row_1, "repore_" + string(ll_i), round(ld_tot_minuti_effettivi / 60, 0) )
			dw_1.setitem(ll_row_1, "repore_confronto_" + string(ll_i), round(ld_tot_minuti_effettivi / 60, 0) )

			dw_1.setitem(ll_row_1, "giorno_" + string(ll_i), "PREC")
			dw_1.setitem(ll_row_2, "soglia_" + string(ll_i), 0)
			dw_1.setitem(ll_row_2, "ore_" + string(ll_i), round(ld_tot_minuti_reparto / 60, 0) )
			dw_1.setitem(ll_row_1, "repore_confronto_" + string(ll_i), round(ld_tot_minuti_effettivi / 60, 0) )
			
		else
			// situazione precisa alla data
			select 	minuti_reparto, 
						minuti_effettivi, 
						minuti_giro_rc
			into   		:ld_tot_minuti_reparto, 
						:ld_tot_minuti_effettivi, 
						:ld_tot_minuti_giro_rc
			from 		tab_cal_produzione
			where 	cod_azienda = :s_cs_xx.cod_azienda and  
						cod_reparto = :ls_cod_reparto and
						data_giorno = :idd_date_colonne[ll_i];
			
			ld_soglia_effettiva = (ld_tot_minuti_reparto * ld_soglia_carico) / 100

			dw_1.setitem(ll_row_1, "giorno_" + string(ll_i), string( idd_date_colonne[ll_i], "dd/mm/yyyy" ))
			dw_1.setitem(ll_row_1, "soglia_" + string(ll_i), round(ld_soglia_effettiva   / 60,0) )
			dw_1.setitem(ll_row_1, "ore_"    + string(ll_i), round(ld_tot_minuti_reparto / 60, 0) )
			dw_1.setitem(ll_row_1, "repore_" + string(ll_i), round(ld_tot_minuti_effettivi / 60, 0) )
			dw_1.setitem(ll_row_1, "repore_confronto_" + string(ll_i), round(ld_tot_minuti_effettivi / 60, 0) )

			dw_1.setitem(ll_row_2, "giorno_" + string(ll_i), string( idd_date_colonne[ll_i], "dd/mm/yyyy" ))
			dw_1.setitem(ll_row_2, "soglia_" + string(ll_i), round(ld_soglia_effettiva   / 60,0) )
			dw_1.setitem(ll_row_2, "ore_"    + string(ll_i), round(ld_tot_minuti_reparto / 60, 0) )
			dw_1.setitem(ll_row_2, "repore_" + string(ll_i), round(ld_tot_minuti_giro_rc / 60, 0) )
			dw_1.setitem(ll_row_2, "repore_confronto_" + string(ll_i), round(ld_tot_minuti_effettivi / 60, 0) )
			
		end if
	next
next

return
end subroutine

public function datetime wf_min_data_pronto ();
datetime			ldt_return
long				ll_index

ldt_return = datetime(date(2999, 12, 31), 00:00:00)

for ll_index=1 to upperbound(idt_date_pronto[])
	if not isnull(idt_date_pronto[ll_index]) then
		if idt_date_pronto[ll_index] < ldt_return then ldt_return = idt_date_pronto[ll_index]
	end if
next

if ldt_return = datetime(date(2999, 12, 31), 00:00:00) then ldt_return = datetime(relativedate(date(idt_data_partenza), -10), 00:00:00)

return ldt_return
end function

public function boolean wf_check_se_produzione (date adt_data, ref string as_testo);
//controlla se la data elaborata è una data produzione di qualche reparto
//restituisce un testo per la colonna

long				ll_index


as_testo = ""

for ll_index=1 to upperbound(idt_date_pronto[])
	if not isnull(idt_date_pronto[ll_index]) then
		if date(idt_date_pronto[ll_index]) = adt_data then
			//è la data produzione di uno dei reparti ...
			as_testo = is_reparti[ll_index]
			return true
		end if
	end if
next


return false
end function

on w_cal_produzione_reparti.create
int iCurrent
call super::create
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.pb_colore_1=create pb_colore_1
this.pb_colore_2=create pb_colore_2
this.pb_colore_3=create pb_colore_3
this.pb_colore_4=create pb_colore_4
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_4
this.Control[iCurrent+2]=this.st_3
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.pb_colore_1
this.Control[iCurrent+6]=this.pb_colore_2
this.Control[iCurrent+7]=this.pb_colore_3
this.Control[iCurrent+8]=this.pb_colore_4
this.Control[iCurrent+9]=this.dw_1
end on

on w_cal_produzione_reparti.destroy
call super::destroy
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.pb_colore_1)
destroy(this.pb_colore_2)
destroy(this.pb_colore_3)
destroy(this.pb_colore_4)
destroy(this.dw_1)
end on

event pc_setwindow;call super::pc_setwindow;string						ls_str, ls_vuoto[]
datetime					ldt_data_partenza, ldt_vuoto[]
		

set_w_options(c_CloseNoSave + c_noenablepopup + c_NoResizeWin)
save_on_close(c_socnosave)


if upperbound(s_cs_xx.parametri.parametro_s_4_a[]) = 0 then
	g_mb.error("Nessun reparto da visualizzare!")
	s_cs_xx.parametri.parametro_s_4_a = ls_vuoto[]
	setnull(s_cs_xx.parametri.parametro_data_4)
	s_cs_xx.parametri.parametro_data_1_a[] = ldt_vuoto[]
	this.postevent(close!)
	return
end if

is_reparti[] = s_cs_xx.parametri.parametro_s_4_a[]
idt_data_partenza = s_cs_xx.parametri.parametro_data_4
idt_date_pronto[] = s_cs_xx.parametri.parametro_data_1_a[]

//pulizia variabili globali
s_cs_xx.parametri.parametro_s_4_a = ls_vuoto[]
setnull(s_cs_xx.parametri.parametro_data_4)
s_cs_xx.parametri.parametro_data_1_a[] = ldt_vuoto[]

// carico i codici dei colori
select stringa
into   :ls_str
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
  		 cod_parametro = 'CC1';
if sqlca.sqlcode <> 0 or isnull(ls_str) or len(ls_str) < 1 then
	pb_colore_1.backcolor = 8053814
else
	pb_colore_1.backcolor = long(ls_str)
end if

	
select stringa
into   :ls_str
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
  		 cod_parametro = 'CC2';
if sqlca.sqlcode <> 0 or isnull(ls_str) or len(ls_str) < 1 then
	pb_colore_2.backcolor = 8442861
else
	pb_colore_2.backcolor = long(ls_str)
end if

select stringa
into   :ls_str
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
  		 cod_parametro = 'CC3';
if sqlca.sqlcode <> 0 or isnull(ls_str) or len(ls_str) < 1 then
	pb_colore_3.backcolor = 1149386
else
	pb_colore_3.backcolor = long(ls_str)
end if

select stringa
into   :ls_str
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
  		 cod_parametro = 'CC4';
if sqlca.sqlcode <> 0 or isnull(ls_str) or len(ls_str) < 1 then
	//pb_colore_4.backcolor = 536870912
	pb_colore_4.backcolor = 16777215
else
	pb_colore_4.backcolor = long(ls_str)
end if

//---------------------------------------------------------

move(1,1)

this.width = w_cs_xx_mdi.mdi_1.width - 500
//this.height = w_cs_xx_mdi.mdi_1.height - 40

dw_1.x = this.x
dw_1.y = 312
dw_1.width = this.width

//---------------------------------------------------------


wf_crea_calendario()
end event

type st_4 from statictext within w_cal_produzione_reparti
integer x = 773
integer y = 108
integer width = 4155
integer height = 72
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "In blu la colonna relativa alla data partenza dell~'ordine"
boolean focusrectangle = false
end type

type st_3 from statictext within w_cal_produzione_reparti
integer x = 773
integer y = 192
integer width = 4137
integer height = 72
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Nelle celle in grassetto le ore Potenziali/Impegno dei Reparti produzione coinvolti nella riga ordine"
boolean focusrectangle = false
end type

type st_2 from statictext within w_cal_produzione_reparti
integer x = 773
integer y = 16
integer width = 4105
integer height = 72
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Doppio clic su una qualsiasi cella di una colonna per acquisirne la nuova data partenza"
boolean focusrectangle = false
end type

type st_1 from statictext within w_cal_produzione_reparti
integer x = 37
integer y = 112
integer width = 667
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Potenziale / Impegno"
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type pb_colore_1 from picturebutton within w_cal_produzione_reparti
boolean visible = false
integer x = 3397
integer y = 312
integer width = 402
integer height = 108
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Disponibile"
boolean originalsize = true
alignment htextalign = left!
end type

event clicked;string ls_color

s_cs_xx.parametri.parametro_d_1 = backcolor

window_open(w_scegli_colori, 0)

backcolor = s_cs_xx.parametri.parametro_d_1

delete parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and cod_parametro = 'CC1';

ls_color = string( long(s_cs_xx.parametri.parametro_d_1) )

insert into parametri_azienda
	(cod_azienda,
	 flag_parametro,
	 cod_parametro,
	 stringa)
values
	 (:s_cs_xx.cod_azienda,
	  'S',
	  'CC1',
	  :ls_color);

commit;

end event

type pb_colore_2 from picturebutton within w_cal_produzione_reparti
boolean visible = false
integer x = 3397
integer y = 464
integer width = 402
integer height = 108
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Soglia"
boolean originalsize = true
alignment htextalign = left!
end type

event clicked;string ls_color

s_cs_xx.parametri.parametro_d_1 = backcolor

window_open(w_scegli_colori, 0)

backcolor = s_cs_xx.parametri.parametro_d_1

delete parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and cod_parametro = 'CC2';

ls_color = string( long(s_cs_xx.parametri.parametro_d_1) )

insert into parametri_azienda
	(cod_azienda,
	 flag_parametro,
	 cod_parametro,
	 stringa)
values
	 (:s_cs_xx.cod_azienda,
	  'S',
	  'CC2',
	  :ls_color);

commit;

end event

type pb_colore_3 from picturebutton within w_cal_produzione_reparti
boolean visible = false
integer x = 3401
integer y = 616
integer width = 402
integer height = 108
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Fuori Soglia"
boolean originalsize = true
alignment htextalign = left!
end type

event clicked;string ls_color

s_cs_xx.parametri.parametro_d_1 = backcolor

window_open(w_scegli_colori, 0)

backcolor = s_cs_xx.parametri.parametro_d_1

delete parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and cod_parametro = 'CC3';

ls_color = string( long(s_cs_xx.parametri.parametro_d_1) )

insert into parametri_azienda
	(cod_azienda,
	 flag_parametro,
	 cod_parametro,
	 stringa)
values
	 (:s_cs_xx.cod_azienda,
	  'S',
	  'CC3',
	  :ls_color);

commit;

end event

type pb_colore_4 from picturebutton within w_cal_produzione_reparti
boolean visible = false
integer x = 3397
integer y = 768
integer width = 402
integer height = 108
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sfondo"
boolean originalsize = true
alignment htextalign = left!
end type

event clicked;string ls_color

s_cs_xx.parametri.parametro_d_1 = backcolor

window_open(w_scegli_colori, 0)

backcolor = s_cs_xx.parametri.parametro_d_1

delete parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and cod_parametro = 'CC4';

ls_color = string( long(s_cs_xx.parametri.parametro_d_1) )

insert into parametri_azienda
	(cod_azienda,
	 flag_parametro,
	 cod_parametro,
	 stringa)
values
	 (:s_cs_xx.cod_azienda,
	  'S',
	  'CC4',
	  :ls_color);

commit;

end event

type dw_1 from datawindow within w_cal_produzione_reparti
integer x = 32
integer y = 312
integer width = 4882
integer height = 1324
integer taborder = 10
string title = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;long						ll_num_col, ll_pos, ll_col
datetime					ldt_data
date						ldd_data
string						ls_name, ls_data, ls_domanda
boolean					lb_cambio




if isvalid(dwo) then
	if row>0 then
		ls_name = dwo.name
		ll_pos = pos(ls_name, "_", 1)
		ll_col = long(right(ls_name, len(ls_name) - ll_pos))
		
		try
			ls_data = getitemstring(row, "giorno_"+string(ll_col))
			if isdate(ls_data) then
				ldd_data = date(ls_data)
				ldt_data = datetime(ldd_data, 00:00:00)

			else
				g_mb.show("APICE","Data non riconosciuta: "+ls_data)
				return
			end if
			
		catch (throwable err)
			g_mb.show("APICE: try-catch","Data non riconosciuta")
			return
		end try
		
		//fin qui in ldt_data c'è la data
		if ldt_data = idt_data_partenza then
			lb_cambio = false
			g_mb.show("Data selezionata uguale a quella di partenza attuale")
			return
			
		elseif ldt_data > idt_data_partenza then
			lb_cambio = true
			
		elseif ldt_data < idt_data_partenza then
			lb_cambio = true
			
		else
			lb_cambio = false
		end if
		
		s_cs_xx.parametri.parametro_b_2 = false
		if lb_cambio then
			
			ls_domanda = "Sei sicuro di voler cambiare la data di partenza dell'ordine dal " + string(idt_data_partenza,"dd/MM/yyyy") + &
							" al " + string(ldt_data,"dd/MM/yyyy") + " e ricacolare tutte le date pronto dei reparti per tutti i prodotti dell’ordine?"
			
			if g_mb.confirm("APICE", ls_domanda) then
				
				if g_mb.confirm("APICE", "Sei sicuro di voler procedere?") then
				
					g_mb.show("APICE","Confermato spostamento. Dopo il salvataggio della riga d'ordine verrà applicata la modifica!")
					s_cs_xx.parametri.parametro_b_2 = true
					s_cs_xx.parametri.parametro_data_4 = ldt_data
					close(parent)
				
				else
					s_cs_xx.parametri.parametro_b_2 = false
					setnull(s_cs_xx.parametri.parametro_data_4)
					return
				end if
			else
				s_cs_xx.parametri.parametro_b_2 = false
				setnull(s_cs_xx.parametri.parametro_data_4)
				return
			end if
		end if
		
	end if
end if
end event

