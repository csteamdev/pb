﻿$PBExportHeader$w_cal_produzione_sel_reparti.srw
forward
global type w_cal_produzione_sel_reparti from window
end type
type cb_imposta_pred from commandbutton within w_cal_produzione_sel_reparti
end type
type cb_predefinito from commandbutton within w_cal_produzione_sel_reparti
end type
type cb_ok from commandbutton within w_cal_produzione_sel_reparti
end type
type dw_lista from datawindow within w_cal_produzione_sel_reparti
end type
end forward

global type w_cal_produzione_sel_reparti from window
integer width = 2528
integer height = 1912
boolean titlebar = true
string title = "Seleziona Reparti da Visualizzare nel Calendario"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cb_imposta_pred cb_imposta_pred
cb_predefinito cb_predefinito
cb_ok cb_ok
dw_lista dw_lista
end type
global w_cal_produzione_sel_reparti w_cal_produzione_sel_reparti

forward prototypes
public subroutine wf_memorizza_filtro ()
public subroutine wf_leggi_filtro ()
public subroutine wf_parse_reparti (string fs_stringa, string fs_parser, ref string fs_reparti[])
end prototypes

public subroutine wf_memorizza_filtro ();string ls_cod_reparto, ls_flag_sel, ls_memo, ls_cod_utente
long ll_i, ll_righe, ll_cont


if not g_mb.confirm("APICE","Memorizza l'attuale impostazione dei filtro come predefinito?") then return

ll_righe = dw_lista.rowcount()

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

//carico in ls_memo tutti i codici reparto selezionati, separati da TAB

ls_memo = ""
for ll_i = 1 to ll_righe
	ls_cod_reparto = dw_lista.getitemstring(ll_i, "cod_reparto")
	ls_flag_sel = dw_lista.getitemstring(ll_i, "selezionato")
	
	if ls_flag_sel="S" then
	else
		continue
	end if
	
	ls_memo += ls_cod_reparto + "~t"
next

select count(*)
into   :ll_cont
from   filtri_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'CALEN';
		
if ll_cont > 0 then
	update filtri_manutenzioni
	set filtri = :ls_memo
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente =  :ls_cod_utente and
		   tipo_filtro = 'CALEN';
	if sqlca.sqlcode <> 0 then
		g_mb.error("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
else
	insert into filtri_manutenzioni
		(cod_azienda,
		 cod_utente,
		 filtri,
		 tipo_filtro)
	 values
		 (:s_cs_xx.cod_azienda,
		  :ls_cod_utente,
		  :ls_memo,
		  'CALEN');
	if sqlca.sqlcode <> 0 then
		g_mb.error("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

commit;

return
end subroutine

public subroutine wf_leggi_filtro ();string ls_cod_reparto, ls_cod_reparto2, ls_memo, ls_cod_utente, ls_cod_reparti[], ls_parser
long ll_i, ll_rows, ll_i2, ll_rows2

ll_rows = dw_lista.rowcount()

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""

select filtri 
into   :ls_memo
from   filtri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'CALEN';
		 
if sqlca.sqlcode = 100 then
	//nessu filtro memorizzato: metti tutti a Selezionato (è il valore di default quind fai semplicemente return)
	g_mb.messagebox("OMNIA", "Nessun filtro memorizzato.")
	return
end if
if sqlca.sqlcode <> 0 then
	g_mb.error("OMNIA", "Errore in lettura impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
	return
end if

if trim(ls_memo)="" or isnull(ls_memo) then return

ls_parser = "~t"
wf_parse_reparti(ls_memo, ls_parser, ls_cod_reparti)

ll_rows2 = upperbound(ls_cod_reparti)

for ll_i = 1 to ll_rows
	//de-seleziona tutti
	dw_lista.setitem(ll_i, "selezionato", "N")
next

for ll_i = 1 to ll_rows2
	
	ls_cod_reparto2 = ls_cod_reparti[ll_i]
	
	for ll_i2 = 1 to ll_rows
		ls_cod_reparto = dw_lista.getitemstring(ll_i2, "cod_reparto")
		
		if ls_cod_reparto2 = ls_cod_reparto then
			dw_lista.setitem(ll_i2, "selezionato", "S")
			exit
		end if
	next
next

return
end subroutine

public subroutine wf_parse_reparti (string fs_stringa, string fs_parser, ref string fs_reparti[]);string ls_tmp
long ll_pos, ll_pos2, ll_index

ll_pos = 1
ll_index = 0
do while true
	if len(fs_stringa) > 0 and not isnull(fs_stringa) then
		ll_pos2 = pos(fs_stringa, fs_parser, ll_pos)
		
		ls_tmp = mid(fs_stringa, ll_pos, ll_pos2 - ll_pos)
		
		if isnull(ls_tmp) or trim(ls_tmp)="" then exit
		
		ll_index += 1
		fs_reparti[ll_index] = ls_tmp
		
		ll_pos = ll_pos2 + 1
	end if
loop
end subroutine

on w_cal_produzione_sel_reparti.create
this.cb_imposta_pred=create cb_imposta_pred
this.cb_predefinito=create cb_predefinito
this.cb_ok=create cb_ok
this.dw_lista=create dw_lista
this.Control[]={this.cb_imposta_pred,&
this.cb_predefinito,&
this.cb_ok,&
this.dw_lista}
end on

on w_cal_produzione_sel_reparti.destroy
destroy(this.cb_imposta_pred)
destroy(this.cb_predefinito)
destroy(this.cb_ok)
destroy(this.dw_lista)
end on

event open;long ll_rows, ll_index, ll_index1, ll_tot
string ls_cod_rep_1, ls_cod_rep_2, ls_vuoto[]

dw_lista.settransobject(sqlca)
ll_rows = dw_lista.retrieve(s_cs_xx.cod_azienda)

cb_predefinito.postevent("clicked")

/*
//---------------------------------------------------------------------------------------
ll_tot = upperbound(s_cs_xx.parametri.parametro_s_4_a)

//se vuoto lascia tutto a S
if ll_tot <= 0 then return

//reset
for ll_index = 1 to ll_rows
	dw_lista.setitem(ll_index, "selezionato", "N")
next

for ll_index = 1 to ll_tot
	ls_cod_rep_1 = s_cs_xx.parametri.parametro_s_4_a[ll_index]
	
	for ll_index1 = 1 to ll_rows
		ls_cod_rep_2 = dw_lista.getitemstring(ll_index1, "cod_reparto")
		
		if ls_cod_rep_2 = ls_cod_rep_1 then
			dw_lista.setitem(ll_index1, "selezionato", "S")
			exit
		end if
		
	next
	
next

s_cs_xx.parametri.parametro_s_4_a = ls_vuoto
//---------------------------------------------------------------------------------------
*/
end event

type cb_imposta_pred from commandbutton within w_cal_produzione_sel_reparti
integer x = 2039
integer y = 1700
integer width = 402
integer height = 88
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imp.Predef."
end type

event clicked;dw_lista.accepttext()
wf_memorizza_filtro()
end event

type cb_predefinito from commandbutton within w_cal_produzione_sel_reparti
integer x = 1618
integer y = 1700
integer width = 402
integer height = 88
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Predefinito"
end type

event clicked;wf_leggi_filtro()
end event

type cb_ok from commandbutton within w_cal_produzione_sel_reparti
integer x = 960
integer y = 1700
integer width = 402
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;string ls_temp, ls_sel, ls_vuoto[]
long ll_tot, ll_index, ll_count

dw_lista.accepttext()
ll_tot = dw_lista.rowcount()
ll_count = 0
s_cs_xx.parametri.parametro_s_4_a[] = ls_vuoto[]

for ll_index = 1 to ll_tot
	ls_sel = dw_lista.getitemstring(ll_index, "selezionato")
	
	if ls_sel="S" then
		ll_count += 1
		ls_temp = dw_lista.getitemstring(ll_index, "cod_reparto")
		
		s_cs_xx.parametri.parametro_s_4_a[ll_count] = ls_temp
	end if
	
next

if upperbound(s_cs_xx.parametri.parametro_s_4_a[]) > 0 then
	close(parent)
else
	g_mb.error("Selezionare almeno un reparto da visualizzare sul calendario!")
	return
end if
end event

type dw_lista from datawindow within w_cal_produzione_sel_reparti
integer x = 37
integer y = 32
integer width = 2427
integer height = 1636
integer taborder = 10
string title = "none"
string dataobject = "d_sel_reparti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event buttonclicked;string			ls_azione, ls_testo
long			ll_index

choose case dwo.name
	case "b_sel"
		
		if dwo.text = "Tutti" then
			//seleziona tutti
			ls_azione = "S"
			ls_testo = "Nessuno"
		else
			//deseleziona tutti
			ls_azione = "N"
			ls_testo = "Tutti"
		end if
		
		for ll_index=1 to rowcount()
			setitem(ll_index, "selezionato", ls_azione)
		next
	
		dwo.text = ls_testo
		
		
end choose
end event

