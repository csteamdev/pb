﻿$PBExportHeader$w_carica_cal_produzione.srw
forward
global type w_carica_cal_produzione from w_cs_xx_risposta
end type
type st_log from statictext within w_carica_cal_produzione
end type
type dw_reparti from datawindow within w_carica_cal_produzione
end type
type cb_annulla from commandbutton within w_carica_cal_produzione
end type
type cb_carica from commandbutton within w_carica_cal_produzione
end type
type dw_carica_cal_produzione from uo_cs_xx_dw within w_carica_cal_produzione
end type
end forward

global type w_carica_cal_produzione from w_cs_xx_risposta
integer width = 2263
integer height = 2188
string title = "Creazione Calendario Produzione"
st_log st_log
dw_reparti dw_reparti
cb_annulla cb_annulla
cb_carica cb_carica
dw_carica_cal_produzione dw_carica_cal_produzione
end type
global w_carica_cal_produzione w_carica_cal_produzione

forward prototypes
public function integer wf_carica_reparto (string fs_cod_reparto, string fs_flag_sabato, string fs_flag_domenica, datetime fdt_data_inizio, datetime fdt_data_fine, long fl_ore_potenziali, ref string fs_errore)
end prototypes

public function integer wf_carica_reparto (string fs_cod_reparto, string fs_flag_sabato, string fs_flag_domenica, datetime fdt_data_inizio, datetime fdt_data_fine, long fl_ore_potenziali, ref string fs_errore);date		ldd_data
datetime ldt_data
long 		ll_ret, ll_minuti, ll_cont

ldd_data = date(fdt_data_inizio)

do while true
	
	ll_minuti = fl_ore_potenziali * 60
	
	// Domenica
	if fs_flag_domenica = "S" and daynumber(ldd_data) = 1 then 
		ll_minuti = 0
	end if
	
	// Sabato
	if fs_flag_sabato   = "S" and daynumber(ldd_data) = 7 then 
		ll_minuti = 0
	end if
	
	ldt_data = datetime(ldd_data, 00:00:00)
	
	if ldt_data > fdt_data_fine then exit
	
	ll_cont = 0
	
	select count(*)
	into   :ll_cont
	from   tab_cal_produzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_reparto = :fs_cod_reparto and
			 data_giorno = :ldt_data;
			 
	if ll_cont > 0 and not isnull(ll_cont) then
		
		update tab_cal_produzione
		set    minuti_reparto = :ll_minuti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_reparto = :fs_cod_reparto and
				 data_giorno = :ldt_data;
	
	else
	
		insert into tab_cal_produzione
				(cod_azienda,
				 cod_reparto,
				 data_giorno,
				 minuti_reparto,
				 minuti_effettivi,
				 minuti_giro_rc)
		values
				(:s_cs_xx.cod_azienda,
				 :fs_cod_reparto,
				 :ldt_data,
				 :ll_minuti,
				 0,
				 0);
	end if
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in creazione calendario per reparto "+fs_cod_reparto+&
						", giorno "+string(ldt_data,"dd/mm/yyyy")+"~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	ldd_data = date(ldt_data)
	ldd_data = relativedate(ldd_data, 1)
	
loop

return 1

end function

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_noenablepopup)

dw_carica_cal_produzione.change_dw_current()

dw_carica_cal_produzione.set_dw_options(sqlca, &
													 pcca.null_object, &
													 c_newonopen + &
													 c_nomodify + &
													 c_nodelete + &
													 c_noretrieveonopen, &
													 c_nohighlightselected + &
													 c_nocursorrowfocusrect)
													 
dw_carica_cal_produzione.resetupdate()


dw_reparti.settransobject(sqlca)

end event

on w_carica_cal_produzione.create
int iCurrent
call super::create
this.st_log=create st_log
this.dw_reparti=create dw_reparti
this.cb_annulla=create cb_annulla
this.cb_carica=create cb_carica
this.dw_carica_cal_produzione=create dw_carica_cal_produzione
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_log
this.Control[iCurrent+2]=this.dw_reparti
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.cb_carica
this.Control[iCurrent+5]=this.dw_carica_cal_produzione
end on

on w_carica_cal_produzione.destroy
call super::destroy
destroy(this.st_log)
destroy(this.dw_reparti)
destroy(this.cb_annulla)
destroy(this.cb_carica)
destroy(this.dw_carica_cal_produzione)
end on

event pc_setddlb;call super::pc_setddlb;
string ls_where_depositi


ls_where_depositi = 	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
							"((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
							"isnumeric(cod_deposito)=1 and "+&
							"right('000' + cod_deposito, 2) = '00'"
//ls_where_depositi = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))"


f_po_loaddddw_dw(		dw_carica_cal_produzione, &
								"cod_deposito", &
								sqlca, &
								"anag_depositi", &
								"cod_deposito", &
								"des_deposito", &
								ls_where_depositi)
end event

type st_log from statictext within w_carica_cal_produzione
integer x = 46
integer y = 1988
integer width = 2149
integer height = 56
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Pronto!"
boolean focusrectangle = false
end type

type dw_reparti from datawindow within w_carica_cal_produzione
integer x = 32
integer y = 632
integer width = 2171
integer height = 1324
integer taborder = 20
string title = "none"
string dataobject = "d_carica_cal_produzione_reparti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = styleraised!
end type

event buttonclicked;string			ls_azione, ls_testo
long			ll_index

choose case dwo.name
	case "b_sel"
		
		if dwo.text = "Tutti" then
			//seleziona tutti
			ls_azione = "S"
			ls_testo = "Nessuno"
		else
			//deseleziona tutti
			ls_azione = "N"
			ls_testo = "Tutti"
		end if
		
		for ll_index=1 to rowcount()
			setitem(ll_index, "selezionato", ls_azione)
		next
	
		dwo.text = ls_testo
		
		
end choose
end event

type cb_annulla from commandbutton within w_carica_cal_produzione
integer x = 695
integer y = 512
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;close(parent)
end event

type cb_carica from commandbutton within w_carica_cal_produzione
integer x = 1106
integer y = 512
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Carica"
end type

event clicked;string 	ls_cod_reparto, ls_flag_sabato, ls_flag_domenica, ls_errore
date		ldd_data
datetime ldt_data_inizio, ldt_data_fine, ldt_data
long 		ll_ore_potenziali, ll_ret, ll_minuti, ll_cont


dw_carica_cal_produzione.accepttext()

//ls_cod_reparto 	= dw_carica_cal_produzione.getitemstring(1, "cod_reparto")

//se si allora devo escludere il sab e/o la domenica dalla pianiFICAzione
ls_flag_sabato 	= dw_carica_cal_produzione.getitemstring(1, "flag_sabato")
ls_flag_domenica 	= dw_carica_cal_produzione.getitemstring(1, "flag_domenica")

ldt_data_inizio 	= dw_carica_cal_produzione.getitemdatetime(1, "data_inizio")
ldt_data_fine 		= dw_carica_cal_produzione.getitemdatetime(1, "data_fine")
ll_ore_potenziali = dw_carica_cal_produzione.getitemnumber(1, "ore_potenziale")

if isnull(ldt_data_inizio) then
	g_mb.warning("SEP", "Impostare una data di inizio.")
	return
end if

if isnull(ldt_data_fine) then
	g_mb.warning("SEP", "Impostare una data di fine.")
	return
end if

if ldt_data_inizio > ldt_data_fine then
	g_mb.warning("SEP", "La data inizio non può essere maggiore della data fine.")
	return
end if

if isnull(ll_ore_potenziali) or ll_ore_potenziali<0 then ll_ore_potenziali = 0
if ll_ore_potenziali = 0 then
	if not g_mb.confirm("SEP", "Attenzione. Hai specificato ore potenziali pari a zero. Sicuro di procedere?") then
		return
	end if
end if

if dw_reparti.rowcount() = 0 then
	g_mb.warning("SEP", "Non ci sono reparti in lista.")
	return
end if

if g_mb.confirm("SEP", "Procedo con la generazione del calendario, "+&
							"SOVRASCRIVENDO eventuali impostazioni già effettuate PRECEDENTEMENTE ?") then
else
	g_mb.warning("Operazione annullata dall'utente!")
	return
end if


setpointer(Hourglass!)

for ll_cont=1 to dw_reparti.rowcount()
	if dw_reparti.getitemstring(ll_cont, "selezionato") = "S" then
		Yield()
		ls_cod_reparto = dw_reparti.getitemstring(ll_cont, "cod_reparto")
		
		st_log.text = "Elaborazione Ore potenziali reparto " + ls_cod_reparto + " in corso ..."
		
		if wf_carica_reparto(ls_cod_reparto, ls_flag_sabato, ls_flag_domenica, ldt_data_inizio, ldt_data_fine, ll_ore_potenziali, ls_errore) < 0 then
			rollback;
			st_log.text = "terminato con errori!"
			setpointer(Arrow!)
			g_mb.error(ls_errore)
			return
			
		else
			//commit singolo reparto, per non appesantire
			dw_reparti.setitem(ll_cont, "elaborato", "S")
			commit;
		end if
		
	end if
next

setpointer(Arrow!)

st_log.text = "Pronto!"
g_mb.success("Operazione effettuata con successo!")

//close(parent)

end event

type dw_carica_cal_produzione from uo_cs_xx_dw within w_carica_cal_produzione
integer x = 32
integer width = 2171
integer height = 492
integer taborder = 10
string dataobject = "d_carica_cal_produzione"
boolean border = false
end type

event itemchanged;call super::itemchanged;if row>0 then
else
	return
end if

choose case dwo.name
	case "cod_deposito"
		if data <>"" then
			dw_reparti.retrieve(s_cs_xx.cod_azienda, data)
		else
			dw_reparti.reset()
		end if
		
		dw_reparti.object.b_sel.text = "Tutti"
		
end choose
end event

