﻿$PBExportHeader$w_chiudi_prod_tes.srw
forward
global type w_chiudi_prod_tes from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_chiudi_prod_tes
end type
type cb_ok from commandbutton within w_chiudi_prod_tes
end type
type dw_chiudi_tes from datawindow within w_chiudi_prod_tes
end type
end forward

global type w_chiudi_prod_tes from w_cs_xx_risposta
integer width = 2171
integer height = 352
string title = "Dati Chiusura Produzione"
boolean controlmenu = false
cb_annulla cb_annulla
cb_ok cb_ok
dw_chiudi_tes dw_chiudi_tes
end type
global w_chiudi_prod_tes w_chiudi_prod_tes

type variables
boolean ib_chiudi = false
end variables

event closequery;call super::closequery;if not ib_chiudi then
	return 1
end if
end event

on w_chiudi_prod_tes.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.dw_chiudi_tes=create dw_chiudi_tes
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_ok
this.Control[iCurrent+3]=this.dw_chiudi_tes
end on

on w_chiudi_prod_tes.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.dw_chiudi_tes)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_chiudi_tes,"cod_operaio",sqlca,"anag_operai","cod_operaio", &
					  "cognome + ' ' + nome","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_setwindow;call super::pc_setwindow;dw_chiudi_tes.insertrow(0)

dw_chiudi_tes.setitem(1,"data_fine",datetime(today(),00:00:00))

end event

type cb_annulla from commandbutton within w_chiudi_prod_tes
integer x = 1737
integer y = 140
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;s_cs_xx.parametri.parametro_d_1 = -1

ib_chiudi = true

close(parent)
end event

type cb_ok from commandbutton within w_chiudi_prod_tes
integer x = 1737
integer y = 20
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "OK"
end type

event clicked;s_cs_xx.parametri.parametro_d_2 = dw_chiudi_tes.getitemnumber(1,"num_colli")

s_cs_xx.parametri.parametro_data_1 = dw_chiudi_tes.getitemdatetime(1,"data_fine")

s_cs_xx.parametri.parametro_s_1 = dw_chiudi_tes.getitemstring(1,"cod_operaio")

s_cs_xx.parametri.parametro_d_1 = 0

ib_chiudi = true

close(parent)
end event

type dw_chiudi_tes from datawindow within w_chiudi_prod_tes
event ue_key pbm_dwnkey
integer x = 23
integer y = 20
integer width = 1691
integer height = 200
integer taborder = 10
string dataobject = "d_chiudi_prod_tes"
boolean livescroll = true
end type

event ue_key;choose case key
	case keyenter!
		cb_ok.triggerevent("clicked")
	case keyescape!
		cb_annulla.triggerevent("clicked")
end choose
end event

event itemfocuschanged;selecttext(1,len(gettext()))
end event

event getfocus;selecttext(1,len(gettext()))
end event

