﻿$PBExportHeader$w_des_tecniche.srw
$PBExportComments$Window per descrizioni ordini tipo tende tecniche
forward
global type w_des_tecniche from w_cs_xx_principale
end type
type dw_tab_des_tecniche from uo_cs_xx_dw within w_des_tecniche
end type
end forward

global type w_des_tecniche from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2597
integer height = 1500
string title = "Descrizioni per Tende Tecniche"
dw_tab_des_tecniche dw_tab_des_tecniche
end type
global w_des_tecniche w_des_tecniche

on w_des_tecniche.create
int iCurrent
call super::create
this.dw_tab_des_tecniche=create dw_tab_des_tecniche
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_des_tecniche
end on

on w_des_tecniche.destroy
call super::destroy
destroy(this.dw_tab_des_tecniche)
end on

event pc_setwindow;call super::pc_setwindow;dw_tab_des_tecniche.set_dw_key("cod_azienda")
dw_tab_des_tecniche.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_default, &
                         c_default)
end event

type dw_tab_des_tecniche from uo_cs_xx_dw within w_des_tecniche
integer x = 23
integer y = 20
integer width = 2514
integer height = 1360
integer taborder = 10
string dataobject = "d_tab_des_tecniche"
boolean vscrollbar = true
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to rowcount()
   if isnull(getitemstring(ll_i, "cod_azienda")) then
      setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

