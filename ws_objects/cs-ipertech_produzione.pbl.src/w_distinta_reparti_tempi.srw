﻿$PBExportHeader$w_distinta_reparti_tempi.srw
forward
global type w_distinta_reparti_tempi from w_cs_xx_principale
end type
type cb_importa_nuovi from commandbutton within w_distinta_reparti_tempi
end type
type cb_importa from commandbutton within w_distinta_reparti_tempi
end type
type cb_esporta from commandbutton within w_distinta_reparti_tempi
end type
type dw_distinta_reparti_tempi_lista from uo_cs_xx_dw within w_distinta_reparti_tempi
end type
type dw_folder from u_folder within w_distinta_reparti_tempi
end type
type dw_ricerca from u_dw_search within w_distinta_reparti_tempi
end type
end forward

global type w_distinta_reparti_tempi from w_cs_xx_principale
integer width = 3232
integer height = 2040
string title = "Tempi Distinta - Reparti"
event ue_ridimensiona ( )
event ue_posiziona_window ( )
cb_importa_nuovi cb_importa_nuovi
cb_importa cb_importa
cb_esporta cb_esporta
dw_distinta_reparti_tempi_lista dw_distinta_reparti_tempi_lista
dw_folder dw_folder
dw_ricerca dw_ricerca
end type
global w_distinta_reparti_tempi w_distinta_reparti_tempi

type variables
string			is_sql_base
end variables

event ue_ridimensiona();// Sposta gli oggetti all'interno della finestra in modo relativo alla sua dimensione


dw_folder .x = this.x
dw_folder.y = this.y
dw_folder.width = this.width
dw_folder.height = this.height

dw_distinta_reparti_tempi_lista.move(20,110)
dw_distinta_reparti_tempi_lista.height 	= this.height - 400
dw_distinta_reparti_tempi_lista.width 	= this.width - 100

dw_ricerca.x = dw_distinta_reparti_tempi_lista.x
dw_ricerca.y = dw_distinta_reparti_tempi_lista.y
dw_ricerca.width = 2900
dw_ricerca.height = 1280

cb_esporta.x = 500
cb_esporta.y = dw_distinta_reparti_tempi_lista.y + dw_distinta_reparti_tempi_lista.height + 20
cb_esporta.width = 389
cb_esporta.height = 80

cb_importa.x = cb_esporta.x + cb_esporta.width + 20
cb_importa.y = cb_esporta.y
cb_importa.width = cb_esporta.width
cb_importa.height = cb_esporta.height

cb_importa_nuovi.x = cb_importa.x + cb_importa.width + 20
cb_importa_nuovi.y = cb_esporta.y
cb_importa_nuovi.width = cb_esporta.width
cb_importa_nuovi.height = cb_esporta.height
end event

event ue_posiziona_window();move(1,1)

this.width = w_cs_xx_mdi.mdi_1.width - 60
this.height = w_cs_xx_mdi.mdi_1.height - 40

end event

on w_distinta_reparti_tempi.create
int iCurrent
call super::create
this.cb_importa_nuovi=create cb_importa_nuovi
this.cb_importa=create cb_importa
this.cb_esporta=create cb_esporta
this.dw_distinta_reparti_tempi_lista=create dw_distinta_reparti_tempi_lista
this.dw_folder=create dw_folder
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_importa_nuovi
this.Control[iCurrent+2]=this.cb_importa
this.Control[iCurrent+3]=this.cb_esporta
this.Control[iCurrent+4]=this.dw_distinta_reparti_tempi_lista
this.Control[iCurrent+5]=this.dw_folder
this.Control[iCurrent+6]=this.dw_ricerca
end on

on w_distinta_reparti_tempi.destroy
call super::destroy
destroy(this.cb_importa_nuovi)
destroy(this.cb_importa)
destroy(this.cb_esporta)
destroy(this.dw_distinta_reparti_tempi_lista)
destroy(this.dw_folder)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;windowobject					lw_oggetti[], lw_vuoto[]


set_w_options(c_NoResizeWin)




lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_ricerca
dw_folder.fu_AssignTab(1, "Ricerca", lw_oggetti[])

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_distinta_reparti_tempi_lista
lw_oggetti[2] = cb_esporta
lw_oggetti[3] = cb_importa
lw_oggetti[4] = cb_importa_nuovi
dw_folder.fu_AssignTab(2, "Dati", lw_oggetti[])

dw_folder.fu_FolderCreate(2,2)
dw_folder.fu_SelectTab(1)




iuo_dw_main = dw_distinta_reparti_tempi_lista

dw_distinta_reparti_tempi_lista.set_dw_key("cod_azienda")

dw_distinta_reparti_tempi_lista.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_noretrieveonopen, &
                                    c_default + &
												c_nohighlightselected + &
												c_ViewModeBorderUnchanged + &
												c_NoCursorRowFocusRect)
												
												
event post ue_posiziona_window()

event post ue_ridimensiona()

is_sql_base = dw_distinta_reparti_tempi_lista.getsqlselect()

end event

event pc_setddlb;call super::pc_setddlb;
string ls_where_depositi


ls_where_depositi = 	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
							"((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
							"isnumeric(cod_deposito)=1 and "+&
							"right('000' + cod_deposito, 2) = '00'"



f_po_loaddddw_dw(dw_distinta_reparti_tempi_lista, &
                 "cod_prodotto_distinta", &
                 sqlca, &
                 "anag_prodotti", &
                 "cod_prodotto", &
                 "des_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  "cod_prodotto IN " + &
					  " (select distinct cod_prodotto from distinta_padri where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N' ) "  )


f_po_loaddddw_dw(dw_distinta_reparti_tempi_lista, &
					  "cod_reparto", &
					  sqlca, &
					  "anag_reparti", &
					  "cod_reparto", &
					  "des_reparto", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco= 'N' ")


f_po_loaddddw_dw(dw_distinta_reparti_tempi_lista, &
					  "cod_gruppo_variante", &
					  sqlca, &
					  "gruppi_varianti", &
					  "cod_gruppo_variante", &
					  "des_gruppo_variante", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

//-----------------------------------------------------------------------------
f_po_loaddddw_dw(dw_ricerca, &
					  "cod_reparto", &
					  sqlca, &
					  "anag_reparti", &
					  "cod_reparto", &
					  "des_reparto", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_ricerca, &
					  "cod_deposito", &
					  sqlca, &
					  "anag_depositi", &
					  "cod_deposito", &
					  "des_deposito", &
					 ls_where_depositi)

end event

event resize;//event post ue_ridimensiona()
end event

type cb_importa_nuovi from commandbutton within w_distinta_reparti_tempi
integer x = 1285
integer y = 1840
integer width = 494
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Importa Nuovi"
end type

event clicked;window_open(w_distinta_reparti_tempi_nuovi, 0)

end event

type cb_importa from commandbutton within w_distinta_reparti_tempi
integer x = 434
integer y = 1840
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Importa"
end type

event clicked;string	ls_path, ls_filename, ls_str,ls_cod_prodotto_distinta, ls_cod_versione, ls_cod_reparto, &
			ls_cod_prodotto_raggruppato,ls_cod_gruppo_variante

long		ll_ret, ll_file,ll_i, ll_progressivo

dec{4}	ld_tempo_produzione

datastore lds_import

ll_ret = GetFileOpenName("Select File", ls_path, ls_filename, "TXT",  "Text Files (*.TXT),*.TXT, All Files (*.*), *.*")

if ll_ret < 1 then return

//ll_file = fileopen(ls_path, LineMode!)
//
//if ll_file < 1 then return

lds_import = CREATE datastore
lds_import.dataobject = "d_distinta_reparti_tempi_import"


ll_ret = lds_import.importfile( text!, ls_filename)

choose case ll_ret
		
	case is > 0 
		if g_mb.messagebox("APICE","Trovate " + string(ll_ret) + " righe da importare; tutti i dati della tabelle saranno sostituiti con i dati del file prescelto: proseguo?",Question!,YesNo!,2) = 2 then 
			return
		end if
	
	case 0
		g_mb.messagebox("APICE","Il file non contiene nessun dato da importare") 
		return
		
	case -1
		g_mb.messagebox("APICE","No rows or startrow value supplied is greater than the number of rows in the file	") 
		return
		
	case -2
		g_mb.messagebox("APICE","Empty file		") 
		return
		
	case -4
		g_mb.messagebox("APICE","Invalid input		") 
		return
		
	case -5
		g_mb.messagebox("APICE","Could not open the file") 
		return
		
	case -6
		g_mb.messagebox("APICE","Could not close the file") 
		return
		
	case -7
		g_mb.messagebox("APICE","Error reading the text		") 
		return
		
	case -8
		g_mb.messagebox("APICE","Unsupported file name suffix (must be *.txt, *.csv, *.dbf or *.xml)") 
		return
	case -13
		g_mb.messagebox("APICE","Unsupported DataWindow style for import") 
		return
end choose

// SE SONO ARRIVATO QUI VOL DIRE CHE L'IMPORTAZIONE DEL FILE NELLA DW E' ANDATO A BUON FINE	

delete tab_distinta_reparti_tempi;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in cancellazione dati dalla tabella tab_distinta_reparti_tempi.~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

lds_import.setsort("cod_prodotto A, cod_versione A, cod_reparto A")
lds_import.sort()

for ll_i = 1 to lds_import.rowcount()
	
	ls_cod_prodotto_distinta 		= lds_import.getitemstring(ll_i, "cod_prodotto")
	ls_cod_versione 					= lds_import.getitemstring(ll_i, "cod_versione")
	ls_cod_reparto 					= lds_import.getitemstring(ll_i, "cod_reparto")
	ls_cod_prodotto_raggruppato 	= lds_import.getitemstring(ll_i, "cod_prodotto_raggr")
	ls_cod_gruppo_variante 			= lds_import.getitemstring(ll_i, "cod_gruppo_var")
	ld_tempo_produzione 				= dec( lds_import.getitemstring(ll_i, "tempo_produzione") )
	
	// potrebbe essere una riga vuota
	if isnull(ls_cod_prodotto_distinta) or len(ls_cod_prodotto_distinta) < 1 then
		continue
	end if
	
	select max(progressivo)
	into   :ll_progressivo
	from   tab_distinta_reparti_tempi
	where	 cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto_distinta = :ls_cod_prodotto_distinta and
			 cod_versione = :ls_cod_versione;
			 
	if isnull(ll_progressivo) or ll_progressivo < 1 then
		ll_progressivo = 1
	else
		ll_progressivo ++
	end if
	
	insert into tab_distinta_reparti_tempi  
			( cod_azienda,   
			  cod_prodotto_distinta,   
			  cod_versione,   
			  cod_reparto,   
			  progressivo,   
			  cod_prodotto_raggruppato,   
			  cod_gruppo_variante,   
			  tempo_produzione )  
	values ( :s_cs_xx.cod_azienda,   
			  :ls_cod_prodotto_distinta,   
			  :ls_cod_versione,   
			  :ls_cod_reparto,   
			  :ll_progressivo,   
			  :ls_cod_prodotto_raggruppato,   
			  :ls_cod_gruppo_variante,   
			  :ld_tempo_produzione )  ;
			  
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in fase di caricamento dati in tabella tab_distinta_reparti_tempi.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
next

commit;

g_mb.messagebox("APICE","Caricamento dati eseguito con Successo!") 
end event

type cb_esporta from commandbutton within w_distinta_reparti_tempi
integer x = 23
integer y = 1840
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esporta"
end type

event clicked;string 	ls_str, ls_cod_prodotto, ls_descrizione,ls_cod_misura_mag, ls_cod_versione, ls_cod_reparto, ls_cod_gruppo_var, &
			ls_path, ls_file, ls_flag_predefinita

long		ll_i, ll_file, ll_ret

dec{4} 	ld_tempo_prod

ls_path = ""
ll_file = GetFileSaveName("Select File", ls_path, ls_file, "TXT", "File di testo (*.txt),*.txt" )

if ll_file < 1 then return

ll_file = fileopen(ls_path, LineMode!, Write! ,LockWrite!, Replace!)

if ll_file < 1 then return


for ll_i = 1 to dw_distinta_reparti_tempi_lista.rowcount()
	
	ls_str = ""

	// riga di intestazione
	if ll_i = 1 then
		ls_str += "CODICE PRODOTTO" + "~t"
		ls_str += "DESCRIZIONE MAGAZZINO" + "~t"
		ls_str += "UM MAG" + "~t"
		ls_str += "VERS DB" + "~t"
		ls_str += "DESCRIZIONE VERSIONE" + "~t"
		ls_str += "REPARTO" + "~t"
		ls_str += "DESCRIZIONE REPARTO" + "~t"
		ls_str += "PRODOTTO RAGGR" + "~t"
		ls_str += "DESCRIZIONE PRODOTTO RAGGR" + "~t"
		ls_str += "GRUPPO VARIANTE" + "~t"
		ls_str += "DESCRIZIONE GRUPPO VARIANTE" + "~t"
		ls_str += "TEMPO PRODUZIONE (MINUTI)" + "~t"
		
		ll_ret = filewrite(ll_file, ls_str)
		
		if ll_ret < 0 then
			g_mb.messagebox("APICE","Errore in scrittura sul file " + ls_path)
			fileclose(ll_file)
			return
		end if
		
		ls_str = ""
	end if
	
	ls_cod_prodotto = dw_distinta_reparti_tempi_lista.getitemstring(ll_i, "cod_prodotto_distinta")
	
	select des_prodotto, cod_misura_mag
	into   :ls_descrizione, :ls_cod_misura_mag
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if sqlca.sqlcode <> 0 then
		ls_descrizione = "PRODOTTO INESISTENTE"
	end if
	
	ls_str 	+=	ls_cod_prodotto + "~t"
	ls_str 	+= ls_descrizione + "~t"
	ls_str 	+= ls_cod_misura_mag + "~t"
	
	// versione distinta base
	ls_cod_versione = dw_distinta_reparti_tempi_lista.getitemstring(ll_i, "cod_versione")
	ls_str 	+= ls_cod_versione + "~t"
	
	select des_versione, flag_predefinita
	into   :ls_descrizione, :ls_flag_predefinita
	from   distinta_padri
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto= :ls_cod_prodotto and
			 cod_versione= :ls_cod_versione;
	if sqlca.sqlcode <> 0 then
		ls_descrizione = "VERSIONE INESISTENTE"
	end if
	
	if isnull(ls_flag_predefinita) then ls_flag_predefinita = "N"
	if ls_flag_predefinita = "S" then
		ls_descrizione += "-P*"
	end if
	
	ls_str += ls_descrizione + "~t"
		
	
	// reparto
	ls_cod_reparto = dw_distinta_reparti_tempi_lista.getitemstring(ll_i, "cod_reparto")

	select des_reparto
	into   :ls_descrizione
	from   anag_reparti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_reparto = :ls_cod_reparto;
			 
	if sqlca.sqlcode <> 0 then
		ls_descrizione = "REPARTO INESISTENTE"
	end if
	
	ls_str 	+=	ls_cod_reparto + "~t"
	ls_str 	+= ls_descrizione + "~t"
	
	// prodotto raggruppato
	ls_cod_prodotto = dw_distinta_reparti_tempi_lista.getitemstring(ll_i, "cod_prodotto_raggruppato")
	if isnull(ls_cod_prodotto) or len(ls_cod_prodotto) < 1 then
		ls_str 	+=	"~t"
		ls_str 	+= "~t"
	else
		select des_prodotto
		into   :ls_descrizione
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if sqlca.sqlcode <> 0 then
			ls_descrizione = "PRODOTTO INESISTENTE"
		end if
		
		ls_str 	+=	ls_cod_prodotto + "~t"
		ls_str 	+= ls_descrizione + "~t"
	end if
	
	// gruppo variante
	ls_cod_gruppo_var = dw_distinta_reparti_tempi_lista.getitemstring(ll_i, "cod_gruppo_variante")
	
	if isnull(ls_cod_gruppo_var) then
		ls_str 	+=	"~t"
		ls_str 	+= "~t"
	else
		select des_gruppo_variante
		into   :ls_descrizione
		from   gruppi_varianti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_gruppo_variante = :ls_cod_gruppo_var;
				 
		if sqlca.sqlcode <> 0 then
			ls_descrizione = "GRUPPO VARIANTE INESISTENTE"
		end if
		
		ls_str 	+=	ls_cod_gruppo_var + "~t"
		ls_str 	+= ls_descrizione + "~t"
	end if
	
	// cerco unità di misura trovando il ramo che ha quel gruppo variante.
	
	
	// scrivo minuti 
	ld_tempo_prod = dw_distinta_reparti_tempi_lista.getitemnumber(ll_i, "tempo_produzione")
	
	ls_str 	+= string(ld_tempo_prod, "###,##0.00") + "~t"
	
	ll_ret = filewrite(ll_file, ls_str)
	
	if ll_ret < 0 then
		g_mb.messagebox("APICE","Errore in scrittura sul file " + ls_path)
		fileclose(ll_file)
		return
	end if

next

fileclose(ll_file)

end event

type dw_distinta_reparti_tempi_lista from uo_cs_xx_dw within w_distinta_reparti_tempi
integer x = 18
integer y = 112
integer width = 3154
integer height = 1704
integer taborder = 10
string dataobject = "d_distinta_reparti_tempi_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event itemchanged;call super::itemchanged;if isvalid(dwo) then
	
	string ls_null
	
	choose case i_colname
			
		case "cod_prodotto_distinta"
			
			if isnull(i_coltext) or len(trim(i_coltext)) < 1 then
				
				setnull(ls_null)
				setitem(row, "cod_versione", ls_null)
				
				f_po_loaddddw_dw(dw_distinta_reparti_tempi_lista, &
									  "cod_versione", &
									  sqlca, &
									  "distinta_padri", &
									  "cod_versione", &
									  "des_versione", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '' ")
				
			else
				
				setnull(ls_null)
				setitem(row, "cod_versione", ls_null)
				
				f_po_loaddddw_dw(dw_distinta_reparti_tempi_lista, &
									  "cod_versione", &
									  sqlca, &
									  "distinta_padri", &
									  "cod_versione", &
									  "des_versione", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + i_coltext + "' ")
				
			end if
	end choose
end if
				
end event

event rowfocuschanged;call super::rowfocuschanged;if i_rownbr > 0 then
	
	string ls_cod_prodotto
	
	ls_cod_prodotto = getitemstring(i_rownbr, "cod_prodotto_distinta")
	
	if isnull(i_coltext) or len(trim(i_coltext)) < 1 then
		
		f_po_loaddddw_dw(dw_distinta_reparti_tempi_lista, &
							  "cod_versione", &
							  sqlca, &
							  "distinta_padri", &
							  "cod_versione", &
							  "des_versione", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '' ")
		
	else
		
		f_po_loaddddw_dw(dw_distinta_reparti_tempi_lista, &
							  "cod_versione", &
							  sqlca, &
							  "distinta_padri", &
							  "cod_versione", &
							  "des_versione", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + i_coltext + "' ")
		
	end if
end if
end event

event pcd_setkey;call super::pcd_setkey;long 		ll_i, ll_max
string	ls_cod_prodotto, ls_cod_versione

for ll_i = 1 to this.rowcount()
	
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
	if isnull(this.getitemnumber(ll_i, "progressivo")) or this.getitemnumber(ll_i, "progressivo") < 1 then

		ls_cod_prodotto = getitemstring(ll_i, "cod_prodotto_distinta")
		ls_cod_versione = getitemstring(ll_i, "cod_versione")

		select max(progressivo)
		into   :ll_max
		from   tab_distinta_reparti_tempi
		where	 cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto_distinta = :ls_cod_prodotto and
				 cod_versione = :ls_cod_versione;
				 
		if isnull(ll_max) or ll_max < 1 then
			ll_max = 1
		else
			ll_max ++
		end if
		
		setitem(ll_i, "progressivo", ll_max)
		
	end if
	
next

end event

event pcd_retrieve;call super::pcd_retrieve;string				ls_sql, ls_cod_prodotto, ls_cod_reparto, ls_cod_deposito
long				ll_count

dw_ricerca.accepttext()

ls_sql = is_sql_base
ls_sql += " where cod_azienda='"+s_cs_xx.cod_azienda+"' "

ls_cod_prodotto	= dw_ricerca.getitemstring(1, "cod_prodotto")
ls_cod_reparto		= dw_ricerca.getitemstring(1, "cod_reparto")


if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
	ls_sql += " and cod_prodotto_distinta='"+ls_cod_prodotto+"' "
end if

if ls_cod_reparto<>"" and not isnull(ls_cod_reparto) then
	//se c'è il reparto, prevale sul filtro per deposito
	ls_sql += " and cod_reparto='"+ls_cod_reparto+"' "
	
else
	//verifica se hai impstato il filtro per deposito
	ls_cod_deposito	= dw_ricerca.getitemstring(1, "cod_deposito")
	
	if ls_cod_deposito<>"" and not isnull(ls_cod_deposito) then
		ls_sql += " and cod_reparto in (select cod_reparto "+&
												"from anag_reparti "+&
												"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
														 "cod_deposito='"+ls_cod_deposito+"') "
	end if
	
end if

dw_distinta_reparti_tempi_lista.setsqlselect(ls_sql)

ll_count = dw_distinta_reparti_tempi_lista.retrieve()

if ll_count < 0 then
   pcca.error = c_fatal
else
	dw_folder.fu_SelectTab(2)
end if

end event

type dw_folder from u_folder within w_distinta_reparti_tempi
integer width = 3191
integer height = 1936
integer taborder = 20
end type

type dw_ricerca from u_dw_search within w_distinta_reparti_tempi
integer x = 18
integer y = 112
integer width = 2898
integer height = 1280
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_distinta_reparti_tempi_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"rs_cod_prodotto")
		
		
	case "b_cerca"
		dw_distinta_reparti_tempi_lista.change_dw_current( )
		parent.postevent("pc_retrieve")
	
end choose
end event

event itemchanged;call super::itemchanged;string			ls_null

if row>0 then
else
	return
end if

choose case dwo.name
	case "cod_deposito"
		
		if data<>"" then
			
			f_po_loaddddw_dw(dw_ricerca, &
					  "cod_reparto", &
					  sqlca, &
					  "anag_reparti", &
					  "cod_reparto", &
					  "des_reparto", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_deposito='"+data+"'")
					  
		else
			
			f_po_loaddddw_dw(dw_ricerca, &
					  "cod_reparto", &
					  sqlca, &
					  "anag_reparti", &
					  "cod_reparto", &
					  "des_reparto", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
		end if
		
		setnull(ls_null)
		setitem(1, "cod_reparto", ls_null)
		
end choose
end event

