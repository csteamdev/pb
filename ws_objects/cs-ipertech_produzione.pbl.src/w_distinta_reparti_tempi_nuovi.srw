﻿$PBExportHeader$w_distinta_reparti_tempi_nuovi.srw
forward
global type w_distinta_reparti_tempi_nuovi from w_cs_xx_principale
end type
type cb_elimina from commandbutton within w_distinta_reparti_tempi_nuovi
end type
type dw_lista_prodotti from datawindow within w_distinta_reparti_tempi_nuovi
end type
type cb_salva from commandbutton within w_distinta_reparti_tempi_nuovi
end type
type cb_manuale from commandbutton within w_distinta_reparti_tempi_nuovi
end type
type cb_tutti from commandbutton within w_distinta_reparti_tempi_nuovi
end type
type dw_folder from u_folder within w_distinta_reparti_tempi_nuovi
end type
type dw_distinta_reparti_tempi_lista from datawindow within w_distinta_reparti_tempi_nuovi
end type
end forward

global type w_distinta_reparti_tempi_nuovi from w_cs_xx_principale
integer width = 3273
integer height = 2048
string title = "Tempi Distinta - Reparti"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
event ue_ridimensiona ( )
event ue_posiziona_window ( )
cb_elimina cb_elimina
dw_lista_prodotti dw_lista_prodotti
cb_salva cb_salva
cb_manuale cb_manuale
cb_tutti cb_tutti
dw_folder dw_folder
dw_distinta_reparti_tempi_lista dw_distinta_reparti_tempi_lista
end type
global w_distinta_reparti_tempi_nuovi w_distinta_reparti_tempi_nuovi

forward prototypes
public function integer wf_inserisci (string fs_cod_prodotto)
public function integer wf_salva (long fl_row, ref string fs_msg)
end prototypes

event ue_ridimensiona();// Sposta gli oggetti all'interno della finestra in modo relativo alla sua dimensione

dw_folder.move(1,1)
dw_distinta_reparti_tempi_lista.move(20,150)

dw_folder.height 								= height
dw_folder.width 								= width
dw_distinta_reparti_tempi_lista.height 	= height - 240 - 150
dw_distinta_reparti_tempi_lista.width 	= width - 80

dw_lista_prodotti.x = dw_distinta_reparti_tempi_lista.x
dw_lista_prodotti.y = dw_distinta_reparti_tempi_lista.y
dw_lista_prodotti.width = dw_distinta_reparti_tempi_lista.width
dw_lista_prodotti.height = dw_distinta_reparti_tempi_lista.height

cb_tutti.x = 20
cb_tutti.y = dw_distinta_reparti_tempi_lista.y + dw_distinta_reparti_tempi_lista.height + 20
//-----------
cb_salva.x = cb_tutti.x
cb_salva.y = cb_tutti.y

cb_manuale.x = 20 + cb_salva.width + 20
cb_elimina.x = 20 + cb_salva.width + 20 + cb_manuale.width + 20

cb_manuale.y = dw_distinta_reparti_tempi_lista.y + dw_distinta_reparti_tempi_lista.height + 20
cb_elimina.y = dw_distinta_reparti_tempi_lista.y + dw_distinta_reparti_tempi_lista.height + 20


end event

event ue_posiziona_window();move(1,1)

this.width = w_cs_xx_mdi.mdi_1.width - 60
this.height = w_cs_xx_mdi.mdi_1.height - 40

end event

public function integer wf_inserisci (string fs_cod_prodotto);datetime ldt_oggi
string ls_cod_versione_default, ls_reparti_distinta[], ls_msg, ls_null
long ll_tot, ll_index, ll_new
uo_funzioni_1 luo_funzioni


ldt_oggi = datetime(today(), now())		 
setnull(ls_null)

 
select cod_versione
into   :ls_cod_versione_default
from   distinta_padri
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :fs_cod_prodotto and
		 flag_predefinita = 'S' and
		 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
		 
//verifica che il prodotto non sia già presente sulla datawindow
ll_tot = dw_distinta_reparti_tempi_lista.rowcount()
for ll_index = 1 to ll_tot
	if fs_cod_prodotto = dw_distinta_reparti_tempi_lista.getitemstring(ll_index, "cod_prodotto_distinta") then
		//già esiste
		return -1
	end if
next

if ls_cod_versione_default="" then setnull(ls_cod_versione_default)

//Donato 01/02/2012 Spostata funzione globale in user object oggetto
luo_funzioni = create uo_funzioni_1
ll_tot = luo_funzioni.uof_trova_reparti(	true,-1, -1, -1, fs_cod_prodotto, ls_cod_versione_default, ref ls_reparti_distinta[], ref ls_msg)
destroy luo_funzioni

//ll_tot = f_trova_reparti(-1, -1, -1, fs_cod_prodotto, ls_cod_versione_default, ref ls_reparti_distinta[], ref ls_msg)
//fine modifica -------------------------------------------

if ll_tot < 0 then
	ls_msg = "Errore in ricerca reparti~r~n" + ls_msg
	g_mb.error(ls_msg)
	return -1
end if

if upperbound(ls_reparti_distinta) > 0 then
else
	//se non trovi nessun reparto inserisci comunque una riga con reparto vuoto
	ls_reparti_distinta[1] = ls_null
end if

//f_PO_LoadDDDW_DW(dw_distinta_reparti_tempi_lista,"cod_versione",sqlca,&
//					  "distinta_padri","cod_versione","des_versione",&
//					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + fs_cod_prodotto + "' and " + &
//					  "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))")	

//inserisci una riga per ogni reparto
for ll_index = 1 to upperbound(ls_reparti_distinta)
	//if ls_reparti_distinta[ll_index]="" or isnull(ls_reparti_distinta[ll_index]) then continue
	
	ll_new = dw_distinta_reparti_tempi_lista.insertrow(0)
	
	dw_distinta_reparti_tempi_lista.setitem(ll_new, "cod_prodotto_distinta", fs_cod_prodotto)
	dw_distinta_reparti_tempi_lista.setitem(ll_new, "cod_versione", ls_cod_versione_default)
	dw_distinta_reparti_tempi_lista.setitem(ll_new, "cod_reparto", ls_reparti_distinta[ll_index])
next

//alla fine elimina dalla dw di partenza
return 1


end function

public function integer wf_salva (long fl_row, ref string fs_msg);string ls_cod_prodotto_distinta, ls_cod_versione, ls_cod_reparto, ls_cod_prodotto_raggruppato, ls_cod_gruppo_variante
decimal ld_tempo_produzione
long ll_progressivo

fs_msg								= ""
ls_cod_prodotto_distinta 			= dw_distinta_reparti_tempi_lista.getitemstring(fl_row, "cod_prodotto_distinta")
ls_cod_versione 					= dw_distinta_reparti_tempi_lista.getitemstring(fl_row, "cod_versione")
ls_cod_reparto 						= dw_distinta_reparti_tempi_lista.getitemstring(fl_row, "cod_reparto")
ls_cod_prodotto_raggruppato 	= dw_distinta_reparti_tempi_lista.getitemstring(fl_row, "cod_prodotto_raggruppato")
ls_cod_gruppo_variante 			= dw_distinta_reparti_tempi_lista.getitemstring(fl_row, "cod_gruppo_variante")
ld_tempo_produzione 			= dw_distinta_reparti_tempi_lista.getitemdecimal(fl_row, "tempo_produzione")

// prodotto
if isnull(ls_cod_prodotto_distinta) or ls_cod_prodotto_distinta = "" then
	fs_msg = "Codice Prodotto obbligatorio sulla riga "+string(fl_row)
	return -1
end if

// versione
if isnull(ls_cod_versione) or ls_cod_versione = "" then
	fs_msg = "Codice Versione obbligatorio sulla riga "+string(fl_row) + " (prodotto "+ls_cod_prodotto_distinta+")"
	return -1
end if

// reparto
if isnull(ls_cod_reparto) or ls_cod_reparto = "" then
	fs_msg = "Codice Reparto obbligatorio sulla riga "+string(fl_row) + " (prodotto "+ls_cod_prodotto_distinta+")"
	return -1
end if

if trim(ls_cod_prodotto_raggruppato)="" then setnull(ls_cod_prodotto_raggruppato)
if trim(ls_cod_gruppo_variante)="" then setnull(ls_cod_gruppo_variante)
if isnull(ld_tempo_produzione) then ld_tempo_produzione = 0

select max(progressivo)
into   :ll_progressivo
from   tab_distinta_reparti_tempi
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto_distinta = :ls_cod_prodotto_distinta and
		 cod_versione = :ls_cod_versione;
		 
if isnull(ll_progressivo) or ll_progressivo < 1 then
	ll_progressivo = 1
else
	ll_progressivo += 1
end if

insert into tab_distinta_reparti_tempi  
		( cod_azienda,   
		  cod_prodotto_distinta,   
		  cod_versione,   
		  cod_reparto,   
		  progressivo,   
		  cod_prodotto_raggruppato,   
		  cod_gruppo_variante,   
		  tempo_produzione )  
values ( :s_cs_xx.cod_azienda,   
		  :ls_cod_prodotto_distinta,   
		  :ls_cod_versione,   
		  :ls_cod_reparto,   
		  :ll_progressivo,   
		  :ls_cod_prodotto_raggruppato,   
		  :ls_cod_gruppo_variante,   
		  :ld_tempo_produzione )  ;
		  
if sqlca.sqlcode < 0 then
	fs_msg = "Errore in fase di inserimento dati in tabella tab_distinta_reparti_tempi.~r~n"+sqlca.sqlerrtext
	return -1
end if


return 1
end function

on w_distinta_reparti_tempi_nuovi.create
int iCurrent
call super::create
this.cb_elimina=create cb_elimina
this.dw_lista_prodotti=create dw_lista_prodotti
this.cb_salva=create cb_salva
this.cb_manuale=create cb_manuale
this.cb_tutti=create cb_tutti
this.dw_folder=create dw_folder
this.dw_distinta_reparti_tempi_lista=create dw_distinta_reparti_tempi_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_elimina
this.Control[iCurrent+2]=this.dw_lista_prodotti
this.Control[iCurrent+3]=this.cb_salva
this.Control[iCurrent+4]=this.cb_manuale
this.Control[iCurrent+5]=this.cb_tutti
this.Control[iCurrent+6]=this.dw_folder
this.Control[iCurrent+7]=this.dw_distinta_reparti_tempi_lista
end on

on w_distinta_reparti_tempi_nuovi.destroy
call super::destroy
destroy(this.cb_elimina)
destroy(this.dw_lista_prodotti)
destroy(this.cb_salva)
destroy(this.cb_manuale)
destroy(this.cb_tutti)
destroy(this.dw_folder)
destroy(this.dw_distinta_reparti_tempi_lista)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[], lw_vuoto[]

set_w_options(c_NoResizeWin)

//iuo_dw_main = dw_distinta_reparti_tempi_lista

//dw_distinta_reparti_tempi_lista.set_dw_key("cod_azienda")

//dw_distinta_reparti_tempi_lista.set_dw_options(sqlca, &
//                                    pcca.null_object, &
//                                    c_default, &
//                                    c_default + &
//												c_nohighlightselected + &
//												c_ViewModeBorderUnchanged + &
//												c_NoCursorRowFocusRect)

lw_oggetti[1] = dw_lista_prodotti
lw_oggetti[2] = cb_tutti
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

lw_oggetti = lw_vuoto

lw_oggetti[1] = cb_salva
lw_oggetti[2] = dw_distinta_reparti_tempi_lista
lw_oggetti[3] = cb_manuale
lw_oggetti[4] = cb_elimina
dw_folder.fu_assigntab(2, "Inserimento", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

dw_lista_prodotti.settransobject(sqlca)
dw_distinta_reparti_tempi_lista.settransobject(sqlca)
						
event post ue_posiziona_window()

event post ue_ridimensiona()

dw_lista_prodotti.postevent("ue_retrieve")



end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_distinta_reparti_tempi_lista, &
                 "cod_prodotto_distinta", &
                 sqlca, &
                 "anag_prodotti", &
                 "cod_prodotto", &
                 "des_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  "cod_prodotto IN " + &
					  " (select distinct cod_prodotto from distinta_padri where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N' ) "  )


f_po_loaddddw_dw(dw_distinta_reparti_tempi_lista, &
					  "cod_reparto", &
					  sqlca, &
					  "anag_reparti", &
					  "cod_reparto", &
					  "des_reparto", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco= 'N' ")


f_po_loaddddw_dw(dw_distinta_reparti_tempi_lista, &
					  "cod_gruppo_variante", &
					  sqlca, &
					  "gruppi_varianti", &
					  "cod_gruppo_variante", &
					  "des_gruppo_variante", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event resize;event post ue_ridimensiona()
end event

type cb_elimina from commandbutton within w_distinta_reparti_tempi_nuovi
integer x = 1701
integer y = 1840
integer width = 389
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Deseleziona"
end type

event clicked;long ll_row
string ls_cod_prodotto, ls_des_prodotto

dw_distinta_reparti_tempi_lista.accepttext()
ll_row = dw_distinta_reparti_tempi_lista.getrow()

if ll_row > 0 then
	if not g_mb.confirm("Eliminare la riga dalla selezione?") then return
end if

ls_cod_prodotto = dw_distinta_reparti_tempi_lista.getitemstring(ll_row, "cod_prodotto_distinta")
ls_des_prodotto = f_des_tabella("anag_prodotti",  "cod_prodotto='"+ls_cod_prodotto+"''", "des_prodotto")

//elimino dalla selezione
dw_distinta_reparti_tempi_lista.deleterow(ll_row)

//la riporto indietro
ll_row = dw_lista_prodotti.insertrow(0)
dw_lista_prodotti.setitem(ll_row, "cod_prodotto",ls_cod_prodotto)
dw_lista_prodotti.setitem(ll_row, "des_prodotto", ls_des_prodotto)

dw_lista_prodotti.sort()
end event

type dw_lista_prodotti from datawindow within w_distinta_reparti_tempi_nuovi
event ue_retrieve ( )
integer x = 23
integer y = 20
integer width = 3154
integer height = 1800
integer taborder = 20
string title = "none"
string dataobject = "d_prodotti_non_in_reparti_tempi"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event ue_retrieve();dw_lista_prodotti.retrieve(s_cs_xx.cod_azienda)
end event

event doubleclicked;string ls_cod_prodotto, ls_cod_versione_default, ls_reparti_distinta[], ls_msg
datetime ldt_oggi
long ll_ret, ll_tot, ll_index, ll_new
integer li_ret

if row>0 then
	ls_cod_prodotto = getitemstring(row, "cod_prodotto")
	
	if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
		li_ret = wf_inserisci(ls_cod_prodotto)
		
		if li_ret=1 then deleterow(row)
	end if
	
end if
end event

event rowfocuschanged;if currentrow > 0 then
	selectrow(0, false)
	selectrow(currentrow, true)
end if
end event

type cb_salva from commandbutton within w_distinta_reparti_tempi_nuovi
integer x = 1285
integer y = 1840
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;long ll_tot_row, ll_i, ll_max
string ls_cod_prodotto, ls_cod_versione, ls_msg

dw_distinta_reparti_tempi_lista.accepttext()
ll_tot_row = dw_distinta_reparti_tempi_lista.rowcount()

if ll_tot_row > 0 then
	if not g_mb.confirm("Salvare i dati?") then return
else
	g_mb.show("Nessun dato da salvare!")
	return
end if

for ll_i = 1 to ll_tot_row
	
	//funzione di inserimento
	if wf_salva(ll_i, ls_msg)<0 then
		//dai errore
		g_mb.error(ls_msg)
		
		//fai rollback
		rollback;
		return
	end if
next

//se arrivi fin qui fai la commit;
commit;
dw_folder.fu_selecttab(1)
dw_distinta_reparti_tempi_lista.reset()
dw_lista_prodotti.postevent("ue_retrieve")

return
end event

type cb_manuale from commandbutton within w_distinta_reparti_tempi_nuovi
integer x = 434
integer y = 1840
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ins.Manuale"
end type

event clicked;if not g_mb.confirm("Inserire una nuova riga manualmente?") then return

dw_distinta_reparti_tempi_lista.insertrow(0)
end event

type cb_tutti from commandbutton within w_distinta_reparti_tempi_nuovi
integer x = 23
integer y = 1840
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Tutti"
end type

event clicked;long ll_tot, ll_index
string ls_cod_prodotto

if not g_mb.confirm("Selezionare tutti i prodotti mancanti della lista?") then return

ll_tot = dw_lista_prodotti.rowcount()

for ll_index=1 to ll_tot
	
	ls_cod_prodotto = dw_lista_prodotti.getitemstring(ll_index, "cod_prodotto")
	
	wf_inserisci(ls_cod_prodotto)
next
end event

type dw_folder from u_folder within w_distinta_reparti_tempi_nuovi
integer y = 4
integer width = 3195
integer height = 1928
integer taborder = 20
end type

type dw_distinta_reparti_tempi_lista from datawindow within w_distinta_reparti_tempi_nuovi
integer x = 23
integer y = 20
integer width = 3154
integer height = 1800
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_distinta_reparti_tempi_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event itemchanged;
if isvalid(dwo) then
	string ls_null
	
	choose case dwo.name	
		case "cod_prodotto_distinta"
			
			if isnull(data) or len(trim(data)) < 1 then
				setnull(ls_null)
				setitem(row, "cod_versione", ls_null)
				
				f_po_loaddddw_dw(dw_distinta_reparti_tempi_lista, &
									  "cod_versione", &
									  sqlca, &
									  "distinta_padri", &
									  "cod_versione", &
									  "des_versione", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and (cod_prodotto = '' or cod_prodotto is null) ")
				
			else
				setnull(ls_null)
				setitem(row, "cod_versione", ls_null)
				
				f_po_loaddddw_dw(dw_distinta_reparti_tempi_lista, &
									  "cod_versione", &
									  sqlca, &
									  "distinta_padri", &
									  "cod_versione", &
									  "des_versione", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + data + "' ")
			end if
	end choose
end if
				
end event

event rowfocuschanged;if currentrow > 0 then
	
	string ls_cod_prodotto
	
	ls_cod_prodotto = getitemstring(currentrow, "cod_prodotto_distinta")
	
	if isnull(ls_cod_prodotto) or len(trim(ls_cod_prodotto)) < 1 then
		
		f_po_loaddddw_dw(dw_distinta_reparti_tempi_lista, &
							  "cod_versione", &
							  sqlca, &
							  "distinta_padri", &
							  "cod_versione", &
							  "des_versione", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and (cod_prodotto = '' or cod_prodotto is null) ")
		
	else
		
		f_po_loaddddw_dw(dw_distinta_reparti_tempi_lista, &
							  "cod_versione", &
							  sqlca, &
							  "distinta_padri", &
							  "cod_versione", &
							  "des_versione", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto = '" + ls_cod_prodotto + "' ")
		
	end if
	
	selectrow(0, false)
	selectrow(currentrow, true)
	
end if
end event

