﻿$PBExportHeader$w_documenti_ordini.srw
forward
global type w_documenti_ordini from window
end type
type st_stampante_t from statictext within w_documenti_ordini
end type
type st_stampante from statictext within w_documenti_ordini
end type
type cb_cambia_stampante from commandbutton within w_documenti_ordini
end type
type cb_chiudi from commandbutton within w_documenti_ordini
end type
type cb_stampa from commandbutton within w_documenti_ordini
end type
type st_documento from statictext within w_documenti_ordini
end type
type st_titolo from statictext within w_documenti_ordini
end type
type dw_lista from datawindow within w_documenti_ordini
end type
end forward

global type w_documenti_ordini from window
integer width = 3840
integer height = 2476
boolean titlebar = true
string title = "Untitled"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
st_stampante_t st_stampante_t
st_stampante st_stampante
cb_cambia_stampante cb_cambia_stampante
cb_chiudi cb_chiudi
cb_stampa cb_stampa
st_documento st_documento
st_titolo st_titolo
dw_lista dw_lista
end type
global w_documenti_ordini w_documenti_ordini

type variables

private:
	string			is_tipo_documento, is_file_stampati[]
	integer			ii_anno
	long				il_num
end variables

forward prototypes
public function integer wf_estrai_blob (integer ai_anno_documento, long al_num_documento, long al_riga_documento, string as_cod_nota, ref blob abl_documento, ref string as_errore)
public function integer wf_mostra_documento (long al_row, ref string as_errore)
public function integer wf_retrieve ()
public subroutine wf_leggi_stampante ()
end prototypes

public function integer wf_estrai_blob (integer ai_anno_documento, long al_num_documento, long al_riga_documento, string as_cod_nota, ref blob abl_documento, ref string as_errore);

selectblob 	note_esterne
into 			:abl_documento
from det_ord_ven_note
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ai_anno_documento and
		num_registrazione = :al_num_documento and
		prog_riga_ord_ven = :al_riga_documento and
		cod_nota = :as_cod_nota;
	
if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura documento dalla tabella (sqlcode -1): "+sqlca.sqlerrtext
	return -1

elseif sqlca.sqlcode = 100 then
	as_errore = "Documento non trovato! Potrebbe essere stato cancellato da un altro utente!"
	return -1
	
elseif sqlca.sqlcode <> 0 then
	as_errore = "Errore in lettura documento dalla tabella (sqlcode <>-1,<>0 e <>100)"
	return -1
	
end if


return 0
end function

public function integer wf_mostra_documento (long al_row, ref string as_errore);integer					li_anno
long						ll_numero, ll_prog_riga, ll_prog_mimetype, ll_len
string					ls_cod_nota, ls_estensione, ls_temp_dir, ls_rnd, ls_file_name
blob						lb_blob
uo_shellexecute 		luo_run


if al_row > 0 then
	
	li_anno = dw_lista.getitemnumber(al_row, "det_ord_ven_note_anno_registrazione")
	ll_numero = dw_lista.getitemnumber(al_row, "det_ord_ven_note_num_registrazione")
	ll_prog_riga = dw_lista.getitemnumber(al_row, "det_ord_ven_note_prog_riga_ord_ven")
	ls_cod_nota = dw_lista.getitemstring(al_row, "det_ord_ven_note_cod_nota")
	ll_prog_mimetype = dw_lista.getitemnumber(al_row, "det_ord_ven_note_prog_mimetype")
	
	if wf_estrai_blob(li_anno, ll_numero, ll_prog_riga, ls_cod_nota, lb_blob, as_errore) < 0 then
		return -1
	end if
	

	ll_len = lenA(lb_blob)
	
	
	select estensione
	into :ls_estensione
	from tab_mimetype
	where cod_azienda = :s_cs_xx.cod_azienda and
			prog_mimetype = :ll_prog_mimetype;
	
	ls_temp_dir = guo_functions.uof_get_user_temp_folder( )
	
	ls_rnd = string( hour(now()),"00") +"_" + string( minute(now()),"00") +"_" + string( second(now()),"00") 
	ls_file_name =  ls_temp_dir + ls_rnd + "." + ls_estensione
	guo_functions.uof_blob_to_file( lb_blob, ls_file_name)
	
	
	luo_run = create uo_shellexecute
	luo_run.uof_run( handle(this), ls_file_name )
	destroy(luo_run)
		
end if

return 0
end function

public function integer wf_retrieve ();string			ls_doc
long				ll_tot


ls_doc = "DDT"

if is_tipo_documento="F" then
	ls_doc = "FATTURA"
end if

this.title = "Documenti riferiti a ordine per " + ls_doc + " n°" + string(ii_anno) + "/" + string(il_num)

ll_tot = dw_lista.retrieve(s_cs_xx.cod_azienda, ii_anno, il_num)

if ll_tot>0 then
	cb_stampa.enabled = true
else
	cb_stampa.enabled = false
end if

return ll_tot
end function

public subroutine wf_leggi_stampante ();

string					ls_input, ls_printer_name
long						ll_pos


ls_input = PrintGetPrinter()

ll_pos=pos (ls_input, "~t")
ls_printer_name=left(ls_input, ll_pos -1)

st_stampante.text = ls_printer_name


return
end subroutine

on w_documenti_ordini.create
this.st_stampante_t=create st_stampante_t
this.st_stampante=create st_stampante
this.cb_cambia_stampante=create cb_cambia_stampante
this.cb_chiudi=create cb_chiudi
this.cb_stampa=create cb_stampa
this.st_documento=create st_documento
this.st_titolo=create st_titolo
this.dw_lista=create dw_lista
this.Control[]={this.st_stampante_t,&
this.st_stampante,&
this.cb_cambia_stampante,&
this.cb_chiudi,&
this.cb_stampa,&
this.st_documento,&
this.st_titolo,&
this.dw_lista}
end on

on w_documenti_ordini.destroy
destroy(this.st_stampante_t)
destroy(this.st_stampante)
destroy(this.cb_cambia_stampante)
destroy(this.cb_chiudi)
destroy(this.cb_stampa)
destroy(this.st_documento)
destroy(this.st_titolo)
destroy(this.dw_lista)
end on

event open;s_cs_xx_parametri			lstr_parametri



lstr_parametri = message.powerobjectparm


ii_anno = lstr_parametri.parametro_d_1
il_num = lstr_parametri.parametro_d_2
is_tipo_documento = lstr_parametri.parametro_s_14


wf_leggi_stampante()


st_documento.text = "D.D.T. n°"

if is_tipo_documento="F" then
	dw_lista.dataobject = "d_documenti_ordini_fatture"
	st_documento.text = "Fattura n°"
end if

dw_lista.settransobject(sqlca)

st_documento.text += string(ii_anno) + "/" + string(il_num)

dw_lista.object.p_documento.filename = s_cs_xx.volume + s_cs_xx.risorse + "treeview\documento_rosso.png"

wf_retrieve()


									
												

end event

event close;
long			ll_index


//ciclo per cancellare i file creati per la stampa
for ll_index=1 to upperbound(is_file_stampati[])
	filedelete(is_file_stampati[ll_index])
next
end event

type st_stampante_t from statictext within w_documenti_ordini
integer x = 599
integer y = 60
integer width = 448
integer height = 92
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Stampa su"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_stampante from statictext within w_documenti_ordini
integer x = 1065
integer y = 60
integer width = 2711
integer height = 92
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_cambia_stampante from commandbutton within w_documenti_ordini
integer x = 23
integer y = 60
integer width = 558
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cambia Stampante"
end type

event clicked;
open(w_printer)


wf_leggi_stampante()
end event

type cb_chiudi from commandbutton within w_documenti_ordini
integer x = 23
integer y = 324
integer width = 558
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi"
end type

event clicked;

close(parent)
end event

type cb_stampa from commandbutton within w_documenti_ordini
integer x = 23
integer y = 192
integer width = 558
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa Tutti"
end type

event clicked;
string					ls_path_exe, ls_cmd, ls_file_pdf, ls_cod_nota, ls_errore, ls_estensione, ls_temp_dir, ls_rnd, ls_stampante

long						ll_index, ll_tot, ll_numero, ll_prog_riga, ll_prog_mimetype, ll_file

integer					li_anno

blob						lb_blob



if not g_mb.confirm("Mandare in stampa tutti i documenti relativi alle righe ordine riferiti?") then
	return
end if


ls_path_exe = s_cs_xx.volume + s_cs_xx.risorse + "libs\Foxit Reader.exe"

if FileExists(ls_path_exe) then
else
	g_mb.warning("Impossibile stampare: percorso dell'eseguibile Foxit Reader.exe non trovato! ("+ls_path_exe+")")
	return
end if


ll_tot = dw_lista.rowcount()
ll_file = upperbound(is_file_stampati[])


ls_stampante = st_stampante.text

if isnull(ls_stampante) or ls_stampante="" then
	ls_path_exe = '"' + ls_path_exe + '" /p '
	ls_stampante = ""
	
else
	ls_path_exe = '"' + ls_path_exe + '" /t '
	ls_stampante = ' "' + ls_stampante + '"'
	
end if

for ll_index=1 to ll_tot
	
	li_anno = dw_lista.getitemnumber(ll_index, "det_ord_ven_note_anno_registrazione")
	ll_numero = dw_lista.getitemnumber(ll_index, "det_ord_ven_note_num_registrazione")
	ll_prog_riga = dw_lista.getitemnumber(ll_index, "det_ord_ven_note_prog_riga_ord_ven")
	ls_cod_nota = dw_lista.getitemstring(ll_index, "det_ord_ven_note_cod_nota")
	ll_prog_mimetype = dw_lista.getitemnumber(ll_index, "det_ord_ven_note_prog_mimetype")
	
	
	//------------------------------------------------------------------------------------------------------------------------------------
	//estrai il file
	if wf_estrai_blob(li_anno, ll_numero, ll_prog_riga, ls_cod_nota, lb_blob, ls_errore) < 0 then
		return -1
	end if
	
	
	//------------------------------------------------------------------------------------------------------------------------------------
	//salva su cartella temporanea
	select estensione
	into :ls_estensione
	from tab_mimetype
	where cod_azienda = :s_cs_xx.cod_azienda and
			prog_mimetype = :ll_prog_mimetype;
	
	//stampa solo quelli di tipo PDF
	if upper(ls_estensione)<>"PDF" then continue
	
	
	ls_temp_dir = guo_functions.uof_get_user_temp_folder( )
	
	//nome file:    YYMMDD_hhmmss_nn.pdf
	ls_rnd = string( year(today()),"00") + string( month(today()),"00") + string( day(today()),"00") + "_" + &
				string( hour(now()),"00") + string( minute(now()),"00") + string( second(now()),"00") + "_"+string(ll_index)
	ls_file_pdf =  ls_temp_dir + ls_rnd + "." + ls_estensione
	guo_functions.uof_blob_to_file( lb_blob, ls_file_pdf)
	
	
	//------------------------------------------------------------------------------------------------------------------------------------
	//salvo in un array i files pdf che via via vado a creare, per poterli cancellare dopo la stampa
	ll_file += 1
	is_file_stampati[ll_file] = ls_file_pdf
	
	//comando:    "PATH_EXE_FOXIT_READER" /p "PATH_FILE_PDF_TO_PRINT"
	// oppure       "PATH_EXE_FOXIT_READER" /t "PATH_FILE_PDF_TO_PRINT'" "PRINTER_NAME"
	ls_cmd = ls_path_exe + ' "' + ls_file_pdf + '"' + ls_stampante
	
	
	//------------------------------------------------------------------------------------------------------------------------------------
	//lancia il comando di stampa
	run(ls_cmd)
	
next






end event

type st_documento from statictext within w_documenti_ordini
integer x = 635
integer y = 192
integer width = 3163
integer height = 92
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Documento n°"
boolean focusrectangle = false
end type

type st_titolo from statictext within w_documenti_ordini
integer x = 635
integer y = 312
integer width = 3127
integer height = 192
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Vengono mostrate solo le righe del documento fiscale con riferimento a riga ordine di vendita con presenza di documenti che iniziano per ~"P~""
boolean focusrectangle = false
end type

type dw_lista from datawindow within w_documenti_ordini
integer x = 41
integer y = 532
integer width = 3749
integer height = 1828
integer taborder = 10
string title = "none"
string dataobject = "d_documenti_ordini_ddt"
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;string					ls_errore
integer					li_ret


if row>0 then
else
	return
end if

li_ret = wf_mostra_documento(row, ls_errore)

if li_ret<0 then
	g_mb.error(ls_errore)
end if
end event

