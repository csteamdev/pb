﻿$PBExportHeader$w_elenco_prodotti_ordine.srw
$PBExportComments$Finestra con l'elenco dei prodotti in ordine
forward
global type w_elenco_prodotti_ordine from w_cs_xx_risposta
end type
type st_reparto from statictext within w_elenco_prodotti_ordine
end type
type st_cliente from statictext within w_elenco_prodotti_ordine
end type
type st_anno_num_ordine from statictext within w_elenco_prodotti_ordine
end type
type dw_elenco_prodotti_produzione from datawindow within w_elenco_prodotti_ordine
end type
type st_1 from statictext within w_elenco_prodotti_ordine
end type
type cb_conferma from commandbutton within w_elenco_prodotti_ordine
end type
type cb_annulla from commandbutton within w_elenco_prodotti_ordine
end type
end forward

global type w_elenco_prodotti_ordine from w_cs_xx_risposta
integer width = 2981
integer height = 2184
string title = "INSERIRE QUANTITA~'"
boolean controlmenu = false
long backcolor = 80269524
st_reparto st_reparto
st_cliente st_cliente
st_anno_num_ordine st_anno_num_ordine
dw_elenco_prodotti_produzione dw_elenco_prodotti_produzione
st_1 st_1
cb_conferma cb_conferma
cb_annulla cb_annulla
end type
global w_elenco_prodotti_ordine w_elenco_prodotti_ordine

type variables
integer ii_ok
end variables

on w_elenco_prodotti_ordine.create
int iCurrent
call super::create
this.st_reparto=create st_reparto
this.st_cliente=create st_cliente
this.st_anno_num_ordine=create st_anno_num_ordine
this.dw_elenco_prodotti_produzione=create dw_elenco_prodotti_produzione
this.st_1=create st_1
this.cb_conferma=create cb_conferma
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_reparto
this.Control[iCurrent+2]=this.st_cliente
this.Control[iCurrent+3]=this.st_anno_num_ordine
this.Control[iCurrent+4]=this.dw_elenco_prodotti_produzione
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.cb_conferma
this.Control[iCurrent+7]=this.cb_annulla
end on

on w_elenco_prodotti_ordine.destroy
call super::destroy
destroy(this.st_reparto)
destroy(this.st_cliente)
destroy(this.st_anno_num_ordine)
destroy(this.dw_elenco_prodotti_produzione)
destroy(this.st_1)
destroy(this.cb_conferma)
destroy(this.cb_annulla)
end on

event open;call super::open;integer li_anno_registrazione
long ll_num_registrazione,ll_prog_riga_ord_ven,ll_riga,ll_progr_det_produzione
string ls_cod_reparto,ls_cod_prodotto,ls_des_prodotto,ls_des_reparto,ls_rag_soc,ls_cod_cliente
decimal ld_quan_da_produrre

ii_ok=-1

li_anno_registrazione = s_cs_xx.parametri.parametro_ul_1
ll_num_registrazione = s_cs_xx.parametri.parametro_ul_2
ls_cod_reparto = s_cs_xx.parametri.parametro_s_1


select des_reparto
into   :ls_des_reparto
from   anag_reparti
where  cod_azienda =:s_cs_xx.cod_azienda
and    cod_reparto =:ls_cod_reparto;

if sqlca.sqlcode < 0 then 
	s_cs_xx.parametri.parametro_i_1 = 0
	s_cs_xx.parametri.parametro_s_2 = "Errore sul DB: " + sqlca.sqlerrtext
	ii_ok = 0
	w_elenco_prodotti_ordine.postevent("close")
	return -1
end if

select cod_cliente
into   :ls_cod_cliente
from   tes_ord_ven
where  cod_azienda =:s_cs_xx.cod_azienda
and    anno_registrazione=:li_anno_registrazione
and    num_registrazione=:ll_num_registrazione;

if sqlca.sqlcode < 0 then 
	s_cs_xx.parametri.parametro_i_1 = 0
	s_cs_xx.parametri.parametro_s_2 = "Errore sul DB: " + sqlca.sqlerrtext
	ii_ok = 0
	w_elenco_prodotti_ordine.postevent("close")
	return -1
end if

select rag_soc_1
into   :ls_rag_soc
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente;

if sqlca.sqlcode < 0 then 
	s_cs_xx.parametri.parametro_i_1 = 0
	s_cs_xx.parametri.parametro_s_2 = "Errore sul DB: " + sqlca.sqlerrtext
	ii_ok = 0
	w_elenco_prodotti_ordine.postevent("close")
	return -1
end if

s_cs_xx.parametri.parametro_ds_1.sharedata(dw_elenco_prodotti_produzione)

st_anno_num_ordine.text = "Ordine numero " + string(ll_num_registrazione) + " anno " + string(li_anno_registrazione)
st_cliente.text = "Cliente: " + ls_rag_soc
st_reparto.text = "Reparto: " + ls_des_reparto



end event

event closequery;call super::closequery;if ii_ok=-1 then return 1

end event

type st_reparto from statictext within w_elenco_prodotti_ordine
integer x = 23
integer y = 360
integer width = 2903
integer height = 80
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "none"
boolean focusrectangle = false
end type

type st_cliente from statictext within w_elenco_prodotti_ordine
integer x = 23
integer y = 260
integer width = 2903
integer height = 80
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "none"
boolean focusrectangle = false
end type

type st_anno_num_ordine from statictext within w_elenco_prodotti_ordine
integer x = 23
integer y = 160
integer width = 2903
integer height = 80
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "none"
boolean focusrectangle = false
end type

type dw_elenco_prodotti_produzione from datawindow within w_elenco_prodotti_ordine
integer x = 23
integer y = 460
integer width = 2903
integer height = 1440
integer taborder = 10
string title = "none"
string dataobject = "d_elenco_prodotti_produzione"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type st_1 from statictext within w_elenco_prodotti_ordine
integer x = 23
integer y = 20
integer width = 2880
integer height = 100
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 67108864
string text = "Inserire quantità prodotta e numero colli"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_conferma from commandbutton within w_elenco_prodotti_ordine
integer x = 2377
integer y = 1920
integer width = 549
integer height = 140
integer taborder = 30
boolean bringtotop = true
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
boolean default = true
end type

event clicked;dw_elenco_prodotti_produzione.accepttext()
s_cs_xx.parametri.parametro_i_1 = 1

ii_ok=0

close (parent)

end event

type cb_annulla from commandbutton within w_elenco_prodotti_ordine
event clicked pbm_bnclicked
integer x = 23
integer y = 1920
integer width = 549
integer height = 140
integer taborder = 20
boolean bringtotop = true
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = 0
s_cs_xx.parametri.parametro_s_2 = "Processo annullato dall'operatore"
ii_ok = 0
close (parent)
end event

