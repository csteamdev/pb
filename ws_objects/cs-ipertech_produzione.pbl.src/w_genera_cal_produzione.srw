﻿$PBExportHeader$w_genera_cal_produzione.srw
forward
global type w_genera_cal_produzione from w_cs_xx_principale
end type
type tab_1 from tab within w_genera_cal_produzione
end type
type tabpage_selezione from userobject within tab_1
end type
type dw_selezione from uo_std_dw within tabpage_selezione
end type
type dw_1 from uo_std_dw within tabpage_selezione
end type
type tabpage_selezione from userobject within tab_1
dw_selezione dw_selezione
dw_1 dw_1
end type
type tabpage_calendario from userobject within tab_1
end type
type dw_day_simulazione from datawindow within tabpage_calendario
end type
type dw_day_attuale from datawindow within tabpage_calendario
end type
type dw_simulazione from datawindow within tabpage_calendario
end type
type dw_cal_attuale from datawindow within tabpage_calendario
end type
type dw_calendario from datawindow within tabpage_calendario
end type
type tabpage_calendario from userobject within tab_1
dw_day_simulazione dw_day_simulazione
dw_day_attuale dw_day_attuale
dw_simulazione dw_simulazione
dw_cal_attuale dw_cal_attuale
dw_calendario dw_calendario
end type
type tab_1 from tab within w_genera_cal_produzione
tabpage_selezione tabpage_selezione
tabpage_calendario tabpage_calendario
end type
type dw_prodotti from uo_dddw_checkbox within w_genera_cal_produzione
end type
end forward

global type w_genera_cal_produzione from w_cs_xx_principale
integer width = 4782
integer height = 2836
string title = "Carico Manuale Calendario Produzione"
tab_1 tab_1
dw_prodotti dw_prodotti
end type
global w_genera_cal_produzione w_genera_cal_produzione

type variables
long ib_oltre_soglia, ib_soglia, ib_disponibile
string ib_where_origine=""
end variables

on w_genera_cal_produzione.create
int iCurrent
call super::create
this.tab_1=create tab_1
this.dw_prodotti=create dw_prodotti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
this.Control[iCurrent+2]=this.dw_prodotti
end on

on w_genera_cal_produzione.destroy
call super::destroy
destroy(this.tab_1)
destroy(this.dw_prodotti)
end on

event pc_setwindow;call super::pc_setwindow;string ls_sql, ls_str

set_w_options(c_noenablepopup + c_closenosave)

tab_1.tabpage_selezione.dw_1.ib_colora=false
tab_1.tabpage_selezione.dw_selezione.insertrow(0)
tab_1.tabpage_calendario.dw_calendario.insertrow(0)

tab_1.tabpage_selezione.dw_selezione.setitem(1, 1, today() )
tab_1.tabpage_selezione.dw_selezione.setitem(1, 2, today() )
tab_1.tabpage_selezione.dw_selezione.setitem(1, 5, today() )
tab_1.tabpage_selezione.dw_selezione.setitem(1, 6, relativedate(today(), 30) )

ls_sql = 	"select cod_prodotto, des_prodotto from anag_prodotti where cod_prodotto in (select cod_modello from tab_flags_configuratore) and flag_blocco = 'N' order by 1"

dw_prodotti.uof_set_column_by_sql(ls_sql)
dw_prodotti.uof_set_parent_dw(tab_1.tabpage_selezione.dw_selezione, "cod_prodotto", "cod_prodotto")

dw_prodotti.uof_set_column_width(1, 400)
dw_prodotti.uof_set_column_width(2, 800)

dw_prodotti.uof_set_column_name(1, "Cod.")
dw_prodotti.uof_set_column_name(2, "Des. Prodotto")

dw_prodotti.uof_set_column_align(1, "center")

guo_functions.uof_resize_max_mdi( this, 3)

// -------- colori soglie
// carico codici dei colori
select stringa
into   :ls_str
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
  		 cod_parametro = 'CC1';
if sqlca.sqlcode <> 0 or isnull(ls_str) or len(ls_str) < 1 then
	ib_disponibile = 8053814
else
	ib_disponibile = long(ls_str)
end if

	
select stringa
into   :ls_str
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
  		 cod_parametro = 'CC2';
if sqlca.sqlcode <> 0 or isnull(ls_str) or len(ls_str) < 1 then
	ib_soglia = 8442861
else
	ib_soglia = long(ls_str)
end if

select stringa
into   :ls_str
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
  		 cod_parametro = 'CC3';
if sqlca.sqlcode <> 0 or isnull(ls_str) or len(ls_str) < 1 then
	ib_oltre_soglia = 1149386
else
	ib_oltre_soglia = long(ls_str)
end if

end event

event resize;/** TOLTO ANCESTOR **/

tab_1.resize(newwidth - 30, newheight - 30)


tab_1.event ue_resize()

end event

type tab_1 from tab within w_genera_cal_produzione
event ue_resize ( )
integer width = 4731
integer height = 2720
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_selezione tabpage_selezione
tabpage_calendario tabpage_calendario
end type

event ue_resize();// ridimensiono DW di ricerca
/*
tab_1.tabpage_selezione.dw_selezione.width = tab_1.width
tab_1.tabpage_selezione.dw_1.resize(tab_1.width -30, tab_1.height - tab_1.tabpage_selezione.dw_selezione.height -180 )

tab_1.tabpage_calendario.dw_calendario.width = tab_1.width - 130
tab_1.tabpage_calendario.dw_cal_attuale.width = (tab_1.width / 2) - 50
tab_1.tabpage_calendario.dw_simulazione.x = (tab_1.width / 2) + 20
tab_1.tabpage_calendario.dw_simulazione.width = tab_1.tabpage_calendario.dw_cal_attuale.width - 50

tab_1.tabpage_calendario.dw_cal_attuale.height = tab_1.tabpage_calendario.height - tab_1.tabpage_calendario.dw_calendario.height - 100
tab_1.tabpage_calendario.dw_simulazione.height = tab_1.tabpage_calendario.height - tab_1.tabpage_calendario.dw_calendario.height - 100
*/
// ridimensiono DW di ricerca
tab_1.tabpage_selezione.dw_selezione.width = tab_1.width
tab_1.tabpage_selezione.dw_1.resize(tab_1.width -30, tab_1.height - tab_1.tabpage_selezione.dw_selezione.height -180 )

tab_1.tabpage_calendario.dw_calendario.width = tab_1.width - 130
tab_1.tabpage_calendario.dw_cal_attuale.width = (tab_1.width / 2) - 50
tab_1.tabpage_calendario.dw_simulazione.x = (tab_1.width / 2) + 20
tab_1.tabpage_calendario.dw_simulazione.width = tab_1.tabpage_calendario.dw_cal_attuale.width - 50

tab_1.tabpage_calendario.dw_cal_attuale.height = (tab_1.tabpage_calendario.height / 2) - (tab_1.tabpage_calendario.dw_calendario.height  ) - 100

tab_1.tabpage_calendario.dw_day_attuale.x = tab_1.tabpage_calendario.dw_cal_attuale.x
tab_1.tabpage_calendario.dw_day_attuale.width = tab_1.tabpage_calendario.dw_cal_attuale.width
tab_1.tabpage_calendario.dw_day_attuale.y = tab_1.tabpage_calendario.dw_cal_attuale.y + tab_1.tabpage_calendario.dw_cal_attuale.height + 20
tab_1.tabpage_calendario.dw_day_attuale.height = tab_1.tabpage_calendario.height - tab_1.tabpage_calendario.dw_cal_attuale.y - tab_1.tabpage_calendario.dw_cal_attuale.height - 50


tab_1.tabpage_calendario.dw_simulazione.height = (tab_1.tabpage_calendario.height / 2) - tab_1.tabpage_calendario.dw_calendario.height - 100

tab_1.tabpage_calendario.dw_day_simulazione.x = tab_1.tabpage_calendario.dw_simulazione.x
tab_1.tabpage_calendario.dw_day_simulazione.width = tab_1.tabpage_calendario.dw_simulazione.width
tab_1.tabpage_calendario.dw_day_simulazione.y = tab_1.tabpage_calendario.dw_simulazione.y + tab_1.tabpage_calendario.dw_simulazione.height + 20
tab_1.tabpage_calendario.dw_day_simulazione.height = tab_1.tabpage_calendario.height - tab_1.tabpage_calendario.dw_simulazione.y - tab_1.tabpage_calendario.dw_simulazione.height - 50



end event

on tab_1.create
this.tabpage_selezione=create tabpage_selezione
this.tabpage_calendario=create tabpage_calendario
this.Control[]={this.tabpage_selezione,&
this.tabpage_calendario}
end on

on tab_1.destroy
destroy(this.tabpage_selezione)
destroy(this.tabpage_calendario)
end on

type tabpage_selezione from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4695
integer height = 2592
long backcolor = 12632256
string text = "SELEZIONE"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_selezione dw_selezione
dw_1 dw_1
end type

on tabpage_selezione.create
this.dw_selezione=create dw_selezione
this.dw_1=create dw_1
this.Control[]={this.dw_selezione,&
this.dw_1}
end on

on tabpage_selezione.destroy
destroy(this.dw_selezione)
destroy(this.dw_1)
end on

type dw_selezione from uo_std_dw within tabpage_selezione
integer x = 5
integer y = 8
integer width = 4663
integer height = 340
integer taborder = 20
string dataobject = "d_genera_cal_produzione_sel"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_search"
		accepttext()
		tab_1.tabpage_selezione.dw_1.event ue_imposta_sql()
		tab_1.tabpage_calendario.dw_calendario.object.b_verifica.enabled='1'
		
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(tab_1.tabpage_selezione.dw_selezione,"cod_cliente")
end choose
	

end event

event clicked;call super::clicked;choose case dwo.name
	case "cod_prodotto"
		dw_prodotti.show()
end choose
end event

type dw_1 from uo_std_dw within tabpage_selezione
event ue_imposta_sql ( )
event ue_retrieve ( )
integer x = 5
integer y = 348
integer width = 4663
integer height = 2240
integer taborder = 20
string dataobject = "d_genera_cal_produzione_lista_ordini"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

event ue_imposta_sql();string ls_sql, ls_sql1, ls_sql2, ls_sql_add, ls_cod_reparto, ls_flag_inviati, ls_prodotti[], ls_cod_cliente, ls_num_ord_cliente, ls_barcode_produzione, ls_str
long	ll_pos, ll_anno_ordine, ll_num_ordine, ll_ret, ll_i
date ld_data_reg_inizio, ld_data_reg_fine, ld_data_cons_inizio, ld_data_cons_fine

ls_sql = getsqlselect( )
ll_pos = pos( lower(ls_sql), "where" )

ls_sql1 = left( ls_sql, ll_pos - 1)
ls_sql2 = mid( ls_sql, ll_pos + 5 )

if len(ib_where_origine) = 0 then ib_where_origine = ls_sql2

ld_data_reg_inizio = tab_1.tabpage_selezione.dw_selezione.getitemdate(1,"data_inizio")
ld_data_reg_fine = tab_1.tabpage_selezione.dw_selezione.getitemdate(1,"data_fine")
ld_data_cons_inizio = tab_1.tabpage_selezione.dw_selezione.getitemdate(1,"data_consegna_inizio")
ld_data_cons_fine = tab_1.tabpage_selezione.dw_selezione.getitemdate(1,"data_consegna_fine")
ll_anno_ordine = tab_1.tabpage_selezione.dw_selezione.getitemnumber(1,"anno_registrazione")
ll_num_ordine = tab_1.tabpage_selezione.dw_selezione.getitemnumber(1,"num_registrazione")
ls_cod_cliente = tab_1.tabpage_selezione.dw_selezione.getitemstring(1,"cod_cliente")
ls_num_ord_cliente = tab_1.tabpage_selezione.dw_selezione.getitemstring(1,"num_ord_cliente")
g_str.explode( tab_1.tabpage_selezione.dw_selezione.getitemstring(1,"cod_prodotto") , ",", ls_prodotti)

ls_sql_add = " where TES.cod_azienda='" + s_cs_xx.cod_azienda + " ' "

if not isnull(ld_data_reg_inizio) and ld_data_reg_inizio > date("01/01/1900") then
	ls_sql_add += " and TES.data_registrazione >= '" + string(ld_data_reg_inizio, s_cs_xx.db_funzioni.formato_data   ) + "' "
end if
if not isnull(ld_data_reg_fine) and ld_data_reg_fine > date("01/01/1900") then
	ls_sql_add += " and TES.data_registrazione <= '" + string(ld_data_reg_fine, s_cs_xx.db_funzioni.formato_data   )+ "' "
end if

if not isnull(ld_data_cons_inizio) and ld_data_cons_inizio > date("01/01/1900") then
	ls_sql_add += " and TES.data_consegna >= '" + string(ld_data_cons_inizio, s_cs_xx.db_funzioni.formato_data   )+ "' "
end if
if not isnull(ld_data_cons_fine) and ld_data_cons_fine > date("01/01/1900") then
	ls_sql_add += " and TES.data_consegna <= '" + string(ld_data_cons_fine, s_cs_xx.db_funzioni.formato_data   )+ "' "
end if

if not isnull(ll_anno_ordine) and ll_anno_ordine > 0 then
	ls_sql_add += " and TES.anno_registrazione = " + string(ll_anno_ordine )
end if
if not isnull(ll_num_ordine) and ll_num_ordine > 0 then
	ls_sql_add += " and TES.num_registrazione = " + string(ll_num_ordine )
end if

if not isnull(ls_cod_cliente) and len(ls_cod_cliente) > 0 then
	ls_sql_add += " and TES.cod_cliente = '" + ls_cod_cliente + "' "
end if

if not isnull(ls_num_ord_cliente) and len(ls_num_ord_cliente) > 0 then
	ls_sql_add += " and TES.num_ord_cliente like '%" + ls_num_ord_cliente + "%' "
end if

if upperbound(ls_prodotti) > 0 then
	if  ls_prodotti[1] <> "TUTTI" then
		ls_str = ""
		for ll_i = 1 to upperbound(ls_prodotti)
			if len(ls_str) > 0 then ls_str += ","
			ls_str += g_str.format("'$1'",trim(ls_prodotti[ll_i]))
		next
		ls_sql_add += g_str.format(" and DET.cod_prodotto in ($1) ", ls_str)
	end if
end if


ls_sql = ls_sql1 + ls_sql_add + " and " + ib_where_origine

ll_ret = setsqlselect(ls_sql)
if ll_ret < 1 then
	g_mb.error("Errore nell'impostazione SQL della pagina dei risultati.")
	return
end if

triggerevent("ue_retrieve")

return 
end event

event ue_retrieve();long ll_errore, ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_progr_det_produzione
string	ls_cod_reparto, ls_flag_fine_fase, ls_filter
datetime ldt_data_fine_fase, ldt_data_ricezione_telo, ldt_inizio_sessione

settransobject( sqlca )
setredraw(false)
setfilter("")

ll_errore = retrieve()
if ll_errore < 0 then
   pcca.error = c_fatal
end if

setredraw(true)

end event

event buttonclicked;call super::buttonclicked;if dwo.name="b_selezione" then
	long ll_i
	if dwo.text="SEL" then
		for ll_i = 1 to this.rowcount()
			setitem(ll_i,"selezione","S")
		next
		this.object.b_selezione.text = "SEL*"
	elseif dwo.text="SEL*" then
		for ll_i = 1 to this.rowcount()
			setitem(ll_i,"selezione","N")
		next
		this.object.b_selezione.text = "SEL"
	end if
end if

end event

type tabpage_calendario from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 4695
integer height = 2592
long backcolor = 12632256
string text = "CALENDARIO"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_day_simulazione dw_day_simulazione
dw_day_attuale dw_day_attuale
dw_simulazione dw_simulazione
dw_cal_attuale dw_cal_attuale
dw_calendario dw_calendario
end type

on tabpage_calendario.create
this.dw_day_simulazione=create dw_day_simulazione
this.dw_day_attuale=create dw_day_attuale
this.dw_simulazione=create dw_simulazione
this.dw_cal_attuale=create dw_cal_attuale
this.dw_calendario=create dw_calendario
this.Control[]={this.dw_day_simulazione,&
this.dw_day_attuale,&
this.dw_simulazione,&
this.dw_cal_attuale,&
this.dw_calendario}
end on

on tabpage_calendario.destroy
destroy(this.dw_day_simulazione)
destroy(this.dw_day_attuale)
destroy(this.dw_simulazione)
destroy(this.dw_cal_attuale)
destroy(this.dw_calendario)
end on

type dw_day_simulazione from datawindow within tabpage_calendario
integer x = 1970
integer y = 1708
integer width = 1874
integer height = 860
integer taborder = 50
string title = "none"
string dataobject = "d_carica_cal_produzione_det"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_day_attuale from datawindow within tabpage_calendario
integer x = 50
integer y = 1708
integer width = 1851
integer height = 860
integer taborder = 40
string title = "none"
string dataobject = "d_carica_cal_produzione_det"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_simulazione from datawindow within tabpage_calendario
integer x = 1970
integer y = 328
integer width = 1554
integer height = 1360
integer taborder = 40
string title = "none"
string dataobject = "d_genera_cal_produzione_simulazione"
boolean border = false
boolean livescroll = true
end type

event doubleclicked;choose case dwo.name
	case "compute_1"
		string		ls_cod_reparto
		
		ls_cod_reparto = getitemstring(row, "cod_reparto")
		
		if not isnull(ls_cod_reparto) and len(ls_cod_reparto) > 0 then
			tab_1.tabpage_calendario.dw_day_attuale.setfilter( g_str.format("cod_reparto ='$1'",ls_cod_reparto ) )
			tab_1.tabpage_calendario.dw_day_attuale.filter()
			
			tab_1.tabpage_calendario.dw_day_simulazione.setfilter( g_str.format("cod_reparto ='$1'",ls_cod_reparto ) )
			tab_1.tabpage_calendario.dw_day_simulazione.filter()
		end if
		
end choose
end event

type dw_cal_attuale from datawindow within tabpage_calendario
integer x = 50
integer y = 328
integer width = 1851
integer height = 1360
integer taborder = 30
string title = "none"
string dataobject = "d_genera_cal_produzione_impegno"
boolean border = false
boolean livescroll = true
end type

event doubleclicked;choose case dwo.name
	case "compute_1"
		string		ls_cod_reparto
		
		ls_cod_reparto = getitemstring(row, "cod_reparto")
		
		if not isnull(ls_cod_reparto) and len(ls_cod_reparto) > 0 then
			tab_1.tabpage_calendario.dw_day_attuale.setfilter( g_str.format("cod_reparto ='$1'",ls_cod_reparto ) )
			tab_1.tabpage_calendario.dw_day_attuale.filter()
			
			tab_1.tabpage_calendario.dw_day_simulazione.setfilter( g_str.format("cod_reparto ='$1'",ls_cod_reparto ) )
			tab_1.tabpage_calendario.dw_day_simulazione.filter()
end if
		
end choose
end event

type dw_calendario from datawindow within tabpage_calendario
integer x = 50
integer y = 28
integer width = 3497
integer height = 280
integer taborder = 30
string title = "none"
string dataobject = "d_genera_cal_produzione_controllo"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;long	ll_i
string ls_reparti[], ls_errore

accepttext()
dw_cal_attuale.settransobject(sqlca)
dw_day_attuale.settransobject(sqlca)

if isnull(getitemdatetime(getrow(),"data_pronto")) then
	g_mb.show("Selezionare una data valida")
	return
end if

dw_cal_attuale.retrieve(s_cs_xx.cod_azienda, getitemdatetime(getrow(),"data_pronto"))
dw_day_attuale.retrieve(s_cs_xx.cod_azienda, getitemdatetime(getrow(),"data_pronto"))

tab_1.tabpage_calendario.dw_cal_attuale.modify(g_str.format('compute_3.background.color="536870912~tif( minuti_effettivi >minuti_reparto , $1, if( minuti_reparto= minuti_effettivi , $2, $3 ) )" ', ib_oltre_soglia, ib_soglia, ib_disponibile))
tab_1.tabpage_calendario.dw_simulazione.modify(g_str.format('compute_3.background.color="536870912~tif( minuti_effettivi >minuti_reparto , $1, if( minuti_reparto= minuti_effettivi , $2, $3 ) )" ', ib_oltre_soglia, ib_soglia, ib_disponibile))

uo_calendario_prod_new luo_cal
luo_cal = create uo_calendario_prod_new

for ll_i = 1 to tab_1.tabpage_selezione.dw_1.rowcount()
	if tab_1.tabpage_selezione.dw_1.getitemstring(ll_i, "selezione") = "S" then
		luo_cal.uof_aggiorna_varianti_e_calendario( getitemdatetime(getrow(),"data_pronto"), &
																tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i, "anno_registrazione"), &
																tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i, "num_registrazione"), &
																tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i, "prog_riga_ord_ven"), &
																ref ls_reparti[], &
																ref ls_errore)
	end if
next
dw_simulazione.settransobject(sqlca)
dw_day_simulazione.settransobject(sqlca)
dw_simulazione.retrieve(s_cs_xx.cod_azienda, getitemdatetime(getrow(),"data_pronto"))
dw_day_simulazione.retrieve(s_cs_xx.cod_azienda, getitemdatetime(getrow(),"data_pronto"))

destroy luo_cal
		
choose case dwo.name
	case "b_verifica"
		rollback;
		this.object.b_pianifica.enabled='1'
	case "b_pianifica"
		commit;
		this.object.b_pianifica.enabled='0'
		this.object.b_verifica.enabled='0'
end choose
end event

type dw_prodotti from uo_dddw_checkbox within w_genera_cal_produzione
integer x = 297
integer y = 660
integer width = 1874
integer height = 1140
integer taborder = 30
end type

