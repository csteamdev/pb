﻿$PBExportHeader$w_gestione_ologrammi.srw
forward
global type w_gestione_ologrammi from window
end type
type st_2 from statictext within w_gestione_ologrammi
end type
type st_1 from statictext within w_gestione_ologrammi
end type
type cb_cod_servizio from commandbutton within w_gestione_ologrammi
end type
type cb_annulla from commandbutton within w_gestione_ologrammi
end type
type st_riga_ordine from statictext within w_gestione_ologrammi
end type
type st_titolo from statictext within w_gestione_ologrammi
end type
type st_dw_buffer from statictext within w_gestione_ologrammi
end type
type dw_ologramma from datawindow within w_gestione_ologrammi
end type
type dw_buffer from datawindow within w_gestione_ologrammi
end type
type r_1 from rectangle within w_gestione_ologrammi
end type
end forward

global type w_gestione_ologrammi from window
integer width = 3301
integer height = 2460
boolean titlebar = true
string title = "Gestione Eichette Ologrammi"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
event ue_close_ko ( )
event ue_close_ok ( )
st_2 st_2
st_1 st_1
cb_cod_servizio cb_cod_servizio
cb_annulla cb_annulla
st_riga_ordine st_riga_ordine
st_titolo st_titolo
st_dw_buffer st_dw_buffer
dw_ologramma dw_ologramma
dw_buffer dw_buffer
r_1 r_1
end type
global w_gestione_ologrammi w_gestione_ologrammi

type variables
integer			ii_anno_registrazione, ii_qta, ii_verificati
long				il_num_registrazione, il_prog_riga_ord_ven, il_barcode
string				is_tipo_dati, is_tipo_gestione, is_cod_operaio, is_nome_operaio
string				is_codice_servizio = "******"
end variables

forward prototypes
public subroutine wf_imposta_buffer ()
public function integer wf_verifica_assegnazione (string as_codice, ref string as_messaggio)
public function integer wf_get_operaio (string as_codice, ref string as_operaio)
public subroutine wf_assegna_progressivo ()
public function integer wf_prima_assegnazione (string as_codice, ref string as_messaggio)
public subroutine wf_codice_servizio ()
public function integer wf_check_riga (string as_codice, string as_clausola)
end prototypes

event ue_close_ko();s_cs_xx_parametri			lstr_parametri

lstr_parametri.parametro_b_1 = false

closewithreturn(this, lstr_parametri)
end event

event ue_close_ok();s_cs_xx_parametri			lstr_parametri

lstr_parametri.parametro_b_1 = true

closewithreturn(this, lstr_parametri)
end event

public subroutine wf_imposta_buffer ();dw_buffer.reset()
dw_buffer.insertrow(0)
dw_buffer.setfocus()
end subroutine

public function integer wf_verifica_assegnazione (string as_codice, ref string as_messaggio);uo_produzione			luo_prod
integer					li_ret
string						ls_riparazione
long						ll_new,  ll_upd
boolean					lb_new


//verifica se il codice è assegnato ad altra riga
luo_prod = create uo_produzione
li_ret = luo_prod.uof_check_ologramma_riga(as_codice, ii_anno_registrazione, il_num_registrazione, il_prog_riga_ord_ven, as_messaggio)
destroy luo_prod

//se -1 è un errore critico
//se li_ret è 			1 codice già assegnato ad altra riga ma è da riparare, metti a VERIFICATO e RIPARAZIONE e fai INSERT
//se vale 			2 codice già assegnato ad altra riga ma NON è da riparare ANNULLA
//se invece vale 	0 allora tutto normale, cioè metti a VERIFICATO e fai UPDATE
//se vale				3 Nuovo numero ologramma emesso, metti a VERIFICATO e fai INSERT

choose case li_ret
	case is < 0
		//errore critico
		g_mb.error(as_messaggio)
		wf_imposta_buffer()
		return -1
		
	case 2
		//codice già assegnato ad altra riga ma NON è da riparare
		//oppure Nuovo numero ologramma annullato dall'operatore: NULLA DI FATTO
		g_mb.warning(as_messaggio)
		wf_imposta_buffer()
		return 1
		
		
	case 1
		//codice già assegnato ad altra riga ma è da riparare, metti a VERIFICATO e RIPARAZIONE e fai INSERT
		ls_riparazione = "S"
		lb_new = true
		
	
	case 3
		//Nuovo numero ologramma emesso, metti a VERIFICATO e fai INSERT
		
		//ma prima verifica se per caso non è gia presente sulla dw (solo se diverso da quello di servizio)
		if as_codice<>is_codice_servizio  then
			ll_new = dw_ologramma.Find("upper(cod_ologramma)='"+upper(as_codice)+"'",1 , dw_ologramma.RowCount())
			
			if ll_new>0 then
				//trovato: già presente!!!!
				g_mb.warning("Codice Ologramma '"+as_codice+"' già verificato in questo passaggio!")
				wf_imposta_buffer()
				return 1
			else
				//non trovato: OK puoi continuare!
			end if
			
		end if
		
		ls_riparazione = "N"
		lb_new = true
		
	case else
		//tutto normale
		
		ls_riparazione = "N"
		lb_new = false
		
		if as_codice=is_codice_servizio then
			//becca il primo non verificato
			ll_new = dw_ologramma.Find("upper(cod_ologramma)='"+upper(as_codice)+"' and flag_verificato<>'S'",1 , dw_ologramma.RowCount())
		else
			ll_new = dw_ologramma.Find("upper(cod_ologramma)='"+upper(as_codice)+"'",1 , dw_ologramma.RowCount())
		end if
		
		if ll_new>0 then
			
			if as_codice<>is_codice_servizio  then
				//PRIMA DI ANDARE AVANTI VERIFICA CASO MAI HAI GIA' SCANSIONATO IN QUESTA TORNATA E QUINDI E' GIA' VERIFICATO
				if dw_ologramma.getitemstring(ll_new, "flag_verificato") = "S" then
					g_mb.warning("Codice Ologramma '"+as_codice+"' già verificato in questo passaggio!")
					wf_imposta_buffer()
					return 1
				end if
			end if
			
			ii_verificati += 1
			dw_ologramma.setitem(ll_new, "operaio_verifica",			is_cod_operaio)
			dw_ologramma.setitem(ll_new, "nome_operaio_verifica",	is_nome_operaio)
			dw_ologramma.setitem(ll_new, "data_verifica",				datetime(today(), now()))
			dw_ologramma.setitem(ll_new, "flag_verificato",				"S")
			dw_ologramma.setitem(ll_new, "flag_riparazione",			ls_riparazione)
			
			dw_ologramma.scrolltorow(ll_new)
			
		else
			//non dovrebbe capitare, strano, ma segnalo
			g_mb.error("Codice Ologramma '"+as_codice+"' non trovato nella finestra corrente!")
			wf_imposta_buffer()
			return 1
		end if
		
end choose


if lb_new then
	//se arrivi qui devi inserire la riga (nuovo codice o codice già assegnato ad altri ma da riparare) -------------------------------------------
	ll_new = dw_ologramma.insertrow(0)
	dw_ologramma.setitem(ll_new, "cod_azienda",					s_cs_xx.cod_azienda)
	dw_ologramma.setitem(ll_new, "anno_registrazione",		ii_anno_registrazione)
	dw_ologramma.setitem(ll_new, "num_registrazione",			il_num_registrazione)
	dw_ologramma.setitem(ll_new, "prog_riga_ord_ven",		il_prog_riga_ord_ven)
	dw_ologramma.setitem(ll_new, "cod_ologramma",			as_codice)
	dw_ologramma.setitem(ll_new, "operaio_verifica",			is_cod_operaio)
	dw_ologramma.setitem(ll_new, "nome_operaio_verifica",	is_nome_operaio)
	dw_ologramma.setitem(ll_new, "data_verifica",				datetime(today(), now()))
	dw_ologramma.setitem(ll_new, "flag_verificato",				"S")
	dw_ologramma.setitem(ll_new, "flag_riparazione",			ls_riparazione)
	dw_ologramma.scrolltorow(ll_new)
	ii_verificati += 1
end if

if ii_verificati=ii_qta then
	//finito, hai verificato la quantità stabilita!
	
	dw_ologramma.accepttext()
	
	wf_assegna_progressivo()
	
	if dw_ologramma.update() > 0 then
		//g_mb.success("Procedura verifica ologrammi terminata con successo!")
		postevent("ue_close_ok")
	else
		g_mb.error("Errore in update dati!")
		wf_imposta_buffer()
		return -1
	end if
	
else
	wf_imposta_buffer()
	return 0
end if

end function

public function integer wf_get_operaio (string as_codice, ref string as_operaio);
  
select cognome + ' ' + nome
into :as_operaio
from anag_operai
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_operaio=:as_codice;

if isnull(as_operaio) then as_operaio=""

return 0
end function

public subroutine wf_assegna_progressivo ();long ll_prog, ll_index

select max(progressivo)
into :ll_prog
from det_ord_ven_ologramma
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ii_anno_registrazione and
			num_registrazione=:il_num_registrazione and
			prog_riga_ord_ven=:il_prog_riga_ord_ven;
			
if isnull(ll_prog) then ll_prog = 0

for ll_index=1 to dw_ologramma.rowcount()
	if isnull(dw_ologramma.getitemnumber(ll_index, "progressivo")) or dw_ologramma.getitemnumber(ll_index, "progressivo")<=0 then
		ll_prog += 1
		
		dw_ologramma.setitem(ll_index, "progressivo", ll_prog)
	end if
next
end subroutine

public function integer wf_prima_assegnazione (string as_codice, ref string as_messaggio);uo_produzione			luo_prod
integer					li_ret
string						ls_riparazione, ls_temp, ls_nome_operaio
long						ll_new, ll_index


if as_codice<>is_codice_servizio  then
	//verifica se il codice è già assegnato (es. per sbaglio hai riscansionato lo stesso codice e quindi è già sulla dw - non in tabella)
	for ll_index=1 to dw_ologramma.rowcount()
		ls_temp = dw_ologramma.getitemstring(ll_index, "cod_ologramma")
		if ls_temp=as_codice then
			g_mb.warning("Codice "+ls_temp+" già scansionato!")
			wf_imposta_buffer()
			return 1
		end if
	next
end if


//verifica se il codice è assegnato ad altra riga
luo_prod = create uo_produzione
li_ret = luo_prod.uof_check_ologramma_altra_riga(as_codice, ii_anno_registrazione, il_num_registrazione, il_prog_riga_ord_ven, as_messaggio)
destroy luo_prod

//se -1 è un errore critico
//se li_ret è 1, allora vuol dire che l'operatore dice che si tratta di una riparazione, ok va avanti ma segna che è riparazione ...
//se vale 2 allora si cerca di assegnare un codice già assegnato ma non è una riparazione: impediscilo
//se invece vale 0 allora tutto normale

choose case li_ret
	case is < 0
		//errore critico
		g_mb.error(as_messaggio)
		wf_imposta_buffer()
		return -1
		
	case 2
		//si cerca di assegnare un codice già assegnato ma non è una riparazione: impediscilo
		g_mb.warning(as_messaggio)
		wf_imposta_buffer()
		return 1
		
	case 1
		//codice già assegnato ma è una RIPARAZIONE: OK
		ls_riparazione = "S"
	
	case else
		//tutto normale
		ls_riparazione = "N"
	
end choose


ll_new = dw_ologramma.insertrow(0)
dw_ologramma.setitem(ll_new, "cod_azienda",					s_cs_xx.cod_azienda)
dw_ologramma.setitem(ll_new, "anno_registrazione",		ii_anno_registrazione)
dw_ologramma.setitem(ll_new, "num_registrazione",			il_num_registrazione)
dw_ologramma.setitem(ll_new, "prog_riga_ord_ven",		il_prog_riga_ord_ven)
dw_ologramma.setitem(ll_new, "cod_ologramma",			as_codice)
dw_ologramma.setitem(ll_new, "data_assegnazione",		datetime(today(), now()))
dw_ologramma.setitem(ll_new, "operaio_assegnazione",	is_cod_operaio)
dw_ologramma.setitem(ll_new, "nome_operaio_assegna",	is_nome_operaio)
dw_ologramma.setitem(ll_new, "flag_riparazione",			ls_riparazione)
dw_ologramma.scrolltorow(ll_new)

if dw_ologramma.rowcount()=ii_qta then
	//finito, hai assegnato per la quantità stabilita!
	
	dw_ologramma.accepttext()
	
	wf_assegna_progressivo()
	
	if dw_ologramma.update() > 0 then
		//g_mb.success("Procedura assegnazione iniziale ologrammi terminata con successo!")
		postevent("ue_close_ok")
	else
		g_mb.error("Errore in update dati!")
		wf_imposta_buffer()
		return -1
	end if
	
else
	wf_imposta_buffer()
	return 0
end if
end function

public subroutine wf_codice_servizio ();long ll_rows, ll_index
string ls_errore

if is_tipo_gestione = "A" then
	
	if not g_mb.confirm("Vuoi effettuare la prima assegnazione con il codice di servizio (Caso Riparazione e NON esiste il vecchio codice ologramma?)") then return
	
	//prima assegnazione
	for ll_index=1 to ii_qta
		if wf_prima_assegnazione(is_codice_servizio, ls_errore) <> 0 then return
	next
	
elseif is_tipo_gestione = "V" then
	
	if not g_mb.confirm("Vuoi effettuare la verifica con il codice di servizio (Caso Riparazione e NON esiste il vecchio codice ologramma?)") then return
	
	//verifica
	for ll_index=1 to ii_qta
		wf_verifica_assegnazione(is_codice_servizio, ls_errore)
	next
end if
end subroutine

public function integer wf_check_riga (string as_codice, string as_clausola);return 0
//
//
//ll_new = dw_ologramma.Find(as_clausola, 1 , dw_ologramma.RowCount())
//
//if ll_new>0 then
//	
//	if as_codice<>is_codice_servizio  then
//		//PRIMA DI ANDARE AVANTI VERIFICA CASO MAI HAI GIA' SCANSIONATO IN QUESTA TORNATA E QUINDI E' GIA' VERIFICATO
//		if dw_ologramma.getitemstring(ll_new, "flag_verificato") = "S" then
//			g_mb.warning("Codice Ologramma '"+as_codice+"' già verificato in questo passaggio!")
//			wf_imposta_buffer()
//			return -1
//		end if
//	end if
//	
//	ii_verificati += 1
//	dw_ologramma.setitem(ll_new, "operaio_verifica",			is_cod_operaio)
//	dw_ologramma.setitem(ll_new, "nome_operaio_verifica",	is_nome_operaio)
//	dw_ologramma.setitem(ll_new, "data_verifica",				datetime(today(), now()))
//	dw_ologramma.setitem(ll_new, "flag_verificato",				"S")
//	dw_ologramma.setitem(ll_new, "flag_riparazione",			ls_riparazione)
//	
//	dw_ologramma.scrolltorow(ll_new)
//	
//	return 1
//	
//else
//	//non dovrebbe capitare, strano, ma segnalo
//	g_mb.error("Codice Ologramma '"+as_codice+"' non trovato nella finestra corrente!")
//	wf_imposta_buffer()
//	return -1
//end if
end function

event open;s_cs_xx_parametri		lstr_parametri
dec{4}						ld_quan_ordine
string							ls_cod_reparto


lstr_parametri = message.powerobjectparm

//tipo gestione
//""		visualizza
//A		prima assegnazione
//V		verifica
is_tipo_gestione = lstr_parametri.parametro_s_1

//B= barcode produzione			O=riga ordine vendita
is_tipo_dati = lstr_parametri.parametro_s_2

is_cod_operaio = lstr_parametri.parametro_s_3

wf_get_operaio(is_cod_operaio, is_nome_operaio)

if is_tipo_dati = "O" then
	ii_anno_registrazione = lstr_parametri.parametro_d_1_a[1]
	il_num_registrazione = lstr_parametri.parametro_d_1_a[2]
	il_prog_riga_ord_ven = lstr_parametri.parametro_d_1_a[3]
	ii_qta = integer(lstr_parametri.parametro_d_1_a[4])
	setnull(il_barcode)
	
else
	setnull(ii_anno_registrazione)
	setnull(il_num_registrazione)
	setnull(il_prog_riga_ord_ven)
	il_barcode = lstr_parametri.parametro_d_1_a[1]
	ii_qta = integer(lstr_parametri.parametro_d_1_a[2])
	
	//si tratta sempre di ordini tipo report 1 quindi leggo le info dalla det_ordini_produzione
	select anno_registrazione,
			 num_registrazione,
			 prog_riga_ord_ven,
			 cod_reparto
	into	:ii_anno_registrazione,
			:il_num_registrazione,
			:il_prog_riga_ord_ven,
			:ls_cod_reparto
	from   det_ordini_produzione
	where		cod_azienda=:s_cs_xx.cod_azienda and
				progr_det_produzione =:il_barcode;
	
	if sqlca.sqlcode < 0 then 
		g_mb.error("Attenzione!","Errore lettura info riga ordine per barcode "+string(il_barcode)+" :" + sqlca.sqlerrtext)
		postevent("ue_close_ko")
		return
	
	elseif sqlca.sqlcode = 100 then
		g_mb.error("Attenzione!","Nessuna corrispondenza tra le righe ordini vendita per il  barcode "+string(il_barcode)+"!")
		postevent("ue_close_ko")
		return
	end if

end if


st_riga_ordine.text = "Riga Ordine: "+string(ii_anno_registrazione)+"/"+string(il_num_registrazione)+"/"+string(il_prog_riga_ord_ven)



dw_ologramma.settransobject(sqlca)
dw_ologramma.retrieve(	s_cs_xx.cod_azienda, &
								ii_anno_registrazione, &
								il_num_registrazione, &
								il_prog_riga_ord_ven)

choose case is_tipo_gestione
	case "V"
		//in fase di verifica la quantità da considerare è la quantità prodotta in tale fase (avanzamento parziale o chiusura fase)
		//passo quasta quantità in 
		st_titolo.text = "Verifica Assegnazione Ologrammi"
		st_riga_ordine.text += " - Q.tà Prodotta : "+string(ii_qta, "###,###,##0.0")
		st_dw_buffer.visible=true
		dw_buffer.visible = true
		wf_imposta_buffer()
		
	case "A"
		st_dw_buffer.visible=true
		dw_buffer.visible = true
		st_titolo.text = "Prima Assegnazione Ologrammi"
		st_riga_ordine.text += " - Q.tà da Produrre : "+string(ii_qta, "###,###,##0.0")
		wf_imposta_buffer()
		
		
	case else
		st_titolo.text = "Visualizzazione Ologrammi"
		
end choose




end event

on w_gestione_ologrammi.create
this.st_2=create st_2
this.st_1=create st_1
this.cb_cod_servizio=create cb_cod_servizio
this.cb_annulla=create cb_annulla
this.st_riga_ordine=create st_riga_ordine
this.st_titolo=create st_titolo
this.st_dw_buffer=create st_dw_buffer
this.dw_ologramma=create dw_ologramma
this.dw_buffer=create dw_buffer
this.r_1=create r_1
this.Control[]={this.st_2,&
this.st_1,&
this.cb_cod_servizio,&
this.cb_annulla,&
this.st_riga_ordine,&
this.st_titolo,&
this.st_dw_buffer,&
this.dw_ologramma,&
this.dw_buffer,&
this.r_1}
end on

on w_gestione_ologrammi.destroy
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_cod_servizio)
destroy(this.cb_annulla)
destroy(this.st_riga_ordine)
destroy(this.st_titolo)
destroy(this.st_dw_buffer)
destroy(this.dw_ologramma)
destroy(this.dw_buffer)
destroy(this.r_1)
end on

type st_2 from statictext within w_gestione_ologrammi
integer x = 119
integer y = 2084
integer width = 1893
integer height = 216
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Cliccando sul pulsante viene cancellata ogni eventuale precedente assegnazione, assegnando un numero di codici di servizio pari alla quantità riga ordine"
boolean focusrectangle = false
end type

type st_1 from statictext within w_gestione_ologrammi
integer x = 119
integer y = 1944
integer width = 1893
integer height = 156
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Da usare per gli ordini che sono di riparazione, ma non esiste il rispettivo codice ologramma originale del~'ordine precedente."
boolean focusrectangle = false
end type

type cb_cod_servizio from commandbutton within w_gestione_ologrammi
integer x = 2103
integer y = 1980
integer width = 1079
integer height = 236
integer taborder = 10
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "USA CODICE SERVIZIO"
end type

event clicked;

wf_codice_servizio()
end event

type cb_annulla from commandbutton within w_gestione_ologrammi
integer x = 2103
integer y = 268
integer width = 1079
integer height = 236
integer taborder = 20
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "ANNULLA OPERAZIONE"
end type

event clicked;string ls_messaggio

if is_tipo_gestione="A" then
	//prima assegnazione
	ls_messaggio = "(Sarà possibile effettuare la prima assegnazione in chiusura fase)"
	
elseif is_tipo_gestione="V" then
	//verifica assegnazione
	ls_messaggio = "(Rispondendo SI la fase non verrà chiusa!)"
	
end if


if g_mb.confirm("Annullare il processo di verifica? "+ ls_messaggio) then
	parent.postevent("ue_close_ko")
end if
end event

type st_riga_ordine from statictext within w_gestione_ologrammi
integer x = 279
integer y = 144
integer width = 2770
integer height = 108
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Riga Ordine:"
boolean focusrectangle = false
end type

type st_titolo from statictext within w_gestione_ologrammi
integer x = 279
integer y = 28
integer width = 2770
integer height = 108
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_dw_buffer from statictext within w_gestione_ologrammi
boolean visible = false
integer x = 41
integer y = 336
integer width = 613
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Barcode Ologramma:"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_ologramma from datawindow within w_gestione_ologrammi
integer x = 46
integer y = 524
integer width = 3218
integer height = 1360
integer taborder = 20
string title = "none"
string dataobject = "d_gestione_ologrammi"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_buffer from datawindow within w_gestione_ologrammi
boolean visible = false
integer x = 41
integer y = 416
integer width = 613
integer height = 96
integer taborder = 10
string title = "none"
string dataobject = "d_buffer"
boolean border = false
boolean livescroll = true
end type

event itemchanged;string			ls_errore

if row>0 then
else
	return
end if

choose case dwo.name
	case "buffer"
		if data <>"" then
		else
			return
		end if
		
		if is_tipo_gestione="A" then
			//prima assegnazione
			wf_prima_assegnazione(data, ls_errore)
			
	
			
		elseif is_tipo_gestione="V" then
			//verifica assegnazione
			wf_verifica_assegnazione(data, ls_errore)
			
		end if
		
end choose
end event

type r_1 from rectangle within w_gestione_ologrammi
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 46
integer y = 1900
integer width = 3237
integer height = 460
end type

