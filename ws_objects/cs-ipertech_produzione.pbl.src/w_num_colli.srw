﻿$PBExportHeader$w_num_colli.srw
$PBExportComments$Finestra per l'inserimento del numero di colli
forward
global type w_num_colli from w_cs_xx_risposta
end type
type sle_posizione from singlelineedit within w_num_colli
end type
type st_6 from statictext within w_num_colli
end type
type st_5 from statictext within w_num_colli
end type
type st_1 from statictext within w_num_colli
end type
type em_num_colli from editmask within w_num_colli
end type
type st_3 from statictext within w_num_colli
end type
type cb_1 from commandbutton within w_num_colli
end type
type cb_annulla from commandbutton within w_num_colli
end type
end forward

global type w_num_colli from w_cs_xx_risposta
integer width = 2405
integer height = 1024
string title = "INSERIRE QUANTITA~'"
boolean controlmenu = false
sle_posizione sle_posizione
st_6 st_6
st_5 st_5
st_1 st_1
em_num_colli em_num_colli
st_3 st_3
cb_1 cb_1
cb_annulla cb_annulla
end type
global w_num_colli w_num_colli

type variables
integer			ii_ok

long				il_barcode

string				is_posizione_old
end variables

on w_num_colli.create
int iCurrent
call super::create
this.sle_posizione=create sle_posizione
this.st_6=create st_6
this.st_5=create st_5
this.st_1=create st_1
this.em_num_colli=create em_num_colli
this.st_3=create st_3
this.cb_1=create cb_1
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.sle_posizione
this.Control[iCurrent+2]=this.st_6
this.Control[iCurrent+3]=this.st_5
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.em_num_colli
this.Control[iCurrent+6]=this.st_3
this.Control[iCurrent+7]=this.cb_1
this.Control[iCurrent+8]=this.cb_annulla
end on

on w_num_colli.destroy
call super::destroy
destroy(this.sle_posizione)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_1)
destroy(this.em_num_colli)
destroy(this.st_3)
destroy(this.cb_1)
destroy(this.cb_annulla)
end on

event open;call super::open;ii_ok=-1




il_barcode = long(s_cs_xx.parametri.parametro_s_12)
s_cs_xx.parametri.parametro_s_12 = ""

select posizione
into   :is_posizione_old
from   tes_ordini_produzione
where	cod_azienda =:s_cs_xx.cod_azienda and
			progr_tes_produzione=:il_barcode;

if isnull(is_posizione_old) then is_posizione_old=""
sle_posizione.text = is_posizione_old


//em_quan_prodotta.text=string(s_cs_xx.parametri.parametro_d_2)
em_num_colli.setfocus()
end event

event closequery;call super::closequery;if ii_ok=-1 then return 1

end event

type sle_posizione from singlelineedit within w_num_colli
integer x = 1330
integer y = 452
integer width = 983
integer height = 160
integer taborder = 20
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
integer limit = 5
end type

type st_6 from statictext within w_num_colli
integer x = 791
integer y = 492
integer width = 517
integer height = 84
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "(max 5 caratteri)"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_5 from statictext within w_num_colli
integer x = 27
integer y = 444
integer width = 768
integer height = 180
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Posizione"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_num_colli
integer x = 23
integer y = 40
integer width = 2318
integer height = 180
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 12632256
string text = "INSERIRE QUANTITA~'"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_num_colli from editmask within w_num_colli
integer x = 1330
integer y = 236
integer width = 983
integer height = 160
integer taborder = 10
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
alignment alignment = right!
string mask = "########"
string displaydata = "~r"
end type

type st_3 from statictext within w_num_colli
integer x = 37
integer y = 236
integer width = 1266
integer height = 180
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Numero colli"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_num_colli
integer x = 1326
integer y = 660
integer width = 754
integer height = 220
integer taborder = 40
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
boolean default = true
end type

event clicked;string				ls_posizione

ls_posizione = sle_posizione.text
if isnull(ls_posizione) then ls_posizione=""

if ls_posizione<>"" and is_posizione_old<>"" then
	if is_posizione_old<>ls_posizione then
		if g_mb.confirm("La posizione indicata ("+ls_posizione+") è diversa da quella precedentemente memorizzata per questa fase ("+is_posizione_old+"). "+&
								"Vuoi sovrascrivere? [SI=CAMBIA - NO=LASCIA IL PRECEDENTE]") then
			//sovrascrivi				
		else
			//lascia inalterato
			ls_posizione = is_posizione_old
		end if
	end if
end if

s_cs_xx.parametri.parametro_d_2 = double(em_num_colli.text)
s_cs_xx.parametri.parametro_s_12 = ls_posizione
s_cs_xx.parametri.parametro_i_1 = 1


ii_ok=0

close (parent)

end event

type cb_annulla from commandbutton within w_num_colli
event clicked pbm_bnclicked
integer x = 297
integer y = 660
integer width = 754
integer height = 220
integer taborder = 30
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = 0
ii_ok = 0
close (parent)
end event

