﻿$PBExportHeader$w_ordine_conto_dep.srw
forward
global type w_ordine_conto_dep from w_cs_xx_risposta
end type
type st_error from statictext within w_ordine_conto_dep
end type
type st_2 from statictext within w_ordine_conto_dep
end type
type sle_anno_numero from singlelineedit within w_ordine_conto_dep
end type
type st_anno from statictext within w_ordine_conto_dep
end type
type cb_2 from commandbutton within w_ordine_conto_dep
end type
end forward

global type w_ordine_conto_dep from w_cs_xx_risposta
integer width = 1888
integer height = 736
string title = "Anno/Numero Ordine"
st_error st_error
st_2 st_2
sle_anno_numero sle_anno_numero
st_anno st_anno
cb_2 cb_2
end type
global w_ordine_conto_dep w_ordine_conto_dep

type variables

/**
 * Recupera: 
 *		anno / numero
 *		annonumero
 **/
constant string ANNO_NUMERO_PATTERN = "^([0-9]{4})\/?([0-9]+)$"

private: 
	s_cs_xx_parametri istr_data
	uo_regex iuo_regex
end variables

forward prototypes
public subroutine wf_chiudi_con_parametri (long al_anno, long al_numero, long al_chiusura)
end prototypes

public subroutine wf_chiudi_con_parametri (long al_anno, long al_numero, long al_chiusura);/**
 * stefanop
 * 22/05/2015
 *
 * Imposto i parametri di chiusura della finestra
 *
 * @param al_chiusura: Intero 0 => chiudo senza parmetro, 1 => chiudo impostando anno e numero ordine
 **/
 
istr_data.parametro_ul_1 = al_anno
istr_data.parametro_ul_2 = al_numero
istr_data.parametro_ul_3 = al_chiusura

CloseWithReturn(this, istr_data)
end subroutine

on w_ordine_conto_dep.create
int iCurrent
call super::create
this.st_error=create st_error
this.st_2=create st_2
this.sle_anno_numero=create sle_anno_numero
this.st_anno=create st_anno
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_error
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.sle_anno_numero
this.Control[iCurrent+4]=this.st_anno
this.Control[iCurrent+5]=this.cb_2
end on

on w_ordine_conto_dep.destroy
call super::destroy
destroy(this.st_error)
destroy(this.st_2)
destroy(this.sle_anno_numero)
destroy(this.st_anno)
destroy(this.cb_2)
end on

event pc_setwindow;call super::pc_setwindow;string ls_error

iuo_regex = create uo_regex
iuo_regex.initialize(ANNO_NUMERO_PATTERN, true , false) 
end event

type st_error from statictext within w_ordine_conto_dep
integer x = 46
integer y = 340
integer width = 1783
integer height = 140
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_2 from statictext within w_ordine_conto_dep
integer x = 23
integer y = 20
integer width = 1806
integer height = 160
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Inserire l~'anno ed il numero dell~'ordine per il quali si sta esegundo lo scarico di magazzino"
boolean focusrectangle = false
end type

type sle_anno_numero from singlelineedit within w_ordine_conto_dep
integer x = 480
integer y = 220
integer width = 1326
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

event modified;string ls_val
long ll_anno, ll_numero
int li_pos, li_count

try
	ls_val = sle_anno_numero.text
	li_count =  iuo_regex.search(ls_val) 
	if iuo_regex.search(sle_anno_numero.text) > 0 then 
			
		ll_anno = long(iuo_regex.group(1,1))
		ll_numero = long(iuo_regex.group(1, 2))
	
		select count(*)
		into :li_count
		from tes_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno and
				 num_registrazione = :ll_numero;
			 
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante il controllo dell'esistenza dell'ordine inserito.", sqlca)
		elseif sqlca.sqlcode = 100 or li_count <= 0 then
			st_error.text = "Ordine " + g_str.safe(string(ll_anno)) + "/" + g_str.safe(string(ll_numero)) + " non trovato."
		else
			st_error.text = ""
			wf_chiudi_con_parametri(ll_anno, ll_numero, 1)
		end if
	else 
		st_error.text = "Inserire i valori di anno e numero ordine in uno dei seguenti formati: anno/numero oppure annonumero"
	end if	
catch (RuntimeError r)
	g_mb.error("Errore generico.~r~n" + r.getMessage())
end try
end event

type st_anno from statictext within w_ordine_conto_dep
integer x = 46
integer y = 220
integer width = 411
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Anno / Numero:"
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_ordine_conto_dep
integer x = 869
integer y = 500
integer width = 937
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Procedi senza impostare ordine"
end type

event clicked;wf_chiudi_con_parametri(0,0,0)
end event

