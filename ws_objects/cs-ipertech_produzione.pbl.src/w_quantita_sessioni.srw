﻿$PBExportHeader$w_quantita_sessioni.srw
$PBExportComments$finestra che permette l'inserimento della quantità della sessione
forward
global type w_quantita_sessioni from w_cs_xx_risposta
end type
type st_barcode from statictext within w_quantita_sessioni
end type
type sle_posizione from singlelineedit within w_quantita_sessioni
end type
type st_6 from statictext within w_quantita_sessioni
end type
type st_5 from statictext within w_quantita_sessioni
end type
type st_4 from statictext within w_quantita_sessioni
end type
type cbx_reset_colli from checkbox within w_quantita_sessioni
end type
type dw_colli_reparti_prec from datawindow within w_quantita_sessioni
end type
type st_1 from statictext within w_quantita_sessioni
end type
type em_num_colli from editmask within w_quantita_sessioni
end type
type st_3 from statictext within w_quantita_sessioni
end type
type st_2 from statictext within w_quantita_sessioni
end type
type cb_1 from commandbutton within w_quantita_sessioni
end type
type em_quan_prodotta from editmask within w_quantita_sessioni
end type
type cb_annulla from commandbutton within w_quantita_sessioni
end type
end forward

global type w_quantita_sessioni from w_cs_xx_risposta
integer width = 2418
integer height = 1428
string title = "INSERIRE QUANTITA~'"
boolean controlmenu = false
st_barcode st_barcode
sle_posizione sle_posizione
st_6 st_6
st_5 st_5
st_4 st_4
cbx_reset_colli cbx_reset_colli
dw_colli_reparti_prec dw_colli_reparti_prec
st_1 st_1
em_num_colli em_num_colli
st_3 st_3
st_2 st_2
cb_1 cb_1
em_quan_prodotta em_quan_prodotta
cb_annulla cb_annulla
end type
global w_quantita_sessioni w_quantita_sessioni

type variables
integer			ii_ok

double			id_qta_da_produrre

long				il_barcode

string				is_posizione_old
end variables

on w_quantita_sessioni.create
int iCurrent
call super::create
this.st_barcode=create st_barcode
this.sle_posizione=create sle_posizione
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.cbx_reset_colli=create cbx_reset_colli
this.dw_colli_reparti_prec=create dw_colli_reparti_prec
this.st_1=create st_1
this.em_num_colli=create em_num_colli
this.st_3=create st_3
this.st_2=create st_2
this.cb_1=create cb_1
this.em_quan_prodotta=create em_quan_prodotta
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_barcode
this.Control[iCurrent+2]=this.sle_posizione
this.Control[iCurrent+3]=this.st_6
this.Control[iCurrent+4]=this.st_5
this.Control[iCurrent+5]=this.st_4
this.Control[iCurrent+6]=this.cbx_reset_colli
this.Control[iCurrent+7]=this.dw_colli_reparti_prec
this.Control[iCurrent+8]=this.st_1
this.Control[iCurrent+9]=this.em_num_colli
this.Control[iCurrent+10]=this.st_3
this.Control[iCurrent+11]=this.st_2
this.Control[iCurrent+12]=this.cb_1
this.Control[iCurrent+13]=this.em_quan_prodotta
this.Control[iCurrent+14]=this.cb_annulla
end on

on w_quantita_sessioni.destroy
call super::destroy
destroy(this.st_barcode)
destroy(this.sle_posizione)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.cbx_reset_colli)
destroy(this.dw_colli_reparti_prec)
destroy(this.st_1)
destroy(this.em_num_colli)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.cb_1)
destroy(this.em_quan_prodotta)
destroy(this.cb_annulla)
end on

event open;call super::open;
datastore				lds_colli_reparti_prec
long						ll_tot
string						ls_cod_reparto, ls_flag_reset_colli_default

//numero di righe nel datastore s_cs_xx.parametri.parametro_ds_1
ll_tot = long(s_cs_xx.parametri.parametro_d_5)

if ll_tot>0 then
	
	s_cs_xx.parametri.parametro_ds_1.rowscopy(1, ll_tot, Primary!, dw_colli_reparti_prec, 1, Primary!)
	
	this.height = 2350
	cbx_reset_colli.visible = true
	dw_colli_reparti_prec.visible = true
	
	
	ls_cod_reparto = s_cs_xx.parametri.parametro_s_13
	
	select flag_reset_colli_default
	into :ls_flag_reset_colli_default
	from anag_reparti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_reparto=:ls_cod_reparto;
				
	if ls_flag_reset_colli_default="S" then
		cbx_reset_colli.checked = true
		cb_1.text = "CONFERMA CON RESET"
	else
		cb_1.text = "CONFERMA SENZA RESET"
		
	end if
end if


ii_ok=-1

//questa è la max qta che l'operatore può inserire nella casella
id_qta_da_produrre = s_cs_xx.parametri.parametro_d_3

em_quan_prodotta.text=string(s_cs_xx.parametri.parametro_d_3)
st_barcode.text=s_cs_xx.parametri.parametro_s_12

il_barcode = long(s_cs_xx.parametri.parametro_s_12)
s_cs_xx.parametri.parametro_s_12 = ""

select posizione
into   :is_posizione_old
from   det_ordini_produzione
where	cod_azienda =:s_cs_xx.cod_azienda and
			progr_det_produzione=:il_barcode;

if isnull(is_posizione_old) then is_posizione_old=""
sle_posizione.text = is_posizione_old

//em_quan_prodotta.setfocus()
em_num_colli.setfocus()




end event

event closequery;call super::closequery;if ii_ok=-1 then return 1

end event

event resize;//donato 18/01/2012
//tolto ancestor perchè su questa finestra da solo fastidio
//quando fa il resize i pulsanti e i relativi oggetti devono rimanere delle dimensioni originali ....
end event

type st_barcode from statictext within w_quantita_sessioni
integer x = 23
integer y = 220
integer width = 2309
integer height = 180
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 16711680
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_posizione from singlelineedit within w_quantita_sessioni
integer x = 1326
integer y = 876
integer width = 983
integer height = 160
integer taborder = 30
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
integer limit = 5
end type

type st_6 from statictext within w_quantita_sessioni
integer x = 786
integer y = 916
integer width = 517
integer height = 84
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "(max 5 caratteri)"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_5 from statictext within w_quantita_sessioni
integer x = 46
integer y = 868
integer width = 727
integer height = 180
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Posizione"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_4 from statictext within w_quantita_sessioni
boolean visible = false
integer x = 850
integer y = 900
integer width = 654
integer height = 192
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 255
long backcolor = 12632256
string text = "Oggetti nascosti qui sotto!!! Allarga la window per vederli"
boolean focusrectangle = false
end type

type cbx_reset_colli from checkbox within w_quantita_sessioni
boolean visible = false
integer x = 128
integer y = 1180
integer width = 2135
integer height = 104
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 12632256
string text = "Reset Colli reparti esterni precedenti"
boolean lefttext = true
end type

event clicked;

if this.checked then
	//vuoi effettuare il reset dei colli degli stabilimenti precedenti
	cb_1.text = "CONFERMA CON RESET"
else
	//NON vuoi effettuare il reset dei colli degli stabilimenti precedenti
	cb_1.text = "CONFERMA SENZA RESET"
end if
end event

type dw_colli_reparti_prec from datawindow within w_quantita_sessioni
boolean visible = false
integer x = 46
integer y = 1328
integer width = 2281
integer height = 972
integer taborder = 60
string title = "none"
string dataobject = "d_colli_reparti_prec"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_quantita_sessioni
integer x = 23
integer y = 40
integer width = 2309
integer height = 180
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 12632256
string text = "INSERIRE QUANTITA~'"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_num_colli from editmask within w_quantita_sessioni
integer x = 1326
integer y = 668
integer width = 983
integer height = 160
integer taborder = 20
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
alignment alignment = right!
string mask = "########"
string displaydata = "~r"
end type

type st_3 from statictext within w_quantita_sessioni
integer x = 46
integer y = 668
integer width = 1257
integer height = 180
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Numero colli"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_quantita_sessioni
integer x = 46
integer y = 448
integer width = 1257
integer height = 200
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Quantità Prodotta"
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_quantita_sessioni
integer x = 1582
integer y = 1092
integer width = 754
integer height = 220
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&CONFERMA"
boolean default = true
end type

event clicked;double			ldd_colli, ld_qta_prodotta_digitata
string				ls_posizione


//Donato 10/04/2013: segnalazione di Alberto con relativo ticket
if id_qta_da_produrre > 0 and not isnull(id_qta_da_produrre) then
	ld_qta_prodotta_digitata = double(em_quan_prodotta.text)
	
	if ld_qta_prodotta_digitata > id_qta_da_produrre then
		//non puoi produrre una quantità superiore a quella che risulta da produrre
		g_mb.warning("Controllare la quantità prodotta digitata, in quanto risulta superiore a quella effettivamente da produrre, pari a " + string(id_qta_da_produrre, "#####0.00"))
		return
	end if
end if


ldd_colli = double(em_num_colli.text) 

if (ldd_colli<=0 or isnull(ldd_colli)) and cbx_reset_colli.checked and cbx_reset_colli.visible then
	//non è possibile resetttare i colli dei reparti degli stabilimenti precedenti e conteporaneamente
	//dichiarare un numero colli pari a zero nella propria fase
	
	g_mb.warning("Se hai scelto di resettare il numero dei colli dei reparti dei depositi precedenti, il numero di colli prodotto nella propria fase è obbligatorio!")
	return
end if

ls_posizione = sle_posizione.text
if isnull(ls_posizione) then ls_posizione = ""

if ls_posizione<>"" and is_posizione_old<>"" then
	if is_posizione_old<>ls_posizione then
		if g_mb.confirm("La posizione indicata ("+ls_posizione+") è diversa da quella precedentemente memorizzata per questa fase ("+is_posizione_old+"). "+&
								"Vuoi sovrascrivere? [SI=CAMBIA - NO=LASCIA IL PRECEDENTE]") then
			//sovrascrivi				
		else
			//lascia inalterato
			ls_posizione = is_posizione_old
		end if
	end if
end if


s_cs_xx.parametri.parametro_d_1 = double(em_quan_prodotta.text)
s_cs_xx.parametri.parametro_d_2 = ldd_colli
s_cs_xx.parametri.parametro_s_12 = ls_posizione
s_cs_xx.parametri.parametro_i_1 = 1



//qui memorizzo la scelta di reset colli precedenti di stabilimenti diversi
//se la checkbox non è visibile (caso assenza colli reparti precedenti diversi) sarà comunque FALSE
s_cs_xx.parametri.parametro_b_2 = cbx_reset_colli.visible and cbx_reset_colli.checked

ii_ok=0

close (parent)

end event

type em_quan_prodotta from editmask within w_quantita_sessioni
integer x = 1326
integer y = 448
integer width = 983
integer height = 180
integer taborder = 10
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 16777215
boolean border = false
alignment alignment = right!
string mask = "########.####"
string displaydata = "~r"
end type

type cb_annulla from commandbutton within w_quantita_sessioni
event clicked pbm_bnclicked
integer x = 23
integer y = 1092
integer width = 754
integer height = 220
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&ANNULLA"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = 0
ii_ok = 0
close (parent)
end event

