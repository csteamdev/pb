﻿$PBExportHeader$w_reimposta_colli.srw
forward
global type w_reimposta_colli from w_cs_xx_principale
end type
type em_colli from editmask within w_reimposta_colli
end type
type sle_posizione from singlelineedit within w_reimposta_colli
end type
type st_3 from statictext within w_reimposta_colli
end type
type st_1 from statictext within w_reimposta_colli
end type
type cb_chiudi from commandbutton within w_reimposta_colli
end type
type cb_conferma from commandbutton within w_reimposta_colli
end type
type st_2 from statictext within w_reimposta_colli
end type
type cb_diminuisci from commandbutton within w_reimposta_colli
end type
type cb_aumenta from commandbutton within w_reimposta_colli
end type
end forward

global type w_reimposta_colli from w_cs_xx_principale
integer width = 1637
integer height = 1464
string title = "Reimposta Colli"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
em_colli em_colli
sle_posizione sle_posizione
st_3 st_3
st_1 st_1
cb_chiudi cb_chiudi
cb_conferma cb_conferma
st_2 st_2
cb_diminuisci cb_diminuisci
cb_aumenta cb_aumenta
end type
global w_reimposta_colli w_reimposta_colli

on w_reimposta_colli.create
int iCurrent
call super::create
this.em_colli=create em_colli
this.sle_posizione=create sle_posizione
this.st_3=create st_3
this.st_1=create st_1
this.cb_chiudi=create cb_chiudi
this.cb_conferma=create cb_conferma
this.st_2=create st_2
this.cb_diminuisci=create cb_diminuisci
this.cb_aumenta=create cb_aumenta
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.em_colli
this.Control[iCurrent+2]=this.sle_posizione
this.Control[iCurrent+3]=this.st_3
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.cb_chiudi
this.Control[iCurrent+6]=this.cb_conferma
this.Control[iCurrent+7]=this.st_2
this.Control[iCurrent+8]=this.cb_diminuisci
this.Control[iCurrent+9]=this.cb_aumenta
end on

on w_reimposta_colli.destroy
call super::destroy
destroy(this.em_colli)
destroy(this.sle_posizione)
destroy(this.st_3)
destroy(this.st_1)
destroy(this.cb_chiudi)
destroy(this.cb_conferma)
destroy(this.st_2)
destroy(this.cb_diminuisci)
destroy(this.cb_aumenta)
end on

event open;call super::open;long ll_barcode, ll_count
string ls_tipo, ls_barcode

ls_barcode = s_cs_xx.parametri.parametro_s_5
s_cs_xx.parametri.parametro_s_5 = ""

ll_barcode = long(ls_barcode)

select count(*)
into :ll_count
from tab_ord_ven_colli
where 	cod_azienda=:s_cs_xx.cod_azienda and
			barcode=:ll_barcode;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in lettura colli (tab_ord_ven_colli): "+sqlca.sqlerrtext)
	cb_chiudi.postevent(clicked!)
	return
end if

if isnull(ll_count) then ll_count=0

//if ll_count=0 then
//	g_mb.error("Nessun collo risulta prodotto per questo barcode!")
//	cb_chiudi.postevent(clicked!)
//	return
//end if

em_colli.text = string(ll_count)
sle_posizione.text = s_cs_xx.parametri.parametro_s_12

s_cs_xx.parametri.parametro_s_12 = ""


cb_conferma.setfocus( )
end event

type em_colli from editmask within w_reimposta_colli
integer x = 622
integer y = 404
integer width = 974
integer height = 308
integer taborder = 20
integer textsize = -48
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "####0"
double increment = 1
string minmax = "0~~99999999"
end type

type sle_posizione from singlelineedit within w_reimposta_colli
integer x = 622
integer y = 752
integer width = 974
integer height = 252
integer taborder = 20
integer textsize = -36
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
integer limit = 5
borderstyle borderstyle = stylelowered!
end type

type st_3 from statictext within w_reimposta_colli
integer x = 59
integer y = 940
integer width = 475
integer height = 76
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "max 5 caratteri"
boolean focusrectangle = false
end type

type st_1 from statictext within w_reimposta_colli
integer x = 59
integer y = 808
integer width = 594
integer height = 140
integer textsize = -20
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Posizione"
boolean focusrectangle = false
end type

type cb_chiudi from commandbutton within w_reimposta_colli
boolean visible = false
integer x = 110
integer y = 1172
integer width = 302
integer height = 120
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "close"
end type

event clicked;s_cs_xx.parametri.parametro_s_5 = ""

close(parent)
end event

type cb_conferma from commandbutton within w_reimposta_colli
integer x = 613
integer y = 1052
integer width = 974
integer height = 296
integer taborder = 20
integer textsize = -26
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "CONFERMA"
end type

event clicked;string			ls_posizione


ls_posizione = sle_posizione.text
if isnull(ls_posizione) then ls_posizione=""

if isnull(em_colli.text) or em_colli.text="" then em_colli.text = "0"

s_cs_xx.parametri.parametro_s_5 = em_colli.text
s_cs_xx.parametri.parametro_s_12 = ls_posizione

close(parent)

end event

type st_2 from statictext within w_reimposta_colli
integer x = 613
integer y = 36
integer width = 997
integer height = 256
integer textsize = -36
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "COLLI"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_diminuisci from commandbutton within w_reimposta_colli
integer x = 73
integer y = 404
integer width = 402
integer height = 308
integer taborder = 10
integer textsize = -26
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "-"
end type

event clicked;long ll_colli

if isnull(em_colli.text) or em_colli.text="" then em_colli.text = "0"

ll_colli = long(em_colli.text)

ll_colli -= 1

if ll_colli < 0 then
	//non farlo arrivare SOTTO ZERO!!!!!
else
	em_colli.text = string(ll_colli)
end if

cb_conferma.default = true
cb_conferma.setfocus( )
end event

type cb_aumenta from commandbutton within w_reimposta_colli
integer x = 78
integer y = 60
integer width = 402
integer height = 308
integer taborder = 10
integer textsize = -26
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "+"
end type

event clicked;long ll_colli

if isnull(em_colli.text) or em_colli.text = "" then em_colli.text = "0"

ll_colli = long(em_colli.text)

ll_colli += 1

em_colli.text = string(ll_colli)

cb_conferma.default = true
cb_conferma.setfocus( )
end event

