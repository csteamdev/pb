﻿$PBExportHeader$w_rileva_prod_mrp_buffer_sel.srw
forward
global type w_rileva_prod_mrp_buffer_sel from window
end type
type dw_selezione from datawindow within w_rileva_prod_mrp_buffer_sel
end type
end forward

global type w_rileva_prod_mrp_buffer_sel from window
integer width = 3195
integer height = 2520
boolean titlebar = true
string title = "Seleziona IDs movimenti MRP"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
dw_selezione dw_selezione
end type
global w_rileva_prod_mrp_buffer_sel w_rileva_prod_mrp_buffer_sel

type variables
s_cs_xx_parametri				istr_parametri
end variables

forward prototypes
public subroutine wf_seleziona (string as_flag, long al_row)
public function integer wf_conferma (ref string as_errore)
end prototypes

public subroutine wf_seleziona (string as_flag, long al_row);long			ll_index, ll_id_buffer


if as_flag<>"" then
	//si tratta di un seleziona/deseleziona tutti
	for ll_index=1 to dw_selezione.rowcount()
		dw_selezione.setitem(ll_index, "selezionato", as_flag)
	next
	
	return
end if

//se arrivi fin qui si tratta di un'azione su un singolo ID (gruppo di righe)
if al_row>0 then
	ll_id_buffer = dw_selezione.getitemnumber(al_row, "id_buffer")
	as_flag = dw_selezione.getitemstring(al_row, "selezionato")
	
	if as_flag= "N" then
		as_flag= "S"
	else
		as_flag="N"
	end if
	
	if ll_id_buffer>0 then
		for ll_index=1 to dw_selezione.rowcount()
			if dw_selezione.getitemnumber(ll_index, "id_buffer") = ll_id_buffer then
				dw_selezione.setitem(ll_index, "selezionato", as_flag)
			end if
		next
	end if
	
	return
end if


return
end subroutine

public function integer wf_conferma (ref string as_errore);
long			ll_index, ll_new

boolean		lb_almeno_uno=false


istr_parametri.parametro_ds_1.reset()

for ll_index=1 to dw_selezione.rowcount()
	if dw_selezione.getitemstring(ll_index, "selezionato") = "S" then
		ll_new = istr_parametri.parametro_ds_1.insertrow(0)
		istr_parametri.parametro_ds_1.setitem(ll_new, 1, dw_selezione.getitemstring(ll_index, "valore_file"))
		istr_parametri.parametro_ds_1.setitem(ll_new, 2, dw_selezione.getitemnumber(ll_index, "id_buffer"))
		
		lb_almeno_uno=true
	end if
next

if not lb_almeno_uno then
	as_errore = "Selezionare almenu un ID da elaborare!"
	return -1
end if


return 0
end function

on w_rileva_prod_mrp_buffer_sel.create
this.dw_selezione=create dw_selezione
this.Control[]={this.dw_selezione}
end on

on w_rileva_prod_mrp_buffer_sel.destroy
destroy(this.dw_selezione)
end on

event open;
long									ll_id_inizio, ll_id_fine


//struttura passata
//			istr_parametri.parametro_ul_1 = ll_id_buffer
//			istr_parametri.parametro_ul_2 = ll_id_buffer_fine
//			istr_parametri.parametro_ds_1 = lds_data

//struttura del datastore
//		valore_file		 id_buffer

istr_parametri = message.powerobjectparm

ll_id_inizio		= istr_parametri.parametro_ul_1
ll_id_fine		= istr_parametri.parametro_ul_2

dw_selezione.settransobject(sqlca)
dw_selezione.retrieve(s_cs_xx.cod_azienda, ll_id_inizio, ll_id_fine)
end event

type dw_selezione from datawindow within w_rileva_prod_mrp_buffer_sel
integer x = 27
integer y = 36
integer width = 3118
integer height = 2380
integer taborder = 10
string title = "none"
string dataobject = "d_rileva_prod_mrp_buffer_sel"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event buttonclicked;
integer		li_ret
string		ls_errore



choose case dwo.name
	case "b_tutti"
		wf_seleziona("S", 0)
		
	case "b_nessuno"
		wf_seleziona("N", 0)
		
	case "b_conferma"
		li_ret = wf_conferma(ls_errore)
		if li_ret < 0 then
			g_mb.error(ls_errore)
			return
		end if
		
		istr_parametri.parametro_b_1 = true
		closewithreturn(parent, istr_parametri)
		
		
	case "b_annulla"
		if g_mb.confirm("Annullare l'operazione?") then
			istr_parametri.parametro_b_1 = false
			closewithreturn(parent, istr_parametri)
		end if
		
	case "b_sel"
		if row>0 then
		else
			return
		end if
		wf_seleziona("", row)
		
end choose
end event

