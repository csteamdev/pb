﻿$PBExportHeader$w_rileva_prod_mrp_ddt.srw
forward
global type w_rileva_prod_mrp_ddt from window
end type
type cb_nessuno from commandbutton within w_rileva_prod_mrp_ddt
end type
type cb_tutti from commandbutton within w_rileva_prod_mrp_ddt
end type
type dp_data_rif_mrp from datepicker within w_rileva_prod_mrp_ddt
end type
type st_3 from statictext within w_rileva_prod_mrp_ddt
end type
type cb_annulla from commandbutton within w_rileva_prod_mrp_ddt
end type
type cb_conferma from commandbutton within w_rileva_prod_mrp_ddt
end type
type st_documento from statictext within w_rileva_prod_mrp_ddt
end type
type dw_lista from datawindow within w_rileva_prod_mrp_ddt
end type
type st_1 from statictext within w_rileva_prod_mrp_ddt
end type
type ddlb_reparto from dropdownlistbox within w_rileva_prod_mrp_ddt
end type
end forward

global type w_rileva_prod_mrp_ddt from window
integer width = 3927
integer height = 2616
boolean titlebar = true
string title = "Carico da DDT di Trasferimento"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cb_nessuno cb_nessuno
cb_tutti cb_tutti
dp_data_rif_mrp dp_data_rif_mrp
st_3 st_3
cb_annulla cb_annulla
cb_conferma cb_conferma
st_documento st_documento
dw_lista dw_lista
st_1 st_1
ddlb_reparto ddlb_reparto
end type
global w_rileva_prod_mrp_ddt w_rileva_prod_mrp_ddt

type variables
long			il_num_ddt

integer		ii_anno_ddt

string		is_mov_carico_mrp = "CAR"

datetime	idt_data_mov_mrp

end variables

forward prototypes
public function integer wf_retrieve (integer ai_anno, long al_numero)
public function integer wf_carica_drop_reparti (string as_cod_deposito, ref string as_errore)
public function integer wf_conferma (string as_cod_reparto, ref string as_errore)
public function integer wf_verifica_riga_ddt_mrp (long ll_prog_riga_bol_ven, ref integer ai_anno_mov_mrp, ref long al_num_mov_mrp, ref string as_errore)
end prototypes

public function integer wf_retrieve (integer ai_anno, long al_numero);string				ls_cod_deposito_tras, ls_errore

long					ll_index, ll_tot, ll_prog_riga_bol_ven, ll_num_mov_mrp

integer				li_anno_mov_mrp, li_ret


//controlli e letture preliminari ---------------------------------------------------------------------------
select cod_deposito_tras, data_bolla
into :ls_cod_deposito_tras, :idt_data_mov_mrp
from tes_bol_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno and
			num_registrazione=:al_numero;
			
if sqlca.sqlcode<0 then
	ls_errore = "Errore in  lettura deposito arrivo: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	ls_errore = "D.d.T. n° "+string(ai_anno)+"/"+string(al_numero)+" non trovato nel database!"
	return -1
	
elseif ls_cod_deposito_tras="" or isnull(ls_cod_deposito_tras) then
	ls_errore = "Deposito di arrivo NON specificato nel D.d.T. n° "+string(ai_anno)+"/"+string(al_numero)+"!"
	return -1
end if

//se la data bolla è vuota metti data oggi
if isnull(idt_data_mov_mrp) or year(date(idt_data_mov_mrp))<1980 then
	idt_data_mov_mrp = datetime(today(), 00:00:00)
end if


dp_data_rif_mrp.setvalue(date(idt_data_mov_mrp), 00:00:00)


//carico drop dei reparti associati al deposito arrivo --------------------------------------------------
if wf_carica_drop_reparti(ls_cod_deposito_tras, ls_errore)<0 then
	g_mb.error(ls_errore)
	
	return -1
end if


//eseguo retrieve dei dati del dettaglio -----------------------------------------------------------------
ll_tot = dw_lista.retrieve(s_cs_xx.cod_azienda, ai_anno, al_numero)

for ll_index=1 to ll_tot
	
	if dw_lista.getitemstring(ll_index, "cod_prodotto")="" or isnull(dw_lista.getitemstring(ll_index, "cod_prodotto")) then
		//il prodotto della riga risulta nullo, quindi metti a N la selezione, poi la dw
		//si occuperà della proprietà visibile a FALSE del checkbox
		dw_lista.setitem(ll_index, "selezionato", "N")
		dw_lista.setitem(ll_index, "disabilitato", "S")
		continue
	end if
	
	if dw_lista.getitemstring(ll_index, "flag_mrp")<>"S" then
		//il prodotto della riga risulta NON gestito da MRPnullo, quindi metti a N la selezione, poi la dw
		//si occuperà della proprietà visibile a FALSE del checkbox
		dw_lista.setitem(ll_index, "selezionato", "N")
		dw_lista.setitem(ll_index, "disabilitato", "S")
		continue
	end if
	
	ll_prog_riga_bol_ven = dw_lista.getitemnumber(ll_index, "prog_riga_bol_ven")
	
	//verifica se la riga è presente nella tabella movimenti MRP come carico
	
	setnull(li_anno_mov_mrp)
	setnull(ll_num_mov_mrp)
	
	li_ret = wf_verifica_riga_ddt_mrp(ll_prog_riga_bol_ven, li_anno_mov_mrp, ll_num_mov_mrp, ls_errore)
	if li_ret<0 then
		//errore
		g_mb.error(ls_errore)
		return -1
		
	elseif li_ret = 1 then
		//la riga risulta già movimentata, quindi metti a N la selezione, poi la dw
		//si occuperà della proprietà visibile a FALSE del checkbox
		dw_lista.setitem(ll_index, "selezionato", "N")
		dw_lista.setitem(ll_index, "disabilitato", "S")
		dw_lista.setitem(ll_index, "anno_mov_mrp", li_anno_mov_mrp)
		dw_lista.setitem(ll_index, "num_mov_mrp", ll_num_mov_mrp)
		continue

	end if
	
	//se arrivi fin qui pre-seleziona la riga per il carico
	dw_lista.setitem(ll_index, "selezionato", "S")
	dw_lista.setitem(ll_index, "disabilitato", "N")
next


return 0
end function

public function integer wf_carica_drop_reparti (string as_cod_deposito, ref string as_errore);string		ls_cod_reparto, ls_sql, ls_errore

long			ll_index, ll_tot

datastore	lds_data


ddlb_reparto.reset( )

ls_sql = "select cod_reparto "+&
			"from anag_reparti "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"cod_deposito='"+as_cod_deposito+"' and "+&
						"flag_blocco<>'S' and flag_mrp='S' "+&
			"order by cod_reparto asc"

ll_tot = guo_functions.uof_crea_datastore( lds_data, ls_sql, as_errore)
if ll_tot<0 then
	as_errore = "Errore in lettura reparti del deposito '"+as_cod_deposito+"' : "+sqlca.sqlerrtext
	return -1
	
elseif ll_tot=0 then
	as_errore = "Nessun reparto associato al deposito arrivo del ddt specificato dal codice '"+as_cod_deposito+"'"
	return -1
	
end if

for ll_index=1 to ll_tot
	ddlb_reparto.additem( lds_data.getitemstring(ll_index, 1))
next

destroy lds_data

return 0
end function

public function integer wf_conferma (string as_cod_reparto, ref string as_errore);uo_service_mov_mrp					luo_mov_mrp

long											ll_tot, ll_index, ll_prog_riga_doc_origine

dec{4}										ld_quantita, ld_valore_unitario

string										ls_tipo_doc_origine, ls_flag_elaborato,ls_cod_prodotto

integer										li_anno_doc_origine, li_ret



luo_mov_mrp = create uo_service_mov_mrp

ls_tipo_doc_origine = "BOLVEN"
ls_flag_elaborato = "N"

ll_tot = dw_lista.rowcount()
for ll_index=1 to ll_tot
	if dw_lista.getitemstring(ll_index, "selezionato") <> "S" then continue

	ll_prog_riga_doc_origine = dw_lista.getitemnumber(ll_index, "prog_riga_bol_ven")
	ls_cod_prodotto = dw_lista.getitemstring(ll_index, "cod_prodotto")
	ld_quantita = dw_lista.getitemdecimal(ll_index, "quan_consegnata")

	if isnull(ld_quantita) then ld_quantita=0
	ld_valore_unitario = 0

	li_ret = luo_mov_mrp.uof_insert_data(	idt_data_mov_mrp, is_mov_carico_mrp, ls_cod_prodotto, ld_quantita, ld_quantita, ld_valore_unitario, &
															ls_tipo_doc_origine, ii_anno_ddt, il_num_ddt, ll_prog_riga_doc_origine, as_cod_reparto, ls_flag_elaborato, &
															as_errore)
	if li_ret<0 then
		//si è verificato un errore
		destroy luo_mov_mrp
		return -1
	end if
	
next

destroy luo_mov_mrp

return 0

end function

public function integer wf_verifica_riga_ddt_mrp (long ll_prog_riga_bol_ven, ref integer ai_anno_mov_mrp, ref long al_num_mov_mrp, ref string as_errore);string			ls_sql, ls_errore

datastore		lds_data

long				ll_tot


//qua i movimenti, se esistono, ce ne è uno solo, ma per sicurezza faccio il datastore
//( ... e prendo l'ultimo fatto ...)
ls_sql = &
"select anno_registrazione, num_registrazione, data_rif_mrp "+&
"from mov_mrp "+&
"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
			"tipo_doc_origine='BOLVEN' and "+&
			"cod_tipo_mov_mrp='"+is_mov_carico_mrp+"' and "+&
			"anno_doc_origine="+string(ii_anno_ddt) + " and "+&
			"num_doc_origine="+string(il_num_ddt) + " and "+&
			"prog_riga_doc_origine="+string(ll_prog_riga_bol_ven) + "  "+&
"order by data_rif_mrp desc"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)

if ll_tot<0 then
	return -1
end if

if ll_tot>0 then
	//trovato: quindi la selezione della riga va disabilitata
	ai_anno_mov_mrp = lds_data.getitemnumber(1, 1)
	al_num_mov_mrp = lds_data.getitemnumber(1, 2)
	
	destroy lds_data
	return 1
end if

destroy lds_data
return 0
end function

on w_rileva_prod_mrp_ddt.create
this.cb_nessuno=create cb_nessuno
this.cb_tutti=create cb_tutti
this.dp_data_rif_mrp=create dp_data_rif_mrp
this.st_3=create st_3
this.cb_annulla=create cb_annulla
this.cb_conferma=create cb_conferma
this.st_documento=create st_documento
this.dw_lista=create dw_lista
this.st_1=create st_1
this.ddlb_reparto=create ddlb_reparto
this.Control[]={this.cb_nessuno,&
this.cb_tutti,&
this.dp_data_rif_mrp,&
this.st_3,&
this.cb_annulla,&
this.cb_conferma,&
this.st_documento,&
this.dw_lista,&
this.st_1,&
this.ddlb_reparto}
end on

on w_rileva_prod_mrp_ddt.destroy
destroy(this.cb_nessuno)
destroy(this.cb_tutti)
destroy(this.dp_data_rif_mrp)
destroy(this.st_3)
destroy(this.cb_annulla)
destroy(this.cb_conferma)
destroy(this.st_documento)
destroy(this.dw_lista)
destroy(this.st_1)
destroy(this.ddlb_reparto)
end on

event open;s_cs_xx_parametri		lstr_parametri

lstr_parametri = message.powerobjectparm

ii_anno_ddt = lstr_parametri.parametro_ul_1
il_num_ddt = lstr_parametri.parametro_ul_2

this.title = "Conferma carichi MRP per DDT di trasferimento n° "+string(ii_anno_ddt)+"/"+string(il_num_ddt)
st_documento.text = "DDT n° "+string(ii_anno_ddt)+"/"+string(il_num_ddt)

dw_lista.settransobject(sqlca)

setpointer(Hourglass!)
wf_retrieve(ii_anno_ddt, il_num_ddt)
setpointer(Arrow!)
end event

type cb_nessuno from commandbutton within w_rileva_prod_mrp_ddt
integer x = 411
integer y = 152
integer width = 343
integer height = 84
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Nessuno"
end type

event clicked;long					ll_index, ll_tot

ll_tot = dw_lista.rowcount()

//deseleziona tutti, anche quelli disabilitati per diversi motivi (movimento già fatto, prodotto nullo, prodotto non abilitato MRP)
//tanto si tratta di una de-selezione ...
for ll_index=1 to ll_tot
	dw_lista.setitem(ll_index, "selezionato", "N")
next
end event

type cb_tutti from commandbutton within w_rileva_prod_mrp_ddt
integer x = 41
integer y = 152
integer width = 343
integer height = 84
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Tutti"
end type

event clicked;long					ll_index, ll_tot

ll_tot = dw_lista.rowcount()

//seleziona tutti, tranne quelli disabilitati per diversi motivi (movimento già fatto, prodotto nullo, prodotto non abilitato MRP)
for ll_index=1 to ll_tot
	if dw_lista.getitemstring(ll_index, "disabilitato") = "S" then
		dw_lista.setitem(ll_index, "selezionato", "N")
	else
		dw_lista.setitem(ll_index, "selezionato", "S")
	end if
next
end event

type dp_data_rif_mrp from datepicker within w_rileva_prod_mrp_ddt
integer x = 2839
integer y = 144
integer width = 567
integer height = 96
integer taborder = 40
boolean border = true
boolean allowedit = true
date maxdate = Date("2999-12-31")
date mindate = Date("1800-01-01")
datetime value = DateTime(Date("2013-09-26"), Time("11:00:30.000000"))
integer textsize = -9
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
integer calendarfontweight = 400
weekday firstdayofweek = monday!
boolean todaysection = true
boolean todaycircle = true
end type

event valuechanged;idt_data_mov_mrp = dtm
end event

type st_3 from statictext within w_rileva_prod_mrp_ddt
integer x = 2263
integer y = 160
integer width = 553
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Data rif. MRP:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_rileva_prod_mrp_ddt
integer x = 3557
integer y = 152
integer width = 343
integer height = 84
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;
if g_mb.confirm("Annullare l'operazione?") then
	close(parent)
end if
end event

type cb_conferma from commandbutton within w_rileva_prod_mrp_ddt
integer x = 3557
integer y = 28
integer width = 343
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;string		ls_reparto, ls_errore
long			ll_index, ll_tot
boolean		lb_almeno_una = false


dw_lista.accepttext()

ls_reparto = ddlb_reparto.text

if isnull(ls_reparto) or ls_reparto="" then
	g_mb.warning("Per procedere al carico è necessario selezionare un reparto dalla lista!")
	return
end if

if isnull(idt_data_mov_mrp) or year(date(idt_data_mov_mrp))<1980 then
	g_mb.warning("Per procedere al carico è necessario selezionare una data di riferimento per i movimenti MRP!")
	return
end if


ll_tot = dw_lista.rowcount()
for ll_index=1 to ll_tot
	if dw_lista.getitemstring(ll_index, "selezionato") = "S" then
		lb_almeno_una = true
		exit
	end if
next

if not lb_almeno_una then
	g_mb.warning("Per procedere al carico è necessario selezionare almeno una riga del documento!")
	return
end if

if not g_mb.confirm("Procedere con i carichi nel reparto "+ls_reparto+" ?") then
	return
end if

if wf_conferma(ls_reparto, ls_errore) < 0 then
	rollback;
	g_mb.error(ls_errore)
else
	commit;
	g_mb.success("Procedura effettuata con successo!")
	close(parent)
end if
end event

type st_documento from statictext within w_rileva_prod_mrp_ddt
integer x = 32
integer y = 40
integer width = 2021
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "DDT n° "
boolean focusrectangle = false
end type

type dw_lista from datawindow within w_rileva_prod_mrp_ddt
integer x = 32
integer y = 264
integer width = 3867
integer height = 2236
integer taborder = 20
string title = "none"
string dataobject = "d_det_bol_ven_mrp"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type st_1 from statictext within w_rileva_prod_mrp_ddt
integer x = 2263
integer y = 36
integer width = 553
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Reparto da caricare:"
boolean focusrectangle = false
end type

type ddlb_reparto from dropdownlistbox within w_rileva_prod_mrp_ddt
integer x = 2834
integer y = 20
integer width = 571
integer height = 1040
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

