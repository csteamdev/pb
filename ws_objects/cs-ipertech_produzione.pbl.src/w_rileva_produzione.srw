﻿$PBExportHeader$w_rileva_produzione.srw
$PBExportComments$Finestra di rilevazione produzione
forward
global type w_rileva_produzione from window
end type
type dw_etichette_spedizione from datawindow within w_rileva_produzione
end type
type dw_etichetta_rdt from datawindow within w_rileva_produzione
end type
type tab_opzioni from tab within w_rileva_produzione
end type
type tp_produzione from userobject within tab_opzioni
end type
type rb_avanzamento_multiplo from radiobutton within tp_produzione
end type
type rb_menu_allega_docs from radiobutton within tp_produzione
end type
type rb_menu_desospendi_fase from radiobutton within tp_produzione
end type
type rb_menu_sospendi_fase from radiobutton within tp_produzione
end type
type rb_menu_reset_colli_e_stampa from radiobutton within tp_produzione
end type
type rb_menu_reset_colli from radiobutton within tp_produzione
end type
type rb_menu_stato from radiobutton within tp_produzione
end type
type rb_menu_rileva from radiobutton within tp_produzione
end type
type tp_produzione from userobject within tab_opzioni
rb_avanzamento_multiplo rb_avanzamento_multiplo
rb_menu_allega_docs rb_menu_allega_docs
rb_menu_desospendi_fase rb_menu_desospendi_fase
rb_menu_sospendi_fase rb_menu_sospendi_fase
rb_menu_reset_colli_e_stampa rb_menu_reset_colli_e_stampa
rb_menu_reset_colli rb_menu_reset_colli
rb_menu_stato rb_menu_stato
rb_menu_rileva rb_menu_rileva
end type
type tp_stampe from userobject within tab_opzioni
end type
type rb_cerificazione from radiobutton within tp_stampe
end type
type rb_menu_etichette_tenda_ridotta from radiobutton within tp_stampe
end type
type rb_menu_etichette_tenda from radiobutton within tp_stampe
end type
type rb_menu_etichette_spedizioni from radiobutton within tp_stampe
end type
type rb_stampa_doc from radiobutton within tp_stampe
end type
type rb_scheda_prodotto from radiobutton within tp_stampe
end type
type rb_menu_ristampa_etichette_apertura from radiobutton within tp_stampe
end type
type rb_menu_packing_list from radiobutton within tp_stampe
end type
type rb_menu_etichette_sfuso from radiobutton within tp_stampe
end type
type rb_menu_etichette from radiobutton within tp_stampe
end type
type tp_stampe from userobject within tab_opzioni
rb_cerificazione rb_cerificazione
rb_menu_etichette_tenda_ridotta rb_menu_etichette_tenda_ridotta
rb_menu_etichette_tenda rb_menu_etichette_tenda
rb_menu_etichette_spedizioni rb_menu_etichette_spedizioni
rb_stampa_doc rb_stampa_doc
rb_scheda_prodotto rb_scheda_prodotto
rb_menu_ristampa_etichette_apertura rb_menu_ristampa_etichette_apertura
rb_menu_packing_list rb_menu_packing_list
rb_menu_etichette_sfuso rb_menu_etichette_sfuso
rb_menu_etichette rb_menu_etichette
end type
type tp_mrp from userobject within tab_opzioni
end type
type st_log from statictext within tp_mrp
end type
type dw_lista from datawindow within tp_mrp
end type
type dw_selezione from u_dw_search within tp_mrp
end type
type tp_mrp from userobject within tab_opzioni
st_log st_log
dw_lista dw_lista
dw_selezione dw_selezione
end type
type tp_ologrammi from userobject within tab_opzioni
end type
type rb_menu_ricerca_da_olo from radiobutton within tp_ologrammi
end type
type rb_menu_assegna_iniz_olo from radiobutton within tp_ologrammi
end type
type tp_ologrammi from userobject within tab_opzioni
rb_menu_ricerca_da_olo rb_menu_ricerca_da_olo
rb_menu_assegna_iniz_olo rb_menu_assegna_iniz_olo
end type
type tp_altro from userobject within tab_opzioni
end type
type rb_etichetta_commessa from radiobutton within tp_altro
end type
type rb_stampa_commessa from radiobutton within tp_altro
end type
type rb_avanza_commessa from radiobutton within tp_altro
end type
type cb_componi_rdt from commandbutton within tp_altro
end type
type em_copie_etichetta_rdt from editmask within tp_altro
end type
type rb_etichetta_rdt from radiobutton within tp_altro
end type
type rb_scarica_dep_conto_forn from radiobutton within tp_altro
end type
type rb_menu_richiesta_trasf_comp from radiobutton within tp_altro
end type
type rb_menu_conferma_ddt_trasf from radiobutton within tp_altro
end type
type tp_altro from userobject within tab_opzioni
rb_etichetta_commessa rb_etichetta_commessa
rb_stampa_commessa rb_stampa_commessa
rb_avanza_commessa rb_avanza_commessa
cb_componi_rdt cb_componi_rdt
em_copie_etichetta_rdt em_copie_etichetta_rdt
rb_etichetta_rdt rb_etichetta_rdt
rb_scarica_dep_conto_forn rb_scarica_dep_conto_forn
rb_menu_richiesta_trasf_comp rb_menu_richiesta_trasf_comp
rb_menu_conferma_ddt_trasf rb_menu_conferma_ddt_trasf
end type
type tab_opzioni from tab within w_rileva_produzione
tp_produzione tp_produzione
tp_stampe tp_stampe
tp_mrp tp_mrp
tp_ologrammi tp_ologrammi
tp_altro tp_altro
end type
type dw_reparti from u_dw_search within w_rileva_produzione
end type
type dw_buffer from datawindow within w_rileva_produzione
end type
type st_7 from statictext within w_rileva_produzione
end type
type sle_anno_registrazione from singlelineedit within w_rileva_produzione
end type
type st_6 from statictext within w_rileva_produzione
end type
type sle_cod_operaio from singlelineedit within w_rileva_produzione
end type
type st_5 from statictext within w_rileva_produzione
end type
type cb_conferma from commandbutton within w_rileva_produzione
end type
type st_4 from statictext within w_rileva_produzione
end type
type st_2 from statictext within w_rileva_produzione
end type
type sle_prog_riga_ord_ven from singlelineedit within w_rileva_produzione
end type
type sle_num_registrazione from singlelineedit within w_rileva_produzione
end type
type st_3 from statictext within w_rileva_produzione
end type
type st_messaggio from statictext within w_rileva_produzione
end type
type gb_2 from groupbox within w_rileva_produzione
end type
type gb_1 from groupbox within w_rileva_produzione
end type
type dw_richiesta_trasf_componenti from datawindow within w_rileva_produzione
end type
end forward

global type w_rileva_produzione from window
integer width = 3776
integer height = 2848
boolean titlebar = true
string title = "Rilevazione di Produzione"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 12632256
boolean center = true
event ue_posiziona_focus ( )
event ue_impostazioni_mrp ( )
dw_etichette_spedizione dw_etichette_spedizione
dw_etichetta_rdt dw_etichetta_rdt
tab_opzioni tab_opzioni
dw_reparti dw_reparti
dw_buffer dw_buffer
st_7 st_7
sle_anno_registrazione sle_anno_registrazione
st_6 st_6
sle_cod_operaio sle_cod_operaio
st_5 st_5
cb_conferma cb_conferma
st_4 st_4
st_2 st_2
sle_prog_riga_ord_ven sle_prog_riga_ord_ven
sle_num_registrazione sle_num_registrazione
st_3 st_3
st_messaggio st_messaggio
gb_2 gb_2
gb_1 gb_1
dw_richiesta_trasf_componenti dw_richiesta_trasf_componenti
end type
global w_rileva_produzione w_rileva_produzione

type variables
long 					il_barcode

string 				is_cod_operaio, is_opzione = "R"

boolean 				ib_test, ib_supervisore = false, ib_ADP = false

uo_produzione 		iuo_produzione

datastore			ids_ordini

string					is_cod_parametro_blocco = "APD"

uo_fido_cliente		iuo_fido
end variables

forward prototypes
public function boolean wf_scarica_conto_dep_for (string as_barcode, ref string as_error)
public subroutine wf_get_opzione ()
public function integer wf_elabora_codici_mrp (datastore ads_input)
public function integer wf_leggi_file_mrp ()
public function integer wf_leggi_buffer_mrp ()
public function integer wf_procedi_mrp (ref string as_errore)
public function integer wf_leggi_ordini_mrp (string as_cod_reparto)
public function integer wf_decodifica_mrp (long ai_riga, string as_input, ref string as_cod_tipo_mov_mrp, ref string as_cod_reparto, ref string as_cod_prodotto, ref decimal ad_quantita, ref string as_data_scansione, ref string as_errore)
public function integer wf_stampa_documenti (string as_tipo_doc, integer ai_anno_doc, long al_num_doc, ref string as_errore)
public function integer wf_ddt_trasf_mrp (string as_barcode, ref string as_errore)
public function integer wf_etichetta_rdt (integer ai_anno_rdt, long al_num_rdt, integer al_num_copie, ref string as_errore)
public function integer wf_ristampa_rdt (integer ai_anno_rdt, long al_num_rdt, ref string as_errore)
public subroutine wf_help ()
public function integer wf_controllo_blocco_cliente (long al_barcode, integer ai_anno, long al_numero, ref string as_errore)
public function integer wf_avanza_commessa (integer ai_anno_commessa, long al_num_commessa, ref string as_errore)
public function integer wf_etichetta_commessa (integer ai_anno_commessa, long al_num_commessa, ref string as_errore)
public function integer wf_stampa_etichette_spedizione (string as_barcode)
public function integer wf_stampa_etichette_tenda (string as_barcode, string as_formato)
public function integer wf_avanza_produzione_barcode (string as_barcode, ref string as_message)
end prototypes

event ue_posiziona_focus();dw_buffer.setfocus()
end event

event ue_impostazioni_mrp();string							ls_sql, ls_syntax, ls_error, ls_where

long								ll_getrow


ll_getrow = tab_opzioni.tp_mrp.dw_selezione.getrow()

//------------------------------------------------------------------
ls_sql = "select ltrim('  ') as cod_tipo_mov_mrp, ltrim('      ') as cod_reparto, "+&
						"ltrim('               ') as cod_prodotto, 0.0000 as quan_ordine, ltrim('                   ') as data_scansione "+&
						",0 as id_buffer "+&
			"from aziende "+&
			"where cod_azienda='--------' "

ls_syntax = sqlca.SyntaxFromSQL(ls_sql, 'Style(Type=Grid)', ls_error)
tab_opzioni.tp_mrp.dw_lista.create(ls_syntax, ls_error)
tab_opzioni.tp_mrp.dw_lista.SetTransObject(sqlca)
tab_opzioni.tp_mrp.dw_lista.Retrieve()


ids_ordini = create datastore
ids_ordini.dataobject = "d_rileva_produzione_mrp_ordini"
ids_ordini.settransobject(sqlca)		//OCIO: non fare mai la retrieve


tab_opzioni.tp_mrp.dw_selezione.insertrow(0)

//solo quelli con flag_mrp posto a SI
ls_where = 	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
							"((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and "+&
					"flag_mrp='S' "

f_po_loaddddw_dw(		tab_opzioni.tp_mrp.dw_selezione, &
								"cod_reparto", &
								sqlca, &
								"anag_reparti", &
								"cod_reparto", &
								"des_reparto", &
								ls_where)
								
tab_opzioni.tp_mrp.dw_selezione.setitem(ll_getrow, "data_rif_mrp", datetime(today(), 00:00:00))
end event

public function boolean wf_scarica_conto_dep_for (string as_barcode, ref string as_error);/**
 * stefanop
 * 28/03/2012
 *
 * Controllo che il codice barcode sia inserito nella tabella di configurazione dei depositi fornitori
 * se è valido allora apro la finestra modale e attendo i nuovi codici barcode per generare
 * il ddt di entrata
 **/
 
string ls_test
boolean lb_ocd
s_cs_xx_parametri lstr_data

lstr_data.parametro_ul_1 = 0
lstr_data.parametro_ul_2 = 0
lstr_data.parametro_ul_3 = 0

as_barcode = upper(as_barcode)

select cod_barcode
into :ls_test
from con_ril_produzione
where cod_azienda= :s_cs_xx.cod_azienda and
		 cod_barcode = :as_barcode;
		 
if sqlca.sqlcode < 0 then
	as_error = "Errore durante la convalida del barcode.~r~n" + sqlca.sqlerrtext
	return false
elseif sqlca.sqlcode = 100 then
	as_error = "Il codice barcode non è riconosciuto come codice di Scarico Conto Deposito Fornitore. Controllare la tabella di configurazione."
	return false
end if

guo_functions.uof_get_parametro_azienda("OCD", lb_ocd)

if lb_ocd then
	// Devo chiedere l'anno ed il numero dell'ordine.
	window_open(w_ordine_conto_dep, 0)
	
	if isvalid(message.powerobjectparm) then
		lstr_data = message.powerobjectparm
	else
		return false
	end if 
end if

lstr_data.parametro_s_1 = as_barcode
window_open_parm(w_scarico_conto_dep_for, -1, lstr_data)

return  true
end function

public subroutine wf_get_opzione ();string			ls_des_opzione

choose case tab_opzioni.selectedtab
	//#####################################################
	case 1	//PRODUZIONE
		if tab_opzioni.tp_produzione.rb_menu_rileva.checked then
			is_opzione = "R"
			ls_des_opzione = tab_opzioni.tp_produzione.rb_menu_rileva.text
			
		elseif tab_opzioni.tp_produzione.rb_menu_stato.checked then
			is_opzione = "S"
			ls_des_opzione = tab_opzioni.tp_produzione.rb_menu_stato.text
			
		elseif tab_opzioni.tp_produzione.rb_menu_reset_colli.checked then
			is_opzione = "C"
			ls_des_opzione = tab_opzioni.tp_produzione.rb_menu_reset_colli.text
			
		elseif tab_opzioni.tp_produzione.rb_menu_reset_colli_e_stampa.checked then
			is_opzione = "K"
			ls_des_opzione = tab_opzioni.tp_produzione.rb_menu_reset_colli_e_stampa.text
			
		elseif tab_opzioni.tp_produzione.rb_menu_sospendi_fase.checked then
			is_opzione = "SOSP"
			ls_des_opzione = tab_opzioni.tp_produzione.rb_menu_sospendi_fase.text
			
		elseif tab_opzioni.tp_produzione.rb_menu_desospendi_fase.checked then
			is_opzione = "DESOSP"
			ls_des_opzione = tab_opzioni.tp_produzione.rb_menu_desospendi_fase.text
			
		elseif tab_opzioni.tp_produzione.rb_menu_allega_docs.checked then
			is_opzione = "ALLEGA"
			ls_des_opzione = tab_opzioni.tp_produzione.rb_menu_allega_docs.text
			
		elseif tab_opzioni.tp_produzione.rb_avanzamento_multiplo.checked then
			is_opzione = "MULTIAV"
			ls_des_opzione = tab_opzioni.tp_produzione.rb_avanzamento_multiplo.text
			
		else
			is_opzione = ""
			ls_des_opzione = ""
		end if
	
	//#####################################################
	case 2	//STAMPE
		if tab_opzioni.tp_stampe.rb_menu_etichette.checked then
			is_opzione = "E"
			ls_des_opzione = tab_opzioni.tp_stampe.rb_menu_etichette.text
			
		elseif tab_opzioni.tp_stampe.rb_menu_etichette_sfuso.checked then
			is_opzione = "U"
			ls_des_opzione = tab_opzioni.tp_stampe.rb_menu_etichette_sfuso.text
			
		elseif tab_opzioni.tp_stampe.rb_menu_packing_list.checked then
			is_opzione = "P"
			ls_des_opzione = tab_opzioni.tp_stampe.rb_menu_packing_list.text
			
		elseif tab_opzioni.tp_stampe.rb_menu_ristampa_etichette_apertura.checked then
			is_opzione = "A"
			ls_des_opzione = tab_opzioni.tp_stampe.rb_menu_ristampa_etichette_apertura.text
			
		elseif tab_opzioni.tp_stampe.rb_scheda_prodotto.checked then
			is_opzione = "J"
			ls_des_opzione = tab_opzioni.tp_stampe.rb_scheda_prodotto.text
			
		elseif tab_opzioni.tp_stampe.rb_stampa_doc.checked then
			is_opzione = "STAMPADOC"
			ls_des_opzione = tab_opzioni.tp_stampe.rb_stampa_doc.text
			
		elseif tab_opzioni.tp_stampe.rb_menu_etichette_spedizioni.checked then
			is_opzione = "ETICHETTE_SPEDIZIONE"
			ls_des_opzione = tab_opzioni.tp_stampe.rb_stampa_doc.text
			
		elseif tab_opzioni.tp_stampe.rb_menu_etichette_tenda.checked then
			is_opzione = "ETICHETTE_TENDA"
			ls_des_opzione = tab_opzioni.tp_stampe.rb_stampa_doc.text
		elseif tab_opzioni.tp_stampe.rb_menu_etichette_tenda_ridotta.checked then
			is_opzione = "ETICHETTE_TENDA_RIDOTTA"
			ls_des_opzione = tab_opzioni.tp_stampe.rb_stampa_doc.text
		elseif tab_opzioni.tp_stampe.rb_cerificazione.checked then
			is_opzione = "STAMPA_CERTIFICAZIONE"
			ls_des_opzione = tab_opzioni.tp_stampe.rb_stampa_doc.text
		else
			is_opzione = ""
			ls_des_opzione = ""
		end if
	
	
	//#####################################################
	case 3	//MAGAZZINO MRP
		is_opzione = "MRP"
		ls_des_opzione = "Magazzino MRP"
	
	
	//#####################################################
	case 4	//OLOGRAMMI
		if tab_opzioni.tp_ologrammi.rb_menu_assegna_iniz_olo.checked then
			is_opzione = "I"
			ls_des_opzione = tab_opzioni.tp_ologrammi.rb_menu_assegna_iniz_olo.text
			
		elseif tab_opzioni.tp_ologrammi.rb_menu_ricerca_da_olo.checked then
			is_opzione = "N"
			ls_des_opzione = tab_opzioni.tp_ologrammi.rb_menu_ricerca_da_olo.text
			
		else
			is_opzione = ""
			ls_des_opzione = ""
		end if
	
	
	//#####################################################
	case 5	//ALTRO
		if tab_opzioni.tp_altro.rb_menu_conferma_ddt_trasf.checked then
			is_opzione = "D"
			ls_des_opzione = tab_opzioni.tp_altro.rb_menu_conferma_ddt_trasf.text
			
		elseif tab_opzioni.tp_altro.rb_menu_richiesta_trasf_comp.checked then
			is_opzione = "T"
			ls_des_opzione = tab_opzioni.tp_altro.rb_menu_richiesta_trasf_comp.text
		
		elseif tab_opzioni.tp_altro.rb_scarica_dep_conto_forn.checked then
			is_opzione = "F"
			ls_des_opzione = tab_opzioni.tp_altro.rb_scarica_dep_conto_forn.text
		
		elseif tab_opzioni.tp_altro.rb_etichetta_rdt.checked then
			is_opzione = "ET_RDT"
			ls_des_opzione = tab_opzioni.tp_altro.rb_etichetta_rdt.text
		
		//avanzamento commessa interna
		elseif tab_opzioni.tp_altro.rb_avanza_commessa.checked then
			is_opzione = "AVANZACOM"
			ls_des_opzione = tab_opzioni.tp_altro.rb_avanza_commessa.text
		
		//stampa commessa interna
		elseif tab_opzioni.tp_altro.rb_stampa_commessa.checked then
			is_opzione = "STAMPACOM"
			ls_des_opzione = tab_opzioni.tp_altro.rb_stampa_commessa.text
		
		//etichetta commessa interna
		elseif tab_opzioni.tp_altro.rb_etichetta_commessa.checked then
			is_opzione = "ET_COM"
			ls_des_opzione = tab_opzioni.tp_altro.rb_etichetta_commessa.text
		
		else
			is_opzione = ""
			ls_des_opzione = ""
		end if
		
		
	//#####################################################
	case else
			is_opzione = ""
			ls_des_opzione = ""
			
end choose

this.title = "Rilevazione Produzione - " + ls_des_opzione


return


end subroutine

public function integer wf_elabora_codici_mrp (datastore ads_input);string		ls_input, ls_cod_prodotto, ls_errore, ls_cod_reparto, ls_cod_tipo_mov_mrp, ls_data_scansione

long			ll_index, ll_ret, ll_elab, ll_count, ll_getrow, ll_id_buffer

integer		li_return

dec{4}		ld_quantita, ld_qta_temp


		
ll_elab = 0
ll_ret = ads_input.rowcount()
tab_opzioni.tp_mrp.st_log.text = ""
tab_opzioni.tp_mrp.dw_lista.reset()

ll_getrow = tab_opzioni.tp_mrp.dw_selezione.getrow()

ads_input.setsort("id_buffer asc")
ads_input.sort()

for ll_index = 1 to ll_ret
	
	//rileggo il reparto eventualmente impostato
	ls_cod_reparto = tab_opzioni.tp_mrp.dw_selezione.getitemstring(ll_getrow, "cod_reparto")
	
	ls_input = ads_input.getitemstring(ll_index,"input")
	ll_id_buffer = ads_input.getitemnumber(ll_index,"id_buffer")
	
	li_return = wf_decodifica_mrp(ll_index, ls_input, ls_cod_tipo_mov_mrp, ls_cod_reparto, ls_cod_prodotto, ld_quantita, ls_data_scansione, ls_errore)
	if li_return=1 then
		
		if ls_errore<>"" and not isnull(ls_errore) then
			g_mb.warning(ls_errore)
		end if
		
		continue
		
	elseif ls_errore<>"" then
		//es. caso prodotto bloccato
		g_mb.warning(ls_errore)
	end if
	
//	//se il prodotto è già presente nella lista sommo la quantità
//	ll_count = tab_opzioni.tp_mrp.dw_lista.find("cod_prodotto='"+ls_cod_prodotto+"'", 1,tab_opzioni.tp_mrp.dw_lista.rowcount())
//	if ll_count>0 then
//		//articolo già presente, somma la quantità
//		
//		//leggo quella già presente nella riga
//		ld_qta_temp = tab_opzioni.tp_mrp.dw_lista.getitemdecimal(ll_count, "quan_ordine")
//		if isnull(ld_qta_temp) then ld_qta_temp=0
//		
//		//sommo
//		ld_quantita = ld_quantita + ld_qta_temp
//		
//	else
//		//non presente, faccio insertrow
//		ll_count = tab_opzioni.tp_mrp.dw_lista.insertrow(0)
//		tab_opzioni.tp_mrp.dw_lista.setitem(ll_count, "cod_prodotto", ls_cod_prodotto)
//	end if
	
	ll_count = tab_opzioni.tp_mrp.dw_lista.insertrow(0)
	tab_opzioni.tp_mrp.dw_lista.setitem(ll_count, "cod_tipo_mov_mrp", ls_cod_tipo_mov_mrp)
	tab_opzioni.tp_mrp.dw_lista.setitem(ll_count, "cod_reparto", ls_cod_reparto)
	tab_opzioni.tp_mrp.dw_lista.setitem(ll_count, "cod_prodotto", ls_cod_prodotto)
	tab_opzioni.tp_mrp.dw_lista.setitem(ll_count, "quan_ordine", ld_quantita)
	tab_opzioni.tp_mrp.dw_lista.setitem(ll_count, "data_scansione", ls_data_scansione)
	tab_opzioni.tp_mrp.dw_lista.setitem(ll_count, "id_buffer", ll_id_buffer)
	
	ll_elab += 1
	
next

destroy ads_input

tab_opzioni.tp_mrp.st_log.text = "Elaborati: " + string(ll_ret) + " di cui saltati "+string(ll_ret - ll_elab)
		

return 0
end function

public function integer wf_leggi_file_mrp ();
datetime	ldt_data_rif_mrp

string		ls_docname, ls_named, ls_sql, ls_errore, ls_FMP

long			ll_ret, ll_getrow

datastore	lds_input



ll_getrow = tab_opzioni.tp_mrp.dw_selezione.getrow()

tab_opzioni.tp_mrp.dw_selezione.setitem(ll_getrow, "path", "")

tab_opzioni.tp_mrp.dw_selezione.accepttext()

ldt_data_rif_mrp = tab_opzioni.tp_mrp.dw_selezione.getitemdatetime(ll_getrow, "data_rif_mrp")


tab_opzioni.tp_mrp.dw_selezione.setitem(ll_getrow, "id_buffer", 0)
tab_opzioni.tp_mrp.dw_selezione.setitem(ll_getrow, "id_buffer_fine", 0)

if isnull(ldt_data_rif_mrp) or year(date(ldt_data_rif_mrp)) < 1950 then
	g_mb.error("Selezionare una data riferimento MRP!")
	tab_opzioni.tp_mrp.st_log.text = "ERRORE!"
	return -1
end if


if date(ldt_data_rif_mrp) > today() then
	g_mb.error("Non è possibile registrare movimenti MRP in data superiore a quella odierna!")
	tab_opzioni.tp_mrp.st_log.text = "ERRORE!"
	return -1
end if

//FMP
guo_functions.uof_get_parametro_azienda("FMP", ls_FMP)
if ls_FMP<>"" and not isnull(ls_FMP) then ChangeDirectory(ls_FMP)

ll_ret = GetFileOpenName("Seleziona File", ls_docname, ls_named, 	&
															"TXT", &
															"Files di testo (*.TXT),*.TXT,"+&
															"Files CSV (*.CSV),*.CSV,")

if ll_ret = 1 then
	tab_opzioni.tp_mrp.dw_selezione.setitem(ll_getrow, "path", ls_docname)
	
	tab_opzioni.tp_mrp.dw_lista.reset()

	if ls_docname<>"" and not isnull(ls_docname) then
		//aggiorna la lista leggendola dal file

		ls_sql = "select LTRIM('                                                                                                                                                      ') as input, 0 as id_buffer "+&
					"from aziende "+&
					"where cod_azienda=''"
		guo_functions.uof_crea_datastore(lds_input, ls_sql, ls_errore)

		ll_ret = lds_input.importfile(CSV!, ls_docname)


		if ll_ret<0 then
			g_mb.error("Errore in lettura file selezionato: ("+string(ll_ret)+")")
			tab_opzioni.tp_mrp.st_log.text = "ERRORE!"
			return -1
	
		elseif ll_ret=0 then
			g_mb.error("Il file selezionato sembra vuoto!")
			tab_opzioni.tp_mrp.st_log.text = "File vuoto!"
			return -1
		end if

		wf_elabora_codici_mrp(lds_input)
		
	end if
else
	tab_opzioni.tp_mrp.st_log.text = "Nessun file selezionato"
end if

return 0
end function

public function integer wf_leggi_buffer_mrp ();
datetime	ldt_data_rif_mrp

string		ls_sql, ls_errore, ls_valore_file

long			ll_id_buffer, ll_ret, ll_index, ll_new, ll_getrow, ll_id_buffer_fine

datastore	lds_input, lds_data, lds_count

boolean		lb_unico = false

s_cs_xx_parametri		lstr_parametri



ll_getrow = tab_opzioni.tp_mrp.dw_selezione.getrow()

tab_opzioni.tp_mrp.dw_selezione.setitem(ll_getrow, "path", "")

tab_opzioni.tp_mrp.dw_selezione.accepttext()

ldt_data_rif_mrp = tab_opzioni.tp_mrp.dw_selezione.getitemdatetime(ll_getrow, "data_rif_mrp")
ll_id_buffer = tab_opzioni.tp_mrp.dw_selezione.getitemnumber(ll_getrow, "id_buffer")
ll_id_buffer_fine = tab_opzioni.tp_mrp.dw_selezione.getitemnumber(ll_getrow, "id_buffer_fine")



if isnull(ldt_data_rif_mrp) or year(date(ldt_data_rif_mrp)) < 1950 then
	g_mb.error("Selezionare una data riferimento MRP!")
	tab_opzioni.tp_mrp.st_log.text = "ERRORE!"
	return -1
end if

if date(ldt_data_rif_mrp) > today() then
	g_mb.error("Non è possibile registrare movimenti MRP in data superiore a quella odierna!")
	tab_opzioni.tp_mrp.st_log.text = "ERRORE!"
	return -1
end if

if isnull(ll_id_buffer) then  ll_id_buffer = 0
if isnull(ll_id_buffer_fine) then  ll_id_buffer_fine = 0

if ll_id_buffer>ll_id_buffer_fine then
	g_mb.error("Selezionare un intervallo di ID BUFFER MRP congruente!")
	tab_opzioni.tp_mrp.st_log.text = "ERRORE!"
	return -1
end if

if ll_id_buffer=ll_id_buffer_fine and ll_id_buffer=0 then
	g_mb.error("Selezionare un intervallo di ID BUFFER MRP da elaborare!")
	tab_opzioni.tp_mrp.st_log.text = "ERRORE!"
	return -1
end if

//if ll_id_buffer>0 then
//else
//	g_mb.error("Selezionare un ID BUFFER MRP!")
//	tab_opzioni.tp_mrp.st_log.text = "ERRORE!"
//	return -1
//end if


tab_opzioni.tp_mrp.dw_lista.reset()


//inizializzo struttura del datastore di input
ls_sql = "select '                              ' as input, 0 as id_buffer "+&
			"from aziende "+&
			"where cod_azienda=''"
guo_functions.uof_crea_datastore(lds_input, ls_sql, ls_errore)


if ll_id_buffer<ll_id_buffer_fine then
	ls_sql = 	"select count(*), id_buffer "+&
				"from buffer_prodotti_qta "+&
				"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"id_buffer>="+string(ll_id_buffer)+" and id_buffer<="+string(ll_id_buffer_fine) +"and "+&
							"flag_letto='N' "+&
				"group by id_buffer"
	
	ll_ret = guo_functions.uof_crea_datastore(lds_count, ls_sql, ls_errore)
	
	if ll_ret<0 then
		g_mb.error("Errore in controllo intervallo ID: "+sqlca.sqlerrtext)
		tab_opzioni.tp_mrp.st_log.text = "ERRORE!"
		return -1
	end if
	
	if isnull(ll_ret) or ll_ret=0 then
		g_mb.error("Sembra non ci sia alcun dato da elaborare in questo intervallo di ID!")
		tab_opzioni.tp_mrp.st_log.text = "NESSUN DATO DA ELABORARE!"
		return -1
		
	elseif ll_ret = 1 then
		lb_unico = true
		
		//leggo l'id buffer da presentare
		ll_id_buffer = lds_count.getitemnumber(1, 2)
	else
		lb_unico = false
	end if
	
else
	lb_unico = true
end if

ll_ret = 0

if lb_unico then
	//leggi i dati dal buffer e metto in un altro datastore di appoggio
	ls_sql = "select valore_file, id_buffer "+&
				"from buffer_prodotti_qta "+&
				"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"id_buffer="+string(ll_id_buffer) + " and " + &
							"flag_letto='N' "
	ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
	
	if ll_ret<0 then
		g_mb.error("Errore in creazione ds dati dal buffer:"+ls_errore)
		tab_opzioni.tp_mrp.st_log.text = "ERRORE!"
		return -1
	
	elseif ll_ret=0 then
		destroy lds_data
		g_mb.error("Nessun dato da visualizzare per questo ID Buffer oppure ID Buffer inesistente oppure già elaborato!")
		tab_opzioni.tp_mrp.st_log.text = "Nessun dato da visualizzare!"
		return -1
		
	end if
	
else
	//apri una window di conferma progressivi da elaborare, e ritorna il datastore di appoggio già popolato (lds_data)
	
	//preparo la struttura al datastore
	ls_sql = "select valore_file, id_buffer "+&
				"from buffer_prodotti_qta "+&
				"where cod_azienda='CODICEINESISTENTE'"
	guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
	
	lstr_parametri.parametro_ul_1 = ll_id_buffer
	lstr_parametri.parametro_ul_2 = ll_id_buffer_fine
	lstr_parametri.parametro_ds_1 = lds_data
	openwithparm(w_rileva_prod_mrp_buffer_sel, lstr_parametri)
	
	lstr_parametri = message.powerobjectparm
	
	if not lstr_parametri.parametro_b_1 then
		//g_mb.error("Interrotto dall'utente!")
		tab_opzioni.tp_mrp.st_log.text = "Interrotto dall'utente!"
		return -1
	end if
	
	ll_ret = lstr_parametri.parametro_ds_1.rowscopy(1, lstr_parametri.parametro_ds_1.rowcount(), Primary!, lds_data, 1, Primary!)
	ll_ret = lds_data.rowcount()
end if

for ll_index=1 to ll_ret
	ll_new = lds_input.insertrow(0)
	
	ls_valore_file = lds_data.getitemstring(ll_index, 1)
	
	lds_input.setitem(ll_index, 1, ls_valore_file)
	lds_input.setitem(ll_index, 2, lds_data.getitemnumber(ll_index, 2))
next

destroy lds_data


wf_elabora_codici_mrp(lds_input)


return 0
end function

public function integer wf_procedi_mrp (ref string as_errore);long									ll_tot, ll_index, ll_num_doc_origine, ll_prog_riga_doc_origine, ll_num_lp, ll_getrow, ll_id_buffer, ll_id_buffer_old
datetime							ldt_data_rif_mrp
string								ls_cod_reparto, ls_cod_prodotto, ls_tipo_doc_origine, ls_flag_elaborato, ls_temp, ls_cod_tipo_mov_mrp
uo_service_mov_mrp			luo_mrp
dec{4}								ld_quantita, ld_valore_unitario
integer								li_ret, li_anno_doc_origine, li_anno_lp
boolean								lb_lista_prelievo


tab_opzioni.tp_mrp.dw_selezione.accepttext()

ll_getrow = tab_opzioni.tp_mrp.dw_selezione.getrow()


ldt_data_rif_mrp = tab_opzioni.tp_mrp.dw_selezione.getitemdatetime(ll_getrow, "data_rif_mrp")

ids_ordini.reset()

lb_lista_prelievo = false
ls_temp = tab_opzioni.tp_mrp.dw_selezione.describe("lista_prelievo.visible")
if ls_temp="1" then
	lb_lista_prelievo = true
end if

if isnull(ldt_data_rif_mrp) or year(date(ldt_data_rif_mrp)) < 1950 then
	as_errore = "Selezionare una data riferimento MRP!"
	tab_opzioni.tp_mrp.st_log.text = "ERRORE!"
	return -1
end if

if date(ldt_data_rif_mrp) > today() then
	g_mb.error("Non è possibile registrare movimenti MRP in data superiore a quella odierna!")
	tab_opzioni.tp_mrp.st_log.text = "ERRORE!"
	return -1
end if


ll_tot = tab_opzioni.tp_mrp.dw_lista.rowcount()

if ll_tot=0 or isnull(ll_tot) then
	as_errore = "Nessun dato presente nella lista tipoMov/reparto/prodotto/quantità!"
	return -1
end if

//------------------------------------------------------------------------------------------------------------------------------------
//se ci sono movimenti INVENTARIO avvisa prima ...
for ll_index=1 to ll_tot
	ls_cod_tipo_mov_mrp = tab_opzioni.tp_mrp.dw_lista.getitemstring(ll_index, "cod_tipo_mov_mrp") 
	
	if ls_cod_tipo_mov_mrp="INV" then
		if not g_mb.confirm("nella lista sono presenti movimenti di Inventario MRP. Sei sicuro di voler procedere?") then
			//NON PROCEDERE ...
			as_errore = "Annullato dall'operatore!"
			return 1
		else
			//pur essendoci movimenti di INVENTARIO hai scelto di continuare
			//esci dal ciclo e vai avanti
			exit
		end if
	end if
next
//------------------------------------------------------------------------------------------------------------------------------------


if lb_lista_prelievo then
	li_anno_lp = tab_opzioni.tp_mrp.dw_selezione.getitemnumber(ll_getrow, "anno_lp")
	ll_num_lp = tab_opzioni.tp_mrp.dw_selezione.getitemnumber(ll_getrow, "num_lp")
	
	//struttura del datastore che sarà usato per disimpegnare
	// 		progr_det_produzione
	//		anno_registrazione
	//		num_registrazione
	//		prog_riga_ord_ven
	//		cod_reparto
	//		cod_prodotto
	if isnull(ll_num_lp) or ll_num_lp <=0 then
		//chiedi la lista dei barcode produzione da lavorare
		
		
		if g_mb.confirm("Vuoi scansionare i barcodes di produzione degli ordini da disimpegnare?") then
			li_ret = wf_leggi_ordini_mrp(ls_cod_reparto)
		
			if li_ret<0 then
				as_errore = "Ordini da disimpegnare non selezionati! Impossibile continuare!"
				return 1
			end if
		end if
		
	else
		//TO-DO
		//dalla lista prelievo
	end if
	
end if


if not g_mb.confirm("Procedo con l'elaborazione dei dati in data MRP "+string(ldt_data_rif_mrp, "dd/mm/yyyy")+" ?") then
	as_errore = "Annullato dall'operatore!"
	return 1
end if


luo_mrp = create uo_service_mov_mrp

setnull(li_anno_doc_origine)
setnull(ll_num_doc_origine)
setnull(ll_prog_riga_doc_origine)

for ll_index=1 to ll_tot
	ls_cod_tipo_mov_mrp = tab_opzioni.tp_mrp.dw_lista.getitemstring(ll_index, "cod_tipo_mov_mrp") 
	ls_cod_reparto = tab_opzioni.tp_mrp.dw_lista.getitemstring(ll_index, "cod_reparto")
	ls_cod_prodotto = tab_opzioni.tp_mrp.dw_lista.getitemstring(ll_index, "cod_prodotto")
	ld_quantita = tab_opzioni.tp_mrp.dw_lista.getitemdecimal(ll_index, "quan_ordine")
	
	//*****************************
	//TODO: gestire il valore unitario
	ld_valore_unitario = 0
	//*****************************
	
	ls_tipo_doc_origine = ""
	ls_flag_elaborato = "N"
	
	//passo la quantità del file come quan_prevista e quan_elaborata
	li_ret = luo_mrp.uof_insert_data(	ldt_data_rif_mrp, ls_cod_tipo_mov_mrp, ls_cod_prodotto, ld_quantita, ld_quantita, ld_valore_unitario, &
													ls_tipo_doc_origine, li_anno_doc_origine, ll_num_doc_origine, ll_prog_riga_doc_origine, ls_cod_reparto, ls_flag_elaborato, as_errore)
	
	if li_ret<0 then
		//si è verificato un errore
		return -1
	end if
	
next


//ora procedi al disimpegno, se previsto (da lista prelievo o selezione manuale ordini da disimpegnare)
ll_tot = ids_ordini.rowcount()
as_errore = ""

if lb_lista_prelievo then
	
	for ll_index=1 to ll_tot
		li_anno_doc_origine			= ids_ordini.getitemnumber(ll_index, "anno_registrazione")
		ll_num_doc_origine				= ids_ordini.getitemnumber(ll_index, "num_registrazione")
		ll_prog_riga_doc_origine		= ids_ordini.getitemnumber(ll_index, "prog_riga_ord_ven")
		
		//disimpegno
		update mov_mrp
		set flag_elaborato='S'
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					tipo_doc_origine='ORD_VEN' and
					anno_doc_origine=:li_anno_doc_origine and
					num_doc_origine=:ll_num_doc_origine and
					prog_riga_doc_origine=:ll_prog_riga_doc_origine and
					cod_reparto=:ls_cod_reparto;

		if sqlca.sqlcode<0 then
			as_errore = "Errore in disimpegno riga ordine "+string(li_anno_doc_origine)+"/"+string(ll_num_doc_origine)+"/"+string(ll_prog_riga_doc_origine)+&
								" e reparto "+ls_cod_reparto+" in tabella mov_mrp: "+sqlca.sqlerrtext
			return -1
		end if
		
		
	next
	
end if

//se hai letto da buffer_prodotti_qta segna come letto
ll_id_buffer = 0
ll_id_buffer_old = 0

for ll_index=1 to tab_opzioni.tp_mrp.dw_lista.rowcount()
	ll_id_buffer = tab_opzioni.tp_mrp.dw_lista.getitemnumber(ll_index, "id_buffer") 
	
	//se è zero esci subito, hai fatto il carico da file e non da buffer
	if ll_id_buffer=0 or isnull(ll_id_buffer) then exit
	
	if ll_id_buffer<>ll_id_buffer_old then
		
		//aggiorna il flag letto
		update buffer_prodotti_qta
		 set flag_letto='S'
		 where 	cod_azienda=:s_cs_xx.cod_azienda and
					id_buffer=:ll_id_buffer;
		
		//salva in old
		ll_id_buffer_old = ll_id_buffer
	end if
next

//reset dei due campi
tab_opzioni.tp_mrp.dw_selezione.setitem(ll_getrow, "id_buffer", 0)
tab_opzioni.tp_mrp.dw_selezione.setitem(ll_getrow, "id_buffer_fine", 0)

ids_ordini.reset()

destroy luo_mrp

return 0
end function

public function integer wf_leggi_ordini_mrp (string as_cod_reparto);
//per movimenti tipo "-" preparo un datastore con articolo, quantità e riga ordine di riferimento

s_cs_xx_parametri			lstr_parametri


lstr_parametri.parametro_ds_1 = ids_ordini
lstr_parametri.parametro_s_1 = as_cod_reparto

openwithparm(w_rileva_produzione_mrp_ordini, lstr_parametri)

lstr_parametri = message.powerobjectparm

if lstr_parametri.parametro_b_1 then
	ids_ordini = lstr_parametri.parametro_ds_1
	
	return 0
	
else
	//operazione annullata oppure si è verificato un errore
	return -1
end if

end function

public function integer wf_decodifica_mrp (long ai_riga, string as_input, ref string as_cod_tipo_mov_mrp, ref string as_cod_reparto, ref string as_cod_prodotto, ref decimal ad_quantita, ref string as_data_scansione, ref string as_errore);//tipico input  (tipoMov;reparto;articolo;quantità)
//	CAR;S_TS;A25312Z256;125.65				:caso con reparto
//  CAR;;A25312Z300;125.65       				:caso  senza reparto

//long ll_pos1, ll_pos2, ll_len
string			ls_temp, ls_flag_blocco, ls_code[]
long				ll_count, ll_getrow
datetime		ldt_data_blocco, ldt_data_mov_mrp



as_cod_prodotto = ""
setnull(ad_quantita)
as_errore = ""
ll_getrow = tab_opzioni.tp_mrp.dw_selezione.getrow()
ldt_data_mov_mrp = tab_opzioni.tp_mrp.dw_selezione.getitemdatetime(ll_getrow, "data_rif_mrp")

if isnull(as_input) or as_input = "" then
	//as_errore = "E' stato letto nel file un valore vuoto alla riga "+string(ai_riga) + ", pertanto questa riga verrà saltata!"
	return 1
end if

f_split(as_input, ";", ls_code)

//quindi as_input[] è fatto cosi:
//		as_input[1]      codice tipo mov. MRP						es.  "CAR"
//		as_input[2]      codice reparto								es.  "S_TS"  (potrebbe essere anche vuoto)
//		as_input[3]      prodotto										es.  "A20010416"
//		as_input[4]      quantità										es.  "14.00"


//tipo movimento MRP  -----------------------------------------------------------------
as_cod_tipo_mov_mrp = upper(ls_code[1])

if isnull(as_cod_tipo_mov_mrp) or as_cod_tipo_mov_mrp="" then
	as_errore =	"E' stato letto nel file un valore vuoto alla riga "+string(ai_riga) + &
							" per il campo tipo mov. MRP, pertanto questa riga verrà saltata!"
	return 1
end if

setnull(ll_count)

select count(*)
into :ll_count
from tab_tipi_mov_mrp
where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_tipo_mov_mrp=:as_cod_tipo_mov_mrp;

if sqlca.sqlcode=100 or ll_count<=0 or isnull(ll_count) then
	as_errore =	"E' stato letto nel file un valore inesistente in anagrafica alla riga "+string(ai_riga) + &
						" per il campo tipo mov. MRP ("+as_cod_tipo_mov_mrp+"), pertanto questa riga verrà saltata!"
	return 1
end if


//reparto MRP  ------------------------------------------------------------------------
if as_cod_reparto="" or isnull(as_cod_reparto) then
	as_cod_reparto = upper(ls_code[2])
	
	if isnull(as_cod_reparto) or as_cod_reparto="" then
		//reparto NON specificato nel file, considera quello specificato dalla selezione sulla finestra
		as_errore =	"E' stato letto nel file un valore vuoto alla riga "+string(ai_riga) + &
								" per il campo reparto e non è stato specificato alcun reparto in selezione, pertanto questa riga verrà saltata!"
		return 1
	end if
	
	setnull(ll_count)
	
	select count(*)
	into :ll_count
	from anag_reparti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_reparto=:as_cod_reparto and
					flag_mrp='S';
	
	if sqlca.sqlcode=100 or ll_count<=0 or isnull(ll_count) then
		as_errore =	"E' stato letto nel file un valore inesistente in anagrafica alla riga "+string(ai_riga) + &
								" per il campo reparto ("+as_cod_reparto+"), OPPURE non è abilitato alla gestione MRP pertanto questa riga verrà saltata!"
		return 1
	end if
	
end if


//quantità -----------------------------------------------------------------
//nota: se nel numero c'è il carattere "." sostituiscilo con la "," per garantire la corretta lettura dei decimali
ls_temp =  ls_code[4]
guo_functions.uof_replace_string(ls_temp, ".", ",")
ad_quantita = dec(ls_temp)
if isnull(ad_quantita) then
	as_errore ="E' stata letta nel file un valore nullo alla riga "+string(ai_riga) + " per il campo quantità, pertanto questa riga verrà saltata!"
	return 1
end if
//--------------------------------------------------------------------------


//data scansione -----------------------------------------------------------------
as_data_scansione =  ls_code[5]
if isnull(as_data_scansione) then as_data_scansione=""
//--------------------------------------------------------------------------




//l'elaborazione del campo articolo deve essere sempre ultimo
//articolo -----------------------------------------------------------------
as_cod_prodotto = upper(ls_code[3])
if isnull(as_cod_prodotto) or as_cod_prodotto="" then
	as_errore ="E' stato letto nel file un valore nullo alla riga "+string(ai_riga) + " per il campo prodotto, pertanto questa riga verrà saltata!"
	return 1
	
else
	
	//controlla se l'articolo esiste
	select count(*)
	into :ll_count
	from anag_prodotti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:as_cod_prodotto and
				flag_mrp='S';
				
	if sqlca.sqlcode=100 or ll_count<=0 or isnull(ll_count) then
		as_errore =	"E' stato letto nel file un valore inesistente in anagrafica alla riga "+string(ai_riga) + &
							" per il campo prodotto ("+as_cod_prodotto+"), OPPURE non è abilitato alla gestione MRP pertanto questa riga verrà saltata!"
		return 1
	end if
	
	
	//questo controllo deve essere fatto sempre per ultimo, in quanto un eventuale messaggio che ne consegue non deve
	//provacare il blocco della procedura ma solo un alert per singolo prodotto
	//controlla se l'articolo è stato bloccato
	select flag_blocco, data_blocco
	into :ls_flag_blocco, :ldt_data_blocco
	from anag_prodotti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:as_cod_prodotto;
	
	
	if ls_flag_blocco="S" then
		if isnull(ldt_data_blocco) or year(date(ldt_data_blocco))<1950 or ldt_data_blocco<=ldt_data_mov_mrp then
			//data blocco non specificata oppure la data blocco è anteriore o uguale alla data in cui si vuole fare il moviemnto MRP
			as_errore =	"E' stato letto nel file un valore bloccato in anagrafica alla riga "+string(ai_riga) + &
								" per il campo prodotto ("+as_cod_prodotto+"), la riga comunque verrà COMUNQUE ELABORATA!"
			return 0
		end if
	end if
	
end if
//--------------------------------------------------------------------------


return 0
end function

public function integer wf_stampa_documenti (string as_tipo_doc, integer ai_anno_doc, long al_num_doc, ref string as_errore);string							ls_sql, ls_tabella_tes, ls_tabella_det, ls_where_ordini[], ls_docs
datastore						lds_data
long								li_count
s_cs_xx_parametri			lstr_parametri



//as_tipo_doc   vale B o F   (DDT o Fattura)

if as_tipo_doc = "B" then
	ls_tabella_tes = "tes_bol_ven"
	ls_tabella_det = "det_bol_ven"
	ls_where_ordini[1] = 	"anno_registrazione_ord_ven"
	ls_where_ordini[2] = 	"num_registrazione_ord_ven"
	ls_where_ordini[3] = 	"prog_riga_ord_ven"
	ls_docs = "DDT di vendita o  trasferimento"
	
elseif as_tipo_doc = "F" then
	ls_tabella_tes = "tes_fat_ven"
	ls_tabella_det = "det_fat_ven"
	ls_where_ordini[1] = 	"anno_reg_ord_ven"
	ls_where_ordini[2] = 	"num_reg_ord_ven"
	ls_where_ordini[3] = 	"prog_riga_ord_ven"
	ls_docs = "Fattura di Vendita"
	
else
	as_errore = "Atteso Codice di una fattura o di un ddt!"
	return 1
end if
	

//------------------------------------------------------------------------------------------------------------
//controlla se il documento esiste
ls_sql = "select count(*) "+&
			"from "+ls_tabella_tes+" "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"anno_registrazione="+string(ai_anno_doc) + " and "+&
						"num_registrazione="+string(al_num_doc) + " "

if guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore) < 0 then
	return -1
end if

//ci sarà sempre una riga nel datastore, con il conteggio
li_count = lds_data.getitemnumber(1, 1)

destroy lds_data

if li_count > 0 then
else
	as_errore = "Documento "+ls_docs + " "+ string(ai_anno_doc)+"/"+string(al_num_doc)+" non trovato!"
	return 1
end if


//------------------------------------------------------------------------------------------------------------
//controlla se ci sono righe con riferimenti a ordini di vendita
ls_sql = "select count(*) "+&
			"from "+ls_tabella_det+" "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"anno_registrazione="+string(ai_anno_doc) + " and "+&
						"num_registrazione="+string(al_num_doc) + " and " +&
						ls_where_ordini[1] + ">0 and  "+ls_where_ordini[1] + " is not null and "+&
						ls_where_ordini[2] + ">0 and  "+ls_where_ordini[2] + " is not null and "+&
						ls_where_ordini[3] + ">0 and  "+ls_where_ordini[3] + " is not null "
						
if guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore) < 0 then
	return -1
end if

//ci sarà sempre una riga nel datastore, con il conteggio
li_count = lds_data.getitemnumber(1, 1)

destroy lds_data

if li_count > 0 then
else
	as_errore = "Non sembrano esserci righe con riferimenti a ordini di vendita nel Documento " + ls_docs +" " + string(ai_anno_doc) + "/" + string(al_num_doc)
	return 1
end if


//------------------------------------------------------------------------------------------------------------
//se arrivi fin qui apri pure la window
lstr_parametri.parametro_d_1 = ai_anno_doc
lstr_parametri.parametro_d_2 = al_num_doc
lstr_parametri.parametro_s_14 = as_tipo_doc

openwithparm(w_documenti_ordini, lstr_parametri)

return 0
end function

public function integer wf_ddt_trasf_mrp (string as_barcode, ref string as_errore);integer			li_anno_ddt
long				ll_num_ddt, ll_count
string			ls_cod_tipo_bol_ven, ls_flag_blocco, ls_flag_tipo_bol_ven

s_cs_xx_parametri		lstr_parametri

//controlli sul formato del codice a barre ------------------------------------------------------
if len(as_barcode) < 11 then
	as_errore = "Formato codice a barre DDT non riconosciuto!"
	return -1
end if
	
if left(as_barcode, 1) <> "B" then
	as_errore = "E' possibile inserire solo documenti di tipo DDT (prima lettera codice a barre B)"
	return -1
end if

li_anno_ddt = long(mid(as_barcode, 2, 4))
ll_num_ddt = long(mid(as_barcode, 6, 6))

//controlla se il ddt esiste ed è del tipo previsto (di trasferimento) ----------------------
select cod_tipo_bol_ven, flag_blocco
into :ls_cod_tipo_bol_ven, :ls_flag_blocco
from tes_bol_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:li_anno_ddt and
			num_registrazione=:ll_num_ddt;

if sqlca.sqlcode<0 then
	as_errore = "Errore controllo DDT di trasferimento"+string(li_anno_ddt)+"/"+string(ll_num_ddt) + " : "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	as_errore = "Sembra che il DDT di trasferimento "+string(li_anno_ddt)+"/"+string(ll_num_ddt) + " non sia presente nel database!"
	return -1
end if

if ls_flag_blocco="S" then
	as_errore = "Il DDT  "+string(li_anno_ddt)+"/"+string(ll_num_ddt) + " risulta bloccato!"
	return -1
end if

if ls_cod_tipo_bol_ven="" or isnull(ls_cod_tipo_bol_ven) then
	as_errore = "Sembra che nel DDT di trasferimento "+string(li_anno_ddt)+"/"+string(ll_num_ddt) + " non sia stata assegnata la tipologia (Codice Tipo Bolla)!"
	return -1
end if

select flag_tipo_bol_ven
into :ls_flag_tipo_bol_ven
from tab_tipi_bol_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_bol_ven=:ls_cod_tipo_bol_ven;

if sqlca.sqlcode<0 then
	as_errore = "Errore controllo tipo DDT trasferimento "+string(li_anno_ddt)+"/"+string(ll_num_ddt) + " : "+sqlca.sqlerrtext
	return -1
	
else
	if ls_flag_tipo_bol_ven<>"T" then
		as_errore = "Il DDT "+string(li_anno_ddt)+"/"+string(ll_num_ddt) + " non è di Trasferimento!"
		return -1
	end if
end if


//controlla che il ddt di trasferimento non sia stato già elaborato -----------------------
select count(*)
into :ll_count
from det_bol_ven as a
join anag_prodotti as p on 	p.cod_azienda=a.cod_azienda and
										p.cod_prodotto=a.cod_prodotto
where 	a.cod_azienda=:s_cs_xx.cod_azienda and
			a.anno_registrazione=:li_anno_ddt and
			a.num_registrazione=:ll_num_ddt and
			p.flag_mrp='S';
			
if sqlca.sqlcode<0 then
	as_errore = "Errore controllo dettaglio DDT di trasferimento"+string(li_anno_ddt)+"/"+string(ll_num_ddt) + " : "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 or ll_count=0 or isnull(ll_count) then
	as_errore = "Sembra che il DDT di trasferimento "+string(li_anno_ddt)+"/"+string(ll_num_ddt) + " non abbia righe (oppure i prodotti non sono abilitati per MRP)!"
	return -1
end if


//controlli superati, apri finestra con riepilogo righe del ddt
lstr_parametri.parametro_ul_1 = li_anno_ddt
lstr_parametri.parametro_ul_2 = ll_num_ddt
openwithparm(w_rileva_prod_mrp_ddt, lstr_parametri)


return 0
end function

public function integer wf_etichetta_rdt (integer ai_anno_rdt, long al_num_rdt, integer al_num_copie, ref string as_errore);integer			li_anno_ordine
long				ll_num_ordine, ll_riga_ordine, ll_barcode_produzione, job, ll_index
string				ls_cod_reparto_richiedente, ls_cod_deposito_richiedente, ls_deposito_richiedente, ls_cod_deposito_utente, ls_deposito_utente,&
					ls_cod_giro_consegna, ls_cod_prodotto, ls_des_prodotto, ls_valore, ls_cod_utente_evasione, ls_cod_cliente, ls_rag_soc_1
dec{4}			ld_quan_ordine, ld_dim_x, ld_dim_y, ld_dim_z, ld_dim_t
datetime			ldt_data_pronto, ldt_oggi, ldt_ora, ldt_data_fabbisogno
uo_produzione	luo_prod


if al_num_copie>0 then
else
	as_errore = "Digitare il numero di copie di etichette da stampare!"
	return 1
end if

select 	anno_ordine,
			num_ordine,
			riga_ordine,
			cod_reparto_richiedente,
			barcode_det,
			cod_utente_evasione,
			data_fabbisogno
into 	:li_anno_ordine,
		:ll_num_ordine,
		:ll_riga_ordine,
		:ls_cod_reparto_richiedente,
		:ll_barcode_produzione,
		:ls_cod_utente_evasione,
		:ldt_data_fabbisogno
from tes_rdt
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_rdt and
			num_registrazione=:al_num_rdt;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in lettura testata RDT: "+sqlca.sqlerrtext
	return -1
//elseif sqlca.sqlcode=100 or isnull(li_anno_ordine) or li_anno_ordine<=0 then
elseif sqlca.sqlcode=100 then
	as_errore = "La RDT "+string(ai_anno_rdt)+"/"+string(al_num_rdt)+" non è stata trovata!"
	return 1
end if

//deposito che manda il materiale (quello dell'utente collegato)
guo_functions.uof_get_stabilimento_utente(ls_cod_deposito_utente, as_errore)
if isnull(ls_cod_deposito_utente) or len(ls_cod_deposito_utente) < 1 then
	as_errore = "L'utente collegato non ha il deposito assegnato (tabella operatori_utenti)! Impossibile stampare l'etichetta!"
	return 1
else
	select des_deposito
	into :ls_deposito_utente
	from anag_depositi
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_deposito=:ls_cod_deposito_utente;
end if


//deposito che riceverà la merce (quello già memorizzato nella rdt)
select anag_reparti.cod_deposito, anag_depositi.des_deposito
into :ls_cod_deposito_richiedente, :ls_deposito_richiedente
from anag_reparti
join anag_depositi on anag_depositi.cod_azienda=anag_reparti.cod_azienda and
							anag_depositi.cod_deposito=anag_reparti.cod_deposito
where 	anag_reparti.cod_azienda=:s_cs_xx.cod_azienda and
			anag_reparti.cod_reparto=:ls_cod_reparto_richiedente;


if li_anno_ordine>0 and ll_num_ordine>0 and ll_riga_ordine>0 then
	//RDT da riga ordine o codice a barre produzione e non a composizione libera
	
	//dati aggiuntivi riga ordine
	select 	tes_ord_ven.cod_giro_consegna,
				det_ord_ven.cod_prodotto, det_ord_ven.des_prodotto,
				det_ord_ven.quan_ordine,
				comp_det_ord_ven.dim_x, comp_det_ord_ven.dim_y,
				comp_det_ord_ven.dim_z, comp_det_ord_ven.dim_t,
				tes_ord_ven.cod_cliente, anag_clienti.rag_soc_1
	into		:ls_cod_giro_consegna,
				:ls_cod_prodotto, :ls_des_prodotto,
				:ld_quan_ordine, 
				:ld_dim_x, :ld_dim_y, 
				:ld_dim_z, :ld_dim_t,
				:ls_cod_cliente, :ls_rag_soc_1
	from det_ord_ven
	join tes_ord_ven on 	tes_ord_ven.cod_azienda=det_ord_ven.cod_azienda and
								tes_ord_ven.anno_registrazione=det_ord_ven.anno_registrazione and
								tes_ord_ven.num_registrazione=det_ord_ven.num_registrazione
	join anag_clienti on 	anag_clienti.cod_azienda=tes_ord_ven.cod_azienda and
								anag_clienti.cod_cliente=tes_ord_ven.cod_cliente
	join comp_det_ord_ven on 	comp_det_ord_ven.cod_azienda=det_ord_ven.cod_azienda and
										comp_det_ord_ven.anno_registrazione=det_ord_ven.anno_registrazione and
										comp_det_ord_ven.num_registrazione=det_ord_ven.num_registrazione and
										comp_det_ord_ven.prog_riga_ord_ven=det_ord_ven.prog_riga_ord_ven
	where 	det_ord_ven.cod_azienda=:s_cs_xx.cod_azienda and
				det_ord_ven.anno_registrazione=:li_anno_ordine and
				det_ord_ven.num_registrazione=:ll_num_ordine and
				det_ord_ven.prog_riga_ord_ven=:ll_riga_ordine;


	//data pronto reparto richiedente
	select det_ordini_produzione.data_pronto
	into	:ldt_data_pronto
	from det_ordini_produzione
	where 	det_ordini_produzione.cod_azienda=:s_cs_xx.cod_azienda and
				det_ordini_produzione.progr_det_produzione=:ll_barcode_produzione;


	dw_etichetta_rdt.object.data_pronto_t.text = "pronto"
	
	dw_etichetta_rdt.object.cod_modello_t.text = "Mod"
	dw_etichetta_rdt.object.modello_t.text = "Dim"
	dw_etichetta_rdt.object.dimensioni_t.text = "Q.tà"
	dw_etichetta_rdt.object.quantita_t.text = "Cliente"
	dw_etichetta_rdt.object.cliente_t.text = "codice"
else
	//RDT a composizione libera e non da riga ordine o codice a barre produzione
	
	setnull(ll_num_ordine)
	setnull(ll_num_ordine)
	setnull(ll_riga_ordine)
	
	ls_cod_giro_consegna = ""
	ls_cod_prodotto = ""
	ls_des_prodotto = ""
	setnull(ld_quan_ordine)
	setnull(ld_dim_x)
	setnull(ld_dim_y)
	setnull(ld_dim_z)
	setnull(ld_dim_t)
	ls_cod_cliente = ""
	ls_rag_soc_1 = ""
	
	
	//prendo la data del fabbisogno materiale indicato in RDT come data pronto
	ldt_data_pronto = ldt_data_fabbisogno
	
	dw_etichetta_rdt.object.data_pronto_t.text = "fabb."
	
	dw_etichetta_rdt.object.cod_modello_t.text = ""
	dw_etichetta_rdt.object.modello_t.text = ""
	dw_etichetta_rdt.object.dimensioni_t.text = ""
	dw_etichetta_rdt.object.quantita_t.text = ""
	dw_etichetta_rdt.object.cliente_t.text = ""
	
end if

dw_etichetta_rdt.reset()
dw_etichetta_rdt.insertrow(0)

dw_etichetta_rdt.setitem(1, "num_registrazione", string(ll_num_ordine))
dw_etichetta_rdt.setitem(1, "anno_registrazione", string(li_anno_ordine))

luo_prod = create uo_produzione
ls_valore = ""
//ls_valore = luo_prod.uof_get_riga_su_tot_righe(li_anno_ordine, ll_num_ordine, ll_riga_ordine)
//dw_etichetta_rdt.setitem(1, "num_riga_num_tot", ls_valore)
dw_etichetta_rdt.setitem(1, "num_riga_num_tot", string(ll_riga_ordine))

ls_valore = luo_prod.uof_trasforma_data(date(ldt_data_pronto))
dw_etichetta_rdt.setitem(1, "data_pronto", ls_valore)

destroy luo_prod;


dw_etichetta_rdt.setitem(1, "giro_consegna", ls_cod_giro_consegna)
dw_etichetta_rdt.setitem(1, "cod_prodotto_mod", ls_cod_prodotto)
dw_etichetta_rdt.setitem(1, "des_prodotto_mod", ls_des_prodotto)
dw_etichetta_rdt.setitem(1, "dim_x", string(ld_dim_x, "###,##0.00"))
dw_etichetta_rdt.setitem(1, "dim_y", string(ld_dim_y, "###,##0.00"))
dw_etichetta_rdt.setitem(1, "dim_z", string(ld_dim_z, "###,##0.00"))
dw_etichetta_rdt.setitem(1, "dim_t", string(ld_dim_t, "###,##0.00"))
dw_etichetta_rdt.setitem(1, "quan_ordine", string(ld_quan_ordine, "###,##0.00"))

dw_etichetta_rdt.setitem(1, "cliente", ls_cod_cliente + " " + ls_rag_soc_1)

ls_valore = "*R"+string(ai_anno_rdt)+ right("000000" + string(al_num_rdt), 6)+"*"
dw_etichetta_rdt.setitem(1, "barcode", ls_valore)

//quello che riceverà
dw_etichetta_rdt.setitem(1, "cod_deposito", left(ls_deposito_richiedente, 3))
ls_valore = ls_deposito_richiedente + " ("+ls_cod_reparto_richiedente+")"
dw_etichetta_rdt.setitem(1, "des_deposito_a", ls_valore)

//quello che spedisce (quello dell'utente)
dw_etichetta_rdt.setitem(1, "des_deposito_da", ls_deposito_utente)


//ora procedi alla stampa
job = PrintOpen()
for ll_index = 1 to al_num_copie
	PrintDataWindow(job, dw_etichetta_rdt)
next
PrintClose(job)

//TO.DO.
//evadi la RDT in tabella, se ancora non è stata evasa
if isnull(ls_cod_utente_evasione) or ls_cod_utente_evasione="" then

	ldt_oggi = datetime(today(), 00:00:00)
	ldt_ora = datetime(date(1900,1,1), now())

	update tes_rdt
	set 	cod_deposito_evasione=:ls_cod_deposito_utente,
			cod_utente_evasione=:s_cs_xx.cod_utente,
			data_evasione=:ldt_oggi,
			ora_evasione=:ldt_ora
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_rdt and
				num_registrazione=:al_num_rdt;
				
	if sqlca.sqlcode = 0 then
		commit;
	end if
			
end if
			


return 0
end function

public function integer wf_ristampa_rdt (integer ai_anno_rdt, long al_num_rdt, ref string as_errore);//integer			li_anno_ordine
//long				ll_num_ordine, ll_riga_ordine, ll_barcode_produzione
//string				ls_cod_reparto_richiedente, 
//
//
//select 	anno_ordine, num_ordine, riga_ordine, cod_reparto_richiedente, barcode_det
//into 	:li_anno_ordine, :ll_num_ordine, :ll_riga_ordine, :ls_cod_reparto_richiedente, :ll_barcode_produzione
//from tes_rdt
//where 	cod_azienda=:s_cs_xx.cod_azienda and
//			anno_registrazione=:ai_anno_rdt and
//			num_registrazione=:al_num_rdt;
//
//if sqlca.sqlcode < 0 then
//	as_errore = "Errore in lettura testata RDT: "+sqlca.sqlerrtext
//	return -1
//elseif sqlca.sqlcode=100 or isnull(li_anno_ordine) or li_anno_ordine<=0 then
//	as_errore = "La RDT "+string(ai_anno_rdt)+"/"+string(al_num_rdt)+" non è stata trovata!"
//	return 1
//end if
//
//s_cs_xx.parametri.parametro_d_2_a[1] = 6969
//s_cs_xx.parametri.parametro_d_2_a[2] = ai_anno_rdt
//s_cs_xx.parametri.parametro_d_2_a[3] = al_num_rdt
//
//
//
////deposito che manda il materiale (quello dell'utente collegato)
//guo_functions.uof_get_stabilimento_utente(ls_cod_deposito_utente, as_errore)
//if isnull(ls_cod_deposito_utente) or len(ls_cod_deposito_utente) < 1 then
//	as_errore = "L'utente collegato non ha il deposito assegnato (tabella operatori_utenti)! Impossibile stampare l'etichetta!"
//	return 1
//else
//	select des_deposito
//	into :ls_deposito_utente
//	from anag_depositi
//	where 	cod_azienda=:s_cs_xx.cod_azienda and
//				cod_deposito=:ls_cod_deposito_utente;
//end if
//
//
////deposito che riceverà la merce (quello già memorizzato nella rdt)
//select anag_reparti.cod_deposito, anag_depositi.des_deposito
//into :ls_cod_deposito_richiedente, :ls_deposito_richiedente
//from anag_reparti
//join anag_depositi on anag_depositi.cod_azienda=anag_reparti.cod_azienda and
//							anag_depositi.cod_deposito=anag_reparti.cod_deposito
//where 	anag_reparti.cod_azienda=:s_cs_xx.cod_azienda and
//			anag_reparti.cod_reparto=:ls_cod_reparto_richiedente;
//
//
////dati aggiuntivi riga ordine
//select 	tes_ord_ven.cod_giro_consegna,
//			det_ord_ven.cod_prodotto, det_ord_ven.des_prodotto,
//			det_ord_ven.quan_ordine,
//			comp_det_ord_ven.dim_x, comp_det_ord_ven.dim_y,
//			comp_det_ord_ven.dim_z, comp_det_ord_ven.dim_t
//into		:ls_cod_giro_consegna,
//			:ls_cod_prodotto, :ls_des_prodotto,
//			:ld_quan_ordine, 
//			:ld_dim_x, :ld_dim_y, 
//			:ld_dim_z, :ld_dim_t
//from det_ord_ven
//join tes_ord_ven on 	tes_ord_ven.cod_azienda=det_ord_ven.cod_azienda and
//							tes_ord_ven.anno_registrazione=det_ord_ven.anno_registrazione and
//							tes_ord_ven.num_registrazione=det_ord_ven.num_registrazione
//join comp_det_ord_ven on 	comp_det_ord_ven.cod_azienda=det_ord_ven.cod_azienda and
//									comp_det_ord_ven.anno_registrazione=det_ord_ven.anno_registrazione and
//									comp_det_ord_ven.num_registrazione=det_ord_ven.num_registrazione and
//									comp_det_ord_ven.prog_riga_ord_ven=det_ord_ven.prog_riga_ord_ven
//where 	det_ord_ven.cod_azienda=:s_cs_xx.cod_azienda and
//			det_ord_ven.anno_registrazione=:li_anno_ordine and
//			det_ord_ven.num_registrazione=:ll_num_ordine and
//			det_ord_ven.prog_riga_ord_ven=:ll_riga_ordine;
//
//
////data pronto reparto richiedente
//select det_ordini_produzione.data_pronto
//into	:ldt_data_pronto
//from det_ordini_produzione
//where 	det_ordini_produzione.cod_azienda=:s_cs_xx.cod_azienda and
//			det_ordini_produzione.progr_det_produzione=:ll_barcode_produzione;
//
//
////is_cod_prodotto = s_cs_xx.parametri.parametro_s_1_a[1]
////is_des_prodotto = s_cs_xx.parametri.parametro_s_1_a[2]
////il_anno_registrazione = s_cs_xx.parametri.parametro_d_1_a[1]
////il_num_registrazione = s_cs_xx.parametri.parametro_d_1_a[2]
////il_prog_riga_ord_ven = s_cs_xx.parametri.parametro_d_1_a[3]
////il_max_livelli = s_cs_xx.parametri.parametro_d_1_a[4]
////il_barcode = s_cs_xx.parametri.parametro_d_1_a[5]
////id_quan_ordinata = s_cs_xx.parametri.parametro_d_1_a[6]
////il_anno_commessa = s_cs_xx.parametri.parametro_d_1_a[7]
////il_num_commessa = s_cs_xx.parametri.parametro_d_1_a[8]
////idt_data_consegna = s_cs_xx.parametri.parametro_data_3
////idt_data_pronto = s_cs_xx.parametri.parametro_data_4
//
//
//window_open(w_report_db_varianti_componenti, -1)
//

return 0
end function

public subroutine wf_help ();uo_web luo_web
string ls_iperlink, ls_password, ls_cod_cliente, ls_window


ls_window = "w_rileva_produzione"

if not isnull(s_cs_xx.pwd_manuale) and len(s_cs_xx.pwd_manuale) > 0 then
	
	ls_password = s_cs_xx.pwd_manuale + string(today(),"yyyyMMdd") + string(now(),"hhmm")
	
	guo_functions.uof_crypt(ls_password, ref ls_password)
	
	ls_iperlink = "http://kb.csteam.com/auth/cs.do?c=" + ls_password
//	if isvalid(pcca.window_current) then
//		ls_iperlink += "&tag=" +  + pcca.window_current.classname()
//	end if
	ls_iperlink += "&tag="+ls_window
	
	select cod_cliente
	into :ls_cod_cliente
	from sistema;
	
	if sqlca.sqlcode = 0 and not isnull(ls_cod_cliente) and len(ls_cod_cliente) > 0 then 	
		ls_iperlink += "&cl=" + ls_cod_cliente
	else
		ls_iperlink += "&cl=xxx"
	end if
	
	luo_web.openwebpage(ls_iperlink)
	
end if	

end subroutine

public function integer wf_controllo_blocco_cliente (long al_barcode, integer ai_anno, long al_numero, ref string as_errore);string				ls_cod_cliente
integer			li_ret

if ai_anno>0 then
	select cod_cliente
	into :ls_cod_cliente
	from tes_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno and
				num_registrazione=:al_numero;
	
	if sqlca.sqlcode<0 then
		return -1
	elseif sqlca.sqlcode=100 then
		as_errore = "Attenzione: ordine "+string(ai_anno)+"/"+string(al_numero)+" inesistente"
		return -1
	end if
	
else
	li_ret = iuo_produzione.uof_trova_cliente(al_barcode, ls_cod_cliente, as_errore)
	
	if li_ret<0 then
		return -1
	end if
	
end if


li_ret = iuo_fido.uof_get_blocco_cliente(	ls_cod_cliente,&
													datetime(today(), 00:00:00), &
													is_cod_parametro_blocco, &
													as_errore)
if li_ret=2 then
	//blocco
	return 2
elseif li_ret=1 then
	//solo warning
	return 1
end if

return 0
end function

public function integer wf_avanza_commessa (integer ai_anno_commessa, long al_num_commessa, ref string as_errore);s_cs_xx_parametri				lstr_parametri

string								ls_muovi_sl, ls_cod_prodotto_finito, ls_cod_versione, ls_null, ls_flag_tipo_avanzamento, ls_cod_tipo_commessa, ls_ret, &
									ls_cod_deposito_prelievo, ls_cod_deposito_versamento, ls_sql, ls_des_prodotto, ls_cod_tipo_mov_prel_mat_prime, ls_cod_tipo_mov_ver_prod_finiti
									
boolean							lb_muovi_sl
dec{4}							ld_qta_pf, ldd_quan_ordine, ldd_quan_prodotta, ldd_tot_valore_pf

long								ll_lead_time_cumulato, ll_index, ll_tot, ll_new
s_fabbisogno_commessa		lstr_fabbisogno[]

int									li_ret
datetime							ldt_null, ldt_data_chiusura



setnull(ls_null)
setnull(ldt_null)


//leggo i dati della commessa
select		flag_tipo_avanzamento,
			data_chiusura,
			quan_ordine,
			quan_prodotta,
			cod_prodotto,
			cod_versione,
			cod_tipo_commessa,
			cod_deposito_prelievo,
			cod_deposito_versamento,
			tot_valore_pf
into	:ls_flag_tipo_avanzamento,
		:ldt_data_chiusura,
		:ldd_quan_ordine,
		:ldd_quan_prodotta,
		:ls_cod_prodotto_finito,
		:ls_cod_versione,
		:ls_cod_tipo_commessa,
		:ls_cod_deposito_prelievo,
		:ls_cod_deposito_versamento,
		:ldd_tot_valore_pf
from anag_commesse
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:ai_anno_commessa and
			num_commessa=:al_num_commessa;

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura dati commessa: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	as_errore = "La commessa è inesistente!"
	return 1
end if


if ls_cod_prodotto_finito="" or isnull(ls_cod_prodotto_finito) or ls_cod_versione="" or isnull(ls_cod_versione) then
	as_errore = "Manca il prodotto finito nella commessa o la sua versione in distinta base da utilizzare!"
	return 1
end if

if ls_cod_tipo_commessa="" or isnull(ls_cod_tipo_commessa) then
	as_errore = "Manca l'impostazione del tipo nella commessa!"
	return 1
end if

select		cod_tipo_mov_prel_mat_prime,
			cod_tipo_mov_ver_prod_finiti
into		:ls_cod_tipo_mov_prel_mat_prime,
			:ls_cod_tipo_mov_ver_prod_finiti
from tab_tipi_commessa
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_tipo_commessa=:ls_cod_tipo_commessa;


if isnull(ldd_quan_ordine) then ldd_quan_ordine = 0
if isnull(ldd_quan_prodotta) then ldd_quan_prodotta = 0
if isnull(ldd_tot_valore_pf) then ldd_tot_valore_pf = 0
if year(date(ldt_data_chiusura)) <= 1980 then setnull(ldt_data_chiusura)

//residuo da produrre
ld_qta_pf = ldd_quan_ordine - ldd_quan_prodotta

if	ls_flag_tipo_avanzamento="8" or ls_flag_tipo_avanzamento="9" or ls_flag_tipo_avanzamento="7" or &
	ld_qta_pf<=0 or not isnull(ldt_data_chiusura) 				then
	
	as_errore = "La commessa è già chiusa!"
	return 1
end if

if isnull(ls_cod_deposito_prelievo) or ls_cod_deposito_prelievo="" then
	as_errore = "Manca il deposito di prelievo materie prime della commessa!"
	return 1
end if

if isnull(ls_cod_deposito_versamento) or ls_cod_deposito_versamento="" then
	as_errore = "Manca il deposito di versamento prodotto finito della commessa!"
	return 1
end if

if ldd_tot_valore_pf=0 then
	if not g_mb.confirm("Attenzione: il valore del semilavorato della commessa in fase di carico sarà ZERO! Sicuro di voler continuare?") then
		as_errore = ""
		return 1
	end if
end if


//preparazione per passaggio parametri
lstr_parametri.parametro_ul_1 = ai_anno_commessa
lstr_parametri.parametro_ul_2 = al_num_commessa

lstr_parametri.parametro_s_1_a[1] = ls_cod_prodotto_finito
lstr_parametri.parametro_s_1_a[2] = ls_cod_versione
lstr_parametri.parametro_s_1_a[3] = ls_cod_deposito_prelievo
lstr_parametri.parametro_s_1_a[4] = ls_cod_deposito_versamento
lstr_parametri.parametro_s_1_a[5] = ls_cod_tipo_mov_prel_mat_prime
lstr_parametri.parametro_s_1_a[6] = ls_cod_tipo_mov_ver_prod_finiti

lstr_parametri.parametro_d_1_a[1] = ldd_quan_ordine			//ordinata
lstr_parametri.parametro_d_1_a[2] = ld_qta_pf					//residua da proporre (calcolata come ordinata - prodotta)
lstr_parametri.parametro_d_1_a[3] = ldd_quan_prodotta		//eventuale già prodotta

lstr_parametri.parametro_d_1_a[4] = ldd_tot_valore_pf


//apro finestra per gestire scarichi, carichi e tutto il resto
openwithparm(w_avanzamento_commessa, lstr_parametri)

as_errore = message.stringparm

if as_errore="" then
	//commit già fatto nella finestra w_avanzamento_commessa
	return 0
else
	//messaggio da dare (per ora è solo "Annullato dall'operatore")
	return -1
end if

end function

public function integer wf_etichetta_commessa (integer ai_anno_commessa, long al_num_commessa, ref string as_errore);
s_cs_xx_parametri			lstr_parametri


lstr_parametri.parametro_ul_1 = ai_anno_commessa
lstr_parametri.parametro_ul_2 = al_num_commessa


openwithparm(w_stampa_etic_commessa, lstr_parametri)

return 0
end function

public function integer wf_stampa_etichette_spedizione (string as_barcode);/**
 * stefanop
 * 06/07/2016
 *
 * Stampa etichette spedizione
 * Specifica "Stampa_ordini_etichette" Plastinds
 **/
 
string ls_cod_cliente, ls_rag_soc_1, ls_rag_soc_abbreviata, ls_des_cliente, ls_localita[], ls_num_ord_cliente, ls_cod_reparto, ls_rif_interscambio, ls_rag_soc_1_diversa
long ll_num_registrazione, li_barcode, li_anno_registrazione, li_row, li_num_colli
str_stampa_etichette_colli lstr_data

// Controllare se è un intero o una stringa
li_barcode = long(as_barcode)

select distinct 	tes_ord_ven.anno_registrazione, 
					tes_ord_ven.num_registrazione, 
					tes_ord_ven.cod_cliente, 
					tes_ord_ven.rag_soc_1, 
					tes_ord_ven.localita, 
					tes_ord_ven.num_ord_cliente,
					tes_ord_ven.rif_interscambio,
					det_ordini_produzione.cod_reparto
			into	:li_anno_registrazione, 
					:ll_num_registrazione, 
					:ls_cod_cliente, 
					:ls_rag_soc_1_diversa,
					:ls_localita[1], 
					:ls_num_ord_cliente, 
					:ls_rif_interscambio, 
					:ls_cod_reparto
			from	det_ordini_produzione
					join 	tes_ord_ven on
							tes_ord_ven.cod_azienda = det_ordini_produzione.cod_azienda and
							tes_ord_ven.anno_registrazione = det_ordini_produzione.anno_registrazione and
							tes_ord_ven.num_registrazione = det_ordini_produzione.num_registrazione
		where		det_ordini_produzione.cod_azienda=:s_cs_xx.cod_azienda and 
					(det_ordini_produzione.progr_det_produzione = :li_barcode or det_ordini_produzione.progr_tes_produzione = :li_barcode);
if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il controllo del barcode nella produzione.", sqlca)
	return -1
elseif sqlca.sqlcode = 100 then
	g_mb.warning("Il barcode non è stato trovato nella produzione.")
	return -1
end if

lstr_data.anno_registrazione = li_anno_registrazione
lstr_data.num_registrazione = ll_num_registrazione
OpenWithParm(w_stampa_etichette_colli, lstr_data, this)
lstr_data = message.powerobjectparm
li_num_colli = lstr_data.num_colli

select rag_soc_1, rag_soc_abbreviata, localita
into :ls_rag_soc_1, :ls_rag_soc_abbreviata, :ls_localita[2]
from anag_clienti
where cod_azienda = :s_cs_xx.cod_azienda and cod_cliente = :ls_cod_cliente;

if g_str.isnotempty(ls_rag_soc_abbreviata) then
	ls_des_cliente =  ls_rag_soc_abbreviata
else
	ls_des_cliente = ls_rag_soc_1
end if
ls_des_cliente = left(ls_des_cliente, 20)


// resetto Dw
dw_etichette_spedizione.reset()
dw_etichette_spedizione.dataobject = "d_etichette_spedizione"
li_row = dw_etichette_spedizione.insertrow(0)

// dal ls_num_ord_cliente tolgo l'anno (prendo ciò che sta a destra del carattere "/"
if pos(ls_num_ord_cliente,"/") > 0 then
	ls_num_ord_cliente = mid(ls_num_ord_cliente,pos(ls_num_ord_cliente,"/") + 1)
end if

// Carico dati
dw_etichette_spedizione.setitem(li_row, "anno_registrazione", li_anno_registrazione)
dw_etichette_spedizione.setitem(li_row, "num_registrazione", ll_num_registrazione)
dw_etichette_spedizione.setitem(li_row, "cod_cliente", ls_cod_cliente)
dw_etichette_spedizione.setitem(li_row, "num_ord_cliente", ls_num_ord_cliente)
dw_etichette_spedizione.setitem(li_row, "cod_reparto", ls_cod_reparto)
dw_etichette_spedizione.setitem(li_row, "num_colli", li_num_colli)
dw_etichette_spedizione.setitem(li_row, "rif_interscambio", ls_rif_interscambio)

if g_str.isnotempty(ls_rag_soc_1_diversa) then
	dw_etichette_spedizione.setitem(li_row, "des_cliente", left(ls_rag_soc_1_diversa, 20))
else
	dw_etichette_spedizione.setitem(li_row, "des_cliente", ls_des_cliente)
end if


if g_str.isnotempty(ls_localita[1]) then
	dw_etichette_spedizione.setitem(li_row, "localita", ls_localita[1])
else
	dw_etichette_spedizione.setitem(li_row, "localita", ls_localita[2])
end if


dw_etichette_spedizione.print()
 
return 1
end function

public function integer wf_stampa_etichette_tenda (string as_barcode, string as_formato);/* -------------------------------------------------------------
 stefanop 06/07/2016
 Stampa etichette spedizione
 Specifica "Stampa_ordini_etichette" Plastinds
 
 EnMe 20/06/2017
 Specifica Etichette_ridotte rev 1 X Plastinds
 Aggiunto parametro as_formato con valore N=Normale  R=Ridotto
 **/
 
string ls_cod_cliente, ls_des_cliente, ls_num_ord_cliente, ls_cod_reparto, ls_sql, ls_cod_prodotto, ls_des_prodotto, ls_cod_colore_tessuto, &
		ls_cod_variabile_dim_x, ls_cod_variabile_dim_y
long ll_num_registrazione, li_barcode, li_anno_registrazione, li_row, li_num_colli, li_rows, li_i, li_prog_riga_ord_ven, ll_y
decimal{4} ld_dim_x, ld_dim_y, ld_quan_ordine
date ldt_data_ordine[]
str_stampa_etichette_colli lstr_data
datastore lds_store

// resetto Dw
dw_etichette_spedizione.reset()
if as_formato ="N" then
	dw_etichette_spedizione.dataobject = "d_etichette_spedizione_tenda"
else
	dw_etichette_spedizione.dataobject = "d_etichette_spedizione_tenda_ridotta"
end if
// Controllare se è un intero o una stringa
li_barcode = long(as_barcode)

select distinct tes_ord_ven.anno_registrazione, tes_ord_ven.num_registrazione, tes_ord_ven.cod_cliente,
tes_ord_ven.data_registrazione, tes_ord_ven.data_ord_cliente,
anag_clienti.rag_soc_1
into :li_anno_registrazione, :ll_num_registrazione, :ls_cod_cliente,  :ldt_data_ordine[1], :ldt_data_ordine[2], :ls_des_cliente
from det_ordini_produzione
join tes_ord_ven on
tes_ord_ven.cod_azienda = det_ordini_produzione.cod_azienda and
tes_ord_ven.anno_registrazione = det_ordini_produzione.anno_registrazione and
tes_ord_ven.num_registrazione = det_ordini_produzione.num_registrazione
join anag_clienti on
anag_clienti.cod_azienda = tes_ord_ven.cod_azienda and
anag_clienti.cod_cliente = tes_ord_ven.cod_cliente
where
det_ordini_produzione.cod_azienda=:s_cs_xx.cod_azienda and 
(det_ordini_produzione.progr_det_produzione = :li_barcode or det_ordini_produzione.progr_tes_produzione = :li_barcode);

if sqlca.sqlcode < 0 then
	g_mb.error("Errore durante il controllo del barcode nella produzione.", sqlca)
	return -1
elseif sqlca.sqlcode = 100 then
	g_mb.warning("Il barcode non è stato trovato nella produzione.")
	return -1
end if

// Carico le righe dell'ordine
ls_sql = "SELECT prog_riga_ord_ven, cod_prodotto, quan_ordine FROM det_ord_ven WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' and anno_registrazione=" + string(li_anno_registrazione) &
 		+ " and num_registrazione=" + string(ll_num_registrazione) + " and (num_riga_appartenenza is null or num_riga_appartenenza = 0)"
li_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql)

// Carico dati
for li_i = 1 to li_rows
	li_prog_riga_ord_ven = lds_store.getitemnumber(li_i, 1)
	ls_cod_prodotto = lds_store.getitemstring(li_i, 2)
	ld_quan_ordine = lds_store.getitemnumber(li_i, 3)
	
	
	select des_prodotto
	into :ls_des_prodotto
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and cod_prodotto = :ls_cod_prodotto;
	
	// Recupero codici variabili
	select cod_variabile_dim_x, cod_variabile_dim_y
	into :ls_cod_variabile_dim_x, :ls_cod_variabile_dim_y
	from tab_flags_configuratore
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_modello = :ls_cod_prodotto;
	
	// Se non esiste la configurazione allora vado avanti con la riga successiva
	if sqlca.sqlcode <> 0 then
		continue
	end if
	
	// DIM X
	select valore_numero
	into :ld_dim_x
	from det_ord_ven_conf_variabili
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 prog_riga_ord_ven = :li_prog_riga_ord_ven and
			 cod_variabile = :ls_cod_variabile_dim_x;
			 
	// DIM Y
	select valore_numero
	into :ld_dim_y
	from det_ord_ven_conf_variabili
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 prog_riga_ord_ven = :li_prog_riga_ord_ven and
			 cod_variabile = :ls_cod_variabile_dim_y;
			 
	// cod_colore_tessuto
	select valore_stringa
	into :ls_cod_colore_tessuto
	from det_ord_ven_conf_variabili
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 prog_riga_ord_ven = :li_prog_riga_ord_ven and
			 cod_variabile = 'CODCOL';
			 
	// solo se è un prodotto configurato
	dw_etichette_spedizione.reset()
	li_row = dw_etichette_spedizione.insertrow(0)
	dw_etichette_spedizione.setitem(li_row, "anno_registrazione", li_anno_registrazione)
	dw_etichette_spedizione.setitem(li_row, "num_registrazione", ll_num_registrazione)
	dw_etichette_spedizione.setitem(li_row, "cod_cliente", ls_cod_cliente)
	dw_etichette_spedizione.setitem(li_row, "des_cliente", ls_des_cliente)
	dw_etichette_spedizione.setitem(li_row, "cod_prodotto", ls_cod_prodotto)
	dw_etichette_spedizione.setitem(li_row, "des_prodotto", ls_des_prodotto)
	dw_etichette_spedizione.setitem(li_row, "dim_x", ld_dim_x)
	dw_etichette_spedizione.setitem(li_row, "dim_y", ld_dim_y)
	dw_etichette_spedizione.setitem(li_row, "cod_colore_tessuto", ls_cod_colore_tessuto)
	
	if isnull(ldt_data_ordine[2]) then
		// mostro data registrazione
		dw_etichette_spedizione.setitem(li_row, "data_ordine", ldt_data_ordine[1])
	else
		// mostro data ordine
		dw_etichette_spedizione.setitem(li_row, "data_ordine", ldt_data_ordine[2])
	end if
	
	for ll_y = 1 to int( ld_quan_ordine)
		dw_etichette_spedizione.print()
	next
	
next
/*
if dw_etichette_spedizione.rowcount() > 0 then
	dw_etichette_spedizione.print()
else
	g_mb.warning("Non sono stati trovati prodotti configurati per l'ordine " + string(li_anno_registrazione) + "/" + string(ll_num_registrazione))
end if
 */
return 1
end function

public function integer wf_avanza_produzione_barcode (string as_barcode, ref string as_message);string				ls_cgibus, ls_cod_reparto, ls_msg, ls_lettura, ls_messaggio, ls_errore, ls_tipo_doc,ls_num_matricola, ls_cod_prodotto, ls_cod_versione, &
					ls_cod_reparti[], ls_semilavorati[], ls_depositi[], ls_flag_chiudi_automaticamente, ls_barcodes[]
integer				li_anno_ord_ven, li_null, li_risposta, li_anno_registrazione, ll_ret
long					ll_num_registrazione, ll_cont, ll_prog_riga_ord_ven, ll_null, ll_i
boolean				lb_stampa, lb_chiudi_subito
uo_funzioni_1		luo_funzioni
str_rileva_produzione_multiplo lstr_barcodes


if not ib_test then
	lb_chiudi_subito = false
	// nuovo metodo per ALUSISTEMI per il caso dei TELI (Vedi Specifica 2019-Produzione Rev 1 del 29/01/2019)
	ls_lettura = as_barcode
	if f_converti_barcode(ls_lettura, ls_msg) < 0 then
		as_message = ls_msg
		return -1
	end if			

	il_barcode = long (ls_lettura)
	
	// trovo ordine e reparto coinvolto
	li_risposta = iuo_produzione.uof_barcode_anno_reg(il_barcode, li_anno_ord_ven, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_reparto, ls_errore)
	
	if li_risposta < 0 then
		as_message = ls_errore
		rollback;
		return -1
	end if
	
	select cod_prodotto,
			cod_versione
	into	:ls_cod_prodotto,
			:ls_cod_versione
	from	det_ord_ven
	where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_ord_ven and
				num_registrazione = :ll_num_registrazione and
				prog_riga_ord_ven = :ll_prog_riga_ord_ven;

	// estraggo dalla distinta tutti i reparti con i relativi semilavorati		
	luo_funzioni = CREATE uo_funzioni_1
	luo_funzioni.uof_trova_reparti_e_semilavorati( true,li_anno_ord_ven, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_prodotto, ls_cod_versione, ls_cod_reparti[], ls_semilavorati[], ls_depositi[], ls_errore)
	destroy luo_funzioni
	// verifico se sono in presenza di un reparto che richiede apertura/chiusura immediata
	for ll_i = 1 to upperbound(ls_cod_reparti[])
		if ls_cod_reparti[ll_i] = ls_cod_reparto then
			select flag_chiudi_automaticamente
			into	:ls_flag_chiudi_automaticamente
			from	tes_fasi_lavorazione
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_semilavorati[ll_i] and
					cod_versione = :ls_cod_versione and
					cod_reparto = :ls_cod_reparto ;
			
			if sqlca.sqlcode = 0 and ls_flag_chiudi_automaticamente = "S" then
				lb_chiudi_subito =true
			end if
		end if
	next
end if

if lb_chiudi_subito then
	
	select stringa
	into	:is_cod_operaio
	from	parametri_azienda_utente
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente = :s_cs_xx.cod_utente and
			cod_parametro = 'OPR';
	if sqlca.sqlcode <> 0 then
		as_message = "Manca il parametro OPR indicante il codice OPERATORE Corrente!"
		return -1
	end if
			
	// Primo giro di rilevazione produzione	
	li_risposta = iuo_produzione.uof_rileva_produzione(il_barcode,&
																		0,&
																		0,&
																		0,&
																		"",&
																		is_cod_operaio,&
																		ls_errore)
	if li_risposta < 0 then
		as_message = ls_errore
		rollback;
		return -1
	else
		commit;
	end if
	
	
	// Secondo giro di rilevazione produzione	
	li_risposta = iuo_produzione.uof_rileva_produzione(il_barcode,&
																		0,&
																		0,&
																		0,&
																		"",&
																		is_cod_operaio,&
																		ls_errore)
	if li_risposta < 0 then
		as_message = ls_errore
		return -1
	else
		commit;
	end if
	
	il_barcode = 0
	is_cod_operaio = ""
	ib_test = false	
	
else	// fai solo 1 semplice giro di avanzamento
	
	//Donato 06/10/2010 specifica carico ordini con palmare -----------------------
	//verifica i caratteri speciali delimitatori
	ls_lettura = as_barcode
	if f_converti_barcode(ls_lettura, ls_msg) < 0 then
		as_message = ls_msg
		return -1
	end if			

	il_barcode = long (ls_lettura)
	//fine modifica ------------------------------------------------------------------------------
	
	ib_test = true
	
//--- nuovo blocco inserito per ALUSISTEMI. Se un utente ha il parametro OPR allora non viene chiesto il codice operaio, ma se lo prende automaticamente.
	select stringa
	into	:is_cod_operaio
	from	parametri_azienda_utente
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente = :s_cs_xx.cod_utente and
			cod_parametro = 'OPR';
	if sqlca.sqlcode <> 0 then
		as_message = "Manca il parametro OPR indicante il codice OPERATORE Corrente!"
		return -1
	else
		ib_test = false

		li_risposta = iuo_produzione.uof_rileva_produzione(il_barcode,&
																			0,&
																			0,&
																			0,&
																			"",&
																			is_cod_operaio,&
																			ls_errore)
		if li_risposta < 0 then
			as_message = ls_errore
			return -1
		else
			commit;
		end if
		
		il_barcode = 0
		is_cod_operaio = ""
	end if		
end if	

return 0

end function

on w_rileva_produzione.create
this.dw_etichette_spedizione=create dw_etichette_spedizione
this.dw_etichetta_rdt=create dw_etichetta_rdt
this.tab_opzioni=create tab_opzioni
this.dw_reparti=create dw_reparti
this.dw_buffer=create dw_buffer
this.st_7=create st_7
this.sle_anno_registrazione=create sle_anno_registrazione
this.st_6=create st_6
this.sle_cod_operaio=create sle_cod_operaio
this.st_5=create st_5
this.cb_conferma=create cb_conferma
this.st_4=create st_4
this.st_2=create st_2
this.sle_prog_riga_ord_ven=create sle_prog_riga_ord_ven
this.sle_num_registrazione=create sle_num_registrazione
this.st_3=create st_3
this.st_messaggio=create st_messaggio
this.gb_2=create gb_2
this.gb_1=create gb_1
this.dw_richiesta_trasf_componenti=create dw_richiesta_trasf_componenti
this.Control[]={this.dw_etichette_spedizione,&
this.dw_etichetta_rdt,&
this.tab_opzioni,&
this.dw_reparti,&
this.dw_buffer,&
this.st_7,&
this.sle_anno_registrazione,&
this.st_6,&
this.sle_cod_operaio,&
this.st_5,&
this.cb_conferma,&
this.st_4,&
this.st_2,&
this.sle_prog_riga_ord_ven,&
this.sle_num_registrazione,&
this.st_3,&
this.st_messaggio,&
this.gb_2,&
this.gb_1,&
this.dw_richiesta_trasf_componenti}
end on

on w_rileva_produzione.destroy
destroy(this.dw_etichette_spedizione)
destroy(this.dw_etichetta_rdt)
destroy(this.tab_opzioni)
destroy(this.dw_reparti)
destroy(this.dw_buffer)
destroy(this.st_7)
destroy(this.sle_anno_registrazione)
destroy(this.st_6)
destroy(this.sle_cod_operaio)
destroy(this.st_5)
destroy(this.cb_conferma)
destroy(this.st_4)
destroy(this.st_2)
destroy(this.sle_prog_riga_ord_ven)
destroy(this.sle_num_registrazione)
destroy(this.st_3)
destroy(this.st_messaggio)
destroy(this.gb_2)
destroy(this.gb_1)
destroy(this.dw_richiesta_trasf_componenti)
end on

event open;string				ls_flag_supervisore, ls_filename


iuo_fido = create uo_fido_cliente

dw_buffer.insertrow(0)
iuo_produzione = create uo_produzione
postevent("ue_posiziona_focus")


dw_reparti.insertrow(0)
f_po_loaddddw_dw(dw_reparti, &
                 "cod_reparto", &
                 sqlca, &
                 "anag_reparti", &
                 "cod_reparto", &
                 "des_reparto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  

event post ue_impostazioni_mrp()

if s_cs_xx.cod_utente<>"CS_SYSTEM" then
	select flag_supervisore
	into :ls_flag_supervisore
	from utenti
	where cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente;
			
	if ls_flag_supervisore="S" then ib_supervisore = true
else
	ib_supervisore = true
end if

tab_opzioni.tp_mrp.dw_selezione.object.b_procedi.visible = ib_supervisore


//logo su etichetta richiesta trasferimento
select stringa
into   :ls_filename
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_parametro = 'LET';

ls_filename = s_cs_xx.volume + ls_filename
dw_etichetta_rdt.Object.p_logo.filename = ls_filename

guo_functions.uof_get_parametro("ADP", ib_ADP)
tab_opzioni.tp_produzione.rb_menu_allega_docs.visible = ib_ADP








end event

event clicked;dw_buffer.setfocus()
end event

event activate;dw_buffer.setfocus()
end event

event close;

destroy ids_ordini

destroy iuo_fido

destroy iuo_produzione
end event

type dw_etichette_spedizione from datawindow within w_rileva_produzione
boolean visible = false
integer x = 2871
integer y = 64
integer width = 686
integer height = 400
integer taborder = 30
string title = "none"
string dataobject = "d_etichette_spedizione"
end type

type dw_etichetta_rdt from datawindow within w_rileva_produzione
boolean visible = false
integer x = 2880
integer y = 512
integer width = 686
integer height = 400
integer taborder = 20
string title = "none"
string dataobject = "d_label_rdt"
boolean livescroll = true
end type

type tab_opzioni from tab within w_rileva_produzione
integer x = 46
integer y = 1000
integer width = 3675
integer height = 1692
integer taborder = 120
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean multiline = true
boolean focusonbuttondown = true
boolean boldselectedtext = true
alignment alignment = center!
integer selectedtab = 1
tp_produzione tp_produzione
tp_stampe tp_stampe
tp_mrp tp_mrp
tp_ologrammi tp_ologrammi
tp_altro tp_altro
end type

on tab_opzioni.create
this.tp_produzione=create tp_produzione
this.tp_stampe=create tp_stampe
this.tp_mrp=create tp_mrp
this.tp_ologrammi=create tp_ologrammi
this.tp_altro=create tp_altro
this.Control[]={this.tp_produzione,&
this.tp_stampe,&
this.tp_mrp,&
this.tp_ologrammi,&
this.tp_altro}
end on

on tab_opzioni.destroy
destroy(this.tp_produzione)
destroy(this.tp_stampe)
destroy(this.tp_mrp)
destroy(this.tp_ologrammi)
destroy(this.tp_altro)
end on

event clicked;dw_buffer.setfocus()
end event

type tp_produzione from userobject within tab_opzioni
integer x = 18
integer y = 124
integer width = 3639
integer height = 1552
long backcolor = 12632256
string text = "PRODUZIONE"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
rb_avanzamento_multiplo rb_avanzamento_multiplo
rb_menu_allega_docs rb_menu_allega_docs
rb_menu_desospendi_fase rb_menu_desospendi_fase
rb_menu_sospendi_fase rb_menu_sospendi_fase
rb_menu_reset_colli_e_stampa rb_menu_reset_colli_e_stampa
rb_menu_reset_colli rb_menu_reset_colli
rb_menu_stato rb_menu_stato
rb_menu_rileva rb_menu_rileva
end type

on tp_produzione.create
this.rb_avanzamento_multiplo=create rb_avanzamento_multiplo
this.rb_menu_allega_docs=create rb_menu_allega_docs
this.rb_menu_desospendi_fase=create rb_menu_desospendi_fase
this.rb_menu_sospendi_fase=create rb_menu_sospendi_fase
this.rb_menu_reset_colli_e_stampa=create rb_menu_reset_colli_e_stampa
this.rb_menu_reset_colli=create rb_menu_reset_colli
this.rb_menu_stato=create rb_menu_stato
this.rb_menu_rileva=create rb_menu_rileva
this.Control[]={this.rb_avanzamento_multiplo,&
this.rb_menu_allega_docs,&
this.rb_menu_desospendi_fase,&
this.rb_menu_sospendi_fase,&
this.rb_menu_reset_colli_e_stampa,&
this.rb_menu_reset_colli,&
this.rb_menu_stato,&
this.rb_menu_rileva}
end on

on tp_produzione.destroy
destroy(this.rb_avanzamento_multiplo)
destroy(this.rb_menu_allega_docs)
destroy(this.rb_menu_desospendi_fase)
destroy(this.rb_menu_sospendi_fase)
destroy(this.rb_menu_reset_colli_e_stampa)
destroy(this.rb_menu_reset_colli)
destroy(this.rb_menu_stato)
destroy(this.rb_menu_rileva)
end on

type rb_avanzamento_multiplo from radiobutton within tp_produzione
integer x = 91
integer y = 760
integer width = 882
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Avanzamento Multiplo"
end type

type rb_menu_allega_docs from radiobutton within tp_produzione
integer x = 91
integer y = 880
integer width = 1234
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Allega Documenti"
end type

event clicked;dw_buffer.setfocus()
end event

type rb_menu_desospendi_fase from radiobutton within tp_produzione
integer x = 91
integer y = 640
integer width = 1234
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "De-sospendi_fase"
end type

event clicked;
dw_buffer.setfocus()
end event

type rb_menu_sospendi_fase from radiobutton within tp_produzione
integer x = 91
integer y = 520
integer width = 1234
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Sospendi Fase"
end type

event clicked;
dw_buffer.setfocus()
end event

type rb_menu_reset_colli_e_stampa from radiobutton within tp_produzione
integer x = 91
integer y = 400
integer width = 1234
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Reset/Reimposta colli e ristampa"
end type

event clicked;//is_opzione = "K"

dw_buffer.setfocus()
end event

type rb_menu_reset_colli from radiobutton within tp_produzione
integer x = 91
integer y = 280
integer width = 1234
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Reset/Reimposta colli"
end type

event clicked;//is_opzione = "C"

dw_buffer.setfocus()
end event

type rb_menu_stato from radiobutton within tp_produzione
integer x = 91
integer y = 160
integer width = 1234
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Mostra_stato"
end type

event clicked;//is_opzione = "S"

dw_buffer.setfocus()
end event

type rb_menu_rileva from radiobutton within tp_produzione
integer x = 91
integer y = 40
integer width = 1234
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Rileva"
boolean checked = true
end type

event clicked;//is_opzione = "R"

dw_buffer.setfocus()
end event

type tp_stampe from userobject within tab_opzioni
integer x = 18
integer y = 124
integer width = 3639
integer height = 1552
long backcolor = 12632256
string text = "STAMPE"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
rb_cerificazione rb_cerificazione
rb_menu_etichette_tenda_ridotta rb_menu_etichette_tenda_ridotta
rb_menu_etichette_tenda rb_menu_etichette_tenda
rb_menu_etichette_spedizioni rb_menu_etichette_spedizioni
rb_stampa_doc rb_stampa_doc
rb_scheda_prodotto rb_scheda_prodotto
rb_menu_ristampa_etichette_apertura rb_menu_ristampa_etichette_apertura
rb_menu_packing_list rb_menu_packing_list
rb_menu_etichette_sfuso rb_menu_etichette_sfuso
rb_menu_etichette rb_menu_etichette
end type

on tp_stampe.create
this.rb_cerificazione=create rb_cerificazione
this.rb_menu_etichette_tenda_ridotta=create rb_menu_etichette_tenda_ridotta
this.rb_menu_etichette_tenda=create rb_menu_etichette_tenda
this.rb_menu_etichette_spedizioni=create rb_menu_etichette_spedizioni
this.rb_stampa_doc=create rb_stampa_doc
this.rb_scheda_prodotto=create rb_scheda_prodotto
this.rb_menu_ristampa_etichette_apertura=create rb_menu_ristampa_etichette_apertura
this.rb_menu_packing_list=create rb_menu_packing_list
this.rb_menu_etichette_sfuso=create rb_menu_etichette_sfuso
this.rb_menu_etichette=create rb_menu_etichette
this.Control[]={this.rb_cerificazione,&
this.rb_menu_etichette_tenda_ridotta,&
this.rb_menu_etichette_tenda,&
this.rb_menu_etichette_spedizioni,&
this.rb_stampa_doc,&
this.rb_scheda_prodotto,&
this.rb_menu_ristampa_etichette_apertura,&
this.rb_menu_packing_list,&
this.rb_menu_etichette_sfuso,&
this.rb_menu_etichette}
end on

on tp_stampe.destroy
destroy(this.rb_cerificazione)
destroy(this.rb_menu_etichette_tenda_ridotta)
destroy(this.rb_menu_etichette_tenda)
destroy(this.rb_menu_etichette_spedizioni)
destroy(this.rb_stampa_doc)
destroy(this.rb_scheda_prodotto)
destroy(this.rb_menu_ristampa_etichette_apertura)
destroy(this.rb_menu_packing_list)
destroy(this.rb_menu_etichette_sfuso)
destroy(this.rb_menu_etichette)
end on

type rb_cerificazione from radiobutton within tp_stampe
integer x = 91
integer y = 1120
integer width = 1531
integer height = 80
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Certificazione"
end type

event clicked;dw_buffer.setfocus()
end event

type rb_menu_etichette_tenda_ridotta from radiobutton within tp_stampe
integer x = 91
integer y = 1000
integer width = 1294
integer height = 92
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Etichetta Tenda Ridotta (53 x 17)"
end type

event clicked;dw_buffer.setfocus()
end event

type rb_menu_etichette_tenda from radiobutton within tp_stampe
integer x = 91
integer y = 880
integer width = 1234
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Etichetta Tenda"
end type

event clicked;dw_buffer.setfocus()
end event

type rb_menu_etichette_spedizioni from radiobutton within tp_stampe
integer x = 91
integer y = 760
integer width = 1234
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Etichetta Spedizione"
end type

event clicked;dw_buffer.setfocus()
end event

type rb_stampa_doc from radiobutton within tp_stampe
integer x = 91
integer y = 640
integer width = 1234
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Stampa Documenti"
end type

event clicked;//is_opzione = "STAMPADOC"

dw_buffer.setfocus()
end event

type rb_scheda_prodotto from radiobutton within tp_stampe
integer x = 91
integer y = 520
integer width = 1234
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Scheda Prodotto"
end type

event clicked;//is_opzione = "J"

dw_buffer.setfocus()
end event

type rb_menu_ristampa_etichette_apertura from radiobutton within tp_stampe
integer x = 91
integer y = 400
integer width = 1234
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Ristampa Etichette Apertura"
end type

event clicked;//is_opzione = "A"

dw_buffer.setfocus()
end event

type rb_menu_packing_list from radiobutton within tp_stampe
integer x = 91
integer y = 280
integer width = 1234
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Packing List"
end type

event clicked;//is_opzione = "P"

dw_buffer.setfocus()
end event

type rb_menu_etichette_sfuso from radiobutton within tp_stampe
integer x = 91
integer y = 160
integer width = 1234
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Etichette Sfuso"
end type

event clicked;//is_opzione = "U"

dw_buffer.setfocus()
end event

type rb_menu_etichette from radiobutton within tp_stampe
integer x = 91
integer y = 40
integer width = 1234
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Etichette"
boolean checked = true
end type

event clicked;//is_opzione = "E"

dw_buffer.setfocus()
end event

type tp_mrp from userobject within tab_opzioni
integer x = 18
integer y = 124
integer width = 3639
integer height = 1552
long backcolor = 12632256
string text = "MAGAZZINO MRP"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
st_log st_log
dw_lista dw_lista
dw_selezione dw_selezione
end type

on tp_mrp.create
this.st_log=create st_log
this.dw_lista=create dw_lista
this.dw_selezione=create dw_selezione
this.Control[]={this.st_log,&
this.dw_lista,&
this.dw_selezione}
end on

on tp_mrp.destroy
destroy(this.st_log)
destroy(this.dw_lista)
destroy(this.dw_selezione)
end on

type st_log from statictext within tp_mrp
integer x = 55
integer y = 1360
integer width = 3561
integer height = 180
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Pronto!"
boolean focusrectangle = false
end type

type dw_lista from datawindow within tp_mrp
integer x = 1614
integer y = 52
integer width = 2002
integer height = 972
integer taborder = 80
string title = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type dw_selezione from u_dw_search within tp_mrp
event ue_reset_barcode ( )
integer x = 18
integer y = 44
integer width = 1605
integer height = 1252
integer taborder = 70
string title = "none"
string dataobject = "d_rileva_produzione_mrp_sel"
boolean border = false
boolean livescroll = true
end type

event ue_reset_barcode();

setitem(getrow(), "barcode", "")

return
end event

event buttonclicked;string				ls_errore

integer				li_ret


if row>0 then
else
	return
end if


choose case dwo.name

	case "b_seleziona"
		wf_leggi_file_mrp()
		

	case "b_id_buffer"
		wf_leggi_buffer_mrp()
		

	case "b_procedi"
		li_ret = wf_procedi_mrp(ls_errore)
		
		if li_ret<>0 then
		//errore
		tab_opzioni.tp_mrp.st_log.text = "ERRORE!"
		rollback;
		g_mb.error(ls_errore)
		return
		
	elseif li_ret>0 then
		//annullato dall'operatore
		tab_opzioni.tp_mrp.st_log.text = ls_errore
		g_mb.warning(ls_errore)
		return
		
	else
		commit;
		tab_opzioni.tp_mrp.st_log.text = "Pronto!"
		tab_opzioni.tp_mrp.dw_lista.reset()
		tab_opzioni.tp_mrp.dw_selezione.setitem(1, "path", "")
		g_mb.success("Procedura terminata con successo!")
		return
	end if
	
end choose

end event

event rowfocuschanged;call super::rowfocuschanged;

if currentrow>1 then
	deleterow(currentrow)
	setrow(1)
	scrolltorow(1)
	
	return
end if
end event

event itemchanged;call super::itemchanged;long			ll_id
integer		li_ret
string		ls_errore


if row>0 then
else
	return
end if

choose case dwo.name
	case "id_buffer"
		if data <>"" then
			ll_id = getitemnumber(row, "id_buffer_fine")
			if isnull(ll_id) or ll_id<=0 then setitem(row, "id_buffer_fine", long(data))
			
		end if
	//----------------------------------------------------------------------------------------------------------------------------------
	
	case "barcode"
		//elaborazione da ddt di trasferimento per il carico nel reparto del deposito di arrivo
		if data<>"" then
			li_ret = wf_ddt_trasf_mrp(data, ls_errore)
			//tab_opzioni.tp_mrp.dw_selezione.setcolumn("barcode")
			//tab_opzioni.tp_mrp.dw_selezione.setfocus( )
			
			if li_ret<0 and ls_errore<>"" and not isnull(ls_errore) then
				tab_opzioni.tp_mrp.st_log.text = ls_errore
				//return
			end if
			
		end if
		postevent("ue_reset_barcode")
		//----------------------------------------------------------------------------------------------------------------------------------
		
end choose
end event

type tp_ologrammi from userobject within tab_opzioni
integer x = 18
integer y = 124
integer width = 3639
integer height = 1552
long backcolor = 12632256
string text = "OLOGRAMMI"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
rb_menu_ricerca_da_olo rb_menu_ricerca_da_olo
rb_menu_assegna_iniz_olo rb_menu_assegna_iniz_olo
end type

on tp_ologrammi.create
this.rb_menu_ricerca_da_olo=create rb_menu_ricerca_da_olo
this.rb_menu_assegna_iniz_olo=create rb_menu_assegna_iniz_olo
this.Control[]={this.rb_menu_ricerca_da_olo,&
this.rb_menu_assegna_iniz_olo}
end on

on tp_ologrammi.destroy
destroy(this.rb_menu_ricerca_da_olo)
destroy(this.rb_menu_assegna_iniz_olo)
end on

type rb_menu_ricerca_da_olo from radiobutton within tp_ologrammi
integer x = 91
integer y = 160
integer width = 2610
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Ricerca da N°Ologramma + Stampa SP"
end type

event clicked;//is_opzione = "N"

dw_buffer.setfocus()
end event

type rb_menu_assegna_iniz_olo from radiobutton within tp_ologrammi
integer x = 91
integer y = 40
integer width = 2610
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Assegnazione Iniziale Ologrammi"
boolean checked = true
end type

event clicked;//is_opzione = "I"

dw_buffer.setfocus()
end event

type tp_altro from userobject within tab_opzioni
integer x = 18
integer y = 124
integer width = 3639
integer height = 1552
long backcolor = 12632256
string text = "ALTRO"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
rb_etichetta_commessa rb_etichetta_commessa
rb_stampa_commessa rb_stampa_commessa
rb_avanza_commessa rb_avanza_commessa
cb_componi_rdt cb_componi_rdt
em_copie_etichetta_rdt em_copie_etichetta_rdt
rb_etichetta_rdt rb_etichetta_rdt
rb_scarica_dep_conto_forn rb_scarica_dep_conto_forn
rb_menu_richiesta_trasf_comp rb_menu_richiesta_trasf_comp
rb_menu_conferma_ddt_trasf rb_menu_conferma_ddt_trasf
end type

on tp_altro.create
this.rb_etichetta_commessa=create rb_etichetta_commessa
this.rb_stampa_commessa=create rb_stampa_commessa
this.rb_avanza_commessa=create rb_avanza_commessa
this.cb_componi_rdt=create cb_componi_rdt
this.em_copie_etichetta_rdt=create em_copie_etichetta_rdt
this.rb_etichetta_rdt=create rb_etichetta_rdt
this.rb_scarica_dep_conto_forn=create rb_scarica_dep_conto_forn
this.rb_menu_richiesta_trasf_comp=create rb_menu_richiesta_trasf_comp
this.rb_menu_conferma_ddt_trasf=create rb_menu_conferma_ddt_trasf
this.Control[]={this.rb_etichetta_commessa,&
this.rb_stampa_commessa,&
this.rb_avanza_commessa,&
this.cb_componi_rdt,&
this.em_copie_etichetta_rdt,&
this.rb_etichetta_rdt,&
this.rb_scarica_dep_conto_forn,&
this.rb_menu_richiesta_trasf_comp,&
this.rb_menu_conferma_ddt_trasf}
end on

on tp_altro.destroy
destroy(this.rb_etichetta_commessa)
destroy(this.rb_stampa_commessa)
destroy(this.rb_avanza_commessa)
destroy(this.cb_componi_rdt)
destroy(this.em_copie_etichetta_rdt)
destroy(this.rb_etichetta_rdt)
destroy(this.rb_scarica_dep_conto_forn)
destroy(this.rb_menu_richiesta_trasf_comp)
destroy(this.rb_menu_conferma_ddt_trasf)
end on

type rb_etichetta_commessa from radiobutton within tp_altro
integer x = 91
integer y = 1020
integer width = 887
integer height = 92
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Etichetta Commessa"
end type

event clicked;//is_opzione = "ET_COM"

dw_buffer.setfocus()

em_copie_etichetta_rdt.visible = false

end event

type rb_stampa_commessa from radiobutton within tp_altro
integer x = 91
integer y = 860
integer width = 1673
integer height = 92
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Stampa Commessa"
end type

event clicked;//is_opzione = "STAMPACOM"

dw_buffer.setfocus()

em_copie_etichetta_rdt.visible = false
end event

type rb_avanza_commessa from radiobutton within tp_altro
integer x = 91
integer y = 700
integer width = 1710
integer height = 92
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Avanza Commessa"
end type

event clicked;//is_opzione = "AVANZACOM"


dw_buffer.setfocus()

em_copie_etichetta_rdt.visible = false
end event

type cb_componi_rdt from commandbutton within tp_altro
integer x = 1838
integer y = 180
integer width = 544
integer height = 128
integer taborder = 30
integer textsize = -12
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Componi RDT"
end type

event clicked;s_cs_xx_parametri				lstr_parametri
string								ls_cod_operaio, ls_cod_reparto
datetime							ldt_data_fabbisogno


//chiedi il reparto che fa richiesta
openwithparm(w_sel_reparto_data, lstr_parametri)

lstr_parametri = message.powerobjectparm

if not lstr_parametri.parametro_b_1 then
	//operazione annullata dall'utente
	return
end if

ls_cod_operaio = lstr_parametri.parametro_s_1_a[1]
ls_cod_reparto = lstr_parametri.parametro_s_1_a[2]
ldt_data_fabbisogno = lstr_parametri.parametro_data_1			//questo dato non è obbligatorio


s_cs_xx.parametri.parametro_d_1_a[1] = -767				//predisponi l'apertura della finestra di creazione RDT per la composizione libera
s_cs_xx.parametri.parametro_s_1_a[1] = ls_cod_operaio
s_cs_xx.parametri.parametro_s_1_a[2] = ls_cod_reparto
s_cs_xx.parametri.parametro_data_3 = ldt_data_fabbisogno

window_open(w_report_db_varianti_componenti, -1)

end event

type em_copie_etichetta_rdt from editmask within tp_altro
boolean visible = false
integer x = 1838
integer y = 376
integer width = 361
integer height = 96
integer taborder = 60
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
string mask = "####0"
boolean spin = true
end type

type rb_etichetta_rdt from radiobutton within tp_altro
integer x = 91
integer y = 372
integer width = 1915
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Etichetta Richiesta Trasferimento Componenti"
end type

event clicked;//is_opzione = "ET_RDT"

dw_buffer.setfocus()

em_copie_etichetta_rdt.visible = true
end event

type rb_scarica_dep_conto_forn from radiobutton within tp_altro
integer x = 91
integer y = 536
integer width = 2574
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Scarica Conto Deposito Fornitore"
end type

event clicked;//is_opzione = "F"

dw_buffer.setfocus()

em_copie_etichetta_rdt.visible = false
end event

type rb_menu_richiesta_trasf_comp from radiobutton within tp_altro
integer x = 91
integer y = 196
integer width = 2574
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Richiesta Trasferimento Componenti"
end type

event clicked;//is_opzione = "T"

dw_buffer.setfocus()

em_copie_etichetta_rdt.visible = false
end event

type rb_menu_conferma_ddt_trasf from radiobutton within tp_altro
integer x = 91
integer y = 40
integer width = 2574
integer height = 96
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Conferma DDT TRASFERIMENTO"
boolean checked = true
end type

event clicked;//is_opzione = "D"

dw_buffer.setfocus()

em_copie_etichetta_rdt.visible = false
end event

type dw_reparti from u_dw_search within w_rileva_produzione
integer x = 1961
integer y = 436
integer width = 869
integer height = 100
integer taborder = 40
string dataobject = "d_reparti_avanz_manuale"
boolean border = false
end type

type dw_buffer from datawindow within w_rileva_produzione
integer x = 187
integer y = 560
integer width = 571
integer height = 100
integer taborder = 10
string title = "none"
string dataobject = "d_buffer"
boolean border = false
boolean livescroll = true
end type

event itemchanged;string				ls_cgibus, ls_cod_reparto, ls_msg, ls_lettura, ls_messaggio, ls_errore, ls_tipo_doc,ls_num_matricola[], ls_cod_prodotto, ls_cod_versione, &
					ls_cod_reparti[], ls_semilavorati[], ls_depositi[], ls_flag_chiudi_automaticamente, ls_barcodes[]
integer				li_anno_ord_ven, li_null, li_risposta, li_anno_registrazione, ll_ret
long					ll_num_registrazione, ll_cont, ll_prog_riga_ord_ven, ll_null, ll_i
boolean				lb_stampa, lb_chiudi_subito
uo_funzioni_1		luo_funzioni
str_rileva_produzione_multiplo lstr_barcodes, lstr_risultati
datastore 		lds_data
	
		
// parametro che identifica l'applicazione per centro gibus
select flag
into   :ls_cgibus
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='CGH';

if sqlca.sqlcode < 0 then
	ls_cgibus = "N"
end if

setnull(ll_null)
setnull(li_null)

//valorizza la variabile di istanza is_opzione ------------------------------
wf_get_opzione()
//-------------------------------------------------------------------------------

choose case is_opzione

	case "ALLEGA"
		ls_lettura = data
		if f_converti_barcode(ls_lettura, ls_msg) < 0 then
			g_mb.messagebox("APICE",ls_msg, stopsign!)
			goto fine_return-1
		end if			
		il_barcode = long (ls_lettura)
		
		if iuo_produzione.uof_barcode_anno_reg(il_barcode, li_anno_ord_ven, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_reparto, ls_errore) < 0 then
			rollback;
			g_mb.error("SEP", ls_errore)
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			goto fine_return-1
		
		end if
		
		if iuo_produzione.uof_allega_docs_produzione(il_barcode, li_anno_ord_ven, ll_num_registrazione, ll_prog_riga_ord_ven, ls_errore) < 0 then
			rollback;
			g_mb.error("SEP", ls_errore)
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			goto fine_return-1
			
		else
			commit;
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			goto fine_return0
		end if
		
		
		
		

	//avanzamento o stampa commessa di produzione interna o relativa etichetta 
	//(accetta un codice a barre del tipo CYYYYnnnnnn  -> YYYYnnnnn sono anno e numero commessa)
	case "AVANZACOM", "STAMPACOM", "ET_COM"
		if len(data) < 11 then
			g_mb.warning("Formato non riconosciuto. Scansionare un barcode di una stampa di Commessa!")
			goto fine_return-1
		end if
	
		ls_tipo_doc = left(data, 1)
		if ls_tipo_doc <> "C" then
			g_mb.warning("E' possibile selezionare solo documenti di tipo Commessa")
			goto fine_return-1
		end if
		
		//leggo anno e numero commessa
		if not isnumber(mid(data, 2, 4)) then
			g_mb.warning("Anno Commessa non numerico!")
			goto fine_return-1
		else
			li_anno_registrazione = long(mid(data, 2, 4))
		end if
		
		if not isnumber(mid(data, 6, 20)) then
			g_mb.warning("Numero Commessa non numerico!")
			goto fine_return-1
		else
			ll_num_registrazione = long(mid(data, 6, 20))
		end if
		
		if is_opzione = "AVANZACOM" then
			//avanzamento commessa
			ll_cont = wf_avanza_commessa(li_anno_registrazione, ll_num_registrazione, ls_errore)
			
		elseif is_opzione = "STAMPACOM" then
			//stampa commessa
			ll_cont = 0
			s_cs_xx.parametri.parametro_d_1 = li_anno_registrazione
			s_cs_xx.parametri.parametro_d_2 = ll_num_registrazione
			s_cs_xx.parametri.parametro_d_3 = 6969			//se 6969 allora stampa subito
			window_open(w_report_commessa_prod, -1)
	
		else
			//stampa etichetta commessa
			ll_cont = wf_etichetta_commessa(li_anno_registrazione, ll_num_registrazione, ls_errore)
		end if
		
		if ll_cont < 0 then
			g_mb.error(ls_errore)
			goto fine_return-1
			
		elseif ll_cont = 1 then
			//se c'è un warning, dallo ...
			if ls_errore<>"" and not isnull(ls_errore) then g_mb.warning(ls_errore)
			goto fine_return-1
			
		else
			//tutto OK
			commit;
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			goto fine_return0
		end if
		
	//-------------------------------------------------------------------------------------------------------------
	case "ET_RDT"
		if len(data) < 11 then
			g_mb.warning("Formato non riconosciuto. Scansionare un barcode di una Richiesta trasferimento Componenti!")
			goto fine_return-1
		end if
	
		ls_tipo_doc = left(data, 1)
	
		if ls_tipo_doc <> "R" then
			g_mb.warning("E' possibile selezionare solo documenti di tipo Richiesta Trasferimento Componenti")
			goto fine_return-1
		end if
		
		//leggo anno e numero documento
		if not isnumber(mid(data, 2, 4)) then
			g_mb.warning("Anno Richiesta non numerico!")
			goto fine_return-1
			
		else
			li_anno_registrazione = long(mid(data, 2, 4))
		end if
		
		if not isnumber(mid(data, 6, 20)) then
			g_mb.warning("Numero Richiesta non numerico!")
			goto fine_return-1
			
		else
			ll_num_registrazione = long(mid(data, 6, 20))
		end if
	
		ll_cont = long(tab_opzioni.tp_altro.em_copie_etichetta_rdt.text)
		
		ll_cont = wf_etichetta_rdt(li_anno_registrazione, ll_num_registrazione, ll_cont, ls_errore)
		if ll_cont < 0 then
			g_mb.error(ls_errore)
		elseif ll_cont = 1 then
			g_mb.warning(ls_errore)
		end if
		
		goto fine_return-1
	
	
	
	case "STAMPADOC"
		if len(data) < 11 then
			g_mb.warning("Formato non riconosciuto. Scansionare un barcode di un DDT o di una fattura di vendita!")
			goto fine_return-1
		end if
	
		ls_tipo_doc = left(data, 1)
	
		if ls_tipo_doc <> "B" and ls_tipo_doc <> "F" then
			g_mb.warning("E' possibile selezionare solo documenti di tipo DDT oppure FATTURA")
			goto fine_return-1
		end if
		
		//leggo anno e numero documento
		if not isnumber(mid(data, 2, 4)) then
			g_mb.warning("Anno Documento non numerico!")
			goto fine_return-1
			
		else
			li_anno_registrazione = long(mid(data, 2, 4))
		end if
		
		if not isnumber(mid(data, 6, 20)) then
			g_mb.warning("Numero Documento non numerico!")
			goto fine_return-1
			
		else
			ll_num_registrazione = long(mid(data, 6, 20))
		end if
		
		
		ll_cont = wf_stampa_documenti(ls_tipo_doc, li_anno_registrazione, ll_num_registrazione, ls_errore)
		
		if ll_cont < 0 then
			g_mb.error(ls_errore)
			
		elseif ll_cont = 1 then
			g_mb.warning(ls_errore)
			
		end if
		
		goto fine_return-1
		
			
	
	case "SOSP", "DESOSP"
		//sospensione a livello di produzione oppure DE-sospensione a livello di produzione
		//recupero info
		
		ls_lettura = data
		if f_converti_barcode(ls_lettura, ls_msg) < 0 then
			g_mb.messagebox("APICE",ls_msg, stopsign!)
			goto fine_return-1
		end if			
		il_barcode = long (ls_lettura)
		
		if iuo_produzione.uof_barcode_anno_reg(il_barcode, li_anno_ord_ven, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_reparto, ls_errore) < 0 then
			rollback;
			g_mb.error("SEP", ls_errore)
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			goto fine_return-1
		
		end if
		
		if iuo_produzione.uof_sospensione(is_opzione, il_barcode, li_anno_ord_ven, ll_num_registrazione, ll_prog_riga_ord_ven, "", ls_errore) < 0 then
			rollback;
			g_mb.error("SEP", ls_errore)
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			goto fine_return-1
			
		else
			commit;
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			goto fine_return0
		end if
		
	
	//#############################################################################################################
	case "I"
		//assegnazione Iniziale ologramma: accetta prima il barcode di produzione e successivamente il codice operaio
		
		if not ib_test then
			
			ls_lettura = data
			
			if f_converti_barcode(ls_lettura, ls_msg) < 0 then
				g_mb.messagebox("APICE",ls_msg, stopsign!)
				goto fine_return-1
			end if			

			il_barcode = long (ls_lettura)
			
			ib_test = true
			
			st_messaggio.TextColor = RGB(0,0,255)
			st_messaggio.text = "Attesa codice operaio"
			
		else
			is_cod_operaio = data
			ib_test = false
			

			li_risposta = iuo_produzione.uof_ass_iniziale_ologramma(true, il_barcode, li_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, is_cod_operaio, ls_errore)
			
			choose case li_risposta
				case is <0
					//in fs_errore il messaggio
					g_mb.error(ls_errore)
					rollback;
					st_messaggio.TextColor = RGB(0,0,0)
					st_messaggio.text = "Attesa codice a barre"
					dw_buffer.reset()
					dw_buffer.insertrow(0)
					goto fine_return0
					
				case 1
					//assegnazione non prevista OPPURE assegnazione ologrammi prevista ma è stata già effettuata la prima assegnazione
					g_mb.warning(ls_errore)
					rollback;
					st_messaggio.TextColor = RGB(0,0,0)
					st_messaggio.text = "Attesa codice a barre"
					dw_buffer.reset()
					dw_buffer.insertrow(0)
					goto fine_return0
					
					
				case 2
					//probabile annullamento da parte dell'utente
					//non dare alcun messaggio, ci ha già pensato la finestra di assegnazione/verifica ologrammi
					rollback;
					st_messaggio.TextColor = RGB(0,0,0)
					st_messaggio.text = "Attesa codice a barre"
					dw_buffer.reset()
					dw_buffer.insertrow(0)
					goto fine_return0
					
				case else
					//assegnazione iniziale effettuata con successo
					//il COMMIT alla fine confermerà tutto
					//altrimenti, non avendo fatto alcun update, ci sarà un nulla di fatto, anche con il COMMIT finale
					commit;
					st_messaggio.TextColor = RGB(0,0,0)
					st_messaggio.text = "Attesa codice a barre"
					dw_buffer.reset()
					dw_buffer.insertrow(0)
					
			end choose
		end if	
			
		
		
	//#############################################################################################################
	case "N"
		//Ricerca da N° Ologramma + Stampa Schede Prodotto
		if data<>"" then
			li_risposta = iuo_produzione.uof_stampa_scheda_prodotto(data, li_null, ll_null, ll_null, ls_msg)
			if li_risposta<0 then
				rollback;
				g_mb.error(ls_msg)
				
			elseif li_risposta=0 then
				rollback;
				g_mb.warning(ls_msg)
				
			else
				//tutto OK
				goto fine_return0
			end if
		end if
		
	//#############################################################################################################
	case "R"
		if not ib_test then
			lb_chiudi_subito = false
			// nuovo metodo per ALUSISTEMI per il caso dei TELI (Vedi Specifica 2019-Produzione Rev 1 del 29/01/2019)
			ls_lettura = data
			if f_converti_barcode(ls_lettura, ls_msg) < 0 then
				g_mb.messagebox("APICE",ls_msg, stopsign!)
				goto fine_return-1
			end if			
	
			il_barcode = long (ls_lettura)
			
			// trovo ordine e reparto coinvolto
			li_risposta = iuo_produzione.uof_barcode_anno_reg(il_barcode, li_anno_ord_ven, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_reparto, ls_errore)
			
			if li_risposta < 0 then
				g_mb.messagebox("SEP",ls_errore,stopsign!)
				rollback;
				st_messaggio.TextColor = RGB(0,0,0)
				st_messaggio.text = "Attesa codice a barre"
				dw_buffer.reset()
				dw_buffer.insertrow(0)
				goto fine_return0
			end if
			
			select cod_prodotto,
					cod_versione
			into	:ls_cod_prodotto,
					:ls_cod_versione
			from	det_ord_ven
			where	cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :li_anno_ord_ven and
						num_registrazione = :ll_num_registrazione and
						prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
			// estraggo dalla distinta tutti i reparti con i relativi semilavorati		
			luo_funzioni = CREATE uo_funzioni_1
			luo_funzioni.uof_trova_reparti_e_semilavorati( true,li_anno_ord_ven, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_prodotto, ls_cod_versione, ls_cod_reparti[], ls_semilavorati[], ls_depositi[], ls_errore)
			destroy luo_funzioni
			// verifico se sono in presenza di un reparto che richiede apertura/chiusura immediata
			for ll_i = 1 to upperbound(ls_cod_reparti[])
				if ls_cod_reparti[ll_i] = ls_cod_reparto then
					select flag_chiudi_automaticamente
					into	:ls_flag_chiudi_automaticamente
					from	tes_fasi_lavorazione
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_prodotto = :ls_semilavorati[ll_i] and
							cod_versione = :ls_cod_versione and
							cod_reparto = :ls_cod_reparto ;
					
					if sqlca.sqlcode = 0 and ls_flag_chiudi_automaticamente = "S" then
						lb_chiudi_subito =true
					end if
				end if
			next
		end if
		
		if lb_chiudi_subito then
			
			select stringa
			into	:is_cod_operaio
			from	parametri_azienda_utente
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_utente = :s_cs_xx.cod_utente and
					cod_parametro = 'OPR';
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("SEP","Manca il parametro OPR indicante il codice OPERATORE Corrente!",stopsign!)
				rollback;
				st_messaggio.TextColor = RGB(0,0,0)
				st_messaggio.text = "Attesa codice a barre"
				dw_buffer.reset()
				dw_buffer.insertrow(0)
				goto fine_return0
			end if
					
			// Primo giro di rilevazione produzione	
			li_risposta = iuo_produzione.uof_rileva_produzione(il_barcode,&
																				0,&
																				0,&
																				0,&
																				"",&
																				is_cod_operaio,&
																				ls_errore)
			if li_risposta < 0 then
				g_mb.messagebox("SEP",ls_errore,stopsign!)
				rollback;
				st_messaggio.TextColor = RGB(0,0,0)
				st_messaggio.text = "Attesa codice a barre"
				dw_buffer.reset()
				dw_buffer.insertrow(0)
				goto fine_return0
			else
				commit;
				st_messaggio.TextColor = RGB(0,0,0)
				st_messaggio.text = "Attesa codice a barre"
				dw_buffer.reset()
				dw_buffer.insertrow(0)
			end if
			
			
			// Secondo giro di rilevazione produzione	
			li_risposta = iuo_produzione.uof_rileva_produzione(il_barcode,&
																				0,&
																				0,&
																				0,&
																				"",&
																				is_cod_operaio,&
																				ls_errore)
			if li_risposta < 0 then
				g_mb.messagebox("SEP",ls_errore,stopsign!)
				rollback;
				st_messaggio.TextColor = RGB(0,0,0)
				st_messaggio.text = "Attesa codice a barre"
				dw_buffer.reset()
				dw_buffer.insertrow(0)
				goto fine_return0
			else
				commit;
				st_messaggio.TextColor = RGB(0,0,0)
				st_messaggio.text = "Attesa codice a barre"
				dw_buffer.reset()
				dw_buffer.insertrow(0)
			end if
			
			il_barcode = 0
			is_cod_operaio = ""
			ib_test = false	
			
		else
			if  ib_test = false then
				
				//Donato 06/10/2010 specifica carico ordini con palmare -----------------------
				//verifica i caratteri speciali delimitatori
				ls_lettura = data
				if f_converti_barcode(ls_lettura, ls_msg) < 0 then
					g_mb.messagebox("APICE",ls_msg, stopsign!)
					goto fine_return-1
				end if			
		
				il_barcode = long (ls_lettura)
				//fine modifica ------------------------------------------------------------------------------
				
				//##########################################################################
				li_risposta = wf_controllo_blocco_cliente(il_barcode, 0, 0, ls_errore)
				
				if li_risposta<>0 then
					choose case li_risposta
						case 1
							//dare warning ma farlo andara avanti
							g_mb.warning(ls_errore)
							
						case 2
							//messaggio di blocco e fermare tutto
							g_mb.error(ls_errore)
							rollback;
							st_messaggio.TextColor = RGB(0,0,0)
							st_messaggio.text = "Attesa codice a barre"
							dw_buffer.reset()
							dw_buffer.insertrow(0)
							goto fine_return0
							
						case -1
							//errore critico!
							g_mb.error(ls_errore)
							rollback;
							st_messaggio.TextColor = RGB(0,0,0)
							st_messaggio.text = "Attesa codice a barre"
							dw_buffer.reset()
							dw_buffer.insertrow(0)
							goto fine_return0
							
					end choose
				end if
				//##########################################################################
				
				//se superi la clausola IF precedente vai avanti ...
				
				ib_test = true
				
				// aggiunto 10/2/2006 da Enrico per Beatrice.
				select count(*)
				into   :ll_cont
				from   sessioni_lavoro
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 progr_det_produzione = :il_barcode and
						 inizio_sessione is not null and
						 fine_sessione is null;
						 
				if ll_cont > 0 then
					st_messaggio.TextColor = RGB(0,255,0)
				else
					st_messaggio.TextColor = RGB(255,0,0)
				end if

//--- nuovo blocco inserito per ALUSISTEMI. Se un utente ha il parametro OPR allora non viene chiesto il codice operaio, ma se lo prende automaticamente.
				select stringa
				into	:is_cod_operaio
				from	parametri_azienda_utente
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_utente = :s_cs_xx.cod_utente and
						cod_parametro = 'OPR';
				if sqlca.sqlcode <> 0 then
					st_messaggio.text = "Attesa codice operaio"
				else
					ib_test = false

					li_risposta = iuo_produzione.uof_rileva_produzione(il_barcode,&
																						0,&
																						0,&
																						0,&
																						"",&
																						is_cod_operaio,&
																						ls_errore)
					if li_risposta < 0 then
						g_mb.messagebox("SEP",ls_errore,stopsign!)
						rollback;
						st_messaggio.TextColor = RGB(0,0,0)
						st_messaggio.text = "Attesa codice a barre"
						dw_buffer.reset()
						dw_buffer.insertrow(0)
						goto fine_return0
					else
						commit;
						st_messaggio.TextColor = RGB(0,0,0)
						st_messaggio.text = "Attesa codice a barre"
						dw_buffer.reset()
						dw_buffer.insertrow(0)
					end if
					
					il_barcode = 0
					is_cod_operaio = ""
//---
				end if				
			else
				is_cod_operaio = data
				ib_test = false
				
				
				li_risposta = iuo_produzione.uof_rileva_produzione(il_barcode,&
																					0,&
																					0,&
																					0,&
																					"",&
																					is_cod_operaio,&
																					ls_errore)
				if li_risposta < 0 then
					g_mb.messagebox("SEP",ls_errore,stopsign!)
					rollback;
					st_messaggio.TextColor = RGB(0,0,0)
					st_messaggio.text = "Attesa codice a barre"
					dw_buffer.reset()
					dw_buffer.insertrow(0)
					goto fine_return0
				else
					commit;
					st_messaggio.TextColor = RGB(0,0,0)
					st_messaggio.text = "Attesa codice a barre"
					dw_buffer.reset()
					dw_buffer.insertrow(0)
				end if
				
				il_barcode = 0
				is_cod_operaio = ""
				
			end if
		end if	
	//#############################################################################################################
	
	case "MULTIAV"
		ls_lettura = UPPER(data)
		if ls_lettura = "START" then
			ll_ret = Openwithparm (w_rileva_produzione_lettura_multipla,  lstr_barcodes, parent)
			
			lstr_barcodes = message.powerobjectparm
			
			if upperbound(lstr_barcodes.barcode) > 0 then
				for ll_i = 1 to upperbound(lstr_barcodes.barcode)
					wf_avanza_produzione_barcode( lstr_barcodes.barcode[ll_i], lstr_barcodes.messaggi[ll_i] )
				next
			end if
		end if
	
	case "S"
		
		//Donato 06/10/2010 specifica carico ordini con palmare -----------------------
		//verifica i caratteri speciali delimitatori
		ls_lettura = data
		if f_converti_barcode(ls_lettura, ls_msg) < 0 then
			g_mb.messagebox("APICE",ls_msg, stopsign!)
			goto fine_return-1
		end if			

		il_barcode = long (ls_lettura)
		//fine modifica ------------------------------------------------------------------------------
		
		select
			anno_registrazione,
			num_registrazione
		into
			:li_anno_registrazione,
			:ll_num_registrazione
		from
			det_ordini_produzione
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			progr_det_produzione = :il_barcode;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore in lettura ordine collegato: " + sqlca.sqlerrtext,stopsign!)
			goto fine_return-1
		elseif sqlca.sqlcode = 100 then
			
			select
				anno_registrazione,
				num_registrazione
			into
				:li_anno_registrazione,
				:ll_num_registrazione
			from
				tes_ordini_produzione
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				progr_tes_produzione = :il_barcode;
				
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore in lettura ordine collegato: " + sqlca.sqlerrtext,stopsign!)
				goto fine_return-1
			elseif sqlca.sqlcode = 100 then
				g_mb.messagebox("APICE","Errore in lettura ordine collegato: progressivo indicato non trovato in tabelle produzione",stopsign!)
				goto fine_return-1
			end if
			
		end if
		
		s_cs_xx.parametri.parametro_d_1 = li_anno_registrazione
		
		s_cs_xx.parametri.parametro_d_2 = ll_num_registrazione
		
		window_open(w_det_ord_ven_stato,-1)
	
	//#############################################################################################################
	case "E"
		//Donato 06/10/2010 specifica carico ordini con palmare -----------------------
		//verifica i caratteri speciali delimitatori
		ls_lettura = data
		if f_converti_barcode(ls_lettura, ls_msg) < 0 then
			g_mb.messagebox("APICE",ls_msg, stopsign!)
			goto fine_return-1
		end if			

		il_barcode = long (ls_lettura)
		//fine modifica ------------------------------------------------------------------------------		
		
		// se applicazione specifica per centro gibus
		if ls_cgibus = "S" then
			li_risposta = iuo_produzione.uof_etichette_cg(il_barcode,&
																					0,&
																					0,&
																					0,&
																					ls_errore)
				if li_risposta < 0 then
					g_mb.messagebox("SEP",ls_errore,stopsign!)
					rollback;
					st_messaggio.TextColor = RGB(0,0,0)
					st_messaggio.text = "Attesa codice a barre"
					dw_buffer.reset()
					dw_buffer.insertrow(0)
					goto fine_return0
				else
					commit;
					st_messaggio.TextColor = RGB(0,0,0)
					st_messaggio.text = "Attesa codice a barre"
					dw_buffer.reset()
					dw_buffer.insertrow(0)
				end if
				
				il_barcode = 0
				
		else
			
			//----------------------------------------------------------------------------------------------------------------------------------------
			//Donato 14/03/2011 la funzione chiamata per la ristampa è la stessa che viene usata dopo la chiusura della produzione
			//quindi occorre distinguere, perchè nel casi di ristampa e in presenza di + colli, l'operatore deve poter scegliere quali etichette
			//(cioè di quali colli) ristampare
			s_cs_xx.parametri.parametro_s_11 = "RISTAMPA"
			//----------------------------------------------------------------------------------------------------------------------------------------
			
			li_risposta = iuo_produzione.uof_etichette(il_barcode,&
																					0,&
																					0,&
																					0,&
																					ls_errore)
			//----------------------------------------------------------------------------------------------------------------------------------------
			//Donato 14/03/2011 resetto il semaforo							
			s_cs_xx.parametri.parametro_s_11 = ""
			//----------------------------------------------------------------------------------------------------------------------------------------
			
				if li_risposta < 0 then
					g_mb.messagebox("SEP",ls_errore,stopsign!)
					rollback;
					st_messaggio.TextColor = RGB(0,0,0)
					st_messaggio.text = "Attesa codice a barre"
					dw_buffer.reset()
					dw_buffer.insertrow(0)
					goto fine_return0
				else
					commit;
					st_messaggio.TextColor = RGB(0,0,0)
					st_messaggio.text = "Attesa codice a barre"
					dw_buffer.reset()
					dw_buffer.insertrow(0)
				end if
				
				il_barcode = 0
				
			end if
	
	case "ETICHETTE_SPEDIZIONE"
		if wf_stampa_etichette_spedizione(data) = 0 then
			commit;
			dw_buffer.reset()
			dw_buffer.insertrow(0)
			return 0
		else
			rollback;
			dw_buffer.reset()
			dw_buffer.insertrow(0)
			return -1
		end if

	case "ETICHETTE_TENDA"
		if wf_stampa_etichette_tenda(data,"N") = 0 then
			commit;
			dw_buffer.reset()
			dw_buffer.insertrow(0)
			return 0
		else
			rollback;
			dw_buffer.reset()
			dw_buffer.insertrow(0)
			return -1
		end if
		
	case "ETICHETTE_TENDA_RIDOTTA"
		if wf_stampa_etichette_tenda(data,"R") = 0 then
			commit;
			dw_buffer.reset()
			dw_buffer.insertrow(0)
			return 0
		else
			rollback;
			dw_buffer.reset()
			dw_buffer.insertrow(0)
			return -1
		end if
		
	case "STAMPA_CERTIFICAZIONE"
		/* Stampa delle certificazioni del prodotto
		
		1) leggo barcode
		2) recupero anno/numero/riga ordine
		3) recupero num matricola produzione
		4) procedo con il report
		*/	
		s_stampa_certificazione lstr_stampa_certificazione
		
		il_barcode = long(data)
		
		if iuo_produzione.uof_barcode_anno_reg(il_barcode, li_anno_ord_ven, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_reparto, ls_errore) < 0 then
			rollback;
			g_mb.error("SEP", ls_errore)
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			goto fine_return-1
		end if
		
		ll_ret = guo_functions.uof_crea_datastore( lds_data, g_str.format("select cod_ologramma from	det_ord_ven_ologramma where cod_azienda = '$1' and anno_registrazione = $2 and num_registrazione = $3 and prog_riga_ord_ven = $4 ",s_cs_xx.cod_azienda, li_anno_ord_ven, ll_num_registrazione, ll_prog_riga_ord_ven), ls_errore)
		if ll_ret = 0 then
			g_mb.warning(g_str.format("Matricola non trovata per l'ordine $1-$2-$3", li_anno_ord_ven, ll_num_registrazione,ll_prog_riga_ord_ven))
			goto fine_return-1
		end if
		if ll_ret < 0 then
			g_mb.warning(g_str.format("Errore in ricerca matricole per l'ordine $1-$2-$3", li_anno_ord_ven, ll_num_registrazione,ll_prog_riga_ord_ven))
			goto fine_return-1
		end if
		
		for ll_i = 1 to ll_ret
			 ls_num_matricola[ll_i] = lds_data.getitemstring(ll_i,1)
		next
		
		destroy 	lds_data	
		
		lstr_stampa_certificazione.anno_reg_ord_ven = li_anno_ord_ven
		lstr_stampa_certificazione.num_reg_ord_ven = ll_num_registrazione
		lstr_stampa_certificazione.prog_riga_ord_ven = ll_prog_riga_ord_ven
		lstr_stampa_certificazione.cod_matricola = ls_num_matricola[]
		
		window_open_parm(w_stampa_certificazione, -1, lstr_stampa_certificazione)
		
		
		
	//#############################################################################################################
	case "P"
		
		//Donato 06/10/2010 specifica carico ordini con palmare -----------------------
		//verifica i caratteri speciali delimitatori
		ls_lettura = data
		if f_converti_barcode(ls_lettura, ls_msg) < 0 then
			g_mb.messagebox("APICE",ls_msg, stopsign!)
			goto fine_return-1
		end if			

		il_barcode = long (ls_lettura)
		//fine modifica ------------------------------------------------------------------------------		
		
		select
			anno_registrazione,
			num_registrazione
		into
			:li_anno_registrazione,
			:ll_num_registrazione
		from
			det_ordini_produzione
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			progr_det_produzione = :il_barcode;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore in lettura ordine collegato: " + sqlca.sqlerrtext,stopsign!)
			goto fine_return-1
		elseif sqlca.sqlcode = 100 then
			
			select
				anno_registrazione,
				num_registrazione
			into
				:li_anno_registrazione,
				:ll_num_registrazione
			from
				tes_ordini_produzione
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				progr_tes_produzione = :il_barcode;
				
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore in lettura ordine collegato: " + sqlca.sqlerrtext,stopsign!)
				goto fine_return-1
			elseif sqlca.sqlcode = 100 then
				g_mb.messagebox("APICE","Errore in lettura ordine collegato: progressivo indicato non trovato in tabelle produzione",stopsign!)
				goto fine_return-1
			end if
			
		end if
		
		s_cs_xx.parametri.parametro_b_1 = true
		
		s_cs_xx.parametri.parametro_d_1 = li_anno_registrazione
		
		s_cs_xx.parametri.parametro_d_2 = ll_num_registrazione
		
		window_open(w_packing_list_gibus,-1)
	
	//#############################################################################################################
	case "A"
		
		//Donato 06/10/2010 specifica carico ordini con palmare -----------------------
		//verifica i caratteri speciali delimitatori
		ls_lettura = data
		if f_converti_barcode(ls_lettura, ls_msg) < 0 then
			g_mb.messagebox("APICE",ls_msg, stopsign!)
			goto fine_return-1
		end if			
	
		il_barcode = long (ls_lettura)
		//fine modifica ------------------------------------------------------------------------------
		
		
		li_risposta = iuo_produzione.uof_etichette_apertura(il_barcode,&
																			 0,&
																			 0,&
																			 0,&
																			 ref ls_errore)
		
		if li_risposta < 0 then
			g_mb.messagebox("SEP",ls_errore,stopsign!)
			rollback;
			goto fine_return0
		else
			commit;
		end if
	
	//#############################################################################################################
	// stefano 13/05/2010: specifica etichette sfuso
	case "U"
		
		//Donato 06/10/2010 specifica carico ordini con palmare -----------------------
		//verifica i caratteri speciali delimitatori
		ls_lettura = data
		if f_converti_barcode(ls_lettura, ls_msg) < 0 then
			g_mb.messagebox("APICE",ls_msg, stopsign!)
			goto fine_return-1
		end if			
	
		il_barcode = long (ls_lettura)
		//fine modifica ------------------------------------------------------------------------------
		
		li_risposta = iuo_produzione.uof_barcode_anno_reg_sfuso(il_barcode, &
																			li_anno_ord_ven, &
																			ll_num_registrazione, &
																			ls_errore)
																			
		if li_risposta < 0 then
			g_mb.error("SEP", ls_errore)
			rollback;
			goto fine_return0
		else
			s_cs_xx.parametri.parametro_d_1 = long(li_anno_ord_ven)
			s_cs_xx.parametri.parametro_d_2 = long(ll_num_registrazione)
			window_open(w_stampa_etichette_sfuso, 0)
		end if
	// ----
	
	//#############################################################################################################
	//16/03/2011 Donato Reset/reimposta colli (C)   -    Reset/reimposta colli e ristampa
	case "C", "K"
		//verifica i caratteri speciali delimitatori
		ls_lettura = data
		if f_converti_barcode(ls_lettura, ls_msg) < 0 then
			g_mb.messagebox("APICE",ls_msg, stopsign!)
			
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			dw_buffer.reset()
			dw_buffer.insertrow(0)
			
			goto fine_return0
		end if			
		
		il_barcode = long (ls_lettura)
		//fine modifica ------------------------------------------------------------------------------		
		
		li_risposta = iuo_produzione.uof_reimposta_colli(il_barcode, ls_errore)

		if li_risposta < 0 then
			g_mb.error("SEP", ls_errore)
			
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			dw_buffer.reset()
			dw_buffer.insertrow(0)
			
			rollback;
			goto fine_return0
		else
			//tutto a è posto
			commit;
		end if
		
		if is_opzione = "K" then //procedi pure alla ristampa
			
			// se applicazione specifica per centro gibus
			if ls_cgibus = "S" then
				li_risposta = iuo_produzione.uof_etichette_cg(il_barcode,&
																						0,&
																						0,&
																						0,&
																						ls_errore)
					if li_risposta < 0 then
						g_mb.messagebox("SEP",ls_errore,stopsign!)
						rollback;
						st_messaggio.TextColor = RGB(0,0,0)
						st_messaggio.text = "Attesa codice a barre"
						dw_buffer.reset()
						dw_buffer.insertrow(0)
						goto fine_return0
					else
						commit;
						st_messaggio.TextColor = RGB(0,0,0)
						st_messaggio.text = "Attesa codice a barre"
						dw_buffer.reset()
						dw_buffer.insertrow(0)
					end if
					
					il_barcode = 0
					
			else
				//NO CGIBUS
				//----------------------------------------------------------------------------------------------------------------------------------------
				//Donato 14/03/2011 la funzione chiamata per la ristampa è la stessa che viene usata dopo la chiusura della produzione
				//quindi occorre distinguere, perchè nel casi di ristampa e in presenza di + colli, l'operatore deve poter scegliere quali etichette
				//(cioè di quali colli) ristampare
				s_cs_xx.parametri.parametro_s_11 = "RISTAMPA"
				//----------------------------------------------------------------------------------------------------------------------------------------
				
				li_risposta = iuo_produzione.uof_etichette(il_barcode,&
																						0,&
																						0,&
																						0,&
																						ls_errore)
				//----------------------------------------------------------------------------------------------------------------------------------------
				//Donato 14/03/2011 resetto il semaforo							
				s_cs_xx.parametri.parametro_s_11 = ""
				//----------------------------------------------------------------------------------------------------------------------------------------
				
					if li_risposta < 0 then
						g_mb.messagebox("SEP",ls_errore,stopsign!)
						rollback;
						st_messaggio.TextColor = RGB(0,0,0)
						st_messaggio.text = "Attesa codice a barre"
						dw_buffer.reset()
						dw_buffer.insertrow(0)
						goto fine_return0
					else
						commit;
						st_messaggio.TextColor = RGB(0,0,0)
						st_messaggio.text = "Attesa codice a barre"
						dw_buffer.reset()
						dw_buffer.insertrow(0)
					end if
					
					il_barcode = 0
					
				end if
			
		end if
	
	//#############################################################################################################
	case "J"
		//ristampa etichetta scheda prodotto, se previsto
		
		//Donato 06/10/2010 specifica carico ordini con palmare -----------------------
		//verifica i caratteri speciali delimitatori
		ls_lettura = data
		if f_converti_barcode(ls_lettura, ls_msg) < 0 then
			g_mb.messagebox("APICE",ls_msg, stopsign!)
			goto fine_return-1
		end if			
		
		il_barcode = long (ls_lettura)
		//fine modifica ------------------------------------------------------------------------------
		
		li_risposta = iuo_produzione.uof_etichette_scheda_prodotto(il_barcode, 0, 0, 0, "", ls_errore)
			
		if li_risposta < 0 then
			g_mb.messagebox("SEP",ls_errore,stopsign!)
			rollback;
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			dw_buffer.reset()
			dw_buffer.insertrow(0)
			goto fine_return0
		else
			commit;
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			dw_buffer.reset()
			dw_buffer.insertrow(0)
		end if
		
		il_barcode = 0
	
	//#############################################################################################################
	case "D"
		// stefanop: 03/02/2012: conferma bolla di trasferimento.
		if len(data) < 11 then return
	
		if left(data, 1) <> "B" then
			g_mb.warning("E' possibile inserire solo documenti di tipo BOLLA")
			return
		end if
	
		li_anno_registrazione = long(mid(data, 2, 4))
		ll_num_registrazione = long(mid(data, 6, 6))
		
		if f_conferma_mov_mag("tes_bol_ven", &
									 "det_bol_ven", &
									 "prog_riga_bol_ven", &
									  "quan_consegnata", &
									 li_anno_registrazione, &
									 ll_num_registrazione, &
									 ls_messaggio) = -1 then
									 
			g_mb.messagebox("Conferma Bolla", "Errore durante la conferma della bolla~r~nDettaglio errore:" + ls_messaggio + "~r~n", Information!)
			rollback;
		else
			commit;
			g_mb.messagebox("Conferma Bolla","Bolla confermata con successo")
		end if
		
	//#############################################################################################################
	case "T"
		//richiesta trasferimento componenti
		
		//Donato 06/10/2010 specifica carico ordini con palmare -----------------------
		//verifica i caratteri speciali delimitatori
		ls_lettura = data
		if f_converti_barcode(ls_lettura, ls_msg) < 0 then
			g_mb.messagebox("APICE",ls_msg, stopsign!)
			goto fine_return-1
		end if			
		
		il_barcode = long (ls_lettura)
		//fine modifica ------------------------------------------------------------------------------
		
		lb_stampa = false
		li_risposta = iuo_produzione.uof_richiesta_trasf_componenti(li_null, ll_null, ll_null ,il_barcode, lb_stampa, dw_richiesta_trasf_componenti, ls_errore)
		if li_risposta < 0 then
			g_mb.error("SEP",ls_errore)
			rollback;
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			dw_buffer.reset()
			dw_buffer.insertrow(0)
			goto fine_return0
		else
			commit;
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			dw_buffer.reset()
			dw_buffer.insertrow(0)
		end if
		
		il_barcode = 0
	
	//#############################################################################################################
	// stefanop: 28/03/2012: scarico conto deposito fornitori
	case "F"
		if not isnull(data) and data <> "" then
			if wf_scarica_conto_dep_for(data, ls_errore) then
				commit;
				st_messaggio.TextColor = RGB(0,0,0)
				st_messaggio.text = "Attesa codice a barre"
				dw_buffer.reset()
				dw_buffer.insertrow(0)
			else
				g_mb.error("SEP",ls_errore)
				rollback;
				st_messaggio.TextColor = RGB(0,0,0)
				st_messaggio.text = "Attesa codice a barre"
				dw_buffer.reset()
				dw_buffer.insertrow(0)
				goto fine_return0
			end if
		end if
	
	case else
		
		g_mb.messagebox("APICE","Selezionare l'opzione desiderata",exclamation!)
		
		st_messaggio.TextColor = RGB(0,0,0)
		st_messaggio.text = "Attesa codice a barre"
		dw_buffer.reset()
		dw_buffer.insertrow(0)
		
		goto fine_return0

end choose
	

fine_return0:
commit;
dw_buffer.reset()
dw_buffer.insertrow(0)
return

fine_return-1:
rollback;
dw_buffer.reset()
dw_buffer.insertrow(0)
return -1



end event

event getfocus;st_messaggio.TextColor = RGB(0,0,0)
st_messaggio.text = "Attesa codice a barre"
end event

type st_7 from statictext within w_rileva_produzione
integer x = 1097
integer y = 436
integer width = 448
integer height = 80
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Anno ordine"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_anno_registrazione from singlelineedit within w_rileva_produzione
integer x = 1577
integer y = 436
integer width = 343
integer height = 100
integer taborder = 70
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
end type

event getfocus;st_messaggio.text = "Inserire valori manualmente"
end event

type st_6 from statictext within w_rileva_produzione
integer x = 1984
integer y = 556
integer width = 448
integer height = 80
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Cod.operaio"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_cod_operaio from singlelineedit within w_rileva_produzione
integer x = 2464
integer y = 556
integer width = 343
integer height = 100
integer taborder = 100
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
end type

event getfocus;st_messaggio.text = "Inserire valori manualmente"
end event

type st_5 from statictext within w_rileva_produzione
boolean visible = false
integer x = 2002
integer y = 1212
integer width = 274
integer height = 80
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 255
string text = "Reparto"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_conferma from commandbutton within w_rileva_produzione
integer x = 1760
integer y = 820
integer width = 411
integer height = 100
integer taborder = 110
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
end type

event clicked;integer				li_risposta,li_anno_registrazione
string					ls_errore,ls_cod_reparto,ls_cod_operaio
long					ll_num_registrazione,ll_prog_riga_ord_ven, ll_barcode
boolean				lb_stampa

dw_reparti.accepttext()

//valorizza la variabile di istanza is_opzione ------------------------------
wf_get_opzione()
//-------------------------------------------------------------------------------


choose case is_opzione
	
	//-------------------------------------------------------------------------------------------------------
	case "SOSP", "DESOSP"
		setnull(ll_barcode)
	
		li_anno_registrazione = integer(sle_anno_registrazione.text)
		ll_num_registrazione = long(sle_num_registrazione.text)
		ll_prog_riga_ord_ven = long(sle_prog_riga_ord_ven.text)
		
		ls_cod_reparto = dw_reparti.getitemstring(1, "cod_reparto")
		ls_cod_operaio = sle_cod_operaio.text
		
		if li_anno_registrazione = 0 or isnull(li_anno_registrazione) then
			g_mb.messagebox("SEP","Attenzione manca l'anno registrazione dell'ordine.",stopsign!)
			return
		end if
		
		if ll_num_registrazione = 0 or isnull(ll_num_registrazione) then
			g_mb.messagebox("SEP","Attenzione manca il numero registrazione dell'ordine.",stopsign!)
			return
		end if
		
		if ll_prog_riga_ord_ven = 0 or isnull(ll_prog_riga_ord_ven) then
			g_mb.messagebox("SEP","Attenzione manca il numero riga dell'ordine.",stopsign!)
			return
		end if
		
		if ls_cod_reparto="" or isnull(ls_cod_reparto) then
			g_mb.messagebox("SEP","Attenzione manca il reparto.",stopsign!)
			return
		end if
		
		
		//recupero info
		if iuo_produzione.uof_barcode_anno_reg(ll_barcode, li_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_reparto, ls_errore) < 0 then
			g_mb.error("SEP", ls_errore)
			rollback;
			return
		end if
		
		if iuo_produzione.uof_sospensione(is_opzione, ll_barcode, li_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_operaio, ls_errore) < 0 then
			g_mb.error("SEP", ls_errore)
			rollback;
			return
		else
			commit;
		end if
	//-------------------------------------------------------------------------------------------------------
		
	
	case "R"
		
		setnull(ll_barcode)
	
		li_anno_registrazione = integer(sle_anno_registrazione.text)
		ll_num_registrazione = long(sle_num_registrazione.text)
		ll_prog_riga_ord_ven = long(sle_prog_riga_ord_ven.text)
		
		//ls_cod_reparto = f_po_selectddlb(ddlb_reparto)
		ls_cod_reparto = dw_reparti.getitemstring(1, "cod_reparto")
		
		ls_cod_operaio = sle_cod_operaio.text
		
		
		if li_anno_registrazione = 0 or isnull(li_anno_registrazione) then
			g_mb.messagebox("SEP","Attenzione manca l'anno registrazione dell'ordine.",stopsign!)
			return
		end if
		
		if ll_num_registrazione = 0 or isnull(ll_num_registrazione) then
			g_mb.messagebox("SEP","Attenzione manca il numero registrazione dell'ordine.",stopsign!)
			return
		end if
				
		if ls_cod_reparto="" or isnull(ls_cod_reparto) then
			g_mb.messagebox("SEP","Attenzione manca il reparto.",stopsign!)
			return
		end if
		
		if ls_cod_operaio="" or isnull(ls_cod_operaio) then
			g_mb.messagebox("SEP","Attenzione manca il codice operaio.",stopsign!)
			return
		end if
		
		
		//##########################################################################
		li_risposta = wf_controllo_blocco_cliente(0, li_anno_registrazione, ll_num_registrazione, ls_errore)
		
		if li_risposta<>0 then
			choose case li_risposta
				case 1
					//dare warning ma farlo andara avanti
					g_mb.warning(ls_errore)
					
				case 2
					//messaggio di blocco e fermare tutto
					g_mb.error(ls_errore)
					rollback;
					return
					
				case -1
					//errore critico!
					g_mb.error(ls_errore)
					rollback;
					return
					
			end choose
		end if
		//##########################################################################
		
		//se superi la clausola IF precedente vai avanti ...
		
		
		
		li_risposta = iuo_produzione.uof_rileva_produzione(ll_barcode,&
																			li_anno_registrazione,&
																			ll_num_registrazione,&
																			ll_prog_riga_ord_ven,&
																			ls_cod_reparto,&
																			ls_cod_operaio,&
																			ls_errore)
		
		if li_risposta < 0 then
			g_mb.messagebox("SEP",ls_errore,stopsign!)
			rollback;
			return
		else
			commit;
		end if
						
	case "S"
		s_cs_xx.parametri.parametro_d_1 = integer(sle_anno_registrazione.text)
		s_cs_xx.parametri.parametro_d_2 = long(sle_num_registrazione.text)
		window_open(w_det_ord_ven_stato,-1)
		
	case "E"
		
		
	case "P"
		s_cs_xx.parametri.parametro_b_1 = true
		s_cs_xx.parametri.parametro_d_1 = long(sle_anno_registrazione.text)
		s_cs_xx.parametri.parametro_d_2 = long(sle_num_registrazione.text)
		window_open(w_packing_list_gibus,-1)
		
	case "A"
		setnull(ll_barcode)
		li_anno_registrazione = integer(sle_anno_registrazione.text)
		ll_num_registrazione = long(sle_num_registrazione.text)
		ll_prog_riga_ord_ven = long(sle_prog_riga_ord_ven.text)
		
		//ls_cod_reparto = f_po_selectddlb(ddlb_reparto)
		ls_cod_reparto = dw_reparti.getitemstring(1, "cod_reparto")
		
		ls_cod_operaio = sle_cod_operaio.text
		
		if li_anno_registrazione = 0 or isnull(li_anno_registrazione) then
			g_mb.messagebox("SEP","Attenzione manca l'anno registrazione dell'ordine.",stopsign!)
			return
		end if
		
		if ll_num_registrazione = 0 or isnull(ll_num_registrazione) then
			g_mb.messagebox("SEP","Attenzione manca il numero registrazione dell'ordine.",stopsign!)
			return
		end if
		
		if ls_cod_reparto="" or isnull(ls_cod_reparto) then
			g_mb.messagebox("SEP","Attenzione manca il reparto.",stopsign!)
			return
		end if
		
		if ls_cod_operaio="" or isnull(ls_cod_operaio) then
			g_mb.messagebox("SEP","Attenzione manca il codice operaio.",stopsign!)
			return
		end if
		
		li_risposta = iuo_produzione.uof_etichette_apertura(ll_barcode,&
																			 li_anno_registrazione,&
																			 ll_num_registrazione,&
																			 ll_prog_riga_ord_ven,&
																			 ref ls_errore)
		
		if li_risposta < 0 then
			g_mb.messagebox("SEP",ls_errore,stopsign!)
			rollback;
			return
		else
			commit;
		end if
				
				
	// stefano 13/05/2010: specifica etichette sfuso
	case "U"
		setnull(ll_barcode)
		li_anno_registrazione = integer(sle_anno_registrazione.text)
		ll_num_registrazione = long(sle_num_registrazione.text)
		
		li_risposta = iuo_produzione.uof_barcode_anno_reg_sfuso(ll_barcode, &
																			li_anno_registrazione, &
																			ll_num_registrazione, &
																			ls_errore)
																			
		if li_risposta < 0 then
			g_mb.error("SEP", ls_errore)
			rollback;
			return
		else
			s_cs_xx.parametri.parametro_d_1 = long(li_anno_registrazione)
			s_cs_xx.parametri.parametro_d_2 = long(ll_num_registrazione)
			window_open(w_stampa_etichette_sfuso, 0)
		end if
	// ----
	
	case "J"
		//ristampa etichetta scheda prodotto, se previsto
		setnull(ll_barcode)
		li_anno_registrazione = integer(sle_anno_registrazione.text)
		ll_num_registrazione = long(sle_num_registrazione.text)
		ll_prog_riga_ord_ven = long(sle_prog_riga_ord_ven.text)
		
		//ls_cod_reparto = f_po_selectddlb(ddlb_reparto)
		ls_cod_reparto = dw_reparti.getitemstring(1, "cod_reparto")
		
		if li_anno_registrazione = 0 or isnull(li_anno_registrazione) then
			g_mb.messagebox("SEP","Attenzione manca l'anno registrazione dell'ordine.",stopsign!)
			return
		end if
		
		if ll_num_registrazione = 0 or isnull(ll_num_registrazione) then
			g_mb.messagebox("SEP","Attenzione manca il numero registrazione dell'ordine.",stopsign!)
			return
		end if
		
		if ll_prog_riga_ord_ven = 0 or isnull(ll_prog_riga_ord_ven) then
			g_mb.messagebox("SEP","Attenzione manca il numero riga dell'ordine.",stopsign!)
			return
		end if
				
		if ls_cod_reparto="" or isnull(ls_cod_reparto) then
			g_mb.messagebox("SEP","Attenzione manca il reparto.",stopsign!)
			return
		end if
		
		li_risposta = iuo_produzione.uof_etichette_scheda_prodotto(ll_barcode, li_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_reparto, ls_errore)
			
		if li_risposta < 0 then
			g_mb.messagebox("SEP",ls_errore,stopsign!)
			rollback;
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			dw_buffer.reset()
			dw_buffer.insertrow(0)
			return
		else
			commit;
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			dw_buffer.reset()
			dw_buffer.insertrow(0)
		end if
		
		
	case "T"
		//richiesta trasferimento componenti
		setnull(ll_barcode)
		li_anno_registrazione = integer(sle_anno_registrazione.text)
		ll_num_registrazione = long(sle_num_registrazione.text)
		ll_prog_riga_ord_ven = long(sle_prog_riga_ord_ven.text)
		
		if li_anno_registrazione = 0 or isnull(li_anno_registrazione) then
			g_mb.messagebox("SEP","Attenzione manca l'anno registrazione dell'ordine.",stopsign!)
			return
		end if
		
		if ll_num_registrazione = 0 or isnull(ll_num_registrazione) then
			g_mb.messagebox("SEP","Attenzione manca il numero registrazione dell'ordine.",stopsign!)
			return
		end if
		
		if ll_prog_riga_ord_ven = 0 or isnull(ll_prog_riga_ord_ven) then
			g_mb.messagebox("SEP","Attenzione manca il numero riga dell'ordine.",stopsign!)
			return
		end if
		
		lb_stampa = true
		li_risposta = iuo_produzione.uof_richiesta_trasf_componenti(	li_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_barcode, &
																						lb_stampa, dw_richiesta_trasf_componenti, ls_errore)
		if li_risposta < 0 then
			g_mb.error("SEP",ls_errore)
			rollback;
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			dw_buffer.reset()
			dw_buffer.insertrow(0)
			return
		else
			commit;
			st_messaggio.TextColor = RGB(0,0,0)
			st_messaggio.text = "Attesa codice a barre"
			dw_buffer.reset()
			dw_buffer.insertrow(0)
		end if
		
		
		
		
	case else
		
		g_mb.messagebox("APICE","Selezionare l'opzione desiderata",exclamation!)
		return -1
		
end choose

dw_buffer.setfocus()

end event

type st_4 from statictext within w_rileva_produzione
integer x = 1097
integer y = 676
integer width = 448
integer height = 80
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "prog.riga"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_rileva_produzione
integer x = 1097
integer y = 556
integer width = 448
integer height = 80
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Nr.ordine"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_prog_riga_ord_ven from singlelineedit within w_rileva_produzione
integer x = 1577
integer y = 676
integer width = 343
integer height = 100
integer taborder = 90
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
end type

event getfocus;st_messaggio.text = "Inserire valori manualmente"
end event

type sle_num_registrazione from singlelineedit within w_rileva_produzione
integer x = 1577
integer y = 556
integer width = 343
integer height = 100
integer taborder = 80
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
end type

event getfocus;st_messaggio.text = "Inserire valori manualmente"
end event

type st_3 from statictext within w_rileva_produzione
integer x = 215
integer y = 468
integer width = 517
integer height = 76
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Stato lettura"
boolean focusrectangle = false
end type

type st_messaggio from statictext within w_rileva_produzione
integer x = 91
integer y = 68
integer width = 2789
integer height = 200
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "inizio"
alignment alignment = center!
boolean focusrectangle = false
end type

type gb_2 from groupbox within w_rileva_produzione
integer x = 59
integer y = 340
integer width = 914
integer height = 612
integer taborder = 60
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
string text = "Inserimento manuale"
end type

type gb_1 from groupbox within w_rileva_produzione
integer x = 1051
integer y = 340
integer width = 1806
integer height = 616
integer taborder = 70
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Inserimento manuale"
end type

type dw_richiesta_trasf_componenti from datawindow within w_rileva_produzione
boolean visible = false
integer x = 2615
integer y = 56
integer width = 631
integer height = 260
integer taborder = 20
string title = "none"
string dataobject = "d_report_db_varianti_componenti"
boolean border = false
boolean livescroll = true
end type

