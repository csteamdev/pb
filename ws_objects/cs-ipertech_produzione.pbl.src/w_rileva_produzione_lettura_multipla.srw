﻿$PBExportHeader$w_rileva_produzione_lettura_multipla.srw
$PBExportComments$Finestra di rilevazione produzione
forward
global type w_rileva_produzione_lettura_multipla from w_std_principale
end type
type st_1 from statictext within w_rileva_produzione_lettura_multipla
end type
type dw_barcode from datawindow within w_rileva_produzione_lettura_multipla
end type
type cb_1 from commandbutton within w_rileva_produzione_lettura_multipla
end type
type dw_lista from datawindow within w_rileva_produzione_lettura_multipla
end type
end forward

global type w_rileva_produzione_lettura_multipla from w_std_principale
integer width = 3799
integer height = 2052
string title = "LETTURA BARCODE MULTIPLA"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
st_1 st_1
dw_barcode dw_barcode
cb_1 cb_1
dw_lista dw_lista
end type
global w_rileva_produzione_lettura_multipla w_rileva_produzione_lettura_multipla

type variables
boolean ib_exit_4_cancel=true
str_rileva_produzione_multiplo istr_rileva

end variables

on w_rileva_produzione_lettura_multipla.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_barcode=create dw_barcode
this.cb_1=create cb_1
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_barcode
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.dw_lista
end on

on w_rileva_produzione_lettura_multipla.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_barcode)
destroy(this.cb_1)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;string ls_file, ls_ret
dw_lista.reset()

dw_barcode.reset()
dw_barcode.insertrow(0)
dw_barcode.setfocus()

ls_file = s_cs_xx.volume + s_cs_xx.risorse + "12.5\Cancella_record.png"
ls_ret = dw_lista.Modify("b_delete.FileName='"+ ls_file + "'")
ls_file = s_cs_xx.volume + s_cs_xx.risorse + "12.5\Stampa_Label.png"
ls_ret = dw_lista.Modify("b_label.FileName='" + ls_file + "'")

end event

event close;call super::close;long 	ll_i, ll_cont

if not ib_exit_4_cancel then
	ll_cont = 0
	for ll_i = 1 to dw_lista.rowcount()
		if dw_lista.getitemstring(ll_i,3) = "1" then
			ll_cont ++
			istr_rileva.barcode[ll_cont] = dw_lista.getitemstring(ll_i,1)
		end if		
	next
end if

CloseWithReturn ( THIS,  istr_rileva )
end event

type st_1 from statictext within w_rileva_produzione_lettura_multipla
integer x = 1440
integer y = 1860
integer width = 1897
integer height = 80
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 12632256
string text = "Solo le righe di colore BLU saranno elaborate."
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_barcode from datawindow within w_rileva_produzione_lettura_multipla
event ue_key pbm_dwnkey
event ue_reset ( )
integer x = 1463
integer width = 869
integer height = 260
integer taborder = 10
string title = "none"
string dataobject = "d_rileva_produzione_lettura_multipla_input"
boolean border = false
boolean livescroll = true
end type

event ue_key;string	ls_barcode, ls_cod_prodotto, ls_cod_reparto, ls_flag_fine_fase, ls_rag_soc_1, ls_des_prodotto, ls_cod_comodo, ls_des_reparto, ls_flag_visualizza_web
long ll_row, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_barcode, ll_i

if key = keyenter! then
	if getcolumnname() = "barcode" then
		ls_barcode = gettext()
		ll_barcode = long(ls_barcode)
		selecttext( 1, len(ls_barcode))
		
		if g_str.isempty( ls_barcode ) or ll_barcode < 1 then return
		
		// verifico che non ci sia lo stesso barcode già in lista
		for ll_i = 1 to dw_lista.rowcount()
			if ls_barcode = dw_lista.getitemstring(ll_i,1) then
				beep(1)
				return
			end if
		next
		
		
		select		P.anno_registrazione,
					P.num_registrazione,
					P.prog_riga_ord_ven,
					P.cod_prodotto,
					P.cod_reparto,
					P.flag_fine_fase,
					C.rag_soc_1,
					A.des_prodotto,
					A.cod_comodo,
					R.des_reparto,
					R.flag_visualizza_web
		into		:ll_anno_registrazione,
					:ll_num_registrazione,
					:ll_prog_riga_ord_ven,
					:ls_cod_prodotto,
					:ls_cod_reparto,
					:ls_flag_fine_fase,
					:ls_rag_soc_1,
					:ls_des_prodotto,
					:ls_cod_comodo,
					:ls_des_reparto,
					:ls_flag_visualizza_web
		from		det_ordini_produzione P
		left join	tes_ord_ven T on P.cod_azienda=T.cod_azienda and P.anno_registrazione=T.anno_registrazione and P.num_registrazione=T.num_registrazione
		left join   anag_clienti C on C.cod_azienda=T.cod_azienda and C.cod_cliente=T.cod_cliente
		left join 	anag_prodotti A on P.cod_azienda=A.cod_azienda and P.cod_prodotto=A.cod_prodotto
		left join 	anag_reparti R on P.cod_azienda=R.cod_azienda and P.cod_reparto=R.cod_reparto
		where	P.cod_azienda = :s_cs_xx.cod_azienda and
					progr_det_produzione = :ll_barcode;
		if sqlca.sqlcode = 100 then
			ll_row=dw_lista.insertrow(0)
			dw_lista.setitem(ll_row,1, ls_barcode)
			dw_lista.setitem(ll_row,2, "Barcode Inesistente")
			dw_lista.setitem(ll_row,3, "3")
			dw_lista.scrolltorow( ll_row)
			beep(1)
			return
		end if

		if ls_flag_fine_fase = "S" then
			ll_row=dw_lista.insertrow(0)
			dw_lista.setitem(ll_row,1, ls_barcode)
			dw_lista.setitem(ll_row,2, "Fase già Chiusa!")
			dw_lista.setitem(ll_row,3, "3")
			dw_lista.scrolltorow( ll_row)
			beep(1)
			return
		end if
		
		if ls_flag_visualizza_web <> "S" then
			ll_row=dw_lista.insertrow(0)
			dw_lista.setitem(ll_row,1, ls_barcode)
			dw_lista.setitem(ll_row,2, g_str.format("Cliente $1 - Prodotto $2~r~nVerificare il reparto ($3)", trim(ls_rag_soc_1), trim(ls_cod_comodo),ls_des_reparto))
			dw_lista.setitem(ll_row,3, "2")
			dw_lista.scrolltorow( ll_row)
			beep(1)
			return
		end if
				
			
		ll_row=dw_lista.insertrow(0)
		dw_lista.setitem(ll_row,1, ls_barcode)
		dw_lista.setitem(ll_row,2, g_str.format("Cliente $1 - Prodotto $2 - Ordine $3-$4", trim(ls_rag_soc_1), trim(ls_cod_comodo), ll_num_registrazione,ll_prog_riga_ord_ven))
		dw_lista.scrolltorow( ll_row)
	end if	
end if

end event

event ue_reset();reset()
insertrow( 0)

end event

type cb_1 from commandbutton within w_rileva_produzione_lettura_multipla
integer x = 3360
integer y = 1840
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "CONFERMA"
end type

event clicked;ib_exit_4_cancel=false
CLOSE(PARENT)
end event

type dw_lista from datawindow within w_rileva_produzione_lettura_multipla
integer x = 23
integer y = 280
integer width = 3749
integer height = 1540
integer taborder = 10
string title = "none"
string dataobject = "d_rileva_produzione_lettura_multipla"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event buttonclicked;string	ls_errore
long	ll_ret, ll_barcode
uo_produzione	luo_produzione

choose case dwo.name
	case "b_delete"
		deleterow(row)
		
	case "b_label"
		luo_produzione = create uo_produzione
		s_cs_xx.parametri.parametro_s_11 = "RISTAMPA"
		ll_barcode = long( getitemstring(row,1) )
		ll_ret = luo_produzione.uof_etichette(ll_barcode, 0, 0, 0, ls_errore)
		s_cs_xx.parametri.parametro_s_11 = ""
end choose		
end event

