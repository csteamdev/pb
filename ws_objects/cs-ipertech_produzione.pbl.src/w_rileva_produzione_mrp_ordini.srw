﻿$PBExportHeader$w_rileva_produzione_mrp_ordini.srw
forward
global type w_rileva_produzione_mrp_ordini from window
end type
type cb_annulla from commandbutton within w_rileva_produzione_mrp_ordini
end type
type cb_reset from commandbutton within w_rileva_produzione_mrp_ordini
end type
type cb_fine from commandbutton within w_rileva_produzione_mrp_ordini
end type
type st_messaggio from statictext within w_rileva_produzione_mrp_ordini
end type
type dw_lista from datawindow within w_rileva_produzione_mrp_ordini
end type
type dw_buffer from datawindow within w_rileva_produzione_mrp_ordini
end type
end forward

global type w_rileva_produzione_mrp_ordini from window
integer width = 2706
integer height = 1936
boolean titlebar = true
string title = "Lettura codici da stampare"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cb_annulla cb_annulla
cb_reset cb_reset
cb_fine cb_fine
st_messaggio st_messaggio
dw_lista dw_lista
dw_buffer dw_buffer
end type
global w_rileva_produzione_mrp_ordini w_rileva_produzione_mrp_ordini

type variables
datastore			ids_data
string				is_cod_reparto
end variables

forward prototypes
public subroutine wf_reset_dw_buffer ()
public function long wf_verifica_se_presente (long al_barcode)
public function integer wf_prepara_riga_ordine (long al_barcode, integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_tipo, ref string as_errore)
end prototypes

public subroutine wf_reset_dw_buffer ();

dw_buffer.reset()
dw_buffer.insertrow(0)
dw_buffer.setfocus()

return
end subroutine

public function long wf_verifica_se_presente (long al_barcode);//torna il numero riga in cui la riga è presente, altrimenti torn a ZERO

long			ll_index, ll_barcode
integer		li_anno
string		ls_cod_reparto


for ll_index=1 to dw_lista.rowcount()
	ll_barcode = dw_lista.getitemnumber(ll_index, "progr_det_produzione")

	if al_barcode=ll_barcode then
		return ll_index
	end if

next

//se arrivi fin qui vuol dire che non è presente in lista
return 0


end function

public function integer wf_prepara_riga_ordine (long al_barcode, integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_tipo, ref string as_errore);string			ls_sql, ls_cod_prodotto

dec{4}			ld_quantita

datastore		lds_righe

long				ll_tot, ll_index



if as_tipo = "A" then
	//mi serve solo sapere la quantità da proporre (quella residua)
	select d.cod_prodotto, d.quan_ordine - d.quan_evasa - d.quan_in_evasione
	into :ls_cod_prodotto, :ld_quantita
	from det_ord_ven as d
	where 	d.cod_azienda=:s_cs_xx.cod_azienda and
				d.anno_registrazione=:ai_anno_registrazione and
				d.num_registrazione=:al_num_registrazione and
				d.prog_riga_ord_ven=:al_prog_riga_ord_ven and
				d.flag_evasione<>'E' and d.cod_prodotto is not null and
				(d.quan_ordine - d.quan_evasa - d.quan_in_evasione) > 0;
	
	if sqlca.sqlcode<0 then
		as_errore = "Errore in lettura info riga ordine: "+sqlca.sqlerrtext
		return -1
		
	elseif sqlca.sqlcode=100 or ls_cod_prodotto="" or isnull(ls_cod_prodotto) then
		as_errore = "La riga ordine "+string(ai_anno_registrazione)+"/"+string(al_num_registrazione)+"/"+string(al_prog_riga_ord_ven) + " non verrà considerata in quanto " + &
						" potrebbe essere o evasa o con quantità residua pari a zero!"
		return 1
		
	end if
	
	//inserisci la riga nella datawindow
	ll_index = dw_lista.insertrow(0)
	
	dw_lista.setitem(ll_index, "progr_det_produzione", al_barcode)
	dw_lista.setitem(ll_index, "anno_registrazione", ai_anno_registrazione)
	dw_lista.setitem(ll_index, "num_registrazione", al_num_registrazione)
	dw_lista.setitem(ll_index, "prog_riga_ord_ven", al_prog_riga_ord_ven)
	dw_lista.setitem(ll_index, "cod_reparto", is_cod_reparto)
	dw_lista.setitem(ll_index, "cod_prodotto", ls_cod_prodotto)

	dw_lista.scrolltorow(ll_index)
	
	

else	//BoC
	//tipo B,C: recuperare le righe ordine coinvolte nel reparto
	ls_sql = "select distinct "+&
						"d.cod_prodotto,dp.anno_registrazione,dp.num_registrazione,dp.prog_riga_ord_ven,"+&
						"(d.quan_ordine - d.quan_evasa - d.quan_in_evasione) "+&
				"from det_ordini_produzione as dp "+&
				"join det_ord_ven as d on d.cod_azienda=dp.cod_azienda and "+&
													"d.anno_registrazione=dp.anno_registrazione and "+&
													"d.num_registrazione=dp.num_registrazione and "+&
													"d.prog_riga_ord_ven=dp.prog_riga_ord_ven "+&
				"join tes_ord_ven as t on t.cod_azienda=d.cod_azienda and  "+&
													"t.anno_registrazione=d.anno_registrazione and "+&
													"t.num_registrazione=d.num_registrazione "+&
				"where dp.cod_azienda='"+s_cs_xx.cod_azienda+"' and dp.progr_tes_produzione="+string(al_barcode)+" and "+&
    							"d.flag_evasione <> 'E' and d.cod_prodotto is not null and "+&
        						"(d.quan_ordine - d.quan_evasa - d.quan_in_evasione) > 0 and "+&
							"dp.cod_reparto='"+is_cod_reparto+"' "
	
	ll_tot = guo_functions.uof_crea_datastore( lds_righe, ls_sql, as_errore)
	
	if ll_tot<0 then
		destroy lds_righe
		return -1
		
	elseif ll_tot=0 then
		as_errore = "Il barcode di produzione "+string(al_barcode) +" fa riferito a righe dell'ordine vendita "+string(ai_anno_registrazione)+"/"+string(al_num_registrazione) + &
						" evase o con quantità residua pari a zero, quindi non verrà considerato!"
		
		destroy lds_righe
		return 1
		
	end if
	
	
	for ll_index=1 to ll_tot
		ls_cod_prodotto				= lds_righe.getitemstring(ll_index, 1)
		ai_anno_registrazione		= lds_righe.getitemnumber(ll_index, 2)
		al_num_registrazione		= lds_righe.getitemnumber(ll_index, 3)
		al_prog_riga_ord_ven		= lds_righe.getitemnumber(ll_index, 4)
		
		ll_index = dw_lista.insertrow(0)
		dw_lista.setitem(ll_index, "progr_det_produzione", al_barcode)
		dw_lista.setitem(ll_index, "anno_registrazione", ai_anno_registrazione)
		dw_lista.setitem(ll_index, "num_registrazione", al_num_registrazione)
		dw_lista.setitem(ll_index, "prog_riga_ord_ven", al_prog_riga_ord_ven)
		dw_lista.setitem(ll_index, "cod_reparto", is_cod_reparto)
		dw_lista.setitem(ll_index, "cod_prodotto", ls_cod_prodotto)
		
		dw_lista.scrolltorow(ll_index)
		
	next
	
	destroy lds_righe
	
end if



return 0
end function

on w_rileva_produzione_mrp_ordini.create
this.cb_annulla=create cb_annulla
this.cb_reset=create cb_reset
this.cb_fine=create cb_fine
this.st_messaggio=create st_messaggio
this.dw_lista=create dw_lista
this.dw_buffer=create dw_buffer
this.Control[]={this.cb_annulla,&
this.cb_reset,&
this.cb_fine,&
this.st_messaggio,&
this.dw_lista,&
this.dw_buffer}
end on

on w_rileva_produzione_mrp_ordini.destroy
destroy(this.cb_annulla)
destroy(this.cb_reset)
destroy(this.cb_fine)
destroy(this.st_messaggio)
destroy(this.dw_lista)
destroy(this.dw_buffer)
end on

event open;
s_cs_xx_parametri			lstr_parametri
long								ll_index, ll_new


lstr_parametri = message.powerobjectparm

ids_data = lstr_parametri.parametro_ds_1
is_cod_reparto = lstr_parametri.parametro_s_1

ids_data.rowscopy(1, ids_data.rowcount(), Primary!, dw_lista, 1 , Primary!)



wf_reset_dw_buffer()
end event

type cb_annulla from commandbutton within w_rileva_produzione_mrp_ordini
integer x = 2194
integer y = 28
integer width = 402
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;
s_cs_xx_parametri			lstr_parametri

lstr_parametri.parametro_b_1 = false

//poi lascia il datastore intatto
lstr_parametri.parametro_ds_1 = ids_data

closewithreturn(parent, lstr_parametri)
end event

type cb_reset from commandbutton within w_rileva_produzione_mrp_ordini
integer x = 1134
integer y = 28
integer width = 402
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Reset"
end type

event clicked;

if g_mb.confirm( "Sicuro di voler effettuare il rest della lista dei codici barcode scansionati finora (anche quelli precedentemente impostati)?") then
	dw_lista.reset()
	ids_data.reset()
end if
end event

type cb_fine from commandbutton within w_rileva_produzione_mrp_ordini
integer x = 722
integer y = 28
integer width = 402
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Finito"
end type

event clicked;
s_cs_xx_parametri			lstr_parametri



//reset del datastore
ids_data.reset()


//se la dw ha righe popolo il datastore, altrimenti tornerà vuoto
if dw_lista.rowcount()>0 then

	dw_lista.rowscopy(1, dw_lista.rowcount(), Primary!, ids_data, 1 , Primary!)

	//torno con il datastore popolato
	lstr_parametri.parametro_b_1 = true
	lstr_parametri.parametro_ds_1 = ids_data

else
	lstr_parametri.parametro_b_1 = false
end if

closewithreturn(parent, lstr_parametri)



end event

type st_messaggio from statictext within w_rileva_produzione_mrp_ordini
integer x = 37
integer y = 160
integer width = 2642
integer height = 164
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean focusrectangle = false
end type

type dw_lista from datawindow within w_rileva_produzione_mrp_ordini
integer x = 27
integer y = 360
integer width = 2642
integer height = 1464
integer taborder = 20
string title = "none"
string dataobject = "d_rileva_produzione_mrp_ordini"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event rowfocuschanged;

if currentrow>0 then
	selectrow(0, false)
	selectrow(currentrow, true)
end if
end event

type dw_buffer from datawindow within w_rileva_produzione_mrp_ordini
integer x = 27
integer y = 36
integer width = 635
integer height = 100
integer taborder = 10
string title = "none"
string dataobject = "d_buffer"
boolean border = false
boolean livescroll = true
end type

event itemchanged;string					ls_lettura, ls_msg, ls_cod_reparto
long						ll_barcode, ll_num_ordine, ll_riga_ordine, ll_new
integer					li_anno_ordine, li_ret
uo_produzione		luo_prod


choose case dwo.name
	//#####################################################################################
	case "buffer"
		
		st_messaggio.text = ""
		
		if data="" or isnull(data) then
			st_messaggio.text = "Il dato deve essere numerico!"
			wf_reset_dw_buffer()
			return
		end if
			
		ls_lettura = data
		if f_converti_barcode(ls_lettura, ls_msg) < 0 then
			st_messaggio.text = ls_msg
			wf_reset_dw_buffer()
			return
		end if			
		
		if not isnumber(data) then
			st_messaggio.text = "Il dato deve essere numerico!"
			wf_reset_dw_buffer()
			return
		end if
		
		
		ll_barcode = long (ls_lettura)
		
		//verifica che il barcode esista --------------------------------------------------------------------
		setnull(ll_riga_ordine)
		
		luo_prod = create uo_produzione
		li_ret = luo_prod.uof_barcode_anno_reg(ll_barcode, li_anno_ordine, ll_num_ordine, ll_riga_ordine, ls_cod_reparto, ls_msg)
		destroy luo_prod
		
		if li_ret<0 then
			st_messaggio.text = "Barcode " + string(ll_barcode) + " : " + ls_msg
			wf_reset_dw_buffer()
			return
		end if
		
		//verifica caso mai hai già scansionato ----------------------------------------------------------
		li_ret = wf_verifica_se_presente(ll_barcode)
		if li_ret>0 then
			st_messaggio.text = 	"Il Barcode " + string(ll_barcode) + " è già presente in lista!"
			
			dw_lista.scrolltorow(li_ret)
			
			wf_reset_dw_buffer()
			return
		end if
		
		//verifica su reparto (solo quello selezionato all'inizio della procedura)
		if ls_cod_reparto <> is_cod_reparto then
			st_messaggio.text = 	"Il Barcode " + string(ll_barcode) + " si riferisce ad un reparto ("+ls_cod_reparto+") che risulta diverso "+&
												"da quello impostato all'inizio di questa procedura ("+is_cod_reparto+")"
			dw_lista.scrolltorow(li_ret)
			
			wf_reset_dw_buffer()
			return
		end if
		
		ls_msg = ""
		
		if ll_riga_ordine>0 then
			//ordine tipo A
			li_ret = wf_prepara_riga_ordine(ll_barcode,  li_anno_ordine, ll_num_ordine, ll_riga_ordine, "A", ls_msg)
		
		else
			li_ret = wf_prepara_riga_ordine(ll_barcode,  0, 0, 0, "BoC", ls_msg)
			
		end if
		
		if li_ret<>0 then
			st_messaggio.text = ls_msg
		end if
		
		wf_reset_dw_buffer()
		return
		
		//#####################################################################################
		
end choose
end event

