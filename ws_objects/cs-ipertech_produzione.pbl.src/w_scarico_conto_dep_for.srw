﻿$PBExportHeader$w_scarico_conto_dep_for.srw
forward
global type w_scarico_conto_dep_for from w_cs_xx_principale
end type
type note_t from statictext within w_scarico_conto_dep_for
end type
type mle_note from multilineedit within w_scarico_conto_dep_for
end type
type st_log from statictext within w_scarico_conto_dep_for
end type
type dw_list from uo_std_dw within w_scarico_conto_dep_for
end type
type sle_barcode from singlelineedit within w_scarico_conto_dep_for
end type
end forward

global type w_scarico_conto_dep_for from w_cs_xx_principale
integer width = 3008
integer height = 2316
string title = "Scarico Conto Deposito Fornitore"
boolean maxbox = false
boolean center = true
note_t note_t
mle_note mle_note
st_log st_log
dw_list dw_list
sle_barcode sle_barcode
end type
global w_scarico_conto_dep_for w_scarico_conto_dep_for

type variables
constant string IN_ATTESA_DI_BARCODE = "In attesa di barcode"
constant string BARCODE_CHIUSURA = "CHIUDI"
constant long BARCODE_COLOR_WHITE = 16777215
constant long BARCODE_COLOR_ERROR = 12566527

private:
	string is_barcode
	string is_cod_deposito_partenza, is_cod_fornitore, is_cod_tipo_bol_acq
	long il_pos_cod_prodotto
	boolean ib_allow_close = true
	s_cs_xx_parametri istr_data
	
	str_barcode_cdp istr_barcode
end variables

forward prototypes
public subroutine wf_log (string as_log, boolean ab_focus, boolean ab_error)
public function boolean wf_genera_ddt ()
public subroutine wf_stampa_ddt (long al_anno_bolla_acq, long al_num_bolla_acq)
end prototypes

public subroutine wf_log (string as_log, boolean ab_focus, boolean ab_error);st_log.text= as_log

if ab_focus then
	sle_barcode.setfocus()
	sle_barcode.selecttext(1, len(sle_barcode.text))
end if

if ab_error then
	sle_barcode.backcolor = BARCODE_COLOR_ERROR
else
	sle_barcode.backcolor = BARCODE_COLOR_WHITE
end if
end subroutine

public function boolean wf_genera_ddt ();string ls_cod_deposito_arrivo, ls_error, ls_note
long ll_i, ll_anno_bolla_acq, ll_num_bolla_acq, li_j
boolean lb_result
s_det_riga lstr_righe[]
uo_generazione_documenti luo_documenti
uo_magazzino luo_magazzino

dw_list.accepttext()
ls_note = mle_note.text
li_j = 1

if istr_data.parametro_ul_3 = 1 then
	// Aggiungo riferimento all'ordine di vendita
	// lascio vuoto il codice prodotto in modo che la funziona di generazioni calcoli
	// il tipo det RIF appropriato
	lstr_righe[li_j].des_riga = "NS ORDINE " + string(istr_data.parametro_ul_1) + " - " + string(istr_data.parametro_ul_2)	
	li_j++
end if

for ll_i = 1 to dw_list.rowcount()
	
	// considero solo le righe che sono valide
	if dw_list.getitemnumber(ll_i, "colore") = BARCODE_COLOR_WHITE then
		lstr_righe[li_j].cod_prodotto = dw_list.getitemstring(ll_i, "cod_prodotto")
		lstr_righe[li_j].quan_ordine = dw_list.getitemnumber(ll_i, "quantita") * dw_list.getitemnumber(ll_i, "num_colli")
		li_j++
	end if
	
next

// stefanop: 21/12/2011: aggiunto deposito collegato all'operatore
guo_functions.uof_get_stabilimento_utente(ls_cod_deposito_arrivo, ls_error)
// ----

luo_documenti = create uo_generazione_documenti

if luo_documenti.uof_crea_bolla_acquisto(is_cod_fornitore, is_cod_tipo_bol_acq, is_cod_deposito_partenza, ls_cod_deposito_arrivo, lstr_righe,  ref ll_anno_bolla_acq, ref ll_num_bolla_acq, ref ls_error) = 0 then
	commit;
	lb_result = true
	
	if g_mb.confirm("Generata bolla di acquisto " + string(ll_anno_bolla_acq) + "/" + string(ll_num_bolla_acq) + ".~r~nStampare la ricevuta?", 1) then
		
		// stampo
		luo_magazzino = create uo_magazzino
		lb_result = luo_magazzino.uof_stampa_report_scarico_mag_for(ll_anno_bolla_acq, ll_num_bolla_acq, ls_note, ls_error)
		destroy luo_magazzino
	
	end if
	
else
	rollback;
	lb_result = false
	g_mb.error(ls_error)
end if

destroy luo_documenti

return lb_result
end function

public subroutine wf_stampa_ddt (long al_anno_bolla_acq, long al_num_bolla_acq);/* ***/
end subroutine

on w_scarico_conto_dep_for.create
int iCurrent
call super::create
this.note_t=create note_t
this.mle_note=create mle_note
this.st_log=create st_log
this.dw_list=create dw_list
this.sle_barcode=create sle_barcode
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.note_t
this.Control[iCurrent+2]=this.mle_note
this.Control[iCurrent+3]=this.st_log
this.Control[iCurrent+4]=this.dw_list
this.Control[iCurrent+5]=this.sle_barcode
end on

on w_scarico_conto_dep_for.destroy
call super::destroy
destroy(this.note_t)
destroy(this.mle_note)
destroy(this.st_log)
destroy(this.dw_list)
destroy(this.sle_barcode)
end on

event pc_setwindow;call super::pc_setwindow;/**
 * stefanop
 * 22/05/2015
 *
 * Struttura parametri
 * parametro_s_1 = barcode
 * parametro_ul_1 = anno ordine
 * parametro_ul_2 = num_ordine
 * parametro_ul_3 = se 1 allor considera anno/numero ordine se 0 non considerare
 **/

string ls_des_deposito

istr_data = message.powerobjectparm
is_barcode = istr_data.parametro_s_1

// posizione oggetti
dw_list.move(20,20)

// Informazioni aggiuntive
select cod_deposito,
		cod_fornitore,
		cod_tipo_bol_acq,
		posizione_inizio_cod_prodotto,
		num_caratteri_prodotto,
		posizione_inizio_quantita,
		num_caratteri_quantita
into :is_cod_deposito_partenza,
	  :is_cod_fornitore,
	  :is_cod_tipo_bol_acq,
	  :istr_barcode.posizione_prodotto,
	  :istr_barcode.caratteri_prodotto,
	  :istr_barcode.posizione_quantita,
	  :istr_barcode.caratteri_quantita
from con_ril_produzione
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_barcode = :is_barcode;
		

select des_deposito
into :ls_des_deposito
from anag_depositi
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_deposito = :is_cod_deposito_partenza;
		 
		 
title = "Scarico Conto Deposito Fornitore: " + is_cod_deposito_partenza + " - " + ls_des_deposito

wf_log(IN_ATTESA_DI_BARCODE, true, false)


end event

event resize;
mle_note.move(20, newheight - mle_note.height - 20)
mle_note.width = newwidth - 40
note_t.move(20, mle_note.y - note_t.height - 20)

sle_barcode.move(20, note_t.y - sle_barcode.height - 20)
st_log.move(sle_barcode.x + sle_barcode.width + 40, sle_barcode.y + 10)

dw_list.resize(newwidth - 40, sle_barcode.y - 20)

end event

event closequery;call super::closequery;if ib_allow_close then
	return 0
elseif g_mb.confirm("Confermi la chiusura della finestra?") then
	return 0
else 
	return 1
end if
end event

type note_t from statictext within w_scarico_conto_dep_for
integer x = 50
integer y = 1856
integer width = 786
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Note da stampare nel modulo:"
boolean focusrectangle = false
end type

type mle_note from multilineedit within w_scarico_conto_dep_for
integer x = 23
integer y = 1920
integer width = 2926
integer height = 260
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean autohscroll = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type st_log from statictext within w_scarico_conto_dep_for
integer x = 1143
integer y = 1760
integer width = 1783
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "log"
boolean focusrectangle = false
end type

type dw_list from uo_std_dw within w_scarico_conto_dep_for
integer x = 23
integer y = 20
integer width = 2903
integer height = 1680
integer taborder = 10
string dataobject = "d_scarico_conto_dep_for_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

event doubleclicked;call super::doubleclicked;if row > 0 then
	if g_mb.confirm("Cancellare la riga selezionata?") then
		deleterow(row)
	end if
end if

wf_log(IN_ATTESA_DI_BARCODE, true, false)
end event

type sle_barcode from singlelineedit within w_scarico_conto_dep_for
integer x = 41
integer y = 1736
integer width = 1056
integer height = 96
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "barcode"
borderstyle borderstyle = stylelowered!
end type

event modified;string ls_barcode, ls_cod_prodotto, ls_des_prodotto
int li_row, li_quantita, li_num_colli, li_count
long ll_colore
dec{4} ld_quan_utilizzo

// validazione
if isnull(text) or len(trim(text)) < 1 then return
ls_barcode = upper(trim(text))

// E' il barcode di chiusura?
if ls_barcode = BARCODE_CHIUSURA then
	if wf_genera_ddt() then 
		ib_allow_close = true
		close(parent)
	end if
	
	return 0
end if

// la lunghezza del barcode deve essere almeno la lunghezza del prodotto impostato
// configurazione
/*if len(ls_barcode) < ((istr_barcode.posizione_prodotto - 1) + istr_barcode.caratteri_prodotto) then
	wf_log("Barcode non valido", true, true)
	return 0
end if*/

li_quantita = 1
li_num_colli = 1

try
	ls_cod_prodotto = trim(mid(ls_barcode, istr_barcode.posizione_prodotto, istr_barcode.caratteri_prodotto))
	li_quantita = integer(trim(mid(ls_barcode, istr_barcode.posizione_quantita, istr_barcode.caratteri_quantita)))
	
	if isnull(li_quantita) or li_quantita <= 0 then
		li_quantita = 1
	end if
	
catch(RuntimeError e)
		wf_log("Errore durante la trasformazione del barcode; controllare la tabella di configurazione.~r~n" + e.getMessage(), true, false)
		return
end try

// Controllo e recupero descrizione del prodotto
select des_prodotto
into :ls_des_prodotto
from anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :ls_cod_prodotto;
		 
if sqlca.sqlcode < 0 then
	wf_log("Errore durante la ricerca del prodotto.", true, true)
	return 0
elseif sqlca.sqlcode = 100 then
	wf_log("Prodotto non trovato!", true, true)
	ll_colore = BARCODE_COLOR_ERROR
	return 0
end if

ll_colore = BARCODE_COLOR_WHITE

// Se l'utente spara due volte lo stesso barcode, devo aumentare la quantità
li_row = dw_list.find("cod_prodotto='" + ls_cod_prodotto + "'", 1, dw_list.rowcount())

if li_row > 0 then
	li_num_colli = dw_list.getitemnumber(li_row, "num_colli")
	li_num_colli ++
	dw_list.setitem(li_row, "num_colli", li_num_colli)
	wf_log("Quantità prodotto " + ls_cod_prodotto + " aumentata", true, false)
	return 0
end if
// ----

// Verifico se devo controllare il prodotto, all'interno dell'ordine di vendita
if istr_data.parametro_ul_3 = 1 then
	select count(*)
	into :li_count
	from comp_det_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :istr_data.parametro_ul_1 and 
			 num_registrazione = :istr_data.parametro_ul_2 and
			 cod_comando = :ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il controllo nella tabella comp_det_ord_ven", sqlca)
		return
	elseif sqlca.sqlcode = 100 or li_count = 0 then
		select count(*)
		into :li_count
		from det_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :istr_data.parametro_ul_1 and 
				 num_registrazione = :istr_data.parametro_ul_2 and
				 cod_prodotto = :ls_cod_prodotto;
			 
			 if sqlca.sqlcode < 0 then
				g_mb.error("Errore durante il controllo nella tabella det_ord_ven", sqlca)
				return
			elseif sqlca.sqlcode = 100 or li_count = 0 then
				if not g_mb.confirm("Il prodotto " + ls_cod_prodotto + " - " + ls_des_prodotto + " non è stato trovato nell'ordine di riferimento.~r~nAggiungerlo lo stesso alla lista?") then
					return
				end if
			end if
	end if
end if
// ----

// Inserisco il prodotto in lista
li_row = dw_list.insertrow(0)
dw_list.setitem(li_row, "cod_prodotto", ls_cod_prodotto)
dw_list.setitem(li_row, "des_prodotto", ls_des_prodotto)
dw_list.setitem(li_row, "quantita", li_quantita)
dw_list.setitem(li_row, "num_colli", li_num_colli)
dw_list.setitem(li_row, "colore", ll_colore)
dw_list.setredraw(true)
wf_log(IN_ATTESA_DI_BARCODE, true, false)

ib_allow_close = false
sle_barcode.text = ""
sle_barcode.setfocus()

end event

