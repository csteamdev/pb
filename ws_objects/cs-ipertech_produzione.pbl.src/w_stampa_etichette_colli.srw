﻿$PBExportHeader$w_stampa_etichette_colli.srw
forward
global type w_stampa_etichette_colli from w_cs_xx_risposta
end type
type cb_1 from commandbutton within w_stampa_etichette_colli
end type
type em_num_colli from editmask within w_stampa_etichette_colli
end type
type num_colli_t from statictext within w_stampa_etichette_colli
end type
end forward

global type w_stampa_etichette_colli from w_cs_xx_risposta
integer width = 1083
integer height = 860
string title = "Numero Colli"
cb_1 cb_1
em_num_colli em_num_colli
num_colli_t num_colli_t
end type
global w_stampa_etichette_colli w_stampa_etichette_colli

type variables
private:
	boolean ib_close = false
	str_stampa_etichette_colli istr_data
end variables

forward prototypes
public subroutine wf_carica_num_colli ()
end prototypes

public subroutine wf_carica_num_colli ();/**
 * stefanop
 * 06/07/2016
 *
 * Carico il numero di colli
 **/
 
int li_num_colli

select num_colli
into :li_num_colli
from tes_ord_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :istr_data.anno_registrazione and
		 num_registrazione = :istr_data.num_registrazione;
		 
if sqlca.sqlcode <> 0 or isnull(li_num_colli) or li_num_colli = 0 then
	li_num_colli = 1
end if

em_num_colli.text = string(li_num_colli)
em_num_colli.SelectText(1, len(em_num_colli.text))
end subroutine

on w_stampa_etichette_colli.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.em_num_colli=create em_num_colli
this.num_colli_t=create num_colli_t
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.em_num_colli
this.Control[iCurrent+3]=this.num_colli_t
end on

on w_stampa_etichette_colli.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.em_num_colli)
destroy(this.num_colli_t)
end on

event pc_setwindow;call super::pc_setwindow;
istr_data = message.powerobjectparm

setnull( message.powerobjectparm)

wf_carica_num_colli()
end event

event closequery;call super::closequery;if(ib_close) then
	return 0
else
	return 1
end if
end event

type cb_1 from commandbutton within w_stampa_etichette_colli
integer x = 37
integer y = 560
integer width = 987
integer height = 144
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Imposta"
boolean default = true
end type

event clicked;istr_data.num_colli = integer(em_num_colli.text)
ib_close = true

closeWithReturn(parent, istr_data)
end event

type em_num_colli from editmask within w_stampa_etichette_colli
integer x = 37
integer y = 128
integer width = 987
integer height = 144
integer taborder = 10
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##0"
end type

type num_colli_t from statictext within w_stampa_etichette_colli
integer x = 37
integer y = 32
integer width = 987
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Numero Colli:"
boolean focusrectangle = false
end type

