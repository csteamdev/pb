﻿$PBExportHeader$w_stato_elaborazione.srw
forward
global type w_stato_elaborazione from window
end type
type cb_1 from commandbutton within w_stato_elaborazione
end type
type st_log from statictext within w_stato_elaborazione
end type
end forward

global type w_stato_elaborazione from window
integer width = 1495
integer height = 444
windowtype windowtype = popup!
long backcolor = 268435456
string icon = "AppIcon!"
boolean center = true
cb_1 cb_1
st_log st_log
end type
global w_stato_elaborazione w_stato_elaborazione

on w_stato_elaborazione.create
this.cb_1=create cb_1
this.st_log=create st_log
this.Control[]={this.cb_1,&
this.st_log}
end on

on w_stato_elaborazione.destroy
destroy(this.cb_1)
destroy(this.st_log)
end on

type cb_1 from commandbutton within w_stato_elaborazione
boolean visible = false
integer x = 539
integer y = 340
integer width = 402
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Nascondi"
end type

event clicked;close(parent)
end event

type st_log from statictext within w_stato_elaborazione
integer x = 37
integer y = 20
integer width = 1426
integer height = 296
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 268435456
string text = "inizio elaborazione"
alignment alignment = center!
boolean focusrectangle = false
end type

