﻿$PBExportHeader$uo_certificati_dop.sru
forward
global type uo_certificati_dop from nonvisualobject
end type
end forward

global type uo_certificati_dop from nonvisualobject
end type
global uo_certificati_dop uo_certificati_dop

type variables
uo_std_dw luo_certificato, luo_dichiarazione

end variables

forward prototypes
public function integer wf_classe_gtot (decimal ad_gtot, ref long al_classe)
public function integer uof_merge_pdf (uo_std_dw adw_certificazione_prod, uo_std_dw adw_dichiarazione_prestazione, boolean ab_flag_certificazione, boolean ab_flag_dichiarazione, ref string as_path_merged)
public function integer uof_archivia_certificati (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_path, transaction at_tran)
public function integer uof_genera_certificato (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_matricole[], ref uo_std_dw adw_report, ref uo_std_dw adw_dichiarazione, ref string as_message)
public function integer uof_invia_certificati (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_path_file_certificato, ref string as_messaggio)
end prototypes

public function integer wf_classe_gtot (decimal ad_gtot, ref long al_classe);if ad_gtot = 0 then return -1
choose case ad_gtot
	case is >= 0.50
		al_classe = 0
	case is >= 0.35
		al_classe = 1
	case is >= 0.15
		al_classe = 2
	case is >= 0.10
		al_classe = 3
	case else
		al_classe = 4
end choose
return 0

end function

public function integer uof_merge_pdf (uo_std_dw adw_certificazione_prod, uo_std_dw adw_dichiarazione_prestazione, boolean ab_flag_certificazione, boolean ab_flag_dichiarazione, ref string as_path_merged);/* Esegue il merge dei file PDF generati
Valore di ritorno		0= tutto ok
							-1=errore (testo messaggio errore nella variabile as_path_merged
							1 = tutto ok ma nessun file selezionato
*/

long	ll_ret, ll_i
string	ls_file1, ls_file2, ls_temp, ls_pdf[], ls_merged, ls_errore
uo_pdf luo_pdf

ll_i = 0
if ab_flag_certificazione then
	ll_i ++
	ls_pdf[ll_i]=guo_functions.uof_get_random_filename( )
	ls_temp = guo_functions.uof_get_user_temp_folder( )
	ls_pdf[ll_i] =  ls_temp + ls_pdf[ll_i]
	adw_certificazione_prod.saveas( ls_pdf[ll_i], pdf!, false)
	f_log_sistema ( g_str.format("Certificati DOP - PDF 1:'$1'  Dataobject='$2'  Matricola='$3' ", ls_pdf[ll_i], adw_certificazione_prod.dataobject, adw_certificazione_prod.getitemstring(1,"matricola") )  , "PDF" )
end if

sleep(1)

if ab_flag_dichiarazione then
	ll_i ++
	ls_pdf[ll_i]=guo_functions.uof_get_random_filename( )
	ls_temp = guo_functions.uof_get_user_temp_folder( )
	ls_pdf[ll_i] =  ls_temp + ls_pdf[ll_i]
	adw_dichiarazione_prestazione.saveas( ls_pdf[ll_i], pdf!, false)
	f_log_sistema ( g_str.format("Certificati DOP - PDF 1:'$1'  Dataobject='$2'  Modello='$3' ", ls_pdf[ll_i], adw_dichiarazione_prestazione.dataobject, adw_dichiarazione_prestazione.getitemstring(1,"modelllo_tenda") )  , "PDF" )
end if

if upperbound(ls_pdf) = 0 then return 1

luo_pdf = create uo_pdf

ls_merged= guo_functions.uof_get_user_documents_folder( ) + guo_functions.uof_get_random_filename( ) + ".pdf"

ll_ret=luo_pdf.uof_merge( ls_pdf[], ls_merged, ls_errore)
if ll_ret < 0 then
	as_path_merged=ls_errore
	return -1
else
	as_path_merged=ls_merged
	return 0
end if

end function

public function integer uof_archivia_certificati (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_path, transaction at_tran);long ll_prog_mimytype,ll_ret
blob l_blob
string ls_note, ls_nota, ls_cod_nota

ll_ret = f_file_to_blob(as_path, l_blob)
if ll_ret < 0 then return -1

ll_ret = len(l_blob)


SELECT prog_mimetype  
INTO   :ll_prog_mimytype  
FROM   tab_mimetype  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and
		 estensione = 'PDF'  OR estensione = 'pdf';
		 
		 
if sqlca.sqlcode = 100 then
	g_mb.messagebox("APICE","Impossibile archiviare il documento: impostare il mimetype")
	return 0
end if

setnull(ls_cod_nota)

select max(cod_nota)
into   :ls_cod_nota
from   det_ord_ven_note
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione and
		 prog_riga_ord_ven = :al_prog_riga_ord_ven and
		 cod_nota like 'C%';
		 
if isnull(ls_cod_nota) or len(ls_cod_nota) < 1 then
	ls_cod_nota = "C01"
else
	ls_cod_nota = right(ls_cod_nota, 2)
	ll_ret = long(ls_cod_nota)
	ll_ret ++
	ls_cod_nota = "C" + string(ll_ret, "00")
end if

ls_nota = "Invio certificati tramite e-mail"
ls_note = "Cerrtificazioni inviate dall'utente " + s_cs_xx.cod_utente + "~r~n" + &
          "Data invio:" + string(today(), "dd/mm/yyyy") + "  Ora invio:" + string(now(), "hh:mm:ss")

INSERT INTO det_ord_ven_note  
		( cod_azienda,
		  anno_registrazione,   
		  num_registrazione,   
		  prog_riga_ord_ven,
		  cod_nota,   
		  des_nota, 
		  note,
		  prog_mimetype)  
VALUES ( :s_cs_xx.cod_azienda,   
		  :al_anno_registrazione,   
		  :al_num_registrazione,   
		  :al_prog_riga_ord_ven,
		  :ls_cod_nota,   
		  :ls_nota,   
		  :ls_note,
		  :ll_prog_mimytype )
using at_tran;

updateblob det_ord_ven_note
set       	  note_esterne = :l_blob
where      cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :al_anno_registrazione and
			  num_registrazione = :al_num_registrazione and
			  prog_riga_ord_ven = :al_prog_riga_ord_ven and
			  cod_nota = :ls_cod_nota
using at_tran;



end function

public function integer uof_genera_certificato (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_matricole[], ref uo_std_dw adw_report, ref uo_std_dw adw_dichiarazione, ref string as_message);string		ls_cod_prodotto, ls_des_prodotto, ls_des_breve, ls_cod_comodo, ls_cod_tessuto, ls_cod_colore_tessuto, ls_cod_verniciatura,ls_cod_comando,ls_pos_comando, ls_des_verniciatura, &
			ls_des_comando, ls_path_r1_c1, ls_path_r1_c2, ls_path_logo, ls_modify,ls_azienda_rag_soc_1,ls_azienda_indirizzo,ls_azienda_localita,ls_azienda_cap,ls_azienda_prov,ls_dop_anno, &
			ls_dop_usi_previsti, ls_dop_sistema_vvcp, ls_dop_norma, ls_dop_des_libera, ls_dop_flag_car_vento, ls_dop_vento_des_prestazioni, ls_dop_vento_norma, ls_dop_trasmittanza_norma, &
			ls_testo_c1, ls_testo_c2, ls_path_r1_d1, ls_path_r1_d2
			
long		ll_file, ll_ret, ll_classe, ll_i, ll_num_tende, ll_row1, ll_row2

dec{4}	ld_dim_x,ld_dim_y, ld_val_gtot, ld_classe_vento



select 
A.cod_prodotto, 
B.des_prodotto, 
B.des_breve, 
B.cod_comodo, 
COMP.cod_tessuto, 
COMP.cod_non_a_magazzino cod_colore_tessuto, 
COMP.dim_x,
COMP.dim_y,
COMP.cod_verniciatura,
COMP.cod_comando,
COMP.pos_comando,
T.val_gtot,
VERN.des_prodotto,
COM.des_prodotto,
B.cod_barre,
AZI.rag_soc_1 ,
AZI.indirizzo ,
AZI.localita ,
AZI.cap ,
AZI.provincia,
B.cod_comodo_2,
MODEL.dop_usi_previsti ,
MODEL.dop_sistema_vvcp ,
MODEL.dop_norma ,
MODEL.dop_des_libera,
MODEL.dop_flag_car_vento ,
MODEL.dop_vento_des_prestazioni ,
MODEL.dop_vento_norma ,
MODEL.dop_trasmittanza_norma 
into
:ls_cod_prodotto, 
:ls_des_prodotto, 
:ls_des_breve, 
:ls_cod_comodo,
:ls_cod_tessuto, 
:ls_cod_colore_tessuto, 
:ld_dim_x,
:ld_dim_y,
:ls_cod_verniciatura,
:ls_cod_comando,
:ls_pos_comando,
:ld_val_gtot,
:ls_des_verniciatura,
:ls_des_comando,
:ld_classe_vento,
:ls_azienda_rag_soc_1,
:ls_azienda_indirizzo,
:ls_azienda_localita,
:ls_azienda_cap,
:ls_azienda_prov,
:ls_dop_anno,
:ls_dop_usi_previsti ,
:ls_dop_sistema_vvcp ,
:ls_dop_norma ,
:ls_dop_des_libera,
:ls_dop_flag_car_vento ,
:ls_dop_vento_des_prestazioni ,
:ls_dop_vento_norma ,
:ls_dop_trasmittanza_norma 
from det_ord_ven A
left outer join anag_prodotti B on A.cod_azienda = B.cod_azienda and A.cod_prodotto = B.cod_prodotto
left outer join comp_det_ord_ven COMP on A.cod_azienda=B.cod_azienda and A.anno_registrazione=COMP.anno_registrazione and A.num_registrazione=COMP.num_registrazione and A.prog_riga_ord_ven=COMP.prog_riga_ord_ven
left outer join tab_colori_tessuti T on COMP.cod_azienda=T.cod_azienda and COMP.cod_non_a_magazzino=T.cod_colore_tessuto
left outer join anag_prodotti VERN on COMP.cod_azienda=VERN.cod_azienda and COMP.cod_verniciatura=VERN.cod_prodotto
left outer join anag_prodotti COM on COMP.cod_azienda=COM.cod_azienda and COMP.cod_comando=COM.cod_prodotto
left outer join tab_flags_configuratore MODEL on A.cod_azienda=MODEL.cod_azienda and A.cod_prodotto = MODEL.cod_modello
join aziende AZI on AZI.cod_azienda = A.cod_azienda
where A.cod_azienda= :s_cs_xx.cod_azienda and
A.anno_registrazione = :al_anno_registrazione and
A.num_registrazione = :al_num_registrazione and
A.prog_riga_ord_ven = :al_prog_riga_ord_ven;
if sqlca.sqlcode = 100 then
	as_message = g_str.format("Errore in caricamento dati del Report. Dati non trovati per la riga ordine $1-$2-$3",al_anno_registrazione,al_num_registrazione,al_prog_riga_ord_ven)
	return -1
end if

if sqlca.sqlcode < 0 then
	as_message = g_str.format("Errore SQL nel caricamento dati del Report. Rif. riga ordine $1-$2-$3",al_anno_registrazione,al_num_registrazione,al_prog_riga_ord_ven)
	return -1
end if

ll_num_tende = upperbound(as_matricole[])

// -------------- primo report certificazione del produttore --------------------

adw_report.reset()
adw_dichiarazione.reset()

ls_path_r1_c1 = s_cs_xx.volume + s_cs_xx.risorse + "Certificato_Testata.txt"
ls_path_r1_c2 = s_cs_xx.volume + s_cs_xx.risorse + "Certificato_Piede.txt"

if not fileexists(ls_path_r1_c1) then
	as_message = g_str.format("Attenzione, non è stato trovato il file con il testo iniziale. $1",ls_path_r1_c1)
	return -1
end if

if not fileexists(ls_path_r1_c1) then
	as_message = g_str.format("Attenzione, non è stato trovato il file con il testo finale. $1",ls_path_r1_c2)
	return -1
end if

ll_file = fileopen(ls_path_r1_c1, TextMode!, Read!)
if ll_file < 0 then
	as_message =  g_str.format("Errore nell'apertura del file con il testo iniziale. $1",ls_path_r1_c1)
	return -1
end if
ll_ret=filereadex(ll_file, ls_testo_c1)
if ll_ret < 0 then
	as_message =  g_str.format("Errore lettura del file con il testo iniziale. $1",ls_path_r1_c1)
	return -1
end if
fileclose(ll_file)

ll_file = fileopen(ls_path_r1_c2, TextMode!, Read!)
if ll_file < 0 then
	as_message = g_str.format("Errore nell'apertura del file con il testo iniziale. $1",ls_path_r1_c2)
	return -1
end if
ll_ret= filereadex(ll_file, ls_testo_c2)
if ll_ret < 0 then
	as_message =  g_str.format("Errore lettura del file con il testo iniziale. $1",ls_path_r1_c2)
	return -1
end if
fileclose(ll_file)


ls_path_r1_d1 = s_cs_xx.volume + s_cs_xx.risorse + "Dich_prestazione_Testata.txt"
ls_path_r1_d2 = s_cs_xx.volume + s_cs_xx.risorse + "Dich_prestazione_Piede.txt"

if not fileexists(ls_path_r1_d1) then
	as_message =  g_str.format("Attenzione, non è stato trovato il file con il testo iniziale. $1",ls_path_r1_d1)
	return -1
end if

if not fileexists(ls_path_r1_d2) then
	as_message =  g_str.format("Attenzione, non è stato trovato il file con il testo finale. $1",ls_path_r1_d2)
	return -1
end if

ll_file = fileopen(ls_path_r1_d1, TextMode!, Read!)
if ll_file < 0 then
	as_message =  g_str.format("Errore nell'apertura del file con il testo iniziale. $1",ls_path_r1_d1)
	return -1
end if
ll_ret=filereadex(ll_file, ls_path_r1_d1)
if ll_ret < 0 then
	as_message =  g_str.format("Errore lettura del file con il testo iniziale. $1",ls_path_r1_d1)
	return -1
end if

fileclose(ll_file)

ll_file = fileopen(ls_path_r1_d2, TextMode!, Read!)
if ll_file < 0 then
	as_message =  g_str.format("Errore nell'apertura del file con il testo iniziale. $1",ls_path_r1_d2)
	return -1
end if
ll_ret= filereadex(ll_file, ls_path_r1_d2)
if ll_ret < 0 then
	as_message =  g_str.format("Errore lettura del file con il testo iniziale. $1",ls_path_r1_d2)
	return -1
end if

fileclose(ll_file)


for ll_i = 1 to ll_num_tende

	ll_row1=adw_report.insertrow(0)

	adw_report.setitem(ll_row1, "corpo_1", ls_testo_c1)

	adw_report.setitem(ll_row1, "corpo_2", ls_testo_c2)


	// 30-08-2018 Chiesto da Paride, su campo modello tenda deve apparire solo il contenuto del campo cod_comodo
	adw_report.setitem(ll_row1, "modelllo_tenda", ls_cod_comodo )
	adw_report.setitem(ll_row1, "dop_riferimento", g_str.format("$1 - $2",ls_cod_comodo, string(ld_val_gtot,"0.00") ))
	adw_report.setitem(ll_row1, "tessuto", g_str.format("$1  $2",ls_cod_tessuto, ls_cod_colore_tessuto))
	adw_report.setitem(ll_row1, "misure", g_str.format("Larghezza cm.$1 - Sporgenza cm.$2", string(ld_dim_x,"###,##0.0"),string(ld_dim_y,"###,##0.0")  ))
	adw_report.setitem(ll_row1, "colore_struttura", ls_des_verniciatura)
	adw_report.setitem(ll_row1, "movimentazione", g_str.format("$1 - Lato $2",ls_des_comando, ls_pos_comando))
	adw_report.setitem(ll_row1, "matricola", as_matricole[ll_i])
	
	guo_functions.uof_get_parametro_azienda("LC1", ls_path_logo)
	ls_modify = "p_logo_lato.filename='" + s_cs_xx.volume + s_cs_xx.risorse + ls_path_logo + "'~t"
	
	guo_functions.uof_get_parametro_azienda("LC2", ls_path_logo)
	ls_modify += "p_logo_piede.filename='" + s_cs_xx.volume + s_cs_xx.risorse + ls_path_logo + "'~t"
	
	guo_functions.uof_get_parametro_azienda("LC3", ls_path_logo)
	ls_modify += "p_firma.filename='" + s_cs_xx.volume + s_cs_xx.risorse + ls_path_logo + "'~t"
	adw_report.modify(ls_modify)

// -------------- secondo report:  dichiarazione di prestazione -------------------- //

	ll_row2=adw_dichiarazione.insertrow(0)

	adw_dichiarazione.setitem(ll_row2, "corpo_1", ls_path_r1_d1)
	adw_dichiarazione.setitem(ll_row2, "corpo_2", ls_path_r1_d2)


	adw_dichiarazione.setitem(ll_row2, "modelllo_tenda",  g_str.format("$1 - $2",ls_cod_comodo, string(ld_val_gtot,"0.00") ))
	
	adw_dichiarazione.setitem(ll_row2, "rag_doc_produttore", ls_azienda_rag_soc_1)
	adw_dichiarazione.setitem(ll_row2, "indirizzo_produttore", ls_azienda_indirizzo)
	adw_dichiarazione.setitem(ll_row2, "cap_produttore", ls_azienda_cap)
	adw_dichiarazione.setitem(ll_row2, "localitaproduttore", ls_azienda_localita)
	adw_dichiarazione.setitem(ll_row2, "prov_produttore", ls_azienda_prov)
	adw_dichiarazione.setitem(ll_row2, "classe_vento", g_str.format("Classe: $1", string(ld_classe_vento,"#0")))
	
	ll_ret = wf_classe_gtot( ld_val_gtot, ll_classe)
	if ll_ret = 0 then
		adw_dichiarazione.setitem(ll_row2, "trasmittanza", g_str.format("Gtot: $1   Classe $2",string(ld_val_gtot,"##0.00"), ll_classe))
	else
		adw_dichiarazione.setitem(ll_row2, "trasmittanza", g_str.format("Gtot: $1 ",string(ld_val_gtot,"##0.00")))
	end if	
	
	adw_dichiarazione.setitem(ll_row2, "dop_usi_previsti", ls_dop_usi_previsti)
	adw_dichiarazione.setitem(ll_row2, "dop_sistema_vvcp", ls_dop_sistema_vvcp)
	adw_dichiarazione.setitem(ll_row2, "dop_norma",ls_dop_norma )
	adw_dichiarazione.setitem(ll_row2, "dop_des_libera", ls_dop_des_libera)
	adw_dichiarazione.setitem(ll_row2, "dop_flag_car_vento", ls_dop_flag_car_vento)
	adw_dichiarazione.setitem(ll_row2, "dop_vento_des_prestazioni", ls_dop_vento_des_prestazioni)
	adw_dichiarazione.setitem(ll_row2, "dop_vento_norma", ls_dop_vento_norma)
	adw_dichiarazione.setitem(ll_row2, "dop_trasmittanza_norma", ls_dop_trasmittanza_norma)
	
	guo_functions.uof_get_parametro_azienda("LC3", ls_path_logo)
	ls_modify = "p_firma.filename='" + s_cs_xx.volume + s_cs_xx.risorse + ls_path_logo + "'~t"
	
	guo_functions.uof_get_parametro_azienda("LC4", ls_path_logo)
	ls_modify += "p_logo_ce.filename='" + s_cs_xx.volume + s_cs_xx.risorse + ls_path_logo + "'~t"
	
	adw_dichiarazione.modify(ls_modify)
next

return 0
end function

public function integer uof_invia_certificati (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_path_file_certificato, ref string as_messaggio);long					ll_ret
string					ls_rag_soc_1, ls_destinatari[],ls_allegati[], ls_subject, ls_user_email, ls_azienda, ls_message, ls_messaggio,ls_errore
uo_outlook			luo_outlook
transaction			lt_tran_docs
	

select A.rag_soc_1 rag_soc_cliente, 
		A.casella_mail,
		Z.rag_soc_1 rag_soc_azienda
into	:ls_rag_soc_1,
		:ls_destinatari[1],
		:ls_azienda
from	tes_ord_ven T
left outer join anag_clienti A on T.cod_azienda=A.cod_azienda and T.cod_cliente=A.cod_cliente
left outer join aziende Z on Z.cod_azienda = T.cod_azienda
where	T.cod_azienda = :s_cs_xx.cod_azienda and
			T.anno_registrazione = :al_anno_registrazione and
			T.num_registrazione = :al_num_registrazione;
if sqlca.sqlcode <> 0 then
	as_messaggio = g_str.format("Errore SQL durante la ricerca ordine $1 - $2", al_anno_registrazione,al_num_registrazione)
	return -1
end if


ls_allegati[1]    = as_path_file_certificato

select		e_mail
into		:ls_user_email
from 		utenti
where	cod_utente  = :s_cs_xx.cod_utente;
	
if sqlca.sqlcode = 0 and not isnull(ls_user_email) and len(ls_user_email) > 0 then
	ls_destinatari[2] = ls_user_email
else
	as_messaggio = "Email mittente non specificata; non verrà mandata alcuna mail al mittente!"
	return -1
end if	

ls_subject = g_str.format("$1 - Invio Certificazione e DoP. ", ls_azienda)
ls_message = "Gentile Cliente,~r~nalla presente alleghiamo i documenti richiesti.~r~n~r~nCordiali Saluti~r~nAlusistemi srl"

luo_outlook = create uo_outlook
luo_outlook.ib_html = false

luo_outlook.ib_silent_mode=true
if not luo_outlook.uof_invio_sendmail( ls_destinatari[], ls_subject, ls_message, ls_allegati[], ref ls_messaggio) then
	as_messaggio= "Errore in fase di invio mail certificati: " + ls_messaggio
	return -1
else
	
	//creo transazione per i documenti
	if not guo_functions.uof_create_transaction_from( sqlca, lt_tran_docs, ls_errore)  then
		//problemi in creazione transazione documenti, non archiviare il PDF
		as_messaggio = g_str.format("A causa di un errore in creazione transazione non è stato possibile archiviare il PDF , ma l'email è stata inviata correttamente!~r~n$1",ls_errore)
		return -1
	else
		uof_archivia_certificati(al_anno_registrazione,al_num_registrazione, al_prog_riga_ord_ven,as_path_file_certificato, lt_tran_docs)
		commit;
		commit using lt_tran_docs;
		
		disconnect using lt_tran_docs;
		
	end if
	
end if
	
destroy luo_outlook

filedelete(as_path_file_certificato)

return 0
end function

event constructor;/*
Funzione di stampa / invio / archiviazione  dei certificati DOP
*/
end event

on uo_certificati_dop.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_certificati_dop.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

