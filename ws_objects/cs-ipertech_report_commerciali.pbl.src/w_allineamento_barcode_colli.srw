﻿$PBExportHeader$w_allineamento_barcode_colli.srw
$PBExportComments$REPORT RIGHE ORDINI
forward
global type w_allineamento_barcode_colli from w_cs_xx_principale
end type
type cb_4 from commandbutton within w_allineamento_barcode_colli
end type
type sle_max_num_colli from singlelineedit within w_allineamento_barcode_colli
end type
type st_1 from statictext within w_allineamento_barcode_colli
end type
type cb_3 from commandbutton within w_allineamento_barcode_colli
end type
type mle_log from multilineedit within w_allineamento_barcode_colli
end type
type st_msg2 from statictext within w_allineamento_barcode_colli
end type
type cb_2 from commandbutton within w_allineamento_barcode_colli
end type
type st_msg from statictext within w_allineamento_barcode_colli
end type
type cb_1 from commandbutton within w_allineamento_barcode_colli
end type
type dw_selezione from uo_cs_xx_dw within w_allineamento_barcode_colli
end type
type cb_reset from commandbutton within w_allineamento_barcode_colli
end type
end forward

global type w_allineamento_barcode_colli from w_cs_xx_principale
integer x = 5
integer y = 4
integer width = 3397
integer height = 2412
string title = "Report Lista Carico Spedizione"
event ue_posizione ( )
event ue_scroll_log ( )
cb_4 cb_4
sle_max_num_colli sle_max_num_colli
st_1 st_1
cb_3 cb_3
mle_log mle_log
st_msg2 st_msg2
cb_2 cb_2
st_msg st_msg
cb_1 cb_1
dw_selezione dw_selezione
cb_reset cb_reset
end type
global w_allineamento_barcode_colli w_allineamento_barcode_colli

type variables
datastore ids_ordini_A, ids_ordini_BC, ids_tab_ord_ven_colli
string is_cod_operaio = "1"
end variables

forward prototypes
public subroutine wf_where (ref string fs_where)
public function integer wf_leggi ()
public function integer wf_elabora_ordini_bc ()
public function integer wf_elabora_ordini_a ()
public function integer wf_inserisci_colli (ref string fs_msg)
public function integer wf_inserisci_ds_colli (long fl_barcode, long fl_num_colli, long fl_anno_reg, long fl_num_reg, long fl_prog_riga_ord_ven, string fs_cod_reparto, ref string fs_msg)
end prototypes

event ue_scroll_log();mle_log.scroll(mle_log.linecount())

mle_log.SetRedraw(TRUE)
end event

public subroutine wf_where (ref string fs_where);long			ll_ordine_inizio, ll_ordine_fine, ll_anno_ordine

datetime		ldt_data_inizio, ldt_data_fine,ldt_reg_data_inizio, ldt_reg_data_fine	

//da data reg a data reg ------------------------------------------------------------------------
ldt_reg_data_inizio = dw_selezione.getitemdatetime(1,"data_reg_inizio")
ldt_reg_data_fine   = dw_selezione.getitemdatetime(1,"data_reg_fine")
if isnull(ldt_reg_data_inizio) then ldt_reg_data_inizio = datetime(date("01/01/1900"),00:00:00)
if isnull(ldt_reg_data_fine) then ldt_reg_data_fine = datetime(date("31/12/2999"),00:00:00)

fs_where += "and data_registrazione >= '" +string(ldt_reg_data_inizio, s_cs_xx.db_funzioni.formato_data)+ "' "
fs_where += "and data_registrazione <= '" +string(ldt_reg_data_fine, s_cs_xx.db_funzioni.formato_data)+ "' "

//da data consegna a data consegna -----------------------------------------------------------------------------
ldt_data_inizio = dw_selezione.getitemdatetime(1,"d_data_da")
ldt_data_fine   = dw_selezione.getitemdatetime(1,"d_data_a")
if isnull(ldt_data_inizio) then ldt_data_inizio = datetime(date("01/01/1900"),00:00:00)
if isnull(ldt_data_fine) then ldt_data_fine = datetime(date("31/12/2999"),00:00:00)

fs_where += "and data_consegna >= '" +string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data)+ "' "
fs_where += "and data_consegna <= '" +string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data)+ "' "

//anno registrazione ------------------------------------------------------
ll_anno_ordine = dw_selezione.getitemnumber(1,"n_anno")
if ll_anno_ordine>0 then fs_where += " and anno_registrazione="+string(ll_anno_ordine)+" "

//dal numero al numero -----------------------------------------------------
ll_ordine_inizio = dw_selezione.getitemnumber(1,"n_ord_da")
ll_ordine_fine   = dw_selezione.getitemnumber(1,"n_ord_a")
if isnull(ll_ordine_inizio) or ll_ordine_inizio = 0 then ll_ordine_inizio = 1
if isnull(ll_ordine_fine) or ll_ordine_fine = 0 then ll_ordine_fine = 999999

fs_where += " and num_registrazione >= "+string(ll_ordine_inizio)+" "
fs_where += " and num_registrazione <= "+string(ll_ordine_fine)+" "

//flag blocco ------------------------------------------------------------------
fs_where += "and flag_blocco='N'"

end subroutine

public function integer wf_leggi ();string			ls_sql_A, ls_sql_BC, ls_where, ls_max_num_colli
long			ll_tot_A, ll_tot_BC, ll_max_num_colli

setpointer(Hourglass!)

dw_selezione.accepttext()

st_msg.text = ""

wf_where(ls_where)

ls_max_num_colli = sle_max_num_colli.text
if isnumber(ls_max_num_colli) then
	if ll_max_num_colli > 0 then
	else
		ll_max_num_colli = 200
	end if
else
	ll_max_num_colli = 200
end if

ls_sql_A = "select anno_registrazione,num_registrazione from tes_ord_ven "
ls_sql_A += "where cod_azienda='"+s_cs_xx.cod_azienda+"' "
ls_sql_A += " and num_colli > 0 "

//sistemazione anomalie colli sballati
ls_sql_A += " and num_colli < "+string(ll_max_num_colli) + " "

ls_sql_BC = ls_sql_A

ls_sql_A += " and cod_tipo_ord_ven in (      select cod_tipo_ord_ven "+&
													"from tab_tipi_ord_ven "+&
													"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
															"flag_tipo_bcl='A' ) "
															
ls_sql_BC += " and cod_tipo_ord_ven in (      select cod_tipo_ord_ven "+&
													"from tab_tipi_ord_ven "+&
													"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
															"(flag_tipo_bcl='B' or flag_tipo_bcl='C') )"

ls_sql_A += ls_where
ls_sql_BC+= ls_where

if not f_crea_datastore(ids_ordini_A, ls_sql_A) then
	return -1
end if
if not f_crea_datastore(ids_ordini_BC, ls_sql_BC) then
	return -1
end if

ll_tot_A = ids_ordini_A.retrieve()
ll_tot_BC = ids_ordini_BC.retrieve()

setpointer(Arrow!)

st_msg.text = "A: "+string(ll_tot_A)+" - B/C:"+string(ll_tot_BC)+" - Tot."+string(ll_tot_A + ll_tot_BC)

return 1
end function

public function integer wf_elabora_ordini_bc ();long ll_index, ll_tot, ll_anno_reg, ll_num_reg, ll_count, ll_barcode, ll_colli, ll_prog_riga_ord_ven
string ls_ordine, ls_msg, ls_cod_reparto
datastore lds_barcode_tes
long ll_tot_tes_ord_prod


setpointer(Hourglass!)

setnull(ll_prog_riga_ord_ven)

ll_tot = ids_ordini_BC.rowcount()

//cursore tes_ordini_produzione -----------------
//c'è un barcode per ogni reparto (non tutte le righe dell'ordine potrebbero appartenere allo stesso reparto)
declare cu_tes_ord_prod cursor for
		select progr_tes_produzione, cod_reparto
		from tes_ordini_produzione
		where cod_azienda=:s_cs_xx.cod_azienda and 
				anno_registrazione=:ll_anno_reg and num_registrazione=:ll_num_reg;
//------------------------------------------------------------------

for ll_index=1 to ll_tot
	ll_anno_reg = ids_ordini_BC.getitemnumber(ll_index, 1)
	ll_num_reg = ids_ordini_BC.getitemnumber(ll_index, 2)
	
	ls_ordine = string(ll_anno_reg)+"/"+string(ll_num_reg)
	
	st_msg2.text = "Elaboraz. in corso ordine tipo B/C "+ls_ordine + " (ordine "+string(ll_index)+" di "+string(ll_tot)+")"
	
	//vedo se per questo ordine c'è già qualcosa in tab_ord_ven_colli
	select count(*)
	into :ll_count
	from tab_ord_ven_colli
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_reg and num_registrazione=:ll_num_reg;
	
	if sqlca.sqlcode<0 then
		mle_log.text += "##########################################~r~n"
		mle_log.text += "Ordine "+ls_ordine + " saltatato a causa di un errore - "+sqlca.sqlerrtext + "~r~n~r~n"
		
		event trigger ue_scroll_log()
		continue
	end if
	
	if ll_count>0 then
		//hai trovato qualcosa, salta l'ordine
		mle_log.text += "##########################################~r~n"
		mle_log.text += "Barcode "+string(ll_barcode)+" saltato per l'ordine "+ls_ordine + " in quanto tes_ord_ven_colli contiene già qualcosa~r~n"
		event trigger ue_scroll_log()
		
		continue
	end if
	
	
	//apro il cursore della tes_ordini_produzione - - - - - - - - - - - - - - - - - - - - - - - - 
	open cu_tes_ord_prod;
	
	if sqlca.sqlcode < 0 then
		mle_log.text += "##########################################~r~n"
		mle_log.text += "Ordine "+ls_ordine + " saltato a causa di un errore in OPEN cursore cu_tes_ord_prod" + sqlca.sqlerrtext+"~r~n"
		event trigger ue_scroll_log()
		
		//vai all'altro ordine (ciclo for esterno)
		continue;
	end if
	
	//siccome non posso sapere, in caso di ordine tipo B/C con righe i cui prodotti non appartengono tutti allo stesso reparto
	//quanti colli imputare e a chi prendo solo il primo progr_tes_produzione e con quello creo i colli
	fetch cu_tes_ord_prod into :ll_barcode, :ls_cod_reparto;
	
	if sqlca.sqlcode < 0 then
		mle_log.text += "##########################################~r~n"
		mle_log.text += "Ordine "+ls_ordine + " saltato a causa di un errore nella fetch tes_ordini_produzione~r~n"+sqlca.sqlerrtext
		close cu_tes_ord_prod;
		
		continue	//passa all'altro ordine
	end if	
	
	if ll_barcode<0 or isnull(ll_barcode) or ls_cod_reparto="" or isnull(ls_cod_reparto) then
		//niente in tes_ordini_produzione
		mle_log.text += "##########################################~r~n"
		mle_log.text += "Ordine "+ls_ordine + " saltato in quanto tes_ordini_produzione è vuota per questo ordine~r~n"
		event trigger ue_scroll_log()
		close cu_tes_ord_prod;
		
		continue
	end if
	
	//dopo aver letto il primo fetch posso chiudere il cursore
	close cu_tes_ord_prod;
	
	
	//leggo il numero di colli da inserire (sicuramente è maggiore di zero) -----------------------------------------------------
	select num_colli
	into :ll_colli
	from tes_ord_ven
	where cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ll_anno_reg and num_registrazione=:ll_num_reg;
	
	if sqlca.sqlcode<0 then
		mle_log.text += "##########################################~r~n"
		mle_log.text += "Ordine "+ls_ordine + " saltato a causa di un errore in lettura colli in tes_ord_ven - "+sqlca.sqlerrtext + "~r~n~r~n"
		event trigger ue_scroll_log()
		
		continue
	end if
	
	setnull(ll_prog_riga_ord_ven)
	wf_inserisci_ds_colli(ll_barcode, ll_colli, ll_anno_reg, ll_num_reg, ll_prog_riga_ord_ven, ls_cod_reparto, ls_msg)
	mle_log.text += "Ordine "+ls_ordine+" OK  in datastore (colli:"+string(ll_colli)+")~r~n"
	event trigger ue_scroll_log()
	
next

setpointer(Arrow!)

return 1
end function

public function integer wf_elabora_ordini_a ();long ll_index, ll_tot, ll_anno_reg, ll_num_reg, ll_count, ll_barcode, ll_colli, ll_prog_riga_ord_ven, ll_tot_righe, ll_index_2, ll_max_num_colli
string ls_ordine, ls_sql_righe, ls_msg, ls_ordine_riga, ls_max_num_colli, ls_cod_reparto

setpointer(Hourglass!)

ls_max_num_colli = sle_max_num_colli.text

if isnumber(ls_max_num_colli) then
	ll_max_num_colli = long(ls_max_num_colli)
else
	ll_max_num_colli = 200
end if

ll_tot = ids_ordini_A.rowcount()

//cursore righe ordine --------------------------------------------------------
declare 	cu_righe cursor for
		select prog_riga_ord_ven from det_ord_ven
		where cod_azienda=:s_cs_xx.cod_azienda and 
				anno_registrazione=:ll_anno_reg and num_registrazione=:ll_num_reg and
				num_riga_appartenenza=0;
//-----------------------------------------------------------------------------------------

for ll_index=1 to ll_tot
	ll_anno_reg = ids_ordini_A.getitemnumber(ll_index, 1)
	ll_num_reg = ids_ordini_A.getitemnumber(ll_index, 2)
	
	ls_ordine = string(ll_anno_reg)+"/"+string(ll_num_reg)
	
	
	//ricava le righe dell'ordine con num_appartenenza = 0 ----------------------------------------------------------------------
	open cu_righe;
	if sqlca.sqlcode < 0 then
		mle_log.text += "##########################################~r~n"
		mle_log.text += "Ordine "+ls_ordine+" saltato per un errore in open cursore righe - "+sqlca.sqlerrtext + "~r~n~r~n"
		
		event trigger ue_scroll_log()
		
		continue
	end if
	
	//elabora le varie righe ------------------------------------------------------------------------------------------------------------------------
	do while true
		fetch cu_righe into :ll_prog_riga_ord_ven;
		
		
		if sqlca.sqlcode < 0 then
			close cu_righe;
			mle_log.text += "##########################################~r~n"
			mle_log.text += "Ordine "+ls_ordine+" - Riga "+string(ll_prog_riga_ord_ven)+" saltata per un errore in fetch righe - "+sqlca.sqlerrtext + "~r~n~r~n"
			event trigger ue_scroll_log()
			
			continue
		end if
		
		if sqlca.sqlcode = 100 then exit	//dal cursore delle righe
		
		ls_ordine_riga = ls_ordine + "/"+string(ll_prog_riga_ord_ven)
		st_msg2.text = "Elaboraz. in corso Riga ordine tipo A "+ls_ordine_riga +" (ordine "+string(ll_index)+" di "+string(ll_tot)+" - riga "+string(ll_prog_riga_ord_ven)+") "
		
		//recupero il barcode dalla det_ordini_produzione --------------------------------------------------------------------
		//prendo il primo reparto
		select top 1 progr_det_produzione, cod_reparto
		into :ll_barcode, :ls_cod_reparto
		from det_ordini_produzione
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_reg and num_registrazione=:ll_num_reg and prog_riga_ord_ven=:ll_prog_riga_ord_ven;
		
		if sqlca.sqlcode<0 then
			mle_log.text += "##########################################~r~n"
			mle_log.text += "Riga ordine "+ls_ordine_riga + " saltata per un errore lettura bnarcode det_ordini_produzione - "+sqlca.sqlerrtext + "~r~n~r~n"
			event trigger ue_scroll_log()
			
			continue
		end if
		
		if ll_barcode>0 then
		else
			mle_log.text += "##########################################~r~n"
			mle_log.text += "Riga Ordine "+ls_ordine_riga + " saltato perchè il barcode in det_ordini_produzione non è stato trovato~r~n"
			event trigger ue_scroll_log()
			
			continue
		end if
		
		//controllo se per questo questa riga c'è già qualcosa in tab_ord_ven_colli ---------------------------------------
		select count(*)
		into :ll_count
		from tab_ord_ven_colli
		where cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_reg and 
				num_registrazione=:ll_num_reg and 
				prog_riga_ord_ven=:ll_prog_riga_ord_ven;
//		select count(*)
//		into :ll_count
//		from tab_ord_ven_colli
//		where cod_azienda=:s_cs_xx.cod_azienda and
//				barcode=:ll_barcode;
		
		if sqlca.sqlcode<0 then
			mle_log.text += "##########################################~r~n"
			mle_log.text += "Riga ordine "+ls_ordine_riga + "saltata a causa di un errore in count tab_ord_ven_colli - "+sqlca.sqlerrtext + "~r~n~r~n"
			event trigger ue_scroll_log()
			
			continue
		end if
		
		if ll_count>0 then
			//hai trovato qualcosa, salta l'ordine
			mle_log.text += "##########################################~r~n"
			mle_log.text += "Riga Ordine "+ls_ordine_riga + " saltato perchè tes_ord_ven_colli contiene già qualcosa~r~n"
			event trigger ue_scroll_log()
			
			continue
		end if
		
		//leggo quanti colli devono essere inseriti per questa riga ordine -----------------------------------------------------------------------------
		//li associo tutti ad uno stesso barcode (reparto)
		select sum(num_colli)
		into :ll_colli
		from sessioni_lavoro
		where cod_azienda=:s_cs_xx.cod_azienda and
				progr_det_produzione in (select progr_det_produzione
												from det_ordini_produzione
												where cod_azienda=:s_cs_xx.cod_azienda and
														anno_registrazione=:ll_anno_reg and num_registrazione=:ll_num_reg and
														prog_riga_ord_ven=:ll_prog_riga_ord_ven);
		
		if sqlca.sqlcode<0 then
			mle_log.text += "##########################################~r~n"
			mle_log.text += "Riga ordine "+ls_ordine_riga + " saltata a causa di un errore il lettura colli in sessioni_lavoro - "+sqlca.sqlerrtext + "~r~n~r~n"
			event trigger ue_scroll_log()
			
			continue
		end if
		
		if ll_colli>0 then
			if ll_colli > ll_max_num_colli then
				mle_log.text += "Riga Ordine "+ls_ordine_riga + " saltata perchè sessioni_lavoro ha  un numero anomalo di colli prodotti (>"+string(ll_max_num_colli)+")~r~n"
				event trigger ue_scroll_log()
				
				continue
			end if
			
		else
			mle_log.text += "##########################################~r~n"
			mle_log.text += "Riga Ordine "+ls_ordine_riga + " saltata perchè sessioni_lavoro non ha colli prodotti~r~n"
			event trigger ue_scroll_log()
			
			continue
		end if
		
		//associo i colli tutti ad un unico reparto (del prodotto della riga ordine che sto elaborando)
		wf_inserisci_ds_colli(ll_barcode, ll_colli, ll_anno_reg, ll_num_reg, ll_prog_riga_ord_ven, ls_cod_reparto, ls_msg)
		mle_log.text += "Riga Ordine "+ls_ordine_riga+" OK  in datastore (colli:"+string(ll_colli)+" reparto "+ls_cod_reparto+" )~r~n"
		event trigger ue_scroll_log()
		
	loop
	
	close cu_righe;
	
next

setpointer(Arrow!)

return 1
end function

public function integer wf_inserisci_colli (ref string fs_msg);long ll_index, ll_tot, ll_anno_reg, ll_num_reg, ll_prog_riga_ord_ven, ll_progressivo, ll_barcode, ll_max
datetime ldt_data_creazione, ldt_ora_creazione
string ls_cod_reparto, ls_cod_operaio, ls_ordine, ls_barcode

ldt_data_creazione = datetime(today(), 00:00:00)
ldt_ora_creazione = datetime(date(1900, 1, 1), now())

setnull(ls_cod_operaio)
ls_cod_operaio = is_cod_operaio

ll_tot = ids_tab_ord_ven_colli.rowcount()
for ll_index=1 to ll_tot
	ll_barcode = ids_tab_ord_ven_colli.getitemnumber(ll_index, "barcode")
	ll_progressivo = ids_tab_ord_ven_colli.getitemnumber(ll_index, "progressivo")
	ll_anno_reg = ids_tab_ord_ven_colli.getitemnumber(ll_index, "anno_registrazione")
	ll_num_reg = ids_tab_ord_ven_colli.getitemnumber(ll_index, "num_registrazione")
	ll_prog_riga_ord_ven = ids_tab_ord_ven_colli.getitemnumber(ll_index, "prog_riga_ord_ven")
	ls_cod_reparto = ids_tab_ord_ven_colli.getitemstring(ll_index, "cod_reparto")
	
	//preparo il messaggio eventuale di errore
	ls_ordine = "Errore in inserimento colli ordine "+string(ll_anno_reg)+"/"+string(ll_num_reg)
	if isnull(ll_prog_riga_ord_ven) then
		ls_barcode = " - barcode (D) "+string(ll_barcode)
		ls_ordine += ls_barcode
	else
		ls_barcode = " - barcode (T) "+string(ll_barcode)
		ls_ordine += ls_barcode
	end if
	
//	//select max del progressivo
//	select max(progressivo)
//	into :ll_max
//	from tab_ord_ven_colli
//	where cod_azienda=:s_cs_xx.cod_azienda and
//			barcode=:ll_barcode;
//	
//	if sqlca.sqlcode<0 then
//		fs_msg ="Errore in max progressivo da tab_ord_ven_colli, barcode "+string(ll_barcode)+" "+sqlca.sqlerrtext
//		return -1
//	end if
//	
//	if isnull(ll_max) then ll_max = 0
//	ll_max += 1
	
	//procedi con l'inserimento
	insert into tab_ord_ven_colli
	(		cod_azienda,
			barcode,
			progressivo,
			anno_registrazione,
			num_registrazione,
			prog_riga_ord_ven,
			data_creazione,
			ora_creazione,
			cod_operaio,
			cod_reparto)
	values	(	:s_cs_xx.cod_azienda,
				:ll_barcode,
				:ll_progressivo,
				:ll_anno_reg,
				:ll_num_reg,
				:ll_prog_riga_ord_ven,
				:ldt_data_creazione,
				:ldt_ora_creazione,
				:ls_cod_operaio,
				:ls_cod_reparto);
					
	if sqlca.sqlcode < 0 then
		fs_msg = ls_ordine + " - insert into tes_ord_ven_colli - "+sqlca.sqlerrtext
		return -1
	end if
	
	mle_log.text += "Ordine "+string(ll_anno_reg)+"/"+string(ll_num_reg)+ls_barcode+&	
									" OK  in tabella colli (collo: "+string(ll_progressivo)+"reparto: "+ls_cod_reparto+")~r~n"
	event trigger ue_scroll_log()
	
	st_msg2.text = "Inserimento in tes_ord_ven_colli "+string(ll_index)+" di "+string(ll_tot)
	
next

return 1
end function

public function integer wf_inserisci_ds_colli (long fl_barcode, long fl_num_colli, long fl_anno_reg, long fl_num_reg, long fl_prog_riga_ord_ven, string fs_cod_reparto, ref string fs_msg);long ll_barcode_tes, ll_barcode_det, ll_counter, ll_new

for ll_counter = 1 to fl_num_colli
	
	ll_new = ids_tab_ord_ven_colli.insertrow(0)
	
	ids_tab_ord_ven_colli.setitem(ll_new, "barcode", fl_barcode)
	ids_tab_ord_ven_colli.setitem(ll_new, "progressivo", ll_counter)
	ids_tab_ord_ven_colli.setitem(ll_new, "anno_registrazione", fl_anno_reg)
	ids_tab_ord_ven_colli.setitem(ll_new, "num_registrazione", fl_num_reg)
	ids_tab_ord_ven_colli.setitem(ll_new, "prog_riga_ord_ven", fl_prog_riga_ord_ven)
	ids_tab_ord_ven_colli.setitem(ll_new, "cod_reparto", fs_cod_reparto)
	
next

return 1
end function

event pc_setwindow;call super::pc_setwindow;string ls_sql

set_w_options(c_closenosave + c_autoposition + c_noresizewin)
								 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
save_on_close(c_socnosave)

dw_selezione.resetupdate()

ls_sql = "select barcode, progressivo, anno_registrazione,num_registrazione, prog_riga_ord_ven, cod_reparto "
ls_sql += "from tab_ord_ven_colli where cod_azienda='CODICEASSURDO'"

f_crea_datastore(ids_tab_ord_ven_colli, ls_sql)
end event

on w_allineamento_barcode_colli.create
int iCurrent
call super::create
this.cb_4=create cb_4
this.sle_max_num_colli=create sle_max_num_colli
this.st_1=create st_1
this.cb_3=create cb_3
this.mle_log=create mle_log
this.st_msg2=create st_msg2
this.cb_2=create cb_2
this.st_msg=create st_msg
this.cb_1=create cb_1
this.dw_selezione=create dw_selezione
this.cb_reset=create cb_reset
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_4
this.Control[iCurrent+2]=this.sle_max_num_colli
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.cb_3
this.Control[iCurrent+5]=this.mle_log
this.Control[iCurrent+6]=this.st_msg2
this.Control[iCurrent+7]=this.cb_2
this.Control[iCurrent+8]=this.st_msg
this.Control[iCurrent+9]=this.cb_1
this.Control[iCurrent+10]=this.dw_selezione
this.Control[iCurrent+11]=this.cb_reset
end on

on w_allineamento_barcode_colli.destroy
call super::destroy
destroy(this.cb_4)
destroy(this.sle_max_num_colli)
destroy(this.st_1)
destroy(this.cb_3)
destroy(this.mle_log)
destroy(this.st_msg2)
destroy(this.cb_2)
destroy(this.st_msg)
destroy(this.cb_1)
destroy(this.dw_selezione)
destroy(this.cb_reset)
end on

event closequery;call super::closequery;destroy ids_ordini_A;
destroy ids_ordini_BC;
destroy ids_tab_ord_ven_colli;
end event

type cb_4 from commandbutton within w_allineamento_barcode_colli
integer x = 55
integer y = 720
integer width = 448
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "3. Crea Colli"
end type

event clicked;string ls_msg

if g_mb.confirm("Procedo con la crezione dei colli?") then
else
	return
end if

if wf_inserisci_colli(ls_msg) < 0 then
	mle_log.text += "~r~n~r~nERRORE~r~n~r~n"+ls_msg
	rollback;
	return
else
	commit;
end if

st_msg2.text = "Fine Elaborazione!"
mle_log.text += "~r~n~r~nFine elaborazione~r~n~r~n"
end event

type sle_max_num_colli from singlelineedit within w_allineamento_barcode_colli
integer x = 1467
integer y = 248
integer width = 402
integer height = 104
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "200"
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_allineamento_barcode_colli
integer x = 503
integer y = 264
integer width = 946
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Num Max Colli (oltre è anomalia:)"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_3 from commandbutton within w_allineamento_barcode_colli
integer x = 2999
integer y = 440
integer width = 320
integer height = 92
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "elimina"
end type

event clicked;if g_mb.confirm("Sei sicuro di vole cancellare dalla tab_ord_ven_colli dove cod_operaio è '1'?") then
else
	g_mb.show("Operazione annullata!")
	return
end if

delete from tab_ord_ven_colli
where cod_operaio='1';

if sqlca.sqlcode<0 then
	g_mb.error("Errore in cancellazione tab_ord_ven_colli where cod_operaio='1'")
	rollback;
else
	commit;
	g_mb.show("Operazione cancellazione effetuata con successo!")
end if
end event

type mle_log from multilineedit within w_allineamento_barcode_colli
integer x = 59
integer y = 892
integer width = 3250
integer height = 1364
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean border = false
boolean vscrollbar = true
end type

type st_msg2 from statictext within w_allineamento_barcode_colli
integer x = 617
integer y = 616
integer width = 2715
integer height = 196
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_allineamento_barcode_colli
integer x = 55
integer y = 388
integer width = 448
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "1. leggi"
end type

event clicked;mle_log.text = ""
ids_tab_ord_ven_colli.reset()

wf_leggi()
end event

type st_msg from statictext within w_allineamento_barcode_colli
integer x = 576
integer y = 384
integer width = 2373
integer height = 100
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_allineamento_barcode_colli
integer x = 55
integer y = 548
integer width = 448
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "2. Prepara Colli"
end type

event clicked;string ls_msg

mle_log.text = ""
ids_tab_ord_ven_colli.reset()

//elabora gli ordini B/C
wf_elabora_ordini_bc()

//elabora gli ordini A
wf_elabora_ordini_a()

st_msg2.text = "Fine Preparazione!"
mle_log.text += "~r~n~r~nFine Preparazione~r~n~r~n"
end event

type dw_selezione from uo_cs_xx_dw within w_allineamento_barcode_colli
integer x = 18
integer y = 20
integer width = 3314
integer height = 380
integer taborder = 20
string dataobject = "d_sel_allineamento_barcode_colli"
boolean border = false
end type

event pcd_new;call super::pcd_new;//setitem(getrow(),"n_anno",f_anno_esercizio())
end event

type cb_reset from commandbutton within w_allineamento_barcode_colli
integer x = 78
integer y = 256
integer width = 338
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Reset"
end type

event clicked;string ls_null
date ld_null
long ll_null

setnull(ld_null)
setnull(ll_null)
setnull(ls_null)

dw_selezione.setitem(dw_selezione.getrow(), "data_reg_inizio", ld_null)
dw_selezione.setitem(dw_selezione.getrow(), "data_reg_fine", ld_null)
dw_selezione.setitem(dw_selezione.getrow(), "d_data_da", ld_null)
dw_selezione.setitem(dw_selezione.getrow(), "d_data_a", ld_null)

dw_selezione.setitem(dw_selezione.getrow(), "n_ord_da", 1)
dw_selezione.setitem(dw_selezione.getrow(), "n_ord_a", 999999)
dw_selezione.setitem(dw_selezione.getrow(),"n_anno",f_anno_esercizio())
end event

