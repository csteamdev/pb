﻿$PBExportHeader$w_analisi_pronto_ordini.srw
forward
global type w_analisi_pronto_ordini from w_cs_xx_principale
end type
type dw_certificazione_prod from uo_std_dw within w_analisi_pronto_ordini
end type
type dw_dichiarazione_prestazione from uo_std_dw within w_analisi_pronto_ordini
end type
type dw_clienti from uo_dddw_checkbox within w_analisi_pronto_ordini
end type
type tab_1 from tab within w_analisi_pronto_ordini
end type
type tabpage_selezione from userobject within tab_1
end type
type dw_selezione from uo_std_dw within tabpage_selezione
end type
type dw_pronto from uo_std_dw within tabpage_selezione
end type
type tabpage_selezione from userobject within tab_1
dw_selezione dw_selezione
dw_pronto dw_pronto
end type
type tabpage_invio from userobject within tab_1
end type
type dw_excel from uo_std_dw within tabpage_invio
end type
type tabpage_invio from userobject within tab_1
dw_excel dw_excel
end type
type tab_1 from tab within w_analisi_pronto_ordini
tabpage_selezione tabpage_selezione
tabpage_invio tabpage_invio
end type
type dw_prodotti from uo_dddw_checkbox within w_analisi_pronto_ordini
end type
end forward

global type w_analisi_pronto_ordini from w_cs_xx_principale
integer width = 4960
integer height = 2672
string title = "Spedizione Prodotti Finiti"
dw_certificazione_prod dw_certificazione_prod
dw_dichiarazione_prestazione dw_dichiarazione_prestazione
dw_clienti dw_clienti
tab_1 tab_1
dw_prodotti dw_prodotti
end type
global w_analisi_pronto_ordini w_analisi_pronto_ordini

type variables
boolean ib_alusistemi
string ib_where_origine="", is_sql=""
end variables

forward prototypes
public function integer wf_spedisci (ref string as_message)
public function integer wf_genera_excel (string as_file_path, ref string as_errore)
end prototypes

public function integer wf_spedisci (ref string as_message);string			ls_nota_piede, ls_num_matricola[], ls_errore, ls_merged_pdf, ls_dop_flag_stampa, ls_cod_prodotto, ls_flag_certificato_dop, ls_vuoto[]
long 			ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_ret, ll_y
datastore	lds_data
uo_generazione_documenti luo_gen

luo_gen = CREATE uo_generazione_documenti

for ll_i = 1 to tab_1.tabpage_selezione.dw_pronto.rowcount()
	if 	tab_1.tabpage_selezione.dw_pronto.getitemstring(ll_i,13) = "N" then continue
	
	ll_anno_registrazione = tab_1.tabpage_selezione.dw_pronto.getitemnumber(ll_i, 1)
	ll_num_registrazione =  tab_1.tabpage_selezione.dw_pronto.getitemnumber(ll_i, 2)
	ll_prog_riga_ord_ven = tab_1.tabpage_selezione.dw_pronto.getitemnumber(ll_i, 3)
	
	update det_ord_ven
	set 	quan_evasa = quan_ordine,
			flag_evasione ='E'
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode = -1 then
		as_message = g_str.format("Errore in fase di evasione riga ordine $1-$2-$3.~r~n$4", ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,sqlca.sqlerrtext )
		rollback;
		destroy luo_gen
		return -1
	end if
	
	luo_gen.uof_calcola_stato_ordine(ll_anno_registrazione, ll_num_registrazione)
	
	ls_nota_piede = g_str.format("Ultima spedizione in data:$1 ora:$2", string(today(),"dd/mm/yyyy"), string(now(),"hh:mm"))
	
	update tes_ord_ven
	set nota_piede = :ls_nota_piede
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
	if sqlca.sqlcode = -1 then
		as_message = g_str.format("Errore in fase di memo nota piede $1-$2~r~n$4", ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,sqlca.sqlerrtext )
		rollback;
		destroy luo_gen
		return -1
	end if
	
	// 27/8/2020 generazione dei certificati come richiesto da Paride
	
	// per prima cosa verifico che sia necessario il certificato
	select D.cod_prodotto, P.dop_flag_stampa, T.flag_certificato_dop
	into	:ls_cod_prodotto, :ls_dop_flag_stampa, :ls_flag_certificato_dop
	from	det_ord_ven D
			left outer join tab_flags_configuratore P on D.cod_azienda=P.cod_azienda and D.cod_prodotto = P.cod_modello
			left outer join tes_ord_ven T on D.cod_azienda=T.cod_azienda and D.anno_registrazione=T.anno_registrazione and D.num_registrazione=T.num_registrazione
	where D.cod_azienda = :s_cs_xx.cod_azienda and
			D.anno_registrazione = :ll_anno_registrazione and
			D.num_registrazione = :ll_num_registrazione and
			D.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
	if sqlca.sqlcode < 0 then
		as_message = g_str.format("Errore in fase di ricerca prodotto in riga ordine $1-$2~r~n$4", ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,sqlca.sqlerrtext )
		rollback;
		destroy luo_gen
		return -1
	end if
	
	// nel caso in cui non sia richiesto un certificato dalla testata ordine, salto.
	if isnull(ls_flag_certificato_dop) or ls_flag_certificato_dop = "N" then continue
	
	// nel caso in cui non sia richiesto un certificato dal modello di prodotto, salto.
	if isnull(ls_dop_flag_stampa) or ls_dop_flag_stampa = "N" then continue
	
	// devo per forza fare il commit riga per riga, altrimenti mi da errore quando va ad archiviare il blob
	commit using sqlca;
	
	// recupero quindi le matricole dei prodotti finiti		
	ll_ret = guo_functions.uof_crea_datastore( lds_data, g_str.format("select cod_ologramma from	det_ord_ven_ologramma where cod_azienda = '$1' and anno_registrazione = $2 and num_registrazione = $3 and prog_riga_ord_ven = $4 ",s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven), ls_errore)
	if ll_ret = 0 then
		as_message =g_str.format("Matricola non trovata per l'ordine $1-$2-$3", ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_ord_ven)
		destroy luo_gen
		return -1
	end if
	if ll_ret < 0 then
		as_message = g_str.format("Errore in ricerca matricole per l'ordine $1-$2-$3", ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_ord_ven)
		destroy luo_gen
		return -1
	end if
	
	ls_num_matricola[] = ls_vuoto[]

	for ll_y = 1 to ll_ret
		 ls_num_matricola[ll_y] = lds_data.getitemstring(ll_y,1)
	next
	
	destroy 	lds_data	
		
	// genero i certificati	
	
	dw_certificazione_prod.ib_dw_report=true
	dw_dichiarazione_prestazione.ib_dw_report=true
	
	uo_certificati_dop luo_certificati_dop
	luo_certificati_dop = create uo_certificati_dop
	
	if luo_certificati_dop.uof_genera_certificato(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_num_matricola[] , ref dw_certificazione_prod, ref dw_dichiarazione_prestazione, ref as_message) < 0 then return -1
	
	// genero ed unisco i PDF
	if luo_certificati_dop.uof_merge_pdf( dw_certificazione_prod, dw_dichiarazione_prestazione, true, true, ref ls_merged_pdf) < 0 then
		as_message = "Errore durante l'unione dei PDF dei singoli certificati"
		return -1
	end if
	
	// invio certificati tramite mail ed archivio in det_ord_ven_note
	if luo_certificati_dop.uof_invia_certificati(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_merged_pdf, ref as_message) < 0 then return -1
	
	destroy luo_certificati_dop
		
next

commit;
destroy luo_gen

return 0
end function

public function integer wf_genera_excel (string as_file_path, ref string as_errore);string		ls_cod_tessuto, ls_colore_tessuto, ls_cod_reparto,ls_flag_tipo_unione, ls_des_tasca, &
			ls_flag_orientamento_unione, ls_cod_tipo_tasca, ls_flag_mantovana, ls_cod_tessuto_mant, ls_cod_colore_mant, ls_cod_modello, &
			ls_flag_tipo_mantovana, ls_passamaneria, ls_str, ls_indirizzo,  ls_localita, ls_cap, flag_tipo_mantovana_dest, ls_cod_fornitore, ls_rag_soc_1, &
			ls_cod_comodo, ls_cod_cliente_old
			
long		ll_i,  ll_progr_det_produzione, ll_anno_registrazione, ll_num_registrazione ,ll_prog_riga_ord_ven, ll_riga, ll_colli
			
dec{4} 	ld_dim_x, ld_dim_y, ld_quan_ordine

datetime	ldt_date

uo_excel luo_excel


luo_excel = create uo_excel
luo_excel.uof_create( as_file_path, true)



// dati generali di testata
ll_riga = 1
luo_excel.uof_set_bold( ll_riga, "A") // ragione sociale
luo_excel.uof_set_bold( ll_riga, "B") // q.ta
luo_excel.uof_set_bold( ll_riga, "C") // prodotto
luo_excel.uof_set_bold( ll_riga, "D") // riferimento
luo_excel.uof_set_bold( ll_riga, "E")  // ordine
luo_excel.uof_set_bold( ll_riga, "F") // vernic
luo_excel.uof_set_bold( ll_riga, "G") // colore
luo_excel.uof_set_bold( ll_riga, "H") // largh
luo_excel.uof_set_bold( ll_riga, "I") //sp/h
luo_excel.uof_set_bold( ll_riga, "J") // colli

luo_excel.uof_set( ll_riga, "A", "RAGIONE SOCIALE")
luo_excel.uof_set( ll_riga, "B", "Q.TA'")
luo_excel.uof_set( ll_riga, "C", "PRODOTTO")
luo_excel.uof_set( ll_riga, "D", "RIF")
luo_excel.uof_set( ll_riga, "E", "ORDINE")
luo_excel.uof_set( ll_riga, "F", "VERNIC")
luo_excel.uof_set( ll_riga, "G", "COLORE")
luo_excel.uof_set( ll_riga, "H", "LARGH")
luo_excel.uof_set( ll_riga, "I", "SP./H")
luo_excel.uof_set( ll_riga, "J", "COLLI")

luo_excel.uof_set_border( ll_riga, 1, ll_riga, 10, 2, 2, 2, 2)

// Eseguo ordinamento della DW dei dati per CLIENTE
tab_1.tabpage_selezione.dw_pronto.setsort("#6")
tab_1.tabpage_selezione.dw_pronto.sort()

// dati di riga
for ll_i = 1 to tab_1.tabpage_selezione.dw_pronto.rowcount()
	if 	tab_1.tabpage_selezione.dw_pronto.getitemstring(ll_i,13) = "N" then continue
	ll_riga ++
	if ll_i = 2 then ls_cod_cliente_old = tab_1.tabpage_selezione.dw_pronto.getitemstring(ll_i,4)
	
	if ll_riga > 2 then
		if tab_1.tabpage_selezione.dw_pronto.getitemstring(ll_i,4) <>  ls_cod_cliente_old then
			// fascia nera
			luo_excel.uof_merge( ll_riga, 1, ll_riga, "J")
			luo_excel.uof_set_background( ll_riga, 1, 1)
			ll_riga ++
			ls_cod_cliente_old = tab_1.tabpage_selezione.dw_pronto.getitemstring(ll_i,4)
		end if
	end if
	
	luo_excel.uof_set_format(ll_riga, "A", "@")
	luo_excel.uof_set_format(ll_riga, "C", "@")
	luo_excel.uof_set_format(ll_riga, "D", "@")
	luo_excel.uof_set_format(ll_riga, "E", "@")
	luo_excel.uof_set_format(ll_riga, "F", "@")
	luo_excel.uof_set_format(ll_riga, "G", "@")

	luo_excel.uof_set( ll_riga, "A", tab_1.tabpage_selezione.dw_pronto.getitemstring(ll_i,6) )
	luo_excel.uof_set( ll_riga, "B", tab_1.tabpage_selezione.dw_pronto.getitemnumber(ll_i,14))
	luo_excel.uof_set( ll_riga, "C", tab_1.tabpage_selezione.dw_pronto.getitemstring(ll_i,47))
	luo_excel.uof_set( ll_riga, "D", tab_1.tabpage_selezione.dw_pronto.getitemstring(ll_i,45))
	luo_excel.uof_set( ll_riga, "E", tab_1.tabpage_selezione.dw_pronto.getitemnumber(ll_i,2)  )
	luo_excel.uof_set( ll_riga, "F", tab_1.tabpage_selezione.dw_pronto.getitemstring(ll_i,43))
	luo_excel.uof_set( ll_riga, "G",  tab_1.tabpage_selezione.dw_pronto.getitemstring(ll_i,10))
	luo_excel.uof_set( ll_riga, "H", tab_1.tabpage_selezione.dw_pronto.getitemnumber(ll_i,11))
	luo_excel.uof_set( ll_riga, "I", tab_1.tabpage_selezione.dw_pronto.getitemnumber(ll_i,12))
	luo_excel.uof_set( ll_riga, "J", tab_1.tabpage_selezione.dw_pronto.getitemnumber(ll_i,40))
	
	luo_excel.uof_set_bold( ll_riga, "A")
	luo_excel.uof_set_border( ll_riga, 1, ll_riga, 10, 2,2,2,2)
	
	luo_excel.uof_set_format( ll_riga, 7, "@")

	luo_excel.uof_set_bold( ll_riga, 3)
	luo_excel.uof_set_font_color(ll_riga,1,3)
next

luo_excel.uof_set_rows_height(1, ll_riga, 25)
luo_excel.uof_set_font_size( 1, 1,ll_riga, 10,12)
luo_excel.uof_set_horizontal_align( 2, 2, ll_riga, 10, -4108)
luo_excel.uof_set_vertical_align( 2, 1, ll_riga, 10, -4108)
luo_excel.uof_set_autofit( 1, 1,ll_riga, 10)


destroy luo_excel

return 0
end function

on w_analisi_pronto_ordini.create
int iCurrent
call super::create
this.dw_certificazione_prod=create dw_certificazione_prod
this.dw_dichiarazione_prestazione=create dw_dichiarazione_prestazione
this.dw_clienti=create dw_clienti
this.tab_1=create tab_1
this.dw_prodotti=create dw_prodotti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_certificazione_prod
this.Control[iCurrent+2]=this.dw_dichiarazione_prestazione
this.Control[iCurrent+3]=this.dw_clienti
this.Control[iCurrent+4]=this.tab_1
this.Control[iCurrent+5]=this.dw_prodotti
end on

on w_analisi_pronto_ordini.destroy
call super::destroy
destroy(this.dw_certificazione_prod)
destroy(this.dw_dichiarazione_prestazione)
destroy(this.dw_clienti)
destroy(this.tab_1)
destroy(this.dw_prodotti)
end on

event pc_setwindow;call super::pc_setwindow;string ls_sql

guo_functions.uof_get_parametro_azienda("ALU", ib_alusistemi)

set_w_options(c_noenablepopup + c_closenosave)

tab_1.tabpage_selezione.dw_pronto.ib_colora=false
tab_1.tabpage_invio.dw_excel.insertrow(0)


tab_1.tabpage_selezione.dw_selezione.reset()
tab_1.tabpage_selezione.dw_selezione.insertrow(0)

tab_1.tabpage_selezione.dw_selezione.setitem(1, 1, relativedate(today(),-30) )
tab_1.tabpage_selezione.dw_selezione.setitem(1, 2, today() )
tab_1.tabpage_selezione.dw_selezione.setitem(1, 5, relativedate(today(),-30) )
tab_1.tabpage_selezione.dw_selezione.setitem(1, 6, today() )

// Multiselect prodotto
ls_sql = 	"select cod_prodotto, des_prodotto from anag_prodotti where cod_prodotto in (select cod_modello from tab_flags_configuratore) and flag_blocco = 'N' order by 1"
dw_prodotti.uof_set_column_by_sql(ls_sql)
dw_prodotti.uof_set_parent_dw(tab_1.tabpage_selezione.dw_selezione, "cod_prodotto", "cod_prodotto")
dw_prodotti.uof_set_column_width(1, 400)
dw_prodotti.uof_set_column_width(2, 1200)
dw_prodotti.uof_set_column_name(1, "Cod.")
dw_prodotti.uof_set_column_name(2, "Des. Prodotto")
dw_prodotti.uof_set_column_align(1, "center")

// Multiselect Clienti
ls_sql = 	"select cod_cliente, rag_soc_1, localita from anag_clienti order by 2"
dw_clienti.uof_set_column_by_sql(ls_sql)
dw_clienti.uof_set_parent_dw(tab_1.tabpage_selezione.dw_selezione, "cod_cliente", "cod_cliente")
dw_clienti.uof_set_column_width(1, 400)
dw_clienti.uof_set_column_width(2, 1200)
dw_clienti.uof_set_column_name(1, "Cod.")
dw_clienti.uof_set_column_name(2, "Ragione Sociale")
dw_clienti.uof_set_column_align(1, "center")
dw_clienti.setsort("#3")
dw_clienti.sort()


is_sql = tab_1.tabpage_selezione.dw_pronto.getsqlselect()

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(tab_1.tabpage_selezione.dw_selezione, &
                 "cod_giro_consegna", &
                 sqlca, &
                 "tes_giri_consegne", &
                 "cod_giro_consegna", &
                 "des_giro_consegna", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event resize;/** TOLTO ANCESTOR **/

tab_1.resize(newwidth - 30, newheight - 30)


tab_1.event ue_resize()

end event

type dw_certificazione_prod from uo_std_dw within w_analisi_pronto_ordini
boolean visible = false
integer x = 5413
integer y = 260
integer width = 2309
integer height = 2520
integer taborder = 30
string dataobject = "d_report_certificazione_produttore"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type dw_dichiarazione_prestazione from uo_std_dw within w_analisi_pronto_ordini
boolean visible = false
integer x = 5234
integer y = 460
integer width = 2309
integer height = 2520
integer taborder = 40
string dataobject = "d_report_dichiarazione_prestazione"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type dw_clienti from uo_dddw_checkbox within w_analisi_pronto_ordini
integer x = 709
integer y = 440
integer width = 1897
integer height = 1140
integer taborder = 40
boolean bringtotop = true
end type

type tab_1 from tab within w_analisi_pronto_ordini
event ue_resize ( )
integer width = 4891
integer height = 2560
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_selezione tabpage_selezione
tabpage_invio tabpage_invio
end type

event ue_resize();// ridimensiono DW di ricerca
tab_1.tabpage_selezione.dw_selezione.width = tab_1.width
tab_1.tabpage_selezione.dw_pronto.resize(tab_1.width -30, tab_1.height - tab_1.tabpage_selezione.dw_selezione.height -180 )

end event

on tab_1.create
this.tabpage_selezione=create tabpage_selezione
this.tabpage_invio=create tabpage_invio
this.Control[]={this.tabpage_selezione,&
this.tabpage_invio}
end on

on tab_1.destroy
destroy(this.tabpage_selezione)
destroy(this.tabpage_invio)
end on

type tabpage_selezione from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4855
integer height = 2432
long backcolor = 12632256
string text = "SELEZIONE"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_selezione dw_selezione
dw_pronto dw_pronto
end type

on tabpage_selezione.create
this.dw_selezione=create dw_selezione
this.dw_pronto=create dw_pronto
this.Control[]={this.dw_selezione,&
this.dw_pronto}
end on

on tabpage_selezione.destroy
destroy(this.dw_selezione)
destroy(this.dw_pronto)
end on

type dw_selezione from uo_std_dw within tabpage_selezione
integer x = 5
integer y = 8
integer width = 4663
integer height = 420
integer taborder = 20
string dataobject = "d_analisi_pronto_ordini_selezione"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_search"
		accepttext()
		tab_1.tabpage_selezione.dw_pronto.setredraw( false )
		tab_1.tabpage_selezione.dw_pronto.event ue_imposta_sql()
		tab_1.tabpage_selezione.dw_pronto.setredraw( true )
		
	case "b_cliente_ricerca"
		guo_ricerca.uof_ricerca_cliente(tab_1.tabpage_selezione.dw_selezione,"cod_cliente")
	end choose
end event

event clicked;call super::clicked;choose case dwo.name
	case "cod_prodotto"
		dw_prodotti.show()
	case "cod_cliente"
		dw_clienti.show()
end choose
end event

type dw_pronto from uo_std_dw within tabpage_selezione
event ue_imposta_sql ( )
event ue_retrieve ( )
event ue_stato_produzione_riga ( )
integer x = 5
integer y = 428
integer width = 4663
integer height = 1980
integer taborder = 20
string dataobject = "d_analisi_pronto_ordini"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

event ue_imposta_sql();string ls_sql, ls_sql1, ls_sql2, ls_sql_add, ls_cod_cliente, ls_cod_giro, ls_cod_prodotto, ls_flag_stato_produzione, ls_prodotti[], ls_str, ls_clienti[]
long	ll_pos, ll_anno_ordine, ll_num_ordine, ll_ret, ll_i
date ld_data_reg_inizio, ld_data_reg_fine, ld_data_cons_inizio, ld_data_cons_fine

ls_sql = getsqlselect( )
ll_pos = pos( lower(ls_sql), "where" )

ls_sql1 = left( ls_sql, ll_pos - 1)
ls_sql2 = mid( ls_sql, ll_pos + 5 )

if len(ib_where_origine) = 0 then ib_where_origine = ls_sql2

ld_data_reg_inizio = tab_1.tabpage_selezione.dw_selezione.getitemdate(1,"data_inizio")
ld_data_reg_fine = tab_1.tabpage_selezione.dw_selezione.getitemdate(1,"data_fine")
ld_data_cons_inizio = tab_1.tabpage_selezione.dw_selezione.getitemdate(1,"data_consegna_inizio")
ld_data_cons_fine = tab_1.tabpage_selezione.dw_selezione.getitemdate(1,"data_consegna_fine")
ll_anno_ordine = tab_1.tabpage_selezione.dw_selezione.getitemnumber(1,"anno_registrazione")
ll_num_ordine = tab_1.tabpage_selezione.dw_selezione.getitemnumber(1,"num_registrazione")
ls_cod_giro = tab_1.tabpage_selezione.dw_selezione.getitemstring(1,"cod_giro_consegna")
ls_cod_prodotto = tab_1.tabpage_selezione.dw_selezione.getitemstring(1,"cod_prodotto")
ls_cod_cliente = tab_1.tabpage_selezione.dw_selezione.getitemstring(1,"cod_cliente")
ls_flag_stato_produzione = tab_1.tabpage_selezione.dw_selezione.getitemstring(1,"flag_stato_produzione")
g_str.explode( tab_1.tabpage_selezione.dw_selezione.getitemstring(1,"cod_prodotto") , ",", ls_prodotti)
g_str.explode( tab_1.tabpage_selezione.dw_selezione.getitemstring(1,"cod_cliente") , ",", ls_clienti)

ls_sql_add = " where TES.cod_azienda='" + s_cs_xx.cod_azienda + " ' "

if not isnull(ld_data_reg_inizio) and ld_data_reg_inizio > date("01/01/1900") then
	ls_sql_add += " and TES.data_registrazione >= '" + string(ld_data_reg_inizio, s_cs_xx.db_funzioni.formato_data   ) + "' "
end if
if not isnull(ld_data_reg_fine) and ld_data_reg_fine > date("01/01/1900") then
	ls_sql_add += " and TES.data_registrazione <= '" + string(ld_data_reg_fine, s_cs_xx.db_funzioni.formato_data   )+ "' "
end if

if not isnull(ld_data_cons_inizio) and ld_data_cons_inizio > date("01/01/1900") then
	ls_sql_add += " and TES.data_consegna >= '" + string(ld_data_cons_inizio, s_cs_xx.db_funzioni.formato_data   )+ "' "
end if
if not isnull(ld_data_cons_fine) and ld_data_cons_fine > date("01/01/1900") then
	ls_sql_add += " and TES.data_consegna <= '" + string(ld_data_cons_fine, s_cs_xx.db_funzioni.formato_data   )+ "' "
end if

if not isnull(ll_anno_ordine) and ll_anno_ordine > 0 then
	ls_sql_add += " and TES.anno_registrazione = " + string(ll_anno_ordine )
end if
if not isnull(ll_num_ordine) and ll_num_ordine > 0 then
	ls_sql_add += " and TES.num_registrazione = " + string(ll_num_ordine )
end if

if not isnull(ls_cod_giro) then
	ls_sql_add += " and  TES.cod_giro_consegna = '" + ls_cod_giro + "' " 
end if

if upperbound(ls_prodotti) > 0  then
	if ls_prodotti[1] <> "TUTTI" and ls_prodotti[1] <> "" then
		ls_str = ""
		for ll_i = 1 to upperbound(ls_prodotti)
			if len(ls_str) > 0 then ls_str += ","
			ls_str += g_str.format("'$1'",trim(ls_prodotti[ll_i]))
		next
		ls_sql_add += g_str.format(" and DET.cod_prodotto in ($1) ", ls_str)
	end if
end if

if upperbound(ls_clienti) > 0  then
	if ls_clienti[1] <> "TUTTI"  and ls_clienti[1] <> "" then
		ls_str = ""
		for ll_i = 1 to upperbound(ls_clienti)
			if len(ls_str) > 0 then ls_str += ","
			ls_str += g_str.format("'$1'",trim(ls_clienti[ll_i]))
		next
		ls_sql_add += g_str.format(" and TES.cod_cliente in ($1) ", ls_str)
	end if
end if

choose case ls_flag_stato_produzione
	case "1" // pronti
			// in questo caso estraggo solo i record che hanno la produzione pronta
		ls_sql_add += " and &
							NOT EXISTS (select distinct anno_registrazione, num_registrazione, prog_riga_ord_ven from det_ordini_produzione where  &
							TES.anno_registrazione = det_ordini_produzione.anno_registrazione and &
							TES.num_registrazione  =det_ordini_produzione.num_registrazione and &
							DET.prog_riga_ord_ven  =det_ordini_produzione.prog_riga_ord_ven  &
							and flag_fine_fase ='N') "
		
	case "2" // pronti + parziali
			// in questo caso estraggo i record che hanno un lancio di proiduzione attivo
		ls_sql_add += " and &
							EXISTS (select distinct anno_registrazione, num_registrazione, prog_riga_ord_ven from det_ordini_produzione where  &
							TES.anno_registrazione = det_ordini_produzione.anno_registrazione and &
							TES.num_registrazione  =det_ordini_produzione.num_registrazione and &
							DET.prog_riga_ord_ven  =det_ordini_produzione.prog_riga_ord_ven     &
							and flag_fine_fase ='S') "
end choose



ls_sql = ls_sql1 + ls_sql_add + " and " + ib_where_origine

ll_ret = setsqlselect(ls_sql)
if ll_ret < 1 then
	g_mb.error("Errore nell'impostazione SQL della pagina dei risultati.")
	return
end if

event ue_retrieve()



return 
end event

event ue_retrieve();long ll_errore

settransobject( sqlca )

ll_errore = retrieve()
if ll_errore < 0 then
   pcca.error = c_fatal
end if

event post ue_stato_produzione_riga()
end event

event ue_stato_produzione_riga();string 		ls_messaggio, ls_reparti[], ls_colli[], ls_colli_reset[], ls_cod_reparto, ls_msg, ls_vuoto[]
long 			ll_return, ll_anno, ll_num, ll_riga, ll_reparti_stato[], ll_cont, ll_row, ll_i, ll_colli, ll_vuoto[]
datetime 	ldt_data_pronto

uo_produzione luo_prod


luo_prod = create uo_produzione
setredraw(false)

for ll_row = 1 to rowcount()
	
	ll_anno = getitemnumber(ll_row, 1)
	ll_num = getitemnumber(ll_row, 2)
	ll_riga = getitemnumber(ll_row, 3)

	setmicrohelp( g_str.format("Aggiornamento stato produzione ordine $1-$2-$3", ll_anno, ll_num,ll_riga) )
	Yield()

	if ib_alusistemi then
		ls_reparti[] = ls_vuoto[]
		ll_reparti_stato[] = ll_vuoto[]
		ll_return = luo_prod.uof_stato_prod_det_ord_ven(ll_anno,ll_num,ll_riga,ls_messaggio)
		luo_prod.uof_stato_prod_det_ord_ven_reparti( ll_anno,ll_num,ll_riga,ls_reparti[], ll_reparti_stato[], ls_messaggio)
		
		setitem(ll_row, "rep_01",  "" )
		setitem(ll_row, "rep_02",  "" )
		setitem(ll_row, "rep_01_stato",  "0")
		setitem(ll_row, "rep_02_stato",  "0")
		// solo 2 reparti MAX
		for ll_i = upperbound(ll_reparti_stato[]) to 1 step -1
			if ll_i > 2 then continue		// solo 2 reparti
												// il primo è il TELO e il secondo è la tenda
			setitem(ll_row, "rep_"+string(ll_i,"00"),  ls_reparti[ll_i])
			setitem(ll_row, "rep_"+string(ll_i,"00") + "_stato",  string(ll_reparti_stato[ll_i]))
			
			ls_cod_reparto = ls_reparti[ upperbound(ls_reparti) ]
			// prendo i colli dell'ultimo reparto
			if ll_i = 1 then  // così lo fa solo 1 volta e solo se ci sono dei reparti
				
				select count(*)
				into	:ll_colli
				from	tab_ord_ven_colli
				where cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :ll_anno and
						num_registrazione = :ll_num and
						prog_riga_ord_ven = :ll_riga and
						cod_reparto = :ls_cod_reparto;
				
				setitem(ll_row, "rep_2_colli",  ll_colli )
			end if			
		next
		
		// imposto il flag_per il colore riga
		if getitemstring(ll_row,"rep_02") <> '' then // ci sono almeno 2 reparti
			// se sono entrambi completati allora VERDE
			if getitemstring(ll_row,"rep_01_stato")='1' and getitemstring(ll_row,"rep_02_stato")='1' then
				setitem(ll_row, "colore_riga","1")  // verde
			// altrimenti se almeno una delle due è pronta metti giallo	
			elseif getitemstring(ll_row,"rep_01_stato")='1' or getitemstring(ll_row,"rep_02_stato")='1' then
				setitem(ll_row, "colore_riga","2")   // giallo
			else
				setitem(ll_row, "colore_riga","0")   // bianco
			end if
		else
			// un solo reparto
			if getitemstring(ll_row,"rep_01_stato")='1' then
				setitem(ll_row, "colore_riga","1")  // verde
			// altrimenti se almeno una delle due è pronta metti giallo	
			elseif getitemstring(ll_row,"rep_01_stato")='2' then
				setitem(ll_row, "colore_riga","2")   // giallo
			else
				setitem(ll_row, "colore_riga","0")   // bianco
			end if
		end if
		
		// Richiesta di Paride 27/8/2020 mostrare la data produzione al posto della data consegna
		select max(data_pronto)
		into	:ldt_data_pronto
		from  det_ord_ven_prod
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno and
				num_registrazione = :ll_num and
				prog_riga_ord_ven = :ll_riga ;
		setitem(ll_row, "data_produzione", ldt_data_pronto)  
		
	else
		ll_return = luo_prod.uof_stato_prod_det_ord_ven(ll_anno,ll_num,ll_riga,ls_messaggio)
		choose case ll_return
			case 0
				setitem(ll_row, "cf_stato_prod", "N")
			case 1
				setitem(ll_row, "cf_stato_prod", "T")
			case 2
				setitem(ll_row, "cf_stato_prod", "P")
			case else
				setitem(ll_row, "cf_stato_prod", "-")
		end choose
	end if
next
destroy luo_prod;
setredraw(true)

return
end event

event buttonclicked;call super::buttonclicked;if dwo.name="b_selezione" then
	long ll_i
	if dwo.text="SEL" then
		for ll_i = 1 to this.rowcount()
			setitem(ll_i,"selezione","S")
		next
		this.object.b_selezione.text = "SEL*"
	elseif dwo.text="SEL*" then
		for ll_i = 1 to this.rowcount()
			setitem(ll_i,"selezione","N")
		next
		this.object.b_selezione.text = "SEL"
	end if
end if

end event

type tabpage_invio from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4855
integer height = 2432
long backcolor = 12632256
string text = "EXCEL + CONFERMA"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_excel dw_excel
end type

on tabpage_invio.create
this.dw_excel=create dw_excel
this.Control[]={this.dw_excel}
end on

on tabpage_invio.destroy
destroy(this.dw_excel)
end on

type dw_excel from uo_std_dw within tabpage_invio
integer x = 5
integer y = 8
integer width = 3657
integer height = 520
integer taborder = 30
string dataobject = "d_analisi_pronto_ordini_conferma"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_spedisci"
		// evade gli ordini
		string as_message
		if wf_spedisci( as_message ) < 0 then
			g_mb.error(as_message)
		end if
		
	case "b_excel" 
	
	// per prima cosa creo una copia del file di origine
	string ls_file_modello, ls_file_target, ls_errore
	long	ll_ret
	
	accepttext()
	
	ls_file_target = guo_functions.uof_get_user_documents_folder( ) + "Spedizioni\"
	if not DirectoryExists ( ls_file_target ) then
		CreateDirectory ( ls_file_target )
	end if
	ls_file_target = guo_functions.uof_get_user_documents_folder( ) + "Spedizioni\"+ guo_functions.uof_get_random_filename( ) + ".xlsx"
	
	// genero il file Excel
	if wf_genera_excel(ls_file_target, ref ls_errore) < 0 then
		g_mb.error(ls_errore)
	else
		g_mb.show("File Generato con successo")
	end if

end choose
end event

type dw_prodotti from uo_dddw_checkbox within w_analisi_pronto_ordini
integer x = 640
integer y = 460
integer width = 1897
integer height = 1140
integer taborder = 30
boolean bringtotop = true
end type

