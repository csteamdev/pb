﻿$PBExportHeader$w_inventario_magazzino_ptenda.srw
$PBExportComments$Ereditato da report inventario standard.
forward
global type w_inventario_magazzino_ptenda from w_inventario_magazzino_euro
end type
end forward

global type w_inventario_magazzino_ptenda from w_inventario_magazzino_euro
end type
global w_inventario_magazzino_ptenda w_inventario_magazzino_ptenda

on w_inventario_magazzino_ptenda.create
call super::create
end on

on w_inventario_magazzino_ptenda.destroy
call super::destroy
end on

event pc_setwindow;call super::pc_setwindow;

ib_gibus = true
end event

type dw_folder from w_inventario_magazzino_euro`dw_folder within w_inventario_magazzino_ptenda
end type

type dw_report from w_inventario_magazzino_euro`dw_report within w_inventario_magazzino_ptenda
end type

type pb_1 from w_inventario_magazzino_euro`pb_1 within w_inventario_magazzino_ptenda
end type

type dw_selezione from w_inventario_magazzino_euro`dw_selezione within w_inventario_magazzino_ptenda
string dataobject = "d_dw_selezione_rep_inventario_ptenda"
end type

type dw_cod_deposito from w_inventario_magazzino_euro`dw_cod_deposito within w_inventario_magazzino_ptenda
integer x = 2021
integer y = 224
integer height = 876
end type

type cb_report from w_inventario_magazzino_euro`cb_report within w_inventario_magazzino_ptenda
end type

event cb_report::clicked;/**
 * stefanop
 * 07/06/2012
 *
 * TOLTO ANCESTOR SCRIPT, QUALCHE GENIO LO AVEVA LASCIATO SPUNTATO
 **/
  
 
string			ls_cod_prodotto_da, ls_cod_cat_mer_1, ls_flag_bloccato, ls_where, ls_error, ls_cod_prodotto, ls_messaggio,&
				ls_cod_misura_mag, ls_cod_cat_mer, ls_flag_blocco, ls_sort, ls_sql, ls_cod_prodotto_a, &
				ls_des_prodotto, ls_flag_det_depositi, ls_cod_deposito, ls_flag_det_stock, &
				ls_flag_visualizza_val, ls_flag_tipo_valorizzazione, ls_chiave[], ls_flag_lifo, &
				ls_intestazione, ls_des_azienda, ls_flag_visulizza_giac_zero, ls_costo_unitario_t, &
				ls_vettore_nullo[],ls_flag_fiscale, ls_flag_classe, ls_flag_includi_cdf, ls_array[], ls_cod_depositi_array[], ls_flag_attiva_raggruppo
				
integer		li_return, li_cont, ll_i
long			ll_newrow, ll_num_righe, ll_num_stock, ll_j, ll_ret

dec{4}		ld_saldo_quan_inizio_anno, ld_quant_val[], ld_giacenza, ld_totale_quantita, ld_costo_medio, ld_quan_acq, ld_val_inizio_anno, &
				ld_costo_standard, ld_prezzo_acquisto, ld_costo, ld_nulla, ld_giacenza_stock[], ld_costo_unitario, ld_vettore_nullo[], &
				ld_costo_medio_continuo,ld_costo_medio_continuo_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]
				
datetime		ldt_data_a, ldt_data_blocco, ldt_data_chiusura_annuale

uo_costo_medio_continuo	luo_costo_medio
uo_magazzino					luo_magazzino


dw_selezione.AcceptText()


ls_flag_attiva_raggruppo = dw_selezione.getItemstring(1, "flag_attiva_raggruppo")
//se hai attivato il report con raggruppamenti per codice raggruppo allora chiama la funzione apposita
//il raggruppio si attiva solo per dettaglio depositi
//if ls_flag_attiva_raggruppo="S" and (ls_flag_det_depositi="S"  or (ls_flag_det_depositi="N" and ls_flag_det_stock="N")) then
if ls_flag_attiva_raggruppo="S" then
	wf_report_raggruppo()
	return
end if
//------------------------------------------------------------------------------------------------------------------

// datetime ldt_data_a è la data di inventario
dw_report.reset()
dw_report.setredraw(false)
ib_interrupt = false
select data_chiusura_annuale
into :ldt_data_chiusura_annuale
from con_magazzino
where cod_azienda = :s_cs_xx.cod_azienda;

ldt_data_a = dw_selezione.getItemDatetime(1,"data_inventario")

if ldt_data_a < ldt_data_chiusura_annuale then
	g_mb.messagebox("Inventario Magazzino", "La data di inventario deve essere posteriore alla data della chiusura annuale")
	return -1
end if

setnull(ld_nulla)

select rag_soc_1
into :ls_des_azienda
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;
if not isnull(ls_des_azienda) then ls_intestazione = ls_des_azienda

ls_cod_prodotto_da = dw_selezione.getItemString(1, "cod_prodotto_da")
ls_cod_prodotto_a = dw_selezione.getItemString(1, "cod_prodotto_a")
ls_cod_cat_mer_1 = dw_selezione.getItemString(1, "cod_cat_mer")
ldt_data_a = dw_selezione.getItemDatetime(1, "data_inventario")
ls_flag_det_depositi = dw_selezione.getItemString(1, "flag_det_depositi")
// stefanop 06/06/2012 - messa drop multipla
//ls_cod_deposito = dw_selezione.getItemString(1, "cod_deposito")
ls_cod_deposito = dw_cod_deposito.uof_get_selected_column("cod_deposito")
dw_cod_deposito.uof_get_selected_column_as_array("cod_deposito", ls_cod_depositi_array)
//ls_cod_deposito = iuo_dddw_depositi.uof_get_where()
//iuo_dddw_depositi.uof_get_selected_key(ls_cod_depositi_array)
//// ----
ls_flag_det_stock = dw_selezione.getItemString(1, "flag_det_stock")
ls_flag_visualizza_val = dw_selezione.getItemString(1, "flag_visualizza_val")
ls_flag_tipo_valorizzazione = dw_selezione.getItemString(1, "flag_tipo_valorizzazione")
ls_flag_visulizza_giac_zero = dw_selezione.getItemString(1, "flag_visulizza_giac_zero")
ls_flag_fiscale = dw_selezione.getItemString(1, "flag_fiscale")
ls_flag_lifo = dw_selezione.getItemString(1, "flag_lifo")
ls_flag_classe = dw_selezione.getItemString(1, "flag_classe")
ls_flag_includi_cdf = dw_selezione.getItemString(1, "flag_includi_cdf")
if isnull(ls_flag_det_depositi) then ls_flag_det_depositi = "N"
if isnull(ls_flag_det_stock) then ls_flag_det_stock = "N"
if isnull(ls_flag_visulizza_giac_zero) then ls_flag_visulizza_giac_zero = "N"
if isnull(ls_flag_fiscale) or ls_flag_fiscale="" then ls_flag_fiscale = "T"
if isnull(ls_flag_lifo) or ls_flag_lifo="" then ls_flag_lifo = "T"
if isnull(ls_flag_classe) or len(ls_flag_classe) < 1 then ls_flag_classe = "X"


if isnull(ls_cod_cat_mer_1) or ls_cod_cat_mer_1 = "" then	// devo selezionare ls_cod_prodotto_da e ls_cod_prodotto_a
	if (isnull(ls_cod_prodotto_da) or ls_cod_prodotto_da="") or (isnull(ls_cod_prodotto_a)  or ls_cod_prodotto_a="") then
		g_mb.messagebox("Apice", "Selezionare sia cod._prodotto_da, sia cod._prodotto_a")
		return -1
	else
		ls_intestazione = ls_intestazione + "; prodotti da:" + ls_cod_prodotto_da + &
		" a:" + ls_cod_prodotto_a + "; "
	end if
else
	ls_intestazione = ls_intestazione + "; cat. merceologica:" + ls_cod_cat_mer_1 + "; "
end if

if (not isnull(ls_cod_deposito)) and (ls_cod_deposito<>"") then
	ls_intestazione = ls_intestazione + " deposito:" + dw_selezione.getitemstring(1, "cod_deposito") + "; "
end if

choose case ls_flag_visualizza_val
		
	case "N", "X"
		ls_flag_tipo_valorizzazione = "N"
		
	case "S"
		
		CHOOSE CASE ls_flag_tipo_valorizzazione
			CASE "S"
				ls_intestazione = ls_intestazione + " Valorizzazione = Costo Standard"
				ls_costo_unitario_t = "Costo Std."
			CASE "M"
				ls_intestazione = ls_intestazione + " Valorizzazione = Costo Medio"
				ls_costo_unitario_t = "Costo Medio"
			CASE "A"
				ls_intestazione = ls_intestazione + " Valorizzazione = Costo Acquisto"
				ls_costo_unitario_t = "Costo Acq."
			CASE "N"
				ls_costo_unitario_t = "Costo "
			CASE "C"
				ls_costo_unitario_t = "Costo Medio Continuo"
		END CHOOSE
	
end choose

if ls_flag_det_stock ="N" and ls_flag_det_depositi = "N" then
	// caso dettaglio_stock = N e dettaglio_deposito = N
	
	dw_report.DataObject = 'd_dw_report_inventario_euro_pte'
	dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
	dw_report.object.linea.visible = 0
	if isnull(ls_cod_cat_mer_1) or ls_cod_cat_mer_1 = "" then
		ls_sql = "	select cod_prodotto, des_prodotto, cod_misura_mag " +&
					" from anag_prodotti " +&
					" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
					" and cod_prodotto >= '" + ls_cod_prodotto_da + "' " +&
					" and cod_prodotto <= '" + ls_cod_prodotto_a + "' "
	else
		ls_sql = "	select cod_prodotto, des_prodotto, cod_misura_mag " +&
					" from anag_prodotti " +&
					" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
					" and cod_cat_mer = '" + ls_cod_cat_mer_1 + "' "	
	end if

	if ls_flag_fiscale="S" or ls_flag_fiscale = "N" then
		ls_sql += " and flag_articolo_fiscale = '"+ls_flag_fiscale+"' "
	end if
	
	if ls_flag_lifo="S" or ls_flag_lifo = "N" then
		ls_sql += " and flag_lifo = '"+ls_flag_lifo+"' "
	end if
					
	if ls_flag_classe <> "X" then
		ls_sql += " and flag_classe_abc = '" + ls_flag_classe + "' "
	end if
	
	DECLARE cur_1 DYNAMIC CURSOR FOR SQLSA ;
	PREPARE SQLSA FROM :ls_sql;
	OPEN DYNAMIC cur_1 ;
	
	DO while true
		Yield()
		if ib_interrupt then  // var set in other script
			if g_mb.messagebox("Inventario","Interrompo l'elaborazione ?",question!,yesno!,2) = 1 then
				exit
			else
				ib_interrupt = false
			end if
		end if
		FETCH cur_1 INTO :ls_cod_prodotto, :ls_des_prodotto, :ls_cod_misura_mag;
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca prodotti:~r~n" + SQLCA.SQLErrText)
			close cur_1;
			rollback;
			return -1
		end if
		
		select count(*)
		into :ll_num_stock
		from stock
		where cod_azienda = :s_cs_xx.cod_azienda
		  and cod_prodotto = :ls_cod_prodotto;
		if ll_num_stock > 0 then		// elaboro solo prodotti in stock
			ll_newrow = dw_report.insertrow(0) 
			dw_report.ScrollToRow(ll_newrow)
			dw_report.setitem(ll_newrow, "rs_cod_prodotto" ,ls_cod_prodotto)
			dw_report.setitem(ll_newrow, "rs_descrizione"  ,ls_des_prodotto)
			dw_report.setitem(ll_newrow, "rs_cod_misura_mag" ,ls_cod_misura_mag)
		end if
	LOOP
	CLOSE cur_1 ;
	
	ll_num_righe = dw_report.rowcount()
	if ls_flag_tipo_valorizzazione = "C" then	
		luo_costo_medio = CREATE uo_costo_medio_continuo 
	end if
	for ll_i = 1 to ll_num_righe
		Yield()
		if ib_interrupt then  // var set in other script
			if g_mb.messagebox("Inventario","Interrompo l'elaborazione ?",question!,yesno!,2) = 1 then
				exit
			else
				ib_interrupt = false
			end if
		end if
	
		ls_cod_prodotto = dw_report.getitemstring(ll_i, "rs_cod_prodotto")
		if ls_flag_tipo_valorizzazione = "C" then		// costo medio continuo (30/12/2002 Realizzato per colombin - EnMe)
			ll_ret = luo_costo_medio.uof_costo_medio_continuo(ls_cod_prodotto, ldt_data_a, ref ld_giacenza, ref ld_costo_medio_continuo, ref ls_chiave[], ref ld_giacenza_stock[], ref ld_costo_medio_continuo_stock[],dw_selezione.getitemstring(1, "path") ,ref ls_messaggio)
			if ll_ret = -1 then
				g_mb.messagebox("Inventario", ls_messaggio)
				destroy luo_costo_medio
				return -1
			end if	
			
			sle_1.text= "prodotto :" + ls_cod_prodotto
			dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza)
			
			choose case ls_flag_visualizza_val
				case "S"
					dw_report.setitem(ll_i, "rd_costo_unitario", ld_costo_medio_continuo)
					
				case "N"	// nessuna valorizzazione: deve lasciare ultima colonna vuota per rilevamenti
					dw_report.object.valore_t.text = "Quan. rilevata"
					dw_report.object.linea.visible = 1
					dw_report.setitem(ll_i, "rd_costo_unitario", ld_nulla)
					
				case "X"
					dw_report.object.valore_t.visible = 0
					dw_report.object.linea.visible = 0
					dw_report.object.t_1.visible = 0
					dw_report.object.totale.visible = 0
					dw_report.object.costo_unitario_t.visible = 0
					dw_report.object.valore_t.visible = 0
					dw_report.object.valore_riga.visible = 0
					dw_report.object.rd_costo_unitario.visible = 0
			end choose
			
		else
			for li_cont = 1 to 14
				ld_quant_val[li_cont] = 0
			next
			
			sle_1.text= "prodotto :" + ls_cod_prodotto
			
			luo_magazzino = CREATE uo_magazzino
			luo_magazzino.is_considera_depositi_fornitori = ls_flag_includi_cdf
			
			li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
			
			destroy luo_magazzino
			
			if li_return <0 then
				g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
				return -1
			end if
			
			ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
			dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza)
			
			// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
			// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
			// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
			
			choose case ls_flag_visualizza_val
				case "S"
					if ls_flag_tipo_valorizzazione = "S" then	// costo standard
						select costo_standard
						into :ld_costo_standard
						from anag_prodotti
						where cod_azienda = :s_cs_xx.cod_azienda
						  and cod_prodotto = :ls_cod_prodotto;
						if sqlca.sqlcode <>0 then
							g_mb.messagebox("Inventario prodotto:" + ls_cod_prodotto, "Errore lettura anag. prodotti")
							return -1
						end if
						ld_costo = ld_costo_standard
					end if
					if ls_flag_tipo_valorizzazione = "A" then	//ultimo costo acquisto
						// nota: se nell'intervallo indicato non ci sono movimenti di acq, allora prende 0
						if ld_quant_val[14] >0 then
							ld_costo = ld_quant_val[14]
						else
							ld_costo = 0
						end if
					end if
					if ls_flag_tipo_valorizzazione = "M" then	//costo medio di acquisto
						if ld_quant_val[12] > 0 then
							ld_costo = ld_quant_val[13] / ld_quant_val[12]
						else
							
							select costo_medio_ponderato
							into   :ld_costo
							from   lifo
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_prodotto = :ls_cod_prodotto and
									 anno_lifo = (select max(anno_lifo)
													  from   lifo
													  where  cod_azienda = :s_cs_xx.cod_azienda and
																cod_prodotto = :ls_cod_prodotto);
																	
							if sqlca.sqlcode < 0 then
								g_mb.messagebox("Inventario prodotto: " + ls_cod_prodotto, "Errore in lettura tabella lifo: " + sqlca.sqlerrtext)
								return -1
							elseif sqlca.sqlcode = 100 or isnull(ld_costo) then
								ld_costo = 0
							end if
							
						end if
					end if
					dw_report.setitem(ll_i, "rd_costo_unitario", ld_costo)
					
				case "N"		// nessuna valorizzazione: deve lasciare ultima colonna vuota per rilevamenti
					dw_report.object.valore_t.text = "Quan. rilevata"
					dw_report.object.linea.visible = 1
					dw_report.setitem(ll_i, "rd_costo_unitario", ld_nulla)
					
				case "X"
					dw_report.object.valore_t.visible = 0
					dw_report.object.linea.visible = 0
					dw_report.object.t_1.visible = 0
					dw_report.object.totale.visible = 0
					dw_report.object.costo_unitario_t.visible = 0
					dw_report.object.valore_t.visible = 0
					dw_report.object.valore_riga.visible = 0
					dw_report.object.rd_costo_unitario.visible = 0
			end choose
		end if
	next
	if ls_flag_tipo_valorizzazione = "C" then	
		destroy luo_costo_medio
	end if

	ls_sort = "rs_cod_prodotto a"
	dw_report.SetSort(ls_sort)
	dw_report.Sort()
	dw_report.object.titolo.text = "INVENTARIO al " + string(date(ldt_data_a),"dd/mm/yyyy")
	dw_report.object.titolo.width = 3223

end if		// fine caso dettaglio_stock = N e dettaglio_deposito = N


if ls_flag_det_stock ="S" and ls_flag_det_depositi = "N" then
	// caso dettaglio_stock = S e dettaglio_deposito = N
	
	dw_report.DataObject = 'd_dw_report_inventario_stock_euro_pte'
	dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
	dw_report.object.linea.visible = 0
	dw_report.object.rs_chiave_t.text = "Stock"
	if isnull(ls_cod_cat_mer_1) or ls_cod_cat_mer_1 = "" then
		ls_sql = "	select cod_prodotto, des_prodotto, cod_misura_mag " +&
					" from anag_prodotti " +&
					" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
					" and cod_prodotto >= '" + ls_cod_prodotto_da + "' " +&
					" and cod_prodotto <= '" + ls_cod_prodotto_a + "' "
	else
		ls_sql = "	select cod_prodotto, des_prodotto, cod_misura_mag " +&
					" from anag_prodotti " +&
					" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
					" and cod_cat_mer = '" + ls_cod_cat_mer_1 + "' "	
	end if
					
	if ls_flag_fiscale="S" or ls_flag_fiscale = "N" then
		ls_sql += " and flag_articolo_fiscale = '"+ls_flag_fiscale+"' "
	end if
	
	if ls_flag_lifo="S" or ls_flag_lifo = "N" then
		ls_sql += " and flag_lifo = '"+ls_flag_lifo+"' "
	end if
	
	if ls_flag_classe <> "X" then
		ls_sql += " and flag_classe_abc = '" + ls_flag_classe + "' "
	end if
	
	DECLARE cur_2 DYNAMIC CURSOR FOR SQLSA ;
	PREPARE SQLSA FROM :ls_sql;
	OPEN DYNAMIC cur_2 ;
	
	DO while true
		Yield()
		if ib_interrupt then  // var set in other script
			if g_mb.messagebox("Inventario","Interrompo l'elaborazione ?",question!,yesno!,2) = 1 then
				exit
			else
				ib_interrupt = false
			end if
		end if
		FETCH cur_2 INTO :ls_cod_prodotto, :ls_des_prodotto, :ls_cod_misura_mag;
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca prodotti:~r~n" +SQLCA.SQLErrText)
			close cur_2;
			rollback;
			return -1
		end if
		sle_1.text= "prodotto :" + ls_cod_prodotto
		
		select count(*)
		into :ll_num_stock
		from stock
		where cod_azienda = :s_cs_xx.cod_azienda
		  and cod_prodotto = :ls_cod_prodotto;
		if ll_num_stock > 0 then		// elaboro solo prodotti in stock
			ll_newrow = dw_report.insertrow(0) 
			dw_report.ScrollToRow(ll_newrow)
			dw_report.setitem(ll_newrow, "rs_cod_prodotto" ,ls_cod_prodotto)
			dw_report.setitem(ll_newrow, "rs_descrizione"  ,ls_des_prodotto)
			dw_report.setitem(ll_newrow, "rs_cod_misura_mag" ,ls_cod_misura_mag)
		end if
	LOOP
	CLOSE cur_2 ;
	
	ll_num_righe = dw_report.rowcount()
	for ll_i = 1 to ll_num_righe
		Yield()
		if ib_interrupt then  // var set in other script
			if g_mb.messagebox("Inventario","Interrompo l'elaborazione ?",question!,yesno!,2) = 1 then
				exit
			else
				ib_interrupt = false
			end if
		end if
	
		for li_cont = 1 to 14
			ld_quant_val[li_cont] = 0
		next
		ls_cod_prodotto = dw_report.getitemstring(ll_i, "rs_cod_prodotto")
		ls_des_prodotto = dw_report.getitemstring(ll_i, "rs_descrizione")
		ls_cod_misura_mag = dw_report.getitemstring(ll_i,  "rs_cod_misura_mag")
		sle_1.text= "prodotto :" + ls_cod_prodotto
		
		ls_chiave = ls_vettore_nullo
		ld_giacenza_stock = ld_vettore_nullo
		
		luo_magazzino = CREATE uo_magazzino
		luo_magazzino.is_considera_depositi_fornitori = ls_flag_includi_cdf
		
		li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "S", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
		
		destroy luo_magazzino
		
		if li_return <0 then
			g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
			return -1
		end if
		
//		ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]	giacenza totale in tutti gli stock
//		dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza)
		// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
		// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
		// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
		
		choose case ls_flag_visualizza_val
			case "S"
				if ls_flag_tipo_valorizzazione = "S" then	// costo standard
					select costo_standard
					into :ld_costo_standard
					from anag_prodotti
					where cod_azienda = :s_cs_xx.cod_azienda
					  and cod_prodotto = :ls_cod_prodotto;
					if sqlca.sqlcode <>0 then
						g_mb.messagebox("Inventario prodotto:" + ls_cod_prodotto, "Errore lettura anag. prodotti")
						return -1
					end if
					ld_costo = ld_costo_standard
				end if
				if ls_flag_tipo_valorizzazione = "A" then	//ultimo costo acquisto
					// nota: se nell'intervallo indicato non ci sono movimenti di acq, allora prende 0
					if ld_quant_val[14] >0 then
						ld_costo = ld_quant_val[14]
					else
						ld_costo = 0
					end if
				end if
				if ls_flag_tipo_valorizzazione = "M" then	//costo medio
					if ld_quant_val[12] >0 then
						ld_costo = ld_costo_medio_stock[1]
					else
						
						select costo_medio_ponderato
						into   :ld_costo
						from   lifo
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto and
								 anno_lifo = (select max(anno_lifo)
												  from   lifo
												  where  cod_azienda = :s_cs_xx.cod_azienda and
															cod_prodotto = :ls_cod_prodotto);
																
						if sqlca.sqlcode < 0 then
							g_mb.messagebox("Inventario prodotto: " + ls_cod_prodotto, "Errore in lettura tabella lifo: " + sqlca.sqlerrtext)
							return -1
						elseif sqlca.sqlcode = 100 or isnull(ld_costo) then
							ld_costo = 0
						end if
						
					end if
				end if
				dw_report.setitem(ll_i, "rd_costo_unitario", ld_costo)
				
			case "N"		// nessuna valorizzazione: deve lasciare ultima colonna vuota per rilevamenti
				dw_report.object.valore_t.text = "Quan. rilevata"
				dw_report.object.linea.visible = 1
				dw_report.setitem(ll_i, "rd_costo_unitario", ld_nulla)
				
			case "X"
				dw_report.object.valore_t.visible = 0
				dw_report.object.linea.visible = 0
				dw_report.object.t_1.visible = 0
				dw_report.object.totale.visible = 0
				dw_report.object.costo_unitario_t.visible = 0
				dw_report.object.valore_t.visible = 0
				dw_report.object.valore_riga.visible = 0
				dw_report.object.rd_costo_unitario.visible = 0
			
		end choose
		
		ll_num_stock = upperbound(ls_chiave[])
		dw_report.setitem(ll_i, "rs_chiave", ls_chiave[1])
		dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza_stock[1])
		dw_report.accepttext()
		
//		messagebox("debug 1: ll_num_stock ", string(ll_num_stock))
		
		ld_costo_unitario = dw_report.getitemnumber(ll_i, "rd_costo_unitario")
		if ll_num_stock > 1 then	// più stock
			for ll_j = 2 to (ll_num_stock)
				ll_newrow = dw_report.insertrow(ll_i + 1) 
				dw_report.ScrollToRow(ll_newrow)
				ll_num_righe = ll_num_righe + 1
				ll_i = ll_i +1
				dw_report.setitem(ll_newrow, "rs_cod_prodotto" ,ls_cod_prodotto)
				dw_report.setitem(ll_newrow, "rs_descrizione"  ,ls_des_prodotto)
				dw_report.setitem(ll_newrow, "rs_cod_misura_mag" ,ls_cod_misura_mag)
				dw_report.setitem(ll_newrow, "rs_chiave", ls_chiave[ll_j])
				dw_report.setitem(ll_newrow, "rd_giacenza", ld_giacenza_stock[ll_j])
//				dw_report.setitem(ll_newrow, "rd_costo_unitario", ld_costo_unitario) ENME
				dw_report.setitem(ll_newrow, "rd_costo_unitario", ld_costo_medio_stock[ll_j])
			next
		end if

		ls_sort = "rs_cod_prodotto a"
		dw_report.SetSort(ls_sort)
		dw_report.Sort()
		dw_report.object.titolo.text = "INVENTARIO al " + string(date(ldt_data_a),"dd/mm/yyyy")
		dw_report.object.titolo.width = 4618

	next
end if		// fine caso dettaglio_stock = S e dettaglio_deposito = N

if ls_flag_det_stock ="N" and ls_flag_det_depositi = "S" then
	// caso dettaglio_stock = N e dettaglio_deposito = S
	
	dw_report.DataObject = 'd_dw_report_inventario_deposito_euro_pte'
	dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
	
	dw_report.object.linea.visible = 0
	dw_report.object.rs_chiave_t.text = "Deposito"
	if isnull(ls_cod_deposito) or ls_cod_deposito = "" then
		if isnull(ls_cod_cat_mer_1) or ls_cod_cat_mer_1 = "" then
			ls_sql = "	select cod_prodotto, des_prodotto, cod_misura_mag " +&
						" from anag_prodotti " +&
						" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
						" and cod_prodotto >= '" + ls_cod_prodotto_da + "' " +&
						" and cod_prodotto <= '" + ls_cod_prodotto_a + "' "
		else
			ls_sql = "	select cod_prodotto, des_prodotto, cod_misura_mag " +&
						" from anag_prodotti " +&
						" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
						" and cod_cat_mer = '" + ls_cod_cat_mer_1 + "' "	
		end if
		
		if ls_flag_fiscale="S" or ls_flag_fiscale = "N" then
			ls_sql += " and flag_articolo_fiscale = '"+ls_flag_fiscale+"' "
		end if
		
		if ls_flag_lifo="S" or ls_flag_lifo = "N" then
			ls_sql += " and flag_lifo = '"+ls_flag_lifo+"' "
		end if
		
		if ls_flag_classe <> "X" then
			ls_sql += " and flag_classe_abc = '" + ls_flag_classe + "' "
		end if
	
	else
		if isnull(ls_cod_cat_mer_1) or ls_cod_cat_mer_1 = "" then
			
			ls_sql = "	select cod_prodotto, des_prodotto, cod_misura_mag " +&
						" from anag_prodotti " +&
						" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
						" and cod_prodotto >= '" + ls_cod_prodotto_da + "' " +&
						" and cod_prodotto <= '" + ls_cod_prodotto_a + "' "

			if ls_flag_fiscale="S" or ls_flag_fiscale = "N" then
				ls_sql += " and flag_articolo_fiscale = '"+ls_flag_fiscale+"' "
			end if
			
			if ls_flag_lifo="S" or ls_flag_lifo = "N" then
				ls_sql += " and flag_lifo = '"+ls_flag_lifo+"' "
			end if
			
			if ls_flag_classe <> "X" then
				ls_sql += " and flag_classe_abc = '" + ls_flag_classe + "' "
			end if
	
//			ls_sql +=" and cod_prodotto in (select distinct cod_prodotto " +&
//						" from stock " +&
//						" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_deposito = '" + ls_cod_deposito + "' ) "
						
			ls_sql +=" and cod_prodotto in (select distinct cod_prodotto from stock " +&
						" where cod_azienda = '" + s_cs_xx.cod_azienda + "' "
						
			if ls_cod_deposito <> "" then ls_sql += " AND " + ls_cod_deposito // " AND cod_deposito IN (" + ls_cod_deposito + ") "
			ls_sql += ") "
		else
			ls_sql = " select cod_prodotto, des_prodotto, cod_misura_mag " +&
						" from anag_prodotti " +&
						" where  cod_azienda = '" + s_cs_xx.cod_azienda + "' " +&
						" and cod_cat_mer = '" + ls_cod_cat_mer_1 + "' "

			if ls_flag_fiscale="S" or ls_flag_fiscale = "N" then
				ls_sql += " and flag_articolo_fiscale = '"+ls_flag_fiscale+"' "
			end if
			
			if ls_flag_lifo="S" or ls_flag_lifo = "N" then
				ls_sql += " and flag_lifo = '"+ls_flag_lifo+"' "
			end if
			
			if ls_flag_classe <> "X" then
				ls_sql += " and flag_classe_abc = '" + ls_flag_classe + "' "
			end if
	
//			ls_sql +=" and cod_prodotto in (select distinct cod_prodotto " +&
//						" from mov_magazzino " +&
//						" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and  cod_deposito = '" + ls_cod_deposito + "' ) "
			
			ls_sql +=" and cod_prodotto in (select distinct cod_prodotto from mov_magazzino " +&
						" where cod_azienda = '" + s_cs_xx.cod_azienda + "' "
						
			if ls_cod_deposito <> "" then ls_sql += " AND " + ls_cod_deposito //cod_deposito IN (" + ls_cod_deposito + ") "
			ls_sql += ") "
		end if
		
	end if	
	DECLARE cur_3 DYNAMIC CURSOR FOR SQLSA ;
	PREPARE SQLSA FROM :ls_sql;
	OPEN DYNAMIC cur_3 ;
	
	DO while 0=0
		Yield()
		if ib_interrupt then  // var set in other script
			if g_mb.messagebox("Inventario","Interrompo l'elaborazione ?",question!,yesno!,2) = 1 then
				exit
			else
				ib_interrupt = false
			end if
		end if
		FETCH cur_3 INTO :ls_cod_prodotto, :ls_des_prodotto, :ls_cod_misura_mag;
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca prodotti:~r~n" +SQLCA.SQLErrText)
			close cur_3;
			rollback;
			return -1
		end if
		sle_1.text= "prodotto :" + ls_cod_prodotto
		
		select count(*)
		into :ll_num_stock
		from stock
		where cod_azienda = :s_cs_xx.cod_azienda
		  and cod_prodotto = :ls_cod_prodotto;
		if ll_num_stock > 0 then		// elaboro solo prodotti in stock
			ll_newrow = dw_report.insertrow(0) 
			dw_report.ScrollToRow(ll_newrow)
			dw_report.setitem(ll_newrow, "rs_cod_prodotto" ,ls_cod_prodotto)
			dw_report.setitem(ll_newrow, "rs_descrizione"  ,ls_des_prodotto)
			dw_report.setitem(ll_newrow, "rs_cod_misura_mag" ,ls_cod_misura_mag)
		end if
	LOOP
	CLOSE cur_3 ;
	
	ll_num_righe = dw_report.rowcount()
	for ll_i = 1 to ll_num_righe
		Yield()
		if ib_interrupt then  // var set in other script
			if g_mb.messagebox("Inventario","Interrompo l'elaborazione ?",question!,yesno!,2) = 1 then
				exit
			else
				ib_interrupt = false
			end if
		end if
	
		for li_cont = 1 to 14
			ld_quant_val[li_cont] = 0
		next
		ls_cod_prodotto = dw_report.getitemstring(ll_i, "rs_cod_prodotto")
		ls_des_prodotto = dw_report.getitemstring(ll_i, "rs_descrizione")
		ls_cod_misura_mag = dw_report.getitemstring(ll_i,  "rs_cod_misura_mag")
		sle_1.text= "prodotto :" + ls_cod_prodotto

		ls_chiave = ls_vettore_nullo
		ld_giacenza_stock = ld_vettore_nullo
		
		luo_magazzino = CREATE uo_magazzino
		luo_magazzino.is_considera_depositi_fornitori = ls_flag_includi_cdf
			
		li_return = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "D", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])

		destroy luo_magazzino
		
		if li_return <0 then
			g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto, ls_error)
			return -1
		end if
		
//		ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]	giacenza totale in tutti gli stock
//		dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza)
		// ld_quant_val : [1]=quan_inizio_anno,  [2]=val_inizio_anno,  
		// [3]=quan_ultima_chius, [4]=qta_entrata, [5]=val_entrata, [6]=qta_uscita, [7]=val_uscita, 
		// [8]=qta_acq, [9]=val_acq,  [10]=qta_ven, [11]=val_ven
		choose case ls_flag_visualizza_val
			case "S"
				if ls_flag_tipo_valorizzazione = "S" then	// costo standard
					select costo_standard
					into :ld_costo_standard
					from anag_prodotti
					where cod_azienda = :s_cs_xx.cod_azienda
					  and cod_prodotto = :ls_cod_prodotto;
					if sqlca.sqlcode <>0 then
						g_mb.messagebox("Inventario prodotto:" + ls_cod_prodotto, "Errore lettura anag. prodotti")
						return -1
					end if
					ld_costo = ld_costo_standard
				end if
				if ls_flag_tipo_valorizzazione = "A" then	//ultimo costo acquisto
					// nota: se nell'intervallo indicato non ci sono movimenti di acq, allora prende 0
					if ld_quant_val[14] >0 then
						ld_costo = ld_quant_val[14]
					else
						ld_costo = 0
					end if
				end if
				if ls_flag_tipo_valorizzazione = "M" then	//costo medio
					if ld_quant_val[12] > 0 then
						ld_costo = ld_quant_val[13] / ld_quant_val[12]
					else
						
						select costo_medio_ponderato
						into   :ld_costo
						from   lifo
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto and
								 anno_lifo = (select max(anno_lifo)
												  from   lifo
												  where  cod_azienda = :s_cs_xx.cod_azienda and
															cod_prodotto = :ls_cod_prodotto);
																
						if sqlca.sqlcode < 0 then
							g_mb.messagebox("Inventario prodotto: " + ls_cod_prodotto, "Errore in lettura tabella lifo: " + sqlca.sqlerrtext)
							return -1
						elseif sqlca.sqlcode = 100 or isnull(ld_costo) then
							ld_costo = 0
						end if
						
					end if
				end if
				dw_report.setitem(ll_i, "rd_costo_unitario", ld_costo)
				
			case "N"	// nessuna valorizzazione: deve lasciare ultima colonna vuota per rilevamenti
					dw_report.object.valore_t.text = "Quan. rilevata"
					dw_report.object.linea.visible = 1
					dw_report.setitem(ll_i, "rd_costo_unitario", ld_nulla)
					
			case "X"
				dw_report.object.valore_t.visible = 0
				dw_report.object.linea.visible = 0
				dw_report.object.t_1.visible = 0
				dw_report.object.totale.visible = 0
				dw_report.object.costo_unitario_t.visible = 0
				dw_report.object.valore_t.visible = 0
				dw_report.object.valore_riga.visible = 0
				dw_report.object.rd_costo_unitario.visible = 0
			end choose
		ll_num_stock = upperbound(ls_chiave[])
		// devo scrivere tutti depositi 
		//if isnull(ls_cod_deposito) or ls_cod_deposito = "" then
		if isnull(ls_cod_deposito) or ls_cod_deposito = "" or upperbound(ls_cod_depositi_array) > 1 then
			dw_report.setitem(ll_i, "rs_chiave", ls_chiave[1])
			dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza_stock[1])
			dw_report.accepttext()
			
//			messagebox("debug 1: ll_num_depositi ", string(ll_num_stock))
			
			ld_costo_unitario = dw_report.getitemnumber(ll_i, "rd_costo_unitario")
			if ll_num_stock > 1 then	// più depositi
				for ll_j = 2 to (ll_num_stock)
					ll_newrow = dw_report.insertrow(ll_i + 1) 
					dw_report.ScrollToRow(ll_newrow)
					ll_num_righe = ll_num_righe + 1
					ll_i = ll_i +1
					dw_report.setitem(ll_newrow, "rs_cod_prodotto" ,ls_cod_prodotto)
					dw_report.setitem(ll_newrow, "rs_descrizione"  ,ls_des_prodotto)
					dw_report.setitem(ll_newrow, "rs_cod_misura_mag" ,ls_cod_misura_mag)
					dw_report.setitem(ll_newrow, "rs_chiave", ls_chiave[ll_j])
					dw_report.setitem(ll_newrow, "rd_giacenza", ld_giacenza_stock[ll_j])
					dw_report.setitem(ll_newrow, "rd_costo_unitario", ld_costo_unitario)
				next
			end if
		else		// solo deposito indicato
			for ll_j = 1 to ll_num_stock
				//if ls_chiave[ll_j] = ls_cod_deposito then
				// stefanop: 07/06/2012: array di depositi
				if guo_functions.uof_in_array(ls_chiave[ll_j], ls_cod_depositi_array) then
					dw_report.setitem(ll_i, "rs_chiave", ls_chiave[ll_j])
					dw_report.setitem(ll_i, "rd_giacenza", ld_giacenza_stock[ll_j])
				end if
			next
		end if
	next		// next ll_i
	
	ls_sort = "rs_chiave a, rs_cod_prodotto a"
	dw_report.SetSort(ls_sort)
	dw_report.Sort()
	dw_report.groupcalc()
	dw_report.object.titolo.text = "INVENTARIO al " + string(date(ldt_data_a),"dd/mm/yyyy")
	dw_report.object.titolo.width = 3223

end if		// fine caso dettaglio_stock = N e dettaglio_deposito = S

string ls_filtro

if ls_flag_visulizza_giac_zero = "N" then
	ls_filtro = "rd_giacenza > 0"
	dw_report.SetFilter(ls_filtro)
	dw_report.Filter()
	ll_num_righe = dw_report.rowcount()
end if

dw_report.object.intestazione.text = ls_intestazione
dw_report.object.costo_unitario_t.text = ls_costo_unitario_t
dw_report.setredraw(true)

dw_report.Object.DataWindow.Print.Preview	='Yes'
dw_report.Object.DataWindow.Print.Preview.rulers	='Yes'
dw_report.SetSort(ls_sort)
dw_report.Sort()

dw_folder.fu_selecttab(2)
dw_report.Change_DW_Current()

end event

type cb_annulla from w_inventario_magazzino_euro`cb_annulla within w_inventario_magazzino_ptenda
end type

type sle_1 from w_inventario_magazzino_euro`sle_1 within w_inventario_magazzino_ptenda
end type

