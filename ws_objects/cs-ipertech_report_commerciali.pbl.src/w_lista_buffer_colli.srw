﻿$PBExportHeader$w_lista_buffer_colli.srw
$PBExportComments$Finestra Report Attrezzature Semplici
forward
global type w_lista_buffer_colli from w_cs_xx_principale
end type
type dw_folder from u_folder within w_lista_buffer_colli
end type
type dw_lista from uo_cs_xx_dw within w_lista_buffer_colli
end type
type dw_selezione from uo_cs_xx_dw within w_lista_buffer_colli
end type
type dw_import from datawindow within w_lista_buffer_colli
end type
type cb_importa from commandbutton within w_lista_buffer_colli
end type
type cb_annulla from commandbutton within w_lista_buffer_colli
end type
type cb_cerca from commandbutton within w_lista_buffer_colli
end type
end forward

global type w_lista_buffer_colli from w_cs_xx_principale
integer width = 4165
integer height = 2876
string title = "Risultato Scansione Colli"
dw_folder dw_folder
dw_lista dw_lista
dw_selezione dw_selezione
dw_import dw_import
cb_importa cb_importa
cb_annulla cb_annulla
cb_cerca cb_cerca
end type
global w_lista_buffer_colli w_lista_buffer_colli

forward prototypes
public function integer wf_seleziona_file (ref string as_msg)
public function integer wf_elabora_import (datastore ads_input, ref string as_errore)
public subroutine wf_retrieve_by_id (long al_id)
end prototypes

public function integer wf_seleziona_file (ref string as_msg);string			ls_file, ls_docname, ls_named, ls_sql, ls_errore, ls_FMP
long			ll_ret

datastore	lds_input


//FMP
guo_functions.uof_get_parametro_azienda("FMP", ls_FMP)
if ls_FMP<>"" and not isnull(ls_FMP) then ChangeDirectory(ls_FMP)

ll_ret = GetFileOpenName("Seleziona File", ls_docname, ls_named, 	&
															"TXT", &
															"Files di testo (*.TXT),*.TXT,"+&
															"Files CSV (*.CSV),*.CSV,")

if ll_ret = 1 then
	dw_selezione.setitem(1, "file", ls_docname)
else
	dw_selezione.setitem(1, "file", "")
end if

return 0
end function

public function integer wf_elabora_import (datastore ads_input, ref string as_errore);string							ls_input, ls_code[], ls_data[], ls_barcode, ls_tipo, ls_cod_operaio
long							ll_index, ll_ret, ll_count, ll_riga, ll_progressivo, ll_barcode, ll_numero
integer						li_return, li_anno
datetime						ldt_data
s_cs_xx_parametri			lstr_return


ll_ret = ads_input.rowcount()
as_errore = ""

dw_import.reset()

//FORMATO DEL FILE TXT
//			COLLO;BARCODE_NUM;TIPO;ANNO;NUMERO;RIGA;DATA_INVIO;OPERAIO

for ll_index = 1 to ll_ret
	
	ls_input = ads_input.getitemstring(ll_index,"input")
	
	if isnull(ls_input) or ls_input = "" then
		//salta la riga
		continue
	end if
	//COLLO;BARCODE_NUM;TIPO;ANNO;NUMERO;RIGA;DATA_INVIO;OPERAIO
	f_split(ls_input, ";", ls_code[])

	//quindi as_input[] è fatto cosi:
	//		as_input[1]      codice alfanumerico del COLLO
	//		as_input[2]      barcode numerico del collo
	//		as_input[3]		tipologia (T o D)
	//		as_input[4]      anno ordine
	//		as_input[5]      numero ordine
	//		as_input[6]      riga ordine
	//		as_input[7]      data invio
	//		as_input[8]      codice operaio
	
	if upperbound(ls_code[]) < 8 then
		as_errore = "Numero colonne ("+string(upperbound(ls_code[]))+") inferiori a quelle previste (8)!"
		return -1
	end if
	
	ll_count = dw_import.insertrow(0)
	dw_import.setitem(ll_count, "barcode", ls_code[1])
	dw_import.setitem(ll_count, "barcode_num", long(ls_code[2]))
	dw_import.setitem(ll_count, "tipo", ls_code[3])
	dw_import.setitem(ll_count, "anno_registrazione", integer(ls_code[4]))
	dw_import.setitem(ll_count, "num_registrazione", long(ls_code[5]))
	
	if ls_code[6]<>"" and isnumber(ls_code[6]) then
		ll_riga = long(ls_code[6])
	else
		setnull(ll_riga)
	end if
	dw_import.setitem(ll_count, "prog_riga_ord_ven", ll_riga)
	
	try
		f_split(ls_code[7], "/", ls_data[])
		
		ldt_data = 	datetime(date(long(ls_data[3]), long(ls_data[2]), long(ls_data[1])), &
						time(long(ls_data[4]), long(ls_data[5]), 0))
	catch (runtimeerror err)
		ldt_data = datetime(today(), now())
	end try
	
	dw_import.setitem(ll_count, "data_invio", ldt_data)
	dw_import.setitem(ll_count, "cod_operaio", ls_code[8])
next

destroy ads_input

if dw_import.rowcount() = 0 then
	as_errore = "Alla fine dell'elaborazione del file, nessuna riga è stata caricata!"
	return -1
end if

lstr_return.parametro_dw_1 = dw_import

openwithparm(w_lista_buffer_colli_file, lstr_return)

lstr_return = message.powerobjectparm
if lstr_return.parametro_b_1 then
	
	ll_count = dw_import.rowcount()
	
	if ll_count >0 then
	else
		as_errore = "nessuna riga risulta da inserire!"
		return -1
	end if
	
	//inserisci dati
	
	//genera progressivo ID
	select max(progressivo)
	into :ll_progressivo
	from buffer_colli
	where cod_azienda=:s_cs_xx.cod_azienda;
	
	if isnull(ll_progressivo) then ll_progressivo = 0
	ll_progressivo += 1
	
	for ll_index = 1 to ll_count
			ls_barcode = dw_import.getitemstring(ll_index, "barcode")
			ll_barcode = dw_import.getitemnumber(ll_index, "barcode_num")
			ls_tipo = dw_import.getitemstring(ll_index, "tipo")
			li_anno = dw_import.getitemnumber(ll_index, "anno_registrazione")
			ll_numero = dw_import.getitemnumber(ll_index, "num_registrazione")
			ll_riga = dw_import.getitemnumber(ll_index, "prog_riga_ord_ven")
			
			if ll_riga>0 then
			else
				setnull(ll_riga)
			end if
			
			ldt_data = dw_import.getitemdatetime(ll_index, "data_invio")
			ls_cod_operaio = dw_import.getitemstring(ll_index, "cod_operaio")
				
			insert into buffer_colli
					( cod_azienda,
					  progressivo,
					  barcode,
					  barcode_num,
					  tipo,
					  anno_registrazione,
					  num_registrazione,
					  prog_riga_ord_ven,
					  data_invio,
					  cod_operaio)
			values (	:s_cs_xx.cod_azienda,   
						:ll_progressivo,
						:ls_barcode,
						:ll_barcode,
						:ls_tipo,
						:li_anno,
						:ll_numero,
						:ll_riga,
						:ldt_data,   
						:ls_cod_operaio);
		
			if sqlca.sqlcode<0 then			
				as_errore = "Errore inserimento dati " +sqlca.sqlerrtext
				rollback;
				return -1
			end if
		
	next
	
	as_errore = "Importazione avvenuta con successo! ID = " + string(ll_progressivo)
	
	dw_selezione.setitem(1, "file", "")
	wf_retrieve_by_id(ll_progressivo)
	
	return 0
	
else
	as_errore = "Annullato dall'operatore!"
	return 1
end if


end function

public subroutine wf_retrieve_by_id (long al_id);datetime			ldt_null
string				ls_null


setnull(ldt_null)
setnull(ls_null)

dw_selezione.setitem(1, "data_creazione", ldt_null)
dw_selezione.setitem(1, "progressivo", al_id)
dw_selezione.setitem(1, "progressivo_a", al_id)
dw_selezione.setitem(1, "cod_operaio", ls_null)

cb_cerca.postevent(clicked!)

return
end subroutine

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[], lw_vuoto[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)


dw_lista.set_dw_options(sqlca, &
                         pcca.null_object, &
                        c_nonew + &
					c_nomodify + &
					c_nodelete + &
					c_noretrieveonopen + &
					c_noenablenewonopen + &
					c_noenablemodifyonopen + &
					c_scrollparent + &
					c_disablecc, &
                         c_default )
			 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

iuo_dw_main = dw_lista


lw_oggetti[1] = dw_lista
lw_oggetti[2] = dw_selezione
lw_oggetti[3] = cb_cerca
lw_oggetti[4] = cb_annulla
lw_oggetti[4] = cb_importa
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

dw_folder.fu_foldercreate(1,1)
dw_folder.fu_selecttab(1)
							
dw_lista.object.p_1.filename = s_cs_xx.volume + s_cs_xx.risorse + "treeview\cartella.png"
dw_lista.object.p_2.filename = s_cs_xx.volume + s_cs_xx.risorse + "treeview\documento_blu.png"
dw_selezione.object.b_elimina_tutto.visible = false

dw_selezione.object.b_file.visible = true
dw_selezione.object.file.visible = true




end event

on w_lista_buffer_colli.create
int iCurrent
call super::create
this.dw_folder=create dw_folder
this.dw_lista=create dw_lista
this.dw_selezione=create dw_selezione
this.dw_import=create dw_import
this.cb_importa=create cb_importa
this.cb_annulla=create cb_annulla
this.cb_cerca=create cb_cerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_folder
this.Control[iCurrent+2]=this.dw_lista
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.dw_import
this.Control[iCurrent+5]=this.cb_importa
this.Control[iCurrent+6]=this.cb_annulla
this.Control[iCurrent+7]=this.cb_cerca
end on

on w_lista_buffer_colli.destroy
call super::destroy
destroy(this.dw_folder)
destroy(this.dw_lista)
destroy(this.dw_selezione)
destroy(this.dw_import)
destroy(this.cb_importa)
destroy(this.cb_annulla)
destroy(this.cb_cerca)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_sort(dw_selezione, &
									  "cod_operaio", sqlca, &
									  "anag_operai", "cod_operaio", "cognome + ' ' + nome", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N'", &
									  " cognome ASC, nome ASC ")		  					  

end event

type dw_folder from u_folder within w_lista_buffer_colli
integer x = 23
integer y = 20
integer width = 4078
integer height = 2724
integer taborder = 60
end type

event po_tabclicked;call super::po_tabclicked;//CHOOSE CASE i_SelectedTabName
//   CASE "Report"
//      SetFocus(dw_report_manut_eseguite)
//		
//   CASE "Selezione"
//      SetFocus(dw_sel_manut_eseguite)
//
//END CHOOSE
//
end event

type dw_lista from uo_cs_xx_dw within w_lista_buffer_colli
boolean visible = false
integer x = 50
integer y = 856
integer width = 3982
integer height = 1832
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_lista_buffer_colli"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event buttonclicked;call super::buttonclicked;long ll_progressivo

if row>=0 then
else
	return
end if

choose case dwo.name
	case "b_cancella"
		ll_progressivo = getitemnumber(row, "progressivo")
		
		if ll_progressivo>0 then
			
			if g_mb.confirm("Eliminare i dati relativi alla scansione con ID = " + string(ll_progressivo) + " ?") then
			else
				return
			end if
			
			delete from buffer_colli
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						progressivo=:ll_progressivo;
						
			if sqlca.sqlcode<0 then
				g_mb.messagebox("APICE","Errore in cancellazione. "+sqlca.sqlerrtext, StopSign!)
				rollback;
				return
			end if
			
			commit;
			cb_cerca.postevent(clicked!)
		end if
		
end choose
end event

event pcd_retrieve;call super::pcd_retrieve;datetime		ldt_data_rif,ldt_data_rif_fine
long			ll_progressivo, ll_tot, ll_progressivo_a
string			ls_cod_operaio

dw_lista.reset()

dw_selezione.accepttext()

ls_cod_operaio = dw_selezione.getitemstring(1,"cod_operaio")
ll_progressivo = dw_selezione.getitemnumber(1,"progressivo")
ll_progressivo_a = dw_selezione.getitemnumber(1,"progressivo_a")
ldt_data_rif = dw_selezione.getitemdatetime(1,"data_creazione")

if ls_cod_operaio="" then setnull(ls_cod_operaio)
if ll_progressivo<=0 then setnull(ll_progressivo)
if ll_progressivo_a<=0 then setnull(ll_progressivo_a)

if isnull(ldt_data_rif) or year(date(ldt_data_rif))<=1950 then
	ldt_data_rif = datetime(date(1950,1,1), 00:00:00)
	ldt_data_rif_fine = datetime(date(2200,12,31), 23:59:59)
else
	ldt_data_rif = datetime(date(ldt_data_rif), 00:00:00)
	ldt_data_rif_fine = datetime(date(ldt_data_rif), 23:59:59)
end if


ll_tot = retrieve(s_cs_xx.cod_azienda, ldt_data_rif, ldt_data_rif_fine, ll_progressivo, ll_progressivo_a, ls_cod_operaio)

ldt_data_rif_fine = datetime(date(ldt_data_rif), 23:59:59)
end event

type dw_selezione from uo_cs_xx_dw within w_lista_buffer_colli
integer x = 50
integer y = 200
integer width = 2784
integer height = 616
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_report_buffer_carico_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;
string					ls_msg

choose case dwo.name
	case "b_file"
		if wf_seleziona_file(ls_msg) < 0 then
			g_mb.error(ls_msg)
		end if
		
end choose
end event

type dw_import from datawindow within w_lista_buffer_colli
boolean visible = false
integer x = 2939
integer y = 572
integer width = 937
integer height = 364
integer taborder = 60
boolean bringtotop = true
string title = "none"
string dataobject = "d_lista_buffer_colli_file"
boolean border = false
boolean livescroll = true
end type

type cb_importa from commandbutton within w_lista_buffer_colli
integer x = 2839
integer y = 624
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Importa"
end type

event clicked;string			ls_file, ls_sql, ls_errore
datastore	lds_input
long			ll_ret
integer		li_ret



dw_selezione.accepttext()

ls_file = dw_selezione.getitemstring(1, "file")

if g_str.isempty( ls_file ) then
	g_mb.warning("Selezionare prima un file da importare!")
	return
end if

if not g_mb.confirm("Importare il File selezionato nel percorso?") then return

//FORMATO DEL FILE TXT
//			COLLO;BARCODE_NUM;TIPO;ANNO;NUMERO;RIGA;DATA_INVIO;OPERAIO

if ls_file<>"" and not isnull(ls_file) then
	//aggiorna la lista leggendola dal file

	ls_sql = "select LTRIM('                                                                                                                                                      ') as input, 0 as id_buffer "+&
				"from aziende "+&
				"where cod_azienda=''"
	guo_functions.uof_crea_datastore(lds_input, ls_sql, ls_errore)

	ll_ret = lds_input.importfile(CSV!, ls_file)

	if ll_ret<0 then
		g_mb.error("Errore in lettura file selezionato: ("+string(ll_ret)+")")
		return -1

	elseif ll_ret=0 then
		g_mb.error("Il file selezionato sembra vuoto!")
		return -1
	end if

	ls_errore = ""
	li_ret = wf_elabora_import(lds_input, ls_errore)
	if li_ret < 0 then
		destroy lds_input
		g_mb.error(ls_errore)
		return -1
		
	elseif li_ret = 0 then
		commit;
		
		if g_str.isnotempty(ls_errore) then g_mb.success( ls_errore)
		
	elseif li_ret>0 and g_str.isnotempty(ls_errore) then
		g_mb.warning( ls_errore)
		
	end if
	
end if









end event

type cb_annulla from commandbutton within w_lista_buffer_colli
integer x = 2839
integer y = 168
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

on clicked;close(parent)
end on

type cb_cerca from commandbutton within w_lista_buffer_colli
integer x = 2839
integer y = 272
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;


dw_lista.change_dw_current()

parent.postevent("pc_retrieve")


end event

