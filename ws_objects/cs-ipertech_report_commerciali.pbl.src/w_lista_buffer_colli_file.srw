﻿$PBExportHeader$w_lista_buffer_colli_file.srw
$PBExportComments$Finestra Report Attrezzature Semplici
forward
global type w_lista_buffer_colli_file from window
end type
type cb_annulla from commandbutton within w_lista_buffer_colli_file
end type
type cb_procedi from commandbutton within w_lista_buffer_colli_file
end type
type dw_lista from datawindow within w_lista_buffer_colli_file
end type
end forward

global type w_lista_buffer_colli_file from window
integer width = 4000
integer height = 2456
boolean titlebar = true
string title = "Visualizza File Scansioni"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
cb_annulla cb_annulla
cb_procedi cb_procedi
dw_lista dw_lista
end type
global w_lista_buffer_colli_file w_lista_buffer_colli_file

on w_lista_buffer_colli_file.create
this.cb_annulla=create cb_annulla
this.cb_procedi=create cb_procedi
this.dw_lista=create dw_lista
this.Control[]={this.cb_annulla,&
this.cb_procedi,&
this.dw_lista}
end on

on w_lista_buffer_colli_file.destroy
destroy(this.cb_annulla)
destroy(this.cb_procedi)
destroy(this.dw_lista)
end on

event open;
s_cs_xx_parametri				lstr_param


lstr_param = message.powerobjectparm


lstr_param.parametro_dw_1.rowscopy(1, lstr_param.parametro_dw_1.rowcount(), Primary!, dw_lista, 1, Primary!)



end event

type cb_annulla from commandbutton within w_lista_buffer_colli_file
integer x = 827
integer y = 44
integer width = 402
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;s_cs_xx_parametri			lstr_return

lstr_return.parametro_b_1 = false

closewithreturn(parent, lstr_return)
end event

type cb_procedi from commandbutton within w_lista_buffer_colli_file
integer x = 320
integer y = 44
integer width = 402
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Procedi"
end type

event clicked;s_cs_xx_parametri			lstr_return



lstr_return.parametro_b_1 = true

//lstr_return.parametro_dw_1.dataobject = dw_lista.dataobject
//dw_lista.rowscopy(1, dw_lista.rowcount(), Primary!, lstr_return.parametro_dw_1, 1, Primary!)

lstr_return.parametro_dw_1 = dw_lista

closewithreturn(parent, lstr_return)
end event

type dw_lista from datawindow within w_lista_buffer_colli_file
integer x = 27
integer y = 168
integer width = 3922
integer height = 2168
integer taborder = 10
string title = "none"
string dataobject = "d_lista_buffer_colli_file"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

