﻿$PBExportHeader$w_lista_buffer_confronto_carico.srw
$PBExportComments$Finestra Report Attrezzature Semplici
forward
global type w_lista_buffer_confronto_carico from w_cs_xx_principale
end type
type st_1 from statictext within w_lista_buffer_confronto_carico
end type
type dw_folder from u_folder within w_lista_buffer_confronto_carico
end type
type dw_lista from uo_cs_xx_dw within w_lista_buffer_confronto_carico
end type
type dw_selezione from uo_cs_xx_dw within w_lista_buffer_confronto_carico
end type
type cb_annulla from commandbutton within w_lista_buffer_confronto_carico
end type
type cb_cerca from commandbutton within w_lista_buffer_confronto_carico
end type
end forward

global type w_lista_buffer_confronto_carico from w_cs_xx_principale
integer width = 3026
integer height = 2228
string title = "Report Confronto Carico - Selezione"
st_1 st_1
dw_folder dw_folder
dw_lista dw_lista
dw_selezione dw_selezione
cb_annulla cb_annulla
cb_cerca cb_cerca
end type
global w_lista_buffer_confronto_carico w_lista_buffer_confronto_carico

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[], lw_vuoto[]

set_w_options(c_closenosave + c_autoposition + c_noresizewin)


dw_lista.set_dw_options(sqlca, &
                         pcca.null_object, &
                        c_nonew + &
					c_nomodify + &
					c_nodelete + &
					c_noenablenewonopen + &
					c_noenablemodifyonopen + &
					c_scrollparent + &
					c_disablecc, &
                         c_default )
			 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

iuo_dw_main = dw_lista

// *** folder													

lw_oggetti[1] = dw_lista
lw_oggetti[2] = dw_selezione
lw_oggetti[3] = cb_cerca
lw_oggetti[4] = cb_annulla
lw_oggetti[5] = st_1
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])

dw_folder.fu_foldercreate(1,1)

dw_folder.fu_selecttab(1)
													
// ***
end event

on w_lista_buffer_confronto_carico.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_folder=create dw_folder
this.dw_lista=create dw_lista
this.dw_selezione=create dw_selezione
this.cb_annulla=create cb_annulla
this.cb_cerca=create cb_cerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_folder
this.Control[iCurrent+3]=this.dw_lista
this.Control[iCurrent+4]=this.dw_selezione
this.Control[iCurrent+5]=this.cb_annulla
this.Control[iCurrent+6]=this.cb_cerca
end on

on w_lista_buffer_confronto_carico.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_folder)
destroy(this.dw_lista)
destroy(this.dw_selezione)
destroy(this.cb_annulla)
destroy(this.cb_cerca)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_sort(dw_selezione, &
									  "cod_operaio", sqlca, &
									  "anag_operai", "cod_operaio", "cognome + ' ' + nome", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_blocco = 'N'", &
									  " cognome ASC, nome ASC ")		  					  

end event

type st_1 from statictext within w_lista_buffer_confronto_carico
integer x = 73
integer y = 752
integer width = 2871
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
string text = "Doppio clic su una riga per visualizzare il report"
boolean focusrectangle = false
end type

type dw_folder from u_folder within w_lista_buffer_confronto_carico
integer x = 23
integer y = 20
integer width = 2949
integer height = 2080
integer taborder = 60
end type

event po_tabclicked;call super::po_tabclicked;//CHOOSE CASE i_SelectedTabName
//   CASE "Report"
//      SetFocus(dw_report_manut_eseguite)
//		
//   CASE "Selezione"
//      SetFocus(dw_sel_manut_eseguite)
//
//END CHOOSE
//
end event

type dw_lista from uo_cs_xx_dw within w_lista_buffer_confronto_carico
boolean visible = false
integer x = 50
integer y = 856
integer width = 2889
integer height = 1208
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_lista_buffer_confronto_carico"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event buttonclicked;call super::buttonclicked;long ll_progressivo

if row>=0 then
else
	return
end if

choose case dwo.name
	case "b_elimina"
		ll_progressivo = getitemnumber(row, "progressivo")
		
		if ll_progressivo>0 then
			
			if g_mb.messagebox("APICE","Eliminare i dati relativi al confronto di carico selezionato?", Question!, YesNo!, 1)=1 then
			else
				return
			end if
			
			delete from tab_confronto_carico
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						progressivo=:ll_progressivo;
						
			if sqlca.sqlcode<0 then
				g_mb.messagebox("APICE","Errore in cancellazione. "+sqlca.sqlerrtext, StopSign!)
				rollback;
				return
			end if
			
			commit;
			cb_cerca.postevent(clicked!)
		end if
		
end choose
end event

event doubleclicked;call super::doubleclicked;long ll_progressivo

if row>0 then
	ll_progressivo = getitemnumber(row, "progressivo")
	if ll_progressivo>0 then
		
		//openwithparm(w_report_buffer_confronto_carico, string(ll_progressivo))
		
		s_cs_xx.parametri.parametro_s_10 = string(ll_progressivo)
		
		window_open(w_report_buffer_confronto_carico, -1)
	end if
else
	return
end if
end event

type dw_selezione from uo_cs_xx_dw within w_lista_buffer_confronto_carico
integer x = 50
integer y = 200
integer width = 1623
integer height = 492
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_report_buffer_carico_sel"
boolean border = false
end type

type cb_annulla from commandbutton within w_lista_buffer_confronto_carico
integer x = 562
integer y = 616
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

on clicked;close(parent)
end on

type cb_cerca from commandbutton within w_lista_buffer_confronto_carico
integer x = 955
integer y = 616
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;//string	ls_null, ls_da_attrezzatura, ls_a_attrezzatura, ls_cat_attrezzature, ls_cod_area_aziendale, ls_cod_reparto, ls_where, ls_sql, ls_flag_mezzi, ls_des_area
//long	ll_errore
//setnull(ls_null)

datetime ldt_data_rif
long ll_progressivo, ll_tot
string ls_cod_operaio

dw_lista.reset()

dw_selezione.accepttext()

ls_cod_operaio = dw_selezione.getitemstring(1,"cod_operaio")
ll_progressivo = dw_selezione.getitemnumber(1,"progressivo")
ldt_data_rif = dw_selezione.getitemdatetime(1,"data_creazione")

if ls_cod_operaio="" then setnull(ls_cod_operaio)
if ll_progressivo<=0 then setnull(ll_progressivo)
if year(date(ldt_data_rif))<=1950 then setnull(ldt_data_rif)


ll_tot = dw_lista.retrieve(s_cs_xx.cod_azienda, ldt_data_rif, ll_progressivo, ls_cod_operaio)

if ll_tot>0 then
	
end if

//ls_da_attrezzatura = dw_selezione.getitemstring(1,"rs_da_cod_attrezzatura")
//ls_a_attrezzatura = dw_selezione.getitemstring(1,"rs_a_cod_attrezzatura")
//ls_cat_attrezzature = dw_selezione.getitemstring(1,"rs_cod_cat_attrezzatura")
//ls_cod_area_aziendale = dw_selezione.getitemstring(1,"rs_cod_area_aziendale")
//ls_cod_reparto = dw_selezione.getitemstring(1,"rs_cod_reparto")
//ls_flag_mezzi = dw_selezione.getitemstring( 1, "flag_includi_mezzi")
//
//if isnull(ls_flag_mezzi) then ls_flag_mezzi = "N"
//
//ls_where = " WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
//
//if not isnull(ls_cod_area_aziendale) and ls_cod_area_aziendale <> "" then
//	ls_where += " AND cod_area_aziendale = '" + ls_cod_area_aziendale + "' "
//end if
//
//if not isnull(ls_cod_reparto) and ls_cod_reparto <> "" then
//	ls_where += " AND cod_reparto = '" + ls_cod_reparto + "' "
//end if
//
//if not isnull(ls_cat_attrezzature) and ls_cat_attrezzature <> "" then
//	ls_where += " AND cod_cat_attrezzature = '" + ls_cat_attrezzature + "' "
//end if
//
//if not isnull(ls_da_attrezzatura) and ls_da_attrezzatura <> "" then
//	ls_where += " AND cod_attrezzatura >= '" + ls_da_attrezzatura + "' "
//end if
//
//if not isnull(ls_a_attrezzatura) and ls_a_attrezzatura <> "" then
//	ls_where += " AND cod_attrezzatura = '" + ls_a_attrezzatura + "' "
//end if
//
//if not isnull(ls_flag_mezzi) and ls_flag_mezzi = "N" then
//	ls_where += " AND cod_reparto <> 'M' "
//end if
//
//ls_where += " ORDER BY cod_reparto ASC, cod_cat_attrezzature ASC, cod_attrezzatura ASC "
//
//ls_sql = "SELECT anag_attrezzature.cod_reparto,   " + &
//			"			anag_attrezzature.cod_cat_attrezzature,  " + &   
//			"			anag_attrezzature.cod_attrezzatura,     " + &
//			"			anag_attrezzature.descrizione,     " + &
//			"			anag_attrezzature.flag_primario,    " + &
//			"   			'' as attivita,      " + &
//	         "   			'' as fornitore,      " + &
//         	"   			'' as esecutore     " + &
//			"FROM  anag_attrezzature   " +  ls_where
//
//dw_report.SetSQLSelect( ls_sql)
//
//dw_report.resetupdate()
//
//ll_errore = dw_report.retrieve()
//
//if ll_errore < 0 then
//   pcca.error = c_fatal
//else
//	
//	dw_report.setredraw(true)
//	
//	if not isnull(ls_cod_area_aziendale) and ls_cod_area_aziendale <> "" then
//		
//		select des_area
//		into	:ls_des_area
//		from	tab_aree_aziendali
//		where	cod_azienda = :s_cs_xx.cod_azienda and
//				cod_area_aziendale = :ls_cod_area_aziendale;
//				
//		dw_report.object.t_impianto.text = ls_des_area
//	else
//		dw_report.object.t_impianto.text = ''
//	end if
//	dw_report.Object.DataWindow.Print.Preview = 'Yes'
//	dw_folder.fu_selecttab(2)
//	dw_report.change_dw_current()	
//	
//end if
end event

