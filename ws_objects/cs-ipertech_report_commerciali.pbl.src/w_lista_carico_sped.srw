﻿$PBExportHeader$w_lista_carico_sped.srw
$PBExportComments$REPORT RIGHE ORDINI
forward
global type w_lista_carico_sped from w_cs_xx_principale
end type
type dw_colli from datawindow within w_lista_carico_sped
end type
type dw_ordini_2 from datawindow within w_lista_carico_sped
end type
type dw_spedizione from datawindow within w_lista_carico_sped
end type
type dw_barcode from datawindow within w_lista_carico_sped
end type
type dw_lista from datawindow within w_lista_carico_sped
end type
type dw_ricerca from uo_cs_xx_dw within w_lista_carico_sped
end type
type dw_ordini from datawindow within w_lista_carico_sped
end type
type dw_folder from u_folder within w_lista_carico_sped
end type
end forward

global type w_lista_carico_sped from w_cs_xx_principale
integer width = 3831
integer height = 2704
string title = "Carico Spedizioni"
dw_colli dw_colli
dw_ordini_2 dw_ordini_2
dw_spedizione dw_spedizione
dw_barcode dw_barcode
dw_lista dw_lista
dw_ricerca dw_ricerca
dw_ordini dw_ordini
dw_folder dw_folder
end type
global w_lista_carico_sped w_lista_carico_sped

type variables
string is_old_sintax

string is_reset = "RESET"

string is_CSB

datetime idt_data_spedizione = datetime(today(), 00:00:00)
end variables

forward prototypes
public function integer wf_ricerca ()
public function integer wf_reset ()
public subroutine wf_reset_dw_barcode ()
public subroutine wf_proteggi_dw ()
public function integer wf_conferma ()
public function integer wf_cancella_spedizione ()
public function integer wf_controlla_per_evasione ()
public function integer wf_inserisci_riga (string fs_barcode)
public function boolean wf_controlla_barcode (string fs_barcode)
public function integer wf_get_data_barcode (string fs_barcode, ref long fl_anno_registrazione, ref long fl_num_registrazione, ref long fl_prog_riga_ord_ven, ref string fs_cod_cliente)
public function integer wf_retrieve_righe (long fl_row, datawindow fdw_data_source, datawindow fdw_data_retrieve, boolean fb_colli)
public function integer wf_imposta_progressivo (string fs_barcode, ref long fl_progressivo, ref string fs_msg)
public function integer wf_controlla_colli_altra_spedizione (string fs_barcode, long fl_progressivo, ref string fs_colli[], ref string fs_msg)
public function integer wf_controlla_per_pck_list (ref datetime fdt_data_spedizione)
public function integer wf_reset (string fs_barcode)
end prototypes

public function integer wf_ricerca ();string ls_cod_cliente, ls_barcode, ls_sql, ls_msg
long ll_progressivo
datetime ldt_data_spedizione

dw_ricerca.accepttext()

ls_cod_cliente = dw_ricerca.getitemstring(1, "cod_cliente")
ldt_data_spedizione = dw_ricerca.getitemdatetime(1, "data_spedizione")

ll_progressivo = dw_ricerca.getitemnumber(1, "progressivo")

if not isnull(ldt_data_spedizione) then ldt_data_spedizione = datetime(date(ldt_data_spedizione), 00:00:00)

ls_barcode = dw_ricerca.getitemstring(1, "barcode")

//if not isnull(ls_barcode) and ls_barcode<>"" then
//	
//	//verifica i caratteri speciali delimitatori
//	if f_converti_barcode(ls_barcode, ls_msg) < 0 then
//		g_mb.error(ls_msg)
//		return -1
//	end if			
//	//------------------------------------------------------
//	
//	if isnumber(ls_barcode) then
//		//OK
//	else
//		g_mb.error("Barcode di ricerca non numerico!")
//		return -1
//	end if
//end if

ls_sql = is_old_sintax
ls_sql += " where cod_azienda='"+s_cs_xx.cod_azienda+"' "

if ls_cod_cliente<>"" and not isnull(ls_cod_cliente) then ls_sql += "and cod_cliente='"+ls_cod_cliente+"' "
if not isnull(ldt_data_spedizione) then ls_sql += "and data_spedizione='"+string(ldt_data_spedizione, s_cs_xx.db_funzioni.formato_data)+"' "

if not isnull(ls_barcode) and ls_barcode<>"" then ls_sql += "and barcode='"+ls_barcode+"' "

if ll_progressivo>0 then ls_sql += "and progressivo="+string(ll_progressivo)+" "

dw_lista.setsqlselect(ls_sql)
if dw_lista.retrieve() > 0 then
	dw_lista.selectrow(0, false)
	dw_lista.selectrow(1, true)
	
	wf_retrieve_righe(1, dw_lista, dw_ordini_2, true)
else
	wf_retrieve_righe(0, dw_lista, dw_ordini_2, true)
end if

return 1
end function

public function integer wf_reset ();long ll_index, ll_row, ll_anno_reg, ll_num_reg, ll_prog_riga, ll_progressivo
string ls_msg, ls_barcode
//datetime ldt_data_spedizione

ll_row = dw_lista.rowcount()

if ll_row>0 then
else
	g_mb.show("Nessun dato presente nella lista!")
	return 1
end if

if g_mb.confirm("Eliminare tutte le righe della lista dalla spedizione?") then
	for ll_index= 1 to ll_row
		ls_barcode = dw_lista.getitemstring(ll_index, "barcode")
		//ldt_data_spedizione = dw_lista.getitemdatetime(ll_index, "data_spedizione")
		ll_anno_reg = dw_lista.getitemnumber(ll_index, "anno_registrazione")
		ll_num_reg = dw_lista.getitemnumber(ll_index, "num_registrazione")
		ll_prog_riga = dw_lista.getitemnumber(ll_index, "prog_riga_ord_ven")
		ll_progressivo = dw_lista.getitemnumber(ll_index, "progressivo")
		
		//prima elimino i colli corrispondenti messi in spedizione
		delete from tab_lista_carico_sped_colli
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					barcode=:ls_barcode and
					progressivo=:ll_progressivo;
					
		if sqlca.sqlcode<0 then
			ls_msg = string(ll_anno_reg)+"/"+string(ll_num_reg)
			if ll_prog_riga>0 then ls_msg+= "/"+string(ll_prog_riga)
			
			ls_msg +="~r~n"+sqlca.sqlerrtext
			
			g_mb.error("Errore durante la cancellazione dei colli in spedizione della riga! barcode "+ls_barcode+" - Ordine n° "+ls_msg)
			rollback;
			return -1
		end if
		
		
		delete from tab_lista_carico_spedizioni
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					barcode=:ls_barcode and
					progressivo=:ll_progressivo;
					
		if sqlca.sqlcode<0 then
			ls_msg = string(ll_anno_reg)+"/"+string(ll_num_reg)
			if ll_prog_riga>0 then ls_msg+= "/"+string(ll_prog_riga)
			
			ls_msg +="~r~n"+sqlca.sqlerrtext
			
			g_mb.error("Errore durante la cancellazione della riga dalla spedizione! barcode "+ls_barcode+" - Ordine n° "+ls_msg)
			rollback;
			return -1
		end if
	next
	
	//se arrivi fin qui fai commit e retrieve
	commit;
	wf_ricerca()
	g_mb.show("Operazione effettuata con successo!")
	
else
	g_mb.show("Operazione annullata!")
end if

return 1
end function

public subroutine wf_reset_dw_barcode ();dw_barcode.reset()
dw_barcode.insertrow(0)

dw_barcode.setitem(dw_spedizione.getrow(), "data_spedizione", idt_data_spedizione)

dw_barcode.setfocus()
end subroutine

public subroutine wf_proteggi_dw ();string ls_count, ls_col_index, ls_ret, ls_column_name
long ll_index

ls_count = dw_lista.Describe("DataWindow.Column.Count")

for ll_index = 1 to integer(ls_count)
	ls_col_index = "#"+string(ll_index)
	ls_column_name =dw_lista.Describe(ls_col_index+".name")
	
	ls_ret = dw_lista.modify(ls_column_name+'.protect=1')
	ls_ret = dw_spedizione.modify(ls_column_name+'.protect=1')
next
end subroutine

public function integer wf_conferma ();long ll_rows, ll_index, ll_progressivo, ll_anno_reg, ll_num_reg, ll_prog_riga, ll_ret, ll_index2
string ls_msg, ls_barcode, ls_cod_cliente, ls_colli[], ls_vuoto[]
datetime ldt_data_spedizione
uo_produzione luo_prod

ll_rows = dw_spedizione.rowcount()

if ll_rows>0 then
else
	g_mb.show("Nessuna riga presente da mettere in spedizione!")
	return 0
end if

if not g_mb.confirm("Confermare le righe da mettere in spedizione?") then return 0

//calcola il max del progressivo per ogni barcode
for ll_index = 1 to ll_rows
	ls_barcode = dw_spedizione.getitemstring(ll_index, "barcode")
	if wf_imposta_progressivo(ls_barcode, ll_progressivo, ls_msg) < 0 then
		g_mb.error(ls_msg)
	
		rollback;
		return -1
	end if
	
	dw_spedizione.setitem(ll_index, "progressivo", ll_progressivo)
	
	ll_anno_reg = dw_spedizione.getitemnumber(ll_index, "anno_registrazione")
	ll_num_reg = dw_spedizione.getitemnumber(ll_index, "num_registrazione")
	ll_prog_riga = dw_spedizione.getitemnumber(ll_index, "prog_riga_ord_ven")
	ldt_data_spedizione = dw_spedizione.getitemdatetime(ll_index, "data_spedizione")
	ls_cod_cliente = dw_spedizione.getitemstring(ll_index, "cod_cliente")
	
	//inserisci
	insert into tab_lista_carico_spedizioni
	(	cod_azienda,
		barcode,
		progressivo,
		data_spedizione,
		cod_cliente,
		anno_registrazione,
		num_registrazione,
		prog_riga_ord_ven,
		anno_documento,
		num_documento,
		tipo_documento	)
	values	(	:s_cs_xx.cod_azienda,
					:ls_barcode,
					:ll_progressivo,
					:ldt_data_spedizione,
					:ls_cod_cliente,
					:ll_anno_reg,
					:ll_num_reg,
					:ll_prog_riga,
					null,
					null,
					null);
	
	if sqlca.sqlcode<0 then
		g_mb.error("Errore in inserimento spedizione: "+sqlca.sqlerrtext)
		rollback;
		return -1
	end if
	
	ls_colli = ls_vuoto
	
	luo_prod = create uo_produzione
	ll_ret = luo_prod.uof_leggi_colli_ordine(ll_anno_reg, ll_num_reg, ll_prog_riga, ls_colli[], ls_msg)
	destroy luo_prod;
	
	if ll_ret<0 then
		//in fs_msg il messaggio di errore
		g_mb.error(ls_msg)
		rollback;
		return -1
	end if
	
	
	/*
	//ulteriore controllo se qualche codice collo esiste in un'altra spedizione --------------------------------
	ll_ret = wf_controlla_colli_altra_spedizione(ls_barcode, ll_progressivo, ls_colli[], ls_msg)
	
	if ll_ret<0 then
		//in fs_msg il messaggio
		g_mb.error(ls_msg)
		return -1
		
	elseif ll_ret=2 then
		//dai messaggio di alert e continua
		g_mb.show(ls_msg)
	else
		//tutto OK
	end if
	//------------------------------------------------------------------------------------------------------------------------------------------------
	*/
	
	
	for ll_index2 = 1 to upperbound(ls_colli[])
		
		insert into tab_lista_carico_sped_colli  
				( cod_azienda,   
				  barcode,   
				  progressivo,   
				  barcode_collo )  
		values ( :s_cs_xx.cod_azienda,   
				  :ls_barcode,   
				  :ll_progressivo,   
				  :ls_colli[ll_index2]);

		if sqlca.sqlcode<0 then
			ls_msg = "Errore in inserimento tab_lista_carico_sped_colli " + sqlca.sqlerrtext
			g_mb.show(ls_msg)
			rollback;
			return -1
		end if
		
	next
next


//se arrivi fin qui fai commit
commit;
g_mb.show("Operazione confermata con successo!")
	
//reset della dw
dw_spedizione.reset()

return 1

end function

public function integer wf_cancella_spedizione ();long ll_index, ll_rows, ll_progressivo
string ls_barcode

ll_rows = dw_lista.rowcount()

if ll_rows>0 then
else
	 g_mb.show("Nessun dato presente in lista da eliminare!")
	 return 0
end if

if g_mb.confirm("Effettuare la cancellazione dei dati in spedizione presenti a video?") then
else
	return 0
end if

for ll_index=1 to ll_rows
	ll_progressivo = dw_lista.getitemnumber(ll_index, "progressivo")
	ls_barcode = dw_lista.getitemstring(ll_index, "barcode")
	
	//cancella i colli dalla spedizione
	delete from tab_lista_carico_sped_colli
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				barcode=:ls_barcode and
				progressivo=:ll_progressivo;
				
	if sqlca.sqlcode<0 then
		g_mb.error("Errore in cancellazione colli in spedizione per la riga "+ls_barcode+", e progressivo "+string(ll_progressivo))
		rollback;
		
		//rifai la retrieve
		dw_ricerca.postevent("ue_ricerca")
		return -1
	end if
	
	//cancella la spedizione
	delete from tab_lista_carico_spedizioni
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				barcode=:ls_barcode and
				progressivo=:ll_progressivo;
				
	if sqlca.sqlcode<0 then
		g_mb.error("Errore in cancellazione spedizione per la riga "+ls_barcode+", e progressivo "+string(ll_progressivo))
		rollback;
		
		//rifai la retrieve
		dw_ricerca.postevent("ue_ricerca")
		return -1
	end if
	
next

//se arrivi fin qui fai il commit + retrieve
commit;
g_mb.show("Operazione effettuata con successo!")

//rifai la retrieve
dw_ricerca.postevent("ue_ricerca")

return 1


end function

public function integer wf_controlla_per_evasione ();long ll_rows, ll_index, ll_numero_documento
string ls_cod_cliente, ls_temp, ls_barcode

dw_lista.accepttext()

ll_rows = dw_lista.rowcount()

if ll_rows>0 then
else
	g_mb.show("Nessuna riga da evadere!")
	return 0
end if

for ll_index=1 to ll_rows
	ls_temp = dw_lista.getitemstring(ll_index, "cod_cliente")
	ls_barcode = dw_lista.getitemstring(ll_index, "barcode")
	
	//mantieni il primo cliente
	if ll_index=1 then ls_cod_cliente = ls_temp
	
	if ls_temp<>ls_cod_cliente then
		g_mb.error("ATTENZIONE: si possono evadere gli ordini di un cliente per volta! Effettuare prima una ricerca per cliente!")
		return 0
	end if
	
	ll_numero_documento = dw_lista.getitemnumber(ll_index, "num_documento")
	if ll_numero_documento>0 then
		//riga già spedita
		g_mb.error("ATTENZIONE: la riga "+ls_barcode+" risulta già spedita!")
		return 0
	end if
next

return 1
end function

public function integer wf_inserisci_riga (string fs_barcode);long		ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_new
string		ls_cod_cliente

if isnull(idt_data_spedizione) or year(date(idt_data_spedizione))<=1950 then
	g_mb.error("E' necessario impostare prima la data di spedizione!")
	return -1
end if

if not wf_controlla_barcode(fs_barcode) then
	return -1
end if

if wf_get_data_barcode(fs_barcode, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_cliente) < 0 then
	//fuori, messaggio già dato
	return -1
else
	
	//inserisco la riga
	ll_new = dw_spedizione.insertrow(0)

	dw_spedizione.setitem(ll_new, "barcode", fs_barcode)
	dw_spedizione.setitem(ll_new, "cod_azienda", s_cs_xx.cod_azienda)
	dw_spedizione.setitem(ll_new, "data_spedizione", idt_data_spedizione)
	
	//faccio i setitem
	dw_spedizione.setitem(ll_new, "anno_registrazione", ll_anno_registrazione)
	dw_spedizione.setitem(ll_new, "num_registrazione", ll_num_registrazione)
	dw_spedizione.setitem(ll_new, "prog_riga_ord_ven", ll_prog_riga_ord_ven)
	//dw_spedizione.setitem(ll_new, "num_colli", ll_num_colli)
	dw_spedizione.setitem(ll_new, "cod_cliente", ls_cod_cliente)
	
	//il progressivo lo calcolo in fase di conferma dati (select max)
	
	dw_spedizione.selectrow(0, false)
	dw_spedizione.selectrow(ll_new, true)
	wf_retrieve_righe(ll_new, dw_spedizione, dw_ordini, false)
end if

return 1
end function

public function boolean wf_controlla_barcode (string fs_barcode);long ll_index, ll_rows, ll_count
string ls_data
datetime ldt_data_sped, ldt_data_sped_check

ldt_data_sped = dw_barcode.getitemdatetime(dw_barcode.getrow(), "data_spedizione")

//controlla tra le righe già presenti
ll_rows = dw_spedizione.rowcount()
for ll_index=1 to ll_rows
	ls_data = dw_spedizione.getitemstring(ll_index, "barcode")
	
	if fs_barcode=ls_data then
		//dato già presente
		g_mb.show("Questo barcode è già presente in lista!")
		
		return false
	end if
next

select barcode, data_spedizione
into :ls_data, :ldt_data_sped_check
from tab_lista_carico_spedizioni
where 	cod_azienda=:s_cs_xx.cod_azienda and
			barcode=:fs_barcode;
			
if sqlca.sqlcode<0 then
	g_mb.error("Errore in controllo esistenza barcode nelle tabella liste spedizioni: "+sqlca.sqlerrtext)
		
	return false
	
elseif sqlca.sqlcode=0 then
	//dato già presente
	
	if date(ldt_data_sped) = date(ldt_data_sped_check) then
		if g_mb.confirm("Il barcode "+fs_barcode+" è già presente tra le spedizioni nella stessa data "+string(date(ldt_data_sped_check), "dd-mm-yyyy")+"! Vuoi continuare?") then
			//confermato
		else
			return false
		end if
	else
		if g_mb.confirm("Il barcode "+fs_barcode+" è già presente tra le spedizioni in data "+string(date(ldt_data_sped_check), "dd-mm-yyyy")+"! Vuoi continuare?") then
			//confermato
		else
			return false
		end if
	end if

end if

return true
end function

public function integer wf_get_data_barcode (string fs_barcode, ref long fl_anno_registrazione, ref long fl_num_registrazione, ref long fl_prog_riga_ord_ven, ref string fs_cod_cliente);long		ll_count
string		ls_anno_registrazione, ls_num_registrazione, ls_prog_riga_ord_ven

//il barcode è del tipo OyyyyNNNNNNpppp  (relativo alla riga di ordine)

fs_barcode = upper(fs_barcode)
if len(fs_barcode) <> 15 then
	g_mb.messagebox("APICE","Formato BARCODE errato")
	dw_barcode.setfocus()
	dw_barcode.SelectText(1,999999)
	return -1
end if

ls_anno_registrazione = mid(fs_barcode,2,4)
ls_num_registrazione  = mid(fs_barcode,6,6)
ls_prog_riga_ord_ven  = mid(fs_barcode,12,4)

fl_anno_registrazione = long(ls_anno_registrazione)
fl_num_registrazione = long(ls_num_registrazione)
fl_prog_riga_ord_ven = long(ls_prog_riga_ord_ven)


select cod_cliente
into :fs_cod_cliente
from tes_ord_ven
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :fl_anno_registrazione and
			num_registrazione = :fl_num_registrazione;
			
if sqlca.sqlcode < 0 then 
	g_mb.error("Errore sul DB in ricerca cliente ordine: " + sqlca.sqlerrtext)
	return -1
	
elseif sqlca.sqlcode = 100 or fs_cod_cliente="" or isnull(fs_cod_cliente) then
	g_mb.error("Cliente non trovato per l'ordine "+string(fl_anno_registrazione) + "/"+string(fl_num_registrazione) +" oppure l'ordine è inesistente!")
	return -1
	
end if

//CONTROLLA LA ESISTENZA DELLA RIGA
select count(*)
into :ll_count
from det_ord_ven
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno_registrazione and
			num_registrazione=:fl_num_registrazione and
			prog_riga_ord_ven =:fl_prog_riga_ord_ven ;

if ll_count>0 then
else
	g_mb.error("Riga non trovata in corrispondenza del barcode "+fs_barcode)
	return -1
end if

return 1
end function

public function integer wf_retrieve_righe (long fl_row, datawindow fdw_data_source, datawindow fdw_data_retrieve, boolean fb_colli);long ll_anno_reg, ll_num_reg, ll_prog_riga_ord_ven, ll_progressivo
string ls_barcode

if fl_row<=0 then
	ll_anno_reg = -1
	ll_num_reg = -1
	ll_prog_riga_ord_ven = -1
	
else
	ll_anno_reg = fdw_data_source.getitemnumber(fl_row, "anno_registrazione")
	ll_num_reg = fdw_data_source.getitemnumber(fl_row, "num_registrazione")
	ll_prog_riga_ord_ven = fdw_data_source.getitemnumber(fl_row, "prog_riga_ord_ven")
	
end if

fdw_data_retrieve.retrieve(s_cs_xx.cod_azienda, ll_anno_reg, ll_num_reg, ll_prog_riga_ord_ven)

if fb_colli and fl_row>0 then
	ls_barcode = fdw_data_source.getitemstring(fl_row, "barcode")
	//ldt_data_spedizione = fdw_data_source.getitemdatetime(fl_row, "data_spedizione")
	ll_progressivo = fdw_data_source.getitemnumber(fl_row, "progressivo")
	
	dw_colli.retrieve(s_cs_xx.cod_azienda, ls_barcode, ll_progressivo)
	
else
	//potrebbe essere utile chiamare una funzione che fa insert row nella dw_colli (soloper visualizzare i colli disponibili)
	//in base al barcode di riga OYYYYnnnnnnPPPP, ma non ho il tempo di farla
	
	
end if

return 1
end function

public function integer wf_imposta_progressivo (string fs_barcode, ref long fl_progressivo, ref string fs_msg);string ls_barcode

select max(progressivo)
into :fl_progressivo
from tab_lista_carico_spedizioni
where 	cod_azienda=:s_cs_xx.cod_azienda and
			barcode=:fs_barcode;
			
if sqlca.sqlcode<0 then
	fs_msg="Errore in calcolo max progressivo in tab_lista_carico_spedizioni "+sqlca.sqlerrtext
	return -1
end if

if isnull(fl_progressivo) then fl_progressivo=0

fl_progressivo += 1

return 1
end function

public function integer wf_controlla_colli_altra_spedizione (string fs_barcode, long fl_progressivo, ref string fs_colli[], ref string fs_msg);long ll_tot_colli, ll_index, ll_count, ll_indice
string ls_codice_collo, ls_colli_copia[], ls_alert, ls_vuoto[]
boolean lb_alert = false

ls_colli_copia = fs_colli
fs_colli = ls_vuoto

ls_alert = "I seguenti colli sono presenti in altre spedizione. Sono stati quindi esclusi dalla spedizione corrente!"
ll_tot_colli = upperbound(ls_colli_copia[])
ll_indice = 0

for ll_index=1 to ll_tot_colli
	ls_codice_collo = ls_colli_copia[ll_index]
	
	select count(*)
	into :ll_count
	from tab_lista_carico_sped_colli
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				barcode <> :fs_barcode and
				progressivo <> :fl_progressivo and
				barcode_collo=:ls_codice_collo;
				
	if sqlca.sqlcode<0 then
		lb_alert = false
		fs_msg = "Errore in controllo esistenza codice collo in altra spedizione! "+sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ll_count) then ll_count = 0
	
	if ll_count=0 then
		//non presente
		ll_indice += 1
		fs_colli[ll_indice] = ls_codice_collo
	else
		//collo già presente in altra spedizione, segnala ed escludi dalla spedizione
		lb_alert = true
		ls_alert += ls_codice_collo + " "
	end if
	
next

if lb_alert then
	fs_msg = ls_alert
	return 2
end if

return 1
end function

public function integer wf_controlla_per_pck_list (ref datetime fdt_data_spedizione);long ll_rows, ll_index, ll_numero_documento
datetime ldt_data_spedizione, ldt_data_sped_hold

dw_lista.accepttext()

ll_rows = dw_lista.rowcount()

if ll_rows>0 then
else
	g_mb.show("Nessuna riga da elaborare!")
	return 0
end if

for ll_index=1 to ll_rows
	ldt_data_spedizione = dw_lista.getitemdatetime(ll_index, "data_spedizione")
	
	//mantieni la prima data spedizione
	if ll_index=1 then ldt_data_sped_hold = ldt_data_spedizione
	
	if ldt_data_spedizione<>ldt_data_sped_hold then
		g_mb.error("ATTENZIONE: alcune righe hanno data spedizione differenti! si consigli di filtrare prima le righe per data spedizione, generando una packing list su un astessa data spedizione.")
		return 0
	end if
	
next

fdt_data_spedizione = ldt_data_sped_hold
return 1
end function

public function integer wf_reset (string fs_barcode);long ll_count


if isnull(fs_barcode) or fs_barcode="" then return 1

fs_barcode = upper(fs_barcode)
if len(fs_barcode) <> 15 then
	g_mb.error("Formato BARCODE errato")
	return 1
end if

select count(*)
into :ll_count
from tab_lista_carico_spedizioni
where cod_azienda=:s_cs_xx.cod_azienda and
		barcode=:fs_barcode;
		
if sqlca.sqlcode<0 then
	g_mb.error("Errore durante il count in tab_lista_carico_spedizioni per questo barcode "+sqlca.sqlerrtext)
	return -1
end if

if isnull(ll_count) or ll_count=0 then
	g_mb.error("Nessun dato presente in spedizione per questo barcode.")
	return -1
end if

if ll_count > 1 then
	if g_mb.confirm("La riga corrispondente a questo barcode è presente più di una volta in spedizione! Se prosegui la riga verrà rimossa in più punti. Procedo?") then
	else
		g_mb.error("Operazione annullata")
		return 1
	end if
	
else
	
	if g_mb.confirm("Eliminare la riga corrispondente al barcode dalla spedizione?") then
	else
		g_mb.error("Operazione annullata")
		return 1
	end if
end if

//se arrivi fin qui procedi

//prima elimino i colli corrispondenti messi in spedizione per questo barcode di riga
delete from tab_lista_carico_sped_colli
where 	cod_azienda=:s_cs_xx.cod_azienda and
			barcode=:fs_barcode;

if sqlca.sqlcode<0 then
	g_mb.error("Errore durantela cancellazione in tab_lista_carico_sped_colli - "+sqlca.sqlerrtext)
	rollback;
	return -1
end if

//poi elimino la spedizione della riga
delete from tab_lista_carico_spedizioni
where 	cod_azienda=:s_cs_xx.cod_azienda and
			barcode=:fs_barcode;
			
if sqlca.sqlcode<0 then
	g_mb.error("Errore durante la cancellazione della riga dalla spedizione! "+sqlca.sqlerrtext)
	rollback;
	return -1
end if

//se arrivi fin qui fai commit e retrieve
commit;
wf_ricerca()
g_mb.show("Operazione effettuata con successo!")

return 1

end function

on w_lista_carico_sped.create
int iCurrent
call super::create
this.dw_colli=create dw_colli
this.dw_ordini_2=create dw_ordini_2
this.dw_spedizione=create dw_spedizione
this.dw_barcode=create dw_barcode
this.dw_lista=create dw_lista
this.dw_ricerca=create dw_ricerca
this.dw_ordini=create dw_ordini
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_colli
this.Control[iCurrent+2]=this.dw_ordini_2
this.Control[iCurrent+3]=this.dw_spedizione
this.Control[iCurrent+4]=this.dw_barcode
this.Control[iCurrent+5]=this.dw_lista
this.Control[iCurrent+6]=this.dw_ricerca
this.Control[iCurrent+7]=this.dw_ordini
this.Control[iCurrent+8]=this.dw_folder
end on

on w_lista_carico_sped.destroy
call super::destroy
destroy(this.dw_colli)
destroy(this.dw_ordini_2)
destroy(this.dw_spedizione)
destroy(this.dw_barcode)
destroy(this.dw_lista)
destroy(this.dw_ricerca)
destroy(this.dw_ordini)
destroy(this.dw_folder)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[], lw_vuoto[]

set_w_options(c_noenablepopup)

lw_oggetti[1] = dw_ricerca
lw_oggetti[2] =dw_lista
lw_oggetti[3] = dw_ordini_2
lw_oggetti[4] = dw_colli
dw_folder.fu_assigntab(2, "Ricerca-Reset", lw_oggetti[])
lw_oggetti = lw_vuoto

lw_oggetti[1] = dw_spedizione
lw_oggetti[2] = dw_barcode
lw_oggetti[3] = dw_ordini

dw_folder.fu_assigntab(1, "Spedizione", lw_oggetti[])
dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)

dw_ricerca.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
								 
save_on_close(c_socnosave)

dw_lista.settransobject(sqlca)
dw_spedizione.settransobject(sqlca)
dw_ordini.settransobject(sqlca)
dw_ordini_2.settransobject(sqlca)
dw_colli.settransobject(sqlca)

is_old_sintax = dw_lista.getsqlselect()

idt_data_spedizione = datetime(today(), 00:00:00)
wf_reset_dw_barcode()

//lettura parametro aziendale Carattere Speciale barcode (specifica Carico ordini con palmare)
select stringa
into   :is_CSB
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='CSB' and 
		flag_parametro='S';

if sqlca.sqlcode < 0 then
	messagebox("SEP","Errore in ricerca del parametro CSB in tabella parametri azienda: " + sqlca.sqlerrtext,stopsign!)
	is_CSB = ""
	
elseif sqlca.sqlcode=100 then
	is_CSB = ""
end if

if isnull(is_CSB) then is_CSB =""
//fine modifica ------------------------------------------------

wf_proteggi_dw()
end event

type dw_colli from datawindow within w_lista_carico_sped
integer x = 2930
integer y = 1996
integer width = 818
integer height = 580
integer taborder = 50
string title = "none"
string dataobject = "d_lista_colli_ordini_sped"
boolean vscrollbar = true
boolean livescroll = true
end type

type dw_ordini_2 from datawindow within w_lista_carico_sped
integer x = 27
integer y = 1996
integer width = 2889
integer height = 580
integer taborder = 40
boolean bringtotop = true
string title = "none"
string dataobject = "d_lista_righe_ordini_sped"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type dw_spedizione from datawindow within w_lista_carico_sped
integer x = 27
integer y = 448
integer width = 3721
integer height = 1520
integer taborder = 40
boolean bringtotop = true
string title = "none"
string dataobject = "d_lista_carico_sped"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event doubleclicked;this.accepttext()

if row > 0 then
	if g_mb.confirm("Eliminare la riga selezionata?") then deleterow(row)
	
end if
end event

event rowfocuschanged;if currentrow > 0 then
	selectrow(0, false)
	
	selectrow(currentrow, true)
	
	wf_retrieve_righe(currentrow, dw_spedizione, dw_ordini, false)
else
	wf_retrieve_righe(0, dw_spedizione, dw_ordini, false)
end if
end event

type dw_barcode from datawindow within w_lista_carico_sped
event ue_set_data_spedizione ( )
integer x = 27
integer y = 132
integer width = 3721
integer height = 284
integer taborder = 30
boolean bringtotop = true
string title = "none"
string dataobject = "d_lista_carico_sped_nuovo"
boolean livescroll = true
end type

event ue_set_data_spedizione();
//questo evento memorizza il valore della nuova data
idt_data_spedizione = dw_barcode.getitemdatetime(dw_barcode.getrow(), "data_spedizione")

return
end event

event itemchanged;long ll_barcode
string ls_lettura, ls_msg

choose case dwo.name
		
	case "data_spedizione"
		if data<>"" and not isnull(data) then
			postevent("ue_set_data_spedizione")
		end if
		
	case "barcode"
		
		if data="" or isnull(data) then return
		
		choose case upper(data)
			case upper(is_reset)
				//reset dei dati
				if g_mb.confirm("Confermi l'annullamento dei dati impostati per la spedizione?") then dw_spedizione.reset()
				
			case else
				//barcode
				wf_inserisci_riga(data)
				
		end choose
		
		wf_reset_dw_barcode()
		
end choose

end event

event buttonclicked;this.accepttext()

if row>0 then
	choose case dwo.name
		case "b_salva"
			
			wf_conferma()
			
	end choose
end if
end event

type dw_lista from datawindow within w_lista_carico_sped
integer x = 27
integer y = 692
integer width = 3721
integer height = 1280
integer taborder = 20
boolean bringtotop = true
string title = "none"
string dataobject = "d_lista_carico_sped"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event rowfocuschanged;if currentrow > 0 then
	selectrow(0, false)
	
	selectrow(currentrow, true)
	
	wf_retrieve_righe(currentrow, dw_lista, dw_ordini_2, true)
else
	wf_retrieve_righe(0, dw_lista, dw_ordini_2, true)
end if

end event

type dw_ricerca from uo_cs_xx_dw within w_lista_carico_sped
event ue_ricerca ( )
integer x = 27
integer y = 132
integer width = 3721
integer height = 532
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_lista_carico_sped_ricerca"
end type

event ue_ricerca();wf_ricerca()
end event

event buttonclicked;call super::buttonclicked;datetime ldt_data_spedizione
string ls_where

this.accepttext()

if row>0 then

	choose case dwo.name
		case "b_ricerca_cliente"
			guo_ricerca.uof_ricerca_cliente(dw_ricerca,"cod_cliente")
			
		case "b_ricerca"
			//ricerca spedizioni
			wf_ricerca()
			
		case "b_reset"
			//reset dei dati a video
			wf_reset()
			
		case "b_evasione_veloce"
			//evasione veloce ordini: creazione bolla/fattura
			//richiama la finestra w_evas_ordini_barcode
			if wf_controlla_per_evasione() = 1 then
				
				//procedi per l'evasione
				s_cs_xx.parametri.parametro_b_2 = true
				s_cs_xx.parametri.parametro_dw_2 = dw_lista
				
				window_open(w_evas_ordini_barcode, -1)
			end if
			
		case "b_pck_list"
			if wf_controlla_per_pck_list(ldt_data_spedizione) =1 then
				
				s_cs_xx.parametri.parametro_data_4 = ldt_data_spedizione
				
				window_open(w_report_pcklist, -1)
				
			end if
			
	end choose
	
end if
end event

event itemchanged;call super::itemchanged;datetime ldt_datetime
string ls_barcode, ls_cod_cliente

if row>0 then
	choose case dwo.name
		
		//se immetti il barcode chiama la ricerca
		case "barcode"
			if data<>"" and not isnull(data) then
				postevent("ue_ricerca")
				
			end if
			
		case "comando"
			if upper(data)=upper(is_reset) then
				wf_cancella_spedizione()
				
				//memo dello stato della dw
				ldt_datetime = dw_ricerca.getitemdatetime(dw_ricerca.getrow(), "data_spedizione")
				ls_barcode = dw_ricerca.getitemstring(dw_ricerca.getrow(), "barcode")
				ls_cod_cliente = dw_ricerca.getitemstring(dw_ricerca.getrow(), "cod_cliente")
				
				dw_ricerca.reset()
				dw_ricerca.insertrow(0)
				dw_ricerca.setfocus()
				
				dw_ricerca.setitem(dw_ricerca.getrow(), "data_spedizione", ldt_datetime)
				dw_ricerca.setitem(dw_ricerca.getrow(), "barcode", ls_barcode)
				dw_ricerca.setitem(dw_ricerca.getrow(), "cod_cliente", ls_cod_cliente)
				//-----
				
				return 2
			else
				//reset di un barcode di riga
				wf_reset(data)
				
				dw_ricerca.reset()
				dw_ricerca.insertrow(0)
				dw_ricerca.setfocus()
				
				return 2
			end if
			
	end choose
end if
end event

type dw_ordini from datawindow within w_lista_carico_sped
integer x = 27
integer y = 1996
integer width = 3721
integer height = 580
integer taborder = 30
string title = "none"
string dataobject = "d_lista_righe_ordini_sped"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type dw_folder from u_folder within w_lista_carico_sped
integer x = 9
integer y = 20
integer width = 3781
integer height = 2580
integer taborder = 10
boolean border = false
end type

event po_tabclicked;call super::po_tabclicked;choose case i_SelectedTabName
	case "Ricerca-Reset"
      
	case "Spedizione"
		wf_reset_dw_barcode()
		
		
end choose
end event

