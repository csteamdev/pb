﻿$PBExportHeader$w_report_bolle_giri.srw
forward
global type w_report_bolle_giri from w_cs_xx_principale
end type
type dw_sel_report_bolle_giri from uo_cs_xx_dw within w_report_bolle_giri
end type
type dw_report_bolle_giri from uo_cs_xx_dw within w_report_bolle_giri
end type
type cb_report from commandbutton within w_report_bolle_giri
end type
type cb_annulla from commandbutton within w_report_bolle_giri
end type
end forward

global type w_report_bolle_giri from w_cs_xx_principale
integer width = 3401
integer height = 2068
string title = "Report Bolle per Giri Consegna"
boolean maxbox = false
boolean resizable = false
dw_sel_report_bolle_giri dw_sel_report_bolle_giri
dw_report_bolle_giri dw_report_bolle_giri
cb_report cb_report
cb_annulla cb_annulla
end type
global w_report_bolle_giri w_report_bolle_giri

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();long		ll_num_bolla, ll_riga

dec{4}	ld_imp_bolla, ld_iva_bolla

datetime ldt_da, ldt_a, ldt_data_bolla

string   ls_tipo_cliente, ls_flag_giro, ls_cod_giro, ls_sql, ls_des_giro, ls_cod_cliente, ls_rag_soc


dw_sel_report_bolle_giri.accepttext()

ldt_da = dw_sel_report_bolle_giri.getitemdatetime(1,"data_da")

if isnull(ldt_da) then
	ldt_da = datetime(date("01/01/1900"),00:00:00)
end if

ldt_a = dw_sel_report_bolle_giri.getitemdatetime(1,"data_a")

if isnull(ldt_a) then
	ldt_a = datetime(date("31/12/2099"),00:00:00)
end if

if ldt_da > ldt_a then
	g_mb.messagebox("APICE","Le date impostate non sono coerenti!",exclamation!)
	return -1
end if

dw_report_bolle_giri.object.t_sel_date.text = "Da data: " + string(date(ldt_da)) + " a data: " + string(date(ldt_a))

ls_tipo_cliente = dw_sel_report_bolle_giri.getitemstring(1,"flag_tipo_cliente")

ls_flag_giro = dw_sel_report_bolle_giri.getitemstring(1,"flag_giro")

ls_cod_giro = dw_sel_report_bolle_giri.getitemstring(1,"cod_giro")

if ls_flag_giro = "N" and isnull(ls_cod_giro) then
	g_mb.messagebox("APICE","Selezionare un giro di consegna oppure abilitare bolle senza giro",exclamation!)
	return -1
end if

ls_sql = &
"select cod_cliente, " + &
"		  num_documento, " + &
"		  data_bolla, " + &
"		  imponibile_iva, " + &
"		  importo_iva " + &
"from	  tes_bol_ven " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"		  cod_documento is not null and " + &
"		  numeratore_documento is not null and " + &
"		  anno_documento is not null and " + &
"		  num_documento is not null and " + &
"		  data_bolla is not null and " + &
"		  data_bolla >= '" + string(ldt_da,s_cs_xx.db_funzioni.formato_data) + "' and " + &
"		  data_bolla <= '" + string(ldt_a,s_cs_xx.db_funzioni.formato_data) + "'"

choose case ls_tipo_cliente
	
	case "I"
		
		ls_sql += " and cod_cliente in (select cod_cliente from anag_clienti where flag_tipo_cliente in ('F','P','S'))"
		
		dw_report_bolle_giri.object.t_sel_clienti.text = "Tipo Clienti: Italia"
		
	case "E"
		
		ls_sql += " and cod_cliente in (select cod_cliente from anag_clienti where flag_tipo_cliente in ('C','E'))"
		
		dw_report_bolle_giri.object.t_sel_clienti.text = "Tipo Clienti: Estero"
		
	case "T"
		
		dw_report_bolle_giri.object.t_sel_clienti.text = "Tipo Clienti: Tutti"
		
end choose

if ls_flag_giro = "S" then
	
	ls_sql += " and cod_cliente not in (select cod_cliente from det_giri_consegne)"
	
	dw_report_bolle_giri.object.t_sel_giro.text = "Giro di consegna: Bolle senza giro"
	
else
	
	ls_sql += " and cod_cliente in (select cod_cliente from det_giri_consegne where cod_giro_consegna = '" + ls_cod_giro + "')"
	
	select des_giro_consegna
	into   :ls_des_giro
	from   tes_giri_consegne
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_giro_consegna = :ls_cod_giro;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in lettura descrizione " + ls_cod_giro + ": " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	
	dw_report_bolle_giri.object.t_sel_giro.text = "Giro di consegna: " + ls_cod_giro + " - " + ls_des_giro
	
end if

dw_report_bolle_giri.reset()

setredraw(false)

declare bolle dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic bolle;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in open cursore bolle: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if
	
do while true

	fetch bolle
	into  :ls_cod_cliente,
			:ll_num_bolla,
			:ldt_data_bolla,
			:ld_imp_bolla,
			:ld_iva_bolla;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in fetch cursore bolle: " + sqlca.sqlerrtext,stopsign!)
		close bolle;
		return -1
	elseif sqlca.sqlcode = 100 then
		close bolle;
		exit
	end if
	
	select rag_soc_1
	into   :ls_rag_soc
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_cliente = :ls_cod_cliente;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in lettura ragione sociale cliente " + ls_cod_cliente + ": " + sqlca.sqlerrtext,stopsign!)
		close bolle;
		return -1
	end if
	
	ll_riga = dw_report_bolle_giri.insertrow(0)
	
	dw_report_bolle_giri.setitem(ll_riga,"cod_cliente",ls_cod_cliente)
	
	dw_report_bolle_giri.setitem(ll_riga,"rag_soc_1",ls_rag_soc)
	
	dw_report_bolle_giri.setitem(ll_riga,"num_bolla",ll_num_bolla)
	
	dw_report_bolle_giri.setitem(ll_riga,"data_bolla",ldt_data_bolla)
	
	dw_report_bolle_giri.setitem(ll_riga,"imp_bolla",ld_imp_bolla)
	
	dw_report_bolle_giri.setitem(ll_riga,"iva_bolla",ld_iva_bolla)
	
	dw_report_bolle_giri.setitem(ll_riga,"tot_bolla",ld_imp_bolla + ld_iva_bolla)
	
loop

dw_report_bolle_giri.setsort("cod_cliente A, num_bolla A")

dw_report_bolle_giri.sort()

dw_report_bolle_giri.resetupdate()

setredraw(true)

return 0
end function

on w_report_bolle_giri.create
int iCurrent
call super::create
this.dw_sel_report_bolle_giri=create dw_sel_report_bolle_giri
this.dw_report_bolle_giri=create dw_report_bolle_giri
this.cb_report=create cb_report
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_sel_report_bolle_giri
this.Control[iCurrent+2]=this.dw_report_bolle_giri
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.cb_annulla
end on

on w_report_bolle_giri.destroy
call super::destroy
destroy(this.dw_sel_report_bolle_giri)
destroy(this.dw_report_bolle_giri)
destroy(this.cb_report)
destroy(this.cb_annulla)
end on

event pc_setwindow;call super::pc_setwindow;dw_report_bolle_giri.ib_dw_report = true

set_w_options(c_closenosave + c_noenablepopup)

dw_report_bolle_giri.change_dw_current()

iuo_dw_main = dw_report_bolle_giri

dw_sel_report_bolle_giri.set_dw_options(sqlca, &
													 pcca.null_object, &
													 c_newonopen + &
													 c_nomodify + &
													 c_nodelete + &
													 c_noretrieveonopen, &
													 c_nohighlightselected + &
													 c_nocursorrowfocusrect)
													 
dw_sel_report_bolle_giri.resetupdate()
													 
dw_report_bolle_giri.set_dw_options(sqlca, &
													 pcca.null_object, &
													 c_nomodify + &
													 c_nodelete + &
													 c_noretrieveonopen, &
													 c_nohighlightselected + &
													 c_nocursorrowfocusrect)

dw_report_bolle_giri.object.datawindow.print.preview = 'Yes'

dw_report_bolle_giri.object.datawindow.print.preview.rulers = 'Yes'

cb_annulla.postevent("clicked")
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_sel_report_bolle_giri,"cod_giro",sqlca,"tes_giri_consegne","cod_giro_consegna", &
					  "des_giro_consegna","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_sel_report_bolle_giri from uo_cs_xx_dw within w_report_bolle_giri
integer x = 23
integer y = 20
integer width = 2949
integer height = 200
integer taborder = 10
string dataobject = "d_sel_report_bolle_giri"
end type

event itemchanged;call super::itemchanged;dw_sel_report_bolle_giri.resetupdate()
end event

type dw_report_bolle_giri from uo_cs_xx_dw within w_report_bolle_giri
integer x = 23
integer y = 240
integer width = 3337
integer height = 1720
integer taborder = 40
string dataobject = "d_report_bolle_giri"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

type cb_report from commandbutton within w_report_bolle_giri
integer x = 2994
integer y = 140
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;setpointer(hourglass!)

dw_report_bolle_giri.change_dw_current()

parent.triggerevent("pc_retrieve")

setpointer(arrow!)
end event

type cb_annulla from commandbutton within w_report_bolle_giri
integer x = 2994
integer y = 20
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;string   ls_null


setnull(ls_null)

dw_sel_report_bolle_giri.setitem(1,"data_da",datetime(date("01/01/" + string(f_anno_esercizio())),00:00:00))

dw_sel_report_bolle_giri.setitem(1,"data_a",datetime(date("31/12/" + string(f_anno_esercizio())),00:00:00))

dw_sel_report_bolle_giri.setitem(1,"flag_tipo_cliente","T")

dw_sel_report_bolle_giri.setitem(1,"flag_giro","N")

dw_sel_report_bolle_giri.setitem(1,"cod_giro",ls_null)
end event

