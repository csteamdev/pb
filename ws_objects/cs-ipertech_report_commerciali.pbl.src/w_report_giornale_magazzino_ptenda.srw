﻿$PBExportHeader$w_report_giornale_magazzino_ptenda.srw
$PBExportComments$Ereditato dallo standard
forward
global type w_report_giornale_magazzino_ptenda from w_report_giornale_magazzino
end type
end forward

global type w_report_giornale_magazzino_ptenda from w_report_giornale_magazzino
end type
global w_report_giornale_magazzino_ptenda w_report_giornale_magazzino_ptenda

type variables

end variables

event pc_setwindow;call super::pc_setwindow;//windowstate = maximized!

//ib_negativi = true

cb_1.visible=false
end event

event resize;call super::resize;//windowstate = maximized!
end event

on w_report_giornale_magazzino_ptenda.create
call super::create
end on

on w_report_giornale_magazzino_ptenda.destroy
call super::destroy
end on

type cb_1 from w_report_giornale_magazzino`cb_1 within w_report_giornale_magazzino_ptenda
end type

type dw_report from w_report_giornale_magazzino`dw_report within w_report_giornale_magazzino_ptenda
string dataobject = "d_report_giornale_magazzino_gruppi_pt"
end type

type dw_folder from w_report_giornale_magazzino`dw_folder within w_report_giornale_magazzino_ptenda
end type

type dw_selezione from w_report_giornale_magazzino`dw_selezione within w_report_giornale_magazzino_ptenda
string dataobject = "d_selezione_report_giornale_mag_ptenda"
end type

event dw_selezione::ue_key;/* eliminato script UE_KEY


LASCIARE IL FLAG EXTEND ANCESTOR = FALSE



*/



end event

type st_1 from w_report_giornale_magazzino`st_1 within w_report_giornale_magazzino_ptenda
end type

type cb_reset from w_report_giornale_magazzino`cb_reset within w_report_giornale_magazzino_ptenda
end type

event cb_reset::clicked;call super::clicked;

string ls_null

setnull(ls_null)

dw_selezione.setitem(1,"cod_prodotto_inizio", ls_null)
dw_selezione.setitem(1,"cod_prodotto_fine", ls_null)


end event

type cb_report from w_report_giornale_magazzino`cb_report within w_report_giornale_magazzino_ptenda
end type

event cb_report::clicked;boolean				lb_1, lb_2, lb_3, lb_continue=false,lb_salta_prodotto=false, lb_codice_raggruppo=false

string					ls_gruppo[5], ls_gruppo_2, ls_gruppo_3, ls_gruppo_4, ls_gruppo_5,ls_flag_prodotti_movimentati, ls_flag_prodotti_fiscali, &
						ls_flag_classe, ls_sql, ls_cod_prodotto, ls_cod_deposito,ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ls_des_prodotto, &
						ls_cod_comodo, ls_cod_comodo_2, ls_cod_cat_mer,ls_flag_dettaglio, ls_cod_misura_mag, ls_des_deposito, ls_des_cat_mer, &
						ls_azienda_rag_soc_1, ls_azienda_localita, ls_azienda_provincia, ls_partita_iva, ls_des_tipo_movimento, ls_des_tipo_mov_det, &
						ls_flag_saldo, ls_flag_saldo_iniziale, ls_errore, ls_chiave[], ls_null_vett[], ls_cod_deposito_sel, ls_log, &
						ls_prodotti_scartati[], ls_flag_lifo, ls_sql_prodotti, ls_cod_prodotto_ds, ls_sql_base, ls_sql_base_raggruppo, ls_where_movimenti, ls_where_prodotti, &
						ls_cod_prodotto_inizio, ls_cod_prodotto_fine, ls_db, ls_where_depositi
						
long					ll_riga, ll_i,ll_tipo_operazione,ll_y,ll_err, ll_cont_scartati, ll_z, ll_tot_prodotti, ll_index_prodotti, ll_count_raggruppato

dec{4}				ld_quan_movimento,ld_quant_val[],ld_giacenza,ld_giacenza_stock[],ld_costo_medio_stock[],ld_quan_costo_medio_stock[], ld_null[]

datetime				ldt_inizio, ldt_fine, ldt_data_registrazione, ldt_data_inventario

uo_magazzino		luo_magazzino

datastore			lds_prodotti



// ----------------------- impostazioni aspetti grafici del report -----------------------------------------
dw_selezione.accepttext()
dw_report.reset()
dw_report.setredraw(false)
st_1.text = "Attendere la Preparazione del Report; l'operazione potrebbe richiedere parecchi minuti."
// ----------------------- verifica impostazioni e filtri --------------------------------------------------
ldt_inizio = dw_selezione.getitemdatetime(1,"data_inizio")
ldt_fine = dw_selezione.getitemdatetime(1,"data_fine")

ldt_data_inventario = datetime(relativedate(date(ldt_inizio), -1), 00:00:00)


if isnull(ldt_inizio) then
	g_mb.messagebox("APICE","Data Inizio non impostata.", StopSign!)
end if
if isnull(ldt_fine) then
	g_mb.messagebox("APICE","Data Fine non impostata.", StopSign!)
end if
if ldt_inizio > ldt_fine then
	g_mb.messagebox("APICE","Date di inizio e fine elaborazione incongruenti fra loro.", StopSign!)
end if

ls_gruppo[1] = dw_selezione.getitemstring(1,"flag_gruppo_1")
ls_gruppo[2] = dw_selezione.getitemstring(1,"flag_gruppo_2")
ls_gruppo[3] = dw_selezione.getitemstring(1,"flag_gruppo_3")
ls_gruppo[4] = dw_selezione.getitemstring(1,"flag_gruppo_4")
ls_gruppo[5] = dw_selezione.getitemstring(1,"flag_gruppo_5")
if ls_gruppo[1] = '2' or ls_gruppo[1] = '5'  then	
	dw_report.object.cod_misura_mag.visible = 1
else
	dw_report.object.cod_misura_mag.visible = 0
end if
ls_flag_prodotti_movimentati = dw_selezione.getitemstring(1,"flag_prodotti_movimentati")
ls_flag_prodotti_fiscali = dw_selezione.getitemstring(1,"flag_prodotti_fiscali")
ls_flag_classe = dw_selezione.getitemstring(1,"flag_classe")
ls_flag_dettaglio = dw_selezione.getitemstring(1,"flag_dettaglio")
ls_flag_saldo_iniziale = dw_selezione.getitemstring(1,"flag_saldo_iniziale")
ls_flag_saldo = dw_selezione.getitemstring(1,"flag_saldo")
ls_cod_deposito_sel = dw_selezione.getitemstring(1,"cod_deposito")
ls_flag_lifo = dw_selezione.getitemstring(1,"flag_lifo")

ls_cod_prodotto_inizio = dw_selezione.getitemstring(1,"cod_prodotto_inizio")
ls_cod_prodotto_fine = dw_selezione.getitemstring(1,"cod_prodotto_fine")

if isnull(ls_flag_prodotti_fiscali) or ls_flag_prodotti_fiscali="" then ls_flag_prodotti_fiscali="T"
if isnull(ls_flag_lifo) or ls_flag_lifo="" then ls_flag_lifo="T"

if ls_flag_classe = "N" then
	setnull(ls_flag_classe)
end if

dw_report.object.st_titolo.text = "GIORNALE DI MAGAZZINO DAL " + string(ldt_inizio,"dd/mm/yyyy") + " AL " + string(ldt_fine,"dd/mm/yyyy")
select rag_soc_1, 
       localita, 
		 provincia, 
		 partita_iva
into   :ls_azienda_rag_soc_1, 
       :ls_azienda_localita, 
		 :ls_azienda_provincia, 
		 :ls_partita_iva
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

dw_report.object.st_azienda.text = ls_azienda_rag_soc_1
dw_report.object.st_indirizzo.text = ls_azienda_localita + "(" + ls_azienda_provincia + ")"
dw_report.object.st_piva.text = ls_partita_iva

ll_cont_scartati = 0


//----------------------------------------------------------------------------------------
//impostazioni report
dw_report.Object.cf_gruppo_1.Expression = 'gruppo_1'
dw_report.Object.cf_gruppo_2.Expression = 'gruppo_2'
dw_report.Object.cf_gruppo_3.Expression = 'gruppo_3'
dw_report.Object.cf_gruppo_4.Expression = 'gruppo_4'
dw_report.Object.cf_gruppo_5.Expression = 'gruppo_5'

//se viene impostato il gruppo 1 allora nascondi la sezione di dettaglio
//se invece non viene impostato alcun raggruppamento allora la sezione di dettaglio deve essere aperta per evitare
//di generare un report "vuoto"

if isnull(ls_gruppo[1]) then
	dw_report.modify("datawindow.header.1.height = 0")
	dw_report.modify("datawindow.detail.height = 76")
else
	dw_report.modify("datawindow.header.1.height = 76")
	dw_report.modify("datawindow.detail.height = 0")
end if
if isnull(ls_gruppo[2]) then
	dw_report.modify("datawindow.header.2.height = 0")
else
	dw_report.modify("datawindow.header.2.height = 76")
end if
if isnull(ls_gruppo[3]) then
	dw_report.modify("datawindow.header.3.height = 0")
else
	dw_report.modify("datawindow.header.3.height = 76")
end if
if isnull(ls_gruppo[4]) then
	dw_report.modify("datawindow.header.4.height = 0")
else
	dw_report.modify("datawindow.header.4.height = 76")
end if
if isnull(ls_gruppo[5]) then
	dw_report.modify("datawindow.header.5.height = 0")
else
	dw_report.modify("datawindow.header.5.height = 76")
end if
//if ls_flag_dettaglio = "N" then
//	dw_report.modify("datawindow.detail.height = 0")
//else
//	dw_report.modify("datawindow.detail.height = 76")
//end if	
//----------------------------------------------------------------------------------------



ls_sql_base =  "select mov_magazzino.data_registrazione,"+&
							"mov_magazzino.cod_prodotto,"+&
							"mov_magazzino.cod_deposito,"+&
							"mov_magazzino.quan_movimento,"+&
							"mov_magazzino.cod_tipo_movimento,"+&
							"mov_magazzino.cod_tipo_mov_det "+&
					"from mov_magazzino "+&
					"join anag_prodotti on anag_prodotti.cod_azienda=mov_magazzino.cod_azienda and " + &
												"anag_prodotti.cod_prodotto=mov_magazzino.cod_prodotto "+&
					"where mov_magazzino.cod_azienda='" + s_cs_xx.cod_azienda +"' "
					
ls_sql_base_raggruppo =  "select mov_magazzino.data_registrazione,"+&
									"mov_magazzino.cod_prodotto,"+&
									"mov_magazzino.cod_deposito,"+&
									"(mov_magazzino.quan_movimento * anag_prodotti.fat_conversione_rag_mag) as quan_movimento,"+&
									"mov_magazzino.cod_tipo_movimento,"+&
									"mov_magazzino.cod_tipo_mov_det "+&
							"from mov_magazzino "+&
							"join anag_prodotti on anag_prodotti.cod_azienda=mov_magazzino.cod_azienda and " + &
														"anag_prodotti.cod_prodotto=mov_magazzino.cod_prodotto "+&
							"where mov_magazzino.cod_azienda='" + s_cs_xx.cod_azienda +"' "


//##########################################################################
//definisco un datastore dei prodotti da elaborare

ls_sql_prodotti = 	"select cod_prodotto,"+&
									"des_prodotto,"+&
									"cod_comodo,"+&
									"cod_comodo_2,"+&
									"cod_cat_mer, "+&
									"cod_misura_mag "+&
						"from anag_prodotti "+&
						"where anag_prodotti.cod_azienda='"+s_cs_xx.cod_azienda+"' "

ls_where_prodotti = ""
ls_where_movimenti = ""

//where datastore prodotti da elaborare ......................
if not isnull(ls_flag_classe) then
	ls_where_prodotti += "and anag_prodotti.flag_classe_abc='" + ls_flag_classe + "' "
end if			
if ls_flag_prodotti_fiscali="N" or ls_flag_prodotti_fiscali = "S" then
	ls_where_prodotti += "and anag_prodotti.flag_articolo_fiscale='"+ls_flag_prodotti_fiscali+"' "
end if
if ls_flag_lifo="N" or ls_flag_lifo = "S" then
	ls_where_prodotti += "and anag_prodotti.flag_lifo='"+ls_flag_lifo+"' "
end if

if not isnull(ls_cod_prodotto_inizio) and ls_cod_prodotto_inizio<>"" then
	ls_where_prodotti += "and anag_prodotti.cod_prodotto>='" + ls_cod_prodotto_inizio + "' "
end if	
if not isnull(ls_cod_prodotto_fine) and ls_cod_prodotto_fine<>"" then
	ls_where_prodotti += "and anag_prodotti.cod_prodotto<='" + ls_cod_prodotto_fine + "' "
end if		

//where cursore movimenti .......................................
ls_where_depositi = ""
if not isnull(ls_cod_deposito_sel) and ls_cod_deposito_sel<>"" then
	ls_where_movimenti += "and mov_magazzino.cod_deposito='" + ls_cod_deposito_sel + "' "
	
	ls_where_depositi = "and cod_deposito=~~'"+ls_cod_deposito_sel+"~~'"
end if
ls_where_movimenti +="and mov_magazzino.data_registrazione>='" + string(ldt_inizio,s_cs_xx.db_funzioni.formato_data) +"' " + &
								"and mov_magazzino.data_registrazione<='" + string(ldt_fine,s_cs_xx.db_funzioni.formato_data) +"' "

ls_sql_prodotti += ls_where_prodotti

ls_sql_prodotti += " order by cod_prodotto"
//##########################################################################


st_1.text = "Preparazione codici da elaborare, attendere ..."

ll_tot_prodotti = guo_functions.uof_crea_datastore(lds_prodotti, ls_sql_prodotti, ls_errore)


if ll_tot_prodotti < 0 then
	g_mb.error("Errore preparazione datastore dei codici da elaborare: " + ls_errore)
	return
elseif ll_tot_prodotti=0 then
	g_mb.warning("Nessun codice da elaborare con questo filtro impostato!")
	return
end if

declare cu_giornale dynamic cursor for sqlsa;


for ll_index_prodotti=1 to ll_tot_prodotti

	Yield()
	ls_cod_prodotto_ds = lds_prodotti.getitemstring(ll_index_prodotti, 1)
	ls_log = "(" + string(ll_index_prodotti) + " di " + string(ll_tot_prodotti) + ") - Elaborazione articolo " +ls_cod_prodotto_ds+" in corso ..."
	
	//se inizia per spazio saltalo che senno si blocca tutto ....
	if left(ls_cod_prodotto_ds, 1) = " " then continue
	
	ls_des_prodotto = lds_prodotti.getitemstring(ll_index_prodotti, 2)
	ls_cod_comodo = lds_prodotti.getitemstring(ll_index_prodotti, 3)
	ls_cod_comodo_2 = lds_prodotti.getitemstring(ll_index_prodotti, 4)
	ls_cod_cat_mer = lds_prodotti.getitemstring(ll_index_prodotti, 5)
	ls_cod_misura_mag = lds_prodotti.getitemstring(ll_index_prodotti, 6)
	
	st_1.text = ls_log
	
	//-----------------------------------------------------------------
	//stabilisco se si tratta di un codice di raggruppo oppure no
	lb_codice_raggruppo=false
	setnull(ll_count_raggruppato)

	select count(*)
	into	:ll_count_raggruppato
	from anag_prodotti
	where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto_raggruppato = :ls_cod_prodotto_ds;
				
	if ll_count_raggruppato>0 then
		lb_codice_raggruppo = true
	end if
	//-----------------------------------------------------------------
	//preparo il cursore dei movimenti del codice da elaborare
	if not lb_codice_raggruppo then
		//prendo i movimenti del codice prodotto corrente
		ls_sql = ls_sql_base + " and mov_magazzino.cod_prodotto='"+ls_cod_prodotto_ds+"' "
	else
		//prendo i movimenti dei codici raggruppati dal prodotto corrente
		ls_sql = 	ls_sql_base_raggruppo + " and mov_magazzino.cod_prodotto IN (	select cod_prodotto "+&
																											"from anag_prodotti "+&
																											"where cod_prodotto_raggruppato='"+ls_cod_prodotto_ds+"') "
	end if
	
	//aggiungo eventuale clausole su deposito selezionato e periodo di tempo impostato)
	ls_sql += ls_where_movimenti
	
	ls_sql += "order by mov_magazzino.data_registrazione"
	
	
	//############################################
	//elaborazione movimenti del prodotto
	//############################################
	prepare sqlsa from :ls_sql;
	
	open cu_giornale;
	if sqlca.sqlcode <> 0 then
		destroy lds_prodotti
		//close cu_prodotti;
		g_mb.error("Errore in apertura del cursore 'cu_giornale': " + sqlca.sqlerrtext)
		return
	end if
	
	
	//§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§
	do while true
		fetch cu_giornale into		:ldt_data_registrazione, :ls_cod_prodotto, :ls_cod_deposito, :ld_quan_movimento, :ls_cod_tipo_movimento, :ls_cod_tipo_mov_det;
		
		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode <> 0 then
			destroy lds_prodotti
			close cu_giornale;
			//close cu_prodotti;
			g_mb.error("Errore in scorrimento del cursore 'cu_giornale':" + sqlca.sqlerrtext)
			rollback;
			return
		end if
	
		st_1.text = ls_log + " - Data Reg.Mov. "+string(ldt_data_registrazione, "dd/mm/yyyy")
		
		if ls_flag_prodotti_movimentati = "S" and ld_quan_movimento = 0 then continue
				
		// aggiungo un movimento di saldo periodo precedente per ogni prodotto coinvolto nel report
		// ma non ancora estratto; lo faccio una sola volta.
		if ls_flag_saldo_iniziale <> "N" then
			
			ll_y = dw_report.Find("cod_prodotto = '"+ls_cod_prodotto_ds+ "'", 1, dw_report.rowcount() )
		
			
			if ll_y = 0 then 		// prima volta che incontro questo prodotto
				
				choose case ls_flag_saldo_iniziale
					case "S"		// carico il saldo iniziale nella q.ta di carico
			
						for ll_y = 1 to 14
							ld_quant_val[ll_y] = 0
						next
						ls_chiave[] = ls_null_vett[]
						ld_giacenza_stock[] = ld_null[]
						ld_costo_medio_stock[] = ld_null[]
						ld_quan_costo_medio_stock[] = ld_null[]
						
						
						luo_magazzino = CREATE uo_magazzino
						
						if not lb_codice_raggruppo then
							
							ll_err = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto_ds, &
																							ldt_data_inventario, &
																							ls_where_depositi, &
																							ld_quant_val[], &
																							ls_errore, &
																							"N", &
																							ls_chiave[], &
																							ld_giacenza_stock[], &
																							ld_costo_medio_stock[],&
																							ld_quan_costo_medio_stock[])
						else
							//si tratta di un codice che raggruppa, quindi la giacenza va valutata sommando le giacenze
							//dei codici raggruppati moltiplicate per i fattori di conversione
							ll_err = wf_prodotto_raggruppo(		ls_cod_prodotto_ds,&
																			luo_magazzino,&
																			ldt_data_inventario,&
																			ls_where_depositi,&
																			ld_quant_val[],&
																			ls_errore,&
																			"N",&
																			ls_chiave[],&
																			ld_giacenza_stock[],&
																			ld_costo_medio_stock[],&
																			ld_quan_costo_medio_stock[])											
						end if
						destroy luo_magazzino
						
					
						ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
						
						ll_riga = dw_report.insertrow(0)
						ls_des_cat_mer = ""
						ls_des_deposito = ""
						
						lb_continue = false
						
						wf_crea_gruppi(ll_riga, ls_cod_prodotto_ds, ls_des_prodotto, ls_cod_comodo, ls_cod_comodo_2, ls_cod_cat_mer, ls_cod_deposito, ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ldt_data_registrazione, ref lb_continue )
						
						if lb_continue = true then
							dw_report.deleterow(ll_riga)
							continue
						end if
						
						
						dw_report.setitem(ll_riga, "data_registrazione",ldt_inizio)
						
						dw_report.setitem(ll_riga, "cod_prodotto",ls_cod_prodotto_ds)
						
						dw_report.setitem(ll_riga, "des_prodotto",ls_des_prodotto)
						dw_report.setitem(ll_riga, "flag_dettaglio",ls_flag_dettaglio)
						dw_report.setitem(ll_riga, "cod_misura_mag",ls_cod_misura_mag)
						dw_report.setitem(ll_riga, "cod_misura_mag_2",ls_cod_misura_mag)
						
						// può essere solo un carico
						dw_report.setitem(ll_riga, "quan_carico", ld_giacenza)
						
					case "F"		// carico il saldo iniziale nella q.ta di carico
						
						// verifico se è fra gli scartati
						//////////////////lb_salta_prodotto = false
						
						//////////////for ll_z = 1 to ll_cont_scartati
						////////////////	if ls_cod_prodotto = ls_prodotti_scartati[ll_z] then
							///////////	lb_salta_prodotto = true
							/////////////	exit
							////////////////end if
						/////////////next
						
						////////if lb_salta_prodotto then continue		// salto la riga giornale perchè è un prodotto con saldo di fine periodo negativo
	
						// non è fra gli scartati, quindi vado a vedere saldo di fine periodo
						ldt_data_inventario = ldt_fine
						for ll_y = 1 to 14
							ld_quant_val[ll_y] = 0
						next
						ls_chiave[] = ls_null_vett[]
						ld_giacenza_stock[] = ld_null[]
						ld_costo_medio_stock[] = ld_null[]
						ld_quan_costo_medio_stock[] = ld_null[]
						
						
						luo_magazzino = CREATE uo_magazzino
						
						if not lb_codice_raggruppo then
						
							ll_err = luo_magazzino.uof_saldo_prod_date_decimal(ls_cod_prodotto_ds, &
																							ldt_data_inventario, &
																							ls_where_depositi, &
																							ld_quant_val[], &
																							ls_errore, &
																							"N", &
																							ls_chiave[], &
																							ld_giacenza_stock[], &
																							ld_costo_medio_stock[],&
																							ld_quan_costo_medio_stock[])
						else
							//si tratta di un codice che raggruppa, quindi la giacenza va valutata sommando le giacenze
							//dei codici raggruppati moltiplicate per i fattori di conversione
							ll_err = wf_prodotto_raggruppo(		ls_cod_prodotto_ds,&
																			luo_magazzino,&
																			ldt_data_inventario,&
																			ls_where_depositi,&
																			ld_quant_val[],&
																			ls_errore,&
																			"N",&
																			ls_chiave[],&
																			ld_giacenza_stock[],&
																			ld_costo_medio_stock[],&
																			ld_quan_costo_medio_stock[])															
						end if
						
						destroy luo_magazzino
						
					
						ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
						
						// se giacenza < 0 allora salto la riga e lo includo fra i prodotti scartati
						// così non calcolerò più il saldo.
							//////if ld_giacenza < 0 then
								//////ll_cont_scartati ++
								//////ls_prodotti_scartati[ll_cont_scartati]= ls_cod_prodotto
								//////continue
							///////end if
						
				end choose					
			end if		
		end if
		
		ll_riga = dw_report.insertrow(0)
		ls_des_cat_mer = ""
		ls_des_deposito = ""
		
		lb_continue = false
		
		wf_crea_gruppi(ll_riga, ls_cod_prodotto_ds, ls_des_prodotto, ls_cod_comodo, ls_cod_comodo_2, ls_cod_cat_mer, ls_cod_deposito, ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ldt_data_registrazione, ref lb_continue )
		
	
		if lb_continue = true then
			dw_report.deleterow(ll_riga)
			continue
		end if
		
		dw_report.setitem(ll_riga, "data_registrazione",ldt_data_registrazione)
		
		dw_report.setitem(ll_riga, "cod_prodotto",ls_cod_prodotto_ds)
		
		dw_report.setitem(ll_riga, "des_prodotto",ls_des_prodotto)
		dw_report.setitem(ll_riga, "flag_dettaglio",ls_flag_dettaglio)
		dw_report.setitem(ll_riga, "cod_misura_mag",ls_cod_misura_mag)
		dw_report.setitem(ll_riga, "cod_misura_mag_2",ls_cod_misura_mag)
		
		
		luo_magazzino = create uo_magazzino
		luo_magazzino.uof_tipo_movimento(ls_cod_tipo_mov_det, ref ll_tipo_operazione, ref lb_1, ref lb_2, ref lb_3)
		destroy luo_magazzino
		
		choose case ll_tipo_operazione
			case 1,5,19,13  		// movimenti di carico
				dw_report.setitem(ll_riga, "quan_carico", ld_quan_movimento)
				dw_report.setitem(ll_riga, "quan_scarico", 0)
			case 2,6,20,14	 		// rettifiche carico
				dw_report.setitem(ll_riga, "quan_carico", 0)
				dw_report.setitem(ll_riga, "quan_scarico",ld_quan_movimento)
			case 3,7	 				// movimenti si scarico
				dw_report.setitem(ll_riga, "quan_carico", 0)
				dw_report.setitem(ll_riga, "quan_scarico", ld_quan_movimento)
			case 4,8	 				// rettifiche scarico
				dw_report.setitem(ll_riga, "quan_carico",ld_quan_movimento)
				dw_report.setitem(ll_riga, "quan_scarico", 0)
			case else  // 9,15,10,16,11,17,12,18
				// movimenti di rettifica solo valore e quindi salti
				
		end choose
		
	loop
	//§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§§
	
	close cu_giornale;
	
	//############################################
	//FINE elaborazione movimenti del prodotto
	//############################################
	
next
//loop

destroy lds_prodotti
//close cu_prodotti;

// ------------------------  fine report: faccio delle sistemazioni grafiche -------------------------
st_1.text = "PRONTO."

rollback;


// nascondo le colonne relative al saldo
if ls_flag_saldo = "N" then
	dw_report.object.t_saldo.visible = 0
	dw_report.object.saldo.visible = 0
	dw_report.object.diff_1.visible = 0
	dw_report.object.diff_2.visible = 0
	dw_report.object.diff_3.visible = 0
	dw_report.object.diff_4.visible = 0
	dw_report.object.diff_5.visible = 0
	
else
	dw_report.object.t_saldo.visible = 1
	dw_report.object.saldo.visible = 1
	dw_report.object.diff_1.visible = 1
	dw_report.object.diff_2.visible = 1
	dw_report.object.diff_3.visible = 1
	dw_report.object.diff_4.visible = 1
	dw_report.object.diff_5.visible = 1
end if

dw_report.groupcalc()

if not ib_negativi then
	dw_report.setfilter(" diff_1 >= 0 ")
	dw_report.filter()
else
	dw_report.setfilter("")
	dw_report.filter()
end if

dw_report.setredraw(true)
dw_folder.fu_selecttab(2)

dw_report.change_dw_current()


end event

