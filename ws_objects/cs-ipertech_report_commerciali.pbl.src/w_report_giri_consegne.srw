﻿$PBExportHeader$w_report_giri_consegne.srw
forward
global type w_report_giri_consegne from w_cs_xx_principale
end type
type st_1 from statictext within w_report_giri_consegne
end type
type dw_report from uo_cs_xx_dw within w_report_giri_consegne
end type
type dw_selezione from uo_cs_xx_dw within w_report_giri_consegne
end type
type cb_1 from commandbutton within w_report_giri_consegne
end type
end forward

global type w_report_giri_consegne from w_cs_xx_principale
integer x = 9
integer y = 8
integer width = 3534
integer height = 2308
string title = "Report Giri Consegne "
event ue_ridimensiona ( )
event ue_posiziona_window ( )
event ue_focus ( )
st_1 st_1
dw_report dw_report
dw_selezione dw_selezione
cb_1 cb_1
end type
global w_report_giri_consegne w_report_giri_consegne

forward prototypes
public subroutine wf_crea_ds_riepilogo_peso (ref datastore lds_data)
public subroutine wf_crea_riepilogo_peso (datastore ads_data)
public subroutine wf_aggiorna_ds_peso (ref datastore ads_data, integer ai_anno_ordine, long al_num_ordine, decimal ad_peso)
end prototypes

event ue_ridimensiona();// Sposta gli oggetti all'interno della finestra in modo relativo alla sua dimensione

dw_selezione.move(1,1)
dw_selezione.height 	= 280		//180
dw_selezione.width 	= width - 10

st_1.x = int( (width - st_1.width) / 2 ) - 10
st_1.y = dw_selezione.y + dw_selezione.height + 5

cb_1.x = st_1.width + st_1.x + 20
cb_1.y = st_1.y

dw_report.x 	  = 1
dw_report.y 	  = st_1.y + st_1.height + 5 //dw_selezione.y + 300
dw_report.width  = width - 20
dw_report.height = height - 100 - dw_report.y





end event

event ue_posiziona_window();move(1,1)

this.width = w_cs_xx_mdi.mdi_1.width - 60
this.height = w_cs_xx_mdi.mdi_1.height - 40

end event

public subroutine wf_crea_ds_riepilogo_peso (ref datastore lds_data);string				ls_sql

ls_sql = "select '      ' as cliente"+&
					", '                                        ' as rag_soc_1,"+&
					" 0.00000 as peso "+&
			"from aziende "+&
			"where cod_azienda='INESISTENTE' "
guo_functions.uof_crea_datastore(lds_data, ls_sql)
end subroutine

public subroutine wf_crea_riepilogo_peso (datastore ads_data);long				ll_row, ll_index
string				ls_exp_1, ls_exp_2, ls_exp_3

ll_row = ads_data.rowcount()
ls_exp_1 = ""
ls_exp_2 = ""
ls_exp_3 = ""

for ll_index=1 to ll_row
	if ll_index > 1 then
		ls_exp_1 += "~r~n"
		ls_exp_2 += "~r~n"
		ls_exp_3 += "~r~n"
	end if
	ls_exp_1 += ads_data.getitemstring(ll_index, 1)
	ls_exp_2 +=ads_data.getitemstring(ll_index, 2)
	ls_exp_3 += string(ads_data.getitemnumber(ll_index, 3), "###,###,##0") + " Kg"
next

dw_report.object.cod_cliente_riepilogo_t.text = ls_exp_1
dw_report.object.cliente_riepilogo_t.text = ls_exp_2
dw_report.object.peso_riepilogo_t.text = ls_exp_3
end subroutine

public subroutine wf_aggiorna_ds_peso (ref datastore ads_data, integer ai_anno_ordine, long al_num_ordine, decimal ad_peso);
long				ll_count, ll_row
dec{5}			ld_peso
string				ls_cod_cliente


ls_cod_cliente = f_des_tabella("tes_ord_ven", "anno_registrazione="+string(ai_anno_ordine)+" and num_registrazione="+string(al_num_ordine), "cod_cliente")

ll_count = ads_data.rowcount()

if ll_count=0 then
	//insert
	ll_row = ads_data.insertrow(0)
	ads_data.setitem(ll_row, 1, ls_cod_cliente)
	ads_data.setitem(ll_row, 2, f_des_tabella("anag_clienti", "cod_cliente='"+ls_cod_cliente+"'", "rag_soc_1"))
	ads_data.setitem(ll_row, 3, ad_peso)
	
else
	//cercala
	ll_row = ads_data.find("cliente='"+ls_cod_cliente+"'", 1, ll_count)
	
	if ll_row = 0 then
		//insert
		ll_row = ads_data.insertrow(0)
		ads_data.setitem(ll_row, 1, ls_cod_cliente)
		ads_data.setitem(ll_row, 2, f_des_tabella("anag_clienti", "cod_cliente='"+ls_cod_cliente+"'", "rag_soc_1"))
		ads_data.setitem(ll_row, 3, ad_peso)
	else
		//update
		ld_peso = ads_data.getitemnumber(ll_row, 3)
		ld_peso = ld_peso + ad_peso
		ads_data.setitem(ll_row, 3, ld_peso)
	end if
	
end if

return
end subroutine

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_CloseNoSave + c_noenablepopup + c_NoResizeWin)

dw_selezione.set_dw_options(sqlca, &
								  pcca.null_object,&
								  c_newonopen, &
								  c_default)
													 
dw_report.set_dw_options(sqlca, &
								  pcca.null_object, &
								  c_multiselect + &
								  c_noretrieveonopen + &
								  c_nonew + &
								  c_nomodify + &
								  c_nodelete + &
								  c_disablecc + &
								  c_disableccinsert, &
								  c_ViewModeBorderUnchanged + &
								  c_ViewModeColorUnchanged + &
								  c_InactiveDWColorUnchanged)

iuo_dw_main = dw_report
dw_report.change_dw_current()
PCCA.Window_CurrentDW = dw_report
save_on_close(c_socnosave)

dw_selezione.setitem(dw_selezione.getrow(),"data_inizio", datetime(today(), 00:00:00))
dw_selezione.setitem(dw_selezione.getrow(),"data_fine",datetime(today(), 00:00:00) )

event post ue_posiziona_window()
event post ue_ridimensiona()

end event

on w_report_giri_consegne.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_report=create dw_report
this.dw_selezione=create dw_selezione
this.cb_1=create cb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.cb_1
end on

on w_report_giri_consegne.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_report)
destroy(this.dw_selezione)
destroy(this.cb_1)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "cod_giro", &
                 sqlca, &
                 "tes_giri_consegne", &
                 "cod_giro_consegna", &
                 "des_giro_consegna", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_selezione, &
                 "cod_deposito_giro", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_selezione, &
                 "cod_deposito_origine", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

event resize;call super::resize;event ue_ridimensiona()
end event

type st_1 from statictext within w_report_giri_consegne
integer x = 882
integer y = 312
integer width = 2217
integer height = 84
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type dw_report from uo_cs_xx_dw within w_report_giri_consegne
integer x = 14
integer y = 436
integer width = 3470
integer height = 1748
integer taborder = 10
string dataobject = "d_report_giri"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;datetime			ldt_data_da,ldt_data_a, ldt_data_reg, ldt_data_consegna

string				ls_cod_giro, ls_cod_cliente, ls_des_giro, ls_des_cliente, ls_localita, ls_cod_prodotto, &
					ls_des_prodotto, ls_misura, ls_cod_giro_consegna, ls_sql,ls_flag_blocco, ls_provincia, &
					ls_messaggio, ls_cod_cliente_selezione, ls_rif_interscambio, ls_num_ord_cliente,ls_cod_misura_mag,&
					ls_localita_consegna, ls_cod_deposito_giro, ls_cod_deposito_origine, ls_msg

long				ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_prog_giro_consegna, &
					ll_pos, ll_stato_prod_filtro, ll_stato_prod_ordine, ll_num_ord, ll_anno_ord, ll_num_ord_2, &
					ll_anno_ord_2, ll_riga, ll_controllo, ll_stato_ordine

integer			li_row

decimal			ld_dim_x, ld_dim_y, ld_dim_z, ld_quan_ordine, ld_quan_in_evasione, ld_quan_evasa, ld_qta_per_peso
dec{5}			ld_peso_netto_unitario

uo_produzione	luo_prod

datastore		lds_peso


st_1.text = "Attendere il termine dell'elaborazione !!! "
dw_report.setredraw(false)

li_row = dw_selezione.getrow()

//Donato 22/01/2013
//Creazione DS per riepilogo peso per cliente ##############################
wf_crea_ds_riepilogo_peso(lds_peso)
//##########################################################

dw_selezione.accepttext()
setpointer(hourglass!)

dw_report.Object.DataWindow.Print.preview = "no"
dw_report.Object.DataWindow.Print.preview.rulers = "no"

dw_report.reset()

ldt_data_da = dw_selezione.getitemdatetime(li_row, "data_inizio")
if isnull(ldt_data_da) or ldt_data_da <= datetime(date("01/01/1900"), 00:00:00) then
	ldt_data_da = datetime(date("01/01/1900"), 00:00:00)
end if

ldt_data_a = dw_selezione.getitemdatetime(li_row, "data_fine")
if isnull(ldt_data_a) or ldt_data_a <= datetime(date("01/01/1900"), 00:00:00) then
	ldt_data_a = datetime(date("31/12/2030"), 00:00:00)
end if	

ls_cod_giro_consegna = dw_selezione.getitemstring(li_row, "cod_giro")
if g_str.isempty(ls_cod_giro_consegna) then
	//giro consegna non impostato (verifica se richiedi un particolare deposito giro consegna)
	ls_cod_giro_consegna = "%"
	
	ls_cod_deposito_giro = dw_selezione.getitemstring(li_row, "cod_deposito_giro")
	if g_str.isempty(ls_cod_deposito_giro) then setnull(ls_cod_deposito_giro)		//ls_cod_deposito_giro = "%"	
else
	//GIRO consegna impostato, inutile filtrare per deposito giro consegna
	setnull(ls_cod_deposito_giro)		//ls_cod_deposito_giro = "%"
end if

ls_cod_cliente_selezione = dw_selezione.getitemstring(li_row, "cod_cliente")
if g_str.isempty(ls_cod_cliente_selezione) then
	ls_cod_cliente_selezione = "%"
end if

ls_cod_deposito_origine = dw_selezione.getitemstring(li_row, "cod_deposito_origine")
if g_str.isempty(ls_cod_deposito_origine) then
	ls_cod_deposito_origine = "%"
end if


ll_stato_prod_filtro = dw_selezione.getitemnumber(li_row, "flag_avan_prod")

declare  cu_prodotti dynamic cursor for sqlsa;

declare cu_n_giri cursor for
select		tes_ord_ven.cod_giro_consegna 
from		tes_ord_ven
join		tes_giri_consegne on tes_giri_consegne.cod_azienda=tes_ord_ven.cod_azienda and
										tes_giri_consegne.cod_giro_consegna=tes_ord_ven.cod_giro_consegna
where    tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and tes_ord_ven.cod_giro_consegna like :ls_cod_giro_consegna and
			tes_ord_ven.data_consegna >= :ldt_data_da and 	tes_ord_ven.data_consegna <= :ldt_data_a and
			tes_ord_ven.cod_cliente like :ls_cod_cliente_selezione and tes_ord_ven.cod_deposito like :ls_cod_deposito_origine and
			(:ls_cod_deposito_giro is null or tes_giri_consegne.cod_deposito like :ls_cod_deposito_giro)
group by tes_ord_ven.cod_giro_consegna;


declare  cu_cliente_giro cursor for
select   anno_registrazione, num_registrazione, cod_cliente, data_registrazione, data_consegna, rif_interscambio, num_ord_cliente, localita
from     tes_ord_ven
where	cod_azienda = :s_cs_xx.cod_azienda and cod_giro_consegna = :ls_cod_giro and
			data_consegna >= :ldt_data_da and data_consegna <= :ldt_data_a and flag_blocco = 'N' and
			cod_cliente like :ls_cod_cliente_selezione and tes_ord_ven.cod_deposito like :ls_cod_deposito_origine
order by cod_cliente;

open cu_n_giri;
if sqlca.sqlcode <> 0 then
	ls_msg = "Errore nella open del cursore (cu_n_giri)"+ sqlca.sqlerrtext
	rollback;
	g_mb.error(ls_msg)
	return
end if

do while 1 = 1
	
	fetch cu_n_giri
	into  :ls_cod_giro;
	
	if sqlca.sqlcode = 100 then exit

	if sqlca.sqlcode < 0 then
		ls_msg = "Errore nella FETCH del cursore (cu_n_giri): "+sqlca.sqlerrtext
		rollback;
		g_mb.error(ls_msg)
		return
	end if
	
	st_1.text = "Attendere il termine dell'elaborazione !!! " + ls_cod_giro
	
   if sqlca.sqlcode = 0 then
		
		select des_giro_consegna 
		into  :ls_des_giro 
		from tes_giri_consegne 
		where cod_azienda = :s_cs_xx.cod_azienda and
		      cod_giro_consegna like :ls_cod_giro;
				
		if sqlca.sqlcode <> 0 then
			//g_mb.messagebox("Errore", "La descrizione del codice giro '" + ls_cod_giro + "' non trovata")
			ls_des_giro = "Giro '"+ls_cod_giro+"' senza descrizione!"
		end if
		
		open cu_cliente_giro;
		if sqlca.sqlcode < 0 then
			ls_msg = "Errore nella open del cursore (cu_cliente_giro): "+sqlca.sqlerrtext
			rollback;
			g_mb.error(ls_msg)
			return
		end if
		
		do while 1 = 1 
			
			fetch cu_cliente_giro
			into  :ll_anno_registrazione,
			      :ll_num_registrazione,
					:ls_cod_cliente,
					:ldt_data_reg,
					:ldt_data_consegna,
					:ls_rif_interscambio,
					:ls_num_ord_cliente,
					:ls_localita_consegna;
					
			if sqlca.sqlcode = 100 then exit 
			
			st_1.text = "Attendere il termine dell'elaborazione !!! "+ ls_cod_giro + "/" + ls_cod_cliente
			
			if sqlca.sqlcode < 0 then
				ls_msg = "Errore nella FETCH del cursore (cu_cliente_giro).~r~n"+sqlca.sqlerrtext
				rollback;
				g_mb.error(ls_msg)
				return
			end if
	
			if sqlca.sqlcode = 0 then
				
				select rag_soc_1, localita, provincia 
				into :ls_des_cliente, :ls_localita, :ls_provincia
				from anag_clienti
				where cod_azienda = :s_cs_xx.cod_azienda and 
						cod_cliente = :ls_cod_cliente;
						
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Rag. soc./ localita", "Ragione sociale e localita del codice cliente '" + ls_cod_cliente + "' non trovate")
				end if
				
				select prog_giro_consegna
				into   :ll_prog_giro_consegna
				from   det_giri_consegne
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_giro_consegna = :ls_cod_giro and
						 cod_cliente = :ls_cod_cliente;
						 
				if sqlca.sqlcode = 100 then
					ll_prog_giro_consegna = 999999
				elseif sqlca.sqlcode = -1 then
					g_mb.messagebox("APICE","Errore in ricerca progressivo giro in tabella dettaglio giri consegne. Dettaglio=" + sqlca.sqlerrtext)
					ll_prog_giro_consegna = 999999
				end if
				
//				cursore per righe prodotti -----------				
// preparazione SQL per le righe
				ls_sql = "select   cod_prodotto, des_prodotto, quan_ordine, "+&
									"cod_misura, prog_riga_ord_ven, quan_in_evasione, quan_evasa, "+&
									"flag_blocco, isnull(peso_netto, 0) "+&
							"from det_ord_ven " + &
							"where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
									 "anno_registrazione = " + string(ll_anno_registrazione) + " and " + &
									 "num_registrazione = " + string(ll_num_registrazione)
				
				choose case dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato")
					case "R" // residui
						ls_sql = ls_sql + " and flag_evasione in ('A','P')"
					case "E"	// Solo evasi
						ls_sql = ls_sql + " and flag_evasione ='E' "
					case "A"	// Solo aperti
						ls_sql = ls_sql + " and flag_evasione ='A' "
					case "P"	// solo parziali
						ls_sql = ls_sql + " and flag_evasione ='P' "
				end choose
						
				ls_sql = ls_sql + " order by num_registrazione "
				
				prepare  SQLSA FROM :ls_sql;
				
				open dynamic cu_prodotti;
				
				if sqlca.sqlcode > 0 then
					ls_msg = "Errore nella open del cursore (cu_prodotti): "+sqlca.sqlerrtext
					rollback;
					g_mb.error(ls_msg)
					return
				end if
				// OGNI NUOVO ORDINE:
				ll_controllo = 0
				
				do while 1 = 1
					fetch cu_prodotti
					into  :ls_cod_prodotto,
					      :ls_des_prodotto,
							:ld_quan_ordine,
							:ls_misura,
							:ll_prog_riga_ord_ven,
							:ld_quan_in_evasione,
							:ld_quan_evasa,
							:ls_flag_blocco,
							:ld_peso_netto_unitario;
							
					if sqlca.sqlcode = 100 then exit
					
					st_1.text = "Attendere il termine dell'elaborazione !!!  "+ ls_cod_giro + "/" + ls_cod_cliente + "/" + ls_cod_prodotto
					
					if sqlca.sqlcode > 0 then
						ls_msg = "Errore nella FETCH del cursore (cu_prodotti).~r~n"+sqlca.sqlerrtext
						rollback;
						g_mb.error(ls_msg)
						return
					end if
					
					if ls_flag_blocco = "S" then continue
					
					luo_prod = create uo_produzione
						
					ll_stato_prod_ordine = luo_prod.uof_stato_prod_det_ord_ven(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_messaggio)
					
					if ll_stato_prod_ordine = -1 then
						rollback;
						g_mb.error(ls_messaggio)
						return -1
					end if
					
					if ll_stato_prod_filtro <> -1 then
						if ll_stato_prod_filtro = 3 then
							if ll_stato_prod_ordine <> 0 and ll_stato_prod_ordine <> 2 then
								continue
							end if
						else
							if ll_stato_prod_ordine <> ll_stato_prod_filtro then
								continue
							end if
						end if
					end if
					
					//*** Michela 20/03/2006:controllo lo stato dell'ordine
					ll_stato_ordine = -1
					ll_stato_ordine = luo_prod.uof_stato_prod_tes_ord_ven(ll_anno_registrazione,ll_num_registrazione,ls_messaggio)
					if ll_stato_ordine = 1 then // tutto prodotto
						ll_stato_prod_ordine	= 3
					end if					
					
					// modifica claudia 26/01/06 per aggiungedere dei dati
					//inseriti due campi R.I. e rif. vs. ordine come da richiesta del cliente
					if ll_controllo = 0 and len(ls_rif_interscambio) > 0 and not isnull(ls_rif_interscambio) then
						
						ll_i = dw_report.insertrow(0)
						dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
						dw_report.setitem(ll_i, "des_giro", ls_des_giro)
						//dw_report.setitem(ll_i, "data_registrazione", ldt_data_reg)
						//dw_report.setitem(ll_i, "data_consegna", ldt_data_consegna)
						dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
						dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
						dw_report.setitem(ll_i, "localita", ls_localita)
						dw_report.setitem(ll_i, "provincia", ls_provincia)
						dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
						dw_report.setitem(ll_i, "cod_prodotto", "R.I.")
						dw_report.setitem(ll_i, "des_prodotto", ls_rif_interscambio)
						dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
						//dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
						dw_report.setitem(ll_i, "flag_tipo_riga", "S")
						//messo 0 perchè così li mette prima dell'ordine
						dw_report.setitem(ll_i, "prog_riga_ord_ven", 0)
						dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)
						
						if isnull(ls_localita_consegna) or len(trim(ls_localita_consegna)) < 1 then
							dw_report.setitem(ll_i, "localita_consegna", ls_localita)
						else
							dw_report.setitem(ll_i, "localita_consegna", ls_localita_consegna)
						end if
						
						ll_controllo = 1
						
						if len(ls_num_ord_cliente) > 0 and not isnull(ls_num_ord_cliente) then
							
							ll_i = dw_report.insertrow(0)
							dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
							dw_report.setitem(ll_i, "des_giro", ls_des_giro)
							//dw_report.setitem(ll_i, "data_registrazione", ldt_data_reg)
							//dw_report.setitem(ll_i, "data_consegna", ldt_data_consegna)
							dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
							dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
							dw_report.setitem(ll_i, "localita", ls_localita)
							dw_report.setitem(ll_i, "provincia", ls_provincia)
							//dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
							dw_report.setitem(ll_i, "cod_prodotto", "R.VS.ORD.")
							dw_report.setitem(ll_i, "des_prodotto", ls_num_ord_cliente)
							dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
							dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
							dw_report.setitem(ll_i, "prog_riga_ord_ven", 0)
							dw_report.setitem(ll_i, "flag_tipo_riga", "S")
							dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)
							
							if isnull(ls_localita_consegna) or len(trim(ls_localita_consegna)) < 1 then
								dw_report.setitem(ll_i, "localita_consegna", ls_localita)
							else
								dw_report.setitem(ll_i, "localita_consegna", ls_localita_consegna)
							end if
							
						end if
						
					else
						
						if ll_controllo = 0 and len(ls_num_ord_cliente) > 0 and not isnull(ls_num_ord_cliente) then
							
							ll_i = dw_report.insertrow(0)
							ll_controllo = 1
							dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
							dw_report.setitem(ll_i, "des_giro", ls_des_giro)
							//dw_report.setitem(ll_i, "data_registrazione", ldt_data_reg)
							//dw_report.setitem(ll_i, "data_consegna", ldt_data_consegna)
							dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
							dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
							dw_report.setitem(ll_i, "localita", ls_localita)
							dw_report.setitem(ll_i, "provincia", ls_provincia)
							dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
							dw_report.setitem(ll_i, "cod_prodotto", "R.VS.ORD.")
							dw_report.setitem(ll_i, "des_prodotto", ls_num_ord_cliente)
							dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
							//dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
							dw_report.setitem(ll_i, "prog_riga_ord_ven", 0)
							dw_report.setitem(ll_i, "flag_tipo_riga", "S")
							dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)
							
							if isnull(ls_localita_consegna) or len(trim(ls_localita_consegna)) < 1 then
								dw_report.setitem(ll_i, "localita_consegna", ls_localita)
							else
								dw_report.setitem(ll_i, "localita_consegna", ls_localita_consegna)
							end if
							
						end if
						
					end if
					//fine modifica claudia
					
					ll_i = dw_report.insertrow(0)	
					dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
					dw_report.setitem(ll_i, "des_giro", ls_des_giro)
					dw_report.setitem(ll_i, "data_registrazione", ldt_data_reg)
					dw_report.setitem(ll_i, "data_consegna", ldt_data_consegna)
					dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
					dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
					dw_report.setitem(ll_i, "localita", ls_localita)
					dw_report.setitem(ll_i, "provincia", ls_provincia)
					dw_report.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
					dw_report.setitem(ll_i, "des_prodotto", ls_des_prodotto)
					
					if isnull(ls_localita_consegna) or len(trim(ls_localita_consegna)) < 1 then
						dw_report.setitem(ll_i, "localita_consegna", ls_localita)
					else
						dw_report.setitem(ll_i, "localita_consegna", ls_localita_consegna)
					end if
					
					
					if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato") = 'R' or dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato") = 'P' then
						ld_qta_per_peso = ld_quan_ordine - ld_quan_in_evasione - ld_quan_evasa
					else
						ld_qta_per_peso = ld_quan_ordine
					end if
					dw_report.setitem(ll_i, "quan_consegna", ld_qta_per_peso)
					
					
					setnull(ls_cod_misura_mag)
					
					select cod_misura_mag
					into   :ls_cod_misura_mag 	
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
					       cod_prodotto = :ls_cod_prodotto;
					
					// 17/06/2008 su richiesta di PTENDA sostituita l'unità di misura di vendita con quella di magazzino.
					dw_report.setitem(ll_i, "cod_misura", ls_cod_misura_mag)
//					dw_report.setitem(ll_i, "cod_misura", ls_misura)
					dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
					dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
					dw_report.setitem(ll_i, "prog_riga_ord_ven", ll_prog_riga_ord_ven)
					dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)
					
					dw_report.setitem(ll_i,"stato_prod",ll_stato_prod_ordine)
					
					select dim_x, dim_y, dim_z
					into   :ld_dim_x, :ld_dim_y, :ld_dim_z
					from   comp_det_ord_ven
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 anno_registrazione = :ll_anno_registrazione and
							 num_registrazione = :ll_num_registrazione and
							 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
							 
					if sqlca.sqlcode = 0 then
							dw_report.setitem(ll_i, "dim_1", ld_dim_x)
							dw_report.setitem(ll_i, "dim_2", ld_dim_y)
							dw_report.setitem(ll_i, "dim_3", ld_dim_z)
					end if
					
					//#########################################################################
					dw_report.setitem(ll_i, "peso_netto_unitario", ld_peso_netto_unitario)
					wf_aggiorna_ds_peso(lds_peso, ll_anno_registrazione, ll_num_registrazione, ld_peso_netto_unitario * ld_qta_per_peso)
					//#########################################################################
					
				loop
				
				close cu_prodotti;
				
			end if
		
		loop
		
		close cu_cliente_giro;
		
	end if
	
loop

close cu_n_giri;

rollback;

dw_report.setsort("cod_giro A, prog_giro A, cod_cliente A, anno_registrazione A, num_registrazione A, prog_riga_ord_ven A") 
dw_report.sort()
dw_report.groupcalc()	

setpointer(arrow!)

dw_report.object.t_1.text = 'R E P O R T    G I R I    D I    C O N S E G N A  '
if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato") = 'R' or dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato") = 'P' then
	dw_report.object.t_1.text = 'R E P O R T    G I R I    D I    C O N S E G N A  (R)'
end if

//*** Michela 20/03/2006: ho sostituito il quadratino pieno con un triangolo pieno nero
dw_report.Object.p_tutto.Filename = s_cs_xx.volume + s_cs_xx.risorse + "triangolo.bmp"

//faccio qui il setredraw a TRUE perchè deve ridisegnare il riepilogo peso per cliente!!!
dw_report.setredraw(true)
//------------------------------------------------------------------------------------------------

wf_crea_riepilogo_peso(lds_peso)
destroy lds_peso

st_1.text = "Report Pronto."

dw_report.setredraw(true)

dw_report.setfocus()
dw_report.change_dw_current()
PCCA.Window_CurrentDW = dw_report



end event

event objectatpointer;call super::objectatpointer;dw_report.change_dw_current()
end event

type dw_selezione from uo_cs_xx_dw within w_report_giri_consegne
integer width = 3497
integer height = 280
integer taborder = 120
string dataobject = "d_ext_sel_report_giri_consegna"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
end choose
end event

event itemchanged;call super::itemchanged;string				ls_cod_deposito
integer			li_tab_order = 0


choose case dwo.name
	case "cod_giro"
		if data<>"" then
			//hai selezionato un giro in cui cercare
			//proteggi e imposta il campo deposito giro
			
			select cod_deposito
			into :ls_cod_deposito
			from tes_giri_consegne
			where	cod_azienda=:s_cs_xx.cod_azienda and
						cod_giro_consegna=:data;
			
			if g_str.isempty(ls_cod_deposito) then
				setnull(ls_cod_deposito)
			end if
			
			//Inoltre lascia la variabile li_tab_order a ZERO
		else
			//sproteggi il campo deposito giro e ripuliscilo
			setnull(ls_cod_deposito)
			li_tab_order = 5000
		end if
		
		//imposta il taborder (che potrebbe essere anche ZERO)
		settaborder("cod_deposito_giro", li_tab_order)
		
		//imposta il valore (che potrebbe essere anche vuoto)
		setitem(1, "cod_deposito_giro", ls_cod_deposito)
		
		
end choose
end event

type cb_1 from commandbutton within w_report_giri_consegne
integer x = 3113
integer y = 316
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ricerca"
end type

event clicked;dw_report.change_dw_current()

parent.postevent("pc_retrieve")


dw_report.resetupdate( )
end event

