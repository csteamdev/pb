﻿$PBExportHeader$w_report_giri_consegne_stato_prod.srw
$PBExportComments$Report Giri di consegna con stato produzione
forward
global type w_report_giri_consegne_stato_prod from w_cs_xx_principale
end type
type cb_stampa_riassunto from commandbutton within w_report_giri_consegne_stato_prod
end type
type cb_stampa from commandbutton within w_report_giri_consegne_stato_prod
end type
type dw_riassunto from uo_cs_xx_dw within w_report_giri_consegne_stato_prod
end type
type cb_annulla from commandbutton within w_report_giri_consegne_stato_prod
end type
type progess from hprogressbar within w_report_giri_consegne_stato_prod
end type
type st_1 from statictext within w_report_giri_consegne_stato_prod
end type
type dw_report from uo_cs_xx_dw within w_report_giri_consegne_stato_prod
end type
type cb_1 from commandbutton within w_report_giri_consegne_stato_prod
end type
type dw_selezione from uo_cs_xx_dw within w_report_giri_consegne_stato_prod
end type
type dw_folder from u_folder within w_report_giri_consegne_stato_prod
end type
end forward

global type w_report_giri_consegne_stato_prod from w_cs_xx_principale
integer x = 9
integer y = 8
integer width = 4416
integer height = 2636
string title = "Report Giri Consegne con Stato Produzione"
event ue_posiziona_window ( )
event ue_carica_deposito_utente ( )
cb_stampa_riassunto cb_stampa_riassunto
cb_stampa cb_stampa
dw_riassunto dw_riassunto
cb_annulla cb_annulla
progess progess
st_1 st_1
dw_report dw_report
cb_1 cb_1
dw_selezione dw_selezione
dw_folder dw_folder
end type
global w_report_giri_consegne_stato_prod w_report_giri_consegne_stato_prod

type variables
constant string LOG_FILE = "C:\temp\log_ddt_trasf.txt"

private:
	boolean ib_halt = false
	boolean ib_debug = false
	time it_time_start
	
	long il_cpu_start
end variables

forward prototypes
public function integer wf_report_giri_consegna ()
public function integer wf_report_lista_carico ()
public function integer wf_get_tipo_ordine (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_tipo_stampa, ref string fs_msg)
public function integer wf_get_sum_colli (string fs_tipo_stampa, long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_reparto, ref decimal fd_num_colli, ref string fs_msg)
public function integer wf_get_reparti_deposito (string as_cod_deposito, ref string as_cod_reparti[], ref string as_errore)
public function boolean wf_riga_spedita (long al_anno_ord_ven, long al_num_ord_ven, long al_prog_riga_ord_ven, string as_cod_deposito, string as_cod_cliente)
public subroutine wf_pulisci_stato_trasf (long al_riga, integer ai_j)
public function integer wf_get_tipo_ordine (string as_cod_tipo_ord_ven, ref string as_tipo_stampa)
public subroutine wf_log (string as_message)
public subroutine wf_crea_ds_riepilogo_peso (ref datastore lds_data)
public subroutine wf_crea_riepilogo_peso (datastore ads_data)
public function integer wf_report_giri_trasferimento (string as_flag_tipo_report)
public function integer wf_controlla_date (integer al_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_deposito_partenza, datetime adt_data_trasf_inizio, datetime adt_data_trasf_fine, ref string as_errore)
public subroutine wf_aggiorna_ds_peso (ref datastore ads_data, integer ai_anno_ordine, long al_num_ordine, decimal ad_peso)
public function string wf_get_note_fisse_cliente (string as_cod_cliente)
public function string wf_get_note_fido (string as_cod_cliente, datetime adt_data_rif)
public function string wf_get_post_it (integer ai_anno, long al_numero)
public subroutine wf_aggiorna_ds_riassunto_tes (ref datastore ads_data, integer ai_anno_ordine, long al_num_ordine, string as_cliente)
public subroutine wf_aggiorna_ds_riassunto_det (ref datastore ads_data, integer ai_anno_ordine, long al_num_ordine, long al_riga, string as_cliente, decimal ad_peso, decimal ad_colli)
public subroutine wf_mostra_ds_riassunto (datastore ads_data)
public function integer wf_imposta_filtro_trasf ()
public function boolean wf_check_filtro_trasf (string as_cod_deposito_origine, string as_reparti[], string as_depositi[], integer ai_valore[], integer ai_stato_trasf_filtro)
public function string wf_destinazione_diversa (integer ai_anno, long al_numero)
public function string wf_get_operaio (long al_progressivo)
public function long wf_buffer_colli (long al_id, string as_flag_stampa_bcl, integer ai_anno, long al_numero, long al_riga, ref long al_num_colli_letti, ref long al_num_colli_previsti, ref string as_errore)
end prototypes

event ue_posiziona_window();move(1,1)

this.width = w_cs_xx_mdi.mdi_1.width - 60
this.height = w_cs_xx_mdi.mdi_1.height - 40



end event

event ue_carica_deposito_utente();string ls_cod_deposito_ut, ls_error

guo_functions.uof_get_stabilimento_utente(ls_cod_deposito_ut, ls_error)

if isnull(ls_cod_deposito_ut) or len(ls_cod_deposito_ut) < 1 then return

dw_selezione.setitem(1, "cod_deposito_partenza", ls_cod_deposito_ut)
end event

public function integer wf_report_giri_consegna ();boolean 	lb_alusistemi

datetime ldt_data_da,ldt_data_a, ldt_data_reg, ldt_data_consegna

string		ls_cod_giro, ls_cod_cliente, ls_des_giro, ls_des_cliente, ls_localita, ls_cod_prodotto, ls_cod_deposito_origine, &
         	ls_des_prodotto, ls_misura, ls_cod_giro_consegna, ls_sql,ls_flag_blocco, ls_provincia, &
			ls_messaggio, ls_cod_cliente_selezione, ls_rif_interscambio, ls_num_ord_cliente, &
			ls_cod_reparto[],ls_flag_riga_sospesa,ls_cod_tipo_causa_sospensione,ls_flag_urgente,ls_sigla, &
			ls_flag_evasione,ls_cod_tipo_det_ven,ls_flag_tipo_det_ven,ls_cod_valuta, ls_null[],ls_flag_valorizza, &
			ls_barcode, ls_cod_tipo_ord_ven, ls_flag_tipo_bol_fat, ls_cap, ls_telefono, ls_fax, ls_cellulare, ls_internet, &
			ls_des_pagamento, ls_note,ls_cod_pagamento,ls_indirizzo, ls_cod_tessuto, ls_cod_verniciatura, ls_des_tessuto, &
			ls_des_verniciatura, ls_colore_tessuto, ls_barcode_ft, ls_localita_dest, ls_cod_tipo_bol_ven, ls_flag_evasione_tes, &
			ls_cod_tipo_fat_ven,ls_cod_tipo_doc, ls_error, ls_flag_sospesi, ls_cod_misura_mag, ls_posizione, ls_posizione_filtro, &
			ls_dep[], ls_cod_deposito_giro_sel, ls_cod_deposito_origine_sel, ls_cod_deposito_giro, ls_cod_comodo

long		ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_prog_giro_consegna, &
			ll_pos, ll_stato_prod_filtro, ll_stato_prod_ordine, ll_num_ord, ll_anno_ord, ll_num_ord_2, &
			ll_anno_ord_2, ll_riga, ll_controllo, ll_stato_ordine, ll_stato_reparto[], ll_ret, ll_y, ll_null[], &
			ll_anno_filtro, ll_num_filtro, ll_anno_registrazione_old, ll_num_registrazione_old, ll_righe_sfuso, li_count_sosp_rep, ll_id_spedizione
			
double 	ld_sconto[], ld_prezzo_vendita, ld_val_riga, ld_cambio_ven

decimal  ld_dim_x, ld_dim_y, ld_dim_z, ld_quan_ordine, ld_quan_in_evasione, ld_quan_evasa, ld_imponibile_iva, &
			ld_num_colli, ld_tot_colli, ld_peso_netto_unitario, ld_qta_per_peso

string		ls_flag_tipo_stampa, ls_sigla_sosp_reparto, ls_flag_stato_bolla, ls_cod_dep_filtro

long 		ll_num_riga_appartenenza, ll_pos_riga, ll_num_colli_letti, ll_num_colli_previsti

// semaforo che mi indica se devo stampare il nuovo barcode per la testata oppure no
// uso il semaforo per fare in modo che il barcode appaia solo nella prima riga.
boolean lb_stampa_barcode_ft = true

integer	li_stati_bolla[], li_null[], li_stato_trasf_filtro, li_ret

uo_produzione luo_prod
datastore lds_store
uo_array luo_colli_sfuso
datastore	lds_peso

guo_functions.uof_get_parametro_azienda( "ALU", lb_alusistemi)

st_1.text = "Attendere il termine dell'elaborazione !!! "
dw_report.setredraw(false)
luo_colli_sfuso = create uo_array

//Donato 22/01/2013
//Creazione DS per riepilogo peso per cliente ##############################
wf_crea_ds_riepilogo_peso(lds_peso)
//##########################################################

dw_selezione.accepttext()
setpointer(hourglass!)

dw_report.Object.DataWindow.Print.preview = "no"
dw_report.Object.DataWindow.Print.preview.rulers = "no"

dw_report.reset()
dw_riassunto.reset()

ll_anno_filtro = dw_selezione.getitemnumber(dw_selezione.getrow(),"anno_registrazione")
ll_num_filtro = dw_selezione.getitemnumber(dw_selezione.getrow(),"num_registrazione")

ls_flag_sospesi = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_sospesi")

ldt_data_da = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_inizio")
if isnull(ldt_data_da) or ldt_data_da <= datetime(date("01/01/1900"), 00:00:00) then
	ldt_data_da = datetime(date("01/01/1900"), 00:00:00)
end if

ldt_data_a = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_fine")
if isnull(ldt_data_a) or ldt_data_a <= datetime(date("01/01/1900"), 00:00:00) then
	ldt_data_a = datetime(date("31/12/2099"), 00:00:00)
end if	

ls_cod_giro_consegna = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_giro")

if g_str.isempty(ls_cod_giro_consegna) then
	//giro consegna non impostato (verifica se richiedi un particolare deposito giro consegna)
	ls_cod_giro_consegna = "%"
	
	ls_cod_deposito_giro_sel = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_deposito_giro")
	if g_str.isempty(ls_cod_deposito_giro_sel) then setnull(ls_cod_deposito_giro_sel) // ls_cod_deposito_giro_sel = "%"	
else
	//GIRO consegna impostato, inutile filtrare per deposito giro consegna
	setnull(ls_cod_deposito_giro_sel)		// = "%"
end if

ls_cod_deposito_origine_sel = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_deposito_origine")
if g_str.isempty(ls_cod_deposito_origine_sel) then
	ls_cod_deposito_origine_sel = "%"
end if

ls_posizione_filtro = dw_selezione.getitemstring(dw_selezione.getrow(),"posizione")
li_stato_trasf_filtro = wf_imposta_filtro_trasf()

ls_cod_cliente_selezione = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_cliente")

if isnull(ls_cod_cliente_selezione) or ls_cod_cliente_selezione = "" then
	ls_cod_cliente_selezione = "%"
end if

ll_stato_prod_filtro = dw_selezione.getitemnumber(dw_selezione.getrow(),"flag_avan_prod")

ls_flag_valorizza = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_valorizza")

ll_id_spedizione = dw_selezione.getitemnumber(dw_selezione.getrow(),"id_spedizione")


declare  cu_prodotti dynamic cursor for sqlsa;

declare  cu_n_giri cursor for
select   tes_ord_ven.cod_giro_consegna
from     tes_ord_ven
join tes_giri_consegne on 	tes_giri_consegne.cod_azienda=tes_ord_ven.cod_azienda and
									tes_giri_consegne.cod_giro_consegna=tes_ord_ven.cod_giro_consegna
where    tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
	      	tes_ord_ven.data_consegna >= :ldt_data_da and tes_ord_ven.data_consegna <= :ldt_data_a and
			tes_ord_ven.cod_giro_consegna like :ls_cod_giro_consegna and
			tes_ord_ven.cod_cliente like :ls_cod_cliente_selezione and
			(:ll_anno_filtro is null or tes_ord_ven.anno_registrazione = :ll_anno_filtro) and
			(:ll_num_filtro is null or tes_ord_ven.num_registrazione = :ll_num_filtro) and
			tes_ord_ven.cod_deposito like :ls_cod_deposito_origine_sel and
			(:ls_cod_deposito_giro_sel is null or tes_giri_consegne.cod_deposito like :ls_cod_deposito_giro_sel)
group by tes_ord_ven.cod_giro_consegna;


//Donato 15/07/2014: Chiesto da Alberto e Beatrice
//al posto di flag_urgente della det_ord_ven leggere il flag_tassativo dalla tes_ord_ven ed utilizzare lo stesso campo nella dw

declare  cu_cliente_giro cursor for
select   anno_registrazione, 
			num_registrazione, 
			cod_cliente, 
			data_registrazione, 
			data_consegna, 
			rif_interscambio, 
			num_ord_cliente, 
			cambio_ven, 
			cod_valuta, 
			cod_tipo_ord_ven,
			cod_pagamento,
			cod_deposito,
			flag_tassativo,
			flag_evasione
from     tes_ord_ven
where    cod_azienda = :s_cs_xx.cod_azienda and
	      data_consegna >= :ldt_data_da and
			data_consegna <= :ldt_data_a and
	      cod_giro_consegna = :ls_cod_giro and
			flag_blocco = 'N' and
			cod_cliente like :ls_cod_cliente_selezione and
			(:ll_anno_filtro is null or anno_registrazione = :ll_anno_filtro) and
			(:ll_num_filtro is null or num_registrazione = :ll_num_filtro) and
			cod_deposito like :ls_cod_deposito_origine_sel
order by cod_cliente;

open cu_n_giri;
if sqlca.sqlcode > 0 then
	g_mb.messagebox("APICE","Errore nella open del cursore (cu_n_giri).~r~n"+sqlca.sqlerrtext)
	rollback;
	return -1 //####
end if

do while true
	
	fetch cu_n_giri
	into  :ls_cod_giro;
	
	if sqlca.sqlcode = 100 then exit

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore nella FETCH del cursore (cu_n_giri).~r~n"+sqlca.sqlerrtext)
		rollback;
		return -1 //#####
	end if
	
	st_1.text = "Attendere il termine dell'elaborazione !!! " + ls_cod_giro
	
   if sqlca.sqlcode = 0 then
		
		select des_giro_consegna 
		into  :ls_des_giro 
		from tes_giri_consegne 
		where cod_azienda = :s_cs_xx.cod_azienda and
		      cod_giro_consegna like :ls_cod_giro;
				
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Errore", "La descrizione del codice giro '" + ls_cod_giro + "' non trovata")
		end if
		
		if g_str.isnotempty(ls_cod_giro) then
			//deposito giro consegna
			select cod_deposito
			into :ls_cod_deposito_giro
			from tes_giri_consegne
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						cod_giro_consegna=:ls_cod_giro;
		else
			setnull(ls_cod_deposito_giro)
		end if
		
		
		open cu_cliente_giro;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore nella open del cursore (cu_cliente_giro).~r~n"+sqlca.sqlerrtext)
			rollback;
			return -1 //####
		end if
		
		do while TRUE
			
			fetch cu_cliente_giro
			into  :ll_anno_registrazione,
			      :ll_num_registrazione,
					:ls_cod_cliente,
					:ldt_data_reg,
					:ldt_data_consegna,
					:ls_rif_interscambio,
					:ls_num_ord_cliente,
					:ld_cambio_ven,
					:ls_cod_valuta,
					:ls_cod_tipo_ord_ven,
					:ls_cod_pagamento,
					:ls_cod_deposito_origine,
					:ls_flag_urgente,
					:ls_flag_evasione_tes;
					
			if sqlca.sqlcode = 100 then exit 
			
			st_1.text = "Attendere il termine dell'elaborazione !!! "+ ls_cod_giro + "/" + ls_cod_cliente
			lb_stampa_barcode_ft = true
			
			if isnull(ls_flag_urgente) or ls_flag_urgente="" then ls_flag_urgente="N"
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore nella FETCH del cursore (cu_cliente_giro).~r~n"+sqlca.sqlerrtext)
				rollback;
				return -1 //####
			end if
			
			//se il giro consegna ha un deposito considera questo al posto del deposito origine ordine
			if g_str.isnotempty(ls_cod_deposito_giro) then
				ls_cod_deposito_origine = ls_cod_deposito_giro
			end if
			
			
			if isnull(ls_flag_evasione_tes) or ls_flag_evasione_tes="" then ls_flag_evasione_tes = "A"
	
			choose case dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato")
				case "T"
					
				case "R"
					if ls_flag_evasione_tes<>"A" and ls_flag_evasione_tes<>"P" then continue
					
				case "E"
					if ls_flag_evasione_tes<>"E" then continue
					
				case "P"
					if ls_flag_evasione_tes<>"P" then continue
					
				case "A"
					if ls_flag_evasione_tes<>"A" then continue
					
			end choose
			
			
			//in questo cursore vado a leggere il tipo ordine (A, B, C) -----------------------------------------------
			if wf_get_tipo_ordine(ll_anno_registrazione,ll_num_registrazione,ls_flag_tipo_stampa,ls_messaggio)<0 then
				g_mb.error("APICE",ls_messaggio)
				close cu_n_giri;
				close cu_cliente_giro;
				
				rollback;
				return -1 //####
			end if
			//---------------------------------------------------------------------------------------------------------
			
			ls_localita_dest = wf_destinazione_diversa(ll_anno_registrazione, ll_num_registrazione)
			// ----
			
			setnull(ls_flag_tipo_bol_fat)
			setnull(ls_cod_tipo_doc)
				
			// stefanop 04/01/2012 : controllo documento successivo
			select flag_tipo_bol_fat, cod_tipo_bol_ven, cod_tipo_fat_ven
			into :ls_flag_tipo_bol_fat, :ls_cod_tipo_bol_ven, :ls_cod_tipo_fat_ven
			from tab_tipi_ord_ven
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
				
			if ls_flag_tipo_bol_fat = 'B' and len(ls_cod_tipo_bol_ven) > 0 then
				ls_cod_tipo_doc = ls_cod_tipo_bol_ven
			elseif ls_flag_tipo_bol_fat = 'F' and len(ls_cod_tipo_fat_ven) > 0 then
				ls_cod_tipo_doc = ls_cod_tipo_fat_ven
			end if
			/// ----
	
			select rag_soc_1, 
					 indirizzo, 
					 localita, 
					 provincia, 
					 cap,
					 telefono, 
					 fax, 
					 telex, 
					 sito_internet
			into 	 :ls_des_cliente, 
			       :ls_indirizzo, 
					 :ls_localita, 
					 :ls_provincia, 
					 :ls_cap, 
					 :ls_telefono, 
					 :ls_fax, 
					 :ls_cellulare, 
					 :ls_note
			from anag_clienti
			where cod_azienda = :s_cs_xx.cod_azienda and 
					cod_cliente = :ls_cod_cliente;
					
			if sqlca.sqlcode <> 0 then 
				g_mb.messagebox("Rag. soc./ localita", "Ragione sociale e localita del codice cliente '" + ls_cod_cliente + "' non trovate")
			end if
			
			select des_pagamento
			into   :ls_des_pagamento
			from	 tab_pagamenti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_pagamento = :ls_cod_pagamento;
			if sqlca.sqlcode <> 0 then
				ls_des_pagamento = ""
			end if
			
			select prog_giro_consegna
			into   :ll_prog_giro_consegna
			from   det_giri_consegne
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_giro_consegna = :ls_cod_giro and
					 cod_cliente = :ls_cod_cliente;
					 
			if sqlca.sqlcode = 100 then
				ll_prog_giro_consegna = 999999
			elseif sqlca.sqlcode = -1 then
				g_mb.messagebox("APICE","Errore in ricerca progressivo giro in tabella dettaglio giri consegne. Dettaglio=" + sqlca.sqlerrtext)
				ll_prog_giro_consegna = 999999
			end if
			
			
			//#######################################################
			//IMPOSTAZIONE RIGA CLIENTE In DATASTORE PER RIASSUNTO
			wf_aggiorna_ds_riassunto_tes(lds_peso, ll_anno_registrazione,ll_num_registrazione, ls_cod_cliente)
			//#######################################################
			
//			cursore per righe prodotti -----------
//			preparazione SQL per le righe
			ll_pos_riga = 0

			ls_sql = " select   cod_prodotto, des_prodotto, quan_ordine, cod_misura, prog_riga_ord_ven, quan_in_evasione, quan_evasa, " + &
			 			" flag_blocco, imponibile_iva, flag_riga_sospesa, cod_tipo_causa_sospensione, flag_evasione, " + &
						" sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10, prezzo_vendita,cod_tipo_det_ven " + &
						" ,num_riga_appartenenza, isnull(peso_netto, 0) "+ &
						" from det_ord_ven " + &
						" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
								 "anno_registrazione = " + string(ll_anno_registrazione) + " and " + &
								 "num_registrazione = " + string(ll_num_registrazione)
			
			choose case dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato")
				case "R" // residui
					ls_sql = ls_sql + " and flag_evasione in ('A','P')"
				case "E"	// Solo evasi
					ls_sql = ls_sql + " and flag_evasione ='E' "
				case "A"	// Solo aperti
					ls_sql = ls_sql + " and flag_evasione ='A' "
				case "P"	// solo parziali
					ls_sql = ls_sql + " and flag_evasione ='P' "
			end choose
			
			//Donato 22/01/2013
			//##########################################################
			//commentato perchè il filtro sospensione va integrato con la sospensione per reparto
//			choose case ls_flag_sospesi
//				case "S"
//					ls_sql = ls_sql + " and flag_riga_sospesa = 'S' "
//				case "N"
//					ls_sql = ls_sql + " and flag_riga_sospesa = 'N' "
//			end choose
			//##########################################################
					
			ls_sql = ls_sql + " order by prog_riga_ord_ven "
			
			prepare  SQLSA FROM :ls_sql;
			
			open dynamic cu_prodotti;
			
			if sqlca.sqlcode > 0 then
				g_mb.messagebox("APICE","Errore nella open del cursore (cu_prodotti).~r~n"+sqlca.sqlerrtext)
				rollback;
				return -1 //####
			end if
			// OGNI NUOVO ORDINE:
			ll_controllo = 0
			
			do while true
				fetch cu_prodotti
				into  :ls_cod_prodotto,
						:ls_des_prodotto,
						:ld_quan_ordine,
						:ls_misura,
						:ll_prog_riga_ord_ven,
						:ld_quan_in_evasione,
						:ld_quan_evasa,
						:ls_flag_blocco,
						:ld_imponibile_iva,
						:ls_flag_riga_sospesa,
						:ls_cod_tipo_causa_sospensione,
						:ls_flag_evasione,
						:ld_sconto[1],
						:ld_sconto[2],
						:ld_sconto[3],
						:ld_sconto[4],
						:ld_sconto[5],
						:ld_sconto[6],
						:ld_sconto[7],
						:ld_sconto[8],
						:ld_sconto[9],
						:ld_sconto[10],
						:ld_prezzo_vendita,
						:ls_cod_tipo_det_ven,
						:ll_num_riga_appartenenza,
						:ld_peso_netto_unitario;
						
				if sqlca.sqlcode = 100 then exit
				
				st_1.text = "Attendere il termine dell'elaborazione !!!  "+ ls_cod_giro + "/" + ls_cod_cliente + "/" + ls_cod_prodotto
				
				if sqlca.sqlcode > 0 then
					g_mb.messagebox("APICE","Errore nella FETCH del cursore (cu_prodotti).~r~n"+sqlca.sqlerrtext)
					rollback;
					return -1 //####
				end if
				
				
				if ls_flag_blocco = "S" then continue
				
				// -------------------- Alusistemi 09/04/2019
				if lb_alusistemi then
					if ll_num_riga_appartenenza > 0 and not isnull(ll_num_riga_appartenenza) then continue 
					if len(ls_cod_prodotto) > 0 and not isnull(ls_cod_prodotto) then
						select cod_comodo
						into	:ls_cod_comodo
						from	anag_prodotti 
						where cod_azienda = :s_cs_xx.cod_azienda and
								cod_prodotto = :ls_cod_prodotto;
					else
						ls_cod_comodo = ""
					end if
				end if		
				//----------------------------------------------------------------------------
				//se risulta specificato il filtro per ID spedizione verifica che l'ordine che stai elaborando è tra quelli con i colli scansionati
				li_ret = wf_buffer_colli(	ll_id_spedizione, ls_flag_tipo_stampa, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, &
												ll_num_colli_letti, ll_num_colli_previsti, ls_messaggio)
				choose case li_ret
					case is < 0
						g_mb.error("APICE", ls_messaggio)
						close cu_n_giri;
						close cu_cliente_giro;
						close cu_prodotti;
						
						rollback;
						return -1
					
					case 0
						//ignora il controllo e avanti con l'elaborazione
					
					case 1
						////salta la riga
						//continue
						
					case 2
						//avanti con l'elaborazione
					
				end choose
				//----------------------------------------------------------------------------
				
				
				luo_prod = create uo_produzione
				
				//Donato 20/03/2014: filtro per posizione (se impostata nel filltro cerca solo quelle con tale posizione)
				if ls_posizione_filtro<>"" and not isnull(ls_posizione_filtro) then
					//se riga collegata inutile verificare il filtro per posizione anche perchè le tabelle delle fasi,
					//det_ordini_prioduzione e tes_ordini_produzione sono vuote per le righe collegate
					//quindi la regola generale è che la riga collegata sarà visibile nel report se è visibile la riga principale
					
					if ll_num_riga_appartenenza>0 then
						//è una riga collegata ---------------------------------------
						if dw_report.find(	"anno_registrazione="+string(ll_anno_registrazione)+" and "+&
												"num_registrazione="+string(ll_num_registrazione)+" and "+&
												"prog_riga_ord_ven="+string(ll_num_riga_appartenenza), &
												dw_report.rowcount(), 1) > 0 then
							//riga principale PRESENTE nel report, quindi anche la riga collegata lo deve essere
						else
							//riga principale NON presente nel report, quindi la riga è da escludere
							continue
						end if
						
					else
						//è una riga principale -----------------------------------
						//verifica se la posizione della riga è quella richiesta nel filtro
						if not luo_prod.uof_check_posizione(ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,ls_posizione_filtro) then
							continue
						end if
					end if
				else
					//filtro per posizione non specificato
				end if
				
				
				ll_stato_prod_ordine = luo_prod.uof_stato_prod_det_ord_ven(ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,ls_messaggio)
				
				if ll_stato_prod_ordine = -1 then
					g_mb.messagebox("APICE",ls_messaggio,stopsign!)
					return -1
				end if
				
				if ll_stato_prod_filtro <> -1 then
					if ll_stato_prod_filtro = 3 then
						if ll_stato_prod_ordine <> 0 and ll_stato_prod_ordine <> 2 then
							continue
						end if
					else
						if ll_stato_prod_ordine <> ll_stato_prod_filtro then
							continue
						end if
					end if
				end if
				
				
				//Donato 22/01/2012
				//se nel filtro hai specificato di visualizzare solo quelli sospesi o solo quelli non sospesi, tienine conto
				//###############################################################
				if ls_flag_sospesi <> "T" then
					if (ls_flag_tipo_stampa = "B" or ls_flag_tipo_stampa = "C") then
						li_count_sosp_rep = luo_prod.uof_get_sospensioni_riga_ordine(ll_anno_registrazione, ll_num_registrazione, 0)
					else
						li_count_sosp_rep = luo_prod.uof_get_sospensioni_riga_ordine(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven)
					end if
					
					if ls_flag_sospesi = "N" then
						//chiesto di visualizzare solo quelli NON sospesi
						if li_count_sosp_rep = 0 and ls_flag_riga_sospesa="N" then
						else
							continue
						end if
					else
						//chiesto di visualizzare solo quelli SOSPESI
						if li_count_sosp_rep > 0 or ls_flag_riga_sospesa = "S" then
						else
							continue
						end if
					end if
				end if
				//###############################################################
				
				
				
				// stefanop 11/04/2012 - recupero i colli dei reparti per lo sfuso, solo se è una nuova testata d'ordine
				//-------------------------------------------------------------------------------------
				if (ls_flag_tipo_stampa = "B" or ls_flag_tipo_stampa = "C") and&
					ll_anno_registrazione <> ll_anno_registrazione_old or ll_num_registrazione <> ll_num_registrazione_old then 
					destroy lds_store
				//else
					luo_colli_sfuso.removeall()
					
					ls_sql = "SELECT cod_reparto, count(*) FROM tab_ord_ven_colli WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND " + &
								"anno_registrazione=" + string(ll_anno_registrazione) + " AND " + &
								"num_registrazione=" + string(ll_num_registrazione) + &
								" GROUP BY cod_reparto"
					
					ll_righe_sfuso = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)
					if ll_righe_sfuso < 0 then
						g_mb.error(ls_error)
						return -1
					end if
					
					for ll_i = 1 to ll_righe_sfuso
						luo_colli_sfuso.set(lds_store.getitemstring(ll_i, 1), lds_store.getitemnumber(ll_i, 2))
					next
					
					ll_anno_registrazione_old = ll_anno_registrazione
					ll_num_registrazione_old = ll_num_registrazione 
				end if
				//-------------------------------------------------------------------------------------
								
				//*** Michela 20/03/2006:controllo lo stato dell'ordine
				ll_stato_ordine = -1
				ll_stato_ordine = luo_prod.uof_stato_prod_tes_ord_ven(ll_anno_registrazione,ll_num_registrazione,ls_messaggio)
				if ll_stato_ordine = 1 then // tutto prodotto
					ll_stato_prod_ordine	= 3
				end if					
				
				// modifica claudia 26/01/06 per aggiungedere dei dati
				//inseriti due campi R.I. e rif. vs. ordine come da richiesta del cliente
				if ll_controllo = 0 and len(ls_rif_interscambio) > 0 and not isnull(ls_rif_interscambio) then
					ll_i = dw_report.insertrow(0)
					dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
					dw_report.setitem(ll_i, "des_giro", ls_des_giro)
					dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
					dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
					dw_report.setitem(ll_i, "localita", ls_cap + " " + ls_localita)
					dw_report.setitem(ll_i, "provincia", ls_provincia)
					dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
					dw_report.setitem(ll_i, "cod_prodotto", "R.I.")
					dw_report.setitem(ll_i, "des_prodotto", ls_rif_interscambio)
					dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
					dw_report.setitem(ll_i, "flag_tipo_riga", "S")
					//messo 0 perchè così li mette prima dell'ordine
					dw_report.setitem(ll_i, "prog_riga_ord_ven", 0)
					dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)
					
					dw_report.setitem(ll_i, "indirizzo", ls_indirizzo)
					dw_report.setitem(ll_i, "telefono", ls_telefono)
					dw_report.setitem(ll_i, "fax", ls_fax)
					dw_report.setitem(ll_i, "cellulare", ls_cellulare)
					dw_report.setitem(ll_i, "note", ls_note)
					dw_report.setitem(ll_i, "pagamento", ls_des_pagamento)
					dw_report.setitem(ll_i, "flag_valorizza", ls_flag_valorizza)
					
					//18/05/2001 richiesta di Beatrice -----------------------------
					//se il prog_riga_ord_ven è 0 nascondi i colli e relativa label
					//nascondi il tot colli e relativa label
					dw_report.setitem(ll_i,"flag_colli","N")
					//---------------------------------------------------------------
					
					dw_report.setitem(ll_i,"flag_tipo_bol_fat",ls_flag_tipo_bol_fat)
					dw_report.setitem(ll_i,"cod_tipo_doc",ls_cod_tipo_doc)
					
					//destinazione diversa, se presente
					dw_report.setitem(ll_i,"localita_dest",ls_localita_dest)
					
					ll_controllo = 1
					if len(ls_num_ord_cliente) > 0 and not isnull(ls_num_ord_cliente) then
						ll_i = dw_report.insertrow(0)
						dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
						dw_report.setitem(ll_i, "des_giro", ls_des_giro)
						dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
						dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
						dw_report.setitem(ll_i, "localita", ls_cap + " " + ls_localita)
						dw_report.setitem(ll_i, "provincia", ls_provincia)
						dw_report.setitem(ll_i, "cod_prodotto", "R.VS.ORD.")
						dw_report.setitem(ll_i, "des_prodotto", ls_num_ord_cliente)
						dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
						dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
						dw_report.setitem(ll_i, "prog_riga_ord_ven", 0)
						dw_report.setitem(ll_i, "flag_tipo_riga", "S")
						dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)
						
						dw_report.setitem(ll_i, "indirizzo", ls_indirizzo)
						dw_report.setitem(ll_i, "telefono", ls_telefono)
						dw_report.setitem(ll_i, "fax", ls_fax)
						dw_report.setitem(ll_i, "cellulare", ls_cellulare)
						dw_report.setitem(ll_i, "note", ls_note)
						dw_report.setitem(ll_i, "pagamento", ls_des_pagamento)
						dw_report.setitem(ll_i, "flag_valorizza", ls_flag_valorizza)
						
						//18/05/2001 richiesta di Beatrice -----------------------------
						//se il prog_riga_ord_ven è 0 nascondi i colli e relativa label
						//nascondi il tot colli e relativa label
						dw_report.setitem(ll_i,"flag_colli","N")
						//---------------------------------------------------------------
						
						dw_report.setitem(ll_i,"flag_tipo_bol_fat",ls_flag_tipo_bol_fat)
						dw_report.setitem(ll_i,"cod_tipo_doc",ls_cod_tipo_doc)
						
						//destinazione diversa, se presente
						dw_report.setitem(ll_i,"localita_dest",ls_localita_dest)
						
						dw_report.setitem(ll_i,"cod_comodo",ls_cod_comodo)
					end if
					
				elseif not lb_alusistemi then
					
					if ll_controllo = 0 and len(ls_num_ord_cliente) > 0 and not isnull(ls_num_ord_cliente) then
						ll_i = dw_report.insertrow(0)
						ll_controllo = 1
						dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
						dw_report.setitem(ll_i, "des_giro", ls_des_giro)
						dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
						dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
						dw_report.setitem(ll_i, "localita", ls_cap + " " + ls_localita)
						dw_report.setitem(ll_i, "provincia", ls_provincia)
						dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
						dw_report.setitem(ll_i, "cod_prodotto", "R.VS.ORD.")
						dw_report.setitem(ll_i, "des_prodotto", ls_num_ord_cliente)
						dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
						dw_report.setitem(ll_i, "prog_riga_ord_ven", 0)
						dw_report.setitem(ll_i, "flag_tipo_riga", "S")
						dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)
						
						dw_report.setitem(ll_i, "indirizzo", ls_indirizzo)
						dw_report.setitem(ll_i, "telefono", ls_telefono)
						dw_report.setitem(ll_i, "fax", ls_fax)
						dw_report.setitem(ll_i, "cellulare", ls_cellulare)
						dw_report.setitem(ll_i, "note", ls_note)
						dw_report.setitem(ll_i, "pagamento", ls_des_pagamento)
						dw_report.setitem(ll_i, "flag_valorizza", ls_flag_valorizza)
						
						//18/05/2001 richiesta di Beatrice -----------------------------
						//se il prog_riga_ord_ven è 0 nascondi i colli e relativa label
						//nascondi il tot colli e relativa label
						dw_report.setitem(ll_i,"flag_colli","N")
						//---------------------------------------------------------------
						
						dw_report.setitem(ll_i,"flag_tipo_bol_fat",ls_flag_tipo_bol_fat)
						dw_report.setitem(ll_i,"cod_tipo_doc",ls_cod_tipo_doc)
						
						//destinazione diversa, se presente
						dw_report.setitem(ll_i,"localita_dest",ls_localita_dest)
						
					end if
				end if
				//fine modifica claudia
				
				//inserimento riga dettaglio
				ll_pos_riga += 1
				
				ll_i = dw_report.insertrow(0)	
				dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
				dw_report.setitem(ll_i, "des_giro", ls_des_giro)
				dw_report.setitem(ll_i, "data_registrazione", ldt_data_reg)
				dw_report.setitem(ll_i, "data_consegna", ldt_data_consegna)
				dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
				dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
				dw_report.setitem(ll_i, "localita", ls_cap + " " + ls_localita)
				dw_report.setitem(ll_i, "provincia", ls_provincia)
				dw_report.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
				dw_report.setitem(ll_i, "des_prodotto", ls_des_prodotto)
				
				dw_report.setitem(ll_i, "cod_comodo", ls_cod_comodo)
				
				if lb_alusistemi and dw_report.dataobject="d_report_giri_stato_prod_semplif" then
					dw_report.setitem(ll_i, "num_ord_cliente", ls_num_ord_cliente)
				end if
				
				if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato") = 'R' or dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato") = 'P' then
					ld_qta_per_peso = ld_quan_ordine - ld_quan_in_evasione - ld_quan_evasa
				else
					ld_qta_per_peso = ld_quan_ordine
				end if
				dw_report.setitem(ll_i, "quan_consegna", ld_qta_per_peso)
				
				
				setnull(ls_cod_misura_mag)
					
				select cod_misura_mag
				into   :ls_cod_misura_mag 	
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
					
				// 07/02/2013 su richiesta di BEATRICE  sostituita l'unità di misura di vendita con quella di magazzino.
				dw_report.setitem(ll_i, "cod_misura", ls_cod_misura_mag)
				//dw_report.setitem(ll_i, "cod_misura", ls_misura)
				
				dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
				dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
				dw_report.setitem(ll_i, "prog_riga_ord_ven", ll_prog_riga_ord_ven)
				dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)

				dw_report.setitem(ll_i, "indirizzo", ls_indirizzo)
				dw_report.setitem(ll_i, "telefono", ls_telefono)
				dw_report.setitem(ll_i, "fax", ls_fax)
				dw_report.setitem(ll_i, "cellulare", ls_cellulare)
				dw_report.setitem(ll_i, "note", ls_note)
				dw_report.setitem(ll_i, "pagamento", ls_des_pagamento)
				
				dw_report.setitem(ll_i, "peso_netto_unitario", ld_peso_netto_unitario)
				
				ls_barcode = "*O" + string(ll_anno_registrazione,"0000") + string(ll_num_registrazione,"000000") + string(ll_prog_riga_ord_ven,"0000") + "*"
				
				// stefanop 04/01/2012 : aggiungo il barcode per la testata
				// attenzione che metto sempre la variabile ls_barcode nella barcode_bl
				if lb_stampa_barcode_ft then
					ls_barcode_ft = "*O" + string(ll_anno_registrazione,"0000") + string(ll_num_registrazione,"000000") + "*"
					lb_stampa_barcode_ft = false
				else
					setnull(ls_barcode_ft)
				end if
				// ---
			
				if len(ls_flag_tipo_bol_fat) > 0 then
					choose case ls_flag_tipo_bol_fat
						case "B"
							dw_report.setitem(ll_i, "barcode_ft", ls_barcode_ft)
							dw_report.setitem(ll_i, "barcode_bl", ls_barcode)
						case "F"			
							dw_report.setitem(ll_i, "barcode_ft", ls_barcode_ft)
							dw_report.setitem(ll_i, "barcode_bl", ls_barcode)
						case else
							dw_report.setitem(ll_i, "barcode_ft", ls_null)
							dw_report.setitem(ll_i, "barcode_bl", ls_null)
					end choose
				else
					dw_report.setitem(ll_i, "barcode_ft", ls_null)
					dw_report.setitem(ll_i, "barcode_bl", ls_null)
				end if
				
				
				dw_report.setitem(ll_i,"stato_prod",ll_stato_prod_ordine)
				
				// stefanop 04/01/2012 : inserisco la destinazione diversa
				dw_report.setitem(ll_i,"localita_dest",ls_localita_dest)
				dw_report.setitem(ll_i,"flag_tipo_bol_fat",ls_flag_tipo_bol_fat)
				dw_report.setitem(ll_i,"cod_tipo_doc",ls_cod_tipo_doc)
				// ---
				
				//Donato 10/05/2011
				//Beatrice ha richiesto che per le righe è riferite (quindi con colli sempre a ZERO)
				//è inutile far vedere COLLI=...
				if ll_num_riga_appartenenza > 0 then
					//riga riferita, nascondi il tot colli e relativa label
					dw_report.setitem(ll_i,"flag_colli","N")
				end if
				
				select dim_x, dim_y, dim_z, cod_tessuto, cod_verniciatura, cod_non_a_magazzino
				into   :ld_dim_x, :ld_dim_y, :ld_dim_z, :ls_cod_tessuto, :ls_cod_verniciatura, :ls_colore_tessuto
				from   comp_det_ord_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione and
						 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
						 
				if sqlca.sqlcode = 0 then
						dw_report.setitem(ll_i, "dim_1", ld_dim_x)
						dw_report.setitem(ll_i, "dim_2", ld_dim_y)
						dw_report.setitem(ll_i, "dim_3", ld_dim_z)
						
						select des_prodotto
						into   :ls_des_tessuto
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
						       cod_prodotto = :ls_cod_tessuto;
						if sqlca.sqlcode = 0 and not isnull(ls_des_tessuto) and len(ls_des_tessuto) > 0 then
							dw_report.setitem(ll_i, "tessuto", ls_cod_tessuto + " " + ls_des_tessuto)
						end if
						
						select des_prodotto
						into   :ls_des_verniciatura
						from   anag_prodotti
						where  cod_azienda = :s_cs_xx.cod_azienda and
						       cod_prodotto = :ls_cod_verniciatura;
						if sqlca.sqlcode = 0 and not isnull(ls_des_verniciatura) and len(ls_des_verniciatura) > 0 then
							dw_report.setitem(ll_i, "verniciatura", ls_des_verniciatura)
						end if
						
						//Donato 25/01/2010 aggiunto colore tessuto
						dw_report.setitem(ll_i, "cod_non_a_magazzino", ls_colore_tessuto)
						
				end if
				
				// aggiunto da Enrico per Viropa 19/9/2007
				
				dw_report.setitem(ll_i, "flag_urgente",   ls_flag_urgente)
				dw_report.setitem(ll_i, "flag_valorizza", ls_flag_valorizza)
				
				ls_cod_reparto 	= ls_null
				ll_stato_reparto 	= ll_null
				
				ll_ret = luo_prod.uof_stato_prod_det_ord_ven_reparti( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ref ls_cod_reparto[], ref ll_stato_reparto[], ref ls_messaggio )
				if ll_stato_prod_ordine = -1 then
					g_mb.messagebox("APICE",ls_messaggio,stopsign!)
					return -1
				end if
				
				if ll_num_registrazione=21103 then
					ll_num_registrazione = ll_num_registrazione
				end if
				
				ld_tot_colli = 0
				li_stati_bolla[] = li_null[]
				ls_dep[] = ls_null[]
				
				//###################################################################################
				for ll_y = 1 to 5
					
					//Donato 10/05/2011
					//su lasciapassare di Beatrice e Fabio, anche se lo stato riga è Evaso
					//devono essere visualizzati i colli
					if ls_flag_evasione = "E" then
						dw_report.setitem(ll_i, "rep_" + string(ll_y) + "_stato", "E")
						//continue
					end if
					
					if upperbound(ls_cod_reparto) < ll_y then exit
					
					select sigla, cod_deposito
					into   :ls_sigla, :ls_dep[ll_y]
					from   anag_reparti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_reparto = :ls_cod_reparto[ll_y];
							 

					choose case ll_stato_reparto[ll_y]
						case 0
							dw_report.setitem(ll_i, "rep_" + string(ll_y), ls_sigla)
							
						case 1
							dw_report.setitem(ll_i, "rep_" + string(ll_y), "*"+ ls_sigla + "*")
							
						case 2
							dw_report.setitem(ll_i, "rep_" + string(ll_y), ">"+ ls_sigla)

					end choose
					
					
					//Donato 20/03/2004
					//posizione -------------------------------------------------
					luo_prod.uof_get_posizione( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_reparto[ll_y], ls_posizione, ls_messaggio)
					dw_report.setitem(ll_i, "pos_" + string(ll_y), ls_posizione)
					ls_messaggio = ""
					
					
					// -------------------------------------------------------------
					try
						if ls_cod_reparto[ll_y]<>"" and not isnull(ls_cod_reparto[ll_y]) then
							ls_flag_stato_bolla = ""
							ls_cod_dep_filtro = f_des_tabella("anag_reparti", "cod_reparto='"+ls_cod_reparto[ll_y]+"'", "cod_deposito")
							ll_ret = luo_prod.uof_stato_prod_det_ord_ven_trasf(	ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, &
																									ls_cod_dep_filtro, ls_cod_reparto[ll_y], ll_stato_reparto[ll_y], ref ls_flag_stato_bolla, ref ls_messaggio)
							if ll_ret = -1 then
								g_mb.error(ls_messaggio)
								exit
							end if
							
							if not isnull(ls_flag_stato_bolla) then dw_report.setitem(ll_i,"dep_" + string(ll_y), ls_flag_stato_bolla)
							
							li_stati_bolla[ll_y] = ll_ret
							
						end if
					catch (throwable err)
					end try 
					// -------------------------------------------------------------
				
					
					if (ls_flag_tipo_stampa="B" or ls_flag_tipo_stampa="C") and ll_pos_riga>1 then
						//trattasi di una riga successiva alla prima di un ordine B, o C
						dw_report.setitem(ll_i, "num_colli_letti", 0)
						dw_report.setitem(ll_i, "num_colli_previsti", 0)
					else
						//prima riga di un ordine B,C, oppure qualsiasi riga di ordine tipo A
						dw_report.setitem(ll_i, "num_colli_letti", ll_num_colli_letti)
						dw_report.setitem(ll_i, "num_colli_previsti", ll_num_colli_previsti)
					end if
					
					ls_sigla_sosp_reparto = ""

					//Donato 10/05/2011
					//Beatrice ha richiesto questo
					//if (ls_flag_tipo_stampa="B" or ls_flag_tipo_stampa="C") and ll_pos_riga>1 then
					
					ls_sigla_sosp_reparto = ""
					if ls_flag_tipo_stampa="B" or ls_flag_tipo_stampa="C" then
						//trattasi di una riga successiva alla prima di un ordine B, o C
						//Beatrice vuole vedere i colli solo sulla prima riga, sulle altre no
						
						//nascondi il tot colli e relativa label
						//dw_report.setitem(ll_i,"flag_colli","N")
						
						ld_num_colli = dec(luo_colli_sfuso.get(ls_sigla))
						if isnull(ld_num_colli) then ld_num_colli = 0
						ld_tot_colli += ld_num_colli
						
						dw_report.setitem(ll_i, "num_colli_rep_" + string(ll_y), ld_num_colli)
						luo_colli_sfuso.set(ls_sigla,0)
						
						//Donato 22/01/2012
						//visualizza sigla sospensione, se attivata, sul reparto corrispondente
						//###############################################################
						luo_prod.uof_get_sospensioni_reparto(ls_cod_reparto[ll_y], ll_anno_registrazione, ll_num_registrazione, 0, ls_sigla_sosp_reparto )
						//###############################################################
						
					else
						//prima riga di un ordine B,C, oppure qualsiasi riga di ordine tipo A
					
						//cerca nella tab_ord_ven_colli
						if wf_get_sum_colli(ls_flag_tipo_stampa,ll_anno_registrazione,ll_num_registrazione,&
									ll_prog_riga_ord_ven,ls_cod_reparto[ll_y],ld_num_colli,ls_messaggio) < 0 then
							
							g_mb.messagebox("APICE",ls_messaggio,stopsign!)
							close cu_n_giri;
							close cu_cliente_giro;
							close cu_prodotti;
							
							return -1
						end if
										
						ld_tot_colli += ld_num_colli
						
						dw_report.setitem(ll_i, "num_colli_rep_" + string(ll_y), ld_num_colli)
						
						//Donato 22/01/2012
						//visualizza sigla sospensione, se attivata, sul reparto corrispondente
						//###############################################################
						luo_prod.uof_get_sospensioni_reparto(ls_cod_reparto[ll_y], ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_sigla_sosp_reparto )
						//###############################################################
					end if
					
					//Donato 22/01/2012
					//visualizza sigla sospensione, se attivata, sul reparto corrispondente
					//###############################################################
					dw_report.setitem(ll_i, "sosp_" + string(ll_y), ls_sigla_sosp_reparto)
					//###############################################################
				next
				//###################################################################################
				
				//ciclo sulle 5 caselle terminato, se ho attivato il filtro sullo stato tarsferimento lo controllo     
				if li_stato_trasf_filtro <> 7 then
					
					//se riga collegata inutile verificare lo stato trasferimento reparti, anche perchè reparti non ne hanno
					//quindi la regola generale è che la riga collegata sarà visibile nel report se è visibile la riga principale
					if ll_num_riga_appartenenza>0 then
						if dw_report.find(	"anno_registrazione="+string(ll_anno_registrazione)+" and "+&
												"num_registrazione="+string(ll_num_registrazione)+" and "+&
												"prog_riga_ord_ven="+string(ll_num_riga_appartenenza), &
												dw_report.rowcount(), 1) > 0 then
							//riga principale presente nel report
						else
							//riga principale NON presente nel report
							//la riga è da escludere
							dw_report.deleterow(ll_i)
							continue
						end if
						
					else
						//non è una riga collegata
						if not wf_check_filtro_trasf(ls_cod_deposito_origine ,ls_cod_reparto[], ls_dep[], li_stati_bolla[], li_stato_trasf_filtro) then
							//la riga è da escludere
							dw_report.deleterow(ll_i)
							continue
						end if
					end if
					
				end if
				
				
				if ls_flag_riga_sospesa = "S" then
					select sigla
					into   :ls_sigla
					from   tab_tipi_cause_sospensione
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_tipo_causa_sospensione = :ls_cod_tipo_causa_sospensione;
							 
					dw_report.setitem(ll_i, "sigla_sospeso", ls_sigla)
							 
				end if
														 
				dw_report.setitem(ll_i, "imponibile_iva", ld_imponibile_iva)
				
				//#######################################################
				//AGGIRONAMENTO PESO E COLLI CLIENTE IN DATASTORE PER RIASSUNTO
				wf_aggiorna_ds_riassunto_det(	lds_peso, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_cliente, &
															ld_peso_netto_unitario * ld_qta_per_peso, dw_report.object.cf_tot_colli[ll_i])
				//#######################################################
				
			loop
			
			close cu_prodotti;
				
		loop
		
		close cu_cliente_giro;
		
	end if
	
loop

close cu_n_giri;

rollback;

//dw_report.setsort("cod_giro A, prog_giro A, cod_cliente A, anno_registrazione A, num_registrazione A, prog_riga_ord_ven A") 
dw_report.setsort("cod_giro A, prog_giro A, cod_cliente A, pagamento A, cod_tipo_doc A, num_registrazione A, prog_riga_ord_ven A") 
dw_report.sort()
//
setpointer(arrow!)

dw_report.object.t_1.text = 'R E P O R T    G I R I    D I    C O N S E G N A  '
if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato") = 'R' or dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato") = 'P' then
	dw_report.object.t_1.text = 'R E P O R T    G I R I    D I    C O N S E G N A  (R)'
end if

//*** Michela 20/03/2006: ho sostituito il quadratino pieno con un triangolo pieno nero
dw_report.Object.p_tutto.Filename = s_cs_xx.volume + s_cs_xx.risorse + "triangolo.bmp"


// impostazione del font
integer li_bco,li_risposta
string ls_bfo, ls_errore

li_risposta = f_font_barcode(ls_bfo,li_bco,ls_errore)

if li_risposta < 0 then 
	messagebox("SEP","Report giri consegna: " + ls_errore,stopsign!)
	return -1
end if

// EnMe 01/2012:  fissato per questo report
li_bco = 16

dw_report.Modify("barcode_ft.Font.Face='" + ls_bfo + "'")
dw_report.Modify("barcode_ft.Font.Height= -" + string(li_bco) )
dw_report.Modify("barcode_bl.Font.Face='" + ls_bfo + "'")
dw_report.Modify("barcode_bl.Font.Height= -" + string(li_bco) )
// 

//dw_report.Object.DataWindow.Print.Preview = 'Yes'
//dw_report.Object.DataWindow.Print.Preview.Outline = 'No'

st_1.text = "Report Pronto."

dw_report.groupcalc()

//faccio qui il setredraw a TRUE perchè deve ridisegnare il riepilogo peso per cliente!!!
dw_report.setredraw(true)
//------------------------------------------------------------------------------------------------

//wf_crea_riepilogo_peso(lds_peso)
wf_mostra_ds_riassunto(lds_peso)
destroy lds_peso

dw_report.object.DataWindow.Print.Preview = 'Yes'
dw_report.Object.DataWindow.Print.Preview.Outline = 'No'

dw_report.setredraw(true)

dw_report.change_dw_current()

dw_report.resetupdate()
end function

public function integer wf_report_lista_carico ();datetime ldt_data_da,ldt_data_a, ldt_data_reg, ldt_data_consegna

string   	ls_cod_giro, ls_cod_cliente, ls_des_giro, ls_des_cliente, ls_localita, ls_cod_prodotto, ls_cod_deposito_origine, &
         	ls_des_prodotto, ls_misura, ls_cod_giro_consegna, ls_sql,ls_flag_blocco, ls_provincia, &
			ls_messaggio, ls_cod_cliente_selezione, ls_rif_interscambio, ls_num_ord_cliente, ls_localita_dest, &
			ls_cod_reparto[],ls_flag_riga_sospesa,ls_cod_tipo_causa_sospensione,ls_flag_urgente,ls_sigla, &
			ls_flag_evasione,ls_cod_tipo_det_ven,ls_flag_tipo_det_ven,ls_cod_valuta, ls_null[],ls_flag_valorizza, &
			ls_barcode, ls_cod_tipo_ord_ven, ls_flag_tipo_bol_fat, ls_cap, ls_telefono, ls_fax, ls_cellulare, ls_internet, &
			ls_des_pagamento, ls_note,ls_cod_pagamento,ls_indirizzo, ls_cod_tessuto, ls_cod_verniciatura, ls_des_tessuto, &
			ls_des_verniciatura, ls_colore_tessuto, ls_error, ls_des_colore_tessuto, ls_riga_n_di_m, ls_posizione, ls_posizione_filtro, ls_flag_evasione_tes, &
			ls_cod_deposito_giro_sel, ls_cod_deposito_origine_sel, ls_cod_deposito_giro

long     	ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_prog_giro_consegna, &
			ll_pos, ll_stato_prod_filtro, ll_stato_prod_ordine, ll_num_ord, ll_anno_ord, ll_num_ord_2, &
			ll_anno_ord_2, ll_riga, ll_controllo, ll_stato_ordine, ll_stato_reparto[], ll_ret, ll_y, ll_null[], &
			ll_anno_filtro, ll_num_filtro, ll_anno_registrazione_old, ll_num_registrazione_old, ll_righe_colli, ll_id_spedizione, &
			ll_num_colli_letti, ll_num_colli_previsti
			
double 	ld_sconto[], ld_prezzo_vendita, ld_val_riga, ld_cambio_ven

decimal  ld_dim_x, ld_dim_y, ld_dim_z, ld_quan_ordine, ld_quan_in_evasione, ld_quan_evasa, ld_imponibile_iva, &
			ld_num_colli, ld_tot_colli, ld_peso_netto_unitario, ld_qta_per_peso

string		ls_flag_tipo_stampa, ls_flag_stato_bolla, ls_cod_dep_filtro, ls_dep[], ls_flag_non_trasf, ls_flag_in_trasf, ls_flag_trasf

long 		ll_num_riga_appartenenza, ll_pos_riga

integer	li_stati_bolla[], li_null[], li_stato_trasf_filtro, li_ret

uo_produzione luo_prod
datastore lds_store, lds_peso
uo_array luo_colli_sfuso


st_1.text = "Attendere il termine dell'elaborazione !!! "
dw_report.setredraw(false)
luo_colli_sfuso = create uo_array

//Donato 24/03/2014
//Creazione DS per riepilogo peso per cliente ##############################
wf_crea_ds_riepilogo_peso(lds_peso)
//##########################################################

dw_selezione.accepttext()
setpointer(hourglass!)

dw_report.Object.DataWindow.Print.preview = "no"
dw_report.Object.DataWindow.Print.preview.rulers = "no"

dw_report.reset()
dw_riassunto.reset()

ll_anno_filtro = dw_selezione.getitemnumber(dw_selezione.getrow(),"anno_registrazione")
ll_num_filtro = dw_selezione.getitemnumber(dw_selezione.getrow(),"num_registrazione")

ldt_data_da = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_inizio")
if isnull(ldt_data_da) or ldt_data_da <= datetime(date("01/01/1900"), 00:00:00) then
	ldt_data_da = datetime(date("01/01/1900"), 00:00:00)
end if

ldt_data_a = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_fine")
if isnull(ldt_data_a) or ldt_data_a <= datetime(date("01/01/1900"), 00:00:00) then
	ldt_data_a = datetime(date("31/12/2099"), 00:00:00)
end if	

ll_id_spedizione = dw_selezione.getitemnumber(dw_selezione.getrow(),"id_spedizione")

ls_cod_giro_consegna = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_giro")

if g_str.isempty(ls_cod_giro_consegna) then
	//giro consegna non impostato (verifica se richiedi un particolare deposito giro consegna)
	ls_cod_giro_consegna = "%"
	
	ls_cod_deposito_giro_sel = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_deposito_giro")
	if g_str.isempty(ls_cod_deposito_giro_sel) then setnull(ls_cod_deposito_giro_sel)		//ls_cod_deposito_giro_sel = "%"	
else
	//GIRO consegna impostato, inutile filtrare per deposito giro consegna
	setnull(ls_cod_deposito_giro_sel)		//ls_cod_deposito_giro_sel = "%"
end if

ls_cod_deposito_origine_sel = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_deposito_origine")
if g_str.isempty(ls_cod_deposito_origine_sel) then
	ls_cod_deposito_origine_sel = "%"
end if

ls_posizione_filtro = dw_selezione.getitemstring(dw_selezione.getrow(),"posizione")
li_stato_trasf_filtro = wf_imposta_filtro_trasf()

ls_cod_cliente_selezione = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_cliente")

if isnull(ls_cod_cliente_selezione) or ls_cod_cliente_selezione = "" then
	ls_cod_cliente_selezione = "%"
end if

ls_flag_non_trasf = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_non_ancora_trasf")
ls_flag_in_trasf = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_in_trasf")
ls_flag_trasf = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_trasferiti")

ll_stato_prod_filtro = dw_selezione.getitemnumber(dw_selezione.getrow(),"flag_avan_prod")

ls_flag_valorizza = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_valorizza")

declare  cu_prodotti dynamic cursor for sqlsa;

declare  cu_n_giri cursor for
select   tes_ord_ven.cod_giro_consegna 
from     tes_ord_ven
join tes_giri_consegne on 	tes_giri_consegne.cod_azienda=tes_ord_ven.cod_azienda and
									tes_giri_consegne.cod_giro_consegna=tes_ord_ven.cod_giro_consegna
where    tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
	      	tes_ord_ven.data_consegna >= :ldt_data_da and tes_ord_ven.data_consegna <= :ldt_data_a and
			tes_ord_ven.cod_giro_consegna like :ls_cod_giro_consegna and
			tes_ord_ven.cod_cliente like :ls_cod_cliente_selezione and 
			(:ll_anno_filtro is null or tes_ord_ven.anno_registrazione = :ll_anno_filtro) and
			(:ll_num_filtro is null or tes_ord_ven.num_registrazione = :ll_num_filtro) and
			tes_ord_ven.cod_deposito like :ls_cod_deposito_origine_sel and
			(:ls_cod_deposito_giro_sel is null or tes_giri_consegne.cod_deposito like :ls_cod_deposito_giro_sel)
group by tes_ord_ven.cod_giro_consegna;

//Donato 15/07/2014: Chiesto da Alberto e Beatrice
//al posto di flag_urgente della det_ord_ven leggere il flag_tassativo dalla tes_ord_ven ed utilizzare lo stesso campo nella dw

declare  cu_cliente_giro cursor for
select   anno_registrazione, 
			num_registrazione, 
			cod_cliente, 
			data_registrazione, 
			data_consegna, 
			rif_interscambio, 
			num_ord_cliente, 
			cambio_ven, 
			cod_valuta, 
			cod_tipo_ord_ven,
			cod_pagamento,
			cod_deposito,
			flag_tassativo,
			flag_evasione
from     tes_ord_ven
where	cod_azienda = :s_cs_xx.cod_azienda and
			data_consegna >= :ldt_data_da and data_consegna <= :ldt_data_a and
			cod_giro_consegna = :ls_cod_giro and flag_blocco <> 'S' and
			cod_cliente like :ls_cod_cliente_selezione and
			(:ll_anno_filtro is null or anno_registrazione = :ll_anno_filtro) and
			(:ll_num_filtro is null or num_registrazione = :ll_num_filtro) and
			cod_deposito like :ls_cod_deposito_origine_sel
order by cod_cliente;

open cu_n_giri;
if sqlca.sqlcode > 0 then
	g_mb.messagebox("APICE","Errore nella open del cursore (cu_n_giri).~r~n"+sqlca.sqlerrtext)
	rollback;
	return -1 //####
end if

do while true
	
	fetch cu_n_giri
	into  :ls_cod_giro;
	
	if sqlca.sqlcode = 100 then exit

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore nella FETCH del cursore (cu_n_giri).~r~n"+sqlca.sqlerrtext)
		close cu_n_giri;
		
		rollback;
		return -1 //#####
	end if
	
	st_1.text = "Attendere il termine dell'elaborazione !!! " + ls_cod_giro
	
   if sqlca.sqlcode = 0 then
		
		select des_giro_consegna 
		into  :ls_des_giro 
		from tes_giri_consegne 
		where cod_azienda = :s_cs_xx.cod_azienda and
		      cod_giro_consegna like :ls_cod_giro;
				
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Errore", "La descrizione del codice giro '" + ls_cod_giro + "' non trovata")
		end if
		
		
		if g_str.isnotempty(ls_cod_giro) then
			//deposito giro consegna
			select cod_deposito
			into :ls_cod_deposito_giro
			from tes_giri_consegne
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						cod_giro_consegna=:ls_cod_giro;
		else
			setnull(ls_cod_deposito_giro)
		end if
					
		
		open cu_cliente_giro;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore nella open del cursore (cu_cliente_giro).~r~n"+sqlca.sqlerrtext)
			close cu_n_giri;
			
			rollback;
			return -1 //####
		end if
		
		do while TRUE
			
			fetch cu_cliente_giro
			into  :ll_anno_registrazione,
			      :ll_num_registrazione,
					:ls_cod_cliente,
					:ldt_data_reg,
					:ldt_data_consegna,
					:ls_rif_interscambio,
					:ls_num_ord_cliente,
					:ld_cambio_ven,
					:ls_cod_valuta,
					:ls_cod_tipo_ord_ven,
					:ls_cod_pagamento,
					:ls_cod_deposito_origine,
					:ls_flag_urgente,
					:ls_flag_evasione_tes;
					
			if sqlca.sqlcode = 100 then exit 
			
			st_1.text = "Attendere il termine dell'elaborazione !!! "+ ls_cod_giro + "/" + ls_cod_cliente
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore nella FETCH del cursore (cu_cliente_giro).~r~n"+sqlca.sqlerrtext)
				close cu_n_giri;
				close cu_cliente_giro;
				
				rollback;
				return -1 //####
			end if
	
			//se il giro consegna ha un deposito considera questo al posto del deposito origine ordine
			if g_str.isnotempty(ls_cod_deposito_giro) then ls_cod_deposito_origine = ls_cod_deposito_giro
			
			if isnull(ls_flag_evasione_tes) or ls_flag_evasione_tes="" then ls_flag_evasione_tes = "A"
	
			choose case dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato")
				case "T"
					
				case "R"
					if ls_flag_evasione_tes<>"A" and ls_flag_evasione_tes<>"P" then continue
					
				case "E"
					if ls_flag_evasione_tes<>"E" then continue
					
				case "P"
					if ls_flag_evasione_tes<>"P" then continue
					
				case "A"
					if ls_flag_evasione_tes<>"A" then continue
					
			end choose
	
	
			if ls_flag_urgente="" or isnull(ls_flag_urgente) then ls_flag_urgente="N"
	
	
			//in questo cursore vado a leggere il tipo ordine (A, B, C) -----------------------------------------------
			if wf_get_tipo_ordine(ll_anno_registrazione, ll_num_registrazione, ls_flag_tipo_stampa, ls_messaggio)<0 then
				g_mb.error("APICE",ls_messaggio)
				close cu_n_giri;
				close cu_cliente_giro;
				
				rollback;
				return -1 //####
			end if
			//---------------------------------------------------------------------------------------------------------
			

			//carico eventuale destinazione diversa dell'ordine
			ls_localita_dest = wf_destinazione_diversa(ll_anno_registrazione, ll_num_registrazione)
			
			select rag_soc_1, 
					 indirizzo, 
					 localita, 
					 provincia, 
					 cap,
					 telefono, 
					 fax, 
					 telex, 
					 sito_internet
			into 	 :ls_des_cliente, 
			       :ls_indirizzo, 
					 :ls_localita, 
					 :ls_provincia, 
					 :ls_cap, 
					 :ls_telefono, 
					 :ls_fax, 
					 :ls_cellulare, 
					 :ls_note
			from anag_clienti
			where cod_azienda = :s_cs_xx.cod_azienda and 
					cod_cliente = :ls_cod_cliente;
					
			if sqlca.sqlcode <> 0 then 
				g_mb.messagebox("Rag. soc./ localita", "Ragione sociale e localita del codice cliente '" + ls_cod_cliente + "' non trovate")
			end if
			
			select des_pagamento
			into   :ls_des_pagamento
			from	 tab_pagamenti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_pagamento = :ls_cod_pagamento;
			if sqlca.sqlcode <> 0 then
				ls_des_pagamento = ""
			end if
			
			select prog_giro_consegna
			into   :ll_prog_giro_consegna
			from   det_giri_consegne
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_giro_consegna = :ls_cod_giro and
					 cod_cliente = :ls_cod_cliente;
					 
			if sqlca.sqlcode = 100 then
				ll_prog_giro_consegna = 999999
			elseif sqlca.sqlcode = -1 then
				g_mb.messagebox("APICE","Errore in ricerca progressivo giro in tabella dettaglio giri consegne. Dettaglio=" + sqlca.sqlerrtext)
				ll_prog_giro_consegna = 999999
			end if
			
			
			//#######################################################
			//IMPOSTAZIONE RIGA CLIENTE In DATASTORE PER RIASSUNTO
			wf_aggiorna_ds_riassunto_tes(lds_peso, ll_anno_registrazione,ll_num_registrazione, ls_cod_cliente)
			//#######################################################
			
			//cursore per righe prodotti -----------				
 			//preparazione SQL per le righe
			ll_pos_riga = 0

			ls_sql = " select cod_prodotto, des_prodotto, quan_ordine, cod_misura, prog_riga_ord_ven, quan_in_evasione, quan_evasa," + &
			 					"imponibile_iva, flag_riga_sospesa, cod_tipo_causa_sospensione, flag_evasione," + &
								"sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10,"+&
								"prezzo_vendita, cod_tipo_det_ven, num_riga_appartenenza,isnull(peso_netto, 0) "+ &
						" from det_ord_ven " + &
						" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
								 "anno_registrazione = " + string(ll_anno_registrazione) + " and " + &
								 "num_registrazione = " + string(ll_num_registrazione)
			
			choose case dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato")
				case "R" // residui
					ls_sql = ls_sql + " and flag_evasione in ('A','P')"
				case "E"	// Solo evasi
					ls_sql = ls_sql + " and flag_evasione ='E' "
				case "A"	// Solo aperti
					ls_sql = ls_sql + " and flag_evasione ='A' "
				case "P"	// solo parziali
					ls_sql = ls_sql + " and flag_evasione ='P' "
			end choose
			
			choose case dw_selezione.getitemstring(dw_selezione.getrow(),"flag_sospesi")
				case "S"
					ls_sql = ls_sql + " and flag_riga_sospesa = 'S' "
				case "N"
					ls_sql = ls_sql + " and flag_riga_sospesa = 'N' "
			end choose
					
			ls_sql = ls_sql + " order by prog_riga_ord_ven "
			
			prepare  SQLSA FROM :ls_sql;
			
			open dynamic cu_prodotti;
			
			if sqlca.sqlcode > 0 then
				g_mb.messagebox("APICE","Errore nella open del cursore (cu_prodotti).~r~n"+sqlca.sqlerrtext)
				close cu_n_giri;
				close cu_cliente_giro;
				
				rollback;
				return -1 //####
			end if
			// OGNI NUOVO ORDINE:
			ll_controllo = 0
			
			do while true
				fetch cu_prodotti
				into  :ls_cod_prodotto,
						:ls_des_prodotto,
						:ld_quan_ordine,
						:ls_misura,
						:ll_prog_riga_ord_ven,
						:ld_quan_in_evasione,
						:ld_quan_evasa,
						:ld_imponibile_iva,
						:ls_flag_riga_sospesa,
						:ls_cod_tipo_causa_sospensione,
						:ls_flag_evasione,
						:ld_sconto[1],
						:ld_sconto[2],
						:ld_sconto[3],
						:ld_sconto[4],
						:ld_sconto[5],
						:ld_sconto[6],
						:ld_sconto[7],
						:ld_sconto[8],
						:ld_sconto[9],
						:ld_sconto[10],
						:ld_prezzo_vendita,
						:ls_cod_tipo_det_ven,
						:ll_num_riga_appartenenza,
						:ld_peso_netto_unitario;
						
				if sqlca.sqlcode = 100 then exit
				
				st_1.text = "Attendere il termine dell'elaborazione !!!  "+ ls_cod_giro + "/" + ls_cod_cliente + "/" + ls_cod_prodotto
				
				if sqlca.sqlcode > 0 then
					g_mb.messagebox("APICE","Errore nella FETCH del cursore (cu_prodotti).~r~n"+sqlca.sqlerrtext)
					close cu_n_giri;
					close cu_cliente_giro;
					close cu_prodotti;
					
					rollback;
					return -1 //####
				end if
				
				
				if ls_flag_blocco = "S" then continue
				
				//----------------------------------------------------------------------------
				//se risulta specificato il filtro per ID spedizione verifica che l'ordine che stai elaborando è tra quelli con i colli scansionati
				li_ret = wf_buffer_colli(	ll_id_spedizione, ls_flag_tipo_stampa, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, &
												ll_num_colli_letti, ll_num_colli_previsti, ls_messaggio)
				
				choose case li_ret
					case is < 0
						g_mb.error("APICE", ls_messaggio)
						close cu_n_giri;
						close cu_cliente_giro;
						close cu_prodotti;
						
						rollback;
						return -1
					
					case 0
						//ignora il controllo e  avanti con l'elaborazione
					
					case 1
						////salta la riga
						//continue
						
					case 2
						//avanti con l'elaborazione
					
				end choose
				//----------------------------------------------------------------------------
				
				luo_prod = create uo_produzione
				
				//Donato 20/03/2014: filtro per posizione (se impostata nel filltro cerca solo quelle con tale posizione)
				if ls_posizione_filtro<>"" and not isnull(ls_posizione_filtro) then
					//se riga collegata inutile verificare il filtro per posizione anche perchè le tabelle delle fasi,
					//det_ordini_prioduzione e tes_ordini_produzione sono vuote per le righe collegate
					//quindi la regola generale è che la riga collegata sarà visibile nel report se è visibile la riga principale
					
					if ll_num_riga_appartenenza>0 then
						//è una riga collegata ---------------------------------------
						if dw_report.find(	"anno_registrazione="+string(ll_anno_registrazione)+" and "+&
												"num_registrazione="+string(ll_num_registrazione)+" and "+&
												"prog_riga_ord_ven="+string(ll_num_riga_appartenenza), &
												dw_report.rowcount(), 1) > 0 then
							//riga principale PRESENTE nel report, quindi anche la riga collegata lo deve essere
						else
							//riga principale NON presente nel report, quindi la riga è da escludere
							continue
						end if
						
					else
						//è una riga principale -----------------------------------
						//verifica se la posizione della riga è quella richiesta nel filtro
						if not luo_prod.uof_check_posizione(ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,ls_posizione_filtro) then
							continue
						end if
					end if
				else
					//filtro per posizione non specificato
				end if
				
				ll_stato_prod_ordine = luo_prod.uof_stato_prod_det_ord_ven(ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,ls_messaggio)
				
				if ll_stato_prod_ordine = -1 then
					g_mb.messagebox("APICE",ls_messaggio,stopsign!)
					close cu_n_giri;
					close cu_cliente_giro;
					close cu_prodotti;
					
					return -1
				end if
				
				if ll_stato_prod_filtro <> -1 then
					if ll_stato_prod_filtro = 3 then
						if ll_stato_prod_ordine <> 0 and ll_stato_prod_ordine <> 2 then
							continue
						end if
					else
						if ll_stato_prod_ordine <> ll_stato_prod_filtro then
							continue
						end if
					end if
				end if
				
				
				// stefanop 11/04/2012 - recupero i colli dei reparti per lo sfuso, solo se è una nuova testata d'ordine
				//-------------------------------------------------------------------------------------
				if (ls_flag_tipo_stampa = "B" or ls_flag_tipo_stampa = "C") and&
					ll_anno_registrazione <> ll_anno_registrazione_old or ll_num_registrazione <> ll_num_registrazione_old then 
					destroy lds_store
					luo_colli_sfuso.removeall()
					
					ls_sql = "SELECT cod_reparto, count(*) FROM tab_ord_ven_colli WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND " + &
								"anno_registrazione=" + string(ll_anno_registrazione) + " AND " + &
								"num_registrazione=" + string(ll_num_registrazione) + &
								" GROUP BY cod_reparto"
					
					ll_righe_colli = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)
					if ll_righe_colli < 0 then
						g_mb.error(ls_error)
						return -1
					end if
					
					for ll_i = 1 to ll_righe_colli
						luo_colli_sfuso.set(lds_store.getitemstring(ll_i, 1), lds_store.getitemnumber(ll_i, 2))
					next
					
					ll_anno_registrazione_old = ll_anno_registrazione
					ll_num_registrazione_old = ll_num_registrazione 
				end if
				//-------------------------------------------------------------------------------------
				
				
				//*** Michela 20/03/2006:controllo lo stato dell'ordine
				ll_stato_ordine = -1
				ll_stato_ordine = luo_prod.uof_stato_prod_tes_ord_ven(ll_anno_registrazione,ll_num_registrazione,ls_messaggio)
				if ll_stato_ordine = 1 then // tutto prodotto
					ll_stato_prod_ordine	= 3
				end if					
				
				// modifica claudia 26/01/06 per aggiungedere dei dati
				//inseriti due campi R.I. e rif. vs. ordine come da richiesta del cliente
				if ll_controllo = 0 and len(ls_rif_interscambio) > 0 and not isnull(ls_rif_interscambio) then
					ll_i = dw_report.insertrow(0)
					dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
					dw_report.setitem(ll_i, "des_giro", ls_des_giro)
					dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
					dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
					dw_report.setitem(ll_i, "localita", ls_cap + " " + ls_localita)
					dw_report.setitem(ll_i, "provincia", ls_provincia)
					dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
					dw_report.setitem(ll_i, "cod_prodotto", "R.I.")
					dw_report.setitem(ll_i, "des_prodotto", ls_rif_interscambio)
					dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
					dw_report.setitem(ll_i, "flag_tipo_riga", "S")
					//messo 0 perchè così li mette prima dell'ordine
					dw_report.setitem(ll_i, "prog_riga_ord_ven", 0)
					dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)
					
					dw_report.setitem(ll_i, "indirizzo", ls_indirizzo)
					dw_report.setitem(ll_i, "telefono", ls_telefono)
					dw_report.setitem(ll_i, "fax", ls_fax)
					dw_report.setitem(ll_i, "cellulare", ls_cellulare)
					dw_report.setitem(ll_i, "note", ls_note)
					dw_report.setitem(ll_i, "pagamento", ls_des_pagamento)
					dw_report.setitem(ll_i, "flag_valorizza", ls_flag_valorizza)
					
					//18/05/2001 richiesta di Beatrice -----------------------------
					//se il prog_riga_ord_ven è 0 nascondi i colli e relativa label
					//nascondi il tot colli e relativa label
					dw_report.setitem(ll_i,"flag_colli","N")
					//---------------------------------------------------------------
					
					//destinazione diversa, se c'è
					dw_report.setitem(ll_i,"localita_dest", ls_localita_dest)
					
					ll_controllo = 1
					if len(ls_num_ord_cliente) > 0 and not isnull(ls_num_ord_cliente) then
						ll_i = dw_report.insertrow(0)
						dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
						dw_report.setitem(ll_i, "des_giro", ls_des_giro)
						dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
						dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
						dw_report.setitem(ll_i, "localita", ls_cap + " " + ls_localita)
						dw_report.setitem(ll_i, "provincia", ls_provincia)
						dw_report.setitem(ll_i, "cod_prodotto", "R.VS.ORD.")
						dw_report.setitem(ll_i, "des_prodotto", ls_num_ord_cliente)
						dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
						dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
						dw_report.setitem(ll_i, "prog_riga_ord_ven", 0)
						dw_report.setitem(ll_i, "flag_tipo_riga", "S")
						dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)
						
						dw_report.setitem(ll_i, "indirizzo", ls_indirizzo)
						dw_report.setitem(ll_i, "telefono", ls_telefono)
						dw_report.setitem(ll_i, "fax", ls_fax)
						dw_report.setitem(ll_i, "cellulare", ls_cellulare)
						dw_report.setitem(ll_i, "note", ls_note)
						dw_report.setitem(ll_i, "pagamento", ls_des_pagamento)
						dw_report.setitem(ll_i, "flag_valorizza", ls_flag_valorizza)
						
						//18/05/2001 richiesta di Beatrice -----------------------------
						//se il prog_riga_ord_ven è 0 nascondi i colli e relativa label
						//nascondi il tot colli e relativa label
						dw_report.setitem(ll_i,"flag_colli","N")
						//---------------------------------------------------------------
						
						//destinazione diversa, se c'è
						dw_report.setitem(ll_i,"localita_dest", ls_localita_dest)
						
					end if
					
				else
					
					if ll_controllo = 0 and len(ls_num_ord_cliente) > 0 and not isnull(ls_num_ord_cliente) then
						ll_i = dw_report.insertrow(0)
						ll_controllo = 1
						dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
						dw_report.setitem(ll_i, "des_giro", ls_des_giro)
						dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
						dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
						dw_report.setitem(ll_i, "localita", ls_cap + " " + ls_localita)
						dw_report.setitem(ll_i, "provincia", ls_provincia)
						dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
						dw_report.setitem(ll_i, "cod_prodotto", "R.VS.ORD.")
						dw_report.setitem(ll_i, "des_prodotto", ls_num_ord_cliente)
						dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
						dw_report.setitem(ll_i, "prog_riga_ord_ven", 0)
						dw_report.setitem(ll_i, "flag_tipo_riga", "S")
						dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)
						
						dw_report.setitem(ll_i, "indirizzo", ls_indirizzo)
						dw_report.setitem(ll_i, "telefono", ls_telefono)
						dw_report.setitem(ll_i, "fax", ls_fax)
						dw_report.setitem(ll_i, "cellulare", ls_cellulare)
						dw_report.setitem(ll_i, "note", ls_note)
						dw_report.setitem(ll_i, "pagamento", ls_des_pagamento)
						dw_report.setitem(ll_i, "flag_valorizza", ls_flag_valorizza)
						
						//18/05/2001 richiesta di Beatrice -----------------------------
						//se il prog_riga_ord_ven è 0 nascondi i colli e relativa label
						//nascondi il tot colli e relativa label
						dw_report.setitem(ll_i,"flag_colli","N")
						//---------------------------------------------------------------
						
						//destinazione diversa, se c'è
						dw_report.setitem(ll_i,"localita_dest", ls_localita_dest)
						
					end if
				end if
				//fine modifica claudia
				
				//inserimento riga dettaglio
				ll_pos_riga += 1
				
				ll_i = dw_report.insertrow(0)	
				dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
				dw_report.setitem(ll_i, "des_giro", ls_des_giro)
				dw_report.setitem(ll_i, "data_registrazione", ldt_data_reg)
				dw_report.setitem(ll_i, "data_consegna", ldt_data_consegna)
				dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
				dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
				dw_report.setitem(ll_i, "localita", ls_cap + " " + ls_localita)
				dw_report.setitem(ll_i, "provincia", ls_provincia)
				dw_report.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
				dw_report.setitem(ll_i, "des_prodotto", ls_des_prodotto)
				
				if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato") = 'R' or dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato") = 'P' then
					ld_qta_per_peso = ld_quan_ordine - ld_quan_in_evasione - ld_quan_evasa
				else
					ld_qta_per_peso = ld_quan_ordine
				end if
				dw_report.setitem(ll_i, "quan_consegna", ld_qta_per_peso)
				
				dw_report.setitem(ll_i, "cod_misura", ls_misura)
				dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
				dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
				dw_report.setitem(ll_i, "prog_riga_ord_ven", ll_prog_riga_ord_ven)
				dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)

				if ls_flag_tipo_stampa = "A" then
					if ll_anno_registrazione>0 and ll_num_registrazione>0 and ll_prog_riga_ord_ven >0 then
						//visualizzazione n°riga/tot_righe
						ls_riga_n_di_m = ""
						ls_riga_n_di_m = luo_prod.uof_get_riga_su_tot_righe(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven)
						dw_report.setitem(ll_i, "riga_tot", ls_riga_n_di_m)
					end if
				end if

				dw_report.setitem(ll_i, "indirizzo", ls_indirizzo)
				dw_report.setitem(ll_i, "telefono", ls_telefono)
				dw_report.setitem(ll_i, "fax", ls_fax)
				dw_report.setitem(ll_i, "cellulare", ls_cellulare)
				dw_report.setitem(ll_i, "note", ls_note)
				dw_report.setitem(ll_i, "pagamento", ls_des_pagamento)
				
				
				dw_report.setitem(ll_i,"stato_prod",ll_stato_prod_ordine)
				
				//destinazione diversa, se c'è
				dw_report.setitem(ll_i,"localita_dest", ls_localita_dest)
				
				
				//Donato 10/05/2011
				//Beatrice ha richiesto che per le righe è riferite (quindi con colli sempre a ZERO)
				//è inutile far vedere COLLI=...
				if ll_num_riga_appartenenza > 0 then
					//riga riferita, nascondi il tot colli e relativa label
					dw_report.setitem(ll_i,"flag_colli","N")
				end if
				
				// stefanop 23/03/2010: ticket 2010/75 aggiunto colore tessuto
				select dim_x, dim_y, dim_z, cod_tessuto, cod_verniciatura, cod_non_a_magazzino
				into   :ld_dim_x, :ld_dim_y, :ld_dim_z, :ls_cod_tessuto, :ls_cod_verniciatura, :ls_colore_tessuto
				from   comp_det_ord_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione and
						 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
						 
				if sqlca.sqlcode = 0 then
					dw_report.setitem(ll_i, "dim_1", ld_dim_x)
					dw_report.setitem(ll_i, "dim_2", ld_dim_y)
					dw_report.setitem(ll_i, "dim_3", ld_dim_z)
					
					select des_prodotto
					into   :ls_des_tessuto
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_tessuto;
					if sqlca.sqlcode = 0 and not isnull(ls_des_tessuto) and len(ls_des_tessuto) > 0 then
						dw_report.setitem(ll_i, "tessuto", ls_cod_tessuto + " " + ls_des_tessuto)
					end if
					
					select des_prodotto
					into   :ls_des_verniciatura
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_verniciatura;
					if sqlca.sqlcode = 0 and not isnull(ls_des_verniciatura) and len(ls_des_verniciatura) > 0 then
						dw_report.setitem(ll_i, "verniciatura", ls_des_verniciatura)
					end if
					
					if ls_colore_tessuto<>"" and not isnull(ls_colore_tessuto) then
					
						select des_colore_tessuto
						into :ls_des_colore_tessuto
						from tab_colori_tessuti
						where 	cod_azienda=:s_cs_xx.cod_azienda and
									cod_colore_tessuto=:ls_colore_tessuto;
						
						if not isnull(ls_des_colore_tessuto) and ls_des_colore_tessuto<>"" then
							ls_colore_tessuto = ls_colore_tessuto + " - " + ls_des_colore_tessuto
						end if
					end if
					
					// stefanop 23/03/2010 aggiunto colore tessuto
					dw_report.setitem(ll_i, "cod_non_a_magazzino", ls_colore_tessuto)
					
				end if
				
				// aggiunto da Enrico per Viropa 19/9/2007
				
				dw_report.setitem(ll_i, "flag_urgente",   ls_flag_urgente)
				dw_report.setitem(ll_i, "flag_valorizza", ls_flag_valorizza)
				
				ls_cod_reparto 	= ls_null
				ll_stato_reparto 	= ll_null
				
				ll_ret = luo_prod.uof_stato_prod_det_ord_ven_reparti( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ref ls_cod_reparto[], ref ll_stato_reparto[], ref ls_messaggio )
				if ll_stato_prod_ordine = -1 then
					g_mb.messagebox("APICE",ls_messaggio,stopsign!)
					close cu_n_giri;
					close cu_cliente_giro;
					close cu_prodotti;
					
					return -1
				end if
				
				ld_tot_colli = 0
				li_stati_bolla[] = li_null[]
				ls_dep[] = ls_null[]
				
				//#################################################################################
				for ll_y = 1 to 5
					
					//Donato 10/05/2011
					//su lasciapassare di Beatrice e Fabio, anche se lo stato riga è Evaso
					//devono essere visualizzati i colli
					if ls_flag_evasione = "E" then
						dw_report.setitem(ll_i, "rep_" + string(ll_y) + "_stato", "E")
						//continue
					end if
					
					if upperbound(ls_cod_reparto) < ll_y then exit
					
					select sigla, cod_deposito
					into   :ls_sigla, :ls_dep[ll_y]
					from   anag_reparti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_reparto = :ls_cod_reparto[ll_y];

					choose case ll_stato_reparto[ll_y]
						case 0
							dw_report.setitem(ll_i, "rep_" + string(ll_y), ls_sigla)
							
						case 1
							dw_report.setitem(ll_i, "rep_" + string(ll_y), "*"+ ls_sigla + "*")
							
						case 2
							dw_report.setitem(ll_i, "rep_" + string(ll_y), ">"+ ls_sigla)

					end choose
					
					//Donato 20/03/2004
					//posizione -------------------------------------------------
					luo_prod.uof_get_posizione( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_reparto[ll_y], ls_posizione, ls_messaggio)
					dw_report.setitem(ll_i, "pos_" + string(ll_y), ls_posizione)
					ls_messaggio = ""
					
					
					// -------------------------------------------------------------
					try
						if ls_cod_reparto[ll_y]<>"" and not isnull(ls_cod_reparto[ll_y]) then
							ls_flag_stato_bolla = ""
							ls_cod_dep_filtro = f_des_tabella("anag_reparti", "cod_reparto='"+ls_cod_reparto[ll_y]+"'", "cod_deposito")
							ll_ret = luo_prod.uof_stato_prod_det_ord_ven_trasf(	ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, &
																									ls_cod_dep_filtro, ls_cod_reparto[ll_y], ll_stato_reparto[ll_y], ref ls_flag_stato_bolla, ref ls_messaggio)
							if ll_ret = -1 then
								g_mb.error(ls_messaggio)
								exit
							end if
							
							if not isnull(ls_flag_stato_bolla) then dw_report.setitem(ll_i,"dep_" + string(ll_y), ls_flag_stato_bolla)
							
							li_stati_bolla[ll_y] = ll_ret
							
						end if
					catch (throwable err)
					end try 
					// -------------------------------------------------------------
					
					if (ls_flag_tipo_stampa="B" or ls_flag_tipo_stampa="C") and ll_pos_riga>1 then
						//trattasi di una riga successiva alla prima di un ordine B, o C
						dw_report.setitem(ll_i, "num_colli_letti", 0)
						dw_report.setitem(ll_i, "num_colli_previsti", 0)
					else
						//prima riga di un ordine B,C, oppure qualsiasi riga di ordine tipo A
						dw_report.setitem(ll_i, "num_colli_letti", ll_num_colli_letti)
						dw_report.setitem(ll_i, "num_colli_previsti", ll_num_colli_previsti)
					end if

					//Donato 10/05/2011
					//Beatrice ha richiesto questo
					//if (ls_flag_tipo_stampa="B" or ls_flag_tipo_stampa="C") and ll_pos_riga>1 then
					if ls_flag_tipo_stampa="B" or ls_flag_tipo_stampa="C" then
						//trattasi di una riga successiva alla prima di un ordine B, o C
						//Beatrice vuole vedere i colli solo sulla prima riga, sulle altre no
						
						//nascondi il tot colli e relativa label
						//dw_report.setitem(ll_i,"flag_colli","N")
						ld_num_colli = dec(luo_colli_sfuso.get(ls_sigla))
						if isnull(ld_num_colli) then ld_num_colli = 0
						ld_tot_colli += ld_num_colli
						
						dw_report.setitem(ll_i, "num_colli_rep_" + string(ll_y), ld_num_colli)
						luo_colli_sfuso.set(ls_sigla,0)
					else
						//prima riga di un ordine B,C, oppure qualsiasi riga di ordine tipo A
						//cerca nella tab_ord_ven_colli
						if wf_get_sum_colli(ls_flag_tipo_stampa,ll_anno_registrazione,ll_num_registrazione,&
									ll_prog_riga_ord_ven,ls_cod_reparto[ll_y],ld_num_colli,ls_messaggio) < 0 then
							
							g_mb.messagebox("APICE",ls_messaggio,stopsign!)
							close cu_n_giri;
							close cu_cliente_giro;
							close cu_prodotti;
							
							return -1
						end if
						
						//###################################################################################
						//se non ho beccato niente leggo da sessioni_lavoro (compatibilità pre-numerazione univoca colli)
						//se poi il tipo ordine è B,C leggo dalla testata dell'ordine i colli
						//quando andrà a regime la tab_ord_ven_colli toglieremo questo IF e le instruzioni in esso contenute
						if ld_num_colli=0 then
							if ls_flag_tipo_stampa="A" then
						
								// EnMe 14-04-2009
								// Per ogni reparto vado a ricavare il numero di colli
								SELECT sum(sessioni_lavoro.num_colli)  
								into   :ld_num_colli
								FROM   sessioni_lavoro RIGHT OUTER JOIN det_ordini_produzione ON sessioni_lavoro.cod_azienda = det_ordini_produzione.cod_azienda AND sessioni_lavoro.progr_det_produzione = det_ordini_produzione.progr_det_produzione  
								WHERE  ( det_ordini_produzione.anno_registrazione = :ll_anno_registrazione ) AND  
										 ( det_ordini_produzione.num_registrazione = :ll_num_registrazione ) AND  
										 ( det_ordini_produzione.prog_riga_ord_ven = :ll_prog_riga_ord_ven )   AND
										 ( det_ordini_produzione.cod_reparto = :ls_cod_reparto[ll_y] )
								GROUP BY sessioni_lavoro.num_colli   ;
						
							else
								//tipo stampa B,C
								select num_colli
								into :ld_num_colli
								from tes_ord_ven
								where cod_azienda = :s_cs_xx.cod_azienda and
										anno_registrazione = :ll_anno_registrazione and
										num_registrazione = :ll_num_registrazione;
							end if
						
							if isnull(ld_num_colli) then ld_num_colli = 0	
						end if
						//###################################################################################
						
						ld_tot_colli += ld_num_colli
						
						dw_report.setitem(ll_i, "num_colli_rep_" + string(ll_y), ld_num_colli)
					end if
				next
				//#################################################################################
				
				//ciclo sulle 5 caselle terminato, se ho attivato il filtro sullo stato tarsferimento lo controllo
				if li_stato_trasf_filtro <> 7 then
					
					//se riga collegata inutile verificare lo stato trasferimento reparti, anche perchè reparti non ne hanno
					//quindi la regola generale è che la riga collegata sarà visibile nel report se è visibile la riga principale
					if ll_num_riga_appartenenza>0 then
						//si tratta di una riga collegata
						if dw_report.find(	"anno_registrazione="+string(ll_anno_registrazione)+" and "+&
												"num_registrazione="+string(ll_num_registrazione)+" and "+&
												"prog_riga_ord_ven="+string(ll_num_riga_appartenenza), &
												dw_report.rowcount(), 1) > 0 then
							//riga principale presente nel report
						else
							//riga principale NON presente nel report
							//la riga è da escludere
							dw_report.deleterow(ll_i)
							continue
						end if
						
					else
						//si tratta di una riga principale
						if not wf_check_filtro_trasf(ls_cod_deposito_origine, ls_cod_reparto[], ls_dep[], li_stati_bolla[], li_stato_trasf_filtro) then
							//la riga è da escludere
							dw_report.deleterow(ll_i)
							continue
						end if
					end if
					
				end if
				
				
				if ls_flag_riga_sospesa = "S" then
					select sigla
					into   :ls_sigla
					from   tab_tipi_cause_sospensione
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_tipo_causa_sospensione = :ls_cod_tipo_causa_sospensione;
							 
					dw_report.setitem(ll_i, "sigla_sospeso", ls_sigla)
							 
				end if
				
				dw_report.setitem(ll_i, "imponibile_iva", ld_imponibile_iva)
				
				dw_report.setitem(ll_i, "peso_netto_unitario", ld_peso_netto_unitario)
				
				//#######################################################
				//AGGIRONAMENTO PESO E COLLI CLIENTE IN DATASTORE PER RIASSUNTO
				wf_aggiorna_ds_riassunto_det(	lds_peso, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_cliente, &
															ld_peso_netto_unitario * ld_qta_per_peso, dw_report.object.cf_tot_colli[ll_i])
				//#######################################################
				
			loop
			
			close cu_prodotti;
				
		loop
		
		close cu_cliente_giro;
		
	end if
	
loop

close cu_n_giri;

rollback;

dw_report.setsort("cod_giro A, prog_giro A, cod_cliente A, anno_registrazione A, num_registrazione A, prog_riga_ord_ven A") 
dw_report.sort()

wf_mostra_ds_riassunto(lds_peso)
destroy lds_peso

setpointer(arrow!)

//*** Michela 20/03/2006: ho sostituito il quadratino pieno con un triangolo pieno nero
dw_report.Object.p_tutto.Filename = s_cs_xx.volume + s_cs_xx.risorse + "triangolo.bmp"

dw_report.Object.DataWindow.Print.Preview = 'Yes'
dw_report.Object.DataWindow.Print.Preview.Outline = 'No'

dw_riassunto.Object.DataWindow.Print.Preview = 'Yes'
dw_riassunto.Object.DataWindow.Print.Preview.Outline = 'No'

st_1.text = "Report Pronto."

dw_report.object.DataWindow.Print.Preview = 'Yes'

dw_report.groupcalc()	

dw_report.setredraw(true)

dw_report.change_dw_current()

dw_report.resetupdate()

end function

public function integer wf_get_tipo_ordine (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_tipo_stampa, ref string fs_msg);/* DEPRECATED */


string ls_cod_tipo_ord_ven, ls_tipo_stampa

select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fl_anno_registrazione
and    num_registrazione=:fl_num_registrazione;

if sqlca.sqlcode < 0 then 
	fs_msg = "Errore in lettura cod_tipo_ord_ven su ordine "+string(fl_anno_registrazione)+"/"+&
							string(fl_num_registrazione)+" "+sqlca.sqlerrtext
	return -1
end if
//----------------------------

select flag_tipo_bcl
into   :fs_tipo_stampa
from   tab_tipi_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;

if sqlca.sqlcode < 0 then 
	fs_msg = "Errore in lettura flag_tipo_bcl su ordine "+string(fl_anno_registrazione)+"/"+&
							string(fl_num_registrazione)+" "+sqlca.sqlerrtext
	return -1
end if
//----------------------------

if isnull(fs_tipo_stampa) or len(fs_tipo_stampa)<=0 then
	fs_msg = "flag_tipo_bcl Non impostato su ordine "+string(fl_anno_registrazione)+"/"+&
							string(fl_num_registrazione)+" "+sqlca.sqlerrtext
	return -1
end if

return 1
end function

public function integer wf_get_sum_colli (string fs_tipo_stampa, long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_reparto, ref decimal fd_num_colli, ref string fs_msg);
//questa funzione legge da tab_ord_ven_num_colli i colli prodotti per ordine
//a seconda del tipo ordine legge per riga ordine (CASO ordini tipo A), oppure con prog riga null (caso tipo B,C)



choose case fs_tipo_stampa
	case "A"
		select count(*)
		into :fd_num_colli
		from tab_ord_ven_colli
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:fl_anno_registrazione and
					num_registrazione=:fl_num_registrazione and
					prog_riga_ord_ven=:fl_prog_riga_ord_ven and
					cod_reparto=:fs_cod_reparto;
					
	case "B","C"
		select count(*)
		into :fd_num_colli
		from tab_ord_ven_colli
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:fl_anno_registrazione and
					num_registrazione=:fl_num_registrazione and
					prog_riga_ord_ven is null and
					cod_reparto=:fs_cod_reparto;
		
end choose

if sqlca.sqlcode<0 then
	fs_msg = "Errore in lettura colli ordine "+string(fl_anno_registrazione)+"/"+&
							string(fl_num_registrazione)+" "+sqlca.sqlerrtext
	return -1
end if

if isnull(fd_num_colli) then fd_num_colli = 0

return 1
end function

public function integer wf_get_reparti_deposito (string as_cod_deposito, ref string as_cod_reparti[], ref string as_errore);/**
 * stefanop
 * 11/10/2012
 *
 * Dato un deposito mi recupero tutti i reparti associati
 **/
 
 
string ls_sql
long ll_rows, ll_i
datastore lds_store

ls_sql = "SELECT cod_reparto FROM anag_reparti WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND cod_deposito='" + as_cod_deposito + "'"

ll_rows = guo_functions.uof_crea_datastore(lds_store, ls_sql, as_errore)

for ll_i = 1 to ll_rows
	
	as_cod_reparti[ll_i] = lds_store.getitemstring(ll_i, 1)
	
next

return ll_rows
end function

public function boolean wf_riga_spedita (long al_anno_ord_ven, long al_num_ord_ven, long al_prog_riga_ord_ven, string as_cod_deposito, string as_cod_cliente);/**
 * stefanop
 * 03/02/2012
 *
 * Controllo che la riga di ordine non sia già collegata ad una riga di bolla, in questo caso vuol dire che la riga è già stata spedita.
 * Ritorno:
 *   TRUE se esiste una riga in una bolla
 * 	  FALSE la riga non è presente in nessuna bolla
 **/

long ll_count

select count(*)
into :ll_count
from det_bol_ven
	join tes_bol_ven on
		tes_bol_ven.cod_azienda = det_bol_ven.cod_azienda and
		tes_bol_ven.anno_registrazione = det_bol_ven.anno_registrazione and
		tes_bol_ven.num_registrazione = det_bol_ven.num_registrazione
where
	det_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and
	det_bol_ven.anno_registrazione_ord_ven = :al_anno_ord_ven and
	det_bol_ven.num_registrazione_ord_ven = :al_num_ord_ven and
	det_bol_ven.prog_riga_ord_ven = :al_prog_riga_ord_ven and
	tes_bol_ven.cod_deposito = :as_cod_deposito;

if sqlca.sqlcode < 0 then
	ll_count = 0	
end if

return (sqlca.sqlcode = 0 and ll_count > 0)
end function

public subroutine wf_pulisci_stato_trasf (long al_riga, integer ai_j);/**
 * stefanop
 * 18/04/2012
 *
 * Pulisco le caselle, n-1 rispetto al parametro passato, che indicano lo stato di trasferimento in quanto lo stato 
 * deve essere presente solo sull'ultima casella valida.
 **/
 
string ls_stato
int li_i

if ai_j < 2 then return

setnull(ls_stato)
for li_i = ai_j - 1 to 1 step -1
	
	dw_report.setitem(al_riga,"dep_" + string(li_i), ls_stato)
	
next
end subroutine

public function integer wf_get_tipo_ordine (string as_cod_tipo_ord_ven, ref string as_tipo_stampa);/**
 * stefanop
 * 03/07/2012
 *
 * Recupero il tipo di stampa
 **/

select flag_tipo_bcl
into :as_tipo_stampa
from tab_tipi_ord_ven
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_tipo_ord_ven=:as_cod_tipo_ord_ven;

return sqlca.sqlcode
end function

public subroutine wf_log (string as_message);/**
 * stefanop
 * 03/07/2012
 *
 * Log
 **/

int  li_handle
time lt_diff

if ib_debug then
	
	//lt_diff = guo_functions.uof_time_diff(it_time_start, now())
	lt_diff = guo_functions.uof_time_diff(il_cpu_start, cpu())
	
	li_handle = FileOpen(LOG_FILE, LineMode!, Write!, LockWrite!, Append!)
	FileWrite(li_handle, string(lt_diff, "hh:mm:ss:ffffff") + "~t" + as_message)
	FileClose(li_handle)
end if

end subroutine

public subroutine wf_crea_ds_riepilogo_peso (ref datastore lds_data);string				ls_sql, ls_space

ls_space = "'"+space(2000)+"'"

ls_sql = "select '      ' as cliente"+&
					", '                                        ' as rag_soc_1,"+&
					"0.00000 as peso,"+&
					"2000 as colli,"+&
					"'KG' as um_peso,"+&
					ls_space + " as destinazione,"+&
					"'N' as flag_contrassegno,"+&
					ls_space+ " as post_it,"+&
					ls_space+ " as note_fisse,"+&
					ls_space+ " as note_fido,"+&
					"'N' as confermato "+&
			"from aziende "+&
			"where cod_azienda='INESISTENTE' "
guo_functions.uof_crea_datastore(lds_data, ls_sql)
end subroutine

public subroutine wf_crea_riepilogo_peso (datastore ads_data);long				ll_row, ll_index
string				ls_exp_1, ls_exp_2, ls_exp_3

ll_row = ads_data.rowcount()
ls_exp_1 = ""
ls_exp_2 = ""
ls_exp_3 = ""

for ll_index=1 to ll_row
	if ll_index > 1 then
		ls_exp_1 += "~r~n"
		ls_exp_2 += "~r~n"
		ls_exp_3 += "~r~n"
	end if
	ls_exp_1 += ads_data.getitemstring(ll_index, 1)
	ls_exp_2 +=ads_data.getitemstring(ll_index, 2)
	ls_exp_3 += string(ads_data.getitemnumber(ll_index, 3), "###,###,##0") + " Kg"
next

dw_report.object.cod_cliente_riepilogo_t.text = ls_exp_1
dw_report.object.cliente_riepilogo_t.text = ls_exp_2
dw_report.object.peso_riepilogo_t.text = ls_exp_3
end subroutine

public function integer wf_report_giri_trasferimento (string as_flag_tipo_report);//valori ammessi per tipo report
//		S    	intragruppo classico
//		T		intragruppo semplificato filtrato per date pronto reparti


datetime ldt_data_da,ldt_data_a, ldt_data_reg, ldt_data_consegna, ldt_data_trasf_inizio, ldt_data_trasf_fine, ldt_data_pronto_reparto

string		ls_cod_azienda, ls_cod_cliente, ls_des_cliente, ls_localita, ls_cod_prodotto, &
         	ls_des_prodotto, ls_misura, ls_sql,ls_flag_blocco, ls_provincia, &
			ls_messaggio, ls_cod_cliente_selezione, ls_rif_interscambio, ls_num_ord_cliente, &
			ls_cod_reparto[],ls_flag_riga_sospesa,ls_cod_tipo_causa_sospensione,ls_flag_urgente,ls_sigla, &
			ls_flag_evasione,ls_cod_tipo_det_ven,ls_flag_tipo_det_ven, ls_null[], ls_flag_valorizza, &
			ls_barcode, ls_cod_tipo_ord_ven, ls_flag_tipo_bol_fat, ls_cap, ls_telefono, ls_fax, ls_cellulare, &
			ls_des_pagamento, ls_note,ls_indirizzo, ls_cod_tessuto, ls_cod_verniciatura, ls_des_tessuto, &
			ls_des_verniciatura, ls_colore_tessuto, ls_barcode_ft, ls_localita_dest, ls_cod_tipo_bol_ven, &
			ls_cod_tipo_fat_ven,ls_cod_tipo_doc, ls_cod_deposito_origine, ls_error,ls_cod_reparti_deposito[], ls_cod_versione, &
			ls_cod_dep_filtro, ls_cod_deposito_ut, ls_cod_deposito_riga, ls_cod_deposito_ut_f, &
			ls_cod_deposito_prod_tes_sfuso, ls_null_string, ls_sql_columns, ls_sql_from, ls_sql_where, &
			ls_flag_tipo_stampa, ls_cod_deposito_produzione, ls_cod_depositi_produzione[], ls_flag_sospesi, ls_sigla_sosp_reparto, &
			ls_cod_deposito_giro_sel, ls_cod_deposito_origine_sel, ls_cod_giro_consegna, ls_cod_deposito_giro, &
			ls_cod_deposito_produce, ls_deposito_consegna

long     	ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_prog_giro_consegna, &
			ll_stato_prod_filtro, ll_stato_prod_ordine, ll_stato_reparto[], ll_ret, ll_y, ll_null[], ll_pos_rep, &
			ll_anno_filtro, ll_num_filtro, ll_righe_ordini, ll_anno_commessa, ll_num_commessa,ll_anno_registrazione_old, &
			ll_num_registrazione_old, ll_count_reparti, ll_num_riga_appartenenza, ll_controllo, ll_id_spedizione, ll_num_colli_letti, ll_num_colli_previsti

decimal  ld_dim_x, ld_dim_y, ld_dim_z, ld_quan_ordine, ld_quan_in_evasione, ld_quan_evasa, ld_imponibile_iva, &
			ld_num_colli, ld_tot_colli, ld_peso_netto_unitario, ld_qta_per_peso

boolean lb_stampa_barcode_ft = true, lb_filtra_spediti = true, lb_salta_riga, lb_controlla_dep_testata_ord

integer	li_count_sosp_rep, li_ret

datastore lds_store
uo_produzione luo_prod
uo_funzioni_1 luo_funzioni
uo_array luo_colli_sfuso
datastore	lds_peso

luo_funzioni = create uo_funzioni_1
luo_prod = create uo_produzione
luo_colli_sfuso = create uo_array


// ----------------------------------------------------------------------
it_time_start = now()
il_cpu_start = cpu()

setpointer(hourglass!)
st_1.text = "Attendere il termine dell'elaborazione !!! "

//Donato 22/01/2013
//Creazione DS per riepilogo peso per cliente ##############################
wf_crea_ds_riepilogo_peso(lds_peso)
//##########################################################

setnull(ls_error)
setnull(ls_null_string)
setnull(ll_anno_registrazione_old)
setnull(ll_num_registrazione_old)


dw_report.setredraw(false)
dw_report.reset()
dw_riassunto.reset()

dw_report.Object.DataWindow.Print.preview = "no"
dw_report.Object.DataWindow.Print.preview.rulers = "no"

dw_selezione.accepttext()

// recupero parametri dal report
ls_cod_deposito_ut = dw_selezione.getitemstring(1, "cod_deposito_partenza")
if isnull(ls_cod_deposito_ut) or len(ls_cod_deposito_ut) < 1 then
	g_mb.warning("Selezionare un deposito di partenza prima di eseguire il report.")
	return -1
end if

wf_get_reparti_deposito(ls_cod_deposito_ut, ls_cod_reparti_deposito[], ls_error)
if upperbound(ls_cod_reparti_deposito) < 1 then
	g_mb.warning("Lo stabilimento " + ls_cod_deposito_ut + " non ha nessun reparto associato")
	return -1
end if

if as_flag_tipo_report="T" then
	ldt_data_trasf_inizio = dw_selezione.getitemdatetime(1, "data_trasf_inizio")
	ldt_data_trasf_fine = dw_selezione.getitemdatetime(1, "data_trasf_fine")
	
	if not isnull(ldt_data_trasf_inizio) and year(date(ldt_data_trasf_inizio))>1950 and not isnull(ldt_data_trasf_fine) and year(date(ldt_data_trasf_fine))>1950 then
		
		if ldt_data_trasf_inizio>ldt_data_trasf_fine then
			g_mb.warning("Le date di trasferimento impostate nel filtro NON sono congruenti!")
			return -1
		end if
	end if
end if
// ---

lb_stampa_barcode_ft = true
lb_filtra_spediti = (dw_selezione.getitemstring(1, "flag_spedito") = "S")
ll_anno_filtro = dw_selezione.getitemnumber(dw_selezione.getrow(),"anno_registrazione")
ll_num_filtro = dw_selezione.getitemnumber(dw_selezione.getrow(),"num_registrazione")
ldt_data_da = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_inizio")
ldt_data_a = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_fine")
ls_cod_cliente_selezione = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_cliente")
ls_cod_dep_filtro = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_deposito_arrivo")
if len(ls_cod_dep_filtro) < 1 then setnull(ls_cod_dep_filtro)
ll_stato_prod_filtro = dw_selezione.getitemnumber(dw_selezione.getrow(),"flag_avan_prod")
ls_flag_valorizza = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_valorizza")
ls_cod_deposito_ut_f = left(ls_cod_deposito_ut, 1) + "%"
ls_flag_sospesi = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_sospesi")

//filtro per deposito giro consegna della testata ordine di vendita
ls_cod_deposito_giro_sel = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_deposito_giro")

//filtro per deposito testata ordine vendita
ls_cod_deposito_origine_sel = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_deposito_origine")

ll_id_spedizione = dw_selezione.getitemnumber(dw_selezione.getrow(),"id_spedizione")

// ----------------------------------------------------------------------

// Inizializzazione query
// ---------------------------------------------------------------------
ls_sql_columns = "cod_azienda, anno_registrazione, num_registrazione, flag_blocco, cod_cliente, data_registrazione, data_consegna, " + &
	"rif_interscambio, num_ord_cliente, cod_deposito, localita, cod_deposito_prod, " + &
	"cod_prodotto, des_prodotto, quan_ordine, cod_misura, " + &
	"prog_riga_ord_ven, quan_in_evasione, quan_evasa, imponibile_iva, " + &
	"flag_riga_sospesa, cod_tipo_causa_sospensione, flag_urgente, flag_evasione, " + &
	"num_riga_appartenenza, cod_versione, anno_commessa, num_commessa, " + &
	"cod_tipo_det_ven, " + &
	"flag_tipo_bol_fat, cod_tipo_bol_ven, cod_tipo_fat_ven, flag_tipo_bcl, " + &
	"rag_soc_1, indirizzo, anag_clienti_localita, provincia, cap, telefono, fax, telex, sito_internet, " + &
	"des_pagamento, cod_giro_consegna "	
	
ls_sql_from = " FROM view_ddt_trasf_intragruppo as v"

ls_sql_where = " WHERE v.cod_azienda='" + s_cs_xx.cod_azienda + "' " + &
	"AND v.flag_blocco <> 'S' " + &
	"AND v.cod_prodotto is not null "

if g_str.isnotempty(ls_cod_deposito_giro_sel) then ls_sql_where += " and v.cod_giro_consegna in ( select cod_giro_consegna "+&
																														     "from tes_giri_consegne "+&
																															"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
																																	 "cod_deposito='"+ls_cod_deposito_giro_sel+"') "
if g_str.isnotempty(ls_cod_deposito_origine_sel) then  ls_sql_where += " and v.cod_deposito='" + ls_cod_deposito_origine_sel + "' "


if not isnull(ldt_data_da) then ls_sql_where += " AND v.data_consegna>='" + string(ldt_data_da, s_cs_xx.db_funzioni.formato_data) + "' "
if not isnull(ldt_data_a) then ls_sql_where += " AND v.data_consegna<='" + string(ldt_data_a, s_cs_xx.db_funzioni.formato_data) + "' "
if not isnull(ls_cod_cliente_selezione) and ls_cod_cliente_selezione <> "" then ls_sql_where += " AND v.cod_cliente='" + ls_cod_cliente_selezione + "' "
if not isnull(ll_anno_filtro) and ll_anno_filtro > 1900 then ls_sql_where += " AND v.anno_registrazione=" + string(ll_anno_filtro)
if not isnull(ll_num_filtro) and ll_num_filtro > 0 then ls_sql_where += " AND v.num_registrazione=" + string(ll_num_filtro)

choose case dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato")
	case "R" // residui
		ls_sql_where += " AND v.flag_evasione in ('A','P')"
	case "E"	// Solo evasi
		ls_sql_where += " AND v.flag_evasione ='E' "
	case "A"	// Solo aperti
		ls_sql_where += " AND v.flag_evasione ='A' "
	case "P"	// solo parziali
		ls_sql_where += " AND v.flag_evasione ='P' "
end choose


if as_flag_tipo_report="T" then
	ldt_data_trasf_inizio = dw_selezione.getitemdatetime(1, "data_trasf_inizio")
	ldt_data_trasf_fine = dw_selezione.getitemdatetime(1, "data_trasf_fine")
	
end if


wf_log("INIZIO ELABORAZIONE ###############")

// Recupero il numero totale di righe da controllare
// ----------------------------------------------------------------------
ls_sql = "SELECT count(*) " + ls_sql_from + ls_sql_where

ll_righe_ordini = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)
if ll_righe_ordini < 0 then
	
	if isnull(ls_error) then ls_error = "MSG QUERY NULLO"
	
	g_mb.error("Errore durante calcolo delle righe d'ordine interessate nel report! " + ls_error)
	return -1
else
	ll_righe_ordini = lds_store.getitemnumber(1, 1)
end if

setnull(ls_error)
destroy lds_store

progess.maxposition = ll_righe_ordini
progess.position = 0
st_1.text = "Elaborazione in corso. " + string(ll_righe_ordini) + " righe ordine da controllare"
// ----------------------------------------------------------------------

// Cursore
// ----------------------------------------------------------------------
ls_sql = "SELECT " +  ls_sql_columns + ls_sql_from + ls_sql_where + " ORDER BY v.cod_cliente, v.anno_registrazione, v.num_registrazione, v.prog_riga_ord_ven"

declare cu_det_ord_ven dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_det_ord_ven;

if sqlca.sqlcode <> 0 then
	g_mb.error("Errore durante l'apertura del cursore cu_det_ord_ven", sqlca)
	return -1
end if

wf_log("Apertura cursore cu_tes_ord_ven. Trovate " + string(ll_righe_ordini) + " righe")

do while true
	
	fetch cu_det_ord_ven
	into	:ls_cod_azienda, :ll_anno_registrazione, :ll_num_registrazione, :ls_flag_blocco, :ls_cod_cliente, :ldt_data_reg, :ldt_data_consegna, 
			:ls_rif_interscambio, :ls_num_ord_cliente, :ls_cod_deposito_origine, :ls_localita_dest, :ls_cod_deposito_prod_tes_sfuso, 
			:ls_cod_prodotto, :ls_des_prodotto, :ld_quan_ordine, :ls_misura, 
			:ll_prog_riga_ord_ven, :ld_quan_in_evasione, :ld_quan_evasa, :ld_imponibile_iva, 
			:ls_flag_riga_sospesa, :ls_cod_tipo_causa_sospensione, :ls_flag_urgente, :ls_flag_evasione, 
			:ll_num_riga_appartenenza, :ls_cod_versione, :ll_anno_commessa, :ll_num_commessa, :ls_cod_tipo_det_ven,
			:ls_flag_tipo_bol_fat, :ls_cod_tipo_bol_ven, :ls_cod_tipo_fat_ven, :ls_flag_tipo_stampa, 
			:ls_des_cliente, :ls_indirizzo, :ls_localita, :ls_provincia, :ls_cap, :ls_telefono, :ls_fax, :ls_cellulare, :ls_note, 
			:ls_des_pagamento, :ls_cod_giro_consegna;
		
	if sqlca.sqlcode = 100 then
		exit 
	elseif sqlca.sqlcode < 0 then
		g_mb.error("Errore nella FETCH del cursore (cu_det_ord_ven).", sqlca)
		return -1
	end if
	
	if ib_halt then
		st_1.text = "Elaborazione annullata dall'utente"
		
		// esce dal ciclo NON dalla funzione
		exit
	end if
	
	progess.position += 1
	setmicrohelp("Elaborazione ordine: " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "/" + string(ll_prog_riga_ord_ven))
	wf_log("Letta riga " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "/" + string(ll_prog_riga_ord_ven))
	
	yield()
	
	
	//----------------------------------------------------------------------------
	//se risulta specificato il filtro per ID spedizione verifica che l'ordine che stai elaborando è tra quelli con i colli scansionati
	li_ret = wf_buffer_colli(	ll_id_spedizione, ls_flag_tipo_stampa, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, &
									ll_num_colli_letti, ll_num_colli_previsti, ls_messaggio)
	choose case li_ret
		case is < 0
			g_mb.error("APICE", ls_messaggio)
			close cu_det_ord_ven;
			return -1
		
		case 0
			//ignora il controllo e  avanti con l'elaborazione
		
		case 1
			////salta la riga
			//continue
			
		case 2
			//avanti con l'elaborazione
		
	end choose
	//----------------------------------------------------------------------------
	
	
	// Pulizia delle variabili
	// ----------------------------------------------------------------------
	if ll_anno_registrazione_old <> ll_anno_registrazione or ll_num_registrazione_old <> ll_num_registrazione then
		// sono in un nuovo ordine; resetto variabili
		setnull(ls_cod_tipo_doc)
		lb_stampa_barcode_ft = true
		ll_controllo = 0
		//ll_pos_riga = 0
		
		//if isnull(ll_prog_giro_consegna) then ll_prog_giro_consegna = 999999
		ll_prog_giro_consegna = 999999
	end if
	
	if g_str.isnotempty(ls_cod_giro_consegna) then
		//deposito giro consegna
		select cod_deposito
		into :ls_cod_deposito_giro
		from tes_giri_consegne
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_giro_consegna=:ls_cod_giro_consegna;
	else
		setnull(ls_cod_deposito_giro)
	end if
	
	
	//chi deve consegnare? Il deposito origine ordine o il deposito del giro, quando quest'ultimo è specificato
	if g_str.isnotempty(ls_cod_deposito_giro) then
		//il deposito giro consegna
		ls_deposito_consegna = ls_cod_deposito_giro
	else
		//il deposito origine
		ls_deposito_consegna = ls_cod_deposito_origine
	end if
	
	
	
	//leggo il peso netto unitario della riga d'ordine
	//#####################################################################
	select peso_netto
	into :ld_peso_netto_unitario
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_registrazione and
				num_registrazione=:ll_num_registrazione and
				prog_riga_ord_ven=:ll_prog_riga_ord_ven;
	
	if isnull(ld_peso_netto_unitario) then ld_peso_netto_unitario = 0
	//#####################################################################
	
	// pulisco variabili ad ogni ciclo
	lb_salta_riga = true
	lb_controlla_dep_testata_ord = true // indica se devo confrontare il deposito che sta nella testa dell'ordine
	ls_cod_reparto = ls_null
	ll_i = 0
	// ----------------------------------------------------------------------
						
	// stefanop 04/01/2012 : controllo documento successivo		
	if ls_flag_tipo_bol_fat = 'B' and len(ls_cod_tipo_bol_ven) > 0 then
		ls_cod_tipo_doc = ls_cod_tipo_bol_ven
	elseif ls_flag_tipo_bol_fat = 'F' and len(ls_cod_tipo_fat_ven) > 0 then
		ls_cod_tipo_doc = ls_cod_tipo_fat_ven
	end if
	// ----

	// stefanop: 03/02/2012: controllo he la riga non sia già stata spedita
	// ----------------------------------------------------------------------
	if lb_filtra_spediti then
		if wf_riga_spedita(ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven, ls_cod_deposito_ut, "") then
			wf_log("~tRiga già spedita") 
			continue
		end if
	end if
	// ----------------------------------------------------------------------
	
	
	//Donato 22/01/2012
	//se nel filtro hai specificato di visualizzare solo quelli sospesi o solo quelli non sospesi, tienine conto
	//###############################################################
	if ls_flag_sospesi <> "T" then
		wf_log("~tControllo filtro sospensione")
		if (ls_flag_tipo_stampa = "B" or ls_flag_tipo_stampa = "C") then
			li_count_sosp_rep = luo_prod.uof_get_sospensioni_riga_ordine(ll_anno_registrazione, ll_num_registrazione, 0)
		else
			li_count_sosp_rep = luo_prod.uof_get_sospensioni_riga_ordine(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven)
		end if
		
		if ls_flag_sospesi = "N" then
			//chiesto di visualizzare solo quelli NON sospesi
			if li_count_sosp_rep = 0 and ls_flag_riga_sospesa="N" then
			else
				continue
			end if
		else
			//chiesto di visualizzare solo quelli SOSPESI
			if li_count_sosp_rep > 0 or ls_flag_riga_sospesa = "S" then
			else
				continue
			end if
		end if
	end if
	//###############################################################
	
	
	wf_log("~tRecupero reparti - tipo stampa " + ls_flag_tipo_stampa)

	// stefanop: in base al tipo d'ordine decido da dove prendere i reparti (sfuso o altro)
	if ls_flag_tipo_stampa = "B"  then
		
		//CASO SFUSO
		//chi deve consegnare? Il deposito origine ordine o il deposito del giro, quando quest'ultimo è specificato
		//quindi questo è nella variabile "ls_deposito_consegna"

		
		//chi deve produrre? il deposito origine ordine o il deposito produzione testata, quando quest'ultimo è specificato
		if g_str.isnotempty(ls_cod_deposito_prod_tes_sfuso) then
			//E' STATO specificato nella testata il deposito produzione
			ls_cod_deposito_produce = ls_cod_deposito_prod_tes_sfuso
		else
			//NON E' STATO specificato nella testata il deposito produzione
			ls_cod_deposito_produce = ls_cod_deposito_origine
		end if
		
		//Quindi se il deposito che deve consegnare è uguale al deposito che deve produrre non devo trasferire niente: SALTO LA RIGA
		if ls_deposito_consegna = ls_cod_deposito_produce then continue
		
		
		//se il deposito partenza del filtro è diverso dal deposito produzione SALTO AL RIGA
		//DEP_partenza         <> DEP_produzione
		if ls_cod_deposito_ut <> ls_cod_deposito_produce then continue
		
		//se specificato il deposito arrivo del filtro e questo è diverso dal deposito consegna SALTO LA RIGA
		if g_str.isnotempty(ls_cod_dep_filtro) then
			//DEP_arrivo         <> DEP_consegna
			if ls_cod_dep_filtro <> ls_deposito_consegna then continue
		end if
		
		/*
		//se NON specificato il deposito produzione allora vuol dire che coincide con quello di origine e quindi non devo trasferire niente: SALTO LA RIGA
		//se E' stato specificato ed è DIVERSO da quello origine e COINCIDENTE con quello dell'utente (di PRODUZIONE) allora elaboro
		if g_str.isempty(ls_cod_deposito_prod_tes_sfuso) then continue
		
		if ls_cod_deposito_prod_tes_sfuso=ls_cod_dep_filtro or ls_cod_deposito_prod_tes_sfuso<>ls_cod_deposito_ut or &
				ls_cod_dep_filtro<>ls_cod_deposito_origine then
			continue
		end if
		*/
		//-------------------------------------------------------------------------------------
		
		//Questa funzione recupera i reparti del deposito produzione della riga ordine
		//-------------------------------------------------------------------------------------
		if luo_funzioni.uof_trova_reparti_sfuso(ls_cod_prodotto, ls_cod_deposito_produce, ls_null_string, ls_cod_reparto, ls_error) < 0 then
			destroy luo_funzioni
			g_mb.error(ls_error)
			
			exit // esce dal ciclo NON dalla funzione
		end if
		//-------------------------------------------------------------------------------------
		
		// se l'array è vuoto non devo produrre io il prodotto
		if upperbound(ls_cod_reparto) < 1 then continue
	else
		// stefanop 21/02/2012: non scorro più la distinta ma vado a controllare nelle varianti_det_ord_ven
		ls_sql = "SELECT cod_deposito_produzione "
		
		// commesse o ordine
		if not isnull(ll_anno_commessa) and ll_anno_commessa > 1900 and not isnull(ll_num_commessa) then
			ls_sql += "FROM varianti_commesse " + &
					"WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' " + &
					" AND anno_commessa=" + string(ll_anno_commessa) + &
					" AND num_commessa=" + string(ll_num_commessa) + &
					" AND cod_deposito_produzione is not null "
		else
			ls_sql += "FROM varianti_det_ord_ven " + &
					"WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' " + &
					" AND anno_registrazione=" + string(ll_anno_registrazione) + &
					" AND num_registrazione=" + string(ll_num_registrazione) + &
					" AND prog_riga_ord_ven=" + string(ll_prog_riga_ord_ven) + &
					" AND cod_deposito_produzione is not null "
		end if
					
		ll_righe_ordini = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)
		
		wf_log("~tVarianti ( " + string(ll_righe_ordini) + ")" )
		
		if ll_righe_ordini < 0 then
			g_mb.error(ls_error)
			exit
		elseif ll_righe_ordini = 0 then
			
			// controllo la fase in testata del prodotto finito
			// ----------------------------------------------------------------------------------------------------			
			//if ls_cod_deposito_origine <> ls_cod_dep_filtro then continue
			if ls_deposito_consegna <> ls_cod_dep_filtro then continue
			
			
			destroy lds_store
							
			ls_sql = "SELECT cod_deposito_produzione FROM tes_fasi_lavorazione_det WHERE " + &
						"cod_azienda='" + s_cs_xx.cod_azienda + "' AND " + &
						"cod_prodotto='" + ls_cod_prodotto + "' AND " + &
						"cod_deposito_origine='" + ls_deposito_consegna + "' AND " + &
						"cod_deposito_produzione='" + ls_cod_deposito_ut + "' "
			//ls_sql = "SELECT cod_deposito_produzione FROM tes_fasi_lavorazione_det WHERE " + &
						//"cod_azienda='" + s_cs_xx.cod_azienda + "' AND " + &
						//"cod_prodotto='" + ls_cod_prodotto + "' AND " + &
						//"cod_deposito_origine='" + ls_cod_deposito_origine + "' AND " + &
						//"cod_deposito_produzione='" + ls_cod_deposito_ut + "' "
						
			if not isnull(ls_cod_versione) and ls_cod_versione <> "" then
				ls_sql += " AND cod_versione='" + ls_cod_versione + "'"
			end if
			
			ll_righe_ordini = guo_functions.uof_crea_datastore(lds_store,ls_sql,ls_error)
			wf_log("~t~tVarianti testa ( " + string(ll_righe_ordini) + ")" )
			
			if ll_righe_ordini = 0 then
				continue
			elseif ll_righe_ordini < 0 then
				g_mb.error(ls_error)
				exit
			else
				ls_cod_deposito_produzione =  lds_store.getitemstring(1,1)
				
				//if ls_cod_deposito_produzione <> ls_cod_deposito_ut or ls_cod_deposito_origine <> ls_cod_dep_filtro then
				if ls_cod_deposito_produzione <> ls_cod_deposito_ut or ls_deposito_consegna <> ls_cod_dep_filtro then
					continue
				end if
			end if
			// ----------------------------------------------------------------------------------------------------
			
		else
			
			ls_cod_depositi_produzione = ls_null
			
			// stefanop 03/07/2012: prima di iniziare controllo che ci sia il mio depito
			ll_pos_rep = lds_store.find("#1='" + ls_cod_deposito_ut + "'", 1, lds_store.rowcount())
			if ll_pos_rep = 0 then 
				wf_log("~tReparto di produzione non in lista") 
				continue
			end if
			// -----
			
			for ll_i = 1 to ll_righe_ordini
				ls_cod_depositi_produzione[ll_i] =  lds_store.getitemstring(ll_i,1)
			next
		
			//if not guo_functions.uof_in_array(ls_cod_deposito_ut, ls_cod_depositi_produzione, ll_pos_rep) then
			//	continue
			//else
			if ll_pos_rep = upperbound(ls_cod_depositi_produzione) then
				// il mio reparto è presente ma è all'ultima posizione quindi controllo la testata dell'ordine
				lb_controlla_dep_testata_ord = true
			else
				for ll_i = ll_pos_rep + 1  to upperbound(ls_cod_depositi_produzione)
					if ls_cod_depositi_produzione[ll_i] <> ls_cod_deposito_ut then
						lb_controlla_dep_testata_ord = false
					end if
					
					if ls_cod_depositi_produzione[ll_i] <> ls_cod_deposito_ut and (isnull(ls_cod_dep_filtro) or ls_cod_dep_filtro = ls_cod_depositi_produzione[ll_i]) then
						lb_salta_riga = false
						exit
					end if
				next
			end if
				
			if lb_controlla_dep_testata_ord then
				// la riga è da saltare per qunto riguarda i reparti coinvolti ma controllo il deposito origine dell'ordine.
				//if ls_cod_deposito_origine <> ls_cod_deposito_ut and (isnull(ls_cod_dep_filtro) or ls_cod_dep_filtro = ls_cod_deposito_origine) then
				if ls_deposito_consegna <> ls_cod_deposito_ut and (isnull(ls_cod_dep_filtro) or ls_cod_dep_filtro = ls_deposito_consegna) then
					lb_salta_riga = false
				else
					continue // cilco delle righe dell'ordine
				end if
			end if
			
			if lb_salta_riga then
				continue
			end if		
		end if
		//end if
		// ------------------------------------------------------------------------------------------------------------
	
	end if
	
	//#########################################################################
	//controllo sulle date pronto, se il reparto è di tipo T e almeno una data tarsferiemnto è stata specificata
	if as_flag_tipo_report="T" and &
			( 		(not isnull(ldt_data_trasf_inizio) and year(date(ldt_data_trasf_inizio))>1950)  or &
					(not isnull(ldt_data_trasf_fine) and year(date(ldt_data_trasf_fine))>1950) &
			)  then
	
		//mi serve controllare le date in tabella det_ordini_produzione
		li_ret = wf_controlla_date(		ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, &
												ls_cod_deposito_ut, ldt_data_trasf_inizio, ldt_data_trasf_fine, ls_messaggio)
		
		if li_ret<0 then
			g_mb.error(ls_messaggio)
			return -1
			
		elseif li_ret=1 then
			//salta la riga
			continue
		end if
		
	end if
	
	ls_messaggio = ""
	//#########################################################################
	
		
	wf_log("~tControllo reparti di produzione") 
	
	// stefanop 16/02/2012: controllo stato di produzione dei miei reparti e lo assegno alla riga del report
	//-------------------------------------------------------------------------------------
	ll_stato_prod_ordine = luo_prod.uof_stato_prod_det_ord_ven_reparti( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_deposito_ut, ref ls_cod_reparto[], ref ll_stato_reparto[], ref ls_messaggio )
	if ll_stato_prod_ordine = -1 then
		g_mb.error(ls_messaggio)
		exit
	end if
	
	ls_messaggio = ""
	
	if ll_stato_prod_filtro <> -1 then
		if ll_stato_prod_filtro = 3 then
			if ll_stato_prod_ordine <> 0 and ll_stato_prod_ordine <> 2 then
				continue
			end if
		else
			if ll_stato_prod_ordine <> ll_stato_prod_filtro then
				continue
			end if
		end if
	end if 
	
	if ll_stato_prod_ordine = 1 then ll_stato_prod_ordine = 3 // Tutto pronto
	//-------------------------------------------------------------------------------------
	
	// stefanop 11/04/2012 - recupero i colli dei reparti per lo sfuso, solo se è una nuova testata d'ordine
	//-------------------------------------------------------------------------------------
	if (ls_flag_tipo_stampa = "B" or ls_flag_tipo_stampa = "C") and &
		ll_anno_registrazione <> ll_anno_registrazione_old or ll_num_registrazione <> ll_num_registrazione_old then 
		//destroy lds_store
		luo_colli_sfuso.removeall()
		
		ls_sql = "SELECT cod_reparto, count(*) FROM tab_ord_ven_colli WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND " + &
					"anno_registrazione=" + string(ll_anno_registrazione) + " AND " + &
					"num_registrazione=" + string(ll_num_registrazione) + &
					" GROUP BY cod_reparto"
		
		ll_righe_ordini = guo_functions.uof_crea_datastore(lds_store, ls_sql, ls_error)
		if ll_righe_ordini < 0 then
			g_mb.error(ls_error)
			exit
		end if
		
		for ll_i = 1 to ll_righe_ordini
			luo_colli_sfuso.set(lds_store.getitemstring(ll_i, 1), lds_store.getitemnumber(ll_i, 2))
		next
		
		ll_anno_registrazione_old = ll_anno_registrazione
		ll_num_registrazione_old = ll_num_registrazione 
	end if
	//-------------------------------------------------------------------------------------
	
	// modifica claudia 26/01/06 per aggiungedere dei dati
	//inseriti due campi R.I. e rif. vs. ordine come da richiesta del cliente
	if ll_controllo = 0 and len(ls_rif_interscambio) > 0 and not isnull(ls_rif_interscambio) then
		ll_i = dw_report.insertrow(0)
		//dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
		//dw_report.setitem(ll_i, "des_giro", ls_des_giro)
		dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
		dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
		dw_report.setitem(ll_i, "localita", ls_cap + " " + ls_localita)
		dw_report.setitem(ll_i, "provincia", ls_provincia)
		dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
		dw_report.setitem(ll_i, "cod_prodotto", "R.I.")
		dw_report.setitem(ll_i, "des_prodotto", ls_rif_interscambio)
		dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
		dw_report.setitem(ll_i, "flag_tipo_riga", "S")
		dw_report.setitem(ll_i, "prog_riga_ord_ven", 0)
		dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)
		dw_report.setitem(ll_i, "indirizzo", ls_indirizzo)
		dw_report.setitem(ll_i, "telefono", ls_telefono)
		dw_report.setitem(ll_i, "fax", ls_fax)
		dw_report.setitem(ll_i, "cellulare", ls_cellulare)
		dw_report.setitem(ll_i, "note", ls_note)
		dw_report.setitem(ll_i, "pagamento", ls_des_pagamento)
		dw_report.setitem(ll_i, "flag_valorizza", ls_flag_valorizza)
		dw_report.setitem(ll_i,"flag_colli","N")
		dw_report.setitem(ll_i,"flag_tipo_bol_fat",ls_flag_tipo_bol_fat)
		dw_report.setitem(ll_i,"cod_tipo_doc",ls_cod_tipo_doc)
		dw_report.setitem(ll_i, "cod_deposito", ls_cod_deposito_origine)
		
		ll_controllo = 1
		if len(ls_num_ord_cliente) > 0 and not isnull(ls_num_ord_cliente) then
			ll_i = dw_report.insertrow(0)
			//dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
			//dw_report.setitem(ll_i, "des_giro", ls_des_giro)
			dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
			dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
			dw_report.setitem(ll_i, "localita", ls_cap + " " + ls_localita)
			dw_report.setitem(ll_i, "provincia", ls_provincia)
			dw_report.setitem(ll_i, "cod_prodotto", "R.VS.ORD.")
			dw_report.setitem(ll_i, "des_prodotto", ls_num_ord_cliente)
			dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
			dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
			dw_report.setitem(ll_i, "prog_riga_ord_ven", 0)
			dw_report.setitem(ll_i, "flag_tipo_riga", "S")
			dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)
			dw_report.setitem(ll_i, "indirizzo", ls_indirizzo)
			dw_report.setitem(ll_i, "telefono", ls_telefono)
			dw_report.setitem(ll_i, "fax", ls_fax)
			dw_report.setitem(ll_i, "cellulare", ls_cellulare)
			dw_report.setitem(ll_i, "note", ls_note)
			dw_report.setitem(ll_i, "pagamento", ls_des_pagamento)
			dw_report.setitem(ll_i, "flag_valorizza", ls_flag_valorizza)
			dw_report.setitem(ll_i,"flag_colli","N")			
			dw_report.setitem(ll_i,"flag_tipo_bol_fat",ls_flag_tipo_bol_fat)
			dw_report.setitem(ll_i,"cod_tipo_doc",ls_cod_tipo_doc)
			dw_report.setitem(ll_i, "cod_deposito", ls_cod_deposito_origine)
			
		end if
		
	else
		
		if ll_controllo = 0 and len(ls_num_ord_cliente) > 0 and not isnull(ls_num_ord_cliente) then
			ll_i = dw_report.insertrow(0)
			ll_controllo = 1
			//dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
			//dw_report.setitem(ll_i, "des_giro", ls_des_giro)
			dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
			dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
			dw_report.setitem(ll_i, "localita", ls_cap + " " + ls_localita)
			dw_report.setitem(ll_i, "provincia", ls_provincia)
			dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
			dw_report.setitem(ll_i, "cod_prodotto", "R.VS.ORD.")
			dw_report.setitem(ll_i, "des_prodotto", ls_num_ord_cliente)
			dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
			dw_report.setitem(ll_i, "prog_riga_ord_ven", 0)
			dw_report.setitem(ll_i, "flag_tipo_riga", "S")
			dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)
			dw_report.setitem(ll_i, "indirizzo", ls_indirizzo)
			dw_report.setitem(ll_i, "telefono", ls_telefono)
			dw_report.setitem(ll_i, "fax", ls_fax)
			dw_report.setitem(ll_i, "cellulare", ls_cellulare)
			dw_report.setitem(ll_i, "note", ls_note)
			dw_report.setitem(ll_i, "pagamento", ls_des_pagamento)
			dw_report.setitem(ll_i, "flag_valorizza", ls_flag_valorizza)
			dw_report.setitem(ll_i,"flag_colli","N")			
			dw_report.setitem(ll_i,"flag_tipo_bol_fat",ls_flag_tipo_bol_fat)
			dw_report.setitem(ll_i,"cod_tipo_doc",ls_cod_tipo_doc)
			dw_report.setitem(ll_i, "cod_deposito", ls_cod_deposito_origine)
			
		end if
	end if
	//fine modifica claudia
	
	//inserimento riga dettaglio
	//ll_pos_riga += 1
	
	ll_i = dw_report.insertrow(0)	
	//dw_report.setitem(ll_i, "cod_giro", ls_cod_giro)
	//dw_report.setitem(ll_i, "des_giro", ls_des_giro)
	dw_report.setitem(ll_i, "data_registrazione", ldt_data_reg)
	dw_report.setitem(ll_i, "data_consegna", ldt_data_consegna)
	dw_report.setitem(ll_i, "cod_cliente", ls_cod_cliente)
	dw_report.setitem(ll_i, "rag_soc_1", ls_des_cliente)
	dw_report.setitem(ll_i, "localita", ls_cap + " " + ls_localita)
	dw_report.setitem(ll_i, "provincia", ls_provincia)
	dw_report.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)
	dw_report.setitem(ll_i, "des_prodotto", ls_des_prodotto)
	
	
	if dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato") = 'R' or dw_selezione.getitemstring(dw_selezione.getrow(),"flag_stato") = 'P' then
		ld_qta_per_peso = ld_quan_ordine - ld_quan_in_evasione - ld_quan_evasa
	else
		ld_qta_per_peso = ld_quan_ordine
	end if
	dw_report.setitem(ll_i, "quan_consegna", ld_qta_per_peso)
	
	
	dw_report.setitem(ll_i, "cod_misura", ls_misura)
	dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
	dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)
	dw_report.setitem(ll_i, "prog_riga_ord_ven", ll_prog_riga_ord_ven)
	dw_report.setitem(ll_i, "prog_giro", ll_prog_giro_consegna)

	dw_report.setitem(ll_i, "indirizzo", ls_indirizzo)
	dw_report.setitem(ll_i, "telefono", ls_telefono)
	dw_report.setitem(ll_i, "fax", ls_fax)
	dw_report.setitem(ll_i, "cellulare", ls_cellulare)
	dw_report.setitem(ll_i, "note", ls_note)
	dw_report.setitem(ll_i, "pagamento", ls_des_pagamento)
	dw_report.setitem(ll_i, "cod_deposito", ls_cod_deposito_origine)
	
	ls_barcode = "*O" + string(ll_anno_registrazione,"0000") + string(ll_num_registrazione,"000000") + string(ll_prog_riga_ord_ven,"0000") + "*"
	
	//########################################################################
	dw_report.setitem(ll_i, "peso_netto_unitario", ld_peso_netto_unitario)
	wf_aggiorna_ds_peso(lds_peso, ll_anno_registrazione, ll_num_registrazione, ld_peso_netto_unitario * ld_qta_per_peso)
	//########################################################################
	
	// stefanop 04/01/2012 : aggiungo il barcode per la testata
	// attenzione che metto sempre la variabile ls_barcode nella barcode_bl
	if lb_stampa_barcode_ft then
		ls_barcode_ft = "*O" + string(ll_anno_registrazione,"0000") + string(ll_num_registrazione,"000000") + "*"
		lb_stampa_barcode_ft = false
	else
		setnull(ls_barcode_ft)
	end if
	// ---
	
	
	if len(ls_flag_tipo_bol_fat) > 0 then
		choose case ls_flag_tipo_bol_fat
			case "B"
				dw_report.setitem(ll_i, "barcode_ft", ls_barcode_ft)
				dw_report.setitem(ll_i, "barcode_bl", ls_barcode)
			case "F"			
				dw_report.setitem(ll_i, "barcode_ft", ls_barcode_ft)
				dw_report.setitem(ll_i, "barcode_bl", ls_barcode)
			case else
				dw_report.setitem(ll_i, "barcode_ft", ls_null)
				dw_report.setitem(ll_i, "barcode_bl", ls_null)
		end choose
	else
		dw_report.setitem(ll_i, "barcode_ft", ls_null)
		dw_report.setitem(ll_i, "barcode_bl", ls_null)
	end if
	
	
	dw_report.setitem(ll_i,"stato_prod",ll_stato_prod_ordine)
	
	// stefanop 04/01/2012 : inserisco la destinazione diversa
	dw_report.setitem(ll_i,"localita_dest",ls_localita_dest)
	dw_report.setitem(ll_i,"flag_tipo_bol_fat",ls_flag_tipo_bol_fat)
	dw_report.setitem(ll_i,"cod_tipo_doc",ls_cod_tipo_doc)
	// ---
	
	//Donato 10/05/2011
	//Beatrice ha richiesto che per le righe è riferite (quindi con colli sempre a ZERO)
	//è inutile far vedere COLLI=...
	if ll_num_riga_appartenenza > 0 then
		//riga riferita, nascondi il tot colli e relativa label
		dw_report.setitem(ll_i,"flag_colli","N")
	end if
	
	select dim_x, dim_y, dim_z, cod_tessuto, cod_verniciatura, cod_non_a_magazzino
	into   :ld_dim_x, :ld_dim_y, :ld_dim_z, :ls_cod_tessuto, :ls_cod_verniciatura, :ls_colore_tessuto
	from   comp_det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
			 
	if sqlca.sqlcode = 0 then
		dw_report.setitem(ll_i, "dim_1", ld_dim_x)
		dw_report.setitem(ll_i, "dim_2", ld_dim_y)
		dw_report.setitem(ll_i, "dim_3", ld_dim_z)
		
		select des_prodotto
		into   :ls_des_tessuto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_tessuto;
		if sqlca.sqlcode = 0 and not isnull(ls_des_tessuto) and len(ls_des_tessuto) > 0 then
			dw_report.setitem(ll_i, "tessuto", ls_cod_tessuto + " " + ls_des_tessuto)
		end if
		
		select des_prodotto
		into   :ls_des_verniciatura
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_verniciatura;
		if sqlca.sqlcode = 0 and not isnull(ls_des_verniciatura) and len(ls_des_verniciatura) > 0 then
			dw_report.setitem(ll_i, "verniciatura", ls_des_verniciatura)
		end if
		
		//Donato 25/01/2010 aggiunto colore tessuto
		dw_report.setitem(ll_i, "cod_non_a_magazzino", ls_colore_tessuto)
			
	end if
	
	dw_report.setitem(ll_i, "flag_urgente",   ls_flag_urgente)
	dw_report.setitem(ll_i, "flag_valorizza", ls_flag_valorizza)
	
	// stefanop 10/07/2012: commento perchè fatta sopra
	// ****************************************************************
	ls_cod_reparto = ls_null
	ll_stato_reparto = ll_null
	
	
	wf_log("~tRecupero stato di produzione reparti.")
	
	ll_ret = luo_prod.uof_stato_prod_det_ord_ven_reparti( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ref ls_cod_reparto[], ref ll_stato_reparto[], ref ls_messaggio )
	if ll_stato_prod_ordine = -1 then
		g_mb.messagebox("APICE",ls_messaggio,stopsign!)
		exit
	end if
	
	ls_messaggio = ""
	
	wf_log("~tFine recupero stato di produzione reparti.")
	// ****************************************************************		
	ld_tot_colli = 0
	
	ll_count_reparti = upperbound(ls_cod_reparto)
	if ll_count_reparti > 5 then ll_count_reparti = 5
	
	for ll_y = 1 to ll_count_reparti //5
		
		//Donato 10/05/2011
		//su lasciapassare di Beatrice e Fabio, anche se lo stato riga è Evaso
		//devono essere visualizzati i colli
		if ls_flag_evasione = "E" then
			dw_report.setitem(ll_i, "rep_" + string(ll_y) + "_stato", "E")
		end if
		
		//if upperbound(ls_cod_reparto) < ll_y then exit
					
		select sigla
		into   :ls_sigla
		from   anag_reparti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_reparto = :ls_cod_reparto[ll_y];

		choose case ll_stato_reparto[ll_y]
			case 0
				dw_report.setitem(ll_i, "rep_" + string(ll_y), ls_sigla)
				dw_report.setitem(ll_i, "dep_" + string(ll_y), "")
				
			case 1
				dw_report.setitem(ll_i, "rep_" + string(ll_y), "*"+ ls_sigla + "*")
				dw_report.setitem(ll_i, "dep_" + string(ll_y), "S")
				
			case 2
				dw_report.setitem(ll_i, "rep_" + string(ll_y), ">"+ ls_sigla)
				dw_report.setitem(ll_i, "dep_" + string(ll_y), "")

		end choose
		
		wf_log("~tFine recupero sigla reparti.")

		dw_report.setitem(ll_i, "num_colli_letti", ll_num_colli_letti)
		dw_report.setitem(ll_i, "num_colli_previsti", ll_num_colli_previsti)


		//Donato 10/05/2011
		//Beatrice ha richiesto questo
		ls_sigla_sosp_reparto = ""
		if ls_flag_tipo_stampa="B" or ls_flag_tipo_stampa="C" then
			//trattasi di una riga successiva alla prima di un ordine B, o C
			//Beatrice vuole vedere i colli solo sulla prima riga, sulle altre no
			
			//nascondi il tot colli e relativa label
			ld_num_colli = dec(luo_colli_sfuso.get(ls_sigla))
			if isnull(ld_num_colli) then ld_num_colli = 0
			ld_tot_colli += ld_num_colli
			
			dw_report.setitem(ll_i, "num_colli_rep_" + string(ll_y), ld_num_colli)
			luo_colli_sfuso.set(ls_sigla,0)
			
			//Donato 22/01/2012
			//visualizza sigla sospensione, se attivata, sul reparto corrispondente
			//###############################################################
			luo_prod.uof_get_sospensioni_reparto(ls_cod_reparto[ll_y], ll_anno_registrazione, ll_num_registrazione, 0, ls_sigla_sosp_reparto )
			//###############################################################
			
		else
			//prima riga di un ordine B,C, oppure qualsiasi riga di ordine tipo A
		
			//cerca nella tab_ord_ven_colli
			if wf_get_sum_colli(ls_flag_tipo_stampa,ll_anno_registrazione,ll_num_registrazione,&
						ll_prog_riga_ord_ven,ls_cod_reparto[ll_y],ld_num_colli,ls_messaggio) < 0 then
				
				g_mb.messagebox("APICE",ls_messaggio,stopsign!)					
				return -1
			end if
			
			ls_messaggio = ""
						
			ld_tot_colli += ld_num_colli
			
			dw_report.setitem(ll_i, "num_colli_rep_" + string(ll_y), ld_num_colli)
			
			//Donato 22/01/2012
			//visualizza sigla sospensione, se attivata, sul reparto corrispondente
			//###############################################################
			luo_prod.uof_get_sospensioni_reparto(ls_cod_reparto[ll_y], ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_sigla_sosp_reparto )
			//###############################################################
		end if
		
		//Donato 22/01/2012
		//visualizza sigla sospensione, se attivata, sul reparto corrispondente
		//###############################################################
		dw_report.setitem(ll_i, "sosp_" + string(ll_y), ls_sigla_sosp_reparto)
		//###############################################################
		
		wf_log("~tFine recupero num colli e stato sospensioni reparto.")
		
		// -------------------------------------------------------------
		string ls_flag_stato_bolla
		ll_ret = luo_prod.uof_stato_prod_det_ord_ven_trasf(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_dep_filtro, ls_cod_reparto[ll_y], ll_stato_reparto[ll_y], ref ls_flag_stato_bolla, ref ls_messaggio)
		if ll_ret = -1 then
			g_mb.error(ls_messaggio)
			exit
		end if
		
		if not isnull(ls_flag_stato_bolla) then dw_report.setitem(ll_i,"dep_" + string(ll_y), ls_flag_stato_bolla)
		// Se lo stato di produzione è aperto allora devo pulire le caselle precedenti, in modo che venga visualizzata una sola cella di informazione
		// 20/04/2012: la beatrice vuole che se veda sempre lo stato se il numero colli è maggiore di 0
		//if ll_stato_reparto[ll_y] > 0 then wf_pulisci_stato_trasf(ll_i, ll_y)
		// -------------------------------------------------------------
		
		wf_log( "~tFine recupero stato bolla ddt.")
	next
	// ----
	
	
	if ls_flag_riga_sospesa = "S" then
		select sigla
		into :ls_sigla
		from tab_tipi_cause_sospensione
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_causa_sospensione = :ls_cod_tipo_causa_sospensione;
				 
		dw_report.setitem(ll_i, "sigla_sospeso", ls_sigla)
				 
	end if
										 
	dw_report.setitem(ll_i, "imponibile_iva", ld_imponibile_iva)
	
	wf_log("Fine cu_prodotti")
				
loop


rollback;

destroy luo_funzioni

wf_log("FINE ###########")

it_time_start = guo_functions.uof_time_diff(it_time_start, now())

dw_report.setsort("cod_giro A, prog_giro A, cod_cliente A, anno_registrazione A, num_registrazione A, prog_riga_ord_ven A") 
dw_report.sort()

setpointer(arrow!)

dw_report.object.t_1.text = 'C O N S E G N E    P R O D O T T I    I N T R A - G R U P P O'
dw_report.Object.p_tutto.Filename = s_cs_xx.volume + s_cs_xx.risorse + "triangolo.bmp"


// impostazione del font
integer li_bco,li_risposta
string ls_bfo, ls_errore

li_risposta = f_font_barcode(ls_bfo,li_bco,ls_errore)

if li_risposta < 0 then 
	messagebox("SEP","Report giri consegna: " + ls_errore,stopsign!)
	return -1
end if

// EnMe 01/2012:  fissato per questo report
li_bco = 16

dw_report.Modify("barcode_ft.Font.Face='" + ls_bfo + "'")
dw_report.Modify("barcode_ft.Font.Height= -" + string(li_bco) )
dw_report.Modify("barcode_bl.Font.Face='" + ls_bfo + "'")
dw_report.Modify("barcode_bl.Font.Height= -" + string(li_bco) )


dw_report.groupcalc()

//faccio qui il setredraw a TRUE perchè deve ridisegnare il riepilogo peso per cliente!!!
dw_report.setredraw(true)
//------------------------------------------------------------------------------------------------

wf_crea_riepilogo_peso(lds_peso)
destroy lds_peso

st_1.text = "Report completato in " + string(it_time_start, "hh:mm:ss")
setmicrohelp("Report completato in " + string(it_time_start, "hh:mm:ss"))

dw_report.Object.DataWindow.Print.Preview = 'Yes'
dw_report.Object.DataWindow.Print.Preview.Outline = 'No'

dw_report.setredraw(true)
dw_report.change_dw_current()
dw_report.resetupdate()



end function

public function integer wf_controlla_date (integer al_anno_ordine, long al_num_ordine, long al_riga_ordine, string as_deposito_partenza, datetime adt_data_trasf_inizio, datetime adt_data_trasf_fine, ref string as_errore);//torna 0 se il filtro è verificato
//1 se non è verificato (SALTARE)
//-1 in caso di errore

long				ll_count, ll_index
string				ls_sql, ls_dep_succ, ls_cod_dep
datetime			ldt_data_pronto
datastore		lds_data
string				ls_tipo_confronto


if not isnull(adt_data_trasf_inizio) and year(date(adt_data_trasf_inizio))>1950 then
	//data inizio del filtro impostata
	if not isnull(adt_data_trasf_fine) and year(date(adt_data_trasf_fine))>1950 then
		//anche data fine del filtro impostata
		ls_tipo_confronto = "DAL-AL"		//inizio - fine
	else
		//data fine del filtro NON impostata
		ls_tipo_confronto = "DAL"		//inizio - ...
	end if
	
else
	//data inizio del filtro NON impostata
	if not isnull(adt_data_trasf_fine) and year(date(adt_data_trasf_fine))>1950 then
		//anche data fine del filtro impostata
		ls_tipo_confronto = "AL"		//... - fine
	else
		//neanche la data fine del filtro è stata impostata, quindi ignora il filtro
		ls_tipo_confronto = ""
		return 0
	end if
end if


//leggo il deposito origine
select cod_deposito
into :ls_dep_succ
from tes_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:al_anno_ordine and
			num_registrazione=:al_num_ordine;

//in base alla data pronto
//	- di un reparto che deve trasferire ad altro reparto (deposito diverso) 
//	- dell'ultimo reparto (se il deposito origine dell'ordine è diverso)
ls_sql ="select 	det_ordini_produzione.progr_det_produzione,"+&
					"det_ordini_produzione.cod_reparto,"+&
					"anag_reparti.cod_deposito,"+&
					"det_ordini_produzione.data_pronto,"+&
					"'N' as trasf "+&
			"from det_ordini_produzione "+&
			"join anag_reparti on    anag_reparti.cod_azienda=det_ordini_produzione.cod_azienda and "+&
											"anag_reparti.cod_reparto=det_ordini_produzione.cod_reparto "+&
			"where 	det_ordini_produzione.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"det_ordini_produzione.anno_registrazione="+string(al_anno_ordine)+" and "+&
						"det_ordini_produzione.num_registrazione="+string(al_num_ordine)+" and "+&
						"det_ordini_produzione.prog_riga_ord_ven="+string(al_riga_ordine)+" "+&
			"order by progr_det_produzione desc"


ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_count<0 then
	//errore
	destroy lds_data
	return -1
end if

//non appena trovi una data pronto reparto che deve fare il tarsferimento che rientra nel range di date allora torna 0
//se finisci il ciclo for vuol dire che il filtro per date non è stato soddisfatto quindi torna 1
for ll_index=1 to ll_count
	
	//leggo il deposito del reparto della fase e la relativa data pronto
	ldt_data_pronto = lds_data.getitemdatetime(ll_index, 4)
	ls_cod_dep = lds_data.getitemstring(ll_index, 3)
	
	if isnull(ldt_data_pronto) or year(date(ldt_data_pronto))<1950 then
		ls_dep_succ = ls_cod_dep
		continue
	end if
	
	if ls_cod_dep<>ls_dep_succ then
		//il deposito della fase è diverso dal deposito successivo: verifica la data pronto
		
		choose case ls_tipo_confronto
			case "DAL-AL"
				if date(ldt_data_pronto) >= date(adt_data_trasf_inizio) and date(ldt_data_pronto) <= date(adt_data_trasf_fine) then
					//filtro verificato
					return 0
				end if
				
			case "DAL"
				if date(ldt_data_pronto) >= date(adt_data_trasf_inizio) then
					//filtro verificato
					return 0
				end if
				
			case "AL"
				if date(ldt_data_pronto) <= date(adt_data_trasf_fine) then
					//filtro verificato
					return 0
				end if
				
		end choose
		
	else
		//depositi coincidenti, quindi nessu trasferimento previsto in questa fase
	end if
	
	//salvo il deposito della fase letta: sarà quello successivo alla prossima iterata
	ls_dep_succ = ls_cod_dep
next

return 1




//ls_sql = 	"select count(*) "+&
//			"from det_ordini_produzione "+&
//			"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
//						"anno_registrazione="+string(al_anno_ordine)+" and "+&
//						"num_registrazione="+string(al_num_ordine)+" and "+&
//						"prog_riga_ord_ven="+string(al_riga_ordine)+" and "+&
//						"cod_reparto in (	 select cod_reparto from anag_reparti "+&
//												"where    cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
//															"cod_deposito='"+as_deposito_partenza+"')"
//
//
//if not isnull(adt_data_trasf_inizio) and year(date(adt_data_trasf_inizio))>1950 then
//	ls_sql += " and data_pronto>='"+ string(adt_data_trasf_inizio, s_cs_xx.db_funzioni.formato_data)+"' "
//end if
//
//if not isnull(adt_data_trasf_fine) and year(date(adt_data_trasf_fine))>1950 then
//	ls_sql += " and data_pronto<='"+ string(adt_data_trasf_fine, s_cs_xx.db_funzioni.formato_data)+"' "
//end if
//
//if  (not isnull(adt_data_trasf_inizio) and year(date(adt_data_trasf_inizio))>1950) or (not isnull(adt_data_trasf_fine) and year(date(adt_data_trasf_fine))>1950) then
//	ls_sql += " and data_pronto is not null "
//end if
//
//
////se ottieni righe allora torna 0, se invece non ottieni righe torna 1
//ll_count = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
//if ll_count<0 then
//	//errore
//	destroy lds_data
//	return -1
//	
//else
//	ll_count = lds_data.getitemnumber(1, 1)
//	destroy lds_data
//end if
//
//if ll_count > 0 then
//	return 0
//end if
//
//return 1

end function

public subroutine wf_aggiorna_ds_peso (ref datastore ads_data, integer ai_anno_ordine, long al_num_ordine, decimal ad_peso);
long				ll_count, ll_row
dec{5}			ld_peso
string				ls_cod_cliente


ls_cod_cliente = f_des_tabella("tes_ord_ven", "anno_registrazione="+string(ai_anno_ordine)+" and num_registrazione="+string(al_num_ordine), "cod_cliente")

ll_count = ads_data.rowcount()

if ll_count=0 then
	//insert
	ll_row = ads_data.insertrow(0)
	ads_data.setitem(ll_row, 1, ls_cod_cliente)
	ads_data.setitem(ll_row, 2, f_des_tabella("anag_clienti", "cod_cliente='"+ls_cod_cliente+"'", "rag_soc_1"))
	ads_data.setitem(ll_row, 3, ad_peso)
	
else
	//cercala
	ll_row = ads_data.find("cliente='"+ls_cod_cliente+"'", 1, ll_count)
	
	if ll_row = 0 then
		//insert
		ll_row = ads_data.insertrow(0)
		ads_data.setitem(ll_row, 1, ls_cod_cliente)
		ads_data.setitem(ll_row, 2, f_des_tabella("anag_clienti", "cod_cliente='"+ls_cod_cliente+"'", "rag_soc_1"))
		ads_data.setitem(ll_row, 3, ad_peso)
	else
		//update
		ld_peso = ads_data.getitemnumber(ll_row, 3)
		ld_peso = ld_peso + ad_peso
		ads_data.setitem(ll_row, 3, ld_peso)
	end if
	
end if

return
end subroutine

public function string wf_get_note_fisse_cliente (string as_cod_cliente);datastore		lds_data
string				ls_sql, ls_note_fisse, ls_errore
datetime			ldt_oggi
long				ll_index, ll_tot

ls_note_fisse = ""

ldt_oggi = datetime(today(), 00:00:00)

ls_sql = 	"select nota_fissa from tab_note_fisse "+&
			"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and flag_ordine_ven='S' and "+&
						"(data_fine is null or data_fine >= '"+string(ldt_oggi, s_cs_xx.db_funzioni.formato_data)+"') and "+&
						"nota_fissa is not null and nota_fissa<>'' and cod_cliente='"+as_cod_cliente+"' and flag_blocco='N' "

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

for ll_index=1 to ll_tot
	if ls_note_fisse<>"" then ls_note_fisse += "~r~n"
	ls_note_fisse += lds_data.getitemstring(ll_index, 1)
next

return ls_note_fisse
end function

public function string wf_get_note_fido (string as_cod_cliente, datetime adt_data_rif);string						ls_messaggio
integer					li_ret


uo_fido_cliente			luo_fido

luo_fido = create uo_fido_cliente

//disabilito le msg messe in modo selvaggio in una function dello userobject
//(Normalmente tale var.booleana è TRUE)
luo_fido.uof_set_msg_ckeck_cliente(false)

li_ret = luo_fido.uof_check_cliente(as_cod_cliente, adt_data_rif, ls_messaggio)
ls_messaggio = ""

//in assenza di errori, recupero eventuali messaggi sul fido del cliente
if li_ret >= 0 then
	ls_messaggio = luo_fido.uof_get_msg_fido()
	if isnull(ls_messaggio) then ls_messaggio=""
end if

return ls_messaggio
end function

public function string wf_get_post_it (integer ai_anno, long al_numero);
string			ls_testo_post_it, ls_sql, ls_errore
datastore	lds_data
long			ll_tot, ll_index


ls_testo_post_it = ""

ls_sql = 	"select note from tes_ord_ven_note "+&
			"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and flag_postit='S' and "+&
						"anno_registrazione="+string(ai_anno)+" and num_registrazione="+string(al_numero)+ " and "+&
						"note is not null and note<>''"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

for ll_index=1 to ll_tot
	if ls_testo_post_it<>"" then ls_testo_post_it += "~r~n"
	
	 //accodo anche riferimento ad anno e numero ordine
	ls_testo_post_it += lds_data.getitemstring(ll_index, 1) + " (rif.ord. "+string(ai_anno)+"/"+string(al_numero)+")" 
		
next


return ls_testo_post_it

end function

public subroutine wf_aggiorna_ds_riassunto_tes (ref datastore ads_data, integer ai_anno_ordine, long al_num_ordine, string as_cliente);
long				ll_count, ll_row
dec{5}			ld_peso
string				ls_localita, ls_cod_pagamento, ls_temp, ls_flag_contrassegno, ls_temp2


//leggo la località inserita nell'ordine
ls_localita = f_des_tabella("tes_ord_ven", "anno_registrazione="+string(ai_anno_ordine)+" and num_registrazione="+string(al_num_ordine), "localita")
if isnull(ls_localita) or ls_localita="" then
	//in tal caso prendo la località del cliente in anagrafica
	ls_localita = f_des_tabella("anag_clienti", "cod_cliente='"+as_cliente+"'", "localita")
end if
if isnull(ls_localita) then ls_localita=""


//vedo quante righe ci sono nel datastore temporaneo, finora
ll_count = ads_data.rowcount()
if isnull(ll_count) then ll_count = 0

if ll_count>0 then
	//c'è qualcosa, quindi cerca il cliente, se non esiste lo inserisci altrimenti aggiornalo
	ll_count = ads_data.find("cliente='"+as_cliente+"'", 1, ll_count)	
end if

if ll_count > 0 then
	//cliente già trovato, in tal caso se è prevista una destinazione dell'ordine non ancora presente
	//o delle note post_it dell'ordine non ancora presenti, accodale
	ll_row = ll_count		//riga del ds in cui è messo il cliente
	
	//6. gestione localita destinazione ordine -------------------------------------------------------
	if ls_localita<>"" then
	//controlla se la destinazione dell'ordine già è presente (confronto tra stringhe)
	
		ls_temp = ads_data.getitemstring(ll_row, 6)			//leggo quello che c'è attualmente nel campo località
		if isnull(ls_temp) or ls_temp="" then
			//il capo è attualmente vuoto, quindi metti la località dell'ordine senza problemi
			ads_data.setitem(ll_row, 6, ls_localita)
		else
			//campo non vuoto, controlla se per caso la località già è presente
			if pos(ls_temp, ls_localita) > 0 then
				//già presente, quindi non inserire di nuovo (ergo non fare nulla)
			else
				//non presente, quindi accoda
				ls_temp += "~r~n" + ls_localita
				ads_data.setitem(ll_row, 6, ls_temp)
			end if
		end if
	end if

	//colonna simbolo contrassegno, note fisse e note fido già impostate a riga inserita
	
	
	//controlla se ci sono note post-it da inserire (leggi dall'ordine)
	ls_temp2 = wf_get_post_it(ai_anno_ordine, al_num_ordine)
	if ls_temp2<>"" then
		ls_temp = ads_data.getitemstring(ll_row, 8)			//leggo quello che c'è attualmente nei post-it del cliente
		
		if not isnull(ls_temp) and ls_temp<>"" then ls_temp+="~r~n"		//prima di aggiungere lo mando a capo
		
		//accodo e imposto
		ls_temp += ls_temp2
		ads_data.setitem(ll_row, 8, ls_temp)
		
	else
		//nulla da inserire
	end if
	
else
	//non trovato o datastore vuoto, inserisci
	ll_row = ads_data.insertrow(0)
	ads_data.setitem(ll_row, 1, as_cliente)
	ads_data.setitem(ll_row, 2, f_des_tabella("anag_clienti", "cod_cliente='"+as_cliente+"'", "rag_soc_1"))
	
	ads_data.setitem(ll_row, 3, 0)					//peso
	ads_data.setitem(ll_row, 4, 0)					//colli
	ads_data.setitem(ll_row, 5, "KG")				//valore fisso "KG"
	ads_data.setitem(ll_row, 6, ls_localita)		//località destinazione ordine (la metto comunque senza controllare che sia già presente, visto che si trtta di riga appena inserita)
	
	
	//visto che il cliente non c'era nel datastore allora leggo il cod_pagamento
	//leggo il tipo pagamento del cliente, per verificare se previsto avviso contrassegno
	ls_cod_pagamento = f_des_tabella("anag_clienti", "cod_cliente='"+as_cliente+"'", "cod_pagamento")
	ls_flag_contrassegno = f_des_tabella("tab_pagamenti", "cod_pagamento='"+ls_cod_pagamento+"'", "flag_contrassegno")
	if ls_flag_contrassegno<>"S" then ls_flag_contrassegno="N"
	ads_data.setitem(ll_row, 7, ls_flag_contrassegno)
	
	//post-it
	ls_temp = wf_get_post_it(ai_anno_ordine, al_num_ordine)
	ads_data.setitem(ll_row, 8, ls_temp)					//note post-it dell'ordine
	
	ads_data.setitem(ll_row, 9, wf_get_note_fisse_cliente(as_cliente))								//note fisse del cliente
	ads_data.setitem(ll_row, 10, wf_get_note_fido(as_cliente, datetime(today(), 00:00:00)))	//avvisi fido del cliente
	ads_data.setitem(ll_row, 11, "N")	//inserisci come non confermato
	
end if


return
end subroutine

public subroutine wf_aggiorna_ds_riassunto_det (ref datastore ads_data, integer ai_anno_ordine, long al_num_ordine, long al_riga, string as_cliente, decimal ad_peso, decimal ad_colli);
long				ll_row
string				ls_temp, ls_temp2

//in questa funzione aggiungo colli e peso della riga
//nel datastore di riepilogo, il cliente ci deve già essere ...

if isnull(ad_peso) then ad_peso=0
if isnull(ad_colli) then ad_colli=0

//vedo quante righe ci sono nel datastore temporaneo, finora
ll_row = ads_data.find("cliente='"+as_cliente+"'", 1, ads_data.rowcount())

if ll_row > 0 then
	//cliente trovato, aggiorna peso e colli
	ads_data.setitem(ll_row, 3, ads_data.getitemdecimal(ll_row, 3) + ad_peso)
	ads_data.setitem(ll_row, 4, ads_data.getitemdecimal(ll_row, 4) + ad_colli)
	
	ads_data.setitem(ll_row, 11, "S")	//confermato la presenza del cliente nel riepilogo
end if


return
end subroutine

public subroutine wf_mostra_ds_riassunto (datastore ads_data);long				ll_index, ll_new
string				ls_post_it, ls_tutte_le_note, ls_note_fisse, ls_note_fido

for ll_index=1 to ads_data.rowcount()
	//solo se espressamente confermato
	if ads_data.getitemstring(ll_index, 11) = "S" then
		ll_new = dw_riassunto.insertrow(0)
		
		dw_riassunto.setitem(ll_new, "cliente", 					ads_data.getitemstring(ll_index, 1))
		dw_riassunto.setitem(ll_new, "rag_soc_1", 				ads_data.getitemstring(ll_index, 2))
		dw_riassunto.setitem(ll_new, "peso", 					ads_data.getitemdecimal(ll_index, 3))
		dw_riassunto.setitem(ll_new, "colli", 						ads_data.getitemnumber(ll_index, 4))
		dw_riassunto.setitem(ll_new, "um_peso", 				ads_data.getitemstring(ll_index, 5))
		dw_riassunto.setitem(ll_new, "destinazione", 			ads_data.getitemstring(ll_index, 6))
		dw_riassunto.setitem(ll_new, "flag_contrassegno", 	ads_data.getitemstring(ll_index, 7))
		
		ls_post_it = ads_data.getitemstring(ll_index, 8)
		ls_note_fisse = ads_data.getitemstring(ll_index, 9)
		ls_note_fido = ads_data.getitemstring(ll_index, 10)
		ls_tutte_le_note = ""
		
		if len(ls_post_it)>0 then	
			ls_tutte_le_note = "POST-IT:~r~n" + ls_post_it											// post-it degli ordini del cliente
		end if
		
		if len(ls_note_fisse)>0 then												//tutte le note fisse del cliente
			if len(ls_tutte_le_note) > 0 then ls_tutte_le_note+="~r~n~r~n"
			ls_tutte_le_note += "NOTE FISSE:~r~n" + ls_note_fisse
		end if
		
		if len(ls_note_fido)>0 then												//note relative al fido
			if len(ls_tutte_le_note) > 0 then ls_tutte_le_note+="~r~n~r~n"
			ls_tutte_le_note+="NOTE FIDO:~r~n" + ls_note_fido
		end if
		
		dw_riassunto.setitem(ll_new, "tutte_le_note", ls_tutte_le_note)
		
		
	end if
next


return
end subroutine

public function integer wf_imposta_filtro_trasf ();

string			ls_flag_non_ancora_trasf, ls_flag_in_trasf, ls_flag_trasferiti
integer		li_valore


ls_flag_non_ancora_trasf = dw_selezione.getitemstring(dw_selezione.getrow(), "flag_non_ancora_trasf")
ls_flag_in_trasf = dw_selezione.getitemstring(dw_selezione.getrow(), "flag_in_trasf")
ls_flag_trasferiti = dw_selezione.getitemstring(dw_selezione.getrow(), "flag_trasferiti")

li_valore = 0

if isnull(ls_flag_non_ancora_trasf) or ls_flag_non_ancora_trasf="" then ls_flag_non_ancora_trasf="T"
if isnull(ls_flag_in_trasf) or ls_flag_in_trasf="" then ls_flag_in_trasf="T"
if isnull(ls_flag_trasferiti) or ls_flag_trasferiti="" then ls_flag_trasferiti="T"

if ls_flag_non_ancora_trasf = "S" then
	li_valore += 1
end if

if ls_flag_in_trasf="S" then
	li_valore += 2
end if

if ls_flag_trasferiti="S" then
	li_valore += 4
end if

return li_valore

//
//
//if ls_flag_non_ancora_trasf=ls_flag_in_trasf and ls_flag_in_trasf=ls_flag_trasferiti then
//	//hai messo tutti a N o tutti a S o tutti disattivati, quindi vuol dire che non devi considerare come impostato questo filtro
//	//pertanto rimetto tutti a disattivato
//	dw_selezione.setitem(dw_selezione.getrow(), "flag_non_ancora_trasf", "T")
//	dw_selezione.setitem(dw_selezione.getrow(), "flag_in_trasf", "T")
//	dw_selezione.setitem(dw_selezione.getrow(), "flag_trasferiti", "T")
//	
//	//imposto stringa vuota
//	return ""
//end if
//
//return ls_flag_non_ancora_trasf + ls_flag_in_trasf + ls_flag_trasferiti

end function

public function boolean wf_check_filtro_trasf (string as_cod_deposito_origine, string as_reparti[], string as_depositi[], integer ai_valore[], integer ai_stato_trasf_filtro);string				ls_flag_non_ancora_trasf, ls_flag_in_trasf, ls_flag_trasferiti
integer			li_index
boolean			lb_soggetto_a_trasf = false

//se il filtro trasferiemnti è impostato su tutti allora è inutile controllare
//equivale a dire fai vedere tutto, a prescindere dallo stato del trasferimento
if ai_stato_trasf_filtro = 7 then return true

//scopo di questo ciclo è verificare se la riga ha depositi differenti (compreso deposito origine ordine)
//pertanto verifico se esiste almeno un deposito produzione che difefrisce dal deposito origine ordine
//il che equivarrebbe a stabilire che la riga è soggetta a trasferimento ...
for li_index=1 to upperbound(as_depositi[])
	
	if as_depositi[li_index]<>as_cod_deposito_origine then
		//trovato deposito differente da quello origine ordine, quindi c'è un trasferimento
		lb_soggetto_a_trasf = true
		exit
	end if
next

//alla fine di questo ciclo la variabile booleana "lb_soggetto_a_trasf" sarà
//		TRUE se sono previsti trasferimenti
//		FALSE se non sono previsti trasferimenti

//---------------------------------------------------------------------------------------------
//possibili valori dello stato trasferimento
//		0 se non ancora trasferito
//		1 se in trasferimento
//		2 se trasferiti
//---------------------------------------------------------------------------------------------

if not lb_soggetto_a_trasf then return true

choose case ai_stato_trasf_filtro
	case 0	//escludi quelli soggetti a trasferimento, in qualsiasi stato essi siano
		if lb_soggetto_a_trasf then
			return false
//		else
//			return true
		end if
		
	case 1	//solo quelli non ancora trasferiti
		for li_index=1 to upperbound(ai_valore[])
			if ai_valore[li_index] = 0 then return true
		next
		
	case 2	//solo quelli in trasferimento  (ddt non ancora confermato)
		for li_index=1 to upperbound(ai_valore[])
			if ai_valore[li_index] = 1 then return true
		next
		
	case 3	//escludi quelli trasferiti (ddt confermato)
		for li_index=1 to upperbound(ai_valore[])
			if ai_valore[li_index] <= 1 then return true
		next
		
	case 4	//solo quelli trasferiti (ddt confermato)
		for li_index=1 to upperbound(ai_valore[])
			if ai_valore[li_index] = 2 then return true
		next
		
	case 5	//escludi quelli in tarsferimento (ddt non ancora confermato)
		for li_index=1 to upperbound(ai_valore[])
			if ai_valore[li_index] = 0 or ai_valore[li_index] = 2 then return true
		next
		
	case 6	//escludi quelli non ancora trasferiti
		for li_index=1 to upperbound(ai_valore[])
			if ai_valore[li_index] >= 1 then return true
		next
		
	case 7	//includi quelli soggetti a trasferimento, in qualsiasi stato essi siano
		return true
		
end choose

//se sei giunti fin qui escludi
return false

end function

public function string wf_destinazione_diversa (integer ai_anno, long al_numero);string			ls_rag_soc_1, ls_indirizzo, ls_frazione, ls_cap, ls_localita, ls_pr, ls_destinazione

ls_destinazione = ""

select rag_soc_1, indirizzo, frazione, cap, localita, provincia
into :ls_rag_soc_1, :ls_indirizzo, :ls_frazione, :ls_cap, :ls_localita, :ls_pr
from tes_ord_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :ai_anno and
	num_registrazione = :al_numero;
	
if sqlca.sqlcode=0 then
else
	return ""
end if

//composizione destinazione diversa
if ls_rag_soc_1<>"" and not isnull(ls_rag_soc_1) then ls_destinazione += " " + ls_rag_soc_1
if ls_indirizzo<>"" and not isnull(ls_indirizzo) then ls_destinazione += " " + ls_indirizzo
if ls_frazione<>"" and not isnull(ls_frazione) then ls_destinazione += " " + ls_frazione
if ls_cap<>"" and not isnull(ls_cap) then ls_destinazione += " " + ls_cap
if ls_localita<>"" and not isnull(ls_localita) then ls_destinazione += " " + ls_localita
if ls_pr<>"" and not isnull(ls_pr) then ls_destinazione += " (" + ls_pr + ")"

return ls_destinazione


end function

public function string wf_get_operaio (long al_progressivo);string				ls_sql, ls_testo
datastore		lds_data
long				ll_count


ls_sql = "select buffer_colli.cod_operaio, anag_operai.cognome, anag_operai.nome "+&
			"from buffer_colli "+&
			"join anag_operai on anag_operai.cod_azienda=buffer_colli.cod_azienda and anag_operai.cod_operaio=buffer_colli.cod_operaio "+&
			"where 	buffer_colli.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"buffer_colli.progressivo="+string(al_progressivo)+ " "
						
ll_count = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_testo)

ls_testo = ""

if ll_count>0 then
	ls_testo = lds_data.getitemstring(1, 1) + " " + lds_data.getitemstring(1, 3) + " " + lds_data.getitemstring(1, 2)
	
	ls_testo = g_str.replace(ls_testo, "'", "")
	ls_testo = g_str.replace(ls_testo, '"', "")
end if



return ls_testo
end function

public function long wf_buffer_colli (long al_id, string as_flag_stampa_bcl, integer ai_anno, long al_numero, long al_riga, ref long al_num_colli_letti, ref long al_num_colli_previsti, ref string as_errore);
//imposta il numero di colli scansionati in un ID di spedizione e legge il numero dei colli previsti nell'ordine/riga ordine
//anche se nell'ID specificato non ci sono colli snasionati fai comunque vedere la scritta
//														Colli Letti: 0 / Previsti:  N
//torna 	0 se tutto Ok
//			1 se non ci sono colli scansionati in questo ID
//			2 se i colli previsti per l'ordine/riga ordine sono ZERO
//		   -1 in caso di errore critico


al_num_colli_letti = 0
al_num_colli_previsti = 0

if al_id > 0 then
else
	return 0
end if

choose case as_flag_stampa_bcl 
	case "B", "C"
		//-------------------------------------------------------------------------------------------
		//letti
		select count(*)
		into :al_num_colli_letti
		from buffer_colli
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					progressivo=:al_id and
					anno_registrazione=:ai_anno and
					num_registrazione=:al_numero and
					(prog_riga_ord_ven is null or prog_riga_ord_ven=0);
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore lettura da buffer_colli: " + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(al_num_colli_letti) then al_num_colli_letti=0
		
		//se non sono presenti scansioni salta l'ordine
		if al_num_colli_letti = 0 then
			
			//####################################################
			//leggi comunque i num colli previsti, in quanto dovrai comunque mostrarli
			//previsti
			select count(*)
			into :al_num_colli_previsti
			from tab_ord_ven_colli
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						anno_registrazione=:ai_anno and
						num_registrazione=:al_numero and
						(prog_riga_ord_ven is null or prog_riga_ord_ven=0);
			
			if sqlca.sqlcode < 0 then
				as_errore = "Errore lettura da tab_ord_ven_colli: " + sqlca.sqlerrtext
				return -1
			end if
			
			if isnull(al_num_colli_previsti) then al_num_colli_previsti=0
			//####################################################
			
			return 1
		end if
		
		//-------------------------------------------------------------------------------------------
		//previsti
		select count(*)
		into :al_num_colli_previsti
		from tab_ord_ven_colli
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ai_anno and
					num_registrazione=:al_numero and
					(prog_riga_ord_ven is null or prog_riga_ord_ven=0);
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore lettura da tab_ord_ven_colli: " + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(al_num_colli_previsti) then al_num_colli_previsti=0
		return 2
		
		
	case "A"
		//-------------------------------------------------------------------------------------------
		//letti
		select count(*)
		into :al_num_colli_letti
		from buffer_colli
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					progressivo=:al_id and
					anno_registrazione=:ai_anno and
					num_registrazione=:al_numero and
					prog_riga_ord_ven=:al_riga;
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore lettura da buffer_colli: " + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(al_num_colli_letti) then al_num_colli_letti=0
		
		//se non sono presenti scansioni salta l'ordine
		if al_num_colli_letti = 0 then
			
			//####################################################
			//leggi comunque i num colli previsti, in quanto dovrai comunque mostrarli
			//previsti
			select count(*)
			into :al_num_colli_previsti
			from tab_ord_ven_colli
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						anno_registrazione=:ai_anno and
						num_registrazione=:al_numero and
						prog_riga_ord_ven=:al_riga;
			
			if sqlca.sqlcode < 0 then
				as_errore = "Errore lettura da tab_ord_ven_colli: " + sqlca.sqlerrtext
				return -1
			end if
			
			if isnull(al_num_colli_previsti) then al_num_colli_previsti=0
			//####################################################
			
			return 1
		end if
		
		
		//-------------------------------------------------------------------------------------------
		//previsti
		select count(*)
		into :al_num_colli_previsti
		from tab_ord_ven_colli
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ai_anno and
					num_registrazione=:al_numero and
					prog_riga_ord_ven=:al_riga;
		
		if sqlca.sqlcode < 0 then
			as_errore = "Errore lettura da tab_ord_ven_colli: " + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(al_num_colli_previsti) then al_num_colli_previsti=0
		return 2
		
		
	case else
		return 0
		
end choose


return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]


set_w_options(c_CloseNoSave + c_noenablepopup + c_NoResizeWin)

iuo_dw_main = dw_report
dw_report.ib_dw_report = true
dw_riassunto.ib_dw_report = true

lw_oggetti[1] = dw_report
lw_oggetti[2] = cb_stampa
dw_folder.fu_assigntab(1, "Report", lw_oggetti[])

lw_oggetti[1] = dw_riassunto
lw_oggetti[2] = cb_stampa_riassunto
dw_folder.fu_assigntab(2, "Riassunto per Cliente", lw_oggetti[])

dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)


dw_selezione.set_dw_options(sqlca, &
								  pcca.null_object,&
								  c_newonopen, &
								  c_default)
													 
dw_report.set_dw_options(sqlca, &
								  pcca.null_object, &
								  c_noretrieveonopen + &
									c_nomodify + &
									c_nodelete + &
									c_noenablenewonopen + &
									c_noenablemodifyonopen + &
									c_scrollparent + &
									c_disablecc, &
									c_noresizedw + &
									c_nohighlightselected + &
									c_nocursorrowfocusrect + &
									c_nocursorrowpointer)

dw_riassunto.set_dw_options(sqlca, &
								  pcca.null_object, &
								  c_noretrieveonopen + &
									c_nomodify + &
									c_nodelete + &
									c_noenablenewonopen + &
									c_noenablemodifyonopen + &
									c_scrollparent + &
									c_disablecc, &
									c_noresizedw + &
									c_nohighlightselected + &
									c_nocursorrowfocusrect + &
									c_nocursorrowpointer)


dw_selezione.setitem(dw_selezione.getrow(),"data_inizio", datetime(today(), 00:00:00))
dw_selezione.setitem(dw_selezione.getrow(),"data_fine",datetime(today(), 00:00:00) )

save_on_close(c_socnosave)

event post ue_posiziona_window()



end event

on w_report_giri_consegne_stato_prod.create
int iCurrent
call super::create
this.cb_stampa_riassunto=create cb_stampa_riassunto
this.cb_stampa=create cb_stampa
this.dw_riassunto=create dw_riassunto
this.cb_annulla=create cb_annulla
this.progess=create progess
this.st_1=create st_1
this.dw_report=create dw_report
this.cb_1=create cb_1
this.dw_selezione=create dw_selezione
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa_riassunto
this.Control[iCurrent+2]=this.cb_stampa
this.Control[iCurrent+3]=this.dw_riassunto
this.Control[iCurrent+4]=this.cb_annulla
this.Control[iCurrent+5]=this.progess
this.Control[iCurrent+6]=this.st_1
this.Control[iCurrent+7]=this.dw_report
this.Control[iCurrent+8]=this.cb_1
this.Control[iCurrent+9]=this.dw_selezione
this.Control[iCurrent+10]=this.dw_folder
end on

on w_report_giri_consegne_stato_prod.destroy
call super::destroy
destroy(this.cb_stampa_riassunto)
destroy(this.cb_stampa)
destroy(this.dw_riassunto)
destroy(this.cb_annulla)
destroy(this.progess)
destroy(this.st_1)
destroy(this.dw_report)
destroy(this.cb_1)
destroy(this.dw_selezione)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;DataWindowChild			ldwc_spedizioni


f_po_loaddddw_dw(dw_selezione, &
                 "cod_giro", &
                 sqlca, &
                 "tes_giri_consegne", &
                 "cod_giro_consegna", &
                 "des_giro_consegna", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


f_po_loaddddw_dw(dw_selezione, &
                 "cod_deposito_partenza", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_selezione, &
                 "cod_deposito_arrivo", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

event post ue_carica_deposito_utente()

					  
f_po_loaddddw_dw(dw_selezione, &
                 "cod_deposito_giro", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_selezione, &
                 "cod_deposito_origine", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
					  

dw_selezione.GetChild("id_spedizione", ldwc_spedizioni)
ldwc_spedizioni.SetTransObject(SQLCA)
ldwc_spedizioni.Retrieve(s_cs_xx.cod_azienda)


end event

event resize;/** STO CAZZO DI ANCESTOR SCRIPT BISOGNA TOGLIERLO DALLA CIRCOLAZIONE **/

dw_selezione.move(20, 20)
dw_selezione.width = 5000

st_1.move(40, dw_selezione.y + dw_selezione.height)


//cb_1.x = dw_selezione.x + dw_selezione.width + 40
//cb_1.y = dw_selezione.y

//cb_annulla.x = cb_1.x
//cb_annulla.y = cb_1.y + cb_1.height + 20

//cb_stampa.x = cb_1.x
//cb_stampa.y = cb_annulla.y + cb_annulla.height + 20

//cb_stampa_riassunto.x=cb_stampa.x
//cb_stampa_riassunto.y=cb_stampa.y

progess.move(st_1.x + st_1.width + 20, st_1.y)
progess.height = st_1.height


dw_folder.move(20, st_1.y + st_1.height + 20)
dw_folder.resize(newwidth - 60, newheight - dw_selezione.height - 20)


dw_report.move(dw_folder.x + 20, dw_folder.y + 100)
dw_report.resize(dw_folder.width - 40, dw_folder.height - 100)
dw_report.bringtotop = true

dw_riassunto.x = dw_report.x
dw_riassunto.y = dw_report.y
dw_riassunto.width = dw_report.width
dw_riassunto.height = dw_report.height

end event

type cb_stampa_riassunto from commandbutton within w_report_giri_consegne_stato_prod
integer x = 3634
integer y = 476
integer width = 576
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa Riassunto"
end type

event clicked;string		ls_flag_tipo_report

ls_flag_tipo_report = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_tipo_report")

if ls_flag_tipo_report = "G" or ls_flag_tipo_report="L" then
	//report giri consegna o Lista di carico
	dw_riassunto.print()
	
else
	g_mb.warning("La stampa del riassunto è previsto solo per i due report 'Giri Consegne' e 'Lista di Carico'")
	return
end if
end event

type cb_stampa from commandbutton within w_report_giri_consegne_stato_prod
integer x = 3845
integer y = 476
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;string		ls_flag_tipo_report

dw_report.print()

ls_flag_tipo_report = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_tipo_report")

if ls_flag_tipo_report = "G" or ls_flag_tipo_report="L" then
	//report giri consegna o Lista di carico
	dw_riassunto.print()
end if
end event

type dw_riassunto from uo_cs_xx_dw within w_report_giri_consegne_stato_prod
integer x = 800
integer y = 640
integer width = 2679
integer height = 1136
integer taborder = 20
string dataobject = "d_report_giri_stato_prod_riassunto"
boolean border = false
end type

type cb_annulla from commandbutton within w_report_giri_consegne_stato_prod
integer x = 3465
integer y = 28
integer width = 366
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;ib_halt = true
end event

type progess from hprogressbar within w_report_giri_consegne_stato_prod
boolean visible = false
integer x = 2318
integer y = 480
integer width = 1111
integer height = 76
unsignedinteger maxposition = 100
integer setstep = 1
end type

type st_1 from statictext within w_report_giri_consegne_stato_prod
integer x = 32
integer y = 480
integer width = 2217
integer height = 76
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean border = true
boolean focusrectangle = false
end type

type dw_report from uo_cs_xx_dw within w_report_giri_consegne_stato_prod
integer x = 14
integer y = 688
integer width = 4347
integer height = 1552
integer taborder = 10
string dataobject = "d_report_giri_stato_prod"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_flag_tipo_report

dw_selezione.accepttext()
ls_flag_tipo_report = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_tipo_report")
progess.visible = false

if ls_flag_tipo_report = "G" or ls_flag_tipo_report = "E"  then
	//report giri consegna
	if ls_flag_tipo_report = "G" then
		dw_report.dataobject = "d_report_giri_stato_prod"
	else
		dw_report.dataobject = "d_report_giri_stato_prod_semplif"
	end if
		
	wf_report_giri_consegna()
	
elseif ls_flag_tipo_report = 'S' or ls_flag_tipo_report = 'T' then
	// report consegna produzione intra-gruppo
	
	
	if ls_flag_tipo_report = 'S' then
		dw_report.dataobject = "d_report_giri_cons_prod_intragrup"
	else
		dw_report.dataobject = "d_report_giri_cons_prod_intragrup_date"
	end if
	
	progess.visible = true
	
	// stefanop 12/047/2012:unito tutto in un'unica JOIN
	// --------------------------------------------------------------------
	wf_report_giri_trasferimento(ls_flag_tipo_report)
	// --------------------------------------------------------------------
	
	progess.visible = false

else
	//report lista di carico
	dw_report.dataobject = "d_report_giri_stato_prod_lst_carico"
	wf_report_lista_carico()
end if
end event

event pcd_print;call super::pcd_print;string		ls_flag_tipo_report

ls_flag_tipo_report = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_tipo_report")

if ls_flag_tipo_report = "G" or ls_flag_tipo_report="L" then
	//report giri consegna o Lista di carico
	dw_riassunto.print()
end if
end event

type cb_1 from commandbutton within w_report_giri_consegne_stato_prod
integer x = 3845
integer y = 28
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ricerca"
end type

event clicked;dw_report.resetupdate()
dw_report.change_dw_current()

ib_halt = false

parent.postevent("pc_retrieve")
end event

type dw_selezione from uo_cs_xx_dw within w_report_giri_consegne_stato_prod
integer x = 18
integer y = 20
integer width = 4357
integer height = 432
integer taborder = 120
string dataobject = "d_ext_sel_report_giri_consegna_stato_prod"
boolean border = false
end type

event clicked;call super::clicked;if isvalid(dwo) then

	if dwo.name = "b_ricerca_cliente" then

		s_cs_xx.parametri.parametro_w_cs_xx_1 = parent
		dw_selezione.change_dw_current()
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")

	end if
end if
end event

event itemchanged;call super::itemchanged;string						ls_null, ls_cod_deposito, ls_operaio
integer					li_tab_order


setnull(ls_null)

choose case dwo.name
	
	case "id_spedizione"
		ls_operaio = ""
		if data <> "" then
			ls_operaio = wf_get_operaio(long(data))
		end if
		try
			object.operaio_spedizione_t.text = ls_operaio
		catch (runtimeerror err)
		end try
	
	case "flag_tipo_report"
		// se "Consegne produzione intra-gruppo" il codice giro non deve essere usato
		if data = "S" or data = "T" then
			setitem(row, "cod_giro", ls_null)
			setitem(row, "flag_stato", "R")
			setitem(row, "flag_avan_prod", 1)
			modify("cod_giro.TabSequence=0")
		else
			modify("cod_giro.TabSequence=40")
		end if
		
		//abilito/disabilito il tab riassunto per cliente
		if data = "G" or data = "L" then
			//solo per Giri Consegne e Lista di Carico
			dw_folder.fu_enabletab(2)
		else
			dw_folder.fu_disabletab(2)
		end if
		
	//----------------------------------------------------------------------------------------------
	case "cod_giro"
		if data<>"" then
			//hai selezionato un giro in cui cercare
			//proteggi e imposta il campo deposito giro
			
			select cod_deposito
			into :ls_cod_deposito
			from tes_giri_consegne
			where	cod_azienda=:s_cs_xx.cod_azienda and
						cod_giro_consegna=:data;
			
			if g_str.isempty(ls_cod_deposito) then
				setnull(ls_cod_deposito)
			end if
			
			//Inoltre lascia la variabile li_tab_order a ZERO
		else
			//sproteggi il campo deposito giro e ripuliscilo
			setnull(ls_cod_deposito)
			li_tab_order = 5000
		end if
		
		//imposta il taborder (che potrebbe essere anche ZERO)
		settaborder("cod_deposito_giro", li_tab_order)
		
		//imposta il valore (che potrebbe essere anche vuoto)
		setitem(1, "cod_deposito_giro", ls_cod_deposito)
		
end choose
end event

event doubleclicked;call super::doubleclicked;choose case dwo.name
	case "data_inizio_t"
		ib_debug = not ib_debug
		
end choose
end event

type dw_folder from u_folder within w_report_giri_consegne_stato_prod
integer y = 572
integer width = 4370
integer height = 1688
integer taborder = 60
end type

event po_tabclicked;call super::po_tabclicked;


if i_SelectedTab = 1 then dw_report.change_dw_current( )
end event

