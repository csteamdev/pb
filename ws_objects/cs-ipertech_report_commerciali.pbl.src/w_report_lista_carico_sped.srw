﻿$PBExportHeader$w_report_lista_carico_sped.srw
$PBExportComments$REPORT RIGHE ORDINI
forward
global type w_report_lista_carico_sped from w_cs_xx_principale
end type
type dw_report_pack from uo_cs_xx_dw within w_report_lista_carico_sped
end type
type rb_packing_list from radiobutton within w_report_lista_carico_sped
end type
type rb_lista_carico from radiobutton within w_report_lista_carico_sped
end type
type st_msg from statictext within w_report_lista_carico_sped
end type
type dw_report from uo_cs_xx_dw within w_report_lista_carico_sped
end type
type cb_1 from commandbutton within w_report_lista_carico_sped
end type
type dw_selezione from uo_cs_xx_dw within w_report_lista_carico_sped
end type
type cb_reset from commandbutton within w_report_lista_carico_sped
end type
end forward

global type w_report_lista_carico_sped from w_cs_xx_principale
integer x = 5
integer y = 4
integer width = 4032
integer height = 2644
string title = "Report Lista Carico Spedizione"
event ue_posizione ( )
dw_report_pack dw_report_pack
rb_packing_list rb_packing_list
rb_lista_carico rb_lista_carico
st_msg st_msg
dw_report dw_report
cb_1 cb_1
dw_selezione dw_selezione
cb_reset cb_reset
end type
global w_report_lista_carico_sped w_report_lista_carico_sped

type variables
string is_CSB
string is_sql_origine_lista, is_sql_origine_pack
end variables

forward prototypes
public subroutine wf_logo ()
public function integer wf_lista_carico ()
public function integer wf_packing_list ()
public subroutine wf_where (ref string fs_where)
end prototypes

public subroutine wf_logo ();string ls_path_logo_1, ls_modify

select parametri_azienda.stringa
into   :ls_path_logo_1
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO3';

ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"

dw_report.modify(ls_modify)
dw_report.object.datawindow.print.preview = "Yes"

dw_report_pack.modify(ls_modify)
dw_report_pack.object.datawindow.print.preview = "Yes"
end subroutine

public function integer wf_lista_carico ();string			ls_sql, ls_where

setpointer(Hourglass!)

dw_report.visible = true
dw_report_pack.visible = false

dw_selezione.accepttext()
dw_report.reset()

st_msg.text = ""
dw_report.setredraw(false)

ls_sql = is_sql_origine_lista

//nella select originaria c'è gia una where dovuta alle join....
ls_sql += " and det_ord_ven.cod_azienda='"+s_cs_xx.cod_azienda +"' "

wf_where(ls_where)

ls_sql += ls_where

dw_report.setsqlselect(ls_sql)
dw_report.retrieve()

setpointer(Arrow!)

dw_report.sort()
dw_report.groupcalc()
dw_report.setredraw(true)

dw_report.change_dw_current()

iuo_dw_main = dw_report

st_msg.text = "Pronto!"

return 1
end function

public function integer wf_packing_list ();string			ls_sql, ls_where

setpointer(Hourglass!)

dw_report.visible = false
dw_report_pack.visible = true

dw_selezione.accepttext()
dw_report_pack.reset()

st_msg.text = ""
dw_report_pack.setredraw(false)

ls_sql = is_sql_origine_pack

//nella select originaria c'è gia una where dovuta alle join....
ls_sql += " and det_ord_ven.cod_azienda='"+s_cs_xx.cod_azienda +"' "

wf_where(ls_where)

ls_sql += ls_where

dw_report_pack.setsqlselect(ls_sql)
dw_report_pack.retrieve()

setpointer(Arrow!)

dw_report_pack.sort()
dw_report_pack.groupcalc()
dw_report_pack.setredraw(true)

dw_report_pack.change_dw_current()

iuo_dw_main = dw_report_pack

st_msg.text = "Pronto!"

return 1
end function

public subroutine wf_where (ref string fs_where);string			ls_cod_cliente, ls_flag_evaso, ls_flag_evasione_1, ls_flag_evasione_2, ls_nazione, &
				ls_nazione_1, ls_nazione_2, ls_nazione_3, ls_flag_blocco, ls_cod_agente

long			ll_ordine_inizio, ll_ordine_fine, ll_anno_ordine

datetime		ldt_data_inizio, ldt_data_fine,ldt_reg_data_inizio, ldt_reg_data_fine	


fs_where = ""

//cliente ------------------------------------------------------------------------------------------
ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")
if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
	fs_where += "and tes_ord_ven.cod_cliente='"+ls_cod_cliente+"' "
end if

//flag evasione
ls_flag_evaso = dw_selezione.getitemstring(1,"flag_evaso")
if ls_flag_evaso = "A" then
	ls_flag_evasione_1="A"
	ls_flag_evasione_2="A"
elseif ls_flag_evaso = "P" then
	ls_flag_evasione_1="P"
	ls_flag_evasione_2="P"
elseif ls_flag_evaso = "E" then
	ls_flag_evasione_1="E"
	ls_flag_evasione_2="E"
elseif ls_flag_evaso = "T" then
	ls_flag_evasione_1="A"
	ls_flag_evasione_2="P"
end if	

fs_where += "and (det_ord_ven.flag_evasione='"+ls_flag_evasione_1 + "' or "+&
						 "det_ord_ven.flag_evasione='" +ls_flag_evasione_2+ "') "

//da data reg a data reg ------------------------------------------------------------------------
ldt_reg_data_inizio = dw_selezione.getitemdatetime(1,"data_reg_inizio")
ldt_reg_data_fine   = dw_selezione.getitemdatetime(1,"data_reg_fine")
if isnull(ldt_reg_data_inizio) then ldt_reg_data_inizio = datetime(date("01/01/1900"),00:00:00)
if isnull(ldt_reg_data_fine) then ldt_reg_data_fine = datetime(date("31/12/2999"),00:00:00)

fs_where += "and tes_ord_ven.data_registrazione >= '" +string(ldt_reg_data_inizio, s_cs_xx.db_funzioni.formato_data)+ "' "
fs_where += "and tes_ord_ven.data_registrazione <= '" +string(ldt_reg_data_fine, s_cs_xx.db_funzioni.formato_data)+ "' "

//da data consegna a data consegna -----------------------------------------------------------------------------
ldt_data_inizio = dw_selezione.getitemdatetime(1,"d_data_da")
ldt_data_fine   = dw_selezione.getitemdatetime(1,"d_data_a")
if isnull(ldt_data_inizio) then ldt_data_inizio = datetime(date("01/01/1900"),00:00:00)
if isnull(ldt_data_fine) then ldt_data_fine = datetime(date("31/12/2999"),00:00:00)

fs_where += "and tes_ord_ven.data_consegna >= '" +string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data)+ "' "
fs_where += "and tes_ord_ven.data_consegna <= '" +string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data)+ "' "

//anno registrazione ------------------------------------------------------
ll_anno_ordine = dw_selezione.getitemnumber(1,"n_anno")
if ll_anno_ordine>0 then fs_where += " and det_ord_ven.anno_registrazione="+string(ll_anno_ordine)+" "

//dal numero al numero -----------------------------------------------------
ll_ordine_inizio = dw_selezione.getitemnumber(1,"n_ord_da")
ll_ordine_fine   = dw_selezione.getitemnumber(1,"n_ord_a")
if isnull(ll_ordine_inizio) or ll_ordine_inizio = 0 then ll_ordine_inizio = 1
if isnull(ll_ordine_fine) or ll_ordine_fine = 0 then ll_ordine_fine = 999999

fs_where += " and det_ord_ven.num_registrazione >= "+string(ll_ordine_inizio)+" "
fs_where += " and det_ord_ven.num_registrazione <= "+string(ll_ordine_fine)+" "

//flag blocco ------------------------------------------------------------------
ls_flag_blocco = dw_selezione.getitemstring(1,"flag_blocco")
if isnull(ls_flag_blocco) or ls_flag_blocco="" then ls_flag_blocco = 'N'

fs_where += "and det_ord_ven.flag_blocco='"+ls_flag_blocco+"' "

//nazione ---------------------------------------------------------------------
ls_nazione = dw_selezione.getitemstring(1,"nazione")
if ls_nazione = "ITA" then
	ls_nazione_1 = "F"
	ls_nazione_2 = "P"
	ls_nazione_3 = "S" 
	
elseif ls_nazione = "EST" then	
	ls_nazione_1 = "E"
	ls_nazione_2 = "C"
	ls_nazione_3 = ""	
end if

fs_where += " and (anag_clienti.flag_tipo_cliente='"+ls_nazione_1+"' or "+&
						 "anag_clienti.flag_tipo_cliente='"+ls_nazione_2+"' or "+&
						 "anag_clienti.flag_tipo_cliente='"+ls_nazione_3+"') "
						 
//agente ----------------------------------------------------------------------------
ls_cod_agente = dw_selezione.getitemstring(1,"cod_agente")
if ls_cod_agente <> "" and not isnull(ls_cod_agente) then
	fs_where += "and (tes_ord_ven.cod_agente_1='"+ls_cod_agente+"' or "+&
					 " tes_ord_ven.cod_agente_2='"+ls_cod_agente+"') "
end if
end subroutine

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true
dw_report_pack.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_report_pack.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
								 
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
save_on_close(c_socnosave)
iuo_dw_main = dw_report

dw_selezione.object.flag_visualizza_det.visible = 0
dw_selezione.object.t_10.visible = 0

dw_selezione.resetupdate()
//postevent("ue_posizione")

//lettura parametro aziendale Carattere Speciale barcode (specifica Carico ordini con palmare)
select stringa
into   :is_CSB
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='CSB' and 
		flag_parametro='S';

if sqlca.sqlcode < 0 then
	messagebox("SEP","Errore in ricerca del parametro CSB in tabella parametri azienda: " + sqlca.sqlerrtext,stopsign!)
	is_CSB = ""
	
elseif sqlca.sqlcode=100 then
	is_CSB = ""
end if

if isnull(is_CSB) then is_CSB =""
//fine modifica ------------------------------------------------


//conserva gli sql originali
is_sql_origine_lista = dw_report.getsqlselect()
is_sql_origine_pack = dw_report_pack.getsqlselect()



wf_logo()
end event

on w_report_lista_carico_sped.create
int iCurrent
call super::create
this.dw_report_pack=create dw_report_pack
this.rb_packing_list=create rb_packing_list
this.rb_lista_carico=create rb_lista_carico
this.st_msg=create st_msg
this.dw_report=create dw_report
this.cb_1=create cb_1
this.dw_selezione=create dw_selezione
this.cb_reset=create cb_reset
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_pack
this.Control[iCurrent+2]=this.rb_packing_list
this.Control[iCurrent+3]=this.rb_lista_carico
this.Control[iCurrent+4]=this.st_msg
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.cb_1
this.Control[iCurrent+7]=this.dw_selezione
this.Control[iCurrent+8]=this.cb_reset
end on

on w_report_lista_carico_sped.destroy
call super::destroy
destroy(this.dw_report_pack)
destroy(this.rb_packing_list)
destroy(this.rb_lista_carico)
destroy(this.st_msg)
destroy(this.dw_report)
destroy(this.cb_1)
destroy(this.dw_selezione)
destroy(this.cb_reset)
end on

event pc_setddlb;call super::pc_setddlb;//Donato 06/04/2009 aggiunto filtro per agente
f_PO_LoadDDDW_DW(dw_selezione,"cod_agente",sqlca,&
                 "anag_agenti","cod_agente","rag_soc_1", &
                 "(anag_agenti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((anag_agenti.flag_blocco <> 'S') or (anag_agenti.flag_blocco = 'S' and anag_agenti.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))" )
//fine modifica ---------------------------

////Donato 06/04/2009 sistemazione anomalia (descizioni pagamento e listino)
//f_po_loaddddw_dw(dw_report, &
//                 "tes_ord_ven_cod_tipo_listino_prodotto", &
//                 sqlca, &
//                 "tab_tipi_listini_prodotti", &
//                 "cod_tipo_listino_prodotto", &
//                 "des_tipo_listino_prodotto", &
//				"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
////                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//					  
//f_po_loaddddw_dw(dw_report, &
//                 "tes_ord_ven_cod_pagamento", &
//                 sqlca, &
//                 "tab_pagamenti", &
//                 "cod_pagamento", &
//                 "des_pagamento", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
////			  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
////fine modifica ----------------------------------
end event

type dw_report_pack from uo_cs_xx_dw within w_report_lista_carico_sped
boolean visible = false
integer x = 23
integer y = 508
integer width = 3941
integer height = 2004
integer taborder = 50
string dataobject = "d_report_packing_list"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
end type

type rb_packing_list from radiobutton within w_report_lista_carico_sped
integer x = 873
integer y = 408
integer width = 558
integer height = 76
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Report Packing List"
end type

event clicked;dw_report.visible = false
dw_report_pack.visible = true

iuo_dw_main = dw_report_pack
end event

type rb_lista_carico from radiobutton within w_report_lista_carico_sped
integer x = 142
integer y = 404
integer width = 613
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Report Lista di Carico"
boolean checked = true
end type

event clicked;dw_report.visible = true
dw_report_pack.visible = false

iuo_dw_main = dw_report
end event

type st_msg from statictext within w_report_lista_carico_sped
integer x = 3374
integer y = 216
integer width = 594
integer height = 156
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type dw_report from uo_cs_xx_dw within w_report_lista_carico_sped
integer x = 23
integer y = 508
integer width = 3941
integer height = 2004
integer taborder = 30
string dataobject = "d_report_lista_carico_sped"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
end type

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type cb_1 from commandbutton within w_report_lista_carico_sped
integer x = 3360
integer y = 16
integer width = 338
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "R&eport"
end type

event clicked;
if rb_lista_carico.checked then
	//report lista di carico
	
	wf_lista_carico()
else
	//report packing list
	
	wf_packing_list()
end if
end event

type dw_selezione from uo_cs_xx_dw within w_report_lista_carico_sped
integer y = 20
integer width = 3314
integer height = 380
integer taborder = 20
string dataobject = "d_selezione_righe_ordini_gibus"
boolean border = false
end type

event pcd_new;call super::pcd_new;setitem(getrow(),"n_anno",f_anno_esercizio())
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
end choose
end event

type cb_reset from commandbutton within w_report_lista_carico_sped
integer x = 3360
integer y = 104
integer width = 338
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Reset"
end type

event clicked;string ls_null
date ld_null
long ll_null

setnull(ld_null)
setnull(ll_null)
setnull(ls_null)

dw_selezione.setitem(dw_selezione.getrow(), "data_reg_inizio", ld_null)
dw_selezione.setitem(dw_selezione.getrow(), "data_reg_fine", ld_null)
dw_selezione.setitem(dw_selezione.getrow(), "d_data_da", ld_null)
dw_selezione.setitem(dw_selezione.getrow(), "d_data_a", ld_null)
dw_selezione.setitem(dw_selezione.getrow(), "cod_cliente", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "n_ord_da", 1)
dw_selezione.setitem(dw_selezione.getrow(), "n_ord_a", 999999)
dw_selezione.setitem(dw_selezione.getrow(),"n_anno",f_anno_esercizio())
dw_selezione.setitem(dw_selezione.getrow(), "flag_evaso", "A")
dw_selezione.setitem(dw_selezione.getrow(), "nazione", "ITA")
dw_selezione.setitem(dw_selezione.getrow(), "flag_blocco", "N")

dw_selezione.setitem(dw_selezione.getrow(), "flag_visualizza_det", "S")
dw_selezione.setitem(dw_selezione.getrow(), "cod_agente", ls_null)

end event

