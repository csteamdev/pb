﻿$PBExportHeader$w_report_ord_ven_euro.srw
$PBExportComments$Finestra Report Ordini Vendita
forward
global type w_report_ord_ven_euro from w_cs_xx_principale
end type
type ddlb_evasione from dropdownlistbox within w_report_ord_ven_euro
end type
type st_1 from statictext within w_report_ord_ven_euro
end type
type em_copie from editmask within w_report_ord_ven_euro
end type
type cb_tende from commandbutton within w_report_ord_ven_euro
end type
type cb_sfuso from commandbutton within w_report_ord_ven_euro
end type
type cb_stampa from commandbutton within w_report_ord_ven_euro
end type
type cb_tecniche from commandbutton within w_report_ord_ven_euro
end type
type dw_report_ord_ven_tende from uo_cs_xx_dw within w_report_ord_ven_euro
end type
type dw_report_ord_ven_tecniche from uo_cs_xx_dw within w_report_ord_ven_euro
end type
type dw_report_ord_ven_sfuso from uo_cs_xx_dw within w_report_ord_ven_euro
end type
end forward

global type w_report_ord_ven_euro from w_cs_xx_principale
integer width = 3968
integer height = 2020
string title = "Stampa Ordini Clienti"
boolean minbox = false
boolean hscrollbar = true
boolean vscrollbar = true
ddlb_evasione ddlb_evasione
st_1 st_1
em_copie em_copie
cb_tende cb_tende
cb_sfuso cb_sfuso
cb_stampa cb_stampa
cb_tecniche cb_tecniche
dw_report_ord_ven_tende dw_report_ord_ven_tende
dw_report_ord_ven_tecniche dw_report_ord_ven_tecniche
dw_report_ord_ven_sfuso dw_report_ord_ven_sfuso
end type
global w_report_ord_ven_euro w_report_ord_ven_euro

type variables
long il_anno_registrazione, il_num_registrazione
end variables

forward prototypes
public function integer wf_report_tende ()
public function integer wf_report_tecniche ()
public function integer wf_report_sfuso ()
public function integer wf_imposta_barcode ()
public function string wf_imposta_logo_personalizzato (string as_logo_etichetta, string as_cod_cliente)
end prototypes

public function integer wf_report_tende ();boolean 	lb_modello, lb_test,lb_centro_gibus, lb_evasi

string 	ls_stringa, ls_cod_tipo_ord_ven, ls_cod_banca_clien_for, ls_cod_cliente, ls_des_valuta, ls_des_valuta_lingua, &
		 	ls_cod_valuta, ls_cod_pagamento, ls_num_ord_cliente, ls_cod_porto, ls_cod_resa, ls_cod_vettore, &
		 	ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_anag_vettori_rag_soc_1, &
		 	ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
		 	ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, ls_des_operatore, &
		 	ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli, ls_null, &
		 	ls_partita_iva, ls_cod_fiscale, ls_cod_lingua, ls_flag_tipo_cliente, ls_des_giro_consegna, &
		 	ls_des_tipo_ord_ven, ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, ls_sconti,&
  		 	ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua , ls_cod_giro_consegna, &
		 	ls_des_prodotto_anag, ls_flag_stampa_ordine, ls_cod_tipo_det_ven, ls_cod_abi, ls_cod_cab, &
		 	ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_stringa_euro, ls_dicitura_std,ls_cod_operatore, &
		 	ls_cod_prodotto_finito, ls_cod_modello, ls_cod_tessuto, ls_cod_non_a_magazzino, ls_cod_tessuto_mantovana, &
		 	ls_cod_colore_mantovana, ls_flag_tipo_mantovana,ls_flag_cordolo,ls_flag_passamaneria,ls_cod_verniciatura, &
		 	ls_cod_comando,ls_pos_comando,ls_cod_tipo_supporto,ls_flag_luce_finita,ls_note,ls_colore_passamaneria,&
		 	ls_colore_cordolo,ls_des_vernice_fc, ls_abbreviazione_comando, ls_descrizione_riga, ls_dimensioni, &
		 	ls_des_tessuto, ls_des_verniciatura, ls_abbreviazione_verniciatura, ls_abbreviazione_supporto, &
		 	ls_flag_tipo_report, ls_des_comando_1, ls_cod_comando_2, ls_des_comando_2, ls_flag_tassativo, &
		 	ls_flag_st_note_tes, ls_flag_st_note_pie, ls_flag_st_note_det, ls_flag_allegati, ls_cod_misura_mag, &
			ls_formato, ls_fax, ls_telefono, ls_alias_cliente, ls_flag, ls_cod_tipo_anagrafica, ls_des_tipo_anagrafica,&
			ls_rif_interscambio, ls_rif_vs_ordine, ls_nota_prodotto, ls_evasione, ls_telex, ls_cod_deposito, ls_des_deposito, &
			ls_cod_agente_1, ls_cod_agente_2, ls_cod_imballo, ls_des_imballo, ls_des_imballi_lingua, ls_cod_verniciatura_2, &
			ls_cod_verniciatura_3, ls_flag_fc_vernic_2, ls_des_vernic_fc_2,ls_flag_fc_vernic_3, ls_des_vernic_fc_3, &
			ls_nota_testata_st_ord,ls_nota_piede_st_ord, ls_nota_video_st_ord, ls_stato_cli, ls_logo_etichetta

long     ll_errore, ll_prog_riga_ord_ven, ll_num_riga_appartenenza, ll_riga_appartenenza_padre

dec{4} 	ld_tot_val_ordine, ld_sconto, ld_quan_ordine, ld_prezzo_vendita, ld_sconto_1, &
	      	ld_sconto_2, ld_val_riga, ld_sconto_pagamento, ld_cambio_ven, ld_num_colli, ld_peso_lordo, &
			ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t,ld_altezza_mantovana,ld_altezza_asta, ld_quantita_um, ld_prezzo_um, &
         	ld_tot_val_ordine_euro, ld_quan_evasa, ld_provvigione_1, ld_dim_x_riga, ld_dim_y_riga, ld_val_gtot
			
dec{5}   ld_fat_conversione_ven

datetime ldt_data_ord_cliente, ldt_data_registrazione, ldt_data_consegna,ldt_data_pronto,ldt_data_consegna_testata

wf_imposta_barcode()
setnull(ls_null)

if ddlb_evasione.text = "Stampa tutti i dettagli" then
	lb_evasi = true
else
	lb_evasi = false
end if

dw_report_ord_ven_tende.reset()

lb_centro_gibus = false
select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'CG3';
if ls_flag = "S" then lb_centro_gibus = true

select stringa  
into  :ls_stringa  
from  parametri_azienda  
where cod_azienda = :s_cs_xx.cod_azienda and  
      cod_parametro = 'CVL';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

select stringa  
into  :ls_stringa_euro
from  parametri_azienda  
where cod_azienda = :s_cs_xx.cod_azienda and  
      cod_parametro = 'EUR';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa_euro)
end if

// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua

select flag
into	 :ls_flag
from	 parametri_azienda
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'DTL';
		 
if sqlca.sqlcode = 0 and not isnull(ls_flag) and ls_flag = "S" then
	ls_flag = "S" 
else
	ls_flag = "N"
end if

select cod_tipo_ord_ven,   
		 data_registrazione,   
		 cod_cliente,   
		 cod_valuta,
		 cambio_ven,
		 cod_pagamento,   
		 sconto,   
		 cod_banca_clien_for,   
		 num_ord_cliente,   
		 data_ord_cliente,   
		 cod_porto,   
		 cod_resa,   
		 nota_testata,   
		 nota_piede,   
		 tot_val_ordine_valuta,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia,
		 cod_operatore,
		 data_pronto,
		 data_consegna,
		 num_colli,
		 peso_lordo,
		 cod_giro_consegna,
		 cod_vettore,
		 flag_st_note_tes,
		 flag_st_note_pie,
		 flag_tassativo,
		 tot_val_ordine,
		 alias_cliente,
		 rif_interscambio,
		 num_ord_cliente,
		 cod_deposito,
		 cod_agente_1,
		 cod_agente_2,
		 cod_imballo,
		 logo_etichetta
into   :ls_cod_tipo_ord_ven,   
	 	 :ldt_data_registrazione,   
	 	 :ls_cod_cliente,   
		 :ls_cod_valuta,   
		 :ld_cambio_ven,
		 :ls_cod_pagamento,   
		 :ld_sconto,   
		 :ls_cod_banca_clien_for,   
		 :ls_num_ord_cliente,   
		 :ldt_data_ord_cliente,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_tot_val_ordine,   
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,
		 :ls_cod_operatore,
		 :ldt_data_pronto,
		 :ldt_data_consegna_testata,
		 :ld_num_colli,
		 :ld_peso_lordo,
		 :ls_cod_giro_consegna,
		 :ls_cod_vettore,
		 :ls_flag_st_note_tes,
		 :ls_flag_st_note_pie,
		 :ls_flag_tassativo,
		 :ld_tot_val_ordine_euro,
		 :ls_alias_cliente,
		 :ls_rif_interscambio,
		 :ls_rif_vs_ordine,
		 :ls_cod_deposito,
		 :ls_cod_agente_1,
		 :ls_cod_agente_2,
		 :ls_cod_imballo,
		 :ls_logo_etichetta
from   tes_ord_ven  
where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_ord_ven.anno_registrazione = :il_anno_registrazione and 
		 tes_ord_ven.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_ord_ven)
	setnull(ldt_data_registrazione)
	setnull(ls_cod_cliente)
	setnull(ls_cod_valuta)
	setnull(ls_cod_pagamento)
	setnull(ld_sconto)
	setnull(ls_cod_banca_clien_for)
	setnull(ls_num_ord_cliente)
	setnull(ldt_data_ord_cliente)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ld_tot_val_ordine)
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
	setnull(ls_indirizzo)
	setnull(ls_localita)
	setnull(ls_frazione)
	setnull(ls_cap)
	setnull(ls_provincia)
	setnull(ls_alias_cliente)
	setnull(ls_rif_interscambio)
	setnull(ls_rif_vs_ordine)
	setnull(ls_cod_imballo)
end if


try
	ls_logo_etichetta = wf_imposta_logo_personalizzato(ls_logo_etichetta, ls_cod_cliente)
	dw_report_ord_ven_tende.object.p_logo.filename = ls_logo_etichetta
catch (runtimeerror err_logo)
end try



// stefanop: 19/12/2011: aggiunto deposito
select des_deposito
into :ls_des_deposito
from anag_depositi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_deposito = :ls_cod_deposito;

if isnull(ls_des_deposito) then ls_des_deposito = ""
// ----

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return -1
end if	

select		rag_soc_1,   
			rag_soc_2,   
			indirizzo,   
			localita,   
			frazione,   
			cap,   
			provincia,   
			partita_iva,   
			cod_fiscale,   
			cod_lingua,   
			flag_tipo_cliente,
			fax,
			telefono,
			cod_tipo_anagrafica,
			telex,
			stato
into	:ls_rag_soc_1_cli,   
		:ls_rag_soc_2_cli,   
		:ls_indirizzo_cli,   
		:ls_localita_cli,   
		:ls_frazione_cli,   
		:ls_cap_cli,   
		:ls_provincia_cli,   
		:ls_partita_iva,   
		:ls_cod_fiscale,   
		:ls_cod_lingua,   
		:ls_flag_tipo_cliente,
		:ls_fax,
		:ls_telefono,
		:ls_cod_tipo_anagrafica,
		:ls_telex,
		:ls_stato_cli
from   anag_clienti  
where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
       anag_clienti.cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_cli)
	setnull(ls_rag_soc_2_cli)
	setnull(ls_indirizzo_cli)
	setnull(ls_localita_cli)
	setnull(ls_frazione_cli)
	setnull(ls_cap_cli)
	setnull(ls_provincia_cli)
	setnull(ls_partita_iva)
	setnull(ls_cod_fiscale)
	setnull(ls_cod_lingua)
	setnull(ls_flag_tipo_cliente)
	setnull(ls_telex)
	setnull(ls_stato_cli)
end if

if not isnull(ls_stato_cli) and ls_stato_cli<>"" then
	if not isnull(ls_localita_cli) and ls_localita_cli<>"" then
		ls_localita_cli += " - " + ls_stato_cli
	else
		ls_localita_cli = ls_stato_cli
	end if
end if

if ls_flag_tipo_cliente = 'E' then
	ls_partita_iva = ls_cod_fiscale
end if

select tab_tipi_ord_ven.des_tipo_ord_ven  
into   :ls_des_tipo_ord_ven  
from   tab_tipi_ord_ven  
where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
       tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_ord_ven)
end if

select tab_pagamenti.des_pagamento,   
       tab_pagamenti.sconto  
into   :ls_des_pagamento,   
       :ld_sconto_pagamento  
from   tab_pagamenti  
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
end if

select tab_pagamenti_lingue.des_pagamento  
into   :ls_des_pagamento_lingua  
from   tab_pagamenti_lingue  
where  tab_pagamenti_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_pagamenti_lingue.cod_pagamento = :ls_cod_pagamento and
       tab_pagamenti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

select des_banca, cod_abi, cod_cab
into   :ls_des_banca, :ls_cod_abi, :ls_cod_cab
from   anag_banche_clien_for  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_banca_clien_for = :ls_cod_banca_clien_for;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca)
end if

select tab_porti.des_porto  
into   :ls_des_porto  
from   tab_porti  
where  tab_porti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti.cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select tab_porti_lingue.des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  tab_porti_lingue.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti_lingue.cod_porto = :ls_cod_porto and
       tab_porti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  tab_rese.cod_azienda = :s_cs_xx.cod_azienda and
       tab_rese.cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select tab_rese_lingue.des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  tab_rese_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_rese_lingue.cod_resa = :ls_cod_resa and  
       tab_rese_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

// EnMe 20/04/2012; aggiunto imballo
select 	des_imballo
into   		:ls_des_imballo
from   	tab_imballi
where  	cod_azienda = :s_cs_xx.cod_azienda and
       		cod_imballo = :ls_cod_imballo;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballo)
end if

select des_imballi
into   :ls_des_imballi_lingua  
from   tab_imballi_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_imballo = :ls_cod_imballo and  
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballi_lingua)
else
	ls_des_imballo = ls_des_imballi_lingua
end if
// -------------------------------------

select des_giro_consegna
into   :ls_des_giro_consegna
from   tes_giri_consegne
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_giro_consegna = :ls_cod_giro_consegna;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_giro_consegna)
end if

select des_operatore
into   :ls_des_operatore
from   tab_operatori
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_operatore = :ls_cod_operatore;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_operatore)
end if

select rag_soc_1
into   :ls_anag_vettori_rag_soc_1
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_vettore = :ls_cod_vettore;

if sqlca.sqlcode <> 0 then
	setnull(ls_anag_vettori_rag_soc_1)
end if

select tab_valute.des_valuta
into   :ls_des_valuta
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta = :ls_cod_valuta;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta)
end if

select des_valuta
into   :ls_des_valuta_lingua  
from   tab_valute_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_valuta = :ls_cod_valuta and  
       tab_valute_lingue.cod_lingua = :ls_cod_lingua ;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

select des_tipo_anagrafica
into   :ls_des_tipo_anagrafica
from   tab_tipi_anagrafiche
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_tipo_anagrafica = :ls_cod_tipo_anagrafica ;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_anagrafica)
end if

setnull(ls_nota_testata_st_ord)
//f_crea_nota_fissa (ls_cod_cliente, ls_null, "STAMPA_ORD_PROD", ldt_data_consegna_testata, ls_nota_testata_st_ord,ls_nota_piede_st_ord, ls_nota_video_st_ord )

if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "STAMPA_ORD_PROD", ls_null, ldt_data_consegna_testata, ls_nota_testata_st_ord,ls_nota_piede_st_ord, ls_nota_video_st_ord ) < 0 then
	g_mb.error(ls_nota_testata_st_ord)
else
	if len(ls_nota_video_st_ord) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video_st_ord)
end if


declare cu_dettagli cursor for 
	select   prog_riga_ord_ven,
	         cod_tipo_det_ven, 
				cod_misura, 
				quan_ordine, 
				prezzo_vendita, 
				sconto_1, 
				sconto_2, 
				imponibile_iva_valuta, 
				data_consegna, 
				nota_dettaglio, 
				cod_prodotto, 
				des_prodotto,
				fat_conversione_ven,
				num_riga_appartenenza,
				flag_st_note_det,
				quantita_um,
				prezzo_um,
				nota_prodotto,
				quan_evasa,
				flag_evasione,
				provvigione_1, // stefanop 08/02/2012: beatrice
				dim_x,
				dim_y
	from       det_ord_ven 
	where    cod_azienda = :s_cs_xx.cod_azienda and 
				anno_registrazione = :il_anno_registrazione and 
				num_registrazione = :il_num_registrazione
	order by cod_azienda, 
				anno_registrazione, 
				num_registrazione,
				prog_riga_ord_ven;

open cu_dettagli;

do while 0 = 0
   fetch cu_dettagli into :ll_prog_riga_ord_ven,
	                       :ls_cod_tipo_det_ven, 
								  :ls_cod_misura, 
								  :ld_quan_ordine, 
								  :ld_prezzo_vendita,   
								  :ld_sconto_1, 
								  :ld_sconto_2, 
								  :ld_val_riga, 
								  :ldt_data_consegna, 
								  :ls_nota_dettaglio, 
								  :ls_cod_prodotto, 
								  :ls_des_prodotto,
								  :ld_fat_conversione_ven,
								  :ll_num_riga_appartenenza,
								  :ls_flag_st_note_det,
								  :ld_quantita_um,
								  :ld_prezzo_um,
								  :ls_nota_prodotto,
								  :ld_quan_evasa,
								  :ls_evasione,
								  :ld_provvigione_1,
								  :ld_dim_x_riga,
								  :ld_dim_y_riga;

	// per ultima stampo la nota di testata :: solo per Centro Gibus
	if sqlca.sqlcode = 100 and not isnull(ls_nota_testata) and len(ls_nota_testata) > 0 and lb_centro_gibus then 
		dw_report_ord_ven_tende.insertrow(0)
		dw_report_ord_ven_tende.setrow(dw_report_ord_ven_tende.rowcount())
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_des_prodotto", ls_nota_testata)
	end if

   if sqlca.sqlcode <> 0 then exit

	if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then		// potrebbe essere un prodotto modello
		select flag_tipo_report
		into   :ls_flag_tipo_report
		from   tab_flags_configuratore
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_modello = :ls_cod_prodotto;
		if sqlca.sqlcode = 0 and ls_flag_tipo_report = "A" then							// è un prodotto modello
			ll_riga_appartenenza_padre = ll_prog_riga_ord_ven
			lb_modello = true
			lb_test = true
		else
			lb_test = false
			// non è un modello e ha la riga appartenenza = 0; allora non va nel report tipo A
		end if
	else
		if ll_num_riga_appartenenza = ll_riga_appartenenza_padre then
			lb_modello = false
			lb_test = true
		else
			lb_modello= false
			lb_test = false
		end if
	end if
	
	if not lb_evasi then
		
		if ls_evasione = "E" then
			continue
		end if
		
		ld_quan_ordine -= ld_quan_evasa
		
		ld_quantita_um = ld_quan_ordine * ld_fat_conversione_ven
		
	end if

	if lb_test then																					// verifico se inserire la riga oppure no
		dw_report_ord_ven_tende.insertrow(0)
		dw_report_ord_ven_tende.setrow(dw_report_ord_ven_tende.rowcount())
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "formato", ls_formato)
		
	// inzializzo variabili comp_det_ord_ven
		setnull(ls_cod_prodotto_finito)
		setnull(ls_cod_modello)
		ld_dim_x =0
		ld_dim_y =0
		ld_dim_z =0
		ld_dim_t =0
		setnull(ls_cod_tessuto)
		setnull(ls_cod_non_a_magazzino)
		setnull(ls_cod_tessuto_mantovana)
		setnull(ls_cod_colore_mantovana)
		setnull(ls_flag_tipo_mantovana)
		ld_altezza_mantovana = 0
		setnull(ls_flag_cordolo)
		setnull(ls_flag_passamaneria)
		setnull(ls_cod_verniciatura)
		setnull(ls_cod_comando)
		setnull(ls_pos_comando)
		ld_altezza_asta = 0
		setnull(ls_cod_tipo_supporto)
		setnull(ls_flag_luce_finita)
		setnull(ls_colore_passamaneria)
		setnull(ls_colore_cordolo)
		setnull(ls_des_vernice_fc) 
		setnull(ls_des_vernic_fc_2) 
		setnull(ls_des_vernic_fc_3) 
			
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "parametri_azienda_stringa", ls_stringa)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "parametri_azienda_stringa_euro", ls_stringa_euro)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_anno_registrazione", il_anno_registrazione)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_num_registrazione", il_num_registrazione)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_tipo_ord_ven", ls_cod_tipo_ord_ven)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_data_registrazione", ldt_data_registrazione)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_cliente", ls_cod_cliente)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_valuta", ls_cod_valuta)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cambio_ven", ld_cambio_ven)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_valute_des_valuta", ls_des_valuta)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_valute_lingue_des_valuta", ls_des_valuta_lingua)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_pagamento", ls_cod_pagamento)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_sconto", ld_sconto)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_banca_clien_for", ls_cod_banca_clien_for)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_num_ord_cliente", ls_num_ord_cliente)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_data_ord_cliente", ldt_data_ord_cliente)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_rag_soc_1", ls_rag_soc_1)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_rag_soc_2", ls_rag_soc_2)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_indirizzo", ls_indirizzo)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_localita", ls_localita)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_frazione", ls_frazione)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cap", ls_cap)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_provincia", ls_provincia)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "flag_tassativo", ls_flag_tassativo)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "alias_cliente", ls_alias_cliente)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_des_tipo_anagrafica", ls_des_tipo_anagrafica)
		
		//qualcuno in passato ha usato questo campo sulla dw per metterci la nota fissa produzione
		//allora ho inserito sulla dw anche il campo rif_interscambio_2 per metterci riferimento interscambio vero e proprio
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "rif_interscambio",ls_nota_testata_st_ord)
		
		try
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "rif_interscambio_2",ls_rif_interscambio)
		catch (runtimeerror err)
		end try
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "rif_vs_ordine", ls_rif_vs_ordine)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_imballi_des_imballo",ls_des_imballo)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_provvigione_1", ld_provvigione_1)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_agente_1", ls_cod_agente_1)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_agente_2", ls_cod_agente_2)

		
//-------------------------------------- Modifica Nicola -------------------------------------------------------
		if ls_flag_st_note_tes = "I" then   //nota di testata
			select flag_st_note
			  into :ls_flag_st_note_tes
			  from tab_tipi_ord_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		end if		

		if ls_flag_st_note_tes = "N" then
			ls_nota_testata = ""
		end if			
	
		
		if ls_flag_st_note_pie = "I" then //nota di piede
			select flag_st_note
			  into :ls_flag_st_note_pie
			  from tab_tipi_ord_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		end if			
		
		if ls_flag_st_note_pie = "N" then
			ls_nota_piede = ""
		end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------		
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_nota_testata", ls_nota_testata)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_nota_piede", ls_nota_piede)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_porto", ls_cod_porto)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_resa", ls_cod_resa)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_des_operatore", ls_des_operatore)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_vettori_rag_soc_1", ls_anag_vettori_rag_soc_1)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_data_pronto", ldt_data_pronto)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_data_consegna", ldt_data_consegna_testata)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_des_giro", ls_des_giro_consegna)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_num_colli", ld_num_colli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_peso", ld_peso_lordo)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_tot_val_ordine", ld_tot_val_ordine)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_indirizzo", ls_indirizzo_cli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_localita", ls_localita_cli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_frazione", ls_frazione_cli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_cap", ls_cap_cli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_provincia", ls_provincia_cli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_partita_iva", ls_partita_iva)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_fax", ls_fax)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_telefono", ls_telefono)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_telex", ls_telex)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_des_deposito", ls_des_deposito)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_tipi_ord_ven_des_tipo_ord_ven", ls_des_tipo_ord_ven)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_pagamenti_sconto", ld_sconto_pagamento)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_banche_clien_for_des_banca", ls_des_banca)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_banche_clien_for_cod_abi", ls_cod_abi)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_banche_clien_for_cod_cab", ls_cod_cab)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_des_tipo_anagrafica", ls_des_tipo_anagrafica)

	
		select tab_tipi_det_ven.flag_stampa_ordine  
		into   :ls_flag_stampa_ordine  
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
		if sqlca.sqlcode <> 0 then
			setnull(ls_flag_stampa_ordine)
		end if
	
		if ls_flag_stampa_ordine = "S" then
			select des_prodotto, cod_misura_mag
			into   :ls_des_prodotto_anag, :ls_cod_misura_mag
			from   anag_prodotti  
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_anag)
			end if
	

			// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua
			
			if ls_flag = "S" then
				setnull(ls_des_prodotto_lingua)
			else	
		
				select anag_prodotti_lingue.des_prodotto  
				into   :ls_des_prodotto_lingua  
				from   anag_prodotti_lingue  
				where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
						 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
				
				if sqlca.sqlcode <> 0 then
					setnull(ls_des_prodotto_lingua)
				else
					setnull(ls_des_prodotto)
					setnull(ls_des_prodotto_anag)
				end if
				
			end if
	
			select tab_prod_clienti.cod_prod_cliente  
			into   :ls_cod_prod_cliente  
			from   tab_prod_clienti  
			where  tab_prod_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_prod_clienti.cod_prodotto = :ls_cod_prodotto and   
					 tab_prod_clienti.cod_cliente = :ls_cod_cliente;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_cod_prod_cliente)
			end if
	
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_cod_misura", ls_cod_misura)
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_quan_ordine", ld_quan_ordine)
			if ls_cod_misura_mag <> ls_cod_misura then
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_quan_totale", ld_quantita_um)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_prezzo_vendita", ld_prezzo_um)
			else
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_quan_totale", ld_quan_ordine)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_prezzo_vendita", ld_prezzo_vendita)
			end if				
			ls_sconti = ""
			if ld_sconto_1 > 0 then
				if dec(int(ld_sconto_1)) <> ld_sconto_1 then
					ls_sconti = string(ld_sconto_1,"#0,0#")
				else
					ls_sconti = string(ld_sconto_1,"#0")
				end if
			end if				
			if ld_sconto_2 > 0 then
				if dec(int(ld_sconto_2)) <> ld_sconto_2 then
					ls_sconti = ls_sconti + " + " + string(ld_sconto_2,"#0,0#")
				else
					ls_sconti = ls_sconti + " + " + string(ld_sconto_2,"#0")
				end if
			end if				
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_sconti", ls_sconti)
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_val_riga", ld_val_riga)
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_data_consegna", ldt_data_consegna)
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_cod_prodotto", ls_cod_prodotto)
			if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_num_riga", ll_prog_riga_ord_ven)
			else			
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_num_riga", ll_num_riga_appartenenza)
			end if
			if not isnull(ls_des_prodotto_lingua) then
				ls_descrizione_riga = ls_des_prodotto_lingua
			elseif not isnull(ls_des_prodotto) then
				ls_descrizione_riga = ls_des_prodotto
			else
				ls_descrizione_riga = ls_des_prodotto_anag
			end if
	
	
			// ------------  leggo e stampo comp_det_ord_ven ----------------------------------------------------------
			SELECT cod_prod_finito,        cod_modello,    dim_x,    dim_y,    dim_z,    dim_t,    cod_tessuto,    cod_non_a_magazzino,    cod_tessuto_mant,         cod_mant_non_a_magazzino, flag_tipo_mantovana,    alt_mantovana,        flag_cordolo,    flag_passamaneria,    cod_verniciatura,    cod_comando,    pos_comando,    alt_asta,        cod_tipo_supporto,    flag_misura_luce_finita, note,    colore_passamaneria,    colore_cordolo,    des_vernice_fc,    des_comando_1,     cod_comando_2,     des_comando_2 ,   flag_allegati, cod_verniciatura_2, flag_fc_vernic_2, des_vernic_fc_2, cod_verniciatura_3, flag_fc_vernic_3, des_vernic_fc_3
			 INTO :ls_cod_prodotto_finito,:ls_cod_modello,:ld_dim_x,:ld_dim_y,:ld_dim_z,:ld_dim_t,:ls_cod_tessuto,:ls_cod_non_a_magazzino,:ls_cod_tessuto_mantovana,:ls_cod_colore_mantovana, :ls_flag_tipo_mantovana,:ld_altezza_mantovana,:ls_flag_cordolo,:ls_flag_passamaneria,:ls_cod_verniciatura,:ls_cod_comando,:ls_pos_comando,:ld_altezza_asta,:ls_cod_tipo_supporto,:ls_flag_luce_finita,    :ls_note,:ls_colore_passamaneria,:ls_colore_cordolo,:ls_des_vernice_fc, :ls_des_comando_1, :ls_cod_comando_2, :ls_des_comando_2,:ls_flag_allegati, :ls_cod_verniciatura_2, :ls_flag_fc_vernic_2, :ls_des_vernic_fc_2, :ls_cod_verniciatura_3, :ls_flag_fc_vernic_3, :ls_des_vernic_fc_3
			 FROM comp_det_ord_ven  
			WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
					anno_registrazione = :il_anno_registrazione AND  
					num_registrazione = :il_num_registrazione AND  
					prog_riga_ord_ven = :ll_prog_riga_ord_ven ;
			if sqlca.sqlcode = 0 then
				if not isnull(ls_cod_prodotto_finito) and len(ls_cod_prodotto_finito) > 0 then
					if isnull(ls_descrizione_riga) then ls_descrizione_riga = ""
					ls_descrizione_riga = ls_cod_prodotto_finito + "~r~n" + ls_descrizione_riga
				end if
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_x", ld_dim_x)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_y", ld_dim_y)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_z", ld_dim_z)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_t", ld_dim_t)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_tessuto", ls_cod_tessuto)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_colore_tessuto", ls_cod_non_a_magazzino)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_tessuto_mant", ls_cod_colore_mantovana)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "flag_tipo_mantovana", ls_flag_tipo_mantovana)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "altezza_mantovana", ld_altezza_mantovana)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "colore_cordolo", ls_colore_cordolo)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "colore_passamaneria", ls_colore_passamaneria)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_comando", ls_cod_comando)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "pos_comando", ls_pos_comando)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "altezza_asta", ld_altezza_asta)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "flag_luce_finita", ls_flag_luce_finita)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "flag_allegati", ls_flag_allegati)
				
				// stefanop 08/02/2012: nuovi campi per beatrice
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_provvigione_1", ld_provvigione_1)
				// ----
				
				
				if not isnull(ls_cod_tipo_supporto) and ls_cod_tipo_supporto <> "" then
					select cod_comodo
					into   :ls_abbreviazione_supporto
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_tipo_supporto;
					if sqlca.sqlcode = 0 then
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_tipo_supporto", ls_abbreviazione_supporto)
					else
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_tipo_supporto", "")
					end if
				end if
				if not isnull(ls_cod_verniciatura) and ls_cod_verniciatura <> "" then
					select cod_comodo
					into   :ls_abbreviazione_verniciatura
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_verniciatura;
					if sqlca.sqlcode = 0 then
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_verniciatura", ls_abbreviazione_verniciatura)
					else
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_verniciatura", "")
					end if
				end if

				// EnMe 20-04-2012 verniciatura 2 e 3
				if not isnull(ls_cod_verniciatura_2) and ls_cod_verniciatura_2 <> "" then
					select cod_comodo
					into   :ls_abbreviazione_verniciatura
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_verniciatura_2;
					if sqlca.sqlcode = 0 then
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_verniciatura_2", ls_abbreviazione_verniciatura)
					else
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_verniciatura_2", "")
					end if
				end if
				if not isnull(ls_cod_verniciatura_3) and ls_cod_verniciatura_3 <> "" then
					select cod_comodo
					into   :ls_abbreviazione_verniciatura
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_verniciatura_3;
					if sqlca.sqlcode = 0 then
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_verniciatura_3", ls_abbreviazione_verniciatura)
					else
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_verniciatura_3", "")
					end if
				end if
				// ------------------------------------------
				select abbreviazione_comando
				into   :ls_abbreviazione_comando
				from   tab_comandi
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_comando = :ls_cod_comando;
				if sqlca.sqlcode <> 0 then
					setnull(ls_abbreviazione_comando)
				end if
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "abbreviazione_comando", ls_abbreviazione_comando)
				
				if not isnull(ls_cod_tessuto) and ls_cod_tessuto <> "" then
					select des_prodotto
					into   :ls_des_tessuto
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_tessuto;
					if sqlca.sqlcode = 0 and not isnull(ls_des_tessuto) then
						if isnull(ls_descrizione_riga) then ls_descrizione_riga = ""
						ls_descrizione_riga = ls_descrizione_riga + ", " + ls_des_tessuto
					end if
				end if
				
				
				//-----------------------------------------------------------------------------------
				//sia che VGTOT>0 o nullo accodo la scritta relativa al VGTOT
				if g_str.isnotempty(ls_cod_non_a_magazzino) then
				
					select val_gtot
					into :ld_val_gtot
					from tab_colori_tessuti
					where 	cod_azienda=:s_cs_xx.cod_azienda and
								cod_colore_tessuto=:ls_cod_non_a_magazzino;
					
					if isnull(ld_val_gtot) then
						ls_descrizione_riga += " VAL GTOT=<null>"
					else
						ls_descrizione_riga += " VAL GTOT="+string(ld_val_gtot, "###0.00##")
					end if
				end if
				//-----------------------------------------------------------------------------------
				
				
				if not isnull(ls_des_vernice_fc) and len(ls_des_vernice_fc) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_vernice_fc
				end if
				if not isnull(ls_des_comando_1) and len(ls_des_comando_1) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_comando_1
				end if
				if not isnull(ls_des_comando_2) and len(ls_des_comando_2) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_comando_2
				end if
				if not isnull(ls_des_vernic_fc_2) and len(ls_des_vernic_fc_2) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_vernic_fc_2
				end if
				if not isnull(ls_des_vernic_fc_3) and len(ls_des_vernic_fc_3) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_vernic_fc_3
				end if
			else
				if ld_dim_x_riga > 0 and not  isnull(ld_dim_x_riga) then dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_x", ld_dim_x_riga)
				if ld_dim_y_riga > 0 and not  isnull(ld_dim_y_riga) then dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_y", ld_dim_y_riga)
				
				if not isnull(ls_cod_prodotto) then
					ls_descrizione_riga = ls_cod_prodotto + "~r~n" + ls_des_prodotto
				else
					ls_descrizione_riga = ls_des_prodotto
				end if
			end if
			
//------------------------------------------------- Modifica Nicola ---------------------------------------------
			if ls_flag_st_note_det = "I" then //nota dettaglio
				select flag_st_note_or
				  into :ls_flag_st_note_det
				  from tab_tipi_det_ven
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			end if		
			
			
			if ls_flag_st_note_det = "N" then
				setnull(ls_nota_dettaglio)
			end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------					
			
			if not isnull(ls_nota_dettaglio) and len(ls_nota_dettaglio) > 0 then
				ls_descrizione_riga = ls_descrizione_riga + "~r~n" + ls_nota_dettaglio
			end if
			
			if not isnull(ls_nota_prodotto) then
				ls_descrizione_riga += "~r~n" + ls_nota_prodotto
			end if
			
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_des_prodotto", ls_descrizione_riga)
		end if
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_porti_des_porto", ls_des_porto)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_rese_des_resa", ls_des_resa)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_tot_val_ordine", ld_tot_val_ordine)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_tot_val_ordine_euro", ld_tot_val_ordine_euro)
		
	end if

loop

close cu_dettagli;

dw_report_ord_ven_tende.resetupdate()

return 0
end function

public function integer wf_report_tecniche ();boolean 	lb_modello, lb_test, lb_evasi

string 	ls_stringa, ls_cod_tipo_ord_ven, ls_cod_banca_clien_for, ls_cod_cliente, ls_des_valuta, ls_des_valuta_lingua, &
		 	ls_cod_valuta, ls_cod_pagamento, ls_num_ord_cliente, ls_cod_porto, ls_cod_resa, ls_cod_vettore, &
		 	ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_anag_vettori_rag_soc_1, &
		 	ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
		 	ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, ls_des_operatore, &
		 	ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli, &
		 	ls_partita_iva, ls_cod_fiscale, ls_cod_lingua, ls_flag_tipo_cliente, ls_des_giro_consegna, &
		 	ls_des_tipo_ord_ven, ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, ls_sconti,&
  		 	ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua , ls_cod_giro_consegna, &
		 	ls_des_prodotto_anag, ls_flag_stampa_ordine, ls_cod_tipo_det_ven, ls_cod_abi, ls_cod_cab, &
		 	ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_stringa_euro, ls_dicitura_std,ls_cod_operatore, &
		 	ls_cod_prodotto_finito, ls_cod_modello, ls_cod_tessuto, ls_cod_non_a_magazzino, ls_cod_tessuto_mantovana, &
		 	ls_cod_colore_mantovana, ls_flag_tipo_mantovana,ls_flag_cordolo,ls_flag_passamaneria,ls_cod_verniciatura, &
		 	ls_cod_comando,ls_pos_comando,ls_cod_tipo_supporto,ls_flag_luce_finita,ls_note,ls_colore_passamaneria,&
		 	ls_colore_cordolo,ls_des_vernice_fc, ls_abbreviazione_comando, ls_descrizione_riga, ls_dimensioni, &
		 	ls_des_tessuto, ls_des_verniciatura, ls_abbreviazione_verniciatura, ls_abbreviazione_supporto, &
		 	ls_flag_tipo_report, ls_des_comando_1, ls_cod_comando_2, ls_des_comando_2, ls_flag_tassativo, &
		 	ls_flag_st_note_tes, ls_flag_st_note_pie, ls_flag_st_note_det, ls_flag_allegati, ls_cod_misura_mag, &
			ls_formato, ls_fax, ls_telefono, ls_alias_cliente, ls_cod_tipo_anagrafica, ls_des_tipo_anagrafica, &
			ls_rif_interscambio, ls_rif_vs_ordine, ls_nota_prodotto, ls_evasione, ls_flag, ls_telex, ls_cod_deposito, ls_des_deposito, &
			ls_cod_agente_1, ls_cod_agente_2, ls_des_imballo, ls_cod_imballo, ls_des_imballi_lingua, ls_stato_cli, ls_logo_etichetta

long 		ll_errore, ll_prog_riga_ord_ven, ll_num_riga_appartenenza, ll_riga_appartenenza_padre

dec{4}   ld_tot_val_ordine, ld_sconto, ld_quan_ordine, ld_prezzo_vendita, ld_sconto_1, &
	      	ld_sconto_2, ld_val_riga, ld_sconto_pagamento, ld_cambio_ven, ld_num_colli, ld_peso_lordo, &
		 	ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t,ld_altezza_mantovana,ld_altezza_asta, ld_quantita_um, ld_prezzo_um, &
       		ld_tot_val_ordine_euro, ld_quan_evasa,ld_provvigione_1,ld_dim_x_riga,ld_dim_y_riga, ld_val_gtot
			 
dec{5}   ld_fat_conversione_ven

datetime ldt_data_ord_cliente, ldt_data_registrazione, ldt_data_consegna,ldt_data_pronto,ldt_data_consegna_testata

wf_imposta_barcode()

if ddlb_evasione.text = "Stampa tutti i dettagli" then
	lb_evasi = true
else
	lb_evasi = false
end if

dw_report_ord_ven_tecniche.reset()

select stringa  
into  :ls_stringa  
from  parametri_azienda  
where cod_azienda = :s_cs_xx.cod_azienda and  
      cod_parametro = 'CVL';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

select stringa  
into   :ls_stringa_euro
from   parametri_azienda  
where  cod_azienda = :s_cs_xx.cod_azienda and  
       cod_parametro = 'EUR';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa_euro)
end if

// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua

select flag
into	 :ls_flag
from	 parametri_azienda
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'DTL';
		 
if sqlca.sqlcode = 0 and not isnull(ls_flag) and ls_flag = "S" then
	ls_flag = "S" 
else
	ls_flag = "N"
end if

select cod_tipo_ord_ven,   
		 data_registrazione,   
		 cod_cliente,   
		 cod_valuta,
		 cambio_ven,
		 cod_pagamento,   
		 sconto,   
		 cod_banca_clien_for,   
		 num_ord_cliente,   
		 data_ord_cliente,   
		 cod_porto,   
		 cod_resa,   
		 nota_testata,   
		 nota_piede,   
		 tot_val_ordine_valuta,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia,
		 cod_operatore,
		 data_pronto,
		 data_consegna,
		 num_colli,
		 peso_lordo,
		 cod_giro_consegna,
		 cod_vettore,
		 flag_st_note_tes,
		 flag_st_note_pie,
		 flag_tassativo,
		 tot_val_ordine,
		 alias_cliente,
		 rif_interscambio,
		 num_ord_cliente,
		 cod_deposito,
		 cod_agente_1,
		 cod_agente_2,
		 cod_imballo,
		 logo_etichetta
into   :ls_cod_tipo_ord_ven,   
	 	 :ldt_data_registrazione,   
	 	 :ls_cod_cliente,   
		 :ls_cod_valuta,   
		 :ld_cambio_ven,
		 :ls_cod_pagamento,   
		 :ld_sconto,   
		 :ls_cod_banca_clien_for,   
		 :ls_num_ord_cliente,   
		 :ldt_data_ord_cliente,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_tot_val_ordine,   
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,
		 :ls_cod_operatore,
		 :ldt_data_pronto,
		 :ldt_data_consegna_testata,
		 :ld_num_colli,
		 :ld_peso_lordo,
		 :ls_cod_giro_consegna,
		 :ls_cod_vettore,
		 :ls_flag_st_note_tes,
		 :ls_flag_st_note_pie,
		 :ls_flag_tassativo,
		 :ld_tot_val_ordine_euro,
		 :ls_alias_cliente,
		 :ls_rif_interscambio,
		 :ls_rif_vs_ordine,
		 :ls_cod_deposito,
		 :ls_cod_agente_1,
		 :ls_cod_agente_2,
		 :ls_cod_imballo,
		 :ls_logo_etichetta
from   tes_ord_ven  
where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_ord_ven.anno_registrazione = :il_anno_registrazione and 
		 tes_ord_ven.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_ord_ven)
	setnull(ldt_data_registrazione)
	setnull(ls_cod_cliente)
	setnull(ls_cod_valuta)
	setnull(ls_cod_pagamento)
	setnull(ld_sconto)
	setnull(ls_cod_banca_clien_for)
	setnull(ls_num_ord_cliente)
	setnull(ldt_data_ord_cliente)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ld_tot_val_ordine)
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
	setnull(ls_indirizzo)
	setnull(ls_localita)
	setnull(ls_frazione)
	setnull(ls_cap)
	setnull(ls_provincia)
	setnull(ls_alias_cliente)
	setnull(ls_rif_interscambio)
	setnull(ls_rif_vs_ordine)
end if


try
	ls_logo_etichetta = wf_imposta_logo_personalizzato(ls_logo_etichetta, ls_cod_cliente)
	dw_report_ord_ven_tecniche.object.p_logo.filename = ls_logo_etichetta
catch (runtimeerror err_logo)
end try


// stefanop: 19/12/2011: aggiunto deposito
select des_deposito
into :ls_des_deposito
from anag_depositi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_deposito = :ls_cod_deposito;

if isnull(ls_des_deposito) then ls_des_deposito = ""
// ----

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return -1
end if	

select		rag_soc_1,   
			rag_soc_2,   
			indirizzo,   
			localita,   
			frazione,   
			cap,   
			provincia,   
			partita_iva,   
			cod_fiscale,   
			cod_lingua,   
			flag_tipo_cliente,
			fax,
			telefono,
			cod_tipo_anagrafica,
			telex,
			stato
into	:ls_rag_soc_1_cli,   
		:ls_rag_soc_2_cli,   
		:ls_indirizzo_cli,   
		:ls_localita_cli,   
		:ls_frazione_cli,   
		:ls_cap_cli,   
		:ls_provincia_cli,   
		:ls_partita_iva,   
		:ls_cod_fiscale,   
		:ls_cod_lingua,   
		:ls_flag_tipo_cliente,
		:ls_fax,
		:ls_telefono,
		:ls_cod_tipo_anagrafica,
		:ls_telex,
		:ls_stato_cli
from   anag_clienti  
where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
       anag_clienti.cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_cli)
	setnull(ls_rag_soc_2_cli)
	setnull(ls_indirizzo_cli)
	setnull(ls_localita_cli)
	setnull(ls_frazione_cli)
	setnull(ls_cap_cli)
	setnull(ls_provincia_cli)
	setnull(ls_partita_iva)
	setnull(ls_cod_fiscale)
	setnull(ls_cod_lingua)
	setnull(ls_flag_tipo_cliente)
	setnull(ls_cod_tipo_anagrafica)
	setnull(ls_telex)
	setnull(ls_stato_cli)
end if

if not isnull(ls_stato_cli) and ls_stato_cli<>"" then
	if not isnull(ls_localita_cli) and ls_localita_cli<>"" then
		ls_localita_cli += " - " + ls_stato_cli
	else
		ls_localita_cli = ls_stato_cli
	end if
end if

if ls_flag_tipo_cliente = 'E' then
	ls_partita_iva = ls_cod_fiscale
end if

select tab_tipi_ord_ven.des_tipo_ord_ven  
into   :ls_des_tipo_ord_ven  
from   tab_tipi_ord_ven  
where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
       tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_ord_ven)
end if

select tab_pagamenti.des_pagamento,   
       tab_pagamenti.sconto  
into   :ls_des_pagamento,   
       :ld_sconto_pagamento  
from   tab_pagamenti  
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
end if

select tab_pagamenti_lingue.des_pagamento  
into   :ls_des_pagamento_lingua  
from   tab_pagamenti_lingue  
where  tab_pagamenti_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_pagamenti_lingue.cod_pagamento = :ls_cod_pagamento and
       tab_pagamenti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

select des_banca, cod_abi, cod_cab
into   :ls_des_banca, :ls_cod_abi, :ls_cod_cab
from   anag_banche_clien_for  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_banca_clien_for = :ls_cod_banca_clien_for;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca)
end if

select tab_porti.des_porto  
into   :ls_des_porto  
from   tab_porti  
where  tab_porti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti.cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select tab_porti_lingue.des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  tab_porti_lingue.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti_lingue.cod_porto = :ls_cod_porto and
       tab_porti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  tab_rese.cod_azienda = :s_cs_xx.cod_azienda and
       tab_rese.cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select tab_rese_lingue.des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  tab_rese_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_rese_lingue.cod_resa = :ls_cod_resa and  
       tab_rese_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

// EnMe 20/04/2012; aggiunto imballo
select 	des_imballo
into   		:ls_des_imballo
from   	tab_imballi
where  	cod_azienda = :s_cs_xx.cod_azienda and
       		cod_imballo = :ls_cod_resa;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballo)
end if

select des_imballi
into   :ls_des_imballi_lingua  
from   tab_imballi_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_imballo = :ls_cod_imballo and  
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballi_lingua)
else
	ls_des_imballo = ls_des_imballi_lingua
end if
// -------------------------------------


select des_giro_consegna
into   :ls_des_giro_consegna
from   tes_giri_consegne
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_giro_consegna = :ls_cod_giro_consegna;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_giro_consegna)
end if

select des_operatore
into   :ls_des_operatore
from   tab_operatori
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_operatore = :ls_cod_operatore;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_operatore)
end if

select rag_soc_1
into   :ls_anag_vettori_rag_soc_1
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_vettore = :ls_cod_vettore;

if sqlca.sqlcode <> 0 then
	setnull(ls_anag_vettori_rag_soc_1)
end if

select tab_valute.des_valuta
into   :ls_des_valuta
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta = :ls_cod_valuta;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta)
end if

select des_valuta
into   :ls_des_valuta_lingua  
from   tab_valute_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_valuta = :ls_cod_valuta and  
       tab_valute_lingue.cod_lingua = :ls_cod_lingua ;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta_lingua)
end if
 
select des_tipo_anagrafica
into   :ls_des_tipo_anagrafica
from   tab_tipi_anagrafiche 
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_tipo_anagrafica = :ls_cod_tipo_anagrafica ;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_anagrafica)
end if
 
declare cu_dettagli cursor for 
	select   prog_riga_ord_ven,
	         cod_tipo_det_ven, 
				cod_misura, 
				quan_ordine, 
				prezzo_vendita, 
				sconto_1, 
				sconto_2, 
				imponibile_iva_valuta, 
				data_consegna, 
				nota_dettaglio, 
				cod_prodotto, 
				des_prodotto,
				fat_conversione_ven,
				num_riga_appartenenza,
				flag_st_note_det,
				quantita_um,
				prezzo_um,
				nota_prodotto,
				quan_evasa,
				flag_evasione,
				provvigione_1,
				dim_x,
				dim_y
	from     det_ord_ven 
	where    cod_azienda = :s_cs_xx.cod_azienda and 
				anno_registrazione = :il_anno_registrazione and 
				num_registrazione = :il_num_registrazione
	order by cod_azienda, 
				anno_registrazione, 
				num_registrazione,
				prog_riga_ord_ven;

open cu_dettagli;

do while 0 = 0
   fetch cu_dettagli into :ll_prog_riga_ord_ven,
	                       :ls_cod_tipo_det_ven, 
								  :ls_cod_misura, 
								  :ld_quan_ordine, 
								  :ld_prezzo_vendita,   
								  :ld_sconto_1, 
								  :ld_sconto_2, 
								  :ld_val_riga, 
								  :ldt_data_consegna, 
								  :ls_nota_dettaglio, 
								  :ls_cod_prodotto, 
								  :ls_des_prodotto,
								  :ld_fat_conversione_ven,
								  :ll_num_riga_appartenenza,
								  :ls_flag_st_note_det,
								  :ld_quantita_um,
								  :ld_prezzo_um,
								  :ls_nota_prodotto,
								  :ld_quan_evasa,
								  :ls_evasione,
								  :ld_provvigione_1,
								  :ld_dim_x_riga,
								  :ld_dim_y_riga;								  
   if sqlca.sqlcode <> 0 then exit

	if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then		// potrebbe essere un prodotto modello
		select flag_tipo_report
		into   :ls_flag_tipo_report
		from   tab_flags_configuratore
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_modello = :ls_cod_prodotto;
		if sqlca.sqlcode = 0 and ls_flag_tipo_report = "C" then							// è un prodotto modello
			ll_riga_appartenenza_padre = ll_prog_riga_ord_ven
			lb_modello = true
			lb_test = true
		else
			lb_test = false
			// non è un modello e ha la riga appartenenza = 0; allora non va nel report tipo A
		end if
	else
		if ll_num_riga_appartenenza = ll_riga_appartenenza_padre then
			lb_modello = false
			lb_test = true
		else
			lb_modello= false
			lb_test = false
		end if
	end if

	if not lb_evasi then
		
		if ls_evasione = "E" then
			continue
		end if
		
		ld_quan_ordine -= ld_quan_evasa
		
		ld_quantita_um = ld_quan_ordine * ld_fat_conversione_ven
		
	end if

	if lb_test then																					// verifico se inserire la riga oppure no
		dw_report_ord_ven_tecniche.insertrow(0)
		dw_report_ord_ven_tecniche.setrow(dw_report_ord_ven_tecniche.rowcount())
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "formato", ls_formato)
		
	// inzializzo variabili comp_det_ord_ven
		setnull(ls_cod_prodotto_finito)
		setnull(ls_cod_modello)
		ld_dim_x =0
		ld_dim_y =0
		ld_dim_z =0
		ld_dim_t =0
		setnull(ls_cod_tessuto)
		setnull(ls_cod_non_a_magazzino)
		setnull(ls_cod_tessuto_mantovana)
		setnull(ls_cod_colore_mantovana)
		setnull(ls_flag_tipo_mantovana)
		ld_altezza_mantovana = 0
		setnull(ls_flag_cordolo)
		setnull(ls_flag_passamaneria)
		setnull(ls_cod_verniciatura)
		setnull(ls_cod_comando)
		setnull(ls_pos_comando)
		ld_altezza_asta = 0
		setnull(ls_cod_tipo_supporto)
		setnull(ls_flag_luce_finita)
		setnull(ls_colore_passamaneria)
		setnull(ls_colore_cordolo)
		setnull(ls_des_vernice_fc) 
		
	
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "parametri_azienda_stringa", ls_stringa)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "parametri_azienda_stringa_euro", ls_stringa_euro)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_anno_registrazione", il_anno_registrazione)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_num_registrazione", il_num_registrazione)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_tipo_ord_ven", ls_cod_tipo_ord_ven)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_data_registrazione", ldt_data_registrazione)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_cliente", ls_cod_cliente)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_valuta", ls_cod_valuta)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cambio_ven", ld_cambio_ven)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_valute_des_valuta", ls_des_valuta)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_valute_lingue_des_valuta", ls_des_valuta_lingua)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_pagamento", ls_cod_pagamento)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_sconto", ld_sconto)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_banca_clien_for", ls_cod_banca_clien_for)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_num_ord_cliente", ls_num_ord_cliente)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_data_ord_cliente", ldt_data_ord_cliente)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_rag_soc_1", ls_rag_soc_1)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_rag_soc_2", ls_rag_soc_2)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_indirizzo", ls_indirizzo)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_localita", ls_localita)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_frazione", ls_frazione)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cap", ls_cap)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_provincia", ls_provincia)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "flag_tassativo", ls_flag_tassativo)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "alias_cliente", ls_alias_cliente)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_des_tipo_anagrafica", ls_des_tipo_anagrafica)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(),"rif_interscambio",ls_rif_interscambio)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(),"rif_vs_ordine", ls_rif_vs_ordine)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_provvigione_1", ld_provvigione_1)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_agente_1", ls_cod_agente_1)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_agente_2", ls_cod_agente_2)
		
//-------------------------------------- Modifica Nicola -------------------------------------------------------
		if ls_flag_st_note_tes = "I" then   //nota di testata
			select flag_st_note
			  into :ls_flag_st_note_tes
			  from tab_tipi_ord_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		end if		
	
		if ls_flag_st_note_tes = "N" then
			ls_nota_testata = ""
		end if			
	
		
		if ls_flag_st_note_pie = "I" then //nota di piede
			select flag_st_note
			  into :ls_flag_st_note_pie
			  from tab_tipi_ord_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		end if			
		
		if ls_flag_st_note_pie = "N" then
			ls_nota_piede = ""
		end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------
				
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_nota_testata", ls_nota_testata)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_nota_piede", ls_nota_piede)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_porto", ls_cod_porto)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_resa", ls_cod_resa)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_des_operatore", ls_des_operatore)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_vettori_rag_soc_1", ls_anag_vettori_rag_soc_1)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_data_pronto", ldt_data_pronto)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_data_consegna", ldt_data_consegna_testata)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_des_giro", ls_des_giro_consegna)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_num_colli", ld_num_colli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_peso", ld_peso_lordo)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_tot_val_ordine", ld_tot_val_ordine)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_indirizzo", ls_indirizzo_cli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_localita", ls_localita_cli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_frazione", ls_frazione_cli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_cap", ls_cap_cli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_provincia", ls_provincia_cli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_partita_iva", ls_partita_iva)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_fax", ls_fax)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_telefono", ls_telefono)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_telex", ls_telex)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_des_deposito", ls_des_deposito)
		
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_tipi_ord_ven_des_tipo_ord_ven", ls_des_tipo_ord_ven)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_pagamenti_sconto", ld_sconto_pagamento)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_banche_clien_for_des_banca", ls_des_banca)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_banche_clien_for_cod_abi", ls_cod_abi)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_banche_clien_for_cod_cab", ls_cod_cab)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_provvigione_1", ld_provvigione_1)
	
		select tab_tipi_det_ven.flag_stampa_ordine  
		into   :ls_flag_stampa_ordine  
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
		if sqlca.sqlcode <> 0 then
			setnull(ls_flag_stampa_ordine)
		end if
	
		if ls_flag_stampa_ordine = "S" then
			select des_prodotto, cod_misura_mag
			into   :ls_des_prodotto_anag, :ls_cod_misura_mag  
			from   anag_prodotti  
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_anag)
			end if
	
			// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua
			
			if ls_flag = "S" then
				setnull(ls_des_prodotto_lingua)
			else
				
				select anag_prodotti_lingue.des_prodotto  
				into   :ls_des_prodotto_lingua  
				from   anag_prodotti_lingue  
				where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
						 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
				
				if sqlca.sqlcode <> 0 then
					setnull(ls_des_prodotto_lingua)
				else
					setnull(ls_des_prodotto)
					setnull(ls_des_prodotto_anag)
				end if
				
			end if
	
			select tab_prod_clienti.cod_prod_cliente  
			into   :ls_cod_prod_cliente  
			from   tab_prod_clienti  
			where  tab_prod_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_prod_clienti.cod_prodotto = :ls_cod_prodotto and   
					 tab_prod_clienti.cod_cliente = :ls_cod_cliente;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_cod_prod_cliente)
			end if
	
			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_cod_misura", ls_cod_misura)
			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_quan_ordine", ld_quan_ordine)
			if ls_cod_misura_mag <> ls_cod_misura then
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_quan_vendita", ld_quantita_um)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_prezzo_vendita", ld_prezzo_um)
			else
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_quan_vendita", ld_quan_ordine)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_prezzo_vendita", ld_prezzo_vendita)
			end if				
			
			ls_sconti = ""
			if ld_sconto_1 > 0 then
				if dec(int(ld_sconto_1)) <> ld_sconto_1 then
					ls_sconti = string(ld_sconto_1,"#0,0#")
				else
					ls_sconti = string(ld_sconto_1,"#0")
				end if
			end if				
			if ld_sconto_2 > 0 then
				if dec(int(ld_sconto_2)) <> ld_sconto_2 then
					ls_sconti = ls_sconti + " + " + string(ld_sconto_2,"#0,0#")
				else
					ls_sconti = ls_sconti + " + " + string(ld_sconto_2,"#0")
				end if
			end if
			
			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_sconti", ls_sconti)
			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_val_riga", ld_val_riga)
			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_data_consegna", ldt_data_consegna)
			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_cod_prodotto", ls_cod_prodotto)
			
			if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_num_riga", ll_prog_riga_ord_ven)
			else			
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_num_riga", ll_num_riga_appartenenza)
			end if
			if not isnull(ls_des_prodotto_lingua) then
				ls_descrizione_riga = ls_des_prodotto_lingua
			elseif not isnull(ls_des_prodotto) then
				ls_descrizione_riga = ls_des_prodotto
			else
				ls_descrizione_riga = ls_des_prodotto_anag
			end if
	
	
			// ------------  leggo e stampo comp_det_ord_ven ----------------------------------------------------------
			SELECT cod_prod_finito,        cod_modello,    dim_x,    dim_y,    dim_z,    dim_t,    cod_tessuto,    cod_non_a_magazzino,    cod_tessuto_mant,         cod_mant_non_a_magazzino, flag_tipo_mantovana,    alt_mantovana,        flag_cordolo,    flag_passamaneria,    cod_verniciatura,    cod_comando,    pos_comando,    alt_asta,        cod_tipo_supporto,    flag_misura_luce_finita, note,    colore_passamaneria,    colore_cordolo,    des_vernice_fc,    des_comando_1,     cod_comando_2,     des_comando_2,    flag_allegati
			INTO :ls_cod_prodotto_finito,:ls_cod_modello,:ld_dim_x,:ld_dim_y,:ld_dim_z,:ld_dim_t,:ls_cod_tessuto,:ls_cod_non_a_magazzino,:ls_cod_tessuto_mantovana,:ls_cod_colore_mantovana, :ls_flag_tipo_mantovana,:ld_altezza_mantovana,:ls_flag_cordolo,:ls_flag_passamaneria,:ls_cod_verniciatura,:ls_cod_comando,:ls_pos_comando,:ld_altezza_asta,:ls_cod_tipo_supporto,:ls_flag_luce_finita,    :ls_note,:ls_colore_passamaneria,:ls_colore_cordolo,:ls_des_vernice_fc, :ls_des_comando_1, :ls_cod_comando_2, :ls_des_comando_2,:ls_flag_allegati
			FROM comp_det_ord_ven  
			WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
				anno_registrazione = :il_anno_registrazione AND  
				num_registrazione = :il_num_registrazione AND  
				prog_riga_ord_ven = :ll_prog_riga_ord_ven ;
			
			if sqlca.sqlcode = 0 then
				if not isnull(ls_cod_prodotto_finito) and len(ls_cod_prodotto_finito) > 0 then
					if isnull(ls_descrizione_riga) then ls_descrizione_riga = ""
					ls_descrizione_riga = ls_cod_prodotto_finito + "~r~n" + ls_descrizione_riga
				end if
				
				
			
	//			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_prodotto_finito", ls_cod_prodotto_finito)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "dim_x", ld_dim_x)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "dim_y", ld_dim_y)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "dim_z", ld_dim_z)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "dim_t", ld_dim_t)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_tessuto", ls_cod_tessuto)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_colore_tessuto", ls_cod_non_a_magazzino)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_tessuto_mant", ls_cod_colore_mantovana)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "flag_tipo_mantovana", ls_flag_tipo_mantovana)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "altezza_mantovana", ld_altezza_mantovana)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "colore_cordolo", ls_colore_cordolo)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "colore_passamaneria", ls_colore_passamaneria)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_comando", ls_cod_comando)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "pos_comando", ls_pos_comando)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "altezza_asta", ld_altezza_asta)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "flag_luce_finita", ls_flag_luce_finita)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "flag_allegati", ls_flag_allegati)
				if not isnull(ls_cod_comando_2) and len(ls_cod_comando_2) > 0 then
					select abbreviazione_comando
					into   :ls_abbreviazione_comando
					from   tab_comandi
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_comando = :ls_cod_comando_2;
					if sqlca.sqlcode <> 0 then
						setnull(ls_abbreviazione_comando)
					end if
					dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "guide_perlon", ls_abbreviazione_comando)
				else
					dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "guide_perlon", "")
				end if
				if not isnull(ls_cod_tipo_supporto) and ls_cod_tipo_supporto <> "" then
					select cod_comodo
					into   :ls_abbreviazione_supporto
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_tipo_supporto;
					if sqlca.sqlcode = 0 then
						dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_tipo_supporto", ls_abbreviazione_supporto)
					else
						dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_tipo_supporto", "")
					end if
				end if
				if not isnull(ls_cod_verniciatura) and ls_cod_verniciatura <> "" then
					select cod_comodo
					into   :ls_abbreviazione_verniciatura
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_verniciatura;
					if sqlca.sqlcode = 0 then
						dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_verniciatura", ls_abbreviazione_verniciatura)
					else
						dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_verniciatura", "")
					end if
				end if
				
				select abbreviazione_comando
				into   :ls_abbreviazione_comando
				from   tab_comandi
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_comando = :ls_cod_comando;
				if sqlca.sqlcode <> 0 then
					setnull(ls_abbreviazione_comando)
				end if
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "abbreviazione_comando", ls_abbreviazione_comando)
				
				if not isnull(ls_cod_tessuto) and ls_cod_tessuto <> "" then
					select des_prodotto
					into   :ls_des_tessuto
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_tessuto;
					if sqlca.sqlcode = 0 and not isnull(ls_des_tessuto) then
						if isnull(ls_descrizione_riga) then ls_descrizione_riga = ""
						ls_descrizione_riga = ls_descrizione_riga + ", " + ls_des_tessuto
					end if
				end if
				
				//-----------------------------------------------------------------------------------
				//sia che VGTOT>0 o nullo accodo la scritta relativa al VGTOT
				if g_str.isnotempty(ls_cod_non_a_magazzino) then
				
					select val_gtot
					into :ld_val_gtot
					from tab_colori_tessuti
					where 	cod_azienda=:s_cs_xx.cod_azienda and
								cod_colore_tessuto=:ls_cod_non_a_magazzino;
					
					if isnull(ld_val_gtot) then
						ls_descrizione_riga += " VAL GTOT=<null>"
					else
						ls_descrizione_riga += " VAL GTOT="+string(ld_val_gtot, "###0.00##")
					end if
				end if
				//-----------------------------------------------------------------------------------
				
				if not isnull(ls_des_vernice_fc) and len(ls_des_vernice_fc) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_vernice_fc
				end if
				if not isnull(ls_des_comando_1) and len(ls_des_comando_1) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_comando_1
				end if
				if not isnull(ls_des_comando_2) and len(ls_des_comando_2) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_comando_2
				end if
			else
				if ld_dim_x_riga > 0 and not  isnull(ld_dim_x_riga) then dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_x", ld_dim_x_riga)
				if ld_dim_y_riga > 0 and not  isnull(ld_dim_y_riga) then dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_y", ld_dim_y_riga)
				if not isnull(ls_cod_prodotto) then
					ls_descrizione_riga = ls_cod_prodotto + "~r~n" + ls_des_prodotto
				else
					ls_descrizione_riga = ls_des_prodotto
				end if
			end if
			
//------------------------------------------------- Modifica Nicola ---------------------------------------------
			if ls_flag_st_note_det = 'I' then //nota dettaglio
				select flag_st_note_or
				  into :ls_flag_st_note_det
				  from tab_tipi_det_ven
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			end if		
			
			
			if ls_flag_st_note_det = 'N' then
				setnull(ls_nota_dettaglio)
			end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------		
			
			
			if not isnull(ls_nota_dettaglio) and len(ls_nota_dettaglio) > 0 then
				ls_descrizione_riga = ls_descrizione_riga + "~r~n" + ls_nota_dettaglio
			end if
			
			if not isnull(ls_nota_prodotto) then
				ls_descrizione_riga += "~r~n" + ls_nota_prodotto
			end if
			
			// stefanop 23/06/2010: progetto 140 ticket 2010/167: accodo la descrizione tessuto alla descrizione
			// VERNICIATURA
			select des_prodotto
			into :ls_des_verniciatura
			from anag_prodotti
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_verniciatura;
			
			if sqlca.sqlcode = 0 and not isnull(ls_des_verniciatura) and len(ls_des_verniciatura) > 0 then
				ls_descrizione_riga += ", " + ls_des_verniciatura
			end if
			// ----
			
			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_des_prodotto", ls_descrizione_riga)
		end if
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_porti_des_porto", ls_des_porto)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_rese_des_resa", ls_des_resa)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_tot_val_ordine", ld_tot_val_ordine)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_tot_val_ordine_euro", ld_tot_val_ordine_euro)
				
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_provvigione_1", ld_provvigione_1)
		
	end if
//	dw_report_ord_ven_tecniche.object.tes_ord_ven_tot_val_ordine_valuta_t.text = "Totale in " + ls_cod_valuta

loop

close cu_dettagli;

dw_report_ord_ven_tecniche.resetupdate()

return 0
end function

public function integer wf_report_sfuso ();boolean  lb_modello, lb_test, lb_evasi

string 	ls_stringa, ls_cod_tipo_ord_ven, ls_cod_banca_clien_for, ls_cod_cliente, ls_des_valuta, ls_des_valuta_lingua, &
		 	ls_cod_valuta, ls_cod_pagamento, ls_num_ord_cliente, ls_cod_porto, ls_cod_resa, ls_cod_vettore, &
		   ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_anag_vettori_rag_soc_1, &
		   ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
		   ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, ls_des_operatore, &
			ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli, &
			ls_partita_iva, ls_cod_fiscale, ls_cod_lingua, ls_flag_tipo_cliente, ls_des_giro_consegna, &
			ls_des_tipo_ord_ven, ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, ls_sconti,&
  		 	ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua , ls_cod_giro_consegna, &
		   ls_des_prodotto_anag, ls_flag_stampa_ordine, ls_cod_tipo_det_ven, ls_cod_abi, ls_cod_cab, &
			ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_stringa_euro, ls_dicitura_std,ls_cod_operatore, &
			ls_flag_tipo_report, ls_descrizione_riga, ls_cod_misura_mag, ls_flag_st_note_tes, ls_flag_st_note_pie, &
			ls_flag_st_note_det, ls_flag_tassativo, ls_formato, ls_fax, ls_telefono, ls_alias_cliente, &
			ls_rif_interscambio, ls_rif_vs_ordine, ls_nota_prodotto, ls_evasione, ls_flag, &
			ls_cod_tipo_anagrafica, ls_des_tipo_anagrafica, ls_telex, ls_cod_deposito, ls_des_deposito, &
			ls_cod_agente_1, ls_cod_agente_2,ls_des_imballi_lingua, ls_cod_imballo, ls_des_imballo, ls_des_variabile, ls_stato_cli, ls_logo_etichetta

long 		ll_errore, ll_prog_riga_ord_ven, ll_num_riga_appartenenza, ll_riga_appartenenza_padre

dec{4} 	ld_tot_val_ordine, ld_sconto, ld_quan_ordine, ld_prezzo_vendita, ld_sconto_1, &
	    		ld_sconto_2, ld_val_riga, ld_sconto_pagamento, &
		 	ld_cambio_ven, ld_num_colli, ld_peso_lordo, ld_quantita_um, ld_prezzo_um, &
         	ld_tot_val_ordine_euro, ld_quan_evasa, ld_provvigione_1,ld_dim_x_riga,ld_dim_y_riga
			
dec{5}   ld_fat_conversione_ven

datetime ldt_data_ord_cliente, ldt_data_registrazione, ldt_data_consegna,ldt_data_pronto,ldt_data_consegna_testata

wf_imposta_barcode()

if ddlb_evasione.text = "Stampa tutti i dettagli" then
	lb_evasi = true
else
	lb_evasi = false
end if

dw_report_ord_ven_sfuso.reset()

select stringa  
into  :ls_stringa  
from  parametri_azienda  
where cod_azienda = :s_cs_xx.cod_azienda and  
      cod_parametro = 'CVL';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua

select flag
into	 :ls_flag
from	 parametri_azienda
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'DTL';
		 
if sqlca.sqlcode = 0 and not isnull(ls_flag) and ls_flag = "S" then
	ls_flag = "S" 
else
	ls_flag = "N"
end if

select stringa  
into  :ls_stringa_euro
from  parametri_azienda  
where cod_azienda = :s_cs_xx.cod_azienda and  
      cod_parametro = 'EUR';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa_euro)
end if

select cod_tipo_ord_ven,   
		 data_registrazione,   
		 cod_cliente,   
		 cod_valuta,
		 cambio_ven,
		 cod_pagamento,   
		 sconto,   
		 cod_banca_clien_for,   
		 num_ord_cliente,   
		 data_ord_cliente,   
		 cod_porto,   
		 cod_resa,   
		 nota_testata,   
		 nota_piede,   
		 tot_val_ordine_valuta,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia,
		 cod_operatore,
		 data_pronto,
		 data_consegna,
		 num_colli,
		 peso_lordo,
		 cod_giro_consegna,
		 cod_vettore,
		 flag_st_note_tes,
		 flag_st_note_pie,
		 flag_tassativo,
		 tot_val_ordine,
		 alias_cliente,
		 rif_interscambio,
		 num_ord_cliente,
		 cod_deposito,
		 cod_agente_1,
		 cod_agente_2,
		 cod_imballo,
		 logo_etichetta
into   :ls_cod_tipo_ord_ven,   
	 	 :ldt_data_registrazione,   
	 	 :ls_cod_cliente,   
		 :ls_cod_valuta,   
		 :ld_cambio_ven,
		 :ls_cod_pagamento,   
		 :ld_sconto,   
		 :ls_cod_banca_clien_for,   
		 :ls_num_ord_cliente,   
		 :ldt_data_ord_cliente,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_tot_val_ordine,   
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,
		 :ls_cod_operatore,
		 :ldt_data_pronto,
		 :ldt_data_consegna_testata,
		 :ld_num_colli,
		 :ld_peso_lordo,
		 :ls_cod_giro_consegna,
		 :ls_cod_vettore,
		 :ls_flag_st_note_tes,
		 :ls_flag_st_note_pie,
		 :ls_flag_tassativo,
		 :ld_tot_val_ordine_euro,
		 :ls_alias_cliente,
		 :ls_rif_interscambio,
		 :ls_rif_vs_ordine,
		 :ls_cod_deposito,
		 :ls_cod_agente_1,
		 :ls_cod_agente_2,
		 :ls_cod_imballo,
		 :ls_logo_etichetta
from   tes_ord_ven  
where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_ord_ven.anno_registrazione = :il_anno_registrazione and 
		 tes_ord_ven.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_ord_ven)
	setnull(ldt_data_registrazione)
	setnull(ls_cod_cliente)
	setnull(ls_cod_valuta)
	setnull(ls_cod_pagamento)
	setnull(ld_sconto)
	setnull(ls_cod_banca_clien_for)
	setnull(ls_num_ord_cliente)
	setnull(ldt_data_ord_cliente)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ld_tot_val_ordine)
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
	setnull(ls_indirizzo)
	setnull(ls_localita)
	setnull(ls_frazione)
	setnull(ls_cap)
	setnull(ls_provincia)
	setnull(ls_alias_cliente)
	setnull(ls_rif_interscambio)
	setnull(ls_rif_vs_ordine)
end if


try
	ls_logo_etichetta = wf_imposta_logo_personalizzato(ls_logo_etichetta, ls_cod_cliente)
	dw_report_ord_ven_sfuso.object.p_logo.filename = ls_logo_etichetta
catch (runtimeerror err_logo)
end try


// stefanop: 19/12/2011: aggiunto deposito
select des_deposito
into :ls_des_deposito
from anag_depositi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_deposito = :ls_cod_deposito;

if isnull(ls_des_deposito) then ls_des_deposito = ""
// ----

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return -1
end if	

select		rag_soc_1,   
			 rag_soc_2,   
			 indirizzo,   
			 localita,   
			 frazione,   
			 cap,   
			 provincia,   
			 partita_iva,   
			 cod_fiscale,   
			 cod_lingua,   
			 flag_tipo_cliente,
			 fax,
			 telefono,
			 cod_tipo_anagrafica,
			 telex,
			 stato
into
	:ls_rag_soc_1_cli,   
	:ls_rag_soc_2_cli,   
	:ls_indirizzo_cli,   
	:ls_localita_cli,   
	:ls_frazione_cli,   
	:ls_cap_cli,   
	:ls_provincia_cli,   
	:ls_partita_iva,   
	:ls_cod_fiscale,   
	:ls_cod_lingua,   
	:ls_flag_tipo_cliente,
	:ls_fax,
	:ls_telefono,
	:ls_cod_tipo_anagrafica,
	:ls_telex,
	:ls_stato_cli
from   anag_clienti  
where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
       anag_clienti.cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_cli)
	setnull(ls_rag_soc_2_cli)
	setnull(ls_indirizzo_cli)
	setnull(ls_localita_cli)
	setnull(ls_frazione_cli)
	setnull(ls_cap_cli)
	setnull(ls_provincia_cli)
	setnull(ls_partita_iva)
	setnull(ls_cod_fiscale)
	setnull(ls_cod_lingua)
	setnull(ls_flag_tipo_cliente)
	setnull(ls_cod_tipo_anagrafica)
	setnull(ls_telex)
	setnull(ls_stato_cli)
end if

if not isnull(ls_stato_cli) and ls_stato_cli<>"" then
	if not isnull(ls_localita_cli) and ls_localita_cli<>"" then
		ls_localita_cli += " - " + ls_stato_cli
	else
		ls_localita_cli = ls_stato_cli
	end if
end if

if ls_flag_tipo_cliente = 'E' then
	ls_partita_iva = ls_cod_fiscale
end if

select tab_tipi_ord_ven.des_tipo_ord_ven  
into   :ls_des_tipo_ord_ven  
from   tab_tipi_ord_ven  
where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
       tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_ord_ven)
end if

select tab_pagamenti.des_pagamento,   
       tab_pagamenti.sconto  
into   :ls_des_pagamento,   
       :ld_sconto_pagamento  
from   tab_pagamenti  
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
end if

select tab_pagamenti_lingue.des_pagamento  
into   :ls_des_pagamento_lingua  
from   tab_pagamenti_lingue  
where  tab_pagamenti_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_pagamenti_lingue.cod_pagamento = :ls_cod_pagamento and
       tab_pagamenti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

select des_banca, cod_abi, cod_cab
into   :ls_des_banca, :ls_cod_abi, :ls_cod_cab
from   anag_banche_clien_for  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_banca_clien_for = :ls_cod_banca_clien_for;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca)
end if

select tab_porti.des_porto  
into   :ls_des_porto  
from   tab_porti  
where  tab_porti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti.cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select tab_porti_lingue.des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  tab_porti_lingue.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti_lingue.cod_porto = :ls_cod_porto and
       tab_porti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  tab_rese.cod_azienda = :s_cs_xx.cod_azienda and
       tab_rese.cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select tab_rese_lingue.des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  tab_rese_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_rese_lingue.cod_resa = :ls_cod_resa and  
       tab_rese_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

// EnMe 20/04/2012; aggiunto imballo
select 	des_imballo
into   		:ls_des_imballo
from   	tab_imballi
where  	cod_azienda = :s_cs_xx.cod_azienda and
       		cod_imballo = :ls_cod_resa;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballo)
end if

select des_imballi
into   :ls_des_imballi_lingua  
from   tab_imballi_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_imballo = :ls_cod_imballo and  
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballi_lingua)
else
	ls_des_imballo = ls_des_imballi_lingua
end if
// -------------------------------------


select des_giro_consegna
into   :ls_des_giro_consegna
from   tes_giri_consegne
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_giro_consegna = :ls_cod_giro_consegna;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_giro_consegna)
end if

select des_operatore
into   :ls_des_operatore
from   tab_operatori
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_operatore = :ls_cod_operatore;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_operatore)
end if

select rag_soc_1
into   :ls_anag_vettori_rag_soc_1
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_vettore = :ls_cod_vettore;

if sqlca.sqlcode <> 0 then
	setnull(ls_anag_vettori_rag_soc_1)
end if

select tab_valute.des_valuta
into   :ls_des_valuta
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta = :ls_cod_valuta;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta)
end if

select des_valuta
into   :ls_des_valuta_lingua  
from   tab_valute_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_valuta = :ls_cod_valuta and  
       tab_valute_lingue.cod_lingua = :ls_cod_lingua ;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if
 
select des_tipo_anagrafica
into   :ls_des_tipo_anagrafica
from   tab_tipi_anagrafiche 
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_tipo_anagrafica = :ls_cod_tipo_anagrafica ;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_anagrafica)
end if
 
declare cu_dettagli cursor for 
	select   prog_riga_ord_ven,
	         cod_tipo_det_ven, 
				cod_misura, 
				quan_ordine, 
				prezzo_vendita, 
				sconto_1, 
				sconto_2, 
				imponibile_iva_valuta, 
				data_consegna, 
				nota_dettaglio, 
				cod_prodotto, 
				des_prodotto,
				fat_conversione_ven,
				num_riga_appartenenza,
				flag_st_note_det,
				quantita_um,
				prezzo_um,
				nota_prodotto,
				quan_evasa,
				flag_evasione,
				provvigione_1,
				dim_x,
				dim_y
	from     det_ord_ven 
	where    cod_azienda = :s_cs_xx.cod_azienda and 
				anno_registrazione = :il_anno_registrazione and 
				num_registrazione = :il_num_registrazione
	order by cod_azienda, 
				anno_registrazione, 
				num_registrazione,
				prog_riga_ord_ven;

open cu_dettagli;

do while 0 = 0
   fetch cu_dettagli into :ll_prog_riga_ord_ven,
	                       :ls_cod_tipo_det_ven, 
								  :ls_cod_misura, 
								  :ld_quan_ordine, 
								  :ld_prezzo_vendita,   
								  :ld_sconto_1, 
								  :ld_sconto_2, 
								  :ld_val_riga, 
								  :ldt_data_consegna, 
								  :ls_nota_dettaglio, 
								  :ls_cod_prodotto, 
								  :ls_des_prodotto,
								  :ld_fat_conversione_ven,
								  :ll_num_riga_appartenenza,
								  :ls_flag_st_note_det,
								  :ld_quantita_um,
								  :ld_prezzo_um,
								  :ls_nota_prodotto,
								  :ld_quan_evasa,
								  :ls_evasione,
								  :ld_provvigione_1,
								  :ld_dim_x_riga,
								  :ld_dim_y_riga;
								  
   if sqlca.sqlcode <> 0 then exit

	// affinche una riga di ordine compaia nel report sfuso NON deve avere la riga appartenenza 
	// compilata e non deve essere un modello.
	if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then		// potrebbe essere un prodotto modello
		select flag_tipo_report
		into   :ls_flag_tipo_report
		from   tab_flags_configuratore
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_modello = :ls_cod_prodotto;
		if sqlca.sqlcode = 0 then							// è un prodotto modello
			lb_test = false																			// e quindi non stampo
		else
			lb_test = true
			// non è un modello e ha la riga appartenenza = 0; allora non va nel report sfuso
		end if
	else
		lb_test = false
	end if
	
	if not lb_evasi then
		
		if ls_evasione = "E" then
			continue
		end if
		
		ld_quan_ordine -= ld_quan_evasa
		
		ld_quantita_um = ld_quan_ordine * ld_fat_conversione_ven
		
	end if
	
	if lb_test then																					// verifico se inserire la riga oppure no
		dw_report_ord_ven_sfuso.insertrow(0)
		dw_report_ord_ven_sfuso.setrow(dw_report_ord_ven_sfuso.rowcount())
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "formato", ls_formato)
	
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "parametri_azienda_stringa", ls_stringa)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "parametri_azienda_stringa_euro", ls_stringa_euro)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_anno_registrazione", il_anno_registrazione)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_num_registrazione", il_num_registrazione)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_tipo_ord_ven", ls_cod_tipo_ord_ven)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_data_registrazione", ldt_data_registrazione)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_cliente", ls_cod_cliente)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_valuta", ls_cod_valuta)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cambio_ven", ld_cambio_ven)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_valute_des_valuta", ls_des_valuta)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_valute_lingue_des_valuta", ls_des_valuta_lingua)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_pagamento", ls_cod_pagamento)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_sconto", ld_sconto)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_banca_clien_for", ls_cod_banca_clien_for)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_num_ord_cliente", ls_num_ord_cliente)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_data_ord_cliente", ldt_data_ord_cliente)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_rag_soc_1", ls_rag_soc_1)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_rag_soc_2", ls_rag_soc_2)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_indirizzo", ls_indirizzo)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_localita", ls_localita)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_frazione", ls_frazione)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cap", ls_cap)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_provincia", ls_provincia)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "flag_tassativo", ls_flag_tassativo)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "alias_cliente", ls_alias_cliente)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_des_tipo_anagrafica", ls_des_tipo_anagrafica)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(),"rif_interscambio",ls_rif_interscambio)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(),"rif_vs_ordine", ls_rif_vs_ordine)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_agente_1", ls_cod_agente_1)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_agente_2", ls_cod_agente_2)
		
//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_st_note_tes = 'I' then   //nota di testata
		select flag_st_note
		  into :ls_flag_st_note_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_st_note_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_st_note_pie = 'I' then //nota di piede
		select flag_st_note
		  into :ls_flag_st_note_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_st_note_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_nota_testata", ls_nota_testata)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_nota_piede", ls_nota_piede)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_porto", ls_cod_porto)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_resa", ls_cod_resa)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_des_operatore", ls_des_operatore)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_vettori_rag_soc_1", ls_anag_vettori_rag_soc_1)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_data_pronto", ldt_data_pronto)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_data_consegna", ldt_data_consegna_testata)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_des_giro", ls_des_giro_consegna)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_num_colli", ld_num_colli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_peso", ld_peso_lordo)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_tot_val_ordine", ld_tot_val_ordine)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_indirizzo", ls_indirizzo_cli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_localita", ls_localita_cli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_frazione", ls_frazione_cli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_cap", ls_cap_cli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_provincia", ls_provincia_cli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_partita_iva", ls_partita_iva)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_fax", ls_fax)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_telefono", ls_telefono)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_telex", ls_telex)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_des_deposito", ls_des_deposito)
		
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_tipi_ord_ven_des_tipo_ord_ven", ls_des_tipo_ord_ven)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_pagamenti_sconto", ld_sconto_pagamento)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_banche_clien_for_des_banca", ls_des_banca)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_banche_clien_for_cod_abi", ls_cod_abi)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_banche_clien_for_cod_cab", ls_cod_cab)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_provvigione_1", ld_provvigione_1)
	
		select tab_tipi_det_ven.flag_stampa_ordine  
		into   :ls_flag_stampa_ordine  
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
		if sqlca.sqlcode <> 0 then
			setnull(ls_flag_stampa_ordine)
		end if
	
		if ls_flag_stampa_ordine = 'S' then
			select des_prodotto, cod_misura_mag
			into   :ls_des_prodotto_anag, :ls_cod_misura_mag
			from   anag_prodotti  
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_anag)
			end if
			
			// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua
			
			if ls_flag = "S" then
				setnull(ls_des_prodotto_lingua)
			else			
		
				select anag_prodotti_lingue.des_prodotto  
				into   :ls_des_prodotto_lingua  
				from   anag_prodotti_lingue  
				where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
						 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
				
				if sqlca.sqlcode <> 0 then
					setnull(ls_des_prodotto_lingua)
				else
					setnull(ls_des_prodotto)
					setnull(ls_des_prodotto_anag)
				end if
			end if
	
			select tab_prod_clienti.cod_prod_cliente  
			into   :ls_cod_prod_cliente  
			from   tab_prod_clienti  
			where  tab_prod_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_prod_clienti.cod_prodotto = :ls_cod_prodotto and   
					 tab_prod_clienti.cod_cliente = :ls_cod_cliente;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_cod_prod_cliente)
			end if
	
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_cod_misura", ls_cod_misura)
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_prodotti_cod_misura_mag", ls_cod_misura_mag)
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_cod_misura", ls_cod_misura)
			if ls_cod_misura <> ls_cod_misura_mag then
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_quan_ordine", ld_quan_ordine)
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_quantita_um", ld_quantita_um)
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_prezzo_vendita", ld_prezzo_um)
			else
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_quan_ordine", ld_quan_ordine)
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_quantita_um", ld_quan_ordine)
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_prezzo_vendita", ld_prezzo_vendita)
			end if
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_fat_conversione", ld_fat_conversione_ven)

//------------------------------------------------- Modifica Nicola ---------------------------------------------
	if ls_flag_st_note_det = 'I' then //nota dettaglio
		select flag_st_note_or
		  into :ls_flag_st_note_det
		  from tab_tipi_det_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		
	
	
	if ls_flag_st_note_det = 'N' then
		ls_nota_dettaglio = ""
	end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------		
			
			if not isnull(ls_nota_prodotto) then
				ls_nota_dettaglio += "~r~n" + ls_nota_prodotto
			end if
			
			if ld_dim_y_riga > 0 and not isnull(ld_dim_y_riga) and not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then
				select des_variabile
				into :ls_des_variabile
				from tab_des_variabili
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :ls_cod_prodotto and
						cod_variabile = 'DIM_2' and
						flag_ipertech_apice = 'S';
				if sqlca.sqlcode = 0 and len(ls_des_variabile) > 0 and not isnull(ls_des_variabile) then
					ls_nota_dettaglio = ls_des_variabile + "=" + string(ld_dim_y_riga,"###,##0.00") + " - " +  ls_nota_dettaglio
				end if
			end if
			
			if ld_dim_x_riga > 0 and not isnull(ld_dim_x_riga) and not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then
				select des_variabile
				into :ls_des_variabile
				from tab_des_variabili
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :ls_cod_prodotto and
						cod_variabile = 'DIM_1' and
						flag_ipertech_apice = 'S';
				if sqlca.sqlcode = 0 and len(ls_des_variabile) > 0 and not isnull(ls_des_variabile) then
					ls_nota_dettaglio= ls_des_variabile + "=" + string(ld_dim_x_riga,"###,##0.00") + " - " + ls_nota_dettaglio
				end if
			end if
			
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_nota_dettaglio", ls_nota_dettaglio)
	//		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_sconto_1", ld_sconto_1)
	//		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_sconto_2", ld_sconto_2)
			ls_sconti = ""
			if ld_sconto_1 > 0 then
				if dec(int(ld_sconto_1)) <> ld_sconto_1 then
					ls_sconti = string(ld_sconto_1,"#0,0#")
				else
					ls_sconti = string(ld_sconto_1,"#0")
				end if
			end if				
			if ld_sconto_2 > 0 then
				if dec(int(ld_sconto_2)) <> ld_sconto_2 then
					ls_sconti = ls_sconti + " + " + string(ld_sconto_2,"#0,0#")
				else
					ls_sconti = ls_sconti + " + " + string(ld_sconto_2,"#0")
				end if
			end if				
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_sconti", ls_sconti)
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_val_riga", ld_val_riga)
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_data_consegna", ldt_data_consegna)
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_cod_prodotto", ls_cod_prodotto)
			if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_num_riga", ll_prog_riga_ord_ven)
			else			
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_num_riga", ll_num_riga_appartenenza)
			end if
			if not isnull(ls_des_prodotto_lingua) then
				ls_descrizione_riga = ls_des_prodotto_lingua
			elseif not isnull(ls_des_prodotto) then
				ls_descrizione_riga = ls_des_prodotto
			else
				ls_descrizione_riga = ls_des_prodotto_anag
			end if
	

//			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_prodotti_lingue_des_prodotto", ls_des_prodotto_lingua)
			if not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then ls_descrizione_riga =  ls_cod_prodotto + "~r~n" + ls_descrizione_riga
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_des_prodotto", ls_descrizione_riga)
		end if
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_porti_des_porto", ls_des_porto)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_rese_des_resa", ls_des_resa)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_tot_val_ordine", ld_tot_val_ordine)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_tot_val_ordine_euro", ld_tot_val_ordine_euro)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_provvigione_1", ld_provvigione_1)
		
	end if
//	dw_report_ord_ven_sfuso.object.tes_ord_ven_tot_val_ordine_valuta_t.text = "Totale in " + ls_cod_valuta

loop

close cu_dettagli;

dw_report_ord_ven_sfuso.resetupdate()

return 0
end function

public function integer wf_imposta_barcode ();string ls_bfo, ls_errore
int li_risposta, li_bco

li_risposta = f_font_barcode(ls_bfo,li_bco,ls_errore)
                              
if li_risposta < 0 then
	g_mb.error("Controllare parametri barcode." + ls_errore)
	return -1
end if
                              
dw_report_ord_ven_tende.Modify("cf_barcode.Font.Face='" + ls_bfo + "'")
dw_report_ord_ven_tende.Modify("cf_barcode.Font.Height= -" + string(li_bco) )

dw_report_ord_ven_tecniche.Modify("cf_barcode.Font.Face='" + ls_bfo + "'")
dw_report_ord_ven_tecniche.Modify("cf_barcode.Font.Height= -" + string(li_bco) )

dw_report_ord_ven_sfuso.Modify("cf_barcode.Font.Face='" + ls_bfo + "'")
dw_report_ord_ven_sfuso.Modify("cf_barcode.Font.Height= -" + string(li_bco) )
end function

public function string wf_imposta_logo_personalizzato (string as_logo_etichetta, string as_cod_cliente);string			ls_logo, ls_cod_cliente


//se vuoto esci, finito
if g_str.isempty(as_logo_etichetta) or g_str.isempty(as_cod_cliente) then
	return ""
end if

ls_logo = s_cs_xx.volume + s_cs_xx.risorse + "loghi_clienti\"+as_cod_cliente+"\"+as_logo_etichetta

if fileexists(ls_logo) then
	return ls_logo
else
	//file inesistente
	return ""
end if
end function

event pc_setwindow;call super::pc_setwindow;string ls_path_logo_1, ls_modify

dw_report_ord_ven_sfuso.ib_dw_report = true
dw_report_ord_ven_tecniche.ib_dw_report = true
dw_report_ord_ven_tende.ib_dw_report = true

il_anno_registrazione = s_cs_xx.parametri.parametro_d_1
il_num_registrazione  = s_cs_xx.parametri.parametro_d_2
set_w_options(c_noresizewin)

dw_report_ord_ven_sfuso.set_document_name("Ordine di Vendita " + string(il_anno_registrazione) + "/" +  string(il_num_registrazione))
dw_report_ord_ven_tecniche.set_document_name("Ordine di Vendita " + string(il_anno_registrazione) + "/" +  string(il_num_registrazione))
dw_report_ord_ven_tende.set_document_name("Ordine di Vendita " + string(il_anno_registrazione) + "/" +  string(il_num_registrazione))

dw_report_ord_ven_tende.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
											c_disablecc, &
											c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)
												
dw_report_ord_ven_sfuso.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
											c_disablecc, &
											c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)

dw_report_ord_ven_tecniche.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
											c_disablecc, &
											c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)

select parametri_azienda.stringa
into   :ls_path_logo_1
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO5';

ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
dw_report_ord_ven_tende.modify(ls_modify)
dw_report_ord_ven_sfuso.modify(ls_modify)
dw_report_ord_ven_tecniche.modify(ls_modify)

save_on_close(c_socnosave)

end event

on w_report_ord_ven_euro.create
int iCurrent
call super::create
this.ddlb_evasione=create ddlb_evasione
this.st_1=create st_1
this.em_copie=create em_copie
this.cb_tende=create cb_tende
this.cb_sfuso=create cb_sfuso
this.cb_stampa=create cb_stampa
this.cb_tecniche=create cb_tecniche
this.dw_report_ord_ven_tende=create dw_report_ord_ven_tende
this.dw_report_ord_ven_tecniche=create dw_report_ord_ven_tecniche
this.dw_report_ord_ven_sfuso=create dw_report_ord_ven_sfuso
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ddlb_evasione
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.em_copie
this.Control[iCurrent+4]=this.cb_tende
this.Control[iCurrent+5]=this.cb_sfuso
this.Control[iCurrent+6]=this.cb_stampa
this.Control[iCurrent+7]=this.cb_tecniche
this.Control[iCurrent+8]=this.dw_report_ord_ven_tende
this.Control[iCurrent+9]=this.dw_report_ord_ven_tecniche
this.Control[iCurrent+10]=this.dw_report_ord_ven_sfuso
end on

on w_report_ord_ven_euro.destroy
call super::destroy
destroy(this.ddlb_evasione)
destroy(this.st_1)
destroy(this.em_copie)
destroy(this.cb_tende)
destroy(this.cb_sfuso)
destroy(this.cb_stampa)
destroy(this.cb_tecniche)
destroy(this.dw_report_ord_ven_tende)
destroy(this.dw_report_ord_ven_tecniche)
destroy(this.dw_report_ord_ven_sfuso)
end on

type ddlb_evasione from dropdownlistbox within w_report_ord_ven_euro
integer x = 2400
integer y = 20
integer width = 1394
integer height = 360
integer taborder = 30
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "Stampa tutti i dettagli"
string item[] = {"Stampa tutti i dettagli","Stampa solo i dettagli da evadere"}
borderstyle borderstyle = stylelowered!
end type

event selectionchanged;dw_report_ord_ven_sfuso.postevent("pcd_retrieve")
dw_report_ord_ven_tecniche.postevent("pcd_retrieve")
dw_report_ord_ven_tende.postevent("pcd_retrieve")
end event

type st_1 from statictext within w_report_ord_ven_euro
integer x = 23
integer y = 36
integer width = 503
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
boolean enabled = false
string text = "NR.COPIE REPORT:"
boolean focusrectangle = false
end type

type em_copie from editmask within w_report_ord_ven_euro
integer x = 544
integer y = 28
integer width = 238
integer height = 84
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
textcase textcase = upper!
borderstyle borderstyle = stylelowered!
string mask = "##"
boolean spin = true
string displaydata = ","
end type

type cb_tende from commandbutton within w_report_ord_ven_euro
integer x = 827
integer y = 28
integer width = 366
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Tende"
end type

event clicked;dw_report_ord_ven_sfuso.hide()
dw_report_ord_ven_tende.show()
dw_report_ord_ven_tecniche.hide()
end event

type cb_sfuso from commandbutton within w_report_ord_ven_euro
integer x = 1216
integer y = 28
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "S&fuso"
end type

event clicked;dw_report_ord_ven_tende.hide()
dw_report_ord_ven_sfuso.show()
dw_report_ord_ven_tecniche.hide()
end event

type cb_stampa from commandbutton within w_report_ord_ven_euro
integer x = 1989
integer y = 28
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&STAMPA"
end type

event clicked;integer li_copie, ll_i
long ll_job

li_copie = integer(em_copie.text)
for ll_i =1 to li_copie
	
	if dw_report_ord_ven_tende.rowcount() > 0 then
		dw_report_ord_ven_tende.object.datawindow.print.orientation = 1
		dw_report_ord_ven_tende.print()
	end if
	if dw_report_ord_ven_tecniche.rowcount() > 0 then
		dw_report_ord_ven_tecniche.object.datawindow.print.orientation = 1
		dw_report_ord_ven_tecniche.print()
	end if
	if dw_report_ord_ven_sfuso.rowcount() > 0 then
		dw_report_ord_ven_sfuso.object.datawindow.print.orientation = 1
		dw_report_ord_ven_sfuso.print()
	end if
next

end event

type cb_tecniche from commandbutton within w_report_ord_ven_euro
integer x = 1605
integer y = 28
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "T.Tecniche"
end type

event clicked;dw_report_ord_ven_sfuso.hide()
dw_report_ord_ven_tende.hide()
dw_report_ord_ven_tecniche.show()

end event

type dw_report_ord_ven_tende from uo_cs_xx_dw within w_report_ord_ven_euro
integer y = 140
integer width = 4937
integer height = 4500
integer taborder = 70
string dataobject = "d_report_ord_ven_tende_euro"
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report_tende()
end event

event pcd_first;call super::pcd_first;wf_report_tende()
end event

event pcd_last;call super::pcd_last;wf_report_tende()
end event

event pcd_next;call super::pcd_next;wf_report_tende()
end event

event pcd_previous;call super::pcd_previous;wf_report_tende()
end event

event pcd_print;call super::pcd_print;update tes_ord_ven
set flag_stampato = 'S'
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;
		
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Stampa Ordine", "Errore in aggiornamento segnale di ordine stampato.~r~nDettaglio errore " + sqlca.sqlerrtext)
	rollback;
	return
else
	commit;
end if	

end event

type dw_report_ord_ven_tecniche from uo_cs_xx_dw within w_report_ord_ven_euro
integer y = 140
integer width = 4937
integer height = 4500
integer taborder = 51
string dataobject = "d_report_ord_ven_tecniche_euro"
boolean livescroll = true
end type

event pcd_first;call super::pcd_first;wf_report_tecniche()
end event

event pcd_last;call super::pcd_last;wf_report_tecniche()
end event

event pcd_next;call super::pcd_next;wf_report_tecniche()
end event

event pcd_previous;call super::pcd_previous;wf_report_tecniche()
end event

event pcd_print;call super::pcd_print;update tes_ord_ven
set flag_stampato = 'S'
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;
		
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Stampa Ordine", "Errore in aggiornamento segnale di ordine stampato.~r~nDettaglio errore " + sqlca.sqlerrtext)
	rollback;
	return
else
	commit;
end if	

end event

event pcd_retrieve;call super::pcd_retrieve;wf_report_tecniche()
end event

type dw_report_ord_ven_sfuso from uo_cs_xx_dw within w_report_ord_ven_euro
integer y = 140
integer width = 4937
integer height = 4500
integer taborder = 60
string dataobject = "d_report_ord_ven_sfuso_euro"
boolean livescroll = true
end type

event pcd_first;call super::pcd_first;wf_report_sfuso()
end event

event pcd_last;call super::pcd_last;wf_report_sfuso()
end event

event pcd_next;call super::pcd_next;wf_report_sfuso()
end event

event pcd_previous;call super::pcd_previous;wf_report_sfuso()
end event

event pcd_print;call super::pcd_print;update tes_ord_ven
set flag_stampato = 'S'
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;
		
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Stampa Ordine", "Errore in aggiornamento segnale di ordine stampato.~r~nDettaglio errore " + sqlca.sqlerrtext)
	rollback;
	return
else
	commit;
end if	

end event

event pcd_retrieve;call super::pcd_retrieve;wf_report_sfuso()
end event

