﻿$PBExportHeader$w_report_ord_ven_op_euro.srw
$PBExportComments$Finestra Report Ordini Vendita
forward
global type w_report_ord_ven_op_euro from w_cs_xx_principale
end type
type dw_lista from datawindow within w_report_ord_ven_op_euro
end type
type cb_stampa from commandbutton within w_report_ord_ven_op_euro
end type
type dw_folder from u_folder within w_report_ord_ven_op_euro
end type
type dw_elenco_operatori_selezionati from datawindow within w_report_ord_ven_op_euro
end type
type dw_report_ord_ven_sfuso from uo_cs_xx_dw within w_report_ord_ven_op_euro
end type
type dw_report_ord_ven_tecniche from uo_cs_xx_dw within w_report_ord_ven_op_euro
end type
type dw_report_ord_ven_tende from uo_cs_xx_dw within w_report_ord_ven_op_euro
end type
type dw_ricerca from u_dw_search within w_report_ord_ven_op_euro
end type
type cbx_select_all from checkbox within w_report_ord_ven_op_euro
end type
end forward

global type w_report_ord_ven_op_euro from w_cs_xx_principale
integer x = 9
integer y = 8
integer width = 3767
integer height = 2368
string title = "Stampa Ordini Clienti"
boolean minbox = false
boolean maxbox = false
event ue_post_open ( )
dw_lista dw_lista
cb_stampa cb_stampa
dw_folder dw_folder
dw_elenco_operatori_selezionati dw_elenco_operatori_selezionati
dw_report_ord_ven_sfuso dw_report_ord_ven_sfuso
dw_report_ord_ven_tecniche dw_report_ord_ven_tecniche
dw_report_ord_ven_tende dw_report_ord_ven_tende
dw_ricerca dw_ricerca
cbx_select_all cbx_select_all
end type
global w_report_ord_ven_op_euro w_report_ord_ven_op_euro

type variables


string					is_cod_parametro_blocco = "SOC"
uo_fido_cliente		iuo_fido
end variables

forward prototypes
public function integer wf_barcode ()
public function integer wf_estrai_blob_axioma (long al_anno_registrazione, long al_num_registrazione, ref string as_temp_filename[])
public subroutine wf_carica_operatori ()
public function integer wf_ricerca (ref string as_errore)
public function integer wf_report_sfuso (integer ai_anno_ord_ven, long al_num_ord_ven)
public function integer wf_report_tecniche (integer ai_anno_ord_ven, long ai_num_ord_ven)
public function integer wf_report_tende (integer ai_anno_ord_ven, long ai_num_ord_ven)
public function string wf_imposta_logo_personalizzato (string as_logo_etichetta, string as_cod_cliente)
end prototypes

event ue_post_open();
dw_ricerca.setitem(1, "anno_ordine", f_anno_esercizio())
end event

public function integer wf_barcode ();string ls_bfo, ls_errore
int li_risposta, li_bco

li_risposta = f_font_barcode(ls_bfo,li_bco,ls_errore)
                              
if li_risposta < 0 then
	g_mb.error("Controllare parametri barcode." + ls_errore)
	return -1
end if
                              
dw_report_ord_ven_tende.Modify("cf_barcode.Font.Face='" + ls_bfo + "'")
dw_report_ord_ven_tende.Modify("cf_barcode.Font.Height= -" + string(li_bco) )

dw_report_ord_ven_tecniche.Modify("cf_barcode.Font.Face='" + ls_bfo + "'")
dw_report_ord_ven_tecniche.Modify("cf_barcode.Font.Height= -" + string(li_bco) )

dw_report_ord_ven_sfuso.Modify("cf_barcode.Font.Face='" + ls_bfo + "'")
dw_report_ord_ven_sfuso.Modify("cf_barcode.Font.Height= -" + string(li_bco) )
end function

public function integer wf_estrai_blob_axioma (long al_anno_registrazione, long al_num_registrazione, ref string as_temp_filename[]);string ls_temp_dir, ls_rnd,ls_estensione,ls_file_name,ls_cod_nota, ls_sql, ls_null[]
long ll_prog_riga_ord_ven,ll_cont,ll_prog_mimetype, ll_len,ll_rows,ll_i
blob lb_blob
datastore lds_blob
//uo_shellexecute luo_run

as_temp_filename[] = ls_null[]
ls_cod_nota = "AX1"

ls_sql =  " select prog_riga_ord_ven, prog_mimetype from det_ord_ven_note " + &
			" where cod_azienda = '"  + s_cs_xx.cod_azienda + "' and " + &
			" anno_registrazione = " + string(al_anno_registrazione) + " and " + &
			" num_registrazione = " + string(al_num_registrazione) + " and " + &
			" cod_nota = '" + ls_cod_nota  + "'"
lds_blob = create datastore

guo_functions.uof_crea_datastore( ref lds_blob, ls_sql)
lds_blob.settransobject(sqlca)
ll_rows = lds_blob.retrieve()

if ll_rows = 0 then return 0



for ll_i = 1 to ll_rows
	
	as_temp_filename[ll_i] = ""
	
	ll_prog_mimetype = lds_blob.getitemnumber(ll_i, 2)
	if isnull(ll_prog_mimetype) then return -1
	
	ll_prog_riga_ord_ven = lds_blob.getitemnumber(ll_i, 1)

	selectblob note_esterne
	into 		:lb_blob
	from 		det_ord_ven_note
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :al_anno_registrazione and
				num_registrazione = :al_num_registrazione and
				prog_riga_ord_ven = :ll_prog_riga_ord_ven and
				cod_nota = :ls_cod_nota;

	if sqlca.sqlcode <> 0 then return -1
			
	if sqlca.sqlcode = 0  and lenA(lb_blob) > 0 then 
	
		select estensione
		into :ls_estensione
		from tab_mimetype
		where cod_azienda = :s_cs_xx.cod_azienda and
				prog_mimetype = :ll_prog_mimetype;
		
		if sqlca.sqlcode = -1 then
			return -1
		end if
		
		ls_temp_dir = guo_functions.uof_get_user_temp_folder( )
		ls_rnd =  guo_functions.uof_get_random_filename()
		as_temp_filename[ll_i] =  ls_temp_dir + ls_rnd + "." + ls_estensione
		guo_functions.uof_blob_to_file( lb_blob, as_temp_filename[ll_i])
	end if
next
return 0
	

end function

public subroutine wf_carica_operatori ();string			ls_cod_operatore, ls_des_operatore, ls_cod_operatore_utente
long			ll_i

//funzione chiamata all'inizio
//serve a caricare la dw degli operatori ed a impostare l'operatore di default associato all'utente collegato

dw_elenco_operatori_selezionati.visible = false

select cod_operatore
into :ls_cod_operatore_utente
from tab_operatori_utenti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente and
			flag_default='S';

if sqlca.sqlcode=0 and ls_cod_operatore_utente<>"" and not isnull(ls_cod_operatore_utente) then
else
	ls_cod_operatore_utente = ""
end if

declare cu_operatori cursor for
select cod_operatore, des_operatore
from   tab_operatori
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_blocco = 'N'
order by cod_operatore;
open cu_operatori;
do while true
	fetch cu_operatori into :ls_cod_operatore, :ls_des_operatore;
	if sqlca.sqlcode <> 0 then exit
	ll_i = dw_elenco_operatori_selezionati.insertrow(0)
	dw_elenco_operatori_selezionati.setitem(ll_i, "cod_operatore",ls_cod_operatore)
	dw_elenco_operatori_selezionati.setitem(ll_i, "des_operatore",ls_des_operatore)
	
	//pre-seleziona l'operatore default dell'utente collegato
	if ls_cod_operatore_utente<>"" and ls_cod_operatore_utente= ls_cod_operatore then
		dw_elenco_operatori_selezionati.setitem(ll_i, "flag_selezionato", "S")
	else
		dw_elenco_operatori_selezionati.setitem(ll_i, "flag_selezionato", "N")
	end if
	
loop
close cu_operatori;

dw_elenco_operatori_selezionati.setsort("flag_selezionato desc, cod_operatore asc")
dw_elenco_operatori_selezionati.sort()

return
end subroutine

public function integer wf_ricerca (ref string as_errore);integer				li_anno_ordine, il_vuoto[], li_anno_reg_ds, li_ret

long					ll_num_ordine, ll_num_reg_ds, ll_i, ll_new,ll_row_ds

datetime				ldt_data_consegna_inizio, ldt_data_consegna_fine, ldt_data_reg_inizio, ldt_data_reg_fine, ldt_data_consegna_ds, ldt_data_reg_ds, ldt_ora_reg, ldt_oggi

string					ls_cod_cliente, ls_flag_operatori, ls_flag_stampati, ls_sql, ls_cod_cliente_ds, ls_rag_soc_1_ds, ls_cod_tipo_ord_ven_ds, &
						ls_cod_deposito_ds, ls_cod_operatore_ds, ls_flag_tipo_bcl_ds, ls_flag_evasione, ls_messaggio, &
						ls_clienti_elaborati[], ls_clienti_con_blocchi[], ls_clienti_con_warning[]

boolean				lb_salta_ordine, lb_ordini_esclusi

datastore			lds_data



dw_ricerca.accepttext()
dw_lista.reset()

ldt_oggi = datetime(today(), 00:00:00)
lb_ordini_esclusi = false


li_anno_ordine = dw_ricerca.getitemnumber(1, "anno_ordine")
ll_num_ordine = dw_ricerca.getitemnumber(1, "num_ordine")
		
ldt_data_consegna_inizio = dw_ricerca.getitemdatetime(1, "data_consegna_inizio")
ldt_data_consegna_fine = dw_ricerca.getitemdatetime(1, "data_consegna_fine")
ldt_data_reg_inizio = dw_ricerca.getitemdatetime(1, "data_registrazione_inizio")
ldt_data_reg_fine = dw_ricerca.getitemdatetime(1, "data_registrazione_fine")
		
ls_cod_cliente = dw_ricerca.getitemstring(1, "cod_cliente")
ls_flag_operatori = dw_ricerca.getitemstring(1, "flag_operatori")
ls_flag_stampati = dw_ricerca.getitemstring(1, "flag_stampati")
ls_flag_evasione = dw_ricerca.getitemstring(1, "flag_evasione")

ls_sql = "select tes_ord_ven.anno_registrazione,tes_ord_ven.num_registrazione,"+&
					"tes_ord_ven.cod_cliente,anag_clienti.rag_soc_1,"+&
					"tes_ord_ven.data_consegna,tes_ord_ven.data_registrazione,"+&
					"tes_ord_ven.cod_tipo_ord_ven,tes_ord_ven.cod_deposito,"+&
					"tes_ord_ven.cod_operatore,tab_tipi_ord_ven.flag_tipo_bcl, tes_ord_ven.ora_registrazione "+&
			"from tes_ord_ven "+&
			"join anag_clienti on anag_clienti.cod_azienda=tes_ord_ven.cod_azienda and "+&
										"anag_clienti.cod_cliente=tes_ord_ven.cod_cliente "+&
			"join tab_tipi_ord_ven on tab_tipi_ord_ven.cod_azienda=tes_ord_ven.cod_azienda and "+&
										"tab_tipi_ord_ven.cod_tipo_ord_ven=tes_ord_ven.cod_tipo_ord_ven "+&
			"where tes_ord_ven.cod_azienda='"+s_cs_xx.cod_azienda+"' and flag_stampato='"+ls_flag_stampati+"' "
						
if li_anno_ordine>0 then
	ls_sql += " and tes_ord_ven.anno_registrazione="+string(li_anno_ordine)
end if
if ll_num_ordine>0 then
	ls_sql += " and tes_ord_ven.num_registrazione="+string(ll_num_ordine)
end if
if not isnull(ldt_data_consegna_inizio) and year(date(ldt_data_consegna_inizio))>=1950 then
	ls_sql += " and tes_ord_ven.data_consegna>='" + string(ldt_data_consegna_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_data_consegna_fine) and year(date(ldt_data_consegna_fine))>=1950 then
	ls_sql += " and tes_ord_ven.data_consegna<='" + string(ldt_data_consegna_fine, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_data_reg_inizio) and year(date(ldt_data_reg_inizio))>=1950 then
	ls_sql += " and tes_ord_ven.data_registrazione>='" + string(ldt_data_reg_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_data_reg_fine) and year(date(ldt_data_reg_fine))>=1950 then
	ls_sql += " and tes_ord_ven.data_registrazione<='" + string(ldt_data_reg_fine, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ls_cod_cliente) and ls_cod_cliente<>"" then
	ls_sql += " and tes_ord_ven.cod_cliente='"+ls_cod_cliente+"' "
end if
if ls_flag_evasione = "AP" then
	ls_sql += " and tes_ord_ven.flag_evasione<>'E' "
end if

//ls_sql += " order by tes_ord_ven.cod_operatore, tes_ord_ven.data_registrazione asc"


declare cu_ordini dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;
open dynamic cu_ordini;

if sqlca.sqlcode <> 0 then 
	as_errore = "Errore nella open del cursore: " + sqlca.sqlerrtext
	return -1
end if


lds_data = create datastore
lds_data.dataobject = "d_riepilogo_documenti_fido"
lds_data.settransobject(sqlca)

do while true
	fetch cu_ordini into	:li_anno_reg_ds, :ll_num_reg_ds, :ls_cod_cliente_ds, :ls_rag_soc_1_ds, &
								:ldt_data_consegna_ds, :ldt_data_reg_ds, :ls_cod_tipo_ord_ven_ds, &
								:ls_cod_deposito_ds, :ls_cod_operatore_ds, :ls_flag_tipo_bcl_ds, :ldt_ora_reg;
	
	if sqlca.sqlcode < 0 then
		as_errore = "Errore nella fetch del cursore: " + sqlca.sqlerrtext
		destroy lds_data
		close cu_ordini;
		return -1
		
	elseif sqlca.sqlcode = 100 then
		exit
	end if
	
	lb_salta_ordine = false
	
	if ls_flag_operatori = "S" then
		
		//hai scelto di visualizzare ordini di operatori precisi: se l'ordine della fetch non ha operatore allora saltalo
		if isnull(ls_cod_operatore_ds) then continue
		
		for ll_i = 1 to dw_elenco_operatori_selezionati.rowcount()
			if dw_elenco_operatori_selezionati.getitemstring(ll_i, "flag_selezionato") = "N" then
				//se l'ordine è di un operatore non flaggato nella lista allora escludilo
				if dw_elenco_operatori_selezionati.getitemstring(ll_i, "cod_operatore") = ls_cod_operatore_ds then
					lb_salta_ordine = true
					exit
				end if
			end if
		next
	end if
	
	if lb_salta_ordine then continue
	
	
	//prima di mettere in lista controlla il blocco cliente
	//#################################################################################
	li_ret = iuo_fido.uof_get_blocco_cliente_loop(	ls_cod_cliente_ds, ldt_oggi, is_cod_parametro_blocco, &
																ls_clienti_elaborati[], ls_clienti_con_blocchi[], ls_clienti_con_warning[], &
																lb_salta_ordine, ls_messaggio)
	if li_ret <> 0 then
		//salvo in un datastore
		ll_row_ds = lds_data.insertrow(0)
		lds_data.setitem(ll_row_ds, "cod_cliente", ls_cod_cliente_ds)
		lds_data.setitem(ll_row_ds, "rag_soc_1", ls_rag_soc_1_ds)
		lds_data.setitem(ll_row_ds, "anno_ordine", li_anno_reg_ds)
		lds_data.setitem(ll_row_ds, "num_ordine", ll_num_reg_ds)
		
		if isnull(ls_cod_operatore_ds) or trim(ls_cod_operatore_ds) ="" then
			lds_data.setitem(ll_row_ds, "numerazione", "<nessun operatore>")
		else
			lds_data.setitem(ll_row_ds, "numerazione", ls_cod_operatore_ds+" "+f_des_tabella ("tab_operatori", "cod_operatore='"+ls_cod_operatore_ds+"'", "des_operatore"))
		end if
		
		if ls_messaggio<>"" then
			lds_data.setitem(ll_row_ds, "messaggio", ls_messaggio)
			lds_data.setitem(ll_row_ds, "ordinamento", 0)
		else
			lds_data.setitem(ll_row_ds, "ordinamento", 1)
		end if
		
		lds_data.setitem(ll_row_ds, "tipo_blocco", li_ret)

	end if
	
	if lb_salta_ordine then
		//non inserire l'ordine nella lista
		lb_ordini_esclusi = true
		continue
	end if
	//#################################################################################
	
	
	//---------------------------------------------------------------
	//metti in lista
	ll_new = dw_lista.insertrow(0)
	dw_lista.setitem(ll_new, "anno_registrazione", li_anno_reg_ds)
	dw_lista.setitem(ll_new, "num_registrazione", ll_num_reg_ds)
	dw_lista.setitem(ll_new, "cod_cliente", ls_cod_cliente_ds)
	dw_lista.setitem(ll_new, "data_consegna", ldt_data_consegna_ds)
	dw_lista.setitem(ll_new, "data_registrazione", ldt_data_reg_ds)
	dw_lista.setitem(ll_new, "tipo_ord_ven", ls_cod_tipo_ord_ven_ds)
	dw_lista.setitem(ll_new, "ragione_sociale", ls_rag_soc_1_ds)
	dw_lista.setitem(ll_new, "cod_deposito", ls_cod_deposito_ds)
	dw_lista.setitem(ll_new, "cod_operatore", ls_cod_operatore_ds)
	dw_lista.setitem(ll_new, "flag_tipo_bcl", ls_flag_tipo_bcl_ds)
	dw_lista.setitem(ll_new, "ora_registrazione", ldt_ora_reg)
	

loop

close cu_ordini;


//ordina per operatore, anno registrazione, num. registrazione
dw_lista.setsort("cod_operatore,anno_registrazione,num_registrazione desc")
dw_lista.sort()


if lb_ordini_esclusi then
	g_mb.warning("Alcuni clienti negli ordini risultano bloccati o con blocco finanziario "+&
						"e la tabella di configurazione 'parametri blocco' indica di bloccare la stampa dell'ordine!~r~nQuesti ordini non saranno presenti in lista!~r~n"+&
						"Premere OK per visualizzare il riepilogo e successivamente proseguire nella stampa degli ordini in lista.")
end if

s_cs_xx.parametri.parametro_b_1 = false


if lds_data.rowcount() > 0 then
	s_cs_xx.parametri.parametro_b_1 = true
	s_cs_xx.parametri.parametro_ds_1 = lds_data
	s_cs_xx.parametri.parametro_s_10 =  "ORDINI CON CLIENTE BLOCCATO IN ANAGRAFICA O CON BLOCCO FINANZIARIO"
	window_open(w_riepilogo_documenti_fido, 0)
end if

destroy lds_data

return 0
end function

public function integer wf_report_sfuso (integer ai_anno_ord_ven, long al_num_ord_ven);boolean  lb_modello, lb_test, lb_evasi

string		ls_stringa, ls_cod_tipo_ord_ven, ls_cod_banca_clien_for, ls_cod_cliente, ls_des_valuta, ls_des_valuta_lingua, &
			ls_cod_valuta, ls_cod_pagamento, ls_num_ord_cliente, ls_cod_porto, ls_cod_resa, ls_cod_vettore, &
			ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_anag_vettori_rag_soc_1, &
			ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
			ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, ls_des_operatore, &
			ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli, &
			ls_partita_iva, ls_cod_fiscale, ls_cod_lingua, ls_flag_tipo_cliente, ls_des_giro_consegna, &
			ls_des_tipo_ord_ven, ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, ls_sconti,&
			ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua , ls_cod_giro_consegna, &
			ls_des_prodotto_anag, ls_flag_stampa_ordine, ls_cod_tipo_det_ven, ls_cod_abi, ls_cod_cab, &
			ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_stringa_euro, ls_dicitura_std,ls_cod_operatore, &
			ls_flag_tipo_report, ls_descrizione_riga, ls_cod_misura_mag, ls_flag_tassativo, ls_formato, &
			ls_fax, ls_telefono, ls_alias_cliente, ls_cod_tipo_anagrafica, ls_des_tipo_anagrafica, &
			ls_rif_interscambio, ls_rif_vs_ordine, ls_nota_prodotto, ls_evasione, ls_flag, ls_cod_deposito, ls_des_deposito, ls_logo_etichetta, &
			ls_cod_imballo, ls_des_imballo, ls_des_imballi_lingua, ls_cod_agente_1, ls_cod_agente_2, ls_des_variabile, ls_flag_evasione, ls_stato_cli

long     ll_errore, ll_prog_riga_ord_ven, ll_num_riga_appartenenza, ll_riga_appartenenza_padre

dec{4}   ld_tot_val_ordine, ld_sconto, ld_quan_ordine, ld_prezzo_vendita, ld_sconto_1, &
	      ld_sconto_2, ld_val_riga, ld_sconto_pagamento, ld_quantita_um, ld_prezzo_um, &
		   ld_cambio_ven, ld_num_colli, ld_peso_lordo, ld_tot_val_ordine_euro, ld_quan_evasa, ld_provvigione_1,&
			ld_dim_x_riga, ld_dim_y_riga
			
dec{5}   ld_fat_conversione_ven

datetime ldt_data_ord_cliente, ldt_data_registrazione, ldt_data_consegna,ldt_data_pronto,ldt_data_consegna_testata

wf_barcode()

ls_flag_evasione = dw_ricerca.getitemstring(1, "flag_evasione")

if ls_flag_evasione <> "AP" then
	lb_evasi = true
else
	lb_evasi = false
end if

dw_report_ord_ven_sfuso.reset()

select stringa  
into  :ls_stringa  
from  parametri_azienda  
where cod_azienda = :s_cs_xx.cod_azienda and  
      cod_parametro = 'CVL';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

select stringa  
into  :ls_stringa_euro
from  parametri_azienda  
where cod_azienda = :s_cs_xx.cod_azienda and  
      flag_parametro = 'S' and  
      cod_parametro = 'EUR';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa_euro)
end if

// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua

select flag
into	 :ls_flag
from	 parametri_azienda
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'DTL';
		 
if sqlca.sqlcode = 0 and not isnull(ls_flag) and ls_flag = "S" then
	ls_flag = "S" 
else
	ls_flag = "N"
end if

select cod_tipo_ord_ven,   
		 data_registrazione,   
		 cod_cliente,   
		 cod_valuta,
		 cambio_ven,
		 cod_pagamento,   
		 sconto,   
		 cod_banca_clien_for,   
		 num_ord_cliente,   
		 data_ord_cliente,   
		 cod_porto,   
		 cod_resa,   
		 nota_testata,   
		 nota_piede,   
		 tot_val_ordine_valuta,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia,
		 cod_operatore,
		 data_pronto,
		 data_consegna,
		 num_colli,
		 peso_lordo,
		 cod_giro_consegna,
		 cod_vettore,
		 flag_tassativo,
		 tot_val_ordine,
		 alias_cliente,
		 rif_interscambio,
		 num_ord_cliente,
		 cod_deposito,
		 cod_imballo,
		 cod_agente_1,
		 cod_agente_2,
		 logo_etichetta
into   :ls_cod_tipo_ord_ven,   
	 	 :ldt_data_registrazione,   
	 	 :ls_cod_cliente,   
		 :ls_cod_valuta,   
		 :ld_cambio_ven,
		 :ls_cod_pagamento,   
		 :ld_sconto,   
		 :ls_cod_banca_clien_for,   
		 :ls_num_ord_cliente,   
		 :ldt_data_ord_cliente,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_tot_val_ordine,   
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,
		 :ls_cod_operatore,
		 :ldt_data_pronto,
		 :ldt_data_consegna_testata,
		 :ld_num_colli,
		 :ld_peso_lordo,
		 :ls_cod_giro_consegna,
		 :ls_cod_vettore,
		 :ls_flag_tassativo,
		 :ld_tot_val_ordine_euro,
		 :ls_alias_cliente,
		 :ls_rif_interscambio,
		 :ls_rif_vs_ordine,
		 :ls_cod_deposito,
		 :ls_cod_imballo,
		 :ls_cod_agente_1,
		 :ls_cod_agente_2,
		 :ls_logo_etichetta
from   tes_ord_ven  
where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_ord_ven.anno_registrazione = :ai_anno_ord_ven and 
		 tes_ord_ven.num_registrazione = :al_num_ord_ven;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_ord_ven)
	setnull(ldt_data_registrazione)
	setnull(ls_cod_cliente)
	setnull(ls_cod_valuta)
	setnull(ls_cod_pagamento)
	setnull(ld_sconto)
	setnull(ls_cod_banca_clien_for)
	setnull(ls_num_ord_cliente)
	setnull(ldt_data_ord_cliente)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ld_tot_val_ordine)
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
	setnull(ls_indirizzo)
	setnull(ls_localita)
	setnull(ls_frazione)
	setnull(ls_cap)
	setnull(ls_provincia)
	setnull(ls_alias_cliente)
	setnull(ls_rif_interscambio)
	setnull(ls_rif_vs_ordine)
	setnull(ls_cod_imballo)
	setnull(ls_cod_agente_1)
	setnull(ls_cod_agente_2)
end if

try
	ls_logo_etichetta = wf_imposta_logo_personalizzato(ls_logo_etichetta, ls_cod_cliente)
	dw_report_ord_ven_sfuso.object.p_logo.filename = ls_logo_etichetta
catch (runtimeerror err_logo)
end try


// donato: 11/01/2012: aggiunto deposito
select des_deposito
into :ls_des_deposito
from anag_depositi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_deposito = :ls_cod_deposito;

if isnull(ls_des_deposito) then ls_des_deposito = ""
// ----

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return -1
end if

select		rag_soc_1,   
			rag_soc_2,   
			indirizzo,   
			localita,   
			frazione,   
			cap,   
			provincia,   
			partita_iva,   
			cod_fiscale,   
			cod_lingua,   
			flag_tipo_cliente,
			fax,
			telefono,
			cod_tipo_anagrafica,
			stato
into	:ls_rag_soc_1_cli,   
		:ls_rag_soc_2_cli,   
		:ls_indirizzo_cli,   
		:ls_localita_cli,   
		:ls_frazione_cli,   
		:ls_cap_cli,   
		:ls_provincia_cli,   
		:ls_partita_iva,   
		:ls_cod_fiscale,   
		:ls_cod_lingua,   
		:ls_flag_tipo_cliente,
		:ls_fax,
		:ls_telefono,
		:ls_cod_tipo_anagrafica,
		:ls_stato_cli
from   anag_clienti  
where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
       anag_clienti.cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_cli)
	setnull(ls_rag_soc_2_cli)
	setnull(ls_indirizzo_cli)
	setnull(ls_localita_cli)
	setnull(ls_frazione_cli)
	setnull(ls_cap_cli)
	setnull(ls_provincia_cli)
	setnull(ls_partita_iva)
	setnull(ls_cod_fiscale)
	setnull(ls_cod_lingua)
	setnull(ls_flag_tipo_cliente)
	setnull(ls_alias_cliente)
	setnull(ls_stato_cli)
end if

if not isnull(ls_stato_cli) and ls_stato_cli<>"" then
	if not isnull(ls_localita_cli) and ls_localita_cli<>"" then
		ls_localita_cli += " - " + ls_stato_cli
	else
		ls_localita_cli = ls_stato_cli
	end if
end if

if ls_flag_tipo_cliente = 'E' then
	ls_partita_iva = ls_cod_fiscale
end if

select tab_tipi_ord_ven.des_tipo_ord_ven  
into   :ls_des_tipo_ord_ven  
from   tab_tipi_ord_ven  
where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
       tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_ord_ven)
end if

select tab_pagamenti.des_pagamento,   
       tab_pagamenti.sconto  
into   :ls_des_pagamento,   
       :ld_sconto_pagamento  
from   tab_pagamenti  
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
end if

select tab_pagamenti_lingue.des_pagamento  
into   :ls_des_pagamento_lingua  
from   tab_pagamenti_lingue  
where  tab_pagamenti_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_pagamenti_lingue.cod_pagamento = :ls_cod_pagamento and
       tab_pagamenti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

select des_banca, cod_abi, cod_cab
into   :ls_des_banca, :ls_cod_abi, :ls_cod_cab
from   anag_banche_clien_for  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_banca_clien_for = :ls_cod_banca_clien_for;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca)
end if

select tab_porti.des_porto  
into   :ls_des_porto  
from   tab_porti  
where  tab_porti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti.cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select tab_porti_lingue.des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  tab_porti_lingue.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti_lingue.cod_porto = :ls_cod_porto and
       tab_porti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  tab_rese.cod_azienda = :s_cs_xx.cod_azienda and
       tab_rese.cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select tab_rese_lingue.des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  tab_rese_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_rese_lingue.cod_resa = :ls_cod_resa and  
       tab_rese_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

// EnMe 20/04/2012; aggiunto imballo
select 	des_imballo
into   		:ls_des_imballo
from   	tab_imballi
where  	cod_azienda = :s_cs_xx.cod_azienda and
       		cod_imballo = :ls_cod_imballo;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballo)
end if

select des_imballi
into   :ls_des_imballi_lingua  
from   tab_imballi_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_imballo = :ls_cod_imballo and  
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballi_lingua)
else
	ls_des_imballo = ls_des_imballi_lingua
end if
// -------------------------------------

select des_giro_consegna
into   :ls_des_giro_consegna
from   tes_giri_consegne
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_giro_consegna = :ls_cod_giro_consegna;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_giro_consegna)
end if

select des_operatore
into   :ls_des_operatore
from   tab_operatori
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_operatore = :ls_cod_operatore;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_operatore)
end if

select rag_soc_1
into   :ls_anag_vettori_rag_soc_1
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_vettore = :ls_cod_vettore;

if sqlca.sqlcode <> 0 then
	setnull(ls_anag_vettori_rag_soc_1)
end if

select tab_valute.des_valuta
into   :ls_des_valuta
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta = :ls_cod_valuta;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta)
end if

select des_valuta
into   :ls_des_valuta_lingua  
from   tab_valute_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_valuta = :ls_cod_valuta and  
       tab_valute_lingue.cod_lingua = :ls_cod_lingua ;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if
 
select des_tipo_anagrafica
into   :ls_des_tipo_anagrafica
from   tab_tipi_anagrafiche
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_tipo_anagrafica = :ls_cod_tipo_anagrafica ;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_anagrafica)
end if


declare cu_dettagli cursor for 
	select   det_ord_ven.prog_riga_ord_ven,
	         det_ord_ven.cod_tipo_det_ven, 
				det_ord_ven.cod_misura, 
				det_ord_ven.quan_ordine, 
				det_ord_ven.prezzo_vendita, 
				det_ord_ven.sconto_1, 
				det_ord_ven.sconto_2, 
				det_ord_ven.imponibile_iva_valuta, 
				det_ord_ven.data_consegna, 
				det_ord_ven.nota_dettaglio, 
				det_ord_ven.cod_prodotto, 
				det_ord_ven.des_prodotto,
				det_ord_ven.fat_conversione_ven,
				det_ord_ven.num_riga_appartenenza,
				det_ord_ven.quantita_um,
				det_ord_ven.prezzo_um,
				nota_prodotto,
				quan_evasa,
				flag_evasione,
				provvigione_1,
				dim_x,
				dim_y
	from     det_ord_ven 
	where    det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				det_ord_ven.anno_registrazione = :ai_anno_ord_ven and 
				det_ord_ven.num_registrazione = :al_num_ord_ven
	order by det_ord_ven.cod_azienda, 
				det_ord_ven.anno_registrazione, 
				det_ord_ven.num_registrazione,
				det_ord_ven.prog_riga_ord_ven;

open cu_dettagli;

do while 0 = 0
   fetch cu_dettagli into :ll_prog_riga_ord_ven,
	                       :ls_cod_tipo_det_ven, 
								  :ls_cod_misura, 
								  :ld_quan_ordine, 
								  :ld_prezzo_vendita,   
								  :ld_sconto_1, 
								  :ld_sconto_2, 
								  :ld_val_riga, 
								  :ldt_data_consegna, 
								  :ls_nota_dettaglio, 
								  :ls_cod_prodotto, 
								  :ls_des_prodotto,
								  :ld_fat_conversione_ven,
								  :ll_num_riga_appartenenza,
								  :ld_quantita_um,
								  :ld_prezzo_um,
								  :ls_nota_prodotto,
								  :ld_quan_evasa,
								  :ls_evasione,
								  :ld_provvigione_1,
								  :ld_dim_x_riga,
								  :ld_dim_y_riga;
   if sqlca.sqlcode <> 0 then exit

	// affinche una riga di ordine compaia nel report sfuso NON deve avere la riga appartenenza 
	// compilata e non deve essere un modello.
	if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then		// potrebbe essere un prodotto modello
		select flag_tipo_report
		into   :ls_flag_tipo_report
		from   tab_flags_configuratore
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_modello = :ls_cod_prodotto;
		if sqlca.sqlcode = 0 then							// è un prodotto modello
			lb_test = false																			// e quindi non stampo
		else
			lb_test = true
			// non è un modello e ha la riga appartenenza = 0; allora non va nel report sfuso
		end if
	else
		lb_test = false
	end if

	if not lb_evasi then
		
		if ls_evasione = "E" then
			continue
		end if
		
		ld_quan_ordine -= ld_quan_evasa
		
		ld_quantita_um = ld_quan_ordine * ld_fat_conversione_ven
		
	end if
	
	if lb_test then																					// verifico se inserire la riga oppure no
		dw_report_ord_ven_sfuso.insertrow(0)
		dw_report_ord_ven_sfuso.setrow(dw_report_ord_ven_sfuso.rowcount())
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "formato", ls_formato)		
	
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "parametri_azienda_stringa", ls_stringa)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "parametri_azienda_stringa_euro", ls_stringa_euro)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_anno_registrazione", ai_anno_ord_ven)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_num_registrazione", al_num_ord_ven)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_tipo_ord_ven", ls_cod_tipo_ord_ven)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_data_registrazione", ldt_data_registrazione)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_cliente", ls_cod_cliente)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_valuta", ls_cod_valuta)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cambio_ven", ld_cambio_ven)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_valute_des_valuta", ls_des_valuta)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_valute_lingue_des_valuta", ls_des_valuta_lingua)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_pagamento", ls_cod_pagamento)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_sconto", ld_sconto)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_banca_clien_for", ls_cod_banca_clien_for)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_num_ord_cliente", ls_num_ord_cliente)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_data_ord_cliente", ldt_data_ord_cliente)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_rag_soc_1", ls_rag_soc_1)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_rag_soc_2", ls_rag_soc_2)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_indirizzo", ls_indirizzo)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_localita", ls_localita)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_frazione", ls_frazione)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cap", ls_cap)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_provincia", ls_provincia)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_nota_testata", ls_nota_testata)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_nota_piede", ls_nota_piede)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_porto", ls_cod_porto)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_resa", ls_cod_resa)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_des_operatore", ls_des_operatore)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_vettori_rag_soc_1", ls_anag_vettori_rag_soc_1)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_data_pronto", ldt_data_pronto)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_data_consegna", ldt_data_consegna_testata)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_des_giro", ls_des_giro_consegna)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_num_colli", ld_num_colli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_peso", ld_peso_lordo)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_tot_val_ordine", ld_tot_val_ordine)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_indirizzo", ls_indirizzo_cli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_localita", ls_localita_cli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_frazione", ls_frazione_cli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_cap", ls_cap_cli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_provincia", ls_provincia_cli)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_partita_iva", ls_partita_iva)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_fax", ls_fax)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_telefono", ls_telefono)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_des_deposito", ls_des_deposito)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_tipi_ord_ven_des_tipo_ord_ven", ls_des_tipo_ord_ven)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_pagamenti_sconto", ld_sconto_pagamento)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_banche_clien_for_des_banca", ls_des_banca)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_banche_clien_for_cod_abi", ls_cod_abi)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_banche_clien_for_cod_cab", ls_cod_cab)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "flag_tassativo", ls_flag_tassativo)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "alias_cliente", ls_alias_cliente)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_clienti_des_tipo_anagrafica", ls_des_tipo_anagrafica)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(),"rif_interscambio",ls_rif_interscambio)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(),"rif_vs_ordine", ls_rif_vs_ordine)
	
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_provvigione_1", ld_provvigione_1)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_agente_1", ls_cod_agente_1)
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_cod_agente_2", ls_cod_agente_2)

		select tab_tipi_det_ven.flag_stampa_ordine  
		into   :ls_flag_stampa_ordine  
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
		if sqlca.sqlcode <> 0 then
			setnull(ls_flag_stampa_ordine)
		end if
	
		if ls_flag_stampa_ordine = 'S' then
			select des_prodotto, cod_misura_mag
			into   :ls_des_prodotto_anag, :ls_cod_misura_mag
			from   anag_prodotti  
			where  cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_anag)
			end if
	

			// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua
			
			if ls_flag = "S" then
				setnull(ls_des_prodotto_lingua)
			else	
		
				select anag_prodotti_lingue.des_prodotto  
				into   :ls_des_prodotto_lingua  
				from   anag_prodotti_lingue  
				where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
						 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
				
				if sqlca.sqlcode <> 0 then
					setnull(ls_des_prodotto_lingua)
				else
					setnull(ls_des_prodotto)
					setnull(ls_des_prodotto_anag)
				end if
			end if
	
			select tab_prod_clienti.cod_prod_cliente  
			into   :ls_cod_prod_cliente  
			from   tab_prod_clienti  
			where  tab_prod_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_prod_clienti.cod_prodotto = :ls_cod_prodotto and   
					 tab_prod_clienti.cod_cliente = :ls_cod_cliente;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_cod_prod_cliente)
			end if

			if ls_cod_misura <> ls_cod_misura_mag then
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_quan_ordine", ld_quan_ordine)
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_quantita_um", ld_quantita_um)
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_prezzo_vendita", ld_prezzo_um)
			else
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_quan_ordine", ld_quan_ordine)
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_quantita_um", ld_quan_ordine)
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_prezzo_vendita", ld_prezzo_vendita)
			end if
	
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_cod_misura", ls_cod_misura)
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_prodotti_cod_misura_mag", ls_cod_misura_mag)
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_cod_misura", ls_cod_misura)
//			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_quan_ordine", ld_quan_ordine)
//			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_prezzo_vendita", ld_prezzo_vendita)
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_fat_conversione", ld_fat_conversione_ven)
			
			if not isnull(ls_nota_prodotto) then
				ls_nota_dettaglio += "~r~n" + ls_nota_prodotto
			end if
			
			if ld_dim_y_riga > 0 and not isnull(ld_dim_y_riga) and not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then
				select des_variabile
				into :ls_des_variabile
				from tab_des_variabili
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :ls_cod_prodotto and
						cod_variabile = 'DIM_2' and
						flag_ipertech_apice = 'S';
				if sqlca.sqlcode = 0 and len(ls_des_variabile) > 0 and not isnull(ls_des_variabile) then
					ls_nota_dettaglio = ls_des_variabile + "=" + string(ld_dim_y_riga,"###,##0.00") + " - " +  ls_nota_dettaglio
				end if
			end if
			
			if ld_dim_x_riga > 0 and not isnull(ld_dim_x_riga) and not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then
				select des_variabile
				into :ls_des_variabile
				from tab_des_variabili
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :ls_cod_prodotto and
						cod_variabile = 'DIM_1' and
						flag_ipertech_apice = 'S';
				if sqlca.sqlcode = 0 and len(ls_des_variabile) > 0 and not isnull(ls_des_variabile) then
					ls_nota_dettaglio= ls_des_variabile + "=" + string(ld_dim_x_riga,"###,##0.00") + " - " + ls_nota_dettaglio
				end if
			end if
			
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_nota_dettaglio", ls_nota_dettaglio)
			ls_sconti = ""
			if ld_sconto_1 > 0 then
				if dec(int(ld_sconto_1)) <> ld_sconto_1 then
					ls_sconti = string(ld_sconto_1,"#0,0#")
				else
					ls_sconti = string(ld_sconto_1,"#0")
				end if
			end if				
			if ld_sconto_2 > 0 then
				if dec(int(ld_sconto_2)) <> ld_sconto_2 then
					ls_sconti = ls_sconti + " + " + string(ld_sconto_2,"#0,0#")
				else
					ls_sconti = ls_sconti + " + " + string(ld_sconto_2,"#0")
				end if
			end if				
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_sconti", ls_sconti)
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_val_riga", ld_val_riga)
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_data_consegna", ldt_data_consegna)
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_cod_prodotto", ls_cod_prodotto)
			if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_num_riga", ll_prog_riga_ord_ven)
			else			
				dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_num_riga", ll_num_riga_appartenenza)
			end if
			if not isnull(ls_des_prodotto_lingua) then
				ls_descrizione_riga = ls_des_prodotto_lingua
			elseif not isnull(ls_des_prodotto) then
				ls_descrizione_riga = ls_des_prodotto
			else
				ls_descrizione_riga = ls_des_prodotto_anag
			end if
	

//			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "anag_prodotti_lingue_des_prodotto", ls_des_prodotto_lingua)
			if not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then ls_descrizione_riga =  ls_cod_prodotto + "~r~n" + ls_descrizione_riga
			dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "det_ord_ven_des_prodotto", ls_descrizione_riga)
		end if
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_porti_des_porto", ls_des_porto)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_rese_des_resa", ls_des_resa)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_tot_val_ordine", ld_tot_val_ordine)
		
		dw_report_ord_ven_sfuso.setitem(dw_report_ord_ven_sfuso.getrow(), "tes_ord_ven_tot_val_ordine_euro", ld_tot_val_ordine_euro)
		
	end if
//	dw_report_ord_ven_sfuso.object.tes_ord_ven_tot_val_ordine_valuta_t.text = "Totale in " + ls_cod_valuta

loop

close cu_dettagli;

dw_report_ord_ven_sfuso.resetupdate()

return 0
end function

public function integer wf_report_tecniche (integer ai_anno_ord_ven, long ai_num_ord_ven);boolean  lb_modello, lb_test, lb_evasi

string		ls_stringa, ls_cod_tipo_ord_ven, ls_cod_banca_clien_for, ls_cod_cliente, ls_des_valuta, ls_des_valuta_lingua, &
			ls_cod_valuta, ls_cod_pagamento, ls_num_ord_cliente, ls_cod_porto, ls_cod_resa, ls_cod_vettore, &
			ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_anag_vettori_rag_soc_1, &
			ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
			ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, ls_des_operatore, &
			ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli, &
			ls_partita_iva, ls_cod_fiscale, ls_cod_lingua, ls_flag_tipo_cliente, ls_des_giro_consegna, &
			ls_des_tipo_ord_ven, ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, ls_sconti,&
			ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua , ls_cod_giro_consegna, &
			ls_des_prodotto_anag, ls_flag_stampa_ordine, ls_cod_tipo_det_ven, ls_cod_abi, ls_cod_cab, &
			ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_stringa_euro, ls_dicitura_std,ls_cod_operatore, &
			ls_cod_prodotto_finito, ls_cod_modello, ls_cod_tessuto, ls_cod_non_a_magazzino, ls_cod_tessuto_mantovana, &
			ls_cod_colore_mantovana, ls_flag_tipo_mantovana,ls_flag_cordolo,ls_flag_passamaneria,ls_cod_verniciatura, &
			ls_cod_comando,ls_pos_comando,ls_cod_tipo_supporto,ls_flag_luce_finita,ls_note,ls_colore_passamaneria,&
			ls_colore_cordolo,ls_des_vernice_fc, ls_abbreviazione_comando, ls_descrizione_riga, ls_dimensioni, &
			ls_des_tessuto, ls_des_verniciatura, ls_abbreviazione_verniciatura, ls_abbreviazione_supporto, &
			ls_flag_tipo_report, ls_des_comando_1, ls_cod_comando_2, ls_des_comando_2, ls_flag_allegati, ls_cod_misura_mag, &
			ls_flag_tassativo, ls_formato, ls_fax, ls_telefono,ls_alias_cliente, ls_cod_tipo_anagrafica, ls_des_tipo_anagrafica,&
			ls_rif_interscambio, ls_rif_vs_ordine, ls_nota_prodotto, ls_evasione, ls_flag, ls_cod_deposito, ls_des_deposito, &
			ls_cod_imballo, ls_des_imballo,ls_des_imballi_lingua, ls_cod_agente_1, ls_cod_agente_2, ls_flag_evasione, ls_stato_cli, ls_logo_etichetta
			
long     ll_errore, ll_prog_riga_ord_ven, ll_num_riga_appartenenza, ll_riga_appartenenza_padre

dec{4}   ld_tot_val_ordine, ld_sconto, ld_quan_ordine, ld_prezzo_vendita, ld_sconto_1, &
	      ld_sconto_2, ld_val_riga, ld_sconto_pagamento, &
		   ld_cambio_ven, ld_num_colli, ld_peso_lordo, &
		   ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t,ld_altezza_mantovana,ld_altezza_asta, ld_quantita_um, ld_prezzo_um, &
         ld_tot_val_ordine_euro, ld_quan_evasa, ld_provvigione_1,ld_dim_x_riga,ld_dim_y_riga, ld_val_gtot
			
dec{5}   ld_fat_conversione_ven

datetime ldt_data_ord_cliente, ldt_data_registrazione, ldt_data_consegna,ldt_data_pronto,ldt_data_consegna_testata

wf_barcode()

ls_flag_evasione = dw_ricerca.getitemstring(1, "flag_evasione")

if ls_flag_evasione <> "AP" then
	lb_evasi = true
else
	lb_evasi = false
end if

dw_report_ord_ven_tecniche.reset()

select stringa  
into  :ls_stringa  
from  parametri_azienda  
where cod_azienda = :s_cs_xx.cod_azienda and  
      cod_parametro = 'CVL';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

select parametri_azienda.stringa  
into  :ls_stringa_euro
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'EUR';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa_euro)
end if

// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua

select flag
into	 :ls_flag
from	 parametri_azienda
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'DTL';
		 
if sqlca.sqlcode = 0 and not isnull(ls_flag) and ls_flag = "S" then
	ls_flag = "S" 
else
	ls_flag = "N"
end if

select cod_tipo_ord_ven,   
		 data_registrazione,   
		 cod_cliente,   
		 cod_valuta,
		 cambio_ven,
		 cod_pagamento,   
		 sconto,   
		 cod_banca_clien_for,   
		 num_ord_cliente,   
		 data_ord_cliente,   
		 cod_porto,   
		 cod_resa,   
		 nota_testata,   
		 nota_piede,   
		 tot_val_ordine_valuta,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia,
		 cod_operatore,
		 data_pronto,
		 data_consegna,
		 num_colli,
		 peso_lordo,
		 cod_giro_consegna,
		 cod_vettore,
		 flag_tassativo,
		 tot_val_ordine,
		 alias_cliente,
		 rif_interscambio,
		 num_ord_cliente,
		 cod_deposito,
		 cod_imballo,
		 cod_agente_1,
		 cod_agente_2,
		 logo_etichetta
into   :ls_cod_tipo_ord_ven,   
	 	 :ldt_data_registrazione,   
	 	 :ls_cod_cliente,   
		 :ls_cod_valuta,   
		 :ld_cambio_ven,
		 :ls_cod_pagamento,   
		 :ld_sconto,   
		 :ls_cod_banca_clien_for,   
		 :ls_num_ord_cliente,   
		 :ldt_data_ord_cliente,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_tot_val_ordine,   
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,
		 :ls_cod_operatore,
		 :ldt_data_pronto,
		 :ldt_data_consegna_testata,
		 :ld_num_colli,
		 :ld_peso_lordo,
		 :ls_cod_giro_consegna,
		 :ls_cod_vettore,
		 :ls_flag_tassativo,
		 :ld_tot_val_ordine_euro,
		 :ls_alias_cliente,
		 :ls_rif_interscambio,
		 :ls_rif_vs_ordine,
		 :ls_cod_deposito,
		 :ls_cod_imballo,
		 :ls_cod_agente_1,
		 :ls_cod_agente_2,
		 :ls_logo_etichetta
from   tes_ord_ven  
where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_ord_ven.anno_registrazione = :ai_anno_ord_ven and 
		 tes_ord_ven.num_registrazione = :ai_num_ord_ven;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_ord_ven)
	setnull(ldt_data_registrazione)
	setnull(ls_cod_cliente)
	setnull(ls_cod_valuta)
	setnull(ls_cod_pagamento)
	setnull(ld_sconto)
	setnull(ls_cod_banca_clien_for)
	setnull(ls_num_ord_cliente)
	setnull(ldt_data_ord_cliente)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ld_tot_val_ordine)
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
	setnull(ls_indirizzo)
	setnull(ls_localita)
	setnull(ls_frazione)
	setnull(ls_cap)
	setnull(ls_provincia)
	setnull(ls_alias_cliente)
	setnull(ls_rif_interscambio)
	setnull(ls_rif_vs_ordine)
	setnull(ls_cod_imballo)
	setnull(ls_cod_agente_1)
	setnull(ls_cod_agente_2)
end if

try
	ls_logo_etichetta = wf_imposta_logo_personalizzato(ls_logo_etichetta, ls_cod_cliente)
	dw_report_ord_ven_tecniche.object.p_logo.filename = ls_logo_etichetta
catch (runtimeerror err_logo)
end try


// donatoc: 11/01/2012: aggiunto deposito
select des_deposito
into :ls_des_deposito
from anag_depositi
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_deposito = :ls_cod_deposito;

if isnull(ls_des_deposito) then ls_des_deposito = ""
// ----

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return -1
end if	

select		rag_soc_1,   
			rag_soc_2,   
			indirizzo,   
			localita,   
			frazione,   
			cap,   
			provincia,   
			partita_iva,   
			cod_fiscale,   
			cod_lingua,   
			flag_tipo_cliente,
			fax,
			telefono,
			cod_tipo_anagrafica,
			stato
into	:ls_rag_soc_1_cli,   
		:ls_rag_soc_2_cli,   
		:ls_indirizzo_cli,   
		:ls_localita_cli,   
		:ls_frazione_cli,   
		:ls_cap_cli,   
		:ls_provincia_cli,   
		:ls_partita_iva,   
		:ls_cod_fiscale,   
		:ls_cod_lingua,   
		:ls_flag_tipo_cliente,
		:ls_fax,
		:ls_telefono,
		:ls_cod_tipo_anagrafica,
		:ls_stato_cli
from   anag_clienti  
where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
       anag_clienti.cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_cli)
	setnull(ls_rag_soc_2_cli)
	setnull(ls_indirizzo_cli)
	setnull(ls_localita_cli)
	setnull(ls_frazione_cli)
	setnull(ls_cap_cli)
	setnull(ls_provincia_cli)
	setnull(ls_partita_iva)
	setnull(ls_cod_fiscale)
	setnull(ls_cod_lingua)
	setnull(ls_flag_tipo_cliente)
	setnull(ls_cod_tipo_anagrafica)
	setnull(ls_stato_cli)
end if

if not isnull(ls_stato_cli) and ls_stato_cli<>"" then
	if not isnull(ls_localita_cli) and ls_localita_cli<>"" then
		ls_localita_cli += " - " + ls_stato_cli
	else
		ls_localita_cli = ls_stato_cli
	end if
end if

if ls_flag_tipo_cliente = 'E' then
	ls_partita_iva = ls_cod_fiscale
end if

select tab_tipi_ord_ven.des_tipo_ord_ven  
into   :ls_des_tipo_ord_ven  
from   tab_tipi_ord_ven  
where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
       tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_ord_ven)
end if

select tab_pagamenti.des_pagamento,   
       tab_pagamenti.sconto  
into   :ls_des_pagamento,   
       :ld_sconto_pagamento  
from   tab_pagamenti  
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
end if

select tab_pagamenti_lingue.des_pagamento  
into   :ls_des_pagamento_lingua  
from   tab_pagamenti_lingue  
where  tab_pagamenti_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_pagamenti_lingue.cod_pagamento = :ls_cod_pagamento and
       tab_pagamenti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

select des_banca, cod_abi, cod_cab
into   :ls_des_banca, :ls_cod_abi, :ls_cod_cab
from   anag_banche_clien_for  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_banca_clien_for = :ls_cod_banca_clien_for;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca)
end if

select tab_porti.des_porto  
into   :ls_des_porto  
from   tab_porti  
where  tab_porti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti.cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select tab_porti_lingue.des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  tab_porti_lingue.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti_lingue.cod_porto = :ls_cod_porto and
       tab_porti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  tab_rese.cod_azienda = :s_cs_xx.cod_azienda and
       tab_rese.cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select tab_rese_lingue.des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  tab_rese_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_rese_lingue.cod_resa = :ls_cod_resa and  
       tab_rese_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

// EnMe 20/04/2012; aggiunto imballo
select 	des_imballo
into   		:ls_des_imballo
from   	tab_imballi
where  	cod_azienda = :s_cs_xx.cod_azienda and
       		cod_imballo = :ls_cod_imballo;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballo)
end if

select des_imballi
into   :ls_des_imballi_lingua  
from   tab_imballi_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_imballo = :ls_cod_imballo and  
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballi_lingua)
else
	ls_des_imballo = ls_des_imballi_lingua
end if
// -------------------------------------


select des_giro_consegna
into   :ls_des_giro_consegna
from   tes_giri_consegne
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_giro_consegna = :ls_cod_giro_consegna;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_giro_consegna)
end if

select des_operatore
into   :ls_des_operatore
from   tab_operatori
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_operatore = :ls_cod_operatore;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_operatore)
end if

select rag_soc_1
into   :ls_anag_vettori_rag_soc_1
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_vettore = :ls_cod_vettore;

if sqlca.sqlcode <> 0 then
	setnull(ls_anag_vettori_rag_soc_1)
end if

select tab_valute.des_valuta
into   :ls_des_valuta
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta = :ls_cod_valuta;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta)
end if

select des_valuta
into   :ls_des_valuta_lingua  
from   tab_valute_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_valuta = :ls_cod_valuta and  
       tab_valute_lingue.cod_lingua = :ls_cod_lingua ;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if
 
select des_tipo_anagrafica
into   :ls_des_tipo_anagrafica
from   tab_tipi_anagrafiche
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_tipo_anagrafica = :ls_cod_tipo_anagrafica ;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_anagrafica)
end if
 
declare cu_dettagli cursor for 
	select   det_ord_ven.prog_riga_ord_ven,
	         det_ord_ven.cod_tipo_det_ven, 
				det_ord_ven.cod_misura, 
				det_ord_ven.quan_ordine, 
				det_ord_ven.prezzo_vendita, 
				det_ord_ven.sconto_1, 
				det_ord_ven.sconto_2, 
				det_ord_ven.imponibile_iva_valuta, 
				det_ord_ven.data_consegna, 
				det_ord_ven.nota_dettaglio, 
				det_ord_ven.cod_prodotto, 
				det_ord_ven.des_prodotto,
				det_ord_ven.fat_conversione_ven,
				det_ord_ven.num_riga_appartenenza,
				det_ord_ven.quantita_um,
				det_ord_ven.prezzo_um,
				nota_prodotto,
				quan_evasa,
				flag_evasione,
				provvigione_1,
				dim_x,
				dim_y
	from     det_ord_ven 
	where    cod_azienda = :s_cs_xx.cod_azienda and 
				anno_registrazione = :ai_anno_ord_ven and 
				num_registrazione = :ai_num_ord_ven
	order by cod_azienda, 
				anno_registrazione, 
				num_registrazione,
				prog_riga_ord_ven;

open cu_dettagli;

do while 0 = 0
   fetch cu_dettagli into :ll_prog_riga_ord_ven,
	                       		:ls_cod_tipo_det_ven, 
								  :ls_cod_misura, 
								  :ld_quan_ordine, 
								  :ld_prezzo_vendita,   
								  :ld_sconto_1, 
								  :ld_sconto_2, 
								  :ld_val_riga, 
								  :ldt_data_consegna, 
								  :ls_nota_dettaglio, 
								  :ls_cod_prodotto, 
								  :ls_des_prodotto,
								  :ld_fat_conversione_ven,
								  :ll_num_riga_appartenenza,
								  :ld_quantita_um,
								  :ld_prezzo_um,
								  :ls_nota_prodotto,
								  :ld_quan_evasa,
								  :ls_evasione,
								  :ld_provvigione_1,
								  :ld_dim_x_riga,
								  :ld_dim_y_riga;
   if sqlca.sqlcode <> 0 then exit

	if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then		// potrebbe essere un prodotto modello
		select flag_tipo_report
		into   :ls_flag_tipo_report
		from   tab_flags_configuratore
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_modello = :ls_cod_prodotto;
		if sqlca.sqlcode = 0 and ls_flag_tipo_report = "C" then							// è un prodotto modello
			ll_riga_appartenenza_padre = ll_prog_riga_ord_ven
			lb_modello = true
			lb_test = true
		else
			lb_test = false
			// non è un modello e ha la riga appartenenza = 0; allora non va nel report tipo A
		end if
	else
		if ll_num_riga_appartenenza = ll_riga_appartenenza_padre then
			lb_modello = false
			lb_test = true
		else
			lb_modello= false
			lb_test = false
		end if
	end if

	if not lb_evasi then
		
		if ls_evasione = "E" then
			continue
		end if
		
		ld_quan_ordine -= ld_quan_evasa
		
		ld_quantita_um = ld_quan_ordine * ld_fat_conversione_ven
		
	end if

	if lb_test then																					// verifico se inserire la riga oppure no
		dw_report_ord_ven_tecniche.insertrow(0)
		dw_report_ord_ven_tecniche.setrow(dw_report_ord_ven_tecniche.rowcount())
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "formato", ls_formato)
		
	// inzializzo variabili comp_det_ord_ven
		setnull(ls_cod_prodotto_finito)
		setnull(ls_cod_modello)
		ld_dim_x =0
		ld_dim_y =0
		ld_dim_z =0
		ld_dim_t =0
		setnull(ls_cod_tessuto)
		setnull(ls_cod_non_a_magazzino)
		setnull(ls_cod_tessuto_mantovana)
		setnull(ls_cod_colore_mantovana)
		setnull(ls_flag_tipo_mantovana)
		ld_altezza_mantovana = 0
		setnull(ls_flag_cordolo)
		setnull(ls_flag_passamaneria)
		setnull(ls_cod_verniciatura)
		setnull(ls_cod_comando)
		setnull(ls_pos_comando)
		ld_altezza_asta = 0
		setnull(ls_cod_tipo_supporto)
		setnull(ls_flag_luce_finita)
		setnull(ls_colore_passamaneria)
		setnull(ls_colore_cordolo)
		setnull(ls_des_vernice_fc) 
		
	
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "parametri_azienda_stringa", ls_stringa)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "parametri_azienda_stringa_euro", ls_stringa_euro)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_anno_registrazione", ai_anno_ord_ven)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_num_registrazione", ai_num_ord_ven)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_tipo_ord_ven", ls_cod_tipo_ord_ven)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_data_registrazione", ldt_data_registrazione)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_cliente", ls_cod_cliente)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_valuta", ls_cod_valuta)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cambio_ven", ld_cambio_ven)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_valute_des_valuta", ls_des_valuta)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_valute_lingue_des_valuta", ls_des_valuta_lingua)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_pagamento", ls_cod_pagamento)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_sconto", ld_sconto)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_banca_clien_for", ls_cod_banca_clien_for)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_num_ord_cliente", ls_num_ord_cliente)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_data_ord_cliente", ldt_data_ord_cliente)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_rag_soc_1", ls_rag_soc_1)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_rag_soc_2", ls_rag_soc_2)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_indirizzo", ls_indirizzo)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_localita", ls_localita)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_frazione", ls_frazione)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cap", ls_cap)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_provincia", ls_provincia)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_nota_testata", ls_nota_testata)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_nota_piede", ls_nota_piede)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_porto", ls_cod_porto)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_cod_resa", ls_cod_resa)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_des_operatore", ls_des_operatore)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_vettori_rag_soc_1", ls_anag_vettori_rag_soc_1)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_data_pronto", ldt_data_pronto)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_data_consegna", ldt_data_consegna_testata)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_des_giro", ls_des_giro_consegna)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_num_colli", ld_num_colli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_peso", ld_peso_lordo)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_tot_val_ordine", ld_tot_val_ordine)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_indirizzo", ls_indirizzo_cli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_localita", ls_localita_cli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_frazione", ls_frazione_cli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_cap", ls_cap_cli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_provincia", ls_provincia_cli)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_partita_iva", ls_partita_iva)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_fax", ls_fax)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_telefono", ls_telefono)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_des_deposito", ls_des_deposito)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_tipi_ord_ven_des_tipo_ord_ven", ls_des_tipo_ord_ven)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_pagamenti_sconto", ld_sconto_pagamento)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_banche_clien_for_des_banca", ls_des_banca)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_banche_clien_for_cod_abi", ls_cod_abi)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_banche_clien_for_cod_cab", ls_cod_cab)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "flag_tassativo", ls_flag_tassativo)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "alias_cliente", ls_alias_cliente)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_clienti_des_tipo_anagrafica", ls_des_tipo_anagrafica)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(),"rif_interscambio",ls_rif_interscambio)
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(),"rif_vs_ordine", ls_rif_vs_ordine)		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_imballi_des_imballo",ls_des_imballo)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_provvigione_1", ld_provvigione_1)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_agente_1", ls_cod_agente_1)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_agente_2", ls_cod_agente_2)
	
		select tab_tipi_det_ven.flag_stampa_ordine  
		into   :ls_flag_stampa_ordine  
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
		if sqlca.sqlcode <> 0 then
			setnull(ls_flag_stampa_ordine)
		end if
	
		if ls_flag_stampa_ordine = 'S' then
			select des_prodotto, cod_misura_mag
			into   :ls_des_prodotto_anag, :ls_cod_misura_mag
			from   anag_prodotti  
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_anag)
			end if
	

			// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua
			
			if ls_flag = "S" then
				setnull(ls_des_prodotto_lingua)
			else
				
				select anag_prodotti_lingue.des_prodotto  
				into   :ls_des_prodotto_lingua  
				from   anag_prodotti_lingue  
				where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
						 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
				
				if sqlca.sqlcode <> 0 then
					setnull(ls_des_prodotto_lingua)
				else
					setnull(ls_des_prodotto)
					setnull(ls_des_prodotto_anag)
				end if
				
			end if
	
			select tab_prod_clienti.cod_prod_cliente  
			into   :ls_cod_prod_cliente  
			from   tab_prod_clienti  
			where  tab_prod_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_prod_clienti.cod_prodotto = :ls_cod_prodotto and   
					 tab_prod_clienti.cod_cliente = :ls_cod_cliente;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_cod_prod_cliente)
			end if
	
			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_cod_misura", ls_cod_misura)
			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_quan_ordine", ld_quan_ordine)
			if ls_cod_misura_mag <> ls_cod_misura then
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_quan_vendita", ld_quantita_um)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_prezzo_vendita", ld_prezzo_um)
			else
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_quan_vendita", ld_quan_ordine)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_prezzo_vendita", ld_prezzo_vendita)
			end if				
			
//			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_quan_vendita", ld_quan_ordine * ld_fat_conversione_ven)
//			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_prezzo_vendita", ld_prezzo_vendita / ld_fat_conversione_ven)
	//		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_sconto_1", ld_sconto_1)
	//		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_sconto_2", ld_sconto_2)
			ls_sconti = ""
			if ld_sconto_1 > 0 then
				if dec(int(ld_sconto_1)) <> ld_sconto_1 then
					ls_sconti = string(ld_sconto_1,"#0,0#")
				else
					ls_sconti = string(ld_sconto_1,"#0")
				end if
			end if				
			if ld_sconto_2 > 0 then
				if dec(int(ld_sconto_2)) <> ld_sconto_2 then
					ls_sconti = ls_sconti + " + " + string(ld_sconto_2,"#0,0#")
				else
					ls_sconti = ls_sconti + " + " + string(ld_sconto_2,"#0")
				end if
			end if				
			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_sconti", ls_sconti)
			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_val_riga", ld_val_riga)
			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_data_consegna", ldt_data_consegna)
	//		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_nota_dettaglio", ls_nota_dettaglio)
			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_cod_prodotto", ls_cod_prodotto)
			if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_num_riga", ll_prog_riga_ord_ven)
			else			
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_num_riga", ll_num_riga_appartenenza)
			end if
			if not isnull(ls_des_prodotto_lingua) then
				ls_descrizione_riga = ls_des_prodotto_lingua
			elseif not isnull(ls_des_prodotto) then
				ls_descrizione_riga = ls_des_prodotto
			else
				ls_descrizione_riga = ls_des_prodotto_anag
			end if
	
	//		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "anag_prodotti_lingue_des_prodotto", ls_des_prodotto_lingua)
	
			// ------------  leggo e stampo comp_det_ord_ven ----------------------------------------------------------
			SELECT cod_prod_finito,        cod_modello,    dim_x,    dim_y,    dim_z,    dim_t,    cod_tessuto,    cod_non_a_magazzino,    cod_tessuto_mant,         cod_mant_non_a_magazzino, flag_tipo_mantovana,    alt_mantovana,        flag_cordolo,    flag_passamaneria,    cod_verniciatura,    cod_comando,    pos_comando,    alt_asta,        cod_tipo_supporto,    flag_misura_luce_finita, note,    colore_passamaneria,    colore_cordolo,    des_vernice_fc,    des_comando_1,     cod_comando_2,     des_comando_2,    flag_allegati
			 INTO :ls_cod_prodotto_finito,:ls_cod_modello,:ld_dim_x,:ld_dim_y,:ld_dim_z,:ld_dim_t,:ls_cod_tessuto,:ls_cod_non_a_magazzino,:ls_cod_tessuto_mantovana,:ls_cod_colore_mantovana, :ls_flag_tipo_mantovana,:ld_altezza_mantovana,:ls_flag_cordolo,:ls_flag_passamaneria,:ls_cod_verniciatura,:ls_cod_comando,:ls_pos_comando,:ld_altezza_asta,:ls_cod_tipo_supporto,:ls_flag_luce_finita,    :ls_note,:ls_colore_passamaneria,:ls_colore_cordolo,:ls_des_vernice_fc, :ls_des_comando_1, :ls_cod_comando_2, :ls_des_comando_2,:ls_flag_allegati 
			 FROM comp_det_ord_ven  
			WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
					anno_registrazione = :ai_anno_ord_ven AND  
					num_registrazione = :ai_num_ord_ven AND  
					prog_riga_ord_ven = :ll_prog_riga_ord_ven ;
			if sqlca.sqlcode = 0 then
				if not isnull(ls_cod_prodotto_finito) and len(ls_cod_prodotto_finito) > 0 then
					if isnull(ls_descrizione_riga) then ls_descrizione_riga = ""
					ls_descrizione_riga = ls_cod_prodotto_finito + "~r~n" + ls_descrizione_riga
				end if
	//			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_prodotto_finito", ls_cod_prodotto_finito)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "dim_x", ld_dim_x)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "dim_y", ld_dim_y)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "dim_z", ld_dim_z)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "dim_t", ld_dim_t)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_tessuto", ls_cod_tessuto)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_colore_tessuto", ls_cod_non_a_magazzino)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_tessuto_mant", ls_cod_colore_mantovana)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "flag_tipo_mantovana", ls_flag_tipo_mantovana)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "altezza_mantovana", ld_altezza_mantovana)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "colore_cordolo", ls_colore_cordolo)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "colore_passamaneria", ls_colore_passamaneria)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_comando", ls_cod_comando)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "pos_comando", ls_pos_comando)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "altezza_asta", ld_altezza_asta)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "flag_luce_finita", ls_flag_luce_finita)
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "flag_allegati", ls_flag_allegati)
				if not isnull(ls_cod_comando_2) and len(ls_cod_comando_2) > 0 then
					dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "guide_perlon", "S")
				else
					dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "guide_perlon", "")
				end if
				if not isnull(ls_cod_tipo_supporto) and ls_cod_tipo_supporto <> "" then
					select cod_comodo
					into   :ls_abbreviazione_supporto
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_tipo_supporto;
					if sqlca.sqlcode = 0 then
						dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_tipo_supporto", ls_abbreviazione_supporto)
					else
						dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_tipo_supporto", "")
					end if
				end if
				if not isnull(ls_cod_verniciatura) and ls_cod_verniciatura <> "" then
					select cod_comodo
					into   :ls_abbreviazione_verniciatura
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_verniciatura;
					if sqlca.sqlcode = 0 then
						dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_verniciatura", ls_abbreviazione_verniciatura)
					else
						dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "cod_verniciatura", "")
					end if
				end if
				
				select abbreviazione_comando
				into   :ls_abbreviazione_comando
				from   tab_comandi
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_comando = :ls_cod_comando;
				if sqlca.sqlcode <> 0 then
					setnull(ls_abbreviazione_comando)
				end if
				dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "abbreviazione_comando", ls_abbreviazione_comando)
				
				if not isnull(ls_cod_tessuto) and ls_cod_tessuto <> "" then
					select des_prodotto
					into   :ls_des_tessuto
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_tessuto;
					if sqlca.sqlcode = 0 and not isnull(ls_des_tessuto) then
						if isnull(ls_descrizione_riga) then ls_descrizione_riga = ""
						ls_descrizione_riga = ls_descrizione_riga + ", " + ls_des_tessuto
					end if
				end if
				
				
				//-----------------------------------------------------------------------------------
				//sia che VGTOT>0 o nullo accodo la scritta relativa al VGTOT
				if g_str.isnotempty(ls_cod_non_a_magazzino) then
				
					select val_gtot
					into :ld_val_gtot
					from tab_colori_tessuti
					where 	cod_azienda=:s_cs_xx.cod_azienda and
								cod_colore_tessuto=:ls_cod_non_a_magazzino;
					
					if isnull(ld_val_gtot) then
						ls_descrizione_riga += " VAL GTOT=<null>"
					else
						ls_descrizione_riga += " VAL GTOT="+string(ld_val_gtot, "###0.00##")
					end if
				end if
				//-----------------------------------------------------------------------------------
				
				
				if not isnull(ls_des_vernice_fc) and len(ls_des_vernice_fc) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_vernice_fc
				end if
				if not isnull(ls_des_comando_1) and len(ls_des_comando_1) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_comando_1
				end if
				if not isnull(ls_des_comando_2) and len(ls_des_comando_2) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_comando_2
				end if
			else
				if not isnull(ls_cod_prodotto) then
					ls_descrizione_riga = ls_cod_prodotto + "~r~n" + ls_des_prodotto
				else
					ls_descrizione_riga = ls_des_prodotto
				end if
				if ld_dim_x_riga > 0 and not  isnull(ld_dim_x_riga) then dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_x", ld_dim_x_riga)
				if ld_dim_y_riga > 0 and not  isnull(ld_dim_y_riga) then dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_y", ld_dim_y_riga)
			end if
			if not isnull(ls_nota_dettaglio) and len(ls_nota_dettaglio) > 0 then
				ls_descrizione_riga = ls_descrizione_riga + "~r~n" + ls_nota_dettaglio
			end if
			
			if not isnull(ls_nota_prodotto) then
				ls_descrizione_riga += "~r~n" + ls_nota_prodotto
			end if
			
			// stefanop 23/06/2010: progetto 140 ticket 2010/167: accodo la descrizione tessuto alla descrizione
			// VERNICIATURA
			select des_prodotto
			into :ls_des_verniciatura
			from anag_prodotti
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_verniciatura;
			
			if sqlca.sqlcode = 0 and not isnull(ls_des_verniciatura) and len(ls_des_verniciatura) > 0 then
				ls_descrizione_riga += ", " + ls_des_verniciatura
			end if
			// ----
			
			dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "det_ord_ven_des_prodotto", ls_descrizione_riga)
		end if
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_porti_des_porto", ls_des_porto)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_rese_des_resa", ls_des_resa)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_tot_val_ordine", ld_tot_val_ordine)
		
		dw_report_ord_ven_tecniche.setitem(dw_report_ord_ven_tecniche.getrow(), "tes_ord_ven_tot_val_ordine_euro", ld_tot_val_ordine_euro)
		
	end if
//	dw_report_ord_ven_tecniche.object.tes_ord_ven_tot_val_ordine_valuta_t.text = "Totale in " + ls_cod_valuta

loop

close cu_dettagli;

dw_report_ord_ven_tecniche.resetupdate()

return 0
end function

public function integer wf_report_tende (integer ai_anno_ord_ven, long ai_num_ord_ven);boolean  lb_modello, lb_test, lb_evasi

string		ls_stringa, ls_cod_tipo_ord_ven, ls_cod_banca_clien_for, ls_cod_cliente, ls_des_valuta, ls_des_valuta_lingua, &
			ls_cod_valuta, ls_cod_pagamento, ls_num_ord_cliente, ls_cod_porto, ls_cod_resa, ls_cod_vettore, &
			ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_anag_vettori_rag_soc_1, &
			ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
			ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, ls_des_operatore, &
			ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli,ls_null, &
			ls_partita_iva, ls_cod_fiscale, ls_cod_lingua, ls_flag_tipo_cliente, ls_des_giro_consegna, &
			ls_des_tipo_ord_ven, ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, ls_sconti,&
			ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua , ls_cod_giro_consegna, &
			ls_des_prodotto_anag, ls_flag_stampa_ordine, ls_cod_tipo_det_ven, ls_cod_abi, ls_cod_cab, &
			ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_stringa_euro, ls_dicitura_std,ls_cod_operatore, &
			ls_cod_prodotto_finito, ls_cod_modello, ls_cod_tessuto, ls_cod_non_a_magazzino, ls_cod_tessuto_mantovana, &
			ls_cod_colore_mantovana, ls_flag_tipo_mantovana,ls_flag_cordolo,ls_flag_passamaneria,ls_cod_verniciatura, &
			ls_cod_comando,ls_pos_comando,ls_cod_tipo_supporto,ls_flag_luce_finita,ls_note,ls_colore_passamaneria,&
			ls_colore_cordolo,ls_des_vernice_fc, ls_abbreviazione_comando, ls_descrizione_riga, ls_dimensioni, &
			ls_des_tessuto, ls_des_verniciatura, ls_abbreviazione_verniciatura, ls_abbreviazione_supporto, ls_flag_tassativo, &
			ls_flag_tipo_report, ls_des_comando_1, ls_cod_comando_2, ls_des_comando_2, ls_flag_allegati, &
			ls_cod_misura_mag, ls_formato, ls_fax, ls_telefono, ls_alias_cliente, ls_cod_tipo_anagrafica, ls_des_tipo_anagrafica, &
			ls_rif_interscambio, ls_rif_vs_ordine, ls_nota_prodotto, ls_evasione, ls_flag, ls_cod_deposito, ls_des_deposito, &
			ls_cod_imballo, ls_des_imballo, ls_des_imballi_lingua, ls_cod_verniciatura_2, &
			ls_cod_verniciatura_3, ls_flag_fc_vernic_2, ls_des_vernic_fc_2,ls_flag_fc_vernic_3, ls_des_vernic_fc_3, &
			ls_nota_testata_st_ord,ls_nota_piede_st_ord, ls_nota_video_st_ord, ls_cod_agente_1, ls_cod_agente_2, ls_flag_evasione, ls_stato_cli, ls_logo_etichetta

long			ll_errore, ll_prog_riga_ord_ven, ll_num_riga_appartenenza, ll_riga_appartenenza_padre

dec{4}		ld_tot_val_ordine, ld_sconto, ld_quan_ordine, ld_prezzo_vendita, ld_sconto_1, &
				ld_sconto_2, ld_val_riga, ld_sconto_pagamento, &
				ld_cambio_ven, ld_num_colli, ld_peso_lordo, &
				ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t,ld_altezza_mantovana,ld_altezza_asta, ld_quantita_um, ld_prezzo_um, &
				ld_tot_val_ordine_euro, ld_quan_evasa, ld_provvigione_1, ld_dim_x_riga, ld_dim_y_riga, ld_val_gtot
			
dec{5}   ld_fat_conversione_ven

datetime ldt_data_ord_cliente, ldt_data_registrazione, ldt_data_consegna,ldt_data_pronto,ldt_data_consegna_testata

wf_barcode()
setnull(ls_null)

ls_flag_evasione = dw_ricerca.getitemstring(1, "flag_evasione")

if ls_flag_evasione <> "AP" then
	lb_evasi = true
else
	lb_evasi = false
end if

dw_report_ord_ven_tende.reset()

select stringa  
into  :ls_stringa  
from  parametri_azienda  
where cod_azienda = :s_cs_xx.cod_azienda and  
      cod_parametro = 'CVL';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

select parametri_azienda.stringa  
into  :ls_stringa_euro
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'EUR';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa_euro)
end if

// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua

select flag
into	 :ls_flag
from	 parametri_azienda
where	 cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'DTL';
		 
if sqlca.sqlcode = 0 and not isnull(ls_flag) and ls_flag = "S" then
	ls_flag = "S" 
else
	ls_flag = "N"
end if

select cod_tipo_ord_ven,   
		 data_registrazione,   
		 cod_cliente,   
		 cod_valuta,
		 cambio_ven,
		 cod_pagamento,   
		 sconto,   
		 cod_banca_clien_for,   
		 num_ord_cliente,   
		 data_ord_cliente,   
		 cod_porto,   
		 cod_resa,   
		 nota_testata,   
		 nota_piede,   
		 tot_val_ordine_valuta,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia,
		 cod_operatore,
		 data_pronto,
		 data_consegna,
		 num_colli,
		 peso_lordo,
		 cod_giro_consegna,
		 cod_vettore,
		 flag_tassativo,
		 tot_val_ordine,
		 alias_cliente,
		 rif_interscambio,
		 num_ord_cliente,
		 cod_deposito,
		 cod_imballo,
		 cod_agente_1,
		 cod_agente_2,
		 logo_etichetta
into   :ls_cod_tipo_ord_ven,   
	 	 :ldt_data_registrazione,   
	 	 :ls_cod_cliente,   
		 :ls_cod_valuta,   
		 :ld_cambio_ven,
		 :ls_cod_pagamento,   
		 :ld_sconto,   
		 :ls_cod_banca_clien_for,   
		 :ls_num_ord_cliente,   
		 :ldt_data_ord_cliente,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_tot_val_ordine,   
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,
		 :ls_cod_operatore,
		 :ldt_data_pronto,
		 :ldt_data_consegna_testata,
		 :ld_num_colli,
		 :ld_peso_lordo,
		 :ls_cod_giro_consegna,
		 :ls_cod_vettore,
		 :ls_flag_tassativo,
		 :ld_tot_val_ordine_euro,
		 :ls_alias_cliente,
		 :ls_rif_interscambio,
		 :ls_rif_vs_ordine,
		 :ls_cod_deposito,
		 :ls_cod_imballo,
		 :ls_cod_agente_1,
		 :ls_cod_agente_2,
		 :ls_logo_etichetta
from    tes_ord_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		  anno_registrazione = :ai_anno_ord_ven and 
		  num_registrazione = :ai_num_ord_ven;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_ord_ven)
	setnull(ldt_data_registrazione)
	setnull(ls_cod_cliente)
	setnull(ls_cod_valuta)
	setnull(ls_cod_pagamento)
	setnull(ld_sconto)
	setnull(ls_cod_banca_clien_for)
	setnull(ls_num_ord_cliente)
	setnull(ldt_data_ord_cliente)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ld_tot_val_ordine)
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
	setnull(ls_indirizzo)
	setnull(ls_localita)
	setnull(ls_frazione)
	setnull(ls_cap)
	setnull(ls_provincia)
	setnull(ls_rif_interscambio)
	setnull(ls_rif_vs_ordine)
	setnull(ls_cod_imballo)
	setnull(ls_cod_agente_1)
	setnull(ls_cod_agente_2)
end if

try
	ls_logo_etichetta = wf_imposta_logo_personalizzato(ls_logo_etichetta, ls_cod_cliente)
	dw_report_ord_ven_tende.object.p_logo.filename = ls_logo_etichetta
catch (runtimeerror err_logo)
end try


//donatoc: 11/01/2012: aggiunto deposito
select des_deposito
into :ls_des_deposito
from anag_depositi
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_deposito = :ls_cod_deposito;

if isnull(ls_des_deposito) then ls_des_deposito = ""
// ----

select formato
into   :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
	return -1
end if	

select rag_soc_1,   
		rag_soc_2,   
		indirizzo,   
		localita,   
		frazione,   
		cap,   
		provincia,   
		partita_iva,   
		cod_fiscale,   
		cod_lingua,   
		flag_tipo_cliente,
		fax,
		telefono,
		cod_tipo_anagrafica,
		stato
into	:ls_rag_soc_1_cli,   
		:ls_rag_soc_2_cli,   
		:ls_indirizzo_cli,   
		:ls_localita_cli,   
		:ls_frazione_cli,   
		:ls_cap_cli,   
		:ls_provincia_cli,   
		:ls_partita_iva,   
		:ls_cod_fiscale,   
		:ls_cod_lingua,   
		:ls_flag_tipo_cliente,
		:ls_fax,
		:ls_telefono,
		:ls_cod_tipo_anagrafica,
		:ls_stato_cli
from   anag_clienti  
where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
       anag_clienti.cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_cli)
	setnull(ls_rag_soc_2_cli)
	setnull(ls_indirizzo_cli)
	setnull(ls_localita_cli)
	setnull(ls_frazione_cli)
	setnull(ls_cap_cli)
	setnull(ls_provincia_cli)
	setnull(ls_partita_iva)
	setnull(ls_cod_fiscale)
	setnull(ls_cod_lingua)
	setnull(ls_flag_tipo_cliente)
	setnull(ls_alias_cliente)
	setnull(ls_cod_tipo_anagrafica)
	setnull(ls_stato_cli)
end if

if not isnull(ls_stato_cli) and ls_stato_cli<>"" then
	if not isnull(ls_localita_cli) and ls_localita_cli<>"" then
		ls_localita_cli += " - " + ls_stato_cli
	else
		ls_localita_cli = ls_stato_cli
	end if
end if

if ls_flag_tipo_cliente = 'E' then
	ls_partita_iva = ls_cod_fiscale
end if

select des_tipo_ord_ven  
into   :ls_des_tipo_ord_ven  
from   tab_tipi_ord_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_ord_ven)
end if

select des_pagamento,   
       sconto  
into   :ls_des_pagamento,   
       :ld_sconto_pagamento  
from   tab_pagamenti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
end if

select des_pagamento  
into   :ls_des_pagamento_lingua  
from   tab_pagamenti_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_pagamento = :ls_cod_pagamento and
       cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

select des_banca, cod_abi, cod_cab
into   :ls_des_banca, :ls_cod_abi, :ls_cod_cab
from   anag_banche_clien_for  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_banca_clien_for = :ls_cod_banca_clien_for;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_banca)
end if

select des_porto  
into   :ls_des_porto  
from   tab_porti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_porto = :ls_cod_porto and
       cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_resa = :ls_cod_resa and  
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

// EnMe 20/04/2012; aggiunto imballo
select 	des_imballo
into   		:ls_des_imballo
from   	tab_imballi
where  	cod_azienda = :s_cs_xx.cod_azienda and
       		cod_imballo = :ls_cod_imballo;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballo)
end if

select des_imballi
into   :ls_des_imballi_lingua  
from   tab_imballi_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_imballo = :ls_cod_imballo and  
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_imballi_lingua)
else
	ls_des_imballo = ls_des_imballi_lingua
end if
// -------------------------------------

select des_giro_consegna
into   :ls_des_giro_consegna
from   tes_giri_consegne
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_giro_consegna = :ls_cod_giro_consegna;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_giro_consegna)
end if

select des_operatore
into   :ls_des_operatore
from   tab_operatori
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_operatore = :ls_cod_operatore;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_operatore)
end if

select rag_soc_1
into   :ls_anag_vettori_rag_soc_1
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_vettore = :ls_cod_vettore;

if sqlca.sqlcode <> 0 then
	setnull(ls_anag_vettori_rag_soc_1)
end if

select tab_valute.des_valuta
into   :ls_des_valuta
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta = :ls_cod_valuta;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta)
end if

select des_valuta
into   :ls_des_valuta_lingua  
from   tab_valute_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_valuta = :ls_cod_valuta and  
       tab_valute_lingue.cod_lingua = :ls_cod_lingua ;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if
 
select des_tipo_anagrafica
into   :ls_des_tipo_anagrafica
from   tab_tipi_anagrafiche
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_tipo_anagrafica = :ls_cod_tipo_anagrafica ;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_anagrafica)
end if

setnull(ls_nota_testata_st_ord)
if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "STAMPA_ORD_PROD", ls_null, ldt_data_consegna_testata, ls_nota_testata_st_ord,ls_nota_piede_st_ord, ls_nota_video_st_ord ) < 0 then
	g_mb.error(ls_nota_testata_st_ord)
else
	if len(ls_nota_video_st_ord) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video_st_ord)
end if



declare cu_dettagli cursor for 
	select   det_ord_ven.prog_riga_ord_ven,
	         det_ord_ven.cod_tipo_det_ven, 
				det_ord_ven.cod_misura, 
				det_ord_ven.quan_ordine, 
				det_ord_ven.prezzo_vendita, 
				det_ord_ven.sconto_1, 
				det_ord_ven.sconto_2, 
				det_ord_ven.imponibile_iva_valuta, 
				det_ord_ven.data_consegna, 
				det_ord_ven.nota_dettaglio, 
				det_ord_ven.cod_prodotto, 
				det_ord_ven.des_prodotto,
				det_ord_ven.fat_conversione_ven,
				det_ord_ven.num_riga_appartenenza,
				det_ord_ven.quantita_um,
				det_ord_ven.prezzo_um,
				nota_prodotto,
				quan_evasa,
				flag_evasione,
				provvigione_1,
				dim_x,
				dim_y
	from     det_ord_ven 
	where    det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				det_ord_ven.anno_registrazione = :ai_anno_ord_ven and 
				det_ord_ven.num_registrazione = :ai_num_ord_ven
	order by det_ord_ven.cod_azienda, 
				det_ord_ven.anno_registrazione, 
				det_ord_ven.num_registrazione,
				det_ord_ven.prog_riga_ord_ven;

open cu_dettagli;

do while 0 = 0
   fetch cu_dettagli into :ll_prog_riga_ord_ven,
	                       :ls_cod_tipo_det_ven, 
								  :ls_cod_misura, 
								  :ld_quan_ordine, 
								  :ld_prezzo_vendita,   
								  :ld_sconto_1, 
								  :ld_sconto_2, 
								  :ld_val_riga, 
								  :ldt_data_consegna, 
								  :ls_nota_dettaglio, 
								  :ls_cod_prodotto, 
								  :ls_des_prodotto,
								  :ld_fat_conversione_ven,
								  :ll_num_riga_appartenenza,
								  :ld_quantita_um,
								  :ld_prezzo_um,
								  :ls_nota_prodotto,
								  :ld_quan_evasa,
								  :ls_evasione,
								  :ld_provvigione_1,
								  :ld_dim_x_riga,
								  :ld_dim_y_riga;
   if sqlca.sqlcode <> 0 then exit

	if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then		// potrebbe essere un prodotto modello
		select flag_tipo_report
		into   :ls_flag_tipo_report
		from   tab_flags_configuratore
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_modello = :ls_cod_prodotto;
		if sqlca.sqlcode = 0 and ls_flag_tipo_report = "A" then							// è un prodotto modello
			ll_riga_appartenenza_padre = ll_prog_riga_ord_ven
			lb_modello = true
			lb_test = true
		else
			lb_test = false
			// non è un modello e ha la riga appartenenza = 0; allora non va nel report tipo A
		end if
	else
		if ll_num_riga_appartenenza = ll_riga_appartenenza_padre then
			lb_modello = false
			lb_test = true
		else
			lb_modello= false
			lb_test = false
		end if
	end if

	if not lb_evasi then
		
		if ls_evasione = "E" then
			continue
		end if
		
		ld_quan_ordine -= ld_quan_evasa
		
		ld_quantita_um = ld_quan_ordine * ld_fat_conversione_ven
		
	end if

	if lb_test then																					// verifico se inserire la riga oppure no
		dw_report_ord_ven_tende.insertrow(0)
		dw_report_ord_ven_tende.setrow(dw_report_ord_ven_tende.rowcount())
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "formato", ls_formato)
		
	// inzializzo variabili comp_det_ord_ven
		setnull(ls_cod_prodotto_finito)
		setnull(ls_cod_modello)
		ld_dim_x =0
		ld_dim_y =0
		ld_dim_z =0
		ld_dim_t =0
		setnull(ls_cod_tessuto)
		setnull(ls_cod_non_a_magazzino)
		setnull(ls_cod_tessuto_mantovana)
		setnull(ls_cod_colore_mantovana)
		setnull(ls_flag_tipo_mantovana)
		ld_altezza_mantovana = 0
		setnull(ls_flag_cordolo)
		setnull(ls_flag_passamaneria)
		setnull(ls_cod_verniciatura)
		setnull(ls_cod_comando)
		setnull(ls_pos_comando)
		ld_altezza_asta = 0
		setnull(ls_cod_tipo_supporto)
		setnull(ls_flag_luce_finita)
		setnull(ls_colore_passamaneria)
		setnull(ls_colore_cordolo)
		setnull(ls_des_vernice_fc) 
		setnull(ls_des_vernic_fc_2) 
		setnull(ls_des_vernic_fc_3) 
		
	
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "parametri_azienda_stringa", ls_stringa)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "parametri_azienda_stringa_euro", ls_stringa_euro)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_anno_registrazione", ai_anno_ord_ven)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_num_registrazione", ai_num_ord_ven)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_tipo_ord_ven", ls_cod_tipo_ord_ven)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_data_registrazione", ldt_data_registrazione)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_cliente", ls_cod_cliente)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_valuta", ls_cod_valuta)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cambio_ven", ld_cambio_ven)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_valute_des_valuta", ls_des_valuta)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_valute_lingue_des_valuta", ls_des_valuta_lingua)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_pagamento", ls_cod_pagamento)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_sconto", ld_sconto)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_banca_clien_for", ls_cod_banca_clien_for)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_num_ord_cliente", ls_num_ord_cliente)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_data_ord_cliente", ldt_data_ord_cliente)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_rag_soc_1", ls_rag_soc_1)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_rag_soc_2", ls_rag_soc_2)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_indirizzo", ls_indirizzo)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_localita", ls_localita)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_frazione", ls_frazione)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cap", ls_cap)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_provincia", ls_provincia)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_nota_testata", ls_nota_testata)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_nota_piede", ls_nota_piede)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_porto", ls_cod_porto)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_resa", ls_cod_resa)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_des_operatore", ls_des_operatore)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_vettori_rag_soc_1", ls_anag_vettori_rag_soc_1)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_data_pronto", ldt_data_pronto)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_data_consegna", ldt_data_consegna_testata)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_des_giro", ls_des_giro_consegna)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_num_colli", ld_num_colli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_peso", ld_peso_lordo)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_tot_val_ordine", ld_tot_val_ordine)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_indirizzo", ls_indirizzo_cli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_localita", ls_localita_cli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_frazione", ls_frazione_cli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_cap", ls_cap_cli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_provincia", ls_provincia_cli)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_partita_iva", ls_partita_iva)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_fax", ls_fax)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_telefono", ls_telefono)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_des_deposito", ls_des_deposito)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_tipi_ord_ven_des_tipo_ord_ven", ls_des_tipo_ord_ven)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_pagamenti_sconto", ld_sconto_pagamento)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_banche_clien_for_des_banca", ls_des_banca)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_banche_clien_for_cod_abi", ls_cod_abi)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_banche_clien_for_cod_cab", ls_cod_cab)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "flag_tassativo", ls_flag_tassativo)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "alias_cliente", ls_alias_cliente)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "anag_clienti_des_tipo_anagrafica", ls_des_tipo_anagrafica)
		
		//qualcuno in passato ha usato questo campo sulla dw per metterci la nota fissa produzione
		//allora ho inserito sulla dw anche il campo rif_interscambio_2 per metterci riferimento interscambio vero e proprio
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "rif_interscambio",ls_nota_testata_st_ord)
		
		try
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "rif_interscambio_2",ls_rif_interscambio)
		catch (runtimeerror err)
		end try
		
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(),"rif_vs_ordine", ls_rif_vs_ordine)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_imballi_des_imballo",ls_des_imballo)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_provvigione_1", ld_provvigione_1)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_agente_1", ls_cod_agente_1)
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_cod_agente_2", ls_cod_agente_2)
	
		select tab_tipi_det_ven.flag_stampa_ordine  
		into   :ls_flag_stampa_ordine  
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
		if sqlca.sqlcode <> 0 then
			setnull(ls_flag_stampa_ordine)
		end if
	
		if ls_flag_stampa_ordine = 'S' then
			select des_prodotto, cod_misura_mag
			into   :ls_des_prodotto_anag, :ls_cod_misura_mag
			from   anag_prodotti  
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_anag)
			end if
			

			// *** Michela 07/12/2007: se esiste il parametro aziendale DTL ed è a SI allora non visualizzo la descrizione in lingua
			
			if ls_flag = "S" then
				setnull(ls_des_prodotto_lingua)
			else			
		
				select anag_prodotti_lingue.des_prodotto  
				into   :ls_des_prodotto_lingua  
				from   anag_prodotti_lingue  
				where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
						 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
				
				if sqlca.sqlcode <> 0 then
					setnull(ls_des_prodotto_lingua)
				else
					setnull(ls_des_prodotto)
					setnull(ls_des_prodotto_anag)
				end if
				
			end if
	
			select tab_prod_clienti.cod_prod_cliente  
			into   :ls_cod_prod_cliente  
			from   tab_prod_clienti  
			where  tab_prod_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_prod_clienti.cod_prodotto = :ls_cod_prodotto and   
					 tab_prod_clienti.cod_cliente = :ls_cod_cliente;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_cod_prod_cliente)
			end if
	
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_cod_misura", ls_cod_misura)
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_quan_ordine", ld_quan_ordine)
			if ls_cod_misura_mag <> ls_cod_misura then
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_quan_totale", ld_quantita_um)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_prezzo_vendita", ld_prezzo_um)
			else
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_quan_totale", ld_quan_ordine)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_prezzo_vendita", ld_prezzo_vendita)
			end if				
			
			ls_sconti = ""
			if ld_sconto_1 > 0 then
				if dec(int(ld_sconto_1)) <> ld_sconto_1 then
					ls_sconti = string(ld_sconto_1,"#0,0#")
				else
					ls_sconti = string(ld_sconto_1,"#0")
				end if
			end if				
			if ld_sconto_2 > 0 then
				if dec(int(ld_sconto_2)) <> ld_sconto_2 then
					ls_sconti = ls_sconti + " + " + string(ld_sconto_2,"#0,0#")
				else
					ls_sconti = ls_sconti + " + " + string(ld_sconto_2,"#0")
				end if
			end if				
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_sconti", ls_sconti)
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_val_riga", ld_val_riga)
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_data_consegna", ldt_data_consegna)
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_cod_prodotto", ls_cod_prodotto)
			if ll_num_riga_appartenenza = 0 or isnull(ll_num_riga_appartenenza) then
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_num_riga", ll_prog_riga_ord_ven)
			else			
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_num_riga", ll_num_riga_appartenenza)
			end if
			if not isnull(ls_des_prodotto_lingua) then
				ls_descrizione_riga = ls_des_prodotto_lingua
			elseif not isnull(ls_des_prodotto) then
				ls_descrizione_riga = ls_des_prodotto
			else
				ls_descrizione_riga = ls_des_prodotto_anag
			end if
	

			// ------------  leggo e stampo comp_det_ord_ven ----------------------------------------------------------
			SELECT cod_prod_finito,        cod_modello,    dim_x,    dim_y,    dim_z,    dim_t,    cod_tessuto,    cod_non_a_magazzino,    cod_tessuto_mant,         cod_mant_non_a_magazzino, flag_tipo_mantovana,    alt_mantovana,        flag_cordolo,    flag_passamaneria,    cod_verniciatura,    cod_comando,    pos_comando,    alt_asta,        cod_tipo_supporto,    flag_misura_luce_finita, note,    colore_passamaneria,    colore_cordolo,    des_vernice_fc,    des_comando_1,     cod_comando_2,     des_comando_2,    flag_allegati, cod_verniciatura_2, flag_fc_vernic_2, des_vernic_fc_2, cod_verniciatura_3, flag_fc_vernic_3, des_vernic_fc_3
			 INTO :ls_cod_prodotto_finito,:ls_cod_modello,:ld_dim_x,:ld_dim_y,:ld_dim_z,:ld_dim_t,:ls_cod_tessuto,:ls_cod_non_a_magazzino,:ls_cod_tessuto_mantovana,:ls_cod_colore_mantovana, :ls_flag_tipo_mantovana,:ld_altezza_mantovana,:ls_flag_cordolo,:ls_flag_passamaneria,:ls_cod_verniciatura,:ls_cod_comando,:ls_pos_comando,:ld_altezza_asta,:ls_cod_tipo_supporto,:ls_flag_luce_finita,    :ls_note,:ls_colore_passamaneria,:ls_colore_cordolo,:ls_des_vernice_fc, :ls_des_comando_1, :ls_cod_comando_2, :ls_des_comando_2,:ls_flag_allegati , :ls_cod_verniciatura_2, :ls_flag_fc_vernic_2, :ls_des_vernic_fc_2, :ls_cod_verniciatura_3, :ls_flag_fc_vernic_3, :ls_des_vernic_fc_3
			 FROM comp_det_ord_ven  
			WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
					anno_registrazione = :ai_anno_ord_ven AND  
					num_registrazione = :ai_num_ord_ven AND  
					prog_riga_ord_ven = :ll_prog_riga_ord_ven ;
			if sqlca.sqlcode = 0 then
				if not isnull(ls_cod_prodotto_finito) and len(ls_cod_prodotto_finito) > 0 then
					if isnull(ls_descrizione_riga) then ls_descrizione_riga = ""
					ls_descrizione_riga = ls_cod_prodotto_finito + "~r~n" + ls_descrizione_riga
				end if
	//			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_prodotto_finito", ls_cod_prodotto_finito)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_x", ld_dim_x)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_y", ld_dim_y)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_z", ld_dim_z)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_t", ld_dim_t)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_tessuto", ls_cod_tessuto)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_colore_tessuto", ls_cod_non_a_magazzino)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_tessuto_mant", ls_cod_colore_mantovana)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "flag_tipo_mantovana", ls_flag_tipo_mantovana)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "altezza_mantovana", ld_altezza_mantovana)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "colore_cordolo", ls_colore_cordolo)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "colore_passamaneria", ls_colore_passamaneria)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_comando", ls_cod_comando)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "pos_comando", ls_pos_comando)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "altezza_asta", ld_altezza_asta)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "flag_luce_finita", ls_flag_luce_finita)
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "flag_allegati", ls_flag_allegati)
				if not isnull(ls_cod_tipo_supporto) and ls_cod_tipo_supporto <> "" then
					select cod_comodo
					into   :ls_abbreviazione_supporto
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_tipo_supporto;
					if sqlca.sqlcode = 0 then
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_tipo_supporto", ls_abbreviazione_supporto)
					else
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_tipo_supporto", "")
					end if
				end if
				if not isnull(ls_cod_verniciatura) and ls_cod_verniciatura <> "" then
					select cod_comodo
					into   :ls_abbreviazione_verniciatura
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_verniciatura;
					if sqlca.sqlcode = 0 then
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_verniciatura", ls_abbreviazione_verniciatura)
					else
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_verniciatura", "")
					end if
				end if
				
				// EnMe 20-04-2012 verniciatura 2 e 3
				if not isnull(ls_cod_verniciatura_2) and ls_cod_verniciatura_2 <> "" then
					select cod_comodo
					into   :ls_abbreviazione_verniciatura
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_verniciatura_2;
					if sqlca.sqlcode = 0 then
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_verniciatura_2", ls_abbreviazione_verniciatura)
					else
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_verniciatura_2", "")
					end if
				end if
				if not isnull(ls_cod_verniciatura_3) and ls_cod_verniciatura_3 <> "" then
					select cod_comodo
					into   :ls_abbreviazione_verniciatura
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_verniciatura_3;
					if sqlca.sqlcode = 0 then
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_verniciatura_3", ls_abbreviazione_verniciatura)
					else
						dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "cod_verniciatura_3", "")
					end if
				end if
				// ------------------------------------------
				
				select abbreviazione_comando
				into   :ls_abbreviazione_comando
				from   tab_comandi
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_comando = :ls_cod_comando;
				if sqlca.sqlcode <> 0 then
					setnull(ls_abbreviazione_comando)
				end if
				dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "abbreviazione_comando", ls_abbreviazione_comando)
				
				if not isnull(ls_cod_tessuto) and ls_cod_tessuto <> "" then
					select des_prodotto
					into   :ls_des_tessuto
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_tessuto;
					if sqlca.sqlcode = 0 and not isnull(ls_des_tessuto) then
						if isnull(ls_descrizione_riga) then ls_descrizione_riga = ""
						ls_descrizione_riga = ls_descrizione_riga + ", " + ls_des_tessuto
					end if
				end if
				
				
				//-----------------------------------------------------------------------------------
				//sia che VGTOT>0 o nullo accodo la scritta relativa al VGTOT
				if g_str.isnotempty(ls_cod_non_a_magazzino) then
				
					select val_gtot
					into :ld_val_gtot
					from tab_colori_tessuti
					where 	cod_azienda=:s_cs_xx.cod_azienda and
								cod_colore_tessuto=:ls_cod_non_a_magazzino;
					
					if isnull(ld_val_gtot) then
						ls_descrizione_riga += " VAL GTOT=<null>"
					else
						ls_descrizione_riga += " VAL GTOT="+string(ld_val_gtot, "###0.00##")
					end if
				end if
				//-----------------------------------------------------------------------------------
				
				
				if not isnull(ls_des_vernice_fc) and len(ls_des_vernice_fc) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_vernice_fc
				end if
				if not isnull(ls_des_comando_1) and len(ls_des_comando_1) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_comando_1
				end if
				if not isnull(ls_des_comando_2) and len(ls_des_comando_2) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_comando_2
				end if
				if not isnull(ls_des_vernic_fc_2) and len(ls_des_vernic_fc_2) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_vernic_fc_2
				end if
				if not isnull(ls_des_vernic_fc_3) and len(ls_des_vernic_fc_3) > 0 then
					ls_descrizione_riga = ls_descrizione_riga + " " + ls_des_vernic_fc_3
				end if
			else
				if not isnull(ls_cod_prodotto) then
					ls_descrizione_riga = ls_cod_prodotto + "~r~n" + ls_des_prodotto
				else
					ls_descrizione_riga = ls_des_prodotto
				end if
				if ld_dim_x_riga > 0 and not  isnull(ld_dim_x_riga) then dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_x", ld_dim_x_riga)
				if ld_dim_y_riga > 0 and not  isnull(ld_dim_y_riga) then dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "dim_y", ld_dim_y_riga)
				
			end if
			if not isnull(ls_nota_dettaglio) and len(ls_nota_dettaglio) > 0 then
				ls_descrizione_riga = ls_descrizione_riga + "~r~n" + ls_nota_dettaglio
			end if
			
			if not isnull(ls_nota_prodotto) then
				ls_descrizione_riga += "~r~n" + ls_nota_prodotto
			end if
			
			dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "det_ord_ven_des_prodotto", ls_descrizione_riga)
		end if
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_porti_des_porto", ls_des_porto)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_rese_des_resa", ls_des_resa)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_tot_val_ordine", ld_tot_val_ordine)
		
		dw_report_ord_ven_tende.setitem(dw_report_ord_ven_tende.getrow(), "tes_ord_ven_tot_val_ordine_euro", ld_tot_val_ordine_euro)
		
	end if
//	dw_report_ord_ven_tende.object.tes_ord_ven_tot_val_ordine_valuta_t.text = "Totale in " + ls_cod_valuta

loop

close cu_dettagli;

dw_report_ord_ven_tende.resetupdate()

return 0
end function

public function string wf_imposta_logo_personalizzato (string as_logo_etichetta, string as_cod_cliente);string			ls_logo, ls_cod_cliente


//se vuoto esci, finito
if g_str.isempty(as_logo_etichetta) or g_str.isempty(as_cod_cliente) then
	return ""
end if

ls_logo = s_cs_xx.volume + s_cs_xx.risorse + "loghi_clienti\"+as_cod_cliente+"\"+as_logo_etichetta

if fileexists(ls_logo) then
	return ls_logo
else
	//file inesistente
	return ""
end if
end function

event pc_setwindow;call super::pc_setwindow;string					ls_path_logo_1, ls_modify, ls_sql, ls_cod_operatore, ls_flag_stampati
datetime				ldt_data_registrazione
long					ll_i
integer				li_vuoto[]
windowobject		lw_oggetti[], lw_vuoto[]


dw_report_ord_ven_sfuso.ib_dettaglio = true
dw_report_ord_ven_tecniche.ib_dettaglio = true
dw_report_ord_ven_tende.ib_dettaglio = true


iuo_fido = create uo_fido_cliente


// stefanop 09/01/2012 : cambiato il parametro del logo da LO4 a LO5
select parametri_azienda.stringa
into   :ls_path_logo_1
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO5';

ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"

dw_report_ord_ven_tende.modify(ls_modify)
dw_report_ord_ven_sfuso.modify(ls_modify)
dw_report_ord_ven_tecniche.modify(ls_modify)

save_on_close(c_socnosave)

lw_oggetti[1] = dw_ricerca
lw_oggetti[2] = dw_elenco_operatori_selezionati
lw_oggetti[3] = cbx_select_all
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_lista
lw_oggetti[2] = cb_stampa
dw_folder.fu_assigntab(2, "Lista Ordini", lw_oggetti[])

dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)

wf_carica_operatori()

event post ue_post_open()


end event

on w_report_ord_ven_op_euro.create
int iCurrent
call super::create
this.dw_lista=create dw_lista
this.cb_stampa=create cb_stampa
this.dw_folder=create dw_folder
this.dw_elenco_operatori_selezionati=create dw_elenco_operatori_selezionati
this.dw_report_ord_ven_sfuso=create dw_report_ord_ven_sfuso
this.dw_report_ord_ven_tecniche=create dw_report_ord_ven_tecniche
this.dw_report_ord_ven_tende=create dw_report_ord_ven_tende
this.dw_ricerca=create dw_ricerca
this.cbx_select_all=create cbx_select_all
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_lista
this.Control[iCurrent+2]=this.cb_stampa
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_elenco_operatori_selezionati
this.Control[iCurrent+5]=this.dw_report_ord_ven_sfuso
this.Control[iCurrent+6]=this.dw_report_ord_ven_tecniche
this.Control[iCurrent+7]=this.dw_report_ord_ven_tende
this.Control[iCurrent+8]=this.dw_ricerca
this.Control[iCurrent+9]=this.cbx_select_all
end on

on w_report_ord_ven_op_euro.destroy
call super::destroy
destroy(this.dw_lista)
destroy(this.cb_stampa)
destroy(this.dw_folder)
destroy(this.dw_elenco_operatori_selezionati)
destroy(this.dw_report_ord_ven_sfuso)
destroy(this.dw_report_ord_ven_tecniche)
destroy(this.dw_report_ord_ven_tende)
destroy(this.dw_ricerca)
destroy(this.cbx_select_all)
end on

event close;call super::close;

destroy iuo_fido
end event

type dw_lista from datawindow within w_report_ord_ven_op_euro
integer x = 41
integer y = 224
integer width = 3662
integer height = 1996
integer taborder = 100
string title = "none"
string dataobject = "d_report_ord_ven_op_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type cb_stampa from commandbutton within w_report_ord_ven_op_euro
integer x = 3177
integer y = 116
integer width = 279
integer height = 84
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;integer li_copie, ll_i, ll_num_ordini, li_anno_ordine

string  ls_messaggio, ls_temp_filename[], ls_flag_stampa_definitiva

long    ll_job, ll_y, ll_index, ll_num_ordine

uo_calcola_documento_euro luo_doc

ls_flag_stampa_definitiva = dw_ricerca.getitemstring(1, "flag_definitiva")
li_copie = dw_ricerca.getitemnumber(1, "num_copie")
if isnull(li_copie) then li_copie = 0

if li_copie <=0 then
	g_mb.warning("Attenzione: non hai impostato il numero copie da stampare!")
	return
end if

ll_num_ordini = dw_lista.rowcount()			//upperbound(il_num_registrazione) - 1
if ll_num_ordini < 1 then
	g_mb.warning("Nessun ordine da stampare!")
	return
end if

// printsetup()

for ll_index = 1 to ll_num_ordini
	
	li_anno_ordine = dw_lista.getitemnumber(ll_index, "anno_registrazione")
	ll_num_ordine = dw_lista.getitemnumber(ll_index, "num_registrazione")
	
	luo_doc = create uo_calcola_documento_euro

	if luo_doc.uof_calcola_documento(li_anno_ordine, ll_num_ordine, "ord_ven",ls_messaggio) <> 0 then
		rollback;
		g_mb.messagebox("APICE",ls_messaggio)
	else
		commit;
	end if

	destroy luo_doc
	
	//dw_report_ord_ven_tende.show()
	//dw_report_ord_ven_sfuso.hide()
	//dw_report_ord_ven_tecniche.hide()
	dw_report_ord_ven_tende.setfocus()
	wf_report_tende(li_anno_ordine, ll_num_ordine)
	
	//dw_report_ord_ven_tende.hide()
	//dw_report_ord_ven_sfuso.hide()
	//dw_report_ord_ven_tecniche.show()
	dw_report_ord_ven_tecniche.setfocus()
	wf_report_tecniche(li_anno_ordine, ll_num_ordine)

	//dw_report_ord_ven_tende.hide()
	//dw_report_ord_ven_sfuso.show()
	//dw_report_ord_ven_tecniche.hide()
	dw_report_ord_ven_sfuso.setfocus()
	wf_report_sfuso(li_anno_ordine, ll_num_ordine)
	
	wf_estrai_blob_axioma(li_anno_ordine, ll_num_ordine, ls_temp_filename[])
	
	uo_shellexecute luo_print
	luo_print = create uo_shellexecute
	
	for ll_i =1 to li_copie
		if dw_report_ord_ven_tende.rowcount() > 0 or dw_report_ord_ven_tecniche.rowcount() > 0 or dw_report_ord_ven_sfuso.rowcount() > 0 then
			if dw_report_ord_ven_tende.rowcount() > 0 then
				dw_report_ord_ven_tende.print()
	
				for ll_y = 1 to upperbound(ls_temp_filename)
					if len(ls_temp_filename[ll_y]) > 0 and fileexists(ls_temp_filename[ll_y]) then
						luo_print.uof_print( handle(parent), ls_temp_filename[ll_y], PrintGetPrinter() )
					end if
				next
			end if
			if dw_report_ord_ven_tecniche.rowcount() > 0 then
				dw_report_ord_ven_tecniche.print()
			end if
			if dw_report_ord_ven_sfuso.rowcount() > 0 then
				dw_report_ord_ven_sfuso.print()
			end if
		end if
	next
	
	destroy luo_print
	
	if ls_flag_stampa_definitiva="S" then
		update tes_ord_ven
		set flag_stampato = 'S' 
		where cod_azienda = :s_cs_xx.cod_azienda and
		      anno_registrazione = :li_anno_ordine and
		      num_registrazione = :ll_num_ordine ;
		if sqlca.sqlcode <> 0 then
			rollback;
		else
			commit;
		end if
	end if
next
end event

type dw_folder from u_folder within w_report_ord_ven_op_euro
integer width = 3721
integer height = 2260
integer taborder = 70
boolean border = false
end type

event po_tabclicked;call super::po_tabclicked;string			ls_flag_operatori
long			ll_row



if upper(i_SelectedTabName) = "RICERCA" then

	ll_row = dw_ricerca.getrow()

	if ll_row>0 then
	else
		return
	end if
	
	ls_flag_operatori = dw_ricerca.getitemstring(ll_row, "flag_operatori")
	
	if ls_flag_operatori = "N" then		
		dw_elenco_operatori_selezionati.visible = false
	else
		dw_elenco_operatori_selezionati.visible = true
	end if
	
	cbx_select_all.visible = dw_elenco_operatori_selezionati.visible

end if
end event

type dw_elenco_operatori_selezionati from datawindow within w_report_ord_ven_op_euro
boolean visible = false
integer x = 119
integer y = 964
integer width = 1851
integer height = 1252
integer taborder = 60
boolean bringtotop = true
string title = "none"
string dataobject = "d_elenco_operatori_selezionati"
boolean vscrollbar = true
boolean livescroll = true
end type

event itemchanged;

choose case dwo.name
	case "flag_selezionato"
		if data="N" then
			//hai deselezionato l'operatore della riga: metti il checkbox "Seleziona Tutti a N"
			cbx_select_all.checked = false
		end if
		
end choose
end event

type dw_report_ord_ven_sfuso from uo_cs_xx_dw within w_report_ord_ven_op_euro
boolean visible = false
integer x = 2409
integer y = 1708
integer width = 1042
integer height = 428
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_report_ord_ven_sfuso_euro"
boolean border = false
boolean livescroll = true
end type

type dw_report_ord_ven_tecniche from uo_cs_xx_dw within w_report_ord_ven_op_euro
boolean visible = false
integer x = 2423
integer y = 1212
integer width = 1042
integer height = 428
integer taborder = 90
boolean bringtotop = true
string dataobject = "d_report_ord_ven_tecniche_euro"
boolean border = false
boolean livescroll = true
end type

type dw_report_ord_ven_tende from uo_cs_xx_dw within w_report_ord_ven_op_euro
boolean visible = false
integer x = 2423
integer y = 712
integer width = 1042
integer height = 428
integer taborder = 60
boolean bringtotop = true
string dataobject = "d_report_ord_ven_tende_euro"
boolean border = false
boolean livescroll = true
end type

type dw_ricerca from u_dw_search within w_report_ord_ven_op_euro
integer x = 14
integer y = 124
integer width = 2601
integer height = 720
integer taborder = 100
boolean bringtotop = true
string dataobject = "d_report_ord_ven_op_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;string			ls_null, ls_errore
datetime		ldt_null
integer		li_number

choose case dwo.name
	case "b_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca,"cod_cliente")
	
	
	case "b_cerca"
		setpointer(Hourglass!)
		li_number = wf_ricerca(ls_errore)
		if li_number <0 then
			g_mb.error(ls_errore)
		end if
		dw_folder.fu_selecttab(2)
		setpointer(Arrow!)
	
	
	case "b_annulla"
		setnull(li_number)
		setnull(ldt_null)
		setnull(ls_null)
		
		setitem(1, "anno_ordine", f_anno_esercizio())
		setitem(1, "num_ordine", li_number)
		
		setitem(1, "data_consegna_inizio", ldt_null)
		setitem(1, "data_consegna_fine", ldt_null)
		setitem(1, "data_registrazione_inizio", ldt_null)
		setitem(1, "data_registrazione_fine", ldt_null)
		
		setitem(1, "cod_cliente", ls_null)
		setitem(1, "flag_operatori", "N")
		setitem(1, "flag_stampati", "N")
		setitem(1, "flag_evasione", "T")
		//setitem(1, "flag_definitiva", "S")
		
	
end choose

end event

event itemchanged;call super::itemchanged;string			ls_null, ls_errore
datetime		ldt_null
integer		li_number

choose case dwo.name
	//----------------------------------------------------------------------------------------
	case "flag_operatori"
		if data = "N" then
			cbx_select_all.visible = false
		else
			cbx_select_all.visible = true
		end if
		
		dw_elenco_operatori_selezionati.visible = cbx_select_all.visible
	
end choose		




end event

type cbx_select_all from checkbox within w_report_ord_ven_op_euro
boolean visible = false
integer x = 1499
integer y = 876
integer width = 471
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Seleziona tutto"
boolean lefttext = true
end type

event clicked;
string ls_flag = "N"
long ll_i

if cbx_select_all.checked then ls_flag = "S"

for ll_i = 1 to dw_elenco_operatori_selezionati.rowcount()
	dw_elenco_operatori_selezionati.setitem(ll_i, "flag_selezionato", ls_flag)
next
end event

