﻿$PBExportHeader$w_report_pcklist.srw
$PBExportComments$Finestra Report Attrezzature Semplici
forward
global type w_report_pcklist from w_cs_xx_principale
end type
type dw_report from uo_std_dw within w_report_pcklist
end type
type cb_stampa from commandbutton within w_report_pcklist
end type
end forward

global type w_report_pcklist from w_cs_xx_principale
integer width = 3872
integer height = 2768
string title = "Stampa Colli Mancanti"
boolean center = true
dw_report dw_report
cb_stampa cb_stampa
end type
global w_report_pcklist w_report_pcklist

type variables

end variables

forward prototypes
public function integer wf_report (long fl_id, ref string fs_msg)
public function boolean wf_check_barcode (string fs_barcode_collo, long fl_max_row, ref long fl_riga)
public function string wf_prodotti (integer fi_anno_reg, long fl_num_reg, long fl_prog_riga)
public function integer wf_in_spedizione (long fl_id, datetime fdt_data_spedizione, ref datetime fdt_data_scansione, ref datetime fdt_ora_scansione, ref string fs_msg)
public function integer wf_scansionato (long fl_id, ref datetime fdt_data_spedizione, ref datetime fdt_data_scansione, ref datetime fdt_ora_scansione, string fs_msg)
end prototypes

public function integer wf_report (long fl_id, ref string fs_msg);datetime ldt_data_spedizione, ldt_data_scansione, ldt_ora_scansione

//leggi cosa è in spedizione
if wf_in_spedizione(fl_id, ldt_data_spedizione, ldt_data_scansione, ldt_ora_scansione,  fs_msg) < 0 then
	//in fs_msg il messaggio di errore
	return -1
end if

//poi leggi cosa è stato scansionato in tale data spedizione
if wf_scansionato(fl_id, ldt_data_spedizione, ldt_data_scansione, ldt_ora_scansione, fs_msg) < 0 then
	//in fs_msg il messaggio di errore
	return -1
end if


dw_report.sort()
dw_report.groupcalc()


return 1
end function

public function boolean wf_check_barcode (string fs_barcode_collo, long fl_max_row, ref long fl_riga);long ll_index
string ls_barcode_collo_dw

for ll_index=1 to fl_max_row
	ls_barcode_collo_dw = dw_report.getitemstring(ll_index, "barcode_collo")
	
	if ls_barcode_collo_dw=fs_barcode_collo then
		//collo già presente, quindi il collo scansionato con il palmare era effettivamente in spedizione
		
		//riga in cui è presente
		fl_riga = ll_index
		
		return true
	end if
	
next

return false
end function

public function string wf_prodotti (integer fi_anno_reg, long fl_num_reg, long fl_prog_riga);string ls_sql, ls_return, ls_appo
datastore lds_data
long ll_index, ll_tot

ls_sql = 	"select det_ord_ven.prog_riga_ord_ven,det_ord_ven.cod_prodotto,det_ord_ven.des_prodotto,anag_prodotti.des_prodotto "+&
			"from det_ord_ven "+&
			"join anag_prodotti on anag_prodotti.cod_azienda=det_ord_ven.cod_azienda and "+&
					"anag_prodotti.cod_prodotto=det_ord_ven.cod_prodotto "+&
			"where 	 det_ord_ven.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"det_ord_ven.anno_registrazione="+string(fi_anno_reg)+" and "+&
						"det_ord_ven.num_registrazione="+string(fl_num_reg)+" "
					
if fl_prog_riga>0 then ls_sql += "and det_ord_ven.prog_riga_ord_ven="+string(fl_prog_riga)+" "
						
ls_sql += 	"order by det_ord_ven.prog_riga_ord_ven "

if not f_crea_datastore(lds_data, ls_sql) then
	g_mb.error("Errore in creazione datastore prodotti ordine " + string(fi_anno_reg) +"/" + string(fl_num_reg))
	return ""
end if

ll_tot = lds_data.retrieve()

ls_return = ""
for ll_index=1 to ll_tot
	ls_appo = lds_data.getitemstring(ll_index, 2)
	
	if lds_data.getitemstring(ll_index, 3) <> "" and not  isnull(lds_data.getitemstring(ll_index, 3)) then
		ls_appo += " - " + lds_data.getitemstring(ll_index, 3)
		
	elseif lds_data.getitemstring(ll_index, 4) <> "" and not  isnull(lds_data.getitemstring(ll_index, 4)) then
		ls_appo +=  " - " + lds_data.getitemstring(ll_index, 4)
		
	end if
	
	if ll_index>1 then  ls_appo = "~r~n" + ls_appo
	
	ls_return += ls_appo
next

return ls_return
end function

public function integer wf_in_spedizione (long fl_id, datetime fdt_data_spedizione, ref datetime fdt_data_scansione, ref datetime fdt_ora_scansione, ref string fs_msg);uo_produzione 	luo_prod
long 				ll_barcode, ll_num_reg, ll_prog_riga, ll_tot, ll_index, ll_new, ll_index_2, ll_ret, ll_pos
integer			li_anno_reg
string 				ls_colli[], ls_colli_per_report[], ls_vuoto[], ls_appo
string				ls_sql, ls_barcode, ls_barcode_old, ls_cod_cliente

datastore 		lds_data

//leggo la data scansione e quella di spedizione
select top 1 data_invio_scansione, ora_invio_scansione, data_spedizione
into :fdt_data_scansione, :fdt_ora_scansione, :fdt_data_spedizione
from tab_confronto_carico
where 	cod_azienda=:s_cs_xx.cod_azienda and
			progressivo=:fl_id;

if sqlca.sqlcode<0 then
	fs_msg= "Errore in lettura data spedizione da tab_lista_carico_spedizioni "+sqlca.sqlerrtext
	return -1
end if

//leggo i dati che sono in spedizione in tale data
//faccio il distinct perchè eventuali barcode di testata si ripresenterebbero per ogni riga
ls_sql = 	"select distinct barcode_collo "+&
			"from tab_lista_carico_sped_colli "+&
			"where 	 cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"barcode in  (	select barcode "+&
												"from tab_lista_carico_spedizioni "+&
												"where cod_azienda = '"+s_cs_xx.cod_azienda+"' and "+&
															"data_spedizione='"+string(fdt_data_spedizione, s_cs_xx.db_funzioni.formato_data)+"' ) and "+&
						"progressivo in  (	select progressivo "+&
												"from tab_lista_carico_spedizioni "+&
												"where cod_azienda = '"+s_cs_xx.cod_azienda+"' and "+&
															"data_spedizione='"+string(fdt_data_spedizione, s_cs_xx.db_funzioni.formato_data)+"' ) "+&
			"order by barcode_collo "

if not f_crea_datastore(lds_data, ls_sql) then
	fs_msg= "Errore in creazione datastore tab_lista_carico_spedizioni"
	return -1
end if

ll_tot = lds_data.retrieve()
luo_prod = create uo_produzione

for ll_index=1 to ll_tot
	
	ls_barcode = lds_data.getitemstring(ll_index, 1)
	
	ll_pos = pos(ls_barcode, "-")
	
	if ll_pos>0 then
		ls_appo = left(ls_barcode, ll_pos - 1)
		if isnumber(ls_appo) then
			ll_barcode = long(ls_appo)
		else
			fs_msg = "Barcode "+ls_barcode + " non numerico!"
			return -1
		end if
	else
		fs_msg = "Manca il carattere separatore per il barcode "+ls_barcode
		return -1
	end if
	
	ls_appo = ""
	
	//recupero le info su ordine (e riga, se necessario), cliente
	setnull(ll_prog_riga)
	
	if luo_prod.uof_barcode_anno_reg(ll_barcode, li_anno_reg, ll_num_reg, ll_prog_riga, ls_appo, fs_msg) < 0  then
		//in fs_msg il messaggio di errore
		return -1
	end if
	
	if ll_prog_riga>0 then
	else
		ll_prog_riga=0
	end if
	
	select cod_cliente
	into :ls_cod_cliente
	from tes_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:li_anno_reg and
				num_registrazione=:ll_num_reg;
				
	if sqlca.sqlcode<0 then
		fs_msg= "Errore in lettura cliente ordine "+sqlca.sqlerrtext
		return -1
	end if
	
	ll_new = dw_report.insertrow(0)
		
	dw_report.setitem(ll_new, "barcode_collo", ls_barcode)
	dw_report.setitem(ll_new, "in_spedizione", 1)
	dw_report.setitem(ll_new, "cod_cliente", ls_cod_cliente)
	
	dw_report.setitem(ll_new, "anno_registrazione", li_anno_reg)
	dw_report.setitem(ll_new, "num_registrazione", ll_num_reg)
	dw_report.setitem(ll_new, "prog_riga_ord_ven", ll_prog_riga)
	
	dw_report.setitem(ll_new, "des_prodotto", wf_prodotti(li_anno_reg, ll_num_reg, ll_prog_riga))
	
	//dati fissi ---------------------
	dw_report.setitem(ll_new, "cod_azienda", s_cs_xx.cod_azienda)
	dw_report.setitem(ll_new, "id_confronto_carico", fl_id)
	dw_report.setitem(ll_new, "data_spedizione", fdt_data_spedizione)
	dw_report.setitem(ll_new, "data_invio_scansione", fdt_data_scansione)
	dw_report.setitem(ll_new, "ora_invio_scansione", fdt_ora_scansione)
	//----------------------------------
	
	
next

destroy luo_prod


return 1
end function

public function integer wf_scansionato (long fl_id, ref datetime fdt_data_spedizione, ref datetime fdt_data_scansione, ref datetime fdt_ora_scansione, string fs_msg);uo_produzione 	luo_prod
long 				ll_num_reg, ll_prog_riga, ll_tot, ll_index, ll_new, ll_index_2

integer			li_anno_reg

long				ll_pos, ll_barcode2, ll_rows, ll_riga_presente

string 				ls_barcode, ls_sql, ls_cod_cliente, ls_separatore, ls_barcode2, ls_cod_reparto

datastore 		lds_data


ls_separatore = "-"

//leggo quante righe sono presenti sul report
//è l'estremo superiore del controllo da fare se il collo era già in spedizione
ll_rows = dw_report.rowcount()

//leggo i dati che sono stati scansionati in tale ID
ls_sql = "select barcode "+&
			"from tab_confronto_carico "+&
			"where 	 cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"progressivo="+string(fl_id)+" "+&
			"order by barcode "

if not f_crea_datastore(lds_data, ls_sql) then
	fs_msg= "Errore in creazione datastore tab_confronto_carico"
	return -1
end if

ll_tot = lds_data.retrieve()
luo_prod = create uo_produzione

for ll_index=1 to ll_tot
	
	ls_barcode = lds_data.getitemstring(ll_index, 1)
	
	ll_pos = pos(ls_barcode, ls_separatore)
	if ll_pos>0 then
		
		ls_barcode2 = left(ls_barcode, ll_pos - 1)
		if isnumber(ls_barcode2) then
			ll_barcode2 = long(ls_barcode2)
		else
			//avvisa del problema e salta la riga
			g_mb.error("Il barcode "+ls_barcode2+" non è nel formato numerico corretto!")
			continue
		end if
		
	else
		//avvisa del problema e salta la riga
		g_mb.error("Il barcode "+ls_barcode+" non è nel formato corretto!")
		continue
	end if
	
	//"ll_barcode2"  con questo barcode devo risalire alla PK, al cliente, e al prodotto
	setnull(ll_prog_riga)
	
	if luo_prod.uof_barcode_anno_reg(ll_barcode2, li_anno_reg, ll_num_reg, ll_prog_riga, ls_cod_reparto, fs_msg)<0 then
		//in fs_msg il messaggio di errore
		return -1
	end if
	
	if ll_prog_riga>0 then
		//barcode di dettaglio
	else
		//barcode di testata
		ll_prog_riga = 0
	end if
	
	//verifica se tale codice barcode collo è già presente sul report
	//se true allora già esiste altrimenti inserisci la riga sul report
	if wf_check_barcode(ls_barcode, ll_rows, ll_riga_presente) then
		//gia esiste
		dw_report.setitem(ll_riga_presente, "da_palmare", 1)
	else
		//non esiste
		
		ll_new = dw_report.insertrow(0)
		
		dw_report.setitem(ll_new, "barcode_collo", ls_barcode)
		dw_report.setitem(ll_new, "da_palmare", 1)
		
		select cod_cliente
		into :ls_cod_cliente
		from tes_ord_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:li_anno_reg and
					num_registrazione=:ll_num_reg;
					
		if sqlca.sqlcode<0 then
			fs_msg= "Errore in lettura cliente ordine "+sqlca.sqlerrtext
			return -1
		end if
		
		
		dw_report.setitem(ll_new, "cod_cliente", ls_cod_cliente)
		
		dw_report.setitem(ll_new, "anno_registrazione", li_anno_reg)
		dw_report.setitem(ll_new, "num_registrazione", ll_num_reg)
		dw_report.setitem(ll_new, "prog_riga_ord_ven", ll_prog_riga)
		
		//dati fissi ---------------------
		dw_report.setitem(ll_new, "cod_azienda", s_cs_xx.cod_azienda)
		dw_report.setitem(ll_new, "id_confronto_carico", fl_id)
		dw_report.setitem(ll_new, "data_spedizione", fdt_data_spedizione)
		dw_report.setitem(ll_new, "data_invio_scansione", fdt_data_scansione)
		dw_report.setitem(ll_new, "ora_invio_scansione", fdt_ora_scansione)
		//----------------------------------
		
	end if

next

destroy luo_prod


return 1
end function

on w_report_pcklist.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.cb_stampa=create cb_stampa
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.cb_stampa
end on

on w_report_pcklist.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.cb_stampa)
end on

event pc_setwindow;call super::pc_setwindow;string ls_msg, ls_sql, ls_in_select
windowobject lw_oggetti[], lw_vuoto[]
datastore lds_data
long ll_index, ll_tot

datetime ldt_data_spedizione

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

ldt_data_spedizione = s_cs_xx.parametri.parametro_data_4

setnull(s_cs_xx.parametri.parametro_data_4)

this.title = "Packing List in spedizione il "+string(ldt_data_spedizione, "dd-MM-yyyy")

dw_report.object.datawindow.print.preview = "Yes"

//f_get_sum_colli_ordine_sped(2010, 132959, 0, ldt_data_spedizione)


dw_report.settransobject(sqlca)
dw_report.retrieve(s_cs_xx.cod_azienda, ldt_data_spedizione )




end event

type dw_report from uo_std_dw within w_report_pcklist
integer x = 27
integer y = 124
integer width = 3776
integer height = 2512
integer taborder = 20
string dataobject = "d_report_packing_list_2"
boolean vscrollbar = true
borderstyle borderstyle = stylebox!
end type

event ue_setup_dw;return
end event

type cb_stampa from commandbutton within w_report_pcklist
integer x = 41
integer y = 24
integer width = 402
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;
dw_report.print(true, true)

end event

