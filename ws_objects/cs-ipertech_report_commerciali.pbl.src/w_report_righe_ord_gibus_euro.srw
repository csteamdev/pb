﻿$PBExportHeader$w_report_righe_ord_gibus_euro.srw
$PBExportComments$REPORT RIGHE ORDINI
forward
global type w_report_righe_ord_gibus_euro from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_righe_ord_gibus_euro
end type
type cb_1 from commandbutton within w_report_righe_ord_gibus_euro
end type
type dw_selezione from uo_cs_xx_dw within w_report_righe_ord_gibus_euro
end type
type cb_reset from commandbutton within w_report_righe_ord_gibus_euro
end type
type dw_nazioni from uo_dddw_checkbox within w_report_righe_ord_gibus_euro
end type
type dw_pagamenti from uo_dddw_checkbox within w_report_righe_ord_gibus_euro
end type
end forward

global type w_report_righe_ord_gibus_euro from w_cs_xx_principale
integer x = 5
integer y = 4
integer width = 5207
integer height = 2884
string title = "Report Righe Ordini Clienti"
event ue_posizione ( )
dw_report dw_report
cb_1 cb_1
dw_selezione dw_selezione
cb_reset cb_reset
dw_nazioni dw_nazioni
dw_pagamenti dw_pagamenti
end type
global w_report_righe_ord_gibus_euro w_report_righe_ord_gibus_euro

type variables
private:
	string							is_sql_select, is_sql_select_gruppo, is_sql_groupy, is_sql_rda, is_sql_marginalita
	s_cs_xx_parametri			is_cs_xx_costo_ultimo, is_cs_xx_costo_medio
end variables

forward prototypes
public function integer wf_report_riepilogo_prod (string as_where, ref string as_errore)
public subroutine wf_imposta_sql_group ()
public function integer wf_situazione_prodotto (string as_cod_prodotto, long al_row, ref string as_errore)
public function integer wf_report_riepilogo_prod_2 (string as_where, ref string as_errore)
public function boolean wf_cache_prodotto_costo (string as_cod_prodotto, string as_tipo_report, ref decimal ad_costo)
public function decimal wf_costo_prodotto (string as_cod_prodotto, string as_tipo_report, string as_tipo_costo)
public function integer wf_report_riepilogo_prod_3 (string as_where, string as_tipo_report, string as_tipo_costo, ref string as_errore)
end prototypes

public function integer wf_report_riepilogo_prod (string as_where, ref string as_errore);
string					ls_sql, ls_cod_prodotto, ls_cod_tipo_politica_riordino, ls_filter, ls_text, ls_cod_deposito

long					ll_index, ll_tot


ls_cod_tipo_politica_riordino = dw_selezione.getitemstring(1,"cod_tipo_politica_riordino")

ls_sql = is_sql_select_gruppo + " and det_ord_ven.cod_azienda='"+s_cs_xx.cod_azienda+"' " + as_where

if ls_cod_tipo_politica_riordino<>"" and not isnull(ls_cod_tipo_politica_riordino) then
	ls_sql += " and anag_prodotti.cod_tipo_politica_riordino='"+ls_cod_tipo_politica_riordino+"' "
end if

ls_sql += " " + is_sql_groupy

dw_report.setsqlselect(ls_sql)

ll_tot = dw_report.retrieve()

if ll_tot < 0 then
	as_errore = "Errore durante la retrieve dei dati!"
	return -1
end if

ls_cod_deposito = dw_selezione.getitemstring(1,"cod_deposito")

//rielaborazione righe per determinare la giacenza totale, l'ordinato al fornitore e la disponibilità teorica
for ll_index=1 to ll_tot
	
	ls_cod_prodotto = dw_report.getitemstring(ll_index, "cod_prodotto")
	
	ls_text = "Elaborazione giacenze: articolo " + ls_cod_prodotto
	
	if ls_cod_deposito<>"" and not isnull(ls_cod_deposito) then
		ls_text += " in deposito '"+ls_cod_deposito+"'"
	else
		ls_text += " in TUTTI i depositi"
	end if
	
	ls_text += " ("+string(ll_index)+ " di " + string(ll_tot)+")"
	
	dw_selezione.object.log_t.text=ls_text
	
	if wf_situazione_prodotto(ls_cod_prodotto, ll_index, as_errore) < 0 then
		return -1
	end if
next


if dw_selezione.getitemstring(1,"tipo_report") = "R" and dw_selezione.getitemstring(1,"flag_visualizza_negativi") = "S" then
	ls_filter = "giacenza_totale < 0"
	dw_report.setfilter(ls_filter)
	dw_report.filter()
end if


return 0
end function

public subroutine wf_imposta_sql_group ();

datastore 			lds_data
long					ll_pos


lds_data = create datastore
lds_data.dataobject = "d_righe_ordini_gibus_riepilogo_prod"
lds_data.settransobject(sqlca)

is_sql_select_gruppo = lds_data.getsqlselect()
destroy lds_data


//-------------------------------------------------------
ll_pos = pos(is_sql_select_gruppo, "group by")
if ll_pos > 0 then
	is_sql_groupy = " " + mid(is_sql_select_gruppo, ll_pos, len(is_sql_select_gruppo))
	is_sql_select_gruppo = mid(is_sql_select_gruppo, 1, ll_pos -1) + " "
end if

lds_data = create datastore
lds_data.dataobject = "d_righe_ordini_gibus_riepilogo_prod_2"
lds_data.settransobject(sqlca)

is_sql_rda = lds_data.getsqlselect()
destroy lds_data

//-------------------------------------------------------
lds_data = create datastore
lds_data.dataobject = "d_righe_ordini_gibus_riepilogo_prod_3"
lds_data.settransobject(sqlca)

is_sql_marginalita = lds_data.getsqlselect()
destroy lds_data





end subroutine

public function integer wf_situazione_prodotto (string as_cod_prodotto, long al_row, ref string as_errore);datetime				ldt_data_rif

integer				li_return
						
uo_magazzino		luo_mag

string					ls_where, ls_error, ls_chiave[], ls_cod_deposito

dec{4}				ld_saldo_quan_inizio_anno, ld_prog_quan_entrata, ld_prog_quan_uscita, ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], &
						ld_quan_ordinata, 	ld_quan_impegnata, ld_quan_assegnata, ld_quan_in_spedizione, ld_quan_anticipi, ld_reale, ld_teorica, ld_giacenza

	
ls_cod_deposito = dw_selezione.getitemstring(1,"cod_deposito")
ldt_data_rif = datetime(today(), 00:00:00)

select			saldo_quan_inizio_anno, prog_quan_entrata, prog_quan_uscita,  
				quan_ordinata, quan_impegnata, quan_assegnata, quan_in_spedizione, quan_anticipi  
into			:ld_saldo_quan_inizio_anno, :ld_prog_quan_entrata, :ld_prog_quan_uscita,
				:ld_quan_ordinata, :ld_quan_impegnata, :ld_quan_assegnata, :ld_quan_in_spedizione, :ld_quan_anticipi  
from		anag_prodotti
where	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:as_cod_prodotto;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in lettura situazione del prodotto '"+as_cod_prodotto+"': "+sqlca.sqlerrtext
	return -1
end if


//ORDINATO A FORNITORE
dw_report.setitem(al_row, "ordinato_for", ld_quan_ordinata)

//GIACENZA
//ld_giacenza = ld_saldo_quan_inizio_anno + ld_prog_quan_entrata - ld_prog_quan_uscita
//dw_report.setitem(al_row, "giacenza_totale", ld_giacenza)

luo_mag = create uo_magazzino

if ls_cod_deposito <> "" and not isnull(ls_cod_deposito) then
	ls_where =  " and cod_deposito='" + ls_cod_deposito + "' "
else
	ls_where = ""
end if

li_return = luo_mag.uof_saldo_prod_date_decimal(	as_cod_prodotto, ldt_data_rif, ls_where, ld_quant_val, ls_error, "N", ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
destroy luo_mag

ld_giacenza = 0
if upperbound(ld_quant_val[]) >= 6 then
	ld_giacenza = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
end if
dw_report.setitem(al_row, "giacenza_totale", ld_giacenza)


//DISPONIBILITA'
ld_reale =ld_giacenza - ld_quan_impegnata - ld_quan_anticipi - ld_quan_assegnata - ld_quan_in_spedizione


//DISPONIBILITA' TEORICA
ld_teorica = ld_reale + ld_quan_ordinata //+ ld_quan_in_spedizione
dw_report.setitem(al_row, "disp_teorica", ld_teorica)

return 0

end function

public function integer wf_report_riepilogo_prod_2 (string as_where, ref string as_errore);
string					ls_sql, ls_cod_tipo_politica_riordino, ls_filter

long					ll_index, ll_tot

integer				li_flag_filtro_acquisti

datastore			lds_data


ls_cod_tipo_politica_riordino = dw_selezione.getitemstring(1,"cod_tipo_politica_riordino")
li_flag_filtro_acquisti = dw_selezione.getitemnumber(1,"flag_filtro_acquisti")

ls_sql = is_sql_rda + " where det_ord_ven.cod_azienda='"+s_cs_xx.cod_azienda+"' and det_ord_ven.cod_prodotto is not null and det_ord_ven.quan_ordine > 0 "

if ls_cod_tipo_politica_riordino<>"" and not isnull(ls_cod_tipo_politica_riordino) then
	ls_sql += " and anag_prodotti.cod_tipo_politica_riordino='"+ls_cod_tipo_politica_riordino+"' "
end if

ls_sql += as_where

ls_sql += " order by det_ord_ven.cod_prodotto,det_ord_ven.anno_registrazione,det_ord_ven.num_registrazione,det_ord_ven.prog_riga_ord_ven,"+&
							"det_rda.anno_registrazione,det_rda.num_registrazione,det_rda.prog_riga_rda,"+&
							"det_ord_acq.anno_registrazione,det_ord_acq.num_registrazione,det_ord_acq.prog_riga_ordine_acq "

dw_report.setsqlselect(ls_sql)

dw_report.setredraw(false)

ll_tot = dw_report.retrieve()

if ll_tot < 0 then
	dw_report.setredraw(true)
	as_errore = "Errore durante la retrieve dei dati!"
	return -1
end if

choose case li_flag_filtro_acquisti
	case 0
		//indifferente, quindi non impostare alcun filter
		ls_filter = ""
		
	case 1
		//Senza RDA
		ls_filter = "isnull(anno_rda) or anno_rda=0"
		
	case 2
		//Con RDA e senza Ordine Acquisto
		ls_filter = "anno_rda>0 and (isnull(anno_ord_acq) or anno_ord_acq=0)"
		
	case 3
		//Con Ordine Acquisto
		ls_filter =  "anno_ord_acq>0"
		
end choose

dw_report.setfilter(ls_filter)
dw_report.filter()

dw_report.setredraw(true)


return 0
end function

public function boolean wf_cache_prodotto_costo (string as_cod_prodotto, string as_tipo_report, ref decimal ad_costo);long				ll_index, ll_tot
dec{4}			ld_costo


if as_tipo_report = "U" then
	//su costo ultimo
	ll_tot = upperbound(is_cs_xx_costo_ultimo.parametro_s_1_a[])
	
	for ll_index=1 to ll_tot
		if as_cod_prodotto = is_cs_xx_costo_ultimo.parametro_s_1_a[ll_index] then
			//trovato
			ad_costo = is_cs_xx_costo_ultimo.parametro_d_1_a[ll_index]
			return true
		end if
	next
	
else
	//su costo medio
	ll_tot = upperbound(is_cs_xx_costo_medio.parametro_s_1_a[])
	
	for ll_index=1 to ll_tot
		if as_cod_prodotto = is_cs_xx_costo_medio.parametro_s_1_a[ll_index] then
			//trovato
			ad_costo = is_cs_xx_costo_medio.parametro_d_1_a[ll_index]
			return true
		end if
	next
end if

return false
end function

public function decimal wf_costo_prodotto (string as_cod_prodotto, string as_tipo_report, string as_tipo_costo);
dec{4}		ld_costo, ld_qta, ld_imponibile
datastore	lds_data
string			ls_sql, ls_errore
boolean		lb_in_cache
long			ll_new, ll_index, ll_tot
datetime		ldt_data_rif, ldt_oggi
date			ldd_data_rif


//as_tipo_costo vale O oppure D (da ordine acquisto o da ddt di acquisto)


//verifica prima nella cahe, se c'è già il costo ultimo allora non fare la select
lb_in_cache = wf_cache_prodotto_costo(as_cod_prodotto, as_tipo_report, ld_costo)

if not lb_in_cache then
	ld_costo = 0

	if as_tipo_report = "U" then
		//ultimo costo acquisto

		if as_tipo_costo="D" then
			//----------------------------------------------------------------------------------------
			//da ddt
			//----------------------------------------------------------------------------------------

			//prendo anno e numero dell'ultimo ddt di acquisto del codice prodotto
			ls_sql = "select top 1 "+&
							"tes_bol_acq.anno_bolla_acq,"+&
							"tes_bol_acq.num_bolla_acq,"+&
							"det_bol_acq.prog_riga_bolla_acq,"+&
							"tes_bol_acq.data_registrazione,"+&
							"det_bol_acq.quan_arrivata,"+&
							"det_bol_acq.imponibile_iva "+&
						 "from tes_bol_acq "+&
						"join det_bol_acq on	det_bol_acq.cod_azienda=tes_bol_acq.cod_azienda and "+&
													"det_bol_acq.anno_bolla_acq=tes_bol_acq.anno_bolla_acq and "+&
													"det_bol_acq.num_bolla_acq=tes_bol_acq.num_bolla_acq "+&
						"where tes_bol_acq.cod_azienda='"+s_cs_xx.cod_azienda+"' and det_bol_acq.imponibile_iva>=0 and "+&
								 "det_bol_acq.flag_blocco<>'S' and tes_bol_acq.flag_blocco<>'S' and "+&
								"det_bol_acq.cod_prodotto = '"+as_cod_prodotto+"' and det_bol_acq.quan_arrivata>0 "+&
						"order by tes_bol_acq.data_registrazione desc,det_bol_acq.prog_riga_bolla_acq desc "
		else
			//----------------------------------------------------------------------------------------
			//da ordine acquisto
			//----------------------------------------------------------------------------------------
			ls_sql = "select top 1 "+&
							"tes_ord_acq.anno_registrazione,"+&
							"tes_ord_acq.num_registrazione,"+&
							"det_ord_acq.prog_riga_ordine_acq,"+&
							"tes_ord_acq.data_registrazione,"+&
							"det_ord_acq.quan_ordinata,"+&
							"det_ord_acq.imponibile_iva "+&
						"from tes_ord_acq "+&
						"join det_ord_acq on	det_ord_acq.cod_azienda=tes_ord_acq.cod_azienda and "+&
												"det_ord_acq.anno_registrazione=tes_ord_acq.anno_registrazione and "+&
												"det_ord_acq.num_registrazione=tes_ord_acq.num_registrazione "+&
						"where tes_ord_acq.cod_azienda='"+s_cs_xx.cod_azienda+"' and det_ord_acq.imponibile_iva>=0 and "+&
												"det_ord_acq.flag_blocco<>'S' and tes_ord_acq.flag_blocco<>'S' and "+&
												"det_ord_acq.cod_prodotto = '"+as_cod_prodotto+"' and "+&
									  			"det_ord_acq.quan_ordinata>0 "+&
						"order by tes_ord_acq.data_registrazione desc,det_ord_acq.prog_riga_ordine_acq desc "
		end if
		
		if guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore) > 0 then
			//prendo i dati dall'unica riga
			ld_costo = lds_data.getitemnumber(1, 6) / lds_data.getitemnumber(1, 5)			//costo_unitario = imponibile / quantità
			
			if isnull(ld_costo) then ld_costo = 0
		end if
		
		//memorizza in cache
		ll_new = upperbound(is_cs_xx_costo_ultimo.parametro_s_1_a[])
		ll_new += 1
		is_cs_xx_costo_ultimo.parametro_s_1_a[ll_new] = as_cod_prodotto
		is_cs_xx_costo_ultimo.parametro_d_1_a[ll_new] = ld_costo
		
	//#####################################################################
	else
		//costo medio acquisto (ultimi 365 giorni)
		ldt_oggi = datetime(today(), 00:00:00)
		ldd_data_rif = relativedate(date(ldt_oggi), -365)
		ldt_data_rif = datetime(ldd_data_rif, 00:00:00)
		
		if as_tipo_costo="D" then
			//----------------------------------------------------------------------------------------
			//da ddt
			//----------------------------------------------------------------------------------------
			//media valori ddt acquisto dell'ultimi 365 GG
			ls_sql = "select tes_bol_acq.anno_bolla_acq,"+&
								"tes_bol_acq.num_bolla_acq,"+&
								"det_bol_acq.prog_riga_bolla_acq,"+&
								"tes_bol_acq.data_registrazione,"+&
								"det_bol_acq.quan_arrivata,"+&
								"det_bol_acq.imponibile_iva "+&
						 "from tes_bol_acq "+&
						"join det_bol_acq on	det_bol_acq.cod_azienda=tes_bol_acq.cod_azienda and "+&
													"det_bol_acq.anno_bolla_acq=tes_bol_acq.anno_bolla_acq and "+&
													"det_bol_acq.num_bolla_acq=tes_bol_acq.num_bolla_acq "+&
						"where tes_bol_acq.cod_azienda='"+s_cs_xx.cod_azienda+"' and det_bol_acq.imponibile_iva>=0 and "+&
								 "det_bol_acq.flag_blocco<>'S' and tes_bol_acq.flag_blocco<>'S' and "+&
								"det_bol_acq.cod_prodotto = '"+as_cod_prodotto+"' and det_bol_acq.quan_arrivata>0 and "+&
								"tes_bol_acq.data_registrazione>='" + string(ldt_data_rif, s_cs_xx.db_funzioni.formato_data) + "' and "+&
								"tes_bol_acq.data_registrazione<='" + string(ldt_oggi, s_cs_xx.db_funzioni.formato_data) + "' "+&
						"order by tes_bol_acq.data_registrazione desc,det_bol_acq.prog_riga_bolla_acq desc "
		else
			//----------------------------------------------------------------------------------------
			//da ordine acquisto
			//----------------------------------------------------------------------------------------
			//media valori ordini acquisto dell'ultimi 365 GG
			ls_sql = "select  tes_ord_acq.anno_registrazione,"+&
								"tes_ord_acq.num_registrazione,"+&
								"det_ord_acq.prog_riga_ordine_acq,"+&
								"tes_ord_acq.data_registrazione,"+&
								"det_ord_acq.quan_ordinata,"+&
								"det_ord_acq.imponibile_iva "+&
						"from tes_ord_acq "+&
						"join det_ord_acq on	det_ord_acq.cod_azienda=tes_ord_acq.cod_azienda and "+&
											"det_ord_acq.anno_registrazione=tes_ord_acq.anno_registrazione and "+&
											"det_ord_acq.num_registrazione=tes_ord_acq.num_registrazione "+&
						"where tes_ord_acq.cod_azienda='"+s_cs_xx.cod_azienda+"' and det_ord_acq.imponibile_iva>=0 and "+&
									"det_ord_acq.flag_blocco<>'S' and tes_ord_acq.flag_blocco<>'S' and "+&
									"det_ord_acq.cod_prodotto = '"+as_cod_prodotto+"' and "+&
								  	"det_ord_acq.quan_ordinata>0 and "+&
									"tes_ord_acq.data_registrazione>='" + string(ldt_data_rif, s_cs_xx.db_funzioni.formato_data) + "' and "+&
									"tes_ord_acq.data_registrazione<='" + string(ldt_oggi, s_cs_xx.db_funzioni.formato_data) + "' "+&
						"order by tes_ord_acq.data_registrazione desc,det_ord_acq.prog_riga_ordine_acq desc "
		end if
		
		ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
		
		if ll_tot > 0 then
			ld_imponibile = 0
			
			for ll_index=1 to ll_tot
				ld_imponibile += lds_data.getitemnumber(ll_index, 6) / lds_data.getitemnumber(ll_index, 5)		//SUM riga per riga di  ( imponibile / quantità )
			next
		
			//alla fine del ciclo medio sulle righe
			ld_costo = ld_imponibile / ll_tot
		end if
		
	end if
	
else
	//trovato valore in cache ....
end if

return ld_costo
end function

public function integer wf_report_riepilogo_prod_3 (string as_where, string as_tipo_report, string as_tipo_costo, ref string as_errore);
string					ls_sql, ls_cod_tipo_politica_riordino, ls_cod_prodotto, ls_tipo

long					ll_index, ll_tot

dec{4}				ld_costo

integer				li_pos

datastore			lds_data


if as_tipo_report="U" then
	if as_tipo_costo="D" then
		dw_report.object.legenda_t.text = "(da ultimo D.d.T. di Acquisto)"
	else
		dw_report.object.legenda_t.text = "(da ultimo Ordine di Acquisto)"
	end if
	dw_report.object.tipo_report_t.text = "C O S T O   A C Q U I S T O   U L T I M O"
	ls_tipo = "Costo Ultimo Acquisto"
	
else
	if as_tipo_costo="D" then
		dw_report.object.legenda_t.text = "(media Valori D.d.T. di Acquisto degli ultimi 356 GG)"
	else
		dw_report.object.legenda_t.text = "(media Valori Ordine di Acquisto degli ultimi 365 GG)"
	end if
	dw_report.object.tipo_report_t.text = "C O S T O   A C Q U I S T O   M E D I O"
	ls_tipo = "Costo Medio Acquisto"
	
end if


ls_cod_tipo_politica_riordino = dw_selezione.getitemstring(1,"cod_tipo_politica_riordino")

li_pos = pos(is_sql_marginalita, "where")
ls_sql = left(is_sql_marginalita, li_pos - 1)

ls_sql += " where det_ord_ven.cod_azienda='"+s_cs_xx.cod_azienda+"' and det_ord_ven.cod_prodotto is not null and det_ord_ven.quan_ordine > 0 "

if ls_cod_tipo_politica_riordino<>"" and not isnull(ls_cod_tipo_politica_riordino) then
	ls_sql += " and anag_prodotti.cod_tipo_politica_riordino='"+ls_cod_tipo_politica_riordino+"' "
end if

//poi ci metto il resto della where
ls_sql += as_where

ls_sql += " group by det_ord_ven.cod_prodotto,anag_prodotti.des_prodotto,anag_prodotti.cod_misura_mag,anag_clienti.rag_soc_1 "
ls_sql += " order by det_ord_ven.cod_prodotto, anag_clienti.rag_soc_1 "

dw_report.setsqlselect(ls_sql)

dw_report.setredraw(false)

ll_tot = dw_report.retrieve()

if ll_tot < 0 then
	dw_report.setredraw(true)
	as_errore = "Errore durante la retrieve dei dati!"
	return -1
end if

dw_selezione.modify('log_t.text="Preparazione Query report in corso ..."')

for ll_index=1 to ll_tot
	Yield()
	ls_cod_prodotto = dw_report.getitemstring(ll_index, "cod_prodotto")
	
	dw_selezione.modify('log_t.text="('+string(ll_index)+' di '+string(ll_tot)+') Elab. '+ls_tipo+' per '+ls_cod_prodotto+' in corso..."')
	
	ld_costo = wf_costo_prodotto(ls_cod_prodotto, as_tipo_report, as_tipo_costo)
	dw_report.setitem(ll_index, "ultimo_costo", ld_costo)
next


dw_report.setredraw(true)


return 0
end function

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)


dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
save_on_close(c_socnosave)
iuo_dw_main = dw_report
dw_selezione.resetupdate()

is_sql_select = dw_report.getsqlselect()

// DDDW nazioni
dw_nazioni.uof_set_column("cod_nazione, des_nazione", "tab_nazioni", "", "des_nazione")
dw_nazioni.uof_set_parent_dw(dw_selezione, "nazione", "cod_nazione")

dw_pagamenti.visible = false
dw_pagamenti.uof_set_parent_dw( dw_selezione, "cod_pagamento_cs", "cod_pagamento" )
dw_pagamenti.uof_set_column("cod_pagamento, des_pagamento", "tab_pagamenti", &
										"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))", &
										"des_pagamento")
// -----

postevent("ue_posizione")

wf_imposta_sql_group()

this.height = w_cs_xx_mdi.mdi_1.height - 50
postevent("ue_resize")
end event

on w_report_righe_ord_gibus_euro.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.cb_1=create cb_1
this.dw_selezione=create dw_selezione
this.cb_reset=create cb_reset
this.dw_nazioni=create dw_nazioni
this.dw_pagamenti=create dw_pagamenti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_selezione
this.Control[iCurrent+4]=this.cb_reset
this.Control[iCurrent+5]=this.dw_nazioni
this.Control[iCurrent+6]=this.dw_pagamenti
end on

on w_report_righe_ord_gibus_euro.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.cb_1)
destroy(this.dw_selezione)
destroy(this.cb_reset)
destroy(this.dw_nazioni)
destroy(this.dw_pagamenti)
end on

event pc_setddlb;call super::pc_setddlb;datawindowchild ldw_child

f_PO_LoadDDDW_DW(dw_selezione,"cod_agente",sqlca,&
                 "anag_agenti","cod_agente","rag_soc_1", &
                 "(anag_agenti.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((anag_agenti.flag_blocco <> 'S') or (anag_agenti.flag_blocco = 'S' and anag_agenti.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))" )

f_PO_LoadDDDW_DW(dw_selezione,"cod_deposito",sqlca,&
                 " anag_depositi","cod_deposito","des_deposito", &
                 " (anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
			   " ((anag_depositi.flag_blocco <> 'S') or (anag_depositi.flag_blocco = 'S' and anag_depositi.data_blocco > " + s_cs_xx.db_funzioni.oggi  +"))" )

f_po_loaddddw_dw(dw_selezione, &
                 "cod_tipo_politica_riordino", &
                 sqlca, &
                 "tab_tipi_politiche_riordino", &
                 "cod_tipo_politica_riordino", &
                 "des_tipo_politica_riordino", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


// stefanop 21/05/2012
if s_cs_xx.parametri.impresa then
	f_po_loaddddw_dw(dw_selezione, &
		"cod_pagamento", &
		sqlci, &
		"tipo_pagamento", &
		"codice", &
		"descrizione", &
		"")				  
else
	dw_selezione.modify("cod_pagamento_t.visible=0")
	dw_selezione.modify("cf_cod_pagamento.visible=0")
	dw_selezione.modify("cod_pagamento.visible=0")
end if
// -----
end event

event resize;call super::resize;//// stefano 04/05/2010: ticket 2010/113: resize della windows
dw_report.height = this.height - dw_selezione.height - 250

end event

type dw_report from uo_cs_xx_dw within w_report_righe_ord_gibus_euro
integer x = 23
integer y = 720
integer width = 5143
integer height = 2020
integer taborder = 30
string dataobject = "d_righe_ordini_gibus_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;string					ls_cod_cliente, ls_flag_evaso, ls_flag_evasione_1, ls_flag_evasione_2, ls_nazione, ls_nazione_1, ls_nazione_2, ls_nazione_3, ls_flag_blocco, ls_fetch,ls_cod_deposito, &
						ls_where, ls_nazioni[], ls_cod_pagamento_imp
long					ll_ordine_inizio, ll_ordine_fine, ll_errore, ll_anno_ordine
datetime				ldt_data_inizio, ldt_data_fine,ldt_reg_data_inizio, ldt_reg_data_fine
string					ls_flag_visualizza_det, ls_cod_agente, ls_tipo_report, ls_errore, ls_tipo_origine_costo

dw_selezione.accepttext()

dw_report.object.datawindow.print.preview = "No"
dw_report.reset()
dw_report.setfilter("")
dw_report.filter()

dw_selezione.modify('log_t.text="Preparazione Query report in corso ..."')

ls_tipo_report = dw_selezione.getitemstring(1,"tipo_report")

if ls_tipo_report = "C" then
	//layout classico (cioè quello di sempre)

	//Donato 06/04/2009 mostra/nascondi dettaglio
	ls_flag_visualizza_det = dw_selezione.getitemstring(1,"flag_visualizza_det")
	
	if ls_flag_visualizza_det = "N" then
		dw_report.dataobject = "d_righe_ordini_gibus_euro_nodet"	
		dw_report.settransobject( sqlca)
	else
		dw_report.dataobject = "d_righe_ordini_gibus_euro"	
		dw_report.settransobject( sqlca)
	end if

elseif ls_tipo_report = "R" then
	//nuovo layout SR Controllo Struttura: funzione 3 -> LAYOUT PER RIEPILOGO PRODOTTI
	dw_report.dataobject = "d_righe_ordini_gibus_riepilogo_prod"	
	dw_report.settransobject( sqlca)

elseif ls_tipo_report = "U" or ls_tipo_report="M" then
	//marginalita costo utlimo acquisto (o costo medio acquisto ... To.Do. I don't know when ...)
	dw_report.dataobject = "d_righe_ordini_gibus_riepilogo_prod_3"	
	dw_report.settransobject( sqlca)

else		//"A"
	//nuovo layout RIEPILOGO PRODOTTI con riferimento agli acquisti
	dw_report.dataobject = "d_righe_ordini_gibus_riepilogo_prod_2"	
	dw_report.settransobject( sqlca)
end if
//------------------------------------

//attenzione a questo IF se hai inserito una nuova tipologia di report
if ls_tipo_report <> "R" and ls_tipo_report <> "A" and ls_tipo_report <> "U" and ls_tipo_report <> "M"then
	ls_where = " WHERE tes_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' "
end if

ll_anno_ordine = dw_selezione.getitemnumber(1,"n_anno")
ll_ordine_inizio = dw_selezione.getitemnumber(1,"n_ord_da")
ll_ordine_fine   = dw_selezione.getitemnumber(1,"n_ord_a")
ldt_data_inizio = dw_selezione.getitemdatetime(1,"d_data_da")
ldt_data_fine   = dw_selezione.getitemdatetime(1,"d_data_a")
ls_flag_evaso = dw_selezione.getitemstring(1,"flag_evaso")

if isnull(ll_ordine_inizio) or ll_ordine_inizio = 0 then ll_ordine_inizio = 1
if isnull(ll_ordine_fine) or ll_ordine_fine = 0 then ll_ordine_inizio = 999999

if not isnull(ll_anno_ordine) and ll_anno_ordine > 1900 then
	ls_where += " AND det_ord_ven.anno_registrazione=" + string(ll_anno_ordine)
end if
if not isnull(ll_ordine_inizio) and ll_ordine_inizio > 0 then
	ls_where += " AND det_ord_ven.num_registrazione>=" + string(ll_ordine_inizio)
end if
if not isnull(ll_ordine_fine) and ll_ordine_fine > 1900 then
	ls_where += " AND det_ord_ven.num_registrazione<=" + string(ll_ordine_fine)
end if

if not isnull(ldt_data_inizio) then
	ls_where += " AND det_ord_ven.data_consegna >='" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "'"
end if
if not isnull(ldt_data_fine) then
	ls_where += " AND det_ord_ven.data_consegna <='" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "'"
end if

if not isnull(ls_flag_evaso) then
	if ls_flag_evaso = "T" then
		ls_where += " AND det_ord_ven.flag_evasione IN ('A','P')"
	else
		ls_where += " AND det_ord_ven.flag_evasione = '" + ls_flag_evaso + "'"
	end if
end if

ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")
if not isnull(ls_cod_cliente) then
	ls_where += " AND tes_ord_ven.cod_cliente like '" + ls_cod_cliente + "'"
end if

ls_cod_deposito = dw_selezione.getitemstring(1,"cod_deposito")
if not isnull(ls_cod_deposito) then
	ls_where += " AND tes_ord_ven.cod_deposito like '" + ls_cod_deposito + "'"
end if

ls_flag_blocco = dw_selezione.getitemstring(1,"flag_blocco")
if isnull(ls_flag_blocco) then ls_flag_blocco = 'N'
ls_where += " AND det_ord_ven.flag_blocco ='" + ls_flag_blocco + "'"

ldt_reg_data_inizio = dw_selezione.getitemdatetime(1,"data_reg_inizio")
ldt_reg_data_fine   = dw_selezione.getitemdatetime(1,"data_reg_fine")
if not isnull(ldt_reg_data_inizio) then
	ls_where += " AND tes_ord_ven.data_registrazione >='" + string(ldt_reg_data_inizio, s_cs_xx.db_funzioni.formato_data) + "'"
end if
if not isnull(ldt_reg_data_fine) then
	ls_where += " AND tes_ord_ven.data_registrazione <='" + string(ldt_reg_data_fine, s_cs_xx.db_funzioni.formato_data) + "'"
end if

ls_cod_agente = dw_selezione.getitemstring(1,"cod_agente")
if not isnull(ls_cod_agente) then
	ls_where += " AND (tes_ord_ven.cod_agente_1='" + ls_cod_agente + "' OR tes_ord_ven.cod_agente_2='" + ls_cod_agente + "')"
end if

if dw_nazioni.uof_get_selected_column_as_array("cod_nazione", ls_nazioni) > 0 then
	
	//if dw_nazioni.rowcount() <> upperbound(ls_nazioni) then
		// Aggiungo in where l'sql solo se ho selezionato qualcosa e NON tutto.
		// Se ho selezionato tutto allora inutile metterlo in where
		ls_where += " AND anag_clienti.cod_nazione IN ('" + guo_functions.uof_implode(ls_nazioni, "','") + "') "
	//end if
	
end if

ls_cod_pagamento_imp = dw_selezione.getitemstring(1, "cod_pagamento")
if not isnull(ls_cod_pagamento_imp) then
	ls_where += " AND tab_pagamenti.codice_tipo_pag_impresa='" + ls_cod_pagamento_imp + "'"
end if

// stefanop 15/05/2014: aggiunto filtro codice pagamento cs, ovvero NON di impresa.
string ls_cod_pagamento_cs[]

if dw_pagamenti.uof_get_selected_column_as_array("cod_pagamento", ref ls_cod_pagamento_cs) > 0 then
	ls_where += " AND tes_ord_ven.cod_pagamento IN ('" + g_str.implode(ls_cod_pagamento_cs, "','") + "') "
end if
// ----

//avrà effetto solo sul tipo report marginalità (U, M)
ls_tipo_origine_costo = dw_selezione.getitemstring(1, "flag_costo")		//vale O oppure D (da ordine acqiosto o da ddt di acquisto)


if ls_tipo_report = "R" then
	
	if wf_report_riepilogo_prod(ls_where, ls_errore) < 0 then
		g_mb.error(ls_errore)
		dw_selezione.modify('log_t.text="Elaborazione terminata con errori!"')
		return
	end if
	
	dw_selezione.modify('log_t.text="Pronto!"')
	
	return
	
elseif ls_tipo_report = "A" then
	if wf_report_riepilogo_prod_2(ls_where, ls_errore) < 0 then
		g_mb.error(ls_errore)
		dw_selezione.modify('log_t.text="Elaborazione terminata con errori!"')
		return
	end if
	
	dw_selezione.modify('log_t.text="Pronto!"')
	dw_report.object.datawindow.print.preview = "Yes"
	return
	
elseif ls_tipo_report = "U" or ls_tipo_report = "M" then
	//marginalità costo ultimo o costo medio (quest'ultimo ancora da fare ...)
	if wf_report_riepilogo_prod_3(ls_where, ls_tipo_report, ls_tipo_origine_costo, ls_errore) < 0 then
		g_mb.error(ls_errore)
		dw_selezione.modify('log_t.text="Elaborazione terminata con errori!"')
		return
	end if
	
	dw_selezione.modify('log_t.text="Pronto!"')
	dw_report.object.datawindow.print.preview = "Yes"
	return
	
else
	ls_where += " ORDER BY tes_ord_ven.cod_cliente ASC, det_ord_ven.anno_registrazione ASC, det_ord_ven.num_registrazione ASC, det_ord_ven.prog_riga_ord_ven ASC"
	//e continua con l'istruzione setsqlselect più in basso ...
end if


dw_report.setsqlselect(is_sql_select + ls_where)
dw_report.retrieve()
dw_report.object.datawindow.print.preview = "Yes"

dw_report.change_dw_current()

iuo_dw_main = dw_report

dw_selezione.modify('log_t.text="Pronto!"')


end event

type cb_1 from commandbutton within w_report_righe_ord_gibus_euro
integer x = 3360
integer y = 120
integer width = 338
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "R&eport"
end type

event clicked;
setpointer(Hourglass!)

dw_report.setredraw(false)
dw_report.triggerevent("pcd_retrieve")
dw_report.setredraw(true)

dw_report.change_dw_current()

setpointer(Arrow!)
end event

type dw_selezione from uo_cs_xx_dw within w_report_righe_ord_gibus_euro
integer x = 18
integer y = 20
integer width = 3314
integer height = 688
integer taborder = 20
string dataobject = "d_selezione_righe_ordini_gibus"
boolean border = false
end type

event pcd_new;call super::pcd_new;setitem(getrow(),"n_anno",f_anno_esercizio())
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
		
end choose
end event

event clicked;call super::clicked;choose case dwo.name
		
	case "nazione"		
		dw_nazioni.show()
		
	case "cod_pagamento_cs"
		dw_pagamenti.show()
		
end choose
end event

type cb_reset from commandbutton within w_report_righe_ord_gibus_euro
integer x = 3360
integer y = 20
integer width = 338
integer height = 80
integer taborder = 11
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Reset"
end type

event clicked;string ls_null
date ld_null
long ll_null

setnull(ld_null)
setnull(ll_null)
setnull(ls_null)

dw_selezione.setitem(dw_selezione.getrow(), "data_reg_inizio", ld_null)
dw_selezione.setitem(dw_selezione.getrow(), "data_reg_fine", ld_null)
dw_selezione.setitem(dw_selezione.getrow(), "d_data_da", ld_null)
dw_selezione.setitem(dw_selezione.getrow(), "d_data_a", ld_null)
dw_selezione.setitem(dw_selezione.getrow(), "cod_cliente", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "cod_deposito", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "n_ord_da", 1)
dw_selezione.setitem(dw_selezione.getrow(), "n_ord_a", 999999)
dw_selezione.setitem(dw_selezione.getrow(),"n_anno",f_anno_esercizio())
dw_selezione.setitem(dw_selezione.getrow(), "flag_evaso", "A")
dw_selezione.setitem(dw_selezione.getrow(), "nazione", "ITA")
dw_selezione.setitem(dw_selezione.getrow(), "flag_blocco", "N")

dw_selezione.setitem(dw_selezione.getrow(), "flag_visualizza_det", "S")
dw_selezione.setitem(dw_selezione.getrow(), "cod_agente", ls_null)

end event

type dw_nazioni from uo_dddw_checkbox within w_report_righe_ord_gibus_euro
boolean visible = false
integer x = 2071
integer y = 264
integer width = 1253
integer taborder = 40
integer ii_width_percent = 0
end type

type dw_pagamenti from uo_dddw_checkbox within w_report_righe_ord_gibus_euro
boolean visible = false
integer x = 800
integer y = 340
integer width = 2514
integer height = 840
integer taborder = 50
integer ii_width_percent = 200
end type

