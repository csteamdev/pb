﻿$PBExportHeader$w_report_scheda_magazzino_ptenda.srw
$PBExportComments$Report Scheda Magazzino (eseridato dallo standard)
forward
global type w_report_scheda_magazzino_ptenda from w_report_scheda_magazzino
end type
type dw_export from datawindow within w_report_scheda_magazzino_ptenda
end type
end forward

global type w_report_scheda_magazzino_ptenda from w_report_scheda_magazzino
integer height = 2076
dw_export dw_export
end type
global w_report_scheda_magazzino_ptenda w_report_scheda_magazzino_ptenda

type variables
boolean ib_export=false

end variables

forward prototypes
public subroutine wf_report ()
public function integer wf_export ()
end prototypes

public subroutine wf_report ();boolean			lb_1, lb_2, lb_3, lb_continue=false, lb_salta_negativi=false, lb_prodotto_raggruppo = false

string			ls_gruppo[5], ls_gruppo_2, ls_gruppo_3, ls_gruppo_4, ls_gruppo_5,ls_flag_prodotti_movimentati, ls_flag_fiscale, &
				ls_sql, ls_cod_prodotto, ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ls_des_prodotto, &
				ls_cod_comodo,  ls_cod_cat_mer,ls_flag_dettaglio, ls_cod_misura_mag, ls_des_deposito, ls_des_cat_mer, &
				ls_azienda_rag_soc_1, ls_azienda_localita, ls_azienda_provincia, ls_partita_iva, ls_des_tipo_movimento, &
				ls_des_tipo_mov_det,ls_flag_riepilogo_mensile,ls_cod_prodotto_da, ls_cod_prodotto_a, ls_cod_comodo_2, &
				ls_cod_deposito, ls_flag_classe, ls_flag_valorizza, ls_cod_prodotto_mov, ls_des_movimento, ls_errore, ls_prodotto_elaborato, &
				ls_null, ls_chiave[], ls_path_logo_1, ls_modify, ls_des_azienda,ls_mese,ls_null_vett[], ls_flag_dett_mov, ls_flag_lifo

long			ll_riga, ll_tipo_operazione,ll_return, ll_i, ll_j,ll_mese,ll_y,ll_mese_inizio, ll_anno, ll_ind_mese, ll_count_raggruppato, ll_tot_mov, ll_tot_prodotti

dec{4}		ld_quantita, ld_carico_tot, ld_scarico_tot, ld_saldo_tot, ld_riporto, ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], &
				ld_quan_costo_medio_stock[], ld_costo, ld_quan_movimento,ld_null[],ld_saldo_finale

datetime		ldt_inizio, ldt_fine, ldt_data_registrazione, ldt_data_chiusura_periodica, ldt_data_chiusura_annuale, ldt_null, &
				ldt_date_fine_mese[],ldt_fine_mese, ldt_date_start, ldt_date_end

uo_magazzino luo_magazzino
datastore lds_prodotti, lds_movimenti, lds_movimenti_ragg


setpointer(Hourglass!)

dw_selezione.accepttext()

setnull(ldt_null)
setnull(ls_null)

ldt_inizio = dw_selezione.getitemdatetime(1,"data_inizio")
ldt_fine = dw_selezione.getitemdatetime(1,"data_fine")
ls_cod_prodotto_da = dw_selezione.getitemstring(1,"cod_prodotto_da")
ls_cod_prodotto_a = dw_selezione.getitemstring(1,"cod_prodotto_a")
ls_cod_comodo_2 = dw_selezione.getitemstring(1,"cod_comodo_2")
ls_cod_deposito = dw_selezione.getitemstring(1,"cod_deposito")
ls_flag_classe = dw_selezione.getitemstring(1,"flag_classe")
ls_flag_valorizza = dw_selezione.getitemstring(1,"flag_valorizza")
ls_flag_fiscale = dw_selezione.getitemstring(1,"flag_fiscale")
ls_flag_riepilogo_mensile = dw_selezione.getitemstring(1,"flag_riepilogo_mensile")
ls_flag_dett_mov = dw_selezione.getitemstring(1,"flag_dett_mov")
ls_flag_lifo = dw_selezione.getitemstring(1,"flag_lifo")

//imposto a NULL e nella query del datastore non sarà considerato
if ls_flag_lifo<>"S" and ls_flag_lifo<>"N" then setnull(ls_flag_lifo)
if ls_flag_fiscale<>"S" and ls_flag_fiscale<>"N" then setnull(ls_flag_fiscale)


if isnull(ldt_inizio) then
	setpointer(Arrow!)
	g_mb.messagebox("APICE","Data Inizio non impostata.", StopSign!)
	return
end if
if isnull(ldt_fine) then
	setpointer(Arrow!)
	g_mb.messagebox("APICE","Data Fine non impostata.", StopSign!)
	return
end if
if ldt_inizio > ldt_fine then
	setpointer(Arrow!)
	g_mb.messagebox("APICE","Date di inizio e fine elaborazione incongruenti fra loro.", StopSign!)
	return
end if



if ls_flag_riepilogo_mensile ="N" then
	ls_flag_dett_mov = "S"
end if

if ls_flag_riepilogo_mensile ="S" then
	ls_flag_valorizza = "N"
//	ls_flag_dett_mov = "S"
end if

if ls_flag_riepilogo_mensile ="X" then
	ls_flag_valorizza = "N"
	ls_flag_riepilogo_mensile = "S"
	lb_salta_negativi = true
	
	ll_mese_inizio = month(date(ldt_inizio))
	ll_mese = ll_mese_inizio
	ll_i = 0
	
	// calcolo le date di ogni fine mese
	do while true
		
		if ll_mese < ll_mese_inizio then 
			ll_anno ++
			ll_mese = 1
		else
			ll_anno = year(date(ldt_inizio))
		end if
		
		choose case ll_mese
			case 1,3,5,7,8,10,12
				ldt_fine_mese = datetime(date("31/" + string(ll_mese) + "/" + string(ll_anno)),00:00:00)
			case 2
				if mod(ll_anno,4) = 0 then		// anno bisestile
					ldt_fine_mese = datetime(date("29/" + string(ll_mese) + "/" + string(ll_anno)),00:00:00)
				else
					ldt_fine_mese = datetime(date("28/" + string(ll_mese) + "/" + string(ll_anno)),00:00:00)
				end if		
			case else
				ldt_fine_mese = datetime(date("30/" + string(ll_mese) + "/" + string(ll_anno)),00:00:00)
				
		end choose
		
		if ldt_fine_mese > ldt_fine then exit
		
		ll_i ++
		ldt_date_fine_mese[ll_i] = ldt_fine_mese
		ll_mese ++
		if ll_mese > 12 then ll_mese = 0
	loop	
	
end if


if ls_cod_prodotto_da = "" then setnull(ls_cod_prodotto_da)
if ls_cod_prodotto_a = "" then setnull(ls_cod_prodotto_a)
if ls_cod_comodo_2 = "" then setnull(ls_cod_comodo_2)
if ls_flag_classe = "" then setnull(ls_flag_classe)
if ls_flag_valorizza = "" then setnull(ls_flag_valorizza)

dw_report.reset()

if ls_flag_riepilogo_mensile ="S" then
	dw_report.dataobject = 'd_report_scheda_magazzino_mensile'
end if

dw_report.setredraw(false)
st_1.text = "Attendere la Preparazione del Report; l'operazione potrebbe richiedere parecchi minuti."

select rag_soc_1, 
       localita, 
		 provincia, 
		 partita_iva
into   :ls_azienda_rag_soc_1, 
       :ls_azienda_localita, 
		 :ls_azienda_provincia, 
		 :ls_partita_iva
from   aziende
where  cod_azienda = :s_cs_xx.cod_azienda;

dw_report.object.st_azienda.text = ls_azienda_rag_soc_1
dw_report.object.st_indirizzo.text = ls_azienda_localita + "(" + ls_azienda_provincia + ")"
dw_report.object.st_piva.text = ls_partita_iva

dw_report.object.st_movimenti.text = "MOVIMENTI DAL " + string(ldt_inizio,"dd/mm/yyyy") + " AL " + string(ldt_fine,"dd/mm/yyyy")
if not isnull(ls_cod_prodotto_da) and not isnull(ls_cod_prodotto_a) then
	dw_report.object.st_prodotto.text = "DAL PRODOTTO " + ls_cod_prodotto_da + " AL PRODOTTO " + ls_cod_prodotto_a
elseif not isnull(ls_cod_prodotto_da) then
	dw_report.object.st_prodotto.text = "DAL PRODOTTO " + ls_cod_prodotto_da + " AL PRODOTTO " + ls_cod_prodotto_da
elseif not isnull(ls_cod_prodotto_a) then
	dw_report.object.st_prodotto.text = "DAL PRODOTTO " + ls_cod_prodotto_a + " AL PRODOTTO " + ls_cod_prodotto_a
else
	dw_report.object.st_prodotto.text = ""
end if
if not isnull(ls_cod_comodo_2) then
	dw_report.object.st_comodo.text = "COD COMODO " + ls_cod_comodo_2
else
	dw_report.object.st_comodo.text = ""
end if
if not isnull(ls_flag_classe) then
	dw_report.object.st_classe.text = "PRODOTTI DI CLASSE " + ls_flag_classe
else
	dw_report.object.st_classe.text = ""	
end if
if not isnull(ls_cod_deposito) then
	
	select des_deposito
	into   :ls_des_deposito
	from   anag_depositi
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_deposito = :ls_cod_deposito;
		
	dw_report.object.st_deposito.text = "VALIDO PER IL DEPOSITO " + ls_des_deposito
else
	dw_report.object.st_deposito.text = "VALIDO PER TUTTI I DEPOSITI"
end if
// prendo le date di chiusura e controllo che la stampa del magazzino sia successiva

select data_chiusura_periodica,
       data_chiusura_annuale
into   :ldt_data_chiusura_periodica,
       :ldt_data_chiusura_annuale
from   con_magazzino
where  cod_azienda = :s_cs_xx.cod_azienda;


// *** filtro i prodotti

lds_prodotti = create datastore
lds_prodotti.dataobject = "d_ds_report_scheda_magazzino_prodotti"
lds_prodotti.settransobject( sqlca)
ll_return = lds_prodotti.retrieve( s_cs_xx.cod_azienda, ls_cod_prodotto_da, ls_cod_prodotto_a, ls_flag_fiscale, ls_flag_classe, ls_cod_comodo_2, ls_flag_lifo)

if ll_return < 0 then
	setpointer(Arrow!)
	g_mb.messagebox( "APICE", "Errore durante la ricerca dei prodotti iniziali: " + sqlca.sqlerrtext)
	destroy lds_prodotti
	return
end if


lds_movimenti = create datastore
lds_movimenti.dataobject = "d_ds_report_scheda_magazzino_mov"
lds_movimenti.settransobject( sqlca)

//######################################################
//se si tratta di un codice di raggruppo devo elaborare i movimenti dei codici raggruppati
lds_movimenti_ragg = create datastore
lds_movimenti_ragg.dataobject = "d_ds_report_scheda_magazzino_mov_ragg"
lds_movimenti_ragg.settransobject( sqlca)
//######################################################

ll_tot_prodotti = lds_prodotti.rowcount()
for ll_i = 1 to ll_tot_prodotti       // ** filtro con gli altri dati
	Yield()
	lb_prodotto_raggruppo = false
	
	ls_cod_prodotto_mov = lds_prodotti.getitemstring( ll_i, 1)
	
	ls_prodotto_elaborato = "("+string(ll_i) +" di " + string(ll_tot_prodotti) + ") Prodotto: '" + ls_cod_prodotto_mov+"'"
	st_1.text = ls_prodotto_elaborato + " ..."
	
	//se inizia per spazio saltalo che senno si blocca tutto ....
	if left(ls_prodotto_elaborato, 1) = " " then continue
	
	
	select		des_prodotto,
				cod_misura_mag
	into	:ls_des_prodotto,
			:ls_cod_misura_mag
	from	anag_prodotti
	where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto_mov;

	
	//######################################################
	//verifico se si tratta di un codice prodotto che raggruppa
	setnull(ll_count_raggruppato)

	select count(*)
	into	:ll_count_raggruppato
	from anag_prodotti
	where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto_raggruppato = :ls_cod_prodotto_mov;
				
	if ll_count_raggruppato>0 then
		lb_prodotto_raggruppo = true
	end if
	//######################################################

	if isnull(ls_cod_prodotto_mov) then ls_cod_prodotto_mov = ""
	if isnull(ls_des_prodotto) then ls_des_prodotto = ""
	
	
	if ls_flag_riepilogo_mensile = "N" then
		ll_riga = dw_report.insertrow(0)
		dw_report.setitem( ll_riga, "flag_tipo_riga",1)
		dw_report.setitem( ll_riga, "cod_prodotto", ls_cod_prodotto_mov )
		dw_report.setitem( ll_riga, "des_prodotto", ls_des_prodotto)
		dw_report.setitem( ll_riga, "codice_um", ls_cod_misura_mag)
	end if
	
	lds_movimenti.reset()
	lds_movimenti_ragg.reset()
	
	
	st_1.text = ls_prodotto_elaborato + " estrazione movimenti in corso ..."
	
	if lb_prodotto_raggruppo then
		ll_tot_mov = lds_movimenti_ragg.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto_mov, ldt_inizio, ldt_fine, ls_cod_deposito)
		
		//li ricopio nel di ds riferimento
		if ll_tot_mov>0 then lds_movimenti_ragg.rowscopy(1, ll_tot_mov, primary!, lds_movimenti, 1, primary!)
		
	else
		ll_tot_mov = lds_movimenti.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto_mov, ldt_inizio, ldt_fine, ls_cod_deposito)
	end if

	if ll_tot_mov < 0 then
		setpointer(Arrow!)
		g_mb.messagebox( "APICE", "Errore durante la ricerca dei movimenti dei prodotti iniziali: " + sqlca.sqlerrtext)
		destroy lds_movimenti
		destroy lds_movimenti_ragg
		destroy lds_prodotti
		return
	end if
	
	ld_carico_tot = 0
	ld_scarico_tot = 0
	ld_saldo_tot = 0
	
	if ll_tot_mov < 1 then
		
		st_1.text = ls_prodotto_elaborato + " NO MOV. : solo riga riporto ..."
		
		if ls_flag_riepilogo_mensile = "N" then
			ll_riga = dw_report.insertrow(0)
			dw_report.setitem( ll_riga, "flag_tipo_riga", 2)
		end if			
		// **** in questo punto se la data inizio periodo è maggiore alla (data chiusura periodica + 1) oppure è maggiore
		//      della data chiusura annuale allora devo mettere il riporto dell'anno precedente.
		if ldt_inizio > ldt_data_chiusura_periodica and date(ldt_inizio) > relativedate(date(ldt_data_chiusura_annuale),1) then
				
				
			luo_magazzino = CREATE uo_magazzino			

			for ll_y = 1 to 14
				ld_quant_val[ll_y] = 0
			next
			ls_chiave[] = ls_null_vett[]
			ld_giacenza_stock[] = ld_null[]
			ld_costo_medio_stock[] = ld_null[]
			ld_quan_costo_medio_stock[] = ld_null[]
			
			
			if lb_prodotto_raggruppo then
				ll_return = wf_prodotto_raggruppo(	ls_cod_prodotto_mov,&
																luo_magazzino,&
																ldt_inizio,&
																"",&
																ld_quant_val[],&
																ls_errore,&
																"N",&
																ls_chiave[],&
																ld_giacenza_stock[],&
																ld_costo_medio_stock[],&
																ld_quan_costo_medio_stock[])
			else
				ll_return = luo_magazzino.uof_saldo_prod_date_decimal(	ls_cod_prodotto_mov,&
																							datetime(date(relativedate(date(ldt_inizio),-1)),00:00:00), &
																							"",&
																							ld_quant_val[],&
																							ls_errore,&
																							"N",&
																							ls_chiave[],&
																							ld_giacenza_stock[],&
																							ld_costo_medio_stock[],&
																							ld_quan_costo_medio_stock[])
			end if
			
			destroy luo_magazzino
			
			if ll_return <0 then
				g_mb.messagebox( "APICE", "Impossibile calcolare il riporto: " + ls_errore)
			end if
			
			ld_riporto = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
		
			// *** inserisco la riga del riporto
			ld_saldo_tot += ld_riporto
			ll_riga = dw_report.insertrow(0)
			dw_report.setitem( ll_riga, "flag_tipo_riga", 3)
			dw_report.setitem( ll_riga, "mese", "RIPORTO")
			dw_report.setitem( ll_riga, "data_registrazione", ldt_null)
			dw_report.setitem( ll_riga, "cod_movimento", ls_null)
			dw_report.setitem( ll_riga, "des_movimento", "RIPORTO al " + string(relativedate(date(ldt_inizio),-1), "dd/mm/yyyy") )		
			dw_report.setitem( ll_riga, "carico", 0)
			dw_report.setitem( ll_riga, "scarico", 0)
			
			if ls_flag_riepilogo_mensile ="S" then
				dw_report.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto_mov )
				dw_report.setitem(ll_riga, "des_prodotto", ls_des_prodotto)
				dw_report.setitem(ll_riga, "codice_um", ls_cod_misura_mag)
				dw_report.setitem(ll_riga, "carico", ld_saldo_tot)
			else		
				dw_report.setitem(ll_riga, "saldo", ld_saldo_tot)
			end if			
				
		end if		
		
		if not isnull(ls_flag_valorizza) and ls_flag_valorizza <> "" and ls_flag_valorizza = "S" then
			
			for ll_return = 1 to 14
				ld_quant_val[ll_return] = 0
			next
			
			ls_chiave[] = ls_null_vett[]
			ld_giacenza_stock[] = ld_null[]
			ld_costo_medio_stock[] = ld_null[]
			ld_quan_costo_medio_stock[] = ld_null[]

			luo_magazzino = CREATE uo_magazzino
			
			if lb_prodotto_raggruppo then
				ll_return = wf_prodotto_raggruppo(	ls_cod_prodotto_mov,&
																luo_magazzino,&
																ldt_fine,&
																"",&
																ld_quant_val[],&
																ls_errore,&
																"N",&
																ls_chiave[],&
																ld_giacenza_stock[],&
																ld_costo_medio_stock[],&
																ld_quan_costo_medio_stock[])
			else
				ll_return = luo_magazzino.uof_saldo_prod_date_decimal(	ls_cod_prodotto_mov,&
																							ldt_fine,&
																							"",&
																							ld_quant_val[],&
																							ls_errore,&
																							"N",&
																							ls_chiave[],&
																							ld_giacenza_stock[],&
																							ld_costo_medio_stock[],&
																							ld_quan_costo_medio_stock[])
			end if
			
			destroy luo_magazzino
				
			if ll_return <0 then
				g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto_mov, ls_errore)
				continue
			end if
				
			if ld_quant_val[12] > 0 then
				ld_costo = ld_quant_val[13] / ld_quant_val[12]
			else						
				select costo_medio_ponderato
				into   :ld_costo
				from   lifo
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto_mov and
						 anno_lifo = (select max(anno_lifo)
										  from   lifo
										  where  cod_azienda = :s_cs_xx.cod_azienda and
													cod_prodotto = :ls_cod_prodotto_mov);
																	
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Inventario prodotto: " + ls_cod_prodotto_mov, "Errore in lettura tabella lifo: " + sqlca.sqlerrtext)
					continue
				elseif sqlca.sqlcode = 100 or isnull(ld_costo) then
					ld_costo = 0
				end if
							
			end if
			
			if ls_flag_riepilogo_mensile = "N" then
				ll_riga = dw_report.insertrow(0)
				dw_report.setitem( ll_riga, "flag_tipo_riga", 5)
				dw_report.setitem(ll_riga, "costo_medio", ld_costo)		
				ll_riga = dw_report.insertrow(0)
				dw_report.setitem( ll_riga, "flag_tipo_riga", 6)
				dw_report.setitem(ll_riga, "totale", round(ld_costo * ld_saldo_tot,2))				
			end if
			
		end if		
		
		continue
	end if
	
	for ll_j = 1 to ll_tot_mov
		
		st_1.text = ls_prodotto_elaborato + " elab. mov. "+string(ll_j)+" di " + string(ll_tot_mov) + " ..."
		
		if ll_j = 1 then
			if ls_flag_riepilogo_mensile = "N" then
				ll_riga = dw_report.insertrow(0)
				dw_report.setitem( ll_riga, "flag_tipo_riga", 2)
			end if			
			// **** in questo punto se la data inizio periodo è maggiore alla (data chiusura periodica + 1) oppure è maggiore
			//      della data chiusura annuale allora devo mettere il riporto dell'anno precedente.
			if ldt_inizio > ldt_data_chiusura_periodica and date(ldt_inizio) > relativedate(date(ldt_data_chiusura_annuale),1) then
				
				for ll_return = 1 to 14
					ld_quant_val[ll_return] = 0
				next				
				ls_chiave[] = ls_null_vett[]
				ld_giacenza_stock[] = ld_null[]
				ld_costo_medio_stock[] = ld_null[]
				ld_quan_costo_medio_stock[] = ld_null[]
				luo_magazzino = CREATE uo_magazzino
				
				
			luo_magazzino = CREATE uo_magazzino
			
				if lb_prodotto_raggruppo then
					ll_return = wf_prodotto_raggruppo(	ls_cod_prodotto_mov,&
																	luo_magazzino,&
																	ldt_inizio,&
																	"",&
																	ld_quant_val[],&
																	ls_errore,&
																	"N",&
																	ls_chiave[],&
																	ld_giacenza_stock[],&
																	ld_costo_medio_stock[],&
																	ld_quan_costo_medio_stock[])
				else
					ll_return = luo_magazzino.uof_saldo_prod_date_decimal(	ls_cod_prodotto_mov,&
																								datetime(date(relativedate(date(ldt_inizio),-1)),00:00:00),&
																								"",&
																								ld_quant_val[],&
																								ls_errore,&
																								"N",&
																								ls_chiave[],&
																								ld_giacenza_stock[],&
																								ld_costo_medio_stock[],&
																								ld_quan_costo_medio_stock[])
				end if
				
				destroy luo_magazzino
				
				if ll_return <0 then
					g_mb.messagebox( "APICE", "Impossibile calcolare il riporto: " + ls_errore)
				end if
				
				ld_riporto = ld_quant_val[1] + ld_quant_val[4] - ld_quant_val[6]
			
				// *** inserisco la riga del riporto
				ld_saldo_tot += ld_riporto
				ll_riga = dw_report.insertrow(0)
						
				dw_report.setitem( ll_riga, "flag_tipo_riga", 3)
				dw_report.setitem( ll_riga, "mese", "0 - RIPORTO")// DA ERRORE....
				//dw_report.setitem( ll_riga, "des_movimento", "0 - RIPORTO")
				dw_report.setitem( ll_riga, "data_registrazione", ldt_null)
				dw_report.setitem( ll_riga, "cod_movimento", ls_null)
				dw_report.setitem( ll_riga, "des_movimento", "RIPORTO al " + string(relativedate(date(ldt_inizio),-1), "dd/mm/yyyy") )		
				dw_report.setitem( ll_riga, "carico", 0)
				dw_report.setitem( ll_riga, "scarico", 0)
				
				if ls_flag_riepilogo_mensile ="S" then
					dw_report.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto_mov )
					dw_report.setitem(ll_riga, "des_prodotto", ls_des_prodotto)
					dw_report.setitem(ll_riga, "codice_um", ls_cod_misura_mag)
					dw_report.setitem(ll_riga, "carico", ld_saldo_tot)
				else		
					dw_report.setitem(ll_riga, "saldo", ld_saldo_tot)
				end if
				
			//dw_report.setitem(ll_riga, "saldo", ld_saldo_tot)				
			end if			
			
		end if
		
		ldt_data_registrazione = lds_movimenti.getitemdatetime( ll_j, "data_registrazione")
		ls_cod_tipo_movimento = lds_movimenti.getitemstring( ll_j, "cod_tipo_mov_det")
		
		ld_quantita = lds_movimenti.getitemnumber( ll_j, "quantita")
		
		select des_tipo_movimento
		into   :ls_des_movimento
		from   tab_tipi_movimenti_det
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_mov_det = :ls_cod_tipo_movimento;
		
		// *** trovo se è di scarico o di carico
		luo_magazzino = create uo_magazzino
		luo_magazzino.uof_tipo_movimento(ls_cod_tipo_movimento, ref ll_tipo_operazione, ref lb_1, ref lb_2, ref lb_3)
		destroy luo_magazzino
		
		//
		
		ll_riga = dw_report.insertrow(0)
		
		if ls_flag_riepilogo_mensile = "S" then
			dw_report.setitem( ll_riga, "cod_prodotto", ls_cod_prodotto_mov )
			dw_report.setitem( ll_riga, "des_prodotto", ls_des_prodotto)
			dw_report.setitem( ll_riga, "codice_um", ls_cod_misura_mag)
			ll_mese = month(date(ldt_data_registrazione))
			ll_anno = year(date(ldt_data_registrazione))
			choose case ll_mese
				case 1
					ls_mese = "GENNAIO " + string(ll_anno)
				case 2
					ls_mese = "FEBBRAIO " + string(ll_anno)
				case 3
					ls_mese = "MARZO " + string(ll_anno)
				case 4
					ls_mese = "APRILE " + string(ll_anno)
				case 5
					ls_mese = "MAGGIO " + string(ll_anno)
				case 6
					ls_mese = "GIUGNO " + string(ll_anno)
				case 7
					ls_mese = "LUGLIO " + string(ll_anno)
				case 8
					ls_mese = "AGOSTO " + string(ll_anno)
				case 9
					ls_mese = "SETTEMBRE " + string(ll_anno)
				case 10
					ls_mese = "OTTOBRE " + string(ll_anno)
				case 11
					ls_mese = "NOVEMBRE " + string(ll_anno)
				case 12
					ls_mese = "DICEMBRE " + string(ll_anno)
			end choose
			
			dw_report.setitem( ll_riga, "mese", ls_mese)
			dw_report.setitem( ll_riga, "num_mese", (ll_anno * 100) + ll_mese)
		end if
		
		dw_report.setitem( ll_riga, "flag_tipo_riga", 3)
		dw_report.setitem( ll_riga, "data_registrazione", ldt_data_registrazione)
		dw_report.setitem( ll_riga, "cod_movimento", ls_cod_tipo_movimento)
		dw_report.setitem( ll_riga, "des_movimento", ls_des_movimento)
		
		choose case ll_tipo_operazione
			case 1,5,19,13  		// movimenti di carico
				dw_report.setitem(ll_riga, "carico", ld_quantita)
				dw_report.setitem(ll_riga, "scarico", 0)
				ld_carico_tot += ld_quantita
			case 2,6,20,14	 		// rettifiche carico
				dw_report.setitem(ll_riga, "carico", 0)
				dw_report.setitem(ll_riga, "scarico",ld_quantita)
				ld_scarico_tot += ld_quantita
			case 3,7	 				// movimenti si scarico
				dw_report.setitem(ll_riga, "carico", 0)
				dw_report.setitem(ll_riga, "scarico", ld_quantita)
				ld_scarico_tot += ld_quantita
			case 4,8	 				// rettifiche scarico
				dw_report.setitem(ll_riga, "carico",ld_quantita)
				dw_report.setitem(ll_riga, "scarico", 0)
				ld_carico_tot += ld_quantita
			case else  // 9,15,10,16,11,17,12,18
				// movimenti di rettifica solo valore e quindi salti
				dw_report.setitem(ll_riga, "carico",0)
				dw_report.setitem(ll_riga, "scarico", 0)			
		end choose		
		ld_saldo_tot = ld_saldo_tot + ld_carico_tot - ld_scarico_tot
		dw_report.setitem(ll_riga, "saldo", ld_saldo_tot)
		ld_scarico_tot = 0
		ld_carico_tot = 0
		
	next
	
	if ls_flag_riepilogo_mensile = "N" then
		ll_riga = dw_report.insertrow(0)
		dw_report.setitem( ll_riga, "flag_tipo_riga", 4)
		dw_report.setitem(ll_riga, "saldo_totale", ld_saldo_tot)
	end if
	
	if not isnull(ls_flag_valorizza) and ls_flag_valorizza <> "" and ls_flag_valorizza = "S" then
		
		st_1.text = ls_prodotto_elaborato + " operazioni finali ..."
		
		for ll_return = 1 to 14
			ld_quant_val[ll_return] = 0
		next
		
		ls_chiave[] = ls_null_vett[]
		ld_giacenza_stock[] = ld_null[]
		ld_costo_medio_stock[] = ld_null[]
		ld_quan_costo_medio_stock[] = ld_null[]
		luo_magazzino = CREATE uo_magazzino
		
		if lb_prodotto_raggruppo then
			ll_return = wf_prodotto_raggruppo(	ls_cod_prodotto_mov,&
															luo_magazzino,&
															ldt_fine,&
															"",&
															ld_quant_val[],&
															ls_errore,&
															"N",&
															ls_chiave[],&
															ld_giacenza_stock[],&
															ld_costo_medio_stock[],&
															ld_quan_costo_medio_stock[])
		else
			ll_return = luo_magazzino.uof_saldo_prod_date_decimal(	ls_cod_prodotto_mov,&
																						ldt_fine,&
																						"",&
																						ld_quant_val[],&
																						ls_errore,&
																						"N",&
																						ls_chiave[],&
																						ld_giacenza_stock[],&
																						ld_costo_medio_stock[],&
																						ld_quan_costo_medio_stock[])
		end if
		
		destroy luo_magazzino
			
		if ll_return <0 then
			g_mb.messagebox("Errore Inventario Magazzino: prodotto " + ls_cod_prodotto_mov, ls_errore)
			continue
		end if
			
		if ld_quant_val[12] > 0 then
			ld_costo = ld_quant_val[13] / ld_quant_val[12]
		else						
			select costo_medio_ponderato
			into   :ld_costo
			from   lifo
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto_mov and
					 anno_lifo = (select max(anno_lifo)
									  from   lifo
									  where  cod_azienda = :s_cs_xx.cod_azienda and
												cod_prodotto = :ls_cod_prodotto);
																
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Inventario prodotto: " + ls_cod_prodotto, "Errore in lettura tabella lifo: " + sqlca.sqlerrtext)
				continue
			elseif sqlca.sqlcode = 100 or isnull(ld_costo) then
				ld_costo = 0
			end if
						
		end if
		ll_riga = dw_report.insertrow(0)
		dw_report.setitem( ll_riga, "flag_tipo_riga", 5)
		dw_report.setitem(ll_riga, "costo_medio", ld_costo)		
		ll_riga = dw_report.insertrow(0)
		dw_report.setitem( ll_riga, "flag_tipo_riga", 6)
		dw_report.setitem(ll_riga, "totale", round(ld_costo * ld_saldo_tot,2))				
		
	end if
	
next

destroy lds_movimenti
destroy lds_movimenti_ragg

destroy lds_prodotti
rollback;

if ls_flag_riepilogo_mensile = "S" then
	dw_report.setsort("cod_prodotto A, num_mese A,cod_movimento A")
	dw_report.sort()
end if

if ls_flag_dett_mov = "N" then
	dw_report.modify("datawindow.trailer.3.height = 0")
	dw_report.modify("datawindow.header.2.height = 0")
end if

dw_report.groupcalc()
dw_report.setredraw(true)
dw_folder.fu_selecttab(2)
dw_report.change_dw_current()
dw_report.object.datawindow.print.preview = true

setpointer(Arrow!)

if ib_export then
	dw_report.setredraw(false)
	
	wf_export()
	
	// se esco dal report devo rifare il group ed il calc
	if ls_flag_riepilogo_mensile = "S" then
		dw_report.setsort("cod_prodotto A, num_mese A,cod_movimento A")
		dw_report.sort()
	end if
	if ls_flag_dett_mov = "N" then
		dw_report.modify("datawindow.trailer.3.height = 0")
		dw_report.modify("datawindow.header.2.height = 0")
	end if
	
	dw_report.groupcalc()
	dw_report.setredraw(true)
	dw_folder.fu_selecttab(2)
	dw_report.change_dw_current()
	dw_report.object.datawindow.print.preview = true
	
end if

return

end subroutine

public function integer wf_export ();string ls_filename,ls_path
long ll_i, ll_rows,ll_new, ll_ret
dec{4} ld_carico,ld_scarico


dw_report.setredraw(false)

dw_report.setsort("cod_prodotto A, num_mese A,data_registrazione, A, cod_movimento A")
dw_report.sort()

dw_report.groupcalc()

ll_rows = dw_report.rowcount()
dw_export.reset()

ld_carico = 0
ld_scarico = 0

for ll_i = 1 to ll_rows
	
	// se sono all'ultima riga OPPURE se il prodotto è cambiato
	if ll_i > 1 then
		if dw_report.getitemstring(ll_i, "cod_prodotto") <> dw_report.getitemstring(ll_i -1, "cod_prodotto") or dw_report.getitemnumber(ll_i, "num_mese") <> dw_report.getitemnumber(ll_i -1, "num_mese")  then
			ll_new = dw_export.insertrow(0)
			
			dw_export.setitem(ll_new, "cod_prodotto", dw_report.getitemstring(ll_i -1, "cod_prodotto")  )
			dw_export.setitem(ll_new, "des_prodotto",  dw_report.getitemstring(ll_i -1, "des_prodotto") )
			dw_export.setitem(ll_new, "codice_um",  dw_report.getitemstring(ll_i -1, "codice_um") )
			dw_export.setitem(ll_new, "carico", ld_carico  )
			dw_export.setitem(ll_new, "scarico", ld_scarico  )
			dw_export.setitem(ll_new, "saldo",  ld_carico - ld_scarico )
//			dw_export.setitem(ll_new, "carico", dw_report.getitemnumber(ll_i -1, "carico")  )
//			dw_export.setitem(ll_new, "scarico", dw_report.getitemnumber(ll_i -1, "scarico")  )
//			dw_export.setitem(ll_new, "saldo",  dw_report.getitemnumber(ll_i -1, "saldo") )
			dw_export.setitem(ll_new, "num_mese", dw_report.getitemnumber(ll_i -1, "num_mese")  )
			dw_export.setitem(ll_new, "cambio_mese",  "F" )
			ld_carico = 0
			ld_scarico = 0
		end if
	end if
	
	// l'ultima riga va sempre scritta
	if ll_i = ll_rows then
		ll_new = dw_export.insertrow(0)
		dw_export.setitem(ll_new, "cod_prodotto", dw_report.getitemstring(ll_i , "cod_prodotto")  )
		dw_export.setitem(ll_new, "des_prodotto",  dw_report.getitemstring(ll_i, "des_prodotto") )
		dw_export.setitem(ll_new, "codice_um",  dw_report.getitemstring(ll_i , "codice_um") )
//		dw_export.setitem(ll_new, "carico", dw_report.getitemnumber(ll_i , "carico")  )
//		dw_export.setitem(ll_new, "scarico", dw_report.getitemnumber(ll_i , "scarico")  )
//		dw_export.setitem(ll_new, "saldo",  dw_report.getitemnumber(ll_i , "saldo") )
		dw_export.setitem(ll_new, "carico", ld_carico + dw_report.getitemnumber(ll_i , "carico") )
		dw_export.setitem(ll_new, "scarico", ld_scarico + dw_report.getitemnumber(ll_i , "scarico")  )
		dw_export.setitem(ll_new, "saldo", ld_carico + dw_report.getitemnumber(ll_i , "carico") - ld_scarico - dw_report.getitemnumber(ll_i , "scarico") )
		dw_export.setitem(ll_new, "num_mese", dw_report.getitemnumber(ll_i , "num_mese")  )
		dw_export.setitem(ll_new, "cambio_mese",  "F" )
		ld_carico = 0
		ld_scarico = 0
		
	else
			
		ld_carico += dw_report.getitemnumber(ll_i , "carico")
		ld_scarico += dw_report.getitemnumber(ll_i , "scarico")
		
	end if	
	
//	dw_export.setitem(ll_new, "cod_prodotto", dw_report.getitemstring(ll_i, "cod_prodotto")  )
//	dw_export.setitem(ll_new, "des_prodotto",  dw_report.getitemstring(ll_i, "des_prodotto") )
//	dw_export.setitem(ll_new, "codice_um",  dw_report.getitemstring(ll_i, "codice_um") )
//	dw_export.setitem(ll_new, "carico", dw_report.getitemnumber(ll_i, "carico")  )
//	dw_export.setitem(ll_new, "scarico", dw_report.getitemnumber(ll_i, "scarico")  )
//	dw_export.setitem(ll_new, "saldo",  dw_report.getitemnumber(ll_i, "saldo") )
//	dw_export.setitem(ll_new, "num_mese", dw_report.getitemnumber(ll_i, "num_mese")  )
//	dw_export.setitem(ll_new, "cambio_mese",  "M" )
//	if ll_i > 1 and dw_report.getitemstring(ll_i, "cod_prodotto") = dw_report.getitemstring(ll_i -1, "cod_prodotto") then 
//		if dw_report.getitemnumber(ll_i, "num_mese") <> dw_report.getitemnumber(ll_i -1, "num_mese")  then
//			dw_export.setitem(ll_new - 1, "cambio_mese",  "F" )
//		end if
//	else
//		dw_export.setitem(ll_new - 1, "cambio_mese",  "F" )
//	end if
//	
//	if ll_i = ll_rows then
//		dw_export.setitem(ll_new, "cambio_mese",  "F" )
//	end if
	
next

ll_new = dw_export.insertrow(1)

dw_export.setitem(ll_new, "cod_prodotto", "CODICE" )
dw_export.setitem(ll_new, "des_prodotto",  "DESCRIZIONE" )
dw_export.setitem(ll_new, "codice_um",  "UM" )
dw_export.setitem(ll_new, "carico", "CARICO"  )
dw_export.setitem(ll_new, "scarico", "SCARICO" )
dw_export.setitem(ll_new, "saldo",  "SALDO" )
dw_export.setitem(ll_new, "num_mese", "ANNO-MESE"  )
dw_export.setitem(ll_new, "cambio_mese", "M/F"  )


if GetFileSaveName ( "Selezione File",  ls_path, ls_filename, "XLS",  "Tutti i files (*.*),*.*" , guo_functions.uof_get_user_documents_folder( )     ,   32770) > 0 then
	dw_export.saveas( ls_path, Excel!, false)
end if

return 0
end function

on w_report_scheda_magazzino_ptenda.create
int iCurrent
call super::create
this.dw_export=create dw_export
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_export
end on

on w_report_scheda_magazzino_ptenda.destroy
call super::destroy
destroy(this.dw_export)
end on

event resize;call super::resize;//dw_folder.width = newwidth - 20
//dw_folder.height = newheight - 20
//dw_report.width = newwidth - 120
//dw_report.height = newheight - 170
end event

type st_1 from w_report_scheda_magazzino`st_1 within w_report_scheda_magazzino_ptenda
end type

type cb_reset from w_report_scheda_magazzino`cb_reset within w_report_scheda_magazzino_ptenda
end type

type cb_report from w_report_scheda_magazzino`cb_report within w_report_scheda_magazzino_ptenda
end type

event cb_report::clicked;call super::clicked;//dw_report.object.saldo.visible = 0
end event

type dw_selezione from w_report_scheda_magazzino`dw_selezione within w_report_scheda_magazzino_ptenda
integer height = 876
string dataobject = "d_selezione_report_scheda_mag_ptenda"
end type

event dw_selezione::pcd_new;call super::pcd_new;setitem(getrow(),"flag_valorizza", "N")
setitem(getrow(),"flag_riepilogo_mensile", "X")
setitem(getrow(),"flag_fiscale", "S")
end event

event dw_selezione::ue_key;call super::ue_key;if keyflags = 3 then
	if key = KeyEnter! then
		ib_export = true
		parent.title = parent.title + " *** "
	end if
end if
end event

type dw_report from w_report_scheda_magazzino`dw_report within w_report_scheda_magazzino_ptenda
integer height = 1804
end type

type dw_folder from w_report_scheda_magazzino`dw_folder within w_report_scheda_magazzino_ptenda
end type

type dw_export from datawindow within w_report_scheda_magazzino_ptenda
boolean visible = false
integer x = 1294
integer y = 2028
integer width = 686
integer height = 400
integer taborder = 40
boolean bringtotop = true
string title = "none"
string dataobject = "d_report_scheda_magazzino_mensile_export"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

