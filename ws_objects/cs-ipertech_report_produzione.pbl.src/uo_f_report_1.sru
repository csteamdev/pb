﻿$PBExportHeader$uo_f_report_1.sru
$PBExportComments$User Object per impostazione report_1 (NON VIENE USATO)
forward
global type uo_f_report_1 from nonvisualobject
end type
end forward

global type uo_f_report_1 from nonvisualobject
end type
global uo_f_report_1 uo_f_report_1

forward prototypes
public function integer uof_report_1 (ref datawindow fdw_report_1, integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_reparto, long fl_prog_riga_comanda, long fl_num_tenda)
end prototypes

public function integer uof_report_1 (ref datawindow fdw_report_1, integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_reparto, long fl_prog_riga_comanda, long fl_num_tenda);/* Funzione che compila la datawindow di tipo report_1
 nome: f_report_1
 tipo: integer
       -1 failed
  		 0 passed
							

		Creata il 21-12-1999: Autore Diego Ferrari
		modificata 31-10-2006 (EnMe) per adeguamento alla nuova distinta base con versione del figlio.
		
		
*/
long   ll_num_max_tenda,ll_num_commessa,ll_num_righe,ll_num_commessa_opt
string ls_cod_cliente,ls_rag_soc_1,ls_rag_soc_2,ls_indirizzo,ls_cap,ls_localita,ls_provincia,ls_telefono,ls_fax, &
		 ls_cod_operatore,ls_cod_giro_consegna,ls_des_reparto,&
		 ls_des_comando,ls_des_supporto,ls_cod_non_a_magazzino,ls_des_colore_telo,ls_cod_mant_non_a_magazzino, &
		 ls_flag_tipo_mantovana,ls_colore_passamaneria,ls_colore_cordolo,&
		 ls_des_colore_mantovana,ls_flag_allegato,ls_cod_prod_finito,ls_cod_prodotto,ls_cod_dim_x,ls_cod_dim_y,&
		 ls_cod_dim_z,ls_cod_dim_t,ls_des_dim_x,ls_des_dim_y,ls_des_dim_z,ls_des_dim_t,ls_cod_tessuto,ls_cod_tessuto_mant,&
		 ls_flag_cordolo,ls_flag_passamaneria,ls_cod_verniciatura,ls_cod_comando,ls_pos_comando,ls_cod_colore_lastra,&
		 ls_cod_tipo_lastra,ls_cod_tipo_supporto,ls_flag_misura_luce_finita,ls_des_vernice_fc,ls_des_prodotto,& 
		 ls_des_colore_tessuto,ls_nota_dettaglio,ls_nota_configuratore,ls_cod_prodotto_opt,ls_cod_misura_opt, &
		 ls_nota_opt,ls_des_prodotto_opt
datetime ldt_data_consegna,ldt_data_pronto
dec{4} ldd_quan_ordine,ldd_quan_supporto,ldd_alt_mantovana,ldd_dim_x,ldd_dim_y,ldd_dim_z,ldd_dim_t,ldd_alt_asta,ldd_quan_ordine_opt
		 
integer li_risposta
boolean lb_inserito

select max(num_tenda)
into   :ll_num_max_tenda
from   tes_stampa_ordini
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione
and    cod_reparto=:fs_cod_reparto;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tes_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select data_pronto,
		 data_consegna,
		 cod_cliente,
		 cod_giro_consegna,
		 cod_operatore
into   :ldt_data_pronto,
		 :ldt_data_consegna,
		 :ls_cod_cliente,
		 :ls_cod_giro_consegna,
		 :ls_cod_operatore
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tes_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select des_reparto
into   :ls_des_reparto
from   anag_reparti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_reparto=:fs_cod_reparto;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_reparti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select quan_ordine,
		 num_commessa,
		 cod_prodotto,
		 nota_dettaglio
into   :ldd_quan_ordine,
		 :ll_num_commessa,
		 :ls_cod_prodotto,
		 :ls_nota_dettaglio
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione
and    prog_riga_ord_ven=:fl_prog_riga_comanda;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tes_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if
		 
select rag_soc_1,
		 rag_soc_2,
		 indirizzo,
		 cap,
		 localita,
		 provincia,
		 telefono,
		 fax
into   :ls_rag_soc_1,
		 :ls_rag_soc_2,
		 :ls_indirizzo,
		 :ls_cap,
		 :ls_localita,
		 :ls_provincia,
		 :ls_telefono,
		 :ls_fax
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_clienti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select cod_non_a_magazzino,
		 cod_prod_finito,
		 dim_x,
		 dim_y,
		 dim_z,
		 dim_t,
		 cod_tessuto,
		 cod_tessuto_mant,
		 flag_tipo_mantovana,
		 alt_mantovana,
		 flag_cordolo,
		 flag_passamaneria,
		 cod_verniciatura,
		 cod_comando,
		 pos_comando,
		 alt_asta,
		 cod_colore_lastra,
		 cod_tipo_lastra,
		 cod_tipo_supporto,
		 flag_misura_luce_finita,
		 note,
		 colore_passamaneria,
		 colore_cordolo,
		 des_vernice_fc
into   :ls_cod_non_a_magazzino,
		 :ls_cod_prod_finito,
		 :ldd_dim_x,
		 :ldd_dim_y,
		 :ldd_dim_z,
		 :ldd_dim_t,
		 :ls_cod_tessuto,
		 :ls_cod_tessuto_mant,
		 :ls_flag_tipo_mantovana,
		 :ldd_alt_mantovana,
		 :ls_flag_cordolo,
		 :ls_flag_passamaneria,
		 :ls_cod_verniciatura,
		 :ls_cod_comando,
		 :ls_pos_comando,
		 :ldd_alt_asta,
		 :ls_cod_colore_lastra,
		 :ls_cod_tipo_lastra,
		 :ls_cod_tipo_supporto,
		 :ls_flag_misura_luce_finita,
		 :ls_nota_configuratore,
		 :ls_colore_passamaneria,
		 :ls_colore_cordolo,
		 :ls_des_vernice_fc
from   comp_det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione
and    prog_riga_ord_ven=:fl_prog_riga_comanda;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura comp_det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select cod_variabile
into   :ls_cod_dim_x
from   tab_variabili_formule
where  cod_azienda=:s_cs_xx.cod_azienda
and    nome_campo_database='dim_x';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_variabili_formule (dim_x). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select cod_variabile
into   :ls_cod_dim_y
from   tab_variabili_formule
where  cod_azienda=:s_cs_xx.cod_azienda
and    nome_campo_database='dim_y';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_variabili_formule (dim_y). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select cod_variabile
into   :ls_cod_dim_z
from   tab_variabili_formule
where  cod_azienda=:s_cs_xx.cod_azienda
and    nome_campo_database='dim_z';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_variabili_formule (dim_z). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select cod_variabile
into   :ls_cod_dim_t
from   tab_variabili_formule
where  cod_azienda=:s_cs_xx.cod_azienda
and    nome_campo_database='dim_t';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_variabili_formule (dim_t). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select 	des_variabile
into   	:ls_des_dim_x
from   	tab_des_variabili
where  	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:ls_cod_prodotto and
			cod_variabile=:ls_cod_dim_x and
			flag_ipertech_apice = 'S';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_des_variabili (dim_x). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select 	des_variabile
into   	:ls_des_dim_y
from   	tab_des_variabili
where  	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:ls_cod_prodotto and
			cod_variabile=:ls_cod_dim_y and
			flag_ipertech_apice ='S';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_des_variabili (dim_y). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select 	des_variabile
into   	:ls_des_dim_z
from   	tab_des_variabili
where  	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:ls_cod_prodotto and
			cod_variabile=:ls_cod_dim_z and
			flag_ipertech_apice ='S';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_des_variabili (dim_z). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select 	des_variabile
into   	:ls_des_dim_t
from   	tab_des_variabili
where  	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:ls_cod_prodotto and
			cod_variabile=:ls_cod_dim_t and
			flag_ipertech_apice ='S';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_des_variabili (dim_t). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

ll_num_righe=fdw_report_1.rowcount() + 1

if not isnull(ls_cod_prod_finito) then
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice","Codice:")
	fdw_report_1.setitem(ll_num_righe,"descrizione",ls_cod_prod_finito)
	
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",2)
	fdw_report_1.setitem(ll_num_righe,"visibile_2",1)
	fdw_report_1.setitem(ll_num_righe,"codice","Descrizione:")
	fdw_report_1.setitem(ll_num_righe,"descrizione",ls_des_prodotto)
	fdw_report_1.setitem(ll_num_righe,"quantita",string(ldd_quan_ordine))
	
end if

if not isnull(ls_cod_tessuto) then
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_tessuto;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice","")
	fdw_report_1.setitem(ll_num_righe,"descrizione",ls_des_prodotto)
	
end if


if ldd_dim_x <> 0 then
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice",ls_des_dim_x)
	fdw_report_1.setitem(ll_num_righe,"descrizione",string(ldd_dim_x))
end if

if ldd_dim_y <> 0 then
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice",ls_des_dim_y)
	fdw_report_1.setitem(ll_num_righe,"descrizione",string(ldd_dim_y))
end if

if ldd_dim_z <> 0 then
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice",ls_des_dim_z)
	fdw_report_1.setitem(ll_num_righe,"descrizione",string(ldd_dim_z))
end if

if ldd_dim_t <> 0 then
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice",ls_des_dim_t)
	fdw_report_1.setitem(ll_num_righe,"descrizione",string(ldd_dim_t))
end if

if not isnull(ls_flag_misura_luce_finita) then
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice","Mis. finita/luce:")
	fdw_report_1.setitem(ll_num_righe,"descrizione",ls_flag_misura_luce_finita)
end if

if not isnull(ls_cod_verniciatura) then
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_verniciatura;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice","Verniciatura:")
	fdw_report_1.setitem(ll_num_righe,"descrizione",ls_des_prodotto)
end if

if not isnull(ls_des_vernice_fc) then
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"visibile_2",1)
	fdw_report_1.setitem(ll_num_righe,"visibile_1",1)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",3)
	fdw_report_1.setitem(ll_num_righe,"nota","NOTA PER VERNICIATURA:")
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"visibile_2",1)
	fdw_report_1.setitem(ll_num_righe,"visibile_1",1)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",3)
	fdw_report_1.setitem(ll_num_righe,"nota",ls_des_vernice_fc)

end if

if not isnull(ls_cod_comando) then
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_comando;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)

	fdw_report_1.setitem(ll_num_righe,"tipo_riga",2)
	fdw_report_1.setitem(ll_num_righe,"visibile_2",1)

	fdw_report_1.setitem(ll_num_righe,"codice","Tipo Comando:")
	fdw_report_1.setitem(ll_num_righe,"descrizione",ls_des_prodotto)
	fdw_report_1.setitem(ll_num_righe,"quantita",string(ldd_quan_ordine))
	
end if

if not isnull(ls_pos_comando) then
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice","Posizione Com:")
	fdw_report_1.setitem(ll_num_righe,"descrizione",ls_pos_comando)
end if

if ldd_alt_asta<>0 then
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice","H Com./Asta:")
	fdw_report_1.setitem(ll_num_righe,"descrizione",string(ldd_alt_asta))
end if

if not isnull(ls_cod_tipo_supporto) then
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_tipo_supporto;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	select quan_utilizzo
	into   :ldd_quan_supporto
	from   varianti_det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:fi_anno_registrazione
	and    num_registrazione=:fl_num_registrazione
	and    prog_riga_ord_ven=:fl_prog_riga_comanda
	and    cod_prodotto_figlio=:ls_cod_tipo_supporto;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura varianti_det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)

	fdw_report_1.setitem(ll_num_righe,"tipo_riga",2)
	fdw_report_1.setitem(ll_num_righe,"visibile_2",1)

	fdw_report_1.setitem(ll_num_righe,"codice","Tipo supporti:")
	fdw_report_1.setitem(ll_num_righe,"descrizione",ls_des_prodotto)
	fdw_report_1.setitem(ll_num_righe,"quantita",string(ldd_quan_supporto))
	
end if

if not isnull(ls_cod_non_a_magazzino) then
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice","Col.Telo/Lastra:")
	fdw_report_1.setitem(ll_num_righe,"descrizione",ls_cod_non_a_magazzino)

	select des_colore_tessuto
	into   :ls_des_colore_tessuto
	from   tab_colori_tessuti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_colore_tessuto=:ls_cod_non_a_magazzino;
	
	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_colori_tessuti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice","Des.Telo/Lastra:")
	fdw_report_1.setitem(ll_num_righe,"descrizione",ls_des_colore_tessuto)

end if

if not isnull(ls_cod_mant_non_a_magazzino) then
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice","Tess.Mantovana:")
	fdw_report_1.setitem(ll_num_righe,"descrizione",ls_cod_mant_non_a_magazzino)

	select des_colore_tessuto
	into   :ls_des_colore_tessuto
	from   tab_colori_tessuti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_colore_tessuto=:ls_cod_mant_non_a_magazzino;
	
	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_colori_tessuti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice","Des.Mantovana:")
	fdw_report_1.setitem(ll_num_righe,"descrizione",ls_des_colore_tessuto)

end if

if not isnull(ls_flag_tipo_mantovana) then
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice","Tipo/H Mant.:")
	fdw_report_1.setitem(ll_num_righe,"descrizione",ls_flag_tipo_mantovana + " / " + string(ldd_alt_mantovana))
end if

if not isnull(ls_colore_passamaneria) then
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",1)
	fdw_report_1.setitem(ll_num_righe,"codice","Passam./Cordolo")
	if isnull(ls_colore_cordolo) then ls_colore_cordolo = " "
	fdw_report_1.setitem(ll_num_righe,"descrizione",ls_colore_passamaneria + " / " + ls_colore_cordolo)
end if

ll_num_righe++
fdw_report_1.insertrow(ll_num_righe)

fdw_report_1.setitem(ll_num_righe,"tipo_riga",5)
fdw_report_1.setitem(ll_num_righe,"nota_1","           ")

lb_inserito = false

// inizio inserimento optional

declare r_det_ord_ven_app cursor for 
select  cod_prodotto,
		  cod_misura,
		  quan_ordine,
		  nota_dettaglio,
		  num_commessa
from    det_ord_ven
where   cod_azienda=:s_cs_xx.cod_azienda
and     anno_registrazione=:fi_anno_registrazione
and     num_registrazione=:fl_num_registrazione
and     num_riga_appartenenza=:fl_prog_riga_comanda;

open r_det_ord_ven_app;

do while true
	fetch r_det_ord_ven_app
	into  :ls_cod_prodotto_opt,
			:ls_cod_misura_opt,
			:ldd_quan_ordine_opt,
			:ls_nota_opt,
			:ll_num_commessa_opt;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura cursore det_ord_ven per optional. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close r_det_ord_ven_app;
		return -1
	end if

	select des_prodotto
	into   :ls_des_prodotto_opt
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto_opt;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_prodotti per optional. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close r_det_ord_ven_app;
		return -1
	end if

	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	
	if lb_inserito = false then
		fdw_report_1.setitem(ll_num_righe,"tipo_riga",4)
		fdw_report_1.setitem(ll_num_righe,"codice","Codice Optional")
		fdw_report_1.setitem(ll_num_righe,"descrizione","Descrizione Optional")
		fdw_report_1.setitem(ll_num_righe,"quantita","Q.tà")
		fdw_report_1.setitem(ll_num_righe,"um_opt","U.M.")
		fdw_report_1.setitem(ll_num_righe,"nota_optional","Nota di prodotto")
		fdw_report_1.setitem(ll_num_righe,"num_com_opt","Num. Com.")
	
		ll_num_righe++
		fdw_report_1.insertrow(ll_num_righe)
		
		fdw_report_1.setitem(ll_num_righe,"tipo_riga",4)
//		fdw_report_1.setitem(ll_num_righe,"visibile_3",1)
//		fdw_report_1.setitem(ll_num_righe,"visibile_2",1)
//		fdw_report_1.setitem(ll_num_righe,"visibile_1",1)
		fdw_report_1.setitem(ll_num_righe,"codice",ls_cod_prodotto_opt)
		fdw_report_1.setitem(ll_num_righe,"descrizione",ls_des_prodotto_opt)
		fdw_report_1.setitem(ll_num_righe,"um_opt",ls_cod_misura_opt)
		fdw_report_1.setitem(ll_num_righe,"quantita",string(ldd_quan_ordine_opt))
		fdw_report_1.setitem(ll_num_righe,"nota_optional",ls_nota_opt)
		fdw_report_1.setitem(ll_num_righe,"num_com_opt",string(ll_num_commessa_opt))
		
		lb_inserito = true
		
	else
		fdw_report_1.setitem(ll_num_righe,"tipo_riga",4)
//		fdw_report_1.setitem(ll_num_righe,"visibile_3",1)
//		fdw_report_1.setitem(ll_num_righe,"visibile_2",1)
//		fdw_report_1.setitem(ll_num_righe,"visibile_1",1)
		fdw_report_1.setitem(ll_num_righe,"codice",ls_cod_prodotto_opt)
		fdw_report_1.setitem(ll_num_righe,"descrizione",ls_des_prodotto_opt)
		fdw_report_1.setitem(ll_num_righe,"um_opt",ls_cod_misura_opt)
		fdw_report_1.setitem(ll_num_righe,"quantita",string(ldd_quan_ordine_opt))
		fdw_report_1.setitem(ll_num_righe,"nota_optional",ls_nota_opt)
		fdw_report_1.setitem(ll_num_righe,"num_com_opt",string(ll_num_commessa_opt))
		
	end if	
	
loop		
			
close r_det_ord_ven_app;

// fine inserimento optional

ll_num_righe++
fdw_report_1.insertrow(ll_num_righe)

fdw_report_1.setitem(ll_num_righe,"tipo_riga",5)
fdw_report_1.setitem(ll_num_righe,"nota_1","           ")

if not isnull(ls_nota_configuratore) then
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",5)
	fdw_report_1.setitem(ll_num_righe,"nota_1",ls_nota_configuratore)
end if

if not isnull(ls_nota_dettaglio) then
	ll_num_righe++
	fdw_report_1.insertrow(ll_num_righe)
	fdw_report_1.setitem(ll_num_righe,"tipo_riga",5)
	fdw_report_1.setitem(ll_num_righe,"nota_1",ls_nota_dettaglio)
end if

if isnull(ldt_data_pronto) then
	fdw_report_1.Modify("data_pronto.text=' '")
else
	fdw_report_1.Modify("data_pronto.text='"+ string(date(ldt_data_pronto)) +"'")
end if

if isnull(ldt_data_consegna) then
	fdw_report_1.Modify("data_consegna.text=' '")
else
	fdw_report_1.Modify("data_consegna.text='"+ string(date(ldt_data_consegna)) + "'")
end if

fdw_report_1.Modify("num_ordine.text='"+ string(fl_num_registrazione)+ "   " + string(fl_num_tenda) +"/" + string(ll_num_max_tenda) + "'")

if isnull(ll_num_commessa) then
	fdw_report_1.Modify("num_commessa.text=' '")
else
	fdw_report_1.Modify("num_commessa.text='"+ string(ll_num_commessa) + "'")
end if

if isnull(ls_cod_giro_consegna) then
	fdw_report_1.Modify("des_giro.text=' '")
else
	fdw_report_1.Modify("des_giro.text='"+ ls_cod_giro_consegna + "'")
end if

fdw_report_1.Modify("cod_cliente.text='"+ ls_cod_cliente + "'")
fdw_report_1.Modify("rag_soc_1.text='" + ls_rag_soc_1 + "'")

if isnull(ls_rag_soc_2) then
	fdw_report_1.Modify("rag_soc_2.text=' '")
else
	fdw_report_1.Modify("rag_soc_2.text='"+ ls_rag_soc_2 + "'")
end if
	
if isnull(ls_indirizzo) then
	fdw_report_1.Modify("indirizzo.text=' '")
else
	fdw_report_1.Modify("indirizzo.text='"+ ls_indirizzo + "'")
end if

if isnull(ls_cap) then ls_cap = ' '
fdw_report_1.Modify("cap.text='"+ ls_cap + "'")


if isnull(ls_localita) then ls_localita = ' '
fdw_report_1.Modify("localita.text='"+ ls_localita + "'")

if isnull(ls_provincia) then ls_provincia = ' '
fdw_report_1.Modify("provincia.text='"+ ls_provincia + "'")

if isnull(ls_telefono) then ls_telefono = ' '
fdw_report_1.Modify("telefono.text='"+ ls_telefono + "'")

if isnull(ls_fax) then ls_fax = ' '
fdw_report_1.Modify("fax.text='"+ ls_fax + "'")

if isnull(ls_cod_operatore) then ls_cod_operatore = ' '
fdw_report_1.Modify("cod_operatore.text='"+ ls_cod_operatore + "'")

if isnull(ls_des_reparto) then ls_des_reparto=' '
fdw_report_1.Modify("des_reparto.text='"+ ls_des_reparto + "'")

fdw_report_1.Modify("vedi_allegato.visible=1")

return 0

end function

on uo_f_report_1.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_f_report_1.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

