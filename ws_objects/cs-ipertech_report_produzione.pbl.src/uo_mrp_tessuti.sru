﻿$PBExportHeader$uo_mrp_tessuti.sru
forward
global type uo_mrp_tessuti from nonvisualobject
end type
end forward

global type uo_mrp_tessuti from nonvisualobject
end type
global uo_mrp_tessuti uo_mrp_tessuti

type variables
boolean ib_debug=false
end variables

forward prototypes
public function integer uof_mrp_tessuto (string fs_cod_colore, string fs_cod_tessuto, datetime fdt_data_riferimento, ref decimal fd_fabbisogno, ref string fs_errore)
end prototypes

public function integer uof_mrp_tessuto (string fs_cod_colore, string fs_cod_tessuto, datetime fdt_data_riferimento, ref decimal fd_fabbisogno, ref string fs_errore);/*
FUNZIONE DI CALCOLO DISPONIBILITA' DEL TESSUTO AD UNA CERTA DATA.

Parametri in ingresso
-------------------------------------
fs_cod_colore				STRING		codice colore del tessuto
fs_cod_tessuto				STRING		codice del tessuto
fdt_data_riferimento		DATETIME		Data di riferimento per il calcolo
fs_errore					STRING		Eventuale errore di ritorno

*/

string ls_cod_prodotto,ls_flag_evasione,ls_cod_colore_tessuto,ls_cod_tessuto,ls_cod_reparto,ls_flag_fine_fase, ls_cod_versione, &
       ls_materia_prima[], ls_versione_prima[], ls_errore, ls_flag_blocco_det, ls_flag_blocco_tes, ls_vuoto[], &
		 ls_path, ls_file, ls_debug, ls_null
long   ll_anno_reg_ord_ven, ll_num_reg_ord_ven, ll_prog_riga_ord_ven, ll_ret,ll_i,ll_file, ll_progr_det_produzione,ll_cont_sessioni
dec{4} ld_quan_ordine,ld_quan_in_evasione,ld_quan_evasa,ld_quan_fabbisogno
double ld_quantita_utilizzo[],ld_quantita_utilizzo_prec, ld_vuoto[]
datetime ldt_data_oggi
uo_funzioni_1 luo_funzioni

DECLARE cu_colori_tessuti CURSOR FOR  
SELECT det_ord_ven.anno_registrazione ,
		det_ord_ven.num_registrazione ,
		det_ord_ven.prog_riga_ord_ven ,
		det_ord_ven.cod_prodotto,   
		det_ord_ven.cod_versione,
		det_ord_ven.quan_ordine,   
		det_ord_ven.quan_in_evasione,   
		det_ord_ven.quan_evasa,   
		det_ord_ven.flag_evasione,   
		det_ord_ven.flag_blocco,
		comp_det_ord_ven.cod_non_a_magazzino,   
		comp_det_ord_ven.cod_tessuto,
		tes_ord_ven.flag_blocco
 FROM det_ord_ven left OUTER JOIN comp_det_ord_ven ON  comp_det_ord_ven.cod_azienda = det_ord_ven.cod_azienda AND 
        																 comp_det_ord_ven.anno_registrazione = det_ord_ven.anno_registrazione AND 
																		 comp_det_ord_ven.num_registrazione = det_ord_ven.num_registrazione AND 
																		 comp_det_ord_ven.prog_riga_ord_ven = det_ord_ven.prog_riga_ord_ven
						left OUTER JOIN tes_ord_ven      ON  tes_ord_ven.cod_azienda = det_ord_ven.cod_azienda AND 
                                                       tes_ord_ven.anno_registrazione = det_ord_ven.anno_registrazione AND 
 																		 tes_ord_ven.num_registrazione = det_ord_ven.num_registrazione 		
WHERE det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda AND  
		det_ord_ven.flag_evasione in ('A', 'P')  and
		comp_det_ord_ven.cod_non_a_magazzino like :fs_cod_colore and
		comp_det_ord_ven.cod_tessuto like :fs_cod_tessuto and
		det_ord_ven.data_consegna < :fdt_data_riferimento and
		det_ord_ven.data_consegna > :ldt_data_oggi;
		
		
		
		
setnull(ls_null)

ldt_data_oggi = datetime(relativedate(today(),-365), 00:00:00)

if ib_debug then
	ll_file = GetFileSaveName("DEBUG MRP TESSUTI", ls_path, ls_file, "LOG", "All Files (*.*),*.*" , "C:\", 32770)
	if ll_file < 1 then ib_debug = false
	
	ll_file = fileopen(ls_path, linemode!, Write!,LockReadWrite!,replace!)
	if ll_file < 0 then
		g_mb.messagebox("MRP TESSUTI","Errore in apertura file di LOG del debug MRP tessuti")
		ib_debug = false
	end if
	
	ls_debug =  "ANNO ORDINE" + "~t"
	ls_debug += "NUMERO ORDINE" + "~t"
	ls_debug += "RIGA ORDINE" + "~t"
	ls_debug += "COD PRODOTTO FINITO" + "~t"
	ls_debug += "VERSIONE DB" + "~t"
	ls_debug += "QUAN ORDINE RESIDUA" + "~t"
	ls_debug += "QUAN TESSUTO DB" + "~t"
	ls_debug += "PROGRESSIVO FABBISOGNO"
	
	filewrite(ll_file,ls_debug)
	
end if

fd_fabbisogno = 0

if isnull(ls_cod_colore_tessuto) or len(ls_cod_colore_tessuto) < 1 then
	ls_cod_colore_tessuto = "%"
end if

open cu_colori_tessuti;
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in OPEN cursore cu_colori_tessuti~r~n" + sqlca.sqlerrtext
	return -1
end if

ld_quan_fabbisogno = 0

do while true
	fetch cu_colori_tessuti into :ll_anno_reg_ord_ven, :ll_num_reg_ord_ven, :ll_prog_riga_ord_ven, :ls_cod_prodotto,:ls_cod_versione, :ld_quan_ordine,:ld_quan_in_evasione,:ld_quan_evasa,:ls_flag_evasione,:ls_flag_blocco_det, :ls_cod_colore_tessuto,:ls_cod_tessuto, :ls_flag_blocco_tes;
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in FETCH cursore cu_colori_tessuti~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	// cerco il reparto associato il gruppo prodotto (A5000, A5001, ECC....)
	select cod_reparto
	into   :ls_cod_reparto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_tessuto;
			 
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in ricerca reparto in anagrafica prodotti~r~n" + sqlca.sqlerrtext
		close cu_colori_tessuti;
		return -1
	end if
			 
	if sqlca.sqlcode = 100 then
		fs_errore = fs_errore + "~r~nIl tessuto "+ls_cod_tessuto+" non esiste in anagrafica prodotti~r~n" + sqlca.sqlerrtext
		continue
	end if
	
	// salto testate e righe bloccate
	if ls_flag_blocco_det = "S" then continue
	
	if ls_flag_blocco_tes = "S" then continue
	
	if isnull(ls_cod_reparto) or len(ls_cod_reparto) < 1 then
		fs_errore = fs_errore + "~r~nIl tessuto "+ls_cod_tessuto+" non ha associato alcun reparto~r~n" + sqlca.sqlerrtext
		continue
	end if
	
	// verifico lo stato di avanzamento della produzione
	ls_flag_fine_fase = ""
	
	select flag_fine_fase, progr_det_produzione
	into   :ls_flag_fine_fase, :ll_progr_det_produzione
	from   det_ordini_produzione
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_reg_ord_ven and 
			 num_registrazione	= :ll_num_reg_ord_ven and
			 prog_riga_ord_ven  = :ll_prog_riga_ord_ven and
			 cod_reparto = :ls_cod_reparto;

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in ricerca STATO produzione (det_ordini_produzione)~r~n" + sqlca.sqlerrtext
		close cu_colori_tessuti;
		return -1
	end if
			 
	// la fase di è iniziata, il telo è stato già tagliato e quindi scaricato;
	// non lo considero quindi fra gli impegnati.
	if ls_flag_fine_fase = "S" then
		continue
	else
		// se c'è anche solo una sessione di lavoro salta perchè vuol dire che è già iniziato il taglio.
		ll_cont_sessioni = 0
		
		select count(*)
		into   :ll_cont_sessioni
		from   sessioni_lavoro
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 progr_det_produzione = :ll_progr_det_produzione;
				 
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore in ricerca STATO produzione (sessioni lavoro)~r~n" + sqlca.sqlerrtext
			close cu_colori_tessuti;
			return -1
		end if
		
		if ll_cont_sessioni > 0 then continue
		
	end if
	// ricerco la quantità di tessuto prevista dalla variante della distinta base.
	
	// verifico intanto se per questo prodotto è prevista una distinta base; ovviamente dovrebbe esserlo sempre.
	if not isnull(ls_cod_versione) then
		
		ls_materia_prima[]   = ls_vuoto[]
		ls_versione_prima[]  = ls_vuoto[]
		ld_quantita_utilizzo = ld_vuoto[]
		
		luo_funzioni = create uo_funzioni_1
		
		ll_ret = luo_funzioni.uof_trova_mat_prime_varianti (	ls_cod_prodotto, &
																					ls_cod_versione, &
																					ls_null, &
																					ref ls_materia_prima[], &
																					ref ls_versione_prima[], &
																					ref ld_quantita_utilizzo[], &
																					1, &
																					ll_anno_reg_ord_ven, &
																					ll_num_reg_ord_ven, &
																					ll_prog_riga_ord_ven, &
																					"varianti_det_ord_ven", &
																					ls_null,&
																					ls_errore )
																					
		destroy luo_funzioni
		
		if ll_ret < 0 then
			fs_errore = "Errore in ricerca quantità variante in 'f_trova_mat_prima_varianti'~r~n" + ls_errore
		end if
		
		for ll_i = 1 to upperbound(ls_materia_prima)
			if ls_materia_prima[ll_i] = ls_cod_tessuto then
				ld_quan_fabbisogno += ld_quantita_utilizzo[ll_i] * (ld_quan_ordine - ld_quan_in_evasione - ld_quan_evasa)
				
				if ib_debug then
					
					ls_debug =  string(ll_anno_reg_ord_ven,"0000") + "~t"
					ls_debug += string(ll_num_reg_ord_ven,"000000") + "~t"
					ls_debug += string(ll_prog_riga_ord_ven,"0000") + "~t"
					ls_debug += ls_cod_prodotto + "~t"
					ls_debug += ls_cod_versione + "~t"
					ls_debug += string((ld_quan_ordine - ld_quan_in_evasione - ld_quan_evasa),"###,##0.0000") + "~t"
					ls_debug += string(ld_quantita_utilizzo[ll_i],"###,##0.0000") + "~t"
					ls_debug += string(ld_quan_fabbisogno,"###,##0.0000") + "~t"
					
					filewrite(ll_file,ls_debug)
					
				end if
			end if
		next
	end if
	
loop

if ib_debug then
	fileclose(ll_file)
end if

close cu_colori_tessuti;

fd_fabbisogno = ld_quan_fabbisogno

return 0
end function

on uo_mrp_tessuti.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_mrp_tessuti.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

