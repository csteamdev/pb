﻿$PBExportHeader$uo_tree_lancio_produzione.sru
forward
global type uo_tree_lancio_produzione from treeview
end type
end forward

global type uo_tree_lancio_produzione from treeview
integer width = 997
integer height = 1696
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type
global uo_tree_lancio_produzione uo_tree_lancio_produzione

type variables
boolean ib_menu = true, ib_mostra_ordini_chiusi = false
long il_picture[], il_handle
end variables

forward prototypes
private function integer uof_add_picture (string as_image_filename)
private subroutine uof_reset ()
public subroutine uof_add_ramo_nuovo_lancio ()
public function integer uof_carica_tv (long al_num_giorni, ref string as_errore)
end prototypes

private function integer uof_add_picture (string as_image_filename);return addpicture(s_cs_xx.volume + s_cs_xx.risorse + as_image_filename) 


end function

private subroutine uof_reset ();setredraw(false)
deletepictures()
deleteitem(0)
setredraw(true)
end subroutine

public subroutine uof_add_ramo_nuovo_lancio ();long	 ll_item
s_tree_lancio_produzione	lstr_record
treeviewitem	ltvi_campo

lstr_record.livello = -1
ltvi_campo.data = lstr_record
ltvi_campo.label = "Nuova Sessione Produzione"
ltvi_campo.pictureindex = il_picture[1]
ltvi_campo.selectedpictureindex =  il_picture[1]
ltvi_campo.overlaypictureindex =  il_picture[1]
ltvi_campo.children = true
ltvi_campo.selected = false
ll_item = insertitemlast(0, ltvi_campo)

end subroutine

public function integer uof_carica_tv (long al_num_giorni, ref string as_errore);string	ls_sql, ls_errore
long	ll_ret, ll_i, ll_item, ll_stato, ll_picture_index
date	ld_data
datetime ldt_data_indietro
datastore lds_data1
s_tree_lancio_produzione	lstr_record
treeviewitem	ltvi_campo
uo_produzione 	luo_prod


uof_reset()

il_picture[1] = uof_add_picture("12.5\Lancio_prod_aperta.ico")
il_picture[2] = uof_add_picture("12.5\tv_lancio_prod_ottimizzato.png")
il_picture[3] = uof_add_picture("12.5\tv_ordine_cliente.png")
il_picture[4] = uof_add_picture("12.5\tv_righe_ordine.png")
il_picture[5] = uof_add_picture("12.5\Lancio_prod_chiuso.ico")
il_picture[6] = uof_add_picture("12.5\Lancio_prod_parziale.ico")


// inserisco ramo iniziale
uof_add_ramo_nuovo_lancio()

ls_sql = "select id, descrizione, data_registrazione, cod_utente from tes_lancio_prod where cod_azienda = '$1' and flag_terminato='N' "

if al_num_giorni > 0 then
	ld_data = relativedate(today(), al_num_giorni * -1)
	ldt_data_indietro = datetime(ld_data, 00:00:00)
	
	ls_sql = ls_sql + " and data_registrazione >= '" + string(ldt_data_indietro, s_cs_xx.db_funzioni.formato_data) + "' "
	
end if

ls_sql = ls_sql + " order by data_registrazione DESC, id DESC "

ls_sql = g_str.format(ls_sql, s_cs_xx.cod_azienda)

ll_ret =guo_functions.uof_crea_datastore(lds_data1, ls_sql )
if ll_ret < 0 then
	as_errore = "Errore nella estrazione dei lanci di produzione"
	return -1
end if

luo_prod = CREATE uo_produzione

for ll_i = 1  to ll_ret
	
	ll_stato = luo_prod.uof_stato_prod_tes_lancio_prod( lds_data1.getitemnumber(ll_i, 1), ls_errore)
	if not ib_mostra_ordini_chiusi and ll_stato=1 then continue
	
	lstr_record.id_padre = lds_data1.getitemnumber(ll_i, 1)
	lstr_record.livello = 0
	ltvi_campo.data = lstr_record
	if len(lds_data1.getitemstring(ll_i, 2)) = 0 or isnull(lds_data1.getitemstring(ll_i, 2)) then
		ltvi_campo.label = g_str.format("$1 - $2 ($3)", string(lds_data1.getitemdatetime(ll_i, 3),"dd/mm/yyyy hh:mm:ss"), lds_data1.getitemstring(ll_i, 4),  lds_data1.getitemnumber(ll_i, 1))
	else
		ltvi_campo.label = g_str.format("$1 ($2)", lds_data1.getitemstring(ll_i, 2),lds_data1.getitemnumber(ll_i, 1) )
	end if
	
	choose case ll_stato
		case is < 0 
			as_errore = ls_errore
			return -1
		case 0
			ll_picture_index = 1
		case 1
			ll_picture_index = 5
		case 2
			ll_picture_index = 6
	end choose
	
	ltvi_campo.pictureindex = il_picture[ll_picture_index]
	ltvi_campo.selectedpictureindex =  il_picture[ll_picture_index]
	ltvi_campo.overlaypictureindex =  il_picture[ll_picture_index]
	ltvi_campo.children = true
	ltvi_campo.selected = false
	
	ll_item = insertitemlast(0, ltvi_campo)
	
next

return 0
end function

on uo_tree_lancio_produzione.create
end on

on uo_tree_lancio_produzione.destroy
end on

event itempopulate;string ls_sql
long ll_livello = 0, ll_ret, ll_i, ll_current_handle, ll_item
datastore lds_data1
treeviewitem ltv_item
s_tree_lancio_produzione ls_record, ls_record_null

if isnull(handle) or handle <= 0 then
	return 0
end if

ll_current_handle = handle

getitem(handle,ltv_item)

ls_record = ltv_item.data
ll_livello = ls_record.livello


setpointer(HourGlass!)

choose case ll_livello
	case 0		// sono sulla radice
		// estraggo tutti gli ordini
		ls_sql = 	" select distinct A.anno_reg_ord_ven, A.num_reg_ord_ven, B.data_consegna, B.cod_cliente, C.rag_soc_1 " + &
					" from det_lancio_prod A " + &
					" join tes_ord_ven B on A.cod_azienda = B.cod_azienda and A.anno_reg_ord_ven = B.anno_registrazione and A.num_reg_ord_ven = B.num_registrazione " + &
					" join anag_clienti C on B.cod_azienda = C.cod_azienda and B.cod_cliente = C.cod_cliente " + &
					" where id_tes_lancio_prod = " + string(ls_record.id_padre) + &
					" order by C.rag_soc_1, A.anno_reg_ord_ven, A.num_reg_ord_ven "
					
		ll_ret = guo_functions.uof_crea_datastore(lds_data1, ls_sql)
		
		for ll_i = 1 to ll_ret
			ls_record.anno_ordine = lds_data1.getitemnumber(ll_i,1)
			ls_record.num_ordine = lds_data1.getitemnumber(ll_i,2)
			ls_record.livello = 1
			ltv_item.data = ls_record
			ltv_item.label = g_str.format("$1-$2 ($3) $4 - $5", 	&
				lds_data1.getitemnumber(ll_i, 1),&
				lds_data1.getitemnumber(ll_i, 2),&
				lds_data1.getitemstring(ll_i, 4), & 
				lds_data1.getitemstring(ll_i, 5),  &
				string(lds_data1.getitemdatetime(ll_i, 3),"dd/mm/yyyy hh:mm:ss") )
			
			ltv_item.pictureindex = il_picture[3]
			ltv_item.selectedpictureindex =  il_picture[3]
			ltv_item.overlaypictureindex =  il_picture[3]
			ltv_item.children = true
			ltv_item.selected = false
			
			ll_item = insertitemlast(ll_current_handle, ltv_item)

		next
		
		destroy lds_data1
		
	case 1		// sono sull'ordine
		// estraggo le righe di ordine con i prodotti inclusi nell'avanzamento
		
		ls_sql =  " select A.anno_registrazione, A.num_registrazione, A.prog_riga_ord_ven, A.cod_prodotto, B.des_prodotto, C.cod_reparto, C.flag_fine_fase " + &
					" from det_ord_ven A " + &
					" join anag_prodotti B on B.cod_azienda = A.cod_azienda and B.cod_prodotto = A.cod_prodotto " + &
					" join det_ordini_produzione C on C.anno_registrazione = A.anno_registrazione and C.num_registrazione =A.num_registrazione and C.prog_riga_ord_ven =A.prog_riga_ord_ven " + &
					g_str.format(" where A.cod_azienda = '$1' and A.anno_registrazione = $2 and A.num_registrazione=$3",s_cs_xx.cod_azienda, ls_record.anno_ordine,ls_record.num_ordine)
		
		ll_ret = guo_functions.uof_crea_datastore(lds_data1, ls_sql)
		
		for ll_i = 1 to ll_ret
			ls_record = ls_record_null
			ls_record.anno_ordine = lds_data1.getitemnumber(ll_i,1)
			ls_record.num_ordine = lds_data1.getitemnumber(ll_i,2)
			ls_record.prog_riga_ord_ven = lds_data1.getitemnumber(ll_i,3)
			ls_record.livello = 2
			ltv_item.data = ls_record
			ltv_item.label = g_str.format("$1-$2 $3 $4 ", 	&
				lds_data1.getitemnumber(ll_i, 3),&
				lds_data1.getitemstring(ll_i, 4),&
				lds_data1.getitemstring(ll_i, 5), & 
				lds_data1.getitemstring(ll_i, 6) )
			if lds_data1.getitemstring(ll_i, 7) = "S" then
				ltv_item.label = ltv_item.label + " <TERMINATA>"
			else
				ltv_item.label = ltv_item.label + " <APERTA>"
			end if				
			
			ltv_item.pictureindex = il_picture[4]
			ltv_item.selectedpictureindex =  il_picture[4]
			ltv_item.overlaypictureindex =  il_picture[4]
			ltv_item.children = false
			ltv_item.selected = false
			
			ll_item = insertitemlast(ll_current_handle, ltv_item)

		next
		
		
		
	case 2		// sono sulla riga ordine
		// al momento non faccio nulla, però potrei estrarre i reparti coinvolti o altre informazioni utili
		
end choose


setpointer(Arrow!)
end event

event rightclicked;if ib_menu then
	
	long  ll_livello
	treeviewitem ltv_item
	s_tree_lancio_produzione ls_record
	m_pop_tree_lancio_produzione lm_menu
	m_pop_tree_lancio_carica_righe lm_menu_righe
	
	il_handle = handle
	

	if handle > 0 then
	
		getitem(handle,ltv_item)
		ls_record = ltv_item.data
		ll_livello = ls_record.livello
		
		CHOOSE CASE ll_livello
			case -1
				lm_menu_righe = create m_pop_tree_lancio_carica_righe
				lm_menu_righe.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
			case 0
				lm_menu = create m_pop_tree_lancio_produzione
				lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
		end choose
		
		// verifico se è stato eseguito il lancio di produzione; se non è stato fatto inibisco la stampa produzione e lista preparazione
		//lm_menu.m_stampalistapreparazione.enabled=false
		
	else
		lm_menu = create m_pop_tree_lancio_produzione
		lm_menu.m_ottimizza.enabled=false
		lm_menu.m_stampa_ordini.enabled=false
		lm_menu.m_elimina_lancio.enabled=false
		lm_menu.m_lancio_produzione.enabled=false
		lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
		
	end if
	
	destroy lm_menu
	
end if	

end event

