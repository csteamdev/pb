﻿$PBExportHeader$w_commesse_att_ptenda.srw
forward
global type w_commesse_att_ptenda from w_commesse_attivazione
end type
end forward

global type w_commesse_att_ptenda from w_commesse_attivazione
end type
global w_commesse_att_ptenda w_commesse_att_ptenda

on w_commesse_att_ptenda.create
call super::create
end on

on w_commesse_att_ptenda.destroy
call super::destroy
end on

type cb_costi_correttivi from w_commesse_attivazione`cb_costi_correttivi within w_commesse_att_ptenda
end type

type dw_commesse_lista from w_commesse_attivazione`dw_commesse_lista within w_commesse_att_ptenda
end type

event dw_commesse_lista::updatestart;call super::updatestart;string ls_messaggio

long	 ll_i, ll_anno_commessa, ll_num_commessa

uo_produzione luo_prod


for ll_i = 1 to deletedcount()
	
	ll_anno_commessa = getitemnumber(ll_i, "anno_commessa", delete!, true)
	ll_num_commessa = getitemnumber(ll_i, "num_commessa", delete!, true)
	
	//---------- Michele 28/01/2004 Cancellazione dati produzione ------------

	luo_prod = create uo_produzione
	
	if luo_prod.uof_elimina_prod_comm(ll_anno_commessa,ll_num_commessa,ls_messaggio) <> 0 then
		g_mb.messagebox("APICE",ls_messaggio,stopsign!)
		destroy luo_prod
		return 1
	end if
	
	destroy luo_prod
	
	//------------------------------------------------------------------------
	
next
end event

type cb_dettaglio from w_commesse_attivazione`cb_dettaglio within w_commesse_att_ptenda
end type

type cb_attiva from w_commesse_attivazione`cb_attiva within w_commesse_att_ptenda
end type

type em_anno_commessa from w_commesse_attivazione`em_anno_commessa within w_commesse_att_ptenda
end type

type cbx_forza_numerazione from w_commesse_attivazione`cbx_forza_numerazione within w_commesse_att_ptenda
end type

type sle_num_commessa from w_commesse_attivazione`sle_num_commessa within w_commesse_att_ptenda
end type

type cb_varianti from w_commesse_attivazione`cb_varianti within w_commesse_att_ptenda
end type

type cb_annulla_attiva from w_commesse_attivazione`cb_annulla_attiva within w_commesse_att_ptenda
end type

type st_4 from w_commesse_attivazione`st_4 within w_commesse_att_ptenda
end type

type cb_chiudi_commessa from w_commesse_attivazione`cb_chiudi_commessa within w_commesse_att_ptenda
end type

type gb_flag_tipo_avanzamento from w_commesse_attivazione`gb_flag_tipo_avanzamento within w_commesse_att_ptenda
end type

type cbx_assegnazione from w_commesse_attivazione`cbx_assegnazione within w_commesse_att_ptenda
end type

type cbx_attivazione from w_commesse_attivazione`cbx_attivazione within w_commesse_att_ptenda
end type

type cbx_automatico from w_commesse_attivazione`cbx_automatico within w_commesse_att_ptenda
end type

type cbx_chiusa_assegn from w_commesse_attivazione`cbx_chiusa_assegn within w_commesse_att_ptenda
end type

type cbx_chiusa_attiv from w_commesse_attivazione`cbx_chiusa_attiv within w_commesse_att_ptenda
end type

type cbx_chiusa_autom from w_commesse_attivazione`cbx_chiusa_autom within w_commesse_att_ptenda
end type

type cbx_nessuna from w_commesse_attivazione`cbx_nessuna within w_commesse_att_ptenda
end type

type st_num_commesse from w_commesse_attivazione`st_num_commesse within w_commesse_att_ptenda
end type

type cb_tutti from w_commesse_attivazione`cb_tutti within w_commesse_att_ptenda
end type

type st_5 from w_commesse_attivazione`st_5 within w_commesse_att_ptenda
end type

type st_3 from w_commesse_attivazione`st_3 within w_commesse_att_ptenda
end type

type cbx_simultaneo from w_commesse_attivazione`cbx_simultaneo within w_commesse_att_ptenda
end type

type st_cliente from w_commesse_attivazione`st_cliente within w_commesse_att_ptenda
end type

type st_anno from w_commesse_attivazione`st_anno within w_commesse_att_ptenda
end type

type st_numero from w_commesse_attivazione`st_numero within w_commesse_att_ptenda
end type

type st_2 from w_commesse_attivazione`st_2 within w_commesse_att_ptenda
end type

type st_1 from w_commesse_attivazione`st_1 within w_commesse_att_ptenda
end type

type dw_commesse_1 from w_commesse_attivazione`dw_commesse_1 within w_commesse_att_ptenda
end type

type mle_log from w_commesse_attivazione`mle_log within w_commesse_att_ptenda
end type

type dw_commesse_3 from w_commesse_attivazione`dw_commesse_3 within w_commesse_att_ptenda
end type

type dw_commesse_2 from w_commesse_attivazione`dw_commesse_2 within w_commesse_att_ptenda
end type

type dw_folder from w_commesse_attivazione`dw_folder within w_commesse_att_ptenda
end type

