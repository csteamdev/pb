﻿$PBExportHeader$w_gestione_avan_prod.srw
forward
global type w_gestione_avan_prod from w_cs_xx_principale
end type
type cb_elimina_det from commandbutton within w_gestione_avan_prod
end type
type cb_chiudi_det from commandbutton within w_gestione_avan_prod
end type
type cb_elimina_tes from commandbutton within w_gestione_avan_prod
end type
type cb_chiudi_tes from commandbutton within w_gestione_avan_prod
end type
type tab_1 from tab within w_gestione_avan_prod
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_1 from userobject within tab_1
end type
type tabpage_2 from userobject within tab_1
end type
type tabpage_2 from userobject within tab_1
end type
type tab_1 from tab within w_gestione_avan_prod
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type
type dw_det from uo_cs_xx_dw within w_gestione_avan_prod
end type
type dw_tes from uo_cs_xx_dw within w_gestione_avan_prod
end type
end forward

global type w_gestione_avan_prod from w_cs_xx_principale
integer width = 3355
integer height = 1804
string title = "Manutenzione Tabelle di Avanzamento Produzione"
boolean maxbox = false
boolean resizable = false
cb_elimina_det cb_elimina_det
cb_chiudi_det cb_chiudi_det
cb_elimina_tes cb_elimina_tes
cb_chiudi_tes cb_chiudi_tes
tab_1 tab_1
dw_det dw_det
dw_tes dw_tes
end type
global w_gestione_avan_prod w_gestione_avan_prod

on w_gestione_avan_prod.create
int iCurrent
call super::create
this.cb_elimina_det=create cb_elimina_det
this.cb_chiudi_det=create cb_chiudi_det
this.cb_elimina_tes=create cb_elimina_tes
this.cb_chiudi_tes=create cb_chiudi_tes
this.tab_1=create tab_1
this.dw_det=create dw_det
this.dw_tes=create dw_tes
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_elimina_det
this.Control[iCurrent+2]=this.cb_chiudi_det
this.Control[iCurrent+3]=this.cb_elimina_tes
this.Control[iCurrent+4]=this.cb_chiudi_tes
this.Control[iCurrent+5]=this.tab_1
this.Control[iCurrent+6]=this.dw_det
this.Control[iCurrent+7]=this.dw_tes
end on

on w_gestione_avan_prod.destroy
call super::destroy
destroy(this.cb_elimina_det)
destroy(this.cb_chiudi_det)
destroy(this.cb_elimina_tes)
destroy(this.cb_chiudi_tes)
destroy(this.tab_1)
destroy(this.dw_det)
destroy(this.dw_tes)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noenablepopup + c_closenosave)

dw_tes.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)

dw_det.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_nodelete,c_default)
end event

type cb_elimina_det from commandbutton within w_gestione_avan_prod
integer x = 2537
integer y = 1600
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elimina"
end type

event clicked;long 		ll_row, ll_prog_det


ll_row = dw_det.getrow()

if isnull(ll_row) or ll_row < 1 then
	g_mb.messagebox("APICE","Selezionare una riga prima di continuare!",stopsign!)
	return -1
end if

ll_prog_det = dw_det.getitemnumber(ll_row,"progr_det_produzione")

if isnull(ll_prog_det) or ll_prog_det < 1 then
	g_mb.messagebox("APICE","Selezionare una riga prima di continuare!",stopsign!)
	return -1
end if

if g_mb.messagebox("APICE","Eliminare tutti i dati di produzione per la riga selezionata?",question!,yesno!,2) = 2 then
	return -1
end if

delete from
	sessioni_lavoro
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_det_produzione = :ll_prog_det;
	
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in cancellazione sessioni: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

delete from
	det_ordini_produzione
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_det_produzione = :ll_prog_det;
	
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in cancellazione fasi: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

commit;

dw_det.triggerevent("pcd_retrieve")
end event

type cb_chiudi_det from commandbutton within w_gestione_avan_prod
integer x = 2926
integer y = 1600
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi"
end type

event clicked;datetime ldt_data_fine

dec{4}	ld_quan_prodotta

long 		ll_row, ll_prog_det, ll_return


ll_row = dw_det.getrow()

if isnull(ll_row) or ll_row < 1 then
	g_mb.messagebox("APICE","Selezionare una riga prima di continuare!",stopsign!)
	return -1
end if

ll_prog_det = dw_det.getitemnumber(ll_row,"progr_det_produzione")

if isnull(ll_prog_det) or ll_prog_det < 1 then
	g_mb.messagebox("APICE","Selezionare una riga prima di continuare!",stopsign!)
	return -1
end if

if g_mb.messagebox("APICE","Effettuare la chiusura manuale della riga selezionata?",question!,yesno!,2) = 2 then
	return -1
end if

setnull(s_cs_xx.parametri.parametro_d_1)

s_cs_xx.parametri.parametro_d_2 = dw_det.getitemnumber(ll_row,"quan_da_produrre")

setnull(s_cs_xx.parametri.parametro_data_1)

window_open(w_chiudi_prod_det,0)

ll_return = s_cs_xx.parametri.parametro_d_1

ld_quan_prodotta = s_cs_xx.parametri.parametro_d_2

ldt_data_fine = s_cs_xx.parametri.parametro_data_1

setnull(s_cs_xx.parametri.parametro_d_1)

setnull(s_cs_xx.parametri.parametro_d_2)

setnull(s_cs_xx.parametri.parametro_data_1)

if ll_return <> 0 then
	return -1
end if

update
	det_ordini_produzione
set
	quan_prodotta = :ld_quan_prodotta,
	flag_fine_fase = 'S',
	data_fine_fase = :ldt_data_fine
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_det_produzione = :ll_prog_det;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in chiusura fase: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

commit;

dw_det.triggerevent("pcd_retrieve")
end event

type cb_elimina_tes from commandbutton within w_gestione_avan_prod
integer x = 2537
integer y = 1600
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elimina"
end type

event clicked;long 		ll_row, ll_prog_tes


ll_row = dw_tes.getrow()

if isnull(ll_row) or ll_row < 1 then
	g_mb.messagebox("APICE","Selezionare una riga prima di continuare!",stopsign!)
	return -1
end if

ll_prog_tes = dw_tes.getitemnumber(ll_row,"progr_tes_produzione")

if isnull(ll_prog_tes) or ll_prog_tes < 1 then
	g_mb.messagebox("APICE","Selezionare una riga prima di continuare!",stopsign!)
	return -1
end if

if g_mb.messagebox("APICE","Eliminare tutti i dati di produzione per la riga selezionata?",question!,yesno!,2) = 2 then
	return -1
end if

delete from
	sessioni_lavoro
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_tes_produzione = :ll_prog_tes;
	
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in cancellazione sessioni: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

delete from
	det_ordini_produzione
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_tes_produzione = :ll_prog_tes;
	
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in cancellazione fasi: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

delete from
	tes_ordini_produzione
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_tes_produzione = :ll_prog_tes;
	
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in cancellazione tes_ordini_produzione: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

commit;

dw_tes.triggerevent("pcd_retrieve")

dw_det.triggerevent("pcd_retrieve")
end event

type cb_chiudi_tes from commandbutton within w_gestione_avan_prod
integer x = 2926
integer y = 1600
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi"
end type

event clicked;datetime ldt_data_fine

string	ls_cod_operaio

long 		ll_row, ll_prog_tes, ll_return, ll_num_colli


ll_row = dw_tes.getrow()

if isnull(ll_row) or ll_row < 1 then
	g_mb.messagebox("APICE","Selezionare una riga prima di continuare!",stopsign!)
	return -1
end if

ll_prog_tes = dw_tes.getitemnumber(ll_row,"progr_tes_produzione")

if isnull(ll_prog_tes) or ll_prog_tes < 1 then
	g_mb.messagebox("APICE","Selezionare una riga prima di continuare!",stopsign!)
	return -1
end if

if g_mb.messagebox("APICE","Effettuare la chiusura manuale della riga selezionata?",question!,yesno!,2) = 2 then
	return -1
end if

setnull(s_cs_xx.parametri.parametro_d_1)

setnull(s_cs_xx.parametri.parametro_d_2)

setnull(s_cs_xx.parametri.parametro_data_1)

setnull(s_cs_xx.parametri.parametro_s_1)

window_open(w_chiudi_prod_tes,0)

ll_return = s_cs_xx.parametri.parametro_d_1

ll_num_colli = long(s_cs_xx.parametri.parametro_d_2)

ldt_data_fine = s_cs_xx.parametri.parametro_data_1

ls_cod_operaio = s_cs_xx.parametri.parametro_s_1

setnull(s_cs_xx.parametri.parametro_d_1)

setnull(s_cs_xx.parametri.parametro_d_2)

setnull(s_cs_xx.parametri.parametro_data_1)

setnull(s_cs_xx.parametri.parametro_s_1)

if ll_return <> 0 then
	return -1
end if

update
	det_ordini_produzione
set
	quan_prodotta = quan_da_produrre,
	flag_fine_fase = 'S',
	data_fine_fase = :ldt_data_fine
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_tes_produzione = :ll_prog_tes and
	flag_fine_fase = 'N';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in aggiornamento fasi collegate: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

update
	tes_ordini_produzione
set
	flag_pronto_complessivo = 'S',
	num_colli = :ll_num_colli,
	fine_preparazione = :ldt_data_fine,
	cod_operaio_fine = :ls_cod_operaio
where
	cod_azienda = :s_cs_xx.cod_azienda and
	progr_tes_produzione = :ll_prog_tes;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in chiusura tes_ordini_produzione: " + sqlca.sqlerrtext,stopsign!)
	rollback;
	return -1
end if

commit;

dw_tes.triggerevent("pcd_retrieve")

dw_det.triggerevent("pcd_retrieve")
end event

type tab_1 from tab within w_gestione_avan_prod
integer x = 23
integer y = 20
integer width = 3291
integer height = 1680
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean focusonbuttondown = true
alignment alignment = center!
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
end type

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.Control[]={this.tabpage_1,&
this.tabpage_2}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
end on

event selectionchanged;choose case newindex
	
	case 1
		
		dw_tes.show()
		cb_chiudi_tes.show()
		cb_elimina_tes.show()
		dw_det.hide()
		cb_chiudi_det.hide()
		cb_elimina_det.hide()
		
		dw_tes.setfocus()
		iuo_dw_main = dw_tes
		dw_tes.change_dw_current()
		
	case 2
		
		dw_tes.hide()
		cb_chiudi_tes.hide()
		cb_elimina_tes.hide()
		dw_det.show()
		cb_chiudi_det.show()
		cb_elimina_det.show()
		
		dw_det.setfocus()
		iuo_dw_main = dw_det
		dw_det.change_dw_current()
		
end choose
end event

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3255
integer height = 1556
long backcolor = 12632256
string text = "Avanzamento complessivo per reparto"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 3255
integer height = 1556
long backcolor = 12632256
string text = "Fasi"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
end type

type dw_det from uo_cs_xx_dw within w_gestione_avan_prod
integer x = 46
integer y = 140
integer width = 3246
integer height = 1440
integer taborder = 80
boolean bringtotop = true
string dataobject = "d_avan_prod_det"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda)
end event

type dw_tes from uo_cs_xx_dw within w_gestione_avan_prod
integer x = 46
integer y = 140
integer width = 3246
integer height = 1440
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_avan_prod_tes"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda)
end event

