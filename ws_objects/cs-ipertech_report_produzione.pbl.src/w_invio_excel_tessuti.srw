﻿$PBExportHeader$w_invio_excel_tessuti.srw
forward
global type w_invio_excel_tessuti from w_cs_xx_principale
end type
type tab_1 from tab within w_invio_excel_tessuti
end type
type tabpage_selezione from userobject within tab_1
end type
type dw_selezione from uo_std_dw within tabpage_selezione
end type
type dw_1 from uo_std_dw within tabpage_selezione
end type
type tabpage_selezione from userobject within tab_1
dw_selezione dw_selezione
dw_1 dw_1
end type
type tabpage_invio from userobject within tab_1
end type
type dw_excel from uo_std_dw within tabpage_invio
end type
type tabpage_invio from userobject within tab_1
dw_excel dw_excel
end type
type tab_1 from tab within w_invio_excel_tessuti
tabpage_selezione tabpage_selezione
tabpage_invio tabpage_invio
end type
type dw_prodotti from uo_dddw_checkbox within w_invio_excel_tessuti
end type
end forward

global type w_invio_excel_tessuti from w_cs_xx_principale
integer width = 4782
integer height = 2836
string title = "Generazione Excel Tessuti"
tab_1 tab_1
dw_prodotti dw_prodotti
end type
global w_invio_excel_tessuti w_invio_excel_tessuti

type variables
string ib_where_origine=""
end variables

forward prototypes
public function integer wf_genera_excel (string as_target_file, ref string as_errore)
public function integer wf_esporta_lista (string as_target_file, ref string as_errore)
end prototypes

public function integer wf_genera_excel (string as_target_file, ref string as_errore);string		ls_des_reparto, ls_cod_reparto_dest, ls_cod_tessuto, ls_colore_tessuto, ls_cod_reparto,ls_flag_tipo_unione, ls_des_tasca, &
			ls_flag_orientamento_unione, ls_cod_tipo_tasca, ls_flag_mantovana, ls_cod_tessuto_mant, ls_cod_colore_mant, ls_cod_modello, &
			ls_flag_tipo_mantovana, ls_passamaneria, ls_str, ls_indirizzo,  ls_localita, ls_cap, flag_tipo_mantovana_dest, ls_cod_fornitore, ls_rag_soc_1, &
			ls_cod_comodo, ls_des_tipo_unione_tessuto, ls_cod_reparto_invio, ls_nota_dettaglio, ls_diam_tubetto_telo_sup, ls_diam_tubetto_telo_inf, ls_diam_tubetto_mant
			
long		ll_i, ll_num_rinforzi, ll_dimensione_rinforzo, ll_rapporto_mantovana, &
			ll_progr_det_produzione, ll_anno_registrazione, ll_num_registrazione ,ll_prog_riga_ord_ven, ll_riga, ll_alt_mantovana, ll_dim_rinforzo
			
dec{4} 	ld_dim_x, ld_dim_y, ld_quan_ordine

datetime	ldt_oggi

uo_excel luo_excel


luo_excel = create uo_excel
luo_excel.uof_open(as_target_file,true)

ls_cod_reparto_invio = tab_1.tabpage_invio.dw_excel.getitemstring(1,"cod_reparto")
ls_cod_reparto_dest = tab_1.tabpage_invio.dw_excel.getitemstring(1,"cod_reparto_consegna")
ls_cod_fornitore = tab_1.tabpage_invio.dw_excel.getitemstring(1,"cod_fornitore")
ldt_oggi = datetime(today(),00:00:00)

if g_str.isnotempty(ls_cod_fornitore) then
	select rag_soc_1
	into	:ls_rag_soc_1
	from	anag_fornitori
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :ls_cod_fornitore;
	luo_excel.uof_set( 2, "A", ls_rag_soc_1)		
			
end if

select		indirizzo, 
			localita, 
			cap
into		:ls_indirizzo, 
			:ls_localita, 
			:ls_cap
from		anag_reparti R
left join  	anag_operai O on  R.cod_azienda=O.cod_azienda and R.cod_operaio=O.cod_operaio
where 	R.cod_azienda = :s_cs_xx.cod_azienda and
			R.cod_reparto = :ls_cod_reparto_dest;
if sqlca.sqlcode = 0 then
	if g_str.isempty( ls_indirizzo) then ls_indirizzo=""
	if g_str.isempty( ls_localita) then ls_localita=""
	if g_str.isempty( ls_cap) then ls_cap=""
	ls_des_reparto = g_str.format("$1 $2 $3",ls_indirizzo, ls_cap, ls_localita)
end if

// dati generali di testata
luo_excel.uof_set( 3, "B", string(today(),"dd/mm/yyyy"))
luo_excel.uof_set( 3, "L", ls_des_reparto)
ll_riga = 7
// dati di riga
for ll_i = 1 to tab_1.tabpage_selezione.dw_1.rowcount()
	
	if 	tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,13) = "N" then continue
	
	luo_excel.uof_insert_copied_rows( ll_riga,-4121, ll_riga, "A", ll_riga + 1, "Q")
	
	ll_anno_registrazione = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,1)
	ll_num_registrazione = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,2)
	ll_prog_riga_ord_ven = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,3)
	
	ls_cod_tessuto = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,9)
	luo_excel.uof_set( ll_riga, "A", ls_cod_tessuto)

	ls_colore_tessuto = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,10)
	luo_excel.uof_set( ll_riga + 1, "A", ls_colore_tessuto)

	ld_quan_ordine = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,17)
	luo_excel.uof_set( ll_riga, "B", string(ld_quan_ordine,"##0"))
	
	ld_dim_x = 0
	ld_dim_y = 0
	
	// cerco la misura della larghezza
	select C.misura
	into :ld_dim_x
	from distinte_taglio_calcolate C
	join tab_distinte_taglio T on T.cod_azienda =C.cod_azienda  and
									C.cod_prodotto_padre = T.cod_prodotto_padre and
									C.cod_versione = T.cod_versione  and
									C.cod_prodotto_figlio = T.cod_prodotto_figlio  and
									C.cod_versione_figlio = T.cod_versione_figlio  and
									C.progressivo = T.progressivo and
									C.num_sequenza = T.num_sequenza 
	where 	C.cod_azienda = :s_cs_xx.cod_azienda and
				C.anno_registrazione = :ll_anno_registrazione and
				C.num_registrazione = :ll_num_registrazione and
				C.prog_riga_ord_ven = :ll_prog_riga_ord_ven and
				T.cod_prodotto_figlio='TESSUTO' and 
				T.des_distinta_taglio like '%LARG%'	;
				
	// cerco la misura della altezza o sporgenza
	select C.misura
	into :ld_dim_y
	from distinte_taglio_calcolate C
	join tab_distinte_taglio T on T.cod_azienda =C.cod_azienda  and
									C.cod_prodotto_padre = T.cod_prodotto_padre and
									C.cod_versione = T.cod_versione  and
									C.cod_prodotto_figlio = T.cod_prodotto_figlio  and
									C.cod_versione_figlio = T.cod_versione_figlio and
									C.progressivo = T.progressivo and
									C.num_sequenza = T.num_sequenza 
	where 	C.cod_azienda = :s_cs_xx.cod_azienda and
				C.anno_registrazione = :ll_anno_registrazione and
				C.num_registrazione = :ll_num_registrazione and
				C.prog_riga_ord_ven = :ll_prog_riga_ord_ven and
				T.cod_prodotto_figlio='TESSUTO' and 
				( T.des_distinta_taglio like '%ALT%' or T.des_distinta_taglio like '%SPOR%' )	;
				
	if ld_dim_x <> 0 then luo_excel.uof_set( ll_riga, "C", string(ld_dim_x,"##0.0"))

	if ld_dim_y <> 0 then luo_excel.uof_set( ll_riga, "D", string(ld_dim_y,"##0.0"))
	
	ll_num_rinforzi = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,33)
	if ll_num_rinforzi > 0 and not isnull(ll_num_rinforzi) then
		ll_dim_rinforzo = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,34)
		luo_excel.uof_set( ll_riga, "E",  g_str.format("$1 da $2",ll_num_rinforzi, ll_dim_rinforzo) )
	end if
	
//	ls_flag_orientamento_unione = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,22)
//	choose case ls_flag_orientamento_unione
//		case "O"
//			luo_excel.uof_set( ll_riga, "G", "Orizzontale" )
//		case "V"
//			luo_excel.uof_set( ll_riga, "G", "Verticale" )
//	end choose
	
	
	ls_flag_tipo_unione = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,21)
	
	if not isnull(ls_flag_tipo_unione) and ls_flag_tipo_unione <> "000" then
		select des_tipo_unione_tessuto
		into	:ls_des_tipo_unione_tessuto
		from tab_tipi_unione_tessuti
		where cod_azienda = :s_cs_xx.cod_azienda and 
		cod_tipo_unione_tessutO = :ls_flag_tipo_unione;
	
		if sqlca.sqlcode = 0 then
			luo_excel.uof_set( ll_riga, "F", ls_des_tipo_unione_tessuto)
		end if	
	end if
	
//	choose case ls_flag_tipo_unione
//		case "0"
//			//luo_excel.uof_set( ll_riga, "I", "NON SPEC.")
//		case "1"
//			luo_excel.uof_set( ll_riga, "I", "SUNSTOP")
//		case "2"
//			luo_excel.uof_set( ll_riga, "I", "TENARA")
//		case "3"
//			luo_excel.uof_set( ll_riga, "I", "SALDATO")
//		case "4"
//			luo_excel.uof_set( ll_riga, "I", "INCOLLATO")
//		case "5"
//			luo_excel.uof_set( ll_riga, "I", "Sald. 1/2 Alt.")
//		case "6"
//			luo_excel.uof_set( ll_riga, "I", "Taglio")
//		case "7"
//			luo_excel.uof_set( ll_riga, "I", "Telo Unico")
//	end choose			
//	
	ls_cod_modello = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,7)
	
	select cod_comodo
	into	:ls_cod_comodo
	from  anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_modello;
	if sqlca.sqlcode = 0 and len(ls_cod_comodo)>0 and not isnull(ls_cod_comodo) then
		luo_excel.uof_set( ll_riga, "H", ls_cod_comodo )
	else
		luo_excel.uof_set( ll_riga, "H", ls_cod_modello )
	end if	
	// tasca 1
	ls_cod_tipo_tasca = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,23)
	if not isnull(ls_cod_tipo_tasca) and len(ls_cod_tipo_tasca) > 0 then
		ls_des_tasca = f_des_tabella("tab_flags_config_tipo_tasca",g_str.format("cod_modello='$1' and cod_tipo_tasca='$2' ",ls_cod_modello, ls_cod_tipo_tasca), "des_tipo_tasca")
		luo_excel.uof_set( ll_riga, "I", ls_des_tasca )
	else	
		ls_diam_tubetto_telo_sup = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,18)
		luo_excel.uof_set( ll_riga, "I", ls_diam_tubetto_telo_sup)
	end if
	
	// tasca 2
	ls_cod_tipo_tasca = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,35)
	if not isnull(ls_cod_tipo_tasca)  and len(ls_cod_tipo_tasca) > 0 then
		ls_des_tasca = f_des_tabella("tab_flags_config_tipo_tasca",g_str.format("cod_modello='$1' and cod_tipo_tasca='$2' ",ls_cod_modello, ls_cod_tipo_tasca), "des_tipo_tasca")
		luo_excel.uof_set( ll_riga, "J", ls_des_tasca )
	else	
		ls_diam_tubetto_telo_inf = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,19)
		luo_excel.uof_set( ll_riga, "J", ls_diam_tubetto_telo_inf)
	end if
	
	// tasca 3
	ls_cod_tipo_tasca = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,36)
	if not isnull(ls_cod_tipo_tasca)  and len(ls_cod_tipo_tasca)> 0 then
		ls_des_tasca = f_des_tabella("tab_flags_config_tipo_tasca",g_str.format("cod_modello='$1' and cod_tipo_tasca='$2' ",ls_cod_modello, ls_cod_tipo_tasca), "des_tipo_tasca")
		luo_excel.uof_set( ll_riga, "K", ls_des_tasca )
	else	
		ls_diam_tubetto_mant = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,20)
		luo_excel.uof_set( ll_riga, "K", ls_diam_tubetto_mant)
	end if
	
	ll_rapporto_mantovana = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,27)
	luo_excel.uof_set( ll_riga, "M", string(ll_rapporto_mantovana,"##0"))
	
	ls_flag_mantovana = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,24)
	ll_num_rinforzi = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,25)
	ll_dimensione_rinforzo = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,26)
	
	ls_cod_colore_mant = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,29)
	ls_cod_tessuto_mant =  tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,28)
	
	if ls_colore_tessuto <> ls_cod_colore_mant and len(ls_cod_colore_mant) > 0 and not isnull(ls_cod_colore_mant) then
		luo_excel.uof_set( ll_riga, "L", ls_cod_colore_mant)
	end if
	
	ls_cod_reparto = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,14)
	// recupero il progressivo del foglio di lavoro
	select progr_det_produzione
	into	:ll_progr_det_produzione
	from	det_ordini_produzione
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga_ord_ven and
			cod_reparto = :ls_cod_reparto;
	if sqlca.sqlcode = 0 then
		luo_excel.uof_set( ll_riga + 1, "H", string(ll_progr_det_produzione))
	end if		
	
	ll_alt_mantovana =  tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,30)
	luo_excel.uof_set( ll_riga, "N", string(ll_alt_mantovana, "##0"))

	ls_flag_tipo_mantovana =  tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,31)
	
	select flag_tipo_mantovana_dest
	into	:flag_tipo_mantovana_dest
	from	cust_reparto_mantovana
	where cod_reparto = :ls_cod_reparto_invio and 
			flag_tipo_mantovana_origine = :ls_flag_tipo_mantovana;
	if sqlca.sqlcode = 0 then
		luo_excel.uof_set( ll_riga, "O", flag_tipo_mantovana_dest)
	else		
		luo_excel.uof_set( ll_riga, "O", ls_flag_tipo_mantovana)
	end if
	
	ls_passamaneria =  tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,32)
	luo_excel.uof_set( ll_riga, "P", ls_passamaneria)

	ls_nota_dettaglio =  tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,42)
	luo_excel.uof_set( ll_riga, "Q", ls_nota_dettaglio)

	
	ll_riga = ll_riga + 2
next

luo_excel.uof_save( )

destroy luo_excel

for ll_i = 1 to tab_1.tabpage_selezione.dw_1.rowcount()
	
	if 	tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,13) = "N" then continue
	
	ll_anno_registrazione = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,1)
	ll_num_registrazione = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,2)
	ll_prog_riga_ord_ven = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,3)
	
	update det_ord_ven
	set flag_presso_ptenda='S'
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode < 0 then
		rollback;
		as_errore="Errore durante identificazione riga ordine come già inviato TELO.~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	update comp_det_ord_ven
	set data_invio_telo=:ldt_oggi
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	if sqlca.sqlcode < 0 then
		rollback;
		as_errore="Errore durante identificazione riga ordine come già inviato TELO.~r~n" + sqlca.sqlerrtext
		return -1
	end if
next

commit;

return 0
end function

public function integer wf_esporta_lista (string as_target_file, ref string as_errore);string		ls_des_reparto, ls_cod_reparto_dest, ls_cod_tessuto, ls_colore_tessuto, ls_cod_reparto,ls_flag_tipo_unione, ls_des_tasca, &
			ls_flag_orientamento_unione, ls_cod_tipo_tasca, ls_flag_mantovana, ls_cod_tessuto_mant, ls_cod_colore_mant, ls_cod_modello, &
			ls_flag_tipo_mantovana, ls_passamaneria, ls_str, ls_indirizzo,  ls_localita, ls_cap, flag_tipo_mantovana_dest, ls_cod_fornitore, ls_rag_soc_1, &
			ls_cod_comodo, ls_des_tipo_unione_tessuto, ls_cod_reparto_invio, ls_nota_dettaglio, ls_num_ord_cliente
			
long		ll_i, ll_num_rinforzi, ll_dimensione_rinforzo, ll_rapporto_mantovana, &
			ll_progr_det_produzione, ll_anno_registrazione, ll_num_registrazione ,ll_prog_riga_ord_ven, ll_riga, ll_alt_mantovana, ll_dim_rinforzo
			
dec{4} 	ld_dim_x, ld_dim_y, ld_quan_ordine

uo_excel luo_excel


luo_excel = create uo_excel
//luo_excel.uof_open(as_target_file,true)
luo_excel.uof_create(as_target_file,true)

ls_cod_reparto_invio = tab_1.tabpage_invio.dw_excel.getitemstring(1,"cod_reparto")
ll_riga = 1

luo_excel.uof_set_bold( ll_riga, "A") 
luo_excel.uof_set_bold( ll_riga, "B") // q.ta
luo_excel.uof_set_bold( ll_riga, "C") // prodotto
luo_excel.uof_set_bold( ll_riga, "D") // riferimento
luo_excel.uof_set_bold( ll_riga, "E")  // ordine
luo_excel.uof_set_bold( ll_riga, "F") // vernic
luo_excel.uof_set_bold( ll_riga, "G") // colore
luo_excel.uof_set_bold( ll_riga, "H") // largh
luo_excel.uof_set_bold( ll_riga, "I") //sp/h
luo_excel.uof_set_bold( ll_riga, "J") // colli
luo_excel.uof_set_bold( ll_riga, "K") // colli
luo_excel.uof_set_bold( ll_riga, "L") // colli
luo_excel.uof_set_bold( ll_riga, "M") // colli


luo_excel.uof_set(1, "A", "TELO")
luo_excel.uof_set(1, "B", "Q.TA")
luo_excel.uof_set(1, "C", "LARGH")
luo_excel.uof_set(1, "D", "SP/H")
luo_excel.uof_set(1, "E", "PRODOTTO")
luo_excel.uof_set(1, "F", "BARCODE")
luo_excel.uof_set(1, "G", "TELO MANT")
luo_excel.uof_set(1, "H", "H MANT")
luo_excel.uof_set(1, "I", "MANT")
luo_excel.uof_set(1, "J", "PASS")
luo_excel.uof_set(1, "K", "CLIENTE")
luo_excel.uof_set(1, "L", "RIFERIMENTO")
luo_excel.uof_set(1, "M", "SPEDIZIONE")


ll_riga = 2
// dati di riga
for ll_i = 1 to tab_1.tabpage_selezione.dw_1.rowcount()
	
	if 	tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,13) = "N" then continue
	
	ll_anno_registrazione = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,1)
	ll_num_registrazione = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,2)
	ll_prog_riga_ord_ven = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,3)
	
	luo_excel.uof_set_format(ll_riga, "A", "@")
	luo_excel.uof_set_format(ll_riga, "E", "@")
	luo_excel.uof_set_format(ll_riga, "G", "@")
	luo_excel.uof_set_format(ll_riga, "I", "@")
	luo_excel.uof_set_format(ll_riga, "J", "@")
	luo_excel.uof_set_format(ll_riga, "K", "@")
	luo_excel.uof_set_format(ll_riga, "L", "@")
	
	ls_colore_tessuto = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,10)
	luo_excel.uof_set( ll_riga, "A", ls_colore_tessuto)

	ld_quan_ordine = tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,17)
	luo_excel.uof_set( ll_riga, "B", string(ld_quan_ordine,"##0"))
	
	ld_dim_x = 0
	ld_dim_y = 0
	
	// cerco la misura della larghezza
	select C.misura
	into :ld_dim_x
	from distinte_taglio_calcolate C
	join tab_distinte_taglio T on T.cod_azienda =C.cod_azienda  and
									C.cod_prodotto_padre = T.cod_prodotto_padre and
									C.cod_versione = T.cod_versione  and
									C.cod_prodotto_figlio = T.cod_prodotto_figlio  and
									C.cod_versione_figlio = T.cod_versione_figlio  and
									C.progressivo = T.progressivo and
									C.num_sequenza = T.num_sequenza 
	where 	C.cod_azienda = :s_cs_xx.cod_azienda and
				C.anno_registrazione = :ll_anno_registrazione and
				C.num_registrazione = :ll_num_registrazione and
				C.prog_riga_ord_ven = :ll_prog_riga_ord_ven and
				T.cod_prodotto_figlio='TESSUTO' and 
				T.des_distinta_taglio like '%LARG%'	;
				
	// cerco la misura della altezza o sporgenza
	select C.misura
	into :ld_dim_y
	from distinte_taglio_calcolate C
	join tab_distinte_taglio T on T.cod_azienda =C.cod_azienda  and
									C.cod_prodotto_padre = T.cod_prodotto_padre and
									C.cod_versione = T.cod_versione  and
									C.cod_prodotto_figlio = T.cod_prodotto_figlio  and
									C.cod_versione_figlio = T.cod_versione_figlio and
									C.progressivo = T.progressivo and
									C.num_sequenza = T.num_sequenza 
	where 	C.cod_azienda = :s_cs_xx.cod_azienda and
				C.anno_registrazione = :ll_anno_registrazione and
				C.num_registrazione = :ll_num_registrazione and
				C.prog_riga_ord_ven = :ll_prog_riga_ord_ven and
				T.cod_prodotto_figlio='TESSUTO' and 
				( T.des_distinta_taglio like '%ALT%' or T.des_distinta_taglio like '%SPOR%' )	;
				
	if ld_dim_x <> 0 then luo_excel.uof_set( ll_riga, "C", string(ld_dim_x,"##0.0"))

	if ld_dim_y <> 0 then luo_excel.uof_set( ll_riga, "D", string(ld_dim_y,"##0.0"))
	
	ls_cod_modello = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,7)
	
	select cod_comodo
	into	:ls_cod_comodo
	from  anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_modello;
	if sqlca.sqlcode = 0 and len(ls_cod_comodo)>0 and not isnull(ls_cod_comodo) then
		luo_excel.uof_set( ll_riga, "E", ls_cod_comodo )
	else
		luo_excel.uof_set( ll_riga, "E", ls_cod_modello )
	end if	
	

	
	ls_cod_colore_mant = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,29)
	ls_cod_tessuto_mant =  tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,28)
	
	if ls_colore_tessuto <> ls_cod_colore_mant and len(ls_cod_colore_mant) > 0 and not isnull(ls_cod_colore_mant) then
		luo_excel.uof_set( ll_riga, "G", ls_cod_colore_mant)
	end if
	
	ls_cod_reparto = tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,14)
	// recupero il progressivo del foglio di lavoro
	select progr_det_produzione
	into	:ll_progr_det_produzione
	from	det_ordini_produzione
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga_ord_ven and
			cod_reparto = :ls_cod_reparto;
	if sqlca.sqlcode = 0 then
		luo_excel.uof_set( ll_riga, "F", string(ll_progr_det_produzione))
	end if		
	
	ll_alt_mantovana =  tab_1.tabpage_selezione.dw_1.getitemnumber(ll_i,30)
	luo_excel.uof_set( ll_riga, "H", string(ll_alt_mantovana, "##0"))

	ls_flag_tipo_mantovana =  tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,31)
	
	select flag_tipo_mantovana_dest
	into	:flag_tipo_mantovana_dest
	from	cust_reparto_mantovana
	where cod_reparto = :ls_cod_reparto_invio and 
			flag_tipo_mantovana_origine = :ls_flag_tipo_mantovana;
	if sqlca.sqlcode = 0 then
		luo_excel.uof_set( ll_riga, "I", flag_tipo_mantovana_dest)
	else		
		luo_excel.uof_set( ll_riga, "I", ls_flag_tipo_mantovana)
	end if
	
	ls_passamaneria =  tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,32)
	luo_excel.uof_set( ll_riga, "J", ls_passamaneria)

	ls_rag_soc_1 =  tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,6)
	luo_excel.uof_set( ll_riga, "K", ls_rag_soc_1)

	ls_num_ord_cliente =  tab_1.tabpage_selezione.dw_1.getitemstring(ll_i,40)
	luo_excel.uof_set( ll_riga, "L", ls_num_ord_cliente)

	
	ll_riga ++
next
luo_excel.uof_set_border( 1, 1, ll_riga, 13, 2,2,2,2)
luo_excel.uof_set_autofit( 1, 1,ll_riga, 13)


destroy luo_excel
commit;

return 0
end function

on w_invio_excel_tessuti.create
int iCurrent
call super::create
this.tab_1=create tab_1
this.dw_prodotti=create dw_prodotti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
this.Control[iCurrent+2]=this.dw_prodotti
end on

on w_invio_excel_tessuti.destroy
call super::destroy
destroy(this.tab_1)
destroy(this.dw_prodotti)
end on

event pc_setwindow;call super::pc_setwindow;string ls_sql

set_w_options(c_noenablepopup + c_closenosave)

tab_1.tabpage_selezione.dw_1.ib_colora=false
tab_1.tabpage_selezione.dw_selezione.insertrow(0)
tab_1.tabpage_invio.dw_excel.insertrow(0)

tab_1.tabpage_selezione.dw_selezione.setitem(1, 1, today() )
tab_1.tabpage_selezione.dw_selezione.setitem(1, 2, today() )
tab_1.tabpage_selezione.dw_selezione.setitem(1, 5, today() )
tab_1.tabpage_selezione.dw_selezione.setitem(1, 6, relativedate(today(), 30) )

ls_sql = 	"select cod_prodotto, des_prodotto from anag_prodotti where cod_prodotto in (select cod_modello from tab_flags_configuratore) and flag_blocco = 'N' order by 1"

dw_prodotti.uof_set_column_by_sql(ls_sql)
dw_prodotti.uof_set_parent_dw(tab_1.tabpage_selezione.dw_selezione, "cod_prodotto", "cod_prodotto")

dw_prodotti.uof_set_column_width(1, 400)
dw_prodotti.uof_set_column_width(2, 800)

dw_prodotti.uof_set_column_name(1, "Cod.")
dw_prodotti.uof_set_column_name(2, "Des. Prodotto")

dw_prodotti.uof_set_column_align(1, "center")


end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(tab_1.tabpage_invio.dw_excel, &
	"cod_reparto", &
	sqlca, &
	"anag_reparti", &
	"cod_reparto", &
	"des_reparto", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) ")

f_po_loaddddw_dw(tab_1.tabpage_selezione.dw_selezione, &
	"cod_reparto", &
	sqlca, &
	"anag_reparti", &
	"cod_reparto", &
	"des_reparto", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) ")

f_po_loaddddw_dw(tab_1.tabpage_invio.dw_excel, &
	"cod_reparto_consegna", &
	sqlca, &
	"anag_reparti", &
	"cod_reparto", &
	"des_reparto", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) ")

end event

event resize;/** TOLTO ANCESTOR **/

tab_1.resize(newwidth - 30, newheight - 30)


tab_1.event ue_resize()

end event

type tab_1 from tab within w_invio_excel_tessuti
event ue_resize ( )
integer width = 4731
integer height = 2720
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
integer selectedtab = 1
tabpage_selezione tabpage_selezione
tabpage_invio tabpage_invio
end type

event ue_resize();// ridimensiono DW di ricerca
tab_1.tabpage_selezione.dw_selezione.width = tab_1.width
tab_1.tabpage_selezione.dw_1.resize(tab_1.width -30, tab_1.height - tab_1.tabpage_selezione.dw_selezione.height -180 )

end event

on tab_1.create
this.tabpage_selezione=create tabpage_selezione
this.tabpage_invio=create tabpage_invio
this.Control[]={this.tabpage_selezione,&
this.tabpage_invio}
end on

on tab_1.destroy
destroy(this.tabpage_selezione)
destroy(this.tabpage_invio)
end on

type tabpage_selezione from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4695
integer height = 2592
long backcolor = 12632256
string text = "SELEZIONE"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_selezione dw_selezione
dw_1 dw_1
end type

on tabpage_selezione.create
this.dw_selezione=create dw_selezione
this.dw_1=create dw_1
this.Control[]={this.dw_selezione,&
this.dw_1}
end on

on tabpage_selezione.destroy
destroy(this.dw_selezione)
destroy(this.dw_1)
end on

type dw_selezione from uo_std_dw within tabpage_selezione
integer x = 5
integer y = 8
integer width = 4663
integer height = 420
integer taborder = 20
string dataobject = "d_invio_excel_tessuti_sel"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_search"
		accepttext()
		tab_1.tabpage_selezione.dw_1.event ue_imposta_sql()
		
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(tab_1.tabpage_selezione.dw_selezione,"cod_cliente")
end choose
	

end event

event clicked;call super::clicked;choose case dwo.name
	case "cod_prodotto"
		dw_prodotti.show()
end choose
end event

type dw_1 from uo_std_dw within tabpage_selezione
event ue_imposta_sql ( )
event ue_retrieve ( )
integer x = 5
integer y = 428
integer width = 4663
integer height = 2140
integer taborder = 20
string dataobject = "d_invio_excel_tessuti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

event ue_imposta_sql();string ls_sql, ls_sql1, ls_sql2, ls_sql_add, ls_cod_reparto, ls_flag_inviati, ls_prodotti[], ls_cod_cliente, ls_num_ord_cliente, ls_barcode_produzione, ls_str
long	ll_pos, ll_anno_ordine, ll_num_ordine, ll_ret, ll_i
date ld_data_reg_inizio, ld_data_reg_fine, ld_data_cons_inizio, ld_data_cons_fine

ls_sql = getsqlselect( )
ll_pos = pos( lower(ls_sql), "where" )

ls_sql1 = left( ls_sql, ll_pos - 1)
ls_sql2 = mid( ls_sql, ll_pos + 5 )

if len(ib_where_origine) = 0 then ib_where_origine = ls_sql2

ld_data_reg_inizio = tab_1.tabpage_selezione.dw_selezione.getitemdate(1,"data_inizio")
ld_data_reg_fine = tab_1.tabpage_selezione.dw_selezione.getitemdate(1,"data_fine")
ld_data_cons_inizio = tab_1.tabpage_selezione.dw_selezione.getitemdate(1,"data_consegna_inizio")
ld_data_cons_fine = tab_1.tabpage_selezione.dw_selezione.getitemdate(1,"data_consegna_fine")
ll_anno_ordine = tab_1.tabpage_selezione.dw_selezione.getitemnumber(1,"anno_registrazione")
ll_num_ordine = tab_1.tabpage_selezione.dw_selezione.getitemnumber(1,"num_registrazione")
ls_cod_reparto = tab_1.tabpage_selezione.dw_selezione.getitemstring(1,"cod_reparto")
ls_flag_inviati = tab_1.tabpage_selezione.dw_selezione.getitemstring(1,"flag_inviati")
ls_cod_cliente = tab_1.tabpage_selezione.dw_selezione.getitemstring(1,"cod_cliente")
ls_num_ord_cliente = tab_1.tabpage_selezione.dw_selezione.getitemstring(1,"num_ord_cliente")
ls_barcode_produzione =tab_1.tabpage_selezione.dw_selezione.getitemstring(1,"barcode_produzione")
g_str.explode( tab_1.tabpage_selezione.dw_selezione.getitemstring(1,"cod_prodotto") , ",", ls_prodotti)

ls_sql_add = " where TES.cod_azienda='" + s_cs_xx.cod_azienda + " ' "

if not isnull(ld_data_reg_inizio) and ld_data_reg_inizio > date("01/01/1900") then
	ls_sql_add += " and TES.data_registrazione >= '" + string(ld_data_reg_inizio, s_cs_xx.db_funzioni.formato_data   ) + "' "
end if
if not isnull(ld_data_reg_fine) and ld_data_reg_fine > date("01/01/1900") then
	ls_sql_add += " and TES.data_registrazione <= '" + string(ld_data_reg_fine, s_cs_xx.db_funzioni.formato_data   )+ "' "
end if

if not isnull(ld_data_cons_inizio) and ld_data_cons_inizio > date("01/01/1900") then
	ls_sql_add += " and TES.data_consegna >= '" + string(ld_data_cons_inizio, s_cs_xx.db_funzioni.formato_data   )+ "' "
end if
if not isnull(ld_data_cons_fine) and ld_data_cons_fine > date("01/01/1900") then
	ls_sql_add += " and TES.data_consegna <= '" + string(ld_data_cons_fine, s_cs_xx.db_funzioni.formato_data   )+ "' "
end if

if not isnull(ll_anno_ordine) and ll_anno_ordine > 0 then
	ls_sql_add += " and TES.anno_registrazione = " + string(ll_anno_ordine )
end if
if not isnull(ll_num_ordine) and ll_num_ordine > 0 then
	ls_sql_add += " and TES.num_registrazione = " + string(ll_num_ordine )
end if

if not isnull(ls_cod_cliente) and len(ls_cod_cliente) > 0 then
	ls_sql_add += " and TES.cod_cliente = '" + ls_cod_cliente + "' "
end if

if not isnull(ls_num_ord_cliente) and len(ls_num_ord_cliente) > 0 then
	ls_sql_add += " and TES.num_ord_cliente like '%" + ls_num_ord_cliente + "%' "
end if


if not isnull(ls_cod_reparto) then
	ls_sql_add += " and  C.cod_reparto = '" + ls_cod_reparto + "' " 
end if

if not isnull(ls_barcode_produzione) then
	ls_sql_add += " and  P.progr_det_produzione = " + ls_barcode_produzione 
end if

if ls_flag_inviati="S" or ls_flag_inviati="N" then
	ls_sql_add += g_str.format(" and DET.flag_presso_ptenda = '$1' ",ls_flag_inviati)
end if

if upperbound(ls_prodotti) > 0 then
	if  ls_prodotti[1] <> "TUTTI" then
		ls_str = ""
		for ll_i = 1 to upperbound(ls_prodotti)
			if len(ls_str) > 0 then ls_str += ","
			ls_str += g_str.format("'$1'",trim(ls_prodotti[ll_i]))
		next
		ls_sql_add += g_str.format(" and DET.cod_prodotto in ($1) ", ls_str)
	end if
end if


ls_sql = ls_sql1 + ls_sql_add + " and " + ib_where_origine

ll_ret = setsqlselect(ls_sql)
if ll_ret < 1 then
	g_mb.error("Errore nell'impostazione SQL della pagina dei risultati.")
	return
end if

triggerevent("ue_retrieve")

return 
end event

event ue_retrieve();long ll_errore, ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_progr_det_produzione
string	ls_cod_reparto, ls_flag_fine_fase, ls_filter
datetime ldt_data_fine_fase, ldt_data_ricezione_telo, ldt_inizio_sessione

settransobject( sqlca )
setredraw(false)
setfilter("")

ll_errore = retrieve()
if ll_errore < 0 then
   pcca.error = c_fatal
end if


for ll_i = 1 to rowcount()
	ll_anno_registrazione = getitemnumber(ll_i,1)
	ll_num_registrazione = getitemnumber(ll_i,2)
	ll_prog_riga_ord_ven = getitemnumber(ll_i,3)
	ls_cod_reparto = getitemstring(ll_i,14)
	
	select flag_fine_fase,
			data_fine_fase,
			progr_det_produzione
	into	:ls_flag_fine_fase,
			:ldt_data_fine_fase, 
			:ll_progr_det_produzione
	from	det_ordini_produzione
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga_ord_ven and
			cod_reparto = :ls_cod_reparto;
	if sqlca.sqlcode = 0 then
		if ls_flag_fine_fase = "S" then
			setitem(ll_i, "data_ricezione_telo", datetime(date(ldt_data_fine_fase),00:00:00))
		else
			select max(inizio_sessione)
			into	:ldt_inizio_sessione
			from sessioni_lavoro
			where cod_azienda = :s_cs_xx.cod_azienda and progr_det_produzione = :ll_progr_det_produzione;
			
			if ldt_inizio_sessione > datetime(date("01/01/1900"),00:00:00) and not isnull(ldt_inizio_sessione) then
				setitem(ll_i, "data_ricezione_telo", datetime(date(ldt_inizio_sessione),00:00:00))
			else
				ldt_data_fine_fase = datetime(date("01/01/1900"),00:00:00)
				setitem(ll_i, "data_ricezione_telo", ldt_data_fine_fase)
			end if
		end if
	else
		ldt_data_fine_fase = datetime(date("01/01/1900"),00:00:00)
		setitem(ll_i, "data_ricezione_telo", ldt_data_fine_fase)
	end if
next

ldt_data_ricezione_telo = tab_1.tabpage_selezione.dw_selezione.getitemdatetime(1,"data_ricezione_telo")

if ldt_data_ricezione_telo > datetime(date("01/01/1900"),00:00:00) and not isnull(ldt_data_ricezione_telo) then
	ls_filter = g_str.format("data_ricezione_telo = datetime('$1' )", string(ldt_data_ricezione_telo,"dd/mm/yyyy"))
	ll_errore = setfilter(ls_filter)
	ll_errore = filter()
end if

setredraw(true)

end event

event buttonclicked;call super::buttonclicked;if dwo.name="b_selezione" then
	long ll_i
	if dwo.text="SEL" then
		for ll_i = 1 to this.rowcount()
			setitem(ll_i,"selezione","S")
		next
		this.object.b_selezione.text = "SEL*"
	elseif dwo.text="SEL*" then
		for ll_i = 1 to this.rowcount()
			setitem(ll_i,"selezione","N")
		next
		this.object.b_selezione.text = "SEL"
	end if
end if

end event

type tabpage_invio from userobject within tab_1
integer x = 18
integer y = 112
integer width = 4695
integer height = 2592
long backcolor = 12632256
string text = "INVIO"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_excel dw_excel
end type

on tabpage_invio.create
this.dw_excel=create dw_excel
this.Control[]={this.dw_excel}
end on

on tabpage_invio.destroy
destroy(this.dw_excel)
end on

type dw_excel from uo_std_dw within tabpage_invio
integer x = 5
integer y = 8
integer width = 3657
integer height = 520
integer taborder = 30
string dataobject = "d_invio_excel_tessuti_invio"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event buttonclicked;call super::buttonclicked;// per prima cosa creo una copia del file di origine
string ls_file_modello, ls_file_target, ls_errore
long	ll_ret

choose case dwo.name
	case "b_fornitore"
		guo_ricerca.uof_ricerca_fornitore(tab_1.tabpage_invio.dw_excel, "cod_fornitore")
		
	case "b_esporta"
		tab_1.tabpage_selezione.dw_1.saveas("", XLSX!,true, EncodingUTF8!)
	case "b_esporta_lista" 
		accepttext()
		
		ls_file_target = guo_functions.uof_get_user_documents_folder( ) + "Liste Teli\"
		if not DirectoryExists ( ls_file_target ) then
			CreateDirectory ( ls_file_target )
		end if
		ls_file_target = guo_functions.uof_get_user_documents_folder( ) + "Liste Teli\"+ guo_functions.uof_get_random_filename( ) + ".xlsx"
		
		// genero il file Excel
		if wf_esporta_lista(ls_file_target, ref ls_errore) < 0 then
			g_mb.error(ls_errore)
		else
			g_mb.show("File Generato con successo")
		end if

	case "b_excel" 
		accepttext()
		
		ls_file_modello = s_cs_xx.volume + s_cs_xx.risorse + "modello_ordini_teli_conf.xlsx"
		
		ls_file_target = guo_functions.uof_get_user_documents_folder( ) + "Teli da Spedire\"
		if not DirectoryExists ( ls_file_target ) then
			CreateDirectory ( ls_file_target )
		end if
		ls_file_target = guo_functions.uof_get_user_documents_folder( ) + "Teli da Spedire\"+ guo_functions.uof_get_random_filename( ) + ".xlsx"
		
		ll_ret = filecopy(ls_file_modello, ls_file_target, false)
		if ll_ret = -1 then
			g_mb.error("Il file di origine non esiste.~r~n" + ls_file_modello )
		elseif ll_ret = -2 then
			g_mb.error("Impossibile scrivere il file di destinazione.~r~n" + ls_file_target)
		end if
	
		// genero il file Excel
		if wf_genera_excel(ls_file_target, ref ls_errore) < 0 then
			g_mb.error(ls_errore)
		else
			g_mb.show("File Generato con successo")
		end if

end choose
end event

type dw_prodotti from uo_dddw_checkbox within w_invio_excel_tessuti
integer x = 297
integer y = 660
integer width = 1874
integer height = 1140
integer taborder = 30
end type

