﻿$PBExportHeader$w_report_1.srw
$PBExportComments$Window report 1
forward
global type w_report_1 from w_cs_xx_principale
end type
type dw_report_1 from uo_cs_xx_dw within w_report_1
end type
type cb_1 from commandbutton within w_report_1
end type
type cb_2 from commandbutton within w_report_1
end type
type cb_3 from commandbutton within w_report_1
end type
type cb_4 from commandbutton within w_report_1
end type
type cb_5 from commandbutton within w_report_1
end type
end forward

global type w_report_1 from w_cs_xx_principale
integer width = 3968
integer height = 2256
string title = "Stampa Ordini Clienti"
boolean minbox = false
boolean hscrollbar = true
boolean vscrollbar = true
dw_report_1 dw_report_1
cb_1 cb_1
cb_2 cb_2
cb_3 cb_3
cb_4 cb_4
cb_5 cb_5
end type
global w_report_1 w_report_1

type variables
long il_anno_registrazione, il_num_registrazione
end variables

event pc_setwindow;call super::pc_setwindow;dw_report_1.ib_dw_report = true

set_w_options(c_noresizewin)

dw_report_1.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
											c_disablecc, &
											c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)
												



save_on_close(c_socnosave)

end event

on w_report_1.create
int iCurrent
call super::create
this.dw_report_1=create dw_report_1
this.cb_1=create cb_1
this.cb_2=create cb_2
this.cb_3=create cb_3
this.cb_4=create cb_4
this.cb_5=create cb_5
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_1
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_2
this.Control[iCurrent+4]=this.cb_3
this.Control[iCurrent+5]=this.cb_4
this.Control[iCurrent+6]=this.cb_5
end on

on w_report_1.destroy
call super::destroy
destroy(this.dw_report_1)
destroy(this.cb_1)
destroy(this.cb_2)
destroy(this.cb_3)
destroy(this.cb_4)
destroy(this.cb_5)
end on

type dw_report_1 from uo_cs_xx_dw within w_report_1
integer x = 23
integer y = 120
integer width = 4937
integer height = 4500
integer taborder = 50
string dataobject = "d_report_1"
boolean livescroll = true
end type

type cb_1 from commandbutton within w_report_1
integer x = 960
integer y = 20
integer width = 274
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long  job


job = PrintOpen( ) 

PrintDataWindow(job, dw_report_1) 
PrintClose(job)


end event

type cb_2 from commandbutton within w_report_1
integer x = 23
integer y = 20
integer width = 265
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "carica 1"
end type

event clicked;long ll_t

dw_report_1.reset()
dw_report_1.DataObject = 'd_report_1'

dw_report_1.Modify("data_pronto.text='10/12/2000'")
dw_report_1.Modify("data_consegna.text='12/12/2000'")
dw_report_1.Modify("des_reparto.text='Reparto Banane'")
dw_report_1.Modify("vedi_allegato.visible=1")
dw_report_1.Modify("rag_soc_1.text='Pajasso'")



for ll_t = 1 to 10
	
	dw_report_1.insertrow(ll_t)
	
	choose case ll_t 
		case 1
			dw_report_1.setitem(ll_t,"tipo_riga",1)
			dw_report_1.setitem(ll_t,"codice","codice")
			dw_report_1.setitem(ll_t,"descrizione","c0505176")
			
		case 2
			dw_report_1.setitem(ll_t,"tipo_riga",2)
			dw_report_1.setitem(ll_t,"visibile_2",1)
			dw_report_1.setitem(ll_t,"codice","descrizione")
			dw_report_1.setitem(ll_t,"descrizione","CP COMPASSO SEMITONDO")
			dw_report_1.setitem(ll_t,"quantita","1")
		case 3
			dw_report_1.setitem(ll_t,"tipo_riga",1)
			dw_report_1.setitem(ll_t,"codice","Altezza")
			dw_report_1.setitem(ll_t,"descrizione","123")

		case 4
			dw_report_1.setitem(ll_t,"tipo_riga",1)
			dw_report_1.setitem(ll_t,"codice","Larghezza")
			dw_report_1.setitem(ll_t,"descrizione","153")
		case 5
			dw_report_1.setitem(ll_t,"visibile_2",1)
			dw_report_1.setitem(ll_t,"visibile_1",1)
			dw_report_1.setitem(ll_t,"tipo_riga",3)
			dw_report_1.setitem(ll_t,"nota","nota verniciatura")
			
		case 6
			dw_report_1.setitem(ll_t,"tipo_riga",5)
			dw_report_1.setitem(ll_t,"nota_1"," ")
		case 7
			dw_report_1.setitem(ll_t,"tipo_riga",4)
			dw_report_1.setitem(ll_t,"codice","Codice Opt.")
			dw_report_1.setitem(ll_t,"descrizione","Des. Opt.")
			dw_report_1.setitem(ll_t,"quantita","Q.tà")
			dw_report_1.setitem(ll_t,"um_opt","U.M.")
			dw_report_1.setitem(ll_t,"nota_optional","Nota di prodotto")
			dw_report_1.setitem(ll_t,"num_com_opt","Nr. Com.")
			
		case 8
			dw_report_1.setitem(ll_t,"tipo_riga",4)
			dw_report_1.setitem(ll_t,"visibile_3",1)
			dw_report_1.setitem(ll_t,"visibile_2",1)
			dw_report_1.setitem(ll_t,"visibile_1",1)
			dw_report_1.setitem(ll_t,"codice","TT7711")
			dw_report_1.setitem(ll_t,"descrizione","Banana da Corsa")
			dw_report_1.setitem(ll_t,"um_opt","Ba")
			dw_report_1.setitem(ll_t,"quantita","200")
			dw_report_1.setitem(ll_t,"nota_optional","prodotto per scimmie integrai")
			dw_report_1.setitem(ll_t,"num_com_opt","5001")
		case 9
			dw_report_1.setitem(ll_t,"tipo_riga",5)
			dw_report_1.setitem(ll_t,"nota_1"," ")
			
		case 10
			dw_report_1.setitem(ll_t,"tipo_riga",5)
			dw_report_1.setitem(ll_t,"nota_1","NOTA 1   nota 1    nota 1 nota 1 nota 1 nota 1 nota 1 nota 1")
			
	end choose
next

dw_report_1.triggerevent("pcd_retrieve")

end event

type cb_3 from commandbutton within w_report_1
integer x = 297
integer y = 20
integer width = 265
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "carica 2"
end type

event clicked;long ll_t

dw_report_1.reset()
dw_report_1.DataObject = 'd_report_2'

dw_report_1.Modify("data_pronto.text='10/12/2000'")
dw_report_1.Modify("data_consegna.text='12/12/2000'")
dw_report_1.Modify("des_reparto.text='Reparto Pajassi'")




for ll_t = 1 to 10
	
	dw_report_1.insertrow(ll_t)
	
	choose case ll_t 
		case 1
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","c0505176")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 1")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",230.5)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",510.8)
			dw_report_1.setitem(ll_t,"num_commessa",991245)
			dw_report_1.setitem(ll_t,"allegato","SI")
			
			
					
		case 2
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 2")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",22.67)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",670)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")

		case 3
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 3")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",240)
			dw_report_1.setitem(ll_t,"nota_prodotto","pajasso coe rode")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",45.87)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")

		case 4
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","hjjj111")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 4")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",6900)
			dw_report_1.setitem(ll_t,"nota_prodotto","utilizzo del trinciapolli")
			dw_report_1.setitem(ll_t,"um_ven","Kg.")
			dw_report_1.setitem(ll_t,"quan_ven",91222)
			dw_report_1.setitem(ll_t,"num_commessa",34324)
			dw_report_1.setitem(ll_t,"allegato","SI")

		case 5
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 2")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",240)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",670)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")
	
		case 6
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 2")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",240)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",670)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")

		case 7
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 2")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",240)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",670)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")

			
		case 8
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 2")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",240)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",670)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")
			
		case 9
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 2")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",240)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",670)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")

			
		case 10
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 2")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",240)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",670)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")
			
	end choose
next

end event

type cb_4 from commandbutton within w_report_1
integer x = 571
integer y = 20
integer width = 265
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "carica 3"
end type

event clicked;long ll_t

dw_report_1.reset()
dw_report_1.DataObject = 'd_report_3'

dw_report_1.Modify("data_pronto.text='10/12/2000'")
dw_report_1.Modify("data_consegna.text='12/12/2000'")
dw_report_1.Modify("des_reparto.text='Reparto dee jostre'")


for ll_t = 1 to 10
	
	dw_report_1.insertrow(ll_t)
	
	choose case ll_t 
		case 1
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"tipo_riga",1)
			dw_report_1.setitem(ll_t,"codice_prodotto","c0505176")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 1")
			dw_report_1.setitem(ll_t,"des_tessuto","pastelli")
			dw_report_1.setitem(ll_t,"cod_colore","P0000")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",230.5)
			dw_report_1.setitem(ll_t,"larghezza",123.4)
			dw_report_1.setitem(ll_t,"altezza",123.4)
			dw_report_1.setitem(ll_t,"luce_finita","F")
			dw_report_1.setitem(ll_t,"posizione_comando","DX")
			dw_report_1.setitem(ll_t,"tipo_comando","GALLOC")
			dw_report_1.setitem(ll_t,"altezza_comando",120)
			dw_report_1.setitem(ll_t,"guide","SI")
			dw_report_1.setitem(ll_t,"des_tipo_supporto","P")
			dw_report_1.setitem(ll_t,"des_tipo_vernice","NE")
			dw_report_1.setitem(ll_t,"num_commessa",991245)
			dw_report_1.setitem(ll_t,"allegato","SI")
					
		case 2
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"tipo_riga",1)
			dw_report_1.setitem(ll_t,"codice_prodotto","c0505176")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 1")
			dw_report_1.setitem(ll_t,"des_tessuto","pastelli")
			dw_report_1.setitem(ll_t,"cod_colore","P0000")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",230.5)
			dw_report_1.setitem(ll_t,"larghezza",123.4)
			dw_report_1.setitem(ll_t,"altezza",123.4)
			dw_report_1.setitem(ll_t,"luce_finita","F")
			dw_report_1.setitem(ll_t,"posizione_comando","DX")
			dw_report_1.setitem(ll_t,"tipo_comando","GALLOC")
			dw_report_1.setitem(ll_t,"altezza_comando",120)
			dw_report_1.setitem(ll_t,"guide","SI")
			dw_report_1.setitem(ll_t,"des_tipo_supporto","P")
			dw_report_1.setitem(ll_t,"des_tipo_vernice","NE")

			dw_report_1.setitem(ll_t,"num_commessa",991245)
			dw_report_1.setitem(ll_t,"allegato","SI")
			

		case 3
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"tipo_riga",1)
			dw_report_1.setitem(ll_t,"codice_prodotto","c0505176")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 1")
			dw_report_1.setitem(ll_t,"des_tessuto","pastelli")
			dw_report_1.setitem(ll_t,"cod_colore","P0000")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",230.5)
			dw_report_1.setitem(ll_t,"larghezza",123.4)
			dw_report_1.setitem(ll_t,"altezza",123.4)
			dw_report_1.setitem(ll_t,"luce_finita","F")
			dw_report_1.setitem(ll_t,"posizione_comando","DX")
			dw_report_1.setitem(ll_t,"tipo_comando","GALLOC")
			dw_report_1.setitem(ll_t,"altezza_comando",120)
			dw_report_1.setitem(ll_t,"guide","SI")
			dw_report_1.setitem(ll_t,"des_tipo_supporto","P")
			dw_report_1.setitem(ll_t,"des_tipo_vernice","NE")

			dw_report_1.setitem(ll_t,"num_commessa",991245)
			dw_report_1.setitem(ll_t,"allegato","SI")
			dw_report_1.setitem(ll_t,"visibile",1)
			
		case 4

			dw_report_1.setitem(ll_t,"tipo_riga",0)
			dw_report_1.setitem(ll_t,"nota","nota di prodotto nota di prodotto nota di prodotto")
		case 5
			dw_report_1.setitem(ll_t,"visibile",1)
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"tipo_riga",1)
			dw_report_1.setitem(ll_t,"codice_prodotto","c0505176")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 1")
			dw_report_1.setitem(ll_t,"des_tessuto","pastelli")
			dw_report_1.setitem(ll_t,"cod_colore","P0000")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",230.5)
			dw_report_1.setitem(ll_t,"larghezza",123.4)
			dw_report_1.setitem(ll_t,"altezza",123.4)
			dw_report_1.setitem(ll_t,"luce_finita","F")
			dw_report_1.setitem(ll_t,"posizione_comando","DX")
			dw_report_1.setitem(ll_t,"tipo_comando","GALLOC")
			dw_report_1.setitem(ll_t,"altezza_comando",120)
			dw_report_1.setitem(ll_t,"guide","SI")
			dw_report_1.setitem(ll_t,"des_tipo_supporto","P")
			dw_report_1.setitem(ll_t,"des_tipo_vernice","NE")

			dw_report_1.setitem(ll_t,"num_commessa",991245)
			dw_report_1.setitem(ll_t,"allegato","SI")

			
		case 6
			
		case 7
						
		case 8
			
		case 9
			
		case 10
			
	end choose
next

end event

type cb_5 from commandbutton within w_report_1
integer x = 1303
integer y = 20
integer width = 503
integer height = 80
integer taborder = 31
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "stampa completa"
end type

event clicked;long ll_t,job

dw_report_1.reset()
dw_report_1.DataObject = 'd_report_1'

dw_report_1.Modify("data_pronto.text='10/12/2000'")
dw_report_1.Modify("data_consegna.text='12/12/2000'")
dw_report_1.Modify("des_reparto.text='Reparto Banane'")
dw_report_1.Modify("vedi_allegato.visible=1")



for ll_t = 1 to 10
	
	dw_report_1.insertrow(ll_t)
	
	choose case ll_t 
		case 1
			dw_report_1.setitem(ll_t,"tipo_riga",1)
			dw_report_1.setitem(ll_t,"codice","codice")
			dw_report_1.setitem(ll_t,"descrizione","c0505176")
			
		case 2
			dw_report_1.setitem(ll_t,"tipo_riga",2)
			dw_report_1.setitem(ll_t,"visibile_2",1)
			dw_report_1.setitem(ll_t,"codice","descrizione")
			dw_report_1.setitem(ll_t,"descrizione","CP COMPASSO SEMITONDO")
			dw_report_1.setitem(ll_t,"quantita","1")
		case 3
			dw_report_1.setitem(ll_t,"tipo_riga",1)
			dw_report_1.setitem(ll_t,"codice","Altezza")
			dw_report_1.setitem(ll_t,"descrizione","123")

		case 4
			dw_report_1.setitem(ll_t,"tipo_riga",1)
			dw_report_1.setitem(ll_t,"codice","Larghezza")
			dw_report_1.setitem(ll_t,"descrizione","153")
		case 5
			dw_report_1.setitem(ll_t,"visibile_2",1)
			dw_report_1.setitem(ll_t,"visibile_1",1)
			dw_report_1.setitem(ll_t,"tipo_riga",3)
			dw_report_1.setitem(ll_t,"nota","nota verniciatura")
			
		case 6
			dw_report_1.setitem(ll_t,"tipo_riga",5)
			dw_report_1.setitem(ll_t,"nota_1"," ")
		case 7
			dw_report_1.setitem(ll_t,"tipo_riga",4)
			dw_report_1.setitem(ll_t,"codice","Codice Opt.")
			dw_report_1.setitem(ll_t,"descrizione","Des. Opt.")
			dw_report_1.setitem(ll_t,"quantita","Q.tà")
			dw_report_1.setitem(ll_t,"um_opt","U.M.")
			dw_report_1.setitem(ll_t,"nota_optional","Nota di prodotto")
			dw_report_1.setitem(ll_t,"num_com_opt","Nr. Com.")
			
		case 8
			dw_report_1.setitem(ll_t,"tipo_riga",4)
			dw_report_1.setitem(ll_t,"visibile_3",1)
			dw_report_1.setitem(ll_t,"visibile_2",1)
			dw_report_1.setitem(ll_t,"visibile_1",1)
			dw_report_1.setitem(ll_t,"codice","TT7711")
			dw_report_1.setitem(ll_t,"descrizione","Banana da Corsa")
			dw_report_1.setitem(ll_t,"um_opt","Ba")
			dw_report_1.setitem(ll_t,"quantita","200")
			dw_report_1.setitem(ll_t,"nota_optional","prodotto per scimmie integrai")
			dw_report_1.setitem(ll_t,"num_com_opt","5001")
		case 9
			dw_report_1.setitem(ll_t,"tipo_riga",5)
			dw_report_1.setitem(ll_t,"nota_1"," ")
			
		case 10
			dw_report_1.setitem(ll_t,"tipo_riga",5)
			dw_report_1.setitem(ll_t,"nota_1","NOTA 1   nota 1    nota 1 nota 1 nota 1 nota 1 nota 1 nota 1")
			
	end choose
next

job = PrintOpen( ) 

PrintDataWindow(job, dw_report_1) 
PrintClose(job)

dw_report_1.reset()
dw_report_1.DataObject = 'd_report_2'

dw_report_1.Modify("data_pronto.text='10/12/2000'")
dw_report_1.Modify("data_consegna.text='12/12/2000'")
dw_report_1.Modify("des_reparto.text='Reparto Pajassi'")




for ll_t = 1 to 10
	
	dw_report_1.insertrow(ll_t)
	
	choose case ll_t 
		case 1
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","c0505176")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 1")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",230.5)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",510.8)
			dw_report_1.setitem(ll_t,"num_commessa",991245)
			dw_report_1.setitem(ll_t,"allegato","SI")
			
			
					
		case 2
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 2")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",22.67)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",670)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")

		case 3
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 3")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",240)
			dw_report_1.setitem(ll_t,"nota_prodotto","pajasso coe rode")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",45.87)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")

		case 4
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","hjjj111")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 4")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",6900)
			dw_report_1.setitem(ll_t,"nota_prodotto","utilizzo del trinciapolli")
			dw_report_1.setitem(ll_t,"um_ven","Kg.")
			dw_report_1.setitem(ll_t,"quan_ven",91222)
			dw_report_1.setitem(ll_t,"num_commessa",34324)
			dw_report_1.setitem(ll_t,"allegato","SI")

		case 5
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 2")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",240)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",670)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")
	
		case 6
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 2")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",240)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",670)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")

		case 7
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 2")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",240)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",670)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")

			
		case 8
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 2")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",240)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",670)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")
			
		case 9
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 2")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",240)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",670)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")

			
		case 10
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"codice_prodotto","123asdmn59123AA")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 2")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",240)
			dw_report_1.setitem(ll_t,"nota_prodotto","nota della riga di dettaglio")
			dw_report_1.setitem(ll_t,"um_ven","pz")
			dw_report_1.setitem(ll_t,"quan_ven",670)
			dw_report_1.setitem(ll_t,"num_commessa",991345)
			dw_report_1.setitem(ll_t,"allegato","NO")
			
	end choose
next

job = PrintOpen( ) 

PrintDataWindow(job, dw_report_1) 
PrintClose(job)

dw_report_1.reset()
dw_report_1.DataObject = 'd_report_3'

dw_report_1.Modify("data_pronto.text='10/12/2000'")
dw_report_1.Modify("data_consegna.text='12/12/2000'")
dw_report_1.Modify("des_reparto.text='Reparto dee jostre'")


for ll_t = 1 to 10
	
	dw_report_1.insertrow(ll_t)
	
	choose case ll_t 
		case 1
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"tipo_riga",1)
			dw_report_1.setitem(ll_t,"codice_prodotto","c0505176")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 1")
			dw_report_1.setitem(ll_t,"des_tessuto","pastelli")
			dw_report_1.setitem(ll_t,"cod_colore","P0000")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",230.5)
			dw_report_1.setitem(ll_t,"larghezza",123.4)
			dw_report_1.setitem(ll_t,"altezza",123.4)
			dw_report_1.setitem(ll_t,"luce_finita","F")
			dw_report_1.setitem(ll_t,"posizione_comando","DX")
			dw_report_1.setitem(ll_t,"tipo_comando","GALLOC")
			dw_report_1.setitem(ll_t,"altezza_comando",120)
			dw_report_1.setitem(ll_t,"guide","SI")
			dw_report_1.setitem(ll_t,"des_tipo_supporto","P")
			dw_report_1.setitem(ll_t,"des_tipo_vernice","NE")
			dw_report_1.setitem(ll_t,"num_commessa",991245)
			dw_report_1.setitem(ll_t,"allegato","SI")
					
		case 2
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"tipo_riga",1)
			dw_report_1.setitem(ll_t,"codice_prodotto","c0505176")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 1")
			dw_report_1.setitem(ll_t,"des_tessuto","pastelli")
			dw_report_1.setitem(ll_t,"cod_colore","P0000")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",230.5)
			dw_report_1.setitem(ll_t,"larghezza",123.4)
			dw_report_1.setitem(ll_t,"altezza",123.4)
			dw_report_1.setitem(ll_t,"luce_finita","F")
			dw_report_1.setitem(ll_t,"posizione_comando","DX")
			dw_report_1.setitem(ll_t,"tipo_comando","GALLOC")
			dw_report_1.setitem(ll_t,"altezza_comando",120)
			dw_report_1.setitem(ll_t,"guide","SI")
			dw_report_1.setitem(ll_t,"des_tipo_supporto","P")
			dw_report_1.setitem(ll_t,"des_tipo_vernice","NE")

			dw_report_1.setitem(ll_t,"num_commessa",991245)
			dw_report_1.setitem(ll_t,"allegato","SI")
			

		case 3
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"tipo_riga",1)
			dw_report_1.setitem(ll_t,"codice_prodotto","c0505176")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 1")
			dw_report_1.setitem(ll_t,"des_tessuto","pastelli")
			dw_report_1.setitem(ll_t,"cod_colore","P0000")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",230.5)
			dw_report_1.setitem(ll_t,"larghezza",123.4)
			dw_report_1.setitem(ll_t,"altezza",123.4)
			dw_report_1.setitem(ll_t,"luce_finita","F")
			dw_report_1.setitem(ll_t,"posizione_comando","DX")
			dw_report_1.setitem(ll_t,"tipo_comando","GALLOC")
			dw_report_1.setitem(ll_t,"altezza_comando",120)
			dw_report_1.setitem(ll_t,"guide","SI")
			dw_report_1.setitem(ll_t,"des_tipo_supporto","P")
			dw_report_1.setitem(ll_t,"des_tipo_vernice","NE")

			dw_report_1.setitem(ll_t,"num_commessa",991245)
			dw_report_1.setitem(ll_t,"allegato","SI")
			dw_report_1.setitem(ll_t,"visibile",1)
			
		case 4

			dw_report_1.setitem(ll_t,"tipo_riga",0)
			dw_report_1.setitem(ll_t,"nota","nota di prodotto nota di prodotto nota di prodotto")
		case 5
			dw_report_1.setitem(ll_t,"visibile",1)
			dw_report_1.setitem(ll_t,"riga",ll_t)
			dw_report_1.setitem(ll_t,"tipo_riga",1)
			dw_report_1.setitem(ll_t,"codice_prodotto","c0505176")
			dw_report_1.setitem(ll_t,"des_prodotto","descrizione 1")
			dw_report_1.setitem(ll_t,"des_tessuto","pastelli")
			dw_report_1.setitem(ll_t,"cod_colore","P0000")
			dw_report_1.setitem(ll_t,"um_mag","cp")
			dw_report_1.setitem(ll_t,"quan_mag",230.5)
			dw_report_1.setitem(ll_t,"larghezza",123.4)
			dw_report_1.setitem(ll_t,"altezza",123.4)
			dw_report_1.setitem(ll_t,"luce_finita","F")
			dw_report_1.setitem(ll_t,"posizione_comando","DX")
			dw_report_1.setitem(ll_t,"tipo_comando","GALLOC")
			dw_report_1.setitem(ll_t,"altezza_comando",120)
			dw_report_1.setitem(ll_t,"guide","SI")
			dw_report_1.setitem(ll_t,"des_tipo_supporto","P")
			dw_report_1.setitem(ll_t,"des_tipo_vernice","NE")

			dw_report_1.setitem(ll_t,"num_commessa",991245)
			dw_report_1.setitem(ll_t,"allegato","SI")

			
		case 6
			
		case 7
						
		case 8
			
		case 9
			
		case 10
			
	end choose
next

job = PrintOpen( ) 

PrintDataWindow(job, dw_report_1) 
PrintClose(job)

end event

