﻿$PBExportHeader$w_report_buffer_carico_stampa.srw
$PBExportComments$Finestra Report Attrezzature Semplici
forward
global type w_report_buffer_carico_stampa from window
end type
type cb_stampa from commandbutton within w_report_buffer_carico_stampa
end type
type dw_report from uo_cs_xx_dw within w_report_buffer_carico_stampa
end type
end forward

global type w_report_buffer_carico_stampa from window
integer width = 3899
integer height = 2292
boolean titlebar = true
string title = "Stampa Colli Mancanti"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
boolean center = true
cb_stampa cb_stampa
dw_report dw_report
end type
global w_report_buffer_carico_stampa w_report_buffer_carico_stampa

type variables
long il_progressivo
end variables

forward prototypes
public function integer wf_report (long fl_id)
public function integer wf_scrivi_nota (long fl_anno, long fl_numero, ref string fs_nota[])
end prototypes

public function integer wf_report (long fl_id);string 			ls_sql, ls_cod_cliente, ls_cod_reparto, ls_cod_prodotto, ls_cod_colore_tessuto,&
					ls_cod_verniciatura, ls_descrizione, ls_appo, ls_null, ls_sql_det, ls_note[]
datastore 		lds_data, lds_det
long 				ll_index, ll_rows, ll_anno, ll_numero, ll_colli, ll_prog_riga, ll_new, ll_tot_colli, ll_tot_det, ll_index_det

/*
ls_sql = "	select cod_azienda,"+&
				"progressivo,"+&
				"anno_registrazione,"+&
				"num_registrazione,"+&
				"prog_riga_ord_ven,"+&
				"max_num_tenda,"+&
				"tot_colli,"+&
				"num_colli,"+&
				"cod_reparto,"+&
				"cod_cliente,"+&
				"cod_prodotto,"+&
				"cod_colore_tessuto,"+&
				"cod_verniciatura,"+&
				"cod_operaio,"+&
				"data_creazione,"+&
				"ora_creazione "+&
			"from buffer_stampa_ord_carico "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
				"progressivo="+string(fl_id)+" "+&
			"order by anno_registrazione, num_registrazione, prog_riga_ord_ven "
*/

setnull(ls_null)
dw_report.reset()

ls_sql = "select anno_registrazione as anno_ordine,"+&
				"num_registrazione as num_ordine,"+&				
				"sum(num_colli) as colli "+&				
			"from buffer_stampa_ord_carico "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
				"progressivo="+string(fl_id)+" "+&
			"group by anno_registrazione, num_registrazione "+&
			"order by anno_registrazione, num_registrazione "

if not f_crea_datastore(lds_data, ls_sql) then
	g_mb.error("APICE", "Errore in creazione datastore")
	rollback;
	return -1
end if

ll_rows = lds_data.retrieve()

for ll_index=1 to ll_rows
	ll_anno = lds_data.getitemnumber(ll_index, 1)
	ll_numero = lds_data.getitemnumber(ll_index, 2)
	ll_colli = lds_data.getitemnumber(ll_index, 3)
	
	//verifica se mancante totale o parziale	
	select 	num_colli,
				cod_cliente
	into		:ll_tot_colli,
				:ls_cod_cliente
	from tes_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno and
				num_registrazione=:ll_numero;
	
	if ll_tot_colli>0 then
		
		//se completo non elaborare
		if ll_tot_colli = ll_colli then
			continue
		end if
		
		setnull(ll_prog_riga)
		setnull(ll_prog_riga)
		setnull(ls_cod_reparto)
		setnull(ls_cod_prodotto)
		setnull(ls_cod_colore_tessuto)
		setnull(ls_cod_verniciatura)
		
		ls_sql_det = "select 	prog_riga_ord_ven,"+&
										"cod_reparto,"+&
										"cod_prodotto,"+&
										"cod_colore_tessuto,"+&
										"cod_verniciatura "+&
		"from buffer_stampa_ord_carico "+&
		"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
					"progressivo="+string(fl_id)+" and "+&
					"anno_registrazione="+string(ll_anno)+" and "+&
					"num_registrazione="+string(ll_numero) +" "+&
		"order by prog_riga_ord_ven "
		
		if not f_crea_datastore(lds_det, ls_sql_det) then
			destroy lds_data;
			g_mb.error("APICE", "Errore in creazione datastore dettaglio")
			rollback;
			return -1
		end if
		
		ll_tot_det = lds_det.retrieve()
		for ll_index_det=1 to ll_tot_det
			ll_prog_riga					= lds_det.getitemnumber(ll_index_det, 1)
			ls_cod_reparto				= lds_det.getitemstring(ll_index_det, 2)
			ls_cod_prodotto				= lds_det.getitemstring(ll_index_det, 3)
			ls_cod_colore_tessuto	= lds_det.getitemstring(ll_index_det, 4)
			ls_cod_verniciatura		= lds_det.getitemstring(ll_index_det, 5)			
			
			ll_new = dw_report.insertrow(0)		
			dw_report.setitem(ll_new, "ordine", string(ll_anno)+"/"+string(ll_numero))
			dw_report.setitem(ll_new, "cliente", ls_cod_cliente)			
			
			if ll_colli<ll_tot_colli and ll_colli=0 then
				//mancante totale
				dw_report.setitem(ll_new, "tipo", "T")
				dw_report.setitem(ll_new, "mancanti", "MANCANTI "+string(ll_tot_colli))
			else
				//mancante parziale
				dw_report.setitem(ll_new, "tipo", "P")
				dw_report.setitem(ll_new, "mancanti", "MANCANTI "+string(ll_tot_colli - ll_colli)+" SU "+string(ll_tot_colli))
			end if
			
			if ll_prog_riga>0 then
				//tenda da sole
				dw_report.setitem(ll_new, "riga", "Riga:"+string(ll_prog_riga))
				dw_report.setitem(ll_new, "reparto", ls_null)
				
				ls_descrizione = f_des_tabella("anag_prodotti","cod_prodotto='"  + ls_cod_prodotto +"'", "des_prodotto")
				
				if not isnull(ls_cod_colore_tessuto) and ls_cod_colore_tessuto<>"" then
					ls_descrizione +=", "+ls_cod_colore_tessuto
				end if
				
				ls_appo = f_des_tabella("tab_colori_tessuti","cod_prodotto='"  + ls_cod_colore_tessuto +"'", "des_colore_tessuto")
				if not isnull(ls_appo) and ls_appo<>"" then
					ls_descrizione +=", "+ls_appo
				end if
				
				if not isnull(ls_cod_verniciatura) and ls_cod_verniciatura<>"" then
					ls_appo = f_des_tabella("anag_prodotti","cod_prodotto='"  + ls_cod_verniciatura +"'", "des_prodotto")
					if not isnull(ls_appo) and ls_appo<>"" then
						ls_descrizione +=", "+ls_appo
					end if
				end if
				dw_report.setitem(ll_new, "descrizione", ls_descrizione)
			else
				//tenda tecnica o sfuso
				dw_report.setitem(ll_new, "riga", ls_null)
				dw_report.setitem(ll_new, "reparto", ls_cod_reparto)
				dw_report.setitem(ll_new, "descrizione", ls_null)
			end if
			
		next
	else
		//non ci sono colli prodotti: scrivilo nelle note
		wf_scrivi_nota(ll_anno, ll_numero, ls_note[])
	end if
next

dw_report.sort()
dw_report.groupcalc()

destroy lds_data;

return 1
end function

public function integer wf_scrivi_nota (long fl_anno, long fl_numero, ref string fs_nota[]);long ll_ubound
string ls_appo

ll_ubound = upperbound(fs_nota[])

if ll_ubound > 0 then
	 fs_nota[ll_ubound + 1] = string(fl_anno)+"/"+string(fl_numero)
else
	 fs_nota[1] = string(fl_anno)+"/"+string(fl_numero)
	dw_report.object.t_note.text = "Sembra che i seguenti ordini non abbiano colli prodotti: "
end if

ls_appo = dw_report.object.t_note.text
ls_appo += string(fl_anno)+"/"+string(fl_numero)
dw_report.object.t_note.text = ls_appo

return 1
end function

on w_report_buffer_carico_stampa.create
this.cb_stampa=create cb_stampa
this.dw_report=create dw_report
this.Control[]={this.cb_stampa,&
this.dw_report}
end on

on w_report_buffer_carico_stampa.destroy
destroy(this.cb_stampa)
destroy(this.dw_report)
end on

event open;long l_Error

il_progressivo = long(message.stringparm)

dw_report.settransobject(sqlca)
l_Error = dw_report.Retrieve(s_cs_xx.cod_azienda, il_progressivo)

//wf_report(il_progressivo)

this.title = "Report ID="+string(il_progressivo)
end event

type cb_stampa from commandbutton within w_report_buffer_carico_stampa
integer x = 41
integer y = 24
integer width = 402
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;
dw_report.print(true, true)
end event

type dw_report from uo_cs_xx_dw within w_report_buffer_carico_stampa
integer x = 23
integer y = 136
integer width = 3840
integer height = 1980
integer taborder = 10
string dataobject = "d_report_buffer_carico"
boolean hscrollbar = true
boolean vscrollbar = true
end type

