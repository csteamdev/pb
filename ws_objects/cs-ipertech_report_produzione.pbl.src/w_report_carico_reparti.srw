﻿$PBExportHeader$w_report_carico_reparti.srw
$PBExportComments$Window report carico reparti
forward
global type w_report_carico_reparti from w_cs_xx_principale
end type
type dw_report_2 from uo_cs_xx_dw within w_report_carico_reparti
end type
type hpb_1 from hprogressbar within w_report_carico_reparti
end type
type dw_reparti from datawindow within w_report_carico_reparti
end type
type dw_ricerca from u_dw_search within w_report_carico_reparti
end type
type st_text from statictext within w_report_carico_reparti
end type
type dw_sel_giri from datawindow within w_report_carico_reparti
end type
type st_9 from statictext within w_report_carico_reparti
end type
type cb_annulla from commandbutton within w_report_carico_reparti
end type
type cb_report from commandbutton within w_report_carico_reparti
end type
type r_1 from rectangle within w_report_carico_reparti
end type
type st_4 from statictext within w_report_carico_reparti
end type
type dw_report from uo_cs_xx_dw within w_report_carico_reparti
end type
end forward

global type w_report_carico_reparti from w_cs_xx_principale
integer width = 2615
integer height = 2764
string title = "Report Carico Reparti"
dw_report_2 dw_report_2
hpb_1 hpb_1
dw_reparti dw_reparti
dw_ricerca dw_ricerca
st_text st_text
dw_sel_giri dw_sel_giri
st_9 st_9
cb_annulla cb_annulla
cb_report cb_report
r_1 r_1
st_4 st_4
dw_report dw_report
end type
global w_report_carico_reparti w_report_carico_reparti

forward prototypes
public function integer wf_giorno_settimana (datetime fdt_data, ref string fs_giorno)
public function integer wf_trova_reparti (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_versione, ref string fs_cod_reparto[], ref string fs_errore)
public function integer wf_report (ref string fs_errore)
public function integer wf_carica (ref string fs_errore)
public function integer wf_controlla_reparti (ref string fs_reparti_selezionati, ref string fs_errore)
public function integer wf_reparti_da_fasi (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_cod_reparti_trovati[], ref string fs_errore)
public function integer wf_carica_2 (ref string fs_errore)
public function integer wf_report_2 (ref string fs_errore)
public function integer wf_report_3 (ref string fs_errore)
end prototypes

public function integer wf_giorno_settimana (datetime fdt_data, ref string fs_giorno);integer li_num_giorno

li_num_giorno = DayNumber(date(fdt_data))

choose case li_num_giorno
	case 1
		fs_giorno="Domnica"
	
	case 2
		fs_giorno="Lunedì"

	case 3
		fs_giorno="Martedì"

	case 4
		fs_giorno="Mercoledì"

	case 5
		fs_giorno="Giovedì"

	case 6
		fs_giorno="Venerdì"

	case 7
		fs_giorno="Sabato"

end choose

return 0
end function

public function integer wf_trova_reparti (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_versione, ref string fs_cod_reparto[], ref string fs_errore);// Funzione che trova i reparti associati alle fasi di lavoro che sono da stampare 
// nome: wf_trova_reparti
// tipo: integer
//       -1 failed
//  		 0 passed
//
//	Variabili passate: 		nome					 tipo				passaggio per			commento
//							
//
//		Creata il 17-12-1999 (ripresa dalla funzione f_trova_reparti della window w_stampa_ordini_prod
//		Autore Diego Ferrari

string    ls_cod_reparto_liv_n,ls_cod_prodotto_figlio, ls_test_prodotto_f, ls_errore,ls_cod_reparto, ls_cod_prodotto_inserito, & 
			 ls_test, ls_cod_prodotto_variante,ls_flag_materia_prima,ls_flag_materia_prima_variante,ls_flag_stampa_fase
long      ll_num_figli,ll_num_sequenza_fasi_livello,ll_num_righe,ll_t,ll_num_reparti,ll_num_sequenza
integer   li_risposta
boolean   lb_test

datastore lds_righe_distinta


ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli=1 to ll_num_righe
	ls_cod_prodotto_figlio=lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ls_flag_materia_prima=lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")	
	ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	
	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio

	select cod_prodotto,
			 flag_materia_prima
	into   :ls_cod_prodotto_variante,	
			 :ls_flag_materia_prima_variante
	from   varianti_det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:fi_anno_registrazione
	and    num_registrazione=:fl_num_registrazione
	and    prog_riga_ord_ven=:fl_prog_riga_ord_ven
	and    cod_prodotto_padre=:fs_cod_prodotto
	and    cod_prodotto_figlio=:ls_cod_prodotto_figlio
	and    cod_versione=:fs_cod_versione
	and    num_sequenza=:ll_num_sequenza;

   if sqlca.sqlcode < 0 then
		fs_errore="Errore nel DB"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode <> 100 then
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante
		ls_flag_materia_prima = ls_flag_materia_prima_variante
	end if		

	if ls_flag_materia_prima = 'N' or isnull(ls_flag_materia_prima) then
		select cod_azienda
		into   :ls_test
		from   distinta
		where  cod_azienda=:s_cs_xx.cod_azienda 
		and 	 cod_prodotto_padre=:ls_cod_prodotto_inserito;
		
		if sqlca.sqlcode=100 then continue
	else
		continue
	end if

   SELECT cod_reparto,
   		 flag_stampa_fase
   INTO   :ls_cod_reparto,
			 :ls_flag_stampa_fase
   FROM   tes_fasi_lavorazione
   WHERE  cod_azienda = :s_cs_xx.cod_azienda  
	AND    cod_prodotto = :ls_cod_prodotto_inserito;

	if sqlca.sqlcode=100 then continue
  
	if ls_flag_stampa_fase = 'N' then continue
	
   lb_test = false

   for ll_t = 1 to upperbound(fs_cod_reparto[])
		if fs_cod_reparto[ll_t] = ls_cod_reparto then lb_test = true
	next
	
	if lb_test = false then 
		ll_num_reparti = upperbound(fs_cod_reparto[])
		if isnull(ll_num_reparti) then 
			ll_num_reparti = 1
		else
			ll_num_reparti++
		end if
		
		fs_cod_reparto[ll_num_reparti] = ls_cod_reparto
		
	end if
	

	select cod_prodotto_figlio 
	into   :ls_test_prodotto_f
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda
	and	 cod_prodotto_padre=:ls_cod_prodotto_inserito;

	if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
		continue
	else
		li_risposta=wf_trova_reparti(fi_anno_registrazione,fl_num_registrazione,fl_prog_riga_ord_ven,ls_cod_prodotto_inserito,fs_cod_versione,fs_cod_reparto[],ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	
next

destroy lds_righe_distinta

return 0
end function

public function integer wf_report (ref string fs_errore);string					ls_cod_giro_consegna,ls_cod_cliente,ls_cod_prodotto,ls_cod_reparto,ls_sql,ls_giorno,ls_cod_reparto_precedente, & 
						ls_des_reparto,ls_ordine_interno,ls_des_prodotto,ls_rag_soc_1,ls_cod_prodotto_comp,ls_cod_tessuto,ls_cod_prod_finito, &
						ls_cod_prodotto_stampa,ls_des_tessuto,ls_cod_colore_tessuto,ls_des_colore_tessuto,ls_des_verniciatura, & 
						ls_cod_verniciatura,ls_cod_misura_mag,ls_tipo_ordinamento, ls_flag_esegui_somme, ls_sql_count
						
long					ll_num_registrazione,ll_prog_riga_ord_ven,ll_num_righe,job,ll_num_record, ll_stato_prod,ll_stato_prod_app, ll_num_riga_appartenenza, ll_tot, ll_conteggio

integer				li_anno_registrazione

double				ldd_quan_ordine,ldd_dim_x,ldd_dim_y,ldd_dim_z,ldd_dim_t

datetime				ldt_data_pronto,ldt_data_pronto_precedente

datastore			lds_data_count

// eseguire somme
dw_report.reset()

dw_report.object.datawindow.print.orientation = 1
declare righe_carico dynamic cursor for sqlsa;

ls_sql_count = "select count(*) from tab_carico_reparti "

ls_flag_esegui_somme = dw_ricerca.getitemstring(1,"flag_esegui_somme")
if ls_flag_esegui_somme = "S" then
	ls_sql = "select " + &
				" cod_prodotto, " + &
				" cod_reparto, " + &
				" data_pronto, " + &
				" sum(quan_ordine), " + &
				" stato_prod " + &
				" from tab_carico_reparti " + &
				" where cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
						  "cod_utente='"+s_cs_xx.cod_utente+"' "+&
				" group by cod_prodotto,cod_reparto,data_pronto,stato_prod order by cod_reparto,data_pronto"
	
	//riportare la stessa where e group by
	ls_sql_count += " where cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
									"cod_utente='"+s_cs_xx.cod_utente+"' "+&
							" group by cod_prodotto,cod_reparto,data_pronto,stato_prod order by cod_reparto,data_pronto"
else
	ls_sql = "select cod_giro_consegna, " + &
				" cod_cliente, " + &
				" cod_prodotto, " + &
				" cod_reparto, " + &
				" anno_registrazione, " + &
				" num_registrazione, " + &
				" prog_riga_ord_ven, " + &
				" data_pronto, " + &
				" quan_ordine, " + &
				" stato_prod, " + &
				" num_riga_appartenenza " + &
				" from tab_carico_reparti " + &
				" where cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
				 		 "cod_utente='"+s_cs_xx.cod_utente+"' "+&
				" order by cod_reparto,data_pronto,cod_giro_consegna"
	
	//riportare la stessa where e group by
	ls_sql_count += " where cod_azienda='" + s_cs_xx.cod_azienda + "' and " + &
								     "cod_utente='"+s_cs_xx.cod_utente+"' "
	
end if

// ordinamento interno
ls_tipo_ordinamento = dw_ricerca.getitemstring(1, "tipo_ordinamento")

choose case ls_tipo_ordinamento
	case "C"	//codice cliente
		ls_sql = ls_sql + ",cod_cliente"
		
		//ls_sql_count += ",cod_cliente"
		
		ls_ordine_interno = " CLIENTE"
		
	case "O"	//numero ordine
		ls_sql = ls_sql + ",num_registrazione,prog_riga_ord_ven"
		
		//ls_sql_count += ",num_registrazione,prog_riga_ord_ven"
		
		ls_ordine_interno = " N° ORDINE"
		
	case else		//codice Prodotto
		ls_sql = ls_sql + ",cod_prodotto"
		
		//ls_sql_count += ",cod_prodotto"
		
		ls_ordine_interno = " PRODOTTO"
		
end choose


ll_tot = guo_functions.uof_crea_datastore(lds_data_count, ls_sql_count, fs_errore)
if ll_tot<0 then
	//in fs_errore il messaggio
	return -1
end if
if ll_tot>0 then
	ll_tot = lds_data_count.getitemnumber(1, 1)
else
	fs_errore = "Nessuna riga da elaborare!"
	return -1
end if
if isnull(ll_tot) then ll_tot = 0

if ll_tot=0 then
	fs_errore = "Nessuna riga da elaborare!"
	return -1
end if


hpb_1.maxposition = ll_tot
ll_conteggio = 0


prepare sqlsa from :ls_sql;
open dynamic righe_carico;
if sqlca.sqlcode<>0 then
	fs_errore = "Errore in OPEN cursore righe_carico.~r~n" + sqlca.sqlerrtext
	return -1
end if


do while 1=1
	if ls_flag_esegui_somme = "S" then
		
		fetch righe_carico
		into :ls_cod_prodotto,
			  :ls_cod_reparto,
			  :ldt_data_pronto,
			  :ldd_quan_ordine,
			  :ll_stato_prod;
	else
		
		fetch righe_carico
		into :ls_cod_giro_consegna,
			  :ls_cod_cliente,
			  :ls_cod_prodotto,
			  :ls_cod_reparto,
			  :li_anno_registrazione,
			  :ll_num_registrazione,
			  :ll_prog_riga_ord_ven,
			  :ldt_data_pronto,
			  :ldd_quan_ordine,
			  :ll_stato_prod,
			  :ll_num_riga_appartenenza;
	end if
	if sqlca.sqlcode<0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close righe_carico;
		return -1
	end if
	
	yield()
	st_text.text = ls_cod_prodotto + " " + ls_cod_reparto + " " + string(ldt_data_pronto,"dd/mm/yyyy")
	ll_conteggio += 1
	hpb_1.stepit( )
	
	
	ll_num_record++
	
	if sqlca.sqlcode=100 then 
		if ll_num_record > 1 then
			select des_reparto
			into   :ls_des_reparto
			from   anag_reparti
			where  cod_azienda = :s_cs_xx.cod_azienda  and
					 cod_reparto = :ls_cod_reparto_precedente;
			
			if sqlca.sqlcode<0 then
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				close righe_carico;
				return -1
			end if

			dw_report.Modify("des_reparto.text='" + ls_des_reparto + "'")
			dw_report.Modify("ordine_interno.text='" + ls_ordine_interno + "'")
			dw_report.print()
		end if
		exit
	end if

	if ls_flag_esegui_somme <> "S" then

		select rag_soc_1
		into   :ls_rag_soc_1
		from   anag_clienti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_cliente=:ls_cod_cliente;
		
		if sqlca.sqlcode<0 then
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			close righe_carico;
			return -1
		end if
		
		select cod_prod_finito,
				 cod_tessuto,
				 dim_x,
				 dim_y,
				 dim_z,
				 dim_t,
				 cod_non_a_magazzino,
				 cod_verniciatura
		into   :ls_cod_prod_finito,
				 :ls_cod_tessuto,
				 :ldd_dim_x,
				 :ldd_dim_y,
				 :ldd_dim_z,
				 :ldd_dim_t,
				 :ls_cod_colore_tessuto,
				 :ls_cod_verniciatura
		from   comp_det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:li_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    prog_riga_ord_ven=:ll_prog_riga_ord_ven;
	
		if sqlca.sqlcode<0 then
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			close righe_carico;
			return -1
		end if
	
		
		if sqlca.sqlcode = 100 then
			ls_cod_prodotto_stampa = ls_cod_prodotto
		else
			ls_cod_prodotto_stampa = ls_cod_prod_finito
	
			select des_prodotto
			into   :ls_des_tessuto
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_tessuto;
	
			if sqlca.sqlcode<0 then
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				close righe_carico;
				return -1
			end if
	
			select des_colore_tessuto
			into   :ls_des_colore_tessuto
			from   tab_colori_tessuti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_colore_tessuto=:ls_cod_colore_tessuto;
			
			if sqlca.sqlcode<0 then
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				close righe_carico;
				return -1
			end if
	
			select cod_comodo
			into   :ls_des_verniciatura
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_verniciatura;
			
			if sqlca.sqlcode<0 then
				fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
				close righe_carico;
				return -1
			end if
	
		end if
	
	end if	


	select des_prodotto,
			 cod_misura_mag
	into   :ls_des_prodotto,
			 :ls_cod_misura_mag
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;
	
	if sqlca.sqlcode<0 then
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close righe_carico;
		return -1
	end if

	if ls_cod_reparto_precedente <> ls_cod_reparto and ll_num_record > 1 then
		select des_reparto
		into   :ls_des_reparto
		from   anag_reparti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_reparto=:ls_cod_reparto_precedente;
		
		if sqlca.sqlcode<0 then
			fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
			close righe_carico;
			return -1
		end if

		dw_report.Modify("des_reparto.text='" + ls_des_reparto + "'")
		dw_report.Modify("ordine_interno.text='" + ls_ordine_interno + "'")
		dw_report.print()
		dw_report.reset()
		ll_num_righe=0
		ldt_data_pronto_precedente = datetime(date(s_cs_xx.db_funzioni.data_neutra),00:00:00)
		
	end if
		
	if ldt_data_pronto_precedente <> ldt_data_pronto or ll_num_righe = 0 then
		wf_giorno_settimana (ldt_data_pronto,ls_giorno) 
		ll_num_righe++
		dw_report.insertrow(ll_num_righe)
		dw_report.setitem(ll_num_righe,"flag_data_pronto",1)
		dw_report.setitem(ll_num_righe,"data_pronto","DATA PRONTO            " + ls_giorno + "   " + string(date(ldt_data_pronto)))
		
	end if
	
	yield()
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	
	if ls_flag_esegui_somme = "S" then
		dw_report.setitem(ll_num_righe,"flag_data_pronto",0)
		dw_report.setitem(ll_num_righe,"cod_prodotto",ls_cod_prodotto)
		dw_report.setitem(ll_num_righe,"des_prodotto",ls_des_prodotto)
		dw_report.setitem(ll_num_righe,"unita_misura",ls_cod_misura_mag)
		dw_report.setitem(ll_num_righe,"quantita",ldd_quan_ordine)
	else
		dw_report.setitem(ll_num_righe,"flag_data_pronto",0)
		dw_report.setitem(ll_num_righe,"ordine",string(ll_num_registrazione)+"/"+string(ll_prog_riga_ord_ven))
		dw_report.setitem(ll_num_righe,"rag_soc_1",ls_rag_soc_1)
		dw_report.setitem(ll_num_righe,"cod_prodotto",ls_cod_prodotto_stampa)
		dw_report.setitem(ll_num_righe,"des_prodotto",ls_des_prodotto + " " + ls_des_tessuto)
		dw_report.setitem(ll_num_righe,"dim_x",ldd_dim_x)
		dw_report.setitem(ll_num_righe,"dim_y",ldd_dim_y)
		dw_report.setitem(ll_num_righe,"dim_z",ldd_dim_z)
		dw_report.setitem(ll_num_righe,"dim_t",ldd_dim_t)
		dw_report.setitem(ll_num_righe,"unita_misura",ls_cod_misura_mag)
		dw_report.setitem(ll_num_righe,"quantita",ldd_quan_ordine)
		dw_report.setitem(ll_num_righe,"cod_colore",ls_cod_colore_tessuto + " " + ls_des_colore_tessuto)
		dw_report.setitem(ll_num_righe,"cod_verniciatura",ls_des_verniciatura)
		dw_report.setitem(ll_num_righe,"cod_giro_consegna",ls_cod_giro_consegna)
	
		// aggiunto da EnMe per gestione stato anche dei motorizzazioni / accessori collegati con prodotto finito
		if ll_num_riga_appartenenza > 0 and not isnull(ll_num_riga_appartenenza) then
			select stato_prod
			into   :ll_stato_prod_app
			from   tab_carico_reparti
			where	cod_azienda = :s_cs_xx.cod_azienda and
						cod_utente=:s_cs_xx.cod_utente and
						anno_registrazione = :li_anno_registrazione and
						num_registrazione = :ll_num_registrazione and
						prog_riga_ord_ven = :ll_num_riga_appartenenza and
						cod_reparto = :ls_cod_reparto;
			if sqlca.sqlcode = 0 then
				ll_stato_prod = ll_stato_prod_app
			end if
		end if
	end if
	
	dw_report.setitem(ll_num_righe,"stato_prod",ll_stato_prod)
	
	ldt_data_pronto_precedente = ldt_data_pronto
	ls_cod_reparto_precedente = ls_cod_reparto
	
	ls_cod_giro_consegna = ""
	ls_cod_cliente = ""
	ls_cod_prodotto = ""
	ls_des_reparto = ""
	ls_des_prodotto = ""
	ls_rag_soc_1= ""
	ls_cod_prodotto_comp= ""
	ls_cod_tessuto= ""
	ls_cod_prod_finito= ""
	ls_cod_prodotto_stampa= ""
	ls_des_tessuto= ""
	ls_cod_colore_tessuto= ""
	ls_des_colore_tessuto= ""
	ls_des_verniciatura = ""
	ls_cod_verniciatura= ""
	ls_cod_misura_mag= ""
	ll_num_registrazione=0
	ll_prog_riga_ord_ven=0
	li_anno_registrazione=0
	ldd_quan_ordine=0
	ldd_dim_x=0
	ldd_dim_y=0
	ldd_dim_z=0
	ldd_dim_t=0

loop

close righe_carico;

delete tab_carico_reparti 
where 	cod_azienda=:s_cs_xx.cod_azienda and 
			cod_utente=:s_cs_xx.cod_utente;
commit;

return 0
end function

public function integer wf_carica (ref string fs_errore);integer 	li_risposta,li_anno_registrazione
long 		ll_num_registrazione,ll_prog_riga_ord_ven,ll_i,ll_t, ll_stato_prod_filtro, ll_stato_prod_ordine, ll_y,& 
 			ll_num_riga_appartenenza, ll_rows, ll_idx, ll_cont_distinte
string 	ls_cod_prodotto, ls_cod_versione, ls_cod_reparti_trovati[],ls_errore,ls_cod_giro_consegna,ls_cod_cliente, & 
		 	ls_da_cod_prodotto, ls_a_cod_prodotto,ls_cod_reparto,ls_cod_colore,ls_cod_verniciatura,ls_cod_misura, ls_sql_1,& 
		 	ls_test,ls_flag_stampa_fase,ls_sql,ls_test_verniciatura,ls_test_colore,ls_default, ls_cod_tipo_det_ven, ls_text_log, &
		 	ls_sql_2, ls_messaggio, ls_pf, ls_reparti_letti[], ls_vuoto[],ls_cod_reparto_filtro, ls_cod_tipo_ord_ven,ls_tipo_stampa, ls_flag_escludi_evase
datetime ldt_data_pronto_inizio,ldt_data_pronto_fine,ldt_data_pronto
dec{4} 	ldd_quan_ordine

uo_produzione luo_prod
datastore     lds_datastore
uo_funzioni_1  luo_funzioni

//elimino precedenti sessioni dell'utente
delete from tab_carico_reparti 
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente;
commit;

ldt_data_pronto_inizio = dw_ricerca.getitemdatetime(1, "data_inizio_pronto")
ldt_data_pronto_inizio = datetime(date(ldt_data_pronto_inizio), 00:00:00)

ldt_data_pronto_fine = dw_ricerca.getitemdatetime(1, "data_fine_pronto")
ldt_data_pronto_fine = datetime(date(ldt_data_pronto_fine), 23:59:59)

if isnull(ldt_data_pronto_inizio) or year(date(ldt_data_pronto_inizio))<1950 or isnull(ldt_data_pronto_fine) or year(date(ldt_data_pronto_fine))<1950 then
	fs_errore="L'impostazione delle date di pronto è obbligatoria!"
	return -1
end if

ls_da_cod_prodotto = dw_ricerca.getitemstring (  1,"cod_prodotto_da")
ls_a_cod_prodotto = dw_ricerca.getitemstring (  1,"cod_prodotto_a")

ls_cod_colore = dw_ricerca.getitemstring (  1,"cod_colore")
ls_cod_verniciatura = dw_ricerca.getitemstring (  1,"cod_verniciatura")

ls_pf = dw_ricerca.getitemstring(1, "flag_prodotti_finiti")
if isnull(ls_pf) or len(ls_pf) < 1 then
	ls_pf="T"
end if

ls_cod_giro_consegna = ""
for ll_y = 1 to dw_sel_giri.rowcount()
	if dw_sel_giri.getitemstring(ll_y,"flag_selezione") = "S" then
		if len(ls_cod_giro_consegna) > 0 then ls_cod_giro_consegna = ls_cod_giro_consegna + ", "
		ls_cod_giro_consegna = ls_cod_giro_consegna + "'" + dw_sel_giri.getitemstring(ll_y,"cod_giro_consegna") + "'"
	end if
next

//controlla se hai selezionato almeno un reparto
if wf_controlla_reparti(ls_cod_reparto_filtro, fs_errore)<0 then
	//in fs_errore il messaggio
	rollback;
	return -1
end if

ll_stato_prod_filtro = dw_ricerca.getitemnumber(  1,"stato_produzione")

if isnull(ls_cod_giro_consegna) or ls_cod_giro_consegna = "" or ls_cod_giro_consegna="none" then 
	ls_sql_1=" order by cod_giro_consegna asc"   										//modifica richiesta con la funzione 21 di spec_varie_5
	ls_sql_2=""
else																									//modifica richiesta con la funzione 21 di spec_varie_5
//	ls_sql_2 = " and a.cod_giro_consegna like '" + ls_cod_giro_consegna + "'"  //modifica richiesta con la funzione 21 di spec_varie_5
	ls_sql_2 = " and a.cod_giro_consegna in (" + ls_cod_giro_consegna + ")"  
	ls_sql_1=""
end if

ls_sql = "select b.prog_riga_ord_ven, " + &
			" b.cod_prodotto, " + &
			" b.cod_misura, " + &
			" b.quan_ordine - b.quan_evasa, " + &
			" b.cod_versione, " + &
			" b.num_riga_appartenenza, " + &
			" a.anno_registrazione, " + &
			" a.num_registrazione, " + &
			" a.data_pronto, " + &
			" a.cod_cliente, " + &
			" a.cod_giro_consegna," + &
			" a.cod_tipo_ord_ven,"+&
			" b.cod_tipo_det_ven "+&
			" from tes_ord_ven a, det_ord_ven b " + &
			" where  a.cod_azienda='" + s_cs_xx.cod_azienda + "'" + &
			" and b.anno_registrazione=a.anno_registrazione" + &
			" and b.num_registrazione=a.num_registrazione" + &
			" and b.flag_blocco='N' and num_riga_appartenenza=0 "+&
			" and a.data_pronto between '" + string(date(ldt_data_pronto_inizio),s_cs_xx.db_funzioni.formato_data) + "' and '" + string(date(ldt_data_pronto_fine),s_cs_xx.db_funzioni.formato_data) + "'" + &
			ls_sql_2 //+ &
			//" and (b.quan_ordine - b.quan_evasa)>0 " 
			
if not isnull(ls_da_cod_prodotto) and ls_da_cod_prodotto <> "" then ls_sql = ls_sql + " and b.cod_prodotto >= '" + ls_da_cod_prodotto + "' "
if not isnull(ls_a_cod_prodotto) and ls_a_cod_prodotto <> "" then ls_sql = ls_sql + " and b.cod_prodotto <= '" + ls_a_cod_prodotto + "' "

ls_flag_escludi_evase = dw_ricerca.getitemstring(1, "flag_escludi_evase")

if ls_flag_escludi_evase = "S" then
	ls_sql = ls_sql + " and b.flag_evasione <> 'E'"
end if

choose case ls_pf
	case "S"
		ls_sql = ls_sql + " and b.cod_prodotto not like '_0%' "

	case "F"
		ls_sql = ls_sql + " and b.cod_prodotto like '_0%' "
		
	case else
		//tutti
		
end choose

	
ls_sql=ls_sql+ls_sql_1

if not f_crea_datastore ( ref lds_datastore, ls_sql ) then
	fs_errore = "Errore in creazione datastore~r~n" + sqlca.sqlerrtext
	return -1
end if

ll_rows = lds_datastore.retrieve()

hpb_1.maxposition = ll_rows
hpb_1.setstep = 1

for ll_idx = 1 to ll_rows

	ll_prog_riga_ord_ven = lds_datastore.getitemnumber(ll_idx, 1)
	ls_cod_prodotto = lds_datastore.getitemstring(ll_idx, 2)
	ls_cod_misura = lds_datastore.getitemstring(ll_idx, 3)
	ldd_quan_ordine = lds_datastore.getitemnumber(ll_idx, 4)
	ls_cod_versione = lds_datastore.getitemstring(ll_idx, 5)
	ll_num_riga_appartenenza = lds_datastore.getitemnumber(ll_idx, 6)
	li_anno_registrazione = lds_datastore.getitemnumber(ll_idx, 7)
	ll_num_registrazione = lds_datastore.getitemnumber(ll_idx, 8)
	ldt_data_pronto = lds_datastore.getitemdatetime(ll_idx, 9)
	ls_cod_cliente = lds_datastore.getitemstring(ll_idx, 10)
	ls_cod_giro_consegna = lds_datastore.getitemstring(ll_idx, 11)
	ls_cod_tipo_ord_ven =  lds_datastore.getitemstring(ll_idx, 12)
	ls_cod_tipo_det_ven =  lds_datastore.getitemstring(ll_idx, 13)
	
	if isnull(ls_cod_prodotto) then continue
	
	
	//se inserito il codice verniciatura trovare cod_verniciatura --------------------------------------
	if ls_cod_verniciatura <>"none" and ls_cod_verniciatura <> "" then
		select cod_verniciatura
		into   :ls_test_verniciatura
		from   comp_det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda and
			    anno_registrazione=:li_anno_registrazione and
			    num_registrazione=:ll_num_registrazione and
			    prog_riga_ord_ven=:ll_prog_riga_ord_ven and
			    cod_verniciatura=:ls_cod_verniciatura;
		
		if sqlca.sqlcode = 100 or ls_cod_verniciatura <> ls_test_verniciatura then continue
	end if
	//------------------------------------------------------------------------------------------------------------
	
	//se inserito il codice colore trovare cod_colore --------------------------------------------------
	if ls_cod_colore <> "" then
		select cod_non_a_magazzino
		into   :ls_test_colore
		from   comp_det_ord_ven
		where  cod_azienda 			=:s_cs_xx.cod_azienda and
			    anno_registrazione	=:li_anno_registrazione and
			    num_registrazione	=:ll_num_registrazione and
			    prog_riga_ord_ven	=:ll_prog_riga_ord_ven and
			    cod_non_a_magazzino =:ls_cod_colore;
		
		if sqlca.sqlcode = 100 or ls_cod_colore <> ls_test_colore then continue
	end if
	//------------------------------------------------------------------------------------------------------------
	
	select flag_tipo_bcl
	into   :ls_tipo_stampa
	from   tab_tipi_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		destroy lds_datastore;
		rollback;
		return -1
	end if
	
	choose case ls_tipo_stampa
		case "A"
			
		case "B","C"
			if ls_cod_tipo_det_ven='ADD' then continue
			
	end choose
	
	yield()
	
	//--------------------------------------------------------------------------------------------------------------------------------------
	ls_text_log =  string(ll_idx)+ " di "+ string(ll_rows)
	if not isnull(ls_cod_giro_consegna) then ls_text_log += " Giro:"+ ls_cod_giro_consegna
	if not isnull(ls_cod_cliente) then ls_text_log += " Cliente:" + ls_cod_cliente
	if not isnull(ls_cod_prodotto) then ls_text_log += " Prodotto:"+ ls_cod_prodotto
	
	st_text.text = "Elaboraz. " + ls_text_log
	hpb_1.stepit( )
	//--------------------------------------------------------------------------------------------------------------------------------------
	
	luo_prod = create uo_produzione
	ll_stato_prod_ordine = luo_prod.uof_stato_prod_det_ord_ven(li_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,ls_messaggio)
	destroy luo_prod;
	
	//yield()

	if ll_stato_prod_ordine = -1 then
		destroy lds_datastore;
		g_mb.messagebox("APICE",ls_messaggio,stopsign!)
		rollback;
		return -1
	end if
	
	if ll_stato_prod_filtro <> -1 then
		if ll_stato_prod_filtro = 3 then
			if ll_stato_prod_ordine <> 0 and ll_stato_prod_ordine <> 2 then
				continue
			end if
		else
			if ll_stato_prod_ordine <> ll_stato_prod_filtro then
				continue
			end if
		end if
	end if
	
	ls_cod_reparti_trovati[] = ls_vuoto[]
	
	//se esistono fasi inutile leggersi la distinta base per trovare i reparti, leggi direttamente da quelle no?
	//infatti in tal caso non me ne frega niente di ordinare per num_sequenza
	if wf_reparti_da_fasi(li_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,ls_cod_reparti_trovati[], fs_errore)<0 then
		//in fs_errore il messaggio
		destroy lds_datastore;
		rollback;
		return -1
	end if
	
	if upperbound(ls_cod_reparti_trovati[]) > 0 then
		//hai recuperato i reparti dalle fasi di produzione, prosegui oltre
	else
		//recupera i reparti dalla distinta o varianti det ord ven
		
		//#########################################################################
		//#########################################################################
		
		setnull(ll_cont_distinte)
	
		select count(*)
		into   :ll_cont_distinte
		from   distinta_padri
		where  cod_azienda  =:s_cs_xx.cod_azienda and
				 cod_prodotto =:ls_cod_prodotto and
				 cod_versione =:ls_cod_versione;
		
		if isnull(ll_cont_distinte) or ll_cont_distinte = 0 then //se il prodotto non ha distinta base allora cerco il reparto in anag_prodotti
		
			select cod_reparto
			into   :ls_cod_reparto
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;	
			
			if sqlca.sqlcode < 0 then 
				destroy lds_datastore;
				fs_errore = "Lettura (per test) distinta_padri.Errore sul DB: " + sqlca.sqlerrtext
				rollback;
				return -1
			end if
	
			if sqlca.sqlcode = 100 or ls_cod_reparto="" or isnull(ls_cod_reparto) then
				fs_errore = "Nell'ordine anno:" + string(li_anno_registrazione) + & 
				 " Num:" + string(ll_num_registrazione) + " riga:"+string(ll_prog_riga_ord_ven)+", è stato inserito il prodotto:" + ls_cod_prodotto + & 
				 " che non ha il reparto assegnato  in anagrafica prodotti. Verificare."
				destroy lds_datastore;
				rollback;
				return -1				
			end if
			
			//Donato 19/12/2011
			//verifica se esiste eccezione
			ls_reparti_letti[] = ls_vuoto[]
			ls_cod_reparti_trovati[] = ls_vuoto[]
			
			if f_reparti_stabilimenti(		li_anno_registrazione, ll_num_registrazione, ls_cod_prodotto, "", &
												ls_cod_reparto, "", "", ls_reparti_letti[], fs_errore)<0 then
				//in fs_errore il messaggio
				destroy lds_datastore;
				rollback;				
				return -1
			end if
			
			
			//merge con l'array by ref principale
			guo_functions.uof_merge_arrays(ls_cod_reparti_trovati[], ls_reparti_letti[])
			
			//dare messaggio all'utente e chiudere il cursore
			if upperbound(ls_cod_reparti_trovati[]) > 0 then
			else
				fs_errore = "Nessun reparto per il prodotto "+ls_cod_prodotto+"  nell'ordine "&
									+string(li_anno_registrazione)+"/"+string(ll_num_registrazione)+"/"+string(ll_prog_riga_ord_ven)
									
				if not g_mb.confirm("Attenzione!", fs_errore, 1) then
					//hai deciso di interrompere l'elaborazione
					destroy lds_datastore;
					rollback;		
					return -1	
				end if
				
				//se arrivi fin qui vuol dire che hai deciso di continuare, escludendo questa riga di ordine
				fs_errore = ""
									
			end if
			
		else
			// se il prodotto ha DB allora gli 'N' reparti vengono trovati tramite una funzione
			// che carica is_cod_reparto[] con i reparti dalla distinta base 
			// per le fasi di lavoro con flag_stampa fase ='S'
	
			
			//Donato 01/02/2012 Spostata funzione globale in user object oggetto
			luo_funzioni = create uo_funzioni_1
			li_risposta = luo_funzioni.uof_trova_reparti(	true, li_anno_registrazione,ll_num_registrazione, ll_prog_riga_ord_ven, &
																		ls_cod_prodotto, ls_cod_versione, ls_cod_reparti_trovati[], ls_errore)
			destroy luo_funzioni
	
			//li_risposta=wf_trova_reparti(li_anno_registrazione,ll_num_registrazione, ll_prog_riga_ord_ven,ls_cod_prodotto,ls_cod_versione,ls_cod_reparti_trovati[],ls_errore)
			//fine modifica ---------------------------------------------
			
			if li_risposta = -1 then
				fs_errore = ls_errore
				destroy lds_datastore;
				rollback;
				return -1				
			end if
			
			if upperbound(ls_cod_reparti_trovati[]) = 0 then		//nel caso non venga trovato alcun reparto scansionando la distinta
				select cod_reparto,								//viene assegnato il reparto della fase di lavoro del  PF quando era 		
						 flag_stampa_fase							//un SL in un'altra distinta
				into   :ls_cod_reparto,
						 :ls_flag_stampa_fase
				from   tes_fasi_lavorazione
				where  cod_azienda = :s_cs_xx.cod_azienda  and
						 cod_prodotto = :ls_cod_prodotto;
			
				if sqlca.sqlcode=100 then
					
					select cod_reparto
					into   :ls_cod_reparti_trovati[1]
					from   anag_prodotti
					where  cod_azienda=:s_cs_xx.cod_azienda and
							 cod_prodotto=:ls_cod_prodotto;	
					
					if sqlca.sqlcode = 100 then
						fs_errore = "Nell'ordine anno:" + string(li_anno_registrazione) + & 
						 " Num:" + string(ll_num_registrazione) + ", è stato inserito il prodotto finito:" + ls_cod_prodotto + & 
						 " che non ha una fase di lavoro con un reparto assegnato e nemmeno in anagrafica prodotti. Verificare."
						
						destroy lds_datastore;
						rollback;
						return -1				
					end if
	
				end if
			end if
		end if	
		//#########################################################################
		//#########################################################################
	end if
	
	if upperbound(ls_cod_reparti_trovati[]) > 0 then
	else
		//puoi arrivare qui solo se al messaggio di avviso hai risposto che volevi continuare
		//quindi salta la riga di ordine
		continue
	end if

	// inserire i dati in tab_carico_reparti
	for ll_t = 1 to upperbound(ls_cod_reparti_trovati[])
		//bisogna scartare i reparti diversi da quello selezionato
		
		//yield()
		
		if isnull(ls_cod_reparti_trovati[ll_t]) then
			li_risposta = f_scrivi_log("Stampa carico reparti: reparto nullo nell'ordine anno/numero registrazione/progressivo " + string(li_anno_registrazione) + "/" + string(ll_num_registrazione) + "/" + string(ll_prog_riga_ord_ven) + " cod prodotto " + ls_cod_prodotto + " pertanto questa riga d'ordine non apparira nella stampa. Controllare.")
			if li_risposta < 0 then 
				fs_errore="Errore durante la scrittura del file log"
				destroy lds_datastore;
				rollback;
				return -1
			end if
			continue
		end if
		
		if pos(ls_cod_reparto_filtro, ls_cod_reparti_trovati[ll_t])>0 then
			//reparto compreso nella lista
		else
			//reparto non compreso, salta
			continue
		end if
		//if not isnull(ls_cod_reparto_filtro) and ls_cod_reparto_filtro<>"none" and ls_cod_reparto_filtro <> "" then 
		//	if ls_cod_reparti_trovati[ll_t] <> ls_cod_reparto_filtro then continue
		//end if
		
		
		// ****  enrico per Daniele riello 14/4/2007
		//yield()
		
		luo_prod = create uo_produzione
		ll_stato_prod_ordine = luo_prod.uof_stato_prod_det_ord_ven_reparto(li_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven,ls_cod_reparti_trovati[ll_t], ref ls_messaggio)
		destroy luo_prod
		
		if ll_stato_prod_ordine = -1 then
			destroy lds_datastore;
			g_mb.messagebox("APICE",ls_messaggio,stopsign!)
			rollback;
			return -1
		end if
		
		if ll_stato_prod_filtro <> -1 then
			if ll_stato_prod_filtro = 3 then
				if ll_stato_prod_ordine <> 0 and ll_stato_prod_ordine <> 2 then
					continue
				end if
			else
				if ll_stato_prod_ordine <> ll_stato_prod_filtro then
					continue
				end if
			end if
		end if
		// **** fine modifica 14/4/2007

		ll_i++
		
		st_text.text = "Inserimento in tabella temporanea carico reparti ... Reparto "+ls_cod_reparti_trovati[ll_t]
		
		insert into tab_carico_reparti
		(cod_azienda,
		 progr,
		 cod_giro_consegna,
		 cod_cliente,
		 cod_prodotto,
		 cod_reparto,
		 anno_registrazione,
		 num_registrazione,
		 prog_riga_ord_ven,
		 data_pronto,
		 quan_ordine,
		 stato_prod,
		 num_riga_appartenenza,
		 cod_utente)
		values
		(:s_cs_xx.cod_azienda,
		 :ll_i,
		 :ls_cod_giro_consegna,
		 :ls_cod_cliente,
		 :ls_cod_prodotto,
		 :ls_cod_reparti_trovati[ll_t],
		 :li_anno_registrazione,
		 :ll_num_registrazione,
		 :ll_prog_riga_ord_ven,
		 :ldt_data_pronto,
		 :ldd_quan_ordine,
		 :ll_stato_prod_ordine,
		 :ll_num_riga_appartenenza,
		 :s_cs_xx.cod_utente);
		
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore sul DB durante inserimento in tab_carico_reparti: " + sqlca.sqlerrtext
			destroy lds_datastore;
			rollback;
			return -1				
		end if
		
		commit;
		
	next		
	// FINE
next

destroy lds_datastore;
commit;

return 0
end function

public function integer wf_controlla_reparti (ref string fs_reparti_selezionati, ref string fs_errore);long ll_index, ll_tot
string ls_selezionato, ls_cod_reparto_filtro

ll_tot = dw_reparti.rowcount()
fs_reparti_selezionati = ""

for ll_index=1 to ll_tot
	ls_selezionato = dw_reparti.getitemstring(ll_index, "selezionato")
	
	if ls_selezionato = "S" then
		ls_cod_reparto_filtro = dw_reparti.getitemstring(ll_index, "cod_reparto")
		
		if fs_reparti_selezionati<>"" then
			//metti anche una virgola
			fs_reparti_selezionati +=","
		end if
		
		fs_reparti_selezionati +="'"+ls_cod_reparto_filtro+"'"
	end if
	
next

if fs_reparti_selezionati="" then
	fs_errore = "Selezionare almeno un reparto di produzione!"
	return -1
end if

return 0
end function

public function integer wf_reparti_da_fasi (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref string fs_cod_reparti_trovati[], ref string fs_errore);string ls_cod_reparto
long ll_index

ll_index = 0

declare cu_fasi_reparto cursor for  
select cod_reparto
from det_ordini_produzione
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :fi_anno_registrazione and
			num_registrazione = :fl_num_registrazione and
			prog_riga_ord_ven = :fl_prog_riga_ord_ven;
open cu_fasi_reparto;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in OPEN cursore 'cu_fasi_reparto'~r~n" + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_fasi_reparto into :ls_cod_reparto;

	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in FETCH cursore 'cu_fasi_reparto'~r~n" + sqlca.sqlerrtext
		close cu_fasi_reparto;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	
	if ls_cod_reparto<>"" and not isnull(ls_cod_reparto) then
		ll_index += 1
		fs_cod_reparti_trovati[ll_index] = ls_cod_reparto
	end if

loop

close cu_fasi_reparto;

return 0
 
 
 
end function

public function integer wf_carica_2 (ref string fs_errore);datetime			ldtm_data_pronto_inizio, ldtm_data_pronto_fine, ldtm_data_elaborazione
string				ls_da_cod_prodotto, ls_a_cod_prodotto, ls_cod_colore, ls_cod_verniciatura, ls_pf, ls_cod_giro_consegna, ls_cod_reparto_filtro,ls_sql, ls_flag_escludi_evase, ls_msg, &
					ls_sql_giri_consegna, ls_sql_count, ls_elenco_colonne, ls_parte_finale, ls_test_colore, ls_tipo_stampa, ls_test_verniciatura, ls_sql_base, ls_sql_base_count, ls_order_by_base, &
					ls_where_data_pronto, ls_des_periodo
long				ll_y,ll_stato_prod_filtro, ll_rows, ll_num_giorno_da_1_10, ll_num_gg_x_foglio, ll_new, ll_periodo
integer			li_stato_prod_ordine
uo_produzione	luo_prod

//variabili per il cursore principale ------------------------------
integer			li_anno_reg_cu
long				ll_num_reg_cu, ll_prog_riga_ord_ven_cu
string				ls_cod_reparto_cu, ls_flag_fine_fase_cu, ls_cod_prodotto_cu, ls_des_prodotto_cu, ls_cod_giro_consegna_cu, ls_flag_blocco_cu, ls_cod_tipo_ord_ven_cu, &
					ls_cod_tipo_det_ven_cu, ls_des_reparto_cu
decimal			ldd_quan_ordine_cu
datetime			ldtm_data_consegna_cu, ldtm_data_registrazione_cu, ldtm_data_pronto_cu
//--------------------------------------------------------------------

ll_num_gg_x_foglio = 10
ll_num_giorno_da_1_10 = 0
ll_periodo = 1
st_text.text = ""
dw_report_2.reset()

ldtm_data_pronto_inizio = dw_ricerca.getitemdatetime(1, "data_inizio_pronto")
ldtm_data_pronto_inizio = datetime(date(ldtm_data_pronto_inizio), 00:00:00)

ldtm_data_pronto_fine = dw_ricerca.getitemdatetime(1, "data_fine_pronto")
ldtm_data_pronto_fine = datetime(date(ldtm_data_pronto_fine), 23:59:59)

if isnull(ldtm_data_pronto_inizio) or year(date(ldtm_data_pronto_inizio))<1950 or isnull(ldtm_data_pronto_fine) or year(date(ldtm_data_pronto_fine))<1950 then
	fs_errore="L'impostazione delle date di pronto è obbligatoria!"
	return -1
end if

if ldtm_data_pronto_fine < ldtm_data_pronto_inizio then
	fs_errore="La data finale è inferiore a quella iniziale!"
	return -1
end if

ls_da_cod_prodotto = dw_ricerca.getitemstring (  1,"cod_prodotto_da")
ls_a_cod_prodotto = dw_ricerca.getitemstring (  1,"cod_prodotto_a")

ls_cod_colore = dw_ricerca.getitemstring (  1,"cod_colore") //
ls_cod_verniciatura = dw_ricerca.getitemstring (  1,"cod_verniciatura")  //**************

ls_pf = dw_ricerca.getitemstring(1, "flag_prodotti_finiti")
if isnull(ls_pf) or len(ls_pf) < 1 then
	ls_pf="T"
end if

ls_cod_giro_consegna = ""
for ll_y = 1 to dw_sel_giri.rowcount()
	if dw_sel_giri.getitemstring(ll_y,"flag_selezione") = "S" then
		if len(ls_cod_giro_consegna) > 0 then ls_cod_giro_consegna = ls_cod_giro_consegna + ", "
		ls_cod_giro_consegna = ls_cod_giro_consegna + "'" + dw_sel_giri.getitemstring(ll_y,"cod_giro_consegna") + "'"
	end if
next

if len(ls_cod_giro_consegna) > 0 then ls_sql_giri_consegna = " and tes_ord_ven.cod_giro_consegna in (" + ls_cod_giro_consegna + ")"  

//controlla se hai selezionato almeno un reparto
if wf_controlla_reparti(ls_cod_reparto_filtro, fs_errore)<0 then
	//in fs_errore il messaggio
	rollback;
	return -1
end if

ll_stato_prod_filtro = dw_ricerca.getitemnumber(  1,"stato_produzione")

//-----------------
ls_elenco_colonne = "det_ordini_produzione.anno_registrazione, det_ordini_produzione.num_registrazione, det_ordini_produzione.prog_riga_ord_ven,"+&
							"det_ordini_produzione.cod_reparto, det_ordini_produzione.flag_fine_fase, anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto,"+&
							"det_ord_ven.quan_ordine, tes_ord_ven.cod_giro_consegna, tes_ord_ven.data_consegna, tes_ord_ven.data_registrazione, "+&
							"tes_ord_ven.data_pronto, tes_ord_ven.flag_blocco, tes_ord_ven.cod_tipo_ord_ven, det_ord_ven.cod_tipo_det_ven, anag_reparti.des_reparto "
//-----------------
ls_order_by_base =  " ORDER BY tes_ord_ven.data_pronto ASC, det_ordini_produzione.cod_reparto ASC "+&
										",det_ordini_produzione.anno_registrazione ASC, det_ordini_produzione.num_registrazione ASC, det_ordini_produzione.prog_riga_ord_ven ASC "

ls_parte_finale = 	"FROM det_ordini_produzione "+&
						"JOIN anag_reparti ON anag_reparti.cod_azienda = det_ordini_produzione.cod_azienda AND "+&
													 "anag_reparti.cod_reparto = det_ordini_produzione.cod_reparto "+&
						"JOIN det_ord_ven det_ord_ven ON det_ord_ven.cod_azienda = det_ordini_produzione.cod_azienda AND "+&
																  "det_ord_ven.anno_registrazione = det_ordini_produzione.anno_registrazione AND "+&
																  "det_ord_ven.num_registrazione = det_ordini_produzione.num_registrazione AND "+&
																  "det_ord_ven.prog_riga_ord_ven = det_ordini_produzione.prog_riga_ord_ven "+&
						"JOIN tes_ord_ven ON det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda AND "+&
												  "det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione AND "+&
												  "det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione "+&
						"JOIN anag_prodotti ON anag_prodotti.cod_azienda = det_ord_ven.cod_azienda AND "+&
													 "anag_prodotti.cod_prodotto = det_ord_ven.cod_prodotto "+&
						"WHERE   tes_ord_ven.cod_azienda='"+s_cs_xx.cod_azienda+"' AND "+&
									"det_ord_ven.num_riga_appartenenza=0 AND "+&
									"det_ord_ven.flag_blocco<>'S' AND tes_ord_ven.flag_blocco<>'S' AND "+&
									"tes_ord_ven.data_pronto>='" + string(date(ldtm_data_pronto_inizio),s_cs_xx.db_funzioni.formato_data) + "' AND "+&
									"tes_ord_ven.data_pronto<='" + string(date(ldtm_data_pronto_fine),s_cs_xx.db_funzioni.formato_data) + "' "
									
									//"det_ordini_produzione.flag_fine_fase='N' AND "+&
									
//-----------------

if len(ls_cod_reparto_filtro)>0 then ls_parte_finale += "AND det_ordini_produzione.cod_reparto IN ("+ls_cod_reparto_filtro+") "

choose case ls_pf
	case "S"
		ls_parte_finale += " and det_ord_ven.cod_prodotto not like '_0%' "

	case "F"
		ls_parte_finale += " and det_ord_ven.cod_prodotto like '_0%' "
		
	case else
		//tutti, quindi non specificare nulla, al più che non sia vuoto
		ls_parte_finale += " and det_ord_ven.cod_prodotto is not null "
		
end choose

if not isnull(ls_da_cod_prodotto) and ls_da_cod_prodotto <> "" then ls_parte_finale += " AND det_ord_ven.cod_prodotto >= '" + ls_da_cod_prodotto + "' "
if not isnull(ls_a_cod_prodotto) and ls_a_cod_prodotto <> "" then ls_parte_finale += " AND det_ord_ven.cod_prodotto <= '" + ls_a_cod_prodotto + "' "

ls_flag_escludi_evase = dw_ricerca.getitemstring(1, "flag_escludi_evase")
if ls_flag_escludi_evase = "S" then
	ls_parte_finale += " AND det_ord_ven.flag_evasione <> 'E'"
end if

//-----------------

//###################################
ls_sql_base = 	"SELECT "+ls_elenco_colonne + " " + ls_parte_finale
ls_sql_base_count = "SELECT count(*) " + ls_parte_finale

//DICHIARAZIONE cursori conteggio e principale -----------------
declare cu_conteggio dynamic cursor for sqlsa;
declare cu_principale dynamic cursor for sqlsa;

ls_sql_count = ls_sql_base_count + ls_where_data_pronto

ls_sql = ls_sql_base + ls_where_data_pronto
ls_sql += ls_order_by_base

prepare sqlsa from :ls_sql_count;
open dynamic cu_conteggio;

if sqlca.sqlcode <> 0 then 
	fs_errore = "Errore apertura cursore conteggio: " + sqlca.sqlerrtext
	return -1
end if
fetch cu_conteggio into :ll_rows;
	
if sqlca.sqlcode < 0 then
	fs_errore = "Errore nella fetch del cursore conteggio: " + sqlca.sqlerrtext
	close cu_conteggio;
	return -1
	
elseif sqlca.sqlcode = 100 or ll_rows=0 or isnull(ll_rows) then
	//no righe da elaborare ...
	close cu_conteggio;
	//fs_errore = "Nessuna riga da elaborare!"
	return 0
	
end if

//Ok ora sai quante righe dovrai elaborare, puoi chiudere il cursore del conteggio
close cu_conteggio;
//fine conteggio -----------

hpb_1.position = 0
hpb_1.maxposition = ll_rows
hpb_1.setstep = 1
ll_new = 0

//elimino precedenti sessioni dell'utente
delete from tab_carico_reparti 
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente;
commit;

//cursore principale -----------------
prepare sqlsa from :ls_sql;
open dynamic cu_principale;

if sqlca.sqlcode <> 0 then 
	fs_errore = "Errore apertura cursore principale: " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_principale into 	:li_anno_reg_cu,
									:ll_num_reg_cu,
									:ll_prog_riga_ord_ven_cu,
									:ls_cod_reparto_cu, 
									:ls_flag_fine_fase_cu, 
									:ls_cod_prodotto_cu, 
									:ls_des_prodotto_cu,
									:ldd_quan_ordine_cu, 
									:ls_cod_giro_consegna_cu, 
									:ldtm_data_consegna_cu, 
									:ldtm_data_registrazione_cu, 
									:ldtm_data_pronto_cu,
									:ls_flag_blocco_cu,
									:ls_cod_tipo_ord_ven_cu,
									:ls_cod_tipo_det_ven_cu,
									:ls_des_reparto_cu;
									
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore nella fetch del cursore principale: " + sqlca.sqlerrtext
		close cu_principale;
		return -1
	elseif sqlca.sqlcode = 100 then
		exit
	end if
	
	hpb_1.stepit( )
	
	//ok, vai avanti
	st_text.text = "Elaborazione "+string(date(ldtm_data_pronto_cu), "dd/mm/yyy")+" - Ordine "+string(li_anno_reg_cu) + "/" + string(ll_num_reg_cu) + "/" +string( ll_prog_riga_ord_ven_cu)+"  - Reparto "+ls_cod_reparto_cu
						
	
	//se inserito il codice verniciatura trovare cod_verniciatura --------------------------------------
	ls_test_verniciatura = ""
	
	if ls_cod_verniciatura <>"none" and ls_cod_verniciatura <> "" then
		select cod_verniciatura
		into   :ls_test_verniciatura
		from   comp_det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda and
				 anno_registrazione=:li_anno_reg_cu and
				 num_registrazione=:ll_num_reg_cu and
				 prog_riga_ord_ven=:ll_prog_riga_ord_ven_cu and
				 cod_verniciatura=:ls_cod_verniciatura;
		
		if sqlca.sqlcode = 100 or ls_cod_verniciatura <> ls_test_verniciatura then continue
	end if
	//------------------------------------------------------------------------------------------------------------
	
	//se inserito il codice colore trovare cod_colore --------------------------------------------------
	ls_test_colore = ""
	
	if ls_cod_colore <> "" and not isnull(ls_cod_colore) then
		select cod_non_a_magazzino
		into   :ls_test_colore
		from   comp_det_ord_ven
		where  cod_azienda 			=:s_cs_xx.cod_azienda and
				 anno_registrazione	=:li_anno_reg_cu and
				 num_registrazione	=:ll_num_reg_cu and
				 prog_riga_ord_ven	=:ll_prog_riga_ord_ven_cu and
				 cod_non_a_magazzino =:ls_cod_colore;
		
		if sqlca.sqlcode = 100 or ls_cod_colore <> ls_test_colore then continue
	end if
	//------------------------------------------------------------------------------------------------------------
	
	ls_tipo_stampa = ""
	
	select flag_tipo_bcl
	into   :ls_tipo_stampa
	from   tab_tipi_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven_cu;
	
	if sqlca.sqlcode < 0 then 
		fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
		close cu_principale;
		return -1
	end if
	
	choose case ls_tipo_stampa
		case "A"
		case "B","C"
			if ls_cod_tipo_det_ven_cu='ADD' then continue
	end choose
	
	//controlla stato produzione solo se espressamente richiesto #############
	if ll_stato_prod_filtro>=0 then
		luo_prod = create uo_produzione
		li_stato_prod_ordine = luo_prod.uof_stato_prod_det_ord_ven( li_anno_reg_cu, ll_num_reg_cu, ll_prog_riga_ord_ven_cu, fs_errore)
		destroy luo_prod;
		
		if li_stato_prod_ordine = -1 then
			//in fs_errore il messaggio
			close cu_principale;
			return -1
		end if
		
		if ll_stato_prod_filtro = 3 then
			if li_stato_prod_ordine <> 0 and li_stato_prod_ordine <> 2 then
				continue
			end if
		else
			if li_stato_prod_ordine <> ll_stato_prod_filtro then
				continue
			end if
		end if
	end if
	//#################################################
	
//		//se arrivi fin qui inserisci la riga ----
//		ll_new = dw_report_2.insertrow(0)
//		
//		dw_report_2.setitem( ll_new, "periodo", ll_periodo)
//		dw_report_2.setitem( ll_new, "des_periodo", ls_des_periodo)
//		dw_report_2.setitem( ll_new, "cod_reparto", ls_cod_reparto_cu)
//		dw_report_2.setitem( ll_new, "des_reparto", ls_des_reparto_cu)
//		dw_report_2.setitem( ll_new, "cod_prodotto", ls_cod_prodotto_cu)
//		dw_report_2.setitem( ll_new, "des_prodotto", ls_des_prodotto_cu)
//		
//		dw_report_2.setitem( ll_new, "qta_"+string(ll_num_giorno_da_1_10), ldd_quan_ordine_cu)
//		
//		dw_report_2.setitem( ll_new, "data_"+string(ll_num_giorno_da_1_10), "")
	
	ll_new += 1
	
//	st_text.text = "Inserimento in tabella temporanea carico reparti ... Ordine "+&
//				string(li_anno_reg_cu) + "/" + string(ll_num_reg_cu) + "/" +string( ll_prog_riga_ord_ven_cu)+"  - Reparto "+ls_cod_reparto_cu
	
	insert into tab_carico_reparti
		(cod_azienda,
		 progr,
		 cod_giro_consegna,
		 cod_cliente,
		 cod_prodotto,
		 cod_reparto,
		 anno_registrazione,
		 num_registrazione,
		 prog_riga_ord_ven,
		 data_pronto,
		 quan_ordine,
		 stato_prod,
		 num_riga_appartenenza,
		 cod_utente)
	values
		(:s_cs_xx.cod_azienda,
		 :ll_new,
		 null,
		 null,
		 :ls_cod_prodotto_cu,
		 :ls_cod_reparto_cu,
		 :li_anno_reg_cu,
		 :ll_num_reg_cu,
		 :ll_prog_riga_ord_ven_cu,
		 :ldtm_data_pronto_cu,
		 :ldd_quan_ordine_cu,
		 :li_stato_prod_ordine,
		 0,
		 :s_cs_xx.cod_utente);
	
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore sul DB durante inserimento in tab_carico_reparti: " + sqlca.sqlerrtext
		close cu_principale;
		return -1				
	end if
	
loop

commit;

close cu_principale;
//fine cursore principale -----------

return 1
end function

public function integer wf_report_2 (ref string fs_errore);long				ll_num_gg_x_foglio, ll_num_giorno_da_1_10, ll_periodo,ll_rows, ll_new, ll_index
datetime			ldtm_data_pronto_inizio, ldtm_data_pronto_fine, ldtm_data_elaborazione, ldtm_data_inizio_periodo
date				ldt_date_periodo[]
string				ls_des_periodo, ls_msg, ls_sql_count
datastore		lds_count

//variabili per il cursore principale ------------------------------
string				ls_cod_reparto_cu, ls_cod_prodotto_cu, ls_des_prodotto_cu,	ls_des_reparto_cu
decimal			ldd_quan_ordine_cu
datetime			ldtm_data_pronto_cu
//--------------------------------------------------------------------

ll_num_gg_x_foglio = 10
ll_num_giorno_da_1_10 = 0
ll_periodo = 1
st_text.text = ""
dw_report_2.reset()

ldtm_data_pronto_inizio = dw_ricerca.getitemdatetime(1, "data_inizio_pronto")
ldtm_data_pronto_inizio = datetime(date(ldtm_data_pronto_inizio), 00:00:00)

ldtm_data_pronto_fine = dw_ricerca.getitemdatetime(1, "data_fine_pronto")
ldtm_data_pronto_fine = datetime(date(ldtm_data_pronto_fine), 23:59:59)

ls_sql_count = 	"select count(*) "+&
					"from tab_carico_reparti "+&
					"where 	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
								"cod_utente='"+s_cs_xx.cod_utente + "' "+&
					"group by data_pronto, cod_reparto, cod_prodotto"

ll_rows = guo_functions.uof_crea_datastore( lds_count, ls_sql_count, fs_errore)

if ll_rows<0 then
	fs_errore = "Errore in conteggio righe da elaborare: " + sqlca.sqlerrtext
	return -1
	
elseif isnull(ll_rows) or ll_rows=0 then
	fs_errore = "Non è stato possibile recuperare il numero di righe da elaborare!"
	return -1
	
end if


//qui dichiaro il cursore che elaborerà i dati giorno per giorno -----------------------
declare cu_elaborazione cursor for  
	select data_pronto, cod_reparto, cod_prodotto, sum(quan_ordine)
	from tab_carico_reparti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_utente=:s_cs_xx.cod_utente
	group by data_pronto, cod_reparto, cod_prodotto
	order by data_pronto, cod_reparto, cod_prodotto;
//------------------------------------------------------------------------------------------------

open cu_elaborazione;
if sqlca.sqlcode < 0 then
	fs_errore = "Errore in OPEN cursore cu_elaborazione: " + sqlca.sqlerrtext
	return -1
end if

ldtm_data_elaborazione = datetime(date(1950, 1, 1), 00:00:00)

hpb_1.position = 0
hpb_1.maxposition = ll_rows
hpb_1.setstep = 1

//preparo la struttura tabella del report, esploso orizzontalmente per data pronto
do while true
	
	fetch cu_elaborazione into  :ldtm_data_pronto_cu,
										:ls_cod_reparto_cu,
										:ls_cod_prodotto_cu,
										:ldd_quan_ordine_cu;
	if sqlca.sqlcode < 0 then
		fs_errore = "Errore in FETCH cursore cu_elaborazione: " + sqlca.sqlerrtext
		close cu_elaborazione;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit

	//procedi
	
	if ldtm_data_pronto_cu <> ldtm_data_elaborazione then
		//sto elaborando una data successiva a quella precedente, siccome ho ordinato i dati per data pronto
		//e devo mettere nn più di 10 giorni orizzontalmente incremento la variabile del gruppo
		ll_num_giorno_da_1_10 += 1
		
		if ll_num_giorno_da_1_10 > ll_num_gg_x_foglio then
			ll_periodo += 1										//incremento di 1       la variabile periodo del gruppo 10 GG
			ll_num_giorno_da_1_10 = 1						//reset al valore 1 della variabile che conta fino a 10 GG
		end if
		
		if ll_num_giorno_da_1_10=1 then
			//cambio data_inizio periodo
			ldtm_data_inizio_periodo = ldtm_data_pronto_cu
			
			for ll_index=1 to ll_num_gg_x_foglio
				ldt_date_periodo[ll_index] = relativedate(date(ldtm_data_inizio_periodo), ll_index -1)
			next
			
			ls_des_periodo = "(" + string(ll_periodo) +") dal "+string(ldt_date_periodo[1], "dd/mm/yy")+" al "+string(ldt_date_periodo[ll_num_gg_x_foglio], "dd/mm/yy")
		end if
		
		//salvo la data che sto elaborando
		ldtm_data_elaborazione = ldtm_data_pronto_cu
	end if
	
	ls_msg = "Elaborazione Giorno " + string(date(ldtm_data_elaborazione), "dd/mm/yyyy")
	st_text.text = ls_msg + " - Reparto: "+ls_cod_reparto_cu+" -  Prodotto: "+ls_cod_prodotto_cu
	
	
	//#####################################################################
	//se arrivi fin qui inserisci la riga ----
	ll_new = dw_report_2.insertrow(0)
	
	dw_report_2.setitem( ll_new, "periodo", ll_periodo)
	dw_report_2.setitem( ll_new, "des_periodo", ls_des_periodo)
	dw_report_2.setitem( ll_new, "cod_reparto", ls_cod_reparto_cu)
	dw_report_2.setitem( ll_new, "cod_prodotto", ls_cod_prodotto_cu)
	
	select des_reparto
	into :ls_des_reparto_cu
	from anag_reparti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_reparto=:ls_cod_reparto_cu;
				
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in lettura descrizione reparto: " + sqlca.sqlerrtext
		close cu_elaborazione;
		return -1
	end if
	
	dw_report_2.setitem( ll_new, "des_reparto", ls_des_reparto_cu)
	
	select des_prodotto
	into :ls_des_prodotto_cu
	from anag_prodotti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_prodotto=:ls_cod_prodotto_cu;
				
	if sqlca.sqlcode<0 then
		fs_errore = "Errore in lettura descrizione prodotto: " + sqlca.sqlerrtext
		close cu_elaborazione;
		return -1
	end if
	
	dw_report_2.setitem( ll_new, "des_prodotto", ls_des_prodotto_cu)
	

	
	dw_report_2.setitem( ll_new, "qta_"+string(ll_num_giorno_da_1_10), ldd_quan_ordine_cu)
	dw_report_2.setitem( ll_new, "data_"+string(ll_num_giorno_da_1_10), ldtm_data_pronto_cu)
	
	for ll_index=1 to ll_num_gg_x_foglio
		dw_report_2.setitem( ll_new, "data_"+string(ll_index), ldt_date_periodo[ll_index])
	next
	
	//#####################################################################

loop

close cu_elaborazione;

dw_report_2.sort()
dw_report_2.groupcalc()

dw_report_2.print(true, true)

return 1
end function

public function integer wf_report_3 (ref string fs_errore);
window_open( w_report_carico_reparti_preview, -1)

return 1
end function

on w_report_carico_reparti.create
int iCurrent
call super::create
this.dw_report_2=create dw_report_2
this.hpb_1=create hpb_1
this.dw_reparti=create dw_reparti
this.dw_ricerca=create dw_ricerca
this.st_text=create st_text
this.dw_sel_giri=create dw_sel_giri
this.st_9=create st_9
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.r_1=create r_1
this.st_4=create st_4
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_2
this.Control[iCurrent+2]=this.hpb_1
this.Control[iCurrent+3]=this.dw_reparti
this.Control[iCurrent+4]=this.dw_ricerca
this.Control[iCurrent+5]=this.st_text
this.Control[iCurrent+6]=this.dw_sel_giri
this.Control[iCurrent+7]=this.st_9
this.Control[iCurrent+8]=this.cb_annulla
this.Control[iCurrent+9]=this.cb_report
this.Control[iCurrent+10]=this.r_1
this.Control[iCurrent+11]=this.st_4
this.Control[iCurrent+12]=this.dw_report
end on

on w_report_carico_reparti.destroy
call super::destroy
destroy(this.dw_report_2)
destroy(this.hpb_1)
destroy(this.dw_reparti)
destroy(this.dw_ricerca)
destroy(this.st_text)
destroy(this.dw_sel_giri)
destroy(this.st_9)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.r_1)
destroy(this.st_4)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;

//f_PO_LoadDDLB(ddlb_reparto, SQLCA,"anag_reparti" , "cod_reparto", &	
//"des_reparto", "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))","" )

//f_PO_LoadDDLB(ddlb_giro_consegna, SQLCA,"tes_giri_consegne" , "cod_giro_consegna", &	
//"des_giro_consegna", "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))","" )

//f_PO_LoadDDLB(ddlb_verniciatura, SQLCA,"tab_verniciatura" , "cod_verniciatura", &	
//"cod_verniciatura", "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))","" )

f_po_loaddddw_dw(dw_ricerca, &
                 "cod_verniciatura", &
                 sqlca, &
                 "tab_verniciatura", &
                 "cod_verniciatura", &
                 "cod_verniciatura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  

f_po_loaddddw_dw(dw_ricerca, &
                 "cod_deposito_produzione", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and (cod_deposito='100' or cod_deposito='200' or cod_deposito='300' or cod_deposito='400') ")



return 0
end event

event pc_setwindow;call super::pc_setwindow;string ls_null

dw_sel_giri.settransobject(sqlca)
dw_reparti.settransobject(sqlca)

dw_report.ib_dw_report = true
setnull(ls_null)

dw_sel_giri.retrieve(s_cs_xx.cod_azienda)
dw_reparti.retrieve(ls_null, s_cs_xx.cod_azienda)
end event

type dw_report_2 from uo_cs_xx_dw within w_report_carico_reparti
boolean visible = false
integer x = 41
integer y = 988
integer width = 507
integer height = 140
integer taborder = 150
string dataobject = "d_report_carico_reparti_2"
end type

type hpb_1 from hprogressbar within w_report_carico_reparti
integer x = 32
integer y = 2584
integer width = 2482
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 1
end type

type dw_reparti from datawindow within w_report_carico_reparti
integer x = 119
integer y = 604
integer width = 2377
integer height = 532
integer taborder = 80
string title = "none"
string dataobject = "d_carico_reparti_sel_reparti"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_ricerca from u_dw_search within w_report_carico_reparti
integer x = 41
integer y = 36
integer width = 2510
integer height = 1660
integer taborder = 80
string dataobject = "d_carico_reparti_sel"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_cod_deposito

if row>0 then
else
	return
end if

choose case dwo.name
	case "cod_deposito_produzione"
		
		if data="" then
			setnull(ls_cod_deposito)
		else
			ls_cod_deposito = data
		end if
		dw_reparti.retrieve(ls_cod_deposito, s_cs_xx.cod_azienda)
		
end choose
end event

event buttonclicked;call super::buttonclicked;string ls_seleziona = "N"
long ll_index, ll_tot

if row>0 then
else
	return
end if

choose case dwo.name
	case "b_tutti", "b_nessuno"
		ll_tot = dw_reparti.rowcount()
		
		if dwo.name="b_tutti" then ls_seleziona="S"
		
		for ll_index=1 to ll_tot
			dw_reparti.setitem(ll_index, "selezionato", ls_seleziona)
		next
		
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto_da")
		
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto_a")
	
	
end choose
end event

type st_text from statictext within w_report_carico_reparti
integer x = 32
integer y = 2512
integer width = 2482
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type dw_sel_giri from datawindow within w_report_carico_reparti
integer x = 101
integer y = 1832
integer width = 2377
integer height = 512
integer taborder = 150
string title = "none"
string dataobject = "d_carico_reparti_sel_giri"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event itemchanged;resetupdate()
end event

type st_9 from statictext within w_report_carico_reparti
integer x = 32
integer y = 2424
integer width = 1627
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "IMPOSTA ORIENTAM. ORIZZONT. PRIMA DI STAMPARE!"
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_report_carico_reparti
integer x = 1710
integer y = 2412
integer width = 430
integer height = 80
integer taborder = 150
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla Filtri"
end type

event clicked;datetime ldt_null
string ls_null
long ll_index, ll_tot

setnull(ls_null)
setnull(ldt_null)

dw_ricerca.setitem(1,"data_inizio_pronto", ldt_null)
dw_ricerca.setitem(1,"data_fine_pronto", ldt_null)
dw_ricerca.setitem(1,"cod_deposito_produzione", ls_null)

//reparti
ll_tot = dw_reparti.rowcount()
for ll_index=1 to ll_tot
	dw_reparti.setitem(ll_index, "selezionato", "N")
next

dw_ricerca.setitem(1,"flag_prodotti_finiti", "T")
dw_ricerca.setitem(1,"cod_prodotto_da", ls_null)
dw_ricerca.setitem(1,"cod_prodotto_a", ls_null)
dw_ricerca.setitem(1,"cod_colore", ls_null)
dw_ricerca.setitem(1,"cod_verniciatura", ls_null)
dw_ricerca.setitem(1,"stato_produzione", "-1")
dw_ricerca.setitem(1,"flag_escludi_evase", "N")
dw_ricerca.setitem(1,"flag_esegui_somme", "N")
dw_ricerca.setitem(1,"tipo_ordinamento", "P")


end event

type cb_report from commandbutton within w_report_carico_reparti
integer x = 2149
integer y = 2412
integer width = 366
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;integer li_risposta, li_tipo_report
string ls_errore
long ll_count

setpointer(hourglass!)

dw_ricerca.accepttext()
dw_reparti.accepttext()

hpb_1.position = 0

li_tipo_report = dw_ricerca.getitemnumber(1, "flag_tipo_report")

	if li_tipo_report = 1 then
	
	li_risposta = wf_carica(ls_errore)
	
	if li_risposta < 0 then
		g_mb.error("Apice",ls_errore)
		return
	else
		if g_mb.messagebox("Apice","Carico tabella ok. Confermi l'esecuzione della stampa?",question!,yesno!,1) = 2 then 
			setpointer(arrow!)
			return
		end if
	end if
	
	hpb_1.position = 0
	li_risposta = wf_report(ls_errore)

//--------------------------------------------------------------------------
elseif li_tipo_report = 2 then
	ll_count = wf_carica_2(ls_errore)
	
	if ll_count < 0 then
		g_mb.error("Apice",ls_errore)
		rollback;
		return
		
	elseif ll_count=0 then
		g_mb.show("Apice","Nessuna riga da visualizzare!")
		rollback;
		return
		
	else
		if not g_mb.confirm("Apice","Carico tabella ok. Confermi l'esecuzione della stampa?") then 
			setpointer(arrow!)
			return
		end if
	end if
	
	hpb_1.position = 0
	li_risposta = wf_report_3(ls_errore)
	setpointer(arrow!)
	hpb_1.position = 0
	return
	
end if

if li_risposta < 0 then
	g_mb.error("Apice",ls_errore)
	return
end if

setpointer(arrow!)
hpb_1.position = 0
g_mb.show("Apice","Stampa eseguita!")

end event

type r_1 from rectangle within w_report_carico_reparti
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 87
integer y = 1752
integer width = 2423
integer height = 636
end type

type st_4 from statictext within w_report_carico_reparti
integer x = 183
integer y = 1720
integer width = 498
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Giro Consegna"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_report from uo_cs_xx_dw within w_report_carico_reparti
boolean visible = false
integer x = 27
integer y = 852
integer width = 507
integer height = 128
integer taborder = 140
boolean bringtotop = true
string title = "none"
string dataobject = "d_report_carico_reparti"
boolean border = false
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

