﻿$PBExportHeader$w_report_carico_reparti_preview.srw
forward
global type w_report_carico_reparti_preview from w_cs_xx_principale
end type
type st_1 from statictext within w_report_carico_reparti_preview
end type
type htb_1 from htrackbar within w_report_carico_reparti_preview
end type
type dw_report from uo_cs_xx_dw within w_report_carico_reparti_preview
end type
end forward

global type w_report_carico_reparti_preview from w_cs_xx_principale
integer width = 4713
integer height = 2304
string title = "Stampa Carico Reparti"
boolean minbox = false
boolean maxbox = false
st_1 st_1
htb_1 htb_1
dw_report dw_report
end type
global w_report_carico_reparti_preview w_report_carico_reparti_preview

on w_report_carico_reparti_preview.create
int iCurrent
call super::create
this.st_1=create st_1
this.htb_1=create htb_1
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.htb_1
this.Control[iCurrent+3]=this.dw_report
end on

on w_report_carico_reparti_preview.destroy
call super::destroy
destroy(this.st_1)
destroy(this.htb_1)
destroy(this.dw_report)
end on

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report=true

set_w_options(c_CloseNoSave + c_noenablepopup + c_NoResizeWin)
save_on_close(c_socnosave)


dw_report.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
						   c_disablecc, &
						   c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)

iuo_dw_main = dw_report

dw_report.object.datawindow.print.preview = "yes"

this.width = w_cs_xx_mdi.mdi_1.width - 60
this.height = w_cs_xx_mdi.mdi_1.height - 60
end event

event resize;call super::resize;dw_report.width = this.width - 100
dw_report.height = this.height - 300
end event

type st_1 from statictext within w_report_carico_reparti_preview
integer x = 2720
integer y = 36
integer width = 402
integer height = 64
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "100%"
boolean focusrectangle = false
end type

type htb_1 from htrackbar within w_report_carico_reparti_preview
integer x = 32
integer y = 4
integer width = 2638
integer height = 116
integer minposition = 1
integer maxposition = 200
integer position = 100
integer tickfrequency = 5
htickmarks tickmarks = hticksontop!
end type

event moved;dw_report.object.datawindow.zoom = scrollpos

st_1.text = string(scrollpos)
end event

type dw_report from uo_cs_xx_dw within w_report_carico_reparti_preview
integer x = 23
integer y = 136
integer width = 4617
integer height = 2044
integer taborder = 10
string dataobject = "d_report_carico_reparti_3"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;

dw_report.retrieve( s_cs_xx.cod_azienda, s_cs_xx.cod_utente )
end event

event doubleclicked;return
end event

