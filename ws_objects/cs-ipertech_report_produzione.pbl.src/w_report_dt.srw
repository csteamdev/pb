﻿$PBExportHeader$w_report_dt.srw
$PBExportComments$Window report di controllo per distinta di taglio
forward
global type w_report_dt from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_dt
end type
type cb_report from commandbutton within w_report_dt
end type
type dw_ricerca from uo_cs_xx_dw within w_report_dt
end type
end forward

global type w_report_dt from w_cs_xx_principale
integer width = 3721
integer height = 2232
string title = "Report di controllo per distinta di taglio"
dw_report dw_report
cb_report cb_report
dw_ricerca dw_ricerca
end type
global w_report_dt w_report_dt

forward prototypes
public function integer wf_report (string fs_cod_prodotto, string fs_cod_versione, long fl_num_livello_cor, ref string fs_errore)
end prototypes

public function integer wf_report (string fs_cod_prodotto, string fs_cod_versione, long fl_num_livello_cor, ref string fs_errore);string  ls_errore, ls_test_prodotto_f, ls_des_prodotto,ls_flag_materia_prima,ls_formula_quantita,ls_formula_misura,&
		  ls_cod_prodotto_figlio,ls_cod_prodotto_padre,ls_des_distinta_taglio,ls_um_quantita,ls_um_misura,ls_tutti_comandi,&
		  ls_cod_comando,ls_des_comando
integer li_num_priorita,li_risposta
long    ll_i,ll_num_figli,ll_num_sequenza,ll_t,ll_num_righe,ll_j,ll_progressivo


setpointer(hourglass!)

datastore lds_righe_distinta

ll_num_figli = 1

lds_righe_distinta = Create DataStore

lds_righe_distinta.DataObject = "d_data_store_distinta"

lds_righe_distinta.SetTransObject(sqlca)

ll_num_righe = lds_righe_distinta.Retrieve(s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione)
	
for ll_num_figli=1 to ll_num_righe

	ls_cod_prodotto_figlio=lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_figlio")
	ll_num_sequenza = lds_righe_distinta.getitemnumber(ll_num_figli,"num_sequenza")
	ls_cod_prodotto_padre = lds_righe_distinta.getitemstring(ll_num_figli,"cod_prodotto_padre")
	ls_flag_materia_prima = lds_righe_distinta.getitemstring(ll_num_figli,"flag_materia_prima")

   select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto_figlio;
	
	if sqlca.sqlcode < 0 then
		messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ls_des_prodotto=trim(ls_des_prodotto)
	
	declare r_dt cursor for 
	select progressivo,
			 des_distinta_taglio,
			 um_quantita,
			 formula_quantita,
			 um_misura,
			 formula_misura,
			 tutti_comandi
	from   tab_distinte_taglio
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto_padre=:ls_cod_prodotto_padre
	and    num_sequenza=:ll_num_sequenza
	and    cod_prodotto_figlio=:ls_cod_prodotto_figlio
	and    cod_versione=:fs_cod_versione;

	open r_dt;
	
	do while 1=1 
		fetch r_dt 
		into :ll_progressivo,
			  :ls_des_distinta_taglio,
			  :ls_um_quantita,
			  :ls_formula_quantita,
			  :ls_um_misura,
			  :ls_formula_misura,
			  :ls_tutti_comandi;

		if sqlca.sqlcode < 0 then
			messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_dt;
			return -1
		end if
		
		if sqlca.sqlcode=100 then exit

	
		ll_j=dw_report.insertrow(0)
		ll_j=dw_report.insertrow(0)
		dw_report.setitem(ll_j,"vis_1",1)
		dw_report.setitem(ll_j,"vis_2",0)
		dw_report.setitem(ll_j,"elenco","Elenco comandi")
			
		declare r_comandi cursor for
		select cod_comando
		from   distinte_taglio_comandi
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto_padre=:ls_cod_prodotto_padre
		and    num_sequenza=:ll_num_sequenza
		and    cod_prodotto_figlio=:ls_cod_prodotto_figlio
		and    cod_versione=:fs_cod_versione
		and    progressivo=:ll_progressivo;    
		
		open r_comandi;
	
		do while 1=1 
			fetch r_comandi 
			into :ls_cod_comando;
		
			if sqlca.sqlcode < 0 then
				messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_comandi;
				close r_dt;
				return -1
			end if
		
			if sqlca.sqlcode=100 then exit
	
			select des_prodotto
			into   :ls_des_comando
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_comando;
			
			if sqlca.sqlcode < 0 then
				messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close r_comandi;
				close r_dt;
				return -1
			end if
			
		
			ll_j = dw_report.insertrow(0)
			dw_report.setitem(ll_j,"vis_1",1)
			dw_report.setitem(ll_j,"vis_2",0)
			dw_report.setitem(ll_j,"cod_comando",ls_cod_comando)
			dw_report.setitem(ll_j,"des_comando",ls_des_comando)
			
		loop
		
		close r_comandi;

		ll_j = dw_report.insertrow(0)
		ll_j = dw_report.insertrow(0)
		dw_report.setitem(ll_j,"vis_1",0)
		dw_report.setitem(ll_j,"vis_2",1)
		dw_report.setitem(ll_j,"cod_prodotto_distinta",ls_cod_prodotto_figlio)
		dw_report.setitem(ll_j,"des_prodotto_distinta",ls_des_prodotto)
		dw_report.setitem(ll_j,"des_distinta",ls_des_distinta_taglio)
		dw_report.setitem(ll_j,"um_quantita",ls_um_quantita)
		dw_report.setitem(ll_j,"formula_quantita",ls_formula_quantita)
		dw_report.setitem(ll_j,"um_taglio",ls_um_misura)
		dw_report.setitem(ll_j,"formula_taglio",ls_formula_misura)
	loop

	close r_dt;
		 		
	if ls_flag_materia_prima = 'N' or isnull(ls_flag_materia_prima) then
		select cod_prodotto_figlio 
		into   :ls_test_prodotto_f
		from   distinta
		where  cod_azienda=:s_cs_xx.cod_azienda
		and	 cod_prodotto_padre=:ls_cod_prodotto_figlio;

		if isnull(ls_test_prodotto_f) or ls_test_prodotto_f = "" then
			continue
		else
			li_risposta=wf_report(ls_cod_prodotto_figlio, fs_cod_versione,fl_num_livello_cor + 1,fs_errore)

		end if
		
	end if

next

destroy(lds_righe_distinta)

return 0
end function

on w_report_dt.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.cb_report=create cb_report
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.cb_report
this.Control[iCurrent+3]=this.dw_ricerca
end on

on w_report_dt.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.cb_report)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;string ls_cod_prodotto,ls_cod_versione,ls_errore,ls_des_prodotto
integer li_risposta

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_ricerca.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
iuo_dw_main = dw_report


if isnull(ls_cod_prodotto) or ls_cod_prodotto ="" then return 

dw_report.reset()

li_risposta = wf_report(ls_cod_prodotto, ls_cod_versione, 1, ls_errore )

if li_risposta < 0 then return

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto;

if sqlca.sqlcode < 0 then 
	messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return
end if

dw_report.Modify("cod_prodotto.text='"+ ls_cod_prodotto + "'")
dw_report.Modify("des_prodotto.text='"+ ls_des_prodotto + "'")
dw_report.Modify("cod_versione.text='"+ ls_cod_versione + "'")

dw_report.Object.DataWindow.Print.preview= 'Yes'
end event

type dw_report from uo_cs_xx_dw within w_report_dt
integer x = 14
integer y = 360
integer width = 3648
integer height = 1740
integer taborder = 60
string dataobject = "d_report_distinta_taglio"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type cb_report from commandbutton within w_report_dt
integer x = 2345
integer y = 220
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_prodotto,ls_cod_versione,ls_errore,ls_des_prodotto
integer li_risposta

ls_cod_prodotto = dw_ricerca.getitemstring(dw_ricerca.getrow(),"rs_cod_prodotto")
ls_cod_versione = dw_ricerca.getitemstring(dw_ricerca.getrow(),"cod_versione")

if isnull(ls_cod_prodotto) or ls_cod_prodotto ="" then return 

dw_report.reset()

li_risposta = wf_report(ls_cod_prodotto, ls_cod_versione, 1, ls_errore )

if li_risposta < 0 then return

select des_prodotto
into   :ls_des_prodotto
from   anag_prodotti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto;

if sqlca.sqlcode < 0 then 
	messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext, stopsign!)
	return
end if

dw_report.Modify("cod_prodotto.text='"+ ls_cod_prodotto + "'")
dw_report.Modify("des_prodotto.text='"+ ls_des_prodotto + "'")
dw_report.Modify("cod_versione.text='"+ ls_cod_versione + "'")

dw_report.Object.DataWindow.Print.preview= 'Yes'
end event

type dw_ricerca from uo_cs_xx_dw within w_report_dt
integer x = 18
integer y = 16
integer width = 2784
integer height = 328
integer taborder = 40
string dataobject = "d_distinta_padri_cerca"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_cod_versione
long ll_test

if isvalid (dwo) then
	choose case dwo.name
		case "rs_cod_prodotto"
			
			if isnull(data) or data = "" then
				dw_ricerca.object.cod_versione.protect = '1'
				dw_ricerca.object.cod_versione.color = string(RGB(128,128,128))
			else
				dw_ricerca.object.cod_versione.protect = '0'
				dw_ricerca.object.cod_versione.color = string(RGB(255,255,255))
				
				f_PO_LoadDDDW_DW(this,"cod_versione",sqlca,&
							  "distinta_padri","cod_versione","'VERSIONE ' "+guo_functions.uof_concat_op()+" cod_versione",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + data + "'")
							  
			end if
			
	end choose
end if
end event

