﻿$PBExportHeader$w_report_mrp_tessuti.srw
$PBExportComments$Report Inventario del Magazino
forward
global type w_report_mrp_tessuti from w_cs_xx_principale
end type
type cb_gen_rda from commandbutton within w_report_mrp_tessuti
end type
type st_2 from statictext within w_report_mrp_tessuti
end type
type cb_ricerca_tessuti from commandbutton within w_report_mrp_tessuti
end type
type cb_report from commandbutton within w_report_mrp_tessuti
end type
type sle_1 from singlelineedit within w_report_mrp_tessuti
end type
type dw_selezione from uo_cs_xx_dw within w_report_mrp_tessuti
end type
type dw_folder from u_folder within w_report_mrp_tessuti
end type
type dw_report from uo_cs_xx_dw within w_report_mrp_tessuti
end type
end forward

global type w_report_mrp_tessuti from w_cs_xx_principale
integer x = 73
integer y = 300
integer width = 3703
integer height = 1924
string title = "MRP Magazzino Tessuti Progettotenda"
cb_gen_rda cb_gen_rda
st_2 st_2
cb_ricerca_tessuti cb_ricerca_tessuti
cb_report cb_report
sle_1 sle_1
dw_selezione dw_selezione
dw_folder dw_folder
dw_report dw_report
end type
global w_report_mrp_tessuti w_report_mrp_tessuti

type variables
boolean ib_interrupt,ib_debug=false
end variables

on w_report_mrp_tessuti.create
int iCurrent
call super::create
this.cb_gen_rda=create cb_gen_rda
this.st_2=create st_2
this.cb_ricerca_tessuti=create cb_ricerca_tessuti
this.cb_report=create cb_report
this.sle_1=create sle_1
this.dw_selezione=create dw_selezione
this.dw_folder=create dw_folder
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_gen_rda
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.cb_ricerca_tessuti
this.Control[iCurrent+4]=this.cb_report
this.Control[iCurrent+5]=this.sle_1
this.Control[iCurrent+6]=this.dw_selezione
this.Control[iCurrent+7]=this.dw_folder
this.Control[iCurrent+8]=this.dw_report
end on

on w_report_mrp_tessuti.destroy
call super::destroy
destroy(this.cb_gen_rda)
destroy(this.st_2)
destroy(this.cb_ricerca_tessuti)
destroy(this.cb_report)
destroy(this.sle_1)
destroy(this.dw_selezione)
destroy(this.dw_folder)
destroy(this.dw_report)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_selezione, &
					"cod_reparto", &
					sqlca, &
					"anag_reparti", &
					"cod_reparto", &
					"des_reparto", &
					"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW(dw_selezione, &
					"cod_tessuto", &
					sqlca,&
					"anag_prodotti", &
					"cod_prodotto", &
					"des_prodotto",&
					"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto in ( SELECT cod_tessuto from tab_tessuti where cod_azienda = '" + s_cs_xx.cod_azienda + "')" )

f_PO_LoadDDDW_DW(dw_selezione, &
					"cod_tipo_campionario", &
					sqlca,&
					"tab_tipi_campionari", &
					"cod_tipo_campionario", &
					"des_tipo_campionario",&
					"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]

dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
iuo_dw_main = dw_report

l_objects[1] = dw_report
l_objects[2] = cb_gen_rda
dw_folder.fu_assigntab(2, "Report", l_objects[])
l_objects[1] = dw_selezione
l_objects[2] = cb_report
l_objects[3] = sle_1
l_objects[4] = cb_ricerca_tessuti
l_objects[5] = st_2
dw_folder.fu_assigntab(1, "Selezione", l_objects[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

resize(w_cs_xx_mdi.mdi_1.width - 50,w_cs_xx_mdi.mdi_1.height - 50)

end event

event resize;call super::resize;dw_folder.width = newwidth - 20
dw_folder.height = newheight - 20
dw_report.width = newwidth - 120
dw_report.height = newheight - 270
cb_gen_rda.x = newwidth - 416
cb_gen_rda.y = newheight - 124
end event

type cb_gen_rda from commandbutton within w_report_mrp_tessuti
integer x = 3250
integer y = 1696
integer width = 379
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Gen. R.D.A."
end type

event clicked;string ls_cod_prodotto, ls_cod_versione,ls_cod_tipo_politica_riordino, ls_errore, ls_note, ls_rda_generate,&
       ls_flag_gen_commessa, ls_des_prodotto, ls_flag_tipo_riga, ls_null, ls_cod_fornitore,ls_des_specifica
long ll_i, ll_anno_commessa,ll_num_commessa, ll_ret, ll_anno_reg_rda, ll_num_reg_rda, ll_null
dec{4} ldd_quan_rda, ld_alt_tessuto
datetime ldt_data_consegna
uo_generazione_documenti luo_gen_doc
										

dw_report.accepttext( )
setnull(ll_null)

if g_mb.messagebox("SEP","Procedo con la creazione RDA dei prodotti indicati com mancanti ?",Question!,YesNo!,2) = 2 then return

luo_gen_doc = create uo_generazione_documenti 

setnull(ll_anno_reg_rda)
setnull(ll_num_reg_rda)
setnull(ll_anno_commessa)
setnull(ll_num_commessa)
setnull(ls_null)
ls_rda_generate = ""

for ll_i = 1 to dw_report.rowcount()
	
	if dw_report.getitemstring(ll_i, "flag_gen_rda") <> "S" or isnull( dw_report.getitemstring(ll_i, "flag_gen_rda") ) then continue
	
	ls_cod_prodotto  = dw_report.getitemstring(ll_i, "cod_tessuto")
	ls_des_prodotto  = dw_report.getitemstring(ll_i, "des_tessuto")
	ls_note  		  = dw_report.getitemstring(ll_i, "note")
	ls_cod_fornitore = dw_report.getitemstring(ll_i, "cod_fornitore")
	ls_des_specifica = dw_report.getitemstring(ll_i, "cod_colore_tessuto")
	
	
	if dw_report.getitemstring(ll_i, "flag_rda_urgente") = "S" then
		ls_note = " *****  U R G E N T E *****  ~r~n" + ls_note
	end if

	ldd_quan_rda = dw_report.getitemnumber(ll_i, "quan_rda")
	
	// eseguo trasformazione in MQ
	SELECT tab_tessuti.alt_tessuto  
   INTO   :ld_alt_tessuto  
   FROM   tab_tessuti  
   WHERE  cod_azienda = :s_cs_xx.cod_azienda and
          cod_tessuto = :ls_cod_prodotto and
          cod_non_a_magazzino = :ls_des_specifica ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("SEP","Errore in ricerca codice tessuto~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	if ld_alt_tessuto <> 0 then
		ldd_quan_rda = round(ldd_quan_rda * (ld_alt_tessuto / 100), 2)
	end if

	// fine trasformazione in MQ
	
	setnull(ldt_data_consegna)
	
	select cod_tipo_politica_riordino
	into   :ls_cod_tipo_politica_riordino
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("SEP","Errore in ricerca prodotto in anagrafica prodotti")
		rollback;
		return
	end if
	
	ls_flag_gen_commessa = "N"

	ll_ret = luo_gen_doc.uof_genera_rda(ls_cod_prodotto, &
	 											   ls_des_prodotto, &
													ls_null, &
													ls_cod_fornitore, &
													ldd_quan_rda, &
													0, &
													ll_anno_commessa, &
													ll_num_commessa, &
													ldt_data_consegna, &
													ls_cod_tipo_politica_riordino, &
													ls_flag_gen_commessa, &
													ls_note, &
													ls_des_specifica, &
													ll_null,&
													ll_null,&
													ll_null,&
													ref ll_anno_reg_rda, &
													ref ll_num_reg_rda, &
													ref ls_errore)
	if ll_ret < 0 then
		g_mb.messagebox("SEP","Errore durante generazione RDA.~r~n" +ls_errore,stopsign!)
		rollback;
		exit
	end if
	
	ls_rda_generate += "~r~n" + string(ll_anno_reg_rda) + "/" + string(ll_num_reg_rda) 
	
next

destroy luo_gen_doc

commit;

g_mb.messagebox("SEP","RDA Generate:" + ls_rda_generate,stopsign!)

return
end event

type st_2 from statictext within w_report_mrp_tessuti
integer x = 64
integer y = 1280
integer width = 2665
integer height = 84
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "DOPO AVER ELABORATO IL REPORT E~' POSSIBILE GENERARE LE RDA PER I PRODOTTI MANCANTI."
boolean focusrectangle = false
end type

type cb_ricerca_tessuti from commandbutton within w_report_mrp_tessuti
integer x = 2322
integer y = 352
integer width = 73
integer height = 76
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;s_cs_xx.parametri.parametro_uo_dw_1 = dw_selezione
s_cs_xx.parametri.parametro_s_1 = "cod_colore_tessuto"
s_cs_xx.parametri.parametro_s_2 = "cod_tessuto"

window_open(w_tessuti_ricerca, 0)

dw_selezione.setcolumn("cod_tessuto")
dw_selezione.settext(s_cs_xx.parametri.parametro_s_1)
dw_selezione.setcolumn("cod_colore_tessuto")
dw_selezione.settext(s_cs_xx.parametri.parametro_s_2)
dw_selezione.accepttext()

end event

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

type cb_report from commandbutton within w_report_mrp_tessuti
integer x = 1934
integer y = 1176
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string ls_cod_fornitore, ls_cod_tessuto, ls_cod_colore_tessuto, ls_cod_reparto,ls_cod_tipo_campionario, ls_cod_tessuto_mrp, &
       ls_cod_colore_tessuto_mrp, ls_errore, ls_des_colore_tessuto, ls_cod_fornitore_mrp, ls_rag_soc_1_mrp, &
		 ls_cod_misura_acq, ls_des_stato_riordini, ls_des_tessuto, ls_flag_ordinamento, ls_flag_tipo_magazzino
long   ll_ret, ll_row, ll_anno_reg_ord_acq, ll_num_reg_ord_acq, ll_prog_riga_ord_acq, ll_anno_reg_rda, ll_num_reg_rda, &
       ll_prog_riga_rda ,ll_cont_tessuti
dec{4} ld_quan_impegno, ld_quan_riferimento,ld_giacenza_colore, ld_altezza_tessuto, ld_giacenza_minima, &
		 ld_fat_conv_ord_acq, ld_quan_ordinata, ld_quan_arrivata, ld_quan_rda, ld_quan_residuo, ls_quan_ord_rda, &
		 ld_quan_fabbisogno
datetime ldt_data_riferimento, ldt_data_consegna, ldt_data_disponibilita, ldt_data_registrazione
uo_mrp_tessuti luo_mrp_tessuti

declare cu_tessuti_magazzino cursor for
select tab_tessuti.cod_tessuto,   
		 anag_prodotti.des_prodotto,
		 tab_tessuti.cod_non_a_magazzino,
		 tab_tessuti.alt_tessuto,
		 tab_colori_tessuti.des_colore_tessuto
from   tab_tessuti LEFT OUTER JOIN anag_prodotti      ON tab_tessuti.cod_azienda = anag_prodotti.cod_azienda AND 
																	      tab_tessuti.cod_tessuto = anag_prodotti.cod_prodotto  
						 LEFT OUTER JOIN tab_colori_tessuti ON tab_tessuti.cod_azienda         = tab_colori_tessuti.cod_azienda AND 
																	      tab_tessuti.cod_non_a_magazzino = tab_colori_tessuti.cod_colore_tessuto  
where ( tab_tessuti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( tab_tessuti.cod_tipo_campionario like :ls_cod_tipo_campionario or tab_tessuti.cod_tipo_campionario is null) AND  
		( tab_tessuti.cod_tessuto like :ls_cod_tessuto ) AND  
		( tab_tessuti.cod_non_a_magazzino like :ls_cod_colore_tessuto ) AND  
		( anag_prodotti.cod_reparto like :ls_cod_reparto ) AND
		( tab_tessuti.flag_reparto_taglio = 'S') and
		( tab_tessuti.flag_blocco = 'N')
order by tab_tessuti.cod_non_a_magazzino, 
 		   tab_tessuti.cod_tessuto;

declare cu_ordini_tessuti_mrp cursor  for
select det_ord_acq.anno_registrazione,   
		det_ord_acq.num_registrazione,   
		prog_riga_ordine_acq,   
		cod_misura,   
		fat_conversione,   
		det_ord_acq.data_consegna,   
		quan_ordinata,   
		quan_arrivata,
		tes_ord_acq.data_registrazione
from  det_ord_acq   LEFT OUTER JOIN tes_ord_acq ON det_ord_acq.cod_azienda        = tes_ord_acq.cod_azienda AND 
													       det_ord_acq.anno_registrazione = tes_ord_acq.anno_registrazione and 
													       det_ord_acq.num_registrazione  = tes_ord_acq.num_registrazione 
where ( det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( det_ord_acq.cod_prodotto = :ls_cod_tessuto_mrp ) AND  
		( det_ord_acq.des_specifica = :ls_cod_colore_tessuto_mrp ) AND  
		( det_ord_acq.cod_tipo_det_acq in ( select cod_tipo_det_acq from tab_tipi_det_acq where cod_azienda = :s_cs_xx.cod_azienda and flag_tipo_det_acq = 'M') ) AND  
		( det_ord_acq.flag_saldo = 'N' ) AND  
		( det_ord_acq.flag_blocco = 'N' ) AND  
		( det_ord_acq.quan_ordinata > det_ord_acq.quan_arrivata )  ;

declare cu_rda_mrp cursor  for
select anno_registrazione,   
		 num_registrazione,   
		 prog_riga_rda,   
		 quan_richiesta,
		 data_disponibilita
 from  det_rda  
where ( det_rda.cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( det_rda.flag_gen_ord_acq = 'N' ) AND  
		( det_rda.flag_gen_commessa = 'N' ) AND  
		( det_rda.cod_prodotto = :ls_cod_tessuto_mrp ) AND  
		( det_rda.des_specifica = :ls_cod_colore_tessuto_mrp ) ;


dw_selezione.accepttext()

if g_mb.messagebox("APICE","Procedo con l'elaborazione del report?",Question!,Yesno!,2) = 2 then return

dw_report.reset()

ls_cod_fornitore = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_fornitore")
ls_cod_tipo_campionario = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_tipo_campionario")
ls_cod_tessuto = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_tessuto")
ls_cod_colore_tessuto = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_colore_tessuto")
ls_cod_reparto = dw_selezione.getitemstring(dw_selezione.getrow(),"cod_reparto")
ldt_data_riferimento = dw_selezione.getitemdatetime(dw_selezione.getrow(),"data_riferimento")
ld_quan_riferimento    = dw_selezione.getitemnumber(dw_selezione.getrow(),"giacenza_minima")
ls_flag_tipo_magazzino = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_tipo_magazzino")

if isnull(ls_cod_fornitore) or len(ls_cod_fornitore) < 1 then ls_cod_fornitore = "%"
if isnull(ls_cod_tipo_campionario) or len(ls_cod_tipo_campionario) < 1 then ls_cod_tipo_campionario = "%"
if isnull(ls_cod_tessuto) or len(ls_cod_tessuto) < 1 then ls_cod_tessuto = "%"
if isnull(ls_cod_colore_tessuto) or len(ls_cod_colore_tessuto) < 1 then ls_cod_colore_tessuto = "%"
if isnull(ls_cod_reparto) or len(ls_cod_reparto) < 1 then ls_cod_reparto = "%"
if isnull(ld_quan_riferimento) then ld_quan_riferimento = 0

luo_mrp_tessuti = create uo_mrp_tessuti

open cu_tessuti_magazzino;
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in OPEN cursore cu_tessuti_magazzino~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

do while true
	fetch cu_tessuti_magazzino into :ls_cod_tessuto_mrp, :ls_des_tessuto, :ls_cod_colore_tessuto_mrp, :ld_altezza_tessuto, :ls_des_colore_tessuto;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in FETCH cursore cu_tessuti_magazzino~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	if isnull(ls_cod_colore_tessuto_mrp) then ls_cod_colore_tessuto_mrp = ""
	if isnull(ls_cod_tessuto_mrp) then ls_cod_tessuto_mrp = ""
	if isnull(ls_des_colore_tessuto) then ls_des_colore_tessuto = ""

	sle_1.text = ls_cod_colore_tessuto_mrp + "-" + ls_cod_tessuto_mrp + " Calcolo MRP in corso ...."
	
	yield()
	
	luo_mrp_tessuti.ib_debug = ib_debug
	
	ll_ret = luo_mrp_tessuti.uof_mrp_tessuto(ls_cod_colore_tessuto_mrp, ls_cod_tessuto_mrp, ldt_data_riferimento, ref ld_quan_impegno, ref ls_errore)
	
	if ll_ret < 0 then
		g_mb.messagebox("APICE",ls_errore)
		rollback;
		exit
	end if
	
	sle_1.text = ls_cod_colore_tessuto_mrp + "-" + ls_cod_tessuto_mrp + " Analisi dei dati ...."
	
	yield()
		
	if (ld_quan_impegno > ld_quan_riferimento) or (ls_flag_tipo_magazzino <> "I") then
		
		if ls_flag_tipo_magazzino = "T" then
			
			ll_cont_tessuti = 0
			
			select count(*)
			into   :ll_cont_tessuti
			from   sybmgmtlg.dbo.js_process
			where  js_clts = :ls_cod_colore_tessuto_mrp;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE", "Errore in calcolo giacenza reale tessuto~r~n" + sqlca.sqlerrtext)
				rollback;
				exit
			end if
			
			if ll_cont_tessuti = 0 then continue 
			
		end if
		
		
		// Vado a leggere la giancenza dal magazzino tessuti (in mm)
		select sum(js_procqty)
		into   :ld_giacenza_colore
		from   sybmgmtlg.dbo.js_process
		where  js_clts = :ls_cod_colore_tessuto_mrp;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE", "Errore in calcolo giacenza reale tessuto~r~n" + sqlca.sqlerrtext)
			rollback;
			exit
		end if
		
		// converto la giacenza in metri lineari
		if isnull(ld_giacenza_colore) then ld_giacenza_colore = 0
		
		// trasformo la giacenza da millimetri in metri lineari, arrotondando al secondo decimal
		if ld_giacenza_colore <> 0 then	ld_giacenza_colore = round(ld_giacenza_colore / 1000, 2)
		
		// il fabbisogno è espresso in MQ; lo converto in ML dividendo per la larghezza presa dalla tabella tessuti
		// parlato con Beatrice 19/03/2008; il dato proviente dalla distinta è comprensivo del calcolo del vuoto per pieno
		// e quindi per ottenere il totale metri lineari di tessuto è suffiente dividere per la larghezza (ovviamente anche questa espressa in ML)
		if ld_altezza_tessuto = 0 or isnull(ld_altezza_tessuto) then
			g_mb.messagebox("APICE", "Attenzione: il tessuto gruppo:"+ls_cod_tessuto_mrp+", colore:"+ ls_cod_colore_tessuto_mrp +" ha altezza=0; quindi il dato del fabbisogno non è affidabile")
		else
			// lo trasformo in metri lineari
			ld_altezza_tessuto = ld_altezza_tessuto / 100
			ld_quan_impegno = round(ld_quan_impegno / ld_altezza_tessuto,2)
		end if
		
		// ricerco fornitore e giacenza minima da tabella tessuti locale
		setnull(ls_cod_fornitore_mrp)
		setnull(ls_rag_soc_1_mrp)
		setnull(ld_giacenza_minima)
		
		select cod_fornitore, giacenza_minima
		into   :ls_cod_fornitore_mrp, :ld_giacenza_minima
		from   tab_tessuti_locale
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tessuto = :ls_cod_tessuto_mrp and
				 cod_non_a_magazzino = :ls_cod_colore_tessuto_mrp;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE", "Errore in ricerca fornitore e giacenza minima in tabella tessuti locale~r~n" + sqlca.sqlerrtext)
			rollback;
			exit
		end if
		
		if isnull(ls_cod_fornitore_mrp) then
			ls_cod_fornitore_mrp = ""
			ls_rag_soc_1_mrp = "NON INDICATO"
		else
			select rag_soc_1
			into   :ls_rag_soc_1_mrp
			from   anag_fornitori
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_fornitore = :ls_cod_fornitore_mrp;
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE", "Errore in ricerca ragione sociale fornitore "+ls_cod_fornitore_mrp+"~r~n" + sqlca.sqlerrtext)
				rollback;
				exit
			end if
		end if
		
		if isnull(ld_giacenza_minima) then ld_giacenza_minima = 0
		
		
		// ricerco calcolo la quantità in ordine
		ls_des_stato_riordini = ""
		ls_quan_ord_rda = 0
		
		open cu_ordini_tessuti_mrp;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore in OPEN cursore cu_ordini_tessuti_mrp~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		sle_1.text = ls_cod_colore_tessuto_mrp + "-" + ls_cod_tessuto_mrp + " Analisi ordini fornitori ...."
		
		yield()
		
		do while true
			fetch cu_ordini_tessuti_mrp into :ll_anno_reg_ord_acq, :ll_num_reg_ord_acq, :ll_prog_riga_ord_acq, :ls_cod_misura_acq, :ld_fat_conv_ord_acq, :ldt_data_consegna, :ld_quan_ordinata, :ld_quan_arrivata, :ldt_data_registrazione;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore in fetch cursore cu_ordini_tessuti_mrp~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
			if len(ls_des_stato_riordini) > 0 then ls_des_stato_riordini += "~r~n"
			
			ld_quan_residuo = (ld_quan_ordinata - ld_quan_arrivata) / ld_altezza_tessuto
			ls_des_stato_riordini += string(ldt_data_consegna,"dd/mm/yy") + " ORD " + string(ld_quan_residuo,"###,##0.00") + " (" + string(ldt_data_registrazione,"dd/mm/yy") + ")"
			ls_quan_ord_rda += ld_quan_residuo
		loop
		
		close cu_ordini_tessuti_mrp;
		
		sle_1.text = ls_cod_colore_tessuto_mrp + "-" + ls_cod_tessuto_mrp + " Analisi RDA ...."
		
		yield()
		
		// ricerco calcolo la quantità presente nelle RDA
		open cu_rda_mrp;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore in OPEN cursore cu_rda_mrp~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
		
		do while true
			fetch cu_rda_mrp into :ll_anno_reg_rda, :ll_num_reg_rda, :ll_prog_riga_rda, :ld_quan_rda, :ldt_data_disponibilita;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore in fetch cursore cu_ordini_tessuti_mrp~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
			
			if len(ls_des_stato_riordini) > 0 then ls_des_stato_riordini += "~r~n"
			
			ld_quan_rda = ld_quan_rda / ld_altezza_tessuto
			ls_des_stato_riordini += string(ldt_data_disponibilita,"dd/mm/yyyy") +"  RDA  " + string(ld_quan_rda,"###,##0.00")
			ls_quan_ord_rda += ld_quan_rda

		loop
		
		close cu_rda_mrp;
		
		// trasformo la quantià acquisto da MQ a ML
		// attenzione: l'altezza tessuto è già stata trasformata in ML più sopra.
		ls_quan_ord_rda = ls_quan_ord_rda
		ls_quan_ord_rda = round(ls_quan_ord_rda, 2)
		
		// calcolo il fabbisogno reale
		ld_quan_fabbisogno = ld_giacenza_colore - ld_quan_impegno + ls_quan_ord_rda
		ld_quan_fabbisogno = ld_giacenza_minima - ld_quan_fabbisogno
		
		ll_row = dw_report.insertrow(0)
		
		dw_report.setitem(ll_row, "cod_fornitore", ls_cod_fornitore_mrp)
		dw_report.setitem(ll_row, "rag_soc_1", ls_rag_soc_1_mrp)
		dw_report.setitem(ll_row, "cod_tessuto", ls_cod_tessuto_mrp)
		dw_report.setitem(ll_row, "des_tessuto", ls_des_tessuto)
		dw_report.setitem(ll_row, "cod_colore_tessuto", ls_cod_colore_tessuto_mrp)
		dw_report.setitem(ll_row, "des_colore_tessuto", ls_des_colore_tessuto)
		dw_report.setitem(ll_row, "quan_giacenza_attuale", ld_giacenza_colore)
		dw_report.setitem(ll_row, "quan_impegnata", ld_quan_impegno)
		dw_report.setitem(ll_row, "quan_giacenza_minima", ld_giacenza_minima)
		dw_report.setitem(ll_row, "des_stato_riordini", ls_des_stato_riordini)
		dw_report.setitem(ll_row, "quan_giacenza_minima", ld_giacenza_minima)
		
		if ld_quan_fabbisogno < 0 then
			ld_quan_fabbisogno = 0
		end if
		
		dw_report.setitem(ll_row, "quan_fabbisogno", ld_quan_fabbisogno)
		dw_report.setitem(ll_row, "flag_rda_urgente", "N")
		dw_report.setitem(ll_row, "flag_gen_rda", "N")
		dw_report.setitem(ll_row, "quan_rda", 0)
		
		
	end if
loop

sle_1.text = ""

ls_flag_ordinamento = dw_selezione.getitemstring(dw_selezione.getrow(),"flag_ordinamento")

choose case ls_flag_ordinamento
	case "G"
		// giancenza rimanente
		dw_report.setsort("compute_1")
	case "F"
		// quantità fabbisogno
		dw_report.setsort("quan_fabbisogno")
	case "T"
		// codice colote tessuto
		dw_report.setsort("cod_colore_tessuto")
end choose

dw_report.sort()

destroy luo_mrp_tessuti

close cu_tessuti_magazzino;

dw_folder.fu_selecttab(2)

dw_report.change_dw_current()

end event

type sle_1 from singlelineedit within w_report_mrp_tessuti
integer x = 59
integer y = 1176
integer width = 1865
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean enabled = false
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
end type

type dw_selezione from uo_cs_xx_dw within w_report_mrp_tessuti
integer x = 41
integer y = 168
integer width = 2688
integer height = 988
integer taborder = 40
string dataobject = "d_report_mrp_tessuti_selezione"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_nulla

setnull(ls_nulla)

choose case i_colname
	case "cod_tessuto"
			setitem(getrow(),"cod_colore_tessuto", ls_nulla)
end choose
end event

event pcd_new;call super::pcd_new;datetime ldt_data_riferimento

ldt_data_riferimento = datetime(relativedate(today(),30), 00:00:00)

setitem(getrow(),"data_riferimento", ldt_data_riferimento)

end event

event doubleclicked;call super::doubleclicked;if i_extendmode then
	
	if isvalid(dwo) then
		
		if dwo.name = "t_3" then
			
			if ib_debug then
				
				ib_debug = false
				
				g_mb.messagebox("MRP","Modalità DEBUG disattivata")
				
			else
				
				ib_debug = true
				
				g_mb.messagebox("MRP","Modalità DEBUG attivata")
				
			end if
			
		end if
		
	end if
	
end if
		
	
end event

type dw_folder from u_folder within w_report_mrp_tessuti
integer x = 9
integer y = 12
integer width = 3648
integer height = 1788
integer taborder = 90
boolean border = false
end type

type dw_report from uo_cs_xx_dw within w_report_mrp_tessuti
integer x = 46
integer y = 124
integer width = 3589
integer height = 1556
integer taborder = 20
string dataobject = "d_report_mrp_tessuti"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;call super::doubleclicked;if i_extendmode then
	if lower(dwo.name) = "p_note" then
		
		string ls_note
		
		ls_note = getitemstring(row, "note")
		
		openwithparm(w_report_mrp_tessuti_note, ls_note)
		
		ls_note = message.stringparm
		
		setitem(row, "note", ls_note)
		
	end if
end if
end event

