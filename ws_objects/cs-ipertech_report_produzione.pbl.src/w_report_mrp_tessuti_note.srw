﻿$PBExportHeader$w_report_mrp_tessuti_note.srw
forward
global type w_report_mrp_tessuti_note from window
end type
type dw_report_mrp_tessuti_note from uo_std_dw within w_report_mrp_tessuti_note
end type
end forward

global type w_report_mrp_tessuti_note from window
integer width = 2610
integer height = 868
boolean titlebar = true
string title = "NOTE"
boolean controlmenu = true
windowtype windowtype = response!
long backcolor = 12632256
string icon = "DosEdit5!"
boolean center = true
dw_report_mrp_tessuti_note dw_report_mrp_tessuti_note
end type
global w_report_mrp_tessuti_note w_report_mrp_tessuti_note

on w_report_mrp_tessuti_note.create
this.dw_report_mrp_tessuti_note=create dw_report_mrp_tessuti_note
this.Control[]={this.dw_report_mrp_tessuti_note}
end on

on w_report_mrp_tessuti_note.destroy
destroy(this.dw_report_mrp_tessuti_note)
end on

event open;long ll_row
string ls_note

ls_note = message.stringparm

//dw_report_mrp_tessuti_note.ib_dw_report = true

ll_row = dw_report_mrp_tessuti_note.insertrow(0)
dw_report_mrp_tessuti_note.setitem(ll_row, "note", ls_note)
end event

event closequery;long ll_row
string ls_note

dw_report_mrp_tessuti_note.accepttext()
ls_note = dw_report_mrp_tessuti_note.getitemstring(dw_report_mrp_tessuti_note.getrow(), "note")

message.stringparm = ls_note
end event

type dw_report_mrp_tessuti_note from uo_std_dw within w_report_mrp_tessuti_note
integer width = 2587
integer height = 780
integer taborder = 10
string title = "none"
string dataobject = "d_report_mrp_tessuti_note"
boolean controlmenu = true
boolean border = false
end type

