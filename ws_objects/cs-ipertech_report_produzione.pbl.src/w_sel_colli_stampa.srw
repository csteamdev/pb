﻿$PBExportHeader$w_sel_colli_stampa.srw
forward
global type w_sel_colli_stampa from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_sel_colli_stampa
end type
type cb_nessuno from commandbutton within w_sel_colli_stampa
end type
type cb_tutti from commandbutton within w_sel_colli_stampa
end type
type cb_ok from commandbutton within w_sel_colli_stampa
end type
type dw_1 from datawindow within w_sel_colli_stampa
end type
end forward

global type w_sel_colli_stampa from w_cs_xx_principale
integer width = 2583
integer height = 2272
string title = "Seleziona Colli"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
cb_annulla cb_annulla
cb_nessuno cb_nessuno
cb_tutti cb_tutti
cb_ok cb_ok
dw_1 dw_1
end type
global w_sel_colli_stampa w_sel_colli_stampa

on w_sel_colli_stampa.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_nessuno=create cb_nessuno
this.cb_tutti=create cb_tutti
this.cb_ok=create cb_ok
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_nessuno
this.Control[iCurrent+3]=this.cb_tutti
this.Control[iCurrent+4]=this.cb_ok
this.Control[iCurrent+5]=this.dw_1
end on

on w_sel_colli_stampa.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_nessuno)
destroy(this.cb_tutti)
destroy(this.cb_ok)
destroy(this.dw_1)
end on

event open;call super::open;long ll_index, ll_row
string ls_vuoto[]

for ll_index=1 to upperbound(s_cs_xx.parametri.parametro_s_4_a)
	
	ll_row = dw_1.insertrow(0)
	
	dw_1.setitem(ll_row, "barcode", s_cs_xx.parametri.parametro_s_4_a[ll_index])
next

dw_1.sort( )

s_cs_xx.parametri.parametro_s_4_a = ls_vuoto

cb_ok.setfocus( )
end event

type cb_annulla from commandbutton within w_sel_colli_stampa
integer x = 1696
integer y = 1900
integer width = 695
integer height = 232
integer taborder = 30
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;

close(parent)
end event

type cb_nessuno from commandbutton within w_sel_colli_stampa
integer x = 549
integer y = 28
integer width = 402
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "De-sel. Tutti"
end type

event clicked;long ll_index

for ll_index = 1 to dw_1.rowcount()
	dw_1.setitem(ll_index, "sel", "N")
next
end event

type cb_tutti from commandbutton within w_sel_colli_stampa
integer x = 110
integer y = 28
integer width = 402
integer height = 88
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sel. Tutti"
end type

event clicked;long ll_index

for ll_index = 1 to dw_1.rowcount()
	dw_1.setitem(ll_index, "sel", "S")
next
end event

type cb_ok from commandbutton within w_sel_colli_stampa
integer x = 119
integer y = 1900
integer width = 695
integer height = 232
integer taborder = 20
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "STAMPA"
boolean default = true
end type

event clicked;long ll_index, ll_count
string ls_sel

dw_1.accepttext()

ll_count = 0
//for ll_index=1 to dw_1.rowcount()
for ll_index=dw_1.rowcount() to 1 step -1
	
	ls_sel = dw_1.getitemstring(ll_index, "sel")
	if ls_sel="S" then
		ll_count += 1
		
		s_cs_xx.parametri.parametro_s_4_a[ll_count] = dw_1.getitemstring(ll_index, "barcode")
	end if
	
next

close(parent)
end event

type dw_1 from datawindow within w_sel_colli_stampa
integer x = 27
integer y = 152
integer width = 2501
integer height = 1704
integer taborder = 10
string title = "none"
string dataobject = "d_sel_colli_stampa"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

