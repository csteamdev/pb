﻿$PBExportHeader$w_stampa_certificazione.srw
forward
global type w_stampa_certificazione from w_std_principale
end type
type cbx_dop from checkbox within w_stampa_certificazione
end type
type cbx_certificazione from checkbox within w_stampa_certificazione
end type
type st_1 from statictext within w_stampa_certificazione
end type
type dw_dichiarazione_prestazione from uo_std_dw within w_stampa_certificazione
end type
type cb_3 from commandbutton within w_stampa_certificazione
end type
type cb_pdf from commandbutton within w_stampa_certificazione
end type
type cb_stampa from commandbutton within w_stampa_certificazione
end type
type dw_certificazione_prod from uo_std_dw within w_stampa_certificazione
end type
end forward

global type w_stampa_certificazione from w_std_principale
integer width = 1435
integer height = 828
string title = "Certificazione"
cbx_dop cbx_dop
cbx_certificazione cbx_certificazione
st_1 st_1
dw_dichiarazione_prestazione dw_dichiarazione_prestazione
cb_3 cb_3
cb_pdf cb_pdf
cb_stampa cb_stampa
dw_certificazione_prod dw_certificazione_prod
end type
global w_stampa_certificazione w_stampa_certificazione

type variables
string	is_matricola[]
long	il_anno_reg_ord_ven, il_num_reg_ord_ven, il_prog_riga_ord_ven
end variables

on w_stampa_certificazione.create
int iCurrent
call super::create
this.cbx_dop=create cbx_dop
this.cbx_certificazione=create cbx_certificazione
this.st_1=create st_1
this.dw_dichiarazione_prestazione=create dw_dichiarazione_prestazione
this.cb_3=create cb_3
this.cb_pdf=create cb_pdf
this.cb_stampa=create cb_stampa
this.dw_certificazione_prod=create dw_certificazione_prod
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_dop
this.Control[iCurrent+2]=this.cbx_certificazione
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.dw_dichiarazione_prestazione
this.Control[iCurrent+5]=this.cb_3
this.Control[iCurrent+6]=this.cb_pdf
this.Control[iCurrent+7]=this.cb_stampa
this.Control[iCurrent+8]=this.dw_certificazione_prod
end on

on w_stampa_certificazione.destroy
call super::destroy
destroy(this.cbx_dop)
destroy(this.cbx_certificazione)
destroy(this.st_1)
destroy(this.dw_dichiarazione_prestazione)
destroy(this.cb_3)
destroy(this.cb_pdf)
destroy(this.cb_stampa)
destroy(this.dw_certificazione_prod)
end on

event open;call super::open;string							ls_message
s_stampa_certificazione lstr_stampa_certificazione

lstr_stampa_certificazione = message.powerobjectparm

il_anno_reg_ord_ven = lstr_stampa_certificazione.anno_reg_ord_ven
il_num_reg_ord_ven = lstr_stampa_certificazione.num_reg_ord_ven
il_prog_riga_ord_ven = lstr_stampa_certificazione.prog_riga_ord_ven
is_matricola = lstr_stampa_certificazione.cod_matricola

dw_certificazione_prod.ib_dw_report=true
dw_dichiarazione_prestazione.ib_dw_report=true

uo_certificati_dop luo_certificati_dop
luo_certificati_dop = create uo_certificati_dop

if luo_certificati_dop.uof_genera_certificato(il_anno_reg_ord_ven, il_num_reg_ord_ven, il_prog_riga_ord_ven,is_matricola, dw_certificazione_prod, dw_dichiarazione_prestazione, ref ls_message) < 0 then
	g_mb.error( ls_message)
end if

destroy luo_certificati_dop



end event

type cbx_dop from checkbox within w_stampa_certificazione
integer x = 183
integer y = 580
integer width = 960
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 553648127
string text = "DoP"
boolean checked = true
end type

type cbx_certificazione from checkbox within w_stampa_certificazione
integer x = 183
integer y = 480
integer width = 960
integer height = 80
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 553648127
string text = "Stampa Certificazione"
boolean checked = true
end type

type st_1 from statictext within w_stampa_certificazione
integer x = 46
integer y = 20
integer width = 1326
integer height = 220
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Gestione Certificazioni"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_dichiarazione_prestazione from uo_std_dw within w_stampa_certificazione
boolean visible = false
integer x = 2354
integer y = 160
integer width = 2309
integer height = 2520
integer taborder = 20
string dataobject = "d_report_dichiarazione_prestazione"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type cb_3 from commandbutton within w_stampa_certificazione
event type integer wf_calcola_classe_gtot ( decimal ad_gtot,  ref long al_classe )
integer x = 937
integer y = 320
integer width = 389
integer height = 100
integer taborder = 30
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Email"
end type

event clicked;string					ls_message,ls_path
long					ll_ret
uo_certificati_dop luo_certificati_dop
luo_certificati_dop = create uo_certificati_dop

// esegue il merge dei certificati
luo_certificati_dop = create uo_certificati_dop
ll_ret =luo_certificati_dop.uof_merge_pdf( dw_certificazione_prod,dw_dichiarazione_prestazione, cbx_certificazione.checked, cbx_dop.checked, ref ls_path)
//ll_ret = wf_merge_pdf( ls_path)
if ll_ret < 0 then
	g_mb.error ("Errore nella creazione e unione dei file PDf.~r~n" + ls_path)
	return -1
elseif ll_ret = 1 then
	g_mb.warning ("Nessun Report Selezionato")
	return 0
end if

luo_certificati_dop.uof_invia_certificati(il_anno_reg_ord_ven, il_num_reg_ord_ven, il_prog_riga_ord_ven, ls_path, ref ls_message)

destroy luo_certificati_dop
return 0




//// funzione che provvedere all'invio MAIL
/*
long						ll_ret
string						ls_path, ls_rag_soc_1, ls_destinatari[],ls_allegati[], ls_subject, ls_user_email, ls_azienda, ls_message, ls_messaggio,ls_errore
uo_outlook			luo_outlook
transaction			lt_tran_docs
uo_certificati_dop	luo_certificati_dop
	
// esegue il merge dei certificati
luo_certificati_dop = create uo_certificati_dop
ll_ret =luo_certificati_dop.uof_merge_pdf( dw_certificazione_prod,dw_dichiarazione_prestazione, cbx_certificazione.checked, cbx_dop.checked, ref ls_path)
//ll_ret = wf_merge_pdf( ls_path)
if ll_ret < 0 then
	g_mb.error ("Errore nella creazione e unione dei file PDf.~r~n" + ls_path)
	return -1
elseif ll_ret = 1 then
	g_mb.warning ("Nessun Report Selezionato")
	return 0
end if

// --- invio file tramite mail

select A.rag_soc_1 rag_soc_cliente, 
		A.casella_mail,
		Z.rag_soc_1 rag_soc_azienda
into	:ls_rag_soc_1,
		:ls_destinatari[1],
		:ls_azienda
from	tes_ord_ven T
left outer join anag_clienti A on T.cod_azienda=A.cod_azienda and T.cod_cliente=A.cod_cliente
left outer join aziende Z on Z.cod_azienda = T.cod_azienda
where	T.cod_azienda = :s_cs_xx.cod_azienda and
			T.anno_registrazione = :il_anno_reg_ord_ven and
			T.num_registrazione = :il_num_reg_ord_ven;
if sqlca.sqlcode <> 0 then
	g_mb.error(g_str.format("Errore SQL durante la ricerca ordine $1 - $2", il_anno_reg_ord_ven,il_num_reg_ord_ven))
	return
end if


ls_allegati[1]    = ls_path

select		e_mail
into		:ls_user_email
from 		utenti
where	cod_utente  = :s_cs_xx.cod_utente;
	
if sqlca.sqlcode = 0 and not isnull(ls_user_email) and len(ls_user_email) > 0 then
	ls_destinatari[2] = ls_user_email
else
	g_mb.error("Email mittente non specificata; non verrà mandata alcuna mail al mittente!")
end if	

ls_subject = g_str.format("$1 - Invio Certificazione e DoP. ", ls_azienda)
ls_message = "Gentile Cliente,~r~nalla presente alleghiamo i documenti richiesti.~r~n~r~nCordiali Saluti~r~nAlusistemi srl"

luo_outlook = create uo_outlook
luo_outlook.ib_html = false

if not luo_outlook.uof_invio_sendmail( ls_destinatari[], ls_subject, ls_message, ls_allegati[], ref ls_messaggio) then
	g_mb.messagebox("APICE", "Errore in fase di invio Conferma Ordine: " + ls_messaggio)
else
	
	//creo transazione per i documenti
	if not guo_functions.uof_create_transaction_from( sqlca, lt_tran_docs, ls_errore)  then
		//problemi in creazione transazione documenti, non archiviare il PDF
		g_mb.error(ls_errore)
		g_mb.warning("A causa di un errore in creazione transazione non è stato possibile archiviare il PDF , ma l'email è stata inviata correttamente!")
	else
		wf_memorizza_blob(il_anno_reg_ord_ven,il_num_reg_ord_ven, ls_path, lt_tran_docs)
		commit;
		commit using lt_tran_docs;
		
	end if
	
	g_mb.success("Mail inviata con successo")
end if
	
destroy luo_outlook

filedelete(ls_path)

return 0
*/
end event

type cb_pdf from commandbutton within w_stampa_certificazione
integer x = 503
integer y = 320
integer width = 389
integer height = 100
integer taborder = 20
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Pdf"
end type

event clicked;long 		ll_ret
string		ls_merged

uo_certificati_dop	luo_certificati_dop
	
// esegue il merge dei certificati
luo_certificati_dop = create uo_certificati_dop
ll_ret =luo_certificati_dop.uof_merge_pdf( dw_certificazione_prod,dw_dichiarazione_prestazione, cbx_certificazione.checked, cbx_dop.checked, ref ls_merged)
//ll_ret=wf_merge_pdf(ref ls_merged)
if ll_ret < 0 then
	g_mb.error("Errore durante generazione e unione dei PDF. " + ls_merged)
elseif ll_ret = 1 then
	g_mb.messagebox("Apice","Nessun report selezionato.")
else	
	g_mb.messagebox("Apice","Generazione PDF eseguita correttamente.~r~nControllare la cartella Documenti")
end if
destroy luo_certificati_dop
end event

type cb_stampa from commandbutton within w_stampa_certificazione
integer x = 69
integer y = 320
integer width = 389
integer height = 100
integer taborder = 10
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;long ll_job

ll_job=printopen("Stampa Dichiarazioni", TRUE)
if ll_job < 0 then return

if cbx_certificazione.checked then  printdatawindow(ll_job, dw_certificazione_prod)
if cbx_dop.checked then printdatawindow(ll_job, dw_dichiarazione_prestazione)

PrintClose(ll_job)

end event

type dw_certificazione_prod from uo_std_dw within w_stampa_certificazione
boolean visible = false
integer x = 23
integer y = 160
integer width = 2309
integer height = 2520
integer taborder = 10
string dataobject = "d_report_certificazione_produttore"
boolean hscrollbar = true
boolean vscrollbar = true
end type

