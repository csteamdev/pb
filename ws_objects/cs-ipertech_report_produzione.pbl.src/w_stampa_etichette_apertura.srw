﻿$PBExportHeader$w_stampa_etichette_apertura.srw
$PBExportComments$Finestra per la stampa delle etichette di produzione
forward
global type w_stampa_etichette_apertura from w_cs_xx_risposta
end type
type st_3 from statictext within w_stampa_etichette_apertura
end type
type dw_report_produzione from datawindow within w_stampa_etichette_apertura
end type
type st_4 from statictext within w_stampa_etichette_apertura
end type
type dw_report from datawindow within w_stampa_etichette_apertura
end type
type st_2 from statictext within w_stampa_etichette_apertura
end type
type em_num_copie from editmask within w_stampa_etichette_apertura
end type
type st_1 from statictext within w_stampa_etichette_apertura
end type
type cb_conferma from commandbutton within w_stampa_etichette_apertura
end type
type cb_chiudi from commandbutton within w_stampa_etichette_apertura
end type
end forward

global type w_stampa_etichette_apertura from w_cs_xx_risposta
integer width = 3031
integer height = 1420
string title = "STAMPA ETICHETTE PRODUZIONE"
boolean controlmenu = false
st_3 st_3
dw_report_produzione dw_report_produzione
st_4 st_4
dw_report dw_report
st_2 st_2
em_num_copie em_num_copie
st_1 st_1
cb_conferma cb_conferma
cb_chiudi cb_chiudi
end type
global w_stampa_etichette_apertura w_stampa_etichette_apertura

type variables
integer ii_ok
string is_alusistemi
string is_ETIC = "ETIC"
end variables

forward prototypes
public function string wf_get_colonna_dinamica (string fs_cod_cliente)
end prototypes

public function string wf_get_colonna_dinamica (string fs_cod_cliente);long ll_id_valore_lista
string ls_valore_stringa, ls_flag_tipo_colonna
decimal ld_valore_numero
datetime ldt_valore_data


select flag_tipo_colonna  
into :ls_flag_tipo_colonna
from tab_colonne_dinamiche
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_colonna_dinamica=:is_ETIC;

if isnull(ls_flag_tipo_colonna) or sqlca.sqlcode<>0 then return ""

select id_valore_lista,
		valore_stringa,
		valore_numero,
		valore_data
into	:ll_id_valore_lista,
		:ls_valore_stringa,
		:ld_valore_numero,
		:ldt_valore_data
from anag_clienti_col_din
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_colonna_dinamica=:is_ETIC and
			cod_cliente=:fs_cod_cliente;

choose case ls_flag_tipo_colonna
	case "L"
		select des_valore
		into :ls_valore_stringa
		from tab_colonne_dinamiche_valori
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_colonna_dinamica=:is_ETIC and
					progressivo=:ll_id_valore_lista;
		
		if isnull(ls_valore_stringa) or sqlca.sqlcode<>0 then return ""
		
		return ls_valore_stringa
		
	case "S"
		if isnull(ls_valore_stringa) then ls_valore_stringa=""
		return ls_valore_stringa
		
	case "N"
		if isnull(ld_valore_numero) then return ""
		return string(ld_valore_numero, "########0.00")
		
		
	case "D"
		if isnull(ldt_valore_data) or year(date(ldt_valore_data))<1950 then return ""
		return string(ldt_valore_data, "dd/mm/yy")
		
	case else
		return ""
		
end choose
		
return ""
end function

on w_stampa_etichette_apertura.create
int iCurrent
call super::create
this.st_3=create st_3
this.dw_report_produzione=create dw_report_produzione
this.st_4=create st_4
this.dw_report=create dw_report
this.st_2=create st_2
this.em_num_copie=create em_num_copie
this.st_1=create st_1
this.cb_conferma=create cb_conferma
this.cb_chiudi=create cb_chiudi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_3
this.Control[iCurrent+2]=this.dw_report_produzione
this.Control[iCurrent+3]=this.st_4
this.Control[iCurrent+4]=this.dw_report
this.Control[iCurrent+5]=this.st_2
this.Control[iCurrent+6]=this.em_num_copie
this.Control[iCurrent+7]=this.st_1
this.Control[iCurrent+8]=this.cb_conferma
this.Control[iCurrent+9]=this.cb_chiudi
end on

on w_stampa_etichette_apertura.destroy
call super::destroy
destroy(this.st_3)
destroy(this.dw_report_produzione)
destroy(this.st_4)
destroy(this.dw_report)
destroy(this.st_2)
destroy(this.em_num_copie)
destroy(this.st_1)
destroy(this.cb_conferma)
destroy(this.cb_chiudi)
end on

event open;call super::open;long			ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_quan_etichetta_produzione
string			ls_filename, ls_cod_reparto, ls_logo_etichetta, ls_cod_cliente
dec{4}		ld_quan_ordine


ii_ok = -1

// parametro che identifica l'applicazione per centro gibus
select flag
into   :is_alusistemi
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='ALU';

if sqlca.sqlcode < 0 then
	is_alusistemi = "N"
end if

// per alusistemi c'è un dataobject particolare.
if is_alusistemi = "S" then
	dw_report_produzione.dataobject = 'd_label_apertura_produzione_alu'
end if

s_cs_xx.parametri.parametro_ds_1.sharedata(dw_report)
s_cs_xx.parametri.parametro_ds_1.sharedata(dw_report_produzione)

select stringa
into   :ls_filename
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_parametro = 'LET';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("SEP","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if sqlca.sqlcode = 100 then
	g_mb.messagebox("SEP","Manca il parametro aziendale per il logo dell'etichetta. Creare un nuovo parametro aziendale con codice LET di tipo stringa e impostare il nome del file.", stopsign!)
end if

ls_filename = s_cs_xx.volume+ls_filename

dw_report.Object.p_logo.filename = ls_filename


// cerco la quantità del prodotto finito per impostare la quantità di etichette.
// enrico 20/03/2006

select anno_registrazione,
		 num_registrazione,
		 cod_reparto,
		 prog_riga_ord_ven
into   :ll_anno_registrazione,
		 :ll_num_registrazione,
		 :ls_cod_reparto,
		 :ll_prog_riga_ord_ven
from   det_ordini_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr_det_produzione=:s_cs_xx.parametri.parametro_ul_1;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if


select det_ord_ven.quan_ordine, tes_ord_ven.logo_etichetta, tes_ord_ven.cod_cliente
into   :ld_quan_ordine, :ls_logo_etichetta, :ls_cod_cliente
from   det_ord_ven
join tes_ord_ven on 	tes_ord_ven.cod_azienda=det_ord_ven.cod_azienda and
							tes_ord_ven.anno_registrazione=det_ord_ven.anno_registrazione and
							tes_ord_ven.num_registrazione=det_ord_ven.num_registrazione
where	det_ord_ven.cod_azienda=:s_cs_xx.cod_azienda and    
			det_ord_ven.anno_registrazione=:ll_anno_registrazione and
			det_ord_ven.num_registrazione=:ll_num_registrazione and
			det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if g_str.isnotempty(ls_logo_etichetta) then
	ls_logo_etichetta = s_cs_xx.volume + s_cs_xx.risorse + "loghi_clienti\"+ls_cod_cliente+"\" + ls_logo_etichetta
	
	if fileExists(ls_logo_etichetta) then
		dw_report_produzione.Object.p_logo.filename = ls_logo_etichetta
		dw_report.Object.p_logo.filename = ls_logo_etichetta
	end if
end if



select quan_etichetta_produzione
into  :ll_quan_etichetta_produzione
from  anag_reparti
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_reparto = :ls_cod_reparto;
if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if sqlca.sqlcode = 100 then 
	g_mb.messagebox("Sep","Attenzione: il reparto "+ls_cod_reparto+" è inesistente ",stopsign!)
	return -1
end if

string ls_ret
if s_cs_xx.parametri.parametro_s_1="A" then
	ls_ret = dw_report.Modify("t_stabilimento.text='"+s_cs_xx.parametri.parametro_s_14+"'")
	ls_ret = dw_report_produzione.Modify("t_stabilimento.text='"+s_cs_xx.parametri.parametro_s_14+"'")
	s_cs_xx.parametri.parametro_s_14 = ""
end if


em_num_copie.text = string(long(ll_quan_etichetta_produzione))


end event

event closequery;call super::closequery;if ii_ok=-1 then return 1

end event

event pc_setddlb;call super::pc_setddlb;//choose case s_cs_xx.parametri.parametro_s_1
//	
//	case "B"  // sfuso
//		f_po_loaddddw_dw(dw_tab_des, &
//                 "cod_des", &
//                 sqlca, &
//                 "tab_des_sfuso", &
//                 "cod_des_sfuso", &
//                 "des_sfuso", &
//					  "cod_azienda='" + s_cs_xx.cod_azienda +"'")
//		
//	case "C"  // tende tecniche
//		f_po_loaddddw_dw(dw_tab_des, &
//                 "cod_des", &
//                 sqlca, &
//                 "tab_des_tecniche", &
//                 "cod_des_tecniche", &
//                 "des_tecniche", &
//					  "cod_azienda='" + s_cs_xx.cod_azienda +"'")
//
//end choose
//
//
//
//
end event

event pc_setwindow;call super::pc_setwindow;//string ls_filename,ls_modify,li_risposta
//
//dw_tab_des.insertrow(0)
//
//
//
//
end event

type st_3 from statictext within w_stampa_etichette_apertura
integer x = 297
integer y = 516
integer width = 1824
integer height = 100
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "QUANTITA~' ETICHETTE APERTURA =  1"
boolean focusrectangle = false
end type

type dw_report_produzione from datawindow within w_stampa_etichette_apertura
boolean visible = false
integer x = 2341
integer y = 440
integer width = 1714
integer height = 340
integer taborder = 10
string title = "none"
string dataobject = "d_label_apertura_produzione"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type st_4 from statictext within w_stampa_etichette_apertura
integer x = 18
integer y = 252
integer width = 2939
integer height = 180
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 255
long backcolor = 12632256
string text = "APERTURA  /  PRODUZIONE"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_report from datawindow within w_stampa_etichette_apertura
boolean visible = false
integer x = 2341
integer y = 440
integer width = 1714
integer height = 340
integer taborder = 30
string title = "none"
string dataobject = "d_label_apertura"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type st_2 from statictext within w_stampa_etichette_apertura
integer x = 297
integer y = 712
integer width = 1806
integer height = 100
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "QUANTITA~' ETICHETTE PRODUZIONE ="
boolean focusrectangle = false
end type

type em_num_copie from editmask within w_stampa_etichette_apertura
integer x = 2130
integer y = 688
integer width = 347
integer height = 152
integer taborder = 10
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#"
boolean spin = true
end type

type st_1 from statictext within w_stampa_etichette_apertura
integer x = 14
integer y = 16
integer width = 2939
integer height = 196
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "STAMPA ETICHETTE"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_conferma from commandbutton within w_stampa_etichette_apertura
integer x = 311
integer y = 1000
integer width = 754
integer height = 220
integer taborder = 20
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
boolean default = true
end type

event clicked;integer						li_anno_registrazione

long							job,ll_num_copie,ll_t,ll_num_registrazione,ll_num_max_tenda,ll_num_tenda,ll_prog_riga_ord_ven, &
								ll_prog_riga_ord_ven_cfr,ll_quan_etichetta_produzione, ll_y,ld_quan_ordine
								
string							ls_drop_down,ls_cod_des,ls_cod_reparto,ls_flag_etichetta_apertura,ls_flag_etichetta_produzione, &
								ls_data_consegna, ls_rag_soc, ls_indirizzo_1, ls_indirizzo_2, ls_rag_soc_diversa, ls_ind_1_diversa, &
								ls_ind_2_diversa, ls_riga_n_di_m, ls_chiave_etic, ls_cod_cliente, ls_riferimento_cliente, ls_cod_prodotto, ls_cod_comodo
								

uo_produzione luo_prod


ll_num_copie = long (em_num_copie.text)

choose case s_cs_xx.parametri.parametro_s_1
	case 'A'  // tende da sole
		
		select anno_registrazione,
				 num_registrazione,
				 cod_reparto,
				 prog_riga_ord_ven
		into   :li_anno_registrazione,
				 :ll_num_registrazione,
				 :ls_cod_reparto,
				 :ll_prog_riga_ord_ven
		from   det_ordini_produzione
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    progr_det_produzione=:s_cs_xx.parametri.parametro_ul_1;
		
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore in ricerca dati av.prod.: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		select 	num_ord_cliente
		into   		:ls_riferimento_cliente
		from   	tes_ord_ven
		where  	cod_azienda = :s_cs_xx.cod_azienda and
		      		anno_registrazione = :li_anno_registrazione and
				 	num_registrazione = :ll_num_registrazione ;
				 
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		select 	quan_ordine, cod_prodotto
		into   		:ld_quan_ordine, :ls_cod_prodotto
		from   	det_ord_ven
		where  	cod_azienda = :s_cs_xx.cod_azienda and
		       		anno_registrazione = :li_anno_registrazione and
				 	num_registrazione = :ll_num_registrazione and
				 	prog_riga_ord_ven = :ll_prog_riga_ord_ven;
				 
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		select count(*)
		into   :ll_num_max_tenda
		from   det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:li_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    num_riga_appartenenza=0
		and    flag_blocco='N';
		
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		declare  righe_det_ord_ven_1 cursor for
		select   prog_riga_ord_ven
		from     det_ord_ven
		where    cod_azienda = :s_cs_xx.cod_azienda and      
		         anno_registrazione = :li_anno_registrazione and      
					num_registrazione = :ll_num_registrazione and      
					num_riga_appartenenza = 0 and      
					flag_blocco = 'N'
		order by prog_riga_ord_ven;
		
		open righe_det_ord_ven_1;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "SEP", "Errore apertura cursore: " + sqlca.sqlerrtext, stopsign!)
			return -1
		end if
		
		do while 1 = 1			
			
			fetch righe_det_ord_ven_1 into  :ll_prog_riga_ord_ven_cfr;
					
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close righe_det_ord_ven_1;
				return -1
			end if
			
			ll_num_tenda++				
		
			if ll_prog_riga_ord_ven = ll_prog_riga_ord_ven_cfr then exit
			
		loop
			
		close righe_det_ord_ven_1;
			
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		ls_riga_n_di_m = ""
		luo_prod = create uo_produzione
		ls_riga_n_di_m = luo_prod.uof_get_riga_su_tot_righe(li_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven)
		destroy luo_prod;
		
		s_cs_xx.parametri.parametro_ds_1.setitem(1,"num_riga_num_tot", ls_riga_n_di_m)
		//s_cs_xx.parametri.parametro_ds_1.setitem(1,"num_riga_num_tot", string(string(ll_num_tenda)) +"/" + string(ll_num_max_tenda))
		
	case 'B'  // sfuso
		select des_sfuso
		into   :ls_drop_down
		from   tab_des_sfuso
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_des_sfuso=:ls_cod_des;
		
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("SEP","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
	case 'C'  // tende tecniche
		select des_tecniche
		into   :ls_drop_down
		from   tab_des_tecniche
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_des_tecniche=:ls_cod_des;
		
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("SEP","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
end choose

if s_cs_xx.parametri.parametro_s_1 = 'B' or s_cs_xx.parametri.parametro_s_1 ='C' then
	s_cs_xx.parametri.parametro_ds_1.setitem(1,"drop_down",ls_drop_down)
end if

select quan_etichetta_produzione, 
       flag_etichetta_apertura,
		 flag_etichetta_produzione
into  :ll_quan_etichetta_produzione,
		:ls_flag_etichetta_apertura,
		:ls_flag_etichetta_produzione
from  anag_reparti
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_reparto = :ls_cod_reparto;
if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if
if sqlca.sqlcode = 100 then 
	g_mb.messagebox("Sep","Attenzione: il reparto "+ls_cod_reparto+" è inesistente ",stopsign!)
	return -1
end if

dw_report.Object.p_logo_piede.filename = s_cs_xx.volume + s_cs_xx.risorse + "eta" + ls_cod_reparto + ".bmp"

s_cs_xx.parametri.parametro_i_1 = 1

ii_ok=0

if is_alusistemi = "S" then
	dw_report_produzione.setitem(1,"rif_1", ls_riferimento_cliente)
	dw_report_produzione.setitem(1,"data_pronto", string(date(today()),"dd/mm/yy"))
	
	select 	cod_comodo
	into		:ls_cod_comodo
	from 		anag_prodotti
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto;
	
	dw_report_produzione.setitem(1,"des_prodotto_mod", ls_cod_comodo)
	
end if
	
/*
if is_cgibus = "S" then
	// per centro gibus niente nome del giorno
	ls_data_consegna = dw_report_produzione.getitemstring(1,"data_consegna")
	ls_data_consegna = mid(ls_data_consegna,4)
	dw_report_produzione.setitem(1,"data_consegna", ls_data_consegna)
	dw_report_produzione.setitem(1,"anno_registrazione", string(ll_prog_riga_ord_ven) )
	
	// Se c'è destinazione diversa stampo indirizzo destinazione; altrimenti stampo indirizzo normale.
	ls_rag_soc = dw_report_produzione.getitemstring(1,"rag_soc")
	ls_indirizzo_1 = 	dw_report_produzione.getitemstring(1,"indirizzo_1")
	ls_indirizzo_2 = 	dw_report_produzione.getitemstring(1,"indirizzo_2")

	ls_rag_soc_diversa = 	dw_report_produzione.getitemstring(1,"rag_soc_diversa")
	ls_ind_1_diversa = 	dw_report_produzione.getitemstring(1,"indirizzo_1_diversa")
	ls_ind_2_diversa = 	dw_report_produzione.getitemstring(1,"indirizzo_2_diversa")

	
	if isnull(ls_rag_soc_diversa)  or len(trim(ls_rag_soc_diversa))  < 1 then dw_report_produzione.setitem(1,"rag_soc_diversa", ls_rag_soc)
	if isnull(ls_ind_1_diversa) or len(trim(ls_ind_1_diversa)) < 1 then dw_report_produzione.setitem(1,"indirizzo_1_diversa", ls_indirizzo_1)
	if isnull(ls_ind_2_diversa) or len(trim(ls_ind_2_diversa)) < 1 then dw_report_produzione.setitem(1,"indirizzo_2_diversa", ls_indirizzo_2)
	
	
end if
*/
//---------------------------------------------------------------------------------------------
//Donato 14/02/2012
//colonna dinamica ETIC
select cod_cliente
into :ls_cod_cliente
from tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:li_anno_registrazione and
		num_registrazione=:ll_num_registrazione;

ls_chiave_etic = ""
if ls_cod_cliente<>"" and not isnull(ls_cod_cliente) then
	ls_chiave_etic = wf_get_colonna_dinamica(ls_cod_cliente)
end if
try
	dw_report_produzione.Modify("t_chiave.text='"+ ls_chiave_etic + "'")
catch (throwable err)
end try
//---------------------------------------------------------------------------------------------

job = PrintOpen( ) 
for ll_t = 1 to ld_quan_ordine
	
	try
		//-----------------------------------------------------
		if ls_flag_etichetta_apertura = "S" then
			//sempre 1 copia per ogni quantità ordine
				PrintDataWindow(job, dw_report) 
		end if
		
		if ls_flag_etichetta_produzione = "S" then
		//if (ls_flag_etichetta_produzione = "S" and not isnull(ls_flag_etichetta_produzione)) and (ls_flag_etichetta_apertura ="S" and not isnull(ls_flag_etichetta_apertura)) then
			for ll_y = 1 to ll_num_copie
			/*	if is_cgibus = "S" then
					dw_report_produzione.setitem(1,"num_riga_num_tot", string(ll_t) + "/" + string(integer(ld_quan_ordine)) )
				end if  */
				PrintDataWindow(job, dw_report_produzione) 
			next
		end if
		//-----------------------------------------------------
	catch (runtimeerror e)
	end try
	
next

PrintClose(job)

commit;


try
	close (parent)
catch (runtimeerror e2)
end try
end event

type cb_chiudi from commandbutton within w_stampa_etichette_apertura
event clicked pbm_bnclicked
integer x = 1851
integer y = 996
integer width = 782
integer height = 220
integer taborder = 10
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = 1
ii_ok = 0

commit;

try
	close (parent)
catch (runtimeerror e)
end try

end event

