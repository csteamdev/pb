﻿$PBExportHeader$w_stampa_etichette_ce.srw
forward
global type w_stampa_etichette_ce from w_std_principale
end type
type st_stampante from statictext within w_stampa_etichette_ce
end type
type st_1 from statictext within w_stampa_etichette_ce
end type
type cb_stampa from commandbutton within w_stampa_etichette_ce
end type
type dw_etichetta_ce from uo_std_dw within w_stampa_etichette_ce
end type
end forward

global type w_stampa_etichette_ce from w_std_principale
integer width = 2418
integer height = 1056
string title = "Certificazione"
st_stampante st_stampante
st_1 st_1
cb_stampa cb_stampa
dw_etichetta_ce dw_etichetta_ce
end type
global w_stampa_etichette_ce w_stampa_etichette_ce

type variables
s_stampa_etichette_ce istr_stampa_etichette_ce
end variables

forward prototypes
public function integer wf_report ()
public function integer wf_classe_gtot (decimal ad_gtot, ref long al_classe)
public function string wf_get_printer_name ()
end prototypes

public function integer wf_report ();string			ls_cod_prodotto, ls_des_prodotto, ls_des_breve, ls_cod_comodo, ls_cod_tessuto, ls_cod_colore_tessuto, ls_cod_verniciatura,ls_cod_comando,ls_pos_comando, ls_des_verniciatura, &
				ls_des_comando, ls_path_r1_c1, ls_path_r1_c2, ls_testo,ls_path_logo, ls_modify,ls_azienda_rag_soc_1,ls_azienda_indirizzo,ls_azienda_localita,ls_azienda_cap,ls_azienda_prov,&
				ls_dop_anno, ls_sql, ls_errore, ls_matricola
long			ll_row, ll_file, ll_ret, ll_classe, ll_rows, ll_i, ll_y, ll_job
dec{4}		ld_dim_x,ld_dim_y, ld_val_gtot, ld_classe_vento
datastore	lds_data


ll_rows = upperbound(istr_stampa_etichette_ce.anno_ordine)
if ll_rows = 0 then return 0

for ll_i = 1 to ll_rows

	
	ls_sql = "select " + & 
	"A.cod_prodotto, " + &
	"B.des_prodotto, " + &
	"B.des_breve,"  + &
	"B.cod_comodo, "+ &
	"B.cod_comodo, "+ &
	"COMP.cod_tessuto," + & 
	"COMP.cod_non_a_magazzino cod_colore_tessuto,  "+ &
	"COMP.dim_x,  "+ &
	"COMP.dim_y,  "+ &
	"COMP.cod_verniciatura,  "+ &
	"COMP.cod_comando,  "+ &
	"COMP.pos_comando,  "+ &
	"T.val_gtot,  "+ &
	"VERN.des_prodotto,  "+ &
	"COM.des_breve,  "+ &
	"B.cod_barre,  "+ &
	"AZI.rag_soc_1 ,  "+ &
	"AZI.indirizzo ,  "+ &
	"AZI.localita ,  "+ &
	"AZI.cap ,  "+ &
	"AZI.provincia,  "+ &
	"B.cod_comodo_2,  " + &
	"OLO.cod_ologramma,  " + &
	"F.flag_stampa_etichetta_ce  " + &
	"from det_ord_ven A  " + &
	"left outer join anag_prodotti B on A.cod_azienda = B.cod_azienda and A.cod_prodotto = B.cod_prodotto  " + &
	"left outer join comp_det_ord_ven COMP on A.cod_azienda=COMP.cod_azienda and A.anno_registrazione=COMP.anno_registrazione and A.num_registrazione=COMP.num_registrazione and A.prog_riga_ord_ven=COMP.prog_riga_ord_ven  " + &
	"left outer join tab_colori_tessuti T on COMP.cod_azienda=T.cod_azienda and COMP.cod_non_a_magazzino=T.cod_colore_tessuto  " + &
	"left outer join anag_prodotti VERN on COMP.cod_azienda=VERN.cod_azienda and COMP.cod_verniciatura=VERN.cod_prodotto  " + &
	"left outer join anag_prodotti COM on COMP.cod_azienda=COM.cod_azienda and COMP.cod_comando=COM.cod_prodotto  " + &
	"left outer join det_ord_ven_ologramma OLO on A.cod_azienda=OLO.cod_azienda and A.anno_registrazione=OLO.anno_registrazione and A.num_registrazione=OLO.num_registrazione and A.prog_riga_ord_ven=OLO.prog_riga_ord_ven  " + &
	"left outer join tab_flags_configuratore F on A.cod_azienda = F.cod_azienda and A.cod_prodotto = F.cod_modello  " + &
	"join aziende AZI on AZI.cod_azienda = A.cod_azienda  " + &
	g_str.format("where A.cod_azienda= '$1' and  ",s_cs_xx.cod_azienda) + &
	g_str.format("A.anno_registrazione = $1 and ",istr_stampa_etichette_ce.anno_ordine[ll_i])  + &
	g_str.format("A.num_registrazione = $1 and ",istr_stampa_etichette_ce.num_ordine[ll_i]) + &
	"(A.num_riga_appartenenza = 0 or A.num_riga_appartenenza is null)  "
	
	ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql, ls_errore)
	if ll_ret = 0 then
		g_mb.warning(g_str.format("Errore in caricamento dati del Report. Dati non trovati per l' ordine $1-$2",istr_stampa_etichette_ce.anno_ordine[ll_i],istr_stampa_etichette_ce.num_ordine[ll_i]))
		return -1
	end if
	
	if ll_ret < 0 then
		g_mb.error(g_str.format("Errore SQL nel caricamento dati del Report. Rif. ordine $1-$2 $3",istr_stampa_etichette_ce.anno_ordine[ll_i],istr_stampa_etichette_ce.num_ordine[ll_i], ls_errore))
		return -1
	end if
	
	
	ll_job=printopen("Stampa Dichiarazioni", false)
	if ll_job < 0 then return 0
	
	for ll_y = 1 to ll_ret
		
		if lds_data.getitemstring(ll_y,24) = "N" then continue
		
		dw_etichetta_ce.reset()
		
		dw_etichetta_ce.insertrow(0)
		
		dw_etichetta_ce.setitem(1,"anno_fabbricazione",g_str.format("Anno Prod: $1",  year(today())))
		
		dw_etichetta_ce.setitem(1,"costruttore_1","Costruttore: ALUSISTEMI s.r.l. - Via Fermi n.5 - CAP 35030")
		dw_etichetta_ce.setitem(1,"costruttore_2","Caselle di Selvazzano - Padova - Tel +39 049 630157")
		dw_etichetta_ce.setitem(1,"resitenza_vento",g_str.format("Resistenza carichi del vento: Classe $1", lds_data.getitemnumber(ll_y,16) ))
		ld_val_gtot = lds_data.getitemnumber(ll_y,13)
		dw_etichetta_ce.setitem(1,"trasmittanza",g_str.format("Trasmittanza totale di energia solare: $1",string(ld_val_gtot,"##0.00")) )
		dw_etichetta_ce.setitem(1,"comando", lds_data.getitemstring(ll_y,15) )
		dw_etichetta_ce.setitem(1,"modello",   g_str.format("Mod. $1", lds_data.getitemstring(ll_y,4))     )
		ls_matricola = lds_data.getitemstring(ll_y,23)
		dw_etichetta_ce.setitem(1,"matricola",g_str.format("Matr:$1",ls_matricola))
		
		guo_functions.uof_get_parametro_azienda("LC5", ls_path_logo)
		ls_modify = "p_alu.filename='" + s_cs_xx.volume + s_cs_xx.risorse + ls_path_logo + "'~t"
		
		guo_functions.uof_get_parametro_azienda("LC6", ls_path_logo)
		ls_modify += "p_ce.filename='" + s_cs_xx.volume + s_cs_xx.risorse + ls_path_logo + "'~t"
		dw_etichetta_ce.modify(ls_modify)
		
		printdatawindow(ll_job, dw_etichetta_ce)
		
	next
	
	PrintClose(ll_job)
	destroy lds_data
	
next
return 0
end function

public function integer wf_classe_gtot (decimal ad_gtot, ref long al_classe);if ad_gtot = 0 then return -1
choose case ad_gtot
	case is >= 0.50
		al_classe = 0
	case is >= 0.35
		al_classe = 1
	case is >= 0.15
		al_classe = 2
	case is >= 0.10
		al_classe = 3
	case else
		al_classe = 4
end choose
return 0

end function

public function string wf_get_printer_name ();string ls_printer
long ll_pos

ls_printer = PrintGetPrinter ( )
	
do while true
	ll_pos = pos(ls_printer, "~t")
	if ll_pos < 1 then exit
	ls_printer = Replace (ls_printer, ll_pos, 1, " / ")
loop

if isnull(ls_printer) then return ""

if ls_printer<>"" then ls_printer = " - Stampa su " + ls_printer

return ls_printer
end function

on w_stampa_etichette_ce.create
int iCurrent
call super::create
this.st_stampante=create st_stampante
this.st_1=create st_1
this.cb_stampa=create cb_stampa
this.dw_etichetta_ce=create dw_etichetta_ce
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_stampante
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.cb_stampa
this.Control[iCurrent+4]=this.dw_etichetta_ce
end on

on w_stampa_etichette_ce.destroy
call super::destroy
destroy(this.st_stampante)
destroy(this.st_1)
destroy(this.cb_stampa)
destroy(this.dw_etichetta_ce)
end on

event open;call super::open;string		ls_printer
istr_stampa_etichette_ce = message.powerobjectparm

dw_etichetta_ce.ib_dw_report=true

guo_functions.uof_get_parametro_azienda("ST1", ls_printer)

if PrintSetPrinter ( ls_printer ) < 0 then
	g_mb.warning("La stampante richiesta " + ls_printer + " non è stata trovata")
	st_stampante.text = wf_get_printer_name( )
	return
end if

st_stampante.text = ls_printer

//wf_report()
end event

event activate;call super::activate;st_stampante.text = wf_get_printer_name( )
end event

type st_stampante from statictext within w_stampa_etichette_ce
integer y = 180
integer width = 2354
integer height = 100
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "PrinterName"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_1 from statictext within w_stampa_etichette_ce
integer x = 23
integer y = 20
integer width = 2331
integer height = 160
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Gestione Etichette"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_stampa from commandbutton within w_stampa_etichette_ce
integer x = 1006
integer y = 320
integer width = 389
integer height = 100
integer taborder = 10
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;//long ll_job

//ll_job=printopen("Stampa Dichiarazioni", TRUE)
//printdatawindow(ll_job, dw_etichetta_ce)
//if ll_job < 0 then return


//PrintClose(ll_job)

wf_report( )

end event

type dw_etichetta_ce from uo_std_dw within w_stampa_etichette_ce
integer x = 617
integer y = 460
integer width = 1211
integer height = 460
integer taborder = 10
string dataobject = "d_report_etichetta_ce"
boolean livescroll = false
end type

