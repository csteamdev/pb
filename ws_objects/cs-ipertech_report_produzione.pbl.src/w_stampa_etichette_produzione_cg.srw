﻿$PBExportHeader$w_stampa_etichette_produzione_cg.srw
$PBExportComments$Finestra per la stampa delle etichette di produzione
forward
global type w_stampa_etichette_produzione_cg from w_cs_xx_risposta
end type
type dw_report_produzione from uo_cs_xx_dw within w_stampa_etichette_produzione_cg
end type
type dw_report from uo_cs_xx_dw within w_stampa_etichette_produzione_cg
end type
type st_2 from statictext within w_stampa_etichette_produzione_cg
end type
type em_num_copie from editmask within w_stampa_etichette_produzione_cg
end type
type st_1 from statictext within w_stampa_etichette_produzione_cg
end type
type cb_conferma from commandbutton within w_stampa_etichette_produzione_cg
end type
type cb_chiudi from commandbutton within w_stampa_etichette_produzione_cg
end type
end forward

global type w_stampa_etichette_produzione_cg from w_cs_xx_risposta
integer width = 3031
integer height = 1420
string title = "STAMPA ETICHETTE PRODUZIONE"
boolean controlmenu = false
dw_report_produzione dw_report_produzione
dw_report dw_report
st_2 st_2
em_num_copie em_num_copie
st_1 st_1
cb_conferma cb_conferma
cb_chiudi cb_chiudi
end type
global w_stampa_etichette_produzione_cg w_stampa_etichette_produzione_cg

type variables
integer ii_ok
string is_cgibus
end variables

on w_stampa_etichette_produzione_cg.create
int iCurrent
call super::create
this.dw_report_produzione=create dw_report_produzione
this.dw_report=create dw_report
this.st_2=create st_2
this.em_num_copie=create em_num_copie
this.st_1=create st_1
this.cb_conferma=create cb_conferma
this.cb_chiudi=create cb_chiudi
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report_produzione
this.Control[iCurrent+2]=this.dw_report
this.Control[iCurrent+3]=this.st_2
this.Control[iCurrent+4]=this.em_num_copie
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.cb_conferma
this.Control[iCurrent+7]=this.cb_chiudi
end on

on w_stampa_etichette_produzione_cg.destroy
call super::destroy
destroy(this.dw_report_produzione)
destroy(this.dw_report)
destroy(this.st_2)
destroy(this.em_num_copie)
destroy(this.st_1)
destroy(this.cb_conferma)
destroy(this.cb_chiudi)
end on

event open;call super::open;long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_quan_etichetta_produzione
string ls_filename, ls_cod_reparto
dec{4} ld_quan_ordine
ii_ok = -1

// parametro che identifica l'applicazione per centro gibus
select flag
into   :is_cgibus
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='CGH';

if sqlca.sqlcode < 0 then
	is_cgibus = "N"
end if

// per centro gibus c'è un dataobject particolare.
if is_cgibus = "S" then
	dw_report_produzione.dataobject = 'd_label_apertura_produzione_cgibus'
end if

s_cs_xx.parametri.parametro_ds_1.sharedata(dw_report)
s_cs_xx.parametri.parametro_ds_1.sharedata(dw_report_produzione)

select stringa
into   :ls_filename
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_parametro = 'LET';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("SEP","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if sqlca.sqlcode = 100 then
	g_mb.messagebox("SEP","Manca il parametro aziendale per il logo dell'etichetta. Creare un nuovo parametro aziendale con codice LET di tipo stringa e impostare il nome del file.", stopsign!)
end if

ls_filename = s_cs_xx.volume+ls_filename

dw_report.Object.p_logo.filename = ls_filename

// Per centro gibus niente logo da immagine
if is_cgibus = "N" then
	dw_report_produzione.Object.p_logo.filename = ls_filename
end if


// cerco la quantità del prodotto finito per impostare la quantità di etichette.
// enrico 20/03/2006

select anno_registrazione,
		 num_registrazione,
		 cod_reparto,
		 prog_riga_ord_ven
into   :ll_anno_registrazione,
		 :ll_num_registrazione,
		 :ls_cod_reparto,
		 :ll_prog_riga_ord_ven
from   det_ordini_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    progr_det_produzione=:s_cs_xx.parametri.parametro_ul_1;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if


select quan_ordine
into   :ld_quan_ordine
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda and    
       anno_registrazione=:ll_anno_registrazione and
		 num_registrazione=:ll_num_registrazione and
		 prog_riga_ord_ven = :ll_prog_riga_ord_ven;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select quan_etichetta_produzione
into  :ll_quan_etichetta_produzione
from  anag_reparti
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_reparto = :ls_cod_reparto;
if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if sqlca.sqlcode = 100 then 
	g_mb.messagebox("Sep","Attenzione: il reparto "+ls_cod_reparto+" è inesistente ",stopsign!)
	return -1
end if



em_num_copie.text = string(long(ll_quan_etichetta_produzione))


end event

event closequery;call super::closequery;if ii_ok=-1 then return 1

end event

event pc_setddlb;call super::pc_setddlb;//choose case s_cs_xx.parametri.parametro_s_1
//	
//	case "B"  // sfuso
//		f_po_loaddddw_dw(dw_tab_des, &
//                 "cod_des", &
//                 sqlca, &
//                 "tab_des_sfuso", &
//                 "cod_des_sfuso", &
//                 "des_sfuso", &
//					  "cod_azienda='" + s_cs_xx.cod_azienda +"'")
//		
//	case "C"  // tende tecniche
//		f_po_loaddddw_dw(dw_tab_des, &
//                 "cod_des", &
//                 sqlca, &
//                 "tab_des_tecniche", &
//                 "cod_des_tecniche", &
//                 "des_tecniche", &
//					  "cod_azienda='" + s_cs_xx.cod_azienda +"'")
//
//end choose
//
//
//
//
end event

event pc_setwindow;call super::pc_setwindow;dw_report_produzione.ib_dw_report = true
dw_report.ib_dw_report = true
end event

type dw_report_produzione from uo_cs_xx_dw within w_stampa_etichette_produzione_cg
boolean visible = false
integer x = 2341
integer y = 440
integer width = 1714
integer height = 340
integer taborder = 10
string title = "none"
string dataobject = "d_label_apertura_produzione"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_report from uo_cs_xx_dw within w_stampa_etichette_produzione_cg
boolean visible = false
integer x = 2341
integer y = 440
integer width = 1714
integer height = 340
integer taborder = 30
string title = "none"
string dataobject = "d_label_apertura"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type st_2 from statictext within w_stampa_etichette_produzione_cg
integer x = 78
integer y = 620
integer width = 2021
integer height = 152
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Q.TA~' Etichette Produzione per COLLO ="
alignment alignment = right!
boolean focusrectangle = false
end type

type em_num_copie from editmask within w_stampa_etichette_produzione_cg
integer x = 2126
integer y = 620
integer width = 347
integer height = 152
integer taborder = 10
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#"
boolean spin = true
end type

type st_1 from statictext within w_stampa_etichette_produzione_cg
integer width = 2994
integer height = 200
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "STAMPA ETICHETTE PRODUZIONE"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_conferma from commandbutton within w_stampa_etichette_produzione_cg
integer x = 311
integer y = 1004
integer width = 754
integer height = 220
integer taborder = 20
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
boolean default = true
end type

event clicked;integer li_anno_registrazione
long job,ll_num_copie,ll_t,ll_num_registrazione,ll_num_max_tenda,ll_num_tenda,ll_prog_riga_ord_ven, &
     ll_prog_riga_ord_ven_cfr,ll_quan_etichetta_produzione, ll_y,ld_quan_ordine
string ls_drop_down,ls_cod_des,ls_cod_reparto,ls_flag_etichetta_produzione, &
		 ls_data_consegna, ls_rag_soc, ls_indirizzo_1, ls_indirizzo_2, ls_rag_soc_diversa, ls_ind_1_diversa, &
		 ls_ind_2_diversa
long ll_barcode
string ls_barcode, ls_CSB, ls_colli_v[], ls_msg, ls_vuoto[]
uo_produzione luo_prod
long ll_i, ll_tot_colli

ll_num_copie = long (em_num_copie.text)

ll_barcode = s_cs_xx.parametri.parametro_ul_1

//lettura parametro aziendale Carattere Speciale barcode (specifica Carico ordini con palmare)
select stringa
into   :ls_CSB
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='CSB' and 
		flag_parametro='S';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep","Errore in ricerca del parametro CSB in tabella parametri azienda: " + sqlca.sqlerrtext,stopsign!)
	return -1
	
elseif sqlca.sqlcode=100 or isnull(ls_CSB) or ls_CSB = "" then	
	ls_CSB = ""
end if
//------------------------------------------------------------------------------------------------------------------------------------


choose case s_cs_xx.parametri.parametro_s_1
	case 'A'  // tende da sole
		
		select anno_registrazione,
				 num_registrazione,
				 cod_reparto,
				 prog_riga_ord_ven
		into   :li_anno_registrazione,
				 :ll_num_registrazione,
				 :ls_cod_reparto,
				 :ll_prog_riga_ord_ven
		from   det_ordini_produzione
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    progr_det_produzione=:s_cs_xx.parametri.parametro_ul_1;
		
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore in ricerca dati av.prod.: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		select quan_ordine
		into   :ld_quan_ordine
		from   det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :li_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
				 
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		select count(*)
		into   :ll_num_max_tenda
		from   det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:li_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    num_riga_appartenenza=0
		and    flag_blocco='N';
		
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		declare  righe_det_ord_ven_1 cursor for
		select   prog_riga_ord_ven
		from     det_ord_ven
		where    cod_azienda = :s_cs_xx.cod_azienda and      
		         anno_registrazione = :li_anno_registrazione and      
					num_registrazione = :ll_num_registrazione and      
					num_riga_appartenenza = 0 and      
					flag_blocco = 'N'
		order by prog_riga_ord_ven;
		
		open righe_det_ord_ven_1;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "SEP", "Errore apertura cursore: " + sqlca.sqlerrtext, stopsign!)
			return -1
		end if
		
		do while 1 = 1			
			
			fetch righe_det_ord_ven_1 into  :ll_prog_riga_ord_ven_cfr;
					
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close righe_det_ord_ven_1;
				return -1
			end if
			
			ll_num_tenda++				
		
			if ll_prog_riga_ord_ven = ll_prog_riga_ord_ven_cfr then exit
			
		loop
			
		close righe_det_ord_ven_1;
			
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		s_cs_xx.parametri.parametro_ds_1.setitem(1,"num_riga_num_tot", string(string(ll_num_tenda)) +"/" + string(ll_num_max_tenda))
		
	case 'B'  // sfuso
		select des_sfuso
		into   :ls_drop_down
		from   tab_des_sfuso
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_des_sfuso=:ls_cod_des;
		
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("SEP","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
	case 'C'  // tende tecniche
		select des_tecniche
		into   :ls_drop_down
		from   tab_des_tecniche
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_des_tecniche=:ls_cod_des;
		
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("SEP","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
end choose

if s_cs_xx.parametri.parametro_s_1 = 'B' or s_cs_xx.parametri.parametro_s_1 ='C' then
	s_cs_xx.parametri.parametro_ds_1.setitem(1,"drop_down",ls_drop_down)
end if


ls_barcode = "*"+ls_CSB+string(ll_barcode)+ls_CSB+"*"
s_cs_xx.parametri.parametro_ds_1.setitem(1,"barcode",ls_barcode)



select quan_etichetta_produzione, 
       flag_etichetta_produzione
into  :ll_quan_etichetta_produzione,
		:ls_flag_etichetta_produzione
from  anag_reparti
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_reparto = :ls_cod_reparto;
if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if
if sqlca.sqlcode = 100 then 
	g_mb.messagebox("Sep","Attenzione: il reparto "+ls_cod_reparto+" è inesistente ",stopsign!)
	return -1
end if

dw_report.Object.p_logo_piede.filename = s_cs_xx.volume + s_cs_xx.risorse + "eta" + ls_cod_reparto + ".bmp"

s_cs_xx.parametri.parametro_i_1 = 1

ii_ok=0

// per centro gibus niente nome del giorno
ls_data_consegna = dw_report_produzione.getitemstring(1,"data_consegna")
ls_data_consegna = mid(ls_data_consegna,4)
dw_report_produzione.setitem(1,"data_consegna", ls_data_consegna)
dw_report_produzione.setitem(1,"anno_registrazione", string(ll_prog_riga_ord_ven) )

// Se c'è destinazione diversa stampo indirizzo destinazione; altrimenti stampo indirizzo normale.
ls_rag_soc = dw_report_produzione.getitemstring(1,"rag_soc")
ls_indirizzo_1 = 	dw_report_produzione.getitemstring(1,"indirizzo_1")
ls_indirizzo_2 = 	dw_report_produzione.getitemstring(1,"indirizzo_2")

ls_rag_soc_diversa = 	dw_report_produzione.getitemstring(1,"rag_soc_diversa")
ls_ind_1_diversa = 	dw_report_produzione.getitemstring(1,"indirizzo_1_diversa")
ls_ind_2_diversa = 	dw_report_produzione.getitemstring(1,"indirizzo_2_diversa")


if isnull(ls_rag_soc_diversa)  or len(trim(ls_rag_soc_diversa))  < 1 then dw_report_produzione.setitem(1,"rag_soc_diversa", ls_rag_soc)
if isnull(ls_ind_1_diversa) or len(trim(ls_ind_1_diversa)) < 1 then dw_report_produzione.setitem(1,"indirizzo_1_diversa", ls_indirizzo_1)
if isnull(ls_ind_2_diversa) or len(trim(ls_ind_2_diversa)) < 1 then dw_report_produzione.setitem(1,"indirizzo_2_diversa", ls_indirizzo_2)



// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//se ci sono colli in tab_ord_ven_colli allora stampa quelli, altrimenti procedi tradizionalmente
//recupero i colli presenti, sia in caso di avanzamento produzione che semplice ristampa
//infatti nel caso di avanzamento prod. i colli vengono stampati tutti
luo_prod = create uo_produzione

if luo_prod.uof_carica_colli_barcode(s_cs_xx.parametri.parametro_ul_1, ls_colli_v[], ls_cod_reparto, ls_msg) < 0 then
	//in ls_msg l'errore
	destroy luo_prod;
	
	g_mb.error(ls_msg)
	return -1
end if

destroy luo_prod;

//recupero il campo sigla dal reparto
string ls_sigla

select sigla
into :ls_sigla
from anag_reparti
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_reparto=:ls_cod_reparto;

if ls_sigla<>"" and not isnull(ls_sigla) then ls_cod_reparto = ls_sigla

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



//Donato 14/03/2011 la funzione chiamata per la ristampa è la stessa che viene usata dopo la chiusura della produzione
//quindi occorre distinguere, perchè nel casi di ristampa e in presenza di + colli, l'operatore deve poter scegliere quali etichette
//(cioè di quali colli) ristampare
ll_tot_colli = upperbound(ls_colli_v[])

if s_cs_xx.parametri.parametro_s_11 = "RISTAMPA" and ll_tot_colli>0 then
	//apri un finestra di selezione colli da stampare
	s_cs_xx.parametri.parametro_s_4_a[] = ls_colli_v[]
	
	window_open(w_sel_colli_stampa, 0)
	
	//effettuo il travaso dei colli selezionati per la ristampa
	ls_colli_v[] = s_cs_xx.parametri.parametro_s_4_a[]
	
	//reset del vettore per sicurezza
	s_cs_xx.parametri.parametro_s_4_a[] = ls_vuoto[]
	
	//reset del semaforo per sicurezza
	s_cs_xx.parametri.parametro_s_11 = ""
	
	//potrei aver selezionato NESSUN collo da ristampare
	if upperbound(ls_colli_v) = 0 then
		commit;
		close (parent)
		return
	end if
end if



job = PrintOpen( ) 

if upperbound(ls_colli_v[]) > 0 then
	
	//stampa il barcode + il progressivo collo
	for ll_i = 1 to upperbound(ls_colli_v)
		for ll_t = 1 to ll_num_copie
			//stampa         * [CSB] [BARCODE] - [i] [CSB] *
			//s_cs_xx.parametri.parametro_ds_1.setitem(1,"barcode", ls_colli_v[ll_i])
			//s_cs_xx.parametri.parametro_ds_1.setitem(1,"num_colli",string(ll_tot_colli))
			dw_report_produzione.setitem(1,"barcode", ls_colli_v[ll_i])
			dw_report_produzione.setitem(1,"num_colli",string(ll_tot_colli))
			
			
			//campo sigla di anag_reparti=numero colli per reparto
			dw_report_produzione.object.t_reparto.text = ls_cod_reparto + "="+string(ll_tot_colli)
			dw_report_produzione.object.t_6.text = "COLLI"
			
			dw_report_produzione.setitem(1,"num_riga_num_tot", string(ll_i) + "/" + string(upperbound(ls_colli_v)) )
			PrintDataWindow(job, dw_report_produzione)
		next
	next
	
else
	
	//procedi tradizionalmente -----
	for ll_t = 1 to ld_quan_ordine
	
		if ls_flag_etichetta_produzione = "S" then
			for ll_y = 1 to ll_num_copie
				if is_cgibus = "S" then
					dw_report_produzione.setitem(1,"num_riga_num_tot", string(ll_t) + "/" + string(integer(ld_quan_ordine)) )
				end if
				PrintDataWindow(job, dw_report_produzione) 
			next
		end if
		
	next
	
end if


PrintClose(job)

commit;

close (parent)

end event

type cb_chiudi from commandbutton within w_stampa_etichette_produzione_cg
event clicked pbm_bnclicked
integer x = 1851
integer y = 996
integer width = 782
integer height = 220
integer taborder = 10
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = 1
ii_ok = 0

commit;

close (parent)
end event

