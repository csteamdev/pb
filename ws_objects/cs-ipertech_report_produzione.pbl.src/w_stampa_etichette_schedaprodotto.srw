﻿$PBExportHeader$w_stampa_etichette_schedaprodotto.srw
$PBExportComments$Finestra per la stampa delle etichette di produzione
forward
global type w_stampa_etichette_schedaprodotto from w_cs_xx_risposta
end type
type st_2 from statictext within w_stampa_etichette_schedaprodotto
end type
type st_1 from statictext within w_stampa_etichette_schedaprodotto
end type
type cb_conferma from commandbutton within w_stampa_etichette_schedaprodotto
end type
type cb_chiudi from commandbutton within w_stampa_etichette_schedaprodotto
end type
type em_num_copie from editmask within w_stampa_etichette_schedaprodotto
end type
type dw_report from datawindow within w_stampa_etichette_schedaprodotto
end type
end forward

global type w_stampa_etichette_schedaprodotto from w_cs_xx_risposta
integer width = 3031
integer height = 1004
string title = "STAMPA SCHEDA PRODOTTO"
boolean controlmenu = false
st_2 st_2
st_1 st_1
cb_conferma cb_conferma
cb_chiudi cb_chiudi
em_num_copie em_num_copie
dw_report dw_report
end type
global w_stampa_etichette_schedaprodotto w_stampa_etichette_schedaprodotto

type variables
integer			ii_ok
string				is_path_etichetta_standard
end variables

forward prototypes
public function integer wf_log_sistema (long fl_sqlcode, long fl_anno_reg, long fl_num_reg, long fl_num_riga)
end prototypes

public function integer wf_log_sistema (long fl_sqlcode, long fl_anno_reg, long fl_num_reg, long fl_num_riga);
string ls_sqlerrtext = ""
string ls_messaggio,ls_flag_blocco_cu
long ll_id_sistema,ll_prog_riga_cu,ll_num_riga_app_cu
string ls_tipo_log = "ETICHETTE"
integer li_sqlcode = 100
datetime ldt_today, ldt_now

if fl_sqlcode=100 then
else
	//esci dalla funzione
	return 1
end if

//se arrivi fin qui vuol dire che sqlcode è 100: log in tabella

ls_sqlerrtext = string(fl_anno_reg)+"/"
ls_sqlerrtext += string(fl_num_reg)+"/"
ls_sqlerrtext += string(fl_num_riga)


declare  righe_log_sistema cursor for
select   prog_riga_ord_ven,flag_blocco,num_riga_appartenenza
from     det_ord_ven
where    cod_azienda = :s_cs_xx.cod_azienda and      
			anno_registrazione = :fl_anno_reg and      
			num_registrazione = :fl_num_reg
order by prog_riga_ord_ven;

open righe_log_sistema;
if sqlca.sqlcode <> 0 then
	return -1
end if

do while 1 = 1			
	
	fetch righe_log_sistema into  :ll_prog_riga_cu,:ls_flag_blocco_cu,:ll_num_riga_app_cu;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		close righe_log_sistema;
		return -1
	end if
	
	//calcola il max
	select max(id_log_sistema)
	into :ll_id_sistema
	from log_sistema;
	
	if sqlca.sqlcode < 0 then
		close righe_log_sistema;
		return -1
		
	elseif sqlca.sqlcode = 100 or isnull(ll_id_sistema) or ll_id_sistema < 1 then 
		ll_id_sistema = 0
	end if
	
	ll_id_sistema++
	ldt_today = datetime(today(), 00:00:00)
	ldt_now = datetime(date("01/01/1900"), now())
	if len(ls_tipo_log) > 20 then left(ls_tipo_log, 20)
	
	if isnull(ll_prog_riga_cu) then
		ls_messaggio = "riga=NULL "
	else
		ls_messaggio = "riga="+string(ll_prog_riga_cu)+" "
	end if
	
	if isnull(ls_flag_blocco_cu) then ls_flag_blocco_cu="NULL"
	ls_messaggio += "blocco="+ls_flag_blocco_cu+" "
	
	if isnull(ll_num_riga_app_cu) then
		ls_messaggio += "n_riga_app=NULL "
	else
		ls_messaggio += "n_riga_app="+string(ll_num_riga_app_cu)+" "
	end if
	
	insert into log_sistema (
		id_log_sistema,
		data_registrazione,
		ora_registrazione,
		utente,
		flag_tipo_log,
		db_sqlcode,
		db_sqlerrtext,
		messaggio)
	values (
		:ll_id_sistema,
		:ldt_today,
		:ldt_now,
		:s_cs_xx.cod_utente,
		:ls_tipo_log,
		:li_sqlcode,
		:ls_sqlerrtext,
		:ls_messaggio);
	
	if sqlca.sqlcode <> 0 then
		close righe_log_sistema;
		return -1
	end if
	
loop
	
close righe_log_sistema;

return 1
end function

on w_stampa_etichette_schedaprodotto.create
int iCurrent
call super::create
this.st_2=create st_2
this.st_1=create st_1
this.cb_conferma=create cb_conferma
this.cb_chiudi=create cb_chiudi
this.em_num_copie=create em_num_copie
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_2
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.cb_conferma
this.Control[iCurrent+4]=this.cb_chiudi
this.Control[iCurrent+5]=this.em_num_copie
this.Control[iCurrent+6]=this.dw_report
end on

on w_stampa_etichette_schedaprodotto.destroy
call super::destroy
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_conferma)
destroy(this.cb_chiudi)
destroy(this.em_num_copie)
destroy(this.dw_report)
end on

event open;call super::open;string ls_filename, ls_msg
integer li_tot_colli
long ll_quan_ordine

ii_ok = -1

choose case s_cs_xx.parametri.parametro_s_1
	case 'A'  // tende da sole
		
	case else
		//esci
		cb_chiudi.triggerevent(clicked!)
		return
		
end choose

s_cs_xx.parametri.parametro_ds_1.sharedata(dw_report)

if s_cs_xx.parametri.parametro_d_9 = 1 then
	//provengo da stampa per codice ologramma
	ll_quan_ordine = 1
else
	//chiesto da Beatrice: fattore 2 per numero copie scheda prodotto
	ll_quan_ordine = long(dw_report.getitemstring(1, "quan_ordine")) * 2
end if

em_num_copie.text = string(ll_quan_ordine)
//--------------------------


select stringa
into   :ls_filename
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_parametro = 'LET';

if sqlca.sqlcode < 0 then
	g_mb.error("SEP","Errore sul DB: " + sqlca.sqlerrtext)
	return -1
end if

if sqlca.sqlcode = 100 then
	g_mb.show("SEP","Manca il param.az. per il logo etichetta: (codice LET di tipo stringa valore: path del file escluso [volume:])")
end if

is_path_etichetta_standard = s_cs_xx.volume + ls_filename
dw_report.Object.p_logo.filename = is_path_etichetta_standard

cb_conferma.setfocus( )



end event

event closequery;call super::closequery;if ii_ok=-1 then return 1

end event

event pc_setwindow;call super::pc_setwindow;cb_conferma.setfocus( )


end event

type st_2 from statictext within w_stampa_etichette_schedaprodotto
integer x = 544
integer y = 352
integer width = 1417
integer height = 96
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Q.tà etichette Scheda Prodotto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_stampa_etichette_schedaprodotto
integer x = 23
integer y = 20
integer width = 2962
integer height = 180
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 67108864
string text = "STAMPA SCHEDA PRODOTTO"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_conferma from commandbutton within w_stampa_etichette_schedaprodotto
integer x = 1545
integer y = 572
integer width = 754
integer height = 220
integer taborder = 20
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
boolean default = true
end type

event clicked;long		job, ll_num_copie, ll_index, ll_index2
integer	li_numero_schede
string		ls_logo_etichetta, ls_cod_cliente


ll_num_copie = long (em_num_copie.text)
s_cs_xx.parametri.parametro_i_1 = 1

ii_ok=0

job = PrintOpen( ) 


li_numero_schede = dw_report.rowcount()

for ll_index2=1 to li_numero_schede
	ls_logo_etichetta = dw_report.getitemstring(ll_index2, "logo_etichetta")
	ls_cod_cliente = dw_report.getitemstring(ll_index2, "cod_cliente")
	
	dw_report.setfilter("numero="+string(ll_index2))
	dw_report.filter()

	dw_report.Object.p_logo.filename = is_path_etichetta_standard
	

	if g_str.isnotempty(ls_logo_etichetta) then
		ls_logo_etichetta = s_cs_xx.volume + s_cs_xx.risorse + "loghi_clienti\"+ls_cod_cliente+"\" + ls_logo_etichetta
		if fileExists(ls_logo_etichetta) then
			dw_report.Object.p_logo.filename = ls_logo_etichetta
		end if
	end if

	for ll_index = 1 to ll_num_copie
		PrintDataWindow(job, dw_report)
	next
	
next

PrintClose(job)

commit;

close (parent)

end event

type cb_chiudi from commandbutton within w_stampa_etichette_schedaprodotto
event clicked pbm_bnclicked
integer x = 768
integer y = 572
integer width = 754
integer height = 220
integer taborder = 10
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C&hiudi"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = 1
ii_ok = 0

commit;

close (parent)
end event

type em_num_copie from editmask within w_stampa_etichette_schedaprodotto
integer x = 2011
integer y = 344
integer width = 297
integer height = 116
integer taborder = 10
boolean bringtotop = true
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#"
boolean spin = true
end type

type dw_report from datawindow within w_stampa_etichette_schedaprodotto
boolean visible = false
integer x = 101
integer y = 432
integer width = 1714
integer height = 340
integer taborder = 30
string title = "none"
string dataobject = "d_label_scheda_prodotto"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

