﻿$PBExportHeader$w_stampa_etichette_sfuso.srw
forward
global type w_stampa_etichette_sfuso from w_cs_xx_risposta
end type
type st_log from statictext within w_stampa_etichette_sfuso
end type
type dw_list from uo_std_dw within w_stampa_etichette_sfuso
end type
type st_1 from statictext within w_stampa_etichette_sfuso
end type
type cb_stampa from commandbutton within w_stampa_etichette_sfuso
end type
type dw_print from uo_std_dw within w_stampa_etichette_sfuso
end type
end forward

global type w_stampa_etichette_sfuso from w_cs_xx_risposta
integer width = 2999
integer height = 1588
string title = "Stampa Etichette Sfuso"
st_log st_log
dw_list dw_list
st_1 st_1
cb_stampa cb_stampa
dw_print dw_print
end type
global w_stampa_etichette_sfuso w_stampa_etichette_sfuso

type variables
private:
	long il_anno_registrazione, il_num_registrazione
	string is_des_cliente
end variables

forward prototypes
public function integer wf_traduci_prodotti ()
public function integer wf_carica_logo ()
public function integer wf_pre_stampa ()
public function integer wf_imposta_barcode (long ll_anno, long ll_numero, ref string fs_barcode)
public function string wf_logo ()
end prototypes

public function integer wf_traduci_prodotti ();/**
 * Stefano
 * 13/05/2010
 *
 * Traduco la descrizione dei prodottin in base alla lingua del cliente,
 * se vuota non devo tradurre nulla
 **/
 
string ls_cod_cliente, ls_cod_lingua, ls_cod_prodotto, ls_des_prodotto, ls_cod_misura, ls_des_misura
long ll_i, ll_n_etichette

// CLIENTE
select cod_cliente
into :ls_cod_cliente
from tes_ord_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :il_anno_registrazione and
	num_registrazione = :il_num_registrazione;
	
if sqlca.sqlcode < 0 then
	g_mb.error("SEP", "Errore durante il recupero cliente dalla testa ordine.~r~n" + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 or (sqlca.sqlcode = 0 and isnull(ls_cod_cliente)) then
	g_mb.error("SEP", "Errore durante il recupero cliente dalla testa ordine.")
	return -1
end if
// ----

// LINGUA
select cod_lingua, rag_soc_1
into :ls_cod_lingua, :is_des_cliente
from anag_clienti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cliente = :ls_cod_cliente;
	
if sqlca.sqlcode < 0 then
	g_mb.error("SEP", "Errore durante il recupero della lingua del cliente " + ls_cod_cliente + ".~r~n" + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 then
	g_mb.error("SEP", "Cliente " + ls_cod_cliente + " non trovato all'interno del database")
	return -1
end if

if isnull(ls_cod_lingua) or ls_cod_lingua = "" then // se nullo allora non devo tradurre nulla
	return 0
end if
// ----

// nessuna riga, esco
if dw_list.rowcount() < 1 then return 0

for ll_i = 1 to dw_list.rowcount()
	ls_cod_prodotto = dw_list.getitemstring(ll_i, "cod_prodotto")
	ls_cod_misura = dw_list.getitemstring(ll_i, "cod_misura")
	ll_n_etichette = dw_list.getitemnumber(ll_i, "n_etichette")
	
	// se non è da stampare non traduco
	if ll_n_etichette < 1 then continue
	
	// DES PRODOTTO
	select des_prodotto
	into :ls_des_prodotto
	from anag_prodotti_lingue
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :ls_cod_prodotto and
		cod_lingua = :ls_cod_lingua;
		
	if sqlca.sqlcode < 0 then
		// errore traduzione
		g_mb.error("SEP", "Errore durante la traduzione del prodotto " + ls_cod_prodotto + " nella lingua " + ls_cod_lingua)
		return -1
	elseif sqlca.sqlcode = 100 then
		// Richiesto da valerio, se non c' la traduzione lascio quella in Italiano e non chiedo nulla all'utente
		/*
		// chiedo se continuare o bloccare
		if not g_mb.confirm("SEP", "Attenzione: il prodotto " + ls_cod_prodotto + " non ha nessuna traduzione per la lingua " + ls_cod_lingua + ", Continuare il processo di traduzione?") then
			return -1
		end if
		*/
	else
		dw_list.setitem(ll_i, "des_prodotto", ls_des_prodotto)
	end if
	// ----
	
	// DES MISURA
	select des_misura
	into :ls_des_misura
	from tab_misure_lingue
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_misura = :ls_cod_misura and
		cod_lingua = :ls_cod_lingua;
		
	if sqlca.sqlcode < 0 then
		// errore traduzione
		g_mb.error("SEP", "Errore durante la traduzione della misura " + ls_cod_misura + " nella lingua " + ls_cod_lingua)
		return -1
	elseif sqlca.sqlcode = 100 then
		/*
		// chiedo se continuare o bloccare
		if not g_mb.confirm("SEP", "Attenzione: la misura " + ls_cod_misura + " non ha nessuna traduzione per la lingua " + ls_cod_lingua + ", Continuare il processo di traduzione?") then
			return -1
		end if
		*/
	else
		dw_list.setitem(ll_i, "des_misura", ls_des_misura)
	end if
	// ----
		
next

dw_list.resetupdate()

return 0
end function

public function integer wf_carica_logo ();string ls_filename

select stringa
into :ls_filename
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and    
		cod_parametro = 'LET';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("SEP","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if sqlca.sqlcode = 100 then
	g_mb.messagebox("SEP","Manca il parametro aziendale per il logo dell'etichetta. Creare un nuovo parametro aziendale con codice LET di tipo stringa e impostare il nome del file.", stopsign!)
end if

ls_filename = s_cs_xx.volume + ls_filename

dw_print.Object.p_logo.filename = ls_filename
end function

public function integer wf_pre_stampa ();/**
 * Stefano
 * 13/05/2010
 *
 * Inserisce i record all'interno della dw_print e poi manda in stampa
 **/
 
string ls_cod_prodotto, ls_des_prodotto, ls_des_misura, ls_barcode, ls_strqta
long ll_i, ll_j, ll_n_etichette, ll_riga, ll_prog_riga_ord_ven
dec{4} ld_quan_colli

dw_list.accepttext()
if dw_list.rowcount() < 1 then return 0

// resetto e carico loghi
dw_print.reset()
//wf_carica_logo()
// ----

/*  ******  Eliminata questa opzione su richiesta di Beatrice 10-05-2013
//02/03/2011 recupero il barcode dell'ordine sfuso
wf_imposta_barcode(il_anno_registrazione, il_num_registrazione, ls_barcode)
*/

for ll_i = 1 to dw_list.rowcount()
	ll_n_etichette = dw_list.getitemnumber(ll_i, "n_etichette")
		
	// controllo che ci sia almeno una etichetta da stampare
	if isnull(ll_n_etichette) or ll_n_etichette < 1 then continue
	
	ls_cod_prodotto = dw_list.getitemstring(ll_i, "cod_prodotto")
	ls_des_prodotto = dw_list.getitemstring(ll_i, "des_prodotto")
	ls_des_misura = dw_list.getitemstring(ll_i, "des_misura")
	// richiesto da Beatrice 10-5-13 al posto dei colli mettere la quantità.
	ld_quan_colli = dw_list.getitemnumber(ll_i, "n_colli")
	ls_strqta = string(ld_quan_colli,"0000.00")
	ls_strqta = left(ls_strqta, 4) + right(ls_strqta, 2)
	
	ll_prog_riga_ord_ven = dw_list.getitemnumber(ll_i, "prog_riga_ord_ven")
	
	
	for ll_j = 1 to ll_n_etichette
		// INSERT
		ll_riga = dw_print.insertrow(0)
		dw_print.setitem(ll_riga, "anno_registrazione", il_anno_registrazione)
		dw_print.setitem(ll_riga, "num_registrazione", il_num_registrazione)
		dw_print.setitem(ll_riga, "rag_soc_1", is_des_cliente)
		dw_print.setitem(ll_riga, "cod_prodotto", ls_cod_prodotto)
		dw_print.setitem(ll_riga, "des_prodotto", ls_des_prodotto)
		dw_print.setitem(ll_riga, "des_misura", ls_des_misura)
		dw_print.setitem(ll_riga, "n_colli", ld_quan_colli)
		
		// nuovo tipo Barcode richiesto da Beatrice per sfuso
		// 2 cifre per l'anno ordine + 6 cifre ordine + 4 cifre riga + qta con 4 caratteri per interi e 2 car per decimal
		
		ls_barcode = 	right( string(il_anno_registrazione,"0000"), 2) + &		
							string(il_num_registrazione,"000000") + &
							string(ll_prog_riga_ord_ven,"0000") + &
							ls_strqta
		
		
		//setitem del campo barcode
		try
			dw_print.setitem(ll_riga, "barcode", ls_barcode)
		catch (throwable err)
		end try
		//fine modifica ----------------------------------------
		
	next
next
end function

public function integer wf_imposta_barcode (long ll_anno, long ll_numero, ref string fs_barcode);string ls_CSB
long ll_barcode
integer li_anno

//lettura parametro aziendale Carattere Speciale barcode (specifica Carico ordini con palmare)
select stringa
into   :ls_CSB
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='CSB' and 
		flag_parametro='S';

if sqlca.sqlcode < 0 then
	// "Errore in ricerca del parametro CSB in tabella parametri azienda: " + sqlca.sqlerrtext, stopsign!)
	fs_barcode = "er-CSB"
	return -1
	
elseif sqlca.sqlcode=100 or isnull(ls_CSB) or ls_CSB = "" then	
	ls_CSB = ""
end if
//------------------------------------------------------------------------------------------------------------------------------------

//si tratta di uno sfuso quindi mi aspetto una riga
select progr_tes_produzione
into   :ll_barcode	
from   tes_ordini_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:ll_anno
and	 num_registrazione=:ll_numero;
			
if sqlca.sqlcode < 0 then 
	fs_barcode = "er-tes"
	return -1
end if

if sqlca.sqlcode = 100 then
	fs_barcode = "er-NoOrd"
	return -1
end if

if sqlca.sqlcode = 0 then
	fs_barcode = "*"+ls_CSB+string(ll_barcode)+ls_CSB+"*"
	return 0
end if

return 0
end function

public function string wf_logo ();string			ls_parametro

guo_functions.uof_get_parametro_azienda("TLE", ls_parametro)

if isnull(ls_parametro) then ls_parametro = ""

return ls_parametro
end function

on w_stampa_etichette_sfuso.create
int iCurrent
call super::create
this.st_log=create st_log
this.dw_list=create dw_list
this.st_1=create st_1
this.cb_stampa=create cb_stampa
this.dw_print=create dw_print
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_log
this.Control[iCurrent+2]=this.dw_list
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.cb_stampa
this.Control[iCurrent+5]=this.dw_print
end on

on w_stampa_etichette_sfuso.destroy
call super::destroy
destroy(this.st_log)
destroy(this.dw_list)
destroy(this.st_1)
destroy(this.cb_stampa)
destroy(this.dw_print)
end on

event pc_setwindow;call super::pc_setwindow;il_anno_registrazione = long(s_cs_xx.parametri.parametro_d_1)
il_num_registrazione = long(s_cs_xx.parametri.parametro_d_2)

if isnull(il_anno_registrazione) or il_anno_registrazione < 1900 or isnull(il_num_registrazione) or il_num_registrazione < 1 then
	g_mb.error("SEP", "Parametri non validi per aprire la finestra")
	close(this)
end if


//dw_list.set_dw_options(sqlca, pcca.null_object, c_default,c_default)
dw_list.SetTransObject(sqlca)
dw_list.retrieve(s_cs_xx.cod_azienda, il_anno_registrazione, il_num_registrazione)

dw_print.settransobject(sqlca)
dw_print.visible = false


dw_print.Modify("t_7.text='"+wf_logo()+"'")

end event

type st_log from statictext within w_stampa_etichette_sfuso
integer x = 23
integer y = 1340
integer width = 1737
integer height = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type dw_list from uo_std_dw within w_stampa_etichette_sfuso
integer x = 23
integer y = 220
integer width = 2926
integer height = 1060
integer taborder = 10
string dataobject = "d_stampa_etichiette_sfuso"
boolean hscrollbar = true
boolean vscrollbar = true
end type

type st_1 from statictext within w_stampa_etichette_sfuso
integer x = 23
integer y = 20
integer width = 2926
integer height = 180
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 67108864
string text = "STAMPA ETICHETTE SFUSO"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_stampa from commandbutton within w_stampa_etichette_sfuso
integer x = 2309
integer y = 1320
integer width = 617
integer height = 120
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "&Stampa"
end type

event clicked;// TRADUCO
st_log.text = "Traduzione descrizione prodotti..."

if wf_traduci_prodotti() < 0 then
	st_log.text = "Errore durante la traduzione prodotti"
	return -1
end if
// ----

// STAMPO
st_log.text = "Stampo etichette sfuso..."
wf_pre_stampa()
// ----

// solo DEBUG per vedere la dw etichette
//dw_print.visible = true
//dw_print.BringToTop = true
// ---

if dw_print.rowcount() < 1 then
	g_mb.show("SEP", "Attenzione: nessuna etichetta da stampare")
	st_log.text = ""
	return 0
else
	if dw_print.print() = 1 then
		st_log.text = "Etichette sfuso stampate"
	else
		st_log.text = "Errore durante la stampa delle etichette"
	end if
end if
end event

type dw_print from uo_std_dw within w_stampa_etichette_sfuso
boolean visible = false
integer x = 23
integer y = 220
integer width = 2743
integer height = 1060
integer taborder = 20
string dataobject = "d_label_etichette_sfuso"
boolean hscrollbar = true
boolean vscrollbar = true
end type

