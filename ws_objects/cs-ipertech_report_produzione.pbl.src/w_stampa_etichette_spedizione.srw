﻿$PBExportHeader$w_stampa_etichette_spedizione.srw
$PBExportComments$Finestra per la stampa delle etichette di produzione
forward
global type w_stampa_etichette_spedizione from w_cs_xx_risposta
end type
type dw_report from datawindow within w_stampa_etichette_spedizione
end type
type st_des_da_tabella from statictext within w_stampa_etichette_spedizione
end type
type dw_tab_des from datawindow within w_stampa_etichette_spedizione
end type
type st_3 from statictext within w_stampa_etichette_spedizione
end type
type sle_testo_libero from singlelineedit within w_stampa_etichette_spedizione
end type
type st_2 from statictext within w_stampa_etichette_spedizione
end type
type st_1 from statictext within w_stampa_etichette_spedizione
end type
type cb_conferma from commandbutton within w_stampa_etichette_spedizione
end type
type cb_chiudi from commandbutton within w_stampa_etichette_spedizione
end type
type em_num_copie from editmask within w_stampa_etichette_spedizione
end type
end forward

global type w_stampa_etichette_spedizione from w_cs_xx_risposta
integer width = 3031
integer height = 1420
string title = "STAMPA ETICHETTE PRODUZIONE"
boolean controlmenu = false
dw_report dw_report
st_des_da_tabella st_des_da_tabella
dw_tab_des dw_tab_des
st_3 st_3
sle_testo_libero sle_testo_libero
st_2 st_2
st_1 st_1
cb_conferma cb_conferma
cb_chiudi cb_chiudi
em_num_copie em_num_copie
end type
global w_stampa_etichette_spedizione w_stampa_etichette_spedizione

type variables
integer ii_ok
long il_colli[]
end variables

forward prototypes
public function integer wf_log_sistema (long fl_sqlcode, long fl_anno_reg, long fl_num_reg, long fl_num_riga)
public function string wf_lingua_cliente (integer ai_anno_ordine, long al_num_ordine)
end prototypes

public function integer wf_log_sistema (long fl_sqlcode, long fl_anno_reg, long fl_num_reg, long fl_num_riga);
string ls_sqlerrtext = ""
string ls_messaggio,ls_flag_blocco_cu
long ll_id_sistema,ll_prog_riga_cu,ll_num_riga_app_cu
string ls_tipo_log = "ETICHETTE"
integer li_sqlcode = 100
datetime ldt_today, ldt_now

if fl_sqlcode=100 then
else
	//esci dalla funzione
	return 1
end if

//se arrivi fin qui vuol dire che sqlcode è 100: log in tabella

ls_sqlerrtext = string(fl_anno_reg)+"/"
ls_sqlerrtext += string(fl_num_reg)+"/"
ls_sqlerrtext += string(fl_num_riga)


declare  righe_log_sistema cursor for
select   prog_riga_ord_ven,flag_blocco,num_riga_appartenenza
from     det_ord_ven
where    cod_azienda = :s_cs_xx.cod_azienda and      
			anno_registrazione = :fl_anno_reg and      
			num_registrazione = :fl_num_reg
order by prog_riga_ord_ven;

open righe_log_sistema;
if sqlca.sqlcode <> 0 then
	return -1
end if

do while 1 = 1			
	
	fetch righe_log_sistema into  :ll_prog_riga_cu,:ls_flag_blocco_cu,:ll_num_riga_app_cu;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then
		close righe_log_sistema;
		return -1
	end if
	
	//calcola il max
	select max(id_log_sistema)
	into :ll_id_sistema
	from log_sistema;
	
	if sqlca.sqlcode < 0 then
		close righe_log_sistema;
		return -1
		
	elseif sqlca.sqlcode = 100 or isnull(ll_id_sistema) or ll_id_sistema < 1 then 
		ll_id_sistema = 0
	end if
	
	ll_id_sistema++
	ldt_today = datetime(today(), 00:00:00)
	ldt_now = datetime(date("01/01/1900"), now())
	if len(ls_tipo_log) > 20 then left(ls_tipo_log, 20)
	
	if isnull(ll_prog_riga_cu) then
		ls_messaggio = "riga=NULL "
	else
		ls_messaggio = "riga="+string(ll_prog_riga_cu)+" "
	end if
	
	if isnull(ls_flag_blocco_cu) then ls_flag_blocco_cu="NULL"
	ls_messaggio += "blocco="+ls_flag_blocco_cu+" "
	
	if isnull(ll_num_riga_app_cu) then
		ls_messaggio += "n_riga_app=NULL "
	else
		ls_messaggio += "n_riga_app="+string(ll_num_riga_app_cu)+" "
	end if
	
	insert into log_sistema (
		id_log_sistema,
		data_registrazione,
		ora_registrazione,
		utente,
		flag_tipo_log,
		db_sqlcode,
		db_sqlerrtext,
		messaggio)
	values (
		:ll_id_sistema,
		:ldt_today,
		:ldt_now,
		:s_cs_xx.cod_utente,
		:ls_tipo_log,
		:li_sqlcode,
		:ls_sqlerrtext,
		:ls_messaggio);
	
	if sqlca.sqlcode <> 0 then
		close righe_log_sistema;
		return -1
	end if
	
loop
	
close righe_log_sistema;

return 1
end function

public function string wf_lingua_cliente (integer ai_anno_ordine, long al_num_ordine);string				ls_cod_lingua



select anag_clienti.cod_lingua
into :ls_cod_lingua
from tes_ord_ven
join anag_clienti on 	anag_clienti.cod_azienda=tes_ord_ven.cod_azienda and
							anag_clienti.cod_cliente=tes_ord_ven.cod_cliente
where 	tes_ord_ven.cod_azienda=:s_cs_xx.cod_azienda and
			tes_ord_ven.anno_registrazione=:ai_anno_ordine and
			tes_ord_ven.num_registrazione=:al_num_ordine;

if isnull(ls_cod_lingua) then ls_cod_lingua = ""


return ls_cod_lingua
end function

on w_stampa_etichette_spedizione.create
int iCurrent
call super::create
this.dw_report=create dw_report
this.st_des_da_tabella=create st_des_da_tabella
this.dw_tab_des=create dw_tab_des
this.st_3=create st_3
this.sle_testo_libero=create sle_testo_libero
this.st_2=create st_2
this.st_1=create st_1
this.cb_conferma=create cb_conferma
this.cb_chiudi=create cb_chiudi
this.em_num_copie=create em_num_copie
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
this.Control[iCurrent+2]=this.st_des_da_tabella
this.Control[iCurrent+3]=this.dw_tab_des
this.Control[iCurrent+4]=this.st_3
this.Control[iCurrent+5]=this.sle_testo_libero
this.Control[iCurrent+6]=this.st_2
this.Control[iCurrent+7]=this.st_1
this.Control[iCurrent+8]=this.cb_conferma
this.Control[iCurrent+9]=this.cb_chiudi
this.Control[iCurrent+10]=this.em_num_copie
end on

on w_stampa_etichette_spedizione.destroy
call super::destroy
destroy(this.dw_report)
destroy(this.st_des_da_tabella)
destroy(this.dw_tab_des)
destroy(this.st_3)
destroy(this.sle_testo_libero)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_conferma)
destroy(this.cb_chiudi)
destroy(this.em_num_copie)
end on

event open;call super::open;boolean			lb_alusistem
string				ls_filename, ls_msg, ls_path_img_alert, ls_ret, ls_lingua, ls_cod_cliente, ls_logo_etichetta, ls_logo_azienda
integer			li_tot_colli, li_anno
long				ll_numero

ii_ok = -1


li_anno = integer(s_cs_xx.parametri.parametro_ds_1.getitemstring(1,"anno_registrazione"))
ll_numero = long(s_cs_xx.parametri.parametro_ds_1.getitemstring(1,"num_registrazione"))

ls_lingua = wf_lingua_cliente(li_anno, ll_numero)

select cod_cliente, logo_etichetta
into :ls_cod_cliente, :ls_logo_etichetta
from tes_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:li_anno and
			num_registrazione=:ll_numero;


choose case s_cs_xx.parametri.parametro_s_1
	case 'A'  // tende da sole
		st_des_da_tabella.visible = false
		dw_tab_des.visible = false
		
		lb_alusistem = false
		guo_functions.uof_get_parametro_azienda( "ALU", lb_alusistem)
		if lb_alusistem then
			dw_report.dataobject = "d_label_tende_sole_alusistemi"
			// 28/03/2019 aggiunto lofo Alusistemi (VESTA)
			dw_report.Object.intestazione.Filename = s_cs_xx.volume + s_cs_xx.risorse + "logo_vesta.jpg"
		else
			dw_report.dataobject = "d_label_tende_sole"
		end if

	case 'B','C'  // sfuso e tende tecniche
		dw_report.dataobject = "d_label_tende_tecniche_sfuso"
		
end choose

//visualizza lingua del cliente, se c'è
ls_ret = dw_report.modify("t_3.height.autosize=yes")
if ls_lingua<>"" then dw_report.modify("t_3.text='Cliente~r~n("+ls_lingua+")'")


s_cs_xx.parametri.parametro_ds_1.sharedata(dw_report)

select stringa
into   :ls_filename
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and    
       cod_parametro = 'LET';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("SEP","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if sqlca.sqlcode = 100 then
	g_mb.messagebox("SEP","Manca il parametro aziendale per il logo dell'etichetta. Creare un nuovo parametro aziendale con codice LET di tipo stringa e impostare il nome del file.", stopsign!)
end if


ls_filename = s_cs_xx.volume+ls_filename

if g_str.isnotempty(ls_logo_etichetta) then
	ls_logo_etichetta = s_cs_xx.volume + s_cs_xx.risorse + "loghi_clienti\"+ls_cod_cliente+"\"+ls_logo_etichetta
	if fileexists(ls_logo_etichetta) then
		ls_filename = ls_logo_etichetta
	end if
end if


dw_report.Object.p_logo.filename = ls_filename


//11/04/2012: Beatrice, anche in caso sfuso, visualizza sull'etichetta di spedizione le prime 3 lettere del deposito origine dell'ordine
//if s_cs_xx.parametri.parametro_s_1="A" or s_cs_xx.parametri.parametro_s_1="C" then
	ls_ret = dw_report.Modify("t_stabilimento.text='"+s_cs_xx.parametri.parametro_s_14+"'")
	s_cs_xx.parametri.parametro_s_14 = ""
//end if


if s_cs_xx.parametri.parametro_dec4_5 > 0 then
	ls_path_img_alert = s_cs_xx.volume + s_cs_xx.risorse + "alert.jpg"
else
	ls_path_img_alert = ""
end if
s_cs_xx.parametri.parametro_dec4_5 = 0
dw_report.Modify("p_alert.FileName='"+ls_path_img_alert+"'")


cb_conferma.setfocus( )
end event

event closequery;call super::closequery;if ii_ok=-1 then return 1

end event

event pc_setddlb;call super::pc_setddlb;choose case s_cs_xx.parametri.parametro_s_1
	
	case "B"  // sfuso
		f_po_loaddddw_dw(dw_tab_des, &
                 "cod_des", &
                 sqlca, &
                 "tab_des_sfuso", &
                 "cod_des_sfuso", &
                 "des_sfuso", &
					  "cod_azienda='" + s_cs_xx.cod_azienda +"'")
		
	case "C"  // tende tecniche
		f_po_loaddddw_dw(dw_tab_des, &
                 "cod_des", &
                 sqlca, &
                 "tab_des_tecniche", &
                 "cod_des_tecniche", &
                 "des_tecniche", &
					  "cod_azienda='" + s_cs_xx.cod_azienda +"'")

end choose




end event

event pc_setwindow;call super::pc_setwindow;string ls_filename,ls_modify,li_risposta

dw_tab_des.insertrow(0)

cb_conferma.setfocus( )


end event

type dw_report from datawindow within w_stampa_etichette_spedizione
boolean visible = false
integer x = 1262
integer y = 240
integer width = 1714
integer height = 340
integer taborder = 30
string title = "none"
string dataobject = "d_label_tende_sole"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type st_des_da_tabella from statictext within w_stampa_etichette_spedizione
integer x = 23
integer y = 740
integer width = 983
integer height = 100
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Descrizione da tabella"
boolean focusrectangle = false
end type

type dw_tab_des from datawindow within w_stampa_etichette_spedizione
integer x = 23
integer y = 840
integer width = 2926
integer height = 140
integer taborder = 30
string title = "none"
string dataobject = "d_tab_des"
boolean border = false
boolean livescroll = true
end type

type st_3 from statictext within w_stampa_etichette_spedizione
integer x = 23
integer y = 460
integer width = 526
integer height = 100
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Testo libero"
boolean focusrectangle = false
end type

type sle_testo_libero from singlelineedit within w_stampa_etichette_spedizione
integer x = 23
integer y = 560
integer width = 2926
integer height = 120
integer taborder = 20
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type st_2 from statictext within w_stampa_etichette_spedizione
integer x = 23
integer y = 224
integer width = 617
integer height = 184
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Q.tà etichette per collo:"
boolean focusrectangle = false
end type

type st_1 from statictext within w_stampa_etichette_spedizione
integer x = 23
integer y = 20
integer width = 2962
integer height = 180
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean italic = true
long textcolor = 33554432
long backcolor = 67108864
string text = "STAMPA ETICHETTE PRODUZIONE"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_conferma from commandbutton within w_stampa_etichette_spedizione
integer x = 2194
integer y = 1060
integer width = 754
integer height = 220
integer taborder = 20
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
boolean default = true
end type

event clicked;boolean lb_alusistemi
long job,ll_num_copie,ll_t,ll_num_registrazione,ll_num_max_tenda,ll_num_tenda,ll_prog_riga_ord_ven,ll_prog_riga_ord_ven_cfr, ll_i, ll_tot_colli
integer li_anno_registrazione
string ls_drop_down,ls_cod_des,ls_cod_reparto,ls_cgibus, ls_barcode_hold, ls_temp, ls_colli_v[], ls_msg, ls_vuoto[], ls_riga_n_di_m, &
		ls_tessuto, ls_verniciatura, ls_cod_comodo
uo_produzione luo_prod
string ls_NCE


// parametro che dice se siamo in presenza della personalizzazione per alusistemi
lb_alusistemi = false
guo_functions.uof_get_parametro_azienda("ALU",lb_alusistemi)

//parametro che nasconde la scritta relativa ai colli prodotti per reparto
//chiesto da Beatrice il 19/05/2011
select flag
into   :ls_NCE
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda and
		 flag_parametro='F' and
       cod_parametro='NCE';

if sqlca.sqlcode<0 or sqlca.sqlcode=100 or ls_NCE="" or isnull(ls_NCE) then
	ls_NCE = "N"
end if


ll_num_copie = long (em_num_copie.text)

if s_cs_xx.parametri.parametro_s_1 = 'B' or s_cs_xx.parametri.parametro_s_1 ='C' then
	ls_cod_des = dw_tab_des.getitemstring(1,"cod_des")
	
end if

choose case s_cs_xx.parametri.parametro_s_1
	case 'A'  // tende da sole
		
		select anno_registrazione,
				 num_registrazione,
				 cod_reparto,
				 prog_riga_ord_ven
		into   :li_anno_registrazione,
				 :ll_num_registrazione,
				 :ls_cod_reparto,
				 :ll_prog_riga_ord_ven
		from   det_ordini_produzione
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    progr_det_produzione=:s_cs_xx.parametri.parametro_ul_1;
		
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		
		select count(*)
		into   :ll_num_max_tenda
		from   det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:li_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    num_riga_appartenenza=0
		and    flag_blocco='N';

		
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		wf_log_sistema(sqlca.sqlcode,li_anno_registrazione,ll_num_registrazione,ll_prog_riga_ord_ven)
		
		declare  righe_det_ord_ven_1 cursor for
		select   prog_riga_ord_ven
		from     det_ord_ven
		where    cod_azienda = :s_cs_xx.cod_azienda and      
		         anno_registrazione = :li_anno_registrazione and      
					num_registrazione = :ll_num_registrazione and      
					num_riga_appartenenza = 0 and      
					flag_blocco = 'N'
		order by prog_riga_ord_ven;
		
		open righe_det_ord_ven_1;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "SEP", "Errore apertura cursore: " + sqlca.sqlerrtext, stopsign!)
			return -1
		end if
		
		do while 1 = 1			
			
			fetch righe_det_ord_ven_1 into  :ll_prog_riga_ord_ven_cfr;
					
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode < 0 then 
				g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				close righe_det_ord_ven_1;
				return -1
			end if
			
			ll_num_tenda++				
		
			if ll_prog_riga_ord_ven = ll_prog_riga_ord_ven_cfr then exit
			
		loop
			
		close righe_det_ord_ven_1;
			
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		ls_riga_n_di_m = ""
		luo_prod = create uo_produzione
		ls_riga_n_di_m = luo_prod.uof_get_riga_su_tot_righe(li_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven)
		destroy luo_prod;
		
		s_cs_xx.parametri.parametro_ds_1.setitem(1,"num_riga_num_tot", ls_riga_n_di_m)
		//s_cs_xx.parametri.parametro_ds_1.setitem(1,"num_riga_num_tot", string(string(ll_num_tenda)) +"/" + string(ll_num_max_tenda))
		
	case 'B'  // sfuso
		select des_sfuso
		into   :ls_drop_down
		from   tab_des_sfuso
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_des_sfuso=:ls_cod_des;
		
		if sqlca.sqlcode < 0 then 
			g_mb.messagebox("SEP","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
	case 'C'  // tende tecniche
		select des_tecniche
		into   :ls_drop_down
		from   tab_des_tecniche
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_des_tecniche=:ls_cod_des;
		
		if sqlca.sqlcode < 0 then 
			g_mb.error("Errore sul DB: " + sqlca.sqlerrtext)
			return -1
		end if
		
end choose

if s_cs_xx.parametri.parametro_s_1 = 'B' or s_cs_xx.parametri.parametro_s_1 ='C' then
	s_cs_xx.parametri.parametro_ds_1.setitem(1,"drop_down",ls_drop_down)
end if

s_cs_xx.parametri.parametro_ds_1.setitem(1,"testo_libero",sle_testo_libero.text)
s_cs_xx.parametri.parametro_i_1 = 1

ls_barcode_hold = s_cs_xx.parametri.parametro_ds_1.getitemstring(1,"barcode")

ii_ok=0


// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//se ci sono colli in tab_ord_ven_colli allora stampa quelli, altrimenti procedi tradizionalmente
//recupero i colli presenti, sia in caso di avanzamento produzione che semplice ristampa
//infatti nel caso di avanzamento prod. i colli vengono stampati tutti
luo_prod = create uo_produzione

if luo_prod.uof_carica_colli_barcode(s_cs_xx.parametri.parametro_ul_1, ls_colli_v[], ls_cod_reparto, ls_msg) < 0 then
	//in ls_msg l'errore
	destroy luo_prod;
	
	g_mb.error(ls_msg)
	return -1
end if

destroy luo_prod;

//recupero il campo sigla dal reparto
string ls_sigla

select sigla
into :ls_sigla
from anag_reparti
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_reparto=:ls_cod_reparto;

if ls_sigla<>"" and not isnull(ls_sigla) then ls_cod_reparto = ls_sigla

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//Donato 14/03/2011 la funzione chiamata per la ristampa è la stessa che viene usata dopo la chiusura della produzione
//quindi occorre distinguere, perchè nel casi di ristampa e in presenza di + colli, l'operatore deve poter scegliere quali etichette
//(cioè di quali colli) ristampare
ll_tot_colli = upperbound(ls_colli_v[])

//09/03/2012 chiesto da Beatrice e Daniele
//quando stampi, chiedi sempre quali colli stampare dalla finestra di selezione, a prescindere che sia una ristampa o una chiusura fase
//per attivare la scelta è sufficiente che ci siano colli
if ll_tot_colli>0 then
//if s_cs_xx.parametri.parametro_s_11 = "RISTAMPA" and ll_tot_colli>0 then

	//apri un finestra di selezione colli da stampare
	s_cs_xx.parametri.parametro_s_4_a[] = ls_colli_v[]
	
	window_open(w_sel_colli_stampa, 0)
	
	//effettuo il travaso dei colli selezionati per la ristampa
	ls_colli_v[] = s_cs_xx.parametri.parametro_s_4_a[]
	
	//reset del vettore per sicurezza
	s_cs_xx.parametri.parametro_s_4_a[] = ls_vuoto[]
	
	//reset del semaforo per sicurezza
	s_cs_xx.parametri.parametro_s_11 = ""
	
	//potrei aver selezionato NESSUN collo da ristampare
	if upperbound(ls_colli_v) = 0 then
		commit;
		close (parent)
		return
	end if
end if


job = PrintOpen( ) 



if upperbound(ls_colli_v[]) > 0 then
	
	//stampa il barcode + il progressivo collo
	for ll_i = 1 to upperbound(ls_colli_v)
		for ll_t = 1 to ll_num_copie
			//stampa         * [CSB] [BARCODE] - [i] [CSB] *
			s_cs_xx.parametri.parametro_ds_1.setitem(1,"barcode", ls_colli_v[ll_i])
			s_cs_xx.parametri.parametro_ds_1.setitem(1,"num_colli",string(ll_tot_colli))
			
			//In base ad un param aziendale, si nasconde la scritta relativa ai colli prodotti per reparto
			//chiesto da Beatrice il 19/05/2011
			if ls_NCE = "S" then
				dw_report.object.t_reparto.text = ""
			else
				//campo sigla di anag_reparti=numero colli per reparto
				dw_report.object.t_reparto.text = ls_cod_reparto + "~r~n="+string(ll_tot_colli)
			end if
			
			// Personalizzazione per Alusistemi
			if lb_alusistemi then
				s_cs_xx.parametri.parametro_ds_1.setitem(1,"sequenza_collo", string(ll_i) + "/" + string(ll_tot_colli) )
				
				select 	comp_det_ord_ven.cod_non_a_magazzino, 
							anag_prodotti.des_prodotto
				into		:ls_tessuto,
							:ls_verniciatura
				from 		comp_det_ord_ven 
							left join anag_prodotti on anag_prodotti.cod_azienda = comp_det_ord_ven.cod_azienda and anag_prodotti.cod_prodotto = comp_det_ord_ven.cod_verniciatura 
				where 	comp_det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
							comp_det_ord_ven.anno_registrazione=:li_anno_registrazione and
							comp_det_ord_ven.num_registrazione=:ll_num_registrazione and
							comp_det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
							
				select 	anag_prodotti.cod_comodo
				into		:ls_cod_comodo
				from 		det_ord_ven 
							left join anag_prodotti on anag_prodotti.cod_azienda = det_ord_ven.cod_azienda and anag_prodotti.cod_prodotto = det_ord_ven.cod_prodotto
				where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
							det_ord_ven.anno_registrazione=:li_anno_registrazione and
							det_ord_ven.num_registrazione=:ll_num_registrazione and
							det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
							
						
				s_cs_xx.parametri.parametro_ds_1.setitem(1,"tessuto", ls_tessuto )
				s_cs_xx.parametri.parametro_ds_1.setitem(1,"verniciatura", ls_verniciatura )
				s_cs_xx.parametri.parametro_ds_1.setitem(1,"des_prodotto_mod", ls_cod_comodo )
				
			end if
				
			
			PrintDataWindow(job, dw_report)
		next
	next
	
else
	
	//In base ad un param aziendale, si nasconde la scritta relativa ai colli prodotti per reparto
	//chiesto da Beatrice il 19/05/2011
	if ls_NCE = "S" then
		dw_report.object.t_reparto.text = ""
	end if
	//-------------------------------------------------------------------------------
	
	//procedi tradizionalmente -----
	for ll_t = 1 to ll_num_copie
		PrintDataWindow(job, dw_report)
	next
end if

PrintClose(job)


commit;

close (parent)

end event

type cb_chiudi from commandbutton within w_stampa_etichette_spedizione
event clicked pbm_bnclicked
integer x = 1417
integer y = 1060
integer width = 754
integer height = 220
integer taborder = 10
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C&hiudi"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = 1
ii_ok = 0

commit;

close (parent)
end event

type em_num_copie from editmask within w_stampa_etichette_spedizione
integer x = 681
integer y = 292
integer width = 297
integer height = 116
integer taborder = 10
boolean bringtotop = true
integer textsize = -14
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#"
boolean spin = true
end type

