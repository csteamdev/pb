﻿$PBExportHeader$w_stampa_etichette_tecniche.srw
$PBExportComments$Finestra per la stampa delle etichette di produzione
forward
global type w_stampa_etichette_tecniche from w_cs_xx_risposta
end type
type cb_stampa_colli from commandbutton within w_stampa_etichette_tecniche
end type
type dw_lista_prodotti from datawindow within w_stampa_etichette_tecniche
end type
type dw_report_produzione from uo_cs_xx_dw within w_stampa_etichette_tecniche
end type
type st_1 from statictext within w_stampa_etichette_tecniche
end type
type cb_conferma from commandbutton within w_stampa_etichette_tecniche
end type
type cb_chiudi from commandbutton within w_stampa_etichette_tecniche
end type
type dw_folder from u_folder within w_stampa_etichette_tecniche
end type
type st_2 from statictext within w_stampa_etichette_tecniche
end type
type em_num_copie from editmask within w_stampa_etichette_tecniche
end type
end forward

global type w_stampa_etichette_tecniche from w_cs_xx_risposta
integer width = 3077
integer height = 1424
string title = "STAMPA ETICHETTE PRODUZIONE"
boolean controlmenu = false
cb_stampa_colli cb_stampa_colli
dw_lista_prodotti dw_lista_prodotti
dw_report_produzione dw_report_produzione
st_1 st_1
cb_conferma cb_conferma
cb_chiudi cb_chiudi
dw_folder dw_folder
st_2 st_2
em_num_copie em_num_copie
end type
global w_stampa_etichette_tecniche w_stampa_etichette_tecniche

type variables
integer ii_ok
string is_cgibus
end variables

forward prototypes
public function integer wf_visualizza_prodotti ()
end prototypes

public function integer wf_visualizza_prodotti ();integer li_anno_registrazione
long ll_num_registrazione,ll_prog_riga_ord_ven, ll_row,ll_altezza_mantovana
string ls_cod_reparto,ls_data_consegna, ls_cod_prodotto,ls_des_prodotto, ls_nota_dettaglio, ls_cod_colore_tessuto, ls_des_colore_tessuto, ls_cod_colore_mant, ls_des_colore_mant, &
		 ls_flag_tipo_mant, ls_colore_passamaneria,ls_nota_prodotto,ls_errore, ls_rag_soc, ls_indirizzo_1, ls_ind_1_diversa, &
		 ls_rag_soc_diversa, ls_ind_2_diversa,ls_indirizzo_2
datetime ldt_data_consegna, ldt_data_pronto
decimal ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t,ld_quan_etichetta_produzione, ld_quan_ordine


select anno_registrazione,
		 num_registrazione,
		 cod_reparto
into   :li_anno_registrazione,
		 :ll_num_registrazione,
		 :ls_cod_reparto
from   tes_ordini_produzione
where  cod_azienda =: s_cs_xx.cod_azienda and
		 progr_tes_produzione =: s_cs_xx.parametri.parametro_ul_1;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore in ricerca dati av.prod.: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if




declare  righe_det_ord_ven_1 cursor for
select   cod_prodotto, quan_ordine, prog_riga_ord_ven, nota_dettaglio, nota_prodotto, data_consegna
from     det_ord_ven
where    cod_azienda = :s_cs_xx.cod_azienda and      
			anno_registrazione = :li_anno_registrazione and      
			num_registrazione = :ll_num_registrazione and      
			num_riga_appartenenza = 0 and      
			flag_blocco = 'N'
order by prog_riga_ord_ven;

open righe_det_ord_ven_1;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "SEP", "Errore apertura cursore righe_det_ord_ven_1~r~n " + sqlca.sqlerrtext, stopsign!)
	return -1
end if
		
do while true	
	
	fetch righe_det_ord_ven_1 into :ls_cod_prodotto, :ld_quan_ordine, :ll_prog_riga_ord_ven, :ls_nota_dettaglio, :ls_nota_prodotto, :ldt_data_consegna;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then 
		g_mb.messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close righe_det_ord_ven_1;
		return -1
	end if
	
	select dim_x,   
			 dim_y,   
			 dim_z,   
			 dim_t,
			 cod_non_a_magazzino,
			 cod_mant_non_a_magazzino,
			 flag_tipo_mantovana,
			 alt_mantovana,
			 colore_passamaneria
	into   :ld_dim_x,   
			 :ld_dim_y,   
			 :ld_dim_z,   
			 :ld_dim_t,
			 :ls_cod_colore_tessuto,
			 :ls_cod_colore_mant,
			 :ls_flag_tipo_mant,
			 :ll_altezza_mantovana,
			 :ls_colore_passamaneria
	 from  comp_det_ord_ven 
	 where cod_azienda=:s_cs_xx.cod_azienda and
	       anno_registrazione=:li_anno_registrazione and
			 num_registrazione=:ll_num_registrazione 	 and
			 prog_riga_ord_ven=:ll_prog_riga_ord_ven;

	 if sqlca.sqlcode < 0 then 
		ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	 end if
	 
		 
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;
	
	if sqlca.sqlcode < 0 then 
		ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
		return -1
	end if
	
	
	ll_row = dw_lista_prodotti.insertrow(0)
	dw_lista_prodotti.setitem(ll_row,"cod_prodotto", ls_cod_prodotto)
	dw_lista_prodotti.setitem(ll_row,"des_prodotto", ls_cod_prodotto)
	dw_lista_prodotti.setitem(ll_row,"quan_ordine", ld_quan_ordine)
	dw_lista_prodotti.setitem(ll_row,"prog_riga_ord_ven", ll_prog_riga_ord_ven)
	dw_lista_prodotti.setitem(ll_row,"nota_prodotto", ls_nota_prodotto)
	dw_lista_prodotti.setitem(ll_row,"nota_dettaglio", ls_nota_dettaglio)
	dw_lista_prodotti.setitem(ll_row,"data_consegna", ldt_data_consegna)
	
	
loop
end function

on w_stampa_etichette_tecniche.create
int iCurrent
call super::create
this.cb_stampa_colli=create cb_stampa_colli
this.dw_lista_prodotti=create dw_lista_prodotti
this.dw_report_produzione=create dw_report_produzione
this.st_1=create st_1
this.cb_conferma=create cb_conferma
this.cb_chiudi=create cb_chiudi
this.dw_folder=create dw_folder
this.st_2=create st_2
this.em_num_copie=create em_num_copie
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa_colli
this.Control[iCurrent+2]=this.dw_lista_prodotti
this.Control[iCurrent+3]=this.dw_report_produzione
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.cb_conferma
this.Control[iCurrent+6]=this.cb_chiudi
this.Control[iCurrent+7]=this.dw_folder
this.Control[iCurrent+8]=this.st_2
this.Control[iCurrent+9]=this.em_num_copie
end on

on w_stampa_etichette_tecniche.destroy
call super::destroy
destroy(this.cb_stampa_colli)
destroy(this.dw_lista_prodotti)
destroy(this.dw_report_produzione)
destroy(this.st_1)
destroy(this.cb_conferma)
destroy(this.cb_chiudi)
destroy(this.dw_folder)
destroy(this.st_2)
destroy(this.em_num_copie)
end on

event closequery;call super::closequery;if ii_ok=-1 then return 1

end event

event pc_setwindow;call super::pc_setwindow;long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_quan_etichetta_produzione
string ls_filename, ls_cod_reparto
dec{4} ld_quan_ordine
windowobject lw_oggetti[]

dw_report_produzione.ib_dw_report = true

// attivo lil folder
lw_oggetti[1] = dw_lista_prodotti
dw_folder.fu_assigntab(2, "Selezione", lw_oggetti[])
lw_oggetti[1] = em_num_copie
lw_oggetti[2] = st_2
lw_oggetti[3] = cb_chiudi
lw_oggetti[4] = cb_conferma
dw_folder.fu_assigntab(1, "Stampa", lw_oggetti[])
dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)



ii_ok = -1

// cerco la quantità del prodotto finito per impostare la quantità di etichette.
// enrico 20/03/2006

select anno_registrazione,
		 num_registrazione,
		 cod_reparto
into   :ll_anno_registrazione,
		 :ll_num_registrazione,
		 :ls_cod_reparto
from   tes_ordini_produzione
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 progr_tes_produzione = :s_cs_xx.parametri.parametro_ul_1;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore in ricerca testata ordini produzione~r~n" + sqlca.sqlerrtext,stopsign!)
	return -1
end if


select quan_etichetta_produzione
into  :ll_quan_etichetta_produzione
from  anag_reparti
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_reparto = :ls_cod_reparto;
if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if sqlca.sqlcode = 100 then 
	g_mb.messagebox("Sep","Attenzione: il reparto "+ls_cod_reparto+" è inesistente ",stopsign!)
	return -1
end if

em_num_copie.text = string(long(ll_quan_etichetta_produzione))

wf_visualizza_prodotti( )


//verifico se devo stampare le etichettine relative al barcode univoco dei colli
if s_cs_xx.parametri.parametro_s_11 = "COLLI" then
	cb_stampa_colli.visible = true
	cb_conferma.visible = false
	st_2.text = "Q.TA' Etichette Produzione per COLLO = "
	em_num_copie.text = "1"
	
	
	dw_folder.fu_hidetab(2)
	
	cb_stampa_colli.setfocus( )
end if
end event

type cb_stampa_colli from commandbutton within w_stampa_etichette_tecniche
boolean visible = false
integer x = 361
integer y = 620
integer width = 1262
integer height = 220
integer taborder = 30
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa Et. Colli"
boolean default = true
end type

event clicked;integer li_anno_registrazione
long job,ll_num_copie,ll_t,ll_num_registrazione,ll_num_max_tenda,ll_num_tenda,ll_prog_riga_ord_ven, ll_i,&
     ll_prog_riga_ord_ven_cfr,ll_quan_etichetta_produzione, ll_y,ll_num_colli,ll_barcode,ll_num_colli_testata,ll_num_colli_riga, ll_altezza_mantovana
string ls_drop_down,ls_cod_des,ls_cod_reparto,ls_flag_etichetta_apertura,ls_flag_etichetta_produzione,ls_data_consegna, &
       ls_cod_prodotto,ls_des_prodotto, ls_nota_dettaglio, ls_cod_colore_tessuto, ls_des_colore_tessuto, ls_cod_colore_mant, ls_des_colore_mant, &
		 ls_flag_tipo_mant, ls_colore_passamaneria,ls_nota_prodotto,ls_errore, ls_rag_soc, ls_indirizzo_1, ls_ind_1_diversa, &
		 ls_rag_soc_diversa, ls_ind_2_diversa,ls_indirizzo_2,ls_tipo_stampa,ls_cod_tipo_ord_ven
datetime ldt_data_consegna, ldt_data_pronto
decimal ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t,ld_quan_etichetta_produzione, ld_quan_ordine
string ls_barcode, ls_CSB

uo_produzione luo_prod
string ls_colli_v[], ls_vuoto[]
long ll_tot_colli
string ls_sigla


ll_num_copie = long (em_num_copie.text)
ll_barcode = s_cs_xx.parametri.parametro_ul_1

//lettura parametro aziendale Carattere Speciale barcode (specifica Carico ordini con palmare)
select stringa
into   :ls_CSB
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='CSB' and 
		flag_parametro='S';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep", "Errore in ricerca del parametro CSB in tabella parametri azienda: " + sqlca.sqlerrtext, stopsign!)
	return -1
	
elseif sqlca.sqlcode=100 or isnull(ls_CSB) or ls_CSB = "" then	
	ls_CSB = ""
end if
//------------------------------------------------------------------------------------------------------------------------------------


// choose case s_cs_xx.parametri.parametro_s_1

select anno_registrazione,
		 num_registrazione,
		 cod_reparto
into   :li_anno_registrazione,
		 :ll_num_registrazione,
		 :ls_cod_reparto
from   tes_ordini_produzione
where  cod_azienda =: s_cs_xx.cod_azienda and
		 progr_tes_produzione =: s_cs_xx.parametri.parametro_ul_1;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore in ricerca dati av.prod.: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if


// trovo il tipo di stampa sfuso/tecniche///
select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda and
		 anno_registrazione=:li_anno_registrazione and
		 num_registrazione=:ll_num_registrazione;

if sqlca.sqlcode < 0 then 
	ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

select flag_tipo_bcl
into   :ls_tipo_stampa
from   tab_tipi_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;

if sqlca.sqlcode < 0 then 
	ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

if ls_tipo_stampa = "B" then   // sfuso cambio il dataobject
	ls_errore = dw_report_produzione.dataobject
	dw_report_produzione.dataobject = 'd_label_produzione_sfuso'
end if
//

s_cs_xx.parametri.parametro_ds_1.sharedata(dw_report_produzione)

ls_barcode = "*"+ls_CSB+string(ll_barcode)+ls_CSB+"*"
s_cs_xx.parametri.parametro_ds_1.setitem(1,"barcode",ls_barcode)


select count(*)
into   :ll_num_max_tenda
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda and
		 anno_registrazione=:li_anno_registrazione and
		 num_registrazione=:ll_num_registrazione and
		 num_riga_appartenenza=0 and
		 flag_blocco='N';

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if
		

select quan_etichetta_produzione, 
       flag_etichetta_produzione
into   :ll_quan_etichetta_produzione,
		 :ls_flag_etichetta_produzione
from   anag_reparti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_reparto = :ls_cod_reparto;
if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if
if sqlca.sqlcode = 100 then 
	g_mb.messagebox("Sep","Attenzione: il reparto "+ls_cod_reparto+" è inesistente ",stopsign!)
	return -1
end if


if ls_tipo_stampa = "B" then   // etichetta generale dell'ordine.

		ll_i = 1
		ls_cod_prodotto = dw_lista_prodotti.getitemstring(ll_i, "cod_prodotto")
		ls_des_prodotto = dw_lista_prodotti.getitemstring(ll_i, "des_prodotto")
		ld_quan_ordine  = dw_lista_prodotti.getitemnumber(ll_i, "quan_ordine")
		ll_prog_riga_ord_ven = dw_lista_prodotti.getitemnumber(ll_i, "prog_riga_ord_ven")
		ls_nota_dettaglio = dw_lista_prodotti.getitemstring(ll_i, "nota_dettaglio")
		ls_nota_prodotto = dw_lista_prodotti.getitemstring(ll_i, "nota_prodotto")
		ldt_data_consegna = dw_lista_prodotti.getitemdatetime(ll_i, "data_consegna")
		
//		select dim_x,   
//				 dim_y,   
//				 dim_z,   
//				 dim_t,
//				 cod_non_a_magazzino,
//				 cod_mant_non_a_magazzino,
//				 flag_tipo_mantovana,
//				 alt_mantovana,
//				 colore_passamaneria
//		into   :ld_dim_x,   
//				 :ld_dim_y,   
//				 :ld_dim_z,   
//				 :ld_dim_t,
//				 :ls_cod_colore_tessuto,
//				 :ls_cod_colore_mant,
//				 :ls_flag_tipo_mant,
//				 :ll_altezza_mantovana,
//				 :ls_colore_passamaneria
//		 from  comp_det_ord_ven 
//		 where cod_azienda=:s_cs_xx.cod_azienda and
//				 anno_registrazione=:li_anno_registrazione and
//				 num_registrazione=:ll_num_registrazione 	 and
//				 prog_riga_ord_ven=:ll_prog_riga_ord_ven;
//	
//		 if sqlca.sqlcode < 0 then 
//			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
//			return -1
//		 end if
//		 
//			 
//		select des_prodotto
//		into   :ls_des_prodotto
//		from   anag_prodotti
//		where  cod_azienda=:s_cs_xx.cod_azienda
//		and    cod_prodotto=:ls_cod_prodotto;
//		
//		if sqlca.sqlcode < 0 then 
//			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
//			return -1
//		end if
//		
//		select des_colore_tessuto
//		into   :ls_des_colore_tessuto
//		from   tab_colori_tessuti
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_colore_tessuto = :ls_cod_colore_tessuto;
//		
//		if sqlca.sqlcode < 0 then 
//			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
//			return -1
//		end if
//	
//		select des_colore_tessuto
//		into   :ls_des_colore_mant
//		from   tab_colori_tessuti
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_colore_tessuto = :ls_cod_colore_mant;
//		
//		if sqlca.sqlcode < 0 then 
//			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
//			return -1
//		end if
	
		
//		dw_report_produzione.setitem(1,"cod_colore_tessuto", ls_cod_colore_tessuto)
//		dw_report_produzione.setitem(1,"des_colore_tessuto", ls_des_colore_tessuto)
//		dw_report_produzione.setitem(1,"cod_colore_mant", ls_cod_colore_mant)
//		dw_report_produzione.setitem(1,"des_colore_mant", ls_des_colore_mant)
//		dw_report_produzione.setitem(1,"flag_tipo_mant", ls_flag_tipo_mant)
//		dw_report_produzione.setitem(1,"colore_passamaneria", ls_colore_passamaneria)
//		dw_report_produzione.setitem(1,"altezza_mant", string(ll_altezza_mantovana))
//	
//		dw_report_produzione.setitem(1,"cod_prodotto_mod",ls_cod_prodotto)
//		dw_report_produzione.setitem(1,"des_prodotto_mod",ls_des_prodotto)
//		dw_report_produzione.setitem(1,"nota",ls_nota_prodotto)
		
//		if ld_dim_x <> 0 then
//			dw_report_produzione.setitem(1,"dim_x",string(round(ld_dim_x,2)))
//		end if
//		
//		if ld_dim_y <> 0 then
//			dw_report_produzione.setitem(1,"dim_y",string(round(ld_dim_y,2)))
//		end if
//		
//		if ld_dim_z <> 0 then
//			dw_report_produzione.setitem(1,"dim_z",string(round(ld_dim_z,2)))
//		end if
//		
//		if ld_dim_t <> 0 then
//			dw_report_produzione.setitem(1,"dim_t",string(round(ld_dim_t,2)))
//		end if
//		
//		dw_report_produzione.setitem(1,"quan_ordine",string(ld_quan_ordine))
		
		ll_num_tenda++		
		
		dw_report_produzione.setitem(1,"data_consegna",string(ldt_data_consegna,"dd/mm/yyyy"))
		
//		dw_report_produzione.setitem(1,"prog_riga_ord_ven",string(ll_prog_riga_ord_ven))
	
		
		// Se c'è destinazione diversa stampo indirizzo destinazione; altrimenti stampo indirizzo normale.
		ls_rag_soc = dw_report_produzione.getitemstring(1,"rag_soc")
		ls_indirizzo_1 = 	dw_report_produzione.getitemstring(1,"indirizzo_1")
		ls_indirizzo_2 = 	dw_report_produzione.getitemstring(1,"indirizzo_2")
	
		ls_rag_soc_diversa = 	dw_report_produzione.getitemstring(1,"rag_soc_diversa")
		ls_ind_1_diversa = 	dw_report_produzione.getitemstring(1,"indirizzo_1_diversa")
		ls_ind_2_diversa = 	dw_report_produzione.getitemstring(1,"indirizzo_2_diversa")
	
		
		if isnull(ls_rag_soc_diversa)  or len(trim(ls_rag_soc_diversa))  < 1 then dw_report_produzione.setitem(1,"rag_soc_diversa", ls_rag_soc)
		if isnull(ls_ind_1_diversa) or len(trim(ls_ind_1_diversa)) < 1 then dw_report_produzione.setitem(1,"indirizzo_1_diversa", ls_indirizzo_1)
		if isnull(ls_ind_2_diversa) or len(trim(ls_ind_2_diversa)) < 1 then dw_report_produzione.setitem(1,"indirizzo_2_diversa", ls_indirizzo_2)
	
		
		if ls_flag_etichetta_produzione = "S" then
			
			// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			//se ci sono colli in tab_ord_ven_colli allora stampa quelli, altrimenti procedi tradizionalmente
			//recupero i colli presenti, sia in caso di avanzamento produzione che semplice ristampa
			//infatti nel caso di avanzamento prod. i colli vengono stampati tutti
			luo_prod = create uo_produzione
			
			if luo_prod.uof_carica_colli_barcode(s_cs_xx.parametri.parametro_ul_1, ls_colli_v[], ls_cod_reparto, ls_errore) < 0 then
				//in ls_msg l'errore
				destroy luo_prod;
				
				//in ls_errore il messaggio
				return -1
			end if
			
			destroy luo_prod;
			
			//recupero il campo sigla dal reparto
			
			select sigla
			into :ls_sigla
			from anag_reparti
			where cod_azienda=:s_cs_xx.cod_azienda and
					cod_reparto=:ls_cod_reparto;
			
			if ls_sigla<>"" and not isnull(ls_sigla) then ls_cod_reparto = ls_sigla			
			// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
						
			
			//Donato 14/03/2011 la funzione chiamata per la ristampa è la stessa che viene usata dopo la chiusura della produzione
			//quindi occorre distinguere, perchè nel casi di ristampa e in presenza di + colli, l'operatore deve poter scegliere quali etichette
			//(cioè di quali colli) ristampare
			ll_tot_colli = upperbound(ls_colli_v[])
			
			if ll_tot_colli>0 then
				//apri un finestra di selezione colli da stampare
				s_cs_xx.parametri.parametro_s_4_a[] = ls_colli_v[]
				
				window_open(w_sel_colli_stampa, 0)
				
				//effettuo il travaso dei colli selezionati per la ristampa
				ls_colli_v[] = s_cs_xx.parametri.parametro_s_4_a[]
				
				//reset del vettore per sicurezza
				s_cs_xx.parametri.parametro_s_4_a[] = ls_vuoto[]
				
				//reset del semaforo per sicurezza
				s_cs_xx.parametri.parametro_s_11 = ""
				
				//potrei aver selezionato NESSUN collo da ristampare
				if upperbound(ls_colli_v) = 0 then
					commit;
					close (parent)
					return
				end if
			end if
			//-------------------------------------------------------------------------------
						
			
			job = PrintOpen( ) 
			
			//stampa il barcode + il progressivo collo
			for ll_i = 1 to upperbound(ls_colli_v)
				for ll_y = 1 to ll_num_copie
					//stampa         * [CSB] [BARCODE] - [i] [CSB] *
					//s_cs_xx.parametri.parametro_ds_1.setitem(1,"barcode", ls_colli_v[ll_i])
					//s_cs_xx.parametri.parametro_ds_1.setitem(1,"num_colli",string(ll_tot_colli))
					dw_report_produzione.setitem(1,"barcode", ls_colli_v[ll_i])
					dw_report_produzione.setitem(1,"num_colli",string(ll_tot_colli))
					
					
					//campo sigla di anag_reparti=numero colli per reparto
					dw_report_produzione.object.t_reparto.text = ls_cod_reparto + "="+string(ll_tot_colli)
					dw_report_produzione.object.t_6.text = "COLLI"
					
					dw_report_produzione.setitem(1,"num_riga_num_tot", string(ll_i) + "/" + string(upperbound(ls_colli_v)) )
					PrintDataWindow(job, dw_report_produzione)
				next
			next
			
			PrintClose(job)
		
		end if

else
	
	// Tende TEcniche;una etichetta ogni tenda (riga)
	//for ll_i = 1 to dw_lista_prodotti.rowcount()
		ll_i = 1
		
		//if dw_lista_prodotti.getitemstring(ll_i, "flag_selezione") = "N" then continue
		
		ls_cod_prodotto = dw_lista_prodotti.getitemstring(ll_i, "cod_prodotto")
		ls_des_prodotto = dw_lista_prodotti.getitemstring(ll_i, "des_prodotto")
		ld_quan_ordine  = dw_lista_prodotti.getitemnumber(ll_i, "quan_ordine")
		ll_prog_riga_ord_ven = dw_lista_prodotti.getitemnumber(ll_i, "prog_riga_ord_ven")
		ls_nota_dettaglio = dw_lista_prodotti.getitemstring(ll_i, "nota_dettaglio")
		ls_nota_prodotto = dw_lista_prodotti.getitemstring(ll_i, "nota_prodotto")
		ldt_data_consegna = dw_lista_prodotti.getitemdatetime(ll_i, "data_consegna")
		
//		select dim_x,   
//				 dim_y,   
//				 dim_z,   
//				 dim_t,
//				 cod_non_a_magazzino,
//				 cod_mant_non_a_magazzino,
//				 flag_tipo_mantovana,
//				 alt_mantovana,
//				 colore_passamaneria
//		into   :ld_dim_x,   
//				 :ld_dim_y,   
//				 :ld_dim_z,   
//				 :ld_dim_t,
//				 :ls_cod_colore_tessuto,
//				 :ls_cod_colore_mant,
//				 :ls_flag_tipo_mant,
//				 :ll_altezza_mantovana,
//				 :ls_colore_passamaneria
//		 from  comp_det_ord_ven 
//		 where cod_azienda=:s_cs_xx.cod_azienda and
//				 anno_registrazione=:li_anno_registrazione and
//				 num_registrazione=:ll_num_registrazione 	 and
//				 prog_riga_ord_ven=:ll_prog_riga_ord_ven;
//	
//		 if sqlca.sqlcode < 0 then 
//			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
//			return -1
//		 end if
//		 
//			 
//		select des_prodotto
//		into   :ls_des_prodotto
//		from   anag_prodotti
//		where  cod_azienda=:s_cs_xx.cod_azienda
//		and    cod_prodotto=:ls_cod_prodotto;
//		
//		if sqlca.sqlcode < 0 then 
//			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
//			return -1
//		end if
//		
//		select des_colore_tessuto
//		into   :ls_des_colore_tessuto
//		from   tab_colori_tessuti
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_colore_tessuto = :ls_cod_colore_tessuto;
//		
//		if sqlca.sqlcode < 0 then 
//			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
//			return -1
//		end if
//	
//		select des_colore_tessuto
//		into   :ls_des_colore_mant
//		from   tab_colori_tessuti
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//				 cod_colore_tessuto = :ls_cod_colore_mant;
//		
//		if sqlca.sqlcode < 0 then 
//			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
//			return -1
//		end if
//	
//		
//		dw_report_produzione.setitem(1,"cod_colore_tessuto", ls_cod_colore_tessuto)
//		dw_report_produzione.setitem(1,"des_colore_tessuto", ls_des_colore_tessuto)
//		dw_report_produzione.setitem(1,"cod_colore_mant", ls_cod_colore_mant)
//		dw_report_produzione.setitem(1,"des_colore_mant", ls_des_colore_mant)
//		dw_report_produzione.setitem(1,"flag_tipo_mant", ls_flag_tipo_mant)
//		dw_report_produzione.setitem(1,"colore_passamaneria", ls_colore_passamaneria)
//		dw_report_produzione.setitem(1,"altezza_mant", string(ll_altezza_mantovana))
//	
//		dw_report_produzione.setitem(1,"cod_prodotto_mod",ls_cod_prodotto)
//		dw_report_produzione.setitem(1,"des_prodotto_mod",ls_des_prodotto)
//		dw_report_produzione.setitem(1,"nota",ls_nota_prodotto)
//		
//		if ld_dim_x <> 0 then
//			dw_report_produzione.setitem(1,"dim_x",string(round(ld_dim_x,2)))
//		end if
//		
//		if ld_dim_y <> 0 then
//			dw_report_produzione.setitem(1,"dim_y",string(round(ld_dim_y,2)))
//		end if
//		
//		if ld_dim_z <> 0 then
//			dw_report_produzione.setitem(1,"dim_z",string(round(ld_dim_z,2)))
//		end if
//		
//		if ld_dim_t <> 0 then
//			dw_report_produzione.setitem(1,"dim_t",string(round(ld_dim_t,2)))
//		end if
//		
//		dw_report_produzione.setitem(1,"quan_ordine",string(ld_quan_ordine))
		
		ll_num_tenda++		
		
		dw_report_produzione.setitem(1,"data_consegna",string(ldt_data_consegna,"dd/mm/yyyy"))
		
//		dw_report_produzione.setitem(1,"prog_riga_ord_ven",string(ll_prog_riga_ord_ven))
	
		
		// Se c'è destinazione diversa stampo indirizzo destinazione; altrimenti stampo indirizzo normale.
		ls_rag_soc = dw_report_produzione.getitemstring(1,"rag_soc")
		ls_indirizzo_1 = 	dw_report_produzione.getitemstring(1,"indirizzo_1")
		ls_indirizzo_2 = 	dw_report_produzione.getitemstring(1,"indirizzo_2")
	
		ls_rag_soc_diversa = 	dw_report_produzione.getitemstring(1,"rag_soc_diversa")
		ls_ind_1_diversa = 	dw_report_produzione.getitemstring(1,"indirizzo_1_diversa")
		ls_ind_2_diversa = 	dw_report_produzione.getitemstring(1,"indirizzo_2_diversa")
	
		
		if isnull(ls_rag_soc_diversa)  or len(trim(ls_rag_soc_diversa))  < 1 then dw_report_produzione.setitem(1,"rag_soc_diversa", ls_rag_soc)
		if isnull(ls_ind_1_diversa) or len(trim(ls_ind_1_diversa)) < 1 then dw_report_produzione.setitem(1,"indirizzo_1_diversa", ls_indirizzo_1)
		if isnull(ls_ind_2_diversa) or len(trim(ls_ind_2_diversa)) < 1 then dw_report_produzione.setitem(1,"indirizzo_2_diversa", ls_indirizzo_2)
	
		
		if ls_flag_etichetta_produzione = "S" then
		
			// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
			//se ci sono colli in tab_ord_ven_colli allora stampa quelli, altrimenti procedi tradizionalmente
			//recupero i colli presenti, sia in caso di avanzamento produzione che semplice ristampa
			//infatti nel caso di avanzamento prod. i colli vengono stampati tutti
			luo_prod = create uo_produzione
			
			if luo_prod.uof_carica_colli_barcode(s_cs_xx.parametri.parametro_ul_1, ls_colli_v[], ls_cod_reparto, ls_errore) < 0 then
				//in ls_msg l'errore
				destroy luo_prod;
				
				//in ls_errore il messaggio
				return -1
			end if
			
			destroy luo_prod;
			
			//recupero il campo sigla dal reparto
			
			select sigla
			into :ls_sigla
			from anag_reparti
			where cod_azienda=:s_cs_xx.cod_azienda and
					cod_reparto=:ls_cod_reparto;
			
			if ls_sigla<>"" and not isnull(ls_sigla) then ls_cod_reparto = ls_sigla			
			// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
						
			
			//Donato 14/03/2011 la funzione chiamata per la ristampa è la stessa che viene usata dopo la chiusura della produzione
			//quindi occorre distinguere, perchè nel casi di ristampa e in presenza di + colli, l'operatore deve poter scegliere quali etichette
			//(cioè di quali colli) ristampare
			ll_tot_colli = upperbound(ls_colli_v[])
			
			if ll_tot_colli>0 then
				//apri un finestra di selezione colli da stampare
				s_cs_xx.parametri.parametro_s_4_a[] = ls_colli_v[]
				
				window_open(w_sel_colli_stampa, 0)
				
				//effettuo il travaso dei colli selezionati per la ristampa
				ls_colli_v[] = s_cs_xx.parametri.parametro_s_4_a[]
				
				//reset del vettore per sicurezza
				s_cs_xx.parametri.parametro_s_4_a[] = ls_vuoto[]
				
	
				
				//potrei aver selezionato NESSUN collo da ristampare
				if upperbound(ls_colli_v) = 0 then
					commit;
					close (parent)
					return
				end if
			end if
			//-------------------------------------------------------------------------------
		
			job = PrintOpen( ) 
		
			if upperbound(ls_colli_v[]) > 0 then
				
				//stampa il barcode + il progressivo collo
				for ll_t = 1 to upperbound(ls_colli_v)
					for ll_y = 1 to ll_num_copie
						//stampa         * [CSB] [BARCODE] - [i] [CSB] *
						dw_report_produzione.setitem(1,"barcode", ls_colli_v[ll_t])
						dw_report_produzione.setitem(1,"num_colli",string(ll_tot_colli))
						
						
						//campo sigla di anag_reparti=numero colli per reparto
						dw_report_produzione.object.t_reparto.text = ls_cod_reparto + "="+string(ll_tot_colli)
						
						dw_report_produzione.object.t_6.text = "COLLI"
						
						dw_report_produzione.setitem(1,"num_riga_num_tot", string(ll_t) + "/" + string(upperbound(ls_colli_v)) )
						PrintDataWindow(job, dw_report_produzione)
					next
				next
				
			end if
		
			PrintClose(job)
	
		end if
	
	//next
	
end if

s_cs_xx.parametri.parametro_i_1 = 1

ii_ok=0

commit;

close (parent)

end event

type dw_lista_prodotti from datawindow within w_stampa_etichette_tecniche
integer x = 23
integer y = 360
integer width = 2949
integer height = 880
integer taborder = 30
string title = "none"
string dataobject = "d_stampa_etichette_tecniche_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_report_produzione from uo_cs_xx_dw within w_stampa_etichette_tecniche
boolean visible = false
integer x = 2341
integer y = 440
integer width = 1714
integer height = 340
integer taborder = 10
string title = "none"
string dataobject = "d_label_produzione_tecniche"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type st_1 from statictext within w_stampa_etichette_tecniche
integer x = 14
integer y = 16
integer width = 2962
integer height = 196
integer textsize = -28
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "STAMPA ETICHETTE PRODUZIONE"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type cb_conferma from commandbutton within w_stampa_etichette_tecniche
integer x = 283
integer y = 620
integer width = 754
integer height = 220
integer taborder = 20
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
boolean default = true
end type

event clicked;integer li_anno_registrazione
long job,ll_num_copie,ll_t,ll_num_registrazione,ll_num_max_tenda,ll_num_tenda,ll_prog_riga_ord_ven, ll_i,&
     ll_prog_riga_ord_ven_cfr,ll_quan_etichetta_produzione, ll_y,ll_num_colli,ll_barcode,ll_num_colli_testata,ll_num_colli_riga, ll_altezza_mantovana
string ls_drop_down,ls_cod_des,ls_cod_reparto,ls_flag_etichetta_apertura,ls_flag_etichetta_produzione,ls_data_consegna, &
       ls_cod_prodotto,ls_des_prodotto, ls_nota_dettaglio, ls_cod_colore_tessuto, ls_des_colore_tessuto, ls_cod_colore_mant, ls_des_colore_mant, &
		 ls_flag_tipo_mant, ls_colore_passamaneria,ls_nota_prodotto,ls_errore, ls_rag_soc, ls_indirizzo_1, ls_ind_1_diversa, &
		 ls_rag_soc_diversa, ls_ind_2_diversa,ls_indirizzo_2,ls_tipo_stampa,ls_cod_tipo_ord_ven
datetime ldt_data_consegna, ldt_data_pronto
decimal ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t,ld_quan_etichetta_produzione, ld_quan_ordine
string ls_barcode, ls_CSB

ll_num_copie = long (em_num_copie.text)
ll_barcode = s_cs_xx.parametri.parametro_ul_1

//lettura parametro aziendale Carattere Speciale barcode (specifica Carico ordini con palmare)
select stringa
into   :ls_CSB
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='CSB' and 
		flag_parametro='S';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Sep", "Errore in ricerca del parametro CSB in tabella parametri azienda: " + sqlca.sqlerrtext, stopsign!)
	return -1
	
elseif sqlca.sqlcode=100 or isnull(ls_CSB) or ls_CSB = "" then	
	ls_CSB = ""
end if
//------------------------------------------------------------------------------------------------------------------------------------


// choose case s_cs_xx.parametri.parametro_s_1

select anno_registrazione,
		 num_registrazione,
		 cod_reparto
into   :li_anno_registrazione,
		 :ll_num_registrazione,
		 :ls_cod_reparto
from   tes_ordini_produzione
where  cod_azienda =: s_cs_xx.cod_azienda and
		 progr_tes_produzione =: s_cs_xx.parametri.parametro_ul_1;

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore in ricerca dati av.prod.: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if


// trovo il tipo di stampa sfuso/tecniche///
select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda and
		 anno_registrazione=:li_anno_registrazione and
		 num_registrazione=:ll_num_registrazione;

if sqlca.sqlcode < 0 then 
	ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

select flag_tipo_bcl
into   :ls_tipo_stampa
from   tab_tipi_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;

if sqlca.sqlcode < 0 then 
	ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

if ls_tipo_stampa = "B" then   // sfuso cambio il dataobject
	ls_errore = dw_report_produzione.dataobject
	dw_report_produzione.dataobject = 'd_label_produzione_sfuso'
end if
//

s_cs_xx.parametri.parametro_ds_1.sharedata(dw_report_produzione)

ls_barcode = "*"+ls_CSB+string(ll_barcode)+ls_CSB+"*"
s_cs_xx.parametri.parametro_ds_1.setitem(1,"barcode",ls_barcode)


select count(*)
into   :ll_num_max_tenda
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda and
		 anno_registrazione=:li_anno_registrazione and
		 num_registrazione=:ll_num_registrazione and
		 num_riga_appartenenza=0 and
		 flag_blocco='N';

if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if
		

select quan_etichetta_produzione, 
       flag_etichetta_produzione
into   :ll_quan_etichetta_produzione,
		 :ls_flag_etichetta_produzione
from   anag_reparti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_reparto = :ls_cod_reparto;
if sqlca.sqlcode < 0 then 
	g_mb.messagebox("Sep","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if
if sqlca.sqlcode = 100 then 
	g_mb.messagebox("Sep","Attenzione: il reparto "+ls_cod_reparto+" è inesistente ",stopsign!)
	return -1
end if


if ls_tipo_stampa = "B" then   // etichetta generale dell'ordine.
		ll_i = 1
		ls_cod_prodotto = dw_lista_prodotti.getitemstring(ll_i, "cod_prodotto")
		ls_des_prodotto = dw_lista_prodotti.getitemstring(ll_i, "des_prodotto")
		ld_quan_ordine  = dw_lista_prodotti.getitemnumber(ll_i, "quan_ordine")
		ll_prog_riga_ord_ven = dw_lista_prodotti.getitemnumber(ll_i, "prog_riga_ord_ven")
		ls_nota_dettaglio = dw_lista_prodotti.getitemstring(ll_i, "nota_dettaglio")
		ls_nota_prodotto = dw_lista_prodotti.getitemstring(ll_i, "nota_prodotto")
		ldt_data_consegna = dw_lista_prodotti.getitemdatetime(ll_i, "data_consegna")
		
		select dim_x,   
				 dim_y,   
				 dim_z,   
				 dim_t,
				 cod_non_a_magazzino,
				 cod_mant_non_a_magazzino,
				 flag_tipo_mantovana,
				 alt_mantovana,
				 colore_passamaneria
		into   :ld_dim_x,   
				 :ld_dim_y,   
				 :ld_dim_z,   
				 :ld_dim_t,
				 :ls_cod_colore_tessuto,
				 :ls_cod_colore_mant,
				 :ls_flag_tipo_mant,
				 :ll_altezza_mantovana,
				 :ls_colore_passamaneria
		 from  comp_det_ord_ven 
		 where cod_azienda=:s_cs_xx.cod_azienda and
				 anno_registrazione=:li_anno_registrazione and
				 num_registrazione=:ll_num_registrazione 	 and
				 prog_riga_ord_ven=:ll_prog_riga_ord_ven;
	
		 if sqlca.sqlcode < 0 then 
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		 end if
		 
			 
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then 
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		select des_colore_tessuto
		into   :ls_des_colore_tessuto
		from   tab_colori_tessuti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_colore_tessuto = :ls_cod_colore_tessuto;
		
		if sqlca.sqlcode < 0 then 
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
	
		select des_colore_tessuto
		into   :ls_des_colore_mant
		from   tab_colori_tessuti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_colore_tessuto = :ls_cod_colore_mant;
		
		if sqlca.sqlcode < 0 then 
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
	
		
		dw_report_produzione.setitem(1,"cod_colore_tessuto", ls_cod_colore_tessuto)
		dw_report_produzione.setitem(1,"des_colore_tessuto", ls_des_colore_tessuto)
		dw_report_produzione.setitem(1,"cod_colore_mant", ls_cod_colore_mant)
		dw_report_produzione.setitem(1,"des_colore_mant", ls_des_colore_mant)
		dw_report_produzione.setitem(1,"flag_tipo_mant", ls_flag_tipo_mant)
		dw_report_produzione.setitem(1,"colore_passamaneria", ls_colore_passamaneria)
		dw_report_produzione.setitem(1,"altezza_mant", string(ll_altezza_mantovana))
	
		dw_report_produzione.setitem(1,"cod_prodotto_mod",ls_cod_prodotto)
		dw_report_produzione.setitem(1,"des_prodotto_mod",ls_des_prodotto)
		dw_report_produzione.setitem(1,"nota",ls_nota_prodotto)
		
		if ld_dim_x <> 0 then
			dw_report_produzione.setitem(1,"dim_x",string(round(ld_dim_x,2)))
		end if
		
		if ld_dim_y <> 0 then
			dw_report_produzione.setitem(1,"dim_y",string(round(ld_dim_y,2)))
		end if
		
		if ld_dim_z <> 0 then
			dw_report_produzione.setitem(1,"dim_z",string(round(ld_dim_z,2)))
		end if
		
		if ld_dim_t <> 0 then
			dw_report_produzione.setitem(1,"dim_t",string(round(ld_dim_t,2)))
		end if
		
		dw_report_produzione.setitem(1,"quan_ordine",string(ld_quan_ordine))
		
		ll_num_tenda++		
		
		dw_report_produzione.setitem(1,"data_consegna",string(ldt_data_consegna,"dd/mm/yyyy"))
		
		dw_report_produzione.setitem(1,"prog_riga_ord_ven",string(ll_prog_riga_ord_ven))
	
		
		// Se c'è destinazione diversa stampo indirizzo destinazione; altrimenti stampo indirizzo normale.
		ls_rag_soc = dw_report_produzione.getitemstring(1,"rag_soc")
		ls_indirizzo_1 = 	dw_report_produzione.getitemstring(1,"indirizzo_1")
		ls_indirizzo_2 = 	dw_report_produzione.getitemstring(1,"indirizzo_2")
	
		ls_rag_soc_diversa = 	dw_report_produzione.getitemstring(1,"rag_soc_diversa")
		ls_ind_1_diversa = 	dw_report_produzione.getitemstring(1,"indirizzo_1_diversa")
		ls_ind_2_diversa = 	dw_report_produzione.getitemstring(1,"indirizzo_2_diversa")
	
		
		if isnull(ls_rag_soc_diversa)  or len(trim(ls_rag_soc_diversa))  < 1 then dw_report_produzione.setitem(1,"rag_soc_diversa", ls_rag_soc)
		if isnull(ls_ind_1_diversa) or len(trim(ls_ind_1_diversa)) < 1 then dw_report_produzione.setitem(1,"indirizzo_1_diversa", ls_indirizzo_1)
		if isnull(ls_ind_2_diversa) or len(trim(ls_ind_2_diversa)) < 1 then dw_report_produzione.setitem(1,"indirizzo_2_diversa", ls_indirizzo_2)
	
		
		if ls_flag_etichetta_produzione = "S" then
			
			job = PrintOpen( ) 
		
			for ll_y = 1 to ll_num_copie
				dw_report_produzione.setitem(1,"num_riga_num_tot", string(ll_y) + "/" + string(integer(ll_num_copie)) )
				PrintDataWindow(job, dw_report_produzione) 
			next
			
			PrintClose(job)
		
		end if

else
	
	// Tende TEcniche;una etichetta ogni tenda
	for ll_i = 1 to dw_lista_prodotti.rowcount()
		
		if dw_lista_prodotti.getitemstring(ll_i, "flag_selezione") = "N" then continue
		
		ls_cod_prodotto = dw_lista_prodotti.getitemstring(ll_i, "cod_prodotto")
		ls_des_prodotto = dw_lista_prodotti.getitemstring(ll_i, "des_prodotto")
		ld_quan_ordine  = dw_lista_prodotti.getitemnumber(ll_i, "quan_ordine")
		ll_prog_riga_ord_ven = dw_lista_prodotti.getitemnumber(ll_i, "prog_riga_ord_ven")
		ls_nota_dettaglio = dw_lista_prodotti.getitemstring(ll_i, "nota_dettaglio")
		ls_nota_prodotto = dw_lista_prodotti.getitemstring(ll_i, "nota_prodotto")
		ldt_data_consegna = dw_lista_prodotti.getitemdatetime(ll_i, "data_consegna")
		
		select dim_x,   
				 dim_y,   
				 dim_z,   
				 dim_t,
				 cod_non_a_magazzino,
				 cod_mant_non_a_magazzino,
				 flag_tipo_mantovana,
				 alt_mantovana,
				 colore_passamaneria
		into   :ld_dim_x,   
				 :ld_dim_y,   
				 :ld_dim_z,   
				 :ld_dim_t,
				 :ls_cod_colore_tessuto,
				 :ls_cod_colore_mant,
				 :ls_flag_tipo_mant,
				 :ll_altezza_mantovana,
				 :ls_colore_passamaneria
		 from  comp_det_ord_ven 
		 where cod_azienda=:s_cs_xx.cod_azienda and
				 anno_registrazione=:li_anno_registrazione and
				 num_registrazione=:ll_num_registrazione 	 and
				 prog_riga_ord_ven=:ll_prog_riga_ord_ven;
	
		 if sqlca.sqlcode < 0 then 
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		 end if
		 
			 
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then 
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		select des_colore_tessuto
		into   :ls_des_colore_tessuto
		from   tab_colori_tessuti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_colore_tessuto = :ls_cod_colore_tessuto;
		
		if sqlca.sqlcode < 0 then 
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
	
		select des_colore_tessuto
		into   :ls_des_colore_mant
		from   tab_colori_tessuti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_colore_tessuto = :ls_cod_colore_mant;
		
		if sqlca.sqlcode < 0 then 
			ls_errore = "Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
	
		
		dw_report_produzione.setitem(1,"cod_colore_tessuto", ls_cod_colore_tessuto)
		dw_report_produzione.setitem(1,"des_colore_tessuto", ls_des_colore_tessuto)
		dw_report_produzione.setitem(1,"cod_colore_mant", ls_cod_colore_mant)
		dw_report_produzione.setitem(1,"des_colore_mant", ls_des_colore_mant)
		dw_report_produzione.setitem(1,"flag_tipo_mant", ls_flag_tipo_mant)
		dw_report_produzione.setitem(1,"colore_passamaneria", ls_colore_passamaneria)
		dw_report_produzione.setitem(1,"altezza_mant", string(ll_altezza_mantovana))
	
		dw_report_produzione.setitem(1,"cod_prodotto_mod",ls_cod_prodotto)
		dw_report_produzione.setitem(1,"des_prodotto_mod",ls_des_prodotto)
		dw_report_produzione.setitem(1,"nota",ls_nota_prodotto)
		
		if ld_dim_x <> 0 then
			dw_report_produzione.setitem(1,"dim_x",string(round(ld_dim_x,2)))
		end if
		
		if ld_dim_y <> 0 then
			dw_report_produzione.setitem(1,"dim_y",string(round(ld_dim_y,2)))
		end if
		
		if ld_dim_z <> 0 then
			dw_report_produzione.setitem(1,"dim_z",string(round(ld_dim_z,2)))
		end if
		
		if ld_dim_t <> 0 then
			dw_report_produzione.setitem(1,"dim_t",string(round(ld_dim_t,2)))
		end if
		
		dw_report_produzione.setitem(1,"quan_ordine",string(ld_quan_ordine))
		
		ll_num_tenda++		
		
		dw_report_produzione.setitem(1,"data_consegna",string(ldt_data_consegna,"dd/mm/yyyy"))
		
		dw_report_produzione.setitem(1,"prog_riga_ord_ven",string(ll_prog_riga_ord_ven))
	
		
		// Se c'è destinazione diversa stampo indirizzo destinazione; altrimenti stampo indirizzo normale.
		ls_rag_soc = dw_report_produzione.getitemstring(1,"rag_soc")
		ls_indirizzo_1 = 	dw_report_produzione.getitemstring(1,"indirizzo_1")
		ls_indirizzo_2 = 	dw_report_produzione.getitemstring(1,"indirizzo_2")
	
		ls_rag_soc_diversa = 	dw_report_produzione.getitemstring(1,"rag_soc_diversa")
		ls_ind_1_diversa = 	dw_report_produzione.getitemstring(1,"indirizzo_1_diversa")
		ls_ind_2_diversa = 	dw_report_produzione.getitemstring(1,"indirizzo_2_diversa")
	
		
		if isnull(ls_rag_soc_diversa)  or len(trim(ls_rag_soc_diversa))  < 1 then dw_report_produzione.setitem(1,"rag_soc_diversa", ls_rag_soc)
		if isnull(ls_ind_1_diversa) or len(trim(ls_ind_1_diversa)) < 1 then dw_report_produzione.setitem(1,"indirizzo_1_diversa", ls_indirizzo_1)
		if isnull(ls_ind_2_diversa) or len(trim(ls_ind_2_diversa)) < 1 then dw_report_produzione.setitem(1,"indirizzo_2_diversa", ls_indirizzo_2)
	
		
		job = PrintOpen( ) 
		for ll_t = 1 to ld_quan_ordine
			
			if ls_flag_etichetta_produzione = "S" then
				for ll_y = 1 to ll_num_copie
					dw_report_produzione.setitem(1,"num_riga_num_tot", string(ll_t) + "/" + string(integer(ld_quan_ordine)) )
					PrintDataWindow(job, dw_report_produzione) 
				next
			end if
			
		next
		PrintClose(job)
	
	//loop
	next
	
end if

s_cs_xx.parametri.parametro_i_1 = 1

ii_ok=0

commit;

close (parent)

end event

type cb_chiudi from commandbutton within w_stampa_etichette_tecniche
event clicked pbm_bnclicked
integer x = 1851
integer y = 616
integer width = 782
integer height = 220
integer taborder = 10
boolean bringtotop = true
integer textsize = -24
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = 1
ii_ok = 0

commit;

close (parent)
end event

type dw_folder from u_folder within w_stampa_etichette_tecniche
integer x = 23
integer y = 240
integer width = 2994
integer height = 1040
integer taborder = 40
end type

type st_2 from statictext within w_stampa_etichette_tecniche
integer x = 37
integer y = 380
integer width = 2066
integer height = 100
boolean bringtotop = true
integer textsize = -16
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "QUANTITA~' ETICHETTE PRODUZIONE ="
alignment alignment = right!
boolean focusrectangle = false
end type

type em_num_copie from editmask within w_stampa_etichette_tecniche
integer x = 2126
integer y = 360
integer width = 347
integer height = 152
integer taborder = 10
boolean bringtotop = true
integer textsize = -18
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#"
boolean spin = true
end type

