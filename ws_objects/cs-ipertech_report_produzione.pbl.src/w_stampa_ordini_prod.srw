﻿$PBExportHeader$w_stampa_ordini_prod.srw
$PBExportComments$Window stampa ordini di produzione
forward
global type w_stampa_ordini_prod from w_cs_xx_principale
end type
type gb_1 from groupbox within w_stampa_ordini_prod
end type
type st_debugmode from statictext within w_stampa_ordini_prod
end type
type st_debug from statictext within w_stampa_ordini_prod
end type
type hpb_1 from hprogressbar within w_stampa_ordini_prod
end type
type st_ordini_altro_stab from statictext within w_stampa_ordini_prod
end type
type st_stab_operatore from statictext within w_stampa_ordini_prod
end type
type em_quan_etichetta_gen from editmask within w_stampa_ordini_prod
end type
type cb_etichetta_gen from commandbutton within w_stampa_ordini_prod
end type
type cbx_tende_tecniche from checkbox within w_stampa_ordini_prod
end type
type cbx_etichette_sole from checkbox within w_stampa_ordini_prod
end type
type cb_etichette_cg from commandbutton within w_stampa_ordini_prod
end type
type st_num_copie_sfuso from statictext within w_stampa_ordini_prod
end type
type em_num_copie_sfuso from editmask within w_stampa_ordini_prod
end type
type st_num_copie_tecniche from statictext within w_stampa_ordini_prod
end type
type em_num_copie_tecniche from editmask within w_stampa_ordini_prod
end type
type st_num_copie_sole from statictext within w_stampa_ordini_prod
end type
type em_num_copie_sole from editmask within w_stampa_ordini_prod
end type
type cb_ordini_senza_commessa from commandbutton within w_stampa_ordini_prod
end type
type cb_ordini_senza_commessa_esc from commandbutton within w_stampa_ordini_prod
end type
type st_3 from statictext within w_stampa_ordini_prod
end type
type st_4 from statictext within w_stampa_ordini_prod
end type
type st_5 from statictext within w_stampa_ordini_prod
end type
type st_6 from statictext within w_stampa_ordini_prod
end type
type cbx_select_all from checkbox within w_stampa_ordini_prod
end type
type cbx_ordini_senza_commessa from checkbox within w_stampa_ordini_prod
end type
type dw_ordini_senza_commessa from uo_cs_xx_dw within w_stampa_ordini_prod
end type
type dw_report from uo_cs_xx_dw within w_stampa_ordini_prod
end type
type dw_lista_ord_ven_altro_deposito from uo_cs_xx_dw within w_stampa_ordini_prod
end type
type dw_folder from u_folder within w_stampa_ordini_prod
end type
type st_1 from statictext within w_stampa_ordini_prod
end type
type dw_lista_ord_ven_stampa_prod from uo_cs_xx_dw within w_stampa_ordini_prod
end type
type dw_ottimizza_4 from uo_std_dw within w_stampa_ordini_prod
end type
type dw_ottimizza_3 from datawindow within w_stampa_ordini_prod
end type
type cb_stampa from commandbutton within w_stampa_ordini_prod
end type
type dw_elenco_operatori_selezionati from datawindow within w_stampa_ordini_prod
end type
type dw_ottimizza_1 from uo_std_dw within w_stampa_ordini_prod
end type
type cb_etichette from commandbutton within w_stampa_ordini_prod
end type
type dw_ext_selezione from uo_cs_xx_dw within w_stampa_ordini_prod
end type
type cb_lancia from commandbutton within w_stampa_ordini_prod
end type
type cbx_gen_commesse from checkbox within w_stampa_ordini_prod
end type
type tv_lancio from uo_tree_lancio_produzione within w_stampa_ordini_prod
end type
type dw_gg_lancio from datawindow within w_stampa_ordini_prod
end type
end forward

global type w_stampa_ordini_prod from w_cs_xx_principale
integer width = 4571
integer height = 2628
string title = "Lancio - Stampa Ordini di Produzione"
event ue_post_open ( )
event ue_aggiungi_a_sessione ( )
event ue_nuovo_lancio ( )
event ue_elimina_lancio ( )
event ue_lancia_produzione ( )
event ue_stampa_ordini ( )
event ue_ottimizza ( )
event ue_stampa_lista_preparazione ( )
event ue_excel_lista_accessori ( )
event ue_nuova_sessione_da_righe ( )
event ue_tree_lancio_prod_refresh ( )
event ue_mostra_sessioni_chiuse ( )
gb_1 gb_1
st_debugmode st_debugmode
st_debug st_debug
hpb_1 hpb_1
st_ordini_altro_stab st_ordini_altro_stab
st_stab_operatore st_stab_operatore
em_quan_etichetta_gen em_quan_etichetta_gen
cb_etichetta_gen cb_etichetta_gen
cbx_tende_tecniche cbx_tende_tecniche
cbx_etichette_sole cbx_etichette_sole
cb_etichette_cg cb_etichette_cg
st_num_copie_sfuso st_num_copie_sfuso
em_num_copie_sfuso em_num_copie_sfuso
st_num_copie_tecniche st_num_copie_tecniche
em_num_copie_tecniche em_num_copie_tecniche
st_num_copie_sole st_num_copie_sole
em_num_copie_sole em_num_copie_sole
cb_ordini_senza_commessa cb_ordini_senza_commessa
cb_ordini_senza_commessa_esc cb_ordini_senza_commessa_esc
st_3 st_3
st_4 st_4
st_5 st_5
st_6 st_6
cbx_select_all cbx_select_all
cbx_ordini_senza_commessa cbx_ordini_senza_commessa
dw_ordini_senza_commessa dw_ordini_senza_commessa
dw_report dw_report
dw_lista_ord_ven_altro_deposito dw_lista_ord_ven_altro_deposito
dw_folder dw_folder
st_1 st_1
dw_lista_ord_ven_stampa_prod dw_lista_ord_ven_stampa_prod
dw_ottimizza_4 dw_ottimizza_4
dw_ottimizza_3 dw_ottimizza_3
cb_stampa cb_stampa
dw_elenco_operatori_selezionati dw_elenco_operatori_selezionati
dw_ottimizza_1 dw_ottimizza_1
cb_etichette cb_etichette
dw_ext_selezione dw_ext_selezione
cb_lancia cb_lancia
cbx_gen_commesse cbx_gen_commesse
tv_lancio tv_lancio
dw_gg_lancio dw_gg_lancio
end type
global w_stampa_ordini_prod w_stampa_ordini_prod

type variables
string is_CSB
string is_cod_deposito_operatore
string is_cod_deposito_CSSYSTEM = "_TUTTI_"
string is_ETIC = "ETIC"  //codice colonna dinamica attivata su anag clienti

longlong il_sessione

string is_path_filigrana

boolean ib_debug_mode = false
boolean ib_log = false
string is_nome_file_log

uo_fido_cliente			iuo_fido
string						is_cod_parametro_blocco = "LPD"


private:
	boolean 	ib_aip = false
	boolean 	ib_stampa_commesse = false
	string		is_origine_comando
	long		id_lancio_produzione_ottimizzato

end variables

forward prototypes
public function integer f_giorno_settimana (datetime fdt_data, ref string fs_giorno)
public function string wf_elabora (string fs_stringa)
public function integer f_report_1 (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_reparto, long fl_prog_riga_comanda, long fl_num_tenda)
public function integer f_report_2 (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_reparto)
public function integer f_report_3 (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_reparto)
public function integer f_etichetta_cg ()
public function integer f_etichetta_gen ()
public function integer wf_get_stabilimento_operatore ()
public function boolean wf_controlla_reparto (string fs_cod_reparto)
public function integer wf_stampa_ordini (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_tipo_ord_ven, datetime fdt_data_consegna, ref string fs_errore)
public subroutine wf_set_deposito_origine (long fl_anno_registrazione, long fl_num_registrazione, ref datawindow fdw_report)
public function integer wf_aggiungi_buffer_stabilimento (integer fi_anno_registrazione, long fl_num_registrazione, string fs_codice, string fs_tipo_codice, ref string fs_errore)
public function long wf_retrieve_altri_stabilimenti ()
public subroutine wf_stampa_altri_ordini ()
public function longlong wf_get_id_sessione ()
public function string wf_get_printer_name ()
public function integer wf_invia_mail ()
public function integer wf_invia_mail (string fs_messaggio, string fs_deposito_prod)
public function string wf_controlla_allegati (integer fi_anno_registrazione, long fl_num_registrazione)
public function string wf_get_colonna_dinamica (string fs_cod_cliente)
public function integer wf_ordini_senza_commessa (long fi_anno_registrazione, long fl_num_registrazione, ref string fs_errore)
public function integer wf_scrivi_log (long fl_anno, long fl_numero, string fs_cod_reparto, string fs_msg_aggiuntivo)
public function integer wf_get_data_pronto (integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_cod_reparto, ref datetime adt_data_pronto)
public function string wf_logo ()
public subroutine wf_carica_operatori ()
public function integer wf_get_operatori (ref string as_operatori)
public function integer wf_giacenza (long al_row, string as_cod_prodotto, string as_cod_deposito, ref string as_errore)
public subroutine wf_clicked_ottimizza ()
public function integer wf_ottimizza (long al_anno_ordine, long al_num_ordine, ref string as_errore)
public subroutine wf_carica_lista_lancio_prod ()
public function integer f_report_3_plastinds (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_reparto)
public function integer f_report_schede_distinta (long al_job, long al_anno_ordine, long al_num_ordine, ref string as_errore)
public function integer f_report_schede_distinta_singola_riga (long al_job, long al_anno_ordine, long al_num_ordine, long al_prog_riga_ord_ven, ref string as_errore)
public function integer wf_lancio_prod_new (string as_action, ref long al_id_tes_lancio_prod, ref string as_messaggio)
public subroutine wf_lancio_produzione (string as_origine_comando)
public subroutine wf_clicked_stampa (boolean fb_check_commessa, string as_origine_comando)
public function integer f_report_op_scheda_singola (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_reparto, long fl_prog_riga_comanda, long fl_num_tenda)
public function integer wf_report_1_leggi_dt (string as_cod_prodotto_finito, string as_cod_versione_finito, long al_anno_dt, long al_num_dt, long al_prog_dt, ref s_report_ol_produzione_dt astr_dt[], ref string as_errore)
public function integer f_calcola_dt (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_versione, string fs_tipo_ordinamento_dt, ref string fs_errore)
public function integer wf_get_variabile_conf (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_cod_variabile, ref string as_valore_stringa, ref decimal ad_valore_numero, ref string as_errore)
public function integer wf_crea_commesse (long fl_anno_reg_ord_ven, long fl_num_reg_ord_ven, long fl_prog_riga_ord_ven, string fs_cod_tipo_ord_ven, ref string fs_errore)
end prototypes

event ue_post_open();dw_ext_selezione.setitem(1, "anno_ordine", f_anno_esercizio())
end event

event ue_aggiungi_a_sessione();string	ls_messaggio
long ll_id_tes_lancio_prod
treeviewitem ltv_item
s_tree_lancio_produzione ls_record, ls_record_null

if isnull(tv_lancio.il_handle) or tv_lancio.il_handle <= 0 then
	return
end if

tv_lancio.getitem(tv_lancio.il_handle,ltv_item)

ls_record = ltv_item.data
if ls_record.livello = -1 then
	g_mb.warning("Attenzione: si sta cercando di aggiungere ordini ad una sessione inesistente")
	return
end if

ll_id_tes_lancio_prod = ls_record.id_padre

if wf_lancio_prod_new("A", ll_id_tes_lancio_prod, ls_messaggio) < 0 then
	g_mb.error(ls_messaggio)
	rollback;
else
	commit;
end if

tv_lancio.uof_carica_tv(dw_gg_lancio.getitemnumber(1,1), ls_messaggio )
end event

event ue_nuovo_lancio();string	ls_messaggio
long ll_id_tes_lancio_prod
treeviewitem ltv_item
s_tree_lancio_produzione ls_record, ls_record_null

if isnull(tv_lancio.il_handle) or tv_lancio.il_handle <= 0 then
	return
end if

tv_lancio.getitem(tv_lancio.il_handle,ltv_item)

ls_record = ltv_item.data
if ls_record.livello <> -1 then
	g_mb.warning("Attenzione: si sta cercando di creare una nuova sessione su un ramo che è già una sessione!")
	return
end if

ll_id_tes_lancio_prod = ls_record.id_padre

if wf_lancio_prod_new("N", ll_id_tes_lancio_prod, ls_messaggio) < 0 then
	g_mb.error(ls_messaggio)
	rollback;
else
	commit;
end if

tv_lancio.uof_carica_tv(dw_gg_lancio.getitemnumber(1,1), ls_messaggio )

end event

event ue_elimina_lancio();string	ls_str, ls_messaggio
long ll_i, ll_id_tes_lancio_prod
treeviewitem ltv_item
s_tree_lancio_produzione ls_record, ls_record_null

if isnull(tv_lancio.il_handle) or tv_lancio.il_handle <= 0 then
	return
end if

tv_lancio.getitem(tv_lancio.il_handle,ltv_item)

ls_record = ltv_item.data
if ls_record.livello = -1 then
	g_mb.warning("Attenzione: si sta cercando di aggiungere ordini ad una sessione inesistente")
	return
end if


ll_id_tes_lancio_prod = ls_record.id_padre

if g_mb.confirm("Cancello il lancio di produzione selezionato?") then
	delete 	det_lancio_prod_ottimiz
	where 	id_tes_lancio_prod = :ll_id_tes_lancio_prod;
	if sqlca.sqlcode < 0 then
		g_mb.error( g_str.format("Errore in cancellazione dati ottimizzazione.~r~n $1", sqlca.sqlerrtext ) )
		rollback;
		return
	end if
				
	delete 	det_lancio_prod
	where 	id_tes_lancio_prod = :ll_id_tes_lancio_prod;
	if sqlca.sqlcode < 0 then
		g_mb.error( g_str.format("Errore in cancellazione dettaglio lancio produzione.~r~n $1", sqlca.sqlerrtext ) )
		rollback;
		return
	end if
				
	delete 	tes_lancio_prod
	where 	id = :ll_id_tes_lancio_prod;
	if sqlca.sqlcode < 0 then
		g_mb.error( g_str.format("Errore in cancellazione testata lancio produzione.~r~n $1", sqlca.sqlerrtext ) )
		rollback;
		return
	end if
				
	commit;
	
tv_lancio.deleteitem( tv_lancio.il_handle )

end if		



end event

event ue_lancia_produzione();wf_lancio_produzione("T")
end event

event ue_stampa_ordini();wf_clicked_stampa( true, "T")
end event

event ue_ottimizza();wf_clicked_ottimizza()
end event

event ue_stampa_lista_preparazione();long	ll_ret
treeviewitem 	ltv_item
s_tree_lancio_produzione 	ls_record


if isnull(tv_lancio.il_handle) or tv_lancio.il_handle <= 0 then
	g_mb.warning("Selezionare una sessione valida!")
	return 
end if

tv_lancio.getitem(tv_lancio.il_handle,ltv_item)

ls_record = ltv_item.data
// tutte le sessioni hanno il livello ZERO
if ls_record.livello <> 0 then
	g_mb.warning("Selezionare una sessione valida!")
	return 
end if
	
dw_report.reset()
dw_report.dataobject='d_report_lista_preparazione'
dw_report.settransobject( sqlca)
ll_ret = dw_report.retrieve(ls_record.id_padre)
if ll_ret < 0 then
	g_mb.error("Errore durante la lettura dei dati della lista di preaprazione")
	return
end if

if ll_ret = 0 then
	g_mb.show( "Non vi sono dati da stampare: verificare di aver impostato il tipo ottimizzazione in anmagrafica prodotti" )
	return
end if

dw_report.print( )

return 
end event

event ue_excel_lista_accessori();// lista accessori tramite Excel
string		ls_sql, ls_sheetname, ls_str, ls_errore, ls_cod_comando, ls_des_comando, ls_str_old, ls_filename, ls_sql_opt_cfg, ls_cod_opt_cfg
long		ll_ret, ll_i, ll_cont, ll_anno_ordine, ll_num_ordine, ll_prog_riga_ord_ven, ll_ret_opt_cfg, ll_y, ll_prog_riga_opt_cfg
dec{4}	ld_alt_asta, ld_quan_prodotto_finito

datastore						lds_data, lds_data_opt_cfg
treeviewitem 					ltv_item
s_tree_lancio_produzione 	ls_record
uo_excel							luo_excel

ls_sql = "select A.anno_reg_ord_ven anno_reg_ord_ven,   &
 A.num_reg_ord_ven num_reg_ord_ven,   &
 A.prog_riga_ord_ven prog_riga_ord_ven,  &
 D.cod_prodotto det_ord_cod_prodotto,  &
 D.des_prodotto det_ord_des_prodotto,  &
 '' + isnull(V.cod_prodotto,B.cod_prodotto_figlio) cod_prodotto_figlio,  &
 '' + isnull(C1.des_prodotto,C.des_prodotto) des_prodotto_figlio, &
 B.misura misura ,  &
 T.um_misura um_misura, &
 B.quantita quantita,   &
 T.um_quantita um_quantita,  &
 A.id_tes_lancio_prod id_tes_lancio_prod,  &
 E.cod_cliente cod_cliente, &
 CLI.rag_soc_1 rag_soc_1, &
 'P' tipo_riga &
 from det_lancio_prod A   &
 join distinte_taglio_calcolate B on A.cod_azienda = B.cod_azienda and A.anno_reg_ord_ven = B.anno_registrazione and A.num_reg_ord_ven=B.num_registrazione and A.prog_riga_ord_ven=B.prog_riga_ord_ven  &
 left outer join anag_prodotti C on B.cod_azienda = C.cod_azienda and B.cod_prodotto_figlio=C.cod_prodotto  &
 join det_ord_ven D on  A.cod_azienda = D.cod_azienda and A.anno_reg_ord_ven = D.anno_registrazione and A.num_reg_ord_ven=D.num_registrazione and A.prog_riga_ord_ven=D.prog_riga_ord_ven  &
 join tes_ord_ven E on  A.cod_azienda = E.cod_azienda and A.anno_reg_ord_ven = E.anno_registrazione and A.num_reg_ord_ven=E.num_registrazione    &
 left outer join varianti_det_ord_ven V on B.cod_azienda=V.cod_azienda and B.anno_registrazione = V.anno_registrazione and B.num_registrazione = V.num_registrazione and   &
 B.prog_riga_ord_ven = V.prog_riga_ord_ven and B.cod_prodotto_padre=V.cod_prodotto_padre and B.num_sequenza = V.num_sequenza and B.cod_prodotto_figlio = V.cod_prodotto_figlio and   &
 B.cod_versione = V.cod_versione and B.cod_versione_figlio = V.cod_versione_figlio  &
 left outer join anag_prodotti C1 on V.cod_azienda = C1.cod_azienda and V.cod_prodotto=C1.cod_prodotto  &
 join tab_distinte_taglio T on B.cod_azienda = T.cod_azienda and B.cod_prodotto_padre = T.cod_prodotto_padre and B.num_sequenza = T.num_sequenza and B.cod_prodotto_figlio = T.cod_prodotto_figlio and   &
 B.cod_versione = T.cod_versione and B.cod_versione_figlio = T.cod_versione_figlio and B.progressivo = T.progressivo  &
 left join anag_clienti CLI on CLI.cod_azienda=E.cod_azienda and CLI.cod_cliente=E.cod_cliente  &
 where id_tes_lancio_prod = $1   &
 union all   &
 select A.anno_reg_ord_ven anno_reg_ord_ven,  &
 A.num_reg_ord_ven num_reg_ord_ven,  &
 D.prog_riga_ord_ven prog_riga_ord_ven,   &
 D1.cod_prodotto det_ord_cod_prodotto,  &
 C1.des_prodotto det_ord_des_prodotto, &
 D.cod_prodotto cod_prodotto_figlio,  &
 C.des_prodotto des_prodotto_figlio,   &
 D.dim_x misura ,  &
 '' um_misura,  &
 D.quan_ordine quantita,  &
 D.cod_misura,  &
 A.id_tes_lancio_prod id_tes_lancio_prod,  &
 E.cod_cliente cod_cliente, &
 CLI.rag_soc_1 rag_soc_1, &
 'R' tipo_riga &
 from det_lancio_prod A  &
 left join det_ord_ven D on  A.cod_azienda = D.cod_azienda and A.anno_reg_ord_ven = D.anno_registrazione and A.num_reg_ord_ven=D.num_registrazione  and A.prog_riga_ord_ven = D.num_riga_appartenenza  &
 left join det_ord_ven D1 on  A.cod_azienda = D1.cod_azienda and A.anno_reg_ord_ven = D1.anno_registrazione and A.num_reg_ord_ven=D1.num_registrazione and A.prog_riga_ord_ven=D1.prog_riga_ord_ven  &
 left outer join anag_prodotti C on D.cod_azienda = C.cod_azienda and D.cod_prodotto=C.cod_prodotto   &
 left outer join anag_prodotti C1 on D1.cod_azienda = C1.cod_azienda and D1.cod_prodotto=C1.cod_prodotto  &
 left join tes_ord_ven E on  A.cod_azienda = E.cod_azienda and A.anno_reg_ord_ven = E.anno_registrazione and A.num_reg_ord_ven=E.num_registrazione  &
 left join anag_clienti CLI on CLI.cod_azienda=E.cod_azienda and CLI.cod_cliente=E.cod_cliente  &
 where id_tes_lancio_prod = $1  &
 and D.num_riga_appartenenza > 0 and D.num_riga_appartenenza is not null   &
 order by 1,2,3,6,8 "

ls_sql_opt_cfg = " select A.anno_reg_ord_ven anno_reg_ord_ven,   " + &
" A.num_reg_ord_ven num_reg_ord_ven,   " + &
" D1.prog_riga_ord_ven prog_riga_ord_ven,  " + &
" D1.cod_prodotto det_ord_cod_prodotto,  " + &
" D1.des_prodotto det_ord_des_prodotto,  " + &
" '' + isnull(V.cod_prodotto,B.cod_prodotto_figlio) cod_prodotto_figlio,  " + &
" '' + isnull(C1.des_prodotto,C.des_prodotto) des_prodotto_figlio, " + &
" B.misura misura  , " + &
" T.um_misura um_misura, " + &
" B.quantita quantita,   " + &
" T.um_quantita um_quantita,  " + &
" A.id_tes_lancio_prod id_tes_lancio_prod,  " + &
" E.cod_cliente cod_cliente, " + &
" CLI.rag_soc_1 rag_soc_1, " + &
" 'A' tipo_riga " + &
"   from det_lancio_prod A   " + &
" join det_ord_ven D on  A.cod_azienda = D.cod_azienda and A.anno_reg_ord_ven = D.anno_registrazione and A.num_reg_ord_ven=D.num_registrazione and A.prog_riga_ord_ven=D.prog_riga_ord_ven " + &
" join det_ord_ven D1 on  D1.cod_azienda = D.cod_azienda and D1.anno_registrazione  = D.anno_registrazione and D1.num_registrazione =D.num_registrazione and D1.num_riga_appartenenza=D.prog_riga_ord_ven " + &
" left join distinte_taglio_calcolate B on B.cod_azienda = D1.cod_azienda and B.anno_registrazione = D1.anno_registrazione and B.num_registrazione=D1.num_registrazione and B.prog_riga_ord_ven=D1.prog_riga_ord_ven  " + &
" left outer join anag_prodotti C on B.cod_azienda = C.cod_azienda and B.cod_prodotto_figlio=C.cod_prodotto  " + &
" join tes_ord_ven E on  A.cod_azienda = E.cod_azienda and A.anno_reg_ord_ven = E.anno_registrazione and A.num_reg_ord_ven=E.num_registrazione    " + &
" left outer join varianti_det_ord_ven V on B.cod_azienda=V.cod_azienda and B.anno_registrazione = V.anno_registrazione and B.num_registrazione = V.num_registrazione and   " + &
" B.prog_riga_ord_ven = V.prog_riga_ord_ven and B.cod_prodotto_padre=V.cod_prodotto_padre and B.num_sequenza = V.num_sequenza and B.cod_prodotto_figlio = V.cod_prodotto_figlio and   " + &
" B.cod_versione = V.cod_versione and B.cod_versione_figlio = V.cod_versione_figlio  " + &
"  left outer join anag_prodotti C1 on V.cod_azienda = C1.cod_azienda and V.cod_prodotto=C1.cod_prodotto  " + &
" join tab_distinte_taglio T on B.cod_azienda = T.cod_azienda and B.cod_prodotto_padre = T.cod_prodotto_padre and B.num_sequenza = T.num_sequenza and B.cod_prodotto_figlio = T.cod_prodotto_figlio and   " + &
" B.cod_versione = T.cod_versione and B.cod_versione_figlio = T.cod_versione_figlio and B.progressivo = T.progressivo  " + &
" left join anag_clienti CLI on CLI.cod_azienda=E.cod_azienda and CLI.cod_cliente=E.cod_cliente  " + &
" where id_tes_lancio_prod = $1  and D1.prog_riga_ord_ven = $2 and D1.cod_prodotto = '$3' and A.anno_reg_ord_ven=$4 and A.num_reg_ord_ven=$5 " + &
" order by 1,2,3,6,8 "

if isnull(tv_lancio.il_handle) or tv_lancio.il_handle <= 0 then
	g_mb.warning("Selezionare una sessione valida!")
	return 
end if

tv_lancio.getitem(tv_lancio.il_handle,ltv_item)

ls_record = ltv_item.data
// tutte le sessioni hanno il livello ZERO
if ls_record.livello <> 0 then
	g_mb.warning("Selezionare una sessione valida!")
	return 
end if

ls_sql = g_str.format(ls_sql, ls_record.id_padre)


ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore )
if ll_ret = 0 then
	g_mb.show("Non c'è alcun dato nella disinta di taglio per la generazione della lista accessori.")
	return
elseif ll_ret < 0 then
	g_mb.error( g_str.format("Errore SQL in fase di estrazione dei dati~r~n$1",ls_errore))
	return
else
	g_mb.show("Il sistema ora procederà con la generazione del foglio Excel con i dati della lista accessori.~r~nNON CLICCARE sul foglio Excel fino al termine dell'elaborazione!")
end if

luo_excel = CREATE uo_excel
ls_filename = g_str.format(guo_functions.uof_get_user_documents_folder( ) +"liste_preparazione\$1.xls",ls_record.id_padre)
luo_excel.uof_create( ls_filename, true)
ls_sheetname = g_str.format("Accessori Lancio Prod $1 ",ls_record.id_padre)
luo_excel.uof_add_sheet(ls_sheetname, true)
luo_excel.uof_set_sheet(ls_sheetname)

luo_excel.uof_set( 1, 1, G_STR.FORMAT("LISTA PREPARAZIONE ACCESSORI        LANCIO PRODUZIONE NR $1",ls_record.id_padre))
luo_excel.uof_set_bold(1, 1)
//luo_excel.uof_set_font_size(1,1,14)
luo_excel.uof_set_font_name(1,1,"ARIAL BLACK")
luo_excel.uof_merge(1, 1, 1, 7)


luo_excel.uof_set( 2, 1, "ORDINE")
luo_excel.uof_set_bold(2, 1)
luo_excel.uof_set_autofit(2,1)

luo_excel.uof_set( 2, 2, "CLIENTE")
luo_excel.uof_set_bold(2, 2)
luo_excel.uof_set_autofit(2,2)

luo_excel.uof_set( 2, 3, "ARTICOLO")
luo_excel.uof_set_bold(2, 3)
luo_excel.uof_set_autofit(2,3)

luo_excel.uof_set( 2, 4, "MATERIA PRIMA")
luo_excel.uof_set_bold(2, 4)
luo_excel.uof_set_autofit(2,4)

luo_excel.uof_set( 2, 5, "DESCRIZIONE MATERIA PRIMA")
luo_excel.uof_set_bold(2, 5)
luo_excel.uof_set_autofit(2,5)

luo_excel.uof_set( 2, 6, "CM")
luo_excel.uof_set_bold(2, 6)

luo_excel.uof_set( 2, 7, "Q.TA")
luo_excel.uof_set_bold(2, 7)

ls_str_old = ""

ll_cont = 0
for ll_i = 1 to ll_ret
	if (lds_data.getitemnumber(ll_i,8) = 0 or isnull(lds_data.getitemnumber(ll_i,8))) and (lds_data.getitemnumber(ll_i,10)=0 or isnull(lds_data.getitemnumber(ll_i,10))) then continue
	
	ll_prog_riga_opt_cfg = lds_data.getitemnumber(ll_i,3)
	ls_cod_opt_cfg = lds_data.getitemstring(ll_i,6)
	
	ls_sql = g_str.format(ls_sql_opt_cfg, ls_record.id_padre, ll_prog_riga_opt_cfg, ls_cod_opt_cfg, lds_data.getitemnumber(ll_i,1),lds_data.getitemnumber(ll_i,2))
	ll_ret_opt_cfg = guo_functions.uof_crea_datastore(lds_data_opt_cfg, ls_sql, ls_errore )
	if ll_ret_opt_cfg < 0 then
		g_mb.error( g_str.format("Errore SQL in fase di estrazione dei dati~r~n$1",ls_errore))
		return
	end if	
	
	if ll_ret_opt_cfg > 0 then
		for ll_y = 1 to ll_ret_opt_cfg
			ls_str = g_str.format("$1_$2", lds_data_opt_cfg.getitemnumber(ll_y,2), lds_data_opt_cfg.getitemnumber(ll_y,3) )
			ll_cont ++
			luo_excel.uof_set(ll_cont + 2, 1, ls_str )
			luo_excel.uof_set(ll_cont + 2, 2, lds_data_opt_cfg.getitemstring(ll_y,13) )
			luo_excel.uof_set(ll_cont + 2, 3, lds_data_opt_cfg.getitemstring(ll_y,4) )
			luo_excel.uof_set(ll_cont + 2, 4, lds_data_opt_cfg.getitemstring(ll_y,6) )
			luo_excel.uof_set(ll_cont + 2, 5, lds_data_opt_cfg.getitemstring(ll_y,7) )
			luo_excel.uof_set(ll_cont + 2, 6, lds_data_opt_cfg.getitemnumber(ll_y,8) )
			luo_excel.uof_set_format(ll_cont + 2, 6, "##0,00")
			luo_excel.uof_set(ll_cont + 2, 7, g_str.format("$1 $2", lds_data_opt_cfg.getitemstring(ll_y,11), string(lds_data_opt_cfg.getitemnumber(ll_y,10),"###,##0.00" ) ) )
		next
		// salto perchè non voglio che l'optional di cui ho le misure appaia nella lista accessori
		continue	
	end if
	destroy lds_data_opt_cfg

	
	ls_str = g_str.format("$1_$2", lds_data.getitemnumber(ll_i,2), lds_data.getitemnumber(ll_i,3) )
	ll_cont ++
	luo_excel.uof_set(ll_cont + 2, 1, ls_str )
	luo_excel.uof_set(ll_cont + 2, 2, lds_data.getitemstring(ll_i,13) )
	luo_excel.uof_set(ll_cont + 2, 3, lds_data.getitemstring(ll_i,4) )
	luo_excel.uof_set(ll_cont + 2, 4, lds_data.getitemstring(ll_i,6) )
	luo_excel.uof_set(ll_cont + 2, 5, lds_data.getitemstring(ll_i,7) )
	luo_excel.uof_set(ll_cont + 2, 6, lds_data.getitemnumber(ll_i,8) )
	luo_excel.uof_set_format(ll_cont + 2, 6, "##0,00")
	luo_excel.uof_set(ll_cont + 2, 7, g_str.format("$1 $2", lds_data.getitemstring(ll_i,11), string(lds_data.getitemnumber(ll_i,10),"###,##0.00" ) ) )
	
	// Chiesto da Alusistemi 20.03.2018: aggiungere il comando secondo la quantità del prodotto finito
	if ls_str <> ls_str_old and lds_data.getitemstring(ll_i,15) = "P" then
		ls_str_old = ls_str
		ll_anno_ordine = lds_data.getitemnumber(ll_i,1)
		ll_num_ordine = lds_data.getitemnumber(ll_i,2)
		ll_prog_riga_ord_ven = lds_data.getitemnumber(ll_i,3)
		
		select 	C.cod_comando, 
					A.des_prodotto,
					C.alt_asta,
					D.quan_ordine
		into		:ls_cod_comando, 
					:ls_des_comando,
					:ld_alt_asta,
					:ld_quan_prodotto_finito
		from		comp_det_ord_ven C
		join 		anag_prodotti A on A.cod_azienda=C.cod_azienda and A.cod_prodotto=C.cod_comando
		join		det_ord_ven D on C.cod_azienda=D.cod_azienda and C.anno_registrazione=D.anno_registrazione and C.num_registrazione=D.num_registrazione and C.prog_riga_ord_ven=D.prog_riga_ord_ven
		where 	C.cod_azienda = :s_cs_xx.cod_azienda and 
					C.anno_registrazione=:ll_anno_ordine and
					C.num_registrazione=:ll_num_ordine and
					C.prog_riga_ord_ven=:ll_prog_riga_ord_ven;
		if sqlca.sqlcode = 0 and not isnull(ls_cod_comando) then
			ll_cont ++
			luo_excel.uof_set(ll_cont + 2, 1, ls_str )
			luo_excel.uof_set(ll_cont + 2, 2, lds_data.getitemstring(ll_i,13) )
			luo_excel.uof_set(ll_cont + 2, 3, lds_data.getitemstring(ll_i,4) )
			luo_excel.uof_set(ll_cont + 2, 4, ls_cod_comando )
			luo_excel.uof_set(ll_cont + 2, 5, ls_des_comando )
			luo_excel.uof_set(ll_cont + 2, 7, g_str.format("$1 $2", "PZ", string(ld_quan_prodotto_finito,"###,##0.00" ) ) )
		end if
	end if
next
luo_excel.uof_set_horizontal_align( 3, 2, 1)
luo_excel.uof_set_border(3, 1,ll_cont + 2, 7, 2, 2, 2, 2)
luo_excel.uof_set_autofit(2,5)

//destroy luo_excel

return 
end event

event ue_nuova_sessione_da_righe();// Per ALUSISTEMI 18/04/2019. Poter aggiungere le righe degli ordini.
// Apro finestra con righe ordine e poi a conferma creo la sessione di produzione.

string	ls_messaggio
long ll_id_tes_lancio_prod
treeviewitem ltv_item
s_tree_lancio_produzione ls_record, ls_record_null

if isnull(tv_lancio.il_handle) or tv_lancio.il_handle <= 0 then
	return
end if

tv_lancio.getitem(tv_lancio.il_handle,ltv_item)

ls_record = ltv_item.data
if ls_record.livello = -1 then
	window_open_parm( w_stampa_ordini_prod_carica_dettaglio,-1, dw_ext_selezione )
end if

end event

event ue_tree_lancio_prod_refresh();string ls_messaggio

tv_lancio.uof_carica_tv( dw_gg_lancio.getitemnumber(1,1), ls_messaggio )

end event

event ue_mostra_sessioni_chiuse();string ls_messaggio

tv_lancio.ib_mostra_ordini_chiusi=true
tv_lancio.uof_carica_tv(dw_gg_lancio.getitemnumber(1,1), ls_messaggio)
end event

public function integer f_giorno_settimana (datetime fdt_data, ref string fs_giorno);integer li_num_giorno

li_num_giorno = DayNumber(date(fdt_data))

choose case li_num_giorno
	case 1
		fs_giorno="Dom"
	
	case 2
		fs_giorno="Lun"

	case 3
		fs_giorno="Mar"

	case 4
		fs_giorno="Mer"

	case 5
		fs_giorno="Gio"

	case 6
		fs_giorno="Ven"

	case 7
		fs_giorno="Sab"

end choose

return 0
end function

public function string wf_elabora (string fs_stringa);
long ll_posizione,ll_t
ll_posizione = 1
for ll_t = 1 to len(fs_stringa)
	ll_posizione=Pos ( fs_stringa, "'" , ll_posizione)
	if ll_posizione = 0 then exit
	fs_stringa = Replace ( fs_stringa, ll_posizione, 1, char(96) ) 
	ll_posizione=ll_posizione+1
next

return fs_stringa 
end function

public function integer f_report_1 (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_reparto, long fl_prog_riga_comanda, long fl_num_tenda);// Funzione che compila la datawindow di tipo report_1 (Tende da Sole)
// nome: f_report_1
// tipo: integer
//       -1 failed
//  		 0 passed
//							
//
//		Creata il 21-12-1999 
//		Autore Diego Ferrari

boolean	lb_inserito, lb_alusistemi=false

string		ls_cod_cliente,ls_rag_soc_1,ls_rag_soc_2,ls_indirizzo,ls_cap,ls_localita,ls_provincia,ls_telefono,ls_fax, &
				ls_cod_operatore,ls_cod_giro_consegna,ls_des_reparto, ls_null, ls_cod_imballo, ls_des_imballo, &
				ls_des_comando_1,ls_des_supporto,ls_cod_non_a_magazzino,ls_des_colore_telo,ls_cod_mant_non_a_magazzino, &
				ls_flag_tipo_mantovana,ls_colore_passamaneria,ls_colore_cordolo, ls_cod_tipo_det_ven_addizionali, &
				ls_des_colore_mantovana,ls_flag_allegati,ls_cod_prod_finito,ls_cod_prodotto,ls_cod_dim_x,ls_cod_dim_y,&
				ls_cod_dim_z,ls_cod_dim_t,ls_des_dim_x,ls_des_dim_y,ls_des_dim_z,ls_des_dim_t,ls_cod_tessuto,ls_cod_tessuto_mant,&
				ls_flag_cordolo,ls_flag_passamaneria,ls_cod_verniciatura,ls_cod_comando,ls_pos_comando,ls_cod_colore_lastra,&
				ls_cod_tipo_lastra,ls_cod_tipo_supporto,ls_flag_misura_luce_finita,ls_des_vernice_fc,ls_des_prodotto,& 
				ls_des_colore_tessuto,ls_nota_dettaglio,ls_nota_configuratore,ls_cod_prodotto_opt,ls_cod_misura_opt, &
				ls_nota_opt,ls_des_prodotto_opt,ls_giorno,ls_des_giro_consegna,ls_flag_tassativo,ls_cod_prodotto_padre,&
				ls_cod_prodotto_figlio[],ls_cod_versione,ls_des_distinta_taglio[],ls_um_quantita[],ls_um_misura[],&
				ls_des_prodotto_figlio[],ls_flag_non_calcolare_dt,ls_cod_prodotto_variante,ls_flag_calcola_dt, ls_alias, &
				ls_dest_test, ls_rs1_test, ls_rs2_test, ls_ind_test, ls_loc_test, ls_prov_test, ls_cap_test, ls_fraz_test, &
				ls_b_um_quantita,ls_b_um_misura,ls_bfo,ls_errore,ls_num_ord_cliente,ls_rif_interscambio,ls_nota_prodotto, &
				ls_cod_versione_figlio[],ls_flag_urgente,ls_lista_reparti_coinvolti, ls_stabilim_prossimo, ls_cod_deposito_origine, &
				ls_cod_deposito_reparto, ls_reparto_prossimo, ls_riga_n_di_m, ls_chiave_etic, ls_cod_verniciatura_2, ls_des_vernice_fc_2,&
				ls_cod_verniciatura_3, ls_des_vernice_fc_3, ls_cod_comando_2, ls_des_comando_2, ls_nota_testata,ls_nota_piede, ls_nota_video, &
				ls_des_variabile, ls_des_dimensioni, ls_flag_azzera_dt, ls_appoggio, ls_cod_lingua, ls_flag_tipo_bcl, ls_flag_sottotipo_bcl, &
				ls_flag_nascondi_cliente
			  
integer		li_risposta, li_bco, ll_e

long			ll_num_max_tenda,ll_num_commessa,ll_num_righe,ll_num_commessa_opt,ll_num_taglio,ll_num_sequenza,ll_progressivo,&
				ll_prog_calcolo,ll_i,ll_conta_riga,ll_progr_det_produzione, ll_sqlcode
				
double		ld_quantita[],ld_misura[], ldd_quan_ordine,ldd_quan_supporto,ldd_alt_mantovana,ldd_dim_x,ldd_dim_y,ldd_dim_z,ldd_dim_t,ldd_alt_asta,ldd_quan_ordine_opt
dec{4}		ld_dim_x_riga, ld_dim_y_riga

datetime	ldt_data_consegna, ldt_data_pronto, ldt_data_pronto_fasi

uo_produzione luo_prod


setnull(ls_null)

//Donato 12/03/2012: no filigrana per tipo report 1
dw_report.modify("DataWindow.brushmode=0")

dw_report.Modify("t_azienda.text='"+wf_logo()+"'")

guo_functions.uof_get_parametro_azienda( "ALU", lb_alusistemi)


ls_flag_calcola_dt = dw_ext_selezione.getitemstring(1,"flag_calcola_dt")

select quan_ordine,
		 num_commessa,
		 cod_prodotto,
		 nota_dettaglio,
		 cod_versione,
		 nota_prodotto,
		 flag_urgente,
		 des_prodotto
into   :ldd_quan_ordine,
		 :ll_num_commessa,
		 :ls_cod_prodotto,
		 :ls_nota_dettaglio,
		 :ls_cod_versione,
		 :ls_nota_prodotto,
		 :ls_flag_urgente,
		 :ls_des_prodotto
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione
and    prog_riga_ord_ven=:fl_prog_riga_comanda;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if


if ls_flag_calcola_dt = "S" then
	
	select flag_non_calcolare_dt
	into   :ls_flag_non_calcolare_dt
	from   distinta_padri
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto
	and    cod_versione=:ls_cod_versione;
	
	if sqlca.sqlcode< 0 then
		messagebox("Apice","caso report 1, lettura flag_non_calcolare_dt, Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	
	ll_num_taglio=0

	if ls_flag_non_calcolare_dt = 'N' or isnull(ls_flag_non_calcolare_dt) then
					
		select count(*)
		into   :ll_num_taglio
		from   distinte_taglio_calcolate
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:fi_anno_registrazione
		and    num_registrazione=:fl_num_registrazione
		and    prog_riga_ord_ven=:fl_prog_riga_comanda;
		
		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tes_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
	
		if ll_num_taglio > 0 then
			declare r_distinte_calcolate cursor for
			select  cod_prodotto_padre,
					  num_sequenza,
					  cod_prodotto_figlio,
					  cod_versione_figlio,
					  cod_versione,
					  progressivo,
					  prog_calcolo,
					  quantita,
					  misura
			from    distinte_taglio_calcolate
			where   cod_azienda=:s_cs_xx.cod_azienda
			and     anno_registrazione=:fi_anno_registrazione
			and     num_registrazione=:fl_num_registrazione
			and     prog_riga_ord_ven=:fl_prog_riga_comanda;
			
			open   r_distinte_calcolate;
			
			if sqlca.sqlcode < 0 then 
				messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tabella distinte_taglio_calcolate.~r~n" + sqlca.sqlerrtext,stopsign!)
				close   r_distinte_calcolate;
				return -1
			end if
			
			ll_i = 1
			
			
			do while true
				fetch r_distinte_calcolate
				into  :ls_cod_prodotto_padre,
						:ll_num_sequenza,
						:ls_cod_prodotto_figlio[ll_i],
						:ls_cod_versione_figlio[ll_i],
						:ls_cod_versione,
						:ll_progressivo,
						:ll_prog_calcolo,
						:ld_quantita[ll_i],
						:ld_misura[ll_i];
					
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tabella distinte_taglio_calcolate. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					close   r_distinte_calcolate;
					return -1
				end if
			
				if sqlca.sqlcode = 100 then exit
				
				ld_quantita[ll_i] = round(ld_quantita[ll_i],2)
				ld_misura[ll_i] = round(ld_misura[ll_i],2)
				
				select des_distinta_taglio,
						 um_quantita,
						 um_misura
				into   :ls_des_distinta_taglio[ll_i],
						 :ls_b_um_quantita,
						 :ls_b_um_misura
				from   tab_distinte_taglio
				where  cod_azienda=:s_cs_xx.cod_azienda 
				and    cod_prodotto_padre=:ls_cod_prodotto_padre
				and    num_sequenza=:ll_num_sequenza
				and    cod_prodotto_figlio=:ls_cod_prodotto_figlio[ll_i]
				and    cod_versione_figlio=:ls_cod_versione_figlio[ll_i]
				and    cod_versione=:ls_cod_versione
				and    progressivo=:ll_progressivo;
						 
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tabella tab_distinte_taglio. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					close   r_distinte_calcolate;
					return -1
				end if
				
				if ls_b_um_quantita = "" then setnull(ls_b_um_quantita)
				if ls_b_um_misura = "" then setnull(ls_b_um_misura)
				ls_um_quantita[ll_i] = ls_b_um_quantita
				ls_um_misura[ll_i] = ls_b_um_misura
						 
				select cod_prodotto
				into   :ls_cod_prodotto_variante
				from   varianti_det_ord_ven
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    anno_registrazione=:fi_anno_registrazione
				and    num_registrazione=:fl_num_registrazione
				and    prog_riga_ord_ven=:fl_prog_riga_comanda
				and    cod_prodotto_padre=:ls_cod_prodotto_padre
				and    num_sequenza=:ll_num_sequenza
				and    cod_prodotto_figlio=:ls_cod_prodotto_figlio[ll_i]
				and    cod_versione_figlio=:ls_cod_versione_figlio[ll_i]
				and    cod_versione=:ls_cod_versione;
				
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tabella tab_distinte_taglio. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					close   r_distinte_calcolate;
					return -1
				end if
				
				if not isnull(ls_cod_prodotto_variante) and ls_cod_prodotto_variante<>"" then
					ls_cod_prodotto_figlio[ll_i] = ls_cod_prodotto_variante
				end if
				
				select des_prodotto
				into   :ls_des_prodotto_figlio[ll_i]
				from   anag_prodotti
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto_figlio[ll_i];
				
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tabella anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					close   r_distinte_calcolate;
					return -1
				end if
				
				ll_i++
				setnull(ls_cod_prodotto_variante)
			loop
		
			close   r_distinte_calcolate;
			
		end if
	end if
end if



select max(num_tenda)
into   :ll_num_max_tenda
from   tes_stampa_ordini
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione
and    cod_reparto=:fs_cod_reparto and prog_sessione=:il_sessione;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tes_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select tes_ord_ven.data_pronto,
		 tes_ord_ven.data_consegna,
		 tes_ord_ven.cod_cliente,
		 tes_ord_ven.cod_giro_consegna,
		 tes_ord_ven.cod_operatore,
		 tes_ord_ven.flag_tassativo,
		 tes_ord_ven.alias_cliente,
		 tes_ord_ven.cod_des_cliente,
		 tes_ord_ven.rag_soc_1,
		 tes_ord_ven.rag_soc_2,
		 tes_ord_ven.indirizzo,
		 tes_ord_ven.localita,
		 tes_ord_ven.frazione,
		 tes_ord_ven.cap,
		 tes_ord_ven.provincia,
		 tes_ord_ven.num_ord_cliente,
		 tes_ord_ven.rif_interscambio,
		 tes_ord_ven.cod_deposito,
		 tes_ord_ven.cod_imballo,
		 tab_tipi_ord_ven.flag_tipo_bcl,
		 tab_tipi_ord_ven.flag_sottotipo_bcl
into   :ldt_data_pronto,
		 :ldt_data_consegna,
		 :ls_cod_cliente,
		 :ls_cod_giro_consegna,
		 :ls_cod_operatore,
		 :ls_flag_tassativo,
		 :ls_alias,
		 :ls_dest_test,
		 :ls_rs1_test,
		 :ls_rs2_test,
		 :ls_ind_test,
		 :ls_loc_test,
		 :ls_fraz_test,
		 :ls_cap_test,
		 :ls_prov_test,
		 :ls_num_ord_cliente,
		 :ls_rif_interscambio,
		 :ls_cod_deposito_origine,
		 :ls_cod_imballo,
		 :ls_flag_tipo_bcl,
		 :ls_flag_sottotipo_bcl
from   	tes_ord_ven
left outer join tab_tipi_ord_ven on tes_ord_ven.cod_azienda=tab_tipi_ord_ven.cod_azienda and tes_ord_ven.cod_tipo_ord_ven=tab_tipi_ord_ven.cod_tipo_ord_ven
where  	tes_ord_ven.cod_azienda=:s_cs_xx.cod_azienda and
			tes_ord_ven.anno_registrazione=:fi_anno_registrazione and
			tes_ord_ven.num_registrazione=:fl_num_registrazione;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tes_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if isnull(ls_dest_test) and isnull(ls_rs1_test) and isnull(ls_rs2_test) and isnull(ls_ind_test) and &
   isnull(ls_loc_test) and isnull(ls_fraz_test) and isnull(ls_cap_test) and isnull(ls_prov_test) then
	dw_report.modify("t_lc.visible = 0")
else
	dw_report.modify("t_lc.visible = 1")
end if

select 	des_giro_consegna
into   		:ls_des_giro_consegna
from   	tes_giri_consegne
where  	cod_azienda=:s_cs_xx.cod_azienda and
			cod_giro_consegna=:ls_cod_giro_consegna;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tes_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select des_reparto,
		flag_nascondi_cliente
into   :ls_des_reparto,
		:ls_flag_nascondi_cliente
from   anag_reparti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_reparto=:fs_cod_reparto;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_reparti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select des_imballo
into   :ls_des_imballo
from   tab_imballi
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_imballo=:ls_cod_imballo;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tabella imballaggi. Dettaglio Errore: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if


		 
select rag_soc_1,
		 rag_soc_2,
		 indirizzo,
		 cap,
		 localita,
		 provincia,
		 telefono,
		 fax,
		 cod_lingua
into   :ls_rag_soc_1,
		 :ls_rag_soc_2,
		 :ls_indirizzo,
		 :ls_cap,
		 :ls_localita,
		 :ls_provincia,
		 :ls_telefono,
		 :ls_fax,
		 :ls_cod_lingua
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_clienti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

ls_rag_soc_1 = f_cazzillo(ls_rag_soc_1)
ls_rag_soc_2 = f_cazzillo(ls_rag_soc_2)
ls_indirizzo = f_cazzillo(ls_indirizzo)
ls_localita = f_cazzillo(ls_localita)
 
select cod_non_a_magazzino,
		 cod_prod_finito,
		 dim_x,
		 dim_y,
		 dim_z,
		 dim_t,
		 cod_tessuto,
		 cod_tessuto_mant,
		 flag_tipo_mantovana,
		 alt_mantovana,
		 flag_cordolo,
		 flag_passamaneria,
		 cod_verniciatura,
		 cod_comando,
		 pos_comando,
		 alt_asta,
		 cod_colore_lastra,
		 cod_tipo_lastra,
		 cod_tipo_supporto,
		 flag_misura_luce_finita,
		 note,
		 colore_passamaneria,
		 colore_cordolo,
		 des_vernice_fc,
		 cod_mant_non_a_magazzino,
		 flag_allegati,
		 des_comando_1,
		 cod_verniciatura_2,
		 des_vernic_fc_2,
		 cod_verniciatura_3,
		 des_vernic_fc_3,
		 cod_comando_2,
		 des_comando_2,
		 flag_azzera_dt
into	 :ls_cod_non_a_magazzino,
		 :ls_cod_prod_finito,
		 :ldd_dim_x,
		 :ldd_dim_y,
		 :ldd_dim_z,
		 :ldd_dim_t,
		 :ls_cod_tessuto,
		 :ls_cod_tessuto_mant,
		 :ls_flag_tipo_mantovana,
		 :ldd_alt_mantovana,
		 :ls_flag_cordolo,
		 :ls_flag_passamaneria,
		 :ls_cod_verniciatura,
		 :ls_cod_comando,
		 :ls_pos_comando,
		 :ldd_alt_asta,
		 :ls_cod_colore_lastra,
		 :ls_cod_tipo_lastra,
		 :ls_cod_tipo_supporto,
		 :ls_flag_misura_luce_finita,
		 :ls_nota_configuratore,
		 :ls_colore_passamaneria,
		 :ls_colore_cordolo,
		 :ls_des_vernice_fc,
		 :ls_cod_mant_non_a_magazzino,
		 :ls_flag_allegati,
		 :ls_des_comando_1,
		 :ls_cod_verniciatura_2,
		 :ls_des_vernice_fc_2,
		 :ls_cod_verniciatura_3,
		 :ls_des_vernice_fc_3,
		 :ls_cod_comando_2,
		 :ls_des_comando_2,
		 :ls_flag_azzera_dt
from   comp_det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:fi_anno_registrazione and
		num_registrazione=:fl_num_registrazione and
		prog_riga_ord_ven=:fl_prog_riga_comanda;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura comp_det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if isnull(ls_flag_azzera_dt) or ls_flag_azzera_dt="" then ls_flag_azzera_dt = "N"

select cod_variabile
into   :ls_cod_dim_x
from   tab_variabili_formule
where  cod_azienda=:s_cs_xx.cod_azienda
and    nome_campo_database='dim_x';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_variabili_formule (dim_x). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select cod_variabile
into   :ls_cod_dim_y
from   tab_variabili_formule
where  cod_azienda=:s_cs_xx.cod_azienda
and    nome_campo_database='dim_y';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_variabili_formule (dim_y). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select cod_variabile
into   :ls_cod_dim_z
from   tab_variabili_formule
where  cod_azienda=:s_cs_xx.cod_azienda
and    nome_campo_database='dim_z';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_variabili_formule (dim_z). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select cod_variabile
into   :ls_cod_dim_t
from   tab_variabili_formule
where  cod_azienda=:s_cs_xx.cod_azienda
and    nome_campo_database='dim_t';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_variabili_formule (dim_t). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select des_variabile
into   :ls_des_dim_x
from   tab_des_variabili
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto
and    cod_variabile=:ls_cod_dim_x and
flag_ipertech_apice='S';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_des_variabili (dim_x). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select des_variabile
into   :ls_des_dim_y
from   tab_des_variabili
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto
and    cod_variabile=:ls_cod_dim_y and
flag_ipertech_apice='S';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_des_variabili (dim_y). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select des_variabile
into   :ls_des_dim_z
from   tab_des_variabili
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto
and    cod_variabile=:ls_cod_dim_z and
flag_ipertech_apice='S';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_des_variabili (dim_z). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select des_variabile
into   :ls_des_dim_t
from   tab_des_variabili
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_prodotto=:ls_cod_prodotto
and    cod_variabile=:ls_cod_dim_t and
flag_ipertech_apice='S';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_des_variabili (dim_t). Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

ll_num_righe=dw_report.rowcount() + 1

ll_i=1  //imposto il contatore per le righe della tabellina della distinta di taglio

//********************************************************************     DA QUI COMINCIANO GLI INSERT SULLA DW

if not isnull(ls_cod_prod_finito) then
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice","CODICE")
	dw_report.setitem(ll_num_righe,"descrizione",ls_cod_prod_finito)
	
	//Donato 24/04/2013
	//Solo se il des_prodotto della riga è vuoto ci metto quella del prodotto in anag_prodotti
	if isnull(ls_des_prodotto) or ls_des_prodotto="" then
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto;
	
		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
	end if
	//fine modifica ------------------------------------------------------------------------------------
	
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",2)
	dw_report.setitem(ll_num_righe,"visibile_2",1)
	dw_report.setitem(ll_num_righe,"flag_descrizione_1",1)
	dw_report.setitem(ll_num_righe,"codice","DESCRIZIONE")
	dw_report.setitem(ll_num_righe,"descrizione_1",ls_des_prodotto)
	dw_report.setitem(ll_num_righe,"quantita",string(ldd_quan_ordine))
	
end if

if not isnull(ls_cod_tessuto) then
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_tessuto;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice","")
	dw_report.setitem(ll_num_righe,"descrizione",ls_des_prodotto)
	
else

	if not isnull(ls_cod_tessuto_mant) then

//		select cod_tessuto
//		into   :ls_cod_tessuto
//		from   tab_tessuti
//		where  cod_azienda=:s_cs_xx.cod_azienda
//		and    cod_non_a_magazzino=:ls_cod_mant_non_a_magazzino;
//		
//		if sqlca.sqlcode < 0 then 
//			messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_colori_tessuti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
//			return -1
//		end if

		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_tessuto_mant;
	
		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if

		ll_num_righe++
		dw_report.insertrow(ll_num_righe)
		dw_report.setitem(ll_num_righe,"tipo_riga",1)
		dw_report.setitem(ll_num_righe,"codice","")
		dw_report.setitem(ll_num_righe,"descrizione",ls_des_prodotto)

	end if 
	
end if


ll_num_righe++
dw_report.insertrow(ll_num_righe)
dw_report.setitem(ll_num_righe,"tipo_riga",10)
dw_report.setitem(ll_num_righe,"codice","")
dw_report.setitem(ll_num_righe,"descrizione","")

if ldd_dim_x <> 0 then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice",ls_des_dim_x)
	dw_report.setitem(ll_num_righe,"descrizione",string(ldd_dim_x))
end if

if ldd_dim_y <> 0 then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice",ls_des_dim_y)
	dw_report.setitem(ll_num_righe,"descrizione",string(ldd_dim_y))
end if

if ldd_dim_z <> 0 then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice",ls_des_dim_z)
	dw_report.setitem(ll_num_righe,"descrizione",string(ldd_dim_z))
end if

if ldd_dim_t <> 0 then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice",ls_des_dim_t)
	dw_report.setitem(ll_num_righe,"descrizione",string(ldd_dim_t))
end if

if not isnull(ls_flag_misura_luce_finita) then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice","MIS.LUCE/FINITA")
	dw_report.setitem(ll_num_righe,"descrizione",ls_flag_misura_luce_finita)
end if

if not isnull(ls_cod_verniciatura) then
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_verniciatura;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice","-> VR STRUTTURA")
	dw_report.setitem(ll_num_righe,"descrizione",ls_des_prodotto)
end if

if not isnull(ls_des_vernice_fc) then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"visibile_2",1)
	dw_report.setitem(ll_num_righe,"visibile_1",1)
	dw_report.setitem(ll_num_righe,"tipo_riga",3)
	dw_report.setitem(ll_num_righe,"nota","NOTA PER VERNICIATURA")
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"visibile_1",1)
	dw_report.setitem(ll_num_righe,"tipo_riga",3)
	dw_report.setitem(ll_num_righe,"nota",ls_des_vernice_fc)
end if

// EnMe 19/04/2012 aggiunto verniciatura 2 e 3 sull'ordine di lavoro
ls_des_prodotto = ""
if not isnull(ls_cod_verniciatura_2) then
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_verniciatura_2;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura descrizione verniciatura 2 da anag_prodotti. Dettaglio errore: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice","-> VR PACCO TELO")
	dw_report.setitem(ll_num_righe,"descrizione",ls_des_prodotto)
end if

if not isnull(ls_des_vernice_fc_2) then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"visibile_2",1)
	dw_report.setitem(ll_num_righe,"visibile_1",1)
	dw_report.setitem(ll_num_righe,"tipo_riga",3)
	dw_report.setitem(ll_num_righe,"nota","NOTA PER VR PACCO TELO")
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"visibile_1",1)
	dw_report.setitem(ll_num_righe,"tipo_riga",3)
	dw_report.setitem(ll_num_righe,"nota",ls_des_vernice_fc_2)
end if

ls_des_prodotto = ""
if not isnull(ls_cod_verniciatura_3) then
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_verniciatura_3;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura descrizione verniciatura 3 da anag_prodotti. Dettaglio errore: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice","->VR LEGNO")
	dw_report.setitem(ll_num_righe,"descrizione",ls_des_prodotto)
end if

if not isnull(ls_des_vernice_fc_3) then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"visibile_2",1)
	dw_report.setitem(ll_num_righe,"visibile_1",1)
	dw_report.setitem(ll_num_righe,"tipo_riga",3)
	dw_report.setitem(ll_num_righe,"nota","NOTA PER VR LEGNO:")
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"visibile_1",1)
	dw_report.setitem(ll_num_righe,"tipo_riga",3)
	dw_report.setitem(ll_num_righe,"nota",ls_des_vernice_fc_3)
end if


// -------------- fine modifica Enme 19/04/2012

if not isnull(ls_cod_comando) then
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_comando;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ll_num_righe++
	dw_report.insertrow(ll_num_righe)

	dw_report.setitem(ll_num_righe,"tipo_riga",2)
//	dw_report.setitem(ll_num_righe,"visibile_2",1) //riga bastarda

	dw_report.setitem(ll_num_righe,"codice","TIPO COMANDO")
	dw_report.setitem(ll_num_righe,"descrizione",ls_des_prodotto)
	if not lb_alusistemi then dw_report.setitem(ll_num_righe,"quantita",string(ldd_quan_ordine))
	
end if

if not isnull(ls_pos_comando) then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice","POSIZIONE COMANDO")
	dw_report.setitem(ll_num_righe,"descrizione",ls_pos_comando)
	
end if

if not isnull(ls_des_comando_1) then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"visibile_2",1)
	dw_report.setitem(ll_num_righe,"visibile_1",1)
	dw_report.setitem(ll_num_righe,"tipo_riga",3)
	dw_report.setitem(ll_num_righe,"nota","NOTA PER COMANDO:")
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
//	dw_report.setitem(ll_num_righe,"visibile_2",1)
	dw_report.setitem(ll_num_righe,"visibile_1",1)
	dw_report.setitem(ll_num_righe,"tipo_riga",3)
	dw_report.setitem(ll_num_righe,"nota",ls_des_comando_1)

end if

if ldd_alt_asta<>0 then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice","H COM./ASTA")
	dw_report.setitem(ll_num_righe,"descrizione",string(ldd_alt_asta))
end if


// EnMe 19/04/2012 : stampo descondo comando nell'ordine di lavoro
ls_des_prodotto = ""
if not isnull(ls_cod_comando_2) then
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_comando_2;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura comando 2 in anagrafica prodotti.~r~nDettaglio Errore:" + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ll_num_righe++
	dw_report.insertrow(ll_num_righe)

	dw_report.setitem(ll_num_righe,"tipo_riga",2)
	//dw_report.setitem(ll_num_righe,"codice","COMANDO 2")
	dw_report.setitem(ll_num_righe,"codice","")
	dw_report.setitem(ll_num_righe,"descrizione",ls_des_prodotto)
	if not lb_alusistemi then dw_report.setitem(ll_num_righe,"quantita",string(ldd_quan_ordine))
end if

if not isnull(ls_des_comando_2) then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"visibile_2",1)
	dw_report.setitem(ll_num_righe,"visibile_1",1)
	dw_report.setitem(ll_num_righe,"tipo_riga",3)
	dw_report.setitem(ll_num_righe,"nota","NOTA PER COMANDO 2:")
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"visibile_1",1)
	dw_report.setitem(ll_num_righe,"tipo_riga",3)
	dw_report.setitem(ll_num_righe,"nota",ls_des_comando_2)
end if
// ---------->>  FINE MODIFICA SECONDO COMANDO 19-04-2012   <<<----------------------

if not isnull(ls_cod_tipo_supporto) then
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_tipo_supporto;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	select quan_utilizzo
	into   :ldd_quan_supporto
	from   varianti_det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:fi_anno_registrazione
	and    num_registrazione=:fl_num_registrazione
	and    prog_riga_ord_ven=:fl_prog_riga_comanda
	and    cod_prodotto=:ls_cod_tipo_supporto;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura varianti_det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ll_num_righe++
	dw_report.insertrow(ll_num_righe)

	dw_report.setitem(ll_num_righe,"tipo_riga",2)
	
//	dw_report.setitem(ll_num_righe,"visibile_2",1)   // questa riga fa esplodere tutto

	dw_report.setitem(ll_num_righe,"codice","TIPO SUPPORTI")
	dw_report.setitem(ll_num_righe,"descrizione",ls_des_prodotto)
	if not lb_alusistemi then dw_report.setitem(ll_num_righe,"quantita",string(ldd_quan_supporto))
	
end if

ll_num_righe++
dw_report.insertrow(ll_num_righe)
dw_report.setitem(ll_num_righe,"tipo_riga",10)
dw_report.setitem(ll_num_righe,"codice","")
dw_report.setitem(ll_num_righe,"descrizione","")

if not isnull(ls_cod_non_a_magazzino) then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice","COL.TELO/LASTRA")
	dw_report.setitem(ll_num_righe,"descrizione",ls_cod_non_a_magazzino)

	select des_colore_tessuto
	into   :ls_des_colore_tessuto
	from   tab_colori_tessuti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_colore_tessuto=:ls_cod_non_a_magazzino;
	
	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_colori_tessuti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice","DES.TELO/LASTRA")
	dw_report.setitem(ll_num_righe,"descrizione",ls_des_colore_tessuto)
	setnull(ls_des_colore_tessuto)
	
end if

if not isnull(ls_cod_mant_non_a_magazzino) then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice","TESS.MANTOVANA")
	dw_report.setitem(ll_num_righe,"descrizione",ls_cod_mant_non_a_magazzino)

	select des_colore_tessuto
	into   :ls_des_colore_tessuto
	from   tab_colori_tessuti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_colore_tessuto=:ls_cod_mant_non_a_magazzino;
	
	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tab_colori_tessuti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice","DES.MANTOVANA")
	dw_report.setitem(ll_num_righe,"descrizione",ls_des_colore_tessuto)

end if

if not isnull(ls_flag_tipo_mantovana) then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice","TIPO H/MANT.")
	dw_report.setitem(ll_num_righe,"descrizione",ls_flag_tipo_mantovana + "    /    " + string(ldd_alt_mantovana))
end if

if (not isnull(ls_colore_passamaneria)) or (not isnull(ls_colore_cordolo)) then
	if isnull(ls_colore_passamaneria) then ls_colore_passamaneria = ""
	if isnull(ls_colore_cordolo) then ls_colore_cordolo = ""
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",1)
	dw_report.setitem(ll_num_righe,"codice","PASSAM./CORDOLO")
	if isnull(ls_colore_cordolo) then ls_colore_cordolo = " "
	dw_report.setitem(ll_num_righe,"descrizione",ls_colore_passamaneria + "    /    " + ls_colore_cordolo)
end if

ll_num_righe++
dw_report.insertrow(ll_num_righe)

dw_report.setitem(ll_num_righe,"tipo_riga",5)
dw_report.setitem(ll_num_righe,"nota_1","           ")

ll_num_righe++
dw_report.insertrow(ll_num_righe)

dw_report.setitem(ll_num_righe,"tipo_riga",5)
dw_report.setitem(ll_num_righe,"nota_1","           ")

lb_inserito = false

for ll_e = 1 to 20
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
next


//*********************************************************************************************************
//*********************************************************************************************************
// 																						Inizio inserimento per distinta di taglio 
if ls_flag_calcola_dt = "S" then
	if ll_num_taglio > 0 then
		
		ll_conta_riga = 1
		dw_report.setitem(ll_conta_riga,"vis_1",1)
		dw_report.setitem(ll_conta_riga,"vis_4",1)
		dw_report.setitem(ll_conta_riga,"vis_5",1)
		
		for ll_i = 1 to ll_num_taglio
			
			if ll_i >1 then
				if ls_cod_prodotto_figlio[ll_i -1] <> ls_cod_prodotto_figlio[ll_i] then
					// controllo se serve aggiungere altre righe.
					if ll_conta_riga +1 > ll_num_righe then
						ll_num_righe++
						dw_report.insertrow(ll_num_righe)
					end if
					ll_conta_riga++
					dw_report.setitem(ll_conta_riga,"vis_2",1)
					dw_report.setitem(ll_conta_riga,"vis_4",1)
					dw_report.setitem(ll_conta_riga,"des_distinta",ls_cod_prodotto_figlio[ll_i]+"  " + ls_des_prodotto_figlio[ll_i])
				end if
			else		
				// controllo se serve aggiungere altre righe.
				if ll_conta_riga +1 > ll_num_righe then
					ll_num_righe++
					dw_report.insertrow(ll_num_righe)
				end if
				ll_conta_riga++
				dw_report.setitem(ll_conta_riga,"vis_2",1)
				dw_report.setitem(ll_conta_riga,"vis_4",1)
				dw_report.setitem(ll_conta_riga,"des_distinta",ls_cod_prodotto_figlio[ll_i]+"  " + ls_des_prodotto_figlio[ll_i])
			end if
			// controllo se serve aggiungere altre righe.
			if ll_conta_riga +1 > ll_num_righe then
				ll_num_righe++
				dw_report.insertrow(ll_num_righe)
			end if			
			ll_conta_riga++
			dw_report.setitem(ll_conta_riga,"vis_3",1)
			
			if ld_quantita[ll_i]=0 and ld_misura[ll_i]=0 then continue  //inserita su richiesta di Beatrice per non stampare nulla in caso di risultati = 0

			dw_report.setitem(ll_conta_riga,"des_taglio",ls_des_distinta_taglio[ll_i])
			
			ls_appoggio = ""
			if ld_quantita[ll_i]<>0 then
				//dw_report.setitem(ll_conta_riga,"quan_taglio",ls_um_quantita[ll_i] + "  " + string(double(ld_quantita[ll_i])))
				ls_appoggio = ls_um_quantita[ll_i]
				if ls_flag_azzera_dt<>"S" then
					ls_appoggio += "  " + string(double(ld_quantita[ll_i]))
				end if
				dw_report.setitem(ll_conta_riga,"quan_taglio", ls_appoggio)
			end if			
			
			ls_appoggio = ""
			if ld_misura[ll_i]<>0 then
				//dw_report.setitem(ll_conta_riga,"misura_taglio",ls_um_misura[ll_i] + "  " + string(double(ld_misura[ll_i])))
				ls_appoggio = ls_um_misura[ll_i]
				if ls_flag_azzera_dt<>"S" then
					ls_appoggio += "  " + string(double(ld_misura[ll_i]))
				end if
				dw_report.setitem(ll_conta_riga,"misura_taglio", ls_appoggio)
			end if		
		next	
	end if	
	dw_report.setitem(ll_conta_riga,"vis_5",1)
end if
// 																						Fine inserimento per distinta di taglio 
//*********************************************************************************************************
//*********************************************************************************************************

luo_prod = create uo_produzione
if luo_prod.uof_get_tipo_det_ven_add( 	fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_comanda, 	ls_cod_tipo_det_ven_addizionali, ls_errore) < 0 then
	g_mb.error(ls_errore)
	return -1
end if
destroy luo_prod

// inizio inserimento optional
declare r_det_ord_ven_app cursor for 
select  	cod_prodotto,
		  	quan_ordine,
		  	nota_dettaglio,
		  	num_commessa,
		  	des_prodotto,
			dim_x,
			dim_y
from   	det_ord_ven
where   	cod_azienda=:s_cs_xx.cod_azienda and
		 	anno_registrazione=:fi_anno_registrazione and
			num_registrazione=:fl_num_registrazione and
			num_riga_appartenenza=:fl_prog_riga_comanda and
			cod_tipo_det_ven<> :ls_cod_tipo_det_ven_addizionali
order by prog_riga_ord_ven;

open r_det_ord_ven_app;
if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura cursore det_ord_ven per optional. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

do while true
	fetch r_det_ord_ven_app
	into  :ls_cod_prodotto_opt,
			:ldd_quan_ordine_opt,
			:ls_nota_opt,
			:ll_num_commessa_opt,
			:ls_des_prodotto_opt,
			:ld_dim_x_riga,
			:ld_dim_y_riga;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura cursore det_ord_ven per optional. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close r_det_ord_ven_app;
		return -1
	end if

	select cod_misura_mag
	into   :ls_cod_misura_opt
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto_opt;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_prodotti per optional. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close r_det_ord_ven_app;
		return -1
	end if
	
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	
	//se nn ho ancora inserito la testata della griglia lo faccio, poi resetto il semaforo lb_inserito
	if lb_inserito = false then
		dw_report.setitem(ll_num_righe,"tipo_riga",4)
		dw_report.setitem(ll_num_righe,"codice","Codice Optional")
		dw_report.setitem(ll_num_righe,"descrizione","Descrizione Optional")
		dw_report.setitem(ll_num_righe,"quantita","Q.tà")
		dw_report.setitem(ll_num_righe,"um_opt","U.M.")
		dw_report.setitem(ll_num_righe,"nota_optional","Nota di prodotto")
		dw_report.setitem(ll_num_righe,"num_com_opt","Num. Com.")
	
		lb_inserito = true
	end if
	
	
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
		
	dw_report.setitem(ll_num_righe,"tipo_riga",4)
//	dw_report.setitem(ll_num_righe,"visibile_3",1)
//	dw_report.setitem(ll_num_righe,"visibile_2",1)
//	dw_report.setitem(ll_num_righe,"visibile_1",1)
	dw_report.setitem(ll_num_righe,"codice",ls_cod_prodotto_opt)
	dw_report.setitem(ll_num_righe,"descrizione",ls_des_prodotto_opt)
	dw_report.setitem(ll_num_righe,"um_opt",ls_cod_misura_opt)
	dw_report.setitem(ll_num_righe,"quantita",string(ldd_quan_ordine_opt))
		
	if isnull(ls_nota_opt) then ls_nota_opt = ""
	//	EnMe 27/9/2012 stampo le dimensioni degli optionals (spec. Optionals Configurabili) ----------
	if ld_dim_y_riga > 0 and not isnull(ld_dim_y_riga) and not isnull(ls_cod_prodotto_opt) and len(ls_cod_prodotto_opt) > 0 then
		select des_variabile
		into :ls_des_variabile
		from tab_des_variabili
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto_opt and
				cod_variabile = 'DIM_2' and
				flag_ipertech_apice='S';
				
		//se non trovo la des variabile ci metto di default DIM 2
		if ls_des_variabile="" or isnull(ls_des_variabile) then
			ls_des_variabile = "DIM 2"
		end if
		
		ls_nota_opt = ls_des_variabile + "=" + string(ld_dim_y_riga,"###,##0.00") + " - " +  ls_nota_opt
	end if
	
	ls_des_variabile = ""
		
	if ld_dim_x_riga > 0 and not isnull(ld_dim_x_riga) and not isnull(ls_cod_prodotto_opt) and len(ls_cod_prodotto_opt) > 0 then
		select des_variabile
		into :ls_des_variabile
		from tab_des_variabili
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto_opt and
				cod_variabile = 'DIM_1' and
				flag_ipertech_apice='S';
		
		//se non trovo la des variabile ci metto di default DIM 1
		if ls_des_variabile="" or isnull(ls_des_variabile) then
			ls_des_variabile = "DIM 1"
		end if
		ls_nota_opt= ls_des_variabile + "=" + string(ld_dim_x_riga,"###,##0.00") + " - " + ls_nota_opt

	end if
//		-----------fine EnMe 27/09/2012		
		
	dw_report.setitem(ll_num_righe,"nota_optional",ls_nota_opt)
	dw_report.setitem(ll_num_righe,"num_com_opt",string(ll_num_commessa_opt))
		
loop		
			
close r_det_ord_ven_app;

// fine inserimento optional

ll_num_righe++
dw_report.insertrow(ll_num_righe)

dw_report.setitem(ll_num_righe,"tipo_riga",5)
dw_report.setitem(ll_num_righe,"nota_1","           ")

//if not isnull(ls_nota_configuratore) then
//	ll_num_righe++
//	dw_report.insertrow(ll_num_righe)
//	dw_report.setitem(ll_num_righe,"tipo_riga",11)
//	dw_report.setitem(ll_num_righe,"nota_1",ls_nota_configuratore)
//end if
//
if not isnull(ls_nota_dettaglio) and ls_nota_dettaglio <> "" then
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	dw_report.setitem(ll_num_righe,"tipo_riga",11)
	dw_report.setitem(ll_num_righe,"nota_1",ls_nota_dettaglio)
end if

// Se il sottotipo_bcl=4 allora la nota viene in testata (Alusistemi 23/11/2017)
if ls_flag_tipo_bcl='A' and ls_flag_sottotipo_bcl = "4" then
	if not isnull(ls_nota_prodotto) and ls_nota_prodotto <> "" then
		ll_num_righe++
		dw_report.insertrow(ll_num_righe)
		dw_report.setitem(ll_num_righe,"tipo_riga",11)
		dw_report.setitem(ll_num_righe,"nota_1",ls_nota_prodotto)
	end if
end if

//Donato 08/05/2013
//commentato perchè NON deve essere più visualizzata: specifica Calendario_trasferimenti

//if isnull(ldt_data_consegna) then
//	dw_report.Modify("data_consegna.text=' '")
//	dw_report.Modify("giorno_c.text=' '")
//else
//	dw_report.Modify("data_consegna.text='"+ string(date(ldt_data_consegna)) + "'")
//	li_risposta = f_giorno_settimana(ldt_data_consegna,ls_giorno)
//	dw_report.Modify("giorno_c.text='" + ls_giorno +"'")
//
//end if

//fine modifica -----------------



//16/01/2012 chiesto da Beatrice
//visulaizzare il numero di riga configurata su totale righe configurate
//es se riga 30 e in totale ci sono 5 righe configurate (cioè si ariva max a 50)  -->  3/5

ls_riga_n_di_m = ""

luo_prod = create uo_produzione
ls_riga_n_di_m = luo_prod.uof_get_riga_su_tot_righe(fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_comanda)
destroy luo_prod;

dw_report.Modify("num_ordine.text='"+ string(fl_num_registrazione)+ "   " + ls_riga_n_di_m + "'")

//dw_report.Modify("num_ordine.text='"+ string(fl_num_registrazione)+ "   " + string(fl_num_tenda) +"/" + string(ll_num_max_tenda) + "'")
//FINE MODIFICA -------------------------------------

dw_report.Modify("riga_app.text='" + string(fl_prog_riga_comanda) + "'")

wf_set_deposito_origine(fi_anno_registrazione, fl_num_registrazione, dw_report)


if isnull(ll_num_commessa) then
	dw_report.Modify("num_commessa.text=' '")
else
	dw_report.Modify("num_commessa.text='"+ string(ll_num_commessa) + "'")
end if

if isnull(ls_cod_giro_consegna) then
	dw_report.Modify("des_giro.text=' '")
else
	dw_report.Modify("des_giro.text='"+ ls_des_giro_consegna + "'")
end if

ls_rag_soc_1 = wf_elabora(ls_rag_soc_1)

dw_report.Modify("cod_cliente.text='"+ ls_cod_cliente + "'")
if ls_flag_nascondi_cliente="N" then 
	dw_report.Modify("rag_soc_1.text='" + ls_rag_soc_1 + "'")
else
	 dw_report.Modify("rag_soc_1.text=''")
end if

//---------------------------------------------------------------------------------------------
//Donato 14/02/2012
//colonna dinamica ETIC
ls_chiave_etic = ""
if ls_cod_cliente<>"" and not isnull(ls_cod_cliente) then
	ls_chiave_etic = wf_get_colonna_dinamica(ls_cod_cliente)
end if
dw_report.Modify("t_chiave.text='"+ ls_chiave_etic + "'")
//---------------------------------------------------------------------------------------------

if isnull(ls_alias) then
	dw_report.Modify("rag_soc_2.text=' '")
else
	ls_alias = wf_elabora(ls_alias)
	if ls_flag_nascondi_cliente="N" then 
		dw_report.Modify("rag_soc_2.text='" + ls_alias + "'")
	else
		 dw_report.Modify("rag_soc_2.text=''")
	end if
end if

if ls_flag_urgente = "S" and not isnull(ls_flag_urgente) then
	dw_report.Modify("l_urgente.visible=1")
else
	dw_report.Modify("l_urgente.visible=0")
end if	

if isnull(ls_indirizzo) then
	dw_report.Modify("indirizzo.text=' '")
else
	ls_indirizzo = wf_elabora(ls_indirizzo)
	dw_report.Modify("indirizzo.text='"+ ls_indirizzo + "'")
end if

if isnull(ls_cap) then ls_cap = ' '
dw_report.Modify("cap.text='"+ ls_cap + "'")


if isnull(ls_localita) then ls_localita = ' '
ls_localita = wf_elabora(ls_localita)
dw_report.Modify("localita.text='"+ ls_localita + "'")

if isnull(ls_provincia) then ls_provincia = ' '
dw_report.Modify("provincia.text='"+ ls_provincia + "'")

if isnull(ls_telefono) then ls_telefono = ' '
dw_report.Modify("telefono.text='"+ ls_telefono + "'")

if isnull(ls_fax) then ls_fax = ' '
dw_report.Modify("fax.text='"+ ls_fax + "'")

if isnull(ls_cod_operatore) then ls_cod_operatore = ' '
dw_report.Modify("cod_operatore.text='"+ ls_cod_operatore + "'")

if isnull(ls_des_reparto) then ls_des_reparto=' '
dw_report.Modify("des_reparto.text='"+ ls_des_reparto + "'")


// --- aggiunto da Enrico per Beatrice 10/02/2006 ----
if not isnull(fs_cod_reparto) then
	dw_report.Object.p_1.Filename = s_cs_xx.volume + s_cs_xx.risorse + fs_cod_reparto + ".bmp"
end if
// --- fine aggiunta 10/02/2006 ----------------------

if ls_flag_tipo_bcl='A' and ls_flag_sottotipo_bcl = "4" then
	if isnull(ls_nota_prodotto) then ls_nota_prodotto = ' '
	dw_report.Modify("num_ord_cliente.text='"+ ls_nota_prodotto + "'")
else
	if isnull(ls_num_ord_cliente) then ls_num_ord_cliente = ' '
	dw_report.Modify("num_ord_cliente.text='"+ ls_num_ord_cliente + "'")
end if

if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "STAMPA_ORD_PROD", ls_null, ldt_data_consegna, ls_nota_testata,ls_nota_piede, ls_nota_video) < 0 then
	g_mb.error(ls_nota_testata)
end if
//f_crea_nota_fissa (ls_cod_cliente, ls_null, "STAMPA_ORD_PROD", ldt_data_consegna, ls_nota_testata,ls_nota_piede, ls_nota_video )

if isnull(ls_rif_interscambio) then ls_rif_interscambio = ' '
dw_report.Modify("rif_interscambio.text=''")
//dw_report.Modify("rif_interscambio.text='"+ ls_rif_interscambio + "'")
if not isnull(ls_nota_testata) and len(ls_nota_testata) > 0 then
	dw_report.Modify("rif_interscambio.text='"+ ls_nota_testata + "'")
end if

if isnull(ls_des_imballo) then ls_des_imballo = ' '
dw_report.Modify("des_imballo.text=''")
if not isnull(ls_des_imballo) and len(ls_des_imballo) > 0 then
	dw_report.Modify("des_imballo.text='"+ ls_des_imballo + "'")
end if


if ls_flag_allegati="S" then dw_report.Modify("vedi_allegato.visible=1")

if ls_flag_tassativo="S" then dw_report.Modify("flag_tassativo.visible=1")

select 	progr_det_produzione,
			data_pronto
into   :ll_progr_det_produzione,
		:ldt_data_pronto_fasi
from   det_ordini_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione
and    prog_riga_ord_ven=:fl_prog_riga_comanda
and    cod_reparto=:fs_cod_reparto;


//VISUALIZZA LINGUA DEL CLIENTE
//##############################################
if not isnull(ls_cod_lingua) and ls_cod_lingua<>"" then
	dw_report.Modify("test_barcode_t.text='Lingua Cliente: "+ ls_cod_lingua + "'")
else
	dw_report.Modify("test_barcode_t.text=''")
end if
//##############################################


choose case sqlca.sqlcode
	
	case 0
		
		li_risposta = f_font_barcode(ls_bfo,li_bco,ls_errore)
		
		if li_risposta < 0 then 
			messagebox("SEP","Report 1 tende da sole: " + ls_errore,stopsign!)
			return -1
		end if
		
		dw_report.Modify("barcode.text='*"+is_CSB+ string(ll_progr_det_produzione) +is_CSB+ "*'")
		dw_report.Modify("barcode.Font.Face='" + ls_bfo + "'")
		dw_report.Modify("barcode.Font.Height= -" + string(li_bco) )
		
	case is < 0
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tabella det_ordini_produzione. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	
	case 100
		dw_report.Modify("barcode.text=''")
		
end choose


if not isnull(ldt_data_pronto_fasi) and year(date(ldt_data_pronto_fasi))>1950 then
	//uso la data pronto memorizzata nella fasi
	dw_report.Modify("data_pronto.text='"+ string(date(ldt_data_pronto_fasi), "dd/mm/yyyy") +"'")
	
	li_risposta = f_giorno_settimana(ldt_data_pronto_fasi, ls_giorno)
	dw_report.Modify("giorno_p.text='" + ls_giorno +"'")

//chiesto da Alberto: 26/05/2014
//in mancanza di date pronto reparto lasciare vuoto il campo sul report (quindi commento questo elseif)

//elseif not isnull(ldt_data_pronto) and year(date(ldt_data_pronto))>1950 then
//	//uso la data pronto memorizzata in tes_ord_ven: vecchia gestione e relativa compatibilità
//	dw_report.Modify("data_pronto.text='"+ string(date(ldt_data_pronto)) +"'")
//	
//	li_risposta = f_giorno_settimana(ldt_data_pronto, ls_giorno)
//	dw_report.Modify("giorno_p.text='" + ls_giorno +"'")
	
else
	dw_report.Modify("data_pronto.text=' '")
	dw_report.Modify("giorno_p.text=' '")
end if


//Donato 27/12/2011
//info aggiuntive su report OL (stabilimento successivo e lista reparti coinvolti)
luo_prod = create uo_produzione
luo_prod.il_sessione_tes_stampa = il_sessione
luo_prod.ib_altro_stabilimento = false
li_risposta = luo_prod.uof_get_ordine_reparti_2(	fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_comanda, fs_cod_reparto, &
																ls_lista_reparti_coinvolti, ls_stabilim_prossimo, ls_reparto_prossimo)

//metti in grassetto se riferito a stabilimento diverso di quello del reparto corrente
if luo_prod.ib_altro_stabilimento then
	dw_report.object.stab_succ_t.font.weight = 700
else
	dw_report.object.stab_succ_t.font.weight = 400
end if
//--------------------------------------------------------------------------------------------

//se la stampa si riferisce all'ultimo reparto della lista cambia la label dello stabilimento successivo
try
	if li_risposta = 1 then
		//ultimo reparto
		dw_report.object.stab_succ_label_t.text = "Stabilim. partenza merce"
	else
		dw_report.object.stab_succ_label_t.text = "Stabilim. successivo"
	end if
catch (runtimeerror e)
end try
											
destroy luo_prod;

dw_report.Modify("stab_succ_t.text='"+ls_stabilim_prossimo+"'")
dw_report.Modify("reparti_ordine_t.text='"+ls_lista_reparti_coinvolti+"'")
//-----------------------------------------------------


return 0

end function

public function integer f_report_2 (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_reparto);// Funzione che compila la datawindow di tipo report_2 (Sfuso)
// nome: f_report_2
// tipo: integer
//       -1 failed
//  		 0 passed
//
//		Creata il 22-12-1999 
//		Autore Diego Ferrari

string	ls_cod_cliente,ls_rag_soc_1,ls_rag_soc_2,ls_indirizzo,ls_cap,ls_localita,ls_provincia,ls_telefono,ls_fax, ls_null, ls_cod_deposito, &
		ls_cod_operatore,ls_cod_giro_consegna,ls_des_reparto,ls_flag_allegato,ls_cod_prodotto,ls_des_prodotto, & 
		ls_nota_dettaglio,ls_cod_misura,ls_cod_misura_mag,ls_cod_misura_ven,ls_giorno,ls_des_giro_consegna, &
		ls_flag_tassativo, ls_alias, ls_bfo,ls_errore,ls_num_ord_cliente,ls_rif_interscambio, ls_nota_prodotto, ls_flag_evasione_cu, &
 		ls_dest_test, ls_rs1_test, ls_rs2_test, ls_ind_test, ls_loc_test, ls_prov_test, ls_cap_test, ls_fraz_test,ls_flag_urgente, ls_chiave_etic, &
		ls_nota_testata,ls_nota_piede, ls_nota_video, ls_cod_imballo, ls_des_imballo, ls_des_variabile, ls_cod_vettore, ls_anag_vettori_rag_soc_1, &
		ls_cod_lingua, ls_cod_versione, ls_test_distinta, ls_flag_nascondi_cliente
			 
integer  li_risposta,li_bco

long     ll_num_commessa,ll_num_righe,ll_prog_riga_ord_ven,ll_progr_tes_produzione,ll_cont_urgente, ll_count_residuo

dec{4}	ld_dim_x_riga, ld_dim_y_riga, ld_peso_netto, ld_impegnato,ldd_quan_ordine, ldd_quan_vendita
dec{5}   ldd_fat_conversione_ven

datetime ldt_data_consegna, ldt_data_pronto, ldt_data_pronto_fasi

uo_produzione	luo_prod
uo_magazzino luo_magazzino


dw_report.Modify("t_azienda.text='"+wf_logo()+"'")

setnull(ls_null)

//Donato 12/03/2012 -----------------------------------------------------------------------
//preparo la dw per la filigrana del residuo, è necessario farlo ogni volta per dinamicamente ogni volta il dataobject viene re-impostato
if guo_functions.uof_filigrana(is_path_filigrana, dw_report, false, ls_errore)<0 then
	g_mb.error(ls_errore)
	return -1
end  if
//----------------------------------------------------------------------------------------------

if ib_aip then
	luo_magazzino = CREATE uo_magazzino
end if

select data_pronto,
		 data_consegna,
		 cod_cliente,
		 cod_giro_consegna,
		 cod_operatore,
		 flag_tassativo,
		 alias_cliente,
		 cod_des_cliente,
		 rag_soc_1,
		 rag_soc_2,
		 indirizzo,
		 localita,
		 frazione,
		 cap,
		 provincia,
		 num_ord_cliente,
		 rif_interscambio,
		 cod_imballo,
		 cod_vettore,
		 cod_deposito
into   :ldt_data_pronto,
		 :ldt_data_consegna,
		 :ls_cod_cliente,
		 :ls_cod_giro_consegna,
		 :ls_cod_operatore,
		 :ls_flag_tassativo,
		 :ls_alias,
		 :ls_dest_test,
		 :ls_rs1_test,
		 :ls_rs2_test,
		 :ls_ind_test,
		 :ls_loc_test,
		 :ls_fraz_test,
		 :ls_cap_test,
		 :ls_prov_test,
		 :ls_num_ord_cliente,
		 :ls_rif_interscambio,
		 :ls_cod_imballo,
		 :ls_cod_vettore,
		 :ls_cod_deposito
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_2 (f_report_2), lettura tes_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if isnull(ls_dest_test) and isnull(ls_rs1_test) and isnull(ls_rs2_test) and isnull(ls_ind_test) and &
   isnull(ls_loc_test) and isnull(ls_fraz_test) and isnull(ls_cap_test) and isnull(ls_prov_test) then
	dw_report.modify("t_lc.visible = 0")
else
	dw_report.modify("t_lc.visible = 1")
end if

select des_giro_consegna
into   :ls_des_giro_consegna
from   tes_giri_consegne
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_giro_consegna=:ls_cod_giro_consegna;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tes_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select des_reparto,
		flag_nascondi_cliente
into   :ls_des_reparto,
		:ls_flag_nascondi_cliente
from   anag_reparti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_reparto=:fs_cod_reparto;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_2 (f_report_2), lettura anag_reparti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select des_imballo
into   :ls_des_imballo
from   tab_imballi
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_imballo=:ls_cod_imballo;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tabella imballaggi. Dettaglio Errore: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if


		 
select rag_soc_1,
		 rag_soc_2,
		 indirizzo,
		 cap,
		 localita,
		 provincia,
		 telefono,
		 fax,
		 cod_lingua
into   :ls_rag_soc_1,
		 :ls_rag_soc_2,
		 :ls_indirizzo,
		 :ls_cap,
		 :ls_localita,
		 :ls_provincia,
		 :ls_telefono,
		 :ls_fax,
		 :ls_cod_lingua
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_2 (f_report_2), lettura anag_clienti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

dw_report.Modify("num_ordine.text='"+ string(fl_num_registrazione)+ "'")

wf_set_deposito_origine(fi_anno_registrazione, fl_num_registrazione, dw_report)

if isnull(ll_num_commessa) then
	dw_report.Modify("num_commessa.text=' '")
else
	dw_report.Modify("num_commessa.text='"+ string(ll_num_commessa) + "'")
end if

if isnull(ls_cod_giro_consegna) then
	dw_report.Modify("des_giro.text=' '")
else
	dw_report.Modify("des_giro.text='"+ ls_des_giro_consegna + "'")
end if

ls_rag_soc_1 = wf_elabora(ls_rag_soc_1)

dw_report.Modify("cod_cliente.text='"+ ls_cod_cliente + "'")
if ls_flag_nascondi_cliente="N" then 
	dw_report.Modify("rag_soc_1.text='" + ls_rag_soc_1 + "'")
else
	dw_report.Modify("rag_soc_1.text=''")
end if

//---------------------------------------------------------------------------------------------
//Donato 14/02/2012
//colonna dinamica ETIC
ls_chiave_etic = ""
if ls_cod_cliente<>"" and not isnull(ls_cod_cliente) then
	ls_chiave_etic = wf_get_colonna_dinamica(ls_cod_cliente)
end if
dw_report.Modify("t_chiave.text='"+ ls_chiave_etic + "'")
//---------------------------------------------------------------------------------------------

if isnull(ls_alias) then
	dw_report.Modify("rag_soc_2.text=' '")
else
	ls_alias = wf_elabora(ls_alias)
	if ls_flag_nascondi_cliente="N" then 
		dw_report.Modify("rag_soc_2.text='"+ ls_alias + "'")
	else
		dw_report.Modify("rag_soc_2.text=''")
	end if
end if
	
if isnull(ls_indirizzo) then
	dw_report.Modify("indirizzo.text=' '")
else
	ls_indirizzo = wf_elabora(ls_indirizzo)
	dw_report.Modify("indirizzo.text='"+ ls_indirizzo + "'")
end if

if isnull(ls_cap) then ls_cap = ' '
dw_report.Modify("cap.text='"+ ls_cap + "'")

if isnull(ls_localita) then ls_localita = ' '
ls_localita = wf_elabora(ls_localita)
dw_report.Modify("localita.text='"+ ls_localita + "'")

if isnull(ls_provincia) then ls_provincia = ' '
dw_report.Modify("provincia.text='"+ ls_provincia + "'")

if isnull(ls_telefono) then ls_telefono = ' '
dw_report.Modify("telefono.text='"+ ls_telefono + "'")

if isnull(ls_fax) then ls_fax = ' '
dw_report.Modify("fax.text='"+ ls_fax + "'")

if isnull(ls_cod_operatore) then ls_cod_operatore = ' '
dw_report.Modify("cod_operatore.text='"+ ls_cod_operatore + "'")

if isnull(ls_des_reparto) then ls_des_reparto=' '
dw_report.Modify("des_reparto.text='"+ ls_des_reparto + "'")

if isnull(ls_num_ord_cliente) then ls_num_ord_cliente = ' '
dw_report.Modify("num_ord_cliente.text='"+ ls_num_ord_cliente + "'")

if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "STAMPA_ORD_PROD", ls_null, ldt_data_consegna, ls_nota_testata,ls_nota_piede, ls_nota_video) < 0 then
	g_mb.error(ls_nota_testata)
end if
//f_crea_nota_fissa (ls_cod_cliente, ls_null, "STAMPA_ORD_PROD", ldt_data_consegna, ls_nota_testata,ls_nota_piede, ls_nota_video )

if isnull(ls_rif_interscambio) then ls_rif_interscambio = ' '
dw_report.Modify("rif_interscambio.text=''")
//dw_report.Modify("rif_interscambio.text='"+ ls_rif_interscambio + "'")
if not isnull(ls_nota_testata) and len(ls_nota_testata) > 0 then
	dw_report.Modify("rif_interscambio.text='"+ ls_nota_testata + "'")
end if

if isnull(ls_des_imballo) then ls_des_imballo = ' '
dw_report.Modify("des_imballo.text=''")
if not isnull(ls_des_imballo) and len(ls_des_imballo) > 0 then
	dw_report.Modify("des_imballo.text='"+ ls_des_imballo + "'")
end if


if ls_flag_tassativo="S" then dw_report.Modify("flag_tassativo.visible=1")

// ************************************** IMPOSTAZIONE CODICE A BARRE
select progr_tes_produzione
into   :ll_progr_tes_produzione
from   tes_ordini_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione
and    cod_reparto=:fs_cod_reparto;


//VISUALIZZA LINGUA DEL CLIENTE
//##############################################
if not isnull(ls_cod_lingua) and ls_cod_lingua<>"" then
	dw_report.Modify("test_barcode_t.text='Lingua Cliente: "+ ls_cod_lingua + "'")
else
	dw_report.Modify("test_barcode_t.text=''")
end if
//##############################################

choose case sqlca.sqlcode
	
	case 0
		
		li_risposta = f_font_barcode(ls_bfo,li_bco,ls_errore)
		
		if li_risposta < 0 then 
			messagebox("SEP","Report 2 sfuso: " + ls_errore,stopsign!)
			return -1
		end if
		
		dw_report.Modify("barcode.text='*"+is_CSB+ string(ll_progr_tes_produzione) +is_CSB+ "*'")
		dw_report.Modify("barcode.Font.Face='" + ls_bfo + "'")
		dw_report.Modify("barcode.Font.Height= -" + string(li_bco) )
		
	case is < 0
		messagebox("Apice","Errore durante la compilazione del report_2 (f_report_2), lettura tabella tes_ordini_produzione. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	
	case 100
		dw_report.Modify("barcode.text=''")
		
end choose


luo_prod = create uo_produzione
luo_prod.uof_get_data_pronto_reparto(ll_progr_tes_produzione, ldt_data_pronto_fasi)
destroy luo_prod

if not isnull(ldt_data_pronto_fasi) and year(date(ldt_data_pronto_fasi))>1950 then
	
	dw_report.Modify("data_pronto.text='"+ string(date(ldt_data_pronto_fasi)) +"'")
	li_risposta = f_giorno_settimana(ldt_data_pronto_fasi, ls_giorno)
	dw_report.Modify("giorno_p.text='" + ls_giorno +"'")

//chiesto da Alberto: 26/05/2014
//in mancanza di date pronto reparto lasciare vuoto il campo sul report (quindi commento questo elseif)

//elseif not isnull(ldt_data_pronto) and year(date(ldt_data_pronto))>1950 then
//	
//	dw_report.Modify("data_pronto.text='"+ string(date(ldt_data_pronto)) +"'")
//	li_risposta = f_giorno_settimana(ldt_data_pronto,ls_giorno)
//	dw_report.Modify("giorno_p.text='" + ls_giorno +"'")
	
else
	dw_report.Modify("data_pronto.text=' '")
	dw_report.Modify("giorno_p.text=' '")
	
end if



// ************************************** FINE IMPOSTAZIONE CODICE A BARRE

declare 	r_det_stampa_ordini cursor for 
select  	prog_riga_ord_ven
from    	det_stampa_ordini
where   	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_registrazione and
			num_registrazione=:fl_num_registrazione and
			cod_reparto=:fs_cod_reparto and
			prog_riga_comanda=0 and prog_sessione=:il_sessione;

open r_det_stampa_ordini;
if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la apertura cursore r_det_stampa_ordini (f_report_2)~r~n" + sqlca.sqlerrtext,stopsign!)
	return -1
end if

// EnMe per viropa 28/11/2007; se c'è anche solo una riga urgente, allora tutto l'ordine è urgente
ll_cont_urgente = 0

select count(*)
into   :ll_cont_urgente
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fi_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione and
		 flag_urgente = 'S';
if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante il conteggio delle righe urgenti (f_report_2)~r~n" + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if ll_cont_urgente > 0 then
	dw_report.Modify("l_urgente.visible=1")
else
	dw_report.Modify("l_urgente.visible=0")
end if
// fine modifica EnMe 28/11/2007



//Donato 12/03/2012 -------------------------------------------------------------------------------------------------------------------------------------
//verifico se siamo nella condizione di stampa residuo
// --> cioè se esiste almeno 1 riga in stato flag_evasione='E' o 'P'
//NOTA: 	le righe evase non devono comparire
//			quelle parziali devono comparire con quantità=quan_ordine - quan_in_evasione - quan_evasa
select count(*)
into :ll_count_residuo
from    det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda and
			 anno_registrazione=:fi_anno_registrazione and
			 num_registrazione=:fl_num_registrazione and
			 flag_evasione<>'A';

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_2 (f_report_2), controllo presenza righe residue. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	close r_det_stampa_ordini;
	return -1
end if

if ll_count_residuo > 0 then
	ls_errore=dw_report.modify("DataWindow.brushmode=6")
else
	ls_errore=dw_report.modify("DataWindow.brushmode=0")
end if
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------


// stefanop 13/06/2014: aggiunto vettore
if not isnull(ls_cod_vettore) then
	
	select rag_soc_1
	into :ls_anag_vettori_rag_soc_1
	from anag_vettori
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_vettore = :ls_cod_vettore;
			 
end if

do while true
	fetch r_det_stampa_ordini
	into  :ll_prog_riga_ord_ven;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_2 (f_report_2), lettura cursore det_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close r_det_stampa_ordini;
		return -1
	end if
	
	//già escludo le righe evase, ed inoltre dalla quantità della riga deve essere detratta qiuella in spedizione e quella già evasa
	select cod_prodotto,
			 des_prodotto,
			 cod_misura,
			 isnull(quan_ordine,0) - isnull(quan_in_evasione,0) - isnull(quan_evasa,0),
			 nota_dettaglio,
			 num_commessa,
			 fat_conversione_ven,
			 nota_prodotto,
			 flag_urgente,
			 dim_x,
			 dim_y,
			 peso_netto,
			 cod_versione
	into   :ls_cod_prodotto,
			 :ls_des_prodotto,
			 :ls_cod_misura,
			 :ldd_quan_ordine,
			 :ls_nota_dettaglio,
			 :ll_num_commessa,
			 :ldd_fat_conversione_ven,
			 :ls_nota_prodotto,
			 :ls_flag_urgente,
			 :ld_dim_x_riga,
			 :ld_dim_y_riga,
			 :ld_peso_netto,
			 :ls_cod_versione
	from   det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda and
			 anno_registrazione=:fi_anno_registrazione and
			 num_registrazione=:fl_num_registrazione and
			 prog_riga_ord_ven=:ll_prog_riga_ord_ven and
			 flag_evasione<>'E';
	
	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_2 (f_report_2), lettura det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close r_det_stampa_ordini;
		return -1
	end if
	
	select cod_misura_mag,
			 cod_misura_ven
	into   :ls_cod_misura_mag,
			 :ls_cod_misura_ven
	from   anag_prodotti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_prodotto=:ls_cod_prodotto;

	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_2 (f_report_2), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close r_det_stampa_ordini;
		return -1
	end if

	if not isnull(ls_nota_prodotto) then ls_nota_dettaglio = ls_nota_dettaglio + char(13) + char(10) + ls_nota_prodotto
	
	
	//NON VISUALIZZARE LE RIGHE ORDINE COMPLETAMENTE EVASE --------------
	select flag_evasione
	into 	:ls_flag_evasione_cu
	from det_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:fi_anno_registrazione and
			 	num_registrazione=:fl_num_registrazione and
			 	prog_riga_ord_ven=:ll_prog_riga_ord_ven;
	
	if ls_flag_evasione_cu="E" then continue
	//----------------------------------------------------------------------------------------
	
	ll_num_righe++
	dw_report.insertrow(ll_num_righe)
	
	dw_report.setitem(ll_num_righe,"riga",ll_prog_riga_ord_ven)
	dw_report.setitem(ll_num_righe,"codice_prodotto",ls_cod_prodotto)
	dw_report.setitem(ll_num_righe,"des_prodotto",ls_des_prodotto)
	dw_report.setitem(ll_num_righe,"um_mag",ls_cod_misura_mag)
	dw_report.setitem(ll_num_righe,"quan_mag",ldd_quan_ordine)
	dw_report.setitem(ll_num_righe,"um_ven",ls_cod_misura_ven)
	
	dw_report.setitem(ll_num_righe, "peso_netto", ld_peso_netto)
	dw_report.setitem(ll_num_righe, "cod_vettore", ls_cod_vettore)
	dw_report.setitem(ll_num_righe, "anag_vettori_rag_soc_1", ls_anag_vettori_rag_soc_1)
	dw_report.setitem(ll_num_righe, "data_partenza", ldt_data_consegna)
	
	ldd_quan_vendita=ldd_quan_ordine*ldd_fat_conversione_ven
	
	dw_report.setitem(ll_num_righe,"quan_ven",ldd_quan_vendita)
	dw_report.setitem(ll_num_righe,"num_commessa",ll_num_commessa)
	dw_report.setitem(ll_num_righe,"allegato"," ")
	
	dw_report.setitem(ll_num_righe,"flag_urgente",ls_flag_urgente)
	
	// stefanop 16/09/2014: allineo anagrafica prodottoi?
	if ib_aip then
		if luo_magazzino.uof_ricalcola_impegnato(ls_cod_prodotto, ref ls_errore) < 0 then
			rollback;
		end if
	end if
	
	select quan_impegnata
	into :ld_impegnato
	from anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	dw_report.setitem(ll_num_righe,"impegnato", ld_impegnato)
	// ----

	
	if isnull(ls_nota_dettaglio) then ls_nota_dettaglio = ""
//		EnMe 27/9/2012 stampo le dimensioni degli optionals (spec. Optionals Configurabili) ----------
	if ld_dim_y_riga > 0 and not isnull(ld_dim_y_riga) and not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then
		select des_variabile
		into :ls_des_variabile
		from tab_des_variabili
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto and
				cod_variabile = 'DIM_2' and
				flag_ipertech_apice='S';

		//se non trovo la des variabile ci metto di default DIM 2
		if ls_des_variabile="" or isnull(ls_des_variabile) then
			ls_des_variabile = "DIM 2"
		end if
		
		ls_nota_dettaglio = ls_des_variabile + "=" + string(ld_dim_y_riga,"###,##0.00") + " - " +  ls_nota_dettaglio

	end if
	
	ls_des_variabile = ""
	
	if ld_dim_x_riga > 0 and not isnull(ld_dim_x_riga) and not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then
		select des_variabile
		into :ls_des_variabile
		from tab_des_variabili
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto and
				cod_variabile = 'DIM_1' and
				flag_ipertech_apice='S';
		
		//se non trovo la des variabile ci metto di default DIM 1
		if ls_des_variabile="" or isnull(ls_des_variabile) then
			ls_des_variabile = "DIM 1"
		end if
		
		ls_nota_dettaglio= ls_des_variabile + "=" + string(ld_dim_x_riga,"###,##0.00") + " - " + ls_nota_dettaglio

	end if
	
	//		-----------fine EnMe 27/09/2012		
	dw_report.setitem(ll_num_righe,"nota_prodotto",ls_nota_dettaglio)
	//----------------------------------------------------------------------------------------------------------------
	//visualizzo la giacenza dell'articolo nel deposito testata ordine
	//se il campo giacenza non c'è sul report è gestito con un try/catch all'interno della stessa funzione
	wf_giacenza(ll_num_righe, ls_cod_prodotto, ls_cod_deposito, ls_errore)
	ls_errore = ""
	//----------------------------------------------------------------------------------------------------------------

	select 	cod_azienda
	into		:ls_test_distinta
	from		distinta_padri
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto and
				cod_versione = :ls_cod_versione;
	if sqlca.sqlcode = 0 then
		try
			dw_report.setitem(ll_num_righe,"cod_versione", ls_cod_versione)
		catch (runtimeerror err)
			//dw_report.setitem(ll_num_righe,"cod_versione", "")
		end try
	end if		

	setnull(ls_cod_prodotto)
	setnull(ls_des_prodotto)
	setnull(ls_cod_misura)
	setnull(ldd_quan_ordine)
	setnull(ls_nota_dettaglio)
	setnull(ll_num_commessa)
	setnull(ls_cod_misura_mag)
	setnull(ls_cod_misura_ven)
	setnull(ldd_fat_conversione_ven)
	setnull(ls_cod_versione)
	setnull(ls_nota_prodotto)
	setnull(ls_flag_urgente)
	setnull(ld_dim_x_riga)
	setnull(ld_dim_y_riga)
	setnull(ld_peso_netto)

loop		
			
close r_det_stampa_ordini;


return 0

end function

public function integer f_report_3 (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_reparto);// Funzione che compila la datawindow di tipo report_3 (Tende Tecniche)
// nome: f_report_3
// tipo: integer
//       -1 failed
//  		 0 passed
//
//
//		Creata il 22-12-1999 
//		Autore Diego Ferrari


string			ls_cod_cliente,ls_rag_soc_1,ls_rag_soc_2,ls_indirizzo,ls_cap,ls_localita,ls_provincia,ls_telefono,ls_fax, ls_null, &
				ls_cod_operatore,ls_cod_giro_consegna,ls_des_reparto,ls_flag_allegati,ls_cod_prodotto,ls_des_prodotto, & 
				ls_nota_dettaglio,ls_cod_misura,ls_cod_misura_mag,ls_cod_misura_ven,ls_cod_non_a_magazzino,ls_cod_prod_finito,&
				ls_cod_tessuto,ls_cod_tessuto_mant,ls_flag_tipo_mantovana,ls_flag_cordolo,ls_flag_passamaneria, ls_cod_comando_2, & 
				ls_cod_verniciatura,ls_cod_comando,ls_pos_comando,ls_cod_colore_lastra,ls_cod_tipo_lastra,ls_des_giro_consegna,&
				ls_cod_tipo_supporto,ls_flag_misura_luce_finita,ls_nota_configuratore,ls_colore_passamaneria,ls_colore_cordolo,&
				ls_des_vernice_fc,ls_des_tessuto,ls_des_comando,ls_des_breve_tipo_supporto,ls_des_breve_vernice,ls_giorno,&
				ls_des_comando_1,ls_flag_tassativo, ls_alias, ls_errore, ls_bfo,ls_num_ord_cliente,ls_rif_interscambio,&
				ls_dest_test, ls_rs1_test, ls_rs2_test, ls_ind_test, ls_loc_test, ls_prov_test, ls_cap_test, ls_fraz_test, ls_nota_prodotto, &
				ls_flag_urgente, ls_des_verniciatura, ls_lista_reparti_coinvolti, ls_stabilim_prossimo, ls_reparto_prossimo, ls_chiave_etic, &
				ls_nota_testata,ls_nota_piede, ls_nota_video, ls_cod_imballo, ls_des_imballo, ls_des_variabile, ls_cod_lingua, ls_flag_nascondi_cliente

integer		li_giorno_settimana, li_risposta, li_bco

long			ll_num_commessa,ll_num_righe,ll_prog_riga_ord_ven,ll_num_riga_appartenenza,ll_progr_tes_produzione,ll_cont_urgente

double		ldd_quan_ordine,ldd_fat_conversione_ven,ldd_quan_vendita,ldd_dim_x,ldd_dim_y, & 
				ldd_dim_z,ldd_dim_t,ldd_alt_mantovana,ldd_alt_asta
dec{4}		ld_dim_x_riga, ld_dim_y_riga
			
datetime		ldt_data_consegna, ldt_data_pronto,ldt_data_pronto_fasi

boolean		lb_prima_riga=true

uo_produzione luo_prod


setnull(ls_null)
//Donato 12/03/2012: no filigrana per tipo report 3
dw_report.modify("DataWindow.brushmode=0")

dw_report.Modify("t_azienda.text='"+wf_logo()+"'")


select data_pronto,
		 data_consegna,
		 cod_cliente,
		 cod_giro_consegna,
		 cod_operatore,
		 flag_tassativo,
		 alias_cliente,
		 cod_des_cliente,
		 rag_soc_1,
		 rag_soc_2,
		 indirizzo,
		 localita,
		 frazione,
		 cap,
		 provincia,
		 num_ord_cliente,
		 rif_interscambio,
		 cod_imballo
into   :ldt_data_pronto,
		 :ldt_data_consegna,
		 :ls_cod_cliente,
		 :ls_cod_giro_consegna,
		 :ls_cod_operatore,
		 :ls_flag_tassativo,
		 :ls_alias,
		 :ls_dest_test,
		 :ls_rs1_test,
		 :ls_rs2_test,
		 :ls_ind_test,
		 :ls_loc_test,
		 :ls_fraz_test,
		 :ls_cap_test,
		 :ls_prov_test,
		 :ls_num_ord_cliente,
		 :ls_rif_interscambio,
		 :ls_cod_imballo
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura tes_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if isnull(ls_dest_test) and isnull(ls_rs1_test) and isnull(ls_rs2_test) and isnull(ls_ind_test) and &
   isnull(ls_loc_test) and isnull(ls_fraz_test) and isnull(ls_cap_test) and isnull(ls_prov_test) then
	dw_report.modify("t_lc.visible = 0")
else
	dw_report.modify("t_lc.visible = 1")
end if

select des_giro_consegna
into   :ls_des_giro_consegna
from   tes_giri_consegne
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_giro_consegna=:ls_cod_giro_consegna;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tes_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select des_reparto,
		flag_nascondi_cliente
into   :ls_des_reparto,
		:ls_flag_nascondi_cliente
from   anag_reparti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_reparto=:fs_cod_reparto;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura anag_reparti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select des_imballo
into   :ls_des_imballo
from   tab_imballi
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_imballo=:ls_cod_imballo;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tabella imballaggi. Dettaglio Errore: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

 
 
select rag_soc_1,
		 rag_soc_2,
		 indirizzo,
		 cap,
		 localita,
		 provincia,
		 telefono,
		 fax,
		 cod_lingua
into   :ls_rag_soc_1,
		 :ls_rag_soc_2,
		 :ls_indirizzo,
		 :ls_cap,
		 :ls_localita,
		 :ls_provincia,
		 :ls_telefono,
		 :ls_fax,
		 :ls_cod_lingua
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura anag_clienti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if


dw_report.Modify("num_ordine.text='"+ string(fl_num_registrazione)+ "'")

wf_set_deposito_origine(fi_anno_registrazione, fl_num_registrazione, dw_report)

if isnull(ll_num_commessa) then
	dw_report.Modify("num_commessa.text=' '")
else
	dw_report.Modify("num_commessa.text='"+ string(ll_num_commessa) + "'")
end if

if isnull(ls_cod_giro_consegna) then
	dw_report.Modify("des_giro.text=' '")
else
	dw_report.Modify("des_giro.text='"+ ls_des_giro_consegna + "'")
end if

ls_rag_soc_1 = wf_elabora(ls_rag_soc_1)

dw_report.Modify("cod_cliente.text='"+ ls_cod_cliente + "'")
if ls_flag_nascondi_cliente="N" then 
	dw_report.Modify("rag_soc_1.text='" + ls_rag_soc_1 + "'")
else
	dw_report.Modify("rag_soc_1.text=''")
end if

//---------------------------------------------------------------------------------------------
//Donato 14/02/2012
//colonna dinamica ETIC
ls_chiave_etic = ""
if ls_cod_cliente<>"" and not isnull(ls_cod_cliente) then
	ls_chiave_etic = wf_get_colonna_dinamica(ls_cod_cliente)
end if
dw_report.Modify("t_chiave.text='"+ ls_chiave_etic + "'")
//---------------------------------------------------------------------------------------------

if isnull(ls_alias) then
	dw_report.Modify("rag_soc_2.text=' '")
else
	ls_alias = wf_elabora(ls_alias)
	if ls_flag_nascondi_cliente="N" then 
		dw_report.Modify("rag_soc_2.text='"+ ls_alias + "'")
	else
		dw_report.Modify("rag_soc_2.text=''")
	end if
end if
	
if isnull(ls_indirizzo) then
	dw_report.Modify("indirizzo.text=' '")
else
	ls_indirizzo = wf_elabora(ls_indirizzo)
	dw_report.Modify("indirizzo.text='"+ ls_indirizzo + "'")
end if

if isnull(ls_cap) then ls_cap = ' '
dw_report.Modify("cap.text='"+ ls_cap + "'")


if isnull(ls_localita) then ls_localita = ' '
ls_localita = wf_elabora(ls_localita)
dw_report.Modify("localita.text='"+ ls_localita + "'")

if isnull(ls_provincia) then ls_provincia = ' '
dw_report.Modify("provincia.text='"+ ls_provincia + "'")

if isnull(ls_telefono) then ls_telefono = ' '
dw_report.Modify("telefono.text='"+ ls_telefono + "'")

if isnull(ls_fax) then ls_fax = ' '
dw_report.Modify("fax.text='"+ ls_fax + "'")

if isnull(ls_cod_operatore) then ls_cod_operatore = ' '
dw_report.Modify("cod_operatore.text='"+ ls_cod_operatore + "'")

if isnull(ls_des_reparto) then ls_des_reparto=' '
dw_report.Modify("des_reparto.text='"+ ls_des_reparto + "'")


// --- aggiunto da Enrico per Beatrice 10/02/2006 ----
if not isnull(fs_cod_reparto) then
	dw_report.Object.p_1.Filename = s_cs_xx.volume + s_cs_xx.risorse + fs_cod_reparto + ".bmp"
end if
// --- fine aggiunta 10/02/2006 ----------------------


if isnull(ls_num_ord_cliente) then ls_num_ord_cliente = ' '
dw_report.Modify("num_ord_cliente.text='"+ ls_num_ord_cliente + "'")

if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "STAMPA_ORD_PROD", ls_null, ldt_data_consegna, ls_nota_testata,ls_nota_piede, ls_nota_video) < 0 then
	g_mb.error(ls_nota_testata)
end if
//f_crea_nota_fissa (ls_cod_cliente, ls_null, "STAMPA_ORD_PROD", ldt_data_consegna, ls_nota_testata,ls_nota_piede, ls_nota_video )

if isnull(ls_rif_interscambio) then ls_rif_interscambio = ' '
dw_report.Modify("rif_interscambio.text=''")
if not isnull(ls_nota_testata) and len(ls_nota_testata) > 0 then
	dw_report.Modify("rif_interscambio.text='"+ ls_nota_testata + "'")
end if

if isnull(ls_des_imballo) then ls_des_imballo = ' '
dw_report.Modify("des_imballo.text=''")
if not isnull(ls_des_imballo) and len(ls_des_imballo) > 0 then
	dw_report.Modify("des_imballo.text='"+ ls_des_imballo + "'")
end if

if ls_flag_tassativo="S" then dw_report.Modify("flag_tassativo.visible=1")

// ************************************** IMPOSTAZIONE CODICE A BARRE
select progr_tes_produzione
into   :ll_progr_tes_produzione
from   tes_ordini_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione
and    cod_reparto=:fs_cod_reparto;


//VISUALIZZA LINGUA DEL CLIENTE
//##############################################
if not isnull(ls_cod_lingua) and ls_cod_lingua<>"" then
	dw_report.Modify("test_barcode_t.text='Lingua Cliente: "+ ls_cod_lingua + "'")
else
	dw_report.Modify("test_barcode_t.text=''")
end if
//##############################################


choose case sqlca.sqlcode
	
	case 0
		
		li_risposta = f_font_barcode(ls_bfo,li_bco,ls_errore)
		
		if li_risposta < 0 then 
			messagebox("SEP","Report 2 sfuso: " + ls_errore,stopsign!)
			return -1
		end if
		
		dw_report.Modify("barcode.text='*"+is_CSB+ string(ll_progr_tes_produzione) +is_CSB+ "*'")
		dw_report.Modify("barcode.Font.Face='" + ls_bfo + "'")
		dw_report.Modify("barcode.Font.Height= -" + string(li_bco) )
		
	case is < 0
		messagebox("Apice","Errore durante la compilazione del report_2 (f_report_2), lettura tabella tes_ordini_produzione. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	
	case 100
		dw_report.Modify("barcode.text=''")
		
end choose



luo_prod = create uo_produzione
luo_prod.uof_get_data_pronto_reparto(ll_progr_tes_produzione, ldt_data_pronto_fasi)
destroy luo_prod


if not isnull(ldt_data_pronto_fasi) and year(date(ldt_data_pronto_fasi))>1950 then
	
	dw_report.Modify("data_pronto.text='"+ string(date(ldt_data_pronto_fasi)) +"'")
	li_risposta = f_giorno_settimana(ldt_data_pronto_fasi, ls_giorno)
	dw_report.Modify("giorno_p.text='" + ls_giorno +"'")

//chiesto da Alberto: 26/05/2014
//in mancanza di date pronto reparto lasciare vuoto il campo sul report (quindi commento questo elseif)

//elseif not isnull(ldt_data_pronto) and year(date(ldt_data_pronto))>1950 then
//	
//	dw_report.Modify("data_pronto.text='"+ string(date(ldt_data_pronto)) +"'")
//	li_risposta = f_giorno_settimana(ldt_data_pronto,ls_giorno)
//	dw_report.Modify("giorno_p.text='" + ls_giorno +"'")
	
else
	dw_report.Modify("data_pronto.text=' '")
	dw_report.Modify("giorno_p.text=' '")
	
end if


// ************************************** FINE IMPOSTAZIONE CODICE A BARRE


declare 	r_det_stampa_ordini cursor for 
select  	prog_riga_ord_ven
from    	det_stampa_ordini
where   	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_registrazione and
			num_registrazione=:fl_num_registrazione and
			cod_reparto=:fs_cod_reparto and
			prog_riga_comanda=0 and prog_sessione=:il_sessione;

open r_det_stampa_ordini;
if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante il open cursore r_det_stampa_ordini (f_report_3)~r~n" + sqlca.sqlerrtext,stopsign!)
	return -1
end if


// EnMe per viropa 28/11/2007; se c'è anche solo una riga urgente, allora tutto l'ordine è urgente
ll_cont_urgente = 0

select count(*)
into   :ll_cont_urgente
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fi_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione and
		 flag_urgente = 'S';
if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante il conteggio delle righe urgenti (f_report_2)~r~n" + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if ll_cont_urgente > 0 then
	dw_report.Modify("l_urgente.visible=1")
else
	dw_report.Modify("l_urgente.visible=0")
end if
// fine modifica EnMe 28/11/2007


do while true
	fetch r_det_stampa_ordini
	into  :ll_prog_riga_ord_ven;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura cursore det_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close r_det_stampa_ordini;
		return -1
	end if

	declare r_det_ord_ven_3 cursor for
	select cod_prodotto,
			 des_prodotto,
			 cod_misura,
			 quan_ordine,
			 nota_dettaglio,
			 num_commessa,
			 num_riga_appartenenza,
			 nota_prodotto,
			 flag_urgente,
			 dim_x,
			 dim_y
	from   det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda and
			 anno_registrazione=:fi_anno_registrazione and
			 num_registrazione=:fl_num_registrazione and
			(num_riga_appartenenza=:ll_prog_riga_ord_ven or prog_riga_ord_ven=:ll_prog_riga_ord_ven)
	order by prog_riga_ord_ven;

	open r_det_ord_ven_3;
	
	do while true
		fetch r_det_ord_ven_3
		into  :ls_cod_prodotto,
				:ls_des_prodotto,
				:ls_cod_misura,
				:ldd_quan_ordine,
				:ls_nota_dettaglio,
				:ll_num_commessa,
				:ll_num_riga_appartenenza,
				:ls_nota_prodotto,
				:ls_flag_urgente,
				:ld_dim_x_riga,
				:ld_dim_y_riga;

		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_det_ord_ven_3;
			close r_det_stampa_ordini;
			return -1
		end if

		if sqlca.sqlcode = 100 then exit
		
		select cod_misura_mag,
				 cod_misura_ven,
				 fat_conversione_ven
		into   :ls_cod_misura_mag,
				 :ls_cod_misura_ven,
				 :ldd_fat_conversione_ven
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto;

		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_det_stampa_ordini;
			return -1
		end if
	
		if ll_num_riga_appartenenza = 0 then
			select cod_non_a_magazzino,
					 cod_prod_finito,
					 dim_x,
					 dim_y,
					 dim_z,
					 dim_t,
					 cod_tessuto,
					 cod_tessuto_mant,
					 flag_tipo_mantovana,
					 alt_mantovana,
					 flag_cordolo,
					 flag_passamaneria,
					 cod_verniciatura,
					 cod_comando,
					 pos_comando,
					 alt_asta,
					 cod_colore_lastra,
					 cod_tipo_lastra,
					 cod_tipo_supporto,
					 flag_misura_luce_finita,
					 note,
					 colore_passamaneria,
					 colore_cordolo,
					 des_vernice_fc,
					 cod_comando_2,
					 flag_allegati,
					 des_comando_1
			into   :ls_cod_non_a_magazzino,
					 :ls_cod_prod_finito,
					 :ldd_dim_x,
					 :ldd_dim_y,
					 :ldd_dim_z,
					 :ldd_dim_t,
					 :ls_cod_tessuto,
					 :ls_cod_tessuto_mant,
					 :ls_flag_tipo_mantovana,
					 :ldd_alt_mantovana,
					 :ls_flag_cordolo,
					 :ls_flag_passamaneria,
					 :ls_cod_verniciatura,
					 :ls_cod_comando,
					 :ls_pos_comando,
					 :ldd_alt_asta,
					 :ls_cod_colore_lastra,
					 :ls_cod_tipo_lastra,
					 :ls_cod_tipo_supporto,
					 :ls_flag_misura_luce_finita,
					 :ls_nota_configuratore,
					 :ls_colore_passamaneria,
					 :ls_colore_cordolo,
					 :ls_des_vernice_fc,
					 :ls_cod_comando_2,
					 :ls_flag_allegati,
					 :ls_des_comando_1
			from   comp_det_ord_ven
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    anno_registrazione=:fi_anno_registrazione
			and    num_registrazione=:fl_num_registrazione
			and    prog_riga_ord_ven=:ll_prog_riga_ord_ven;
			
			if sqlca.sqlcode < 0 then 
				messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura comp_det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				return -1
			end if
		
		end if
		
		select des_prodotto
		into   :ls_des_tessuto
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_tessuto;
	
		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		select des_prodotto
		into   :ls_des_comando
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_comando;
	
		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		select cod_comodo
		into   :ls_des_breve_tipo_supporto
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_tipo_supporto;
	
		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
	
		select cod_comodo
		into   :ls_des_breve_vernice
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_verniciatura;
	
		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
	
		ll_num_righe++
		dw_report.insertrow(ll_num_righe)
		
		dw_report.setitem(ll_num_righe,"riga",ll_prog_riga_ord_ven)
		dw_report.setitem(ll_num_righe,"tipo_riga",1)
		
		if ll_num_riga_appartenenza = 0 then
			dw_report.setitem(ll_num_righe,"codice_prodotto",ls_cod_prod_finito)	
		else
			dw_report.setitem(ll_num_righe,"codice_prodotto",ls_cod_prodotto)	
		end if
	
		// stefanop 10/06/2010: progetto 140: accodo la descrizione tessuto alla descrizione
		// VERNICIATURA
		select des_prodotto
		into :ls_des_verniciatura
		from anag_prodotti
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_verniciatura;
		
		if sqlca.sqlcode <> 0 or isnull(ls_des_verniciatura) then ls_des_verniciatura = ""
		
		if not isnull(ls_des_tessuto) or ls_des_tessuto <> "" then
			ls_des_prodotto += " - " + ls_des_tessuto
		end if
		ls_des_prodotto += " - " + ls_des_verniciatura
		dw_report.setitem(ll_num_righe,"des_prodotto",ls_des_prodotto)
		
		// CODICE ORIGINALE
		//dw_report.setitem(ll_num_righe,"des_prodotto",ls_des_prodotto)
		//dw_report.setitem(ll_num_righe,"des_tessuto",ls_des_tessuto)
		// ----
		dw_report.setitem(ll_num_righe,"cod_colore",ls_cod_non_a_magazzino)
		dw_report.setitem(ll_num_righe,"um_mag",ls_cod_misura_mag)
		dw_report.setitem(ll_num_righe,"quan_mag",ldd_quan_ordine)
		dw_report.setitem(ll_num_righe,"larghezza",ldd_dim_x)
		dw_report.setitem(ll_num_righe,"altezza",ldd_dim_y)
		dw_report.setitem(ll_num_righe,"luce_finita",ls_flag_misura_luce_finita)
		dw_report.setitem(ll_num_righe,"posizione_comando",ls_pos_comando)
		dw_report.setitem(ll_num_righe,"tipo_comando",ls_des_comando)
		dw_report.setitem(ll_num_righe,"altezza_comando",ldd_alt_asta)

		if not isnull(ls_cod_comando_2) then 
			dw_report.setitem(ll_num_righe,"guide","S")
		else
			dw_report.setitem(ll_num_righe,"guide"," ")
		end if
		
		dw_report.setitem(ll_num_righe,"des_tipo_supporto",ls_des_breve_tipo_supporto)
		dw_report.setitem(ll_num_righe,"des_tipo_vernice",ls_des_breve_vernice)
		dw_report.setitem(ll_num_righe,"num_commessa",ll_num_commessa)
		dw_report.setitem(ll_num_righe,"allegato",ls_flag_allegati)
		dw_report.setitem(ll_num_righe,"flag_urgente",ls_flag_urgente)
		
		if isnull(ls_nota_dettaglio) then ls_nota_dettaglio = ""
//		EnMe 27/9/2012 stampo le dimensioni degli optionals (spec. Optionals Configurabili) ----------
		if ll_num_riga_appartenenza > 0 then
			if ld_dim_y_riga > 0 and not isnull(ld_dim_y_riga) and not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then
				select des_variabile
				into :ls_des_variabile
				from tab_des_variabili
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :ls_cod_prodotto and
						cod_variabile = 'DIM_2' and
						flag_ipertech_apice='S';

				//se non trovo la des variabile ci metto di default DIM 2
				if ls_des_variabile="" or isnull(ls_des_variabile) then
					ls_des_variabile = "DIM 2"
				end if
			
				ls_nota_dettaglio = ls_des_variabile + "=" + string(ld_dim_y_riga,"###,##0.00") + " - " +  ls_nota_dettaglio

			end if
			
			ls_des_variabile = ""
			
			if ld_dim_x_riga > 0 and not isnull(ld_dim_x_riga) and not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then
				select des_variabile
				into :ls_des_variabile
				from tab_des_variabili
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :ls_cod_prodotto and
						cod_variabile = 'DIM_1' and
						flag_ipertech_apice='S';

				//se non trovo la des variabile ci metto di default DIM 1
				if ls_des_variabile="" or isnull(ls_des_variabile) then
					ls_des_variabile = "DIM 1"
				end if
				
				ls_nota_dettaglio= ls_des_variabile + "=" + string(ld_dim_x_riga,"###,##0.00") + " - " + ls_nota_dettaglio
			end if
		end if 
//		-----------fine EnMe 27/09/2012		
		
		if not isnull(ls_nota_dettaglio) and ls_nota_dettaglio<>"" then
			ll_num_righe++
			dw_report.insertrow(ll_num_righe)
			dw_report.setitem(ll_num_righe,"tipo_riga",0)
			dw_report.setitem(ll_num_righe,"nota",ls_nota_dettaglio)
		end if

		if not isnull(ls_nota_prodotto) and ls_nota_prodotto<>"" then
			ll_num_righe++
			dw_report.insertrow(ll_num_righe)
			dw_report.setitem(ll_num_righe,"tipo_riga",0)
			dw_report.setitem(ll_num_righe,"nota",ls_nota_prodotto)
		end if


		if not isnull(ls_des_vernice_fc) and ls_des_vernice_fc<>"" then
			ll_num_righe++
			dw_report.insertrow(ll_num_righe)
			dw_report.setitem(ll_num_righe,"tipo_riga",0)
			dw_report.setitem(ll_num_righe,"nota",ls_des_vernice_fc)
		end if

		if not isnull(ls_des_comando_1) and ls_des_comando_1<>"" then
			ll_num_righe++
			dw_report.insertrow(ll_num_righe)
			dw_report.setitem(ll_num_righe,"tipo_riga",0)
			dw_report.setitem(ll_num_righe,"nota",ls_des_comando_1)
		end if

		setnull(ls_cod_non_a_magazzino)
		setnull(ls_cod_prod_finito)
		setnull(ldd_dim_x)
		setnull(ldd_dim_y)
		setnull(ldd_dim_z)
		setnull(ldd_dim_t)
		setnull(ls_cod_tessuto)
		setnull(ls_des_tessuto)
		setnull(ls_cod_tessuto_mant)
		setnull(ls_flag_tipo_mantovana)
		setnull(ldd_alt_mantovana)
		setnull(ls_flag_cordolo)
		setnull(ls_flag_passamaneria)
		setnull(ls_cod_verniciatura)
		setnull(ls_cod_comando)
		setnull(ls_pos_comando)
		setnull(ls_des_comando)
		setnull(ldd_alt_asta)
		setnull(ls_cod_colore_lastra)
		setnull(ls_cod_tipo_lastra)
		setnull(ls_cod_tipo_supporto)
		setnull(ls_flag_misura_luce_finita)
		setnull(ls_nota_configuratore)
		setnull(ls_colore_passamaneria)
		setnull(ls_colore_cordolo)
		setnull(ls_des_breve_tipo_supporto)
		setnull(ls_des_vernice_fc)
		setnull(ls_cod_comando_2)
		setnull(ls_cod_prodotto)
		setnull(ls_des_prodotto)
		setnull(ls_cod_misura_mag)
		setnull(ldd_quan_ordine)
		setnull(ls_nota_dettaglio)
		setnull(ll_num_commessa)
		setnull(ls_cod_misura_mag)
		setnull(ls_cod_misura_ven)
		setnull(ldd_fat_conversione_ven)
		setnull(ls_des_comando_1)
		setnull(ls_des_breve_vernice)
		
	loop
	
	close r_det_ord_ven_3;
	
	//Donato 27/12/2011
	//entro in questo IF solo la prima volta --------------------
	if lb_prima_riga then
		lb_prima_riga = false
		
		//info aggiuntive su report OL (stabilimento successivo e lista reparti coinvolti)
		luo_prod = create uo_produzione
		luo_prod.il_sessione_tes_stampa = il_sessione
		
		li_risposta = luo_prod.uof_get_ordine_reparti_2(	fi_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven, fs_cod_reparto, &
																ls_lista_reparti_coinvolti, ls_stabilim_prossimo, ls_reparto_prossimo)
		destroy luo_prod;
		
		//se la stampa si riferisce all'ultimo reparto della lista cambia la label dello stabilimento successivo
		try
			if li_risposta = 1 then
				//ultimo reparto
				dw_report.object.stab_succ_label_t.text = "Stabilim. partenza merce"
			else
				dw_report.object.stab_succ_label_t.text = "Stabilim. successivo"
			end if
		catch (runtimeerror e)
		end try
		
		dw_report.Modify("stab_succ_t.text='"+ls_stabilim_prossimo+"'")
		dw_report.Modify("reparti_ordine_t.text='"+ls_lista_reparti_coinvolti+"'")
	end if
	//---------------------------------------------------------------
	
loop		
			
close r_det_stampa_ordini;

return 0



end function

public function integer f_etichetta_cg ();long			ll_num_registrazione,ll_riga,ll_selected[],ll_prog_riga_ord_ven,ll_num_reparti,ll_prog_riga_comanda,job, & 
				ll_num_tenda, ll_num_ordini, ll_tot_ordini,ll_num_copie_sole,ll_num_copie_tecniche,ll_num_copie_sfuso,ll_t, &
				ll_num_righe_effettive,ll_num_riga_corrente,ll_num_sequenza,ll_progressivo,ll_prog_calcolo,ll_num_taglio,ll_i,&
				ll_num_tot_tecniche,ll_conteggio, ll_z
				
integer		li_anno_registrazione,li_risposta,li_numero_etichetta, ll_quan_ordine

string		ls_cod_prodotto,ls_cod_tipo_det_ven,ls_test,ls_cod_versione,ls_cod_reparto_test, ls_tipo_stampa,ls_test_2,& 
				ls_cod_tipo_ord_ven,ls_errore,ls_cod_reparto,ls_flag_stampa_fase,ls_cod_reparti_trovati[],ls_flag_non_calcolare_dt, &
				ls_str, ls_cod_operatore, ls_flag_calcola_dt,ls_sql,ls_cgibus,ls_rag_soc_1,ls_riferimento,ls_giro_consegna,ls_indirizzo,&
				ls_destinazione_diversa,ls_des_prodotto,ls_dimensioni,ls_cod_cliente,ls_cod_giro_consegna,ls_des_giro_consegna,&
				ls_cod_prodotto_figlio,ls_cod_prodotto_variante,ls_um_quantita,ls_b_um_quantita,ls_um_misura,ls_b_um_misura,&
				ls_cod_prodotto_padre,ls_des_distinta_taglio,ls_des_prodotto_figlio,ls_quan_ordine,ls_localita,ls_cap,ls_provincia, &
				ls_indirizzo_2,ls_indirizzo_diverso,ls_localita_diversa,ls_cap_diverso,ls_provincia_diversa,ls_indirizzo_diverso_2, &
				ls_nota_prodotto, ls_num_ord_cliente, ls_rif_interscambio, ls_rag_soc_abbreviata, ls_flag_ordinamento_dt
				
datetime	ldt_data_consegna,ldt_data_consegna_riga

decimal{4} ld_quan_ordine,ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t,ld_quantita,ld_misura


setpointer(hourglass!)

uo_stampa_etichette luo_stampa_etichette

luo_stampa_etichette = CREATE uo_stampa_etichette

dw_lista_ord_ven_stampa_prod.get_selected_rows(ll_selected[])

ls_flag_calcola_dt = dw_ext_selezione.getitemstring(1,"flag_calcola_dt")

ls_flag_ordinamento_dt = dw_ext_selezione.getitemstring(1,"flag_ordinamento_dt")

for ll_riga = 1 to upperbound(ll_selected[])
	
	ll_num_registrazione =dw_lista_ord_ven_stampa_prod.getitemnumber(ll_selected[ll_riga],"num_registrazione")
	li_anno_registrazione =dw_lista_ord_ven_stampa_prod.getitemnumber(ll_selected[ll_riga],"anno_registrazione")
	ls_cod_tipo_ord_ven = dw_lista_ord_ven_stampa_prod.getitemstring(ll_selected[ll_riga],"tipo_ord_ven")
	ldt_data_consegna = dw_lista_ord_ven_stampa_prod.getitemdatetime(ll_selected[ll_riga],"data_consegna")
	
	select flag_tipo_bcl
	into   :ls_tipo_stampa
	from   tab_tipi_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;
	
	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if

	ll_num_tenda = 0
	ll_num_riga_corrente = 0
	choose case ls_tipo_stampa			// verifico il tipo di report 'A'=Report_1 o tende da sole, 'B'=Report_2 o sfuso , 'C'=Report_3 tende tecniche

//************** Inizio Case per report_1
		case 'A'			
			
			declare  righe_det_ord_ven_1 cursor for
			select   cod_prodotto,
					   cod_tipo_det_ven,
					   prog_riga_ord_ven,
					   cod_versione,
						data_consegna,
						quan_ordine,
						nota_prodotto
			from     det_ord_ven
			where    cod_azienda=:s_cs_xx.cod_azienda
			and      anno_registrazione=:li_anno_registrazione
			and      num_registrazione=:ll_num_registrazione
			and      num_riga_appartenenza=0
			and      flag_blocco='N'
			order by prog_riga_ord_ven;
			
			open righe_det_ord_ven_1;
			if sqlca.sqlcode < 0 then 
				messagebox("Apice","Caso Report_1, OPEN cursore su det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				rollback;
				return -1
			end if
			
			do while 1 = 1															//  scorre tutti i dettagli dell'ordine
				fetch righe_det_ord_ven_1
				into  :ls_cod_prodotto,
						:ls_cod_tipo_det_ven,
						:ll_prog_riga_ord_ven,
						:ls_cod_versione,
						:ldt_data_consegna_riga,
						:ld_quan_ordine,
						:ls_nota_prodotto;

						
				if sqlca.sqlcode = 100 then exit
				
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Caso Report_1, lettura cursore su det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					close righe_det_ord_ven_1;
					rollback;
					return -1
				end if
				
				if isnull(ls_nota_prodotto) then ls_nota_prodotto = ""
				
				ll_num_tenda++									

				//************************************************************************************************
				//***********************   CALCOLO DELLA DISTINTA DI TAGLIO          ****************************

				if ls_flag_calcola_dt = "S"  then

					select flag_non_calcolare_dt
					into   :ls_flag_non_calcolare_dt
					from   distinta_padri
					where  cod_azienda=:s_cs_xx.cod_azienda
					and    cod_prodotto=:ls_cod_prodotto
					and    cod_versione=:ls_cod_versione;
					
					if sqlca.sqlcode< 0 then
						messagebox("Apice","caso report 1, lettura flag_non_calcolare_dt, Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
						close righe_det_ord_ven_1;
						rollback;
						return -1
					end if

					if ls_flag_non_calcolare_dt = 'N' or isnull(ls_flag_non_calcolare_dt) then
						
						delete distinte_taglio_calcolate
						where   cod_azienda=:s_cs_xx.cod_azienda and
									anno_registrazione=:li_anno_registrazione and
									num_registrazione=:ll_num_registrazione and
									prog_riga_ord_ven=:ll_prog_riga_ord_ven;
		
						if sqlca.sqlcode< 0 then
							messagebox("Apice","caso report 1, errore durante verifica presenza di distinte di taglio già calcolate, Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
							close righe_det_ord_ven_1;
							rollback;
							return -1
						end if
						
						li_risposta = f_calcola_dt(li_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_prodotto, ls_cod_versione, ls_flag_ordinamento_dt,ls_errore)
						
						if li_risposta < 0 then
							messagebox("Apice",ls_errore,stopsign!)
							close righe_det_ord_ven_1;
							rollback;
							return -1
						end if
						
					end if
					
				end if

				//************************  FINE CALCOLO DISTINTA DI TAGLIO  *****************************
				//****************************************************************************************
				
				
				// *** stampa etichette di testata tende da sole
				
				select count(*) 
				into   :ll_num_righe_effettive
				from   det_ord_ven
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    anno_registrazione=:li_anno_registrazione
				and    num_registrazione=:ll_num_registrazione
				and    num_riga_appartenenza=0
				and    flag_blocco = 'N';
				
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					return -1
				end if
				
				ll_num_riga_corrente++
				
				select cod_cliente,
						 cod_giro_consegna,
						 indirizzo,
						 localita,
						 cap,
						 provincia,
						 num_ord_cliente,
						 rif_interscambio
				into   :ls_cod_cliente,
						 :ls_cod_giro_consegna,
						 :ls_indirizzo_diverso,
						 :ls_localita_diversa,
						 :ls_cap_diverso,
						 :ls_provincia_diversa,
						 :ls_num_ord_cliente,
						 :ls_rif_interscambio
				from   tes_ord_ven
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    anno_registrazione=:li_anno_registrazione
				and    num_registrazione=:ll_num_registrazione;

				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					return -1
				end if
				
				if isnull(ls_indirizzo_diverso) then ls_indirizzo_diverso = ""
				if isnull(ls_localita_diversa) then ls_localita_diversa = ""
				if isnull(ls_cap_diverso) then ls_cap_diverso = ""
				if isnull(ls_provincia_diversa) then ls_provincia_diversa = ""
				if isnull(ls_num_ord_cliente) then ls_num_ord_cliente = ""
				if isnull(ls_rif_interscambio) then ls_rif_interscambio = ""
				
				ls_indirizzo_diverso_2 = ls_cap_diverso + " " + ls_localita_diversa + " (" + ls_provincia_diversa  + ")"
				
				select rag_soc_1,
						 indirizzo,
						 localita,
						 cap,
						 provincia,
						 rag_soc_abbreviata
				into   :ls_rag_soc_1,
						 :ls_indirizzo,
						 :ls_localita,
						 :ls_cap,
						 :ls_provincia,
						 :ls_rag_soc_abbreviata
				from   anag_clienti
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_cliente=:ls_cod_cliente;
				
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					return -1
				end if
				
				if isnull(ls_indirizzo) then ls_indirizzo = ""
				if isnull(ls_localita) then ls_localita = ""
				if isnull(ls_cap) then ls_cap = ""
				if isnull(ls_provincia) then ls_provincia = ""
				if isnull(ls_rag_soc_abbreviata) then ls_rag_soc_abbreviata = ""
				
				ls_indirizzo_2 = ls_cap + " " + ls_localita + " " + "(" + ls_provincia + ")"
				
				if ls_indirizzo_diverso<>"" then
					ls_indirizzo = ls_indirizzo_diverso
					ls_indirizzo_2 = ls_indirizzo_diverso_2
				end if
				
				select des_prodotto
				into   :ls_des_prodotto
				from   anag_prodotti
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					return -1
				end if
				
				select des_giro_consegna
				into   :ls_des_giro_consegna
				from   tes_giri_consegne
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_giro_consegna=:ls_cod_giro_consegna;
				
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					return -1
				end if
				
				select dim_x,
						 dim_y,
						 dim_z,
						 dim_t
				into   :ld_dim_x,
						 :ld_dim_y,
						 :ld_dim_z,
						 :ld_dim_t
				from   comp_det_ord_ven
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    anno_registrazione=:li_anno_registrazione
				and    num_registrazione=:ll_num_registrazione
				and    prog_riga_ord_ven=:ll_prog_riga_ord_ven;
				
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					return -1
				end if
				
				select des_prodotto
				into   :ls_riferimento
				from   det_ord_ven
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    anno_registrazione=:li_anno_registrazione
				and    num_registrazione=:ll_num_registrazione
				and    num_riga_appartenenza=:ll_prog_riga_ord_ven
				and    cod_tipo_det_ven='RIF';
				
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					return -1
				end if
				
				ls_dimensioni = string(round(ld_dim_x,0)) + " - " + string(round(ld_dim_y,0)) + " - " + string(round(ld_dim_z,0)) + " - " + string(round(ld_dim_t,0))
			
				if ld_quan_ordine >0 then
					ls_quan_ordine = string(int(ld_quan_ordine))
				else
					ls_quan_ordine = ""
				end if
				
				select num_copie_etichetta
				into   :ll_num_copie_sole
				from   tab_flags_configuratore
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_modello=:ls_cod_prodotto;
				
				if sqlca.sqlcode< 0 then
					messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					return -1
				end if
				
				for ll_z = 1 to ld_quan_ordine
				
					li_risposta = luo_stampa_etichette.uof_prepara_etichetta("TESTATA_TENDE_SOLE")
					if li_risposta < 0 then 
						messagebox("Apice","Errore in fase di preparazione etichetta!",stopsign!)
						return -1
					else
						li_numero_etichetta = li_risposta
					end if

					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_ORDINE",string(ll_num_registrazione))
//					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_RIGHE",string(ll_num_riga_corrente)+"/"+string(ll_num_righe_effettive))
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_RIGHE",string(ll_prog_riga_ord_ven))
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"DATA_CONSEGNA",string(ldt_data_consegna,"dd/mm/yy"))				
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"GIRO_CONSEGNA",ls_des_giro_consegna)				
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RAGIONE_SOCIALE",left(ls_rag_soc_1,30))
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RAG_SOC_2",right(ls_rag_soc_1,len(ls_rag_soc_1)-30))
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"INDIRIZZO",ls_indirizzo)
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"IND_2",ls_indirizzo_2)
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RIFERIMENTO",ls_riferimento)
					ll_quan_ordine = ld_quan_ordine
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"QUANTITA",string(ll_z)+"/"+string(ll_quan_ordine))
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"DESCRIZIONE_PRODOTTO",ls_des_prodotto)
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"DIMENSIONI",ls_dimensioni)												
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_COPIE",string(ll_num_copie_sole,"0000"))																
			//  	aggiunte su richiesta di Valerio 4/2/2004
					if not isnull(ls_rif_interscambio) and len(ls_rif_interscambio) > 0 and ls_cod_cliente = "0001" then
						ls_num_ord_cliente = ls_rif_interscambio
					end if
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_ORD_CLIENTE",ls_num_ord_cliente)												
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RIF_INTERSCAMBIO",ls_rif_interscambio)												
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NOTA_PRODOTTO",ls_nota_prodotto)												
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RAG_SOC_ABBR",ls_rag_soc_abbreviata)												
					if ll_z < ll_quan_ordine then
						li_risposta = luo_stampa_etichette.uof_prepara_stampa(li_numero_etichetta)
					end if
				next
				
				if ls_flag_calcola_dt = "S" then
				
					select flag_non_calcolare_dt
					into   :ls_flag_non_calcolare_dt
					from   distinta_padri
					where  cod_azienda=:s_cs_xx.cod_azienda
					and    cod_prodotto=:ls_cod_prodotto
					and    cod_versione=:ls_cod_versione;
					
					if sqlca.sqlcode< 0 then
						messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
						return -1
					end if
					
					if ls_flag_non_calcolare_dt = 'N' or isnull(ls_flag_non_calcolare_dt) then
						select count(*)
						into   :ll_num_taglio
						from   distinte_taglio_calcolate
						where  cod_azienda=:s_cs_xx.cod_azienda
						and    anno_registrazione=:li_anno_registrazione
						and    num_registrazione=:ll_num_registrazione
						and    prog_riga_ord_ven=:ll_prog_riga_ord_ven;
						
						if sqlca.sqlcode < 0 then 
							messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
							return -1
						end if
					
						if ll_num_taglio = 0 then
							li_risposta = luo_stampa_etichette.uof_taglio_carta(li_numero_etichetta,"T")							
						else
							li_risposta = luo_stampa_etichette.uof_taglio_carta(li_numero_etichetta,"A")							
						end if
						
					else
						li_risposta = luo_stampa_etichette.uof_taglio_carta(li_numero_etichetta,"A")							
					end if
					
				else
					li_risposta = luo_stampa_etichette.uof_taglio_carta(li_numero_etichetta,"T")
				end if
				
//				if ll_num_righe_effettive = ll_num_riga_corrente then
//					li_risposta = luo_stampa_etichette.uof_taglio_carta(li_numero_etichetta)
//				end if
//				
				li_risposta = luo_stampa_etichette.uof_prepara_stampa(li_numero_etichetta)
					
				
				// *** FINE stampa etichette di testata tende da sole
				
				
				// *** Stampa etichette per distinta di taglio tende da sole
				
			   if ls_flag_calcola_dt = "S" then
					
					ll_num_taglio=0
				
					if ls_flag_non_calcolare_dt = 'N' or isnull(ls_flag_non_calcolare_dt) then
									
						select count(*)
						into   :ll_num_taglio
						from   distinte_taglio_calcolate
						where  cod_azienda=:s_cs_xx.cod_azienda
						and    anno_registrazione=:li_anno_registrazione
						and    num_registrazione=:ll_num_registrazione
						and    prog_riga_ord_ven=:ll_prog_riga_ord_ven;
						
						if sqlca.sqlcode < 0 then 
							messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
							return -1
						end if
					
						
						if ll_num_taglio > 0 then
							declare r_distinte_calcolate cursor for
							select  cod_prodotto_padre,
									  num_sequenza,
									  cod_prodotto_figlio,
									  cod_versione,
									  progressivo,
									  prog_calcolo,
									  quantita,
									  misura
							from    distinte_taglio_calcolate
							where   cod_azienda=:s_cs_xx.cod_azienda
							and     anno_registrazione=:li_anno_registrazione
							and     num_registrazione=:ll_num_registrazione
							and     prog_riga_ord_ven=:ll_prog_riga_ord_ven;
							
							open   r_distinte_calcolate;
							if sqlca.sqlcode < 0 then 
								messagebox("Apice","Errore in OPEN cursore r_distinte_calcolate: " + sqlca.sqlerrtext,stopsign!)
								rollback;
								return -1
							end if
							
							ll_i = 0
							
							do while true
								fetch r_distinte_calcolate
								into  :ls_cod_prodotto_padre,
										:ll_num_sequenza,
										:ls_cod_prodotto_figlio,
										:ls_cod_versione,
										:ll_progressivo,
										:ll_prog_calcolo,
										:ld_quantita,
										:ld_misura;
									
								if sqlca.sqlcode < 0 then 
									messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
									close   r_distinte_calcolate;
									rollback;
									return -1
								end if
							
								if sqlca.sqlcode = 100 then exit
								
								ll_i++
								
								ld_quantita = round(ld_quantita,2)
								ld_misura = round(ld_misura,2)
								
								select des_distinta_taglio,
										 um_quantita,
										 um_misura
								into   :ls_des_distinta_taglio,
										 :ls_b_um_quantita,
										 :ls_b_um_misura
								from   tab_distinte_taglio
								where  cod_azienda=:s_cs_xx.cod_azienda
								and    cod_prodotto_padre=:ls_cod_prodotto_padre
								and    num_sequenza=:ll_num_sequenza
								and    cod_prodotto_figlio=:ls_cod_prodotto_figlio
								and    cod_versione=:ls_cod_versione
								and    progressivo=:ll_progressivo;
										 
								if sqlca.sqlcode < 0 then 
									messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
									close   r_distinte_calcolate;
									rollback;
									return -1
								end if
								
								if ls_b_um_quantita = "" then setnull(ls_b_um_quantita)
								if ls_b_um_misura = "" then setnull(ls_b_um_misura)
								ls_um_quantita = ls_b_um_quantita
								ls_um_misura = ls_b_um_misura
										 
								select cod_prodotto
								into   :ls_cod_prodotto_variante
								from   varianti_det_ord_ven
								where  cod_azienda=:s_cs_xx.cod_azienda
								and    anno_registrazione=:li_anno_registrazione
								and    num_registrazione=:ll_num_registrazione
								and    prog_riga_ord_ven=:ll_prog_riga_ord_ven
								and    cod_prodotto_padre=:ls_cod_prodotto_padre
								and    num_sequenza=:ll_num_sequenza
								and    cod_prodotto_figlio=:ls_cod_prodotto_figlio
								and    cod_versione=:ls_cod_versione;
								
								if sqlca.sqlcode < 0 then 
									messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
									close   r_distinte_calcolate;
									rollback;
									return -1
								end if
								
								if not isnull(ls_cod_prodotto_variante) and ls_cod_prodotto_variante<>"" then
									ls_cod_prodotto_figlio = ls_cod_prodotto_variante
								end if
								
								select des_prodotto
								into   :ls_des_prodotto_figlio
								from   anag_prodotti
								where  cod_azienda=:s_cs_xx.cod_azienda
								and    cod_prodotto=:ls_cod_prodotto_figlio;
								
								if sqlca.sqlcode < 0 then 
									messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
									close   r_distinte_calcolate;
									rollback;
									return -1
								end if
								
				
								li_risposta = luo_stampa_etichette.uof_prepara_etichetta("DISTINTA_TAGLIO")
								
								if li_risposta < 0 then 
									messagebox("Apice","Errore in fase di preparazione etichetta!",stopsign!)
									rollback;
									return -1
								else
									li_numero_etichetta = li_risposta
								end if
								
								ld_misura= round(ld_misura,1)
								ld_quantita = round(ld_quantita/ld_quan_ordine,1)
								
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_ORDINE",string(ll_num_registrazione))
				//	li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_RIGHE",string(ll_num_riga_corrente)+"/"+string(ll_num_righe_effettive))
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_RIGHE",string(ll_prog_riga_ord_ven))
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RAGIONE_SOCIALE",left(ls_rag_soc_1,30))
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RAG_SOC_2",right(ls_rag_soc_1,len(ls_rag_soc_1)-30))				
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RIFERIMENTO",ls_riferimento)												
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"COD_PRODOTTO_FIGLIO",ls_cod_prodotto_figlio)												
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"DES_PRODOTTO_FIGLIO",ls_des_prodotto_figlio)												
	
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"QUANTITA",string(ld_quantita,"###0.0"))												
								//*****************   RICORDARSI DI DIVIDERE ld_quantita PER LA quan_ordine   ****************************************
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"MISURA",string(ld_misura,"###0.0"))												
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"UM_QUANTITA",ls_um_quantita)												
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"UM_MISURA",ls_um_misura)												
								
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"DESCRIZIONE_PRODOTTO",ls_des_prodotto)												
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"DIMENSIONI",ls_dimensioni)

				// aggiunto su richiesta di Valerio 4/2/2004				
								if not isnull(ls_rif_interscambio) and len(ls_rif_interscambio) > 0 and ls_cod_cliente = "0001" then
									ls_num_ord_cliente = ls_rif_interscambio
								end if
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_ORD_CLIENTE",ls_num_ord_cliente)												
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RIF_INTERSCAMBIO",ls_rif_interscambio)												
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NOTA_PRODOTTO",ls_nota_prodotto)												
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RAG_SOC_ABBR",ls_rag_soc_abbreviata)												
				// ---- fine aggiunta 4/2/2004
								li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_COPIE",string(ld_quan_ordine,"0000"))																
								
								if ll_i = ll_num_taglio then
									li_risposta = luo_stampa_etichette.uof_taglio_carta(li_numero_etichetta,"T")	
								else
									li_risposta = luo_stampa_etichette.uof_taglio_carta(li_numero_etichetta,"N")							
								end if
								
								li_risposta = luo_stampa_etichette.uof_prepara_stampa(li_numero_etichetta)
								
								// qui ci va il comando di stampa per le etichette per le distinte di taglio
								// saranno stampate tante copie quanto è la quan_ordine
							
								setnull(ls_cod_prodotto_variante)
							loop
						
							close   r_distinte_calcolate;
						
						end if
					end if
				end if
	
				
								
				
				// *** FINE stampa etichette per distinta di taglio tende da sole
			loop
			
			close righe_det_ord_ven_1;
			
			setnull(ls_cod_prodotto)
			setnull(ls_cod_tipo_det_ven)
			setnull(ll_prog_riga_ord_ven)
			setnull(ls_cod_versione)
			ll_num_righe_effettive = 0
			setnull(ls_cod_cliente)
			setnull(ls_cod_giro_consegna)
			setnull(ls_destinazione_diversa)
			setnull(ls_rag_soc_1)
			setnull(ls_indirizzo)
			setnull(ls_des_prodotto)
			setnull(ls_des_giro_consegna)
			ld_dim_x = 0
			ld_dim_y=0
			ld_dim_z=0
			ld_dim_t=0
			setnull(ls_riferimento)
		   ll_num_riga_corrente = 0
//************** Fine Case per report_1
			
//************** Inizio Case Report_3 
		case 'C'
			
			select count(*)
			into   :ll_num_tot_tecniche
			from    det_ord_ven
			where   cod_azienda=:s_cs_xx.cod_azienda
			and     anno_registrazione=:li_anno_registrazione
			and     num_registrazione=:ll_num_registrazione
			and     num_riga_appartenenza=0
			and     flag_blocco='N'
			and     cod_tipo_det_ven <> 'ADD';
			
			if sqlca.sqlcode < 0 then 
				messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
				return -1
			end if
			
			ll_conteggio = 0
			
			declare righe_det_ord_ven_2 cursor for
			select  cod_prodotto,
					  cod_tipo_det_ven,
					  prog_riga_ord_ven,
					  cod_versione,
					  quan_ordine,
					  nota_prodotto
			from    det_ord_ven
			where   cod_azienda=:s_cs_xx.cod_azienda
			and     anno_registrazione=:li_anno_registrazione
			and     num_registrazione=:ll_num_registrazione
			and     num_riga_appartenenza=0
			and     flag_blocco='N'
			and     cod_tipo_det_ven <> 'ADD';
			
			open righe_det_ord_ven_2;
			
			do while true															//  scorre tutti i dettagli dell'ordine
				fetch righe_det_ord_ven_2
				into  :ls_cod_prodotto,
						:ls_cod_tipo_det_ven,
						:ll_prog_riga_ord_ven,
						:ls_cod_versione,
						:ld_quan_ordine,
						:ls_nota_prodotto;
						
				if sqlca.sqlcode = 100 then exit
				
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Caso Report_2 errore in fase di lettura cursore su det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					close righe_det_ord_ven_2;
					rollback;
					return -1
				end if
									
				// *******************************************
				// *** INIZIO STAMPA ETICHETTE TENDE TECNICHE
				
				if isnull(ls_nota_prodotto) then ls_nota_prodotto = ""
				ll_conteggio++
			
				select cod_cliente,
						 cod_giro_consegna,
						 indirizzo,
						 localita,
						 cap,
						 provincia,
						 num_ord_cliente,
						 rif_interscambio
				into   :ls_cod_cliente,
						 :ls_cod_giro_consegna,
						 :ls_indirizzo_diverso,
						 :ls_localita_diversa,
						 :ls_cap_diverso,
						 :ls_provincia_diversa,
						 :ls_num_ord_cliente,
						 :ls_rif_interscambio
				from   tes_ord_ven
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    anno_registrazione=:li_anno_registrazione
				and    num_registrazione=:ll_num_registrazione;

				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					return -1
				end if
				
				if isnull(ls_indirizzo_diverso) then ls_indirizzo_diverso = ""
				if isnull(ls_localita_diversa) then ls_localita_diversa = ""
				if isnull(ls_cap_diverso) then ls_cap_diverso = ""
				if isnull(ls_provincia_diversa) then ls_provincia_diversa = ""
				if isnull(ls_num_ord_cliente) then ls_num_ord_cliente = ""
				if isnull(ls_rif_interscambio) then ls_rif_interscambio = ""
				
				ls_indirizzo_diverso_2 = ls_cap_diverso + " " + ls_localita_diversa + " (" + ls_provincia_diversa  + ")"
				
				select rag_soc_1,
						 indirizzo,
						 localita,
						 cap,
						 provincia,
						 rag_soc_abbreviata
				into   :ls_rag_soc_1,
						 :ls_indirizzo,
						 :ls_localita,
						 :ls_cap,
						 :ls_provincia,
						 :ls_rag_soc_abbreviata
				from   anag_clienti
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_cliente=:ls_cod_cliente;
				
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					return -1
				end if
				
				if isnull(ls_indirizzo) then ls_indirizzo = ""
				if isnull(ls_localita) then ls_localita = ""
				if isnull(ls_cap) then ls_cap = ""
				if isnull(ls_provincia) then ls_provincia = ""
				if isnull(ls_rag_soc_abbreviata) then ls_rag_soc_abbreviata = ""
				
				ls_indirizzo_2 = ls_cap + " " + ls_localita + " " + "(" + ls_provincia + ")"
				
				if ls_indirizzo_diverso<>"" then
					ls_indirizzo=ls_indirizzo_diverso
					ls_indirizzo_2=ls_indirizzo_diverso_2
				end if
				
				select des_prodotto
				into   :ls_des_prodotto
				from   anag_prodotti
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto;
				
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					return -1
				end if
				
				select des_giro_consegna
				into   :ls_des_giro_consegna
				from   tes_giri_consegne
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_giro_consegna=:ls_cod_giro_consegna;
				
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					return -1
				end if
				
				select dim_x,
						 dim_y,
						 dim_z,
						 dim_t
				into   :ld_dim_x,
						 :ld_dim_y,
						 :ld_dim_z,
						 :ld_dim_t
				from   comp_det_ord_ven
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    anno_registrazione=:li_anno_registrazione
				and    num_registrazione=:ll_num_registrazione
				and    prog_riga_ord_ven=:ll_prog_riga_ord_ven;
				
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
					return -1
				end if
				
				select des_prodotto
				into   :ls_riferimento
				from   det_ord_ven
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    anno_registrazione=:li_anno_registrazione
				and    num_registrazione=:ll_num_registrazione
				and    num_riga_appartenenza=:ll_prog_riga_ord_ven
				and    cod_tipo_det_ven='RIF';
				
				if sqlca.sqlcode < 0 then 
					messagebox("Apice","Errore in ricerca riga RIF~r~n " + sqlca.sqlerrtext,stopsign!)
					return -1
				end if
				
				// modificato su richiesta di Valerio.
				ls_dimensioni = string(round(ld_dim_x,1)) + " - " + string(round(ld_dim_y,1)) + " - " + string(round(ld_dim_z,1))
			
				select num_copie_etichetta
				into   :ll_num_copie_tecniche
				from   tab_flags_configuratore
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_modello=:ls_cod_prodotto;
				
				if sqlca.sqlcode< 0 then
					messagebox("Apice","Errore in lettura numero copie etichetta da parametri configuratore~r~n " + sqlca.sqlerrtext,stopsign!)
					return -1
				end if
				
				for ll_z = 1 to ld_quan_ordine
				
					li_risposta = luo_stampa_etichette.uof_prepara_etichetta("TENDE_TECNICHE")
					
					if li_risposta < 0 then 
						messagebox("Apice","Errore in fase di preparazione etichetta!",stopsign!)
						return -1
					else
						li_numero_etichetta = li_risposta
					end if
				
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_ORDINE",string(ll_num_registrazione))
//					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_RIGHE",string(ll_conteggio)+"/"+string(ll_num_tot_tecniche))
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_RIGHE",string(ll_prog_riga_ord_ven))
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"DATA_CONSEGNA",string(ldt_data_consegna,"dd/mm/yy"))				
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"GIRO_CONSEGNA",ls_des_giro_consegna)				
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RAGIONE_SOCIALE",left(ls_rag_soc_1,30))
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RAG_SOC_2",right(ls_rag_soc_1,len(ls_rag_soc_1)-30))
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"INDIRIZZO",ls_indirizzo)
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"IND_2",ls_indirizzo_2)
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RIFERIMENTO",ls_riferimento)
					ll_quan_ordine = ld_quan_ordine
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"QUANTITA",string(ll_z)+"/"+string(ll_quan_ordine))												
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"DESCRIZIONE_PRODOTTO",ls_des_prodotto)												
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"DIMENSIONI",ls_dimensioni)	
				
// aggiunto su richiesta di Valerio 4/2/2004				
					if not isnull(ls_rif_interscambio) and len(ls_rif_interscambio) > 0 and ls_cod_cliente = "0001" then
						ls_num_ord_cliente = ls_rif_interscambio
					end if
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_ORD_CLIENTE",ls_num_ord_cliente)												
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RIF_INTERSCAMBIO",ls_rif_interscambio)												
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NOTA_PRODOTTO",ls_nota_prodotto)												
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RAG_SOC_ABBR",ls_rag_soc_abbreviata)												
				// ---- fine aggiunta 4/2/2004
				
					li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_COPIE",string(ll_num_copie_tecniche,"0000"))																

					if ll_z < ll_quan_ordine then
						li_risposta = luo_stampa_etichette.uof_prepara_stampa(li_numero_etichetta)
					end if

				next

				if ll_conteggio = ll_num_tot_tecniche then
					li_risposta = luo_stampa_etichette.uof_taglio_carta(li_numero_etichetta,"T")					
				else
					li_risposta = luo_stampa_etichette.uof_taglio_carta(li_numero_etichetta,"A")
				end if
			
				li_risposta = luo_stampa_etichette.uof_prepara_stampa(li_numero_etichetta)
		
				// *** FINE STAMPA ETICHETTE TENDE TECNICHE
				//*******************************************
				
				
			loop
			
			close righe_det_ord_ven_2;
			
//************** Fine Case Report_3

		
			
	end choose
next

li_risposta = luo_stampa_etichette.uof_stampa_etichette()

destroy (luo_stampa_etichette)

rollback;

setpointer(arrow!)

return 0
end function

public function integer f_etichetta_gen ();long				ll_num_registrazione,ll_riga,ll_selected[],ll_prog_riga_ord_ven,ll_num_reparti,ll_prog_riga_comanda,job, & 
					ll_num_tenda, ll_num_ordini, ll_tot_ordini,ll_num_copie_sole,ll_num_copie_tecniche,ll_num_copie_sfuso,ll_t, &
					ll_num_righe_effettive,ll_num_riga_corrente,ll_num_sequenza,ll_progressivo,ll_prog_calcolo,ll_num_taglio,ll_i,&
					ll_num_tot_tecniche,ll_conteggio, ll_num_copie_etichetta, ll_etichetta
					
integer			li_anno_registrazione,li_risposta,li_numero_etichetta

string			ls_cod_prodotto,ls_cod_tipo_det_ven,ls_test,ls_cod_versione,ls_cod_reparto_test, ls_test_2,& 
					ls_cod_tipo_ord_ven,ls_errore,ls_cod_reparto,ls_flag_stampa_fase,ls_cod_reparti_trovati[],ls_flag_non_calcolare_dt, &
					ls_str, ls_cod_operatore, ls_sql,ls_rag_soc_1,ls_riferimento,ls_giro_consegna,ls_indirizzo,&
					ls_destinazione_diversa,ls_des_prodotto,ls_dimensioni,ls_cod_cliente,ls_cod_giro_consegna,ls_des_giro_consegna,&
					ls_cod_prodotto_figlio,ls_cod_prodotto_variante,ls_um_quantita,ls_b_um_quantita,ls_um_misura,ls_b_um_misura,&
					ls_cod_prodotto_padre,ls_des_distinta_taglio,ls_des_prodotto_figlio,ls_quan_ordine,ls_localita,ls_cap,ls_provincia, &
					ls_indirizzo_2,ls_indirizzo_diverso,ls_localita_diversa,ls_cap_diverso,ls_provincia_diversa,ls_indirizzo_diverso_2, &
					ls_num_ord_cliente, ls_rif_interscambio, ls_rag_soc_abbreviata, ls_nota_prodotto_det
					
datetime		ldt_data_consegna,ldt_data_consegna_riga

decimal{4}		ld_quan_ordine,ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t,ld_quantita,ld_misura


ll_num_copie_etichetta = long(em_quan_etichetta_gen.text)
if ll_num_copie_etichetta < 1 then
	messagebox("APICE","Attenzione!! E' necessario impostare un numero nella campo quantità etichette",stopsign!)
	return -1
end if

setpointer(hourglass!)

uo_stampa_etichette luo_stampa_etichette

luo_stampa_etichette = CREATE uo_stampa_etichette

dw_lista_ord_ven_stampa_prod.get_selected_rows(ll_selected[])

for ll_riga = 1 to upperbound(ll_selected[])
	
	for ll_etichetta = 1 to ll_num_copie_etichetta
		ll_num_registrazione =dw_lista_ord_ven_stampa_prod.getitemnumber(ll_selected[ll_riga],"num_registrazione")
		li_anno_registrazione =dw_lista_ord_ven_stampa_prod.getitemnumber(ll_selected[ll_riga],"anno_registrazione")
		ls_cod_tipo_ord_ven = dw_lista_ord_ven_stampa_prod.getitemstring(ll_selected[ll_riga],"tipo_ord_ven")
		ldt_data_consegna = dw_lista_ord_ven_stampa_prod.getitemdatetime(ll_selected[ll_riga],"data_consegna")
		
		ll_num_tenda = 0
		ll_num_riga_corrente = 0
		
	
			// *** stampa etichetta 
			
		select count(*) 
		into   :ll_num_righe_effettive
		from   det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:li_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    num_riga_appartenenza=0
		and    flag_blocco = 'N';
		
		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		ll_num_riga_corrente++
		
		select cod_cliente,
				 cod_giro_consegna,
				 indirizzo,
				 localita,
				 cap,
				 provincia,
				 num_ord_cliente,
				 rif_interscambio
		into   :ls_cod_cliente,
				 :ls_cod_giro_consegna,
				 :ls_indirizzo_diverso,
				 :ls_localita_diversa,
				 :ls_cap_diverso,
				 :ls_provincia_diversa,
				 :ls_num_ord_cliente,
				 :ls_rif_interscambio
		from   tes_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:li_anno_registrazione
		and    num_registrazione=:ll_num_registrazione;

		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		if isnull(ls_indirizzo_diverso) then ls_indirizzo_diverso = ""
		if isnull(ls_localita_diversa) then ls_localita_diversa = ""
		if isnull(ls_cap_diverso) then ls_cap_diverso = ""
		if isnull(ls_provincia_diversa) then ls_provincia_diversa = ""
		
		ls_indirizzo_diverso_2 = ls_cap_diverso + " " + ls_localita_diversa + " (" + ls_provincia_diversa  + ")"
		
		select rag_soc_1,
				 indirizzo,
				 localita,
				 cap,
				 provincia,
				 rag_soc_abbreviata
		into   :ls_rag_soc_1,
				 :ls_indirizzo,
				 :ls_localita,
				 :ls_cap,
				 :ls_provincia,
				 :ls_rag_soc_abbreviata
		from   anag_clienti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_cliente=:ls_cod_cliente;
		
		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		if isnull(ls_indirizzo) then ls_indirizzo = ""
		if isnull(ls_localita) then ls_localita = ""
		if isnull(ls_cap) then ls_cap = ""
		if isnull(ls_provincia) then ls_provincia = ""
		
		ls_indirizzo_2 = ls_cap + " " + ls_localita + " " + "(" + ls_provincia + ")"
		
		if ls_indirizzo_diverso <> "" then
			ls_indirizzo = ls_indirizzo_diverso
			ls_indirizzo_2 = ls_indirizzo_diverso_2
		end if
		
		select des_prodotto
		into   :ls_des_prodotto
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		select des_giro_consegna
		into   :ls_des_giro_consegna
		from   tes_giri_consegne
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_giro_consegna=:ls_cod_giro_consegna;
		
		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		select dim_x,
				 dim_y,
				 dim_z,
				 dim_t
		into   :ld_dim_x,
				 :ld_dim_y,
				 :ld_dim_z,
				 :ld_dim_t
		from   comp_det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:li_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    prog_riga_ord_ven=:ll_prog_riga_ord_ven;
		
		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		select des_prodotto,
				 nota_prodotto
		into   :ls_riferimento,
				 :ls_nota_prodotto_det
		from   det_ord_ven
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    anno_registrazione=:li_anno_registrazione
		and    num_registrazione=:ll_num_registrazione
		and    num_riga_appartenenza=:ll_prog_riga_ord_ven
		and    cod_tipo_det_ven='RIF';
		
		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
		
		ls_dimensioni = string(round(ld_dim_x,0)) + " - " + string(round(ld_dim_y,0)) + " - " + string(round(ld_dim_z,0)) + " - " + string(round(ld_dim_t,0))
	
		li_risposta = luo_stampa_etichette.uof_prepara_etichetta("ETICHETTA_GENERALE")
		
		if li_risposta < 0 then 
			messagebox("Apice","In fase di preparazione etichetta!",stopsign!)
			return -1
		else
			li_numero_etichetta = li_risposta
		end if
		
		if ld_quan_ordine > 0 then
			ls_quan_ordine = string(int(ld_quan_ordine))
		else
			ls_quan_ordine = ""
		end if
		
		
		ll_num_copie_sole = 1
		
		if not isnull(ls_rif_interscambio) and len(ls_rif_interscambio) > 0 and ls_cod_cliente = "0001" then
			ls_num_ord_cliente = ls_rif_interscambio
		end if
		li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_ORDINE",string(ll_num_registrazione))
		li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_RIGHE",string(ll_etichetta) + "/" + string(ll_num_copie_etichetta))
		li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"DATA_CONSEGNA",string(ldt_data_consegna,"dd/mm/yy"))				
		li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"GIRO_CONSEGNA",ls_des_giro_consegna)				
		li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RAGIONE_SOCIALE_35",left(ls_rag_soc_1,35))
		li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RAGIONE_SOCIALE",ls_rag_soc_1)
		li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RAGIONE_SOC_ABBR",ls_rag_soc_abbreviata)
//			li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RAG_SOC_2",right(ls_rag_soc_1,len(ls_rag_soc_1)-30))
		li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"INDIRIZZO",ls_indirizzo)
		li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"IND_2",ls_indirizzo_2)
//			li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RIFERIMENTO",ls_riferimento)												
		li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RIF_VS_ORDINE",ls_num_ord_cliente)												
		li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"RIF_INTERSCAMBIO",ls_rif_interscambio)												
		li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NOTA_PRODOTTO",ls_nota_prodotto_det)												
//			li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"QUANTITA","1")												
//			li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"DESCRIZIONE_PRODOTTO",ls_des_prodotto)												
//			li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"DIMENSIONI",ls_dimensioni)												
		li_risposta = luo_stampa_etichette.uof_aggiungi_valore(li_numero_etichetta,"NUM_COPIE",string(ll_num_copie_sole,"0000"))																
		
	
		if ll_etichetta < ll_num_copie_etichetta then
			// aggiunto per gestione del taglio ad ogni ordine
			li_risposta = luo_stampa_etichette.uof_taglio_carta(li_numero_etichetta,"A")	
		else
			// taglio ad ogni ordine completo
			li_risposta = luo_stampa_etichette.uof_taglio_carta(li_numero_etichetta,"T")	
		end if

		li_risposta = luo_stampa_etichette.uof_prepara_stampa(li_numero_etichetta)
		
		
//		close righe_det_ord_ven_1;
		
		setnull(ls_cod_prodotto)
		setnull(ls_cod_tipo_det_ven)
		setnull(ll_prog_riga_ord_ven)
		setnull(ls_cod_versione)
		ll_num_righe_effettive = 0
		setnull(ls_cod_cliente)
		setnull(ls_cod_giro_consegna)
		setnull(ls_destinazione_diversa)
		setnull(ls_rag_soc_1)
		setnull(ls_indirizzo)
		setnull(ls_des_prodotto)
		setnull(ls_des_giro_consegna)
		ld_dim_x = 0
		ld_dim_y=0
		ld_dim_z=0
		ld_dim_t=0
		setnull(ls_riferimento)
		ll_num_riga_corrente = 0
	next
	
next

li_risposta = luo_stampa_etichette.uof_stampa_etichette()

destroy (luo_stampa_etichette)

rollback;

setpointer(arrow!)

return 0
end function

public function integer wf_get_stabilimento_operatore ();boolean lb_fsu
string ls_errore, ls_des_stabilimento
integer li_ret

// Filtra stabilimento utente
guo_functions.uof_get_parametro_azienda( "FSU", lb_fsu)

if s_cs_xx.cod_utente = "CS_SYSTEM" or lb_fsu then
	//metto la stringa "_TUTTI_" in is_cod_deposito_operatore
	is_cod_deposito_operatore = is_cod_deposito_CSSYSTEM
else
	li_ret = guo_functions.uof_get_stabilimento_utente(is_cod_deposito_operatore, ls_errore)
	
	if li_ret<0 then
		setnull(is_cod_deposito_operatore)
		g_mb.error("Attenzione!", ls_errore)
		return -1
		
	end if
end if

if is_cod_deposito_operatore = is_cod_deposito_CSSYSTEM then
	ls_des_stabilimento = "TUTTI"
	
elseif isnull(is_cod_deposito_operatore) then
	ls_des_stabilimento = "NESSUNO"
	
else
	ls_des_stabilimento = f_des_tabella("anag_depositi","cod_deposito ='" +  is_cod_deposito_operatore +"'", "des_deposito")
	ls_des_stabilimento = is_cod_deposito_operatore + " - " + ls_des_stabilimento
end if

st_stab_operatore.text = "Stabilimento Operatore: " + ls_des_stabilimento + wf_get_printer_name()

return 0
end function

public function boolean wf_controlla_reparto (string fs_cod_reparto);//torna TRUE se il reparto appartiene allo stabilimento dell'operatore, altrimenti FALSE
//se CS_SYSTEM torna sempre TRUE

if is_cod_deposito_operatore=is_cod_deposito_CSSYSTEM then return true

if fs_cod_reparto="" or isnull(fs_cod_reparto) then return false
	
if f_des_tabella("anag_reparti","cod_reparto ='" +  fs_cod_reparto +"'", "cod_deposito")=is_cod_deposito_operatore then
	//il reparto appartiene allo stabilimento dell'operatore
	return true
end if

return false
end function

public function integer wf_stampa_ordini (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_tipo_ord_ven, datetime fdt_data_consegna, ref string fs_errore);integer			li_zoom=100

long				ll_riga, ll_prog_riga_ord_ven, ll_num_reparti, ll_prog_riga_comanda, job, ll_num_riga_appartenenza, & 
					ll_num_tenda, ll_num_ordini, ll_tot_ordini, ll_num_copie_sole, ll_num_copie_tecniche, ll_num_copie_sfuso,ll_t
					
integer			li_risposta

string			ls_cod_prodotto, ls_cod_tipo_det_ven, ls_test, ls_cod_versione, ls_cod_reparto_test, ls_tipo_stampa, ls_test_2,& 
					ls_errore, ls_cod_reparto, ls_flag_stampa_fase, ls_cod_reparti_trovati[], ls_flag_non_calcolare_dt, ls_cod_tipo_det_ven_addizionali, &
					ls_str, ls_cod_operatore, ls_flag_calcola_dt, ls_sql, ls_cgibus, ls_document_name, ls_vuoto[], ls_reparti_letti[], ls_flag_ordinamento_dt, &
					ls_cod_semilavorato[], ls_cod_deposito_fase[], ls_flag_stampa_foglio
uo_funzioni_1 luo_funzioni
uo_produzione luo_prod

datetime		ldt_data_pronto

ls_flag_calcola_dt = dw_ext_selezione.getitemstring(1,"flag_calcola_dt")
ls_flag_ordinamento_dt = dw_ext_selezione.getitemstring(1,"flag_ordinamento_dt")
if isnull(ls_flag_ordinamento_dt) then ls_flag_ordinamento_dt = "S"

select flag_tipo_bcl
into   :ls_tipo_stampa
from   tab_tipi_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_tipo_ord_ven=:fs_cod_tipo_ord_ven;

if sqlca.sqlcode < 0 then 
	fs_errore = "Errore sul DB: " + sqlca.sqlerrtext
	return -1
end if

ll_num_tenda = 0

choose case ls_tipo_stampa			// verifico il tipo di report 'A'=Report_1, 'B'=Report_2, 'C'=Report_3

//************** Inizio Case per report_1
	case 'A'			
		
		declare  righe_det_ord_ven_1 cursor for
		select   cod_prodotto,
					cod_tipo_det_ven,
					prog_riga_ord_ven,
					cod_versione,
					num_riga_appartenenza
		from     	det_ord_ven
		where    cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:fi_anno_registrazione and
					num_registrazione=:fl_num_registrazione and
					flag_blocco='N'
		order by prog_riga_ord_ven;
		
		open righe_det_ord_ven_1;
		if sqlca.sqlcode < 0 then 
			fs_errore = "Caso Report_1, OPEN cursore su det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext
			return -1
		end if
		
		do while 1 = 1															//  scorre tutti i dettagli dell'ordine
			fetch righe_det_ord_ven_1
			into  :ls_cod_prodotto,
					:ls_cod_tipo_det_ven,
					:ll_prog_riga_ord_ven,
					:ls_cod_versione,
					:ll_num_riga_appartenenza;
					
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode < 0 then 
				fs_errore = "Caso Report_1, lettura cursore su det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext
				close righe_det_ord_ven_1;
				return -1
			end if
			
			ll_num_tenda++									

			f_null_array(ls_cod_reparti_trovati[])           //azzero contenuto array dei reparti per la funzione trova_reparti
			
			select cod_azienda
			into	:ls_test
			from   distinta_padri
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;			 //test se prodotto è un prodotto con distinta base
																		 //devo aprire un cursore poichè possono essercu più versioni
//			open test_padre;
			
//			fetch test_padre into :ls_test;
				
			if sqlca.sqlcode < 0 then 
				fs_errore = "Caso Report_1, lettura (per test) distinta_padri.Errore sul DB: " + sqlca.sqlerrtext
				close righe_det_ord_ven_1;
//				close test_padre;
				return -1
			end if
			
			if sqlca.sqlcode = 100 then	//se il prodotto non ha distinta base è un errore di compilazione 
													//dell'ordine poichè nei report_1 i prodotti che non sono optional 
													//o addizionali devono avere distinta base
				f_scrivi_log("Stampa ordini produzione -->>Caso Report_1. Nell'ordine anno:" + string(fi_anno_registrazione) + & 
								 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + ls_cod_prodotto + & 
								 " che non essendo ne optional ne addizionale DEVE avere la distinta base poichè la stampa di questo ordine per la produzione è di tipo Report 1. Verificare.")
				
//				close test_padre;
				continue //esce dall'ordine corrente e scansiona il prodotto successivo
				
			else
				// valido solo per le righe primarie
					if ll_num_riga_appartenenza = 0 then
					// se il prodotto ha DB allora gli 'N' reparti vengono trovati tramite una funzione
					// che carica is_cod_reparto[] con i reparti dalla distinta base 
					// per le fasi di lavoro con flag_stampa fase ='S'
					
					luo_funzioni = create uo_funzioni_1
					ls_cod_semilavorato[] = ls_vuoto[] 
					ls_cod_reparti_trovati[] = ls_vuoto[] 
					ls_cod_deposito_fase[] = ls_vuoto[] 
					li_risposta = luo_funzioni.uof_trova_reparti_e_semilavorati(	true, fi_anno_registrazione, fl_num_registrazione, & 
														 ll_prog_riga_ord_ven, ls_cod_prodotto, ls_cod_versione, ref ls_cod_reparti_trovati[], ref ls_cod_semilavorato[], ls_cod_deposito_fase[], ls_errore)
														 
					destroy luo_funzioni
	
					
					if li_risposta = -1 then
						f_scrivi_log("Stampa ordini produzione -->>Caso Report_1. Nell'ordine anno:" + string(fi_anno_registrazione) + & 
						 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + ls_cod_prodotto + & 
						 " che durante la scansione della distinta base/fasi lavoro per trovare i reparti ha provocato questo errore:" + & 
						 ls_errore + " Verificare.")
		
	//					close test_padre;
						continue //esce dalla riga ordine corrente e scansiona il prodotto successivo
	
					end if
					
					if upperbound(ls_cod_reparti_trovati[]) = 0 then		//nel caso non venga trovato alcun reparto scansionando la distinta
						SELECT cod_reparto,								//viene assegnato il reparto della fase di lavoro del  PF quando era 		
								 flag_stampa_fase							//un SL in un'altra distinta
						INTO   :ls_cod_reparto,
								 :ls_flag_stampa_fase
						FROM   tes_fasi_lavorazione
						WHERE  cod_azienda = :s_cs_xx.cod_azienda  
						AND    cod_prodotto = :ls_cod_prodotto;
					
						if sqlca.sqlcode=100 then
							f_scrivi_log("Stampa ordini produzione -->>Caso Report_1. Nell'ordine anno:" + string(fi_anno_registrazione) + & 
							 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + ls_cod_prodotto + & 
							 " che non ha una fase di lavoro con un reparto assegnato. Verificare.")
	//						close test_padre;
							continue
						end if
					  
						if ls_flag_stampa_fase = 'N' then
							f_scrivi_log("Stampa ordini produzione-->>Case Report_1. Nell'ordine anno:" + string(fi_anno_registrazione) + & 
							 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + ls_cod_prodotto + & 
							 " che ha una fase di lavoro con un reparto assegnato ma il flag_stampa_fase non è impostato. Verificare.")
							
	//						close test_padre;
							continue
							
						end if																						
					end if
					
	//				close test_padre;
					
					for ll_num_reparti = 1 to upperbound(ls_cod_reparti_trovati[])
						//Poichè nel caso del report_1 le righe in tabella tes_stampa_ordini coinciderebbero con quelle in det_stampa_ordini
						//si decide di non inseire alcuna riga in det_stampa_ordini (l'informazione sarebbe inutilmente ripetuta)
						
						//##########################################################
						li_risposta = wf_get_data_pronto(fi_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven, ls_cod_reparti_trovati[ll_num_reparti], ldt_data_pronto)
						if li_risposta<0 then
							ldt_data_pronto = fdt_data_consegna
						end if
						//##########################################################
						
						select flag_stampa_foglio 
						into	:ls_flag_stampa_foglio 
						from  tes_fasi_lavorazione
						where cod_azienda = :s_cs_xx.cod_azienda and
								cod_prodotto = :ls_cod_semilavorato[ll_num_reparti] and
								cod_reparto = :ls_cod_reparti_trovati[ll_num_reparti];
						if sqlca.sqlcode <> 0 then ls_flag_stampa_foglio = "S"
						if isnull(ls_flag_stampa_foglio) then ls_flag_stampa_foglio = "S"
	
						
						insert into tes_stampa_ordini
						(cod_azienda,
						 anno_registrazione,
						 num_registrazione,
						 cod_reparto,
						 data_consegna,
						 prog_riga_comanda,
						 num_tenda,
						 prog_sessione,
						 ordine,
						 flag_stampa_foglio)
						 values 
						 (:s_cs_xx.cod_azienda,
						  :fi_anno_registrazione,
						  :fl_num_registrazione,
						  :ls_cod_reparti_trovati[ll_num_reparti],
						  :ldt_data_pronto,
						  :ll_prog_riga_ord_ven,
						  :ll_num_tenda,
						  :il_sessione,
						  :ll_num_reparti,
						  :ls_flag_stampa_foglio);
	
						if sqlca.sqlcode < 0 then 
							fs_errore = "Caso Report_1, errore in fase di inserimento in tes_stampa_ordini.Errore sul DB: " + sqlca.sqlerrtext
							close righe_det_ord_ven_1;
	//						close test_padre;
			
							return -1
						end if
						
					
					next
					
				end if
			end if
//			close test_padre;

//****************************************************************************************************************
//***********************                  CALCOLO DELLA DISTINTA DI TAGLIO          *****************************

			if ls_flag_calcola_dt = "S"  then

				select flag_non_calcolare_dt
				into   :ls_flag_non_calcolare_dt
				from   distinta_padri
				where  cod_azienda=:s_cs_xx.cod_azienda
				and    cod_prodotto=:ls_cod_prodotto
				and    cod_versione=:ls_cod_versione;
				
				if sqlca.sqlcode< 0 then
					fs_errore = "caso report 1, lettura flag_non_calcolare_dt, Errore sul DB: " + sqlca.sqlerrtext
					close righe_det_ord_ven_1;
			
					return -1
				end if

				if ls_flag_non_calcolare_dt = 'N' or isnull(ls_flag_non_calcolare_dt) then
					
					delete    	distinte_taglio_calcolate
					where   	cod_azienda = :s_cs_xx.cod_azienda and
								anno_registrazione = :fi_anno_registrazione and
								num_registrazione = :fl_num_registrazione and
								prog_riga_ord_ven = :ll_prog_riga_ord_ven;
								
					if sqlca.sqlcode < 0 then
						fs_errore = g_str.format("Caso Report 1: Errore in cancellazione distinte taglio calcolate riga ordine $1-$2-$3. $4",fi_anno_registrazione,fl_num_registrazione,ll_prog_riga_ord_ven,sqlca.sqlerrtext )
						close righe_det_ord_ven_1;
						return -1
					end if

					
					li_risposta = f_calcola_dt(fi_anno_registrazione,  fl_num_registrazione, ll_prog_riga_ord_ven, ls_cod_prodotto, ls_cod_versione, ls_flag_ordinamento_dt, ls_errore)
					
					if li_risposta < 0 then
						messagebox("Apice",ls_errore,stopsign!)
						close righe_det_ord_ven_1;
						return -1
					end if
					
				end if
				
			end if

//************************                FINE CALCOLO DISTINTA DI TAGLIO            *****************************
//****************************************************************************************************************


		loop
		
		close righe_det_ord_ven_1;
		
				  
//************** Fine Case per report_1
		
//************** Inizio Case Report_2  o sfuso
	case 'B'
		
		declare righe_det_ord_ven_2 cursor for
		select  cod_prodotto,
				  cod_tipo_det_ven,
				  prog_riga_ord_ven,
				  cod_versione
		from    det_ord_ven
		where   cod_azienda=:s_cs_xx.cod_azienda
		and     anno_registrazione=:fi_anno_registrazione
		and     num_registrazione=:fl_num_registrazione
		and     num_riga_appartenenza=0
		and     flag_blocco='N'
		and     cod_tipo_det_ven <> 'ADD';
		
		open righe_det_ord_ven_2;
		if sqlca.sqlcode < 0 then 
			fs_errore = "Caso Report_2; OPEN CURSOR righe_det_ord_ven_2~r~n" + sqlca.sqlerrtext
			
			return -1
		end if
		
		do while true															//  scorre tutti i dettagli dell'ordine
			fetch righe_det_ord_ven_2
			into  :ls_cod_prodotto,
					:ls_cod_tipo_det_ven,
					:ll_prog_riga_ord_ven,
					:ls_cod_versione;
					
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode < 0 then 
				fs_errore = "Caso Report_2 errore in fase di lettura cursore su det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext
				close righe_det_ord_ven_2;
				
				return -1
			end if
			
			setnull(ls_cod_reparto)
			
			select cod_reparto
			into   :ls_cod_reparto
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;
			
			if sqlca.sqlcode < 0 then 
				f_scrivi_log("Stampa ordini produzione -->>Caso Report_2. Nell'ordine anno:" + string(fi_anno_registrazione) + & 
								 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + ls_cod_prodotto + & 
								 " che non ha il reparto associato nell'anagrafica prodotti. Verificare.")
				
				continue
			end if
			
			if isnull(ls_cod_reparto) then
				f_scrivi_log("Stampa ordini produzione -->>Caso Report_2. Nell'ordine anno:" + string(fi_anno_registrazione) + & 
								 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + ls_cod_prodotto + & 
								 " che non ha il reparto associato nell'anagrafica prodotti. Verificare.")
				
				continue

			end if
			
			//Donato 19/12/2011
			//verifica se esiste eccezione
			ls_reparti_letti[] = ls_vuoto[]
			ls_cod_reparti_trovati[] = ls_vuoto[]
			if f_reparti_stabilimenti(		fi_anno_registrazione, fl_num_registrazione, ls_cod_prodotto, "", &
												ls_cod_reparto, "", "", ls_reparti_letti[], ls_errore)<0 then
				//in fs_errore il messaggio
				fs_errore =  ls_errore + sqlca.sqlerrtext
				close righe_det_ord_ven_2;
									
				return -1
			end if
		
			//merge con l'array by ref principale
			guo_functions.uof_merge_arrays(ls_cod_reparti_trovati[], ls_reparti_letti[])
			
			//donato 13/03/2012 modifica per prevenire errori di configurazione prodotti in ordine sfuso (no-reparti)
			//dare messaggio all'utente e chiudere il cursore
			if upperbound(ls_cod_reparti_trovati[]) > 0 then
			else
				fs_errore = "Nessun reparto per il prodotto "+ls_cod_prodotto+"  nell'ordine tipo Report 2 "&
									+string(fi_anno_registrazione)+"/"+string(fl_num_registrazione)+"/"+string(ll_prog_riga_ord_ven)
				close righe_det_ord_ven_2;
				
				return -1		
			end if
			
			ls_cod_reparto = ls_cod_reparti_trovati[1]
			
			
			select cod_reparto
			into   :ls_cod_reparto_test
			from   tes_stampa_ordini
			where  cod_azienda=:s_cs_xx.cod_azienda
			and	 anno_registrazione=:fi_anno_registrazione
			and    num_registrazione=:fl_num_registrazione
			and    cod_reparto=:ls_cod_reparto
			and    prog_riga_comanda=0 and prog_sessione=:il_sessione;
			
			if sqlca.sqlcode < 0 then 
				fs_errore = "Caso Report_2 errore in fase di lettura tes_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext
				close righe_det_ord_ven_2;
									
				return -1
			end if
			
			if sqlca.sqlcode = 100 then
				
				//##########################################################
				li_risposta = wf_get_data_pronto(fi_anno_registrazione, fl_num_registrazione, 0, ls_cod_reparto, ldt_data_pronto)
				if li_risposta<0 then
					ldt_data_pronto = fdt_data_consegna
				end if
				//##########################################################
				
				
				insert into tes_stampa_ordini
				(cod_azienda,
				 anno_registrazione,
				 num_registrazione,
				 cod_reparto,
				 data_consegna,
				 prog_riga_comanda,
				 prog_sessione,
				 ordine)
				 values 
				 (:s_cs_xx.cod_azienda,
				  :fi_anno_registrazione,
				  :fl_num_registrazione,
				  :ls_cod_reparto,
				  :fdt_data_consegna,
				  0,
				  :il_sessione,
				  1);

				if sqlca.sqlcode < 0 then 
					fs_errore = "Caso Report_2 errore in fase di inserimento tes_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext
					close righe_det_ord_ven_2;
											
					return -1
				end if
				
				insert into det_stampa_ordini
				(cod_azienda,
				 anno_registrazione,
				 num_registrazione,
				 cod_reparto,
				 prog_riga_comanda,
				 prog_riga_ord_ven,
				 prog_sessione)
				 values 
				 (:s_cs_xx.cod_azienda,
				  :fi_anno_registrazione,
				  :fl_num_registrazione,
				  :ls_cod_reparto,
				  0,
				  :ll_prog_riga_ord_ven,
				  :il_sessione);

				if sqlca.sqlcode < 0 then 
					fs_errore = "Caso Report_2 errore in inserimento det_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext
					close righe_det_ord_ven_2;
											
					return -1
				end if
				
			else
				insert into det_stampa_ordini
				(cod_azienda,
				 anno_registrazione,
				 num_registrazione,
				 cod_reparto,
				 prog_riga_comanda,
				 prog_riga_ord_ven,
				 prog_sessione)
				 values 
				 (:s_cs_xx.cod_azienda,
				  :fi_anno_registrazione,
				  :fl_num_registrazione,
				  :ls_cod_reparto,
				  0,
				  :ll_prog_riga_ord_ven,
				  :il_sessione);

				if sqlca.sqlcode < 0 then 
					fs_errore = "Caso Report_2 errore in inserimento det_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext
					close righe_det_ord_ven_2;
											
					return -1
				end if
				
			end if
			
		loop
		
		close righe_det_ord_ven_2;
		
//************** Fine Case Report_2

// ************************** INIZIO CASO REPORT_3 TENDE TECNICHE

	case 'C'
		declare righe_det_ord_ven_3 cursor for
		select  cod_prodotto,
				  cod_tipo_det_ven,
				  prog_riga_ord_ven,
				  cod_versione
		from    det_ord_ven
		where   cod_azienda=:s_cs_xx.cod_azienda
		and     anno_registrazione=:fi_anno_registrazione
		and     num_registrazione=:fl_num_registrazione
		and     num_riga_appartenenza=0
		and     flag_blocco='N' ;
		
		open righe_det_ord_ven_3;
		
		do while true															//  scorre tutti i dettagli dell'ordine
			fetch righe_det_ord_ven_3
			into  :ls_cod_prodotto,
					:ls_cod_tipo_det_ven,
					:ll_prog_riga_ord_ven,
					:ls_cod_versione;
					
			if sqlca.sqlcode = 100 then exit
			
			if sqlca.sqlcode < 0 then 
				fs_errore = "Caso Report_3 (tende tecniche) errore in fase di lettura cursore su det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext
				close righe_det_ord_ven_3;
				
				return -1
			end if
			
			
			// Donato 18/06/2012: controllo se si tratta di riga addizionale; in questo caso salto
			luo_prod = create uo_produzione
			if luo_prod.uof_get_tipo_det_ven_add( 	fi_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven, 	ls_cod_tipo_det_ven_addizionali, fs_errore) < 0 then
				return -1
			end if
			destroy luo_prod
			if ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_addizionali then continue
			
			
								
			// trova reparti
			
			f_null_array(ls_cod_reparti_trovati[])           //azzero contenuto array dei reparti per la funzione trova reparti
			
//			declare test_padre_1 cursor for
			select cod_azienda
			into	:ls_test
			from   distinta_padri
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto;			 //test se prodotto è un prodotto con distinta base
																		 //devo aprire un cursore poichè possono essercu più versioni
//			open test_padre_1;
			
//			fetch test_padre_1 into :ls_test;
				
			if sqlca.sqlcode < 0 then 
				fs_errore = "Caso Report_3 (tende tecniche), lettura (per test) distinta_padri.Errore sul DB: " + sqlca.sqlerrtext
				close righe_det_ord_ven_3;
//				close test_padre_1;
				
				return -1
			end if
			
			if sqlca.sqlcode = 100 then	//se il prodotto non ha distinta base è un errore di compilazione 
													//dell'ordine poichè nei report_1 (tende da sole) e nei report 3 (tende tecniche) i prodotti che non sono optional 
													//o addizionali devono avere distinta base
				f_scrivi_log("Stampa ordini produzione -->>Caso Report_1. Nell'ordine anno:" + string(fi_anno_registrazione) + & 
								 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + ls_cod_prodotto + & 
								 " che non essendo ne optional ne addizionale DEVE avere la distinta base poichè la stampa di questo ordine per la produzione è di tipo Report 1. Verificare.")
				
//				close test_padre_1;
				continue //esce dall'ordine corrente e scansiona il prodotto successivo
				
			else
				// se il prodotto ha DB allora gli 'N' reparti vengono trovati tramite una funzione
				// che carica is_cod_reparto[] con i reparti dalla distinta base 
				// per le fasi di lavoro con flag_stampa fase ='S'
				
				//Donato 01/02/2012 Spostata funzione globale in user object oggetto
				luo_funzioni = create uo_funzioni_1
				li_risposta = luo_funzioni.uof_trova_reparti(	true, fi_anno_registrazione, fl_num_registrazione, & 
													 ll_prog_riga_ord_ven, ls_cod_prodotto ,ls_cod_versione, ls_cod_reparti_trovati[], ls_errore)
				destroy luo_funzioni
				
				//li_risposta=f_trova_reparti(fi_anno_registrazione, fl_num_registrazione, & 
				//									 ll_prog_riga_ord_ven,ls_cod_prodotto,ls_cod_versione,ls_cod_reparti_trovati[],ls_errore)
				
				if li_risposta = -1 then
					f_scrivi_log("Stampa ordini produzione -->>Caso Report_3 (tende tecniche). Nell'ordine anno:" + string(fi_anno_registrazione) + & 
					 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + ls_cod_prodotto + & 
					 " che durante la scansione della distinta base/fasi lavoro per trovare i reparti ha provocato questo errore:" + & 
					 ls_errore + " Verificare.")
	
//					close test_padre_1;
					continue //esce dalla riga ordine corrente e scansiona il prodotto successivo

				end if
				
				if upperbound(ls_cod_reparti_trovati[]) = 0 then		//nel caso non venga trovato alcun reparto scansionando la distinta
					SELECT cod_reparto,								//viene assegnato il reparto della fase di lavoro del  PF quando era 		
							 flag_stampa_fase							//un SL in un'altra distinta
					INTO   :ls_cod_reparto,
							 :ls_flag_stampa_fase
					FROM   tes_fasi_lavorazione
					WHERE  cod_azienda = :s_cs_xx.cod_azienda  and
							 cod_prodotto = :ls_cod_prodotto and
							 cod_versione = :ls_cod_versione;
				
					if sqlca.sqlcode=100 then
						f_scrivi_log("Stampa ordini produzione -->>Caso Report_3 (tende tecniche). Nell'ordine anno:" + string(fi_anno_registrazione) + & 
						 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + ls_cod_prodotto + & 
						 " che non ha una fase di lavoro con un reparto assegnato. Verificare.")
//						close test_padre;
						continue
					end if
				  
					if ls_flag_stampa_fase = 'N' then
						f_scrivi_log("Stampa ordini produzione -->>Case Report_3 (tende tecniche). Nell'ordine anno:" + string(fi_anno_registrazione) + & 
						 " Num:" + string(fl_num_registrazione) + ", è stato inserito il prodotto:" + ls_cod_prodotto + & 
						 " che ha una fase di lavoro con un reparto assegnato ma il flag_stampa_fase non è impostato. Verificare.")
						
//						close test_padre;
						continue
					end if																						
				end if
			end if
				
//			close test_padre_1;
			
			// fine trova reparti
			
			
			//ciclo for di inserimento reparti....
			
			for ll_num_reparti = 1 to upperbound(ls_cod_reparti_trovati[])
			
			
				select cod_reparto
				into   :ls_cod_reparto_test
				from   tes_stampa_ordini
				where  cod_azienda=:s_cs_xx.cod_azienda
				and	 anno_registrazione=:fi_anno_registrazione
				and    num_registrazione=:fl_num_registrazione
				and    cod_reparto=:ls_cod_reparti_trovati[ll_num_reparti]
				and    prog_riga_comanda=0 and prog_sessione=:il_sessione;
				
				if sqlca.sqlcode < 0 then 
					fs_errore = "Caso Report_3 (tende tecniche) errore in fase di lettura tes_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext
					close righe_det_ord_ven_3;
										
					return -1
				end if
				
				if sqlca.sqlcode = 100 then
					
					//##########################################################
					li_risposta = wf_get_data_pronto(fi_anno_registrazione, fl_num_registrazione, 0, ls_cod_reparti_trovati[ll_num_reparti], ldt_data_pronto)
					if li_risposta<0 then
						ldt_data_pronto = fdt_data_consegna
					end if
					//##########################################################
					
					insert into tes_stampa_ordini
					(cod_azienda,
					 anno_registrazione,
					 num_registrazione,
					 cod_reparto,
					 data_consegna,
					 prog_riga_comanda,
					 prog_sessione,
					 ordine)
					 values 
					 (:s_cs_xx.cod_azienda,
					  :fi_anno_registrazione,
					  :fl_num_registrazione,
					  :ls_cod_reparti_trovati[ll_num_reparti],
					  :fdt_data_consegna,
					  0,
					  :il_sessione,
					  :ll_num_reparti);
	
					if sqlca.sqlcode < 0 then 
						fs_errore = "Caso Report_3 (tende tecniche) errore in fase di inserimento tes_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext
						close righe_det_ord_ven_3;
												
						return -1
					end if
					
					insert into det_stampa_ordini
					(cod_azienda,
					 anno_registrazione,
					 num_registrazione,
					 cod_reparto,
					 prog_riga_comanda,
					 prog_riga_ord_ven,
					 prog_sessione)
					 values 
					 (:s_cs_xx.cod_azienda,
					  :fi_anno_registrazione,
					  :fl_num_registrazione,
					  :ls_cod_reparti_trovati[ll_num_reparti],
					  0,
					  :ll_prog_riga_ord_ven,
					  :il_sessione);
	
					if sqlca.sqlcode < 0 then 
						fs_errore = "Caso Report_3 (tende tecniche) errore in inserimento det_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext
						close righe_det_ord_ven_3;
												
						return -1
					end if
					
				else
					insert into det_stampa_ordini
					(cod_azienda,
					 anno_registrazione,
					 num_registrazione,
					 cod_reparto,
					 prog_riga_comanda,
					 prog_riga_ord_ven,
					 prog_sessione)
					 values 
					 (:s_cs_xx.cod_azienda,
					  :fi_anno_registrazione,
					  :fl_num_registrazione,
					  :ls_cod_reparti_trovati[ll_num_reparti],
					  0,
					  :ll_prog_riga_ord_ven,
					  :il_sessione);
	
					if sqlca.sqlcode < 0 then 
						fs_errore = "Caso Report_3 (tende tecniche) errore in inserimento det_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext
						close righe_det_ord_ven_3;
												
						return -1
					end if
					
				end if
			next
		loop
		
		close righe_det_ord_ven_3;
		

//*****************	FINE CASO REPORT 3 TENDE TECNICHE

end choose


return 1
end function

public subroutine wf_set_deposito_origine (long fl_anno_registrazione, long fl_num_registrazione, ref datawindow fdw_report);string ls_cod_deposito, ls_des_deposito_origine

select cod_deposito
into :ls_cod_deposito
from tes_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fl_anno_registrazione and
			num_registrazione=:fl_num_registrazione;

if not isnull(ls_cod_deposito) and ls_cod_deposito<>"" then
	
	select des_deposito
	into :ls_des_deposito_origine
	from anag_depositi
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_deposito = :ls_cod_deposito;
				
	if ls_des_deposito_origine<>"" and not isnull(ls_des_deposito_origine) then
		fdw_report.Modify("stabilimento_t.text='" + ls_des_deposito_origine + "'")
	end if
			
end if
end subroutine

public function integer wf_aggiungi_buffer_stabilimento (integer fi_anno_registrazione, long fl_num_registrazione, string fs_codice, string fs_tipo_codice, ref string fs_errore);string ls_cod_deposito, ls_non_bloccante, ls_flag_allegati
long ll_count

ls_non_bloccante = "Errore NON bloccante!"

if fs_tipo_codice="R" then
	select cod_deposito
	into :ls_cod_deposito
	from anag_reparti
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_reparto=:fs_codice;
				
	if sqlca.sqlcode<0 then
		fs_errore = ls_non_bloccante + "-  Errore in lettura deposito reparto "+fs_codice+": "+sqlca.sqlerrtext
		return -1
	end if
else
	ls_cod_deposito = fs_codice
end if

if isnull(ls_cod_deposito) or ls_cod_deposito="" then
	if fs_tipo_codice="R" then
		fs_errore = ls_non_bloccante + "-  Deposito non impostato per il reparto "+fs_codice
	else
		fs_errore = ls_non_bloccante + "-  Deposito origine non impostato per l'ordine "+string(fi_anno_registrazione)+"/"+string(fl_num_registrazione)
	end if
	
	return -1
end if

//se già esiste (per tale stabilimento) ignora l'inserimento...
select count(*)
into :ll_count
from tes_stampa_ord_ven_depositi
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_registrazione and
			num_registrazione=:fl_num_registrazione and
			cod_deposito_produzione=:ls_cod_deposito;

if sqlca.sqlcode<0 then
	fs_errore = ls_non_bloccante + " -  Errore in calcolo max spooler stampa per altri stabilimenti: "+sqlca.sqlerrtext
	return -1
end if

ls_flag_allegati = wf_controlla_allegati(fi_anno_registrazione, fl_num_registrazione)

if isnull(ll_count) or ll_count=0 or sqlca.sqlcode=100 then
	//solo in tal caso inserisci
	
	insert into	tes_stampa_ord_ven_depositi  
				( 	cod_azienda,   
					anno_registrazione,   
					num_registrazione,   
					cod_deposito_produzione,
					cod_deposito_origine,
					flag_allegati)  
	values ( 	:s_cs_xx.cod_azienda,
				:fi_anno_registrazione,   
				:fl_num_registrazione,   
				:ls_cod_deposito,
				:is_cod_deposito_operatore,
				:ls_flag_allegati);
				
	if sqlca.sqlcode<0 then
		fs_errore = ls_non_bloccante + " -  Errore in inserimento spooler stampa per altri stabilimenti: "+sqlca.sqlerrtext
		return -1
	end if
end if

return 1
end function

public function long wf_retrieve_altri_stabilimenti ();string			ls_cod_deposito
long			ll_ret

setpointer(hourglass!)

if is_cod_deposito_operatore=is_cod_deposito_CSSYSTEM then
	setnull(ls_cod_deposito)
	
elseif isnull(is_cod_deposito_operatore) then
	ls_cod_deposito=""
else
	ls_cod_deposito = is_cod_deposito_operatore
end if

ll_ret = dw_lista_ord_ven_altro_deposito.retrieve(s_cs_xx.cod_azienda, ls_cod_deposito)
dw_lista_ord_ven_altro_deposito.resetupdate()

setpointer(arrow!)

if ll_ret>0 then
	dw_lista_ord_ven_altro_deposito.selectrow(0,false)
	dw_lista_ord_ven_altro_deposito.selectrow(1, true)
	
	//p_ordini_altro_stab.visible = true
	st_ordini_altro_stab.visible = true
	
else
	//p_ordini_altro_stab.visible = false
	st_ordini_altro_stab.visible = false
	
end if

if ll_ret>0 then dw_lista_ord_ven_altro_deposito.postevent("pcd_modify")

return ll_ret
end function

public subroutine wf_stampa_altri_ordini ();integer			li_zoom=100, li_anno_registrazione, li_risposta

long				ll_riga, ll_num_registrazione, ll_num_copie_sole, ll_num_copie_tecniche, ll_num_copie_sfuso, ll_prog_riga_comanda, &
					ll_num_tenda, job, ll_t, ll_tot, ll_tot_sel

string				ls_flag_calcola_dt, ls_cod_tipo_det_ven, ls_cod_tipo_ord_ven, ls_sql, ls_cod_reparto, ls_document_name, ls_tipo_stampa, &
					ls_errore, ls_cod_deposito_reparto, ls_selezionato

datetime			ldt_data_consegna


il_sessione = wf_get_id_sessione()

dw_lista_ord_ven_altro_deposito.accepttext()

select numero
into   :li_zoom
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'ZSP';
if sqlca.sqlcode <> 0 then li_zoom = 100
if isnull(li_zoom) then li_zoom = 100


ls_flag_calcola_dt = dw_ext_selezione.getitemstring(1,"flag_calcola_dt")

ll_tot = dw_lista_ord_ven_altro_deposito.rowcount()

hpb_1.position = 0

ll_tot_sel = 0
for ll_riga=1 to ll_tot
	ls_selezionato = dw_lista_ord_ven_altro_deposito.getitemstring(ll_riga,"selezionato")
	if ls_selezionato="S" then ll_tot_sel += 1
next

hpb_1.maxposition = ll_tot_sel


for ll_riga = 1 to ll_tot //scorre le righe selezionate dall'utente
	
	ls_selezionato = dw_lista_ord_ven_altro_deposito.getitemstring(ll_riga,"selezionato")
	
	if ls_selezionato="S" then
	else
		continue
	end if
	
	hpb_1.stepit( )
	
	li_anno_registrazione =dw_lista_ord_ven_altro_deposito.getitemnumber(ll_riga,"anno_registrazione")
	ll_num_registrazione =dw_lista_ord_ven_altro_deposito.getitemnumber(ll_riga,"num_registrazione")
	ls_cod_tipo_ord_ven = dw_lista_ord_ven_altro_deposito.getitemstring(ll_riga,"cod_tipo_ord_ven")
	ldt_data_consegna = dw_lista_ord_ven_altro_deposito.getitemdatetime(ll_riga,"data_consegna")
	
	if wf_stampa_ordini(li_anno_registrazione, ll_num_registrazione, ls_cod_tipo_ord_ven, ldt_data_consegna, ls_errore) < 0 then
		rollback;
		g_mb.error("Apice",ls_errore)
		return
	end if
	
next


// *********** INIZIO PROCESSO DI STAMPA *********************

ll_num_copie_sole = long(em_num_copie_sole.text)
ll_num_copie_tecniche = long(em_num_copie_tecniche.text)
ll_num_copie_sfuso = long(em_num_copie_sfuso.text)


declare r_tes_stampa dynamic cursor for sqlsa;

ls_sql =" select  anno_registrazione," + &
		  " num_registrazione," + &
		  " cod_reparto," + &
		  " prog_riga_comanda," + &
		  " num_tenda" + &
	     " from    tes_stampa_ordini" + &
	     " where   cod_azienda= '" + s_cs_xx.cod_azienda + "' and prog_sessione="+string(il_sessione)
	
ls_sql=ls_sql + " order by cod_reparto, data_consegna, anno_registrazione, num_registrazione, num_tenda "

prepare sqlsa from :ls_sql;

open dynamic r_tes_stampa;

do while 1 = 1	
	fetch r_tes_stampa
	into  :li_anno_registrazione,
			:ll_num_registrazione,
			:ls_cod_reparto,
			:ll_prog_riga_comanda,
			:ll_num_tenda;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then 
		g_mb.error("Apice","Processo di Lettura cursore su tes_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext)
		close r_tes_stampa;
		rollback;		
		return
	end if
	
		// stefanop 28/05/2010: ticket 136: costruisco il nome del documento
	ls_document_name = g_str.format("Ordine $1-$2-$3-$4-$5",li_anno_registrazione, ll_num_registrazione, ls_cod_reparto,ll_prog_riga_comanda, ll_num_tenda)


	select cod_tipo_ord_ven
	into   :ls_cod_tipo_ord_ven
	from   tes_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;
	
	if sqlca.sqlcode < 0 then 
		g_mb.error("Apice","Errore sul DB: " + sqlca.sqlerrtext)
		close r_tes_stampa;
		rollback;		
		return
	end if
	
	select flag_tipo_bcl      //lettura tipo report
	into   :ls_tipo_stampa
	from   tab_tipi_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;
	
	if sqlca.sqlcode < 0 then 
		g_mb.error("Apice","Errore sul DB: " + sqlca.sqlerrtext)
		close r_tes_stampa;
		rollback;
		return
	end if

	select cod_deposito
	into :ls_cod_deposito_reparto
	from anag_reparti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_reparto=:ls_cod_reparto;
	
	//se si tratta di un reparto di stabilimento diverso da quello dell'operatore non stampare per tale reparto
	if ls_cod_deposito_reparto=is_cod_deposito_operatore or is_cod_deposito_operatore=is_cod_deposito_CSSYSTEM then
		//se entri nell'IF il reparto appartiene al tuo stabilimento oppure sei CS_SYSTEM, quindi prosegui normalmente
		
		choose case ls_tipo_stampa // case per tipo report e impostazione datawindow
			case 'A'
				dw_report.reset()
				
				if ls_flag_calcola_dt = "S" then                      // per ora teniamo distinti i due report 1: uno apposito per la distinta di taglio e l'altro normale
					dw_report.DataObject = 'd_report_1_dt'
				else
					dw_report.DataObject = 'd_report_1'
				end if
				
				//funzione che compila report 1
				
				li_risposta = f_report_1(li_anno_registrazione,ll_num_registrazione,ls_cod_reparto,ll_prog_riga_comanda,ll_num_tenda)
				
				if li_risposta < 0 then 
					close r_tes_stampa;
					rollback;				
					return
				end if
							
				job = PrintOpen(ls_document_name) // stefanop 28/05/2010: ticket 136
				for ll_t = 1 to ll_num_copie_sole
					dw_report.object.datawindow.zoom = li_zoom
					PrintDataWindow(job, dw_report) 
				next
				PrintClose(job)
				
			case 'B'
				
				dw_report.DataObject = 'd_report_2'
				
				//funzione che compila report 2
				li_risposta = f_report_2(li_anno_registrazione,ll_num_registrazione,ls_cod_reparto)
	
				if li_risposta < 0 then 
					close r_tes_stampa;
					rollback;				
					return
				end if
				
				job = PrintOpen(ls_document_name) // stefanop 28/05/2010: ticket 136
				for ll_t = 1 to ll_num_copie_sfuso
					dw_report.object.datawindow.zoom = li_zoom
					PrintDataWindow(job, dw_report) 
				next
				PrintClose(job)
				
			case 'C'
				dw_report.reset()
				dw_report.DataObject = 'd_report_3'
				
				//funzione che compila report 3
				li_risposta = f_report_3(li_anno_registrazione,ll_num_registrazione,ls_cod_reparto)
	
				if li_risposta < 0 then 
					close r_tes_stampa;
					rollback;				
					return
				end if
				
				job = PrintOpen(ls_document_name) // stefanop 28/05/2010: ticket 136
				for ll_t = 1 to ll_num_copie_tecniche
					dw_report.object.datawindow.zoom = li_zoom
					PrintDataWindow(job, dw_report) 
				next
				PrintClose(job)
				
		end choose	
	
	else
		//NON inserire in spooler per altri stabilimenti, non fare niente ...
	end if
	
	update tes_ord_ven
	set    flag_stampata_bcl='S'
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;

	if sqlca.sqlcode < 0 then 
		g_mb.error("Apice","Errore durante update tes_ord_ven flag_stampata_blc. Errore sul DB: " + sqlca.sqlerrtext)
		close r_tes_stampa;
		rollback;
		return
	end if

	//Donato 19-11-2008 rimettere il flag_aut_blocco_fido a "N"
	//modifica per gestione fidi PTENDA
	update tes_ord_ven
	set    flag_aut_sblocco_fido='N'
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;

	if sqlca.sqlcode < 0 then 
		g_mb.error("Apice","Errore durante update tes_ord_ven flag_aut_sblocco_fido. Errore sul DB: " + sqlca.sqlerrtext)
		close r_tes_stampa;
		rollback;
		return
	end if
	//fine modifica -----------------------------------------------------------
	
	if  is_cod_deposito_operatore<>is_cod_deposito_CSSYSTEM and not isnull(is_cod_deposito_operatore) then
		//ripulisci l'ordine dallo spooler per lo stabilimento
		delete from tes_stampa_ord_ven_depositi
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:li_anno_registrazione and
					num_registrazione=:ll_num_registrazione and
					cod_deposito_produzione=:is_cod_deposito_operatore;
					
		if sqlca.sqlcode<0 then
			g_mb.error("Apice","Errore durante la cancellazione dell'ordine "+string(li_anno_registrazione)+"/"+string(ll_num_registrazione)+&
												" dallo spooler dallo stabilimento "+is_cod_deposito_operatore+": " + sqlca.sqlerrtext)
			close r_tes_stampa;
			rollback;
		return
		end if
	end if
	
loop

close r_tes_stampa;

// *********** FINE PROCESSO DI STAMPA ***********************

commit;


wf_retrieve_altri_stabilimenti()

delete det_stampa_ordini where prog_sessione=:il_sessione;
delete tes_stampa_ordini where prog_sessione=:il_sessione;

commit;

g_mb.show("APICE", "Stampa Ordini di Produzione terminata!")

hpb_1.position = 0
end subroutine

public function longlong wf_get_id_sessione ();longlong			ll_id, ll_temp
string				ls_temp

//numero 		es.			2012011109452235 (fino ai millesecondi con due cifre)
ls_temp = string(today(), "yyyymmdd") + string(now(), "hhmmssff")

//come ulteriore sicurezza ci attacco il numero dello stabilimento (solo se è numerico con cifre nn oltre 3)
if isnumber(is_cod_deposito_operatore) and len(is_cod_deposito_operatore)<=3 and &
				not isnull(is_cod_deposito_operatore) and is_cod_deposito_operatore<>"" then
	
	//es per lo stabilimento 100  ->  1002012011109452235
	ls_temp = is_cod_deposito_operatore + ls_temp
end if
ll_id = longlong(ls_temp)

return ll_id
end function

public function string wf_get_printer_name ();string ls_printer
long ll_pos

ls_printer = PrintGetPrinter ( )
	
do while true
	ll_pos = pos(ls_printer, "~t")
	if ll_pos < 1 then exit
	ls_printer = Replace (ls_printer, ll_pos, 1, " / ")
loop

if isnull(ls_printer) then return ""

if ls_printer<>"" then ls_printer = " - Stampa su " + ls_printer

return ls_printer
end function

public function integer wf_invia_mail ();string				ls_cod_deposito_prod_cu, ls_sql, ls_errore, ls_messaggio
datastore		lds_data
long				ll_tot, ll_index, ll_anno_reg_cu, ll_num_reg_cu

if is_cod_deposito_operatore=is_cod_deposito_CSSYSTEM then return 1

uo_sendmail  luo_send
luo_send = create uo_sendmail


//ordino per deposito produzione, leggendo solo quelli del mio stabilimento
declare  mail_altri cursor for
select distinct cod_deposito_produzione
from tes_stampa_ord_ven_depositi
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_deposito_origine=:is_cod_deposito_operatore
order by cod_deposito_produzione;

open mail_altri;
if sqlca.sqlcode < 0 then 
	g_mb.error("Errore apertura cursore mail_altri: " + sqlca.sqlerrtext)
	return -1
end if

do while 1 = 1
	fetch mail_altri
	into  :ls_cod_deposito_prod_cu;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then 
		g_mb.error("Errore lettura cursore su mail_altri: " + sqlca.sqlerrtext)
		close mail_altri;
		return -1
	end if
	
	ls_sql = 	"select anno_registrazione, num_registrazione "+&
				"from tes_stampa_ord_ven_depositi "+&
				"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"cod_deposito_origine='"+is_cod_deposito_operatore+"' and "+&
							"cod_deposito_produzione='"+ls_cod_deposito_prod_cu+"' "
	
	ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
	if ll_tot<0 then
		g_mb.error(ls_errore)
		close mail_altri;
		return -1
	end if
	
	if ll_tot=0 then
		//g_mb.show("Nessun destinatario impostato per stabilimento con codice "+ls_cod_deposito_prod_cu)
		continue
	end if
	
	ls_messaggio = ""
	for ll_index=1 to ll_tot
		//leggo anno e numero ordine
		ll_anno_reg_cu = lds_data.getitemnumber(ll_index, 1)
		ll_num_reg_cu = lds_data.getitemnumber(ll_index, 2)
		
		ls_messaggio += string(ll_anno_reg_cu)+"/"+string(ll_num_reg_cu)+"~r~n"
	next

	//invio mail per questo stabilimento di produzione
	wf_invia_mail(ls_messaggio, ls_cod_deposito_prod_cu)

loop

close mail_altri;

//g_mb.show("Processo invio mail terminato!")

return 1
end function

public function integer wf_invia_mail (string fs_messaggio, string fs_deposito_prod);string					ls_des_stabilimento, ls_dest[], ls_cc[], ls_from, ls_oggetto, ls_sql, ls_errore, ls_des_stabilimento_prod, &
						ls_smtp, ls_usr_smtp, ls_pwd_smtp, ls_messaggio_test
uo_sendmail		luo_send
long					ll_index, ll_tot_mittenti, ll_tot_destinatari
datastore			lds_cc, lds_to

ls_des_stabilimento = f_des_tabella("anag_depositi","cod_deposito ='" +  is_cod_deposito_operatore +"'", "des_deposito")
ls_des_stabilimento_prod = f_des_tabella("anag_depositi","cod_deposito ='" +  fs_deposito_prod +"'", "des_deposito")

ls_messaggio_test = ""
//ls_messaggio_test="ATTENZIONE QUESTO E' UN MESSAGGIO DI TEST INVIO ORDINI DI PROVA di ConsultingSoftware: "+&
//							"SI PREGA DI IGNORARE MI RACCOMANDO!~r~n~r~n~r~n"

luo_send = create uo_sendmail

//MITTENTE (sender) --------------------------------------------------------------------------------------
//lo leggo dal deposito operatore
select email, smtp, usr_smtp, pwd_smtp
into :ls_from, :ls_smtp, :ls_usr_smtp, :ls_pwd_smtp
from anag_depositi_mittenti
where cod_azienda=:s_cs_xx.cod_azienda and
		cod_deposito=:is_cod_deposito_operatore;

if sqlca.sqlcode<0 then
	destroy luo_send;
	g_mb.error("Errore in lettura parametri sender: "+sqlca.sqlerrtext)
	return -1
end if

if sqlca.sqlcode=100 or isnull(ls_from) or ls_from="" or isnull(ls_smtp) or ls_smtp="" then
	destroy luo_send;
	g_mb.error("Mancano i parametri sender per stabilimento "+is_cod_deposito_operatore + " (tabella anag_depositi_mittenti)")
	return -1
end if

//ls_from = "bortolotto.beatrice@progettotenda.com"
//ls_smtp = "192.168.1.115"
//ls_usr_smtp = ""
//ls_pwd_smtp = ""

if isnull(ls_usr_smtp) then ls_usr_smtp = ""
if isnull(ls_usr_smtp) then ls_pwd_smtp = ""

//ls_from = "bortolotto.beatrice@progettotenda.com"  //"noreply@gibus.it"
luo_send.uof_set_from( ls_from )
//----------------------------------------------------------------------------------------------------


//datastore mail per conoscenza CC ----------------------------------------------------------
ls_sql = 	"select email "+&
			"from anag_depositi_destinatari "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda +"' and "+&
						"cod_deposito='"+is_cod_deposito_operatore+"' "
						
ll_tot_mittenti = guo_functions.uof_crea_datastore(lds_cc, ls_sql, ls_errore)

if ll_tot_mittenti <0 then
	destroy luo_send;
	destroy lds_cc;
	g_mb.error(ls_errore)
	
	return -1
end if

if ll_tot_mittenti=0 then
	destroy luo_send;
	destroy lds_cc;
	g_mb.error("Nessun mittente impostato per lo stabilimento "+ls_des_stabilimento)
	
	return -1
end if

for ll_index=1 to ll_tot_mittenti
	ls_cc[ll_index] = lds_cc.getitemstring(ll_index, 1)
next

destroy lds_cc;

//ls_cc[1] = "donato.cassano@csteam.com"
luo_send.uof_add_cc( ls_cc[] )
//--------------------------------------------------------------------------------------------------

//DESTINATARI ---------------------------------------------------------------------------------
ls_sql = 	"select email "+&
			"from anag_depositi_destinatari "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda +"' and "+&
						"cod_deposito='"+fs_deposito_prod+"' "

ll_tot_destinatari = guo_functions.uof_crea_datastore(lds_to, ls_sql, ls_errore)

if ll_tot_destinatari <0 then
	destroy luo_send;
	destroy lds_to;
	g_mb.error(ls_errore)
	
	return -1
end if

if ll_tot_destinatari=0 then
	destroy luo_send;
	destroy lds_cc;
	g_mb.error("Nessun destinatario impostato per lo stabilimento "+ls_des_stabilimento_prod)
	
	return -1
end if

for ll_index=1 to ll_tot_destinatari
	ls_dest[ll_index] = lds_to.getitemstring(ll_index, 1)
next

destroy lds_to;

luo_send.uof_add_to( ls_dest[] )
//--------------------------------------------------------------------------------------------------


//OGGETTO ---------------------------------------------------------------------------------------
ls_oggetto = "Notifica stampa ordini produzione da "+ls_des_stabilimento
luo_send.uof_set_subject( ls_oggetto )
//--------------------------------------------------------------------------------------------------

//TESTO MESSAGGIO ---------------------------------------------------------------------------
fs_messaggio = ls_messaggio_test + "Sono stati stampati i seguenti ordini dallo stabilimento "+ls_des_stabilimento+"~r~n"+fs_messaggio
//fs_messaggio = "Sono stati stampati i seguenti ordini dallo stabilimento "+ls_des_stabilimento+"~r~n"+fs_messaggio

//--------------------------------------------------------------------------------------------------

luo_send.uof_set_message( fs_messaggio )

luo_send.uof_set_smtp( ls_smtp, ls_usr_smtp, ls_pwd_smtp)


if not luo_send.uof_send( ) then
	g_mb.error("Errore invio mail a "+ls_des_stabilimento_prod)
	return 2
else
	//Messaggio inviato
end if

destroy luo_send;

return 0
end function

public function string wf_controlla_allegati (integer fi_anno_registrazione, long fl_num_registrazione);long ll_count

//verifica tra le righe di dettaglio, ce n'è almeno una con flag allegato pari a SI
select count(*)
into :ll_count
from comp_det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda  and
		 anno_registrazione = :fi_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and flag_allegati='S';
		 
if ll_count>0 then
	return "S"
else
	return "N"
end if

end function

public function string wf_get_colonna_dinamica (string fs_cod_cliente);long ll_id_valore_lista
string ls_valore_stringa, ls_flag_tipo_colonna
decimal ld_valore_numero
datetime ldt_valore_data


select flag_tipo_colonna  
into :ls_flag_tipo_colonna
from tab_colonne_dinamiche
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_colonna_dinamica=:is_ETIC;

if isnull(ls_flag_tipo_colonna) or sqlca.sqlcode<>0 then return ""

select id_valore_lista,
		valore_stringa,
		valore_numero,
		valore_data
into	:ll_id_valore_lista,
		:ls_valore_stringa,
		:ld_valore_numero,
		:ldt_valore_data
from anag_clienti_col_din
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_colonna_dinamica=:is_ETIC and
			cod_cliente=:fs_cod_cliente;

choose case ls_flag_tipo_colonna
	case "L"
		select des_valore
		into :ls_valore_stringa
		from tab_colonne_dinamiche_valori
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_colonna_dinamica=:is_ETIC and
					progressivo=:ll_id_valore_lista;
		
		if isnull(ls_valore_stringa) or sqlca.sqlcode<>0 then return ""
		
		return ls_valore_stringa
		
	case "S"
		if isnull(ls_valore_stringa) then ls_valore_stringa=""
		return ls_valore_stringa
		
	case "N"
		if isnull(ld_valore_numero) then return ""
		return string(ld_valore_numero, "########0.00")
		
		
	case "D"
		if isnull(ldt_valore_data) or year(date(ldt_valore_data))<1950 then return ""
		return string(ldt_valore_data, "dd/mm/yyyy")
		
	case else
		return ""
		
end choose
		
return ""
end function

public function integer wf_ordini_senza_commessa (long fi_anno_registrazione, long fl_num_registrazione, ref string fs_errore);//torna 	1 se l'ordine è senza commesse (basta una riga in tale condizione)
//			0 se l'ordine è a posto dal punto di vista delle commesse
//			-1 in caso di errore

//se trova un ordine in condizione senza commesse lo inserisce nella dw_ordini_senza_commessa

string						ls_cod_tipo_det_ven, ls_flag_gen_commessa, ls_flag_tipo_det_ven, ls_cod_cliente, ls_cod_tipo_ord_ven, &
								ls_rag_soc_1, ls_cod_deposito, ls_des_deposito, ls_des_tipo_ord_ven

datetime					ldt_data_consegna, ldt_data_registrazione

boolean					lb_ok

long							ll_new


declare cu_det_ord_ven_commessa cursor for 
	select cod_tipo_det_ven,
			 flag_gen_commessa
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_registrazione = :fi_anno_registrazione and 
			 num_registrazione = :fl_num_registrazione and 
			 quan_in_evasione + quan_evasa < quan_ordine and  
			 flag_blocco = 'N' and  
			 anno_commessa is null;

open cu_det_ord_ven_commessa;

if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la creazione del cursore cu_det_ord_ven_commessa: " + sqlca.sqlerrtext
	return -1
end if

lb_ok = false
do while 0 = 0
	fetch cu_det_ord_ven_commessa 
	into :ls_cod_tipo_det_ven,
		  :ls_flag_gen_commessa;

	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then
		fs_errore = "Errore durante la fetch del cursore cu_det_ord_ven. Dettaglio " + sqlca.sqlerrtext
		close cu_det_ord_ven_commessa;
		rollback;
		return -1
	end if

	select flag_tipo_det_ven 
	into   :ls_flag_tipo_det_ven  
	from   tab_tipi_det_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			 
	if ls_flag_tipo_det_ven = 'M' and ls_flag_gen_commessa='S' then
		lb_ok = true
		exit
	else
//		if ls_flag_gen_commessa = 'S' then
//			if ls_flag_non_collegati = 'S' and ls_flag_tipo_det_ven = 'C' then
//				lb_ok = true
//				exit
//			end if
//		end if
	end if
loop
close cu_det_ord_ven_commessa;

if lb_ok then
	
	select	cod_cliente,
				data_consegna,
				data_registrazione,
				cod_tipo_ord_ven,
				rag_soc_1,
				cod_deposito
	into		:ls_cod_cliente,
				:ldt_data_consegna,
				:ldt_data_registrazione,
				:ls_cod_tipo_ord_ven,
				:ls_rag_soc_1,
				:ls_cod_deposito
	from tes_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione = :fi_anno_registrazione and 
				num_registrazione = :fl_num_registrazione;
				
	if sqlca.sqlcode<0 then
		fs_errore = "Errore durante la lettura info testata ordine senza commessa: " + sqlca.sqlerrtext
		return -1
	end if
	
	//inserisci riga su datawindow
	ll_new = dw_ordini_senza_commessa.insertrow(0)
	
	dw_ordini_senza_commessa.setitem(ll_new, "anno_registrazione", fi_anno_registrazione)
	dw_ordini_senza_commessa.setitem(ll_new, "num_registrazione", fl_num_registrazione)
	dw_ordini_senza_commessa.setitem(ll_new, "cod_cliente", ls_cod_cliente)
	dw_ordini_senza_commessa.setitem(ll_new, "data_consegna", ldt_data_consegna)
	dw_ordini_senza_commessa.setitem(ll_new, "data_registrazione", ldt_data_registrazione)
	dw_ordini_senza_commessa.setitem(ll_new, "tipo_ord_ven", ls_cod_tipo_ord_ven)
	dw_ordini_senza_commessa.setitem(ll_new, "ragione_sociale", ls_rag_soc_1)
	dw_ordini_senza_commessa.setitem(ll_new, "cod_deposito", ls_cod_deposito)
	
	
	//ls_des_deposito, ls_des_tipo_ord_ven
	select des_deposito
	into :ls_des_deposito
	from anag_depositi
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_deposito=:ls_cod_deposito;
	dw_ordini_senza_commessa.setitem(ll_new, "des_deposito", ls_des_deposito)
	
	select des_tipo_ord_ven
	into :ls_des_tipo_ord_ven
	from tab_tipi_ord_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;
	dw_ordini_senza_commessa.setitem(ll_new, "des_tipo_ord_ven", ls_des_tipo_ord_ven)
	
	return 1
end if

return 0


end function

public function integer wf_scrivi_log (long fl_anno, long fl_numero, string fs_cod_reparto, string fs_msg_aggiuntivo);string			ls_messaggio
long				li_file, li_risposta

//scrive nel log solo se espressamente richiesto
if not ib_log then return 0

li_file = fileopen(is_nome_file_log, linemode!, Write!, LockWrite!, Append!)

if isnull(fs_msg_aggiuntivo) then fs_msg_aggiuntivo=""

ls_messaggio = string(today(),"dd/mm/yyyy") + "~t" + string(now(),"hh:mm:ss") +  "~t" + &
									string(fl_anno)+"/"+string(fl_numero)+"~t "+fs_cod_reparto + "~t"+ fs_msg_aggiuntivo
									
li_risposta = filewrite(li_file, ls_messaggio)
fileclose(li_file)

return 0

end function

public function integer wf_get_data_pronto (integer ai_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_cod_reparto, ref datetime adt_data_pronto);string						ls_flag_tipo_bcl
long							ll_progr_tes_produzione
uo_produzione			luo_prod



setnull(adt_data_pronto)


select tab_tipi_ord_ven.flag_tipo_bcl
into	:ls_flag_tipo_bcl
from tab_tipi_ord_ven
join tes_ord_ven on 	tes_ord_ven.cod_azienda=tab_tipi_ord_ven.cod_azienda and
								tes_ord_ven.cod_tipo_ord_ven=tab_tipi_ord_ven.cod_tipo_ord_ven
where 	tab_tipi_ord_ven.cod_azienda=:s_cs_xx.cod_azienda and
			tes_ord_ven.anno_registrazione=:ai_anno_registrazione and
			tes_ord_ven.num_registrazione=:al_num_registrazione;

if ls_flag_tipo_bcl="A" then
	
	select 	data_pronto
	into  :adt_data_pronto
	from   det_ordini_produzione
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_registrazione and
				num_registrazione=:al_num_registrazione and
				prog_riga_ord_ven=:al_prog_riga_ord_ven and
				cod_reparto=:as_cod_reparto;
	
	
elseif ls_flag_tipo_bcl="B" or ls_flag_tipo_bcl="C" then
	
	select progr_tes_produzione
	into   :ll_progr_tes_produzione
	from   tes_ordini_produzione
	where	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ai_anno_registrazione and
				num_registrazione=:al_num_registrazione and
				cod_reparto=:as_cod_reparto;

luo_prod = create uo_produzione
luo_prod.uof_get_data_pronto_reparto(ll_progr_tes_produzione, adt_data_pronto)
destroy luo_prod
	
else
	return -1
end if

if not isnull(adt_data_pronto) and year(date(adt_data_pronto))>1950 then
	return 0
end if

return -1
end function

public function string wf_logo ();string			ls_parametro

guo_functions.uof_get_parametro_azienda("TLE", ls_parametro)

if isnull(ls_parametro) then ls_parametro = ""

return ls_parametro
end function

public subroutine wf_carica_operatori ();string			ls_cod_operatore, ls_des_operatore, ls_cod_operatore_utente
long			ll_i

//funzione chiamata all'inizio
//serve a caricare la dw degli operatori ed a impostare l'operatore di default associato all'utente collegato

dw_elenco_operatori_selezionati.visible = false

select cod_operatore
into :ls_cod_operatore_utente
from tab_operatori_utenti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_utente=:s_cs_xx.cod_utente and
			flag_default='S';

if sqlca.sqlcode=0 and ls_cod_operatore_utente<>"" and not isnull(ls_cod_operatore_utente) then
else
	ls_cod_operatore_utente = ""
end if

declare cu_operatori cursor for
select cod_operatore, des_operatore
from   tab_operatori
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_blocco = 'N'
order by cod_operatore;
open cu_operatori;
do while true
	fetch cu_operatori into :ls_cod_operatore, :ls_des_operatore;
	if sqlca.sqlcode <> 0 then exit
	ll_i = dw_elenco_operatori_selezionati.insertrow(0)
	dw_elenco_operatori_selezionati.setitem(ll_i, "cod_operatore",ls_cod_operatore)
	dw_elenco_operatori_selezionati.setitem(ll_i, "des_operatore",ls_des_operatore)
	
	//pre-seleziona l'operatore default dell'utente collegato
	if ls_cod_operatore_utente<>"" and ls_cod_operatore_utente= ls_cod_operatore then
		dw_elenco_operatori_selezionati.setitem(ll_i, "flag_selezionato", "S")
	else
		dw_elenco_operatori_selezionati.setitem(ll_i, "flag_selezionato", "N")
	end if
	
loop
close cu_operatori;

dw_elenco_operatori_selezionati.setsort("flag_selezionato desc, cod_operatore asc")
dw_elenco_operatori_selezionati.sort()

return
end subroutine

public function integer wf_get_operatori (ref string as_operatori);
long				ll_tot, ll_index
boolean			lb_almeno_uno = false

//se entri in questa funzione vuol dire che hai specificato il filtro per operatori ed in checkbox "Seleziona Tutti è disattivato"

as_operatori = ""

ll_tot = dw_elenco_operatori_selezionati.rowcount()

//controlla che ci sia almeno uno posto a S
for ll_index=1 to ll_tot
	if dw_elenco_operatori_selezionati.getitemstring(ll_index, "flag_selezionato")="S" then
		lb_almeno_uno = true
		exit
	end if
next

//no operatori selezionati, ignora filtro per operatori
if not lb_almeno_uno then return 0

for ll_index=1 to ll_tot
	if dw_elenco_operatori_selezionati.getitemstring(ll_index, "flag_selezionato")="S" then
		if as_operatori<>"" then as_operatori += ","
		as_operatori += "'"+dw_elenco_operatori_selezionati.getitemstring(ll_index, "cod_operatore")+"'"
	end if
next

return 1
end function

public function integer wf_giacenza (long al_row, string as_cod_prodotto, string as_cod_deposito, ref string as_errore);

try
	dw_report.setitem(al_row,"giacenza", 0)
catch (runtimeerror err)
	//se va in errore allora esci dalla funzione
	return 0
end try


//prosegui pure con il calcolo della giacenza
uo_magazzino luo_magazzino

string					ls_where, ls_error, ls_chiave[]
datetime				ldt_data_a
dec{4}				ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_giacenza
integer				li_return

ldt_data_a = datetime(today(), 23:59:59)

if g_str.isnotempty(as_cod_deposito) then ls_where = "and cod_deposito=~~'"+as_cod_deposito+"~~'"

luo_magazzino = CREATE uo_magazzino

li_return = luo_magazzino.uof_saldo_prod_date_decimal(	as_cod_prodotto, ldt_data_a, ls_where, ld_quant_val, ls_error, "D", &
																			ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[],ld_quan_costo_medio_stock[])
destroy luo_magazzino

//prendo solo il primo (ce ne sarà uno solo perchè gli ho passato il flitro per deposito)
if upperbound(ld_giacenza_stock[]) > 0 then
	ld_giacenza = ld_giacenza_stock[1]
end if

if isnull(ld_giacenza) then ld_giacenza = 0
dw_report.setitem(al_row,"giacenza", ld_giacenza)

return 0
end function

public subroutine wf_clicked_ottimizza ();long				ll_anno_registrazione, ll_riga, ll_num_registrazione,  ll_tot

string				ls_sql, ls_errore

datetime			ldt_data_consegna

datastore		lds_sessione
treeviewitem 	ltv_item
s_tree_lancio_produzione 	ls_record, ls_record_null


tv_lancio.getitem(tv_lancio.il_handle,ltv_item)

ls_record = ltv_item.data

// tutte le sessioni hanno il livello ZERO
if ls_record.livello <> 0 then
	g_mb.warning("Selezionare una sessione valida!")
	return 
end if

dw_ottimizza_1.reset()
//dw_ottimizza_2.reset()
dw_ottimizza_3.reset()
dw_ottimizza_4.reset()


ls_sql = g_str.format(" select distinct anno_reg_ord_ven, num_reg_ord_ven, tes_ord_ven.cod_tipo_ord_ven, tes_ord_ven.data_consegna, tes_ord_ven.cod_cliente " + &
" from det_lancio_prod  " + &
" join tes_ord_ven on tes_ord_ven.cod_azienda = det_lancio_prod.cod_azienda and tes_ord_ven.anno_registrazione = det_lancio_prod.anno_reg_ord_ven and tes_ord_ven.num_registrazione= det_lancio_prod.num_reg_ord_ven " + &
" join tab_tipi_ord_ven on tab_tipi_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and tab_tipi_ord_ven.cod_tipo_ord_Ven = tes_ord_ven.cod_tipo_ord_ven " + &
" where id_tes_lancio_prod = $1 ",ls_record.id_padre)

ll_tot = guo_functions.uof_crea_datastore(lds_sessione, ls_sql)

dw_folder.fu_selecttab(5)
setpointer(HourGlass!)

for ll_riga = 1 to ll_tot

	pcca.mdi_frame.setmicrohelp("Elaborazione ordine " + string(lds_sessione.getitemnumber(ll_riga, 1)) +"/" + string(lds_sessione.getitemnumber(ll_riga, 2)) + " ...")
	
	wf_ottimizza(lds_sessione.getitemnumber(ll_riga, 1), lds_sessione.getitemnumber(ll_riga, 2), ls_errore)

next

if dw_ottimizza_1.rowcount() > 0 then
	id_lancio_produzione_ottimizzato = ls_record.id_padre

	// ordino per prodotto
	dw_ottimizza_1.setsort("cod_materia_prima, anno_registrazione , num_registrazione, prog_riga_ord_ven")
	dw_ottimizza_1.sort()

	dw_ottimizza_3.insertrow(0)
else
	dw_ottimizza_3.reset()
	id_lancio_produzione_ottimizzato = 0
	g_mb.warning("Non è stata trovato alcun elemento da ottimizzare; verificare la distinta base e l'anagrafica prodotti.")
end if
	

setpointer(Arrow!)
pcca.mdi_frame.setmicrohelp("Ottimizzazione Terminata")






end subroutine

public function integer wf_ottimizza (long al_anno_ordine, long al_num_ordine, ref string as_errore);// processo di ottimizzazione die tagli

string		ls_sql, ls_errore,ls_cod_prodotto,ls_cod_versione, ls_null, ls_vuoto[], ls_materie_prime[], ls_versione_materie_prime[], ls_cod_deposito_prelievo, &
			ls_cod_prodotto_padre, ls_cod_versione_padre, ls_cod_prodotto_figlio, ls_cod_versione_figlio, ls_cod_prodotto_variante, ls_cod_matricola, &
			ls_des_prodotto_finito, ls_flag_tipo_ottimizzazione

long		ll_ret, ll_i, ll_prog_riga_ord_ven, ll_j, ll_row, ll_cont, ll_dt, ll_y

dec{4}	ld_quan_ordine
double	ldd_quantita_utilizzo[], ldd_quan_ordine, ldd_vuoto[], ld_misura

datastore	lds_data, lds_varianti, lds_dt
uo_funzioni_1 luo_funzioni
uo_ottimizza_tagli 		luo_ottimizza

setnull(ls_null)

// preparo datastore per estrarre righe che hanno distinta base e soggette a generazione commessa
ls_sql = 	" select det_ord_ven.cod_prodotto, det_ord_ven.cod_versione, det_ord_ven.quan_ordine, det_ord_ven.prog_riga_ord_ven, anag_prodotti.des_prodotto from det_ord_ven " + &
			" left outer join anag_prodotti on anag_prodotti.cod_azienda = det_ord_ven.cod_azienda and anag_prodotti.cod_prodotto = det_ord_ven.cod_prodotto " + &
			" where det_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_ord_ven.anno_registrazione = " + string(al_anno_ordine) + " and det_ord_ven.num_registrazione = " + string(al_num_ordine) + " and det_ord_ven.flag_gen_commessa='S' and det_ord_ven.cod_versione is not null "
			
ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_ret < 0 then return -1

luo_funzioni = create uo_funzioni_1

for ll_i = 1 to ll_ret
	
	ls_cod_prodotto = lds_data.getitemstring(ll_i, 1)
	ls_cod_versione = lds_data.getitemstring(ll_i, 2)
	ld_quan_ordine = lds_data.getitemnumber(ll_i,3)
	ll_prog_riga_ord_ven = lds_data.getitemnumber(ll_i,4)
	ls_des_prodotto_finito = lds_data.getitemstring(ll_i, 5)
	
	if isnull(ls_cod_prodotto) then 
		g_mb.warning(g_str.format("Attenzione nell'ordine $1/$2 la riga $3 è priva di codice prodotto e sarà saltata!", al_anno_ordine, al_num_ordine,ll_prog_riga_ord_ven ))
		continue
	end if
	
	if isnull(ls_cod_versione) then 
		g_mb.warning(g_str.format("Attenzione il prodotto $1 relativo alla riga ordine $2-$3-$4 è privo di versione è verrà saltato!",ls_cod_prodotto, al_anno_ordine, al_num_ordine,ll_prog_riga_ord_ven))
		continue
	end if
	
	pcca.mdi_frame.setmicrohelp(g_str.format("Elaborazione ordine $1/$2 riga $3", al_anno_ordine, al_num_ordine, ll_prog_riga_ord_ven ))
	Yield()

	
	ldd_quan_ordine = double(ld_quan_ordine)

	if luo_funzioni.uof_trova_mat_prime_varianti(	ls_cod_prodotto, &
													ls_cod_versione,&
													ref ls_cod_deposito_prelievo, &
													ref ls_materie_prime[],  &
													ref ls_versione_materie_prime[], &
													ref ldd_quantita_utilizzo[], &
													ldd_quan_ordine, &
													al_anno_ordine, &
													al_num_ordine, &
													ll_prog_riga_ord_ven, &
													"varianti_det_ord_ven", &
													ls_null, &
													ls_errore) = -1 then
		as_errore = "Errore durante l'estrazione della disinta base: " + ls_errore 
		destroy luo_funzioni
		destroy lds_data
		return -1
	end if
	
	for ll_j = 1 to upperbound(ls_materie_prime[])
		
		if ldd_quantita_utilizzo[ll_j] > 0 then
			/*
			for ll_y = 1 to ldd_quan_ordine
		
				ll_row = dw_ottimizza_1.insertrow(0)
				
				dw_ottimizza_1.setitem(ll_row, "anno_registrazione",al_anno_ordine)
				dw_ottimizza_1.setitem(ll_row, "num_registrazione",al_num_ordine)
				dw_ottimizza_1.setitem(ll_row, "prog_riga_ord_ven",ll_prog_riga_ord_ven)
				
				dw_ottimizza_1.setitem(ll_row, "cod_prodotto_finito", ls_cod_prodotto )
				dw_ottimizza_1.setitem(ll_row, "des_prodotto_finito", ls_des_prodotto_finito )
				dw_ottimizza_1.setitem(ll_row, "cod_materia_prima", ls_materie_prime[ll_j] )
				dw_ottimizza_1.setitem(ll_row, "des_materia_prima", f_des_tabella ( "anag_prodotti", "cod_prodotto = '" + ls_materie_prime[ll_j] + "'" , "des_prodotto" ) )
				dw_ottimizza_1.setitem(ll_row, "quan_utilizzo", ldd_quantita_utilizzo[ll_j] / ldd_quan_ordine)
				
				// prendere la matricole da tabella det_ord_Ven_ologramma
				select 	cod_ologramma
				into		:ls_cod_matricola
				from		det_ord_ven_ologramma
				where		cod_azienda = :s_cs_xx.cod_azienda and
							anno_registrazione = :al_anno_ordine and
							num_registrazione = :al_num_ordine and
							prog_riga_ord_ven = :ll_prog_riga_ord_ven and
							progressivo = :ll_y;
				if sqlca.sqlcode = 0 then
					dw_ottimizza_1.setitem(ll_row, "matricola",ls_cod_matricola)
				else
					dw_ottimizza_1.setitem(ll_row, "matricola", "**ASSENTE**")
				end if
			next
			*/
			
			pcca.mdi_frame.setmicrohelp(g_str.format("Elaborazione ordine $1/$2 RIGA=$3 MP=$4", al_anno_ordine, al_num_ordine, ll_prog_riga_ord_ven, ls_materie_prime[ll_j] ))
			Yield()
		end if
	next
	
next

// §Ciclo per distinte di taglio


for ll_i = 1 to ll_ret
	
	ls_cod_prodotto = lds_data.getitemstring(ll_i, 1)
	ls_cod_versione = lds_data.getitemstring(ll_i, 2)
	ld_quan_ordine = lds_data.getitemnumber(ll_i,3)
	ll_prog_riga_ord_ven = lds_data.getitemnumber(ll_i,4)
	ls_des_prodotto_finito =  lds_data.getitemstring(ll_i, 5)
	
	ldd_quan_ordine = double(ld_quan_ordine)
	
	
	ls_sql =	" select cod_prodotto_padre, cod_versione, cod_prodotto_figlio,cod_versione_figlio, cod_prodotto " + &
	" from varianti_det_ord_ven " + &
	" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(al_anno_ordine) + " and num_registrazione = " + string(al_num_ordine) + " and prog_riga_ord_ven = " + string(ll_prog_riga_ord_ven)
		
	ll_cont = guo_functions.uof_crea_datastore(lds_varianti, ls_sql, as_errore)
	if ll_cont < 0 then return -1
	
	ls_materie_prime[] = ls_vuoto[]
	ldd_quantita_utilizzo[] = ldd_vuoto[]
	
	for ll_j = 1 to ll_cont
		
		ls_cod_prodotto_padre = lds_varianti.getitemstring(ll_j, 1)
		ls_cod_versione_padre = lds_varianti.getitemstring(ll_j, 2)
		ls_cod_prodotto_figlio = lds_varianti.getitemstring(ll_j, 3)
		ls_cod_versione_figlio = lds_varianti.getitemstring(ll_j, 4)
		ls_cod_prodotto_variante = lds_varianti.getitemstring(ll_j, 5)
		
		ls_sql = 	" select 	misura from distinte_taglio_calcolate " + &
					" where cod_azienda = '" + s_cs_xx.cod_azienda + "'  and anno_registrazione = " + string(al_anno_ordine) + " and num_registrazione = " + string(al_num_ordine) + &
					" and prog_riga_ord_ven = "+ string(ll_prog_riga_ord_ven) + " and " + &
					" cod_prodotto_padre = '"+ ls_cod_prodotto_padre + "' and " + &
					" cod_versione = '" + ls_cod_versione_padre + "' and " + &
					" cod_prodotto_figlio  = '" + ls_cod_prodotto_figlio + "' and " + &
					" cod_versione_figlio  = '" + ls_cod_versione_figlio + "' "  
					
		ll_dt = guo_functions.uof_crea_datastore(lds_dt, ls_sql, as_errore)
		if ll_dt < 0 then return -1
					
		for ll_y = 1 to ll_dt
			ld_misura = lds_dt.getitemnumber(ll_y, 1)
			if not isnull(ld_misura) and ld_misura > 0 then
				ls_materie_prime[upperbound(ls_materie_prime) + 1] = ls_cod_prodotto_variante
				ldd_quantita_utilizzo[upperbound(ldd_quantita_utilizzo) + 1] = ld_misura
			end if
		next
			
	next
	
	for ll_j = 1 to upperbound(ls_materie_prime[])
		
		select flag_tipo_ottimizzazione
		into :ls_flag_tipo_ottimizzazione
		from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
		cod_prodotto = :ls_materie_prime[ll_j];
		
		if ls_flag_tipo_ottimizzazione='0' then continue
		
		if ldd_quantita_utilizzo[ll_j] > 0 then
			
			for ll_y = 1 to ldd_quan_ordine
		
				ll_row = dw_ottimizza_1.insertrow(0)
				
				
				
				dw_ottimizza_1.setitem(ll_row, "anno_registrazione",al_anno_ordine)
				dw_ottimizza_1.setitem(ll_row, "num_registrazione",al_num_ordine)
				dw_ottimizza_1.setitem(ll_row, "prog_riga_ord_ven",ll_prog_riga_ord_ven)
		
				dw_ottimizza_1.setitem(ll_row, "cod_prodotto_finito", ls_cod_prodotto )
				dw_ottimizza_1.setitem(ll_row, "des_prodotto_finito", ls_des_prodotto_finito )
				dw_ottimizza_1.setitem(ll_row, "cod_materia_prima", ls_materie_prime[ll_j] )
				dw_ottimizza_1.setitem(ll_row, "des_materia_prima", f_des_tabella ( "anag_prodotti", "cod_prodotto = '" + ls_materie_prime[ll_j] + "'" , "des_prodotto" ) )
	//			dw_ottimizza_1.setitem(ll_row, "quan_utilizzo", ldd_quantita_utilizzo[ll_j] )
			
				dw_ottimizza_1.setitem(ll_row, "quan_utilizzo", ldd_quantita_utilizzo[ll_j] / ldd_quan_ordine)
				
				// prendere la matricole da tabella det_ord_Ven_ologramma
				select 	cod_ologramma
				into		:ls_cod_matricola
				from		det_ord_ven_ologramma
				where		cod_azienda = :s_cs_xx.cod_azienda and
							anno_registrazione = :al_anno_ordine and
							num_registrazione = :al_num_ordine and
							prog_riga_ord_ven = :ll_prog_riga_ord_ven and
							progressivo = :ll_y;
				if sqlca.sqlcode = 0 then
					dw_ottimizza_1.setitem(ll_row, "matricola",ls_cod_matricola)
				else
					dw_ottimizza_1.setitem(ll_row, "matricola", "**ASSENTE**")
				end if
			next
			pcca.mdi_frame.setmicrohelp(g_str.format("Elaborazione disinta taglio ordine $1/$2 RIGA=$3 MP=$4", al_anno_ordine, al_num_ordine, ll_prog_riga_ord_ven, ls_materie_prime[ll_j] ))
			Yield()
		end if
	next
	
next

destroy luo_funzioni
destroy lds_data

return 0
end function

public subroutine wf_carica_lista_lancio_prod ();string		ls_errore
if tv_lancio.uof_carica_tv( dw_gg_lancio.getitemnumber(1,1), ls_errore ) < 0 then
	g_mb.error(ls_errore)
end if

end subroutine

public function integer f_report_3_plastinds (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_reparto);// Funzione che compila la datawindow di tipo report_3 (Tende Tecniche)
// nome: f_report_3
// tipo: integer
//       -1 failed
//  		 0 passed
//
//
//		Creata il 09/05/2016 
//		Autore Enrico Menegotto


string			ls_cod_cliente,ls_rag_soc_1,ls_rag_soc_2,ls_indirizzo,ls_cap,ls_localita,ls_provincia,ls_telefono,ls_fax, ls_null, &
				ls_cod_operatore,ls_cod_giro_consegna,ls_des_reparto,ls_flag_allegati,ls_cod_prodotto,ls_des_prodotto, & 
				ls_nota_dettaglio,ls_cod_misura,ls_cod_misura_mag,ls_cod_misura_ven,ls_cod_non_a_magazzino,ls_cod_prod_finito,&
				ls_cod_tessuto,ls_cod_tessuto_mant,ls_flag_tipo_mantovana,ls_flag_cordolo,ls_flag_passamaneria,  & 
				ls_cod_verniciatura,ls_cod_comando,ls_pos_comando,ls_cod_colore_lastra,ls_cod_tipo_lastra,ls_des_giro_consegna,&
				ls_cod_tipo_supporto,ls_flag_misura_luce_finita,ls_nota_configuratore,ls_colore_passamaneria,ls_colore_cordolo,&
				ls_des_vernice_fc,ls_des_tessuto,ls_des_comando,ls_des_breve_vernice,ls_giorno,&
				ls_des_comando_1,ls_flag_tassativo, ls_alias, ls_errore, ls_bfo,ls_num_ord_cliente,ls_rif_interscambio,&
				ls_dest_test, ls_rs1_test, ls_rs2_test, ls_ind_test, ls_loc_test, ls_prov_test, ls_cap_test, ls_fraz_test, ls_nota_prodotto, &
				ls_flag_urgente, ls_des_verniciatura, ls_lista_reparti_coinvolti, ls_stabilim_prossimo, ls_reparto_prossimo, ls_chiave_etic, &
				ls_nota_testata,ls_nota_piede, ls_nota_video, ls_cod_imballo, ls_des_imballo, ls_des_variabile, ls_cod_lingua, ls_valore_variabile, &
				ls_flag_asta, ls_logo, ls_cod_tipo_binario, ls_des_prodotto_mag,ls_tipo_guida, ls_flag_nascondi_cliente

integer		li_giorno_settimana, li_risposta, li_bco

long			ll_num_commessa,ll_num_righe,ll_prog_riga_ord_ven,ll_num_riga_appartenenza,ll_progr_tes_produzione,ll_cont_urgente, ll_ret

dec{4}		ldd_quan_ordine,ldd_fat_conversione_ven,ldd_quan_vendita,ldd_dim_x,ldd_dim_y, & 
				ldd_dim_z,ldd_dim_t,ldd_alt_mantovana
dec{4}		ld_dim_x_riga, ld_dim_y_riga, ld_imponibile_ordine, ld_imponibile_riga, ld_valore_variabile
			
datetime		ldt_data_consegna, ldt_data_pronto,ldt_data_pronto_fasi, ldt_data_registrazione

boolean		lb_prima_riga=true

uo_produzione luo_prod

setnull(ls_null)
//Donato 12/03/2012: no filigrana per tipo report 3
dw_report.modify("DataWindow.brushmode=0")

dw_report.Modify("t_azienda.text='"+wf_logo()+"'")


select data_pronto,
		 data_consegna,
		 cod_cliente,
		 cod_giro_consegna,
		 cod_operatore,
		 flag_tassativo,
		 alias_cliente,
		 cod_des_cliente,
		 rag_soc_1,
		 rag_soc_2,
		 indirizzo,
		 localita,
		 frazione,
		 cap,
		 provincia,
		 num_ord_cliente,
		 rif_interscambio,
		 cod_imballo,
		 imponibile_iva,
		 data_registrazione
into   :ldt_data_pronto,
		 :ldt_data_consegna,
		 :ls_cod_cliente,
		 :ls_cod_giro_consegna,
		 :ls_cod_operatore,
		 :ls_flag_tassativo,
		 :ls_alias,
		 :ls_dest_test,
		 :ls_rs1_test,
		 :ls_rs2_test,
		 :ls_ind_test,
		 :ls_loc_test,
		 :ls_fraz_test,
		 :ls_cap_test,
		 :ls_prov_test,
		 :ls_num_ord_cliente,
		 :ls_rif_interscambio,
		 :ls_cod_imballo,
		 :ld_imponibile_ordine,
		 :ldt_data_registrazione
from   tes_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura tes_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

// logo del cliente se esiste, altrimenti LOGO standard
ls_logo = s_cs_xx.volume + s_cs_xx.risorse + ls_cod_cliente + ".jpg"
if not FileExists ( ls_logo ) then
	guo_functions.uof_get_parametro_azienda( "LO8", ls_logo)
	ls_logo = s_cs_xx.volume + s_cs_xx.risorse + ls_logo
end if	
dw_report.Object.p_logo.Filename=ls_logo



select sigla, 
		flag_nascondi_cliente
into   :ls_des_reparto,
		:ls_flag_nascondi_cliente
from   anag_reparti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_reparto=:fs_cod_reparto;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura anag_reparti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select des_imballo
into   :ls_des_imballo
from   tab_imballi
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_imballo=:ls_cod_imballo;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tabella imballaggi. Dettaglio Errore: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

 
 
select rag_soc_1,
		 rag_soc_2,
		 indirizzo,
		 cap,
		 localita,
		 provincia,
		 telefono,
		 fax,
		 cod_lingua
into   :ls_rag_soc_1,
		 :ls_rag_soc_2,
		 :ls_indirizzo,
		 :ls_cap,
		 :ls_localita,
		 :ls_provincia,
		 :ls_telefono,
		 :ls_fax,
		 :ls_cod_lingua
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura anag_clienti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if


dw_report.Modify("num_ordine.text='"+ string(fl_num_registrazione)+ "'")

wf_set_deposito_origine(fi_anno_registrazione, fl_num_registrazione, dw_report)

if isnull(ll_num_commessa) then
	dw_report.Modify("num_commessa.text=' '")
else
	dw_report.Modify("num_commessa.text='"+ string(ll_num_commessa) + "'")
end if

if isnull(ls_cod_giro_consegna) then
	dw_report.Modify("des_giro.text=' '")
else
	dw_report.Modify("des_giro.text='"+ ls_des_giro_consegna + "'")
end if

ls_rag_soc_1 = wf_elabora(ls_rag_soc_1)

dw_report.Modify("cod_cliente.text='"+ ls_cod_cliente + "'")
if ls_flag_nascondi_cliente = "N" then 
	dw_report.Modify("rag_soc_1.text='" + ls_rag_soc_1 + "'")
else
	dw_report.Modify("rag_soc_1.text=''")
end if


if isnull(ls_alias) then
	dw_report.Modify("rag_soc_2.text=' '")
else
	ls_alias = wf_elabora(ls_alias)
	if ls_flag_nascondi_cliente = "N" then 
		dw_report.Modify("rag_soc_2.text='"+ ls_alias + "'")
	else
		dw_report.Modify("rag_soc_2.text=''")
	end if
end if
	
if isnull(ls_indirizzo) then
	dw_report.Modify("indirizzo.text=' '")
else
	ls_indirizzo = wf_elabora(ls_indirizzo)
	dw_report.Modify("indirizzo.text='"+ ls_indirizzo + "'")
end if

if isnull(ls_cap) then ls_cap = ' '
dw_report.Modify("cap.text='"+ ls_cap + "'")


if isnull(ls_localita) then ls_localita = ' '
ls_localita = wf_elabora(ls_localita)
dw_report.Modify("localita.text='"+ ls_localita + "'")

if isnull(ls_provincia) then ls_provincia = ' '
dw_report.Modify("provincia.text='"+ ls_provincia + "'")

if isnull(ls_telefono) then ls_telefono = ' '
dw_report.Modify("telefono.text='"+ ls_telefono + "'")

if isnull(ls_fax) then ls_fax = ' '
dw_report.Modify("fax.text='"+ ls_fax + "'")

if isnull(ls_cod_operatore) then ls_cod_operatore = ' '
dw_report.Modify("cod_operatore.text='"+ ls_cod_operatore + "'")

if isnull(ls_des_reparto) then ls_des_reparto=' '
dw_report.Modify("des_reparto.text='"+ ls_des_reparto + "'")


if isnull(ls_num_ord_cliente) then ls_num_ord_cliente = ' '
dw_report.Modify("num_ord_cliente.text='"+ ls_num_ord_cliente + "'")

if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "STAMPA_ORD_PROD", ls_null, ldt_data_consegna, ls_nota_testata,ls_nota_piede, ls_nota_video) < 0 then
	g_mb.error(ls_nota_testata)
end if
//f_crea_nota_fissa (ls_cod_cliente, ls_null, "STAMPA_ORD_PROD", ldt_data_consegna, ls_nota_testata,ls_nota_piede, ls_nota_video )

//VISUALIZZA LINGUA DEL CLIENTE
//##############################################
if not isnull(ls_cod_lingua) and ls_cod_lingua<>"" then
	dw_report.Modify("test_barcode_t.text='Lingua Cliente: "+ ls_cod_lingua + "'")
else
	dw_report.Modify("test_barcode_t.text=''")
end if
//##############################################


// ************************************** IMPOSTAZIONE CODICE A BARRE PRODUZIONE
li_risposta = f_font_barcode(ls_bfo,li_bco,ls_errore)
if li_risposta < 0 then 
	messagebox("SEP","Report 3 Tende Tecniche: " + ls_errore,stopsign!)
	return -1
end if


select 	progr_tes_produzione
into   		:ll_progr_tes_produzione
from   	tes_ordini_produzione
where  	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_registrazione and
			num_registrazione=:fl_num_registrazione and
			cod_reparto=:fs_cod_reparto;

choose case sqlca.sqlcode
	
	case 0
		
		dw_report.Modify("barcode.text='*"+is_CSB+ string(ll_progr_tes_produzione) +is_CSB+ "*'")
		dw_report.Modify("barcode.Font.Face='" + ls_bfo + "'")
		dw_report.Modify("barcode.Font.Height= -" + string(li_bco) )
		
	case is < 0
		messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura tabella tes_ordini_produzione. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	
	case 100
		dw_report.Modify("barcode.text=''")
		
end choose


// ************************************** IMPOSTAZIONE CODICE A BARRE SPEDIZIONE
dw_report.Modify("barcode_spedizione.text='*"+is_CSB+ "O" + string(fi_anno_registrazione,"0000") + string(fl_num_registrazione,"000000") +is_CSB+ "*'")
dw_report.Modify("barcode_spedizione.Font.Face='" + ls_bfo + "'")
dw_report.Modify("barcode_spedizione.Font.Height= -" + string(li_bco) )

dw_report.Modify("data_pronto.text='"+ string(date(ldt_data_pronto)) +"'")

// ************************************** FINE IMPOSTAZIONE CODICE A BARRE


declare 	r_det_stampa_ordini cursor for 
select  	prog_riga_ord_ven
from    	det_stampa_ordini
where   	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_registrazione and
			num_registrazione=:fl_num_registrazione and
			cod_reparto=:fs_cod_reparto and
			prog_riga_comanda=0 and prog_sessione=:il_sessione;

open r_det_stampa_ordini;
if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante il open cursore r_det_stampa_ordini (f_report_3)~r~n" + sqlca.sqlerrtext,stopsign!)
	return -1
end if


do while true
	fetch r_det_stampa_ordini
	into  :ll_prog_riga_ord_ven;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then 
		messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura cursore det_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		close r_det_stampa_ordini;
		return -1
	end if

	declare r_det_ord_ven_3 cursor for
	select cod_prodotto,
			 des_prodotto,
			 cod_misura,
			 quan_ordine,
			 nota_dettaglio,
			 num_commessa,
			 num_riga_appartenenza,
			 nota_prodotto,
			 flag_urgente,
			 dim_x,
			 dim_y,
			 imponibile_iva
	from   det_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda and
			 anno_registrazione=:fi_anno_registrazione and
			 num_registrazione=:fl_num_registrazione and
			(num_riga_appartenenza=:ll_prog_riga_ord_ven or prog_riga_ord_ven=:ll_prog_riga_ord_ven)
	order by prog_riga_ord_ven;

	open r_det_ord_ven_3;
	
	do while true
		fetch r_det_ord_ven_3
		into  :ls_cod_prodotto,
				:ls_des_prodotto,
				:ls_cod_misura,
				:ldd_quan_ordine,
				:ls_nota_dettaglio,
				:ll_num_commessa,
				:ll_num_riga_appartenenza,
				:ls_nota_prodotto,
				:ls_flag_urgente,
				:ld_dim_x_riga,
				:ld_dim_y_riga,
				:ld_imponibile_riga;

		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_det_ord_ven_3;
			close r_det_stampa_ordini;
			return -1
		end if

		if sqlca.sqlcode = 100 then exit
		
		select cod_misura_mag,
				 cod_misura_ven,
				 fat_conversione_ven,
				 des_prodotto
		into   :ls_cod_misura_mag,
				 :ls_cod_misura_ven,
				 :ldd_fat_conversione_ven,
				 :ls_des_prodotto_mag
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto;

		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			close r_det_stampa_ordini;
			return -1
		end if
	
		if ll_num_riga_appartenenza = 0 then
			ll_ret = wf_get_variabile_conf(fi_anno_registrazione, fl_num_registrazione,ll_prog_riga_ord_ven, "dim_01", ls_valore_variabile, ldd_dim_x, ls_errore)
			if ll_ret=1 then
				ldd_dim_x = 0
			elseif ll_ret < 0 then
				g_mb.error(ls_errore)
				return -1
			end if

			ll_ret = wf_get_variabile_conf(fi_anno_registrazione, fl_num_registrazione,ll_prog_riga_ord_ven, "dim_02", ls_valore_variabile, ldd_dim_y, ls_errore)
			if ll_ret=1 then
				ldd_dim_y = 0
			elseif ll_ret < 0 then
				g_mb.error(ls_errore)
				return -1
			end if

			// posizione del comando
			ll_ret = wf_get_variabile_conf(fi_anno_registrazione, fl_num_registrazione,ll_prog_riga_ord_ven, "poscom", ls_pos_comando, ld_valore_variabile, ls_errore)
			if ll_ret=1 then
				setnull(ls_pos_comando)
			elseif ll_ret < 0 then
				g_mb.error(ls_errore)
				return -1
			end if

			// colore tessuto/lamella
			ll_ret = wf_get_variabile_conf(fi_anno_registrazione, fl_num_registrazione,ll_prog_riga_ord_ven, "codcol", ls_cod_non_a_magazzino, ld_valore_variabile, ls_errore)
			if ll_ret=1 then
				setnull(ls_cod_non_a_magazzino)
			elseif ll_ret < 0 then
				g_mb.error(ls_errore)
				return -1
			end if

			// tipo tessuto/lamella
			ll_ret = wf_get_variabile_conf(fi_anno_registrazione, fl_num_registrazione,ll_prog_riga_ord_ven, "nomtes", ls_des_tessuto, ld_valore_variabile, ls_errore)
			if ll_ret=1 then
				ls_des_tessuto=""
			elseif ll_ret < 0 then
				g_mb.error(ls_errore)
				return -1
			end if

			// presenza asta
			ll_ret = wf_get_variabile_conf(fi_anno_registrazione, fl_num_registrazione,ll_prog_riga_ord_ven, "flasta", ls_flag_asta, ld_valore_variabile, ls_errore)
			if ll_ret=1 then
				ls_flag_asta=""
			elseif ll_ret < 0 then
				g_mb.error(ls_errore)
				return -1
			end if

			// codice motore
			ll_ret = wf_get_variabile_conf(fi_anno_registrazione, fl_num_registrazione,ll_prog_riga_ord_ven, "commot", ls_cod_comando, ld_valore_variabile, ls_errore)
			if ll_ret=1 then
				ls_cod_comando=""
			elseif ll_ret < 0 then
				g_mb.error(ls_errore)
				return -1
			end if

			// tipo supporto
			ll_ret = wf_get_variabile_conf(fi_anno_registrazione, fl_num_registrazione,ll_prog_riga_ord_ven, "tipsup", ls_cod_tipo_supporto, ld_valore_variabile, ls_errore)
			if ll_ret=1 then
				ls_cod_tipo_supporto=""
			elseif ll_ret < 0 then
				g_mb.error(ls_errore)
				return -1
			end if

			// tipo binario
			ll_ret = wf_get_variabile_conf(fi_anno_registrazione, fl_num_registrazione,ll_prog_riga_ord_ven, "tipbin", ls_cod_tipo_binario, ld_valore_variabile, ls_errore)
			if ll_ret=1 then
				ls_cod_tipo_binario=""
			elseif ll_ret < 0 then
				g_mb.error(ls_errore)
				return -1
			end if

			//misure luce/finita
			ll_ret = wf_get_variabile_conf(fi_anno_registrazione, fl_num_registrazione,ll_prog_riga_ord_ven, "misura", ls_flag_misura_luce_finita, ld_valore_variabile, ls_errore)
			if ll_ret=1 then
				ls_flag_misura_luce_finita=""
			elseif ll_ret < 0 then
				g_mb.error(ls_errore)
				return -1
			end if

			// presenza guide
			ll_ret = wf_get_variabile_conf(fi_anno_registrazione, fl_num_registrazione,ll_prog_riga_ord_ven, "fguida", ls_tipo_guida, ld_valore_variabile, ls_errore)
			if ll_ret=1 then
				setnull(ls_tipo_guida)
			elseif ll_ret < 0 then
				g_mb.error(ls_errore)
				return -1
			end if
		end if
		
		select cod_comodo
		into   :ls_des_breve_vernice
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_verniciatura;
	
		if sqlca.sqlcode < 0 then 
			messagebox("Apice","Errore durante la compilazione del report_3 (f_report_3), lettura anag_prodotti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
			return -1
		end if
	
		ll_num_righe++
		dw_report.insertrow(ll_num_righe)
		
		dw_report.setitem(ll_num_righe,"riga",ll_prog_riga_ord_ven)
		dw_report.setitem(ll_num_righe,"tipo_riga",1)
		
		dw_report.setitem(ll_num_righe,"codice_prodotto",ls_cod_prodotto)	
		
		dw_report.setitem(ll_num_righe,"flag_tassativo",ls_flag_tassativo)
		
		
		dw_report.setitem(ll_num_righe,"rag_soc_1_destinazione",ls_rs1_test)
		dw_report.setitem(ll_num_righe,"indirizzo_destinazione",ls_ind_test)
		dw_report.setitem(ll_num_righe,"localita_destinazione",ls_loc_test)
		dw_report.setitem(ll_num_righe,"cap_destinazione",ls_cap_test)
		dw_report.setitem(ll_num_righe,"provincia_destinazione",ls_prov_test)
		
		dw_report.setitem(ll_num_righe,"totale_imponibile",ld_imponibile_ordine)
		dw_report.setitem(ll_num_righe,"imponibile_riga",ld_imponibile_riga)
	
		dw_report.setitem(ll_num_righe,"data_ordine", ldt_data_registrazione)
		dw_report.setitem(ll_num_righe,"rif_interscambio",ls_rif_interscambio)
		// stefanop 10/06/2010: progetto 140: accodo la descrizione tessuto alla descrizione
		// VERNICIATURA
		select des_prodotto
		into :ls_des_verniciatura
		from anag_prodotti
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_verniciatura;
		
		if sqlca.sqlcode <> 0 or isnull(ls_des_verniciatura) then ls_des_verniciatura = ""
		
		if not isnull(ls_des_tessuto) or ls_des_tessuto <> "" then
			ls_des_prodotto += " - " + ls_des_tessuto
		end if
		ls_des_prodotto += " - " + ls_des_verniciatura
		dw_report.setitem(ll_num_righe,"des_prodotto",ls_des_prodotto)
		
		// CODICE ORIGINALE
		//dw_report.setitem(ll_num_righe,"des_prodotto",ls_des_prodotto)
		
		// 27.04.2017 (EnMe)  aggiunto su indicazioni di Simone -----
		dw_report.setitem(ll_num_righe,"des_tessuto",ls_des_tessuto)
		// ----
		dw_report.setitem(ll_num_righe,"cod_colore",ls_cod_non_a_magazzino)
		dw_report.setitem(ll_num_righe,"um_mag",ls_cod_misura_mag)
		dw_report.setitem(ll_num_righe,"quan_mag",ldd_quan_ordine)
		dw_report.setitem(ll_num_righe,"larghezza",ldd_dim_x)
		dw_report.setitem(ll_num_righe,"altezza",ldd_dim_y)
		dw_report.setitem(ll_num_righe,"luce_finita",ls_flag_misura_luce_finita)
		dw_report.setitem(ll_num_righe,"posizione_comando",ls_pos_comando)
		dw_report.setitem(ll_num_righe,"tipo_comando",ls_cod_comando)
		dw_report.setitem(ll_num_righe,"altezza_comando",ls_flag_asta)
		dw_report.setitem(ll_num_righe,"guide",ls_tipo_guida)
		
		dw_report.setitem(ll_num_righe,"des_tipo_supporto",ls_cod_tipo_supporto)
		dw_report.setitem(ll_num_righe,"des_tipo_vernice",ls_des_breve_vernice)
		dw_report.setitem(ll_num_righe,"num_commessa",ll_num_commessa)
		dw_report.setitem(ll_num_righe,"allegato",ls_flag_allegati)
		dw_report.setitem(ll_num_righe,"flag_urgente",ls_flag_urgente)
		dw_report.setitem(ll_num_righe,"flag_tassativo",ls_flag_tassativo)
		dw_report.setitem(ll_num_righe,"tipo_binario",ls_cod_tipo_binario)

		
		if isnull(ls_nota_dettaglio) then ls_nota_dettaglio = ""
		
		if not isnull(ls_nota_dettaglio) and len(trim(ls_nota_dettaglio)) > 0 then
//			ll_num_righe++
//			dw_report.insertrow(ll_num_righe)
//			dw_report.setitem(ll_num_righe,"tipo_riga",0)
			if len(trim(ls_nota_prodotto)) > 0 then ls_nota_dettaglio = g_str.format("$1 [$3] $2",ls_nota_dettaglio,ls_nota_prodotto, ls_cod_prodotto)
			if ll_num_riga_appartenenza > 0 then
				dw_report.setitem(ll_num_righe,"nota",ls_nota_dettaglio)
			else
				dw_report.setitem(ll_num_righe,"des_prodotto",ls_nota_dettaglio)
			end if
		else
			if len(trim(ls_des_prodotto)) > 0 then
				if len(trim(ls_nota_prodotto)) > 0 then
					dw_report.setitem(ll_num_righe,"nota", g_str.format("$1 [$3] $2",ls_des_prodotto,ls_nota_prodotto, ls_cod_prodotto) )
				else
					dw_report.setitem(ll_num_righe,"nota", g_str.format("$1 [$2]", ls_des_prodotto, ls_cod_prodotto) )
				end if					
			else
				if len(trim(ls_nota_prodotto)) > 0 then
					dw_report.setitem(ll_num_righe,"nota", g_str.format("$1 [$3] $2",ls_des_prodotto_mag,ls_nota_prodotto, ls_cod_prodotto) )
				else
					dw_report.setitem(ll_num_righe,"nota", g_str.format("$1 [$2]",ls_des_prodotto_mag, ls_cod_prodotto) )
				end if					
			end if
		end if

/*
		if not isnull(ls_nota_prodotto) and ls_nota_prodotto<>"" then
			ll_num_righe++
			dw_report.insertrow(ll_num_righe)
			dw_report.setitem(ll_num_righe,"tipo_riga",0)
			dw_report.setitem(ll_num_righe,"nota",ls_nota_prodotto)
		end if


		if not isnull(ls_des_vernice_fc) and ls_des_vernice_fc<>"" then
			ll_num_righe++
			dw_report.insertrow(ll_num_righe)
			dw_report.setitem(ll_num_righe,"tipo_riga",0)
			dw_report.setitem(ll_num_righe,"nota",ls_des_vernice_fc)
		end if

		if not isnull(ls_des_comando_1) and ls_des_comando_1<>"" then
			ll_num_righe++
			dw_report.insertrow(ll_num_righe)
			dw_report.setitem(ll_num_righe,"tipo_riga",0)
			dw_report.setitem(ll_num_righe,"nota",ls_des_comando_1)
		end if
*/
		setnull(ls_cod_non_a_magazzino)
		setnull(ls_cod_prod_finito)
		setnull(ldd_dim_x)
		setnull(ldd_dim_y)
		setnull(ldd_dim_z)
		setnull(ldd_dim_t)
		setnull(ls_cod_tessuto)
		setnull(ls_des_tessuto)
		setnull(ls_cod_tessuto_mant)
		setnull(ls_flag_tipo_mantovana)
		setnull(ldd_alt_mantovana)
		setnull(ls_flag_cordolo)
		setnull(ls_flag_passamaneria)
		setnull(ls_cod_verniciatura)
		setnull(ls_cod_comando)
		setnull(ls_pos_comando)
		setnull(ls_des_comando)
//		setnull(ldd_alt_asta)
		setnull(ls_flag_asta)
		setnull(ls_cod_colore_lastra)
		setnull(ls_cod_tipo_lastra)
		setnull(ls_cod_tipo_supporto)
		setnull(ls_flag_misura_luce_finita)
		setnull(ls_nota_configuratore)
		setnull(ls_colore_passamaneria)
		setnull(ls_colore_cordolo)
//		setnull(ls_des_breve_tipo_supporto)
		setnull(ls_des_vernice_fc)
		setnull(ls_cod_prodotto)
		setnull(ls_des_prodotto)
		setnull(ls_cod_misura_mag)
		setnull(ldd_quan_ordine)
		setnull(ls_nota_dettaglio)
		setnull(ll_num_commessa)
		setnull(ls_cod_misura_mag)
		setnull(ls_cod_misura_ven)
		setnull(ldd_fat_conversione_ven)
		setnull(ls_des_comando_1)
		setnull(ls_des_breve_vernice)
		setnull(ls_cod_tipo_binario)
		setnull(ls_des_prodotto_mag)
		
	loop
	
	close r_det_ord_ven_3;
	
	//Donato 27/12/2011
	//entro in questo IF solo la prima volta --------------------
	/*
	if lb_prima_riga then
		lb_prima_riga = false
		
		//info aggiuntive su report OL (stabilimento successivo e lista reparti coinvolti)
		luo_prod = create uo_produzione
		luo_prod.il_sessione_tes_stampa = il_sessione
		
		li_risposta = luo_prod.uof_get_ordine_reparti_2(	fi_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven, fs_cod_reparto, &
																ls_lista_reparti_coinvolti, ls_stabilim_prossimo, ls_reparto_prossimo)
		destroy luo_prod;
		
		//se la stampa si riferisce all'ultimo reparto della lista cambia la label dello stabilimento successivo
		try
			if li_risposta = 1 then
				//ultimo reparto
				dw_report.object.stab_succ_label_t.text = "Stabilim. partenza merce"
			else
				dw_report.object.stab_succ_label_t.text = "Stabilim. successivo"
			end if
		catch (runtimeerror e)
		end try
		
		dw_report.Modify("stab_succ_t.text='"+ls_stabilim_prossimo+"'")
		dw_report.Modify("reparti_ordine_t.text='"+ls_lista_reparti_coinvolti+"'")
	end if
	*/
	//---------------------------------------------------------------
	
loop		
			
close r_det_stampa_ordini;

return 0



end function

public function integer f_report_schede_distinta (long al_job, long al_anno_ordine, long al_num_ordine, ref string as_errore);/* stampa schede distinta.

VIENE ESAMINATA OGNI RIGA E PER OGNUNA SI PROVVEDE ALLA STAMPA DELLA DISINTA BASE .
SE MANCA LA DISTINTA NON FACCIO NULLA
LUPO 22-6-2016   SPECIFICA REPORT-OL REV 2
*/

string 		ls_sql, ls_cod_prodotto, ls_cod_versione, ls_flag_blocco
long			ll_rows, ll_i, ll_prog_riga_ord_ven
datastore 	lds_righe

ls_sql = g_str.format("select prog_riga_ord_ven, cod_prodotto, cod_versione from det_ord_ven where cod_azienda = '$1' and anno_registrazione = $2 and num_registrazione = $3 order by prog_riga_ord_ven ",s_cs_xx.cod_azienda, al_anno_ordine, al_num_ordine)

ll_rows = guo_functions.uof_crea_datastore(lds_righe, ls_sql)
if ll_rows < 0 then
	as_errore = "Errore in creazione Datastore righe ordine "
	return -1
end if

for ll_i = 1 to ll_rows
	
	ll_prog_riga_ord_ven = lds_righe.getitemnumber(ll_i, 1)
	ls_cod_prodotto = lds_righe.getitemstring(ll_i, 2)
	ls_cod_versione = lds_righe.getitemstring(ll_i, 3)
	
	select flag_blocco
	into	:ls_flag_blocco
	from	distinta_padri
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto and
			cod_versione = :ls_cod_versione;
	if sqlca.sqlcode = 100 then 
		continue
	elseif sqlca.sqlcode < 0 then
		as_errore = g_str.format("Errore in ricerca disinta padri $1", sqlca.sqlerrtext)
		return -1
	end if
	
	if ls_flag_blocco = "S" then continue
	
	if f_report_schede_distinta_singola_riga( al_job, al_anno_ordine, al_num_ordine, ll_prog_riga_ord_ven, ref as_errore) < 0 then
		return -1
	end if
	
next



return 0
end function

public function integer f_report_schede_distinta_singola_riga (long al_job, long al_anno_ordine, long al_num_ordine, long al_prog_riga_ord_ven, ref string as_errore);string 		ls_sql, ls_cod_prodotto, ls_cod_versione, ls_flag_blocco, ls_cod_tipo_comm, ls_des_tipo_comm
long			ll_ret, ll_i, ll_prog_riga_ord_ven, ll_rows, ll_riga
dec{4}		ld_quan_distinta,ld_quan_ordine,ld_quan_prodotta
datastore 	lds_commessa, lds_distinta


lds_commessa = create datastore
lds_commessa.dataobject = 'd_report_2_distinta_datastore'
lds_commessa.settransobject(sqlca)
ll_ret = lds_commessa.retrieve(s_cs_xx.cod_azienda, al_anno_ordine, al_num_ordine, al_prog_riga_ord_ven)

dw_report.reset()
dw_report.dataobject = 'd_report_2_distinta'

ls_sql = g_str.format("select cod_prodotto_figlio, cod_misura_mag, des_prodotto, quan_utilizzo from distinta join anag_prodotti on anag_prodotti.cod_azienda = distinta.cod_azienda and anag_prodotti.cod_prodotto = distinta.cod_prodotto_figlio where distinta.cod_azienda = '$1' and cod_prodotto_padre = '$2' and cod_versione = '$3' order by num_sequenza ",s_cs_xx.cod_azienda, lds_commessa.getitemstring(1, "cod_prodotto"), lds_commessa.getitemstring(1, "cod_versione") )

ll_rows = guo_functions.uof_crea_datastore(lds_distinta, ls_sql)

if ll_rows = 0 then return 0

if ll_rows < 0 then
	as_errore = g_str.format("Errore in caricamento datastore lds_distinta. $1",ls_sql)
	return -1
end if

for ll_i = 1 to ll_rows

	ll_riga = dw_report.insertrow(0)
	
	ld_quan_distinta = lds_distinta.getitemnumber(ll_i,4)
	if isnull(ld_quan_distinta) then ld_quan_distinta = 0
	ld_quan_ordine = lds_commessa.getitemnumber(1,"quan_ordine")
	if isnull(ld_quan_ordine) then ld_quan_ordine = 0
	ld_quan_prodotta = lds_commessa.getitemnumber(1,"quan_prodotta")
	if isnull(ld_quan_prodotta) then ld_quan_prodotta = 0
	
	dw_report.setitem(ll_riga,"cod_mp", lds_distinta.getitemstring(ll_i,1) )
	dw_report.setitem(ll_riga,"des_prodotto_mp",  lds_distinta.getitemstring(ll_i,3) )
	dw_report.setitem(ll_riga,"cod_misura_mp",  lds_distinta.getitemstring(ll_i,2) )
	dw_report.setitem(ll_riga,"quan_impegno", ld_quan_distinta * ( ld_quan_ordine - ld_quan_prodotta) )
	dw_report.setitem(ll_riga,"flag_tipo_avanzamento", lds_commessa.getitemstring(1,"flag_tipo_avanzamento") )
	dw_report.setitem(ll_riga,"cod_operatore", lds_commessa.getitemstring(1,"cod_operatore") )

	dw_report.setitem(ll_riga,"cod_tipo_mov_prel_mat_prime", lds_commessa.getitemstring(1,"cod_tipo_mov_prel_mat_prime") )
	dw_report.setitem(ll_riga,"cod_tipo_mov_ver_prod_finiti", lds_commessa.getitemstring(1,"cod_tipo_mov_ver_prod_finiti") )
	dw_report.setitem(ll_riga,"cod_deposito_versamento", lds_commessa.getitemstring(1,"cod_deposito_versamento") )
	dw_report.setitem(ll_riga,"cod_deposito_prelievo", lds_commessa.getitemstring(1,"cod_deposito_prelievo") )
	dw_report.setitem(ll_riga,"quan_prodotta", ld_quan_prodotta )
	dw_report.setitem(ll_riga,"quan_ordine", ld_quan_ordine )
	dw_report.setitem(ll_riga,"cod_versione_finito", lds_commessa.getitemstring(1, "cod_versione") )
	dw_report.setitem(ll_riga,"cod_prodotto_finito", lds_commessa.getitemstring(1, "cod_prodotto") )
	dw_report.setitem(ll_riga,"des_prodotto_finito", lds_commessa.getitemstring(1, "des_prodotto") )
	dw_report.setitem(ll_riga,"cod_misura_finito", lds_commessa.getitemstring(1, "cod_misura_mag") )

	dw_report.setitem(ll_riga,"data_consegna", lds_commessa.getitemdatetime(1,"data_consegna") )
	dw_report.setitem(ll_riga,"data_registrazione", lds_commessa.getitemdatetime(1,"data_registrazione") )
	dw_report.setitem(ll_riga,"anno_commessa", lds_commessa.getitemnumber(1,"anno_commessa") )
	dw_report.setitem(ll_riga,"num_commessa",  lds_commessa.getitemnumber(1,"num_commessa"))
	
	if lds_commessa.getitemstring(1, "flag_gen_commessa") = "N" then
		dw_report.setitem(ll_riga,"des_report", "DISTINTA ARTICOLO")
	else
		dw_report.setitem(ll_riga,"des_report", "COMMESSA ASSEMBLAGGIO")
	end if
	
	ls_cod_tipo_comm = lds_commessa.getitemstring(1,"cod_tipo_commessa")
	ls_des_tipo_comm = f_des_tabella("tab_tipi_commessa","cod_tipo_commessa='" + lds_commessa.getitemstring(1,"cod_tipo_commessa") + "'", "des_tipo_commessa" )
	
	
	dw_report.setitem(ll_riga,"cod_tipo_commessa",  ls_cod_tipo_comm)
	
	dw_report.setitem(ll_riga,"des_tipo_commessa", f_des_tabella("tab_tipi_commessa","cod_tipo_commessa='" + lds_commessa.getitemstring(1,"cod_tipo_commessa") + "'", "des_tipo_commessa" ) )
	
	dw_report.setitem(ll_riga,"des_deposito_prelievo", f_des_tabella("anag_depositi","cod_deposito='" + lds_commessa.getitemstring(1,"cod_deposito_prelievo") + "'", "des_deposito" ) )
	dw_report.setitem(ll_riga,"des_deposito_versamento", f_des_tabella("anag_depositi","cod_deposito='" + lds_commessa.getitemstring(1,"cod_deposito_versamento") + "'", "des_deposito" ) )
	dw_report.setitem(ll_riga,"des_operatore", f_des_tabella("tab_operatori","cod_operatore='" + lds_commessa.getitemstring(1,"cod_operatore") + "'", "des_operatore" ))

next

dw_report.Object.DataWindow.Print.DocumentName=g_str.format("SCHDST-$1-$2-$3",al_anno_ordine, al_num_ordine, al_prog_riga_ord_ven)
PrintDataWindow(al_job, dw_report) 

return 0
end function

public function integer wf_lancio_prod_new (string as_action, ref long al_id_tes_lancio_prod, ref string as_messaggio);/*
FUNZIONE PER INVIO LANCIO PRODUZIONE
	al_action N=nuovo lancio A=Aggiungi a lancio esistente


*/

long ll_selected[], ll_ret, ll_y, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_riga
string	ls_sql, ls_descrizione
datetime ldt_oggi
datastore lds_data


dw_lista_ord_ven_stampa_prod.get_selected_rows(ll_selected[])

if upperbound(ll_selected[]) >0 then
else
	as_messaggio = "Selezionare almeno un ordine dalla lista"
	return -1
end if

if as_action = "N" then 		// nuovo lancio produzione
	
	ldt_oggi = datetime(today(), now())
	ls_descrizione = g_str.format("$1 - $2 ", string(ldt_oggi,"dd/mm/yyyy hh:mm:ss"), s_cs_xx.cod_utente)

	insert into tes_lancio_prod(
		cod_azienda,
		descrizione,
		cod_utente,
		flag_terminato,
		data_registrazione
		)
	values (
		:s_cs_xx.cod_azienda,
		:ls_descrizione,
		:s_cs_xx.cod_utente,
		'N',
		:ldt_oggi	
		);
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore INSERT testata lancio produzione" + sqlca.sqlerrtext
		return -1
	end if
	
	guo_functions.uof_get_identity(sqlca, al_id_tes_lancio_prod)
	
end if
	
	
for ll_riga = 1 to upperbound(ll_selected[])
		
	ll_anno_registrazione =dw_lista_ord_ven_stampa_prod.getitemnumber(ll_selected[ll_riga],"anno_registrazione")
	ll_num_registrazione =dw_lista_ord_ven_stampa_prod.getitemnumber(ll_selected[ll_riga],"num_registrazione")
	
	ls_sql = g_str.format("select prog_riga_ord_ven from det_ord_ven where cod_azienda ='$1' and anno_registrazione=$2 and num_registrazione=$3 and num_riga_appartenenza= 0 and flag_blocco ='N' ",s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)
	
	ll_ret = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_messaggio)
	if ll_ret < 0 then
		as_messaggio = "Errore in creazione datastore: " + as_messaggio
		return -1
	end if
	
	for ll_y = 1 to ll_ret
		
		ll_prog_riga_ord_ven = lds_data.getitemnumber(ll_y, 1)
		
		insert into det_lancio_prod(
			cod_azienda,
			id_tes_lancio_prod,
			anno_reg_ord_ven ,
			num_reg_ord_ven ,
			prog_riga_ord_ven )
		values (
			:s_cs_xx.cod_azienda,
			:al_id_tes_lancio_prod,
			:ll_anno_registrazione,
			:ll_num_registrazione,
			:ll_prog_riga_ord_ven);
			
		if sqlca.sqlcode < 0 then
			as_messaggio = "Errore INSERT dettaglio lancio produzione" + sqlca.sqlerrtext
			return -1
		end if
		
	next
	
next


return 0
end function

public subroutine wf_lancio_produzione (string as_origine_comando);// funzione di lancio produzione che può trarre origine dalla lista degli ordini (sistema classico) oppure dalle sessioni di produzione (sistema tablet).
//	as_origine_comando  D=lista ordine originata dalla datawindow
//								T=lista ordine originata dalla sessione corrente selezionata nella TV

long				ll_num_registrazione,ll_riga,ll_selected[],ll_prog_riga_ord_ven,ll_num_reparti,ll_prog_riga_comanda, ll_tot, ll_ret, ll_tot_selected, ll_row_ds

integer			li_anno_registrazione,li_risposta,li_r

string				ls_cod_prodotto,ls_cod_tipo_det_ven,ls_cod_versione,ls_cod_reparto_test,ls_tipo_stampa, ls_cod_cliente, ls_sql, & 
		 			ls_cod_tipo_ord_ven,ls_errore,ls_cod_reparto,ls_cod_reparti_trovati[], ls_selezionato, ls_utente, ls_des_utente
					 
datetime			ldt_data_consegna,ldt_data_consegna_riga, ldt_oggi

decimal{4} 		ld_quan_ordine
uo_produzione 	luo_produzione
uo_log_sistema luo_log

string				ls_clienti_elaborati[], ls_clienti_con_blocchi[], ls_clienti_con_warning[]

boolean			lb_salta, lb_esclusi

datastore		lds_data, lds_sessione
treeviewitem 	ltv_item
s_tree_lancio_produzione ls_record, ls_record_null



if as_origine_comando <> "D" and as_origine_comando <> "T" then
	g_mb.warning("Lancio di produzione deve avere origine dalla lista ordini oppure dalle sessioni!")
	return
end if

ldt_oggi = datetime(today(), 00:00:00)
lb_esclusi = false

// Enrico 03-04-2013 -----
// Prima di eseguire la procedura devo verificare che un altro utente non la stia eseguendo in parallelo
luo_log = create uo_log_sistema

if luo_log.uof_verify_process("LPI", "LPF", ls_errore) > 0 then
	
	if g_mb.messagebox("Apice", ls_errore, Question!, YesNo!, 2) = 2 then
		return
	else
		if g_mb.messagebox("Apice", "Sei assolutamente certo di voler procedere (questa conferma viene registrata) ?",Question!, YesNo!, 2) = 2 then return
	end if
	
end if

ls_errore = ""


// Segno nella log_sistema che sto eseguendo la procedura, così altri utenti saranno avvisati
luo_log = create uo_log_sistema
luo_log.uof_write_log_sistema("LPI","LANCIO PRODUZIONE INIZIO")

commit using sqlca;
destroy luo_log
// ora posso partire
setpointer(hourglass!)


luo_produzione = create uo_produzione

if as_origine_comando = "T" then
	if isnull(tv_lancio.il_handle) or tv_lancio.il_handle <= 0 then
		g_mb.warning("Selezionare una sessione valida!")
		return 
	end if
	
	tv_lancio.getitem(tv_lancio.il_handle,ltv_item)
	
	ls_record = ltv_item.data
	// tutte le sessioni hanno il livello ZERO
	if ls_record.livello <> 0 then
		g_mb.warning("Selezionare una sessione valida!")
		return 
	end if
	
	ls_sql = g_str.format(" select distinct anno_reg_ord_ven, num_reg_ord_ven, tes_ord_ven.cod_tipo_ord_ven, tes_ord_ven.data_consegna, tes_ord_ven.cod_cliente, det_lancio_prod.prog_riga_ord_ven " + &
	" from det_lancio_prod  " + &
	" join tes_ord_ven on tes_ord_ven.cod_azienda = det_lancio_prod.cod_azienda and tes_ord_ven.anno_registrazione = det_lancio_prod.anno_reg_ord_ven and tes_ord_ven.num_registrazione= det_lancio_prod.num_reg_ord_ven " + &
	" join tab_tipi_ord_ven on tab_tipi_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and tab_tipi_ord_ven.cod_tipo_ord_Ven = tes_ord_ven.cod_tipo_ord_ven " + &
	" where id_tes_lancio_prod = $1 ",ls_record.id_padre)
	
	ll_tot = guo_functions.uof_crea_datastore(lds_sessione, ls_sql)
		
else
	dw_lista_ord_ven_stampa_prod.get_selected_rows(ll_selected[])
	ll_tot = upperbound(ll_selected[])
end if

if  ll_tot>0 then
else
	rollback;
	destroy luo_produzione;
	g_mb.error("Apice", "Selezionare almeno un ordine")
	return
end if

hpb_1.position = 0
hpb_1.maxposition = ll_tot

lds_data = create datastore
lds_data.dataobject = "d_riepilogo_documenti_fido"
lds_data.settransobject(sqlca)

	
for ll_riga = 1 to ll_tot
	Yield()
	hpb_1.stepit( )
	
	if as_origine_comando = "D" then
		li_anno_registrazione =dw_lista_ord_ven_stampa_prod.getitemnumber(ll_selected[ll_riga],"anno_registrazione")
		ll_num_registrazione =dw_lista_ord_ven_stampa_prod.getitemnumber(ll_selected[ll_riga],"num_registrazione")
		ls_cod_tipo_ord_ven = dw_lista_ord_ven_stampa_prod.getitemstring(ll_selected[ll_riga],"tipo_ord_ven")
		ldt_data_consegna = dw_lista_ord_ven_stampa_prod.getitemdatetime(ll_selected[ll_riga],"data_consegna")
		ls_cod_cliente = dw_lista_ord_ven_stampa_prod.getitemstring(ll_selected[ll_riga],"cod_cliente")
	else
		li_anno_registrazione =lds_sessione.getitemnumber(ll_riga,1)
		ll_num_registrazione =lds_sessione.getitemnumber(ll_riga,2)
		ls_cod_tipo_ord_ven = lds_sessione.getitemstring(ll_riga,3)
		ldt_data_consegna = lds_sessione.getitemdatetime(ll_riga,4)
		ls_cod_cliente = lds_sessione.getitemstring(ll_riga,5)
		ll_prog_riga_ord_ven = lds_sessione.getitemnumber(ll_riga,6)
	end if
	
	pcca.mdi_frame.setmicrohelp("Elaborazione ordine " + string(li_anno_registrazione) +"/" + string(ll_num_registrazione) + " ...")
	
	//#################################################################################
	li_risposta = iuo_fido.uof_get_blocco_cliente_loop(	ls_cod_cliente, ldt_oggi, is_cod_parametro_blocco, &
																		ls_clienti_elaborati[], ls_clienti_con_blocchi[], ls_clienti_con_warning[], &
																		lb_salta, ls_errore)
	if li_risposta <> 0 then

		//salvo in un datastore
		ll_row_ds = lds_data.insertrow(0)
		lds_data.setitem(ll_row_ds, "cod_cliente", ls_cod_cliente)
		lds_data.setitem(ll_row_ds, "rag_soc_1", f_des_tabella ( "anag_clienti", "cod_cliente = '" +  ls_cod_cliente + "'", "rag_soc_1" ))
		lds_data.setitem(ll_row_ds, "anno_ordine", li_anno_registrazione)
		lds_data.setitem(ll_row_ds, "num_ordine", ll_num_registrazione)
		if ls_errore<>"" then
			lds_data.setitem(ll_row_ds, "messaggio", ls_errore)
			lds_data.setitem(ll_row_ds, "ordinamento", 0)
		else
			lds_data.setitem(ll_row_ds, "ordinamento", 1)
		end if
		
		lds_data.setitem(ll_row_ds, "tipo_blocco", li_risposta)

	end if
	
	if lb_salta then
		//rimuovi la selezione della riga
		lb_esclusi = true
		if as_origine_comando = "D" then 
			dw_lista_ord_ven_stampa_prod.selectrow(ll_selected[ll_riga], false)
		end if
		continue
	end if
	//#################################################################################
	
	ls_errore = ""
	if as_origine_comando = "D" then
		li_risposta = luo_produzione.uof_controlla_produzione(ll_num_registrazione, li_anno_registrazione, 0, ls_errore)
	else
		li_risposta = luo_produzione.uof_controlla_produzione(ll_num_registrazione, li_anno_registrazione, ll_prog_riga_ord_ven, ls_errore)
	end if
	
	choose case li_risposta
		case is < 0
			
			if pos(ls_errore, "Procedure has not been executed or has no results") > 0 then
				ls_errore = "Problema lettura righe selezionate: riprova a cliccare sul pulsante LANCIO PRODUZIONE"
			end if
			
			messagebox("SEP",ls_errore,stopsign!)
			destroy luo_produzione;
			
			rollback;
			pcca.mdi_frame.setmicrohelp("Pronto!")
			return
		
		case 1 // non è un errore ma significa che esiste almeno una sessione di lavoro
			li_r = messagebox("SEP","Attenzione! Nell'ordine " + string(ll_num_registrazione) + "/" + & 
			string(li_anno_registrazione) + " esistono già delle rilevazioni di produzione." + &  
			" Se si procede con il lancio di produzione, tali rilevazioni andranno perdute." + &  
			" Vuoi procedere con il lancio di produzione per questo ordine?",question!,yesnocancel!,1)
	
			choose case li_r
				case 1
					// non fa niente e continua il processo normalmente
					
				case 2
					// skippa all'ordine successivo
					continue
					
				case 3
					// esce dalla procedure e annulla le modifiche
					messagebox("SEP","Processo annullato dall'operatore, tutte le modifiche sono state annullate",stopsign!)
					destroy luo_produzione;
					rollback;
					pcca.mdi_frame.setmicrohelp("Pronto!")
					return
					
			end choose
		
	
		case 2,3 //  se 2 non è un errore ma significa che almeno un record in det_ordini_produzione
					// se 3 non è un errore ma significa che almeno un record in tes_ordini_produzione
			li_r = messagebox("SEP","Attenzione! La produzione per l'ordine " + string(ll_num_registrazione) + "/" + & 
			string(li_anno_registrazione) + " è già stata lanciata." + &  
			" Si può comunque procedere alla ripetizione del lancio di produzione. In questo modo se ci sono" + &  
			" state delle modifiche alla struttura del prodotto saranno riprocessate e reinserite in produzione." + &
			" Vuoi procedere con il lancio di produzione per questo ordine?",question!,yesnocancel!,1)
	
			choose case li_r
				case 1
					// non fa niente e continua il processo normalmente
					
				case 2
					// skippa all'ordine successivo
					continue
					
				case 3
					// esce dalla procedure e annulla le modifiche
					messagebox("SEP","Processo annullato dall'operatore, tutte le modifiche sono state annullate",stopsign!)
					destroy luo_produzione;
					rollback;
					pcca.mdi_frame.setmicrohelp("Pronto!")
					return
					
			end choose
	

		case 0 // procede poichè significa che non vi è alcun record nelle tabelle di produzione
			// procede regolarmente 
		
	end choose
	
	if as_origine_comando = "D" then
		li_risposta = luo_produzione.uof_elimina_produzione(ll_num_registrazione, li_anno_registrazione, as_origine_comando, ls_errore)
	else																			 
		li_risposta = luo_produzione.uof_elimina_produzione_riga(ll_num_registrazione, li_anno_registrazione, ll_prog_riga_ord_ven, as_origine_comando, ls_errore)
	end if
	
	if li_risposta < 0 then
		messagebox("SEP",ls_errore,stopsign!)
		destroy luo_produzione;
		rollback;
		pcca.mdi_frame.setmicrohelp("Pronto!")
		return
	end if


	if as_origine_comando = "D" then
		li_risposta = luo_produzione.uof_lancia_produzione(ll_num_registrazione, li_anno_registrazione, 0, ls_cod_tipo_ord_ven, ldt_data_consegna, ls_errore)
	else
		li_risposta = luo_produzione.uof_lancia_produzione(ll_num_registrazione, li_anno_registrazione, ll_prog_riga_ord_ven, ls_cod_tipo_ord_ven, ldt_data_consegna, ls_errore)
	end if
	
	if li_risposta < 0 then
		messagebox("SEP",ls_errore,stopsign!)
		destroy luo_produzione;
		rollback;
		pcca.mdi_frame.setmicrohelp("Pronto!")
		return
	end if
	
	// ------- EnMe 13/01/2020 per Alusistemi. Ora da qui è possibile anche generare le commesse di produzione //
	if cbx_gen_commesse.checked then
		if as_origine_comando = "D" then
			li_risposta = wf_crea_commesse(li_anno_registrazione,ll_num_registrazione, 0, ls_cod_tipo_ord_ven, ref ls_errore)
		else
			li_risposta = wf_crea_commesse(li_anno_registrazione,ll_num_registrazione, ll_prog_riga_ord_ven, ls_cod_tipo_ord_ven, ref ls_errore)
		end if
		if li_risposta < 0 then
			messagebox("SEP",ls_errore,stopsign!)
			destroy luo_produzione;
			rollback;
			pcca.mdi_frame.setmicrohelp("Pronto!")
			return
		end if
	end if
	// ---
next

destroy luo_produzione

pcca.mdi_frame.setmicrohelp("Pronto!")
commit;

luo_log = create uo_log_sistema
luo_log.uof_write_log_sistema("LPF","LANCIO PRODUZIONE FINE")

commit using sqlca;
destroy luo_log

hpb_1.position = 0
setpointer(arrow!)


if lb_esclusi then
	g_mb.warning("Alcuni clienti negli ordini risultano bloccati o con blocco finanziario "+&
						"e la tabella di configurazione 'parametri blocco' indica di bloccare il lancio di produzione!~r~nPer questi ordini non sarà lanciata la produzione "+&
						"e vengono de-selezionati per la successiva stampa!~r~n"+&
						"Premere OK per visualizzare il riepilogo.")
end if

if lds_data.rowcount() > 0 then
	s_cs_xx.parametri.parametro_ds_1 = lds_data
	s_cs_xx.parametri.parametro_s_10 = "ORDINI CON LANCIO NON EFFETTUATO (PER BLOCCO CLIENTE) O EFFETTUATO MA CON WARNING"
	window_open(w_riepilogo_documenti_fido, 0)
end if

destroy lds_data


messagebox("SEP","Processo di lancio di produzione completato.",information!)


end subroutine

public subroutine wf_clicked_stampa (boolean fb_check_commessa, string as_origine_comando);/*	stampa ordini di produzione su carta
	ENME 03-08-2016 - aggiunto parametro as_origine_comando 	T=lista ordini proveniente da sessione lancio del treeview
																					D=lista ordini proveniente dalla DW lista classica

*/
integer			li_zoom=100, li_anno_registrazione, li_risposta

long				ll_riga, ll_num_registrazione, ll_num_copie_sole, ll_num_copie_tecniche, ll_num_copie_sfuso, ll_prog_riga_comanda, ll_selected[], ll_tot, &
					ll_num_tenda, job, ll_t, ll_tot_stampe_reparti, ll_tot_stampe_reparti_stab, ll_tot_stampe_reparti_altri, ll_count_senza_commessa, ll_tot_ordini

string				ls_flag_calcola_dt, ls_cod_tipo_det_ven, ls_cod_tipo_ord_ven, ls_sql, ls_cod_reparto, ls_document_name, ls_tipo_stampa, &
					ls_errore, ls_cod_deposito_reparto, ls_percentuale, ls_flag_stampa_scheda_distinta, ls_flag_sottotipo_bcl, ls_flag_stampa_foglio

datetime			ldt_data_consegna

boolean			lb_altri_reparti = false, lb_tendemodel=false

datastore		lds_sessione
treeviewitem 	ltv_item
s_tree_lancio_produzione 	ls_record, ls_record_null
s_stampa_ordini_prod		ls_stampa_ordini[]
// ------------------------------------------------------------



dw_lista_ord_ven_stampa_prod.resetupdate()

ll_count_senza_commessa = 0
dw_ordini_senza_commessa.reset()

ib_stampa_commesse = dw_ext_selezione.getitemstring(dw_ext_selezione.getrow(), "flag_stampa_scheda_distinta") = "S"


if as_origine_comando = "T" then
	if isnull(tv_lancio.il_handle) or tv_lancio.il_handle <= 0 then
		g_mb.warning("Selezionare una sessione valida!")
		return 
	end if
	
	tv_lancio.getitem(tv_lancio.il_handle,ltv_item)
	
	ls_record = ltv_item.data
	// tutte le sessioni hanno il livello ZERO
	if ls_record.livello <> 0 then
		g_mb.warning("Selezionare una sessione valida!")
		return 
	end if
	
	ls_sql = g_str.format(" select distinct anno_reg_ord_ven, num_reg_ord_ven, tes_ord_ven.cod_tipo_ord_ven, tes_ord_ven.data_consegna, tes_ord_ven.cod_cliente " + &
	" from det_lancio_prod  " + &
	" join tes_ord_ven on tes_ord_ven.cod_azienda = det_lancio_prod.cod_azienda and tes_ord_ven.anno_registrazione = det_lancio_prod.anno_reg_ord_ven and tes_ord_ven.num_registrazione= det_lancio_prod.num_reg_ord_ven " + &
	" join tab_tipi_ord_ven on tab_tipi_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and tab_tipi_ord_ven.cod_tipo_ord_Ven = tes_ord_ven.cod_tipo_ord_ven " + &
	" where id_tes_lancio_prod = $1 ",ls_record.id_padre)
	
	ll_tot = guo_functions.uof_crea_datastore(lds_sessione, ls_sql)
	for ll_riga = 1 to ll_tot
		ls_stampa_ordini[ll_riga].al_anno_registrazione 	= lds_sessione.getitemnumber(ll_riga, 1)
		ls_stampa_ordini[ll_riga].al_num_registrazione 	= lds_sessione.getitemnumber(ll_riga, 2)
		ls_stampa_ordini[ll_riga].as_cod_tipo_ord_ven 	= lds_sessione.getitemstring(ll_riga, 3)
		ls_stampa_ordini[ll_riga].adt_data_consegna 		= lds_sessione.getitemdatetime(ll_riga, 4)
	next
	destroy lds_sessione

else
	dw_lista_ord_ven_stampa_prod.get_selected_rows(ll_selected[])
	if upperbound(ll_selected[]) <= 0  then
		rollback;
		g_mb.error("Apice", "Selezionare almeno un ordine")
		return
	end if
	
	for ll_riga = 1 to upperbound(ll_selected[])
		ls_stampa_ordini[ll_riga].al_anno_registrazione 	= dw_lista_ord_ven_stampa_prod.getitemnumber(ll_selected[ll_riga],"anno_registrazione")
		ls_stampa_ordini[ll_riga].al_num_registrazione 	= dw_lista_ord_ven_stampa_prod.getitemnumber(ll_selected[ll_riga],"num_registrazione")
		ls_stampa_ordini[ll_riga].as_cod_tipo_ord_ven 	= dw_lista_ord_ven_stampa_prod.getitemstring(ll_selected[ll_riga],"tipo_ord_ven")
		ls_stampa_ordini[ll_riga].adt_data_consegna	 	= dw_lista_ord_ven_stampa_prod.getitemdatetime(ll_selected[ll_riga],"data_consegna")
	next
end if	


is_origine_comando = ""
if fb_check_commessa then
	Yield()
	for ll_riga = 1 to upperbound(ls_stampa_ordini[])
		
		li_anno_registrazione = ls_stampa_ordini[ll_riga].al_anno_registrazione
		ll_num_registrazione = ls_stampa_ordini[ll_riga].al_num_registrazione
		
		pcca.mdi_frame.setmicrohelp("Controllo esistenza Commesse ordine " + string(li_anno_registrazione) +"/"+ string(ll_num_registrazione) + " ...")
		li_risposta = wf_ordini_senza_commessa(li_anno_registrazione, ll_num_registrazione, ls_errore)
		
		ll_count_senza_commessa += li_risposta
		
	next
	
	pcca.mdi_frame.setmicrohelp("")
	
	if ll_count_senza_commessa>0 then
		//ci sono ordini senza commessa, visualizzarli sul folder relativo e blocca tutti gli altri folder
		//l'utente deve esplicitamente accettare di andare avanti nella stampa
		
		dw_folder.fu_enabletab(4)
		
		dw_folder.fu_disabletab(3)
		dw_folder.fu_disabletab(2)
		dw_folder.fu_disabletab(1)
		
		cbx_ordini_senza_commessa.checked = false
		cb_ordini_senza_commessa.enabled = false
		
		is_origine_comando = as_origine_comando
		
		dw_folder.fu_selecttab(4)
		
		//dw_ordini_senza_commessa.resetupdate()
		
		return
	end if
end if

//dw_ordini_senza_commessa.resetupdate()


//genera sessione		YYYYMMDDhhmmss
il_sessione = wf_get_id_sessione()

ll_tot_stampe_reparti = 0
ll_tot_stampe_reparti_stab = 0
ll_tot_stampe_reparti_altri = 0

select numero
into   :li_zoom
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'ZSP';
if sqlca.sqlcode <> 0 then li_zoom = 100
if isnull(li_zoom) then li_zoom = 100

ls_flag_calcola_dt = dw_ext_selezione.getitemstring(1,"flag_calcola_dt")

hpb_1.position = 0
hpb_1.maxposition = upperbound(ll_selected[])

for ll_riga = 1 to  upperbound(ls_stampa_ordini[])
	Yield()
	hpb_1.stepit( )
	
	li_anno_registrazione = ls_stampa_ordini[ll_riga].al_anno_registrazione
	ll_num_registrazione = ls_stampa_ordini[ll_riga].al_num_registrazione
	ls_cod_tipo_ord_ven = ls_stampa_ordini[ll_riga].as_cod_tipo_ord_ven
	ldt_data_consegna = ls_stampa_ordini[ll_riga].adt_data_consegna
	
	pcca.mdi_frame.setmicrohelp("Elaborazione ordine " + string(li_anno_registrazione) +"/" + string(ll_num_registrazione) + " ...")
	
	wf_scrivi_log(li_anno_registrazione, ll_num_registrazione, "  elaborazione reparti ", "")
	
	if wf_stampa_ordini(li_anno_registrazione, ll_num_registrazione, ls_cod_tipo_ord_ven, ldt_data_consegna, ls_errore) < 0 then
		rollback;
		g_mb.error("Apice",ls_errore)
		return
	end if
	
next


// *********** INIZIO PROCESSO DI STAMPA *********************

//faccio un commit
commit;
wf_scrivi_log(0, 0, "  COMMIT dopo scrittura su tabelle buffer di stampa; ID SESSIONE: "+string(il_sessione), "")


pcca.mdi_frame.setmicrohelp("Inizio processo di stampa ...")

ll_num_copie_sole = long(em_num_copie_sole.text)
ll_num_copie_tecniche = long(em_num_copie_tecniche.text)
ll_num_copie_sfuso = long(em_num_copie_sfuso.text)

select 	count(*)
into :ll_tot_ordini
from    tes_stampa_ordini
where   cod_azienda=:s_cs_xx.cod_azienda and 
			prog_sessione=:il_sessione;

if isnull(ll_tot_ordini) then ll_tot_ordini=0

ll_riga = 0
hpb_1.position = 0
hpb_1.maxposition = ll_tot_ordini

declare r_tes_stampa dynamic cursor for sqlsa;

ls_sql =" select  anno_registrazione, " + &
		  " num_registrazione, " + &
		  " cod_reparto, " + &
		  " prog_riga_comanda, " + &
		  " num_tenda, " + &
		  " flag_stampa_foglio " + &
	     " from    tes_stampa_ordini" + &
	     " where   cod_azienda= '" + s_cs_xx.cod_azienda + "' and prog_sessione="+string(il_sessione)
	
ls_sql=ls_sql + " order by cod_reparto, data_consegna, anno_registrazione, num_registrazione, num_tenda"

prepare sqlsa from :ls_sql;

open dynamic r_tes_stampa;

wf_scrivi_log(0, 0, " ----- ", "################ INIZIO PROCESSO DI STAMPA ############")

do while 1 = 1	
	fetch r_tes_stampa
	into  :li_anno_registrazione,
			:ll_num_registrazione,
			:ls_cod_reparto,
			:ll_prog_riga_comanda,
			:ll_num_tenda,
			:ls_flag_stampa_foglio;
			
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode < 0 then 
		g_mb.error("Apice","Processo di Lettura cursore su tes_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext)
		close r_tes_stampa;
		rollback;		
		return
	end if
	
	Yield()
	
		// stefanop 28/05/2010: ticket 136: costruisco il nome del documento
	ls_document_name = g_str.format("Ordine $1-$2-$3-$4-$5",li_anno_registrazione, ll_num_registrazione, ls_cod_reparto,ll_prog_riga_comanda, ll_num_tenda)
//	ls_document_name = "Ordine " + string(li_anno_registrazione) + "-" + string(ll_num_registrazione) + " - " + ls_cod_reparto
	
	wf_scrivi_log(li_anno_registrazione, ll_num_registrazione, ls_cod_reparto, "INIZIO ELABORAZIONE")
	
	
	ll_riga += 1
	hpb_1.stepit( )
	ls_percentuale = string(ll_riga) + " di " + string(ll_tot_ordini) + " - "
	
	pcca.mdi_frame.setmicrohelp(ls_percentuale+"Stampa documento "+ls_document_name)
	
	select cod_tipo_ord_ven
	into   :ls_cod_tipo_ord_ven
	from   tes_ord_ven
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;
	
	if sqlca.sqlcode < 0 then 
		g_mb.error("Apice","Errore sul DB: " + sqlca.sqlerrtext)
		close r_tes_stampa;
		rollback;		
		return
	end if
	
	//lettura tipo report
	select 	flag_tipo_bcl,
				flag_stampa_scheda_distinta,
				flag_sottotipo_bcl
	into   		:ls_tipo_stampa, 
				:ls_flag_stampa_scheda_distinta,
				:ls_flag_sottotipo_bcl
	from   	tab_tipi_ord_ven
	where  	cod_azienda=:s_cs_xx.cod_azienda and
				cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;
	
	if sqlca.sqlcode < 0 then 
		g_mb.error("Apice","Errore sul DB: " + sqlca.sqlerrtext)
		close r_tes_stampa;
		rollback;
		return
	end if

	select cod_deposito
	into :ls_cod_deposito_reparto
	from anag_reparti
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    cod_reparto=:ls_cod_reparto;
	
	ll_tot_stampe_reparti += 1
	
	//se si tratta di un reparto di stabilimento diverso da quello dell'operatore non stampare per tale reparto, ma metti in spooler stabilimenti
	if ls_cod_deposito_reparto=is_cod_deposito_operatore or is_cod_deposito_operatore=is_cod_deposito_CSSYSTEM then
		//se entri nell'IF il reparto appartiene al tuo stabilimento oppure sei CS_SYSTEM, quindi prosegui normalmente
		
		choose case ls_tipo_stampa // case per tipo report e impostazione datawindow
			case 'A'
				// REPORT TENDE DA SOLE
				dw_report.reset()
				
				if ls_flag_calcola_dt = "S" then                    
					choose case ls_flag_sottotipo_bcl
						case "2"
							dw_report.DataObject = 'd_report_op_scheda_singola'
						case "3"
							dw_report.DataObject = 'd_report_op_scheda_singola_sezioni'
						case "4"
							dw_report.DataObject = 'd_report_op_scheda_singola_sez_semplif'
						case else // vecchio sistema
							dw_report.DataObject = 'd_report_1_dt'
					end choose
				else
					choose case ls_flag_sottotipo_bcl
						case "2"
							dw_report.DataObject = 'd_report_op_scheda_singola'
						case "3"
							dw_report.DataObject = 'd_report_op_scheda_singola_sezioni'
						case "4"
							dw_report.DataObject = 'd_report_op_scheda_singola_sez_semplif'
						case else // vecchio sistema)
							dw_report.DataObject = 'd_report_1'
					end choose
				end if
				
				//funzione che compila report 1
				
				if isnull(ls_flag_sottotipo_bcl) or len(ls_flag_sottotipo_bcl) < 1 or ls_flag_sottotipo_bcl = "1" then
					li_risposta = f_report_1(li_anno_registrazione,ll_num_registrazione,ls_cod_reparto,ll_prog_riga_comanda,ll_num_tenda)
				else
					li_risposta = f_report_op_scheda_singola(li_anno_registrazione,ll_num_registrazione,ls_cod_reparto,ll_prog_riga_comanda,ll_num_tenda)
				end if
				
				if li_risposta < 0 then 
					close r_tes_stampa;
					rollback;				
					return
				end if
							
				if ls_flag_stampa_foglio = "S" then 
					job = PrintOpen(ls_document_name) // stefanop 28/05/2010: ticket 136
					for ll_t = 1 to ll_num_copie_sole
						dw_report.Object.DataWindow.Print.DocumentName=g_str.format("OL-$1-$2-$3-$4-$5-$6",li_anno_registrazione,ll_num_registrazione,ls_cod_reparto,ll_prog_riga_comanda,ll_num_tenda,ll_t)
						dw_report.object.datawindow.zoom = li_zoom
						PrintDataWindow(job, dw_report) 
					next
					PrintClose(job)
				end if
			case 'B'
				// REPORT SFUSO
				dw_report.DataObject = 'd_report_2'
				
				//funzione che compila report 2
				li_risposta = f_report_2(li_anno_registrazione,ll_num_registrazione,ls_cod_reparto)
	
				if li_risposta < 0 then 
					close r_tes_stampa;
					rollback;				
					return
				end if
				
				if ls_flag_stampa_foglio = "S" then 
					job = PrintOpen(ls_document_name) // stefanop 28/05/2010: ticket 136
					for ll_t = 1 to ll_num_copie_sfuso
						dw_report.Object.DataWindow.Print.DocumentName=g_str.format("OL-$1-$2-$3-$4",li_anno_registrazione,ll_num_registrazione,ls_cod_reparto,ll_t)
						dw_report.object.datawindow.zoom = li_zoom
						PrintDataWindow(job, dw_report) 
						
						// Stampa scheda distinta come da specifica REPORT-OL (Lupo) 22/06/2061
						if ls_flag_stampa_scheda_distinta = "S" and ib_stampa_commesse  then
							// il processo di stampa avviene all'interno della funzione ecco perchè passo il Job
							f_report_schede_distinta(job, li_anno_registrazione,ll_num_registrazione, ref ls_errore)
						end if
					next
					PrintClose(job)
				end if
				
			case 'C'
				// REPORT TENDE TECNICHE
				dw_report.reset()
				
				// verifico se siamo nel caso del report personalizzato per TendeModel Milano
				guo_functions.uof_get_parametro_azienda( "TDP", lb_tendemodel)
				
				if lb_tendemodel then
					dw_report.DataObject = 'd_report_3_plastinds'
					//funzione che compila report 3 sencondo le specifiche Plastinds e Tendemodel
					li_risposta = f_report_3_plastinds(li_anno_registrazione,ll_num_registrazione,ls_cod_reparto)
				else
					dw_report.DataObject = 'd_report_3'
					//funzione che compila report 3
					li_risposta = f_report_3(li_anno_registrazione,ll_num_registrazione,ls_cod_reparto)
				end if
				
				if li_risposta < 0 then 
					close r_tes_stampa;
					rollback;				
					return
				end if
				
				if ls_flag_stampa_foglio = "S" then 
					for ll_t = 1 to ll_num_copie_tecniche
						dw_report.object.datawindow.zoom = li_zoom
						if lb_tendemodel then 
							dw_report.Object.DataWindow.Print.Orientation=1
						end if
						dw_report.Object.DataWindow.Print.DocumentName=g_str.format("OL-$1-$2-$3-$4",li_anno_registrazione,ll_num_registrazione,ls_cod_reparto,ll_t)
						dw_report.print()
					next
				end if
		end choose	
		
		ll_tot_stampe_reparti_stab += 1
		
	else
		wf_scrivi_log(li_anno_registrazione, ll_num_registrazione, ls_cod_reparto, "reparto non appartenente al deposito utente")
		if not ib_debug_mode then
			//inserire in spooler per altri stabilimenti
			if wf_aggiungi_buffer_stabilimento(li_anno_registrazione, ll_num_registrazione, ls_cod_reparto, "R", ls_errore) < 0 then
				//errore non bloccante ma segnala!
				g_mb.messagebox("Attenzione!", ls_errore, Exclamation!)
			end if
			
			ll_tot_stampe_reparti_altri += 1
		end if
	end if
	
	wf_scrivi_log(li_anno_registrazione, ll_num_registrazione, ls_cod_reparto, "update ordine stampato")
	update tes_ord_ven
	set    flag_stampata_bcl='S'
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;

	if sqlca.sqlcode < 0 then 
		g_mb.error("Apice","Errore durante update tes_ord_ven flag_stampata_blc. Errore sul DB: " + sqlca.sqlerrtext)
		close r_tes_stampa;
		rollback;
		return
	end if

	wf_scrivi_log(li_anno_registrazione, ll_num_registrazione, ls_cod_reparto, "update flag sblocco fido")
	//Donato 19-11-2008 rimettere il flag_aut_blocco_fido a "N"
	//modifica per gestione fidi PTENDA
	update tes_ord_ven
	set    flag_aut_sblocco_fido='N'
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:li_anno_registrazione
	and    num_registrazione=:ll_num_registrazione;

	if sqlca.sqlcode < 0 then 
		g_mb.error("Apice","Errore durante update tes_ord_ven flag_aut_sblocco_fido. Errore sul DB: " + sqlca.sqlerrtext)
		close r_tes_stampa;
		rollback;
		return
	end if
	//fine modifica -----------------------------------------------------------


loop

close r_tes_stampa;

// *********** FINE PROCESSO DI STAMPA ***********************

wf_scrivi_log(0, 0, "------", "############# FINE PROCESSO DI STAMPA #############")


commit;
pcca.mdi_frame.setmicrohelp("")

//qui funzione che invia la mail di avviso agli altri stabilimenti (se ce ne sono)
if ll_tot_stampe_reparti_altri>0 and not ib_debug_mode then wf_invia_mail()


dw_lista_ord_ven_stampa_prod.change_dw_current()
this.triggerevent("pc_retrieve")


delete det_stampa_ordini where prog_sessione=:il_sessione;
delete tes_stampa_ordini where prog_sessione=:il_sessione;

commit;

g_mb.show("APICE", "Stampa Ordini di Produzione terminata! Totale reparti: "+string(ll_tot_stampe_reparti) +&
											", Interni: "+string(ll_tot_stampe_reparti_stab) + ", Esterni: "+string(ll_tot_stampe_reparti_altri))

hpb_1.position = 0

end subroutine

public function integer f_report_op_scheda_singola (integer fi_anno_registrazione, long fl_num_registrazione, string fs_cod_reparto, long fl_prog_riga_comanda, long fl_num_tenda);// Funzione che compila ordine di produzione (Report Scheda singola) - un foglio per prodotto per ogni reparto.
// nome: f_report_op_scheda_singola
// tipo: integer
//       -1	failed
//  	0 	passed
//
//		Creata il 12/12/2016 
//		Autore Enrico Menegotto

boolean	lb_inserito, lb_alusistemi=false

string		ls_cod_cliente,ls_rag_soc_1,ls_rag_soc_2,ls_indirizzo,ls_cap,ls_localita,ls_provincia,ls_telefono,ls_fax, &
				ls_cod_operatore,ls_cod_giro_consegna,ls_des_reparto, ls_null, ls_cod_imballo, ls_des_imballo, &
				ls_des_comando_1,ls_des_supporto,ls_cod_non_a_magazzino,ls_des_colore_telo,ls_cod_mant_non_a_magazzino, &
				ls_flag_tipo_mantovana,ls_colore_passamaneria,ls_colore_cordolo, ls_cod_tipo_det_ven_addizionali, &
				ls_des_colore_mantovana,ls_flag_allegati,ls_cod_prod_finito,ls_cod_prodotto,ls_cod_dim_x,ls_cod_dim_y,&
				ls_cod_dim_z,ls_cod_dim_t,ls_des_dim_x,ls_des_dim_y,ls_des_dim_z,ls_des_dim_t,ls_cod_tessuto,ls_cod_tessuto_mant,&
				ls_flag_cordolo,ls_flag_passamaneria,ls_cod_verniciatura,ls_cod_comando,ls_pos_comando,ls_cod_colore_lastra,&
				ls_cod_tipo_lastra,ls_cod_tipo_supporto,ls_flag_misura_luce_finita,ls_des_vernice_fc,ls_des_prodotto,& 
				ls_des_colore_tessuto,ls_nota_dettaglio,ls_nota_configuratore,ls_cod_prodotto_opt,ls_cod_misura_opt, &
				ls_nota_opt,ls_des_prodotto_opt,ls_giorno,ls_des_giro_consegna,ls_flag_tassativo,ls_cod_prodotto_padre,&
				ls_cod_prodotto_figlio[],ls_cod_versione,ls_des_distinta_taglio[],ls_um_quantita[],ls_um_misura[],&
				ls_des_prodotto_figlio[],ls_flag_non_calcolare_dt,ls_cod_prodotto_variante,ls_flag_calcola_dt, ls_alias, &
				ls_dest_test, ls_rs1_test, ls_rs2_test, ls_ind_test, ls_loc_test, ls_prov_test, ls_cap_test, ls_fraz_test, &
				ls_b_um_quantita,ls_b_um_misura,ls_bfo,ls_errore,ls_num_ord_cliente,ls_nota_prodotto, &
				ls_cod_versione_figlio[],ls_flag_urgente,ls_lista_reparti_coinvolti, ls_stabilim_prossimo, ls_cod_deposito_origine, &
				ls_cod_deposito_reparto, ls_reparto_prossimo, ls_riga_n_di_m, ls_chiave_etic, ls_cod_verniciatura_2, ls_des_vernice_fc_2,&
				ls_cod_verniciatura_3, ls_des_vernice_fc_3, ls_cod_comando_2, ls_des_comando_2, ls_nota_testata,ls_nota_piede, ls_nota_video, &
				ls_des_variabile, ls_des_dimensioni, ls_flag_azzera_dt, ls_appoggio, ls_cod_lingua, ls_sql, ls_cod_prodotto_dt_old, ls_matricole, &
				ls_flag_nascondi_cliente
			  
integer		li_risposta, li_bco, ll_e

long			ll_num_max_tenda,ll_num_commessa,ll_num_righe,ll_num_commessa_opt,ll_num_taglio,ll_num_sequenza,ll_progressivo,&
				ll_prog_calcolo,ll_i,ll_conta_riga,ll_progr_det_produzione, ll_sqlcode, ll_rows, ll_riga, ll_riga_dt
				
double		ld_quantita[],ld_misura[], ldd_quan_ordine,ldd_quan_supporto,ldd_alt_mantovana,ldd_dim_x,ldd_dim_y,ldd_dim_z,ldd_dim_t,ldd_alt_asta,ldd_quan_ordine_opt
dec{4}		ld_dim_x_riga, ld_dim_y_riga

datetime	ldt_data_consegna, ldt_data_pronto, ldt_data_pronto_fasi

datastore	lds_opt

uo_produzione luo_prod

s_report_ol_produzione_dt lstr_dt[]

datastore	lds_data


setnull(ls_null)

//Donato 12/03/2012: no filigrana per tipo report 1
dw_report.modify("DataWindow.brushmode=0")

dw_report.Modify("t_azienda.text='"+wf_logo()+"'")

guo_functions.uof_get_parametro_azienda( "ALU", lb_alusistemi)


ls_flag_calcola_dt = dw_ext_selezione.getitemstring(1,"flag_calcola_dt")

select quan_ordine,
		 num_commessa,
		 cod_prodotto,
		 nota_dettaglio,
		 cod_versione,
		 nota_prodotto,
		 flag_urgente,
		 des_prodotto
into   :ldd_quan_ordine,
		 :ll_num_commessa,
		 :ls_cod_prodotto,
		 :ls_nota_dettaglio,
		 :ls_cod_versione,
		 :ls_nota_prodotto,
		 :ls_flag_urgente,
		 :ls_des_prodotto
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione
and    prog_riga_ord_ven=:fl_prog_riga_comanda;

if sqlca.sqlcode <> 0 then 
	g_mb.error("Apice","Errore durante la compilazione del ordine lavoro (f_report_op_scheda_singola), lettura tabella det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext)
	return -1
end if

select 	flag_allegati
into		:ls_flag_allegati
from 		comp_det_ord_ven
where  	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_registrazione and
			num_registrazione=:fl_num_registrazione and
			prog_riga_ord_ven=:fl_prog_riga_comanda;
if sqlca.sqlcode < 0 then 
	g_mb.error("Apice","Errore durante la compilazione del ordine lavoro (f_report_op_scheda_singola), lettura tabella comp_det_ord_ven. Errore sul DB: " + sqlca.sqlerrtext)
	return -1
end if

if ls_flag_calcola_dt = "S" then
	if wf_report_1_leggi_dt(ls_cod_prodotto,ls_cod_versione, fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_comanda, ref lstr_dt[], ref ls_errore) < 0 then
		g_mb.error(ls_errore)
	end if
end if

select 	max(num_tenda)
into   		:ll_num_max_tenda
from   	tes_stampa_ordini
where  	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :fi_anno_registrazione and
			num_registrazione = :fl_num_registrazione and
			cod_reparto = :fs_cod_reparto and prog_sessione=:il_sessione;
if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tes_stampa_ordini. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select data_pronto,
		 data_consegna,
		 cod_cliente,
		 cod_giro_consegna,
		 cod_operatore,
		 flag_tassativo,
		 alias_cliente,
		 cod_des_cliente,
		 rag_soc_1,
		 rag_soc_2,
		 indirizzo,
		 localita,
		 frazione,
		 cap,
		 provincia,
		 num_ord_cliente,
		 cod_deposito,
		 cod_imballo
into   :ldt_data_pronto,
		 :ldt_data_consegna,
		 :ls_cod_cliente,
		 :ls_cod_giro_consegna,
		 :ls_cod_operatore,
		 :ls_flag_tassativo,
		 :ls_alias,
		 :ls_dest_test,
		 :ls_rs1_test,
		 :ls_rs2_test,
		 :ls_ind_test,
		 :ls_loc_test,
		 :ls_fraz_test,
		 :ls_cap_test,
		 :ls_prov_test,
		 :ls_num_ord_cliente,
		 :ls_cod_deposito_origine,
		 :ls_cod_imballo
from   	tes_ord_ven
where  	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_registrazione and
			num_registrazione=:fl_num_registrazione;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tes_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

if isnull(ls_dest_test) and isnull(ls_rs1_test) and isnull(ls_rs2_test) and isnull(ls_ind_test) and &
   isnull(ls_loc_test) and isnull(ls_fraz_test) and isnull(ls_cap_test) and isnull(ls_prov_test) then
	dw_report.modify("t_lc.visible = 0")
else
	dw_report.modify("t_lc.visible = 1")
end if

select 	des_giro_consegna
into   		:ls_des_giro_consegna
from   	tes_giri_consegne
where  	cod_azienda=:s_cs_xx.cod_azienda and
			cod_giro_consegna=:ls_cod_giro_consegna;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tes_ord_ven. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select des_reparto, 
		flag_nascondi_cliente
into   	:ls_des_reparto,
		:ls_flag_nascondi_cliente
from   anag_reparti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_reparto=:fs_cod_reparto;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_reparti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

select des_imballo
into   :ls_des_imballo
from   tab_imballi
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_imballo=:ls_cod_imballo;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tabella imballaggi. Dettaglio Errore: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if


		 
select rag_soc_1,
		 rag_soc_2,
		 indirizzo,
		 cap,
		 localita,
		 provincia,
		 telefono,
		 fax,
		 cod_lingua
into   :ls_rag_soc_1,
		 :ls_rag_soc_2,
		 :ls_indirizzo,
		 :ls_cap,
		 :ls_localita,
		 :ls_provincia,
		 :ls_telefono,
		 :ls_fax,
		 :ls_cod_lingua
from   anag_clienti
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_cliente=:ls_cod_cliente;

if sqlca.sqlcode < 0 then 
	messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura anag_clienti. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return -1
end if

ls_rag_soc_1 = f_cazzillo(ls_rag_soc_1)
ls_rag_soc_2 = f_cazzillo(ls_rag_soc_2)
ls_indirizzo = f_cazzillo(ls_indirizzo)
ls_localita = f_cazzillo(ls_localita)

/* SIGNIFICATO DEL FLAG_TIPO RIGA
1=RIGA VARIABILE
2=RIGA OPTIONAL
3=RIGA NOTA
*/
// --------------------- leggo le matricole coinvolte nel foglio di lavoro ------------------------------------------------ //
if dw_report.dataobject = 'd_report_op_scheda_singola_sez_semplif' then
	ls_sql = g_str.format("select cod_ologramma from det_ord_ven_ologramma where cod_azienda='$1' and anno_registrazione=$2 and num_registrazione=$3 and prog_riga_ord_ven=$4",s_cs_xx.cod_azienda, fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_comanda)
	ll_rows = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
	ls_matricole = ""
	for ll_i = 1 to ll_rows
		if len(ls_matricole) > 1 then ls_matricole = ls_matricole + char(13) + char(10)
		ls_matricole += lds_data.getitemstring(ll_i,1)
	next
	destroy lds_data
end if

 
// --------------------- lettura variabili da tabella det_ord_ven_conf_variabili ------------------------------------------------ //

ls_sql = g_str.format(" select a.cod_prodotto, b.cod_variabile,c.des_variabile,b.flag_tipo_dato,b.valore_stringa,b.valore_numero,b.valore_datetime,c.prog_ordinamento_report_ol, b.des_valore, d.des_prodotto,e.flag_layout_report_prod, c.prog_ordinamento, c.flag_nascondi_codice " + &
" from det_ord_ven a  " + &
" left outer join det_ord_ven_conf_variabili b on a.cod_azienda = b.cod_azienda and a.anno_registrazione = b.anno_registrazione and a.num_registrazione=b.num_registrazione and a.prog_riga_ord_ven=b.prog_riga_ord_ven  " + &
" join tab_des_variabili c on b.cod_azienda= c.cod_azienda and b.cod_variabile=c.cod_variabile and a.cod_prodotto=c.cod_prodotto  " + &
" join anag_prodotti d on a.cod_azienda= d.cod_azienda and a.cod_prodotto=d.cod_prodotto  " + &
" join tab_variabili_formule e on e.cod_azienda= c.cod_azienda and e.cod_variabile= c.cod_variabile  " + &
" where a.anno_registrazione= $1 and a.num_registrazione = $2 and a.prog_riga_ord_ven= $3 and b.flag_visible_in_stampa='S' order by c.prog_ordinamento_report_ol ", fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_comanda)

ll_rows = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)

if ll_rows < 0 then
	g_mb.error( "f_report_op_scheda_singola(): Errore caricamento datastore riga/variabili " + ls_errore  ) 
end if

for ll_i = 1 to ll_rows
	
	if ll_i = 1 and dw_report.dataobject = 'd_report_op_scheda_singola_sezioni' then
		ll_riga = dw_report.insertrow(0)
		dw_report.setitem(ll_riga,"flag_vis_dt", "5")
		dw_report.setitem(ll_riga, "sottotitolo1","VARIABILI")
		dw_report.setitem(ll_riga, "sottotitolo2","VALORI")

		// prodotto finito
		dw_report.setitem(ll_riga, "cod_prodotto", lds_data.getitemstring(ll_i,1))
		dw_report.setitem(ll_riga, "des_prodotto", lds_data.getitemstring(ll_i,10))
		
		if not isnull(ls_nota_prodotto) and len(ls_nota_prodotto) > 0 then
			// inserisco prima una riga vuota
			ll_riga = dw_report.insertrow(0)
			dw_report.setitem(ll_riga,"flag_tipo_riga","1")
			dw_report.setitem(ll_riga, "des_variabile", "------->> NOTA PRODUZIONE")
			dw_report.setitem(ll_riga, "valore_stringa", ls_nota_prodotto)
			dw_report.setitem(ll_riga, "flag_tipo_dato", "S")
		end if
		// inserisco la nota di dettaglio
		if not isnull(ls_nota_dettaglio) and len(ls_nota_dettaglio) > 0 then
			ll_riga = dw_report.insertrow(0)
			dw_report.setitem(ll_riga,"flag_tipo_riga","1")
			dw_report.setitem(ll_riga, "des_variabile", "------->> NOTA PER IL CLIENTE")
			dw_report.setitem(ll_riga, "valore_stringa", ls_nota_dettaglio)
			dw_report.setitem(ll_riga, "flag_tipo_dato", "S")
		end if
	end if

	ll_riga = dw_report.insertrow(0)
	
	dw_report.setitem(ll_riga, "flag_tipo_riga", "1")
	dw_report.setitem(ll_riga, "cod_prodotto", lds_data.getitemstring(ll_i,1))
	dw_report.setitem(ll_riga, "des_prodotto", lds_data.getitemstring(ll_i,10))
	dw_report.setitem(ll_riga, "cod_variabile", lds_data.getitemstring(ll_i,2))
	dw_report.setitem(ll_riga, "des_variabile", lds_data.getitemstring(ll_i,3))
	dw_report.setitem(ll_riga, "flag_tipo_dato", lds_data.getitemstring(ll_i,4))
	
	if lds_data.getitemstring(ll_i,11) = "L" then
		// è una lista con codice non nascosto, quindi stampo codice e descrizione usando sempre il campo "stringa del report"
		if lds_data.getitemstring(ll_i,13) = "N" then
			choose case upper(lds_data.getitemstring(ll_i,4))
				case "S"
					dw_report.setitem(ll_riga, "valore_stringa", lds_data.getitemstring(ll_i,5))
				case "N"
					if int(lds_data.getitemnumber(ll_i,6)) <> lds_data.getitemnumber(ll_i,6) then
						// ci sono decimali
						dw_report.setitem(ll_riga, "valore_stringa", string(lds_data.getitemnumber(ll_i,6),"###,##0.0###"))
					else
						dw_report.setitem(ll_riga, "valore_stringa", string(lds_data.getitemnumber(ll_i,6),"###,##0"))
					end if
				case "D"
					// lista di date non prevista
			end choose
		else
			dw_report.setitem(ll_riga, "valore_stringa", "")
		end if
		dw_report.setitem(ll_riga, "des_valore", lds_data.getitemstring(ll_i,9))
	else
		// non è una lista, quindi stampo il dato richiesto
//		if lds_data.getitemstring(ll_i,13) = "N" then
			choose case upper(lds_data.getitemstring(ll_i,4))
				case "S"
					dw_report.setitem(ll_riga, "valore_stringa", lds_data.getitemstring(ll_i,5))
				case "N"
					dw_report.setitem(ll_riga, "valore_numero", lds_data.getitemnumber(ll_i,6))
				case "D"
					dw_report.setitem(ll_riga, "valore_datetime", lds_data.getitemdatetime(ll_i,7))
			end choose
//		end if
	end if
next


/* ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
// inizio inserimento optional
luo_prod = create uo_produzione
if luo_prod.uof_get_tipo_det_ven_add( 	fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_comanda, 	ls_cod_tipo_det_ven_addizionali, ls_errore) < 0 then
	g_mb.error(ls_errore)
	return -1
end if
destroy luo_prod

ll_riga = dw_report.insertrow(0)


ls_sql = g_str.format("select cod_prodotto, 	quan_ordine,nota_dettaglio, des_prodotto,dim_x,dim_y from det_ord_ven where 	cod_azienda='$1' and anno_registrazione= $2 and num_registrazione=$3 and num_riga_appartenenza=$4 and cod_tipo_det_ven<> '$5' order by prog_riga_ord_ven ", &
					s_cs_xx.cod_azienda, fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_comanda, ls_cod_tipo_det_ven_addizionali)

ll_rows = guo_functions.uof_crea_datastore( lds_opt, ls_sql)

if ll_rows > 0 then
	ll_riga = dw_report.insertrow(0)

	for ll_i = 1 to ll_rows

		ls_cod_prodotto_opt = lds_opt.getitemstring(ll_i,1)
		ldd_quan_ordine_opt = lds_opt.getitemnumber(ll_i,2)
		ls_nota_opt = lds_opt.getitemstring(ll_i,3)
		ls_des_prodotto_opt = lds_opt.getitemstring(ll_i,4)
		ld_dim_x_riga = lds_opt.getitemnumber(ll_i,5)
		ld_dim_y_riga  = lds_opt.getitemnumber(ll_i,6)

		select cod_misura_mag
		into   :ls_cod_misura_opt
		from   anag_prodotti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_prodotto=:ls_cod_prodotto_opt;
	
		if sqlca.sqlcode < 0 then 
			g_mb.error(g_str.format("Attenzione, errore in ricerca unità di misura per il prodotto $1.~r~nErrore SQL: $2", ls_cod_prodotto_opt, sqlca.sqlerrtext))
			ls_cod_misura_opt = ""
		end if
		
		
		if isnull(ls_nota_opt) then ls_nota_opt = ""

		if ld_dim_y_riga > 0 and not isnull(ld_dim_y_riga) and not isnull(ls_cod_prodotto_opt) and len(ls_cod_prodotto_opt) > 0 then
			select des_variabile
			into :ls_des_variabile
			from tab_des_variabili
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_cod_prodotto_opt and
					cod_variabile = 'DIM_2' and
					flag_ipertech_apice='S';
					
			//se non trovo la des variabile ci metto di default DIM 2
			if ls_des_variabile="" or isnull(ls_des_variabile) then
				ls_des_variabile = "DIM 2"
			end if
			
			ls_nota_opt = ls_des_variabile + "=" + string(ld_dim_y_riga,"###,##0.00") + " - " +  ls_nota_opt
		end if
		
		ls_des_variabile = ""
			
		if ld_dim_x_riga > 0 and not isnull(ld_dim_x_riga) and not isnull(ls_cod_prodotto_opt) and len(ls_cod_prodotto_opt) > 0 then
			select des_variabile
			into :ls_des_variabile
			from tab_des_variabili
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_cod_prodotto_opt and
					cod_variabile = 'DIM_1' and
					flag_ipertech_apice='S';
			
			//se non trovo la des variabile ci metto di default DIM 1
			if ls_des_variabile="" or isnull(ls_des_variabile) then
				ls_des_variabile = "DIM 1"
			end if
			
			ls_nota_opt= ls_des_variabile + "=" + string(ld_dim_x_riga,"###,##0.00") + " - " + ls_nota_opt
		end if
			
		ll_riga = dw_report.insertrow(0)
		if dw_report.dataobject <> 'd_report_op_scheda_singola_sezioni' then
			dw_report.setitem(ll_riga, "flag_tipo_riga", "2")
			dw_report.setitem(ll_riga, "cod_prodotto_optional",ls_cod_prodotto_opt )
			dw_report.setitem(ll_riga, "des_prodotto_optional", ls_des_prodotto_opt + " (" + ls_cod_misura_opt+ ")")
			dw_report.setitem(ll_riga, "quan_optional", ldd_quan_ordine_opt)
			if not isnull(ls_nota_opt) and len(ls_nota_opt) > 0 then
				ll_riga = dw_report.insertrow(0)
				dw_report.setitem(ll_riga, "flag_tipo_riga", "3")
				dw_report.setitem(ll_riga,"nota_riga",ls_nota_opt)
			end if		
		else
			dw_report.setitem(ll_riga, "flag_tipo_riga", "1")
			dw_report.setitem(ll_riga, "des_variabile", g_str.format("OPTIONAL $1",ll_i))
			if not isnull(ls_nota_opt) and len(ls_nota_opt) > 0 then ls_nota_opt = "~r~n" + ls_nota_opt
			dw_report.setitem(ll_riga, "valore_stringa", g_str.format("$1 - $2  $3 $4 $5",ls_cod_prodotto_opt,ls_des_prodotto_opt,ls_cod_misura_opt, string(ldd_quan_ordine_opt,"###,##0.00"), ls_nota_opt  ) )
			dw_report.setitem(ll_riga, "flag_tipo_dato", "S")
		end if
	

	next
end if

// fine inserimento optional


// inserisco la nota prodotto
if dw_report.dataobject <> 'd_report_op_scheda_singola_sezioni' then
	
	// 07-12-2017 Alusistemi non vuole la nota prodotot nel foglio di produzione perchè viene stampato sopra come riferimento cliente
	if dw_report.dataobject <> 'd_report_op_scheda_singola_sez_semplif' then
		if not isnull(ls_nota_prodotto) and len(ls_nota_prodotto) > 0 then
			// inserisco prima una riga vuota
			ll_riga = dw_report.insertrow(0)
			dw_report.setitem(ll_riga,"flag_tipo_riga","3")
			dw_report.setitem(ll_riga,"nota_riga","------->> NOTA PRODUZIONE << -------")
			ll_riga = dw_report.insertrow(0)
			dw_report.setitem(ll_riga,"flag_tipo_riga","3")
			dw_report.setitem(ll_riga,"nota_riga",ls_nota_prodotto)
		end if
	end if
	
	// inserisco la nota di dettaglio
	if not isnull(ls_nota_dettaglio) and len(ls_nota_dettaglio) > 0 then
		ll_riga = dw_report.insertrow(0)
		dw_report.setitem(ll_riga,"flag_tipo_riga","3")
		dw_report.setitem(ll_riga,"nota_riga","------->> NOTA PER IL CLIENTE << -------")
		ll_riga = dw_report.insertrow(0)
		dw_report.setitem(ll_riga,"flag_tipo_riga","3")
		dw_report.setitem(ll_riga,"nota_riga",ls_nota_dettaglio)
	end if
end if
 
/* ********************************************************************************************************
VALORE del flag_vis_dt
0= riga con variabili e relative descrizioni
1= distinta di taglio: riga prodotto
2= distinta di taglio: riga con misure
3= disinta di taglio (solo per il report con scheda affiancato) riga con misure di taglio
4= sottotitolo sfondo grigio / testo nero grassetto (separatore di sezione)

*/
if ls_flag_calcola_dt = "S" then
	if upperbound( lstr_dt )  > 0 then
		ls_cod_prodotto_dt_old = ""
		ll_riga_dt = 0
		
		choose case dw_report.dataobject
			case 'd_report_op_scheda_singola_sezioni'
		
				for ll_i = 1 to upperbound( lstr_dt )
					
					if ll_i = 1 then
						ll_riga_dt = dw_report.insertrow(0)
						dw_report.setitem(ll_riga_dt,"flag_vis_dt", "5")
						dw_report.setitem(ll_riga_dt, "sottotitolo1","DISTINTA DI TAGLIO")
						dw_report.setitem(ll_riga_dt, "sottotitolo2","")
					end if
					
					// EnMe 13.03.2018: Chiesto da Alusistemi in modo che se un prodotto ha q.ta=0 non esca in DT
					if lstr_dt[ll_i].quan_dt = 0 or isnull(lstr_dt[ll_i].quan_dt) then continue
					
					if ls_cod_prodotto_dt_old <> lstr_dt[ll_i].cod_prodotto_dt then
						ll_riga_dt = dw_report.insertrow(0)
						dw_report.setitem(ll_riga_dt,"flag_vis_dt", "1")
						dw_report.setitem(ll_riga_dt,"cod_prodotto_dt", g_str.format("$1 - $2", lstr_dt[ll_i].cod_prodotto_dt, lstr_dt[ll_i].des_prodotto_dt))
						ls_cod_prodotto_dt_old = lstr_dt[ll_i].cod_prodotto_dt
					end if
					
					if lstr_dt[ll_i].quan_dt=0 and lstr_dt[ll_i].misura_dt=0 then continue
		
					ll_riga_dt = dw_report.insertrow(0)
					
					dw_report.setitem(ll_riga_dt,"flag_vis_dt", "2")
					
					dw_report.setitem(ll_riga_dt,"des_dt",lstr_dt[ll_i].descrizione_dt)
					
					ls_appoggio = ""
					if lstr_dt[ll_i].quan_dt <> 0 then
						ls_appoggio = lstr_dt[ll_i].cod_misura_qta_dt
						if ls_flag_azzera_dt <> "S" then
							ls_appoggio += "  " + string(lstr_dt[ll_i].quan_dt)
						end if
						dw_report.setitem(ll_riga_dt,"des_qta_dt", ls_appoggio)
					end if			
					
					ls_appoggio = ""
					if lstr_dt[ll_i].misura_dt <> 0 then
						ls_appoggio =lstr_dt[ll_i].cod_misura_dt
						if ls_flag_azzera_dt<>"S" then
							ls_appoggio += "  " + string(lstr_dt[ll_i].misura_dt)
						end if
						dw_report.setitem(ll_riga_dt,"des_misura_dt", ls_appoggio)
					end if		
				next	
			
			case else	

				for ll_i = 1 to upperbound( lstr_dt )
					
					// EnMe 13.03.2018: Chiesto da Alusistemi in modo che se un prodotto ha q.ta=0 non esca in DT
					if lstr_dt[ll_i].quan_dt = 0 or isnull(lstr_dt[ll_i].quan_dt) then continue

					if ls_cod_prodotto_dt_old <> lstr_dt[ll_i].cod_prodotto_dt then
						if dw_report.rowcount() <= ll_riga_dt then 
							ll_riga_dt = dw_report.insertrow(0)
						else
							ll_riga_dt ++
						end if
						dw_report.setitem(ll_riga_dt,"flag_vis_dt", "1")
						dw_report.setitem(ll_riga_dt,"cod_prodotto_dt", g_str.format("$1 - $2", lstr_dt[ll_i].cod_prodotto_dt, lstr_dt[ll_i].des_prodotto_dt))
						ls_cod_prodotto_dt_old = lstr_dt[ll_i].cod_prodotto_dt
					end if
					
					if lstr_dt[ll_i].quan_dt=0 and lstr_dt[ll_i].misura_dt=0 then continue
		
					if len(lstr_dt[ll_i].descrizione_dt) > 0 then
						if dw_report.rowcount() <= ll_riga_dt then 
							ll_riga_dt = dw_report.insertrow(0)
						else
							ll_riga_dt ++
						end if
						
						dw_report.setitem(ll_riga_dt,"flag_vis_dt", "3")
						dw_report.setitem(ll_riga_dt,"des_dt",lstr_dt[ll_i].descrizione_dt)
					end if
					
					if dw_report.rowcount() <= ll_riga_dt then 
						ll_riga_dt = dw_report.insertrow(0)
					else
						ll_riga_dt ++
					end if
					
					dw_report.setitem(ll_riga_dt,"flag_vis_dt", "2")
					
					ls_appoggio = ""
					if lstr_dt[ll_i].quan_dt <> 0 then
						ls_appoggio = lstr_dt[ll_i].cod_misura_qta_dt
						if ls_flag_azzera_dt <> "S" then
							ls_appoggio += "  " + string(lstr_dt[ll_i].quan_dt,"###,##0.00")
						end if
						dw_report.setitem(ll_riga_dt,"des_qta_dt", ls_appoggio)
					end if			
					
					ls_appoggio = ""
					if lstr_dt[ll_i].misura_dt <> 0 then
						ls_appoggio =lstr_dt[ll_i].cod_misura_dt
						if ls_flag_azzera_dt<>"S" then
							ls_appoggio += "  " + string(lstr_dt[ll_i].misura_dt,"###,##0.00")
						end if
						dw_report.setitem(ll_riga_dt,"des_misura_dt", ls_appoggio)
					end if		
					
					// Aggiunto per Alusistemi 23/01/2018
					if dw_report.dataobject = 'd_report_op_scheda_singola_sez_semplif' then
						dw_report.setitem(dw_report.getrow(),"quan_ordine",ldd_quan_ordine)
//						dw_report.setitem(ll_riga_dt, "matricole", ls_matricole)
					end if
				next	
		end choose
	end if	
end if

// 																						Fine inserimento per distinta di taglio 
//*********************************************************************************************************
//*********************************************************************************************************

if isnull(ldt_data_consegna) then
	dw_report.Modify("data_consegna.text=' '")
	dw_report.Modify("giorno_c.text=' '")
else
	dw_report.Modify("t_13.text='DATA PRONTO'")
	
	select data_pronto	
	into	:ldt_data_consegna
	from  det_ord_ven_prod
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :fi_anno_registrazione and
			num_registrazione = :fl_num_registrazione and
			prog_riga_ord_ven = :fl_prog_riga_comanda and
			cod_reparto = :fs_cod_reparto;
	if sqlca.sqlcode = 0 then
		dw_report.Modify("data_consegna.text='"+ string(date(ldt_data_consegna)) + "'")
		li_risposta = f_giorno_settimana(ldt_data_consegna,ls_giorno)
		dw_report.Modify("giorno_c.text='" + ls_giorno +"'")
	else
		dw_report.Modify("data_consegna.text=''")
		dw_report.Modify("giorno_c.text=''")
	end if		

end if

//16/01/2012 chiesto da Beatrice
//visulaizzare il numero di riga configurata su totale righe configurate
//es se riga 30 e in totale ci sono 5 righe configurate (cioè si ariva max a 50)  -->  3/5

ls_riga_n_di_m = ""

luo_prod = create uo_produzione
ls_riga_n_di_m = luo_prod.uof_get_riga_su_tot_righe(fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_comanda)
destroy luo_prod;

dw_report.Modify("num_ordine.text='"+ string(fl_num_registrazione)+ "   " + ls_riga_n_di_m + "'")

//dw_report.Modify("num_ordine.text='"+ string(fl_num_registrazione)+ "   " + string(fl_num_tenda) +"/" + string(ll_num_max_tenda) + "'")
//FINE MODIFICA -------------------------------------

dw_report.Modify("riga_app.text='" + string(fl_prog_riga_comanda) + "'")


if isnull(ll_num_commessa) then
	dw_report.Modify("num_commessa.text=' '")
else
	dw_report.Modify("num_commessa.text='"+ string(ll_num_commessa) + "'")
end if

if isnull(ls_cod_giro_consegna) then
	dw_report.Modify("des_giro.text=' '")
else
	dw_report.Modify("des_giro.text='"+ ls_des_giro_consegna + "'")
end if

ls_rag_soc_1 = wf_elabora(ls_rag_soc_1)

dw_report.Modify("cod_cliente.text='"+ ls_cod_cliente + "'")
if ls_flag_nascondi_cliente = "N" then 
	dw_report.Modify("rag_soc_1.text='" + ls_rag_soc_1 + "'")
else
	dw_report.Modify("rag_soc_1.text=''")
end if

//---------------------------------------------------------------------------------------------
//Donato 14/02/2012
//colonna dinamica ETIC
ls_chiave_etic = ""
if ls_cod_cliente<>"" and not isnull(ls_cod_cliente) then
	ls_chiave_etic = wf_get_colonna_dinamica(ls_cod_cliente)
end if
dw_report.Modify("t_chiave.text='"+ ls_chiave_etic + "'")
//---------------------------------------------------------------------------------------------

if isnull(ls_alias) then
	dw_report.Modify("rag_soc_2.text=' '")
else
	ls_alias = wf_elabora(ls_alias)
	dw_report.Modify("rag_soc_2.text='" + ls_alias + "'")
end if

if ls_flag_urgente = "S" and not isnull(ls_flag_urgente) then
	dw_report.Modify("l_urgente.visible=1")
else
	dw_report.Modify("l_urgente.visible=0")
end if	

if isnull(ls_indirizzo) then
	dw_report.Modify("indirizzo.text=' '")
else
	ls_indirizzo = wf_elabora(ls_indirizzo)
	dw_report.Modify("indirizzo.text='"+ ls_indirizzo + "'")
end if

if isnull(ls_cap) then ls_cap = ' '
dw_report.Modify("cap.text='"+ ls_cap + "'")


if isnull(ls_localita) then ls_localita = ' '
ls_localita = wf_elabora(ls_localita)
dw_report.Modify("localita.text='"+ ls_localita + "'")

if isnull(ls_provincia) then ls_provincia = ' '
dw_report.Modify("provincia.text='"+ ls_provincia + "'")

if isnull(ls_telefono) then ls_telefono = ' '
dw_report.Modify("telefono.text='"+ ls_telefono + "'")

if isnull(ls_fax) then ls_fax = ' '
dw_report.Modify("fax.text='"+ ls_fax + "'")

if isnull(ls_des_reparto) then ls_des_reparto=' '
dw_report.Modify("des_reparto.text='"+ ls_des_reparto + "'")

if not isnull(fs_cod_reparto) then
	dw_report.Object.p_1.Filename = s_cs_xx.volume + s_cs_xx.risorse + fs_cod_reparto + ".bmp"
end if

if dw_report.dataobject = 'd_report_op_scheda_singola_sez_semplif' then
//	if isnull(ls_nota_prodotto) then ls_nota_prodotto = ' '
	if isnull(ls_num_ord_cliente) then ls_num_ord_cliente = ' '
	dw_report.Modify("num_ord_cliente.text='"+ ls_num_ord_cliente + "'")
end if

if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "STAMPA_ORD_PROD", ls_null, ldt_data_consegna, ls_nota_testata,ls_nota_piede, ls_nota_video) < 0 then
	g_mb.error(ls_nota_testata)
end if

if isnull(ls_des_imballo) then ls_des_imballo = ' '
dw_report.Modify("des_imballo.text=''")
if not isnull(ls_des_imballo) and len(ls_des_imballo) > 0 then
	dw_report.Modify("des_imballo.text='"+ ls_des_imballo + "'")
end if


if ls_flag_allegati="S" then dw_report.Modify("vedi_allegato.visible=1")

if ls_flag_tassativo="S" then dw_report.Modify("flag_tassativo.visible=1")

select 	progr_det_produzione,
			data_pronto
into   :ll_progr_det_produzione,
		:ldt_data_pronto_fasi
from   det_ordini_produzione
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione
and    prog_riga_ord_ven=:fl_prog_riga_comanda
and    cod_reparto=:fs_cod_reparto;


//VISUALIZZA LINGUA DEL CLIENTE
//##############################################
if not isnull(ls_cod_lingua) and ls_cod_lingua<>"" then
	dw_report.Modify("test_barcode_t.text='Lingua Cliente: "+ ls_cod_lingua + "'")
else
	dw_report.Modify("test_barcode_t.text=''")
end if
//##############################################


choose case sqlca.sqlcode
	
	case 0
		
		li_risposta = f_font_barcode(ls_bfo,li_bco,ls_errore)
		
		if li_risposta < 0 then 
			messagebox("SEP","Report 1 tende da sole: " + ls_errore,stopsign!)
			return -1
		end if
		
		dw_report.Modify("barcode.text='*"+is_CSB+ string(ll_progr_det_produzione) +is_CSB+ "*'")
		dw_report.Modify("barcode.Font.Face='" + ls_bfo + "'")
		dw_report.Modify("barcode.Font.Height= -" + string(li_bco) )
		
	case is < 0
		messagebox("Apice","Errore durante la compilazione del report_1 (f_report_1), lettura tabella det_ordini_produzione. Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
		return -1
	
	case 100
		dw_report.Modify("barcode.text=''")
		
end choose


if not isnull(ldt_data_pronto_fasi) and year(date(ldt_data_pronto_fasi))>1950 then
	//uso la data pronto memorizzata nella fasi
	dw_report.Modify("data_pronto.text='"+ string(date(ldt_data_pronto_fasi), "dd/mm/yyyy") +"'")
	
	li_risposta = f_giorno_settimana(ldt_data_pronto_fasi, ls_giorno)
	dw_report.Modify("giorno_p.text='" + ls_giorno +"'")
else
	dw_report.Modify("data_pronto.text=' '")
	dw_report.Modify("giorno_p.text=' '")
end if


//Donato 27/12/2011
//info aggiuntive su report OL (stabilimento successivo e lista reparti coinvolti)
luo_prod = create uo_produzione
luo_prod.il_sessione_tes_stampa = il_sessione
luo_prod.ib_altro_stabilimento = false
li_risposta = luo_prod.uof_get_ordine_reparti_2(	fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_comanda, fs_cod_reparto, &
																ls_lista_reparti_coinvolti, ls_stabilim_prossimo, ls_reparto_prossimo)

//metti in grassetto se riferito a stabilimento diverso di quello del reparto corrente
if luo_prod.ib_altro_stabilimento then
	dw_report.object.stab_succ_t.font.weight = 700
else
	dw_report.object.stab_succ_t.font.weight = 400
end if
//--------------------------------------------------------------------------------------------

//se la stampa si riferisce all'ultimo reparto della lista cambia la label dello stabilimento successivo
try
	if li_risposta = 1 then
		//ultimo reparto
		dw_report.object.stab_succ_label_t.text = "Stabilim. partenza merce"
	else
		dw_report.object.stab_succ_label_t.text = "Stabilim. successivo"
	end if
catch (runtimeerror e)
end try
											
destroy luo_prod;

dw_report.Modify("stab_succ_t.text='"+ls_stabilim_prossimo+"'")
dw_report.Modify("reparti_ordine_t.text='"+ls_lista_reparti_coinvolti+"'")
//-----------------------------------------------------
dw_report.setitem(dw_report.rowcount(),"matricole",ls_matricole)

return 0

end function

public function integer wf_report_1_leggi_dt (string as_cod_prodotto_finito, string as_cod_versione_finito, long al_anno_dt, long al_num_dt, long al_prog_dt, ref s_report_ol_produzione_dt astr_dt[], ref string as_errore);// Funzione di caricamento della DT calcolata
string 	ls_flag_non_calcolare_dt, ls_sql, ls_errore,ls_cod_prodotto_padre,ls_cod_prodotto_figlio[], ls_cod_versione, &
			ls_des_distinta_taglio[],ls_b_um_quantita,ls_b_um_misura, ls_cod_versione_figlio[], ls_um_quantita[], ls_um_misura[], &
			ls_cod_prodotto_variante, ls_des_prodotto_figlio[]
			
long		ll_num_taglio, ll_rows, ll_y,ll_num_sequenza, ll_progressivo, ll_prog_calcolo, ll_i, ll_prog_riga_ord_ven

dec{4}	ld_quantita[],ld_misura[]

s_report_ol_produzione_dt lstr_dt[]

datastore lds_dt



// -------------------------------------------------------------------------
astr_dt[] = lstr_dt[]

select 	flag_non_calcolare_dt
into   		:ls_flag_non_calcolare_dt
from   	distinta_padri
where  	cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :as_cod_prodotto_finito and
			cod_versione = :as_cod_versione_finito;

if sqlca.sqlcode< 0 then
	as_errore = g_str.format("Lettura Distinta Taglio, errore in lettura distinta_padri.flag_non_calcolare_dt. $1",sqlca.sqlerrtext)
	return -1
end if

ll_num_taglio=0

if ls_flag_non_calcolare_dt = 'N' or isnull(ls_flag_non_calcolare_dt) then
				
	select 	count(*)
	into   		:ll_num_taglio
	from   	distinte_taglio_calcolate
	where  	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione = :al_anno_dt and
				num_registrazione = :al_num_dt and
				prog_riga_ord_ven = :al_prog_dt;
	
	if sqlca.sqlcode < 0 then 
		as_errore = g_str.format("Lettura Distinta Taglio, errore in select from distinte_taglio_calcolate. $1",sqlca.sqlerrtext)
		return -1
	end if

	if ll_num_taglio > 0 then
		
//		ls_sql = g_str.format("select  cod_prodotto_padre,  num_sequenza,  cod_prodotto_figlio,  cod_versione_figlio,  cod_versione,  progressivo,  prog_calcolo,  quantita,  misura from distinte_taglio_calcolate where   cod_azienda='$4' and     anno_registrazione = $1 and     num_registrazione = $2 and     prog_riga_ord_ven = $3 order by prog_calcolo",al_anno_dt,al_num_dt,al_prog_dt, s_cs_xx.cod_azienda)
//		ls_sql = g_str.format("select  A.cod_prodotto_padre,  A.num_sequenza,  A.cod_prodotto_figlio,  A.cod_versione_figlio,  A.cod_versione,  A.progressivo,  A.prog_calcolo,  A.quantita,  A.misura from distinte_taglio_calcolate A join tab_distinte_taglio B on A.cod_azienda=B.cod_azienda and A.cod_prodotto_padre=B.cod_prodotto_padre and  A.num_sequenza=B.num_sequenza and A.cod_prodotto_figlio=B.cod_prodotto_figlio and A.cod_versione_figlio=B.cod_versione_figlio and A.cod_versione = B.cod_versione and A.progressivo=B.progressivo where   A.cod_azienda='$4' and  A.anno_registrazione = $1 and A.num_registrazione = $2 and A.prog_riga_ord_ven = $3 order by A.prog_calcolo, B.num_ordinamento",al_anno_dt,al_num_dt,al_prog_dt, s_cs_xx.cod_azienda)

ls_sql = g_str.format(" &
select  A.cod_prodotto_padre,  A.num_sequenza,  A.cod_prodotto_figlio,  A.cod_versione_figlio,  A.cod_versione,  A.progressivo,  A.prog_calcolo,  A.quantita,  A.misura,  B.num_ordinamento, A.prog_riga_ord_ven &
from distinte_taglio_calcolate A  &
join tab_distinte_taglio B on A.cod_azienda=B.cod_azienda and A.cod_prodotto_padre=B.cod_prodotto_padre and  A.num_sequenza=B.num_sequenza and A.cod_prodotto_figlio=B.cod_prodotto_figlio and A.cod_versione_figlio=B.cod_versione_figlio and A.cod_versione = B.cod_versione and A.progressivo=B.progressivo  &
where   A.cod_azienda='$4' and  A.anno_registrazione = $1 and A.num_registrazione = $2 and A.prog_riga_ord_ven = $3  &
union &
select  A.cod_prodotto_padre,  A.num_sequenza,  A.cod_prodotto_figlio,  A.cod_versione_figlio,  A.cod_versione,  A.progressivo,  A.prog_calcolo,  A.quantita,  A.misura,  B.num_ordinamento, A.prog_riga_ord_ven &
from distinte_taglio_calcolate A  &
join tab_distinte_taglio B on A.cod_azienda=B.cod_azienda and A.cod_prodotto_padre=B.cod_prodotto_padre and  A.num_sequenza=B.num_sequenza and A.cod_prodotto_figlio=B.cod_prodotto_figlio and A.cod_versione_figlio=B.cod_versione_figlio and A.cod_versione = B.cod_versione and A.progressivo=B.progressivo  &
join det_ord_ven O on O.cod_azienda = A.cod_azienda and O.anno_registrazione = A.anno_registrazione and O.num_registrazione = A.num_registrazione and O.prog_riga_ord_ven = A.prog_riga_ord_ven  &
where   A.cod_azienda='$4' and  O.anno_registrazione = $1 and O.num_registrazione = $2 and O.num_riga_appartenenza = $3  &
order by 7, 10 &
",al_anno_dt,al_num_dt,al_prog_dt, s_cs_xx.cod_azienda)


		
		ll_rows = guo_functions.uof_crea_datastore( lds_dt, ls_sql, ls_errore)
		if ll_rows < 0 then 
			as_errore = g_str.format("Errore in caricamento datastore distinte taglio calcolate. $1", ls_errore)
			return -1
		end if
		
		for ll_i = 1 to ll_rows
			
			ls_cod_prodotto_padre = lds_dt.getitemstring(ll_i, 1)
			ll_num_sequenza = lds_dt.getitemnumber(ll_i, 2)

			ls_cod_prodotto_figlio[ll_i] = lds_dt.getitemstring(ll_i, 3)
			astr_dt[ll_i].cod_prodotto_dt = lds_dt.getitemstring(ll_i, 3)

			ls_cod_versione_figlio[ll_i]  = lds_dt.getitemstring(ll_i, 4)
			astr_dt[ll_i].cod_versione_prodotto_dt =  lds_dt.getitemstring(ll_i, 3)

			ls_cod_versione  = lds_dt.getitemstring(ll_i, 5)
			ll_progressivo = lds_dt.getitemnumber(ll_i, 6)
			ll_prog_calcolo  = lds_dt.getitemnumber(ll_i, 7)
			
			ld_quantita[ll_i]  = lds_dt.getitemnumber(ll_i, 8)
			ld_misura[ll_i]  = lds_dt.getitemnumber(ll_i, 9)
			ld_quantita[ll_i] = round(ld_quantita[ll_i],2)
			ld_misura[ll_i] = round(ld_misura[ll_i],2)
			
			astr_dt[ll_i].quan_dt = ld_quantita[ll_i]
			astr_dt[ll_i].misura_dt = ld_misura[ll_i]
			
			
			select des_distinta_taglio,
					 um_quantita,
					 um_misura
			into   :ls_des_distinta_taglio[ll_i],
					 :ls_b_um_quantita,
					 :ls_b_um_misura
			from   tab_distinte_taglio
			where  cod_azienda=:s_cs_xx.cod_azienda and 
					cod_prodotto_padre=:ls_cod_prodotto_padre and
					num_sequenza=:ll_num_sequenza and 
					cod_prodotto_figlio=:ls_cod_prodotto_figlio[ll_i] and 
					cod_versione_figlio=:ls_cod_versione_figlio[ll_i] and 
					cod_versione=:ls_cod_versione and 
					progressivo=:ll_progressivo;
					 
			if sqlca.sqlcode < 0 then 
				as_errore = g_str.format("wf_report_1_leggi_dt(): Errore in lettura distinta taglio. Padre=$1 Versione=$2 Figlio=$3 Progressivo=$4 Errore SQL=$5",ls_cod_prodotto_padre, ls_cod_versione, ls_cod_prodotto_figlio[ll_i] , ll_progressivo, sqlca.sqlerrtext)
				return -1
			end if
			
			if ls_b_um_quantita = "" then setnull(ls_b_um_quantita)
			if ls_b_um_misura = "" then setnull(ls_b_um_misura)
			ls_um_quantita[ll_i] = ls_b_um_quantita
			ls_um_misura[ll_i] = ls_b_um_misura
			astr_dt[ll_i].cod_misura_qta_dt =  ls_um_quantita[ll_i] 
			astr_dt[ll_i].cod_misura_dt  =  ls_um_misura[ll_i]
			astr_dt[ll_i].descrizione_dt = ls_des_distinta_taglio[ll_i]
			ll_prog_riga_ord_ven = lds_dt.getitemnumber(ll_i, 11)
					 
			select 	cod_prodotto
			into   		:ls_cod_prodotto_variante
			from   	varianti_det_ord_ven
			where  	cod_azienda=:s_cs_xx.cod_azienda and    
						anno_registrazione = :al_anno_dt and    
						num_registrazione = :al_num_dt and    
						prog_riga_ord_ven = :ll_prog_riga_ord_ven and    
						cod_prodotto_padre = :ls_cod_prodotto_padre and    
						num_sequenza = :ll_num_sequenza and    
						cod_prodotto_figlio = :ls_cod_prodotto_figlio[ll_i] and   
						cod_versione_figlio = :ls_cod_versione_figlio[ll_i] and    
						cod_versione = :ls_cod_versione;
			
			if sqlca.sqlcode < 0 then 
				as_errore = g_str.format("wf_report_1_leggi_dt: Errore in lettura variante. Ordine $3/$4/$5  Prodotto Figlio=$1 Errore SQL=$2", ls_cod_prodotto_figlio[ll_i] , sqlca.sqlerrtext, al_anno_dt,al_num_dt, al_prog_dt)
				return -1
			end if
			
			if not isnull(ls_cod_prodotto_variante) and ls_cod_prodotto_variante<>"" then
				ls_cod_prodotto_figlio[ll_i] = ls_cod_prodotto_variante
				astr_dt[ll_i].cod_prodotto_dt = ls_cod_prodotto_variante
			end if
			
			select des_prodotto
			into   :ls_des_prodotto_figlio[ll_i]
			from   anag_prodotti
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:ls_cod_prodotto_figlio[ll_i];
			
			if sqlca.sqlcode < 0 then 
				as_errore = g_str.format("wf_report_1_leggi_dt: Errore in anag_prodotti. Prodotto=$1   Errore= $2",ls_cod_prodotto_figlio[ll_i], sqlca.sqlerrtext )
				return -1
			end if
			
			astr_dt[ll_i].des_prodotto_dt = ls_des_prodotto_figlio[ll_i]
			
//			ll_i++
			setnull(ls_cod_prodotto_variante)
			
		next
	
		destroy lds_dt
		
	end if
end if

end function

public function integer f_calcola_dt (integer fi_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, string fs_cod_prodotto, string fs_cod_versione, string fs_tipo_ordinamento_dt, ref string fs_errore);// Funzione che calcola le distinte di taglio per gli ordini di vendita
// Autore: Diego Ferrari
// data creazione: 24/12/2001

// 1) Si verifica che il prodotto finito in questione in distinta padri abbia il flag "non calcolare DT" impostato a 'N'
//    se è a si si esce dalla funzione di calcolo.
// 2) si scorre la distinta base del prodotto finito individuando TUTTE le distinte di taglio associate ad 
//    ogni ramo.
// 3) per ogni distinta di taglio si verifica il flag tutti i comandi se è a si si procede con lo step 4 se invece 
//    è a 'N' si confronta ogni comando della distinta di taglio con il comando presente nella comp_det_ord_ven
//		se nessuno dei comandi coincide con quello della comp_det_ord_ven si prosegue con gli altri rami di distinta.
// 4) Si esegue la valoriazzione della distinta di taglio: prima si sostituiscono le variabili e poi si da tutto al parser
// 5) Si inseriscono i dati nella tabella di buffer denominata distinte_taglio_calcolate
// 6) Fine funzione.

/* --- EnMe 06-03-2017 aggiunto tipo ordinamento DT
	S=ordinamento classico secondo la sequenza della disinta base
	N=nuovo sistema secondo la colonna num_ordinamento_dt presente in disinta base, ovvero ordinamento assoluto
*/
string		ls_cod_prodotto_figlio, ls_errore,ls_cod_prodotto_inserito, ls_cod_tessuto,& 
			ls_test, ls_cod_prodotto_variante,ls_flag_materia_prima,ls_flag_materia_prima_variante, ls_risultato_quantita,& 
			ls_flag_non_calcolare_dt,ls_formula_quantita,ls_formula_misura,ls_tutti_comandi,ls_luce_finita,&
			ls_risultato_misura,ls_cod_comando_ord,ls_cod_comando,ls_cod_versione_figlio, ls_sql, ls_cod_versione_inserito, ls_cod_versione_variante
			
long		ll_num_figli,ll_num_sequenza,ll_num_righe,ll_progressivo,ll_prog_calcolo, ll_num_ordinamento_dt, ll_cont

integer	li_risposta

double	ld_dim_x,ld_dim_y,ld_dim_z,ld_dim_t,ld_alt_mantovana,ld_quan_utilizzo,ld_quan_ordine,& 
			ld_quan_utilizzo_variante,ld_misura,ld_quantita
			
boolean	lb_controllo,lb_test

datastore lds_righe_distinta
dec{4}	ld_risultato

uo_formule_calcolo		luo_formule

//-----------------------------------------

if isnull(fs_tipo_ordinamento_dt) then fs_tipo_ordinamento_dt = "S"

select dim_x,
		 dim_y,
		 dim_z,
		 dim_t,
		 cod_tessuto,
		 alt_mantovana,
		 flag_misura_luce_finita,
		 cod_comando
into   :ld_dim_x,
		 :ld_dim_y,
		 :ld_dim_z,
		 :ld_dim_t,
		 :ls_cod_tessuto,
		 :ld_alt_mantovana,
		 :ls_luce_finita,
		 :ls_cod_comando_ord
from   comp_det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione
and    prog_riga_ord_ven=:fl_prog_riga_ord_ven;

if sqlca.sqlcode < 0 then
	fs_errore="Errore in lettura tabella comp_det_ord_ven~r~n"+ sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_tessuto) then ls_cod_tessuto = ""
if isnull(ls_luce_finita) then ls_luce_finita = ""
if isnull(ls_cod_comando_ord) then ls_cod_comando_ord = ""

select quan_ordine
into   :ld_quan_ordine
from   det_ord_ven
where  cod_azienda=:s_cs_xx.cod_azienda
and    anno_registrazione=:fi_anno_registrazione
and    num_registrazione=:fl_num_registrazione
and    prog_riga_ord_ven=:fl_prog_riga_ord_ven;

if sqlca.sqlcode < 0 then
	fs_errore="Errore in lettura tabella riga ordine "+string(fi_anno_registrazione)+ "/" + string(fl_num_registrazione)+ "/" + string(fl_prog_riga_ord_ven) + "~r~n"+ sqlca.sqlerrtext
	return -1
end if


ll_num_figli = 1

ls_sql = " SELECT distinta.cod_prodotto_padre, distinta.cod_prodotto_figlio, distinta.cod_versione, distinta.cod_versione_figlio,   distinta.quan_utilizzo,   distinta.num_sequenza,   distinta.flag_materia_prima, " + &
			" anag_prodotti.des_prodotto, anag_prodotti.cod_misura_mag, distinta.flag_ramo_descrittivo, distinta.quan_tecnica, distinta.cod_formula_quan_utilizzo, distinta.cod_formula_quan_tecnica, distinta.cod_formula_prod_figlio, "+ &
			" distinta.cod_formula_vers_figlio, anag_prodotti.flag_blocco, distinta.num_ordinamento_dt " + &
			" FROM distinta " + &
			" LEFT OUTER JOIN anag_prodotti ON	distinta.cod_azienda = anag_prodotti.cod_azienda and distinta.cod_prodotto_figlio = anag_prodotti.cod_prodotto " + &
			g_str.format(" WHERE	distinta.cod_azienda = '$1'  AND  cod_prodotto_padre = '$2'  AND  cod_versione = '$3' ",s_cs_xx.cod_azienda,fs_cod_prodotto,fs_cod_versione) + &
			" ORDER BY distinta.num_sequenza "

ll_num_righe = guo_functions.uof_crea_datastore(lds_righe_distinta, ls_sql)

if ll_num_righe < 0 then
	fs_errore=g_str.format("Calcolo DT (f_calcola_dt): Errore nella lettura distinta della riga ordine $1-$2-$3~r~nPADRE:$4  FIGLIO:$5 ~r~n"+ sqlca.sqlerrtext,fi_anno_registrazione, fl_num_registrazione,fl_prog_riga_ord_ven, fs_cod_prodotto,ls_cod_prodotto_figlio) 
	return -1
end if
//lds_righe_distinta = Create DataStore
//lds_righe_distinta.DataObject = "d_data_store_distinta"
//lds_righe_distinta.SetTransObject(sqlca)
//ll_num_righe = lds_righe_distinta.Retrieve()

select max(prog_calcolo)
into :ll_prog_calcolo
from distinte_taglio_calcolate
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :fi_anno_registrazione and
		num_registrazione = :fl_num_registrazione and
		prog_riga_ord_ven = :fl_prog_riga_ord_ven;

if isnull(ll_prog_calcolo) then 	ll_prog_calcolo = 0
	
for ll_num_figli=1 to ll_num_righe
	ls_cod_prodotto_figlio = lds_righe_distinta.getitemstring(ll_num_figli,2)
	ls_cod_versione_figlio = lds_righe_distinta.getitemstring(ll_num_figli,4)
	ls_flag_materia_prima  = lds_righe_distinta.getitemstring(ll_num_figli,7)	
	ll_num_sequenza  = lds_righe_distinta.getitemnumber(ll_num_figli,6)	
	ld_quan_utilizzo = lds_righe_distinta.getitemnumber(ll_num_figli,5)	
	ls_cod_prodotto_inserito = ls_cod_prodotto_figlio
	ls_cod_versione_inserito = ls_cod_versione_figlio
	ll_num_ordinamento_dt = lds_righe_distinta.getitemnumber(ll_num_figli,17)	

	select cod_prodotto,
			cod_versione_variante,
			 flag_materia_prima,
			 quan_utilizzo
	into	:ls_cod_prodotto_variante,
			:ls_cod_versione_variante,	
			:ls_flag_materia_prima_variante,
			:ld_quan_utilizzo_variante
	from   varianti_det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
		    anno_registrazione = :fi_anno_registrazione and
		    num_registrazione = :fl_num_registrazione and
		    prog_riga_ord_ven = :fl_prog_riga_ord_ven and
		    cod_prodotto_padre = :fs_cod_prodotto and
		    cod_prodotto_figlio = :ls_cod_prodotto_figlio and
		    cod_versione_figlio = :ls_cod_versione_figlio and
		    cod_versione = :fs_cod_versione and
		    num_sequenza = :ll_num_sequenza;

   if sqlca.sqlcode < 0 then
		fs_errore="Errore in lettura variante riga ordine "+string(fi_anno_registrazione)+ "/" + string(fl_num_registrazione)+ "/" + string(fl_prog_riga_ord_ven) + "  Padre "+fs_cod_prodotto+" Figlio " + ls_cod_prodotto_figlio + "~r~n"+ sqlca.sqlerrtext
		return -1
	end if

	if sqlca.sqlcode <> 100 then
		ls_cod_prodotto_inserito = ls_cod_prodotto_variante   
		ls_cod_versione_inserito = ls_cod_versione_variante
		ls_flag_materia_prima = ls_flag_materia_prima_variante
		ld_quan_utilizzo=ld_quan_utilizzo_variante
	end if		

//********************
// vengono scansionate tutte le distinte di taglio legate al ramo di distinta base corrente
//********************
	declare r_distinte_taglio cursor for
	select  progressivo,
			  formula_quantita,
			  formula_misura,
			  tutti_comandi
	from    tab_distinte_taglio
	where   cod_azienda = :s_cs_xx.cod_azienda and
	  	     cod_prodotto_padre = :fs_cod_prodotto and
	  	     num_sequenza = :ll_num_sequenza and
		     cod_prodotto_figlio = :ls_cod_prodotto_figlio and
		     cod_versione_figlio = :ls_cod_versione_figlio and
		     cod_versione = :fs_cod_versione
	order by num_ordinamento;
	
	open r_distinte_taglio;
	if sqlca.sqlcode < 0 then
		fs_errore="Errore in open cursore r_distinte_taglio~r~n" + sqlca.sqlerrtext
		close r_distinte_taglio;
		return -1
	end if
	
	do while true
		fetch r_distinte_taglio
		into  :ll_progressivo,
				:ls_formula_quantita,
				:ls_formula_misura,
				:ls_tutti_comandi;
		
		if sqlca.sqlcode < 0 then
			fs_errore="Errore in fetch cursore r_distinte_taglio~r~n " + sqlca.sqlerrtext
			close r_distinte_taglio;
			return -1
		end if
		
		if sqlca.sqlcode = 100 then exit


		lb_test = false 						// azzera flag di controllo per il successivo inserimento in tabella
		
		ls_formula_quantita = trim(ls_formula_quantita)
		ls_formula_misura = trim(ls_formula_misura)
		
		//CREAZIONE OGGETTO FORMULE #################################################
		luo_formule = CREATE uo_formule_calcolo
		
		luo_formule.ii_anno_reg_ord_ven = fi_anno_registrazione
		luo_formule.il_num_reg_ord_ven = fl_num_registrazione
		luo_formule.il_prog_riga_ord_ven = fl_prog_riga_ord_ven
		
		// ### aggiunto da Enrico 15/03/2016
		// senza di questo non riesce ad andare a prendere le variabili
		luo_formule.is_cod_prodotto_padre = fs_cod_prodotto
		luo_formule.is_cod_versione_padre = fs_cod_versione
		luo_formule.is_cod_prodotto_figlio = ls_cod_prodotto_figlio
		luo_formule.is_cod_versione_figlio = ls_cod_versione_figlio
		luo_formule.il_num_sequenza = ll_num_sequenza
		luo_formule.is_contesto = "ORD_VEN"
		
		//######################################################################
		
		ld_quantita = 0
		ld_misura = 0
		
		if ls_tutti_comandi = 'S' then

			//***************************************************************************
			//*****************       risolve formula quantita			********************
			//***************************************************************************
			
			if not isnull(ls_formula_quantita) and ls_formula_quantita<>"" then 
				
				
				ld_risultato = 0
				if luo_formule.uof_calcola_formula_dtaglio( ls_formula_quantita, false, fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ref ld_risultato, ref fs_errore)	 < 0 then			
					fs_errore = "(F.QUANTITA DT) Padre: "+fs_cod_prodotto+" / " + fs_cod_versione +"- Figlio: "+ls_cod_prodotto_figlio+" / " +ls_cod_versione_figlio + " "+ fs_errore
					close r_distinte_taglio;
					destroy luo_formule
					return -1
				end if
				
				ld_quantita = ld_risultato
			
				
				lb_test = true          // imposta flag di controllo per inserimento
				
			end if

			//********************************************************************************
			//****************        risolve formula misura			*************************
			//********************************************************************************

			if not isnull(ls_formula_misura) and ls_formula_misura<>"" then 

				ld_risultato = 0
				if luo_formule.uof_calcola_formula_dtaglio( ls_formula_misura, false, fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ref ld_risultato, ref fs_errore)	 < 0 then			
					
					fs_errore = "(F.MISURA DT) Padre: "+fs_cod_prodotto+" / " + fs_cod_versione +"- Figlio: "+ls_cod_prodotto_figlio+" / " +ls_cod_versione_figlio + " "+ fs_errore
					close r_distinte_taglio;
					destroy luo_formule
					return -1
				end if
				ld_misura = ld_risultato
			
				lb_test = true          // imposta flag di controllo per inserimento
				
			end if
			 			
		else
			//*********************************************************
			//*****   in questo caso esegue il confronto dei comandi	
			//*********************************************************
			
			lb_controllo=false
			
			declare r_distinte_taglio_comandi cursor for
			select  	cod_comando
			from    	distinte_taglio_comandi
			where   	cod_azienda=:s_cs_xx.cod_azienda and
			     	  	cod_prodotto_padre=:fs_cod_prodotto and
			     	  	num_sequenza=:ll_num_sequenza and
			     		cod_prodotto_figlio=:ls_cod_prodotto_figlio and
			     		cod_versione_figlio=:ls_cod_versione_figlio and
			     		cod_versione=:fs_cod_versione and
			     		progressivo=:ll_progressivo; 
			
			open r_distinte_taglio_comandi;
			if sqlca.sqlcode < 0 then
				fs_errore="Errore in open cursore r_distinte_taglio_comandi~r~n" + sqlca.sqlerrtext
				close r_distinte_taglio;
				return -1
			end if
			
			do while true
				fetch r_distinte_taglio_comandi 
				into  :ls_cod_comando;
				
				if sqlca.sqlcode < 0 then
					fs_errore="Errore in fetch cursore r_distinte_taglio_comandi~r~n" + sqlca.sqlerrtext
					close r_distinte_taglio;
					close r_distinte_taglio_comandi;
					return -1
				end if
		
				if sqlca.sqlcode = 100 then exit
				
				if ls_cod_comando = ls_cod_comando_ord then 
					lb_controllo = true
					exit
				end if
				
			loop
			
			close r_distinte_taglio_comandi;
			
			if lb_controllo = true then //*************************** ESEGUE LO STESSO CALCOLO COME SOPRA

				//***************************************************************************
				//*****************       risolve formula quantita			********************
				//***************************************************************************

				if not isnull(ls_formula_quantita) and ls_formula_quantita<>"" then 
					ld_risultato = 0
					if luo_formule.uof_calcola_formula_dtaglio( ls_formula_quantita, false, fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ref ld_risultato, ref fs_errore)	 < 0 then			
						
						fs_errore = "(F.QUANTITA DT) Padre: "+fs_cod_prodotto+" / " + fs_cod_versione +"- Figlio: "+ls_cod_prodotto_figlio+" / " +ls_cod_versione_figlio + " "+ fs_errore
						close r_distinte_taglio;
						destroy luo_formule
						return -1
					end if
					ld_quantita = ld_risultato
					
					lb_test = true          // imposta flag di controllo per inserimento
					
				end if
	
				//********************************************************************************
				//****************        risolve formula misura			*************************
				//********************************************************************************
	
				if not isnull(ls_formula_misura) and ls_formula_misura<>"" then 
					ld_risultato = 0
					if luo_formule.uof_calcola_formula_dtaglio( ls_formula_misura, false, fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ref ld_risultato, ref fs_errore)	 < 0 then			
						fs_errore = "(F.MISURA DT) Padre: "+fs_cod_prodotto+" / " + fs_cod_versione +"- Figlio: "+ls_cod_prodotto_figlio+" / " +ls_cod_versione_figlio + " "+ fs_errore
						close r_distinte_taglio;
						destroy luo_formule
						return -1
					end if
					ld_misura = ld_risultato
					
					lb_test = true          // imposta flag di controllo per inserimento
					
				end if
				
			end if                      //******************************* FINE DEL CALCOLO COME SOPRA
			
			
		end if
		
		//DISTRUZIONE OGGETTO FORMULE ###############################################
		destroy luo_formule
		//######################################################################
		
		
		if lb_test = false then	// SE NON E' STATA CALCOLATA ALMENO UNA DELLE 2 FORMULE ALLORA IMPOSTA I VALORI A 0
			
			ld_quantita = 0
			ld_misura = 0
		
		end if
		
		if fs_tipo_ordinamento_dt = "S" then
			ll_prog_calcolo++
		else
			ll_prog_calcolo = ll_num_ordinamento_dt
		end if

		if lb_test = true then
		
			insert into distinte_taglio_calcolate
			(cod_azienda,
			 anno_registrazione,
			 num_registrazione,
			 prog_riga_ord_ven,
			 cod_prodotto_padre,
			 num_sequenza,
			 cod_prodotto_figlio,
			 cod_versione_figlio,
			 cod_versione,
			 progressivo,
			 prog_calcolo,
			 quantita,
			 misura)
			values
			(:s_cs_xx.cod_azienda,
			 :fi_anno_registrazione,
			 :fl_num_registrazione,
			 :fl_prog_riga_ord_ven,
			 :fs_cod_prodotto,
			 :ll_num_sequenza,
			 :ls_cod_prodotto_figlio,
			 :ls_cod_versione_figlio,
			 :fs_cod_versione,
			 :ll_progressivo,
			 :ll_prog_calcolo,
			 :ld_quantita,
			 :ld_misura);
			 
			if sqlca.sqlcode < 0 then
				fs_errore="Attenzione! Nell'ordine " + string(fi_anno_registrazione)+"/"+string(fl_num_registrazione) + & 
				" riga "+string(fl_prog_riga_ord_ven)+ & 
				" c'è un prodotto che presenta una distinta di taglio appartenente ad un ramo ripetuto più di una volta all'interno della distinta base. Verificare. Errore sul DB: " + & 
				sqlca.sqlerrtext
				
				close r_distinte_taglio;
				return -1
			end if
		end if					
	loop

	close r_distinte_taglio;	
//************************
// fine del cursore che scansione le distinte di taglio associate al ramo corrente di distinta
//************************

	// se è impostato il flag_materia_prima allora vado avanti e non scendo in profondità nella distinta
	if ls_flag_materia_prima = "S" then continue
	
	// controllo se la distinta ha figli 
	select 	count(*)
	into   		:ll_cont
	from   	distinta
	where  	cod_azienda = :s_cs_xx.cod_azienda  and
				cod_prodotto_padre=:ls_cod_prodotto_inserito and
				cod_versione=:ls_cod_versione_figlio;	
	if sqlca.sqlcode < 0 then
		fs_errore= g_str.format( "Distinta di taglio: errore in controllo presenza figli della distinta padre $1 ",ls_cod_prodotto_inserito)
		return -1
	end if


	// Se non ci sono figli vado avanti
	if isnull(ll_cont) or ll_cont < 1 then
		continue
	else
		li_risposta=f_calcola_dt(fi_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ls_cod_prodotto_inserito, ls_cod_versione_figlio,fs_tipo_ordinamento_dt, ls_errore)
		if li_risposta = -1 then
			fs_errore=ls_errore
			return -1
		end if
	end if
	
next

destroy lds_righe_distinta

return 0

end function

public function integer wf_get_variabile_conf (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_cod_variabile, ref string as_valore_stringa, ref decimal ad_valore_numero, ref string as_errore);/*
Funzione SOLO PER TENDEMODEL di ricerca di una variabile nelle nuove tabelle del configuratore
return:	-1		errore
			0		trovata variabile
			1		varisbile non trovata


*/
string ls_flag_tipo_dato

select tab_variabili_formule.flag_tipo_dato, 
		det_ord_ven_conf_variabili.valore_stringa, 
		det_ord_ven_conf_variabili.valore_numero
into	:ls_flag_tipo_dato, 
		:as_valore_stringa, 
		:ad_valore_numero
from	det_ord_ven_conf_variabili
join	tab_variabili_formule on tab_variabili_formule.cod_azienda=det_ord_ven_conf_variabili.cod_azienda and tab_variabili_formule.cod_variabile=det_ord_ven_conf_variabili.cod_variabile
where	 det_ord_ven_conf_variabili.cod_azienda = :s_cs_xx.cod_azienda and
		det_ord_ven_conf_variabili.anno_registrazione = :al_anno_registrazione and
		det_ord_ven_conf_variabili.num_registrazione = :al_num_registrazione and
		det_ord_ven_conf_variabili.prog_riga_ord_ven = :al_prog_riga_ord_ven and
		det_ord_ven_conf_variabili.cod_variabile = :as_cod_variabile and
		det_ord_ven_conf_variabili.flag_visible_in_stampa='S';
if sqlca.sqlcode = 100 then
	as_valore_stringa = ""
	ad_valore_numero = 0
	return 1
elseif sqlca.sqlcode < 0 then
	as_valore_stringa = ""
	ad_valore_numero = 0
	as_errore = g_str.format("Errore in ricerca variabile $1. Dettaglio errore~r~n $2",as_cod_variabile,sqlca.sqlerrtext)
	return -1
else
	choose case ls_flag_tipo_dato
		case "N"
			as_valore_stringa = ""
		case "S"
			ad_valore_numero = 0
		case else
			as_errore = g_str.format("Errore in ricerca variabile $1. Valore del tipo non previsto", as_cod_variabile)
	end choose	
	return 0
end if

return 0

end function

public function integer wf_crea_commesse (long fl_anno_reg_ord_ven, long fl_num_reg_ord_ven, long fl_prog_riga_ord_ven, string fs_cod_tipo_ord_ven, ref string fs_errore);string		ls_sql, ls_errore
integer 	ll_anno_commessa
long		ll_ret, ll_i, ll_num_commessa, ll_prog_riga_ord_ven, ll_num_commessa_forzato, ll_err
datastore lds_data
uo_funzioni_1 luo_f1

ls_sql = g_str.format("select anno_commessa, num_commessa, prog_riga_ord_ven from det_ord_ven where cod_azienda ='$1' and anno_registrazione = $2 and num_registrazione = $3", s_cs_xx.cod_azienda, fl_anno_reg_ord_ven, fl_num_reg_ord_ven)

if not isnull(fl_prog_riga_ord_ven) and fl_prog_riga_ord_ven > 0 then
	ls_sql += g_str.format(ls_sql + " prog_riga_ord_ven=$3 ",fl_prog_riga_ord_ven)
end if

ls_sql += " and flag_evasione <> 'E' and flag_gen_commessa = 'S' "

ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql, ref fs_errore) 
if ll_ret < 0 then
	return -1
end if

luo_f1 = create uo_funzioni_1

for ll_i = 1 to ll_ret
	ll_anno_commessa = lds_data.getitemnumber(ll_i,1)
	ll_prog_riga_ord_ven = lds_data.getitemnumber(ll_i,3)
	setnull(ll_num_commessa_forzato)
	if isnull(ll_anno_commessa) then
		luo_f1.ib_flag_non_collegati=true
		ll_err = luo_f1.uof_genera_commesse( fs_cod_tipo_ord_ven, fl_anno_reg_ord_ven, fl_num_reg_ord_ven, ll_prog_riga_ord_ven, ll_num_commessa_forzato, ref ll_anno_commessa, ref ll_num_commessa, ref ls_errore)
		if ll_err < 0 then
			fs_errore = ls_errore
			return -1
		end if
	end if
next

destroy luo_f1
destroy lds_data

return 0
end function

on w_stampa_ordini_prod.create
int iCurrent
call super::create
this.gb_1=create gb_1
this.st_debugmode=create st_debugmode
this.st_debug=create st_debug
this.hpb_1=create hpb_1
this.st_ordini_altro_stab=create st_ordini_altro_stab
this.st_stab_operatore=create st_stab_operatore
this.em_quan_etichetta_gen=create em_quan_etichetta_gen
this.cb_etichetta_gen=create cb_etichetta_gen
this.cbx_tende_tecniche=create cbx_tende_tecniche
this.cbx_etichette_sole=create cbx_etichette_sole
this.cb_etichette_cg=create cb_etichette_cg
this.st_num_copie_sfuso=create st_num_copie_sfuso
this.em_num_copie_sfuso=create em_num_copie_sfuso
this.st_num_copie_tecniche=create st_num_copie_tecniche
this.em_num_copie_tecniche=create em_num_copie_tecniche
this.st_num_copie_sole=create st_num_copie_sole
this.em_num_copie_sole=create em_num_copie_sole
this.cb_ordini_senza_commessa=create cb_ordini_senza_commessa
this.cb_ordini_senza_commessa_esc=create cb_ordini_senza_commessa_esc
this.st_3=create st_3
this.st_4=create st_4
this.st_5=create st_5
this.st_6=create st_6
this.cbx_select_all=create cbx_select_all
this.cbx_ordini_senza_commessa=create cbx_ordini_senza_commessa
this.dw_ordini_senza_commessa=create dw_ordini_senza_commessa
this.dw_report=create dw_report
this.dw_lista_ord_ven_altro_deposito=create dw_lista_ord_ven_altro_deposito
this.dw_folder=create dw_folder
this.st_1=create st_1
this.dw_lista_ord_ven_stampa_prod=create dw_lista_ord_ven_stampa_prod
this.dw_ottimizza_4=create dw_ottimizza_4
this.dw_ottimizza_3=create dw_ottimizza_3
this.cb_stampa=create cb_stampa
this.dw_elenco_operatori_selezionati=create dw_elenco_operatori_selezionati
this.dw_ottimizza_1=create dw_ottimizza_1
this.cb_etichette=create cb_etichette
this.dw_ext_selezione=create dw_ext_selezione
this.cb_lancia=create cb_lancia
this.cbx_gen_commesse=create cbx_gen_commesse
this.tv_lancio=create tv_lancio
this.dw_gg_lancio=create dw_gg_lancio
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.gb_1
this.Control[iCurrent+2]=this.st_debugmode
this.Control[iCurrent+3]=this.st_debug
this.Control[iCurrent+4]=this.hpb_1
this.Control[iCurrent+5]=this.st_ordini_altro_stab
this.Control[iCurrent+6]=this.st_stab_operatore
this.Control[iCurrent+7]=this.em_quan_etichetta_gen
this.Control[iCurrent+8]=this.cb_etichetta_gen
this.Control[iCurrent+9]=this.cbx_tende_tecniche
this.Control[iCurrent+10]=this.cbx_etichette_sole
this.Control[iCurrent+11]=this.cb_etichette_cg
this.Control[iCurrent+12]=this.st_num_copie_sfuso
this.Control[iCurrent+13]=this.em_num_copie_sfuso
this.Control[iCurrent+14]=this.st_num_copie_tecniche
this.Control[iCurrent+15]=this.em_num_copie_tecniche
this.Control[iCurrent+16]=this.st_num_copie_sole
this.Control[iCurrent+17]=this.em_num_copie_sole
this.Control[iCurrent+18]=this.cb_ordini_senza_commessa
this.Control[iCurrent+19]=this.cb_ordini_senza_commessa_esc
this.Control[iCurrent+20]=this.st_3
this.Control[iCurrent+21]=this.st_4
this.Control[iCurrent+22]=this.st_5
this.Control[iCurrent+23]=this.st_6
this.Control[iCurrent+24]=this.cbx_select_all
this.Control[iCurrent+25]=this.cbx_ordini_senza_commessa
this.Control[iCurrent+26]=this.dw_ordini_senza_commessa
this.Control[iCurrent+27]=this.dw_report
this.Control[iCurrent+28]=this.dw_lista_ord_ven_altro_deposito
this.Control[iCurrent+29]=this.dw_folder
this.Control[iCurrent+30]=this.st_1
this.Control[iCurrent+31]=this.dw_lista_ord_ven_stampa_prod
this.Control[iCurrent+32]=this.dw_ottimizza_4
this.Control[iCurrent+33]=this.dw_ottimizza_3
this.Control[iCurrent+34]=this.cb_stampa
this.Control[iCurrent+35]=this.dw_elenco_operatori_selezionati
this.Control[iCurrent+36]=this.dw_ottimizza_1
this.Control[iCurrent+37]=this.cb_etichette
this.Control[iCurrent+38]=this.dw_ext_selezione
this.Control[iCurrent+39]=this.cb_lancia
this.Control[iCurrent+40]=this.cbx_gen_commesse
this.Control[iCurrent+41]=this.tv_lancio
this.Control[iCurrent+42]=this.dw_gg_lancio
end on

on w_stampa_ordini_prod.destroy
call super::destroy
destroy(this.gb_1)
destroy(this.st_debugmode)
destroy(this.st_debug)
destroy(this.hpb_1)
destroy(this.st_ordini_altro_stab)
destroy(this.st_stab_operatore)
destroy(this.em_quan_etichetta_gen)
destroy(this.cb_etichetta_gen)
destroy(this.cbx_tende_tecniche)
destroy(this.cbx_etichette_sole)
destroy(this.cb_etichette_cg)
destroy(this.st_num_copie_sfuso)
destroy(this.em_num_copie_sfuso)
destroy(this.st_num_copie_tecniche)
destroy(this.em_num_copie_tecniche)
destroy(this.st_num_copie_sole)
destroy(this.em_num_copie_sole)
destroy(this.cb_ordini_senza_commessa)
destroy(this.cb_ordini_senza_commessa_esc)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.st_5)
destroy(this.st_6)
destroy(this.cbx_select_all)
destroy(this.cbx_ordini_senza_commessa)
destroy(this.dw_ordini_senza_commessa)
destroy(this.dw_report)
destroy(this.dw_lista_ord_ven_altro_deposito)
destroy(this.dw_folder)
destroy(this.st_1)
destroy(this.dw_lista_ord_ven_stampa_prod)
destroy(this.dw_ottimizza_4)
destroy(this.dw_ottimizza_3)
destroy(this.cb_stampa)
destroy(this.dw_elenco_operatori_selezionati)
destroy(this.dw_ottimizza_1)
destroy(this.cb_etichette)
destroy(this.dw_ext_selezione)
destroy(this.cb_lancia)
destroy(this.cbx_gen_commesse)
destroy(this.tv_lancio)
destroy(this.dw_gg_lancio)
end on

event pc_setwindow;call super::pc_setwindow;boolean 		lb_gsc

string 		ls_keyword,ls_values[], ls_volume, ls_appo

integer 		li_return

long 			ll_num_copie_sole,ll_num_copie_tecniche,ll_num_copie_sfuso

windowobject lw_oggetti[], lw_vuoto[]
ContextKeyword lcx_key 
environment env




iuo_fido = create uo_fido_cliente


if s_cs_xx.cod_utente = "CS_SYSTEM" then
	g_mb.show( "Si sconsiglia per ora il lancio di produzione e la stampa degli ordini se sei CS_SYSTEM! "+&
						"Entra con un utente dello stabilimento interessato piuttosto!" )
end if

guo_functions.uof_get_parametro_azienda("AIP", ref ib_aip)


dw_lista_ord_ven_stampa_prod.postevent("pcd_modify")


//controllo se devo generare un log completo durante la stampa
Registryget(s_cs_xx.chiave_root + "applicazione_" +  s_cs_xx.profilocorrente, "LSP", ls_appo)
if ls_appo="1" then
	ib_log = true
else
	ib_log = false
end if

//preparo il percorso del file di log ------------------
is_nome_file_log = ""
is_nome_file_log = "stampaordini_"+string(today(), "yymmdd") +"_"+ string(now(), "hhmmssfff") + "_" + s_cs_xx.cod_utente+".log"

ls_volume = guo_functions.uof_get_user_temp_folder()
if right(ls_volume, 1) <> "\" then ls_volume += "\"
is_nome_file_log = ls_volume + is_nome_file_log

//---------------------------------------------------------------


//lettura parametro aziendale Carattere Speciale barcode (specifica Carico ordini con palmare)
select stringa
into   :is_CSB
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='CSB' and 
		flag_parametro='S';

if sqlca.sqlcode < 0 then
	messagebox("SEP","Errore in ricerca del parametro CSB in tabella parametri azienda: " + sqlca.sqlerrtext,stopsign!)
	is_CSB = ""
	
elseif sqlca.sqlcode=100 then
	is_CSB = ""
end if

if isnull(is_CSB) then is_CSB =""
//fine modifica ------------------------------------------------

//path del file per la filigrana del residuo
is_path_filigrana = s_cs_xx.volume + s_cs_xx.risorse + "filigrana_residuo.jpg"

dw_report.ib_dw_report = true

set_w_options(c_noenablepopup)

//#######################################################################
lw_oggetti[1] = dw_lista_ord_ven_stampa_prod
lw_oggetti[2] = cb_lancia
lw_oggetti[3] = cb_stampa
lw_oggetti[4] = cb_etichette
lw_oggetti[5] = tv_lancio
lw_oggetti[6] = st_1
lw_oggetti[7] = cbx_gen_commesse
lw_oggetti[8] = dw_gg_lancio

cb_etichette_cg.visible = false
cbx_etichette_sole.visible = false
cbx_tende_tecniche.visible = false
cb_etichetta_gen.visible = false
em_quan_etichetta_gen.visible = false

dw_folder.fu_assigntab(2, "Ordini", lw_oggetti[])

//#######################################################################
lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_lista_ord_ven_altro_deposito
dw_folder.fu_assigntab(3, "Ordini altri Stabilimenti", lw_oggetti[])

//#######################################################################
lw_oggetti = lw_vuoto
lw_oggetti[1] = cbx_ordini_senza_commessa
lw_oggetti[2] = dw_ordini_senza_commessa
lw_oggetti[3] = cb_ordini_senza_commessa
lw_oggetti[4] = cb_ordini_senza_commessa_esc
dw_folder.fu_assigntab(4, "Ordini senza Commessa", lw_oggetti[])

//#######################################################################
lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_ext_selezione
lw_oggetti[2] = dw_elenco_operatori_selezionati
lw_oggetti[3] = gb_1
lw_oggetti[4] = st_3
lw_oggetti[5] = st_4
lw_oggetti[6] = st_5
lw_oggetti[7] = st_6
dw_folder.fu_assigntab(1, "Selezione", lw_oggetti[])
//#######################################################################
//#######################################################################
lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_ottimizza_1
lw_oggetti[2] = dw_ottimizza_4
lw_oggetti[3] = dw_ottimizza_3
dw_folder.fu_assigntab(5, "Ottimizzazione Tagli", lw_oggetti[])

dw_folder.fu_foldercreate(5, 5)
dw_folder.fu_selecttab(1)

dw_folder.fu_disabletab(4)


dw_lista_ord_ven_stampa_prod.set_dw_options(	sqlca, &
                                   									pcca.null_object, &
											  					c_noretrieveonopen + &
                                  									c_multiselect + &
																c_nonew + &
																c_nomodify + &
																c_nodelete + &
																c_disablecc + &
																c_disableccinsert, &
																c_default)

dw_ext_selezione.set_dw_options(	sqlca, &
												pcca.null_object,&
												c_newonopen, &
												c_default)
																	
dw_lista_ord_ven_altro_deposito.set_dw_options(		sqlca, &
																	pcca.null_object, &
																	c_noretrieveonopen + &
																	c_multiselect + &
																	c_nonew + &
																	c_nodelete + &
																	c_disablecc + &
																	c_disableccinsert, &
																	c_default)
																	
																	
save_on_close(c_socnosave)


select numero
into   :ll_num_copie_sole
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='NCS';

if sqlca.sqlcode < 0 then
	messagebox("SEP","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

if sqlca.sqlcode = 100 then
	em_num_copie_sole.visible = false
	st_num_copie_sole.visible = false
else
	em_num_copie_sole.text = string(ll_num_copie_sole)
end if

select numero
into   :ll_num_copie_tecniche
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='NCT';

if sqlca.sqlcode < 0 then
	messagebox("SEP","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

if sqlca.sqlcode = 100 then
	em_num_copie_tecniche.visible = false
	st_num_copie_tecniche.visible = false
else
	em_num_copie_tecniche.text = string(ll_num_copie_tecniche)
end if
	
	
select numero
into   :ll_num_copie_sfuso
from   parametri_azienda
where  cod_azienda=:s_cs_xx.cod_azienda
and    cod_parametro='NCF';

if sqlca.sqlcode < 0 then
	messagebox("SEP","Errore sul DB: " + sqlca.sqlerrtext,stopsign!)
	return
end if

if sqlca.sqlcode = 100 then
	em_num_copie_sfuso.visible = false
	st_num_copie_sfuso.visible = false
else
	em_num_copie_sfuso.text = string(ll_num_copie_sfuso)
end if

guo_functions.uof_get_parametro_azienda( "GSC", ref lb_gsc)
if lb_gsc then
	cbx_gen_commesse.checked = true
end if

//Donato 03/01/2012 valorizza una variabile di istanza con il codice dello stabilimento legato all'operatore
wf_get_stabilimento_operatore()

wf_retrieve_altri_stabilimenti()

wf_carica_operatori()

dw_gg_lancio.insertrow(0)
wf_carica_lista_lancio_prod()

//st_1.dragicon = s_cs_xx.volume + s_cs_xx.risorse + "11.5\arrow_left_ico.ico"
//st_1.dragicon = s_cs_xx.volume + s_cs_xx.risorse + "12.5\hand_point.ico"
st_1.dragicon = s_cs_xx.volume + s_cs_xx.risorse + "12.5\tv_aggiungi.ico"

event post ue_post_open()
end event

event pc_close;call super::pc_close;

destroy iuo_fido
end event

type gb_1 from groupbox within w_stampa_ordini_prod
integer x = 1970
integer y = 1052
integer width = 1783
integer height = 880
integer taborder = 70
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "NOTE OPERATIVE"
end type

type st_debugmode from statictext within w_stampa_ordini_prod
boolean visible = false
integer x = 3936
integer y = 8
integer width = 466
integer height = 116
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "DEBUGmode NO-MAIL"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_debug from statictext within w_stampa_ordini_prod
integer x = 4475
integer y = 4
integer width = 82
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean focusrectangle = false
end type

event doubleclicked;if MessageBox ("Attenzione-DEBUGmode", "Premere SI per DISABILITARE l'invio mail depositi esterni, NO per ABILITARE l'invio mail!", Exclamation!, YesNo!, 2) = 1 then
	//SI:   --------> quindi DEBUG MODE
	//no invio mail ad altri stabilimenti, se interessati
	ib_debug_mode = true
	parent.title = "DEBUGmode: NO MAIL e NO spooler ordini altri stabilimenti"
else
	//NO:  --------> quindi modalità normale
	//ABILITA invio mail ad altri stabilimenti, se interessati
	ib_debug_mode = false
	parent.title = "Lancio - Stampa Ordini di Produzione GIBUS"
end if

st_debugmode.visible = ib_debug_mode
end event

type hpb_1 from hprogressbar within w_stampa_ordini_prod
integer x = 27
integer y = 2432
integer width = 4489
integer height = 68
unsignedinteger maxposition = 100
integer setstep = 1
end type

type st_ordini_altro_stab from statictext within w_stampa_ordini_prod
boolean visible = false
integer x = 2514
integer y = 44
integer width = 1381
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Attenzione! sono presenti ordini di altri Stabilimenti"
boolean focusrectangle = false
end type

type st_stab_operatore from statictext within w_stampa_ordini_prod
integer x = 37
integer y = 32
integer width = 2427
integer height = 72
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
string text = "Stabilimento Operatore"
boolean focusrectangle = false
end type

type em_quan_etichetta_gen from editmask within w_stampa_ordini_prod
integer x = 3392
integer y = 2224
integer width = 361
integer height = 80
integer taborder = 70
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "##0"
boolean spin = true
end type

type cb_etichetta_gen from commandbutton within w_stampa_ordini_prod
integer x = 3008
integer y = 2228
integer width = 366
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Etichetta Gen."
end type

event clicked;if f_etichetta_gen() < 0 then
	rollback;
else
	commit;
end if
	
end event

type cbx_tende_tecniche from checkbox within w_stampa_ordini_prod
integer x = 2185
integer y = 2288
integer width = 411
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "E.T.Tecniche"
boolean checked = true
boolean lefttext = true
end type

type cbx_etichette_sole from checkbox within w_stampa_ordini_prod
integer x = 2185
integer y = 2228
integer width = 411
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "E.T.Sole"
boolean checked = true
boolean lefttext = true
boolean righttoleft = true
end type

type cb_etichette_cg from commandbutton within w_stampa_ordini_prod
integer x = 2619
integer y = 2228
integer width = 366
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Etichette C.G."
end type

event clicked;if f_etichetta_cg() < 0 then
	rollback;
else
	commit;
end if
	
end event

type st_num_copie_sfuso from statictext within w_stampa_ordini_prod
integer x = 1499
integer y = 2228
integer width = 393
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Nr.copie Sfuso"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_num_copie_sfuso from editmask within w_stampa_ordini_prod
integer x = 1911
integer y = 2228
integer width = 206
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "######"
boolean spin = true
end type

type st_num_copie_tecniche from statictext within w_stampa_ordini_prod
integer x = 699
integer y = 2228
integer width = 526
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Nr.copie T.Tecniche"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_num_copie_tecniche from editmask within w_stampa_ordini_prod
integer x = 1248
integer y = 2228
integer width = 206
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "######"
boolean spin = true
end type

type st_num_copie_sole from statictext within w_stampa_ordini_prod
integer x = 37
integer y = 2228
integer width = 389
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Nr.copie T.Sole"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_num_copie_sole from editmask within w_stampa_ordini_prod
integer x = 448
integer y = 2228
integer width = 206
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "1"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "######"
boolean spin = true
end type

type cb_ordini_senza_commessa from commandbutton within w_stampa_ordini_prod
integer x = 1723
integer y = 312
integer width = 727
integer height = 88
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Prosegui con la stampa"
end type

event clicked;long ll_index, ll_tot, ll_anno_reg, ll_num_reg
string ls_messaggio, ls_tipo_log

//salva nel log l'azione
ll_tot = dw_ordini_senza_commessa.rowcount()

ls_tipo_log = "stampa senza commessa"

for ll_index=1 to ll_tot
	ll_anno_reg = dw_ordini_senza_commessa.getitemnumber(ll_index, "anno_registrazione")
	ll_num_reg = dw_ordini_senza_commessa.getitemnumber(ll_index, "num_registrazione")
	
	ls_messaggio = "Ordine "+string(ll_anno_reg)+"/"+string(ll_num_reg)+" stampato con alcune righe senza commessa!"
	f_log_sistema(ls_messaggio, ls_tipo_log)
next

commit;

dw_ordini_senza_commessa.reset()
cbx_ordini_senza_commessa.checked = false
cb_ordini_senza_commessa.enabled = false

dw_folder.fu_disabletab( 4)
dw_folder.fu_enabletab(1)
dw_folder.fu_enabletab(2)
dw_folder.fu_enabletab(3)

//STAMPA SENZA PIù CONTROLLARE L'ESISTENZA DELLE COMMESSE
dw_folder.fu_Selecttab(2)

if len(is_origine_comando) > 0 then
	wf_clicked_stampa(false,is_origine_comando)
else
	wf_clicked_stampa(false,"D")
end if	
end event

type cb_ordini_senza_commessa_esc from commandbutton within w_stampa_ordini_prod
integer x = 2478
integer y = 312
integer width = 727
integer height = 88
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;
dw_ordini_senza_commessa.reset()
cbx_ordini_senza_commessa.checked = false
cb_ordini_senza_commessa.enabled = false

dw_folder.fu_enabletab(1)
dw_folder.fu_enabletab(2)
dw_folder.fu_enabletab(3)

dw_folder.fu_disabletab(4)

dw_folder.fu_Selecttab(2)

end event

type st_3 from statictext within w_stampa_ordini_prod
integer x = 2016
integer y = 1164
integer width = 1691
integer height = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean enabled = false
string text = "- Le righe di dettaglio ordine che vengono elaborate sono quelle che hanno 0 (zero) come riga di appartenenza."
boolean focusrectangle = false
end type

type st_4 from statictext within w_stampa_ordini_prod
integer x = 2016
integer y = 1564
integer width = 1673
integer height = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean enabled = false
string text = "- Se si attiva la selezione per operatore gli ordini senza operatore saranno saltati."
boolean focusrectangle = false
end type

type st_5 from statictext within w_stampa_ordini_prod
integer x = 2016
integer y = 1364
integer width = 1691
integer height = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean enabled = false
string text = "- Le righe di dettaglio ordine che non verranno stampate nei report sono quelle con tipo dettaglio ~'ADD~'."
boolean focusrectangle = false
end type

type st_6 from statictext within w_stampa_ordini_prod
integer x = 2016
integer y = 1764
integer width = 1710
integer height = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean enabled = false
string text = "- Se si attiva la selezione degli ALLEGATI gli ordini senza righe configurate saranno saltati."
boolean focusrectangle = false
end type

type cbx_select_all from checkbox within w_stampa_ordini_prod
boolean visible = false
integer x = 1362
integer y = 1020
integer width = 471
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Seleziona tutto"
boolean lefttext = true
end type

event clicked;/**
 * Stefano
 * 08/04/2010
 *
 * Progetto 140
 **/
 
string ls_flag = "N"
long ll_i

if cbx_select_all.checked then ls_flag = "S"

for ll_i = 1 to dw_elenco_operatori_selezionati.rowcount()
	dw_elenco_operatori_selezionati.setitem(ll_i, "flag_selezionato", ls_flag)
next
end event

type cbx_ordini_senza_commessa from checkbox within w_stampa_ordini_prod
integer x = 78
integer y = 308
integer width = 1563
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "OK, voglio stampare lo stesso gli ordini senza commessa"
end type

event clicked;

if cbx_ordini_senza_commessa.checked then
	cb_ordini_senza_commessa.enabled = true
else
	cb_ordini_senza_commessa.enabled = false
end if
end event

type dw_ordini_senza_commessa from uo_cs_xx_dw within w_stampa_ordini_prod
integer x = 41
integer y = 432
integer width = 4448
integer height = 1696
integer taborder = 70
string dataobject = "d_lista_ord_ven_stampa_senza_commessa"
end type

type dw_report from uo_cs_xx_dw within w_stampa_ordini_prod
boolean visible = false
integer x = 3493
integer y = 1620
integer width = 640
integer height = 80
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_report_1"
boolean livescroll = true
end type

type dw_lista_ord_ven_altro_deposito from uo_cs_xx_dw within w_stampa_ordini_prod
integer x = 41
integer y = 260
integer width = 4439
integer height = 1876
integer taborder = 20
string dataobject = "d_lista_ord_ven_stampa_prod_altri"
boolean vscrollbar = true
end type

event clicked;call super::clicked;long ll_index, ll_tot
string ls_sel

choose case dwo.name
	case "b_refresh"
		wf_retrieve_altri_stabilimenti()
		
	case "b_stampa"
		wf_stampa_altri_ordini()
		
	case "b_nessuno", "b_tutti"
		accepttext()
		ll_tot = rowcount()
		
		if ll_tot>0 then
		else
			return
		end if
		
		if dwo.name="b_nessuno" then
			ls_sel="N"
		else
			ls_sel="S"
		end if
		
		for ll_index=1 to ll_tot
			setitem(ll_index, "selezionato", ls_sel)
		next
		
end choose
end event

type dw_folder from u_folder within w_stampa_ordini_prod
integer x = 27
integer y = 140
integer width = 4489
integer height = 2272
integer taborder = 10
boolean border = false
end type

event po_tabclicked;call super::po_tabclicked;string			ls_flag_operatori
long			ll_row



if upper(i_SelectedTabName) = "SELEZIONE" then

	ll_row = dw_ext_selezione.getrow()

	if ll_row>0 then
	else
		return
	end if
	
	ls_flag_operatori = dw_ext_selezione.getitemstring(ll_row, "flag_operatori")
	
	if ls_flag_operatori = "N" then		
		dw_elenco_operatori_selezionati.visible = false
	else
		dw_elenco_operatori_selezionati.visible = true
	end if
	
	cbx_select_all.visible = dw_elenco_operatori_selezionati.visible

end if
end event

type st_1 from statictext within w_stampa_ordini_prod
integer x = 46
integer y = 2136
integer width = 3150
integer height = 84
string dragicon = "Form!"
boolean dragauto = true
boolean bringtotop = true
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 67108864
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

type dw_lista_ord_ven_stampa_prod from uo_cs_xx_dw within w_stampa_ordini_prod
event ue_cont_selected ( )
integer x = 46
integer y = 264
integer width = 3150
integer height = 1868
integer taborder = 50
boolean bringtotop = true
string dataobject = "d_lista_ord_ven_stampa_prod"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event ue_cont_selected();if i_extendmode then
	long ll_selected[]
	get_selected_rows(ll_selected[])
	
	if upperbound(ll_selected[]) = 0 then
		st_1.text = ""
	else
		st_1.text = g_str.format("Trascinare da qui per creare sessioni di produzione con le $1 righe selezionate", upperbound(ll_selected[]))
	end if
	
end if
end event

event pcd_retrieve;call super::pcd_retrieve;boolean		lb_ok, lb_salta_ordine, lb_allegati

integer		li_anno_commessa, li_ret_check_reparti, li_anno_ordine_filtro

string			ls_cod_cliente, ls_cod_tipo_ord_ven, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_cod_cliente_sel, ls_errore, &
				ls_flag_gen_commessa, ls_flag_stampata_bcl, ls_flag_blocco,ls_rag_soc_1, ls_flag_con_commessa, ls_flag_allegati, &
				ls_elenco_operatori, ls_flag_operatori, ls_cod_operatore, ls_stato_allegato, ls_cod_deposito_origine, ls_des_deposito, &
				ls_des_tipo_ord_ven, ls_operatore_ds, ls_operatori_in, ls_flag_stampa_ordini_prod
				
long			ll_anno_registrazione, ll_num_registrazione, ll_cont, ll_i,ll_prog_riga_ord_ven, ll_tot, ll_counter, ll_num_ordine_filtro, ll_index, ll_cont_lancio

datetime		ldt_data_registrazione,ldt_da_data,ldt_a_data,ldt_data_consegna, ldt_da_data_reg, ldt_a_data_reg

//implemento qui una cache di prodotti/versioni
string			ls_cod_prodotto_cache[], ls_cod_versione_cache[], ls_vuoto[], ls_sql_cursore

datastore	lds_data

setpointer(Hourglass!)

dw_lista_ord_ven_stampa_prod.resetupdate()
dw_ext_selezione.accepttext()

li_anno_ordine_filtro = dw_ext_selezione.getitemnumber(1,"anno_ordine")
ll_num_ordine_filtro = dw_ext_selezione.getitemnumber(1,"num_ordine")

ldt_da_data = dw_ext_selezione.getitemdatetime(1,"data_consegna_inizio")
ldt_a_data = dw_ext_selezione.getitemdatetime(1,"data_consegna_fine")
ldt_da_data_reg = dw_ext_selezione.getitemdatetime(1,"data_registrazione_inizio")
ldt_a_data_reg = dw_ext_selezione.getitemdatetime(1,"data_registrazione_fine")
ls_flag_stampata_bcl = dw_ext_selezione.getitemstring(1,"flag_stampati")
ls_flag_blocco = dw_ext_selezione.getitemstring(1,"flag_bloccati")
ls_flag_con_commessa = dw_ext_selezione.getitemstring(1,"flag_con_commessa")
ls_flag_allegati = dw_ext_selezione.getitemstring(1,"flag_allegati")
ls_flag_operatori = dw_ext_selezione.getitemstring(1,"flag_operatori")
ls_cod_cliente_sel = dw_ext_selezione.getitemstring(1,"cod_cliente")
ls_flag_stampa_ordini_prod = dw_ext_selezione.getitemstring(1,"flag_stampa_ordini_prod")

if isnull(ls_cod_cliente_sel) or len(ls_cod_cliente_sel) < 1 then ls_cod_cliente_sel = "%"

dw_lista_ord_ven_stampa_prod.reset()

//vedo prima quanti sono
ll_counter = 0

//di base mettiamo azienda, e deposito dell'utente
ls_sql_cursore = "select 	tes_ord_ven.anno_registrazione,"+&
								"tes_ord_ven.num_registrazione,"+&
								"tes_ord_ven.data_consegna,"+&
								"tes_ord_ven.data_registrazione,"+&
								"tes_ord_ven.cod_cliente,"+&
						 		"tes_ord_ven.cod_tipo_ord_ven,"+&
								 "tes_ord_ven.cod_operatore,"+&
								 "tes_ord_ven.cod_deposito,"+&
								 "tab_operatori.des_operatore "+&
						"from   tes_ord_ven "+&
						"left outer join tab_operatori on tab_operatori.cod_azienda=tes_ord_ven.cod_azienda and "+&
																"tab_operatori.cod_operatore=tes_ord_ven.cod_operatore "+&
						"where	tes_ord_ven.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
									"(tes_ord_ven.cod_deposito='"+is_cod_deposito_operatore+"' or '"+is_cod_deposito_operatore+"'='"+is_cod_deposito_CSSYSTEM+"') and " + &
									"tes_ord_ven.cod_cliente like '"+ls_cod_cliente_sel+"' and "+&
									"tes_ord_ven.data_consegna>='" + string(ldt_da_data, s_cs_xx.db_funzioni.formato_data) + "' and "+&
									"tes_ord_ven.data_consegna<='" + string(ldt_a_data, s_cs_xx.db_funzioni.formato_data) + "' and "+&
									"tes_ord_ven.data_registrazione>='" + string(ldt_da_data_reg, s_cs_xx.db_funzioni.formato_data) + "' and "+&
									"tes_ord_ven.data_registrazione<='" + string(ldt_a_data_reg, s_cs_xx.db_funzioni.formato_data) + "' and "+&
									"tes_ord_ven.flag_stampata_bcl='"+ls_flag_stampata_bcl+"' and tes_ord_ven.flag_blocco='"+ls_flag_blocco+"' and tes_ord_ven.flag_evasione<>'E' "

if li_anno_ordine_filtro>0 then
	ls_sql_cursore += " and tes_ord_ven.anno_registrazione="+string(li_anno_ordine_filtro)
end if
if ll_num_ordine_filtro>0 then
	ls_sql_cursore += " and tes_ord_ven.num_registrazione="+string(ll_num_ordine_filtro)
end if

if ls_flag_operatori="S" and not cbx_select_all.checked then
	//hai chiesto ordini di particolari operatori: recupera la stringa per "cod_operatore in (.....) "
	if wf_get_operatori(ls_operatori_in) > 0 then
		ls_sql_cursore += " and tes_ord_ven.cod_operatore in ("+ls_operatori_in+")"
	end if
end if
						
//order by del cursore
ls_sql_cursore += " order by tes_ord_ven.anno_registrazione asc, tes_ord_ven.num_registrazione asc"


ll_counter = guo_functions.uof_crea_datastore(lds_data, ls_sql_cursore, ls_errore)
if ll_counter<0 then
	setpointer(Arrow!)
	g_mb.error(ls_errore)
	return
	
elseif ll_counter=0 then
	setpointer(Arrow!)
	destroy lds_data;
	//g_mb.warning(	"Nessun ordine (dello stabilimento/deposito dell'utente collegato) corrisponde al filtro di ricerca impostato!")
	return
end if

for ll_index=1 to ll_counter
	ll_anno_registrazione			= lds_data.getitemnumber(ll_index, 1)
	ll_num_registrazione			= lds_data.getitemnumber(ll_index, 2)
	ldt_data_consegna				= lds_data.getitemdatetime(ll_index, 3)
	ldt_data_registrazione		= lds_data.getitemdatetime(ll_index, 4)
	ls_cod_cliente					= lds_data.getitemstring(ll_index, 5)
	ls_cod_tipo_ord_ven			= lds_data.getitemstring(ll_index, 6)
	ls_cod_operatore				= lds_data.getitemstring(ll_index, 7)
	ls_cod_deposito_origine		= lds_data.getitemstring(ll_index, 8)
	ls_operatore_ds				= lds_data.getitemstring(ll_index, 9)
	
	pcca.mdi_frame.setmicrohelp("Elaboraz. Ordine " + string(ll_anno_registrazione)+"/"+string(ll_num_registrazione) + " - " + string(ll_index) + " di " + string(ll_counter))
	Yield()

 
	if ls_flag_stampa_ordini_prod = "N" then
		// (EnMe 21-4-16): se ho eseguito qualche lancio produzione non includo l'ordine e salto al successivo
		select 	count(*)
		into		:ll_cont_lancio
		from		det_lancio_prod
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_reg_ord_ven = :ll_anno_registrazione and
					num_reg_ord_ven = :ll_num_registrazione ;
		if ll_cont_lancio > 0 then continue
	end if
 
	declare cu_det_ord_ven cursor for 
	select prog_riga_ord_ven,
	       cod_tipo_det_ven,
			 flag_gen_commessa,
			 flag_blocco,
			 anno_commessa
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda 
	and 	 anno_registrazione = :ll_anno_registrazione
	and 	 num_registrazione = :ll_num_registrazione;
	
	open cu_det_ord_ven;
	
	lb_ok = false
	if ls_flag_allegati = "S"	then lb_allegati = false
	if ls_flag_allegati = "N"	then lb_allegati = true
	
	do while 0 = 0
		fetch cu_det_ord_ven 
		into :ll_prog_riga_ord_ven,
		     :ls_cod_tipo_det_ven,
			  :ls_flag_gen_commessa,
			  :ls_flag_blocco,
			  :li_anno_commessa;
			  
		if sqlca.sqlcode <> 0 then exit
		
		if ls_flag_con_commessa = "S" then
			if not(isnull(li_anno_commessa)) then lb_ok=true
		else
			lb_ok=true				
		end if
		
		// controllo allegati
		if ls_flag_allegati <> "T" then
			select flag_allegati
			into   :ls_stato_allegato
			from   comp_det_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda  and
			       anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and
					 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
			if sqlca.sqlcode = 0 and not lb_allegati then
				if ls_flag_allegati = "S" and ls_stato_allegato = "S" then lb_allegati = true
			end if
			if sqlca.sqlcode = 0 and lb_allegati then
				if ls_flag_allegati = "N" and ls_stato_allegato = "S" then lb_allegati = false
			end if
		end if
		
	loop
	close cu_det_ord_ven;

	if lb_ok = true and (lb_allegati or ls_flag_allegati = "T") then
		select rag_soc_1
		into   :ls_rag_soc_1
		from   anag_clienti
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_cliente=:ls_cod_cliente;
		
		dw_lista_ord_ven_stampa_prod.insertrow(0)
		dw_lista_ord_ven_stampa_prod.setrow(dw_lista_ord_ven_stampa_prod.rowcount())
		dw_lista_ord_ven_stampa_prod.setitem(dw_lista_ord_ven_stampa_prod.getrow(), "anno_registrazione", ll_anno_registrazione)
		dw_lista_ord_ven_stampa_prod.setitem(dw_lista_ord_ven_stampa_prod.getrow(), "num_registrazione", ll_num_registrazione)
		dw_lista_ord_ven_stampa_prod.setitem(dw_lista_ord_ven_stampa_prod.getrow(), "data_registrazione", ldt_data_registrazione)
		dw_lista_ord_ven_stampa_prod.setitem(dw_lista_ord_ven_stampa_prod.getrow(), "data_consegna", ldt_data_consegna)
		dw_lista_ord_ven_stampa_prod.setitem(dw_lista_ord_ven_stampa_prod.getrow(), "cod_cliente", ls_cod_cliente)
		dw_lista_ord_ven_stampa_prod.setitem(dw_lista_ord_ven_stampa_prod.getrow(), "tipo_ord_ven", ls_cod_tipo_ord_ven)
		dw_lista_ord_ven_stampa_prod.setitem(dw_lista_ord_ven_stampa_prod.getrow(), "ragione_sociale", ls_rag_soc_1)
		dw_lista_ord_ven_stampa_prod.setitem(dw_lista_ord_ven_stampa_prod.getrow(), "cod_deposito", ls_cod_deposito_origine)
		
		select des_deposito
		into :ls_des_deposito
		from anag_depositi
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_deposito=:ls_cod_deposito_origine;
		dw_lista_ord_ven_stampa_prod.setitem(dw_lista_ord_ven_stampa_prod.getrow(), "des_deposito", ls_des_deposito)
		
		select des_tipo_ord_ven
		into :ls_des_tipo_ord_ven
		from tab_tipi_ord_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_tipo_ord_ven=:ls_cod_tipo_ord_ven;
		dw_lista_ord_ven_stampa_prod.setitem(dw_lista_ord_ven_stampa_prod.getrow(), "des_tipo_ord_ven", ls_des_tipo_ord_ven)
		
		dw_lista_ord_ven_stampa_prod.setitem(dw_lista_ord_ven_stampa_prod.getrow(), "operatore", ls_operatore_ds)
	end if
next

setpointer(Arrow!)

destroy lds_data

dw_lista_ord_ven_stampa_prod.sort()
dw_lista_ord_ven_stampa_prod.resetupdate()

event post ue_cont_selected()
end event

event clicked;call super::clicked;event post ue_cont_selected()
end event

type dw_ottimizza_4 from uo_std_dw within w_stampa_ordini_prod
integer x = 50
integer y = 1356
integer width = 4421
integer height = 1024
integer taborder = 110
boolean bringtotop = true
string dataobject = "d_stampa_ordini_prod_ottimizza_4"
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

type dw_ottimizza_3 from datawindow within w_stampa_ordini_prod
integer x = 1691
integer y = 1256
integer width = 2789
integer height = 96
integer taborder = 120
boolean bringtotop = true
string title = "none"
string dataobject = "d_stampa_ordini_prod_ottimizza_3"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;if dwo.name = "b_ottimizza" then
	
	string	ls_cod_materia_prima, ls_matricola, ls_flag_tipo_ottimizzazione, ls_id_tes_lancio,ls_messaggio, ls_token, ls_ws
	long ll_i,ll_dim_ottimizzazione,ll_moltiplicatore,ll_anno_reg_ord_ven,ll_num_reg_ord_ven,ll_prog_riga_ord_ven, ll_cont, ll_response_error, ll_ret
	dec{4} ld_misura_taglio
	
	accepttext()
	
	select count(*)
	into	:ll_cont
	from	det_lancio_prod_ottimiz
	where id_tes_lancio_prod = :id_lancio_produzione_ottimizzato;
	
	if ll_cont > 0 then
		if g_mb.confirm("Attenzione: esiste già un archivio di ottimizzazioni. SI=Sovrascrivi  NO=Aggiungi") then
			delete		det_lancio_prod_ottimiz
			where 	id_tes_lancio_prod = :id_lancio_produzione_ottimizzato;
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore durante la cancellazione delle ottimizzazioni. " + sqlca.sqlerrtext)
				rollback;
				return
			end if
		end if
		
	end if
	
	ll_dim_ottimizzazione = getitemnumber(1,"al_dimensione_ottimizzazione")
	ll_moltiplicatore = getitemnumber(1,"al_moltiplicatore")
	
	// per sicurezza cancello sempre prima di procedere
	delete buffer_ottimizzazioni
	where is_tes_lancio_prod = :id_lancio_produzione_ottimizzato;
	
	for ll_i = 1 to dw_ottimizza_1.rowcount()
		
		ll_anno_reg_ord_ven = dw_ottimizza_1.getitemnumber(ll_i, "anno_registrazione")
		ll_num_reg_ord_ven = dw_ottimizza_1.getitemnumber(ll_i, "num_registrazione")
		ll_prog_riga_ord_ven = dw_ottimizza_1.getitemnumber(ll_i, "prog_riga_ord_ven")
		ls_cod_materia_prima = dw_ottimizza_1.getitemstring(ll_i,"cod_materia_prima")
		ls_matricola = dw_ottimizza_1.getitemstring(ll_i,"matricola")
		ld_misura_taglio = dw_ottimizza_1.getitemnumber(ll_i, "quan_utilizzo") * ll_moltiplicatore
		ls_matricola = dw_ottimizza_1.getitemstring(ll_i,"matricola")
		
		select flag_tipo_ottimizzazione
		into	:ls_flag_tipo_ottimizzazione
		from	anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_materia_prima;
				
				
		if not isnull(ls_flag_tipo_ottimizzazione) and ls_flag_tipo_ottimizzazione = '1' then

			INSERT INTO buffer_ottimizzazioni  
				( is_tes_lancio_prod,   
				  matricola,   
				  anno_reg_ord_ven,   
				  num_reg_ord_ven,   
				  prog_riga_ord_ven,   
				  cod_materia_prima,   
				  misura_ottimizzazione,   
				  misura_taglio )  
			VALUES (
				  :id_lancio_produzione_ottimizzato,
				  :ls_matricola,   
				  :ll_anno_reg_ord_ven,   
				  :ll_num_reg_ord_ven,   
				  :ll_prog_riga_ord_ven,   
				  :ls_cod_materia_prima,   
				  :ll_dim_ottimizzazione,   
				  :ld_misura_taglio )  ;
			if sqlca.sqlcode < 0 then
				g_mb.error( "ERRORE SQL", "Errore in fase di INSERT SQL tabella ottimizzazioni.~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
		end if
	next
	commit;
	
	ls_id_tes_lancio = string(id_lancio_produzione_ottimizzato)
	
	select ws_ottimizzazione,
			ws_token
	into	:ls_ws,
			:ls_token
	from	con_magazzino
	where cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode < 0 then
		g_mb.error( "ERRORE SQL", "Errore in lettura parametri WS in parametri magazzino.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if

	if isnull(ls_ws) or len(ls_ws) < 1 then
		g_mb.error( "WEBSERVICE", "Manca indirizzo WS nel parametri magazzino.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if

	if isnull(ls_token) or len(ls_token) < 1 then
		g_mb.error( "WEBSERVICE", "Manca indicazione TOKEN nel parametri magazzino.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if

	
	uo_xhr2 luo_xhr
	oleobject lole_json
	
	luo_xhr = create uo_xhr2
	
	luo_xhr.setrequestheader( "Token", ls_token)
	luo_xhr.getJson(ls_ws + "?num="+ls_id_tes_lancio + "&Requesttime="+ string(hour(now())) + string(minute(now())) + string(second(now())) )
	
	ll_response_error = luo_xhr.send()
	
	if ll_response_error <> 200 then
		try 
//			luo_xhr.responsejson(lole_json)
			ls_messaggio = luo_xhr.responsetext( )
			destroy luo_xhr
			
			g_mb.error("WEB SERVICE ERROR",ls_messaggio)
			
//			mle_response.text = string(lole_json.live)
		catch (XhrException e)
		end try
	else
		destroy luo_xhr
		g_mb.error("WEB SERVICE", "Ottimizzazione Effettuata!")	
		
		dw_ottimizza_4.reset()
		dw_ottimizza_4.settransobject(sqlca)
		ll_ret = dw_ottimizza_4.retrieve(id_lancio_produzione_ottimizzato)
		
	end if
	
end if



end event

type cb_stampa from commandbutton within w_stampa_ordini_prod
integer x = 3397
integer y = 2320
integer width = 366
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;wf_clicked_stampa(true,"D")
end event

type dw_elenco_operatori_selezionati from datawindow within w_stampa_ordini_prod
boolean visible = false
integer x = 73
integer y = 1100
integer width = 1851
integer height = 1000
integer taborder = 90
boolean bringtotop = true
string title = "none"
string dataobject = "d_elenco_operatori_selezionati"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event getfocus;//call super::getfocus;dw_ext_selezione.change_dw_current()
//s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
//s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
//s_cs_xx.parametri.parametro_tipo_ricerca = 2
end event

event itemchanged;choose case dwo.name
	case "flag_selezionato"
		if data="N" then
			//hai deselezionato l'operatore della riga: metti il checkbox "Seleziona Tutti a N"
			cbx_select_all.checked = false
		end if
		
end choose
end event

type dw_ottimizza_1 from uo_std_dw within w_stampa_ordini_prod
integer x = 46
integer y = 264
integer width = 4439
integer height = 992
integer taborder = 80
string dataobject = "d_stampa_ordini_prod_ottimizza_1"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event doubleclicked;call super::doubleclicked;long ll_i

if dwo.name = "flag_selezione_t" then
	
	if rowcount() > 0 then
		if getitemstring(1,"flag_selezione") = "S" then
			for ll_i = 1 to rowcount()
				setitem(ll_i, "flag_selezione","N")
			next
		else
			for ll_i = 1 to rowcount()
				setitem(ll_i, "flag_selezione","S")
			next
		end if
	end if
end if

if dwo.name = "compute_3" then
	
	if rowcount() > 0 then
		if getitemstring(1,"flag_selezione") = "S" then
			for ll_i = 1 to rowcount()
				if getitemstring(ll_i,"cod_materia_prima") = getitemstring(row,"cod_materia_prima") then
					setitem(ll_i, "flag_selezione","N")
				end if
			next
		else
			for ll_i = 1 to rowcount()
				if getitemstring(ll_i,"cod_materia_prima") = getitemstring(row,"cod_materia_prima") then
					setitem(ll_i, "flag_selezione","S")
				end if
			next
		end if
	end if
end if
end event

type cb_etichette from commandbutton within w_stampa_ordini_prod
integer x = 3008
integer y = 2320
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Etichette"
end type

event clicked;long		ll_rows, ll_i, ll_selected[]
s_stampa_etichette_ce lstr_stampa_etichette_ce

dw_lista_ord_ven_stampa_prod.get_selected_rows(ll_selected[])

ll_rows = upperbound(ll_selected)
if ll_rows = 0 then
	g_mb.warning("Selezionare un ordine dalla lista")
	return
end if

for ll_i = 1 to ll_rows
	lstr_stampa_etichette_ce.anno_ordine[ll_i] = dw_lista_ord_ven_stampa_prod.getitemnumber(ll_selected[ll_i], "anno_registrazione")
	lstr_stampa_etichette_ce.num_ordine[ll_i] = dw_lista_ord_ven_stampa_prod.getitemnumber(ll_selected[ll_i], "num_registrazione")
next

window_open_parm(w_stampa_etichette_ce, -1, lstr_stampa_etichette_ce)

/*
ll_rows = dw_lista_ord_ven_stampa_prod.rowcount()
ll_idx = 0

for ll_i = 1 to ll_rows
	if dw_lista_ord_ven_stampa_prod.getitemstring(ll_i, "flag_selezione") = "S" then
		ll_idx ++
		lstr_stampa_etichette_ce.anno_ordine[ll_idx] = dw_lista_ord_ven_stampa_prod.getitemnumber(ll_i, "anno_registrazione")
		lstr_stampa_etichette_ce.num_ordine[ll_idx] = dw_lista_ord_ven_stampa_prod.getitemnumber(ll_i, "num_registrazione")
	end if
next

if ll_idx = 0 then
	g_mb.warning("Selezionare un ordine dalla lista")
	return
end if

window_open_parm(w_stampa_etichette_ce, -1, lstr_stampa_etichette_ce)
*/		

end event

type dw_ext_selezione from uo_cs_xx_dw within w_stampa_ordini_prod
event ue_reset_dati_filtro ( )
integer x = 69
integer y = 260
integer width = 4411
integer height = 760
integer taborder = 60
boolean bringtotop = true
string dataobject = "d_ext_selezione_stampa_ord_prod"
end type

event ue_reset_dati_filtro();string						ls_null
datetime					ldt_data
integer					li_null


setnull(ls_null)
setnull(li_null)
ldt_data = datetime(today(),00:00:00)


dw_ext_selezione.setitem(dw_ext_selezione.getrow(),"data_consegna_inizio",datetime(date("01/01/1900"),00:00:00))
dw_ext_selezione.setitem(dw_ext_selezione.getrow(),"data_consegna_fine",datetime(date("31/12/2099"),00:00:00))
dw_ext_selezione.setitem(dw_ext_selezione.getrow(),"data_registrazione_inizio",datetime(date("01/01/1900"),00:00:00))
dw_ext_selezione.setitem(dw_ext_selezione.getrow(),"data_registrazione_fine",datetime(date("31/12/2099"),00:00:00))
dw_ext_selezione.setitem(dw_ext_selezione.getrow(),"cod_cliente", ls_null)
dw_ext_selezione.setitem(dw_ext_selezione.getrow(),"flag_operatori", "N")
dw_ext_selezione.setitem(dw_ext_selezione.getrow(),"flag_stampati", "N")
dw_ext_selezione.setitem(dw_ext_selezione.getrow(),"flag_con_commessa", "N")
dw_ext_selezione.setitem(dw_ext_selezione.getrow(),"flag_bloccati", "N")

//Chiesto da Beatrice: anche se si fa Annulla Ricerca (il flag impostato sul calcola distinte taglio NON deve essere resettato )
//dw_ext_selezione.setitem(dw_ext_selezione.getrow(),"flag_calcola_dt", "N")

dw_ext_selezione.setitem(dw_ext_selezione.getrow(),"flag_allegati", "T")


dw_ext_selezione.setitem(dw_ext_selezione.getrow(),"anno_ordine", f_anno_esercizio())
dw_ext_selezione.setitem(dw_ext_selezione.getrow(),"num_ordine", li_null)



end event

event itemchanged;call super::itemchanged;
if i_extendmode then
	choose case i_colname
		case "flag_operatori"
			if i_coltext = "N" then
				cbx_select_all.visible = false // stefanop 08/06/2010: progetto 140
				dw_elenco_operatori_selezionati.visible = false
			else
				cbx_select_all.visible = true // stefanop 08/06/2010: progetto 140
				dw_elenco_operatori_selezionati.visible = true

			end if
	end choose		
end if
end event

event pcd_new;call super::pcd_new;dw_ext_selezione.setitem(dw_ext_selezione.getrow(),"data_consegna_inizio",datetime(today(),00:00:00))
dw_ext_selezione.setitem(dw_ext_selezione.getrow(),"data_consegna_fine",datetime(RelativeDate(today(), 30),00:00:00))


end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ext_selezione,"cod_cliente")
		
	case "b_ricerca"
		accepttext()
		dw_lista_ord_ven_stampa_prod.change_dw_current()
		parent.triggerevent("pc_retrieve")
		dw_folder.fu_selecttab(2)	
		
	case "b_reset"
		event ue_reset_dati_filtro()
end choose
end event

type cb_lancia from commandbutton within w_stampa_ordini_prod
integer x = 2619
integer y = 2320
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Lancia Prod."
end type

event clicked;wf_lancio_produzione("D")

end event

type cbx_gen_commesse from checkbox within w_stampa_ordini_prod
integer x = 1189
integer y = 2320
integer width = 1371
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 553648127
string text = "Genera Commesse durante il lancio produzione."
end type

type tv_lancio from uo_tree_lancio_produzione within w_stampa_ordini_prod
integer x = 3200
integer y = 360
integer width = 1280
integer height = 1860
integer taborder = 20
boolean bringtotop = true
end type

event dragdrop;call super::dragdrop;long ll_selected[], ll_cont, ll_livello
treeviewitem ltv_item
s_tree_lancio_produzione ls_record, ls_record_null

if isnull(handle) or handle <= 0 then
	return 0
end if

il_handle = handle

getitem(handle,ltv_item)

ls_record = ltv_item.data
ll_livello = ls_record.livello

dw_lista_ord_ven_stampa_prod.get_selected_rows(ll_selected[])
ll_cont = upperbound(ll_selected[])

if ll_cont = 0 then 
	g_mb.warning("E' necessario selezionare alcuni ordini per creare sessioni di produzione!")
	return
end if

choose case ll_livello
	case -1
		// siamo sul ramo "Nuova Sessione Produzione"
		if g_mb.confirm( "NUOVA SESSIONE PRODUZIONE", g_str.format("Procedo con la creazione di una nuova sessione di produzione per le $1 righe selezionate?",ll_cont) ) then
			parent.event ue_nuovo_lancio( )
		end if
	case 0
		// aggiungo ad una sessione esistente
		if g_mb.confirm( "AGGIUNTA SESSIONE PRODUZIONE", g_str.format("Procedo con l'aggiunta delle $1 righe selezionate alla sessione di produzione selezionata?",ll_cont) ) then
			parent.event ue_aggiungi_a_sessione( )
		end if
	case else
		g_mb.warning("La creazione o aggiunta è possibile solo sui rami di tipo SESSIONE.")
end choose
end event

type dw_gg_lancio from datawindow within w_stampa_ordini_prod
integer x = 3200
integer y = 260
integer width = 1280
integer height = 100
integer taborder = 70
boolean bringtotop = true
string title = "none"
string dataobject = "d_stampa_ordini_prod_gg_lancio"
boolean border = false
boolean livescroll = true
end type

event itemchanged;accepttext()

wf_carica_lista_lancio_prod()
end event

