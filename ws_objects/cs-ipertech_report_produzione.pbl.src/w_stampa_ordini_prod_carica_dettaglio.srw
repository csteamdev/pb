﻿$PBExportHeader$w_stampa_ordini_prod_carica_dettaglio.srw
forward
global type w_stampa_ordini_prod_carica_dettaglio from w_cs_xx_principale
end type
type dw_prodotti from uo_dddw_checkbox within w_stampa_ordini_prod_carica_dettaglio
end type
type dw_selezione from uo_std_dw within w_stampa_ordini_prod_carica_dettaglio
end type
type cb_1 from commandbutton within w_stampa_ordini_prod_carica_dettaglio
end type
type dw_righe from uo_std_dw within w_stampa_ordini_prod_carica_dettaglio
end type
end forward

global type w_stampa_ordini_prod_carica_dettaglio from w_cs_xx_principale
integer width = 5426
integer height = 2788
string title = "Nuova Sessione Produzione da Righe Ordine "
dw_prodotti dw_prodotti
dw_selezione dw_selezione
cb_1 cb_1
dw_righe dw_righe
end type
global w_stampa_ordini_prod_carica_dettaglio w_stampa_ordini_prod_carica_dettaglio

type variables
string is_sql
end variables

on w_stampa_ordini_prod_carica_dettaglio.create
int iCurrent
call super::create
this.dw_prodotti=create dw_prodotti
this.dw_selezione=create dw_selezione
this.cb_1=create cb_1
this.dw_righe=create dw_righe
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_prodotti
this.Control[iCurrent+2]=this.dw_selezione
this.Control[iCurrent+3]=this.cb_1
this.Control[iCurrent+4]=this.dw_righe
end on

on w_stampa_ordini_prod_carica_dettaglio.destroy
call super::destroy
destroy(this.dw_prodotti)
destroy(this.dw_selezione)
destroy(this.cb_1)
destroy(this.dw_righe)
end on

event pc_setwindow;call super::pc_setwindow;string ls_sql

dw_selezione.insertrow(0)

ls_sql = 	"select cod_prodotto, des_prodotto from anag_prodotti where cod_prodotto in (select cod_modello from tab_flags_configuratore) and flag_blocco = 'N' order by 1"

dw_prodotti.uof_set_column_by_sql(ls_sql)
dw_prodotti.uof_set_parent_dw(dw_selezione, "cod_prodotto_finito", "cod_prodotto")

dw_prodotti.uof_set_column_width(1, 400)
dw_prodotti.uof_set_column_width(2, 800)

dw_prodotti.uof_set_column_name(1, "Cod.")
dw_prodotti.uof_set_column_name(2, "Des. Prodotto")

dw_prodotti.uof_set_column_align(1, "center")

guo_functions.uof_leggi_filtro( dw_selezione, "STAPR1")

is_sql = dw_righe.getsqlselect()



end event

type dw_prodotti from uo_dddw_checkbox within w_stampa_ordini_prod_carica_dettaglio
integer x = 549
integer y = 100
integer width = 1989
integer height = 824
integer taborder = 20
integer ii_width_percent = 0
end type

type dw_selezione from uo_std_dw within w_stampa_ordini_prod_carica_dettaglio
integer x = 23
integer y = 20
integer width = 5349
integer height = 120
integer taborder = 10
string dataobject = "d_stampa_ordini_prod_carica_dettaglio_sel"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event clicked;call super::clicked;choose case dwo.name
	case "cod_prodotto_finito"
		dw_prodotti.show()
		
	case "b_filtro_memo"
		guo_functions.uof_memorizza_filtro(dw_selezione, "STAPR1")
		
	case "b_filtro_reset"
		uo_functions.uof_reset_filtro( "STAPR1" )
		
	case "b_ricerca"
		dw_righe.event ue_retrieve()
end choose
end event

type cb_1 from commandbutton within w_stampa_ordini_prod_carica_dettaglio
integer x = 4914
integer y = 2580
integer width = 466
integer height = 92
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Nuova Sessione"
end type

event clicked;long ll_ret, ll_y, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_riga, ll_id_tes_lancio_prod
string	ls_sql, ls_descrizione
datetime ldt_oggi
datastore lds_data


if not g_mb.confirm("Sei sicuro di voler creare una nuova sessione con le righe selezionate?") then return
	
ldt_oggi = datetime(today(), now())
ls_descrizione = g_str.format("$1 - $2 ", string(ldt_oggi,"dd/mm/yyyy hh:mm:ss"), s_cs_xx.cod_utente)

dw_righe.accepttext()

insert into tes_lancio_prod(
	cod_azienda,
	descrizione,
	cod_utente,
	flag_terminato,
	data_registrazione
	)
values (
	:s_cs_xx.cod_azienda,
	:ls_descrizione,
	:s_cs_xx.cod_utente,
	'N',
	:ldt_oggi	
	);
if sqlca.sqlcode < 0 then
	rollback;
	g_mb.error( "Errore INSERT testata lancio produzione" + sqlca.sqlerrtext )
	return -1
end if

guo_functions.uof_get_identity(sqlca, ll_id_tes_lancio_prod)

for ll_riga = 1 to dw_righe.rowcount( )
	
	if dw_righe.getitemstring(ll_riga,"flag_selezione") = "N" then continue
		
	ll_anno_registrazione =dw_righe.getitemnumber(ll_riga,2)
	ll_num_registrazione =dw_righe.getitemnumber(ll_riga,3)
	ll_prog_riga_ord_ven =dw_righe.getitemnumber(ll_riga,4)
	
		
	insert into det_lancio_prod(
		cod_azienda,
		id_tes_lancio_prod,
		anno_reg_ord_ven ,
		num_reg_ord_ven ,
		prog_riga_ord_ven )
	values (
		:s_cs_xx.cod_azienda,
		:ll_id_tes_lancio_prod,
		:ll_anno_registrazione,
		:ll_num_registrazione,
		:ll_prog_riga_ord_ven);
		
	if sqlca.sqlcode < 0 then
		rollback;
		g_mb.error( "Errore INSERT dettaglio lancio produzione" + sqlca.sqlerrtext )
		return -1
	end if
		
next

commit;

close(parent)
end event

type dw_righe from uo_std_dw within w_stampa_ordini_prod_carica_dettaglio
event ue_retrieve ( )
integer x = 23
integer y = 140
integer width = 5349
integer height = 2420
integer taborder = 10
string dataobject = "d_stampa_ordini_prod_carica_dettaglio"
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

event ue_retrieve();string		ls_cod_cliente, ls_sql, ls_where, ls_cod_prodotti[], ls_str
long		ll_anno_ordine, ll_num_ordine, ll_ret, ll_i
datetime ldt_data_consegna_inizio, ldt_data_consegna_fine, ldt_data_reg_inizio, ldt_data_reg_fine

uo_cs_xx_dw ldw_selezione

ldw_selezione = i_openparm
ldt_data_consegna_inizio = ldw_selezione.getitemdatetime(ldw_selezione.getrow(),"data_consegna_inizio")
ldt_data_consegna_fine = ldw_selezione.getitemdatetime(ldw_selezione.getrow(),"data_consegna_fine")

ldt_data_reg_inizio = ldw_selezione.getitemdatetime(ldw_selezione.getrow(),"data_registrazione_inizio")
ldt_data_reg_fine = ldw_selezione.getitemdatetime(ldw_selezione.getrow(),"data_registrazione_fine")

ls_cod_cliente = ldw_selezione.getitemstring(ldw_selezione.getrow(),"cod_cliente")

ll_anno_ordine = ldw_selezione.getitemnumber(ldw_selezione.getrow(),"anno_ordine")
ll_num_ordine = ldw_selezione.getitemnumber(ldw_selezione.getrow(),"num_ordine")

g_str.explode( dw_selezione.getitemstring(1,"cod_prodotto_finito") , ",", ls_cod_prodotti)


//dw_prodotti.uof_get_selected_column_as_array("cod_prodotto",ref ls_cod_prodotti)


ls_sql = is_sql

ls_where = ""
if not isnull(ldt_data_consegna_inizio) and ldt_data_consegna_inizio > datetime(date("01/01/1900"),00:00:00) then
	ls_where += " and T.data_consegna >= '" + string(ldt_data_consegna_inizio,s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_data_consegna_fine) and ldt_data_consegna_fine > datetime(date("01/01/1900"),00:00:00) then
	ls_where += " and T.data_consegna <= '" + string(ldt_data_consegna_fine,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_reg_inizio) and ldt_data_reg_inizio > datetime(date("01/01/1900"),00:00:00) then
	ls_where += " and T.data_registrazione >= '" + string(ldt_data_reg_inizio,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_reg_fine) and ldt_data_reg_fine > datetime(date("01/01/1900"),00:00:00) then
	ls_where += " and T.data_registrazione <= '" + string(ldt_data_reg_fine,s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ll_anno_ordine) and ll_anno_ordine>0  then
	ls_where += " and T.anno_registrazione = " + string(ll_anno_ordine)
end if

if not isnull(ll_num_ordine) and ll_num_ordine>0  then
	ls_where += " and T.num_registrazione = " + string(ll_num_ordine)
end if

if upperbound(ls_cod_prodotti) > 0 and ls_cod_prodotti[1] <> "TUTTI" then
	ls_str = ""
	for ll_i = 1 to upperbound(ls_cod_prodotti)
		if len(ls_str) > 0 then ls_str += ","
		ls_str += g_str.format("'$1'",trim(ls_cod_prodotti[ll_i]))
	next
	ls_where += g_str.format(" and D.cod_prodotto in ($1) ", ls_str)
end if


ls_sql = ls_sql + ls_where + " order by 2,3,4 "

ll_ret = dw_righe.setsqlselect( ls_sql )

dw_righe.settransobject(sqlca)

dw_righe.retrieve()


end event

