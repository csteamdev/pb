﻿$PBExportHeader$w_test_ottimizza.srw
forward
global type w_test_ottimizza from window
end type
type mle_1 from multilineedit within w_test_ottimizza
end type
type cb_1 from commandbutton within w_test_ottimizza
end type
type em_10 from editmask within w_test_ottimizza
end type
type em_9 from editmask within w_test_ottimizza
end type
type em_8 from editmask within w_test_ottimizza
end type
type em_7 from editmask within w_test_ottimizza
end type
type em_6 from editmask within w_test_ottimizza
end type
type em_5 from editmask within w_test_ottimizza
end type
type em_4 from editmask within w_test_ottimizza
end type
type em_3 from editmask within w_test_ottimizza
end type
type em_2 from editmask within w_test_ottimizza
end type
type em_1 from editmask within w_test_ottimizza
end type
end forward

global type w_test_ottimizza from window
integer width = 3163
integer height = 1980
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
mle_1 mle_1
cb_1 cb_1
em_10 em_10
em_9 em_9
em_8 em_8
em_7 em_7
em_6 em_6
em_5 em_5
em_4 em_4
em_3 em_3
em_2 em_2
em_1 em_1
end type
global w_test_ottimizza w_test_ottimizza

on w_test_ottimizza.create
this.mle_1=create mle_1
this.cb_1=create cb_1
this.em_10=create em_10
this.em_9=create em_9
this.em_8=create em_8
this.em_7=create em_7
this.em_6=create em_6
this.em_5=create em_5
this.em_4=create em_4
this.em_3=create em_3
this.em_2=create em_2
this.em_1=create em_1
this.Control[]={this.mle_1,&
this.cb_1,&
this.em_10,&
this.em_9,&
this.em_8,&
this.em_7,&
this.em_6,&
this.em_5,&
this.em_4,&
this.em_3,&
this.em_2,&
this.em_1}
end on

on w_test_ottimizza.destroy
destroy(this.mle_1)
destroy(this.cb_1)
destroy(this.em_10)
destroy(this.em_9)
destroy(this.em_8)
destroy(this.em_7)
destroy(this.em_6)
destroy(this.em_5)
destroy(this.em_4)
destroy(this.em_3)
destroy(this.em_2)
destroy(this.em_1)
end on

type mle_1 from multilineedit within w_test_ottimizza
integer x = 987
integer y = 904
integer width = 2112
integer height = 932
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_test_ottimizza
integer x = 969
integer y = 524
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "OTTIMIZZA"
end type

event clicked;long						ll_tagli[], ll_i, ll_j
uo_ottimizza_tagli 		luo_ottimizza

luo_ottimizza = create uo_ottimizza_tagli

ll_tagli[1] = long( em_1.text )
ll_tagli[2] = long( em_2.text )
ll_tagli[3] = long( em_3.text )
ll_tagli[4] = long( em_4.text )
ll_tagli[5] = long( em_5.text )
ll_tagli[6] = long( em_6.text )
ll_tagli[7] = long( em_7.text )
ll_tagli[8] = long( em_8.text )
ll_tagli[9] = long( em_9.text )
ll_tagli[10] = long( em_10.text )

luo_ottimizza.uof_test( ll_tagli[] )

luo_ottimizza.uof_ottimizza( ll_tagli[] )

mle_1.text = ""

for ll_i = 1 to upperbound( luo_ottimizza.istr_profili.astr_profili[] )
	
	mle_1.text += g_str.format("PROFILO $1 TAGLI = ", ll_i)
	
	for ll_j = 1 to upperbound( luo_ottimizza.istr_profili.astr_profili[ll_i].astr_tagli[] ) 
		mle_1.text += g_str.format(" $1mm, ",luo_ottimizza.istr_profili.astr_profili[ll_i].astr_tagli[ll_j])
	next
	
	mle_1.text += "~r~n"
	
next

destroy luo_ottimizza

return



end event

type em_10 from editmask within w_test_ottimizza
integer x = 14
integer y = 1384
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "5700"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#####"
end type

type em_9 from editmask within w_test_ottimizza
integer x = 9
integer y = 1200
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "3500"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#####"
end type

type em_8 from editmask within w_test_ottimizza
integer x = 14
integer y = 1048
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "215"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#####"
end type

type em_7 from editmask within w_test_ottimizza
integer x = 18
integer y = 904
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "1107"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#####"
end type

type em_6 from editmask within w_test_ottimizza
integer x = 18
integer y = 744
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "1502"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#####"
end type

type em_5 from editmask within w_test_ottimizza
integer x = 14
integer y = 584
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "857"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#####"
end type

type em_4 from editmask within w_test_ottimizza
integer x = 23
integer y = 428
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "759"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#####"
end type

type em_3 from editmask within w_test_ottimizza
integer x = 18
integer y = 308
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "615"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#####"
end type

type em_2 from editmask within w_test_ottimizza
integer x = 27
integer y = 164
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "315"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#####"
end type

type em_1 from editmask within w_test_ottimizza
integer x = 23
integer y = 12
integer width = 402
integer height = 112
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "300"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "#####"
end type

