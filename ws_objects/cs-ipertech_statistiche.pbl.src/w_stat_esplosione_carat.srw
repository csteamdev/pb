﻿$PBExportHeader$w_stat_esplosione_carat.srw
forward
global type w_stat_esplosione_carat from w_cs_xx_risposta
end type
type dw_export from datawindow within w_stat_esplosione_carat
end type
type dw_note from datawindow within w_stat_esplosione_carat
end type
type dw_non_note from datawindow within w_stat_esplosione_carat
end type
type cb_esporta from commandbutton within w_stat_esplosione_carat
end type
type cb_chiudi from commandbutton within w_stat_esplosione_carat
end type
type cb_stampa from commandbutton within w_stat_esplosione_carat
end type
type dw_esplosione_carat from uo_cs_xx_dw within w_stat_esplosione_carat
end type
end forward

global type w_stat_esplosione_carat from w_cs_xx_risposta
integer x = 0
integer y = 0
integer width = 4649
integer height = 2924
string title = "Esplosione caratteristiche"
boolean resizable = false
dw_export dw_export
dw_note dw_note
dw_non_note dw_non_note
cb_esporta cb_esporta
cb_chiudi cb_chiudi
cb_stampa cb_stampa
dw_esplosione_carat dw_esplosione_carat
end type
global w_stat_esplosione_carat w_stat_esplosione_carat

type variables

end variables

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();long   ll_i, ll_j, ll_riga, ll_anno_ord, ll_num_ord, ll_riga_ord, ll_riga_rif

string ls_cod_prodotto, ls_des_prodotto, ls_cod_tessuto, ls_cod_mantovana, ls_cod_verniciatura, ls_cod_comando, &
		 ls_cod_comando_2, ls_cod_supporto, ls_prod_test, ls_tess_test, ls_mant_test, ls_vern_test, ls_com_test, &
		 ls_com2_test, ls_supp_test, ls_des_tessuto, ls_des_mantovana, ls_des_verniciatura, ls_des_comando, &
		 ls_des_comando_2, ls_des_supporto, ls_colore_tessuto, ls_colore_tessuto_test, ls_prod_prec, ls_tess_prec, &
		 ls_col_prec, ls_mant_prec, ls_vern_prec, ls_com_prec, ls_com2_prec, ls_supp_prec, ls_um_mag, ls_um_ven, &
		 ls_mag_test, ls_ven_test, ls_mag_prec, ls_ven_prec
		 
dec{4} ld_dim_x, ld_dim_y, ld_quantita, ld_fatturato, ld_quan_neg, ld_fatt_neg, ld_x_test, ld_y_test, ld_fat_rif, &
       ld_x_prec, ld_y_prec, ld_quan_ven


setpointer(hourglass!)

ls_prod_prec = ""

ls_tess_prec = ""

ls_col_prec = ""

ls_mant_prec = ""

ls_vern_prec = ""

ls_com_prec = ""

ls_com2_prec = ""

ls_supp_prec = ""

ld_x_prec = 0

ld_y_prec = 0

ls_mag_prec = ""

ls_ven_prec = ""

for ll_i = 1 to dw_non_note.rowcount()
	
	ll_riga_rif = dw_non_note.getitemnumber(ll_i,"riga_riferimento")
	
	if not isnull(ll_riga_rif) and ll_riga_rif > 0 then
		continue
	end if
	
	ll_anno_ord = dw_non_note.getitemnumber(ll_i,"anno_ordine")
	
	ll_num_ord = dw_non_note.getitemnumber(ll_i,"num_ordine")
	
	ll_riga_ord = dw_non_note.getitemnumber(ll_i,"riga_ordine")
	
	ls_cod_prodotto = dw_non_note.getitemstring(ll_i,"cod_prodotto")
	
	if isnull(ls_cod_prodotto) then
		ls_cod_prodotto = ""
	end if
	
	ls_des_prodotto = dw_non_note.getitemstring(ll_i,"des_prodotto")
	
	ls_cod_tessuto = dw_non_note.getitemstring(ll_i,"cod_tessuto")
	
	if isnull(ls_cod_tessuto) then
		ls_cod_tessuto = ""
	end if
	
	ls_colore_tessuto = dw_non_note.getitemstring(ll_i,"cod_colore_tessuto")
	
	if isnull(ls_colore_tessuto) then
		ls_colore_tessuto = ""
	end if
	
	ls_cod_mantovana = dw_non_note.getitemstring(ll_i,"cod_tessuto_mantovana")
	
	if isnull(ls_cod_mantovana) then
		ls_cod_mantovana = ""
	end if
	
	ls_cod_verniciatura = dw_non_note.getitemstring(ll_i,"cod_verniciatura")
	
	if isnull(ls_cod_verniciatura) then
		ls_cod_verniciatura = ""
	end if
	
	ls_cod_comando = dw_non_note.getitemstring(ll_i,"cod_comando")
	
	if isnull(ls_cod_comando) then
		ls_cod_comando = ""
	end if
	
	ls_cod_comando_2 = dw_non_note.getitemstring(ll_i,"cod_comando_2")
	
	if isnull(ls_cod_comando_2) then
		ls_cod_comando_2 = ""
	end if
	
	ls_cod_supporto = dw_non_note.getitemstring(ll_i,"cod_supporto")
	
	if isnull(ls_cod_supporto) then
		ls_cod_supporto = ""
	end if
		 
	ld_dim_x = dw_non_note.getitemnumber(ll_i,"dim_x")
	
	if isnull(ld_dim_x) then
		ld_dim_x = 0
	end if
	
	ld_dim_y = dw_non_note.getitemnumber(ll_i,"dim_y")
	
	if isnull(ld_dim_y) then
		ld_dim_y = 0
	end if
	
	ls_um_mag = dw_non_note.getitemstring(ll_i,"cod_misura_mag")
	
	if isnull(ls_um_mag) then
		ls_um_mag = ""
	end if
	
	ls_um_ven = dw_non_note.getitemstring(ll_i,"cod_misura_ven")
	
	if isnull(ls_um_ven) then
		ls_um_ven = ""
	end if
	
	if not isnull(ll_anno_ord) and not isnull(ll_num_ord) and not isnull(ll_riga_ord) then
		
		select sum(round((quan_fatturata * prezzo_vendita) / 100 * (100 - sconto_1) / 100 * (100 - sconto_2) / 100 * (100 - sconto_tes) / 100 * (100 - sconto_pagamento),2))
		into   :ld_fat_rif
      from   view_dati_prodotti  
      where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_ordine = :ll_anno_ord and
				 num_ordine = :ll_num_ord and
				 riga_riferimento = :ll_riga_ord;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore in lettura fatturato delle righe di riferimento: " + sqlca.sqlerrtext)
			exit
		end if
		
		if isnull(ld_fat_rif) then
			ld_fat_rif = 0
		end if
		
	end if
	
	if ls_cod_prodotto <> ls_prod_prec or ls_cod_tessuto <> ls_tess_prec or ls_colore_tessuto <> ls_col_prec or &
		ls_cod_mantovana <> ls_mant_prec or ls_cod_verniciatura <> ls_vern_prec or ls_cod_comando <> ls_com_prec or &
		ls_cod_comando_2 <> ls_com2_prec or ls_cod_supporto <> ls_supp_prec or ld_dim_x <> ld_x_prec or &
		ld_dim_y <> ld_y_prec or ls_mag_prec <> ls_um_mag or ls_ven_prec <> ls_um_ven then
		
		ls_prod_prec = ls_cod_prodotto

		ls_tess_prec = ls_cod_tessuto
		
		ls_col_prec = ls_colore_tessuto
		
		ls_mant_prec = ls_cod_mantovana
		
		ls_vern_prec = ls_cod_verniciatura
		
		ls_com_prec = ls_cod_comando
		
		ls_com2_prec = ls_cod_comando_2
		
		ls_supp_prec = ls_cod_supporto
		
		ld_x_prec = ld_dim_x
		
		ld_y_prec = ld_dim_y
		
		ls_mag_prec = ls_um_mag
		
		ls_ven_prec = ls_um_ven
		
		ld_quantita = dw_non_note.getitemnumber(ll_i,"quantita")
		
		ld_quan_ven = dw_non_note.getitemnumber(ll_i,"quan_ven")
	
		ld_fatturato = dw_non_note.getitemnumber(ll_i,"fatturato")
	
		for ll_j = 1 to dw_note.rowcount()
			
			ls_prod_test = dw_note.getitemstring(ll_j,"cod_prodotto")
			
			if isnull(ls_prod_test) then
				ls_prod_test = ""
			end if
			
			if ls_prod_test <> ls_cod_prodotto then
				continue
			end if
			
			ls_tess_test = dw_note.getitemstring(ll_j,"cod_tessuto")
			
			if isnull(ls_tess_test) then
				ls_tess_test = ""
			end if
			
			if ls_tess_test <> ls_cod_tessuto then
				continue
			end if
			
			ls_colore_tessuto_test = dw_note.getitemstring(ll_j,"cod_colore_tessuto")
			
			if isnull(ls_colore_tessuto_test) then
				ls_colore_tessuto_test = ""
			end if
			
			if ls_colore_tessuto_test <> ls_colore_tessuto then
				continue
			end if
			
			ls_mant_test = dw_note.getitemstring(ll_j,"cod_tessuto_mantovana")
			
			if isnull(ls_mant_test) then
				ls_mant_test = ""
			end if
			
			if ls_mant_test <> ls_cod_mantovana then
				continue
			end if
			
			ls_vern_test = dw_note.getitemstring(ll_j,"cod_verniciatura")
			
			if isnull(ls_vern_test) then
				ls_vern_test = ""
			end if
			
			if ls_vern_test <> ls_cod_verniciatura then
				continue
			end if
			
			ls_com_test = dw_note.getitemstring(ll_j,"cod_comando")
			
			if isnull(ls_com_test) then
				ls_com_test = ""
			end if
			
			if ls_com_test <> ls_cod_comando then
				continue
			end if
			
			ls_com2_test = dw_note.getitemstring(ll_j,"cod_comando_2")
			
			if isnull(ls_com2_test) then
				ls_com2_test = ""
			end if
			
			if ls_com2_test <> ls_cod_comando_2 then
				continue
			end if
			
			ls_supp_test = dw_note.getitemstring(ll_j,"cod_supporto")
			
			if isnull(ls_supp_test) then
				ls_supp_test = ""
			end if
			
			if ls_supp_test <> ls_cod_supporto then
				continue
			end if
			
			ld_x_test = dw_note.getitemnumber(ll_j,"dim_x")
			
			if isnull(ld_x_test) then
				ld_x_test = 0
			end if
			
			if ld_x_test <> ld_dim_x then
				continue
			end if
			
			ld_y_test = dw_note.getitemnumber(ll_j,"dim_y")
			
			if isnull(ld_y_test) then
				ld_y_test = 0
			end if
			
			if ld_y_test <> ld_dim_y then
				continue
			end if
			
			ls_mag_test = dw_note.getitemstring(ll_j,"um_mag")
			
			if isnull(ls_mag_test) then
				ls_mag_test = ""
			end if
			
			if ls_mag_test <> ls_um_mag then
				continue
			end if
			
			ls_ven_test = dw_note.getitemstring(ll_j,"um_ven")
			
			if isnull(ls_ven_test) then
				ls_ven_test = ""
			end if
			
			if ls_ven_test <> ls_um_ven then
				continue
			end if
			
			ld_quan_neg = (dw_note.getitemnumber(ll_j,"quantita") * 2)
			ld_quantita = ld_quantita - ld_quan_neg
			ld_quan_neg = (dw_note.getitemnumber(ll_j,"quan_ven") * 2)
			ld_quan_ven = ld_quan_ven - ld_quan_neg
			ld_fatt_neg = (dw_note.getitemnumber(ll_j,"fatturato") * 2)
			ld_fatturato = ld_fatturato - ld_fatt_neg
			exit
		
		next
	
		if ls_cod_tessuto <> "" then
			
			select des_prodotto
			into   :ls_des_tessuto
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_tessuto;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore nella select di anag_prodotti: " + sqlca.sqlerrtext)
				exit
			end if
		
		else
			
			ls_cod_tessuto = " "
			ls_des_tessuto = " "
			
		end if
		
		if ls_cod_mantovana <> "" then
			
			select des_prodotto
			into   :ls_des_mantovana
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_mantovana;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore nella select di anag_prodotti: " + sqlca.sqlerrtext)
				exit
			end if
			
		else
			
			ls_cod_mantovana = " "
			ls_des_mantovana = " "	
			
		end if
		
		if ls_cod_verniciatura <> "" then
			
			select des_prodotto
			into   :ls_des_verniciatura
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_verniciatura;
	
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore nella select di anag_prodotti: " + sqlca.sqlerrtext)
				exit
			end if
			
		else
			
			ls_cod_verniciatura = " "
			ls_des_verniciatura = " "	
	
		end if
	
		if ls_cod_comando <> "" then
	
			select des_prodotto
			into   :ls_des_comando
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_comando;
	
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore nella select di anag_prodotti: " + sqlca.sqlerrtext)
				exit
			end if
			
		else
			
			ls_cod_comando = " "
			ls_des_comando = " "	
	
		end if
	
		if ls_cod_comando_2 <> "" then
	
			select des_prodotto
			into   :ls_des_comando_2
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_comando_2;
	
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore nella select di anag_prodotti: " + sqlca.sqlerrtext)
				exit
			end if
			
		else
			
			ls_cod_comando_2 = " "
			ls_des_comando_2 = " "	
	
		end if
	
		if ls_cod_supporto <> "" then
	
			select des_prodotto
			into   :ls_des_supporto
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_supporto;
	
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore nella select di anag_prodotti: " + sqlca.sqlerrtext)
				exit
			end if
			
		else
			
			ls_cod_supporto = " "
			ls_des_supporto = " "	
	
		end if
	
		ll_riga = dw_esplosione_carat.insertrow(0)
		dw_esplosione_carat.setitem(ll_riga,"dim_x",ld_dim_x)
		dw_esplosione_carat.setitem(ll_riga,"dim_y",ld_dim_y)
		dw_esplosione_carat.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)
		dw_esplosione_carat.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
		dw_esplosione_carat.setitem(ll_riga,"cod_tessuto",ls_des_tessuto)
		dw_esplosione_carat.setitem(ll_riga,"cod_colore_tessuto",ls_colore_tessuto)
		dw_esplosione_carat.setitem(ll_riga,"cod_tessuto_mantovana",ls_cod_mantovana + "~n" + ls_des_mantovana)
		dw_esplosione_carat.setitem(ll_riga,"cod_verniciatura",ls_cod_verniciatura + "~n" + ls_des_verniciatura)
		dw_esplosione_carat.setitem(ll_riga,"cod_comando",ls_cod_comando + "~n" + ls_des_comando)
		dw_esplosione_carat.setitem(ll_riga,"cod_comando_2",ls_cod_comando_2 + "~n" + ls_des_comando_2)
		dw_esplosione_carat.setitem(ll_riga,"cod_supporto",ls_cod_supporto + "~n" + ls_des_supporto)
		dw_esplosione_carat.setitem(ll_riga,"um_mag",ls_um_mag)
		dw_esplosione_carat.setitem(ll_riga,"quantita",ld_quantita)
		dw_esplosione_carat.setitem(ll_riga,"um_ven",ls_um_ven)
		dw_esplosione_carat.setitem(ll_riga,"quan_ven",ld_quan_ven)
		
		if ls_um_mag = ls_um_ven and ld_quantita <> ld_quan_ven and ld_quan_ven = 0 then
			
			dw_esplosione_carat.setitem(ll_riga,"quan_ven",ld_quantita)
			
		end if
		
		ld_fatturato = round(ld_fatturato + ld_fat_rif,2)
		dw_esplosione_carat.setitem(ll_riga,"fatturato",ld_fatturato)
		
	else
		
		ld_fatturato = round(dw_esplosione_carat.getitemnumber(ll_riga,"fatturato") + ld_fat_rif,2)
		dw_esplosione_carat.setitem(ll_riga,"fatturato",ld_fatturato)
		
	end if

next

setpointer(arrow!)

return 0
end function

on w_stat_esplosione_carat.create
int iCurrent
call super::create
this.dw_export=create dw_export
this.dw_note=create dw_note
this.dw_non_note=create dw_non_note
this.cb_esporta=create cb_esporta
this.cb_chiudi=create cb_chiudi
this.cb_stampa=create cb_stampa
this.dw_esplosione_carat=create dw_esplosione_carat
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_export
this.Control[iCurrent+2]=this.dw_note
this.Control[iCurrent+3]=this.dw_non_note
this.Control[iCurrent+4]=this.cb_esporta
this.Control[iCurrent+5]=this.cb_chiudi
this.Control[iCurrent+6]=this.cb_stampa
this.Control[iCurrent+7]=this.dw_esplosione_carat
end on

on w_stat_esplosione_carat.destroy
call super::destroy
destroy(this.dw_export)
destroy(this.dw_note)
destroy(this.dw_non_note)
destroy(this.cb_esporta)
destroy(this.cb_chiudi)
destroy(this.cb_stampa)
destroy(this.dw_esplosione_carat)
end on

event pc_setwindow;call super::pc_setwindow;dw_esplosione_carat.ib_dw_report = true

dw_esplosione_carat.object.datawindow.print.preview.rulers = 'Yes'

dw_esplosione_carat.object.datawindow.print.preview = 'Yes'

dw_non_note.settransobject(sqlca)

dw_non_note.retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_s_1, &
									  s_cs_xx.parametri.parametro_data_1, s_cs_xx.parametri.parametro_data_2)

dw_note.settransobject(sqlca)

dw_note.retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_s_1, &
									  s_cs_xx.parametri.parametro_data_1, s_cs_xx.parametri.parametro_data_2)

dw_esplosione_carat.object.t_data.text = "Dal " + string(date(s_cs_xx.parametri.parametro_data_1)) + &
											  " al " + string(date(s_cs_xx.parametri.parametro_data_2))
											  
wf_report()
end event

event closequery;call super::closequery;if dw_esplosione_carat.rowcount() > 0 then
	if g_mb.messagebox("APICE","Chiudere esplosione caratteristiche?",question!,yesno!,2) = 2 then
		return 1
	end if	
end if
end event

event open;call super::open;x = 0
y = 0

end event

type dw_export from datawindow within w_stat_esplosione_carat
boolean visible = false
integer x = 274
integer y = 2760
integer width = 91
integer height = 72
integer taborder = 20
string dataobject = "d_prod_statistiche_esplodi_export"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_note from datawindow within w_stat_esplosione_carat
boolean visible = false
integer x = 160
integer y = 2760
integer width = 91
integer height = 72
integer taborder = 30
string dataobject = "d_prod_statistiche_esplodi_2"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_non_note from datawindow within w_stat_esplosione_carat
boolean visible = false
integer x = 46
integer y = 2760
integer width = 91
integer height = 72
integer taborder = 20
string dataobject = "d_prod_statistiche_esplodi_1"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_esporta from commandbutton within w_stat_esplosione_carat
integer x = 3406
integer y = 2740
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esporta"
end type

event clicked;long    ll_i, ll_riga, ll_pos

string  ls_cod_prodotto, ls_des_prodotto, ls_tessuto, ls_mantovana, ls_com_1, ls_com_2, ls_supporto, &
		  ls_verniciatura, ls_cod_colore_tessuto, ls_um_mag, ls_um_ven
		 
decimal ld_quantita, ld_fatturato, ld_x, ld_y, ld_quan_ven		 


for ll_i = 1 to dw_esplosione_carat.rowcount()
	
	ls_cod_prodotto = dw_esplosione_carat.getitemstring(ll_i,"cod_prodotto")
	
	ls_des_prodotto = dw_esplosione_carat.getitemstring(ll_i,"des_prodotto")
	
	ls_tessuto = dw_esplosione_carat.getitemstring(ll_i,"cod_tessuto")
	
	ls_cod_colore_tessuto = dw_esplosione_carat.getitemstring(ll_i,"cod_colore_tessuto")
	
	ls_mantovana = dw_esplosione_carat.getitemstring(ll_i,"cod_tessuto_mantovana")
	
	ls_com_1 = dw_esplosione_carat.getitemstring(ll_i,"cod_comando")
	
	ls_com_2 = dw_esplosione_carat.getitemstring(ll_i,"cod_comando_2")
	
	ls_supporto = dw_esplosione_carat.getitemstring(ll_i,"cod_supporto")
	
	ls_verniciatura = dw_esplosione_carat.getitemstring(ll_i,"cod_verniciatura")
	
	ld_x = dw_esplosione_carat.getitemnumber(ll_i,"dim_x")
	
	ld_y = dw_esplosione_carat.getitemnumber(ll_i,"dim_y")
	
	ls_um_mag = dw_esplosione_carat.getitemstring(ll_i,"um_mag")
	
	ld_quantita = dw_esplosione_carat.getitemnumber(ll_i,"quantita")
	
	ls_um_ven = dw_esplosione_carat.getitemstring(ll_i,"um_ven")
	
	ld_quan_ven = dw_esplosione_carat.getitemnumber(ll_i,"quan_ven")
	
	ld_fatturato = dw_esplosione_carat.getitemnumber(ll_i,"fatturato")
	
	ll_riga = dw_export.insertrow(0)
	
	dw_export.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)
	
	dw_export.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
	
	ll_pos = pos(ls_tessuto,"~n",1)
	
	dw_export.setitem(ll_riga,"des_tessuto",trim(right(ls_tessuto,len(ls_tessuto) - ll_pos)))
	
	dw_export.setitem(ll_riga,"cod_colore_tessuto",ls_cod_colore_tessuto)
	
	ll_pos = pos(ls_mantovana,"~n",1)
	
	dw_export.setitem(ll_riga,"cod_tessuto_mantovana",trim(left(ls_mantovana,ll_pos - 1)))
	
	dw_export.setitem(ll_riga,"des_mantovana",trim(right(ls_mantovana,len(ls_mantovana) - ll_pos)))
	
	ll_pos = pos(ls_com_1,"~n",1)
	
	dw_export.setitem(ll_riga,"cod_comando",trim(left(ls_com_1,ll_pos - 1)))
	
	dw_export.setitem(ll_riga,"des_comando",trim(right(ls_com_1,len(ls_com_1) - ll_pos)))
	
	ll_pos = pos(ls_com_2,"~n",1)
	
	dw_export.setitem(ll_riga,"cod_comando_2",trim(left(ls_com_2,ll_pos - 1)))
	
	dw_export.setitem(ll_riga,"des_comando_2",trim(right(ls_com_2,len(ls_com_2) - ll_pos)))
	
	ll_pos = pos(ls_supporto,"~n",1)
	
	dw_export.setitem(ll_riga,"cod_supporto",trim(left(ls_supporto,ll_pos - 1)))
	
	dw_export.setitem(ll_riga,"des_supporto",trim(right(ls_supporto,len(ls_supporto) - ll_pos)))
	
	ll_pos = pos(ls_verniciatura,"~n",1)
	
	dw_export.setitem(ll_riga,"cod_verniciatura",trim(left(ls_verniciatura,ll_pos - 1)))
	
	dw_export.setitem(ll_riga,"des_verniciatura",trim(right(ls_verniciatura,len(ls_verniciatura) - ll_pos)))
	
	dw_export.setitem(ll_riga,"dim_x",ld_x)
	
	dw_export.setitem(ll_riga,"dim_y",ld_y)
	
	dw_export.setitem(ll_riga,"um_mag",ls_um_mag)
	
	dw_export.setitem(ll_riga,"quantita",ld_quantita)
	
	dw_export.setitem(ll_riga,"um_ven",ls_um_ven)
	
	dw_export.setitem(ll_riga,"quan_ven",ld_quan_ven)
	
	dw_export.setitem(ll_riga,"fatturato",ld_fatturato)
	
next	

dw_export.saveas("",excel!,true)
end event

type cb_chiudi from commandbutton within w_stat_esplosione_carat
integer x = 4229
integer y = 2740
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;close(parent)
end event

type cb_stampa from commandbutton within w_stat_esplosione_carat
integer x = 3817
integer y = 2740
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;if g_mb.messagebox("APICE","Stampare il report visualizzato?",question!,yesno!,2) = 1 then
	dw_esplosione_carat.print()
end if
end event

type dw_esplosione_carat from uo_cs_xx_dw within w_stat_esplosione_carat
integer x = 23
integer y = 20
integer width = 4571
integer height = 2692
integer taborder = 10
string dataobject = "d_prod_statistiche_esplodi_report"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

