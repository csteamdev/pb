﻿$PBExportHeader$w_stat_esplosione_tes.srw
forward
global type w_stat_esplosione_tes from w_cs_xx_risposta
end type
type dw_nc_detraz from datawindow within w_stat_esplosione_tes
end type
type dw_detraz_fatture from datawindow within w_stat_esplosione_tes
end type
type rb_2 from radiobutton within w_stat_esplosione_tes
end type
type rb_1 from radiobutton within w_stat_esplosione_tes
end type
type dw_note from datawindow within w_stat_esplosione_tes
end type
type cb_salva from commandbutton within w_stat_esplosione_tes
end type
type cb_chiudi from commandbutton within w_stat_esplosione_tes
end type
type cb_stampa from commandbutton within w_stat_esplosione_tes
end type
type dw_esplosione_tes from uo_cs_xx_dw within w_stat_esplosione_tes
end type
end forward

global type w_stat_esplosione_tes from w_cs_xx_risposta
integer x = 0
integer y = 0
integer width = 5019
integer height = 2940
string title = "Esplosione testata"
dw_nc_detraz dw_nc_detraz
dw_detraz_fatture dw_detraz_fatture
rb_2 rb_2
rb_1 rb_1
dw_note dw_note
cb_salva cb_salva
cb_chiudi cb_chiudi
cb_stampa cb_stampa
dw_esplosione_tes dw_esplosione_tes
end type
global w_stat_esplosione_tes w_stat_esplosione_tes

type variables
boolean ib_quantita
str_statistiche_vendita istr_statistiche_vendita
end variables

forward prototypes
public function integer wf_report ()
end prototypes

public function integer wf_report ();long    ll_i, ll_j, ll_riga, ll_ret

string  ls_cod_tipo_fat_ven, ls_cliente, ls_tipo_cliente, ls_provincia, ls_agente, ls_zona ,ls_cod_prodotto, &
		  ls_tipo_fat_test, ls_cliente_test, ls_tipo_cliente_test, ls_provincia_test, ls_agente_test, &
		  ls_zona_test, ls_prodotto_test, ls_des_fattura, ls_des_provincia, ls_des_agente, ls_des_zona, &
		  ls_des_giro, ls_des_cliente, ls_cod_categoria, ls_categoria_test, ls_mag, ls_ven, ls_mag_test, ls_ven_test, &
		  ls_deposito_test, ls_cod_deposito, ls_tipo_anagrafica_test, ls_cod_tipo_anagrafica

dec{4}  ld_quantita, ld_fatturato, ld_quan_neg, ld_fatt_neg, ld_quan_ven

boolean lb_tipo_fat, lb_cliente, lb_tipo_cliente, lb_provincia, lb_agente, lb_zona, lb_prodotto, lb_categoria, lb_tipo_anagrafica, lb_deposito


setpointer(hourglass!)

if pos(s_cs_xx.parametri.parametro_s_1,"cod_prodotto",1) > 0 then
	
	lb_prodotto = true
	dw_esplosione_tes.object.cod_prodotto.alignment = 2
	dw_esplosione_tes.object.cod_prodotto_t.text = "Prodotto"
	dw_esplosione_tes.object.des_prodotto.alignment = 0
	dw_esplosione_tes.object.des_prodotto_t.text = "Descrizione"
	dw_esplosione_tes.object.des_livello_1.alignment = 0
	dw_esplosione_tes.object.des_livello_1_t.text = "Livello 1"
	dw_esplosione_tes.object.des_livello_2.alignment = 0
	dw_esplosione_tes.object.des_livello_2_t.text = "Livello 2"
	dw_esplosione_tes.object.des_livello_3.alignment = 0
	dw_esplosione_tes.object.des_livello_3_t.text = "Livello 3"
	dw_esplosione_tes.object.des_livello_4.alignment = 0
	dw_esplosione_tes.object.des_livello_4_t.text = "Livello 4"
//	dw_esplosione_tes.object.view_dati_prodotti_cod_prodotto.alignment = 2
//	dw_esplosione_tes.object.view_dati_prodotti_cod_prodotto_t.text = "Prodotto"
//	dw_esplosione_tes.object.view_dati_prodotti_des_prodotto.alignment = 0
//	dw_esplosione_tes.object.view_dati_prodotti_des_prodotto_t.text = "Descrizione"
//	dw_esplosione_tes.object.view_livelli_prodotti_des_livello_1.alignment = 0
//	dw_esplosione_tes.object.view_livelli_prodotti_des_livello_1_t.text = "Livello 1"
//	dw_esplosione_tes.object.view_livelli_prodotti_des_livello_2.alignment = 0
//	dw_esplosione_tes.object.view_livelli_prodotti_des_livello_2_t.text = "Livello 2"
//	dw_esplosione_tes.object.view_livelli_prodotti_des_livello_3.alignment = 0
//	dw_esplosione_tes.object.view_livelli_prodotti_des_livello_3_t.text = "Livello 3"
//	dw_esplosione_tes.object.view_livelli_prodotti_des_livello_4.alignment = 0
//	dw_esplosione_tes.object.view_livelli_prodotti_des_livello_4_t.text = "Livello 4"
//	
	
//	dw_esplosione_tes.saveas( "c:\dw_exp.xls",Excel!	, true)
	
	if pos(istr_statistiche_vendita.sql_origine,"cod_tipo_fat_ven",1) > 0 then
		lb_tipo_fat = true
		dw_esplosione_tes.object.cod_tipo_fat_ven.alignment = 2
		dw_esplosione_tes.object.cod_tipo_fat_ven_t.text = "Tipo fattura"
	else
		lb_tipo_fat = false
	end if

	if pos(s_cs_xx.parametri.parametro_s_1,"cod_cliente",1) > 0 then
		lb_cliente = true
		dw_esplosione_tes.object.cod_cliente.alignment = 2
		dw_esplosione_tes.object.cod_cliente_t.text = "Cliente"
		dw_esplosione_tes.object.cliente_rag_soc.alignment = 0
		dw_esplosione_tes.object.cliente_rag_soc_t.text = "Ragione sociale"
	else
		lb_cliente = false
	end if

	if pos(s_cs_xx.parametri.parametro_s_1,"flag_tipo_cliente",1) > 0 then
		lb_tipo_cliente = true
		dw_esplosione_tes.object.flag_tipo_cliente.alignment = 2
		dw_esplosione_tes.object.flag_tipo_cliente_t.text = "Tipo cliente"
	else
		lb_tipo_cliente = false
	end if

	if pos(s_cs_xx.parametri.parametro_s_1,"provincia",1) > 0 then
		lb_provincia = true
		dw_esplosione_tes.object.provincia.alignment = 2
		dw_esplosione_tes.object.provincia_t.text = "Provincia"
	else
		lb_provincia = false
	end if

	if pos(s_cs_xx.parametri.parametro_s_1,"cod_agente_1",1) > 0 then
		lb_agente = true
		dw_esplosione_tes.object.cod_agente_1.alignment = 2
		dw_esplosione_tes.object.cod_agente_1_t.text = "Agente"
	else
		lb_agente = false
	end if

	if pos(s_cs_xx.parametri.parametro_s_1,"cod_zona",1) > 0 then
		lb_zona = true
		dw_esplosione_tes.object.cod_zona.alignment = 2
		dw_esplosione_tes.object.cod_zona_t.text = "Zona"
	else
		lb_zona = false
	end if
	
	if pos(s_cs_xx.parametri.parametro_s_1,"cod_categoria",1) > 0 then
		lb_categoria = true
		dw_esplosione_tes.object.cod_categoria.alignment = 2
		dw_esplosione_tes.object.cod_categoria_t.text = "Categoria cliente"
		dw_esplosione_tes.object.des_categoria.alignment = 0
		dw_esplosione_tes.object.des_categoria_t.text = "Descrizione categoria"
	else
		lb_categoria = false
	end if

	if pos(s_cs_xx.parametri.parametro_s_1,"cod_deposito",1) > 0 then
		lb_deposito = true
		dw_esplosione_tes.object.cod_deposito.alignment = 2
		dw_esplosione_tes.object.cod_deposito_t.text = "Deposito"
	else
		lb_deposito = false
	end if
	
	if pos(s_cs_xx.parametri.parametro_s_1,"cod_tipo_anagrafica",1) > 0 then
		lb_tipo_anagrafica = true
		dw_esplosione_tes.object.cod_tipo_anagrafica.alignment = 2
		dw_esplosione_tes.object.cod_tipo_anagrafica_t.text = "Tipo Anagr."
	else
		lb_tipo_anagrafica = false
	end if
	

	if ib_quantita then
		dw_esplosione_tes.object.quantita.alignment = 1
	end if	
	
	dw_esplosione_tes.object.fatturato.alignment = 1

	for ll_i = 1 to dw_esplosione_tes.rowcount()
		
		if lb_tipo_fat then
			ls_cod_tipo_fat_ven = dw_esplosione_tes.getitemstring(ll_i,"cod_tipo_fat_ven")
			if isnull(ls_cod_tipo_fat_ven) then ls_cod_tipo_fat_ven = ""
			
			select des_tipo_fat_ven
			into   :ls_des_fattura
			from   tab_tipi_fat_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
		
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore nella select di tab_tipi_fat_ven: " + sqlca.sqlerrtext)
				return -1
			end if
		
			dw_esplosione_tes.setitem(ll_i,"cod_tipo_fat_ven",ls_des_fattura)
		
		end if
	
		if lb_cliente then
			ls_cliente = dw_esplosione_tes.getitemstring(ll_i,"cod_cliente")
			if isnull(ls_cliente) then ls_cliente = ""
		end if
		
		if lb_tipo_cliente then
			ls_tipo_cliente = dw_esplosione_tes.getitemstring(ll_i,"flag_tipo_cliente")
			if isnull(ls_tipo_cliente) then ls_tipo_cliente = ""
			
			choose case ls_tipo_cliente
				case "C"
					ls_des_cliente = "C.E.E."
				case "E"
					ls_des_cliente = "Estero"
				case "F"
					ls_des_cliente = "Persona Fisica"
				case "P"
					ls_des_cliente = "Privato"
				case "S"
					ls_des_cliente = "Società"
			end choose
		
			dw_esplosione_tes.setitem(ll_i,"flag_tipo_cliente",ls_des_cliente)
		
		end if
	
		if lb_provincia then
			ls_provincia = dw_esplosione_tes.getitemstring(ll_i,"provincia")
			if isnull(ls_provincia) then ls_provincia = ""
		end if
	
		if lb_agente then
			ls_agente = dw_esplosione_tes.getitemstring(ll_i,"cod_agente_1")
			if isnull(ls_agente) then ls_agente = ""
			
			select rag_soc_1
			into   :ls_des_agente
			from   anag_agenti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_agente = :ls_agente;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore nella select di anag_agenti: " + sqlca.sqlerrtext)
				return -1
			end if
			
			dw_esplosione_tes.setitem(ll_i,"cod_agente_1",ls_des_agente)
			
		end if
	
		if lb_zona then
			ls_zona = dw_esplosione_tes.getitemstring(ll_i,"cod_zona")
			if isnull(ls_zona) then ls_zona = ""
			
			select des_zona
			into   :ls_des_zona
			from   tab_zone
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_zona = :ls_zona;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore nella select di tab_zone: " + sqlca.sqlerrtext)
				return -1
			end if
			
			dw_esplosione_tes.setitem(ll_i,"cod_zona",ls_des_zona)
			
		end if
		
		if lb_categoria then
			ls_cod_categoria = dw_esplosione_tes.getitemstring(ll_i,"cod_categoria")
			if isnull(ls_cod_categoria) then ls_cod_categoria = ""
		end if
	
		if lb_deposito then
			ls_cod_deposito = dw_esplosione_tes.getitemstring(ll_i,"cod_deposito")
			if isnull(ls_cod_deposito) then ls_cod_deposito = ""
		end if
	
		if lb_tipo_anagrafica then
			ls_cod_tipo_anagrafica = dw_esplosione_tes.getitemstring(ll_i,"cod_tipo_anagrafica")
			if isnull(ls_cod_tipo_anagrafica) then ls_cod_tipo_anagrafica = ""
		end if
	
		if lb_prodotto then
			ls_cod_prodotto = dw_esplosione_tes.getitemstring(ll_i,"cod_prodotto")
			if isnull(ls_cod_prodotto) then ls_cod_prodotto = ""
			ls_mag = dw_esplosione_tes.getitemstring(ll_i,"cod_misura_mag")
			if isnull(ls_mag) then ls_mag = ""
			ls_ven = dw_esplosione_tes.getitemstring(ll_i,"cod_misura_ven")
			if isnull(ls_ven) then ls_ven = ""
		end if
	
		if ib_quantita then
			ld_quantita = dw_esplosione_tes.getitemnumber(ll_i,"quantita")
			ld_quan_ven = dw_esplosione_tes.getitemnumber(ll_i,"quan_ven")
		end if
	
		ld_fatturato = dw_esplosione_tes.getitemnumber(ll_i,"fatturato")


		// detraggo gli importi delle note di credito
		for ll_j = 1 to dw_note.rowcount()

			if lb_tipo_fat then
				ls_tipo_fat_test = dw_note.getitemstring(ll_j,"cod_tipo_fat_ven")
				if isnull(ls_tipo_fat_test) then ls_tipo_fat_test = ""
				if ls_tipo_fat_test <> ls_cod_tipo_fat_ven then	continue			
			end if
	
			if lb_cliente then
				ls_cliente_test = dw_note.getitemstring(ll_j,"cod_cliente")
				if isnull(ls_cliente_test) then ls_cliente_test = ""
				if ls_cliente_test <> ls_cliente then continue
			end if
	
			if lb_tipo_cliente then
				ls_tipo_cliente_test = dw_note.getitemstring(ll_j,"flag_tipo_cliente")
				if isnull(ls_tipo_cliente_test) then ls_tipo_cliente_test = ""
				if ls_tipo_cliente_test <> ls_tipo_cliente then continue
			end if
	
			if lb_provincia then
				ls_provincia_test = dw_note.getitemstring(ll_j,"provincia")
				if isnull(ls_provincia_test) then ls_provincia_test = ""
				if ls_provincia_test <> ls_provincia then continue
			end if

			if lb_agente then
				ls_agente_test = dw_note.getitemstring(ll_j,"cod_agente_1")
				if isnull(ls_agente_test) then ls_agente_test = ""
				if ls_agente_test <> ls_agente then continue
			end if

			if lb_zona then
				ls_zona_test = dw_note.getitemstring(ll_j,"cod_zona")
				if isnull(ls_zona_test) then ls_zona_test = ""
				if ls_zona_test <> ls_zona then continue
			end if
			
			if lb_categoria then
				ls_categoria_test = dw_note.getitemstring(ll_j,"cod_categoria")
				if isnull(ls_categoria_test) then ls_categoria_test = ""
				if ls_categoria_test <> ls_cod_categoria then continue
			end if

			if lb_deposito then
				ls_deposito_test = dw_note.getitemstring(ll_j,"cod_deposito")
				if isnull(ls_deposito_test) then ls_deposito_test = ""
				if ls_deposito_test <> ls_cod_deposito then continue
			end if

			if lb_tipo_anagrafica then
				ls_tipo_anagrafica_test = dw_note.getitemstring(ll_j,"cod_tipo_anagrafica")
				if isnull(ls_cod_tipo_anagrafica) then ls_tipo_anagrafica_test = ""
				if ls_cod_tipo_anagrafica <> ls_cod_tipo_anagrafica then continue
			end if

			if lb_prodotto then
				ls_prodotto_test = dw_note.getitemstring(ll_j,"cod_prodotto")
				if isnull(ls_prodotto_test) then ls_prodotto_test = ""
				if ls_prodotto_test <> ls_cod_prodotto then continue
				ls_mag_test = dw_note.getitemstring(ll_j,"cod_misura_mag")
				if isnull(ls_mag_test) then ls_mag_test = ""
				if ls_mag_test <> ls_mag then continue
				ls_ven_test = dw_note.getitemstring(ll_j,"cod_misura_ven")
				if isnull(ls_ven_test) then ls_ven_test = ""
				if ls_ven_test <> ls_ven then continue
			end if

			if ib_quantita then
				
				ld_quan_neg = (dw_note.getitemnumber(ll_j,"quantita") * 2)
				ld_quantita = ld_quantita - ld_quan_neg
				ld_quan_neg = (dw_note.getitemnumber(ll_j,"quan_ven") * 2)
				ld_quan_ven = ld_quan_ven - ld_quan_neg
				
				dw_esplosione_tes.setitem(ll_i,"quantita",ld_quantita)
				dw_esplosione_tes.setitem(ll_i,"quan_ven",ld_quan_ven)
			end if	
		
			ld_fatt_neg = (dw_note.getitemnumber(ll_j,"fatturato") * 2)
			ld_fatturato = ld_fatturato - ld_fatt_neg
	  		ld_fatturato = round(ld_fatturato,2)
   			dw_esplosione_tes.setitem(ll_i,"fatturato",ld_fatturato)
		
			exit

		next	
		
		// *****************
		

		// detraggo gli importi di eventuali righe di sconto messe in fattura
		for ll_j = 1 to dw_detraz_fatture.rowcount()

			if lb_tipo_fat then
				ls_tipo_fat_test = dw_detraz_fatture.getitemstring(ll_j,"cod_tipo_fat_ven")
				if isnull(ls_tipo_fat_test) then ls_tipo_fat_test = ""
				if ls_tipo_fat_test <> ls_cod_tipo_fat_ven then	continue			
			end if
	
			if lb_cliente then
				ls_cliente_test = dw_detraz_fatture.getitemstring(ll_j,"cod_cliente")
				if isnull(ls_cliente_test) then ls_cliente_test = ""
				if ls_cliente_test <> ls_cliente then continue
			end if
	
			if lb_tipo_cliente then
				ls_tipo_cliente_test = dw_detraz_fatture.getitemstring(ll_j,"flag_tipo_cliente")
				if isnull(ls_tipo_cliente_test) then ls_tipo_cliente_test = ""
				if ls_tipo_cliente_test <> ls_tipo_cliente then continue
			end if
	
			if lb_provincia then
				ls_provincia_test = dw_detraz_fatture.getitemstring(ll_j,"provincia")
				if isnull(ls_provincia_test) then ls_provincia_test = ""
				if ls_provincia_test <> ls_provincia then continue
			end if

			if lb_agente then
				ls_agente_test = dw_detraz_fatture.getitemstring(ll_j,"cod_agente_1")
				if isnull(ls_agente_test) then ls_agente_test = ""
				if ls_agente_test <> ls_agente then continue
			end if

			if lb_zona then
				ls_zona_test = dw_detraz_fatture.getitemstring(ll_j,"cod_zona")
				if isnull(ls_zona_test) then ls_zona_test = ""
				if ls_zona_test <> ls_zona then continue
			end if
			
			if lb_categoria then
				ls_categoria_test = dw_detraz_fatture.getitemstring(ll_j,"cod_categoria")
				if isnull(ls_categoria_test) then ls_categoria_test = ""
				if ls_categoria_test <> ls_cod_categoria then continue
			end if

			if lb_deposito then
				ls_deposito_test = dw_detraz_fatture.getitemstring(ll_j,"cod_deposito")
				if isnull(ls_deposito_test) then ls_deposito_test = ""
				if ls_deposito_test <> ls_cod_deposito then continue
			end if

			if lb_tipo_anagrafica then
				ls_tipo_anagrafica_test = dw_detraz_fatture.getitemstring(ll_j,"cod_tipo_anagrafica")
				if isnull(ls_cod_tipo_anagrafica) then ls_tipo_anagrafica_test = ""
				if ls_cod_tipo_anagrafica <> ls_cod_tipo_anagrafica then continue
			end if

			if lb_prodotto then
				ls_prodotto_test = dw_detraz_fatture.getitemstring(ll_j,"cod_prodotto")
				if isnull(ls_prodotto_test) then ls_prodotto_test = ""
				if ls_prodotto_test <> ls_cod_prodotto then continue
				ls_mag_test = dw_detraz_fatture.getitemstring(ll_j,"cod_misura_mag")
				if isnull(ls_mag_test) then ls_mag_test = ""
				if ls_mag_test <> ls_mag then continue
				ls_ven_test = dw_detraz_fatture.getitemstring(ll_j,"cod_misura_ven")
				if isnull(ls_ven_test) then ls_ven_test = ""
				if ls_ven_test <> ls_ven then continue
			end if

			if ib_quantita then
				
				ld_quan_neg = (dw_detraz_fatture.getitemnumber(ll_j,"quantita") * 2)
				ld_quantita = ld_quantita - ld_quan_neg
				ld_quan_neg = (dw_detraz_fatture.getitemnumber(ll_j,"quan_ven") * 2)
				ld_quan_ven = ld_quan_ven - ld_quan_neg
				
				dw_esplosione_tes.setitem(ll_i,"quantita",ld_quantita)
				dw_esplosione_tes.setitem(ll_i,"quan_ven",ld_quan_ven)
			end if	
		
			ld_fatt_neg = (dw_detraz_fatture.getitemnumber(ll_j,"fatturato") * 2)
			
			// EnMe 06-03-2012; eseguita correzione; l'importo arriva già negativo			
			//ld_fatturato = ld_fatturato - ld_fatt_neg
			//ld_fatturato = ld_fatturato + ld_fatt_neg
	  		//ld_fatturato = round(ld_fatturato,2)
   			//dw_esplosione_tes.setitem(ll_i,"fatturato",ld_fatturato)
		
			//exit

		next	
		
		// *****************
		
		// AGGIUNGO gli importi di eventuali righe di sconto messe in nota di credito
		for ll_j = 1 to dw_nc_detraz.rowcount()

			if lb_tipo_fat then
				ls_tipo_fat_test = dw_nc_detraz.getitemstring(ll_j,"cod_tipo_fat_ven")
				if isnull(ls_tipo_fat_test) then ls_tipo_fat_test = ""
				if ls_tipo_fat_test <> ls_cod_tipo_fat_ven then	continue			
			end if
	
			if lb_cliente then
				ls_cliente_test = dw_nc_detraz.getitemstring(ll_j,"cod_cliente")
				if isnull(ls_cliente_test) then ls_cliente_test = ""
				if ls_cliente_test <> ls_cliente then continue
			end if
	
			if lb_tipo_cliente then
				ls_tipo_cliente_test = dw_nc_detraz.getitemstring(ll_j,"flag_tipo_cliente")
				if isnull(ls_tipo_cliente_test) then ls_tipo_cliente_test = ""
				if ls_tipo_cliente_test <> ls_tipo_cliente then continue
			end if
	
			if lb_provincia then
				ls_provincia_test = dw_nc_detraz.getitemstring(ll_j,"provincia")
				if isnull(ls_provincia_test) then ls_provincia_test = ""
				if ls_provincia_test <> ls_provincia then continue
			end if

			if lb_agente then
				ls_agente_test = dw_nc_detraz.getitemstring(ll_j,"cod_agente_1")
				if isnull(ls_agente_test) then ls_agente_test = ""
				if ls_agente_test <> ls_agente then continue
			end if

			if lb_zona then
				ls_zona_test = dw_nc_detraz.getitemstring(ll_j,"cod_zona")
				if isnull(ls_zona_test) then ls_zona_test = ""
				if ls_zona_test <> ls_zona then continue
			end if
			
			if lb_categoria then
				ls_categoria_test = dw_nc_detraz.getitemstring(ll_j,"cod_categoria")
				if isnull(ls_categoria_test) then ls_categoria_test = ""
				if ls_categoria_test <> ls_cod_categoria then continue
			end if

			if lb_deposito then
				ls_deposito_test = dw_nc_detraz.getitemstring(ll_j,"cod_deposito")
				if isnull(ls_deposito_test) then ls_deposito_test = ""
				if ls_deposito_test <> ls_cod_deposito then continue
			end if

			if lb_tipo_anagrafica then
				ls_tipo_anagrafica_test = dw_nc_detraz.getitemstring(ll_j,"cod_tipo_anagrafica")
				if isnull(ls_cod_tipo_anagrafica) then ls_tipo_anagrafica_test = ""
				if ls_cod_tipo_anagrafica <> ls_cod_tipo_anagrafica then continue
			end if

			if lb_prodotto then
				ls_prodotto_test = dw_nc_detraz.getitemstring(ll_j,"cod_prodotto")
				if isnull(ls_prodotto_test) then ls_prodotto_test = ""
				if ls_prodotto_test <> ls_cod_prodotto then continue
				ls_mag_test = dw_nc_detraz.getitemstring(ll_j,"cod_misura_mag")
				if isnull(ls_mag_test) then ls_mag_test = ""
				if ls_mag_test <> ls_mag then continue
				ls_ven_test = dw_nc_detraz.getitemstring(ll_j,"cod_misura_ven")
				if isnull(ls_ven_test) then ls_ven_test = ""
				if ls_ven_test <> ls_ven then continue
			end if

			if ib_quantita then
				
				ld_quan_neg = (dw_nc_detraz.getitemnumber(ll_j,"quantita") * 2)
				ld_quantita = ld_quantita - ld_quan_neg
				ld_quan_neg = (dw_nc_detraz.getitemnumber(ll_j,"quan_ven") * 2)
				ld_quan_ven = ld_quan_ven - ld_quan_neg
				
				dw_esplosione_tes.setitem(ll_i,"quantita",ld_quantita)
				dw_esplosione_tes.setitem(ll_i,"quan_ven",ld_quan_ven)
			end if	
		
			ld_fatt_neg = (dw_nc_detraz.getitemnumber(ll_j,"fatturato") * 2)
			ld_fatturato = ld_fatturato - ld_fatt_neg
	  		ld_fatturato = round(ld_fatturato,2)
   			dw_esplosione_tes.setitem(ll_i,"fatturato",ld_fatturato)
		
			exit

		next	
		
		// *****************

		
		
		
		

		ls_mag_test = dw_esplosione_tes.getitemstring(ll_i,"cod_misura_mag")
				
		ls_ven_test = dw_esplosione_tes.getitemstring(ll_i,"cod_misura_ven")
		
		if isnull(ls_mag_test) then ls_mag_test = ""
		
		if isnull(ls_ven_test) then ls_ven_test = ""
		
		if ls_mag_test = ls_ven_test and ld_quantita <> ld_quan_ven and ld_quan_ven = 0 then
			
			ld_quan_ven = ld_quantita
			
			dw_esplosione_tes.setitem(ll_i,"quan_ven",ld_quan_ven)
			
		end if
						

	next
	
else
	
	lb_prodotto = false
	
	if pos(istr_statistiche_vendita.sql_origine,"cod_tipo_fat_ven",1) > 0 then
//	if pos(s_cs_xx.parametri.parametro_s_1,"cod_tipo_fat_ven",1) > 0 then
		lb_tipo_fat = true
		dw_esplosione_tes.object.cod_tipo_fat_ven.alignment = 2
		dw_esplosione_tes.object.cod_tipo_fat_ven_t.text = "Tipo fattura"
	else
		lb_tipo_fat = false
	end if

	if pos(s_cs_xx.parametri.parametro_s_1,"cod_cliente",1) > 0 then
		lb_cliente = true
		dw_esplosione_tes.object.cod_cliente.alignment = 2
		dw_esplosione_tes.object.cod_cliente_t.text = "Cliente"
		dw_esplosione_tes.object.cliente_rag_soc.alignment = 0
		dw_esplosione_tes.object.cliente_rag_soc_t.text = "Ragione sociale"
	else
		lb_cliente = false
	end if

	if pos(s_cs_xx.parametri.parametro_s_1,"flag_tipo_cliente",1) > 0 then
		lb_tipo_cliente = true
		dw_esplosione_tes.object.flag_tipo_cliente.alignment = 2
		dw_esplosione_tes.object.flag_tipo_cliente_t.text = "Tipo cliente"
	else
		lb_tipo_cliente = false
	end if

	if pos(s_cs_xx.parametri.parametro_s_1,"provincia",1) > 0 then
		lb_provincia = true
		dw_esplosione_tes.object.provincia.alignment = 2
		dw_esplosione_tes.object.provincia_t.text = "Provincia"
	else
		lb_provincia = false
	end if

	if pos(s_cs_xx.parametri.parametro_s_1,"cod_agente_1",1) > 0 then
		lb_agente = true
		dw_esplosione_tes.object.cod_agente_1.alignment = 2
		dw_esplosione_tes.object.cod_agente_1_t.text = "Agente"
	else
		lb_agente = false
	end if

	if pos(s_cs_xx.parametri.parametro_s_1,"cod_zona",1) > 0 then
		lb_zona = true
		dw_esplosione_tes.object.cod_zona.alignment = 2
		dw_esplosione_tes.object.cod_zona_t.text = "Zona"
	else
		lb_zona = false
	end if
	
	if pos(s_cs_xx.parametri.parametro_s_1,"cod_categoria",1) > 0 then
		lb_categoria = true
		dw_esplosione_tes.object.cod_categoria.alignment = 2
		dw_esplosione_tes.object.cod_categoria_t.text = "Categoria cliente"
		dw_esplosione_tes.object.des_categoria.alignment = 0
		dw_esplosione_tes.object.des_categoria_t.text = "Descrizione categoria"
	else
		lb_categoria = false
	end if

	if pos(s_cs_xx.parametri.parametro_s_1,"cod_deposito",1) > 0 then
		lb_deposito = true
		dw_esplosione_tes.object.cod_deposito.alignment = 2
		dw_esplosione_tes.object.cod_deposito_t.text = "Deposito"
	else
		lb_deposito = false
	end if
	
	if pos(s_cs_xx.parametri.parametro_s_1,"cod_tipo_anagrafica",1) > 0 then
		lb_tipo_anagrafica = true
		dw_esplosione_tes.object.cod_tipo_anagrafica.alignment = 2
		dw_esplosione_tes.object.cod_tipo_anagrafica_t.text = "Tipo Anagr."
	else
		lb_tipo_anagrafica = false
	end if
	
	if ib_quantita then
		dw_esplosione_tes.object.quantita.alignment = 1
	end if	
	
	dw_esplosione_tes.object.fatturato.alignment = 1

	for ll_i = 1 to dw_esplosione_tes.rowcount()
		
		if lb_tipo_fat then
			ll_ret = dw_esplosione_tes.setcolumn("cod_tipo_fat_ven")
				if ll_ret > 0 then
				ls_cod_tipo_fat_ven = dw_esplosione_tes.getitemstring(ll_i,"cod_tipo_fat_ven")
				if isnull(ls_cod_tipo_fat_ven) then ls_cod_tipo_fat_ven = ""
				
				select des_tipo_fat_ven
				into   :ls_des_fattura
				from   tab_tipi_fat_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
			
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("APICE","Errore nella select di tab_tipi_fat_ven: " + sqlca.sqlerrtext)
					return -1
				end if
			
				dw_esplosione_tes.setitem(ll_i,"cod_tipo_fat_ven",ls_des_fattura)
			end if
		end if
	
		if lb_cliente then
			ls_cliente = dw_esplosione_tes.getitemstring(ll_i,"cod_cliente")
			if isnull(ls_cliente) then ls_cliente = ""
		end if
		
		if lb_tipo_cliente then
			ls_tipo_cliente = dw_esplosione_tes.getitemstring(ll_i,"flag_tipo_cliente")
			if isnull(ls_tipo_cliente) then ls_tipo_cliente = ""
			
			choose case ls_tipo_cliente
				case "C"
					ls_des_cliente = "C.E.E."
				case "E"
					ls_des_cliente = "Estero"
				case "F"
					ls_des_cliente = "Persona Fisica"
				case "P"
					ls_des_cliente = "Privato"
				case "S"
					ls_des_cliente = "Società"
			end choose
		
			dw_esplosione_tes.setitem(ll_i,"flag_tipo_cliente",ls_des_cliente)
		
		end if
	
		if lb_provincia then
			ls_provincia = dw_esplosione_tes.getitemstring(ll_i,"provincia")
			if isnull(ls_provincia) then ls_provincia = ""
		end if
	
		if lb_agente then
			ls_agente = dw_esplosione_tes.getitemstring(ll_i,"cod_agente_1")
			if isnull(ls_agente) then ls_agente = ""
			
			select rag_soc_1
			into   :ls_des_agente
			from   anag_agenti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_agente = :ls_agente;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore nella select di anag_agenti: " + sqlca.sqlerrtext)
				return -1
			end if
			
			dw_esplosione_tes.setitem(ll_i,"cod_agente_1",ls_des_agente)
			
		end if
	
		if lb_zona then
			ls_zona = dw_esplosione_tes.getitemstring(ll_i,"cod_zona")
			if isnull(ls_zona) then ls_zona = ""
			
			select des_zona
			into   :ls_des_zona
			from   tab_zone
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_zona = :ls_zona;
			
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore nella select di tab_zone: " + sqlca.sqlerrtext)
				return -1
			end if
			
			dw_esplosione_tes.setitem(ll_i,"cod_zona",ls_des_zona)
			
		end if
		
		if lb_categoria then
			ls_cod_categoria = dw_esplosione_tes.getitemstring(ll_i,"cod_categoria")
			if isnull(ls_cod_categoria) then ls_cod_categoria = ""
		end if
	
		if lb_deposito then
			ls_cod_deposito = dw_esplosione_tes.getitemstring(ll_i,"cod_deposito")
			if isnull(ls_cod_deposito) then ls_cod_deposito = ""
		end if
	
		if lb_tipo_anagrafica then
			ls_cod_tipo_anagrafica = dw_esplosione_tes.getitemstring(ll_i,"cod_tipo_anagrafica")
			if isnull(ls_cod_tipo_anagrafica) then ls_cod_tipo_anagrafica = ""
		end if
	
		if lb_prodotto then
			ls_cod_prodotto = dw_esplosione_tes.getitemstring(ll_i,"cod_prodotto")
			if isnull(ls_cod_prodotto) then ls_cod_prodotto = ""
		end if
	
		if ib_quantita then
			ld_quantita = dw_esplosione_tes.getitemnumber(ll_i,"quantita")
		end if
	
		ld_fatturato = dw_esplosione_tes.getitemnumber(ll_i,"fatturato")
		
		//*********************

		for ll_j = 1 to dw_note.rowcount()

			if lb_tipo_fat then
				ls_tipo_fat_test = dw_note.getitemstring(ll_j,"cod_tipo_fat_ven")
				if isnull(ls_tipo_fat_test) then ls_tipo_fat_test = ""
				if ls_tipo_fat_test <> ls_cod_tipo_fat_ven then	continue			
			end if
	
			if lb_cliente then
				ls_cliente_test = dw_note.getitemstring(ll_j,"cod_cliente")
				if isnull(ls_cliente_test) then ls_cliente_test = ""
				if ls_cliente_test <> ls_cliente then continue
			end if
	
			if lb_tipo_cliente then
				ls_tipo_cliente_test = dw_note.getitemstring(ll_j,"flag_tipo_cliente")
				if isnull(ls_tipo_cliente_test) then ls_tipo_cliente_test = ""
				if ls_tipo_cliente_test <> ls_tipo_cliente then continue
			end if
	
			if lb_provincia then
				ls_provincia_test = dw_note.getitemstring(ll_j,"provincia")
				if isnull(ls_provincia_test) then ls_provincia_test = ""
				if ls_provincia_test <> ls_provincia then continue
			end if

			if lb_agente then
				ls_agente_test = dw_note.getitemstring(ll_j,"cod_agente_1")
				if isnull(ls_agente_test) then ls_agente_test = ""
				if ls_agente_test <> ls_agente then continue
			end if

			if lb_zona then
				ls_zona_test = dw_note.getitemstring(ll_j,"cod_zona")
				if isnull(ls_zona_test) then ls_zona_test = ""
				if ls_zona_test <> ls_zona then continue
			end if
			
			if lb_categoria then
				ls_categoria_test = dw_note.getitemstring(ll_j,"cod_categoria")
				if isnull(ls_categoria_test) then ls_categoria_test = ""
				if ls_categoria_test <> ls_cod_categoria then continue
			end if

			if lb_tipo_anagrafica then
				ls_tipo_anagrafica_test = dw_note.getitemstring(ll_j,"cod_tipo_anagrafica")
				if isnull(ls_tipo_anagrafica_test) then ls_tipo_anagrafica_test = ""
				if ls_tipo_anagrafica_test <> ls_cod_tipo_anagrafica then continue
			end if

			if lb_deposito then
				ls_deposito_test = dw_note.getitemstring(ll_j,"cod_deposito")
				if isnull(ls_deposito_test) then ls_deposito_test = ""
				if ls_deposito_test <> ls_cod_deposito then continue
			end if

			if lb_prodotto then
				ls_prodotto_test = dw_note.getitemstring(ll_j,"cod_prodotto")
				if isnull(ls_prodotto_test) then ls_prodotto_test = ""
				if ls_prodotto_test <> ls_cod_prodotto then continue
			end if

			if ib_quantita then
				ld_quan_neg = (dw_note.getitemnumber(ll_j,"quantita") * 2)
				ld_quantita = ld_quantita - ld_quan_neg
				dw_esplosione_tes.setitem(ll_i,"quantita",ld_quantita)
			end if	
		
			ld_fatt_neg = (dw_note.getitemnumber(ll_j,"fatturato") * 2)
			ld_fatturato = ld_fatturato - ld_fatt_neg
	  		ld_fatturato = round(ld_fatturato,2)
   			dw_esplosione_tes.setitem(ll_i,"fatturato",ld_fatturato)
		
			exit

		next	
		
		// *****************
		

		// detraggo gli importi di eventuali righe di sconto messe in fattura
		for ll_j = 1 to dw_detraz_fatture.rowcount()

			if lb_tipo_fat then
				ls_tipo_fat_test = dw_detraz_fatture.getitemstring(ll_j,"cod_tipo_fat_ven")
				if isnull(ls_tipo_fat_test) then ls_tipo_fat_test = ""
				if ls_tipo_fat_test <> ls_cod_tipo_fat_ven then	continue			
			end if
	
			if lb_cliente then
				ls_cliente_test = dw_detraz_fatture.getitemstring(ll_j,"cod_cliente")
				if isnull(ls_cliente_test) then ls_cliente_test = ""
				if ls_cliente_test <> ls_cliente then continue
			end if
	
			if lb_tipo_cliente then
				ls_tipo_cliente_test = dw_detraz_fatture.getitemstring(ll_j,"flag_tipo_cliente")
				if isnull(ls_tipo_cliente_test) then ls_tipo_cliente_test = ""
				if ls_tipo_cliente_test <> ls_tipo_cliente then continue
			end if
	
			if lb_provincia then
				ls_provincia_test = dw_detraz_fatture.getitemstring(ll_j,"provincia")
				if isnull(ls_provincia_test) then ls_provincia_test = ""
				if ls_provincia_test <> ls_provincia then continue
			end if

			if lb_agente then
				ls_agente_test = dw_detraz_fatture.getitemstring(ll_j,"cod_agente_1")
				if isnull(ls_agente_test) then ls_agente_test = ""
				if ls_agente_test <> ls_agente then continue
			end if

			if lb_zona then
				ls_zona_test = dw_detraz_fatture.getitemstring(ll_j,"cod_zona")
				if isnull(ls_zona_test) then ls_zona_test = ""
				if ls_zona_test <> ls_zona then continue
			end if
			
			if lb_categoria then
				ls_categoria_test = dw_detraz_fatture.getitemstring(ll_j,"cod_categoria")
				if isnull(ls_categoria_test) then ls_categoria_test = ""
				if ls_categoria_test <> ls_cod_categoria then continue
			end if

			if lb_tipo_anagrafica then
				ls_tipo_anagrafica_test = dw_detraz_fatture.getitemstring(ll_j,"cod_tipo_anagrafica")
				if isnull(ls_tipo_anagrafica_test) then ls_tipo_anagrafica_test = ""
				if ls_tipo_anagrafica_test <> ls_cod_tipo_anagrafica then continue
			end if

			if lb_deposito then
				ls_deposito_test = dw_detraz_fatture.getitemstring(ll_j,"cod_deposito")
				if isnull(ls_deposito_test) then ls_deposito_test = ""
				if ls_deposito_test <> ls_cod_deposito then continue
			end if

			if lb_prodotto then
				ls_prodotto_test = dw_detraz_fatture.getitemstring(ll_j,"cod_prodotto")
				if isnull(ls_prodotto_test) then ls_prodotto_test = ""
				if ls_prodotto_test <> ls_cod_prodotto then continue
			end if

			if ib_quantita then
				ld_quan_neg = (dw_detraz_fatture.getitemnumber(ll_j,"quantita") * 2)
				ld_quantita = ld_quantita - ld_quan_neg
				dw_esplosione_tes.setitem(ll_i,"quantita",ld_quantita)
			end if	
		
			ld_fatt_neg = (dw_detraz_fatture.getitemnumber(ll_j,"fatturato") * 2)

			//EnMe 06-3-2012; eseguita correzione; l'importo arriva già negativo
			//ld_fatturato = ld_fatturato - ld_fatt_neg
			//ld_fatturato = ld_fatturato + ld_fatt_neg
	  		//ld_fatturato = round(ld_fatturato,2)
   			//dw_esplosione_tes.setitem(ll_i,"fatturato",ld_fatturato)
		
			exit

		next	
		
		// *****************
		
//		// AGGIUNGO gli importi di eventuali righe di sconto messe in nota di credito
//		for ll_j = 1 to dw_nc_detraz.rowcount()
//
//
//			if lb_tipo_fat then
//				ls_tipo_fat_test = dw_note.getitemstring(ll_j,"cod_tipo_fat_ven")
//				if isnull(ls_tipo_fat_test) then ls_tipo_fat_test = ""
//				if ls_tipo_fat_test <> ls_cod_tipo_fat_ven then	continue			
//			end if
//	
//			if lb_cliente then
//				ls_cliente_test = dw_note.getitemstring(ll_j,"cod_cliente")
//				if isnull(ls_cliente_test) then ls_cliente_test = ""
//				if ls_cliente_test <> ls_cliente then continue
//			end if
//	
//			if lb_tipo_cliente then
//				ls_tipo_cliente_test = dw_note.getitemstring(ll_j,"flag_tipo_cliente")
//				if isnull(ls_tipo_cliente_test) then ls_tipo_cliente_test = ""
//				if ls_tipo_cliente_test <> ls_tipo_cliente then continue
//			end if
//	
//			if lb_provincia then
//				ls_provincia_test = dw_note.getitemstring(ll_j,"provincia")
//				if isnull(ls_provincia_test) then ls_provincia_test = ""
//				if ls_provincia_test <> ls_provincia then continue
//			end if
//
//			if lb_agente then
//				ls_agente_test = dw_note.getitemstring(ll_j,"cod_agente_1")
//				if isnull(ls_agente_test) then ls_agente_test = ""
//				if ls_agente_test <> ls_agente then continue
//			end if
//
//			if lb_zona then
//				ls_zona_test = dw_note.getitemstring(ll_j,"cod_zona")
//				if isnull(ls_zona_test) then ls_zona_test = ""
//				if ls_zona_test <> ls_zona then continue
//			end if
//			
//			if lb_categoria then
//				ls_categoria_test = dw_note.getitemstring(ll_j,"cod_categoria")
//				if isnull(ls_categoria_test) then ls_categoria_test = ""
//				if ls_categoria_test <> ls_cod_categoria then continue
//			end if
//
//			if lb_tipo_anagrafica then
//				ls_tipo_anagrafica_test = dw_note.getitemstring(ll_j,"cod_tipo_anagrafica")
//				if isnull(ls_tipo_anagrafica_test) then ls_tipo_anagrafica_test = ""
//				if ls_tipo_anagrafica_test <> ls_cod_tipo_anagrafica then continue
//			end if
//
//			if lb_deposito then
//				ls_deposito_test = dw_note.getitemstring(ll_j,"cod_deposito")
//				if isnull(ls_deposito_test) then ls_deposito_test = ""
//				if ls_deposito_test <> ls_cod_deposito then continue
//			end if
//
//			if lb_prodotto then
//				ls_prodotto_test = dw_note.getitemstring(ll_j,"cod_prodotto")
//				if isnull(ls_prodotto_test) then ls_prodotto_test = ""
//				if ls_prodotto_test <> ls_cod_prodotto then continue
//			end if
//
//			if ib_quantita then
//				ld_quan_neg = (dw_note.getitemnumber(ll_j,"quantita") * 2)
//				ld_quantita = ld_quantita - ld_quan_neg
//				dw_esplosione_tes.setitem(ll_i,"quantita",ld_quantita)
//			end if	
//		
//		
//			ld_fatt_neg = (dw_nc_detraz.getitemnumber(ll_j,"fatturato") * 2)
//			ld_fatturato = ld_fatturato - ld_fatt_neg
//	  		ld_fatturato = round(ld_fatturato,2)
//   			dw_esplosione_tes.setitem(ll_i,"fatturato",ld_fatturato)
//		
//			exit
//
//		next	
//		
		// *****************

		
		

	next
	
end if

setpointer(arrow!)

return 0
end function

on w_stat_esplosione_tes.create
int iCurrent
call super::create
this.dw_nc_detraz=create dw_nc_detraz
this.dw_detraz_fatture=create dw_detraz_fatture
this.rb_2=create rb_2
this.rb_1=create rb_1
this.dw_note=create dw_note
this.cb_salva=create cb_salva
this.cb_chiudi=create cb_chiudi
this.cb_stampa=create cb_stampa
this.dw_esplosione_tes=create dw_esplosione_tes
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_nc_detraz
this.Control[iCurrent+2]=this.dw_detraz_fatture
this.Control[iCurrent+3]=this.rb_2
this.Control[iCurrent+4]=this.rb_1
this.Control[iCurrent+5]=this.dw_note
this.Control[iCurrent+6]=this.cb_salva
this.Control[iCurrent+7]=this.cb_chiudi
this.Control[iCurrent+8]=this.cb_stampa
this.Control[iCurrent+9]=this.dw_esplosione_tes
end on

on w_stat_esplosione_tes.destroy
call super::destroy
destroy(this.dw_nc_detraz)
destroy(this.dw_detraz_fatture)
destroy(this.rb_2)
destroy(this.rb_1)
destroy(this.dw_note)
destroy(this.cb_salva)
destroy(this.cb_chiudi)
destroy(this.cb_stampa)
destroy(this.dw_esplosione_tes)
end on

event pc_setwindow;call super::pc_setwindow;string ls_syntax, ls_error
long   ll_pos

istr_statistiche_vendita = message.powerobjectparm

dw_esplosione_tes.ib_dw_report = true

//if isnull(s_cs_xx.parametri.parametro_s_1) then
if isnull(istr_statistiche_vendita.sql_fatture) then
	cb_salva.enabled = false
	cb_stampa.enabled = false
	rb_1.enabled = false
	rb_2.enabled = false
	return -1
end if	

//if pos(s_cs_xx.parametri.parametro_s_1,"quantita",1) > 0 then
if pos(istr_statistiche_vendita.sql_fatture ,"quantita",1) > 0 then
	ib_quantita = true
else	
	ib_quantita = false
end if	

// creo la DW per la  gestione degli importi delle detrazioni fatture per righe di sconto incondizionato

//ls_syntax = sqlca.syntaxfromsql(s_cs_xx.parametri.parametro_s_2,"style(type=grid)",ls_error)
ls_syntax = sqlca.syntaxfromsql(istr_statistiche_vendita.sql_note_credito  ,"style(type=grid)",ls_error)
	
if isnull(ls_syntax) or len(ls_syntax) < 1 then
	g_mb.messagebox("Errore costruzione sintassi",ls_error)
	return -1
end if

do
	ll_pos = pos(ls_syntax,"decimal(8)",1)
	if ll_pos > 0 then
		ls_syntax = replace(ls_syntax,ll_pos,11,"decimal(4)")
	end if
loop while ll_pos > 0

dw_note.create(ls_syntax)

dw_note.settransobject(sqlca)

dw_note.retrieve()


// Creo la DW per la gestione degli importo relativi a righe di sconto presenti in fatture
ls_syntax = sqlca.syntaxfromsql(istr_statistiche_vendita.sql_detrazioni_fatture  ,"style(type=grid)",ls_error)
	
if isnull(ls_syntax) or len(ls_syntax) < 1 then
	g_mb.messagebox("Errore costruzione sintassi",ls_error)
	return -1
end if

do
	ll_pos = pos(ls_syntax,"decimal(8)",1)
	if ll_pos > 0 then
		ls_syntax = replace(ls_syntax,ll_pos,11,"decimal(4)")
	end if
loop while ll_pos > 0

dw_detraz_fatture.create(ls_syntax)

dw_detraz_fatture.settransobject(sqlca)

dw_detraz_fatture.retrieve()



// Creo la DW per la gestione degli importo relativi a righe di sconto presenti in note di credito
ls_syntax = sqlca.syntaxfromsql(istr_statistiche_vendita.sql_detrazioni_note_credito  ,"style(type=grid)",ls_error)
	
if isnull(ls_syntax) or len(ls_syntax) < 1 then
	g_mb.messagebox("Errore costruzione sintassi",ls_error)
	return -1
end if

do
	ll_pos = pos(ls_syntax,"decimal(8)",1)
	if ll_pos > 0 then
		ls_syntax = replace(ls_syntax,ll_pos,11,"decimal(4)")
	end if
loop while ll_pos > 0

dw_nc_detraz.create(ls_syntax)

dw_nc_detraz.settransobject(sqlca)

dw_nc_detraz.retrieve()


// Creo la DW principale delle righe fattura
	
ls_syntax = sqlca.syntaxfromsql(s_cs_xx.parametri.parametro_s_1,"style(type=grid) column(border=2) text(alignment=2 border=2)",ls_error)
										  
if isnull(ls_syntax) or len(ls_syntax) < 1 then
	g_mb.messagebox("Errore costruzione sintassi",ls_error)
	return -1
end if

do
	ll_pos = pos(ls_syntax,"decimal(8)",1)
	if ll_pos > 0 then
		ls_syntax = replace(ls_syntax,ll_pos,11,"decimal(4)")
	end if
loop while ll_pos > 0

dw_esplosione_tes.create(ls_syntax)

dw_esplosione_tes.settransobject(sqlca)

dw_esplosione_tes.retrieve()

if dw_esplosione_tes.rowcount() < 1 then
	cb_salva.enabled = false
	cb_stampa.enabled = false
	rb_1.enabled = false
	rb_2.enabled = false
	return -1
end if

dw_esplosione_tes.object.datawindow.print.preview.rulers = 'Yes'

dw_esplosione_tes.object.datawindow.print.preview = 'Yes'

dw_esplosione_tes.object.datawindow.sparse = s_cs_xx.parametri.parametro_s_3

if ib_quantita then
	dw_esplosione_tes.setformat("quantita","###,###,###,##0.00")
end if	

dw_esplosione_tes.setformat("fatturato","###,###,###,##0.00")

//dw_esplosione_tes.saveas()

wf_report()

dw_esplosione_tes.show()
end event

event closequery;call super::closequery;if dw_esplosione_tes.rowcount() > 0 then
	if g_mb.messagebox("APICE","Chiudere esplosione testata?",question!,yesno!,2) = 2 then
		return 1
	end if	
end if
end event

event open;call super::open;x = 0
y = 0
end event

type dw_nc_detraz from datawindow within w_stat_esplosione_tes
boolean visible = false
integer x = 795
integer y = 104
integer width = 192
integer height = 148
integer taborder = 40
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type dw_detraz_fatture from datawindow within w_stat_esplosione_tes
boolean visible = false
integer x = 457
integer y = 100
integer width = 192
integer height = 148
integer taborder = 20
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type rb_2 from radiobutton within w_stat_esplosione_tes
integer x = 709
integer y = 2740
integer width = 640
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Stampa in orizzontale"
end type

event clicked;rb_1.checked = false
dw_esplosione_tes.object.datawindow.print.orientation = 1
end event

type rb_1 from radiobutton within w_stat_esplosione_tes
integer x = 46
integer y = 2740
integer width = 581
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Stampa in verticale"
boolean checked = true
end type

event clicked;rb_2.checked = false
dw_esplosione_tes.object.datawindow.print.orientation = 2
end event

type dw_note from datawindow within w_stat_esplosione_tes
boolean visible = false
integer x = 119
integer y = 96
integer width = 192
integer height = 148
integer taborder = 30
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_salva from commandbutton within w_stat_esplosione_tes
integer x = 3392
integer y = 2740
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esporta"
end type

event clicked;dw_esplosione_tes.saveas("",excel!,true)
end event

type cb_chiudi from commandbutton within w_stat_esplosione_tes
integer x = 4229
integer y = 2740
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;close(parent)
end event

type cb_stampa from commandbutton within w_stat_esplosione_tes
integer x = 3817
integer y = 2740
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;if g_mb.messagebox("APICE","Stampare il report visualizzato?",question!,yesno!,2) = 1 then
	dw_esplosione_tes.print()
end if
end event

type dw_esplosione_tes from uo_cs_xx_dw within w_stat_esplosione_tes
boolean visible = false
integer x = 23
integer y = 20
integer width = 4571
integer height = 2692
integer taborder = 10
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

