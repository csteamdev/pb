﻿$PBExportHeader$w_statistiche_vendita.srw
forward
global type w_statistiche_vendita from w_cs_xx_principale
end type
type cb_avanti from commandbutton within w_statistiche_vendita
end type
type cb_indietro from commandbutton within w_statistiche_vendita
end type
type st_14 from statictext within w_statistiche_vendita
end type
type st_descrizione from statictext within w_statistiche_vendita
end type
type st_codice from statictext within w_statistiche_vendita
end type
type st_13 from statictext within w_statistiche_vendita
end type
type st_10 from statictext within w_statistiche_vendita
end type
type tab_1 from tab within w_statistiche_vendita
end type
type tabpage_1 from userobject within tab_1
end type
type st_18 from statictext within tabpage_1
end type
type dw_tes_statistiche_lista from datawindow within tabpage_1
end type
type tabpage_1 from userobject within tab_1
st_18 st_18
dw_tes_statistiche_lista dw_tes_statistiche_lista
end type
type tabpage_2 from userobject within tab_1
end type
type st_20 from statictext within tabpage_2
end type
type st_19 from statictext within tabpage_2
end type
type st_8 from statictext within tabpage_2
end type
type dw_8 from datawindow within tabpage_2
end type
type cbx_prodotti from checkbox within tabpage_2
end type
type st_data_fine from statictext within tabpage_2
end type
type st_data_inizio from statictext within tabpage_2
end type
type cb_esplodi_tes from commandbutton within tabpage_2
end type
type em_data_fine from editmask within tabpage_2
end type
type em_data_inizio from editmask within tabpage_2
end type
type st_esplodi_testata from statictext within tabpage_2
end type
type st_1 from statictext within tabpage_2
end type
type st_2 from statictext within tabpage_2
end type
type st_3 from statictext within tabpage_2
end type
type st_4 from statictext within tabpage_2
end type
type st_5 from statictext within tabpage_2
end type
type st_6 from statictext within tabpage_2
end type
type st_7 from statictext within tabpage_2
end type
type st_filtri_disponibili from statictext within tabpage_2
end type
type st_9 from statictext within tabpage_2
end type
type dw_1 from datawindow within tabpage_2
end type
type dw_2 from datawindow within tabpage_2
end type
type dw_3 from datawindow within tabpage_2
end type
type dw_4 from datawindow within tabpage_2
end type
type dw_5 from datawindow within tabpage_2
end type
type dw_6 from datawindow within tabpage_2
end type
type dw_7 from datawindow within tabpage_2
end type
type gb_1 from groupbox within tabpage_2
end type
type gb_2 from groupbox within tabpage_2
end type
type gb_3 from groupbox within tabpage_2
end type
type tabpage_2 from userobject within tab_1
st_20 st_20
st_19 st_19
st_8 st_8
dw_8 dw_8
cbx_prodotti cbx_prodotti
st_data_fine st_data_fine
st_data_inizio st_data_inizio
cb_esplodi_tes cb_esplodi_tes
em_data_fine em_data_fine
em_data_inizio em_data_inizio
st_esplodi_testata st_esplodi_testata
st_1 st_1
st_2 st_2
st_3 st_3
st_4 st_4
st_5 st_5
st_6 st_6
st_7 st_7
st_filtri_disponibili st_filtri_disponibili
st_9 st_9
dw_1 dw_1
dw_2 dw_2
dw_3 dw_3
dw_4 dw_4
dw_5 dw_5
dw_6 dw_6
dw_7 dw_7
gb_1 gb_1
gb_2 gb_2
gb_3 gb_3
end type
type tabpage_3 from userobject within tab_1
end type
type st_16 from statictext within tabpage_3
end type
type st_12 from statictext within tabpage_3
end type
type cb_superiore from commandbutton within tabpage_3
end type
type st_liv_scelto_5 from statictext within tabpage_3
end type
type st_liv_scelto_4 from statictext within tabpage_3
end type
type st_livello_4 from statictext within tabpage_3
end type
type st_liv_scelto_2 from statictext within tabpage_3
end type
type st_liv_scelto_1 from statictext within tabpage_3
end type
type st_livello_5 from statictext within tabpage_3
end type
type st_liv_scelto_3 from statictext within tabpage_3
end type
type st_livello_3 from statictext within tabpage_3
end type
type st_livello_2 from statictext within tabpage_3
end type
type st_livello_1 from statictext within tabpage_3
end type
type dw_prod_statistiche_livelli from datawindow within tabpage_3
end type
type gb_4 from groupbox within tabpage_3
end type
type tabpage_3 from userobject within tab_1
st_16 st_16
st_12 st_12
cb_superiore cb_superiore
st_liv_scelto_5 st_liv_scelto_5
st_liv_scelto_4 st_liv_scelto_4
st_livello_4 st_livello_4
st_liv_scelto_2 st_liv_scelto_2
st_liv_scelto_1 st_liv_scelto_1
st_livello_5 st_livello_5
st_liv_scelto_3 st_liv_scelto_3
st_livello_3 st_livello_3
st_livello_2 st_livello_2
st_livello_1 st_livello_1
dw_prod_statistiche_livelli dw_prod_statistiche_livelli
gb_4 gb_4
end type
type tabpage_4 from userobject within tab_1
end type
type pb_togli_tutti from commandbutton within tabpage_4
end type
type pb_togli_corrente from commandbutton within tabpage_4
end type
type pb_aggiungi_corrente from commandbutton within tabpage_4
end type
type pb_aggiungi_tutti from commandbutton within tabpage_4
end type
type st_livello from statictext within tabpage_4
end type
type st_prod_selez from statictext within tabpage_4
end type
type st_15 from statictext within tabpage_4
end type
type st_prod_disponibili from statictext within tabpage_4
end type
type st_11 from statictext within tabpage_4
end type
type dw_prod_statistiche_selez from datawindow within tabpage_4
end type
type cbx_senza_livello from checkbox within tabpage_4
end type
type dw_prod_statistiche_elenco from datawindow within tabpage_4
end type
type gb_6 from groupbox within tabpage_4
end type
type gb_7 from groupbox within tabpage_4
end type
type gb_5 from groupbox within tabpage_4
end type
type gb_10 from groupbox within tabpage_4
end type
type tabpage_4 from userobject within tab_1
pb_togli_tutti pb_togli_tutti
pb_togli_corrente pb_togli_corrente
pb_aggiungi_corrente pb_aggiungi_corrente
pb_aggiungi_tutti pb_aggiungi_tutti
st_livello st_livello
st_prod_selez st_prod_selez
st_15 st_15
st_prod_disponibili st_prod_disponibili
st_11 st_11
dw_prod_statistiche_selez dw_prod_statistiche_selez
cbx_senza_livello cbx_senza_livello
dw_prod_statistiche_elenco dw_prod_statistiche_elenco
gb_6 gb_6
gb_7 gb_7
gb_5 gb_5
gb_10 gb_10
end type
type tabpage_5 from userobject within tab_1
end type
type st_prodotto from statictext within tabpage_5
end type
type gb_8 from groupbox within tabpage_5
end type
type gb_9 from groupbox within tabpage_5
end type
type dw_prod_statistiche_carat from datawindow within tabpage_5
end type
type cbx_esplosione_carat from checkbox within tabpage_5
end type
type st_da_data from statictext within tabpage_5
end type
type em_da_data from editmask within tabpage_5
end type
type st_a_data from statictext within tabpage_5
end type
type em_a_data from editmask within tabpage_5
end type
type cb_esegui from commandbutton within tabpage_5
end type
type tabpage_5 from userobject within tab_1
st_prodotto st_prodotto
gb_8 gb_8
gb_9 gb_9
dw_prod_statistiche_carat dw_prod_statistiche_carat
cbx_esplosione_carat cbx_esplosione_carat
st_da_data st_da_data
em_da_data em_da_data
st_a_data st_a_data
em_a_data em_a_data
cb_esegui cb_esegui
end type
type tabpage_6 from userobject within tab_1
end type
type st_17 from statictext within tabpage_6
end type
type dw_note_prec from datawindow within tabpage_6
end type
type dw_note from datawindow within tabpage_6
end type
type dw_esporta_report from datawindow within tabpage_6
end type
type dw_retrieve_prec from datawindow within tabpage_6
end type
type dw_retrieve from datawindow within tabpage_6
end type
type st_perc from statictext within tabpage_6
end type
type st_elaborazione from statictext within tabpage_6
end type
type hpb_1 from hprogressbar within tabpage_6
end type
type dw_report_statistiche_selez from datawindow within tabpage_6
end type
type dw_report_statistiche from datawindow within tabpage_6
end type
type tabpage_6 from userobject within tab_1
st_17 st_17
dw_note_prec dw_note_prec
dw_note dw_note
dw_esporta_report dw_esporta_report
dw_retrieve_prec dw_retrieve_prec
dw_retrieve dw_retrieve
st_perc st_perc
st_elaborazione st_elaborazione
hpb_1 hpb_1
dw_report_statistiche_selez dw_report_statistiche_selez
dw_report_statistiche dw_report_statistiche
end type
type tab_1 from tab within w_statistiche_vendita
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
tabpage_6 tabpage_6
end type
type r_1 from rectangle within w_statistiche_vendita
end type
type cb_modifica from commandbutton within w_statistiche_vendita
end type
type cb_salva_filtri from commandbutton within w_statistiche_vendita
end type
type cb_cancella from commandbutton within w_statistiche_vendita
end type
type cb_annulla_modifiche from commandbutton within w_statistiche_vendita
end type
type cb_stampa from commandbutton within w_statistiche_vendita
end type
type cb_esporta from commandbutton within w_statistiche_vendita
end type
type cb_interrompi from commandbutton within w_statistiche_vendita
end type
type cb_nuovo from commandbutton within w_statistiche_vendita
end type
type cb_modifica_filtri from commandbutton within w_statistiche_vendita
end type
type cb_selezione from commandbutton within w_statistiche_vendita
end type
type cb_azzera_testata from commandbutton within w_statistiche_vendita
end type
type cb_azzera_caratteristiche from commandbutton within w_statistiche_vendita
end type
type cb_azzera_prodotti from commandbutton within w_statistiche_vendita
end type
type cb_azzera_livelli from commandbutton within w_statistiche_vendita
end type
type cb_report from commandbutton within w_statistiche_vendita
end type
end forward

global type w_statistiche_vendita from w_cs_xx_principale
integer x = 0
integer y = 0
integer width = 4649
integer height = 2612
string title = "Statistiche di vendita"
boolean maxbox = false
boolean resizable = false
cb_avanti cb_avanti
cb_indietro cb_indietro
st_14 st_14
st_descrizione st_descrizione
st_codice st_codice
st_13 st_13
st_10 st_10
tab_1 tab_1
r_1 r_1
cb_modifica cb_modifica
cb_salva_filtri cb_salva_filtri
cb_cancella cb_cancella
cb_annulla_modifiche cb_annulla_modifiche
cb_stampa cb_stampa
cb_esporta cb_esporta
cb_interrompi cb_interrompi
cb_nuovo cb_nuovo
cb_modifica_filtri cb_modifica_filtri
cb_selezione cb_selezione
cb_azzera_testata cb_azzera_testata
cb_azzera_caratteristiche cb_azzera_caratteristiche
cb_azzera_prodotti cb_azzera_prodotti
cb_azzera_livelli cb_azzera_livelli
cb_report cb_report
end type
global w_statistiche_vendita w_statistiche_vendita

type variables
long   il_count, il_livello, il_prodotti, il_par_aperte, il_par_chiuse, il_caratteristiche, il_tab,&
		 il_retrievestart, il_null, il_prod_report, il_where_prod

string is_items[8], is_where_1, is_where_2, is_where_3, is_where_4, is_where_5, is_where_6, is_where_7, is_where_8, &
       is_select, is_codice_tes, is_codice_corrente, is_livello_1, is_livello_2, is_livello_3, is_livello_4, &
		 is_livello_5, is_codice_liv, is_codice_prod, is_codice_carat, is_cod_statistica, is_where_tes, is_where_prod[],&
		 is_prodotti[], is_filtri_report
		 
boolean ib_testata_mod, ib_livelli_mod, ib_prodotti_mod, ib_caratteristiche_mod, ib_consenti_modifica, ib_background,&
		  ib_report, ib_interrompi, ib_note_credito, ib_ferma_prodotti
		  
		  
// -- stefanop 18/01/2010
string	is_sql_in, is_sql_pf
end variables

forward prototypes
public function integer wf_sblocca_caratteristiche ()
public function integer wf_carica_drop_carat ()
public function long wf_ultimo_giorno_mese (long fl_anno, long fl_mese)
public function integer wf_calcola_periodi_sett (ref datetime fdt_data_da[], ref datetime fdt_data_a[], long fl_anno, long fl_mese, long fl_giorno, long fl_periodi)
public function integer wf_calcola_periodi_mesi (ref datetime fdt_data_da[], ref datetime fdt_data_a[], long fl_anno, long fl_mese, string fs_periodicita, long fl_periodi)
public function integer wf_carica_elenco_prodotti ()
public function integer wf_carica_livelli ()
public function integer wf_salva_livelli ()
public function integer wf_salva_prodotti ()
public function integer wf_blocca_caratteristiche ()
public function integer wf_carica_prodotti ()
public function integer wf_salva_testata ()
public function integer wf_carica_filtri_tes ()
public function integer wf_carica_filtri_tes_2 ()
public function integer wf_carica_dropdown (ref datawindow fdw_1)
public function integer wf_resetta_tutto ()
public function integer wf_salva_caratteristiche ()
public function integer wf_leggi_filtri ()
public function integer wf_carica_caratteristiche ()
public function integer wf_report ()
end prototypes

public function integer wf_sblocca_caratteristiche ();tab_1.tabpage_5.dw_prod_statistiche_carat.object.apri_par.protect = false

tab_1.tabpage_5.dw_prod_statistiche_carat.object.variante.protect = false

tab_1.tabpage_5.dw_prod_statistiche_carat.object.operatore_confronto.protect = false

tab_1.tabpage_5.dw_prod_statistiche_carat.object.valore_numerico.protect = false

tab_1.tabpage_5.dw_prod_statistiche_carat.object.valore_stringa.protect = false

tab_1.tabpage_5.dw_prod_statistiche_carat.object.chiudi_par.protect = false

tab_1.tabpage_5.dw_prod_statistiche_carat.object.operatore_logico.protect = false

return 0
end function

public function integer wf_carica_drop_carat ();datawindowchild ldwc_child

string ls_null

long   ll_riga


setnull(ls_null)

tab_1.tabpage_5.dw_prod_statistiche_carat.getchild("apri_par",ldwc_child)

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column",ls_null)

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","(")

tab_1.tabpage_5.dw_prod_statistiche_carat.getchild("variante",ldwc_child)

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column",ls_null)

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","Dimensioni X")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","Dimensioni Y")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","Tessuto telo")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","Tessuto Mantovana")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","Verniciatura")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","Comando")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","Comando 2")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","Supporto")

tab_1.tabpage_5.dw_prod_statistiche_carat.getchild("operatore_confronto",ldwc_child)

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column",ls_null)

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column",">")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","<")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column",">=")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","<=")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","=")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","<>")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","like")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","null")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","!null")

tab_1.tabpage_5.dw_prod_statistiche_carat.getchild("chiudi_par",ldwc_child)

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column",ls_null)

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column",")")

tab_1.tabpage_5.dw_prod_statistiche_carat.getchild("operatore_logico",ldwc_child)

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column",ls_null)

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","and")

ll_riga = ldwc_child.insertrow(0)

ldwc_child.setitem(ll_riga,"data_column","or")

return 0
end function

public function long wf_ultimo_giorno_mese (long fl_anno, long fl_mese);choose case fl_mese

	case 11,4,6,9
		
		return 30
		
	case 2
		
		if mod(fl_anno,4) = 0 then
			if mod(fl_anno,100) = 0 then
				if mod(fl_anno,400) = 0 then
					return 29
				else
					return 28
				end if	
			else
				return 29
			end if
		else
			return 28
		end if	
		
	case 1,3,5,7,8,10,12	
		
		return 31

	case else
		
		return -1

end choose
end function

public function integer wf_calcola_periodi_sett (ref datetime fdt_data_da[], ref datetime fdt_data_a[], long fl_anno, long fl_mese, long fl_giorno, long fl_periodi);long ll_i, ll_j


for ll_i = 1 to fl_periodi
	
	fdt_data_da[ll_i] = datetime(date( string(fl_giorno) + "/" + string(fl_mese) + "/" + string(fl_anno) ))
	
	for ll_j = 1 to 7
		
		fl_giorno++
		
		if fl_giorno > wf_ultimo_giorno_mese(fl_anno,fl_mese) then
			
			fl_mese++
			
			fl_giorno = 1
			
		end if
		
		if ll_j = 6 then			
			fdt_data_a[ll_i] = datetime(date( string(fl_giorno) + "/" + string(fl_mese) + "/" + string(fl_anno) ))
		end if
		
	next	
		
next	

return 0
end function

public function integer wf_calcola_periodi_mesi (ref datetime fdt_data_da[], ref datetime fdt_data_a[], long fl_anno, long fl_mese, string fs_periodicita, long fl_periodi);long ll_giorno, ll_i, ll_j




for ll_i = 1 to fl_periodi
	
	ll_giorno = 1
	
	fdt_data_da[ll_i] = datetime(date( string(ll_giorno) + "/" + string(fl_mese) + "/" + string(fl_anno) ))
	
	choose case fs_periodicita
			
		case "1"
			
			ll_giorno = wf_ultimo_giorno_mese(fl_anno,fl_mese)	
			
			fdt_data_a[ll_i] = datetime(date( string(ll_giorno) + "/" + string(fl_mese) + "/" + string(fl_anno) ))
			
			fl_mese++
				
			if fl_mese > 12 then
				fl_anno++
				fl_mese = 1
			end if
						
		case "3"
			
			for ll_j = 1 to 3
				
				fl_mese++
				
				if fl_mese > 12 then
					fl_anno++
					fl_mese = 1
				end if
				
				if ll_j = 2 then
					ll_giorno = wf_ultimo_giorno_mese(fl_anno,fl_mese)	
					fdt_data_a[ll_i] = datetime(date( string(ll_giorno) + "/" + string(fl_mese) + "/" + string(fl_anno) ))
				end if
				
			next
			
		case "6"
			
			for ll_j = 1 to 6
				
				fl_mese++
				
				if fl_mese > 12 then
					fl_anno++
					fl_mese = 1
				end if
				
				if ll_j = 5 then
					ll_giorno = wf_ultimo_giorno_mese(fl_anno,fl_mese)	
					fdt_data_a[ll_i] = datetime(date( string(ll_giorno) + "/" + string(fl_mese) + "/" + string(fl_anno) ))
				end if
				
			next
			
		case "12"
			
			for ll_j = 1 to 12
				
				fl_mese++
				
				if fl_mese > 12 then
					fl_anno++
					fl_mese = 1
				end if
				
				if ll_j = 11 then
					ll_giorno = wf_ultimo_giorno_mese(fl_anno,fl_mese)	
					fdt_data_a[ll_i] = datetime(date( string(ll_giorno) + "/" + string(fl_mese) + "/" + string(fl_anno) ))
				end if
				
			next
			
	end choose	
	
next	
	
	

return 0
end function

public function integer wf_carica_elenco_prodotti ();setpointer(hourglass!)

choose case il_livello
		
	case 0
		
		tab_1.tabpage_4.dw_prod_statistiche_elenco.setsqlselect("select cod_prodotto, " + &
						 															  "des_prodotto " + &
															  						  "from view_livelli_prodotti " + &
																					  "where cod_prodotto in (select distinct cod_prodotto from det_fat_ven where cod_prodotto is not null) " + &
																					  "order by cod_prodotto")
		
	case 1
		
		if tab_1.tabpage_4.cbx_senza_livello.checked then
		
			tab_1.tabpage_4.dw_prod_statistiche_elenco.setsqlselect("select cod_prodotto, " + &
						 															  "des_prodotto " + &
															  						  "from view_livelli_prodotti " + &
																					  "where livello_1 = '" + is_livello_1 + "' " + &
																					  " and  cod_prodotto in (select distinct cod_prodotto from det_fat_ven where cod_prodotto is not null) " + &
																					  "or (livello_1 is null " + &
																					  "and livello_2 is null " + &
																					  "and livello_3 is null " + &
																					  "and livello_4 is null " + &
																					  "and livello_5 is null) " + &
																					  "order by cod_prodotto")
			
		else
			
			tab_1.tabpage_4.dw_prod_statistiche_elenco.setsqlselect("select cod_prodotto, " + &
						 															  "des_prodotto " + &
															  						  "from view_livelli_prodotti " + &
																					  "where livello_1 = '" + is_livello_1 + "' " + &
																					  " and  cod_prodotto in (select distinct cod_prodotto from det_fat_ven where cod_prodotto is not null) " + &
																					  "order by cod_prodotto")
		
		end if
		
	case 2
		
		if tab_1.tabpage_4.cbx_senza_livello.checked then
		
			tab_1.tabpage_4.dw_prod_statistiche_elenco.setsqlselect("select cod_prodotto, " + &
						 															  "des_prodotto " + &
															  						  "from view_livelli_prodotti " + &
																					  "where (livello_1 = '" + is_livello_1 + "' " + &
																					  "and livello_2 = '" + is_livello_2 + "' " + &
																					  " and  cod_prodotto in (select distinct cod_prodotto from det_fat_ven where cod_prodotto is not null)) " + &
																					  "or (livello_1 is null " + &
																					  "and livello_2 is null " + &
																					  "and livello_3 is null " + &
																					  "and livello_4 is null " + &
																					  "and livello_5 is null) " + &
																					  "order by cod_prodotto")
			
		else
			
			tab_1.tabpage_4.dw_prod_statistiche_elenco.setsqlselect("select cod_prodotto, " + &
						 															  "des_prodotto " + &
															  						  "from view_livelli_prodotti " + &
																					  "where livello_1 = '" + is_livello_1 + "' " + &
																					  "and livello_2 = '" + is_livello_2 + "' " + &
																					  "and  cod_prodotto in (select distinct cod_prodotto from det_fat_ven where cod_prodotto is not null) " + &
																					  "order by cod_prodotto")
		
		end if
		
	case 3
		
		if tab_1.tabpage_4.cbx_senza_livello.checked then
		
			tab_1.tabpage_4.dw_prod_statistiche_elenco.setsqlselect("select cod_prodotto, " + &
						 															  "des_prodotto " + &
															  						  "from view_livelli_prodotti " + &
																					  "where (livello_1 = '" + is_livello_1 + "' " + &
																					  "and livello_2 = '" + is_livello_2 + "' " + &
																					  "and livello_3 = '" + is_livello_3 + "' " + &
																					  " and  cod_prodotto in (select distinct cod_prodotto from det_fat_ven where cod_prodotto is not null)) " + &
																					  "or (livello_1 is null " + &
																					  "and livello_2 is null " + &
																					  "and livello_3 is null " + &
																					  "and livello_4 is null " + &
																					  "and livello_5 is null) " + &
																					  "order by cod_prodotto")		
			
		else
			
			tab_1.tabpage_4.dw_prod_statistiche_elenco.setsqlselect("select cod_prodotto, " + &
						 															  "des_prodotto " + &
															  						  "from view_livelli_prodotti " + &
																					  "where livello_1 = '" + is_livello_1 + "' " + &
																					  "and livello_2 = '" + is_livello_2 + "' " + &
																					  "and livello_3 = '" + is_livello_3 + "' " + &
																					  "and  cod_prodotto in (select distinct cod_prodotto from det_fat_ven where cod_prodotto is not null) " + &
																					  "order by cod_prodotto")			
		
		end if
		
	case 4
		
		if tab_1.tabpage_4.cbx_senza_livello.checked then
		
			tab_1.tabpage_4.dw_prod_statistiche_elenco.setsqlselect("select cod_prodotto, " + &
						 															  "des_prodotto " + &
															  						  "from view_livelli_prodotti " + &
																					  "where (livello_1 = '" + is_livello_1 + "' " + &
																					  "and livello_2 = '" + is_livello_2 + "' " + &
																					  "and livello_3 = '" + is_livello_3 + "' " + &
																					  "and livello_4 = '" + is_livello_4 + "' " + &
																					  " and  cod_prodotto in (select distinct cod_prodotto from det_fat_ven where cod_prodotto is not null)) " + &
																					  "or (livello_1 is null " + &
																					  "and livello_2 is null " + &
																					  "and livello_3 is null " + &
																					  "and livello_4 is null " + &
																					  "and livello_5 is null) " + &
																					  "order by cod_prodotto")
			
		else
			
			tab_1.tabpage_4.dw_prod_statistiche_elenco.setsqlselect("select cod_prodotto, " + &
						 															  "des_prodotto " + &
															  						  "from view_livelli_prodotti " + &
																					  "where livello_1 = '" + is_livello_1 + "' " + &
																					  "and livello_2 = '" + is_livello_2 + "' " + &
																					  "and livello_3 = '" + is_livello_3 + "' " + &
																					  "and livello_4 = '" + is_livello_4 + "' " + &
																					  "and  cod_prodotto in (select distinct cod_prodotto from det_fat_ven where cod_prodotto is not null) " + &
																					  "order by cod_prodotto")
																					  
		end if
		
	case 5
		
		if tab_1.tabpage_4.cbx_senza_livello.checked then
		
			tab_1.tabpage_4.dw_prod_statistiche_elenco.setsqlselect("select cod_prodotto, " + &
						 															  "des_prodotto " + &
															  						  "from view_livelli_prodotti " + &
																					  "where (livello_1 = '" + is_livello_1 + "' " + &
																					  "and livello_2 = '" + is_livello_2 + "' " + &
																					  "and livello_3 = '" + is_livello_3 + "' " + &
																					  "and livello_4 = '" + is_livello_4 + "' " + &
																					  "and livello_5 = '" + is_livello_5 + "' " + &
																					  " and  cod_prodotto in (select distinct cod_prodotto from det_fat_ven where cod_prodotto is not null)) " + &
																					  "or (livello_1 is null " + &
																					  "and livello_2 is null " + &
																					  "and livello_3 is null " + &
																					  "and livello_4 is null " + &
																					  "and livello_5 is null) " + &
																					  "order by cod_prodotto")
			
		else
			
			tab_1.tabpage_4.dw_prod_statistiche_elenco.setsqlselect("select cod_prodotto, " + &
						 															  "des_prodotto " + &
															  						  "from view_livelli_prodotti " + &
																					  "where livello_1 = '" + is_livello_1 + "' " + &
																					  "and livello_2 = '" + is_livello_2 + "' " + &
																					  "and livello_3 = '" + is_livello_3 + "' " + &
																					  "and livello_4 = '" + is_livello_4 + "' " + &
																					  "and livello_5 = '" + is_livello_5 + "' " + &
																					  "and  cod_prodotto in (select distinct cod_prodotto from det_fat_ven where cod_prodotto is not null)" + &
																					  "order by cod_prodotto")
		
		end if
		
end choose

tab_1.tabpage_4.dw_prod_statistiche_elenco.retrieve()

tab_1.tabpage_4.st_prod_disponibili.text = string(tab_1.tabpage_4.dw_prod_statistiche_elenco.rowcount())

setpointer(arrow!)

return 0
end function

public function integer wf_carica_livelli ();string ls_des_livello[5]

setpointer(hourglass!)

select cod_livello_prod_1,
		 cod_livello_prod_2,
		 cod_livello_prod_3,
		 cod_livello_prod_4,
		 cod_livello_prod_5
into   :is_livello_1,
		 :is_livello_2,
		 :is_livello_3,
		 :is_livello_4,
		 :is_livello_5
from   tes_statistiche
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_statistica = :is_codice_corrente;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tes_statistiche: " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if

if not isnull(is_livello_5) then
	
	il_livello = 5
	
	select des_livello_1,
			 des_livello_2,
			 des_livello_3,
			 des_livello_4,
			 des_livello_5
	into   :ls_des_livello[1],
			 :ls_des_livello[2],
			 :ls_des_livello[3],
			 :ls_des_livello[4],
			 :ls_des_livello[5]
	from   view_livelli_prodotti	
	where  livello_1 = :is_livello_1 and
			 livello_2 = :is_livello_2 and
			 livello_3 = :is_livello_3 and
			 livello_4 = :is_livello_4 and
			 livello_5 = :is_livello_5;
	
elseif not isnull(is_livello_4) then
	
	il_livello = 4
	
	select des_livello_1,
			 des_livello_2,
			 des_livello_3,
			 des_livello_4
	into   :ls_des_livello[1],
			 :ls_des_livello[2],
			 :ls_des_livello[3],
			 :ls_des_livello[4]
	from   view_livelli_prodotti	
	where  livello_1 = :is_livello_1 and
			 livello_2 = :is_livello_2 and
			 livello_3 = :is_livello_3 and
			 livello_4 = :is_livello_4;
	
elseif not isnull(is_livello_3) then
	
	il_livello = 3
	
	select des_livello_1,
			 des_livello_2,
			 des_livello_3
	into   :ls_des_livello[1],
			 :ls_des_livello[2],
			 :ls_des_livello[3]
	from   view_livelli_prodotti	
	where  livello_1 = :is_livello_1 and
			 livello_2 = :is_livello_2 and
			 livello_3 = :is_livello_3;
	
elseif not isnull(is_livello_2) then
	
	il_livello = 2
	
	select des_livello_1,
			 des_livello_2
	into   :ls_des_livello[1],
			 :ls_des_livello[2]
	from   view_livelli_prodotti	
	where  livello_1 = :is_livello_1 and
			 livello_2 = :is_livello_2;
	
elseif not isnull(is_livello_1) then
	
	il_livello = 1
	
	select des_livello_1
	into   :ls_des_livello[1]
	from   view_livelli_prodotti	
	where  livello_1 = :is_livello_1;
	
else
	
	il_livello = 0
	
end if

choose case il_livello
		
	case 0
		
		tab_1.tabpage_4.st_livello.text = "Nessun livello selezionato"
		
		tab_1.tabpage_3.dw_prod_statistiche_livelli.setsqlselect("select distinct livello_1, " + &
						 															"des_livello_1 " + &
																					"from view_livelli_prodotti " + &
						 															"where livello_1 is not null " + &
						 															"order by livello_1")
																					 
		tab_1.tabpage_3.dw_prod_statistiche_livelli.retrieve()		
		
	case 1
		
		tab_1.tabpage_4.st_livello.text = is_livello_1 + " - " + ls_des_livello[1]
		
		tab_1.tabpage_3.st_liv_scelto_1.text = ls_des_livello[1]
		
		tab_1.tabpage_3.st_livello_1.italic = true
		
		tab_1.tabpage_3.st_livello_1.textcolor = rgb(0,0,255)		
		
		tab_1.tabpage_3.dw_prod_statistiche_livelli.setsqlselect("select distinct livello_2, " + &
						 															"des_livello_2 " + &
																					"from view_livelli_prodotti " + &
						 															"where livello_2 is not null " + &
						 															"and livello_1 = '" + is_livello_1 + "' " + &
						 															"order by livello_2")
		
		tab_1.tabpage_3.dw_prod_statistiche_livelli.retrieve()
	
	case 2
		
		tab_1.tabpage_4.st_livello.text = is_livello_2 + " - " + ls_des_livello[2]
		
		tab_1.tabpage_3.st_liv_scelto_1.text = ls_des_livello[1]
		
		tab_1.tabpage_3.st_livello_1.italic = true
		
		tab_1.tabpage_3.st_livello_1.textcolor = rgb(0,0,255)
		
		tab_1.tabpage_3.st_liv_scelto_2.text = ls_des_livello[2]
		
		tab_1.tabpage_3.st_livello_2.italic = true
		
		tab_1.tabpage_3.st_livello_2.textcolor = rgb(0,0,255)
		
		tab_1.tabpage_3.dw_prod_statistiche_livelli.setsqlselect("select distinct livello_3, " + &
						 															"des_livello_3 " + &
																					"from view_livelli_prodotti " + &
						 															"where livello_3 is not null " + &
						 															"and livello_1 = '" + is_livello_1 + "' " + &
																					"and livello_2 = '" + is_livello_2 + "' " + &
						 															"order by livello_3")
		
		tab_1.tabpage_3.dw_prod_statistiche_livelli.retrieve()
		
	case 3
		
		tab_1.tabpage_4.st_livello.text = is_livello_3 + " - " + ls_des_livello[3]
		
		tab_1.tabpage_3.st_liv_scelto_1.text = ls_des_livello[1]
		
		tab_1.tabpage_3.st_livello_1.italic = true
		
		tab_1.tabpage_3.st_livello_1.textcolor = rgb(0,0,255)
		
		tab_1.tabpage_3.st_liv_scelto_2.text = ls_des_livello[2]
		
		tab_1.tabpage_3.st_livello_2.italic = true
		
		tab_1.tabpage_3.st_livello_2.textcolor = rgb(0,0,255)
		
		tab_1.tabpage_3.st_liv_scelto_3.text = ls_des_livello[3]
		
		tab_1.tabpage_3.st_livello_3.italic = true
		
		tab_1.tabpage_3.st_livello_3.textcolor = rgb(0,0,255)
		
		tab_1.tabpage_3.dw_prod_statistiche_livelli.setsqlselect("select distinct livello_4, " + &
						 															"des_livello_4 " + &
																					"from view_livelli_prodotti " + &
						 															"where livello_4 is not null " + &
						 															"and livello_1 = '" + is_livello_1 + "' " + &
																					"and livello_2 = '" + is_livello_2 + "' " + &
																					"and livello_3 = '" + is_livello_3 + "' " + &
						 															"order by livello_4")
																					 
		tab_1.tabpage_3.dw_prod_statistiche_livelli.retrieve()
		
	case 4
		
		tab_1.tabpage_4.st_livello.text = is_livello_4 + " - " + ls_des_livello[4]
		
		tab_1.tabpage_3.st_liv_scelto_1.text = ls_des_livello[1]
		
		tab_1.tabpage_3.st_livello_1.italic = true
		
		tab_1.tabpage_3.st_livello_1.textcolor = rgb(0,0,255)
		
		tab_1.tabpage_3.st_liv_scelto_2.text = ls_des_livello[2]
		
		tab_1.tabpage_3.st_livello_2.italic = true
		
		tab_1.tabpage_3.st_livello_2.textcolor = rgb(0,0,255)
		
		tab_1.tabpage_3.st_liv_scelto_3.text = ls_des_livello[3]
		
		tab_1.tabpage_3.st_livello_3.italic = true
				
		tab_1.tabpage_3.st_livello_3.textcolor = rgb(0,0,255)
		
		tab_1.tabpage_3.st_liv_scelto_4.text = ls_des_livello[4]
		
		tab_1.tabpage_3.st_livello_4.italic = true
		
		tab_1.tabpage_3.st_livello_4.textcolor = rgb(0,0,255)
		
		tab_1.tabpage_3.dw_prod_statistiche_livelli.setsqlselect("select distinct livello_5, " + &
						 															"des_livello_5 " + &
																					"from view_livelli_prodotti " + &
						 															"where livello_5 is not null " + &
						 															"and livello_1 = '" + is_livello_1 + "' " + &
																					"and livello_2 = '" + is_livello_2 + "' " + &
																					"and livello_3 = '" + is_livello_3 + "' " + &
																					"and livello_4 = '" + is_livello_4 + "' " + &
						 															"order by livello_5")
																					 
		tab_1.tabpage_3.dw_prod_statistiche_livelli.retrieve()
		
	case 5
		
		tab_1.tabpage_4.st_livello.text = is_livello_5 + " - " + ls_des_livello[5]
		
		tab_1.tabpage_3.st_liv_scelto_1.text = ls_des_livello[1]
		
		tab_1.tabpage_3.st_livello_1.italic = true
		
		tab_1.tabpage_3.st_livello_1.textcolor = rgb(0,0,255)
		
		tab_1.tabpage_3.st_liv_scelto_2.text = ls_des_livello[2]
		
		tab_1.tabpage_3.st_livello_2.italic = true
		
		tab_1.tabpage_3.st_livello_2.textcolor = rgb(0,0,255)
		
		tab_1.tabpage_3.st_liv_scelto_3.text = ls_des_livello[3]
		
		tab_1.tabpage_3.st_livello_3.italic = true
		
		tab_1.tabpage_3.st_livello_3.textcolor = rgb(0,0,255)
		
		tab_1.tabpage_3.st_liv_scelto_4.text = ls_des_livello[4]
		
		tab_1.tabpage_3.st_livello_4.italic = true
		
		tab_1.tabpage_3.st_livello_4.textcolor = rgb(0,0,255)
		
		tab_1.tabpage_3.st_liv_scelto_5.text = ls_des_livello[5]
		
		tab_1.tabpage_3.st_livello_5.italic = true
		
		tab_1.tabpage_3.st_livello_5.textcolor = rgb(0,0,255)
		
end choose		

ib_livelli_mod = false

if il_livello > 0 and ib_consenti_modifica = true then
	cb_azzera_livelli.enabled = true
	tab_1.tabpage_3.cb_superiore.enabled = true	
end if

setpointer(arrow!)

return 0
end function

public function integer wf_salva_livelli ();update tes_statistiche
set	 cod_livello_prod_1 = :is_livello_1,
		 cod_livello_prod_2 = :is_livello_2,
		 cod_livello_prod_3 = :is_livello_3,
		 cod_livello_prod_4 = :is_livello_4,
		 cod_livello_prod_5 = :is_livello_5
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_statistica = :is_codice_corrente;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella update di tes_statistiche: " + sqlca.sqlerrtext)
	rollback;
	return -1
else
	commit;
	ib_livelli_mod = false	
end if

return 0
end function

public function integer wf_salva_prodotti ();string  ls_prodotto, ls_prod_controllo

long    ll_i, ll_count

boolean lb_trovato


declare controllo cursor for
select cod_prodotto
from   det_statistiche_filtro_prod
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_statistica = :is_codice_corrente;
		 
open controllo;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella open del cursore controllo: " + sqlca.sqlerrtext)
	return -1
end if

do while true
	
	fetch controllo
	into  :ls_prod_controllo;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella fetch del cursore controllo: " + sqlca.sqlerrtext)
		close controllo;
		rollback;		
		return -1
	elseif sqlca.sqlcode = 100 then
		close controllo;
		commit;
		exit
	end if
	
	lb_trovato = false
	
	for ll_i = 1 to tab_1.tabpage_4.dw_prod_statistiche_selez.rowcount()
		
		if tab_1.tabpage_4.dw_prod_statistiche_selez.getitemstring(ll_i,"cod_prodotto") = ls_prod_controllo then
			lb_trovato = true
			exit
		end if	
		
	next
	
	if lb_trovato = false or tab_1.tabpage_4.dw_prod_statistiche_selez.rowcount() > 1 then
		
		delete
		from   det_stat_filtro_varianti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_statistica = :is_codice_corrente and
				 cod_prodotto = :ls_prod_controllo;
				 
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore nella delete di det_stat_filtro_varianti: " + sqlca.sqlerrtext)
			close controllo;
			rollback;
			return -1
		end if
		
	end if	
		
	if lb_trovato = false then	
		
		delete
		from   det_statistiche_filtro_prod
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_statistica = :is_codice_corrente and
				 cod_prodotto = :ls_prod_controllo;
				 
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore nella delete di det_statistiche_filtro_prod: " + sqlca.sqlerrtext)
			close controllo;
			rollback;
			return -1
		end if
		
	end if	
	
loop	

for ll_i = 1 to tab_1.tabpage_4.dw_prod_statistiche_selez.rowcount()
	
	ls_prodotto = tab_1.tabpage_4.dw_prod_statistiche_selez.getitemstring(ll_i,"cod_prodotto")
	
	select count(*)
	into   :ll_count
	from   det_statistiche_filtro_prod
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_statistica = :is_codice_corrente and
			 cod_prodotto = :ls_prodotto;
			 
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella select di det_statistiche_filtro_prod: " + sqlca.sqlerrtext)
		return -1
	elseif ll_count > 0 then
		continue
	end if	
	
	insert
	into   det_statistiche_filtro_prod
			 (cod_azienda,
			 cod_statistica,
			 cod_prodotto)
	values (:s_cs_xx.cod_azienda,
			 :is_codice_corrente,
			 :ls_prodotto);
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella insert di det_statistiche_filtro_prod per il prodotto " + ls_prodotto + ": " + sqlca.sqlerrtext)
		rollback;
		return -1
	else
		commit;
	end if	
	
next	

ib_prodotti_mod = false

return 0
end function

public function integer wf_blocca_caratteristiche ();tab_1.tabpage_5.dw_prod_statistiche_carat.object.apri_par.protect = true

tab_1.tabpage_5.dw_prod_statistiche_carat.object.variante.protect = true

tab_1.tabpage_5.dw_prod_statistiche_carat.object.operatore_confronto.protect = true

tab_1.tabpage_5.dw_prod_statistiche_carat.object.valore_numerico.protect = true

tab_1.tabpage_5.dw_prod_statistiche_carat.object.valore_stringa.protect = true

tab_1.tabpage_5.dw_prod_statistiche_carat.object.chiudi_par.protect = true

tab_1.tabpage_5.dw_prod_statistiche_carat.object.operatore_logico.protect = true

return 0
end function

public function integer wf_carica_prodotti ();string ls_prodotto, ls_des_prodotto

long   ll_riga


wf_carica_elenco_prodotti()

setpointer(hourglass!)

tab_1.tabpage_4.dw_prod_statistiche_selez.reset()

il_prodotti = 0

tab_1.tabpage_4.st_prod_selez.text = "0"

declare prodotti cursor for
select cod_prodotto
from   det_statistiche_filtro_prod
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_statistica = :is_codice_corrente;
		 
open prodotti;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella open del cursore prodotti: " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if

do while true
	
	fetch prodotti
	into  :ls_prodotto;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella fetch del cursore prodotti: " + sqlca.sqlerrtext)
		close prodotti;
		setpointer(arrow!)
		return -1
	elseif sqlca.sqlcode = 100 then
		exit
	end if
	
	select des_prodotto
	into   :ls_des_prodotto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_prodotto;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nelal select di anag_prodotti: " + sqlca.sqlerrtext)
		close prodotti;
		setpointer(arrow!)
		return -1
	end if	

	ll_riga = tab_1.tabpage_4.dw_prod_statistiche_selez.insertrow(0)

	tab_1.tabpage_4.dw_prod_statistiche_selez.setitem(ll_riga,"cod_prodotto",ls_prodotto)
	
	tab_1.tabpage_4.dw_prod_statistiche_selez.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
	
	il_prodotti++

	tab_1.tabpage_4.st_prod_selez.text = string(il_prodotti)
	
loop

ib_prodotti_mod = false

if il_prodotti > 0 and ib_consenti_modifica = true then
	cb_azzera_prodotti.enabled = true
end if

close prodotti;

setpointer(arrow!)

return 0
end function

public function integer wf_salva_testata ();string ls_valore

delete
from   det_statistiche_filtro_tes
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_statistica = :is_codice_corrente;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella delete di det_statistiche_filtro_tes: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

if tab_1.tabpage_2.dw_1.rowcount() > 0 then 
	
	ls_valore = tab_1.tabpage_2.dw_1.getitemstring(1,"dati")

	insert
	into   det_statistiche_filtro_tes
			 (cod_azienda,
			 cod_statistica,
			 prog_ordinamento,
			 nome_campo,
			 valore_campo)
	values (:s_cs_xx.cod_azienda,
			 :is_codice_corrente,
			 1,
			 :is_items[1],
			 :ls_valore);
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella insert di det_statistiche_filtro_tes: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if	
	
end if	

if tab_1.tabpage_2.dw_2.rowcount() > 0 then 

	ls_valore = tab_1.tabpage_2.dw_2.getitemstring(1,"dati")

	insert
	into   det_statistiche_filtro_tes
			 (cod_azienda,
			 cod_statistica,
			 prog_ordinamento,
			 nome_campo,
			 valore_campo)
	values (:s_cs_xx.cod_azienda,
			 :is_codice_corrente,
			 2	,
			 :is_items[2],
			 :ls_valore);
		 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella insert di det_statistiche_filtro_tes: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
			 
end if

if tab_1.tabpage_2.dw_3.rowcount() > 0 then 
	
	ls_valore = tab_1.tabpage_2.dw_3.getitemstring(1,"dati")
	
	insert
	into   det_statistiche_filtro_tes
			 (cod_azienda,
			 cod_statistica,
			 prog_ordinamento,
			 nome_campo,
			 valore_campo)
	values (:s_cs_xx.cod_azienda,
			 :is_codice_corrente,
			 3,
			 :is_items[3],
			 :ls_valore);
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella insert di det_statistiche_filtro_tes: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
		 
end if

if tab_1.tabpage_2.dw_4.rowcount() > 0 then 

	ls_valore = tab_1.tabpage_2.dw_4.getitemstring(1,"dati")

	insert
	into   det_statistiche_filtro_tes
			 (cod_azienda,
			 cod_statistica,
			 prog_ordinamento,
			 nome_campo,
			 valore_campo)
	values (:s_cs_xx.cod_azienda,
			 :is_codice_corrente,
			 4,
			 :is_items[4],
			 :ls_valore);
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella insert di det_statistiche_filtro_tes: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
		 
end if

if tab_1.tabpage_2.dw_5.rowcount() > 0 then 
	
	ls_valore = tab_1.tabpage_2.dw_5.getitemstring(1,"dati")

	insert
	into   det_statistiche_filtro_tes
			 (cod_azienda,
			 cod_statistica,
			 prog_ordinamento,
			 nome_campo,
			 valore_campo)
	values (:s_cs_xx.cod_azienda,
			 :is_codice_corrente,
			 5,
			 :is_items[5],
			 :ls_valore);
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella insert di det_statistiche_filtro_tes: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
		 
end if

if tab_1.tabpage_2.dw_6.rowcount() > 0 then 

	ls_valore = tab_1.tabpage_2.dw_6.getitemstring(1,"dati")

	insert
	into   det_statistiche_filtro_tes
			 (cod_azienda,
			 cod_statistica,
			 prog_ordinamento,
			 nome_campo,
			 valore_campo)
	values (:s_cs_xx.cod_azienda,
			 :is_codice_corrente,
			 6,
			 :is_items[6],
			 :ls_valore);
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella insert di det_statistiche_filtro_tes: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
				 
end if

if tab_1.tabpage_2.dw_7.rowcount() > 0 then 

	ls_valore = tab_1.tabpage_2.dw_7.getitemstring(1,"dati")

	insert
	into   det_statistiche_filtro_tes
			 (cod_azienda,
			 cod_statistica,
			 prog_ordinamento,
			 nome_campo,
			 valore_campo)
	values (:s_cs_xx.cod_azienda,
			 :is_codice_corrente,
			 7,
			 :is_items[7],
			 :ls_valore);
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella insert di det_statistiche_filtro_tes: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
				 
end if

if tab_1.tabpage_2.dw_8.rowcount() > 0 then 

	ls_valore = tab_1.tabpage_2.dw_8.getitemstring(1,"dati")

	insert
	into   det_statistiche_filtro_tes
			 (cod_azienda,
			 cod_statistica,
			 prog_ordinamento,
			 nome_campo,
			 valore_campo)
	values (:s_cs_xx.cod_azienda,
			 :is_codice_corrente,
			 8,
			 :is_items[8],
			 :ls_valore);
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella insert di det_statistiche_filtro_tes: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
				 
end if

commit;

ib_testata_mod = false
	
return 0
end function

public function integer wf_carica_filtri_tes ();string ls_campo, ls_valore

long   ll_ordine


setpointer(hourglass!)

declare filtro_tes cursor for
select prog_ordinamento,
		 nome_campo,
		 valore_campo
from   det_statistiche_filtro_tes
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_statistica = :is_codice_corrente and
		 prog_ordinamento < 5;
		 
open filtro_tes;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella open del cursore filtro_tes: " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if

do while true
	
	fetch filtro_tes
	into  :ll_ordine,
			:ls_campo,
			:ls_valore;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella open del cursore filtro_tes: " + sqlca.sqlerrtext)
		close filtro_tes;
		setpointer(arrow!)
		return -1
	elseif sqlca.sqlcode = 100 then
		exit
	end if
	
	if isnull(ls_valore) or len(ls_valore) < 1 then
		setnull(ls_valore)
		il_null++
	end if
		
	choose case ll_ordine
			
		case 1
			
			tab_1.tabpage_2.dw_1.object.dati.protect = true
			
			choose case ls_campo
				
				case "cod_tipo_fat_ven"
					tab_1.tabpage_2.dw_1.insertrow(0)
					is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata order by tipi_fat_cod_tipo_fat_ven"
					wf_carica_dropdown(tab_1.tabpage_2.dw_1)				
					tab_1.tabpage_2.dw_1.object.dati_t.text = tab_1.tabpage_2.st_1.text
					tab_1.tabpage_2.dw_1.visible = true
					tab_1.tabpage_2.dw_1.setfocus()	
					is_items[1] = "cod_tipo_fat_ven"
					il_count++
					tab_1.tabpage_2.st_1.italic = true
					tab_1.tabpage_2.st_1.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_1.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_1 = " where 1=1"
					else
						is_where_1 = " where tipi_fat_cod_tipo_fat_ven = '" + ls_valore + "'"
					end if	
				case "cod_cliente"
					tab_1.tabpage_2.dw_1.insertrow(0)
					is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata order by clienti_cod_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_1)				
					tab_1.tabpage_2.dw_1.object.dati_t.text = tab_1.tabpage_2.st_2.text
					tab_1.tabpage_2.dw_1.visible = true
					tab_1.tabpage_2.dw_1.setfocus()	
					is_items[1] = "cod_cliente"
					il_count++
					tab_1.tabpage_2.st_2.italic = true
					tab_1.tabpage_2.st_2.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_1.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_1 = " where 1=1"
					else
						is_where_1 = " where clienti_cod_cliente = '" + ls_valore + "'"
					end if	
				case "flag_tipo_cliente"
					tab_1.tabpage_2.dw_1.insertrow(0)
					is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata order by clienti_flag_tipo_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_1)				
					tab_1.tabpage_2.dw_1.object.dati_t.text = tab_1.tabpage_2.st_3.text
					tab_1.tabpage_2.dw_1.visible = true
					tab_1.tabpage_2.dw_1.setfocus()	
					is_items[1] = "flag_tipo_cliente"
					il_count++
					tab_1.tabpage_2.st_3.italic = true
					tab_1.tabpage_2.st_3.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_1.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_1 = " where 1=1"
					else
						is_where_1 = " where clienti_flag_tipo_cliente = '" + ls_valore + "'"
					end if	
				case "provincia"
					tab_1.tabpage_2.dw_1.insertrow(0)
					is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata order by clienti_provincia"
					wf_carica_dropdown(tab_1.tabpage_2.dw_1)				
					tab_1.tabpage_2.dw_1.object.dati_t.text = tab_1.tabpage_2.st_4.text
					tab_1.tabpage_2.dw_1.visible = true
					tab_1.tabpage_2.dw_1.setfocus()	
					is_items[1] = "provincia"
					il_count++
					tab_1.tabpage_2.st_4.italic = true
					tab_1.tabpage_2.st_4.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_1.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_1 = " where 1=1"
					else
						is_where_1 = " where clienti_provincia = '" + ls_valore + "'"
					end if	
				case "cod_agente_1"
					tab_1.tabpage_2.dw_1.insertrow(0)
					is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata order by clienti_cod_agente_1"
					wf_carica_dropdown(tab_1.tabpage_2.dw_1)				
					tab_1.tabpage_2.dw_1.object.dati_t.text = tab_1.tabpage_2.st_5.text
					tab_1.tabpage_2.dw_1.visible = true
					tab_1.tabpage_2.dw_1.setfocus()	
					is_items[1] = "cod_agente_1"
					il_count++
					tab_1.tabpage_2.st_5.italic = true
					tab_1.tabpage_2.st_5.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_1.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_1 = " where 1=1"
					else
						is_where_1 = " where clienti_cod_agente_1 = '" + ls_valore + "'"
					end if	
				case "cod_zona"
					tab_1.tabpage_2.dw_1.insertrow(0)
					is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata order by clienti_cod_zona"
					wf_carica_dropdown(tab_1.tabpage_2.dw_1)				
					tab_1.tabpage_2.dw_1.object.dati_t.text = tab_1.tabpage_2.st_6.text
					tab_1.tabpage_2.dw_1.visible = true
					tab_1.tabpage_2.dw_1.setfocus()	
					is_items[1] = "cod_zona"
					il_count++
					tab_1.tabpage_2.st_6.italic = true
					tab_1.tabpage_2.st_6.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_1.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_1 = " where 1=1"
					else
						is_where_1 = " where clienti_cod_zona = '" + ls_valore + "'"
					end if	
				case "cod_giro"					
					tab_1.tabpage_2.dw_1.insertrow(0)
					is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata order by giri_cod_giro_consegna"
					wf_carica_dropdown(tab_1.tabpage_2.dw_1)				
					tab_1.tabpage_2.dw_1.object.dati_t.text = tab_1.tabpage_2.st_7.text
					tab_1.tabpage_2.dw_1.visible = true
					tab_1.tabpage_2.dw_1.setfocus()	
					is_items[1] = "cod_giro"
					il_count++
					tab_1.tabpage_2.st_7.italic = true
					tab_1.tabpage_2.st_7.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_1.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_1 = " where 1=1"
					else
						is_where_1 = " where giri_cod_giro_consegna = '" + ls_valore + "'"
					end if
				case "cod_categoria"					
					tab_1.tabpage_2.dw_1.insertrow(0)
					is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata order by categorie_des_categoria"
					wf_carica_dropdown(tab_1.tabpage_2.dw_1)				
					tab_1.tabpage_2.dw_1.object.dati_t.text = tab_1.tabpage_2.st_8.text
					tab_1.tabpage_2.dw_1.visible = true
					tab_1.tabpage_2.dw_1.setfocus()	
					is_items[1] = "cod_categoria"
					il_count++
					tab_1.tabpage_2.st_8.italic = true
					tab_1.tabpage_2.st_8.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_1.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_1 = " where 1=1"
					else
						is_where_1 = " where clienti_cod_categoria = '" + ls_valore + "'"
					end if	
				case "cod_deposito"					
					tab_1.tabpage_2.dw_1.insertrow(0)
					is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata order by cod_deposito"
					wf_carica_dropdown(tab_1.tabpage_2.dw_1)				
					tab_1.tabpage_2.dw_1.object.dati_t.text = tab_1.tabpage_2.st_19.text
					tab_1.tabpage_2.dw_1.visible = true
					tab_1.tabpage_2.dw_1.setfocus()	
					is_items[1] = "cod_deposito"
					il_count++
					tab_1.tabpage_2.st_19.italic = true
					tab_1.tabpage_2.st_19.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_1.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_1 = " where 1=1"
					else
						is_where_1 = " where cod_deposito = '" + ls_valore + "'"
					end if	
				case "cod_tipo_anagrafica"					
					tab_1.tabpage_2.dw_1.insertrow(0)
					is_select = "select distinct(cod_tipo_anagrafica), des_tipo_anagrafica from view_sel_testata order by cod_tipo_anagrafica"
					wf_carica_dropdown(tab_1.tabpage_2.dw_1)				
					tab_1.tabpage_2.dw_1.object.dati_t.text = tab_1.tabpage_2.st_20.text
					tab_1.tabpage_2.dw_1.visible = true
					tab_1.tabpage_2.dw_1.setfocus()	
					is_items[1] = "cod_tipo_anagrafica"
					il_count++
					tab_1.tabpage_2.st_20.italic = true
					tab_1.tabpage_2.st_20.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_1.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_1 = " where 1=1"
					else
						is_where_1 = " where cod_tipo_anagrafica = '" + ls_valore + "'"
					end if	
			end choose
			
		case 2
			
			tab_1.tabpage_2.dw_2.object.dati.protect = true
			
			choose case ls_campo
				
				case "cod_tipo_fat_ven"
					tab_1.tabpage_2.dw_1.object.dati.protect = true
					tab_1.tabpage_2.dw_2.insertrow(0)
					is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata" + &
									is_where_1 + " order by tipi_fat_cod_tipo_fat_ven"
					wf_carica_dropdown(tab_1.tabpage_2.dw_2)				
					tab_1.tabpage_2.dw_2.object.dati_t.text = tab_1.tabpage_2.st_1.text
					tab_1.tabpage_2.dw_2.visible = true
					tab_1.tabpage_2.dw_2.setfocus()	
					is_items[2] = "cod_tipo_fat_ven"
					il_count++
					tab_1.tabpage_2.st_1.italic = true
					tab_1.tabpage_2.st_1.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_2.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_2 = ""
					else
						is_where_2 = " and tipi_fat_cod_tipo_fat_ven = '" + ls_valore + "'"
					end if	
				case "cod_cliente"
					tab_1.tabpage_2.dw_1.object.dati.protect = true
					tab_1.tabpage_2.dw_2.insertrow(0)
					is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata" + &
									is_where_1 + " order by clienti_cod_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_2)				
					tab_1.tabpage_2.dw_2.object.dati_t.text = tab_1.tabpage_2.st_2.text
					tab_1.tabpage_2.dw_2.visible = true
					tab_1.tabpage_2.dw_2.setfocus()	
					is_items[2] = "cod_cliente"
					il_count++
					tab_1.tabpage_2.st_2.italic = true
					tab_1.tabpage_2.st_2.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_2.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_2 = ""
					else
						is_where_2 = " and clienti_cod_cliente = '" + ls_valore + "'"
					end if	
				case "flag_tipo_cliente"
					tab_1.tabpage_2.dw_1.object.dati.protect = true
					tab_1.tabpage_2.dw_2.insertrow(0)
					is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata" + &
									is_where_1 + " order by clienti_flag_tipo_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_2)				
					tab_1.tabpage_2.dw_2.object.dati_t.text = tab_1.tabpage_2.st_3.text
					tab_1.tabpage_2.dw_2.visible = true
					tab_1.tabpage_2.dw_2.setfocus()	
					is_items[2] = "flag_tipo_cliente"
					il_count++
					tab_1.tabpage_2.st_3.italic = true
					tab_1.tabpage_2.st_3.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_2.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_2 = ""
					else
						is_where_2 = " and clienti_flag_tipo_cliente = '" + ls_valore + "'"
					end if	
				case "provincia"
					tab_1.tabpage_2.dw_1.object.dati.protect = true
					tab_1.tabpage_2.dw_2.insertrow(0)
					is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata" + &
									is_where_1 + " order by clienti_provincia"
					wf_carica_dropdown(tab_1.tabpage_2.dw_2)				
					tab_1.tabpage_2.dw_2.object.dati_t.text = tab_1.tabpage_2.st_4.text
					tab_1.tabpage_2.dw_2.visible = true
					tab_1.tabpage_2.dw_2.setfocus()	
					is_items[2] = "provincia"
					il_count++
					tab_1.tabpage_2.st_4.italic = true
					tab_1.tabpage_2.st_4.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_2.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_2 = ""
					else
						is_where_2 = " and clienti_provincia = '" + ls_valore + "'"
					end if	
				case "cod_agente_1"
					tab_1.tabpage_2.dw_1.object.dati.protect = true
					tab_1.tabpage_2.dw_2.insertrow(0)
					is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata" + &
									is_where_1 + " order by clienti_cod_agente_1"
					wf_carica_dropdown(tab_1.tabpage_2.dw_2)				
					tab_1.tabpage_2.dw_2.object.dati_t.text = tab_1.tabpage_2.st_5.text
					tab_1.tabpage_2.dw_2.visible = true
					tab_1.tabpage_2.dw_2.setfocus()	
					is_items[2] = "cod_agente_1"
					il_count++
					tab_1.tabpage_2.st_5.italic = true
					tab_1.tabpage_2.st_5.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_2.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_2 = ""
					else
						is_where_2 = " and clienti_cod_agente_1 = '" + ls_valore + "'"
					end if	
				case "cod_zona"
					tab_1.tabpage_2.dw_1.object.dati.protect = true
					tab_1.tabpage_2.dw_2.insertrow(0)
					is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata" + &
									is_where_1 + " order by clienti_cod_zona"
					wf_carica_dropdown(tab_1.tabpage_2.dw_2)				
					tab_1.tabpage_2.dw_2.object.dati_t.text = tab_1.tabpage_2.st_6.text
					tab_1.tabpage_2.dw_2.visible = true
					tab_1.tabpage_2.dw_2.setfocus()	
					is_items[2] = "cod_zona"
					il_count++
					tab_1.tabpage_2.st_6.italic = true
					tab_1.tabpage_2.st_6.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_2.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_2 = ""
					else
						is_where_2 = " and clienti_cod_zona = '" + ls_valore + "'"
					end if	
				case "cod_giro"
					tab_1.tabpage_2.dw_1.object.dati.protect = true
					tab_1.tabpage_2.dw_2.insertrow(0)
					is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata" + &
									is_where_1 + " order by giri_cod_giro_consegna"
					wf_carica_dropdown(tab_1.tabpage_2.dw_2)				
					tab_1.tabpage_2.dw_2.object.dati_t.text = tab_1.tabpage_2.st_7.text
					tab_1.tabpage_2.dw_2.visible = true
					tab_1.tabpage_2.dw_2.setfocus()	
					is_items[2] = "cod_giro"
					il_count++
					tab_1.tabpage_2.st_7.italic = true
					tab_1.tabpage_2.st_7.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_2.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_2 = ""
					else
						is_where_2 = " and giri_cod_giro_consegna = '" + ls_valore + "'"
					end if
				case "cod_categoria"
					tab_1.tabpage_2.dw_1.object.dati.protect = true
					tab_1.tabpage_2.dw_2.insertrow(0)
					is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata" + &
									is_where_1 + " order by clienti_cod_categoria"
					wf_carica_dropdown(tab_1.tabpage_2.dw_2)				
					tab_1.tabpage_2.dw_2.object.dati_t.text = tab_1.tabpage_2.st_8.text
					tab_1.tabpage_2.dw_2.visible = true
					tab_1.tabpage_2.dw_2.setfocus()	
					is_items[2] = "cod_categoria"
					il_count++
					tab_1.tabpage_2.st_8.italic = true
					tab_1.tabpage_2.st_8.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_2.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_2 = ""
					else
						is_where_2 = " and clienti_cod_categoria = '" + ls_valore + "'"
					end if	
				case "cod_deposito"					
					tab_1.tabpage_2.dw_1.object.dati.protect = true
					tab_1.tabpage_2.dw_2.insertrow(0)
					is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata " + 	is_where_1 + " order by cod_deposito"
					wf_carica_dropdown(tab_1.tabpage_2.dw_2)				
					tab_1.tabpage_2.dw_2.object.dati_t.text = tab_1.tabpage_2.st_19.text
					tab_1.tabpage_2.dw_2.visible = true
					tab_1.tabpage_2.dw_2.setfocus()	
					is_items[2] = "cod_deposito"
					il_count++
					tab_1.tabpage_2.st_19.italic = true
					tab_1.tabpage_2.st_19.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_2.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_2 = ""
					else
						is_where_2 = " and cod_deposito = '" + ls_valore + "'"
					end if	
				case "cod_tipo_anagrafica"					
					tab_1.tabpage_2.dw_2.insertrow(0)
					is_select = "select distinct(cod_tipo_anagrafica), des_tipo_anagrafica from view_sel_testata " + 	is_where_1 + " order by cod_tipo_anagrafica"
					wf_carica_dropdown(tab_1.tabpage_2.dw_2)				
					tab_1.tabpage_2.dw_2.object.dati_t.text = tab_1.tabpage_2.st_20.text
					tab_1.tabpage_2.dw_2.visible = true
					tab_1.tabpage_2.dw_2.setfocus()	
					is_items[2] = "cod_tipo_anagrafica"
					il_count++
					tab_1.tabpage_2.st_20.italic = true
					tab_1.tabpage_2.st_20.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_2.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_2 = ""
					else
						is_where_2 = " where cod_tipo_anagrafica = '" + ls_valore + "'"
					end if	
			end choose
			
		case 3
			
			tab_1.tabpage_2.dw_3.object.dati.protect = true
			
			choose case ls_campo
				
				case "cod_tipo_fat_ven"
					tab_1.tabpage_2.dw_2.object.dati.protect = true
					tab_1.tabpage_2.dw_3.insertrow(0)
					is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata" + &
									is_where_1 + is_where_2 + " order by tipi_fat_cod_tipo_fat_ven"
					wf_carica_dropdown(tab_1.tabpage_2.dw_3)				
					tab_1.tabpage_2.dw_3.object.dati_t.text = tab_1.tabpage_2.st_1.text
					tab_1.tabpage_2.dw_3.visible = true
					tab_1.tabpage_2.dw_3.setfocus()	
					is_items[3] = "cod_tipo_fat_ven"
					il_count++
					tab_1.tabpage_2.st_1.italic = true
					tab_1.tabpage_2.st_1.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_3.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_3 = ""
					else
						is_where_3 = " and tipi_fat_cod_tipo_fat_ven = '" + ls_valore + "'"
					end if	
				case "cod_cliente"
					tab_1.tabpage_2.dw_2.object.dati.protect = true
					tab_1.tabpage_2.dw_3.insertrow(0)
					is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata" + &
									is_where_1 + is_where_2 + " order by clienti_cod_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_3)				
					tab_1.tabpage_2.dw_3.object.dati_t.text = tab_1.tabpage_2.st_2.text
					tab_1.tabpage_2.dw_3.visible = true
					tab_1.tabpage_2.dw_3.setfocus()	
					is_items[3] = "cod_cliente"
					il_count++
					tab_1.tabpage_2.st_2.italic = true
					tab_1.tabpage_2.st_2.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_3.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_3 = ""
					else
						is_where_3 = " and clienti_cod_cliente = '" + ls_valore + "'"
					end if	
				case "flag_tipo_cliente"
					tab_1.tabpage_2.dw_2.object.dati.protect = true
					tab_1.tabpage_2.dw_3.insertrow(0)
					is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata" + &
									is_where_1 + is_where_2 + " order by clienti_flag_tipo_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_3)				
					tab_1.tabpage_2.dw_3.object.dati_t.text = tab_1.tabpage_2.st_3.text
					tab_1.tabpage_2.dw_3.visible = true
					tab_1.tabpage_2.dw_3.setfocus()	
					is_items[3] = "flag_tipo_cliente"
					il_count++
					tab_1.tabpage_2.st_3.italic = true
					tab_1.tabpage_2.st_3.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_3.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_3 = ""
					else
						is_where_3 = " and clienti_flag_tipo_cliente = '" + ls_valore + "'"
					end if	
				case "provincia"
					tab_1.tabpage_2.dw_2.object.dati.protect = true
					tab_1.tabpage_2.dw_3.insertrow(0)
					is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata" + &
									is_where_1 + is_where_2 + " order by clienti_provincia"
					wf_carica_dropdown(tab_1.tabpage_2.dw_3)				
					tab_1.tabpage_2.dw_3.object.dati_t.text = tab_1.tabpage_2.st_4.text
					tab_1.tabpage_2.dw_3.visible = true
					tab_1.tabpage_2.dw_3.setfocus()	
					is_items[3] = "provincia"
					il_count++
					tab_1.tabpage_2.st_4.italic = true
					tab_1.tabpage_2.st_4.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_3.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_3 = ""
					else
						is_where_3 = " and clienti_provincia = '" + ls_valore + "'"
					end if	
				case "cod_agente_1"
					tab_1.tabpage_2.dw_2.object.dati.protect = true
					tab_1.tabpage_2.dw_3.insertrow(0)
					is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata" + &
									is_where_1 + is_where_2 + " order by clienti_cod_agente_1"
					wf_carica_dropdown(tab_1.tabpage_2.dw_3)				
					tab_1.tabpage_2.dw_3.object.dati_t.text = tab_1.tabpage_2.st_5.text
					tab_1.tabpage_2.dw_3.visible = true
					tab_1.tabpage_2.dw_3.setfocus()	
					is_items[3] = "cod_agente_1"
					il_count++
					tab_1.tabpage_2.st_5.italic = true
					tab_1.tabpage_2.st_5.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_3.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_3 = ""
					else
						is_where_3 = " and clienti_cod_agente_1 = '" + ls_valore + "'"
					end if	
				case "cod_zona"
					tab_1.tabpage_2.dw_2.object.dati.protect = true
					tab_1.tabpage_2.dw_3.insertrow(0)
					is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata" + &
									is_where_1 + is_where_2 + " order by clienti_cod_zona"
					wf_carica_dropdown(tab_1.tabpage_2.dw_3)				
					tab_1.tabpage_2.dw_3.object.dati_t.text = tab_1.tabpage_2.st_6.text
					tab_1.tabpage_2.dw_3.visible = true
					tab_1.tabpage_2.dw_3.setfocus()	
					is_items[3] = "cod_zona"
					il_count++
					tab_1.tabpage_2.st_6.italic = true
					tab_1.tabpage_2.st_6.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_3.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_3 = ""
					else
						is_where_3 = " and clienti_cod_zona = '" + ls_valore + "'"
					end if	
				case "cod_giro"
					tab_1.tabpage_2.dw_2.object.dati.protect = true
					tab_1.tabpage_2.dw_3.insertrow(0)
					is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata" + &
									is_where_1 + is_where_2 + " order by giri_cod_giro_consegna"
					wf_carica_dropdown(tab_1.tabpage_2.dw_3)				
					tab_1.tabpage_2.dw_3.object.dati_t.text = tab_1.tabpage_2.st_7.text
					tab_1.tabpage_2.dw_3.visible = true
					tab_1.tabpage_2.dw_3.setfocus()	
					is_items[3] = "cod_giro"
					il_count++
					tab_1.tabpage_2.st_7.italic = true
					tab_1.tabpage_2.st_7.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_3.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_3 = ""
					else
						is_where_3 = " and giri_cod_giro_consegna = '" + ls_valore + "'"
					end if
				case "cod_categoria"
					tab_1.tabpage_2.dw_2.object.dati.protect = true
					tab_1.tabpage_2.dw_3.insertrow(0)
					is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata" + &
									is_where_1 + is_where_2 + " order by clienti_cod_categoria"
					wf_carica_dropdown(tab_1.tabpage_2.dw_3)				
					tab_1.tabpage_2.dw_3.object.dati_t.text = tab_1.tabpage_2.st_8.text
					tab_1.tabpage_2.dw_3.visible = true
					tab_1.tabpage_2.dw_3.setfocus()	
					is_items[3] = "cod_categoria"
					il_count++
					tab_1.tabpage_2.st_8.italic = true
					tab_1.tabpage_2.st_8.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_3.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_3 = ""
					else
						is_where_3 = " and clienti_cod_categoria = '" + ls_valore + "'"
					end if	
				case "cod_deposito"					
					tab_1.tabpage_2.dw_2.object.dati.protect = true
					tab_1.tabpage_2.dw_3.insertrow(0)
					is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata" + &
									is_where_1 + is_where_2 + " order by cod_deposito"
					wf_carica_dropdown(tab_1.tabpage_2.dw_3)				
					tab_1.tabpage_2.dw_3.object.dati_t.text = tab_1.tabpage_2.st_19.text
					tab_1.tabpage_2.dw_3.visible = true
					tab_1.tabpage_2.dw_3.setfocus()	
					is_items[3] = "cod_deposito"
					il_count++
					tab_1.tabpage_2.st_19.italic = true
					tab_1.tabpage_2.st_19.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_3.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_3 = ""
					else
						is_where_3 = " and cod_deposito = '" + ls_valore + "'"
					end if	
			end choose	
			
		case 4
			
			tab_1.tabpage_2.dw_4.object.dati.protect = true
			
			choose case ls_campo
				
				case "cod_tipo_fat_ven"
					tab_1.tabpage_2.dw_3.object.dati.protect = true
					tab_1.tabpage_2.dw_4.insertrow(0)
					is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata" + &
									is_where_1 + is_where_2 + is_where_3 + " order by tipi_fat_cod_tipo_fat_ven"
					wf_carica_dropdown(tab_1.tabpage_2.dw_4)				
					tab_1.tabpage_2.dw_4.object.dati_t.text = tab_1.tabpage_2.st_1.text
					tab_1.tabpage_2.dw_4.visible = true
					tab_1.tabpage_2.dw_4.setfocus()	
					is_items[4] = "cod_tipo_fat_ven"
					il_count++
					tab_1.tabpage_2.st_1.italic = true
					tab_1.tabpage_2.st_1.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_4.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_4 = ""
					else
						is_where_4 = " and tipi_fat_cod_tipo_fat_ven = '" + ls_valore + "'"
					end if	
				case "cod_cliente"
					tab_1.tabpage_2.dw_3.object.dati.protect = true
					tab_1.tabpage_2.dw_4.insertrow(0)
					is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata" + &
									is_where_1 + is_where_2 + is_where_3 + " order by clienti_cod_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_4)				
					tab_1.tabpage_2.dw_4.object.dati_t.text = tab_1.tabpage_2.st_2.text
					tab_1.tabpage_2.dw_4.visible = true
					tab_1.tabpage_2.dw_4.setfocus()	
					is_items[4] = "cod_cliente"
					il_count++
					tab_1.tabpage_2.st_2.italic = true
					tab_1.tabpage_2.st_2.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_4.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_4 = ""
					else
						is_where_4 = " and clienti_cod_cliente = '" + ls_valore + "'"
					end if	
				case "flag_tipo_cliente"
					tab_1.tabpage_2.dw_3.object.dati.protect = true
					tab_1.tabpage_2.dw_4.insertrow(0)
					is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata" + &
									is_where_1 + is_where_2 + is_where_3 + " order by clienti_flag_tipo_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_4)				
					tab_1.tabpage_2.dw_4.object.dati_t.text = tab_1.tabpage_2.st_3.text
					tab_1.tabpage_2.dw_4.visible = true
					tab_1.tabpage_2.dw_4.setfocus()	
					is_items[4] = "flag_tipo_cliente"
					il_count++
					tab_1.tabpage_2.st_3.italic = true
					tab_1.tabpage_2.st_3.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_4.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_4 = ""
					else
						is_where_4 = " and clienti_flag_tipo_cliente = '" + ls_valore + "'"
					end if	
				case "provincia"
					tab_1.tabpage_2.dw_3.object.dati.protect = true
					tab_1.tabpage_2.dw_4.insertrow(0)
					is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata" + &
									is_where_1 + is_where_2 + is_where_3 + " order by clienti_provincia"
					wf_carica_dropdown(tab_1.tabpage_2.dw_4)				
					tab_1.tabpage_2.dw_4.object.dati_t.text = tab_1.tabpage_2.st_4.text
					tab_1.tabpage_2.dw_4.visible = true
					tab_1.tabpage_2.dw_4.setfocus()	
					is_items[4] = "provincia"
					il_count++
					tab_1.tabpage_2.st_4.italic = true
					tab_1.tabpage_2.st_4.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_4.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_4 = ""
					else
						is_where_4 = " and clienti_provincia = '" + ls_valore + "'"
					end if	
				case "cod_agente_1"
					tab_1.tabpage_2.dw_3.object.dati.protect = true
					tab_1.tabpage_2.dw_4.insertrow(0)
					is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata" + &
									is_where_1 + is_where_2 + is_where_3 + " order by clienti_cod_agente_1"
					wf_carica_dropdown(tab_1.tabpage_2.dw_4)				
					tab_1.tabpage_2.dw_4.object.dati_t.text = tab_1.tabpage_2.st_5.text
					tab_1.tabpage_2.dw_4.visible = true
					tab_1.tabpage_2.dw_4.setfocus()	
					is_items[4] = "cod_agente_1"
					il_count++
					tab_1.tabpage_2.st_5.italic = true
					tab_1.tabpage_2.st_5.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_4.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_4 = ""
					else
						is_where_4 = " and clienti_cod_agente_1 = '" + ls_valore + "'"
					end if	
				case "cod_zona"
					tab_1.tabpage_2.dw_3.object.dati.protect = true
					tab_1.tabpage_2.dw_4.insertrow(0)
					is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata" + &
									is_where_1 + is_where_2 + is_where_3 + " order by clienti_cod_zona"
					wf_carica_dropdown(tab_1.tabpage_2.dw_4)				
					tab_1.tabpage_2.dw_4.object.dati_t.text = tab_1.tabpage_2.st_6.text
					tab_1.tabpage_2.dw_4.visible = true
					tab_1.tabpage_2.dw_4.setfocus()	
					is_items[4] = "cod_zona"
					il_count++
					tab_1.tabpage_2.st_6.italic = true
					tab_1.tabpage_2.st_6.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_4.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_4 = ""
					else
						is_where_4 = " and clienti_cod_zona = '" + ls_valore + "'"
					end if	
				case "cod_giro"
					tab_1.tabpage_2.dw_3.object.dati.protect = true
					tab_1.tabpage_2.dw_4.insertrow(0)
					is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata" + &
									is_where_1 + is_where_2 + is_where_3 + " order by giri_cod_giro_consegna"
					wf_carica_dropdown(tab_1.tabpage_2.dw_4)				
					tab_1.tabpage_2.dw_4.object.dati_t.text = tab_1.tabpage_2.st_7.text
					tab_1.tabpage_2.dw_4.visible = true
					tab_1.tabpage_2.dw_4.setfocus()	
					is_items[4] = "cod_giro"
					il_count++
					tab_1.tabpage_2.st_7.italic = true
					tab_1.tabpage_2.st_7.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_4.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_4 = ""
					else
						is_where_4 = " and giri_cod_giro_consegna = '" + ls_valore + "'"
					end if
				case "cod_categoria"
					tab_1.tabpage_2.dw_3.object.dati.protect = true
					tab_1.tabpage_2.dw_4.insertrow(0)
					is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata" + &
									is_where_1 + is_where_2 + is_where_3 + " order by clienti_cod_categoria"
					wf_carica_dropdown(tab_1.tabpage_2.dw_4)				
					tab_1.tabpage_2.dw_4.object.dati_t.text = tab_1.tabpage_2.st_8.text
					tab_1.tabpage_2.dw_4.visible = true
					tab_1.tabpage_2.dw_4.setfocus()	
					is_items[4] = "cod_categoria"
					il_count++
					tab_1.tabpage_2.st_8.italic = true
					tab_1.tabpage_2.st_8.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_4.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_4 = ""
					else
						is_where_4 = " and clienti_cod_categoria = '" + ls_valore + "'"
					end if	
				case "cod_deposito"					
					tab_1.tabpage_2.dw_3.object.dati.protect = true
					tab_1.tabpage_2.dw_4.insertrow(0)
					is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata" + &
									is_where_1 + is_where_2 +is_where_3 + " order by cod_deposito"
					wf_carica_dropdown(tab_1.tabpage_2.dw_4)				
					tab_1.tabpage_2.dw_4.object.dati_t.text = tab_1.tabpage_2.st_19.text
					tab_1.tabpage_2.dw_4.visible = true
					tab_1.tabpage_2.dw_4.setfocus()	
					is_items[4] = "cod_deposito"
					il_count++
					tab_1.tabpage_2.st_19.italic = true
					tab_1.tabpage_2.st_19.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_4.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_4 = ""
					else
						is_where_4 = " and cod_deposito = '" + ls_valore + "'"
					end if	
			end choose		
		
	end choose		
	
loop

close filtro_tes;

setpointer(arrow!)

return 0
end function

public function integer wf_carica_filtri_tes_2 ();string ls_campo, ls_valore

long   ll_ordine


setpointer(hourglass!)

declare filtro_tes cursor for
select prog_ordinamento,
		 nome_campo,
		 valore_campo
from   det_statistiche_filtro_tes
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_statistica = :is_codice_corrente and
		 prog_ordinamento > 4;
		 
open filtro_tes;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella open del cursore filtro_tes: " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if

do while true
	
	fetch filtro_tes
	into  :ll_ordine,
			:ls_campo,
			:ls_valore;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella open del cursore filtro_tes: " + sqlca.sqlerrtext)
		close filtro_tes;
		setpointer(arrow!)
		return -1
	elseif sqlca.sqlcode = 100 then
		exit
	end if
	
	if isnull(ls_valore) or len(ls_valore) < 1 then
		setnull(ls_valore)
		il_null++
	end if
		
	choose case ll_ordine
			
		case 5
			
			tab_1.tabpage_2.dw_5.object.dati.protect = true
			
			choose case ls_campo
				
				case "cod_tipo_fat_ven"
					tab_1.tabpage_2.dw_4.object.dati.protect = true
					tab_1.tabpage_2.dw_5.insertrow(0)
					is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + " order by tipi_fat_cod_tipo_fat_ven"
					wf_carica_dropdown(tab_1.tabpage_2.dw_5)				
					tab_1.tabpage_2.dw_5.object.dati_t.text = tab_1.tabpage_2.st_1.text
					tab_1.tabpage_2.dw_5.visible = true
					tab_1.tabpage_2.dw_5.setfocus()	
					is_items[5] = "cod_tipo_fat_ven"
					il_count++
					tab_1.tabpage_2.st_1.italic = true
					tab_1.tabpage_2.st_1.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_5.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_5 = ""
					else
						is_where_5 = " and tipi_fat_cod_tipo_fat_ven = '" + ls_valore + "'"
					end if	
				case "cod_cliente"
					tab_1.tabpage_2.dw_4.object.dati.protect = true
					tab_1.tabpage_2.dw_5.insertrow(0)
					is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata" + 	is_where_1 + is_where_2 + is_where_3 + is_where_4 + " order by clienti_cod_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_5)				
					tab_1.tabpage_2.dw_5.object.dati_t.text = tab_1.tabpage_2.st_2.text
					tab_1.tabpage_2.dw_5.visible = true
					tab_1.tabpage_2.dw_5.setfocus()	
					is_items[5] = "cod_cliente"
					il_count++
					tab_1.tabpage_2.st_2.italic = true
					tab_1.tabpage_2.st_2.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_5.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_5 = ""
					else
						is_where_5 = " and clienti_cod_cliente = '" + ls_valore + "'"
					end if	
				case "flag_tipo_cliente"
					tab_1.tabpage_2.dw_4.object.dati.protect = true
					tab_1.tabpage_2.dw_5.insertrow(0)
					is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + " order by clienti_flag_tipo_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_5)				
					tab_1.tabpage_2.dw_5.object.dati_t.text = tab_1.tabpage_2.st_3.text
					tab_1.tabpage_2.dw_5.visible = true
					tab_1.tabpage_2.dw_5.setfocus()	
					is_items[5] = "flag_tipo_cliente"
					il_count++
					tab_1.tabpage_2.st_3.italic = true
					tab_1.tabpage_2.st_3.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_5.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_5 = ""
					else
						is_where_5 = " and clienti_flag_tipo_cliente = '" + ls_valore + "'"
					end if	
				case "provincia"
					tab_1.tabpage_2.dw_4.object.dati.protect = true
					tab_1.tabpage_2.dw_5.insertrow(0)
					is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata" + 	is_where_1 + is_where_2 + is_where_3 + is_where_4 + " order by clienti_provincia"
					wf_carica_dropdown(tab_1.tabpage_2.dw_5)				
					tab_1.tabpage_2.dw_5.object.dati_t.text = tab_1.tabpage_2.st_4.text
					tab_1.tabpage_2.dw_5.visible = true
					tab_1.tabpage_2.dw_5.setfocus()	
					is_items[5] = "provincia"
					il_count++
					tab_1.tabpage_2.st_4.italic = true
					tab_1.tabpage_2.st_4.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_5.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_5 = ""
					else
						is_where_5 = " and clienti_provincia = '" + ls_valore + "'"
					end if	
				case "cod_agente_1"
					tab_1.tabpage_2.dw_4.object.dati.protect = true
					tab_1.tabpage_2.dw_5.insertrow(0)
					is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + " order by clienti_cod_agente_1"
					wf_carica_dropdown(tab_1.tabpage_2.dw_5)				
					tab_1.tabpage_2.dw_5.object.dati_t.text = tab_1.tabpage_2.st_5.text
					tab_1.tabpage_2.dw_5.visible = true
					tab_1.tabpage_2.dw_5.setfocus()	
					is_items[5] = "cod_agente_1"
					il_count++
					tab_1.tabpage_2.st_5.italic = true
					tab_1.tabpage_2.st_5.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_5.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_5 = ""
					else
						is_where_5 = " and clienti_cod_agente_1 = '" + ls_valore + "'"
					end if	
				case "cod_zona"
					tab_1.tabpage_2.dw_4.object.dati.protect = true
					tab_1.tabpage_2.dw_5.insertrow(0)
					is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + " order by clienti_cod_zona"
					wf_carica_dropdown(tab_1.tabpage_2.dw_5)				
					tab_1.tabpage_2.dw_5.object.dati_t.text = tab_1.tabpage_2.st_6.text
					tab_1.tabpage_2.dw_5.visible = true
					tab_1.tabpage_2.dw_5.setfocus()	
					is_items[5] = "cod_zona"
					il_count++
					tab_1.tabpage_2.st_6.italic = true
					tab_1.tabpage_2.st_6.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_5.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_5 = ""
					else
						is_where_5 = " and clienti_cod_zona = '" + ls_valore + "'"
					end if	
				case "cod_giro"
					tab_1.tabpage_2.dw_4.object.dati.protect = true
					tab_1.tabpage_2.dw_5.insertrow(0)
					is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + " order by giri_cod_giro_consegna"
					wf_carica_dropdown(tab_1.tabpage_2.dw_5)				
					tab_1.tabpage_2.dw_5.object.dati_t.text = tab_1.tabpage_2.st_7.text
					tab_1.tabpage_2.dw_5.visible = true
					tab_1.tabpage_2.dw_5.setfocus()	
					is_items[5] = "cod_giro"
					il_count++
					tab_1.tabpage_2.st_7.italic = true
					tab_1.tabpage_2.st_7.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_5.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_5 = ""
					else
						is_where_5 = " and giri_cod_giro_consegna = '" + ls_valore + "'"
					end if
				case "cod_categoria"
					tab_1.tabpage_2.dw_4.object.dati.protect = true
					tab_1.tabpage_2.dw_5.insertrow(0)
					is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + " order by clienti_cod_categoria"
					wf_carica_dropdown(tab_1.tabpage_2.dw_5)				
					tab_1.tabpage_2.dw_5.object.dati_t.text = tab_1.tabpage_2.st_8.text
					tab_1.tabpage_2.dw_5.visible = true
					tab_1.tabpage_2.dw_5.setfocus()	
					is_items[5] = "cod_categoria"
					il_count++
					tab_1.tabpage_2.st_8.italic = true
					tab_1.tabpage_2.st_8.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_5.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_5 = ""
					else
						is_where_5 = " and clienti_cod_categoria = '" + ls_valore + "'"
					end if	
				case "cod_deposito"
					tab_1.tabpage_2.dw_4.object.dati.protect = true
					tab_1.tabpage_2.dw_5.insertrow(0)
					is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + " order by clienti_cod_categoria"
					wf_carica_dropdown(tab_1.tabpage_2.dw_5)				
					tab_1.tabpage_2.dw_5.object.dati_t.text = tab_1.tabpage_2.st_19.text
					tab_1.tabpage_2.dw_5.visible = true
					tab_1.tabpage_2.dw_5.setfocus()	
					is_items[5] = "cod_categoria"
					il_count++
					tab_1.tabpage_2.st_19.italic = true
					tab_1.tabpage_2.st_19.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_5.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_5 = ""
					else
						is_where_5 = " and clienti_cod_categoria = '" + ls_valore + "'"
					end if	
			end choose
			
		case 6
			
			tab_1.tabpage_2.dw_6.object.dati.protect = true
			
			choose case ls_campo
				
				case "cod_tipo_fat_ven"
					tab_1.tabpage_2.dw_5.object.dati.protect = true
					tab_1.tabpage_2.dw_6.insertrow(0)
					is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by tipi_fat_cod_tipo_fat_ven"
					wf_carica_dropdown(tab_1.tabpage_2.dw_6)				
					tab_1.tabpage_2.dw_6.object.dati_t.text = tab_1.tabpage_2.st_1.text
					tab_1.tabpage_2.dw_6.visible = true
					tab_1.tabpage_2.dw_6.setfocus()	
					is_items[6] = "cod_tipo_fat_ven"
					il_count++
					tab_1.tabpage_2.st_1.italic = true
					tab_1.tabpage_2.st_1.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_6.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_6 = ""
					else
						is_where_6 = " and tipi_fat_cod_tipo_fat_ven = '" + ls_valore + "'"
					end if	
				case "cod_cliente"
					tab_1.tabpage_2.dw_5.object.dati.protect = true
					tab_1.tabpage_2.dw_6.insertrow(0)
					is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by clienti_cod_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_6)				
					tab_1.tabpage_2.dw_6.object.dati_t.text = tab_1.tabpage_2.st_2.text
					tab_1.tabpage_2.dw_6.visible = true
					tab_1.tabpage_2.dw_6.setfocus()	
					is_items[6] = "cod_cliente"
					il_count++
					tab_1.tabpage_2.st_2.italic = true
					tab_1.tabpage_2.st_2.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_6.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_6 = ""
					else
						is_where_6 = " and clienti_cod_cliente = '" + ls_valore + "'"
					end if	
				case "flag_tipo_cliente"
					tab_1.tabpage_2.dw_5.object.dati.protect = true
					tab_1.tabpage_2.dw_6.insertrow(0)
					is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by clienti_flag_tipo_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_6)				
					tab_1.tabpage_2.dw_6.object.dati_t.text = tab_1.tabpage_2.st_3.text
					tab_1.tabpage_2.dw_6.visible = true
					tab_1.tabpage_2.dw_6.setfocus()	
					is_items[6] = "flag_tipo_cliente"
					il_count++
					tab_1.tabpage_2.st_3.italic = true
					tab_1.tabpage_2.st_3.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_6.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_6 = ""
					else
						is_where_6 = " and clienti_flag_tipo_cliente = '" + ls_valore + "'"
					end if	
				case "provincia"
					tab_1.tabpage_2.dw_5.object.dati.protect = true
					tab_1.tabpage_2.dw_6.insertrow(0)
					is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by clienti_provincia"
					wf_carica_dropdown(tab_1.tabpage_2.dw_6)				
					tab_1.tabpage_2.dw_6.object.dati_t.text = tab_1.tabpage_2.st_4.text
					tab_1.tabpage_2.dw_6.visible = true
					tab_1.tabpage_2.dw_6.setfocus()	
					is_items[6] = "provincia"
					il_count++
					tab_1.tabpage_2.st_4.italic = true
					tab_1.tabpage_2.st_4.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_6.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_6 = ""
					else
						is_where_6 = " and clienti_provincia = '" + ls_valore + "'"
					end if	
				case "cod_agente_1"
					tab_1.tabpage_2.dw_5.object.dati.protect = true
					tab_1.tabpage_2.dw_6.insertrow(0)
					is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by clienti_cod_agente_1"
					wf_carica_dropdown(tab_1.tabpage_2.dw_6)				
					tab_1.tabpage_2.dw_6.object.dati_t.text = tab_1.tabpage_2.st_5.text
					tab_1.tabpage_2.dw_6.visible = true
					tab_1.tabpage_2.dw_6.setfocus()	
					is_items[6] = "cod_agente_1"
					il_count++
					tab_1.tabpage_2.st_5.italic = true
					tab_1.tabpage_2.st_5.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_6.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_6 = ""
					else
						is_where_6 = " and clienti_cod_agente_1 = '" + ls_valore + "'"
					end if	
				case "cod_zona"
					tab_1.tabpage_2.dw_5.object.dati.protect = true
					tab_1.tabpage_2.dw_6.insertrow(0)
					is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by clienti_cod_zona"
					wf_carica_dropdown(tab_1.tabpage_2.dw_6)				
					tab_1.tabpage_2.dw_6.object.dati_t.text = tab_1.tabpage_2.st_6.text
					tab_1.tabpage_2.dw_6.visible = true
					tab_1.tabpage_2.dw_6.setfocus()	
					is_items[6] = "cod_zona"
					il_count++
					tab_1.tabpage_2.st_6.italic = true
					tab_1.tabpage_2.st_6.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_6.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_6 = ""
					else
						is_where_6 = " and clienti_cod_zona = '" + ls_valore + "'"
					end if
				case "cod_giro"
					tab_1.tabpage_2.dw_5.object.dati.protect = true
					tab_1.tabpage_2.dw_6.insertrow(0)
					is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by giri_cod_giro_consegna"
					wf_carica_dropdown(tab_1.tabpage_2.dw_6)				
					tab_1.tabpage_2.dw_6.object.dati_t.text = tab_1.tabpage_2.st_7.text
					tab_1.tabpage_2.dw_6.visible = true
					tab_1.tabpage_2.dw_6.setfocus()	
					is_items[6] = "cod_giro"
					il_count++
					tab_1.tabpage_2.st_7.italic = true
					tab_1.tabpage_2.st_7.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_6.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_6 = ""
					else
						is_where_6 = " and giri_cod_giro_consegna = '" + ls_valore + "'"
					end if
				case "cod_categoria"
					tab_1.tabpage_2.dw_5.object.dati.protect = true
					tab_1.tabpage_2.dw_6.insertrow(0)
					is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by clienti_cod_categoria"
					wf_carica_dropdown(tab_1.tabpage_2.dw_6)				
					tab_1.tabpage_2.dw_6.object.dati_t.text = tab_1.tabpage_2.st_8.text
					tab_1.tabpage_2.dw_6.visible = true
					tab_1.tabpage_2.dw_6.setfocus()	
					is_items[6] = "cod_categoria"
					il_count++
					tab_1.tabpage_2.st_8.italic = true
					tab_1.tabpage_2.st_8.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_6.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_6 = ""
					else
						is_where_6 = " and clienti_cod_categoria = '" + ls_valore + "'"
					end if	
				case "cod_deposito"
					tab_1.tabpage_2.dw_5.object.dati.protect = true
					tab_1.tabpage_2.dw_6.insertrow(0)
					is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by clienti_cod_categoria"
					wf_carica_dropdown(tab_1.tabpage_2.dw_6)				
					tab_1.tabpage_2.dw_6.object.dati_t.text = tab_1.tabpage_2.st_19.text
					tab_1.tabpage_2.dw_6.visible = true
					tab_1.tabpage_2.dw_6.setfocus()	
					is_items[6] = "cod_deposito"
					il_count++
					tab_1.tabpage_2.st_19.italic = true
					tab_1.tabpage_2.st_19.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_6.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_6 = ""
					else
						is_where_6 = " and cod_deposito = '" + ls_valore + "'"
					end if	
			end choose	
			
		case 7
			
			tab_1.tabpage_2.dw_7.object.dati.protect = true
			
			choose case ls_campo
				
				case "cod_tipo_fat_ven"
					tab_1.tabpage_2.dw_6.object.dati.protect = true
					tab_1.tabpage_2.dw_7.insertrow(0)
					is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by tipi_fat_cod_tipo_fat_ven"
					wf_carica_dropdown(tab_1.tabpage_2.dw_7)				
					tab_1.tabpage_2.dw_7.object.dati_t.text = tab_1.tabpage_2.st_1.text
					tab_1.tabpage_2.dw_7.visible = true
					tab_1.tabpage_2.dw_7.setfocus()	
					is_items[7] = "cod_tipo_fat_ven"
					il_count++
					tab_1.tabpage_2.st_1.italic = true
					tab_1.tabpage_2.st_1.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_7.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_7 = ""
					else
						is_where_7 = " and tipi_fat_cod_tipo_fat_ven = '" + ls_valore + "'"
					end if	
				case "cod_cliente"
					tab_1.tabpage_2.dw_6.object.dati.protect = true
					tab_1.tabpage_2.dw_7.insertrow(0)
					is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by clienti_cod_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_7)				
					tab_1.tabpage_2.dw_7.object.dati_t.text = tab_1.tabpage_2.st_2.text
					tab_1.tabpage_2.dw_7.visible = true
					tab_1.tabpage_2.dw_7.setfocus()	
					is_items[7] = "cod_cliente"
					il_count++
					tab_1.tabpage_2.st_2.italic = true
					tab_1.tabpage_2.st_2.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_7.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_7 = ""
					else
						is_where_7 = " and clienti_cod_cliente = '" + ls_valore + "'"
					end if	
				case "flag_tipo_cliente"
					tab_1.tabpage_2.dw_6.object.dati.protect = true
					tab_1.tabpage_2.dw_7.insertrow(0)
					is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by clienti_flag_tipo_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_7)				
					tab_1.tabpage_2.dw_7.object.dati_t.text = tab_1.tabpage_2.st_3.text
					tab_1.tabpage_2.dw_7.visible = true
					tab_1.tabpage_2.dw_7.setfocus()	
					is_items[7] = "flag_tipo_cliente"
					il_count++
					tab_1.tabpage_2.st_3.italic = true
					tab_1.tabpage_2.st_3.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_7.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_7 = ""
					else
						is_where_7 = " and clienti_flag_tipo_cliente = '" + ls_valore + "'"
					end if	
				case "provincia"
					tab_1.tabpage_2.dw_6.object.dati.protect = true
					tab_1.tabpage_2.dw_7.insertrow(0)
					is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by clienti_provincia"
					wf_carica_dropdown(tab_1.tabpage_2.dw_7)				
					tab_1.tabpage_2.dw_7.object.dati_t.text = tab_1.tabpage_2.st_4.text
					tab_1.tabpage_2.dw_7.visible = true
					tab_1.tabpage_2.dw_7.setfocus()	
					is_items[7] = "provincia"
					il_count++
					tab_1.tabpage_2.st_4.italic = true
					tab_1.tabpage_2.st_4.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_7.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_7 = ""
					else
						is_where_7 = " and clienti_provincia = '" + ls_valore + "'"
					end if	
				case "cod_agente_1"
					tab_1.tabpage_2.dw_6.object.dati.protect = true
					tab_1.tabpage_2.dw_7.insertrow(0)
					is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by clienti_cod_agente_1"
					wf_carica_dropdown(tab_1.tabpage_2.dw_7)				
					tab_1.tabpage_2.dw_7.object.dati_t.text = tab_1.tabpage_2.st_5.text
					tab_1.tabpage_2.dw_7.visible = true
					tab_1.tabpage_2.dw_7.setfocus()	
					is_items[7] = "cod_agente_1"
					il_count++
					tab_1.tabpage_2.st_5.italic = true
					tab_1.tabpage_2.st_5.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_7.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_7 = ""
					else
						is_where_7 = " and clienti_cod_agente_1 = '" + ls_valore + "'"
					end if	
				case "cod_zona"
					tab_1.tabpage_2.dw_6.object.dati.protect = true
					tab_1.tabpage_2.dw_7.insertrow(0)
					is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by clienti_cod_zona"
					wf_carica_dropdown(tab_1.tabpage_2.dw_7)				
					tab_1.tabpage_2.dw_7.object.dati_t.text = tab_1.tabpage_2.st_6.text
					tab_1.tabpage_2.dw_7.visible = true
					tab_1.tabpage_2.dw_7.setfocus()	
					is_items[7] = "cod_zona"
					il_count++
					tab_1.tabpage_2.st_6.italic = true
					tab_1.tabpage_2.st_6.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_7.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_7 = ""
					else
						is_where_7 = " and clienti_cod_zona = '" + ls_valore + "'"
					end if	
				case "cod_giro"
					tab_1.tabpage_2.dw_6.object.dati.protect = true
					tab_1.tabpage_2.dw_7.insertrow(0)
					is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by giri_cod_giro_consegna"
					wf_carica_dropdown(tab_1.tabpage_2.dw_7)				
					tab_1.tabpage_2.dw_7.object.dati_t.text = tab_1.tabpage_2.st_7.text
					tab_1.tabpage_2.dw_7.visible = true
					tab_1.tabpage_2.dw_7.setfocus()	
					is_items[7] = "cod_giro"
					il_count++
					tab_1.tabpage_2.st_7.italic = true
					tab_1.tabpage_2.st_7.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_7.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_7 = ""
					else
						is_where_7 = " and giri_cod_giro_consegna = '" + ls_valore + "'"
					end if
				case "cod_categoria"
					tab_1.tabpage_2.dw_6.object.dati.protect = true
					tab_1.tabpage_2.dw_7.insertrow(0)
					is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by clienti_cod_categoria"
					wf_carica_dropdown(tab_1.tabpage_2.dw_7)				
					tab_1.tabpage_2.dw_7.object.dati_t.text = tab_1.tabpage_2.st_8.text
					tab_1.tabpage_2.dw_7.visible = true
					tab_1.tabpage_2.dw_7.setfocus()	
					is_items[7] = "cod_categoria"
					il_count++
					tab_1.tabpage_2.st_8.italic = true
					tab_1.tabpage_2.st_8.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_7.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_7 = ""
					else
						is_where_7 = " and clienti_cod_categoria = '" + ls_valore + "'"
					end if	
				case "cod_deposito"
					tab_1.tabpage_2.dw_6.object.dati.protect = true
					tab_1.tabpage_2.dw_7.insertrow(0)
					is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by clienti_cod_categoria"
					wf_carica_dropdown(tab_1.tabpage_2.dw_7)				
					tab_1.tabpage_2.dw_7.object.dati_t.text = tab_1.tabpage_2.st_19.text
					tab_1.tabpage_2.dw_7.visible = true
					tab_1.tabpage_2.dw_7.setfocus()	
					is_items[7] = "cod_deposito"
					il_count++
					tab_1.tabpage_2.st_19.italic = true
					tab_1.tabpage_2.st_19.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_7.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_7 = ""
					else
						is_where_7 = " and cod_deposito = '" + ls_valore + "'"
					end if	
			end choose
			
		case 8
			
			tab_1.tabpage_2.dw_8.object.dati.protect = true
			
			choose case ls_campo
				
				case "cod_tipo_fat_ven"
					tab_1.tabpage_2.dw_7.object.dati.protect = true
					tab_1.tabpage_2.dw_8.insertrow(0)
					is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by tipi_fat_cod_tipo_fat_ven"
					wf_carica_dropdown(tab_1.tabpage_2.dw_8)				
					tab_1.tabpage_2.dw_8.object.dati_t.text = tab_1.tabpage_2.st_1.text
					tab_1.tabpage_2.dw_8.visible = true
					tab_1.tabpage_2.dw_8.setfocus()	
					is_items[8] = "cod_tipo_fat_ven"
					il_count++
					tab_1.tabpage_2.st_1.italic = true
					tab_1.tabpage_2.st_1.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_8.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_8 = ""
					else
						is_where_8 = " and tipi_fat_cod_tipo_fat_ven = '" + ls_valore + "'"
					end if	
				case "cod_cliente"
					tab_1.tabpage_2.dw_7.object.dati.protect = true
					tab_1.tabpage_2.dw_8.insertrow(0)
					is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata" + 	is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by clienti_cod_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_8)				
					tab_1.tabpage_2.dw_8.object.dati_t.text = tab_1.tabpage_2.st_2.text
					tab_1.tabpage_2.dw_8.visible = true
					tab_1.tabpage_2.dw_8.setfocus()	
					is_items[8] = "cod_cliente"
					il_count++
					tab_1.tabpage_2.st_2.italic = true
					tab_1.tabpage_2.st_2.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_8.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_8 = ""
					else
						is_where_8 = " and clienti_cod_cliente = '" + ls_valore + "'"
					end if	
				case "flag_tipo_cliente"
					tab_1.tabpage_2.dw_7.object.dati.protect = true
					tab_1.tabpage_2.dw_8.insertrow(0)
					is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by clienti_flag_tipo_cliente"
					wf_carica_dropdown(tab_1.tabpage_2.dw_8)				
					tab_1.tabpage_2.dw_8.object.dati_t.text = tab_1.tabpage_2.st_3.text
					tab_1.tabpage_2.dw_8.visible = true
					tab_1.tabpage_2.dw_8.setfocus()	
					is_items[8] = "flag_tipo_cliente"
					il_count++
					tab_1.tabpage_2.st_3.italic = true
					tab_1.tabpage_2.st_3.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_8.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_8 = ""
					else
						is_where_8 = " and clienti_flag_tipo_cliente = '" + ls_valore + "'"
					end if	
				case "provincia"
					tab_1.tabpage_2.dw_7.object.dati.protect = true
					tab_1.tabpage_2.dw_8.insertrow(0)
					is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by clienti_provincia"
					wf_carica_dropdown(tab_1.tabpage_2.dw_8)				
					tab_1.tabpage_2.dw_8.object.dati_t.text = tab_1.tabpage_2.st_4.text
					tab_1.tabpage_2.dw_8.visible = true
					tab_1.tabpage_2.dw_8.setfocus()	
					is_items[8] = "provincia"
					il_count++
					tab_1.tabpage_2.st_4.italic = true
					tab_1.tabpage_2.st_4.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_8.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_8 = ""
					else
						is_where_8 = " and clienti_provincia = '" + ls_valore + "'"
					end if	
				case "cod_agente_1"
					tab_1.tabpage_2.dw_7.object.dati.protect = true
					tab_1.tabpage_2.dw_8.insertrow(0)
					is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by clienti_cod_agente_1"
					wf_carica_dropdown(tab_1.tabpage_2.dw_8)				
					tab_1.tabpage_2.dw_8.object.dati_t.text = tab_1.tabpage_2.st_5.text
					tab_1.tabpage_2.dw_8.visible = true
					tab_1.tabpage_2.dw_8.setfocus()	
					is_items[8] = "cod_agente_1"
					il_count++
					tab_1.tabpage_2.st_5.italic = true
					tab_1.tabpage_2.st_5.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_8.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_8 = ""
					else
						is_where_8 = " and clienti_cod_agente_1 = '" + ls_valore + "'"
					end if	
				case "cod_zona"
					tab_1.tabpage_2.dw_7.object.dati.protect = true
					tab_1.tabpage_2.dw_8.insertrow(0)
					is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by clienti_cod_zona"
					wf_carica_dropdown(tab_1.tabpage_2.dw_8)				
					tab_1.tabpage_2.dw_8.object.dati_t.text = tab_1.tabpage_2.st_6.text
					tab_1.tabpage_2.dw_8.visible = true
					tab_1.tabpage_2.dw_8.setfocus()	
					is_items[8] = "cod_zona"
					il_count++
					tab_1.tabpage_2.st_6.italic = true
					tab_1.tabpage_2.st_6.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_8.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_8 = ""
					else
						is_where_8 = " and clienti_cod_zona = '" + ls_valore + "'"
					end if	
				case "cod_giro"
					tab_1.tabpage_2.dw_7.object.dati.protect = true
					tab_1.tabpage_2.dw_8.insertrow(0)
					is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by giri_cod_giro_consegna"
					wf_carica_dropdown(tab_1.tabpage_2.dw_8)				
					tab_1.tabpage_2.dw_8.object.dati_t.text = tab_1.tabpage_2.st_7.text
					tab_1.tabpage_2.dw_8.visible = true
					tab_1.tabpage_2.dw_8.setfocus()	
					is_items[8] = "cod_giro"
					il_count++
					tab_1.tabpage_2.st_7.italic = true
					tab_1.tabpage_2.st_7.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_8.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_8 = ""
					else
						is_where_8 = " and giri_cod_giro_consegna = '" + ls_valore + "'"
					end if
				case "cod_categoria"
					tab_1.tabpage_2.dw_7.object.dati.protect = true
					tab_1.tabpage_2.dw_8.insertrow(0)
					is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by clienti_cod_categoria"
					wf_carica_dropdown(tab_1.tabpage_2.dw_8)				
					tab_1.tabpage_2.dw_8.object.dati_t.text = tab_1.tabpage_2.st_8.text
					tab_1.tabpage_2.dw_8.visible = true
					tab_1.tabpage_2.dw_8.setfocus()	
					is_items[8] = "cod_categoria"
					il_count++
					tab_1.tabpage_2.st_8.italic = true
					tab_1.tabpage_2.st_8.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_8.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_8 = ""
					else
						is_where_8 = " and clienti_cod_categoria = '" + ls_valore + "'"
					end if	
				case "cod_deposito"
					tab_1.tabpage_2.dw_7.object.dati.protect = true
					tab_1.tabpage_2.dw_8.insertrow(0)
					is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata" + is_where_1 + is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by clienti_cod_categoria"
					wf_carica_dropdown(tab_1.tabpage_2.dw_8)				
					tab_1.tabpage_2.dw_8.object.dati_t.text = tab_1.tabpage_2.st_19.text
					tab_1.tabpage_2.dw_8.visible = true
					tab_1.tabpage_2.dw_8.setfocus()	
					is_items[8] = "cod_deposito"
					il_count++
					tab_1.tabpage_2.st_19.italic = true
					tab_1.tabpage_2.st_19.textcolor = rgb(0,0,255)
					tab_1.tabpage_2.dw_8.setitem(1,"dati",ls_valore)
					if isnull(ls_valore) then
						is_where_8 = ""
					else
						is_where_8 = " and cod_deposito = '" + ls_valore + "'"
					end if	
			end choose	
		
	end choose		
	
loop

if il_count > 0 and ib_consenti_modifica = true then
	cb_azzera_testata.enabled = true
end if

ib_testata_mod = false

close filtro_tes;

setpointer(arrow!)

return 0
end function

public function integer wf_carica_dropdown (ref datawindow fdw_1);string ls_select, ls_modify, ls_return

long   ll_pos, ll_start, ll_i, ll_pos_tipo_cliente, ll_pos_where

datawindowchild ldwc_1


fdw_1.getchild("dati",ldwc_1)

ldwc_1.settransobject(sqlca)

ls_select = is_select

ll_start = 1

do
	
	ll_pos = pos(ls_select,"'",ll_start)
	
	if ll_pos <> 0 then
		ls_select = replace(ls_select,ll_pos,1,"~~'")	
		ll_start = ll_pos + 2
	end if	
	
loop while ll_pos <> 0	

ls_modify = "DataWindow.Table.Select='" + ls_select + "'"

ls_return = ldwc_1.modify(ls_modify)

if ls_return <> "" then
	g_mb.messagebox("Errore","Errore nella modifica della select della dropdown datawindow: " + ls_return)
	return -1
end if

if ldwc_1.retrieve() = -1 then
	g_mb.messagebox("Errore","Errore nella retrieve della dropdown datawindow")
	return -1
end if

ll_pos_tipo_cliente = pos(ls_select,"tipo_cliente",1)

ll_pos_where = pos(ls_select,"where",1)

if ll_pos_tipo_cliente <= 0  or (ll_pos_where > 0 and ll_pos_tipo_cliente > ll_pos_where) then
	return 0
else
	
	for ll_i = 1 to ldwc_1.rowcount()
		
		choose case ldwc_1.getitemstring(ll_i,"display_column")
			case "C"
				ldwc_1.setitem(ll_i,"display_column","C.E.E.")
			case "E"
				ldwc_1.setitem(ll_i,"display_column","Estero")
			case "F"
				ldwc_1.setitem(ll_i,"display_column","Persona Fisica")
			case "P"
				ldwc_1.setitem(ll_i,"display_column","Privato")
			case "S"
				ldwc_1.setitem(ll_i,"display_column","Società")
		end choose		
		
	next
	
end if	

return 0
end function

public function integer wf_resetta_tutto ();il_count = 0

setnull(is_livello_1)

setnull(is_livello_2)

setnull(is_livello_3)

setnull(is_livello_4)

setnull(is_livello_5)

il_null = 0

il_livello = 0

il_prodotti = 0

il_par_aperte = 0

il_par_chiuse = 0

il_caratteristiche = 0

ib_testata_mod = false
	
ib_livelli_mod = false
		
ib_prodotti_mod = false

ib_caratteristiche_mod = false

ib_consenti_modifica = false

ib_background = false

ib_report = false

cb_azzera_testata.enabled = false

tab_1.tabpage_3.cb_superiore.enabled = false

cb_azzera_livelli.enabled = false

tab_1.tabpage_3.dw_prod_statistiche_livelli.enabled = false

tab_1.tabpage_4.dw_prod_statistiche_elenco.enabled = false

tab_1.tabpage_4.dw_prod_statistiche_selez.enabled = false

cb_azzera_prodotti.enabled = false

tab_1.tabpage_4.cbx_senza_livello.enabled = false

cb_azzera_caratteristiche.enabled = false

tab_1.tabpage_4.pb_aggiungi_tutti.enabled = false

tab_1.tabpage_4.pb_aggiungi_corrente.enabled = false

tab_1.tabpage_4.pb_togli_corrente.enabled = false

tab_1.tabpage_4.pb_togli_tutti.enabled = false

tab_1.tabpage_2.dw_1.object.dati.protect = true
tab_1.tabpage_2.dw_1.object.t_annulla.visible = false

tab_1.tabpage_2.dw_2.object.dati.protect = true
tab_1.tabpage_2.dw_2.object.t_annulla.visible = false

tab_1.tabpage_2.dw_3.object.dati.protect = true
tab_1.tabpage_2.dw_3.object.t_annulla.visible = false

tab_1.tabpage_2.dw_4.object.dati.protect = true
tab_1.tabpage_2.dw_4.object.t_annulla.visible = false

tab_1.tabpage_2.dw_5.object.dati.protect = true
tab_1.tabpage_2.dw_5.object.t_annulla.visible = false

tab_1.tabpage_2.dw_6.object.dati.protect = true
tab_1.tabpage_2.dw_6.object.t_annulla.visible = false

tab_1.tabpage_2.dw_7.object.dati.protect = true
tab_1.tabpage_2.dw_7.object.t_annulla.visible = false

tab_1.tabpage_2.dw_8.object.dati.protect = true
tab_1.tabpage_2.dw_8.object.t_annulla.visible = false

wf_blocca_caratteristiche()

tab_1.tabpage_5.dw_prod_statistiche_carat.reset()

cb_salva_filtri.enabled = false

cb_annulla_modifiche.enabled = false

if tab_1.tabpage_1.dw_tes_statistiche_lista.rowcount() > 0 then
	cb_modifica_filtri.enabled = true
	tab_1.tabpage_6.enabled = true
else	
	cb_modifica_filtri.enabled = false
end if

cb_selezione.triggerevent("clicked")

tab_1.selecttab(1)

return 0
end function

public function integer wf_salva_caratteristiche ();string ls_apri_par, ls_variante, ls_operatore_confronto, ls_tipo_variante, ls_valore, ls_chiudi_par,&
		 ls_operatore_logico, ls_cod_prodotto, ls_null

long   ll_i


tab_1.tabpage_5.dw_prod_statistiche_carat.accepttext()

if il_par_aperte <> il_par_chiuse then
	g_mb.messagebox("Salvataggio caratteristiche","Il numero di parentesi aperte e chiuse non corrisponde: controllare la sintassi!")
	return -1
end if

if isnull(tab_1.tabpage_5.dw_prod_statistiche_carat.getitemstring(tab_1.tabpage_5.dw_prod_statistiche_carat.rowcount(),"variante")) then
		
	if tab_1.tabpage_5.dw_prod_statistiche_carat.rowcount() > 1 then	
		setnull(ls_null)
		tab_1.tabpage_5.dw_prod_statistiche_carat.setitem(tab_1.tabpage_5.dw_prod_statistiche_carat.rowcount() - 1,"operatore_logico",ls_null)
	end if	
	tab_1.tabpage_5.dw_prod_statistiche_carat.deleterow(tab_1.tabpage_5.dw_prod_statistiche_carat.rowcount())
	il_caratteristiche = il_caratteristiche - 1
		
else
	
	if isnull(tab_1.tabpage_5.dw_prod_statistiche_carat.getitemstring(tab_1.tabpage_5.dw_prod_statistiche_carat.rowcount(),"variante")) then
		g_mb.messagebox("Salvataggio caratteristiche","Il campo variante non può essere vuoto")
		return -1
	end if
		
	if isnull(tab_1.tabpage_5.dw_prod_statistiche_carat.getitemstring(tab_1.tabpage_5.dw_prod_statistiche_carat.rowcount(),"operatore_confronto")) then
		g_mb.messagebox("Salvataggio caratteristiche","Il campo variante non può essere vuoto")
		return -1
	end if
		
	choose case tab_1.tabpage_5.dw_prod_statistiche_carat.getitemstring(tab_1.tabpage_5.dw_prod_statistiche_carat.rowcount(),"tipo_variante")
				
		case "S"
				
			if isnull(tab_1.tabpage_5.dw_prod_statistiche_carat.getitemstring(tab_1.tabpage_5.dw_prod_statistiche_carat.rowcount(),"valore_stringa")) then
				g_mb.messagebox("Salvataggio caratteristiche","Il campo valore non può essere vuoto")
				return -1
			end if
				
		case "N"
				
			if isnull(tab_1.tabpage_5.dw_prod_statistiche_carat.getitemnumber(tab_1.tabpage_5.dw_prod_statistiche_carat.rowcount(),"valore_numerico")) then
				g_mb.messagebox("Salvataggio caratteristiche","Il campo valore non può essere vuoto")
				return -1
			end if
				
	end choose
	
end if	

ls_cod_prodotto = tab_1.tabpage_4.dw_prod_statistiche_selez.getitemstring(1,"cod_prodotto")

delete
from   det_stat_filtro_varianti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_statistica = :is_codice_corrente and
		 cod_prodotto = :ls_cod_prodotto;
		 
if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella delete di det_stat_filtro_varianti: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

for ll_i = 1 to tab_1.tabpage_5.dw_prod_statistiche_carat.rowcount()
	
	ls_apri_par = tab_1.tabpage_5.dw_prod_statistiche_carat.getitemstring(ll_i,"apri_par")
	
	ls_variante = tab_1.tabpage_5.dw_prod_statistiche_carat.getitemstring(ll_i,"variante")
	
	choose case ls_variante
		case "Dimensioni X"
			ls_variante = "dim_x"
		case "Dimensioni Y"
			ls_variante = "dim_y"
		case "Tessuto telo"
			ls_variante = "cod_tessuto"
		case "Tessuto Mantovana"
			ls_variante = "cod_tessuto_mantovana"
		case "Verniciatura"
			ls_variante = "cod_verniciatura"
		case "Comando"
			ls_variante = "cod_comando"
		case "Comando 2"
			ls_variante = "cod_comando_2"
		case "Supporto"
			ls_variante = "cod_supporto"
	end choose
	
	if not isnull(ls_apri_par) then
		ls_variante = ls_apri_par + ls_variante
	end if
	
	ls_tipo_variante = tab_1.tabpage_5.dw_prod_statistiche_carat.getitemstring(ll_i,"tipo_variante")
	
	ls_operatore_confronto = tab_1.tabpage_5.dw_prod_statistiche_carat.getitemstring(ll_i,"operatore_confronto")
	
	choose case ls_tipo_variante
			
		case "N"
			ls_valore = string(tab_1.tabpage_5.dw_prod_statistiche_carat.getitemnumber(ll_i,"valore_numerico"))
		case "S"
			ls_valore = "'" + tab_1.tabpage_5.dw_prod_statistiche_carat.getitemstring(ll_i,"valore_stringa") + "'"
		case else
			setnull(ls_valore)
	
	end choose
	
	ls_chiudi_par = tab_1.tabpage_5.dw_prod_statistiche_carat.getitemstring(ll_i,"chiudi_par")
	
	if not isnull(ls_chiudi_par) then
		
		choose case ls_tipo_variante			
			case "N","S"
				ls_valore = ls_valore + ls_chiudi_par
			case else
				ls_operatore_confronto = ls_operatore_confronto + ls_chiudi_par
		end choose		
	end if
	
	ls_operatore_logico = tab_1.tabpage_5.dw_prod_statistiche_carat.getitemstring(ll_i,"operatore_logico")
	
	insert
	into   det_stat_filtro_varianti
			 (cod_azienda,
			 cod_statistica,
			 cod_prodotto,
			 progressivo,
			 variante,
			 operatore_confronto,
			 valore,
			 operatore_logico)
	values (:s_cs_xx.cod_azienda,
			 :is_codice_corrente,
			 :ls_cod_prodotto,
			 :ll_i,
			 :ls_variante,
			 :ls_operatore_confronto,
			 :ls_valore,
			 :ls_operatore_logico);
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella insert di det_stat_filtro_varianti: " + sqlca.sqlerrtext)
		rollback;
		return -1
	end if			 
	
next

commit;

ib_caratteristiche_mod = false

return 0
end function

public function integer wf_leggi_filtri ();string ls_nome_campo, ls_valore_campo, ls_variante, ls_operatore_confronto, ls_valore, ls_operatore_logico, &
		 ls_cod_prodotto, ls_cliente, ls_des
		 
long   ll_count, ll_count_2, ll_count_testata, ll_count_prodotti, ll_count_varianti, ll_progress, ll_i


il_prod_report = 0

il_where_prod = 0

for ll_i = 1 to upperbound(is_prodotti)
	setnull(is_prodotti[ll_i])
next

for ll_i = 1 to upperbound(is_where_prod)
	setnull(is_where_prod[ll_i])
next

ib_note_credito = true

is_where_tes = ""

is_filtri_report = ""

tab_1.tabpage_6.st_elaborazione.text = "Lettura filtri di testata"

select count(*)
into   :ll_count
from   det_statistiche_filtro_tes
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_statistica = :is_cod_statistica;
		 
if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di det_statistiche_filtro_tes: " + sqlca.sqlerrtext)
	setpointer(arrow!)
	tab_1.tabpage_6.st_elaborazione.text = ""
	tab_1.tabpage_6.hpb_1.position = 0		
	tab_1.tabpage_6.st_perc.text = "0%"
	cb_report.enabled = true
	cb_interrompi.enabled = false
	return -1
end if

if ll_count > 0 then

	ll_count_testata = 0

	declare testata cursor for
	select nome_campo,
			 valore_campo
	from   det_statistiche_filtro_tes
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_statistica = :is_cod_statistica
	order by prog_ordinamento;

	open testata;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella open del cursore testata: " + sqlca.sqlerrtext)
		setpointer(arrow!)
		tab_1.tabpage_6.st_elaborazione.text = ""
		tab_1.tabpage_6.hpb_1.position = 0		
		tab_1.tabpage_6.st_perc.text = "0%"
		cb_report.enabled = true
		cb_interrompi.enabled = false
		return -1
	end if

	do while true
		
		yield()
		
		if ib_interrompi then
			close testata;
			setpointer(arrow!)			
			tab_1.tabpage_6.st_elaborazione.text = ""
			tab_1.tabpage_6.hpb_1.position = 0		
			tab_1.tabpage_6.st_perc.text = "0%"
			cb_selezione.postevent("clicked")
			cb_report.enabled = true
			cb_interrompi.enabled = false
			return 0
		end if
	
		fetch testata
		into  :ls_nome_campo,
				:ls_valore_campo;
			
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore nella fetch del cursore testata: " + sqlca.sqlerrtext)
			close testata;
			setpointer(arrow!)
			tab_1.tabpage_6.st_elaborazione.text = ""
			tab_1.tabpage_6.hpb_1.position = 0		
			tab_1.tabpage_6.st_perc.text = "0%"
			cb_report.enabled = true
			cb_interrompi.enabled = false
			return -1
		elseif sqlca.sqlcode = 100 then
			close testata;
			exit
		end if
		
		if isnull(ls_valore_campo) or len(ls_valore_campo) < 1 then
			continue
		end if	
	
		ll_count_testata++
		
		ll_progress = round( (ll_count_testata * 1000 / ll_count) , 0 )
		
		tab_1.tabpage_6.hpb_1.position = ll_progress
		
		ll_progress = round( ll_progress / 10 , 0 )
		
		tab_1.tabpage_6.st_perc.text = string(ll_progress) + "%"
		
		if ll_count_testata > 1 then
			is_filtri_report = is_filtri_report + ", "
		else
			is_filtri_report = ""
		end if	
		
		choose case ls_nome_campo
				
			case "cod_tipo_fat_ven"
				
				select des_tipo_fat_ven
				into   :ls_des
				from   tab_tipi_fat_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_fat_ven = :ls_valore_campo;
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore nella select di tab_tipi_fat_ven: " + sqlca.sqlerrtext)
					close testata;
					setpointer(arrow!)
					tab_1.tabpage_6.st_elaborazione.text = ""
					tab_1.tabpage_6.hpb_1.position = 0		
					tab_1.tabpage_6.st_perc.text = "0%"
					cb_report.enabled = true
					cb_interrompi.enabled = false
					return -1
				end if
				
				is_filtri_report = is_filtri_report + "Tipo fattura = '" + ls_des + "'"
				
			case "cod_cliente"
				
				select rag_soc_1
				into   :ls_des
				from   anag_clienti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_cliente = :ls_valore_campo;
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore nella select di anag_clienti: " + sqlca.sqlerrtext)
					close testata;
					setpointer(arrow!)
					tab_1.tabpage_6.st_elaborazione.text = ""
					tab_1.tabpage_6.hpb_1.position = 0		
					tab_1.tabpage_6.st_perc.text = "0%"
					cb_report.enabled = true
					cb_interrompi.enabled = false
					return -1
				end if
				
				is_filtri_report = is_filtri_report + "Cliente = '" + ls_des + "'"
				
			case "flag_tipo_cliente"
				
				is_filtri_report = is_filtri_report + "Tipo cliente = '" + ls_valore_campo + "'"
				
			case "provincia"
				
				is_filtri_report = is_filtri_report + "Provincia = '" + ls_valore_campo + "'"
				
			case "cod_agente_1"
				
				select rag_soc_1
				into   :ls_des
				from   anag_agenti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_agente = :ls_valore_campo;
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore nella select di anag_agenti: " + sqlca.sqlerrtext)
					close testata;
					setpointer(arrow!)
					tab_1.tabpage_6.st_elaborazione.text = ""
					tab_1.tabpage_6.hpb_1.position = 0		
					tab_1.tabpage_6.st_perc.text = "0%"
					cb_report.enabled = true
					cb_interrompi.enabled = false
					return -1
				end if
				
				is_filtri_report = is_filtri_report + "Agente = '" + ls_des + "'"
				
			case "cod_zona"
				
				select des_zona
				into   :ls_des
				from   tab_zone
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_zona = :ls_valore_campo;
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore nella select di tab_zone: " + sqlca.sqlerrtext)
					close testata;
					setpointer(arrow!)
					tab_1.tabpage_6.st_elaborazione.text = ""
					tab_1.tabpage_6.hpb_1.position = 0		
					tab_1.tabpage_6.st_perc.text = "0%"
					cb_report.enabled = true
					cb_interrompi.enabled = false
					return -1
				end if
				
				is_filtri_report = is_filtri_report + "Zona = '" + ls_des + "'"
				
			case "cod_giro"
				
				select des_giro_consegna
				into   :ls_des
				from   tes_giri_consegne
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_giro_consegna = :ls_valore_campo;
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore nella select di tes_giri_consegne: " + sqlca.sqlerrtext)
					close testata;
					setpointer(arrow!)
					tab_1.tabpage_6.st_elaborazione.text = ""
					tab_1.tabpage_6.hpb_1.position = 0		
					tab_1.tabpage_6.st_perc.text = "0%"
					cb_report.enabled = true
					cb_interrompi.enabled = false
					return -1
				end if
				
				is_filtri_report = is_filtri_report + "Giro = '" + ls_des + "'"
				
			case "cod_categoria"
				
				select des_categoria
				into   :ls_des
				from   tab_categorie
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_categoria = :ls_valore_campo;
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore nella select di tab_categorie: " + sqlca.sqlerrtext)
					close testata;
					setpointer(arrow!)
					tab_1.tabpage_6.st_elaborazione.text = ""
					tab_1.tabpage_6.hpb_1.position = 0		
					tab_1.tabpage_6.st_perc.text = "0%"
					cb_report.enabled = true
					cb_interrompi.enabled = false
					return -1
				end if
				
				is_filtri_report = is_filtri_report + "Categoria cliente = '" + ls_des + "'"	
				
		end choose
		
		if ls_nome_campo = "cod_tipo_fat_ven" then
			ib_note_credito = false
		end if	
		
		if ls_nome_campo = "cod_giro" then
			
			declare giro cursor for
			select distinct(cod_cliente)
			from   det_giri_consegne
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_giro_consegna = :ls_valore_campo;
					 
			open giro;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore nella open del cursore giro: " + sqlca.sqlerrtext)
				close testata;
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
			
			ll_i = 0
			
			do while true
				
				fetch giro
				into  :ls_cliente;
				
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("APICE","Errore nella fetch del cursore giro: " + sqlca.sqlerrtext)
					close testata;
					close giro;
					setpointer(arrow!)
					tab_1.tabpage_6.st_elaborazione.text = ""
					tab_1.tabpage_6.hpb_1.position = 0		
					tab_1.tabpage_6.st_perc.text = "0%"
					cb_report.enabled = true
					cb_interrompi.enabled = false
					return -1
				elseif sqlca.sqlcode = 100 then
					close giro;
					exit
				end if
				
				ll_i++
			
				if ll_i = 1 then
					is_where_tes = is_where_tes + "and cod_cliente in("
				else	
					is_where_tes = is_where_tes + ","
				end if
				
				is_where_tes = is_where_tes + "'" + ls_cliente + "'"
				
			loop
			
			if ll_i > 0 then
				is_where_tes = is_where_tes + ")"
			end if	
				
		else
	
			is_where_tes = is_where_tes + "and " + ls_nome_campo + " = '" + ls_valore_campo + "' "
			
		end if	

	loop
	
end if

tab_1.tabpage_6.st_elaborazione.text = ""

tab_1.tabpage_6.hpb_1.position = 0
		
tab_1.tabpage_6.st_perc.text = "0%"

select count(*)
into   :ll_count
from   det_statistiche_filtro_prod
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_statistica = :is_cod_statistica;
		 
if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella select di det_statistiche_filtro_prod: " + sqlca.sqlerrtext)
	setpointer(arrow!)
	tab_1.tabpage_6.st_elaborazione.text = ""
	tab_1.tabpage_6.hpb_1.position = 0		
	tab_1.tabpage_6.st_perc.text = "0%"
	cb_report.enabled = true
	cb_interrompi.enabled = false
	return -1
end if

if ll_count > 0 then

	declare prodotti cursor for
	select cod_prodotto
	from   det_statistiche_filtro_prod
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_statistica = :is_cod_statistica
	order by cod_prodotto;

	open prodotti;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella open del cursore prodotti: " + sqlca.sqlerrtext)
		setpointer(arrow!)
		tab_1.tabpage_6.st_elaborazione.text = ""
		tab_1.tabpage_6.hpb_1.position = 0		
		tab_1.tabpage_6.st_perc.text = "0%"
		cb_report.enabled = true
		cb_interrompi.enabled = false
		return -1
	end if

	ll_count_prodotti = 0
	
	ll_i = 0

	do while true
		
		yield()
		
		if ib_interrompi then
			close prodotti;
			setpointer(arrow!)
			tab_1.tabpage_6.st_elaborazione.text = ""
			tab_1.tabpage_6.hpb_1.position = 0		
			tab_1.tabpage_6.st_perc.text = "0%"
			cb_selezione.postevent("clicked")
			cb_report.enabled = true
			cb_interrompi.enabled = false
			return 0
		end if
	
		tab_1.tabpage_6.st_elaborazione.text = "Lettura filtri di prodotto"
		
		fetch prodotti
		into  :ls_cod_prodotto;
			
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore nella fetch del cursore prodotti: " + sqlca.sqlerrtext)
			close prodotti;
			setpointer(arrow!)
			tab_1.tabpage_6.st_elaborazione.text = ""
			tab_1.tabpage_6.hpb_1.position = 0		
			tab_1.tabpage_6.st_perc.text = "0%"
			cb_report.enabled = true
			cb_interrompi.enabled = false
			return -1
		elseif sqlca.sqlcode = 100 then
			close prodotti;
			exit
		end if
	
		ll_count_prodotti++
		
		il_prod_report++
		
		ll_progress = round( (ll_count_prodotti * 1000 / ll_count) , 0 )
		
		tab_1.tabpage_6.hpb_1.position = ll_progress
		
		ll_progress = round( ll_progress / 10 , 0 )
		
		tab_1.tabpage_6.st_perc.text = string(ll_progress) + "%"
	
		is_prodotti[ll_count_prodotti] = ls_cod_prodotto
		
		if mod(ll_count_prodotti,100) = 1 then
			if ll_i > 0 then
				is_where_prod[ll_i] = is_where_prod[ll_i] + ") group by cod_prodotto"
			end if			
			ll_i++
			il_where_prod++
			is_where_prod[ll_i] = "and ( ("
		else	
			is_where_prod[ll_i] = is_where_prod[ll_i] + "or ("
		end if			
			
		is_where_prod[ll_i] = is_where_prod[ll_i] + "cod_prodotto = '" + ls_cod_prodotto + "'"		
	
		select count(*)
		into   :ll_count_2
		from   det_stat_filtro_varianti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_statistica = :is_cod_statistica and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore nella select di det_stat_filtro_varianti: " + sqlca.sqlerrtext)
			close prodotti;
			setpointer(arrow!)
			tab_1.tabpage_6.st_elaborazione.text = ""
			tab_1.tabpage_6.hpb_1.position = 0		
			tab_1.tabpage_6.st_perc.text = "0%"
			cb_report.enabled = true
			cb_interrompi.enabled = false
			return -1
		end if
		
		if ll_count_2 > 0 then
	
			declare caratteristiche cursor for
			select variante,
					 operatore_confronto,
					 valore,
					 operatore_logico
			from   det_stat_filtro_varianti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_statistica = :is_cod_statistica and
					 cod_prodotto = :ls_cod_prodotto
			order by progressivo;

			open caratteristiche;

			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore nella open del cursore caratteristiche: " + sqlca.sqlerrtext)
				close prodotti;
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
	
			ll_count_varianti = 0
	
			tab_1.tabpage_6.st_elaborazione.text = "Lettura caratteristiche per il prodotto " + ls_cod_prodotto
	
			do while true
	
				fetch caratteristiche
				into  :ls_variante,
						:ls_operatore_confronto,
						:ls_valore,
						:ls_operatore_logico;
				
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("APICE","Errore nella fetch del cursore caratteristiche: " + sqlca.sqlerrtext)
					close prodotti;
					close caratteristiche;
					setpointer(arrow!)
					tab_1.tabpage_6.st_elaborazione.text = ""
					tab_1.tabpage_6.hpb_1.position = 0		
					tab_1.tabpage_6.st_perc.text = "0%"
					cb_report.enabled = true
					cb_interrompi.enabled = false
					return -1
				elseif sqlca.sqlcode = 100 then
					close caratteristiche;
					exit
				end if
			
				ll_count_varianti++				
			
				if ll_count_varianti = 1 then			
					is_where_prod[ll_i] = is_where_prod[ll_i] + " and ("		
				end if
		
				is_where_prod[ll_i] = is_where_prod[ll_i] + ls_variante
			
				if ls_operatore_confronto = "null" then
					ls_operatore_confronto = " is null"
					is_where_prod[ll_i] = is_where_prod[ll_i] + ls_operatore_confronto
				elseif ls_operatore_confronto = "!null" then
					ls_operatore_confronto = " is not null"
					is_where_prod[ll_i] = is_where_prod[ll_i] + ls_operatore_confronto
				else
					ls_operatore_confronto = " " + ls_operatore_confronto + " "
					is_where_prod[ll_i] = is_where_prod[ll_i] + ls_operatore_confronto + ls_valore
				end if
			
				if not isnull(ls_operatore_logico) then
					ls_operatore_logico = " " + ls_operatore_logico + " "
					is_where_prod[ll_i] = is_where_prod[ll_i] + ls_operatore_logico
				end if				

			loop
			
		end if	
	
		if ll_count_varianti > 0 then
			is_where_prod[ll_i] = is_where_prod[ll_i] + ") "
		end if
	
		is_where_prod[ll_i] = is_where_prod[ll_i] + ") "
	
	loop
	
end if

if ll_count_prodotti > 0 then
	is_where_prod[ll_i] = is_where_prod[ll_i] + ") group by cod_prodotto "
end if

return 0
end function

public function integer wf_carica_caratteristiche ();string ls_cod_prodotto, ls_variante, ls_operatore_confronto, ls_valore, ls_operatore_logico, ls_tipo_variante, &
		 ls_des_prodotto

long 	 ll_riga, ll_pos


setpointer(hourglass!)

il_par_aperte = 0

il_par_chiuse = 0

ls_cod_prodotto = tab_1.tabpage_4.dw_prod_statistiche_selez.getitemstring(1,"cod_prodotto")
		
ls_des_prodotto = tab_1.tabpage_4.dw_prod_statistiche_selez.getitemstring(1,"des_prodotto")		
		
if not isnull(ls_des_prodotto) then		
	tab_1.tabpage_5.st_prodotto.text = tab_1.tabpage_5.st_prodotto.text + " - " + ls_des_prodotto
end if
		
tab_1.tabpage_5.st_prodotto.visible = true

declare caratteristiche cursor for
select 	variante,
		 	operatore_confronto,
		 	valore,
		 	operatore_logico			 
from   	det_stat_filtro_varianti
where  	cod_azienda = :s_cs_xx.cod_azienda and
		 	cod_statistica = :is_codice_corrente and
		 	cod_prodotto = :ls_cod_prodotto
order by progressivo;

open caratteristiche;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella open del cursore caratteristiche: " + sqlca.sqlerrtext)
	setpointer(arrow!)
	return -1
end if

do while true
	
	fetch caratteristiche
	into  :ls_variante,
			:ls_operatore_confronto,
			:ls_valore,
			:ls_operatore_logico;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore nella fetch del cursore caratteristiche: " + sqlca.sqlerrtext)
		close caratteristiche;
		setpointer(arrow!)
		return -1
	elseif sqlca.sqlcode = 100 then
		close caratteristiche;
		setpointer(arrow!)
		return ll_riga
	end if
	
	ll_riga = tab_1.tabpage_5.dw_prod_statistiche_carat.insertrow(0)
	
	if pos(ls_variante,"(",1) <> 0 then
		
		il_par_aperte++
		
		ls_variante = mid(ls_variante,2)
		
		choose case ls_variante
			case "dim_x"
				ls_variante = "Dimensioni X"
			case "dim_y"
				ls_variante = "Dimensioni Y"
			case "cod_tessuto"
				ls_variante = "Tessuto telo"
			case "cod_tessuto_mantovana"
				ls_variante = "Tessuto Mantovana"
			case "cod_verniciatura"
				ls_variante = "Verniciatura"
			case "cod_comando"
				ls_variante = "Comando"
			case "cod_comando_2"
				ls_variante = "Comando 2"
			case "cod_supporto"
				ls_variante = "Supporto"
		end choose
		
		tab_1.tabpage_5.dw_prod_statistiche_carat.setitem(ll_riga,"variante",ls_variante)
		tab_1.tabpage_5.dw_prod_statistiche_carat.setitem(ll_riga,"apri_par","(")
		
	else
		
		choose case ls_variante
			case "dim_x"
				ls_variante = "Dimensioni X"
			case "dim_y"
				ls_variante = "Dimensioni Y"
			case "cod_tessuto"
				ls_variante = "Tessuto telo"
			case "cod_tessuto_mantovana"
				ls_variante = "Tessuto Mantovana"
			case "cod_verniciatura"
				ls_variante = "Verniciatura"
			case "cod_comando"
				ls_variante = "Comando"
			case "cod_comando_2"
				ls_variante = "Comando 2"
			case "cod_supporto"
				ls_variante = "Supporto"
		end choose
		
		tab_1.tabpage_5.dw_prod_statistiche_carat.setitem(ll_riga,"variante",ls_variante)
		
	end if
	
	choose case ls_variante			
		case "Dimensioni X","Dimensioni Y"
			ls_tipo_variante = "N"
		case else
			ls_tipo_variante = "S"			
	end choose
	
	if pos(ls_operatore_confronto,")",1) <> 0 then			
		il_par_chiuse++		
		ls_operatore_confronto = mid(ls_operatore_confronto,1,len(ls_operatore_confronto) - 1)		
		tab_1.tabpage_5.dw_prod_statistiche_carat.setitem(ll_riga,"chiudi_par",")")		
	end if
	
	choose case ls_operatore_confronto
		case "null","!null"
			ls_tipo_variante = "I"
		case "like"
			ls_tipo_variante = "S"
	end choose	
	
	tab_1.tabpage_5.dw_prod_statistiche_carat.setitem(ll_riga,"operatore_confronto",ls_operatore_confronto)
	
	if ls_tipo_variante <> "I" then
	
		ll_pos = 1
	
		do
		
			ll_pos = pos(ls_valore,"'",ll_pos)
		
			if ll_pos > 0 then
				ls_valore = replace(ls_valore,ll_pos,1,"")
			end if
		
		loop while ll_pos > 0
	
		if pos(ls_valore,")",1) <> 0 then			
			il_par_chiuse++		
			ls_valore = mid(ls_valore,1,len(ls_valore) - 1)		
			tab_1.tabpage_5.dw_prod_statistiche_carat.setitem(ll_riga,"chiudi_par",")")
		end if
		
		choose case ls_tipo_variante
			case "S"
				tab_1.tabpage_5.dw_prod_statistiche_carat.setitem(ll_riga,"valore_stringa",ls_valore)
			case "N"
				tab_1.tabpage_5.dw_prod_statistiche_carat.setitem(ll_riga,"valore_numerico",dec(ls_valore))
		end choose
		
	end if	
	
	tab_1.tabpage_5.dw_prod_statistiche_carat.setitem(ll_riga,"operatore_logico",ls_operatore_logico)
	
	tab_1.tabpage_5.dw_prod_statistiche_carat.setitem(ll_riga,"tipo_variante",ls_tipo_variante)
	
loop
end function

public function integer wf_report ();string   ls_des_prodotto, ls_des_statistica, ls_tipo, ls_periodo, ls_cod_prodotto, ls_select, ls_select_prec, &
			ls_where, ls_where_prec, ls_stringa_periodo, ls_prod_letto, ls_where_fatturato, &
			ls_contenuto_periodo, ls_where_note_credito
			
dec{4}	ld_quantita, ld_fatturato, ld_quan_prec, ld_fatt_prec, ld_diff_quan, ld_perc_quan, ld_diff_fatt, &
			ld_perc_fatt, ld_quan_neg, ld_fatt_neg

long     ll_riga, ll_i, ll_num_periodi,  ll_anno, ll_mese, ll_giorno, ll_j, ll_progress, ll_return, &
			ll_pos, ll_start, ll_end, ll_where_prod, ll_old_row
			
boolean  lb_trovato

datetime ldt_data_da[], ldt_data_a[], ldt_data_prec_da[], ldt_data_prec_a[], ldt_data_inizio, ldt_oggi


setpointer(hourglass!)
tab_1.tabpage_6.st_elaborazione.text = ""
tab_1.tabpage_6.hpb_1.position = 0		
tab_1.tabpage_6.st_perc.text = "0%"
tab_1.tabpage_6.dw_report_statistiche_selez.accepttext()
cb_report.enabled = false
ib_interrompi = false
cb_interrompi.enabled = true
is_cod_statistica = tab_1.tabpage_6.dw_report_statistiche_selez.getitemstring(1,"cod_statistica")

if isnull(is_cod_statistica) then
	g_mb.messagebox("APICE","Selezionare una statistica dall'elenco prima di eseguire l'elaborazione!")
	setpointer(arrow!)
	cb_report.enabled = true
	cb_interrompi.enabled = false
	return -1
end if

ls_tipo = tab_1.tabpage_6.dw_report_statistiche_selez.getitemstring(1,"tipo_periodicita")

if isnull(ls_tipo) then
	g_mb.messagebox("APICE","Selezionare il tipo di periodicità da utilizzare nel report!")
	setpointer(arrow!)
	cb_report.enabled = true
	cb_interrompi.enabled = false
	return -1
end if

choose case ls_tipo
		
	case "F"

		ldt_data_da[1] = tab_1.tabpage_6.dw_report_statistiche_selez.getitemdatetime(1,"da_data")
		ldt_data_a[1] = tab_1.tabpage_6.dw_report_statistiche_selez.getitemdatetime(1,"a_data")		

		if isnull(ldt_data_da) or isnull(ldt_data_a) then
			g_mb.messagebox("APICE","Selezionare entrambe le date prima di eseguire l'elaborazione!")
			setpointer(arrow!)
			cb_report.enabled = true
			cb_interrompi.enabled = false
			return -1
		end if
		
		ls_stringa_periodo = "Definito da utente: "
		
	case "V"		
		
		ll_num_periodi = tab_1.tabpage_6.dw_report_statistiche_selez.getitemnumber(1,"periodi")
		
		if isnull(ll_num_periodi) then
			g_mb.messagebox("APICE","Selezionare il numero di periodi!")
			setpointer(arrow!)
			cb_report.enabled = true
			cb_interrompi.enabled = false
			return -1
		end if
		
		if ll_num_periodi < 1 then
			g_mb.messagebox("APICE","Il numero di periodi non può essere minore di 1!")
			setpointer(arrow!)
			cb_report.enabled = true
			cb_interrompi.enabled = false
			return -1
		end if
		
		ls_periodo = tab_1.tabpage_6.dw_report_statistiche_selez.getitemstring(1,"periodicita")
		
		if isnull(ls_periodo) then
			g_mb.messagebox("APICE","Selezionare la durata dei singoli periodi!")
			setpointer(arrow!)
			cb_report.enabled = true
			cb_interrompi.enabled = false
			return -1
		end if
		
		if ls_periodo = 'S' then
			
			ldt_data_inizio = tab_1.tabpage_6.dw_report_statistiche_selez.getitemdatetime(1,"data_inizio")
		
			if isnull(ldt_data_inizio) then
				g_mb.messagebox("APICE","Selezionare la data di partenza prima di eseguire l'elaborazione!")
				setpointer(arrow!)
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
			
			ll_anno = year(date(ldt_data_inizio))			
			ll_mese = month(date(ldt_data_inizio))			
			ll_giorno = day(date(ldt_data_inizio))			
			wf_calcola_periodi_sett(ldt_data_da[],ldt_data_a[],ll_anno,ll_mese,ll_giorno,ll_num_periodi)			
			ls_stringa_periodo = "Settimana n."
			
		else
			
			ll_anno = tab_1.tabpage_6.dw_report_statistiche_selez.getitemnumber(1,"anno_inizio")
		
			if isnull(ll_anno) then
				g_mb.messagebox("APICE","Selezionare l'anno di partenza prima di eseguire l'elaborazione!")
				setpointer(arrow!)
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
			
			if ll_anno < 1000 or ll_anno > 3000 then
				g_mb.messagebox("APICE","Selezionare un anno compreso fra 1000 e 3000!")
				setpointer(arrow!)
				return -1
				cb_report.enabled = true
				cb_interrompi.enabled = false
			end if
			
			if ls_periodo = '12' then
				
				ll_mese = 1
				
			else	
			
				ll_mese = tab_1.tabpage_6.dw_report_statistiche_selez.getitemnumber(1,"mese_inizio")
		
				if isnull(ll_mese) then
					g_mb.messagebox("APICE","Selezionare il mese di partenza prima di eseguire l'elaborazione!")
					setpointer(arrow!)
					cb_report.enabled = true
					cb_interrompi.enabled = false
					return -1
				end if
				
			end if	
			
			wf_calcola_periodi_mesi(ldt_data_da[],ldt_data_a[],ll_anno,ll_mese,ls_periodo,ll_num_periodi)
			
			choose case ls_periodo
				case "1"
					ls_stringa_periodo = "Mese n."
				case "3"
					ls_stringa_periodo = "Trimestre n."
				case "6"
					ls_stringa_periodo = "Semestre n."
				case "12"
					ls_stringa_periodo = "Anno n."
			end choose					
			
		end if
		
end choose

for ll_i = 1 to upperbound(ldt_data_da)	
	ll_giorno = day(date(ldt_data_da[ll_i]))	
	ll_mese = month(date(ldt_data_da[ll_i]))	
	ll_anno = year(date(ldt_data_da[ll_i]))	
	ll_anno = ll_anno - 1	
	ldt_data_prec_da[ll_i] = datetime(date( string(ll_giorno) + "/" + string(ll_mese) + "/" + string(ll_anno) ))	
	ll_giorno = day(date(ldt_data_a[ll_i]))	
	ll_mese = month(date(ldt_data_a[ll_i]))	
	ll_anno = year(date(ldt_data_a[ll_i]))	
	ll_anno = ll_anno - 1	
	ldt_data_prec_a[ll_i] = datetime(date( string(ll_giorno) + "/" + string(ll_mese) + "/" + string(ll_anno) ))		
next

select des_statistica
into   :ls_des_statistica
from   tes_statistiche
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_statistica = :is_cod_statistica;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di tes_statistiche: " + sqlca.sqlerrtext)
	setpointer(arrow!)
	tab_1.tabpage_6.st_elaborazione.text = ""
	tab_1.tabpage_6.hpb_1.position = 0		
	tab_1.tabpage_6.st_perc.text = "0%"
	cb_report.enabled = true
	cb_interrompi.enabled = false
	return -1
end if

if wf_leggi_filtri() = -1 then
	return -1
end if

if ib_note_credito then
	ls_where_note_credito = " and (flag_tipo_det_ven = 'S' or cod_tipo_fat_ven in (select cod_tipo_fat_ven from tab_tipi_fat_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_fat_ven = 'N')) "
else
	ls_where_note_credito = " and flag_tipo_det_ven = 'S' "
end if

tab_1.tabpage_6.st_elaborazione.text = ""
tab_1.tabpage_6.hpb_1.position = 0		
tab_1.tabpage_6.st_perc.text = "0%"
tab_1.tabpage_6.dw_report_statistiche.reset()
tab_1.tabpage_6.dw_report_statistiche.setredraw(false)
tab_1.tabpage_6.dw_report_statistiche.object.datawindow.print.preview = 'Yes'
tab_1.tabpage_6.dw_report_statistiche.object.datawindow.print.preview.rulers = 'Yes'
tab_1.tabpage_6.dw_report_statistiche.object.t_statistica.text = is_cod_statistica + "  " + ls_des_statistica

ll_start = 1

do
	
	ll_pos = pos(is_filtri_report,"~"",ll_start)
	
	if ll_pos <> 0 then
		is_filtri_report = replace(is_filtri_report,ll_pos,1,"~~~"")	
		ll_start = ll_pos + 2
	end if	
	
loop while ll_pos <> 0

tab_1.tabpage_6.dw_report_statistiche.object.t_filtri.text = is_filtri_report

if il_prod_report > 0 then
	tab_1.tabpage_6.dw_retrieve.dataobject = "d_ds_report_prod"
	tab_1.tabpage_6.dw_retrieve_prec.dataobject = "d_ds_report_prod"
	tab_1.tabpage_6.dw_note.dataobject = "d_ds_report_prod"
	tab_1.tabpage_6.dw_note_prec.dataobject = "d_ds_report_prod"
else
	tab_1.tabpage_6.dw_retrieve.dataobject = "d_ds_report"
	tab_1.tabpage_6.dw_retrieve_prec.dataobject = "d_ds_report"
	tab_1.tabpage_6.dw_note.dataobject = "d_ds_report"
	tab_1.tabpage_6.dw_note_prec.dataobject = "d_ds_report"
end if


for ll_i = 1 to upperbound(ldt_data_da)
	
	yield()
		
	if ib_interrompi then
		setpointer(arrow!)
		tab_1.tabpage_6.st_elaborazione.text = ""
		tab_1.tabpage_6.hpb_1.position = 0		
		tab_1.tabpage_6.st_perc.text = "0%"
		cb_selezione.postevent("clicked")
		cb_report.enabled = true
		cb_interrompi.enabled = false
		return 0
	end if
	
	tab_1.tabpage_6.st_elaborazione.text = "Periodo " + string(ll_i) + "/" + string(upperbound(ldt_data_da)) + &
														" - calcolo date e impostazione select"	
	
	ll_riga = tab_1.tabpage_6.dw_report_statistiche.insertrow(0)		
	tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"tipo_riga","I")
	
	if ls_tipo = 'F' then		
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"periodo","Periodo definito da utente: " + string(date(ldt_data_da[ll_i])) + " - " + string(date(ldt_data_a[ll_i])))		
	else			
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"periodo",ls_stringa_periodo + string(ll_i) + ": " + string(date(ldt_data_da[ll_i])) + " - " + string(date(ldt_data_a[ll_i])))
	end if
	
	if il_prod_report > 0 then		
		ll_riga = tab_1.tabpage_6.dw_report_statistiche.insertrow(0)		
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"tipo_riga","E")		
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_1","Prodotto")		
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_2","Quan. analisi")	
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_3","Quan. precedente")	
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_4","Quan. differenza")	
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_5","%")	
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_6","Fatt. analisi")	
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_7","Fatt. precedente")	
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_8","Fatt. differenza")	
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_9","%")		
	else		
		ll_riga = tab_1.tabpage_6.dw_report_statistiche.insertrow(0)
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"tipo_riga","E")		
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_1","Tipo dettaglio di vendita")		
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_2","Quan. analisi")	
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_3","Quan. precedente")	
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_4","Quan. differenza")
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_5","%")	
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_6","Fatt. analisi")
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_7","Fatt. precedente")
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_8","Fatt. differenza")	
		tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"etichetta_9","%")	
	end if	
	
	if il_prod_report > 0 then
		
		ls_select = "select distinct(cod_prodotto), sum(quan_fatturata) as quantita, " + &
						"sum(round((quan_fatturata * prezzo_vendita) / 100 * (100 - sconto_1) / 100 * (100 - sconto_2) / 100 * (100 - sconto_tes) / 100 * (100 - sconto_pagamento),2)) as fatturato " + &
	   	         "from view_dati_prodotti "
		
		ls_select_prec = ls_select
	
	else
	
		ls_select = "select sum(quan_fatturata) as quantita, " + &
						"sum(round((quan_fatturata * prezzo_vendita) / 100 * (100 - sconto_1) / 100 * (100 - sconto_2) / 100 * (100 - sconto_tes) / 100 * (100 - sconto_pagamento),2)) as fatturato " + &
	   	         "from view_dati_prodotti "
						
		ls_select_prec = ls_select
	
	end if

	ls_where = "where cod_azienda = '" + s_cs_xx.cod_azienda + "'" + &
	           " and data_fattura >= '" + string(date(ldt_data_da[ll_i]),s_cs_xx.db_funzioni.formato_data) + "'" + &
				  " and data_fattura <= '" + string(date(ldt_data_a[ll_i]),s_cs_xx.db_funzioni.formato_data) + "' " + &
				  " and flag_sola_iva = 'N' "
				  
	ls_where_prec = "where cod_azienda = '" + s_cs_xx.cod_azienda + "'" + &
	           " and data_fattura >= '" + string(date(ldt_data_prec_da[ll_i]),s_cs_xx.db_funzioni.formato_data) + "'" + &
				  " and data_fattura <= '" + string(date(ldt_data_prec_a[ll_i]),s_cs_xx.db_funzioni.formato_data) + "' " + &
				  " and flag_sola_iva = 'N' "

	ls_select = ls_select + ls_where
	
	ls_select_prec = ls_select_prec + ls_where_prec

	if len(is_where_tes) > 0 then
		ls_select = ls_select + is_where_tes
		ls_select_prec = ls_select_prec + is_where_tes
	end if

	if il_prod_report > 0 then
		
		tab_1.tabpage_6.st_elaborazione.text = "Periodo " + string(ll_i) + "/" + string(upperbound(ldt_data_da)) + &
															" - retrieve dei dati"		
		
		for ll_j = 1 to il_where_prod

			if isnull(is_where_prod[ll_j]) then
				continue
			end if

			yield()

			if ib_interrompi then
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_selezione.postevent("clicked")
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return 0
			end if
			
			if ll_j = 1 then
				il_retrievestart = 0
			else
				il_retrievestart = 2
			end if			
		
			if tab_1.tabpage_6.dw_retrieve.settransobject(sqlca) = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della transazione di dw_retrieve")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
		
//			if tab_1.tabpage_6.dw_retrieve.setsqlselect(ls_select + is_where_prod[ll_j] ) = -1 then
			if tab_1.tabpage_6.dw_retrieve.setsqlselect(ls_select + is_where_prod[ll_j] + " order by 1 ") = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della select di dw_retrieve")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if	
			
			ll_return = tab_1.tabpage_6.dw_retrieve.retrieve()
	
			if ll_return = -1 then
				g_mb.messagebox("APICE","Errore nella retrieve di dw_retrieve")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
		
			if tab_1.tabpage_6.dw_retrieve_prec.settransobject(sqlca) = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della transazione di dw_retrieve_prec")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
		
//			if tab_1.tabpage_6.dw_retrieve_prec.setsqlselect(ls_select_prec + is_where_prod[ll_j]) = -1 then
			if tab_1.tabpage_6.dw_retrieve_prec.setsqlselect(ls_select_prec + is_where_prod[ll_j] + " order by 1 ") = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della select di dw_retrieve_prec")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
			
			ll_return = tab_1.tabpage_6.dw_retrieve_prec.retrieve()

			if ll_return = -1 then
				g_mb.messagebox("APICE","Errore nella retrieve di dw_retrieve_prec")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
			
			if tab_1.tabpage_6.dw_note.settransobject(sqlca) = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della transazione di dw_note")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
		
//			if tab_1.tabpage_6.dw_note.setsqlselect(ls_select + ls_where_note_credito + is_where_prod[ll_j]) = -1 then
			if tab_1.tabpage_6.dw_note.setsqlselect(ls_select + ls_where_note_credito + is_where_prod[ll_j] + " order by 1 ") = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della select di dw_note")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
			
			ll_return = tab_1.tabpage_6.dw_note.retrieve()

			if ll_return = -1 then
				g_mb.messagebox("APICE","Errore nella retrieve di dw_note")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
			
			if tab_1.tabpage_6.dw_note_prec.settransobject(sqlca) = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della transazione di dw_note_prec")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
		
//			if tab_1.tabpage_6.dw_note_prec.setsqlselect(ls_select_prec + ls_where_note_credito + is_where_prod[ll_j]) = -1 then
			if tab_1.tabpage_6.dw_note_prec.setsqlselect(ls_select_prec + ls_where_note_credito + is_where_prod[ll_j] + " order by 1 ") = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della select di dw_note_prec")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
			
			ll_return = tab_1.tabpage_6.dw_note_prec.retrieve()

			if ll_return = -1 then
				g_mb.messagebox("APICE","Errore nella retrieve di dw_note_prec")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
	
		next		

		for ll_j = 1 to il_prod_report

			if isnull(is_prodotti[ll_j]) then
				continue
			end if	

			yield()

			if ib_interrompi then
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_selezione.postevent("clicked")
				return 0
				cb_report.enabled = true
				cb_interrompi.enabled = false
			end if

			tab_1.tabpage_6.st_elaborazione.text = "Periodo " + string(ll_i) + "/" + string(upperbound(ldt_data_da)) + &
																" - lettura prodotto " + string(ll_j) + &
																"/" + string(il_prod_report) + " (" + is_prodotti[ll_j] + ")"

			ll_progress = ll_j + (ll_i - 1) * il_prod_report
			ll_progress = ll_progress * 1000 / (il_prod_report * upperbound(ldt_data_da))
			ll_progress = round( ll_progress , 0 )
			tab_1.tabpage_6.hpb_1.position = ll_progress
			ll_progress = round( ll_progress / 10 , 0 )
			tab_1.tabpage_6.st_perc.text = string(ll_progress) + "%"													
			
			ls_cod_prodotto = is_prodotti[ll_j]

			ld_quantita = 0
			ld_fatturato = 0

			lb_trovato = false
			ll_pos = 0
			ll_start = 1
			ll_end = tab_1.tabpage_6.dw_retrieve.rowcount()

			if ll_end > 0 then
				// verifico se è l'ultimo
				if tab_1.tabpage_6.dw_retrieve.getitemstring(ll_end,"cod_prodotto") = ls_cod_prodotto then

					lb_trovato = true
					ll_pos = ll_end
				// verifico se è il primo
				elseif tab_1.tabpage_6.dw_retrieve.getitemstring(ll_start,"cod_prodotto") = ls_cod_prodotto then
				
					lb_trovato = true
					ll_pos = ll_start
					
				else // in tutti gli altri casi lo cerco
					
					do while ll_start < (ll_end - 1)
						
						ll_pos = round( (ll_start + ll_end) / 2 , 0 )						
						ls_prod_letto = tab_1.tabpage_6.dw_retrieve.getitemstring(ll_pos,"cod_prodotto")
						
						if ls_prod_letto = ls_cod_prodotto then
							lb_trovato = true
							exit
						elseif ls_prod_letto < ls_cod_prodotto then
							ll_start = ll_pos
						elseif ls_prod_letto > ls_cod_prodotto then
							ll_end = ll_pos
						end if
						
					loop	
					
				end if
				
			end if

			if lb_trovato then				
				ld_quantita = tab_1.tabpage_6.dw_retrieve.getitemnumber(ll_pos,"quantita")				
				ld_fatturato = tab_1.tabpage_6.dw_retrieve.getitemnumber(ll_pos,"fatturato")				
			end if
			
			ld_quan_neg = 0
			ld_fatt_neg = 0
	
			lb_trovato = false
			ll_pos = 0
			ll_start = 1
			ll_end = tab_1.tabpage_6.dw_note.rowcount()
	
			if ll_end > 0 then
	
				if tab_1.tabpage_6.dw_note.getitemstring(ll_end,"cod_prodotto") = ls_cod_prodotto then
	
					lb_trovato = true
					ll_pos = ll_end
	
				elseif tab_1.tabpage_6.dw_note.getitemstring(ll_start,"cod_prodotto") = ls_cod_prodotto then
	
					lb_trovato = true
					ll_pos = ll_start
					
				else
					
					do while ll_start < (ll_end - 1)
						
						ll_pos = round( (ll_start + ll_end) / 2 , 0 )						
						ls_prod_letto = tab_1.tabpage_6.dw_note.getitemstring(ll_pos,"cod_prodotto")
						
						if ls_prod_letto = ls_cod_prodotto then
							lb_trovato = true
							exit
						elseif ls_prod_letto < ls_cod_prodotto then
							ll_start = ll_pos
						elseif ls_prod_letto > ls_cod_prodotto then
							ll_end = ll_pos
						end if
						
					loop	
					
				end if
				
			end if
	
			if lb_trovato then				
				ld_quan_neg = tab_1.tabpage_6.dw_note.getitemnumber(ll_pos,"quantita")				
				ld_fatt_neg = tab_1.tabpage_6.dw_note.getitemnumber(ll_pos,"fatturato")				
			end if
			
			ld_quantita = ld_quantita - ld_quan_neg * 2
			ld_fatturato = ld_fatturato - ld_fatt_neg * 2
			
			ld_quan_prec = 0
			ld_fatt_prec = 0
			
			lb_trovato = false			
			ll_pos = 0			
			ll_start = 1
			ll_end = tab_1.tabpage_6.dw_retrieve_prec.rowcount()

			if ll_end > 0 then

				if tab_1.tabpage_6.dw_retrieve_prec.getitemstring(ll_end,"cod_prodotto") = ls_cod_prodotto then

					lb_trovato = true
					ll_pos = ll_end

				elseif tab_1.tabpage_6.dw_retrieve_prec.getitemstring(ll_start,"cod_prodotto") = ls_cod_prodotto then

					lb_trovato = true
					ll_pos = ll_start

				else

					do while ll_start < (ll_end - 1)
						
						ll_pos = round( (ll_start + ll_end) / 2 , 0 )						
						ls_prod_letto = tab_1.tabpage_6.dw_retrieve_prec.getitemstring(ll_pos,"cod_prodotto")
						
						if ls_prod_letto = ls_cod_prodotto then
							lb_trovato = true
							exit
						elseif ls_prod_letto < ls_cod_prodotto then
							ll_start = ll_pos
						elseif ls_prod_letto > ls_cod_prodotto then
							ll_end = ll_pos
						end if
				
					loop	
				
				end if
				
			end if

			if lb_trovato then
				ld_quan_prec = tab_1.tabpage_6.dw_retrieve_prec.getitemnumber(ll_pos,"quantita")
				ld_fatt_prec = tab_1.tabpage_6.dw_retrieve_prec.getitemnumber(ll_pos,"fatturato")
			end if
			
			ld_quan_neg = 0			
			ld_fatt_neg = 0
				
			lb_trovato = false			
			ll_pos = 0			
			ll_start = 1
			ll_end = tab_1.tabpage_6.dw_note_prec.rowcount()

			if ll_end > 0 then

				if tab_1.tabpage_6.dw_note_prec.getitemstring(ll_end,"cod_prodotto") = ls_cod_prodotto then

					lb_trovato = true
					ll_pos = ll_end

				elseif tab_1.tabpage_6.dw_note_prec.getitemstring(ll_start,"cod_prodotto") = ls_cod_prodotto then

					lb_trovato = true
					ll_pos = ll_start

				else

					do while ll_start < (ll_end - 1)
						
						ll_pos = round( (ll_start + ll_end) / 2 , 0 )						
						ls_prod_letto = tab_1.tabpage_6.dw_note_prec.getitemstring(ll_pos,"cod_prodotto")
						
						if ls_prod_letto = ls_cod_prodotto then
							lb_trovato = true
							exit
						elseif ls_prod_letto < ls_cod_prodotto then
							ll_start = ll_pos
						elseif ls_prod_letto > ls_cod_prodotto then
							ll_end = ll_pos
						end if
				
					loop	
				
				end if
				
			end if

			if lb_trovato then
				ld_quan_neg = tab_1.tabpage_6.dw_note_prec.getitemnumber(ll_pos,"quantita")
				ld_fatt_neg = tab_1.tabpage_6.dw_note_prec.getitemnumber(ll_pos,"fatturato")
			end if
			
			ld_quan_prec = ld_quan_prec - ld_quan_neg * 2
			ld_fatt_prec = ld_fatt_prec - ld_fatt_neg * 2
				
			ld_diff_quan = ld_quantita - ld_quan_prec

			if ld_quantita = 0 then
				setnull(ld_perc_quan)
			else
				ld_perc_quan = ld_diff_quan * 100 / ld_quantita
			end if	
			
			ld_diff_fatt = ld_fatturato - ld_fatt_prec
			
			if ld_fatturato = 0 then
				setnull(ld_perc_fatt)
			else
				ld_perc_fatt = ld_diff_fatt * 100 / ld_fatturato
			end if			
			
			select des_prodotto
			into   :ls_des_prodotto
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
				 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore nella select di anag_prodotti: " + sqlca.sqlerrtext)
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if			

			ld_fatturato = round(ld_fatturato,2)
			ld_fatt_prec = round(ld_fatt_prec,2)
			ld_diff_fatt = round(ld_diff_fatt,2)
			ll_riga = tab_1.tabpage_6.dw_report_statistiche.insertrow(0)
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"tipo_riga","P")
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"des_prodotto",ls_des_prodotto)
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"quan_analisi",ld_quantita)
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"quan_prec",ld_quan_prec)
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"diff_quan_num",ld_diff_quan)
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"diff_quan_perc",ld_perc_quan)
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"fatt_analisi",ld_fatturato)
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"fatt_prec",ld_fatt_prec)
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"diff_fatt_num",ld_diff_fatt)
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"diff_fatt_perc",ld_perc_fatt)			

		next	

	else	// non è una elaborazione che riguarda i prodotti
		
		for ll_j = 1 to 3
			
			yield()
		
			if ib_interrompi then
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_selezione.postevent("clicked")
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return 0
			end if
			
			choose case ll_j
			
				case 1					
					ls_contenuto_periodo = "Fatturato prodotti a magazzino"					
					ls_where_fatturato = "and flag_tipo_det_ven in ('M','C') "					
					tab_1.tabpage_6.st_elaborazione.text = "Periodo " + string(ll_i) + "/" + string(upperbound(ldt_data_da)) + &
															         " - lettura fatturato a magazzino"					
				case 2					
					ls_contenuto_periodo = "Altro fatturato"					
					ls_where_fatturato = "and flag_tipo_det_ven not in ('M','C') "					
					tab_1.tabpage_6.st_elaborazione.text = "Periodo " + string(ll_i) + "/" + string(upperbound(ldt_data_da)) + &
															         " - lettura altro fatturato"					
				case 3					
					ls_contenuto_periodo = "Totale fatturato"					
					ls_where_fatturato = ""					
					tab_1.tabpage_6.st_elaborazione.text = "Periodo " + string(ll_i) + "/" + string(upperbound(ldt_data_da)) + &
															         " - lettura fatturato totale"			
			end choose			
			
			ll_progress = round( ((ll_j + (ll_i - 1) * 3) * 1000 / (upperbound(ldt_data_da) * 3)) , 0 )		
			tab_1.tabpage_6.hpb_1.position = ll_progress		
			ll_progress = round( ll_progress / 10 , 0 )	
			tab_1.tabpage_6.st_perc.text = string(ll_progress) + "%"
			
			il_retrievestart = 0			
		
			if tab_1.tabpage_6.dw_retrieve.settransobject(sqlca) = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della transazione di dw_retrieve")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
		
			if tab_1.tabpage_6.dw_retrieve.setsqlselect(ls_select + ls_where_fatturato) = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della select di dw_retrieve")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
		
			ll_return = tab_1.tabpage_6.dw_retrieve.retrieve()
	
			if ll_return = -1 then
				g_mb.messagebox("APICE","Errore nella retrieve di dw_retrieve")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			elseif ll_return = 0 then			
				ld_quantita = 0
				ld_fatturato = 0
			else
				ld_quantita = tab_1.tabpage_6.dw_retrieve.getitemnumber(1,"quantita")
				ld_fatturato = tab_1.tabpage_6.dw_retrieve.getitemnumber(1,"fatturato")
				if isnull(ld_quantita) then
					ld_quantita = 0
				end if			
				if isnull(ld_fatturato) then
					ld_fatturato = 0
				end if
			end if			
			
			if tab_1.tabpage_6.dw_note.settransobject(sqlca) = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della transazione di dw_note")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
		
			if tab_1.tabpage_6.dw_note.setsqlselect(ls_select + ls_where_fatturato + ls_where_note_credito) = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della select di dw_note")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
		
			ll_return = tab_1.tabpage_6.dw_note.retrieve()
	
			if ll_return = -1 then
				g_mb.messagebox("APICE","Errore nella retrieve di dw_note")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			elseif ll_return = 0 then			
				ld_quan_neg = 0
				ld_fatt_neg = 0
			else
				ld_quan_neg = tab_1.tabpage_6.dw_note.getitemnumber(1,"quantita")
				ld_fatt_neg = tab_1.tabpage_6.dw_note.getitemnumber(1,"fatturato")
				if isnull(ld_quan_neg) then
					ld_quan_neg = 0
				end if			
				if isnull(ld_fatt_neg) then
					ld_fatt_neg = 0
				end if
				ld_quantita = ld_quantita - ld_quan_neg * 2
				ld_fatturato = ld_fatturato - ld_fatt_neg * 2
			end if

			if tab_1.tabpage_6.dw_retrieve_prec.settransobject(sqlca) = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della transazione di dw_retrieve_prec")
				setpointer(arrow!)			
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
		
			if tab_1.tabpage_6.dw_retrieve_prec.setsqlselect(ls_select_prec + ls_where_fatturato) = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della select di dw_retrieve_prec")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
		
			ll_return = tab_1.tabpage_6.dw_retrieve_prec.retrieve()

			if ll_return = -1 then
				g_mb.messagebox("APICE","Errore nella retrieve di dw_retrieve_prec")
				setpointer(arrow!)			
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			elseif ll_return = 0 then			
				ld_quan_prec = 0
				ld_fatt_prec = 0
			else
				ld_quan_prec = tab_1.tabpage_6.dw_retrieve_prec.getitemnumber(1,"quantita")
				ld_fatt_prec = tab_1.tabpage_6.dw_retrieve_prec.getitemnumber(1,"fatturato")
				if isnull(ld_quan_prec) then
					ld_quan_prec = 0
				end if			
				if isnull(ld_fatt_prec) then
					ld_fatt_prec = 0
				end if
			end if
		
			if tab_1.tabpage_6.dw_note_prec.settransobject(sqlca) = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della transazione di dw_note_prec")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
		
			if tab_1.tabpage_6.dw_note_prec.setsqlselect(ls_select_prec + ls_where_fatturato + ls_where_note_credito) = -1 then
				g_mb.messagebox("APICE","Errore nella modifica della select di dw_note_prec")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			end if
			
			ll_return = tab_1.tabpage_6.dw_note_prec.retrieve()
	
			if ll_return = -1 then
				g_mb.messagebox("APICE","Errore nella retrieve di dw_note_prec")
				setpointer(arrow!)
				tab_1.tabpage_6.st_elaborazione.text = ""
				tab_1.tabpage_6.hpb_1.position = 0		
				tab_1.tabpage_6.st_perc.text = "0%"
				cb_report.enabled = true
				cb_interrompi.enabled = false
				return -1
			elseif ll_return = 0 then			
				ld_quan_neg = 0
				ld_fatt_neg = 0
			else
				ld_quan_neg = tab_1.tabpage_6.dw_note_prec.getitemnumber(1,"quantita")
				ld_fatt_neg = tab_1.tabpage_6.dw_note_prec.getitemnumber(1,"fatturato")
				if isnull(ld_quan_neg) then
					ld_quan_neg = 0
				end if			
				if isnull(ld_fatt_neg) then
					ld_fatt_neg = 0
				end if
				ld_quan_prec = ld_quan_prec - ld_quan_neg * 2
				ld_fatt_prec = ld_fatt_prec - ld_fatt_neg * 2
			end if
		
			ld_diff_quan = ld_quantita - ld_quan_prec

			if ld_quantita = 0 then
				setnull(ld_perc_quan)
			else
				ld_perc_quan = ld_diff_quan * 100 / ld_quantita
			end if	
			
			ld_diff_fatt = ld_fatturato - ld_fatt_prec
		
			if ld_fatturato = 0 then
				setnull(ld_perc_fatt)
			else
				ld_perc_fatt = ld_diff_fatt * 100 / ld_fatturato
			end if	
		
			ll_riga = tab_1.tabpage_6.dw_report_statistiche.insertrow(0)		
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"tipo_riga","D")		
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"contenuto_periodo",ls_contenuto_periodo)					
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"quan_analisi",ld_quantita)		
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"quan_prec",ld_quan_prec)		
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"diff_quan_num",ld_diff_quan)		
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"diff_quan_perc",ld_perc_quan)		
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"fatt_analisi",ld_fatturato)		
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"fatt_prec",ld_fatt_prec)		
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"diff_fatt_num",ld_diff_fatt)		
			tab_1.tabpage_6.dw_report_statistiche.setitem(ll_riga,"diff_fatt_perc",ld_perc_fatt)
			
		next	
	
	end if	
	
next

setpointer(arrow!)
cb_interrompi.enabled = false
cb_selezione.show()
cb_stampa.enabled = true
cb_esporta.enabled = true
ib_report = true

tab_1.tabpage_6.st_elaborazione.text = ""
tab_1.tabpage_6.hpb_1.position = 0
tab_1.tabpage_6.st_perc.text = "0%"

tab_1.tabpage_6.st_elaborazione.hide()
tab_1.tabpage_6.hpb_1.hide()
tab_1.tabpage_6.st_perc.hide()
tab_1.tabpage_6.dw_report_statistiche_selez.hide()
tab_1.tabpage_6.dw_report_statistiche.setredraw(true)
tab_1.tabpage_6.dw_report_statistiche.show()
tab_1.tabpage_6.dw_report_statistiche.setfocus()
tab_1.tabpage_6.dw_report_statistiche.resetupdate()

ldt_oggi = datetime(today(),now())

update tes_statistiche
set    data_ultima_elaborazione = :ldt_oggi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_statistica = :is_codice_corrente;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella update di tes_statistiche: " + sqlca.sqlerrtext)
	rollback;
	return -1
else
	commit;
end if

ll_old_row = tab_1.tabpage_1.dw_tes_statistiche_lista.getrow()

tab_1.tabpage_1.dw_tes_statistiche_lista.retrieve(s_cs_xx.cod_azienda)

tab_1.tabpage_1.dw_tes_statistiche_lista.setrow(ll_old_row)

return 0
end function

on w_statistiche_vendita.create
int iCurrent
call super::create
this.cb_avanti=create cb_avanti
this.cb_indietro=create cb_indietro
this.st_14=create st_14
this.st_descrizione=create st_descrizione
this.st_codice=create st_codice
this.st_13=create st_13
this.st_10=create st_10
this.tab_1=create tab_1
this.r_1=create r_1
this.cb_modifica=create cb_modifica
this.cb_salva_filtri=create cb_salva_filtri
this.cb_cancella=create cb_cancella
this.cb_annulla_modifiche=create cb_annulla_modifiche
this.cb_stampa=create cb_stampa
this.cb_esporta=create cb_esporta
this.cb_interrompi=create cb_interrompi
this.cb_nuovo=create cb_nuovo
this.cb_modifica_filtri=create cb_modifica_filtri
this.cb_selezione=create cb_selezione
this.cb_azzera_testata=create cb_azzera_testata
this.cb_azzera_caratteristiche=create cb_azzera_caratteristiche
this.cb_azzera_prodotti=create cb_azzera_prodotti
this.cb_azzera_livelli=create cb_azzera_livelli
this.cb_report=create cb_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_avanti
this.Control[iCurrent+2]=this.cb_indietro
this.Control[iCurrent+3]=this.st_14
this.Control[iCurrent+4]=this.st_descrizione
this.Control[iCurrent+5]=this.st_codice
this.Control[iCurrent+6]=this.st_13
this.Control[iCurrent+7]=this.st_10
this.Control[iCurrent+8]=this.tab_1
this.Control[iCurrent+9]=this.r_1
this.Control[iCurrent+10]=this.cb_modifica
this.Control[iCurrent+11]=this.cb_salva_filtri
this.Control[iCurrent+12]=this.cb_cancella
this.Control[iCurrent+13]=this.cb_annulla_modifiche
this.Control[iCurrent+14]=this.cb_stampa
this.Control[iCurrent+15]=this.cb_esporta
this.Control[iCurrent+16]=this.cb_interrompi
this.Control[iCurrent+17]=this.cb_nuovo
this.Control[iCurrent+18]=this.cb_modifica_filtri
this.Control[iCurrent+19]=this.cb_selezione
this.Control[iCurrent+20]=this.cb_azzera_testata
this.Control[iCurrent+21]=this.cb_azzera_caratteristiche
this.Control[iCurrent+22]=this.cb_azzera_prodotti
this.Control[iCurrent+23]=this.cb_azzera_livelli
this.Control[iCurrent+24]=this.cb_report
end on

on w_statistiche_vendita.destroy
call super::destroy
destroy(this.cb_avanti)
destroy(this.cb_indietro)
destroy(this.st_14)
destroy(this.st_descrizione)
destroy(this.st_codice)
destroy(this.st_13)
destroy(this.st_10)
destroy(this.tab_1)
destroy(this.r_1)
destroy(this.cb_modifica)
destroy(this.cb_salva_filtri)
destroy(this.cb_cancella)
destroy(this.cb_annulla_modifiche)
destroy(this.cb_stampa)
destroy(this.cb_esporta)
destroy(this.cb_interrompi)
destroy(this.cb_nuovo)
destroy(this.cb_modifica_filtri)
destroy(this.cb_selezione)
destroy(this.cb_azzera_testata)
destroy(this.cb_azzera_caratteristiche)
destroy(this.cb_azzera_prodotti)
destroy(this.cb_azzera_livelli)
destroy(this.cb_report)
end on

event pc_setwindow;call super::pc_setwindow;string ls_sql, ls_cod, ls_temp, ls_sql_nc, ls_errore,ls_sql_pf
long ll_ret, ll_i
datastore lds_nc, lds_pf


tab_1.tabpage_1.dw_tes_statistiche_lista.setrowfocusindicator(hand!)

tab_1.tabpage_3.dw_prod_statistiche_livelli.setrowfocusindicator(hand!)

tab_1.tabpage_3.dw_prod_statistiche_livelli.settransobject(sqlca)

tab_1.tabpage_4.dw_prod_statistiche_elenco.setrowfocusindicator(hand!)

tab_1.tabpage_4.dw_prod_statistiche_elenco.settransobject(sqlca)

tab_1.tabpage_4.dw_prod_statistiche_selez.setrowfocusindicator(hand!)

wf_carica_drop_carat()

tab_1.tabpage_6.dw_report_statistiche_selez.insertrow(0)

tab_1.postevent("ue_retrieve")

update det_fat_ven
set    quan_fatturata = 0
where  quan_fatturata is null;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore nella update di det_fat_ven: " + sqlca.sqlerrtext)
	close(this)
	rollback;
	return -1
end if

update det_fat_ven
set    prezzo_vendita = 0
where  prezzo_vendita is null;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore nella update di det_fat_ven: " + sqlca.sqlerrtext)
	close(this)
	rollback;
	return -1
end if

update det_fat_ven
set    sconto_1 = 0
where  sconto_1 is null;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore nella update di det_fat_ven: " + sqlca.sqlerrtext)
	close(this)
	rollback;
	return -1
end if

update det_fat_ven
set    sconto_2 = 0
where  sconto_2 is null;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore nella update di det_fat_ven: " + sqlca.sqlerrtext)
	close(this)
	rollback;
	return -1
end if

update tes_fat_ven
set    sconto = 0
where  sconto is null;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore nella update di tes_fat_ven: " + sqlca.sqlerrtext)
	close(this)
	rollback;
	return -1
end if

update tab_pagamenti
set    sconto = 0
where  sconto is null;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore nella update di tab_pagamenti: " + sqlca.sqlerrtext)
	close(this)
	rollback;
	return -1
end if

commit;

ls_sql_nc = " SELECT cod_tipo_fat_ven FROM tab_tipi_fat_ven WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' AND flag_tipo_fat_ven = 'N' "

ll_ret = guo_functions.uof_crea_datastore( lds_nc, ls_sql_nc,  ls_errore)

if ll_ret < 0 then
	g_mb.messagebox("Statistiche di vendita", "Errore creazione lista fatture NC " + ls_errore)
	close(this)
end if

is_sql_in = ""
for ll_i = 1 to ll_ret
	if ll_i > 1 then is_sql_in += ", "
	is_sql_in += " '" + lds_nc.getitemstring(ll_i,1) +  "' "
next

ls_sql_pf = " SELECT cod_tipo_fat_ven FROM tab_tipi_fat_ven WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' AND flag_tipo_fat_ven in ('P','F') "

ll_ret = guo_functions.uof_crea_datastore( lds_pf, ls_sql_pf,  ls_errore)

if ll_ret < 0 then
	g_mb.messagebox("Statistiche di vendita", "Errore creazione lista fatture escluse ProForma " + ls_errore)
	close(this)
end if

is_sql_pf = ""
for ll_i = 1 to ll_ret
	if ll_i > 1 then is_sql_pf += ", "
	is_sql_pf += " '" + lds_pf.getitemstring(ll_i,1) +  "' "
next


end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(tab_1.tabpage_6.dw_report_statistiche_selez,"cod_statistica",sqlca,"tes_statistiche",&
					  "cod_statistica","des_statistica","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event open;call super::open;x = 0
y = 0
end event

type cb_avanti from commandbutton within w_statistiche_vendita
integer x = 4210
integer y = 2320
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "A&vanti"
end type

event clicked;tab_1.selecttab(il_tab + 1)
end event

type cb_indietro from commandbutton within w_statistiche_vendita
integer x = 3433
integer y = 2320
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Indietro"
end type

event clicked;tab_1.selecttab(il_tab - 1)
end event

type st_14 from statictext within w_statistiche_vendita
integer x = 672
integer y = 2400
integer width = 421
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "DESCRIZIONE:"
boolean focusrectangle = false
end type

type st_descrizione from statictext within w_statistiche_vendita
integer x = 1097
integer y = 2400
integer width = 1280
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type st_codice from statictext within w_statistiche_vendita
integer x = 297
integer y = 2400
integer width = 366
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean focusrectangle = false
end type

type st_13 from statictext within w_statistiche_vendita
integer x = 50
integer y = 2400
integer width = 238
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "CODICE:"
boolean focusrectangle = false
end type

type st_10 from statictext within w_statistiche_vendita
integer x = 50
integer y = 2312
integer width = 896
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Statistica attualmente selezionata"
boolean focusrectangle = false
end type

type tab_1 from tab within w_statistiche_vendita
event ue_retrieve ( )
integer y = 40
integer width = 4594
integer height = 2240
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean fixedwidth = true
boolean focusonbuttondown = true
alignment alignment = center!
integer selectedtab = 1
tabpage_1 tabpage_1
tabpage_2 tabpage_2
tabpage_3 tabpage_3
tabpage_4 tabpage_4
tabpage_5 tabpage_5
tabpage_6 tabpage_6
end type

event ue_retrieve();tab_1.tabpage_1.dw_tes_statistiche_lista.settrans(sqlca)

setpointer(hourglass!)

if tab_1.tabpage_1.dw_tes_statistiche_lista.retrieve(s_cs_xx.cod_azienda) < 1 then
	tab_1.tabpage_2.enabled = false
	tab_1.tabpage_3.enabled = false
	tab_1.tabpage_4.enabled = false
	tab_1.tabpage_5.enabled = false
	tab_1.tabpage_6.enabled = false
	cb_avanti.enabled = false
	cb_cancella.enabled = false
	cb_modifica.enabled = false
	cb_modifica_filtri.enabled = false
	setnull(is_codice_tes)
	setnull(is_codice_liv)
	setnull(is_codice_prod)
	setnull(is_codice_carat)
	setnull(is_codice_corrente)
	st_codice.text = ""
	st_descrizione.text = ""	
else
	tab_1.tabpage_2.enabled = true
	tab_1.tabpage_3.enabled = true
	tab_1.tabpage_4.enabled = true
	tab_1.tabpage_5.enabled = true
	tab_1.tabpage_6.enabled = true
	cb_avanti.enabled = true
	cb_cancella.enabled = true
	cb_modifica.enabled = true
	cb_modifica_filtri.enabled = true
	setnull(is_codice_tes)
	setnull(is_codice_liv)
	setnull(is_codice_prod)
	setnull(is_codice_carat)
	is_codice_corrente = tab_1.tabpage_1.dw_tes_statistiche_lista.getitemstring(1,"cod_statistica")
	st_codice.text = is_codice_corrente
	st_descrizione.text = tab_1.tabpage_1.dw_tes_statistiche_lista.getitemstring(1,"des_statistica")	
end if

setpointer(arrow!)

wf_resetta_tutto()
end event

on tab_1.create
this.tabpage_1=create tabpage_1
this.tabpage_2=create tabpage_2
this.tabpage_3=create tabpage_3
this.tabpage_4=create tabpage_4
this.tabpage_5=create tabpage_5
this.tabpage_6=create tabpage_6
this.Control[]={this.tabpage_1,&
this.tabpage_2,&
this.tabpage_3,&
this.tabpage_4,&
this.tabpage_5,&
this.tabpage_6}
end on

on tab_1.destroy
destroy(this.tabpage_1)
destroy(this.tabpage_2)
destroy(this.tabpage_3)
destroy(this.tabpage_4)
destroy(this.tabpage_5)
destroy(this.tabpage_6)
end on

event selectionchanged;long   ll_return

string ls_cod_prodotto, ls_des_prodotto


il_tab = newindex

choose case newindex
		
	case 1
		
		tab_1.tabpage_1.tabtextcolor = 8388608
		
		tab_1.tabpage_2.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_3.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_4.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_5.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_6.tabtextcolor = rgb(0,0,0)
		
		cb_indietro.enabled = false
		
		if tab_1.tabpage_1.dw_tes_statistiche_lista.rowcount() > 0 then
			cb_avanti.enabled = true
		end if	
		
		cb_modifica_filtri.hide()
		
		cb_annulla_modifiche.hide()
		
		cb_salva_filtri.hide()
		
		cb_azzera_testata.hide()
		
		cb_azzera_livelli.hide()
		
		cb_azzera_prodotti.hide()
		
		cb_azzera_caratteristiche.hide()
		
		cb_report.hide()
		
		cb_selezione.hide()
		
		cb_stampa.hide()
		
		cb_esporta.hide()
		
		cb_nuovo.show()
		
		cb_cancella.show()
		
		cb_modifica.show()
		
	case 2
		
		if tabpage_2.enabled = false then
			selecttab(oldindex)
			return
		end if
		
		if isnull(is_codice_corrente) then
			g_mb.messagebox("APICE","Selezionare prima una statistica dall'elenco!")
			selecttab(oldindex)
			return
		end if
		
		tab_1.tabpage_1.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_2.tabtextcolor = 8388608
		
		tab_1.tabpage_3.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_4.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_5.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_6.tabtextcolor = rgb(0,0,0)
		
		cb_indietro.enabled = true
		
		cb_avanti.enabled = true
		
		cb_modifica_filtri.show()
		
		cb_annulla_modifiche.show()
		
		cb_salva_filtri.show()
		
		cb_azzera_testata.show()
		
		cb_azzera_livelli.hide()
		
		cb_azzera_prodotti.hide()
		
		cb_azzera_caratteristiche.hide()
		
		cb_report.hide()
		
		cb_selezione.hide()
		
		cb_stampa.hide()
		
		cb_esporta.hide()
		
		cb_nuovo.hide()
		
		cb_cancella.hide()
		
		cb_modifica.hide()
		
		if is_codice_tes = is_codice_corrente then
			return
		end if		
		
		ib_background = true
		
		cb_azzera_testata.triggerevent("clicked")

		if wf_carica_filtri_tes() = -1 then
			cb_azzera_testata.triggerevent("clicked")
		else
			if wf_carica_filtri_tes_2() = -1 then
				cb_azzera_testata.triggerevent("clicked")
			else
				is_codice_tes = is_codice_corrente
			end if
		end if
		
		ib_background = false
		
	case 3
		
		if tabpage_3.enabled = false then
			selecttab(oldindex)
			return
		end if
		
		if isnull(is_codice_corrente) then
			g_mb.messagebox("APICE","Selezionare prima una statistica dall'elenco!")
			selecttab(oldindex)
			return
		end if
		
		tab_1.tabpage_1.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_2.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_3.tabtextcolor = 8388608
		
		tab_1.tabpage_4.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_5.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_6.tabtextcolor = rgb(0,0,0)
		
		cb_indietro.enabled = true
		
		cb_avanti.enabled = true
		
		cb_modifica_filtri.show()
		
		cb_annulla_modifiche.show()
		
		cb_salva_filtri.show()
		
		cb_azzera_testata.hide()
		
		cb_azzera_livelli.show()
		
		cb_azzera_prodotti.hide()
		
		cb_azzera_caratteristiche.hide()
		
		cb_report.hide()
		
		cb_selezione.hide()
		
		cb_stampa.hide()
		
		cb_esporta.hide()
		
		cb_nuovo.hide()
		
		cb_cancella.hide()
		
		cb_modifica.hide()
		
		if is_codice_liv = is_codice_corrente then
			return
		end if
		
		ib_background = true

		cb_azzera_livelli.triggerevent("clicked")
		
		if wf_carica_livelli() = -1 then
			cb_azzera_livelli.triggerevent("clicked")
		else
			is_codice_liv = is_codice_corrente
		end if
		
		ib_background = false
			
	case 4
		
		if tabpage_4.enabled = false then
			selecttab(oldindex)
			return
		end if
		
		if isnull(is_codice_corrente) then
			g_mb.messagebox("APICE","Selezionare prima una statistica dall'elenco!")
			selecttab(oldindex)
			return
		end if
		
		tab_1.tabpage_1.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_2.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_3.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_4.tabtextcolor = 8388608
		
		tab_1.tabpage_5.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_6.tabtextcolor = rgb(0,0,0)
		
		cb_indietro.enabled = true
		
		cb_avanti.enabled = true
		
		cb_modifica_filtri.show()
		
		cb_annulla_modifiche.show()
		
		cb_salva_filtri.show()
		
		cb_azzera_testata.hide()
		
		cb_azzera_livelli.hide()
		
		cb_azzera_prodotti.show()
		
		cb_azzera_caratteristiche.hide()
		
		cb_report.hide()
		
		cb_selezione.hide()
		
		cb_stampa.hide()
		
		cb_esporta.hide()
		
		cb_nuovo.hide()
		
		cb_cancella.hide()
		
		cb_modifica.hide()		
		
		if is_codice_prod = is_codice_corrente and is_codice_liv = is_codice_corrente and ib_livelli_mod = false then
			return
		end if		
		
		ib_background = true
		
		if is_codice_liv <> is_codice_corrente or isnull(is_codice_liv) then
			
			cb_azzera_livelli.triggerevent("clicked")
		
			if wf_carica_livelli() = -1 then
				
				cb_azzera_livelli.triggerevent("clicked")
				
				ib_background = false
				
				return
				
			else
				
				is_codice_liv = is_codice_corrente
				
			end if
			
		end if
		
		if il_livello = 0 then
			tab_1.tabpage_4.cbx_senza_livello.checked = true
			tab_1.tabpage_4.cbx_senza_livello.enabled = false
		else	
			if tab_1.tabpage_4.cbx_senza_livello.checked = true and tab_1.tabpage_4.cbx_senza_livello.enabled = false then
				tab_1.tabpage_4.cbx_senza_livello.checked = false
			end if	
			if ib_consenti_modifica then
				tab_1.tabpage_4.cbx_senza_livello.enabled = true
			end if	
		end if
		
		choose case il_livello		
			case 0		
				tab_1.tabpage_4.st_livello.text = "Nessun livello selezionato"		
			case 1		
				tab_1.tabpage_4.st_livello.text = is_livello_1 + " - " + tab_1.tabpage_3.st_liv_scelto_1.text	
			case 2		
				tab_1.tabpage_4.st_livello.text = is_livello_2 + " - " + tab_1.tabpage_3.st_liv_scelto_2.text		
			case 3		
				tab_1.tabpage_4.st_livello.text = is_livello_3 + " - " + tab_1.tabpage_3.st_liv_scelto_3.text	
			case 4		
				tab_1.tabpage_4.st_livello.text = is_livello_4 + " - " + tab_1.tabpage_3.st_liv_scelto_4.text		
			case 5		
				tab_1.tabpage_4.st_livello.text = is_livello_5 + " - " + tab_1.tabpage_3.st_liv_scelto_5.text				
		end choose
		
		if is_codice_prod = is_codice_corrente and ib_livelli_mod = true then
		
			wf_carica_elenco_prodotti()
			
		else
									
			cb_azzera_prodotti.triggerevent("clicked")		
				
			if wf_carica_prodotti() = -1 then
				cb_azzera_prodotti.triggerevent("clicked")
				ib_background = false
				return
			else
				is_codice_prod = is_codice_corrente
			end if			
		
		end if
		
		ib_background = false		
		
	case 5
		
		if tabpage_5.enabled = false then
			selecttab(oldindex)
			return
		end if
		
		if isnull(is_codice_corrente) then
			g_mb.messagebox("APICE","Selezionare prima una statistica dall'elenco!")
			selecttab(oldindex)
			return
		end if
		
		tab_1.tabpage_1.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_2.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_3.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_4.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_5.tabtextcolor = 8388608
		
		tab_1.tabpage_6.tabtextcolor = rgb(0,0,0)
		
		cb_indietro.enabled = true
		
		cb_avanti.enabled = true
		
		cb_modifica_filtri.show()
		
		cb_annulla_modifiche.show()
		
		cb_salva_filtri.show()
		
		cb_azzera_testata.hide()
		
		cb_azzera_livelli.hide()
		
		cb_azzera_prodotti.hide()
		
		cb_azzera_caratteristiche.show()
		
		cb_report.hide()
		
		cb_selezione.hide()
		
		cb_stampa.hide()
		
		cb_esporta.hide()
		
		cb_nuovo.hide()
		
		cb_cancella.hide()
		
		cb_modifica.hide()
		
		if is_codice_carat = is_codice_corrente and (is_codice_prod = is_codice_corrente and ib_prodotti_mod = false) and &
			is_codice_liv = is_codice_corrente then
			return
		end if
		
		ib_background = true
		
		if is_codice_liv <> is_codice_corrente or isnull(is_codice_liv) then
			
			cb_azzera_livelli.triggerevent("clicked")
		
			if wf_carica_livelli() = -1 then
				
				cb_azzera_livelli.triggerevent("clicked")
				
				ib_background = false
				
				return
				
			else
				
				if il_livello = 0 then
					tab_1.tabpage_4.cbx_senza_livello.checked = true
					tab_1.tabpage_4.cbx_senza_livello.enabled = false
				else	
					tab_1.tabpage_4.cbx_senza_livello.checked = false
					tab_1.tabpage_4.cbx_senza_livello.enabled = true
				end if
				
				is_codice_liv = is_codice_corrente
								
			end if		
			
		end if
		
		if is_codice_prod = is_codice_corrente and ib_livelli_mod = true then
		
			wf_carica_elenco_prodotti()
			
		elseif is_codice_prod <> is_codice_corrente or isnull(is_codice_prod) then
									
			cb_azzera_prodotti.triggerevent("clicked")		
				
			if wf_carica_prodotti() = -1 then
				cb_azzera_prodotti.triggerevent("clicked")
				ib_background = false
				return
			else
				is_codice_prod = is_codice_corrente
			end if			
		
		end if
		
		cb_azzera_caratteristiche.triggerevent("clicked")
		
		if il_prodotti <> 1 then
			if il_prodotti = 0 then
				tabpage_5.st_prodotto.text = "Nessun prodotto selezionato"
			else
				tabpage_5.st_prodotto.text = "Caratteristiche non disponibili"
			end if
			ib_background = false			
			tabpage_5.dw_prod_statistiche_carat.hide()
			tab_1.tabpage_5.cbx_esplosione_carat.enabled = false
			tab_1.tabpage_5.cbx_esplosione_carat.checked = false
			tab_1.tabpage_5.cb_esegui.hide()
			tab_1.tabpage_5.st_da_data.hide()
			tab_1.tabpage_5.em_da_data.hide()
			tab_1.tabpage_5.st_a_data.hide()
			tab_1.tabpage_5.em_a_data.hide()
			return
		else
			tab_1.tabpage_5.st_prodotto.text = tab_1.tabpage_4.dw_prod_statistiche_selez.getitemstring(1,"cod_prodotto")
			tabpage_5.dw_prod_statistiche_carat.show()
			tab_1.tabpage_5.cbx_esplosione_carat.enabled = true
		end if
		
		il_caratteristiche = wf_carica_caratteristiche()
		
		tab_1.tabpage_5.cbx_esplosione_carat.checked = false		
		tab_1.tabpage_5.cb_esegui.hide()		
		tab_1.tabpage_5.st_da_data.hide()		
		tab_1.tabpage_5.em_da_data.text = ""		
		tab_1.tabpage_5.em_da_data.hide()
		tab_1.tabpage_5.st_a_data.hide()		
		tab_1.tabpage_5.em_a_data.text = ""		
		tab_1.tabpage_5.em_a_data.hide()
		
		if il_caratteristiche = -1 then
			il_caratteristiche = 0
			cb_azzera_caratteristiche.triggerevent("clicked")
			ib_background = false
			return
		elseif il_caratteristiche = 0 and ib_consenti_modifica = true then
			tab_1.tabpage_5.dw_prod_statistiche_carat.insertrow(0)
			il_caratteristiche++
		elseif il_caratteristiche > 0 and ib_consenti_modifica = true then
			cb_azzera_caratteristiche.enabled = true
		end if
		
		is_codice_carat = is_codice_corrente		
		
		if ib_consenti_modifica = false then
			wf_blocca_caratteristiche()
		end if	
		
		ib_background = false		
		
	case 6
		
		tab_1.tabpage_1.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_2.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_3.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_4.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_5.tabtextcolor = rgb(0,0,0)
		
		tab_1.tabpage_6.tabtextcolor = 8388608
		
		cb_indietro.enabled = true
		
		cb_avanti.enabled = false
		
		cb_modifica_filtri.hide()
		
		cb_annulla_modifiche.hide()
		
		cb_salva_filtri.hide()
		
		cb_azzera_testata.hide()
		
		cb_azzera_livelli.hide()
		
		cb_azzera_prodotti.hide()
		
		cb_azzera_caratteristiche.hide()
		
		if ib_report then
			
			cb_report.hide()
		
			cb_selezione.show()
			
		else
			
			cb_report.show()
		
			cb_selezione.hide()
			
		end if	
		
		cb_stampa.show()
		
		cb_esporta.show()
		
		cb_nuovo.hide()
		
		cb_cancella.hide()
		
		cb_modifica.hide()
		
		f_po_loaddddw_dw(tab_1.tabpage_6.dw_report_statistiche_selez,"cod_statistica",sqlca,"tes_statistiche",&
					  		  "cod_statistica","des_statistica","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
		
		tabpage_6.dw_report_statistiche_selez.setitem(1,"cod_statistica",is_codice_corrente)		
				
end choose
end event

event selectionchanging;if newindex = 6 and ib_consenti_modifica then
	return 1
end if
end event

type tabpage_1 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 4558
integer height = 2116
long backcolor = 12632256
string text = "Tipi Statistiche"
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
st_18 st_18
dw_tes_statistiche_lista dw_tes_statistiche_lista
end type

on tabpage_1.create
this.st_18=create st_18
this.dw_tes_statistiche_lista=create dw_tes_statistiche_lista
this.Control[]={this.st_18,&
this.dw_tes_statistiche_lista}
end on

on tabpage_1.destroy
destroy(this.st_18)
destroy(this.dw_tes_statistiche_lista)
end on

type st_18 from statictext within tabpage_1
integer x = 5
integer y = 12
integer width = 4544
integer height = 80
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Elenco tipi statistiche"
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type dw_tes_statistiche_lista from datawindow within tabpage_1
integer x = 5
integer y = 108
integer width = 4549
integer height = 2004
integer taborder = 20
string dataobject = "d_tes_statistiche_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event rowfocuschanging;if ib_testata_mod then
	
	if ib_livelli_mod then
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata, livelli, prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return 1
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata, livelli e prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return 1
				end if
				
			end if
			
		else			
		 
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata, livelli e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return 1
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata e livelli modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return 1
				end if
				
			end if
		
		end if
	
	else
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata, prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return 1
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata e prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return 1
				end if
				
			end if			
		
		else
		
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return 1
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return 1
				end if
				
			end if
		
		end if
		
	end if	
	
else
	
	if ib_livelli_mod then
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di livelli, prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return 1
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di livelli e prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return 1
				end if
				
			end if
			
		else	
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di livelli e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return 1
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di livelli modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return 1
				end if
				
			end if
		
		end if
		
	else
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return 1
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return 1
				end if
				
			end if
		
		else
	
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return 1
				end if
			
			else
				
				return 0
			
			end if

		end if
	
	end if
	
end if

wf_resetta_tutto()
end event

event rowfocuschanged;if isnull(currentrow) or currentrow = 0 then
	return -1
end if	

is_codice_corrente = getitemstring(currentrow,"cod_statistica")

st_codice.text = is_codice_corrente

st_descrizione.text = getitemstring(currentrow,"des_statistica")
end event

type tabpage_2 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 4558
integer height = 2116
long backcolor = 12632256
string text = "Filtri Testata"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
st_20 st_20
st_19 st_19
st_8 st_8
dw_8 dw_8
cbx_prodotti cbx_prodotti
st_data_fine st_data_fine
st_data_inizio st_data_inizio
cb_esplodi_tes cb_esplodi_tes
em_data_fine em_data_fine
em_data_inizio em_data_inizio
st_esplodi_testata st_esplodi_testata
st_1 st_1
st_2 st_2
st_3 st_3
st_4 st_4
st_5 st_5
st_6 st_6
st_7 st_7
st_filtri_disponibili st_filtri_disponibili
st_9 st_9
dw_1 dw_1
dw_2 dw_2
dw_3 dw_3
dw_4 dw_4
dw_5 dw_5
dw_6 dw_6
dw_7 dw_7
gb_1 gb_1
gb_2 gb_2
gb_3 gb_3
end type

on tabpage_2.create
this.st_20=create st_20
this.st_19=create st_19
this.st_8=create st_8
this.dw_8=create dw_8
this.cbx_prodotti=create cbx_prodotti
this.st_data_fine=create st_data_fine
this.st_data_inizio=create st_data_inizio
this.cb_esplodi_tes=create cb_esplodi_tes
this.em_data_fine=create em_data_fine
this.em_data_inizio=create em_data_inizio
this.st_esplodi_testata=create st_esplodi_testata
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.st_4=create st_4
this.st_5=create st_5
this.st_6=create st_6
this.st_7=create st_7
this.st_filtri_disponibili=create st_filtri_disponibili
this.st_9=create st_9
this.dw_1=create dw_1
this.dw_2=create dw_2
this.dw_3=create dw_3
this.dw_4=create dw_4
this.dw_5=create dw_5
this.dw_6=create dw_6
this.dw_7=create dw_7
this.gb_1=create gb_1
this.gb_2=create gb_2
this.gb_3=create gb_3
this.Control[]={this.st_20,&
this.st_19,&
this.st_8,&
this.dw_8,&
this.cbx_prodotti,&
this.st_data_fine,&
this.st_data_inizio,&
this.cb_esplodi_tes,&
this.em_data_fine,&
this.em_data_inizio,&
this.st_esplodi_testata,&
this.st_1,&
this.st_2,&
this.st_3,&
this.st_4,&
this.st_5,&
this.st_6,&
this.st_7,&
this.st_filtri_disponibili,&
this.st_9,&
this.dw_1,&
this.dw_2,&
this.dw_3,&
this.dw_4,&
this.dw_5,&
this.dw_6,&
this.dw_7,&
this.gb_1,&
this.gb_2,&
this.gb_3}
end on

on tabpage_2.destroy
destroy(this.st_20)
destroy(this.st_19)
destroy(this.st_8)
destroy(this.dw_8)
destroy(this.cbx_prodotti)
destroy(this.st_data_fine)
destroy(this.st_data_inizio)
destroy(this.cb_esplodi_tes)
destroy(this.em_data_fine)
destroy(this.em_data_inizio)
destroy(this.st_esplodi_testata)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.st_5)
destroy(this.st_6)
destroy(this.st_7)
destroy(this.st_filtri_disponibili)
destroy(this.st_9)
destroy(this.dw_1)
destroy(this.dw_2)
destroy(this.dw_3)
destroy(this.dw_4)
destroy(this.dw_5)
destroy(this.dw_6)
destroy(this.dw_7)
destroy(this.gb_1)
destroy(this.gb_2)
destroy(this.gb_3)
end on

type st_20 from statictext within tabpage_2
integer x = 133
integer y = 1760
integer width = 850
integer height = 88
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Tipo Anagrafica"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event clicked;if italic = true or ib_consenti_modifica = false then
	return -1
end if	

il_count++

choose case il_count
		
	case 1
		dw_1.insertrow(0)
		is_select = "select distinct(cod_tipo_anagrafica), des_tipo_anagrafica from view_sel_testata order by cod_tipo_anagrafica"
		wf_carica_dropdown(dw_1)
		dw_1.object.dati_t.text = text
		dw_1.visible = true
		dw_1.setfocus()
		cb_salva_filtri.enabled = true
		cb_azzera_testata.enabled = true
		cb_annulla_modifiche.enabled = true
	case 2
		dw_1.object.dati.protect = true
		dw_1.object.t_annulla.visible = false
		dw_2.insertrow(0)
		is_select = "select distinct(cod_tipo_anagrafica), des_tipo_anagrafica from view_sel_testata" + is_where_1 + &
						" order by cod_tipo_anagrafica"
		wf_carica_dropdown(dw_2)
		dw_2.object.dati_t.text = text
		dw_2.visible = true
		dw_2.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 3
		dw_2.object.dati.protect = true
		dw_2.object.t_annulla.visible = false
		dw_3.insertrow(0)
		is_select = "select distinct(cod_tipo_anagrafica), des_tipo_anagrafica from view_sel_testata" + is_where_1 + &
						" order by cod_tipo_anagrafica"
		wf_carica_dropdown(dw_3)
		dw_3.object.dati_t.text = text
		dw_3.visible = true
		dw_3.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 4
		dw_3.object.dati.protect = true
		dw_3.object.t_annulla.visible = false
		dw_4.insertrow(0)
		is_select = "select distinct(cod_tipo_anagrafica), des_tipo_anagrafica from view_sel_testata" + is_where_1 + &
						" order by cod_tipo_anagrafica"
		wf_carica_dropdown(dw_4)
		dw_4.object.dati_t.text = text
		dw_4.visible = true
		dw_4.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 5
		dw_4.object.dati.protect = true
		dw_4.object.t_annulla.visible = false
		dw_5.insertrow(0)
		is_select = "select distinct(cod_tipo_anagrafica), des_tipo_anagrafica from view_sel_testata" + is_where_1 + &
						" order by cod_tipo_anagrafica"
		wf_carica_dropdown(dw_5)
		dw_5.object.dati_t.text = text
		dw_5.visible = true
		dw_5.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 6
		dw_5.object.dati.protect = true
		dw_5.object.t_annulla.visible = false
		dw_6.insertrow(0)
		is_select = "select distinct(cod_tipo_anagrafica), des_tipo_anagrafica from view_sel_testata" + is_where_1 + &
						" order by cod_tipo_anagrafica"
		wf_carica_dropdown(dw_6)
		dw_6.object.dati_t.text = text
		dw_6.visible = true
		dw_6.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 7
		dw_6.object.dati.protect = true
		dw_6.object.t_annulla.visible = false
		dw_7.insertrow(0)
		is_select = "select distinct(cod_tipo_anagrafica), des_tipo_anagrafica from view_sel_testata" + is_where_1 + &
						" order by cod_tipo_anagrafica"
		wf_carica_dropdown(dw_7)
		dw_7.object.dati_t.text = text
		dw_7.visible = true
		dw_7.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 8
		dw_7.object.dati.protect = true
		dw_7.object.t_annulla.visible = false
		dw_8.insertrow(0)
		is_select = "select distinct(cod_tipo_anagrafica), des_tipo_anagrafica from view_sel_testata" + is_where_1 + &
						" order by cod_tipo_anagrafica"
		wf_carica_dropdown(dw_8)
		dw_8.object.dati_t.text = text
		dw_8.visible = true
		dw_8.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true	
		
		
end choose

is_items[il_count] = "cod_tipo_anagrafica"
italic = true
textcolor = rgb(0,0,255)
ib_testata_mod = true
end event

type st_19 from statictext within tabpage_2
integer x = 133
integer y = 1588
integer width = 850
integer height = 88
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Deposito"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event clicked;if italic = true or ib_consenti_modifica = false then
	return -1
end if	

il_count++

choose case il_count
		
	case 1
		dw_1.insertrow(0)
		is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata order by cod_deposito"
		wf_carica_dropdown(dw_1)
		dw_1.object.dati_t.text = text
		dw_1.visible = true
		dw_1.setfocus()
		cb_salva_filtri.enabled = true
		cb_azzera_testata.enabled = true
		cb_annulla_modifiche.enabled = true
	case 2
		dw_1.object.dati.protect = true
		dw_1.object.t_annulla.visible = false
		dw_2.insertrow(0)
		is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata" + is_where_1 + &
						" order by cod_deposito"
		wf_carica_dropdown(dw_2)
		dw_2.object.dati_t.text = text
		dw_2.visible = true
		dw_2.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 3
		dw_2.object.dati.protect = true
		dw_2.object.t_annulla.visible = false
		dw_3.insertrow(0)
		is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata" + is_where_1 + &
						" order by cod_deposito"
		wf_carica_dropdown(dw_3)
		dw_3.object.dati_t.text = text
		dw_3.visible = true
		dw_3.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 4
		dw_3.object.dati.protect = true
		dw_3.object.t_annulla.visible = false
		dw_4.insertrow(0)
		is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata" + is_where_1 + &
						" order by cod_deposito"
		wf_carica_dropdown(dw_4)
		dw_4.object.dati_t.text = text
		dw_4.visible = true
		dw_4.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 5
		dw_4.object.dati.protect = true
		dw_4.object.t_annulla.visible = false
		dw_5.insertrow(0)
		is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata" + is_where_1 + &
						" order by cod_deposito"
		wf_carica_dropdown(dw_5)
		dw_5.object.dati_t.text = text
		dw_5.visible = true
		dw_5.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 6
		dw_5.object.dati.protect = true
		dw_5.object.t_annulla.visible = false
		dw_6.insertrow(0)
		is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata" + is_where_1 + &
						" order by cod_deposito"
		wf_carica_dropdown(dw_6)
		dw_6.object.dati_t.text = text
		dw_6.visible = true
		dw_6.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 7
		dw_6.object.dati.protect = true
		dw_6.object.t_annulla.visible = false
		dw_7.insertrow(0)
		is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata" + is_where_1 + &
						" order by cod_deposito"
		wf_carica_dropdown(dw_7)
		dw_7.object.dati_t.text = text
		dw_7.visible = true
		dw_7.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 8
		dw_7.object.dati.protect = true
		dw_7.object.t_annulla.visible = false
		dw_8.insertrow(0)
		is_select = "select distinct(cod_deposito), des_deposito from view_sel_testata" + is_where_1 + &
						" order by cod_deposito"
		wf_carica_dropdown(dw_8)
		dw_8.object.dati_t.text = text
		dw_8.visible = true
		dw_8.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true	
		
		
end choose

is_items[il_count] = "cod_deposito"
italic = true
textcolor = rgb(0,0,255)
ib_testata_mod = true
end event

type st_8 from statictext within tabpage_2
integer x = 133
integer y = 556
integer width = 850
integer height = 88
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Categoria Cliente"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event doubleclicked;if italic = true or ib_consenti_modifica = false then
	return -1
end if	

il_count++

choose case il_count
		
	case 1
		dw_1.insertrow(0)
		is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata order by clienti_cod_categoria"
		wf_carica_dropdown(dw_1)
		dw_1.object.dati_t.text = text
		dw_1.visible = true
		dw_1.setfocus()
		cb_salva_filtri.enabled = true
		cb_azzera_testata.enabled = true
		cb_annulla_modifiche.enabled = true
	case 2
		dw_1.object.dati.protect = true
		dw_1.object.t_annulla.visible = false
		dw_2.insertrow(0)
		is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata" + is_where_1 + &
						" order by clienti_cod_categoria"
		wf_carica_dropdown(dw_2)
		dw_2.object.dati_t.text = text
		dw_2.visible = true
		dw_2.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 3
		dw_2.object.dati.protect = true
		dw_2.object.t_annulla.visible = false
		dw_3.insertrow(0)
		is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata" + is_where_1 + &
						is_where_2 + " order by clienti_cod_categoria"
		wf_carica_dropdown(dw_3)
		dw_3.object.dati_t.text = text
		dw_3.visible = true
		dw_3.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 4
		dw_3.object.dati.protect = true
		dw_3.object.t_annulla.visible = false
		dw_4.insertrow(0)
		is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + " order by clienti_cod_categoria"
		wf_carica_dropdown(dw_4)
		dw_4.object.dati_t.text = text
		dw_4.visible = true
		dw_4.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 5
		dw_4.object.dati.protect = true
		dw_4.object.t_annulla.visible = false
		dw_5.insertrow(0)
		is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + " order by clienti_cod_categoria"
		wf_carica_dropdown(dw_5)
		dw_5.object.dati_t.text = text
		dw_5.visible = true
		dw_5.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 6
		dw_5.object.dati.protect = true
		dw_5.object.t_annulla.visible = false
		dw_6.insertrow(0)
		is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by clienti_cod_categoria"
		wf_carica_dropdown(dw_6)
		dw_6.object.dati_t.text = text
		dw_6.visible = true
		dw_6.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 7
		dw_6.object.dati.protect = true
		dw_6.object.t_annulla.visible = false
		dw_7.insertrow(0)
		is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by clienti_cod_categoria"
		wf_carica_dropdown(dw_7)
		dw_7.object.dati_t.text = text
		dw_7.visible = true
		dw_7.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 8
		dw_7.object.dati.protect = true
		dw_7.object.t_annulla.visible = false
		dw_8.insertrow(0)
		is_select = "select distinct(clienti_cod_categoria), categorie_des_categoria from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by clienti_cod_categoria"
		wf_carica_dropdown(dw_8)
		dw_8.object.dati_t.text = text
		dw_8.visible = true
		dw_8.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true	
		
end choose

is_items[il_count] = "cod_categoria"
italic = true
textcolor = rgb(0,0,255)
ib_testata_mod = true
end event

type dw_8 from datawindow within tabpage_2
integer x = 1262
integer y = 1448
integer width = 3200
integer height = 80
integer taborder = 30
string title = "none"
string dataobject = "d_statistiche_filtri"
boolean border = false
boolean livescroll = true
end type

event itemchanged;if dwo.name <> "dati" then
	return
end if

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

if isnull(data) then
	is_where_8 = ""
	dw_8.object.t_annulla.visible = false
else
	choose case is_items[8]		
		case "cod_tipo_fat_ven"
			is_where_8 = " and tipi_fat_cod_tipo_fat_ven = '" + data + "'"
		case "cod_cliente"
			is_where_8 = " and clienti_cod_cliente = '" + data + "'"		
		case "flag_tipo_cliente"
			is_where_8 = " and clienti_flag_tipo_cliente = '" + data + "'"
		case "provincia"
			is_where_8 = " and clienti_provincia = '" + data + "'"
		case "cod_agente_1"
			is_where_8 = " and clienti_cod_agente_1 = '" + data + "'"
		case "cod_zona"
			is_where_8 = " and clienti_cod_zona = '" + data + "'"
		case "cod_giro"		
			is_where_8 = " and giri_cod_giro_consegna = '" + data + "'"
		case "cod_categoria"		
			is_where_8 = " and clienti_cod_categoria = '" + data + "'"	
		case "cod_deposito"		
			is_where_8 = " where cod_deposito = '" + data + "'"	
		case "cod_tipo_anagrafica"		
			is_where_8 = " where cod_tipo_anagrafica = '" + data + "'"	
	end choose
	dw_8.object.t_annulla.visible = true
end if
end event

event doubleclicked;if il_count > 8 or dwo.name = "dati" or ib_consenti_modifica = false or rowcount() = 0 then
	return -1
end if

choose case is_items[8]
		
	case "cod_tipo_fat_ven"
		dw_7.object.dati.protect = false
		dw_8.reset()
		dw_8.object.dati_t.text = ""
		st_1.italic = false
		st_1.textcolor = rgb(0,0,0)
	case "cod_cliente"
		dw_7.object.dati.protect = false
		dw_8.reset()
		dw_8.object.dati_t.text = ""
		st_2.italic = false
		st_2.textcolor = rgb(0,0,0)
	case "flag_tipo_cliente"
		dw_7.object.dati.protect = false
		dw_8.reset()
		dw_8.object.dati_t.text = ""
		st_3.italic = false
		st_3.textcolor = rgb(0,0,0)
	case "provincia"
		dw_7.object.dati.protect = false
		dw_8.reset()
		dw_8.object.dati_t.text = ""
		st_4.italic = false
		st_4.textcolor = rgb(0,0,0)
	case "cod_agente_1"
		dw_7.object.dati.protect = false
		dw_8.reset()
		dw_8.object.dati_t.text = ""
		st_5.italic = false
		st_5.textcolor = rgb(0,0,0)
	case "cod_zona"
		dw_7.object.dati.protect = false
		dw_8.reset()
		dw_8.object.dati_t.text = ""
		st_6.italic = false
		st_6.textcolor = rgb(0,0,0)
	case "cod_giro"
		dw_7.object.dati.protect = false
		dw_8.reset()
		dw_8.object.dati_t.text = ""
		st_7.italic = false
		st_7.textcolor = rgb(0,0,0)
	case "cod_categoria"
		dw_7.object.dati.protect = false
		dw_8.reset()
		dw_8.object.dati_t.text = ""
		st_8.italic = false
		st_8.textcolor = rgb(0,0,0)	
	case "cod_deposito"
		dw_7.object.dati.protect = false
		dw_8.reset()
		dw_8.object.dati_t.text = ""
		st_19.italic = false
		st_19.textcolor = rgb(0,0,0)	
	case "cod_tipo_anagrafica"
		dw_7.object.dati.protect = false
		dw_8.reset()
		dw_8.object.dati_t.text = ""
		st_20.italic = false
		st_20.textcolor = rgb(0,0,0)	
		
end choose		

setnull(is_items[il_count])
is_where_8 = ""
il_count = il_count -1

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

if not isnull(dw_7.getitemstring(1,"dati")) then
	dw_7.object.t_annulla.visible = true
end if
end event

event clicked;string ls_null


if il_count > 8 or ib_consenti_modifica = false or dwo.name <> "t_annulla" then
	return -1
end if

setnull(ls_null)

dw_8.setitem(1,"dati",ls_null)

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

is_where_8 = ""

dw_8.object.t_annulla.visible = false
end event

type cbx_prodotti from checkbox within tabpage_2
integer x = 2907
integer y = 2000
integer width = 919
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Includi prodotti e livelli nel report"
end type

type st_data_fine from statictext within tabpage_2
integer x = 1993
integer y = 2008
integer width = 206
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "A data"
boolean focusrectangle = false
end type

type st_data_inizio from statictext within tabpage_2
integer x = 1033
integer y = 2008
integer width = 229
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Da data"
boolean focusrectangle = false
end type

type cb_esplodi_tes from commandbutton within tabpage_2
integer x = 4142
integer y = 1996
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;long     ll_i

string   ls_select, ls_where, ls_group, ls_sparse, ls_valore, ls_where_2, ls_where_3, ls_where_4, ls_order, ls_titolo

datetime ldt_da_data, ldt_a_data

boolean  lb_cliente, lb_categoria

str_statistiche_vendita lstr_statistiche_vendita

if cbx_prodotti.checked then
//	ls_where = " where view_dati_prodotti.cod_prodotto *= view_livelli_prodotti.cod_prodotto"
//	ls_where = " left outer join view_livelli_prodotti on view_dati_prodotti.cod_prodotto = view_livelli_prodotti.cod_prodotto where "
end if

for ll_i = 1 to il_count
	
	if is_items[ll_i] = "cod_giro" then
		continue
	end if
	
	if is_items[ll_i] = "cod_cliente" then
		lb_cliente = true
	else
		lb_cliente = false
	end if
	
	if is_items[ll_i] = "cod_categoria" then
		lb_categoria = true
	else
		lb_categoria = false
	end if
	
	choose case ll_i
		case 1
			ls_valore = dw_1.getitemstring(1,"dati")
		case 2
			ls_valore = dw_2.getitemstring(1,"dati")
		case 3
			ls_valore = dw_3.getitemstring(1,"dati")
		case 4
			ls_valore = dw_4.getitemstring(1,"dati")
		case 5
			ls_valore = dw_5.getitemstring(1,"dati")
		case 6
			ls_valore = dw_6.getitemstring(1,"dati")
		case 7
			ls_valore = dw_7.getitemstring(1,"dati")
		case 8
			ls_valore = dw_8.getitemstring(1,"dati")	
	end choose
	
	if len(ls_select) < 1 then
		ls_select = "select view_dati_prodotti." + is_items[ll_i]
	else
		ls_select = ls_select + ", view_dati_prodotti." + is_items[ll_i]
	end if
	
	if lb_cliente then
		ls_select = ls_select + ", view_dati_prodotti.cliente_rag_soc"
	end if
	
	if lb_categoria then
		ls_select = ls_select + ", view_dati_prodotti.des_categoria"
	end if
	
	if not isnull(ls_valore) and len(ls_valore) > 0 then 	ls_where = ls_where + " view_dati_prodotti." + is_items[ll_i] + " = '" + ls_valore + "'"
	
	if len(ls_group) < 1 then
		ls_group = " group by view_dati_prodotti." + is_items[ll_i]
	else
		ls_group = ls_group + ", view_dati_prodotti." + is_items[ll_i]
	end if
	
	if lb_cliente then
		ls_group = ls_group + ", view_dati_prodotti.cliente_rag_soc"
	end if
	
	if lb_categoria then
		ls_group = ls_group + ", view_dati_prodotti.des_categoria"
	end if
	
	if len(ls_order) < 1 then
		ls_order = " order by view_dati_prodotti." + is_items[ll_i]
	else
		ls_order = ls_order + ", view_dati_prodotti." + is_items[ll_i]
	end if
	
	if lb_cliente then
		ls_order = ls_order + ", view_dati_prodotti.cliente_rag_soc"
	end if
	
	if lb_categoria then
		ls_order = ls_order + ", view_dati_prodotti.des_categoria"
	end if
	
	if len(ls_sparse) < 1 then
		ls_sparse = "view_dati_prodotti." + is_items[ll_i]
	else	
		ls_sparse = ls_sparse + "~tview_dati_prodotti." + is_items[ll_i]
	end if
	
	if lb_cliente then
		ls_sparse = ls_sparse + "~tview_dati_prodotti.cliente_rag_soc"
	end if
	
	if lb_categoria then
		ls_sparse = ls_sparse + "~tview_dati_prodotti.des_categoria"
	end if
	
	if len(ls_titolo) < 1 then
		ls_titolo = ""
	else	
		ls_titolo = ls_titolo + ", "
	end if
	
	choose case is_items[ll_i]
		case "cod_tipo_fat_ven"
			ls_titolo = ls_titolo + "Tipo fattura"
		case "cod_cliente"
			ls_titolo = ls_titolo + "Cliente"
		case "flag_tipo_cliente"
			ls_titolo = ls_titolo + "Tipo cliente"
		case "provincia"
			ls_titolo = ls_titolo + "Provincia"
		case "cod_agente_1"
			ls_titolo = ls_titolo + "Agente"
		case "cod_zona"
			ls_titolo = ls_titolo + "Zona"
		case "cod_categoria"
			ls_titolo = ls_titolo + "Categoria Cliente"
		case "cod_deposito"
			ls_titolo = ls_titolo + "Stabilimento"
		case "cod_deposito"
			ls_titolo = ls_titolo + "Tipo Anagrafica"
	end choose		
	
next

if cbx_prodotti.checked then
	if len(ls_select) < 1 then
		ls_select = "select des_livello_1, des_livello_2, des_livello_3, des_livello_4, view_dati_prodotti.cod_prodotto, view_dati_prodotti.des_prodotto, cod_misura_mag, sum(quan_fatturata) as quantita, cod_misura_ven, sum(quantita_um) as quan_ven"
	else
		ls_select = ls_select + ", des_livello_1, des_livello_2, des_livello_3, des_livello_4, view_dati_prodotti.cod_prodotto, view_dati_prodotti.des_prodotto, cod_misura_mag, sum(quan_fatturata) as quantita, cod_misura_ven, sum(quantita_um) as quan_ven"
	end if
	if len(ls_group) < 1 then
		ls_group = " group by des_livello_1, des_livello_2, des_livello_3, des_livello_4, view_dati_prodotti.cod_prodotto, view_dati_prodotti.des_prodotto, cod_misura_mag, cod_misura_ven"
	else
		ls_group = ls_group + ", des_livello_1, des_livello_2, des_livello_3, des_livello_4, view_dati_prodotti.cod_prodotto, view_dati_prodotti.des_prodotto, cod_misura_mag, cod_misura_ven"
	end if
	if len(ls_order) < 1 then
		ls_order = " order by des_livello_1, des_livello_2, des_livello_3, des_livello_4, view_dati_prodotti.cod_prodotto, view_dati_prodotti.des_prodotto, cod_misura_mag, cod_misura_ven"
	else
		ls_order = ls_order + ", des_livello_1, des_livello_2, des_livello_3, des_livello_4, view_dati_prodotti.cod_prodotto, view_dati_prodotti.des_prodotto, cod_misura_mag, cod_misura_ven"
	end if
	if len(ls_titolo) < 1 then
		ls_titolo = "Prodotto"
	else	
		ls_titolo = ls_titolo + ", Prodotto"
	end if
	if len(ls_sparse) < 1 then
		ls_sparse = "view_dati_prodotti.cod_prodotto~tview_dati_prodotti.des_prodotto~tview_livelli_prodotti_des_livello_1~tview_livelli_prodotti_des_livello_2~tview_livelli_prodotti_des_livello_3~tview_livelli_prodotti_des_livello_4"
	else	
		ls_sparse = ls_sparse + "~t" + "view_dati_prodotti.cod_prodotto~tview_dati_prodotti.des_prodotto~tview_livelli_prodotti_des_livello_1~tview_livelli_prodotti_des_livello_2~tview_livelli_prodotti_des_livello_3~tview_livelli_prodotti_des_livello_4"
	end if	
end if

if len(ls_select) <= 0 then
	ls_select = " select "
else
	ls_select += ", "
end if


ls_select += " sum( imponibile_iva ) as fatturato"

ls_select += " from view_dati_prodotti"

if len(ls_where) > 0 then ls_where = ls_where + " and"

ls_where = " where " + ls_where + " data_fattura >= '" + string(date(em_data_inizio.text),s_cs_xx.db_funzioni.formato_data) + "'" + &
							 " and data_fattura <= '" + string(date(em_data_fine.text),s_cs_xx.db_funzioni.formato_data) + "'" + &
							 " and flag_sola_iva = 'N'"
						 
// solo note di credito e righe di accredito
ls_where_2 = ls_where + " and (flag_tipo_det_ven <> 'S' and cod_tipo_fat_ven in (" + is_sql_in +")) "

// solo righe di "sconto incondizionato" da fatture
ls_where_3 = ls_where + " and (flag_tipo_det_ven = 'S' and cod_tipo_fat_ven NOT in (" + is_sql_in +")) "

// solo righe di "sconto incondizionato" da note di credito
ls_where_4 = ls_where + " and (flag_tipo_det_ven = 'S' and cod_tipo_fat_ven in (" + is_sql_in +")) "

lstr_statistiche_vendita.sql_origine = ls_select

ls_where = ls_where + " and cod_tipo_fat_ven not in (" + is_sql_pf + ") "

s_cs_xx.parametri.parametro_s_1 = ls_select + ls_where + ls_group + ls_order
lstr_statistiche_vendita.sql_fatture =  ls_select + ls_where + ls_group + ls_order

s_cs_xx.parametri.parametro_s_2 = ls_select + ls_where_2 + ls_group + ls_order
lstr_statistiche_vendita.sql_note_credito =   ls_select + ls_where_2 + ls_group + ls_order

lstr_statistiche_vendita.sql_detrazioni_fatture =   ls_select + ls_where_3 + ls_group + ls_order

lstr_statistiche_vendita.sql_detrazioni_note_credito =   ls_select + ls_where_4 + ls_group + ls_order

s_cs_xx.parametri.parametro_s_3 = ls_sparse
lstr_statistiche_vendita.dw_sparse = ls_sparse

s_cs_xx.parametri.parametro_s_4 = ls_titolo
lstr_statistiche_vendita.dw_titolo = ls_titolo

if em_data_inizio.text = "" or em_data_fine.text = "" then
	setnull(s_cs_xx.parametri.parametro_s_1)
	setnull(lstr_statistiche_vendita.sql_fatture)
else
	s_cs_xx.parametri.parametro_data_1 = datetime(date(em_data_inizio.text))
	s_cs_xx.parametri.parametro_data_2 = datetime(date(em_data_fine.text))
	lstr_statistiche_vendita.data_inizio = datetime(date(em_data_inizio.text))
	lstr_statistiche_vendita.data_fine = datetime(date(em_data_fine.text))
	
end if	

openwithparm(w_stat_esplosione_tes, lstr_statistiche_vendita)
end event

type em_data_fine from editmask within tabpage_2
integer x = 2199
integer y = 1996
integer width = 411
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datetimemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type em_data_inizio from editmask within tabpage_2
integer x = 1262
integer y = 1996
integer width = 411
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datetimemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type st_esplodi_testata from statictext within tabpage_2
integer x = 50
integer y = 2000
integer width = 763
integer height = 72
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Esplosione filtri testata:"
boolean focusrectangle = false
end type

type st_1 from statictext within tabpage_2
integer x = 133
integer y = 212
integer width = 850
integer height = 88
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Tipo fattura di vendita"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event doubleclicked;if italic = true or ib_consenti_modifica = false then
	return -1
end if	

il_count++

choose case il_count
		
	case 1
		dw_1.insertrow(0)		
		is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata order by tipi_fat_cod_tipo_fat_ven"
		wf_carica_dropdown(dw_1)
		dw_1.object.dati_t.text = text
		dw_1.visible = true
		dw_1.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
		cb_azzera_testata.enabled = true
	case 2
		dw_1.object.dati.protect = true
		dw_1.object.t_annulla.visible = false
		dw_2.insertrow(0)
		is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata" + is_where_1 + &
						" order by tipi_fat_cod_tipo_fat_ven"
		wf_carica_dropdown(dw_2)
		dw_2.object.dati_t.text = text
		dw_2.visible = true
		dw_2.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 3
		dw_2.object.dati.protect = true
		dw_2.object.t_annulla.visible = false
		dw_3.insertrow(0)
		is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata" + is_where_1 + &
				      is_where_2 + " order by tipi_fat_cod_tipo_fat_ven"
		wf_carica_dropdown(dw_3)
		dw_3.object.dati_t.text = text
		dw_3.visible = true
		dw_3.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 4
		dw_3.object.dati.protect = true
		dw_3.object.t_annulla.visible = false
		dw_4.insertrow(0)
		is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + " order by tipi_fat_cod_tipo_fat_ven"
		wf_carica_dropdown(dw_4)				
		dw_4.object.dati_t.text = text
		dw_4.visible = true
		dw_4.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 5
		dw_4.object.dati.protect = true
		dw_4.object.t_annulla.visible = false
		dw_5.insertrow(0)
		is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + " order by tipi_fat_cod_tipo_fat_ven"
		wf_carica_dropdown(dw_5)				
		dw_5.object.dati_t.text = text
		dw_5.visible = true
		dw_5.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 6
		dw_5.object.dati.protect = true
		dw_5.object.t_annulla.visible = false
		dw_6.insertrow(0)
		is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by tipi_fat_cod_tipo_fat_ven"
		wf_carica_dropdown(dw_6)				
		dw_6.object.dati_t.text = text
		dw_6.visible = true
		dw_6.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 7
		dw_6.object.dati.protect = true
		dw_6.object.t_annulla.visible = false
		dw_7.insertrow(0)
		is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by tipi_fat_cod_tipo_fat_ven"
		wf_carica_dropdown(dw_7)				
		dw_7.object.dati_t.text = text
		dw_7.visible = true
		dw_7.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 8
		dw_7.object.dati.protect = true
		dw_7.object.t_annulla.visible = false
		dw_8.insertrow(0)
		is_select = "select distinct(tipi_fat_cod_tipo_fat_ven), tipi_fat_des_tipo_fat_ven from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by tipi_fat_cod_tipo_fat_ven"
		wf_carica_dropdown(dw_8)				
		dw_8.object.dati_t.text = text
		dw_8.visible = true
		dw_8.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true	
		
end choose

is_items[il_count] = "cod_tipo_fat_ven"
italic = true
textcolor = rgb(0,0,255)

ib_testata_mod = true
end event

type st_2 from statictext within tabpage_2
integer x = 133
integer y = 900
integer width = 850
integer height = 88
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Cliente"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event doubleclicked;if italic = true or ib_consenti_modifica = false then
	return -1
end if	

il_count++

choose case il_count
		
	case 1
		dw_1.insertrow(0)
		is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata order by clienti_cod_cliente"
		wf_carica_dropdown(dw_1)
		dw_1.object.dati_t.text = text
		dw_1.visible = true
		dw_1.setfocus()
		cb_salva_filtri.enabled = true
		cb_azzera_testata.enabled = true
		cb_annulla_modifiche.enabled = true
	case 2
		dw_1.object.dati.protect = true
		dw_1.object.t_annulla.visible = false
		dw_2.insertrow(0)
		is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata" + is_where_1 + &
						" order by clienti_cod_cliente"
		wf_carica_dropdown(dw_2)
		dw_2.object.dati_t.text = text
		dw_2.visible = true
		dw_2.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 3
		dw_2.object.dati.protect = true
		dw_2.object.t_annulla.visible = false
		dw_3.insertrow(0)
		is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata" + is_where_1 + &
						is_where_2 + " order by clienti_cod_cliente"
		wf_carica_dropdown(dw_3)
		dw_3.object.dati_t.text = text
		dw_3.visible = true
		dw_3.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 4
		dw_3.object.dati.protect = true
		dw_3.object.t_annulla.visible = false
		dw_4.insertrow(0)
		is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + " order by clienti_cod_cliente"
		wf_carica_dropdown(dw_4)
		dw_4.object.dati_t.text = text
		dw_4.visible = true
		dw_4.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 5
		dw_4.object.dati.protect = true
		dw_4.object.t_annulla.visible = false
		dw_5.insertrow(0)
		is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + " order by clienti_cod_cliente"
		wf_carica_dropdown(dw_5)
		dw_5.object.dati_t.text = text
		dw_5.visible = true
		dw_5.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 6
		dw_5.object.dati.protect = true
		dw_5.object.t_annulla.visible = false
		dw_6.insertrow(0)
		is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by clienti_cod_cliente"
		wf_carica_dropdown(dw_6)
		dw_6.object.dati_t.text = text
		dw_6.visible = true
		dw_6.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 7
		dw_6.object.dati.protect = true
		dw_6.object.t_annulla.visible = false
		dw_7.insertrow(0)
		is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by clienti_cod_cliente"
		wf_carica_dropdown(dw_7)
		dw_7.object.dati_t.text = text
		dw_7.visible = true
		dw_7.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 8
		dw_7.object.dati.protect = true
		dw_7.object.t_annulla.visible = false
		dw_8.insertrow(0)
		is_select = "select distinct(clienti_cod_cliente), clienti_rag_soc_1 from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by clienti_cod_cliente"
		wf_carica_dropdown(dw_8)
		dw_8.object.dati_t.text = text
		dw_8.visible = true
		dw_8.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
		
end choose

is_items[il_count] = "cod_cliente"
italic = true
textcolor = rgb(0,0,255)
ib_testata_mod = true
end event

type st_3 from statictext within tabpage_2
integer x = 133
integer y = 384
integer width = 850
integer height = 88
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Tipo cliente"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event doubleclicked;if italic = true or ib_consenti_modifica = false then
	return -1
end if	

il_count++

choose case il_count
		
	case 1
		dw_1.insertrow(0)
		is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata order by 1"
		wf_carica_dropdown(dw_1)
		dw_1.object.dati_t.text = text
		dw_1.visible = true
		dw_1.setfocus()
		cb_salva_filtri.enabled = true
		cb_azzera_testata.enabled = true
		cb_annulla_modifiche.enabled = true
	case 2
		dw_1.object.dati.protect = true
		dw_1.object.t_annulla.visible = false
		dw_2.insertrow(0)
		is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata" + is_where_1 + &
						" order by 1"
		wf_carica_dropdown(dw_2)
		dw_2.object.dati_t.text = text
		dw_2.visible = true
		dw_2.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 3
		dw_2.object.dati.protect = true
		dw_2.object.t_annulla.visible = false
		dw_3.insertrow(0)
		is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata" + is_where_1 + &
						is_where_2 + " order by 1"
		wf_carica_dropdown(dw_3)
		dw_3.object.dati_t.text = text
		dw_3.visible = true
		dw_3.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 4
		dw_3.object.dati.protect = true
		dw_3.object.t_annulla.visible = false
		dw_4.insertrow(0)
		is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + " order by 1"
		wf_carica_dropdown(dw_4)
		dw_4.object.dati_t.text = text
		dw_4.visible = true
		dw_4.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 5
		dw_4.object.dati.protect = true
		dw_4.object.t_annulla.visible = false
		dw_5.insertrow(0)
		is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + " order by 1"
		wf_carica_dropdown(dw_5)
		dw_5.object.dati_t.text = text
		dw_5.visible = true
		dw_5.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 6
		dw_5.object.dati.protect = true
		dw_5.object.t_annulla.visible = false
		dw_6.insertrow(0)
		is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by 1"
		wf_carica_dropdown(dw_6)
		dw_6.object.dati_t.text = text
		dw_6.visible = true
		dw_6.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 7
		dw_6.object.dati.protect = true
		dw_6.object.t_annulla.visible = false
		dw_7.insertrow(0)
		is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by 1"
		wf_carica_dropdown(dw_7)
		dw_7.object.dati_t.text = text
		dw_7.visible = true
		dw_7.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 8
		dw_7.object.dati.protect = true
		dw_7.object.t_annulla.visible = false
		dw_8.insertrow(0)
		is_select = "select distinct(clienti_flag_tipo_cliente), clienti_flag_tipo_cliente from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by 1"
		wf_carica_dropdown(dw_8)
		dw_8.object.dati_t.text = text
		dw_8.visible = true
		dw_8.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true	
		
end choose

is_items[il_count] = "flag_tipo_cliente"
italic = true
textcolor = rgb(0,0,255)
ib_testata_mod = true
end event

type st_4 from statictext within tabpage_2
integer x = 133
integer y = 728
integer width = 850
integer height = 88
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Provincia cliente"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event doubleclicked;if italic = true or ib_consenti_modifica = false then
	return -1
end if	

il_count++

choose case il_count
		
	case 1
		dw_1.insertrow(0)
		is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata order by 1"
		wf_carica_dropdown(dw_1)
		dw_1.object.dati_t.text = text
		dw_1.visible = true
		dw_1.setfocus()
		cb_salva_filtri.enabled = true
		cb_azzera_testata.enabled = true
		cb_annulla_modifiche.enabled = true
	case 2
		dw_1.object.dati.protect = true
		dw_1.object.t_annulla.visible = false
		dw_2.insertrow(0)
		is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata" + is_where_1 + &
						" order by 1"
		wf_carica_dropdown(dw_2)
		dw_2.object.dati_t.text = text
		dw_2.visible = true
		dw_2.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 3
		dw_2.object.dati.protect = true
		dw_2.object.t_annulla.visible = false
		dw_3.insertrow(0)
		is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata" + is_where_1 + &
						is_where_2 + " order by 1"
		wf_carica_dropdown(dw_3)
		dw_3.object.dati_t.text = text
		dw_3.visible = true
		dw_3.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 4
		dw_3.object.dati.protect = true
		dw_3.object.t_annulla.visible = false
		dw_4.insertrow(0)
		is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + " order by 1"
		wf_carica_dropdown(dw_4)
		dw_4.object.dati_t.text = text
		dw_4.visible = true
		dw_4.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 5
		dw_4.object.dati.protect = true
		dw_4.object.t_annulla.visible = false
		dw_5.insertrow(0)
		is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + " order by 1"
		wf_carica_dropdown(dw_5)
		dw_5.object.dati_t.text = text
		dw_5.visible = true
		dw_5.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 6
		dw_5.object.dati.protect = true
		dw_5.object.t_annulla.visible = false
		dw_6.insertrow(0)
		is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by 1"
		wf_carica_dropdown(dw_6)
		dw_6.object.dati_t.text = text
		dw_6.visible = true
		dw_6.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 7
		dw_6.object.dati.protect = true
		dw_6.object.t_annulla.visible = false
		dw_7.insertrow(0)
		is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by 1"
		wf_carica_dropdown(dw_7)
		dw_7.object.dati_t.text = text
		dw_7.visible = true
		dw_7.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 8
		dw_7.object.dati.protect = true
		dw_7.object.t_annulla.visible = false
		dw_8.insertrow(0)
		is_select = "select distinct(clienti_provincia), clienti_provincia from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by 1"
		wf_carica_dropdown(dw_8)
		dw_8.object.dati_t.text = text
		dw_8.visible = true
		dw_8.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true	
		
end choose

is_items[il_count] = "provincia"
italic = true
textcolor = rgb(0,0,255)
ib_testata_mod = true
end event

type st_5 from statictext within tabpage_2
integer x = 133
integer y = 1072
integer width = 850
integer height = 88
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Agente"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event doubleclicked;if italic = true or ib_consenti_modifica = false then
	return -1
end if	

il_count++

choose case il_count
		
	case 1
		dw_1.insertrow(0)
		is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata order by clienti_cod_agente_1"
		wf_carica_dropdown(dw_1)
		dw_1.object.dati_t.text = text
		dw_1.visible = true
		dw_1.setfocus()
		cb_salva_filtri.enabled = true
		cb_azzera_testata.enabled = true
		cb_annulla_modifiche.enabled = true
	case 2
		dw_1.object.dati.protect = true
		dw_1.object.t_annulla.visible = false
		dw_2.insertrow(0)
		is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata" + is_where_1 + &
						" order by clienti_cod_agente_1"
		wf_carica_dropdown(dw_2)
		dw_2.object.dati_t.text = text
		dw_2.visible = true
		dw_2.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 3
		dw_2.object.dati.protect = true
		dw_2.object.t_annulla.visible = false
		dw_3.insertrow(0)
		is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata" + is_where_1 + &
						is_where_2 + " order by clienti_cod_agente_1"
		wf_carica_dropdown(dw_3)
		dw_3.object.dati_t.text = text
		dw_3.visible = true
		dw_3.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 4
		dw_3.object.dati.protect = true
		dw_3.object.t_annulla.visible = false
		dw_4.insertrow(0)
		is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + " order by clienti_cod_agente_1"
		wf_carica_dropdown(dw_4)
		dw_4.object.dati_t.text = text
		dw_4.visible = true
		dw_4.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 5
		dw_4.object.dati.protect = true
		dw_4.object.t_annulla.visible = false
		dw_5.insertrow(0)
		is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + " order by clienti_cod_agente_1"
		wf_carica_dropdown(dw_5)
		dw_5.object.dati_t.text = text
		dw_5.visible = true
		dw_5.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 6
		dw_5.object.dati.protect = true
		dw_5.object.t_annulla.visible = false
		dw_6.insertrow(0)
		is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by clienti_cod_agente_1"
		wf_carica_dropdown(dw_6)
		dw_6.object.dati_t.text = text
		dw_6.visible = true
		dw_6.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 7
		dw_6.object.dati.protect = true
		dw_6.object.t_annulla.visible = false
		dw_7.insertrow(0)
		is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by clienti_cod_agente_1"
		wf_carica_dropdown(dw_7)
		dw_7.object.dati_t.text = text
		dw_7.visible = true
		dw_7.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 8
		dw_7.object.dati.protect = true
		dw_7.object.t_annulla.visible = false
		dw_8.insertrow(0)
		is_select = "select distinct(clienti_cod_agente_1), agenti_des_agente_1 from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by clienti_cod_agente_1"
		wf_carica_dropdown(dw_8)
		dw_8.object.dati_t.text = text
		dw_8.visible = true
		dw_8.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true	
		
end choose

is_items[il_count] = "cod_agente_1"
italic = true
textcolor = rgb(0,0,255)
ib_testata_mod = true
end event

type st_6 from statictext within tabpage_2
integer x = 133
integer y = 1244
integer width = 850
integer height = 88
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Zona"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event doubleclicked;if italic = true or ib_consenti_modifica = false then
	return -1
end if	

il_count++

choose case il_count
		
	case 1
		dw_1.insertrow(0)
		is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata order by clienti_cod_zona"
		wf_carica_dropdown(dw_1)
		dw_1.object.dati_t.text = text
		dw_1.visible = true
		dw_1.setfocus()
		cb_salva_filtri.enabled = true
		cb_azzera_testata.enabled = true
		cb_annulla_modifiche.enabled = true
	case 2
		dw_1.object.dati.protect = true
		dw_1.object.t_annulla.visible = false
		dw_2.insertrow(0)
		is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata" + is_where_1 + &
						" order by clienti_cod_zona"
		wf_carica_dropdown(dw_2)
		dw_2.object.dati_t.text = text
		dw_2.visible = true
		dw_2.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 3
		dw_2.object.dati.protect = true
		dw_2.object.t_annulla.visible = false
		dw_3.insertrow(0)
		is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata" + is_where_1 + &
						is_where_2 + " order by clienti_cod_zona"
		wf_carica_dropdown(dw_3)
		dw_3.object.dati_t.text = text
		dw_3.visible = true
		dw_3.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 4
		dw_3.object.dati.protect = true
		dw_3.object.t_annulla.visible = false
		dw_4.insertrow(0)
		is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + " order by clienti_cod_zona"
		wf_carica_dropdown(dw_4)
		dw_4.object.dati_t.text = text
		dw_4.visible = true
		dw_4.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 5
		dw_4.object.dati.protect = true
		dw_4.object.t_annulla.visible = false
		dw_5.insertrow(0)
		is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + " order by clienti_cod_zona"
		wf_carica_dropdown(dw_5)
		dw_5.object.dati_t.text = text
		dw_5.visible = true
		dw_5.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 6
		dw_5.object.dati.protect = true
		dw_5.object.t_annulla.visible = false
		dw_6.insertrow(0)
		is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by clienti_cod_zona"
		wf_carica_dropdown(dw_6)
		dw_6.object.dati_t.text = text
		dw_6.visible = true
		dw_6.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 7
		dw_6.object.dati.protect = true
		dw_6.object.t_annulla.visible = false
		dw_7.insertrow(0)
		is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by clienti_cod_zona"
		wf_carica_dropdown(dw_7)
		dw_7.object.dati_t.text = text
		dw_7.visible = true
		dw_7.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 8
		dw_7.object.dati.protect = true
		dw_7.object.t_annulla.visible = false
		dw_8.insertrow(0)
		is_select = "select distinct(clienti_cod_zona), zone_des_zona from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by clienti_cod_zona"
		wf_carica_dropdown(dw_8)
		dw_8.object.dati_t.text = text
		dw_8.visible = true
		dw_8.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
		
end choose

italic = true
textcolor = rgb(0,0,255)
is_items[il_count] = "cod_zona"
ib_testata_mod = true
end event

type st_7 from statictext within tabpage_2
integer x = 133
integer y = 1416
integer width = 850
integer height = 88
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Giro"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = styleraised!
boolean focusrectangle = false
end type

event doubleclicked;if italic = true or ib_consenti_modifica = false then
	return -1
end if	

il_count++

choose case il_count
		
	case 1
		dw_1.insertrow(0)
		is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata order by giri_cod_giro_consegna"
		wf_carica_dropdown(dw_1)
		dw_1.object.dati_t.text = text
		dw_1.visible = true
		dw_1.setfocus()
		cb_salva_filtri.enabled = true
		cb_azzera_testata.enabled = true
		cb_annulla_modifiche.enabled = true
	case 2
		dw_1.object.dati.protect = true
		dw_1.object.t_annulla.visible = false
		dw_2.insertrow(0)
		is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata" + is_where_1 + &
						" order by giri_cod_giro_consegna"
		wf_carica_dropdown(dw_2)
		dw_2.object.dati_t.text = text
		dw_2.visible = true
		dw_2.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 3
		dw_2.object.dati.protect = true
		dw_2.object.t_annulla.visible = false
		dw_3.insertrow(0)
		is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata" + is_where_1 + &
						is_where_2 + " order by giri_cod_giro_consegna"
		wf_carica_dropdown(dw_3)
		dw_3.object.dati_t.text = text
		dw_3.visible = true
		dw_3.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 4
		dw_3.object.dati.protect = true
		dw_3.object.t_annulla.visible = false
		dw_4.insertrow(0)
		is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + " order by giri_cod_giro_consegna"
		wf_carica_dropdown(dw_4)
		dw_4.object.dati_t.text = text
		dw_4.visible = true
		dw_4.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 5
		dw_4.object.dati.protect = true
		dw_4.object.t_annulla.visible = false
		dw_5.insertrow(0)
		is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + " order by giri_cod_giro_consegna"
		wf_carica_dropdown(dw_5)
		dw_5.object.dati_t.text = text
		dw_5.visible = true
		dw_5.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 6
		dw_5.object.dati.protect = true
		dw_5.object.t_annulla.visible = false
		dw_6.insertrow(0)
		is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + " order by giri_cod_giro_consegna"
		wf_carica_dropdown(dw_6)
		dw_6.object.dati_t.text = text
		dw_6.visible = true
		dw_6.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 7
		dw_6.object.dati.protect = true
		dw_6.object.t_annulla.visible = false
		dw_7.insertrow(0)
		is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + " order by giri_cod_giro_consegna"
		wf_carica_dropdown(dw_7)
		dw_7.object.dati_t.text = text
		dw_7.visible = true
		dw_7.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true
	case 8
		dw_7.object.dati.protect = true
		dw_7.object.t_annulla.visible = false
		dw_8.insertrow(0)
		is_select = "select distinct(giri_cod_giro_consegna), giri_des_giro_consegna from view_sel_testata" + is_where_1 + &
						is_where_2 + is_where_3 + is_where_4 + is_where_5 + is_where_6 + is_where_7 + " order by giri_cod_giro_consegna"
		wf_carica_dropdown(dw_8)
		dw_8.object.dati_t.text = text
		dw_8.visible = true
		dw_8.setfocus()
		cb_salva_filtri.enabled = true
		cb_annulla_modifiche.enabled = true	
		
		
end choose

is_items[il_count] = "cod_giro"
italic = true
textcolor = rgb(0,0,255)
ib_testata_mod = true
end event

type st_filtri_disponibili from statictext within tabpage_2
integer x = 5
integer y = 12
integer width = 1111
integer height = 80
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Filtri disponibili"
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type st_9 from statictext within tabpage_2
integer x = 1170
integer y = 12
integer width = 3378
integer height = 80
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Filtri selezionati"
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type dw_1 from datawindow within tabpage_2
integer x = 1262
integer y = 216
integer width = 3200
integer height = 80
integer taborder = 20
string title = "none"
string dataobject = "d_statistiche_filtri"
boolean border = false
boolean livescroll = true
end type

event doubleclicked;if il_count > 1 or dwo.name = "dati" or ib_consenti_modifica = false or rowcount() = 0 then
	return -1
end if

choose case is_items[1]
		
	case "cod_tipo_fat_ven"
		dw_1.reset()
		dw_1.object.dati_t.text = ""
		st_1.italic = false
		st_1.textcolor = rgb(0,0,0)
	case "cod_cliente"
		dw_1.reset()
		dw_1.object.dati_t.text = ""
		st_2.italic = false
		st_2.textcolor = rgb(0,0,0)
	case "flag_tipo_cliente"
		dw_1.reset()
		dw_1.object.dati_t.text = ""
		st_3.italic = false
		st_3.textcolor = rgb(0,0,0)
	case "provincia"
		dw_1.reset()
		dw_1.object.dati_t.text = ""
		st_4.italic = false
		st_4.textcolor = rgb(0,0,0)
	case "cod_agente_1"
		dw_1.reset()
		dw_1.object.dati_t.text = ""
		st_5.italic = false
		st_5.textcolor = rgb(0,0,0)
	case "cod_zona"
		dw_1.reset()
		dw_1.object.dati_t.text = ""
		st_6.italic = false
		st_6.textcolor = rgb(0,0,0)
	case "cod_giro"
		dw_1.reset()
		dw_1.object.dati_t.text = ""
		st_7.italic = false
		st_7.textcolor = rgb(0,0,0)
	case "cod_categoria"
		dw_1.reset()
		dw_1.object.dati_t.text = ""
		st_8.italic = false
		st_8.textcolor = rgb(0,0,0)	
	case "cod_deposito"
		dw_1.reset()
		dw_1.object.dati_t.text = ""
		st_19.italic = false
		st_19.textcolor = rgb(0,0,0)	
	case "cod_tipo_anagrafica"
		dw_1.reset()
		dw_1.object.dati_t.text = ""
		st_20.italic = false
		st_20.textcolor = rgb(0,0,0)	
		
end choose		

setnull(is_items[il_count])
il_count = il_count -1
is_where_1 = " where 1 = 1"
cb_salva_filtri.enabled = true
cb_annulla_modifiche.enabled = true
cb_azzera_testata.enabled = false
ib_testata_mod = true
end event

event itemchanged;if dwo.name <> "dati" then
	return
end if

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

if isnull(data) then
	is_where_1 = " where 1 = 1"
	dw_1.object.t_annulla.visible = false
else
	choose case is_items[1]		
		case "cod_tipo_fat_ven"
			is_where_1 = " where tipi_fat_cod_tipo_fat_ven = '" + data + "'"
		case "cod_cliente"
			is_where_1 = " where clienti_cod_cliente = '" + data + "'"		
		case "flag_tipo_cliente"
			is_where_1 = " where clienti_flag_tipo_cliente = '" + data + "'"
		case "provincia"
			is_where_1 = " where clienti_provincia = '" + data + "'"
		case "cod_agente_1"
			is_where_1 = " where clienti_cod_agente_1 = '" + data + "'"
		case "cod_zona"
			is_where_1 = " where clienti_cod_zona = '" + data + "'"
		case "cod_giro"		
			is_where_1 = " where giri_cod_giro_consegna = '" + data + "'"
		case "cod_categoria"		
			is_where_1 = " where clienti_cod_categoria = '" + data + "'"	
		case "cod_deposito"		
			is_where_1 = " where cod_deposito = '" + data + "'"	
		case "cod_tipo_anagrafica"		
			is_where_1 = " where cod_tipo_anagrafica = '" + data + "'"	
	end choose
	dw_1.object.t_annulla.visible = true
end if
end event

event clicked;string ls_null


if il_count > 1 or ib_consenti_modifica = false or dwo.name <> "t_annulla" then
	return -1
end if

setnull(ls_null)

dw_1.setitem(1,"dati",ls_null)

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

is_where_1 = " where 1 = 1"

dw_1.object.t_annulla.visible = false
end event

type dw_2 from datawindow within tabpage_2
integer x = 1262
integer y = 392
integer width = 3200
integer height = 80
integer taborder = 20
string title = "none"
string dataobject = "d_statistiche_filtri"
boolean border = false
boolean livescroll = true
end type

event doubleclicked;if il_count > 2 or dwo.name = "dati" or ib_consenti_modifica = false or rowcount() = 0 then
	return -1
end if	

choose case is_items[2]
		
	case "cod_tipo_fat_ven"
		dw_1.object.dati.protect = false
		dw_2.reset()
		dw_2.object.dati_t.text = ""
		st_1.italic = false
		st_1.textcolor = rgb(0,0,0)
	case "cod_cliente"
		dw_1.object.dati.protect = false
		dw_2.reset()
		dw_2.object.dati_t.text = ""
		st_2.italic = false
		st_2.textcolor = rgb(0,0,0)
	case "flag_tipo_cliente"
		dw_1.object.dati.protect = false
		dw_2.reset()
		dw_2.object.dati_t.text = ""
		st_3.italic = false
		st_3.textcolor = rgb(0,0,0)
	case "provincia"
		dw_1.object.dati.protect = false
		dw_2.reset()
		dw_2.object.dati_t.text = ""
		st_4.italic = false
		st_4.textcolor = rgb(0,0,0)
	case "cod_agente_1"
		dw_1.object.dati.protect = false
		dw_2.reset()
		dw_2.object.dati_t.text = ""
		st_5.italic = false
		st_5.textcolor = rgb(0,0,0)
	case "cod_zona"
		dw_1.object.dati.protect = false
		dw_2.reset()
		dw_2.object.dati_t.text = ""
		st_6.italic = false
		st_6.textcolor = rgb(0,0,0)
	case "cod_giro"
		dw_1.object.dati.protect = false
		dw_2.reset()
		dw_2.object.dati_t.text = ""
		st_7.italic = false
		st_7.textcolor = rgb(0,0,0)
	case "cod_categoria"
		dw_1.object.dati.protect = false
		dw_2.reset()
		dw_2.object.dati_t.text = ""
		st_8.italic = false
		st_8.textcolor = rgb(0,0,0)	
	case "cod_deposito"
		dw_1.object.dati.protect = false
		dw_2.reset()
		dw_2.object.dati_t.text = ""
		st_19.italic = false
		st_19.textcolor = rgb(0,0,0)	
	case "cod_tipo_anagrafica"
		dw_1.object.dati.protect = false
		dw_2.reset()
		dw_2.object.dati_t.text = ""
		st_20.italic = false
		st_20.textcolor = rgb(0,0,0)	
		
		
end choose		

setnull(is_items[il_count])
is_where_2 = ""
il_count = il_count -1

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

if not isnull(dw_1.getitemstring(1,"dati")) then
	dw_1.object.t_annulla.visible = true
end if
end event

event itemchanged;if dwo.name <> "dati" then
	return
end if

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

if isnull(data) then
	is_where_2 = ""
	dw_2.object.t_annulla.visible = false
else
	choose case is_items[2]		
		case "cod_tipo_fat_ven"
			is_where_2 = " and tipi_fat_cod_tipo_fat_ven = '" + data + "'"
		case "cod_cliente"
			is_where_2 = " and clienti_cod_cliente = '" + data + "'"		
		case "flag_tipo_cliente"
			is_where_2 = " and clienti_flag_tipo_cliente = '" + data + "'"
		case "provincia"
			is_where_2 = " and clienti_provincia = '" + data + "'"
		case "cod_agente"
			is_where_2 = " and clienti_cod_agente_1 = '" + data + "'"
		case "cod_zona"
			is_where_2 = " and clienti_cod_zona = '" + data + "'"
		case "cod_giro"		
			is_where_2 = " and giri_cod_giro_consegna = '" + data + "'"
		case "cod_categoria"		
			is_where_2 = " and clienti_cod_categoria = '" + data + "'"	
		case "cod_deposito"		
			is_where_2 = " where cod_deposito = '" + data + "'"	
		case "cod_tipo_anagrafica"		
			is_where_2 = " where cod_tipo_anagrafica = '" + data + "'"	
	end choose
	dw_2.object.t_annulla.visible = true
end if
end event

event clicked;string ls_null


if il_count > 2 or ib_consenti_modifica = false or dwo.name <> "t_annulla" then
	return -1
end if

setnull(ls_null)

dw_2.setitem(1,"dati",ls_null)

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

is_where_2 = ""

dw_2.object.t_annulla.visible = false
end event

type dw_3 from datawindow within tabpage_2
integer x = 1262
integer y = 568
integer width = 3200
integer height = 80
integer taborder = 20
string title = "none"
string dataobject = "d_statistiche_filtri"
boolean border = false
boolean livescroll = true
end type

event doubleclicked;if il_count > 3 or dwo.name = "dati" or ib_consenti_modifica = false or rowcount() = 0 then
	return -1
end if	

choose case is_items[3]
		
	case "cod_tipo_fat_ven"
		dw_2.object.dati.protect = false
		dw_3.reset()
		dw_3.object.dati_t.text = ""
		st_1.italic = false
		st_1.textcolor = rgb(0,0,0)
	case "cod_cliente"
		dw_2.object.dati.protect = false
		dw_3.reset()
		dw_3.object.dati_t.text = ""
		st_2.italic = false
		st_2.textcolor = rgb(0,0,0)
	case "flag_tipo_cliente"
		dw_2.object.dati.protect = false
		dw_3.reset()
		dw_3.object.dati_t.text = ""
		st_3.italic = false
		st_3.textcolor = rgb(0,0,0)
	case "provincia"
		dw_2.object.dati.protect = false
		dw_3.reset()
		dw_3.object.dati_t.text = ""
		st_4.italic = false
		st_4.textcolor = rgb(0,0,0)
	case "cod_agente_1"
		dw_2.object.dati.protect = false
		dw_3.reset()
		dw_3.object.dati_t.text = ""
		st_5.italic = false
		st_5.textcolor = rgb(0,0,0)
	case "cod_zona"
		dw_2.object.dati.protect = false
		dw_3.reset()
		dw_3.object.dati_t.text = ""
		st_6.italic = false
		st_6.textcolor = rgb(0,0,0)
	case "cod_giro"
		dw_2.object.dati.protect = false
		dw_3.reset()
		dw_3.object.dati_t.text = ""
		st_7.italic = false
		st_7.textcolor = rgb(0,0,0)
	case "cod_categoria"
		dw_2.object.dati.protect = false
		dw_3.reset()
		dw_3.object.dati_t.text = ""
		st_8.italic = false
		st_8.textcolor = rgb(0,0,0)	
	case "cod_deposito"
		dw_2.object.dati.protect = false
		dw_3.reset()
		dw_3.object.dati_t.text = ""
		st_19.italic = false
		st_19.textcolor = rgb(0,0,0)	
	case "cod_tipo_anagrafica"
		dw_2.object.dati.protect = false
		dw_3.reset()
		dw_3.object.dati_t.text = ""
		st_20.italic = false
		st_20.textcolor = rgb(0,0,0)	
		
end choose		

setnull(is_items[il_count])
is_where_3 = ""
il_count = il_count -1

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

if not isnull(dw_2.getitemstring(1,"dati")) then
	dw_2.object.t_annulla.visible = true
end if
end event

event itemchanged;if dwo.name <> "dati" then
	return
end if

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

if isnull(data) then
	is_where_3 = ""
	dw_3.object.t_annulla.visible = false
else
	choose case is_items[3]		
		case "cod_tipo_fat_ven"
			is_where_3 = " and tipi_fat_cod_tipo_fat_ven = '" + data + "'"
		case "cod_cliente"
			is_where_3 = " and clienti_cod_cliente = '" + data + "'"		
		case "flag_tipo_cliente"
			is_where_3 = " and clienti_flag_tipo_cliente = '" + data + "'"
		case "provincia"
			is_where_3 = " and clienti_provincia = '" + data + "'"
		case "cod_agente_1"
			is_where_3 = " and clienti_cod_agente_1 = '" + data + "'"
		case "cod_zona"
			is_where_3 = " and clienti_cod_zona = '" + data + "'"
		case "cod_giro"		
			is_where_3 = " and giri_cod_giro_consegna = '" + data + "'"
		case "cod_categoria"		
			is_where_3 = " and clienti_cod_categoria = '" + data + "'"	
		case "cod_deposito"		
			is_where_3 = " where cod_deposito = '" + data + "'"	
		case "cod_tipo_anagrafica"		
			is_where_3 = " where cod_tipo_anagrafica = '" + data + "'"	
	end choose
	dw_3.object.t_annulla.visible = true
end if
end event

event clicked;string ls_null


if il_count > 3 or ib_consenti_modifica = false or dwo.name <> "t_annulla" then
	return -1
end if

setnull(ls_null)

dw_3.setitem(1,"dati",ls_null)

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

is_where_3 = ""

dw_3.object.t_annulla.visible = false
end event

type dw_4 from datawindow within tabpage_2
integer x = 1262
integer y = 744
integer width = 3200
integer height = 80
integer taborder = 20
string title = "none"
string dataobject = "d_statistiche_filtri"
boolean border = false
boolean livescroll = true
end type

event doubleclicked;if il_count > 4 or dwo.name = "dati" or ib_consenti_modifica = false or rowcount() = 0 then
	return -1
end if

choose case is_items[4]
		
	case "cod_tipo_fat_ven"
		dw_3.object.dati.protect = false
		dw_4.reset()
		dw_4.object.dati_t.text = ""
		st_1.italic = false
		st_1.textcolor = rgb(0,0,0)
	case "cod_cliente"
		dw_3.object.dati.protect = false
		dw_4.reset()
		dw_4.object.dati_t.text = ""
		st_2.italic = false
		st_2.textcolor = rgb(0,0,0)
	case "flag_tipo_cliente"
		dw_3.object.dati.protect = false
		dw_4.reset()
		dw_4.object.dati_t.text = ""
		st_3.italic = false
		st_3.textcolor = rgb(0,0,0)
	case "provincia"
		dw_3.object.dati.protect = false
		dw_4.reset()
		dw_4.object.dati_t.text = ""
		st_4.italic = false
		st_4.textcolor = rgb(0,0,0)
	case "cod_agente_1"
		dw_3.object.dati.protect = false
		dw_4.reset()
		dw_4.object.dati_t.text = ""
		st_5.italic = false
		st_5.textcolor = rgb(0,0,0)
	case "cod_zona"
		dw_3.object.dati.protect = false
		dw_4.reset()
		dw_4.object.dati_t.text = ""
		st_6.italic = false
		st_6.textcolor = rgb(0,0,0)
	case "cod_giro"
		dw_3.object.dati.protect = false
		dw_4.reset()
		dw_4.object.dati_t.text = ""
		st_7.italic = false
		st_7.textcolor = rgb(0,0,0)
	case "cod_categoria"
		dw_3.object.dati.protect = false
		dw_4.reset()
		dw_4.object.dati_t.text = ""
		st_8.italic = false
		st_8.textcolor = rgb(0,0,0)	
	case "cod_deposito"
		dw_3.object.dati.protect = false
		dw_4.reset()
		dw_4.object.dati_t.text = ""
		st_19.italic = false
		st_19.textcolor = rgb(0,0,0)	
	case "cod_tipo_anagrafica"
		dw_3.object.dati.protect = false
		dw_4.reset()
		dw_4.object.dati_t.text = ""
		st_20.italic = false
		st_20.textcolor = rgb(0,0,0)	
		
end choose		

setnull(is_items[il_count])
is_where_4 = ""
il_count = il_count -1

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

if not isnull(dw_3.getitemstring(1,"dati")) then
	dw_3.object.t_annulla.visible = true
end if
end event

event itemchanged;if dwo.name <> "dati" then
	return
end if

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

if isnull(data) then
	is_where_4 = ""
	dw_4.object.t_annulla.visible = false
else
	choose case is_items[4]		
		case "cod_tipo_fat_ven"
			is_where_4 = " and tipi_fat_cod_tipo_fat_ven = '" + data + "'"
		case "cod_cliente"
			is_where_4 = " and clienti_cod_cliente = '" + data + "'"		
		case "flag_tipo_cliente"
			is_where_4 = " and clienti_flag_tipo_cliente = '" + data + "'"
		case "provincia"
			is_where_4 = " and clienti_provincia = '" + data + "'"
		case "cod_agente_1"
			is_where_4 = " and clienti_cod_agente_1 = '" + data + "'"
		case "cod_zona"
			is_where_4 = " and clienti_cod_zona = '" + data + "'"
		case "cod_giro"		
			is_where_4 = " and giri_cod_giro_consegna = '" + data + "'"
		case "cod_categoria"		
			is_where_4 = " and clienti_cod_categoria = '" + data + "'"	
		case "cod_deposito"		
			is_where_4 = " where cod_deposito = '" + data + "'"	
		case "cod_tipo_anagrafica"		
			is_where_4 = " where cod_tipo_anagrafica = '" + data + "'"	
	end choose
	dw_4.object.t_annulla.visible = true
end if
end event

event clicked;string ls_null


if il_count > 4 or ib_consenti_modifica = false or dwo.name <> "t_annulla" then
	return -1
end if

setnull(ls_null)

dw_4.setitem(1,"dati",ls_null)

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

is_where_4 = ""

dw_4.object.t_annulla.visible = false
end event

type dw_5 from datawindow within tabpage_2
integer x = 1262
integer y = 920
integer width = 3200
integer height = 80
integer taborder = 20
string title = "none"
string dataobject = "d_statistiche_filtri"
boolean border = false
boolean livescroll = true
end type

event doubleclicked;if il_count > 5 or dwo.name = "dati" or ib_consenti_modifica = false or rowcount() = 0 then
	return -1
end if

choose case is_items[5]
		
	case "cod_tipo_fat_ven"
		dw_4.object.dati.protect = false
		dw_5.reset()
		dw_5.object.dati_t.text = ""
		st_1.italic = false
		st_1.textcolor = rgb(0,0,0)
	case "cod_cliente"
		dw_4.object.dati.protect = false
		dw_5.reset()
		dw_5.object.dati_t.text = ""
		st_2.italic = false
		st_2.textcolor = rgb(0,0,0)
	case "flag_tipo_cliente"
		dw_4.object.dati.protect = false
		dw_5.reset()
		dw_5.object.dati_t.text = ""
		st_3.italic = false
		st_3.textcolor = rgb(0,0,0)
	case "provincia"
		dw_4.object.dati.protect = false
		dw_5.reset()
		dw_5.object.dati_t.text = ""
		st_4.italic = false
		st_4.textcolor = rgb(0,0,0)
	case "cod_agente_1"
		dw_4.object.dati.protect = false
		dw_5.reset()
		dw_5.object.dati_t.text = ""
		st_5.italic = false
		st_5.textcolor = rgb(0,0,0)
	case "cod_zona"
		dw_4.object.dati.protect = false
		dw_5.reset()
		dw_5.object.dati_t.text = ""
		st_6.italic = false
		st_6.textcolor = rgb(0,0,0)
	case "cod_giro"
		dw_4.object.dati.protect = false
		dw_5.reset()
		dw_5.object.dati_t.text = ""
		st_7.italic = false
		st_7.textcolor = rgb(0,0,0)
	case "cod_categoria"
		dw_4.object.dati.protect = false
		dw_5.reset()
		dw_5.object.dati_t.text = ""
		st_8.italic = false
		st_8.textcolor = rgb(0,0,0)	
	case "cod_deposito"
		dw_4.object.dati.protect = false
		dw_5.reset()
		dw_5.object.dati_t.text = ""
		st_19.italic = false
		st_19.textcolor = rgb(0,0,0)	
	case "cod_tipo_anagrafica"
		dw_4.object.dati.protect = false
		dw_5.reset()
		dw_5.object.dati_t.text = ""
		st_20.italic = false
		st_20.textcolor = rgb(0,0,0)	

end choose		

setnull(is_items[il_count])
is_where_5 = ""
il_count = il_count -1

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

if not isnull(dw_4.getitemstring(1,"dati")) then
	dw_4.object.t_annulla.visible = true
end if
end event

event itemchanged;if dwo.name <> "dati" then
	return
end if

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

if isnull(data) then
	is_where_5 = ""
	dw_5.object.t_annulla.visible = false
else
	choose case is_items[5]		
		case "cod_tipo_fat_ven"
			is_where_5 = " and tipi_fat_cod_tipo_fat_ven = '" + data + "'"
		case "cod_cliente"
			is_where_5 = " and clienti_cod_cliente = '" + data + "'"		
		case "flag_tipo_cliente"
			is_where_5 = " and clienti_flag_tipo_cliente = '" + data + "'"
		case "provincia"
			is_where_5 = " and clienti_provincia = '" + data + "'"
		case "cod_agente_1"
			is_where_5 = " and clienti_cod_agente_1 = '" + data + "'"
		case "cod_zona"
			is_where_5 = " and clienti_cod_zona = '" + data + "'"
		case "cod_giro"		
			is_where_5 = " and giri_cod_giro_consegna = '" + data + "'"
		case "cod_categoria"		
			is_where_5 = " and clienti_cod_categoria = '" + data + "'"	
		case "cod_deposito"		
			is_where_5 = " where cod_deposito = '" + data + "'"	
		case "cod_tipo_anagrafica"		
			is_where_5 = " where cod_tipo_anagrafica = '" + data + "'"	
	end choose
	dw_5.object.t_annulla.visible = true
end if
end event

event clicked;string ls_null


if il_count > 5 or ib_consenti_modifica = false or dwo.name <> "t_annulla" then
	return -1
end if

setnull(ls_null)

dw_5.setitem(1,"dati",ls_null)

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

is_where_5 = ""

dw_5.object.t_annulla.visible = false
end event

type dw_6 from datawindow within tabpage_2
integer x = 1262
integer y = 1096
integer width = 3200
integer height = 80
integer taborder = 20
string title = "none"
string dataobject = "d_statistiche_filtri"
boolean border = false
boolean livescroll = true
end type

event doubleclicked;if il_count > 6 or dwo.name = "dati" or ib_consenti_modifica = false or rowcount() = 0 then
	return -1
end if

choose case is_items[6]
		
	case "cod_tipo_fat_ven"
		dw_5.object.dati.protect = false
		dw_6.reset()
		dw_6.object.dati_t.text = ""
		st_1.italic = false
		st_1.textcolor = rgb(0,0,0)
	case "cod_cliente"
		dw_5.object.dati.protect = false
		dw_6.reset()
		dw_6.object.dati_t.text = ""
		st_2.italic = false
		st_2.textcolor = rgb(0,0,0)
	case "flag_tipo_cliente"
		dw_5.object.dati.protect = false
		dw_6.reset()
		dw_6.object.dati_t.text = ""
		st_3.italic = false
		st_3.textcolor = rgb(0,0,0)
	case "provincia"
		dw_5.object.dati.protect = false
		dw_6.reset()
		dw_6.object.dati_t.text = ""
		st_4.italic = false
		st_4.textcolor = rgb(0,0,0)
	case "cod_agente_1"
		dw_5.object.dati.protect = false
		dw_6.reset()
		dw_6.object.dati_t.text = ""
		st_5.italic = false
		st_5.textcolor = rgb(0,0,0)
	case "cod_zona"
		dw_5.object.dati.protect = false
		dw_6.reset()
		dw_6.object.dati_t.text = ""
		st_6.italic = false
		st_6.textcolor = rgb(0,0,0)
	case "cod_giro"
		dw_5.object.dati.protect = false
		dw_6.reset()
		dw_6.object.dati_t.text = ""
		st_7.italic = false
		st_7.textcolor = rgb(0,0,0)
	case "cod_categoria"
		dw_5.object.dati.protect = false
		dw_6.reset()
		dw_6.object.dati_t.text = ""
		st_8.italic = false
		st_8.textcolor = rgb(0,0,0)	
	case "cod_deposito"
		dw_5.object.dati.protect = false
		dw_6.reset()
		dw_6.object.dati_t.text = ""
		st_19.italic = false
		st_19.textcolor = rgb(0,0,0)	
	case "cod_tipo_anagrafica"
		dw_5.object.dati.protect = false
		dw_6.reset()
		dw_6.object.dati_t.text = ""
		st_20.italic = false
		st_20.textcolor = rgb(0,0,0)	

end choose		

setnull(is_items[il_count])
is_where_6 = ""
il_count = il_count -1

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

if not isnull(dw_5.getitemstring(1,"dati")) then
	dw_5.object.t_annulla.visible = true
end if
end event

event itemchanged;if dwo.name <> "dati" then
	return
end if

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

if isnull(data) then
	is_where_6 = ""
	dw_6.object.t_annulla.visible = false
else
	choose case is_items[6]		
		case "cod_tipo_fat_ven"
			is_where_6 = " and tipi_fat_cod_tipo_fat_ven = '" + data + "'"
		case "cod_cliente"
			is_where_6 = " and clienti_cod_cliente = '" + data + "'"		
		case "flag_tipo_cliente"
			is_where_6 = " and clienti_flag_tipo_cliente = '" + data + "'"
		case "provincia"
			is_where_6 = " and clienti_provincia = '" + data + "'"
		case "cod_agente_1"
			is_where_6 = " and clienti_cod_agente_1 = '" + data + "'"
		case "cod_zona"
			is_where_6 = " and clienti_cod_zona = '" + data + "'"
		case "cod_giro"		
			is_where_6 = " and giri_cod_giro_consegna = '" + data + "'"
		case "cod_categoria"		
			is_where_6 = " and clienti_cod_categoria = '" + data + "'"	
		case "cod_deposito"		
			is_where_6 = " where cod_deposito = '" + data + "'"	
		case "cod_tipo_anagrafica"		
			is_where_6 = " where cod_tipo_anagrafica = '" + data + "'"	
	end choose
	dw_6.object.t_annulla.visible = true
end if
end event

event clicked;string ls_null


if il_count > 6 or ib_consenti_modifica = false or dwo.name <> "t_annulla" then
	return -1
end if

setnull(ls_null)

dw_6.setitem(1,"dati",ls_null)

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

is_where_6 = ""

dw_6.object.t_annulla.visible = false
end event

type dw_7 from datawindow within tabpage_2
integer x = 1262
integer y = 1272
integer width = 3200
integer height = 80
integer taborder = 20
string title = "none"
string dataobject = "d_statistiche_filtri"
boolean border = false
boolean livescroll = true
end type

event doubleclicked;if il_count > 7 or dwo.name = "dati" or ib_consenti_modifica = false or rowcount() = 0 then
	return -1
end if

choose case is_items[7]
		
	case "cod_tipo_fat_ven"
		dw_6.object.dati.protect = false
		dw_7.reset()
		dw_7.object.dati_t.text = ""
		st_1.italic = false
		st_1.textcolor = rgb(0,0,0)
	case "cod_cliente"
		dw_6.object.dati.protect = false
		dw_7.reset()
		dw_7.object.dati_t.text = ""
		st_2.italic = false
		st_2.textcolor = rgb(0,0,0)
	case "flag_tipo_cliente"
		dw_6.object.dati.protect = false
		dw_7.reset()
		dw_7.object.dati_t.text = ""
		st_3.italic = false
		st_3.textcolor = rgb(0,0,0)
	case "provincia"
		dw_6.object.dati.protect = false
		dw_7.reset()
		dw_7.object.dati_t.text = ""
		st_4.italic = false
		st_4.textcolor = rgb(0,0,0)
	case "cod_agente_1"
		dw_6.object.dati.protect = false
		dw_7.reset()
		dw_7.object.dati_t.text = ""
		st_5.italic = false
		st_5.textcolor = rgb(0,0,0)
	case "cod_zona"
		dw_6.object.dati.protect = false
		dw_7.reset()
		dw_7.object.dati_t.text = ""
		st_6.italic = false
		st_6.textcolor = rgb(0,0,0)
	case "cod_giro"
		dw_6.object.dati.protect = false
		dw_7.reset()
		dw_7.object.dati_t.text = ""
		st_7.italic = false
		st_7.textcolor = rgb(0,0,0)		
	case "cod_categoria"
		dw_6.object.dati.protect = false
		dw_7.reset()
		dw_7.object.dati_t.text = ""
		st_8.italic = false
		st_8.textcolor = rgb(0,0,0)	
	case "cod_deposito"
		dw_6.object.dati.protect = false
		dw_7.reset()
		dw_7.object.dati_t.text = ""
		st_19.italic = false
		st_19.textcolor = rgb(0,0,0)	
	case "cod_tipo_anagrafica"
		dw_6.object.dati.protect = false
		dw_7.reset()
		dw_7.object.dati_t.text = ""
		st_20.italic = false
		st_20.textcolor = rgb(0,0,0)	

end choose		

setnull(is_items[il_count])
is_where_7 = ""
il_count = il_count -1

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

if not isnull(dw_6.getitemstring(1,"dati")) then
	dw_6.object.t_annulla.visible = true
end if
end event

event itemchanged;if dwo.name <> "dati" then
	return
end if

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

if isnull(data) then
	is_where_7 = ""
	dw_7.object.t_annulla.visible = false
else
	choose case is_items[7]		
		case "cod_tipo_fat_ven"
			is_where_7 = " and tipi_fat_cod_tipo_fat_ven = '" + data + "'"
		case "cod_cliente"
			is_where_7 = " and clienti_cod_cliente = '" + data + "'"		
		case "flag_tipo_cliente"
			is_where_7 = " and clienti_flag_tipo_cliente = '" + data + "'"
		case "provincia"
			is_where_7 = " and clienti_provincia = '" + data + "'"
		case "cod_agente_1"
			is_where_7 = " and clienti_cod_agente_1 = '" + data + "'"
		case "cod_zona"
			is_where_7 = " and clienti_cod_zona = '" + data + "'"
		case "cod_giro"		
			is_where_7 = " and giri_cod_giro_consegna = '" + data + "'"
		case "cod_categoria"		
			is_where_7 = " and clienti_cod_categoria = '" + data + "'"	
		case "cod_deposito"		
			is_where_7 = " where cod_deposito = '" + data + "'"	
		case "cod_tipo_anagrafica"		
			is_where_7 = " where cod_tipo_anagrafica = '" + data + "'"	
	end choose
	dw_7.object.t_annulla.visible = true
end if
end event

event clicked;string ls_null


if il_count > 7 or ib_consenti_modifica = false or dwo.name <> "t_annulla" then
	return -1
end if

setnull(ls_null)

dw_7.setitem(1,"dati",ls_null)

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_testata_mod = true

is_where_7 = ""

dw_7.object.t_annulla.visible = false
end event

type gb_1 from groupbox within tabpage_2
integer x = 5
integer y = 84
integer width = 1115
integer height = 1852
integer taborder = 30
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_2 from groupbox within tabpage_2
integer x = 1170
integer y = 84
integer width = 3383
integer height = 1852
integer taborder = 30
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_3 from groupbox within tabpage_2
integer x = 5
integer y = 1936
integer width = 4549
integer height = 164
integer taborder = 30
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
end type

type tabpage_3 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 4558
integer height = 2116
long backcolor = 12632256
string text = "Livelli Prodotti"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
st_16 st_16
st_12 st_12
cb_superiore cb_superiore
st_liv_scelto_5 st_liv_scelto_5
st_liv_scelto_4 st_liv_scelto_4
st_livello_4 st_livello_4
st_liv_scelto_2 st_liv_scelto_2
st_liv_scelto_1 st_liv_scelto_1
st_livello_5 st_livello_5
st_liv_scelto_3 st_liv_scelto_3
st_livello_3 st_livello_3
st_livello_2 st_livello_2
st_livello_1 st_livello_1
dw_prod_statistiche_livelli dw_prod_statistiche_livelli
gb_4 gb_4
end type

on tabpage_3.create
this.st_16=create st_16
this.st_12=create st_12
this.cb_superiore=create cb_superiore
this.st_liv_scelto_5=create st_liv_scelto_5
this.st_liv_scelto_4=create st_liv_scelto_4
this.st_livello_4=create st_livello_4
this.st_liv_scelto_2=create st_liv_scelto_2
this.st_liv_scelto_1=create st_liv_scelto_1
this.st_livello_5=create st_livello_5
this.st_liv_scelto_3=create st_liv_scelto_3
this.st_livello_3=create st_livello_3
this.st_livello_2=create st_livello_2
this.st_livello_1=create st_livello_1
this.dw_prod_statistiche_livelli=create dw_prod_statistiche_livelli
this.gb_4=create gb_4
this.Control[]={this.st_16,&
this.st_12,&
this.cb_superiore,&
this.st_liv_scelto_5,&
this.st_liv_scelto_4,&
this.st_livello_4,&
this.st_liv_scelto_2,&
this.st_liv_scelto_1,&
this.st_livello_5,&
this.st_liv_scelto_3,&
this.st_livello_3,&
this.st_livello_2,&
this.st_livello_1,&
this.dw_prod_statistiche_livelli,&
this.gb_4}
end on

on tabpage_3.destroy
destroy(this.st_16)
destroy(this.st_12)
destroy(this.cb_superiore)
destroy(this.st_liv_scelto_5)
destroy(this.st_liv_scelto_4)
destroy(this.st_livello_4)
destroy(this.st_liv_scelto_2)
destroy(this.st_liv_scelto_1)
destroy(this.st_livello_5)
destroy(this.st_liv_scelto_3)
destroy(this.st_livello_3)
destroy(this.st_livello_2)
destroy(this.st_livello_1)
destroy(this.dw_prod_statistiche_livelli)
destroy(this.gb_4)
end on

type st_16 from statictext within tabpage_3
integer x = 3118
integer y = 12
integer width = 1435
integer height = 80
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Livelli selezionati"
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type st_12 from statictext within tabpage_3
integer x = 5
integer y = 12
integer width = 3086
integer height = 80
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Livelli disponibili"
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type cb_superiore from commandbutton within tabpage_3
integer x = 3653
integer y = 1976
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Superiore"
end type

event clicked;choose case il_livello
		
	case 1
		if tab_1.tabpage_4.dw_prod_statistiche_selez.rowcount() > 1 then
			if g_mb.messagebox("APICE","Non impostando alcun livello è possibile selezionare un solo prodotto. " + &
							  "Attualmente vi sono più prodotti selezionati, se si procede tali prodotti saranno" + &
							  "deselezionati.~nContinuare?",question!,yesno!,2) = 2 then
				return
			end if
			cb_azzera_prodotti.triggerevent("clicked")
		end if
		cb_superiore.enabled = false
		cb_azzera_livelli.enabled = false
		setnull(is_livello_1)
		st_liv_scelto_1.text = ""
		st_livello_1.textcolor = rgb(0,0,0)
		st_livello_1.italic = false
		tab_1.tabpage_3.dw_prod_statistiche_livelli.setsqlselect("select distinct livello_1, " + &
						 "des_livello_1 " + &
						 "from view_livelli_prodotti " + &
						 "where livello_1 is not null " + &
						 "order by livello_1")
		tab_1.tabpage_3.dw_prod_statistiche_livelli.retrieve()		
	case 2
		setnull(is_livello_2)
		st_liv_scelto_2.text = ""
		st_livello_2.textcolor = rgb(0,0,0)
		st_livello_2.italic = false
		tab_1.tabpage_3.dw_prod_statistiche_livelli.setsqlselect("select distinct livello_2, " + &
						 "des_livello_2 " + &
						 "from view_livelli_prodotti " + &
						 "where livello_2 is not null " + &
						 "and livello_1 = '" + is_livello_1 + "' " + &
						 "order by livello_2")
		tab_1.tabpage_3.dw_prod_statistiche_livelli.retrieve()				 
	case 3
		setnull(is_livello_3)
		st_liv_scelto_3.text = ""
		st_livello_3.textcolor = rgb(0,0,0)
		st_livello_3.italic = false
		tab_1.tabpage_3.dw_prod_statistiche_livelli.setsqlselect("select distinct livello_3, " + &
						 "des_livello_3 " + &
						 "from view_livelli_prodotti " + &
						 "where livello_3 is not null " + &
						 "and livello_1 = '" + is_livello_1 + "' " + &
						 "and livello_2 = '" + is_livello_2 + "' " + &
						 "order by livello_3")
		tab_1.tabpage_3.dw_prod_statistiche_livelli.retrieve()				 
	case 4
		setnull(is_livello_4)
		st_liv_scelto_4.text = ""
		st_livello_4.textcolor = rgb(0,0,0)
		st_livello_4.italic = false
		tab_1.tabpage_3.dw_prod_statistiche_livelli.setsqlselect("select distinct livello_4, " + &
						 "des_livello_4 " + &
						 "from view_livelli_prodotti " + &
						 "where livello_4 is not null " + &
						 "and livello_1 = '" + is_livello_1 + "' " + &
						 "and livello_2 = '" + is_livello_2 + "' " + &
						 "and livello_3 = '" + is_livello_3 + "' " + &
						 "order by livello_4")
		tab_1.tabpage_3.dw_prod_statistiche_livelli.retrieve()				 
	case 5
		setnull(is_livello_5)
		st_liv_scelto_5.text = ""
		st_livello_5.textcolor = rgb(0,0,0)
		st_livello_5.italic = false
		tab_1.tabpage_3.dw_prod_statistiche_livelli.setsqlselect("select distinct livello_5, " + &
						 "des_livello_5 " + &
						 "from view_livelli_prodotti " + &
						 "where livello_5 is not null " + &
						 "and livello_1 = '" + is_livello_1 + "' " + &
						 "and livello_2 = '" + is_livello_2 + "' " + &
						 "and livello_3 = '" + is_livello_3 + "' " + &
						 "and livello_4 = '" + is_livello_3 + "' " + &
						 "order by livello_5")
		tab_1.tabpage_3.dw_prod_statistiche_livelli.retrieve()
						 
end choose

il_livello = il_livello - 1

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_livelli_mod = true
end event

type st_liv_scelto_5 from statictext within tabpage_3
integer x = 3159
integer y = 1672
integer width = 1349
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_liv_scelto_4 from statictext within tabpage_3
integer x = 3163
integer y = 1304
integer width = 1349
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_livello_4 from statictext within tabpage_3
integer x = 3634
integer y = 1244
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Livello 4"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_liv_scelto_2 from statictext within tabpage_3
integer x = 3163
integer y = 576
integer width = 1349
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_liv_scelto_1 from statictext within tabpage_3
integer x = 3163
integer y = 212
integer width = 1349
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_livello_5 from statictext within tabpage_3
integer x = 3634
integer y = 1608
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Livello 5"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_liv_scelto_3 from statictext within tabpage_3
integer x = 3163
integer y = 940
integer width = 1349
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_livello_3 from statictext within tabpage_3
integer x = 3634
integer y = 880
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Livello 3"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_livello_2 from statictext within tabpage_3
integer x = 3634
integer y = 516
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Livello 2"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_livello_1 from statictext within tabpage_3
integer x = 3634
integer y = 152
integer width = 402
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Livello 1"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_prod_statistiche_livelli from datawindow within tabpage_3
integer x = 5
integer y = 112
integer width = 3086
integer height = 1992
integer taborder = 20
boolean enabled = false
string title = "none"
string dataobject = "d_prod_statistiche_livelli"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;if isnull(row) or row = 0 or ib_consenti_modifica = false then
	return
end if

il_livello++

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ib_livelli_mod = true

choose case il_livello
		
	case 1
		cb_superiore.enabled = true
		cb_azzera_livelli.enabled = true
		is_livello_1 = getitemstring(row,"livello")
		st_liv_scelto_1.text = getitemstring(row,"des_livello")
		st_livello_1.textcolor = rgb(0,0,255)
		st_livello_1.italic = true
		setsqlselect("select distinct livello_2, " + &
						 "des_livello_2 " + &
						 "from view_livelli_prodotti " + &
						 "where livello_2 is not null " + &
						 "and livello_1 = '" + is_livello_1 + "' " + &
						 "order by livello_2")
		retrieve()		
	case 2
		is_livello_2 = getitemstring(row,"livello")
		st_liv_scelto_2.text = getitemstring(row,"des_livello")
		st_livello_2.textcolor = rgb(0,0,255)
		st_livello_2.italic = true
		setsqlselect("select distinct livello_3, " + &
						 "des_livello_3 " + &
						 "from view_livelli_prodotti " + &
						 "where livello_3 is not null " + &
						 "and livello_1 = '" + is_livello_1 + "' " + &
						 "and livello_2 = '" + is_livello_2 + "' " + &
						 "order by livello_3")
		retrieve()				 
	case 3
		is_livello_3 = getitemstring(row,"livello")
		st_liv_scelto_3.text = getitemstring(row,"des_livello")
		st_livello_3.textcolor = rgb(0,0,255)
		st_livello_3.italic = true
		setsqlselect("select distinct livello_4, " + &
						 "des_livello_4 " + &
						 "from view_livelli_prodotti " + &
						 "where livello_4 is not null " + &
						 "and livello_1 = '" + is_livello_1 + "' " + &
						 "and livello_2 = '" + is_livello_2 + "' " + &
						 "and livello_3 = '" + is_livello_3 + "' " + &
						 "order by livello_4")
		retrieve()				 
	case 4
		is_livello_4 = getitemstring(row,"livello")
		st_liv_scelto_4.text = getitemstring(row,"des_livello")
		st_livello_4.textcolor = rgb(0,0,255)
		st_livello_4.italic = true
		setsqlselect("select distinct livello_5, " + &
						 "des_livello_5 " + &
						 "from view_livelli_prodotti " + &
						 "where livello_5 is not null " + &
						 "and livello_1 = '" + is_livello_1 + "' " + &
						 "and livello_2 = '" + is_livello_2 + "' " + &
						 "and livello_3 = '" + is_livello_3 + "' " + &
						 "and livello_4 = '" + is_livello_4 + "' " + &
						 "order by livello_5")
		retrieve()				 
	case 5
		is_livello_5 = getitemstring(row,"livello")
		st_liv_scelto_5.text = getitemstring(row,"des_livello")
		st_livello_5.textcolor = rgb(0,0,255)
		st_livello_5.italic = true
						 
end choose
end event

type gb_4 from groupbox within tabpage_3
integer x = 3118
integer y = 80
integer width = 1435
integer height = 2024
integer taborder = 30
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
end type

type tabpage_4 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 4558
integer height = 2116
long backcolor = 12632256
string text = "Prodotti"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
pb_togli_tutti pb_togli_tutti
pb_togli_corrente pb_togli_corrente
pb_aggiungi_corrente pb_aggiungi_corrente
pb_aggiungi_tutti pb_aggiungi_tutti
st_livello st_livello
st_prod_selez st_prod_selez
st_15 st_15
st_prod_disponibili st_prod_disponibili
st_11 st_11
dw_prod_statistiche_selez dw_prod_statistiche_selez
cbx_senza_livello cbx_senza_livello
dw_prod_statistiche_elenco dw_prod_statistiche_elenco
gb_6 gb_6
gb_7 gb_7
gb_5 gb_5
gb_10 gb_10
end type

on tabpage_4.create
this.pb_togli_tutti=create pb_togli_tutti
this.pb_togli_corrente=create pb_togli_corrente
this.pb_aggiungi_corrente=create pb_aggiungi_corrente
this.pb_aggiungi_tutti=create pb_aggiungi_tutti
this.st_livello=create st_livello
this.st_prod_selez=create st_prod_selez
this.st_15=create st_15
this.st_prod_disponibili=create st_prod_disponibili
this.st_11=create st_11
this.dw_prod_statistiche_selez=create dw_prod_statistiche_selez
this.cbx_senza_livello=create cbx_senza_livello
this.dw_prod_statistiche_elenco=create dw_prod_statistiche_elenco
this.gb_6=create gb_6
this.gb_7=create gb_7
this.gb_5=create gb_5
this.gb_10=create gb_10
this.Control[]={this.pb_togli_tutti,&
this.pb_togli_corrente,&
this.pb_aggiungi_corrente,&
this.pb_aggiungi_tutti,&
this.st_livello,&
this.st_prod_selez,&
this.st_15,&
this.st_prod_disponibili,&
this.st_11,&
this.dw_prod_statistiche_selez,&
this.cbx_senza_livello,&
this.dw_prod_statistiche_elenco,&
this.gb_6,&
this.gb_7,&
this.gb_5,&
this.gb_10}
end on

on tabpage_4.destroy
destroy(this.pb_togli_tutti)
destroy(this.pb_togli_corrente)
destroy(this.pb_aggiungi_corrente)
destroy(this.pb_aggiungi_tutti)
destroy(this.st_livello)
destroy(this.st_prod_selez)
destroy(this.st_15)
destroy(this.st_prod_disponibili)
destroy(this.st_11)
destroy(this.dw_prod_statistiche_selez)
destroy(this.cbx_senza_livello)
destroy(this.dw_prod_statistiche_elenco)
destroy(this.gb_6)
destroy(this.gb_7)
destroy(this.gb_5)
destroy(this.gb_10)
end on

type pb_togli_tutti from commandbutton within tabpage_4
integer x = 3273
integer y = 2020
integer width = 320
integer height = 68
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "<<"
end type

event clicked;if ib_consenti_modifica then
	cb_azzera_prodotti.triggerevent("clicked")
end if	
end event

type pb_togli_corrente from commandbutton within tabpage_4
integer x = 2953
integer y = 2020
integer width = 320
integer height = 68
integer taborder = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "<"
end type

event clicked;long row


row = tab_1.tabpage_4.dw_prod_statistiche_selez.getrow()

if isnull(row) or row = 0 or ib_consenti_modifica = false then
	return
end if

il_prodotti = il_prodotti - 1

st_prod_selez.text = string(il_prodotti)

if il_prodotti = 0 then
	cb_azzera_prodotti.enabled = false
end if

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

tab_1.tabpage_4.dw_prod_statistiche_selez.deleterow(row)

ib_prodotti_mod = true
end event

type pb_aggiungi_corrente from commandbutton within tabpage_4
integer x = 2633
integer y = 2020
integer width = 320
integer height = 68
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = ">"
end type

event clicked;long ll_riga, ll_i, row


row = tab_1.tabpage_4.dw_prod_statistiche_elenco.getrow()

if isnull(row) or row = 0 or ib_consenti_modifica = false then
	return
end if

for ll_i = 1 to dw_prod_statistiche_selez.rowcount()
	if tab_1.tabpage_4.dw_prod_statistiche_elenco.getitemstring(row,"cod_prodotto") = dw_prod_statistiche_selez.getitemstring(ll_i,"cod_prodotto") then
		return
	end if	
next	

il_prodotti++

st_prod_selez.text = string(il_prodotti)

if il_prodotti = 1 then
	cb_azzera_prodotti.enabled = true
end if	

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ll_riga = dw_prod_statistiche_selez.insertrow(0)

dw_prod_statistiche_selez.setitem(ll_riga,"cod_prodotto",tab_1.tabpage_4.dw_prod_statistiche_elenco.getitemstring(row,"cod_prodotto"))

dw_prod_statistiche_selez.setitem(ll_riga,"des_prodotto",tab_1.tabpage_4.dw_prod_statistiche_elenco.getitemstring(row,"des_prodotto"))

ib_prodotti_mod = true
end event

type pb_aggiungi_tutti from commandbutton within tabpage_4
integer x = 2313
integer y = 2020
integer width = 320
integer height = 68
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = ">>"
end type

event clicked;long    ll_riga, ll_j, row
string  ls_cod_prodotto
boolean lb_trovato


if ib_consenti_modifica = false then
	return
end if

if text = "||" then
	ib_ferma_prodotti = true
	return 0
end if

ib_ferma_prodotti = false

setpointer(hourglass!)

text = "||"

for row = 1 to tab_1.tabpage_4.dw_prod_statistiche_elenco.rowcount()
	
	yield()
	
	if ib_ferma_prodotti then
		setpointer(arrow!)
		text = ">>"
		ib_ferma_prodotti = false
		return 0
	end if	
	
	ls_cod_prodotto = tab_1.tabpage_4.dw_prod_statistiche_elenco.getitemstring(row,"cod_prodotto")
	
	lb_trovato = false
	
	for ll_j = 1 to dw_prod_statistiche_selez.rowcount()
		
		if ls_cod_prodotto = tab_1.tabpage_4.dw_prod_statistiche_selez.getitemstring(ll_j,"cod_prodotto") then
			lb_trovato = true
			exit
		end if
		
	next
	
	if lb_trovato then
		continue
	end if
	
	ll_riga = dw_prod_statistiche_selez.insertrow(0)
	
	dw_prod_statistiche_selez.setitem(ll_riga,"cod_prodotto",ls_cod_prodotto)

	dw_prod_statistiche_selez.setitem(ll_riga,"des_prodotto",tab_1.tabpage_4.dw_prod_statistiche_elenco.getitemstring(row,"des_prodotto"))

	il_prodotti++

	st_prod_selez.text = string(il_prodotti)

	if il_prodotti = 1 then
		cb_azzera_prodotti.enabled = true
	end if	

	cb_salva_filtri.enabled = true

	cb_annulla_modifiche.enabled = true
	
	ib_prodotti_mod = true
	
next

setpointer(arrow!)

text = ">>"

return 0
end event

type st_livello from statictext within tabpage_4
integer x = 5
integer y = 12
integer width = 4544
integer height = 80
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type st_prod_selez from statictext within tabpage_4
integer x = 4183
integer y = 2020
integer width = 352
integer height = 68
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_15 from statictext within tabpage_4
integer x = 3648
integer y = 2024
integer width = 526
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Prodotti selezionati:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_prod_disponibili from statictext within tabpage_4
integer x = 1897
integer y = 2020
integer width = 352
integer height = 68
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_11 from statictext within tabpage_4
integer x = 1385
integer y = 2024
integer width = 507
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Prodotti disponibili:"
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_prod_statistiche_selez from datawindow within tabpage_4
integer x = 2290
integer y = 112
integer width = 2263
integer height = 1872
integer taborder = 20
boolean enabled = false
string title = "none"
string dataobject = "d_prod_statistiche_selez"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;if isnull(row) or row = 0 or ib_consenti_modifica = false then
	return
end if

il_prodotti = il_prodotti - 1

st_prod_selez.text = string(il_prodotti)

if il_prodotti = 1 then	tab_1.tabpage_5.visible = true

if il_prodotti = 0 then
	cb_azzera_prodotti.enabled = false
end if

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

deleterow(row)

ib_prodotti_mod = true
end event

type cbx_senza_livello from checkbox within tabpage_4
integer x = 27
integer y = 2020
integer width = 1271
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Includi nell~'elenco anche i prodotti senza livelli"
end type

event clicked;wf_carica_elenco_prodotti()
end event

type dw_prod_statistiche_elenco from datawindow within tabpage_4
integer x = 5
integer y = 112
integer width = 2263
integer height = 1872
integer taborder = 20
boolean enabled = false
string title = "none"
string dataobject = "d_prod_statistiche_elenco"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;long ll_riga, ll_i

if isnull(row) or row = 0 or ib_consenti_modifica = false then
	return
end if

for ll_i = 1 to dw_prod_statistiche_selez.rowcount()
	if getitemstring(row,"cod_prodotto") = dw_prod_statistiche_selez.getitemstring(ll_i,"cod_prodotto") then return
next

il_prodotti++

st_prod_selez.text = string(il_prodotti)

if il_prodotti = 1 then
	cb_azzera_prodotti.enabled = true
end if

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

ll_riga = dw_prod_statistiche_selez.insertrow(0)

dw_prod_statistiche_selez.setitem(ll_riga,"cod_prodotto",getitemstring(row,"cod_prodotto"))

dw_prod_statistiche_selez.setitem(ll_riga,"des_prodotto",getitemstring(row,"des_prodotto"))

ib_prodotti_mod = true
end event

type gb_6 from groupbox within tabpage_4
integer x = 5
integer y = 1976
integer width = 1330
integer height = 132
integer taborder = 90
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_7 from groupbox within tabpage_4
integer x = 2290
integer y = 1976
integer width = 1330
integer height = 132
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_5 from groupbox within tabpage_4
integer x = 3639
integer y = 1976
integer width = 919
integer height = 132
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_10 from groupbox within tabpage_4
integer x = 1353
integer y = 1976
integer width = 919
integer height = 132
integer taborder = 100
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type tabpage_5 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 4558
integer height = 2116
long backcolor = 12632256
string text = "Caratteristiche"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
st_prodotto st_prodotto
gb_8 gb_8
gb_9 gb_9
dw_prod_statistiche_carat dw_prod_statistiche_carat
cbx_esplosione_carat cbx_esplosione_carat
st_da_data st_da_data
em_da_data em_da_data
st_a_data st_a_data
em_a_data em_a_data
cb_esegui cb_esegui
end type

on tabpage_5.create
this.st_prodotto=create st_prodotto
this.gb_8=create gb_8
this.gb_9=create gb_9
this.dw_prod_statistiche_carat=create dw_prod_statistiche_carat
this.cbx_esplosione_carat=create cbx_esplosione_carat
this.st_da_data=create st_da_data
this.em_da_data=create em_da_data
this.st_a_data=create st_a_data
this.em_a_data=create em_a_data
this.cb_esegui=create cb_esegui
this.Control[]={this.st_prodotto,&
this.gb_8,&
this.gb_9,&
this.dw_prod_statistiche_carat,&
this.cbx_esplosione_carat,&
this.st_da_data,&
this.em_da_data,&
this.st_a_data,&
this.em_a_data,&
this.cb_esegui}
end on

on tabpage_5.destroy
destroy(this.st_prodotto)
destroy(this.gb_8)
destroy(this.gb_9)
destroy(this.dw_prod_statistiche_carat)
destroy(this.cbx_esplosione_carat)
destroy(this.st_da_data)
destroy(this.em_da_data)
destroy(this.st_a_data)
destroy(this.em_a_data)
destroy(this.cb_esegui)
end on

type st_prodotto from statictext within tabpage_5
integer x = 5
integer y = 12
integer width = 4544
integer height = 80
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type gb_8 from groupbox within tabpage_5
integer x = 5
integer y = 1952
integer width = 4549
integer height = 156
integer taborder = 50
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type gb_9 from groupbox within tabpage_5
integer x = 5
integer y = 84
integer width = 4549
integer height = 1868
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
end type

type dw_prod_statistiche_carat from datawindow within tabpage_5
integer x = 23
integer y = 124
integer width = 4512
integer height = 1812
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_prod_statistiche_carat"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event doubleclicked;string ls_null

if isnull(row) or row = 0 or row <> rowcount() or ib_consenti_modifica = false or dwo.name <> "zona_click" then
	return
end if

if row > 1 then
	setnull(ls_null)
	setitem(row - 1,"operatore_logico",ls_null)
end if

if not isnull(getitemstring(row,"apri_par")) then
	il_par_aperte = il_par_aperte - 1
end if

if not isnull(getitemstring(row,"chiudi_par")) then
	il_par_chiuse = il_par_chiuse - 1
end if

deleterow(row)

il_caratteristiche = il_caratteristiche - 1

if row = 1 then
	insertrow(0)
	il_caratteristiche++
end if	

ib_caratteristiche_mod = true

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true

if rowcount() = 0 then
	cb_azzera_caratteristiche.enabled = false
end if
end event

event itemchanged;choose case dwo.name
		
	case "variante"
		
		if isnull(data) then
			if row = 1 then
				cb_azzera_caratteristiche.enabled = false
			end if			
			if row <> rowcount() then
				g_mb.messagebox("APICE","Impossibile annullare una riga se non è l'ultima")
				return 1
			else							
				setitem(row,"tipo_variante","")
			end if
		elseif data = "Dimensioni X" or data = "Dimensioni Y" then
			setitem(row,"tipo_variante","N")			
		else
			setitem(row,"tipo_variante","S")
		end if
		
		if not isnull(data) and row = 1 then
			cb_azzera_caratteristiche.enabled = true
		end if	
		
	case "operatore_confronto"
		
		if data = "null" or data = "!null" then
			setitem(row,"tipo_variante","I")
		elseif data = "like" then
			setitem(row,"tipo_variante","S")
		else			
			
			choose case getitemstring(row,"variante")
					
				case "Dimensioni X","Dimensioni Y"
					setitem(row,"tipo_variante","N")				
				case else
					if isnull(getitemstring(row,"variante")) then
						setitem(row,"tipo_variante","")
					else
						setitem(row,"tipo_variante","S")
					end if	
					
			end choose		
			
		end if			
		
	case "apri_par"
		
		if isnull(data) then
			il_par_aperte = il_par_aperte - 1
		else
			il_par_aperte++
		end if
		
	case "chiudi_par"
		
		if isnull(data) then
			il_par_chiuse = il_par_chiuse - 1
		else
			il_par_chiuse++
		end if
		
	case "valore_stringa"
		
		if pos(data,"'",1) <> 0 or pos(data,"~"",1) <> 0 then
			g_mb.messagebox("APICE","Il campo valore non può contenere i caratteri ' e ~"")
			return 1
		end if	
		
	case "operatore_logico"
		
		if isnull(getitemstring(row,"variante")) then
			g_mb.messagebox("APICE","Il campo variante non può essere vuoto")
			return 2
		end if
		
		if isnull(getitemstring(row,"operatore_confronto")) then
			g_mb.messagebox("APICE","L'operatore di confronto non può essere vuoto")
			return 2
		end if
		
		choose case getitemstring(row,"tipo_variante")
				
			case "S"
				
				if isnull(getitemstring(row,"valore_stringa")) then
					g_mb.messagebox("APICE","Il campo valore non può essere vuoto")
					return 2
				end if
				
			case "N"
				
				if isnull(getitemnumber(row,"valore_numerico")) then
					g_mb.messagebox("APICE","Il campo valore non può essere vuoto")
					return 2
				end if
				
		end choose
		
		if isnull(data) and row <> rowcount() then
			g_mb.messagebox("APICE","L'operatore logico non può essere vuoto se è seguito da altre righe")
			return 2
		end if
		
		if row = rowcount() then
			insertrow(0)		
			scrolltorow(rowcount())
			il_caratteristiche++
			cb_azzera_caratteristiche.enabled = true
		end if
		
end choose

ib_caratteristiche_mod = true

cb_salva_filtri.enabled = true

cb_annulla_modifiche.enabled = true
end event

type cbx_esplosione_carat from checkbox within tabpage_5
integer x = 50
integer y = 2008
integer width = 1362
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Esplodi le caratteristiche del prodotto selezionato"
end type

event clicked;if checked then
	cb_esegui.show()
	st_a_data.show()
	em_a_data.show()
	st_da_data.show()
	em_da_data.show()
	
else
	cb_esegui.hide()
	st_a_data.hide()
	em_a_data.hide()
	st_da_data.hide()
	em_da_data.hide()
end if	
end event

type st_da_data from statictext within tabpage_5
boolean visible = false
integer x = 1947
integer y = 2016
integer width = 229
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Da data:"
boolean focusrectangle = false
end type

type em_da_data from editmask within tabpage_5
boolean visible = false
integer x = 2199
integer y = 2004
integer width = 411
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datetimemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type st_a_data from statictext within tabpage_5
boolean visible = false
integer x = 3063
integer y = 2016
integer width = 183
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "A data:"
boolean focusrectangle = false
end type

type em_a_data from editmask within tabpage_5
boolean visible = false
integer x = 3269
integer y = 2004
integer width = 411
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datetimemask!
string mask = "dd/mm/yyyy"
boolean spin = true
end type

type cb_esegui from commandbutton within tabpage_5
boolean visible = false
integer x = 4160
integer y = 2004
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Esegui"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = tab_1.tabpage_4.dw_prod_statistiche_selez.getitemstring(tab_1.tabpage_4.dw_prod_statistiche_selez.getrow(),"cod_prodotto")

s_cs_xx.parametri.parametro_data_1 = datetime(date(em_da_data.text))

s_cs_xx.parametri.parametro_data_2 = datetime(date(em_a_data.text))

open(w_stat_esplosione_carat)
end event

type tabpage_6 from userobject within tab_1
integer x = 18
integer y = 108
integer width = 4558
integer height = 2116
long backcolor = 12632256
string text = "Report"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
st_17 st_17
dw_note_prec dw_note_prec
dw_note dw_note
dw_esporta_report dw_esporta_report
dw_retrieve_prec dw_retrieve_prec
dw_retrieve dw_retrieve
st_perc st_perc
st_elaborazione st_elaborazione
hpb_1 hpb_1
dw_report_statistiche_selez dw_report_statistiche_selez
dw_report_statistiche dw_report_statistiche
end type

on tabpage_6.create
this.st_17=create st_17
this.dw_note_prec=create dw_note_prec
this.dw_note=create dw_note
this.dw_esporta_report=create dw_esporta_report
this.dw_retrieve_prec=create dw_retrieve_prec
this.dw_retrieve=create dw_retrieve
this.st_perc=create st_perc
this.st_elaborazione=create st_elaborazione
this.hpb_1=create hpb_1
this.dw_report_statistiche_selez=create dw_report_statistiche_selez
this.dw_report_statistiche=create dw_report_statistiche
this.Control[]={this.st_17,&
this.dw_note_prec,&
this.dw_note,&
this.dw_esporta_report,&
this.dw_retrieve_prec,&
this.dw_retrieve,&
this.st_perc,&
this.st_elaborazione,&
this.hpb_1,&
this.dw_report_statistiche_selez,&
this.dw_report_statistiche}
end on

on tabpage_6.destroy
destroy(this.st_17)
destroy(this.dw_note_prec)
destroy(this.dw_note)
destroy(this.dw_esporta_report)
destroy(this.dw_retrieve_prec)
destroy(this.dw_retrieve)
destroy(this.st_perc)
destroy(this.st_elaborazione)
destroy(this.hpb_1)
destroy(this.dw_report_statistiche_selez)
destroy(this.dw_report_statistiche)
end on

type st_17 from statictext within tabpage_6
integer x = 5
integer y = 12
integer width = 4544
integer height = 80
integer textsize = -11
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Report statistiche di vendita"
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type dw_note_prec from datawindow within tabpage_6
boolean visible = false
integer x = 27
integer y = 428
integer width = 114
integer height = 92
integer taborder = 40
borderstyle borderstyle = stylelowered!
end type

event retrievestart;return il_retrievestart
end event

type dw_note from datawindow within tabpage_6
boolean visible = false
integer x = 27
integer y = 328
integer width = 114
integer height = 92
integer taborder = 40
borderstyle borderstyle = stylelowered!
end type

event retrievestart;return il_retrievestart
end event

type dw_esporta_report from datawindow within tabpage_6
boolean visible = false
integer x = 27
integer y = 528
integer width = 114
integer height = 92
integer taborder = 40
string dataobject = "d_ds_report_export"
borderstyle borderstyle = stylelowered!
end type

type dw_retrieve_prec from datawindow within tabpage_6
boolean visible = false
integer x = 27
integer y = 228
integer width = 114
integer height = 92
integer taborder = 30
borderstyle borderstyle = stylelowered!
end type

event retrievestart;return il_retrievestart
end event

type dw_retrieve from datawindow within tabpage_6
boolean visible = false
integer x = 27
integer y = 128
integer width = 114
integer height = 92
integer taborder = 20
borderstyle borderstyle = stylelowered!
end type

event retrievestart;return il_retrievestart
end event

type st_perc from statictext within tabpage_6
integer x = 3045
integer y = 1412
integer width = 178
integer height = 72
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "0%"
alignment alignment = center!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_elaborazione from statictext within tabpage_6
integer x = 1399
integer y = 1352
integer width = 1819
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type hpb_1 from hprogressbar within tabpage_6
integer x = 1399
integer y = 1412
integer width = 1641
integer height = 72
unsignedinteger maxposition = 1000
integer setstep = 1
boolean smoothscroll = true
end type

type dw_report_statistiche_selez from datawindow within tabpage_6
integer x = 5
integer y = 108
integer width = 4549
integer height = 2004
integer taborder = 20
string dataobject = "d_report_statistiche_selez"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event itemchanged;long ll_i

if dwo.name = "cod_statistica" then
	
	for ll_i = 1 to tab_1.tabpage_1.dw_tes_statistiche_lista.rowcount()
		
		if tab_1.tabpage_1.dw_tes_statistiche_lista.getitemstring(ll_i,"cod_statistica") = data then
			tab_1.tabpage_1.dw_tes_statistiche_lista.setrow(ll_i)
			return 0
		end if
	
	next
	
end if
end event

type dw_report_statistiche from datawindow within tabpage_6
boolean visible = false
integer x = 5
integer y = 12
integer width = 4549
integer height = 2100
integer taborder = 30
string dataobject = "d_report_statistiche"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

type r_1 from rectangle within w_statistiche_vendita
integer linethickness = 1
long fillcolor = 12632256
integer x = 27
integer y = 2368
integer width = 2382
integer height = 124
end type

type cb_modifica from commandbutton within w_statistiche_vendita
integer x = 4210
integer y = 2412
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Modifica"
end type

event clicked;if ib_testata_mod then
	
	if ib_livelli_mod then
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata, livelli, prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata, livelli e prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
			
		else			
		 
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata, livelli e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata e livelli modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
		
		end if
	
	else
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata, prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata e prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if			
		
		else
		
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
		
		end if
		
	end if	
	
else
	
	if ib_livelli_mod then
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di livelli, prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di livelli e prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
			
		else	
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di livelli e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di livelli modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
		
		end if
		
	else
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
		
		else
	
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			end if

		end if
	
	end if
	
end if

s_cs_xx.parametri.parametro_s_1 = "modifica"

s_cs_xx.parametri.parametro_s_2 = is_codice_corrente

openwithparm(w_statistiche_vendita_det,0)

tab_1.postevent("ue_retrieve")
end event

type cb_salva_filtri from commandbutton within w_statistiche_vendita
boolean visible = false
integer x = 4210
integer y = 2412
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Salva filtri"
end type

event clicked;setpointer(hourglass!)

if ib_testata_mod then
	if wf_salva_testata() = -1 then
		setpointer(arrow!)
		return -1
	end if
end if

if ib_livelli_mod then
	if wf_salva_livelli() = -1 then
		setpointer(arrow!)
		return -1
	end if
	setnull(is_codice_prod)
end if

if ib_prodotti_mod then
	if wf_salva_prodotti() = -1 then
		setpointer(arrow!)
		return -1
	end if
	setnull(is_codice_carat)
end if

if ib_caratteristiche_mod then
	if il_prodotti <> 1 then
		ib_background = true
		cb_azzera_caratteristiche.triggerevent("clicked")
		ib_background = false
		ib_caratteristiche_mod = false
	else
		if wf_salva_caratteristiche() = -1 then
			setpointer(arrow!)
			return -1
		end if
	end if	
end if

choose case il_count
		
	case 1
		tab_1.tabpage_2.dw_1.object.dati.protect = true
		tab_1.tabpage_2.dw_1.object.t_annulla.visible = false
	case 2
		tab_1.tabpage_2.dw_2.object.dati.protect = true
		tab_1.tabpage_2.dw_2.object.t_annulla.visible = false
	case 3
		tab_1.tabpage_2.dw_3.object.dati.protect = true
		tab_1.tabpage_2.dw_3.object.t_annulla.visible = false
	case 4
		tab_1.tabpage_2.dw_4.object.dati.protect = true
		tab_1.tabpage_2.dw_4.object.t_annulla.visible = false
	case 5
		tab_1.tabpage_2.dw_5.object.dati.protect = true
		tab_1.tabpage_2.dw_5.object.t_annulla.visible = false
	case 6
		tab_1.tabpage_2.dw_6.object.dati.protect = true
		tab_1.tabpage_2.dw_6.object.t_annulla.visible = false
	case 7
		tab_1.tabpage_2.dw_7.object.dati.protect = true	
		tab_1.tabpage_2.dw_7.object.t_annulla.visible = false
	case 8
		tab_1.tabpage_2.dw_8.object.dati.protect = true	
		tab_1.tabpage_2.dw_8.object.t_annulla.visible = false	
		
end choose

wf_blocca_caratteristiche()

if il_caratteristiche = 1 then
	if isnull(tab_1.tabpage_5.dw_prod_statistiche_carat.getitemstring(1,"variante")) then
		tab_1.tabpage_5.dw_prod_statistiche_carat.reset()
		il_caratteristiche = 0
	end if	
end if

enabled = false

cb_annulla_modifiche.enabled = false

cb_azzera_testata.enabled = false

cb_azzera_livelli.enabled = false

tab_1.tabpage_3.cb_superiore.enabled = false

tab_1.tabpage_3.dw_prod_statistiche_livelli.enabled = false

tab_1.tabpage_4.dw_prod_statistiche_elenco.enabled = false

tab_1.tabpage_4.dw_prod_statistiche_selez.enabled = false

cb_azzera_prodotti.enabled = false

tab_1.tabpage_4.cbx_senza_livello.enabled = false

cb_azzera_caratteristiche.enabled = false

tab_1.tabpage_4.pb_aggiungi_tutti.enabled = false

tab_1.tabpage_4.pb_aggiungi_corrente.enabled = false

tab_1.tabpage_4.pb_togli_corrente.enabled = false

tab_1.tabpage_4.pb_togli_tutti.enabled = false

tab_1.tabpage_6.enabled = true

cb_modifica_filtri.enabled = true

ib_consenti_modifica = false

ib_background = true
if ib_report then
	cb_selezione.triggerevent("clicked")
end if	
ib_background = false

setpointer(arrow!)
end event

type cb_cancella from commandbutton within w_statistiche_vendita
integer x = 3822
integer y = 2412
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Cancella"
end type

event clicked;if ib_testata_mod then
	
	if ib_livelli_mod then
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata, livelli, prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata, livelli e prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
			
		else			
		 
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata, livelli e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata e livelli modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
		
		end if
	
	else
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata, prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata e prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if			
		
		else
		
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
		
		end if
		
	end if	
	
else
	
	if ib_livelli_mod then
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di livelli, prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di livelli e prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
			
		else	
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di livelli e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di livelli modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
		
		end if
		
	else
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
		
		else
	
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Cancellare il record selezionato?",question!,yesno!,2) = 2 then
					return
				end if
		
			end if

		end if
	
	end if
	
end if
	
delete
from  det_stat_filtro_varianti
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_statistica = :is_codice_corrente;
	
if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella delete di det_stat_filtro_varianti: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if	
	
delete
from  det_statistiche_filtro_prod
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_statistica = :is_codice_corrente;
	
if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","Errore nella delete di det_statistiche_filtro_prod: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

delete
from  det_statistiche_filtro_tes
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_statistica = :is_codice_corrente;
		
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella delete di det_statistiche_filtro_tes: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

delete
from  tes_statistiche
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_statistica = :is_codice_corrente;
		
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella delete di tes_statistiche: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

commit;
	
tab_1.postevent("ue_retrieve")
end event

type cb_annulla_modifiche from commandbutton within w_statistiche_vendita
boolean visible = false
integer x = 3822
integer y = 2412
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "A&nnulla"
end type

event clicked;if ib_testata_mod then
	
	if ib_livelli_mod then
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata, livelli, prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata, livelli e prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
			
		else			
		 
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata, livelli e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata e livelli modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
		
		end if
	
	else
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata, prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata e prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if			
		
		else
		
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
		
		end if
		
	end if	
	
else
	
	if ib_livelli_mod then
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di livelli, prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di livelli e prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
			
		else	
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di livelli e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di livelli modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if			
				
			end if
		
		end if
		
	else
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
		
		else
	
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			else
				
				choose case il_count
		
					case 1
						tab_1.tabpage_2.dw_1.object.dati.protect = true
						tab_1.tabpage_2.dw_1.object.t_annulla.visible = false
					case 2
						tab_1.tabpage_2.dw_2.object.dati.protect = true
						tab_1.tabpage_2.dw_2.object.t_annulla.visible = false
					case 3
						tab_1.tabpage_2.dw_3.object.dati.protect = true
						tab_1.tabpage_2.dw_3.object.t_annulla.visible = false
					case 4
						tab_1.tabpage_2.dw_4.object.dati.protect = true
						tab_1.tabpage_2.dw_4.object.t_annulla.visible = false
					case 5
						tab_1.tabpage_2.dw_5.object.dati.protect = true
						tab_1.tabpage_2.dw_5.object.t_annulla.visible = false
					case 6
						tab_1.tabpage_2.dw_6.object.dati.protect = true
						tab_1.tabpage_2.dw_6.object.t_annulla.visible = false
					case 7
						tab_1.tabpage_2.dw_7.object.dati.protect = true	
						tab_1.tabpage_2.dw_7.object.t_annulla.visible = false
					case 8
						tab_1.tabpage_2.dw_8.object.dati.protect = true	
						tab_1.tabpage_2.dw_8.object.t_annulla.visible = false	
		
				end choose

				wf_blocca_caratteristiche()

				cb_salva_filtri.enabled = false

				enabled = false

				cb_azzera_testata.enabled = false

				cb_azzera_livelli.enabled = false				

				tab_1.tabpage_3.cb_superiore.enabled = false

				cb_azzera_prodotti.enabled = false
				
				tab_1.tabpage_4.cbx_senza_livello.enabled = false

				cb_azzera_caratteristiche.enabled = false
				
				tab_1.tabpage_3.dw_prod_statistiche_livelli.enabled = false

				tab_1.tabpage_4.dw_prod_statistiche_elenco.enabled = false

				tab_1.tabpage_4.dw_prod_statistiche_selez.enabled = false

				tab_1.tabpage_4.pb_aggiungi_tutti.enabled = false

				tab_1.tabpage_4.pb_aggiungi_corrente.enabled = false
				
				tab_1.tabpage_4.pb_togli_corrente.enabled = false

				tab_1.tabpage_4.pb_togli_tutti.enabled = false

				tab_1.tabpage_6.enabled = true

				cb_modifica_filtri.enabled = true

				ib_consenti_modifica = false
				
				if il_caratteristiche = 1 then
					if isnull(tab_1.tabpage_5.dw_prod_statistiche_carat.getitemstring(1,"variante")) then
						tab_1.tabpage_5.dw_prod_statistiche_carat.reset()
						il_caratteristiche = 0
					end if	
				end if
				
				return 0
			
			end if

		end if
	
	end if
	
end if

wf_blocca_caratteristiche()

cb_salva_filtri.enabled = false

enabled = false

cb_azzera_testata.enabled = false

cb_azzera_livelli.enabled = false				

tab_1.tabpage_3.cb_superiore.enabled = false

cb_azzera_prodotti.enabled = false
				
tab_1.tabpage_4.cbx_senza_livello.enabled = false

cb_azzera_caratteristiche.enabled = false

tab_1.tabpage_3.dw_prod_statistiche_livelli.enabled = false

tab_1.tabpage_4.dw_prod_statistiche_elenco.enabled = false

tab_1.tabpage_4.dw_prod_statistiche_selez.enabled = false

tab_1.tabpage_4.pb_aggiungi_tutti.enabled = false

tab_1.tabpage_4.pb_aggiungi_corrente.enabled = false

tab_1.tabpage_4.pb_togli_corrente.enabled = false

tab_1.tabpage_4.pb_togli_tutti.enabled = false

tab_1.tabpage_6.enabled = true

cb_modifica_filtri.enabled = true

ib_consenti_modifica = false

if ib_testata_mod then
	ib_testata_mod = false
	setnull(is_codice_tes)
end if	

if ib_livelli_mod then
	ib_livelli_mod = false
	setnull(is_codice_liv)
end if	

if ib_prodotti_mod then
	ib_prodotti_mod = false
	setnull(is_codice_prod)
end if

if ib_caratteristiche_mod then
	ib_caratteristiche_mod = false
	setnull(is_codice_carat)
end if

if il_caratteristiche = 1 then
	if isnull(tab_1.tabpage_5.dw_prod_statistiche_carat.getitemstring(1,"variante")) then
		tab_1.tabpage_5.dw_prod_statistiche_carat.reset()
		il_caratteristiche = 0
	end if	
end if

tab_1.selecttab(il_tab)
end event

type cb_stampa from commandbutton within w_statistiche_vendita
integer x = 3822
integer y = 2412
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Stampa"
end type

event clicked;if g_mb.messagebox("Stampa report","Stampare il report visualizzato?",question!,yesno!,2) = 1 then
	tab_1.tabpage_6.dw_report_statistiche.print()
end if	
end event

type cb_esporta from commandbutton within w_statistiche_vendita
integer x = 4210
integer y = 2412
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Esporta"
end type

event clicked;long   ll_i, ll_riga
string ls_tipo_riga, ls_string
dec{4} ld_number


setpointer(hourglass!)

for ll_i = 1 to tab_1.tabpage_6.dw_report_statistiche.rowcount()
	
	ls_tipo_riga = tab_1.tabpage_6.dw_report_statistiche.getitemstring(ll_i,"tipo_riga")
	
	choose case ls_tipo_riga
		case "E"
			ll_riga = tab_1.tabpage_6.dw_esporta_report.insertrow(0)
			ls_string = tab_1.tabpage_6.dw_report_statistiche.getitemstring(ll_i,"etichetta_1")
			tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_1",ls_string)
			ls_string = tab_1.tabpage_6.dw_report_statistiche.getitemstring(ll_i,"etichetta_2")
			tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_2",ls_string)
			ls_string = tab_1.tabpage_6.dw_report_statistiche.getitemstring(ll_i,"etichetta_3")
			tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_3",ls_string)
			ls_string = tab_1.tabpage_6.dw_report_statistiche.getitemstring(ll_i,"etichetta_4")
			tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_4",ls_string)
			ls_string = tab_1.tabpage_6.dw_report_statistiche.getitemstring(ll_i,"etichetta_5")
			tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_5",ls_string)
			ls_string = tab_1.tabpage_6.dw_report_statistiche.getitemstring(ll_i,"etichetta_6")
			tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_6",ls_string)
			ls_string = tab_1.tabpage_6.dw_report_statistiche.getitemstring(ll_i,"etichetta_7")
			tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_7",ls_string)
			ls_string = tab_1.tabpage_6.dw_report_statistiche.getitemstring(ll_i,"etichetta_8")
			tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_8",ls_string)
			ls_string = tab_1.tabpage_6.dw_report_statistiche.getitemstring(ll_i,"etichetta_9")
			tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_9",ls_string)
			continue
		case "I"
			if ll_i > 1 then
				ll_riga = tab_1.tabpage_6.dw_esporta_report.insertrow(0)
			end if	
			ll_riga = tab_1.tabpage_6.dw_esporta_report.insertrow(0)
			ls_string = tab_1.tabpage_6.dw_report_statistiche.getitemstring(ll_i,"periodo")
			tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_1",ls_string)
			continue
		case "D"
			ll_riga = tab_1.tabpage_6.dw_esporta_report.insertrow(0)
			ls_string = tab_1.tabpage_6.dw_report_statistiche.getitemstring(ll_i,"contenuto_periodo")
			tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_1",ls_string)
		case "P"
			ll_riga = tab_1.tabpage_6.dw_esporta_report.insertrow(0)
			ls_string = tab_1.tabpage_6.dw_report_statistiche.getitemstring(ll_i,"cod_prodotto") + ",   " + &
						   tab_1.tabpage_6.dw_report_statistiche.getitemstring(ll_i,"des_prodotto")
			tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_1",ls_string)				
	end choose
	
	ld_number = tab_1.tabpage_6.dw_report_statistiche.getitemnumber(ll_i,"quan_analisi")
	tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_2",string(ld_number))
	
	ld_number = tab_1.tabpage_6.dw_report_statistiche.getitemnumber(ll_i,"quan_prec")
	tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_3",string(ld_number))
	
	ld_number = tab_1.tabpage_6.dw_report_statistiche.getitemnumber(ll_i,"diff_quan_num")
	tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_4",string(ld_number))
	
	ld_number = tab_1.tabpage_6.dw_report_statistiche.getitemnumber(ll_i,"diff_quan_perc")
	tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_5",string(ld_number))
	
	ld_number = tab_1.tabpage_6.dw_report_statistiche.getitemnumber(ll_i,"fatt_analisi")
	tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_6",string(ld_number))
	
	ld_number = tab_1.tabpage_6.dw_report_statistiche.getitemnumber(ll_i,"fatt_prec")
	tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_7",string(ld_number))
	
	ld_number = tab_1.tabpage_6.dw_report_statistiche.getitemnumber(ll_i,"diff_fatt_num")
	tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_8",string(ld_number))
	
	ld_number = tab_1.tabpage_6.dw_report_statistiche.getitemnumber(ll_i,"diff_fatt_perc")
	tab_1.tabpage_6.dw_esporta_report.setitem(ll_riga,"etichetta_9",string(ld_number))

next

setpointer(arrow!)

tab_1.tabpage_6.dw_esporta_report.saveas("", excel!, FALSE)
end event

type cb_interrompi from commandbutton within w_statistiche_vendita
integer x = 3433
integer y = 2412
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "In&terrompi"
end type

event clicked;ib_interrompi = true

enabled = false
end event

type cb_nuovo from commandbutton within w_statistiche_vendita
integer x = 3433
integer y = 2412
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Nuovo"
end type

event clicked;if ib_testata_mod then
	
	if ib_livelli_mod then
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata, livelli, prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata, livelli e prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
			
		else			
		 
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata, livelli e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata e livelli modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
		
		end if
	
	else
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata, prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata e prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if			
		
		else
		
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di testata e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di testata modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
		
		end if
		
	end if	
	
else
	
	if ib_livelli_mod then
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di livelli, prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di livelli e prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
			
		else	
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di livelli e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di livelli modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
		
		end if
		
	else
		
		if ib_prodotti_mod then
			
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di prodotti e caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			else
				
				if g_mb.messagebox("APICE","Filtri di prodotti modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
				
			end if
		
		else
	
			if ib_caratteristiche_mod then
				
				if g_mb.messagebox("APICE","Filtri di caratteristiche modificati.~nIgnorare le modifiche?",question!,yesno!,2) = 2 then
					return
				end if
			
			end if

		end if
	
	end if
	
end if

s_cs_xx.parametri.parametro_s_1 = "nuovo"

s_cs_xx.parametri.parametro_s_2 = is_codice_corrente

openwithparm(w_statistiche_vendita_det,0)

tab_1.postevent("ue_retrieve")
end event

type cb_modifica_filtri from commandbutton within w_statistiche_vendita
boolean visible = false
integer x = 3433
integer y = 2412
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Mod. &filtri"
end type

event clicked;ib_consenti_modifica = true

if il_count > 0 then
	cb_azzera_testata.enabled = true
end if

choose case il_count
		
	case 1
		tab_1.tabpage_2.dw_1.object.dati.protect = false
		if not isnull(tab_1.tabpage_2.dw_1.getitemstring(1,"dati")) then
			tab_1.tabpage_2.dw_1.object.t_annulla.visible = true
		end if	
	case 2
		tab_1.tabpage_2.dw_2.object.dati.protect = false
		if not isnull(tab_1.tabpage_2.dw_2.getitemstring(1,"dati")) then
			tab_1.tabpage_2.dw_2.object.t_annulla.visible = true
		end if	
	case 3
		tab_1.tabpage_2.dw_3.object.dati.protect = false
		if not isnull(tab_1.tabpage_2.dw_3.getitemstring(1,"dati")) then
			tab_1.tabpage_2.dw_3.object.t_annulla.visible = true
		end if	
	case 4
		tab_1.tabpage_2.dw_4.object.dati.protect = false
		if not isnull(tab_1.tabpage_2.dw_4.getitemstring(1,"dati")) then
			tab_1.tabpage_2.dw_4.object.t_annulla.visible = true
		end if	
	case 5
		tab_1.tabpage_2.dw_5.object.dati.protect = false
		if not isnull(tab_1.tabpage_2.dw_5.getitemstring(1,"dati")) then
			tab_1.tabpage_2.dw_5.object.t_annulla.visible = true
		end if	
	case 6
		tab_1.tabpage_2.dw_6.object.dati.protect = false
		if not isnull(tab_1.tabpage_2.dw_6.getitemstring(1,"dati")) then
			tab_1.tabpage_2.dw_6.object.t_annulla.visible = true
		end if	
	case 7
		tab_1.tabpage_2.dw_7.object.dati.protect = false
		if not isnull(tab_1.tabpage_2.dw_7.getitemstring(1,"dati")) then
			tab_1.tabpage_2.dw_7.object.t_annulla.visible = true
		end if
	case 8
		tab_1.tabpage_2.dw_8.object.dati.protect = false
		if not isnull(tab_1.tabpage_2.dw_8.getitemstring(1,"dati")) then
			tab_1.tabpage_2.dw_8.object.t_annulla.visible = true
		end if	
		
end choose		

tab_1.tabpage_3.dw_prod_statistiche_livelli.enabled = true

if il_livello = 0 then	
	tab_1.tabpage_3.cb_superiore.enabled = false
	cb_azzera_livelli.enabled = false
else
	tab_1.tabpage_4.cbx_senza_livello.enabled = true
	tab_1.tabpage_3.cb_superiore.enabled = true
	cb_azzera_livelli.enabled = true
end if

tab_1.tabpage_4.dw_prod_statistiche_elenco.enabled = true
tab_1.tabpage_4.dw_prod_statistiche_selez.enabled = true

if il_prodotti > 0 then	
	cb_azzera_prodotti.enabled = true
end if

tab_1.tabpage_4.pb_aggiungi_tutti.enabled = true
tab_1.tabpage_4.pb_aggiungi_corrente.enabled = true
tab_1.tabpage_4.pb_togli_corrente.enabled = true
tab_1.tabpage_4.pb_togli_tutti.enabled = true

if il_caratteristiche > 0 then
	cb_azzera_caratteristiche.enabled = true
elseif il_prodotti = 1 then		
	tab_1.tabpage_5.dw_prod_statistiche_carat.insertrow(0)
	il_caratteristiche++
end if

wf_sblocca_caratteristiche()

tab_1.tabpage_6.enabled = false

enabled = false

cb_annulla_modifiche.enabled = true
end event

type cb_selezione from commandbutton within w_statistiche_vendita
integer x = 3822
integer y = 2320
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Selezione"
end type

event clicked;tab_1.tabpage_6.dw_report_statistiche_selez.show()

tab_1.tabpage_6.st_elaborazione.show()

tab_1.tabpage_6.hpb_1.show()

tab_1.tabpage_6.st_perc.show()

tab_1.tabpage_6.dw_report_statistiche.hide()

ib_report = false

if ib_background = false then
	
	hide()

	cb_report.show()
	
end if	

cb_report.enabled = true

cb_stampa.enabled = false

cb_esporta.enabled = false
end event

type cb_azzera_testata from commandbutton within w_statistiche_vendita
boolean visible = false
integer x = 3822
integer y = 2320
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Azzera"
end type

event clicked;long ll_i


tab_1.tabpage_2.dw_1.reset()
tab_1.tabpage_2.dw_1.object.dati_t.text = ""
tab_1.tabpage_2.dw_1.object.dati.protect = false
tab_1.tabpage_2.dw_2.reset()
tab_1.tabpage_2.dw_2.object.dati_t.text = ""
tab_1.tabpage_2.dw_2.object.dati.protect = false
tab_1.tabpage_2.dw_3.reset()
tab_1.tabpage_2.dw_3.object.dati_t.text = ""
tab_1.tabpage_2.dw_3.object.dati.protect = false
tab_1.tabpage_2.dw_4.reset()
tab_1.tabpage_2.dw_4.object.dati_t.text = ""
tab_1.tabpage_2.dw_4.object.dati.protect = false
tab_1.tabpage_2.dw_5.reset()
tab_1.tabpage_2.dw_5.object.dati_t.text = ""
tab_1.tabpage_2.dw_5.object.dati.protect = false
tab_1.tabpage_2.dw_6.reset()
tab_1.tabpage_2.dw_6.object.dati_t.text = ""
tab_1.tabpage_2.dw_6.object.dati.protect = false
tab_1.tabpage_2.dw_7.reset()
tab_1.tabpage_2.dw_7.object.dati_t.text = ""
tab_1.tabpage_2.dw_7.object.dati.protect = false
tab_1.tabpage_2.dw_8.reset()
tab_1.tabpage_2.dw_8.object.dati_t.text = ""
tab_1.tabpage_2.dw_8.object.dati.protect = false

tab_1.tabpage_2.st_1.italic = false
tab_1.tabpage_2.st_1.textcolor = rgb(0,0,0)
tab_1.tabpage_2.st_2.italic = false
tab_1.tabpage_2.st_2.textcolor = rgb(0,0,0)
tab_1.tabpage_2.st_3.italic = false
tab_1.tabpage_2.st_3.textcolor = rgb(0,0,0)
tab_1.tabpage_2.st_4.italic = false
tab_1.tabpage_2.st_4.textcolor = rgb(0,0,0)
tab_1.tabpage_2.st_5.italic = false
tab_1.tabpage_2.st_5.textcolor = rgb(0,0,0)
tab_1.tabpage_2.st_6.italic = false
tab_1.tabpage_2.st_6.textcolor = rgb(0,0,0)
tab_1.tabpage_2.st_7.italic = false
tab_1.tabpage_2.st_7.textcolor = rgb(0,0,0)
tab_1.tabpage_2.st_8.italic = false
tab_1.tabpage_2.st_8.textcolor = rgb(0,0,0)

for ll_i = 1 to il_count
	setnull(is_items[ll_i])
next

il_count = 0

is_where_1 = " where 1 = 1"
is_where_2 = ""
is_where_3 = ""
is_where_4 = ""
is_where_5 = ""
is_where_6 = ""
is_where_7 = ""
is_where_8 = ""

if ib_background = false then
	
	ib_testata_mod = true

	cb_salva_filtri.enabled = true

	cb_annulla_modifiche.enabled = true
	
end if	

cb_azzera_testata.enabled = false
end event

type cb_azzera_caratteristiche from commandbutton within w_statistiche_vendita
boolean visible = false
integer x = 3822
integer y = 2320
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Azzera"
end type

event clicked;tab_1.tabpage_5.dw_prod_statistiche_carat.reset()

il_par_aperte = 0

il_par_chiuse = 0

il_caratteristiche = 0

enabled = false

if ib_background = false then

	ib_caratteristiche_mod = true

	cb_salva_filtri.enabled = true

	cb_annulla_modifiche.enabled = true
	
	tab_1.tabpage_5.dw_prod_statistiche_carat.insertrow(0)
	
	il_caratteristiche++

end if
end event

type cb_azzera_prodotti from commandbutton within w_statistiche_vendita
boolean visible = false
integer x = 3822
integer y = 2320
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Azzera"
end type

event clicked;il_prodotti = 0

tab_1.tabpage_4.st_prod_selez.text = string(il_prodotti)

tab_1.tabpage_4.dw_prod_statistiche_selez.reset()

enabled = false

if ib_background = false then
	
	ib_prodotti_mod = true

	cb_salva_filtri.enabled = true

	cb_annulla_modifiche.enabled = true
	
end if	
end event

type cb_azzera_livelli from commandbutton within w_statistiche_vendita
boolean visible = false
integer x = 3822
integer y = 2320
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Azzera"
end type

event clicked;if ib_background = false then
	
	ib_livelli_mod = true

	cb_salva_filtri.enabled = true

	cb_annulla_modifiche.enabled = true
	
	tab_1.tabpage_3.cb_superiore.enabled = false

	cb_azzera_livelli.enabled = false
	
end if

setnull(is_livello_1)

tab_1.tabpage_3.st_liv_scelto_1.text = ""

tab_1.tabpage_3.st_livello_1.textcolor = rgb(0,0,0)

tab_1.tabpage_3.st_livello_1.italic = false
				
setnull(is_livello_2)

tab_1.tabpage_3.st_liv_scelto_2.text = ""

tab_1.tabpage_3.st_livello_2.textcolor = rgb(0,0,0)

tab_1.tabpage_3.st_livello_2.italic = false

setnull(is_livello_3)

tab_1.tabpage_3.st_liv_scelto_3.text = ""

tab_1.tabpage_3.st_livello_3.textcolor = rgb(0,0,0)

tab_1.tabpage_3.st_livello_3.italic = false

setnull(is_livello_4)

tab_1.tabpage_3.st_liv_scelto_4.text = ""

tab_1.tabpage_3.st_livello_4.textcolor = rgb(0,0,0)

tab_1.tabpage_3.st_livello_4.italic = false

setnull(is_livello_5)

tab_1.tabpage_3.st_liv_scelto_5.text = ""

tab_1.tabpage_3.st_livello_5.textcolor = rgb(0,0,0)

tab_1.tabpage_3.st_livello_5.italic = false

il_livello = 0

tab_1.tabpage_3.dw_prod_statistiche_livelli.setsqlselect("select distinct livello_1, " + &
						 "des_livello_1 " + &
						 "from view_livelli_prodotti " + &
						 "where livello_1 is not null " + &
						 "order by livello_1")
						 
tab_1.tabpage_3.dw_prod_statistiche_livelli.retrieve()
end event

type cb_report from commandbutton within w_statistiche_vendita
integer x = 3822
integer y = 2320
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;wf_report()
end event

