﻿$PBExportHeader$w_con_off_acq.srw
$PBExportComments$Finestra Gestione Parametri Offerte Vendita
forward
global type w_con_off_acq from w_cs_xx_principale
end type
type dw_con_off_acq from uo_cs_xx_dw within w_con_off_acq
end type
end forward

global type w_con_off_acq from w_cs_xx_principale
int Width=2181
int Height=389
boolean TitleBar=true
string Title="Gestione Parametri Offerte Acquisto"
dw_con_off_acq dw_con_off_acq
end type
global w_con_off_acq w_con_off_acq

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_con_off_acq.set_dw_key("cod_azienda")
dw_con_off_acq.set_dw_options(sqlca, &
                              pcca.null_object, &
                              c_default, &
                              c_default)

end on

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_po_loaddddw_dw(dw_con_off_acq, &
                 "cod_tipo_off_acq", &
                 sqlca, &
                 "tab_tipi_off_acq", &
                 "cod_tipo_off_acq", &
                 "des_tipo_off_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end on

on w_con_off_acq.create
int iCurrent
call w_cs_xx_principale::create
this.dw_con_off_acq=create dw_con_off_acq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_con_off_acq
end on

on w_con_off_acq.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_con_off_acq)
end on

type dw_con_off_acq from uo_cs_xx_dw within w_con_off_acq
int X=23
int Y=21
int Width=2103
int Height=241
string DataObject="d_con_off_acq"
BorderStyle BorderStyle=StyleRaised!
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

