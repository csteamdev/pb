﻿$PBExportHeader$w_det_gen_ord_acq.srw
$PBExportComments$Finestra Dettaglio Generazione Ordini Acquisto
forward
global type w_det_gen_ord_acq from w_cs_xx_risposta
end type
type cb_genera from uo_cb_ok within w_det_gen_ord_acq
end type
type cb_1 from uo_cb_close within w_det_gen_ord_acq
end type
type dw_det_gen_ord_acq from uo_cs_xx_dw within w_det_gen_ord_acq
end type
end forward

global type w_det_gen_ord_acq from w_cs_xx_risposta
integer width = 3305
integer height = 1340
string title = "Dettaglio Generazione Ordini Acquisto"
cb_genera cb_genera
cb_1 cb_1
dw_det_gen_ord_acq dw_det_gen_ord_acq
end type
global w_det_gen_ord_acq w_det_gen_ord_acq

on w_det_gen_ord_acq.create
int iCurrent
call super::create
this.cb_genera=create cb_genera
this.cb_1=create cb_1
this.dw_det_gen_ord_acq=create dw_det_gen_ord_acq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_genera
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_det_gen_ord_acq
end on

on w_det_gen_ord_acq.destroy
call super::destroy
destroy(this.cb_genera)
destroy(this.cb_1)
destroy(this.dw_det_gen_ord_acq)
end on

event pc_setwindow;call super::pc_setwindow;dw_det_gen_ord_acq.set_dw_options(sqlca, &
                                  pcca.null_object, &
											 c_modifyonopen + &
						   			    c_nonew + &
											 c_nodelete + &
											 c_disablecc + &
											 c_disableccinsert, &
                                  c_default)
save_on_close(c_socnosave)

end event

type cb_genera from uo_cb_ok within w_det_gen_ord_acq
event clicked pbm_bnclicked
integer x = 2491
integer y = 1140
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera"
end type

event clicked;call super::clicked;boolean lb_ok
long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_anno_offerta, ll_num_offerta, ll_prog_riga_off_acq, ll_anno_commessa, &
     ll_num_commessa, ll_i1, ll_i2, ll_flag, ll_prog_riga_ord_acq, ll_anno_reg_rda, ll_num_reg_rda, ll_prog_riga_rda
string ls_cod_tipo_ord_acq, ls_cod_tipo_off_acq, ls_nota_testata, ls_cod_fornitore, &
		 ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, &
		 ls_frazione, ls_cap, ls_provincia, ls_cod_fil_fornitore, ls_cod_des_fornitore, &
		 ls_cod_deposito, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
		 ls_cod_banca_clien_for, ls_cod_imballo, ls_cod_vettore, &
		 ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_nota_piede, ls_cod_banca, &
		 ls_cod_tipo_det_acq, ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, &
		 ls_cod_prod_fornitore, ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_acq, &
		 ls_sql_stringa, ls_lire, ls_flag, ls_flag_sola_iva, ls_tabella_orig, ls_tabella_des, &
		 ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ls_cod_tipo_analisi_off, ls_cod_prodotto_raggruppato,&
		 ls_cod_tipo_analisi_ord, ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det
datetime ldt_data_registrazione, ldt_data_consegna, ldt_data_consegna_det
dec{4} ld_cambio_acq, ld_sconto, ld_tot_val_offerta, ld_quan_ordinata, ld_prezzo_acquisto, &
		 ld_fat_conversione, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_val_riga, ld_sconto_4, &
		 ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		 ld_sconto_testata, ld_sconto_pagamento, ld_val_sconto_1, ld_quan_richiesta, &
		 ld_val_riga_sconto_1, ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, &
		 ld_val_riga_sconto_3, ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, &
		 ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, &
		 ld_val_riga_sconto_7, ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, &
		 ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10, &
		 ld_val_sconto_testata, ld_val_riga_sconto_testata, ld_val_sconto_pagamento, &
		 ld_val_riga_netto, ld_tot_val_documento, ld_quan_residua, ld_quan_raggruppo

setpointer(hourglass!)

lb_ok = false
for ll_i2 = 1 to dw_det_gen_ord_acq.rowcount()
		dw_det_gen_ord_acq.setrow(ll_i2)
		dw_det_gen_ord_acq.setcolumn(7)
	if double(dw_det_gen_ord_acq.gettext()) > 0 then
		lb_ok = true
		exit
	end if
next


if lb_ok = true then
	select parametri_azienda.numero
	into   :ll_anno_registrazione
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'N' and &
			 parametri_azienda.cod_parametro = 'ESC';
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Errore durante la lettura del parametro ESC.", Exclamation!, ok!)
		return
	end if
	
	select parametri_azienda.stringa
	into   :ls_lire
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LIR';
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Configurare il codice valuta per le Lire Italiane.", exclamation!, ok!)
		return
	end if
	
	select con_ord_acq.num_registrazione
	into   :ll_num_registrazione
	from   con_ord_acq
	where  con_ord_acq.cod_azienda = :s_cs_xx.cod_azienda;
	
	ll_num_registrazione ++
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Errore durante la lettura della tabella Controllo Ordini Acquisto.~r~n" + sqlca.sqlerrtext, Exclamation!, ok!)
		return
	end if
	
	update con_ord_acq
	set    con_ord_acq.num_registrazione = :ll_num_registrazione
	where  con_ord_acq.cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Errore durante la scrittura sulla tabella Controllo Ordini Acquisto.~r~n" + sqlca.sqlerrtext, Exclamation!, ok!)
		rollback;
		return
	end if
	
	ls_cod_tipo_off_acq = s_cs_xx.parametri.parametro_s_1
	
	select tab_tipi_off_acq.cod_tipo_ord_acq,
			 tab_tipi_off_acq.cod_tipo_analisi
	into   :ls_cod_tipo_ord_acq,
			 :ls_cod_tipo_analisi_off
	from   tab_tipi_off_acq  
	where  tab_tipi_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_off_acq.cod_tipo_off_acq = :ls_cod_tipo_off_acq;
	
	if isnull(ls_cod_tipo_ord_acq) then
		g_mb.messagebox("Attenzione", "Per questo tipo offerta non è specificato il relativo tipo ordine.", Exclamation!, ok!)
		rollback;
		return
	end if
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Errore durante la lettura della tabella Tipi Offerte Acquisto.", Exclamation!, ok!)
		rollback;
		return
	end if
	
	ll_anno_offerta = s_cs_xx.parametri.parametro_d_1
	ll_num_offerta = s_cs_xx.parametri.parametro_d_2
	
	SELECT tes_off_acq.cod_fornitore,   
			 tes_off_acq.cod_operatore,   
			 tes_off_acq.rag_soc_1,   
			 tes_off_acq.rag_soc_2,   
			 tes_off_acq.indirizzo,   
			 tes_off_acq.localita,   
			 tes_off_acq.frazione,   
			 tes_off_acq.cap,   
			 tes_off_acq.provincia,   
			 tes_off_acq.cod_fil_fornitore,   
			 tes_off_acq.cod_des_fornitore,   
			 tes_off_acq.cod_deposito,   
			 tes_off_acq.cod_valuta,   
			 tes_off_acq.cambio_acq,   
			 tes_off_acq.cod_tipo_listino_prodotto,   
			 tes_off_acq.cod_pagamento,   
			 tes_off_acq.sconto,   
			 tes_off_acq.cod_banca_clien_for,   
			 tes_off_acq.cod_imballo,   
			 tes_off_acq.cod_vettore,   
			 tes_off_acq.cod_inoltro,   
			 tes_off_acq.cod_mezzo,   
			 tes_off_acq.cod_porto,   
			 tes_off_acq.cod_resa,   
			 tes_off_acq.nota_testata,   
			 tes_off_acq.nota_piede,   
			 tes_off_acq.data_consegna,   
			 tes_off_acq.cod_banca,
			 tes_off_acq.flag_doc_suc_tes, 
			 tes_off_acq.flag_doc_suc_pie			 			 
	 INTO  :ls_cod_fornitore,   
			 :ls_cod_operatore,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_fil_fornitore,   
			 :ls_cod_des_fornitore,   
			 :ls_cod_deposito,   
			 :ls_cod_valuta,   
			 :ld_cambio_acq,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_banca_clien_for,   
			 :ls_cod_imballo,   
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_cod_porto,   
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ldt_data_consegna,   
			 :ls_cod_banca,
			 :ls_flag_doc_suc_tes, 
			 :ls_flag_doc_suc_pie			 			 
	 FROM  tes_off_acq  
	WHERE  tes_off_acq.cod_azienda = :s_cs_xx.cod_azienda AND  
			 tes_off_acq.anno_registrazione = :ll_anno_offerta AND  
			 tes_off_acq.num_registrazione = :ll_num_offerta;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Errore durante la lettura Testata Offerte Acquisto.", Exclamation!, ok!)
		rollback;
		return
	end if
	
	ldt_data_registrazione = datetime(today())
	
	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if
	
//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_off_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_off_acq = :ls_cod_tipo_off_acq;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_off_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_off_acq = :ls_cod_tipo_off_acq;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica -------------------------------------------------------	
	ls_nota_testata = "Ordine relativo alla richiesta di offerta N. " + string(ll_anno_offerta) + "/" + &
							string(ll_num_offerta) + "~r~n" + ls_nota_testata
	
	INSERT INTO tes_ord_acq  
					(cod_azienda,   
					 anno_registrazione,   
					 num_registrazione,   
					 cod_tipo_ord_acq,   
					 data_registrazione,   
					 cod_operatore,   
					 cod_fornitore,   
					 rag_soc_1,   
					 rag_soc_2,   
					 indirizzo,   
					 localita,   
					 frazione,   
					 cap,   
					 provincia,   
					 cod_fil_fornitore,   
					 cod_des_fornitore,   
					 cod_deposito,   
					 cod_valuta,   
					 cambio_acq,   
					 cod_tipo_listino_prodotto,   
					 cod_pagamento,   
					 sconto,   
					 cod_banca_clien_for,   
					 num_ord_fornitore,   
					 data_ord_fornitore,   
					 cod_imballo,   
					 cod_vettore,   
					 cod_inoltro,   
					 cod_mezzo,   
					 cod_porto,   
					 cod_resa,   
					 flag_blocco,   
					 flag_evasione,   
					 tot_val_ordine,   
					 tot_val_evaso,   
					 nota_testata,   
					 nota_piede,   
					 data_consegna,   
					 cod_banca,
					 flag_doc_suc_tes,
					 flag_st_note_tes,					
					 flag_doc_suc_pie,
					 flag_st_note_pie)					 
	VALUES	 	 (:s_cs_xx.cod_azienda,   
					  :ll_anno_registrazione,   
					  :ll_num_registrazione,   
					  :ls_cod_tipo_ord_acq,   
					  :ldt_data_registrazione,   
					  :ls_cod_operatore,   
					  :ls_cod_fornitore,   
					  :ls_rag_soc_1,   
					  :ls_rag_soc_2,   
					  :ls_indirizzo,   
					  :ls_localita,   
					  :ls_frazione,   
					  :ls_cap,   
					  :ls_provincia,   
					  :ls_cod_fil_fornitore,   
					  :ls_cod_des_fornitore,   
					  :ls_cod_deposito,   
					  :ls_cod_valuta,   
					  :ld_cambio_acq,   
					  :ls_cod_tipo_listino_prodotto,   
					  :ls_cod_pagamento,   
					  :ld_sconto_testata,   
					  :ls_cod_banca_clien_for,   
					  null,   
					  null,   
					  :ls_cod_imballo,   
					  :ls_cod_vettore,   
					  :ls_cod_inoltro,   
					  :ls_cod_mezzo,   
					  :ls_cod_porto,   
					  :ls_cod_resa,   
					  'N',   
					  'A',   
					  0,   
					  0,   
					  :ls_nota_testata,   
					  :ls_nota_piede,   
					  :ldt_data_consegna,   
					  :ls_cod_banca,
					  'I',
					  'I',
					  'I',
					  'I');
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Errore durante la scrittura Testata Ordine Acquisto.", &
					  Exclamation!, ok!)
		rollback;
		return
	end if
	
	select tab_pagamenti.sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;
	
	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if
	
	ld_tot_val_documento = 0
	
	if isnull(ld_sconto_testata) then ld_sconto_testata = 0
	
	ll_prog_riga_ord_acq = 0
	for ll_i = 1 to dw_det_gen_ord_acq.rowcount()
		dw_det_gen_ord_acq.setrow(ll_i)
		dw_det_gen_ord_acq.setcolumn(7)
		ld_quan_richiesta = double(dw_det_gen_ord_acq.gettext())
		if ld_quan_richiesta > 0 then
			ll_prog_riga_ord_acq = ll_prog_riga_ord_acq + 10
			ld_quan_residua = dw_det_gen_ord_acq.getitemnumber(ll_i, "compute_0006")
			ll_prog_riga_off_acq = dw_det_gen_ord_acq.getitemnumber(ll_i, "prog_riga_off_acq")
			ls_cod_tipo_det_acq = dw_det_gen_ord_acq.getitemstring(ll_i, "cod_tipo_det_acq")
			ls_cod_prodotto = dw_det_gen_ord_acq.getitemstring(ll_i, "cod_prodotto")
			ls_cod_misura = dw_det_gen_ord_acq.getitemstring(ll_i, "cod_misura")
			ldt_data_consegna_det = dw_det_gen_ord_acq.getitemdatetime(ll_i, "data_consegna")
	
			SELECT det_off_acq.des_prodotto,   
					 det_off_acq.prezzo_acquisto,   
					 det_off_acq.fat_conversione,   
					 det_off_acq.sconto_1,   
					 det_off_acq.sconto_2,   
					 det_off_acq.sconto_3,   
					 det_off_acq.cod_iva,   
					 det_off_acq.cod_prod_fornitore,   
					 det_off_acq.nota_dettaglio,   
					 det_off_acq.anno_commessa,   
					 det_off_acq.num_commessa,   
					 det_off_acq.cod_centro_costo,   
					 det_off_acq.sconto_4,   
					 det_off_acq.sconto_5,   
					 det_off_acq.sconto_6,   
					 det_off_acq.sconto_7,   
					 det_off_acq.sconto_8,   
					 det_off_acq.sconto_9,   
					 det_off_acq.sconto_10,
					 det_off_acq.flag_doc_suc_det,
					 det_off_acq.anno_reg_rda,
					 det_off_acq.num_reg_rda, 
					 det_off_acq.prog_riga_rda 
			  INTO :ls_des_prodotto, 
					 :ld_prezzo_acquisto, 
					 :ld_fat_conversione, 
					 :ld_sconto_1, 
					 :ld_sconto_2, 
					 :ld_sconto_3, 
					 :ls_cod_iva, 
					 :ls_cod_prod_fornitore, 
					 :ls_nota_dettaglio, 
					 :ll_anno_commessa, 
					 :ll_num_commessa, 
					 :ls_cod_centro_costo, 
					 :ld_sconto_4, 
					 :ld_sconto_5, 
					 :ld_sconto_6, 
					 :ld_sconto_7, 
					 :ld_sconto_8,
					 :ld_sconto_9, 
					 :ld_sconto_10,
					 :ls_flag_doc_suc_det,
					 :ll_anno_reg_rda,
					 :ll_num_reg_rda,
					 :ll_prog_riga_rda
			  FROM det_off_acq  
			 WHERE det_off_acq.cod_azienda = :s_cs_xx.cod_azienda AND  
					 det_off_acq.anno_registrazione = :ll_anno_offerta AND  
					 det_off_acq.num_registrazione = :ll_num_offerta AND
					 det_off_acq.prog_riga_off_acq = :ll_prog_riga_off_acq 
			ORDER BY det_off_acq.prog_riga_off_acq ASC;
		
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Errore durante la lettura Dettagli Offerte Acquisto.", &
							  Exclamation!, ok!)
				rollback;
				return
			end if
		
			select tab_tipi_det_acq.flag_tipo_det_acq,
					 tab_tipi_det_acq.flag_sola_iva
			into   :ls_flag_tipo_det_acq,
					 :ls_flag_sola_iva
			from   tab_tipi_det_acq
			where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;
		
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettagli.", &
							  exclamation!, ok!)
				return
			end if
		
			if ls_flag_sola_iva = 'N'then
				ld_val_riga = (ld_quan_richiesta) * ld_prezzo_acquisto
				ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
				ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
				ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
				ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
				ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
				ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
				ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
				ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
				ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
				ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
				ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
				ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
				ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
				ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
				ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
				ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
				ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
				ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
				ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
			
				if ls_cod_valuta = ls_lire then
					ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
				else
					ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
				end if   
			
				if ls_flag_tipo_det_acq = "M" or &
					ls_flag_tipo_det_acq = "C" or &
					ls_flag_tipo_det_acq = "N" then
					ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
					ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
					ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
					ld_val_riga_netto = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
				elseif ls_flag_tipo_det_acq = "S" then
					ld_val_riga_netto = ld_val_riga_sconto_10 * -1
				else
					ld_val_riga_netto = ld_val_riga_sconto_10
				end if
			
				ld_tot_val_documento = ld_tot_val_documento + ld_val_riga_netto
				
				if ls_cod_valuta = ls_lire then
					ld_tot_val_documento = round(ld_tot_val_documento, 0)
				end if   
			else
				ld_val_riga_sconto_10 = 0
			end if
			
//------------------------------------------------- Modifica Nicola ---------------------------------------------
		if ls_flag_doc_suc_det = 'I' then //nota dettaglio
			select flag_doc_suc
			  into :ls_flag_doc_suc_det
			  from tab_tipi_det_acq
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_det_acq = :ls_cod_tipo_det_acq;
		end if		
		
		
		if ls_flag_doc_suc_det = 'N' then
			setnull(ls_nota_dettaglio)
		end if				
//-------------------------------------------------- Fine Modifica ----------------------------------------------			
			
			INSERT INTO det_ord_acq  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_ordine_acq,   
							 cod_tipo_det_acq,   
							 cod_prodotto,   
							 cod_misura,   
							 des_prodotto,   
							 quan_ordinata,   
							 prezzo_acquisto,   
							 fat_conversione,   
							 sconto_1,   
							 sconto_2,   
							 sconto_3,   
							 cod_iva,   
							 data_consegna,   
							 cod_prod_fornitore,   
							 quan_arrivata,   
							 val_riga,   
							 flag_saldo,   
							 flag_blocco,   
							 nota_dettaglio,   
							 anno_commessa,   
							 num_commessa,   
							 cod_centro_costo,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,   
							 anno_off_acq,   
							 num_off_acq,   
							 prog_riga_off_acq,
							 flag_doc_suc_det,
							 flag_st_note_det,
							 anno_reg_rda,
							 num_reg_rda,
							 prog_riga_rda)							 
			  VALUES		 (:s_cs_xx.cod_azienda,   
							  :ll_anno_registrazione,   
							  :ll_num_registrazione,   
							  :ll_prog_riga_ord_acq,   
							  :ls_cod_tipo_det_acq,   
							  :ls_cod_prodotto,   
							  :ls_cod_misura,   
							  :ls_des_prodotto,   
							  :ld_quan_richiesta,   
							  :ld_prezzo_acquisto,   
							  :ld_fat_conversione,   
							  :ld_sconto_1,   
							  :ld_sconto_2,   
							  :ld_sconto_3,   
							  :ls_cod_iva,   
							  :ldt_data_consegna_det,   
							  :ls_cod_prod_fornitore,   
							  0,   
							  :ld_val_riga_sconto_10,
							  'N',   
							  'N',   
							  :ls_nota_dettaglio,   
							  :ll_anno_commessa,   
							  :ll_num_commessa,   
							  :ls_cod_centro_costo,   
							  :ld_sconto_4,   
							  :ld_sconto_5,   
							  :ld_sconto_6,   
							  :ld_sconto_7,   
							  :ld_sconto_8,   
							  :ld_sconto_9,   
							  :ld_sconto_10,   
							  :ll_anno_offerta,   
							  :ll_num_offerta,   
							  :ll_prog_riga_off_acq,
							  'I',
							  'I',
							  :ll_anno_reg_rda,
							  :ll_num_reg_rda,
							  :ll_prog_riga_rda);							  
		
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Attenzione", "Errore durante la scrittura Dettagli Ordini Acquisto.", Exclamation!, ok!)
				rollback;
				return
			end if
		
		  UPDATE tes_ord_acq  
			  SET tot_val_ordine = :ld_tot_val_documento  
			WHERE tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda AND  
					tes_ord_acq.anno_registrazione = :ll_anno_registrazione AND  
					tes_ord_acq.num_registrazione = :ll_num_registrazione;
		
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Attenzione", "Errore durante l'aggiornamento Testata Ordini Acquisto.", &
							  Exclamation!, ok!)
				rollback;
				return
			end if
		
			if ls_flag_tipo_det_acq = "M" then
				update anag_prodotti  
				set 	 quan_ordinata = quan_ordinata + :ld_quan_richiesta
				where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", &
								  exclamation!, ok!)
					rollback;
					return
				end if
				
				// enme 08/1/2006 gestione prodotto raggruppato
				setnull(ls_cod_prodotto_raggruppato)
				
				select cod_prodotto_raggruppato
				into   :ls_cod_prodotto_raggruppato
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
						 
				if not isnull(ls_cod_prodotto_raggruppato) then
					
					ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_richiesta, "M")
					
					update anag_prodotti  
						set quan_ordinata = quan_ordinata + :ld_quan_raggruppo  
					 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
							 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
	
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
						return 1
					end if
				end if
				
			end if				
		
			if ld_quan_residua = ld_quan_richiesta then
				ls_flag = 'E'
			else
				ls_flag = 'P'
			end if
			
			UPDATE det_off_acq  
				SET flag_evasione = :ls_flag,   
					 quan_evasa = quan_evasa + :ld_quan_richiesta
			 WHERE det_off_acq.cod_azienda = :s_cs_xx.cod_azienda AND  
					 det_off_acq.anno_registrazione = :ll_anno_offerta AND  
					 det_off_acq.num_registrazione = :ll_num_offerta AND  
					 det_off_acq.prog_riga_off_acq = :ll_prog_riga_off_acq;
		
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento Dettaglio Offerte Acquisto.", &
							  exclamation!, ok!)
				rollback;
				return
			end if

			// aggiunto per RDA; se genero ordine da offerta metto il flag generato ordine anche su RDA.
			if isnull(ll_anno_reg_rda) then ll_anno_reg_rda = 0
			if ll_anno_reg_rda > 0 then
				update det_rda
				set flag_gen_ord_acq = 'S'
				where cod_azienda = :s_cs_xx.cod_azienda and
						anno_reg_rda = :ll_anno_reg_rda and
						num_reg_rda = :ll_num_reg_rda and
						prog_riga_rda = :ll_prog_riga_rda;
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("APICE", "Si è verificato un errore in fase di aggiornamento RDA con flag_gen_ord_acq = 'S'.~r~n" + sqlca.sqlerrtext, exclamation!, ok!)
					rollback;
					return
				end if
			end if
			
			ls_tabella_orig = "det_off_acq_stat"
			ls_tabella_des = "det_ord_acq_stat"
			ls_prog_riga_doc_orig = "prog_riga_off_acq"
			ls_prog_riga_doc_des = "prog_riga_ordine_acq"
			if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ll_anno_offerta, ll_num_offerta, ll_prog_riga_off_acq, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_acq) = -1 then
				return
			end if	

			select tab_tipi_ord_acq.cod_tipo_analisi  
			into	 :ls_cod_tipo_analisi_ord
			from   tab_tipi_ord_acq  
			where  tab_tipi_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_tipi_ord_acq.cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore nella lettura della Tabella Tipi Ordini.", &
							  Exclamation!, ok!)
				rollback;
				return
			end if
	
			if ls_cod_tipo_analisi_off <> ls_cod_tipo_analisi_ord then
				if f_crea_distribuzione(ls_cod_tipo_analisi_ord, ls_cod_prodotto, ls_cod_tipo_det_acq, ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_ord_acq, 2) = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore nella Generazione dei Dettagli Statistici.", &
								  Exclamation!, ok!)
					rollback;
					return
				end if
			end if
		end if
	next
	
	DECLARE cu_det_off_acq CURSOR FOR  
	 SELECT det_off_acq.flag_evasione  
		FROM det_off_acq  
	  WHERE det_off_acq.cod_azienda = :s_cs_xx.cod_azienda AND  
			  det_off_acq.anno_registrazione = :ll_anno_offerta AND  
			  det_off_acq.num_registrazione = :ll_num_offerta;
	
	open cu_det_off_acq;
	
	ll_i1 = 0
	ll_flag = 0
	do while 0 = 0
		fetch cu_det_off_acq into :ls_flag;
		ll_i1 ++	

		if ls_flag = 'E' then
			ll_flag ++
		end if

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Errore durante la lettura Dettagli Offerte Acquisto.", &
						  Exclamation!, ok!)
			rollback;
			close cu_det_off_acq;
			return
		end if
	loop
	
	close cu_det_off_acq;
	
	if ll_flag = ll_i1 then
		ls_flag = 'E'
	else
		ls_flag = 'P'
	end if

	UPDATE tes_off_acq  
		SET flag_evasione = :ls_flag  
	 WHERE tes_off_acq.cod_azienda = :s_cs_xx.cod_azienda AND  
			 tes_off_acq.anno_registrazione = :ll_anno_offerta AND  
			 tes_off_acq.num_registrazione = :ll_num_offerta;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento Testata Offerte Acquisto.", &
					  exclamation!, ok!)
		rollback;
		return
	end if
	
	commit;
	
	s_cs_xx.parametri.parametro_d_1 = ll_anno_offerta
	s_cs_xx.parametri.parametro_d_2 = ll_num_offerta
	s_cs_xx.parametri.parametro_d_3 = ll_anno_registrazione
	s_cs_xx.parametri.parametro_d_4 = ll_num_registrazione
	s_cs_xx.parametri.parametro_b_1 = true
else
	s_cs_xx.parametri.parametro_b_1 = false
end if

setpointer(arrow!)
close(parent)
end event

type cb_1 from uo_cb_close within w_det_gen_ord_acq
integer x = 2880
integer y = 1140
integer width = 366
integer height = 80
integer taborder = 3
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

type dw_det_gen_ord_acq from uo_cs_xx_dw within w_det_gen_ord_acq
integer x = 23
integer y = 20
integer width = 3223
integer height = 1100
integer taborder = 10
string dataobject = "d_det_gen_ord_acq"
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_d_1, s_cs_xx.parametri.parametro_d_2)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

