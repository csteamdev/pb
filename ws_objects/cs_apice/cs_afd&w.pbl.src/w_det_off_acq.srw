﻿$PBExportHeader$w_det_off_acq.srw
$PBExportComments$Finestra Gestione  Dettaglio Offerte Acquisto
forward
global type w_det_off_acq from w_cs_xx_principale
end type
type st_2 from statictext within w_det_off_acq
end type
type st_1 from statictext within w_det_off_acq
end type
type dw_det_off_acq_lista from uo_cs_xx_dw within w_det_off_acq
end type
type cb_blocca from commandbutton within w_det_off_acq
end type
type cb_prezzo from commandbutton within w_det_off_acq
end type
type cb_sblocca from commandbutton within w_det_off_acq
end type
type cb_sconti from commandbutton within w_det_off_acq
end type
type pb_prod_view from cb_listview within w_det_off_acq
end type
type cb_corrispondenze from commandbutton within w_det_off_acq
end type
type cb_c_industriale from commandbutton within w_det_off_acq
end type
type cb_prodotti_note_ricerca from cb_prod_note_ricerca within w_det_off_acq
end type
type dw_det_off_acq_det_2 from uo_cs_xx_dw within w_det_off_acq
end type
type dw_det_off_acq_det_1 from uo_cs_xx_dw within w_det_off_acq
end type
type dw_folder from u_folder within w_det_off_acq
end type
end forward

global type w_det_off_acq from w_cs_xx_principale
integer width = 3579
integer height = 1924
string title = "Dettaglio Offerte Acquisto"
boolean minbox = false
event ue_imposta_colore ( )
st_2 st_2
st_1 st_1
dw_det_off_acq_lista dw_det_off_acq_lista
cb_blocca cb_blocca
cb_prezzo cb_prezzo
cb_sblocca cb_sblocca
cb_sconti cb_sconti
pb_prod_view pb_prod_view
cb_corrispondenze cb_corrispondenze
cb_c_industriale cb_c_industriale
cb_prodotti_note_ricerca cb_prodotti_note_ricerca
dw_det_off_acq_det_2 dw_det_off_acq_det_2
dw_det_off_acq_det_1 dw_det_off_acq_det_1
dw_folder dw_folder
end type
global w_det_off_acq w_det_off_acq

type variables
boolean ib_modifica=false, ib_nuovo=false


end variables

event ue_imposta_colore();string	ls_modify, ls_flag_tipo_des_riga


select
	flag_tipo_des_riga
into
	:ls_flag_tipo_des_riga
from
	con_ord_acq
where
	cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode = 0 and ls_flag_tipo_des_riga  = "S" then
	ls_modify = dw_det_off_acq_lista.modify("des_prodotto.color='0~tif(des_prodotto <> anag_prodotti_des_prodotto or isnull(des_prodotto), 255, 0)'")
	if ls_modify <> "" then
		g_mb.messagebox("APICE",ls_modify,stopsign!)
	end if
end if
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det_off_acq_lista, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_det_off_acq_det_1, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//f_po_loaddddw_dw(dw_det_off_acq_det_1, &
//                 "cod_prodotto", &
//                 sqlca, &
//                 "anag_prodotti", &
//                 "cod_prodotto", &
//                 "des_prodotto", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_off_acq_det_1, &
                 "cod_misura", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//f_po_loaddddw_dw(dw_det_off_acq_lista, &
//                 "cod_misura", &
//                 sqlca, &
//                 "tab_misure", &
//                 "cod_misura", &
//                 "des_misura", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//f_po_loaddddw_dw(dw_det_off_acq_det_3, &
//                 "cod_centro_costo", &
//                 sqlca, &
//                 "tab_centri_costo", &
//                 "cod_centro_costo", &
//                 "des_centro_costo", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_off_acq_det_2, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;dw_det_off_acq_lista.set_dw_key("cod_azienda")
dw_det_off_acq_lista.set_dw_key("anno_registrazione")
dw_det_off_acq_lista.set_dw_key("num_registrazione")

dw_det_off_acq_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + c_InactiveDWColorUnchanged)
dw_det_off_acq_det_1.set_dw_options(sqlca, &
                                    dw_det_off_acq_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
dw_det_off_acq_det_2.set_dw_options(sqlca, &
                                    dw_det_off_acq_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

windowobject lw_oggetti[]
lw_oggetti[1] = dw_det_off_acq_det_2
lw_oggetti[2] = cb_prodotti_note_ricerca
dw_folder.fu_assigntab(2, "Note / Analitica", lw_oggetti[])
lw_oggetti[1] = dw_det_off_acq_det_1
lw_oggetti[2] = cb_sconti
lw_oggetti[3] = cb_prezzo
lw_oggetti[4] = cb_blocca
lw_oggetti[5] = cb_sblocca
lw_oggetti[6] = cb_c_industriale
lw_oggetti[7] = cb_corrispondenze
lw_oggetti[8] = pb_prod_view
dw_folder.fu_assigntab(1, "Dettaglio", lw_oggetti[])
dw_folder.fu_foldercreate(2, 4)
dw_folder.fu_selecttab(1)

iuo_dw_main=dw_det_off_acq_lista
cb_prezzo.enabled=false
cb_blocca.enabled=false
cb_sblocca.enabled=false
cb_c_industriale.enabled=false
cb_corrispondenze.enabled=false

postevent("ue_imposta_colore")

// stefano 07/05/2010: arrivo dalle offerte fornitori, e non posso modificare
if s_cs_xx.parametri.parametro_b_1 then
	set_w_options(c_noenablepopup)
	cb_prezzo.enabled=false
	cb_blocca.enabled=false
	cb_sblocca.enabled=false
	cb_c_industriale.enabled=false
	cb_corrispondenze.enabled=false
end if
// ----

dw_det_off_acq_lista.is_cod_parametro_blocco_prodotto = 'POA'
dw_det_off_acq_det_1.is_cod_parametro_blocco_prodotto = 'POA'

end event

on w_det_off_acq.create
int iCurrent
call super::create
this.st_2=create st_2
this.st_1=create st_1
this.dw_det_off_acq_lista=create dw_det_off_acq_lista
this.cb_blocca=create cb_blocca
this.cb_prezzo=create cb_prezzo
this.cb_sblocca=create cb_sblocca
this.cb_sconti=create cb_sconti
this.pb_prod_view=create pb_prod_view
this.cb_corrispondenze=create cb_corrispondenze
this.cb_c_industriale=create cb_c_industriale
this.cb_prodotti_note_ricerca=create cb_prodotti_note_ricerca
this.dw_det_off_acq_det_2=create dw_det_off_acq_det_2
this.dw_det_off_acq_det_1=create dw_det_off_acq_det_1
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_2
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.dw_det_off_acq_lista
this.Control[iCurrent+4]=this.cb_blocca
this.Control[iCurrent+5]=this.cb_prezzo
this.Control[iCurrent+6]=this.cb_sblocca
this.Control[iCurrent+7]=this.cb_sconti
this.Control[iCurrent+8]=this.pb_prod_view
this.Control[iCurrent+9]=this.cb_corrispondenze
this.Control[iCurrent+10]=this.cb_c_industriale
this.Control[iCurrent+11]=this.cb_prodotti_note_ricerca
this.Control[iCurrent+12]=this.dw_det_off_acq_det_2
this.Control[iCurrent+13]=this.dw_det_off_acq_det_1
this.Control[iCurrent+14]=this.dw_folder
end on

on w_det_off_acq.destroy
call super::destroy
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_det_off_acq_lista)
destroy(this.cb_blocca)
destroy(this.cb_prezzo)
destroy(this.cb_sblocca)
destroy(this.cb_sconti)
destroy(this.pb_prod_view)
destroy(this.cb_corrispondenze)
destroy(this.cb_c_industriale)
destroy(this.cb_prodotti_note_ricerca)
destroy(this.dw_det_off_acq_det_2)
destroy(this.dw_det_off_acq_det_1)
destroy(this.dw_folder)
end on

event pc_delete;call super::pc_delete;long ll_i

if dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "flag_evasione") <> "A" then
   g_mb.messagebox("Attenzione", "Offerta non cancellabile! Ha già subito un'evasione.", &
              exclamation!, ok!)
   dw_det_off_acq_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

for ll_i = 1 to dw_det_off_acq_lista.deletedcount()
	if dw_det_off_acq_lista.getitemstring(ll_i, "flag_blocco", delete!, true) = "S" then
	   g_mb.messagebox("Attenzione", "Offerta non cancellabile! E' stata bloccata.", &
	              exclamation!, ok!)
	   dw_det_off_acq_lista.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return
	end if
next

cb_prezzo.enabled=false
cb_blocca.enabled=false
cb_sblocca.enabled=false
cb_c_industriale.enabled=false
cb_corrispondenze.enabled=false
end event

event pc_modify;call super::pc_modify;if dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "flag_evasione") <> "A" then
   g_mb.messagebox("Attenzione", "Offerta non modificabile! Ha già subito un'evasione.", &
              exclamation!, ok!)
   dw_det_off_acq_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

if dw_det_off_acq_lista.getitemstring(dw_det_off_acq_lista.getrow(), "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Offerta non modificabile! E' stata bloccata.", &
              exclamation!, ok!)
   dw_det_off_acq_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

end event

event pc_new;call super::pc_new;if dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "flag_evasione") <> "A" then
   g_mb.messagebox("Attenzione", "Offerta non modificabile! Ha già subito un'evasione.", &
              exclamation!, ok!)
   dw_det_off_acq_lista.set_dw_view(c_ignorechanges)
   dw_det_off_acq_det_1.set_dw_view(c_ignorechanges)
   dw_det_off_acq_det_2.set_dw_view(c_ignorechanges)
//   dw_det_off_acq_det_3.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

if dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Offerta non modificabile! E' stata chiusa.", &
              exclamation!, ok!)
   dw_det_off_acq_lista.set_dw_view(c_ignorechanges)
   dw_det_off_acq_det_1.set_dw_view(c_ignorechanges)
   dw_det_off_acq_det_2.set_dw_view(c_ignorechanges)
//   dw_det_off_acq_det_3.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

end event

type st_2 from statictext within w_det_off_acq
integer x = 69
integer y = 1692
integer width = 3401
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_1 from statictext within w_det_off_acq
integer x = 69
integer y = 1632
integer width = 1193
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "SHIFT - F4 sulla lista = Nuova Riga"
boolean focusrectangle = false
end type

type dw_det_off_acq_lista from uo_cs_xx_dw within w_det_off_acq
integer x = 23
integer y = 20
integer width = 3497
integer height = 660
integer taborder = 50
string dataobject = "d_det_off_acq_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_acq, ls_cod_tipo_off_acq, ls_flag_tipo_det_acq, &
          ls_modify, ls_null, ls_cod_fornitore, ls_cod_for_pot, ls_cod_iva
   long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_acq, ll_null
   datetime ldt_data_consegna, ldt_data_esenzione_iva, ldt_data_registrazione

   setnull(ls_null)
	setnull(ll_null)
   ll_anno_registrazione = dw_det_off_acq_lista.i_parentdw.getitemnumber(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "anno_registrazione")
   ll_num_registrazione = dw_det_off_acq_lista.i_parentdw.getitemnumber(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "num_registrazione")
   ldt_data_registrazione = dw_det_off_acq_lista.i_parentdw.getitemdatetime(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
   ls_cod_fornitore = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")
   ls_cod_for_pot = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cod_for_pot")
   ldt_data_consegna = dw_det_off_acq_lista.i_parentdw.getitemdatetime(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "data_consegna")

   select max(det_off_acq.prog_riga_off_acq)
   into   :ll_prog_riga_off_acq
   from   det_off_acq
   where  det_off_acq.cod_azienda = :s_cs_xx.cod_azienda and
          det_off_acq.anno_registrazione = :ll_anno_registrazione and
          det_off_acq.num_registrazione = :ll_num_registrazione;

   if isnull(ll_prog_riga_off_acq) then
      dw_det_off_acq_det_1.setitem(dw_det_off_acq_det_1.getrow(), "prog_riga_off_acq", 10)
   else
      dw_det_off_acq_det_1.setitem(dw_det_off_acq_det_1.getrow(), "prog_riga_off_acq", ll_prog_riga_off_acq + 10)
   end if

   ls_cod_tipo_off_acq = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cod_tipo_off_acq")

   select tab_tipi_off_acq.cod_tipo_det_acq
   into   :ls_cod_tipo_det_acq
   from   tab_tipi_off_acq
   where  tab_tipi_off_acq.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_off_acq.cod_tipo_off_acq = :ls_cod_tipo_off_acq;
   
   if sqlca.sqlcode = 0 then
      dw_det_off_acq_det_1.setitem(dw_det_off_acq_det_1.getrow(), "cod_tipo_det_acq", ls_cod_tipo_det_acq)
      if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
           select tab_tipi_det_acq.cod_iva  
           into   :ls_cod_iva  
           from   tab_tipi_det_acq  
           where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
                  tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;
           if sqlca.sqlcode = 0 then
              this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
           end if
      end if
   else
      ls_cod_tipo_det_acq = ls_null
   end if

   dw_det_off_acq_det_1.setitem(dw_det_off_acq_det_1.getrow(), "quan_ordinata", 1)
   dw_det_off_acq_det_1.setitem(dw_det_off_acq_det_1.getrow(), "data_consegna", ldt_data_consegna)
   
   if not isnull(ls_cod_fornitore) then
      select anag_fornitori.cod_iva,
             anag_fornitori.data_esenzione_iva
      into   :ls_cod_iva,
             :ldt_data_esenzione_iva
      from   anag_fornitori
      where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
             anag_fornitori.cod_fornitore = :ls_cod_fornitore;
   else
      select anag_for_pot.cod_iva,
             anag_for_pot.data_esenzione_iva
      into   :ls_cod_iva,
             :ldt_data_esenzione_iva
      from   anag_for_pot
      where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
             anag_for_pot.cod_for_pot = :ls_cod_for_pot;
   end if
   
   if sqlca.sqlcode = 0 then
      if ls_cod_iva <> "" and &
         (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
         this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
      end if
   end if
   
   ls_modify = "cod_tipo_det_acq.protect='0'~t"
   dw_det_off_acq_det_1.modify(ls_modify)
   ls_modify = "cod_tipo_det_acq.background.color='16777215'~t"
   dw_det_off_acq_det_1.modify(ls_modify)

   if not isnull(ls_cod_tipo_det_acq) then
      dw_det_off_acq_det_1.setitem(dw_det_off_acq_det_1.getrow(), "cod_tipo_det_acq", ls_cod_tipo_det_acq)
      ld_datawindow = dw_det_off_acq_det_1
		lp_prod_view = pb_prod_view
		setnull(lc_prodotti_ricerca)
      f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_acq)
   else
      ls_modify = "cod_prodotto.protect='1'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "cod_prodotto.background.color='12632256'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "des_prodotto.protect='1'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "des_prodotto.background.color='12632256'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "cod_misura.protect='1'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "cod_misura.background.color='12632256'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "fat_conversione.protect='1'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "fat_conversione.background.color='12632256'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "quan_ordinata.protect='1'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "quan_ordinata.background.color='12632256'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "prezzo_acquisto.protect='1'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "prezzo_acquisto.background.color='12632256'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_1.protect='1'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_1.background.color='12632256'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_2.protect='1'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_2.background.color='12632256'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "cod_iva.protect='1'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "cod_iva.background.color='12632256'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "data_consegna.protect='1'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "data_consegna.background.color='12632256'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "cod_prod_fornitore.protect='1'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "cod_prod_fornitore.background.color='12632256'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
   end if

   ib_nuovo = true
   cb_sconti.enabled = true
//   cb_commesse_ric.enabled = true
	cb_prezzo.enabled=true
	cb_blocca.enabled=false
	cb_sblocca.enabled=false
	cb_c_industriale.enabled=false
	cb_corrispondenze.enabled=false
	
	dw_det_off_acq_det_2.setitem(dw_det_off_acq_lista.getrow(), "flag_doc_suc_det", "I")
	dw_det_off_acq_det_2.setitem(dw_det_off_acq_lista.getrow(), "flag_st_note_det", "I")	
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
   long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_prog_riga_off_acq
   double ld_sconto_testata
   string ls_cod_pagamento, ls_cod_valuta, ls_tabella, ls_quan_documento, &
          ls_tot_val_documento, ls_nome_prog
	
   ll_i = i_parentdw.i_selectedrows[1]

   ll_anno_registrazione = i_parentdw.getitemnumber(ll_i, "anno_registrazione")
   ll_num_registrazione = i_parentdw.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_off_acq = getitemnumber(this.getrow(), "prog_riga_off_acq")
   ld_sconto_testata = i_parentdw.getitemnumber(ll_i, "sconto")
   ls_cod_pagamento = i_parentdw.getitemstring(ll_i, "cod_pagamento")
   ls_cod_valuta = i_parentdw.getitemstring(ll_i, "cod_valuta")
   ls_tabella = "tes_off_acq"
   ls_quan_documento = "quan_ordinata"
   ls_tot_val_documento = "tot_val_offerta"

	ib_modifica = false
	ib_nuovo = false
	dw_det_off_acq_det_1.object.b_ricerca_prodotto.enabled = false
	pb_prod_view.enabled = false
	cb_prodotti_note_ricerca.enabled = false
	cb_sconti.enabled = false
	cb_prezzo.enabled=false
	cb_blocca.enabled=true
	cb_sblocca.enabled=true

   this.change_dw_current()
   f_crea_listini_fornitori()

   ls_tabella = "det_off_acq_stat"
	ls_nome_prog = "prog_riga_off_acq"

   for ll_i = 1 to this.deletedcount()
		ll_prog_riga_off_acq = this.getitemnumber(ll_i, "prog_riga_off_acq", delete!, true)		
      if f_elimina_det_stat(ls_tabella, ll_anno_registrazione, ll_num_registrazione,ls_nome_prog,ll_prog_riga_off_acq) = -1 then
		return 1
	end if
   next
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	ib_modifica = false
	ib_nuovo = false
	cb_prodotti_note_ricerca.enabled = false
dw_det_off_acq_det_1.object.b_ricerca_prodotto.enabled = false
	pb_prod_view.enabled = false
	cb_sconti.enabled = false
//	cb_commesse_ric.enabled = false
	cb_prezzo.enabled=false
	
	// stefano 07/05/2010: controllo il parametro s_cs_xx.parametri.parametro_b_1 per verificare
	// se la finestra è in sola visualizzazione, se si blocco i pulsanti
	if s_cs_xx.parametri.parametro_b_1 then return
	
	if this.getrow() > 0 then
		if this.getitemnumber(this.getrow(), "anno_registrazione") > 0 then
		cb_c_industriale.enabled=true
		cb_corrispondenze.enabled=true
		cb_blocca.enabled=true
		cb_sblocca.enabled=true
		end if
	else
		cb_c_industriale.enabled=false
		cb_corrispondenze.enabled=false
		cb_blocca.enabled=false
		cb_sblocca.enabled=false
	end if
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_fornitore, ls_cod_for_pot, ls_cod_iva, ls_cod_tipo_det_acq, ls_modify, &
	       ls_cod_misura_mag, ls_cod_prodotto
   datetime ldt_data_registrazione, ldt_data_esenzione_iva

   ls_cod_fornitore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fornitore")
   ldt_data_registrazione = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_registrazione")

	if this.getrow() > 0 then
		
		ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")		

      select anag_prodotti.cod_misura_mag
      into   :ls_cod_misura_mag
      from   anag_prodotti
      where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
             anag_prodotti.cod_prodotto = :ls_cod_prodotto;

		if sqlca.sqlcode = 0 then
	   	dw_det_off_acq_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
	   	dw_det_off_acq_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")
		else
	   	dw_det_off_acq_det_1.modify("st_prezzo.text='Prezzo:'")
	   	dw_det_off_acq_det_1.modify("st_quantita.text='Quantità:'")
		end if
		
		if not isnull(this.getitemstring(this.getrow(),"cod_misura")) then
		   cb_prezzo.text = "Prezzo al " + this.getitemstring(this.getrow(),"cod_misura")
		else
		   cb_prezzo.text = "Prezzo"
		end if
		
		if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
			len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
			len(trim(ls_cod_misura_mag)) <> 0 then
			dw_det_off_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
		else
			dw_det_off_acq_det_1.modify("st_fattore_conv.text=''")		
		end if
	
		if not isnull(ls_cod_fornitore) then
			select anag_fornitori.cod_iva,
					 anag_fornitori.data_esenzione_iva
			into   :ls_cod_iva,
					 :ldt_data_esenzione_iva
			from   anag_fornitori
			where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_fornitori.cod_fornitore = :ls_cod_fornitore;
		else
			ls_cod_for_pot = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_for_pot")
	
			select anag_for_pot.cod_iva,
					 anag_for_pot.data_esenzione_iva
			into   :ls_cod_iva,
					 :ldt_data_esenzione_iva
			from   anag_for_pot
			where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_for_pot.cod_for_pot = :ls_cod_for_pot;
		end if
	
		if sqlca.sqlcode = 0 then
			if ls_cod_iva <> "" and ldt_data_esenzione_iva <= ldt_data_registrazione then
				f_po_loaddddw_dw(dw_det_off_acq_lista, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_calcolo = 'N'")
				f_po_loaddddw_dw(dw_det_off_acq_det_1, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_calcolo = 'N'")
			else
				f_po_loaddddw_dw(dw_det_off_acq_lista, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
				f_po_loaddddw_dw(dw_det_off_acq_det_1, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
			end if
		end if
	
		if ib_modifica or ib_nuovo then
			ls_cod_tipo_det_acq = dw_det_off_acq_det_1.getitemstring(dw_det_off_acq_det_1.getrow(), "cod_tipo_det_acq")
			ld_datawindow = dw_det_off_acq_det_1
			lp_prod_view = pb_prod_view
			setnull(lc_prodotti_ricerca)
			f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_acq)
			f_tipo_dettaglio_acq_lista(dw_det_off_acq_lista, ls_cod_tipo_det_acq)
		
			if not isnull(dw_det_off_acq_lista.getitemstring(dw_det_off_acq_det_1.getrow(),"cod_prodotto")) then
				cb_prodotti_note_ricerca.enabled = true
			else
				cb_prodotti_note_ricerca.enabled = false
			end if
		end if
		
	end if

   if ib_nuovo then
      ls_modify = "cod_tipo_det_acq.protect='0~tif(isrownew(),0,1)'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
      ls_modify = "cod_tipo_det_acq.background.color='16777215~tif(isrownew(),16777215,12632256)'~t"
      dw_det_off_acq_det_1.modify(ls_modify)
   end if

end if
end event

event updateend;call super::updateend;this.postevent("pcd_view")

if isvalid(s_cs_xx.parametri.parametro_w_off_acq) then
	s_cs_xx.parametri.parametro_w_off_acq.postevent("ue_calcola_documento")
end if

end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if
next

end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore, ll_anno_registrazione, ll_num_registrazione


ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_modify;call super::pcd_modify;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_acq, ls_modify


   ib_modifica = true

   ls_modify = "cod_tipo_det_acq.protect='1'~t"
   dw_det_off_acq_det_1.modify(ls_modify)
   ls_modify = "cod_tipo_det_acq.background.color='12632256'~t"
   dw_det_off_acq_det_1.modify(ls_modify)
   ls_cod_tipo_det_acq = dw_det_off_acq_det_1.getitemstring(dw_det_off_acq_det_1.getrow(), "cod_tipo_det_acq")
   ld_datawindow = dw_det_off_acq_det_1
	lp_prod_view = pb_prod_view
	setnull(lc_prodotti_ricerca)
   f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_acq)

   if not isnull(dw_det_off_acq_det_1.getitemstring(dw_det_off_acq_det_1.getrow(),"cod_prodotto")) then
      cb_prodotti_note_ricerca.enabled = true
   else
      cb_prodotti_note_ricerca.enabled = false
   end if
   cb_sconti.enabled = true
//   cb_commesse_ric.enabled = true
	cb_prezzo.enabled=true
	cb_blocca.enabled=false
	cb_sblocca.enabled=false
	cb_c_industriale.enabled=false
	cb_corrispondenze.enabled=false
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_i1,ll_prog_riga_off_acq
	string  ls_cod_tipo_analisi,ls_cod_prodotto,ls_cod_tipo_off_acq,ls_cod_tipo_det_acq,ls_test
	integer li_risposta

	ll_i = dw_det_off_acq_lista.i_parentdw.i_selectedrows[1]
	ll_i1 = dw_det_off_acq_lista.getrow()

	ll_anno_registrazione = dw_det_off_acq_lista.i_parentdw.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = dw_det_off_acq_lista.i_parentdw.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_off_acq = dw_det_off_acq_lista.getitemnumber(ll_i1, "prog_riga_off_acq")
	ls_cod_tipo_det_acq = dw_det_off_acq_lista.getitemstring(ll_i1, "cod_tipo_det_acq")
	ls_cod_prodotto= dw_det_off_acq_lista.getitemstring(ll_i1, "cod_prodotto")
	ls_cod_tipo_off_acq = dw_det_off_acq_lista.i_parentdw.getitemstring(ll_i, "cod_tipo_off_acq")
	
	SELECT cod_tipo_analisi
	INTO   :ls_cod_tipo_analisi  
	FROM   tab_tipi_off_acq
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND    cod_tipo_off_acq = :ls_cod_tipo_off_acq;

	select cod_azienda
	into   :ls_test
	from 	 det_off_acq_stat
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_registrazione
	and    num_registrazione=:ll_num_registrazione
	and    prog_riga_off_acq=:ll_prog_riga_off_acq;

	if sqlca.sqlcode=100 then
		li_risposta = f_crea_distribuzione(ls_cod_tipo_analisi,ls_cod_prodotto,ls_cod_tipo_det_acq,ll_num_registrazione,ll_anno_registrazione,ll_prog_riga_off_acq,1)
		if li_risposta=-1 then
			g_mb.messagebox("Apice","Attenzione! Si è verificato un errore durante la creazione dei dettagli statistici.",exclamation!)
		end if
	end if

end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_cod_iva_esente, ls_cod_iva_prodotto, &
          ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, &
          ls_flag_tipo_det_acq, ls_modify, ls_null, ls_cod_tipo_det_acq, ls_flag_decimali, &
          ls_cod_for_pot, ls_cod_prod_fornitore, ls_cod_misura_mag, ls_nota_prodotto, ls_des_prodotto, ls_flag_tipo_des_riga
   double ld_fat_conversione, ld_quantita, ld_cambio_acq, ld_quan_minima, ld_inc_ordine, &
          ld_quan_massima, ld_tempo_app
   datetime ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva
	long ll_riga_origine, ll_i

   setnull(ls_null)

   ls_cod_tipo_listino_prodotto = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
   ldt_data_registrazione = dw_det_off_acq_lista.i_parentdw.getitemdatetime(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
   ls_cod_fornitore = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")
   ls_cod_for_pot = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cod_for_pot")
   ls_cod_valuta = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
   ld_cambio_acq = dw_det_off_acq_lista.i_parentdw.getitemnumber(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cambio_acq")
   ls_cod_tipo_det_acq = this.getitemstring(this.getrow(),"cod_tipo_det_acq")
   
   choose case i_colname
      case "cod_tipo_det_acq"
         ls_modify = "cod_prodotto.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "cod_prodotto.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "cod_misura.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "cod_misura.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "fat_conversione.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "fat_conversione.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "quan_ordinata.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "quan_ordinata.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "prezzo_acquisto.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "prezzo_acquisto.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_1.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_1.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_2.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_2.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "cod_iva.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "cod_iva.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "data_consegna.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "data_consegna.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "cod_prod_fornitore.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "cod_prod_fornitore.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
   
         ld_datawindow = dw_det_off_acq_det_1
			lp_prod_view = pb_prod_view
			setnull(lc_prodotti_ricerca)
         f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext)
         if isnull(this.getitemstring(this.getrow(),"cod_prodotto")) then
            cb_prodotti_note_ricerca.enabled=false
         end if

         if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
              select tab_tipi_det_acq.cod_iva  
              into   :ls_cod_iva  
              from   tab_tipi_det_acq  
              where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
                     tab_tipi_det_acq.cod_tipo_det_acq = :i_coltext;
              if sqlca.sqlcode = 0 then
                 this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
              end if
         end if
			dw_det_off_acq_det_2.setitem(dw_det_off_acq_det_2.getrow(), "nota_dettaglio", ls_null)
			
      case "cod_prodotto"
	
			if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1

			setnull(ls_cod_iva_prodotto)
			setnull(ls_cod_iva_esente)
			setnull(ls_cod_iva)
			
	         select des_prodotto,
			anag_prodotti.cod_misura_mag,
					 anag_prodotti.cod_misura_acq,
                anag_prodotti.fat_conversione_acq,
                anag_prodotti.flag_decimali,
                anag_prodotti.cod_iva,
                anag_prodotti.quan_minima,
                anag_prodotti.inc_ordine,
                anag_prodotti.quan_massima,
                anag_prodotti.tempo_app
         into   :ls_des_prodotto,
			:ls_cod_misura_mag,
					 :ls_cod_misura,
                :ld_fat_conversione,
                :ls_flag_decimali,
                :ls_cod_iva_prodotto,
                :ld_quan_minima,
                :ld_inc_ordine,
                :ld_quan_massima,
                :ld_tempo_app
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :i_coltext;
   
         if sqlca.sqlcode = 0 then
            this.setitem(i_rownbr, "cod_misura", ls_cod_misura_mag)
				dw_det_off_acq_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
				dw_det_off_acq_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")
            if ld_fat_conversione <> 0 then
               this.setitem(i_rownbr, "fat_conversione", ld_fat_conversione)
            else
               this.setitem(i_rownbr, "fat_conversione", 1)
            end if
			
			select
				flag_tipo_des_riga
			into
				:ls_flag_tipo_des_riga
			from
				con_ord_acq
			where
				cod_azienda = :s_cs_xx.cod_azienda;
			
			if sqlca.sqlcode = 0 and ls_flag_tipo_des_riga  = "S" then
				setitem(i_rownbr,"des_prodotto",ls_des_prodotto)
			end if
			
            // this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
			else
				dw_det_off_acq_det_1.modify("st_prezzo.text='Prezzo:'")
				dw_det_off_acq_det_1.modify("st_quantita.text='Quantità:'")
				dw_det_off_acq_lista.change_dw_current()
				guo_ricerca.uof_ricerca_prodotto(dw_det_off_acq_lista,"cod_prodotto")
         end if

			if not isnull(this.getitemstring(i_rownbr, "cod_misura")) then
				cb_prezzo.text= "Prezzo al " + this.getitemstring(i_rownbr, "cod_misura")
			else
				cb_prezzo.text= "Prezzo"
			end if

         if not isnull(ls_cod_fornitore) then
            select anag_fornitori.cod_iva,
                   anag_fornitori.data_esenzione_iva
            into   :ls_cod_iva_esente,
                   :ldt_data_esenzione_iva
            from   anag_fornitori
            where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                   anag_fornitori.cod_fornitore = :ls_cod_fornitore;
         else
            select anag_for_pot.cod_iva,
                   anag_for_pot.data_esenzione_iva
            into   :ls_cod_iva_esente,
                   :ldt_data_esenzione_iva
            from   anag_for_pot
            where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
                   anag_for_pot.cod_for_pot = :ls_cod_for_pot;
         end if

         if sqlca.sqlcode = 0 then
            if not isnull(ls_cod_iva_esente) and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
               this.setitem(i_rownbr, "cod_iva", ls_cod_iva_esente)
				else																	// fornitore soggetto ad iva
					if not isnull(ls_cod_iva_prodotto) then				// iva inserita in anag_prodotti
	               this.setitem(i_rownbr, "cod_iva", ls_cod_iva_prodotto)
					else																// iba da tipo dettaglio
						ls_cod_tipo_det_acq = this.getitemstring(getrow(),"cod_tipo_det_acq")
						if not isnull(ls_cod_tipo_det_acq) then
							select cod_iva
							into   :ls_cod_iva
							from   tab_tipi_det_acq
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_tipo_det_acq = :ls_cod_tipo_det_acq;
						end if
	               this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
					end if
            end if
         end if

         ls_cod_prodotto = i_coltext
         ld_quantita = this.getitemnumber(i_rownbr, "quan_ordinata")
         if not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
            ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")
         else
            ldt_data_consegna = ldt_data_registrazione
         end if
         if ls_flag_decimali = "N" and &
            ld_quantita - int(ld_quantita) > 0 then
            ld_quantita = ceiling(ld_quantita)
            this.setitem(i_rownbr, "quan_ordinata", ld_quantita)
         end if

         f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_consegna, ls_cod_tipo_det_acq)

         if ld_quantita < ld_quan_minima and ld_quan_minima > 0 then
            this.setitem(i_rownbr, "quan_ordinata", ld_quan_minima)
         end if
         if ld_quantita > ld_quan_massima and ld_quan_massima > 0 then
            this.setitem(i_rownbr, "quan_ordinata", ld_quan_massima)
         end if
         if ld_inc_ordine > 0 then
            this.setitem(i_rownbr, "quan_ordinata", ld_quan_minima)
         end if
         if datetime(relativedate(date(ldt_data_registrazione), ld_tempo_app)) > this.getitemdatetime(i_rownbr, "data_consegna") and ld_tempo_app > 0 and &
            not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
            g_mb.messagebox("Attenzione", "La data di consegna non rispetta il tempo di approvvigionamento: " + string(ld_tempo_app) + " giorni.", &
                        exclamation!, ok!)
         end if

         if not isnull(i_coltext) then
            cb_prodotti_note_ricerca.enabled = true
         else
            cb_prodotti_note_ricerca.enabled = false
         end if

         select tab_prod_fornitori.cod_prod_fornitore
         into   :ls_cod_prod_fornitore
         from   tab_prod_fornitori
         where  tab_prod_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                tab_prod_fornitori.cod_prodotto = :i_coltext and
                tab_prod_fornitori.cod_fornitore = :ls_cod_fornitore;

         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "cod_prod_fornitore", ls_cod_prod_fornitore)
         else
            this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
         end if

			if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_off_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
			else
				dw_det_off_acq_det_1.modify("st_fattore_conv.text=''")
			end if
			select  anag_prodotti_note.nota_prodotto  
			into    :ls_nota_prodotto  
			from    anag_prodotti_note  
			where ( anag_prodotti_note.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         		( anag_prodotti_note.cod_prodotto = :ls_cod_prodotto ) AND  
         		( anag_prodotti_note.cod_nota_prodotto = 
							 ( select parametri_azienda.stringa  
								from parametri_azienda  
								where (parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda ) AND  
								      (parametri_azienda.flag_parametro = 'S' ) AND  
                              (parametri_azienda.cod_parametro = 'CNP' )  )) ;
			if sqlca.sqlcode = 0 then
				dw_det_off_acq_det_2.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
				dw_det_off_acq_det_2.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if

		case "cod_misura"
			if not isnull(i_coltext) and i_coltext <> "" then
	   		cb_prezzo.text = "Prezzo al " + i_coltext
			else
				cb_prezzo.text = "Prezzo"
			end if

         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.cod_misura_mag
         into   :ls_cod_misura_mag
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> i_coltext and len(trim(i_coltext)) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_off_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + i_coltext + ")'")
			else
				dw_det_off_acq_det_1.modify("st_fattore_conv.text=''")
			end if
      case "quan_ordinata"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.flag_decimali,
                anag_prodotti.quan_minima,
                anag_prodotti.inc_ordine,
                anag_prodotti.quan_massima
         into   :ls_flag_decimali,
                :ld_quan_minima,
                :ld_inc_ordine,
                :ld_quan_massima
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
         if sqlca.sqlcode = 0 then
            if ls_flag_decimali = "N" and &
               (double(i_coltext) - int(double(i_coltext))) > 0 then
               i_coltext = string(ceiling(double(i_coltext)))
               this.setitem(i_rownbr, "quan_ordinata", double(i_coltext))
               this.settext(i_coltext)
               return 2
            end if
         end if

         ld_quantita = double(i_coltext)
         if not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
            ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")
         else
            ldt_data_consegna = dw_det_off_acq_lista.i_parentdw.getitemdatetime(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
         end if
         f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_consegna, ls_cod_tipo_det_acq)

         if double(i_coltext) < ld_quan_minima and ld_quan_minima > 0 then
            g_mb.messagebox("Attenzione", "Quantità Ordinata inferiore alla quantità minima ordinabile: " + string(ld_quan_minima), &
                        exclamation!, ok!)
         end if
         if double(i_coltext) > ld_quan_massima and ld_quan_massima > 0 then
            g_mb.messagebox("Attenzione", "Quantità Ordinata superiore alla quantità massima ordinabile: " + string(ld_quan_massima), &
                        exclamation!, ok!)
         end if
         if ld_inc_ordine > 0 then
            if mod(double(i_coltext),ld_inc_ordine) <> 0 then
               g_mb.messagebox("Attenzione", "Proporzione incremento ordine non rispettato: " + string(ld_inc_ordine), &
                           exclamation!, ok!)
            end if
         end if
      case "data_consegna"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         ld_quantita = this.getitemnumber(i_rownbr, "quan_ordinata")
         if not isnull(i_coltext) then
            ldt_data_consegna = datetime(date(i_coltext))
         else
            ldt_data_consegna = dw_det_off_acq_lista.i_parentdw.getitemdatetime(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
         end if
         f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_consegna, ls_cod_tipo_det_acq)

         select anag_prodotti.inc_ordine
         into   :ld_tempo_app
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
         if relativedate(date(ldt_data_registrazione), ld_tempo_app) > date(i_coltext) and ld_tempo_app > 0 then
            g_mb.messagebox("Attenzione", "La data di consegna non rispetta il tempo di approvvigionamento: " + string(ld_tempo_app) + " giorni.", &
                        exclamation!, ok!)
         end if
   end choose
end if
end event

event ue_key;call super::ue_key;choose case this.getcolumnname()
	case "cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
		guo_ricerca.uof_ricerca_prodotto(dw_det_off_acq_lista,"cod_prodotto")
		end if
	case "des_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_off_acq_lista,"cod_prodotto")		
		end if
	case "prezzo_acquisto"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_8 = getitemstring(getrow(),"cod_prodotto")
			s_cs_xx.parametri.parametro_s_9 = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.getrow(), "cod_fornitore")
			s_cs_xx.parametri.parametro_s_10 = "F"
			//-CLAUDIA 23/07/07 MIGLIORAMENTO PERCHè SI POSSA RICERCARE PER DESCRIZIONE
			s_cs_xx.parametri.parametro_s_12 = '%'+Left (getitemstring(getrow(),"des_prodotto"), 20)+'%'
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_uo_dw_1 = this
			if isvalid(w_det_ord_acq_storico) then
				w_det_ord_acq_storico.show()
			else
				window_open(w_det_ord_acq_storico, 0)
			end if
		end if
end choose

if key = keyF4! and keyflags = 1 then
	triggerevent("pcd_save")
	postevent("pcd_new")
end if

end event

type cb_blocca from commandbutton within w_det_off_acq
event clicked pbm_bnclicked
integer x = 2331
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 130
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Blocca"
end type

event clicked;long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_acq, ll_i, ll_blocco


if dw_det_off_acq_lista.getrow() > 0 then
	if dw_det_off_acq_lista.getitemstring(dw_det_off_acq_lista.getrow(), "flag_blocco") = "N" then
		ll_anno_registrazione = dw_det_off_acq_lista.getitemnumber(dw_det_off_acq_lista.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_det_off_acq_lista.getitemnumber(dw_det_off_acq_lista.getrow(), "num_registrazione")
		ll_prog_riga_off_acq = dw_det_off_acq_lista.getitemnumber(dw_det_off_acq_lista.getrow(), "prog_riga_off_acq")

		update det_off_acq
			set flag_blocco = 'S'
		 where det_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_off_acq.anno_registrazione = :ll_anno_registrazione and  
				 det_off_acq.num_registrazione = :ll_num_registrazione and
				 det_off_acq.prog_riga_off_acq = :ll_prog_riga_off_acq;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio offerta.", &
						  exclamation!, ok!)
			rollback;
			return
		end if

		dw_det_off_acq_det_1.setitem(dw_det_off_acq_det_1.getrow(), "flag_blocco", "S")
		dw_det_off_acq_det_1.setitemstatus(dw_det_off_acq_det_1.getrow(), "flag_blocco", primary!, notmodified!)

		ll_blocco = 0

		for ll_i = 1 to dw_det_off_acq_det_1.rowcount()
			if dw_det_off_acq_lista.getitemstring(ll_i, "flag_blocco") = "S" then
				ll_blocco ++
			end if
		next

		if ll_blocco = dw_det_off_acq_det_1.rowcount() then
			update tes_off_acq
			set flag_blocco = 'S'
			where tes_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
					tes_off_acq.anno_registrazione = :ll_anno_registrazione and  
					tes_off_acq.num_registrazione = :ll_num_registrazione;

			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata offerta.", &
							  exclamation!, ok!)
				dw_det_off_acq_lista.i_parentdw.setitem(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco", "N")
				dw_det_off_acq_lista.i_parentdw.setitemstatus(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco", primary!, notmodified!)
				rollback;
				return
			end if
			dw_det_off_acq_lista.i_parentdw.setitem(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco", "S")
			dw_det_off_acq_lista.i_parentdw.setitemstatus(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco", primary!, notmodified!)
		end if
		commit;
	end if
end if
end event

type cb_prezzo from commandbutton within w_det_off_acq
event clicked pbm_bnclicked
integer x = 2720
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Prezzo"
end type

event clicked;dw_det_off_acq_det_1.accepttext()

dw_det_off_acq_det_1.setcolumn(7)
s_cs_xx.parametri.parametro_s_1 = dw_det_off_acq_det_1.gettext()
dw_det_off_acq_det_1.setcolumn(10)
s_cs_xx.parametri.parametro_d_1 = double(dw_det_off_acq_det_1.gettext())
dw_det_off_acq_det_1.setcolumn(11)
s_cs_xx.parametri.parametro_d_2 = double(dw_det_off_acq_det_1.gettext())
dw_det_off_acq_det_1.setcolumn(9)
s_cs_xx.parametri.parametro_d_3 = double(dw_det_off_acq_det_1.gettext())

window_open(w_prezzo_um, 0)

if s_cs_xx.parametri.parametro_d_1 <> 0 then
   dw_det_off_acq_det_1.setitem(dw_det_off_acq_det_1.getrow(), "prezzo_acquisto", s_cs_xx.parametri.parametro_d_1)
end if

if s_cs_xx.parametri.parametro_d_3 <> 0 then
   dw_det_off_acq_det_1.setitem(dw_det_off_acq_det_1.getrow(), "quan_ordinata", s_cs_xx.parametri.parametro_d_3)
end if
end event

type cb_sblocca from commandbutton within w_det_off_acq
event clicked pbm_bnclicked
integer x = 1943
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sbl&occa"
end type

event clicked;long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_acq


if dw_det_off_acq_lista.getrow() > 0 then
	if dw_det_off_acq_lista.getitemstring(dw_det_off_acq_lista.getrow(), "flag_blocco") = "S" then
		ll_anno_registrazione = dw_det_off_acq_lista.getitemnumber(dw_det_off_acq_lista.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_det_off_acq_lista.getitemnumber(dw_det_off_acq_lista.getrow(), "num_registrazione")
		ll_prog_riga_off_acq = dw_det_off_acq_lista.getitemnumber(dw_det_off_acq_lista.getrow(), "prog_riga_off_acq")

		update det_off_acq
			set flag_blocco = 'N'
		 where det_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_off_acq.anno_registrazione = :ll_anno_registrazione and  
				 det_off_acq.num_registrazione = :ll_num_registrazione and
				 det_off_acq.prog_riga_off_acq = :ll_prog_riga_off_acq;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio offerta.", &
						  exclamation!, ok!)
			rollback;
			return
		end if

		update tes_off_acq
		set flag_blocco = 'N'
		where tes_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				tes_off_acq.anno_registrazione = :ll_anno_registrazione and  
				tes_off_acq.num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata offerta.", &
						  exclamation!, ok!)
			rollback;
			return
		end if

		dw_det_off_acq_det_1.setitem(dw_det_off_acq_det_1.getrow(), "flag_blocco", "N")
		dw_det_off_acq_det_1.setitemstatus(dw_det_off_acq_det_1.getrow(), "flag_blocco", primary!, notmodified!)
		dw_det_off_acq_lista.i_parentdw.setitem(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco", "N")
		dw_det_off_acq_lista.i_parentdw.setitemstatus(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco", primary!, notmodified!)
		commit;
	end if
end if
end event

type cb_sconti from commandbutton within w_det_off_acq
integer x = 3109
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Sconti"
end type

on clicked;s_cs_xx.parametri.parametro_uo_dw_1 = dw_det_off_acq_lista
window_open(w_sconti, 0)

end on

type pb_prod_view from cb_listview within w_det_off_acq
integer x = 2606
integer y = 952
integer width = 69
integer height = 80
integer taborder = 10
boolean bringtotop = true
boolean originalsize = false
end type

event getfocus;call super::getfocus;dw_det_off_acq_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end event

type cb_corrispondenze from commandbutton within w_det_off_acq
event clicked pbm_bnclicked
integer x = 1166
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corrispond."
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "Off_Acq"
window_open_parm(w_det_acq_ven_corr, -1, dw_det_off_acq_det_1)

end event

type cb_c_industriale from commandbutton within w_det_off_acq
integer x = 1554
integer y = 1540
integer width = 366
integer height = 80
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C.Industriale"
end type

event clicked;
window_open_parm(w_det_off_acq_stat,-1,dw_det_off_acq_lista)
end event

event getfocus;call super::getfocus;string ls_null


setnull(ls_null)
dw_det_off_acq_det_2.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "anno_commessa"
s_cs_xx.parametri.parametro_s_2 = "num_commessa"
s_cs_xx.parametri.parametro_s_10 = dw_det_off_acq_lista.getitemstring(dw_det_off_acq_lista.getrow(), "cod_prodotto")
end event

type cb_prodotti_note_ricerca from cb_prod_note_ricerca within w_det_off_acq
integer x = 146
integer y = 900
integer height = 80
integer taborder = 40
boolean bringtotop = true
end type

on getfocus;call cb_prod_note_ricerca::getfocus;s_cs_xx.parametri.parametro_s_1 = dw_det_off_acq_det_1.getitemstring(dw_det_off_acq_det_1.getrow(),"cod_prodotto")
dw_det_off_acq_det_2.change_dw_current()
end on

type dw_det_off_acq_det_2 from uo_cs_xx_dw within w_det_off_acq
integer x = 46
integer y = 792
integer width = 3451
integer height = 840
integer taborder = 70
string dataobject = "d_det_off_acq_det_2"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_commessa"
		guo_ricerca.uof_ricerca_commessa(dw_det_off_acq_det_2,"anno_commessa","num_commessa")
end choose
end event

type dw_det_off_acq_det_1 from uo_cs_xx_dw within w_det_off_acq
integer x = 46
integer y = 792
integer width = 3447
integer height = 772
integer taborder = 150
string dataobject = "d_det_off_acq_det_1"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_cod_iva_prodotto, ls_cod_iva_esente, &
          ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, &
          ls_flag_tipo_det_acq, ls_modify, ls_null, ls_cod_tipo_det_acq, ls_flag_decimali, &
          ls_cod_for_pot, ls_cod_prod_fornitore, ls_cod_misura_mag, ls_nota_prodotto, ls_des_prodotto, ls_flag_tipo_des_riga
   double ld_fat_conversione, ld_quantita, ld_cambio_acq, ld_quan_minima, ld_inc_ordine, &
          ld_quan_massima, ld_tempo_app
   datetime ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva
	long ll_riga_origine, ll_i

   setnull(ls_null)

   ls_cod_tipo_listino_prodotto = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
   ldt_data_registrazione = dw_det_off_acq_lista.i_parentdw.getitemdatetime(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
   ls_cod_fornitore = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")
   ls_cod_for_pot = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cod_for_pot")
   ls_cod_valuta = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
   ld_cambio_acq = dw_det_off_acq_lista.i_parentdw.getitemnumber(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cambio_acq")
   ls_cod_tipo_det_acq = this.getitemstring(this.getrow(),"cod_tipo_det_acq")
   
   choose case i_colname
		case "prog_riga_off_acq"
			ll_riga_origine = this.getrow()
			for ll_i = 1 to this.rowcount()
				if ll_i <> ll_riga_origine and &
					long(i_coltext) = this.getitemnumber(ll_i, "prog_riga_off_acq") then
	            g_mb.messagebox("Attenzione", "Progressivo riga già utilizzato.", &
   	                     exclamation!, ok!)
               i_coltext = string(this.getitemnumber(ll_riga_origine, "prog_riga_off_acq", primary!,true))
               this.setitem(ll_riga_origine, "prog_riga_off_acq", double(i_coltext))
               this.settext(i_coltext)
               return 2
					pcca.error = c_fatal
				end if
			next
      case "cod_tipo_det_acq"
         ls_modify = "cod_prodotto.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "cod_prodotto.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "cod_misura.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "cod_misura.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "fat_conversione.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "fat_conversione.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "quan_ordinata.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "quan_ordinata.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "prezzo_acquisto.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "prezzo_acquisto.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_1.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_1.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_2.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_2.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "cod_iva.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "cod_iva.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "data_consegna.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "data_consegna.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "cod_prod_fornitore.protect='0'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
         ls_modify = "cod_prod_fornitore.background.color='16777215'~t"
         dw_det_off_acq_det_1.modify(ls_modify)
   
         ld_datawindow = dw_det_off_acq_det_1
			lp_prod_view = pb_prod_view
			setnull(lc_prodotti_ricerca)
         f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext)
         if isnull(this.getitemstring(this.getrow(),"cod_prodotto")) then
            cb_prodotti_note_ricerca.enabled=false
         end if

         if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
              select tab_tipi_det_acq.cod_iva  
              into   :ls_cod_iva  
              from   tab_tipi_det_acq  
              where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
                     tab_tipi_det_acq.cod_tipo_det_acq = :i_coltext;
              if sqlca.sqlcode = 0 then
                 this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
              end if
         end if
			dw_det_off_acq_det_2.setitem(dw_det_off_acq_det_2.getrow(), "nota_dettaglio", ls_null)
      case "cod_prodotto"
			if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1

			setnull(ls_cod_iva_prodotto)
			setnull(ls_cod_iva_esente)
			setnull(ls_cod_iva)
         select des_prodotto,
			anag_prodotti.cod_misura_mag,
					 anag_prodotti.cod_misura_acq,
                anag_prodotti.fat_conversione_acq,
                anag_prodotti.flag_decimali,
                anag_prodotti.cod_iva,
                anag_prodotti.quan_minima,
                anag_prodotti.inc_ordine,
                anag_prodotti.quan_massima,
                anag_prodotti.tempo_app
         into   :ls_des_prodotto,
			:ls_cod_misura_mag,
					 :ls_cod_misura,
                :ld_fat_conversione,
                :ls_flag_decimali,
                :ls_cod_iva_prodotto,
                :ld_quan_minima,
                :ld_inc_ordine,
                :ld_quan_massima,
                :ld_tempo_app
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :i_coltext;
   
         if sqlca.sqlcode = 0 then
            this.setitem(i_rownbr, "cod_misura", ls_cod_misura)

				dw_det_off_acq_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
				dw_det_off_acq_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")
				
            if ld_fat_conversione <> 0 then
               this.setitem(i_rownbr, "fat_conversione", ld_fat_conversione)
            else
               this.setitem(i_rownbr, "fat_conversione", 1)
            end if
//            this.setitem(i_rownbr, "cod_iva", ls_cod_iva)

			select
				flag_tipo_des_riga
			into
				:ls_flag_tipo_des_riga
			from
				con_ord_acq
			where
				cod_azienda = :s_cs_xx.cod_azienda;
			
			if sqlca.sqlcode = 0 and ls_flag_tipo_des_riga  = "S" then
				setitem(i_rownbr,"des_prodotto",ls_des_prodotto)
			end if

			else
				dw_det_off_acq_det_1.modify("st_prezzo.text='Prezzo:'")
				dw_det_off_acq_det_1.modify("st_quantita.text='Quantità:'")
         end if

			if not isnull(this.getitemstring(i_rownbr, "cod_misura")) then
				cb_prezzo.text= "Prezzo al " + this.getitemstring(i_rownbr, "cod_misura")
			else
				cb_prezzo.text= "Prezzo"
			end if

         if not isnull(ls_cod_fornitore) then
            select anag_fornitori.cod_iva,
                   anag_fornitori.data_esenzione_iva
            into   :ls_cod_iva_esente,
                   :ldt_data_esenzione_iva
            from   anag_fornitori
            where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                   anag_fornitori.cod_fornitore = :ls_cod_fornitore;
         else
            select anag_for_pot.cod_iva,
                   anag_for_pot.data_esenzione_iva
            into   :ls_cod_iva_esente,
                   :ldt_data_esenzione_iva
            from   anag_for_pot
            where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
                   anag_for_pot.cod_for_pot = :ls_cod_for_pot;
         end if
         if sqlca.sqlcode = 0 then
            if not isnull(ls_cod_iva_esente) and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
               this.setitem(i_rownbr, "cod_iva", ls_cod_iva_esente)
				else																	// il fornitore è soggetto a iva
					if not isnull(ls_cod_iva_prodotto) then				// iva inserita in anagrafica del prodotto
	               this.setitem(i_rownbr, "cod_iva", ls_cod_iva_prodotto)
					else																// iva del tipo dettaglio prodotto
						ls_cod_tipo_det_acq = this.getitemstring(getrow(),"cod_tipo_det_acq")
						if not isnull(ls_cod_tipo_det_acq) then
							select cod_iva
							into   :ls_cod_iva
							from   tab_tipi_det_acq
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_tipo_det_acq = :ls_cod_tipo_det_acq;
						end if
	               this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
					end if
            end if
         end if

         ls_cod_prodotto = i_coltext
         ld_quantita = this.getitemnumber(i_rownbr, "quan_ordinata")
         if not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
            ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")
         else
            ldt_data_consegna = ldt_data_registrazione
         end if
         if ls_flag_decimali = "N" and &
            ld_quantita - int(ld_quantita) > 0 then
            ld_quantita = ceiling(ld_quantita)
            this.setitem(i_rownbr, "quan_ordinata", ld_quantita)
         end if
			
		//-------------------
		this.change_dw_current()

         f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_consegna, ls_cod_tipo_det_acq)

         if ld_quantita < ld_quan_minima and ld_quan_minima > 0 then
            this.setitem(i_rownbr, "quan_ordinata", ld_quan_minima)
         end if
         if ld_quantita > ld_quan_massima and ld_quan_massima > 0 then
            this.setitem(i_rownbr, "quan_ordinata", ld_quan_massima)
         end if
         if ld_inc_ordine > 0 then
            this.setitem(i_rownbr, "quan_ordinata", ld_quan_minima)
         end if
         if datetime(relativedate(date(ldt_data_registrazione), ld_tempo_app)) > this.getitemdatetime(i_rownbr, "data_consegna") and ld_tempo_app > 0 and &
            not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
            g_mb.messagebox("Attenzione", "La data di consegna non rispetta il tempo di approvvigionamento: " + string(ld_tempo_app) + " giorni.", &
                        exclamation!, ok!)
         end if

         if not isnull(i_coltext) then
            cb_prodotti_note_ricerca.enabled = true
         else
            cb_prodotti_note_ricerca.enabled = false
         end if

         select tab_prod_fornitori.cod_prod_fornitore
         into   :ls_cod_prod_fornitore
         from   tab_prod_fornitori
         where  tab_prod_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                tab_prod_fornitori.cod_prodotto = :i_coltext and
                tab_prod_fornitori.cod_fornitore = :ls_cod_fornitore;

         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "cod_prod_fornitore", ls_cod_prod_fornitore)
         else
            this.setitem(this.getrow(), "cod_prod_fornitore", ls_null)
         end if

			if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_off_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
			else
				dw_det_off_acq_det_1.modify("st_fattore_conv.text=''")
			end if
			select  anag_prodotti_note.nota_prodotto  
			into    :ls_nota_prodotto  
			from    anag_prodotti_note  
			where ( anag_prodotti_note.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         		( anag_prodotti_note.cod_prodotto = :ls_cod_prodotto ) AND  
         		( anag_prodotti_note.cod_nota_prodotto = 
							 ( select parametri_azienda.stringa  
								from parametri_azienda  
								where (parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda ) AND  
								      (parametri_azienda.flag_parametro = 'S' ) AND  
                              (parametri_azienda.cod_parametro = 'CNP' )  )) ;
			if sqlca.sqlcode = 0 then
				dw_det_off_acq_det_2.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
				dw_det_off_acq_det_2.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if

		case "cod_misura"
			if not isnull(i_coltext) and i_coltext <> "" then
	   		cb_prezzo.text = "Prezzo al " + i_coltext
			else
				cb_prezzo.text = "Prezzo"
			end if

         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.cod_misura_mag
         into   :ls_cod_misura_mag
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> i_coltext and len(trim(i_coltext)) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_off_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + i_coltext + ")'")
			else
				dw_det_off_acq_det_1.modify("st_fattore_conv.text=''")
			end if
		case "fat_conversione"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.cod_misura_mag
         into   :ls_cod_misura_mag
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> this.getitemstring(i_rownbr, "cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_off_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + i_coltext + " " + this.getitemstring(i_rownbr, "cod_misura") + ")'")
			else
				dw_det_off_acq_det_1.modify("st_fattore_conv.text=''")
			end if
      case "quan_ordinata"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.flag_decimali,
                anag_prodotti.quan_minima,
                anag_prodotti.inc_ordine,
                anag_prodotti.quan_massima
         into   :ls_flag_decimali,
                :ld_quan_minima,
                :ld_inc_ordine,
                :ld_quan_massima
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
         if sqlca.sqlcode = 0 then
            if ls_flag_decimali = "N" and &
               (double(i_coltext) - int(double(i_coltext))) > 0 then
               i_coltext = string(ceiling(double(i_coltext)))
               this.setitem(i_rownbr, "quan_ordinata", double(i_coltext))
               this.settext(i_coltext)
               return 2
            end if
         end if

         ld_quantita = double(i_coltext)
         if not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
            ldt_data_consegna = this.getitemdatetime(i_rownbr, "data_consegna")
         else
            ldt_data_consegna = dw_det_off_acq_lista.i_parentdw.getitemdatetime(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
         end if
			
		//-------------------
		this.change_dw_current()
			
         f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_consegna, ls_cod_tipo_det_acq)

         if double(i_coltext) < ld_quan_minima and ld_quan_minima > 0 then
            g_mb.messagebox("Attenzione", "Quantità Ordinata inferiore alla quantità minima ordinabile: " + string(ld_quan_minima), &
                        exclamation!, ok!)
         end if
         if double(i_coltext) > ld_quan_massima and ld_quan_massima > 0 then
            g_mb.messagebox("Attenzione", "Quantità Ordinata superiore alla quantità massima ordinabile: " + string(ld_quan_massima), &
                        exclamation!, ok!)
         end if
         if ld_inc_ordine > 0 then
            if mod(double(i_coltext),ld_inc_ordine) <> 0 then
               g_mb.messagebox("Attenzione", "Proporzione incremento ordine non rispettato: " + string(ld_inc_ordine), &
                           exclamation!, ok!)
            end if
         end if
      case "data_consegna"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         ld_quantita = this.getitemnumber(i_rownbr, "quan_ordinata")
         if not isnull(i_coltext) then
            ldt_data_consegna = datetime(date(i_coltext))
         else
            ldt_data_consegna = dw_det_off_acq_lista.i_parentdw.getitemdatetime(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
         end if
			
		//-------------------
		this.change_dw_current()
			
         f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_consegna, ls_cod_tipo_det_acq)

         select anag_prodotti.inc_ordine
         into   :ld_tempo_app
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
         if relativedate(date(ldt_data_registrazione), ld_tempo_app) > date(i_coltext) and ld_tempo_app > 0 then
            g_mb.messagebox("Attenzione", "La data di consegna non rispetta il tempo di approvvigionamento: " + string(ld_tempo_app) + " giorni.", &
                        exclamation!, ok!)
         end if
   end choose
end if
end event

event pcd_validaterow;call uo_cs_xx_dw::pcd_validaterow;string ls_modify, ls_flag_tipo_det_acq, ls_cod_tipo_det_acq, &
       ls_null, ls_match, ls_match_1


setnull(ls_null)
if i_rownbr > 0 then
   ls_cod_tipo_det_acq = this.getitemstring(i_rownbr,"cod_tipo_det_acq")

   select tab_tipi_det_acq.flag_tipo_det_acq
   into   :ls_flag_tipo_det_acq
   from   tab_tipi_det_acq
   where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Acquisto.", &
                 exclamation!, ok!)
      return
   end if

   if i_insave > 0 then
      if ls_flag_tipo_det_acq = "M" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("cod_misura")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Unità di misura obbligatoria.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("quan_ordinata")
         if integer(get_col_data(i_rownbr, i_colnbr)) = 0 then
          	g_mb.messagebox("Attenzione", "Quantità ordinata obbligatoria.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      elseif ls_flag_tipo_det_acq = "C" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      end if
   end if
end if
end event

event ue_key;call super::ue_key;string ls_cod_valuta, ls_cod_misura
long   ll_cont
dec{4} ld_prezzo_acquisto, ld_fat_conversione
double ld_sconto_tot, ld_sconti[]
datetime ldt_data_consegna
uo_condizioni_cliente luo_condizioni_cliente

choose case this.getcolumnname()
	case "cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_off_acq_det_1,"cod_prodotto")	
		end if
	case "des_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_off_acq_det_1,"cod_prodotto")	
		end if
	case "cod_prod_fornitore"
		string ls_cod_prodotto, ls_cod_prod_fornitore, ls_des_prod_fornitore, ls_cod_fornitore, ls_des_prod_old, ls_null
		
		if key = keyF2!  and keyflags = 1 then
			ls_cod_prodotto       = this.getitemstring(this.getrow(),"cod_prodotto")
			ls_cod_prod_fornitore = this.gettext()
			ls_des_prod_fornitore = this.getitemstring(this.getrow(),"des_prodotto")
			ls_cod_fornitore      = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")
			if isnull(ls_cod_prodotto) or isnull(ls_cod_prod_fornitore) then
				g_mb.messagebox("APICE","Inserire un codice prodotto fornitore e un codice prodotto di magazzino: impossibile memorizzare!", Information!)
				return
			end if
			setnull(ls_null)
			select des_prod_fornitore
			into   :ls_des_prod_old
			from   tab_prod_fornitori
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_fornitore = :ls_cod_fornitore and
					 cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode = 0 then
				if g_mb.messagebox("APICE","Per questo prodotto di magazzino esiste già un codice prodotto del fornitore: lo aggiorno?", Question!, YesNo!,1) = 1 then
					if isnull(ls_des_prod_fornitore) or len(ls_des_prod_fornitore) = 0 then	ls_des_prod_fornitore = ls_des_prod_old
					update tab_prod_fornitori
					set    cod_prod_fornitore = :ls_cod_prod_fornitore, des_prod_fornitore = :ls_des_prod_fornitore
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_fornitore = :ls_cod_fornitore and
							 cod_prodotto = :ls_cod_prodotto;
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("APICE","Errore in fase di aggiornamento codice prodotto fornitore in tabella prodotti fornitori. Dettaglio " + sqlca.sqlerrtext)
						return;
					end if
					this.setitem(this.getrow(),"des_prodotto", ls_null)
				end if
			else
				if g_mb.messagebox("APICE","Inserisco un codice prodotto del fornitore?", Question!, YesNo!,1) = 1 then
					insert into tab_prod_fornitori
								(cod_azienda,
								cod_fornitore,
								cod_prodotto,
								cod_prod_fornitore,
								des_prod_fornitore)
					values  (:s_cs_xx.cod_azienda,
					         :ls_cod_fornitore,
							   :ls_cod_prodotto,
							   :ls_cod_prod_fornitore,
							   :ls_des_prod_fornitore);
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("APICE","Errore in fase di aggiornamento codice prodotto fornitore in tabella prodotti fornitori. Dettaglio " + sqlca.sqlerrtext)
						return;
					end if
					this.setitem(this.getrow(),"des_prodotto", ls_null)
				end if
			end if				
		end if
		
	case "prezzo_acquisto"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_8 = getitemstring(getrow(),"cod_prodotto")
			s_cs_xx.parametri.parametro_s_9 = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.getrow(), "cod_fornitore")
			s_cs_xx.parametri.parametro_s_10 = "F"
			//-CLAUDIA 23/07/07 MIGLIORAMENTO PERCHè SI POSSA RICERCARE PER DESCRIZIONE
			s_cs_xx.parametri.parametro_s_12 = '%'+Left (getitemstring(getrow(),"des_prodotto"), 20)+'%'
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_uo_dw_1 = this
			if isvalid(w_det_ord_acq_storico) then
				w_det_ord_acq_storico.show()
			else
				window_open(w_det_ord_acq_storico, 0)
			end if
		end if
		if key = keyF2!  and keyflags = 1 then
			if g_mb.messagebox("APICE","Memorizzo il prezzo di acquisto nei listini fornitori?", Question!, YesNo!, 1) = 1 then
				ls_cod_prodotto       = this.getitemstring(this.getrow(),"cod_prodotto")
				ls_cod_fornitore      = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")
				ls_cod_valuta         = dw_det_off_acq_lista.i_parentdw.getitemstring(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
				ls_cod_misura         = this.getitemstring(this.getrow(),"cod_misura")
				ld_fat_conversione    = this.getitemnumber(this.getrow(),"fat_conversione")
				if not isnull(this.getitemdatetime(i_rownbr, "data_consegna")) then
					ldt_data_consegna = this.getitemdatetime(this.getrow(), "data_consegna")
				else
					ldt_data_consegna = dw_det_off_acq_lista.i_parentdw.getitemdatetime(dw_det_off_acq_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
				end if
				ld_prezzo_acquisto = double(this.gettext())
				ld_sconti[1] = this.getitemnumber(this.getrow(),"sconto_1")
				ld_sconti[2] = this.getitemnumber(this.getrow(),"sconto_2")
				ld_sconti[3] = this.getitemnumber(this.getrow(),"sconto_3")
				ld_sconti[4] = this.getitemnumber(this.getrow(),"sconto_4")
				ld_sconti[5] = this.getitemnumber(this.getrow(),"sconto_5")
				ld_sconti[6] = this.getitemnumber(this.getrow(),"sconto_6")
				ld_sconti[7] = this.getitemnumber(this.getrow(),"sconto_7")
				ld_sconti[8] = this.getitemnumber(this.getrow(),"sconto_8")
				ld_sconti[9] = this.getitemnumber(this.getrow(),"sconto_9")
				ld_sconti[10] = this.getitemnumber(this.getrow(),"sconto_10")
				luo_condizioni_cliente = CREATE uo_condizioni_cliente 
				luo_condizioni_cliente.uof_calcola_sconto_tot(ld_sconti[], ld_sconto_tot)
				destroy luo_condizioni_cliente
				
				select count(*) 
				into   :ll_cont
				from   listini_fornitori
				where  cod_azienda   = :s_cs_xx.cod_azienda and
						 cod_prodotto  = :ls_cod_prodotto and
						 cod_fornitore = :ls_cod_fornitore and
						 cod_valuta    = :ls_cod_valuta and
						 data_inizio_val = :ldt_data_consegna;
				if ll_cont = 0 then		// nessun record allora inserisco
					INSERT INTO listini_fornitori  
							( cod_azienda,   
							  cod_prodotto,   
							  cod_fornitore,   
							  cod_valuta,   
							  data_inizio_val,   
							  des_listino_for,   
							  quantita_1,   
							  prezzo_1,   
							  sconto_1,   
							  quantita_2,   
							  prezzo_2,   
							  sconto_2,   
							  quantita_3,   
							  prezzo_3,   
							  sconto_3,   
							  quantita_4,   
							  prezzo_4,   
							  sconto_4,   
							  quantita_5,   
							  prezzo_5,   
							  sconto_5,   
							  flag_for_pref,   
							  prezzo_ult_acquisto,   
							  data_ult_acquisto,   
							  cod_misura,   
							  fat_conversione )  
					VALUES ( :s_cs_xx.cod_azienda,   
							  :ls_cod_prodotto,   
							  :ls_cod_fornitore,   
							  :ls_cod_valuta,   
							  :ldt_data_consegna,   
							  null,   
							  99999999,   
							  :ld_prezzo_acquisto,   
							  :ld_sconto_tot,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  'N',   
							  0,   
							  null,   
							  :ls_cod_misura,   
							  :ld_fat_conversione)  ;
						if sqlca.sqlcode <> 0 then
							g_mb.messagebox("APICE","Errore in inserimento condizione fornitore-prodotto. Dettaglio: " + sqlca.sqlerrtext)
							return
						end if
					else
					if g_mb.messagebox("APICE","Aggiorno la condizione del fornitore?",Question!,YesNo!,1) = 1 then
						update listini_fornitori
						set    prezzo_1 = :ld_prezzo_acquisto, 
								 sconto_1 = :ld_sconto_tot
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto and
								 cod_fornitore = :ls_cod_fornitore and
								 cod_valuta = :ls_cod_valuta and
								 data_inizio_val = :ldt_data_consegna;
						if sqlca.sqlcode <> 0 then
							g_mb.messagebox("APICE","Errore in aggiornamento condizione fornitore-prodotto. Dettaglio: " + sqlca.sqlerrtext)
							return
						end if
					end if
				end if			
			end if
		end if
end choose

end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	dw_det_off_acq_lista.setrow(getrow())
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_det_off_acq_det_1,"cod_prodotto")
end choose
end event

type dw_folder from u_folder within w_det_off_acq
integer x = 23
integer y = 692
integer width = 3497
integer height = 1080
integer taborder = 60
end type

