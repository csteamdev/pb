﻿$PBExportHeader$w_tes_gen_ord_acq.srw
$PBExportComments$Finestra Testata Generazione Ordini Acquisto
forward
global type w_tes_gen_ord_acq from w_cs_xx_principale
end type
type cb_genera from uo_cb_ok within w_tes_gen_ord_acq
end type
type cb_1 from uo_cb_close within w_tes_gen_ord_acq
end type
type cb_dettaglio from commandbutton within w_tes_gen_ord_acq
end type
type dw_tes_gen_ord_acq from uo_cs_xx_dw within w_tes_gen_ord_acq
end type
type dw_folder from u_folder within w_tes_gen_ord_acq
end type
type dw_gen_orig_dest from uo_cs_xx_dw within w_tes_gen_ord_acq
end type
end forward

global type w_tes_gen_ord_acq from w_cs_xx_principale
integer width = 3301
integer height = 1368
string title = "Generazione Ordini Acquisto"
cb_genera cb_genera
cb_1 cb_1
cb_dettaglio cb_dettaglio
dw_tes_gen_ord_acq dw_tes_gen_ord_acq
dw_folder dw_folder
dw_gen_orig_dest dw_gen_orig_dest
end type
global w_tes_gen_ord_acq w_tes_gen_ord_acq

type variables

end variables

forward prototypes
public function integer wf_calcola_documento (long al_anno_registrazione, long al_num_registrazione)
end prototypes

public function integer wf_calcola_documento (long al_anno_registrazione, long al_num_registrazione);string ls_messaggio
uo_calcola_documento_euro luo_doc

if not isnull(al_anno_registrazione) and al_anno_registrazione <> 0 and not isnull(al_num_registrazione) and al_num_registrazione <> 0 then

	luo_doc = create uo_calcola_documento_euro

	if luo_doc.uof_calcola_documento(al_anno_registrazione, al_num_registrazione ,"ord_acq", ls_messaggio) <> 0 then
		g_mb.error(ls_messaggio)
		destroy luo_doc
		return -1
	end if

	destroy luo_doc	
end if	

return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]


set_w_options(c_noenablepopup)

lw_oggetti[1] = dw_tes_gen_ord_acq
dw_folder.fu_assigntab(1, "Offerte", lw_oggetti[])
lw_oggetti[1] = dw_gen_orig_dest
dw_folder.fu_assigntab(2, "Ordini Generati", lw_oggetti[])
dw_folder.fu_foldercreate(2, 4)
dw_folder.fu_selecttab(1)

dw_tes_gen_ord_acq.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_multiselect + &
						   			    c_nonew + &
							   		    c_nomodify + &
											 c_nodelete + &
											 c_disablecc + &
											 c_disableccinsert, &
                                  c_default)

dw_gen_orig_dest.set_dw_options(sqlca, &
                             	  pcca.null_object, &
										  c_nonew + &
										  c_nomodify + &
										  c_nodelete + &
										  c_disablecc + &
										  c_disableccinsert, &
	                             c_viewmodeblack)
													 
save_on_close(c_socnosave)

end event

on w_tes_gen_ord_acq.create
int iCurrent
call super::create
this.cb_genera=create cb_genera
this.cb_1=create cb_1
this.cb_dettaglio=create cb_dettaglio
this.dw_tes_gen_ord_acq=create dw_tes_gen_ord_acq
this.dw_folder=create dw_folder
this.dw_gen_orig_dest=create dw_gen_orig_dest
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_genera
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_dettaglio
this.Control[iCurrent+4]=this.dw_tes_gen_ord_acq
this.Control[iCurrent+5]=this.dw_folder
this.Control[iCurrent+6]=this.dw_gen_orig_dest
end on

on w_tes_gen_ord_acq.destroy
call super::destroy
destroy(this.cb_genera)
destroy(this.cb_1)
destroy(this.cb_dettaglio)
destroy(this.dw_tes_gen_ord_acq)
destroy(this.dw_folder)
destroy(this.dw_gen_orig_dest)
end on

type cb_genera from uo_cb_ok within w_tes_gen_ord_acq
event clicked pbm_bnclicked
integer x = 2491
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera"
end type

event clicked;call super::clicked;long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_selected[], ll_anno_offerta, ll_num_offerta, ll_prog_riga_off_acq, &
     ll_anno_commessa, ll_num_commessa, ll_num_reg_ord, ll_anno_reg_rda, ll_num_reg_rda, ll_prog_riga_rda
string ls_cod_tipo_ord_acq, ls_cod_tipo_off_acq, ls_nota_testata, ls_cod_fornitore, &
		 ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, &
		 ls_frazione, ls_cap, ls_provincia, ls_cod_fil_fornitore, ls_cod_des_fornitore, &
		 ls_cod_deposito, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
		 ls_cod_banca_clien_for, ls_cod_imballo, ls_cod_vettore, &
		 ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_nota_piede, ls_cod_banca, &
		 ls_cod_tipo_det_acq, ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, &
		 ls_cod_prod_fornitore, ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_acq, &
		 ls_lire, ls_flag_sola_iva, ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, &
		 ls_prog_riga_doc_des, ls_cod_tipo_analisi_off, ls_cod_tipo_analisi_ord, &
		 ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det,ls_cod_prodotto_raggruppato, &
		 ls_cod_tipo_lista_dist, ls_cod_lista_dist, ls_flag_confermato
datetime ldt_data_registrazione, ldt_data_consegna, ldt_data_consegna_det
dec{4} ld_cambio_acq, ld_sconto, ld_tot_val_offerta, ld_quan_ordinata, ld_prezzo_acquisto, &
		 ld_fat_conversione, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_val_riga, ld_sconto_4, &
		 ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		 ld_quan_evasa, ld_sconto_testata, ld_sconto_pagamento, ld_val_sconto_1, &
		 ld_val_riga_sconto_1, ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, &
		 ld_val_riga_sconto_3, ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, &
		 ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, &
		 ld_val_riga_sconto_7, ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, &
		 ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10, &
		 ld_val_sconto_testata, ld_val_riga_sconto_testata, ld_val_sconto_pagamento, &
		 ld_val_riga_netto, ld_tot_val_documento, ld_quan_raggruppo

string  ls_cod_operatore_new, ls_messaggio


setpointer(hourglass!)

select parametri_azienda.numero
into   :ll_anno_registrazione
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
		 parametri_azienda.flag_parametro = 'N' and &
		 parametri_azienda.cod_parametro = 'ESC';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Errore durante la lettura del parametro ESC.~n" + sqlca.sqlerrtext, Exclamation!, ok!)
	return
end if

select parametri_azienda.stringa
into   :ls_lire
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
   g_mb.messagebox("Attenzione", "Configurare il codice valuta per le Lire Italiane.~r~n" + sqlca.sqlerrtext, exclamation!, ok!)
   return
end if

dw_tes_gen_ord_acq.get_selected_rows(ll_selected[])
for ll_i = 1 to upperbound(ll_selected)
	select num_registrazione
	into   :ll_num_reg_ord
	from   con_ord_acq
	where  cod_azienda = :s_cs_xx.cod_azienda;
	if isnull(ll_num_reg_ord) then ll_num_reg_ord = 0
	
	select max(num_registrazione)
	into   :ll_num_registrazione
	from   tes_ord_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione;
	if isnull(ll_num_registrazione) then ll_num_registrazione = 0
	if ll_num_reg_ord > ll_num_registrazione then
		ll_num_registrazione = ll_num_reg_ord + 1
	else
		ll_num_registrazione ++
	end if
	
	update con_ord_acq
	set    num_registrazione = :ll_num_registrazione
	where  cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Errore durante la scrittura sulla tabella Controllo Ordini Acquisto.~n" + sqlca.sqlerrtext, Exclamation!, ok!)
		rollback;
		return
	end if
	
	ls_cod_tipo_off_acq = dw_tes_gen_ord_acq.getitemstring(ll_selected[ll_i], "cod_tipo_off_acq")
	
	select tab_tipi_off_acq.cod_tipo_ord_acq,  
			 tab_tipi_off_acq.cod_tipo_analisi  
	into   :ls_cod_tipo_ord_acq,  
			 :ls_cod_tipo_analisi_off
	from   tab_tipi_off_acq  
	where  tab_tipi_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_off_acq.cod_tipo_off_acq = :ls_cod_tipo_off_acq;
	
	if isnull(ls_cod_tipo_ord_acq) then
		g_mb.messagebox("Attenzione", "Per questo tipo offerta non è specificato il relativo tipo ordine.", &
					  Exclamation!, ok!)
		rollback;
		return
	end if

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Errore durante la lettura della tabella Tipi Offerte Acquisto.~n" + sqlca.sqlerrtext, Exclamation!, ok!)
		rollback;
		return
	end if
	
	ll_anno_offerta = dw_tes_gen_ord_acq.getitemnumber(ll_selected[ll_i], "anno_registrazione")
	ll_num_offerta = dw_tes_gen_ord_acq.getitemnumber(ll_selected[ll_i], "num_registrazione")

	select tes_off_acq.cod_fornitore,   
			 tes_off_acq.cod_operatore,   
			 tes_off_acq.rag_soc_1,   
			 tes_off_acq.rag_soc_2,   
			 tes_off_acq.indirizzo,   
			 tes_off_acq.localita,   
			 tes_off_acq.frazione,   
			 tes_off_acq.cap,   
			 tes_off_acq.provincia,   
			 tes_off_acq.cod_fil_fornitore,   
			 tes_off_acq.cod_des_fornitore,   
			 tes_off_acq.cod_deposito,   
			 tes_off_acq.cod_valuta,   
			 tes_off_acq.cambio_acq,   
			 tes_off_acq.cod_tipo_listino_prodotto,   
			 tes_off_acq.cod_pagamento,   
			 tes_off_acq.sconto,   
			 tes_off_acq.cod_banca_clien_for,   
			 tes_off_acq.cod_imballo,   
			 tes_off_acq.cod_vettore,   
			 tes_off_acq.cod_inoltro,   
			 tes_off_acq.cod_mezzo,   
			 tes_off_acq.cod_porto,   
			 tes_off_acq.cod_resa,   
			 tes_off_acq.nota_testata,   
			 tes_off_acq.nota_piede,   
			 tes_off_acq.data_consegna,   
			 tes_off_acq.cod_banca,
			 tes_off_acq.flag_doc_suc_tes, 
			 tes_off_acq.flag_doc_suc_pie			 			 
	 into  :ls_cod_fornitore,   
		 	 :ls_cod_operatore,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_fil_fornitore,   
			 :ls_cod_des_fornitore,   
			 :ls_cod_deposito,   
			 :ls_cod_valuta,   
			 :ld_cambio_acq,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_banca_clien_for,   
			 :ls_cod_imballo,   
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_cod_porto,   
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ldt_data_consegna,   
			 :ls_cod_banca,
			 :ls_flag_doc_suc_tes, 
			 :ls_flag_doc_suc_pie			 			 
	from   tes_off_acq  
	where  tes_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_off_acq.anno_registrazione = :ll_anno_offerta and  
			 tes_off_acq.num_registrazione = :ll_num_offerta;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Errore durante la lettura Testata Offerte Acquisto.~n" + sqlca.sqlerrtext, Exclamation!, ok!)
		rollback;
		return
	end if

	ldt_data_registrazione = datetime(today())

	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if
//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_off_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_off_acq = :ls_cod_tipo_off_acq;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_off_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_off_acq = :ls_cod_tipo_off_acq;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				
//--------------------------------------- Fine Modifica --------------------------------------------------------
	ls_nota_testata = "Ordine relativo alla richiesta di offerta N. " + string(ll_anno_offerta) + "/" + &
							string(ll_num_offerta) + "~r~n" + ls_nota_testata
							
							
	// stefanop 05/08/2010: ENG2K
	// Controllo il tipo ordine di acquisto se ha la gestione dei cicli di vita
	// Se si allora flag_confermato = N altrimenti flag_confermato = S
	select cod_tipo_lista_dist, cod_lista_dist
	into :ls_cod_tipo_lista_dist, :ls_cod_lista_dist
	from tab_tipi_ord_acq
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
	
	if isnull(ls_cod_tipo_lista_dist) or ls_cod_tipo_lista_dist = "" or isnull(ls_cod_lista_dist) or ls_cod_lista_dist = "" then
		ls_flag_confermato = "S"
	else
		ls_flag_confermato = "N"
	end if
	
	//prima di fare la insert metti l'operatore collegato per il nuovo documento
	if f_get_operatore_doc(ls_cod_operatore_new, ls_messaggio)>0 then
		ls_cod_operatore = ls_cod_operatore_new
	end if
	
	insert into tes_ord_acq
              (cod_azienda,   
               anno_registrazione,   
               num_registrazione,   
               cod_tipo_ord_acq,   
               data_registrazione,   
               cod_operatore,   
               cod_fornitore,   
               rag_soc_1,   
               rag_soc_2,   
               indirizzo,   
               localita,   
               frazione,   
               cap,   
               provincia,   
               cod_fil_fornitore,   
               cod_des_fornitore,   
               cod_deposito,   
               cod_valuta,   
               cambio_acq,   
               cod_tipo_listino_prodotto,   
               cod_pagamento,   
               sconto,   
               cod_banca_clien_for,   
               num_ord_fornitore,   
               data_ord_fornitore,   
               cod_imballo,   
               cod_vettore,   
               cod_inoltro,   
               cod_mezzo,   
               cod_porto,   
               cod_resa,   
               flag_blocco,   
               flag_evasione,   
               tot_val_ordine,   
               tot_val_evaso,   
               nota_testata,   
               nota_piede,   
               data_consegna,   
               cod_banca,
					flag_doc_suc_tes,
					flag_st_note_tes,					
					flag_doc_suc_pie,
					flag_st_note_pie,
			flag_confermato) // stefanop 05/08/2010					
  values      (:s_cs_xx.cod_azienda,   
           		:ll_anno_registrazione,   
      	      :ll_num_registrazione,   
		         :ls_cod_tipo_ord_acq,   
           		:ldt_data_registrazione,   
					:ls_cod_operatore,   
					:ls_cod_fornitore,   
					:ls_rag_soc_1,   
					:ls_rag_soc_2,   
					:ls_indirizzo,   
					:ls_localita,   
					:ls_frazione,   
					:ls_cap,   
					:ls_provincia,   
					:ls_cod_fil_fornitore,   
					:ls_cod_des_fornitore,   
					:ls_cod_deposito,   
					:ls_cod_valuta,   
					:ld_cambio_acq,   
					:ls_cod_tipo_listino_prodotto,   
					:ls_cod_pagamento,   
					:ld_sconto_testata,   
					:ls_cod_banca_clien_for,   
					null,   
					null,   
					:ls_cod_imballo,   
					:ls_cod_vettore,   
					:ls_cod_inoltro,   
					:ls_cod_mezzo,   
					:ls_cod_porto,   
					:ls_cod_resa,   
					'N',   
					'A',   
					0,   
					0,   
					:ls_nota_testata,   
					:ls_nota_piede,   
					:ldt_data_consegna,   
					:ls_cod_banca,
					'I',
					'I',
					'I',
					'I',
					:ls_flag_confermato); // stefanop 05/08/2010					

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Errore durante la scrittura Testata Ordine Acquisto.~n" + sqlca.sqlerrtext, stopsign!, ok!)
		rollback;
		return
	end if

   select tab_pagamenti.sconto
   into   :ld_sconto_pagamento
   from   tab_pagamenti
   where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

   if sqlca.sqlcode <> 0 then
      ld_sconto_pagamento = 0
   end if

	ld_tot_val_documento = 0

	if isnull(ld_sconto_testata) then ld_sconto_testata = 0

	declare cu_det_off_acq cursor for  
	select 	det_off_acq.prog_riga_off_acq,   
          	det_off_acq.cod_tipo_det_acq,   
          	det_off_acq.cod_prodotto,   
          	det_off_acq.cod_misura,   
          	det_off_acq.des_prodotto,   
          	det_off_acq.quan_ordinata,   
          	det_off_acq.prezzo_acquisto,   
          	det_off_acq.fat_conversione,   
          	det_off_acq.sconto_1,   
          	det_off_acq.sconto_2,   
          	det_off_acq.sconto_3,   
          	det_off_acq.cod_iva,   
          	det_off_acq.data_consegna,   
          	det_off_acq.cod_prod_fornitore,   
          	det_off_acq.nota_dettaglio,   
          	det_off_acq.anno_commessa,   
          	det_off_acq.num_commessa,   
          	det_off_acq.cod_centro_costo,   
          	det_off_acq.sconto_4,   
          	det_off_acq.sconto_5,   
          	det_off_acq.sconto_6,   
          	det_off_acq.sconto_7,   
          	det_off_acq.sconto_8,   
          	det_off_acq.sconto_9,   
          	det_off_acq.sconto_10,
			 	det_off_acq.quan_evasa,
				det_off_acq.flag_doc_suc_det,
				det_off_acq.anno_reg_rda,
				det_off_acq.num_reg_rda,
				det_off_acq.prog_riga_rda
     from 	det_off_acq  
    where 	det_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
          	det_off_acq.anno_registrazione = :ll_anno_offerta and  
          	det_off_acq.num_registrazione = :ll_num_offerta and
			 	det_off_acq.flag_blocco = 'N' and
			 	det_off_acq.flag_evasione <> 'E'   
	order by det_off_acq.prog_riga_off_acq asc;

	open cu_det_off_acq;
	
	do while 0 = 0
		fetch cu_det_off_acq into :ll_prog_riga_off_acq, :ls_cod_tipo_det_acq, &
				:ls_cod_prodotto, :ls_cod_misura, :ls_des_prodotto, :ld_quan_ordinata, &
				:ld_prezzo_acquisto, :ld_fat_conversione, :ld_sconto_1, :ld_sconto_2, &
				:ld_sconto_3, :ls_cod_iva, :ldt_data_consegna_det, :ls_cod_prod_fornitore, &
				:ls_nota_dettaglio, :ll_anno_commessa, :ll_num_commessa, :ls_cod_centro_costo, &
				:ld_sconto_4, :ld_sconto_5, :ld_sconto_6, :ld_sconto_7, :ld_sconto_8, &
				:ld_sconto_9, :ld_sconto_10, :ld_quan_evasa, :ls_flag_doc_suc_det, :ll_anno_reg_rda, &
				:ll_num_reg_rda, :ll_prog_riga_rda;

	   if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Errore durante la lettura Dettagli Offerte Acquisto.~n" + sqlca.sqlerrtext, Exclamation!, ok!)
			rollback;
			close cu_det_off_acq;
			return
		end if

		select tab_tipi_det_acq.flag_tipo_det_acq,
				 tab_tipi_det_acq.flag_sola_iva
		into   :ls_flag_tipo_det_acq,
				 :ls_flag_sola_iva
		from   tab_tipi_det_acq
		where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettagli.~r~n" + sqlca.sqlerrtext,exclamation!, ok!)
			close cu_det_off_acq;
			return
		end if

		if ls_flag_sola_iva = 'N' then
			ld_val_riga = (ld_quan_ordinata - ld_quan_evasa) * ld_prezzo_acquisto
			ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
			ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
			ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
			ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
			ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
			ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
			ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
			ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
			ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
			ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
			ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
			ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
			ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
			ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
			ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
			ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
			ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
			ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
			ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
			if ls_cod_valuta = ls_lire then
				ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
			else
				ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
			end if   
	
			if ls_flag_tipo_det_acq = "M" or &
				ls_flag_tipo_det_acq = "C" or &
				ls_flag_tipo_det_acq = "N" then
				ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
				ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
				ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
				ld_val_riga_netto = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
			elseif ls_flag_tipo_det_acq = "S" then
				ld_val_riga_netto = ld_val_riga_sconto_10 * -1
			else
				ld_val_riga_netto = ld_val_riga_sconto_10
			end if
	
			ld_tot_val_documento = ld_tot_val_documento + ld_val_riga_netto
	
			if ls_cod_valuta = ls_lire then
				ld_tot_val_documento = round(ld_tot_val_documento, 0)
			end if   
		else
			ld_val_riga_sconto_10 = 0
		end if
		
//------------------------------------------------- Modifica Nicola ---------------------------------------------
		if ls_flag_doc_suc_det = 'I' then //nota dettaglio
			select flag_doc_suc
			  into :ls_flag_doc_suc_det
			  from tab_tipi_det_acq
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_det_acq = :ls_cod_tipo_det_acq;
		end if		
		
		
		if ls_flag_doc_suc_det = 'N' then
			setnull(ls_nota_dettaglio)
		end if				
//-------------------------------------------------- Fine Modifica ----------------------------------------------
 	   insert into det_ord_acq  
				      (cod_azienda,   
				  		 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_ordine_acq,   
						 cod_tipo_det_acq,   
						 cod_prodotto,   
						 cod_misura,   
						 des_prodotto,   
						 quan_ordinata,   
						 prezzo_acquisto,   
						 fat_conversione,   
						 sconto_1,   
						 sconto_2,   
						 sconto_3,   
						 cod_iva,   
						 data_consegna,   
						 cod_prod_fornitore,   
						 quan_arrivata,   
						 val_riga,   
						 flag_saldo,   
						 flag_blocco,   
						 nota_dettaglio,   
						 anno_commessa,   
						 num_commessa,   
						 cod_centro_costo,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,   
						 anno_off_acq,   
						 num_off_acq,   
						 prog_riga_off_acq,
						 flag_doc_suc_det,
						 flag_st_note_det,
						 anno_reg_rda,
						 num_reg_rda,
						 prog_riga_rda)						 
	  values			(:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_off_acq,   
						 :ls_cod_tipo_det_acq,   
						 :ls_cod_prodotto,   
						 :ls_cod_misura,   
						 :ls_des_prodotto,   
						 :ld_quan_ordinata - :ld_quan_evasa,   
						 :ld_prezzo_acquisto,   
						 :ld_fat_conversione,   
						 :ld_sconto_1,   
						 :ld_sconto_2,   
						 :ld_sconto_3,   
						 :ls_cod_iva,   
						 :ldt_data_consegna_det,   
						 :ls_cod_prod_fornitore,   
						 0,   
						 :ld_val_riga_sconto_10,
						 'N',   
						 'N',   
						 :ls_nota_dettaglio,   
						 :ll_anno_commessa,   
						 :ll_num_commessa,   
						 :ls_cod_centro_costo,   
						 :ld_sconto_4,   
						 :ld_sconto_5,   
						 :ld_sconto_6,   
						 :ld_sconto_7,   
						 :ld_sconto_8,   
						 :ld_sconto_9,   
						 :ld_sconto_10,   
						 :ll_anno_offerta,   
						 :ll_num_offerta,   
						 :ll_prog_riga_off_acq,
						 'I',
						 'I',
						 :ll_anno_reg_rda,
						 :ll_num_reg_rda,
						 :ll_prog_riga_rda);						 

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Errore durante la scrittura Dettagli Ordini Acquisto.~n" + sqlca.sqlerrtext, Exclamation!, ok!)
			rollback;
			close cu_det_off_acq;
			return
		end if

	  update tes_ord_acq  
	     set tot_val_ordine = :ld_tot_val_documento  
	   where tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
   	      tes_ord_acq.anno_registrazione = :ll_anno_registrazione and  
      	   tes_ord_acq.num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Errore durante l'aggiornamento Testata Ordini Acquisto.~n" + sqlca.sqlerrtext, Exclamation!, ok!)
			rollback;
			close cu_det_off_acq;
			return
		end if

		if ls_flag_tipo_det_acq = "M" then
			update anag_prodotti  
				set quan_ordinata = quan_ordinata + :ld_quan_ordinata - :ld_quan_evasa
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.~r~n" + sqlca.sqlerrtext, exclamation!, ok!)
				rollback;
				close cu_det_off_acq;
				return
			end if
			
				// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
				
				ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordinata - ld_quan_evasa), "M")
				
				update anag_prodotti  
				set quan_ordinata = quan_ordinata + :ld_quan_raggruppo
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;

				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
					return 1
				end if
			end if
			
		end if				

 	   update det_off_acq  
		   set flag_evasione = 'E',   
			 	 quan_evasa = :ld_quan_ordinata
		 WHERE det_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 	 det_off_acq.anno_registrazione = :ll_anno_offerta and  
				 det_off_acq.num_registrazione = :ll_num_offerta and  
				 det_off_acq.prog_riga_off_acq = :ll_prog_riga_off_acq;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento Dettaglio Offerte Acquisto.~r~n" + sqlca.sqlerrtext, exclamation!, ok!)
			rollback;
			close cu_det_off_acq;
			return
		end if
		
		ls_tabella_orig = "det_off_acq_stat"
		ls_tabella_des = "det_ord_acq_stat"
		ls_prog_riga_doc_orig = "prog_riga_off_acq"
		ls_prog_riga_doc_des = "prog_riga_ordine_acq"

		if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ll_anno_offerta, ll_num_offerta, ll_prog_riga_off_acq, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_acq) = -1 then
			close cu_det_off_acq;
			return
		end if	

		select tab_tipi_ord_acq.cod_tipo_analisi  
		into	 :ls_cod_tipo_analisi_ord
		from   tab_tipi_ord_acq  
		where  tab_tipi_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_ord_acq.cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore nella lettura della Tabella Tipi Ordini.~r~n" + sqlca.sqlerrtext, Exclamation!, ok!)
			close cu_det_off_acq;
			rollback;
			return
		end if

		if ls_cod_tipo_analisi_off <> ls_cod_tipo_analisi_ord then
			if f_crea_distribuzione(ls_cod_tipo_analisi_ord, ls_cod_prodotto, ls_cod_tipo_det_acq, ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_off_acq, 2) = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore nella Generazione dei Dettagli Statistici.~r~n" + sqlca.sqlerrtext, Exclamation!, ok!)
				close cu_det_off_acq;
				rollback;
				return
			end if
		end if
		
		// aggiunto per RDA; se genero ordine da offerta metto il flag generato ordine anche su RDA.
		if isnull(ll_anno_reg_rda) then ll_anno_reg_rda = 0
		if ll_anno_reg_rda > 0 then
			update det_rda
			set flag_gen_ord_acq = 'S'
			where cod_azienda = :s_cs_xx.cod_azienda and
			      anno_registrazione = :ll_anno_reg_rda and
					num_registrazione = :ll_num_reg_rda and
					prog_riga_rda = :ll_prog_riga_rda;
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("APICE", "Si è verificato un errore in fase di aggiornamento RDA con flag_gen_ord_acq = 'S'.~r~n" + sqlca.sqlerrtext, exclamation!, ok!)
				close cu_det_off_acq;
				rollback;
				return
			end if
		end if
	loop
	close cu_det_off_acq;

	update tes_off_acq  
      set flag_evasione = 'E'  
    where tes_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
          tes_off_acq.anno_registrazione = :ll_anno_offerta and  
          tes_off_acq.num_registrazione = :ll_num_offerta;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento Testata Offerte Acquisto.~r~n" + sqlca.sqlerrtext,exclamation!, ok!)
		rollback;
		return
	end if

   dw_gen_orig_dest.insertrow(0)
   dw_gen_orig_dest.setrow(dw_gen_orig_dest.rowcount())
   dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_orig", ll_anno_offerta)
   dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_orig", ll_num_offerta)
   dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_dest", ll_anno_registrazione)
   dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_dest", ll_num_registrazione)
	
	if wf_calcola_documento(ll_anno_registrazione, ll_num_registrazione) < 0 then
		rollback;
		return
	end if
next

commit;

dw_folder.fu_selecttab(2)
dw_tes_gen_ord_acq.triggerevent("pcd_retrieve")

setpointer(arrow!)
end event

type cb_1 from uo_cb_close within w_tes_gen_ord_acq
integer x = 2880
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

type cb_dettaglio from commandbutton within w_tes_gen_ord_acq
event clicked pbm_bnclicked
integer x = 2103
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettagli"
end type

event clicked;long ll_selected[]


dw_tes_gen_ord_acq.get_selected_rows(ll_selected[])

if upperbound(ll_selected) > 1 then
   g_mb.messagebox("Attenzione", "Selezionare solamente un'offerta.", &
              exclamation!, ok!)
   return
end if

s_cs_xx.parametri.parametro_s_1 = dw_tes_gen_ord_acq.getitemstring(dw_tes_gen_ord_acq.getrow(), "cod_tipo_off_acq")
s_cs_xx.parametri.parametro_d_1 = dw_tes_gen_ord_acq.getitemnumber(dw_tes_gen_ord_acq.getrow(), "anno_registrazione")
s_cs_xx.parametri.parametro_d_2 = dw_tes_gen_ord_acq.getitemnumber(dw_tes_gen_ord_acq.getrow(), "num_registrazione")
s_cs_xx.parametri.parametro_b_1 = false

window_open(w_det_gen_ord_acq, 0)

if s_cs_xx.parametri.parametro_b_1 then
	dw_gen_orig_dest.insertrow(0)
	dw_gen_orig_dest.setrow(dw_gen_orig_dest.rowcount())
	dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_orig", s_cs_xx.parametri.parametro_d_1)
	dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_orig",  s_cs_xx.parametri.parametro_d_2)
	dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "anno_registrazione_dest", s_cs_xx.parametri.parametro_d_3)
	dw_gen_orig_dest.setitem(dw_gen_orig_dest.getrow(), "num_registrazione_dest",  s_cs_xx.parametri.parametro_d_4)


	dw_folder.fu_selecttab(2)
	dw_tes_gen_ord_acq.triggerevent("pcd_retrieve")
end if
end event

type dw_tes_gen_ord_acq from uo_cs_xx_dw within w_tes_gen_ord_acq
event pcd_retrieve pbm_custom60
integer x = 23
integer y = 120
integer width = 3200
integer height = 1000
integer taborder = 30
string dataobject = "d_tes_gen_ord_acq"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_select
long ll_errore
datetime ldt_data_oggi


ldt_data_oggi = datetime(today(), now())
ll_errore = retrieve(s_cs_xx.cod_azienda, ldt_data_oggi)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type dw_folder from u_folder within w_tes_gen_ord_acq
integer y = 20
integer width = 3246
integer height = 1120
integer taborder = 20
end type

type dw_gen_orig_dest from uo_cs_xx_dw within w_tes_gen_ord_acq
event pcd_retrieve pbm_custom60
integer x = 23
integer y = 120
integer width = 3200
integer height = 1000
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_gen_orig_dest"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_print;call super::pcd_print;long job

job = PrintOpen( ) 

PrintDataWindow(job, this) 
PrintClose(job)
end event

