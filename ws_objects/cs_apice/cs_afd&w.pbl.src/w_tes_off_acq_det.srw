﻿$PBExportHeader$w_tes_off_acq_det.srw
$PBExportComments$Finestra Gestione Testata Offerte di Acquisto
forward
global type w_tes_off_acq_det from w_cs_xx_principale
end type
type cb_stampa from commandbutton within w_tes_off_acq_det
end type
type cb_dettaglio from commandbutton within w_tes_off_acq_det
end type
type cb_sblocca from commandbutton within w_tes_off_acq_det
end type
type cb_blocca from commandbutton within w_tes_off_acq_det
end type
type cb_duplica_doc from commandbutton within w_tes_off_acq_det
end type
type cb_note from commandbutton within w_tes_off_acq_det
end type
type cb_corrispondenze from commandbutton within w_tes_off_acq_det
end type
type dw_tes_off_acq_det_1 from uo_cs_xx_dw within w_tes_off_acq_det
end type
type dw_tes_off_acq_det_2 from uo_cs_xx_dw within w_tes_off_acq_det
end type
type dw_folder from u_folder within w_tes_off_acq_det
end type
type dw_tes_off_acq_det_4 from uo_cs_xx_dw within w_tes_off_acq_det
end type
type dw_tes_off_acq_det_3 from uo_cs_xx_dw within w_tes_off_acq_det
end type
end forward

global type w_tes_off_acq_det from w_cs_xx_principale
integer width = 3168
integer height = 1780
string title = "Gestione Testata Offerte Acquisti"
event type long ue_calcola_documento ( )
cb_stampa cb_stampa
cb_dettaglio cb_dettaglio
cb_sblocca cb_sblocca
cb_blocca cb_blocca
cb_duplica_doc cb_duplica_doc
cb_note cb_note
cb_corrispondenze cb_corrispondenze
dw_tes_off_acq_det_1 dw_tes_off_acq_det_1
dw_tes_off_acq_det_2 dw_tes_off_acq_det_2
dw_folder dw_folder
dw_tes_off_acq_det_4 dw_tes_off_acq_det_4
dw_tes_off_acq_det_3 dw_tes_off_acq_det_3
end type
global w_tes_off_acq_det w_tes_off_acq_det

type variables
boolean ib_new
string is_cod_parametro_blocco="BOA"
end variables

event type long ue_calcola_documento();string ls_messaggio

long   ll_anno_reg, ll_num_reg

uo_calcola_documento_euro luo_doc


ll_anno_reg = dw_tes_off_acq_det_1.getitemnumber(dw_tes_off_acq_det_1.getrow(),"anno_registrazione")
ll_num_reg = dw_tes_off_acq_det_1.getitemnumber(dw_tes_off_acq_det_1.getrow(),"num_registrazione")

if not isnull(ll_anno_reg) and ll_anno_reg <> 0 and not isnull(ll_num_reg) and ll_num_reg <> 0 then

	luo_doc = create uo_calcola_documento_euro

	if luo_doc.uof_calcola_documento(ll_anno_reg,ll_num_reg,"off_acq",ls_messaggio) <> 0 then
		g_mb.messagebox("Apice",ls_messaggio)
		rollback;
		destroy luo_doc
		return -1
	else
		commit;	
	end if

	destroy luo_doc

	dw_tes_off_acq_det_1.postevent("pcd_retrieve")
	
end if	

return 0
end event

event pc_setddlb;call super::pc_setddlb;string ls_select_operatori
if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + ")) and cod_operatore in (select cod_operatore from tab_operatori_utenti where cod_utente = '" + &
						  s_cs_xx.cod_utente + "')"
else
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + "))"
end if

f_po_loaddddw_dw(dw_tes_off_acq_det_1, &
                 "cod_operatore", &
                 sqlca, &
                 "tab_operatori", &
                 "cod_operatore", &
                 "des_operatore", &
					  ls_select_operatori)
f_po_loaddddw_dw(dw_tes_off_acq_det_1, &
                 "cod_tipo_off_acq", &
                 sqlca, &
                 "tab_tipi_off_acq", &
                 "cod_tipo_off_acq", &
                 "des_tipo_off_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//f_po_loaddddw_dw(dw_tes_off_acq_det_1, &
//                 "cod_for_pot", &
//                 sqlca, &
//                 "anag_for_pot", &
//                 "cod_for_pot", &
//                 "rag_soc_1", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//f_po_loaddddw_dw(dw_tes_off_acq_det_1, &
//                 "cod_fornitore", &
//                 sqlca, &
//                 "anag_fornitori", &
//                 "cod_fornitore", &
//                 "rag_soc_1", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_acq_det_1, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_acq_det_1, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_acq_det_1, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'A' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_acq_det_1, &
                 "cod_pagamento", &
                 sqlca, &
                 "tab_pagamenti", &
                 "cod_pagamento", &
                 "des_pagamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_acq_det_1, &
                 "cod_banca_clien_for", &
                 sqlca, &
                 "anag_banche_clien_for", &
                 "cod_banca_clien_for", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_acq_det_1, &
                 "cod_banca", &
                 sqlca, &
                 "anag_banche", &
                 "cod_banca", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_acq_det_3, &
                 "cod_imballo", &
                 sqlca, &
                 "tab_imballi", &
                 "cod_imballo", &
                 "des_imballo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_acq_det_3, &
                 "cod_vettore", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_acq_det_3, &
                 "cod_inoltro", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_acq_det_3, &
                 "cod_mezzo", &
                 sqlca, &
                 "tab_mezzi", &
                 "cod_mezzo", &
                 "des_mezzo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_acq_det_3, &
                 "cod_porto", &
                 sqlca, &
                 "tab_porti", &
                 "cod_porto", &
                 "des_porto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_off_acq_det_3, &
                 "cod_resa", &
                 sqlca, &
                 "tab_rese", &
                 "cod_resa", &
                 "des_resa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;unsignedlong lul_modalita


choose case s_cs_xx.parametri.parametro_s_1
   case "nuovo"
      lul_modalita = c_newonopen
   case "modifica"
      lul_modalita = c_modifyonopen
   case else
      lul_modalita = c_viewonopen
end choose

dw_tes_off_acq_det_1.set_dw_key("cod_azienda")

dw_tes_off_acq_det_1.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent + lul_modalita, &
                                    c_default)
dw_tes_off_acq_det_2.set_dw_options(sqlca, &
                                    dw_tes_off_acq_det_1, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
dw_tes_off_acq_det_3.set_dw_options(sqlca, &
                                    dw_tes_off_acq_det_1, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
dw_tes_off_acq_det_4.set_dw_options(sqlca, &
                                    dw_tes_off_acq_det_1, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

windowobject lw_oggetti[], lw_oggetti1[]

lw_oggetti1[1] = dw_tes_off_acq_det_1
dw_folder.fu_assigntab(1, "Testata", lw_oggetti1[])
lw_oggetti[1] = dw_tes_off_acq_det_2
dw_folder.fu_assigntab(2, "Fornitori/Destinazione", lw_oggetti[])
lw_oggetti[1] = dw_tes_off_acq_det_3
dw_folder.fu_assigntab(3, "Trasporto", lw_oggetti[])
lw_oggetti[1] = dw_tes_off_acq_det_4
dw_folder.fu_assigntab(4, "Note", lw_oggetti[])
dw_folder.fu_foldercreate(4, 4)
dw_folder.fu_selecttab(1)

iuo_dw_main = dw_tes_off_acq_det_1
ib_prima_riga = false

dw_tes_off_acq_det_1.object.b_ricerca_banca_for.enabled = false
dw_tes_off_acq_det_1.object.b_ricerca_fornitore.enabled = false
dw_tes_off_acq_det_1.object.b_ricerca_for_pot.enabled = false
cb_dettaglio.enabled = false
cb_stampa.enabled = false
cb_blocca.enabled=false
cb_sblocca.enabled=false
cb_duplica_doc.enabled=false
cb_note.enabled=false
cb_corrispondenze.enabled=false
end event

event pc_delete;call super::pc_delete;integer li_i


for li_i = 1 to dw_tes_off_acq_det_1.deletedcount()
	if dw_tes_off_acq_det_1.getitemstring(li_i, "flag_evasione", delete!, true) <> "A" then
	   g_mb.messagebox("Attenzione", "Offerta non cancellabile! Ha già subito un'evasione.", &
	              exclamation!, ok!)
	   dw_tes_off_acq_det_1.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return
	end if

	if dw_tes_off_acq_det_1.getitemstring(li_i, "flag_blocco", delete!, true) = "S" then
	   g_mb.messagebox("Attenzione", "Offerta non cancellabile! E' stata bloccata.", &
	              exclamation!, ok!)
	   dw_tes_off_acq_det_1.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return
	end if
next

dw_tes_off_acq_det_1.object.b_ricerca_banca_for.enabled = false
dw_tes_off_acq_det_1.object.b_ricerca_fornitore.enabled = false
dw_tes_off_acq_det_1.object.b_ricerca_for_pot.enabled = false
cb_dettaglio.enabled = false
cb_stampa.enabled = false
cb_blocca.enabled=false
cb_sblocca.enabled=false
cb_duplica_doc.enabled=false
cb_note.enabled=false
cb_corrispondenze.enabled=false
end event

on w_tes_off_acq_det.create
int iCurrent
call super::create
this.cb_stampa=create cb_stampa
this.cb_dettaglio=create cb_dettaglio
this.cb_sblocca=create cb_sblocca
this.cb_blocca=create cb_blocca
this.cb_duplica_doc=create cb_duplica_doc
this.cb_note=create cb_note
this.cb_corrispondenze=create cb_corrispondenze
this.dw_tes_off_acq_det_1=create dw_tes_off_acq_det_1
this.dw_tes_off_acq_det_2=create dw_tes_off_acq_det_2
this.dw_folder=create dw_folder
this.dw_tes_off_acq_det_4=create dw_tes_off_acq_det_4
this.dw_tes_off_acq_det_3=create dw_tes_off_acq_det_3
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa
this.Control[iCurrent+2]=this.cb_dettaglio
this.Control[iCurrent+3]=this.cb_sblocca
this.Control[iCurrent+4]=this.cb_blocca
this.Control[iCurrent+5]=this.cb_duplica_doc
this.Control[iCurrent+6]=this.cb_note
this.Control[iCurrent+7]=this.cb_corrispondenze
this.Control[iCurrent+8]=this.dw_tes_off_acq_det_1
this.Control[iCurrent+9]=this.dw_tes_off_acq_det_2
this.Control[iCurrent+10]=this.dw_folder
this.Control[iCurrent+11]=this.dw_tes_off_acq_det_4
this.Control[iCurrent+12]=this.dw_tes_off_acq_det_3
end on

on w_tes_off_acq_det.destroy
call super::destroy
destroy(this.cb_stampa)
destroy(this.cb_dettaglio)
destroy(this.cb_sblocca)
destroy(this.cb_blocca)
destroy(this.cb_duplica_doc)
destroy(this.cb_note)
destroy(this.cb_corrispondenze)
destroy(this.dw_tes_off_acq_det_1)
destroy(this.dw_tes_off_acq_det_2)
destroy(this.dw_folder)
destroy(this.dw_tes_off_acq_det_4)
destroy(this.dw_tes_off_acq_det_3)
end on

event pc_modify;call super::pc_modify;if dw_tes_off_acq_det_1.getitemstring(dw_tes_off_acq_det_1.getrow(), "flag_evasione") <> "A" then
   g_mb.messagebox("Attenzione", "Offerta non modificabile! Ha già subito un'evasione.", &
              exclamation!, ok!)
   dw_tes_off_acq_det_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

if dw_tes_off_acq_det_1.getitemstring(dw_tes_off_acq_det_1.getrow(), "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Offerta non modificabile! E' stata bloccata.", &
              exclamation!, ok!)
   dw_tes_off_acq_det_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

end event

event open;call super::open;postevent("ue_calcola_documento")
end event

event getfocus;call super::getfocus;dw_tes_off_acq_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_for_pot"
end event

type cb_stampa from commandbutton within w_tes_off_acq_det
integer x = 2350
integer y = 1580
integer width = 361
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;long   ll_count, ll_anno_reg, ll_num_reg


ll_anno_reg = dw_tes_off_acq_det_1.getitemnumber(dw_tes_off_acq_det_1.getrow(),"anno_registrazione")
ll_num_reg = dw_tes_off_acq_det_1.getitemnumber(dw_tes_off_acq_det_1.getrow(),"num_registrazione")

select count(*)
into   :ll_count
from   det_off_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_reg and
		 num_registrazione = :ll_num_reg;
		 
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Apice","Errore nella select di det_off_acq: " + sqlca.sqlerrtext)
	return -1
end if

if isnull(ll_count) then ll_count = 0

if ll_count < 1 then
	g_mb.messagebox("Stampa Offerta Acquisto","Non ci sono dettagli per questo documento, impossibile stampare")
	return -1
end if	

parent.triggerevent("ue_calcola_documento")

window_open_parm(w_report_off_acq_euro, -1, dw_tes_off_acq_det_1)
end event

type cb_dettaglio from commandbutton within w_tes_off_acq_det
integer x = 2738
integer y = 1580
integer width = 361
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettagli"
end type

event clicked;s_cs_xx.parametri.parametro_w_off_acq = parent

window_open_parm(w_det_off_acq, -1, dw_tes_off_acq_det_1)

end event

type cb_sblocca from commandbutton within w_tes_off_acq_det
event clicked pbm_bnclicked
integer x = 1184
integer y = 1580
integer width = 361
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sbl&occa"
end type

event clicked;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_acq
string ls_sql_stringa

if dw_tes_off_acq_det_1.getrow() > 0 then
	ll_anno_registrazione = dw_tes_off_acq_det_1.getitemnumber(dw_tes_off_acq_det_1.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_tes_off_acq_det_1.getitemnumber(dw_tes_off_acq_det_1.getrow(), "num_registrazione")

	declare cu_dettagli dynamic cursor for sqlsa;

	ls_sql_stringa = "select det_off_acq.prog_riga_off_acq from det_off_acq where det_off_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_off_acq.anno_registrazione = " + string(ll_anno_registrazione) + " and det_off_acq.num_registrazione = " + string(ll_num_registrazione)

	prepare sqlsa from :ls_sql_stringa;

	open dynamic cu_dettagli;

	ll_i = 0
	do while 0 = 0
		ll_i ++
		fetch cu_dettagli into :ll_prog_riga_off_acq;
			if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura per aggiornamento magazzino.", &
						  exclamation!, ok!)
			close cu_dettagli;
			rollback;
			return
		end if

		if sqlca.sqlcode <> 0 then exit

		update det_off_acq
			set flag_blocco = 'N'
		 where det_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_off_acq.anno_registrazione = :ll_anno_registrazione and  
				 det_off_acq.num_registrazione = :ll_num_registrazione and
				 det_off_acq.prog_riga_off_acq = :ll_prog_riga_off_acq;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio offerta.", &
						  exclamation!, ok!)
			rollback;
			close cu_dettagli;
			return
		end if

		update tes_off_acq
		set flag_blocco = 'N'
		where tes_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				tes_off_acq.anno_registrazione = :ll_anno_registrazione and  
				tes_off_acq.num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata offerta.", &
						  exclamation!, ok!)
			rollback;
			close cu_dettagli;
			return
		end if
	loop

	commit;
	close cu_dettagli;
	dw_tes_off_acq_det_1.setitem(dw_tes_off_acq_det_1.getrow(), "flag_blocco", "N")
	dw_tes_off_acq_det_1.setitemstatus(dw_tes_off_acq_det_1.getrow(), "flag_blocco", primary!, notmodified!)
end if
end event

type cb_blocca from commandbutton within w_tes_off_acq_det
event clicked pbm_bnclicked
integer x = 1573
integer y = 1580
integer width = 361
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Blocca"
end type

event clicked;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_acq
string ls_sql_stringa

if dw_tes_off_acq_det_1.getrow() > 0 then
	if dw_tes_off_acq_det_1.getitemstring(dw_tes_off_acq_det_1.getrow(), "flag_blocco") = "N" then
		ll_anno_registrazione = dw_tes_off_acq_det_1.getitemnumber(dw_tes_off_acq_det_1.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_tes_off_acq_det_1.getitemnumber(dw_tes_off_acq_det_1.getrow(), "num_registrazione")

		declare cu_dettagli dynamic cursor for sqlsa;

		ls_sql_stringa = "select det_off_acq.prog_riga_off_acq from det_off_acq where det_off_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_off_acq.anno_registrazione = " + string(ll_anno_registrazione) + " and det_off_acq.num_registrazione = " + string(ll_num_registrazione)

		prepare sqlsa from :ls_sql_stringa;

		open dynamic cu_dettagli;

		ll_i = 0
		do while 0 = 0
		   ll_i ++
		   fetch cu_dettagli into :ll_prog_riga_off_acq;
			   if sqlca.sqlcode = -1 then
		      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura per aggiornamento magazzino.", &
		                 exclamation!, ok!)
				close cu_dettagli;
				rollback;
		  	   return
		   end if

		   if sqlca.sqlcode <> 0 then exit
		
			update det_off_acq
				set flag_blocco = 'S'
			 where det_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
					 det_off_acq.anno_registrazione = :ll_anno_registrazione and  
					 det_off_acq.num_registrazione = :ll_num_registrazione and
					 det_off_acq.prog_riga_off_acq = :ll_prog_riga_off_acq;

			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio offerta.", &
							  exclamation!, ok!)
				rollback;
				close cu_dettagli;
				rollback;
				return
			end if

			update tes_off_acq
			set flag_blocco = 'S'
			where tes_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
					tes_off_acq.anno_registrazione = :ll_anno_registrazione and  
					tes_off_acq.num_registrazione = :ll_num_registrazione;

			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata offerta.", &
							  exclamation!, ok!)
				rollback;
				close cu_dettagli;
				return
			end if
		loop

		close cu_dettagli;
		dw_tes_off_acq_det_1.setitem(dw_tes_off_acq_det_1.getrow(), "flag_blocco", "S")
		dw_tes_off_acq_det_1.setitemstatus(dw_tes_off_acq_det_1.getrow(), "flag_blocco", primary!, notmodified!)
		commit;
	end if
end if
end event

type cb_duplica_doc from commandbutton within w_tes_off_acq_det
integer x = 800
integer y = 1580
integer width = 361
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Duplica Doc"
end type

event clicked;long ll_anno_registrazione, ll_num_registrazione, ll_anno_registrazione_orig, &
	  ll_num_registrazione_orig, ll_riga
datetime ldt_oggi


ldt_oggi = datetime(today())

select parametri_azienda.numero
into   :ll_anno_registrazione
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
		 parametri_azienda.flag_parametro = 'N' and &
		 parametri_azienda.cod_parametro = 'ESC';

select con_off_acq.num_registrazione
into   :ll_num_registrazione
from   con_off_acq
where  con_off_acq.cod_azienda = :s_cs_xx.cod_azienda;

ll_anno_registrazione_orig = dw_tes_off_acq_det_1.getitemnumber(dw_tes_off_acq_det_1.getrow(), "anno_registrazione")
ll_num_registrazione_orig = dw_tes_off_acq_det_1.getitemnumber(dw_tes_off_acq_det_1.getrow(), "num_registrazione")

insert into tes_off_acq  
				(cod_azienda,   
				 anno_registrazione,   
				 num_registrazione,   
				 cod_fornitore,   
				 cod_tipo_off_acq,   
				 data_registrazione,   
				 cod_operatore,   
				 rag_soc_1,   
				 rag_soc_2,   
				 indirizzo,   
				 localita,   
				 frazione,   
				 cap,   
				 provincia,   
				 cod_fil_fornitore,   
				 cod_des_fornitore,   
				 cod_deposito,   
				 cod_valuta,   
				 cambio_acq,   
				 cod_tipo_listino_prodotto,   
				 cod_pagamento,   
				 sconto,   
				 cod_banca_clien_for,   
				 num_off_fornitore,   
				 data_off_fornitore,   
				 cod_imballo,   
				 cod_vettore,   
				 cod_inoltro,   
				 cod_mezzo,   
				 cod_porto,   
				 cod_resa,   
				 flag_blocco,   
				 tot_val_offerta,   
				 nota_testata,   
				 nota_piede,   
				 cod_for_pot,   
				 data_consegna,   
				 data_scadenza,   
				 cod_banca,   
				 flag_evasione)  
select		 tes_off_acq.cod_azienda,   
				 :ll_anno_registrazione,   
			    :ll_num_registrazione + 1,   
				 tes_off_acq.cod_fornitore,   
				 tes_off_acq.cod_tipo_off_acq,   
			 	 :ldt_oggi,   
				 tes_off_acq.cod_operatore,   
				 tes_off_acq.rag_soc_1,   
				 tes_off_acq.rag_soc_2,   
				 tes_off_acq.indirizzo,   
				 tes_off_acq.localita,   
				 tes_off_acq.frazione,   
				 tes_off_acq.cap,   
				 tes_off_acq.provincia,   
				 tes_off_acq.cod_fil_fornitore,   
				 tes_off_acq.cod_des_fornitore,   
				 tes_off_acq.cod_deposito,   
				 tes_off_acq.cod_valuta,   
				 tes_off_acq.cambio_acq,   
				 tes_off_acq.cod_tipo_listino_prodotto,   
				 tes_off_acq.cod_pagamento,   
				 tes_off_acq.sconto,   
				 tes_off_acq.cod_banca_clien_for,   
				 tes_off_acq.num_off_fornitore,   
				 tes_off_acq.data_off_fornitore,   
				 tes_off_acq.cod_imballo,   
				 tes_off_acq.cod_vettore,   
				 tes_off_acq.cod_inoltro,   
				 tes_off_acq.cod_mezzo,   
				 tes_off_acq.cod_porto,   
				 tes_off_acq.cod_resa,   
				 'N',   
				 tes_off_acq.tot_val_offerta,   
				 tes_off_acq.nota_testata,   
				 tes_off_acq.nota_piede,   
				 tes_off_acq.cod_for_pot,   
				 tes_off_acq.data_consegna,   
				 tes_off_acq.data_scadenza,   
				 tes_off_acq.cod_banca,   
				 'A'  
from  		 tes_off_acq  
where			 tes_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 	 tes_off_acq.anno_registrazione = :ll_anno_registrazione_orig and  
				 tes_off_acq.num_registrazione = :ll_num_registrazione_orig;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di duplicazione testata offerte.", &
				  exclamation!, ok!)
	rollback;
	return
end if

insert into det_off_acq  
		      (cod_azienda,   
			    anno_registrazione,   
			    num_registrazione,   
			    prog_riga_off_acq,   
			    cod_tipo_det_acq,   
			    cod_prodotto,   
			    cod_misura,   
			    des_prodotto,   
			    quan_ordinata,   
			    prezzo_acquisto,   
			    fat_conversione,   
			    sconto_1,   
			    sconto_2,   
			    sconto_3,   
			    cod_iva,   
			    data_consegna,   
			    cod_prod_fornitore,   
			    val_riga,   
			    nota_dettaglio,   
			    anno_commessa,   
			    num_commessa,   
			    cod_centro_costo,   
			    sconto_4,   
			    sconto_5,   
			    sconto_6,   
			    sconto_7,   
			    sconto_8,   
			    sconto_9,   
			    sconto_10,   
			    quan_evasa,   
			    flag_blocco,   
			    flag_evasione)  
select      det_off_acq.cod_azienda,   
				:ll_anno_registrazione,   
				:ll_num_registrazione + 1,   
				det_off_acq.prog_riga_off_acq,   
				det_off_acq.cod_tipo_det_acq,   
				det_off_acq.cod_prodotto,   
				det_off_acq.cod_misura,   
				det_off_acq.des_prodotto,   
				det_off_acq.quan_ordinata,   
				det_off_acq.prezzo_acquisto,   
				det_off_acq.fat_conversione,   
				det_off_acq.sconto_1,   
				det_off_acq.sconto_2,   
				det_off_acq.sconto_3,   
				det_off_acq.cod_iva,   
				det_off_acq.data_consegna,   
				det_off_acq.cod_prod_fornitore,   
				det_off_acq.val_riga,   
				det_off_acq.nota_dettaglio,   
				det_off_acq.anno_commessa,   
				det_off_acq.num_commessa,   
				det_off_acq.cod_centro_costo,   
				det_off_acq.sconto_4,   
				det_off_acq.sconto_5,   
				det_off_acq.sconto_6,   
				det_off_acq.sconto_7,   
				det_off_acq.sconto_8,   
				det_off_acq.sconto_9,   
				det_off_acq.sconto_10,   
				0,   
				'N',   
				'A'  
from     	det_off_acq  
where    	det_off_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				det_off_acq.anno_registrazione = :ll_anno_registrazione_orig and  
				det_off_acq.num_registrazione = :ll_num_registrazione_orig;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di duplicazione dettagli offerte.", &
				  exclamation!, ok!)
	rollback;
	return
end if

insert into det_off_acq_stat  
				(cod_azienda,   
			    anno_registrazione,   
			    num_registrazione,   
			    prog_riga_off_acq,   
			    cod_tipo_analisi,   
			    progressivo,   
			    cod_statistico_1,   
			    cod_statistico_2,   
			    cod_statistico_3,   
			    cod_statistico_4,   
			    cod_statistico_5,   
			    cod_statistico_6,   
			    cod_statistico_7,   
			    cod_statistico_8,   
			    cod_statistico_9,   
			    cod_statistico_10,   
			    percentuale,   
			    flag_trasferito)  
select      det_off_acq_stat.cod_azienda,   
			   :ll_anno_registrazione,   
			   :ll_num_registrazione + 1,   
			   det_off_acq_stat.prog_riga_off_acq,   
			   det_off_acq_stat.cod_tipo_analisi,   
			   det_off_acq_stat.progressivo,   
			   det_off_acq_stat.cod_statistico_1,   
			   det_off_acq_stat.cod_statistico_2,   
			   det_off_acq_stat.cod_statistico_3,   
			   det_off_acq_stat.cod_statistico_4,   
			   det_off_acq_stat.cod_statistico_5,   
			   det_off_acq_stat.cod_statistico_6,   
			   det_off_acq_stat.cod_statistico_7,   
			   det_off_acq_stat.cod_statistico_8,   
			   det_off_acq_stat.cod_statistico_9,   
			   det_off_acq_stat.cod_statistico_10,   
			   det_off_acq_stat.percentuale,   
			   'N'  
from		   det_off_acq_stat  
where       det_off_acq_stat.cod_azienda = :s_cs_xx.cod_azienda and  
			   det_off_acq_stat.anno_registrazione = :ll_anno_registrazione_orig and  
			   det_off_acq_stat.num_registrazione = :ll_num_registrazione_orig;


if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di duplicazione dettagli statistici.", &
				  exclamation!, ok!)
	rollback;
	return
end if

update con_off_acq
set    con_off_acq.num_registrazione = :ll_num_registrazione + 1
where  con_off_acq.cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento contatore.", &
				  exclamation!, ok!)
	rollback;
	return
end if

commit;

if ll_num_registrazione + 1 > ll_num_registrazione_orig then
	ll_riga = dw_tes_off_acq_det_1.i_parentdw.getrow()
else
	ll_riga = dw_tes_off_acq_det_1.i_parentdw.getrow() + 1
end if

dw_tes_off_acq_det_1.i_parentdw.triggerevent("pcd_retrieve")

dw_tes_off_acq_det_1.i_parentdw.setrow(ll_riga)

g_mb.messagebox("Generazione Offerta", "Generata Offerta N. " + string(ll_anno_registrazione) + "/" + &
			  string(ll_num_registrazione + 1), information!, ok!)

end event

type cb_note from commandbutton within w_tes_off_acq_det
event clicked pbm_bnclicked
integer x = 407
integer y = 1580
integer width = 361
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Note"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "Off_Acq"
window_open_parm(w_acq_ven_note, -1, dw_tes_off_acq_det_1)

end event

type cb_corrispondenze from commandbutton within w_tes_off_acq_det
event clicked pbm_bnclicked
integer x = 18
integer y = 1580
integer width = 361
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corrispond."
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "Off_Acq"
window_open_parm(w_acq_ven_corr, -1, dw_tes_off_acq_det_1)

end event

event getfocus;call super::getfocus;dw_tes_off_acq_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_banca_clien_for"
end event

type dw_tes_off_acq_det_1 from uo_cs_xx_dw within w_tes_off_acq_det
integer x = 41
integer y = 116
integer width = 2990
integer height = 1396
integer taborder = 40
string dataobject = "d_tes_off_acq_det_1"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
   string ls_null, ls_cod_fornitore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
          ls_frazione, ls_cap, ls_localita, ls_provincia, ls_telefono, &
          ls_fax, ls_telex, ls_partita_iva, ls_cod_fiscale, ls_cod_deposito, &
          ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
          ls_cod_banca_clien_for, ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, &
          ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_cod_for_pot, ls_nota_testata, &
			 ls_nota_piede, ls_nota_video, ls_cod_banca, ls_messaggio
	integer li_ret
   double ld_sconto
   datetime ldt_data_registrazione
	uo_fido_cliente luo_fido_cliente
	
   setnull(ls_null)

   choose case i_colname
      case "data_registrazione"
         ls_cod_valuta = this.getitemstring(i_rownbr, "cod_valuta")
         f_cambio_acq(ls_cod_valuta, datetime(date(i_coltext)))
      case "cod_fornitore"
			
		ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
		luo_fido_cliente = create uo_fido_cliente
		li_ret = luo_fido_cliente.uof_get_blocco_fornitore( data, ldt_data_registrazione, is_cod_parametro_blocco, ls_messaggio)
		if li_ret=2 then
			//blocco
			g_mb.error(ls_messaggio)
			this.setitem(i_rownbr, i_colname, ls_null)
			this.SetColumn ( i_colname )
			destroy luo_fido_cliente
			return 2
		elseif li_ret=1 then
			//solo warning
			g_mb.warning(ls_messaggio)
		end if
		destroy luo_fido_cliente
			
			
         select anag_fornitori.rag_soc_1,
                anag_fornitori.rag_soc_2,
                anag_fornitori.indirizzo,
                anag_fornitori.frazione,
                anag_fornitori.cap,
                anag_fornitori.localita,
                anag_fornitori.provincia,
                anag_fornitori.telefono,
                anag_fornitori.fax,
                anag_fornitori.telex,
                anag_fornitori.partita_iva,
                anag_fornitori.cod_fiscale,
                anag_fornitori.cod_deposito,
                anag_fornitori.cod_valuta,
                anag_fornitori.cod_tipo_listino_prodotto,
                anag_fornitori.cod_pagamento,
                anag_fornitori.sconto,
                anag_fornitori.cod_banca_clien_for,
                anag_fornitori.cod_banca,
                anag_fornitori.cod_imballo,
                anag_fornitori.cod_vettore,
                anag_fornitori.cod_inoltro,
                anag_fornitori.cod_mezzo,
                anag_fornitori.cod_porto,
                anag_fornitori.cod_resa
         into   :ls_rag_soc_1,    
                :ls_rag_soc_2,
                :ls_indirizzo,    
                :ls_frazione,
                :ls_cap,
                :ls_localita,
                :ls_provincia,
                :ls_telefono,
                :ls_fax,
                :ls_telex,
                :ls_partita_iva,
                :ls_cod_fiscale,
                :ls_cod_deposito,
                :ls_cod_valuta,
                :ls_cod_tipo_listino_prodotto,
                :ls_cod_pagamento,
                :ld_sconto,
                :ls_cod_banca_clien_for,
                :ls_cod_banca,
                :ls_cod_imballo,
                :ls_cod_vettore,
                :ls_cod_inoltro,
                :ls_cod_mezzo,
                :ls_cod_porto,
                :ls_cod_resa
         from   anag_fornitori
         where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_fornitori.cod_fornitore = :i_coltext;
 
         if sqlca.sqlcode = 0 then 
            this.setitem(i_rownbr, "cod_deposito", ls_cod_deposito)
            this.setitem(i_rownbr, "cod_valuta", ls_cod_valuta)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
            this.setitem(i_rownbr, "cod_pagamento", ls_cod_pagamento)
            this.setitem(i_rownbr, "sconto", ld_sconto)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_cod_banca_clien_for)
            this.setitem(i_rownbr, "cod_banca", ls_cod_banca)
            if not isnull(i_coltext) then
               dw_tes_off_acq_det_2.modify("st_cod_fornitore.text='" + i_coltext + "'")
            else
               dw_tes_off_acq_det_2.modify("st_cod_fornitore.text=''")
            end if
            if not isnull(ls_rag_soc_1) then
               dw_tes_off_acq_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
            else
               dw_tes_off_acq_det_2.modify("st_rag_soc_1.text=''")
            end if
            if not isnull(ls_rag_soc_2) then
               dw_tes_off_acq_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
            else
               dw_tes_off_acq_det_2.modify("st_rag_soc_2.text=''")
            end if
            if not isnull(ls_indirizzo) then
               dw_tes_off_acq_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
            else
               dw_tes_off_acq_det_2.modify("st_indirizzo.text=''")
            end if
            if not isnull(ls_frazione) then
               dw_tes_off_acq_det_2.modify("st_frazione.text='" + ls_frazione + "'")
            else
               dw_tes_off_acq_det_2.modify("st_frazione.text=''")
            end if
            if not isnull(ls_cap) then
               dw_tes_off_acq_det_2.modify("st_cap.text='" + ls_cap + "'")
            else
               dw_tes_off_acq_det_2.modify("st_cap.text=''")
            end if
            if not isnull(ls_localita) then
               dw_tes_off_acq_det_2.modify("st_localita.text='" + ls_localita + "'")
            else
               dw_tes_off_acq_det_2.modify("st_localita.text=''")
            end if
            if not isnull(ls_provincia) then
               dw_tes_off_acq_det_2.modify("st_provincia.text='" + ls_provincia + "'")
            else
               dw_tes_off_acq_det_2.modify("st_provincia.text=''")
            end if
            if not isnull(ls_telefono) then
               dw_tes_off_acq_det_2.modify("st_telefono.text='" + ls_telefono + "'")
            else
               dw_tes_off_acq_det_2.modify("st_telefono.text=''")
            end if
            if not isnull(ls_fax) then
               dw_tes_off_acq_det_2.modify("st_fax.text='" + ls_fax + "'")
            else
               dw_tes_off_acq_det_2.modify("st_fax.text=''")
            end if
            if not isnull(ls_telex) then
               dw_tes_off_acq_det_2.modify("st_telex.text='" + ls_telex + "'")
            else
               dw_tes_off_acq_det_2.modify("st_telex.text=''")
            end if
            if not isnull(ls_partita_iva) then
               dw_tes_off_acq_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
            else
               dw_tes_off_acq_det_2.modify("st_partita_iva.text=''")
            end if
            if not isnull(ls_cod_fiscale) then
               dw_tes_off_acq_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
            else
               dw_tes_off_acq_det_2.modify("st_cod_fiscale.text=''")
            end if
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_imballo", ls_cod_imballo)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_vettore", ls_cod_vettore)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_inoltro", ls_cod_inoltro)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_mezzo", ls_cod_mezzo)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_porto", ls_cod_porto)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_resa", ls_cod_resa)
   
            f_po_loaddddw_dw(this, &
                             "cod_des_fornitore", &
                             sqlca, &
                             "anag_des_fornitori", &
                             "cod_des_fornitore", &
                             "rag_soc_1", &
                             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + i_coltext + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
    
            this.setitem(i_rownbr, "cod_des_fornitore", ls_null)

            f_po_loaddddw_dw(this, &
                             "cod_fil_fornitore", &
                             sqlca, &
                             "anag_fil_fornitori", &
                             "cod_fil_fornitore", &
                             "rag_soc_1", &
                             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + i_coltext + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
    
            this.setitem(i_rownbr, "cod_fil_fornitore", ls_null)

            ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
            f_cambio_acq(ls_cod_valuta, ldt_data_registrazione)
            
            select anag_for_pot.cod_for_pot
            into   :ls_cod_for_pot
            from   anag_for_pot
            where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
                   anag_for_pot.cod_fornitore = :i_coltext;
   
            if sqlca.sqlcode = 0 then 
               this.setitem(this.getrow(), "cod_for_pot", ls_cod_for_pot)
               dw_tes_off_acq_det_2.modify("st_cod_for_pot.text='" + ls_cod_for_pot + "'")
            else
               this.setitem(this.getrow(), "cod_for_pot", ls_null)
               dw_tes_off_acq_det_2.modify("st_cod_for_pot.text=''")
            end if         
         else
            this.setitem(i_rownbr, "cod_deposito", ls_null)
            this.setitem(i_rownbr, "cod_valuta", ls_null)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_null)
            this.setitem(i_rownbr, "cod_pagamento", ls_null)
            this.setitem(i_rownbr, "sconto", ls_null)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_null)
            this.setitem(i_rownbr, "cod_banca", ls_null)
            dw_tes_off_acq_det_2.modify("st_cod_fornitore.text=''")
            dw_tes_off_acq_det_2.modify("st_cod_for_pot.text=''")
            dw_tes_off_acq_det_2.modify("st_rag_soc_1.text=''")
            dw_tes_off_acq_det_2.modify("st_rag_soc_2.text=''")
            dw_tes_off_acq_det_2.modify("st_indirizzo.text=''")
            dw_tes_off_acq_det_2.modify("st_frazione.text=''")
            dw_tes_off_acq_det_2.modify("st_cap.text=''")
            dw_tes_off_acq_det_2.modify("st_localita.text=''")
            dw_tes_off_acq_det_2.modify("st_provincia.text=''")
            dw_tes_off_acq_det_2.modify("st_telefono.text=''")
            dw_tes_off_acq_det_2.modify("st_fax.text=''")
            dw_tes_off_acq_det_2.modify("st_telex.text=''")
            dw_tes_off_acq_det_2.modify("st_partita_iva.text=''")
            dw_tes_off_acq_det_2.modify("st_cod_fiscale.text=''")
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_imballo", ls_null)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_vettore", ls_null)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_inoltro", ls_null)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_mezzo", ls_null)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_porto", ls_null)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_resa", ls_null)
            this.setitem(i_rownbr, "cod_des_fornitore", ls_null)
            this.setitem(i_rownbr, "cod_fil_fornitore", ls_null)
            this.setitem(this.getrow(), "cod_for_pot", ls_null)
         end if 

			setnull(ls_null)
			ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
			
			if guo_functions.uof_get_note_fisse(ls_null, data, ls_null, "OFF_ACQ", ls_null, ldt_data_registrazione, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
				g_mb.error(ls_nota_testata)
			else
				if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
			end if
			
		case "cod_for_pot"
         select anag_for_pot.rag_soc_1,
                anag_for_pot.rag_soc_2,
                anag_for_pot.indirizzo,
                anag_for_pot.frazione,
                anag_for_pot.cap,
                anag_for_pot.localita,
                anag_for_pot.provincia,
                anag_for_pot.telefono,
                anag_for_pot.fax,
                anag_for_pot.telex,
                anag_for_pot.partita_iva,
                anag_for_pot.cod_fiscale,
                anag_for_pot.cod_deposito,
                anag_for_pot.cod_valuta,
                anag_for_pot.cod_tipo_listino_prodotto,
                anag_for_pot.cod_pagamento,
                anag_for_pot.sconto,
                anag_for_pot.cod_banca_clien_for,
                anag_for_pot.cod_imballo,
                anag_for_pot.cod_vettore,
                anag_for_pot.cod_inoltro,
                anag_for_pot.cod_mezzo,
                anag_for_pot.cod_porto,
                anag_for_pot.cod_resa,
                anag_for_pot.cod_fornitore
         into   :ls_rag_soc_1,    
                :ls_rag_soc_2,
                :ls_indirizzo,    
                :ls_frazione,
                :ls_cap,
                :ls_localita,
                :ls_provincia,
                :ls_telefono,
                :ls_fax,
                :ls_telex,
                :ls_partita_iva,
                :ls_cod_fiscale,
                :ls_cod_deposito,
                :ls_cod_valuta,
                :ls_cod_tipo_listino_prodotto,
                :ls_cod_pagamento,
                :ld_sconto,
                :ls_cod_banca_clien_for,
                :ls_cod_imballo,
                :ls_cod_vettore,
                :ls_cod_inoltro,
                :ls_cod_mezzo,
                :ls_cod_porto,
                :ls_cod_resa,
                :ls_cod_fornitore
         from   anag_for_pot
         where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_for_pot.cod_for_pot = :i_coltext;
    
         if sqlca.sqlcode = 0 then 
            this.setitem(i_rownbr, "cod_deposito", ls_cod_deposito)
            this.setitem(i_rownbr, "cod_valuta", ls_cod_valuta)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
            this.setitem(i_rownbr, "cod_pagamento", ls_cod_pagamento)
            this.setitem(i_rownbr, "sconto", ld_sconto)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_cod_banca_clien_for)
            this.setitem(i_rownbr, "cod_banca", ls_cod_banca)
            this.setitem(i_rownbr, "cod_fornitore", ls_cod_fornitore)
            if not isnull(i_coltext) then
               dw_tes_off_acq_det_2.modify("st_cod_for_pot.text='" + i_coltext + "'")
            else
               dw_tes_off_acq_det_2.modify("st_cod_for_pot.text=''")
            end if
            if not isnull(ls_cod_fornitore) then
               dw_tes_off_acq_det_2.modify("st_cod_fornitore.text='" + ls_cod_fornitore + "'")
            else
               dw_tes_off_acq_det_2.modify("st_cod_fornitore.text=''")
               ls_cod_fornitore = ""
            end if
            if not isnull(ls_rag_soc_1) then
               dw_tes_off_acq_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
            else
               dw_tes_off_acq_det_2.modify("st_rag_soc_1.text=''")
            end if
            if not isnull(ls_rag_soc_2) then
               dw_tes_off_acq_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
            else
               dw_tes_off_acq_det_2.modify("st_rag_soc_2.text=''")
            end if
            if not isnull(ls_indirizzo) then
               dw_tes_off_acq_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
            else
               dw_tes_off_acq_det_2.modify("st_indirizzo.text=''")
            end if
            if not isnull(ls_frazione) then
               dw_tes_off_acq_det_2.modify("st_frazione.text='" + ls_frazione + "'")
            else
               dw_tes_off_acq_det_2.modify("st_frazione.text=''")
            end if
            if not isnull(ls_cap) then
               dw_tes_off_acq_det_2.modify("st_cap.text='" + ls_cap + "'")
            else
               dw_tes_off_acq_det_2.modify("st_cap.text=''")
            end if
            if not isnull(ls_localita) then
               dw_tes_off_acq_det_2.modify("st_localita.text='" + ls_localita + "'")
            else
               dw_tes_off_acq_det_2.modify("st_localita.text=''")
            end if
            if not isnull(ls_provincia) then
               dw_tes_off_acq_det_2.modify("st_provincia.text='" + ls_provincia + "'")
            else
               dw_tes_off_acq_det_2.modify("st_provincia.text=''")
            end if
            if not isnull(ls_telefono) then
               dw_tes_off_acq_det_2.modify("st_telefono.text='" + ls_telefono + "'")
            else
               dw_tes_off_acq_det_2.modify("st_telefono.text=''")
            end if
            if not isnull(ls_fax) then
               dw_tes_off_acq_det_2.modify("st_fax.text='" + ls_fax + "'")
            else
               dw_tes_off_acq_det_2.modify("st_fax.text=''")
            end if
            if not isnull(ls_telex) then
               dw_tes_off_acq_det_2.modify("st_telex.text='" + ls_telex + "'")
            else
               dw_tes_off_acq_det_2.modify("st_telex.text=''")
            end if
            if not isnull(ls_partita_iva) then
               dw_tes_off_acq_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
            else
               dw_tes_off_acq_det_2.modify("st_partita_iva.text=''")
            end if
            if not isnull(ls_cod_fiscale) then
               dw_tes_off_acq_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
            else
               dw_tes_off_acq_det_2.modify("st_cod_fiscale.text=''")
            end if
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_imballo", ls_cod_imballo)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_vettore", ls_cod_vettore)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_inoltro", ls_cod_inoltro)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_mezzo", ls_cod_mezzo)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_porto", ls_cod_porto)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_resa", ls_cod_resa)
   
            f_po_loaddddw_dw(this, &
                             "cod_des_fornitore", &
                             sqlca, &
                             "anag_des_fornitori", &
                             "cod_des_fornitore", &
                             "rag_soc_1", &
                             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + ls_cod_fornitore + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
   
            this.setitem(i_rownbr, "cod_des_fornitore", ls_null)

            f_po_loaddddw_dw(this, &
                             "cod_fil_fornitore", &
                             sqlca, &
                             "anag_fil_fornitori", &
                             "cod_fil_fornitore", &
                             "rag_soc_1", &
                             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + ls_cod_fornitore + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
   
            this.setitem(i_rownbr, "cod_des_fornitore", ls_null)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "rag_soc_1", ls_null)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "rag_soc_2", ls_null)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "indirizzo", ls_null)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "frazione", ls_null)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "cap", ls_null)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "localita", ls_null)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "provincia", ls_null)
            ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
            f_cambio_acq(ls_cod_valuta, ldt_data_registrazione)
         else
            this.setitem(i_rownbr, "cod_deposito", ls_null)
            this.setitem(i_rownbr, "cod_valuta", ls_null)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_null)
            this.setitem(i_rownbr, "cod_pagamento", ls_null)
            this.setitem(i_rownbr, "sconto", ls_null)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_null)
            this.setitem(i_rownbr, "cod_banca", ls_null)
            this.setitem(i_rownbr, "cod_fornitore", ls_null)
            dw_tes_off_acq_det_2.modify("st_cod_fornitore.text=''")
            dw_tes_off_acq_det_2.modify("st_cod_for_pot.text=''")
            dw_tes_off_acq_det_2.modify("st_rag_soc_1.text=''")
            dw_tes_off_acq_det_2.modify("st_rag_soc_2.text=''")
            dw_tes_off_acq_det_2.modify("st_indirizzo.text=''")
            dw_tes_off_acq_det_2.modify("st_frazione.text=''")
            dw_tes_off_acq_det_2.modify("st_cap.text=''")
            dw_tes_off_acq_det_2.modify("st_localita.text=''")
            dw_tes_off_acq_det_2.modify("st_provincia.text=''")
            dw_tes_off_acq_det_2.modify("st_telefono.text=''")
            dw_tes_off_acq_det_2.modify("st_fax.text=''")
            dw_tes_off_acq_det_2.modify("st_telex.text=''")
            dw_tes_off_acq_det_2.modify("st_partita_iva.text=''")
            dw_tes_off_acq_det_2.modify("st_cod_fiscale.text=''")
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_imballo", ls_null)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_vettore", ls_null)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_inoltro", ls_null)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_mezzo", ls_null)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_porto", ls_null)
            dw_tes_off_acq_det_3.setitem(i_rownbr, "cod_resa", ls_null)
            this.setitem(i_rownbr, "cod_des_fornitore", ls_null)
            this.setitem(i_rownbr, "cod_fil_fornitore", ls_null)
         end if 
      case "cod_fil_fornitore"
         ls_cod_fornitore = this.getitemstring(i_rownbr, "cod_fornitore")
         ls_cod_for_pot = this.getitemstring(i_rownbr, "cod_for_pot")

         select anag_fil_fornitori.rag_soc_1,
                anag_fil_fornitori.rag_soc_2,
                anag_fil_fornitori.indirizzo,
                anag_fil_fornitori.frazione,
                anag_fil_fornitori.cap,
                anag_fil_fornitori.localita,
                anag_fil_fornitori.provincia,
                anag_fil_fornitori.telefono,
                anag_fil_fornitori.fax,
                anag_fil_fornitori.telex
         into   :ls_rag_soc_1,    
                :ls_rag_soc_2,
                :ls_indirizzo,    
                :ls_frazione,
                :ls_cap,
                :ls_localita,
                :ls_provincia,
                :ls_telefono,
                :ls_fax,
                :ls_telex
         from   anag_fil_fornitori
         where  anag_fil_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_fil_fornitori.cod_fornitore = :ls_cod_fornitore and 
					 anag_fil_fornitori.cod_fil_fornitore = :i_coltext;
 
         if sqlca.sqlcode = 0 then 
            if not isnull(ls_rag_soc_1) then
               dw_tes_off_acq_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
            else
               dw_tes_off_acq_det_2.modify("st_rag_soc_1.text=''")
            end if
            if not isnull(ls_rag_soc_2) then
               dw_tes_off_acq_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
            else
               dw_tes_off_acq_det_2.modify("st_rag_soc_2.text=''")
            end if
            if not isnull(ls_indirizzo) then
               dw_tes_off_acq_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
            else
               dw_tes_off_acq_det_2.modify("st_indirizzo.text=''")
            end if
            if not isnull(ls_frazione) then
               dw_tes_off_acq_det_2.modify("st_frazione.text='" + ls_frazione + "'")
            else
               dw_tes_off_acq_det_2.modify("st_frazione.text=''")
            end if
            if not isnull(ls_cap) then
               dw_tes_off_acq_det_2.modify("st_cap.text='" + ls_cap + "'")
            else
               dw_tes_off_acq_det_2.modify("st_cap.text=''")
            end if
            if not isnull(ls_localita) then
               dw_tes_off_acq_det_2.modify("st_localita.text='" + ls_localita + "'")
            else
               dw_tes_off_acq_det_2.modify("st_localita.text=''")
            end if
            if not isnull(ls_provincia) then
               dw_tes_off_acq_det_2.modify("st_provincia.text='" + ls_provincia + "'")
            else
               dw_tes_off_acq_det_2.modify("st_provincia.text=''")
            end if
            if not isnull(ls_telefono) then
               dw_tes_off_acq_det_2.modify("st_telefono.text='" + ls_telefono + "'")
            else
               dw_tes_off_acq_det_2.modify("st_telefono.text=''")
            end if
            if not isnull(ls_fax) then
               dw_tes_off_acq_det_2.modify("st_fax.text='" + ls_fax + "'")
            else
               dw_tes_off_acq_det_2.modify("st_fax.text=''")
            end if
            if not isnull(ls_telex) then
               dw_tes_off_acq_det_2.modify("st_telex.text='" + ls_telex + "'")
            else
               dw_tes_off_acq_det_2.modify("st_telex.text=''")
            end if
			else
	         select anag_fornitori.rag_soc_1,
   	             anag_fornitori.rag_soc_2,
      	          anag_fornitori.indirizzo,
	                anag_fornitori.frazione,
   	             anag_fornitori.cap,
      	          anag_fornitori.localita,
         	       anag_fornitori.provincia,
            	    anag_fornitori.telefono,
	                anag_fornitori.fax,
	                anag_fornitori.telex
	         into   :ls_rag_soc_1,    
	                :ls_rag_soc_2,
	                :ls_indirizzo,    
	                :ls_frazione,
	                :ls_cap,
	                :ls_localita,
	                :ls_provincia,
	                :ls_telefono,
	                :ls_fax,
	                :ls_telex
	         from   anag_fornitori
	         where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
	                anag_fornitori.cod_fornitore = :ls_cod_fornitore;
 
	         if sqlca.sqlcode = 0 then 
	            if not isnull(ls_rag_soc_1) then
	               dw_tes_off_acq_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
	            else
	               dw_tes_off_acq_det_2.modify("st_rag_soc_1.text=''")
	            end if
	            if not isnull(ls_rag_soc_2) then
	               dw_tes_off_acq_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
	            else
	               dw_tes_off_acq_det_2.modify("st_rag_soc_2.text=''")
	            end if
	            if not isnull(ls_indirizzo) then
	               dw_tes_off_acq_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
	            else
	               dw_tes_off_acq_det_2.modify("st_indirizzo.text=''")
	            end if
	            if not isnull(ls_frazione) then
	               dw_tes_off_acq_det_2.modify("st_frazione.text='" + ls_frazione + "'")
	            else
	               dw_tes_off_acq_det_2.modify("st_frazione.text=''")
	            end if
	            if not isnull(ls_cap) then
	               dw_tes_off_acq_det_2.modify("st_cap.text='" + ls_cap + "'")
	            else
	               dw_tes_off_acq_det_2.modify("st_cap.text=''")
	            end if
	            if not isnull(ls_localita) then
	               dw_tes_off_acq_det_2.modify("st_localita.text='" + ls_localita + "'")
	            else
	               dw_tes_off_acq_det_2.modify("st_localita.text=''")
	            end if
	            if not isnull(ls_provincia) then
	               dw_tes_off_acq_det_2.modify("st_provincia.text='" + ls_provincia + "'")
	            else
	               dw_tes_off_acq_det_2.modify("st_provincia.text=''")
	            end if
	            if not isnull(ls_telefono) then
	               dw_tes_off_acq_det_2.modify("st_telefono.text='" + ls_telefono + "'")
	            else
	               dw_tes_off_acq_det_2.modify("st_telefono.text=''")
	            end if
	            if not isnull(ls_fax) then
	               dw_tes_off_acq_det_2.modify("st_fax.text='" + ls_fax + "'")
	            else
	               dw_tes_off_acq_det_2.modify("st_fax.text=''")
	            end if
	            if not isnull(ls_telex) then
	               dw_tes_off_acq_det_2.modify("st_telex.text='" + ls_telex + "'")
	            else
	               dw_tes_off_acq_det_2.modify("st_telex.text=''")
	            end if
	         else
   		      select anag_for_pot.rag_soc_1,
	      	          anag_for_pot.rag_soc_2,
		                anag_for_pot.indirizzo,
		                anag_for_pot.frazione,
		                anag_for_pot.cap,
		                anag_for_pot.localita,
		                anag_for_pot.provincia,
		                anag_for_pot.telefono,
		                anag_for_pot.fax,
		                anag_for_pot.telex
		         into   :ls_rag_soc_1,    
		                :ls_rag_soc_2,
		                :ls_indirizzo,    
		                :ls_frazione,
		                :ls_cap,
		                :ls_localita,
		                :ls_provincia,
		                :ls_telefono,
		                :ls_fax,
		                :ls_telex
		         from   anag_for_pot
		         where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
		                anag_for_pot.cod_for_pot = :ls_cod_for_pot;
    
		         if sqlca.sqlcode = 0 then 
		            if not isnull(ls_rag_soc_1) then
		               dw_tes_off_acq_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
		            else
		               dw_tes_off_acq_det_2.modify("st_rag_soc_1.text=''")
		            end if
		            if not isnull(ls_rag_soc_2) then
		               dw_tes_off_acq_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
		            else
		               dw_tes_off_acq_det_2.modify("st_rag_soc_2.text=''")
		            end if
		            if not isnull(ls_indirizzo) then
		               dw_tes_off_acq_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
		            else
		               dw_tes_off_acq_det_2.modify("st_indirizzo.text=''")
		            end if
		            if not isnull(ls_frazione) then
		               dw_tes_off_acq_det_2.modify("st_frazione.text='" + ls_frazione + "'")
		            else
		               dw_tes_off_acq_det_2.modify("st_frazione.text=''")
		            end if
		            if not isnull(ls_cap) then
		               dw_tes_off_acq_det_2.modify("st_cap.text='" + ls_cap + "'")
		            else
		               dw_tes_off_acq_det_2.modify("st_cap.text=''")
		            end if
		            if not isnull(ls_localita) then
		               dw_tes_off_acq_det_2.modify("st_localita.text='" + ls_localita + "'")
		            else
		               dw_tes_off_acq_det_2.modify("st_localita.text=''")
		            end if
		            if not isnull(ls_provincia) then
		               dw_tes_off_acq_det_2.modify("st_provincia.text='" + ls_provincia + "'")
		            else
		               dw_tes_off_acq_det_2.modify("st_provincia.text=''")
		            end if
		            if not isnull(ls_telefono) then
		               dw_tes_off_acq_det_2.modify("st_telefono.text='" + ls_telefono + "'")
		            else
		               dw_tes_off_acq_det_2.modify("st_telefono.text=''")
		            end if
		            if not isnull(ls_fax) then
		               dw_tes_off_acq_det_2.modify("st_fax.text='" + ls_fax + "'")
		            else
		               dw_tes_off_acq_det_2.modify("st_fax.text=''")
		            end if
		            if not isnull(ls_telex) then
		               dw_tes_off_acq_det_2.modify("st_telex.text='" + ls_telex + "'")
		            else
		               dw_tes_off_acq_det_2.modify("st_telex.text=''")
		            end if
		         else
		            dw_tes_off_acq_det_2.modify("st_rag_soc_1.text=''")
		            dw_tes_off_acq_det_2.modify("st_rag_soc_2.text=''")
		            dw_tes_off_acq_det_2.modify("st_indirizzo.text=''")
		            dw_tes_off_acq_det_2.modify("st_frazione.text=''")
		            dw_tes_off_acq_det_2.modify("st_cap.text=''")
		            dw_tes_off_acq_det_2.modify("st_localita.text=''")
		            dw_tes_off_acq_det_2.modify("st_provincia.text=''")
		            dw_tes_off_acq_det_2.modify("st_telefono.text=''")
		            dw_tes_off_acq_det_2.modify("st_fax.text=''")
		            dw_tes_off_acq_det_2.modify("st_telex.text=''")
		         end if 
				end if 
         end if
      case "cod_des_fornitore"
         ls_cod_fornitore = this.getitemstring(i_rownbr, "cod_fornitore")
         select anag_des_fornitori.rag_soc_1,
                anag_des_fornitori.rag_soc_2,
                anag_des_fornitori.indirizzo,
                anag_des_fornitori.frazione,
                anag_des_fornitori.cap,
                anag_des_fornitori.localita,
                anag_des_fornitori.provincia
         into   :ls_rag_soc_1,    
                :ls_rag_soc_2,
                :ls_indirizzo,    
                :ls_frazione,
                :ls_cap,
                :ls_localita,
                :ls_provincia
         from   anag_des_fornitori
         where  anag_des_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_des_fornitori.cod_fornitore = :ls_cod_fornitore and
                anag_des_fornitori.cod_des_fornitore = :i_coltext;
   
         if sqlca.sqlcode = 0 then
            dw_tes_off_acq_det_2.setitem(i_rownbr, "rag_soc_1", ls_rag_soc_1)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "rag_soc_2", ls_rag_soc_2)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "indirizzo", ls_indirizzo)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "frazione", ls_frazione)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "cap", ls_cap)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "localita", ls_localita)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "provincia", ls_provincia)
         else
            dw_tes_off_acq_det_2.setitem(i_rownbr, "rag_soc_1", ls_null)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "rag_soc_2", ls_null)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "indirizzo", ls_null)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "frazione", ls_null)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "cap", ls_null)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "localita", ls_null)
            dw_tes_off_acq_det_2.setitem(i_rownbr, "provincia", ls_null)
         end if
      case "cod_valuta"
         ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
         f_cambio_acq(i_coltext, ldt_data_registrazione)
   end choose
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	dw_tes_off_acq_det_1.object.b_ricerca_banca_for.enabled = false
	dw_tes_off_acq_det_1.object.b_ricerca_fornitore.enabled = false
	dw_tes_off_acq_det_1.object.b_ricerca_for_pot.enabled = false
	ib_new = false
   if this.getrow() > 0 and this.getitemnumber(this.getrow(), "num_registrazione") > 0 then
      cb_dettaglio.enabled = true
      cb_stampa.enabled = true
	cb_blocca.enabled=true
		cb_sblocca.enabled=true
		cb_duplica_doc.enabled=true
		cb_note.enabled=true
		cb_corrispondenze.enabled=true
	else
      cb_dettaglio.enabled = false
      cb_stampa.enabled = false
		cb_blocca.enabled=false
		cb_sblocca.enabled=false
		cb_duplica_doc.enabled=false
		cb_note.enabled=false
		cb_corrispondenze.enabled=false
   end if
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
   string  ls_tabella, ls_tot_documento, ls_nome_prog, ls_cod_fornitore, &
			  ls_nota_testata, ls_nota_piede, ls_nota_old, ls_null, ls_nota_video
   long    ll_anno_registrazione, ll_num_registrazione, ll_i
	datetime ldt_data_registrazione

	
   for ll_i = 1 to this.deletedcount()
      ll_anno_registrazione = this.getitemnumber(ll_i, "anno_registrazione", delete!, true)
      ll_num_registrazione = this.getitemnumber(ll_i, "num_registrazione", delete!, true)

		ls_tabella = "det_off_acq_stat"
		ls_nome_prog = ""

      if f_elimina_det_stat(ls_tabella, ll_anno_registrazione, ll_num_registrazione,ls_nome_prog,0) = -1 then
			return 1
		end if

      ls_tabella = "det_off_acq"

      if f_del_det(ls_tabella, ll_anno_registrazione, ll_num_registrazione) = -1 then
			return 1
		end if
	next

   if this.getrow() > 0 then
      ll_anno_registrazione = this.getitemnumber(this.getrow(), "anno_registrazione")
      ll_num_registrazione = this.getitemnumber(this.getrow(), "num_registrazione")
      if ll_anno_registrazione <> 0 then
         ls_tabella = "det_off_acq"

			if dw_tes_off_acq_det_1.getitemstring(dw_tes_off_acq_det_1.getrow(), "flag_evasione") <> "A" then
				g_mb.messagebox("Attenzione", "Offerta non modificabile! Ha già subito un'evasione.", &
							  exclamation!, ok!)
				dw_tes_off_acq_det_1.set_dw_view(c_ignorechanges)
				pcca.error = c_fatal
				return 1
			end if
			
			if dw_tes_off_acq_det_1.getitemstring(dw_tes_off_acq_det_1.getrow(), "flag_blocco") = "S" then
				g_mb.messagebox("Attenzione", "Offerta non modificabile! E' stata bloccata.", &
							  exclamation!, ok!)
				dw_tes_off_acq_det_1.set_dw_view(c_ignorechanges)
				pcca.error = c_fatal
				return 1
			end if
         
			ls_tot_documento = "tot_val_offerta"
			
			if ib_new then
				setnull(ls_null)
				ls_cod_fornitore = this.getitemstring(this.getrow(), "cod_fornitore")
				ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
				
				if guo_functions.uof_get_note_fisse(ls_null, ls_cod_fornitore, ls_null, "OFF_ACQ", ls_null, ldt_data_registrazione, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
					g_mb.error(ls_nota_testata)
				else
					ls_nota_old = this.getitemstring(this.getrow(), "nota_testata")
					if isnull(ls_nota_old) then ls_nota_old = ""
					this.setitem(this.getrow(), "nota_testata", ls_nota_testata + ls_nota_old )
					
					ls_nota_old = this.getitemstring(this.getrow(), "nota_piede")
					if isnull(ls_nota_old) then ls_nota_old = ""
					this.setitem(this.getrow(), "nota_piede", ls_nota_piede + ls_nota_old )
				end if
			end if
      end if
   end if
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
   string ls_cod_fornitore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, &
          ls_cap, ls_localita, ls_provincia, ls_telefono, ls_fax, ls_telex, &
          ls_partita_iva, ls_cod_fiscale, ls_cod_for_pot, ls_cod_fil_fornitore


   if this.getrow() > 0 then
      ls_cod_fornitore = this.getitemstring(this.getrow(), "cod_fornitore")
      ls_cod_for_pot = this.getitemstring(this.getrow(), "cod_for_pot")
      ls_cod_fil_fornitore = this.getitemstring(this.getrow(), "cod_fil_fornitore")

      select anag_fornitori.rag_soc_1,
             anag_fornitori.rag_soc_2,
             anag_fornitori.indirizzo,
             anag_fornitori.frazione,
             anag_fornitori.cap,
             anag_fornitori.localita,
             anag_fornitori.provincia,
             anag_fornitori.telefono,
             anag_fornitori.fax,
             anag_fornitori.telex,
             anag_fornitori.partita_iva,
             anag_fornitori.cod_fiscale
      into   :ls_rag_soc_1,    
             :ls_rag_soc_2,
             :ls_indirizzo,    
             :ls_frazione,
             :ls_cap,
             :ls_localita,
             :ls_provincia,
             :ls_telefono,
             :ls_fax,
             :ls_telex,
             :ls_partita_iva,
             :ls_cod_fiscale
      from   anag_fornitori
      where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
             anag_fornitori.cod_fornitore = :ls_cod_fornitore;

      if sqlca.sqlcode = 0 then
         if not isnull(ls_cod_fornitore) then
            dw_tes_off_acq_det_2.modify("st_cod_fornitore.text='" + ls_cod_fornitore + "'")
         else
            ls_cod_fornitore = ""
            dw_tes_off_acq_det_2.modify("st_cod_fornitore.text=''")
         end if
         if not isnull(ls_rag_soc_1) then
            dw_tes_off_acq_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
         else
            dw_tes_off_acq_det_2.modify("st_rag_soc_1.text=''")
         end if
         if not isnull(ls_rag_soc_2) then
            dw_tes_off_acq_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
         else
            dw_tes_off_acq_det_2.modify("st_rag_soc_2.text=''")
         end if
         if not isnull(ls_indirizzo) then
            dw_tes_off_acq_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
         else
            dw_tes_off_acq_det_2.modify("st_indirizzo.text=''")
         end if
         if not isnull(ls_frazione) then
            dw_tes_off_acq_det_2.modify("st_frazione.text='" + ls_frazione + "'")
         else
            dw_tes_off_acq_det_2.modify("st_frazione.text=''")
         end if
         if not isnull(ls_cap) then
            dw_tes_off_acq_det_2.modify("st_cap.text='" + ls_cap + "'")
         else
            dw_tes_off_acq_det_2.modify("st_cap.text=''")
         end if
         if not isnull(ls_localita) then
            dw_tes_off_acq_det_2.modify("st_localita.text='" + ls_localita + "'")
         else
            dw_tes_off_acq_det_2.modify("st_localita.text=''")
         end if
         if not isnull(ls_provincia) then
            dw_tes_off_acq_det_2.modify("st_provincia.text='" + ls_provincia + "'")
         else
            dw_tes_off_acq_det_2.modify("st_provincia.text=''")
         end if
         if not isnull(ls_telefono) then
            dw_tes_off_acq_det_2.modify("st_telefono.text='" + ls_telefono + "'")
         else
            dw_tes_off_acq_det_2.modify("st_telefono.text=''")
         end if
         if not isnull(ls_fax) then
            dw_tes_off_acq_det_2.modify("st_fax.text='" + ls_fax + "'")
         else
            dw_tes_off_acq_det_2.modify("st_fax.text=''")
         end if
         if not isnull(ls_telex) then
            dw_tes_off_acq_det_2.modify("st_telex.text='" + ls_telex + "'")
         else
            dw_tes_off_acq_det_2.modify("st_telex.text=''")
            end if
         if not isnull(ls_partita_iva) then
            dw_tes_off_acq_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
         else
            dw_tes_off_acq_det_2.modify("st_partita_iva.text=''")
         end if
         if not isnull(ls_cod_fiscale) then
            dw_tes_off_acq_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
         else
            dw_tes_off_acq_det_2.modify("st_cod_fiscale.text=''")
         end if

         f_po_loaddddw_dw(this, &
                          "cod_des_fornitore", &
                          sqlca, &
                          "anag_des_fornitori", &
                          "cod_des_fornitore", &
                          "rag_soc_1", &
                          "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + ls_cod_fornitore + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

         f_po_loaddddw_dw(this, &
                          "cod_fil_fornitore", &
                          sqlca, &
                          "anag_fil_fornitori", &
                          "cod_fil_fornitore", &
                          "rag_soc_1", &
                          "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + ls_cod_fornitore + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

         select anag_for_pot.cod_for_pot
         into   :ls_cod_for_pot
         from   anag_for_pot
         where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_for_pot.cod_fornitore = :ls_cod_fornitore;
   
         if sqlca.sqlcode = 0 then 
            dw_tes_off_acq_det_2.modify("st_cod_for_pot.text='" + ls_cod_for_pot + "'")
         else
            dw_tes_off_acq_det_2.modify("st_cod_for_pot.text=''")
         end if         

         select anag_fil_fornitori.rag_soc_1,
                anag_fil_fornitori.rag_soc_2,
                anag_fil_fornitori.indirizzo,
                anag_fil_fornitori.frazione,
                anag_fil_fornitori.cap,
                anag_fil_fornitori.localita,
                anag_fil_fornitori.provincia,
                anag_fil_fornitori.telefono,
                anag_fil_fornitori.fax,
                anag_fil_fornitori.telex
         into   :ls_rag_soc_1,    
                :ls_rag_soc_2,
                :ls_indirizzo,    
                :ls_frazione,
                :ls_cap,
                :ls_localita,
                :ls_provincia,
                :ls_telefono,
                :ls_fax,
                :ls_telex
         from   anag_fil_fornitori
         where  anag_fil_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_fil_fornitori.cod_fornitore = :ls_cod_fornitore and 
					 anag_fil_fornitori.cod_fil_fornitore = :ls_cod_fil_fornitore;
 
         if sqlca.sqlcode = 0 then 
            if not isnull(ls_rag_soc_1) then
               dw_tes_off_acq_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
            else
               dw_tes_off_acq_det_2.modify("st_rag_soc_1.text=''")
            end if
            if not isnull(ls_rag_soc_2) then
               dw_tes_off_acq_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
            else
               dw_tes_off_acq_det_2.modify("st_rag_soc_2.text=''")
            end if
            if not isnull(ls_indirizzo) then
               dw_tes_off_acq_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
            else
               dw_tes_off_acq_det_2.modify("st_indirizzo.text=''")
            end if
            if not isnull(ls_frazione) then
               dw_tes_off_acq_det_2.modify("st_frazione.text='" + ls_frazione + "'")
            else
               dw_tes_off_acq_det_2.modify("st_frazione.text=''")
            end if
            if not isnull(ls_cap) then
               dw_tes_off_acq_det_2.modify("st_cap.text='" + ls_cap + "'")
            else
               dw_tes_off_acq_det_2.modify("st_cap.text=''")
            end if
            if not isnull(ls_localita) then
               dw_tes_off_acq_det_2.modify("st_localita.text='" + ls_localita + "'")
            else
               dw_tes_off_acq_det_2.modify("st_localita.text=''")
            end if
            if not isnull(ls_provincia) then
               dw_tes_off_acq_det_2.modify("st_provincia.text='" + ls_provincia + "'")
            else
               dw_tes_off_acq_det_2.modify("st_provincia.text=''")
            end if
            if not isnull(ls_telefono) then
               dw_tes_off_acq_det_2.modify("st_telefono.text='" + ls_telefono + "'")
            else
               dw_tes_off_acq_det_2.modify("st_telefono.text=''")
            end if
            if not isnull(ls_fax) then
               dw_tes_off_acq_det_2.modify("st_fax.text='" + ls_fax + "'")
            else
               dw_tes_off_acq_det_2.modify("st_fax.text=''")
            end if
            if not isnull(ls_telex) then
               dw_tes_off_acq_det_2.modify("st_telex.text='" + ls_telex + "'")
            else
               dw_tes_off_acq_det_2.modify("st_telex.text=''")
            end if
         end if
		else
         select anag_for_pot.rag_soc_1,
                anag_for_pot.rag_soc_2,
                anag_for_pot.indirizzo,
                anag_for_pot.frazione,
                anag_for_pot.cap,
                anag_for_pot.localita,
                anag_for_pot.provincia,
                anag_for_pot.telefono,
                anag_for_pot.fax,
                anag_for_pot.telex,
                anag_for_pot.partita_iva,
                anag_for_pot.cod_fiscale
         into   :ls_rag_soc_1,    
                :ls_rag_soc_2,
                :ls_indirizzo,    
                :ls_frazione,
                :ls_cap,
                :ls_localita,
                :ls_provincia,
                :ls_telefono,
                :ls_fax,
                :ls_telex,
                :ls_partita_iva,
                :ls_cod_fiscale
         from   anag_for_pot
         where  anag_for_pot.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_for_pot.cod_for_pot = :ls_cod_for_pot;

         dw_tes_off_acq_det_2.modify("st_cod_for_pot.text=''")

         if sqlca.sqlcode = 0 then
            if not isnull(ls_cod_for_pot) then
               dw_tes_off_acq_det_2.modify("st_cod_for_pot.text='" + ls_cod_for_pot + "'")
            else
               dw_tes_off_acq_det_2.modify("st_cod_for_pot.text=''")
            end if
            if not isnull(ls_rag_soc_1) then
               dw_tes_off_acq_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
            else
               dw_tes_off_acq_det_2.modify("st_rag_soc_1.text=''")
            end if
            if not isnull(ls_rag_soc_2) then
               dw_tes_off_acq_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
            else
               dw_tes_off_acq_det_2.modify("st_rag_soc_2.text=''")
            end if
            if not isnull(ls_indirizzo) then
               dw_tes_off_acq_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
            else
               dw_tes_off_acq_det_2.modify("st_indirizzo.text=''")
            end if
            if not isnull(ls_frazione) then
               dw_tes_off_acq_det_2.modify("st_frazione.text='" + ls_frazione + "'")
            else
               dw_tes_off_acq_det_2.modify("st_frazione.text=''")
            end if
            if not isnull(ls_cap) then
               dw_tes_off_acq_det_2.modify("st_cap.text='" + ls_cap + "'")
            else
               dw_tes_off_acq_det_2.modify("st_cap.text=''")
            end if
            if not isnull(ls_localita) then
               dw_tes_off_acq_det_2.modify("st_localita.text='" + ls_localita + "'")
            else
               dw_tes_off_acq_det_2.modify("st_localita.text=''")
            end if
            if not isnull(ls_provincia) then
               dw_tes_off_acq_det_2.modify("st_provincia.text='" + ls_provincia + "'")
            else
               dw_tes_off_acq_det_2.modify("st_provincia.text=''")
            end if
            if not isnull(ls_telefono) then
               dw_tes_off_acq_det_2.modify("st_telefono.text='" + ls_telefono + "'")
            else
               dw_tes_off_acq_det_2.modify("st_telefono.text=''")
            end if
            if not isnull(ls_fax) then
               dw_tes_off_acq_det_2.modify("st_fax.text='" + ls_fax + "'")
            else
               dw_tes_off_acq_det_2.modify("st_fax.text=''")
            end if
            if not isnull(ls_telex) then
               dw_tes_off_acq_det_2.modify("st_telex.text='" + ls_telex + "'")
            else
               dw_tes_off_acq_det_2.modify("st_telex.text=''")
               end if
            if not isnull(ls_partita_iva) then
               dw_tes_off_acq_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
            else
               dw_tes_off_acq_det_2.modify("st_partita_iva.text=''")
            end if
            if not isnull(ls_cod_fiscale) then
               dw_tes_off_acq_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
            else
               dw_tes_off_acq_det_2.modify("st_cod_fiscale.text=''")
            end if
         else
            dw_tes_off_acq_det_2.modify("st_cod_fornitore.text=''")
            dw_tes_off_acq_det_2.modify("st_cod_for_pot.text=''")
            dw_tes_off_acq_det_2.modify("st_rag_soc_1.text=''")
            dw_tes_off_acq_det_2.modify("st_rag_soc_2.text=''")
            dw_tes_off_acq_det_2.modify("st_indirizzo.text=''")
            dw_tes_off_acq_det_2.modify("st_frazione.text=''")
            dw_tes_off_acq_det_2.modify("st_cap.text=''")
            dw_tes_off_acq_det_2.modify("st_localita.text=''")
            dw_tes_off_acq_det_2.modify("st_provincia.text=''")
            dw_tes_off_acq_det_2.modify("st_telefono.text=''")
            dw_tes_off_acq_det_2.modify("st_fax.text=''")
            dw_tes_off_acq_det_2.modify("st_telex.text=''")
            dw_tes_off_acq_det_2.modify("st_partita_iva.text=''")
            dw_tes_off_acq_det_2.modify("st_cod_fiscale.text=''")
         end if
      end if
   end if
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_cod_tipo_off_acq, ls_cod_operatore, ls_stringa, ls_cod_deposito, ls_errore

   select con_off_acq.cod_tipo_off_acq
   into   :ls_cod_tipo_off_acq
   from   con_off_acq
   where  con_off_acq.cod_azienda = :s_cs_xx.cod_azienda;

   if sqlca.sqlcode = 0 then
      this.setitem(this.getrow(), "cod_tipo_off_acq", ls_cod_tipo_off_acq)
		this.setitem(this.getrow(), "data_registrazione", datetime(today()))
	end if
	
	select cod_operatore
	  into :ls_cod_operatore
	  from tab_operatori_utenti
	 where cod_azienda = :s_cs_xx.cod_azienda and
	 		 cod_utente = :s_cs_xx.cod_utente and
			 flag_default = 'S';
			 
   if sqlca.sqlcode = -1 then
		g_mb.messagebox("Estrazione Operatori", "Errore durante l'Estrazione dell'Operatore di Default")
		pcca.error = c_fatal
		return
	elseif sqlca.sqlcode = 0 then
      this.setitem(this.getrow(), "cod_operatore", ls_cod_operatore)
	end if
	
	// stefanop: 21/12/2011: aggiunto deposito collegato all'operatore
	guo_functions.uof_get_stabilimento_utente(ls_cod_deposito, ls_errore)
	if not isnull(ls_cod_deposito) then setitem(getrow(), "cod_deposito", ls_cod_deposito)
	// ----

	// banca per RIBA - richiesto da colombin / meco, ma utile per tutti
	select stringa
	into   :ls_stringa
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'BA1';
	if sqlca.sqlcode = 0 then 
		this.setitem(this.getrow(), "cod_banca_clien_for", ls_stringa)
	end if
			 
	// banca per B.B. - richiesto da colombin / meco, ma utile per tutti
	select stringa
	into   :ls_stringa
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_parametro = 'BA2';
	if sqlca.sqlcode = 0 then 
		this.setitem(this.getrow(), "cod_banca", ls_stringa)
	end if
			 
	dw_tes_off_acq_det_1.object.b_ricerca_banca_for.enabled = true
	dw_tes_off_acq_det_1.object.b_ricerca_fornitore.enabled = true
	dw_tes_off_acq_det_1.object.b_ricerca_for_pot.enabled = true
	cb_dettaglio.enabled = false
	cb_stampa.enabled = false
	cb_blocca.enabled=false
	cb_sblocca.enabled=false
	cb_duplica_doc.enabled=false
	cb_note.enabled=false
	cb_corrispondenze.enabled=false
	ib_new = true
	
	dw_tes_off_acq_det_4.setitem(dw_tes_off_acq_det_1.getrow(), "flag_doc_suc_tes", "I")
	dw_tes_off_acq_det_4.setitem(dw_tes_off_acq_det_1.getrow(), "flag_st_note_tes", "I")
	dw_tes_off_acq_det_4.setitem(dw_tes_off_acq_det_1.getrow(), "flag_doc_suc_pie", "I")
	dw_tes_off_acq_det_4.setitem(dw_tes_off_acq_det_1.getrow(), "flag_st_note_pie	", "I")
end if
end event

event pcd_validaterow;call uo_cs_xx_dw::pcd_validaterow;if i_insave > 0 then
   if this.getrow() > 0 then
      if isnull(this.getitemstring(this.getrow(), "cod_for_pot")) and &
         isnull(this.getitemstring(this.getrow(), "cod_fornitore")) then
         g_mb.messagebox("Attenzione", "Inserire Codice Fornitore Potenziale o Codice Fornitore.", &
                    exclamation!, ok!)
         pcca.error = c_fatal
         return
      end if
   end if
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	if pcca.error = c_success then
		dw_tes_off_acq_det_1.object.b_ricerca_fornitore.enabled = false
		dw_tes_off_acq_det_1.object.b_ricerca_banca_for.enabled = false
		ib_new = false
		
		if this.getrow() > 0 and this.getitemnumber(this.getrow(), "num_registrazione") > 0 then
			cb_dettaglio.enabled = true
			cb_stampa.enabled = true
			cb_blocca.enabled=true
			cb_sblocca.enabled=true
			cb_duplica_doc.enabled=true
			cb_note.enabled=true
			cb_corrispondenze.enabled=true
		else
			cb_dettaglio.enabled = false
			cb_stampa.enabled = false
			cb_blocca.enabled=false
			cb_sblocca.enabled=false
			cb_duplica_doc.enabled=false
			cb_note.enabled=false
			cb_corrispondenze.enabled=false
		end if
	end if
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	dw_tes_off_acq_det_1.object.b_ricerca_banca_for.enabled = true
	dw_tes_off_acq_det_1.object.b_ricerca_fornitore.enabled = true
	dw_tes_off_acq_det_1.object.b_ricerca_for_pot.enabled = true
	cb_dettaglio.enabled = false
	cb_stampa.enabled = false
	cb_blocca.enabled=false
	cb_sblocca.enabled=false
	cb_duplica_doc.enabled=false
	cb_note.enabled=false
	cb_corrispondenze.enabled=false
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_max_registrazione


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if this.getitemnumber(ll_i, "anno_registrazione") = 0 or &
      isnull(this.getitemnumber(ll_i, "anno_registrazione")) then
		ll_anno_registrazione = f_anno_esercizio()
      if ll_anno_registrazione > 0 then
         this.setitem(this.getrow(), "anno_registrazione", int(ll_anno_registrazione))
      else
			g_mb.messagebox("Offerte Fornitori","Impostare l'anno di esercizio in parametri aziendali")
		end if
      select con_off_acq.num_registrazione
      into   :ll_num_registrazione
      from   con_off_acq
      where  con_off_acq.cod_azienda = :s_cs_xx.cod_azienda;
		
		choose case sqlca.sqlcode
			case 0
				ll_num_registrazione ++
			case 100
				g_mb.messagebox("Offerte di acquisto","Errore in assegnazione numero offerta: verificare di aver impostato i parametri offerte di vendita. ~r~nIl salvataggio verrà effettuato ugualmente")
			case else
				g_mb.messagebox("Offerte di acquisto","Errore in assegnazione numero offerta.~r~nDettaglio errore "+ sqlca.sqlerrtext +"~r~nIl salvataggio verrà effettuato ugualmente")
		end choose
		
		ll_max_registrazione = 0
		select max(num_registrazione)
		into   :ll_max_registrazione
		from   tes_off_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione;
		if not isnull(ll_max_registrazione) then
			if ll_max_registrazione >= ll_num_registrazione then ll_num_registrazione = ll_max_registrazione + 1
		end if
		
      this.setitem(this.getrow(), "num_registrazione", ll_num_registrazione)
      update con_off_acq
      set    con_off_acq.num_registrazione = :ll_num_registrazione
      where  con_off_acq.cod_azienda = :s_cs_xx.cod_azienda;
    end if
next      
end event

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_for_pot"
		guo_ricerca.uof_ricerca_fornitore_potenziale(dw_tes_off_acq_det_1,"cod_for_pot")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_tes_off_acq_det_1,"cod_fornitore")
case "b_ricerca_banca_for"
		guo_ricerca.uof_ricerca_banca_clifor(dw_tes_off_acq_det_1,"cod_banca_clien_for")
end choose
end event

type dw_tes_off_acq_det_2 from uo_cs_xx_dw within w_tes_off_acq_det
integer x = 41
integer y = 116
integer width = 2921
integer height = 1420
integer taborder = 50
string dataobject = "d_tes_off_acq_det_2"
boolean border = false
end type

type dw_folder from u_folder within w_tes_off_acq_det
integer x = 18
integer y = 20
integer width = 3081
integer height = 1540
integer taborder = 0
end type

type dw_tes_off_acq_det_4 from uo_cs_xx_dw within w_tes_off_acq_det
integer x = 41
integer y = 116
integer width = 3013
integer height = 1180
integer taborder = 70
string dataobject = "d_tes_off_acq_det_4"
boolean border = false
end type

event ue_key;call super::ue_key;string ls_nota_fissa

choose case this.getcolumnname()
	
	case "nota_testata"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_1 = "%"
			s_cs_xx.parametri.parametro_s_2 = dw_tes_off_acq_det_1.getitemstring(dw_tes_off_acq_det_1.getrow(),"cod_fornitore")
			s_cs_xx.parametri.parametro_s_3 = "%"
			s_cs_xx.parametri.parametro_s_4 = "%"
			s_cs_xx.parametri.parametro_s_5 = "%"
			s_cs_xx.parametri.parametro_s_6 = "%"
			s_cs_xx.parametri.parametro_s_7 = "S"
			s_cs_xx.parametri.parametro_s_8 = "%"
			s_cs_xx.parametri.parametro_s_9 = "T"
			s_cs_xx.parametri.parametro_data_1 = dw_tes_off_acq_det_1.getitemdatetime(dw_tes_off_acq_det_1.getrow(),"data_registrazione")
			
			
			dw_tes_off_acq_det_4.change_dw_current()
			setnull(s_cs_xx.parametri.parametro_s_11)
			window_open(w_ricerca_note_fisse, 0)
			if not isnull(s_cs_xx.parametri.parametro_s_11) then
				
				select nota_fissa
				  into :ls_nota_fissa
				  from tab_note_fisse
				 where cod_azienda = :s_cs_xx.cod_azienda
				   and cod_nota_fissa = :s_cs_xx.parametri.parametro_s_11;
				
				if not isnull(ls_nota_fissa) then
					if len(this.gettext()) > 0 then
						this.setcolumn("nota_testata")
						this.settext(this.gettext() + "~r~n" + ls_nota_fissa)
					else
						this.setcolumn("nota_testata")
						this.settext(ls_nota_fissa)
					end if	
				end if	
			end if
		end if
		
	case "nota_piede"
		if key = keyF1!  and keyflags = 1 then		
			s_cs_xx.parametri.parametro_s_1 = "%"
			s_cs_xx.parametri.parametro_s_2 = dw_tes_off_acq_det_1.getitemstring(dw_tes_off_acq_det_1.getrow(),"cod_fornitore")
			s_cs_xx.parametri.parametro_s_3 = "%"
			s_cs_xx.parametri.parametro_s_4 = "%"
			s_cs_xx.parametri.parametro_s_5 = "%"
			s_cs_xx.parametri.parametro_s_6 = "%"
			s_cs_xx.parametri.parametro_s_7 = "S"
			s_cs_xx.parametri.parametro_s_8 = "%"
			s_cs_xx.parametri.parametro_s_9 = "P"
			s_cs_xx.parametri.parametro_data_1 = dw_tes_off_acq_det_1.getitemdatetime(dw_tes_off_acq_det_1.getrow(),"data_registrazione")
			
			
			dw_tes_off_acq_det_4.change_dw_current()
			setnull(s_cs_xx.parametri.parametro_s_11)
			window_open(w_ricerca_note_fisse, 0)
			if not isnull(s_cs_xx.parametri.parametro_s_11) then
				
				select nota_fissa
				  into :ls_nota_fissa
				  from tab_note_fisse
				 where cod_azienda = :s_cs_xx.cod_azienda
				   and cod_nota_fissa = :s_cs_xx.parametri.parametro_s_11;
				
				if not isnull(ls_nota_fissa) then	
					if len(this.gettext()) > 0 then					
						this.setcolumn("nota_piede")
						this.settext(this.gettext() + "~r~n" + ls_nota_fissa)
					else	
						this.setcolumn("nota_piede")
						this.settext(ls_nota_fissa)
					end if	
				end if	
			end if
		end if	
		
end choose

end event

type dw_tes_off_acq_det_3 from uo_cs_xx_dw within w_tes_off_acq_det
integer x = 41
integer y = 116
integer width = 1920
integer height = 560
integer taborder = 60
string dataobject = "d_tes_off_acq_det_3"
boolean border = false
end type

