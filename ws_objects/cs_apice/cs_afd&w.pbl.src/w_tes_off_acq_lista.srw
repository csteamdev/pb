﻿$PBExportHeader$w_tes_off_acq_lista.srw
$PBExportComments$Finestra Testata Offerte Acquisti Lista
forward
global type w_tes_off_acq_lista from w_cs_xx_principale
end type
type sle_filtro_data from u_sle_search within w_tes_off_acq_lista
end type
type st_filtro_data from statictext within w_tes_off_acq_lista
end type
type cb_offerta from commandbutton within w_tes_off_acq_lista
end type
type cb_ricerca from commandbutton within w_tes_off_acq_lista
end type
type cb_annulla from commandbutton within w_tes_off_acq_lista
end type
type dw_tes_off_acq_ricerca from u_dw_search within w_tes_off_acq_lista
end type
type dw_tes_off_acq_lista from uo_cs_xx_dw within w_tes_off_acq_lista
end type
type dw_folder from u_folder within w_tes_off_acq_lista
end type
end forward

global type w_tes_off_acq_lista from w_cs_xx_principale
integer width = 2377
integer height = 1388
string title = "Lista Offerte Acquisti"
sle_filtro_data sle_filtro_data
st_filtro_data st_filtro_data
cb_offerta cb_offerta
cb_ricerca cb_ricerca
cb_annulla cb_annulla
dw_tes_off_acq_ricerca dw_tes_off_acq_ricerca
dw_tes_off_acq_lista dw_tes_off_acq_lista
dw_folder dw_folder
end type
global w_tes_off_acq_lista w_tes_off_acq_lista

type variables
boolean ib_apertura = false


end variables

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[],l_objects[]

dw_tes_off_acq_lista.set_dw_key("cod_azienda")
dw_tes_off_acq_lista.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_noretrieveonopen, &
                                    c_default)
												
												
lw_oggetti[1] = dw_tes_off_acq_lista
lw_oggetti[2] = cb_offerta
dw_folder.fu_assigntab(2, "Lista Offerte", lw_oggetti[])
lw_oggetti[1] = dw_tes_off_acq_ricerca
lw_oggetti[2] = cb_ricerca
lw_oggetti[3] = cb_annulla
lw_oggetti[4] = sle_filtro_data
lw_oggetti[5] = st_filtro_data
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])
dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)



l_criteriacolumn[1] = "anno_registrazione"
l_criteriacolumn[2] = "num_registrazione"
l_criteriacolumn[3] = "cod_fornitore"
l_criteriacolumn[4] = "cod_for_pot"
l_criteriacolumn[5] = "cod_tipo_off_acq"
l_criteriacolumn[6] = "flag_evasione"

l_searchtable[1] = "tes_off_acq"
l_searchtable[2] = "tes_off_acq"
l_searchtable[3] = "tes_off_acq"
l_searchtable[4] = "tes_off_acq"
l_searchtable[5] = "tes_off_acq"
l_searchtable[6] = "tes_off_acq"

l_searchcolumn[1] = "anno_registrazione"
l_searchcolumn[2] = "num_registrazione"
l_searchcolumn[3] = "cod_fornitore"
l_searchcolumn[4] = "cod_for_pot"
l_searchcolumn[5] = "cod_tipo_off_acq"
l_searchcolumn[6] = "flag_evasione"

dw_tes_off_acq_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_tes_off_acq_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)

dw_tes_off_acq_ricerca.setitem(1, "anno_registrazione", f_anno_esercizio())
dw_tes_off_acq_ricerca.setitem(1, "flag_evasione", "A")												

sle_filtro_data.fu_WireDW(dw_tes_off_acq_lista, &
    "tes_off_acq", &
    "data_registrazione", &
    SQLCA)
end event

on w_tes_off_acq_lista.create
int iCurrent
call super::create
this.sle_filtro_data=create sle_filtro_data
this.st_filtro_data=create st_filtro_data
this.cb_offerta=create cb_offerta
this.cb_ricerca=create cb_ricerca
this.cb_annulla=create cb_annulla
this.dw_tes_off_acq_ricerca=create dw_tes_off_acq_ricerca
this.dw_tes_off_acq_lista=create dw_tes_off_acq_lista
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.sle_filtro_data
this.Control[iCurrent+2]=this.st_filtro_data
this.Control[iCurrent+3]=this.cb_offerta
this.Control[iCurrent+4]=this.cb_ricerca
this.Control[iCurrent+5]=this.cb_annulla
this.Control[iCurrent+6]=this.dw_tes_off_acq_ricerca
this.Control[iCurrent+7]=this.dw_tes_off_acq_lista
this.Control[iCurrent+8]=this.dw_folder
end on

on w_tes_off_acq_lista.destroy
call super::destroy
destroy(this.sle_filtro_data)
destroy(this.st_filtro_data)
destroy(this.cb_offerta)
destroy(this.cb_ricerca)
destroy(this.cb_annulla)
destroy(this.dw_tes_off_acq_ricerca)
destroy(this.dw_tes_off_acq_lista)
destroy(this.dw_folder)
end on

event pc_delete;call super::pc_delete;integer li_i


for li_i = 1 to dw_tes_off_acq_lista.deletedcount()
	if dw_tes_off_acq_lista.getitemstring(li_i, "flag_evasione", delete!, true) <> "A" then
	   g_mb.messagebox("Attenzione", "Offerta non cancellabile! Ha già subito un'evasione.", &
	              exclamation!, ok!)
	   dw_tes_off_acq_lista.set_dw_view(c_ignorechanges)
		triggerevent("pc_retrieve")
	   pcca.error = c_fatal
	   return
	end if

	if dw_tes_off_acq_lista.getitemstring(li_i, "flag_blocco", delete!, true) = "S" then
	   g_mb.messagebox("Attenzione", "Offerta non cancellabile! E' Bloccata.", &
	              exclamation!, ok!)
	   dw_tes_off_acq_lista.set_dw_view(c_ignorechanges)
		triggerevent("pc_retrieve")
	   pcca.error = c_fatal
	   return
	end if
next
end event

event pc_modify;call super::pc_modify;if dw_tes_off_acq_lista.getitemstring(dw_tes_off_acq_lista.getrow(), "flag_evasione") <> "A" then
   g_mb.messagebox("Attenzione", "Offerta non modificabile! Ha già subito un'evasione.", &
              exclamation!, ok!)
   w_tes_off_acq_det.dw_tes_off_acq_det_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

if dw_tes_off_acq_lista.getitemstring(dw_tes_off_acq_lista.getrow(), "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Offerta non modificabile! E' Bloccata.", &
              exclamation!, ok!)
   w_tes_off_acq_det.dw_tes_off_acq_det_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

end event

event pc_setddlb;call super::pc_setddlb;//dw_tes_off_acq_ricerca.fu_loadcode("cod_fornitore", &
//                       "anag_fornitori", &
//							  "cod_fornitore", &
//							  "rag_soc_1", &
//							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_fornitore asc", "(Tutti)" )
//
//dw_tes_off_acq_ricerca.fu_loadcode("cod_for_pot", &
//                       "anag_for_pot", &
//							  "cod_for_pot", &
//							  "rag_soc_1", &
//							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_for_pot asc", "(Tutti)" )

dw_tes_off_acq_ricerca.fu_loadcode("cod_tipo_off_acq", &
                       "tab_tipi_off_acq", &
							  "cod_tipo_off_acq", &
							  "des_tipo_off_acq", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", "(Tutti)" )

end event

type sle_filtro_data from u_sle_search within w_tes_off_acq_lista
integer x = 640
integer y = 1160
integer width = 846
integer height = 80
integer taborder = 30
end type

type st_filtro_data from statictext within w_tes_off_acq_lista
integer x = 69
integer y = 1180
integer width = 562
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Filtro su Data Offerta:"
boolean focusrectangle = false
end type

type cb_offerta from commandbutton within w_tes_off_acq_lista
integer x = 1897
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Offerta"
end type

on clicked;ib_apertura = true
dw_tes_off_acq_lista.postevent("pcd_view")

end on

type cb_ricerca from commandbutton within w_tes_off_acq_lista
integer x = 1897
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;// cb_search clicked event
dw_tes_off_acq_ricerca.fu_BuildSearch(TRUE)
sle_filtro_data.fu_BuildSearch(false)
dw_folder.fu_SelectTab(2)
dw_tes_off_acq_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_annulla from commandbutton within w_tes_off_acq_lista
integer x = 1509
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;dw_tes_off_acq_ricerca.fu_Reset()
end event

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_uo_dw_search = dw_tes_off_acq_ricerca
s_cs_xx.parametri.parametro_s_1 = "cod_for_pot"
end event

type dw_tes_off_acq_ricerca from u_dw_search within w_tes_off_acq_lista
integer x = 69
integer y = 140
integer width = 2194
integer height = 500
integer taborder = 30
string dataobject = "d_tes_off_acq_ricerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_tes_off_acq_ricerca,"cod_fornitore")
case "b_ricerca_cod_for"
		guo_ricerca.uof_ricerca_fornitore_potenziale(dw_tes_off_acq_ricerca,"cod_for_pot")
end choose
end event

type dw_tes_off_acq_lista from uo_cs_xx_dw within w_tes_off_acq_lista
integer x = 69
integer y = 140
integer width = 2194
integer height = 1000
integer taborder = 70
string dataobject = "d_tes_off_acq_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_view;integer li_row
string ls_expression
if s_cs_xx.parametri.parametro_i_1 > 0 and s_cs_xx.parametri.parametro_ul_1 > 0 then
	ls_expression = "anno_registrazione = " + string(s_cs_xx.parametri.parametro_i_1) + &
						 " and num_registrazione = " + string(s_cs_xx.parametri.parametro_ul_1)
	li_row = dw_tes_off_acq_lista.find(ls_expression, 1, dw_tes_off_acq_lista.rowcount())
	if li_row > 0 then
		dw_tes_off_acq_lista.selectrow(li_row, true)
		dw_tes_off_acq_lista.setrow(li_row)
	end if
	setnull(s_cs_xx.parametri.parametro_i_1)
	setnull(s_cs_xx.parametri.parametro_ul_1)
end if

if i_extendmode then
   if this.getrow() > 0 then
      if ib_apertura then
         s_cs_xx.parametri.parametro_s_1 = ""
         window_open_parm(w_tes_off_acq_det, -1, this)
         ib_apertura = false
      end if
   end if
end if
end event

on pcd_modify;if i_extendmode then
   s_cs_xx.parametri.parametro_s_1 = "modifica"
   window_open_parm(w_tes_off_acq_det, -1, this)
end if
end on

on pcd_new;if i_extendmode then
   s_cs_xx.parametri.parametro_s_1 = "nuovo"
   window_open_parm(w_tes_off_acq_det, -1, this)
end if
end on

event updatestart;call super::updatestart;if i_extendmode then
   integer ll_i
   long ll_anno_registrazione, ll_num_registrazione
   string ls_tabella, ls_nome_prog
	
		
   for ll_i = 1 to this.deletedcount()
      ll_anno_registrazione = this.getitemnumber(ll_i, "anno_registrazione", delete!, true)
      ll_num_registrazione = this.getitemnumber(ll_i, "num_registrazione", delete!, true)

		ls_tabella = "det_off_acq_stat"
		ls_nome_prog = ""

      if f_elimina_det_stat(ls_tabella, ll_anno_registrazione, ll_num_registrazione,ls_nome_prog,0) = -1 then
			return 1
		end if

      ls_tabella = "det_off_acq"
      if f_del_det(ls_tabella, ll_anno_registrazione, ll_num_registrazione) = -1 then
			return 1
		end if
	next
end if
end event

on doubleclicked;call uo_cs_xx_dw::doubleclicked;if i_extendmode then
   ib_apertura = true
   this.postevent("pcd_view")
end if
end on

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

if s_cs_xx.parametri.parametro_i_1 > 0 and s_cs_xx.parametri.parametro_ul_1 > 0 then
	this.postevent("pcd_view")
end if
end event

type dw_folder from u_folder within w_tes_off_acq_lista
integer x = 23
integer y = 20
integer width = 2286
integer height = 1240
integer taborder = 40
end type

