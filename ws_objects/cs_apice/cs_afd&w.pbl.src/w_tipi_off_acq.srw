﻿$PBExportHeader$w_tipi_off_acq.srw
$PBExportComments$Finestra Gestione Tipi Offerte Acquisto
forward
global type w_tipi_off_acq from w_cs_xx_principale
end type
type dw_tipi_off_acq_lista from uo_cs_xx_dw within w_tipi_off_acq
end type
type dw_tipi_off_acq_det from uo_cs_xx_dw within w_tipi_off_acq
end type
end forward

global type w_tipi_off_acq from w_cs_xx_principale
int Width=2263
int Height=1349
boolean TitleBar=true
string Title="Gestione Tipi Offerte Acquisto"
dw_tipi_off_acq_lista dw_tipi_off_acq_lista
dw_tipi_off_acq_det dw_tipi_off_acq_det
end type
global w_tipi_off_acq w_tipi_off_acq

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tipi_off_acq_det, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_tipi_off_acq_det, &
                 "cod_tipo_ord_acq", &
                 sqlca, &
                 "tab_tipi_ord_acq", &
                 "cod_tipo_ord_acq", &
                 "des_tipo_ord_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_tipi_off_acq_det, &
                 "cod_tipo_analisi", &
                 sqlca, &
                 "tab_tipi_analisi", &
                 "cod_tipo_analisi", &
                 "des_tipo_analisi", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tipi_off_acq_lista.set_dw_key("cod_azienda")
dw_tipi_off_acq_lista.set_dw_options(sqlca, &
                                     pcca.null_object, &
                                     c_default, &
                                     c_default)
dw_tipi_off_acq_det.set_dw_options(sqlca, &
                                   dw_tipi_off_acq_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)
iuo_dw_main = dw_tipi_off_acq_lista
end on

on w_tipi_off_acq.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tipi_off_acq_lista=create dw_tipi_off_acq_lista
this.dw_tipi_off_acq_det=create dw_tipi_off_acq_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tipi_off_acq_lista
this.Control[iCurrent+2]=dw_tipi_off_acq_det
end on

on w_tipi_off_acq.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tipi_off_acq_lista)
destroy(this.dw_tipi_off_acq_det)
end on

type dw_tipi_off_acq_lista from uo_cs_xx_dw within w_tipi_off_acq
int X=23
int Y=21
int Width=2172
int Height=501
int TabOrder=10
string DataObject="d_tipi_off_acq_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

event pcd_new;call super::pcd_new;dw_tipi_off_acq_det.setitem(dw_tipi_off_acq_lista.getrow(), "flag_doc_suc", "S")
dw_tipi_off_acq_det.setitem(dw_tipi_off_acq_lista.getrow(), "flag_st_note", "S")
end event

type dw_tipi_off_acq_det from uo_cs_xx_dw within w_tipi_off_acq
int X=23
int Y=541
int Width=2172
int Height=681
int TabOrder=20
string DataObject="d_tipi_off_acq_det"
BorderStyle BorderStyle=StyleRaised!
end type

