﻿$PBExportHeader$uo_moduli_stampa.sru
forward
global type uo_moduli_stampa from nonvisualobject
end type
end forward

global type uo_moduli_stampa from nonvisualobject
end type
global uo_moduli_stampa uo_moduli_stampa

type variables
constant string LOG_NAME = "MODSTAMPA"

private:
	uo_log_sistema iuo_log
end variables

forward prototypes
public function integer uof_stampa_moduli (ref datawindow adw_datawindow, string as_tipo_gestione, string as_cod_lingua, string as_cod_cliente, ref string as_error)
private function string uof_get_documentname (datawindow adw_datawindow)
public function integer uof_stampa_moduli (ref datawindow adw_datawindow, string as_tipo_gestione, string as_cod_lingua, string as_cod_cliente, integer ai_num_copie, ref string as_error)
public function integer uof_stampa_moduli (datawindow adw_datawindow, str_modulo_stampa astr_moduli[], integer ai_num_copie, ref string as_error)
end prototypes

public function integer uof_stampa_moduli (ref datawindow adw_datawindow, string as_tipo_gestione, string as_cod_lingua, string as_cod_cliente, ref string as_error);/**
 * stefanop 
 * 06/06/2014
 *
 * Recupero tutti i moduli da stampare e invio l'array alla
 * funzione di stampa
 **/

return uof_stampa_moduli(adw_datawindow, as_tipo_gestione, as_cod_lingua, as_cod_cliente, 1, as_error)
end function

private function string uof_get_documentname (datawindow adw_datawindow);/**
 * stefanop
 * 09/06/2014
 *
 * Ottengo il nome per il job di stampa
 **/
 
string ls_name

ls_name = adw_datawindow.Object.DataWindow.Print.DocumentName

if isnull(ls_name) or ls_name = "" then
	ls_name = "job-modulo-stampa"
end if

return ls_name
end function

public function integer uof_stampa_moduli (ref datawindow adw_datawindow, string as_tipo_gestione, string as_cod_lingua, string as_cod_cliente, integer ai_num_copie, ref string as_error);/**
 * stefanop 
 * 06/06/2014
 *
 * Recupero tutti i moduli da stampare e invio l'array alla
 * funzione di stampa
 **/
 
string ls_where, ls_cod_modulo, ls_where_modulo, ls_dataobject
int li_moduli_count, li_i, li_count, li_j, li_index, li_moduli_manuali
datastore lds_cod_moduli, lds_moduli
str_modulo_stampa lstr_moduli[]

// Creo where
li_moduli_manuali = 0
ls_where = " WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND cod_tipo_gestione='" + as_tipo_gestione + "' "

setnull(as_error)
li_moduli_count = guo_functions.uof_crea_datastore(lds_cod_moduli, "SELECT DISTINCT cod_modulo FROM tab_moduli_stampa " + ls_where, as_error)
if li_moduli_count <= 0 then
	destroy lds_cod_moduli
	return li_moduli_count
end if

// Ciclo tutti i moduli e controllo cosa stampare
for li_i = 1 to li_moduli_count
	
	ls_cod_modulo = lds_cod_moduli.getitemstring(li_i, 1)
	ls_where_modulo = "SELECT * FROM tab_moduli_stampa" + ls_where + " AND cod_modulo='" + ls_cod_modulo + "' "
	
	if not isnull(as_cod_cliente) then
		// Il cliente ha la precedenza su tutto
		li_count = guo_functions.uof_crea_datastore(lds_moduli, ls_where_modulo + " AND cod_cliente='" + as_cod_cliente + "' ", as_error)
		
		if li_count <= 0 then
			// Se il cliente non ha nessun modulo allora provo a cercare per la lingua
			if g_str.isempty(as_cod_lingua) or as_cod_lingua = "ITA" then
				li_count = guo_functions.uof_crea_datastore(lds_moduli, ls_where_modulo + " AND (cod_lingua is null OR cod_lingua='ITA') ", as_error)
			else
				li_count = guo_functions.uof_crea_datastore(lds_moduli, ls_where_modulo + " AND cod_lingua='" + as_cod_lingua + "' ", as_error)
			end if
			
		end if
	elseif g_str.isnotempty(as_cod_lingua) then
		// Se non viene impostato il cliente, provo a cercare solo per la lingua
		if g_str.isempty(as_cod_lingua) or as_cod_lingua = "ITA" then
			li_count = guo_functions.uof_crea_datastore(lds_moduli, ls_where_modulo + " AND (cod_lingua is null OR cod_lingua='ITA') ", as_error)
		else
			li_count = guo_functions.uof_crea_datastore(lds_moduli, ls_where_modulo + " AND cod_lingua='" + as_cod_lingua + "' ", as_error)
		end if
	else
		// non sono presenti moduli
		li_count = 0
	end if
	
	if li_count <= 0 then
		destroy lds_cod_moduli
		destroy lds_moduli
		return li_count
	end if
	
	for li_j = 1 to li_count
		
		ls_dataobject = lds_moduli.getitemstring(li_j, "dataobject")
		
		// controllo che il dataobject sia valido
		if g_str.isnotempty(ls_dataobject) then
			li_index = upperbound(lstr_moduli) + 1
			lstr_moduli[li_index].progressivo =  lds_moduli.getitemnumber(li_j, "progressivo")
			lstr_moduli[li_index].descrizione = lds_moduli.getitemstring(li_j, "titolo")
			lstr_moduli[li_index].dataobject = lds_moduli.getitemstring(li_j, "dataobject")
			lstr_moduli[li_index].logo_testata = lds_moduli.getitemstring(li_j, "logo_testata")
			lstr_moduli[li_index].automatico = lds_moduli.getitemstring(li_j, "flag_stampa_automatica") = "S"
			
			// tengo memoria se ci sono dei moduli manuali
			if lstr_moduli[li_index].automatico = false then li_moduli_manuali++
		end if
	next
	
next
// ---

// Ci sono dei moduli manuali da chiedere all'utente quali stampare?
if li_moduli_manuali > 0 then
	
	// Apro finestra
	str_moduli_param lstr_param
	lstr_param.moduli_stampa = lstr_moduli
	openwithParm(w_moduli_stampa_scelta, lstr_param)
	
	lstr_param = message.powerobjectparm
	
	if isvalid(lstr_param) then
		if lstr_param.user_cancel then
			// L'utente ha annullato tutto
			return 0
		end if
		
		lstr_moduli = lstr_param.moduli_stampa
	end if
	// ----
	
end if

if uof_stampa_moduli(adw_datawindow, lstr_moduli, ai_num_copie,  as_error) < 0 then
	return -1
else
	return 1
end if
end function

public function integer uof_stampa_moduli (datawindow adw_datawindow, str_modulo_stampa astr_moduli[], integer ai_num_copie, ref string as_error);/**
 * stefanop
 * 09/06/2014
 *
 * Stampa i moduli passati
 * Ritorna: 1 tutto ok, -1 errore
 **/
 
string ls_dataobject, ls_logo_testata, ls_dw_name
long ll_count, ll_i, ll_return, li_page
RuntimeError ex

ll_count = upperbound(astr_moduli)
ll_return = 1
 
if ll_count > 0 then
	// Apro la finestra dove c'è appoggiata una DW (nascosta) che serve per caricare il
	// dataobject. Se non di fa così PB non stampa nulla.
	window_open(w_moduli_stampa_loading, -1)
	w_moduli_stampa_loading.dw_1.settransobject(sqlca)
		
	ls_dataobject = adw_datawindow.dataobject
	ls_dw_name = uof_get_documentname(adw_datawindow)
		
	try
		adw_datawindow.setredraw(false)
		
		// Stampo per il numero di copie richieste
		for li_page = 1 to ai_num_copie
			
			for ll_i = 1 to ll_count
				w_moduli_stampa_loading.dw_1.dataobject =astr_moduli[ll_i].dataobject
				w_moduli_stampa_loading.dw_1.Object.DataWindow.Print.DocumentName = ls_dw_name
				
				adw_datawindow.rowscopy(1, adw_datawindow.rowcount(), Primary!, w_moduli_stampa_loading.dw_1, 1, Primary!)
				
				// check valid datawindow
				if isvalid(w_moduli_stampa_loading.dw_1.object) then
					// provo a cambiare il logo di intestazione se indicato
					if not isnull(astr_moduli[ll_i].logo_testata) and astr_moduli[ll_i].logo_testata <> "" then
						w_moduli_stampa_loading.dw_1.modify( "intestazione.filename='" + s_cs_xx.volume + s_cs_xx.risorse + astr_moduli[ll_i].logo_testata + "'")
					end if
						
					w_moduli_stampa_loading.dw_1.print()
				else
					iuo_log.uof_write_log_sistema_not_sqlca(LOG_NAME, "Il dataobject " + astr_moduli[ll_i].dataobject + " non esiste, stampa saltata")
				end if
				
			next
		next
		
	catch (RuntimeError e)
		as_error = e.getMessage()
		ll_return = -1
	finally
		close(w_moduli_stampa_loading)
	end try
	
	// reimposto la DW originale
	adw_datawindow.setredraw(true)
end if
 
return ll_return
end function

on uo_moduli_stampa.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_moduli_stampa.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;iuo_log = create uo_log_sistema
end event

event destructor;destroy iuo_log
end event

