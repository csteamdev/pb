﻿$PBExportHeader$w_allegato_doc_ven.srw
forward
global type w_allegato_doc_ven from w_cs_xx_principale
end type
type dw_allegato_doc_ven_1 from uo_cs_xx_dw within w_allegato_doc_ven
end type
type dw_allegato_doc_ven_2 from uo_cs_xx_dw within w_allegato_doc_ven
end type
type dw_allegato_doc_ven_3 from uo_cs_xx_dw within w_allegato_doc_ven
end type
type dw_allegato_doc_ven_4 from uo_cs_xx_dw within w_allegato_doc_ven
end type
type dw_folder from u_folder within w_allegato_doc_ven
end type
end forward

global type w_allegato_doc_ven from w_cs_xx_principale
integer width = 2126
integer height = 1852
string title = "Allegato Documenti di Vendita"
boolean maxbox = false
boolean resizable = false
dw_allegato_doc_ven_1 dw_allegato_doc_ven_1
dw_allegato_doc_ven_2 dw_allegato_doc_ven_2
dw_allegato_doc_ven_3 dw_allegato_doc_ven_3
dw_allegato_doc_ven_4 dw_allegato_doc_ven_4
dw_folder dw_folder
end type
global w_allegato_doc_ven w_allegato_doc_ven

on w_allegato_doc_ven.create
int iCurrent
call super::create
this.dw_allegato_doc_ven_1=create dw_allegato_doc_ven_1
this.dw_allegato_doc_ven_2=create dw_allegato_doc_ven_2
this.dw_allegato_doc_ven_3=create dw_allegato_doc_ven_3
this.dw_allegato_doc_ven_4=create dw_allegato_doc_ven_4
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_allegato_doc_ven_1
this.Control[iCurrent+2]=this.dw_allegato_doc_ven_2
this.Control[iCurrent+3]=this.dw_allegato_doc_ven_3
this.Control[iCurrent+4]=this.dw_allegato_doc_ven_4
this.Control[iCurrent+5]=this.dw_folder
end on

on w_allegato_doc_ven.destroy
call super::destroy
destroy(this.dw_allegato_doc_ven_1)
destroy(this.dw_allegato_doc_ven_2)
destroy(this.dw_allegato_doc_ven_3)
destroy(this.dw_allegato_doc_ven_4)
destroy(this.dw_folder)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]


dw_allegato_doc_ven_1.set_dw_options(sqlca, &
                              		 pcca.null_object, &
                              		 c_default, &
                              		 c_default)

dw_allegato_doc_ven_2.set_dw_options(sqlca, &
                              		 dw_allegato_doc_ven_1, &
                              		 c_sharedata + c_scrollparent, &
                              		 c_default)

dw_allegato_doc_ven_3.set_dw_options(sqlca, &
                              		 dw_allegato_doc_ven_1, &
                              		 c_sharedata + c_scrollparent, &
                              		 c_default)

dw_allegato_doc_ven_4.set_dw_options(sqlca, &
                              		 dw_allegato_doc_ven_1, &
                              		 c_sharedata + c_scrollparent, &
                              		 c_default)

lw_oggetti[1] = dw_allegato_doc_ven_1
dw_folder.fu_assigntab(1,"Testo 1", lw_oggetti[])

lw_oggetti[1] = dw_allegato_doc_ven_2
dw_folder.fu_assigntab(2,"Testo 2", lw_oggetti[])

lw_oggetti[1] = dw_allegato_doc_ven_3
dw_folder.fu_assigntab(3,"Testo 3", lw_oggetti[])

lw_oggetti[1] = dw_allegato_doc_ven_4
dw_folder.fu_assigntab(4,"Testo 4", lw_oggetti[])

dw_folder.fu_foldercreate(4,4)
dw_folder.fu_selecttab(1)

iuo_dw_main = dw_allegato_doc_ven_1
end event

type dw_allegato_doc_ven_1 from uo_cs_xx_dw within w_allegato_doc_ven
integer x = 46
integer y = 140
integer width = 2011
integer height = 1580
integer taborder = 10
string dataobject = "d_allegato_doc_ven_1"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda)
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i,"cod_azienda")) then
		setitem(ll_i,"cod_azienda",s_cs_xx.cod_azienda)
	end if
next
end event

type dw_allegato_doc_ven_2 from uo_cs_xx_dw within w_allegato_doc_ven
integer x = 46
integer y = 140
integer width = 2011
integer height = 1580
integer taborder = 20
string dataobject = "d_allegato_doc_ven_2"
boolean border = false
end type

type dw_allegato_doc_ven_3 from uo_cs_xx_dw within w_allegato_doc_ven
integer x = 46
integer y = 140
integer width = 2011
integer height = 1580
integer taborder = 20
string dataobject = "d_allegato_doc_ven_3"
boolean border = false
end type

type dw_allegato_doc_ven_4 from uo_cs_xx_dw within w_allegato_doc_ven
integer x = 46
integer y = 140
integer width = 2011
integer height = 1580
integer taborder = 20
string dataobject = "d_allegato_doc_ven_4"
boolean border = false
end type

type dw_folder from u_folder within w_allegato_doc_ven
integer x = 23
integer y = 20
integer width = 2057
integer height = 1720
integer taborder = 10
end type

