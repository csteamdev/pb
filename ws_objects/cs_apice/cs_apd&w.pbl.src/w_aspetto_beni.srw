﻿$PBExportHeader$w_aspetto_beni.srw
$PBExportComments$Finestra Composizione Aspetto Beni da Tabella Imballi
forward
global type w_aspetto_beni from w_cs_xx_risposta
end type
type dw_imballi from uo_cs_xx_dw within w_aspetto_beni
end type
type cb_1 from commandbutton within w_aspetto_beni
end type
type cb_2 from commandbutton within w_aspetto_beni
end type
end forward

global type w_aspetto_beni from w_cs_xx_risposta
int Width=2135
int Height=865
boolean TitleBar=true
string Title="Selezione Aspetto Beni"
boolean Resizable=false
dw_imballi dw_imballi
cb_1 cb_1
cb_2 cb_2
end type
global w_aspetto_beni w_aspetto_beni

event pc_setwindow;call super::pc_setwindow;dw_imballi.set_dw_options(sqlca, &
                          pcca.null_object, &
								  c_nonew + &
								  c_nomodify + &
								  c_nodelete + &
								  c_multiselect, &
								  c_default)

end event

on w_aspetto_beni.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_imballi=create dw_imballi
this.cb_1=create cb_1
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_imballi
this.Control[iCurrent+2]=cb_1
this.Control[iCurrent+3]=cb_2
end on

on w_aspetto_beni.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_imballi)
destroy(this.cb_1)
destroy(this.cb_2)
end on

type dw_imballi from uo_cs_xx_dw within w_aspetto_beni
int X=23
int Y=21
int Width=2058
int Height=621
int TabOrder=10
string DataObject="d_imballi"
BorderStyle BorderStyle=StyleLowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type cb_1 from commandbutton within w_aspetto_beni
int X=1715
int Y=661
int Width=366
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="&OK"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_str
long ll_selected[], ll_num, ll_i

ll_num = dw_imballi.get_selected_rows(ll_selected[])
ls_str = ""
for ll_i = 1 to ll_num
	ls_str = ls_str + dw_imballi.getitemstring(ll_selected[ll_i], "des_imballo")
	if ll_i <> ll_num then ls_str = ls_str + " - "
next

s_cs_xx.parametri.parametro_s_1 = ls_str
close(parent)

end event

type cb_2 from commandbutton within w_aspetto_beni
int X=1326
int Y=661
int Width=366
int Height=81
int TabOrder=3
boolean BringToTop=true
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;close(parent)
end event

