﻿$PBExportHeader$w_cambio_iva.srw
forward
global type w_cambio_iva from w_cs_xx_risposta
end type
type dw_cambio_iva from uo_cs_xx_dw within w_cambio_iva
end type
type cb_esegui from commandbutton within w_cambio_iva
end type
type cb_annulla from commandbutton within w_cambio_iva
end type
type st_1 from statictext within w_cambio_iva
end type
end forward

global type w_cambio_iva from w_cs_xx_risposta
int Width=2140
int Height=545
boolean TitleBar=true
string Title="Cambio IVA"
dw_cambio_iva dw_cambio_iva
cb_esegui cb_esegui
cb_annulla cb_annulla
st_1 st_1
end type
global w_cambio_iva w_cambio_iva

type variables
string is_tabella_det
long il_anno_registrazione, il_num_registrazione

end variables

forward prototypes
public function integer wf_aggiorna_iva (string as_tabella, long al_anno_registrazione, long al_num_registrazione, string as_cod_iva_da, string as_cod_iva_a, ref string as_messaggio)
end prototypes

public function integer wf_aggiorna_iva (string as_tabella, long al_anno_registrazione, long al_num_registrazione, string as_cod_iva_da, string as_cod_iva_a, ref string as_messaggio);integer li_risposta
string ls_sql


ls_sql = "update " + as_tabella + " set cod_iva ='" + as_cod_iva_a + "' where " + &
		 "anno_registrazione =" + string(al_anno_registrazione) + &
		 " and num_registrazione = " + string(al_num_registrazione)

if (not isnull(as_cod_iva_da)) then
	ls_sql = ls_sql + " and cod_iva ='" + as_cod_iva_da +"' "
end if

EXECUTE IMMEDIATE :ls_sql ;

li_risposta = sqlca.sqlcode
as_messaggio = sqlca.sqlerrtext

return li_risposta 
end function

on w_cambio_iva.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_cambio_iva=create dw_cambio_iva
this.cb_esegui=create cb_esegui
this.cb_annulla=create cb_annulla
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_cambio_iva
this.Control[iCurrent+2]=cb_esegui
this.Control[iCurrent+3]=cb_annulla
this.Control[iCurrent+4]=st_1
end on

on w_cambio_iva.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_cambio_iva)
destroy(this.cb_esegui)
destroy(this.cb_annulla)
destroy(this.st_1)
end on

event pc_setwindow;call super::pc_setwindow;is_tabella_det = s_cs_xx.parametri.parametro_s_1
il_anno_registrazione = s_cs_xx.parametri.parametro_d_1
il_num_registrazione = s_cs_xx.parametri.parametro_d_2

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_cambio_iva.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)




end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_cambio_iva, &
                 "cod_iva_da", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
					  
f_po_loaddddw_dw(dw_cambio_iva, &
                 "cod_iva_a", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

type dw_cambio_iva from uo_cs_xx_dw within w_cambio_iva
int X=23
int Y=21
int Width=2058
int Height=201
int TabOrder=10
string DataObject="d_cambio_iva"
BorderStyle BorderStyle=StyleLowered!
end type

type cb_esegui from commandbutton within w_cambio_iva
int X=1715
int Y=241
int Width=375
int Height=81
int TabOrder=20
boolean BringToTop=true
string Text="Esegui"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;string ls_cod_iva_da, ls_cod_iva_a, ls_messaggio
long ll_risp

dw_cambio_iva.accepttext()
ls_cod_iva_da = dw_cambio_iva.getitemstring(1, "cod_iva_da")
ls_cod_iva_a = dw_cambio_iva.getitemstring(1, "cod_iva_a")

if isnull(ls_cod_iva_a) then
	g_mb.messagebox("Apice", "Inserire il nuovo codice iva")
	return
end if
g_mb.messagebox("Apice: Attenzione!", "Ricordarsi che le righe di riferimento con Aliquota iva non nulla possono causare problemi nel calcolo fattura.", Information!)
ll_risp = wf_aggiorna_iva(is_tabella_det, il_anno_registrazione, il_num_registrazione, ls_cod_iva_da, ls_cod_iva_a, ls_messaggio)
if ll_risp < 0 then
	g_mb.messagebox("Apice", "Errore in aggiornamento dettagli: " + ls_messaggio)
elseif ll_risp = 0 then	// ok
	g_mb.messagebox("Apice", "Aggiornamento iva eseguito correttamente")
else			// ll_risp = 100: eseguito su nessuna riga
	g_mb.messagebox("Apice", "Aggiornamento iva non eseguito")
end if
s_cs_xx.parametri.parametro_i_1 = ll_risp		// operazione andata a buon fine
close(parent)
end event

type cb_annulla from commandbutton within w_cambio_iva
int X=1303
int Y=241
int Width=375
int Height=81
int TabOrder=3
boolean BringToTop=true
string Text="Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;
s_cs_xx.parametri.parametro_i_1 = 100	//operazione interrotta da utente oppure nessuna riga aggiornata

close(parent)


end event

type st_1 from statictext within w_cambio_iva
int X=23
int Y=241
int Width=1253
int Height=181
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
string Text="Selezionare l'eventuale IVA da cambiare, indicare la nuova aliquota, quindi fare click su esegui"
Alignment Alignment=Center!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

