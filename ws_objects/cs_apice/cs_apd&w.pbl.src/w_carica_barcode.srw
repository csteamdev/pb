﻿$PBExportHeader$w_carica_barcode.srw
forward
global type w_carica_barcode from w_cs_xx_principale
end type
type cb_aggiorna_db from commandbutton within w_carica_barcode
end type
type cb_2 from cb_prod_ricerca within w_carica_barcode
end type
type st_1 from statictext within w_carica_barcode
end type
type dw_selezione_barcode from uo_cs_xx_dw within w_carica_barcode
end type
type cb_annulla from commandbutton within w_carica_barcode
end type
type cb_cerca from commandbutton within w_carica_barcode
end type
type dw_elenco_prodotti_barcode from uo_cs_xx_dw within w_carica_barcode
end type
type dw_folder from u_folder within w_carica_barcode
end type
type dw_lista_barcode from uo_cs_xx_dw within w_carica_barcode
end type
end forward

global type w_carica_barcode from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3200
integer height = 1424
string title = "Carica Barcode"
cb_aggiorna_db cb_aggiorna_db
cb_2 cb_2
st_1 st_1
dw_selezione_barcode dw_selezione_barcode
cb_annulla cb_annulla
cb_cerca cb_cerca
dw_elenco_prodotti_barcode dw_elenco_prodotti_barcode
dw_folder dw_folder
dw_lista_barcode dw_lista_barcode
end type
global w_carica_barcode w_carica_barcode

type variables
boolean ib_ricerca = false
end variables

on w_carica_barcode.create
int iCurrent
call super::create
this.cb_aggiorna_db=create cb_aggiorna_db
this.cb_2=create cb_2
this.st_1=create st_1
this.dw_selezione_barcode=create dw_selezione_barcode
this.cb_annulla=create cb_annulla
this.cb_cerca=create cb_cerca
this.dw_elenco_prodotti_barcode=create dw_elenco_prodotti_barcode
this.dw_folder=create dw_folder
this.dw_lista_barcode=create dw_lista_barcode
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_aggiorna_db
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.dw_selezione_barcode
this.Control[iCurrent+5]=this.cb_annulla
this.Control[iCurrent+6]=this.cb_cerca
this.Control[iCurrent+7]=this.dw_elenco_prodotti_barcode
this.Control[iCurrent+8]=this.dw_folder
this.Control[iCurrent+9]=this.dw_lista_barcode
end on

on w_carica_barcode.destroy
call super::destroy
destroy(this.cb_aggiorna_db)
destroy(this.cb_2)
destroy(this.st_1)
destroy(this.dw_selezione_barcode)
destroy(this.cb_annulla)
destroy(this.cb_cerca)
destroy(this.dw_elenco_prodotti_barcode)
destroy(this.dw_folder)
destroy(this.dw_lista_barcode)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione_barcode.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)


dw_elenco_prodotti_barcode.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_cursorrowpointer)
//                            c_nohighlightselected + &
									 
dw_lista_barcode.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
									 
windowobject lw_oggetti[]
lw_oggetti[1] = dw_lista_barcode
lw_oggetti[2] = dw_elenco_prodotti_barcode
lw_oggetti[3] = st_1
dw_folder.fu_assigntab(2, "Codici a Barre", lw_oggetti[])
lw_oggetti[1] = dw_selezione_barcode
lw_oggetti[2] = cb_2
lw_oggetti[3] = cb_annulla
lw_oggetti[4] = cb_cerca
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])
dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)

end event

type cb_aggiorna_db from commandbutton within w_carica_barcode
integer x = 2697
integer y = 500
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Aggiorna DB"
end type

event clicked;string ls_cod_prodotto,ls_barcode
transaction sqlcb

if g_mb.messagebox("APICE","Sei sicuro di voler aggiornare l'archivio dei codici a barre con le modifiche apportate?",Question!,YesNo!,2) = 2 then return

sqlcb = create transaction
if f_po_getconnectinfo("", "database_5", sqlcb) <> 0 then
   g_mb.messagebox("APICE","Errore durante la connessione al sistema centrale; verificare la rete")
	return
end if

if f_po_connect(sqlcb, true) <> 0 then
   g_mb.messagebox("APICE","Errore durante la connessione al sistema centrale; verificare la rete")
	return
end if

delete from anag_prodotti_barcode
where cod_azienda = :s_cs_xx.cod_azienda
using sqlcb;
if sqlcb.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in cancellazione barcode nel sistema centrale~r~n"+sqlcb.sqlerrtext)
	rollback using sqlcb;
end if


declare cu_barcode cursor for  
select  cod_prodotto, barcode  
from    anag_prodotti_barcode  
where   cod_azienda = :s_cs_xx.cod_azienda   
order by cod_prodotto asc,   barcode asc;

open cu_barcode;
if sqlcb.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in open cursore cu_barcode~r~n" + sqlca.sqlerrtext)
	rollback using sqlca;
	return
end if

do while true
	fetch cu_barcode into :ls_cod_prodotto, :ls_barcode;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore in fetch cursore cu_barcode~r~n" + sqlca.sqlerrtext)
		rollback using sqlca;
		return
	end if
	if sqlca.sqlcode = 100 then exit
	
	insert into anag_prodotti_barcode
	(cod_azienda, cod_prodotto, barcode)
	values
	(:s_cs_xx.cod_azienda, :ls_cod_prodotto, :ls_barcode)
	using sqlcb;
	if sqlcb.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in trasferimento barcode dal sistema remoto al centrale.~r~nProdotto " + ls_cod_prodotto+"~r~nBarcodeo " + ls_barcode+ "~r~n" + sqlcb.sqlerrtext)
		rollback using sqlcb;
		rollback using sqlca;
		return
	end if
loop

commit using sqlca;
commit using sqlcb;


end event

type cb_2 from cb_prod_ricerca within w_carica_barcode
integer x = 2514
integer y = 260
integer width = 73
integer height = 80
integer taborder = 30
end type

event getfocus;call super::getfocus;dw_selezione_barcode.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

type st_1 from statictext within w_carica_barcode
integer x = 69
integer y = 1220
integer width = 3017
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean border = true
boolean focusrectangle = false
end type

type dw_selezione_barcode from uo_cs_xx_dw within w_carica_barcode
integer x = 46
integer y = 140
integer width = 2629
integer height = 480
integer taborder = 10
string dataobject = "d_selezione_barcode"
boolean border = false
end type

event itemchanged;call super::itemchanged;long ll_cont
choose case i_colname
	case "cod_fornitore"
		if not isnull(i_coltext) then
			select count(rag_soc_1)
			into   :ll_cont
			from   anag_fornitori
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_fornitore = :i_coltext;
			if ll_cont < 1 then
				g_mb.messagebox("APICE","Fornitore inesistente in anagrafica")
				return 1
			end if
		end if
end  choose
end event

type cb_annulla from commandbutton within w_carica_barcode
integer x = 2697
integer y = 240
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;dw_selezione_barcode.reset()
dw_selezione_barcode.insertrow(0)
end event

type cb_cerca from commandbutton within w_carica_barcode
integer x = 2697
integer y = 140
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;string ls_sql, ls_prodotto,ls_flag_origine_codici, ls_fornitore
long ll_righe, ll_i, ll_row
datastore lds_prodotti

ib_ricerca = false
dw_selezione_barcode.accepttext()
ls_prodotto = dw_selezione_barcode.getitemstring(dw_selezione_barcode.getrow(),"cod_prodotto")
ls_fornitore = dw_selezione_barcode.getitemstring(dw_selezione_barcode.getrow(),"cod_fornitore")
ls_flag_origine_codici = dw_selezione_barcode.getitemstring(dw_selezione_barcode.getrow(),"flag_origine_codici")
if isnull(ls_prodotto) then ls_prodotto = "%"
if isnull(ls_fornitore) then ls_fornitore = "%"

lds_prodotti = CREATE datastore
if ls_flag_origine_codici = "A" then
	lds_prodotti.dataobject = 'd_ds_elenco_prodotti_barcode_2'
else
	lds_prodotti.dataobject = 'd_ds_elenco_prodotti_barcode_1'
end if

lds_prodotti.settransobject(sqlca)
ll_righe = lds_prodotti.retrieve(s_cs_xx.cod_azienda, ls_prodotto, ls_fornitore)

dw_elenco_prodotti_barcode.reset()
for ll_i = 1 to ll_righe
	ll_row = dw_elenco_prodotti_barcode.insertrow(0)
	dw_elenco_prodotti_barcode.setitem(ll_row,"cod_prodotto",lds_prodotti.getitemstring(ll_i,"cod_prodotto"))
	dw_elenco_prodotti_barcode.setitem(ll_row,"des_prodotto",lds_prodotti.getitemstring(ll_i,"des_prodotto"))
	dw_elenco_prodotti_barcode.setitem(ll_row,"cod_misura_mag",lds_prodotti.getitemstring(ll_i,"cod_misura_mag"))
	st_1.text = "   Riga " + string(ll_i) + " di " + string(ll_righe)
next

destroy lds_prodotti
ib_ricerca=true

dw_folder.fu_selecttab(2)

end event

type dw_elenco_prodotti_barcode from uo_cs_xx_dw within w_carica_barcode
integer x = 69
integer y = 140
integer width = 2034
integer height = 1020
integer taborder = 20
string dataobject = "d_elenco_prodotti_barcode"
boolean vscrollbar = true
boolean livescroll = true
end type

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode and ib_ricerca then
	dw_lista_barcode.postevent("ue_barcode")
end if
end event

event getfocus;call super::getfocus;postevent("rowfocuschanged")
end event

type dw_folder from u_folder within w_carica_barcode
integer x = 23
integer y = 20
integer width = 3109
integer height = 1280
integer taborder = 10
end type

type dw_lista_barcode from uo_cs_xx_dw within w_carica_barcode
event ue_barcode ( )
integer x = 2126
integer y = 140
integer width = 983
integer height = 1020
integer taborder = 30
string dataobject = "d_lista_barcode"
end type

event ue_barcode();string ls_cod_prodotto, ls_barcode
long ll_riga

DECLARE cu_barcode CURSOR FOR  
SELECT barcode  
FROM anag_prodotti_barcode  
WHERE cod_azienda = :s_cs_xx.cod_azienda AND cod_prodotto = :ls_cod_prodotto
ORDER BY anag_prodotti_barcode.barcode ASC  ;

ls_cod_prodotto = dw_elenco_prodotti_barcode.getitemstring(dw_elenco_prodotti_barcode.getrow(),"cod_prodotto")

open cu_barcode;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in open cursore cu_barcode~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

reset()
ll_riga = 0
do while true
	fetch cu_barcode into :ls_barcode;
	if sqlca.sqlcode <> 0 then exit
	ll_riga = insertrow(0)
	setitem(ll_riga,"barcode",ls_barcode)
loop
close cu_barcode;
commit;
change_dw_focus(this)
setrow(insertrow(0))
setcolumn(1)

end event

event ue_key;call super::ue_key;string ls_barcode, ls_cod_prodotto, ls_str
long ll_i

choose case key
	case keyenter!
		ls_barcode = gettext()
		if isnull(ls_barcode) or len(ls_barcode) < 1 then return
		ls_cod_prodotto = dw_elenco_prodotti_barcode.getitemstring(dw_elenco_prodotti_barcode.getrow(),"cod_prodotto")
		
		select count(cod_prodotto), cod_prodotto
		into   :ll_i,:ls_str
		from   anag_prodotti_barcode
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 barcode = :ls_barcode
		group by cod_prodotto;
		if ll_i > 0 then   // trovato !!!
			if g_mb.messagebox("APICE","Codice a barre già presente nel prodotto " + ls_str + "~r~nProseguo lo stesso con l'inserimento?",Question!,YesNo!,1) = 2 then return 1
		end if
		
		accepttext()
		if dw_selezione_barcode.getitemstring(dw_selezione_barcode.getrow(),"flag_salva") = "N" then
			if g_mb.messagebox("APICE","Memorizzo il codice a barre ?",Question!,YesNo!,1) = 2 then return
		end if

		
		delete anag_prodotti_barcode
		where cod_azienda = :s_cs_xx.cod_azienda and
		      cod_prodotto = :ls_cod_prodotto;
		
		for ll_i = 1 to rowcount()
			ls_barcode = getitemstring(ll_i,"barcode")
			
			insert into anag_prodotti_barcode
			(cod_azienda, cod_prodotto, barcode) values
			(:s_cs_xx.cod_azienda, :ls_cod_prodotto, :ls_barcode);
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in inserimento barcode sul prodotto " + ls_cod_prodotto + "~r~n" + sqlca.sqlerrtext)
				rollback;
				return
			end if
		next
		commit;
		if dw_elenco_prodotti_barcode.getrow() < dw_elenco_prodotti_barcode.rowcount() then
			dw_elenco_prodotti_barcode.setrow(dw_elenco_prodotti_barcode.getrow() + 1)
		end if
		
	case keyuparrow!
		if dw_elenco_prodotti_barcode.getrow() > 1 then
			dw_elenco_prodotti_barcode.setrow(dw_elenco_prodotti_barcode.getrow() - 1)
		end if
		
	case keydownarrow!
		if dw_elenco_prodotti_barcode.getrow() < dw_elenco_prodotti_barcode.rowcount() then
			dw_elenco_prodotti_barcode.setrow(dw_elenco_prodotti_barcode.getrow() + 1)
		end if
		
	case keydelete!
		if getrow() > 0 then
			ls_cod_prodotto = dw_elenco_prodotti_barcode.getitemstring(dw_elenco_prodotti_barcode.getrow(),"cod_prodotto")
			ls_barcode = getitemstring(getrow(),"barcode")
			
			delete from anag_prodotti_barcode
			where cod_azienda = :s_cs_xx.cod_azienda and
			      cod_prodotto = :ls_cod_prodotto and
					barcode = :ls_barcode;
			
			commit;			
			postevent("ue_barcode")
		end if
end choose
commit;
end event

