﻿$PBExportHeader$w_ces_acquisti.srw
$PBExportComments$Finestra Gestione Cessioni Acquisti
forward
global type w_ces_acquisti from w_cs_xx_principale
end type
type dw_ces_acquisti_lista from uo_cs_xx_dw within w_ces_acquisti
end type
type dw_ces_acquisti_det from uo_cs_xx_dw within w_ces_acquisti
end type
end forward

global type w_ces_acquisti from w_cs_xx_principale
int Width=3406
int Height=1745
boolean TitleBar=true
string Title="Gestione Cessioni Acquisti"
dw_ces_acquisti_lista dw_ces_acquisti_lista
dw_ces_acquisti_det dw_ces_acquisti_det
end type
global w_ces_acquisti w_ces_acquisti

event pc_setwindow;call super::pc_setwindow;dw_ces_acquisti_lista.set_dw_key("cod_azienda")
dw_ces_acquisti_lista.set_dw_options(sqlca, &
                                     pcca.null_object, &
                                     c_default, &
                                     c_default)
dw_ces_acquisti_det.set_dw_options(sqlca, &
                                   dw_ces_acquisti_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)
iuo_dw_main = dw_ces_acquisti_lista
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ces_acquisti_det, &
                 "cod_documento", &
                 sqlca, &
                 "tab_documenti", &
                 "cod_documento", &
                 "des_documento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")					  
f_po_loaddddw_dw(dw_ces_acquisti_det, &
                 "cod_nomenclatura", &
                 sqlca, &
                 "tab_nomenclature", &
                 "cod_nomenclatura", &
                 "des_nomenclatura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

on w_ces_acquisti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_ces_acquisti_lista=create dw_ces_acquisti_lista
this.dw_ces_acquisti_det=create dw_ces_acquisti_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_ces_acquisti_lista
this.Control[iCurrent+2]=dw_ces_acquisti_det
end on

on w_ces_acquisti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_ces_acquisti_lista)
destroy(this.dw_ces_acquisti_det)
end on

type dw_ces_acquisti_lista from uo_cs_xx_dw within w_ces_acquisti
int X=23
int Y=21
int Width=3338
int Height=501
int TabOrder=10
string DataObject="d_ces_acquisti_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

type dw_ces_acquisti_det from uo_cs_xx_dw within w_ces_acquisti
int X=23
int Y=541
int Width=3338
int Height=1081
int TabOrder=20
string DataObject="d_ces_acquisti_det"
BorderStyle BorderStyle=StyleRaised!
end type

