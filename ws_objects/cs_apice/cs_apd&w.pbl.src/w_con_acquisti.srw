﻿$PBExportHeader$w_con_acquisti.srw
$PBExportComments$Finestra Condizioni di acquisto
forward
global type w_con_acquisti from w_cs_xx_principale
end type
type dw_con_acquisti from uo_cs_xx_dw within w_con_acquisti
end type
end forward

global type w_con_acquisti from w_cs_xx_principale
int Width=3077
int Height=581
boolean TitleBar=true
string Title="Condizioni Acquisto"
dw_con_acquisti dw_con_acquisti
end type
global w_con_acquisti w_con_acquisti

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_con_acquisti, &
                 "cod_tipo_det_acq_costo", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_con_acquisti, &
                 "cod_tipo_det_acq_sconto", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_con_acquisti, &
                 "cod_tipo_det_acq_sconto_inc", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
end event

event pc_setwindow;call super::pc_setwindow;dw_con_acquisti.set_dw_key("cod_azienda")

dw_con_acquisti.set_dw_options(sqlca, &
										pcca.null_object, &
										c_retrieveonopen, &
										c_default)				
end event

on w_con_acquisti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_con_acquisti=create dw_con_acquisti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_con_acquisti
end on

on w_con_acquisti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_con_acquisti)
end on

type dw_con_acquisti from uo_cs_xx_dw within w_con_acquisti
int X=23
int Y=21
int Width=2995
int Height=461
string DataObject="d_con_acquisti"
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i

for ll_i = 1 to this.rowcount()
	this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
next	
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)
	
if ll_errore < 0 then
	pcca.error = c_fatal
end if

end event

event pcd_new;call super::pcd_new;
this.setitem(1, "flag_tipo_ridistribuzione", "V")
end event

