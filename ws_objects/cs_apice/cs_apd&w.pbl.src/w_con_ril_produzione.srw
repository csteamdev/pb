﻿$PBExportHeader$w_con_ril_produzione.srw
forward
global type w_con_ril_produzione from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_con_ril_produzione
end type
type dw_list from uo_cs_xx_dw within w_con_ril_produzione
end type
end forward

global type w_con_ril_produzione from w_cs_xx_principale
integer width = 4603
integer height = 1604
string title = "Configurazione Rileva Produzione"
boolean maxbox = false
boolean resizable = false
boolean center = true
cb_1 cb_1
dw_list dw_list
end type
global w_con_ril_produzione w_con_ril_produzione

on w_con_ril_produzione.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_list=create dw_list
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_list
end on

on w_con_ril_produzione.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_list)
end on

event pc_setwindow;call super::pc_setwindow;
dw_list.set_dw_key("cod_azienda")
dw_list.set_dw_options(sqlca, pcca.null_object, c_default, c_default)
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_list, &
	"cod_deposito", &
	sqlca, &
	"anag_depositi", &
	"cod_deposito", &
	"des_deposito", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) AND flag_tipo_deposito='C' ")
	
	
f_PO_LoadDDDW_DW( dw_list, &
	"cod_tipo_bol_acq", &
	sqlca, &
	"tab_tipi_bol_acq", &
	"cod_tipo_bol_acq", &
	"des_tipo_bol_acq", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
end event

type cb_1 from commandbutton within w_con_ril_produzione
integer x = 4050
integer y = 1396
integer width = 517
integer height = 104
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa Etichetta"
end type

type dw_list from uo_cs_xx_dw within w_con_ril_produzione
integer x = 27
integer y = 20
integer width = 4539
integer height = 1352
integer taborder = 10
string dataobject = "d_con_ril_produzione"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event ue_key;call super::ue_key;choose case this.getcolumnname()

	case "cod_fornitore"
		if key = keyF1!  and keyflags = 1 then
			guo_ricerca.uof_set_response()
			guo_ricerca.uof_ricerca_fornitore(dw_list, "cod_fornitore")
		end if
		
end choose
end event

