﻿$PBExportHeader$w_con_vendite.srw
$PBExportComments$Finestra Parametri Vendite
forward
global type w_con_vendite from w_cs_xx_principale
end type
type dw_con_vendite_3 from uo_cs_xx_dw within w_con_vendite
end type
type dw_con_vendite_1 from uo_cs_xx_dw within w_con_vendite
end type
type dw_folder from u_folder within w_con_vendite
end type
type dw_con_vendite_2 from uo_cs_xx_dw within w_con_vendite
end type
end forward

global type w_con_vendite from w_cs_xx_principale
integer width = 3081
integer height = 2308
string title = "Parametri Vendite"
dw_con_vendite_3 dw_con_vendite_3
dw_con_vendite_1 dw_con_vendite_1
dw_folder dw_folder
dw_con_vendite_2 dw_con_vendite_2
end type
global w_con_vendite w_con_vendite

event pc_setwindow;call super::pc_setwindow;dw_con_vendite_1.set_dw_key("cod_azienda")
dw_con_vendite_1.set_dw_options(sqlca, &
                              pcca.null_object, &
                              c_default, &
                              c_NoResizeDW)
dw_con_vendite_2.set_dw_options(sqlca, &
                              dw_con_vendite_1, &
                              c_sharedata + c_scrollparent, &
                              c_NoResizeDW)
										
dw_con_vendite_3.set_dw_options(sqlca, &
                              dw_con_vendite_1, &
                              c_sharedata + c_scrollparent, &
                              c_NoResizeDW)
										
windowobject lw_oggetti[], l_objects[ ]


lw_oggetti[1] = dw_con_vendite_1
dw_folder.fu_assigntab(1, "Listini", lw_oggetti[])
lw_oggetti[1] = dw_con_vendite_2
dw_folder.fu_assigntab(2, "Altre Funzioni", lw_oggetti[])
lw_oggetti[1] = dw_con_vendite_3
dw_folder.fu_assigntab(3, "IperTech", lw_oggetti[])

dw_folder.fu_foldercreate(3,4)
dw_folder.fu_selecttab(1)

iuo_dw_main = dw_con_vendite_1
return 0
end event

on w_con_vendite.create
int iCurrent
call super::create
this.dw_con_vendite_3=create dw_con_vendite_3
this.dw_con_vendite_1=create dw_con_vendite_1
this.dw_folder=create dw_folder
this.dw_con_vendite_2=create dw_con_vendite_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_con_vendite_3
this.Control[iCurrent+2]=this.dw_con_vendite_1
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_con_vendite_2
end on

on w_con_vendite.destroy
call super::destroy
destroy(this.dw_con_vendite_3)
destroy(this.dw_con_vendite_1)
destroy(this.dw_folder)
destroy(this.dw_con_vendite_2)
end on

event pc_setddlb;f_PO_LoadDDDW_DW(dw_con_vendite_2,"cod_tipo_det_ven_trasporto",sqlca,&
                 "tab_tipi_det_ven","cod_tipo_det_ven","des_tipo_det_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_PO_LoadDDDW_DW(dw_con_vendite_3,"cod_tipo_off_ven_sfuso_ita",sqlca,&
                 "tab_tipi_off_ven","cod_tipo_off_ven","des_tipo_off_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_con_vendite_3,"cod_tipo_off_ven_sfuso_cee",sqlca,&
                 "tab_tipi_off_ven","cod_tipo_off_ven","des_tipo_off_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_con_vendite_3,"cod_tipo_off_ven_sfuso_excee",sqlca,&
                 "tab_tipi_off_ven","cod_tipo_off_ven","des_tipo_off_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_con_vendite_3,"cod_tipo_off_ven_finito_ita",sqlca,&
                 "tab_tipi_off_ven","cod_tipo_off_ven","des_tipo_off_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_con_vendite_3,"cod_tipo_off_ven_finito_cee",sqlca,&
                 "tab_tipi_off_ven","cod_tipo_off_ven","des_tipo_off_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_con_vendite_3,"cod_tipo_off_ven_finito_excee",sqlca,&
                 "tab_tipi_off_ven","cod_tipo_off_ven","des_tipo_off_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")



f_PO_LoadDDDW_DW(dw_con_vendite_3,"cod_tipo_ord_ven_sfuso_ita",sqlca,&
                 "tab_tipi_ord_ven","cod_tipo_ord_ven","des_tipo_ord_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_con_vendite_3,"cod_tipo_ord_ven_sfuso_cee",sqlca,&
                 "tab_tipi_ord_ven","cod_tipo_ord_ven","des_tipo_ord_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_con_vendite_3,"cod_tipo_ord_ven_sfuso_excee",sqlca,&
                 "tab_tipi_ord_ven","cod_tipo_ord_ven","des_tipo_ord_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_con_vendite_3,"cod_tipo_ord_ven_finito_ita",sqlca,&
                 "tab_tipi_ord_ven","cod_tipo_ord_ven","des_tipo_ord_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_con_vendite_3,"cod_tipo_ord_ven_finito_cee",sqlca,&
                 "tab_tipi_ord_ven","cod_tipo_ord_ven","des_tipo_ord_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_con_vendite_3,"cod_tipo_ord_ven_finito_excee",sqlca,&
                 "tab_tipi_ord_ven","cod_tipo_ord_ven","des_tipo_ord_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_con_vendite_3,"cod_tipo_off_ven_ipertech_hide",sqlca,&
                 "tab_tipi_off_ven","cod_tipo_off_ven","des_tipo_off_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")




end event

type dw_con_vendite_3 from uo_cs_xx_dw within w_con_vendite
integer x = 69
integer y = 120
integer width = 2903
integer height = 2020
integer taborder = 21
string dataobject = "d_con_vendite_3"
boolean border = false
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

type dw_con_vendite_1 from uo_cs_xx_dw within w_con_vendite
integer x = 69
integer y = 120
integer width = 2903
integer height = 2020
string dataobject = "d_con_vendite_1"
boolean border = false
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

type dw_folder from u_folder within w_con_vendite
integer x = 23
integer y = 20
integer width = 2994
integer height = 2160
integer taborder = 21
end type

type dw_con_vendite_2 from uo_cs_xx_dw within w_con_vendite
integer x = 69
integer y = 120
integer width = 2926
integer height = 2020
integer taborder = 11
string dataobject = "d_con_vendite_2"
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_cerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_con_vendite_2, "cod_cliente_bol_trasf")
		
end choose
end event

