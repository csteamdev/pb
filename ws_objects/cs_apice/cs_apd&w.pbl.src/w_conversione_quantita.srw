﻿$PBExportHeader$w_conversione_quantita.srw
forward
global type w_conversione_quantita from window
end type
type dw_1 from datawindow within w_conversione_quantita
end type
type st_2 from statictext within w_conversione_quantita
end type
type em_1 from editmask within w_conversione_quantita
end type
type cb_4 from commandbutton within w_conversione_quantita
end type
type cb_3 from commandbutton within w_conversione_quantita
end type
type cb_2 from commandbutton within w_conversione_quantita
end type
type st_1 from statictext within w_conversione_quantita
end type
type cb_1 from commandbutton within w_conversione_quantita
end type
end forward

global type w_conversione_quantita from window
integer width = 2578
integer height = 1144
boolean titlebar = true
string title = "Conversione Dettagli Documenti"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
dw_1 dw_1
st_2 st_2
em_1 em_1
cb_4 cb_4
cb_3 cb_3
cb_2 cb_2
st_1 st_1
cb_1 cb_1
end type
global w_conversione_quantita w_conversione_quantita

type variables
transaction sqlcb
end variables

on w_conversione_quantita.create
this.dw_1=create dw_1
this.st_2=create st_2
this.em_1=create em_1
this.cb_4=create cb_4
this.cb_3=create cb_3
this.cb_2=create cb_2
this.st_1=create st_1
this.cb_1=create cb_1
this.Control[]={this.dw_1,&
this.st_2,&
this.em_1,&
this.cb_4,&
this.cb_3,&
this.cb_2,&
this.st_1,&
this.cb_1}
end on

on w_conversione_quantita.destroy
destroy(this.dw_1)
destroy(this.st_2)
destroy(this.em_1)
destroy(this.cb_4)
destroy(this.cb_3)
destroy(this.cb_2)
destroy(this.st_1)
destroy(this.cb_1)
end on

event open;integer li_risposta

sqlcb = CREATE transaction
if f_po_getconnectinfo("", "database_" +  s_cs_xx.profilocorrente, sqlcb) <> 0 then
   halt close
end if
if f_po_connect(sqlcb, true) <> 0 then
   halt close
end if
return
end event

event close;destroy sqlcb
end event

type dw_1 from datawindow within w_conversione_quantita
integer x = 23
integer y = 240
integer width = 2491
integer height = 780
integer taborder = 20
string title = "none"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type st_2 from statictext within w_conversione_quantita
integer x = 23
integer y = 20
integer width = 402
integer height = 64
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "ANNO:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_1 from editmask within w_conversione_quantita
integer x = 457
integer y = 20
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "###0"
end type

type cb_4 from commandbutton within w_conversione_quantita
integer x = 891
integer y = 20
integer width = 389
integer height = 80
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Offerte"
end type

event clicked;string ls_cod_prodotto, ls_cod_misura
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_off_ven, ll_anno
double ld_quan_offerta,ld_fat_conversione_ven, ld_prezzo_vendita, ld_prezzo_um, ld_quantita_um

ll_anno = long(em_1.text)

declare cu_dettagli cursor for 
	select   anno_registrazione, num_registrazione, prog_riga_off_ven, cod_prodotto, cod_misura, quan_offerta, fat_conversione_ven, prezzo_vendita
	from     det_off_ven 
	where    cod_azienda = :s_cs_xx.cod_azienda and anno_registrazione = :ll_anno
	order by cod_azienda, 
				anno_registrazione, 
				num_registrazione,
				prog_riga_off_ven;

open cu_dettagli;

do while 0 = 0
	fetch cu_dettagli into :ll_anno_registrazione,
								  :ll_num_registrazione,
								  :ll_prog_riga_off_ven,
								  :ls_cod_prodotto,
								  :ls_cod_misura, 
								  :ld_quan_offerta,
								  :ld_fat_conversione_ven,
								  :ld_prezzo_vendita;
								  
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Conversione quantità","errore in fetch cursore fatture. Errore Nr." + string(sqlca.sqlcode) + " Descrizione=" + sqlca.sqlerrtext)
		rollback;
		exit
	end if

	st_1.text = string(ll_anno_registrazione) + " - " + string(ll_num_registrazione) + " - " + string(ll_prog_riga_off_ven) 
	if ld_fat_conversione_ven <> 0 then
		ld_quantita_um = round(ld_quan_offerta * ld_fat_conversione_ven,4)
		ld_prezzo_um = round(ld_prezzo_vendita / ld_fat_conversione_ven,4)
	else
		ld_quantita_um = 0
		ld_prezzo_um = 0
	end if
	
	update det_off_ven
	set quantita_um = :ld_quantita_um,
		 prezzo_um = :ld_prezzo_um
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_off_ven = :ll_prog_riga_off_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Conversione quantità","errore in update dettaglio. Errore Nr." + string(sqlca.sqlcode) + " Descrizione=" + sqlca.sqlerrtext)
		rollback;
		return
	end if
loop

close cu_dettagli;
commit;
end event

type cb_3 from commandbutton within w_conversione_quantita
integer x = 1303
integer y = 20
integer width = 389
integer height = 80
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ordine"
end type

event clicked;string ls_cod_prodotto, ls_cod_misura
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_anno, ll_cont, ll_i
double ld_quan_ordine,ld_fat_conversione_ven, ld_prezzo_vendita, ld_prezzo_um, ld_quantita_um

ll_anno = long(em_1.text)

dw_1.dataobject = "d_conversione_quan_ord_ven"
dw_1.settransobject(sqlca)
dw_1.retrieve(s_cs_xx.cod_azienda, ll_anno)

//declare cu_dettagli cursor for 
//	select   anno_registrazione, num_registrazione, prog_riga_ord_ven, cod_prodotto, cod_misura, quan_ordine, fat_conversione_ven, prezzo_vendita
//	from     det_ord_ven 
//	where    cod_azienda = :s_cs_xx.cod_azienda and anno_registrazione = :ll_anno
//	order by cod_azienda, 
//				anno_registrazione, 
//				num_registrazione,
//				prog_riga_ord_ven;
//
//open cu_dettagli;

ll_cont = dw_1.rowcount()
ll_i = 0
do while 0 = 0
	ll_i ++
	if ll_i > ll_cont then exit
	
	ll_anno_registrazione = dw_1.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = dw_1.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_ord_ven = dw_1.getitemnumber(ll_i, "prog_riga_ord_ven")
	ls_cod_prodotto = dw_1.getitemstring(ll_i, "cod_prodotto")
	ls_cod_misura = dw_1.getitemstring(ll_i, "cod_misura")
	ld_quan_ordine = dw_1.getitemnumber(ll_i, "quan_ordine")
	ld_fat_conversione_ven = dw_1.getitemnumber(ll_i, "fat_conversione_ven")
	ld_prezzo_vendita = dw_1.getitemnumber(ll_i, "prezzo_vendita")

//	fetch cu_dettagli into :ll_anno_registrazione,
//								  :ll_num_registrazione,
//								  :ll_prog_riga_ord_ven,
//								  :ls_cod_prodotto,
//								  :ls_cod_misura, 
//								  :ld_quan_ordine,
//								  :ld_fat_conversione_ven,
//								  :ld_prezzo_vendita;
//								  
//	if sqlca.sqlcode = 100 then exit
//	if sqlca.sqlcode <> 0 then
//		messagebox("Conversione quantità","errore in fetch cursore fatture. Errore Nr." + string(sqlca.sqlcode) + " Descrizione=" + sqlca.sqlerrtext)
//		rollback;
//		exit
//	end if

	st_1.text = string(ll_anno_registrazione) + " - " + string(ll_num_registrazione) + " - " + string(ll_prog_riga_ord_ven)  + " (" + string(ll_i) + "/" + string(ll_cont) + ")"
	if ld_fat_conversione_ven <> 0 then
		ld_quantita_um = round(ld_quan_ordine * ld_fat_conversione_ven,4)
		ld_prezzo_um = round(ld_prezzo_vendita / ld_fat_conversione_ven,4)
	else
		ld_quantita_um = 0
		ld_prezzo_um = 0
	end if
	
	update det_ord_ven
	set quantita_um = :ld_quantita_um,
		 prezzo_um = :ld_prezzo_um
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_ord_ven = :ll_prog_riga_ord_ven
	using sqlcb;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Conversione quantità","errore in update dettaglio. Errore Nr." + string(sqlcb.sqlcode) + " Descrizione=" + sqlcb.sqlerrtext)
		rollback using sqlcb;
		return
	else
		commit using sqlcb;
	end if
loop

commit using sqlcb;
rollback using sqlca;
dw_1.reset()
return
end event

type cb_2 from commandbutton within w_conversione_quantita
integer x = 1714
integer y = 20
integer width = 389
integer height = 80
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Bolle"
end type

event clicked;string ls_cod_prodotto, ls_cod_misura
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven, ll_anno
double ld_quan_consegnata,ld_fat_conversione_ven, ld_prezzo_vendita, ld_prezzo_um, ld_quantita_um

ll_anno = long(em_1.text)

declare cu_dettagli cursor for 
	select   anno_registrazione, num_registrazione, prog_riga_bol_ven, cod_prodotto, cod_misura, quan_consegnata, fat_conversione_ven, prezzo_vendita
	from     det_bol_ven 
	where    cod_azienda = :s_cs_xx.cod_azienda and anno_registrazione = :ll_anno
	order by cod_azienda, 
				anno_registrazione, 
				num_registrazione,
				prog_riga_bol_ven;

open cu_dettagli;

do while 0 = 0
	fetch cu_dettagli into :ll_anno_registrazione,
								  :ll_num_registrazione,
								  :ll_prog_riga_bol_ven,
								  :ls_cod_prodotto,
								  :ls_cod_misura, 
								  :ld_quan_consegnata,
								  :ld_fat_conversione_ven,
								  :ld_prezzo_vendita;
								  
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Conversione quantità","errore in fetch cursore fatture. Errore Nr." + string(sqlca.sqlcode) + " Descrizione=" + sqlca.sqlerrtext)
		rollback;
		exit
	end if

	st_1.text = string(ll_anno_registrazione) + " - " + string(ll_num_registrazione) + " - " + string(ll_prog_riga_bol_ven) 
	if ld_fat_conversione_ven <> 0 then
		ld_quantita_um = round(ld_quan_consegnata * ld_fat_conversione_ven,4)
		ld_prezzo_um = round(ld_prezzo_vendita / ld_fat_conversione_ven,4)
	else
		ld_quantita_um = 0
		ld_prezzo_um = 0
	end if
	
	update det_bol_ven
	set quantita_um = :ld_quantita_um,
		 prezzo_um = :ld_prezzo_um
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_bol_ven = :ll_prog_riga_bol_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Conversione quantità","errore in update dettaglio. Errore Nr." + string(sqlca.sqlcode) + " Descrizione=" + sqlca.sqlerrtext)
		rollback;
		return
	end if
loop

close cu_dettagli;
commit;
end event

type st_1 from statictext within w_conversione_quantita
integer x = 891
integer y = 120
integer width = 1623
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 15780518
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_conversione_quantita
integer x = 2126
integer y = 20
integer width = 389
integer height = 80
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Fatture"
end type

event clicked;string ls_cod_prodotto, ls_cod_misura
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven, ll_anno
double ld_quan_fatturata,ld_fat_conversione_ven, ld_prezzo_vendita, ld_prezzo_um, ld_quantita_um

ll_anno = long(em_1.text)

declare cu_dettagli cursor for 
	select   anno_registrazione, num_registrazione, prog_riga_fat_ven, cod_prodotto, cod_misura, quan_fatturata, fat_conversione_ven, prezzo_vendita
	from     det_fat_ven 
	where    cod_azienda = :s_cs_xx.cod_azienda and anno_registrazione = :ll_anno
	order by cod_azienda, 
				anno_registrazione, 
				num_registrazione,
				prog_riga_bol_ven;

open cu_dettagli;

do while 0 = 0
	fetch cu_dettagli into :ll_anno_registrazione,
								  :ll_num_registrazione,
								  :ll_prog_riga_fat_ven,
								  :ls_cod_prodotto,
								  :ls_cod_misura, 
								  :ld_quan_fatturata,
								  :ld_fat_conversione_ven,
								  :ld_prezzo_vendita;
								  
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Conversione quantità","errore in fetch cursore fatture. Errore Nr." + string(sqlca.sqlcode) + " Descrizione=" + sqlca.sqlerrtext)
		rollback;
		exit
	end if

	st_1.text = string(ll_anno_registrazione) + " - " + string(ll_num_registrazione) + " - " + string(ll_prog_riga_fat_ven)
	if ld_fat_conversione_ven <> 0 then
		ld_quantita_um = round(ld_quan_fatturata * ld_fat_conversione_ven,4)
		ld_prezzo_um = round(ld_prezzo_vendita / ld_fat_conversione_ven,4)
	else
		ld_quantita_um = 0
		ld_prezzo_um = 0
	end if
	
	update det_fat_ven
	set quantita_um = :ld_quantita_um,
		 prezzo_um = :ld_prezzo_um
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_fat_ven = :ll_prog_riga_fat_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Conversione quantità","errore in update dettaglio. Errore Nr." + string(sqlca.sqlcode) + " Descrizione=" + sqlca.sqlerrtext)
		rollback;
		return
	end if
loop

close cu_dettagli;
commit;
end event

