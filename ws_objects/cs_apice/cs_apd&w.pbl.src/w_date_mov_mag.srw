﻿$PBExportHeader$w_date_mov_mag.srw
forward
global type w_date_mov_mag from w_cs_xx_principale
end type
type rb_fat_ven from radiobutton within w_date_mov_mag
end type
type rb_fat_acq from radiobutton within w_date_mov_mag
end type
type rb_bol_ven from radiobutton within w_date_mov_mag
end type
type rb_bol_acq from radiobutton within w_date_mov_mag
end type
type cb_esegui from commandbutton within w_date_mov_mag
end type
type hpb_1 from hprogressbar within w_date_mov_mag
end type
type gb_1 from groupbox within w_date_mov_mag
end type
type gb_2 from groupbox within w_date_mov_mag
end type
end forward

global type w_date_mov_mag from w_cs_xx_principale
integer width = 1385
integer height = 744
string title = "Aggiornamento Data Movimenti"
boolean maxbox = false
boolean resizable = false
rb_fat_ven rb_fat_ven
rb_fat_acq rb_fat_acq
rb_bol_ven rb_bol_ven
rb_bol_acq rb_bol_acq
cb_esegui cb_esegui
hpb_1 hpb_1
gb_1 gb_1
gb_2 gb_2
end type
global w_date_mov_mag w_date_mov_mag

forward prototypes
public function integer wf_date_mov_mag ()
end prototypes

public function integer wf_date_mov_mag ();datetime ldt_data_doc, ldt_data_chiusura

long   	ll_i, ll_count, ll_anno_mov, ll_num_mov, ll_prog_mov

string 	ls_tab_tes, ls_tab_det, ls_col_data, ls_col_anno, ls_col_num, ls_col_riga, ls_where, ls_sql, ls_errore

datastore lds_datastore


select data_chiusura_annuale
into	 :ldt_data_chiusura
from	 con_magazzino
where	 cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura data ultima chiusura annuale: " + sqlca.sqlerrtext,stopsign!)
	return -1
elseif sqlca.sqlcode = 100  or isnull(ldt_data_chiusura) then
	ldt_data_chiusura = datetime(date("01/01/1900"),00:00:00)
end if

if rb_bol_acq.checked then
	ls_tab_tes = "tes_bol_acq"
	ls_tab_det = "det_bol_acq"
	ls_col_data = "data_bolla"
	ls_col_anno = "anno_bolla_acq"
	ls_col_num = "num_bolla_acq"
	ls_col_riga = "prog_riga_bolla_acq"
	ls_where = ""
elseif rb_bol_ven.checked then
	ls_tab_tes = "tes_bol_ven"
	ls_tab_det = "det_bol_ven"
	ls_col_data = "data_bolla"
	ls_col_anno = "anno_registrazione"
	ls_col_num = "num_registrazione"
	ls_col_riga = "prog_riga_bol_ven"
	ls_where = ""
elseif rb_fat_acq.checked then
	ls_tab_tes = "tes_fat_acq"
	ls_tab_det = "det_fat_acq"
	ls_col_data = "data_doc_origine"
	ls_col_anno = "anno_registrazione"
	ls_col_num = "num_registrazione"
	ls_col_riga = "prog_riga_fat_acq"
	ls_where = " and anno_bolla_acq is null and num_bolla_acq is null and prog_riga_bolla_acq is null "
elseif rb_fat_ven.checked then
	ls_tab_tes = "tes_fat_ven"
	ls_tab_det = "det_fat_ven"
	ls_col_data = "data_fattura"
	ls_col_anno = "anno_registrazione"
	ls_col_num = "num_registrazione"
	ls_col_riga = "prog_riga_fat_ven"
	ls_where = " and anno_registrazione_bol_ven is null and num_registrazione_bol_ven is null and prog_riga_bol_ven is null "
else
	return 100
end if

ls_sql = &
"select " + ls_col_data + ", " + &
"		  anno_registrazione_mov_mag, " + &
"		  num_registrazione_mov_mag " + &
"from   " + ls_tab_tes + ", " + &
"		  " + ls_tab_det + " " + &
"where  " + ls_tab_tes + ".cod_azienda = " + ls_tab_det + ".cod_azienda and " + &
"		  " + ls_tab_tes + "." + ls_col_anno + " = " + ls_tab_det + "." + ls_col_anno + " and " + &
"		  " + ls_tab_tes + "." + ls_col_num + " = " + ls_tab_det + "." + ls_col_num + " and " + &
"		  " + ls_tab_tes + ".cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"		  anno_registrazione_mov_mag is not null and " + &
"		  num_registrazione_mov_mag is not null " + ls_where + &
"order by " + ls_tab_det + "." + ls_col_anno + " ASC, " + &
"			 " + ls_tab_det + "." + ls_col_num + " ASC, " + &
"			 " + ls_tab_det + "." + ls_col_riga + " ASC "

setnull(ls_errore)

ls_sql = sqlca.syntaxfromsql(ls_sql,"grid",ls_errore)

if ls_sql = "" and not isnull(ls_errore) then
	g_mb.messagebox("APICE",ls_errore,stopsign!)
	return -1
end if

lds_datastore = create datastore

if lds_datastore.create(ls_sql,ls_errore) = -1 then
	g_mb.messagebox("APICE",ls_errore,stopsign!)
	return -1
end if

if lds_datastore.settransobject(sqlca) = -1 then
	g_mb.messagebox("APICE","Errore in impostazione transazione",stopsign!)
	return -1
end if

if lds_datastore.retrieve() = -1 then
	g_mb.messagebox("APICE","Errore in lettura elenco righe documenti",stopsign!)
	return -1
end if

ll_count = lds_datastore.rowcount()

for ll_i = 1 to ll_count
	
	ldt_data_doc = lds_datastore.getitemdatetime(ll_i,1)
	
	if ldt_data_doc < ldt_data_chiusura then
		continue
	end if
	
	ll_anno_mov = lds_datastore.getitemnumber(ll_i,2)
	
	ll_num_mov = lds_datastore.getitemnumber(ll_i,3)
	
	select prog_mov
	into	 :ll_prog_mov
	from	 mov_magazzino
	where	 cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_mov and
			 num_registrazione = :ll_num_mov;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in lettura progressivo movimento: " + sqlca.sqlerrtext,stopsign!)
		return -1
	elseif sqlca.sqlcode = 100 then
		g_mb.messagebox("APICE","Movimento non trovato: " + string(ll_anno_mov) + "/" + string(ll_num_mov),exclamation!)
		continue
	end if
	
	update mov_magazzino
	set	 data_registrazione = :ldt_data_doc
	where	 cod_azienda = :s_cs_xx.cod_azienda and
			 prog_mov = :ll_prog_mov and
			 data_registrazione > :ldt_data_chiusura;
	
	if sqlca.sqlcode <> 0 then
		rollback;
		g_mb.messagebox("APICE","Errore in aggiornamento data movimenti: " + sqlca.sqlerrtext,stopsign!)
		return -1
	end if
	
	commit;
	
	hpb_1.position = round((ll_i * 100 / ll_count),0)
	
next

return 0
end function

on w_date_mov_mag.create
int iCurrent
call super::create
this.rb_fat_ven=create rb_fat_ven
this.rb_fat_acq=create rb_fat_acq
this.rb_bol_ven=create rb_bol_ven
this.rb_bol_acq=create rb_bol_acq
this.cb_esegui=create cb_esegui
this.hpb_1=create hpb_1
this.gb_1=create gb_1
this.gb_2=create gb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.rb_fat_ven
this.Control[iCurrent+2]=this.rb_fat_acq
this.Control[iCurrent+3]=this.rb_bol_ven
this.Control[iCurrent+4]=this.rb_bol_acq
this.Control[iCurrent+5]=this.cb_esegui
this.Control[iCurrent+6]=this.hpb_1
this.Control[iCurrent+7]=this.gb_1
this.Control[iCurrent+8]=this.gb_2
end on

on w_date_mov_mag.destroy
call super::destroy
destroy(this.rb_fat_ven)
destroy(this.rb_fat_acq)
destroy(this.rb_bol_ven)
destroy(this.rb_bol_acq)
destroy(this.cb_esegui)
destroy(this.hpb_1)
destroy(this.gb_1)
destroy(this.gb_2)
end on

type rb_fat_ven from radiobutton within w_date_mov_mag
integer x = 777
integer y = 280
integer width = 526
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Fatture di Vendita"
boolean lefttext = true
borderstyle borderstyle = stylelowered!
end type

type rb_fat_acq from radiobutton within w_date_mov_mag
integer x = 69
integer y = 280
integer width = 571
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Fatture di Acquisto"
borderstyle borderstyle = stylelowered!
end type

type rb_bol_ven from radiobutton within w_date_mov_mag
integer x = 823
integer y = 120
integer width = 480
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Bolle di Vendita"
boolean lefttext = true
borderstyle borderstyle = stylelowered!
end type

type rb_bol_acq from radiobutton within w_date_mov_mag
integer x = 69
integer y = 120
integer width = 571
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Bolle di Acquisto"
borderstyle borderstyle = stylelowered!
end type

type cb_esegui from commandbutton within w_date_mov_mag
integer x = 937
integer y = 512
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Esegui"
end type

event clicked;long ll_return


setpointer(hourglass!)

hpb_1.position = 0

ll_return = wf_date_mov_mag()

if ll_return = 0 then
	g_mb.messagebox("APICE","Aggiornamento completato con successo",information!)
elseif ll_return = 100 then
	g_mb.messagebox("APICE","Selezionare un tipo documento!",exclamation!)
else
	g_mb.messagebox("APICE","Operazione interrotta a causa di errori",exclamation!)
end if

hpb_1.position = 0

rb_bol_acq.checked = false

rb_bol_ven.checked = false

rb_fat_acq.checked = false

rb_fat_ven.checked = false

setpointer(arrow!)
end event

type hpb_1 from hprogressbar within w_date_mov_mag
integer x = 69
integer y = 520
integer width = 837
integer height = 64
unsignedinteger maxposition = 100
integer setstep = 1
end type

type gb_1 from groupbox within w_date_mov_mag
integer x = 23
integer y = 20
integer width = 1326
integer height = 380
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Selezione Tipo Documento"
borderstyle borderstyle = stylelowered!
end type

type gb_2 from groupbox within w_date_mov_mag
integer x = 23
integer y = 420
integer width = 1326
integer height = 220
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Avanzamento Elaborazione"
borderstyle borderstyle = stylelowered!
end type

