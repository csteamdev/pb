﻿$PBExportHeader$w_det_giri_consegne.srw
$PBExportComments$Finestra Dettaglio Giri Consegna
forward
global type w_det_giri_consegne from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_det_giri_consegne
end type
type dw_det_giri_consegne_lista from uo_cs_xx_dw within w_det_giri_consegne
end type
type dw_det_giri_consegne_det from uo_cs_xx_dw within w_det_giri_consegne
end type
end forward

global type w_det_giri_consegne from w_cs_xx_principale
integer width = 2395
integer height = 1236
string title = "Dettaglio Giri Consegna"
cb_1 cb_1
dw_det_giri_consegne_lista dw_det_giri_consegne_lista
dw_det_giri_consegne_det dw_det_giri_consegne_det
end type
global w_det_giri_consegne w_det_giri_consegne

type variables
boolean ib_new
end variables

on w_det_giri_consegne.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_det_giri_consegne_lista=create dw_det_giri_consegne_lista
this.dw_det_giri_consegne_det=create dw_det_giri_consegne_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_det_giri_consegne_lista
this.Control[iCurrent+3]=this.dw_det_giri_consegne_det
end on

on w_det_giri_consegne.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_det_giri_consegne_lista)
destroy(this.dw_det_giri_consegne_det)
end on

event pc_setwindow;call super::pc_setwindow;dw_det_giri_consegne_lista.set_dw_key("cod_azienda")
dw_det_giri_consegne_lista.set_dw_key("cod_giro_consegna")
dw_det_giri_consegne_lista.set_dw_key("cod_cliente")
dw_det_giri_consegne_lista.set_dw_key("prog_giro_consegna")

dw_det_giri_consegne_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default)

dw_det_giri_consegne_det.set_dw_options(sqlca, &
                                    dw_det_giri_consegne_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

iuo_dw_main = dw_det_giri_consegne_lista
//cb_ricerca_cliente.enabled = false
ib_new = false
end event

event pc_delete;call super::pc_delete;//cb_ricerca_cliente.enabled = false
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det_giri_consegne_lista, &
                 "cod_cliente", &
                 sqlca, &
                 "anag_clienti", &
                 "cod_cliente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

//f_po_loaddddw_dw(dw_det_giri_consegne_det, &
//                 "cod_cliente", &
//                 sqlca, &
//                 "anag_clienti", &
//                 "cod_cliente", &
//                 "rag_soc_1", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type cb_1 from commandbutton within w_det_giri_consegne
integer x = 1943
integer y = 1040
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Riordina x 10"
end type

event clicked;long ll_i, ll_righe, ll_cont

ll_righe = dw_det_giri_consegne_lista.rowcount()
ll_cont = 0
for ll_i = 1 to ll_righe
	ll_cont = ll_cont + 10
	dw_det_giri_consegne_lista.setitem(ll_i, "prog_giro_consegna", ll_cont)
next
parent.triggerevent("pc_save")
end event

type dw_det_giri_consegne_lista from uo_cs_xx_dw within w_det_giri_consegne
integer x = 23
integer y = 20
integer width = 2309
integer height = 500
integer taborder = 30
string dataobject = "d_det_giri_consegne_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;long  l_error
string ls_cod_giro_consegna, ls_des_giro_consegna, ls_titolo

ls_cod_giro_consegna = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_giro_consegna")
ls_des_giro_consegna = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "des_giro_consegna")

l_error = retrieve(s_cs_xx.cod_azienda, ls_cod_giro_consegna)


ls_titolo = "Clienti Giro Consegna"

if ls_cod_giro_consegna<>"" and not isnull(ls_cod_giro_consegna) then
	ls_titolo += " " + ls_cod_giro_consegna
	if ls_des_giro_consegna<>"" and not isnull(ls_des_giro_consegna) then ls_titolo += " " + ls_des_giro_consegna
end if

if l_error < 0 then
   pcca.error = c_fatal
end if

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 to this.rowcount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
   IF IsNull(GetItemstring(l_Idx, "cod_giro_consegna")) THEN
      SetItem(l_Idx, "cod_giro_consegna", i_parentdw.GetItemstring( &
				  i_parentdw.i_selectedrows[1], "cod_giro_consegna"))
   END IF
NEXT

end event

event pcd_save;call super::pcd_save;//cb_ricerca_cliente.enabled = false
ib_new = false
cb_1.enabled = false
end event

event pcd_new;call super::pcd_new;//string   ls_cod_cliente
//
//datetime ldt_oggi
//
//
//ldt_oggi = datetime(today(),00:00:00)
//
//declare cur_clienti cursor for
//select cod_cliente
//from   anag_clienti
//where  cod_azienda = :s_cs_xx.cod_azienda and
// 		 ( flag_blocco = 'N' or (flag_blocco = 'S' and data_blocco > :ldt_oggi) );
// 
//open cur_clienti;
//
//if sqlca.sqlcode = -1 then
//	g_mb.messagebox("Estrazione Cliente", "Errore nella open del cursore cur_clienti: " + sqlca.sqlerrtext)
//	pcca.error = c_fatal
//	return
//end if
//
//fetch cur_clienti into :ls_cod_cliente;
//
//if sqlca.sqlcode = -1 then
//	g_mb.messagebox("Estrazione Cliente", "Errore nella fetch del cursore cur_clienti: " + sqlca.sqlerrtext)
//	close cur_clienti;
//	pcca.error = c_fatal
//	return
//elseif sqlca.sqlcode = 0 then
//	this.setitem(this.getrow(), "cod_cliente", ls_cod_cliente)
//end if
//
////cb_ricerca_cliente.enabled = true
//
//ib_new = true
//
//close cur_clienti;
//
//cb_1.enabled = false
end event

event pcd_modify;call super::pcd_modify;

//cb_ricerca_cliente.visible = false
//cb_ricerca_cliente.enabled = false
cb_1.enabled = true
end event

event pcd_view;call super::pcd_view;//cb_ricerca_cliente.enabled = false
ib_new = false
cb_1.enabled = false
end event

event updatestart;call super::updatestart;if i_extendmode then
	if ib_new then
		long ll_prog_giro_consegna
		string ls_cod_giro_consegna, ls_cod_cliente
		
		ll_prog_giro_consegna = this.getitemnumber(this.getrow(), "prog_giro_consegna")
		if isnull(ll_prog_giro_consegna) or ll_prog_giro_consegna = 0 then
			ls_cod_giro_consegna = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], &
																			"cod_giro_consegna")
			ls_cod_cliente = this.getitemstring (this.getrow(), "cod_cliente")
			select max(prog_giro_consegna)
			  into :ll_prog_giro_consegna
			  from det_giri_consegne
			 where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_giro_consegna = :ls_cod_giro_consegna and
					 cod_cliente = :ls_cod_cliente;
					 
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Calcolo Progressivo", "Errore nell'Estrazione Progressivo")
				pcca.error = c_fatal
				return
			end if
		
			if ll_prog_giro_consegna = 0 or isnull(ll_prog_giro_consegna) then
				ll_prog_giro_consegna = 100
			else
				ll_prog_giro_consegna = ll_prog_giro_consegna - mod(ll_prog_giro_consegna, 100) + 100
			end if
			this.setitem(this.getrow(), "prog_giro_consegna", ll_prog_giro_consegna)
		end if
	end if
end if
end event

event pcd_delete;call super::pcd_delete;cb_1.enabled = false
end event

event itemchanged;call super::itemchanged;//cb_ricerca_cliente.enabled = false
end event

type dw_det_giri_consegne_det from uo_cs_xx_dw within w_det_giri_consegne
integer x = 23
integer y = 520
integer width = 2309
integer height = 500
integer taborder = 20
string dataobject = "d_det_giri_consegne_det"
borderstyle borderstyle = styleraised!
end type

event pcd_modify;call super::pcd_modify;//cb_ricerca_cliente.enabled =FALSE
end event

event pcd_new;call super::pcd_new;////cb_ricerca_cliente.enabled = TRUE
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_det_giri_consegne_det,"cod_cliente")
end choose
end event

