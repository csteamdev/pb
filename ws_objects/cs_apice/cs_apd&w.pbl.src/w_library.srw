﻿$PBExportHeader$w_library.srw
forward
global type w_library from window
end type
type cb_2 from commandbutton within w_library
end type
type dw_1 from datawindow within w_library
end type
type mle_1 from multilineedit within w_library
end type
type cb_1 from commandbutton within w_library
end type
end forward

global type w_library from window
integer width = 3351
integer height = 2196
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
cb_2 cb_2
dw_1 dw_1
mle_1 mle_1
cb_1 cb_1
end type
global w_library w_library

on w_library.create
this.cb_2=create cb_2
this.dw_1=create dw_1
this.mle_1=create mle_1
this.cb_1=create cb_1
this.Control[]={this.cb_2,&
this.dw_1,&
this.mle_1,&
this.cb_1}
end on

on w_library.destroy
destroy(this.cb_2)
destroy(this.dw_1)
destroy(this.mle_1)
destroy(this.cb_1)
end on

type cb_2 from commandbutton within w_library
integer x = 23
integer y = 720
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "none"
end type

event clicked;DW_1.dataobject = 'd_report_fat_ven_euro'
end event

type dw_1 from datawindow within w_library
integer y = 860
integer width = 3291
integer height = 1200
integer taborder = 30
string title = "none"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type mle_1 from multilineedit within w_library
integer x = 23
integer y = 140
integer width = 3269
integer height = 560
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
boolean hscrollbar = true
boolean vscrollbar = true
boolean autohscroll = true
boolean autovscroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_1 from commandbutton within w_library
integer x = 274
integer y = 20
integer width = 983
integer height = 100
integer taborder = 10
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "CAMBI LIBRARY"
end type

event clicked;long ll_ret
string ls_library

ls_library = GetLibraryList ( )

ls_library = "c:\cs_105\cs_infomaker1.pbl, " + ls_library

ll_ret = SetLibraryList (ls_library)

MESSAGEBOX("", LL_RET)

//ll_ret = addtolibrarylist( "c:\cs_105\cs_infomaker1.pbl" )

mle_1.text = GetLibraryList ( )
end event

