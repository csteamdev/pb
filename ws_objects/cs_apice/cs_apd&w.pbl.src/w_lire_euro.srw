﻿$PBExportHeader$w_lire_euro.srw
forward
global type w_lire_euro from window
end type
type rb_8 from radiobutton within w_lire_euro
end type
type rb_7 from radiobutton within w_lire_euro
end type
type sle_dec from singlelineedit within w_lire_euro
end type
type st_dec from statictext within w_lire_euro
end type
type st_log from statictext within w_lire_euro
end type
type cbx_log from checkbox within w_lire_euro
end type
type rb_6 from radiobutton within w_lire_euro
end type
type rb_5 from radiobutton within w_lire_euro
end type
type cbx_1 from checkbox within w_lire_euro
end type
type rb_4 from radiobutton within w_lire_euro
end type
type rb_3 from radiobutton within w_lire_euro
end type
type lb_1 from listbox within w_lire_euro
end type
type documento from statictext within w_lire_euro
end type
type st_3 from statictext within w_lire_euro
end type
type elemento from statictext within w_lire_euro
end type
type st_1 from statictext within w_lire_euro
end type
type cb_1 from commandbutton within w_lire_euro
end type
type rb_2 from radiobutton within w_lire_euro
end type
type rb_1 from radiobutton within w_lire_euro
end type
type r_1 from rectangle within w_lire_euro
end type
type r_2 from rectangle within w_lire_euro
end type
type r_3 from rectangle within w_lire_euro
end type
type r_4 from rectangle within w_lire_euro
end type
type r_5 from rectangle within w_lire_euro
end type
type r_6 from rectangle within w_lire_euro
end type
type r_7 from rectangle within w_lire_euro
end type
end forward

global type w_lire_euro from window
integer width = 2958
integer height = 1764
boolean titlebar = true
string title = "Conversione  LIRE -> EURO"
boolean controlmenu = true
boolean minbox = true
long backcolor = 67108864
rb_8 rb_8
rb_7 rb_7
sle_dec sle_dec
st_dec st_dec
st_log st_log
cbx_log cbx_log
rb_6 rb_6
rb_5 rb_5
cbx_1 cbx_1
rb_4 rb_4
rb_3 rb_3
lb_1 lb_1
documento documento
st_3 st_3
elemento elemento
st_1 st_1
cb_1 cb_1
rb_2 rb_2
rb_1 rb_1
r_1 r_1
r_2 r_2
r_3 r_3
r_4 r_4
r_5 r_5
r_6 r_6
r_7 r_7
end type
global w_lire_euro w_lire_euro

type variables
string is_tipo_doc

long   il_dec
end variables

forward prototypes
public function integer wf_converti_listini_dim ()
public function integer wf_converti_listini_fornitori ()
public function integer wf_converti_listini_prod ()
public function integer wf_converti_listini_vendite ()
public function integer wf_converti_gruppi_sconto ()
public function integer wf_converti_cli_for ()
public function integer wf_converti_aperti ()
public function integer wf_converti_parziali ()
end prototypes

public function integer wf_converti_listini_dim ();long      ll_conv, ll_err, ll_progressivo, ll_num_scaglione, ll_i

string    ls_flag,ls_cod_valuta_lire, ls_cod_valuta_euro, ls_cod_tipo, ls_i_o

decimal   ld_limite_dimensione_1, ld_limite_dimensione_2, ld_variazione

datetime  ldt_data_inizio_val

datastore lds_dim


lb_1.additem("Conversione di LISTINI DIMENSIONI")
lb_1.additem("")

ll_err = 0
ll_conv = 0

select stringa
into   :ls_cod_valuta_lire
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'CVL';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("Impossibile ottenere il codice della valuta lire")
	lb_1.additem("Errore nella select del parametro CVL da parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if	

select stringa
into   :ls_cod_valuta_euro
from	 parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'EUR';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("Impossibile ottenere il codice della valuta euro")
	lb_1.additem("Errore nella select del parametro CVL da parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if		
				
lds_dim = create datastore

lds_dim.dataobject = "d_ds_list_dim"

if lds_dim.settransobject(sqlca) < 0 then
	lb_1.additem("Impossibile ottenere i dati da convertire: errore nell'impostazione della transazione del datastore")
	destroy lds_dim
	return -1
end if

if lds_dim.retrieve(s_cs_xx.cod_azienda,ls_cod_valuta_lire) < 0 then
	lb_1.additem("Impossibile ottenere i dati da convertire: errore nella retrieve del datastore")
	return -1
end if

for ll_i = 1 to lds_dim.rowcount()
	
	ls_cod_tipo = lds_dim.getitemstring(ll_i,"cod_tipo_listino_prodotto")
	ldt_data_inizio_val = lds_dim.getitemdatetime(ll_i,"data_inizio_val")
	ll_progressivo = lds_dim.getitemnumber(ll_i,"progressivo")
	ll_num_scaglione = lds_dim.getitemnumber(ll_i,"num_scaglione")
	ld_limite_dimensione_1 = lds_dim.getitemnumber(ll_i,"limite_dimensione_1")
	ld_limite_dimensione_2 = lds_dim.getitemnumber(ll_i,"limite_dimensione_2")
	ls_flag = lds_dim.getitemstring(ll_i,"flag_sconto_mag_prezzo")
	ld_variazione = lds_dim.getitemnumber(ll_i,"variazione")

	elemento.text = ls_cod_tipo + " " + string(date(ldt_data_inizio_val)) + ", " + string(ll_progressivo) + " " + &
	                string(round(ld_limite_dimensione_1,0)) + "X" + string(round(ld_limite_dimensione_2,0))

	if ls_flag = 'P' then
		if ld_variazione <> 0 then
			ld_variazione = round( (ld_variazione / 1936.27) , il_dec )
		end if
	end if	
	
	insert into listini_vendite_dimensioni
               (cod_azienda,
           		cod_tipo_listino_prodotto,
           		cod_valuta,
           		data_inizio_val,
           		progressivo,
           		num_scaglione,
           		limite_dimensione_1,
           		limite_dimensione_2,
           		flag_sconto_mag_prezzo,
           		variazione)
   select      cod_azienda,
            	cod_tipo_listino_prodotto,
            	:ls_cod_valuta_euro,
            	data_inizio_val,
            	progressivo,
            	num_scaglione,
            	limite_dimensione_1,
            	limite_dimensione_2,
            	flag_sconto_mag_prezzo,
            	:ld_variazione
   from 			listini_vendite_dimensioni
	where       cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_listino_prodotto = :ls_cod_tipo and
					cod_valuta = :ls_cod_valuta_lire and
					data_inizio_val = :ldt_data_inizio_val and
					progressivo = :ll_progressivo and
					num_scaglione = :ll_num_scaglione and
					limite_dimensione_1 = :ld_limite_dimensione_1 and
					limite_dimensione_2 = :ld_limite_dimensione_2 ;

	if sqlca.sqlcode <> 0 then
		lb_1.additem("Errore nella insert di listini_dimensioni: " + sqlca.sqlerrtext)
		rollback;
		ll_err++
	else
		commit;
		ll_conv++
	end if	
	 
next

destroy lds_dim

elemento.text = ""

if ll_conv <> 0 then

	if ll_conv <> 1 then
		ls_i_o = "i sono stati convertiti in EURO"
	else
		ls_i_o = "o è stato convertito in EURO"
	end if

	lb_1.additem("- " + string(ll_conv) + " listin" + ls_i_o)

end if

if ll_err <> 0 then

	if ll_err <> 1 then
		ls_i_o = "i hanno provocato errori"
	else
		ls_i_o = "o ha provocato errori"
	end if

	lb_1.additem("- " + string(ll_err) + " listin" + ls_i_o)

end if

return 0
end function

public function integer wf_converti_listini_fornitori ();string 	  ls_cod_valuta_lire, ls_cod_valuta_euro, ls_cod_prodotto, ls_cod_fornitore, ls_i_o

dec{4} 	  ld_prezzo_1, ld_prezzo_2, ld_prezzo_3, ld_prezzo_4, ld_prezzo_5, ld_prezzo_ult

datetime   ldt_data_inizio_val

long       ll_conv, ll_err, ll_i

datastore  lds_acq


lb_1.additem("Conversione di LISTINI DI ACQUISTO")
lb_1.additem("")

ll_err = 0
ll_conv = 0

select stringa
into   :ls_cod_valuta_lire
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'CVL';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("Impossibile ottenere il codice della valuta lire")
	lb_1.additem("Errore nella select del parametro CVL da parametri_azienda: " + sqlca.sqlerrtext)
	return -1	
end if	

select stringa
into   :ls_cod_valuta_euro
from	 parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'EUR';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("Impossibile ottenere il codice della valuta euro")
	lb_1.additem("Errore nella select del parametro CVL da parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if		
				
lds_acq = create datastore

lds_acq.dataobject = "d_ds_list_for"

if lds_acq.settransobject(sqlca) < 0 then
	lb_1.additem("Impossibile ottenere i dati da convertire: errore nell'impostazione della transazione del datastore")
	destroy lds_acq
	return -1
end if		

if lds_acq.retrieve(s_cs_xx.cod_azienda,ls_cod_valuta_lire) < 0 then
	lb_1.additem("Impossibile ottenere i dati da convertire: errore nella retrieve del datastore")	
	return -1
end if

for ll_i = 1 to lds_acq.rowcount()
	
	ls_cod_prodotto = lds_acq.getitemstring(ll_i,"cod_prodotto")
	ls_cod_fornitore = lds_acq.getitemstring(ll_i,"cod_fornitore")
	ldt_data_inizio_val = lds_acq.getitemdatetime(ll_i,"data_inizio_val")
	ld_prezzo_1 = lds_acq.getitemnumber(ll_i,"prezzo_1")
	ld_prezzo_2 = lds_acq.getitemnumber(ll_i,"prezzo_2")
	ld_prezzo_3 = lds_acq.getitemnumber(ll_i,"prezzo_3")
	ld_prezzo_4 = lds_acq.getitemnumber(ll_i,"prezzo_4")
	ld_prezzo_5 = lds_acq.getitemnumber(ll_i,"prezzo_5")
	ld_prezzo_ult = lds_acq.getitemnumber(ll_i,"prezzo_ult_acquisto")
	
	elemento.text = ls_cod_prodotto + " " + ls_cod_fornitore + " " + string(date(ldt_data_inizio_val))
	
	if ld_prezzo_1 <> 0 then
		ld_prezzo_1 = round( (ld_prezzo_1 / 1936.27) , il_dec )
	end if
	
	if ld_prezzo_2 <> 0 then
		ld_prezzo_2 = round( (ld_prezzo_2 / 1936.27) , il_dec )
	end if
	
	if ld_prezzo_3 <> 0 then
		ld_prezzo_3 = round( (ld_prezzo_3 / 1936.27) , il_dec )
	end if
	
	if ld_prezzo_4 <> 0 then
		ld_prezzo_4 = round( (ld_prezzo_4 / 1936.27) , il_dec )
	end if
	
	if ld_prezzo_5 <> 0 then
		ld_prezzo_5 = round( (ld_prezzo_5 / 1936.27) , il_dec )
	end if
	
	if ld_prezzo_ult <> 0 then
		ld_prezzo_ult = round( (ld_prezzo_ult / 1936.27) , il_dec )
	end if
	
	insert into listini_fornitori  
            	( cod_azienda,   
	            cod_prodotto,   
  	          	cod_fornitore,   
  	          	cod_valuta,   
  	          	data_inizio_val,   
            	des_listino_for,   
            	quantita_1,   
            	prezzo_1,   
            	sconto_1,   
            	quantita_2,   
            	prezzo_2,   
            	sconto_2,   
            	quantita_3,   
            	prezzo_3,   
            	sconto_3,   
            	quantita_4,   
            	prezzo_4,   
            	sconto_4,   
            	quantita_5,   
            	prezzo_5,   
            	sconto_5,   
            	flag_for_pref,   
            	prezzo_ult_acquisto,   
            	data_ult_acquisto,   
            	cod_misura,   
            	fat_conversione )  
	select      cod_azienda,   
   	         cod_prodotto,   
      	      cod_fornitore,   
         	   :ls_cod_valuta_euro,   
         		data_inizio_val,   
         	   des_listino_for,   
          	   quantita_1,   
            	:ld_prezzo_1,   
            	sconto_1,   
            	quantita_2,   
            	:ld_prezzo_2,   
            	sconto_2,   
            	quantita_3,   
            	:ld_prezzo_3,   
            	sconto_3,   
            	quantita_4,   
            	:ld_prezzo_4,   
            	sconto_4,   
            	quantita_5,   
            	:ld_prezzo_5,   
            	sconto_5,   
            	flag_for_pref,   
            	:ld_prezzo_ult,   
            	data_ult_acquisto,   
            	cod_misura,   
            	fat_conversione  
	from       	listini_fornitori	
	where       cod_azienda = :s_cs_xx.cod_azienda and
					cod_prodotto = :ls_cod_prodotto and
		 			cod_fornitore = :ls_cod_fornitore and
					cod_valuta = :ls_cod_valuta_lire and 
		 			data_inizio_val = :ldt_data_inizio_val;
					
	if sqlca.sqlcode <> 0 then
		lb_1.additem("Errore nella insert di listini_fornitori: " + sqlca.sqlerrtext)
		rollback;
		ll_err++
	else
		commit;
		ll_conv++
	end if	
	 
next

destroy lds_acq

elemento.text = ""

if ll_conv <> 0 then

	if ll_conv <> 1 then
		ls_i_o = "i sono stati convertiti in EURO"
	else
		ls_i_o = "o è stato convertito in EURO"
	end if

	lb_1.additem("- " + string(ll_conv) + " listin" + ls_i_o)

end if

if ll_err <> 0 then

	if ll_err <> 1 then
		ls_i_o = "i hanno provocato errori"
	else
		ls_i_o = "o ha provocato errori"
	end if

	lb_1.additem("- " + string(ll_err) + " listin" + ls_i_o)

end if

return 0
end function

public function integer wf_converti_listini_prod ();string    ls_cod_valuta_lire, ls_cod_valuta_euro, ls_cod_tipo, ls_flag[5], ls_i_o, ls_cod_prodotto_listino, &
			 ls_cod_versione

datetime  ldt_data_inizio_val

dec{4}    ld_variazione[5]

long      ll_progressivo, ll_i, ll_err, ll_conv, ll_prog_listino_produzione, ll_j

datastore lds_prod


lb_1.additem("Conversione di LISTINI PRODUZIONE")
lb_1.additem("")

ll_err = 0
ll_conv = 0

select stringa
into   :ls_cod_valuta_lire
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'CVL';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("Impossibile ottenere il codice della valuta lire")
	lb_1.additem("Errore nella select del parametro CVL da parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if	

select stringa
into   :ls_cod_valuta_euro
from	 parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'EUR';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("Impossibile ottenere il codice della valuta euro")
	lb_1.additem("Errore nella select del parametro CVL da parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if		
				
lds_prod = create datastore

lds_prod.dataobject = "d_ds_list_produz"
				
if lds_prod.settransobject(sqlca) < 0 then
	lb_1.additem("Impossibile ottenere i dati da convertire: errore nell'impostazione della transazione del datastore")
	destroy lds_prod
	return -1
end if

if lds_prod.retrieve(s_cs_xx.cod_azienda, ls_cod_valuta_lire) < 0 then
	lb_1.additem("Impossibile ottenere i dati da convertire: errore nella retrieve del datastore")
	return -1
end if

for ll_i = 1 to lds_prod.rowcount()
	
	ls_cod_tipo = lds_prod.getitemstring(ll_i,"cod_tipo_listino_prodotto")
	ldt_data_inizio_val = lds_prod.getitemdatetime(ll_i,"data_inizio_val")
	ll_progressivo = lds_prod.getitemnumber(ll_i,"progressivo")
	ls_cod_prodotto_listino = lds_prod.getitemstring(ll_i,"cod_prodotto_listino")
	ls_cod_versione = lds_prod.getitemstring(ll_i,"cod_versione")
	ll_prog_listino_produzione = lds_prod.getitemnumber(ll_i,"prog_listino_produzione")
	ls_flag[1] = lds_prod.getitemstring(ll_i,"flag_sconto_mag_prezzo_1")
	ls_flag[2] = lds_prod.getitemstring(ll_i,"flag_sconto_mag_prezzo_2")
	ls_flag[3] = lds_prod.getitemstring(ll_i,"flag_sconto_mag_prezzo_3")
	ls_flag[4] = lds_prod.getitemstring(ll_i,"flag_sconto_mag_prezzo_4")
	ls_flag[5] = lds_prod.getitemstring(ll_i,"flag_sconto_mag_prezzo_5")
	ld_variazione[1] = lds_prod.getitemnumber(ll_i,"variazione_1")
	ld_variazione[2] = lds_prod.getitemnumber(ll_i,"variazione_2")
	ld_variazione[3] = lds_prod.getitemnumber(ll_i,"variazione_3")
	ld_variazione[4] = lds_prod.getitemnumber(ll_i,"variazione_4")
	ld_variazione[5] = lds_prod.getitemnumber(ll_i,"variazione_5")
	
	elemento.text = ls_cod_tipo + " " + string(date(ldt_data_inizio_val)) + ", " + string(ll_progressivo)
	
	for ll_j = 1 to 5
		if ls_flag[ll_j] = 'P' then
			if ld_variazione[ll_j] <> 0 then
				ld_variazione[ll_j] = round( (ld_variazione[ll_j] / 1936.27) , il_dec )
			end if
		end if
	next
	
	insert into listini_produzione  
         		(cod_azienda,   
           		cod_tipo_listino_prodotto,   
           		cod_valuta,   
           		data_inizio_val,   
           		progressivo,   
           		cod_prodotto_listino,   
           		cod_versione,   
           		prog_listino_produzione,   
           		cod_prodotto_padre,   
           		num_sequenza,   
           		cod_prodotto_figlio,   
           		cod_gruppo_variante,   
           		progr,   
           		cod_prodotto,   
           		des_listino_produzione,   
           		minimo_fatt_altezza,   
           		minimo_fatt_larghezza,   
           		minimo_fatt_profondita,   
           		minimo_fatt_superficie,   
           		minimo_fatt_volume,   
           		flag_sconto_a_parte,   
           		flag_tipo_scaglioni,   
           		scaglione_1,   
           		scaglione_2,   
           		scaglione_3,   
           		scaglione_4,   
           		scaglione_5,   
           		flag_sconto_mag_prezzo_1,   
           		flag_sconto_mag_prezzo_2,   
           		flag_sconto_mag_prezzo_3,   
           		flag_sconto_mag_prezzo_4,   
           		flag_sconto_mag_prezzo_5,   
           		variazione_1,   
           		variazione_2,   
           		variazione_3,   
           		variazione_4,   
           		variazione_5,   
           		flag_origine_prezzo_1,   
           		flag_origine_prezzo_2,   
           		flag_origine_prezzo_3,   
           		flag_origine_prezzo_4,   
          		flag_origine_prezzo_5,   
           		provvigione_1,   
           		provvigione_2 )  
   select 		cod_azienda,   
            	cod_tipo_listino_prodotto,   
            	:ls_cod_valuta_euro,   
            	data_inizio_val,   
            	progressivo,   
            	cod_prodotto_listino,   
            	cod_versione,   
            	prog_listino_produzione,   
            	cod_prodotto_padre,   
            	num_sequenza,   
            	cod_prodotto_figlio,   
            	cod_gruppo_variante,   
            	progr,   
            	cod_prodotto,   
            	des_listino_produzione,   
            	minimo_fatt_altezza,   
            	minimo_fatt_larghezza,   
            	minimo_fatt_profondita,   
            	minimo_fatt_superficie,   
            	minimo_fatt_volume,   
            	flag_sconto_a_parte,   
            	flag_tipo_scaglioni,   
            	scaglione_1,   
            	scaglione_2,   
            	scaglione_3,   
            	scaglione_4,   
            	scaglione_5,   
            	flag_sconto_mag_prezzo_1,   
            	flag_sconto_mag_prezzo_2,   
            	flag_sconto_mag_prezzo_3,   
            	flag_sconto_mag_prezzo_4,   
            	flag_sconto_mag_prezzo_5,   
            	:ld_variazione[1],   
            	:ld_variazione[2],   
            	:ld_variazione[3],   
            	:ld_variazione[4],   
            	:ld_variazione[5],   
           		flag_origine_prezzo_1,   
           	 	flag_origine_prezzo_2,   
        	   	flag_origine_prezzo_3,   
        		   flag_origine_prezzo_4,   
            	flag_origine_prezzo_5,   
            	provvigione_1,   
            	provvigione_2  
   from        listini_produzione
	where       cod_azienda = :s_cs_xx.cod_azienda and
					cod_valuta = :ls_cod_valuta_lire and
					cod_tipo_listino_prodotto= :ls_cod_tipo and
		 			data_inizio_val = :ldt_data_inizio_val and
		 			progressivo = :ll_progressivo and
		 			cod_prodotto_listino = :ls_cod_prodotto_listino and
		 			cod_versione = :ls_cod_versione and
					prog_listino_produzione = :ll_prog_listino_produzione;
					
	if sqlca.sqlcode <> 0 then
		lb_1.additem("Errore nella insert di listini_produzione: " + sqlca.sqlerrtext)
		rollback;
		ll_err++
	else
		commit;
		ll_conv++
	end if
	 
next

destroy lds_prod

elemento.text = ""

if ll_conv <> 0 then

	if ll_conv <> 1 then
		ls_i_o = "i sono stati convertiti in EURO"
	else
		ls_i_o = "o è stato convertito in EURO"
	end if

	lb_1.additem("- " + string(ll_conv) + " listin" + ls_i_o)

end if

if ll_err <> 0 then

	if ll_err <> 1 then
		ls_i_o = "i hanno provocato errori"
	else
		ls_i_o = "o ha provocato errori"
	end if

	lb_1.additem("- " + string(ll_err) + " listin" + ls_i_o)

end if

return 0
end function

public function integer wf_converti_listini_vendite ();string     ls_cod_valuta_lire, ls_cod_valuta_euro, ls_cod_tipo, ls_flag[5], ls_i_o

datetime   ldt_data_inizio_val

dec{4}     ld_variazione[5]

long       ll_progressivo, ll_i, ll_err, ll_conv, ll_j

datastore  lds_ven


lb_1.additem("Conversione di LISTINI DI VENDITA")
lb_1.additem("")

ll_err = 0
ll_conv = 0

select stringa
into   :ls_cod_valuta_lire
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'CVL';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("Impossibile ottenere il codice della valuta lire")
	lb_1.additem("Errore nella select del parametro CVL da parametri_azienda: " + sqlca.sqlerrtext)		
	return -1
end if	

select stringa
into   :ls_cod_valuta_euro
from	 parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'EUR';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("Impossibile ottenere il codice della valuta euro")
	lb_1.additem("Errore nella select del parametro CVL da parametri_azienda: " + sqlca.sqlerrtext)	
	return -1
end if

lds_ven = create datastore

lds_ven.dataobject = "d_ds_list_ven"
				
if lds_ven.settransobject(sqlca) < 0 then
	lb_1.additem("Impossibile ottenere i dati da convertire: errore nell'impostazione della transazione del datastore")
	destroy lds_ven
	return -1
end if
		 
if lds_ven.retrieve(s_cs_xx.cod_azienda,ls_cod_valuta_lire) < 0 then
	lb_1.additem("Impossibile ottenere i dati da convertire: errore nella retrieve del datastore")
	destroy lds_ven
	return -1
end if

for ll_i = 1 to lds_ven.rowcount()
	
	ls_cod_tipo = lds_ven.getitemstring(ll_i,"cod_tipo_listino_prodotto")
	ldt_data_inizio_val = lds_ven.getitemdatetime(ll_i,"data_inizio_val")
	ll_progressivo = lds_ven.getitemnumber(ll_i,"progressivo")
	ls_flag[1] = lds_ven.getitemstring(ll_i,"flag_sconto_mag_prezzo_1")
	ls_flag[2] = lds_ven.getitemstring(ll_i,"flag_sconto_mag_prezzo_2")
	ls_flag[3] = lds_ven.getitemstring(ll_i,"flag_sconto_mag_prezzo_3")
	ls_flag[4] = lds_ven.getitemstring(ll_i,"flag_sconto_mag_prezzo_4")
	ls_flag[5] = lds_ven.getitemstring(ll_i,"flag_sconto_mag_prezzo_5")
	ld_variazione[1] = lds_ven.getitemnumber(ll_i,"variazione_1")
	ld_variazione[2] = lds_ven.getitemnumber(ll_i,"variazione_2")
	ld_variazione[3] = lds_ven.getitemnumber(ll_i,"variazione_3")
	ld_variazione[4] = lds_ven.getitemnumber(ll_i,"variazione_4")
	ld_variazione[5] = lds_ven.getitemnumber(ll_i,"variazione_5")
		   
	elemento.text = ls_cod_tipo + " " + string(date(ldt_data_inizio_val)) + ", " + string(ll_progressivo)
	
	for ll_j = 1 to 5
		if ls_flag[ll_j] = 'P' then
			if ld_variazione[ll_j] <> 0 then
				ld_variazione[ll_j] = round( (ld_variazione[ll_j] / 1936.27) , il_dec )
			end if
		end if
	next
	
	insert into listini_vendite  
         		( cod_azienda,   
           		cod_tipo_listino_prodotto,   
           		cod_valuta,   
           		data_inizio_val,   
           		progressivo,   
           		cod_cat_mer,   
           		cod_prodotto,   
           		cod_categoria,   
           		cod_cliente,   
           		des_listino_vendite,   
           		minimo_fatt_altezza,   
           		minimo_fatt_larghezza,   
           		minimo_fatt_profondita,   
           		minimo_fatt_superficie,   
           		minimo_fatt_volume,   
           		flag_sconto_a_parte,   
           		flag_tipo_scaglioni,   
           		scaglione_1,   
           		scaglione_2,   
           		scaglione_3,   
           		scaglione_4,   
           		scaglione_5,   
           		flag_sconto_mag_prezzo_1,   
           		flag_sconto_mag_prezzo_2,   
           		flag_sconto_mag_prezzo_3,   
           		flag_sconto_mag_prezzo_4,   
           		flag_sconto_mag_prezzo_5,   
           		variazione_1,   
           		variazione_2,   
           		variazione_3,   
           		variazione_4,   
           		variazione_5,   
           		flag_origine_prezzo_1,   
           		flag_origine_prezzo_2,   
           		flag_origine_prezzo_3,   
           		flag_origine_prezzo_4,   
           		flag_origine_prezzo_5 )  
   select      cod_azienda,   
            	cod_tipo_listino_prodotto,   
            	:ls_cod_valuta_euro,   
            	data_inizio_val,   
            	progressivo,   
            	cod_cat_mer,   
            	cod_prodotto,   
            	cod_categoria,   
            	cod_cliente,   
            	des_listino_vendite,   
            	minimo_fatt_altezza,   
            	minimo_fatt_larghezza,   
            	minimo_fatt_profondita,   
            	minimo_fatt_superficie,   
            	minimo_fatt_volume,   
            	flag_sconto_a_parte,   
            	flag_tipo_scaglioni,   
            	scaglione_1,   
            	scaglione_2,   
            	scaglione_3,   
            	scaglione_4,   
            	scaglione_5,   
            	flag_sconto_mag_prezzo_1,   
           		flag_sconto_mag_prezzo_2,   
            	flag_sconto_mag_prezzo_3,   
            	flag_sconto_mag_prezzo_4,   
            	flag_sconto_mag_prezzo_5,   
            	:ld_variazione[1],   
            	:ld_variazione[2],   
            	:ld_variazione[3],   
            	:ld_variazione[4],   
            	:ld_variazione[5],   
            	flag_origine_prezzo_1,   
            	flag_origine_prezzo_2,   
            	flag_origine_prezzo_3,   
            	flag_origine_prezzo_4,   
            	flag_origine_prezzo_5  
   from        listini_vendite
	where       cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_listino_prodotto = :ls_cod_tipo and
					cod_valuta = :ls_cod_valuta_lire and
					data_inizio_val = :ldt_data_inizio_val and
					progressivo = :ll_progressivo;

					
	if sqlca.sqlcode <> 0 then
		lb_1.additem("Errore nella insert di listini_vendite: " + sqlca.sqlerrtext)
		rollback;
		ll_err++
	else
		commit;
		ll_conv++
	end if	
	 
next

destroy lds_ven

elemento.text = ""

if ll_conv <> 0 then

	if ll_conv <> 1 then
		ls_i_o = "i sono stati convertiti in EURO"
	else
		ls_i_o = "o è stato convertito in EURO"
	end if

	lb_1.additem("- " + string(ll_conv) + " listin" + ls_i_o)

end if

if ll_err <> 0 then

	if ll_err <> 1 then
		ls_i_o = "i hanno provocato errori"
	else
		ls_i_o = "o ha provocato errori"
	end if

	lb_1.additem("- " + string(ll_err) + " listin" + ls_i_o)

end if

return 0
end function

public function integer wf_converti_gruppi_sconto ();string    ls_cod_valuta_lire, ls_cod_valuta_euro, ls_cod_gruppo, ls_i_o

long      ll_i, ll_err, ll_conv, ll_j, ll_progressivo

datetime  ldt_data_inizio_val

datastore lds_gruppi_sconto


lb_1.additem("Conversione di GRUPPI SCONTO")
lb_1.additem("")

ll_err = 0
ll_conv = 0

select stringa
into   :ls_cod_valuta_lire
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'CVL';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("Impossibile ottenere il codice della valuta lire")
	lb_1.additem("Errore nella select del parametro CVL da parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if	

select stringa
into   :ls_cod_valuta_euro
from	 parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'EUR';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("Impossibile ottenere il codice della valuta euro")
	lb_1.additem("Errore nella select del parametro CVL da parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if		
				
lds_gruppi_sconto = create datastore

lds_gruppi_sconto.dataobject = "d_ds_gruppi_sconto"
				
if lds_gruppi_sconto.settransobject(sqlca) < 0 then
	lb_1.additem("Impossibile ottenere i dati da convertire: errore nell'impostazione della transazione del datastore")
	destroy lds_gruppi_sconto
	return -1
end if

if lds_gruppi_sconto.retrieve(s_cs_xx.cod_azienda, ls_cod_valuta_lire) < 0 then
	lb_1.additem("Impossibile ottenere i dati da convertire: errore nella retrieve del datastore")
	return -1
end if

for ll_i = 1 to lds_gruppi_sconto.rowcount()
	
	ls_cod_gruppo = lds_gruppi_sconto.getitemstring(ll_i,"cod_gruppo_sconto")
	ldt_data_inizio_val = lds_gruppi_sconto.getitemdatetime(ll_i,"data_inizio_val")
	ll_progressivo = lds_gruppi_sconto.getitemnumber(ll_i,"progressivo")
	
	elemento.text = ls_cod_gruppo + " " + string(date(ldt_data_inizio_val)) + ", " + string(ll_progressivo)
	
	insert into gruppi_sconto  
               (cod_azienda,   
		         cod_gruppo_sconto,   
           		cod_valuta,   
           		data_inizio_val,   
           		progressivo,   
           		cod_agente,   
           		cod_cliente,   
           		cod_livello_prod_1,   
					cod_livello_prod_2,   
					cod_livello_prod_3,   
					cod_livello_prod_4,   
					cod_livello_prod_5,   
					sconto_1,   
					sconto_2,   
					sconto_3,   
					sconto_4,   
					sconto_5,   
					sconto_6,   
					sconto_7,   
					sconto_8,   
					sconto_9,   
					sconto_10,   
					provvigione_1,   
					provvigione_2 )  
	select      cod_azienda,   
					cod_gruppo_sconto,   
					:ls_cod_valuta_euro,
					data_inizio_val,   
					progressivo,   
					cod_agente,   
					cod_cliente,   
					cod_livello_prod_1,   
					cod_livello_prod_2,   
					cod_livello_prod_3,   
					cod_livello_prod_4,   
					cod_livello_prod_5,   
					sconto_1,   
					sconto_2,   
					sconto_3,   
					sconto_4,   
					sconto_5,   
					sconto_6,   
					sconto_7,   
					sconto_8,   
					sconto_9,   
					sconto_10,   
					provvigione_1,   
					provvigione_2  
	from        gruppi_sconto
	where       cod_azienda = :s_cs_xx.cod_azienda and
					cod_gruppo_sconto = :ls_cod_gruppo and
					cod_valuta = :ls_cod_valuta_lire and
					data_inizio_val = :ldt_data_inizio_val and
					progressivo = :ll_progressivo;
	
	if sqlca.sqlcode <> 0 then
		lb_1.additem("Errore nella insert di gruppi_sconto: " + sqlca.sqlerrtext)
		rollback;
		ll_err++
	else
		commit;
		ll_conv++
	end if
	 
next

destroy lds_gruppi_sconto

elemento.text = ""

if ll_conv <> 0 then

	if ll_conv <> 1 then
		ls_i_o = "i sconto sono stati convertiti in EURO"
	else
		ls_i_o = "o sconto è stato convertito in EURO"
	end if

	lb_1.additem("- " + string(ll_conv) + " grupp" + ls_i_o)

end if

if ll_err <> 0 then

	if ll_err <> 1 then
		ls_i_o = "i sconto hanno provocato errori"
	else
		ls_i_o = "o sconto ha provocato errori"
	end if

	lb_1.additem("- " + string(ll_err) + " grupp" + ls_i_o)

end if

return 0
end function

public function integer wf_converti_cli_for ();string    ls_cod_valuta_lire, ls_cod_valuta_euro, ls_i_o

long      ll_prima, ll_dopo, ll_clienti, ll_fornitori

lb_1.additem("Conversione di CLIENTI E FORNITORI")
lb_1.additem("")

select stringa
into   :ls_cod_valuta_lire
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'CVL';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("Impossibile ottenere il codice della valuta lire")
	lb_1.additem("Errore nella select del parametro CVL da parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if	

select stringa
into   :ls_cod_valuta_euro
from	 parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'EUR';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("Impossibile ottenere il codice della valuta euro")
	lb_1.additem("Errore nella select del parametro CVL da parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if		
				
select count(*)
into   :ll_prima
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta_euro;
		 
if sqlca.sqlcode < 0 then
	lb_1.additem("Errore nella select di anag_clienti: " + sqlca.sqlerrtext)
	return -1
end if

update anag_clienti
set    cod_valuta = :ls_cod_valuta_euro
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta_lire;
		 
if sqlca.sqlcode < 0 then
	lb_1.additem("Errore nella update di anag_clienti: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

select count(*)
into   :ll_dopo
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta_euro;
		 
if sqlca.sqlcode < 0 then
	lb_1.additem("Errore nella select di anag_clienti: " + sqlca.sqlerrtext)
	return -1
end if

ll_clienti = ll_dopo - ll_prima

select count(*)
into   :ll_prima
from   anag_fornitori
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta_euro;
		 
if sqlca.sqlcode < 0 then
	lb_1.additem("Errore nella select di anag_fornitori: " + sqlca.sqlerrtext)
	return -1
end if

update anag_fornitori
set    cod_valuta = :ls_cod_valuta_euro
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta_lire;
		 
if sqlca.sqlcode < 0 then
	lb_1.additem("Errore nella update di anag_fornitori: " + sqlca.sqlerrtext)
	rollback;
	return -1
end if

select count(*)
into   :ll_dopo
from   anag_fornitori
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :ls_cod_valuta_euro;
		 
if sqlca.sqlcode < 0 then
	lb_1.additem("Errore nella select di anag_fornitori: " + sqlca.sqlerrtext)
	return -1
end if

ll_fornitori = ll_dopo - ll_prima

commit;

if ll_clienti <> 0 then

	if ll_clienti <> 1 then
		ls_i_o = "i con valuta LIRE sono stati convertiti alla valuta EURO"
	else
		ls_i_o = "e con valuta LIRE è stato convertito alla valuta EURO"
	end if

	lb_1.additem("- " + string(ll_clienti) + " client" + ls_i_o)

end if

if ll_fornitori <> 0 then

	if ll_fornitori <> 1 then
		ls_i_o = "i con valuta LIRE sono stati convertiti alla valuta EURO"
	else
		ls_i_o = "e con valuta LIRE è stato convertito alla valuta EURO"
	end if

	lb_1.additem("- " + string(ll_fornitori) + " fornitor" + ls_i_o)

end if

return 0
end function

public function integer wf_converti_aperti ();string  ls_cod_lire, ls_cod_euro, ls_testata, ls_dettaglio, ls_cod_valuta, ls_sql, ls_prezzo, ls_prog, ls_i_o,&
		  ls_documento, ls_messaggio

dec{4}  ld_prezzo

long    ll_anno_registrazione, ll_num_registrazione, ll_prog_riga, ll_count, ll_imp, ll_err, ll_skip, ll_i,&
		  ll_err_calc

boolean lb_det_err

uo_calcola_documento_euro luo_doc

n_tran sqlcb


sqlcb = create n_tran
sqlcb.dbms = sqlca.dbms
sqlcb.dbparm = sqlca.dbparm
sqlcb.autocommit = sqlca.autocommit
sqlcb.database = sqlca.database
sqlcb.dbpass = sqlca.dbpass
sqlcb.lock = sqlca.lock
sqlcb.logid = sqlca.logid
sqlcb.logpass = sqlca.logpass
sqlcb.servername = sqlca.servername
sqlcb.userid = sqlca.userid

connect using sqlcb;

if sqlcb.sqlcode = -1 then
	lb_1.additem("Errore nella connessione della transaction sqlcb: " + sqlcb.sqlerrtext)
	return -1
end if

ll_imp = 0
ll_err = 0
ll_skip = 0
ll_err_calc = 0

select stringa
into   :ls_cod_lire
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'CVL';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("- Impossibile ottenere il codice della valuta Lire da parametri_azienda")
	lb_1.additem("  Errore nella select di parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if

select stringa
into   :ls_cod_euro
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'EUR';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("- Impossibile ottenere il codice della valuta Euro da parametri_azienda")
	lb_1.additem("  Errore nella select di parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if

choose case is_tipo_doc
	
	case "ord_ven"
	
		lb_1.additem("Conversione degli ordini di vendita APERTI")
		lb_1.additem("")
		ls_testata = "tes_ord_ven"
		ls_dettaglio = "det_ord_ven"
		ls_prezzo = "prezzo_vendita"
		ls_prog = "prog_riga_ord_ven"
		ls_documento = "ordine di vendita"
	
	case "ord_acq"
		
		lb_1.additem("Conversione degli ordini di acquisto APERTI")
		lb_1.additem("")
		ls_testata = "tes_ord_acq"
		ls_dettaglio = "det_ord_acq"
		ls_prezzo = "prezzo_acquisto"
		ls_prog = "prog_riga_ordine_acq"
		ls_documento = "ordine di acquisto"
	
	case else
	
		lb_1.additem("Nessun tipo documento specificato per la conversione!")
		lb_1.additem("Scegliere un tipo di documento prima di continuare")		
		return -1

end choose

ls_sql = "select anno_registrazione, num_registrazione, cod_valuta from " + ls_testata + " where cod_azienda = '" + &
			s_cs_xx.cod_azienda + "' and cod_valuta = '" + ls_cod_lire + "' order by anno_registrazione, num_registrazione"
			
prepare sqlsa from :ls_sql;

declare testata dynamic cursor for sqlsa;

open dynamic testata;

if sqlca.sqlcode <> 0 then
	lb_1.additem("- Impossibile ottenere i dati da convertire")
	lb_1.additem("  Errore nella open del cursore testata: " + sqlca.sqlerrtext)		
	return -1
end if	

do while true
	
	fetch testata
	into  :ll_anno_registrazione,
			:ll_num_registrazione,
			:ls_cod_valuta;
	
	if sqlca.sqlcode = -1 then
		lb_1.additem("- Impossibile ottenere i dati da convertire")
		lb_1.additem("  Errore nella fetch del cursore testata: " + sqlca.sqlerrtext)		
		close testata;
		return -1
	elseif sqlca.sqlcode = 100 then
		close testata;
		exit;
	end if
	
	elemento.text = string(ll_anno_registrazione) + " / " + string(ll_num_registrazione)
	
	if is_tipo_doc = "ord_acq" then
		
		ls_sql = "select count(*) from " + ls_dettaglio + " where cod_azienda = '" + s_cs_xx.cod_azienda + &
					"' and anno_registrazione = " + string(ll_anno_registrazione) + " and num_registrazione = " + &
					string(ll_num_registrazione) + " and (flag_saldo = 'S' or quan_arrivata <> 0)"
	else			
	
		ls_sql = "select count(*) from " + ls_dettaglio + " where cod_azienda = '" + s_cs_xx.cod_azienda + &
					"' and anno_registrazione = " + string(ll_anno_registrazione) + " and num_registrazione = " + &
					string(ll_num_registrazione) + " and flag_evasione <> 'A'"
					
	end if							
					
	prepare sqlsa from :ls_sql;
	
	declare controllo dynamic cursor for sqlsa;
	
	open controllo;
	
	if sqlca.sqlcode <> 0 then
		lb_1.additem("- Impossibile controllare l'evasione di " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
		lb_1.additem("  Errore nella open del cursore controllo: " + sqlca.sqlerrtext)
		lb_1.additem("")
		ll_err++
		continue
	end if
	
	fetch controllo into :ll_count;
	
	if sqlca.sqlcode = -1 then
		lb_1.additem("- Impossibile controllare l'evasione di " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
		lb_1.additem("  Errore nella fetch del cursore controllo: " + sqlca.sqlerrtext)
		lb_1.additem("")		
		close controllo;
		ll_err++
		continue
	elseif ll_count <> 0 then
		close controllo;
		ll_skip++
		continue
	end if
	
	close controllo;	
	
	ls_sql = "select " + ls_prezzo + ", " + ls_prog + " from " + ls_dettaglio + " where cod_azienda = '" + &
			   s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(ll_anno_registrazione) + &
				" and num_registrazione = " + string(ll_num_registrazione)
				
	prepare sqlsa from :ls_sql;
	
	declare dettaglio dynamic cursor for sqlsa;
	
	open dettaglio;
	
	if sqlca.sqlcode <> 0 then
		lb_1.additem("- Impossibile leggere i dettagli di " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
		lb_1.additem("  Errore nella open del cursore dettaglio: " + sqlca.sqlerrtext)
		ll_err++
		continue
	end if
	
	do while true
		
		fetch dettaglio
		into  :ld_prezzo,
				:ll_prog_riga;
				
		if sqlca.sqlcode = -1 then
			
			lb_1.additem("- Impossibile leggere i dettagli di " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
			lb_1.additem("  Errore nella fetch del cursore dettaglio: " + sqlca.sqlerrtext)  
			lb_1.additem("")
			ll_err++
			lb_det_err = true
			exit
		elseif sqlca.sqlcode = 100 then			
			exit
		end if		
		
		choose case is_tipo_doc
				
			case "ord_ven"
				
				ld_prezzo = round((ld_prezzo / 1936.27),il_dec)				
				
				update det_ord_ven
				set    prezzo_vendita = :ld_prezzo
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione and
						 prog_riga_ord_ven = :ll_prog_riga
				using  sqlcb;
						 
				if sqlcb.sqlcode <> 0 then
					lb_1.additem("- Errore nella update di det_ord_ven: " + sqlcb.sqlerrtext)
					lb_1.additem("  Documento: " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " riga: " + string(ll_prog_riga))										
					ll_err++
					rollback using sqlcb;
					lb_det_err = true
					exit
				end if				
				
			case "ord_acq"
				
				ld_prezzo = round((ld_prezzo / 1936.27),il_dec)		
				
				update det_ord_acq
				set    prezzo_acquisto = :ld_prezzo
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione and
						 prog_riga_ordine_acq = :ll_prog_riga
				using  sqlcb;
						 
				if sqlcb.sqlcode <> 0 then
					lb_1.additem("- Errore nella update di det_ord_acq: " + sqlcb.sqlerrtext)
					lb_1.additem("  Documento: " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " riga: " + string(ll_prog_riga))
					ll_err++
					rollback using sqlcb;
					lb_det_err = true
					exit				
				end if
				
		end choose
		
	loop
			
	close dettaglio;
	
	if lb_det_err = false then
		
		choose case is_tipo_doc
		
			case "ord_ven"
				
				update tes_ord_ven
				set    cod_valuta = :ls_cod_euro,
						 cambio_ven = 1
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione
				using  sqlcb;
						 
				if sqlcb.sqlcode <> 0 then
					lb_1.additem("- Errore nella update di tes_ord_ven: " + sqlcb.sqlerrtext)
					lb_1.additem("  Documento: " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
					ll_err++
					rollback using sqlcb;
					continue;
				else
					ll_imp++
					commit using sqlcb;	
				end if
				
				luo_doc = create uo_calcola_documento_euro
		
				if luo_doc.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,is_tipo_doc,ls_messaggio) <> 0 then
					lb_1.additem("- Errore nel calcolo di " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
					lb_1.additem("  " + ls_messaggio)  
					lb_1.additem("")
					ll_err_calc++
				end if	
		
				destroy luo_doc
		
			case "ord_acq"
				
				update tes_ord_acq
				set    cod_valuta = :ls_cod_euro,
						 cambio_acq = 1
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione
			 	using  sqlcb;		 
						 
				if sqlcb.sqlcode <> 0 then
					lb_1.additem("- Errore nella update di tes_ord_acq: " + sqlcb.sqlerrtext)
					lb_1.additem("  Documento: " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
					ll_err++
					rollback using sqlcb;
					continue;
				else
					ll_imp++
					commit using sqlcb;	
				end if
				
				luo_doc = create uo_calcola_documento_euro
		
				if luo_doc.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,is_tipo_doc,ls_messaggio) <> 0 then
					lb_1.additem("- Errore nel calcolo di " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
					lb_1.additem("  " + ls_messaggio)  
					lb_1.additem("")
					ll_err_calc++
				end if	
		
				destroy luo_doc
			
		end choose		
		
	end if	
	
loop

close testata;

elemento.text = ""

if ll_imp <> 1 then
	ls_i_o = "i sono stati convertiti in Euro"
else
	ls_i_o = "o è stato convertito in Euro"
end if

lb_1.additem("- " + string(ll_imp) + " document" + ls_i_o)

if ll_err <> 0 then

	if ll_err <> 1 then
		ls_i_o = "i hanno provocato errori durante la conversione"
	else
		ls_i_o = "o ha provocato errori durante la conversione"
	end if

	lb_1.additem("- " + string(ll_err) + " document" + ls_i_o)

end if

if ll_skip <> 0 then

	if ll_skip <> 1 then
		ls_i_o = "i sono stati saltati perchè non in stato evasione APERTO"
	else
		ls_i_o = "o è stato saltato perchè non in stato evasione APERTO"
	end if

	lb_1.additem("- " + string(ll_skip) + " document" + ls_i_o)

end if

if ll_err_calc <> 0 then

	if ll_err_calc <> 1 then
		ls_i_o = "i hanno causato errori nel calcolo dopo la conversione"
	else
		ls_i_o = "o ha causato errori nel calcolo dopo la conversione"
	end if

	lb_1.additem("- " + string(ll_err_calc) + " document" + ls_i_o)

end if

disconnect using sqlcb;

if sqlcb.sqlcode = -1 then
	lb_1.additem("Errore nella disconnessione della transaction sqlcb: " + sqlcb.sqlerrtext)	
end if

destroy sqlcb;

return 0
end function

public function integer wf_converti_parziali ();string  ls_cod_lire, ls_cod_euro, ls_testata, ls_dettaglio, ls_cod_valuta, ls_sql, ls_prezzo, ls_prog, ls_i_o,&
		  ls_documento, ls_messaggio, ls_residuo, ls_quan_1, ls_quan_2, ls_riferimento

dec{4}  ld_prezzo, ld_residuo, ld_quan_1, ld_quan_2

long    ll_anno_registrazione, ll_num_registrazione, ll_prog_riga, ll_count, ll_imp, ll_err, ll_skip, ll_i,&
		  ll_err_calc, ll_num_righe, ll_prog_new, ll_num_reg_max

boolean lb_det_err

uo_calcola_documento_euro luo_doc

n_tran sqlcb


choose case is_tipo_doc
	
	case "ord_ven"
		
		lb_1.additem("")
		lb_1.additem("Inizio operazione " + string(today()) + " " + string(now()))
		lb_1.additem("")
		lb_1.additem("Conversione degli ordini di vendita PARZIALI")
		lb_1.additem("")
		ls_testata = "tes_ord_ven"
		ls_dettaglio = "det_ord_ven"
		ls_prezzo = "prezzo_vendita"
		ls_prog = "prog_riga_ord_ven"
		ls_documento = "ordine di vendita"
		ls_residuo = "quan_ordine - quan_evasa"
		ls_quan_1 = "quan_ordine"
		ls_quan_2 = "quan_evasa"
	
	case "ord_acq"
		
		lb_1.additem("")
		lb_1.additem("Inizio operazione " + string(today()) + " " + string(now()))
		lb_1.additem("")
		lb_1.additem("Conversione degli ordini di acquisto PARZIALI")
		lb_1.additem("")
		ls_testata = "tes_ord_acq"
		ls_dettaglio = "det_ord_acq"
		ls_prezzo = "prezzo_acquisto"
		ls_prog = "prog_riga_ordine_acq"
		ls_documento = "ordine di acquisto"
		ls_residuo = "quan_ordinata - quan_arrivata"
		ls_quan_1 = "quan_ordinata"
		ls_quan_2 = "quan_arrivata"
	
	case "off_ven"
		
		return 0
		
	case "off_acq"	
		
		return 0
	
	case else
	
		lb_1.additem("Nessun tipo documento specificato per la conversione!")
		lb_1.additem("Scegliere un tipo di documento prima di continuare")		
		return -1

end choose

sqlcb = create n_tran
sqlcb.dbms = sqlca.dbms
sqlcb.dbparm = sqlca.dbparm
sqlcb.autocommit = sqlca.autocommit
sqlcb.database = sqlca.database
sqlcb.dbpass = sqlca.dbpass
sqlcb.lock = sqlca.lock
sqlcb.logid = sqlca.logid
sqlcb.logpass = sqlca.logpass
sqlcb.servername = sqlca.servername
sqlcb.userid = sqlca.userid

connect using sqlcb;

if sqlcb.sqlcode = -1 then
	lb_1.additem("Errore nella connessione della transaction sqlcb: " + sqlcb.sqlerrtext)
	return -1
end if

ll_imp = 0
ll_err = 0
ll_skip = 0
ll_err_calc = 0

select stringa
into   :ls_cod_lire
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'CVL';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("- Impossibile ottenere il codice della valuta Lire da parametri_azienda")
	lb_1.additem("  Errore nella select di parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if

select stringa
into   :ls_cod_euro
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'EUR';
		 
if sqlca.sqlcode <> 0 then
	lb_1.additem("- Impossibile ottenere il codice della valuta Euro da parametri_azienda")
	lb_1.additem("  Errore nella select di parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if

ls_sql = "select anno_registrazione, num_registrazione from " + ls_testata + " where cod_azienda = '" + &
			s_cs_xx.cod_azienda + "' and cod_valuta = '" + ls_cod_lire + "' order by anno_registrazione, num_registrazione" 
			
prepare sqlsa from :ls_sql;

declare testata dynamic cursor for sqlsa;

open dynamic testata;

if sqlca.sqlcode <> 0 then
	lb_1.additem("- Impossibile ottenere i dati da convertire")
	lb_1.additem("  Errore nella open del cursore testata: " + sqlca.sqlerrtext)		
	return -1
end if	

do while true
	
	fetch testata
	into  :ll_anno_registrazione,
			:ll_num_registrazione;			
	
	if sqlca.sqlcode = -1 then
		lb_1.additem("- Impossibile ottenere i dati da convertire")
		lb_1.additem("  Errore nella fetch del cursore testata: " + sqlca.sqlerrtext)		
		close testata;
		return -1
	elseif sqlca.sqlcode = 100 then
		close testata;
		exit;
	end if
	
	elemento.text = string(ll_anno_registrazione) + " / " + string(ll_num_registrazione)
	
	ls_riferimento = "Generato da ordine in LIRE " + string(ll_anno_registrazione) + " / " + string(ll_num_registrazione)
	
	choose case is_tipo_doc
			
		case "ord_ven"
			
			select max(num_registrazione) + 1
			into   :ll_num_reg_max
			from   tes_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione;
			
		case "ord_acq"
			
			select max(num_registrazione) + 1
			into   :ll_num_reg_max
			from   tes_ord_acq
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione;
			
	end choose
	
	if sqlca.sqlcode <> 0 then
		lb_1.additem("- Impossibile ottenere il massimo numero di registrazione")
		lb_1.additem("  Errore nella select di " + ls_testata + ": " + sqlca.sqlerrtext)		
		close testata;
		return -1
	end if	
	
	ls_sql = "select count(*) from " + ls_dettaglio + " where cod_azienda = '" + s_cs_xx.cod_azienda + &
				"' and anno_registrazione = " + string(ll_anno_registrazione) + " and num_registrazione = " + &
				string(ll_num_registrazione)
				
	prepare sqlsa from :ls_sql;
	
	declare num_det dynamic cursor for sqlsa;
	
	open num_det;
	
	if sqlca.sqlcode <> 0 then
		lb_1.additem("- Impossibile controllare l'evasione di " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
		lb_1.additem("  Errore nella open del cursore num_det: " + sqlca.sqlerrtext)
		lb_1.additem("")
		ll_err++
		continue
	end if
	
	fetch num_det into :ll_num_righe;
	
	if sqlca.sqlcode = -1 then
		lb_1.additem("- Impossibile controllare l'evasione di " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
		lb_1.additem("  Errore nella fetch del cursore num_det: " + sqlca.sqlerrtext)
		lb_1.additem("")		
		close num_det;
		ll_err++
		continue
	end if
	
	close num_det;
	
	if isnull(ll_num_righe) or ll_num_righe = 0 then
		ll_skip++
		continue
	end if
	
	if is_tipo_doc = "ord_acq" then
		
		ls_sql = "select count(*) from " + ls_dettaglio + " where cod_azienda = '" + s_cs_xx.cod_azienda + &
					"' and anno_registrazione = " + string(ll_anno_registrazione) + " and num_registrazione = " + &
					string(ll_num_registrazione) + " and (flag_saldo = 'N' and quan_arrivata <> 0)"
	else			
	
		ls_sql = "select count(*) from " + ls_dettaglio + " where cod_azienda = '" + s_cs_xx.cod_azienda + &
					"' and anno_registrazione = " + string(ll_anno_registrazione) + " and num_registrazione = " + &
					string(ll_num_registrazione) + " and flag_evasione = 'P'"
					
	end if							
					
	prepare sqlsa from :ls_sql;
	
	declare controllo dynamic cursor for sqlsa;
	
	open controllo;
	
	if sqlca.sqlcode <> 0 then
		lb_1.additem("- Impossibile controllare l'evasione di " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
		lb_1.additem("  Errore nella open del cursore controllo: " + sqlca.sqlerrtext)
		lb_1.additem("")
		ll_err++
		continue
	end if
	
	fetch controllo into :ll_count;
	
	if sqlca.sqlcode = -1 then
		lb_1.additem("- Impossibile controllare l'evasione di " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
		lb_1.additem("  Errore nella fetch del cursore controllo: " + sqlca.sqlerrtext)
		lb_1.additem("")		
		close controllo;
		ll_err++
		continue
	end if
	
	close controllo;
			
	if ll_count = 0 then
		
		if is_tipo_doc = "ord_acq" then
		
			ls_sql = "select count(*) from " + ls_dettaglio + " where cod_azienda = '" + s_cs_xx.cod_azienda + &
						"' and anno_registrazione = " + string(ll_anno_registrazione) + " and num_registrazione = " + &
						string(ll_num_registrazione) + " and (flag_saldo = 'N' and quan_arrivata = 0)"
		else			
	
			ls_sql = "select count(*) from " + ls_dettaglio + " where cod_azienda = '" + s_cs_xx.cod_azienda + &
						"' and anno_registrazione = " + string(ll_anno_registrazione) + " and num_registrazione = " + &
						string(ll_num_registrazione) + " and flag_evasione = 'A'"
					
		end if							
					
		prepare sqlsa from :ls_sql;
	
		declare controllo_2 dynamic cursor for sqlsa;
	
		open controllo_2;
	
		if sqlca.sqlcode <> 0 then
			lb_1.additem("- Impossibile controllare l'evasione di " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
			lb_1.additem("  Errore nella open del cursore controllo: " + sqlca.sqlerrtext)
			lb_1.additem("")
			ll_err++
			continue
		end if
	
		fetch controllo_2 into :ll_count;
	
		if sqlca.sqlcode = -1 then
			lb_1.additem("- Impossibile controllare l'evasione di " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
			lb_1.additem("  Errore nella fetch del cursore controllo: " + sqlca.sqlerrtext)
			lb_1.additem("")		
			close controllo_2;
			ll_err++
			continue
		end if
		
		close controllo_2;
		
		if ll_count = 0  or ll_count = ll_num_righe then
			ll_skip++
			continue		
		end if		
				
	end if
	
	choose case is_tipo_doc
		
		case "ord_ven"				
		
			insert into tes_ord_ven  
        					( cod_azienda,   
        					anno_registrazione,   
        					num_registrazione,   
        					data_registrazione,   
        					ora_registrazione,   
        					cod_operatore,   
        					cod_cliente,   
        					cod_tipo_ord_ven,   
        					cod_des_cliente,   
        					rag_soc_1,   
        					rag_soc_2,   
        					indirizzo,   
         				localita,   
           				frazione,   
           				cap,   
           				provincia,   
           				cod_deposito,   
           				cod_ubicazione,   
           				cod_valuta,   
           				cambio_ven,   
           				cod_tipo_listino_prodotto,   
           				cod_pagamento,   
           				sconto,   
           				cod_agente_1,   
           				cod_agente_2,   
           				cod_banca,   
           				num_ord_cliente,   
           				data_ord_cliente,   
           				cod_imballo,   
           				aspetto_beni,   
           				peso_netto,   
           				peso_lordo,   
           				num_colli,   
           				cod_vettore,   
           				cod_inoltro,   
           				cod_mezzo,   
           				causale_trasporto,   
           				cod_porto,   
           				cod_resa,   
           				nota_testata,   
           				nota_piede,   
           				flag_fuori_fido,   
           				flag_blocco,   
           				flag_evasione,   
           				flag_riep_bol,   
           				flag_riep_fat,   
           				tot_val_evaso,   
           				tot_val_ordine,   
           				data_consegna,   
           				cod_banca_clien_for,   
           				cod_causale,   
          			 	flag_stampato,   
           				cod_giro_consegna,   
           				flag_stampata_bcl,   
           				tot_val_ordine_valuta,   
           				tot_merci,   
           				tot_spese_trasporto,   
           				tot_spese_imballo,   
           				tot_spese_bolli,   
           				tot_spese_varie,   
           				tot_sconto_cassa,   
           				tot_sconti_commerciali,   
           				imponibile_provvigioni_1,   
           				imponibile_provvigioni_2,   
           				tot_provvigioni_1,   
           				tot_provvigioni_2,   
           				importo_iva,   
           				imponibile_iva,   
           				importo_iva_valuta,   
           				imponibile_iva_valuta,   
           				flag_doc_suc_tes,   
           				flag_doc_suc_pie,   
           				flag_st_note_tes,   
           				flag_st_note_pie )  
         select 		cod_azienda,   
           				anno_registrazione,   
           				:ll_num_reg_max,   
           				data_registrazione,   
           				ora_registrazione,   
           				cod_operatore,   
           				cod_cliente,   
           				cod_tipo_ord_ven,   
           				cod_des_cliente,   
           				rag_soc_1,   
           				rag_soc_2,   
           				indirizzo,   
           				localita,   
           				frazione,   
           				cap,   
           				provincia,   
           				cod_deposito,   
           				cod_ubicazione,   
           				:ls_cod_euro,   
           				1,   
           				cod_tipo_listino_prodotto,   
           				cod_pagamento,   
           				sconto,   
           				cod_agente_1,   
           				cod_agente_2,   
           				cod_banca,   
           				num_ord_cliente,   
          				data_ord_cliente,   
           				cod_imballo,   
         				aspetto_beni,   
           				peso_netto,   
           				peso_lordo,   
           				num_colli,   
           				cod_vettore,   
           				cod_inoltro,   
           				cod_mezzo,   
           				causale_trasporto,   
           				cod_porto,   
           				cod_resa,   
           				:ls_riferimento,   
           				nota_piede,   
           				flag_fuori_fido,   
           				flag_blocco,   
           				'A',   
           				flag_riep_bol,   
           				flag_riep_fat,   
           				tot_val_evaso,   
           				tot_val_ordine,   
           				data_consegna,   
           				cod_banca_clien_for,   
           				cod_causale,   
           				flag_stampato,   
           				cod_giro_consegna,   
           				flag_stampata_bcl,   
           				tot_val_ordine_valuta,   
           				tot_merci,   
           				tot_spese_trasporto,   
           				tot_spese_imballo,   
           				tot_spese_bolli,   
           				tot_spese_varie,   
           				tot_sconto_cassa,   
           				tot_sconti_commerciali,   
           				imponibile_provvigioni_1,   
           				imponibile_provvigioni_2,   
           				tot_provvigioni_1,   
           				tot_provvigioni_2,   
           				importo_iva,   
           				imponibile_iva,   
           				importo_iva_valuta,   
           				imponibile_iva_valuta,   
           				flag_doc_suc_tes,   
           				flag_doc_suc_pie,   
           				flag_st_note_tes,   
           				flag_st_note_pie  
         from 			tes_ord_ven
			where  		cod_azienda = :s_cs_xx.cod_azienda and
					 		anno_registrazione = :ll_anno_registrazione and
					 		num_registrazione = :ll_num_registrazione
			using  		sqlcb;
										 
			if sqlcb.sqlcode <> 0 then
				lb_1.additem("- Errore nella insert di tes_ord_ven: " + sqlcb.sqlerrtext)
				lb_1.additem("  Documento: " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
				ll_err++
				rollback using sqlcb;
				continue;
			else
				commit using sqlcb;	
			end if
			
			update tes_ord_ven
			set    flag_evasione = 'E'
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione
			using  sqlcb;
			
			if sqlcb.sqlcode <> 0 then
				lb_1.additem("- Errore nella update di tes_ord_ven: " + sqlcb.sqlerrtext)
				lb_1.additem("  Documento: " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
				ll_err++
				rollback using sqlcb;
				continue;
			else
				commit using sqlcb;	
			end if
		
		case "ord_acq"
							  
			insert into tes_ord_acq  
         				( cod_azienda,   
          				anno_registrazione,   
          				num_registrazione,   
           				cod_tipo_ord_acq,   
           				data_registrazione,   
           				cod_operatore,   
           				cod_fornitore,   
           				rag_soc_1,   
           				rag_soc_2,   
           				indirizzo,   
         				localita,   
         				frazione,   
         				cap,   
         				provincia,   
         				cod_fil_fornitore,   
         				cod_des_fornitore,   
         				cod_deposito,   
         				cod_valuta,   
         				cambio_acq,   
         				cod_tipo_listino_prodotto,   
         				cod_pagamento,   
         				sconto,   
         				cod_banca_clien_for,   
         				num_ord_fornitore,   
         				data_ord_fornitore,   
         				cod_imballo,   
         				cod_vettore,   
         				cod_inoltro,   
         				cod_mezzo,   
         				cod_porto,   
         				cod_resa,   
         				flag_blocco,   
         				flag_evasione,   
         				tot_val_ordine,   
         				tot_val_evaso,   
         				nota_testata,   
         				nota_piede,   
         				data_consegna,   
         				cod_banca,   
         				data_consegna_fornitore,   
         				flag_doc_suc_tes,   
         				flag_doc_suc_pie,   
         				flag_st_note_tes,   
         				flag_st_note_pie,   
         				tot_val_ordine_valuta,   
         				imponibile_iva,   
         				importo_iva,   
         				imponibile_iva_valuta,   
         				importo_iva_valuta,   
         				tot_merci,   
         				tot_spese_trasporto,   
         				tot_spese_imballo,   
         				tot_spese_bolli,   
         				tot_spese_varie,   
         				tot_spese_cassa,   
         				tot_sconti_commerciali )  
         select 		cod_azienda,   
          				anno_registrazione,   
           				:ll_num_reg_max,   
           				cod_tipo_ord_acq,   
           				data_registrazione,   
           				cod_operatore,   
           				cod_fornitore,   
           				rag_soc_1,   
           				rag_soc_2,   
           				indirizzo,   
           				localita,   
           				frazione,   
           				cap,   
           				provincia,   
           				cod_fil_fornitore,   
           				cod_des_fornitore,   
           				cod_deposito,   
           				:ls_cod_euro,   
           				1,   
           				cod_tipo_listino_prodotto,   
           				cod_pagamento,   
           				sconto,   
           				cod_banca_clien_for,   
           				num_ord_fornitore,   
           				data_ord_fornitore,   
           				cod_imballo,   
           				cod_vettore,   
           				cod_inoltro,   
           				cod_mezzo,   
           				cod_porto,   
           				cod_resa,   
           				flag_blocco,   
           				'A',   
           				tot_val_ordine,   
           				tot_val_evaso,   
           				:ls_riferimento,   
           				nota_piede,   
           				data_consegna,   
           				cod_banca,   
           				data_consegna_fornitore,   
           				flag_doc_suc_tes,   
           				flag_doc_suc_pie,   
           				flag_st_note_tes,   
           				flag_st_note_pie,   
           				tot_val_ordine_valuta,   
           				imponibile_iva,   
           				importo_iva,   
           				imponibile_iva_valuta,   
           				importo_iva_valuta,   
           				tot_merci,   
           				tot_spese_trasporto,   
           				tot_spese_imballo,   
           				tot_spese_bolli,   
           				tot_spese_varie,   
           				tot_spese_cassa,   
           				tot_sconti_commerciali  
         from 			tes_ord_acq
			where  		cod_azienda = :s_cs_xx.cod_azienda and
					 		anno_registrazione = :ll_anno_registrazione and
					 		num_registrazione = :ll_num_registrazione
			using  		sqlcb;
										 
			if sqlcb.sqlcode <> 0 then
				lb_1.additem("- Errore nella insert di tes_ord_acq: " + sqlcb.sqlerrtext)
				lb_1.additem("  Documento: " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
				ll_err++
				rollback using sqlcb;
				continue;
			else				
				commit using sqlcb;	
			end if
			
			update tes_ord_acq
			set    flag_evasione = 'E'
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione
			using  sqlcb;
			
			if sqlcb.sqlcode <> 0 then
				lb_1.additem("- Errore nella update di tes_ord_acq: " + sqlcb.sqlerrtext)
				lb_1.additem("  Documento: " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
				ll_err++
				rollback using sqlcb;
				continue;
			else
				commit using sqlcb;	
			end if
									
	end choose
	
	ls_sql = "select " + ls_prezzo + ", " + ls_prog + ", " + ls_residuo + ", " + ls_quan_1 + ", " + ls_quan_2 + &
				" from " + ls_dettaglio + " where cod_azienda = '" + s_cs_xx.cod_azienda + &
				"' and anno_registrazione = " + string(ll_anno_registrazione) + " and num_registrazione = " + &
				string(ll_num_registrazione)
				
	prepare sqlsa from :ls_sql;
	
	declare dettaglio dynamic cursor for sqlsa;
	
	open dettaglio;
	
	if sqlca.sqlcode <> 0 then
		lb_1.additem("- Impossibile leggere i dettagli di " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
		lb_1.additem("  Errore nella open del cursore dettaglio: " + sqlca.sqlerrtext)
		ll_err++
		continue
	end if
	
	ll_prog_new = 10
	
	do while true
		
		fetch dettaglio
		into  :ld_prezzo,
				:ll_prog_riga,
				:ld_residuo,
				:ld_quan_1,
				:ld_quan_2;
				
		if sqlca.sqlcode = -1 then
			
			lb_1.additem("- Impossibile leggere i dettagli di " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione))
			lb_1.additem("  Errore nella fetch del cursore dettaglio: " + sqlca.sqlerrtext)  
			lb_1.additem("")
			ll_err++
			lb_det_err = true
			exit
		elseif sqlca.sqlcode = 100 then
			exit
		end if
		
		if ld_residuo = 0 then continue
		
		choose case is_tipo_doc
				
			case "ord_ven"				
				
				ld_prezzo = round((ld_prezzo / 1936.27),il_dec)
				
				insert into det_ord_ven  
         					( cod_azienda,   
           					anno_registrazione,   
           					num_registrazione,   
           					prog_riga_ord_ven,   
           					cod_prodotto,   
           					cod_tipo_det_ven,   
           					cod_misura,   
           					des_prodotto,   
           					quan_ordine,   
           					prezzo_vendita,   
           					fat_conversione_ven,   
           					sconto_1,   
           					sconto_2,   
           					provvigione_1,   
           					provvigione_2,   
           					cod_iva,   
           					data_consegna,   
           					val_riga,   
           					quan_in_evasione,   
           					quan_evasa,   
           					flag_evasione,   
           					flag_blocco,   
           					nota_dettaglio,   
           					anno_registrazione_off,   
           					num_registrazione_off,   
           					prog_riga_off_ven,   
           					anno_commessa,   
           					num_commessa,   
           					cod_centro_costo,   
           					sconto_3,   
           					sconto_4,   
           					sconto_5,   
           					sconto_6,   
           					sconto_7,   
           					sconto_8,   
           					sconto_9,   
           					sconto_10,   
           					cod_versione,   
           					num_confezioni,   
          					num_pezzi_confezione,   
           					num_riga_appartenenza,   
           					flag_gen_commessa,   
           					flag_doc_suc_det,   
           					flag_st_note_det,   
           					quantita_um,   
           					prezzo_um,   
           					imponibile_iva,   
           					imponibile_iva_valuta )  
     			select 		cod_azienda,   
            		 		anno_registrazione,   
            		 		:ll_num_reg_max,   
            		 		:ll_prog_new,   
            		 		cod_prodotto,   
            				cod_tipo_det_ven,   
           					cod_misura,   
            				des_prodotto,   
            				:ld_quan_1,   
            				:ld_prezzo,   
           	 				fat_conversione_ven,   
            				sconto_1,   
            				sconto_2,   
            				provvigione_1,   
            				provvigione_2,   
            				cod_iva,   
            				data_consegna,   
            				val_riga,   
            				quan_in_evasione,   
            				:ld_quan_2,
            				flag_evasione,   
            				flag_blocco,   
            				nota_dettaglio,   
            				anno_registrazione_off,   
            				num_registrazione_off,   
            				prog_riga_off_ven,   
            				anno_commessa,   
            				num_commessa,   
            				cod_centro_costo,   
            				sconto_3,   
            				sconto_4,   
            				sconto_5,   
            				sconto_6,   
            				sconto_7,   
           					sconto_8,   
            				sconto_9,   
            				sconto_10,   
            				cod_versione,   
           					num_confezioni,   
            				num_pezzi_confezione,   
           					num_riga_appartenenza,   
            				flag_gen_commessa,   
            				flag_doc_suc_det,   
            				flag_st_note_det,   
            				quantita_um,   
            				prezzo_um,   
            				imponibile_iva,   
            				imponibile_iva_valuta  
            from 			det_ord_ven
				where  		cod_azienda = :s_cs_xx.cod_azienda and
								anno_registrazione = :ll_anno_registrazione and
						 		num_registrazione = :ll_num_registrazione and
								prog_riga_ord_ven = :ll_prog_riga 
				using  		sqlcb;

				if sqlcb.sqlcode <> 0 then
					lb_1.additem("- Errore nella insert di det_ord_ven: " + sqlcb.sqlerrtext)
					lb_1.additem("  Documento: " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " riga: " + string(ll_prog_riga))										
					ll_err++
					rollback using sqlcb;
					lb_det_err = true
					exit
				else				
					commit using sqlcb;
				end if
				
				update det_ord_ven
				set    quan_evasa = :ld_quan_1,
						 flag_evasione = 'E',
						 anno_commessa = null,
						 num_commessa = null
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione and
						 prog_riga_ord_ven = :ll_prog_riga
				using  sqlcb;
						 
				if sqlca.sqlcode <> 0 then
					lb_1.additem("- Impossibile chiudere " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " dettaglio: " + string(ll_prog_riga))
					lb_1.additem("  Errore nell'update di det_ord_ven: " + sqlcb.sqlerrtext)  
					lb_1.additem("")
					ll_err++
					lb_det_err = true
					rollback using sqlcb;
					exit
				else
					commit using sqlcb;
				end if
				
			case "ord_acq"				
				
				ld_prezzo = round((ld_prezzo / 1936.27),il_dec)
				
				insert into det_ord_acq  								
         					( cod_azienda,   
           					anno_registrazione,   
           					num_registrazione,   
           					prog_riga_ordine_acq,   
           					cod_tipo_det_acq,   
           					cod_prodotto,   
           					cod_misura,   
           					des_prodotto,   
           					quan_ordinata,   
           					prezzo_acquisto,   
           					fat_conversione,   
           					sconto_1,   
           					sconto_2,   
           					sconto_3,   
           					cod_iva,   
           					data_consegna,   
           					cod_prod_fornitore,   
           					quan_arrivata,   
           					val_riga,   
           					flag_saldo,   
           					flag_blocco,   
           					nota_dettaglio,   
           					anno_commessa,   
           					num_commessa,   
           					cod_centro_costo,   
           					sconto_4,   
           					sconto_5,   
           					sconto_6,   
           					sconto_7,   
           					sconto_8,   
           					sconto_9,   
           					sconto_10,   
           					anno_off_acq,   
           					num_off_acq,   
           					prog_riga_off_acq,   
           					data_consegna_fornitore,   
           					num_confezioni,   
           					num_pezzi_confezione,   
           					flag_doc_suc_det,   
           					flag_st_note_det,   
           					imponibile_iva,   
           					imponibile_iva_valuta )  								  
            select 		cod_azienda,   
            				anno_registrazione,   
            				:ll_num_reg_max,   
            				:ll_prog_new,   
            				cod_tipo_det_acq,   
            				cod_prodotto,   
            				cod_misura,   
            				des_prodotto,   
            				:ld_quan_1,   
            				:ld_prezzo,   
            				fat_conversione,   
            				sconto_1,   
            				sconto_2,   
            				sconto_3,   
            				cod_iva,   
            				data_consegna,   
            				cod_prod_fornitore,   
            				:ld_quan_2,   
            				val_riga,   
            				flag_saldo,   
            				flag_blocco,   
            				nota_dettaglio,   
            				anno_commessa,   
            				num_commessa,   
            				cod_centro_costo,   
            				sconto_4,   
            				sconto_5,   
            				sconto_6,   
            				sconto_7,   
            				sconto_8,   
            				sconto_9,   
            				sconto_10,   
            				anno_off_acq,   
            				num_off_acq,   
            				prog_riga_off_acq,   
            				data_consegna_fornitore,   
            				num_confezioni,   
            				num_pezzi_confezione,   
            				flag_doc_suc_det,   
            				flag_st_note_det,   
            				imponibile_iva,   
            				imponibile_iva_valuta  
       		from 			det_ord_acq
				where  		cod_azienda = :s_cs_xx.cod_azienda and
						 		anno_registrazione = :ll_anno_registrazione and
						 		num_registrazione = :ll_num_registrazione and
								prog_riga_ordine_acq = :ll_prog_riga 
				using  		sqlcb;
				
				if sqlcb.sqlcode <> 0 then
					lb_1.additem("- Errore nella insert di det_ord_acq: " + sqlcb.sqlerrtext)
					lb_1.additem("  Documento: " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " riga: " + string(ll_prog_riga))
					ll_err++
					rollback using sqlcb;
					lb_det_err = true
					exit				
				else				
					commit using sqlcb;
				end if	
				
				update det_ord_acq
				set    quan_arrivata = :ld_quan_1,
						 flag_saldo = 'S',
						 anno_commessa = null,
						 num_commessa = null
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 anno_registrazione = :ll_anno_registrazione and
						 num_registrazione = :ll_num_registrazione and
						 prog_riga_ordine_acq = :ll_prog_riga
				using  sqlcb;
						 
				if sqlca.sqlcode <> 0 then
					lb_1.additem("- Impossibile chiudere " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " dettaglio: " + string(ll_prog_riga))
					lb_1.additem("  Errore nell'update di det_ord_acq: " + sqlcb.sqlerrtext)  
					lb_1.additem("")
					ll_err++
					lb_det_err = true
					rollback using sqlcb;
					exit
				else
					commit using sqlcb;
				end if
				
		end choose
		
		ll_prog_new = ll_prog_new + 10
		
	loop
	
	close dettaglio;
			
	if lb_det_err = false then
	
		ll_imp++
		
		lb_1.additem("- Ordine " + &
		             string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + &
				       " è stato convertito in EURO come " + &
						 string(ll_anno_registrazione) + "/" + string(ll_num_reg_max))				
	
		luo_doc = create uo_calcola_documento_euro
		
		if luo_doc.uof_calcola_documento(ll_anno_registrazione,ll_num_reg_max,is_tipo_doc,ls_messaggio) <> 0 then
			lb_1.additem("- Errore nel calcolo di " + ls_documento + " " + string(ll_anno_registrazione) + "/" + string(ll_num_reg_max))
			lb_1.additem("  " + ls_messaggio)  
			lb_1.additem("")
			ll_err_calc++
		end if		
		
		destroy luo_doc
		
	end if	
	
loop

close testata;

elemento.text = ""

if ll_imp <> 1 then
	ls_i_o = "i sono stati convertiti in Euro"
else
	ls_i_o = "o è stato convertito in Euro"
end if

lb_1.additem("- " + string(ll_imp) + " document" + ls_i_o)

if ll_err <> 0 then

	if ll_err <> 1 then
		ls_i_o = "i hanno provocato errori durante la conversione"
	else
		ls_i_o = "o ha provocato errori durante la conversione"
	end if

	lb_1.additem("- " + string(ll_err) + " document" + ls_i_o)

end if

if ll_skip <> 0 then

	if ll_skip <> 1 then
		ls_i_o = "i sono stati saltati perchè non in stato evasione PARZIALE o senza dettagli"
	else
		ls_i_o = "o è stato saltato perchè non in stato evasione PARZIALE o senza dettagli"
	end if

	lb_1.additem("- " + string(ll_skip) + " document" + ls_i_o)

end if

if ll_err_calc <> 0 then

	if ll_err_calc <> 1 then
		ls_i_o = "i hanno causato errori nel calcolo dopo la conversione"
	else
		ls_i_o = "o ha causato errori nel calcolo dopo la conversione"
	end if

	lb_1.additem("- " + string(ll_err_calc) + " document" + ls_i_o)

end if

disconnect using sqlcb;

if sqlcb.sqlcode = -1 then
	lb_1.additem("Errore nella disconnessione della transaction sqlcb: " + sqlcb.sqlerrtext)	
end if

destroy sqlcb;

return 0
end function

on w_lire_euro.create
this.rb_8=create rb_8
this.rb_7=create rb_7
this.sle_dec=create sle_dec
this.st_dec=create st_dec
this.st_log=create st_log
this.cbx_log=create cbx_log
this.rb_6=create rb_6
this.rb_5=create rb_5
this.cbx_1=create cbx_1
this.rb_4=create rb_4
this.rb_3=create rb_3
this.lb_1=create lb_1
this.documento=create documento
this.st_3=create st_3
this.elemento=create elemento
this.st_1=create st_1
this.cb_1=create cb_1
this.rb_2=create rb_2
this.rb_1=create rb_1
this.r_1=create r_1
this.r_2=create r_2
this.r_3=create r_3
this.r_4=create r_4
this.r_5=create r_5
this.r_6=create r_6
this.r_7=create r_7
this.Control[]={this.rb_8,&
this.rb_7,&
this.sle_dec,&
this.st_dec,&
this.st_log,&
this.cbx_log,&
this.rb_6,&
this.rb_5,&
this.cbx_1,&
this.rb_4,&
this.rb_3,&
this.lb_1,&
this.documento,&
this.st_3,&
this.elemento,&
this.st_1,&
this.cb_1,&
this.rb_2,&
this.rb_1,&
this.r_1,&
this.r_2,&
this.r_3,&
this.r_4,&
this.r_5,&
this.r_6,&
this.r_7}
end on

on w_lire_euro.destroy
destroy(this.rb_8)
destroy(this.rb_7)
destroy(this.sle_dec)
destroy(this.st_dec)
destroy(this.st_log)
destroy(this.cbx_log)
destroy(this.rb_6)
destroy(this.rb_5)
destroy(this.cbx_1)
destroy(this.rb_4)
destroy(this.rb_3)
destroy(this.lb_1)
destroy(this.documento)
destroy(this.st_3)
destroy(this.elemento)
destroy(this.st_1)
destroy(this.cb_1)
destroy(this.rb_2)
destroy(this.rb_1)
destroy(this.r_1)
destroy(this.r_2)
destroy(this.r_3)
destroy(this.r_4)
destroy(this.r_5)
destroy(this.r_6)
destroy(this.r_7)
end on

event open;string ls_volume, ls_log

registryget(s_cs_xx.chiave_root + "\applicazione_" + s_cs_xx.profilocorrente,"vol",ls_volume)

select stringa
into   :ls_log
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'LOG';
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Impossibile ottenere il nome del file di LOG: " + sqlca.sqlerrtext + "~nMemorizzazione su file log disabilitata")
	cbx_log.checked = false
	cbx_log.enabled = false
	st_log.text = ""
	st_log.enabled = false
	return -1
end if

st_log.text = ls_volume + ls_log
end event

type rb_8 from radiobutton within w_lire_euro
integer x = 709
integer y = 280
integer width = 640
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Clienti / Fornitori"
end type

event clicked;checked = true
rb_1.checked = false
rb_2.checked = false
rb_3.checked = false
rb_4.checked = false
rb_5.checked = false
rb_6.checked = false
rb_7.checked = false
cbx_1.checked = false
cbx_1.enabled = false

documento.text = "CLIENTI E FORNITORI"

is_tipo_doc = "cli_for"

cb_1.enabled = true
end event

type rb_7 from radiobutton within w_lire_euro
integer x = 46
integer y = 280
integer width = 640
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Gruppi sconto"
end type

event clicked;checked = true
rb_1.checked = false
rb_2.checked = false
rb_3.checked = false
rb_4.checked = false
rb_5.checked = false
rb_6.checked = false
rb_8.checked = false
cbx_1.checked = false
cbx_1.enabled = false

documento.text = "GRUPPI SCONTO"

is_tipo_doc = "gruppi_sconto"

cb_1.enabled = true
end event

type sle_dec from singlelineedit within w_lire_euro
integer x = 1234
integer y = 388
integer width = 114
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "2"
boolean autohscroll = false
borderstyle borderstyle = stylelowered!
boolean righttoleft = true
end type

type st_dec from statictext within w_lire_euro
integer x = 46
integer y = 400
integer width = 1179
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Cifre decimali da usare negli arrotondamenti:"
boolean focusrectangle = false
end type

type st_log from statictext within w_lire_euro
integer x = 2240
integer y = 404
integer width = 640
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
alignment alignment = center!
boolean focusrectangle = false
end type

type cbx_log from checkbox within w_lire_euro
integer x = 1417
integer y = 404
integer width = 823
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Salva i messaggi nel file log:"
end type

type rb_6 from radiobutton within w_lire_euro
integer x = 709
integer y = 200
integer width = 640
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Listini dimensioni"
end type

event clicked;checked = true
rb_1.checked = false
rb_2.checked = false
rb_3.checked = false
rb_4.checked = false
rb_5.checked = false
rb_7.checked = false
rb_8.checked = false
cbx_1.checked = false
cbx_1.enabled = false

documento.text = "LISTINI Dimensioni"

is_tipo_doc = "list_dim"

cb_1.enabled = true
end event

type rb_5 from radiobutton within w_lire_euro
integer x = 46
integer y = 200
integer width = 640
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Listini produzione"
end type

event clicked;checked = true
rb_1.checked = false
rb_2.checked = false
rb_3.checked = false
rb_4.checked = false
rb_6.checked = false
rb_7.checked = false
rb_8.checked = false
cbx_1.checked = false
cbx_1.enabled = false

documento.text = "LISTINI Produzione"

is_tipo_doc = "list_prod"

cb_1.enabled = true
end event

type cbx_1 from checkbox within w_lire_euro
integer x = 1486
integer y = 40
integer width = 1335
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
string text = "Esegui anche la conversione degli ordini parziali"
end type

type rb_4 from radiobutton within w_lire_euro
integer x = 709
integer y = 120
integer width = 640
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Listini di vendita"
end type

event clicked;checked = true
rb_1.checked = false
rb_2.checked = false
rb_3.checked = false
rb_5.checked = false
rb_6.checked = false
rb_7.checked = false
rb_8.checked = false
cbx_1.checked = false
cbx_1.enabled = false

documento.text = "LISTINI Vendita"

is_tipo_doc = "list_ven"

cb_1.enabled = true
end event

type rb_3 from radiobutton within w_lire_euro
integer x = 46
integer y = 120
integer width = 640
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Listini di acquisto"
end type

event clicked;checked = true
rb_1.checked = false
rb_2.checked = false
rb_4.checked = false
rb_5.checked = false
rb_6.checked = false
rb_7.checked = false
rb_8.checked = false
cbx_1.checked = false
cbx_1.enabled = false

documento.text = "LISTINI Acquisto"

is_tipo_doc = "list_acq"

cb_1.enabled = true
end event

type lb_1 from listbox within w_lire_euro
integer x = 46
integer y = 524
integer width = 2834
integer height = 1100
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean hscrollbar = true
boolean vscrollbar = true
boolean sorted = false
borderstyle borderstyle = stylelowered!
end type

type documento from statictext within w_lire_euro
integer x = 1829
integer y = 160
integer width = 667
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
alignment alignment = center!
boolean focusrectangle = false
end type

type st_3 from statictext within w_lire_euro
integer x = 1417
integer y = 160
integer width = 411
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Conversione di:"
boolean focusrectangle = false
end type

type elemento from statictext within w_lire_euro
integer x = 1920
integer y = 280
integer width = 960
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
alignment alignment = right!
boolean focusrectangle = false
end type

type st_1 from statictext within w_lire_euro
integer x = 1417
integer y = 280
integer width = 507
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 79741120
string text = "Elemento corrente:"
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_lire_euro
integer x = 2537
integer y = 148
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "&Converti"
end type

event clicked;long    ll_i

boolean lb_errore

char    lc_testo[]


lc_testo = sle_dec.text

for ll_i = 1 to upperbound(lc_testo)
	choose case lc_testo[ll_i]
		case "0","1","2","3","4","5","6","7","8","9"
		case else
			g_mb.messagebox("APICE","Si è indicato un valore errato per il numero di cifre decimali " + &
			           "da utilizzare negli arrotondamenti:~n" + sle_dec.text + " non è un numero intero positivo!")
			return -1			  
	end choose
next

lb_errore = false

enabled = false

sle_dec.enabled = false
cbx_log.enabled = false
rb_1.enabled = false
rb_2.enabled = false
rb_3.enabled = false
rb_4.enabled = false
rb_5.enabled = false
rb_6.enabled = false
rb_7.enabled = false
rb_8.enabled = false
cbx_1.enabled = false

setpointer(hourglass!)	

il_dec = long(sle_dec.text)

lb_1.reset()
lb_1.additem(string(today()) + " " + string(now()) + " Inizio operazione")
lb_1.additem("")

choose case is_tipo_doc

	case "ord_acq","ord_ven"
		
		if wf_converti_aperti() = -1 then
			lb_errore = true
		end if

		if cbx_1.checked = true then
			
			if wf_converti_parziali() = -1 then
				lb_errore = true
			end if
			
		end if	
		
	case "list_acq"
		
		if wf_converti_listini_fornitori() = -1 then
			lb_errore = true
		end if

	case "list_ven"
		
		if wf_converti_listini_vendite() = -1 then
			lb_errore = true
		end if
		
	case "list_prod"
		
		if wf_converti_listini_prod() = -1 then
			lb_errore = true
		end if

	case "list_dim"
		
		if wf_converti_listini_dim() = -1 then
			lb_errore = true
		end if
		
	case "gruppi_sconto"	
		
		if wf_converti_gruppi_sconto() = -1 then
			lb_errore = true
		end if
		
	case "cli_for"	
		
		if wf_converti_cli_for() = -1 then
			lb_errore = true
		end if

end choose

if lb_errore = false then
	lb_1.additem("")
	lb_1.additem(string(today()) + " " + string(now()) + " Operazione completata")
else
	lb_1.additem("")
	lb_1.additem(string(today()) + " " + string(now()) + " Operazione interrotta a causa di errori")			
end if

if cbx_log.checked = true then
	for ll_i = 1 to lb_1.totalitems()
		f_scrivi_log(lb_1.text(ll_i))
	next
end if

setpointer(arrow!)

rb_1.enabled = true
rb_2.enabled = true
rb_3.enabled = true
rb_4.enabled = true
rb_5.enabled = true
rb_6.enabled = true
rb_7.enabled = true
rb_8.enabled = true
cbx_log.enabled = true
sle_dec.enabled = true

rb_1.checked = false
rb_2.checked = false
rb_3.checked = false
rb_4.checked = false
rb_5.checked = false
rb_6.checked = false
rb_7.checked = false
rb_8.checked = false
cbx_1.checked = false
end event

type rb_2 from radiobutton within w_lire_euro
integer x = 709
integer y = 40
integer width = 640
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ordini di vendita"
end type

event clicked;checked = true
rb_1.checked = false
rb_3.checked = false
rb_4.checked = false
rb_5.checked = false
rb_6.checked = false
rb_7.checked = false
rb_8.checked = false
cbx_1.enabled = true

documento.text = "ORDINI Vendita"

is_tipo_doc = "ord_ven"

cb_1.enabled = true
end event

type rb_1 from radiobutton within w_lire_euro
integer x = 46
integer y = 40
integer width = 640
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
string text = "Ordini di acquisto"
end type

event clicked;checked = true
rb_2.checked = false
rb_3.checked = false
rb_4.checked = false
rb_5.checked = false
rb_6.checked = false
rb_7.checked = false
rb_8.checked = false
cbx_1.enabled = true

documento.text = "ORDINI Acquisto"

is_tipo_doc = "ord_acq"

cb_1.enabled = true
end event

type r_1 from rectangle within w_lire_euro
integer linethickness = 1
long fillcolor = 12632256
integer x = 1394
integer y = 260
integer width = 1509
integer height = 100
end type

type r_2 from rectangle within w_lire_euro
integer linethickness = 1
long fillcolor = 12632256
integer x = 23
integer y = 20
integer width = 1349
integer height = 340
end type

type r_3 from rectangle within w_lire_euro
integer linethickness = 1
long fillcolor = 12632256
integer x = 1394
integer y = 140
integer width = 1120
integer height = 100
end type

type r_4 from rectangle within w_lire_euro
integer linethickness = 1
long fillcolor = 12632256
integer x = 23
integer y = 504
integer width = 2880
integer height = 1140
end type

type r_5 from rectangle within w_lire_euro
integer linethickness = 1
long fillcolor = 12632256
integer x = 1394
integer y = 20
integer width = 1509
integer height = 100
end type

type r_6 from rectangle within w_lire_euro
integer linethickness = 1
long fillcolor = 12632256
integer x = 1394
integer y = 384
integer width = 1509
integer height = 100
end type

type r_7 from rectangle within w_lire_euro
integer linethickness = 1
long fillcolor = 12632256
integer x = 23
integer y = 380
integer width = 1349
integer height = 100
end type

