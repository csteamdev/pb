﻿$PBExportHeader$w_lista_barcode_prodotti.srw
forward
global type w_lista_barcode_prodotti from w_cs_xx_risposta
end type
type cb_annulla from commandbutton within w_lista_barcode_prodotti
end type
type cb_conferma from commandbutton within w_lista_barcode_prodotti
end type
type dw_lista_barcode_prodotti from uo_cs_xx_dw within w_lista_barcode_prodotti
end type
end forward

global type w_lista_barcode_prodotti from w_cs_xx_risposta
integer width = 1938
integer height = 1308
string title = "Elenco prodotti con Medesimo BARCODE"
cb_annulla cb_annulla
cb_conferma cb_conferma
dw_lista_barcode_prodotti dw_lista_barcode_prodotti
end type
global w_lista_barcode_prodotti w_lista_barcode_prodotti

event pc_setwindow;call super::pc_setwindow;dw_lista_barcode_prodotti.set_dw_options(sqlca, &
                            pcca.null_object, &
									 c_nonew + &
									 c_nomodify + &
									 c_nodelete + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_cursorrowpointer + &
									 c_ViewModeborderUnchanged)


//                            c_nohighlightselected + &

end event

on w_lista_barcode_prodotti.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_conferma=create cb_conferma
this.dw_lista_barcode_prodotti=create dw_lista_barcode_prodotti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_conferma
this.Control[iCurrent+3]=this.dw_lista_barcode_prodotti
end on

on w_lista_barcode_prodotti.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_conferma)
destroy(this.dw_lista_barcode_prodotti)
end on

type cb_annulla from commandbutton within w_lista_barcode_prodotti
integer x = 1074
integer y = 1100
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;setnull(s_cs_xx.parametri.parametro_s_10)
setnull(s_cs_xx.parametri.parametro_s_1)
close(parent)
end event

type cb_conferma from commandbutton within w_lista_barcode_prodotti
integer x = 1486
integer y = 1100
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;if dw_lista_barcode_prodotti.getrow() > 0 then
	s_cs_xx.parametri.parametro_s_10 = dw_lista_barcode_prodotti.getitemstring(dw_lista_barcode_prodotti.getrow(),"anag_prodotti_barcode_cod_prodotto")
	setnull(s_cs_xx.parametri.parametro_s_1)
else
	g_mb.messagebox("APICE","Nulla di selezionato")
	return
end if
close(parent)
end event

type dw_lista_barcode_prodotti from uo_cs_xx_dw within w_lista_barcode_prodotti
integer x = 23
integer y = 20
integer width = 1851
integer height = 1060
integer taborder = 10
string dataobject = "d_lista_barcode_prodotti"
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda, s_cs_xx.parametri.parametro_s_1)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event doubleclicked;call super::doubleclicked;cb_conferma.postevent(clicked!)
end event

event ue_key;call super::ue_key;CHOOSE CASE key
	CASE KeyEnter!
//    levato perchè prende sempre la riga successiva. kzkzkzkz		
//		cb_conferma.postevent("clicked")
	Case keydownarrow! 
		if getrow() < rowcount() then
			setrow(getrow() + 1)
		end if
	Case keyuparrow!
		if getrow() > 1 then
			setrow(getrow() - 1)
		end if
END CHOOSE

end event

