﻿$PBExportHeader$w_lista_dest_cliente.srw
$PBExportComments$Finestra di Lista Destinazioni per Cliente
forward
global type w_lista_dest_cliente from w_cs_xx_risposta
end type
type dw_lista_dest_cliente from uo_cs_xx_dw within w_lista_dest_cliente
end type
type cb_ok from commandbutton within w_lista_dest_cliente
end type
type cb_annulla from uo_cb_close within w_lista_dest_cliente
end type
end forward

global type w_lista_dest_cliente from w_cs_xx_risposta
int Width=2593
int Height=1257
boolean TitleBar=true
string Title="Lista Destinazioni Cliente"
dw_lista_dest_cliente dw_lista_dest_cliente
cb_ok cb_ok
cb_annulla cb_annulla
end type
global w_lista_dest_cliente w_lista_dest_cliente

type variables
string is_cod_cliente, is_flag_dest_merce_fat
long il_row
end variables

event pc_setwindow;call super::pc_setwindow;string ls_rag_soc_1

if s_cs_xx.parametri.parametro_s_10 <> "" and not isnull(s_cs_xx.parametri.parametro_s_10) then
	is_cod_cliente = s_cs_xx.parametri.parametro_s_10
	setnull(s_cs_xx.parametri.parametro_s_10)
else
	g_mb.messagebox("Lista Destinazioni Cliente", "Nessun Cliente Selezionato")
	close(this)
end if

is_flag_dest_merce_fat = s_cs_xx.parametri.parametro_s_11

// -------------   verifico che il cliente esista in anag_clienti  -------------------------------

select rag_soc_1
 into  :ls_rag_soc_1
 from  anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :is_cod_cliente;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Lista Destinazioni Cliente", "Errore durante l'Estrazione del Cliente")
	pcca.error = c_fatal
elseif sqlca.sqlcode = 100 then
	g_mb.messagebox("Lista Destinazioni Cliente", "Nessun Cliente per il Codice Selezionato")
	close(this)
elseif sqlca.sqlcode <> 0 then
	g_mb.messagebox("Lista Destinazioni Cliente", "Errore in fase di lettura anagrafica cliente. ~r~nDettaglio: "+ sqlca.sqlerrtext)
	close(this)
elseif sqlca.sqlcode = 0 then
	this.title = this.title + ": " + ls_rag_soc_1
	dw_lista_dest_cliente.setfocus()
end if
dw_lista_dest_cliente.setfocus()

dw_lista_dest_cliente.set_dw_key("cod_azienda")
dw_lista_dest_cliente.set_dw_options(sqlca,pcca.null_object,c_nonew + c_nomodify + c_default, c_default)

end event

on w_lista_dest_cliente.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_lista_dest_cliente=create dw_lista_dest_cliente
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_lista_dest_cliente
this.Control[iCurrent+2]=cb_ok
this.Control[iCurrent+3]=cb_annulla
end on

on w_lista_dest_cliente.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_lista_dest_cliente)
destroy(this.cb_ok)
destroy(this.cb_annulla)
end on

type dw_lista_dest_cliente from uo_cs_xx_dw within w_lista_dest_cliente
int X=14
int Y=17
int Width=2524
int Height=1025
int TabOrder=10
string DataObject="d_lista_dest_cliente"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

if s_cs_xx.parametri.parametro_s_12 <> "D" then
	is_flag_dest_merce_fat = "S"
end if

if isnull(is_flag_dest_merce_fat) then
	is_flag_dest_merce_fat = "S"
end if	

ll_errore = retrieve(s_cs_xx.cod_azienda, is_cod_cliente, is_flag_dest_merce_fat)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event doubleclicked;call super::doubleclicked;cb_ok.triggerevent(clicked!)
end event

event ue_key;call super::ue_key;long ll_row

ll_row = this.getrow()

choose case key
	case KeyLeftArrow!
		if ll_row < this.rowcount() then
			ll_row = this.getrow() + 1
			dw_lista_dest_cliente.SetRow ( ll_row )
		end if
	case KeyRightArrow!
		if ll_row > 1 then
			ll_row = this.getrow() - 1
			dw_lista_dest_cliente.SetRow ( ll_row )
		end if		
	case KeyEnter!
		ll_row = this.getrow()
		cb_ok.triggerevent("clicked")		
end choose		

end event

type cb_ok from commandbutton within w_lista_dest_cliente
int X=2186
int Y=1057
int Width=362
int Height=81
int TabOrder=20
string Text="&Ok"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;integer li_row
string ls_cod_des_cliente

li_row = dw_lista_dest_cliente.getrow()
ls_cod_des_cliente = dw_lista_dest_cliente.getitemstring(li_row, "cod_des_cliente")
if ls_cod_des_cliente <> "" and not isnull(ls_cod_des_cliente) then
	s_cs_xx.parametri.parametro_s_10 = ls_cod_des_cliente
else
	setnull(s_cs_xx.parametri.parametro_s_10)
end if
close(parent)
end event

type cb_annulla from uo_cb_close within w_lista_dest_cliente
int X=1793
int Y=1057
int Width=362
int Height=81
int TabOrder=3
string Text="&Chiudi"
boolean Cancel=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

