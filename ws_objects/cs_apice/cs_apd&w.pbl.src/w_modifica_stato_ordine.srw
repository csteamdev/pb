﻿$PBExportHeader$w_modifica_stato_ordine.srw
forward
global type w_modifica_stato_ordine from w_cs_xx_principale
end type
type cb_2 from commandbutton within w_modifica_stato_ordine
end type
type cb_1 from commandbutton within w_modifica_stato_ordine
end type
type dw_modifica_stato_ordine from uo_cs_xx_dw within w_modifica_stato_ordine
end type
type dw_selezione_modifica_stato_ordine from uo_cs_xx_dw within w_modifica_stato_ordine
end type
end forward

global type w_modifica_stato_ordine from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 3232
integer height = 1324
string title = "Modifica Stato Ordine"
cb_2 cb_2
cb_1 cb_1
dw_modifica_stato_ordine dw_modifica_stato_ordine
dw_selezione_modifica_stato_ordine dw_selezione_modifica_stato_ordine
end type
global w_modifica_stato_ordine w_modifica_stato_ordine

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_modifica_stato_ordine.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_nonew + &
								 c_noretrieveonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione_modifica_stato_ordine.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
save_on_close(c_socnosave)
iuo_dw_main = dw_modifica_stato_ordine

end event

on w_modifica_stato_ordine.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_modifica_stato_ordine=create dw_modifica_stato_ordine
this.dw_selezione_modifica_stato_ordine=create dw_selezione_modifica_stato_ordine
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_modifica_stato_ordine
this.Control[iCurrent+4]=this.dw_selezione_modifica_stato_ordine
end on

on w_modifica_stato_ordine.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_modifica_stato_ordine)
destroy(this.dw_selezione_modifica_stato_ordine)
end on

type cb_2 from commandbutton within w_modifica_stato_ordine
integer x = 2789
integer y = 120
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Elabora"
end type

event clicked;string ls_cod_prodotto, ls_cod_tipo_det_ven, ls_falg_evasione,ls_flag_tipo_det_ven
long ll_anno_bolla, ll_num_bolla, ll_anno_ordine, ll_num_ordine, ll_righe, ll_prog_riga_ord_ven, ll_i, ll_riga_ordine, &
     ll_cont
dec{4} ld_quan_ordine

dw_selezione_modifica_stato_ordine.accepttext()

ll_anno_bolla = dw_selezione_modifica_stato_ordine.getitemnumber(dw_selezione_modifica_stato_ordine.getrow(),"anno_bolla")
ll_num_bolla = dw_selezione_modifica_stato_ordine.getitemnumber(dw_selezione_modifica_stato_ordine.getrow(),"num_bolla")
ll_anno_ordine = dw_selezione_modifica_stato_ordine.getitemnumber(dw_selezione_modifica_stato_ordine.getrow(),"anno_ordine")
ll_num_ordine = dw_selezione_modifica_stato_ordine.getitemnumber(dw_selezione_modifica_stato_ordine.getrow(),"num_ordine")

if isnull(ll_anno_bolla) or ll_anno_bolla = 0 then
	g_mb.messagebox("APICE","Impostano l'anno di registrazione della bolla")
	return
end if
if isnull(ll_num_bolla) or ll_num_bolla = 0 then
	g_mb.messagebox("APICE","Impostano il numero di registrazione della bolla")
	return
end if
if isnull(ll_anno_ordine) or ll_anno_ordine = 0 then
	g_mb.messagebox("APICE","Impostano l'anno di registrazione dell'ordine da collegare alla bolla")
	return
end if
if isnull(ll_num_ordine) or ll_num_ordine = 0 then
	g_mb.messagebox("APICE","Impostano il numero dell'ordine da collegare alla bolla")
	return
end if

dw_modifica_stato_ordine.retrieve(s_cs_xx.cod_azienda, ll_anno_bolla,ll_num_bolla)
ll_righe = dw_modifica_stato_ordine.rowcount()
if ll_righe = 0 or isnull(ll_righe) then
	g_mb.messagebox("APICE","Attenzione: nella bolla specificata non sono state trovate righe da collegare.",stopsign!)
	return
end if

select min(prog_riga_ord_ven)
into   :ll_prog_riga_ord_ven
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_ordine and
		 num_registrazione = :ll_num_ordine and
		 flag_evasione <> 'E' ;
if ll_prog_riga_ord_ven = 0 or isnull(ll_prog_riga_ord_ven) then
	g_mb.messagebox("APICE","Attenzione: nell'ordine specificato non sono state trovate righe da evadere.",stopsign!)
	return
end if

ll_cont = 0
select count(*)
into   :ll_cont
from   det_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_bolla and
		 num_registrazione = :ll_num_bolla and
       prog_riga_ord_ven  is not null;
if ll_cont > 0 then
	if g_mb.messagebox("APICE","Attenzione: la bolla contiene righe collegate ad altri ordini~r~n. Procedo elaborando le righe non collegate a nessun ordine ?",question!,YesNo!,2) = 2 then
		rollback;
		return
	end if
end if
ll_cont = 0
select count(*)
into   :ll_cont
from   det_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_bolla and
		 num_registrazione = :ll_num_bolla and
       prog_riga_ord_ven  is null;
if ll_cont < 1 then
	g_mb.messagebox("APICE","Attenzione: tutte le righe della bolla risultano collegate ad ordini; elaborazione interrotta!",Stopsign! )
	rollback;
	return
end if
	

// **********************  collego le righe bolla alla prima riga ordine trovata ****************************

update det_bol_ven
set    anno_registrazione_ord_ven = :ll_anno_ordine,
       num_registrazione_ord_ven  = :ll_num_ordine,
		 prog_riga_ord_ven = :ll_prog_riga_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :ll_anno_bolla and
		 num_registrazione = :ll_num_bolla and
		 (prog_riga_ord_ven is null or prog_riga_ord_ven = 0);
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore durante aggiornamento riferimento ordine in righe bolla.~r~n" + sqlca.sqlerrtext)
	rollback;
	return
end if

// ********************** rendo evase tutte le righe dell'ordine ********************************************

declare cu_righe_ordine cursor for
select prog_riga_ord_ven, cod_tipo_det_ven, cod_prodotto,quan_ordine
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_ordine and
		 num_registrazione = :ll_num_ordine and
		 flag_evasione <> 'E';
open cu_righe_ordine;
do while 1=1
	fetch cu_righe_ordine into :ll_riga_ordine, :ls_cod_tipo_det_ven, :ls_cod_prodotto, :ld_quan_ordine;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore durante lettura righe ordine~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	select flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore durante ricerca tipo dettaglio "+ls_cod_tipo_det_ven+"~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	// ************************** update impegnato del magazzino ******************************************
	if ls_flag_tipo_det_ven = "M" then
		update anag_prodotti
		set quan_impegnata = quan_impegnata - :ld_quan_ordine
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore durante decremento impegnato del prodotto "+ls_cod_prodotto+"~r~n" + sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if	
	
	update det_ord_ven
	set    flag_evasione = 'E',
			 quan_evasa = quan_ordine
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_ordine and
			 num_registrazione = :ll_num_ordine and
			 prog_riga_ord_ven = :ll_riga_ordine;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore durante aggiornamento riga ordine "+string(ll_riga_ordine)+"~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	update tes_ord_ven
	set    flag_evasione = 'E'
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_ordine and
			 num_registrazione = :ll_num_ordine ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore durante aggiornamento riga ordine "+string(ll_riga_ordine)+"~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
loop			

commit;
g_mb.messagebox("APICE","Elaborazione eseguita con successo")
cb_1.triggerevent("clicked")

end event

type cb_1 from commandbutton within w_modifica_stato_ordine
integer x = 2789
integer y = 20
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca Bolla"
end type

event clicked;long ll_anno_bolla, ll_num_bolla, ll_anno_ordine, ll_num_ordine

dw_selezione_modifica_stato_ordine.accepttext()

ll_anno_bolla = dw_selezione_modifica_stato_ordine.getitemnumber(dw_selezione_modifica_stato_ordine.getrow(),"anno_bolla")
ll_num_bolla = dw_selezione_modifica_stato_ordine.getitemnumber(dw_selezione_modifica_stato_ordine.getrow(),"num_bolla")
ll_anno_ordine = dw_selezione_modifica_stato_ordine.getitemnumber(dw_selezione_modifica_stato_ordine.getrow(),"anno_ordine")
ll_num_ordine = dw_selezione_modifica_stato_ordine.getitemnumber(dw_selezione_modifica_stato_ordine.getrow(),"num_ordine")

if isnull(ll_anno_bolla) or ll_anno_bolla = 0 then
	g_mb.messagebox("APICE","Impostano l'anno di registrazione della bolla")
	return
end if
if isnull(ll_num_bolla) or ll_num_bolla = 0 then
	g_mb.messagebox("APICE","Impostano il numero di registrazione della bolla")
	return
end if
if isnull(ll_anno_ordine) or ll_anno_ordine = 0 then
	g_mb.messagebox("APICE","Impostano l'anno di registrazione dell'ordine da collegare alla bolla")
	return
end if
if isnull(ll_num_ordine) or ll_num_ordine = 0 then
	g_mb.messagebox("APICE","Impostano il numero dell'ordine da collegare alla bolla")
	return
end if

dw_modifica_stato_ordine.change_dw_current()
parent.triggerevent("pc_retrieve")

end event

type dw_modifica_stato_ordine from uo_cs_xx_dw within w_modifica_stato_ordine
integer x = 23
integer y = 240
integer width = 3154
integer height = 960
integer taborder = 10
string dataobject = "d_modifica_stato_ordine"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda, &
                     dw_selezione_modifica_stato_ordine.getitemnumber(dw_selezione_modifica_stato_ordine.getrow(),"anno_bolla"), &
                     dw_selezione_modifica_stato_ordine.getitemnumber(dw_selezione_modifica_stato_ordine.getrow(),"num_bolla"))

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type dw_selezione_modifica_stato_ordine from uo_cs_xx_dw within w_modifica_stato_ordine
integer x = 23
integer y = 20
integer width = 2331
integer height = 220
integer taborder = 10
string dataobject = "d_selezione_modifica_stato_ordine"
boolean border = false
end type

