﻿$PBExportHeader$w_moduli_stampa.srw
forward
global type w_moduli_stampa from w_cs_xx_master_detail
end type
type dw_detail from uo_cs_xx_dw within w_moduli_stampa
end type
end forward

global type w_moduli_stampa from w_cs_xx_master_detail
integer width = 3218
integer height = 2120
string title = "Moduli Stampa"
dw_detail dw_detail
end type
global w_moduli_stampa w_moduli_stampa

forward prototypes
public function boolean wf_imposta_where_ricerca (string as_sql, ref string as_where)
public subroutine wf_duplica (integer ai_row)
end prototypes

public function boolean wf_imposta_where_ricerca (string as_sql, ref string as_where);string ls_value
int li_progressivo

as_where = " WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' "

li_progressivo = tab_1.ricerca.dw_ricerca.getitemnumber(1, "progressivo")
if not isnull(li_progressivo) and li_progressivo > 0 then
	as_where += " AND progressivo =" + string(li_progressivo) + " "
end if

ls_value = tab_1.ricerca.dw_ricerca.getitemstring(1, "cod_tipo_gestione")
if ls_value <> "X" then
	as_where += " AND cod_tipo_gestione ='" + ls_value + "' "
end if

ls_value = tab_1.ricerca.dw_ricerca.getitemstring(1, "cod_lingua")
if not isnull(ls_value) and ls_value <> "" then
	as_where += " AND cod_lingua ='" + ls_value + "' "
end if

ls_value = tab_1.ricerca.dw_ricerca.getitemstring(1, "cod_cliente")
if not isnull(ls_value) and ls_value <> "" then
	as_where += " AND cod_cliente ='" + ls_value + "' "
end if

ls_value = tab_1.ricerca.dw_ricerca.getitemstring(1, "dataobject")
if not isnull(ls_value) and ls_value <> "" then
	as_where += " AND dataobject like '%" + ls_value + "%' "
end if

ls_value = tab_1.ricerca.dw_ricerca.getitemstring(1, "flag_stampa_automatica")
if ls_value <> "X" then
	as_where += " AND flag_stampa_automatica='" + ls_value + "' "
end if

return true
end function

public subroutine wf_duplica (integer ai_row);/**
 * stefanop
 * 05/09/2014
 *
 * Duplica il modulo
 **/
 
long ll_progressivo, ll_progressivo_riga
 
ll_progressivo_riga  = dw_detail.getitemnumber(ai_row, "progressivo")
if ll_progressivo_riga <= 0 then
	g_mb.warning("Impossibile leggere il progressivo della riga corrente")
	return
end if

select max(progressivo)
into :ll_progressivo
from tab_moduli_stampa
where cod_azienda = :s_cs_xx.cod_azienda;
		 
if sqlca.sqlcode <> 0 or isnull(ll_progressivo) then
	ll_progressivo = 0
end if

ll_progressivo ++

INSERT INTO tab_moduli_stampa (
	cod_azienda,   
	progressivo,   
	cod_tipo_gestione,   
	cod_modulo,   
	titolo,   
	cod_lingua,   
	cod_cliente,   
	dataobject,   
	logo_testata,   
	flag_stampa_automatica
) 
SELECT
	cod_azienda,   
	:ll_progressivo,   
	cod_tipo_gestione,   
	cod_modulo,   
	titolo,   
	cod_lingua,   
	cod_cliente,   
	dataobject,   
	logo_testata,   
	flag_stampa_automatica
FROM tab_moduli_stampa
WHERE cod_azienda = :s_cs_xx.cod_azienda AND
		 progressivo  = :ll_progressivo_riga;
		 
if sqlca.sqlcode = 0 then
	commit;
	g_mb.success("Duplicazione avvenuta con successo. Nuovo progressivo: " + string(ll_progressivo))
	wf_esegui_ricerca()
else
	g_mb.error("Errore durante la duplicazione del modulo.", sqlca)
	rollback;
end if

end subroutine

on w_moduli_stampa.create
int iCurrent
call super::create
this.dw_detail=create dw_detail
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_detail
end on

on w_moduli_stampa.destroy
call super::destroy
destroy(this.dw_detail)
end on

event pc_setwindow;call super::pc_setwindow;
dw_detail.set_dw_key("cod_azienda")
dw_detail.set_dw_options(sqlca, tab_1.lista.dw_lista, c_sharedata + c_scrollparent, c_default)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(tab_1.ricerca.dw_ricerca, &
	"cod_lingua", &
	sqlca, &
	"tab_lingue", &
	"cod_lingua", &
	"des_lingua", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
	
	
f_po_loaddddw_dw(dw_detail, &
	"cod_lingua", &
	sqlca, &
	"tab_lingue", &
	"cod_lingua", &
	"des_lingua", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
	
guo_functions.uof_load_dddw(dw_detail, "cod_modulo", "SELECT DISTINCT cod_modulo,cod_modulo FROM tab_moduli_stampa WHERE cod_azienda='" + s_cs_xx.cod_azienda +"' ")

end event

type tab_1 from w_cs_xx_master_detail`tab_1 within w_moduli_stampa
integer width = 2949
integer height = 1000
end type

on tab_1.create
call super::create
this.Control[]={this.lista,&
this.ricerca}
end on

on tab_1.destroy
call super::destroy
end on

type lista from w_cs_xx_master_detail`lista within tab_1
integer width = 2798
integer height = 968
end type

type dw_lista from w_cs_xx_master_detail`dw_lista within lista
integer width = 2766
string dataobject = "d_moduli_stampa_lista"
end type

event dw_lista::pcd_setkey;call super::pcd_setkey;string ls_cod_tipo_gestione
long ll_i, ll_progressivo


for ll_i = 1 to rowcount()
	if isnull(getitemstring(ll_i, "cod_azienda")) then
		setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if

	if isnull(getitemnumber(ll_i, "progressivo")) or getitemnumber(ll_i, "progressivo") <= 0 then
				
		select max(progressivo)
		into :ll_progressivo
		from tab_moduli_stampa
		where cod_azienda = :s_cs_xx.cod_azienda;
				 
		if sqlca.sqlcode <> 0 or isnull(ll_progressivo) then
			ll_progressivo = 0
		end if
		
		ll_progressivo++
		
		setitem(ll_i, "progressivo", ll_progressivo)
		
	end if
	
next
end event

type ricerca from w_cs_xx_master_detail`ricerca within tab_1
integer width = 2798
integer height = 968
end type

type dw_ricerca from w_cs_xx_master_detail`dw_ricerca within ricerca
integer height = 700
string dataobject = "d_moduli_stampa_ricerca"
end type

event dw_ricerca::buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca, "cod_cliente")
		
end choose
end event

type dw_detail from uo_cs_xx_dw within w_moduli_stampa
integer x = 23
integer y = 1040
integer width = 2697
integer height = 960
integer taborder = 11
boolean bringtotop = true
string dataobject = "d_moduli_stampa_det"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_detail, "cod_cliente")
		
	case "b_duplica"
		if row > 0 then
			if g_mb.confirm("Duplicare il modulo selezionato?") then
				wf_duplica(row)
			end if
		end if
		
end choose
end event

