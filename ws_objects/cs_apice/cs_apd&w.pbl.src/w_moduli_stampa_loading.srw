﻿$PBExportHeader$w_moduli_stampa_loading.srw
forward
global type w_moduli_stampa_loading from window
end type
type st_1 from statictext within w_moduli_stampa_loading
end type
type dw_1 from datawindow within w_moduli_stampa_loading
end type
end forward

global type w_moduli_stampa_loading from window
integer width = 2075
integer height = 436
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
st_1 st_1
dw_1 dw_1
end type
global w_moduli_stampa_loading w_moduli_stampa_loading

on w_moduli_stampa_loading.create
this.st_1=create st_1
this.dw_1=create dw_1
this.Control[]={this.st_1,&
this.dw_1}
end on

on w_moduli_stampa_loading.destroy
destroy(this.st_1)
destroy(this.dw_1)
end on

type st_1 from statictext within w_moduli_stampa_loading
integer x = 46
integer y = 160
integer width = 1989
integer height = 120
integer textsize = -18
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
string text = "Stampa in corso...."
alignment alignment = center!
boolean focusrectangle = false
end type

type dw_1 from datawindow within w_moduli_stampa_loading
boolean visible = false
integer x = 91
integer y = 240
integer width = 1920
integer height = 400
integer taborder = 10
string title = "none"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

