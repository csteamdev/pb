﻿$PBExportHeader$w_moduli_stampa_scelta.srw
forward
global type w_moduli_stampa_scelta from w_cs_xx_risposta
end type
type ddlb_printer from dropdownlistbox within w_moduli_stampa_scelta
end type
type st_1 from statictext within w_moduli_stampa_scelta
end type
type cb_annulla from commandbutton within w_moduli_stampa_scelta
end type
type cb_ok from commandbutton within w_moduli_stampa_scelta
end type
type dw_lista from uo_cs_xx_dw within w_moduli_stampa_scelta
end type
end forward

global type w_moduli_stampa_scelta from w_cs_xx_risposta
integer width = 1669
integer height = 1428
string title = "Scelta Moduli"
boolean controlmenu = false
ddlb_printer ddlb_printer
st_1 st_1
cb_annulla cb_annulla
cb_ok cb_ok
dw_lista dw_lista
end type
global w_moduli_stampa_scelta w_moduli_stampa_scelta

type variables
private:
	str_modulo_stampa istr_moduli[]
	uo_map iuo_map_moduli
end variables

forward prototypes
public subroutine wf_carica_stampanti ()
end prototypes

public subroutine wf_carica_stampanti ();string ls_prntrs, ls_printname, ls_curent_printer
int li_pos, li_current

li_current = 0

// Nome della stampante corrente
ls_curent_printer = PrintGetPrinter()
ls_curent_printer = left(ls_curent_printer, pos(ls_curent_printer, "~t") - 1)

// Lista delle stampanti disponibili
ls_prntrs  = PrintGetPrinters ( )
li_pos = pos(ls_prntrs, "~n")
do while  li_pos > 0
	ls_printname = left(ls_prntrs, pos(ls_prntrs, "~t") - 1)
	ddlb_printer.additem(ls_printname)
	ls_prntrs = mid(ls_prntrs, li_pos + 1)
	li_pos = pos(ls_prntrs, "~n")
loop

li_current = ddlb_printer.finditem(ls_curent_printer, 1)
li_current = ddlb_printer.selectitem(li_current)
end subroutine

on w_moduli_stampa_scelta.create
int iCurrent
call super::create
this.ddlb_printer=create ddlb_printer
this.st_1=create st_1
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.ddlb_printer
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.cb_ok
this.Control[iCurrent+5]=this.dw_lista
end on

on w_moduli_stampa_scelta.destroy
call super::destroy
destroy(this.ddlb_printer)
destroy(this.st_1)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.dw_lista)
end on

event pc_setwindow;call super::pc_setwindow;//string ls_where
//
//
//ls_where = message.stringparm
//
//setnull(message.stringparm)
//
//dw_lista.settransobject(sqlca)
//dw_lista.setsqlselect(dw_lista.getsqlselect() + ls_where)
//dw_lista.retrieve()
//
//wf_carica_stampanti()
int li_i, li_r
str_moduli_param lstr_param

lstr_param = message.powerobjectparm

if isvalid(lstr_param) then
	istr_moduli = lstr_param.moduli_stampa
	iuo_map_moduli = create uo_map
	
	for li_i = 1 to upperbound(istr_moduli)
		
		if istr_moduli[li_i].automatico = false then
			
			li_r = dw_lista.insertrow(0)
			dw_lista.setitem(li_r, "progressivo", istr_moduli[li_i].progressivo)
			dw_lista.setitem(li_r, "titolo", istr_moduli[li_i].descrizione)
			dw_lista.setitem(li_r, "selected", "N")
			
			iuo_map_moduli.add(string(istr_moduli[li_i].progressivo), istr_moduli[li_i])
			
		end if
		
	next
	
	wf_carica_stampanti()
else
	close(this)
end if



end event

type ddlb_printer from dropdownlistbox within w_moduli_stampa_scelta
boolean visible = false
integer x = 434
integer y = 1340
integer width = 1143
integer height = 360
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
end type

type st_1 from statictext within w_moduli_stampa_scelta
boolean visible = false
integer x = 23
integer y = 1360
integer width = 343
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Stampa su:"
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_moduli_stampa_scelta
integer x = 23
integer y = 1200
integer width = 389
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;str_moduli_param lstr_response

lstr_response.user_cancel = true

closewithreturn(parent, lstr_response)
end event

type cb_ok from commandbutton within w_moduli_stampa_scelta
integer x = 1211
integer y = 1200
integer width = 389
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Seleziona"
end type

event clicked;string ls_printername
int li_i, li_j
str_moduli_param lstr_response
str_modulo_stampa lstr_moduli[]

// Ricopio tutti i moduli automatici
for li_i = 1 to upperbound(istr_moduli)
	if istr_moduli[li_i].automatico then
		lstr_moduli[upperbound(lstr_moduli) + 1] = istr_moduli[li_i]
	end if
next

// Controllo i moduli selezionati
for li_i = 1 to dw_lista.rowcount()
	if dw_lista.getitemstring(li_i, "selected") = "S" then
		
		lstr_moduli[upperbound(lstr_moduli) +1] = iuo_map_moduli.get(string(dw_lista.getitemnumber(li_i, "progressivo")))
		
	end if
next

lstr_response.user_cancel = false
//lstr_response.printer_name = ddlb_printer.text
lstr_response.moduli_stampa = lstr_moduli

closewithreturn(parent, lstr_response)
end event

type dw_lista from uo_cs_xx_dw within w_moduli_stampa_scelta
integer x = 23
integer y = 20
integer width = 1577
integer height = 1140
integer taborder = 10
string dataobject = "d_moduli_stampa_scelta"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

