﻿$PBExportHeader$w_ricerca_note_fisse.srw
$PBExportComments$Mashera di ricerca note fisse
forward
global type w_ricerca_note_fisse from w_cs_xx_risposta
end type
type dw_ricerca_note_fisse from uo_cs_xx_dw within w_ricerca_note_fisse
end type
type cb_annulla from uo_cb_close within w_ricerca_note_fisse
end type
type cb_ok from uo_cb_ok within w_ricerca_note_fisse
end type
type dw_ricerca_note_fisse_det from uo_cs_xx_dw within w_ricerca_note_fisse
end type
end forward

global type w_ricerca_note_fisse from w_cs_xx_risposta
int Width=2574
int Height=1205
boolean TitleBar=true
string Title="Note Fisse"
dw_ricerca_note_fisse dw_ricerca_note_fisse
cb_annulla cb_annulla
cb_ok cb_ok
dw_ricerca_note_fisse_det dw_ricerca_note_fisse_det
end type
global w_ricerca_note_fisse w_ricerca_note_fisse

on w_ricerca_note_fisse.create
int iCurrent
call w_cs_xx_risposta::create
this.dw_ricerca_note_fisse=create dw_ricerca_note_fisse
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.dw_ricerca_note_fisse_det=create dw_ricerca_note_fisse_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_ricerca_note_fisse
this.Control[iCurrent+2]=cb_annulla
this.Control[iCurrent+3]=cb_ok
this.Control[iCurrent+4]=dw_ricerca_note_fisse_det
end on

on w_ricerca_note_fisse.destroy
call w_cs_xx_risposta::destroy
destroy(this.dw_ricerca_note_fisse)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.dw_ricerca_note_fisse_det)
end on

event pc_setwindow;call super::pc_setwindow;if s_cs_xx.parametri.parametro_s_1 = "%" then
	dw_ricerca_note_fisse.DataObject = 'd_ricerca_note_fisse_f'
elseif s_cs_xx.parametri.parametro_s_2 = "%" then
	dw_ricerca_note_fisse.DataObject = 'd_ricerca_note_fisse'
end if	

dw_ricerca_note_fisse.set_dw_key("cod_azienda")
dw_ricerca_note_fisse.set_dw_options(sqlca, &
                                      pcca.null_object, &
                                      c_default, &
                                      c_default)

dw_ricerca_note_fisse_det.set_dw_options(sqlca, &
                                   dw_ricerca_note_fisse, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)
											  
//iuo_dw_main = dw_ricerca_note_fisse											  

end event

type dw_ricerca_note_fisse from uo_cs_xx_dw within w_ricerca_note_fisse
int X=23
int Y=21
int Width=2492
int Height=581
int TabOrder=20
string DataObject="d_ricerca_note_fisse"
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_cli_for, ls_flag_offerta_ven, ls_flag_ordine_ven, ls_flag_bolla_ven, &
		 ls_flag_fattura_ven, ls_flag_offerta_acq, ls_flag_ordine_acq, ls_flag_piede_testata, &
		 ls_flag_bolla_acq, ls_flag_fattura_acq
datetime ld_data

if s_cs_xx.parametri.parametro_s_1 = "%" then
	ls_cod_cli_for = s_cs_xx.parametri.parametro_s_2
elseif s_cs_xx.parametri.parametro_s_2 = "%" then
	ls_cod_cli_for = s_cs_xx.parametri.parametro_s_1
end if	

//ls_cod_cliente = s_cs_xx.parametri.parametro_s_1
//ls_cod_fornitore = s_cs_xx.parametri.parametro_s_2
ls_flag_offerta_ven = s_cs_xx.parametri.parametro_s_3
ls_flag_ordine_ven = s_cs_xx.parametri.parametro_s_4
ls_flag_bolla_ven = s_cs_xx.parametri.parametro_s_5
ls_flag_fattura_ven = s_cs_xx.parametri.parametro_s_6
ls_flag_offerta_acq = s_cs_xx.parametri.parametro_s_7
ls_flag_ordine_acq = s_cs_xx.parametri.parametro_s_8
ls_flag_piede_testata = s_cs_xx.parametri.parametro_s_9
ld_data = s_cs_xx.parametri.parametro_data_1

if isnull(s_cs_xx.parametri.parametro_s_12) or &
	(s_cs_xx.parametri.parametro_s_12 <> 'S' and &
	s_cs_xx.parametri.parametro_s_12 <> 'N' and &
	s_cs_xx.parametri.parametro_s_12 <> '%') then
	s_cs_xx.parametri.parametro_s_12 = '%'
end if	

ls_flag_bolla_acq = s_cs_xx.parametri.parametro_s_12

if isnull(s_cs_xx.parametri.parametro_s_13) or &
	(s_cs_xx.parametri.parametro_s_13 <> 'S' and &
	s_cs_xx.parametri.parametro_s_13 <> 'N' and &
	s_cs_xx.parametri.parametro_s_13 <> '%') then
	s_cs_xx.parametri.parametro_s_13 = '%'
end if	

ls_flag_fattura_acq = s_cs_xx.parametri.parametro_s_13

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_cli_for, ld_data, ls_flag_offerta_ven, ls_flag_ordine_ven, ls_flag_bolla_ven, ls_flag_fattura_ven, ls_flag_offerta_acq, ls_flag_ordine_acq, ls_flag_piede_testata, ls_flag_bolla_acq, ls_flag_fattura_acq)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

end event

event pcd_setkey;call super::pcd_setkey;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type cb_annulla from uo_cb_close within w_ricerca_note_fisse
int X=1761
int Y=1001
int Width=366
int Height=81
int TabOrder=30
string Text="&Annulla"
boolean Cancel=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_ok from uo_cb_ok within w_ricerca_note_fisse
int X=2149
int Y=1001
int Width=366
int Height=81
int TabOrder=10
boolean Default=true
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;call super::clicked;if dw_ricerca_note_fisse.getrow() > 0 then
   s_cs_xx.parametri.parametro_s_11 = dw_ricerca_note_fisse.getitemstring(dw_ricerca_note_fisse.getrow(),"cod_nota_fissa")
else
   setnull(s_cs_xx.parametri.parametro_s_11)
end if
close(parent)
end event

type dw_ricerca_note_fisse_det from uo_cs_xx_dw within w_ricerca_note_fisse
int X=23
int Y=621
int Width=2492
int Height=361
int TabOrder=2
string DataObject="d_ricerca_note_fisse_det"
boolean LiveScroll=true
end type

