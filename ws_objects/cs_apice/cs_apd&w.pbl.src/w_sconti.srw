﻿$PBExportHeader$w_sconti.srw
$PBExportComments$Finestra Gestione  Sconti
forward
global type w_sconti from Window
end type
type cb_ok from commandbutton within w_sconti
end type
type cb_annulla from commandbutton within w_sconti
end type
type dw_sconti from datawindow within w_sconti
end type
end forward

global type w_sconti from Window
int X=833
int Y=357
int Width=2707
int Height=465
boolean TitleBar=true
string Title="Sconti"
long BackColor=12632256
boolean ControlMenu=true
WindowType WindowType=response!
cb_ok cb_ok
cb_annulla cb_annulla
dw_sconti dw_sconti
end type
global w_sconti w_sconti

on open;integer li_riga
decimal ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, &
        ld_sconto_9, ld_sconto_10

dw_sconti.insertrow(1)

li_riga = s_cs_xx.parametri.parametro_uo_dw_1.getrow()

ld_sconto_3 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(li_riga, "sconto_3")
ld_sconto_4 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(li_riga, "sconto_4")
ld_sconto_5 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(li_riga, "sconto_5")
ld_sconto_6 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(li_riga, "sconto_6")
ld_sconto_7 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(li_riga, "sconto_7")
ld_sconto_8 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(li_riga, "sconto_8")
ld_sconto_9 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(li_riga, "sconto_9")
ld_sconto_10 = s_cs_xx.parametri.parametro_uo_dw_1.getitemnumber(li_riga, "sconto_10")

dw_sconti.setitem(1, "sconto_3", ld_sconto_3)
dw_sconti.setitem(1, "sconto_4", ld_sconto_4)
dw_sconti.setitem(1, "sconto_5", ld_sconto_5)
dw_sconti.setitem(1, "sconto_6", ld_sconto_6)
dw_sconti.setitem(1, "sconto_7", ld_sconto_7)
dw_sconti.setitem(1, "sconto_8", ld_sconto_8)
dw_sconti.setitem(1, "sconto_9", ld_sconto_9)
dw_sconti.setitem(1, "sconto_10", ld_sconto_10)

end on

on w_sconti.create
this.cb_ok=create cb_ok
this.cb_annulla=create cb_annulla
this.dw_sconti=create dw_sconti
this.Control[]={ this.cb_ok,&
this.cb_annulla,&
this.dw_sconti}
end on

on w_sconti.destroy
destroy(this.cb_ok)
destroy(this.cb_annulla)
destroy(this.dw_sconti)
end on

type cb_ok from commandbutton within w_sconti
int X=1921
int Y=281
int Width=366
int Height=81
int TabOrder=20
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;dec ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, &
    ld_sconto_9, ld_sconto_10
integer li_riga


li_riga = s_cs_xx.parametri.parametro_uo_dw_1.getrow()

dw_sconti.setcolumn("sconto_3")
ld_sconto_3 = dw_sconti.getitemnumber(1, "sconto_3")
dw_sconti.setcolumn("sconto_4")
ld_sconto_4 = dw_sconti.getitemnumber(1, "sconto_4")
dw_sconti.setcolumn("sconto_5")
ld_sconto_5 = dw_sconti.getitemnumber(1, "sconto_5")
dw_sconti.setcolumn("sconto_6")
ld_sconto_6 = dw_sconti.getitemnumber(1, "sconto_6")
dw_sconti.setcolumn("sconto_7")
ld_sconto_7 = dw_sconti.getitemnumber(1, "sconto_7")
dw_sconti.setcolumn("sconto_8")
ld_sconto_8 = dw_sconti.getitemnumber(1, "sconto_8")
dw_sconti.setcolumn("sconto_9")
ld_sconto_9 = dw_sconti.getitemnumber(1, "sconto_9")
dw_sconti.setcolumn("sconto_10")
ld_sconto_10 = dw_sconti.getitemnumber(1, "sconto_10")

s_cs_xx.parametri.parametro_uo_dw_1.setitem(li_riga, "sconto_3", ld_sconto_3)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(li_riga, "sconto_4", ld_sconto_4)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(li_riga, "sconto_5", ld_sconto_5)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(li_riga, "sconto_6", ld_sconto_6)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(li_riga, "sconto_7", ld_sconto_7)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(li_riga, "sconto_8", ld_sconto_8)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(li_riga, "sconto_9", ld_sconto_9)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(li_riga, "sconto_10", ld_sconto_10)

close(parent)
end on

type cb_annulla from commandbutton within w_sconti
int X=2309
int Y=281
int Width=366
int Height=81
int TabOrder=30
string Text="&Annulla"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

on clicked;close(parent)
end on

type dw_sconti from datawindow within w_sconti
int X=23
int Y=21
int Width=2652
int Height=241
int TabOrder=10
string DataObject="d_sconti"
boolean LiveScroll=true
end type

