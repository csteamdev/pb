﻿$PBExportHeader$w_tab_collegamenti.srw
forward
global type w_tab_collegamenti from w_cs_xx_principale
end type
type dw_tab_collegamenti_lista from uo_cs_xx_dw within w_tab_collegamenti
end type
type dw_tab_collegamenti_det from uo_cs_xx_dw within w_tab_collegamenti
end type
end forward

global type w_tab_collegamenti from w_cs_xx_principale
int Width=2995
int Height=1281
boolean TitleBar=true
string Title="Gestione Collegamenti"
dw_tab_collegamenti_lista dw_tab_collegamenti_lista
dw_tab_collegamenti_det dw_tab_collegamenti_det
end type
global w_tab_collegamenti w_tab_collegamenti

on w_tab_collegamenti.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tab_collegamenti_lista=create dw_tab_collegamenti_lista
this.dw_tab_collegamenti_det=create dw_tab_collegamenti_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tab_collegamenti_lista
this.Control[iCurrent+2]=dw_tab_collegamenti_det
end on

on w_tab_collegamenti.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tab_collegamenti_lista)
destroy(this.dw_tab_collegamenti_det)
end on

event open;call super::open;dw_tab_collegamenti_lista.set_dw_key("cod_azienda")
dw_tab_collegamenti_lista.set_dw_options(sqlca, &
                                  pcca.null_object, &
                                  c_default, &
                                  c_default)
dw_tab_collegamenti_det.set_dw_options(sqlca, &
                                  dw_tab_collegamenti_lista, &
                                  c_sharedata + c_scrollparent, &
                                  c_default)
											 
iuo_dw_main = dw_tab_collegamenti_lista

end event

type dw_tab_collegamenti_lista from uo_cs_xx_dw within w_tab_collegamenti
int X=23
int Y=21
int Width=2903
int Height=681
string DataObject="d_tab_collegamenti_lista"
boolean HScrollBar=true
boolean VScrollBar=true
boolean LiveScroll=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end event

event pcd_new;call super::pcd_new;long ll_progressivo

select max(progressivo)
into :ll_progressivo
from tab_collegamenti
where cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <0 then
	g_mb.messagebox("Apice: Errore in lettura tabella collegamenti", "Impostare il progressivo manualmente")
end if	

if sqlca.sqlcode =100 then
	ll_progressivo = 1
else
	ll_progressivo = ll_progressivo + 1
end if

this.setitem(this.getrow(), "progressivo", ll_progressivo)


end event

type dw_tab_collegamenti_det from uo_cs_xx_dw within w_tab_collegamenti
int X=23
int Y=721
int Width=2903
int Height=421
int TabOrder=2
string DataObject="d_tab_collegamenti_det"
end type

