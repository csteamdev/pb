﻿$PBExportHeader$w_tab_conversione_num_lett.srw
$PBExportComments$Finestra Conversione Numer in Lettere
forward
global type w_tab_conversione_num_lett from w_cs_xx_principale
end type
type dw_tab_conversione_num_lett from uo_cs_xx_dw within w_tab_conversione_num_lett
end type
type cb_1 from commandbutton within w_tab_conversione_num_lett
end type
type em_1 from editmask within w_tab_conversione_num_lett
end type
type cb_2 from commandbutton within w_tab_conversione_num_lett
end type
type st_1 from statictext within w_tab_conversione_num_lett
end type
end forward

global type w_tab_conversione_num_lett from w_cs_xx_principale
integer width = 1289
integer height = 1120
string title = "Conversione Numeri in Lettere"
dw_tab_conversione_num_lett dw_tab_conversione_num_lett
cb_1 cb_1
em_1 em_1
cb_2 cb_2
st_1 st_1
end type
global w_tab_conversione_num_lett w_tab_conversione_num_lett

on w_tab_conversione_num_lett.create
int iCurrent
call super::create
this.dw_tab_conversione_num_lett=create dw_tab_conversione_num_lett
this.cb_1=create cb_1
this.em_1=create em_1
this.cb_2=create cb_2
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tab_conversione_num_lett
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.em_1
this.Control[iCurrent+4]=this.cb_2
this.Control[iCurrent+5]=this.st_1
end on

on w_tab_conversione_num_lett.destroy
call super::destroy
destroy(this.dw_tab_conversione_num_lett)
destroy(this.cb_1)
destroy(this.em_1)
destroy(this.cb_2)
destroy(this.st_1)
end on

event pc_setwindow;call super::pc_setwindow;long ll_cont

ll_cont = 0
select count(*)
into   :ll_cont
from   tab_conversione_num_lett;

if ll_cont < 1 then
	dw_tab_conversione_num_lett.set_dw_options(sqlca, &
												 pcca.null_object, &
												 c_nonew + &
												 c_nodelete + &
												 c_disablecc, &  
												 c_default)
else
	dw_tab_conversione_num_lett.set_dw_options(sqlca, &
												 pcca.null_object, &
												 c_nonew + &
												 c_nodelete + &
												 c_modifyonopen + &
												 c_disablecc, &  
												 c_default)
	cb_1.enabled = false
end if
iuo_dw_main = dw_tab_conversione_num_lett
dw_tab_conversione_num_lett.ib_proteggi_chiavi = false
em_1.text = "123"
end event

type dw_tab_conversione_num_lett from uo_cs_xx_dw within w_tab_conversione_num_lett
integer x = 23
integer y = 20
integer width = 846
integer height = 876
integer taborder = 20
string dataobject = "d_tab_conversione_num_lett"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

type cb_1 from commandbutton within w_tab_conversione_num_lett
integer x = 891
integer y = 20
integer width = 343
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Crea Tab."
end type

event clicked;INSERT INTO tab_conversione_num_lett  
         ( conv_1,   
           conv_2,   
           conv_3,   
           conv_4,   
           conv_5,   
           conv_6,   
           conv_7,   
           conv_8,   
           conv_9,   
           conv_0 )  
VALUES   ( 'A',   
           'E',   
           'G',   
           'H',   
           'M',   
           'P',   
           'S',   
           'T',   
           'K',   
           'Z' )  ;

COMMIT;

parent.triggerevent("pc_retrieve")
end event

type em_1 from editmask within w_tab_conversione_num_lett
integer x = 23
integer y = 920
integer width = 457
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
alignment alignment = right!
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = decimalmask!
string mask = "###,###,###,###.####"
string displaydata = "Ä"
end type

type cb_2 from commandbutton within w_tab_conversione_num_lett
integer x = 503
integer y = 920
integer width = 242
integer height = 80
integer taborder = 3
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Test >>"
end type

event clicked;double ld_numero
string ls_stringa

ld_numero = double(em_1.text)
f_con_num_lett(ld_numero, ls_stringa)
st_1.text = ls_stringa
end event

type st_1 from statictext within w_tab_conversione_num_lett
integer x = 777
integer y = 920
integer width = 457
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 67108864
boolean enabled = false
alignment alignment = right!
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

