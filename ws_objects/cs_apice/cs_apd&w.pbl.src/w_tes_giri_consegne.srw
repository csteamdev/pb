﻿$PBExportHeader$w_tes_giri_consegne.srw
$PBExportComments$Finestra Testata Giri Consegne
forward
global type w_tes_giri_consegne from w_cs_xx_principale
end type
type dw_tes_giri_consegne_det from uo_cs_xx_dw within w_tes_giri_consegne
end type
type cb_dettaglio from commandbutton within w_tes_giri_consegne
end type
type dw_folder from u_folder within w_tes_giri_consegne
end type
type dw_tes_giri_consegne_lista from uo_cs_xx_dw within w_tes_giri_consegne
end type
type dw_ricerca from u_dw_search within w_tes_giri_consegne
end type
end forward

global type w_tes_giri_consegne from w_cs_xx_principale
integer width = 2533
integer height = 1832
string title = "Giri Consegne"
dw_tes_giri_consegne_det dw_tes_giri_consegne_det
cb_dettaglio cb_dettaglio
dw_folder dw_folder
dw_tes_giri_consegne_lista dw_tes_giri_consegne_lista
dw_ricerca dw_ricerca
end type
global w_tes_giri_consegne w_tes_giri_consegne

type variables


string			is_sql_base
end variables

forward prototypes
public function integer wf_ricerca ()
public subroutine wf_reset ()
end prototypes

public function integer wf_ricerca ();string			ls_sql, ls_cod_giro_consegna, ls_des_giro_consegna, ls_cod_deposito, ls_cod_cliente, ls_flag_blocco, ls_flag_rc

setpointer(Hourglass!)

dw_ricerca.accepttext()

ls_sql = is_sql_base

ls_sql += " where cod_azienda='"+s_cs_xx.cod_azienda+"'"

ls_cod_giro_consegna	= dw_ricerca.getitemstring(1, "cod_giro_consegna")
ls_des_giro_consegna	= dw_ricerca.getitemstring(1, "des_giro_consegna")
ls_cod_deposito			= dw_ricerca.getitemstring(1, "cod_deposito")
ls_cod_cliente				= dw_ricerca.getitemstring(1, "cod_cliente")
ls_flag_blocco				= dw_ricerca.getitemstring(1, "flag_blocco")
ls_flag_rc					= dw_ricerca.getitemstring(1, "flag_rc")

if g_str.isnotempty(ls_cod_giro_consegna) then ls_sql += " and cod_giro_consegna like '"+ls_cod_giro_consegna+"'"
if g_str.isnotempty(ls_des_giro_consegna) then ls_sql += " and des_giro_consegna like '%"+ls_des_giro_consegna+"%'"
if g_str.isnotempty(ls_cod_deposito) then ls_sql += " and cod_deposito='"+ls_cod_deposito+"'"

if g_str.isnotempty(ls_cod_cliente) then ls_sql += " and cod_giro_consegna IN (select cod_giro_consegna "+&
																									"from det_giri_consegne "+&
																									"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
																											 "cod_cliente like '"+ls_cod_cliente+"')"

if ls_flag_blocco="S" or ls_flag_blocco="N" then ls_sql += " and flag_blocco='"+ls_flag_blocco+"'"
if ls_flag_rc="S" or ls_flag_rc="N" then ls_sql += " and flag_rc='"+ls_flag_rc+"'"


dw_tes_giri_consegne_lista.setsqlselect(ls_sql)
dw_tes_giri_consegne_lista.change_dw_current()

this.triggerevent("pc_retrieve")

dw_folder.fu_selecttab(2)
setpointer(Arrow!)


return 0
end function

public subroutine wf_reset ();string			ls_null


dw_ricerca.setitem(1, "cod_giro_consegna", ls_null)
dw_ricerca.setitem(1, "des_giro_consegna", ls_null)
dw_ricerca.setitem(1, "cod_deposito", ls_null)
dw_ricerca.setitem(1, "flag_rc", "T")
dw_ricerca.setitem(1, "cod_cliente", ls_null)
dw_ricerca.setitem(1, "flag_blocco", "N")


return
end subroutine

on w_tes_giri_consegne.create
int iCurrent
call super::create
this.dw_tes_giri_consegne_det=create dw_tes_giri_consegne_det
this.cb_dettaglio=create cb_dettaglio
this.dw_folder=create dw_folder
this.dw_tes_giri_consegne_lista=create dw_tes_giri_consegne_lista
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_giri_consegne_det
this.Control[iCurrent+2]=this.cb_dettaglio
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_tes_giri_consegne_lista
this.Control[iCurrent+5]=this.dw_ricerca
end on

on w_tes_giri_consegne.destroy
call super::destroy
destroy(this.dw_tes_giri_consegne_det)
destroy(this.cb_dettaglio)
destroy(this.dw_folder)
destroy(this.dw_tes_giri_consegne_lista)
destroy(this.dw_ricerca)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],  lw_vuoto[]



dw_tes_giri_consegne_lista.set_dw_key("cod_azienda")
dw_tes_giri_consegne_lista.set_dw_key("cod_giro_consegna")
dw_tes_giri_consegne_lista.set_dw_options(sqlca,pcca.null_object,c_noretrieveonopen,c_default)
dw_tes_giri_consegne_det.set_dw_options(sqlca,dw_tes_giri_consegne_lista,c_sharedata+c_scrollparent,c_default)


lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_ricerca
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])

lw_oggetti = lw_vuoto
lw_oggetti[1] = dw_tes_giri_consegne_lista
dw_folder.fu_assigntab(2, "Lista", lw_oggetti[])

dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)

iuo_dw_main = dw_tes_giri_consegne_lista

is_sql_base = dw_tes_giri_consegne_lista.getsqlselect()
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW( dw_ricerca, &
	"cod_deposito", &
	sqlca, &
	"anag_depositi", &
	"cod_deposito", &
	"des_deposito", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' ")


f_PO_LoadDDDW_DW( dw_tes_giri_consegne_det, &
	"cod_deposito", &
	sqlca, &
	"anag_depositi", &
	"cod_deposito", &
	"des_deposito", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) ")
	
end event

type dw_tes_giri_consegne_det from uo_cs_xx_dw within w_tes_giri_consegne
integer x = 23
integer y = 852
integer width = 2459
integer height = 744
integer taborder = 10
string dataobject = "d_tes_giri_consegne_det"
borderstyle borderstyle = styleraised!
end type

type cb_dettaglio from commandbutton within w_tes_giri_consegne
integer x = 2107
integer y = 1632
integer width = 375
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettagli"
end type

event clicked;window_open_parm(w_det_giri_consegne, -1, dw_tes_giri_consegne_lista)

end event

type dw_folder from u_folder within w_tes_giri_consegne
integer x = 5
integer y = 20
integer width = 2459
integer height = 804
integer taborder = 30
boolean border = false
end type

type dw_tes_giri_consegne_lista from uo_cs_xx_dw within w_tes_giri_consegne
integer x = 27
integer y = 112
integer width = 2382
integer height = 704
integer taborder = 20
boolean bringtotop = true
string dataobject = "d_tes_giri_consegne_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve()

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event pcd_setkey;call super::pcd_setkey;LONG  l_Idx

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_modify;call super::pcd_modify;cb_dettaglio.enabled = false
end event

event pcd_new;call super::pcd_new;cb_dettaglio.enabled = false
end event

event pcd_save;call super::pcd_save;cb_dettaglio.enabled = true
end event

event pcd_view;call super::pcd_view;cb_dettaglio.enabled = true
end event

event updatestart;call super::updatestart;integer li_i
string ls_cod_giro_consegna
if i_extendmode then
   for li_i = 1 to this.deletedcount()
      ls_cod_giro_consegna = this.getitemstring(li_i, "cod_giro_consegna", delete!, true)
		
		delete det_giri_consegne
		where  cod_azienda=:s_cs_xx.cod_azienda
		and    cod_giro_consegna = :ls_cod_giro_consegna;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore Durante Cancellazione Dettaglio Giri Consegne",stopsign!)
			return 1
		end if
   next
end if
end event

type dw_ricerca from u_dw_search within w_tes_giri_consegne
integer x = 23
integer y = 112
integer width = 2377
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_tes_giri_consegne_ricerca"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca"
		wf_ricerca()
		
	case "b_annulla"
		wf_reset()
		
	case "b_sel_giro"
		guo_ricerca.uof_ricerca_giro_consegna(dw_ricerca, "cod_giro_consegna")
		
	case "b_sel_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca, "cod_cliente")
		
		
end choose
end event

