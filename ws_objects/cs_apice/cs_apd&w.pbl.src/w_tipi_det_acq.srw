﻿$PBExportHeader$w_tipi_det_acq.srw
$PBExportComments$Finestra Gestione Tipi Dettaglio Acquisti
forward
global type w_tipi_det_acq from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_tipi_det_acq
end type
type dw_tipi_det_acq_lista from uo_cs_xx_dw within w_tipi_det_acq
end type
type dw_tipi_det_acq_det from uo_cs_xx_dw within w_tipi_det_acq
end type
end forward

global type w_tipi_det_acq from w_cs_xx_principale
integer width = 2222
integer height = 2112
string title = "Gestione Tipi Dettagli Acquisto"
cb_1 cb_1
dw_tipi_det_acq_lista dw_tipi_det_acq_lista
dw_tipi_det_acq_det dw_tipi_det_acq_det
end type
global w_tipi_det_acq w_tipi_det_acq

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tipi_det_acq_det, &
                 "cod_tipo_movimento", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_det_acq_det, &
                 "cod_tipo_mov_ret_pos", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_det_acq_det, &
                 "cod_tipo_mov_ret_neg", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_det_acq_det, &
                 "cod_gruppo", &
                 sqlca, &
                 "tab_gruppi", &
                 "cod_gruppo", &
                 "des_gruppo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tipi_det_acq_det, &
                 "cod_iva", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tipi_det_acq_lista.set_dw_key("cod_azienda")
dw_tipi_det_acq_lista.set_dw_options(sqlca, &
                                     pcca.null_object, &
                                     c_default, &
                                     c_default)
dw_tipi_det_acq_det.set_dw_options(sqlca, &
                                   dw_tipi_det_acq_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)
iuo_dw_main = dw_tipi_det_acq_lista
end on

on w_tipi_det_acq.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_tipi_det_acq_lista=create dw_tipi_det_acq_lista
this.dw_tipi_det_acq_det=create dw_tipi_det_acq_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_tipi_det_acq_lista
this.Control[iCurrent+3]=this.dw_tipi_det_acq_det
end on

on w_tipi_det_acq.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_tipi_det_acq_lista)
destroy(this.dw_tipi_det_acq_det)
end on

type cb_1 from commandbutton within w_tipi_det_acq
integer x = 1737
integer y = 1912
integer width = 430
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Centro di Costo"
end type

event clicked;if dw_tipi_det_acq_lista.rowcount() > 0 then
	window_open_parm(w_tipi_det_acq_cc, -1, dw_tipi_det_acq_lista)

end if
end event

type dw_tipi_det_acq_lista from uo_cs_xx_dw within w_tipi_det_acq
integer x = 23
integer y = 20
integer width = 2149
integer height = 500
integer taborder = 10
string dataobject = "d_tipi_det_acq_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	string ls_flag_tipo_det_acq, ls_flag_distribuibile, ls_nulla
	setnull(ls_nulla)
	
	ls_flag_tipo_det_acq = getitemstring(getrow(),"flag_tipo_det_acq")
	ls_flag_distribuibile = getitemstring(getrow(),"flag_distribuibile")
	dw_tipi_det_acq_det.SetTabOrder("cod_tipo_det_distr", 0) 
	if not isnull(ls_flag_tipo_det_acq) then
		f_po_loaddddw_dw(dw_tipi_det_acq_det, &
							  "cod_tipo_det_distr", &
							  sqlca, &
							  "tab_tipi_det_acq", &
							  "cod_tipo_det_acq", &
							  "des_tipo_det_acq", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_det_acq = '" + ls_flag_tipo_det_acq + "' ")
	end if

//	if not isnull(ls_flag_distribuibile) and ls_flag_distribuibile="S" then
//		dw_tipi_det_acq_det.SetTabOrder("cod_tipo_det_distr", 130) 
//	else
////		setitem(getrow(), "cod_tipo_det_distr", ls_nulla)
//		dw_tipi_det_acq_det.SetTabOrder("cod_tipo_det_distr", 0) 
//	end if
//
end if


end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	string ls_flag_distribuibile
	ls_flag_distribuibile = getitemstring(getrow(),"flag_distribuibile")

	if not isnull(ls_flag_distribuibile) and ls_flag_distribuibile="S" then
		dw_tipi_det_acq_det.SetTabOrder("cod_tipo_det_distr", 130) 
	else
		dw_tipi_det_acq_det.SetTabOrder("cod_tipo_det_distr", 0) 
	end if

end if


end event

event pcd_new;call super::pcd_new;dw_tipi_det_acq_det.setitem(dw_tipi_det_acq_lista.getrow(), "flag_doc_suc", "S")
dw_tipi_det_acq_det.setitem(dw_tipi_det_acq_lista.getrow(), "flag_st_note", "S")
dw_tipi_det_acq_det.setitem(dw_tipi_det_acq_lista.getrow(), "flag_doc_suc_or", "S")
dw_tipi_det_acq_det.setitem(dw_tipi_det_acq_lista.getrow(), "flag_st_note_or", "S")
dw_tipi_det_acq_det.setitem(dw_tipi_det_acq_lista.getrow(), "flag_doc_suc_bl", "S")
dw_tipi_det_acq_det.setitem(dw_tipi_det_acq_lista.getrow(), "flag_st_note_bl", "S")
dw_tipi_det_acq_det.setitem(dw_tipi_det_acq_lista.getrow(), "flag_st_note_ft", "S")
end event

type dw_tipi_det_acq_det from uo_cs_xx_dw within w_tipi_det_acq
integer x = 23
integer y = 540
integer width = 2149
integer height = 1360
integer taborder = 20
string dataobject = "d_tipi_det_acq_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_flag_tipo_det_acq, ls_flag_distribuibile, ls_cod_tipo_det_acq, ls_nulla
	setnull(ls_nulla)
	
	if i_colname = "flag_distribuibile" then
		if i_coltext = "S" then
			dw_tipi_det_acq_det.SetTabOrder("cod_tipo_det_distr", 130) 
//			dw_tipi_det_acq_det.Modify("cod_tipo_det_distr.Protect=0")
			ls_flag_tipo_det_acq = getitemstring(getrow(),"flag_tipo_det_acq")
			if not isnull(ls_flag_tipo_det_acq) then
				f_po_loaddddw_dw(dw_tipi_det_acq_det, &
									  "cod_tipo_det_distr", &
									  sqlca, &
									  "tab_tipi_det_acq", &
									  "cod_tipo_det_acq", &
									  "des_tipo_det_acq", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_det_acq = '" + ls_flag_tipo_det_acq + "' ")
			end if
		else
			setitem(getrow(), "cod_tipo_det_distr", ls_nulla)
			dw_tipi_det_acq_det.SetTabOrder("cod_tipo_det_distr", 0) 
		end if
	end if
	if i_colname = "cod_tipo_det_distr" then
		ls_cod_tipo_det_acq = getitemstring(getrow(),"cod_tipo_det_acq")
		if i_coltext = ls_cod_tipo_det_acq then return 2
	end if
	
end if


end event

