﻿$PBExportHeader$w_tipi_det_ven.srw
$PBExportComments$Finestra Gestione Tipi Dettagli Vendita
forward
global type w_tipi_det_ven from w_cs_xx_principale
end type
type st_1 from statictext within w_tipi_det_ven
end type
type cb_2 from commandbutton within w_tipi_det_ven
end type
type dw_tipi_det_ven_lista from uo_cs_xx_dw within w_tipi_det_ven
end type
type dw_tipi_det_ven_det from uo_cs_xx_dw within w_tipi_det_ven
end type
end forward

global type w_tipi_det_ven from w_cs_xx_principale
integer width = 2322
integer height = 2160
string title = "Gestione Tipi Dettagli Vendita"
st_1 st_1
cb_2 cb_2
dw_tipi_det_ven_lista dw_tipi_det_ven_lista
dw_tipi_det_ven_det dw_tipi_det_ven_det
end type
global w_tipi_det_ven w_tipi_det_ven

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tipi_det_ven_det, &
                 "cod_tipo_movimento", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_det_ven_det, &
                 "cod_gruppo", &
                 sqlca, &
                 "tab_gruppi", &
                 "cod_gruppo", &
                 "des_gruppo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tipi_det_ven_det, &
                 "cod_iva", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tipi_det_ven_det, &
                 "cod_tipo_det_ven_proforma", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_det_ven = 'C'")


end event

event pc_setwindow;call super::pc_setwindow;dw_tipi_det_ven_lista.set_dw_key("cod_azienda")
dw_tipi_det_ven_lista.set_dw_options(sqlca, &
                                     pcca.null_object, &
                                     c_default, &
                                     c_default)
dw_tipi_det_ven_det.set_dw_options(sqlca, &
                                   dw_tipi_det_ven_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)
			  
iuo_dw_main = dw_tipi_det_ven_lista
end event

on w_tipi_det_ven.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_2=create cb_2
this.dw_tipi_det_ven_lista=create dw_tipi_det_ven_lista
this.dw_tipi_det_ven_det=create dw_tipi_det_ven_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.dw_tipi_det_ven_lista
this.Control[iCurrent+4]=this.dw_tipi_det_ven_det
end on

on w_tipi_det_ven.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_2)
destroy(this.dw_tipi_det_ven_lista)
destroy(this.dw_tipi_det_ven_det)
end on

type st_1 from statictext within w_tipi_det_ven
integer x = 18
integer y = 1976
integer width = 1792
integer height = 68
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Lo ~"SCONTO~" non segue la distribuzione CC dei gruppi contabili"
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_tipi_det_ven
integer x = 1806
integer y = 1964
integer width = 475
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Centro di Costo"
end type

event clicked;if dw_tipi_det_ven_lista.rowcount() > 0 then
	window_open_parm(w_tipi_det_ven_cc, -1, dw_tipi_det_ven_lista)

end if
end event

type dw_tipi_det_ven_lista from uo_cs_xx_dw within w_tipi_det_ven
integer x = 27
integer y = 20
integer width = 2240
integer height = 500
integer taborder = 10
string dataobject = "d_tipi_det_ven_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_new;call super::pcd_new;dw_tipi_det_ven_det.setitem(dw_tipi_det_ven_lista.getrow(), "flag_doc_suc", "S")
dw_tipi_det_ven_det.setitem(dw_tipi_det_ven_lista.getrow(), "flag_st_note", "S")
dw_tipi_det_ven_det.setitem(dw_tipi_det_ven_lista.getrow(), "flag_doc_suc_or", "S")
dw_tipi_det_ven_det.setitem(dw_tipi_det_ven_lista.getrow(), "flag_st_note_or", "S")
dw_tipi_det_ven_det.setitem(dw_tipi_det_ven_lista.getrow(), "flag_doc_suc_bl", "S")
dw_tipi_det_ven_det.setitem(dw_tipi_det_ven_lista.getrow(), "flag_st_note_bl", "S")
dw_tipi_det_ven_det.setitem(dw_tipi_det_ven_lista.getrow(), "flag_doc_suc_ft", "S")
dw_tipi_det_ven_det.setitem(dw_tipi_det_ven_lista.getrow(), "flag_st_note_ft", "S")
end event

type dw_tipi_det_ven_det from uo_cs_xx_dw within w_tipi_det_ven
integer x = 23
integer y = 540
integer width = 2240
integer height = 1420
integer taborder = 20
string dataobject = "d_tipi_det_ven_det"
borderstyle borderstyle = styleraised!
end type

