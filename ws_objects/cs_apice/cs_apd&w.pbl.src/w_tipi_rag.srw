﻿$PBExportHeader$w_tipi_rag.srw
$PBExportComments$Finestra Tipi Raggruppamento
forward
global type w_tipi_rag from w_cs_xx_principale
end type
type dw_tipi_rag_lista from uo_cs_xx_dw within w_tipi_rag
end type
type dw_tipi_rag_det from uo_cs_xx_dw within w_tipi_rag
end type
end forward

global type w_tipi_rag from w_cs_xx_principale
int Width=3283
int Height=1257
boolean TitleBar=true
string Title="Gestione Raggruppamenti"
dw_tipi_rag_lista dw_tipi_rag_lista
dw_tipi_rag_det dw_tipi_rag_det
end type
global w_tipi_rag w_tipi_rag

event pc_setwindow;call super::pc_setwindow;dw_tipi_rag_lista.set_dw_key("cod_azienda")
dw_tipi_rag_lista.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_default, &
                                 c_default)
dw_tipi_rag_det.set_dw_options(sqlca, &
                               dw_tipi_rag_lista, &
                               c_sharedata + c_scrollparent, &
                               c_default)
iuo_dw_main = dw_tipi_rag_lista
end event

on w_tipi_rag.create
int iCurrent
call w_cs_xx_principale::create
this.dw_tipi_rag_lista=create dw_tipi_rag_lista
this.dw_tipi_rag_det=create dw_tipi_rag_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_tipi_rag_lista
this.Control[iCurrent+2]=dw_tipi_rag_det
end on

on w_tipi_rag.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_tipi_rag_lista)
destroy(this.dw_tipi_rag_det)
end on

type dw_tipi_rag_lista from uo_cs_xx_dw within w_tipi_rag
int X=23
int Y=21
int Width=1463
int Height=1121
int TabOrder=10
string DataObject="d_tipi_rag_lista"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
boolean LiveScroll=true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

type dw_tipi_rag_det from uo_cs_xx_dw within w_tipi_rag
int X=1509
int Y=21
int Width=1715
int Height=1121
int TabOrder=20
string DataObject="d_tipi_rag_det"
BorderStyle BorderStyle=StyleRaised!
end type

