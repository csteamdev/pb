﻿$PBExportHeader$w_transazioni_barcode.srw
forward
global type w_transazioni_barcode from w_cs_xx_principale
end type
type cb_ddt from commandbutton within w_transazioni_barcode
end type
type em_totale from editmask within w_transazioni_barcode
end type
type em_imponibile from editmask within w_transazioni_barcode
end type
type st_11 from statictext within w_transazioni_barcode
end type
type st_10 from statictext within w_transazioni_barcode
end type
type cb_fatt from commandbutton within w_transazioni_barcode
end type
type cb_1 from commandbutton within w_transazioni_barcode
end type
type st_6 from statictext within w_transazioni_barcode
end type
type st_5 from statictext within w_transazioni_barcode
end type
type st_4 from statictext within w_transazioni_barcode
end type
type st_3 from statictext within w_transazioni_barcode
end type
type st_2 from statictext within w_transazioni_barcode
end type
type st_1 from statictext within w_transazioni_barcode
end type
type dw_transazioni_barcode from uo_cs_xx_dw within w_transazioni_barcode
end type
type dw_selezione_transazioni_barcode from uo_cs_xx_dw within w_transazioni_barcode
end type
end forward

global type w_transazioni_barcode from w_cs_xx_principale
integer width = 4014
integer height = 1760
string title = "Transazioni"
cb_ddt cb_ddt
em_totale em_totale
em_imponibile em_imponibile
st_11 st_11
st_10 st_10
cb_fatt cb_fatt
cb_1 cb_1
st_6 st_6
st_5 st_5
st_4 st_4
st_3 st_3
st_2 st_2
st_1 st_1
dw_transazioni_barcode dw_transazioni_barcode
dw_selezione_transazioni_barcode dw_selezione_transazioni_barcode
end type
global w_transazioni_barcode w_transazioni_barcode

type variables
boolean ib_input_barcode = true
string is_cod_tipo_fat_ven = "TRN"			// tipo fattura per transazioni
string is_cod_tipo_det_ven = "TRN"			// tipo fattura per transazioni
string is_cod_prodotto
long il_anno_registrazione, il_num_registrazione   // memorizzano la transazione corrente

uo_condizioni_cliente iuo_condizioni_cliente
end variables

forward prototypes
public function integer wf_crea_transazione (string fs_cod_cliente, ref long fl_anno_registrazione, ref long fl_num_registrazione)
end prototypes

public function integer wf_crea_transazione (string fs_cod_cliente, ref long fl_anno_registrazione, ref long fl_num_registrazione);string ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cap, ls_localita, ls_provincia, ls_telefono, ls_fax, ls_telex, &
       ls_partita_iva, ls_cod_fiscale, ls_cod_deposito, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento,  &
		 ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca_clien_for,  ls_cod_banca, ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, &
		 ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_flag_fuori_fido, ls_flag_blocco,  ls_flag_bol_fat, ls_flag_tipo_fat_ven, &
		 ls_cod_des_cliente,ls_rag_soc_1_dest_fat,ls_cod_tipo_fat_ven_com,ls_flag_tipo_fat_ven_com, ls_flag_dest_merce_fat,&
		 ls_rag_soc_2_dest_fat,ls_frazione_dest_fat,ls_cap_dest_fat,ls_localita_dest_fat,ls_provincia_dest_fat, &
		 ls_indirizzo_dest_fat,ls_cod_causale
long ll_anno_registrazione, ll_num_registrazione,li_count
dec{4} ld_sconto
datetime ldt_data_blocco, ldt_data_registrazione
         

ldt_data_registrazione = datetime(today(),00:00:00)			
			
select rag_soc_1,
		 rag_soc_2,
		 indirizzo,
		 frazione,
		 cap,
		 localita,
		 provincia,
		 partita_iva,
		 cod_fiscale,
		 cod_deposito,
		 cod_valuta,
		 cod_tipo_listino_prodotto,
		 cod_pagamento,
		 sconto,
		 cod_agente_1,
		 cod_agente_2,
		 cod_banca_clien_for,
		 cod_banca,
		 cod_imballo,
		 cod_vettore,
		 cod_inoltro,
		 cod_mezzo,
		 cod_porto,
		 cod_resa,
		 flag_fuori_fido,
		 flag_blocco,
		 data_blocco,
		 flag_bol_fat
into   :ls_rag_soc_1,    
		 :ls_rag_soc_2,
		 :ls_indirizzo,    
		 :ls_frazione,
		 :ls_cap,
		 :ls_localita,
		 :ls_provincia,
		 :ls_partita_iva,
		 :ls_cod_fiscale,
		 :ls_cod_deposito,
		 :ls_cod_valuta,
		 :ls_cod_tipo_listino_prodotto,
		 :ls_cod_pagamento,
		 :ld_sconto,
		 :ls_cod_agente_1,
		 :ls_cod_agente_2,
		 :ls_cod_banca_clien_for,
		 :ls_cod_banca,
		 :ls_cod_imballo,
		 :ls_cod_vettore,
		 :ls_cod_inoltro,
		 :ls_cod_mezzo,
		 :ls_cod_porto,
		 :ls_cod_resa,
		 :ls_flag_fuori_fido,
		 :ls_flag_blocco,
		 :ldt_data_blocco,
		 :ls_flag_bol_fat
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_cliente = :fs_cod_cliente;

if sqlca.sqlcode = 0 then 
	if ldt_data_blocco < ldt_data_registrazione and ls_flag_blocco = "S" then
		g_mb.messagebox("Apice","Attenzione cliente bloccato!")
		return -1
	end if

	ll_anno_registrazione = f_anno_esercizio()
	ll_num_registrazione = 0 
	select max(num_registrazione)
	into :ll_num_registrazione
	from tes_fat_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
	      anno_registrazione = :ll_anno_registrazione;
	if ll_num_registrazione = 0 then
		ll_num_registrazione = 1
	else
		ll_num_registrazione++
	end if
	
	setnull(ls_cod_des_cliente)
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
	setnull(ls_indirizzo)
	setnull(ls_frazione)
	setnull(ls_cap)
	setnull(ls_localita)
	setnull(ls_provincia)
	setnull(ls_rag_soc_1_dest_fat)
	setnull(ls_rag_soc_2_dest_fat)
	setnull(ls_frazione_dest_fat)
	setnull(ls_cap_dest_fat)
	setnull(ls_localita_dest_fat)
	setnull(ls_provincia_dest_fat)
	// cerco la destinazione del cliente
	
	ls_cod_tipo_fat_ven_com = is_cod_tipo_fat_ven
	
	select flag_tipo_fat_ven, cod_causale
	into   :ls_flag_tipo_fat_ven_com, :ls_cod_causale
	from   tab_tipi_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven_com;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca tipo fattuta in tabella tab_tipi_fat_ven.~r~nDettaglio errore:"+sqlca.sqlerrtext)
		return -1
	end if			
	
	if ls_flag_tipo_fat_ven_com = "I" then
		ls_flag_dest_merce_fat = "S"
	else 
		ls_flag_dest_merce_fat = "N"
	end if	

	select count(*)
	into   :li_count
	from   anag_des_clienti
	where  anag_des_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
			 anag_des_clienti.cod_cliente = :fs_cod_cliente and
			 anag_des_clienti.flag_dest_merce_fat = :ls_flag_dest_merce_fat and
			 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_data_registrazione ));
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("APICE","Errore in ricerca destinazione merce.~r~nDettaglio errore:"+sqlca.sqlerrtext)
		return -1
	end if

	if li_count > 0 then
		if li_count > 1 then
			s_cs_xx.parametri.parametro_s_10 = fs_cod_cliente
			s_cs_xx.parametri.parametro_s_11 = ls_flag_dest_merce_fat
			s_cs_xx.parametri.parametro_s_12 = ls_flag_tipo_fat_ven_com
			window_open(w_lista_dest_cliente, 0)		
			if s_cs_xx.parametri.parametro_s_10 = "" or isnull(s_cs_xx.parametri.parametro_s_10) then
				g_mb.messagebox("APICE","Selezione della destinazione obbligatoria")
				return -1
			end if
			ls_cod_des_cliente = s_cs_xx.parametri.parametro_s_10
			setnull(s_cs_xx.parametri.parametro_s_10)
			setnull(s_cs_xx.parametri.parametro_s_11)
			setnull(s_cs_xx.parametri.parametro_s_12)
		else
			select cod_des_cliente
			into   :ls_cod_des_cliente
			from   anag_des_clienti
			where  anag_des_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_des_clienti.cod_cliente = :fs_cod_cliente and
					 anag_des_clienti.flag_dest_merce_fat = :ls_flag_dest_merce_fat and
					 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_data_registrazione ));
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("APICE","Errore in ricerca destinazioni.~r~nDettaglio errore:"+sqlca.sqlerrtext)
				return -1
			end if
			if sqlca.sqlcode = 100 then
				g_mb.messagebox("APICE","Destinazione non trovata " + ls_cod_des_cliente)
				return -1
			end if
		end if		
		if ls_flag_tipo_fat_ven_com = "I" then
			select anag_des_clienti.rag_soc_1,
					 anag_des_clienti.rag_soc_2,
					 anag_des_clienti.indirizzo,
					 anag_des_clienti.frazione,
					 anag_des_clienti.cap,
					 anag_des_clienti.localita,
					 anag_des_clienti.provincia
			into   :ls_rag_soc_1,    
					 :ls_rag_soc_2,
					 :ls_indirizzo,    
					 :ls_frazione,
					 :ls_cap,
					 :ls_localita,
					 :ls_provincia
			from   anag_des_clienti
			where  anag_des_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_des_clienti.cod_cliente = :fs_cod_cliente and
					 anag_des_clienti.cod_des_cliente = :ls_cod_des_cliente and
					 anag_des_clienti.flag_dest_merce_fat = :ls_flag_dest_merce_fat and						 
					 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_data_registrazione));
		else
			select anag_des_clienti.rag_soc_1,
					 anag_des_clienti.rag_soc_2,
					 anag_des_clienti.indirizzo,
					 anag_des_clienti.frazione,
					 anag_des_clienti.cap,
					 anag_des_clienti.localita,
					 anag_des_clienti.provincia
			into   :ls_rag_soc_1_dest_fat,    
					 :ls_rag_soc_2_dest_fat,
					 :ls_indirizzo_dest_fat,    
					 :ls_frazione_dest_fat,
					 :ls_cap_dest_fat,
					 :ls_localita_dest_fat,
					 :ls_provincia_dest_fat
			from   anag_des_clienti
			where  anag_des_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_des_clienti.cod_cliente = :fs_cod_cliente and
					 anag_des_clienti.cod_des_cliente = :ls_cod_des_cliente and
					 anag_des_clienti.flag_dest_merce_fat = :ls_flag_dest_merce_fat and						 
					 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_data_registrazione));
		end if			
		
		setnull(s_cs_xx.parametri.parametro_s_10)
		setnull(s_cs_xx.parametri.parametro_s_11)
		setnull(s_cs_xx.parametri.parametro_s_12)
	end if
	
	// fine ricerca destinazione
   INSERT INTO tes_fat_ven  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           data_registrazione,   
           cod_operatore,   
           cod_tipo_fat_ven,   
           cod_cliente,   
           cod_des_cliente,   
           rag_soc_1,   
           rag_soc_2,   
           indirizzo,   
           localita,   
           frazione,   
           cap,   
           provincia,   
           rag_soc_1_dest_fat,   
           rag_soc_2_dest_fat,   
           indirizzo_dest_fat,   
           frazione_dest_fat,   
           cap_dest_fat,   
           localita_dest_fat,   
           provincia_dest_fat,   
           cod_deposito,   
           cod_ubicazione,   
           cod_valuta,   
           cambio_ven,   
           cod_tipo_listino_prodotto,   
           cod_pagamento,   
           sconto,   
           cod_agente_1,   
           cod_agente_2,   
           cod_banca,   
           num_ord_cliente,   
           data_ord_cliente,   
           cod_imballo,   
           aspetto_beni,   
           peso_netto,   
           peso_lordo,   
           num_colli,   
           cod_vettore,   
           cod_inoltro,   
           cod_mezzo,   
           causale_trasporto,   
           cod_porto,   
           cod_resa,   
           cod_documento,   
           numeratore_documento,   
           anno_documento,   
           num_documento,   
           data_fattura,   
           nota_testata,   
           nota_piede,   
           flag_fuori_fido,   
           flag_blocco,   
           flag_calcolo,   
           flag_movimenti,   
           flag_contabilita,   
           tot_merci,   
           tot_spese_trasporto,   
           tot_spese_imballo,   
           tot_spese_bolli,   
           tot_spese_varie,   
           tot_sconto_cassa,   
           tot_sconti_commerciali,   
           imponibile_provvigioni_1,   
           imponibile_provvigioni_2,   
           tot_provvigioni_1,   
           tot_provvigioni_2,   
           importo_iva,   
           imponibile_iva,   
           importo_iva_valuta,   
           imponibile_iva_valuta,   
           tot_fattura,   
           tot_fattura_valuta,   
           flag_cambio_zero,   
           cod_banca_clien_for,   
           cod_causale,   
           data_inizio_trasporto,   
           ora_inizio_trasporto,   
           cod_fornitore,   
           cod_fil_fornitore,   
           cod_des_fornitore,   
           flag_st_note_tes,   
           flag_st_note_pie )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ll_anno_registrazione,   
           :ll_num_registrazione,   
           :ldt_data_registrazione,   
           null,   
           :is_cod_tipo_fat_ven,   
           :fs_cod_cliente,   
           :ls_cod_des_cliente,   
			  :ls_rag_soc_1,    
			  :ls_rag_soc_2,
			  :ls_indirizzo,    
			  :ls_frazione,
			  :ls_cap,
			  :ls_localita,
			  :ls_provincia,
			  :ls_rag_soc_1_dest_fat,    
			  :ls_rag_soc_2_dest_fat,
			  :ls_indirizzo_dest_fat,    
			  :ls_frazione_dest_fat,
			  :ls_cap_dest_fat,
			  :ls_localita_dest_fat,
			  :ls_provincia_dest_fat,
           :ls_cod_deposito,   
           null,   
           :ls_cod_valuta,   
           1,   
           :ls_cod_tipo_listino_prodotto,   
           :ls_cod_pagamento,   
           :ld_sconto,   
           :ls_cod_agente_1,   
           :ls_cod_agente_2,   
           :ls_cod_banca,   
           null,   
           null,   
           :ls_cod_imballo,   
           null,   
           0,   
           0,   
           0,   
           :ls_cod_vettore,   
           :ls_cod_inoltro,   
           :ls_cod_mezzo,   
           null,   
           :ls_cod_porto,   
           :ls_cod_resa,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           'N',   
           'N',   
           'N',   
           'N',   
           'N',   
           0,   
           0,   
           0,   
           0,   
			  
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           'N',   
           :ls_cod_banca_clien_for,   
           :ls_cod_causale,   
           null,   
           null,   
           null,   
           null,   
           null,   
           'N',   
           'N' )  ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in inserimento testata transazioni.Dettaglio errore:~r~n"+sqlca.sqlerrtext)
		return -1
	end if

else
	g_mb.messagebox("APICE","Errore in ricerca cliente.Dettaglio errore:~r~n"+sqlca.sqlerrtext)
	return -1
end if	
	
fl_anno_registrazione = ll_anno_registrazione
fl_num_registrazione = ll_num_registrazione
   
return 0
end function

on w_transazioni_barcode.create
int iCurrent
call super::create
this.cb_ddt=create cb_ddt
this.em_totale=create em_totale
this.em_imponibile=create em_imponibile
this.st_11=create st_11
this.st_10=create st_10
this.cb_fatt=create cb_fatt
this.cb_1=create cb_1
this.st_6=create st_6
this.st_5=create st_5
this.st_4=create st_4
this.st_3=create st_3
this.st_2=create st_2
this.st_1=create st_1
this.dw_transazioni_barcode=create dw_transazioni_barcode
this.dw_selezione_transazioni_barcode=create dw_selezione_transazioni_barcode
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ddt
this.Control[iCurrent+2]=this.em_totale
this.Control[iCurrent+3]=this.em_imponibile
this.Control[iCurrent+4]=this.st_11
this.Control[iCurrent+5]=this.st_10
this.Control[iCurrent+6]=this.cb_fatt
this.Control[iCurrent+7]=this.cb_1
this.Control[iCurrent+8]=this.st_6
this.Control[iCurrent+9]=this.st_5
this.Control[iCurrent+10]=this.st_4
this.Control[iCurrent+11]=this.st_3
this.Control[iCurrent+12]=this.st_2
this.Control[iCurrent+13]=this.st_1
this.Control[iCurrent+14]=this.dw_transazioni_barcode
this.Control[iCurrent+15]=this.dw_selezione_transazioni_barcode
end on

on w_transazioni_barcode.destroy
call super::destroy
destroy(this.cb_ddt)
destroy(this.em_totale)
destroy(this.em_imponibile)
destroy(this.st_11)
destroy(this.st_10)
destroy(this.cb_fatt)
destroy(this.cb_1)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.dw_transazioni_barcode)
destroy(this.dw_selezione_transazioni_barcode)
end on

event pc_setwindow;call super::pc_setwindow;//set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_selezione_transazioni_barcode.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)


dw_transazioni_barcode.set_dw_options(sqlca, &
                            pcca.null_object, &
									 c_viewonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer + &
									 c_ViewModeborderUnchanged)
									 
select cod_tipo_det_ven
into   :is_cod_tipo_det_ven
from   tab_tipi_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_fat_ven = :is_cod_tipo_det_ven;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Confirare il tipo dettaglio e il tipo fattura appropriati")
end if
iuo_dw_main = dw_transazioni_barcode

dw_selezione_transazioni_barcode.postevent("ue_reset")
end event

event open;call super::open;iuo_condizioni_cliente = CREATE uo_condizioni_cliente
end event

event close;call super::close;destroy iuo_condizioni_cliente
end event

type cb_ddt from commandbutton within w_transazioni_barcode
integer x = 2743
integer y = 1560
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Invia DDT."
end type

event clicked;string ls_cod_prodotto,ls_barcode,ls_cod_tipo_bol_ven, ls_cod_cliente, ls_cod_des_cliente, ls_rag_soc_1, ls_rag_soc_2,ls_indirizzo,   &
       ls_localita, ls_frazione, ls_cap, ls_provincia, ls_rag_soc_1_dest_fat, ls_rag_soc_2_dest_fat, ls_indirizzo_dest_fat, ls_frazione_dest_fat, &
		 ls_cap_dest_fat, ls_localita_dest_fat, ls_provincia_dest_fat, ls_cod_deposito, ls_cod_ubicazione, ls_cod_valuta, &
		 ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca, ls_num_ord_cliente, ls_cod_imballo, &
		 ls_aspetto_beni, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_causale_trasporto, ls_cod_porto, ls_cod_resa, ls_cod_documento, &
		 ls_numeratore_documento, ls_nota_testata, ls_nota_piede, ls_flag_fuori_fido, ls_flag_blocco, ls_flag_calcolo, ls_flag_movimenti, ls_flag_contabilita, &
		 ls_flag_cambio_zero, ls_cod_banca_clien_for, ls_cod_causale, ls_cod_fornitore, ls_cod_fil_fornitore, ls_cod_des_fornitore, ls_flag_st_note_tes, &
		 ls_flag_st_note_pie, ls_cod_lotto[], ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_cod_deposito_det[], ls_cod_ubicazione_det[], &
		 ls_cod_tipo_det_ven,ls_flag_tipo_det_ven,ls_cod_tipo_movimento,ls_null, ls_flag_bol_fat,ls_flag_riep_fat, ls_messaggio
long ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_fat_ven,ll_progr_stock[]
dec{4} ld_anno_documento, ld_num_documento, ld_tot_merci, ld_tot_spese_trasporto, ld_tot_spese_imballo, ld_tot_spese_bolli, ld_tot_spese_varie, &
       ld_tot_sconto_cassa, ld_tot_sconti_commerciali, ld_imponibile_provvigioni_1, ld_imponibile_provvigioni_2, ld_tot_provvigioni_1, ld_tot_provvigioni_2, &
		 ld_importo_iva, ld_imponibile_iva, ld_importo_iva_valuta, ld_imponibile_iva_valuta, ld_tot_fattura, ld_tot_fattura_valuta, ld_peso_netto, &
		 ld_peso_lordo, ld_num_colli, ld_cambio_ven, ld_sconto,ld_quan_fatturata,ld_prezzo_vendita,ld_sconto_1,ld_fat_conversione_ven,&
		 ld_num_confezioni,ld_num_pezzi_confezione,ld_quan_disponibile
datetime ldt_data_registrazione,ldt_data_inizio_trasporto, ldt_ora_inizio_trasporto, ldt_data_bolla, ldt_data_ord_cliente,ldt_data_stock[]
transaction sqlcb

uo_calcola_documento_euro luo_calcolo


if g_mb.messagebox("APICE","Procedo con il trasferimento del DDT nel sistema centrale?",Question!,YesNo!,2) = 2 then return

SELECT data_registrazione,   
	cod_cliente,   
	cod_des_cliente,   
	rag_soc_1,   
	rag_soc_2,   
	indirizzo,   
	localita,   
	frazione,   
	cap,   
	provincia,   
	rag_soc_1_dest_fat,   
	rag_soc_2_dest_fat,   
	indirizzo_dest_fat,   
	frazione_dest_fat,   
	cap_dest_fat,   
	localita_dest_fat,   
	provincia_dest_fat,   
	cod_deposito,   
	cod_ubicazione,   
	cod_valuta,   
	cambio_ven,   
	cod_tipo_listino_prodotto,   
	cod_pagamento,   
	sconto,   
	cod_agente_1,   
	cod_agente_2,   
	cod_banca,   
	num_ord_cliente,   
	data_ord_cliente,   
	cod_imballo,   
	aspetto_beni,   
	peso_netto,   
	peso_lordo,   
	num_colli,   
	cod_vettore,   
	cod_inoltro,   
	cod_mezzo,   
	causale_trasporto,   
	cod_porto,   
	cod_resa,   
	cod_documento,   
	numeratore_documento,   
	anno_documento,   
	num_documento,   
	data_fattura,   
	nota_testata,   
	nota_piede,   
	flag_fuori_fido,   
	flag_blocco,   
	flag_calcolo,   
	flag_movimenti,   
	flag_contabilita,   
	tot_merci,   
	tot_spese_trasporto,   
	tot_spese_imballo,   
	tot_spese_bolli,   
	tot_spese_varie,   
	tot_sconto_cassa,   
	tot_sconti_commerciali,   
	imponibile_provvigioni_1,   
	imponibile_provvigioni_2,   
	tot_provvigioni_1,   
	tot_provvigioni_2,   
	importo_iva,   
	imponibile_iva,   
	importo_iva_valuta,   
	imponibile_iva_valuta,   
	tot_fattura,   
	tot_fattura_valuta,   
	flag_cambio_zero,   
	cod_banca_clien_for,   
	cod_causale,   
	data_inizio_trasporto,   
	ora_inizio_trasporto,   
	cod_fornitore,   
	cod_fil_fornitore,   
	cod_des_fornitore,   
	flag_st_note_tes,   
	flag_st_note_pie  
INTO  :ldt_data_registrazione,   
	:ls_cod_cliente,   
	:ls_cod_des_cliente,   
	:ls_rag_soc_1,   
	:ls_rag_soc_2,   
	:ls_indirizzo,   
	:ls_localita,   
	:ls_frazione,   
	:ls_cap,   
	:ls_provincia,   
	:ls_rag_soc_1_dest_fat,   
	:ls_rag_soc_2_dest_fat,   
	:ls_indirizzo_dest_fat,   
	:ls_frazione_dest_fat,   
	:ls_cap_dest_fat,   
	:ls_localita_dest_fat,   
	:ls_provincia_dest_fat,   
	:ls_cod_deposito,   
	:ls_cod_ubicazione,   
	:ls_cod_valuta,   
	:ld_cambio_ven,   
	:ls_cod_tipo_listino_prodotto,   
	:ls_cod_pagamento,   
	:ld_sconto,   
	:ls_cod_agente_1,   
	:ls_cod_agente_2,   
	:ls_cod_banca,   
	:ls_num_ord_cliente,   
	:ldt_data_ord_cliente,   
	:ls_cod_imballo,   
	:ls_aspetto_beni,   
	:ld_peso_netto,   
	:ld_peso_lordo,   
	:ld_num_colli,   
	:ls_cod_vettore,   
	:ls_cod_inoltro,   
	:ls_cod_mezzo,   
	:ls_causale_trasporto,   
	:ls_cod_porto,   
	:ls_cod_resa,   
	:ls_cod_documento,   
	:ls_numeratore_documento,   
	:ld_anno_documento,   
	:ld_num_documento,   
	:ldt_data_bolla,   
	:ls_nota_testata,   
	:ls_nota_piede,   
	:ls_flag_fuori_fido,   
	:ls_flag_blocco,   
	:ls_flag_calcolo,   
	:ls_flag_movimenti,   
	:ls_flag_contabilita,   
	:ld_tot_merci,   
	:ld_tot_spese_trasporto,   
	:ld_tot_spese_imballo,   
	:ld_tot_spese_bolli,   
	:ld_tot_spese_varie,   
	:ld_tot_sconto_cassa,   
	:ld_tot_sconti_commerciali,   
	:ld_imponibile_provvigioni_1,   
	:ld_imponibile_provvigioni_2,   
	:ld_tot_provvigioni_1,   
	:ld_tot_provvigioni_2,   
	:ld_importo_iva,   
	:ld_imponibile_iva,   
	:ld_importo_iva_valuta,   
	:ld_imponibile_iva_valuta,   
	:ld_tot_fattura,   
	:ld_tot_fattura_valuta,   
	:ls_flag_cambio_zero,   
	:ls_cod_banca_clien_for,   
	:ls_cod_causale,   
	:ldt_data_inizio_trasporto,   
	:ldt_ora_inizio_trasporto,   
	:ls_cod_fornitore,   
	:ls_cod_fil_fornitore,   
	:ls_cod_des_fornitore,   
	:ls_flag_st_note_tes,   
	:ls_flag_st_note_pie
FROM  tes_fat_ven  
WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
		anno_registrazione = :il_anno_registrazione AND  
		num_registrazione = :il_num_registrazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Transazione non trovata !!!",Stopsign!)
	return
end if

// -------------------------- controllo se cliente per bolle o per fatture ----------------------------------

select flag_bol_fat, flag_riep_fatt
into   :ls_flag_bol_fat, :ls_flag_riep_fat
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Cliente "+ls_cod_cliente+" inesistente !!!",Stopsign!)
	return
end if

if not isnull(ls_flag_bol_fat) and ls_flag_bol_fat = "F" then
	if g_mb.messagebox("APICE","Il documento preferenziale per questo cliente è la fattura;~r~nProcedo lo stesso con il DDT ?",Question!,YesNo!,2) = 2 then return
end if
/// -------------------------  creo la conessione con il sistema centrale ------------------------------------
sqlcb = create transaction
if f_po_getconnectinfo("", "database_5", sqlcb) <> 0 then
   g_mb.messagebox("APICE","Errore durante la connessione al sistema centrale; verificare la rete")
	return
end if

if f_po_connect(sqlcb, true) <> 0 then
   g_mb.messagebox("APICE","Errore durante la connessione al sistema centrale; verificare la rete")
	return
end if

//  -------------------------  sistemo alcuni parametri necessario --------------------------------------------
ll_anno_registrazione = f_anno_esercizio()
select max(num_registrazione)
into :ll_num_registrazione
from tes_bol_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :ll_anno_registrazione
using sqlcb;
if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
	ll_num_registrazione = 1
else
	ll_num_registrazione ++
end if

select stringa
into :ls_cod_tipo_bol_ven
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_parametro = 'TDT'
using sqlcb;
if sqlcb.sqlcode <> 0 then
	g_mb.messagebox("APICE","Parametro TDT per generazione tipo fattura non trovato!!",Stopsign!)
	destroy sqlcb;
	return
end if

// ---------------------------  procedo con insert testata -------------------------------------------------------

INSERT INTO tes_bol_ven  
	( cod_azienda,   
	  anno_registrazione,   
	  num_registrazione,   
	  data_registrazione,   
	  cod_operatore,   
	  cod_tipo_bol_ven,   
	  cod_cliente,   
	  cod_des_cliente,   
	  rag_soc_1,   
	  rag_soc_2,   
	  indirizzo,   
	  localita,   
	  frazione,   
	  cap,   
	  provincia,   
	  cod_deposito,   
	  cod_ubicazione,   
	  cod_valuta,   
	  cambio_ven,   
	  cod_tipo_listino_prodotto,   
	  cod_pagamento,   
	  sconto,   
	  cod_agente_1,   
	  cod_agente_2,   
	  cod_banca,   
	  num_ord_cliente,   
	  data_ord_cliente,   
	  cod_imballo,   
	  aspetto_beni,   
	  peso_netto,   
	  peso_lordo,   
	  num_colli,   
	  cod_vettore,   
	  cod_inoltro,   
	  cod_mezzo,   
	  causale_trasporto,   
	  cod_porto,   
	  cod_resa,   
	  cod_documento,   
	  numeratore_documento,   
	  anno_documento,   
	  num_documento,   
	  data_bolla,   
	  nota_testata,   
	  nota_piede,   
	  flag_fuori_fido,   
	  flag_blocco,   
	  flag_movimenti,   
	  tot_merci,   
	  tot_spese_trasporto,   
	  tot_spese_imballo,   
	  tot_spese_bolli,   
	  tot_spese_varie,   
	  tot_sconto_cassa,   
	  tot_sconti_commerciali,   
	  imponibile_provvigioni_1,   
	  imponibile_provvigioni_2,   
	  tot_provvigioni_1,   
	  tot_provvigioni_2,   
	  importo_iva,   
	  imponibile_iva,   
	  importo_iva_valuta,   
	  imponibile_iva_valuta,   
	  tot_val_bolla,   
	  tot_val_bolla_valuta,   
	  cod_banca_clien_for,   
	  cod_causale,   
	  data_inizio_trasporto,   
	  ora_inizio_trasporto,   
	  cod_fornitore,   
	  cod_fil_fornitore,   
	  cod_des_fornitore,   
	  flag_st_note_tes,   
	  flag_st_note_pie,
	  flag_riep_fat,
	  flag_gen_fat,
	  flag_doc_suc_tes,
	  flag_doc_suc_pie)  
values (:s_cs_xx.cod_azienda,   
	  :ll_anno_registrazione,   
	  :ll_num_registrazione,   
	  :ldt_data_registrazione,   
	  null,   
	  :ls_cod_tipo_bol_ven,   
	  :ls_cod_cliente,   
	  :ls_cod_des_cliente,   
	  :ls_rag_soc_1,   
	  :ls_rag_soc_2,   
	  :ls_indirizzo,   
	  :ls_localita,   
	  :ls_frazione,   
	  :ls_cap,   
	  :ls_provincia,   
	  :ls_cod_deposito,   
	  :ls_cod_ubicazione,   
	  :ls_cod_valuta,   
	  :ld_cambio_ven,   
	  :ls_cod_tipo_listino_prodotto,   
	  :ls_cod_pagamento,   
	  :ld_sconto,   
	  :ls_cod_agente_1,   
	  :ls_cod_agente_2,   
	  :ls_cod_banca,   
	  :ls_num_ord_cliente,   
	  :ldt_data_ord_cliente,   
	  :ls_cod_imballo,   
	  :ls_aspetto_beni,   
	  :ld_peso_netto,   
	  :ld_peso_lordo,   
	  :ld_num_colli,   
	  :ls_cod_vettore,   
	  :ls_cod_inoltro,   
	  :ls_cod_mezzo,   
	  :ls_causale_trasporto,   
	  :ls_cod_porto,   
	  :ls_cod_resa,   
	  :ls_cod_documento,   
	  :ls_numeratore_documento,   
	  :ld_anno_documento,   
	  :ld_num_documento,   
	  :ldt_data_bolla,   
	  :ls_nota_testata,   
	  :ls_nota_piede,   
	  'N',   
	  :ls_flag_blocco,   
	  'N',   
	  :ld_tot_merci,   
	  :ld_tot_spese_trasporto,   
	  :ld_tot_spese_imballo,   
	  :ld_tot_spese_bolli,   
	  :ld_tot_spese_varie,   
	  :ld_tot_sconto_cassa,   
	  :ld_tot_sconti_commerciali,   
	  :ld_imponibile_provvigioni_1,   
	  :ld_imponibile_provvigioni_2,   
	  :ld_tot_provvigioni_1,   
	  :ld_tot_provvigioni_2,   
	  :ld_importo_iva,   
	  :ld_imponibile_iva,   
	  :ld_importo_iva_valuta,   
	  :ld_imponibile_iva_valuta,   
	  :ld_tot_fattura,   
	  :ld_tot_fattura_valuta,   
	  :ls_cod_banca_clien_for,   
	  :ls_cod_causale,   
	  :ldt_data_inizio_trasporto,   
	  :ldt_ora_inizio_trasporto,   
	  :ls_cod_fornitore,   
	  :ls_cod_fil_fornitore,   
	  :ls_cod_des_fornitore,   
	  :ls_flag_st_note_tes,   
	  :ls_flag_st_note_pie,
	  :ls_flag_riep_fat,
	  'N',
	  'I',
	  'I')
using sqlcb;
if sqlcb.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in inserimento fattura nel sistema centrale.~r~n" + sqlcb.sqlerrtext)
	rollback using sqlcb;
	destroy sqlcb;
	return
end if

DECLARE cu_det_fat_ven CURSOR FOR  
SELECT prog_riga_fat_ven,
      cod_tipo_det_ven,
		cod_prodotto,   
		cod_deposito,   
		cod_ubicazione,   
		cod_lotto,   
		data_stock,   
		progr_stock,   
		cod_misura,   
		des_prodotto,   
		quan_fatturata,   
		prezzo_vendita,   
		sconto_1,   
		cod_iva,   
		fat_conversione_ven,   
		num_confezioni,   
		num_pezzi_confezione  
 FROM det_fat_ven  
WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
		anno_registrazione = :il_anno_registrazione AND  
		num_registrazione = :il_num_registrazione   
ORDER BY prog_riga_fat_ven ASC;

open cu_det_fat_ven;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in open cursore cu_det_fat_ven.~r~n" + sqlcb.sqlerrtext)
	rollback;
	rollback using sqlcb;
	destroy sqlcb;
	return
end if

do while true
	fetch cu_det_fat_ven into :ll_prog_riga_fat_ven, :ls_cod_tipo_det_ven, :ls_cod_prodotto, :ls_cod_deposito_det[1], :ls_cod_ubicazione_det[1], :ls_cod_lotto[1], :ldt_data_stock[1], :ll_progr_stock[1], :ls_cod_misura, :ls_des_prodotto, :ld_quan_fatturata, :ld_prezzo_vendita, :ld_sconto_1, :ls_cod_iva, :ld_fat_conversione_ven, :ld_num_confezioni, :ld_num_pezzi_confezione;
	if sqlca.sqlcode = -1 then 
		g_mb.messagebox("APICE","Errore in fetch cursore cu_det_fat_ven.~r~n" + sqlcb.sqlerrtext)
		rollback;
		rollback using sqlcb;
		destroy sqlcb;
		return
	end if
	if sqlca.sqlcode = 100 then  exit
	
	INSERT INTO det_bol_ven  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           prog_riga_bol_ven,   
           cod_prodotto,   
           cod_tipo_det_ven,   
           cod_deposito,   
           cod_ubicazione,   
           cod_lotto,   
           data_stock,   
           progr_stock,   
           cod_misura,   
           des_prodotto,   
           quan_consegnata,   
           prezzo_vendita,   
           sconto_1,   
           sconto_2,   
           provvigione_1,   
           provvigione_2,   
           cod_iva,   
           cod_tipo_movimento,   
           num_registrazione_mov_mag,   
           nota_dettaglio,   
           anno_commessa,   
           num_commessa,   
           cod_centro_costo,   
           sconto_3,   
           sconto_4,   
           sconto_5,   
           sconto_6,   
           sconto_7,   
           sconto_8,   
           sconto_9,   
           sconto_10,   
           anno_registrazione_mov_mag,   
           fat_conversione_ven,   
           anno_reg_des_mov,   
           num_reg_des_mov,   
           anno_registrazione_ord_ven,   
           num_registrazione_ord_ven,   
           prog_riga_ord_ven,   
           cod_versione,   
           num_confezioni,   
           num_pezzi_confezione,   
           imponibile_iva,   
           imponibile_iva_valuta,   
           flag_st_note_det,   
           flag_doc_suc_det,   
           quantita_um,   
           prezzo_um)  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ll_anno_registrazione,   
           :ll_num_registrazione,   
           :ll_prog_riga_fat_ven,   
           :ls_cod_prodotto,   
           :ls_cod_tipo_det_ven,   
           :ls_cod_deposito_det[1],   
           :ls_cod_ubicazione_det[1],   
           :ls_cod_lotto[1],   
           :ldt_data_stock[1],   
           :ll_progr_stock[1],   
           :ls_cod_misura,   
           :ls_des_prodotto,   
           :ld_quan_fatturata,   
           :ld_prezzo_vendita,   
           :ld_sconto_1,   
           0,   
           0,   
           0,   
           :ls_cod_iva,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           null,   
           1,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           :ld_num_confezioni,   
           :ld_num_pezzi_confezione,   
           0,   
           0,   
           'I',   
           'I',   
           0,   
           0  )
		using sqlcb;
		if sqlcb.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in inserimento della riga di dettaglio "+string(ll_prog_riga_fat_ven)+".~r~n" + sqlcb.sqlerrtext)
			rollback;
			rollback using sqlcb;
			destroy sqlcb;
			return
		end if
loop

// ----------------------  cancello la transazione passata ----------------------
delete from det_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;
delete from iva_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;
delete from scad_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;
delete from cont_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;
delete from tes_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;

luo_calcolo = create uo_calcola_documento_euro

if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_ven",ls_messaggio) <> 0 then
	g_mb.messagebox("APICE",ls_messaggio)
	rollback;
	rollback using sqlcb;
	destroy luo_calcolo
	return -1
end if

destroy luo_calcolo

commit;		
commit using sqlcb;

g_mb.messagebox("APICE","Generato D.D.T. nr." + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " con successo !",information!)
dw_transazioni_barcode.reset()
setnull(ls_null)
dw_selezione_transazioni_barcode.setitem(1,"cod_cliente",ls_null)
dw_selezione_transazioni_barcode.postevent("ue_reset")

end event

type em_totale from editmask within w_transazioni_barcode
integer x = 3429
integer y = 200
integer width = 457
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "none"
boolean border = false
string mask = "###,###,###,##0.0000"
end type

type em_imponibile from editmask within w_transazioni_barcode
integer x = 3429
integer y = 120
integer width = 457
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "none"
boolean border = false
string mask = "###,###,###,##0.0000"
end type

type st_11 from statictext within w_transazioni_barcode
integer x = 2743
integer y = 200
integer width = 663
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Totale Fattura:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_10 from statictext within w_transazioni_barcode
integer x = 2743
integer y = 120
integer width = 663
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Imponibile:"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_fatt from commandbutton within w_transazioni_barcode
integer x = 3154
integer y = 1560
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Invia FAT."
end type

event clicked;string ls_cod_prodotto,ls_barcode,ls_cod_tipo_fat_ven, ls_cod_cliente, ls_cod_des_cliente, ls_rag_soc_1, ls_rag_soc_2,ls_indirizzo,   &
       ls_localita, ls_frazione, ls_cap, ls_provincia, ls_rag_soc_1_dest_fat, ls_rag_soc_2_dest_fat, ls_indirizzo_dest_fat, ls_frazione_dest_fat, &
		 ls_cap_dest_fat, ls_localita_dest_fat, ls_provincia_dest_fat, ls_cod_deposito, ls_cod_ubicazione, ls_cod_valuta, &
		 ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca, ls_num_ord_cliente, ls_cod_imballo, &
		 ls_aspetto_beni, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_causale_trasporto, ls_cod_porto, ls_cod_resa, ls_cod_documento, &
		 ls_numeratore_documento, ls_nota_testata, ls_nota_piede, ls_flag_fuori_fido, ls_flag_blocco, ls_flag_calcolo, ls_flag_movimenti, ls_flag_contabilita, &
		 ls_flag_cambio_zero, ls_cod_banca_clien_for, ls_cod_causale, ls_cod_fornitore, ls_cod_fil_fornitore, ls_cod_des_fornitore, ls_flag_st_note_tes, &
		 ls_flag_st_note_pie, ls_cod_lotto[], ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_cod_deposito_det[], ls_cod_ubicazione_det[], &
		 ls_cod_tipo_det_ven,ls_flag_tipo_det_ven,ls_cod_tipo_movimento,ls_null, ls_flag_bol_fat, ls_messaggio
long ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_fat_ven,ll_progr_stock[]
dec{4} ld_anno_documento, ld_num_documento, ld_tot_merci, ld_tot_spese_trasporto, ld_tot_spese_imballo, ld_tot_spese_bolli, ld_tot_spese_varie, &
       ld_tot_sconto_cassa, ld_tot_sconti_commerciali, ld_imponibile_provvigioni_1, ld_imponibile_provvigioni_2, ld_tot_provvigioni_1, ld_tot_provvigioni_2, &
		 ld_importo_iva, ld_imponibile_iva, ld_importo_iva_valuta, ld_imponibile_iva_valuta, ld_tot_fattura, ld_tot_fattura_valuta, ld_peso_netto, &
		 ld_peso_lordo, ld_num_colli, ld_cambio_ven, ld_sconto,ld_quan_fatturata,ld_prezzo_vendita,ld_sconto_1,ld_fat_conversione_ven,&
		 ld_num_confezioni,ld_num_pezzi_confezione,ld_quan_disponibile
datetime ldt_data_registrazione,ldt_data_inizio_trasporto, ldt_ora_inizio_trasporto, ldt_data_fattura, ldt_data_ord_cliente,ldt_data_stock[]
transaction sqlcb

uo_calcola_documento_euro luo_calcolo


if g_mb.messagebox("APICE","Procedo con il trasferimento della fattura di vendita nel sistema centrale?",Question!,YesNo!,2) = 2 then return

SELECT data_registrazione,   
	cod_tipo_fat_ven,   
	cod_cliente,   
	cod_des_cliente,   
	rag_soc_1,   
	rag_soc_2,   
	indirizzo,   
	localita,   
	frazione,   
	cap,   
	provincia,   
	rag_soc_1_dest_fat,   
	rag_soc_2_dest_fat,   
	indirizzo_dest_fat,   
	frazione_dest_fat,   
	cap_dest_fat,   
	localita_dest_fat,   
	provincia_dest_fat,   
	cod_deposito,   
	cod_ubicazione,   
	cod_valuta,   
	cambio_ven,   
	cod_tipo_listino_prodotto,   
	cod_pagamento,   
	sconto,   
	cod_agente_1,   
	cod_agente_2,   
	cod_banca,   
	num_ord_cliente,   
	data_ord_cliente,   
	cod_imballo,   
	aspetto_beni,   
	peso_netto,   
	peso_lordo,   
	num_colli,   
	cod_vettore,   
	cod_inoltro,   
	cod_mezzo,   
	causale_trasporto,   
	cod_porto,   
	cod_resa,   
	cod_documento,   
	numeratore_documento,   
	anno_documento,   
	num_documento,   
	data_fattura,   
	nota_testata,   
	nota_piede,   
	flag_fuori_fido,   
	flag_blocco,   
	flag_calcolo,   
	flag_movimenti,   
	flag_contabilita,   
	tot_merci,   
	tot_spese_trasporto,   
	tot_spese_imballo,   
	tot_spese_bolli,   
	tot_spese_varie,   
	tot_sconto_cassa,   
	tot_sconti_commerciali,   
	imponibile_provvigioni_1,   
	imponibile_provvigioni_2,   
	tot_provvigioni_1,   
	tot_provvigioni_2,   
	importo_iva,   
	imponibile_iva,   
	importo_iva_valuta,   
	imponibile_iva_valuta,   
	tot_fattura,   
	tot_fattura_valuta,   
	flag_cambio_zero,   
	cod_banca_clien_for,   
	cod_causale,   
	data_inizio_trasporto,   
	ora_inizio_trasporto,   
	cod_fornitore,   
	cod_fil_fornitore,   
	cod_des_fornitore,   
	flag_st_note_tes,   
	flag_st_note_pie  
INTO  :ldt_data_registrazione,   
	:ls_cod_tipo_fat_ven,   
	:ls_cod_cliente,   
	:ls_cod_des_cliente,   
	:ls_rag_soc_1,   
	:ls_rag_soc_2,   
	:ls_indirizzo,   
	:ls_localita,   
	:ls_frazione,   
	:ls_cap,   
	:ls_provincia,   
	:ls_rag_soc_1_dest_fat,   
	:ls_rag_soc_2_dest_fat,   
	:ls_indirizzo_dest_fat,   
	:ls_frazione_dest_fat,   
	:ls_cap_dest_fat,   
	:ls_localita_dest_fat,   
	:ls_provincia_dest_fat,   
	:ls_cod_deposito,   
	:ls_cod_ubicazione,   
	:ls_cod_valuta,   
	:ld_cambio_ven,   
	:ls_cod_tipo_listino_prodotto,   
	:ls_cod_pagamento,   
	:ld_sconto,   
	:ls_cod_agente_1,   
	:ls_cod_agente_2,   
	:ls_cod_banca,   
	:ls_num_ord_cliente,   
	:ldt_data_ord_cliente,   
	:ls_cod_imballo,   
	:ls_aspetto_beni,   
	:ld_peso_netto,   
	:ld_peso_lordo,   
	:ld_num_colli,   
	:ls_cod_vettore,   
	:ls_cod_inoltro,   
	:ls_cod_mezzo,   
	:ls_causale_trasporto,   
	:ls_cod_porto,   
	:ls_cod_resa,   
	:ls_cod_documento,   
	:ls_numeratore_documento,   
	:ld_anno_documento,   
	:ld_num_documento,   
	:ldt_data_fattura,   
	:ls_nota_testata,   
	:ls_nota_piede,   
	:ls_flag_fuori_fido,   
	:ls_flag_blocco,   
	:ls_flag_calcolo,   
	:ls_flag_movimenti,   
	:ls_flag_contabilita,   
	:ld_tot_merci,   
	:ld_tot_spese_trasporto,   
	:ld_tot_spese_imballo,   
	:ld_tot_spese_bolli,   
	:ld_tot_spese_varie,   
	:ld_tot_sconto_cassa,   
	:ld_tot_sconti_commerciali,   
	:ld_imponibile_provvigioni_1,   
	:ld_imponibile_provvigioni_2,   
	:ld_tot_provvigioni_1,   
	:ld_tot_provvigioni_2,   
	:ld_importo_iva,   
	:ld_imponibile_iva,   
	:ld_importo_iva_valuta,   
	:ld_imponibile_iva_valuta,   
	:ld_tot_fattura,   
	:ld_tot_fattura_valuta,   
	:ls_flag_cambio_zero,   
	:ls_cod_banca_clien_for,   
	:ls_cod_causale,   
	:ldt_data_inizio_trasporto,   
	:ldt_ora_inizio_trasporto,   
	:ls_cod_fornitore,   
	:ls_cod_fil_fornitore,   
	:ls_cod_des_fornitore,   
	:ls_flag_st_note_tes,   
	:ls_flag_st_note_pie
FROM  tes_fat_ven  
WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
		anno_registrazione = :il_anno_registrazione AND  
		num_registrazione = :il_num_registrazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Transazione non trovata !!!",Stopsign!)
	return
end if

// -------------------------- controllo se cliente per bolle o per fatture ----------------------------------

select flag_bol_fat
into   :ls_flag_bol_fat
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Cliente "+ls_cod_cliente+" inesistente !!!",Stopsign!)
	return
end if

if not isnull(ls_flag_bol_fat) and ls_flag_bol_fat = "B" then
	if g_mb.messagebox("APICE","Il documento preferenziale per questo cliente è la bolla;~r~nProcedo lo stesso con la fattura?",Question!,YesNo!,2) = 2 then return
end if

/// -------------------------  creo la conessione con il sistema centrale ------------------------------------
sqlcb = create transaction
if f_po_getconnectinfo("", "database_5", sqlcb) <> 0 then
   g_mb.messagebox("APICE","Errore durante la connessione al sistema centrale; verificare la rete")
	return
end if

if f_po_connect(sqlcb, true) <> 0 then
   g_mb.messagebox("APICE","Errore durante la connessione al sistema centrale; verificare la rete")
	return
end if

//  -------------------------  sistemo alcuni parametri necessario --------------------------------------------
ll_anno_registrazione = f_anno_esercizio()
select max(num_registrazione)
into :ll_num_registrazione
from tes_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :ll_anno_registrazione
using sqlcb;
if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then
	ll_num_registrazione = 1
else
	ll_num_registrazione ++
end if

select stringa
into :ls_cod_tipo_fat_ven
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_parametro = 'TFT'
using sqlcb;
if sqlcb.sqlcode <> 0 then
	g_mb.messagebox("APICE","Parametro TFT per generazione tipo fattura non trovato!!",Stopsign!)
	destroy sqlcb;
	return
end if

// ---------------------------  procedo con insert testata -------------------------------------------------------

INSERT INTO tes_fat_ven  
	( cod_azienda,   
	  anno_registrazione,   
	  num_registrazione,   
	  data_registrazione,   
	  cod_operatore,   
	  cod_tipo_fat_ven,   
	  cod_cliente,   
	  cod_des_cliente,   
	  rag_soc_1,   
	  rag_soc_2,   
	  indirizzo,   
	  localita,   
	  frazione,   
	  cap,   
	  provincia,   
	  rag_soc_1_dest_fat,   
	  rag_soc_2_dest_fat,   
	  indirizzo_dest_fat,   
	  frazione_dest_fat,   
	  cap_dest_fat,   
	  localita_dest_fat,   
	  provincia_dest_fat,   
	  cod_deposito,   
	  cod_ubicazione,   
	  cod_valuta,   
	  cambio_ven,   
	  cod_tipo_listino_prodotto,   
	  cod_pagamento,   
	  sconto,   
	  cod_agente_1,   
	  cod_agente_2,   
	  cod_banca,   
	  num_ord_cliente,   
	  data_ord_cliente,   
	  cod_imballo,   
	  aspetto_beni,   
	  peso_netto,   
	  peso_lordo,   
	  num_colli,   
	  cod_vettore,   
	  cod_inoltro,   
	  cod_mezzo,   
	  causale_trasporto,   
	  cod_porto,   
	  cod_resa,   
	  cod_documento,   
	  numeratore_documento,   
	  anno_documento,   
	  num_documento,   
	  data_fattura,   
	  nota_testata,   
	  nota_piede,   
	  flag_fuori_fido,   
	  flag_blocco,   
	  flag_calcolo,   
	  flag_movimenti,   
	  flag_contabilita,   
	  tot_merci,   
	  tot_spese_trasporto,   
	  tot_spese_imballo,   
	  tot_spese_bolli,   
	  tot_spese_varie,   
	  tot_sconto_cassa,   
	  tot_sconti_commerciali,   
	  imponibile_provvigioni_1,   
	  imponibile_provvigioni_2,   
	  tot_provvigioni_1,   
	  tot_provvigioni_2,   
	  importo_iva,   
	  imponibile_iva,   
	  importo_iva_valuta,   
	  imponibile_iva_valuta,   
	  tot_fattura,   
	  tot_fattura_valuta,   
	  flag_cambio_zero,   
	  cod_banca_clien_for,   
	  cod_causale,   
	  data_inizio_trasporto,   
	  ora_inizio_trasporto,   
	  cod_fornitore,   
	  cod_fil_fornitore,   
	  cod_des_fornitore,   
	  flag_st_note_tes,   
	  flag_st_note_pie )  
values (:s_cs_xx.cod_azienda,   
	  :ll_anno_registrazione,   
	  :ll_num_registrazione,   
	  :ldt_data_registrazione,   
	  null,   
	  :ls_cod_tipo_fat_ven,   
	  :ls_cod_cliente,   
	  :ls_cod_des_cliente,   
	  :ls_rag_soc_1,   
	  :ls_rag_soc_2,   
	  :ls_indirizzo,   
	  :ls_localita,   
	  :ls_frazione,   
	  :ls_cap,   
	  :ls_provincia,   
	  :ls_rag_soc_1_dest_fat,   
	  :ls_rag_soc_2_dest_fat,   
	  :ls_indirizzo_dest_fat,   
	  :ls_frazione_dest_fat,   
	  :ls_cap_dest_fat,   
	  :ls_localita_dest_fat,   
	  :ls_provincia_dest_fat,   
	  :ls_cod_deposito,   
	  :ls_cod_ubicazione,   
	  :ls_cod_valuta,   
	  :ld_cambio_ven,   
	  :ls_cod_tipo_listino_prodotto,   
	  :ls_cod_pagamento,   
	  :ld_sconto,   
	  :ls_cod_agente_1,   
	  :ls_cod_agente_2,   
	  :ls_cod_banca,   
	  :ls_num_ord_cliente,   
	  :ldt_data_ord_cliente,   
	  :ls_cod_imballo,   
	  :ls_aspetto_beni,   
	  :ld_peso_netto,   
	  :ld_peso_lordo,   
	  :ld_num_colli,   
	  :ls_cod_vettore,   
	  :ls_cod_inoltro,   
	  :ls_cod_mezzo,   
	  :ls_causale_trasporto,   
	  :ls_cod_porto,   
	  :ls_cod_resa,   
	  :ls_cod_documento,   
	  :ls_numeratore_documento,   
	  :ld_anno_documento,   
	  :ld_num_documento,   
	  :ldt_data_fattura,   
	  :ls_nota_testata,   
	  :ls_nota_piede,   
	  :ls_flag_fuori_fido,   
	  :ls_flag_blocco,   
	  :ls_flag_calcolo,   
	  :ls_flag_movimenti,   
	  :ls_flag_contabilita,   
	  :ld_tot_merci,   
	  :ld_tot_spese_trasporto,   
	  :ld_tot_spese_imballo,   
	  :ld_tot_spese_bolli,   
	  :ld_tot_spese_varie,   
	  :ld_tot_sconto_cassa,   
	  :ld_tot_sconti_commerciali,   
	  :ld_imponibile_provvigioni_1,   
	  :ld_imponibile_provvigioni_2,   
	  :ld_tot_provvigioni_1,   
	  :ld_tot_provvigioni_2,   
	  :ld_importo_iva,   
	  :ld_imponibile_iva,   
	  :ld_importo_iva_valuta,   
	  :ld_imponibile_iva_valuta,   
	  :ld_tot_fattura,   
	  :ld_tot_fattura_valuta,   
	  :ls_flag_cambio_zero,   
	  :ls_cod_banca_clien_for,   
	  :ls_cod_causale,   
	  :ldt_data_inizio_trasporto,   
	  :ldt_ora_inizio_trasporto,   
	  :ls_cod_fornitore,   
	  :ls_cod_fil_fornitore,   
	  :ls_cod_des_fornitore,   
	  :ls_flag_st_note_tes,   
	  :ls_flag_st_note_pie )
using sqlcb;
if sqlcb.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in inserimento fattura nel sistema centrale.~r~n" + sqlcb.sqlerrtext)
	rollback using sqlcb;
	destroy sqlcb;
	return
end if

DECLARE cu_det_fat_ven CURSOR FOR  
SELECT prog_riga_fat_ven,
      cod_tipo_det_ven,
		cod_prodotto,   
		cod_deposito,   
		cod_ubicazione,   
		cod_lotto,   
		data_stock,   
		progr_stock,   
		cod_misura,   
		des_prodotto,   
		quan_fatturata,   
		prezzo_vendita,   
		sconto_1,   
		cod_iva,   
		fat_conversione_ven,   
		num_confezioni,   
		num_pezzi_confezione  
 FROM det_fat_ven  
WHERE cod_azienda = :s_cs_xx.cod_azienda AND  
		anno_registrazione = :il_anno_registrazione AND  
		num_registrazione = :il_num_registrazione   
ORDER BY prog_riga_fat_ven ASC;

open cu_det_fat_ven;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in open cursore cu_det_fat_ven.~r~n" + sqlcb.sqlerrtext)
	rollback;
	rollback using sqlcb;
	destroy sqlcb;
	return
end if

do while true
	fetch cu_det_fat_ven into :ll_prog_riga_fat_ven, :ls_cod_tipo_det_ven, :ls_cod_prodotto, :ls_cod_deposito_det[1], :ls_cod_ubicazione_det[1], :ls_cod_lotto[1], :ldt_data_stock[1], :ll_progr_stock[1], :ls_cod_misura, :ls_des_prodotto, :ld_quan_fatturata, :ld_prezzo_vendita, :ld_sconto_1, :ls_cod_iva, :ld_fat_conversione_ven, :ld_num_confezioni, :ld_num_pezzi_confezione;
	if sqlca.sqlcode = -1 then 
		g_mb.messagebox("APICE","Errore in fetch cursore cu_det_fat_ven.~r~n" + sqlcb.sqlerrtext)
		rollback;
		rollback using sqlcb;
		destroy sqlcb;
		return
	end if
	if sqlca.sqlcode = 100 then  exit
	
  INSERT INTO det_fat_ven  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           prog_riga_fat_ven,   
           cod_prodotto,   
           cod_tipo_det_ven,   
           cod_deposito,   
           cod_ubicazione,   
           cod_lotto,   
           data_stock,   
           progr_stock,   
           cod_misura,   
           des_prodotto,   
           quan_fatturata,   
           prezzo_vendita,   
           sconto_1,   
           sconto_2,   
           provvigione_1,   
           provvigione_2,   
           cod_iva,   
           cod_tipo_movimento,   
           num_registrazione_mov_mag,   
           anno_registrazione_fat,   
           num_registrazione_fat,   
           nota_dettaglio,   
           anno_registrazione_bol_ven,   
           num_registrazione_bol_ven,   
           prog_riga_bol_ven,   
           anno_commessa,   
           num_commessa,   
           cod_centro_costo,   
           sconto_3,   
           sconto_4,   
           sconto_5,   
           sconto_6,   
           sconto_7,   
           sconto_8,   
           sconto_9,   
           sconto_10,   
           anno_registrazione_mov_mag,   
           fat_conversione_ven,   
           anno_reg_des_mov,   
           num_reg_des_mov,   
           anno_reg_ord_ven,   
           num_reg_ord_ven,   
           prog_riga_ord_ven,   
           cod_versione,   
           num_confezioni,   
           num_pezzi_confezione,   
           imponibile_iva,   
           imponibile_iva_valuta,   
           flag_st_note_det,   
           quantita_um,   
           prezzo_um,   
           prod_mancante )  
  VALUES ( :s_cs_xx.cod_azienda,   
           :ll_anno_registrazione,   
           :ll_num_registrazione,   
           :ll_prog_riga_fat_ven,   
           :ls_cod_prodotto,   
           :ls_cod_tipo_det_ven,   
           :ls_cod_deposito_det[1],   
           :ls_cod_ubicazione_det[1],   
           :ls_cod_lotto[1],   
           :ldt_data_stock[1],   
           :ll_progr_stock[1],   
           :ls_cod_misura,   
           :ls_des_prodotto,   
           :ld_quan_fatturata,   
           :ld_prezzo_vendita,   
           :ld_sconto_1,   
           0,   
           0,   
           0,   
           :ls_cod_iva,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           null,   
           1,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           :ld_num_confezioni,   
           :ld_num_pezzi_confezione,   
           0,   
           0,   
           'I',   
           0,   
           0,   
           'N')
		using sqlcb;
		if sqlcb.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in inserimento della riga di dettaglio "+string(ll_prog_riga_fat_ven)+".~r~n" + sqlcb.sqlerrtext)
			rollback;
			rollback using sqlcb;
			destroy sqlcb;
			return
		end if
loop

// ----------------------  cancello la transazione passata ----------------------
delete from det_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;
delete from iva_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;
delete from scad_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;
delete from cont_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;
delete from tes_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and
      anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;


luo_calcolo = create uo_calcola_documento_euro

if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"fat_ven",ls_messaggio) <> 0 then
	g_mb.messagebox("APICE",ls_messaggio)
	rollback;
	rollback using sqlcb;
	destroy luo_calcolo
	return -1
end if

commit;		
commit using sqlcb;

destroy luo_calcolo

g_mb.messagebox("APICE","Generato fattura nr." + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " con successo !",information!)
dw_transazioni_barcode.reset()
setnull(ls_null)
dw_selezione_transazioni_barcode.setitem(1,"cod_cliente",ls_null)
dw_selezione_transazioni_barcode.postevent("ue_reset")

end event

type cb_1 from commandbutton within w_transazioni_barcode
integer x = 3566
integer y = 1560
integer width = 389
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Calcola"
end type

event clicked;string ls_messaggio, ls_mezzi_flag_calcolo_trasporto, ls_flag_calcolo_trasporto, ls_cod_mezzo, ls_cod_tipo_det_ven_trasporto, &
       ls_cod_pagamento, ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven, ls_cod_valuta,ls_cod_lire,ls_cod_valuta_corrente
long   ll_i
dec{4} ld_tot_fattura, ld_imponibile_iva

uo_calcola_documento_euro luo_calcola_documento_euro


select cod_valuta,
       cod_pagamento,
		 cod_mezzo,
		 cod_tipo_fat_ven
into   :ls_cod_valuta,
       :ls_cod_pagamento,
       :ls_cod_mezzo,
       :ls_cod_tipo_fat_ven
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Transazione non trovata",Stopsign!)
	return
end if

luo_calcola_documento_euro = create uo_calcola_documento_euro

if luo_calcola_documento_euro.uof_calcola_documento(il_anno_registrazione,il_num_registrazione,"fat_ven",ls_messaggio) <> 0 then
	g_mb.messagebox("APICE",ls_messaggio)
	destroy luo_calcola_documento_euro
	rollback;
	return -1
else
	commit;		
end if

destroy luo_calcola_documento_euro

dw_transazioni_barcode.change_dw_current()
dw_transazioni_barcode.postevent("pcd_retrieve")

select tot_fattura,
       imponibile_iva
into   :ld_tot_fattura,
       :ld_imponibile_iva
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Transazione non trovata",Stopsign!)
	return
end if

em_totale.text = string(ld_tot_fattura)
em_imponibile.text = string(ld_imponibile_iva)
end event

type st_6 from statictext within w_transazioni_barcode
integer x = 1097
integer y = 1560
integer width = 1243
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12639424
string text = "  INS = Digitazione del codice prodotto / Barcode"
boolean focusrectangle = false
end type

type st_5 from statictext within w_transazioni_barcode
integer x = 23
integer y = 1560
integer width = 997
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12639424
string text = "  SHIFT - F1  =  Ricerca per descrizione"
boolean focusrectangle = false
end type

type st_4 from statictext within w_transazioni_barcode
integer x = 3611
integer y = 40
integer width = 46
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "/"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_3 from statictext within w_transazioni_barcode
integer x = 2743
integer y = 40
integer width = 663
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Transazione corrente:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_2 from statictext within w_transazioni_barcode
integer x = 3680
integer y = 40
integer width = 274
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean focusrectangle = false
end type

type st_1 from statictext within w_transazioni_barcode
integer x = 3429
integer y = 40
integer width = 183
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_transazioni_barcode from uo_cs_xx_dw within w_transazioni_barcode
event ue_leggi_righe ( )
event ue_accetta_dato ( )
integer x = 23
integer y = 280
integer width = 3931
integer height = 1260
integer taborder = 20
string dataobject = "d_transazioni_det_1"
end type

event ue_leggi_righe();change_dw_focus(this)
parent.postevent("pc_retrieve")
end event

event ue_accetta_dato();//setitem(getrow(),"cod_prodotto","")
//accepttext()
ib_input_barcode = false
setitem(getrow(),"cod_prodotto",is_cod_prodotto)
dw_transazioni_barcode.triggerevent(itemchanged!)
//accepttext()
setcolumn("num_confezioni")

end event

event pcd_new;call super::pcd_new;if i_extendmode then
	long ll_prog_riga_fat_ven
	
	cb_1.enabled = false
	cb_ddt.enabled = false
	cb_fatt.enabled = false
	setitem(getrow(),"cod_tipo_det_ven",is_cod_tipo_det_ven)
	
	ll_prog_riga_fat_ven = 0
	select max(prog_riga_fat_ven)
	into :ll_prog_riga_fat_ven
	from det_fat_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
	      anno_registrazione = :il_anno_registrazione and
	      num_registrazione = :il_num_registrazione;
   if ll_prog_riga_fat_ven = 0 or isnull(ll_prog_riga_fat_ven) then
		ll_prog_riga_fat_ven = 10
	else
		ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10
	end if
	setitem(getrow(),"prog_riga_fat_ven", ll_prog_riga_fat_ven)
	change_dw_focus(this)
end if

end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda, il_anno_registrazione, il_num_registrazione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", il_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", il_num_registrazione)
   end if
next

end event

event itemchanged;call super::itemchanged;if i_extendmode then
   string ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_messaggio, ls_des_prodotto, &
          ls_cod_tipo_listino_prodotto, ls_cod_cliente, ls_cod_valuta, ls_colonna_sconto, &
          ls_flag_tipo_det_ven, ls_modify, ls_null, ls_cod_agente_1, ls_flag_prezzo,&
          ls_cod_agente_2, ls_cod_tipo_det_ven, ls_flag_decimali, ls_cod_versione, &
			 ls_cod_misura_mag, ls_cod_deposito, ls_cod_tipo_movimento, ls_nota_prodotto, ls_cod_fornitore, &
			 ls_cod_iva_cliente, ls_cod_iva_prodotto, ls_cod_prodotto_td,ls_flag_blocco
   dec{4} ld_fat_conversione, ld_quantita, ld_cambio_ven, ld_quan_impegnata, ld_ultimo_prezzo, ld_variazioni[], &
			 ll_sconti[], ll_maggiorazioni[], ld_min_fat_altezza,ld_min_fat_larghezza,ld_min_fat_profondita, &
			 ld_min_fat_superficie,ld_min_fat_volume, ld_quan_proposta, ld_num_pezzi_confezione, ld_num_confezioni
   datetime ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva, ldt_null
	long ll_riga_origine, ll_i, ll_null, ll_y, ll_cont

   setnull(ls_null)
   setnull(ldt_null)
   setnull(ll_null)
	
	select cod_cliente,
	       cod_tipo_listino_prodotto,
	       data_registrazione,
			 cod_valuta,
			 cambio_ven,
			 cod_agente_1,
			 cod_agente_2
	into  :ls_cod_cliente,
			:ls_cod_tipo_listino_prodotto,
			:ldt_data_registrazione,
			:ls_cod_valuta,
			:ld_cambio_ven,
			:ls_cod_agente_1,
			:ls_cod_agente_2
	from  tes_fat_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
	      anno_registrazione = :il_anno_registrazione and
			num_registrazione = :il_num_registrazione;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","(itemchanged) Transazione inesistente !~r~n" + sqlca.sqlerrtext )
		return 1
	end if
   
   ls_cod_tipo_det_ven = is_cod_tipo_det_ven
   
   choose case i_colname
      case "cod_prodotto"
			if ib_input_barcode then
				if isnull(i_coltext) or len(i_coltext) < 1 then return 1
				
				select count(*)
				into   :ll_cont
				from   anag_prodotti_barcode
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 barcode = :i_coltext;
				if isnull(ll_cont) then ll_cont = 0
				choose case ll_cont
					case 0
						beep(5)
						selecttext(1,len(gettext()))
						return 1
					// inesistente
					
					case 1
						// trovato
						select cod_prodotto
						into   :is_cod_prodotto
						from   anag_prodotti_barcode
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 barcode = :i_coltext;
						
						postevent("ue_accetta_dato")
						return 0
						
					case else
						s_cs_xx.parametri.parametro_s_1 = i_coltext
						window_open(w_lista_barcode_prodotti,0)
						if isnull(s_cs_xx.parametri.parametro_s_10) then return 1
						
						is_cod_prodotto = s_cs_xx.parametri.parametro_s_10					
						
						postevent("ue_accetta_dato")
						return 0
						
						
				end choose
			end if
			
			ib_input_barcode = true
			
         select cod_misura_mag,
					 cod_misura_ven,
                fat_conversione_ven,
                flag_decimali,
                cod_iva,
					 quan_impegnata,
					 des_prodotto,
					 pezzi_collo,
					 flag_blocco
         into   :ls_cod_misura_mag,
					 :ls_cod_misura,
                :ld_fat_conversione,
                :ls_flag_decimali,
                :ls_cod_iva,
					 :ld_quan_impegnata,
					 :ls_des_prodotto,
					 :ld_num_pezzi_confezione,
					 :ls_flag_blocco
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :i_coltext;
   
         if sqlca.sqlcode = 0 then
				if ls_flag_blocco = "S" then
					g_mb.messagebox("APICE","Prodotto fuori stagione~r~nPremere INVIO")
				end if
            this.setitem(i_rownbr, "cod_misura", ls_cod_misura)
            this.setitem(i_rownbr, "des_prodotto", ls_des_prodotto)
            this.setitem(i_rownbr, "num_pezzi_confezione", ld_num_pezzi_confezione)

            if ld_fat_conversione <> 0 then
               this.setitem(i_rownbr, "fat_conversione_ven", ld_fat_conversione)
            else
					ld_fat_conversione = 1
               this.setitem(i_rownbr, "fat_conversione_ven", 1)
            end if
            if not isnull(ls_cod_iva) then this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
				ls_cod_prodotto = i_coltext
				if f_proponi_stock (this.getitemstring(row,"cod_tipo_det_ven"), this, row, ls_messaggio ) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
				end if
				select flag_prezzo
				into   :ls_flag_prezzo
				from   tab_misure
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_misura = :ls_cod_misura;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore in ricerca unità di misura. Attenzione al calcolo della quantità totale~r~nProseguo lo stesso")
				else
					ld_num_pezzi_confezione = getitemnumber(getrow(),"num_pezzi_confezione")
					ld_num_confezioni = getitemnumber(getrow(),"num_confezioni")
					if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi_confezione > 0 then
						if ls_flag_prezzo = 'S' then
							ld_quantita = ld_num_pezzi_confezione * ld_num_confezioni
							this.setitem(i_rownbr, "quan_fatturata", ld_quantita)
						else
							ld_quantita = ld_num_confezioni
							this.setitem(i_rownbr, "quan_fatturata", ld_quantita)
						end if
					end if
				end if
						
			else
				guo_ricerca.uof_ricerca_prodotto(dw_transazioni_barcode,"cod_prodotto")
			end if
			// controllo esenzione iva			
         if not isnull(ls_cod_cliente) then
            select anag_clienti.cod_iva,
                   anag_clienti.data_esenzione_iva
            into   :ls_cod_iva,
                   :ldt_data_esenzione_iva
            from   anag_clienti
            where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
                   anag_clienti.cod_cliente = :ls_cod_cliente;
				if sqlca.sqlcode = 0 then
					if ls_cod_iva <> "" and &
						(ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
						this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
					end if
				end if
         end if

         ls_cod_prodotto = i_coltext
         ld_quantita = this.getitemnumber(i_rownbr, "quan_fatturata")
         if ls_flag_decimali = "N" and &
            ld_quantita - int(ld_quantita) > 0 then
            ld_quantita = ceiling(ld_quantita)
            this.setitem(i_rownbr, "quan_fatturata", ld_quantita)
         end if

			if mid(f_flag_controllo(),1,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			if f_leggi_nota_prodotto(ls_cod_tipo_det_ven, ls_cod_prodotto, ls_nota_prodotto) = 0 then
				this.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
				this.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if

			select cod_versione
			into   :ls_cod_versione
			from   distinta_padri
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:i_coltext
			and    flag_predefinita='S';
			if sqlca.sqlcode = 0 then 
				setitem(getrow(), "cod_versione", ls_cod_versione)
			elseif sqlca.sqlcode = 100 then
				setitem(getrow(), "cod_versione", ls_null)
			else
				g_mb.messagebox("APICE","Errore in ricerca codice versiuone distinta predefinita. Dettaglio errore=" + sqlca.sqlerrtext)
			end if
			
		case "quan_fatturata"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.flag_decimali
         into   :ls_flag_decimali
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
         if sqlca.sqlcode = 0 then
            if ls_flag_decimali = "N" and &
               (double(i_coltext) - int(double(i_coltext))) > 0 then
               i_coltext = string(ceiling(double(i_coltext)))
               this.setitem(i_rownbr, "quan_fatturata", double(i_coltext))
               this.settext(i_coltext)
               return 2
            end if
         end if

			if mid(f_flag_controllo(),2,1) = "S" then
				ld_quantita = double(i_coltext)
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if			
		case "sconto_1", "sconto_2", "sconto_3", "sconto_4", "sconto_5", "sconto_6", "sconto_7", "sconto_8", "sconto_9", "sconto_10"
			if mid(f_flag_controllo(),9,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.prezzo_listino = getitemnumber(getrow(),"prezzo_vendita")
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_fatturata")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
		case "prezzo_vendita"
			if mid(f_flag_controllo(),3,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.prezzo_listino = double(i_coltext)
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_fatturata")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
		case "num_confezioni"
         ls_cod_prodotto = getitemstring(getrow(), "cod_prodotto")
			ld_quantita = getitemnumber(getrow(),"quan_fatturata")
			ld_num_confezioni = double(i_coltext)
			ld_num_pezzi_confezione  = getitemnumber(getrow(),"num_pezzi_confezione")
			ls_cod_misura = getitemstring(getrow(),"cod_misura")
			select flag_prezzo
			into   :ls_flag_prezzo
			from   tab_misure
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_misura = :ls_cod_misura;
			if sqlca.sqlcode = 0 then
				if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi_confezione > 0 then
					if ls_flag_prezzo = 'S' then
						ld_quantita = ld_num_pezzi_confezione * ld_num_confezioni
						setitem(getrow(), "quan_fatturata", ld_quantita)
					else
						ld_quantita = ld_num_confezioni
						setitem(getrow(), "quan_fatturata", ld_quantita)
					end if
				else
					if isnull(ld_num_confezioni) or ld_num_confezioni = 0 then return 1
				end if
			else
				g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
				return 1
			end if		
         
			if mid(f_flag_controllo(),1,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			
		case "num_pezzi_confezione"
         ls_cod_prodotto = getitemstring(getrow(), "cod_prodotto")
			ld_quantita = getitemnumber(getrow(),"quan_fatturata")
			ld_num_pezzi_confezione = double(i_coltext)
			ld_num_confezioni = getitemnumber(getrow(),"num_confezioni")
			ls_cod_misura = getitemstring(getrow(),"cod_misura")
			select flag_prezzo
			into   :ls_flag_prezzo
			from   tab_misure
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_misura = :ls_cod_misura;
			if sqlca.sqlcode = 0 then
				if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi_confezione > 0 then
					if ls_flag_prezzo = 'S' then
						ld_quantita = ld_num_pezzi_confezione * ld_num_confezioni
						this.setitem(i_rownbr, "quan_fatturata", ld_quantita)
					else
						ld_quantita = ld_num_confezioni
						this.setitem(i_rownbr, "quan_fatturata", ld_quantita)
					end if
				else
					if isnull(ld_num_confezioni) or ld_num_confezioni = 0 then return 1
				end if
			else
				g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
				return 1
			end if
			
         if mid(f_flag_controllo(),1,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			
   end choose

end if
end event

event ue_key;call super::ue_key;string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, ls_listino, &
       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_cod_tipo_listino_prodotto, ls_messaggio
long ll_i, ll_y
double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo, ll_sconti[], ll_maggiorazioni[], &
	    ld_min_fat_altezza,ld_min_fat_larghezza,ld_min_fat_profondita,ld_min_fat_superficie,ld_min_fat_volume
datetime ldt_data_registrazione


choose case this.getcolumnname()

	case "sconto_1"
		if key = keyenter! then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		end if
		if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		end if
				
	case "prezzo_vendita"
//		if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
//			this.triggerevent("pcd_save")
//			this.postevent("pcd_new")
//		end if				
//		if key = keyenter! then
//			this.triggerevent("pcd_save")
//			this.postevent("pcd_new")
//		else
//			if (key = keyF1! or key = keyF2! or key = keyF3! or key = keyF4! or key = keyF5!) and keyflags = 1 then
			if key = keyF1! or key = keyF2! or key = keyF3! or key = keyF4! or key = keyF5! then
				setpointer(hourglass!)
				select cod_cliente,
						 cod_tipo_listino_prodotto,
						 data_registrazione,
						 cod_valuta,
						 cambio_ven,
						 cod_agente_1,
						 cod_agente_2
				into  :ls_cod_cliente,
						:ls_cod_tipo_listino_prodotto,
						:ldt_data_registrazione,
						:ls_cod_valuta,
						:ld_cambio_ven,
						:ls_cod_agente_1,
						:ls_cod_agente_2
				from  tes_fat_ven
				where cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :il_anno_registrazione and
						num_registrazione = :il_num_registrazione;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","(itemchanged) Transazione inesistente !~r~n" + sqlca.sqlerrtext )
					return
				end if
				
				ls_cod_tipo_det_ven = is_cod_tipo_det_ven
				ls_cod_prodotto = getitemstring(getrow(), "cod_prodotto")
				ld_quantita = getitemnumber(getrow(), "quan_fatturata")
				choose case key
					case keyF1!
						ls_listino = 'LI1'
					case keyF2!
						ls_listino = 'LI2'
					case keyF3!
						ls_listino = 'LI3'
					case keyF4!
						ls_listino = 'LI4'
					case keyF5!
						ls_listino = 'LI5'
					case else
						return
				end choose
				if ls_listino <> "LI5" then
					select stringa
					into  :ls_stringa
					from  parametri_azienda
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_parametro = :ls_listino ;
					if sqlca.sqlcode = 0 and mid(f_flag_controllo(),1,1) = "S" then
						iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
						iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_stringa
						iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
						iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
						iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
						iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
						iuo_condizioni_cliente.str_parametri.dim_1 = 0
						iuo_condizioni_cliente.str_parametri.dim_2 = 0
						iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
						iuo_condizioni_cliente.str_parametri.valore = 0
						iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
						iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
						iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
						iuo_condizioni_cliente.wf_condizioni_cliente()
					end if			
					setpointer(arrow!)
				else
					select prezzo_acquisto
					into   :ld_prezzo_acquisto
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
					       cod_prodotto = :ls_cod_prodotto;
					setitem(getrow(), "prezzo_vendita", ld_prezzo_acquisto)
				end if					 
				setpointer(arrow!)
			end if
//		end if
	case "cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_transazioni_barcode,"cod_prodotto")
		end if
		if key = KeyInsert! then
			if ib_input_barcode then
				ib_input_barcode = false
			else
				ib_input_barcode = true
			end if
		end if
	case "des_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_transazioni_barcode,"cod_prodotto")
		end if
end choose


end event

event updateend;call super::updateend;//dw_selezione_transazioni_barcode.postevent("ue_barcode")
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	cb_1.enabled = false
	cb_ddt.enabled = false
	cb_fatt.enabled = false
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	cb_1.enabled = true
	cb_ddt.enabled = true
	cb_fatt.enabled = true
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
	if isnull(getitemstring(getrow(),"cod_prodotto")) then
		g_mb.messagebox("APICE","Codice Prodotto Obbligatorio!",StopSign!)
		return 1
	end if
end if
end event

type dw_selezione_transazioni_barcode from uo_cs_xx_dw within w_transazioni_barcode
event ue_reset ( )
integer x = 23
integer y = 20
integer width = 2651
integer height = 240
integer taborder = 10
string dataobject = "d_selezione_transazioni_barcode"
end type

event ue_reset();reset_dw_modified(c_NoResetChildren)
end event

event itemchanged;call super::itemchanged;long ll_i, ll_cont
dec{4} ld_tot_fattura, ld_imponibile_iva

if i_extendmode then
	em_totale.text = ""
	em_imponibile.text = ""
	st_1.text = ""
	st_2.text = ""
	
	choose case i_colname
		case "cod_cliente"
			if isnull(i_coltext) then
				dw_transazioni_barcode.reset()
				return
			end if
			select count(*)
			into   :ll_i
			from   tes_fat_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cliente = :i_coltext and
					 cod_tipo_fat_ven = :is_cod_tipo_fat_ven;
			if ll_i > 0 then
				select anno_registrazione, num_registrazione
				into   :il_anno_registrazione, :il_num_registrazione
				from   tes_fat_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_cliente = :i_coltext and
						 cod_tipo_fat_ven = :is_cod_tipo_fat_ven;
				if sqlca.sqlcode = 0 then
					this.resetupdate()
					change_dw_focus(dw_transazioni_barcode)
					dw_transazioni_barcode.postevent("ue_leggi_righe")
				else
					g_mb.messagebox("APICE","Errore in ricerca transazione.~r~n" + sqlca.sqlerrtext)
					return 1
				end if
//				wf_leggi_transazione(i_coltext)
			else
				dw_transazioni_barcode.reset()
				if g_mb.messagebox("APICE","Per questo cliente non esiste alcuna transazione.~r~nLa Creo?",Question!,YesNo!,1) = 2 then return 1
				if wf_crea_transazione(i_coltext, ref il_anno_registrazione, ref il_num_registrazione) = 0  then
					commit;
				else
					rollback;
				end if
			end if
			
		case "barcode"
				
	
	end choose
	select tot_fattura,
			 imponibile_iva
	into   :ld_tot_fattura,
			 :ld_imponibile_iva
	from   tes_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Transazione non trovata",Stopsign!)
		return
	end if
	em_totale.text = string(ld_tot_fattura)
	em_imponibile.text = string(ld_imponibile_iva)
	st_1.text = string(il_anno_registrazione)
	st_2.text = string(il_num_registrazione)
end if
postevent("ue_reset")

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione_transazioni_barcode,"cod_cliente")
end choose
end event

