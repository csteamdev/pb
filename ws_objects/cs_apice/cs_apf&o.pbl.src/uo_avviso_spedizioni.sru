﻿$PBExportHeader$uo_avviso_spedizioni.sru
forward
global type uo_avviso_spedizioni from nonvisualobject
end type
end forward

global type uo_avviso_spedizioni from nonvisualobject
end type
global uo_avviso_spedizioni uo_avviso_spedizioni

type variables
long il_anno_documento, il_num_documento

//può essere fat_ven o bol_ven
string is_tipo_gestione
string is_num_fiscale_doc = ""

string is_cod_cliente
string is_path_logo_intestazione = ""
datawindow idw_report
end variables

forward prototypes
public function integer uof_avviso_spedizione (long fl_anno, long fl_numero, string fs_tipo_gestione, ref string fs_msg)
public function integer uof_check_pagamento (ref string fs_msg)
public function integer uof_check_definitiva (ref string fs_msg)
public function integer uof_msg_std_contrassegno (ref string fs_test_standard, ref string fs_msg)
public function integer uof_memorizza_documento (long fl_anno, long fl_numero, string fs_tipo_gestione, ref string fs_msg)
public function integer uof_check_contrassegno_inviato (ref string fs_msg)
public function integer uof_check_flag_invio_aut (string fs_cod_cliente, string fs_tipo_gestione, ref string fs_msg)
public function integer uof_invia_contrassegno (long fl_invio_aut, ref string fs_msg)
public function integer uof_crea_pdf_report (ref string fs_path, ref string fs_msg)
public function integer uof_check_flag_avviso_contrassegno (ref string fs_msg)
public function integer uof_invio_aut_fax (ref string fs_msg)
end prototypes

public function integer uof_avviso_spedizione (long fl_anno, long fl_numero, string fs_tipo_gestione, ref string fs_msg);long ll_return, ll_ret_invio_aut


if fl_anno>0 and fl_numero>0 and &
				(fs_tipo_gestione="fat_ven" or fs_tipo_gestione="bol_ven") then
	
	//memorizza nelle var. istanza le variabili
	il_anno_documento = fl_anno
	il_num_documento = fl_numero
	is_tipo_gestione = fs_tipo_gestione
	
	//verifica se il documento ha la numerazione fiscale, se no esci dall'oggetto --------------------
	ll_return = uof_check_definitiva(fs_msg)
	
	if ll_return<0 then
		//errore: in fs_msg c'è il messaggio
		return -1
		
	elseif ll_return = 0 then
		//esci e non fare altro
		return 0

	end if
	//-------------------------------------------------------------------------------------------------------
	
	//chiama la funzione che verifica se la tipologia pagamento del cliente prevede l'avviso contrassegno
	//dopo questa funzione nella var.istanza is_cod_cliente avrò il codice cliente (a meno che torni -1)
	ll_return = uof_check_pagamento(fs_msg)
	
	//controlla anche se in anagrafica cliente i relativi flag di invio automatico sono abilitati °°°°°°°°°°°°
	//se si devi allegare oltre al contrassegno, anche il report del documento
	ll_ret_invio_aut = uof_check_flag_invio_aut(is_cod_cliente, is_tipo_gestione, fs_msg)
	//-1 errore
	//1 invio automatico
	//0 no invio automatico
	if ll_ret_invio_aut<0 then
		//c'è un errore: è nella variabile fs_msg
		return -1
	end if
	//---------------------------------------------------------------------------------------------------°°°°°°°°°°°°
	
	
	if ll_return<0 then
		//c'è un errore: è nella variabile fs_msg
		return -1
		
	elseif ll_return=1 then
		//il pagamento prevede l'AVVISO CONTRASSEGNO
		ll_return = uof_invia_contrassegno(ll_ret_invio_aut, fs_msg)
		
		if ll_return<0 then
			//errore: in fs_msg c'è il messaggio
			return -1
			
		elseif ll_return=1 then
			//contrassegno inviato
			return 1
		else
			//contrassegno non inviato ma non per un errore
			return 0
		end if
		
	else
		//il pagamento NON prevede l'AVVISO CONTRASSEGNO
		
		//se ll_ret_invio_aut = 1 in anagrafica cliente i relativi flag di invio automatico sono abilitati		
		if ll_ret_invio_aut=1 then
			//il cliente prevede l'invio automatico via fax del documento
			//---> SCRIVI FUNZIONE###########################
			ll_return = uof_invio_aut_fax(fs_msg)
			
			if ll_return<0 then
				
			elseif ll_return = 1 then
				//fax inviato
				return 1
			else
				//fax non inviato ma non per un errore
				return 0
			end if
			
		else
			//il cliente non prevede nemmeno l'invio automatico via fax del documento
			//esci
			return 0
		end if
	end if
else
	return 1
end if

return 1
end function

public function integer uof_check_pagamento (ref string fs_msg);//torna 1 se il tipo pagamento del cliente del documento prevede l'avviso contrassegno
//		0 se non lo prevede
//		-1 se c'è un errore tornando byref una stringa contenente il messaggio di errore

string ls_cod_cliente, ls_cod_pagamento, ls_flag_avvisa_contrassegno

//leggi il cliente del documento
choose case is_tipo_gestione
	case "bol_ven"
		select cod_cliente
		into :ls_cod_cliente
		from tes_bol_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:il_anno_documento and
					num_registrazione=:il_num_documento;
		
	case "fat_ven"
		select cod_cliente
		into :ls_cod_cliente
		from tes_fat_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:il_anno_documento and
					num_registrazione=:il_num_documento;
					
	case else
		return 0
		
end choose

if sqlca.sqlcode<0 then
	fs_msg = "Errore il lettura cliente documento: "+sqlca.sqlerrtext
	return -1
end if

//######################################
//Donato 24/02/2011 ci sono casi in cui è valorizzato il cod fornitore ma non il cod cliente
//in questi casi non segnalare errore, esci dall'oggetto ed ignora l'avviso contrassegno
//quindi commento con ritorno 0

if isnull(ls_cod_cliente) or ls_cod_cliente="" then
	//fs_msg = "Non è stato possibile leggere il cliente del documento. Verificare!"
	//return -1
	return 0
end if
//fine modifica ############################

//-----------------------------------------------------------------------------------------

//leggi le condizioni di pagamento del cliente
select cod_pagamento
into :ls_cod_pagamento
from anag_clienti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_cliente=:ls_cod_cliente;

if sqlca.sqlcode<0 then
	fs_msg = "Errore il lettura tipo pagamento del cliente del documento: "+sqlca.sqlerrtext
	return -1
end if
//------------------------------------------------------------------------------------------

//leggi se il tipo pagamento prevede l'avviso contrassegno
select flag_avvisa_contrassegno
into :ls_flag_avvisa_contrassegno
from tab_pagamenti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_pagamento=:ls_cod_pagamento;

if sqlca.sqlcode<0 then
	fs_msg = "Errore il lettura flag avviso contrassegno del tipo pagamento del cliente del documento: "+sqlca.sqlerrtext
	return -1
end if
//------------------------------------------------------------------------------------------

is_cod_cliente = ls_cod_cliente

if ls_flag_avvisa_contrassegno="S" then
	return 1
else
	return 0
end if
end function

public function integer uof_check_definitiva (ref string fs_msg);//torna 1 se il documento è già stampato in definitivo
//		0 se non lo è
//		-1 se c'è un errore tornando byref una stringa contenente il messaggio di errore

long ll_anno_doc_fiscale, ll_num_doc_fiscale, ll_ret
string ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven
datetime ldt_data_doc_fiscale

//leggi la numerazione fiscale del documento
choose case is_tipo_gestione
	case "bol_ven"
		select anno_documento, num_documento, data_bolla
		into :ll_anno_doc_fiscale, :ll_num_doc_fiscale, :ldt_data_doc_fiscale
		from tes_bol_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:il_anno_documento and
					num_registrazione=:il_num_documento;
		
	case "fat_ven"
		
		//N.B. se Nota di credito allora ignora
		
		
		select anno_documento, num_documento, cod_tipo_fat_ven, data_fattura
		into :ll_anno_doc_fiscale, :ll_num_doc_fiscale, :ls_cod_tipo_fat_ven, :ldt_data_doc_fiscale
		from tes_fat_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:il_anno_documento and
					num_registrazione=:il_num_documento;
					
	case else
		return 0
		
end choose

if sqlca.sqlcode<0 then
	fs_msg = "Errore in controllo se il documento è stampa in definitivo: "+sqlca.sqlerrtext
	return -1
end if

//solo se fattura: verifica se nota di credito; se si allora esci
if is_tipo_gestione = "fat_ven" then
	select flag_tipo_fat_ven
	into   :ls_flag_tipo_fat_ven
	from   tab_tipi_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
			 
	if sqlca.sqlcode <> 0 then
		fs_msg = "Errore in controllo del flag_tipo fattura nella tabella tipi fatture~r~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if ls_flag_tipo_fat_ven = "N" then
		//è una nota di credito
		return 0
	end if
end if

is_num_fiscale_doc = ""

if not isnull(ll_anno_doc_fiscale) and ll_anno_doc_fiscale>0 and &
		not isnull(ll_num_doc_fiscale) and ll_num_doc_fiscale>0 then
	//il documento è stato stampato in definitivo
	
	if is_tipo_gestione = "bol_ven" then
		is_num_fiscale_doc = "DDT "
	elseif is_tipo_gestione = "fat_ven" then
		is_num_fiscale_doc = "Fattura "
	end if
	
	is_num_fiscale_doc += string(ll_anno_doc_fiscale)+"/"+string(ll_num_doc_fiscale)
	if not isnull(ldt_data_doc_fiscale) then is_num_fiscale_doc += " del "+string(ldt_data_doc_fiscale, "dd/mm/yyyy")
	
	//controlla se per caso c'è già tra le note un contrassegno inviato
	//se si avvisa l'operatore se vuole re-inviarlo
	ll_ret = uof_check_contrassegno_inviato(fs_msg)
	if ll_ret<0 then
		//in fs_msg l'errore
		return -1
	elseif ll_ret=0 then
		//non procedere all'invio contrassegno
		return 0
	else
		//OK all'invio contrassegno
		return 1
	end if

end if

//se arrivi fin qui considera il documento non ancora stampato in definitivo
//e quindi non attivare nessuna procedura di invio contrassegno

return 0

end function

public function integer uof_msg_std_contrassegno (ref string fs_test_standard, ref string fs_msg);long ll_anno_doc, ll_num_doc
datetime ldt_data_doc
decimal ld_totale

fs_test_standard = "IN RIFERIMENTO ALLA CONSEGNA COME DA "

choose case is_tipo_gestione
	case "bol_ven"
		fs_test_standard += " DDT ALLEGATO NR. "
		
		//recupera la numerazione fiscale della bolla, l'importo totale iva inclusa
		select 	anno_documento,
					num_documento,
					data_bolla,
					tot_val_bolla
		into 		:ll_anno_doc,
					:ll_num_doc,
					:ldt_data_doc,
					:ld_totale
		from tes_bol_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:il_anno_documento and
					num_registrazione=:il_num_documento;
		
	case "fat_ven"
		fs_test_standard += " FATTURA ALLEGATA NR. "
		
		//recupera la numerazione fiscale della fattura, l'importo totale iva inclusa
		select 	anno_documento,
					num_documento,
					data_fattura,
					tot_fattura
		into 		:ll_anno_doc,
					:ll_num_doc,
					:ldt_data_doc,
					:ld_totale
		from tes_fat_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:il_anno_documento and
					num_registrazione=:il_num_documento;
		
	case else
		return 0
		
end choose

if sqlca.sqlcode<0 then
	//errore
	fs_msg = "Errore in lettura numerazione fiscale documento e/o totale documento: "+sqlca.sqlerrtext
	return -1
end if


fs_test_standard += string(ll_anno_doc)+"/"+string(ll_num_doc)+" del "+string(ldt_data_doc, "dd-mm-yyyy")+", "
fs_test_standard += "VI COMUNICHIAMO CHE L'IMPORTO DA CONSEGNARE AL NOSTRO AUTISTA È DI € "
fs_test_standard += string(ld_totale, "###,###,##0.00") + " ."

return 1
end function

public function integer uof_memorizza_documento (long fl_anno, long fl_numero, string fs_tipo_gestione, ref string fs_msg);


return 1
end function

public function integer uof_check_contrassegno_inviato (ref string fs_msg);//leggi tra le note se c'è già un contrassegno inviato per questo documento
long ll_count

choose case is_tipo_gestione
	case "bol_ven"
		select count(*)
		into :ll_count
		from tes_bol_ven_note
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:il_anno_documento and
					num_registrazione=:il_num_documento and
					cod_nota like 'C%' and
					des_nota like '%contrassegno%';
		
	case "fat_ven"
		select count(*)
		into :ll_count
		from tes_fat_ven_note
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:il_anno_documento and
					num_registrazione=:il_num_documento and
					cod_nota like 'C%' and
					des_nota like '%contrassegno%';
					
	case else
		return 0
		
end choose

if sqlca.sqlcode<0 then
	fs_msg = "Errore in controllo se per il documento è stato già inviato il contrassegno: ~r~n"+sqlca.sqlerrtext
	return -1
end if

if ll_count>0 then
	if g_mb.confirm("Per questo documento risulta che il contrassegno è stato già inviato. Vuoi procedere comunque all'invio?") then
		//OK al re-invio
		return 1
	else
		//non procedere
		return 0
	end if
else
	//contrassegno non ancora inviato, quindi OK all'invio
	return 1
end if
end function

public function integer uof_check_flag_invio_aut (string fs_cod_cliente, string fs_tipo_gestione, ref string fs_msg);//torna 1 se il cliente del documento prevede l'invio automatico del documento via fax
//		0 se non lo prevede
//		-1 se c'è un errore tornando byref una stringa contenente il messaggio di errore

string ls_flag_invio_aut

//leggi il flag opportuno dall'anagrafica cliente
choose case fs_tipo_gestione
	case "bol_ven"
		select flag_invia_aut_ddt
		into :ls_flag_invio_aut
		from anag_clienti
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_cliente=:fs_cod_cliente;
		
	case "fat_ven"
		select flag_invia_aut_fat
		into :ls_flag_invio_aut
		from anag_clienti
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_azienda=:s_cs_xx.cod_azienda and
					cod_cliente=:fs_cod_cliente;
					
	case else
		return 0
		
end choose

if sqlca.sqlcode<0 then
	fs_msg = "Errore il lettura flag invio automatico per il cliente documento: "+sqlca.sqlerrtext
	return -1
end if

if ls_flag_invio_aut="S" then
	return 1
else
	return 0
end if
end function

public function integer uof_invia_contrassegno (long fl_invio_aut, ref string fs_msg);long ll_ret
string ls_testo_standard
boolean lb_invio_aut

//se arrivi fin qui il cliente ha un pagamento che prevede l'avviso di contrassegno
//chiedi conferma prima di proseguire

if not g_mb.confirm("Procedo con la stampa contrassegno?") then
	//annullato
	return 0
end if

ll_ret = uof_msg_std_contrassegno(ls_testo_standard, fs_msg)
if ll_ret<0 then
	//in fs_msg c'è l'errore
	return -1
	
elseif ll_ret=1 then
	//prosegui al di fuori dell'IF.....
else
	//no bolla o fattura di vendita
	return 0
end if

lb_invio_aut = false
setnull(s_cs_xx.parametri.parametro_dw_1)
//ls_path_pdf_doc = ""
if fl_invio_aut=1 then
	
	lb_invio_aut = true
	s_cs_xx.parametri.parametro_dw_1 = idw_report
//	//genera pdf temporaneo del report da allegare all'invio del contrassegno
//	//---> ###########################################
//	if uof_crea_pdf_report(ls_path_pdf_doc, fs_msg)<0 then
//		//in fs_msg il messaggio di errore
//		return -1
//	end if
end if


//se arrivi fin qui devi aprire la finestra di modifica testo standard e/o inserimento annotazioni contrassegno
s_cs_xx.parametri.parametro_s_1_a[1] = is_tipo_gestione
s_cs_xx.parametri.parametro_s_1_a[2] = ls_testo_standard
s_cs_xx.parametri.parametro_s_1_a[3] = is_path_logo_intestazione
s_cs_xx.parametri.parametro_b_1 = lb_invio_aut
s_cs_xx.parametri.parametro_d_1 = il_anno_documento
s_cs_xx.parametri.parametro_d_2 = il_num_documento

window_open(w_report_avviso_contrassegno, 0)

return 1
end function

public function integer uof_crea_pdf_report (ref string fs_path, ref string fs_msg);uo_archivia_pdf luo_pdf
luo_pdf = create uo_archivia_pdf

fs_path = luo_pdf.uof_crea_pdf_path(idw_report)

if fs_path = "errore" then
	fs_msg = "Errore nella creazione del file pdf."
	fs_path = ""
	destroy luo_pdf;
	return -1
end if

destroy luo_pdf;

return 1
end function

public function integer uof_check_flag_avviso_contrassegno (ref string fs_msg);//leggi tra le note se c'è già un contrassegno inviato per questo documento
string ls_flag_contrassegno_inviato

choose case is_tipo_gestione
	case "bol_ven"
		select flag_contrassegno_inviato
		into :ls_flag_contrassegno_inviato
		from tes_bol_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:il_anno_documento and
					num_registrazione=:il_num_documento;
		
	case "fat_ven"
		select flag_contrassegno_inviato
		into :ls_flag_contrassegno_inviato
		from tes_fat_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:il_anno_documento and
					num_registrazione=:il_num_documento;
					
end choose

if sqlca.sqlcode<0 then
	fs_msg = "Errore in controllo flag se per il documento è stato già inviato il contrassegno: ~r~n"+sqlca.sqlerrtext
	return -1
end if

if ls_flag_contrassegno_inviato="S" then
	fs_msg = "Per questo documento risulta che il contrassegno è stato già inviato. Ricordarsi di re-inviarlo!"
	return 1
end if

return 0
end function

public function integer uof_invio_aut_fax (ref string fs_msg);//il tipo pagamento del cliente non prevde l'invio del contrassegno
//però in anagrafica cliente il flag invio aut fax (ddt e/o fattura) è posto a S
//questa funzione invia fax del report del documento (bolla o fattura)

string ls_path_report, ls_testo, ls_fax_cliente, ls_cod_webfax, ls_allegati[]
uo_webfax luo_webfax
long ll_ret
blob l_blob

if not g_mb.confirm("Per il cliente è previsto l'invio automatico del documento tramite webfax. Procedere?") then
	//invio annullato
	return 0
end if

//apri la selezione del webfax
open(w_seleziona_webfax)

ls_cod_webfax = message.stringparm
if ls_cod_webfax="" or isnull(ls_cod_webfax) then
	//selezione annullata
	return 0
end if
//------

//genera il pdf del report del documento ------------------------------------
if uof_crea_pdf_report(ls_path_report, fs_msg) < 0 then
	//in fs_msg il messagio di errore
	return -1
end if

//invia fax ----------------------------------------------------------------------
select fax
into :ls_fax_cliente
from anag_clienti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_cliente =:is_cod_cliente;
			
if sqlca.sqlcode<0 then
	filedelete(ls_path_report)
	fs_msg = "Errore in lettura numero fax del cliente! "+sqlca.sqlerrtext
	return -1
end if

if ls_fax_cliente="" or isnull(ls_fax_cliente) then
	filedelete(ls_path_report)
	fs_msg = "Numero fax cliente del documento non specificato in anagrafica!"
	return -1
end if

ls_testo = ""	//lasciamo il testo del messaggio vuoto

luo_webfax = create uo_webfax
ls_allegati[1] = ls_path_report
ll_ret = luo_webfax.uof_invia_webfax(ls_cod_webfax, ls_testo, ls_allegati[], ls_fax_cliente, is_num_fiscale_doc, fs_msg)

if ll_ret < 0 then
	//in fs_msg il messaggio di errore
	filedelete(ls_path_report)
	destroy luo_webfax;
	return -1
end if

destroy luo_webfax;


//memorizza nella relativa tabella del documento, cod cod_nota Xnn ------------------
long ll_prog_mimytype
string ls_cod_nota, ls_note, ls_nota, ls_documento

ll_ret = f_file_to_blob(ls_path_report, l_blob)
if ll_ret < 0 then return -1

ll_ret = len(l_blob)

select prog_mimetype  
into   :ll_prog_mimytype  
from   tab_mimetype  
where  cod_azienda = :s_cs_xx.cod_azienda and
		 estensione = 'PDF';

if sqlca.sqlcode = 100 then
	filedelete(ls_path_report)
	fs_msg = "Impossibile archiviare il documento: impostare il mimetype PDF"
	return -1
end if

//preparazione dati per il salvataggio nella relativa tabella note (DDT o Fattura) -----------------------------------------

ls_nota = "WebFax documento "+ls_documento

ls_note = "Documento "+ls_documento + " inviato dall'utente " + s_cs_xx.cod_utente + "~r~n" + &
          "Data invio:" + string(today(), "dd/mm/yyyy") + "  Ora invio:" + string(now(), "hh:mm:ss")
//----------------------------------------------------------------------------------------------------------------------------------

setnull(ls_cod_nota)
//per il cod_nota facciamo una lettera ("X" per il FAX, in quanto F è in uso per invio mail fattura) più un progressivo di 2 cifre
//esempio X01

if is_tipo_gestione = "bol_ven" then
	select max(cod_nota)
	into   :ls_cod_nota
	from   tes_bol_ven_note
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_documento and
			 num_registrazione = :il_num_documento and
			 cod_nota like 'X%';
			 
	if sqlca.sqlcode<0 then
		filedelete(ls_path_report)
		fs_msg = "Errore in lettura max cod nota: ~r~n"+sqlca.sqlerrtext
		return -1
	end if
			 
	if isnull(ls_cod_nota) or len(ls_cod_nota) < 1 then
		ls_cod_nota = "X01"
	else
		ls_cod_nota = right(ls_cod_nota, 2)
		ll_ret = long(ls_cod_nota)
		ll_ret += 1
		ls_cod_nota = "X" + string(ll_ret, "00")
	end if
	
	insert into tes_bol_ven_note  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  cod_nota,   
		  des_nota, 
		  note,
		  prog_mimetype)  
	values ( :s_cs_xx.cod_azienda,   
			  :il_anno_documento,   
			  :il_num_documento,   
			  :ls_cod_nota,   
			  :ls_nota,   
			  :ls_note,
			  :ll_prog_mimytype );
	
	if sqlca.sqlcode<0 then
		filedelete(ls_path_report)
		fs_msg = "Errore in inserimento allegato (INSERT): ~r~n"+sqlca.sqlerrtext
		return -1
	end if
	
	updateblob tes_bol_ven_note
	set        note_esterne = :l_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
				  anno_registrazione = :il_anno_documento and
				  num_registrazione = :il_num_documento and
				  cod_nota = :ls_cod_nota;
	
	if sqlca.sqlcode<0 then
		filedelete(ls_path_report)
		fs_msg = "Errore in inserimento allegato (UPD BLOB): ~r~n"+sqlca.sqlerrtext
		return -1
	end if
	
elseif  is_tipo_gestione = "fat_ven" then
	
	select max(cod_nota)
	into   :ls_cod_nota
	from   tes_fat_ven_note
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_documento and
			 num_registrazione = :il_num_documento and
			 cod_nota like 'X%';
			 
	if sqlca.sqlcode<0 then
		filedelete(ls_path_report)
		fs_msg = "Errore in lettura max cod nota: ~r~n"+sqlca.sqlerrtext
		return -1
	end if
			 
	if isnull(ls_cod_nota) or len(ls_cod_nota) < 1 then
		ls_cod_nota = "X01"
	else
		ls_cod_nota = right(ls_cod_nota, 2)
		ll_ret = long(ls_cod_nota)
		ll_ret += 1
		ls_cod_nota = "X" + string(ll_ret, "00")
	end if
	
	insert into tes_fat_ven_note  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  cod_nota,   
		  des_nota, 
		  note,
		  prog_mimetype)  
	values ( :s_cs_xx.cod_azienda,   
			  :il_anno_documento,   
			  :il_num_documento,   
			  :ls_cod_nota,   
			  :ls_nota,   
			  :ls_note,
			  :ll_prog_mimytype );
	
	if sqlca.sqlcode<0 then
		filedelete(ls_path_report)
		fs_msg = "Errore in inserimento allegato (INSERT): ~r~n"+sqlca.sqlerrtext
		return -1
	end if
	
	updateblob tes_fat_ven_note
	set        note_esterne = :l_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
				  anno_registrazione = :il_anno_documento and
				  num_registrazione = :il_num_documento and
				  cod_nota = :ls_cod_nota;
	
	if sqlca.sqlcode<0 then
		filedelete(ls_path_report)
		fs_msg = "Errore in inserimento allegato (UPD BLOB): ~r~n"+sqlca.sqlerrtext
		return -1
	end if
	
end if


//elimina il file quando finito
filedelete(ls_path_report)

return 1
end function

on uo_avviso_spedizioni.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_avviso_spedizioni.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;//oggetto creato per la specifica AvvisoMetrceSpedita - rev05 del 09/11/2010
end event

