﻿$PBExportHeader$uo_calcola_documento_euro.sru
$PBExportComments$Calcolo documenti di acquisto e vendita in EURO
forward
global type uo_calcola_documento_euro from nonvisualobject
end type
end forward

global type uo_calcola_documento_euro from nonvisualobject
end type
global uo_calcola_documento_euro uo_calcola_documento_euro

type variables
long      il_anno_registrazione, il_num_registrazione
string    is_tipo_documento, is_cod_valuta, is_colonna_quantita, is_colonna_prog_riga, is_flag_cc_manuali, &
          is_gestione_cc
dec{4}    id_cambio, id_precisione_valuta, id_sconto_commerciale, id_sconto_cassa, id_spese_varie, id_spese_bolli,&
		    id_spese_imballo, id_spese_trasporto, id_merci, id_precisione
datastore ids_righe_documento

//Donato 06-11-2008 aggiunta variabile booleana normalmente false
//per saltare il controllo sull'esposizione del cliente se messa prima a true
//(solo nei punti che istanziano l'oggetto e che hanno già fatto il controllo)
//ed evitare di dare il msg due volte
boolean ib_salta_esposiz_cliente = false
end variables

forward prototypes
public function integer uof_cancella_dettagli (ref string fs_messaggio)
public function integer uof_costo_trasporto (ref string fs_messaggio)
public function integer uof_spese_bancarie (ref string fs_messaggio)
public function integer uof_totalizza_parziali (ref string fs_messaggio)
public function integer uof_calcola_imponibile_riga (ref string fs_messaggio)
public function integer uof_calcola_provvigioni (ref string fs_messaggio)
public function integer uof_azzera_documento (ref string fs_messaggio)
public function integer uof_fine_calcolo (ref string fs_messaggio)
public function integer uof_calcola_contropartite (ref string fs_messaggio)
public function integer uof_calcola_evasione (ref string fs_messaggio)
public function decimal uof_arrotonda_valuta (decimal fd_valore, string fs_cod_valuta, string fs_tipo_arrotondamento, ref string fs_messaggio)
public function integer uof_arrotonda (ref decimal fd_valore, decimal fd_precisione, string fs_tipo_arrotondamento)
public function integer uof_totalizza_iva (ref string fs_messaggio)
public function integer uof_controllo_quadratura (ref string fs_messaggio)
public function date uof_calcola_data_fine_mese (date fdd_data)
public function integer uof_addebiti_cliente (ref string fs_messaggio)
public function integer uof_calcola_scadenze (ref string fs_messaggio)
public function integer uof_calcola_totali_documento (ref string fs_messaggio)
public function integer uof_esposizione_cliente (ref string fs_messaggio)
public function integer uof_calcola_documento (long fl_anno_registrazione, long fl_num_registrazione, string fs_tipo_documento, ref string fs_messaggio)
public function integer uof_calcola_centro_costo (ref string fs_messaggio)
public function integer uof_totalizza_centro_costo (ref string fs_messaggio)
public function date uof_converti_data (string fs_giorno, string fs_mese, string fs_anno)
public function integer uof_addebiti_cliente_bolle (ref string fs_messaggio)
public function integer uof_calcola_costo_trasporto (string fs_cod_vettore_forzato, string fs_cod_destinazione_forzata, ref decimal fd_costo_trasporto, ref string fs_messaggio)
public function integer uof_sostituzione_date (date fdd_data, ref date fdd_data_sost, string fs_cod_cliente, string fs_cod_fornitore, string fs_cod_contatto, ref string fs_messaggio)
public function string uof_tipo_fornitore (string as_cod_fornitore)
public function integer uof_get_imponibile_riga (string as_tipo_input, long al_prog_riga, datawindow adw_data, string as_cod_tipo_dettaglio, decimal ad_prezzo_valuta, decimal ad_quantita, decimal ad_sconti_riga[], decimal ad_imponibile_forzato, string as_flag_imponibile_forzato, ref decimal ad_imponibile_riga, ref decimal ad_imponibile_iva_valuta, ref string fs_messaggio)
public function integer uof_calcola_rate (decimal fd_imponibile, decimal fd_iva, string fs_cod_pagamento, date fdd_data_partenza, ref date fdd_rate_data[], ref decimal fd_rate_importo[], ref string fs_rate_tipo[], ref string fs_modalita[], ref long fl_tot_rate, ref string fs_messaggio)
public function integer uof_salda_ordine (long al_anno_registrazione, long al_num_registrazione, ref string as_message)
end prototypes

public function integer uof_cancella_dettagli (ref string fs_messaggio);string ls_cod_cliente, ls_cod_valuta, ls_cod_tipo_det_ven, ls_cod_documento, ls_cod_tipo_det_ven_trasporto,&
		 ls_cod_pagamento

datetime ldt_data_riferimento, ldt_data_fattura, ldt_data_registrazione, ldt_data_bolla


if is_tipo_documento = "fat_ven" then
	
	// **********************************************************************************************************
	// CANCELLAZIONE DELLE RIGHE DI ADDEBITI CLIENTE AGGIUNTE IN PRECEDENZA ALLA FATTURA
	// **********************************************************************************************************
	select cod_valuta,
			 cod_cliente,		 
			 data_registrazione,		 
			 data_fattura,
			 cod_documento
	into   :ls_cod_valuta,
  	       :ls_cod_cliente,		 
			 :ldt_data_registrazione,		 
			 :ldt_data_fattura,
			 :ls_cod_documento
	from   tes_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
		    anno_registrazione = :il_anno_registrazione and
 			 num_registrazione = :il_num_registrazione;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di tes_fat_ven: " + sqlca.sqlerrtext
		return -1
	end if

	if isnull(ls_cod_documento) then
	
		ldt_data_riferimento = ldt_data_registrazione
	
		if isnull(ldt_data_riferimento) then
			ldt_data_riferimento = datetime(today(), 00:00:00)
		end if	
	
	else
	
		ldt_data_riferimento = ldt_data_fattura

	end if

	declare cu_addebiti cursor for
	select  cod_tipo_det_ven       
	from    anag_clienti_addebiti
	where   cod_azienda = :s_cs_xx.cod_azienda and
  	        cod_cliente = :ls_cod_cliente and
			  data_inizio <= :ldt_data_riferimento and
			  data_fine >= :ldt_data_riferimento and
			  cod_valuta = :ls_cod_valuta and
			  flag_mod_valore_fatture = 'N' and
			  flag_tipo_documento = 'F';
		 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di anag_clienti_addebiti: " + sqlca.sqlerrtext
		return -1
	end if

	open cu_addebiti;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Errore nella open del cursore cu_addebiti: " + sqlca.sqlerrtext
		return -1
	end if

	do while true
	
		fetch cu_addebiti into :ls_cod_tipo_det_ven;
	
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella fetch del cursore cu_addebiti: " + sqlca.sqlerrtext
			close cu_addebiti;
			return -1
		end if
	
		if sqlca.sqlcode = 100 then
			exit
		end if	
	
		if isnull(ls_cod_tipo_det_ven) then
			fs_messaggio = "Errore nella fetch del cursore cu_addebiti: impostare il tipo dettaglio vendita negli addebiti clienti"
			close cu_addebiti;
			return -1
		end if
	
		if isnull(ls_cod_valuta) then
			fs_messaggio = "Errore nella fetch del cursore cu_addebiti: impostare la valuta negli addebiti clienti"
			close cu_addebiti;
			return -1
		end if	
		
		delete det_fat_ven_cc
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 prog_riga_fat_ven in ( select prog_riga_fat_ven
				                        from   det_fat_ven
												where  cod_azienda = :s_cs_xx.cod_azienda and
														 anno_registrazione = :il_anno_registrazione and
														 num_registrazione = :il_num_registrazione and
														 cod_tipo_det_ven = :ls_cod_tipo_det_ven) ;
			 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella delete di det_fat_ven_cc: " + sqlca.sqlerrtext
			return -1
		end if	
	

		delete det_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella delete di det_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if	
	
	loop
	
	close cu_addebiti;

	// **********************************************************************************************************
	// CANCELLAZIONE DELLE RIGHE DI SPESE BANCARIE AGGIUNTE IN PRECEDENZA ALLA FATTURA
	// **********************************************************************************************************
	select cod_pagamento
	into 	 :ls_cod_pagamento
	from   tes_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
		    anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione;
 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di tes_fat_ven: " + sqlca.sqlerrtext
		return -1
	end if
	
	delete det_fat_ven_cc
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione and
			 prog_riga_fat_ven in ( select prog_riga_fat_ven
											from   det_fat_ven
											where  cod_azienda = :s_cs_xx.cod_azienda and
													 anno_registrazione = :il_anno_registrazione and
													 num_registrazione = :il_num_registrazione and
													 cod_tipo_det_ven in ( select distinct cod_tipo_det_ven
													 							from tab_pagamenti
																				 where cod_azienda = :s_cs_xx.cod_azienda ) 	 ) ;
		 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella delete di det_fat_ven_cc: " + sqlca.sqlerrtext
		return -1
	end if	
	
	delete det_fat_ven
	where cod_azienda        = :s_cs_xx.cod_azienda and
  	       anno_registrazione  = :il_anno_registrazione and
		   num_registrazione  = :il_num_registrazione and
  	       cod_tipo_det_ven    in ( select distinct cod_tipo_det_ven
										from tab_pagamenti
										where cod_azienda = :s_cs_xx.cod_azienda ) 	;
		 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella delete di det_fat_ven: " + sqlca.sqlerrtext
		return -1
	end if
	
elseif is_tipo_documento = "bol_ven" then
	
	// *** MICHELA 24/01/2006 PER SPECIFICA STORICO BOLLE PTENDA: FUNZIONE 5 ************************************
	// **********************************************************************************************************
	// CANCELLAZIONE DELLE RIGHE DI ADDEBITI CLIENTE AGGIUNTE IN PRECEDENZA ALLA BOLLA
	// **********************************************************************************************************
	
	select cod_valuta,
			 cod_cliente,		 
			 data_registrazione,		 
			 data_bolla,
			 cod_documento
	into   :ls_cod_valuta,
  	       :ls_cod_cliente,		 
			 :ldt_data_registrazione,		 
			 :ldt_data_bolla,
			 :ls_cod_documento
	from   tes_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
		    anno_registrazione = :il_anno_registrazione and
 			 num_registrazione = :il_num_registrazione;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di tes_bol_ven: " + sqlca.sqlerrtext
		return -1
	end if

	if isnull(ls_cod_documento) then
	
		ldt_data_riferimento = ldt_data_registrazione
	
		if isnull(ldt_data_riferimento) then
			ldt_data_riferimento = datetime(today(), 00:00:00)
		end if	
	
	else
	
		ldt_data_riferimento = ldt_data_bolla

	end if

	declare cu_addebiti_bol cursor for
	select  cod_tipo_det_ven       
	from    anag_clienti_addebiti
	where   cod_azienda = :s_cs_xx.cod_azienda and
  	        cod_cliente = :ls_cod_cliente and
			  data_inizio <= :ldt_data_riferimento and
			  data_fine >= :ldt_data_riferimento and
			  cod_valuta = :ls_cod_valuta and
			  flag_mod_valore_bolle = 'N'  and
			  flag_tipo_documento = 'B';
		 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di anag_clienti_addebiti: " + sqlca.sqlerrtext
		return -1
	end if

	open cu_addebiti_bol;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Errore nella open del cursore cu_addebiti_bol: " + sqlca.sqlerrtext
		return -1
	end if

	do while true
	
		fetch cu_addebiti_bol into :ls_cod_tipo_det_ven;
	
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella fetch del cursore cu_addebiti_bol: " + sqlca.sqlerrtext
			close cu_addebiti_bol;
			return -1
		end if
	
		if sqlca.sqlcode = 100 then
			exit
		end if	
	
		if isnull(ls_cod_tipo_det_ven) then
			fs_messaggio = "Errore nella fetch del cursore cu_addebiti_bol: impostare il tipo dettaglio vendita negli addebiti clienti"
			close cu_addebiti_bol;
			return -1
		end if
	
		if isnull(ls_cod_valuta) then
			fs_messaggio = "Errore nella fetch del cursore cu_addebiti_bol: impostare la valuta negli addebiti clienti"
			close cu_addebiti_bol;
			return -1
		end if	

		delete det_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella delete di det_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if	
	
	loop
	
	close cu_addebiti_bol;

	// **********************************************************************************************************
	// CANCELLAZIONE DELLE RIGHE DI SPESE BANCARIE AGGIUNTE IN PRECEDENZA ALLA FATTURA
	// **********************************************************************************************************
	select cod_pagamento
	into 	 :ls_cod_pagamento
	from   tes_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
		    anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione;
 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di tes_bol_ven: " + sqlca.sqlerrtext
		return -1
	end if

	select cod_tipo_det_ven
	into   :ls_cod_tipo_det_ven
	from   tab_pagamenti  
	where  cod_azienda = :s_cs_xx.cod_azienda  and  
   	    cod_pagamento = :ls_cod_pagamento;
		 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di tab_pagamenti: " + sqlca.sqlerrtext
		return -1
	end if
	
	
	delete det_bol_ven
	where  cod_azienda        = :s_cs_xx.cod_azienda and
  	       anno_registrazione = :il_anno_registrazione and
			 num_registrazione  = :il_num_registrazione and
  	       cod_tipo_det_ven    in ( select distinct cod_tipo_det_ven
										from tab_pagamenti
										 where cod_azienda = :s_cs_xx.cod_azienda ) ;
		 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella delete di det_bol_ven: " + sqlca.sqlerrtext
		return -1
	end if	
	
end if	

// **********************************************************************************************************
// CANCELLAZIONE DELLE RIGHE DI COSTO TRASPORTO AGGIUNTE IN PRECEDENZA AL DOCUMENTO
// **********************************************************************************************************
select cod_tipo_det_ven_trasporto
into   :ls_cod_tipo_det_ven_trasporto
from   con_vendite
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di con_vendite: " + sqlca.sqlerrtext
	return -1
end if

choose case is_tipo_documento
		
	case "off_ven"
		
		delete
		from   det_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven_trasporto;
			
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di det_off_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
	case "ord_ven"
				
		delete
		from   det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven_trasporto;
			
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di det_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
	case "bol_ven"
				
		delete
		from   det_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven_trasporto;
			
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di det_bol_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "fat_ven"
	
		// stefanop: 27/02/2014: aggiunto  flag_riga_spesa_trasp <> 'S in quanto non deve
		// cancellare le spese automatiche!
		delete from det_fat_ven_cc
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 prog_riga_fat_ven in ( select prog_riga_fat_ven
												from   det_fat_ven
												where  cod_azienda = :s_cs_xx.cod_azienda and
														 anno_registrazione = :il_anno_registrazione and
														 num_registrazione = :il_num_registrazione and
														 cod_tipo_det_ven = :ls_cod_tipo_det_ven_trasporto and
														  flag_riga_spesa_trasp <> 'S') ;			 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella delete di det_fat_ven_cc: " + sqlca.sqlerrtext
			return -1
		end if	
	
		delete from det_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven_trasporto and
				 flag_riga_spesa_trasp <> 'S';		
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di det_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
end choose

return 0
end function

public function integer uof_costo_trasporto (ref string fs_messaggio);string ls_cod_cliente, ls_cod_valuta, ls_cod_vettore, ls_cod_destinazione, ls_prov_dest, ls_provincia, ls_cod_nazione, &
       ls_cod_mezzo, ls_flag_inoltro, ls_flag_tipo_tariffa, ls_nota, ls_cod_tipo_det_ven_trasporto, ls_cod_iva, &
	 	 ls_cod_pagamento, ls_cod_iva_cli, ls_flag_tipo_det_ven, ls_messaggio, ls_select, ls_sql, ls_sql_stringa, &
		 ls_sql_select, ls_flag_contrassegno, ls_flag_calcolo_trasporto, ls_tabella_testata, ls_tabella_dettaglio, &
		 ls_tabella_iva, ls_tabella_tipo_doc, ls_tabella_controllo, ls_colonna_tipo_doc, ls_colonna_prog, &
		 ls_colonna_quan, ls_colonna_totale, ls_colonna_totale_valuta, ls_cod_contatto
		 
dec{4} ld_cambio_ven, ld_prezzo_spedizione, ld_prezzo_inoltro, ld_perc_valore_merce, ld_costo_trasp, ld_peso_lordo, &
       ld_tot_documento, ld_sconto_testata, ld_costo_forfait_sped, ld_costo_forfait_inol, ld_min_contrassegno, &
		 ld_perc_contrassegno, ld_arrotonda_peso, ld_quan_tonda, ld_quantita, ld_costo_contrassegno, ld_costo_quan, &
		 ld_costo_per_val, ld_tot_imponibile, ld_tot_iva, ld_tot_documento_valuta, &
		 ld_tot_imponibile_valuta, ld_tot_iva_valuta, ld_tot_merci, ld_tot_sconti_commerciali
		 
long   ll_prog_riga_doc_ven, ll_i, ll_risp, ll_cont


select cod_tipo_det_ven_trasporto
into   :ls_cod_tipo_det_ven_trasporto 
from   con_vendite
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di con_vendite: " + sqlca.sqlerrtext
	return -1
end if

choose case is_tipo_documento
		
	case "off_ven"
		
		ls_tabella_testata = "tes_off_ven"
		ls_tabella_dettaglio = "det_off_ven"
		ls_tabella_iva = "iva_off_ven"
		ls_tabella_tipo_doc = "tab_tipi_off_ven"
		ls_tabella_controllo = "con_off_ven"
		ls_colonna_tipo_doc = "cod_tipo_off_ven"
		ls_colonna_prog = "prog_riga_off_ven"
		ls_colonna_quan = "quan_offerta"
		ls_colonna_totale = "tot_val_offerta"
		ls_colonna_totale_valuta  = "tot_val_offerta_valuta"		
				
	case "ord_ven"
		
		ls_tabella_testata = "tes_ord_ven"
		ls_tabella_dettaglio = "det_ord_ven"
		ls_tabella_iva = "iva_ord_ven"
		ls_tabella_tipo_doc = "tab_tipi_ord_ven"
		ls_tabella_controllo = "con_ord_ven"
		ls_colonna_tipo_doc = "cod_tipo_ord_ven"
		ls_colonna_prog = "prog_riga_ord_ven"
		ls_colonna_quan = "quan_ordine"
		ls_colonna_totale = "tot_val_ordine"
		ls_colonna_totale_valuta  = "tot_val_ordine_valuta"		
		
	case "bol_ven"
		
		ls_tabella_testata = "tes_bol_ven"
		ls_tabella_dettaglio = "det_bol_ven"
		ls_tabella_iva = "iva_bol_ven"
		ls_tabella_tipo_doc = "tab_tipi_bol_ven"
		ls_tabella_controllo = "con_bol_ven"
		ls_colonna_tipo_doc = "cod_tipo_bol_ven"
		ls_colonna_prog = "prog_riga_bol_ven"
		ls_colonna_quan = "quan_consegnata"
		ls_colonna_totale = "tot_val_bolla"
		ls_colonna_totale_valuta  = "tot_val_bolla_valuta"
		
	case "fat_ven"
	
		ls_tabella_testata = "tes_fat_ven"
		ls_tabella_dettaglio = "det_fat_ven"
		ls_tabella_iva = "iva_fat_ven"
		ls_tabella_tipo_doc = "tab_tipi_fat_ven"
		ls_tabella_controllo = "con_fat_ven"
		ls_colonna_tipo_doc = "cod_tipo_fat_ven"
		ls_colonna_prog = "prog_riga_fat_ven"
		ls_colonna_quan = "quan_fatturata"
		ls_colonna_totale = "tot_fattura"
		ls_colonna_totale_valuta  = "tot_fattura_valuta"
		
end choose

select flag_tipo_det_ven
into   :ls_flag_tipo_det_ven
from   tab_tipi_det_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_det_ven = :ls_cod_tipo_det_ven_trasporto;
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di tab_tipi_det_ven: " + sqlca.sqlerrtext
	return -1
end if

if ls_flag_tipo_det_ven <>  "T" then
	fs_messaggio = "Tipo dettaglio vendite deve essere di tipo TRASPORTI"
	return -1
end if
//
//declare cu_cursore_testata dynamic cursor for sqlsa;
//
//ls_sql_select = "select " + ls_colonna_totale + ", " + ls_colonna_totale_valuta + ", imponibile_iva, imponibile_iva_valuta, importo_iva, importo_iva_valuta, tot_merci, tot_sconti_commerciali from " + ls_tabella_testata + &
//                " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(il_anno_registrazione) + &
//			       " and num_registrazione = " + string(il_num_registrazione)
//					 
//prepare sqlsa from :ls_sql_select;
//
//open dynamic cu_cursore_testata;
//
//if sqlca.sqlcode <> 0 then
//	fs_messaggio = "Errore nella open del cursore cu_cursore_testata: " + sqlca.sqlerrtext
//	return -1
//end if
//
//fetch cu_cursore_testata into :ld_tot_documento, :ld_tot_documento_valuta, :ld_tot_imponibile, :ld_tot_imponibile_valuta, :ld_tot_iva, :ld_tot_iva_valuta, :ld_tot_merci, :ld_tot_sconti_commerciali;
//
//if sqlca.sqlcode <> 0 then
//	fs_messaggio = "Errore nella fetch del cursore cu_cursore_testata: " + sqlca.sqlerrtext
//	close cu_cursore_testata;
//	return -1
//end if
//
//close cu_cursore_testata;
//
//
//ls_sql = " select cod_cliente, cod_valuta, cod_pagamento, provincia, cod_vettore, cod_mezzo, peso_lordo from " + ls_tabella_testata + " " + &
//				" where cod_azienda = '"+ s_cs_xx.cod_azienda + "' and " + &
//				" anno_registrazione = "+ string(il_anno_registrazione) + " and " + &
//				" num_registrazione = "+ string(il_num_registrazione)
//
//declare cu_testata dynamic cursor for sqlsa;
//
//prepare sqlsa from :ls_sql;
//
//open dynamic cu_testata;
//
//if sqlca.sqlcode <> 0 then
//	fs_messaggio = "Errore nella open del cursore cu_testata: " + sqlca.sqlerrtext
//	return -1
//end if
//
//fetch cu_testata into :ls_cod_cliente, :ls_cod_valuta, :ls_cod_pagamento, :ls_prov_dest, :ls_cod_vettore, :ls_cod_mezzo, :ld_peso_lordo;
//	
//if sqlca.sqlcode <> 0 then
//	fs_messaggio = "Errore nella fetch del cursore cu_testata: " + sqlca.sqlerrtext
//	close cu_cursore_testata;
//	return -1
//end if
//
//close cu_testata;
//
//setnull(ls_cod_contatto)
//
//if is_tipo_documento = "off_ven" and isnull(ls_cod_cliente) then
//	
//	select cod_contatto
//	into   :ls_cod_contatto
//	from   tes_off_ven
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       anno_registrazione = :il_anno_registrazione and
//			 num_registrazione = :il_num_registrazione;
//			 
//	if sqlca.sqlcode <> 0 then
//		fs_messaggio = "Errore nella select di tes_off_ven: "  + sqlca.sqlerrtext
//		return -1
//	end if
//	
//end if
//
//if isnull(ls_cod_contatto) and isnull(ls_cod_cliente) then
//	fs_messaggio = "Cliente o contatto non indicato: impossibile proseguire"
//	return -1
//end if
//
//if isnull(ls_cod_cliente) then
//	
//	// la bolla o la fattura riguardano un fornitore; caso non previsto quindi esco dal calcolo trasporto.
//	if is_tipo_documento = "bol_ven" or is_tipo_documento = "fat_ven" then
//		return 0
//	end if
//	
//	select provincia,
//			 cod_nazione,
//			 cod_iva
//	into   :ls_provincia,
//		    :ls_cod_nazione,
//		    :ls_cod_iva_cli
//	from   anag_contatti
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_contatto = :ls_cod_contatto;
//			 
//	if sqlca.sqlcode <> 0 then
//		fs_messaggio = "Errore nella select di anag_contatti: " + sqlca.sqlerrtext
//		return -1
//	end if		 
//			 
//else  
//	
//	select provincia,
//			 cod_nazione,
//			 cod_iva
//	into   :ls_provincia,
//		    :ls_cod_nazione,
//		    :ls_cod_iva_cli
//	from   anag_clienti
//	where  cod_azienda = :s_cs_xx.cod_azienda and
//	       cod_cliente = :ls_cod_cliente;
//			 
//	if sqlca.sqlcode <> 0 then
//		fs_messaggio = "Errore nella select di anag_clienti: " + sqlca.sqlerrtext
//		return -1
//	end if		 
//			 
//end if
//
//if isnull(ls_cod_nazione) then
//	ls_cod_nazione = "ITA"
//end if
//
//if isnull(ld_peso_lordo) then
//	ld_peso_lordo = 0
//end if
//
//if isnull(ld_tot_documento) then
//	ld_tot_documento = 0
//end if
//
//if not isnull(ls_prov_dest) then 
//	ls_cod_destinazione = ls_prov_dest
//elseif not isnull(ls_provincia) then
//	ls_cod_destinazione = ls_provincia
////else
////	fs_messaggio = "Inserire provincia di destinazione in testata documento"
////	return -1
//end if
//
//if isnull(ls_cod_vettore) then
//	fs_messaggio = "La condizione di pagamento prevede l'addebito del trasporto, ma non è stato indicato il vettore in testata: il calcolo del documento viene effettuato ugualmente!"
//	return 0
//end if
//
//if isnull(ls_cod_mezzo) then
//	
//	ls_flag_inoltro = "N"
//	
//else
//	
//	setnull(ls_flag_inoltro)
//	
//	select flag_inoltro
//	into :ls_flag_inoltro
//	from tab_mezzi
//	where cod_azienda = :s_cs_xx.cod_azienda
//	  and cod_mezzo = :ls_cod_mezzo;
//	  
//	if sqlca.sqlcode < 0 then
//		fs_messaggio = "Errore nella select di tab_mezzi: " + sqlca.sqlerrtext
//		return -1
//	end if
//	
//end if
//
//select count(*)
//into   :ll_cont
//from   listino_vettori
//WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
//		( cod_vettore = :ls_cod_vettore ) AND  
//		( cod_valuta = :ls_cod_valuta ) AND  
//		( cod_destinazione = 'XX');
//		
//if ll_cont > 0 then
//	
//	SELECT flag_tipo_tariffa,   
//			prezzo_spedizione,   
//			prezzo_inoltro,   
//			perc_valore_merce,   
//			costo_forfait_sped, 
//			costo_forfait_inol,
//			min_contrassegno,
//			perc_contrassegno,
//			arrotonda_peso
//	INTO :ls_flag_tipo_tariffa,   
//			:ld_prezzo_spedizione,   
//			:ld_prezzo_inoltro,   
//			:ld_perc_valore_merce,
//			:ld_costo_forfait_sped, 
//			:ld_costo_forfait_inol,
//			:ld_min_contrassegno,
//			:ld_perc_contrassegno,
//			:ld_arrotonda_peso
//	FROM listino_vettori  
//	WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
//			( cod_vettore = :ls_cod_vettore ) AND  
//			( cod_valuta = :ls_cod_valuta ) AND  
//			( cod_destinazione = 'XX');
//			
//else
//	
//	SELECT flag_tipo_tariffa,   
//			prezzo_spedizione,   
//			prezzo_inoltro,   
//			perc_valore_merce,   
//			costo_forfait_sped, 
//			costo_forfait_inol,
//			min_contrassegno,
//			perc_contrassegno,
//			arrotonda_peso
//	INTO :ls_flag_tipo_tariffa,   
//			:ld_prezzo_spedizione,   
//			:ld_prezzo_inoltro,   
//			:ld_perc_valore_merce,
//			:ld_costo_forfait_sped, 
//			:ld_costo_forfait_inol,
//			:ld_min_contrassegno,
//			:ld_perc_contrassegno,
//			:ld_arrotonda_peso
//	FROM listino_vettori  
//	WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
//			( cod_vettore = :ls_cod_vettore ) AND  
//			( cod_valuta = :ls_cod_valuta ) AND  
//			( cod_destinazione = :ls_cod_destinazione );
//			
//end if
//
//if sqlca.sqlcode < 0 then
//	fs_messaggio = "Errore nella select di listino_vettori: " + sqlca.sqlerrtext
//	return -1
//elseif sqlca.sqlcode = 100 then
//	return 0
//end if
//
//if isnull(ls_flag_inoltro) or (ls_flag_inoltro = "N") then 
//	ld_prezzo_inoltro = 0
//	ld_costo_forfait_inol = 0
//end if
//
//if isnull(ld_prezzo_spedizione) then
//	ld_prezzo_spedizione = 0
//end if
//
//if isnull(ld_prezzo_inoltro) then
//	ld_prezzo_inoltro = 0
//end if
//
//if isnull(ld_perc_valore_merce) then
//	ld_perc_valore_merce = 0
//end if
//
//if isnull(ld_costo_forfait_sped) then
//	ld_costo_forfait_sped = 0
//end if
//
//if isnull(ld_costo_forfait_inol) then
//	ld_costo_forfait_inol = 0
//end if
//
//if isnull(ld_min_contrassegno) then
//	ld_min_contrassegno = 0
//end if
//
//if isnull(ld_perc_contrassegno) then
//	ld_perc_contrassegno = 0
//end if
//
//if isnull(ld_arrotonda_peso) then
//	ld_arrotonda_peso = 0
//end if	
//
//select flag_contrassegno, 
//       flag_calcolo_trasporto
//into   :ls_flag_contrassegno, 
//       :ls_flag_calcolo_trasporto
//from   tab_pagamenti
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 cod_pagamento = :ls_cod_pagamento;
//		 
//if sqlca.sqlcode <>0 then
//	fs_messaggio = "Errore nella select di tab_pagamenti"
//	return -1
//end if
//
//if isnull(ls_flag_contrassegno) then
//	ls_flag_contrassegno = "N"
//end if
//
//if isnull(ls_flag_calcolo_trasporto) then
//	ls_flag_calcolo_trasporto = "N"
//end if	
//
//if ld_perc_valore_merce <> 0 then
//	ld_costo_per_val = (ld_tot_merci - ld_tot_sconti_commerciali) * ld_perc_valore_merce / 100
//end if
//
//if ls_flag_contrassegno = "S" and ls_flag_calcolo_trasporto = "S" then
//	
//	if ld_perc_contrassegno <> 0 then
//		
//		ld_costo_contrassegno = ld_tot_documento * ld_perc_contrassegno / 100
//		
//		if ld_min_contrassegno > 0 and ld_min_contrassegno > ld_costo_contrassegno then
//			ld_costo_contrassegno = ld_min_contrassegno
//		end if
//		
//	end if
//	
//end if
//
//if ls_flag_tipo_tariffa = "P" then		// costo in base al peso
//
//	if ld_arrotonda_peso <> 0 then
//		
//		ld_quan_tonda = (truncate((ld_peso_lordo / ld_arrotonda_peso), 0) * ld_arrotonda_peso)
//		
//		if (ld_peso_lordo - ld_quan_tonda) <> 0 then
//			ld_quan_tonda = ld_quan_tonda + ld_arrotonda_peso
//		end if
//		
//	else
//		
//		ld_quan_tonda = ld_peso_lordo
//		
//	end if
//	
//	ld_costo_quan = (ld_prezzo_spedizione + ld_prezzo_inoltro) * ld_quan_tonda
//	
//end if
//
//if ls_flag_tipo_tariffa = "V" then		// costo in base al volume
//	fs_messaggio = "Manca Volume in testata documento"
//	return  -1
//end if
//
//if ls_flag_tipo_tariffa = "F" then		// costo Forfait
//	ld_costo_quan = 0
//end if
//
//ld_costo_trasp     = ld_costo_forfait_sped + ld_costo_forfait_inol + ld_costo_quan + ld_costo_per_val + ld_costo_contrassegno
//
//uof_arrotonda(ld_costo_trasp, id_precisione_valuta, "euro")



// fine del calcolo costo del trasporto; vado a inserire la riga di costo nel documento.

if uof_calcola_costo_trasporto("", "", ref ld_costo_trasp, ref fs_messaggio) < 0 then
	return -1
end if

if ld_costo_trasp = 0 or isnull(ld_costo_trasp) then
	return 0
end if

// carico aliquota IVA standard dal tipo dettaglio
select cod_iva
into   :ls_cod_iva
from   tab_tipi_det_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_det_ven = :ls_cod_tipo_det_ven_trasporto;
  
if (sqlca.sqlcode <>0) or (isnull(ls_cod_iva)) then
	fs_messaggio = "Errore nella select di tab_tipi_det_ven: " + sqlca.sqlerrtext
	return -1
end if

// Cerco cliente o contatto e verifico se ente IVA

ls_sql = " select cod_cliente from " + ls_tabella_testata + " " + &
				" where cod_azienda = '"+ s_cs_xx.cod_azienda + "' and " + &
				" anno_registrazione = "+ string(il_anno_registrazione) + " and " + &
				" num_registrazione = "+ string(il_num_registrazione)

declare cu_testata dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic cu_testata;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella open del cursore cu_testata: " + sqlca.sqlerrtext
	return -1
end if

fetch cu_testata into :ls_cod_cliente;
	
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella fetch del cursore cu_testata: " + sqlca.sqlerrtext
	close cu_testata;
	return -1
end if

close cu_testata;

setnull(ls_cod_contatto)

if is_tipo_documento = "off_ven" and isnull(ls_cod_cliente) then
	
	select cod_contatto
	into   :ls_cod_contatto
	from   tes_off_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di tes_off_ven: "  + sqlca.sqlerrtext
		return -1
	end if
	
end if

if isnull(ls_cod_contatto) and isnull(ls_cod_cliente) then
	fs_messaggio = "Cliente o contatto non indicato: impossibile proseguire"
	return -1
end if

if isnull(ls_cod_cliente) then
	
	// la bolla o la fattura riguardano un fornitore; caso non previsto quindi esco dal calcolo trasporto.
	if is_tipo_documento = "bol_ven" or is_tipo_documento = "fat_ven" then
		return 0
	end if
	
	select cod_iva
	into   :ls_cod_iva_cli
	from   anag_contatti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_contatto = :ls_cod_contatto;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di anag_contatti: " + sqlca.sqlerrtext
		return -1
	end if		 
			 
else  
	
	select cod_iva
	into   :ls_cod_iva_cli
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cliente = :ls_cod_cliente;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di anag_clienti: " + sqlca.sqlerrtext
		return -1
	end if		 
			 
end if

// se il cliente è esente impost il codice di esenzione del dettaglio.

if not isnull(ls_cod_iva_cli) then
	ls_cod_iva = ls_cod_iva_cli	// se cliente è esente iva
end if	

ls_sql = "select max(" + ls_colonna_prog + ") " + &
	" from " +  ls_tabella_dettaglio + " " + &
	" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
	" anno_registrazione = " + string(il_anno_registrazione) + " and " + &
	" num_registrazione = " + string(il_num_registrazione) 
	
declare cu_dettaglio dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic cu_dettaglio;

if sqlca.sqlcode = -1 then
	fs_messaggio = "Errore nella open del cursore cu_dettaglio: " + sqlca.sqlerrtext
end if	

fetch cu_dettaglio into :ll_prog_riga_doc_ven;

if sqlca.sqlcode = -1 then
	fs_messaggio = "Errore nella fetch del cursore cu_dettaglio: " + sqlca.sqlerrtext
end if

close cu_dettaglio;

if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore lettura numero riga dettaglio: " + sqlca.sqlerrtext
	return -1
end if

ll_prog_riga_doc_ven = ll_prog_riga_doc_ven + 10

ls_sql_stringa = "insert into " + ls_tabella_dettaglio + "( cod_azienda,   anno_registrazione,  num_registrazione, " + ls_colonna_prog + " , cod_tipo_det_ven, des_prodotto, " + ls_colonna_quan + ", prezzo_vendita, cod_iva "

if is_tipo_documento <> "fat_ven" and is_tipo_documento <> "bol_ven" then
	ls_sql_stringa = ls_sql_stringa + ", val_riga) "
else
	ls_sql_stringa = ls_sql_stringa + ") "
end if

ls_sql_stringa = ls_sql_stringa + " values ( '" + s_cs_xx.cod_azienda + "', " + string(il_anno_registrazione) + ", " + string(il_num_registrazione) + ", " + 	 string(ll_prog_riga_doc_ven) + ", '" + ls_cod_tipo_det_ven_trasporto + "', 'Spese Trasporto', " + 	 " 1, " + f_double_string(ld_costo_trasp) + ", '" + ls_cod_iva + "'"

if is_tipo_documento <> "fat_ven" and is_tipo_documento <> "bol_ven" then
	ls_sql_stringa = ls_sql_stringa + ", " + 	 f_double_string(ld_costo_trasp) + " )"
else
	ls_sql_stringa = ls_sql_stringa + ") "
end if

execute immediate :ls_sql_stringa ;

if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore in inserimento spesa trasporto in dettaglio: " + sqlca.sqlerrtext
	return -1
end if

return 0

end function

public function integer uof_spese_bancarie (ref string fs_messaggio);string ls_cod_pagamento, ls_cod_valuta, ls_cod_cliente, ls_flag_calcolo, ls_cod_lingua, ls_flag_spese_bancarie, ls_cod_tipo_det_ven, &
       ls_des_spese_bancarie, ls_messaggio, ls_stringa, ls_str, ls_cod_iva, ls_flag_tipo_pagamento, ls_cod_tipo_doc_ven, &
		 ls_cod_fornitore, ls_flag_tipo_doc_ven, ls_cod_contatto
		 
long   ll_prog_riga_fat_ven

dec{4} ld_importo_per_rata, ld_importo_forfait, ld_perc_valore_fattura, ld_num_rate_com, ld_tot_fattura_valuta,&
		 ld_tot_spese_bancarie

datetime ldt_data_esenzione_iva, ldt_data_registrazione


select  cod_pagamento,
          cod_valuta,
		 cod_cliente,
		 cod_fornitore,
		 cod_contatto,
		 flag_calcolo,
		 data_registrazione,
		 cod_tipo_fat_ven
into		:ls_cod_pagamento,
			:ls_cod_valuta,
			:ls_cod_cliente,
			:ls_cod_fornitore,
			:ls_cod_contatto,
			:ls_flag_calcolo,
			:ldt_data_registrazione,
			:ls_cod_tipo_doc_ven
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
	    anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione;
 
if sqlca.sqlcode = 100 then
	fs_messaggio = "La fattura specificata non esiste"
	return -1
elseif sqlca.sqlcode <> 0 and sqlca.sqlcode <> 100 then
	fs_messaggio = "Errore nella select di tes_fat_ven: " + sqlca.sqlerrtext
	return -1
end if

select flag_tipo_fat_ven
into   :ls_flag_tipo_doc_ven
from   tab_tipi_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
	    cod_tipo_fat_ven = :ls_cod_tipo_doc_ven;
 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di tab_tipi_fat_ven: " + sqlca.sqlerrtext
	return -1
end if

// segnalato da Progettotenda 16/05/2011
// su nota di credito si deve saltare il calcolo spese bancarie
if ls_flag_tipo_doc_ven = "N" then return 0


if ls_flag_tipo_doc_ven <> "F" then
	
	//se in questo IF il campo cliente è vuoto, molto probabilmente la fattura è una PROFORMA a contatto (anag_contatti)
	//quindi a seconda dei casi leggiamo da anag_clienti o da anag_contatti
	
	if not isnull(ls_cod_cliente) and ls_cod_cliente<>"" then
	
		select cod_lingua
		into   :ls_cod_lingua
		from   anag_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_cliente = :ls_cod_cliente;
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella select di anag_clienti: " + sqlca.sqlerrtext
			return -1
		end if
		
	elseif not isnull(ls_cod_contatto) and ls_cod_contatto<>"" then
		
		select cod_lingua
		into   :ls_cod_lingua
		from   anag_contatti
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_contatto = :ls_cod_contatto;
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella select di anag_contatti: " + sqlca.sqlerrtext
			return -1
		end if
		
	else
		fs_messaggio = "Il tipo fattura richiede il campo cliente (o al più il campo contatto, ma solo se Proforma)!"
		return -1
	end if
		
	
else
	
	select cod_lingua
	into   :ls_cod_lingua
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_fornitore = :ls_cod_fornitore;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di anag_fornitori: " + sqlca.sqlerrtext
		return -1
	end if
	
end if

if not isnull(ls_cod_lingua) then
	
	select cod_lingua
	into   :ls_cod_lingua
	from   tab_lingue
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_lingua = :ls_cod_lingua;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di tab_lingue: " + sqlca.sqlerrtext
		return -1
	end if
	
end if

select num_rate_com,
       flag_spese_bancarie,   
       importo_per_rata,   
       importo_forfait,   
       perc_valore_fattura,   
       cod_tipo_det_ven,   
       des_spese_bancarie,
		 flag_tipo_pagamento
into   :ld_num_rate_com,
       :ls_flag_spese_bancarie,   
       :ld_importo_per_rata,   
       :ld_importo_forfait,   
       :ld_perc_valore_fattura,   
       :ls_cod_tipo_det_ven,   
       :ls_des_spese_bancarie,
		 :ls_flag_tipo_pagamento
from   tab_pagamenti  
where  cod_azienda = :s_cs_xx.cod_azienda  and  
       cod_pagamento = :ls_cod_pagamento;
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di tab_pagamenti: " + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_tipo_det_ven) and ls_flag_tipo_pagamento = "R"  and ls_flag_spese_bancarie = "S" then
	fs_messaggio = "Impostare il tipo dettaglio vendita per riga automatica spese bancarie"
	return -1
end if

if ls_flag_spese_bancarie = "N" then
	return 0
end if	

if ld_perc_valore_fattura <> 0 and not isnull(ld_perc_valore_fattura) then
	
	select tot_fattura_valuta
	into   :ld_tot_fattura_valuta
	from   tes_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di tes_fat_ven: " + sqlca.sqlerrtext
		return -1
	end if
	
end if

if not isnull(ls_cod_valuta) then
	
	select importo_per_rata,   
			 importo_forfait  
	into   :ld_importo_per_rata,   
			 :ld_importo_forfait  
	from   tab_pagamenti_valute
	where  cod_azienda   = :s_cs_xx.cod_azienda  and  
			 cod_pagamento = :ls_cod_pagamento and
			 cod_valuta    = :ls_cod_valuta;
			 
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Non sono stati impostati i valori in valuta delle spese bancarie: " + sqlca.sqlerrtext
		return -1
	elseif sqlca.sqlcode <> 0 and sqlca.sqlcode <> 100 then
		fs_messaggio = "Errore nela select di tab_pagamenti_valute: " + sqlca.sqlerrtext
		return -1
	end if
	
end if

if not isnull(ls_cod_lingua) and ls_flag_spese_bancarie = "S" then
	
	select des_spese_bancarie  
	into   :ls_des_spese_bancarie  
	from   tab_pagamenti_lingue 
	where  cod_azienda   = :s_cs_xx.cod_azienda  and  
			 cod_pagamento = :ls_cod_pagamento and
			 cod_lingua    = :ls_cod_lingua;
			 
	if sqlca.sqlcode = 100 then
		fs_messaggio = "Non è stata impostata la descrizione in lingua delle spese bancarie: " + sqlca.sqlerrtext
		return -1
	elseif sqlca.sqlcode <> 0 and sqlca.sqlcode <> 100 then
		fs_messaggio = "Errore nella select di tab_pagamenti_lingue: " + sqlca.sqlerrtext
		return -1
	end if
	
end if

if ld_importo_per_rata > 0 and not isnull(ld_importo_per_rata) then ld_tot_spese_bancarie = ld_tot_spese_bancarie + (ld_num_rate_com * ld_importo_per_rata)
if ld_importo_forfait > 0 and not isnull(ld_importo_forfait) then ld_tot_spese_bancarie = ld_tot_spese_bancarie + ld_importo_forfait
if ld_perc_valore_fattura > 0 and not isnull(ld_perc_valore_fattura) then ld_tot_spese_bancarie = ld_tot_spese_bancarie + ( (ld_tot_fattura_valuta / 100) * ld_perc_valore_fattura)

uof_arrotonda(ld_tot_spese_bancarie, id_precisione_valuta, "euro")

// Ricerca della prima riga libera nel documento
select max(prog_riga_fat_ven)
into   :ll_prog_riga_fat_ven
from   det_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione;
		 
if ll_prog_riga_fat_ven = 0 or isnull(ll_prog_riga_fat_ven) then
	ll_prog_riga_fat_ven = 10
else
	ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10
end if

// Ricerca di aliquota iva o esenzione del dettaglio  
// la variabile ls_cod_tipo_det_ven non può essere a null: già verificato sopra
select tab_tipi_det_ven.cod_iva  
into   :ls_str
from   tab_tipi_det_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di tab_tipi_det_ven: " + sqlca.sqlerrtext
	return -1
end if

if not isnull(ls_str) then
	ls_cod_iva = ls_str
end if	

select cod_iva,
		 data_esenzione_iva
into   :ls_str,
		 :ldt_data_esenzione_iva
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_cliente = :ls_cod_cliente;
		 
if sqlca.sqlcode = 0 then
	if not isnull(ls_str) and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
		ls_cod_iva = ls_str
	end if	
end if

// Inserimento della riga di spese bancarie nel dettaglio documento

insert into det_fat_ven  
         ( cod_azienda,   
           anno_registrazione,   
           num_registrazione,   
           prog_riga_fat_ven,   
           cod_tipo_det_ven,   
           cod_deposito,   
           cod_ubicazione,   
           cod_lotto,   
           data_stock,   
           progr_stock,   
           cod_misura,   
           cod_prodotto,   
           des_prodotto,   
           quan_fatturata,   
           prezzo_vendita,   
           sconto_1,   
           sconto_2,   
           provvigione_1,   
           provvigione_2,   
           cod_iva,   
           cod_tipo_movimento,   
           num_registrazione_mov_mag,   
           anno_registrazione_fat,   
           num_registrazione_fat,   
           nota_dettaglio,   
           anno_registrazione_bol_ven,   
           num_registrazione_bol_ven,   
           prog_riga_bol_ven,   
           anno_commessa,   
           num_commessa,   
           cod_centro_costo,   
           sconto_3,   
           sconto_4,   
           sconto_5,   
           sconto_6,   
           sconto_7,   
           sconto_8,   
           sconto_9,   
           sconto_10,   
           anno_registrazione_mov_mag,   
           fat_conversione_ven,   
           anno_reg_des_mov,   
           num_reg_des_mov,   
           anno_reg_ord_ven,   
           num_reg_ord_ven,   
           prog_riga_ord_ven,   
           cod_versione,   
           num_confezioni,   
           num_pezzi_confezione,
		  ordinamento)  
  values ( :s_cs_xx.cod_azienda,   
           :il_anno_registrazione,   
           :il_num_registrazione,   
           :ll_prog_riga_fat_ven,   
           :ls_cod_tipo_det_ven,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           :ls_des_spese_bancarie,   
           1,   
           :ld_tot_spese_bancarie,   
           0,   
           0,   
           0,   
           0,   
           :ls_cod_iva,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           0,   
           null,   
           1,   
           null,   
           null,   
           null,   
           null,   
           null,   
           null,   
           0,   
           0,
		   :ll_prog_riga_fat_ven)  ;
			  
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella insert di det_fat_ven: " + sqlca.sqlerrtext
	return -1
end if

return 0

end function

public function integer uof_totalizza_parziali (ref string fs_messaggio);
choose case is_tipo_documento

	case "bol_ven"
	
		update tes_bol_ven
		set    tot_sconti_commerciali = :id_sconto_commerciale,
				 tot_sconto_cassa = :id_sconto_cassa,
				 tot_spese_varie = :id_spese_varie,
				 tot_spese_bolli = :id_spese_bolli,
				 tot_spese_imballo = :id_spese_imballo,
				 tot_spese_trasporto = :id_spese_trasporto,
				 tot_merci = :id_merci
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_bol_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "bol_acq"
	
		update tes_bol_acq
		set    tot_sconti_commerciali = :id_sconto_commerciale,
				 tot_spese_cassa = :id_sconto_cassa,
				 tot_spese_varie = :id_spese_varie,
				 tot_spese_bolli = :id_spese_bolli,
				 tot_spese_imballo = :id_spese_imballo,
				 tot_spese_trasporto = :id_spese_trasporto,
				 tot_merci = :id_merci
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_bolla_acq = :il_anno_registrazione and
				 num_bolla_acq = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_bol_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "fat_ven"
	
		update tes_fat_ven
		set    tot_sconti_commerciali = :id_sconto_commerciale,
				 tot_sconto_cassa = :id_sconto_cassa,
				 tot_spese_varie = :id_spese_varie,
				 tot_spese_bolli = :id_spese_bolli,
				 tot_spese_imballo = :id_spese_imballo,
				 tot_spese_trasporto = :id_spese_trasporto,
				 tot_merci = :id_merci
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "fat_acq"
	
		update tes_fat_acq
		set    tot_sconti_commerciali = :id_sconto_commerciale,
				 tot_spese_cassa = :id_sconto_cassa,
				 tot_spese_varie = :id_spese_varie,
				 tot_spese_bolli = :id_spese_bolli,
				 tot_spese_imballo = :id_spese_imballo,
				 tot_spese_trasporto = :id_spese_trasporto,
				 tot_merci = :id_merci
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "off_ven"
	
		update tes_off_ven
		set    tot_sconti_commerciali = :id_sconto_commerciale,
				 tot_sconto_cassa = :id_sconto_cassa,
				 tot_spese_varie = :id_spese_varie,
				 tot_spese_bolli = :id_spese_bolli,
				 tot_spese_imballo = :id_spese_imballo,
				 tot_spese_trasporto = :id_spese_trasporto,
				 tot_merci = :id_merci
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_off_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "off_acq"
	
		update tes_off_acq
		set    tot_sconti_commerciali = :id_sconto_commerciale,
				 tot_spese_cassa = :id_sconto_cassa,
				 tot_spese_varie = :id_spese_varie,
				 tot_spese_bolli = :id_spese_bolli,
				 tot_spese_imballo = :id_spese_imballo,
				 tot_spese_trasporto = :id_spese_trasporto,
				 tot_merci = :id_merci
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_off_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "ord_ven"
	
		update tes_ord_ven
		set    tot_sconti_commerciali = :id_sconto_commerciale,
				 tot_sconto_cassa = :id_sconto_cassa,
				 tot_spese_varie = :id_spese_varie,
				 tot_spese_bolli = :id_spese_bolli,
				 tot_spese_imballo = :id_spese_imballo,
				 tot_spese_trasporto = :id_spese_trasporto,
				 tot_merci = :id_merci
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "ord_acq"
	
		update tes_ord_acq
		set    tot_sconti_commerciali = :id_sconto_commerciale,
				 tot_spese_cassa = :id_sconto_cassa,
				 tot_spese_varie = :id_spese_varie,
				 tot_spese_bolli = :id_spese_bolli,
				 tot_spese_imballo = :id_spese_imballo,
				 tot_spese_trasporto = :id_spese_trasporto,
				 tot_merci = :id_merci
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_ord_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
end choose

return 0
end function

public function integer uof_calcola_imponibile_riga (ref string fs_messaggio);dec{4} ld_quantita, ld_prezzo_ven, ld_prezzo_ven_valuta, ld_imponibile_riga, ld_imponibile_riga_valuta,&
       ld_sconto_cassa, ld_sconto_cassa_valuta, ld_sconto_commerciale, ld_sconto_commerciale_valuta,&
		 ld_valore_parziale, ld_valore_parziale_valuta, ld_sconto, ld_sconto_pagamento, ld_sconto_testata
		 
long   ll_prog_riga, ll_i, ll_j

string ls_cod_pagamento, ls_sconto, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_flag_sola_iva, &
       ls_flag_imponibile_forzato


id_sconto_commerciale = 0
id_sconto_cassa = 0
id_spese_varie = 0
id_spese_bolli = 0
id_spese_imballo = 0
id_spese_trasporto = 0
id_merci = 0
ls_flag_imponibile_forzato = "N"

// Lettura di cod_pagamento e dello sconto specificato in testata
choose case is_tipo_documento
	
   case "bol_ven"
	
		select cod_pagamento,   
     			 sconto
		into   :ls_cod_pagamento,   
      		 :ld_sconto_testata
		from   tes_bol_ven
		where  cod_azienda        = :s_cs_xx.cod_azienda and
     			 anno_registrazione = :il_anno_registrazione and
		 		 num_registrazione  = :il_num_registrazione ;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella select di tes_bol_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "bol_acq"
	
		select cod_pagamento,   
     			 sconto
		into   :ls_cod_pagamento,   
      		 :ld_sconto_testata
		from   tes_bol_acq
		where  cod_azienda        = :s_cs_xx.cod_azienda and
     			 anno_bolla_acq = :il_anno_registrazione and
		 		 num_bolla_acq  = :il_num_registrazione ;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella select di tes_bol_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "fat_ven"	

		select cod_pagamento,   
     			 sconto
		into   :ls_cod_pagamento,   
      		 :ld_sconto_testata
		from   tes_fat_ven
		where  cod_azienda        = :s_cs_xx.cod_azienda and
     			 anno_registrazione = :il_anno_registrazione and
		 		 num_registrazione  = :il_num_registrazione ;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella select di tes_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "fat_acq"	

		select cod_pagamento,   
     			 sconto
		into   :ls_cod_pagamento,   
      		 :ld_sconto_testata
		from   tes_fat_acq
		where  cod_azienda        = :s_cs_xx.cod_azienda and
     			 anno_registrazione = :il_anno_registrazione and
		 		 num_registrazione  = :il_num_registrazione ;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella select di tes_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "off_ven"
	
		select cod_pagamento,   
     			 sconto
		into   :ls_cod_pagamento,   
      		 :ld_sconto_testata
		from   tes_off_ven
		where  cod_azienda        = :s_cs_xx.cod_azienda and
     			 anno_registrazione = :il_anno_registrazione and
		 		 num_registrazione  = :il_num_registrazione ;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella select di tes_off_ven: " + sqlca.sqlerrtext
			return -1
		end if	
	
	case "off_acq"
	
		select cod_pagamento,   
     			 sconto
		into   :ls_cod_pagamento,   
      		 :ld_sconto_testata
		from   tes_off_acq
		where  cod_azienda        = :s_cs_xx.cod_azienda and
     			 anno_registrazione = :il_anno_registrazione and
		 		 num_registrazione  = :il_num_registrazione ;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella select di tes_off_acq: " + sqlca.sqlerrtext
			return -1
		end if	
	
	case "ord_ven"
		
		select cod_pagamento,   
     			 sconto
		into   :ls_cod_pagamento,   
      		 :ld_sconto_testata
		from   tes_ord_ven
		where  cod_azienda        = :s_cs_xx.cod_azienda and
     			 anno_registrazione = :il_anno_registrazione and
		 		 num_registrazione  = :il_num_registrazione ;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella select di tes_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "ord_acq"
		
		select cod_pagamento,   
     			 sconto
		into   :ls_cod_pagamento,   
      		 :ld_sconto_testata
		from   tes_ord_acq
		where  cod_azienda        = :s_cs_xx.cod_azienda and
     			 anno_registrazione = :il_anno_registrazione and
		 		 num_registrazione  = :il_num_registrazione ;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella select di tes_ord_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
end choose		

if isnull(ld_sconto_testata) then ld_sconto_testata = 0

// Lettura dello sconto eventualmente specificato per il tipo di pagamento in questione
select sconto
into   :ld_sconto_pagamento
from   tab_pagamenti
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
		 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode = -1 then
	fs_messaggio = "Errore nella select di tab_pagamenti: " + sqlca.sqlerrtext
	return -1
end if

if isnull(ld_sconto_pagamento) then ld_sconto_pagamento = 0

// Si scorrono le righe del documento per calcolarne l'imponibile in valuta e in euro
for ll_i = 1 to ids_righe_documento.rowcount()
	
	ld_sconto_commerciale_valuta = 0
	ld_sconto_commerciale = 0
	ld_sconto_cassa_valuta = 0
	ld_sconto_cassa = 0

	// Lettura del progressivo riga (per il successivo update della tabella)
	ll_prog_riga = ids_righe_documento.getitemnumber(ll_i, is_colonna_prog_riga)
	
	choose case is_tipo_documento
	
		case "bol_ven","fat_ven","off_ven","ord_ven"
			
			// Lettura del codice tipo dettaglio vendita
			ls_cod_tipo_det_ven = ids_righe_documento.getitemstring(ll_i, "cod_tipo_det_ven")
	
			select flag_tipo_det_ven,
					 flag_sola_iva
			into   :ls_flag_tipo_det_ven,
					 :ls_flag_sola_iva
			from   tab_tipi_det_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
					 
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella select di tab_tipi_det_ven: " + sqlca.sqlerrtext
				return -1
			end if

			// Lettura del prezzo di vendita (per valuta ed euro) della riga	
			ld_prezzo_ven_valuta = ids_righe_documento.getitemnumber(ll_i, "prezzo_vendita")
			if isnull(ld_prezzo_ven_valuta) then ld_prezzo_ven_valuta = 0
			ld_prezzo_ven = ld_prezzo_ven_valuta * id_cambio
			
		case "bol_acq","fat_acq","off_acq","ord_acq"
			
			// Lettura del codice tipo dettaglio acquisto
			ls_cod_tipo_det_ven = ids_righe_documento.getitemstring(ll_i, "cod_tipo_det_acq")
	
			select flag_tipo_det_acq,
					 flag_sola_iva
			into   :ls_flag_tipo_det_ven,
					 :ls_flag_sola_iva
			from   tab_tipi_det_acq
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_det_acq = :ls_cod_tipo_det_ven;
					 
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella select di tab_tipi_det_acq: " + sqlca.sqlerrtext
				return -1
			end if

			// Lettura del prezzo di acquisto (per valuta ed euro) della riga	
			ld_prezzo_ven_valuta = ids_righe_documento.getitemnumber(ll_i, "prezzo_acquisto")
			if isnull(ld_prezzo_ven_valuta) then ld_prezzo_ven_valuta = 0
			ld_prezzo_ven = ld_prezzo_ven_valuta * id_cambio
			
			if is_tipo_documento	= "fat_acq" then
				ls_flag_imponibile_forzato = ids_righe_documento.getitemstring(ll_i, "flag_imponibile_forzato")
			end if
	
	end choose	

	// Se è una fattura di acquisto con imponibile forzato, salto calcolo riga.
	if ls_flag_imponibile_forzato = "S" then
		
		ld_imponibile_riga_valuta = ids_righe_documento.getitemnumber(ll_i, "imponibile_iva_valuta")
		ld_imponibile_riga = ld_imponibile_riga_valuta * id_cambio
		
		// se è stato impostato un cambio arrotondo in automatico; se il cambio è 1 tengo tutto come è
		if id_cambio <> 1 then
			uof_arrotonda(ld_imponibile_riga, id_precisione, "euro")
		end if
				
	else

		// Lettura di quantita della riga
		ld_quantita = ids_righe_documento.getitemnumber(ll_i, is_colonna_quantita)	
		if isnull(ld_quantita) then ld_quantita = 0
	
		// Calcolo del valore riga
		ld_imponibile_riga_valuta = ld_quantita * ld_prezzo_ven_valuta
		
		// Arrotondamento del valore riga in valuta
		uof_arrotonda(ld_imponibile_riga_valuta, id_precisione_valuta, "euro")
		
		if id_cambio <> 1 then
			ld_imponibile_riga = ld_imponibile_riga_valuta * id_cambio
		else
			ld_imponibile_riga = ld_quantita * ld_prezzo_ven
		end if
		
		// Arrotondamento del valore riga in base alla valuta del sistema
		uof_arrotonda(ld_imponibile_riga, id_precisione, "euro")

	end if
	
	// Se la riga rappresenta uno sconto non vengono cercati ulteriori sconti per quella riga
	if ls_flag_tipo_det_ven <> 'S' then
		
		// Controllo  totale forzato in fattura di acquisto
		if ls_flag_imponibile_forzato <> "S" then
		
			// Si usano due variabili di appoggio per non perdere il valore originale di valore riga
			ld_valore_parziale_valuta = ld_imponibile_riga_valuta
			ld_valore_parziale = ld_imponibile_riga
	
			// Sul valore riga vengono applicati gli sconti (da sconto_1 a sconto_10)
			for ll_j = 1 to 10
			
				ls_sconto = "sconto_" + string(ll_j)
				ld_sconto = ids_righe_documento.getitemnumber(ll_i, ls_sconto)
			
				if ld_sconto = 0 or isnull(ld_sconto) then
					continue
				end if	
			
				// Si calcola il totale degli sconti di riga
				ld_sconto_commerciale_valuta = ld_sconto_commerciale_valuta + (ld_valore_parziale_valuta / 100 * ld_sconto)
				ld_sconto_commerciale = ld_sconto_commerciale + (ld_valore_parziale / 100 * ld_sconto)
				ld_valore_parziale_valuta = ld_valore_parziale_valuta - (ld_valore_parziale_valuta / 100 * ld_sconto)
				ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto)
		
			next	
		
			// Al totale degli sconti di riga viene applicato anche lo sconto di testata per ottenere lo sconto commerciale
			ld_sconto_commerciale_valuta = ld_sconto_commerciale_valuta + (ld_valore_parziale_valuta / 100 * ld_sconto_testata)
			ld_sconto_commerciale = ld_sconto_commerciale + (ld_valore_parziale / 100 * ld_sconto_testata)
			ld_valore_parziale_valuta = ld_valore_parziale_valuta - (ld_valore_parziale_valuta / 100 * ld_sconto_testata)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_testata)
			
			// Lo sconto commerciale viene arrotondato
			uof_arrotonda(ld_sconto_commerciale_valuta, id_precisione_valuta, "euro")
			uof_arrotonda(ld_sconto_commerciale, id_precisione, "euro")
			
			// Viene aggiornato il valore dello sconto commerciale
			id_sconto_commerciale = id_sconto_commerciale + ld_sconto_commerciale
					
			// Viene calolato lo sconto di pagamento		
			ld_sconto_cassa_valuta = (ld_valore_parziale_valuta / 100 * ld_sconto_pagamento)
			ld_sconto_cassa = (ld_valore_parziale / 100 * ld_sconto_pagamento)		
		
			// Lo sconto di pagamento viene arrotondato
			uof_arrotonda(ld_sconto_cassa_valuta, id_precisione_valuta, "euro")
			uof_arrotonda(ld_sconto_cassa, id_precisione, "euro")		
			
			// Viene aggiornato il valore dello sconto di pagamento
			id_sconto_cassa = id_sconto_cassa + ld_sconto_cassa
			
		end if		
		
		// Nella totalizzazione dei parziali non vengono considerati gli imponibili delle righe di sola iva
		if ls_flag_sola_iva <> 'S' then
		
			choose case ls_flag_tipo_det_ven
				
				case "V"
					
					id_spese_varie = id_spese_varie + ld_imponibile_riga
				
				case "B"
					
					id_spese_bolli = id_spese_bolli + ld_imponibile_riga
					
				case "I"
					
					id_spese_imballo = id_spese_imballo + ld_imponibile_riga
					
				case "T"
					
					id_spese_trasporto = id_spese_trasporto + ld_imponibile_riga
					
				case else
					
					id_merci = id_merci + ld_imponibile_riga
					
			end choose
			
		end if
		
		// Sul valore riga vengono applicati gli sconti per ottenere l'imponibile
		ld_imponibile_riga_valuta = ld_imponibile_riga_valuta - ld_sconto_commerciale_valuta - ld_sconto_cassa_valuta
		ld_imponibile_riga = ld_imponibile_riga - ld_sconto_commerciale - ld_sconto_cassa		
				
	else	
		
		// Viene aggiornato il valore dello sconto commerciale
		id_sconto_commerciale = id_sconto_commerciale + ld_imponibile_riga
		
		// Le righe di sconto dovranno avere imponibile negativo
		ld_imponibile_riga_valuta = ld_imponibile_riga_valuta * -1
		ld_imponibile_riga = ld_imponibile_riga * -1		
		
	end if
	
	// L'imponibile ottenuto in valuta ed in euro viene memorizzato nel database
	choose case is_tipo_documento
	
		case "bol_ven"
		
			update det_bol_ven
			set    imponibile_iva_valuta = :ld_imponibile_riga_valuta,
					 imponibile_iva = :ld_imponibile_riga
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione and
					 prog_riga_bol_ven = :ll_prog_riga;
				 
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nell'update di det_bol_ven: " + sqlca.sqlerrtext
				return -1
			end if
		
		case "bol_acq"
		
			update det_bol_acq
			set    imponibile_iva_valuta = :ld_imponibile_riga_valuta,
					 imponibile_iva = :ld_imponibile_riga
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_bolla_acq = :il_anno_registrazione and
					 num_bolla_acq = :il_num_registrazione and
					 prog_riga_bolla_acq = :ll_prog_riga;
				 
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nell'update di det_bol_acq: " + sqlca.sqlerrtext
				return -1
			end if
		
		case "fat_ven"
		
			update det_fat_ven
			set    imponibile_iva_valuta = :ld_imponibile_riga_valuta,
					 imponibile_iva = :ld_imponibile_riga
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione and
					 prog_riga_fat_ven = :ll_prog_riga;
				 
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nell'update di det_fat_ven: " + sqlca.sqlerrtext
				return -1
			end if	
		
		case "fat_acq"
		
			update det_fat_acq
			set    imponibile_iva_valuta = :ld_imponibile_riga_valuta,
					 imponibile_iva = :ld_imponibile_riga
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione and
					 prog_riga_fat_acq = :ll_prog_riga;
				 
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nell'update di det_fat_acq: " + sqlca.sqlerrtext
				return -1
			end if	
		
		case "off_ven"
		
			update det_off_ven
			set    imponibile_iva_valuta = :ld_imponibile_riga_valuta,
					 imponibile_iva = :ld_imponibile_riga
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione and
					 prog_riga_off_ven = :ll_prog_riga;
				 
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nell'update di det_off_ven: " + sqlca.sqlerrtext
				return -1
			end if
		
		case "off_acq"
		
			update det_off_acq
			set    imponibile_iva_valuta = :ld_imponibile_riga_valuta,
					 imponibile_iva = :ld_imponibile_riga
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione and
					 prog_riga_off_acq = :ll_prog_riga;
				 
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nell'update di det_off_acq: " + sqlca.sqlerrtext
				return -1
			end if
		
		case "ord_ven"

			update det_ord_ven
			set    imponibile_iva_valuta = :ld_imponibile_riga_valuta,
					 imponibile_iva = :ld_imponibile_riga
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione and
					 prog_riga_ord_ven = :ll_prog_riga;
					 
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nell'update di det_ord_ven: " + sqlca.sqlerrtext
				return -1
			end if
		
		case "ord_acq"

			update det_ord_acq
			set    imponibile_iva_valuta = :ld_imponibile_riga_valuta,
					 imponibile_iva = :ld_imponibile_riga
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione and
					 prog_riga_ordine_acq = :ll_prog_riga;
					 
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nell'update di det_ord_acq: " + sqlca.sqlerrtext
				return -1
			end if
		
	end choose
	
next

return 0
end function

public function integer uof_calcola_provvigioni (ref string fs_messaggio);dec{4} ld_imp_prov_1, ld_tot_prov_1, ld_imp_prov_2, ld_tot_prov_2


choose case is_tipo_documento
	
	case "bol_ven"
		
		// Calcolo provvigioni per l'agente_1
		select sum(imponibile_iva),
				 sum(imponibile_iva * provvigione_1 / 100)
		into   :ld_imp_prov_1,
				 :ld_tot_prov_1
		from   det_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
	 	 		 provvigione_1 <> 0;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_bol_ven: " + sqlca.sqlerrtext
			return -1
		end if

		uof_arrotonda(ld_tot_prov_1, id_precisione, "euro")

		// Calcolo provvigioni per l'agente_2
		select sum(imponibile_iva),
				 sum(imponibile_iva * provvigione_2 / 100)
		into   :ld_imp_prov_2,
				 :ld_tot_prov_2
		from   det_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 provvigione_2 <> 0;

		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_bol_ven: " + sqlca.sqlerrtext
			return -1
		end if

		uof_arrotonda(ld_tot_prov_2, id_precisione, "euro")

		// Aggiornamento in testata delle provvigioni calcolate
		update tes_bol_ven
		set    imponibile_provvigioni_1 = :ld_imp_prov_1,
				 tot_provvigioni_1 = :ld_tot_prov_1,
				 imponibile_provvigioni_2 = :ld_imp_prov_2,
				 tot_provvigioni_2 = :ld_tot_prov_2
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_bol_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
	case "fat_ven"
		
		// Calcolo provvigioni per l'agente_1
		select sum(imponibile_iva),
				 sum(imponibile_iva * provvigione_1 / 100)
		into   :ld_imp_prov_1,
				 :ld_tot_prov_1
		from   det_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
	 	 		 provvigione_1 <> 0;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if

		uof_arrotonda(ld_tot_prov_1, id_precisione, "euro")

		// Calcolo provvigioni per l'agente_2
		select sum(imponibile_iva),
				 sum(imponibile_iva * provvigione_2 / 100)
		into   :ld_imp_prov_2,
				 :ld_tot_prov_2
		from   det_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 provvigione_2 <> 0;

		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if

		uof_arrotonda(ld_tot_prov_2, id_precisione, "euro")

		// Aggiornamento in testata delle provvigioni calcolate
		update tes_fat_ven
		set    imponibile_provvigioni_1 = :ld_imp_prov_1,
				 tot_provvigioni_1 = :ld_tot_prov_1,
				 imponibile_provvigioni_2 = :ld_imp_prov_2,
				 tot_provvigioni_2 = :ld_tot_prov_2
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if


	case "off_ven"
				
		// Calcolo provvigioni per l'agente_1
		select sum(imponibile_iva),
				 sum(imponibile_iva * provvigione_1 / 100)
		into   :ld_imp_prov_1,
				 :ld_tot_prov_1
		from   det_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
	 	 		 provvigione_1 <> 0;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_off_ven: " + sqlca.sqlerrtext
			return -1
		end if

		uof_arrotonda(ld_tot_prov_1, id_precisione, "euro")

		// Calcolo provvigioni per l'agente_2
		select sum(imponibile_iva),
				 sum(imponibile_iva * provvigione_2 / 100)
		into   :ld_imp_prov_2,
				 :ld_tot_prov_2
		from   det_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 provvigione_2 <> 0;

		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_off_ven: " + sqlca.sqlerrtext
			return -1
		end if

		uof_arrotonda(ld_tot_prov_2, id_precisione, "euro")

		// Aggiornamento in testata delle provvigioni calcolate
		update tes_off_ven
		set    imponibile_provvigioni_1 = :ld_imp_prov_1,
				 tot_provvigioni_1 = :ld_tot_prov_1,
				 imponibile_provvigioni_2 = :ld_imp_prov_2,
				 tot_provvigioni_2 = :ld_tot_prov_2
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_off_ven: " + sqlca.sqlerrtext
			return -1
		end if

	
	case "ord_ven"
		
		// Calcolo provvigioni per l'agente_1
		select sum(imponibile_iva),
				 sum(imponibile_iva * provvigione_1 / 100)
		into   :ld_imp_prov_1,
				 :ld_tot_prov_1
		from   det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
	 	 		 provvigione_1 <> 0;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if

		uof_arrotonda(ld_tot_prov_1, id_precisione, "euro")

		// Calcolo provvigioni per l'agente_2
		select sum(imponibile_iva),
				 sum(imponibile_iva * provvigione_2 / 100)
		into   :ld_imp_prov_2,
				 :ld_tot_prov_2
		from   det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 provvigione_2 <> 0;

		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if

		uof_arrotonda(ld_tot_prov_2, id_precisione, "euro")

		// Aggiornamento in testata delle provvigioni calcolate
		update tes_ord_ven
		set    imponibile_provvigioni_1 = :ld_imp_prov_1,
				 tot_provvigioni_1 = :ld_tot_prov_1,
				 imponibile_provvigioni_2 = :ld_imp_prov_2,
				 tot_provvigioni_2 = :ld_tot_prov_2
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if

end choose

return 0
end function

public function integer uof_azzera_documento (ref string fs_messaggio);choose case is_tipo_documento
		
	case "fat_ven"	

		delete
		from   cont_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
		 		 num_registrazione = :il_num_registrazione;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di cont_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
	case "fat_acq"
		
		delete
		from   cont_fat_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
		 		 num_registrazione = :il_num_registrazione;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di cont_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if		
		
end choose

choose case is_tipo_documento
	
	case "bol_ven"		
		
		update tes_bol_ven
		set    imponibile_provvigioni_1 = 0,
				 tot_provvigioni_1 = 0,
				 imponibile_provvigioni_2 = 0,
				 tot_provvigioni_2 = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_bol_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
	case "fat_ven"		
		
		update tes_fat_ven
		set    imponibile_provvigioni_1 = 0,
				 tot_provvigioni_1 = 0,
				 imponibile_provvigioni_2 = 0,
				 tot_provvigioni_2 = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if


	case "off_ven"
				
		update tes_off_ven
		set    imponibile_provvigioni_1 = 0,
				 tot_provvigioni_1 = 0,
				 imponibile_provvigioni_2 = 0,
				 tot_provvigioni_2 = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_off_ven: " + sqlca.sqlerrtext
			return -1
		end if

	
	case "ord_ven"
		
		update tes_ord_ven
		set    imponibile_provvigioni_1 = 0,
				 tot_provvigioni_1 = 0,
				 imponibile_provvigioni_2 = 0,
				 tot_provvigioni_2 = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if

end choose

choose case is_tipo_documento
		
	case "fat_ven"	
		
		delete from scad_fat_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :il_anno_registrazione and
				num_registrazione = :il_num_registrazione;

		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di scad_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if		
		
	case "fat_acq"
		
		delete from scad_fat_acq
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :il_anno_registrazione and
				num_registrazione = :il_num_registrazione;

		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di scad_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
end choose

choose case is_tipo_documento
	
	case "bol_ven"
	
		update tes_bol_ven
		set	 imponibile_iva_valuta = 0,
				 importo_iva_valuta = 0,
				 tot_val_bolla_valuta = 0,
				 imponibile_iva = 0,
				 importo_iva = 0,
				 tot_val_bolla = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_bol_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "bol_acq"
	
		update tes_bol_acq
		set	 imponibile_valuta_iva = 0,
				 importo_valuta_iva = 0,
				 tot_val_bol_acq = 0,
				 imponibile_iva = 0,
				 importo_iva = 0,
				 tot_valuta_bol_acq = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_bolla_acq = :il_anno_registrazione and
				 num_bolla_acq = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_bol_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "fat_ven"	
		
		update tes_fat_ven
		set	 imponibile_iva_valuta = 0,
				 importo_iva_valuta = 0,
				 tot_fattura_valuta = 0,
				 imponibile_iva = 0,
				 importo_iva = 0,
				 tot_fattura = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if	 
	
	case "fat_acq"	
		
		update tes_fat_acq
		set	 imponibile_valuta_iva = 0,
				 importo_valuta_iva = 0,
				 tot_valuta_fat_acq = 0,
				 imponibile_iva = 0,
				 importo_iva = 0,
				 tot_val_fat_acq = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if	 
	
	case "off_ven"
		
		update tes_off_ven
		set	 imponibile_iva_valuta = 0,
				 importo_iva_valuta = 0,
				 tot_val_offerta_valuta = 0,
				 imponibile_iva = 0,
				 importo_iva = 0,
				 tot_val_offerta = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_off_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "off_acq"
		
		update tes_off_acq
		set	 imponibile_iva_valuta = 0,
				 importo_iva_valuta = 0,
				 tot_val_offerta_valuta = 0,
				 imponibile_iva = 0,
				 importo_iva = 0,
				 tot_val_offerta = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_off_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "ord_ven"
		
		update tes_ord_ven
		set	 imponibile_iva_valuta = 0,
				 importo_iva_valuta = 0,
				 tot_val_ordine_valuta = 0,
				 imponibile_iva = 0,
				 importo_iva = 0,
				 tot_val_ordine = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "ord_acq"
		
		update tes_ord_acq
		set	 imponibile_iva_valuta = 0,
				 importo_iva_valuta = 0,
				 tot_val_ordine_valuta = 0,
				 imponibile_iva = 0,
				 importo_iva = 0,
				 tot_val_ordine = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_ord_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
end choose

choose case is_tipo_documento
	
  	case "bol_ven"
	
		delete
		from   iva_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_bol_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "bol_acq"
	
		delete
		from   iva_bol_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_bolla_acq = :il_anno_registrazione and
				 num_bolla_acq = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_bol_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "fat_ven"	

		delete
		from   iva_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "fat_acq"	

		delete
		from   iva_fat_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "off_ven"
	
		delete
		from   iva_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_off_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "off_acq"
	
		delete
		from   iva_off_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_off_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "ord_ven"
	
		delete
		from   iva_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "ord_acq"
	
		delete
		from   iva_ord_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_ord_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
end choose

choose case is_tipo_documento

	case "bol_ven"
	
		update tes_bol_ven
		set    tot_sconti_commerciali = 0,
				 tot_sconto_cassa = 0,
				 tot_spese_varie = 0,
				 tot_spese_bolli = 0,
				 tot_spese_imballo = 0,
				 tot_spese_trasporto = 0,
				 tot_merci = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_bol_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "bol_acq"
	
		update tes_bol_acq
		set    tot_sconti_commerciali = 0,
				 tot_spese_cassa = 0,
				 tot_spese_varie = 0,
				 tot_spese_bolli = 0,
				 tot_spese_imballo = 0,
				 tot_spese_trasporto = 0,
				 tot_merci = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_bolla_acq = :il_anno_registrazione and
				 num_bolla_acq = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_bol_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "fat_ven"
	
		update tes_fat_ven
		set    tot_sconti_commerciali = 0,
				 tot_sconto_cassa = 0,
				 tot_spese_varie = 0,
				 tot_spese_bolli = 0,
				 tot_spese_imballo = 0,
				 tot_spese_trasporto = 0,
				 tot_merci = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "fat_acq"
	
		update tes_fat_acq
		set    tot_sconti_commerciali = 0,
				 tot_spese_cassa = 0,
				 tot_spese_varie = 0,
				 tot_spese_bolli = 0,
				 tot_spese_imballo = 0,
				 tot_spese_trasporto = 0,
				 tot_merci = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "off_ven"
	
		update tes_off_ven
		set    tot_sconti_commerciali = 0,
				 tot_sconto_cassa = 0,
				 tot_spese_varie = 0,
				 tot_spese_bolli = 0,
				 tot_spese_imballo = 0,
				 tot_spese_trasporto = 0,
				 tot_merci = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_off_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "off_acq"
	
		update tes_off_acq
		set    tot_sconti_commerciali = 0,
				 tot_spese_cassa = 0,
				 tot_spese_varie = 0,
				 tot_spese_bolli = 0,
				 tot_spese_imballo = 0,
				 tot_spese_trasporto = 0,
				 tot_merci = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_off_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "ord_ven"
	
		update tes_ord_ven
		set    tot_sconti_commerciali = 0,
				 tot_sconto_cassa = 0,
				 tot_spese_varie = 0,
				 tot_spese_bolli = 0,
				 tot_spese_imballo = 0,
				 tot_spese_trasporto = 0,
				 tot_merci = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "ord_acq"
	
		update tes_ord_acq
		set    tot_sconti_commerciali = 0,
				 tot_spese_cassa = 0,
				 tot_spese_varie = 0,
				 tot_spese_bolli = 0,
				 tot_spese_imballo = 0,
				 tot_spese_trasporto = 0,
				 tot_merci = 0
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
			 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nell'update di tes_ord_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
end choose

return 0
end function

public function integer uof_fine_calcolo (ref string fs_messaggio);if is_tipo_documento = "fat_ven" then
		
	update tes_fat_ven
	set    flag_calcolo = 'S'
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione;
				 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nell'update di flag_calcolo in tes_fat_ven: " + sqlca.sqlerrtext
		return -1
	end if
	
end if

return 0
end function

public function integer uof_calcola_contropartite (ref string fs_messaggio);boolean lb_trovato

long    ll_i, ll_j, ll_pos, ll_prog_riga

dec{4}  ld_imp_riga_valuta, ld_imp_riga, ld_importo_cont[], ld_importo_cont_valuta[]

string  ls_cod_gruppo, ls_cod_prodotto, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_flag_tipo_cliente,&
		  ls_flag_tipo_fat_ven, ls_conto_contropartita, ls_cod_tipo_fat_ven, ls_conti[], ls_cod_cliente,&
		  ls_cod_fornitore, ls_flag_sola_iva, ls_cod_contatto
		  

choose case is_tipo_documento
		
	case "fat_ven"	

		if is_flag_cc_manuali <> "S" then
			delete
			from   cont_fat_ven_cc
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione;
			 
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella delete di cont_fat_ven_cc: " + sqlca.sqlerrtext
				return -1
			end if
		end if
		
		delete
		from   cont_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
		 		 num_registrazione = :il_num_registrazione;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di cont_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if

		select		cod_cliente,
					cod_fornitore,
					cod_contatto,
					cod_tipo_fat_ven
		into		:ls_cod_cliente,
					:ls_cod_fornitore,
					:ls_cod_contatto,
					:ls_cod_tipo_fat_ven
		from   tes_fat_ven
		where  cod_azienda        = :s_cs_xx.cod_azienda and
		       anno_registrazione = :il_anno_registrazione and
				 num_registrazione  = :il_num_registrazione;
		 
		if sqlca.sqlcode = -1 or sqlca.sqlcode = 100 then
			fs_messaggio = "Errore nella select di tes_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if

		select flag_tipo_fat_ven
		into   :ls_flag_tipo_fat_ven
		from   tab_tipi_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
		 
		if sqlca.sqlcode = -1 or sqlca.sqlcode = 100 then
			fs_messaggio = "Errore nella select di tab_tipi_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
	case "fat_acq"
		
		if is_flag_cc_manuali <> "S" then
			delete
			from   cont_fat_acq_cc
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione;
			 
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella delete di cont_fat_acq_cc: " + sqlca.sqlerrtext
				return -1
			end if
		end if
		
		delete
		from   cont_fat_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
		 		 num_registrazione = :il_num_registrazione;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di cont_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if

		select cod_fornitore,
				 cod_tipo_fat_acq
		into   :ls_cod_fornitore,
				 :ls_cod_tipo_fat_ven
		from   tes_fat_acq
		where  cod_azienda        = :s_cs_xx.cod_azienda and
		       anno_registrazione = :il_anno_registrazione and
				 num_registrazione  = :il_num_registrazione;
		 
		if sqlca.sqlcode = -1 or sqlca.sqlcode = 100 then
			fs_messaggio = "Errore nella select di tes_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
		setnull(ls_cod_cliente)
		setnull(ls_cod_contatto)

		select flag_tipo_fat_acq
		into   :ls_flag_tipo_fat_ven
		from   tab_tipi_fat_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_tipo_fat_acq = :ls_cod_tipo_fat_ven;
		 
		if sqlca.sqlcode = -1 or sqlca.sqlcode = 100 then
			fs_messaggio = "Errore nella select di tab_tipi_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
end choose		

if is_tipo_documento = "fat_ven" then
//		ls_flag_tipo_fat_ven <> "F" then
	
	if not isnull(ls_cod_cliente) and ls_cod_cliente<>"" then
	
		select flag_tipo_cliente
		into   :ls_flag_tipo_cliente
		from   anag_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente;
				 
		if sqlca.sqlcode = -1 or sqlca.sqlcode = 100 then
			fs_messaggio = "Errore nella select di anag_clienti: " + sqlca.sqlerrtext
			return -1
		end if
		
	else
		//cerca il tipo contatto da anag_contatti (colonna flag_tipo_cliente)
		select flag_tipo_cliente
		into   :ls_flag_tipo_cliente
		from   anag_contatti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_contatto = :ls_cod_contatto;
				 
		if sqlca.sqlcode = -1 or sqlca.sqlcode = 100 then
			fs_messaggio = "Errore nella select di anag_clienti: " + sqlca.sqlerrtext
			return -1
		end if
		
	end if
	
else
	
	select flag_tipo_fornitore
	into   :ls_flag_tipo_cliente
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_fornitore = :ls_cod_fornitore;
			 
	if sqlca.sqlcode = -1 or sqlca.sqlcode = 100 then
		fs_messaggio = "Errore nella select di anag_fornitori: " + sqlca.sqlerrtext
		return -1
	end if
	
end if

for ll_i = 1 to ids_righe_documento.rowcount()
	
	choose case is_tipo_documento
			
		case "fat_ven"
			
			ls_cod_tipo_det_ven = ids_righe_documento.getitemstring(ll_i, "cod_tipo_det_ven")
	
			select tab_tipi_det_ven.flag_tipo_det_ven,
					 tab_tipi_det_ven.flag_sola_iva
			into   :ls_flag_tipo_det_ven,
					 :ls_flag_sola_iva
			from   tab_tipi_det_ven
			where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella select tab_tipi_det_ven: " + sqlca.sqlerrtext
				return -1
			end if
			
		case "fat_acq"
			
			ls_cod_tipo_det_ven = ids_righe_documento.getitemstring(ll_i, "cod_tipo_det_acq")
	
			select tab_tipi_det_acq.flag_tipo_det_acq,
					 tab_tipi_det_acq.flag_sola_iva
			into   :ls_flag_tipo_det_ven,
					 :ls_flag_sola_iva
			from   tab_tipi_det_acq
			where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_ven;

			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella select tab_tipi_det_acq: " + sqlca.sqlerrtext
				return -1
			end if
			
	end choose		
	
	ld_imp_riga_valuta = ids_righe_documento.getitemnumber(ll_i, "imponibile_iva_valuta")

	if ld_imp_riga_valuta = 0 or isnull(ld_imp_riga_valuta) then
		continue
	end if
	
	ld_imp_riga = ids_righe_documento.getitemnumber(ll_i, "imponibile_iva")

	if ld_imp_riga = 0 or isnull(ld_imp_riga) then
		continue
	end if
	
	ll_prog_riga = ids_righe_documento.getitemnumber(ll_i, is_colonna_prog_riga)
	
	ls_cod_prodotto = ids_righe_documento.getitemstring(ll_i, "cod_prodotto")
		
	if ls_flag_sola_iva = 'S' then
		continue
	end if
	
	setnull(ls_cod_gruppo)
	
	if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" then
		
		choose case is_tipo_documento
				
			case "fat_ven"
		
				select anag_prodotti.cod_gruppo_ven  
				into   :ls_cod_gruppo  
				from   anag_prodotti  
				where (anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
						(anag_prodotti.cod_prodotto = :ls_cod_prodotto );
						
				if sqlca.sqlcode = -1 then
					fs_messaggio = "Errore nella select di anag_prodotti: " + sqlca.sqlerrtext
					return -1
				end if
				
			case "fat_acq"
				
				select anag_prodotti.cod_gruppo_acq
				into   :ls_cod_gruppo  
				from   anag_prodotti  
				where (anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda ) AND  
						(anag_prodotti.cod_prodotto = :ls_cod_prodotto );
						
				if sqlca.sqlcode = -1 then
					fs_messaggio = "Errore nella select di anag_prodotti: " + sqlca.sqlerrtext
					return -1
				end if
		
		end choose
	
	end if

	if isnull(ls_cod_gruppo) or len(ls_cod_gruppo) < 1 then
		
		choose case is_tipo_documento
				
			case "fat_ven"
				
				select cod_gruppo
				into   :ls_cod_gruppo
				from   tab_tipi_det_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
			 	       cod_tipo_det_ven = :ls_cod_tipo_det_ven;
				  
				if sqlca.sqlcode = -1 then
					fs_messaggio = "Errore nella select di tab_tipi_det_ven: " + sqlca.sqlerrtext
					return -1
				end if
				
			case "fat_acq"
				
				select cod_gruppo
				into   :ls_cod_gruppo
				from   tab_tipi_det_acq
				where  cod_azienda = :s_cs_xx.cod_azienda and
			 	       cod_tipo_det_acq = :ls_cod_tipo_det_ven;
				  
				if sqlca.sqlcode = -1 then
					fs_messaggio = "Errore nella select di tab_tipi_det_acq: " + sqlca.sqlerrtext
					return -1
				end if
				
		end choose		
		
	end if

	if isnull(ls_cod_gruppo) or len(ls_cod_gruppo) < 1 then
		
		choose case is_tipo_documento
				
			case "fat_ven"
				
				select cod_gruppo
				into   :ls_cod_gruppo
				from   con_fat_ven
				where  cod_azienda = :s_cs_xx.cod_azienda ;
		
				if sqlca.sqlcode = -1 then
					fs_messaggio = "Errore nella select di con_fat_ven: " + sqlca.sqlerrtext
					return -1
				end if
				
			case "fat_acq"
				
				select cod_gruppo
				into   :ls_cod_gruppo
				from   con_fat_acq
				where  cod_azienda = :s_cs_xx.cod_azienda ;
		
				if sqlca.sqlcode = -1 then
					fs_messaggio = "Errore nella select di con_fat_acq: " + sqlca.sqlerrtext
					return -1
				end if
				
		end choose		
	
		if isnull(ls_cod_gruppo) then
			fs_messaggio = "Impostare il codice gruppo in tabella controllo fatture"
			return -1
		end if
		
	end if

	choose case ls_flag_tipo_cliente
			
		case "E"
			
			choose case ls_flag_tipo_fat_ven
					
				case "N"
					
					choose case is_tipo_documento
							
						case "fat_ven"

							select cod_conto_ven_not_est
							into   :ls_conto_contropartita
							from   tab_gruppi
							where  cod_azienda = :s_cs_xx.cod_azienda and
					     		  	 cod_gruppo =  :ls_cod_gruppo ;
							 
							if sqlca.sqlcode <> 0 then
								fs_messaggio = "Errore in ricerca conto contropartita " + ls_cod_gruppo + " in tabella gruppi contabili"
								return -1
							end if
							
						case "fat_acq"
							
							select cod_conto_acq_not_est
							into   :ls_conto_contropartita
							from   tab_gruppi
							where  cod_azienda = :s_cs_xx.cod_azienda and
					     		  	 cod_gruppo =  :ls_cod_gruppo ;
							 
							if sqlca.sqlcode <> 0 then
								fs_messaggio = "Errore in ricerca conto contropartita " + ls_cod_gruppo + " in tabella gruppi contabili"
								return -1
							end if
							
					end choose		
					
				case else
					
					choose case is_tipo_documento
							
						case "fat_ven"	
					
							select cod_conto_ven_fat_est
							into   :ls_conto_contropartita
							from   tab_gruppi
							where  cod_azienda = :s_cs_xx.cod_azienda and
					     		  	 cod_gruppo =  :ls_cod_gruppo ;
							 
							if sqlca.sqlcode <> 0 then
								fs_messaggio = "Errore in ricerca conto contropartita " + ls_cod_gruppo + " in tabella gruppi contabili"
								return -1
							end if
							
						case "fat_acq"
							
							select cod_conto_acq_fat_est
							into   :ls_conto_contropartita
							from   tab_gruppi
							where  cod_azienda = :s_cs_xx.cod_azienda and
					     		  	 cod_gruppo =  :ls_cod_gruppo ;
							 
							if sqlca.sqlcode <> 0 then
								fs_messaggio = "Errore in ricerca conto contropartita " + ls_cod_gruppo + " in tabella gruppi contabili"
								return -1
							end if
							
					end choose		
					
			end choose
			
		case "C"
			
			choose case ls_flag_tipo_fat_ven
					
				case "N"
					
					choose case is_tipo_documento
							
						case "fat_ven"	
					
							select cod_conto_ven_not_cee
							into   :ls_conto_contropartita
							from   tab_gruppi
							where  cod_azienda = :s_cs_xx.cod_azienda and
					     		  	 cod_gruppo =  :ls_cod_gruppo ;
							 
							if sqlca.sqlcode <> 0 then
								fs_messaggio = "Errore in ricerca conto contropartita " + ls_cod_gruppo + " in tabella gruppi contabili"
								return -1
							end if
						
						case "fat_acq"
							
							select cod_conto_acq_not_cee
							into   :ls_conto_contropartita
							from   tab_gruppi
							where  cod_azienda = :s_cs_xx.cod_azienda and
					     		  	 cod_gruppo =  :ls_cod_gruppo ;
							 
							if sqlca.sqlcode <> 0 then
								fs_messaggio = "Errore in ricerca conto contropartita " + ls_cod_gruppo + " in tabella gruppi contabili"
								return -1
							end if
							
					end choose		
					
				case else
					
					choose case is_tipo_documento
							
						case "fat_ven"	
					
							select cod_conto_ven_fat_cee
							into   :ls_conto_contropartita
							from   tab_gruppi
							where  cod_azienda = :s_cs_xx.cod_azienda and
					       		 cod_gruppo =  :ls_cod_gruppo ;
							 
							if sqlca.sqlcode <> 0 then
								fs_messaggio = "Errore in ricerca conto contropartita " + ls_cod_gruppo + " in tabella gruppi contabili"
								return -1
							end if
							
						case "fat_acq"
							
							select cod_conto_acq_fat_cee
							into   :ls_conto_contropartita
							from   tab_gruppi
							where  cod_azienda = :s_cs_xx.cod_azienda and
					       		 cod_gruppo =  :ls_cod_gruppo ;
							 
							if sqlca.sqlcode <> 0 then
								fs_messaggio = "Errore in ricerca conto contropartita " + ls_cod_gruppo + " in tabella gruppi contabili"
								return -1
							end if
							
					end choose		
					
			end choose
			
		case else
			
			choose case ls_flag_tipo_fat_ven
					
				case "N"
					
					choose case is_tipo_documento
							
						case "fat_ven"
					
							select cod_conto_ven_not_ital
							into   :ls_conto_contropartita
							from   tab_gruppi
							where  cod_azienda = :s_cs_xx.cod_azienda and
					       		 cod_gruppo =  :ls_cod_gruppo ;
							 
							if sqlca.sqlcode <> 0 then
								fs_messaggio = "Errore in ricerca conto contropartita " + ls_cod_gruppo + " in tabella gruppi contabili"
								return -1
							end if
						
						case "fat_acq"
							
							select cod_conto_acq_not_ital
							into   :ls_conto_contropartita
							from   tab_gruppi
							where  cod_azienda = :s_cs_xx.cod_azienda and
					       		 cod_gruppo =  :ls_cod_gruppo ;
							 
							if sqlca.sqlcode <> 0 then
								fs_messaggio = "Errore in ricerca conto contropartita " + ls_cod_gruppo + " in tabella gruppi contabili"
								return -1
							end if
							
					end choose		
					
				case else
					
					choose case is_tipo_documento
					
						case "fat_ven"
							
							select cod_conto_ven_fat_ital
							into   :ls_conto_contropartita
							from   tab_gruppi
							where  cod_azienda = :s_cs_xx.cod_azienda and
					       		 cod_gruppo =  :ls_cod_gruppo ;
							 
							if sqlca.sqlcode <> 0 then
								fs_messaggio = "Errore in ricerca conto contropartita " + ls_cod_gruppo + " in tabella gruppi contabili"
								return -1
							end if
							
						case "fat_acq"
							
							select cod_conto_acq_fat_ital
							into   :ls_conto_contropartita
							from   tab_gruppi
							where  cod_azienda = :s_cs_xx.cod_azienda and
					       		 cod_gruppo =  :ls_cod_gruppo ;
							 
							if sqlca.sqlcode <> 0 then
								fs_messaggio = "Errore in ricerca conto contropartita " + ls_cod_gruppo + " in tabella gruppi contabili"
								return -1
							end if
							
					end choose		
					
			end choose
			
	end choose

	if isnull(ls_conto_contropartita) then
		fs_messaggio = "Errore durante totalizzazione contropartite: nessun conto associato al gruppo "
		return -1
	end if

	lb_trovato = false

	for ll_j = 1 to upperbound(ls_conti)
		
		if ls_conti[ll_j] = ls_conto_contropartita then
			lb_trovato = true
			ld_importo_cont_valuta[ll_j]  = ld_importo_cont_valuta[ll_j] + ld_imp_riga_valuta
			ld_importo_cont[ll_j] = ld_importo_cont[ll_j] + ld_imp_riga
			exit
		end if	
				
	next
	
	if lb_trovato = false then		
		ll_pos = upperbound(ls_conti) + 1
		ls_conti[ll_pos] = ls_conto_contropartita
		ld_importo_cont_valuta[ll_pos]  = ld_imp_riga_valuta
		ld_importo_cont[ll_pos] = ld_imp_riga
	end if	
	
	// memorizzo il codice del conto nei dettagli dei centri di costo
					
	choose case is_tipo_documento
			
		case "fat_ven"
					
			update det_fat_ven_cc
			set    cod_conto = :ls_conto_contropartita
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione and
					 prog_riga_fat_ven = :ll_prog_riga;
					 
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore in memorizzazione conto nei centri di costo.~r~n" + sqlca.sqlerrtext
				return -1
			end if

		case "fat_acq"
					
			update det_fat_acq_cc
			set    cod_conto = :ls_conto_contropartita
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione and
					 prog_riga_fat_acq = :ll_prog_riga;
					 
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore in memorizzazione conto nei centri di costo.~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
	end choose
next

for ll_i = 1 to upperbound(ls_conti)
	
	choose case is_tipo_documento
			
		case "fat_ven"
			
			insert into cont_fat_ven  
	  	 					(cod_azienda,   
						   anno_registrazione,   
						   num_registrazione,   
						   cod_conto,   
							imp_riga,   
							imp_valuta,   
							flag_dare_avere,   
							des_aggiunta )  
			values      (:s_cs_xx.cod_azienda,   
						   :il_anno_registrazione,   
							:il_num_registrazione,   
							:ls_conti[ll_i],   
							:ld_importo_cont[ll_i],   
							:ld_importo_cont_valuta[ll_i],   
							'A',   
							null );
				
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella insert di cont_fat_ven: " + sqlca.sqlerrtext
				return -1
			end if
			
		case "fat_acq"
			
			insert into cont_fat_acq  
	  	 					(cod_azienda,   
						   anno_registrazione,   
						   num_registrazione,   
						   cod_conto,   
							importo_riga,   
							importo_valuta,   
							flag_dare_avere,   
							des_aggiunta )  
			values      (:s_cs_xx.cod_azienda,   
						   :il_anno_registrazione,   
							:il_num_registrazione,   
							:ls_conti[ll_i],   
							:ld_importo_cont[ll_i],   
							:ld_importo_cont_valuta[ll_i],   
							'A',   
							null );
				
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella insert di cont_fat_acq: " + sqlca.sqlerrtext
				return -1
			end if
			
	end choose		
	
next	

return 0
end function

public function integer uof_calcola_evasione (ref string fs_messaggio);uo_generazione_documenti luo_stato


choose case is_tipo_documento
	
	case "ord_acq"
		
		
		string ls_flag_stato_ordine, ls_cod_tipo_ord_acq, ls_flag_tipo_ordine

		long   ll_cont=0, ll_evasi=0, ll_parziali=0, ll_aperti=0
		
		
		// aggiunto da Enrico per la gestione di ordini di servizi.
		select flag_tipo_ord_acq
		into   :ls_flag_tipo_ordine
		from   tab_tipi_ord_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_ord_acq = (select cod_tipo_ord_acq
											from   tes_ord_acq
											where  cod_azienda = :s_cs_xx.cod_azienda and
													 anno_registrazione = :il_anno_registrazione and
													 num_registrazione = :il_num_registrazione ) ;
		
		if sqlca.sqlcode = 100 then ls_flag_tipo_ordine = 'P'
		
		if sqlca.sqlcode = -1 then return -1
		
		if ls_flag_tipo_ordine = 'S' then
			select count(*)
			into   :ll_aperti
			from   det_ord_acq
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione and
					 quan_arrivata = 0 and
					 flag_saldo = 'N';
			if sqlca.sqlcode = -1 then return -1
			
		else
			select count(*)
			into   :ll_aperti
			from   det_ord_acq
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione and
					 cod_prodotto is not null and
					 quan_arrivata = 0 and
					 flag_saldo = 'N';
			if sqlca.sqlcode = -1 then return -1
		end if				 
		
		select count(*)
		into   :ll_parziali
		from   det_ord_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 quan_arrivata > 0 and
				 quan_arrivata < quan_ordinata and
				 flag_saldo = 'N';
		if sqlca.sqlcode = -1 then return -1
		
		select count(*)
		into   :ll_evasi
		from   det_ord_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 (quan_arrivata = quan_ordinata or flag_saldo = 'S');
		if sqlca.sqlcode = -1 then return -1
		
		
		if isnull(ll_evasi) then ll_evasi = 0
		if isnull(ll_aperti) then ll_aperti = 0
		if isnull(ll_parziali) then ll_parziali = 0
		
		ll_cont = ll_evasi + ll_parziali + ll_aperti
		if ll_cont = 0 then 
			ls_flag_stato_ordine = "A"
		else
			if ll_cont = ll_evasi then
				ls_flag_stato_ordine = "E"
			elseif ll_cont = ll_aperti then
				ls_flag_stato_ordine = "A"
			else
				ls_flag_stato_ordine = "P"
			end if
		end if
		
		update tes_ord_acq
		set    flag_evasione = :ls_flag_stato_ordine
		where  cod_azienda  = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in aggiornamento STATO ORDINE di acquisto. " + sqlca.sqlerrtext)
			return -1
		end if
		
	case "ord_ven"
		
		luo_stato = create uo_generazione_documenti
		
		if luo_stato.uof_calcola_stato_ordine(il_anno_registrazione,il_num_registrazione) <> 0 then
			destroy luo_stato
			return -1
		end if
		
		destroy luo_stato
	
end choose

return 0
end function

public function decimal uof_arrotonda_valuta (decimal fd_valore, string fs_cod_valuta, string fs_tipo_arrotondamento, ref string fs_messaggio);long   ll_return

dec{4} ld_precisione, ld_valore_tondo


select precisione
into   :ld_precisione
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :fs_cod_valuta;
		 
if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore in lettura precisione valuta da tab_valute: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	fs_messaggio = "Errore in lettura precisione valuta da tab_valute: impossibile trovare la valuta specificata"
	return -1
end if

ld_valore_tondo = fd_valore

ll_return = uof_arrotonda(ld_valore_tondo,ld_precisione,fs_tipo_arrotondamento)

choose case ll_return
	case -1		
		fs_messaggio = "La precisione della valuta " + fs_cod_valuta + " in tab_valute non è valida: verificare"
		return -1		
	case -10		
		fs_messaggio = "Impossibile arrotondare un valore negativo"
		return -1		
	case -100		
		fs_messaggio = "Il tipo arrotondamento specificato non è valido"
		return -1		
	case else		
		return ld_valore_tondo
end choose
end function

public function integer uof_arrotonda (ref decimal fd_valore, decimal fd_precisione, string fs_tipo_arrotondamento);dec{4} ld_valore_tondo, ld_scarto


// La precisione non può essere zero o negativa
if fd_precisione <= 0 then
	return -1
end if

// Il valore da arrotondare non può essere negativo
if fd_valore < 0 then
	return -10
end if

// Arrotonda il valore per difetto e annota lo scarto dal valore originale
ld_valore_tondo = truncate(fd_valore / fd_precisione, 0) * fd_precisione
ld_scarto = fd_valore - ld_valore_tondo

// Controlla se è necessario arrotondare il valore passato, altrimenti esce
if ld_scarto = 0 then
	return fd_valore
end if	

// Controllo del tipo di arrotondamento specificato
choose case fs_tipo_arrotondamento
	
	// arrotondamento standard: fino a 0.4 per difetto, da 0.5 in poi per eccesso (considerando la precisione come unità)
	case "round","iva","euro"
		
		// se la differenza fra il valore passato e quello troncato è maggiore o uguale a metà della precisione, allora
		// si arrotonda per eccesso e viene quindi aggiunta un'unità di precisione
		if ld_scarto >= (fd_precisione / 2) then
			ld_valore_tondo = ld_valore_tondo + fd_precisione
		end if
		
		// in caso contrario il valore è già arrotondato per difetto
		
		// se il valore arrotondato risulta inferiore alla precisione allora si arrotonda al valore di precisione
		if ld_valore_tondo < fd_precisione then
			ld_valore_tondo = fd_precisione
		end if
		
	// arrotondamento per eccesso
	case "eccesso"
		
		// si aggiunge un'unità di precisione
		ld_valore_tondo = ld_valore_tondo + fd_precisione		

	// arrotondamento per difetto
	case "difetto"
		
		// già implementato all'inizio della funzione
		
	// tipo di arrotondamento non riconosciuto dalla funzione
	case else
		
		return -100
	
end choose

// restituisce il valore arrotondato
fd_valore = ld_valore_tondo

return 0
end function

public function integer uof_totalizza_iva (ref string fs_messaggio);// Gli array ls_cod_ive, ll_aliquote, ld_imponibili, ld_sola_iva conterranno rispettivamente:
// - tutti i distinti codici iva presenti nelle righe del documento;
// - il valore di aliquota per ogni codice iva;
// - il totale dell'imponibile (in valuta e non) per ogni codice iva;
// - il totale dell'imponibile di sola iva (in valuta e non) per ogni codice iva;

long    ll_i, ll_j, ll_pos

dec{4}  ld_imponibile_valuta, ld_imponibile, ld_imponibili_valuta[], ld_imponibili[], ld_sola_iva_valuta[],&
		  ld_sola_iva[], ld_importo_iva_valuta, ld_importo_iva, ld_aliquota, ld_aliquote[], ld_perc_sogg, &
		  ld_imp_diff, ld_imp_sogg, ld_imp_diff_valuta, ld_imp_sogg_valuta, ld_aliquota_diff
		  
string  ls_cod_iva, ls_cod_tipo_det_ven, ls_flag_sola_iva, ls_ive[], ls_flag_calcolo, ls_cod_iva_diff

boolean lb_trovato


// Cancellazione delle righe di iva già esistenti
choose case is_tipo_documento
	
  	case "bol_ven"
	
		delete
		from   iva_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_bol_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "bol_acq"
	
		delete
		from   iva_bol_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_bolla_acq = :il_anno_registrazione and
				 num_bolla_acq = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_bol_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "fat_ven"	

		delete
		from   iva_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "fat_acq"	

		delete
		from   iva_fat_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "off_ven"
	
		delete
		from   iva_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_off_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "off_acq"
	
		delete
		from   iva_off_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_off_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "ord_ven"
	
		delete
		from   iva_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "ord_acq"
	
		delete
		from   iva_ord_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di iva_ord_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
end choose

// Riga per riga vengono totalizzati gli imponibili riferiti ad ogni codice iva
for ll_i = 1 to ids_righe_documento.rowcount()
	
	// 14.05.2019 EnMe: gestione bollo virtuale in fattura elettronica; non deve essere totalizzato negli imponibili iva, ma solo nel totale documento
	if is_tipo_documento = "fat_ven" then
		if  ids_righe_documento.getitemstring(ll_i, "cod_tipo_det_ven") = "SPB" then continue
	end if
	
	// Viene letto l'imponibile della riga corrente in valuta e in euro
	ld_imponibile_valuta = ids_righe_documento.getitemnumber(ll_i, "imponibile_iva_valuta")
	ld_imponibile = ids_righe_documento.getitemnumber(ll_i, "imponibile_iva")
	
	if isnull(ld_imponibile_valuta) then ld_imponibile_valuta = 0
	if isnull(ld_imponibile) then ld_imponibile = 0
	
	if ld_imponibile_valuta = 0 or ld_imponibile = 0 then
		continue
	end if	
	
	// Viene letto il codice iva della riga corrente
	ls_cod_iva = ids_righe_documento.getitemstring(ll_i, "cod_iva")
	
	choose case is_tipo_documento
			
		case "bol_ven","fat_ven","off_ven","ord_ven"
	
			// Viene letto il codice tipo dettaglio vendita
			ls_cod_tipo_det_ven = ids_righe_documento.getitemstring(ll_i, "cod_tipo_det_ven")
			
			// Viene letto il valore del flag sola iva da tab_tipi_det_ven
			select flag_sola_iva
			into   :ls_flag_sola_iva
			from   tab_tipi_det_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			 
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella select di tab_tipi_det_ven: " + sqlca.sqlerrtext
				return -1
			end if
			
		case "bol_acq","fat_acq","off_acq","ord_acq"
	
			// Viene letto il codice tipo dettaglio vendita
			ls_cod_tipo_det_ven = ids_righe_documento.getitemstring(ll_i, "cod_tipo_det_acq")
			
			// Viene letto il valore del flag sola iva da tab_tipi_det_acq
			select flag_sola_iva
			into   :ls_flag_sola_iva
			from   tab_tipi_det_acq
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_det_acq = :ls_cod_tipo_det_ven;
			 
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella select di tab_tipi_det_acq: " + sqlca.sqlerrtext
				return -1
			end if
			
	end choose		
	
	// Viene letto il valore di aliquota per l'iva della riga corrente
	select aliquota,				
			 flag_calcolo,
			 perc_imponibile_soggetto,
			 cod_iva_differenziato
	into   :ld_aliquota,
			 :ls_flag_calcolo,
			 :ld_perc_sogg,
			 :ls_cod_iva_diff
	from   tab_ive
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_iva = :ls_cod_iva;
	 
	if sqlca.sqlcode = -1 then
		fs_messaggio = "Errore nella select di tab_ive: " + sqlca.sqlerrtext
		return -1
	end if
	
	// Si controlla se è stato specificato di non eseguire il calcolo per l'iva della riga corrente
	if ls_flag_calcolo = 'N' then
		continue
	end if
	
	// Gestione assoggettamento IVA differenziato
	if not isnull(ld_perc_sogg) and ld_perc_sogg > 0 and ld_perc_sogg < 100 then
	
		ld_imp_diff = ld_imponibile
		
		ld_imp_sogg = ld_imp_diff / 100 * ld_perc_sogg
		
		uof_arrotonda(ld_imp_sogg,id_precisione,"euro")
		
		ld_imponibile = ld_imp_sogg
		
		ld_imp_diff = ld_imp_diff - ld_imp_sogg
		
		ld_imp_diff_valuta = ld_imponibile_valuta
		
		ld_imp_sogg_valuta = ld_imp_diff_valuta / 100 * ld_perc_sogg
		
		uof_arrotonda(ld_imp_sogg_valuta,id_precisione,"euro")
		
		ld_imponibile_valuta = ld_imp_sogg_valuta
		
		ld_imp_diff_valuta = ld_imp_diff_valuta - ld_imp_sogg_valuta
		
	end if
	
	// Si controlla se il codice iva corrente era già stato memorizzato nell'array corrispondente:
	lb_trovato = false
	
	for ll_j = 1 to upperbound(ls_ive)
		
		// 1) in caso affermativo viene sommato all'imponibile totale per quel cod_iva l'imponibile appena letto...
		if ls_ive[ll_j] = ls_cod_iva then
			
			lb_trovato = true
			
			// ...distinguendo il caso in cui si tratti di sola iva
			choose case ls_flag_sola_iva
			
				case "S"
					ld_sola_iva_valuta[ll_j] = ld_sola_iva_valuta[ll_j] + ld_imponibile_valuta
					ld_sola_iva[ll_j] = ld_sola_iva[ll_j] + ld_imponibile
				
				case "N"
					ld_imponibili_valuta[ll_j] = ld_imponibili_valuta[ll_j] + ld_imponibile_valuta
					ld_imponibili[ll_j] = ld_imponibili[ll_j] + ld_imponibile
			
			end choose
			
			exit
		
		end if		
	
	next
	
	// 2) in caso contrario viene aggiunto alla fine dell'array un nuovo elemento per il codice iva corrente...
	if lb_trovato = false then
		
		ll_pos = upperbound(ls_ive) + 1
		ls_ive[ll_pos] = ls_cod_iva
		ld_aliquote[ll_pos] = ld_aliquota
		
		// ...sempre distinguendo il caso in cui si tratti di sola iva
		choose case ls_flag_sola_iva
		
			case "S"
				
				ld_sola_iva_valuta[ll_pos] = ld_imponibile_valuta
				ld_sola_iva[ll_pos] = ld_imponibile
				ld_imponibili_valuta[ll_pos] = 0
				ld_imponibili[ll_pos] = 0
			
			case "N"
				
				ld_imponibili_valuta[ll_pos] = ld_imponibile_valuta
				ld_imponibili[ll_pos] = ld_imponibile
				ld_sola_iva_valuta[ll_pos] = 0
				ld_sola_iva[ll_pos] = 0
		
		end choose
		
	end if
	
	//Inserimento IVA relativa all'assoggettamento differenziato
	if not isnull(ls_cod_iva_diff) and not isnull(ld_perc_sogg) and ld_perc_sogg > 0 and ld_perc_sogg < 100 then
		
		select aliquota
		into   :ld_aliquota_diff
		from   tab_ive
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_iva = :ls_cod_iva_diff;
		 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di tab_ive: " + sqlca.sqlerrtext
			return -1
		end if
		
		// Si controlla se il codice iva corrente era già stato memorizzato nell'array corrispondente:
		lb_trovato = false
		
		for ll_j = 1 to upperbound(ls_ive)
			
			// 1) in caso affermativo viene sommato all'imponibile totale per quel cod_iva l'imponibile appena letto...
			if ls_ive[ll_j] = ls_cod_iva_diff then
				
				lb_trovato = true
				
				// ...distinguendo il caso in cui si tratti di sola iva
				choose case ls_flag_sola_iva
				
					case "S"
						ld_sola_iva_valuta[ll_j] = ld_sola_iva_valuta[ll_j] + ld_imp_diff_valuta
						ld_sola_iva[ll_j] = ld_sola_iva[ll_j] + ld_imp_diff
					
					case "N"
						ld_imponibili_valuta[ll_j] = ld_imponibili_valuta[ll_j] + ld_imp_diff_valuta
						ld_imponibili[ll_j] = ld_imponibili[ll_j] + ld_imp_diff
				
				end choose
				
				exit
			
			end if		
		
		next
		
		// 2) in caso contrario viene aggiunto alla fine dell'array un nuovo elemento per il codice iva corrente...
		if lb_trovato = false then
			
			ll_pos = upperbound(ls_ive) + 1
			ls_ive[ll_pos] = ls_cod_iva_diff
			ld_aliquote[ll_pos] = ld_aliquota_diff
			
			// ...sempre distinguendo il caso in cui si tratti di sola iva
			choose case ls_flag_sola_iva
			
				case "S"
					
					ld_sola_iva_valuta[ll_pos] = ld_imp_diff_valuta
					ld_sola_iva[ll_pos] = ld_imp_diff
					ld_imponibili_valuta[ll_pos] = 0
					ld_imponibili[ll_pos] = 0
				
				case "N"
					
					ld_imponibili_valuta[ll_pos] = ld_imp_diff_valuta
					ld_imponibili[ll_pos] = ld_imp_diff
					ld_sola_iva_valuta[ll_pos] = 0
					ld_sola_iva[ll_pos] = 0
			
			end choose
			
		end if
		
	end if	
	
next

// Si riscorre l'array ls_ive per calcolare l'importo di ogni distinta iva
for ll_i = 1 to upperbound(ls_ive)
	
	ld_importo_iva_valuta = 0
	ld_importo_iva = 0
	
	// Importo in valuta
	if (upperbound(ld_imponibili_valuta) > 0) then
		if ld_imponibili_valuta[ll_i] <> 0 then
			ld_importo_iva_valuta = ld_imponibili_valuta[ll_i] / 100 * ld_aliquote[ll_i]
		end if
	end if	
	
	if (upperbound(ld_sola_iva_valuta) > 0) then
		if(ld_sola_iva_valuta[ll_i] <> 0) then
			ld_importo_iva_valuta = ld_importo_iva_valuta + (ld_sola_iva_valuta[ll_i] / 100 * ld_aliquote[ll_i])
		end if	
	end if
	
	if ld_importo_iva_valuta < 0 then
	
		ld_importo_iva_valuta = ld_importo_iva_valuta * -1
		
		// Arrotondamento del valore calcolato
		uof_arrotonda(ld_importo_iva_valuta, id_precisione_valuta, "iva")
		
		ld_importo_iva_valuta = ld_importo_iva_valuta * -1
	
	else	
	
		// Arrotondamento del valore calcolato
		uof_arrotonda(ld_importo_iva_valuta, id_precisione_valuta, "iva")
	
	end if
	
	// Importo in euro
	if (upperbound(ld_imponibili) > 0) then
		if ld_imponibili[ll_i] <> 0 then
			ld_importo_iva = ld_imponibili[ll_i] / 100 * ld_aliquote[ll_i]
		end if
	end if	
	
	if (upperbound(ld_sola_iva) > 0) then
		if(ld_sola_iva[ll_i] <> 0) then
			ld_importo_iva = ld_importo_iva + (ld_sola_iva[ll_i] / 100 * ld_aliquote[ll_i])
		end if	
	end if
	
	if ld_importo_iva < 0 then
	
		ld_importo_iva = ld_importo_iva * -1
		
		// Arrotondamento del valore calcolato
		uof_arrotonda(ld_importo_iva, id_precisione, "iva")
		
		ld_importo_iva = ld_importo_iva * -1
	
	else	
	
		// Arrotondamento del valore calcolato
		uof_arrotonda(ld_importo_iva, id_precisione, "iva")
	
	end if
	
	// I valori di imponibile e importo iva vengono inseriti nella tabella corrispondente a seconda del tipo di documento
	choose case is_tipo_documento
	
   	case "bol_ven"
	
			insert into iva_bol_ven
			            (cod_azienda,
							anno_registrazione,
							num_registrazione,
							cod_iva,
							imponibile_valuta_iva,
							imponibile_iva,
							importo_valuta_iva,
							importo_iva,
							perc_iva)
			values		(:s_cs_xx.cod_azienda,
							:il_anno_registrazione,
							:il_num_registrazione,
							:ls_ive[ll_i],
							:ld_imponibili_valuta[ll_i],
							:ld_imponibili[ll_i],
							:ld_importo_iva_valuta,
							:ld_importo_iva,
							:ld_aliquote[ll_i]);
							
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella insert di iva_bol_ven: " + sqlca.sqlerrtext
				return -1
			end if
		
		case "bol_acq"
	
			insert into iva_bol_acq
			            (cod_azienda,
							anno_bolla_acq,
							num_bolla_acq,
							cod_iva,
							imponibile_valuta_iva,
							imponibile_iva,
							importo_valuta_iva,
							importo_iva,
							perc_iva)
			values		(:s_cs_xx.cod_azienda,
							:il_anno_registrazione,
							:il_num_registrazione,
							:ls_ive[ll_i],
							:ld_imponibili_valuta[ll_i],
							:ld_imponibili[ll_i],
							:ld_importo_iva_valuta,
							:ld_importo_iva,
							:ld_aliquote[ll_i]);
							
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella insert di iva_bol_acq: " + sqlca.sqlerrtext
				return -1
			end if
		
		case "fat_ven"

			insert into iva_fat_ven
			            (cod_azienda,
							anno_registrazione,
							num_registrazione,
							cod_iva,
							imponibile_valuta_iva,
							imponibile_iva,
							importo_valuta_iva,
							importo_iva,
							perc_iva,
							imponibile_solo_iva,
							imponibile_solo_iva_valuta)
			values		(:s_cs_xx.cod_azienda,
							:il_anno_registrazione,
							:il_num_registrazione,
							:ls_ive[ll_i],
							:ld_imponibili_valuta[ll_i],
							:ld_imponibili[ll_i],
							:ld_importo_iva_valuta,
							:ld_importo_iva,
							:ld_aliquote[ll_i],
							:ld_sola_iva[ll_i],
							:ld_sola_iva_valuta[ll_i]);
							
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella insert di iva_fat_ven: " + sqlca.sqlerrtext
				return -1
			end if	
		
		case "fat_acq"

			insert into iva_fat_acq
			            (cod_azienda,
							anno_registrazione,
							num_registrazione,
							cod_iva,
							imponibile_valuta_iva,
							imponibile_iva,
							importo_valuta_iva,
							importo_iva,
							perc_iva)
			values		(:s_cs_xx.cod_azienda,
							:il_anno_registrazione,
							:il_num_registrazione,
							:ls_ive[ll_i],
							:ld_imponibili_valuta[ll_i],
							:ld_imponibili[ll_i],
							:ld_importo_iva_valuta,
							:ld_importo_iva,
							:ld_aliquote[ll_i]);
							
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella insert di iva_fat_acq: " + sqlca.sqlerrtext
				return -1
			end if	
		
		case "off_ven"
	
			insert into iva_off_ven
			            (cod_azienda,
							anno_registrazione,
							num_registrazione,
							cod_iva,
							imponibile_valuta_iva,
							imponibile_iva,
							importo_valuta_iva,
							importo_iva,
							perc_iva)
			values		(:s_cs_xx.cod_azienda,
							:il_anno_registrazione,
							:il_num_registrazione,
							:ls_ive[ll_i],
							:ld_imponibili_valuta[ll_i],
							:ld_imponibili[ll_i],
							:ld_importo_iva_valuta,
							:ld_importo_iva,
							:ld_aliquote[ll_i]);
							
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella insert di iva_off_ven: " + sqlca.sqlerrtext
				return -1
			end if
	
		case "off_acq"
	
			insert into iva_off_acq
			            (cod_azienda,
							anno_registrazione,
							num_registrazione,
							cod_iva,
							imponibile_valuta_iva,
							imponibile_iva,
							importo_valuta_iva,
							importo_iva,
							perc_iva)
			values		(:s_cs_xx.cod_azienda,
							:il_anno_registrazione,
							:il_num_registrazione,
							:ls_ive[ll_i],
							:ld_imponibili_valuta[ll_i],
							:ld_imponibili[ll_i],
							:ld_importo_iva_valuta,
							:ld_importo_iva,
							:ld_aliquote[ll_i]);
							
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella insert di iva_off_acq: " + sqlca.sqlerrtext
				return -1
			end if
	
		case "ord_ven"
		
			insert into iva_ord_ven
			            (cod_azienda,
							anno_registrazione,
							num_registrazione,
							cod_iva,
							imponibile_valuta_iva,
							imponibile_iva,
							importo_valuta_iva,
							importo_iva,
							perc_iva)
			values		(:s_cs_xx.cod_azienda,
							:il_anno_registrazione,
							:il_num_registrazione,
							:ls_ive[ll_i],
							:ld_imponibili_valuta[ll_i],
							:ld_imponibili[ll_i],
							:ld_importo_iva_valuta,
							:ld_importo_iva,
							:ld_aliquote[ll_i]);
							
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella insert di iva_ord_ven: " + sqlca.sqlerrtext
				return -1
			end if
		
		case "ord_acq"
		
			insert into iva_ord_acq
			            (cod_azienda,
							anno_registrazione,
							num_registrazione,
							cod_iva,
							imponibile_valuta_iva,
							imponibile_iva,
							importo_valuta_iva,
							importo_iva,
							perc_iva)
			values		(:s_cs_xx.cod_azienda,
							:il_anno_registrazione,
							:il_num_registrazione,
							:ls_ive[ll_i],
							:ld_imponibili_valuta[ll_i],
							:ld_imponibili[ll_i],
							:ld_importo_iva_valuta,
							:ld_importo_iva,
							:ld_aliquote[ll_i]);
							
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella insert di iva_ord_acq: " + sqlca.sqlerrtext
				return -1
			end if
		
	end choose
	
next	

return 0
end function

public function integer uof_controllo_quadratura (ref string fs_messaggio);string ls_scad_manuali, ls_flag_tipo_fornitore, ls_cod_fornitore, ls_flag_esigibilita_iva

dec{4} ld_tot_scadenze, ld_tot_contropartite, ld_tot_fattura, ld_tot_iva


ls_flag_tipo_fornitore="S"
setnull(ls_flag_esigibilita_iva)
choose case is_tipo_documento
	
	case "fat_ven"
		
		// Calcolo totale fattura
		select isnull(T.imponibile_iva,0) + isnull(T.importo_iva,0),
				 isnull(T.importo_iva,0),
				 T.flag_scad_manuali,
				 I.fatel_esigibilita_iva
		into   :ld_tot_fattura,
				 :ld_tot_iva,
				 :ls_scad_manuali,
				 :ls_flag_esigibilita_iva
		from   	tes_fat_ven T
					left join anag_clienti A on T.cod_azienda=A.cod_azienda and T.cod_cliente=A.cod_cliente
					left join tab_ive I on A.cod_azienda=I.cod_azienda and A.cod_iva=I.cod_iva
		where  	T.cod_azienda = :s_cs_xx.cod_azienda and
		       		T.anno_registrazione = :il_anno_registrazione and
				 	T.num_registrazione = :il_num_registrazione;
		
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di tes_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
		// Calcolo totale contropartite
		select sum(imp_riga)
		into   :ld_tot_contropartite
		from   cont_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
		
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di cont_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
		ld_tot_contropartite = ld_tot_contropartite + ld_tot_iva
		
		if ls_scad_manuali <> "S" and isnull(ls_flag_esigibilita_iva) and ls_flag_esigibilita_iva <> "S" then
		
			// Calcolo totale scadenze
			select sum(imp_rata)
			into   :ld_tot_scadenze
			from   scad_fat_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione and
					 cod_tipo_pagamento <> 'CPR';
			
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella select di scad_fat_ven: " + sqlca.sqlerrtext
				return -1
			end if
			
		else
			
			ld_tot_scadenze = ld_tot_fattura
			
		end if
		
	case "fat_acq"
		
		// Calcolo totale fattura
		select imponibile_iva + importo_iva,
				 importo_iva,
				 cod_fornitore
		into   :ld_tot_fattura,
				 :ld_tot_iva,
				 :ls_cod_fornitore
		from   tes_fat_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
		
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di tes_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
		ls_flag_tipo_fornitore = uof_tipo_fornitore(ls_cod_fornitore)
		
		// Calcolo totale contropartite
		select sum(importo_riga)
		into   :ld_tot_contropartite
		from   cont_fat_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
		
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di cont_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
		if ls_flag_tipo_fornitore <> "C" then
			ld_tot_contropartite = ld_tot_contropartite + ld_tot_iva
		else
			ld_tot_contropartite = ld_tot_contropartite
		end if
		
		// Calcolo totale scadenze
		select sum(imp_rata)
		into   :ld_tot_scadenze
		from   scad_fat_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 cod_tipo_pagamento <> 'CPR';
		
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di scad_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
end choose	


// Controllo di corrispondenza fra i totali
if ls_flag_tipo_fornitore = "C" then
	if not ((ld_tot_scadenze = ld_tot_contropartite) and ( (ld_tot_contropartite + ld_tot_iva) = ld_tot_fattura) ) then
		fs_messaggio = "Totale scadenze = " + string(ld_tot_scadenze)
		fs_messaggio = fs_messaggio + "~nTotale contropartite = " + string(ld_tot_contropartite)
		fs_messaggio = fs_messaggio + "~nTotale fattura = " + string(ld_tot_fattura)
		return 100
	end if	
else
	if not ((ld_tot_scadenze = ld_tot_contropartite) and ( ld_tot_contropartite = ld_tot_fattura)) then
		fs_messaggio = "Totale scadenze = " + string(ld_tot_scadenze)
		fs_messaggio = fs_messaggio + "~nTotale contropartite = " + string(ld_tot_contropartite)
		fs_messaggio = fs_messaggio + "~nTotale fattura = " + string(ld_tot_fattura)
		return 100
	end if	
end if	
return 0
end function

public function date uof_calcola_data_fine_mese (date fdd_data);string  ls_data

long 	  ll_mese

date 	  ldd_data

boolean lb_bisestile


ll_mese = month(fdd_data)

if mod(year(fdd_data),4) = 0 and mod(year(fdd_data),100) <> 0 or mod(year(fdd_data),400) = 0 then
	lb_bisestile = true
else
	lb_bisestile = false
end if

choose case ll_mese
	case 4,6,9,11
		ls_data = "30/" + string(ll_mese) + "/" + string(year(fdd_data))
		ldd_data = date(ls_data)
	case 1,3,5,7,8,10,12
		ls_data = "31/" + string(ll_mese) + "/" + string(year(fdd_data))
		ldd_data = date(ls_data)
	case 2
		if lb_bisestile then
			ls_data = "29/2/" + string(year(fdd_data))
			ldd_data = date(ls_data)
		else				
			ls_data = "28/2/" + string(year(fdd_data))
			ldd_data = date(ls_data)
		end if
end choose

return ldd_data
end function

public function integer uof_addebiti_cliente (ref string fs_messaggio);string   ls_cod_pagamento, ls_cod_valuta, ls_cod_cliente, ls_flag_calcolo, ls_cod_lingua, ls_cod_tipo_det_ven, &
         ls_messaggio, ls_stringa, ls_str, ls_cod_iva, ls_flag_tipo_pagamento, ls_cod_tipo_doc_ven, &
		   ls_cod_fornitore, ls_flag_tipo_doc_ven, ls_cod_documento,ls_des_addebito, ls_cod_prodotto_appo, ls_descrizione_appo
			
long     ll_prog_riga_fat_ven, ll_pos

dec{4}   ld_importo_addebito

datetime ldt_data_esenzione_iva, ldt_data_registrazione, ldt_data_fattura, ldt_data_riferimento


select cod_pagamento,
  		 cod_valuta,
		 cod_cliente,
		 cod_fornitore,
		 flag_calcolo,
		 data_registrazione,
 		 cod_tipo_fat_ven,
		 data_fattura,
		 cod_documento
into   	:ls_cod_pagamento,
		:ls_cod_valuta,
       	:ls_cod_cliente,
       	:ls_cod_fornitore,
		:ls_flag_calcolo,
		:ldt_data_registrazione,
		:ls_cod_tipo_doc_ven,
		:ldt_data_fattura,
		:ls_cod_documento
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
	    anno_registrazione = :il_anno_registrazione and
 		 num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di tes_fat_ven: " + sqlca.sqlerrtext
	return -1
end if

select flag_tipo_fat_ven
into   :ls_flag_tipo_doc_ven
from   tab_tipi_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_fat_ven = :ls_cod_tipo_doc_ven;
 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di tab_tipi_fat_ven: " + sqlca.sqlerrtext
	return -1
end if

if ls_flag_tipo_doc_ven <> "F" then
	
	//se in questo IF il campo cliente è vuoto, molto probabilmente la fattura è una PROFORMA a contatto (anag_contatti)
	//poichè al momento (26/09/2012) non esiste una tabella e neppure una gestione addebiti contatti (analogo di anag_clienti_addebiti)
	//usciamo subito da questa funzione
	if ls_cod_cliente="" or isnull(ls_cod_cliente) then return 0
	
	
	select cod_lingua
	into   :ls_cod_lingua
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_cliente = :ls_cod_cliente;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di anag_clienti: " + sqlca.sqlerrtext
		return -1
	end if
	
else
	
	return 0		// per i fornitori non fare nulla
	
end if

if not isnull(ls_cod_lingua) then
	
	select cod_lingua
	into   :ls_cod_lingua
	from   tab_lingue
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_lingua = :ls_cod_lingua;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di tab_lingue: " + sqlca.sqlerrtext
		return -1
	end if
	
end if

if isnull(ls_cod_documento) then
	
	ldt_data_riferimento = ldt_data_registrazione
	
	if isnull(ldt_data_riferimento) then
		ldt_data_riferimento = datetime(today(), 00:00:00)
	end if	
	
else
	
	ldt_data_riferimento = ldt_data_fattura

end if

declare cu_addebiti cursor for
select des_addebito,
       cod_tipo_det_ven,
       importo_addebito,
		 cod_valuta
from   anag_clienti_addebiti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente and
		 data_inizio <= :ldt_data_riferimento and
		 data_fine >= :ldt_data_riferimento and
		 cod_valuta = :ls_cod_valuta and
		 flag_mod_valore_fatture = 'N' and
		 flag_tipo_documento = 'F';
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di anag_clienti_addebiti: " + sqlca.sqlerrtext
	return -1
end if

open cu_addebiti;

if sqlca.sqlcode = -1 then
	fs_messaggio = "Errore nella open del cursore cu_addebiti: " + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_addebiti into :ls_des_addebito, 
	                       :ls_cod_tipo_det_ven, 
								  :ld_importo_addebito, 
								  :ls_cod_valuta;
	
	if sqlca.sqlcode = -1 then
		fs_messaggio = "Errore nella fetch del cursore cu_addebiti: " + sqlca.sqlerrtext
		close cu_addebiti;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then
		exit
	end if	
	
	if isnull(ls_cod_tipo_det_ven) then
		fs_messaggio = "Errore nella fetch del cursore cu_addebiti: impostare il tipo dettaglio vendita negli addebiti clienti"
		close cu_addebiti;
		return -1
	end if
	
	if isnull(ls_cod_valuta) then
		fs_messaggio = "Errore nella fetch del cursore cu_addebiti: impostare la valuta negli addebiti clienti"
		close cu_addebiti;
		return -1
	end if
			
   uof_arrotonda(ld_importo_addebito, id_precisione_valuta, "euro")
		
	// Ricerca della prima riga libera nel documento
	select max(prog_riga_fat_ven)
	into   :ll_prog_riga_fat_ven
	from   det_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione;
			 
	if ll_prog_riga_fat_ven = 0 or isnull(ll_prog_riga_fat_ven) then
		ll_prog_riga_fat_ven = 10
	else
		ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10
	end if
	
	// Ricerca di aliquota iva o esenzione del dettaglio
	// La variabile ls_cod_tipo_det_ven non può essere a null: già verificato sopra	
	select cod_iva  
	into   :ls_str
	from   tab_tipi_det_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di tab_tipi_det_ven: " + sqlca.sqlerrtext
		close cu_addebiti;
		return -1
	end if
	
	if not isnull(ls_str) then ls_cod_iva = ls_str
	
	select cod_iva,
			 data_esenzione_iva
	into   :ls_str,
			 :ldt_data_esenzione_iva
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_cliente = :ls_cod_cliente;
			 
	if sqlca.sqlcode = 0 then
		if not isnull(ls_str) and (ldt_data_esenzione_iva <= ldt_data_riferimento or isnull(ldt_data_esenzione_iva)) then
			ls_cod_iva = ls_str
		end if	
	end if
	
	// *** Michela 26/01/2006: se il primo carattere della descrizione dell'addebito è = "*" allora la 
	//                         parte restante della descrizione è un codice prodotto e la descrizione vado a prendermela 
	//                         dall'anagrafica prodotti
	
	setnull(ls_cod_prodotto_appo)
	setnull(ls_descrizione_appo)
	ll_pos = 0
	ll_pos = pos( ls_des_addebito, "*")
	if not isnull(ll_pos) and ll_pos = 1 then
		ls_cod_prodotto_appo = mid( ls_des_addebito, 2)
		
		select des_prodotto
		into   :ls_descrizione_appo
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto_appo;
				 
		if sqlca.sqlcode <> 0 then
			setnull(ls_cod_prodotto_appo)
		else
			ls_des_addebito = ls_descrizione_appo
		end if
	end if
	
	// La riga di spese bancarie viene inserita nel dettaglio del documento
	insert into det_fat_ven  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  prog_riga_fat_ven,   
				  cod_tipo_det_ven,   
				  cod_deposito,   
				  cod_ubicazione,   
				  cod_lotto,   
				  data_stock,   
				  progr_stock,   
				  cod_misura,   
				  cod_prodotto,   
				  des_prodotto,   
				  quan_fatturata,   
				  prezzo_vendita,   
				  sconto_1,   
				  sconto_2,   
				  provvigione_1,   
				  provvigione_2,   
				  cod_iva,   
				  cod_tipo_movimento,   
				  num_registrazione_mov_mag,   
				  anno_registrazione_fat,   
				  num_registrazione_fat,   
				  nota_dettaglio,   
				  anno_registrazione_bol_ven,   
				  num_registrazione_bol_ven,   
				  prog_riga_bol_ven,   
				  anno_commessa,   
				  num_commessa,   
				  cod_centro_costo,   
				  sconto_3,   
				  sconto_4,   
				  sconto_5,   
				  sconto_6,   
				  sconto_7,   
				  sconto_8,   
				  sconto_9,   
				  sconto_10,   
				  anno_registrazione_mov_mag,   
				  fat_conversione_ven,   
				  anno_reg_des_mov,   
				  num_reg_des_mov,   
				  anno_reg_ord_ven,   
				  num_reg_ord_ven,   
				  prog_riga_ord_ven,   
				  cod_versione,   
				  num_confezioni,   
				  num_pezzi_confezione,
				  ordinamento)  
	  values ( :s_cs_xx.cod_azienda,   
				  :il_anno_registrazione,   
				  :il_num_registrazione,   
				  :ll_prog_riga_fat_ven,   
				  :ls_cod_tipo_det_ven,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  :ls_cod_prodotto_appo,   
				  :ls_des_addebito,   
				  1,   
				  :ld_importo_addebito,   
				  0,   
				  0,   
				  0,   
				  0,   
				  :ls_cod_iva,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  null,   
				  1,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  0,   
				  0,
				  :ll_prog_riga_fat_ven)  ;
				  
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella insert di det_fat_ven: " + sqlca.sqlerrtext
		return -1
	end if
	
loop

close cu_addebiti;

return 0

end function

public function integer uof_calcola_scadenze (ref string fs_messaggio);long 		ll_cont_rate, ll_i

date     ldd_data_fattura, ldd_rate_data[]

datetime ldt_data_fattura, ldt_data_registrazione, ldt_rata_data, ldt_data_decorrenza_pagamento, ldt_data_neutra

dec{4} 	ld_tot_imp, ld_tot_iva, ld_rate_importo[], ld_rata_importo, ld_rata_valuta

string   ls_cod_pagamento, ls_cod_cliente, ls_cod_fornitore, ls_rate_tipo[], ls_scad_manuali, ls_cod_contatto, ls_modalita_pagamento[], ls_flag_esigibilita_iva


// Lettura dati fattura

setnull(ldt_data_decorrenza_pagamento)
setnull(ls_cod_contatto)

ldt_data_neutra = datetime(date("19000101"), 00:00:00)
setnull(ls_flag_esigibilita_iva)

choose case is_tipo_documento
		
	case "fat_ven"
		
		select T.imponibile_iva,
		       	T.importo_iva,
				T.data_fattura,
				T.data_registrazione,
				T.cod_pagamento,
		       	T.cod_cliente,
				T.cod_fornitore,
				T.cod_contatto,
				T.flag_scad_manuali,
				T.data_decorrenza_pagamento,
				I.fatel_esigibilita_iva
		into   :ld_tot_imp,
		       	:ld_tot_iva,
				:ldt_data_fattura,
				:ldt_data_registrazione,
				:ls_cod_pagamento,
		       	:ls_cod_cliente,
				:ls_cod_fornitore,
				:ls_cod_contatto,
				:ls_scad_manuali,
				:ldt_data_decorrenza_pagamento,
				:ls_flag_esigibilita_iva
		from   	tes_fat_ven T
					left join anag_clienti A on T.cod_azienda=A.cod_azienda and T.cod_cliente=A.cod_cliente
					left join tab_ive I on A.cod_azienda=I.cod_azienda and A.cod_iva=I.cod_iva
		where  	T.cod_azienda = :s_cs_xx.cod_azienda and
		       		T.anno_registrazione = :il_anno_registrazione and
				 	T.num_registrazione = :il_num_registrazione;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in lettura dati fattura da tes_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
	case "fat_acq"
		
		select imponibile_iva,
		       	importo_iva,
				data_protocollo,
				data_registrazione,
				cod_pagamento,
		       	cod_fornitore
		into   :ld_tot_imp,
		       	:ld_tot_iva,
				:ldt_data_fattura,
				:ldt_data_registrazione,
				:ls_cod_pagamento,
		       	:ls_cod_fornitore
		from   tes_fat_acq
		where	 tes_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and
		       	 tes_fat_acq.anno_registrazione = :il_anno_registrazione and
				 tes_fat_acq.num_registrazione = :il_num_registrazione;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in lettura dati fattura da tes_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
		// se il fornitore è comunitario, genero le scadenze solo per l'imponibile
		if uof_tipo_fornitore(ls_cod_fornitore) = "C" then ld_tot_iva = 0
		
	case "ord_ven"
		
		select imponibile_iva,
		       	importo_iva,
				data_consegna,
				data_registrazione,
				cod_pagamento,
		       	cod_cliente,
				'N',
				data_consegna
		into   :ld_tot_imp,
		       	:ld_tot_iva,
				:ldt_data_fattura,
				:ldt_data_registrazione,
				:ls_cod_pagamento,
		       	:ls_cod_cliente,
				:ls_scad_manuali,
				:ldt_data_decorrenza_pagamento
		from   	tes_ord_ven
		where  	cod_azienda = :s_cs_xx.cod_azienda and
		       		anno_registrazione = :il_anno_registrazione and
				 	num_registrazione = :il_num_registrazione;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in lettura dati fattura da tes_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
	case "ord_acq"
		
		select imponibile_iva,
		       	importo_iva,
				data_consegna,
				data_registrazione,
				cod_pagamento,
				cod_fornitore,
				'N',
				data_consegna
		into   :ld_tot_imp,
		       	:ld_tot_iva,
				:ldt_data_fattura,
				:ldt_data_registrazione,
				:ls_cod_pagamento,
		       	:ls_cod_fornitore,
				:ls_scad_manuali,
				:ldt_data_decorrenza_pagamento
		from   	tes_ord_acq
		where  	cod_azienda = :s_cs_xx.cod_azienda and
		       		anno_registrazione = :il_anno_registrazione and
				 	num_registrazione = :il_num_registrazione;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in lettura dati fattura da tes_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
		// se il fornitore è comunitario, genero le scadenze solo per l'imponibile
		if uof_tipo_fornitore(ls_cod_fornitore) = "C" then ld_tot_iva = 0
		
end choose

if ls_scad_manuali = "S" then
	return 0
end if

if not isnull(ldt_data_fattura) then
	if not isnull(ldt_data_decorrenza_pagamento) and ldt_data_decorrenza_pagamento > ldt_data_neutra then
		ldd_data_fattura = date(ldt_data_decorrenza_pagamento)
	else
		ldd_data_fattura = date(ldt_data_fattura)
	end if
else
	ldd_data_fattura = date(ldt_data_registrazione)
end if

// Calcolo rate in base al pagamento
if ls_flag_esigibilita_iva <> "S" or isnull(ls_flag_esigibilita_iva)then
	if uof_calcola_rate(ld_tot_imp,ld_tot_iva,ls_cod_pagamento,ldd_data_fattura,ldd_rate_data, ld_rate_importo,ls_rate_tipo, ls_modalita_pagamento, ll_cont_rate,fs_messaggio) <> 0 then
		fs_messaggio = "Errore in calcolo rate pagamento.~n" + fs_messaggio
		return -1
	end if
else
	// caso split paymento, cacolo le rate solo solla base dell'imponibile
	if uof_calcola_rate(ld_tot_imp, 0,ls_cod_pagamento,ldd_data_fattura,ldd_rate_data, ld_rate_importo,ls_rate_tipo, ls_modalita_pagamento, ll_cont_rate,fs_messaggio) <> 0 then
		fs_messaggio = "Errore in calcolo rate pagamento.~n" + fs_messaggio
		return -1
	end if
end if
// Sostituzione date in base alle eventuali impostazioni del cliente / fornitore / contatto

for ll_i = 1 to ll_cont_rate
	
	if uof_sostituzione_date(ldd_rate_data[ll_i],ldd_rate_data[ll_i], ls_cod_cliente, ls_cod_fornitore, ls_cod_contatto, fs_messaggio) <> 0 then
		fs_messaggio = "Errore in sostituzione date.~n" + fs_messaggio
		return -1
	end if
	
next

// Cancellazione delle scadenze già esistenti

choose case is_tipo_documento
		
	case "fat_ven"	
		
		delete from scad_fat_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :il_anno_registrazione and
				num_registrazione = :il_num_registrazione;

		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di scad_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
	case "fat_acq"
		
		delete from scad_fat_acq
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :il_anno_registrazione and
				num_registrazione = :il_num_registrazione;

		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di scad_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
	case "ord_ven"	
		
		delete from scad_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :il_anno_registrazione and
				num_registrazione = :il_num_registrazione;

		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di scad_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
	case "ord_acq"
		
		delete from scad_ord_acq
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :il_anno_registrazione and
				num_registrazione = :il_num_registrazione;

		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella delete di scad_ord_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
end choose
		
// Scrittura delle rate calcolate

for ll_i = 1 to ll_cont_rate
			
	ldt_rata_data = datetime(ldd_rate_data[ll_i])
		
	ld_rata_importo = ld_rate_importo[ll_i]
			
	ld_rata_valuta = ld_rate_importo[ll_i] / id_cambio
			
	uof_arrotonda(ld_rata_valuta, id_precisione_valuta, "euro")
	
	choose case is_tipo_documento
		
		case "fat_ven"
			
			insert into scad_fat_ven
						(cod_azienda,
						anno_registrazione,
						num_registrazione,
						data_scadenza,
						prog_scadenza,
						imp_rata,
						imp_rata_valuta,
						bolli_rata,
						cod_tipo_pagamento,
						fatel_modalita_pagamento,
						flag_pagata)
			values 
						(:s_cs_xx.cod_azienda,
						:il_anno_registrazione,
						:il_num_registrazione,
						:ldt_rata_data,
						:ll_i,
						:ld_rata_importo,
						:ld_rata_valuta,
						0,
						:ls_rate_tipo[ll_i], 
						:ls_modalita_pagamento[ll_i],
						'N');
			  
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella insert di scad_fat_ven: " + sqlca.sqlerrtext
				return -1
			end if
			
			
		case "fat_acq"
			
			insert into scad_fat_acq
						(cod_azienda,
						anno_registrazione,
						num_registrazione,
						data_scadenza,
						prog_scadenza,
						imp_rata,
						imp_rata_valuta,
						cod_tipo_pagamento)
			values 
						(:s_cs_xx.cod_azienda,
						:il_anno_registrazione,
						:il_num_registrazione,
						:ldt_rata_data,
						:ll_i,
						:ld_rata_importo,
						:ld_rata_valuta,
						:ls_rate_tipo[ll_i]);
			  
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella insert di scad_fat_acq: " + sqlca.sqlerrtext
				return -1
			end if
			
		case "ord_ven"
			
			insert into scad_ord_ven
						(cod_azienda,
						anno_registrazione,
						num_registrazione,
						data_scadenza,
						prog_scadenza,
						imp_rata,
						imp_rata_valuta,
						bolli_rata,
						cod_tipo_pagamento)
			values		
						(:s_cs_xx.cod_azienda,
						:il_anno_registrazione,
						:il_num_registrazione,
						:ldt_rata_data,
						:ll_i,
						:ld_rata_importo,
						:ld_rata_valuta,
						0,
						:ls_rate_tipo[ll_i]);
			  
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella insert di scad_ord_ven: " + sqlca.sqlerrtext
				return -1
			end if
			
			
		case "ord_acq"
			
			insert into scad_ord_acq
						(cod_azienda,
						anno_registrazione,
						num_registrazione,
						data_scadenza,
						prog_scadenza,
						imp_rata,
						imp_rata_valuta,
						bolli_rata,
						cod_tipo_pagamento)
			values		
						(:s_cs_xx.cod_azienda,
						:il_anno_registrazione,
						:il_num_registrazione,
						:ldt_rata_data,
						:ll_i,
						:ld_rata_importo,
						:ld_rata_valuta,
						0,
						:ls_rate_tipo[ll_i]);
			  
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella insert di scad_ord_acq: " + sqlca.sqlerrtext
				return -1
			end if
			
	end choose
	
next

return 0
end function

public function integer uof_calcola_totali_documento (ref string fs_messaggio);dec{4} ld_totale_imponibile_valuta, ld_totale_iva_valuta, ld_totale_documento_valuta, ld_totale_imponibile,&
		 ld_totale_iva, ld_totale_documento

	
// Viene aggiornata la tabella corrispondente al tipo di documento con i valori totali di iva e imponibile
choose case is_tipo_documento
	
	case "bol_ven"
	
		select sum(importo_valuta_iva),				 
				 sum(importo_iva)
		into	 :ld_totale_iva_valuta,				 
				 :ld_totale_iva
		from   iva_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di iva_bol_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
		select sum(imponibile_iva_valuta),
		   	 sum(imponibile_iva)
		into   :ld_totale_imponibile_valuta,
		       :ld_totale_imponibile
		from   det_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 cod_tipo_det_ven not in (select cod_tipo_det_ven
				 								  from   tab_tipi_det_ven
												  where  cod_azienda = :s_cs_xx.cod_azienda and
												         flag_sola_iva = 'S');
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_bol_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ld_totale_imponibile_valuta) then
			ld_totale_imponibile_valuta = 0
		end if
		
		if isnull(ld_totale_imponibile) then
			ld_totale_imponibile = 0
		end if
		
		if isnull(ld_totale_iva_valuta) then
			ld_totale_iva_valuta = 0
		end if
		
		if isnull(ld_totale_iva) then
			ld_totale_iva = 0
		end if
		
		ld_totale_documento_valuta = ld_totale_imponibile_valuta + ld_totale_iva_valuta
		ld_totale_documento = ld_totale_imponibile + ld_totale_iva
		
		update tes_bol_ven
		set	 imponibile_iva_valuta = :ld_totale_imponibile_valuta,
				 importo_iva_valuta = :ld_totale_iva_valuta,
				 tot_val_bolla_valuta = :ld_totale_documento_valuta,
				 imponibile_iva = :ld_totale_imponibile,
				 importo_iva = :ld_totale_iva,
				 tot_val_bolla = :ld_totale_documento
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_bol_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "bol_acq"
	
		select sum(importo_valuta_iva),
				 sum(importo_iva)
		into	 :ld_totale_iva_valuta,
				 :ld_totale_iva
		from   iva_bol_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_bolla_acq = :il_anno_registrazione and
				 num_bolla_acq = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di iva_bol_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
		select sum(imponibile_iva_valuta),
		   	 sum(imponibile_iva)
		into   :ld_totale_imponibile_valuta,
		       :ld_totale_imponibile
		from   det_bol_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_bolla_acq = :il_anno_registrazione and
				 num_bolla_acq = :il_num_registrazione and
				 cod_tipo_det_acq not in (select cod_tipo_det_acq
				 								  from   tab_tipi_det_acq
												  where  cod_azienda = :s_cs_xx.cod_azienda and
												         flag_sola_iva = 'S');
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_bol_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ld_totale_imponibile_valuta) then
			ld_totale_imponibile_valuta = 0
		end if
		
		if isnull(ld_totale_imponibile) then
			ld_totale_imponibile = 0
		end if
		
		if isnull(ld_totale_iva_valuta) then
			ld_totale_iva_valuta = 0
		end if
		
		if isnull(ld_totale_iva) then
			ld_totale_iva = 0
		end if
		
		ld_totale_documento_valuta = ld_totale_imponibile_valuta + ld_totale_iva_valuta
		ld_totale_documento = ld_totale_imponibile + ld_totale_iva
		
		update tes_bol_acq
		set	 imponibile_valuta_iva = :ld_totale_imponibile_valuta,
				 importo_valuta_iva = :ld_totale_iva_valuta,
				 tot_val_bol_acq = :ld_totale_documento,
				 imponibile_iva = :ld_totale_imponibile,
				 importo_iva = :ld_totale_iva,
				 tot_valuta_bol_acq = :ld_totale_documento_valuta
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_bolla_acq = :il_anno_registrazione and
				 num_bolla_acq = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_bol_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "fat_ven"	
		
		select sum(importo_valuta_iva),
				 sum(importo_iva)
		into	 :ld_totale_iva_valuta,
				 :ld_totale_iva
		from   iva_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di iva_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
		select sum(imponibile_iva_valuta),
		   	 sum(imponibile_iva)
		into   :ld_totale_imponibile_valuta,
		       :ld_totale_imponibile
		from   det_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 cod_tipo_det_ven not in (select cod_tipo_det_ven
				 								  from   tab_tipi_det_ven
												  where  cod_azienda = :s_cs_xx.cod_azienda and
												         flag_sola_iva = 'S');
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ld_totale_imponibile_valuta) then
			ld_totale_imponibile_valuta = 0
		end if
		
		if isnull(ld_totale_imponibile) then
			ld_totale_imponibile = 0
		end if
		
		if isnull(ld_totale_iva_valuta) then
			ld_totale_iva_valuta = 0
		end if
		
		if isnull(ld_totale_iva) then
			ld_totale_iva = 0
		end if
		
		ld_totale_documento_valuta = ld_totale_imponibile_valuta + ld_totale_iva_valuta
		ld_totale_documento = ld_totale_imponibile + ld_totale_iva
		
		update tes_fat_ven
		set	 imponibile_iva_valuta = :ld_totale_imponibile_valuta,
				 importo_iva_valuta = :ld_totale_iva_valuta,
				 tot_fattura_valuta = :ld_totale_documento_valuta,
				 imponibile_iva = :ld_totale_imponibile,
				 importo_iva = :ld_totale_iva,
				 tot_fattura = :ld_totale_documento
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if	 
	
	case "fat_acq"	
		
		select sum(importo_valuta_iva),
				 sum(importo_iva)
		into	 :ld_totale_iva_valuta,
				 :ld_totale_iva
		from   iva_fat_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di iva_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
		select sum(imponibile_iva_valuta),
		   	 sum(imponibile_iva)
		into   :ld_totale_imponibile_valuta,
		       :ld_totale_imponibile
		from   det_fat_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 cod_tipo_det_acq not in (select cod_tipo_det_acq
				 								  from   tab_tipi_det_acq
												  where  cod_azienda = :s_cs_xx.cod_azienda and
												         flag_sola_iva = 'S');
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ld_totale_imponibile_valuta) then
			ld_totale_imponibile_valuta = 0
		end if
		
		if isnull(ld_totale_imponibile) then
			ld_totale_imponibile = 0
		end if
		
		if isnull(ld_totale_iva_valuta) then
			ld_totale_iva_valuta = 0
		end if
		
		if isnull(ld_totale_iva) then
			ld_totale_iva = 0
		end if
		
		ld_totale_documento_valuta = ld_totale_imponibile_valuta + ld_totale_iva_valuta
		ld_totale_documento = ld_totale_imponibile + ld_totale_iva
		
		update tes_fat_acq
		set	 imponibile_valuta_iva = :ld_totale_imponibile_valuta,
				 importo_valuta_iva = :ld_totale_iva_valuta,
				 tot_valuta_fat_acq = :ld_totale_documento_valuta,
				 imponibile_iva = :ld_totale_imponibile,
				 importo_iva = :ld_totale_iva,
				 tot_val_fat_acq = :ld_totale_documento
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_fat_acq: " + sqlca.sqlerrtext
			return -1
		end if	 
	
	case "off_ven"
		
		select sum(importo_valuta_iva),
				 sum(importo_iva)
		into	 :ld_totale_iva_valuta,
				 :ld_totale_iva
		from   iva_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di iva_off_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
		select sum(imponibile_iva_valuta),
		   	 sum(imponibile_iva)
		into   :ld_totale_imponibile_valuta,
		       :ld_totale_imponibile
		from   det_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 cod_tipo_det_ven not in (select cod_tipo_det_ven
				 								  from   tab_tipi_det_ven
												  where  cod_azienda = :s_cs_xx.cod_azienda and
												         flag_sola_iva = 'S');
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_off_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ld_totale_imponibile_valuta) then
			ld_totale_imponibile_valuta = 0
		end if
		
		if isnull(ld_totale_imponibile) then
			ld_totale_imponibile = 0
		end if
		
		if isnull(ld_totale_iva_valuta) then
			ld_totale_iva_valuta = 0
		end if
		
		if isnull(ld_totale_iva) then
			ld_totale_iva = 0
		end if
		
		ld_totale_documento_valuta = ld_totale_imponibile_valuta + ld_totale_iva_valuta
		ld_totale_documento = ld_totale_imponibile + ld_totale_iva
		
		update tes_off_ven
		set	 imponibile_iva_valuta = :ld_totale_imponibile_valuta,
				 importo_iva_valuta = :ld_totale_iva_valuta,
				 tot_val_offerta_valuta = :ld_totale_documento_valuta,
				 imponibile_iva = :ld_totale_imponibile,
				 importo_iva = :ld_totale_iva,
				 tot_val_offerta = :ld_totale_documento
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_off_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "off_acq"
		
		select sum(importo_valuta_iva),
				 sum(importo_iva)
		into	 :ld_totale_iva_valuta,
				 :ld_totale_iva
		from   iva_off_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di iva_off_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
		select sum(imponibile_iva_valuta),
		   	 sum(imponibile_iva)
		into   :ld_totale_imponibile_valuta,
		       :ld_totale_imponibile
		from   det_off_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 cod_tipo_det_acq not in (select cod_tipo_det_acq
				 								  from   tab_tipi_det_acq
												  where  cod_azienda = :s_cs_xx.cod_azienda and
												         flag_sola_iva = 'S');
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_off_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ld_totale_imponibile_valuta) then
			ld_totale_imponibile_valuta = 0
		end if
		
		if isnull(ld_totale_imponibile) then
			ld_totale_imponibile = 0
		end if
		
		if isnull(ld_totale_iva_valuta) then
			ld_totale_iva_valuta = 0
		end if
		
		if isnull(ld_totale_iva) then
			ld_totale_iva = 0
		end if
		
		ld_totale_documento_valuta = ld_totale_imponibile_valuta + ld_totale_iva_valuta
		ld_totale_documento = ld_totale_imponibile + ld_totale_iva
		
		update tes_off_acq
		set	 imponibile_iva_valuta = :ld_totale_imponibile_valuta,
				 importo_iva_valuta = :ld_totale_iva_valuta,
				 tot_val_offerta_valuta = :ld_totale_documento_valuta,
				 imponibile_iva = :ld_totale_imponibile,
				 importo_iva = :ld_totale_iva,
				 tot_val_offerta = :ld_totale_documento
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_off_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "ord_ven"
		
		select sum(importo_valuta_iva),
				 sum(importo_iva)
		into	 :ld_totale_iva_valuta,
				 :ld_totale_iva
		from   iva_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di iva_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
		select sum(imponibile_iva_valuta),
		   	 sum(imponibile_iva)
		into   :ld_totale_imponibile_valuta,
		       :ld_totale_imponibile
		from   det_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 cod_tipo_det_ven not in (select cod_tipo_det_ven
				 								  from   tab_tipi_det_ven
												  where  cod_azienda = :s_cs_xx.cod_azienda and
												         flag_sola_iva = 'S');
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ld_totale_imponibile_valuta) then
			ld_totale_imponibile_valuta = 0
		end if
		
		if isnull(ld_totale_imponibile) then
			ld_totale_imponibile = 0
		end if
		
		if isnull(ld_totale_iva_valuta) then
			ld_totale_iva_valuta = 0
		end if
		
		if isnull(ld_totale_iva) then
			ld_totale_iva = 0
		end if
		
		ld_totale_documento_valuta = ld_totale_imponibile_valuta + ld_totale_iva_valuta
		ld_totale_documento = ld_totale_imponibile + ld_totale_iva
		
		update tes_ord_ven
		set	 imponibile_iva_valuta = :ld_totale_imponibile_valuta,
				 importo_iva_valuta = :ld_totale_iva_valuta,
				 tot_val_ordine_valuta = :ld_totale_documento_valuta,
				 imponibile_iva = :ld_totale_imponibile,
				 importo_iva = :ld_totale_iva,
				 tot_val_ordine = :ld_totale_documento
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "ord_acq"
		
		select sum(importo_valuta_iva),
				 sum(importo_iva)
		into	 :ld_totale_iva_valuta,
				 :ld_totale_iva
		from   iva_ord_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di iva_ord_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
		select sum(imponibile_iva_valuta),
		   	 sum(imponibile_iva)
		into   :ld_totale_imponibile_valuta,
		       :ld_totale_imponibile
		from   det_ord_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione and
				 cod_tipo_det_acq not in (select cod_tipo_det_acq
				 								  from   tab_tipi_det_acq
												  where  cod_azienda = :s_cs_xx.cod_azienda and
												         flag_sola_iva = 'S');
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di det_ord_acq: " + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ld_totale_imponibile_valuta) then
			ld_totale_imponibile_valuta = 0
		end if
		
		if isnull(ld_totale_imponibile) then
			ld_totale_imponibile = 0
		end if
		
		if isnull(ld_totale_iva_valuta) then
			ld_totale_iva_valuta = 0
		end if
		
		if isnull(ld_totale_iva) then
			ld_totale_iva = 0
		end if
		
		ld_totale_documento_valuta = ld_totale_imponibile_valuta + ld_totale_iva_valuta
		ld_totale_documento = ld_totale_imponibile + ld_totale_iva
		
		update tes_ord_acq
		set	 imponibile_iva_valuta = :ld_totale_imponibile_valuta,
				 importo_iva_valuta = :ld_totale_iva_valuta,
				 tot_val_ordine_valuta = :ld_totale_documento_valuta,
				 imponibile_iva = :ld_totale_imponibile,
				 importo_iva = :ld_totale_iva,
				 tot_val_ordine = :ld_totale_documento
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if	sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella update di tes_ord_acq: " + sqlca.sqlerrtext
			return -1
		end if
	
end choose

return 0
end function

public function integer uof_esposizione_cliente (ref string fs_messaggio);/*
	Funzione che verifica se il totale del documento di vendita fa sì che l'esposizione del cliente ecceda il relativo fido concesso.
	Valori di ritorno:
		0: controllo eseguito con successo
		-1: errore della procedura (specificato in FS_MESSAGGIO)

	Enrico 30/11/2004: eseguita correzione per bolle di uscita che hanno il codice fornitore.
*/



datetime	ldt_today

string		ls_cod_cliente, ls_check, ls_cod_fornitore

uo_fido_cliente luo_fido

long ll_ret


//Verifica che si tratti di un documento di vendita
if is_tipo_documento <> "bol_ven" and &
   is_tipo_documento <> "fat_ven" and &
   is_tipo_documento <> "off_ven" and &
   is_tipo_documento <> "ord_ven" then
	return 0
end if

//Verifica se nei parametri aziendali è abilitato il controllo del fuori fido
select
	flag
into
	:ls_check
from
	parametri_azienda
where
	cod_azienda = :s_cs_xx.cod_azienda and
	flag_parametro = 'F' and
	cod_parametro = 'CEC';

if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore in lettura parametro aziendale CEC: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_check) or ls_check <> "S" then
	return 0
end if

//Lettura del cliente dall'intestazione del documento
choose case is_tipo_documento
	
	case "bol_ven"
	
		select
			cod_cliente, cod_fornitore
		into
			:ls_cod_cliente, :ls_cod_fornitore
		from
			tes_bol_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :il_anno_registrazione and
			num_registrazione = :il_num_registrazione;
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in lettura cliente da tes_bol_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
		// se nella bolla è stato caricato il fornitore non controllo niente.
		if not isnull(ls_cod_fornitore) or len(ls_cod_fornitore) > 0 then return 0
		
		//ticket 2014/513
		if isnull(ls_cod_cliente) then
			//probabile caso di ddt di vendita verso contatto e non su cliente
			//quindi non vado avanti nel controllo del fido
			return 0
		end if
		
	case "fat_ven"	
		
		select
			cod_cliente
		into
			:ls_cod_cliente
		from
			tes_fat_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :il_anno_registrazione and
			num_registrazione = :il_num_registrazione;
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in lettura cliente da tes_fat_ven: " + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ls_cod_cliente) then
			//probabile caso di fattura di vendita pro-forma su contatto e non su cliente
			//quindi non vado avanti nel controllo del fido
			return 0
		end if
	
	case "off_ven"
		
		select
			cod_cliente
		into
			:ls_cod_cliente
		from
			tes_off_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :il_anno_registrazione and
			num_registrazione = :il_num_registrazione;
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in lettura cliente da tes_off_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
	case "ord_ven"
		
		select
			cod_cliente
		into
			:ls_cod_cliente
		from
			tes_ord_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :il_anno_registrazione and
			num_registrazione = :il_num_registrazione;
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in lettura cliente da tes_ord_ven: " + sqlca.sqlerrtext
			return -1
		end if
	
end choose

ldt_today = datetime(today(),00:00:00)

//Chiamata alla funzione per la verifica del fuori fido
luo_fido = create uo_fido_cliente

ll_ret = luo_fido.uof_check_cliente(ls_cod_cliente,ldt_today,fs_messaggio)
destroy luo_fido

return ll_ret  
//-1 se errore (il msg lo da dopo)
//0 se tutto a posto oppure fido superato oppure scaduti non pagati e NON devo bloccare (eventuali msg già dati)
//2 se tutto a posto oppure fido superato oppure scaduti non pagati e DEVO bloccare (eventuali msg già dati)


//if luo_fido.uof_check_cliente(ls_cod_cliente,ldt_today,fs_messaggio) <> 0 then
//	destroy luo_fido
//	return -1
//end if
//
//destroy luo_fido
//
//return 0
end function

public function integer uof_calcola_documento (long fl_anno_registrazione, long fl_num_registrazione, string fs_tipo_documento, ref string fs_messaggio);long   		ll_return, ll_count
string 		ls_messaggio, ls_cod_lire, ls_flag, ls_flag_contabilita,ls_flag_calcola_fat_confermata, ls_flag_tipo_fat_ven, &
       			ls_cod_tipo_fat_ven, ls_SBF, ls_cod_cliente, ls_cod_pagamento
datetime 	ldt_data_fattura, ldt_data_decorrenza_pagamento
boolean 	lb_calcola_spese_bancarie

// carico parametro per attivazione della gestione centri di costo
select flag
into   :is_gestione_cc
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'GCC';

if sqlca.sqlcode <> 0 then
	is_gestione_cc = "N"
end if

//---------------------------------------------------------------------
//Chiesto da Antonella Bellin di PTENDA
//Se il parametro az. SBF  esiste ed è posto a S allora il calcolo delle spese bancarie nelle fatture di vendita potrebbe non essere effettuato 
//in base a tali vincoli:
//-	se si tratta di una nota di credito non fare il calcolo
//-	se nello stesso mese (data doc. fiscale) esiste una fattura precedente dello stesso cliente con lo stesso tipo di pagamento non fare il calcolo
//-	in tutti gli altri casi fare il calcolo

select flag
into   :ls_SBF
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'SBF' and flag_parametro='F';

if sqlca.sqlcode <> 0 then
	ls_SBF = "N"
end if
//----------------------------------------------------------------------

il_anno_registrazione = fl_anno_registrazione
il_num_registrazione = fl_num_registrazione
is_tipo_documento = fs_tipo_documento

// Controllo correttezza del valore di precisione per la valuta euro (letto nel constructor)
if isnull(id_precisione) then
	fs_messaggio = "Valuta EURO non trovata.~nVerificare l'impostazione del parametro EUR in parametri aziendali"
	return -1
elseif id_precisione = -1 then
	fs_messaggio = "Errore in lettura precisione valuta EURO da tab_valute"
	return -1
elseif id_precisione = 0 then
	fs_messaggio = "Precisione non valida per la valuta EURO.~nVerificare le impostazioni in tabella valute"
	return -1
end if

// Si controlla che sia stato specificato un tipo di documento valido
choose case fs_tipo_documento
		
	case "bol_acq","fat_acq","off_acq","ord_acq"
		
		if fs_tipo_documento = "fat_acq" then
			select flag_contabilita, flag_cc_manuali
			into   :ls_flag_contabilita, :is_flag_cc_manuali
			from   tes_fat_acq
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione;
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore in controllo del flag_contabilita nella fattura di acquisto~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
			if isnull(is_flag_cc_manuali) then is_flag_cc_manuali = "N"
			
			// Se la fattura è contabilizzata non eseguo più il calcolo altrimenti si incasina tutto
			if ls_flag_contabilita = "S" then return 0
		end if
		
	case "bol_ven","off_ven","ord_ven","fat_ven"
		
		if fs_tipo_documento = "fat_ven" then
			select flag_contabilita, cod_tipo_fat_ven, flag_cc_manuali
			into   :ls_flag_contabilita, :ls_cod_tipo_fat_ven, :is_flag_cc_manuali
			from   tes_fat_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione;
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore in controllo del flag_contabilita nella fattura di vendita~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
			if isnull(is_flag_cc_manuali) then is_flag_cc_manuali = "N"
			
			select flag_tipo_fat_ven
			into   :ls_flag_tipo_fat_ven
			from   tab_tipi_fat_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore in controllo del flag_tipo fattura nella tabella tipi fatture~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
			// controllo se calcolare le fatture contabilizzate
			ls_flag_calcola_fat_confermata = "N"
			select flag_calcola_fat_confermata
			into   :ls_flag_calcola_fat_confermata
			from   con_vendite
			where  cod_azienda = :s_cs_xx.cod_azienda;
			
			
			// Se la fattura non è proforma ed è già contabilizzata salto il calcolo del documento
			if ls_flag_tipo_fat_ven <> "P" and ls_flag_tipo_fat_ven <> "F" then
				if ls_flag_contabilita = "S" and ls_flag_calcola_fat_confermata = "N" then
					return 0			// no calcolo fattura
				end if
			end if
		end if
		
		// Viene richiamata la funzione per la cancellazione delle righe di spese e addebiti presenti nel documento
		ll_return = uof_cancella_dettagli(ls_messaggio)

		if ll_return = -1 then
			fs_messaggio = "Errore nella cancellazione dei dettagli aggiunti.~n" + ls_messaggio
			return -1
		end if
		
	case else
		
		fs_messaggio = "Tipo di documento specificato non valido. (bol/fat/off/ord)_(acq/ven)"
		return -1
		
end choose

// Viene individuato il codice valuta impostato per le lire
select stringa
into   :ls_cod_lire
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'CVL';
		 
if sqlca.sqlcode = -1 then
	fs_messaggio = "Errore nella select di parametri_azienda: " + sqlca.sqlerrtext
	return -1
end if

if sqlca.sqlcode = 100 then
	fs_messaggio = "Errore nella select di parametri_azienda: parametro CVL non trovato"
	return -1
end if

// Impostazione dell'oggetto datastore tramite l'assegnazione della datawindow corrispondente al documento specificato
// Impostazione delle variabili che serviranno ad identificare il nome delle colonne quantita e prog_riga
// Lettura dei valori di codice e cambio per la valuta corrente dalla testata del documento 
choose case is_tipo_documento
	
	case "bol_ven"
		
		ids_righe_documento.dataobject = "d_ds_det_bol_ven"
		is_colonna_quantita = "quan_consegnata"
		is_colonna_prog_riga = "prog_riga_bol_ven"
		
		select cod_valuta,
				 cambio_ven
		into   :is_cod_valuta,
				 :id_cambio
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di tes_bol_ven: " + sqlca.sqlerrtext
			return -1
		elseif sqlca.sqlcode = 100 then
			fs_messaggio = "Errore nella select di tes_bol_ven: documento non trovato"
			return -1
		end if
		
		if is_cod_valuta = ls_cod_lire then
			id_cambio = 0.00051646
		end if
		
		// *********************************************************************************************************
		// *** Michela 24/01/2006: alla bolla vengono aggiunti gli addebiti del cliente; specifica PTENDA funzione 5
		ll_return = uof_addebiti_cliente_bolle(fs_messaggio)
		
		if ll_return = -1 then
			fs_messaggio = "Errore nel calcolo degli addebiti cliente.~n" + fs_messaggio
			return -1
		end if				
		// *********************************************************************************************************
		// *********************************************************************************************************
		
	case "bol_acq"
		
		ids_righe_documento.dataobject = "d_ds_det_bol_acq"
		is_colonna_quantita = "quan_arrivata"
		is_colonna_prog_riga = "prog_riga_bolla_acq"
		
		select cod_valuta,
				 cambio_acq
		into   :is_cod_valuta,
				 :id_cambio
		from   tes_bol_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_bolla_acq = :il_anno_registrazione and
				 num_bolla_acq = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di tes_bol_acq: " + sqlca.sqlerrtext
			return -1
		elseif sqlca.sqlcode = 100 then
			fs_messaggio = "Errore nella select di tes_bol_acq: documento non trovato"
			return -1
		end if
		
		if is_cod_valuta = ls_cod_lire then
			id_cambio = 0.00051646
		end if
	
	case "fat_ven"
		
		ids_righe_documento.dataobject = "d_ds_det_fat_ven"		
		is_colonna_quantita = "quan_fatturata"
		is_colonna_prog_riga = "prog_riga_fat_ven"
		
		select cod_valuta,
				 cambio_ven
		into   :is_cod_valuta,
				 :id_cambio
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
			    anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;

		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di tes_fat_ven: " + sqlca.sqlerrtext
			return -1
		elseif sqlca.sqlcode = 100 then
			fs_messaggio = "Errore nella select di tes_fat_ven: documento non trovato"
			return -1	
		end if
		
		if is_cod_valuta = ls_cod_lire then
			id_cambio = 0.00051646
		end if
		
		// Alla fattura vengono aggiunti gli addebiti del cliente		
		ll_return = uof_addebiti_cliente(fs_messaggio)
		
		if ll_return = -1 then
			fs_messaggio = "Errore nel calcolo degli addebiti cliente.~n" + ls_messaggio
			return -1
		end if

	case "fat_acq"
		
		ids_righe_documento.dataobject = "d_ds_det_fat_acq"		
		is_colonna_quantita = "quan_fatturata"
		is_colonna_prog_riga = "prog_riga_fat_acq"
		
		select cod_valuta,
				 cambio_acq
		into   :is_cod_valuta,
				 :id_cambio
		from   tes_fat_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
			    anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;

		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di tes_fat_acq: " + sqlca.sqlerrtext
			return -1
		elseif sqlca.sqlcode = 100 then
			fs_messaggio = "Errore nella select di tes_fat_acq: documento non trovato"
			return -1	
		end if
		
		if is_cod_valuta = ls_cod_lire then
			id_cambio = 0.00051646
		end if
		
	case "off_ven"
		
		ids_righe_documento.dataobject = "d_ds_det_off_ven"		
		is_colonna_quantita = "quan_offerta"
		is_colonna_prog_riga = "prog_riga_off_ven"
		
		select cod_valuta,
				 cambio_ven
		into   :is_cod_valuta,
				 :id_cambio
		from   tes_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di tes_off_ven: " + sqlca.sqlerrtext
			return -1
		elseif sqlca.sqlcode = 100 then
			fs_messaggio = "Errore nella select di tes_off_ven: documento non trovato"
			return -1	
		end if
		
		if is_cod_valuta = ls_cod_lire then
			id_cambio = 0.00051646
		end if
	
	case "off_acq"
		
		ids_righe_documento.dataobject = "d_ds_det_off_acq"		
		is_colonna_quantita = "quan_ordinata"
		is_colonna_prog_riga = "prog_riga_off_acq"
		
		select cod_valuta,
				 cambio_acq
		into   :is_cod_valuta,
				 :id_cambio
		from   tes_off_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di tes_off_acq: " + sqlca.sqlerrtext
			return -1
		elseif sqlca.sqlcode = 100 then
			fs_messaggio = "Errore nella select di tes_off_acq: documento non trovato"
			return -1	
		end if
		
		if is_cod_valuta = ls_cod_lire then
			id_cambio = 0.00051646
		end if
	
	case "ord_ven"
		
		ids_righe_documento.dataobject = 'd_ds_det_ord_ven'	
		is_colonna_quantita = "quan_ordine"
		is_colonna_prog_riga = "prog_riga_ord_ven"
		
		select cod_valuta,
				 cambio_ven
		into   :is_cod_valuta,
				 :id_cambio
		from   tes_ord_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di tes_ord_ven: " + sqlca.sqlerrtext
			return -1
		elseif sqlca.sqlcode = 100 then
			fs_messaggio = "Errore nella select di tes_ord_ven: documento non trovato"
			return -1	
		end if
		
		if is_cod_valuta = ls_cod_lire then
			id_cambio = 0.00051646
		end if

	case "ord_acq"
		
		ids_righe_documento.dataobject = 'd_ds_det_ord_acq'	
		is_colonna_quantita = "quan_ordinata"
		is_colonna_prog_riga = "prog_riga_ordine_acq"
		
		select cod_valuta,
				 cambio_acq
		into   :is_cod_valuta,
				 :id_cambio
		from   tes_ord_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Errore nella select di tes_ord_acq: " + sqlca.sqlerrtext
			return -1
		elseif sqlca.sqlcode = 100 then
			fs_messaggio = "Errore nella select di tes_ord_acq: documento non trovato"
			return -1	
		end if
		
		if is_cod_valuta = ls_cod_lire then
			id_cambio = 0.00051646
		end if

end choose

// controllo blocco calcolo documenti in LIRE
select flag
into   :ls_flag
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'BCL';
if sqlca.sqlcode <> 0 then 
	ls_flag = "N"
end if
if is_cod_valuta = ls_cod_lire and ls_flag = "S" then return 0

// Viene letto il valore di precisione per la valuta corrente
select precisione
into   :id_precisione_valuta
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = :is_cod_valuta;
		 
if sqlca.sqlcode = -1 then
	fs_messaggio = "Errore nella select di tab_valute: " + sqlca.sqlerrtext
	return -1
end if

// Il valore di precisione non può essere zero, nullo o negativo
if isnull(id_precisione_valuta) or id_precisione_valuta <= 0 then
	fs_messaggio = "Errore nella select di tab_valute: il valore di precisione non può essere zero, nullo o negativo"
	return -1
end if

// Impostazione dell'oggetto transazione per il datastore ids_righe_documento
ll_return = ids_righe_documento.settransobject(sqlca)

// Viene richiamato il metodo retrieve della datawindow
ll_return = ids_righe_documento.retrieve(s_cs_xx.cod_azienda, il_anno_registrazione, il_num_registrazione)

if ll_return = -1 then	
	fs_messaggio = "Errore nella retrieve del datastore ids_righe_documento: datawindow " + ids_righe_documento.dataobject
	return -1
elseif ll_return = 0 then	// Se non ci sono dettagli il documento viene "resettato"
	ll_return = uof_azzera_documento(ls_messaggio)
	if ll_return = -1 then
		fs_messaggio = "Errore nell'azzeramento del documento.~n" + ls_messaggio
		return -1
	end if	
	return 0
end if

// Viene richiamata la funzione per il calcolo degli imponibili di riga
ll_return = uof_calcola_imponibile_riga(ls_messaggio)

if ll_return = -1 then
	fs_messaggio = "Errore nel calcolo dell'imponibile.~n" + ls_messaggio
	return -1
end if	

// Viene richiamato il metodo retrieve per aggiornare il datastore alle ultime modifiche
ll_return = ids_righe_documento.retrieve(s_cs_xx.cod_azienda, il_anno_registrazione, il_num_registrazione)

if ll_return = -1 then
	fs_messaggio = "Errore nel retrieve della datawindow " + ids_righe_documento.dataobject
	return -1
end if

// Viene richiamata la funzione per la totalizzazione dell'iva
ll_return = uof_totalizza_iva(ls_messaggio)

if ll_return = -1 then
	fs_messaggio = "Errore nella totalizzazione dell'iva.~n" + ls_messaggio
	return -1
end if

// Viene richiamata la funzione per il calcolo dei totali del documento
ll_return = uof_calcola_totali_documento(ls_messaggio)

if ll_return = -1 then
	fs_messaggio = "Errore nel calcolo dei totali del documento.~n" + ls_messaggio
	return -1
end if

//#########################################################################################
// Solo per fatture: spese bancarie e di trasporto, si ricalcola il documento, calcolo contropartite e scadenze
if is_tipo_documento = "fat_ven" then
	
	lb_calcola_spese_bancarie = false
	if ls_SBF<>"S" then
		//funzionamento tradizionale
		//calcola sempre le spese bancarie
		lb_calcola_spese_bancarie = true
	else
		//parametro SBF impostato: verifica se calcolare le spese bancarie
		if ls_flag_tipo_fat_ven="N" then //se N allora è Nota di Credito
			//non calcolare
			lb_calcola_spese_bancarie = false
		else
			//verifica se esiste per lo stesso cliente nello stesso mese (data doc. fiscale) una fattura di vendita
			//precedente con la stessa tipologia di pagamento
			
			select cod_cliente, cod_pagamento, data_fattura, data_decorrenza_pagamento
			into :ls_cod_cliente, :ls_cod_pagamento, :ldt_data_fattura, :ldt_data_decorrenza_pagamento
			from tes_fat_ven
			where 	cod_azienda=:s_cs_xx.cod_azienda and
						anno_registrazione=:fl_anno_registrazione and 
						num_registrazione=:fl_num_registrazione;
			
			if sqlca.sqlcode = -1 then
				fs_messaggio = "Errore nella select di tes_fat_ven (spese bancarie): " + sqlca.sqlerrtext
				return -1
			elseif sqlca.sqlcode = 100 then
				fs_messaggio = "Errore nella select di tes_fat_ven ()spese bancarie: documento non trovato"
				return -1	
			end if
			
			if isnull(ldt_data_fattura) or year(date(ldt_data_fattura))<=1950 or ls_cod_cliente="" or isnull(ls_cod_cliente) or 	ls_cod_pagamento="" or isnull(ls_cod_pagamento) then
						
				lb_calcola_spese_bancarie = true
			else
				
				long ll_year, ll_month, ll_day
				
				ll_year = year(date(ldt_data_fattura))
				ll_month = month(date(ldt_data_fattura))
				ll_day = day(date(ldt_data_fattura))
				
				select count(*)
				into :ll_count
				from tes_fat_ven
				where  	cod_azienda=:s_cs_xx.cod_azienda and
							cod_pagamento=:ls_cod_pagamento and
							cod_cliente=:ls_cod_cliente and 
							num_registrazione<:fl_num_registrazione and
							year(data_fattura)=:ll_year and 
							 month(data_fattura)=:ll_month and 
							 day(data_fattura)<=:ll_day;
				
				if sqlca.sqlcode = -1 then
					fs_messaggio = "Errore nella select count tes_fat_ven (spese bancarie): " + sqlca.sqlerrtext
					return -1
				end if
				
				if ll_count>0 then
					//trovate altre fatture precedenti del cliente: non calcolare le spese bancarie
					lb_calcola_spese_bancarie = false
				else
					//calcola le spese bancarie
					lb_calcola_spese_bancarie = true
				end if
			end if			
		end if
	end if
	
	if lb_calcola_spese_bancarie then
		// Vengono cercate le spese bancarie per la fattura
		ll_return = uof_spese_bancarie(ls_messaggio)
		
		if ll_return = -1 then
			fs_messaggio = "Errore nel calcolo delle spese bancarie della fattura.~n" + ls_messaggio
			return -1
		end if
	end if
	//fine modifica ################################################################################
	
	
	// Vengono applicati i costi di trasporto alla fattura
	ll_return = uof_costo_trasporto(ls_messaggio)
	
	if ll_return = -1 then
		fs_messaggio = "Errore nel calcolo delle spese trasporto della fattura.~n" + ls_messaggio
		return -1
	end if
	
	// Viene richiamato il metodo retrieve per aggiornare il datastore alle ultime modifiche
	ll_return = ids_righe_documento.retrieve(s_cs_xx.cod_azienda, il_anno_registrazione, il_num_registrazione)

	if ll_return = -1 then
		fs_messaggio = "Errore nel retrieve della datawindow " + ids_righe_documento.dataobject
		return -1
	end if
	
	// Viene richiamata la funzione per il calcolo degli imponibili di riga
	ll_return = uof_calcola_imponibile_riga(ls_messaggio)

	if ll_return = -1 then
		fs_messaggio = "Errore nel calcolo dell'imponibile.~n" + ls_messaggio
		return -1
	end if	

	// Viene richiamato il metodo retrieve per aggiornare il datastore alle ultime modifiche
	ll_return = ids_righe_documento.retrieve(s_cs_xx.cod_azienda, il_anno_registrazione, il_num_registrazione)

	if ll_return = -1 then
		fs_messaggio = "Errore nel retrieve della datawindow " + ids_righe_documento.dataobject
		return -1
	end if

	// Viene richiamata la funzione per la totalizzazione dell'iva
	ll_return = uof_totalizza_iva(ls_messaggio)

	if ll_return = -1 then
		fs_messaggio = "Errore nella totalizzazione dell'iva.~n" + ls_messaggio
		return -1
	end if

	// Viene richiamata la funzione per il calcolo dei totali
	ll_return = uof_calcola_totali_documento(ls_messaggio)

	if ll_return = -1 then
		fs_messaggio = "Errore nel calcolo dei totali del documento.~n" + ls_messaggio
		return -1
	end if
	
end if

if is_tipo_documento = "fat_ven" or is_tipo_documento = "fat_acq" then
	
	// Viene richiamata la funzione per il calcolo dei valori dei centri di costo
	// è indispoensabile che questa funzione venga eseguita prima del calcolo
	// delle contropartite
	ll_return = uof_calcola_centro_costo(ls_messaggio)
	
	if ll_return = -1 then
		fs_messaggio = "Errore nel calcolo importi dei centri di costo.~n" + ls_messaggio
		return -1
	end if
	
	// Viene richiamata la funzione per il calcolo delle contropartite
	ll_return = uof_calcola_contropartite(ls_messaggio)

	if ll_return = -1 then
		fs_messaggio = "Errore nel calcolo delle contropartite.~n" + ls_messaggio
		return -1
	end if
	
	// Viene richiamata la funzione totalizzazione centro di costo in testata
	ll_return = uof_totalizza_centro_costo(ls_messaggio)

	if ll_return = -1 then
		fs_messaggio = "Errore nelLa totalizzazione centri di costo in testata.~n" + ls_messaggio
		return -1
	end if
	
	// Viene richiamata la funzione per il calcolo delle scadenze
	ll_return = uof_calcola_scadenze(ls_messaggio)

	if ll_return = -1 then
		fs_messaggio = "Errore nel calcolo delle scadenze.~n" + ls_messaggio
		return -1
	end if
	
	// Viene richiamata la funzione per il controllo quadratura
	ll_return = uof_controllo_quadratura(ls_messaggio)

	if ll_return = -1 then
		fs_messaggio = "Errore nel controllo quadratura.~n" + ls_messaggio
		return -1
	end if

	if ll_return = 100 then
		fs_messaggio = "Controllo quadratura non superato:~n" + ls_messaggio
		return -1
	end if

elseif  is_tipo_documento = "ord_ven" or is_tipo_documento = "ord_acq" then
	// Viene richiamata la funzione per il calcolo delle scadenze
	ll_return = uof_calcola_scadenze(ls_messaggio)

	if ll_return = -1 then
		fs_messaggio = "Errore nel calcolo delle scadenze.~n" + ls_messaggio
		return -1
	end if
	
end if	

// Viene richiamata la funzione per la totalizzazione dei parziali del documento
ll_return = uof_totalizza_parziali(ls_messaggio)

if ll_return = -1 then
	fs_messaggio = "Errore nella totalizzazione dei parziali.~n" + ls_messaggio
	return -1
end if

if is_tipo_documento = "bol_ven" or is_tipo_documento = "fat_ven" or is_tipo_documento = "off_ven" or is_tipo_documento = "ord_ven" then

	// Viene richiamata la funzione per il calcolo delle provvigioni
	ll_return = uof_calcola_provvigioni(ls_messaggio)

	if ll_return = -1 then
		fs_messaggio = "Errore nel calcolo delle provvigioni.~n" + ls_messaggio
		return -1
	end if
	
end if

// Viene richiamata la funzione per il calcolo dello stato di evasione
ll_return = uof_calcola_evasione(ls_messaggio)

if ll_return = -1 then
	fs_messaggio = "Errore nel calcolo dello stato di evasione.~n" + ls_messaggio
	return -1
end if

// Il documento è stato calcolato. Viene richiamata la funzione per l'aggiornamento dello stato di calcolo.
ll_return = uof_fine_calcolo(ls_messaggio)

if ll_return = -1 then
	fs_messaggio = "Errore nell'aggiornamento dello stato di calcolo.~n" + ls_messaggio
	return -1
end if

//Donato 06-11-2008
//la var booleana può essere true solo se si proviene da
//w_det_ord_ven (STD e/o PTENDA)	script: click cb_calcola della w_tes_bol_ven_det
//w_det_bol_ven (STD)						script: click cb_calcola della w_tes_bol_ven_det
//w_configuratore	(PTENDA)
//e quindi il controllo sull'esposizione è stato già fatto 
//e si evita di dare due volte l'eventuale msg di superamento del fido
if ib_salta_esposiz_cliente then return 0

// Michele specifica interna 05/05/2004 - Viene richiamata la funzione per la verifica dell'esposizione del cliente
ll_return = uof_esposizione_cliente(ls_messaggio)

if ll_return = -1 then
	fs_messaggio = "Errore in verifica esposizione cliente.~n" + ls_messaggio
	return -1
end if

//Donato 05-11-2008 Modifica per specifica cliente PTENDA_ gestione fidi
if ll_return = 2 then
	//blocca perchè non sono amministratore	
	return 2
end if
//fine modifica ---------------------------

return 0
end function

public function integer uof_calcola_centro_costo (ref string fs_messaggio);// funzione di calcolo importi e percentuali centri di costo per riga

string ls_cod_tipo_det_ven,ls_cod_tipo_det_acq,ls_flag_tipo_det_ven,ls_flag_tipo_det_acq, ls_cod_prodotto, &
       ls_cod_centro_costo,ls_cod_gruppo_ven, ls_cod_gruppo_acq, ls_dataobject, ls_flag_cc_manuali,ls_flag_sola_iva, ls_str

long ll_i, ll_y, ll_cont_tipo_det,ll_prog_riga,ll_cont_prodotto, ll_cont_cc

dec{4} ld_imponibile_riga, ld_importo_cc, ld_perc_cc, ld_importo_totale_cc

datastore lds_cc

// gestione centri costo non attiva
if is_gestione_cc = "N" then return 0

if is_flag_cc_manuali = "S" then return 0

for ll_i = 1 to ids_righe_documento.rowcount()

	// Lettura del progressivo riga (per il successivo update della tabella)
	ll_prog_riga    = ids_righe_documento.getitemnumber(ll_i, is_colonna_prog_riga)
	ls_cod_prodotto = ids_righe_documento.getitemstring(ll_i, "cod_prodotto")
	ld_imponibile_riga    = ids_righe_documento.getitemnumber(ll_i, "imponibile_iva")
	
	if ld_imponibile_riga = 0 then continue
	
	choose case is_tipo_documento
	
		case "fat_ven"
			
			// la riga di fattura hai centri di costo impostati manualmente
			ls_flag_cc_manuali = ids_righe_documento.getitemstring(ll_i, "flag_cc_manuali")
			
			if ls_flag_cc_manuali = "S" then continue
			
			// cancello dati precedenti
			delete from det_fat_ven_cc
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :il_anno_registrazione and
					num_registrazione = :il_num_registrazione and
					prog_riga_fat_ven = :ll_prog_riga;

			// Verifico se esiste l'importazione delle percentuali sul tipo dettaglio
			ls_cod_tipo_det_ven = ids_righe_documento.getitemstring(ll_i, "cod_tipo_det_ven")
			
			// carico valore flag tipo dettaglio
			select flag_tipo_det_ven,
			       flag_sola_iva
			into   :ls_flag_tipo_det_ven,
			       :ls_flag_sola_iva
			from   tab_tipi_det_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella select di tab_tipi_det_ven: " + sqlca.sqlerrtext
				return -1
			end if
			
			if ls_flag_sola_iva = "S" then continue
			
			ll_cont_tipo_det = 0
	
			select count(*)
			into   :ll_cont_tipo_det
			from   tab_tipi_det_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
					 
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella select di tab_tipi_det_ven_cc: " + sqlca.sqlerrtext
				return -1
			end if
			
			// nel caso si tratti di prodotti a magazzino o non collegati a magazzino
			ll_cont_prodotto = 0
			
			if ls_flag_tipo_det_ven = "M" or ls_flag_tipo_det_ven = "C" then
				
				// vado a verificare se esiste un suddivisione per cc legata al prodotto		
				
				select count(*)
				into   :ll_cont_prodotto
				from   anag_prodotti_cc_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_prodotto = :ls_cod_prodotto;
							
				if sqlca.sqlcode <> 0 then
					fs_messaggio = "Errore nella select di anag_prodotti_cc: " + sqlca.sqlerrtext
					return -1
				end if
				
				ls_dataobject = 'd_ds_anag_prodotti_cc_ven'
				// vado a verificare se esiste un suddivisione legata al gruppo contabile indicato nel prodotto	
				if ll_cont_prodotto = 0 then
				
					ls_dataobject = ''

					select cod_gruppo_ven
					into   :ls_cod_gruppo_ven
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_prodotto;

					if sqlca.sqlcode <> 0 then
						ls_str = ls_cod_prodotto
						if isnull(ls_str) then ls_str = "<NULL>"
						fs_messaggio = "Errore nella select cod_gruppo_ven in anag_prodotti del prodotto " + ls_str + "~r~nTipo Dett: " + ls_cod_tipo_det_ven + "~r~n" + sqlca.sqlerrtext
						return -1
					end if
				
					if isnull(ls_cod_gruppo_ven)	 then
						
						ll_cont_prodotto = 0
						
					else
						
						select count(*)
						into   :ll_cont_prodotto
						from   tab_gruppi_cc
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_gruppo = :ls_cod_gruppo_ven;
									
						if sqlca.sqlcode <> 0 then
							fs_messaggio = "Errore nella select di anag_prodotti_cc: " + sqlca.sqlerrtext
							return -1
						end if
						
						if ll_cont_prodotto > 0 then ls_dataobject = 'd_ds_tab_gruppi_cc'
						
					end if				
					
				end if
					
			end if					
			
			// se esistono dei dati legati al prodotto non vado neanche a vedere gruppi e tipi_dett 
			// perchè il prodotto ha sempre la precendenza

			if ll_cont_prodotto = 0 then
				
				ll_cont_cc = 0
				
				// pertanto a questo punto non ho trovato alcun dati allegato al prodotto o 
				// al gruppo contabile del prodotto; procedo con controllo su gruppo contabile 
				// del tipo dettaglio e poi direttamente il tipo dettaglio.
				
				if ls_flag_tipo_det_ven <> "S" then
					// 27/05/2011: richiesto da A.Bellin; nelle righe di tipo sconto considero solo la distribuzione del tipo dettaglio.
				
					select cod_gruppo
					into   :ls_cod_gruppo_ven
					from   tab_tipi_det_ven
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
					if sqlca.sqlcode <> 0 then
						fs_messaggio = "Errore nella select di tab_tipi_det_ven : " + sqlca.sqlerrtext
						return -1
					end if
					
					if not isnull(ls_cod_gruppo_ven) then
						
						select count(*)
						into   :ll_cont_cc
						from   tab_gruppi_cc
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_gruppo = :ls_cod_gruppo_ven;
									
						if sqlca.sqlcode <> 0 then
							fs_messaggio = "Errore nella count centri costo da gruppi contabili: " + sqlca.sqlerrtext
							return -1
						end if
						
						if ll_cont_cc > 0 then ls_dataobject = 'd_ds_tab_gruppi_cc'
						
					end if
					
				end if
				// trovato niente passo ad esaminare il tipo dettaglio
				if ll_cont_cc = 0 then
					
					select count(*)
					into   :ll_cont_cc
					from   tab_tipi_det_ven_cc
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
								
					if sqlca.sqlcode <> 0 then
						fs_messaggio = "Errore nella count centri costo da gruppi contabili: " + sqlca.sqlerrtext
						return -1
					end if
					
					// se non trovo niente neanche nel tipo dettaglio segnalo, ma l'errore non deve essere bloccante.
					if ll_cont_cc = 0 then
						fs_messaggio = "Non esiste una suddivisione predefinita per i centri di costo sulla riga " + string(ll_prog_riga)
						return -2
					end if
						
					if ll_cont_cc > 0 then ls_dataobject = 'd_ds_tab_tipi_det_ven_cc'
					
				end if					
					
			end if
				
			lds_cc = create datastore
			lds_cc.dataobject = ls_dataobject
			lds_cc.settransobject(sqlca)
			
			choose case ls_dataobject
					
				case 'd_ds_anag_prodotti_cc_ven'
					
					lds_cc.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)
					
				case 'd_ds_tab_gruppi_cc'
					
					lds_cc.retrieve(s_cs_xx.cod_azienda, ls_cod_gruppo_ven)
					
				case 'd_ds_tab_tipi_det_ven_cc'
					
					lds_cc.retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_det_ven)
					
			end choose
			
			ld_importo_totale_cc = 0
			
			for ll_y = 1 to lds_cc.rowcount()
			
				ld_perc_cc = lds_cc.getitemnumber(ll_y, "percentuale")
				ls_cod_centro_costo = lds_cc.getitemstring(ll_y, "cod_centro_costo")					
				if ll_y <> lds_cc.rowcount() then
				
					ld_importo_cc = round((ld_imponibile_riga * ld_perc_cc) / 100, 2)
					ld_importo_totale_cc += ld_importo_cc
				
				else
					ld_importo_cc = ld_imponibile_riga - ld_importo_totale_cc
				
				end if
				
				insert into det_fat_ven_cc
						(cod_azienda,
						anno_registrazione, num_registrazione,
						prog_riga_fat_ven,
						cod_centro_costo,
						percentuale,
						importo)
				values
						(:s_cs_xx.cod_azienda,
						:il_anno_registrazione,
						:il_num_registrazione,
						:ll_prog_riga,
						:ls_cod_centro_costo,
						:ld_perc_cc,
						:ld_importo_cc);
				
				if sqlca.sqlcode <> 0 then
					fs_messaggio = "Errore nella insert in det_fat_ven_cc: " + sqlca.sqlerrtext
					return -1
				end if
			
			next
			
			destroy lds_cc



		case "fat_acq"
			
			// la riga di fattura hai centri di costo impostati manualmente
			ls_flag_cc_manuali = ids_righe_documento.getitemstring(ll_i, "flag_cc_manuali")
			
			if ls_flag_cc_manuali = "S" then continue
			
			// cancello dati precedenti
			delete from det_fat_acq_cc
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :il_anno_registrazione and
					num_registrazione = :il_num_registrazione and
					prog_riga_fat_acq = :ll_prog_riga;

			// Verifico se esiste l'importazione delle percentuali sul tipo dettaglio
			ls_cod_tipo_det_acq = ids_righe_documento.getitemstring(ll_i, "cod_tipo_det_acq")
			
			// carico valore flag tipo dettaglio
			select flag_tipo_det_acq,
			       flag_sola_iva
			into   :ls_flag_tipo_det_acq,
			       :ls_flag_sola_iva
			from   tab_tipi_det_acq
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_det_acq = :ls_cod_tipo_det_acq;
			
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella select di tab_tipi_det_acq: " + sqlca.sqlerrtext
				return -1
			end if
			
			if ls_flag_sola_iva = "S" then continue
			
			ll_cont_tipo_det = 0
	
			select count(*)
			into   :ll_cont_tipo_det
			from   tab_tipi_det_acq
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_det_acq = :ls_cod_tipo_det_acq;
					 
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella select di tab_tipi_det_acq_cc: " + sqlca.sqlerrtext
				return -1
			end if
			
			// nel caso si tratti di prodotti a magazzino o non collegati a magazzino
			ll_cont_prodotto = 0
			
			if ls_flag_tipo_det_acq = "M" or ls_flag_tipo_det_acq = "C" then
				
				// vado a verificare se esiste un suddivisione per cc legata al prodotto		
				
				select count(*)
				into   :ll_cont_prodotto
				from   anag_prodotti_cc_acq
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_prodotto = :ls_cod_prodotto;
							
				if sqlca.sqlcode <> 0 then
					fs_messaggio = "Errore nella select di anag_prodotti_cc: " + sqlca.sqlerrtext
					return -1
				end if
				
				ls_dataobject = 'd_ds_anag_prodotti_cc_acq'
				// vado a verificare se esiste un suddivisione legata al gruppo contabile indicato nel prodotto	
				if ll_cont_prodotto = 0 then
				
					ls_dataobject = ''

					select cod_gruppo_acq
					into   :ls_cod_gruppo_acq
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_prodotto;

					if sqlca.sqlcode <> 0 then
						fs_messaggio = "Errore nella select cod_gruppo_acq in anag_prodotti: " + sqlca.sqlerrtext
						return -1
					end if
				
					if isnull(ls_cod_gruppo_acq)	 then
						
						ll_cont_prodotto = 0
						
					else
						
						select count(*)
						into   :ll_cont_prodotto
						from   tab_gruppi_cc
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_gruppo = :ls_cod_gruppo_acq;
									
						if sqlca.sqlcode <> 0 then
							fs_messaggio = "Errore nella select di tab_gruppi_cc: " + sqlca.sqlerrtext
							return -1
						end if
						
						if ll_cont_prodotto > 0 then ls_dataobject = 'd_ds_tab_gruppi_cc'
						
					end if				
					
				end if
					
			end if					
			
			// se esistono dei dati legati al prodotto non vado neanche a vedere gruppi e tipi_dett 
			// perchè il prodotto ha sempre la precendenza

			if ll_cont_prodotto = 0 then
				
				ll_cont_cc = 0
				
				// pertanto a questo punto non ho trovato alcun dati allegato al prodotto o 
				// al gruppo contabile del prodotto; procedo con controllo su gruppo contabile 
				// del tipo dettaglio e poi direttamente il tipo dettaglio.
				
				select cod_gruppo
				into   :ls_cod_gruppo_acq
				from   tab_tipi_det_acq
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       cod_tipo_det_acq = :ls_cod_tipo_det_acq;

				if sqlca.sqlcode <> 0 then
					fs_messaggio = "Errore nella select di tab_tipi_det_acq : " + sqlca.sqlerrtext
					return -1
				end if
				
				if not isnull(ls_cod_gruppo_acq) then
					
					select count(*)
					into   :ll_cont_cc
					from   tab_gruppi_cc
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_gruppo = :ls_cod_gruppo_acq;
								
					if sqlca.sqlcode <> 0 then
						fs_messaggio = "Errore nella count centri costo da gruppi contabili: " + sqlca.sqlerrtext
						return -1
					end if
					
					if ll_cont_cc > 0 then ls_dataobject = 'd_ds_tab_gruppi_cc'
					
				end if
				
				// trovato niente passo ad esaminare il tipo dettaglio
				if ll_cont_cc = 0 then
					
					select count(*)
					into   :ll_cont_cc
					from   tab_tipi_det_acq_cc
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_tipo_det_acq = :ls_cod_tipo_det_acq;
								
					if sqlca.sqlcode <> 0 then
						fs_messaggio = "Errore nella count centri costo da tipi dettagli acquisto: " + sqlca.sqlerrtext
						return -1
					end if
					
					// se non trovo niente neanche nel tipo dettaglio segnalo, ma l'errore non deve essere bloccante.
					if ll_cont_cc = 0 then
						fs_messaggio = "Non esiste una suddivisione predefinita per i centri di costo sulla riga " + string(ll_prog_riga)
						return -2
					end if
						
					if ll_cont_cc > 0 then ls_dataobject = 'd_ds_tab_tipi_det_acq_cc'
					
				end if					
					
			end if
				
			lds_cc = create datastore
			lds_cc.dataobject = ls_dataobject
			lds_cc.settransobject(sqlca)
			
			choose case ls_dataobject
					
				case 'd_ds_anag_prodotti_cc_acq'
					
					lds_cc.retrieve(s_cs_xx.cod_azienda, ls_cod_prodotto)
					
				case 'd_ds_tab_gruppi_cc'
					
					lds_cc.retrieve(s_cs_xx.cod_azienda, ls_cod_gruppo_acq)
					
				case 'd_ds_tab_tipi_det_acq_cc'
					
					lds_cc.retrieve(s_cs_xx.cod_azienda, ls_cod_tipo_det_acq)
					
			end choose
			
			ld_importo_totale_cc = 0
			
			for ll_y = 1 to lds_cc.rowcount()
			
				ld_perc_cc = lds_cc.getitemnumber(ll_y, "percentuale")
				ls_cod_centro_costo = lds_cc.getitemstring(ll_y, "cod_centro_costo")					
				if ll_y <> lds_cc.rowcount() then
				
					ld_importo_cc = round((ld_imponibile_riga * ld_perc_cc) / 100, 2)
					ld_importo_totale_cc += ld_importo_cc
				
				else
					ld_importo_cc = ld_imponibile_riga - ld_importo_totale_cc
				
				end if
				
				insert into det_fat_acq_cc
						(cod_azienda,
						anno_registrazione, 
						num_registrazione,
						prog_riga_fat_acq,
						cod_centro_costo,
						percentuale,
						importo)
				values
						(:s_cs_xx.cod_azienda,
						:il_anno_registrazione,
						:il_num_registrazione,
						:ll_prog_riga,
						:ls_cod_centro_costo,
						:ld_perc_cc,
						:ld_importo_cc);
				
				if sqlca.sqlcode <> 0 then
					fs_messaggio = "Errore nella insert in det_fat_acq_cc: " + sqlca.sqlerrtext
					return -1
				end if
			
			next
			
			destroy lds_cc

			
	end choose			

next

return 0
end function

public function integer uof_totalizza_centro_costo (ref string fs_messaggio);string ls_str,ls_cod_conto, ls_cod_centro_costo,ls_cod_conto_prec

long ll_i, ll_rows

dec{4} ld_importo_cc,ld_import_conto, ld_perc, ld_perc_tot

datastore lds_datastore

if is_gestione_cc = "N" then return 0

if is_flag_cc_manuali = "S" then return 0

lds_datastore = create datastore

if is_tipo_documento = "fat_ven" then
	
	lds_datastore.dataobject = 'd_ds_cont_fat_ven_cc'
	
elseif is_tipo_documento = "fat_acq" then
	
	lds_datastore.dataobject = 'd_ds_cont_fat_acq_cc'
	
end if

lds_datastore.settransobject(sqlca)

ll_rows = 0

ll_rows = lds_datastore.retrieve(s_cs_xx.cod_azienda, il_anno_registrazione, il_num_registrazione)

if ll_rows = 0 then return 0

if ll_rows < 0 then 
	fs_messaggio = "Errore in retrieve datastore centri di costo (uof_totalizza_cc)"
	return -1
end if

for ll_i = 1 to lds_datastore.rowcount()
	
	ls_cod_conto = lds_datastore.getitemstring(ll_i, "cod_conto")
	ls_cod_centro_costo = lds_datastore.getitemstring(ll_i, "cod_centro_costo")
	ld_importo_cc = lds_datastore.getitemnumber(ll_i, "importo")
	
	if ll_i = 1 then ls_cod_conto_prec = ls_cod_conto

	if is_tipo_documento = "fat_ven" then
		
		select imp_riga  
		 into  :ld_import_conto  
		 from  cont_fat_ven  
		where  cod_azienda = :s_cs_xx.cod_azienda  and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione  and
				 cod_conto = :ls_cod_conto ;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca conto in totalizzazione centrio di costo~r~n"  + sqlca.sqlerrtext
			return -1
		end if
		
		if ls_cod_conto <> ls_cod_conto_prec then 
			ld_perc = round((ld_importo_cc * 100) / ld_import_conto, 2)
			ld_perc_tot = ld_perc
		else
			if ll_i < lds_datastore.rowcount() then
				if ls_cod_conto <> lds_datastore.getitemstring(ll_i + 1, "cod_conto") then
					ld_perc = 100 - ld_perc_tot 
					ld_perc_tot = 100
				else
					ld_perc = round((ld_importo_cc * 100) / ld_import_conto, 2)
					ld_perc_tot += ld_perc
				end if
			else
				ld_perc = 100 - ld_perc_tot 
				ld_perc_tot = 100
			end if
		end if
		
		if ld_importo_cc < 0 then
			fs_messaggio = "Attenzione! C'è qualche incongruenza nei dati dei CC; non sono ammessi importi negativi nei totali"
			return -2
		end if
		
		insert into cont_fat_ven_cc  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  cod_conto,   
				  cod_centro_costo,   
				  percentuale,   
				  importo )  
		values ( :s_cs_xx.cod_azienda,   
				  :il_anno_registrazione,
				  :il_num_registrazione,   
				  :ls_cod_conto,   
				  :ls_cod_centro_costo,   
				  :ld_perc,   
				  :ld_importo_cc)  ;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in inserimento dati centri di costo per conto~r~nNum Interno " + string(il_anno_registrazione) + "/" +  string(il_num_registrazione) + "~r~n" + sqlca.sqlerrtext
			return -1
		end if
	
	elseif is_tipo_documento = "fat_acq" then
		
		select importo_riga  
		 into  :ld_import_conto  
		 from  cont_fat_acq
		where  cod_azienda = :s_cs_xx.cod_azienda  and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione  and
				 cod_conto = :ls_cod_conto ;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca conto in totalizzazione centrio di costo~r~nNum Interno " + string(il_anno_registrazione) + "/" +  string(il_num_registrazione) + "~r~n" + sqlca.sqlerrtext
			return -1
		end if

		if ls_cod_conto <> ls_cod_conto_prec then 
			ld_perc = round((ld_importo_cc * 100) / ld_import_conto, 2)
			ld_perc_tot = ld_perc
		else
			if ll_i < lds_datastore.rowcount() then
				if ls_cod_conto <> lds_datastore.getitemstring(ll_i + 1, "cod_conto") then
					ld_perc = 100 - ld_perc_tot 
					ld_perc_tot = 100
				else
					ld_perc = round((ld_importo_cc * 100) / ld_import_conto, 2)
					ld_perc_tot += ld_perc
				end if
			else
				ld_perc = 100 - ld_perc_tot 
				ld_perc_tot = 100
			end if
		end if
		
		if ld_importo_cc < 0 then
			fs_messaggio = "Attenzione! C'è qualche incongruenza nei dati dei CC; non sono ammessi importi negativi nei totali"
			return -2
		end if
		
		
		insert into cont_fat_acq_cc  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  cod_conto,   
				  cod_centro_costo,   
				  percentuale,   
				  importo )  
		values ( :s_cs_xx.cod_azienda,   
				  :il_anno_registrazione,
				  :il_num_registrazione,   
				  :ls_cod_conto,   
				  :ls_cod_centro_costo,   
				  :ld_perc,   
				  :ld_importo_cc)  ;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in inserimento dati centri di costo per conto~r~nNum Interno " + string(il_anno_registrazione) + "/" +  string(il_num_registrazione) + "~r~n" + sqlca.sqlerrtext
			return -1
		end if
				  
	end if
	
	ls_cod_conto_prec = ls_cod_conto
	
next

destroy lds_datastore

return 0
end function

public function date uof_converti_data (string fs_giorno, string fs_mese, string fs_anno);string  ls_data

long    ll_giorno, ll_mese, ll_anno

date    ldd_data

boolean lb_bisestile


ll_mese = long(fs_mese)

ll_giorno = long(fs_giorno)

if mod(long(fs_anno), 4) = 0 then
	if mod(long(fs_anno), 100) = 0 then
		if mod(long(fs_anno), 400) = 0 then
			lb_bisestile = true
		else
			lb_bisestile = false
		end if
	else
		lb_bisestile = true
	end if
else
	lb_bisestile = false
end if	

choose case ll_mese
		
	case 4,6,9,11
		
		if ll_giorno > 30 then
			ll_giorno = 30
		end if
		
	case 1,3,5,7,8,10,12
		
		if ll_giorno > 31 then
			ll_giorno = 31
		end if
		
	case 2
		
		if lb_bisestile then
			if ll_giorno > 29 then
				ll_giorno = 29
			end if	
		else				
			if ll_giorno > 28 then
				ll_giorno = 28
			end if	
		end if
		
end choose

ls_data = string(ll_giorno) + "/" + fs_mese + "/" + fs_anno
ldd_data = date(ls_data)

return ldd_data
end function

public function integer uof_addebiti_cliente_bolle (ref string fs_messaggio);string   ls_cod_pagamento, ls_cod_valuta, ls_cod_cliente, ls_flag_calcolo, ls_cod_lingua, ls_cod_tipo_det_ven, &
         ls_messaggio, ls_stringa, ls_str, ls_cod_iva, ls_flag_tipo_pagamento, ls_cod_tipo_doc_ven, &
		   ls_cod_fornitore, ls_flag_tipo_doc_ven, ls_cod_documento,ls_des_addebito, ls_cod_prodotto_appo, ls_descrizione_appo
			
long     ll_prog_riga_bol_ven, ll_pos

dec{4}   ld_importo_addebito

datetime ldt_data_esenzione_iva, ldt_data_registrazione, ldt_data_bolla, ldt_data_riferimento


select cod_pagamento,
  		 cod_valuta,
		 cod_cliente,
		 cod_fornitore,
		 data_registrazione,
 		 cod_tipo_bol_ven,
		 data_bolla,
		 cod_documento
into   :ls_cod_pagamento,
		 :ls_cod_valuta,
       :ls_cod_cliente,
       :ls_cod_fornitore,
		 :ldt_data_registrazione,
		 :ls_cod_tipo_doc_ven,
		 :ldt_data_bolla,
		 :ls_cod_documento
from   tes_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
	    anno_registrazione = :il_anno_registrazione and
 		 num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di tes_bol_ven: " + sqlca.sqlerrtext
	return -1
end if

select flag_tipo_bol_ven
into   :ls_flag_tipo_doc_ven
from   tab_tipi_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_bol_ven = :ls_cod_tipo_doc_ven;
 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di tab_tipi_fat_ven: " + sqlca.sqlerrtext
	return -1
end if

if ls_flag_tipo_doc_ven <> "C" and ls_flag_tipo_doc_ven <> "R" then
	
	//ticket 2014/513
	//se il campo cliente è vuoto, molto probabilmente il DDt e verso Contatto
	//poichè al momento non esiste una tabella e neppure una gestione addebiti contatti (analogo di anag_clienti_addebiti)
	//usciamo subito da questa funzione
	if ls_cod_cliente="" or isnull(ls_cod_cliente) then return 0
	
	select cod_lingua
	into   :ls_cod_lingua
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_cliente = :ls_cod_cliente;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di anag_clienti: " + sqlca.sqlerrtext
		return -1
	end if
	
else
	
	return 0		// per i fornitori non fare nulla
	
end if

if not isnull(ls_cod_lingua) then
	
	select cod_lingua
	into   :ls_cod_lingua
	from   tab_lingue
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_lingua = :ls_cod_lingua;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di tab_lingue: " + sqlca.sqlerrtext
		return -1
	end if
	
end if

if isnull(ls_cod_documento) then
	
	ldt_data_riferimento = ldt_data_registrazione
	
	if isnull(ldt_data_riferimento) then
		ldt_data_riferimento = datetime(today(), 00:00:00)
	end if	
	
else
	
	ldt_data_riferimento = ldt_data_bolla

end if

declare cu_addebiti cursor for
select des_addebito,
       cod_tipo_det_ven,
       importo_addebito,
		 cod_valuta
from   anag_clienti_addebiti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente and
		 data_inizio <= :ldt_data_riferimento and
		 data_fine >= :ldt_data_riferimento and
		 cod_valuta = :ls_cod_valuta and
		 flag_mod_valore_bolle = 'N' and
		 flag_tipo_documento = 'B';
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di anag_clienti_addebiti: " + sqlca.sqlerrtext
	return -1
end if

open cu_addebiti;

if sqlca.sqlcode = -1 then
	fs_messaggio = "Errore nella open del cursore cu_addebiti: " + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_addebiti into :ls_des_addebito, 
								  :ls_cod_tipo_det_ven, 
								  :ld_importo_addebito, 
								  :ls_cod_valuta;
	
	if sqlca.sqlcode = -1 then
		fs_messaggio = "Errore nella fetch del cursore cu_addebiti: " + sqlca.sqlerrtext
		close cu_addebiti;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then
		exit
	end if	
	
	if isnull(ls_cod_tipo_det_ven) then
		fs_messaggio = "Errore nella fetch del cursore cu_addebiti: impostare il tipo dettaglio vendita negli addebiti clienti"
		close cu_addebiti;
		return -1
	end if
	
	if isnull(ls_cod_valuta) then
		fs_messaggio = "Errore nella fetch del cursore cu_addebiti: impostare la valuta negli addebiti clienti"
		close cu_addebiti;
		return -1
	end if
			
   uof_arrotonda(ld_importo_addebito, id_precisione_valuta, "euro")
		
	// Ricerca della prima riga libera nel documento
	select max(prog_riga_bol_ven)
	into   :ll_prog_riga_bol_ven
	from   det_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione;
			 
	if ll_prog_riga_bol_ven = 0 or isnull(ll_prog_riga_bol_ven) then
		ll_prog_riga_bol_ven = 10
	else
		ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
	end if
	
	// Ricerca di aliquota iva o esenzione del dettaglio
	// La variabile ls_cod_tipo_det_ven non può essere a null: già verificato sopra	
	select cod_iva  
	into   :ls_str
	from   tab_tipi_det_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di tab_tipi_det_ven: " + sqlca.sqlerrtext
		close cu_addebiti;
		return -1
	end if
	
	if not isnull(ls_str) then ls_cod_iva = ls_str
	
	select cod_iva,
			 data_esenzione_iva
	into   :ls_str,
			 :ldt_data_esenzione_iva
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_cliente = :ls_cod_cliente;
			 
	if sqlca.sqlcode = 0 then
		if not isnull(ls_str) and (ldt_data_esenzione_iva <= ldt_data_riferimento or isnull(ldt_data_esenzione_iva)) then
			ls_cod_iva = ls_str
		end if	
	end if
	
	// *** Michela 26/01/2006: se il primo carattere della descrizione dell'addebito è = "*" allora la 
	//                         parte restante della descrizione è un codice prodotto e la descrizione vado a prendermela 
	//                         dall'anagrafica prodotti
	
	setnull(ls_cod_prodotto_appo)
	setnull(ls_descrizione_appo)
	ll_pos = 0
	ll_pos = pos( ls_des_addebito, "*")
	if not isnull(ll_pos) and ll_pos = 1 then
		ls_cod_prodotto_appo = mid( ls_des_addebito, 2)
		
		select des_prodotto
		into   :ls_descrizione_appo
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto_appo;
				 
		if sqlca.sqlcode <> 0 then
			setnull(ls_cod_prodotto_appo)
		else
			ls_des_addebito = ls_descrizione_appo
		end if
	end if	
	
	// La riga di spese bancarie viene inserita nel dettaglio del documento
	insert into det_bol_ven  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  prog_riga_bol_ven,   
				  cod_tipo_det_ven,   
				  cod_deposito,   
				  cod_ubicazione,   
				  cod_lotto,   
				  data_stock,   
				  progr_stock,   
				  cod_misura,   
				  cod_prodotto,   
				  des_prodotto,   
				  quan_consegnata,   
				  prezzo_vendita,  
				  fat_conversione_ven,
				  sconto_1,   
				  sconto_2,   
				  provvigione_1,   
				  provvigione_2,   
				  cod_iva,   
				  cod_tipo_movimento,   
				  num_registrazione_mov_mag,   
				  nota_dettaglio,   
				  anno_registrazione_ord_ven,   
				  num_registrazione_ord_ven,   
				  prog_riga_ord_ven,   
				  anno_commessa,   
				  num_commessa,   
				  cod_centro_costo,   
				  sconto_3,   
				  sconto_4,   
				  sconto_5,   
				  sconto_6,   
				  sconto_7,   
				  sconto_8,   
				  sconto_9,   
				  sconto_10,   
				  anno_registrazione_mov_mag,   
				  anno_reg_bol_acq,   
				  num_reg_bol_acq,   
				  prog_riga_bol_acq,   
				  anno_reg_des_mov,   
				  num_reg_des_mov,   
				  cod_versione,   
				  num_confezioni,   
				  num_pezzi_confezione,   
				  flag_doc_suc_det,   
				  flag_st_note_det,   
				  quantita_um,   
				  prezzo_um,   
				  imponibile_iva,   
				  imponibile_iva_valuta)  
	  values ( :s_cs_xx.cod_azienda,   
				  :il_anno_registrazione,   
				  :il_num_registrazione,   
				  :ll_prog_riga_bol_ven,   
				  :ls_cod_tipo_det_ven,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  :ls_cod_prodotto_appo,   
				  :ls_des_addebito,   
				  1,   
				  :ld_importo_addebito,   
				  1,   
				  0,
				  0,   
				  0,   
				  0,   
				  :ls_cod_iva,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  null,   
				  null,   
				  null,   
				  1,   
				  null,   
				  null,   
				  null,   
				  0,   
				  0,   
				  'I',   
				  'I',   
				  0,
				  0,
				  0,
				  0)  ;
				  
	
				  
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella insert di det_bol_ven: " + sqlca.sqlerrtext
		return -1
	end if
	
loop

close cu_addebiti;

return 0

end function

public function integer uof_calcola_costo_trasporto (string fs_cod_vettore_forzato, string fs_cod_destinazione_forzata, ref decimal fd_costo_trasporto, ref string fs_messaggio);string			ls_cod_cliente, ls_cod_valuta, ls_cod_vettore, ls_cod_destinazione, ls_prov_dest, ls_provincia, ls_cod_nazione, &
				ls_cod_mezzo, ls_flag_inoltro, ls_flag_tipo_tariffa, ls_nota, ls_cod_tipo_det_ven_trasporto, ls_cod_iva, &
				ls_cod_pagamento, ls_cod_iva_cli, ls_flag_tipo_det_ven, ls_messaggio, ls_select, ls_sql, ls_sql_stringa, &
				ls_sql_select, ls_flag_contrassegno, ls_flag_calcolo_trasporto, ls_tabella_testata, ls_tabella_dettaglio, &
				ls_tabella_iva, ls_tabella_tipo_doc, ls_tabella_controllo, ls_colonna_tipo_doc, ls_colonna_prog, &
				ls_colonna_quan, ls_colonna_totale, ls_colonna_totale_valuta, ls_cod_contatto, ls_flag_tipo_fat_ven
		 
dec{4} ld_cambio_ven, ld_prezzo_spedizione, ld_prezzo_inoltro, ld_perc_valore_merce, ld_costo_trasp, ld_peso_lordo, &
       ld_tot_documento, ld_sconto_testata, ld_costo_forfait_sped, ld_costo_forfait_inol, ld_min_contrassegno, &
		 ld_perc_contrassegno, ld_arrotonda_peso, ld_quan_tonda, ld_quantita, ld_costo_contrassegno, ld_costo_quan, &
		 ld_costo_per_val, ld_tot_imponibile, ld_tot_iva, ld_tot_documento_valuta, &
		 ld_tot_imponibile_valuta, ld_tot_iva_valuta, ld_tot_merci, ld_tot_sconti_commerciali
		 
long   ll_prog_riga_doc_ven, ll_i, ll_risp, ll_cont


if len(fs_cod_vettore_forzato) < 1 then setnull(fs_cod_vettore_forzato)
if len(fs_cod_destinazione_forzata) < 1 then setnull(fs_cod_destinazione_forzata)

select cod_tipo_det_ven_trasporto
into   :ls_cod_tipo_det_ven_trasporto 
from   con_vendite
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di con_vendite: " + sqlca.sqlerrtext
	return -1
end if

choose case is_tipo_documento
		
	case "off_ven"
		
		ls_tabella_testata = "tes_off_ven"
		ls_tabella_dettaglio = "det_off_ven"
		ls_tabella_iva = "iva_off_ven"
		ls_tabella_tipo_doc = "tab_tipi_off_ven"
		ls_tabella_controllo = "con_off_ven"
		ls_colonna_tipo_doc = "cod_tipo_off_ven"
		ls_colonna_prog = "prog_riga_off_ven"
		ls_colonna_quan = "quan_offerta"
		ls_colonna_totale = "tot_val_offerta"
		ls_colonna_totale_valuta  = "tot_val_offerta_valuta"		
				
	case "ord_ven"
		
		ls_tabella_testata = "tes_ord_ven"
		ls_tabella_dettaglio = "det_ord_ven"
		ls_tabella_iva = "iva_ord_ven"
		ls_tabella_tipo_doc = "tab_tipi_ord_ven"
		ls_tabella_controllo = "con_ord_ven"
		ls_colonna_tipo_doc = "cod_tipo_ord_ven"
		ls_colonna_prog = "prog_riga_ord_ven"
		ls_colonna_quan = "quan_ordine"
		ls_colonna_totale = "tot_val_ordine"
		ls_colonna_totale_valuta  = "tot_val_ordine_valuta"		
		
	case "bol_ven"
		
		ls_tabella_testata = "tes_bol_ven"
		ls_tabella_dettaglio = "det_bol_ven"
		ls_tabella_iva = "iva_bol_ven"
		ls_tabella_tipo_doc = "tab_tipi_bol_ven"
		ls_tabella_controllo = "con_bol_ven"
		ls_colonna_tipo_doc = "cod_tipo_bol_ven"
		ls_colonna_prog = "prog_riga_bol_ven"
		ls_colonna_quan = "quan_consegnata"
		ls_colonna_totale = "tot_val_bolla"
		ls_colonna_totale_valuta  = "tot_val_bolla_valuta"
		
	case "fat_ven"
	
		ls_tabella_testata = "tes_fat_ven"
		ls_tabella_dettaglio = "det_fat_ven"
		ls_tabella_iva = "iva_fat_ven"
		ls_tabella_tipo_doc = "tab_tipi_fat_ven"
		ls_tabella_controllo = "con_fat_ven"
		ls_colonna_tipo_doc = "cod_tipo_fat_ven"
		ls_colonna_prog = "prog_riga_fat_ven"
		ls_colonna_quan = "quan_fatturata"
		ls_colonna_totale = "tot_fattura"
		ls_colonna_totale_valuta  = "tot_fattura_valuta"
		
end choose

select flag_tipo_det_ven
into   :ls_flag_tipo_det_ven
from   tab_tipi_det_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_det_ven = :ls_cod_tipo_det_ven_trasporto;
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di tab_tipi_det_ven: " + sqlca.sqlerrtext
	return -1
end if

if ls_flag_tipo_det_ven <>  "T" then
	fs_messaggio = "Tipo dettaglio vendite deve essere di tipo TRASPORTI"
	return -1
end if

declare cu_cursore_testata dynamic cursor for sqlsa;

ls_sql_select = "select " + ls_colonna_totale + ", " + ls_colonna_totale_valuta + ", imponibile_iva, imponibile_iva_valuta, importo_iva, importo_iva_valuta, tot_merci, tot_sconti_commerciali from " + ls_tabella_testata + &
                " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(il_anno_registrazione) + &
			       " and num_registrazione = " + string(il_num_registrazione)
					 
prepare sqlsa from :ls_sql_select;

open dynamic cu_cursore_testata;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella open del cursore cu_cursore_testata: " + sqlca.sqlerrtext
	return -1
end if

fetch cu_cursore_testata into :ld_tot_documento, :ld_tot_documento_valuta, :ld_tot_imponibile, :ld_tot_imponibile_valuta, :ld_tot_iva, :ld_tot_iva_valuta, :ld_tot_merci, :ld_tot_sconti_commerciali;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella fetch del cursore cu_cursore_testata: " + sqlca.sqlerrtext
	close cu_cursore_testata;
	return -1
end if

close cu_cursore_testata;


ls_sql = " select cod_cliente, cod_valuta, cod_pagamento, provincia, cod_vettore, cod_mezzo, peso_lordo from " + ls_tabella_testata + " " + &
				" where cod_azienda = '"+ s_cs_xx.cod_azienda + "' and " + &
				" anno_registrazione = "+ string(il_anno_registrazione) + " and " + &
				" num_registrazione = "+ string(il_num_registrazione)

declare cu_testata dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic cu_testata;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella open del cursore cu_testata: " + sqlca.sqlerrtext
	return -1
end if

fetch cu_testata into :ls_cod_cliente, :ls_cod_valuta, :ls_cod_pagamento, :ls_prov_dest, :ls_cod_vettore, :ls_cod_mezzo, :ld_peso_lordo;
	
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella fetch del cursore cu_testata: " + sqlca.sqlerrtext
	close cu_cursore_testata;
	return -1
end if

close cu_testata;

if not isnull(fs_cod_destinazione_forzata) then
	ls_prov_dest = fs_cod_destinazione_forzata
end if

if not isnull(fs_cod_vettore_forzato) then
	ls_cod_vettore = fs_cod_vettore_forzato
end if

setnull(ls_cod_contatto)

if isnull(ls_cod_cliente) then

	if is_tipo_documento = "off_ven" then
		
		//leggi il contatto da testata offerte vendita ---------------------------
		select cod_contatto
		into   :ls_cod_contatto
		from   tes_off_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :il_anno_registrazione and
				 num_registrazione = :il_num_registrazione;
				 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella select di tes_off_ven: "  + sqlca.sqlerrtext
			return -1
		end if
		
	elseif is_tipo_documento="fat_ven" then
		//se fattura proforma recupero contatto da testata fattura
		
		//controlla se proforma ------------------------------
		select tab_tipi_fat_ven.flag_tipo_fat_ven
		into :ls_flag_tipo_fat_ven
		from tes_fat_ven
		join tab_tipi_fat_ven on 	tab_tipi_fat_ven.cod_azienda=tes_fat_ven.cod_azienda and
										tab_tipi_fat_ven.cod_tipo_fat_ven=tes_fat_ven.cod_tipo_fat_ven
		where 	tes_fat_ven.cod_azienda=:s_cs_xx.cod_azienda and
					tes_fat_ven.anno_registrazione = :il_anno_registrazione and
				 	tes_fat_ven.num_registrazione = :il_num_registrazione;
		
		if sqlca.sqlcode < 0 then
			fs_messaggio = "Errore nella select di tes_fat_ven: "  + sqlca.sqlerrtext
			return -1
		end if
		
		if ls_flag_tipo_fat_ven="P" then
			//è una proforma, e dentro l'IF principale vuol dire che non c'è il cliente
			//quindi mi aspetto il contatto
			
			select cod_contatto
			into   :ls_cod_contatto
			from   tes_fat_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :il_anno_registrazione and
					 num_registrazione = :il_num_registrazione;
					 
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella select cod_contatto in tes_fat_ven: "  + sqlca.sqlerrtext
				return -1
			end if
			
		end if

	end if
end if


if isnull(ls_cod_contatto) and isnull(ls_cod_cliente) then
	fs_messaggio = "Cliente o contatto non indicato: impossibile proseguire"
	return -1
end if

if isnull(ls_cod_cliente) then
	
	// la bolla o la fattura riguardano un fornitore; caso non previsto quindi esco dal calcolo trasporto.
	
	//aggiunto codice per gestione proforma a contatto
	//...se si tratta di una fattura proforma senza cliente prosegui (in tal caso ho valorizzato ls_flag_tipo_fat_ven con P)
	
	//if is_tipo_documento = "bol_ven" or is_tipo_documento = "fat_ven" then
	if is_tipo_documento = "bol_ven" or (is_tipo_documento = "fat_ven" and ls_flag_tipo_fat_ven<>"P") then
		return 0
	end if
	
	select provincia,
			 cod_nazione,
			 cod_iva
	into   :ls_provincia,
		    :ls_cod_nazione,
		    :ls_cod_iva_cli
	from   anag_contatti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_contatto = :ls_cod_contatto;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di anag_contatti: " + sqlca.sqlerrtext
		return -1
	end if		 
			 
else  
	
	select provincia,
			 cod_nazione,
			 cod_iva
	into   :ls_provincia,
		    :ls_cod_nazione,
		    :ls_cod_iva_cli
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cliente = :ls_cod_cliente;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di anag_clienti: " + sqlca.sqlerrtext
		return -1
	end if		 
			 
end if

if isnull(ls_cod_nazione) then
	ls_cod_nazione = "ITA"
end if

if isnull(ld_peso_lordo) then
	ld_peso_lordo = 0
end if

if isnull(ld_tot_documento) then
	ld_tot_documento = 0
end if

if not isnull(ls_prov_dest) then 
	ls_cod_destinazione = ls_prov_dest
elseif not isnull(ls_provincia) then
	ls_cod_destinazione = ls_provincia
//else
//	fs_messaggio = "Inserire provincia di destinazione in testata documento"
//	return -1
end if

if isnull(ls_cod_vettore) then
	fs_messaggio = "La condizione di pagamento prevede l'addebito del trasporto, ma non è stato indicato il vettore in testata: il calcolo del documento viene effettuato ugualmente!"
	return 0
end if

if isnull(ls_cod_mezzo) then
	
	ls_flag_inoltro = "N"
	
else
	
	setnull(ls_flag_inoltro)
	
	select flag_inoltro
	into :ls_flag_inoltro
	from tab_mezzi
	where cod_azienda = :s_cs_xx.cod_azienda
	  and cod_mezzo = :ls_cod_mezzo;
	  
	if sqlca.sqlcode < 0 then
		fs_messaggio = "Errore nella select di tab_mezzi: " + sqlca.sqlerrtext
		return -1
	end if
	
end if

select count(*)
into   :ll_cont
from   listino_vettori
WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
		( cod_vettore = :ls_cod_vettore ) AND  
		( cod_valuta = :ls_cod_valuta ) AND  
		( cod_destinazione = 'XX');
		
if ll_cont > 0 then
	
	SELECT flag_tipo_tariffa,   
			prezzo_spedizione,   
			prezzo_inoltro,   
			perc_valore_merce,   
			costo_forfait_sped, 
			costo_forfait_inol,
			min_contrassegno,
			perc_contrassegno,
			arrotonda_peso
	INTO :ls_flag_tipo_tariffa,   
			:ld_prezzo_spedizione,   
			:ld_prezzo_inoltro,   
			:ld_perc_valore_merce,
			:ld_costo_forfait_sped, 
			:ld_costo_forfait_inol,
			:ld_min_contrassegno,
			:ld_perc_contrassegno,
			:ld_arrotonda_peso
	FROM listino_vettori  
	WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
			( cod_vettore = :ls_cod_vettore ) AND  
			( cod_valuta = :ls_cod_valuta ) AND  
			( cod_destinazione = 'XX');
			
else
	
	SELECT flag_tipo_tariffa,   
			prezzo_spedizione,   
			prezzo_inoltro,   
			perc_valore_merce,   
			costo_forfait_sped, 
			costo_forfait_inol,
			min_contrassegno,
			perc_contrassegno,
			arrotonda_peso
	INTO :ls_flag_tipo_tariffa,   
			:ld_prezzo_spedizione,   
			:ld_prezzo_inoltro,   
			:ld_perc_valore_merce,
			:ld_costo_forfait_sped, 
			:ld_costo_forfait_inol,
			:ld_min_contrassegno,
			:ld_perc_contrassegno,
			:ld_arrotonda_peso
	FROM listino_vettori  
	WHERE ( cod_azienda = :s_cs_xx.cod_azienda ) AND  
			( cod_vettore = :ls_cod_vettore ) AND  
			( cod_valuta = :ls_cod_valuta ) AND  
			( cod_destinazione = :ls_cod_destinazione );
			
end if

if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore nella select di listino_vettori: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	return 0
end if

if isnull(ls_flag_inoltro) or (ls_flag_inoltro = "N") then 
	ld_prezzo_inoltro = 0
	ld_costo_forfait_inol = 0
end if

if isnull(ld_prezzo_spedizione) then
	ld_prezzo_spedizione = 0
end if

if isnull(ld_prezzo_inoltro) then
	ld_prezzo_inoltro = 0
end if

if isnull(ld_perc_valore_merce) then
	ld_perc_valore_merce = 0
end if

if isnull(ld_costo_forfait_sped) then
	ld_costo_forfait_sped = 0
end if

if isnull(ld_costo_forfait_inol) then
	ld_costo_forfait_inol = 0
end if

if isnull(ld_min_contrassegno) then
	ld_min_contrassegno = 0
end if

if isnull(ld_perc_contrassegno) then
	ld_perc_contrassegno = 0
end if

if isnull(ld_arrotonda_peso) then
	ld_arrotonda_peso = 0
end if	

select flag_contrassegno, 
       flag_calcolo_trasporto
into   :ls_flag_contrassegno, 
       :ls_flag_calcolo_trasporto
from   tab_pagamenti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_pagamento = :ls_cod_pagamento;
		 
if sqlca.sqlcode <>0 then
	fs_messaggio = "Errore nella select di tab_pagamenti"
	return -1
end if

if isnull(ls_flag_contrassegno) then
	ls_flag_contrassegno = "N"
end if

if isnull(ls_flag_calcolo_trasporto) then
	ls_flag_calcolo_trasporto = "N"
end if	

if ld_perc_valore_merce <> 0 then
	ld_costo_per_val = (ld_tot_merci - ld_tot_sconti_commerciali) * ld_perc_valore_merce / 100
end if

if ls_flag_contrassegno = "S" and ls_flag_calcolo_trasporto = "S" then
	
	if ld_perc_contrassegno <> 0 then
		
		ld_costo_contrassegno = ld_tot_documento * ld_perc_contrassegno / 100
		
		if ld_min_contrassegno > 0 and ld_min_contrassegno > ld_costo_contrassegno then
			ld_costo_contrassegno = ld_min_contrassegno
		end if
		
	end if
	
end if

if ls_flag_tipo_tariffa = "P" then		// costo in base al peso

	if ld_arrotonda_peso <> 0 then
		
		ld_quan_tonda = (truncate((ld_peso_lordo / ld_arrotonda_peso), 0) * ld_arrotonda_peso)
		
		if (ld_peso_lordo - ld_quan_tonda) <> 0 then
			ld_quan_tonda = ld_quan_tonda + ld_arrotonda_peso
		end if
		
	else
		
		ld_quan_tonda = ld_peso_lordo
		
	end if
	
	ld_costo_quan = (ld_prezzo_spedizione + ld_prezzo_inoltro) * ld_quan_tonda
	
end if

if ls_flag_tipo_tariffa = "V" then		// costo in base al volume
	fs_messaggio = "Manca Volume in testata documento"
	return  -1
end if

if ls_flag_tipo_tariffa = "F" then		// costo Forfait
	ld_costo_quan = 0
end if

ld_costo_trasp     = ld_costo_forfait_sped + ld_costo_forfait_inol + ld_costo_quan + ld_costo_per_val + ld_costo_contrassegno

uof_arrotonda(ld_costo_trasp, id_precisione_valuta, "euro")

fd_costo_trasporto = ld_costo_trasp

return 0

end function

public function integer uof_sostituzione_date (date fdd_data, ref date fdd_data_sost, string fs_cod_cliente, string fs_cod_fornitore, string fs_cod_contatto, ref string fs_messaggio);long				ll_giorno_sost, ll_mese_sost, ll_anno_sost, ll_giorno_fisso_scadenza, ll_mese_esclusione_1, &
	  				ll_mese_esclusione_2, ll_data_sostituzione_1, ll_data_sostituzione_2

string				ls_tabella, ls_where_colonna, ls_sql, ls_errore
datastore		lds_data
long				ll_ggmm_esclusione_1_da, ll_ggmm_esclusione_1_a, ll_ggmm_esclusione_2_da, ll_ggmm_esclusione_2_a, ll_tot
date				ldd_data, ldd_data_1, ldd_data_2

// Lettura dati cliente / fornitore

//Donato 14/11/2012
//Le select qui sotto vengono modificate per recuperare i nuovi campi nelle tabelle dei clienti, fornitorti, contatti e fornitori potenziali
//anziche mese esclusione 1 e 2 vengono introdotte 4 nuove colonne:
// ggmm_esclusione_1_da ...  ggmm_esclusione_1_a  e   ggmm_esclusione_2_da ...  ggmm_esclusione_2_a
//in pratica la sostituzione date non avviene se il documento cade nel mese indicato
//ma se cade tra le date esclusione indicate nel formato giorno/mese
//(ES.  dal 01/08  al 31/08  sostituire con  07/09, mentre prima c'era solo il mese esclusione (8=Agosto))


if not isnull(fs_cod_cliente) and fs_cod_cliente<>"" then
	
	ls_tabella = "anag_clienti"
	ls_where_colonna = "cod_cliente='"+fs_cod_cliente+"' "
	
//	select giorno_fisso_scadenza,
//          mese_esclusione_1,
//          mese_esclusione_2,
//          data_sostituzione_1,
//          data_sostituzione_2
//   into   :ll_giorno_fisso_scadenza,
//          :ll_mese_esclusione_1,
//          :ll_mese_esclusione_2,
//          :ll_data_sostituzione_1,
//          :ll_data_sostituzione_2
//   from   anag_clienti
//   where  (cod_azienda = :s_cs_xx.cod_azienda ) AND
//          (cod_cliente = :fs_cod_cliente );
//			 
//	if sqlca.sqlcode = -1 then
//		fs_messaggio = "Errore nella select di anag_clienti: " + sqlca.sqlerrtext
//		return -1
//	end if
	
elseif not isnull(fs_cod_fornitore) and fs_cod_fornitore<>"" then
	
	ls_tabella = "anag_fornitori"
	ls_where_colonna = "cod_fornitore='"+fs_cod_fornitore+"' "
	
//	select giorno_fisso_scadenza,
//          mese_esclusione_1,
//          mese_esclusione_2,
//          data_sostituzione_1,
//          data_sostituzione_2
//   into   :ll_giorno_fisso_scadenza,
//          :ll_mese_esclusione_1,
//          :ll_mese_esclusione_2,
//          :ll_data_sostituzione_1,
//          :ll_data_sostituzione_2
//   from   anag_fornitori
//   where  (cod_azienda = :s_cs_xx.cod_azienda ) AND
//          (cod_fornitore = :fs_cod_fornitore );
//			 
//	if sqlca.sqlcode = -1 then
//		fs_messaggio = "Errore nella select di anag_fornitori: " + sqlca.sqlerrtext
//		return -1
//	end if

elseif not isnull(fs_cod_contatto) and fs_cod_contatto<>"" then
	ls_tabella = "anag_contatti"
	ls_where_colonna = "cod_contatto='"+fs_cod_contatto+"' "
	
else
	return 0
end if

ls_sql = "select 	giorno_fisso_scadenza,"+&
						"ggmm_esclusione_1_da,"+&
						"ggmm_esclusione_1_a,"+&
						"ggmm_esclusione_2_da,"+&
						"ggmm_esclusione_2_a,"+&
						"data_sostituzione_1,"+&
						"data_sostituzione_2 "+&
			"from "+ls_tabella+" "+&
			"where cod_azienda='"+s_cs_xx.cod_azienda+"' and " + ls_where_colonna

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_errore)
if ll_tot < 0 then
	fs_messaggio = "Errore nella select da tabella "+ls_tabella+": " + ls_errore
	return -1
end if

if ll_tot>0 then
	//sempre e solo una riga
	ll_giorno_fisso_scadenza  	= lds_data.getitemnumber(1, 1)
	ll_ggmm_esclusione_1_da	= lds_data.getitemnumber(1, 2)
	ll_ggmm_esclusione_1_a		= lds_data.getitemnumber(1, 3)
	ll_ggmm_esclusione_2_da	= lds_data.getitemnumber(1, 4)
	ll_ggmm_esclusione_2_a		= lds_data.getitemnumber(1, 5)
	ll_data_sostituzione_1		= lds_data.getitemnumber(1, 6)
	ll_data_sostituzione_2		= lds_data.getitemnumber(1, 7)
end if


fdd_data_sost = fdd_data

//if (ll_mese_esclusione_1 > 0) and not isnull(ll_mese_esclusione_1) then
if ll_ggmm_esclusione_1_da>0 and not isnull(ll_ggmm_esclusione_1_da) and ll_ggmm_esclusione_1_a>0 and not isnull(ll_ggmm_esclusione_1_a) then
	
	//valuto le date del primo periodo di esclusione
	if guo_functions.uof_get_date_from_giornomese(ll_ggmm_esclusione_1_da, year(fdd_data_sost), ldd_data_1) and &
			guo_functions.uof_get_date_from_giornomese(ll_ggmm_esclusione_1_a, year(fdd_data_sost), ldd_data_2) then
			
		//if month(fdd_data_sost) = ll_mese_esclusione_1 then
		if fdd_data_sost >= ldd_data_1 and fdd_data_sost<=ldd_data_2 then
			ll_giorno_sost = int(ll_data_sostituzione_1 / 100)
			ll_mese_sost   = ll_data_sostituzione_1 - (ll_giorno_sost * 100)
			ll_anno_sost   = year(fdd_data_sost)
			if ll_mese_sost < month(fdd_data_sost) then ll_anno_sost = ll_anno_sost + 1
			fdd_data_sost = uof_converti_data(string(ll_giorno_sost),string(ll_mese_sost),string(ll_anno_sost))
		end if
	end if
end if

//if (ll_mese_esclusione_2 > 0) and not isnull(ll_mese_esclusione_2) then
if ll_ggmm_esclusione_2_da>0 and not isnull(ll_ggmm_esclusione_2_da) and ll_ggmm_esclusione_2_a>0 and not isnull(ll_ggmm_esclusione_2_a) then
	
	//valuto le date del secondo periodo di esclusione
		if guo_functions.uof_get_date_from_giornomese(ll_ggmm_esclusione_2_da, year(fdd_data_sost), ldd_data_1) and &
			guo_functions.uof_get_date_from_giornomese(ll_ggmm_esclusione_2_a, year(fdd_data_sost), ldd_data_2) then
	
		//if month(fdd_data_sost) = ll_mese_esclusione_2 then
		if fdd_data_sost >= ldd_data_1 and fdd_data_sost<=ldd_data_2 then
			ll_giorno_sost = int(ll_data_sostituzione_2 / 100)
			ll_mese_sost   = ll_data_sostituzione_2 - (ll_giorno_sost * 100)
			ll_anno_sost   = year(fdd_data_sost)
			if ll_mese_sost < month(fdd_data_sost) then ll_anno_sost = ll_anno_sost + 1
			fdd_data_sost = uof_converti_data(string(ll_giorno_sost),string(ll_mese_sost),string(ll_anno_sost))
		end if
	end if
	
end if

if (ll_giorno_fisso_scadenza > 0) and not isnull(ll_giorno_fisso_scadenza) then
	fdd_data_sost = uof_converti_data(string(ll_giorno_fisso_scadenza),string(month(fdd_data)),string(year(fdd_data)))
end if

return 0
end function

public function string uof_tipo_fornitore (string as_cod_fornitore);string	ls_flag_tipo_fornitore


select 	flag_tipo_fornitore
into 		:ls_flag_tipo_fornitore
from 		anag_fornitori
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_fornitore = :as_cod_fornitore;
			
if isnull(ls_flag_tipo_fornitore) or len(ls_flag_tipo_fornitore) < 1 then ls_flag_tipo_fornitore = "S"

return ls_flag_tipo_fornitore
	
	
end function

public function integer uof_get_imponibile_riga (string as_tipo_input, long al_prog_riga, datawindow adw_data, string as_cod_tipo_dettaglio, decimal ad_prezzo_valuta, decimal ad_quantita, decimal ad_sconti_riga[], decimal ad_imponibile_forzato, string as_flag_imponibile_forzato, ref decimal ad_imponibile_riga, ref decimal ad_imponibile_iva_valuta, ref string fs_messaggio);dec{4}			ld_quantita, ld_prezzo_ven, ld_prezzo_ven_valuta, ld_imponibile_riga, ld_imponibile_riga_valuta,&
					ld_sconto_cassa, ld_sconto_cassa_valuta, ld_sconto_commerciale, ld_sconto_commerciale_valuta,&
					ld_valore_parziale, ld_valore_parziale_valuta, ld_sconto, ld_sconto_pagamento, ld_sconto_testata, &
					ld_sconti_riga[]
		 
long				ll_i, ll_j, ll_tot

string				ls_cod_pagamento, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_flag_sola_iva, &
					ls_flag_imponibile_forzato, ls_tabella_tes, ls_tabella_det, ls_sql, ls_colonna_anno, ls_colonna_numero, ls_colonna_tipo_det, ls_colonna_prezzo

datastore		lds_data


id_sconto_commerciale = 0
id_sconto_cassa = 0
id_spese_varie = 0
id_spese_bolli = 0
id_spese_imballo = 0
id_spese_trasporto = 0
id_merci = 0
ls_flag_imponibile_forzato = "N"


// Lettura di cod_pagamento e dello sconto specificato in testata
choose case is_tipo_documento

   case "bol_ven"
		ls_tabella_tes = "tes_bol_ven"
		ls_tabella_det = "det_bol_ven"
		ls_colonna_anno = "anno_registrazione"
		ls_colonna_numero = "num_registrazione"
		ls_colonna_tipo_det = "cod_tipo_det_ven"
		ls_colonna_prezzo = "prezzo_vendita"

	
	case "bol_acq"
		ls_tabella_tes = "tes_bol_acq"
		ls_tabella_det = "det_bol_acq"
		ls_colonna_anno = "anno_bolla_acq"
		ls_colonna_numero = "num_bolla_acq"
		ls_colonna_tipo_det = "cod_tipo_det_acq"
		ls_colonna_prezzo = "prezzo_acquisto"
	
	
	case "fat_ven"	
		ls_tabella_tes = "tes_fat_ven"
		ls_tabella_det = "det_fat_ven"
		ls_colonna_anno = "anno_registrazione"
		ls_colonna_numero = "num_registrazione"
		ls_colonna_tipo_det = "cod_tipo_det_ven"
		ls_colonna_prezzo = "prezzo_vendita"
		
	
	case "fat_acq"	
		ls_tabella_tes = "tes_fat_acq"
		ls_tabella_det = "det_fat_acq"
		ls_colonna_anno = "anno_registrazione"
		ls_colonna_numero = "num_registrazione"
		ls_colonna_tipo_det = "cod_tipo_det_acq"
		ls_colonna_prezzo = "prezzo_acquisto"
		
	case "off_ven"
		ls_tabella_tes = "tes_off_ven"
		ls_tabella_det = "det_off_ven"
		ls_colonna_anno = "anno_registrazione"
		ls_colonna_numero = "num_registrazione"
		ls_colonna_tipo_det = "cod_tipo_det_ven"
		ls_colonna_prezzo = "prezzo_vendita"
		
	
	case "off_acq"
		ls_tabella_tes = "tes_off_acq"
		ls_tabella_det = "det_off_acq"
		ls_colonna_anno = "anno_registrazione"
		ls_colonna_numero = "num_registrazione"
		ls_colonna_tipo_det = "cod_tipo_det_acq"
		ls_colonna_prezzo = "prezzo_acquisto"
		
	
	case "ord_ven"
		ls_tabella_tes = "tes_ord_ven"
		ls_tabella_det = "det_ord_ven"
		ls_colonna_anno = "anno_registrazione"
		ls_colonna_numero = "num_registrazione"
		ls_colonna_tipo_det = "cod_tipo_det_ven"
		ls_colonna_prezzo = "prezzo_vendita"
		
	
	case "ord_acq"
		ls_tabella_tes = "tes_ord_acq"
		ls_tabella_det = "det_ord_acq"
		ls_colonna_anno = "anno_registrazione"
		ls_colonna_numero = "num_registrazione"
		ls_colonna_tipo_det = "cod_tipo_det_acq"
		ls_colonna_prezzo = "prezzo_acquisto"
		
	
end choose


ls_sql = "select cod_pagamento, sconto from " + ls_tabella_tes + " " + &
		   "where  cod_azienda='"+s_cs_xx.cod_azienda+"' and "+ls_colonna_anno+"="+string(il_anno_registrazione)+" and "+ls_colonna_numero+"="+string(il_num_registrazione)

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, fs_messaggio)

if ll_tot < 0 then
	return -1
elseif ll_tot=0 then
	fs_messaggio = "Documento testata inesistente!"
	destroy lds_data
	return -1
end if

//se ci sono dati sono su un'unica riga
ls_cod_pagamento = lds_data.getitemstring(1, 1)
ld_sconto_testata = lds_data.getitemnumber(1, 2)

destroy lds_data

if isnull(ld_sconto_testata) then ld_sconto_testata = 0

//--------------------------------------------------------------------------------------------------------------------------------------------
if as_tipo_input="table" then
	ls_sql = "select "+ls_colonna_tipo_det+","+ls_colonna_prezzo+","+is_colonna_quantita+","+&
						"sconto_1,sconto_2,sconto_3,sconto_4,sconto_5,sconto_6,sconto_7,sconto_8,sconto_9,sconto_10"
	
	if is_tipo_documento = "fat_acq" then
		ls_sql += ",imponibile_iva_valuta,flag_imponibile_forzato"
	end if
	
	ls_sql += " from " + ls_tabella_det + " "+&
				"where	cod_azienda='"+s_cs_xx.cod_azienda+"' and "+ls_colonna_anno+"="+string(il_anno_registrazione)+" and "+&
							ls_colonna_numero+"="+string(il_num_registrazione) +" and "+is_colonna_prog_riga+"="+string(al_prog_riga)

	ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, fs_messaggio)
	
	if ll_tot < 0 then
		return -1
	elseif ll_tot=0 then
		fs_messaggio = "Riga Documento inesistente!"
		destroy lds_data
		return -1
	end if
	
	//se ci sono dati sono su un'unica riga
	ls_cod_tipo_det_ven = lds_data.getitemstring(1, 1)
	ld_prezzo_ven_valuta = lds_data.getitemnumber(1, 2)
	ld_quantita = lds_data.getitemnumber(1, 3)
	ld_sconti_riga[1] = lds_data.getitemnumber(1, 4)
	ld_sconti_riga[2] = lds_data.getitemnumber(1, 5)
	ld_sconti_riga[3] = lds_data.getitemnumber(1, 6)
	ld_sconti_riga[4] = lds_data.getitemnumber(1, 7)
	ld_sconti_riga[5] = lds_data.getitemnumber(1, 8)
	ld_sconti_riga[6] = lds_data.getitemnumber(1, 9)
	ld_sconti_riga[7] = lds_data.getitemnumber(1, 10)
	ld_sconti_riga[8] = lds_data.getitemnumber(1, 11)
	ld_sconti_riga[9] = lds_data.getitemnumber(1, 12)
	ld_sconti_riga[10] = lds_data.getitemnumber(1, 13)
	
	if is_tipo_documento = "fat_acq" then
		ld_imponibile_riga_valuta = lds_data.getitemnumber(1, 14)
		ls_flag_imponibile_forzato = lds_data.getitemstring(1, 15)
	end if
	
	destroy lds_data

//--------------------------------------------------------------------------------------------------------------------------------------------
elseif as_tipo_input="input" then
	//la riga non è salvata, quidi ho passato i valori sulla riga come argomento della funzione
	ls_cod_tipo_det_ven = as_cod_tipo_dettaglio
	ld_prezzo_ven_valuta = ad_prezzo_valuta
	ld_quantita = ad_quantita
	ld_sconti_riga[] = ad_sconti_riga[]

	if is_tipo_documento = "fat_acq" then
		ld_imponibile_riga_valuta = ad_imponibile_forzato
		ls_flag_imponibile_forzato = as_flag_imponibile_forzato
	end if
//--------------------------------------------------------------------------------------------------------------------------------------------
else
	//su datawindow
	ls_cod_tipo_det_ven = adw_data.getitemstring(al_prog_riga, ls_colonna_tipo_det)
	ld_prezzo_ven_valuta = adw_data.getitemnumber(al_prog_riga, ls_colonna_prezzo)
	ld_quantita = adw_data.getitemnumber(al_prog_riga, is_colonna_quantita)

	//se arriva quantità nulla imposta quantità unitaria
	if isnull(ld_quantita) or ld_quantita=0 then ld_quantita = 1

	for ll_i=1 to 10
		ld_sconti_riga[ll_i] = adw_data.getitemnumber(al_prog_riga, "sconto_"+string(ll_i))
	next

	if is_tipo_documento = "fat_acq" then
		ld_imponibile_riga_valuta = adw_data.getitemnumber(al_prog_riga, "imponibile_iva_valuta")
		ls_flag_imponibile_forzato = adw_data.getitemstring(al_prog_riga, "flag_imponibile_forzato")
	end if
end if



// Lettura dello sconto eventualmente specificato per il tipo di pagamento in questione
select sconto
into   :ld_sconto_pagamento
from   tab_pagamenti
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
		 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode = -1 then
	fs_messaggio = "Errore nella select di tab_pagamenti: " + sqlca.sqlerrtext
	return -1
end if

if isnull(ld_sconto_pagamento) then ld_sconto_pagamento = 0

ld_sconto_commerciale_valuta = 0
ld_sconto_commerciale = 0
ld_sconto_cassa_valuta = 0
ld_sconto_cassa = 0

	
choose case is_tipo_documento

	case "bol_ven","fat_ven","off_ven","ord_ven"
		select flag_tipo_det_ven,
				 flag_sola_iva
		into   :ls_flag_tipo_det_ven,
				 :ls_flag_sola_iva
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
				 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella select di tab_tipi_det_ven: " + sqlca.sqlerrtext
			return -1
		end if

		ld_prezzo_ven = ld_prezzo_ven_valuta * id_cambio
		
		
	case "bol_acq","fat_acq","off_acq","ord_acq"
		select flag_tipo_det_acq,
				 flag_sola_iva
		into   :ls_flag_tipo_det_ven,
				 :ls_flag_sola_iva
		from   tab_tipi_det_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_det_acq = :ls_cod_tipo_det_ven;
				 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore nella select di tab_tipi_det_acq: " + sqlca.sqlerrtext
			return -1
		end if

		ld_prezzo_ven = ld_prezzo_ven_valuta * id_cambio
		
		if is_tipo_documento	= "fat_acq" then
			//ls_flag_imponibile_forzato letto dalla select from det_fat_acq precedente
		end if

end choose	

// Se è una fattura di acquisto con imponibile forzato, salto calcolo riga.
if ls_flag_imponibile_forzato = "S" then
	
	//ld_imponibile_riga_valuta letto dalla select from det_fat_acq precedente
	ld_imponibile_riga = ld_imponibile_riga_valuta * id_cambio
	
	// se è stato impostato un cambio arrotondo in automatico; se il cambio è 1 tengo tutto come è
	if id_cambio <> 1 then
		uof_arrotonda(ld_imponibile_riga, id_precisione, "euro")
	end if
			
else
	// Calcolo del valore riga
	ld_imponibile_riga_valuta = ld_quantita * ld_prezzo_ven_valuta
	
	// Arrotondamento del valore riga in valuta
	uof_arrotonda(ld_imponibile_riga_valuta, id_precisione_valuta, "euro")
	
	if id_cambio <> 1 then
		ld_imponibile_riga = ld_imponibile_riga_valuta * id_cambio
	else
		ld_imponibile_riga = ld_quantita * ld_prezzo_ven
	end if
	
	// Arrotondamento del valore riga in base alla valuta del sistema
	uof_arrotonda(ld_imponibile_riga, id_precisione, "euro")

end if
	
// Se la riga rappresenta uno sconto non vengono cercati ulteriori sconti per quella riga
if ls_flag_tipo_det_ven <> 'S' then
	
	// Controllo  totale forzato in fattura di acquisto
	if ls_flag_imponibile_forzato <> "S" then
	
		// Si usano due variabili di appoggio per non perdere il valore originale di valore riga
		ld_valore_parziale_valuta = ld_imponibile_riga_valuta
		ld_valore_parziale = ld_imponibile_riga

		// Sul valore riga vengono applicati gli sconti (da sconto_1 a sconto_10)
		for ll_j = 1 to 10
			ld_sconto = ld_sconti_riga[ll_j]
		
			if ld_sconto = 0 or isnull(ld_sconto) then
				continue
			end if	
		
			// Si calcola il totale degli sconti di riga
			ld_sconto_commerciale_valuta = ld_sconto_commerciale_valuta + (ld_valore_parziale_valuta / 100 * ld_sconto)
			ld_sconto_commerciale = ld_sconto_commerciale + (ld_valore_parziale / 100 * ld_sconto)
			ld_valore_parziale_valuta = ld_valore_parziale_valuta - (ld_valore_parziale_valuta / 100 * ld_sconto)
			ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto)
	
		next	
	
		// Al totale degli sconti di riga viene applicato anche lo sconto di testata per ottenere lo sconto commerciale
		ld_sconto_commerciale_valuta = ld_sconto_commerciale_valuta + (ld_valore_parziale_valuta / 100 * ld_sconto_testata)
		ld_sconto_commerciale = ld_sconto_commerciale + (ld_valore_parziale / 100 * ld_sconto_testata)
		ld_valore_parziale_valuta = ld_valore_parziale_valuta - (ld_valore_parziale_valuta / 100 * ld_sconto_testata)
		ld_valore_parziale = ld_valore_parziale - (ld_valore_parziale / 100 * ld_sconto_testata)
		
		// Lo sconto commerciale viene arrotondato
		uof_arrotonda(ld_sconto_commerciale_valuta, id_precisione_valuta, "euro")
		uof_arrotonda(ld_sconto_commerciale, id_precisione, "euro")
		
		// Viene aggiornato il valore dello sconto commerciale
		id_sconto_commerciale = id_sconto_commerciale + ld_sconto_commerciale
				
		// Viene calolato lo sconto di pagamento		
		ld_sconto_cassa_valuta = (ld_valore_parziale_valuta / 100 * ld_sconto_pagamento)
		ld_sconto_cassa = (ld_valore_parziale / 100 * ld_sconto_pagamento)		
	
		// Lo sconto di pagamento viene arrotondato
		uof_arrotonda(ld_sconto_cassa_valuta, id_precisione_valuta, "euro")
		uof_arrotonda(ld_sconto_cassa, id_precisione, "euro")		
		
		// Viene aggiornato il valore dello sconto di pagamento
		id_sconto_cassa = id_sconto_cassa + ld_sconto_cassa
		
	end if		
	
	// Nella totalizzazione dei parziali non vengono considerati gli imponibili delle righe di sola iva
	if ls_flag_sola_iva <> 'S' then
	
		choose case ls_flag_tipo_det_ven
			
			case "V"
				
				id_spese_varie = id_spese_varie + ld_imponibile_riga
			
			case "B"
				
				id_spese_bolli = id_spese_bolli + ld_imponibile_riga
				
			case "I"
				
				id_spese_imballo = id_spese_imballo + ld_imponibile_riga
				
			case "T"
				
				id_spese_trasporto = id_spese_trasporto + ld_imponibile_riga
				
			case else
				
				id_merci = id_merci + ld_imponibile_riga
				
		end choose
		
	end if
	
	// Sul valore riga vengono applicati gli sconti per ottenere l'imponibile
	ld_imponibile_riga_valuta = ld_imponibile_riga_valuta - ld_sconto_commerciale_valuta - ld_sconto_cassa_valuta
	ld_imponibile_riga = ld_imponibile_riga - ld_sconto_commerciale - ld_sconto_cassa		
			
else	
	
	// Viene aggiornato il valore dello sconto commerciale
	id_sconto_commerciale = id_sconto_commerciale + ld_imponibile_riga
	
	// Le righe di sconto dovranno avere imponibile negativo
	ld_imponibile_riga_valuta = ld_imponibile_riga_valuta * -1
	ld_imponibile_riga = ld_imponibile_riga * -1		
	
end if

//in ld_imponibile_riga_valuta e ld_imponibile_riga i valori voluti
ad_imponibile_iva_valuta = ld_imponibile_riga_valuta
ad_imponibile_riga = ld_imponibile_riga


return 0
end function

public function integer uof_calcola_rate (decimal fd_imponibile, decimal fd_iva, string fs_cod_pagamento, date fdd_data_partenza, ref date fdd_rate_data[], ref decimal fd_rate_importo[], ref string fs_rate_tipo[], ref string fs_modalita[], ref long fl_tot_rate, ref string fs_messaggio);string ls_tipo_scadenza, ls_tipo_rata, ls_modalita_pagamento

long 	 ll_giorni_rata, ll_num_rate_int, ll_giorni_int, ll_giorno_fisso_rata, ll_i, ll_month, ll_year, ll_num_mesi

dec{6} ld_perc_iva, ld_perc_imp, ld_controllo, ld_scarto, ld_importo_rata, ld_importo_int

date		ldd_ultima_data_calcolata


// stefanop: se il codice pagamento non c'è non calcolo nessuna rata
if isnull(fs_cod_pagamento) or fs_cod_pagamento = "" then
	return 0
end if

// Lettura configurazione rate pagamento

declare rate cursor for
select 	giorni_decorrenza,
		 	numero_rate,
		 	giorni_tra_rate,
		 	tipo_scadenza,
		 	giorno_fisso,
		 	perc_iva,
		 	perc_imponibile,
			cod_tipo_pagamento,
			fatel_modalita_pagamento
from   	tab_pagamenti_rate
where  	cod_azienda = :s_cs_xx.cod_azienda and
		 	cod_pagamento = :fs_cod_pagamento
order by ordine_rata;

open rate;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in lettura rate pagamento.~nErrore nella open del cursore rate: " + sqlca.sqlerrtext
	return -1
end if

fl_tot_rate = 0

ld_controllo = 0

do while true
	
	fetch rate
	into  :ll_giorni_rata,
			:ll_num_rate_int,
			:ll_giorni_int,
			:ls_tipo_scadenza,
			:ll_giorno_fisso_rata,
			:ld_perc_iva,
			:ld_perc_imp,
			:ls_tipo_rata,
			:ls_modalita_pagamento;
			
	if sqlca.sqlcode < 0 then
		fs_messaggio = "Errore in lettura rate pagamento.~nErrore nella fetch del cursore rate: " + sqlca.sqlerrtext
		close rate;
		return -1
	elseif sqlca.sqlcode = 100 then
		close rate;
		exit
	end if
	
	if isnull(ll_giorni_rata) then
		ll_giorni_rata = 0
	end if
	
	if isnull(ll_num_rate_int) then
		ll_num_rate_int = 0
	end if
	
	if ls_tipo_scadenza = "F" and isnull(ll_giorno_fisso_rata) then
		fs_messaggio = "Impossibile calcolare una scadenza fissa senza indicazione del giorno fisso: " + &
							"verificare la configurazione delle rate in tabella pagamenti"
		close rate;
		return -1
	end if
	
	if isnull(ld_perc_iva) then
		ld_perc_iva = 0
	end if
	
	if isnull(ld_perc_imp) then
		ld_perc_imp = 0
	end if
	
	ld_importo_rata = (fd_imponibile * ld_perc_imp / 100) + (fd_iva * ld_perc_iva / 100)
	
	uof_arrotonda(ld_importo_rata,id_precisione,"difetto")
	
	ld_importo_int = ld_importo_rata / ll_num_rate_int
	
	uof_arrotonda(ld_importo_int,id_precisione,"difetto")
	
	// Calcolo singola rata fattura
	
	for ll_i = 1 to ll_num_rate_int
	
		fl_tot_rate ++
		
		if ll_i > 1 then
			//fdd_rate_data[fl_tot_rate] = fdd_rate_data[fl_tot_rate - 1]
			
			//riprendo dall'ultima rata calcolata, che potrebbe essere diversa da fdd_rate_data[fl_tot_rate - 1] (caso spostamento per scadenza fine mese + giorno fisso, del mese successivo)
			fdd_rate_data[fl_tot_rate] = ldd_ultima_data_calcolata
			
			ll_giorni_rata = ll_giorni_int
		else
			fdd_rate_data[fl_tot_rate] = fdd_data_partenza
		end if
		
//		if not isnull(ll_giorni_rata) and ll_giorni_rata > 0 then
//			// se sono a gennaio, sommando 30 gg vado a marzo, quindi correggo
//			if ll_giorni_rata = 30 and month(fdd_rate_data[fl_tot_rate]) = 2 then
//				ll_giorni_rata = 28
//			end if
//			fdd_rate_data[fl_tot_rate] = relativedate(fdd_rate_data[fl_tot_rate],ll_giorni_rata)
//		end if

		if not isnull(ll_giorni_rata) and ll_giorni_rata > 0 then
			
			if mod(ll_giorni_rata, 30) = 0 then
				
				ll_num_mesi = ll_giorni_rata / 30
				ll_month = month(fdd_rate_data[fl_tot_rate]) + ll_num_mesi
				ll_year  = year(fdd_rate_data[fl_tot_rate])
				
				if ll_month > 12 then
					ll_month = ll_month - 12
					ll_year ++
				end if
				
				fdd_rate_data[fl_tot_rate] = uof_converti_data(string(day(fdd_rate_data[fl_tot_rate])), string(ll_month), string(ll_year))
				
			else
				
				fdd_rate_data[fl_tot_rate] = relativedate(fdd_rate_data[fl_tot_rate],ll_giorni_rata)
				
			end if
			
		end if
		
		//salvo in una variabile la data fin qui ottenuta -----------
		ldd_ultima_data_calcolata = fdd_rate_data[fl_tot_rate]
		//---------------------------------------------------------------
		
		if ls_tipo_scadenza = "M" then
			
			fdd_rate_data[fl_tot_rate] = uof_calcola_data_fine_mese(fdd_rate_data[fl_tot_rate])
			
			//salvo in una variabile la data fin qui ottenuta -----------
			ldd_ultima_data_calcolata = fdd_rate_data[fl_tot_rate]
			//---------------------------------------------------------------
			
			//------------------------------------------------------------------------------------------
			//Donato 16/11/2012
			//Se è impostato anche giorno_fisso sposto ancora a partire dal fine mese, e quindi sicuramente cadrò nel mese successivo
			if ll_giorno_fisso_rata > 0 then
				ll_month = month(fdd_rate_data[fl_tot_rate]) + 1
				
				ll_year  = year(fdd_rate_data[fl_tot_rate])
				//se ho sforato vuol dire che devo andare al mese corrispondente nell'anno successivo
				if ll_month > 12 then
					ll_month = ll_month - 12
					ll_year ++
				end if
				
				fdd_rate_data[fl_tot_rate] = uof_converti_data(string(ll_giorno_fisso_rata), string(ll_month), string(ll_year))
			end if
			//------------------------------------------------------------------------------------------
			
			
		elseif ls_tipo_scadenza = "F" then
			
//			fdd_rate_data[fl_tot_rate] = relativedate(fdd_rate_data[fl_tot_rate],ll_giorno_fisso_rata)

			if ll_i > 1 then
				ll_month = month(fdd_rate_data[fl_tot_rate])
			else
				ll_month = month(fdd_rate_data[fl_tot_rate]) + 1
			end if
			
			ll_year  = year(fdd_rate_data[fl_tot_rate])
			if ll_month > 12 then
				ll_month = ll_month - 12
				ll_year ++
			end if
			
			fdd_rate_data[fl_tot_rate] = uof_converti_data(string(ll_giorno_fisso_rata), string(ll_month), string(ll_year))
			
			//salvo in una variabile la data fin qui ottenuta -----------
			ldd_ultima_data_calcolata = fdd_rate_data[fl_tot_rate]
			//---------------------------------------------------------------
			
		end if
		
		// Correzione eventuale scarto fra totale rate intermedie e importo rata
		
		if ll_i = ll_num_rate_int then
			
			ld_scarto = ld_importo_rata - (ld_importo_int * ll_num_rate_int)
			
			if ld_scarto <> 0 then
				ld_importo_int = ld_importo_int + ld_scarto
			end if
			
		end if
		
		fd_rate_importo[fl_tot_rate] = ld_importo_int
		
		// Incremento sommatoria importi rate
		
		ld_controllo = ld_controllo + fd_rate_importo[fl_tot_rate]
		
		fs_rate_tipo[fl_tot_rate] = ls_tipo_rata
	 	
		fs_modalita[][fl_tot_rate] = ls_modalita_pagamento
		 //salvo l'ultima data della rata appena calcolata
		 
	next
	
loop

if fl_tot_rate < 1 then
	fs_messaggio = "Non sono state configurate le rate per il pagamento " + fs_cod_pagamento
	return -1
end if

// Controllo differenza fra totale documento e sommatoria importi rate

ld_scarto = (fd_imponibile + fd_iva) - ld_controllo

// Correzione eventuale scarto incrementando l'ultima rata

if ld_scarto <> 0 then
	fd_rate_importo[fl_tot_rate] = fd_rate_importo[fl_tot_rate] + ld_scarto
end if

return 0
end function

public function integer uof_salda_ordine (long al_anno_registrazione, long al_num_registrazione, ref string as_message);boolean lb_dt1

long  ll_prog_riga_ord_ven, ll_i

string ls_cod_tipo_det_ven, ls_cod_prodotto, ls_sql_stringa, ls_flag_tipo_det_ven, &
		 ls_flag_blocco,ls_cod_prodotto_raggruppato

dec{4} ld_quan_ordine, ld_quan_evasa, ld_quan_in_evasione, ld_quan_raggruppo


if al_anno_registrazione>0 and al_num_registrazione>0 then
else
	as_message = "E' stato passato un numero e anno non valido alla procedura di SALDO ORDINE."
	return -1
end if

declare cu_dettagli dynamic cursor for sqlsa;

ls_sql_stringa = "select det_ord_ven.cod_tipo_det_ven, det_ord_ven.prog_riga_ord_ven, det_ord_ven.cod_prodotto, det_ord_ven.quan_ordine, det_ord_ven.quan_in_evasione, det_ord_ven.quan_evasa, det_ord_ven.flag_blocco from det_ord_ven where det_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_ord_ven.anno_registrazione = " + string(al_anno_registrazione) + " and det_ord_ven.num_registrazione = " + string(al_num_registrazione)

prepare sqlsa from :ls_sql_stringa;

open dynamic cu_dettagli;
if sqlca.sqlcode = -1 then
	as_message = "Si è verificato un errore in OPEN del cursore cu_dettagli (uo_calcola_documento.uof_salda_ordine())."
	close cu_dettagli;
	rollback;
	return -1
end if

ll_i = 0
do while true
	ll_i ++
	fetch cu_dettagli into :ls_cod_tipo_det_ven, :ll_prog_riga_ord_ven, :ls_cod_prodotto, :ld_quan_ordine, :ld_quan_in_evasione, :ld_quan_evasa, :ls_flag_blocco;
	if sqlca.sqlcode = -1 then
		as_message = "Si è verificato un errore in fase di lettura per aggiornamento magazzino."
		close cu_dettagli;
		rollback;
		return -1
	end if

	if sqlca.sqlcode <> 0 then exit

	select tab_tipi_det_ven.flag_tipo_det_ven
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	if sqlca.sqlcode = -1 then
		as_message = "Si è verificato un errore in fase di lettura tipi dettagli.~r~n" + sqlca.sqlerrtext
		close cu_dettagli;
		rollback;
		return -1
	end if

	if ls_flag_tipo_det_ven = "M" and ld_quan_evasa < ld_quan_ordine and ls_flag_blocco = "N" then
		update anag_prodotti  
			set quan_impegnata = quan_impegnata - (:ld_quan_ordine - :ld_quan_evasa)
		 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

		if sqlca.sqlcode = -1 then
			as_message = "Si è verificato un errore in fase di aggiornamento magazzino.~r~n" + sqlca.sqlerrtext
			rollback;
			close cu_dettagli;
			return -1
		end if
		
		// enme 08/1/2006 gestione prodotto raggruppato
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then
			
			ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, (ld_quan_ordine - ld_quan_evasa), "M")
			
			update anag_prodotti  
				set quan_impegnata = quan_impegnata - :ld_quan_raggruppo
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto_raggruppato;
	
			if sqlca.sqlcode = -1 then
				as_message =  "Si è verificato un errore in fase di aggiornamento magazzino.~r~n" + sqlca.sqlerrtext
				rollback;
				close cu_dettagli;
				return -1
			end if
		end if
		
	end if				
	
	update det_ord_ven
		set flag_evasione = 'E'
	 where cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_registrazione = :al_anno_registrazione and  
			 num_registrazione = :al_num_registrazione and
			 prog_riga_ord_ven = :ll_prog_riga_ord_ven;

	if sqlca.sqlcode = -1 then
		as_message = "Si è verificato un errore in fase di aggiornamento dettaglio ordine.~r~n" + sqlca.sqlerrtext
		rollback;
		close cu_dettagli;
		return -1
	end if

	update tes_ord_ven
	set flag_evasione = 'E'
	where cod_azienda = :s_cs_xx.cod_azienda and  
			anno_registrazione = :al_anno_registrazione and  
			num_registrazione = :al_num_registrazione;

	if sqlca.sqlcode = -1 then
		as_message = "Si è verificato un errore in fase di aggiornamento testata ordine.~r~n" + sqlca.sqlerrtext
		rollback;
		close cu_dettagli;
		return -1
	end if
	
	// EnMe 04/05/2012: su richiesta di Beatrice cancello le distinte di taglio collegate.
	
	guo_functions.uof_get_parametro( "DT1", lb_dt1)
	
	if  not lb_dt1 then
		delete distinte_taglio_calcolate
		where  cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:al_anno_registrazione and
				num_registrazione=:al_num_registrazione;
		
		if sqlca.sqlcode = -1 then
			as_message = "Si è verificato un errore in fase di cancellazione distinte di taglio calcolate."
			rollback;
			close cu_dettagli;
			return -1
		end if
	end if
loop

close cu_dettagli;
commit;

return 0


end function

on uo_calcola_documento_euro.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_calcola_documento_euro.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;// Alla creazione dell'oggetto viene istanziato anche l'oggetto datastore
ids_righe_documento = create datastore

// Lettura precisione della valuta euro
select precisione
into   :id_precisione
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_valuta = (select stringa
							from   parametri_azienda
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 flag_parametro = 'S' and
									 cod_parametro = 'EUR');

if sqlca.sqlcode < 0 then
	id_precisione = -1
elseif sqlca.sqlcode = 100 then
	setnull(id_precisione)
end if
end event

event destructor;// Al momento di distruggere l'oggetto viene distrutto anche il datastore
destroy ids_righe_documento
end event

