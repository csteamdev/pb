﻿$PBExportHeader$uo_cerca_destinazione.sru
$PBExportComments$UO di ricerca e scrittura destinazione merce in testata documenti ( per fatture !!!! )
forward
global type uo_cerca_destinazione from nonvisualobject
end type
end forward

global type uo_cerca_destinazione from nonvisualobject
end type
global uo_cerca_destinazione uo_cerca_destinazione

type variables

end variables

forward prototypes
public function integer wf_cerca_destinazione (uo_cs_xx_dw fdw_oggetto, long fl_num_riga, string fs_cod_clien_for, string fs_flag_clien_for, string fs_cod_destinazione, string fs_flag_tipo_fat_ven, string fs_messaggio)
public subroutine wf_protezione (string wfs_flag_tipo_fat_ven, long wfl_row)
public subroutine wf_protezione (uo_cs_xx_dw fdw_oggetto, string wfs_flag_tipo_fat_ven, long wfl_row)
end prototypes

public function integer wf_cerca_destinazione (uo_cs_xx_dw fdw_oggetto, long fl_num_riga, string fs_cod_clien_for, string fs_flag_clien_for, string fs_cod_destinazione, string fs_flag_tipo_fat_ven, string fs_messaggio);string ls_cod_des_cliente, ls_cod_des_fornitore, ls_null, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cap, &
       ls_localita, ls_provincia, ls_flag_dest_merce_fat
long li_count

datetime ldt_oggi

ldt_oggi = datetime(today())

setnull(ls_null)

//-------------------------------- Modifica_Nicola --------------------------------------
fdw_oggetto.setitem(fl_num_riga, "rag_soc_1", ls_null)
fdw_oggetto.setitem(fl_num_riga, "rag_soc_2", ls_null)
fdw_oggetto.setitem(fl_num_riga, "indirizzo", ls_null)
fdw_oggetto.setitem(fl_num_riga, "frazione", ls_null)
fdw_oggetto.setitem(fl_num_riga, "cap", ls_null)
fdw_oggetto.setitem(fl_num_riga, "localita", ls_null)
fdw_oggetto.setitem(fl_num_riga, "provincia", ls_null)
fdw_oggetto.setitem(fl_num_riga, "rag_soc_1_dest_fat", ls_null)
fdw_oggetto.setitem(fl_num_riga, "rag_soc_2_dest_fat", ls_null)
fdw_oggetto.setitem(fl_num_riga, "indirizzo_dest_fat", ls_null)
fdw_oggetto.setitem(fl_num_riga, "frazione_dest_fat", ls_null)
fdw_oggetto.setitem(fl_num_riga, "cap_dest_fat", ls_null)
fdw_oggetto.setitem(fl_num_riga, "localita_dest_fat", ls_null)
fdw_oggetto.setitem(fl_num_riga, "provincia_dest_fat", ls_null)

if fs_flag_tipo_fat_ven = "I" then
	ls_flag_dest_merce_fat = "S"
	fdw_oggetto.Modify("rag_soc_1_dest_fat.Background.Color='12632256'")
	fdw_oggetto.Object.rag_soc_1_dest_fat.Protect=1
	fdw_oggetto.Modify("rag_soc_2_dest_fat.Background.Color='12632256'")
	fdw_oggetto.Object.rag_soc_2_dest_fat.Protect=1
	fdw_oggetto.Modify("indirizzo_dest_fat.Background.Color='12632256'")
	fdw_oggetto.Object.indirizzo_dest_fat.Protect=1
	fdw_oggetto.Modify("frazione_dest_fat.Background.Color='12632256'")
	fdw_oggetto.Object.frazione_dest_fat.Protect=1				
	fdw_oggetto.Modify("cap_dest_fat.Background.Color='12632256'")
	fdw_oggetto.Object.cap_dest_fat.Protect=1								
	fdw_oggetto.Modify("localita_dest_fat.Background.Color='12632256'")
	fdw_oggetto.Object.localita_dest_fat.Protect=1								
	fdw_oggetto.Modify("provincia_dest_fat.Background.Color='12632256'")
	fdw_oggetto.Object.provincia_dest_fat.Protect=1								
	
	fdw_oggetto.Modify("rag_soc_1.Background.Color='16777215'")
	fdw_oggetto.Object.rag_soc_1.Protect=0
	fdw_oggetto.Modify("rag_soc_2.Background.Color='16777215'")
	fdw_oggetto.Object.rag_soc_2.Protect=0
	fdw_oggetto.Modify("indirizzo.Background.Color='16777215'")
	fdw_oggetto.Object.indirizzo.Protect=0
	fdw_oggetto.Modify("frazione.Background.Color='16777215'")
	fdw_oggetto.Object.frazione.Protect=0				
	fdw_oggetto.Modify("cap.Background.Color='16777215'")
	fdw_oggetto.Object.cap.Protect=0								
	fdw_oggetto.Modify("localita.Background.Color='16777215'")
	fdw_oggetto.Object.localita.Protect=0								
	fdw_oggetto.Modify("provincia.Background.Color='16777215'")
	fdw_oggetto.Object.provincia.Protect=0															
else 	
	ls_flag_dest_merce_fat = "N"
	fdw_oggetto.Modify("rag_soc_1.Background.Color='12632256'")
	fdw_oggetto.Object.rag_soc_1.Protect=1
	fdw_oggetto.Modify("rag_soc_2.Background.Color='12632256'")
	fdw_oggetto.Object.rag_soc_2.Protect=1
	fdw_oggetto.Modify("indirizzo.Background.Color='12632256'")
	fdw_oggetto.Object.indirizzo.Protect=1
	fdw_oggetto.Modify("frazione.Background.Color='12632256'")
	fdw_oggetto.Object.frazione.Protect=1				
	fdw_oggetto.Modify("cap.Background.Color='12632256'")
	fdw_oggetto.Object.cap.Protect=1								
	fdw_oggetto.Modify("localita.Background.Color='12632256'")
	fdw_oggetto.Object.localita.Protect=1								
	fdw_oggetto.Modify("provincia.Background.Color='12632256'")
	fdw_oggetto.Object.provincia.Protect=1											
	
	fdw_oggetto.Modify("rag_soc_1_dest_fat.Background.Color='16777215'")
	fdw_oggetto.Object.rag_soc_1_dest_fat.Protect=0
	fdw_oggetto.Modify("rag_soc_2_dest_fat.Background.Color='16777215'")
	fdw_oggetto.Object.rag_soc_2_dest_fat.Protect=0
	fdw_oggetto.Modify("indirizzo_dest_fat.Background.Color='16777215'")
	fdw_oggetto.Object.indirizzo_dest_fat.Protect=0
	fdw_oggetto.Modify("frazione_dest_fat.Background.Color='16777215'")
	fdw_oggetto.Object.frazione_dest_fat.Protect=0				
	fdw_oggetto.Modify("cap_dest_fat.Background.Color='16777215'")
	fdw_oggetto.Object.cap_dest_fat.Protect=0								
	fdw_oggetto.Modify("localita_dest_fat.Background.Color='16777215'")
	fdw_oggetto.Object.localita_dest_fat.Protect=0								
	fdw_oggetto.Modify("provincia_dest_fat.Background.Color='16777215'")
	fdw_oggetto.Object.provincia_dest_fat.Protect=0								
end if	
//--------------------------------- Fine Modifica ---------------------------------------

choose case fs_flag_clien_for
	case "C"
		if not isnull(fs_cod_destinazione) then
			select anag_des_clienti.rag_soc_1,
					 anag_des_clienti.rag_soc_2,
					 anag_des_clienti.indirizzo,
					 anag_des_clienti.frazione,
					 anag_des_clienti.cap,
					 anag_des_clienti.localita,
					 anag_des_clienti.provincia
			into   :ls_rag_soc_1,    
					 :ls_rag_soc_2,
					 :ls_indirizzo,    
					 :ls_frazione,
					 :ls_cap,
					 :ls_localita,
					 :ls_provincia
			from   anag_des_clienti
			where  anag_des_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_des_clienti.cod_cliente = :fs_cod_clien_for and
					 anag_des_clienti.cod_des_cliente = :fs_cod_destinazione and
					 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_oggi ));
			if sqlca.sqlcode = 0 then
				if fs_flag_tipo_fat_ven = "I" then
					fdw_oggetto.setitem(fl_num_riga, "rag_soc_1", ls_rag_soc_1)
					fdw_oggetto.setitem(fl_num_riga, "rag_soc_2", ls_rag_soc_2)
					fdw_oggetto.setitem(fl_num_riga, "indirizzo", ls_indirizzo)
					fdw_oggetto.setitem(fl_num_riga, "frazione", ls_frazione)
					fdw_oggetto.setitem(fl_num_riga, "cap", ls_cap)
					fdw_oggetto.setitem(fl_num_riga, "localita", ls_localita)
					fdw_oggetto.setitem(fl_num_riga, "provincia", ls_provincia)
				else 	
					fdw_oggetto.setitem(fl_num_riga, "rag_soc_1_dest_fat", ls_rag_soc_1)
					fdw_oggetto.setitem(fl_num_riga, "rag_soc_2_dest_fat", ls_rag_soc_2)
					fdw_oggetto.setitem(fl_num_riga, "indirizzo_dest_fat", ls_indirizzo)
					fdw_oggetto.setitem(fl_num_riga, "frazione_dest_fat", ls_frazione)
					fdw_oggetto.setitem(fl_num_riga, "cap_dest_fat", ls_cap)
					fdw_oggetto.setitem(fl_num_riga, "localita_dest_fat", ls_localita)
					fdw_oggetto.setitem(fl_num_riga, "provincia_dest_fat", ls_provincia)
				end if	
			else
				fdw_oggetto.setitem(fl_num_riga, "rag_soc_1", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "rag_soc_2", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "indirizzo", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "frazione", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "cap", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "localita", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "provincia", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "rag_soc_1_dest_fat", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "rag_soc_2_dest_fat", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "indirizzo_dest_fat", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "frazione_dest_fat", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "cap_dest_fat", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "localita_dest_fat", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "provincia_dest_fat", ls_null)
			end if
		else

			select count(*)
			into   :li_count
			from   anag_des_clienti
			where  anag_des_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_des_clienti.cod_cliente = :fs_cod_clien_for and
					 anag_des_clienti.flag_dest_merce_fat = :ls_flag_dest_merce_fat and
					 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_oggi ));
			
			if sqlca.sqlcode = -1 then
				fs_messaggio = sqlca.sqlerrtext
				return -1
			end if

			if li_count = 0 then
				fdw_oggetto.setitem(fdw_oggetto.getrow(), "cod_des_cliente", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "rag_soc_1", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "rag_soc_2", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "indirizzo", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "frazione", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "cap", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "localita", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "provincia", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "rag_soc_1_dest_fat", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "rag_soc_2_dest_fat", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "indirizzo_dest_fat", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "frazione_dest_fat", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "cap_dest_fat", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "localita_dest_fat", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "provincia_dest_fat", ls_null)
				
			elseif li_count = 1 then				
				select cod_des_cliente
				into   :ls_cod_des_cliente
				from   anag_des_clienti
				where  anag_des_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
						 anag_des_clienti.cod_cliente = :fs_cod_clien_for and
						 anag_des_clienti.flag_dest_merce_fat = :ls_flag_dest_merce_fat and
						 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_oggi ));
			
				if sqlca.sqlcode = -1 then
					fs_messaggio = sqlca.sqlerrtext
					return -1
				end if
				fdw_oggetto.setitem(fdw_oggetto.getrow(), "cod_des_cliente", ls_cod_des_cliente)
				select anag_des_clienti.rag_soc_1,
						 anag_des_clienti.rag_soc_2,
						 anag_des_clienti.indirizzo,
						 anag_des_clienti.frazione,
						 anag_des_clienti.cap,
						 anag_des_clienti.localita,
						 anag_des_clienti.provincia
				into   :ls_rag_soc_1,    
						 :ls_rag_soc_2,
						 :ls_indirizzo,    
						 :ls_frazione,
						 :ls_cap,
						 :ls_localita,
						 :ls_provincia
				from   anag_des_clienti
				where  anag_des_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
						 anag_des_clienti.cod_cliente = :fs_cod_clien_for and
						 anag_des_clienti.cod_des_cliente = :ls_cod_des_cliente and
						 anag_des_clienti.flag_dest_merce_fat = :ls_flag_dest_merce_fat and						 
						 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_oggi ));
				if sqlca.sqlcode = 0 then
					if fs_flag_tipo_fat_ven = "I" then
						fdw_oggetto.setitem(fl_num_riga, "rag_soc_1", ls_rag_soc_1)
						fdw_oggetto.setitem(fl_num_riga, "rag_soc_2", ls_rag_soc_2)
						fdw_oggetto.setitem(fl_num_riga, "indirizzo", ls_indirizzo)
						fdw_oggetto.setitem(fl_num_riga, "frazione", ls_frazione)
						fdw_oggetto.setitem(fl_num_riga, "cap", ls_cap)
						fdw_oggetto.setitem(fl_num_riga, "localita", ls_localita)
						fdw_oggetto.setitem(fl_num_riga, "provincia", ls_provincia)
					else
						fdw_oggetto.setitem(fl_num_riga, "rag_soc_1_dest_fat", ls_rag_soc_1)
						fdw_oggetto.setitem(fl_num_riga, "rag_soc_2_dest_fat", ls_rag_soc_2)
						fdw_oggetto.setitem(fl_num_riga, "indirizzo_dest_fat", ls_indirizzo)
						fdw_oggetto.setitem(fl_num_riga, "frazione_dest_fat", ls_frazione)
						fdw_oggetto.setitem(fl_num_riga, "cap_dest_fat", ls_cap)
						fdw_oggetto.setitem(fl_num_riga, "localita_dest_fat", ls_localita)
						fdw_oggetto.setitem(fl_num_riga, "provincia_dest_fat", ls_provincia)
					end if	
				else
					fdw_oggetto.setitem(fl_num_riga, "rag_soc_1", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "rag_soc_2", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "indirizzo", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "frazione", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "cap", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "localita", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "provincia", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "rag_soc_1_dest_fat", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "rag_soc_2_dest_fat", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "indirizzo_dest_fat", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "frazione_dest_fat", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "cap_dest_fat", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "localita_dest_fat", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "provincia_dest_fat", ls_null)					
				end if
///////////////////////////////////////////////////////////////////////////////////////////				
			elseif li_count > 1 then				
				
				s_cs_xx.parametri.parametro_s_10 = fs_cod_clien_for
				s_cs_xx.parametri.parametro_s_11 = ls_flag_dest_merce_fat
				s_cs_xx.parametri.parametro_s_12 = fs_flag_tipo_fat_ven
				window_open(w_lista_dest_cliente, 0)		
				if s_cs_xx.parametri.parametro_s_10 <> "" and not isnull(s_cs_xx.parametri.parametro_s_10) then
					fdw_oggetto.setitem(fdw_oggetto.getrow(), "cod_des_cliente", s_cs_xx.parametri.parametro_s_10)
					select anag_des_clienti.rag_soc_1,
							 anag_des_clienti.rag_soc_2,
							 anag_des_clienti.indirizzo,
							 anag_des_clienti.frazione,
							 anag_des_clienti.cap,
							 anag_des_clienti.localita,
							 anag_des_clienti.provincia
					into   :ls_rag_soc_1,    
							 :ls_rag_soc_2,
							 :ls_indirizzo,    
							 :ls_frazione,
							 :ls_cap,
							 :ls_localita,
							 :ls_provincia
					from   anag_des_clienti
					where  anag_des_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
							 anag_des_clienti.cod_cliente = :fs_cod_clien_for and
							 anag_des_clienti.cod_des_cliente = :s_cs_xx.parametri.parametro_s_10 and
							 anag_des_clienti.flag_dest_merce_fat = :ls_flag_dest_merce_fat and							 
							 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_oggi ));
					if sqlca.sqlcode = 0 then
						if fs_flag_tipo_fat_ven = "I" then
							fdw_oggetto.setitem(fl_num_riga, "rag_soc_1", ls_rag_soc_1)
							fdw_oggetto.setitem(fl_num_riga, "rag_soc_2", ls_rag_soc_2)
							fdw_oggetto.setitem(fl_num_riga, "indirizzo", ls_indirizzo)
							fdw_oggetto.setitem(fl_num_riga, "frazione", ls_frazione)
							fdw_oggetto.setitem(fl_num_riga, "cap", ls_cap)
							fdw_oggetto.setitem(fl_num_riga, "localita", ls_localita)
							fdw_oggetto.setitem(fl_num_riga, "provincia", ls_provincia)
						else 	
							fdw_oggetto.setitem(fl_num_riga, "rag_soc_1_dest_fat", ls_rag_soc_1)
							fdw_oggetto.setitem(fl_num_riga, "rag_soc_2_dest_fat", ls_rag_soc_2)
							fdw_oggetto.setitem(fl_num_riga, "indirizzo_dest_fat", ls_indirizzo)
							fdw_oggetto.setitem(fl_num_riga, "frazione_dest_fat", ls_frazione)
							fdw_oggetto.setitem(fl_num_riga, "cap_dest_fat", ls_cap)
							fdw_oggetto.setitem(fl_num_riga, "localita_dest_fat", ls_localita)
							fdw_oggetto.setitem(fl_num_riga, "provincia_dest_fat", ls_provincia)
						end if	
					else
						fdw_oggetto.setitem(fl_num_riga, "rag_soc_1", ls_null)
						fdw_oggetto.setitem(fl_num_riga, "rag_soc_2", ls_null)
						fdw_oggetto.setitem(fl_num_riga, "indirizzo", ls_null)
						fdw_oggetto.setitem(fl_num_riga, "frazione", ls_null)
						fdw_oggetto.setitem(fl_num_riga, "cap", ls_null)
						fdw_oggetto.setitem(fl_num_riga, "localita", ls_null)
						fdw_oggetto.setitem(fl_num_riga, "provincia", ls_null)
						fdw_oggetto.setitem(fl_num_riga, "rag_soc_1_dest_fat", ls_null)
						fdw_oggetto.setitem(fl_num_riga, "rag_soc_2_dest_fat", ls_null)
						fdw_oggetto.setitem(fl_num_riga, "indirizzo_dest_fat", ls_null)
						fdw_oggetto.setitem(fl_num_riga, "frazione_dest_fat", ls_null)
						fdw_oggetto.setitem(fl_num_riga, "cap_dest_fat", ls_null)
						fdw_oggetto.setitem(fl_num_riga, "localita_dest_fat", ls_null)
						fdw_oggetto.setitem(fl_num_riga, "provincia_dest_fat", ls_null)						
					end if
					setnull(s_cs_xx.parametri.parametro_s_10)
					setnull(s_cs_xx.parametri.parametro_s_11)
					setnull(s_cs_xx.parametri.parametro_s_12)
				end if
			end if									
		end if
	case "F"
		if not isnull(fs_cod_destinazione) then
			select anag_des_fornitori.rag_soc_1,
					 anag_des_fornitori.rag_soc_2,
					 anag_des_fornitori.indirizzo,
					 anag_des_fornitori.frazione,
					 anag_des_fornitori.cap,
					 anag_des_fornitori.localita,
					 anag_des_fornitori.provincia
			into   :ls_rag_soc_1,    
					 :ls_rag_soc_2,
					 :ls_indirizzo,    
					 :ls_frazione,
					 :ls_cap,
					 :ls_localita,
					 :ls_provincia
			from   anag_des_fornitori
			where  anag_des_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_des_fornitori.cod_fornitore = :fs_cod_clien_for and
					 anag_des_fornitori.cod_des_fornitore = :fs_cod_destinazione and
					 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_oggi ));
			if sqlca.sqlcode = 0 then
				fdw_oggetto.setitem(fl_num_riga, "rag_soc_1", ls_rag_soc_1)
				fdw_oggetto.setitem(fl_num_riga, "rag_soc_2", ls_rag_soc_2)
				fdw_oggetto.setitem(fl_num_riga, "indirizzo", ls_indirizzo)
				fdw_oggetto.setitem(fl_num_riga, "frazione", ls_frazione)
				fdw_oggetto.setitem(fl_num_riga, "cap", ls_cap)
				fdw_oggetto.setitem(fl_num_riga, "localita", ls_localita)
				fdw_oggetto.setitem(fl_num_riga, "provincia", ls_provincia)
			else
				fdw_oggetto.setitem(fl_num_riga, "rag_soc_1", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "rag_soc_2", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "indirizzo", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "frazione", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "cap", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "localita", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "provincia", ls_null)
			end if
		else	
			select count(*)
			into   :li_count
			from   anag_des_fornitori
			where  anag_des_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_des_fornitori.cod_fornitore = :fs_cod_clien_for and
					 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_oggi ));
			
			if sqlca.sqlcode = -1 then
				fs_messaggio = sqlca.sqlerrtext
				return -1
			end if
			
			if li_count = 0 then
				fdw_oggetto.setitem(fl_num_riga, "cod_des_fornitore", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "rag_soc_1", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "rag_soc_2", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "indirizzo", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "frazione", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "cap", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "localita", ls_null)
				fdw_oggetto.setitem(fl_num_riga, "provincia", ls_null)
			elseif li_count = 1 then
				select cod_des_fornitore
				into   :fs_cod_destinazione
				from   anag_des_fornitori
				where  anag_des_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
						 anag_des_fornitori.cod_fornitore = :fs_cod_clien_for and
						 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_oggi ));
			
				if sqlca.sqlcode = -1 then
					fs_messaggio = sqlca.sqlerrtext
					return -1
				end if
				fdw_oggetto.setitem(fl_num_riga, "cod_des_fornitore", fs_cod_destinazione)
				select anag_des_fornitori.rag_soc_1,
						 anag_des_fornitori.rag_soc_2,
						 anag_des_fornitori.indirizzo,
						 anag_des_fornitori.frazione,
						 anag_des_fornitori.cap,
						 anag_des_fornitori.localita,
						 anag_des_fornitori.provincia
				into   :ls_rag_soc_1,    
						 :ls_rag_soc_2,
						 :ls_indirizzo,    
						 :ls_frazione,
						 :ls_cap,
						 :ls_localita,
						 :ls_provincia
				from   anag_des_fornitori
				where  anag_des_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
						 anag_des_fornitori.cod_fornitore = :fs_cod_clien_for and
						 anag_des_fornitori.cod_des_fornitore = :fs_cod_destinazione and
						 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_oggi ));
				if sqlca.sqlcode = 0 then
					fdw_oggetto.setitem(fl_num_riga, "rag_soc_1", ls_rag_soc_1)
					fdw_oggetto.setitem(fl_num_riga, "rag_soc_2", ls_rag_soc_2)
					fdw_oggetto.setitem(fl_num_riga, "indirizzo", ls_indirizzo)
					fdw_oggetto.setitem(fl_num_riga, "frazione", ls_frazione)
					fdw_oggetto.setitem(fl_num_riga, "cap", ls_cap)
					fdw_oggetto.setitem(fl_num_riga, "localita", ls_localita)
					fdw_oggetto.setitem(fl_num_riga, "provincia", ls_provincia)
				else
					fdw_oggetto.setitem(fl_num_riga, "rag_soc_1", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "rag_soc_2", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "indirizzo", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "frazione", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "cap", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "localita", ls_null)
					fdw_oggetto.setitem(fl_num_riga, "provincia", ls_null)
				end if
				
			elseif li_count > 1 then
//				s_cs_xx.parametri.parametro_s_10 = fs_cod_clien_for
//				window_open(w_lista_dest_fornitore, 0)
//				if s_cs_xx.parametri.parametro_s_10 <> "" and not isnull(s_cs_xx.parametri.parametro_s_10) then
//					fdw_oggetto.setitem(fl_num_riga, "cod_des_fornitore", s_cs_xx.parametri.parametro_s_10)
//					select anag_des_fornitori.rag_soc_1,
//							 anag_des_fornitori.rag_soc_2,
//							 anag_des_fornitori.indirizzo,
//							 anag_des_fornitori.frazione,
//							 anag_des_fornitori.cap,
//							 anag_des_fornitori.localita,
//							 anag_des_fornitori.provincia
//					into   :ls_rag_soc_1,    
//							 :ls_rag_soc_2,
//							 :ls_indirizzo,    
//							 :ls_frazione,
//							 :ls_cap,
//							 :ls_localita,
//							 :ls_provincia
//					from   anag_des_fornitori
//					where  anag_des_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
//							 anag_des_fornitori.cod_fornitore = :fs_cod_clien_for and
//							 anag_des_fornitori.cod_des_fornitori = :s_cs_xx.parametri.parametro_s_10;
//					if sqlca.sqlcode = 0 then
//						fdw_oggetto.setitem(fl_num_riga, "rag_soc_1", ls_rag_soc_1)
//						fdw_oggetto.setitem(fl_num_riga, "rag_soc_2", ls_rag_soc_2)
//						fdw_oggetto.setitem(fl_num_riga, "indirizzo", ls_indirizzo)
//						fdw_oggetto.setitem(fl_num_riga, "frazione", ls_frazione)
//						fdw_oggetto.setitem(fl_num_riga, "cap", ls_cap)
//						fdw_oggetto.setitem(fl_num_riga, "localita", ls_localita)
//						fdw_oggetto.setitem(fl_num_riga, "provincia", ls_provincia)
//					else
//						fdw_oggetto.setitem(fl_num_riga, "rag_soc_1", ls_null)
//						fdw_oggetto.setitem(fl_num_riga, "rag_soc_2", ls_null)
//						fdw_oggetto.setitem(fl_num_riga, "indirizzo", ls_null)
//						fdw_oggetto.setitem(fl_num_riga, "frazione", ls_null)
//						fdw_oggetto.setitem(fl_num_riga, "cap", ls_null)
//						fdw_oggetto.setitem(fl_num_riga, "localita", ls_null)
//						fdw_oggetto.setitem(fl_num_riga, "provincia", ls_null)
//					end if
//					setnull(s_cs_xx.parametri.parametro_s_10)
//				end if
			end if									
		end if
		
end choose

setnull(s_cs_xx.parametri.parametro_s_10)
setnull(s_cs_xx.parametri.parametro_s_11)
setnull(s_cs_xx.parametri.parametro_s_12)
return 0
end function

public subroutine wf_protezione (string wfs_flag_tipo_fat_ven, long wfl_row);string ls_null
setnull(ls_null)
	if wfs_flag_tipo_fat_ven = "I" then
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_1_dest_fat.Background.Color='12632256'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_1_dest_fat.protect = 1
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_2_dest_fat.Background.Color='12632256'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_2_dest_fat.protect = 1
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("indirizzo_dest_fat.Background.Color='12632256'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.indirizzo_dest_fat.protect = 1
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("frazione_dest_fat.Background.Color='12632256'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.frazione_dest_fat.protect = 1		
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("cap_dest_fat.Background.Color='12632256'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.cap_dest_fat.protect = 1
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("localita_dest_fat.Background.Color='12632256'")		
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.localita_dest_fat.protect = 1
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("provincia_dest_fat.Background.Color='12632256'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.provincia_dest_fat.protect = 1
		
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_1.Background.Color='16777215'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_1.protect = 0
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_2.Background.Color='16777215'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_2.protect = 0
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("indirizzo.Background.Color='16777215'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.indirizzo.protect = 0
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("frazione.Background.Color='16777215'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.frazione.protect = 0
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("cap.Background.Color='16777215'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.cap.protect = 0								
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("localita.Background.Color='16777215'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.localita.protect = 0
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("provincia.Background.Color='16777215'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.provincia.protect = 0
	else 	
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_1.Background.Color='12632256'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_1.protect = 1
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_2.Background.Color='12632256'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_2.protect = 1
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("indirizzo.Background.Color='12632256'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.indirizzo.protect = 1
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("frazione.Background.Color='12632256'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.frazione.protect = 1			
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("cap.Background.Color='12632256'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.cap.protect = 1						
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("localita.Background.Color='12632256'")	
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.localita.protect = 1
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("provincia.Background.Color='12632256'")			
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.provincia.protect = 1
		
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_1_dest_fat.Background.Color='16777215'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_1_dest_fat.protect = 0
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_2_dest_fat.Background.Color='16777215'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_2_dest_fat.protect = 0
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("indirizzo_dest_fat.Background.Color='16777215'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.indirizzo_dest_fat.protect = 0
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("frazione_dest_fat.Background.Color='16777215'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.frazione_dest_fat.protect = 0
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("cap_dest_fat.Background.Color='16777215'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.cap_dest_fat.protect = 0								
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("localita_dest_fat.Background.Color='16777215'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.localita_dest_fat.protect = 0
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("provincia_dest_fat.Background.Color='16777215'")
		w_tes_fat_ven.tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.provincia_dest_fat.protect = 0
	end if	
return
end subroutine

public subroutine wf_protezione (uo_cs_xx_dw fdw_oggetto, string wfs_flag_tipo_fat_ven, long wfl_row);string ls_null
setnull(ls_null)
	if wfs_flag_tipo_fat_ven = "I" then
		fdw_oggetto.Modify("rag_soc_1_dest_fat.Background.Color='12632256'")
		fdw_oggetto.Object.rag_soc_1_dest_fat.protect = 1
		fdw_oggetto.Modify("rag_soc_2_dest_fat.Background.Color='12632256'")
		fdw_oggetto.Object.rag_soc_2_dest_fat.protect = 1
		fdw_oggetto.Modify("indirizzo_dest_fat.Background.Color='12632256'")
		fdw_oggetto.Object.indirizzo_dest_fat.protect = 1
		fdw_oggetto.Modify("frazione_dest_fat.Background.Color='12632256'")
		fdw_oggetto.Object.frazione_dest_fat.protect = 1		
		fdw_oggetto.Modify("cap_dest_fat.Background.Color='12632256'")
		fdw_oggetto.Object.cap_dest_fat.protect = 1
		fdw_oggetto.Modify("localita_dest_fat.Background.Color='12632256'")		
		fdw_oggetto.Object.localita_dest_fat.protect = 1
		fdw_oggetto.Modify("provincia_dest_fat.Background.Color='12632256'")
		fdw_oggetto.Object.provincia_dest_fat.protect = 1
		
		fdw_oggetto.Modify("rag_soc_1.Background.Color='16777215'")
		fdw_oggetto.Object.rag_soc_1.protect = 0
		fdw_oggetto.Modify("rag_soc_2.Background.Color='16777215'")
		fdw_oggetto.Object.rag_soc_2.protect = 0
		fdw_oggetto.Modify("indirizzo.Background.Color='16777215'")
		fdw_oggetto.Object.indirizzo.protect = 0
		fdw_oggetto.Modify("frazione.Background.Color='16777215'")
		fdw_oggetto.Object.frazione.protect = 0
		fdw_oggetto.Modify("cap.Background.Color='16777215'")
		fdw_oggetto.Object.cap.protect = 0								
		fdw_oggetto.Modify("localita.Background.Color='16777215'")
		fdw_oggetto.Object.localita.protect = 0
		fdw_oggetto.Modify("provincia.Background.Color='16777215'")
		fdw_oggetto.Object.provincia.protect = 0
	else 	
		fdw_oggetto.Modify("rag_soc_1.Background.Color='12632256'")
		fdw_oggetto.Object.rag_soc_1.protect = 1
		fdw_oggetto.Modify("rag_soc_2.Background.Color='12632256'")
		fdw_oggetto.Object.rag_soc_2.protect = 1
		fdw_oggetto.Modify("indirizzo.Background.Color='12632256'")
		fdw_oggetto.Object.indirizzo.protect = 1
		fdw_oggetto.Modify("frazione.Background.Color='12632256'")
		fdw_oggetto.Object.frazione.protect = 1			
		fdw_oggetto.Modify("cap.Background.Color='12632256'")
		fdw_oggetto.Object.cap.protect = 1						
		fdw_oggetto.Modify("localita.Background.Color='12632256'")	
		fdw_oggetto.Object.localita.protect = 1
		fdw_oggetto.Modify("provincia.Background.Color='12632256'")			
		fdw_oggetto.Object.provincia.protect = 1
		
		fdw_oggetto.Modify("rag_soc_1_dest_fat.Background.Color='16777215'")
		fdw_oggetto.Object.rag_soc_1_dest_fat.protect = 0
		fdw_oggetto.Modify("rag_soc_2_dest_fat.Background.Color='16777215'")
		fdw_oggetto.Object.rag_soc_2_dest_fat.protect = 0
		fdw_oggetto.Modify("indirizzo_dest_fat.Background.Color='16777215'")
		fdw_oggetto.Object.indirizzo_dest_fat.protect = 0
		fdw_oggetto.Modify("frazione_dest_fat.Background.Color='16777215'")
		fdw_oggetto.Object.frazione_dest_fat.protect = 0
		fdw_oggetto.Modify("cap_dest_fat.Background.Color='16777215'")
		fdw_oggetto.Object.cap_dest_fat.protect = 0								
		fdw_oggetto.Modify("localita_dest_fat.Background.Color='16777215'")
		fdw_oggetto.Object.localita_dest_fat.protect = 0
		fdw_oggetto.Modify("provincia_dest_fat.Background.Color='16777215'")
		fdw_oggetto.Object.provincia_dest_fat.protect = 0
	end if	
return
end subroutine

on uo_cerca_destinazione.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_cerca_destinazione.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

