﻿$PBExportHeader$uo_costo_medio_continuo.sru
forward
global type uo_costo_medio_continuo from nonvisualobject
end type
end forward

global type uo_costo_medio_continuo from nonvisualobject
end type
global uo_costo_medio_continuo uo_costo_medio_continuo

forward prototypes
public function long uof_costo_medio_continuo (string fs_cod_prodotto, datetime fdt_data, ref decimal fd_giacenza_prodotto, ref decimal fd_costo_medio_continuo, ref string fs_stock[], ref decimal fd_giacenza_stock[], ref decimal fd_costo_medio_continuo_stock[], string fs_path, ref string fs_messaggio)
end prototypes

public function long uof_costo_medio_continuo (string fs_cod_prodotto, datetime fdt_data, ref decimal fd_giacenza_prodotto, ref decimal fd_costo_medio_continuo, ref string fs_stock[], ref decimal fd_giacenza_stock[], ref decimal fd_costo_medio_continuo_stock[], string fs_path, ref string fs_messaggio);boolean lb_crea_nuovo_stock, lb_aggiorna_costo_medio,lb_aggiorna_costo_ultimo
string ls_flag_carico_scarico, ls_flag_costo_medio,ls_cod_tipo_movimento_det,ls_cod_tipo_movimento
long ll_riga, ll_i, ll_num_mov,ll_tipo_operazione
dec{4} ld_quan_movimento, ld_val_movimento,ld_giacenza_prec, ld_giacenza, ld_quantita_giorno, ld_valore_giorno,ld_valore_giorno_prec,&
       ld_valore_giacenza, ld_costo_medio
datetime ldt_data_chiusura_annuale,ldt_data_reg_old,ldt_data_registrazione
uo_magazzino luo_magazzino
datastore lds_mov_magazzino,lds_movimenti
//datastore lds_movimenti

// --------------------------- carico parametri necessario all'elaborazione ---------------------------------
select data_chiusura_annuale
into :ldt_data_chiusura_annuale
from con_magazzino
where cod_azienda=:s_cs_xx.cod_azienda;

if isnull(ldt_data_chiusura_annuale ) then ldt_data_chiusura_annuale=datetime(date("01/01/1900"))
if fdt_data < ldt_data_chiusura_annuale then
	fs_messaggio="Non si possono calcolare le quantità per movimenti avvenuti prima dell'ultima chiusura annuale"
	return -1
end if

// --------------------------- istanzio gli oggetti necessari ----------------------------------------------
luo_magazzino = CREATE uo_magazzino

lds_movimenti = CREATE datastore
lds_movimenti.DataObject = 'd_ds_costo_medio_continuo'

lds_mov_magazzino= create datastore
lds_mov_magazzino.DataObject = 'd_ds_saldo_mag_2'
lds_mov_magazzino.SetTransObject (sqlca)


// ---------------------------- leggo movimenti del periodo   ----------------------------------------------
ll_num_mov = lds_mov_magazzino.retrieve(s_cs_xx.cod_azienda, fs_cod_prodotto, fdt_data, ldt_data_chiusura_annuale)
lds_mov_magazzino.setsort("data_registrazione A")
lds_mov_magazzino.sort()

// --------------------------  valuto il singolo movimento se di carico o scarico -----------------------------------
for ll_i = 1 to ll_num_mov
	
	ls_cod_tipo_movimento_det = lds_mov_magazzino.getitemstring(ll_i,"cod_tipo_mov_det")
	ls_cod_tipo_movimento = lds_mov_magazzino.getitemstring(ll_i,"cod_tipo_movimento")
	ld_quan_movimento = lds_mov_magazzino.getitemnumber(ll_i,"quan_movimento")
	ld_val_movimento = lds_mov_magazzino.getitemnumber(ll_i,"val_movimento")
	if luo_magazzino.uof_tipo_movimento(ls_cod_tipo_movimento_det, ll_tipo_operazione, lb_crea_nuovo_stock, lb_aggiorna_costo_medio, lb_aggiorna_costo_ultimo) = -1 then
		destroy luo_magazzino
		destroy lds_mov_magazzino
		destroy lds_movimenti
		return -1
	end if
	
	choose case ll_tipo_operazione
		case 1, 5, 4, 8, 13					// aggiorno in incremento giacenza e costo medio
			ls_flag_carico_scarico = "+"
		case 2, 3, 6, 7, 14
			ls_flag_carico_scarico = "-"
		case 9, 10, 17, 18			// incremento del solo valore giacenza (aumento costo medio ) - ( rettifica )
//			ls_flag_carico_scarico = "+"
			ls_flag_carico_scarico = "="
		case 15, 16, 11, 12			// decremento del solo valore giacenza (diminuzione costo medio ) - ( rettifica )
//			ls_flag_carico_scarico = "-"
			ls_flag_carico_scarico = "="
	end choose
	
	select det_tipi_movimenti.flag_costo_medio  
	into   :ls_flag_costo_medio  
	from   det_tipi_movimenti  
	where  cod_azienda = :s_cs_xx.cod_azienda AND  
			 cod_tipo_movimento = :ls_cod_tipo_movimento AND  
			 cod_tipo_mov_det = :ls_cod_tipo_movimento_det ;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in ricerca tipo movimento di dettaglio " + ls_cod_tipo_movimento_det + "~r~n" + sqlca.sqlerrtext
		destroy luo_magazzino
		destroy lds_mov_magazzino
		destroy lds_movimenti
		return -1
	end if
	
	if isnull(ls_flag_costo_medio)  then ls_flag_costo_medio  = "N"
	
//	ls_flag_costo_medio = "N"
//	if lb_aggiorna_costo_medio then     // aggiorno costo medio giacenza stock
//		ls_flag_costo_medio = "S"
//	end if
	
	ll_riga = lds_movimenti.insertrow(0)
	
	lds_movimenti.setitem(ll_riga, "data_registrazione", lds_mov_magazzino.getitemdatetime(ll_i,"data_registrazione") )
	lds_movimenti.setitem(ll_riga, "anno_registrazione", lds_mov_magazzino.getitemnumber(ll_i,"anno_registrazione") )
	lds_movimenti.setitem(ll_riga, "num_registrazione", lds_mov_magazzino.getitemnumber(ll_i,"num_registrazione") )
	lds_movimenti.setitem(ll_riga, "cod_tipo_movimento", ls_cod_tipo_movimento)
	lds_movimenti.setitem(ll_riga, "cod_tipo_mov_det", ls_cod_tipo_movimento_det)
	lds_movimenti.setitem(ll_riga, "flag_carico_scarico", ls_flag_carico_scarico)
	lds_movimenti.setitem(ll_riga, "flag_costo_medio", ls_flag_costo_medio)
	lds_movimenti.setitem(ll_riga, "quan_movimento", ld_quan_movimento)
	lds_movimenti.setitem(ll_riga, "val_movimento", ld_val_movimento)
	lds_movimenti.setitem(ll_riga, "giacenza", 0)
	lds_movimenti.setitem(ll_riga, "costo_medio_continuo", 0)
next

// ----------------------  finito il caricamento del DS, sorto le righe in modo che nella stessa data -------------------------
//									compiaiano prima gli scarichi e poi i carichi

lds_movimenti.setsort("data_registrazione A, flag_carico_scarico D, anno_registrazione A, num_registrazione A")
lds_movimenti.sort()
ll_num_mov = lds_movimenti.rowcount()
ld_giacenza_prec = 0
ldt_data_reg_old = datetime(date("01/01/1900"), 00:00:00)
ld_quantita_giorno = 0
ld_valore_giorno = 0
ld_valore_giorno_prec = 0
ld_valore_giacenza = 0
ld_costo_medio = 0

for ll_i = 1 to ll_num_mov
   ld_quan_movimento = lds_movimenti.getitemnumber(ll_i, "quan_movimento")
   ld_val_movimento = lds_movimenti.getitemnumber(ll_i, "val_movimento")
   ldt_data_registrazione = lds_movimenti.getitemdatetime(ll_i, "data_registrazione")
	if lds_movimenti.getitemstring(ll_i,"flag_costo_medio") = "S" then
		if ldt_data_reg_old <> ldt_data_registrazione then
			ldt_data_reg_old = ldt_data_registrazione
			ld_quantita_giorno = ld_quan_movimento
			ld_valore_giorno   = ld_quan_movimento * ld_val_movimento
			if ll_i > 1 then
				ld_valore_giorno_prec = ld_giacenza * ld_costo_medio
			end if
		elseif ll_i > 1 then
			ld_quantita_giorno = ld_quantita_giorno + ld_quan_movimento
			ld_valore_giorno   =  ld_valore_giorno + (ld_quan_movimento * ld_val_movimento)
		end if
	end if
	
	choose case lds_movimenti.getitemstring(ll_i,"flag_carico_scarico")
		case "+"
			ld_giacenza = ld_giacenza_prec + ld_quan_movimento
		case "-"
			ld_giacenza = ld_giacenza_prec - ld_quan_movimento
		case "="
			// modifica solo valore; non faccio nulla alla giacenza.
	end choose
	
	ld_giacenza_prec = ld_giacenza
	if lds_movimenti.getitemstring(ll_i,"flag_costo_medio") = "S" then
		if ld_giacenza = 0 then
			ld_costo_medio = 0 
		else
			ld_costo_medio = (ld_valore_giorno_prec + (ld_valore_giorno) ) / ld_giacenza
		end if
	end if
	lds_movimenti.setitem(ll_i, "giacenza", ld_giacenza)
	lds_movimenti.setitem(ll_i, "costo_medio_continuo", ld_costo_medio)
next

fd_giacenza_prodotto = ld_giacenza
fd_costo_medio_continuo = ld_costo_medio

if len(fs_path) > 0 and not isnull(fs_path) then 
	lds_movimenti.SaveAsAscii(fs_path + fs_cod_prodotto + ".txt")
end if

destroy luo_magazzino
destroy lds_mov_magazzino
destroy lds_movimenti
return 0
end function

on uo_costo_medio_continuo.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_costo_medio_continuo.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

