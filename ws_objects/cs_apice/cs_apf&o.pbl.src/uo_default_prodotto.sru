﻿$PBExportHeader$uo_default_prodotto.sru
$PBExportComments$User Object Ricerca valori default per versione e gen_commessa
forward
global type uo_default_prodotto from nonvisualobject
end type
end forward

global type uo_default_prodotto from nonvisualobject
end type
global uo_default_prodotto uo_default_prodotto

type variables
string is_cod_versione
string is_flag_gen_commessa, is_cod_tipo_politica_riordino, &
         is_flag_tipo_riordino, is_messaggio

end variables

forward prototypes
public function integer uof_flag_gen_commessa (string fs_cod_prodotto)
public function integer uof_ricerca_versione (string fs_cod_prodotto)
end prototypes

public function integer uof_flag_gen_commessa (string fs_cod_prodotto);choose case uof_ricerca_versione(fs_cod_prodotto)
	case 0
		select cod_tipo_politica_riordino
		into   :is_cod_tipo_politica_riordino
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :fs_cod_prodotto;
		if sqlca.sqlcode = 0 then
			if not isnull(is_cod_tipo_politica_riordino) then
				select flag_tipo_riordino
				into   :is_flag_tipo_riordino
				from   tab_tipi_politiche_riordino
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_politica_riordino = :is_cod_tipo_politica_riordino;
				if sqlca.sqlcode = 0 and not isnull(is_flag_tipo_riordino) then
					choose case is_flag_tipo_riordino
						case "C"
							is_flag_gen_commessa = "S"
						case "A", "T"
							is_flag_gen_commessa = "N"
						case else
							is_flag_gen_commessa = "N"
					end choose					
				end if			
			end if
		end if
	case 1
		return 0
	case -1
		return -1
end choose
return 0

end function

public function integer uof_ricerca_versione (string fs_cod_prodotto);select cod_versione
into   :is_cod_versione
from   distinta_padri
where  cod_azienda = :s_cs_xx.cod_azienda
and    cod_prodotto = :fs_cod_prodotto
and    flag_predefinita = 'S';
if sqlca.sqlcode = 0 then
	is_flag_gen_commessa = "S"
elseif sqlca.sqlcode = 100 then
	is_flag_gen_commessa = "N"
	return 1
else
	is_messaggio = "Errore in ricerca versione in distinta padri " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

on uo_default_prodotto.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_default_prodotto.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;setnull(is_cod_versione)
end event

