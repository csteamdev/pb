﻿$PBExportHeader$uo_fido_cliente.sru
forward
global type uo_fido_cliente from nonvisualobject
end type
end forward

global type uo_fido_cliente from nonvisualobject
end type
global uo_fido_cliente uo_fido_cliente

type variables
dec{4}	id_val_check_aggiuntivo = 0
dec{4}	id_importo = 0
string		is_flag_autorizza_sblocco = "S"

private:
	boolean		ib_msg_check_cliente = true
	string			is_msg_report_giri = ""
	boolean		ib_log = false

end variables

forward prototypes
public function integer uof_fido_cliente (string as_cod_cliente, ref decimal ad_fido, ref string as_fuori_fido, ref string as_messaggio)
public function integer uof_check_cliente (string as_cod_cliente, datetime adt_data_rif, ref string as_messaggio)
public function integer uof_esposizione_cliente_ivato (string as_cod_cliente, datetime adt_data_rif, ref s_esposizione_cliente as_esposizione, ref string as_messaggio)
public subroutine uof_set_msg_ckeck_cliente (boolean ab_value)
public function string uof_get_msg_fido ()
public function integer uof_get_blocco_cliente (string as_cod_cliente, datetime adt_data_rif, string as_cod_parametro_blocco, ref string as_msg)
public function integer uof_get_blocco_cliente_loop (string as_cod_cliente, datetime adt_data_rif, string as_cod_parametro_blocco, ref string as_clienti_elaborati[], ref string as_clienti_bloccati[], ref string as_clienti_warning[], ref boolean ab_salta, ref string as_messaggio)
public function integer uof_check_cliente_con_blocco (string as_cod_cliente, string as_clienti_elaborati[], string as_clienti_bloccati[], string as_clienti_warning[])
public subroutine uof_scrivi_log_sistema (string as_flag_log, string as_valore)
public function integer uof_get_blocco_fornitore (string as_cod_fornitore, datetime adt_data_rif, string as_cod_parametro_blocco, ref string as_msg)
public function integer uof_get_blocco_prodotto (string as_cod_prodotto, datetime adt_data_rif, string as_cod_parametro_blocco, ref string as_msg)
end prototypes

public function integer uof_fido_cliente (string as_cod_cliente, ref decimal ad_fido, ref string as_fuori_fido, ref string as_messaggio);/*
	Funzione che dato un cliente (AS_COD_CLIENTE) legge dall'anagrafica il massimo fido concesso (AD_FIDO) e 
	l'abilitazione al fuori fido (AS_FUORI_FIDO).
	Valori di ritorno:
		 0, verifica completata con successo
		-1, errore della procedura (specificato in AS_MESSAGGIO)
*/

//Verifica parametri ricevuti
if isnull(as_cod_cliente) then
	as_messaggio = "Specificare il cliente"
	return -1
end if

//Lettura dati cliente dall'anagrafica
select
	fido,
	flag_fuori_fido
into
	:ad_fido,
	:as_fuori_fido
from
	anag_clienti
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cliente = :as_cod_cliente;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura fido cliente: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	ad_fido = 0
	as_fuori_fido = "N"
end if

//Validazione dei valori ottenuti
if isnull(ad_fido) then
	ad_fido = 0
end if

if isnull(as_fuori_fido) then
	as_fuori_fido = "N"
end if

return 0
end function

public function integer uof_check_cliente (string as_cod_cliente, datetime adt_data_rif, ref string as_messaggio);/*
	Funzione che dato un cliente (AS_COD_CLIENTE) verifica se alla data indicata (ADT_DATA_RIF) risulta aver superato il fido
	massimo indicato nella propria anagrafica. Qualora il fido in anagrafica sia impsotato (> zero) e l'esposizione del cliente
	risulti essere superiore, viene visualizzata una messagebox di avviso.
	Valori di ritorno:
		 0, verifica completata con successo
		-1, errore della procedura (specificato in AS_MESSAGGIO)
*/

string	ls_fuori_fido, ls_rag_soc
boolean lb_fuori_fido = false
boolean lb_scaduto = false
dec{4}	ld_fido, ld_esposizione, ld_saldo_contabile, ld_fat_ven, ld_bol_ven, ld_ord_ven_ass, ld_ord_ven_spe, ld_ord_ven_a_p
string ls_FBF, ls_FBS
s_esposizione_cliente lstr_esposizione

//Verifica parametri ricevuti
if isnull(as_cod_cliente) or isnull(adt_data_rif) then
	as_messaggio = "Specificare cliente e data di riferimento"
	return -1
end if

//Lettura fido cliente ed abilitazione al fuori fido
if uof_fido_cliente(as_cod_cliente,ld_fido,ls_fuori_fido,as_messaggio) <> 0 then
	as_messaggio = "Errore in lettura fido cliente:~n" + as_messaggio
	return -1
end if

//Donato 05-11-2008 modifica per specifica PTENDA: gestione fidi
//Lettura dettagli esposizione cliente
if uof_esposizione_cliente_ivato(as_cod_cliente,adt_data_rif,ref lstr_esposizione, ref as_messaggio) <> 0 then
	return -1
end if
	
//Calcolo esposizione totale
//ld_esposizione = ld_saldo_contabile + ld_fat_ven + ld_bol_ven + ld_ord_ven_ass + ld_ord_ven_spe + ld_ord_ven_a_p + id_importo
ld_esposizione = 	lstr_esposizione.saldo_contabile + &
						lstr_esposizione.fatture_non_contabilizzate + &
						lstr_esposizione.ddt_non_fatturati + &
						lstr_esposizione.ordini_assegnati + lstr_esposizione.ordini_non_spediti + lstr_esposizione.ordini_aperti


as_messaggio = ""
is_msg_report_giri = ""

if ld_fido > 0 then
	//solo in questo caso fai i controlli
	
	select rag_soc_1
	into :ls_rag_soc
	from anag_clienti
	where cod_azienda = :s_cs_xx.cod_azienda and cod_cliente = :as_cod_cliente;	
	if sqlca.sqlcode <> 0 then
		as_messaggio = "Errore in lettura dati cliente: " + sqlca.sqlerrtext
		return -1
	end if
	
	if  ld_esposizione > ld_fido then
		//fido superato
		as_messaggio = "L'esposizione attuale del cliente:~n   " + as_cod_cliente + " - " + ls_rag_soc + "~nè pari a:~n   " + &
							string(ld_esposizione,"###,###,###,##0.00") + " € IVA INCLUSA~n" + &
							"Il fido massimo consentito è pari a:~n   " + string(ld_fido,"###,###,###,##0.00") + " €"
	
		if ls_fuori_fido = "S" then
			as_messaggio += "~nAl cliente è concesso il fuori fido"
		else
			as_messaggio += "~nAl cliente non è concesso il fuori fido"
		end if
		lb_fuori_fido = true
	end if
	
	if id_val_check_aggiuntivo > 0 then
		//scaduto maggiore di zero
		as_messaggio += "~n~nPer il cliente:~n   " + as_cod_cliente + " - " + ls_rag_soc + "~n risulta un importo complessivo pari a:~n   " + &
							string(id_val_check_aggiuntivo,"###,###,###,##0.00") + " € di scaduto non pagato."
		lb_scaduto = true
	end if
	
	//dare eventuali msg se presenti
	if lb_fuori_fido or lb_scaduto then
		//prima di tutto leggi i parametri aziendali FBF e FBS cosa dicono
		select flag
		into :ls_FBF
		from parametri_azienda
		where cod_azienda = :s_cs_xx.cod_azienda
			and cod_parametro = 'FBF' and flag_parametro = 'F';			
		if sqlca.sqlcode = 0 then
		else
			ls_FBF = "N" //cioè non deve bloccare
		end if
		
		select flag
		into :ls_FBS
		from parametri_azienda
		where cod_azienda = :s_cs_xx.cod_azienda
			and cod_parametro = 'FBS' and flag_parametro = 'F';			
		if sqlca.sqlcode = 0 then
		else
			ls_FBS = "N" //cioè non deve bloccare
		end if				
		
		if (lb_fuori_fido and ls_FBF = "S" and is_flag_autorizza_sblocco="N") or &
				(lb_scaduto and ls_FBS = "S" and is_flag_autorizza_sblocco="N") then
			//il msg deve essere bloccante
			
			as_messaggio = "Impossibile proseguire!"+ "~n" + as_messaggio
			
			//salvo qui dentro il messaggio
			is_msg_report_giri += as_messaggio
			
			if ib_msg_check_cliente then g_mb.messagebox("APICE",as_messaggio,exclamation!)
			
			setnull(as_messaggio)
			return 2
		else
			//il msg deve NON DEVE essere bloccante
			as_messaggio = "Avviso!"+ "~n" + as_messaggio
			
			//salvo qui dentro il messaggio
			is_msg_report_giri += as_messaggio
			
			if ib_msg_check_cliente then g_mb.messagebox("APICE",as_messaggio,exclamation!)
			
			setnull(as_messaggio)
			return 0
		end if
		
	else
		//tutto a posto
		setnull(as_messaggio)
		is_msg_report_giri = ""
		return 0
	end if
else
	//non fare nessun controllo: tutto a posto!
	setnull(as_messaggio)
	is_msg_report_giri = ""
	return 0
end if
//---------------------------------------------------------------------
end function

public function integer uof_esposizione_cliente_ivato (string as_cod_cliente, datetime adt_data_rif, ref s_esposizione_cliente as_esposizione, ref string as_messaggio);/*
	Funzione che dato un cliente (AS_COD_CLIENTE) e una data di riferimento (ADT_DATA_RIF) calcola i subtotali dell'esposizione
	del cliente leggendo i dati dalla contabilità (se il collegamento è abilitato) e dai documenti di vendita.
	Valori di ritorno:
		 0, verifica completata con successo
		-1, errore della procedura (specificato in AS_MESSAGGIO)


	
	--->>>>>  A T T E N Z I O N E  !!!!      , i valori ritornato in questo caso sono valori IVA INCLUSA.
	
	
	--->>>>>  A T T E N Z I O N E  !!!! QUALSIASI MODIFICA VENGA FATTA A QUESTA FUNZIONE DEVE ESSERE ANCHE RIPORTATA NELLA UOF_ESPOSIZIONE_CLIENTE che esegue il calcolo senza iva.
		
*/


//Verifica parametri ricevuti
if isnull(as_cod_cliente) or isnull(adt_data_rif) then
	as_messaggio = "Specificare cliente e data di riferimento"
	return -1
end if

//nuove variabili per nuova gestione fidi clienti
string 	ls_cod_impresa, ls_cod_capoconto
long ll_gg_gos, ll_gg_riba,ll_anno
dec{4} ld_valore, ad_tot_ord_ven_a_p_ivato, ad_tot_ord_ven_a_p_esente, ld_dare,ld_avere,ld_valore_totale, ld_valore_manageriali, ld_tot_nc_non_contabilizzate
datetime ldt_data_gg_gos, ldt_oggi_toll_riba
uo_impresa luo_impresa	

id_val_check_aggiuntivo = 0 //qui andrà eventualmente lo scaduto aperto + le riba insolute

//Lettura saldo contabile da IMPRESA (AD_SALDO_CONTABILE)
if s_cs_xx.parametri.impresa and isvalid(sqlci) then
	ll_anno = year(date(adt_data_rif))
	
	select
		cod_capoconto
	into
		:ls_cod_capoconto
	from
		anag_clienti
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		cod_cliente = :as_cod_cliente;
	
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in lettura codice capoconto cliente: " + sqlca.sqlerrtext
		return -1
	elseif sqlca.sqlcode = 100 then
		as_messaggio = "Errore in lettura codice capoconto cliente: cliente non trovato"
		return -1
	end if
	
	luo_impresa = create uo_impresa
	
	ls_cod_impresa = luo_impresa.uof_codice_cli_for("C",ls_cod_capoconto, as_cod_cliente)
	
	destroy luo_impresa
	
	//Donato 05-11-2008 modifica per specifica PTENDA gestione fidi
	//nuovo codice
	
	//lettura parametro RIB
	//rappresenta il n° di giorni oltre i quali le RIBA vanno considerate scadute
	select numero
	into :ll_gg_riba
	from parametri_azienda
	where cod_azienda = :s_cs_xx.cod_azienda
		and cod_parametro = 'RIB' and flag_parametro = 'N';
		
	if sqlca.sqlcode <> 0 then
		ll_gg_riba = -1
	end if

	as_esposizione.saldo_contabile = 0
	
	//inizio gestione dati IMPRESA
	//scadenze aperte impresa ------------------------

	
	// eseguo select per totale civili e manageriali
	ld_valore_totale = 0
	
	//select mov_Scadenza.dare, mov_Scadenza.avere, scadenza.num_documento, scadenza.data_scadenza, mov_scadenza.id_mov_contabile, reg_pd.data_registrazione
//	select sum(isnull(mov_Scadenza.dare,0) ) as dare, sum( isnull(mov_Scadenza.avere,0) ) as avere, dare - avere
	select sum(isnull(mov_scadenza.dare,0) ) , sum( isnull(mov_scadenza.avere,0) ), sum(isnull(mov_scadenza.dare,0) ) - sum( isnull(mov_scadenza.avere,0) ) 
	into    :ld_dare, :ld_avere, :ld_valore_totale
		from mov_scadenza
			join scadenza
			on mov_scadenza.id_scadenza = scadenza.id_scadenza
			join conto on scadenza.id_conto = conto.id_conto
			left outer join mov_contabile on mov_contabile.id_mov_contabile = mov_scadenza.id_mov_contabile
			left outer join reg_pd on mov_contabile.id_reg_pd = reg_pd.id_reg_pd
			left outer join       dba.tipo_pagamento on dba.tipo_pagamento.id_tipo_pagamento=dba.scadenza.id_tipo_pagamento
		where  conto.codice = :ls_cod_impresa and 	mov_scadenza.data <= :adt_data_rif and (reg_pd.data_registrazione <= :adt_data_rif or reg_pd.data_registrazione is null)
	group by conto.codice
	using sqlci;
	
	if sqlci.sqlcode < 0 then
		as_messaggio = "Errore SQL in calcolo dare, avere, saldo cliente: " + ls_cod_impresa + "~r~n" + sqlci.sqlerrtext
		return -1
	end if	
	
	// INIZIO AD EFFETTUARE I CALCOLI PER LE MANAGERIALI
	ld_valore_manageriali = 0
	
//	select sum(isnull(mov_Scadenza.dare,0) ) as dare, sum( isnull(mov_Scadenza.avere,0) ) as avere, dare - avere
	select sum(isnull(mov_Scadenza.dare,0) ) , sum( isnull(mov_Scadenza.avere,0) ) , sum(isnull(mov_Scadenza.dare,0) ) - sum( isnull(mov_Scadenza.avere,0) ) 
	into    :ld_dare, :ld_avere, :ld_valore_manageriali
	from mov_scadenza
		join scadenza
		on mov_Scadenza.id_scadenza = scadenza.id_Scadenza
		join conto
		on scadenza.id_conto = conto.id_conto
		join mov_contabile
		on mov_scadenza.id_mov_contabile = mov_contabile.id_mov_contabile
		join reg_pd
		on mov_contabile.id_reg_pd = reg_pd.id_reg_pd
		join registro
		on reg_pd.id_registro = registro.id_registro
	where conto .codice =:ls_cod_impresa	
	and registro.tipo_registro = 'MN'	
	group by conto.codice
	using sqlci;
	
	if sqlci.sqlcode < 0 then
		as_messaggio = "Errore SQL in calcolo dare, avere, saldo cliente (Manageriali): " + ls_cod_impresa + "~r~n" + sqlci.sqlerrtext
		return -1
	end if	
	
	
	setnull(ld_valore)
	
	if isnull(ld_valore_totale) then ld_valore_totale = 0
	if isnull(ld_valore_manageriali) then ld_valore_manageriali = 0
	ld_valore = ld_valore_totale
	
	as_esposizione.saldo_contabile = ld_valore
	as_esposizione.saldo_manageriali = ld_valore_manageriali
	
	// EnMe 11-07-2013: CALCOLO IL SALDO DELLE MANAGERIALI CHE HANNO OTTENUTO UNA ULTERIORE DILAZIONE
	// lo capisco dal fatto che la data di registrazione della manageriale è diversa dalla data di scadenza
	
	ld_valore_manageriali = 0
	
//	select sum(isnull(mov_Scadenza.dare,0) ) as dare, sum( isnull(mov_Scadenza.avere,0) ) as avere, dare - avere
	select sum(isnull(mov_Scadenza.dare,0) ), sum( isnull(mov_Scadenza.avere,0) ),  sum(isnull(mov_Scadenza.dare,0) ) - sum( isnull(mov_Scadenza.avere,0) ) 
	into    :ld_dare, :ld_avere, :ld_valore_manageriali
	from mov_scadenza
		join scadenza
		on mov_Scadenza.id_scadenza = scadenza.id_Scadenza
		join conto
		on scadenza.id_conto = conto.id_conto
		join mov_contabile
		on mov_scadenza.id_mov_contabile = mov_contabile.id_mov_contabile
		join reg_pd
		on mov_contabile.id_reg_pd = reg_pd.id_reg_pd
		join registro
		on reg_pd.id_registro = registro.id_registro
	where conto .codice =:ls_cod_impresa	
	and registro.tipo_registro = 'MN'	
	and dateadd(day, 90, scadenza.data_scadenza) < reg_pd.data_registrazione
	group by conto.codice
	using sqlci;
	
//		and dateadd(day, :ll_gg_riba, scadenza.data_scadenza) < reg_pd.data_registrazione

	
	if sqlci.sqlcode < 0 then
		as_messaggio = "Errore SQL in calcolo dare, avere, saldo cliente (Manageriali con dilazione): " + ls_cod_impresa + "~r~n" + sqlci.sqlerrtext
		return -1
	end if	
	
	setnull(ld_valore)
	
	if isnull(ld_valore_totale) then ld_valore_totale = 0
	if isnull(ld_valore_manageriali) then ld_valore_manageriali = 0
	ld_valore = ld_valore_totale
	
	as_esposizione.saldo_manageriali_dilazionate = ld_valore_manageriali
	as_esposizione.saldo_manageriali_normali = as_esposizione.saldo_manageriali - as_esposizione.saldo_manageriali_dilazionate
	
	
	// FINE ELABORAZIONI TOTALI SU MAGAERIALI	
	
	//riba insolute ----------------------------------------
	
	if ll_gg_riba = -1 then
		//nessuna tolleranza per le RIBA
		ldt_oggi_toll_riba = adt_data_rif
	else
		//calcola la data dopo la quale considerare insolute le RIBA
		ldt_oggi_toll_riba = datetime(RelativeDate(date(adt_data_rif), - ll_gg_riba),00:00:00)
	end if		
		
	setnull(ld_valore)
	
	select sum(saldo)
	into :ld_valore
	from dba.scadenza
	join dba.conto on dba.conto.id_conto=dba.scadenza.id_conto
	join dba.tipo_pagamento on dba.tipo_pagamento.id_tipo_pagamento=dba.scadenza.id_tipo_pagamento
	where (dba.scadenza.esito_pagamento='I' or dba.scadenza.data_scadenza < :ldt_oggi_toll_riba)
		and dba.conto.codice=:ls_cod_impresa and dba.tipo_pagamento.codice in ('RIBA','RB')
	using sqlci;
	
	if sqlci.sqlcode < 0 then
		as_messaggio = "Errore in lettura riba insolute cliente da IMPRESA: " + sqlci.sqlerrtext
		return -1
	elseif sqlci.sqlcode = 100 then
		ld_valore = 0
	end if		
	if isnull(ld_valore) then ld_valore = 0
	
	as_esposizione.riba_insolute = ld_valore
	
	id_val_check_aggiuntivo += ld_valore //riba insolute
	
	//scaduto aperto -------------------------------------
	
	setnull(ld_valore)
	
	select sum(saldo)
	into :ld_valore
	from dba.scadenza
	join dba.conto on dba.conto.id_conto=dba.scadenza.id_conto
	join dba.tipo_pagamento on dba.tipo_pagamento.id_tipo_pagamento=dba.scadenza.id_tipo_pagamento
	where dba.scadenza.tipo_scadenza='A' and dba.scadenza.saldo <> 0 and dba.scadenza.data_scadenza < :adt_data_rif
		and dba.conto.codice=:ls_cod_impresa and dba.tipo_pagamento.codice not in ('RIBA','RB')
	using sqlci;
	
	if sqlci.sqlcode < 0 then
		as_messaggio = "Errore in lettura scaduto aperto cliente da IMPRESA: " + sqlci.sqlerrtext
		return -1
	elseif sqlci.sqlcode = 100 then
		ld_valore = 0
	end if			
	if isnull(ld_valore) then ld_valore = 0
	
	//non partecipa al saldo contabile perchè già compreso in scadenze aperte
	//ad_saldo_contabile += ld_valore
	
	id_val_check_aggiuntivo += ld_valore //scaduto aperto
		
	//riba a scadere ------------------------------------------
	setnull(ld_valore)
	
	select sum(isnull(dare,0) - isnull(avere,0))
	into :ld_valore
	from dba.scadenza
	join dba.conto on dba.conto.id_conto=dba.scadenza.id_conto
	join dba.tipo_pagamento on dba.tipo_pagamento.id_tipo_pagamento=dba.scadenza.id_tipo_pagamento
	where dba.scadenza.tipo_scadenza='A'
		and dba.scadenza.data_scadenza >= :ldt_oggi_toll_riba
		and dba.conto.codice=:ls_cod_impresa
		and dba.tipo_pagamento.codice in ('RIBA','RB')
		and dba.scadenza.esito_pagamento<>'I'
	using sqlci;
	
	if sqlci.sqlcode < 0 then
		as_messaggio = "Errore in lettura riba a scadere cliente da IMPRESA: " + sqlci.sqlerrtext
		return -1
	elseif sqlci.sqlcode = 100 then
		ld_valore = 0
	end if			
	if isnull(ld_valore) then ld_valore = 0
	
	as_esposizione.riba_future = ld_valore
	//-----------------------------------------------------------
else
	as_esposizione.saldo_contabile = 0
	as_esposizione.saldo_manageriali = 0
	as_esposizione.scadenze_future = 0
	as_esposizione.riba_future = 0
end if
//fine gestione dati IMPRESA ..........................................................................................
//in "ad_saldo_contabile" c'è la roba che viene da IMPRESA da imputare al fido
//in "id_val_check_aggiuntivo" c'è la roba che, se diversa da zero e qualora non si superasse il fido concesso, andrebbe segnalata


//inizio gestione dati APICE
//Lettura valore totale delle fatture stampate in definitivo ma non ancora contabilizzate (AD_TOT_FAT_VEN)
// sono esclude da questo calcolo le fatture pro-forma

select
	sum(tot_fattura)
into
	:as_esposizione.fatture_non_contabilizzate
from
	tes_fat_ven
	join tab_tipi_fat_ven on tes_fat_ven.cod_azienda = tab_tipi_fat_ven.cod_azienda and tes_fat_ven.cod_tipo_fat_ven = tab_tipi_fat_ven.cod_tipo_fat_ven 
where
	tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and tes_fat_ven.cod_cliente = :as_cod_cliente and
	tes_fat_ven.cod_documento is not null and tes_fat_ven.numeratore_documento is not null and
	tes_fat_ven.anno_documento is not null and tes_fat_ven.num_documento is not null and
	tes_fat_ven.flag_contabilita = 'N'  and
	tes_fat_ven.data_fattura <= :adt_data_rif and
	tab_tipi_fat_ven.flag_tipo_fat_ven not in ('P','F','N') ;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura totale fatture di vendita confermate non contabilizzate: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(as_esposizione.fatture_non_contabilizzate) then
	as_esposizione.fatture_non_contabilizzate = 0
end if

// totalizzo le note di credito
select
	sum(tot_fattura)
into
	:ld_tot_nc_non_contabilizzate
from
	tes_fat_ven
	join tab_tipi_fat_ven on tes_fat_ven.cod_azienda = tab_tipi_fat_ven.cod_azienda and tes_fat_ven.cod_tipo_fat_ven = tab_tipi_fat_ven.cod_tipo_fat_ven 
where
	tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and tes_fat_ven.cod_cliente = :as_cod_cliente and
	tes_fat_ven.cod_documento is not null and tes_fat_ven.numeratore_documento is not null and
	tes_fat_ven.anno_documento is not null and tes_fat_ven.num_documento is not null and
	tes_fat_ven.flag_contabilita = 'N'  and
	tes_fat_ven.data_fattura <= :adt_data_rif and
	tab_tipi_fat_ven.flag_tipo_fat_ven in ('N') ;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura totale fatture di vendita confermate non contabilizzate: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(ld_tot_nc_non_contabilizzate) then
	ld_tot_nc_non_contabilizzate = 0
end if

// detraggo le note di credito dal totale
as_esposizione.fatture_non_contabilizzate = as_esposizione.fatture_non_contabilizzate - ld_tot_nc_non_contabilizzate



//Lettura valore totale delle bolle stampate in definitivo ma non ancora fatturate (AD_TOT_BOL_VEN)
//come prima ma con l'esclusione delle bolle con causale di tipo non fatturabile
select
	sum(tes_bol_ven.tot_val_bolla)
into
	:as_esposizione.ddt_non_fatturati
from
	tes_bol_ven
join tab_causali_trasp on tab_causali_trasp.cod_azienda=tes_bol_ven.cod_azienda
	and tab_causali_trasp.cod_causale=tes_bol_ven.cod_causale
where
	tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and tes_bol_ven.cod_cliente = :as_cod_cliente
	and tes_bol_ven.cod_documento is not null and tes_bol_ven.numeratore_documento is not null
	and tes_bol_ven.anno_documento is not null and tes_bol_ven.num_documento is not null
	and tes_bol_ven.flag_gen_fat = 'N' and tes_bol_ven.data_bolla <= :adt_data_rif
	and tab_causali_trasp.flag_segue_fattura='S';

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura totale bolle di vendita fatturabili confermate non contabilizzate: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(as_esposizione.ddt_non_fatturati) then
	as_esposizione.ddt_non_fatturati = 0
end if


//Lettura valore totale degli ordini assegnati (AD_TOT_ORD_VEN_ASS)
//come prima con l'esclusione degli ordini bloccati e con periodo di riferimento da valutarsi
//tra N giorni prima di oggi e oggi
//se il parametro del "numero dei giorni prima" non esiste allora tutte quelle inferiori a oggi

//lettura parametro "numero giorni prima"
select numero
into :ll_gg_gos
from parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda
	and cod_parametro = 'GOS' and flag_parametro = 'N';
	
if sqlca.sqlcode = 0 then
else
	ll_gg_gos = -1
end if

if ll_gg_gos = -1 then
	select
		sum(det_ord_ven.imponibile_iva / det_ord_ven.quan_ordine * det_ord_ven.quan_in_evasione)
	into
		:as_esposizione.ordini_assegnati
	from
		tes_ord_ven
		join det_ord_ven on det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione and det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione
	where
		tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
		tes_ord_ven.cod_cliente = :as_cod_cliente and
		tes_ord_ven.data_registrazione <= :adt_data_rif and
		det_ord_ven.quan_ordine <> 0 and tes_ord_ven.flag_blocco <>'S';
else
	ldt_data_gg_gos = datetime(RelativeDate(date(adt_data_rif), -ll_gg_gos), 00:00:00)
	
	select
		sum(det_ord_ven.imponibile_iva / det_ord_ven.quan_ordine * det_ord_ven.quan_in_evasione)
	into
		:as_esposizione.ordini_assegnati
	from
		tes_ord_ven
		join det_ord_ven on det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione and det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione
	where
		tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
		tes_ord_ven.cod_cliente = :as_cod_cliente and
		tes_ord_ven.data_registrazione <= :adt_data_rif and
		det_ord_ven.quan_ordine <> 0 and tes_ord_ven.flag_blocco <> 'S' and
		tes_ord_ven.data_registrazione >= :ldt_data_gg_gos;
end if

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura totale ordini di vendita assegnati: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(as_esposizione.ordini_assegnati) then
	as_esposizione.ordini_assegnati = 0
end if

//Lettura valore totale degli ordini in spedizione (bolle ancora non stampate in definitivo) (AD_TOT_ORD_VEN_SPE)
select
	sum(tot_val_bolla)
into
	:as_esposizione.ordini_non_spediti
from
	tes_bol_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_cliente = :as_cod_cliente and
	data_bolla is null and
	flag_gen_fat = 'N' and
	data_registrazione <= :adt_data_rif;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura totale ordini di vendita in spedizione: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(as_esposizione.ordini_non_spediti) then
	as_esposizione.ordini_non_spediti = 0
end if

//Lettura valore totale degli ordini aperti e parziali (AD_TOT_ORD_VEN_A_P)
//come prima con l'esclusione degli ordini bloccati e con periodo di riferimento da valutarsi
//tra N giorni prima di oggi e oggi
//se il parametro del "numero dei giorni prima" (letto prima) non esiste allora tutte quelle inferiori a oggi

// 19-07-2012 EnMe: ora tiene anche conto dello sconto pagamento inserito nella forma di pagamento.
if ll_gg_gos = -1 then

	select 
		sum(  ( ( isnull(det_ord_ven.imponibile_iva,0) / isnull(det_ord_ven.quan_ordine,0) * (isnull(det_ord_ven.quan_ordine,0) - isnull(det_ord_ven.quan_evasa,0) - isnull(det_ord_ven.quan_in_evasione,0) ) ) * (1+ (isnull(tab_ive.aliquota,0) / 100) ) ) /  ( 1 + (isnull(tab_pagamenti.sconto,0) / 100) ) )
	into
		:ad_tot_ord_ven_a_p_ivato
	from det_ord_ven 
		join tes_ord_ven on det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione and det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione
		join tab_ive on	det_ord_ven.cod_azienda = tab_ive.cod_azienda and det_ord_ven.cod_iva = tab_ive.cod_iva
		join tab_pagamenti on tes_ord_ven.cod_azienda = tab_pagamenti.cod_azienda and tes_ord_ven.cod_pagamento = tab_pagamenti.cod_pagamento	
	where
		tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
		tes_ord_ven.cod_cliente = :as_cod_cliente and
		tes_ord_ven.data_registrazione <= :adt_data_rif and
		det_ord_ven.quan_ordine <> 0 and 
		tes_ord_ven.flag_blocco <>'S' and 
		det_ord_ven.flag_evasione <> 'E' and
		tes_ord_ven.flag_fuori_fido = 'N'  ;
	
	if isnull(ad_tot_ord_ven_a_p_ivato) then ad_tot_ord_ven_a_p_ivato = 0 
	if isnull(ad_tot_ord_ven_a_p_esente) then ad_tot_ord_ven_a_p_esente = 0 
		
	as_esposizione.ordini_aperti = ad_tot_ord_ven_a_p_ivato
//	as_esposizione.ordini_aperti = ad_tot_ord_ven_a_p_ivato + ad_tot_ord_ven_a_p_esente
		
else		
	select 
		sum(  ( ( isnull(det_ord_ven.imponibile_iva,0) / isnull(det_ord_ven.quan_ordine,0) * (isnull(det_ord_ven.quan_ordine,0) - isnull(det_ord_ven.quan_evasa,0) - isnull(det_ord_ven.quan_in_evasione,0) ) ) * (1+ (isnull(tab_ive.aliquota,0) / 100) ) ) /  ( 1 + (isnull(tab_pagamenti.sconto,0) / 100) ) )
	into
		:ad_tot_ord_ven_a_p_ivato
	from det_ord_ven 
		join tes_ord_ven on det_ord_ven.cod_azienda = tes_ord_ven.cod_azienda and det_ord_ven.anno_registrazione = tes_ord_ven.anno_registrazione and det_ord_ven.num_registrazione = tes_ord_ven.num_registrazione
		join tab_ive on	det_ord_ven.cod_azienda = tab_ive.cod_azienda and det_ord_ven.cod_iva = tab_ive.cod_iva
		join tab_pagamenti on tes_ord_ven.cod_azienda = tab_pagamenti.cod_azienda and tes_ord_ven.cod_pagamento = tab_pagamenti.cod_pagamento	
	where
		tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
		tes_ord_ven.cod_cliente = :as_cod_cliente and
		tes_ord_ven.data_registrazione <= :adt_data_rif and
		det_ord_ven.quan_ordine <> 0 and 
		tes_ord_ven.flag_blocco <>'S' and 
		det_ord_ven.flag_evasione <> 'E' and
		tes_ord_ven.data_registrazione >= :ldt_data_gg_gos ;
		
	if isnull(ad_tot_ord_ven_a_p_ivato) then ad_tot_ord_ven_a_p_ivato = 0 
	if isnull(ad_tot_ord_ven_a_p_esente) then ad_tot_ord_ven_a_p_esente = 0 
		
//	as_esposizione.ordini_aperti = ad_tot_ord_ven_a_p_ivato + ad_tot_ord_ven_a_p_esente
	as_esposizione.ordini_aperti = ad_tot_ord_ven_a_p_ivato
		
	
end if

if sqlca.sqlcode < 0 then
	as_messaggio =  "Errore in lettura totale ordini di vendita aperti e parziali: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(as_esposizione.ordini_aperti) then
	as_esposizione.ordini_aperti = 0
end if

return 0
	

end function

public subroutine uof_set_msg_ckeck_cliente (boolean ab_value);
ib_msg_check_cliente = ab_value
end subroutine

public function string uof_get_msg_fido ();

return is_msg_report_giri
end function

public function integer uof_get_blocco_cliente (string as_cod_cliente, datetime adt_data_rif, string as_cod_parametro_blocco, ref string as_msg);
//verifica se il cliente alla data riferimento risulta bloccato o bloccato finanziariamente in un contesto attivato dalla parametri_blocco

//torna 	0 se non c'è alcun blocco (e quindi nessun messaggio da dare)
//			1 se c'è un warning da dare (l'azione deve comunque andare avanti)
//			2 se c'è un messaggio bloccante da dare (l'azione deve essere bloccata)
//inutile gestire eventuali errori SQL in questa funzione (in tal caso torna comunque 0)

string				ls_flag_blocco, ls_msg_blocco_bloccato, ls_msg_blocco_warning, &
					ls_flag_blocco_fin, ls_msg_blocco_fin_bloccato, ls_msg_blocco_fin_warning, &
					ls_flag_blocco_cliente, ls_flag_blocco_fin_cliente, ls_msg_default, ls_msg_default_2, ls_temp
datetime			ldt_data_blocco_cliente, ldt_data_blocco_fin_cliente
boolean			lb_data_rif, lb_warning_blocco


if isnull(as_cod_cliente) or trim(as_cod_cliente)="" then return 0


//----------------------------------------------------------------------------------------------------------------------
//leggo la tabella parametri_blocco nel contesto passato
select 	flag_blocco,
			msg_blocco_bloccato,
			msg_blocco_warning,
			flag_blocco_fin,
			msg_blocco_fin_bloccato,
			msg_blocco_fin_warning
into		:ls_flag_blocco,
			:ls_msg_blocco_bloccato,
			:ls_msg_blocco_warning,
			:ls_flag_blocco_fin,
			:ls_msg_blocco_fin_bloccato,
			:ls_msg_blocco_fin_warning
from parametri_blocco
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_parametro=:as_cod_parametro_blocco;
			
if isnull(ls_flag_blocco) or ls_flag_blocco="" then ls_flag_blocco="N"
if isnull(ls_flag_blocco_fin) or ls_flag_blocco_fin="" then ls_flag_blocco_fin="N"


ls_temp = "Cliente: " + as_cod_cliente+" - Contesto: "+as_cod_parametro_blocco


//----------------------------------------------------------------------------------------------------------------------
//se non è stato impostato alcun blocco nel contesto allora inutile proseguire con i controlli sul cliente
if ls_flag_blocco="N" and ls_flag_blocco_fin="N" then
	uof_scrivi_log_sistema("BL.FIN", ls_temp+" : no blocchi attivati in parametri_blocco")
	return 0
end if


//----------------------------------------------------------------------------------------------------------------------
//leggo se il cliente ha dei blocchi impostati in anagrafica
select 	flag_blocco, data_blocco,
			flag_blocco_fin, data_blocco_fin
into 		:ls_flag_blocco_cliente, :ldt_data_blocco_cliente,
			:ls_flag_blocco_fin_cliente, :ldt_data_blocco_fin_cliente
from anag_clienti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_cliente=:as_cod_cliente;
	
//se il cliente non risulta nè bloccato nè con blocco finanziario inutile proseguire con i controlli sul cliente
if ls_flag_blocco_cliente="N" and ls_flag_blocco_fin_cliente="N" then
	uof_scrivi_log_sistema("BL.FIN", ls_temp+" : Cliente senza blocco anagrafica e finanziario")
	return 0
end if


//----------------------------------------------------------------------------------------------------------------------
//imposto messaggi di default se per caso non ho inserito il relativo testo in tabella parametri blocco
ls_msg_default = "(Cod. " + as_cod_cliente + ") Cliente con Blocco in anagrafica"
ls_msg_default_2 = "(Cod. " + as_cod_cliente + ") Cliente con Blocco Finanziario in anagrafica"

//impostazione messaggio con iniezione del codice cliente (se impostato) per una maggiore chiarezza, altrimenti metto quello di default
if isnull(ls_msg_blocco_bloccato) or trim(ls_msg_blocco_bloccato)="" then
	ls_msg_blocco_bloccato = ls_msg_default
else
	ls_msg_blocco_bloccato = "(Cod. " + as_cod_cliente + ") " + ls_msg_blocco_bloccato
end if
if isnull(ls_msg_blocco_warning) or trim(ls_msg_blocco_warning)="" then
	ls_msg_blocco_warning = ls_msg_default
else
	ls_msg_blocco_warning = "(Cod. " + as_cod_cliente + ") " + ls_msg_blocco_warning
end if
if isnull(ls_msg_blocco_fin_bloccato) or trim(ls_msg_blocco_fin_bloccato)="" then
	ls_msg_blocco_fin_bloccato = ls_msg_default_2
else
	ls_msg_blocco_fin_bloccato = "(Cod. " + as_cod_cliente + ") " + ls_msg_blocco_fin_bloccato
end if
if isnull(ls_msg_blocco_fin_warning) or trim(ls_msg_blocco_fin_warning)="" then
	ls_msg_blocco_fin_warning = ls_msg_default_2
else
	ls_msg_blocco_fin_warning = "(Cod. " + as_cod_cliente + ") " + ls_msg_blocco_fin_warning
end if


lb_warning_blocco = false


//NOTA BENE
//se durante questo controllo c'è un warning NON bloccante sul blocco cliente ed anche un blocco finanziario (non warning)
//allora fai prevalere il messaggio bloccante del blocco finanziario !!!!!
//----------------------------------------------------------------------------------------------------------------------
//prima controllo il blocco di anagrafica
//----------------------------------------------------------------------------------------------------------------------

//se le date non sono impostate evita di controllare se risulta il blocco alla data riferimento
lb_data_rif = true

//NON FARE QUESTO CONTROLLO SULLA DATA DEL BLOCCO E LA DATA DI RIFERIMENTO
//if 	not isnull(ldt_data_blocco_cliente) and year(date(ldt_data_blocco_cliente))>=1950 and &
//	not isnull(adt_data_rif) and year(date(adt_data_rif))>=1950	then
//	
//	if adt_data_rif<ldt_data_blocco_cliente then
//		//il blocco è stato impostato dopo la data riferimento, quindi considera come se non fosse bloccato
//		lb_data_rif = false
//	end if
//	
//end if

if ls_flag_blocco_cliente="S" and ls_flag_blocco<>"N" and lb_data_rif then
	//cliente bloccato e blocco in parametri attivato (S , W): QUINDI DARE MESSAGGIO
	
	if ls_flag_blocco="S" then
		//blocca
		as_msg = ls_msg_blocco_bloccato
		uof_scrivi_log_sistema("BL.FIN", ls_temp+" : (BLOCCO) " + as_msg)
		return 2
		
	elseif ls_flag_blocco="W" then
		//solo alert
		//non uscire subito, prima controlla se per caso c'è un blocco finanziorio bloccante
		as_msg = ls_msg_blocco_warning
		lb_warning_blocco = true
		//return 1
	end if
else
	//controllo su blocco cliente superato
end if


//----------------------------------------------------------------------------------------------------------------------
//poi controllo il blocco finanziario
//----------------------------------------------------------------------------------------------------------------------

//se le date non sono impostate evita di controllare se risulta il blocco alla data riferimento
lb_data_rif = true

//NON FARE QUESTO CONTROLLo SULLA DATA DEL BLOCCO E LA DATA DI RIFERIMENTO
//if not isnull(ldt_data_blocco_fin_cliente) and year(date(ldt_data_blocco_fin_cliente))>=1950 and &
//	not isnull(adt_data_rif) and year(date(adt_data_rif))>=1950	then
//	
//	if adt_data_rif<ldt_data_blocco_fin_cliente then
//		//il blocco finanziario è stato impostato dopo la data riferimento, quindi considera come se non fosse bloccato
//		lb_data_rif = false
//	end if
//	
//end if

if ls_flag_blocco_fin_cliente="S" and ls_flag_blocco_fin<>"N" and lb_data_rif then
	//cliente bloccato finanziariamente blocco in parametri attivato (S , W): QUINDI DARE MESSAGGIO
	
	if ls_flag_blocco_fin="S" then
		//blocca
		as_msg = ls_msg_blocco_fin_bloccato
		uof_scrivi_log_sistema("BL.FIN", ls_temp+" : (BLOCCO) " + as_msg)
		return 2
		
	elseif ls_flag_blocco_fin="W" then
		//solo alert
		
		//se è rimasto in sospeso un warning sul blocco anagrafica segnala quello
		if lb_warning_blocco then
			//solo alert
			uof_scrivi_log_sistema("BL.FIN", ls_temp+" : (ALERT) " + as_msg)
			return 1
		end if
		
		as_msg = ls_msg_blocco_fin_warning
		uof_scrivi_log_sistema("BL.FIN", ls_temp+" : (ALERT) " + as_msg)
		return 1
	end if
else
	//controllo su blocco finanziario cliente superato
	
	//se è rimasto in sospeso un warning sul blocco anagrafica segnalalo
	if lb_warning_blocco then
		//solo alert
		uof_scrivi_log_sistema("BL.FIN", ls_temp+" : (ALERT) " + as_msg)
		return 1
	end if
	
end if

uof_scrivi_log_sistema("BL.FIN", ls_temp+" : Controllo Blocco finanziario superato")



return 0














end function

public function integer uof_get_blocco_cliente_loop (string as_cod_cliente, datetime adt_data_rif, string as_cod_parametro_blocco, ref string as_clienti_elaborati[], ref string as_clienti_bloccati[], ref string as_clienti_warning[], ref boolean ab_salta, ref string as_messaggio);//da chiamare all'interno di un loop di documenti/azioni (es. creazione commesse, generazione ordini da offerte, ecc...)


integer			li_ret
long				ll_index


//#################################################################################
//0 	verifica prima con la funzione uof_get_blocco_cliente
//1		già tra quelli con warning, non saltare l'azione ma inutile chiamare la funzione uof_get_blocco_cliente
//2		già tra quelli con blocco, non saltare l'azione ma inutile chiamare la funzione uof_get_blocco_cliente
//3		già tra quelli elaborati, inutile chiamare la funzione uof_get_blocco_cliente, elabora subito l'azione
//NOTA mette comunque il cliente tra quelli elaborati, se non ancora presente

as_messaggio = ""
li_ret = uof_check_cliente_con_blocco(as_cod_cliente, as_clienti_elaborati[], as_clienti_bloccati[], as_clienti_warning[])

choose case li_ret
	case 3
		//-------------------------------------------------------------------------------------------
		//NON controllare ulteriormente con uof_get_blocco_cliente e NON saltare l'azione
		//cliente già elaborato e privo di blocchi o warning sull'azione
		ab_salta = false
		as_messaggio = ""
		return 0
	
	case 2
		//-------------------------------------------------------------------------------------------
		//NON controllare ulteriormente con uof_get_blocco_cliente e SALTA l'azione
		//cliente già elaborato e con blocco sull'azione (no msg in quanto già dato all'interno del loop)
		ab_salta = true
		as_messaggio = ""
		return 2
	
	case 1
		//-------------------------------------------------------------------------------------------
		//NON controllare con uof_get_blocco_cliente e NON saltare l'azione
		ab_salta = false
		as_messaggio = ""
		return 1
	
	case else
		//-------------------------------------------------------------------------------------------
		//0:		//CONTROLLA PRIMA con uof_get_blocco_cliente
		li_ret = uof_get_blocco_cliente( as_cod_cliente, adt_data_rif, as_cod_parametro_blocco, as_messaggio)
		
		//inserisco comunque il cliente tra quelli elaborati
		as_clienti_elaborati[upperbound(as_clienti_elaborati[]) + 1] = as_cod_cliente
		
		
		if li_ret=2 then
			//dai messaggio di blocco			
			//inserisci il codice cliente tra i bloccati
			as_clienti_bloccati[upperbound(as_clienti_bloccati[]) + 1] = as_cod_cliente
			
			//SALTA l'azione e dai il messaggio in "as_messaggio"
			ab_salta = true
			return 2
			
		elseif li_ret=1 then
			//dai messaggio di warning
			//inserisci il codice cliente tra quelli CON warning
			as_clienti_warning[upperbound(as_clienti_warning[]) + 1] = as_cod_cliente
			
			//NON saltare l'azione e dai il messaggio in "as_messaggio"
			ab_salta = false
			return 1
			
		end if
	
end choose

as_messaggio = ""
ab_salta = false
return 0

end function

public function integer uof_check_cliente_con_blocco (string as_cod_cliente, string as_clienti_elaborati[], string as_clienti_bloccati[], string as_clienti_warning[]);long			ll_index


//----------------------------------------------------------------------------------------------
//verifica se è tra quelli bloccati
for ll_index=1 to upperbound(as_clienti_bloccati[])
	if as_cod_cliente=as_clienti_bloccati[ll_index] then
		//già inserito nella lista bloccati, quindi salta l'azione e non dare alcun messaggio sul blocco (è stato già dato in precedente elaborazione!)
		return 2
	end if
next

//----------------------------------------------------------------------------------------------
//verifica se è tra quelli con warning
for ll_index=1 to upperbound(as_clienti_warning[])
	if as_cod_cliente=as_clienti_warning[ll_index] then
		//già inserito nella lista clienti con warning, quindi NON saltare ll'azione e non dare alcun messaggio di warning (è stato già dato in precedente elaborazione!)
		return 1
	end if
next

//----------------------------------------------------------------------------------------------
//verifica se è tra quelli già elaborati
for ll_index=1 to upperbound(as_clienti_elaborati[])
	if as_cod_cliente=as_clienti_elaborati[ll_index] then
		//già inserito nella lista clienti elaborati, quindi NON saltare l'azione ma non occorre controllarlo ulteriormente
		return 3
	end if
next

//se arrivi fin qui il cliente non è stato mai elaborato, occorre dunque elaborarlo con la funzione di controllo della parametri_blocco
return 0
end function

public subroutine uof_scrivi_log_sistema (string as_flag_log, string as_valore);uo_log_sistema				luo_log

if not ib_log then return

luo_log = create uo_log_sistema
luo_log.uof_write_log_sistema_not_sqlca(as_flag_log, as_valore)
destroy luo_log

return
end subroutine

public function integer uof_get_blocco_fornitore (string as_cod_fornitore, datetime adt_data_rif, string as_cod_parametro_blocco, ref string as_msg);
//verifica se il fornitore alla data riferimento risulta bloccato o bloccato finanziariamente in un contesto attivato dalla parametri_blocco

//torna 	0 se non c'è alcun blocco (e quindi nessun messaggio da dare)
//			1 se c'è un warning da dare (l'azione deve comunque andare avanti)
//			2 se c'è un messaggio bloccante da dare (l'azione deve essere bloccata)
//inutile gestire eventuali errori SQL in questa funzione (in tal caso torna comunque 0)

string				ls_flag_blocco, ls_msg_blocco_bloccato, ls_msg_blocco_warning, &
					ls_flag_blocco_fornitore, ls_msg_default, ls_temp
datetime			ldt_data_blocco_fornitore
boolean			lb_data_rif, lb_warning_blocco


if isnull(as_cod_fornitore) or trim(as_cod_fornitore)="" then return 0


//----------------------------------------------------------------------------------------------------------------------
//leggo la tabella parametri_blocco nel contesto passato
select 	flag_blocco,
			msg_blocco_bloccato,
			msg_blocco_warning
			msg_blocco_fin_warning
into		:ls_flag_blocco,
			:ls_msg_blocco_bloccato,
			:ls_msg_blocco_warning
from 		parametri_blocco
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_parametro=:as_cod_parametro_blocco;
			
if isnull(ls_flag_blocco) or ls_flag_blocco="" then ls_flag_blocco="N"


ls_temp = "Fornitore: " + as_cod_fornitore+" - Contesto: "+as_cod_parametro_blocco


//----------------------------------------------------------------------------------------------------------------------
//se non è stato impostato alcun blocco nel contesto allora inutile proseguire con i controlli sul fornitore
if ls_flag_blocco="N" then
	uof_scrivi_log_sistema("BL.FOR", ls_temp+" : no blocchi attivati in parametri_blocco")
	return 0
end if


//----------------------------------------------------------------------------------------------------------------------
//leggo se il fornitore ha dei blocchi impostati in anagrafica
select 	flag_blocco, data_blocco
into 		:ls_flag_blocco_fornitore, :ldt_data_blocco_fornitore
from 		anag_fornitori
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_fornitore=:as_cod_fornitore;
	
//se il fornitore non risulta nè bloccato inutile proseguire con i controlli sul fornitore
if ls_flag_blocco_fornitore="N" then
	uof_scrivi_log_sistema("BL.FOR", ls_temp+" : Fornitore senza blocco,")
	return 0
end if


//----------------------------------------------------------------------------------------------------------------------
//imposto messaggi di default se per caso non ho inserito il relativo testo in tabella parametri blocco
ls_msg_default = "(Cod. " + as_cod_fornitore + ") Fornitore con Blocco in anagrafica"

//impostazione messaggio con iniezione del codice fornitore (se impostato) per una maggiore chiarezza, altrimenti metto quello di default
if isnull(ls_msg_blocco_bloccato) or trim(ls_msg_blocco_bloccato)="" then
	ls_msg_blocco_bloccato = ls_msg_default
else
	ls_msg_blocco_bloccato = "(Cod. " + as_cod_fornitore + ") " + ls_msg_blocco_bloccato
end if
if isnull(ls_msg_blocco_warning) or trim(ls_msg_blocco_warning)="" then
	ls_msg_blocco_warning = ls_msg_default
else
	ls_msg_blocco_warning = "(Cod. " + as_cod_fornitore + ") " + ls_msg_blocco_warning
end if

lb_warning_blocco = false

//se le date non sono impostate evita di controllare se risulta il blocco alla data riferimento
lb_data_rif = true

if ls_flag_blocco_fornitore="S" and ls_flag_blocco<>"N" and lb_data_rif then
	//Fornitore bloccato e blocco in parametri attivato (S , W): QUINDI DARE MESSAGGIO
	
	if ls_flag_blocco="S" then
		//blocca
		as_msg = ls_msg_blocco_bloccato
		uof_scrivi_log_sistema("BL.FOR", ls_temp+" : (BLOCCO) " + as_msg)
		return 2
		
	elseif ls_flag_blocco="W" then
		//solo alert
		as_msg = ls_msg_blocco_warning
		lb_warning_blocco = true
		return 1
	end if
else
	//controllo su blocco fornitore superato
end if

return 0














end function

public function integer uof_get_blocco_prodotto (string as_cod_prodotto, datetime adt_data_rif, string as_cod_parametro_blocco, ref string as_msg);
//verifica se il prodotto alla data riferimento risulta bloccato in un contesto attivato dalla parametri_blocco

//torna 	0 se non c'è alcun blocco (e quindi nessun messaggio da dare)
//			1 se c'è un warning da dare (l'azione deve comunque andare avanti)
//			2 se c'è un messaggio bloccante da dare (l'azione deve essere bloccata)
//inutile gestire eventuali errori SQL in questa funzione (in tal caso torna comunque 0)

string				ls_flag_blocco, ls_msg_blocco_bloccato, ls_msg_blocco_warning, &
					ls_flag_blocco_prodotto, ls_msg_default, ls_temp
datetime			ldt_data_blocco_prodotto
boolean			lb_data_rif, lb_warning_blocco


if isnull(as_cod_prodotto) or trim(as_cod_prodotto)="" then return 0


//----------------------------------------------------------------------------------------------------------------------
//leggo la tabella parametri_blocco nel contesto passato
select 	flag_blocco,
			msg_blocco_bloccato,
			msg_blocco_warning
			msg_blocco_fin_warning
into		:ls_flag_blocco,
			:ls_msg_blocco_bloccato,
			:ls_msg_blocco_warning
from 		parametri_blocco
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_parametro=:as_cod_parametro_blocco;
			
if isnull(ls_flag_blocco) or ls_flag_blocco="" then ls_flag_blocco="N"


ls_temp = "Prodotto: " + as_cod_prodotto+" - Contesto: "+as_cod_parametro_blocco


//----------------------------------------------------------------------------------------------------------------------
//se non è stato impostato alcun blocco nel contesto allora inutile proseguire con i controlli sul prodotto
if ls_flag_blocco="N" then
	uof_scrivi_log_sistema("BL.PRD", ls_temp+" : no blocchi attivati in parametri_blocco")
	return 0
end if


//----------------------------------------------------------------------------------------------------------------------
//leggo se il fornitore ha dei blocchi impostati in anagrafica
select 	flag_blocco, data_blocco
into 		:ls_flag_blocco_prodotto, :ldt_data_blocco_prodotto
from 		anag_prodotti
where 	cod_azienda=:s_cs_xx.cod_azienda and
			cod_prodotto=:as_cod_prodotto;
	
//se il fornitore non risulta nè bloccato inutile proseguire con i controlli sul fornitore
if ls_flag_blocco_prodotto="N" then
	uof_scrivi_log_sistema("BL.PRD", ls_temp+" : Fornitore senza blocco,")
	return 0
end if


//----------------------------------------------------------------------------------------------------------------------
//imposto messaggi di default se per caso non ho inserito il relativo testo in tabella parametri blocco
ls_msg_default = "(Cod. " + as_cod_prodotto + ") Prodotto con Blocco in anagrafica"

//impostazione messaggio con iniezione del codice fornitore (se impostato) per una maggiore chiarezza, altrimenti metto quello di default
if isnull(ls_msg_blocco_bloccato) or trim(ls_msg_blocco_bloccato)="" then
	ls_msg_blocco_bloccato = ls_msg_default
else
	ls_msg_blocco_bloccato = "(Cod. " + as_cod_prodotto + ") " + ls_msg_blocco_bloccato
end if
if isnull(ls_msg_blocco_warning) or trim(ls_msg_blocco_warning)="" then
	ls_msg_blocco_warning = ls_msg_default
else
	ls_msg_blocco_warning = "(Cod. " + as_cod_prodotto + ") " + ls_msg_blocco_warning
end if

lb_warning_blocco = false

//se le date non sono impostate evita di controllare se risulta il blocco alla data riferimento
lb_data_rif = true

if ls_flag_blocco_prodotto="S" and ls_flag_blocco<>"N" and lb_data_rif then
	//Prodotto bloccato e blocco in parametri attivato (S , W): QUINDI DARE MESSAGGIO
	
	if ls_flag_blocco="S" then
		//blocca
		as_msg = ls_msg_blocco_bloccato
		uof_scrivi_log_sistema("BL.PRD", ls_temp+" : (BLOCCO) " + as_msg)
		return 2
		
	elseif ls_flag_blocco="W" then
		//solo alert
		as_msg = ls_msg_blocco_warning
		lb_warning_blocco = true
		return 1
	end if
else
	//controllo su blocco prodotto superato
end if

return 0














end function

on uo_fido_cliente.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_fido_cliente.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

