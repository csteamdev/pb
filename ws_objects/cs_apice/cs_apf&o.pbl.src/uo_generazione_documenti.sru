﻿$PBExportHeader$uo_generazione_documenti.sru
$PBExportComments$UO per generazione bolle e fatture singole da ordini.
forward
global type uo_generazione_documenti from nonvisualobject
end type
end forward

global type uo_generazione_documenti from nonvisualobject
end type
global uo_generazione_documenti uo_generazione_documenti

type variables
string is_cod_causale, is_aspetto_beni, is_cod_porto, is_cod_imballo
string is_cod_mezzo, is_cod_vettore, is_cod_inoltro, is_num_ord_cliente
string is_destinazione, is_cod_resa, is_rag_soc_1, is_rag_soc_2
string is_indirizzo, is_localita, is_cap, is_provincia, is_frazione
string is_nota_piede
dec{6} id_num_colli, id_peso_lordo, id_peso_netto
datetime idt_data_inizio_trasporto, idt_data_ord_cliente
time it_ora_inizio_trasporto

// ------------  variabili per anticipo spedizioni ------------------
dec{6} id_quan_anticipo, id_quan_assegnazione
string is_cod_deposito, is_cod_ubicazione, is_cod_lotto
datetime idt_data_stock
long il_prog_stock
long il_anno_mov_mag_ant, il_num_mov_mag_ant


// ----------- variabili per decidere se mettere le note prodotto nella fattura  ----------
boolean ib_note_prodotto = false

// ----------- aggiunto 4-7-2005 per generazione bolle da packing list  ----------
string is_rif_packing_list=""

// -- stefanop 08/06/2010: progetto 140
string is_cod_tipo_bol_ven, is_cod_tipo_fat_ven
boolean ib_change_det_ven = false
boolean ib_change_mov = false

// -- EnMe 22/06/2010 specifica richieste_varie_2010
string is_cod_tipo_ord_acq

// -- EnMe 13/04/2011
boolean ib_carica_sconti_listini_fornitori=true, ib_usa_prezzo_listino_fornitori=true

// stefanop 26/01/2012: bolle di trasferimento
string is_causale_trasporto

// stefanop 04/03/2014: abilitio o meno la visualizzazione dello storico delle spese trasp
boolean ib_visualizza_spese_trasp = true

// ------------  variabili per anticipo spedizioni ------------------
long		il_incremento_riga

private:
	boolean ib_gibus = false  //PARAMETRO AZIENDALE				identifica l'applicazione per GIBUS s.r.l.
	boolean ib_mast = false  //PARAMETRO MULTI-AZIENDALE		identifica l'applicazione per MAST
	boolean ib_ANC = false	//se attiva la gestione inserimento automatico note conformità prodotti in fattura
end variables

forward prototypes
public function integer uof_calcola_stato_ordine (long fl_anno_registrazione, long fl_num_registrazione)
public function integer uof_crea_nota_fissa (string fs_documento, long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio)
public function integer uof_evasione_ordine (long al_anno_ordine, long al_num_ordine, ref string as_messaggio)
public function integer uof_assegnazione (long al_anno_registrazione, long al_num_registrazione, ref string as_messaggio)
public function integer uof_crea_aggiungi_bolla_anticipo (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref long fl_anno_bolla_dest, ref long fl_num_bolla_dest, ref long fl_prog_riga_bol_ven, ref string fs_messaggio)
public function integer uof_crea_fattura_proforma (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_fat_ven, ref long fl_num_reg_fat_ven, ref string fs_errore)
public function integer uof_crea_fattura (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_fat_ven, ref long fl_num_reg_fat_ven, ref string fs_errore)
public function integer uof_crea_bolla (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_bol_ven, ref long fl_num_reg_bol_ven, ref string fs_errore)
public function integer uof_crea_aggiungi_fattura (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_fat_ven, ref long fl_num_reg_fat_ven, ref string fs_errore)
public function integer uof_crea_fat_riep_da_ordini (long fl_anno_registrazione[], long fl_num_registrazione[], string fs_cod_tipo_raggruppamento, long fl_anno_reg_dest[], long fl_num_reg_dest[], string fs_errore)
public function integer uof_genera_bolle_singole (long al_anno_ordine, long al_num_ordine, string as_cod_tipo_ord_ven, ref string ls_messaggio, ref long al_anno_bolla, ref long al_num_bolla)
public function integer uof_genera_fatture_singole (long al_anno_ordine, long al_num_ordine, string as_cod_tipo_ord_ven, ref string ls_messaggio, ref long al_anno_fattura, ref long al_num_fattura)
public function integer uof_crea_aggiungi_bolla (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_bol_ven, ref long fl_num_reg_bol_ven, ref string fs_errore)
public function integer uof_crea_aggiungi_bolla_pack (string fs_elenco_pl[], long fl_anno_registrazione[], long fl_num_registrazione[], ref long fl_anno_reg_bol_ven, ref long fl_num_reg_bol_ven, ref string fs_errore)
public function integer uof_genera_rda (string fs_cod_prodotto, string fs_des_prodotto, string fs_cod_versione, decimal fd_quan_richiesta, long fl_anno_commessa, long fl_num_commessa, datetime fdt_data_disponibilita, string fs_cod_tipo_politica_riordino, string fs_flag_gen_commessa, string fs_motivazione, ref long fl_anno_registrazione, ref long fl_num_registrazione, ref string fs_errore)
public function integer uof_genera_commessa (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quan_ordine, datetime fdt_data_consegna, boolean fb_attiva, ref long fl_anno_commessa, ref long fl_num_commessa, ref string fs_errore)
public function integer uof_genera_ordini_fornitori (string fs_cod_prodotto[], string fs_des_prodotto[], decimal fd_quan_ordine[], datetime fdt_data_fabbisogno[], string fs_flag_gen_commessa[], string fs_cod_versione[], string fs_note[], ref long fl_anno_ordine[], ref long fl_num_ordine[], ref string fs_messaggio)
public function integer uof_crea_aggiungi_bolla_lavorazione (long fl_anno_reg_ord_acq, long fl_num_reg_ord_acq, datawindow fdw_righe_ordine, ref long fl_anno_reg_bol_ven, ref long fl_num_reg_bol_ven, ref string fs_errore)
public function integer uof_genera_rda (string fs_cod_prodotto, string fs_des_prodotto, string fs_cod_versione, string fs_cod_fornitore, decimal fd_quan_richiesta, decimal fd_prezzo_richiesta, long fl_anno_commessa, long fl_num_commessa, datetime fdt_data_disponibilita, string fs_cod_tipo_politica_riordino, string fs_flag_gen_commessa, string fs_motivazione, string fs_des_specifica, long fl_anno_reg_ord_ven, long fl_num_reg_ord_ven, long fl_prog_riga_ord_ven, ref long fl_anno_registrazione, ref long fl_num_registrazione, ref string fs_errore)
public function integer uof_crea_bolla_trasferimento (string as_cod_deposito_partenza, string as_cod_deposito_arrivo, s_det_riga astr_righe_ordini[], ref long al_anno_bol_ven, ref long al_num_bol_ven, ref string as_error)
public function integer uof_crea_bolla_trasferimento (string as_cod_deposito_partenza, string as_cod_deposito_arrivo, string as_table, long al_anno, long al_numero, ref long al_anno_bol_ven, ref long al_num_bol_ven, ref string as_error)
public function integer uof_controlla_ordine_in_fattura (integer fi_anno_fattura, long fl_num_fattura, integer fi_anno_ordine, long fl_num_ordine, long fl_riga_ordine, ref string fs_errore)
public function integer uof_controlla_ordine_in_spedizione (integer fi_anno_ordine, long fl_num_ordine, long fl_riga_ordine, decimal fd_qta_da_spedire, ref string fs_errore)
public function integer uof_crea_bolla_acquisto (string as_cod_fornitore, string as_cod_tipo_bol_acq, string as_cod_deposito_partenza, string as_cod_deposito_arrivo, s_det_riga astr_righe[], ref long al_anno_bol_acq, ref long al_num_bol_acq, ref string as_error)
public function integer uof_ass_ord_ven (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_cod_tipo_det_ven, string as_cod_deposito, string as_cod_ubicazione, string as_cod_prodotto, double ad_quan_ordine, double ad_quan_commessa, double ad_quan_evasa, longlong al_prog_sessione)
public function integer uof_crea_aggiungi_fattura_sessione (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_fat_ven, ref long fl_num_reg_fat_ven, longlong fl_prog_sessione, ref string fs_errore)
public function integer uof_crea_aggiungi_bolla_sessione (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_bol_ven, ref long fl_num_reg_bol_ven, longlong fl_prog_sessione, ref string fs_errore)
public function integer uof_azz_ass_ord_ven (long al_anno_registrazione, long al_num_registrazione, string as_perc, ref string as_messaggio)
public function integer uof_import_righe_documento_excel (string fs_path_file, long fl_riga_partenza, string fs_tipo_documento, long fl_anno_documento, long fl_num_documento, string fs_cod_tipo_det_ven, ref string fs_errore)
public function integer uof_controlla_qta_in_documento (integer fi_anno_ordine, long fl_num_ordine, long fl_riga_ordine, ref decimal fd_quan_documento, ref string fs_errore)
public subroutine uof_correggi_errori_in_evasione (integer fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven)
public function integer uof_crea_fattura_proforma_offerta (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_fat_ven, ref long fl_num_reg_fat_ven, ref string fs_errore)
public subroutine uof_estrai_richtext_off_ven (integer fi_anno_reg, long fl_num_reg, ref string fs_nota_testata, ref string fs_nota_piede)
public function integer uof_listini_fornitori (string as_cod_tipo_listino_prodotto, string as_cod_fornitore, string as_cod_valuta, double ad_cambio_acq, string as_cod_prodotto, double ad_quantita, datetime adt_data_consegna, string as_cod_tipo_det_acq, ref decimal ad_prezzo_acquisto, ref decimal ad_sconto_1, ref decimal ad_fat_conversione, ref string as_cod_misura)
public function integer uof_inserisci_rif_vs_ordine_in_fattura (integer ai_anno, long al_numero, long al_riga, string as_cod_tipo_det_ven, string as_descrizione, string as_cod_iva, ref string as_errore)
public function integer uof_controlla_buchi (string as_gestione, string as_cod_documento, string as_numeratore_documento, integer ai_anno_esercizio, long al_num_documento, datetime adt_oggi, ref string as_messaggio)
public function integer uof_note_in_fattura (integer ai_anno_fattura, long al_num_fattura, ref long al_prog_riga_fat_ven, ref string as_errore)
public subroutine uof_note_in_fattura_prepare (integer ai_index, ref string as_des_prodotto, ref string as_note[])
public function integer uof_azz_ass_ord_ven_sessione (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, longlong al_prog_sessione, ref string as_messaggio)
end prototypes

public function integer uof_calcola_stato_ordine (long fl_anno_registrazione, long fl_num_registrazione);string ls_flag_stato_ordine

long   ll_cont=0, ll_evasi=0, ll_parziali=0, ll_aperti=0

if ib_mast then
	//per MAST il conteggio delle righe aperte deve essere fatto in base al falg tipo dettaglio
	//loro usano molto i codici tipo dettaglio PNI, prodotti non a magazzino, in cui non è richiesto il codice
	//se mettiamo in where la clausola "... cod_prodotto is not null ..." queste righe andrebbero escluse dal conteggio
	//quindi secondo l'algoritmo di questa funzione verrebbero messe a EVASE (vedi update finale se ls_flag_stato_ordine = "E")
	
	//è chiaro che in questo caso una riga aperta di tipo RIF (se configurata in tab_tipi_det_ven) 
	//essa come "prodotti non a magazzino viene conteggiata come aperta"
	
	//conteggiamo le righe aperte con flag del tipo dettaglio con i seguenti valori
	//M	prodotti a magazzino
	//C	prodotti non collegati
	//N	prodotti non a magazzino
	select count(*)
	into   :ll_aperti
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 cod_tipo_det_ven IN (	select cod_tipo_det_ven
			 								from tab_tipi_det_ven
											 where 	cod_azienda=:s_cs_xx.cod_azienda and
											 			flag_tipo_det_ven in ('M','C','N')) and
			 flag_evasione = 'A';
else
	//per tutti gli altri clienti
	select count(*)
	into   :ll_aperti
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione and
			 cod_prodotto is not null and
			 flag_evasione = 'A';
	
end if
if sqlca.sqlcode = -1 then return -1
		 
select count(*)
into   :ll_parziali
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 flag_evasione = 'P';
if sqlca.sqlcode = -1 then return -1
		 
select count(*)
into   :ll_evasi
from   det_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 flag_evasione = 'E';
if sqlca.sqlcode = -1 then return -1

if isnull(ll_evasi) then ll_evasi = 0
if isnull(ll_aperti) then ll_aperti = 0
if isnull(ll_parziali) then ll_parziali = 0

ll_cont = ll_evasi + ll_parziali + ll_aperti
if ll_cont = 0 then 
	ls_flag_stato_ordine = "A"
else
	if ll_cont = ll_evasi then
		ls_flag_stato_ordine = "E"
	elseif ll_cont = ll_aperti then
		ls_flag_stato_ordine = "A"
	else
		ls_flag_stato_ordine = "P"
	end if
end if

update tes_ord_ven
set    flag_evasione = :ls_flag_stato_ordine
where  cod_azienda  = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in aggiornamento STATO ORDINE di vendita. " + sqlca.sqlerrtext)
	return -1
end if

if ls_flag_stato_ordine = "E" then
	// se l'ordine è evaso metto evasi tutti i dettagli per sicurezza; potrebbero restare delle righe 
	// di prodotti non a magazzino aperte o parziali che poi darebbero fastidio nei report righe ordini
	update det_ord_ven
	set    flag_evasione = :ls_flag_stato_ordine
	where  cod_azienda  = :s_cs_xx.cod_azienda and
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in aggiornamento STATO ORDINE di vendita. " + sqlca.sqlerrtext)
		return -1
	end if
end if	
return 0
end function

public function integer uof_crea_nota_fissa (string fs_documento, long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio);string ls_cod_cliente, ls_cod_fornitore, ls_nota_testata, ls_nota_piede, ls_nota_video, ls_db, ls_nota_testata_1, ls_nota_piede_1, ls_cod_tipo_fat_ven, ls_null, ls_flag_tipo_bol_ven
datetime ldt_data_registrazione
integer li_risposta

setnull(ls_null)
li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
if upper(ls_db) = "MSSQL" then ls_db="SYBASE_ASE"

choose case fs_documento
	case "FAT_PRO"

		select cod_cliente, 
				 nota_testata,
				 nota_piede,
				 data_registrazione
		into   :ls_cod_cliente,
				 :ls_nota_testata,
				 :ls_nota_piede,
				 :ldt_data_registrazione
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :fl_anno_registrazione and
				 num_registrazione = :fl_num_registrazione;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca fattura per inserimento nota fissa: verificare"
			return -1
		end if
		
		setnull(ls_cod_fornitore)
		
		if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_cod_fornitore, ls_null, "GEN_FAT_PRO", ls_null, ldt_data_registrazione,  ls_nota_testata, ls_nota_piede, ls_nota_video) < 0 then
			g_mb.error(ls_nota_testata)
		end if
		
		ls_nota_testata_1 = ls_nota_testata
		ls_nota_piede_1 = ls_nota_piede
		
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_testata_1) > 255 then
			ls_nota_testata_1 = left(ls_nota_testata_1, 255)
		end if
		if ls_db = "SYBASE_ASE" and len(ls_nota_piede_1) > 255 then
			ls_nota_piede_1 = left(ls_nota_piede_1, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		update tes_fat_ven  
			set nota_testata = :ls_nota_testata_1,
				 nota_piede = :ls_nota_piede_1
		 where tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tes_fat_ven.anno_registrazione = :fl_anno_registrazione and  
				 tes_fat_ven.num_registrazione = :fl_num_registrazione;
				
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Si è verificato un errore in fase di aggiornamento note nella Testata Fattura Vendita."
			return -1
		end if
	case "FAT_VEN"

		select cod_cliente, 
				 nota_testata,
				 nota_piede,
				 data_registrazione,
				 cod_tipo_fat_ven
		into   :ls_cod_cliente,
				 :ls_nota_testata,
				 :ls_nota_piede,
				 :ldt_data_registrazione,
				 :ls_cod_tipo_fat_ven
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :fl_anno_registrazione and
				 num_registrazione = :fl_num_registrazione;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca fattura per inserimento nota fissa: verificare"
			return -1
		end if
		
		setnull(ls_cod_fornitore)
		if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_cod_fornitore, ls_null, "GEN_FAT_VEN", ls_cod_tipo_fat_ven, ldt_data_registrazione,  ls_nota_testata, ls_nota_piede, ls_nota_video) < 0 then
			g_mb.error(ls_nota_testata)
		end if
		
		ls_nota_testata_1 = ls_nota_testata
		ls_nota_piede_1 = ls_nota_piede
		
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_testata_1) > 255 then
			ls_nota_testata_1 = left(ls_nota_testata_1, 255)
		end if
		if ls_db = "SYBASE_ASE" and len(ls_nota_piede_1) > 255 then
			ls_nota_piede_1 = left(ls_nota_piede_1, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		update tes_fat_ven  
			set nota_testata = :ls_nota_testata_1,
				 nota_piede = :ls_nota_piede_1
		 where tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tes_fat_ven.anno_registrazione = :fl_anno_registrazione and  
				 tes_fat_ven.num_registrazione = :fl_num_registrazione;
				
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Si è verificato un errore in fase di aggiornamento note nella Testata Fattura Vendita."
			return -1
		end if

case "BOL_VEN"
		select A.cod_cliente, 
				A.cod_fornitore,
				 A.nota_testata,
				 A.nota_piede,
				 A.data_registrazione,
				 B.flag_tipo_bol_ven
		into   :ls_cod_cliente,
				:ls_cod_fornitore,
				:ls_nota_testata,
				:ls_nota_piede,
				:ldt_data_registrazione,
				:ls_flag_tipo_bol_ven
		from   tes_bol_ven A
		left outer join tab_tipi_bol_ven B on A.cod_azienda=B.cod_azienda and A.cod_tipo_bol_ven = B.cod_tipo_bol_ven
		where  A.cod_azienda = :s_cs_xx.cod_azienda and
				 A.anno_registrazione = :fl_anno_registrazione and
				 A.num_registrazione = :fl_num_registrazione;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca bolla per inserimento nota fissa: verificare"
			return -1
		end if
		
		choose case upper(ls_flag_tipo_bol_ven)
			case "V","T"
				setnull(ls_cod_fornitore)
				if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_cod_fornitore, ls_null, "GEN_BOL_VEN", ls_null, ldt_data_registrazione,  ls_nota_testata, ls_nota_piede, ls_nota_video) < 0 then
					g_mb.error(ls_nota_testata)
				end if
			case else
				setnull(ls_cod_cliente)
				if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_cod_fornitore, ls_null, "GEN_BOL_VEN", ls_null, ldt_data_registrazione,  ls_nota_testata, ls_nota_piede, ls_nota_video) < 0 then
					g_mb.error(ls_nota_testata)
				end if
		end choose
		
		ls_nota_testata_1 = ls_nota_testata
		ls_nota_piede_1 = ls_nota_piede
		
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_testata_1) > 255 then
			ls_nota_testata_1 = left(ls_nota_testata_1, 255)
		end if
		if ls_db = "SYBASE_ASE" and len(ls_nota_piede_1) > 255 then
			ls_nota_piede_1 = left(ls_nota_piede_1, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		update tes_bol_ven  
			set nota_testata = :ls_nota_testata_1,
				 nota_piede = :ls_nota_piede_1
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 anno_registrazione = :fl_anno_registrazione and  
				 num_registrazione = :fl_num_registrazione;
				
		if sqlca.sqlcode = -1 then
			fs_messaggio = "Si è verificato un errore in fase di aggiornamento note nella Testata Bolle di Uscita."
			return -1
		end if
end choose
return 0
end function

public function integer uof_evasione_ordine (long al_anno_ordine, long al_num_ordine, ref string as_messaggio);long   ll_anno_generato, ll_num_generato, ll_return

string ls_cod_cliente, ls_cod_tipo_ord_ven, ls_flag_bol_fat, ls_documento


select cod_cliente,
		 cod_tipo_ord_ven
into   :ls_cod_cliente,
		 :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno_ordine and
		 num_registrazione = :al_num_ordine;
		 
if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura dati ordine da tes_ord_ven: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_messaggio = "Errore in lettura dati ordine da tes_ord_ven: impossibile trovare l'ordine specificato"
	return -1
end if

select flag_bol_fat
into   :ls_flag_bol_fat
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :ls_cod_cliente;

if sqlca.sqlcode < 0 then
	as_messaggio = "Errore in lettura del documento da generare da anag_clienti: " + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 then
	as_messaggio = "Errore in lettura del documento da generare da anag_clienti: " + &
						"impossibile trovare il cliente specificato"
	return -1
end if

if isnull(ls_flag_bol_fat) then
	
	select flag_tipo_bol_fat
	into   :ls_flag_bol_fat
	from   tab_tipi_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
			 
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in lettura del documento da generare da tab_tipi_ord_ven: " + sqlca.sqlerrtext
		return -1
	elseif sqlca.sqlcode = 100 then
		as_messaggio = "Errore in lettura del documento da generare da tab_tipi_ord_ven: " + &
							"impossibile trovare il tipo ordine specificato"
		return -1
	end if
	
end if

if uof_assegnazione(al_anno_ordine,al_num_ordine,as_messaggio) = -1 then
	as_messaggio = "Errore nell'assegnazione dell'ordine " + string(al_anno_ordine) + "/" + &
						string(al_num_ordine) + ":~n" + as_messaggio
	return -1
end if

choose case ls_flag_bol_fat
		
	case "B"
		
		ls_documento = "DDT"
		ll_return = uof_genera_bolle_singole(al_anno_ordine,al_num_ordine,ls_cod_tipo_ord_ven,as_messaggio,ll_anno_generato,ll_num_generato)
		
	case "F"
		
		ls_documento = "fattura"
		ll_return = uof_genera_fatture_singole(al_anno_ordine,al_num_ordine,ls_cod_tipo_ord_ven,as_messaggio,ll_anno_generato,ll_num_generato)
		
end choose

if ll_return = -1 then
	as_messaggio = "Errore nell'evasione dell'ordine " + string(al_anno_ordine) + "/" + &
						string(al_num_ordine) + ":~n" + as_messaggio
	return -1
else
	as_messaggio = "L'ordine " + string(al_anno_ordine) + "/" + string(al_num_ordine) + &
						" è stato evaso con " + ls_documento + " " + string(ll_anno_generato) + &
	               "/" + string(ll_num_generato)
	return 0
end if
end function

public function integer uof_assegnazione (long al_anno_registrazione, long al_num_registrazione, ref string as_messaggio);integer li_return[], li_return_1, li_return_2, li_return_3
long    ll_prog_riga_ord_ven, ll_i, ll_i1, ll_anno_commessa, ll_num_commessa
string  ls_cod_tipo_det_ven, ls_cod_prodotto, ls_sql_stringa, ls_cod_deposito, &
		  ls_cod_ubicazione
dec{4}  ld_quan_ordine, ld_quan_evasa, ld_quan_prodotta, ld_quan_commessa


select cod_deposito,
		 cod_ubicazione
into   :ls_cod_deposito,
		 :ls_cod_ubicazione
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno_registrazione and
		 num_registrazione = :al_num_registrazione;
		 
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore nella select di tes_ord_ven: " + sqlca.sqlerrtext
	return -1
end if
		
declare cu_dettagli dynamic cursor for sqlsa;

ls_sql_stringa = "select cod_tipo_det_ven, " + &
					  "prog_riga_ord_ven, " + &
					  "cod_prodotto, " + &
					  "quan_ordine, " + &
					  "quan_evasa, " + &
					  "anno_commessa, " + &
					  "num_commessa " + &
					  "from det_ord_ven " + &
					  "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  "anno_registrazione = " + string(al_anno_registrazione) + " and " + &
					  "num_registrazione = " + string(al_num_registrazione) + " and " + &
					  "flag_blocco = 'N' and " + &
					  "flag_evasione <> 'E' and " + &
					  "quan_in_evasione = 0 " + &
					  "order by prog_riga_ord_ven"

prepare sqlsa from :ls_sql_stringa;

open dynamic cu_dettagli;

if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore nella open del cursore cu_dettagli: " + sqlca.sqlerrtext
	return -1
end if

ll_i = 0

do while true
	
	fetch cu_dettagli
	into  :ls_cod_tipo_det_ven,
	  	   :ll_prog_riga_ord_ven, 
			:ls_cod_prodotto, 
			:ld_quan_ordine, 
			:ld_quan_evasa,
			:ll_anno_commessa,
		   :ll_num_commessa;

	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore nella fetch del cursore cu_dettagli: " + sqlca.sqlerrtext
		close cu_dettagli;
		return -1
	elseif sqlca.sqlcode = 100 then
		exit
	end if

	if not isnull(ll_anno_commessa) and not isnull(ll_num_commessa) then
		
		select anag_commesse.quan_prodotta
		into   :ld_quan_prodotta
		from   anag_commesse
		where  cod_azienda   = :s_cs_xx.cod_azienda and
				 anno_commessa = :ll_anno_commessa  and  
				 num_commessa  = :ll_num_commessa   ;
				 
		if sqlca.sqlcode = 0 then
			ld_quan_commessa = ld_quan_prodotta		// assegno la quantità prodotta dall'ordine
		else
			ld_quan_commessa = 0							// vado avanti con la quantità dell'ordine
		end if
		
	else
		
		ld_quan_commessa = 0
		
	end if			
	
	ll_i ++
	
	li_return[ll_i] = f_ass_ord_ven(al_anno_registrazione,al_num_registrazione,ll_prog_riga_ord_ven,ls_cod_tipo_det_ven,ls_cod_deposito,ls_cod_ubicazione,ls_cod_prodotto,ld_quan_ordine,ld_quan_commessa,ld_quan_evasa)
	
loop

close cu_dettagli;

li_return_1 = 3
li_return_2 = 3
li_return_3 = 3

for ll_i1 = 1 to upperbound(li_return[])
	choose case li_return[ll_i1]
		case 0
			li_return_1 = 0
		case 1
			li_return_2 = 1
		case 2
			li_return_3 = 2
	end choose
next

if li_return_1 = 3 and li_return_2 = 1 and li_return_3 = 3 then
	as_messaggio = "Assegnazione avvenuta con successo"
	return 0
end if

if li_return_1 = 0 and li_return_2 = 3 and li_return_3 = 3 then
	as_messaggio = "Nessun dettaglio è stato assegnato; è probabile che non vi siano le quantità disponibili a magazzino.~r~nVerificare che il deposito di prelievo indicato in testata ordini sia corretto"
	return -1
end if

as_messaggio = "Non tutte le righe ordine sono state assegnate; entrare nei dettagli e veriricare"

return 0
end function

public function integer uof_crea_aggiungi_bolla_anticipo (long fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven, ref long fl_anno_bolla_dest, ref long fl_num_bolla_dest, ref long fl_prog_riga_bol_ven, ref string fs_messaggio);boolean lb_riferimento, lb_aggiungi
long ll_anno_registrazione, ll_num_registrazione, ll_anno_commessa, ll_num_commessa, ll_progr_stock[1], &
	  ll_prog_riga_bol_ven, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga
string ls_cod_tipo_bol_ven, ls_nota_testata, ls_cod_cliente[1], ls_cod_tipo_ord_ven, &
		 ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
		 ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione[1], ls_cod_deposito[1], &
		 ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, &
		 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
		 ls_cod_resa, ls_nota_piede, ls_cod_banca, ls_cod_tipo_det_ven, ls_cod_prodotto, &
		 ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, &
		 ls_flag_tipo_det_ven, ls_cod_tipo_analisi_bol, ls_cod_tipo_analisi_ord, &
		 ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, ls_causale_trasporto, &
		 ls_flag_riep_fat, ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, &
		 ls_prog_riga_doc_des, ls_num_ord_cliente, ls_cod_lotto[1], ls_cod_tipo_movimento, &
		 ls_vostro_ordine, ls_vostro_ordine_data, ls_flag_evasione, ls_lire, ls_flag_sola_iva, &
		 ls_cod_fornitore[], ls_causale_trasporto_tab, ls_cod_versione, ls_cod_causale_tab, &
		 ls_flag_abilita_rif_ord_ven,  ls_cod_tipo_det_ven_rif_ord_ven, ls_des_riferimento_ord_ven, &
  		 ls_flag_rif_anno_ord_ven, ls_flag_rif_num_ord_ven, ls_flag_rif_data_ord_ven, &
		 ls_des_riferimento, ls_cod_iva_rif, ls_cod_causale, ls_note_dettaglio_rif, &
		 ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det
datetime ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock[1], &
		   ldt_vostro_ordine_data
dec{6} ld_cambio_ven, ld_prezzo_vendita, ld_fat_conversione_ven, &
		 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, &
		 ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_testata, &
		 ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, &
		 ld_quan_ordine, ld_quan_evasa, ld_tot_val_evaso, ld_tot_val_ordine, &
		 ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, &
		 ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, &
		 ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, &
		 ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, ld_quantita_um, ld_prezzo_um, &
		 ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso
		 
uo_calcola_documento_euro luo_calcolo


lb_riferimento = true
setnull(ls_cod_fornitore[1])

if isnull(fl_anno_bolla_dest) or fl_anno_bolla_dest = 0 then
	lb_aggiungi = false
else
	lb_aggiungi = true
end if

select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca testata ordine di vendita. Dettaglio errore:"+sqlca.sqlerrtext,stopsign!)
	return -1
end if

select cod_tipo_bol_ven,  
		 cod_tipo_analisi  
into   :ls_cod_tipo_bol_ven,  
		 :ls_cod_tipo_analisi_ord
from   tab_tipi_ord_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore durante la lettura della tabella Tipi Ordini Vendita. Dettaglio errore:" + sqlca.sqlerrtext
	return -1
end if

if not isnull(ls_cod_tipo_bol_ven) then
	if not lb_aggiungi then
		ll_anno_registrazione = f_anno_esercizio()
		
		select con_bol_ven.num_registrazione
		into   :ll_num_registrazione
		from   con_bol_ven
		where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la lettura della tabella Controllo Bolle Vendita. Dettaglio errore:" + sqlca.sqlerrtext
			return -1
		end if
	
		ll_num_registrazione ++
		
		update con_bol_ven
		set    con_bol_ven.num_registrazione = :ll_num_registrazione
		where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la scrittura sulla tabella Controllo Bolle Vendita. Errore DB:" + sqlca.sqlerrtext
			return -1
		end if
	else
		ll_anno_registrazione = fl_anno_bolla_dest
		ll_num_registrazione = fl_num_bolla_dest
	end if

	select tes_ord_ven.cod_operatore,   
			 tes_ord_ven.data_registrazione,   
			 tes_ord_ven.cod_cliente,   
			 tes_ord_ven.cod_des_cliente,   
			 tes_ord_ven.rag_soc_1,   
			 tes_ord_ven.rag_soc_2,   
			 tes_ord_ven.indirizzo,   
			 tes_ord_ven.localita,   
			 tes_ord_ven.frazione,   
			 tes_ord_ven.cap,   
			 tes_ord_ven.provincia,   
			 tes_ord_ven.cod_deposito,   
			 tes_ord_ven.cod_ubicazione,   
			 tes_ord_ven.cod_valuta,   
			 tes_ord_ven.cambio_ven,   
			 tes_ord_ven.cod_tipo_listino_prodotto,   
			 tes_ord_ven.cod_pagamento,   
			 tes_ord_ven.sconto,   
			 tes_ord_ven.cod_agente_1,
			 tes_ord_ven.cod_agente_2,
			 tes_ord_ven.cod_banca,   
			 tes_ord_ven.num_ord_cliente,   
			 tes_ord_ven.data_ord_cliente,   
			 tes_ord_ven.cod_imballo,   
			 tes_ord_ven.aspetto_beni,
			 tes_ord_ven.peso_netto,
			 tes_ord_ven.peso_lordo,
			 tes_ord_ven.num_colli,
			 tes_ord_ven.cod_vettore,   
			 tes_ord_ven.cod_inoltro,   
			 tes_ord_ven.cod_mezzo,
			 tes_ord_ven.causale_trasporto,
			 tes_ord_ven.cod_porto,   
			 tes_ord_ven.cod_resa,   
			 tes_ord_ven.nota_testata,   
			 tes_ord_ven.nota_piede,   
			 tes_ord_ven.flag_riep_fat,
			 tes_ord_ven.tot_val_evaso,
			 tes_ord_ven.tot_val_ordine,
			 tes_ord_ven.cod_banca_clien_for,
			 tes_ord_ven.cod_causale,
			 tes_ord_ven.flag_doc_suc_tes, 
			 tes_ord_ven.flag_doc_suc_pie			 
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ls_flag_riep_fat,
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_cod_causale,
			 :ls_flag_doc_suc_tes, 
			 :ls_flag_doc_suc_pie			 
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione and  
			 tes_ord_ven.num_registrazione = :fl_num_registrazione;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la lettura Testata Ordine Vendita. Errore DB:" + sqlca.sqlerrtext
		return -1
	end if
	
	// stefanop: 18/07/2012 - l'operatore che inserire è SEMPRE l'operatore collegato ad Apice
	// ------------------------------------------------------
	ls_cod_operatore = guo_functions.uof_get_operatore_utente()
	// ------------------------------------------------------

	select tab_tipi_bol_ven.cod_tipo_analisi,
			 tab_tipi_bol_ven.causale_trasporto,
			 tab_tipi_bol_ven.cod_causale,
			 tab_tipi_bol_ven.flag_abilita_rif_ord_ven,
			 tab_tipi_bol_ven.cod_tipo_det_ven_rif_ord_ven,
			 tab_tipi_bol_ven.des_riferimento_ord_ven,
			 tab_tipi_bol_ven.flag_rif_anno_ord_ven,
			 tab_tipi_bol_ven.flag_rif_num_ord_ven,
			 tab_tipi_bol_ven.flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi_bol,
			 :ls_causale_trasporto_tab,
			 :ls_cod_causale_tab,
			 :ls_flag_abilita_rif_ord_ven,
			 :ls_cod_tipo_det_ven_rif_ord_ven,
			 :ls_des_riferimento_ord_ven,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_bol_ven  
	where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
	
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Si è verificato un errore nella lettura della Tabella Tipi Bolle. Dettaglio errore:" + sqlca.sqlerrtext
		return -1
	end if


	ldt_data_registrazione = datetime(today())

	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if

	if not isnull(ls_num_ord_cliente) and ls_cod_tipo_det_ven_rif_ord_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if

//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------


	if ls_cod_tipo_det_ven_rif_ord_ven = "T" then
		ls_nota_testata = ls_des_riferimento_ord_ven + string(fl_anno_registrazione) + "/" + &
								string(fl_num_registrazione) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
	end if
	if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
		ls_causale_trasporto = ls_causale_trasporto_tab
	end if
	if isnull(ls_cod_causale) then
		ls_cod_causale = ls_cod_causale_tab
	end if
	select parametri_azienda.stringa
	into   :ls_lire
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LIR';

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Configurare il codice valuta per le Lire Italiane."
		return -1
	end if

	select tab_pagamenti.sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if

	if not lb_aggiungi then
		insert into tes_bol_ven
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 data_registrazione,   
						 cod_operatore,   
						 cod_tipo_bol_ven,   
						 cod_cliente,
						 cod_fornitore,
						 cod_des_cliente,   
						 cod_fil_fornitore,
						 rag_soc_1,   
						 rag_soc_2,   
						 indirizzo,   
						 localita,   
						 frazione,   
						 cap,   
						 provincia,   
						 cod_deposito,   
						 cod_deposito_tras,
						 cod_ubicazione,
						 cod_valuta,   
						 cambio_ven,   
						 cod_tipo_listino_prodotto,   
						 cod_pagamento,   
						 sconto,   
						 cod_agente_1,
						 cod_agente_2,
						 cod_banca,   
						 num_ord_cliente,   
						 data_ord_cliente,   
						 cod_imballo,   
						 aspetto_beni,
						 peso_netto,
						 peso_lordo,
						 num_colli,
						 cod_vettore,   
						 cod_inoltro,   
						 cod_mezzo,
						 causale_trasporto,
						 cod_porto,   
						 cod_resa,   
						 cod_documento,
						 numeratore_documento,
						 anno_documento,
						 num_documento,
						 data_bolla,
						 nota_testata,   
						 nota_piede,   
						 flag_fuori_fido,
						 flag_blocco,   
						 flag_movimenti,  
						 flag_riep_fat,
						 cod_banca_clien_for,
						 flag_gen_fat,
						 cod_causale,
						 flag_doc_suc_tes,
						 flag_st_note_tes,
						 flag_doc_suc_pie,
						 flag_st_note_pie) 						 
		values      (:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ldt_data_registrazione,   
						 :ls_cod_operatore,   
						 :ls_cod_tipo_bol_ven,   
						 :ls_cod_cliente[1],
						 null,
						 :ls_cod_des_cliente,   
						 null,
						 :ls_rag_soc_1,   
						 :ls_rag_soc_2,   
						 :ls_indirizzo,   
						 :ls_localita,   
						 :ls_frazione,   
						 :ls_cap,   
						 :ls_provincia,   
						 :ls_cod_deposito[1],   
						 null,
						 :ls_cod_ubicazione[1],
						 :ls_cod_valuta,   
						 :ld_cambio_ven,   
						 :ls_cod_tipo_listino_prodotto,   
						 :ls_cod_pagamento,   
						 :ld_sconto_testata,
						 :ls_cod_agente_1,
						 :ls_cod_agente_2,
						 :ls_cod_banca,   
						 :ls_num_ord_cliente,   
						 :ldt_data_ord_cliente,   
						 :ls_cod_imballo,   
						 :ls_aspetto_beni,
						 :ld_peso_netto,
						 :ld_peso_lordo,
						 :ld_num_colli,
						 :ls_cod_vettore,   
						 :ls_cod_inoltro,   
						 :ls_cod_mezzo,   
						 :ls_causale_trasporto,
						 :ls_cod_porto,   
						 :ls_cod_resa,   
						 null,
						 null,
						 0,
						 0,
						 null,
						 :ls_nota_testata,   
						 :ls_nota_piede,   
						 'N',
						 'N',   
						 'N',
						 :ls_flag_riep_fat,
						 :ls_cod_banca_clien_for,
						 'N',
						 :ls_cod_causale,
						 'I',
						 'I',
						 'I',
						 'I');						 
	
		 if sqlca.sqlcode <> 0 then
			 fs_messaggio = "Errore durante la scrittura Testata Bolle Vendita. Errore DB:" + sqlca.sqlerrtext
			 return -1
		 end if
		if uof_crea_nota_fissa("BOL_VEN", ll_anno_registrazione, ll_num_registrazione, fs_messaggio) = -1 then return -1
		 
	 end if
	 
	lb_riferimento = true
	ll_prog_riga_bol_ven = 0

	ls_cod_deposito[1] = is_cod_deposito
	ls_cod_ubicazione[1] = is_cod_ubicazione
   ls_cod_lotto[1] = is_cod_lotto
   ldt_data_stock[1] = idt_data_stock
	ll_progr_stock[1] = il_prog_stock

	if lb_aggiungi then
		setnull(ll_prog_riga_bol_ven)
		
		select max(prog_riga_bol_ven)
		into   :ll_prog_riga_bol_ven
		from   det_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :fl_anno_bolla_dest and
				 num_registrazione = :fl_num_bolla_dest;
		if sqlca.sqlcode <> 0 or isnull(ll_prog_riga_bol_ven) then
			ll_prog_riga_bol_ven = 10
		else				
			ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		end if
	else
		ll_prog_riga_bol_ven = 10
	end if
		
	select	det_ord_ven.cod_tipo_det_ven,   
				det_ord_ven.cod_misura,   
				det_ord_ven.cod_prodotto,   
				det_ord_ven.des_prodotto,   
				det_ord_ven.quan_ordine,
				det_ord_ven.prezzo_vendita,   
				det_ord_ven.fat_conversione_ven,   
				det_ord_ven.sconto_1,   
				det_ord_ven.sconto_2,  
				det_ord_ven.provvigione_1,
				det_ord_ven.provvigione_2,
				det_ord_ven.cod_iva,   
				det_ord_ven.quan_evasa,
				det_ord_ven.nota_dettaglio,   
				det_ord_ven.anno_commessa,
				det_ord_ven.num_commessa,
				det_ord_ven.cod_centro_costo,
				det_ord_ven.sconto_3,   
				det_ord_ven.sconto_4,   
				det_ord_ven.sconto_5,   
				det_ord_ven.sconto_6,   
				det_ord_ven.sconto_7,   
				det_ord_ven.sconto_8,   
				det_ord_ven.sconto_9,   
				det_ord_ven.sconto_10,
				det_ord_ven.cod_versione,
				det_ord_ven.flag_doc_suc_det,
				det_ord_ven.quantita_um,
				det_ord_ven.prezzo_um
	into		:ls_cod_tipo_det_ven, 
				:ls_cod_misura, 
				:ls_cod_prodotto, 
				:ls_des_prodotto, 
				:ld_quan_ordine,
				:ld_prezzo_vendita, 
				:ld_fat_conversione_ven, 
				:ld_sconto_1, 
				:ld_sconto_2, 
				:ld_provvigione_1, 
				:ld_provvigione_2, 
				:ls_cod_iva, 
				:ld_quan_evasa,
				:ls_nota_dettaglio, 
				:ll_anno_commessa,
				:ll_num_commessa,
				:ls_cod_centro_costo,
				:ld_sconto_3, 
				:ld_sconto_4, 
				:ld_sconto_5, 
				:ld_sconto_6, 
				:ld_sconto_7, 
				:ld_sconto_8, 
				:ld_sconto_9, 
				:ld_sconto_10,
				:ls_cod_versione,
				:ls_flag_doc_suc_det,
				:ld_quantita_um,
				:ld_prezzo_um
	  from 	det_ord_ven  
	 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
				det_ord_ven.num_registrazione = :fl_num_registrazione and
				det_ord_ven.prog_riga_ord_ven = :fl_prog_riga_ord_ven;

	if sqlca.sqlcode <> 0 then
		fs_messaggio =  "Errore durante la lettura Dettagli Ordini Vendita. Dettaglio errore:" + sqlca.sqlerrtext
		return -1
	end if
			
//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
// ricalcolo la quantità di vendita nel caso di evasioni parziali
		if id_quan_anticipo <> ld_quan_ordine then
			ld_quantita_um = round(id_quan_anticipo * ld_fat_conversione_ven, 4)
		end if
//------------------------------------------------- Modifica Nicola ---------------------------------------------
	if ls_flag_doc_suc_det = 'I' then //nota dettaglio
		select flag_doc_suc_or
		  into :ls_flag_doc_suc_det
		  from tab_tipi_det_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		
	
	
	if ls_flag_doc_suc_det = 'N' then
		setnull(ls_nota_dettaglio)
	end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------		
					
			
	select tab_tipi_det_ven.flag_tipo_det_ven,
			 tab_tipi_det_ven.cod_tipo_movimento,
			 tab_tipi_det_ven.flag_sola_iva
	into   :ls_flag_tipo_det_ven,
			 :ls_cod_tipo_movimento,
			 :ls_flag_sola_iva
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli."
		return -1
	end if

	if ls_flag_tipo_det_ven = "M" then
		if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
			fs_messaggio = "Si è verificato un errore in fase di creazione destinazioni movimenti."
			return -1
		end if
		if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
			return -1
		end if
	end if

	if lb_riferimento and ls_flag_abilita_rif_ord_ven = "D" then
		lb_riferimento = false
		setnull(ls_note_dettaglio_rif)
		ls_des_riferimento = ls_des_riferimento_ord_ven
		if ls_flag_rif_anno_ord_ven = "S" and not isnull(fl_anno_registrazione) then
			ls_des_riferimento = ls_des_riferimento + "-" +string(fl_anno_registrazione)
		end if
		if ls_flag_rif_num_ord_ven = "S" and not isnull(fl_num_registrazione) then
			ls_des_riferimento = ls_des_riferimento + "-" + string(fl_num_registrazione)
		end if
		if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
			ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
		end if
		if not isnull(ls_num_ord_cliente) then
			ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
			if not isnull(ldt_data_ord_cliente) then
				ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
			end if
		end if
			
		if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_ord_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
			g_mb.messagebox("Generazione Bolla di Anticipo","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
		end if
		
		insert into det_bol_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_bol_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 progr_stock,
						 data_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_consegnata,   
						 prezzo_vendita,   
						 fat_conversione_ven,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 nota_dettaglio,   
						 anno_registrazione_ord_ven,   
						 num_registrazione_ord_ven,   
						 prog_riga_ord_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 anno_reg_bol_acq,
						 num_reg_bol_acq,
						 prog_riga_bol_acq,
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 cod_versione,
						 flag_doc_suc_det,
						 flag_st_note_det,
						 quantita_um,
						 prezzo_um)
	  values			(:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_bol_ven,   
						 :ls_cod_tipo_det_ven_rif_ord_ven,   
						 null,
						 null,
						 null,
						 null,
						 null,
						 null,   
						 null,   
						 :ls_des_riferimento,   
						 0,   
						 0,   
						 1,   
						 0,   
						 0,  
						 0,
						 0,
						 :ls_cod_iva_rif,   
						 null,
						 null,
						 :ls_note_dettaglio_rif,   
						 null,   
						 null,   
						 null,
						 null,   
						 null,   
						 null,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,
						 null,
						 null,
						 null,
						 null,
						 null,
						 null,
						 null,
						 'I',
						 'I',
						 0,
						 0);						 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la scrittura Dettagli Bolle Vendita. Errore DB:" + sqlca.sqlerrtext
			return -1
		end if
		ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
	end if	

	insert into det_bol_ven  
					(cod_azienda,   
					 anno_registrazione,   
					 num_registrazione,   
					 prog_riga_bol_ven,   
					 cod_tipo_det_ven,   
					 cod_deposito,
					 cod_ubicazione,
					 cod_lotto,
					 progr_stock,
					 data_stock,
					 cod_misura,   
					 cod_prodotto,   
					 des_prodotto,   
					 quan_consegnata,   
					 prezzo_vendita,   
					 fat_conversione_ven,   
					 sconto_1,   
					 sconto_2,   
					 provvigione_1,
					 provvigione_2,
					 cod_iva,   
					 cod_tipo_movimento,
					 num_registrazione_mov_mag,
					 nota_dettaglio,   
					 anno_registrazione_ord_ven,   
					 num_registrazione_ord_ven,   
					 prog_riga_ord_ven,
					 anno_commessa,
					 num_commessa,
					 cod_centro_costo,
					 sconto_3,   
					 sconto_4,   
					 sconto_5,   
					 sconto_6,   
					 sconto_7,   
					 sconto_8,   
					 sconto_9,   
					 sconto_10,
					 anno_registrazione_mov_mag,
					 anno_reg_bol_acq,
					 num_reg_bol_acq,
					 prog_riga_bol_acq,
					 anno_reg_des_mov,
					 num_reg_des_mov,
					 cod_versione,
					 flag_doc_suc_det,
					 flag_st_note_det,
					 quantita_um,
					 prezzo_um)
  values			(:s_cs_xx.cod_azienda,   
					 :ll_anno_registrazione,   
					 :ll_num_registrazione,   
					 :ll_prog_riga_bol_ven,   
					 :ls_cod_tipo_det_ven,   
					 :ls_cod_deposito[1],
					 :ls_cod_ubicazione[1],
					 :ls_cod_lotto[1],
					 :ll_progr_stock[1],
					 :ldt_data_stock[1],
					 :ls_cod_misura,   
					 :ls_cod_prodotto,   
					 :ls_des_prodotto,   
					 :id_quan_anticipo,   
					 :ld_prezzo_vendita,   
					 :ld_fat_conversione_ven,   
					 :ld_sconto_1,   
					 :ld_sconto_2,  
					 :ld_provvigione_1,
					 :ld_provvigione_2,
					 :ls_cod_iva,   
					 :ls_cod_tipo_movimento,
					 null,
					 :ls_nota_dettaglio,   
					 :fl_anno_registrazione,   
					 :fl_num_registrazione,   
					 :fl_prog_riga_ord_ven,
					 :ll_anno_commessa,   
					 :ll_num_commessa,   
					 :ls_cod_centro_costo,   
					 :ld_sconto_3,   
					 :ld_sconto_4,   
					 :ld_sconto_5,   
					 :ld_sconto_6,   
					 :ld_sconto_7,   
					 :ld_sconto_8,   
					 :ld_sconto_9,   
					 :ld_sconto_10,
					 null,
					 null,
					 null,
					 null,
					 :ll_anno_reg_des_mov,
					 :ll_num_reg_des_mov,
					 :ls_cod_versione,
					 'I',
					 'I',
					 :ld_quantita_um,
					 :ld_prezzo_um);					 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la scrittura Dettagli Bolle Vendita. Errore DB:" + sqlca.sqlerrtext
		return -1
	end if
	
	if ls_flag_tipo_det_ven = "M" then
		update anag_prodotti  
			set quan_assegnata = quan_assegnata - :id_quan_assegnazione,
				 quan_in_spedizione = quan_in_spedizione + :id_quan_anticipo
		 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Si è verificato un errore in fase di aggiornamento quantità assegnata / spedizione in anagrafica prodotti.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
			return -1
		end if
		
		update stock 
		set 	 quan_assegnata = quan_assegnata - :id_quan_assegnazione,   
				 quan_in_spedizione = quan_in_spedizione + :id_quan_anticipo
		where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
				 stock.cod_prodotto = :ls_cod_prodotto and 
				 stock.cod_deposito = :ls_cod_deposito[1] and 
				 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
				 stock.cod_lotto = :ls_cod_lotto[1] and 
				 stock.data_stock = :ldt_data_stock[1] and 
				 stock.prog_stock = :ll_progr_stock[1];

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Si è verificato un errore in fase di aggiornamento quantità assegnata / spedizione stock.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
			return -1
		end if
	end if				

	ls_tabella_orig = "det_ord_ven_stat"
	ls_tabella_des = "det_bol_ven_stat"
	ls_prog_riga_doc_orig = "prog_riga_ord_ven"
	ls_prog_riga_doc_des = "prog_riga_bol_ven"

	if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, fl_anno_registrazione, fl_num_registrazione, fl_prog_riga_ord_ven, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven) = -1 then
		return -1
	end if	

	if ls_cod_tipo_analisi_bol <> ls_cod_tipo_analisi_ord then
		if f_crea_distribuzione(ls_cod_tipo_analisi_bol, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_bol_ven, 5) = -1 then
			fs_messaggio = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
			return -1
		end if
	end if

	if ld_quan_ordine = ld_quan_evasa + id_quan_assegnazione then
		ls_flag_evasione = "E"
	else
		ls_flag_evasione = "P"
	end if
	
	update det_ord_ven  
		set quan_in_evasione = quan_in_evasione - :id_quan_assegnazione,
			 quan_evasa = quan_evasa + :id_quan_assegnazione,
			 flag_evasione = :ls_flag_evasione
	 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
			 det_ord_ven.num_registrazione = :fl_num_registrazione and
			 det_ord_ven.prog_riga_ord_ven = :fl_prog_riga_ord_ven;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento quantità assegnata / spedizione in Dettaglio Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
		return -1
	end if

	if ls_flag_sola_iva = 'N' then
		ld_val_riga = id_quan_assegnazione * ld_prezzo_vendita
		ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
		ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
		ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
		ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
		ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
		ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
		ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
		ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
		ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
		ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
		ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
		ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
		ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
		ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
		ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
		ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
		ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
		ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
		ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100

		if ls_cod_valuta = ls_lire then
			ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
		else
			ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
		end if   

		if ls_flag_tipo_det_ven = "M" or &
			ls_flag_tipo_det_ven = "C" or &
			ls_flag_tipo_det_ven = "N" then
			ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
			ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
			ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
			ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
		elseif ls_flag_tipo_det_ven = "S" then
			ld_val_evaso = ld_val_riga_sconto_10 * -1
		else
			ld_val_evaso = ld_val_riga_sconto_10
		end if
		ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
	end if

	if ls_cod_valuta = ls_lire then
		ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
	end if   

	if ld_tot_val_ordine = ld_tot_val_evaso then
		ls_flag_evasione = "E"
	else
		ls_flag_evasione = "P"
	end if

//	update tes_ord_ven  
//		set tot_val_evaso = :ld_tot_val_evaso
//	 where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione and  
//			 tes_ord_ven.num_registrazione = :fl_num_registrazione;
//
//	if sqlca.sqlcode <> 0 then
//		fs_messaggio = "Si è verificato un errore in fase di aggiornamento Valore Evaso in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
//		return -1
//	end if
	
	if uof_calcola_stato_ordine(fl_anno_registrazione,fl_num_registrazione) = -1 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento stato dell'ordine in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
		return -1
	end if

	luo_calcolo = create uo_calcola_documento_euro

	if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_ven",fs_messaggio) <> 0 then
		destroy luo_calcolo
		return -1
	end if
	
	destroy luo_calcolo

	fl_anno_bolla_dest = ll_anno_registrazione
	fl_num_bolla_dest  = ll_num_registrazione
	fl_prog_riga_bol_ven = ll_prog_riga_bol_ven

end if
return 0
end function

public function integer uof_crea_fattura_proforma (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_fat_ven, ref long fl_num_reg_fat_ven, ref string fs_errore);boolean lb_riferimento
long ll_anno_registrazione, ll_num_registrazione, &
	  ll_prog_riga_ord_ven, ll_anno_commessa, ll_num_commessa, ll_progr_stock[1], &
	  ll_prog_riga_fat_ven, ll_prog_riga, ll_num_confezioni, ll_num_pezzi_confezione
string ls_cod_tipo_fat_proforma, ls_nota_testata, ls_cod_cliente[1], ls_cod_tipo_ord_ven, &
		 ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
		 ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione[1], ls_cod_deposito[1], &
		 ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, &
		 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
		 ls_cod_resa, ls_nota_piede, ls_cod_banca, ls_cod_tipo_det_ven, ls_cod_prodotto, &
		 ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, &
		 ls_flag_tipo_det_ven, ls_cod_tipo_analisi_fat, &
		 ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, ls_causale_trasporto, &
		 ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, &
		 ls_num_ord_cliente, ls_cod_lotto[1], ls_cod_tipo_movimento, ls_vostro_ordine, &
		 ls_vostro_ordine_data, ls_flag_evasione, ls_lire, ls_flag_sola_iva, &
		 ls_cod_fornitore[], ls_causale_trasporto_tab,ls_cod_versione, ls_flag_abilita_rif_bol_ven, &
		 ls_cod_tipo_det_ven_rif_bol_ven, ls_des_riferimento_ord_ven, ls_flag_rif_anno_ord_ven, &
		 ls_flag_rif_num_ord_ven, ls_flag_rif_data_ord_ven, ls_des_riferimento, ls_cod_iva_rif, &
		 ls_note_dettaglio_rif, ls_db, ls_cod_tipo_det_ven_proforma, &
		 ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det
datetime ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock[1], &
		 ldt_vostro_ordine_data
dec{6} ld_cambio_ven, ld_prezzo_vendita, ld_fat_conversione_ven, &
		 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, &
		 ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_testata, &
		 ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, &
		 ld_quan_ordine,ld_sconto_pagamento, ld_quantita_um, ld_prezzo_um
integer li_risposta

uo_calcola_documento_euro luo_calcolo

//Donato 25/03/2009 modifica per specifica Integrazione proforma
/*	Se per il cliente intestatario dell'ordine di vendita in esame esistono già delle fatture proforma NON CONFERMATE
	si deve poter scegliere se crearne una nuova oppure se scegliere a quale di queste aggiungere la riga
	In quest'ultimo caso, se il tipo pagamento dell'ordine è diverso da quello della fattura scelta ciò deve essere segnalato 
	e l'utente deve quindi confermare di voler forzare il tipo pag. della fattura a quello dell'ordine oppure annullare tutto.
*/

string 			ls_cod_cliente_ordine, ls_rag_soc_cliente_ordine
long 				ll_righe
boolean			lb_crea_fattura_vendita
integer			li_ret

//verifica se il cliente ha fatture del tipo "pro-forma" non ancora confermate:
//recupera il cliente, il tipo pagamento dell'ordine e altre info
select	tes_ord_ven.cod_cliente,
			anag_clienti.rag_soc_1,
			tes_ord_ven.data_registrazione,
			tes_ord_ven.cod_pagamento,
			tes_ord_ven.num_ord_cliente,
			tes_ord_ven.data_ord_cliente
into		:ls_cod_cliente_ordine,
			:ls_rag_soc_cliente_ordine,
			:ldt_data_ordine,
			:ls_cod_pagamento,
			:ls_num_ord_cliente,
			:ldt_data_ord_cliente
from tes_ord_ven
join anag_clienti on anag_clienti.cod_azienda = tes_ord_ven.cod_azienda
			and anag_clienti.cod_cliente = tes_ord_ven.cod_cliente
where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			tes_ord_ven. anno_registrazione = :fl_anno_registrazione and  
			 tes_ord_ven.num_registrazione = :fl_num_registrazione;
			 
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la lettura del Cliente in Testata Ordine Vendita."
	return -1
end if	 

 select count(tes_fat_ven.cod_azienda)
 into :ll_righe
 from tes_fat_ven
 join tab_tipi_fat_ven on tab_tipi_fat_ven.cod_azienda = tes_fat_ven.cod_azienda
 		and tab_tipi_fat_ven.cod_tipo_fat_ven = tes_fat_ven.cod_tipo_fat_ven
where tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_fat_ven.cod_cliente = :ls_cod_cliente_ordine and
			 tab_tipi_fat_ven.flag_tipo_fat_ven = 'P' and
			 (tes_fat_ven.cod_documento = '' or tes_fat_ven.cod_documento is null) and
			 (tes_fat_ven.numeratore_documento = '' or tes_fat_ven.numeratore_documento is null) and
			 (tes_fat_ven.anno_documento = 0 or tes_fat_ven.anno_documento is null) and
			 (tes_fat_ven.num_documento = 0 or tes_fat_ven.num_documento is null);
		
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la lettura delle Fatture di vendita del Cliente."
	return -1
end if	 

//di default crea fattura nuova
lb_crea_fattura_vendita = true

if ll_righe > 0 then
	li_ret = g_mb.messagebox("APICE", "Esistono già fatture pro-forma di vendita non confermate per il cliente "+&
				ls_cod_cliente_ordine + " - " + ls_rag_soc_cliente_ordine + "." + char(13) + &
				"Si desidera aggiungere l'ordine ad una di queste fatture? " + char(13) + char(13) + &
				"N.B. In caso di risposta negativa sarà creata una nuova fattura di vendita", Question!, YesNoCancel!, 1)
				
	choose case li_ret
		case 1			//SI: aggiungi a fattura esistente
			//apri una maschera di selezione fattura vendita
			s_cs_xx.parametri.parametro_s_1 = ls_cod_cliente_ordine
			s_cs_xx.parametri.parametro_s_2 = ls_rag_soc_cliente_ordine
			s_cs_xx.parametri.parametro_s_3 = ls_cod_pagamento
			
			open(w_sel_fat_ven_proforma)
			
			setnull(s_cs_xx.parametri.parametro_s_1)
			setnull(s_cs_xx.parametri.parametro_s_2)
			setnull(s_cs_xx.parametri.parametro_s_3)
			
			lb_crea_fattura_vendita = false
			
		case 2			//NO: crea nuova fattura
			lb_crea_fattura_vendita = true
			
		case else		//ANNULLA: rollback
			fs_errore = "Operazione annullata dall'utente!"
			return -1
			
	end choose
		
end if

if not lb_crea_fattura_vendita then
	
	lb_riferimento = true
	//-------------------------------------------------------------------------------------------------------------------------------------
	//aggiungi ad una fattura esistente
	//prima di tutto verifica se devi fare il rollback (scelto dall'utente)
	if s_cs_xx.parametri.parametro_b_1 then
		setnull(s_cs_xx.parametri.parametro_d_1)
		setnull(s_cs_xx.parametri.parametro_d_2)
		setnull(s_cs_xx.parametri.parametro_b_1)
		setnull(s_cs_xx.parametri.parametro_b_2)
		
		fs_errore = "Operazione annullata dall'utente!"
		return -1
	end if
	
	ll_anno_registrazione = s_cs_xx.parametri.parametro_d_1
	ll_num_registrazione = s_cs_xx.parametri.parametro_d_2
	
	setnull(s_cs_xx.parametri.parametro_d_1)
	setnull(s_cs_xx.parametri.parametro_d_2)
		
	//ora verifica se devi aggiornare il cod_pagamento della fattura prescelta
	if s_cs_xx.parametri.parametro_b_2 then
		setnull(s_cs_xx.parametri.parametro_b_2)
		
		update tes_fat_ven
		set cod_pagamento = :ls_cod_pagamento,
			flag_movimenti = 'S'
		where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and num_registrazione = :ll_num_registrazione;
			
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante l'aggiornamento della Fattura pro-forma di Vendita.~r~n" + sqlca.sqlerrtext
			return -1
		end if	
	else
		//non aggiornare il tipo pagamento, tanto è uguale
		//ma aggiorna il flag_movimenti
		update tes_fat_ven
		set flag_movimenti = 'S'
		where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and num_registrazione = :ll_num_registrazione;
			
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante l'aggiornamento della Fattura pro-forma di Vendita.~r~n" + sqlca.sqlerrtext
			return -1
		end if	
	end if
	
	setnull(s_cs_xx.parametri.parametro_b_2)
	
	//ora procedi con l'aggiornamento del dettaglio della fattura
	//########################################################
	
	select data_registrazione
	into :ldt_data_registrazione
	from tes_fat_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and num_registrazione = :ll_num_registrazione;
	
	select cod_tipo_ord_ven
	into   :ls_cod_tipo_ord_ven
	from   tes_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione  = :fl_num_registrazione;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la ricerca del tipo ordine di vendita in testata ordini di vendita."
		return -1
	end if
	
	select cod_tipo_fat_proforma
	into   :ls_cod_tipo_fat_proforma
	from   tab_tipi_ord_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la ricerca tipo fattura in tabella Tipi Ordini Vendita."
		return -1
	end if

	if isnull(ls_cod_tipo_fat_proforma) then
		fs_errore = "Nel tipo ordine manca l'indicazione del tipo fattura proforma di destinazione; operazione annullata "
		return -1
	end if
	
	if not isnull(ls_cod_tipo_fat_proforma) then	
		declare  cu_righe_ordine_2 cursor for  
		select	cod_tipo_det_ven, cod_misura, cod_prodotto, des_prodotto, quan_ordine, prezzo_vendita, fat_conversione_ven, 
					sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10,
					provvigione_1,	provvigione_2,	cod_iva, nota_dettaglio, anno_commessa, num_commessa,
					cod_centro_costo, cod_versione, num_confezioni, num_pezzi_confezione, flag_doc_suc_det,
					quantita_um, prezzo_um, prog_riga_ord_ven
		  from 	det_ord_ven  
		 where 	cod_azienda = :s_cs_xx.cod_azienda and  
					anno_registrazione = :fl_anno_registrazione and  
					num_registrazione = :fl_num_registrazione 
		order by prog_riga_ord_ven asc;
	
		open cu_righe_ordine_2;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la OPEN del cursore righe ordine."
			return -1
		end if
		
		//calcolo il max det_fat_ven.prog_riga_fat_ven (in quanto la fattura è esistente)
		select max(prog_riga_fat_ven)
		into :ll_prog_riga_fat_ven
		from det_fat_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and num_registrazione =:ll_num_registrazione;
		
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la lettura del max prog. riga nel dettaglio fattura!"
			return -1
		end if
		
		if isnull(ll_prog_riga_fat_ven) then ll_prog_riga_fat_ven = 0		
		
		do while 0 = 0
			fetch cu_righe_ordine_2
			into		:ls_cod_tipo_det_ven, :ls_cod_misura, :ls_cod_prodotto, :ls_des_prodotto, :ld_quan_ordine, :ld_prezzo_vendita, 
						:ld_fat_conversione_ven, :ld_sconto_1, :ld_sconto_2,:ld_sconto_3, :ld_sconto_4, :ld_sconto_5, :ld_sconto_6, :ld_sconto_7, 
						:ld_sconto_8, :ld_sconto_9, :ld_sconto_10, :ld_provvigione_1, :ld_provvigione_2, :ls_cod_iva,
						:ls_nota_dettaglio, :ll_anno_commessa,	:ll_num_commessa,	:ls_cod_centro_costo, :ls_cod_versione, :ll_num_confezioni,
						:ll_num_pezzi_confezione, :ls_flag_doc_suc_det, :ld_quantita_um, :ld_prezzo_um, :ll_prog_riga_ord_ven;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode = -1 then
				fs_errore =  "Errore durante la lettura delle righe dell'ordine.~r~n" + sqlca.sqlerrtext
				close cu_righe_ordine_2;
				return -1
			end if
			ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10
	//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
	// ricalcolo la quantità di vendita nel caso di evasioni parziali
			if ls_flag_doc_suc_det = 'I' then //nota dettaglio
				select flag_doc_suc_or
				  into :ls_flag_doc_suc_det
				  from tab_tipi_det_ven
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			end if		
		
			if ls_flag_doc_suc_det = 'N' then setnull(ls_nota_dettaglio)
			
			select flag_tipo_det_ven,
					 cod_tipo_movimento,
					 flag_sola_iva,
					 cod_tipo_det_ven_proforma
			into   :ls_flag_tipo_det_ven,
					 :ls_cod_tipo_movimento,
					 :ls_flag_sola_iva,
					 :ls_cod_tipo_det_ven_proforma
			from   tab_tipi_det_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			if sqlca.sqlcode = -1 then
				fs_errore = "Si è verificato un errore in fase di lettura tipi dettagli."
				close cu_righe_ordine_2;
				return -1
			end if
	
			if ls_flag_tipo_det_ven = "M" then
				if isnull(ls_cod_tipo_det_ven_proforma) then
					fs_errore = "Non è stato indicato il tipo dettaglio da usare nelle PROFORMA nel tipo dettaglio " + ls_cod_tipo_det_ven + ".~r~nAndare ad indicare il dato e ripetere l'operazione."
					close cu_righe_ordine_2;
					return -1
				end if
				ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_proforma
			end if
			
			select tab_tipi_fat_ven.causale_trasporto,
					 tab_tipi_fat_ven.flag_abilita_rif_bol_ven,
					 tab_tipi_fat_ven.cod_tipo_det_ven_rif_bol_ven,
					 tab_tipi_fat_ven.des_riferimento_ord_ven,
					 tab_tipi_fat_ven.flag_rif_anno_ord_ven,
					 tab_tipi_fat_ven.flag_rif_num_ord_ven,
					 tab_tipi_fat_ven.flag_rif_data_ord_ven
			into	 :ls_causale_trasporto,
					 :ls_flag_abilita_rif_bol_ven,
					 :ls_cod_tipo_det_ven_rif_bol_ven,
					 :ls_des_riferimento_ord_ven,
					 :ls_flag_rif_anno_ord_ven,
					 :ls_flag_rif_num_ord_ven,
					 :ls_flag_rif_data_ord_ven
			from   tab_tipi_fat_ven  
			where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_proforma;
			if sqlca.sqlcode <> 0 then
				fs_errore = "Si è verificato un errore nella lettura della Tabella Tipi Fatture."
				return -1
			end if
	
			if lb_riferimento and ls_flag_abilita_rif_bol_ven = "D" then
				lb_riferimento = false
				setnull(ls_note_dettaglio_rif)
				ls_des_riferimento = ls_des_riferimento_ord_ven
				if ls_flag_rif_anno_ord_ven = "S" and not isnull(fl_anno_registrazione) then
					ls_des_riferimento = ls_des_riferimento + "-" +string(fl_anno_registrazione)
				end if
				if ls_flag_rif_num_ord_ven = "S" and not isnull(fl_num_registrazione) then
					ls_des_riferimento = ls_des_riferimento + "-" + string(fl_num_registrazione)
				end if
				if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
					ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
				end if
				if not isnull(ls_num_ord_cliente) then
					ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
					if not isnull(ldt_data_ord_cliente) then
						ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
					end if
				end if
				
				ls_cod_cliente[1] = ls_cod_cliente_ordine
				
				if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_bol_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
					g_mb.messagebox("Generazione Bolla Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
				end if
				
				insert into det_fat_ven  
								(cod_azienda,   
								 anno_registrazione,   
								 num_registrazione,   
								 prog_riga_fat_ven,   
								 cod_tipo_det_ven,   
								 cod_deposito,
								 cod_ubicazione,
								 cod_lotto,
								 data_stock,
								 progr_stock,
								 cod_misura,   
								 cod_prodotto,   
								 des_prodotto,   
								 quan_fatturata,   
								 prezzo_vendita,   
								 sconto_1,   
								 sconto_2,   
								 provvigione_1,
								 provvigione_2,
								 cod_iva,   
								 cod_tipo_movimento,
								 num_registrazione_mov_mag,
								 anno_registrazione_fat,
								 num_registrazione_fat,
								 nota_dettaglio,   
								 anno_registrazione_bol_ven,   
								 num_registrazione_bol_ven,   
								 prog_riga_bol_ven,
								 anno_commessa,
								 num_commessa,
								 cod_centro_costo,
								 sconto_3,   
								 sconto_4,   
								 sconto_5,   
								 sconto_6,   
								 sconto_7,   
								 sconto_8,   
								 sconto_9,   
								 sconto_10,
								 anno_registrazione_mov_mag,
								 fat_conversione_ven,   
								 anno_reg_des_mov,
								 num_reg_des_mov,
								 anno_reg_ord_ven,   
								 num_reg_ord_ven,   
								 prog_riga_ord_ven,
								 cod_versione,
								 num_confezioni,
								 num_pezzi_confezione,
								 flag_st_note_det,
								 quantita_um,
								 prezzo_um)							 
			  values			(:s_cs_xx.cod_azienda,   
								 :ll_anno_registrazione,   
								 :ll_num_registrazione,   
								 :ll_prog_riga_fat_ven,   
								 :ls_cod_tipo_det_ven_rif_bol_ven,   
								 null,
								 null,
								 null,
								 null,
								 null,
								 null,
								 null,
								 :ls_des_riferimento,   
								 0,   
								 0,   
								 0,   
								 0,  
								 0,
								 0,
								 :ls_cod_iva_rif,   
								 null,
								 null,
								 null,
								 null,
								 null,   
								 null,
								 null,
								 null,
								 null,   
								 null,   
								 null,   
								 0,   
								 0,   
								 0,   
								 0,   
								 0,   
								 0,   
								 0,   
								 0,
								 null,
								 1,
								 null,
								 null,
								 null,   
								 null,   
								 null,
								 null,
								 0,
								 0,
								 'I',
								 0,
								 0);
		
				if sqlca.sqlcode <> 0 then
					fs_errore = "Errore durante la scrittura Dettagli Bolle Vendita.~r~n" + sqlca.sqlerrtext
					close cu_righe_ordine_2;
					return -1
				end if
				ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10
			end if	
	
			insert into det_fat_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_fat_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 data_stock,
							 progr_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_fatturata,   
							 prezzo_vendita,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 anno_registrazione_fat,
							 num_registrazione_fat,
							 nota_dettaglio,   
							 anno_registrazione_bol_ven,   
							 num_registrazione_bol_ven,   
							 prog_riga_bol_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 fat_conversione_ven,   
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 anno_reg_ord_ven,   
							 num_reg_ord_ven,   
							 prog_riga_ord_ven,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)			
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_fat_ven,   
							 :ls_cod_tipo_det_ven,   
							 null,
							 null,
							 null,
							 null,
							 null,
							 :ls_cod_misura,   
							 :ls_cod_prodotto,   
							 :ls_des_prodotto,   
							 :ld_quan_ordine,
							 :ld_prezzo_vendita,   
							 :ld_sconto_1,   
							 :ld_sconto_2,  
							 :ld_provvigione_1,
							 :ld_provvigione_2,
							 :ls_cod_iva,   
							 :ls_cod_tipo_movimento,
							 null,
							 null,
							 null,
							 :ls_nota_dettaglio,   
							 null,
							 null,
							 null,
							 :ll_anno_commessa,   
							 :ll_num_commessa,   
							 :ls_cod_centro_costo,   
							 :ld_sconto_3,   
							 :ld_sconto_4,   
							 :ld_sconto_5,   
							 :ld_sconto_6,   
							 :ld_sconto_7,   
							 :ld_sconto_8,   
							 :ld_sconto_9,   
							 :ld_sconto_10,
							 null,
							 :ld_fat_conversione_ven,
							 null,
							 null,
							 :fl_anno_registrazione,   
							 :fl_num_registrazione,   
							 :ll_prog_riga_ord_ven,
							 :ls_cod_versione,
							 :ll_num_confezioni,
							 :ll_num_pezzi_confezione,
							 'I',
							 :ld_quantita_um,
							 :ld_prezzo_um);								 						 
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la scrittura Dettagli Fatture Vendita.~r~n" + sqlca.sqlerrtext
				close cu_righe_ordine_2;
				return -1
			end if
			
			//--------------------------------------------------------------------
			//SR Note_conformita_prodotto
			if uof_note_in_fattura(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven, fs_errore) < 0 then
				close cu_righe_ordine_2;
				return -1
			end if
			//--------------------------------------------------------------------
			
		loop
		close cu_righe_ordine_2;
		
		luo_calcolo = create uo_calcola_documento_euro
	
		if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"fat_ven",fs_errore) <> 0 then
			destroy luo_calcolo
			return -1
		end if
		
		destroy luo_calcolo
		
		fl_anno_reg_fat_ven = ll_anno_registrazione
		fl_num_reg_fat_ven = ll_num_registrazione
	end if
	//########################################################
	
	//-------------------------------------------------------------------------------------------------------------------------------------
else
	//crea una nuova fattura
	//-------------------------------------------------------------------------------------------------------------------------------------	
	setnull(s_cs_xx.parametri.parametro_d_1)
	setnull(s_cs_xx.parametri.parametro_d_2)
	setnull(s_cs_xx.parametri.parametro_b_1)
	setnull(s_cs_xx.parametri.parametro_b_2)	

	li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
	if upper(ls_db) = "MSSQL" then ls_db="SYBASE_ASE"
	
	
	lb_riferimento = true
	setnull(ls_cod_fornitore[1])
	
	select cod_tipo_ord_ven
	into   :ls_cod_tipo_ord_ven
	from   tes_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione  = :fl_num_registrazione;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la ricerca del tipo ordine di vendita in testata ordini di vendita."
		return -1
	end if
	
	select cod_tipo_fat_proforma
	into   :ls_cod_tipo_fat_proforma
	from   tab_tipi_ord_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la ricerca tipo fattura in tabella Tipi Ordini Vendita."
		return -1
	end if
	
	if isnull(ls_cod_tipo_fat_proforma) then
		fs_errore = "Nel tipo ordine manca l'indicazione del tipo fattura proforma di destinazione; operazione annullata "
		return -1
	end if
	
	if not isnull(ls_cod_tipo_fat_proforma) then
		ll_anno_registrazione = f_anno_esercizio()
		
		select max(num_registrazione)
		into   :ll_num_registrazione
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione;
	
		if ll_anno_registrazione = 0 or isnull(ll_num_registrazione) then ll_num_registrazione = 0
		ll_num_registrazione ++
		
		update con_fat_ven
		set    con_fat_ven.num_registrazione = :ll_num_registrazione
		where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura sulla tabella Controllo Fatture Vendita."
			return -1
		end if
	
		select tes_ord_ven.cod_operatore,   
				 tes_ord_ven.data_registrazione,   
				 tes_ord_ven.cod_cliente,   
				 tes_ord_ven.cod_des_cliente,   
				 tes_ord_ven.rag_soc_1,   
				 tes_ord_ven.rag_soc_2,   
				 tes_ord_ven.indirizzo,   
				 tes_ord_ven.localita,   
				 tes_ord_ven.frazione,   
				 tes_ord_ven.cap,   
				 tes_ord_ven.provincia,   
				 tes_ord_ven.cod_deposito,   
				 tes_ord_ven.cod_ubicazione,   
				 tes_ord_ven.cod_valuta,   
				 tes_ord_ven.cambio_ven,   
				 tes_ord_ven.cod_tipo_listino_prodotto,   
				 tes_ord_ven.cod_pagamento,   
				 tes_ord_ven.sconto,   
				 tes_ord_ven.cod_agente_1,
				 tes_ord_ven.cod_agente_2,
				 tes_ord_ven.cod_banca,   
				 tes_ord_ven.num_ord_cliente,   
				 tes_ord_ven.data_ord_cliente,   
				 tes_ord_ven.cod_imballo,   
				 tes_ord_ven.aspetto_beni,
				 tes_ord_ven.peso_netto,
				 tes_ord_ven.peso_lordo,
				 tes_ord_ven.num_colli,
				 tes_ord_ven.cod_vettore,   
				 tes_ord_ven.cod_inoltro,   
				 tes_ord_ven.cod_mezzo,
				 tes_ord_ven.causale_trasporto,
				 tes_ord_ven.cod_porto,   
				 tes_ord_ven.cod_resa,   
				 tes_ord_ven.nota_testata,   
				 tes_ord_ven.nota_piede,
				 tes_ord_ven.cod_banca_clien_for,
				 tes_ord_ven.flag_doc_suc_tes, 
				 tes_ord_ven.flag_doc_suc_pie			 
		 into  :ls_cod_operatore,   
				 :ldt_data_ordine,   
				 :ls_cod_cliente[1],   
				 :ls_cod_des_cliente,   
				 :ls_rag_soc_1,   
				 :ls_rag_soc_2,   
				 :ls_indirizzo,   
				 :ls_localita,   
				 :ls_frazione,   
				 :ls_cap,   
				 :ls_provincia,   
				 :ls_cod_deposito[1],   
				 :ls_cod_ubicazione[1],
				 :ls_cod_valuta,   
				 :ld_cambio_ven,   
				 :ls_cod_tipo_listino_prodotto,   
				 :ls_cod_pagamento,   
				 :ld_sconto_testata,   
				 :ls_cod_agente_1,
				 :ls_cod_agente_2,
				 :ls_cod_banca,   
				 :ls_num_ord_cliente,   
				 :ldt_data_ord_cliente,   
				 :ls_cod_imballo,   
				 :ls_aspetto_beni,
				 :ld_peso_netto,
				 :ld_peso_lordo,
				 :ld_num_colli,
				 :ls_cod_vettore,   
				 :ls_cod_inoltro,   
				 :ls_cod_mezzo,   
				 :ls_causale_trasporto,
				 :ls_cod_porto,
				 :ls_cod_resa,   
				 :ls_nota_testata,   
				 :ls_nota_piede,
				 :ls_cod_banca_clien_for,
				 :ls_flag_doc_suc_tes, 
				 :ls_flag_doc_suc_pie			 
		from   tes_ord_ven  
		where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tes_ord_ven.anno_registrazione = :fl_anno_registrazione and  
				 tes_ord_ven.num_registrazione = :fl_num_registrazione;
	
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la lettura Testata Ordine Vendita."
			return -1
		end if
		
		// stefanop: 18/07/2012 - l'operatore che inserire è SEMPRE l'operatore collegato ad Apice
		// ------------------------------------------------------
		ls_cod_operatore = guo_functions.uof_get_operatore_utente()
		// ------------------------------------------------------
	
		select tab_tipi_fat_ven.causale_trasporto,
				 tab_tipi_fat_ven.flag_abilita_rif_bol_ven,
				 tab_tipi_fat_ven.cod_tipo_det_ven_rif_bol_ven,
				 tab_tipi_fat_ven.des_riferimento_ord_ven,
				 tab_tipi_fat_ven.flag_rif_anno_ord_ven,
				 tab_tipi_fat_ven.flag_rif_num_ord_ven,
				 tab_tipi_fat_ven.flag_rif_data_ord_ven
		into	 :ls_causale_trasporto,
				 :ls_flag_abilita_rif_bol_ven,
				 :ls_cod_tipo_det_ven_rif_bol_ven,
				 :ls_des_riferimento_ord_ven,
				 :ls_flag_rif_anno_ord_ven,
				 :ls_flag_rif_num_ord_ven,
				 :ls_flag_rif_data_ord_ven
		from   tab_tipi_fat_ven  
		where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_proforma;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Si è verificato un errore nella lettura della Tabella Tipi Fatture."
			return -1
		end if
		
		ldt_data_registrazione = datetime(today())
	
		if isnull(ls_nota_testata) then
			ls_nota_testata = ""
		end if
	
		if not isnull(ls_num_ord_cliente)  and ls_cod_tipo_det_ven_rif_bol_ven = "T" then
			ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
			if not isnull(ldt_data_ord_cliente) then
				ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
			else
				ls_vostro_ordine_data = ""
			end if
		else
			ls_vostro_ordine = ""
			ls_vostro_ordine_data = ""
		end if
	
		if ls_flag_doc_suc_tes = 'I' then //nota di testata
			select flag_doc_suc
			  into :ls_flag_doc_suc_tes
			  from tab_tipi_ord_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		end if		
	
		if ls_flag_doc_suc_tes = 'N' then
			ls_nota_testata = ""
		end if			
	
		
		if ls_flag_doc_suc_pie = 'I' then //nota di piede
			select flag_doc_suc
			  into :ls_flag_doc_suc_pie
			  from tab_tipi_ord_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		end if			
		
		if ls_flag_doc_suc_pie = 'N' then
			ls_nota_piede = ""
		end if				
	
		if ls_cod_tipo_det_ven_rif_bol_ven = "T" then
			ls_nota_testata = "Rif. ns. Ordine Nr. " + string(fl_anno_registrazione) + "/" + &
									string(fl_num_registrazione) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
									ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
		end if
			
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
			ls_nota_testata = left(ls_nota_testata, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
	
		if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
			ls_causale_trasporto = ls_causale_trasporto_tab
		end if
	
		select parametri_azienda.stringa
		into   :ls_lire
		from   parametri_azienda
		where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
				 parametri_azienda.flag_parametro = 'S' and &
				 parametri_azienda.cod_parametro = 'LIR';
	
		if sqlca.sqlcode <> 0 then
			fs_errore = "Configurare il codice valuta per le Lire Italiane."
			return -1
		end if
	
		select tab_pagamenti.sconto
		into   :ld_sconto_pagamento
		from   tab_pagamenti
		where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;
	
		if sqlca.sqlcode <> 0 then
			ld_sconto_pagamento = 0
		end if
		
		insert into tes_fat_ven
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 data_registrazione,   
						 cod_operatore,   
						 cod_tipo_fat_ven,   
						 cod_cliente,
						 cod_des_cliente,   
						 rag_soc_1,   
						 rag_soc_2,   
						 indirizzo,   
						 localita,   
						 frazione,   
						 cap,   
						 provincia,   
						 cod_deposito,   
						 cod_ubicazione,
						 cod_valuta,   
						 cambio_ven,   
						 cod_tipo_listino_prodotto,   
						 cod_pagamento,   
						 sconto,   
						 cod_agente_1,
						 cod_agente_2,
						 cod_banca,   
						 num_ord_cliente,   
						 data_ord_cliente,   
						 cod_imballo,   
						 aspetto_beni,
						 peso_netto,
						 peso_lordo,
						 num_colli,
						 cod_vettore,   
						 cod_inoltro,   
						 cod_mezzo,
						 causale_trasporto,
						 cod_porto,   
						 cod_resa,   
						 cod_documento,
						 numeratore_documento,
						 anno_documento,
						 num_documento,
						 data_fattura,
						 nota_testata,   
						 nota_piede,   
						 flag_fuori_fido,
						 flag_blocco,   
						 flag_movimenti,  
						 flag_contabilita,
						 tot_merci,
						 tot_spese_trasporto,
						 tot_spese_imballo,
						 tot_spese_bolli,
						 tot_spese_varie,
						 tot_sconto_cassa,
						 tot_sconti_commerciali,
						 imponibile_provvigioni_1,
						 imponibile_provvigioni_2,
						 tot_provvigioni_1,
						 tot_provvigioni_2,
						 importo_iva,
						 imponibile_iva,
						 importo_iva_valuta,
						 imponibile_iva_valuta,
						 tot_fattura,
						 tot_fattura_valuta,
						 flag_cambio_zero,
						 cod_banca_clien_for,
						 flag_st_note_tes,
						 flag_st_note_pie)  					 
		values      (:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ldt_data_registrazione,   
						 :ls_cod_operatore,   
						 :ls_cod_tipo_fat_proforma,   
						 :ls_cod_cliente[1],
						 :ls_cod_des_cliente,   
						 :ls_rag_soc_1,   
						 :ls_rag_soc_2,   
						 :ls_indirizzo,   
						 :ls_localita,   
						 :ls_frazione,   
						 :ls_cap,   
						 :ls_provincia,   
						 :ls_cod_deposito[1],   
						 :ls_cod_ubicazione[1],
						 :ls_cod_valuta,   
						 :ld_cambio_ven,   
						 :ls_cod_tipo_listino_prodotto,   
						 :ls_cod_pagamento,   
						 :ld_sconto_testata,
						 :ls_cod_agente_1,
						 :ls_cod_agente_2,
						 :ls_cod_banca,   
						 :ls_num_ord_cliente,   
						 :ldt_data_ord_cliente,   
						 :ls_cod_imballo,   
						 :ls_aspetto_beni,
						 :ld_peso_netto,
						 :ld_peso_lordo,
						 :ld_num_colli,
						 :ls_cod_vettore,   
						 :ls_cod_inoltro,   
						 :ls_cod_mezzo,   
						 :ls_causale_trasporto,
						 :ls_cod_porto,   
						 :ls_cod_resa,   
						 null,
						 null,
						 0,
						 0,
						 null,
						 :ls_nota_testata,   
						 :ls_nota_piede,   
						 'N',
						 'N',   
						 'S',
						 'S',
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 'N',
						 :ls_cod_banca_clien_for,
						 'I',
						 'I');					 
	
		 if sqlca.sqlcode <> 0 then
			 fs_errore = "Errore durante la scrittura Testata Fatture Vendita."
			 return -1
		 end if
		 if uof_crea_nota_fissa("FAT_PRO", ll_anno_registrazione, ll_num_registrazione, fs_errore) = -1 then return -1
	
		declare  cu_righe_ordine cursor for  
		select	cod_tipo_det_ven, cod_misura, cod_prodotto, des_prodotto, quan_ordine, prezzo_vendita, fat_conversione_ven, 
					sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10,
					provvigione_1,	provvigione_2,	cod_iva, nota_dettaglio, anno_commessa, num_commessa,
					cod_centro_costo, cod_versione, num_confezioni, num_pezzi_confezione, flag_doc_suc_det,
					quantita_um, prezzo_um, prog_riga_ord_ven
		  from 	det_ord_ven  
		 where 	cod_azienda = :s_cs_xx.cod_azienda and  
					anno_registrazione = :fl_anno_registrazione and  
					num_registrazione = :fl_num_registrazione 
		order by prog_riga_ord_ven asc;
	
		open cu_righe_ordine;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la OPEN del curosore righe ordine."
			return -1
		end if
		
		ll_prog_riga_fat_ven = 0
		do while 0 = 0
			fetch cu_righe_ordine
			into		:ls_cod_tipo_det_ven, :ls_cod_misura, :ls_cod_prodotto, :ls_des_prodotto, :ld_quan_ordine, :ld_prezzo_vendita, 
						:ld_fat_conversione_ven, :ld_sconto_1, :ld_sconto_2,:ld_sconto_3, :ld_sconto_4, :ld_sconto_5, :ld_sconto_6, :ld_sconto_7, 
						:ld_sconto_8, :ld_sconto_9, :ld_sconto_10, :ld_provvigione_1, :ld_provvigione_2, :ls_cod_iva,
						:ls_nota_dettaglio, :ll_anno_commessa,	:ll_num_commessa,	:ls_cod_centro_costo, :ls_cod_versione, :ll_num_confezioni,
						:ll_num_pezzi_confezione, :ls_flag_doc_suc_det, :ld_quantita_um, :ld_prezzo_um, :ll_prog_riga_ord_ven;
			if sqlca.sqlcode = 100 then exit
			if sqlca.sqlcode = -1 then
				fs_errore =  "Errore durante la lettura delle righe dell'ordine.~r~n" + sqlca.sqlerrtext
				close cu_righe_ordine;
				return -1
			end if
			ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10
	//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
	// ricalcolo la quantità di vendita nel caso di evasioni parziali
			if ls_flag_doc_suc_det = 'I' then //nota dettaglio
				select flag_doc_suc_or
				  into :ls_flag_doc_suc_det
				  from tab_tipi_det_ven
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			end if		
		
			if ls_flag_doc_suc_det = 'N' then setnull(ls_nota_dettaglio)
			
			select flag_tipo_det_ven,
					 cod_tipo_movimento,
					 flag_sola_iva,
					 cod_tipo_det_ven_proforma
			into   :ls_flag_tipo_det_ven,
					 :ls_cod_tipo_movimento,
					 :ls_flag_sola_iva,
					 :ls_cod_tipo_det_ven_proforma
			from   tab_tipi_det_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			if sqlca.sqlcode = -1 then
				fs_errore = "Si è verificato un errore in fase di lettura tipi dettagli."
				close cu_righe_ordine;
				return -1
			end if
	
			if ls_flag_tipo_det_ven = "M" then
				if isnull(ls_cod_tipo_det_ven_proforma) then
					fs_errore = "Non è stato indicato il tipo dettaglio da usare nelle PROFORMA nel tipo dettaglio " + ls_cod_tipo_det_ven + ".~r~nAndare ad indicare il dato e ripetere l'operazione."
					close cu_righe_ordine;
					return -1
				end if
				ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_proforma
			end if
	
			if lb_riferimento and ls_flag_abilita_rif_bol_ven = "D" then
				lb_riferimento = false
				setnull(ls_note_dettaglio_rif)
				ls_des_riferimento = ls_des_riferimento_ord_ven
				if ls_flag_rif_anno_ord_ven = "S" and not isnull(fl_anno_registrazione) then
					ls_des_riferimento = ls_des_riferimento + "-" +string(fl_anno_registrazione)
				end if
				if ls_flag_rif_num_ord_ven = "S" and not isnull(fl_num_registrazione) then
					ls_des_riferimento = ls_des_riferimento + "-" + string(fl_num_registrazione)
				end if
				if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
					ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
				end if
				if not isnull(ls_num_ord_cliente) then
					ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
					if not isnull(ldt_data_ord_cliente) then
						ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
					end if
				end if
					
				if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_bol_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
					g_mb.messagebox("Generazione Bolla Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
				end if
				
				insert into det_fat_ven  
								(cod_azienda,   
								 anno_registrazione,   
								 num_registrazione,   
								 prog_riga_fat_ven,   
								 cod_tipo_det_ven,   
								 cod_deposito,
								 cod_ubicazione,
								 cod_lotto,
								 data_stock,
								 progr_stock,
								 cod_misura,   
								 cod_prodotto,   
								 des_prodotto,   
								 quan_fatturata,   
								 prezzo_vendita,   
								 sconto_1,   
								 sconto_2,   
								 provvigione_1,
								 provvigione_2,
								 cod_iva,   
								 cod_tipo_movimento,
								 num_registrazione_mov_mag,
								 anno_registrazione_fat,
								 num_registrazione_fat,
								 nota_dettaglio,   
								 anno_registrazione_bol_ven,   
								 num_registrazione_bol_ven,   
								 prog_riga_bol_ven,
								 anno_commessa,
								 num_commessa,
								 cod_centro_costo,
								 sconto_3,   
								 sconto_4,   
								 sconto_5,   
								 sconto_6,   
								 sconto_7,   
								 sconto_8,   
								 sconto_9,   
								 sconto_10,
								 anno_registrazione_mov_mag,
								 fat_conversione_ven,   
								 anno_reg_des_mov,
								 num_reg_des_mov,
								 anno_reg_ord_ven,   
								 num_reg_ord_ven,   
								 prog_riga_ord_ven,
								 cod_versione,
								 num_confezioni,
								 num_pezzi_confezione,
								 flag_st_note_det,
								 quantita_um,
								 prezzo_um)							 
			  values			(:s_cs_xx.cod_azienda,   
								 :ll_anno_registrazione,   
								 :ll_num_registrazione,   
								 :ll_prog_riga_fat_ven,   
								 :ls_cod_tipo_det_ven_rif_bol_ven,   
								 null,
								 null,
								 null,
								 null,
								 null,
								 null,
								 null,
								 :ls_des_riferimento,   
								 0,   
								 0,   
								 0,   
								 0,  
								 0,
								 0,
								 :ls_cod_iva_rif,   
								 null,
								 null,
								 null,
								 null,
								 null,   
								 null,
								 null,
								 null,
								 null,   
								 null,   
								 null,   
								 0,   
								 0,   
								 0,   
								 0,   
								 0,   
								 0,   
								 0,   
								 0,
								 null,
								 1,
								 null,
								 null,
								 null,   
								 null,   
								 null,
								 null,
								 0,
								 0,
								 'I',
								 0,
								 0);
		
				if sqlca.sqlcode <> 0 then
					fs_errore = "Errore durante la scrittura Dettagli Bolle Vendita.~r~n" + sqlca.sqlerrtext
					close cu_righe_ordine;
					return -1
				end if
				ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10
			end if	
	
			insert into det_fat_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_fat_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 data_stock,
							 progr_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_fatturata,   
							 prezzo_vendita,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 anno_registrazione_fat,
							 num_registrazione_fat,
							 nota_dettaglio,   
							 anno_registrazione_bol_ven,   
							 num_registrazione_bol_ven,   
							 prog_riga_bol_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 fat_conversione_ven,   
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 anno_reg_ord_ven,   
							 num_reg_ord_ven,   
							 prog_riga_ord_ven,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)			
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_fat_ven,   
							 :ls_cod_tipo_det_ven,   
							 null,
							 null,
							 null,
							 null,
							 null,
							 :ls_cod_misura,   
							 :ls_cod_prodotto,   
							 :ls_des_prodotto,   
							 :ld_quan_ordine,
							 :ld_prezzo_vendita,   
							 :ld_sconto_1,   
							 :ld_sconto_2,  
							 :ld_provvigione_1,
							 :ld_provvigione_2,
							 :ls_cod_iva,   
							 :ls_cod_tipo_movimento,
							 null,
							 null,
							 null,
							 :ls_nota_dettaglio,   
							 null,
							 null,
							 null,
							 :ll_anno_commessa,   
							 :ll_num_commessa,   
							 :ls_cod_centro_costo,   
							 :ld_sconto_3,   
							 :ld_sconto_4,   
							 :ld_sconto_5,   
							 :ld_sconto_6,   
							 :ld_sconto_7,   
							 :ld_sconto_8,   
							 :ld_sconto_9,   
							 :ld_sconto_10,
							 null,
							 :ld_fat_conversione_ven,
							 null,
							 null,
							 :fl_anno_registrazione,   
							 :fl_num_registrazione,   
							 :ll_prog_riga_ord_ven,
							 :ls_cod_versione,
							 :ll_num_confezioni,
							 :ll_num_pezzi_confezione,
							 'I',
							 :ld_quantita_um,
							 :ld_prezzo_um);								 						 
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la scrittura Dettagli Fatture Vendita.~r~n" + sqlca.sqlerrtext
				close cu_righe_ordine;
				return -1
			end if
			
			//--------------------------------------------------------------------
			//SR Note_conformita_prodotto
			if uof_note_in_fattura(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven, fs_errore) < 0 then
				close cu_righe_ordine_2;
				return -1
			end if
			//--------------------------------------------------------------------
			
		loop
		close cu_righe_ordine;
		
		luo_calcolo = create uo_calcola_documento_euro
	
		if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"fat_ven",fs_errore) <> 0 then
			destroy luo_calcolo
			return -1
		end if
		
		// *****************************
		// Calcolo le spese di trasporto
		uo_spese_trasporto luo_spese_trasporto
		luo_spese_trasporto = create uo_spese_trasporto
		
		if luo_spese_trasporto.uof_calcola_spese(ll_anno_registrazione, ll_num_registrazione, "FATVEN", ref fs_errore) < 0 then
			destroy luo_spese_trasporto
			destroy luo_calcolo
			return -1
		end if
			
		// di seguito ri-calcolo il documento per includere nel calcolo le spese di trasporto
		if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"fat_ven",fs_errore) <> 0 then
			destroy luo_calcolo
			destroy luo_spese_trasporto
			return -1
		end if
		
		if ib_visualizza_spese_trasp then
			
			//valori di ritorno della funzione
			//0		SPESE TRASPORTO CONFERMATE (o non abilitate dal parametro SSP), quindi non fare null'altro
			//1		SPESE TRASPORTO ANNULLATE, quindi chiama la funzione che toglie le righe trasporto e che ricalcola il documento
			//2		SPESE TRASPORTO DA SOLE PERCENTUALI IMPONIBILE RIGHE ORDINE. quindi chiama funzione opportuna
			//NOTA BENE anche nei nei casi di ritorno 1 o 2 ci sarà un commit fatto in tal caso all'uscita di questa funzione
			li_risposta = luo_spese_trasporto.uof_apri_storico(ll_anno_registrazione, ll_num_registrazione, "FATVEN")
			
			if li_risposta = 0 then
			else
				if li_risposta = 1 then
					//--------------------------------------------------------------------------------------------------------------------------------------------
					//ANNULLAMENTO SPESE TRASPORTO
					li_risposta = luo_spese_trasporto.uof_azzera_trasporto_documento(ll_anno_registrazione, ll_num_registrazione, "FATVEN", fs_errore)
					//--------------------------------------------------------------------------------------------------------------------------------------------
				elseif li_risposta = 2 then
					//--------------------------------------------------------------------------------------------------------------------------------------------
					//CALCOLO SPESE TRASPORTO DA SOLE PERCENTUALI IMPONIBILE RIGHE ORDINE
					li_risposta = luo_spese_trasporto.uof_imposta_trasporto_solo_percentuale(ll_anno_registrazione, ll_num_registrazione, "FATVEN", fs_errore)
					//--------------------------------------------------------------------------------------------------------------------------------------------
				end if
				
				if li_risposta<0 then
					destroy luo_spese_trasporto
					destroy luo_calcolo
					return -1
				end if
			end if
		end if
		
		destroy luo_spese_trasporto
		destroy luo_calcolo
		
		fl_anno_reg_fat_ven = ll_anno_registrazione
		fl_num_reg_fat_ven = ll_num_registrazione
	end if
	//-------------------------------------------------------------------------------------------------------------------------------------
end if

return 0
end function

public function integer uof_crea_fattura (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_fat_ven, ref long fl_num_reg_fat_ven, ref string fs_errore);boolean lb_riferimento
long ll_anno_registrazione, ll_num_registrazione, &
	  ll_prog_riga_ord_ven, ll_anno_commessa, ll_num_commessa, ll_progr_stock[1], &
	  ll_prog_riga_fat_ven, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga, &
	  ll_num_confezioni, ll_num_pezzi_confezione
string ls_cod_tipo_fat_ven, ls_nota_testata, ls_cod_cliente[1], ls_cod_tipo_ord_ven, &
		 ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
		 ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione[1], ls_cod_deposito[1], &
		 ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, &
		 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
		 ls_cod_resa, ls_nota_piede, ls_cod_banca, ls_cod_tipo_det_ven, ls_cod_prodotto, &
		 ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, &
		 ls_flag_tipo_det_ven, ls_cod_tipo_analisi_fat, ls_cod_tipo_analisi_ord, &
		 ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, ls_causale_trasporto, &
		 ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, &
		 ls_num_ord_cliente, ls_cod_lotto[1], ls_cod_tipo_movimento, ls_vostro_ordine, &
		 ls_vostro_ordine_data, ls_flag_evasione, ls_lire, ls_flag_sola_iva, &
		 ls_cod_fornitore[], ls_causale_trasporto_tab,ls_cod_versione, ls_flag_abilita_rif_bol_ven, &
		 ls_cod_tipo_det_ven_rif_bol_ven, ls_des_riferimento_ord_ven, ls_flag_rif_anno_ord_ven, &
		 ls_flag_rif_num_ord_ven, ls_flag_rif_data_ord_ven, ls_des_riferimento, ls_cod_iva_rif, &
		 ls_note_dettaglio_rif, ls_db, ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det
datetime ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock[1], &
		 ldt_vostro_ordine_data
dec{6} ld_cambio_ven, ld_quan_in_evasione, ld_prezzo_vendita, ld_fat_conversione_ven, &
		 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, &
		 ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_testata, &
		 ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, &
		 ld_quan_ordine, ld_quan_evasa, ld_tot_val_evaso, ld_tot_val_ordine, &
		 ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, &
		 ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, &
		 ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, &
		 ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, &
		 ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quantita_um, ld_prezzo_um
integer li_risposta

uo_calcola_documento_euro luo_calcolo


li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
if upper(ls_db) = "MSSQL" then ls_db="SYBASE_ASE"

guo_functions.uof_get_parametro_azienda( "IRF", ref il_incremento_riga)
if isnull(il_incremento_riga) or il_incremento_riga = 0 then
	il_incremento_riga = 10
end if

lb_riferimento = true
setnull(ls_cod_fornitore[1])

select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and  
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la ricerca del tipo ordine di vendita in testata ordini di vendita.~n" + sqlca.sqlerrtext
	return -1
end if

select cod_tipo_fat_ven,  
		 cod_tipo_analisi  
into   :ls_cod_tipo_fat_ven,  
		 :ls_cod_tipo_analisi_ord
from   tab_tipi_ord_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la ricerca tipo fattura in tabella Tipi Ordini Vendita.~n" + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_tipo_fat_ven) then
	fs_errore = "Nel tipo ordine manca l'indicazione del tipo fattura di destinazione; operazione annullata "
	return -1
end if

if not isnull(ls_cod_tipo_fat_ven) then
	select con_fat_ven.num_registrazione
	into   :ll_num_registrazione
	from   con_fat_ven
	where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;

	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la lettura della tabella Controllo Fatture Vendita.~n" + sqlca.sqlerrtext
		return -1
	end if

	ll_num_registrazione ++
	
	update con_fat_ven
	set    con_fat_ven.num_registrazione = :ll_num_registrazione
	where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la scrittura sulla tabella Controllo Fatture Vendita.~n" + sqlca.sqlerrtext
		return -1
	end if

	select tes_ord_ven.cod_operatore,   
			 tes_ord_ven.data_registrazione,   
			 tes_ord_ven.cod_cliente,   
			 tes_ord_ven.cod_des_cliente,   
			 tes_ord_ven.rag_soc_1,   
			 tes_ord_ven.rag_soc_2,   
			 tes_ord_ven.indirizzo,   
			 tes_ord_ven.localita,   
			 tes_ord_ven.frazione,   
			 tes_ord_ven.cap,   
			 tes_ord_ven.provincia,   
			 tes_ord_ven.cod_deposito,   
			 tes_ord_ven.cod_ubicazione,   
			 tes_ord_ven.cod_valuta,   
			 tes_ord_ven.cambio_ven,   
			 tes_ord_ven.cod_tipo_listino_prodotto,   
			 tes_ord_ven.cod_pagamento,   
			 tes_ord_ven.sconto,   
			 tes_ord_ven.cod_agente_1,
			 tes_ord_ven.cod_agente_2,
			 tes_ord_ven.cod_banca,   
			 tes_ord_ven.num_ord_cliente,   
			 tes_ord_ven.data_ord_cliente,   
			 tes_ord_ven.cod_imballo,   
			 tes_ord_ven.aspetto_beni,
			 tes_ord_ven.peso_netto,
			 tes_ord_ven.peso_lordo,
			 tes_ord_ven.num_colli,
			 tes_ord_ven.cod_vettore,   
			 tes_ord_ven.cod_inoltro,   
			 tes_ord_ven.cod_mezzo,
			 tes_ord_ven.causale_trasporto,
			 tes_ord_ven.cod_porto,   
			 tes_ord_ven.cod_resa,   
			 tes_ord_ven.nota_testata,   
			 tes_ord_ven.nota_piede,   
			 tes_ord_ven.tot_val_evaso,
			 tes_ord_ven.tot_val_ordine,
			 tes_ord_ven.cod_banca_clien_for,
			 tes_ord_ven.flag_doc_suc_tes, 
			 tes_ord_ven.flag_doc_suc_pie			 
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_flag_doc_suc_tes, 
			 :ls_flag_doc_suc_pie			 
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione and  
			 tes_ord_ven.num_registrazione = :fl_num_registrazione;

	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la lettura Testata Ordine Vendita.~n" + sqlca.sqlerrtext
		return -1
	end if
	
	// stefanop: 18/07/2012 - l'operatore che inserire è SEMPRE l'operatore collegato ad Apice
	// ------------------------------------------------------
	ls_cod_operatore = guo_functions.uof_get_operatore_utente()
	// ------------------------------------------------------

	select tab_tipi_fat_ven.cod_tipo_analisi,
			 tab_tipi_fat_ven.causale_trasporto,
			 tab_tipi_fat_ven.flag_abilita_rif_bol_ven,
			 tab_tipi_fat_ven.cod_tipo_det_ven_rif_bol_ven,
			 tab_tipi_fat_ven.des_riferimento_ord_ven,
			 tab_tipi_fat_ven.flag_rif_anno_ord_ven,
			 tab_tipi_fat_ven.flag_rif_num_ord_ven,
			 tab_tipi_fat_ven.flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi_fat,
			 :ls_causale_trasporto,
			 :ls_flag_abilita_rif_bol_ven,
			 :ls_cod_tipo_det_ven_rif_bol_ven,
			 :ls_des_riferimento_ord_ven,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_fat_ven  
	where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Si è verificato un errore nella lettura della Tabella Tipi Fatture.~n" + sqlca.sqlerrtext
		return -1
	end if
	
	ldt_data_registrazione = datetime(today())

	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if

	if not isnull(ls_num_ord_cliente)  and ls_cod_tipo_det_ven_rif_bol_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if

//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------
	
	
	if ls_cod_tipo_det_ven_rif_bol_ven = "T" then
		ls_nota_testata = "Rif. ns. Ordine Nr. " + string(fl_anno_registrazione) + "/" + &
								string(fl_num_registrazione) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
	end if

		
	// ----- controllo lunghezza note per ASE
	if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE


	if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
		ls_causale_trasporto = ls_causale_trasporto_tab
	end if

	select parametri_azienda.stringa
	into   :ls_lire
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LIR';

	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in lettura valuta corrente da parametri_azienda.~n" + sqlca.sqlerrtext
		return -1
	end if

	select tab_pagamenti.sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if

	insert into tes_fat_ven
					(cod_azienda,   
					 anno_registrazione,   
					 num_registrazione,   
					 data_registrazione,   
					 cod_operatore,   
					 cod_tipo_fat_ven,   
					 cod_cliente,
					 cod_des_cliente,   
					 rag_soc_1,   
					 rag_soc_2,   
					 indirizzo,   
					 localita,   
					 frazione,   
					 cap,   
					 provincia,   
					 cod_deposito,   
					 cod_ubicazione,
					 cod_valuta,   
					 cambio_ven,   
					 cod_tipo_listino_prodotto,   
					 cod_pagamento,   
					 sconto,   
					 cod_agente_1,
					 cod_agente_2,
					 cod_banca,   
					 num_ord_cliente,   
					 data_ord_cliente,   
					 cod_imballo,   
					 aspetto_beni,
					 peso_netto,
					 peso_lordo,
					 num_colli,
					 cod_vettore,   
					 cod_inoltro,   
					 cod_mezzo,
					 causale_trasporto,
					 cod_porto,   
					 cod_resa,   
					 cod_documento,
					 numeratore_documento,
					 anno_documento,
					 num_documento,
					 data_fattura,
					 nota_testata,   
					 nota_piede,   
					 flag_fuori_fido,
					 flag_blocco,   
					 flag_movimenti,  
					 flag_contabilita,
					 tot_merci,
					 tot_spese_trasporto,
					 tot_spese_imballo,
					 tot_spese_bolli,
					 tot_spese_varie,
					 tot_sconto_cassa,
					 tot_sconti_commerciali,
					 imponibile_provvigioni_1,
					 imponibile_provvigioni_2,
					 tot_provvigioni_1,
					 tot_provvigioni_2,
					 importo_iva,
					 imponibile_iva,
					 importo_iva_valuta,
					 imponibile_iva_valuta,
					 tot_fattura,
					 tot_fattura_valuta,
					 flag_cambio_zero,
					 cod_banca_clien_for,
					 flag_st_note_tes,
					 flag_st_note_pie)  					 
	values      (:s_cs_xx.cod_azienda,   
					 :ll_anno_registrazione,   
					 :ll_num_registrazione,   
					 :ldt_data_registrazione,   
					 :ls_cod_operatore,   
					 :ls_cod_tipo_fat_ven,   
					 :ls_cod_cliente[1],
					 :ls_cod_des_cliente,   
					 :ls_rag_soc_1,   
					 :ls_rag_soc_2,   
					 :ls_indirizzo,   
					 :ls_localita,   
					 :ls_frazione,   
					 :ls_cap,   
					 :ls_provincia,   
					 :ls_cod_deposito[1],   
					 :ls_cod_ubicazione[1],
					 :ls_cod_valuta,   
					 :ld_cambio_ven,   
					 :ls_cod_tipo_listino_prodotto,   
					 :ls_cod_pagamento,   
					 :ld_sconto_testata,
					 :ls_cod_agente_1,
					 :ls_cod_agente_2,
					 :ls_cod_banca,   
					 :ls_num_ord_cliente,   
					 :ldt_data_ord_cliente,   
					 :ls_cod_imballo,   
					 :ls_aspetto_beni,
					 :ld_peso_netto,
					 :ld_peso_lordo,
					 :ld_num_colli,
					 :ls_cod_vettore,   
					 :ls_cod_inoltro,   
					 :ls_cod_mezzo,   
					 :ls_causale_trasporto,
					 :ls_cod_porto,   
					 :ls_cod_resa,   
					 null,
					 null,
					 0,
					 0,
					 null,
					 :ls_nota_testata,   
					 :ls_nota_piede,   
					 'N',
					 'N',   
					 'N',
					 'N',
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 'N',
					 :ls_cod_banca_clien_for,
					 'I',
					 'I');					 

	 if sqlca.sqlcode <> 0 then
	 	fs_errore = "Errore durante la scrittura Testata Fatture Vendita.~n" + sqlca.sqlerrtext
		return -1
	 end if
	 if uof_crea_nota_fissa("FAT_VEN", ll_anno_registrazione, ll_num_registrazione, fs_errore) = -1 then
		fs_errore = "Errore durante la scrittura nota fissa"
		return -1
 	 end if

	 declare cu_evas_ord_ven cursor for  
	 select  evas_ord_ven.prog_riga_ord_ven,   
				evas_ord_ven.prog_riga,   
				evas_ord_ven.cod_deposito,   
				evas_ord_ven.cod_ubicazione,   
				evas_ord_ven.cod_lotto,   
				evas_ord_ven.data_stock,   
				evas_ord_ven.prog_stock,   
				evas_ord_ven.quan_in_evasione
	from     evas_ord_ven  
	where    evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				evas_ord_ven.anno_registrazione = :fl_anno_registrazione and
				evas_ord_ven.num_registrazione = :fl_num_registrazione and
				evas_ord_ven.cod_utente = :s_cs_xx.cod_utente
	order by evas_ord_ven.prog_riga_ord_ven asc,   
				evas_ord_ven.prog_riga asc;

	open cu_evas_ord_ven;
	
	ll_prog_riga_fat_ven = 0
	do while 0 = 0
		fetch cu_evas_ord_ven into :ll_prog_riga_ord_ven,   
											:ll_prog_riga,
											:ls_cod_deposito[1],   
											:ls_cod_ubicazione[1],   
											:ls_cod_lotto[1],   
											:ldt_data_stock[1],   
											:ll_progr_stock[1],   
											:ld_quan_in_evasione;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode = -1 then
			fs_errore =  "Errore durante la lettura Evasione Ordini Vendita.~n" + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga
		
		select	det_ord_ven.cod_tipo_det_ven,   
					det_ord_ven.cod_misura,   
					det_ord_ven.cod_prodotto,   
					det_ord_ven.des_prodotto,   
					det_ord_ven.quan_ordine,
					det_ord_ven.prezzo_vendita,   
					det_ord_ven.fat_conversione_ven,   
					det_ord_ven.sconto_1,   
					det_ord_ven.sconto_2,  
					det_ord_ven.provvigione_1,
					det_ord_ven.provvigione_2,
					det_ord_ven.cod_iva,   
					det_ord_ven.quan_evasa,
					det_ord_ven.nota_dettaglio,   
					det_ord_ven.anno_commessa,
					det_ord_ven.num_commessa,
					det_ord_ven.cod_centro_costo,
					det_ord_ven.sconto_3,   
					det_ord_ven.sconto_4,   
					det_ord_ven.sconto_5,   
					det_ord_ven.sconto_6,   
					det_ord_ven.sconto_7,   
					det_ord_ven.sconto_8,   
					det_ord_ven.sconto_9,   
					det_ord_ven.sconto_10,
					det_ord_ven.cod_versione,
					det_ord_ven.num_confezioni,
					det_ord_ven.num_pezzi_confezione,
					det_ord_ven.flag_doc_suc_det,
					det_ord_ven.quantita_um,
					det_ord_ven.prezzo_um
		into		:ls_cod_tipo_det_ven, 
					:ls_cod_misura, 
					:ls_cod_prodotto, 
					:ls_des_prodotto, 
					:ld_quan_ordine,
					:ld_prezzo_vendita, 
					:ld_fat_conversione_ven, 
					:ld_sconto_1, 
					:ld_sconto_2, 
					:ld_provvigione_1, 
					:ld_provvigione_2, 
					:ls_cod_iva, 
					:ld_quan_evasa,
					:ls_nota_dettaglio, 
					:ll_anno_commessa,
					:ll_num_commessa,
					:ls_cod_centro_costo,
					:ld_sconto_3, 
					:ld_sconto_4, 
					:ld_sconto_5, 
					:ld_sconto_6, 
					:ld_sconto_7, 
					:ld_sconto_8, 
					:ld_sconto_9, 
					:ld_sconto_10,
					:ls_cod_versione,
					:ll_num_confezioni,
					:ll_num_pezzi_confezione,
					:ls_flag_doc_suc_det,
					:ld_quantita_um,
					:ld_prezzo_um
		  from 	det_ord_ven  
		 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
					det_ord_ven.num_registrazione = :fl_num_registrazione and
					det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven
		order by det_ord_ven.prog_riga_ord_ven asc;
	
		if sqlca.sqlcode = -1 then
			fs_errore =  "Errore durante la lettura Dettagli Ordini Vendita.~n" + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if
			
//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
// ricalcolo la quantità di vendita nel caso di evasioni parziali
		if ld_quan_in_evasione <> ld_quan_ordine then
			ld_quantita_um = round(ld_quan_in_evasione * ld_fat_conversione_ven, 4)
		end if
//------------------------------------------------- Modifica Nicola ---------------------------------------------
	if ls_flag_doc_suc_det = 'I' then //nota dettaglio
		select flag_doc_suc_or
		  into :ls_flag_doc_suc_det
		  from tab_tipi_det_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		
	
	
	if ls_flag_doc_suc_det = 'N' then
		setnull(ls_nota_dettaglio)
	end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------		
					
			
		select tab_tipi_det_ven.flag_tipo_det_ven,
				 tab_tipi_det_ven.cod_tipo_movimento,
				 tab_tipi_det_ven.flag_sola_iva
		into   :ls_flag_tipo_det_ven,
				 :ls_cod_tipo_movimento,
				 :ls_flag_sola_iva
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode = -1 then
			fs_errore = "Si è verificato un errore in fase di lettura tipi dettagli.~n" + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
				fs_errore = "Si è verificato un errore in fase di creazione destinazioni movimenti."
				close cu_evas_ord_ven;
				return -1
			end if

			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
				fs_errore = "Si è verificato un errore in fase di verifica destinazioni movimenti."
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if lb_riferimento and ls_flag_abilita_rif_bol_ven = "D" then
			lb_riferimento = false
			setnull(ls_note_dettaglio_rif)
			ls_des_riferimento = ls_des_riferimento_ord_ven
			if ls_flag_rif_anno_ord_ven = "S" and not isnull(fl_anno_registrazione) then
				ls_des_riferimento = ls_des_riferimento + "-" +string(fl_anno_registrazione)
			end if
			if ls_flag_rif_num_ord_ven = "S" and not isnull(fl_num_registrazione) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(fl_num_registrazione)
			end if
			if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
			end if
			if not isnull(ls_num_ord_cliente) then
				ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
				if not isnull(ldt_data_ord_cliente) then
					ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
				end if
			end if
				
			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_bol_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
				g_mb.messagebox("Generazione Bolla Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if
			
			insert into det_fat_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_fat_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 data_stock,
							 progr_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_fatturata,   
							 prezzo_vendita,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 anno_registrazione_fat,
							 num_registrazione_fat,
							 nota_dettaglio,   
							 anno_registrazione_bol_ven,   
							 num_registrazione_bol_ven,   
							 prog_riga_bol_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 fat_conversione_ven,   
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 anno_reg_ord_ven,   
							 num_reg_ord_ven,   
							 prog_riga_ord_ven,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)							 
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_fat_ven,   
							 :ls_cod_tipo_det_ven_rif_bol_ven,   
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 :ls_des_riferimento,   
							 0,   
							 0,   
							 0,   
							 0,  
							 0,
							 0,
							 :ls_cod_iva_rif,   
							 null,
							 null,
							 null,
							 null,
							 null,   
							 null,
							 null,
							 null,
							 null,   
							 null,   
							 null,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,
							 null,
							 1,
							 null,
							 null,
							 null,   
							 null,   
							 null,
							 null,
							 0,
							 0,
							 'I',
							 0,
							 0);
	
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la scrittura Dettagli Bolle Vendita.~n" + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
			ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga
		end if	

		insert into det_fat_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_fat_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 data_stock,
						 progr_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_fatturata,   
						 prezzo_vendita,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 anno_registrazione_fat,
						 num_registrazione_fat,
						 nota_dettaglio,   
						 anno_registrazione_bol_ven,   
						 num_registrazione_bol_ven,   
						 prog_riga_bol_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 fat_conversione_ven,   
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 anno_reg_ord_ven,   
						 num_reg_ord_ven,   
						 prog_riga_ord_ven,
						 cod_versione,
                   num_confezioni,
					    num_pezzi_confezione,
						 flag_st_note_det,
						 quantita_um,
						 prezzo_um)			
	  values			(:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_fat_ven,   
						 :ls_cod_tipo_det_ven,   
						 :ls_cod_deposito[1],
						 :ls_cod_ubicazione[1],
						 :ls_cod_lotto[1],
						 :ldt_data_stock[1],
						 :ll_progr_stock[1],
						 :ls_cod_misura,   
						 :ls_cod_prodotto,   
						 :ls_des_prodotto,   
						 :ld_quan_in_evasione,   
						 :ld_prezzo_vendita,   
						 :ld_sconto_1,   
						 :ld_sconto_2,  
						 :ld_provvigione_1,
						 :ld_provvigione_2,
						 :ls_cod_iva,   
						 :ls_cod_tipo_movimento,
						 null,
						 null,
						 null,
						 :ls_nota_dettaglio,   
						 null,
						 null,
						 null,
						 :ll_anno_commessa,   
						 :ll_num_commessa,   
						 :ls_cod_centro_costo,   
						 :ld_sconto_3,   
						 :ld_sconto_4,   
						 :ld_sconto_5,   
						 :ld_sconto_6,   
						 :ld_sconto_7,   
						 :ld_sconto_8,   
						 :ld_sconto_9,   
						 :ld_sconto_10,
						 null,
						 :ld_fat_conversione_ven,
						 :ll_anno_reg_des_mov,
						 :ll_num_reg_des_mov,
						 :fl_anno_registrazione,   
						 :fl_num_registrazione,   
						 :ll_prog_riga_ord_ven,
						 :ls_cod_versione,
                   :ll_num_confezioni,
					    :ll_num_pezzi_confezione,
						 'I',
						 :ld_quantita_um,
						 :ld_prezzo_um);								 						 

		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura Dettagli Fatture Vendita.~n" + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			update anag_prodotti  
				set quan_assegnata = quan_assegnata - :ld_quan_in_evasione,
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode <> 0 then
				fs_errore = "Si è verificato un errore in fase di aggiornamento magazzino.~n" + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
			
			update stock 
			set 	 quan_assegnata = quan_assegnata - :ld_quan_in_evasione,   
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione  
			where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
					 stock.cod_prodotto = :ls_cod_prodotto and 
					 stock.cod_deposito = :ls_cod_deposito[1] and 
					 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
					 stock.cod_lotto = :ls_cod_lotto[1] and 
					 stock.data_stock = :ldt_data_stock[1] and 
					 stock.prog_stock = :ll_progr_stock[1];

			if sqlca.sqlcode <> 0 then
				fs_errore = "Si è verificato un errore in fase di aggiornamento stock.~n" + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
		end if				

		ls_tabella_orig = "det_ord_ven_stat"
		ls_tabella_des = "det_fat_ven_stat"
		ls_prog_riga_doc_orig = "prog_riga_ord_ven"
		ls_prog_riga_doc_des = "prog_riga_fat_ven"

		if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven) = -1 then
			fs_errore = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
			close cu_evas_ord_ven;
			return -1
		end if	

		if ls_cod_tipo_analisi_fat <> ls_cod_tipo_analisi_ord then
			if f_crea_distribuzione(ls_cod_tipo_analisi_fat, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_fat_ven, 6) = -1 then
				fs_errore = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if ld_quan_ordine <= ld_quan_evasa + ld_quan_in_evasione then
			ls_flag_evasione = "E"
		else
			ls_flag_evasione = "P"
		end if
		
		update det_ord_ven  
			set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione,
				 quan_evasa = quan_evasa + :ld_quan_in_evasione,
				 flag_evasione = :ls_flag_evasione
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
				 det_ord_ven.num_registrazione = :fl_num_registrazione and
				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
		if sqlca.sqlcode = -1 then
			fs_errore = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Vendita.~n" + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_sola_iva = 'N' then
			ld_val_riga = ld_quan_in_evasione * ld_prezzo_vendita
			ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
			ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
			ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
			ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
			ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
			ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
			ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
			ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
			ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
			ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
			ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
			ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
			ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
			ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
			ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
			ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
			ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
			ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
			ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
			if ls_cod_valuta = ls_lire then
				ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
			else
				ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
			end if   
	
			if ls_flag_tipo_det_ven = "M" or &
				ls_flag_tipo_det_ven = "C" or &
				ls_flag_tipo_det_ven = "N" then
				ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
				ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
				ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
				ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
			elseif ls_flag_tipo_det_ven = "S" then
				ld_val_evaso = ld_val_riga_sconto_10 * -1
			else
				ld_val_evaso = ld_val_riga_sconto_10
			end if
			ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
		end if

		if ls_cod_valuta = ls_lire then
			ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
		end if   

		delete from evas_ord_ven 
		where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
						evas_ord_ven.anno_registrazione = :fl_anno_registrazione and
						evas_ord_ven.num_registrazione = :fl_num_registrazione and 
						evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
						evas_ord_ven.prog_riga = :ll_prog_riga and
						evas_ord_ven.cod_utente = :s_cs_xx.cod_utente;

		if sqlca.sqlcode = -1 then
			fs_errore = "Si è verificato un errore in fase di cancellazione Evasione Ordini.~n" + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if
	loop
	close cu_evas_ord_ven;

	if ld_tot_val_ordine <= ld_tot_val_evaso then
		ls_flag_evasione = "E"
	else
		ls_flag_evasione = "P"
	end if

//	update tes_ord_ven  
//		set tot_val_evaso = :ld_tot_val_evaso
//	 where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione and  
//			 tes_ord_ven.num_registrazione = :fl_num_registrazione;
//
//	if sqlca.sqlcode <> 0 then
//		fs_errore = "Si è verificato un errore in fase di aggiornamento Valore Evaso in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
//		return -1
//	end if
	
	if uof_calcola_stato_ordine(fl_anno_registrazione,fl_num_registrazione) = -1 then
		fs_errore = "Si è verificato un errore in fase di aggiornamento stato dell'ordine in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
		return -1
	end if

	luo_calcolo = create uo_calcola_documento_euro

	if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"fat_ven",fs_errore) <> 0 then
		destroy luo_calcolo
		return -1
	end if
	
	destroy luo_calcolo

	fl_anno_reg_fat_ven = ll_anno_registrazione
	fl_num_reg_fat_ven = ll_num_registrazione
end if

return 0
end function

public function integer uof_crea_bolla (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_bol_ven, ref long fl_num_reg_bol_ven, ref string fs_errore);boolean lb_riferimento
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_anno_commessa, ll_num_commessa, &
     ll_progr_stock[1], ll_prog_riga_bol_ven, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga, &
	  ll_num_confezioni, ll_num_pezzi_confezione
string ls_cod_tipo_ord_ven, ls_cod_tipo_bol_ven, ls_nota_testata, ls_cod_cliente[1], ls_cod_operatore, ls_rag_soc_1, &
		 ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione[1], &
		 ls_cod_deposito[1], ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, ls_cod_imballo, &
		 ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_nota_piede, ls_cod_banca, ls_cod_tipo_det_ven, &
		 ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_ven, &
		 ls_cod_tipo_analisi_bol, ls_cod_tipo_analisi_ord, ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, ls_causale_trasporto, &
		 ls_flag_riep_fat, ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ls_num_ord_cliente, &
		 ls_cod_lotto[1], ls_cod_tipo_movimento, ls_vostro_ordine, ls_vostro_ordine_data, ls_flag_evasione, ls_lire, &
		 ls_flag_sola_iva, ls_cod_fornitore[], ls_causale_trasporto_tab, ls_cod_versione, ls_cod_causale_tab, ls_flag_abilita_rif_ord_ven, &
		 ls_cod_tipo_det_ven_rif_ord_ven, ls_des_riferimento_ord_ven, ls_flag_rif_anno_ord_ven, ls_flag_rif_num_ord_ven,&
		 ls_flag_rif_data_ord_ven, ls_des_riferimento, ls_cod_iva_rif, ls_cod_causale, ls_note_dettaglio_rif, &
		 ls_db, ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det
datetime ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock[1], ldt_vostro_ordine_data
dec{6} ld_cambio_ven, ld_quan_in_evasione, ld_prezzo_vendita, ld_fat_conversione_ven, ld_sconto_1, ld_sconto_2, &
       ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		 ld_sconto_testata, ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, ld_quan_ordine, &
		 ld_quan_evasa, ld_tot_val_evaso, ld_tot_val_ordine, ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, ld_val_riga_sconto_4, &
		 ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10, &
		 ld_val_sconto_testata, ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quantita_um, ld_prezzo_um
integer li_risposta

uo_calcola_documento_euro luo_calcolo


li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
if upper(ls_db) = "MSSQL" then ls_db="SYBASE_ASE"

ll_anno_registrazione = f_anno_esercizio()

lb_riferimento = true
setnull(ls_cod_fornitore[1])

select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in fase di ricerca ordine:~n" + sqlca.sqlerrtext
	return -1
end if

select cod_tipo_bol_ven,  
		 cod_tipo_analisi  
into   :ls_cod_tipo_bol_ven,  
		 :ls_cod_tipo_analisi_ord
from   tab_tipi_ord_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la lettura della tabella Tipi Ordini Vendita.~n" + sqlca.sqlerrtext
	return -1
end if

if not isnull(ls_cod_tipo_bol_ven) then
	select con_bol_ven.num_registrazione
	into   :ll_num_registrazione
	from   con_bol_ven
	where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;

	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la lettura della tabella Controllo Bolle Vendita.~n" + sqlca.sqlerrtext
		return -1
	end if

	ll_num_registrazione ++
	
	update con_bol_ven
	set    con_bol_ven.num_registrazione = :ll_num_registrazione
	where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la scrittura sulla tabella Controllo Bolle Vendita.~n" + sqlca.sqlerrtext
		return -1
	end if

	select tes_ord_ven.cod_operatore,   
			 tes_ord_ven.data_registrazione,   
			 tes_ord_ven.cod_cliente,   
			 tes_ord_ven.cod_des_cliente,   
			 tes_ord_ven.rag_soc_1,   
			 tes_ord_ven.rag_soc_2,   
			 tes_ord_ven.indirizzo,   
			 tes_ord_ven.localita,   
			 tes_ord_ven.frazione,   
			 tes_ord_ven.cap,   
			 tes_ord_ven.provincia,   
			 tes_ord_ven.cod_deposito,   
			 tes_ord_ven.cod_ubicazione,   
			 tes_ord_ven.cod_valuta,   
			 tes_ord_ven.cambio_ven,   
			 tes_ord_ven.cod_tipo_listino_prodotto,   
			 tes_ord_ven.cod_pagamento,   
			 tes_ord_ven.sconto,   
			 tes_ord_ven.cod_agente_1,
			 tes_ord_ven.cod_agente_2,
			 tes_ord_ven.cod_banca,   
			 tes_ord_ven.num_ord_cliente,   
			 tes_ord_ven.data_ord_cliente,   
			 tes_ord_ven.cod_imballo,   
			 tes_ord_ven.aspetto_beni,
			 tes_ord_ven.peso_netto,
			 tes_ord_ven.peso_lordo,
			 tes_ord_ven.num_colli,
			 tes_ord_ven.cod_vettore,   
			 tes_ord_ven.cod_inoltro,   
			 tes_ord_ven.cod_mezzo,
			 tes_ord_ven.causale_trasporto,
			 tes_ord_ven.cod_porto,   
			 tes_ord_ven.cod_resa,   
			 tes_ord_ven.nota_testata,   
			 tes_ord_ven.nota_piede,   
			 tes_ord_ven.flag_riep_fat,
			 tes_ord_ven.tot_val_evaso,
			 tes_ord_ven.tot_val_ordine,
			 tes_ord_ven.cod_banca_clien_for,
			 tes_ord_ven.cod_causale,
			 tes_ord_ven.flag_doc_suc_tes, 
			 tes_ord_ven.flag_doc_suc_pie			 
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ls_flag_riep_fat,
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_cod_causale,
			 :ls_flag_doc_suc_tes, 
			 :ls_flag_doc_suc_pie			 
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione and  
			 tes_ord_ven.num_registrazione = :fl_num_registrazione;

	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la lettura Testata Ordine Vendita.~n" + sqlca.sqlerrtext
		return -1
	end if
	
	// stefanop: 18/07/2012 - l'operatore che inserire è SEMPRE l'operatore collegato ad Apice
	// ------------------------------------------------------
	ls_cod_operatore = guo_functions.uof_get_operatore_utente()
	// ------------------------------------------------------

	select tab_tipi_bol_ven.cod_tipo_analisi,
			 tab_tipi_bol_ven.causale_trasporto,
			 tab_tipi_bol_ven.cod_causale,
			 tab_tipi_bol_ven.flag_abilita_rif_ord_ven,
			 tab_tipi_bol_ven.cod_tipo_det_ven_rif_ord_ven,
			 tab_tipi_bol_ven.des_riferimento_ord_ven,
			 tab_tipi_bol_ven.flag_rif_anno_ord_ven,
			 tab_tipi_bol_ven.flag_rif_num_ord_ven,
			 tab_tipi_bol_ven.flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi_bol,
			 :ls_causale_trasporto_tab,
			 :ls_cod_causale_tab,
			 :ls_flag_abilita_rif_ord_ven,
			 :ls_cod_tipo_det_ven_rif_ord_ven,
			 :ls_des_riferimento_ord_ven,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_bol_ven  
	where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Si è verificato un errore nella lettura della Tabella Tipi Bolle.~n" + sqlca.sqlerrtext
		return -1
	end if


	ldt_data_registrazione = datetime(today())

	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if

	if not isnull(ls_num_ord_cliente) and ls_cod_tipo_det_ven_rif_ord_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if

//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------


	if ls_cod_tipo_det_ven_rif_ord_ven = "T" then
		ls_nota_testata = ls_des_riferimento_ord_ven + string(fl_anno_registrazione) + "/" + &
								string(fl_num_registrazione) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
	end if
	
	// ----- controllo lunghezza note per ASE
	if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE

	
	
	if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
		ls_causale_trasporto = ls_causale_trasporto_tab
	end if
	if isnull(ls_cod_causale) then
		ls_cod_causale = ls_cod_causale_tab
	end if
	select parametri_azienda.stringa
	into   :ls_lire
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LIR';

	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in lettura valuta corrente da parametri_azienda.~n" + sqlca.sqlerrtext
		return -1
	end if

	select tab_pagamenti.sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if

	insert into tes_bol_ven
					(cod_azienda,   
					 anno_registrazione,   
					 num_registrazione,   
					 data_registrazione,   
					 cod_operatore,   
					 cod_tipo_bol_ven,   
					 cod_cliente,
					 cod_fornitore,
					 cod_des_cliente,   
					 cod_fil_fornitore,
					 rag_soc_1,   
					 rag_soc_2,   
					 indirizzo,   
					 localita,   
					 frazione,   
					 cap,   
					 provincia,   
					 cod_deposito,   
					 cod_deposito_tras,
					 cod_ubicazione,
					 cod_valuta,   
					 cambio_ven,   
					 cod_tipo_listino_prodotto,   
					 cod_pagamento,   
					 sconto,   
					 cod_agente_1,
					 cod_agente_2,
					 cod_banca,   
					 num_ord_cliente,   
					 data_ord_cliente,   
					 cod_imballo,   
					 aspetto_beni,
					 peso_netto,
					 peso_lordo,
					 num_colli,
					 cod_vettore,   
					 cod_inoltro,   
					 cod_mezzo,
					 causale_trasporto,
					 cod_porto,   
					 cod_resa,   
					 cod_documento,
					 numeratore_documento,
					 anno_documento,
					 num_documento,
					 data_bolla,
					 nota_testata,   
					 nota_piede,   
					 flag_fuori_fido,
					 flag_blocco,   
					 flag_movimenti,  
					 flag_riep_fat,
					 cod_banca_clien_for,
					 flag_gen_fat,
					 cod_causale,
					 flag_doc_suc_tes,
					 flag_st_note_tes,
					 flag_doc_suc_pie,
					 flag_st_note_pie)  					 
	values      (:s_cs_xx.cod_azienda,   
					 :ll_anno_registrazione,   
					 :ll_num_registrazione,   
					 :ldt_data_registrazione,   
					 :ls_cod_operatore,   
					 :ls_cod_tipo_bol_ven,   
					 :ls_cod_cliente[1],
					 null,
					 :ls_cod_des_cliente,   
					 null,
					 :ls_rag_soc_1,   
					 :ls_rag_soc_2,   
					 :ls_indirizzo,   
					 :ls_localita,   
					 :ls_frazione,   
					 :ls_cap,   
					 :ls_provincia,   
					 :ls_cod_deposito[1],   
					 null,
					 :ls_cod_ubicazione[1],
					 :ls_cod_valuta,   
					 :ld_cambio_ven,   
					 :ls_cod_tipo_listino_prodotto,   
					 :ls_cod_pagamento,   
					 :ld_sconto_testata,
					 :ls_cod_agente_1,
					 :ls_cod_agente_2,
					 :ls_cod_banca,   
					 :ls_num_ord_cliente,   
					 :ldt_data_ord_cliente,   
					 :ls_cod_imballo,   
					 :ls_aspetto_beni,
					 :ld_peso_netto,
					 :ld_peso_lordo,
					 :ld_num_colli,
					 :ls_cod_vettore,   
					 :ls_cod_inoltro,   
					 :ls_cod_mezzo,   
					 :ls_causale_trasporto,
					 :ls_cod_porto,   
					 :ls_cod_resa,   
					 null,
					 null,
					 0,
					 0,
					 null,
					 :ls_nota_testata,   
					 :ls_nota_piede,   
					 'N',
					 'N',   
					 'N',
					 :ls_flag_riep_fat,
					 :ls_cod_banca_clien_for,
					 'N',
					 :ls_cod_causale,
					 'I',
					 'I',
					 'I',
					 'I');					 

	 if sqlca.sqlcode <> 0 then
		 fs_errore = "Errore durante la scrittura Testata Bolle Vendita.~n" + sqlca.sqlerrtext
		 return -1
	 end if
	 
	 if uof_crea_nota_fissa("BOL_VEN", ll_anno_registrazione, ll_num_registrazione, fs_errore) = -1 then return -1

	 declare cu_evas_ord_ven cursor for  
	 select  evas_ord_ven.prog_riga_ord_ven,   
				evas_ord_ven.prog_riga,   
				evas_ord_ven.cod_deposito,   
				evas_ord_ven.cod_ubicazione,   
				evas_ord_ven.cod_lotto,   
				evas_ord_ven.data_stock,   
				evas_ord_ven.prog_stock,   
				evas_ord_ven.quan_in_evasione
	from     evas_ord_ven  
	where    evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				evas_ord_ven.anno_registrazione = :fl_anno_registrazione and
				evas_ord_ven.num_registrazione = :fl_num_registrazione and
				evas_ord_ven.cod_utente = :s_cs_xx.cod_utente
	order by evas_ord_ven.prog_riga_ord_ven asc,   
				evas_ord_ven.prog_riga asc;

	open cu_evas_ord_ven;
	
	lb_riferimento = true
	ll_prog_riga_bol_ven = 0
	do while 0 = 0
		fetch cu_evas_ord_ven into :ll_prog_riga_ord_ven,   
											:ll_prog_riga,
											:ls_cod_deposito[1],   
											:ls_cod_ubicazione[1],   
											:ls_cod_lotto[1],   
											:ldt_data_stock[1],   
											:ll_progr_stock[1],   
											:ld_quan_in_evasione;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode = -1 then
			fs_errore =  "Errore durante la lettura Evasione Ordini Vendita.~n" + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		
		select	det_ord_ven.cod_tipo_det_ven,   
					det_ord_ven.cod_misura,   
					det_ord_ven.cod_prodotto,   
					det_ord_ven.des_prodotto,   
					det_ord_ven.quan_ordine,
					det_ord_ven.prezzo_vendita,   
					det_ord_ven.fat_conversione_ven,   
					det_ord_ven.sconto_1,   
					det_ord_ven.sconto_2,  
					det_ord_ven.provvigione_1,
					det_ord_ven.provvigione_2,
					det_ord_ven.cod_iva,   
					det_ord_ven.quan_evasa,
					det_ord_ven.nota_dettaglio,   
					det_ord_ven.anno_commessa,
					det_ord_ven.num_commessa,
					det_ord_ven.cod_centro_costo,
					det_ord_ven.sconto_3,   
					det_ord_ven.sconto_4,   
					det_ord_ven.sconto_5,   
					det_ord_ven.sconto_6,   
					det_ord_ven.sconto_7,   
					det_ord_ven.sconto_8,   
					det_ord_ven.sconto_9,   
					det_ord_ven.sconto_10,
					det_ord_ven.cod_versione,
					det_ord_ven.num_confezioni,
					det_ord_ven.num_pezzi_confezione,
					det_ord_ven.flag_doc_suc_det,
					det_ord_ven.quantita_um,
					det_ord_ven.prezzo_um
		into		:ls_cod_tipo_det_ven, 
					:ls_cod_misura, 
					:ls_cod_prodotto, 
					:ls_des_prodotto, 
					:ld_quan_ordine,
					:ld_prezzo_vendita, 
					:ld_fat_conversione_ven, 
					:ld_sconto_1, 
					:ld_sconto_2, 
					:ld_provvigione_1, 
					:ld_provvigione_2, 
					:ls_cod_iva, 
					:ld_quan_evasa,
					:ls_nota_dettaglio, 
					:ll_anno_commessa,
					:ll_num_commessa,
					:ls_cod_centro_costo,
					:ld_sconto_3, 
					:ld_sconto_4, 
					:ld_sconto_5, 
					:ld_sconto_6, 
					:ld_sconto_7, 
					:ld_sconto_8, 
					:ld_sconto_9, 
					:ld_sconto_10,
					:ls_cod_versione,
					:ll_num_confezioni,
					:ll_num_pezzi_confezione,
					:ls_flag_doc_suc_det,
					:ld_quantita_um,
					:ld_prezzo_um
		  from 	det_ord_ven  
		 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
					det_ord_ven.num_registrazione = :fl_num_registrazione and
					det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven
		order by det_ord_ven.prog_riga_ord_ven asc;
	
		if sqlca.sqlcode = -1 then
			fs_errore =  "Errore durante la lettura Dettagli Ordini Vendita.~n" + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if
//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
// ricalcolo la quantità di vendita nel caso di evasioni parziali
		if ld_quan_in_evasione <> ld_quan_ordine then
			ld_quantita_um = round(ld_quan_in_evasione * ld_fat_conversione_ven, 4)
		end if
			
//------------------------------------------------- Modifica Nicola ---------------------------------------------
	if ls_flag_doc_suc_det = 'I' then //nota dettaglio
		select flag_doc_suc_or
		  into :ls_flag_doc_suc_det
		  from tab_tipi_det_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		
	
	
	if ls_flag_doc_suc_det = 'N' then
		setnull(ls_nota_dettaglio)
	end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------		
					
			
		select tab_tipi_det_ven.flag_tipo_det_ven,
				 tab_tipi_det_ven.cod_tipo_movimento,
				 tab_tipi_det_ven.flag_sola_iva
		into   :ls_flag_tipo_det_ven,
				 :ls_cod_tipo_movimento,
				 :ls_flag_sola_iva
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode = -1 then
			fs_errore = "Si è verificato un errore in fase di lettura tipi dettagli.~n" + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
				fs_errore = "Si è verificato un errore in fase di creazione destinazioni movimenti."
				close cu_evas_ord_ven;
				return -1
			end if

			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
				fs_errore = "Si è verificato un errore in fase di verifica destinazioni movimenti."
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if lb_riferimento and ls_flag_abilita_rif_ord_ven = "D" then
			lb_riferimento = false
			setnull(ls_note_dettaglio_rif)
			ls_des_riferimento = ls_des_riferimento_ord_ven
			if ls_flag_rif_anno_ord_ven = "S" and not isnull(fl_anno_registrazione) then
				ls_des_riferimento = ls_des_riferimento + "-" +string(fl_anno_registrazione)
			end if
			if ls_flag_rif_num_ord_ven = "S" and not isnull(fl_num_registrazione) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(fl_num_registrazione)
			end if
			if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
			end if
			if not isnull(ls_num_ord_cliente) then
				ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
				if not isnull(ldt_data_ord_cliente) then
					ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
				end if
			end if

			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_ord_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
				g_mb.messagebox("Generazione Bolla Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if
			
			insert into det_bol_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_bol_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 progr_stock,
							 data_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_consegnata,   
							 prezzo_vendita,   
							 fat_conversione_ven,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 nota_dettaglio,   
							 anno_registrazione_ord_ven,   
							 num_registrazione_ord_ven,   
							 prog_riga_ord_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 anno_reg_bol_acq,
							 num_reg_bol_acq,
							 prog_riga_bol_acq,
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_doc_suc_det,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)							 
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_bol_ven,   
							 :ls_cod_tipo_det_ven_rif_ord_ven,   
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,   
							 null,   
							 :ls_des_riferimento,   
							 0,   
							 0,   
							 1,   
							 0,   
							 0,  
							 0,
							 0,
							 :ls_cod_iva_rif,   
							 null,
							 null,
							 :ls_note_dettaglio_rif,   
							 null,   
							 null,   
							 null,
							 null,   
							 null,   
							 null,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 0,
							 0,
							 'I',
							 'I',
							 0,
							 0);							 
	
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la scrittura Dettagli Bolle Vendita.~n" + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
			ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		end if	

		insert into det_bol_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_bol_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 progr_stock,
						 data_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_consegnata,   
						 prezzo_vendita,   
						 fat_conversione_ven,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 nota_dettaglio,   
						 anno_registrazione_ord_ven,   
						 num_registrazione_ord_ven,   
						 prog_riga_ord_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 anno_reg_bol_acq,
						 num_reg_bol_acq,
						 prog_riga_bol_acq,
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 cod_versione,
						 num_confezioni,
						 num_pezzi_confezione,
						 flag_doc_suc_det,
						 flag_st_note_det,
						 quantita_um,
						 prezzo_um)
	  values			(:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_bol_ven,   
						 :ls_cod_tipo_det_ven,   
						 :ls_cod_deposito[1],
						 :ls_cod_ubicazione[1],
						 :ls_cod_lotto[1],
						 :ll_progr_stock[1],
						 :ldt_data_stock[1],
						 :ls_cod_misura,   
						 :ls_cod_prodotto,   
						 :ls_des_prodotto,   
						 :ld_quan_in_evasione,   
						 :ld_prezzo_vendita,   
						 :ld_fat_conversione_ven,   
						 :ld_sconto_1,   
						 :ld_sconto_2,  
						 :ld_provvigione_1,
						 :ld_provvigione_2,
						 :ls_cod_iva,   
						 :ls_cod_tipo_movimento,
						 null,
						 :ls_nota_dettaglio,   
						 :fl_anno_registrazione,   
						 :fl_num_registrazione,   
						 :ll_prog_riga_ord_ven,
						 :ll_anno_commessa,   
						 :ll_num_commessa,   
						 :ls_cod_centro_costo,   
						 :ld_sconto_3,   
						 :ld_sconto_4,   
						 :ld_sconto_5,   
						 :ld_sconto_6,   
						 :ld_sconto_7,   
						 :ld_sconto_8,   
						 :ld_sconto_9,   
						 :ld_sconto_10,
						 null,
						 null,
						 null,
						 null,
						 :ll_anno_reg_des_mov,
						 :ll_num_reg_des_mov,
						 :ls_cod_versione,
						 :ll_num_confezioni,
						 :ll_num_pezzi_confezione,
						 'I',
						 'I',
						 :ld_quantita_um,
						 :ld_prezzo_um);							 						 
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura Dettagli Bolle Vendita.~n" + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			update anag_prodotti  
				set quan_assegnata = quan_assegnata - :ld_quan_in_evasione,
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode <> 0 then
				fs_errore = "Si è verificato un errore in fase di aggiornamento magazzino.~n" + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
			
			update stock 
			set 	 quan_assegnata = quan_assegnata - :ld_quan_in_evasione,   
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione  
			where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
					 stock.cod_prodotto = :ls_cod_prodotto and 
					 stock.cod_deposito = :ls_cod_deposito[1] and 
					 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
					 stock.cod_lotto = :ls_cod_lotto[1] and 
					 stock.data_stock = :ldt_data_stock[1] and 
					 stock.prog_stock = :ll_progr_stock[1];

			if sqlca.sqlcode <> 0 then
				fs_errore = "Si è verificato un errore in fase di aggiornamento stock.~n" + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
		end if				

		ls_tabella_orig = "det_ord_ven_stat"
		ls_tabella_des = "det_bol_ven_stat"
		ls_prog_riga_doc_orig = "prog_riga_ord_ven"
		ls_prog_riga_doc_des = "prog_riga_bol_ven"

		if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven) = -1 then
			fs_errore = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
			close cu_evas_ord_ven;
			return -1
		end if	

		if ls_cod_tipo_analisi_bol <> ls_cod_tipo_analisi_ord then
			if f_crea_distribuzione(ls_cod_tipo_analisi_bol, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_bol_ven, 5) = -1 then
				fs_errore = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if ld_quan_ordine <= ld_quan_evasa + ld_quan_in_evasione then
			ls_flag_evasione = "E"
		else
			ls_flag_evasione = "P"
		end if
		
		update det_ord_ven  
			set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione,
				 quan_evasa = quan_evasa + :ld_quan_in_evasione,
				 flag_evasione = :ls_flag_evasione
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
				 det_ord_ven.num_registrazione = :fl_num_registrazione and
				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
		if sqlca.sqlcode = -1 then
			fs_errore = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Vendita.~n" + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_sola_iva = 'N' then
			ld_val_riga = ld_quan_in_evasione * ld_prezzo_vendita
			ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
			ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
			ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
			ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
			ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
			ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
			ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
			ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
			ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
			ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
			ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
			ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
			ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
			ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
			ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
			ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
			ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
			ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
			ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
			if ls_cod_valuta = ls_lire then
				ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
			else
				ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
			end if   
	
			if ls_flag_tipo_det_ven = "M" or &
				ls_flag_tipo_det_ven = "C" or &
				ls_flag_tipo_det_ven = "N" then
				ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
				ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
				ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
				ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
			elseif ls_flag_tipo_det_ven = "S" then
				ld_val_evaso = ld_val_riga_sconto_10 * -1
			else
				ld_val_evaso = ld_val_riga_sconto_10
			end if
			ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
		end if

		if ls_cod_valuta = ls_lire then
			ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
		end if   

		delete from evas_ord_ven 
		where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
						evas_ord_ven.anno_registrazione = :fl_anno_registrazione and
						evas_ord_ven.num_registrazione = :fl_num_registrazione and 
						evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
						evas_ord_ven.prog_riga = :ll_prog_riga and
						evas_ord_ven.cod_utente = :s_cs_xx.cod_utente;

		if sqlca.sqlcode = -1 then
			fs_errore = "Si è verificato un errore in fase di cancellazione Evasione Ordini.~n" + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if
	loop
	close cu_evas_ord_ven;

	if ld_tot_val_ordine <= ld_tot_val_evaso then
		ls_flag_evasione = "E"
	else
		ls_flag_evasione = "P"
	end if

//	update tes_ord_ven  
//		set tot_val_evaso = :ld_tot_val_evaso
//	 where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione and  
//			 tes_ord_ven.num_registrazione = :fl_num_registrazione;
//
//	if sqlca.sqlcode <> 0 then
//		fs_errore = "Si è verificato un errore in fase di aggiornamento Valore Evaso in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
//		return -1
//	end if
	
	if uof_calcola_stato_ordine(fl_anno_registrazione,fl_num_registrazione) = -1 then
		fs_errore = "Si è verificato un errore in fase di aggiornamento stato dell'ordine in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
		return -1
	end if
	
	luo_calcolo = create uo_calcola_documento_euro

	if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_ven",fs_errore) <> 0 then
		destroy luo_calcolo
		return -1
	end if
	
	destroy luo_calcolo
	
	fl_anno_reg_bol_ven = ll_anno_registrazione
	fl_num_reg_bol_ven = ll_num_registrazione
end if

return 0
end function

public function integer uof_crea_aggiungi_fattura (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_fat_ven, ref long fl_num_reg_fat_ven, ref string fs_errore);boolean lb_riferimento, lb_aggiungi=false
string ls_cod_tipo_fat_ven, ls_nota_testata, ls_cod_cliente[1], ls_cod_tipo_ord_ven, ls_cod_operatore, ls_rag_soc_1, &
		 ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione[1],&
		 ls_cod_deposito[1], ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, &
		 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_nota_piede, ls_cod_banca, &
		 ls_cod_tipo_det_ven, ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, &
		 ls_flag_tipo_det_ven, ls_cod_tipo_analisi_fat, ls_cod_tipo_analisi_ord, ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, &
		 ls_causale_trasporto, ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ls_num_ord_cliente, &
		 ls_cod_lotto[1], ls_cod_tipo_movimento, ls_vostro_ordine, ls_vostro_ordine_data, ls_flag_evasione, ls_lire, ls_flag_sola_iva, &
		 ls_cod_fornitore[], ls_causale_trasporto_tab,ls_cod_versione, ls_flag_abilita_rif_bol_ven, ls_cod_tipo_det_ven_rif_bol_ven, &
		 ls_des_riferimento_ord_ven, ls_flag_rif_anno_ord_ven, ls_flag_rif_num_ord_ven, ls_flag_rif_data_ord_ven, ls_des_riferimento, &
		 ls_cod_iva_rif, ls_note_dettaglio_rif, ls_db, ls_cod_tipo_fat_ven_dest, ls_cod_causale, ls_nota_piede_fattura, &
		 ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det, ls_rif_packing_list, &
		 ls_cod_tipo_det_ven_new, ls_cod_tipo_det_ven_ord, ls_cod_tipo_movimento_ord, ls_cod_tipo_movimento_new
integer li_risposta
long  ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_anno_commessa, ll_num_commessa, &
		ll_progr_stock[1], ll_prog_riga_fat_ven, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga, ll_num_confezioni, &
		ll_num_pezzi_confezione
dec{6} ld_cambio_ven, ld_quan_in_evasione, ld_prezzo_vendita, ld_fat_conversione_ven, ld_sconto_1, ld_sconto_2, ld_sconto_3, &
       ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_testata, &
		 ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, ld_quan_ordine, ld_quan_evasa, &
		 ld_tot_val_evaso, ld_tot_val_ordine, ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, ld_val_riga_sconto_4, &
		 ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10, &
		 ld_val_sconto_testata, ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quantita_um, ld_prezzo_um
datetime ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock[1], &
		 ldt_vostro_ordine_data, ldt_ora_inizio_trasporto

uo_calcola_documento_euro luo_calcolo


li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
if upper(ls_db) = "MSSQL" then ls_db="SYBASE_ASE"

ll_anno_registrazione = f_anno_esercizio()

guo_functions.uof_get_parametro_azienda( "IRF", ref il_incremento_riga)
if isnull(il_incremento_riga) or il_incremento_riga = 0 then
	il_incremento_riga = 10
end if

lb_riferimento = true
setnull(ls_cod_fornitore[1])
if not isnull(fl_anno_reg_fat_ven) and fl_anno_reg_fat_ven > 0 then lb_aggiungi = true
if lb_aggiungi and (fl_num_reg_fat_ven = 0 or isnull(fl_num_reg_fat_ven)) then
	fs_errore = "Manca l'indicazione del numero di fattura di destinazione"
	return -1
end if

select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and  
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la ricerca del tipo ordine di vendita in testata ordini di vendita.Dettaglio errore: " + sqlca.sqlerrtext
	return -1
end if

select cod_tipo_fat_ven, cod_tipo_analisi  
into :ls_cod_tipo_fat_ven, :ls_cod_tipo_analisi_ord
from tab_tipi_ord_ven  
where 
	cod_azienda = :s_cs_xx.cod_azienda and  
	cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la ricerca tipo fattura in tabella Tipi Ordini Vendita.Dettaglio errore: " + sqlca.sqlerrtext
	return -1
end if
	
// stefanop 08/06/2010: non carico il tipo bol ven se impostato: progetto 140
if isnull(this.is_cod_tipo_fat_ven) or this.is_cod_tipo_fat_ven = "" then
	// lascio quello di default dell'applicazione, 99% dei casi
	this.ib_change_det_ven = false
else
	
	if this.ib_change_det_ven then
		// recupero il dettaglio orignale che sarà sostituito con quello nuovo
		select cod_tipo_det_ven
		into :ls_cod_tipo_det_ven_ord
		from tab_tipi_fat_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
			
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante il recupero del dettaglio fattura.~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		// recupero il nuovo dettaglio
		select cod_tipo_det_ven
		into :ls_cod_tipo_det_ven_new
		from tab_tipi_fat_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_fat_ven = :is_cod_tipo_fat_ven;
			
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante il recupero del nuovo dettaglio bolla"
			return -1
		end if
	end if
	
	// MOV_MAG
	if this.ib_change_mov then
		// carico movimento default, mi serve per il confronto
		select cod_tipo_movimento
		into :ls_cod_tipo_movimento_ord
		from tab_tipi_det_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and 
			cod_tipo_det_ven = :ls_cod_tipo_det_ven_ord;
			
		if sqlca.sqlcode <> 0 then 
			fs_errore = "Errore durante la ricerca del tipo movimento."
			return -1
		end if
		
		// carico il nuovo movimento di magazzioni che sarà inserito
		select cod_tipo_movimento
		into :ls_cod_tipo_movimento_new
		from tab_tipi_det_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_det_ven = :ls_cod_tipo_det_ven_new;
			
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la ricerca del nuovo tipo movimento."
			return -1
		end if
	end if
	
	// la variabile viene impostata dalla finestra w_richiesta_dati_bolla
	ls_cod_tipo_fat_ven = this.is_cod_tipo_fat_ven
end if
// ----

if isnull(ls_cod_tipo_fat_ven) then
	fs_errore = "Nel tipo ordine manca l'indicazione del tipo fattura di destinazione; operazione annullata "
	return -1
end if
if lb_aggiungi then
	select cod_tipo_fat_ven
	into   :ls_cod_tipo_fat_ven_dest
	from   tes_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :fl_anno_reg_fat_ven and
			 num_registrazione = :fl_num_reg_fat_ven;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in ricerca tipo fattura di vendita in fattura di destinazione. Dettaglio: " + sqlca.sqlerrtext
		return -1
	end if
	
	if ls_cod_tipo_fat_ven <> ls_cod_tipo_fat_ven_dest then
		fs_errore = "Il tipo fattura deve coincidere con il tipo fattura di destinazione indicato nel tipo ordine: controllare il tipo fattura di destinazione nella tabella tipi ordini"
		return -1
	end if
end if

if not isnull(ls_cod_tipo_fat_ven) then
	select tes_ord_ven.cod_operatore,   
			 tes_ord_ven.data_registrazione,   
			 tes_ord_ven.cod_cliente,   
			 tes_ord_ven.cod_des_cliente,   
			 tes_ord_ven.rag_soc_1,   
			 tes_ord_ven.rag_soc_2,   
			 tes_ord_ven.indirizzo,   
			 tes_ord_ven.localita,   
			 tes_ord_ven.frazione,   
			 tes_ord_ven.cap,   
			 tes_ord_ven.provincia,   
			 tes_ord_ven.cod_deposito,   
			 tes_ord_ven.cod_ubicazione,   
			 tes_ord_ven.cod_valuta,   
			 tes_ord_ven.cambio_ven,   
			 tes_ord_ven.cod_tipo_listino_prodotto,   
			 tes_ord_ven.cod_pagamento,   
			 tes_ord_ven.sconto,   
			 tes_ord_ven.cod_agente_1,
			 tes_ord_ven.cod_agente_2,
			 tes_ord_ven.cod_banca,   
			 tes_ord_ven.num_ord_cliente,   
			 tes_ord_ven.data_ord_cliente,   
			 tes_ord_ven.cod_imballo,   
			 tes_ord_ven.aspetto_beni,
			 tes_ord_ven.peso_netto,
			 tes_ord_ven.peso_lordo,
			 tes_ord_ven.num_colli,
			 tes_ord_ven.cod_vettore,   
			 tes_ord_ven.cod_inoltro,   
			 tes_ord_ven.cod_mezzo,
			 tes_ord_ven.causale_trasporto,
			 tes_ord_ven.cod_porto,   
			 tes_ord_ven.cod_resa,   
			 tes_ord_ven.nota_testata,   
			 tes_ord_ven.nota_piede,   
			 tes_ord_ven.tot_val_evaso,
			 tes_ord_ven.tot_val_ordine,
			 tes_ord_ven.cod_banca_clien_for,
			 tes_ord_ven.cod_causale,
			 tes_ord_ven.flag_doc_suc_tes, 
			 tes_ord_ven.flag_doc_suc_pie			 
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_cod_causale,
			 :ls_flag_doc_suc_tes, 
			 :ls_flag_doc_suc_pie			 
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione and  
			 tes_ord_ven.num_registrazione = :fl_num_registrazione;

	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la lettura Testata Ordine Vendita.Dettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
	
	// stefanop: 18/07/2012 - l'operatore che inserire è SEMPRE l'operatore collegato ad Apice
	// ------------------------------------------------------
	ls_cod_operatore = guo_functions.uof_get_operatore_utente()
	// ------------------------------------------------------

	select tab_tipi_fat_ven.cod_tipo_analisi,
			 tab_tipi_fat_ven.causale_trasporto,
			 tab_tipi_fat_ven.flag_abilita_rif_bol_ven,
			 tab_tipi_fat_ven.cod_tipo_det_ven_rif_bol_ven,
			 tab_tipi_fat_ven.des_riferimento_ord_ven,
			 tab_tipi_fat_ven.flag_rif_anno_ord_ven,
			 tab_tipi_fat_ven.flag_rif_num_ord_ven,
			 tab_tipi_fat_ven.flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi_fat,
			 :ls_causale_trasporto,
			 :ls_flag_abilita_rif_bol_ven,
			 :ls_cod_tipo_det_ven_rif_bol_ven,
			 :ls_des_riferimento_ord_ven,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_fat_ven  
	where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Si è verificato un errore nella lettura della Tabella Tipi Fatture.Dettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
	
	ldt_data_registrazione = datetime(today())

	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if

	if not isnull(ls_num_ord_cliente)  and ls_cod_tipo_det_ven_rif_bol_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if

//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------	
	
	if ls_cod_tipo_det_ven_rif_bol_ven = "T" then
		if len(is_rif_packing_list) > 0 and not isnull(is_rif_packing_list) then ls_rif_packing_list = is_rif_packing_list + "~r~n"
		ls_nota_testata = ls_rif_packing_list + "Rif. ns. Ordine Nr. " + string(fl_anno_registrazione) + "/" + &
								string(fl_num_registrazione) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
	end if

		
	// ----- controllo lunghezza note per ASE
	if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE


	if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
		ls_causale_trasporto = ls_causale_trasporto_tab
	end if

	select parametri_azienda.stringa
	into   :ls_lire
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LIR';

	if sqlca.sqlcode <> 0 then
		fs_errore = "Configurare il codice valuta per le Lire Italiane.Dettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if

	select tab_pagamenti.sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if
	if lb_aggiungi then
		ldt_ora_inizio_trasporto = datetime(date("01/01/1900"), it_ora_inizio_trasporto)
		ll_num_registrazione = fl_num_reg_fat_ven
		
		select nota_piede
		into   :ls_nota_piede_fattura
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_reg_fat_ven and
		       num_registrazione = :fl_num_reg_fat_ven;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante ricercaTestata Bolle Vendita. Dettaglio errore " + sqlca.sqlerrtext
			return -1
		end if

		if isnull(ls_nota_piede_fattura) then ls_nota_piede_fattura = ""
		if len(trim(ls_nota_piede_fattura)) > 0 then
			//ls_nota_piede_fattura = ls_nota_piede_fattura + "~r~n" + is_nota_piede
			ls_nota_piede_fattura = ls_nota_piede_fattura
		else
			//ls_nota_piede_fattura = ls_nota_piede_fattura + is_nota_piede
			ls_nota_piede_fattura = ls_nota_piede_fattura
		end if
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
			ls_nota_piede_fattura = left(ls_nota_piede_fattura, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		
		update tes_fat_ven
		set    num_colli  = num_colli + :id_num_colli,
		       peso_lordo = peso_lordo + :id_peso_lordo,
				 peso_netto = peso_netto + :id_peso_netto,
				 rag_soc_1  = :is_rag_soc_1,
				 rag_soc_2  = :is_rag_soc_2,
				 indirizzo  = :is_indirizzo,
				 localita   = :is_localita,
				 cap = :is_cap,
				 provincia = :is_provincia,
				 frazione  = :is_frazione,
				 data_inizio_trasporto = :idt_data_inizio_trasporto,
				 ora_inizio_trasporto  = :ldt_ora_inizio_trasporto,
				 cod_des_cliente = :is_destinazione,
				 cod_causale  = :is_cod_causale,
				 aspetto_beni = :is_aspetto_beni,
				 cod_porto = :is_cod_porto,
				 cod_mezzo = :is_cod_mezzo,
				 cod_vettore = :is_cod_vettore,
				 cod_inoltro = :is_cod_inoltro,
				 cod_imballo = :is_cod_imballo,
				 nota_piede = :ls_nota_piede_fattura
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_reg_fat_ven and
				 num_registrazione = :fl_num_reg_fat_ven;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante aggiornamento Testata Fatture Vendita. Dettaglio errore " + sqlca.sqlerrtext
			return -1
		end if
	else
		select max(num_registrazione)
		into   :ll_num_registrazione
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la ricerca massimo numero registrazione per incremento numeratore interno fatture.Dettaglio errore: " + sqlca.sqlerrtext
			return -1
		end if
		if isnull(ll_num_registrazione) then ll_num_registrazione = 0
		ll_num_registrazione ++
		update con_fat_ven
		set    num_registrazione = :ll_num_registrazione
		where  cod_azienda = :s_cs_xx.cod_azienda;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura sulla tabella Controllo Fatture Vendita.Dettaglio errore: " + sqlca.sqlerrtext
			return -1
		end if
		
		// *** Modifica Michela 05/12/2007: aggiungo anche l'imballo da segnalazione di viropa
		if not isnull(is_cod_imballo) then ls_cod_imballo = is_cod_imballo
		if not isnull(is_cod_causale) then ls_cod_causale = is_cod_causale
		if not isnull(is_aspetto_beni) then ls_aspetto_beni = is_aspetto_beni
		if not isnull(is_cod_porto) then ls_cod_porto = is_cod_porto
		if not isnull(is_cod_mezzo) then ls_cod_mezzo = is_cod_mezzo
		if not isnull(is_cod_vettore) then ls_cod_vettore = is_cod_vettore
		if not isnull(is_cod_inoltro) then ls_cod_inoltro = is_cod_inoltro
		if not isnull(is_cod_resa) then ls_cod_resa = is_cod_resa
		if not isnull(is_destinazione) then
			ls_cod_des_cliente = is_destinazione
			ls_rag_soc_1 = is_rag_soc_1
			ls_rag_soc_2 = is_rag_soc_2
			ls_indirizzo = is_indirizzo
			ls_localita = is_localita
			ls_frazione = is_frazione
			ls_cap = is_cap
			ls_provincia = is_provincia
		else
			if not isnull(is_rag_soc_1) and is_rag_soc_1 <> "" then
				ls_rag_soc_1 = is_rag_soc_1
			end if
			if not isnull(is_rag_soc_2) and is_rag_soc_2 <> "" then
				ls_rag_soc_2 = is_rag_soc_2
			end if
			if not isnull(is_indirizzo) and is_indirizzo <> "" then
				ls_indirizzo = is_indirizzo
			end if
			if not isnull(is_localita) and is_localita <> "" then
				ls_localita = is_localita
			end if
			if not isnull(is_frazione) and is_frazione <> "" then
				ls_frazione = is_frazione
			end if
			if not isnull(is_cap) and is_cap <> "" then
				ls_cap = is_cap
			end if			
			if not isnull(is_provincia) and is_provincia <> "" then
				ls_provincia = is_provincia
			end if									
		end if
		if not isnull(id_num_colli) and id_num_colli <> 0 then ld_num_colli = id_num_colli
		if not isnull(id_peso_lordo) and id_peso_lordo <> 0 then ld_peso_lordo = id_peso_lordo
		if not isnull(id_peso_netto) and id_peso_netto <> 0 then ld_peso_netto = id_peso_netto
		ldt_ora_inizio_trasporto = datetime(date("01/01/1900"), it_ora_inizio_trasporto)
		if isnull(ls_num_ord_cliente) or len(ls_num_ord_cliente) < 1 then
			ls_num_ord_cliente = is_num_ord_cliente
		end if
		if isnull(ldt_data_ord_cliente) then
			ldt_data_ord_cliente = idt_data_ord_cliente
		end if
		if isnull(ls_nota_piede) then ls_nota_piede = ""
		if len(trim(ls_nota_piede)) > 0 then
			//ls_nota_piede = ls_nota_piede + "~r~n" + is_nota_piede
			ls_nota_piede = ls_nota_piede
		else
			//ls_nota_piede = ls_nota_piede + is_nota_piede
			ls_nota_piede = ls_nota_piede
		end if
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_piede) > 255 then
			ls_nota_piede = left(ls_nota_piede, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		

		insert into tes_fat_ven
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 data_registrazione,   
						 cod_operatore,   
						 cod_tipo_fat_ven,   
						 cod_cliente,
						 cod_des_cliente,   
						 rag_soc_1,   
						 rag_soc_2,   
						 indirizzo,   
						 localita,   
						 frazione,   
						 cap,   
						 provincia,   
						 cod_deposito,   
						 cod_ubicazione,
						 cod_valuta,   
						 cambio_ven,   
						 cod_tipo_listino_prodotto,   
						 cod_pagamento,   
						 sconto,   
						 cod_agente_1,
						 cod_agente_2,
						 cod_banca,   
						 num_ord_cliente,   
						 data_ord_cliente,   
						 cod_imballo,   
						 aspetto_beni,
						 peso_netto,
						 peso_lordo,
						 num_colli,
						 cod_vettore,   
						 cod_inoltro,   
						 cod_mezzo,
						 causale_trasporto,
						 cod_porto,   
						 cod_resa,   
						 cod_documento,
						 numeratore_documento,
						 anno_documento,
						 num_documento,
						 data_fattura,
						 nota_testata,   
						 nota_piede,   
						 flag_fuori_fido,
						 flag_blocco,   
						 flag_movimenti,  
						 flag_contabilita,
						 tot_merci,
						 tot_spese_trasporto,
						 tot_spese_imballo,
						 tot_spese_bolli,
						 tot_spese_varie,
						 tot_sconto_cassa,
						 tot_sconti_commerciali,
						 imponibile_provvigioni_1,
						 imponibile_provvigioni_2,
						 tot_provvigioni_1,
						 tot_provvigioni_2,
						 importo_iva,
						 imponibile_iva,
						 importo_iva_valuta,
						 imponibile_iva_valuta,
						 tot_fattura,
						 tot_fattura_valuta,
						 flag_cambio_zero,
						 cod_banca_clien_for,
						 cod_causale,
						 data_inizio_trasporto,
						 ora_inizio_trasporto,
						 flag_st_note_tes,
						 flag_st_note_pie)  						 
		values      (:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ldt_data_registrazione,   
						 :ls_cod_operatore,   
						 :ls_cod_tipo_fat_ven,   
						 :ls_cod_cliente[1],
						 :ls_cod_des_cliente,   
						 :ls_rag_soc_1,   
						 :ls_rag_soc_2,   
						 :ls_indirizzo,   
						 :ls_localita,   
						 :ls_frazione,   
						 :ls_cap,   
						 :ls_provincia,   
						 :ls_cod_deposito[1],   
						 :ls_cod_ubicazione[1],
						 :ls_cod_valuta,   
						 :ld_cambio_ven,   
						 :ls_cod_tipo_listino_prodotto,   
						 :ls_cod_pagamento,   
						 :ld_sconto_testata,
						 :ls_cod_agente_1,
						 :ls_cod_agente_2,
						 :ls_cod_banca,   
						 :ls_num_ord_cliente,   
						 :ldt_data_ord_cliente,   
						 :ls_cod_imballo,   
						 :ls_aspetto_beni,
						 :ld_peso_netto,
						 :ld_peso_lordo,
						 :ld_num_colli,
						 :ls_cod_vettore,   
						 :ls_cod_inoltro,   
						 :ls_cod_mezzo,   
						 :ls_causale_trasporto,
						 :ls_cod_porto,   
						 :ls_cod_resa,   
						 null,
						 null,
						 0,
						 0,
						 null,
						 :ls_nota_testata,   
						 :ls_nota_piede,   
						 'N',
						 'N',   
						 'N',
						 'N',
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 'N',
						 :ls_cod_banca_clien_for,
						 :ls_cod_causale,
						 :idt_data_inizio_trasporto,
						 :ldt_ora_inizio_trasporto,
						 'I',
						 'I');						 
		 if sqlca.sqlcode <> 0 then
			 fs_errore = "Errore durante la scrittura Testata Fatture Vendita.Dettaglio errore: " + sqlca.sqlerrtext
			 return -1
		 end if
		 if uof_crea_nota_fissa("FAT_VEN", ll_anno_registrazione, ll_num_registrazione, fs_errore) = -1 then return -1
	 end if

	 declare cu_evas_ord_ven cursor for  
	 select  evas_ord_ven.prog_riga_ord_ven,   
				evas_ord_ven.prog_riga,   
				evas_ord_ven.cod_deposito,   
				evas_ord_ven.cod_ubicazione,   
				evas_ord_ven.cod_lotto,   
				evas_ord_ven.data_stock,   
				evas_ord_ven.prog_stock,   
				evas_ord_ven.quan_in_evasione
	from     evas_ord_ven  
	where    evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				evas_ord_ven.anno_registrazione = :fl_anno_registrazione and
				evas_ord_ven.num_registrazione = :fl_num_registrazione and
				evas_ord_ven.cod_utente = :s_cs_xx.cod_utente
	order by evas_ord_ven.prog_riga_ord_ven asc,   
				evas_ord_ven.prog_riga asc;

	open cu_evas_ord_ven;
	if lb_aggiungi then
		select max(prog_riga_fat_ven)
		into   :ll_prog_riga_fat_ven
		from   det_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
			if isnull(ll_prog_riga_fat_ven) then ll_prog_riga_fat_ven = 0
	else
		ll_prog_riga_fat_ven = 0	
	end if
	
	do while 0 = 0
		fetch cu_evas_ord_ven into :ll_prog_riga_ord_ven,   
											:ll_prog_riga,
											:ls_cod_deposito[1],   
											:ls_cod_ubicazione[1],   
											:ls_cod_lotto[1],   
											:ldt_data_stock[1],   
											:ll_progr_stock[1],   
											:ld_quan_in_evasione;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode = -1 then
			fs_errore =  "Errore durante la lettura Evasione Ordini Vendita.Dettaglio errore: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga
		
		select	det_ord_ven.cod_tipo_det_ven,   
					det_ord_ven.cod_misura,   
					det_ord_ven.cod_prodotto,   
					det_ord_ven.des_prodotto,   
					det_ord_ven.quan_ordine,
					det_ord_ven.prezzo_vendita,   
					det_ord_ven.fat_conversione_ven,   
					det_ord_ven.sconto_1,   
					det_ord_ven.sconto_2,  
					det_ord_ven.provvigione_1,
					det_ord_ven.provvigione_2,
					det_ord_ven.cod_iva,   
					det_ord_ven.quan_evasa,
					det_ord_ven.nota_dettaglio,   
					det_ord_ven.anno_commessa,
					det_ord_ven.num_commessa,
					det_ord_ven.cod_centro_costo,
					det_ord_ven.sconto_3,   
					det_ord_ven.sconto_4,   
					det_ord_ven.sconto_5,   
					det_ord_ven.sconto_6,   
					det_ord_ven.sconto_7,   
					det_ord_ven.sconto_8,   
					det_ord_ven.sconto_9,   
					det_ord_ven.sconto_10,
					det_ord_ven.cod_versione,
					det_ord_ven.num_confezioni,
					det_ord_ven.num_pezzi_confezione,
					det_ord_ven.flag_doc_suc_det,
					det_ord_ven.quantita_um,
					det_ord_ven.prezzo_um
		into		:ls_cod_tipo_det_ven, 
					:ls_cod_misura, 
					:ls_cod_prodotto, 
					:ls_des_prodotto, 
					:ld_quan_ordine,
					:ld_prezzo_vendita, 
					:ld_fat_conversione_ven, 
					:ld_sconto_1, 
					:ld_sconto_2, 
					:ld_provvigione_1, 
					:ld_provvigione_2, 
					:ls_cod_iva, 
					:ld_quan_evasa,
					:ls_nota_dettaglio, 
					:ll_anno_commessa,
					:ll_num_commessa,
					:ls_cod_centro_costo,
					:ld_sconto_3, 
					:ld_sconto_4, 
					:ld_sconto_5, 
					:ld_sconto_6, 
					:ld_sconto_7, 
					:ld_sconto_8, 
					:ld_sconto_9, 
					:ld_sconto_10,
					:ls_cod_versione,
					:ll_num_confezioni,
					:ll_num_pezzi_confezione,
					:ls_flag_doc_suc_det,
					:ld_quantita_um,
					:ld_prezzo_um
		  from 	det_ord_ven  
		 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
					det_ord_ven.num_registrazione = :fl_num_registrazione and
					det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven
		order by det_ord_ven.prog_riga_ord_ven asc;
	
		if sqlca.sqlcode <> 0 then
			fs_errore =  "Errore durante la lettura Dettagli Ordini Vendita.Dettaglio errore: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if
			
//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
// ricalcolo la quantità di vendita nel caso di evasioni parziali
		if ld_quan_in_evasione <> ld_quan_ordine then
			ld_quantita_um = round(ld_quan_in_evasione * ld_fat_conversione_ven, 4)
		end if

//------------------------------------------------- Modifica Nicola ---------------------------------------------
	if ls_flag_doc_suc_det = 'I' then //nota dettaglio
		select flag_doc_suc_or
		  into :ls_flag_doc_suc_det
		  from tab_tipi_det_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		
	
	
	if ls_flag_doc_suc_det = 'N' then
		setnull(ls_nota_dettaglio)
	end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------		
					
			
		select tab_tipi_det_ven.flag_tipo_det_ven,
				 tab_tipi_det_ven.cod_tipo_movimento,
				 tab_tipi_det_ven.flag_sola_iva
		into   :ls_flag_tipo_det_ven,
				 :ls_cod_tipo_movimento,
				 :ls_flag_sola_iva
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode <> 0 then
			fs_errore = "Si è verificato un errore in fase di lettura tipi dettagli.Dettaglio errore: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
				fs_errore = "Si è verificato un errore in fase di creazione destinazioni movimenti."
				close cu_evas_ord_ven;
				return -1
			end if

			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if lb_riferimento and ls_flag_abilita_rif_bol_ven = "D" then
			lb_riferimento = false
			setnull(ls_note_dettaglio_rif)
			ls_des_riferimento = ls_des_riferimento_ord_ven
			if ls_flag_rif_anno_ord_ven = "S" and not isnull(fl_anno_registrazione) then
				ls_des_riferimento = ls_des_riferimento + "-" +string(fl_anno_registrazione)
			end if
			if ls_flag_rif_num_ord_ven = "S" and not isnull(fl_num_registrazione) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(fl_num_registrazione)
			end if
			if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
			end if
			
			if not isnull(is_rif_packing_list) and len(is_rif_packing_list) > 0 then
				if not isnull(ls_note_dettaglio_rif) and len(ls_note_dettaglio_rif) > 0 then 
					ls_note_dettaglio_rif += "~r~n"
				else
					ls_note_dettaglio_rif = ""
				end if
				ls_note_dettaglio_rif = ls_note_dettaglio_rif + is_rif_packing_list
			end if
			
			if not isnull(ls_num_ord_cliente) then
				if not isnull(ls_note_dettaglio_rif) and len(ls_note_dettaglio_rif) > 0 then 
					ls_note_dettaglio_rif += "~r~n"
				else
					ls_note_dettaglio_rif = ""
				end if
				ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
				if not isnull(ldt_data_ord_cliente) then
					ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
				end if
			end if
			
				
			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_bol_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
				g_mb.messagebox("Generazione Bolla Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if
			
			insert into det_fat_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_fat_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 data_stock,
							 progr_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_fatturata,   
							 prezzo_vendita,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 anno_registrazione_fat,
							 num_registrazione_fat,
							 nota_dettaglio,   
							 anno_registrazione_bol_ven,   
							 num_registrazione_bol_ven,   
							 prog_riga_bol_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 fat_conversione_ven,   
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 anno_reg_ord_ven,   
							 num_reg_ord_ven,   
							 prog_riga_ord_ven,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_fat_ven,   
							 :ls_cod_tipo_det_ven_rif_bol_ven,   
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 :ls_des_riferimento,   
							 0,   
							 0,   
							 0,   
							 0,  
							 0,
							 0,
							 :ls_cod_iva_rif,   
							 null,
							 null,
							 null,
							 null,
							 null,   
							 null,
							 null,
							 null,
							 null,   
							 null,   
							 null,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,
							 null,
							 1,
							 null,
							 null,
							 null,   
							 null,   
							 null,
							 null,
							 0,
							 0,
							 'I',
							 0,
							 0);							 
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la scrittura Dettagli Fatture Vendita.Dettaglio errore: " + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
			ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga
		end if	
		
		// stefanop 09/05/2010: progetto 140: controllo se devo cambiare il dettaglio
		if this.ib_change_det_ven then
			// cambio il dettaglio solo se uguale a quello di default, altrimenti lascio invariato
			if ls_cod_tipo_det_ven_ord = ls_cod_tipo_det_ven then
				ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_new
			end if
		end if
		
		if this.ib_change_mov then
			if ls_cod_tipo_movimento = ls_cod_tipo_movimento_ord then
				ls_cod_tipo_movimento = ls_cod_tipo_movimento_new
			end if
		end if
		// ---
		
		
		//#####################################################################
		//per ora LASCIAMO COMMENTATO QUESTO CONTROLLO
		//E ATIVIAMO IL CONTROLLO IN FASE DI SPEDIZIONE, CIOè IN EVAS_ORD_VEN
		
//		//Donato 12/03/2012 prima di inserire controlla se la riga di ordine riferita è già presente in fattura
//		//(problema doppia evasione riga ordine che porta a quantità residua negativa!!!!!)
//		if uof_controlla_ordine_in_fattura(	ll_anno_registrazione, ll_num_registrazione, &
//													fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven,fs_errore)<0 then
//			//in fs_errore il messaggio
//			close cu_evas_ord_ven;
//			return -1
//		end if
		//#####################################################################
		
		//########################################################################################
		//Donato 27/06/2012: per nessun motivo deve finire in bolla una quantità superiore a quella della riga di ordine vendita riferito
		if uof_controlla_qta_in_documento(fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven, ld_quan_in_evasione, fs_errore) < 0 then
			//in fs_errore il messaggio
			close cu_evas_ord_ven;
			return -1
		end if
		//########################################################################################
		
		insert into det_fat_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_fat_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 data_stock,
						 progr_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_fatturata,   
						 prezzo_vendita,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 anno_registrazione_fat,
						 num_registrazione_fat,
						 nota_dettaglio,   
						 anno_registrazione_bol_ven,   
						 num_registrazione_bol_ven,   
						 prog_riga_bol_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 fat_conversione_ven,   
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 anno_reg_ord_ven,   
						 num_reg_ord_ven,   
						 prog_riga_ord_ven,
						 cod_versione,
                   		num_confezioni,
					    	num_pezzi_confezione,
						 flag_st_note_det,
						 quantita_um,
						 prezzo_um)						 
	  values			(:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_fat_ven,   
						 :ls_cod_tipo_det_ven,   
						 :ls_cod_deposito[1],
						 :ls_cod_ubicazione[1],
						 :ls_cod_lotto[1],
						 :ldt_data_stock[1],
						 :ll_progr_stock[1],
						 :ls_cod_misura,   
						 :ls_cod_prodotto,   
						 :ls_des_prodotto,   
						 :ld_quan_in_evasione,   
						 :ld_prezzo_vendita,   
						 :ld_sconto_1,   
						 :ld_sconto_2,  
						 :ld_provvigione_1,
						 :ld_provvigione_2,
						 :ls_cod_iva,   
						 :ls_cod_tipo_movimento,
						 null,
						 null,
						 null,
						 :ls_nota_dettaglio,   
						 null,
						 null,
						 null,
						 :ll_anno_commessa,   
						 :ll_num_commessa,   
						 :ls_cod_centro_costo,   
						 :ld_sconto_3,   
						 :ld_sconto_4,   
						 :ld_sconto_5,   
						 :ld_sconto_6,   
						 :ld_sconto_7,   
						 :ld_sconto_8,   
						 :ld_sconto_9,   
						 :ld_sconto_10,
						 null,
						 :ld_fat_conversione_ven,
						 :ll_anno_reg_des_mov,
						 :ll_num_reg_des_mov,
						 :fl_anno_registrazione,   
						 :fl_num_registrazione,   
						 :ll_prog_riga_ord_ven,
						 :ls_cod_versione,
                   :ll_num_confezioni,
					    :ll_num_pezzi_confezione,
						 'I',
						 :ld_quantita_um,
						 :ld_prezzo_um);										 

		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura Dettagli Fatture Vendita.Dettaglio errore: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			update anag_prodotti  
				set quan_assegnata = quan_assegnata - :ld_quan_in_evasione,
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode <> 0 then
				fs_errore = "Si è verificato un errore in fase di aggiornamento magazzino.Dettaglio errore: " + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
			
			update stock 
			set 	 quan_assegnata = quan_assegnata - :ld_quan_in_evasione,   
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione  
			where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
					 stock.cod_prodotto = :ls_cod_prodotto and 
					 stock.cod_deposito = :ls_cod_deposito[1] and 
					 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
					 stock.cod_lotto = :ls_cod_lotto[1] and 
					 stock.data_stock = :ldt_data_stock[1] and 
					 stock.prog_stock = :ll_progr_stock[1];

			if sqlca.sqlcode <> 0 then
				fs_errore = "Si è verificato un errore in fase di aggiornamento stock.Dettaglio errore: " + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
		end if				

		ls_tabella_orig = "det_ord_ven_stat"
		ls_tabella_des = "det_fat_ven_stat"
		ls_prog_riga_doc_orig = "prog_riga_ord_ven"
		ls_prog_riga_doc_des = "prog_riga_fat_ven"

		if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven) = -1 then
			close cu_evas_ord_ven;
			return -1
		end if	

		if ls_cod_tipo_analisi_fat <> ls_cod_tipo_analisi_ord then
			if f_crea_distribuzione(ls_cod_tipo_analisi_fat, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_fat_ven, 6) = -1 then
				fs_errore = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if ld_quan_ordine <= ld_quan_evasa + ld_quan_in_evasione then
			ls_flag_evasione = "E"
		else
			ls_flag_evasione = "P"
		end if
		
		update det_ord_ven  
			set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione,
				 quan_evasa = quan_evasa + :ld_quan_in_evasione,
				 flag_evasione = :ls_flag_evasione
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
				 det_ord_ven.num_registrazione = :fl_num_registrazione and
				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
		if sqlca.sqlcode <> 0 then
			fs_errore = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Vendita.Dettaglio errore: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if
		
		//######################################################################################
		//Donato 27/06/2012 cerco di evitare i casi in cui si possono avere residuo negativo
		//la casistica prevede ad esempio che 
		//1)		la quantità in evasione non si azzera dopo l'evasione, 
		//			e allora il residuo dato da quan_ordine - quan_in_evasione - quan_evasa da un valore minore di zero (caso SACCOLONGO)
		
		//2)		in altri casi si ha invece un azzeramento (giustamente) della quan_in_evasione ma una quantità_evasa superiore a quella dell'ordine (caso VEGGIANO)
		
		uof_correggi_errori_in_evasione(fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven)
		//###########################################################

		if ls_flag_sola_iva = 'N' then
			ld_val_riga = ld_quan_in_evasione * ld_prezzo_vendita
			ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
			ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
			ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
			ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
			ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
			ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
			ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
			ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
			ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
			ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
			ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
			ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
			ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
			ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
			ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
			ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
			ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
			ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
			ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
			if ls_cod_valuta = ls_lire then
				ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
			else
				ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
			end if   
	
			if ls_flag_tipo_det_ven = "M" or &
				ls_flag_tipo_det_ven = "C" or &
				ls_flag_tipo_det_ven = "N" then
				ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
				ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
				ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
				ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
			elseif ls_flag_tipo_det_ven = "S" then
				ld_val_evaso = ld_val_riga_sconto_10 * -1
			else
				ld_val_evaso = ld_val_riga_sconto_10
			end if
			ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
		end if

		if ls_cod_valuta = ls_lire then
			ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
		end if   

		delete from evas_ord_ven 
		where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
						evas_ord_ven.anno_registrazione = :fl_anno_registrazione and
						evas_ord_ven.num_registrazione = :fl_num_registrazione and 
						evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
						evas_ord_ven.prog_riga = :ll_prog_riga and
						evas_ord_ven.cod_utente = :s_cs_xx.cod_utente;

		if sqlca.sqlcode <> 0 then
			fs_errore = "Si è verificato un errore in fase di cancellazione Evasione Ordini.Dettaglio errore: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if
	loop
	close cu_evas_ord_ven;

	if ld_tot_val_ordine <= ld_tot_val_evaso then
		ls_flag_evasione = "E"
	else
		ls_flag_evasione = "P"
	end if

//	update tes_ord_ven  
//		set tot_val_evaso = :ld_tot_val_evaso
//	 where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione and  
//			 tes_ord_ven.num_registrazione = :fl_num_registrazione;
//
//	if sqlca.sqlcode <> 0 then
//		fs_errore = "Si è verificato un errore in fase di aggiornamento Valore Evaso in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
//		return -1
//	end if
	
	if uof_calcola_stato_ordine(fl_anno_registrazione,fl_num_registrazione) = -1 then
		fs_errore = "Si è verificato un errore in fase di aggiornamento stato dell'ordine in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
		return -1
	end if
	
	luo_calcolo = create uo_calcola_documento_euro

	if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"fat_ven",fs_errore) <> 0 then
		destroy luo_calcolo
		return -1
	end if
	
	destroy luo_calcolo
	
	fl_anno_reg_fat_ven = ll_anno_registrazione
	fl_num_reg_fat_ven = ll_num_registrazione
end if

return 0
end function

public function integer uof_crea_fat_riep_da_ordini (long fl_anno_registrazione[], long fl_num_registrazione[], string fs_cod_tipo_raggruppamento, long fl_anno_reg_dest[], long fl_num_reg_dest[], string fs_errore);datastore lds_rag_documenti
boolean lb_riferimento
long ll_i, ll_selected, ll_prog_riga_documento, ll_riga, ll_num_reg_fat_ven, ll_num_ordine_old, &
	  ll_num_registrazione, ll_prog_riga_fat_ven, ll_prog_riga_ord_ven, ll_prog_riga, ll_anno_reg_fat_ven, &
	  ll_progr_stock[1], ll_anno_commessa, ll_num_commessa, ll_anno_reg_des_mov, &
	  ll_num_reg_des_mov, ll_num_confezioni, ll_num_pezzi_confezione,ll_anno_registrazione
string ls_cod_tipo_ord_ven, ls_flag_riep_fat, ls_cod_tipo_fat_ven, ls_cod_cliente[1], &
		 ls_cod_valuta, ls_cod_pagamento, ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca, &
		 ls_cod_banca_clien_for, ls_cod_des_cliente, ls_num_ord_cliente, &
		 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
		 ls_cod_resa, ls_flag_tipo_doc, ls_flag_destinazione, ls_flag_num_doc, &
		 ls_flag_imballo, ls_flag_vettore, ls_flag_inoltro, ls_flag_mezzo, ls_flag_porto, &
		 ls_flag_resa, ls_flag_data_consegna, ls_cod_tipo_rag, ls_sort, ls_key_sort, &
		 ls_key_sort_old, ls_cod_tipo_analisi_fat, ls_causale_trasporto_tab, ls_lire, &
		 ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
		 ls_cap, ls_provincia, ls_cod_tipo_listino_prodotto, ls_aspetto_beni, ls_causale_trasporto, ls_nota_testata, &
		 ls_nota_piede, ls_vostro_ordine, ls_vostro_ordine_data, &
		 ls_cod_deposito[1], ls_cod_ubicazione[1], ls_cod_lotto[1], ls_cod_tipo_det_ven, &
		 ls_cod_misura, ls_cod_prodotto, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio,  &
		 ls_cod_centro_costo, ls_flag_tipo_det_ven, ls_cod_tipo_movimento, ls_flag_sola_iva, &
		 ls_cod_fornitore[1], ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, &
		 ls_prog_riga_doc_des, ls_cod_tipo_analisi_ord, ls_flag_evasione,ls_cod_versione, &
		 ls_flag_abilita_rif_bol_ven, ls_cod_tipo_det_ven_rif_bol_ven, ls_des_riferimento_fat_ven, &
		 ls_flag_rif_cod_documento, ls_flag_rif_numeratore_doc, ls_flag_rif_anno_documento, &
		 ls_flag_rif_num_documento, ls_flag_rif_data_fattura, ls_cod_tipo_analisi, &
		 ls_des_riferimento_ord, ls_flag_rif_anno_ord_ven, ls_flag_rif_num_ord_ven, &
		 ls_flag_rif_data_ord_ven, ls_des_riferimento, ls_cod_iva_rif, ls_note_dettaglio_rif, &
		 ls_db, ls_nota_testata_1, ls_nota_piede_1, &
		 ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det
datetime ldt_data_consegna, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_registrazione, &
	  		ldt_data_stock[1], ldt_data_consegna_ordine
dec{6} ld_sconto_pagamento, ld_cambio_ven, ld_sconto_testata, ld_peso_netto, ld_peso_lordo, &
		 ld_num_colli, ld_tot_val_evaso, ld_tot_val_ordine, &
		 ld_quan_in_evasione, ld_quan_ordine, ld_prezzo_vendita, ld_fat_conversione_ven, &
		 ld_sconto_1, ld_sconto_2, ld_provvigione_1, ld_provvigione_2, ld_quan_evasa, &
		 ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, &
		 ld_sconto_9, ld_sconto_10, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, &
		 ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, &
		 ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, &
		 ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, &
		 ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quantita_um, ld_prezzo_um
integer li_risposta

uo_calcola_documento_euro luo_calcolo


li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
if upper(ls_db) = "MSSQL" then ls_db="SYBASE_ASE"

guo_functions.uof_get_parametro_azienda( "IRF", ref il_incremento_riga)
if isnull(il_incremento_riga) or il_incremento_riga = 0 then
	il_incremento_riga = 10
end if

ll_selected = upperbound(fl_anno_registrazione)
lds_rag_documenti = create datastore
lds_rag_documenti.dataobject = "d_rag_documenti"
lds_rag_documenti.settransobject(sqlca)
if isnull(fs_cod_tipo_raggruppamento) or len(fs_cod_tipo_raggruppamento) < 1 then
	fs_errore = "Indicare il tipo raggrupamento da utilizzare"
	return -1
end if

for ll_i = 1 to ll_selected

	select flag_riep_fat, 
	       cod_tipo_ord_ven
	into   :ls_flag_riep_fat,
	       :ls_cod_tipo_ord_ven
	from   tes_ord_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 anno_registrazione = :fl_anno_registrazione[ll_i] and 
			 num_registrazione = :fl_num_registrazione[ll_i];
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la lettura della Testata Ordini Vendita"
		return -1
	end if

	if ls_flag_riep_fat = 'N' then
		if uof_crea_fattura(fl_anno_registrazione[ll_i], fl_num_registrazione[ll_i], fl_anno_reg_dest[ll_i], fl_num_reg_dest[ll_i], fs_errore) = -1 then
			return -1
		end if
//		ll_riga = dw_gen_orig_dest.insertrow(0)
//		dw_gen_orig_dest.setitem(ll_riga, "anno_registrazione_orig",  fl_anno_registrazione[ll_i])
//		dw_gen_orig_dest.setitem(ll_riga, "num_registrazione_orig" , fl_num_registrazione[ll_i])
//		dw_gen_orig_dest.setitem(ll_riga, "anno_registrazione_dest",  ll_anno_reg_fat_ven)
//		dw_gen_orig_dest.setitem(ll_riga, "num_registrazione_dest" , ll_num_reg_fat_ven)
	else
		select cod_tipo_fat_ven
		into   :ls_cod_tipo_fat_ven
		from   tab_tipi_ord_ven  
		where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la lettura della tabella Tipi Ordini Vendita."
			return -1
		end if
		
		if not isnull(ls_cod_tipo_fat_ven) then
			if not isnull(ls_cod_tipo_rag) and trim(ls_cod_tipo_rag) <> "" then
				select tab_tipi_rag.flag_tipo_doc,   
						 tab_tipi_rag.flag_destinazione,   
						 tab_tipi_rag.flag_num_doc,   
						 tab_tipi_rag.flag_imballo,   
						 tab_tipi_rag.flag_vettore,   
						 tab_tipi_rag.flag_inoltro,   
						 tab_tipi_rag.flag_mezzo,   
						 tab_tipi_rag.flag_porto,   
						 tab_tipi_rag.flag_resa,   
						 tab_tipi_rag.flag_data_consegna  
				into   :ls_flag_tipo_doc,   
						 :ls_flag_destinazione,   
						 :ls_flag_num_doc,   
						 :ls_flag_imballo,   
						 :ls_flag_vettore,   
						 :ls_flag_inoltro,   
						 :ls_flag_mezzo,   
						 :ls_flag_porto,   
						 :ls_flag_resa,   
						 :ls_flag_data_consegna  
				from   tab_tipi_rag  
				where  tab_tipi_rag.cod_azienda = :s_cs_xx.cod_azienda and
						 tab_tipi_rag.cod_tipo_rag = :ls_cod_tipo_rag;
	
				if sqlca.sqlcode <> 0 then
					fs_errore = "Errore durante la lettura Tabella Tipi Ragruppamenti."
					destroy lds_rag_documenti
					return -1
				end if
			end if
			
			select tes_ord_ven.cod_cliente,   
					 tes_ord_ven.cod_valuta,   
					 tes_ord_ven.cod_pagamento,   
					 tes_ord_ven.cod_agente_1,
					 tes_ord_ven.cod_agente_2,
					 tes_ord_ven.cod_banca,   
					 tes_ord_ven.cod_banca_clien_for,
					 tes_ord_ven.cod_tipo_ord_ven,
					 tes_ord_ven.cod_des_cliente,   
					 tes_ord_ven.num_ord_cliente,   
					 tes_ord_ven.cod_imballo,   
					 tes_ord_ven.cod_vettore,   
					 tes_ord_ven.cod_inoltro,   
					 tes_ord_ven.cod_mezzo,
					 tes_ord_ven.cod_porto,   
					 tes_ord_ven.cod_resa
			 into  :ls_cod_cliente[1],
					 :ls_cod_valuta,   
					 :ls_cod_pagamento,   
					 :ls_cod_agente_1,
					 :ls_cod_agente_2,
					 :ls_cod_banca,   
					 :ls_cod_banca_clien_for,
					 :ls_cod_tipo_ord_ven,
					 :ls_cod_des_cliente,   
					 :ls_num_ord_cliente,   
					 :ls_cod_imballo,   
					 :ls_cod_vettore,   
					 :ls_cod_inoltro,   
					 :ls_cod_mezzo,   
					 :ls_cod_porto,
					 :ls_cod_resa
			from   tes_ord_ven
			where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					 tes_ord_ven.anno_registrazione = :fl_anno_registrazione[ll_i] and  
					 tes_ord_ven.num_registrazione = :fl_num_registrazione[ll_i];
		
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la lettura Testata Ordine Vendita."
				destroy lds_rag_documenti
				return -1
			end if

			if ls_flag_data_consegna = "S" then
				declare cu_det_ord_ven cursor for  
				select   det_ord_ven.prog_riga_ord_ven,
							det_ord_ven.data_consegna
				from     det_ord_ven  
				where    det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
							det_ord_ven.anno_registrazione = :fl_anno_registrazione[ll_i] and
							det_ord_ven.num_registrazione = :fl_num_registrazione[ll_i]
				order by det_ord_ven.data_consegna asc;

				open cu_det_ord_ven;
				
				do while 0 = 0
					fetch cu_det_ord_ven into :ll_prog_riga_documento, :ldt_data_consegna;
			
					if sqlca.sqlcode = 100 then exit
					
					if sqlca.sqlcode <> 0 then
						fs_errore = "Errore durante la lettura Dettagli Ordine di Vendita."
						destroy lds_rag_documenti
						close cu_det_ord_ven;
						return -1
					end if

					lds_rag_documenti.insertrow(0)
					lds_rag_documenti.setrow(lds_rag_documenti.rowcount())
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "anno_documento", fl_anno_registrazione[ll_i])
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "num_documento", fl_num_registrazione[ll_i])
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "prog_riga_documento", ll_prog_riga_documento)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_cliente", ls_cod_cliente[1])
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_valuta", ls_cod_valuta)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_pagamento", ls_cod_pagamento)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_agente_1", ls_cod_agente_1)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_agente_2", ls_cod_agente_2)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca", ls_cod_banca)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca_clien_for", ls_cod_banca_clien_for)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_tipo_documento", ls_cod_tipo_ord_ven)
//					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_doc_esterno", ls_num_ord_cliente)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_imballo", ls_cod_imballo)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_vettore", ls_cod_vettore)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_inoltro", ls_cod_inoltro)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_mezzo", ls_cod_mezzo)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_porto", ls_cod_porto)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_resa", ls_cod_resa)
					lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "data_consegna", ldt_data_consegna)
				loop
				close cu_det_ord_ven;
			else
				lds_rag_documenti.insertrow(0)
				lds_rag_documenti.setrow(lds_rag_documenti.rowcount())
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "anno_documento", fl_anno_registrazione[ll_i])
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "num_documento", fl_num_registrazione[ll_i])
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_cliente", ls_cod_cliente[1])
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_valuta", ls_cod_valuta)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_pagamento", ls_cod_pagamento)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_agente_1", ls_cod_agente_1)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_agente_2", ls_cod_agente_2)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca", ls_cod_banca)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca_clien_for", ls_cod_banca_clien_for)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_tipo_documento", ls_cod_tipo_ord_ven)
//				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_doc_esterno", ls_num_ord_cliente)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_imballo", ls_cod_imballo)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_vettore", ls_cod_vettore)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_inoltro", ls_cod_inoltro)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_mezzo", ls_cod_mezzo)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_porto", ls_cod_porto)
				lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_resa", ls_cod_resa)
			end if
		end if
	end if
next

ls_sort = "cod_cliente A, cod_valuta A, cod_pagamento A, cod_agente_1 A, cod_agente_2 A, cod_banca A, cod_banca_clien_for A"

if ls_flag_tipo_doc = "S" then
	ls_sort = ls_sort + ", cod_tipo_documento A"
end if

if ls_flag_destinazione = "S" then
	ls_sort = ls_sort + ", cod_destinazione A"
end if

if ls_flag_num_doc = "S" then
	ls_sort = ls_sort + ", cod_doc_esterno A"
end if

if ls_flag_imballo = "S" then
	ls_sort = ls_sort + ", cod_imballo A"
end if

if ls_flag_vettore = "S" then
	ls_sort = ls_sort + ", cod_vettore A"
end if

if ls_flag_inoltro = "S" then
	ls_sort = ls_sort + ", cod_inoltro A"
end if

if ls_flag_mezzo = "S" then
	ls_sort = ls_sort + ", cod_mezzo A"
end if

if ls_flag_porto = "S" then
	ls_sort = ls_sort + ", cod_porto A"
end if

if ls_flag_resa = "S" then
	ls_sort = ls_sort + ", cod_resa A"
end if

if ls_flag_data_consegna = "S" then
	ls_sort = ls_sort + ", data_consegna A"
end if

ls_sort = ls_sort + ", anno_documento A, num_documento A, prog_riga_documento A"

lds_rag_documenti.setsort(ls_sort)

lds_rag_documenti.sort()

ls_key_sort_old = ""
ll_num_ordine_old = 0
for ll_i = 1 to lds_rag_documenti.rowcount()
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_cliente")) then
		ls_key_sort = lds_rag_documenti.getitemstring(ll_i, "cod_cliente")
	else
		ls_key_sort = "      "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_valuta")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_valuta")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_agente_1")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_agente_1")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_agente_2")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_agente_2")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_tipo_doc = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_destinazione = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_destinazione")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_destinazione")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_num_doc = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_doc_esterno")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_doc_esterno")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_imballo = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_imballo")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_imballo")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_vettore = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_vettore")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_vettore")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_inoltro = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_inoltro")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_inoltro")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_mezzo = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_mezzo")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_mezzo")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_porto = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_porto")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_porto")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_resa = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_resa")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_resa")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_data_consegna = "S" and &
		not isnull(lds_rag_documenti.getitemdatetime(ll_i, "data_consegna")) then
		ls_key_sort = ls_key_sort + string(lds_rag_documenti.getitemdatetime(ll_i, "data_consegna"))
	else
		ls_key_sort = ls_key_sort + "        "
	end if

	fl_anno_registrazione[ll_i] = lds_rag_documenti.getitemnumber(ll_i, "anno_documento")
	fl_num_registrazione[ll_i] = lds_rag_documenti.getitemnumber(ll_i, "num_documento")
	ldt_data_consegna = lds_rag_documenti.getitemdatetime(ll_i, "data_consegna")
	ls_cod_tipo_ord_ven = lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")

	select tab_tipi_ord_ven.cod_tipo_fat_ven,
			 tab_tipi_ord_ven.cod_tipo_analisi
	into   :ls_cod_tipo_fat_ven,
			 :ls_cod_tipo_analisi_ord
	from   tab_tipi_ord_ven  
	where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_ord_ven.cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la lettura della tabella Tipi Ordini Vendita."
		return -1
	end if
	
	select tes_ord_ven.cod_operatore,   
			 tes_ord_ven.data_registrazione,   
			 tes_ord_ven.cod_cliente,   
			 tes_ord_ven.cod_des_cliente,   
			 tes_ord_ven.rag_soc_1,   
			 tes_ord_ven.rag_soc_2,   
			 tes_ord_ven.indirizzo,   
			 tes_ord_ven.localita,   
			 tes_ord_ven.frazione,   
			 tes_ord_ven.cap,   
			 tes_ord_ven.provincia,   
			 tes_ord_ven.cod_deposito,   
			 tes_ord_ven.cod_ubicazione,   
			 tes_ord_ven.cod_valuta,   
			 tes_ord_ven.cambio_ven,   
			 tes_ord_ven.cod_tipo_listino_prodotto,   
			 tes_ord_ven.cod_pagamento,   
			 tes_ord_ven.sconto,   
			 tes_ord_ven.cod_agente_1,
			 tes_ord_ven.cod_agente_2,
			 tes_ord_ven.cod_banca,   
			 tes_ord_ven.num_ord_cliente,   
			 tes_ord_ven.data_ord_cliente,   
			 tes_ord_ven.cod_imballo,   
			 tes_ord_ven.aspetto_beni,
			 tes_ord_ven.peso_netto,
			 tes_ord_ven.peso_lordo,
			 tes_ord_ven.num_colli,
			 tes_ord_ven.cod_vettore,   
			 tes_ord_ven.cod_inoltro,   
			 tes_ord_ven.cod_mezzo,
			 tes_ord_ven.causale_trasporto,
			 tes_ord_ven.cod_porto,   
			 tes_ord_ven.cod_resa,   
			 tes_ord_ven.nota_testata,   
			 tes_ord_ven.nota_piede,   
			 tes_ord_ven.flag_riep_fat,
			 tes_ord_ven.tot_val_evaso,
			 tes_ord_ven.tot_val_ordine,
			 tes_ord_ven.cod_banca_clien_for,
			 tes_ord_ven.flag_doc_suc_tes, 
			 tes_ord_ven.flag_doc_suc_pie					 			 
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ls_flag_riep_fat,
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_flag_doc_suc_tes, 
			 :ls_flag_doc_suc_pie			 
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione[ll_i] and  
			 tes_ord_ven.num_registrazione = :fl_num_registrazione[ll_i];

	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la lettura Testata Ordine Vendita."
		destroy lds_rag_documenti
		return -1
	end if
	
	// stefanop: 18/07/2012 - l'operatore che inserire è SEMPRE l'operatore collegato ad Apice
	// ------------------------------------------------------
	ls_cod_operatore = guo_functions.uof_get_operatore_utente()
	// ------------------------------------------------------

	select tab_tipi_fat_ven.cod_tipo_analisi,
			 tab_tipi_fat_ven.causale_trasporto,
			 tab_tipi_fat_ven.flag_abilita_rif_bol_ven,
			 tab_tipi_fat_ven.cod_tipo_det_ven_rif_bol_ven,
			 tab_tipi_fat_ven.des_riferimento_fat_ven,
			 tab_tipi_fat_ven.flag_rif_cod_documento,
			 tab_tipi_fat_ven.flag_rif_numeratore_doc,
			 tab_tipi_fat_ven.flag_rif_anno_documento,
			 tab_tipi_fat_ven.flag_rif_num_documento,
			 tab_tipi_fat_ven.flag_rif_data_fattura,
			 tab_tipi_fat_ven.des_riferimento_ord_ven,
			 tab_tipi_fat_ven.flag_rif_anno_ord_ven,
			 tab_tipi_fat_ven.flag_rif_num_ord_ven,
			 tab_tipi_fat_ven.flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi,
			 :ls_causale_trasporto_tab,
			 :ls_flag_abilita_rif_bol_ven,
			 :ls_cod_tipo_det_ven_rif_bol_ven,
			 :ls_des_riferimento_fat_ven,
			 :ls_flag_rif_cod_documento,
			 :ls_flag_rif_numeratore_doc,
			 :ls_flag_rif_anno_documento,
			 :ls_flag_rif_num_documento,
			 :ls_flag_rif_data_fattura,
			 :ls_des_riferimento_ord,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_fat_ven  
	where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Si è verificato un errore nella lettura della Tabella Tipi Fatture."
		destroy lds_rag_documenti
		return -1
	end if
	
	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if

	if isnull(ls_nota_piede) then
		ls_nota_piede = ""
	else
		ls_nota_piede = ls_nota_piede + "~r~n"
	end if

//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------


	if not isnull(ls_num_ord_cliente)  and ls_flag_abilita_rif_bol_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if
   if ls_flag_abilita_rif_bol_ven = "T" then
		ls_nota_testata = ls_des_riferimento_fat_ven + string(fl_anno_registrazione[ll_i]) + "/" + &
								string(fl_num_registrazione[ll_i]) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata + &
								"~r~n"
	end if	
	
	// ----- controllo lunghezza note per ASE
	if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE


	if ls_key_sort <> ls_key_sort_old then
		select con_fat_ven.num_registrazione
		into   :ll_num_registrazione
		from   con_fat_ven
		where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la lettura della tabella Controllo Fatture Vendita."
			destroy lds_rag_documenti
			return -1
		end if
	
		ll_num_registrazione ++
		
		update con_fat_ven
		set    con_fat_ven.num_registrazione = :ll_num_registrazione
		where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura sulla tabella Controllo Fatture Vendita."
			destroy lds_rag_documenti
			return -1
		end if
	
		ldt_data_registrazione = datetime(today())
	
		// spostato da qui la select tab_tipi_fat_ven	
		if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
			ls_causale_trasporto = ls_causale_trasporto_tab
		end if
	
		select parametri_azienda.stringa
		into   :ls_lire
		from   parametri_azienda
		where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
				 parametri_azienda.flag_parametro = 'S' and &
				 parametri_azienda.cod_parametro = 'LIR';
	
		if sqlca.sqlcode <> 0 then
			fs_errore = "Configurare il codice valuta per le Lire Italiane."
			destroy lds_rag_documenti
			return -1
		end if
	
		select tab_pagamenti.sconto
		into   :ld_sconto_pagamento
		from   tab_pagamenti
		where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;
	
		if sqlca.sqlcode <> 0 then
			ld_sconto_pagamento = 0
		end if
		
		ll_anno_registrazione = f_anno_esercizio()
		insert into tes_fat_ven
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 data_registrazione,   
						 cod_operatore,   
						 cod_tipo_fat_ven,   
						 cod_cliente,
						 cod_des_cliente,   
						 rag_soc_1,   
						 rag_soc_2,   
						 indirizzo,   
						 localita,   
						 frazione,   
						 cap,   
						 provincia,   
						 cod_deposito,   
						 cod_ubicazione,
						 cod_valuta,   
						 cambio_ven,   
						 cod_tipo_listino_prodotto,   
						 cod_pagamento,   
						 sconto,   
						 cod_agente_1,
						 cod_agente_2,
						 cod_banca,   
						 num_ord_cliente,   
						 data_ord_cliente,   
						 cod_imballo,   
						 aspetto_beni,
						 peso_netto,
						 peso_lordo,
						 num_colli,
						 cod_vettore,   
						 cod_inoltro,   
						 cod_mezzo,
						 causale_trasporto,
						 cod_porto,   
						 cod_resa,   
						 cod_documento,
						 numeratore_documento,
						 anno_documento,
						 num_documento,
						 data_fattura,
						 nota_testata,   
						 nota_piede,   
						 flag_fuori_fido,
						 flag_blocco,   
						 flag_movimenti,  
						 flag_contabilita,
						 tot_merci,
						 tot_spese_trasporto,
						 tot_spese_imballo,
						 tot_spese_bolli,
						 tot_spese_varie,
						 tot_sconto_cassa,
						 tot_sconti_commerciali,
						 imponibile_provvigioni_1,
						 imponibile_provvigioni_2,
						 tot_provvigioni_1,
						 tot_provvigioni_2,
						 importo_iva,
						 imponibile_iva,
						 importo_iva_valuta,
						 imponibile_iva_valuta,
						 tot_fattura,
						 tot_fattura_valuta,
						 flag_cambio_zero,
						 cod_banca_clien_for,
						 flag_st_note_tes,
						 flag_st_note_pie)						 
		values      (:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ldt_data_registrazione,   
						 :ls_cod_operatore,   
						 :ls_cod_tipo_fat_ven,   
						 :ls_cod_cliente[1],
						 :ls_cod_des_cliente,   
						 :ls_rag_soc_1,   
						 :ls_rag_soc_2,   
						 :ls_indirizzo,   
						 :ls_localita,   
						 :ls_frazione,   
						 :ls_cap,   
						 :ls_provincia,   
						 :ls_cod_deposito[1],   
						 :ls_cod_ubicazione[1],
						 :ls_cod_valuta,   
						 :ld_cambio_ven,   
						 :ls_cod_tipo_listino_prodotto,   
						 :ls_cod_pagamento,   
						 :ld_sconto_testata,
						 :ls_cod_agente_1,
						 :ls_cod_agente_2,
						 :ls_cod_banca,   
						 :ls_num_ord_cliente,   
						 :ldt_data_ord_cliente,   
						 :ls_cod_imballo,   
						 :ls_aspetto_beni,
						 :ld_peso_netto,
						 :ld_peso_lordo,
						 :ld_num_colli,
						 :ls_cod_vettore,   
						 :ls_cod_inoltro,   
						 :ls_cod_mezzo,   
						 :ls_causale_trasporto,
						 :ls_cod_porto,   
						 :ls_cod_resa,   
						 null,
						 null,
						 0,
						 0,
						 null,
						 '',   
						 '',   
						 'N',
						 'N',   
						 'N',
						 'N',
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 'N',
						 :ls_cod_banca_clien_for,
						 'I',
						 'I');						 
	
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura Testata Fatture Vendita."
			destroy lds_rag_documenti
			return -1
		end if
	   if uof_crea_nota_fissa("FAT_VEN", ll_anno_registrazione, ll_num_registrazione, fs_errore) = -1 then return -1

		ll_prog_riga_fat_ven = 0
	end if

	declare cu_evas_ord_ven cursor for  
	select   evas_ord_ven.prog_riga_ord_ven,   
			   evas_ord_ven.prog_riga,   
			   evas_ord_ven.cod_deposito,   
			   evas_ord_ven.cod_ubicazione,   
			   evas_ord_ven.cod_lotto,   
			   evas_ord_ven.data_stock,   
			   evas_ord_ven.prog_stock,   
			   evas_ord_ven.quan_in_evasione
	from     evas_ord_ven  
	where    evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				evas_ord_ven.anno_registrazione = :fl_anno_registrazione[ll_i] and
				evas_ord_ven.num_registrazione = :fl_num_registrazione[ll_i] and
				evas_ord_ven.cod_utente = :s_cs_xx.cod_utente
	order by evas_ord_ven.prog_riga_ord_ven asc,   
				evas_ord_ven.prog_riga asc;

	lb_riferimento = true
	open cu_evas_ord_ven;
	
	do while 0 = 0
		fetch cu_evas_ord_ven into :ll_prog_riga_ord_ven,   
											:ll_prog_riga,
											:ls_cod_deposito[1],   
											:ls_cod_ubicazione[1],   
											:ls_cod_lotto[1],   
											:ldt_data_stock[1],   
											:ll_progr_stock[1],   
											:ld_quan_in_evasione;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode = -1 then
			fs_errore =  "Errore durante la lettura Evasione Ordini Vendita."
			close cu_evas_ord_ven;
			destroy lds_rag_documenti
			return -1
		end if

		ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga
		
		select	det_ord_ven.cod_tipo_det_ven,   
					det_ord_ven.cod_misura,   
					det_ord_ven.cod_prodotto,   
					det_ord_ven.des_prodotto,   
					det_ord_ven.quan_ordine,
					det_ord_ven.prezzo_vendita,   
					det_ord_ven.fat_conversione_ven,   
					det_ord_ven.sconto_1,   
					det_ord_ven.sconto_2,  
					det_ord_ven.provvigione_1,
					det_ord_ven.provvigione_2,
					det_ord_ven.cod_iva,   
					det_ord_ven.data_consegna,
					det_ord_ven.quan_evasa,
					det_ord_ven.nota_dettaglio,   
					det_ord_ven.anno_commessa,
					det_ord_ven.num_commessa,
					det_ord_ven.cod_centro_costo,
					det_ord_ven.sconto_3,   
					det_ord_ven.sconto_4,   
					det_ord_ven.sconto_5,   
					det_ord_ven.sconto_6,   
					det_ord_ven.sconto_7,   
					det_ord_ven.sconto_8,   
					det_ord_ven.sconto_9,   
					det_ord_ven.sconto_10,
					det_ord_ven.cod_versione,
					det_ord_ven.num_confezioni,
					det_ord_ven.num_pezzi_confezione,
					det_ord_ven.flag_doc_suc_det,
					det_ord_ven.quantita_um,
					det_ord_ven.prezzo_um
		into		:ls_cod_tipo_det_ven, 
					:ls_cod_misura, 
					:ls_cod_prodotto, 
					:ls_des_prodotto, 
					:ld_quan_ordine,
					:ld_prezzo_vendita, 
					:ld_fat_conversione_ven, 
					:ld_sconto_1, 
					:ld_sconto_2, 
					:ld_provvigione_1, 
					:ld_provvigione_2, 
					:ls_cod_iva, 
					:ldt_data_consegna_ordine,
					:ld_quan_evasa,
					:ls_nota_dettaglio, 
					:ll_anno_commessa,
					:ll_num_commessa,
					:ls_cod_centro_costo,
					:ld_sconto_3, 
					:ld_sconto_4, 
					:ld_sconto_5, 
					:ld_sconto_6, 
					:ld_sconto_7, 
					:ld_sconto_8, 
					:ld_sconto_9, 
					:ld_sconto_10,
					:ls_cod_versione,
					:ll_num_confezioni,
					:ll_num_pezzi_confezione,
					:ls_flag_doc_suc_det,
					:ld_quantita_um,
					:ld_prezzo_um
		  from 	det_ord_ven  
		 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					det_ord_ven.anno_registrazione = :fl_anno_registrazione[ll_i] and  
					det_ord_ven.num_registrazione = :fl_num_registrazione[ll_i] and
					det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven
		order by det_ord_ven.prog_riga_ord_ven asc;
	
		if sqlca.sqlcode = -1 then
			fs_errore =  "Errore durante la lettura Dettagli Ordini Vendita."
			close cu_evas_ord_ven;
			destroy lds_rag_documenti
			return -1
		end if
		
//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
// ricalcolo la quantità di vendita nel caso di evasioni parziali
		if ld_quan_in_evasione <> ld_quan_ordine then
			ld_quantita_um = round(ld_quan_in_evasione * ld_fat_conversione_ven, 4)
		end if

//------------------------------------------------- Modifica Nicola ---------------------------------------------
	if ls_flag_doc_suc_det = 'I' then //nota dettaglio
		select flag_doc_suc_or
		  into :ls_flag_doc_suc_det
		  from tab_tipi_det_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		
	
	
	if ls_flag_doc_suc_det = 'N' then
		setnull(ls_nota_dettaglio)
	end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------		
		
		
		if (ls_flag_data_consegna = "S" and &
			 (ldt_data_consegna = ldt_data_consegna_ordine) or &
			  isnull(ldt_data_consegna) and isnull(ldt_data_consegna_ordine)) or &
			ls_flag_data_consegna = "N" then
			select tab_tipi_det_ven.flag_tipo_det_ven,
					 tab_tipi_det_ven.cod_tipo_movimento,
					 tab_tipi_det_ven.flag_sola_iva
			into   :ls_flag_tipo_det_ven,
					 :ls_cod_tipo_movimento,
					 :ls_flag_sola_iva
			from   tab_tipi_det_ven
			where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
			if sqlca.sqlcode = -1 then
				fs_errore = "Si è verificato un errore in fase di lettura tipi dettagli."
				close cu_evas_ord_ven;
				destroy lds_rag_documenti
				return -1
			end if
	
			if ls_flag_tipo_det_ven = "M" then
				if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
					fs_errore = "Si è verificato un errore in fase di creazione destinazioni movimenti."
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
	
				if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
			end if

			if lb_riferimento and ls_flag_abilita_rif_bol_ven = "D" then
				lb_riferimento = false
				setnull(ls_note_dettaglio_rif)
				ls_des_riferimento = ls_des_riferimento_ord
				if ls_flag_rif_anno_ord_ven = "S" and not isnull(fl_anno_registrazione[ll_i]) then
					ls_des_riferimento = ls_des_riferimento + "-" +string(fl_anno_registrazione[ll_i])
				end if
				if ls_flag_rif_num_ord_ven = "S" and not isnull(fl_num_registrazione[ll_i]) then
					ls_des_riferimento = ls_des_riferimento + "-" + string(fl_num_registrazione[ll_i])
				end if
				if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
					ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
				end if
				if not isnull(ls_num_ord_cliente) then
					ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
					if not isnull(ldt_data_ord_cliente) then
						ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
					end if
				end if
					
			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_bol_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
				g_mb.messagebox("Generazione Bolla Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if
			ll_anno_registrazione = f_anno_esercizio()
			insert into det_fat_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_fat_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 data_stock,
							 progr_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_fatturata,   
							 prezzo_vendita,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 anno_registrazione_fat,
							 num_registrazione_fat,
							 nota_dettaglio,   
							 anno_registrazione_bol_ven,   
							 num_registrazione_bol_ven,   
							 prog_riga_bol_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 fat_conversione_ven,   
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 anno_reg_ord_ven,   
							 num_reg_ord_ven,   
							 prog_riga_ord_ven,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_fat_ven,   
							 :ls_cod_tipo_det_ven_rif_bol_ven,   
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 :ls_des_riferimento,   
							 0,   
							 0,   
							 0,   
							 0,  
							 0,
							 0,
							 :ls_cod_iva_rif,   
							 null,
							 null,
							 null,
							 null,
							 :ls_note_dettaglio_rif,   
							 null,
							 null,
							 null,
							 null,   
							 null,   
							 null,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,
							 null,
							 1,
							 null,
							 null,
							 null,   
							 null,   
							 null,
							 null,
							 0,
							 0,
							 'I',
							 0,
							 0);
				if sqlca.sqlcode <> 0 then
					fs_errore = "Errore durante la scrittura Dettagli Fatture Vendita."
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
				ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga
			end if	
	
	
			insert into det_fat_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_fat_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 data_stock,
							 progr_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_fatturata,   
							 prezzo_vendita,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 anno_registrazione_fat,
							 num_registrazione_fat,
							 nota_dettaglio,   
							 anno_registrazione_bol_ven,   
							 num_registrazione_bol_ven,   
							 prog_riga_bol_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 fat_conversione_ven,   
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 anno_reg_ord_ven,   
							 num_reg_ord_ven,   
							 prog_riga_ord_ven,
							 cod_versione,
                      num_confezioni,
					       num_pezzi_confezione,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_fat_ven,   
							 :ls_cod_tipo_det_ven,   
							 :ls_cod_deposito[1],
							 :ls_cod_ubicazione[1],
							 :ls_cod_lotto[1],
							 :ldt_data_stock[1],
							 :ll_progr_stock[1],
							 :ls_cod_misura,   
							 :ls_cod_prodotto,   
							 :ls_des_prodotto,   
							 :ld_quan_in_evasione,   
							 :ld_prezzo_vendita,   
							 :ld_sconto_1,   
							 :ld_sconto_2,  
							 :ld_provvigione_1,
							 :ld_provvigione_2,
							 :ls_cod_iva,   
							 :ls_cod_tipo_movimento,
							 null,
							 null,
							 null,
							 :ls_nota_dettaglio,   
							 null,
							 null,
							 null,
							 :ll_anno_commessa,   
							 :ll_num_commessa,   
							 :ls_cod_centro_costo,   
							 :ld_sconto_3,   
							 :ld_sconto_4,   
							 :ld_sconto_5,   
							 :ld_sconto_6,   
							 :ld_sconto_7,   
							 :ld_sconto_8,   
							 :ld_sconto_9,   
							 :ld_sconto_10,
							 null,
							 :ld_fat_conversione_ven,
							 :ll_anno_reg_des_mov,
							 :ll_num_reg_des_mov,
							 :fl_anno_registrazione[ll_i],   
							 :fl_num_registrazione[ll_i],   
							 :ll_prog_riga_ord_ven,
							 :ls_cod_versione,
                      :ll_num_confezioni,
					       :ll_num_pezzi_confezione,
							 'I',
							 :ld_quantita_um,
							 :ld_prezzo_um);
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la scrittura Dettagli Fatture Vendita."
				close cu_evas_ord_ven;
				destroy lds_rag_documenti
				return -1
			end if
	
			if ls_flag_tipo_det_ven = "M" then
				update anag_prodotti  
					set quan_assegnata = quan_assegnata - :ld_quan_in_evasione,
						 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
				if sqlca.sqlcode <> 0 then
					fs_errore = "Si è verificato un errore in fase di aggiornamento magazzino."
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
				
				update stock 
				set 	 quan_assegnata = quan_assegnata - :ld_quan_in_evasione,   
						 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione  
				where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
						 stock.cod_prodotto = :ls_cod_prodotto and 
						 stock.cod_deposito = :ls_cod_deposito[1] and 
						 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
						 stock.cod_lotto = :ls_cod_lotto[1] and 
						 stock.data_stock = :ldt_data_stock[1] and 
						 stock.prog_stock = :ll_progr_stock[1];
	
				if sqlca.sqlcode <> 0 then
					fs_errore = "Si è verificato un errore in fase di aggiornamento stock."
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
			end if				
	
			ls_tabella_orig = "det_ord_ven_stat"
			ls_tabella_des = "det_fat_ven_stat"
			ls_prog_riga_doc_orig = "prog_riga_ord_ven"
			ls_prog_riga_doc_des = "prog_riga_fat_ven"
	
			if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, fl_anno_registrazione[ll_i], fl_num_registrazione[ll_i], ll_prog_riga_ord_ven, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven) = -1 then
				close cu_evas_ord_ven;
				destroy lds_rag_documenti
				return -1
			end if	
	
			if ls_cod_tipo_analisi_fat <> ls_cod_tipo_analisi_ord then
				if f_crea_distribuzione(ls_cod_tipo_analisi_fat, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_fat_ven, 6) = -1 then
					fs_errore = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
					close cu_evas_ord_ven;
					destroy lds_rag_documenti
					return -1
				end if
			end if
	
			if ld_quan_ordine <= ld_quan_evasa + ld_quan_in_evasione then
				ls_flag_evasione = "E"
			else
				ls_flag_evasione = "P"
			end if
			
			update det_ord_ven  
				set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione,
					 quan_evasa = quan_evasa + :ld_quan_in_evasione,
					 flag_evasione = :ls_flag_evasione
			 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					 det_ord_ven.anno_registrazione = :fl_anno_registrazione[ll_i] and  
					 det_ord_ven.num_registrazione = :fl_num_registrazione[ll_i] and
					 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
		
			if sqlca.sqlcode = -1 then
				fs_errore = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Vendita."
				close cu_evas_ord_ven;
				destroy lds_rag_documenti
				return -1
			end if
	
			if ls_flag_sola_iva = 'N' then
				ld_val_riga = ld_quan_in_evasione * ld_prezzo_vendita
				ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
				ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
				ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
				ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
				ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
				ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
				ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
				ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
				ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
				ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
				ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
				ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
				ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
				ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
				ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
				ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
				ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
				ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
				ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
		
				if ls_cod_valuta = ls_lire then
					ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
				else
					ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
				end if   
		
				if ls_flag_tipo_det_ven = "M" or &
					ls_flag_tipo_det_ven = "C" or &
					ls_flag_tipo_det_ven = "N" then
					ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
					ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
					ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
					ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
				elseif ls_flag_tipo_det_ven = "S" then
					ld_val_evaso = ld_val_riga_sconto_10 * -1
				else
					ld_val_evaso = ld_val_riga_sconto_10
				end if
				ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
			end if
	
			if ls_cod_valuta = ls_lire then
				ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
			end if   
	
			delete from evas_ord_ven 
			where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
							evas_ord_ven.anno_registrazione = :fl_anno_registrazione[ll_i] and
							evas_ord_ven.num_registrazione = :fl_num_registrazione[ll_i] and 
							evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
							evas_ord_ven.prog_riga = :ll_prog_riga and
							evas_ord_ven.cod_utente = :s_cs_xx.cod_utente;
	
			if sqlca.sqlcode = -1 then
				fs_errore = "Si è verificato un errore in fase di cancellazione Evasione Ordini."
				close cu_evas_ord_ven;
				destroy lds_rag_documenti
				return -1
			end if
		end if
	loop
	close cu_evas_ord_ven;

	if ld_tot_val_ordine <= ld_tot_val_evaso then
		ls_flag_evasione = "E"
	else
		ls_flag_evasione = "P"
	end if

//	update tes_ord_ven  
//		set tot_val_evaso = :ld_tot_val_evaso
//	 where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione[ll_i] and  
//			 tes_ord_ven.num_registrazione = :fl_num_registrazione[ll_i];
//
//	if sqlca.sqlcode <> 0 then
//		fs_errore = "Si è verificato un errore in fase di aggiornamento Valore Evaso in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
//		destroy lds_rag_documenti
//		return -1
//	end if
	
	if uof_calcola_stato_ordine(fl_anno_registrazione[ll_i],fl_num_registrazione[ll_i]) = -1 then
		fs_errore = "Si è verificato un errore in fase di aggiornamento stato dell'ordine in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
		destroy lds_rag_documenti
		return -1
	end if

	if (string(fl_num_registrazione[ll_i]) + ls_key_sort) <> (string(ll_num_ordine_old) + ls_key_sort_old) then
		select nota_testata, nota_piede
			into :ls_nota_testata_1, :ls_nota_piede_1
		from tes_fat_ven
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 anno_registrazione = :ll_anno_registrazione and  
				 num_registrazione = :ll_num_registrazione;
		ls_nota_testata_1 = ls_nota_testata_1 + ls_nota_testata
		ls_nota_piede_1 = ls_nota_piede_1 + ls_nota_piede
		
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_testata_1) > 255 then
			ls_nota_testata_1 = left(ls_nota_testata_1, 255)
		end if
		if ls_db = "SYBASE_ASE" and len(ls_nota_piede_1) > 255 then
			ls_nota_piede_1 = left(ls_nota_piede_1, 255)
		end if
		// -----  fine controllo lunghezza note per ASE

		update tes_fat_ven  
			set nota_testata = :ls_nota_testata_1,
				 nota_piede = :ls_nota_piede_1
		 where tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tes_fat_ven.anno_registrazione = :ll_anno_registrazione and  
				 tes_fat_ven.num_registrazione = :ll_num_registrazione;
	
		if sqlca.sqlcode = -1 then
			fs_errore = "Si è verificato un errore in fase di aggiornamento Testata Fattura Vendita."
			destroy lds_rag_documenti
			return -1
		end if
		
		luo_calcolo = create uo_calcola_documento_euro

		if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"fat_ven",fs_errore) <> 0 then
			destroy luo_calcolo
			return -1
		end if
		
		destroy luo_calcolo
		
		fl_anno_reg_dest[ll_i] = ll_anno_registrazione
		fl_num_reg_dest[ll_i] = ll_num_registrazione
	end if
	
	ls_key_sort_old = ls_key_sort
	ll_num_ordine_old = fl_num_registrazione[ll_i]
next

destroy lds_rag_documenti

return 0
end function

public function integer uof_genera_bolle_singole (long al_anno_ordine, long al_num_ordine, string as_cod_tipo_ord_ven, ref string ls_messaggio, ref long al_anno_bolla, ref long al_num_bolla);boolean lb_riferimento
long ll_num_registrazione, &
	  ll_prog_riga_ord_ven, ll_anno_commessa, ll_num_commessa, ll_progr_stock[1], &
	  ll_prog_riga_bol_ven, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga, &
	  ll_anno_registrazione
string ls_cod_tipo_bol_ven, ls_nota_testata, ls_cod_cliente[1], &
		 ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
		 ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione[1], ls_cod_deposito[1], &
		 ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, &
		 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
		 ls_cod_resa, ls_nota_piede, ls_cod_banca, ls_cod_tipo_det_ven, ls_cod_prodotto, &
		 ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, &
		 ls_flag_tipo_det_ven, ls_cod_tipo_analisi_bol, ls_cod_tipo_analisi_ord, &
		 ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, ls_causale_trasporto, &
		 ls_flag_riep_fat, ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, &
		 ls_prog_riga_doc_des, ls_num_ord_cliente, ls_cod_lotto[1], ls_cod_tipo_movimento, &
		 ls_vostro_ordine, ls_vostro_ordine_data, ls_flag_evasione, ls_lire, ls_flag_sola_iva, &
		 ls_cod_fornitore[], ls_causale_trasporto_tab, ls_cod_versione, ls_cod_causale_tab, &
		 ls_flag_abilita_rif_ord_ven,  ls_cod_tipo_det_ven_rif_ord_ven, ls_des_riferimento_ord_ven, &
  		 ls_flag_rif_anno_ord_ven, ls_flag_rif_num_ord_ven, ls_flag_rif_data_ord_ven, &
		 ls_des_riferimento, ls_cod_iva_rif, ls_cod_causale, ls_note_dettaglio_rif, &
		 ls_db, ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det, &
		 ls_cod_tipo_ord_ven
datetime ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock[1], &
		   ldt_vostro_ordine_data
decimal ld_cambio_ven, ld_quan_in_evasione, ld_prezzo_vendita, ld_fat_conversione_ven, &
		 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, &
		 ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_testata, &
		 ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, &
		 ld_quan_ordine, ld_quan_evasa, ld_tot_val_evaso, ld_tot_val_ordine, &
		 ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, &
		 ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, &
		 ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, &
		 ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, &
		 ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quantita_um, ld_prezzo_um, &
		 ld_num_confezioni, ld_num_pezzi_confezione 
integer li_risposta

uo_calcola_documento_euro luo_calcolo


li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
if upper(ls_db) = "MSSQL" then ls_db="SYBASE_ASE"

ll_anno_registrazione = f_anno_esercizio()
lb_riferimento = true
setnull(ls_cod_fornitore[1])

select tab_tipi_ord_ven.cod_tipo_bol_ven,  
		 tab_tipi_ord_ven.cod_tipo_analisi  
into   :ls_cod_tipo_bol_ven,  
		 :ls_cod_tipo_analisi_ord
from   tab_tipi_ord_ven  
where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tab_tipi_ord_ven.cod_tipo_ord_ven = :as_cod_tipo_ord_ven;

if sqlca.sqlcode <> 0 then
	ls_messaggio = "Errore durante la lettura della tabella Tipi Ordini Vendita: " + sqlca.sqlerrtext
	return -1
end if

// michela: se il tipo di bolla è nullo, avverto l'utente --> 24/11/2003
if isnull(ls_cod_tipo_bol_ven) then
	ls_messaggio = "Tipo di bolla non specificato per il Tipo Ordine di Vendita " + as_cod_tipo_ord_ven
	return -1
end if

if not isnull(ls_cod_tipo_bol_ven) then
	select con_bol_ven.num_registrazione
	into   :ll_num_registrazione
	from   con_bol_ven
	where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura della tabella Controllo Bolle Vendita: " + sqlca.sqlerrtext
		return -1
	end if

	ll_num_registrazione ++
	
	update con_bol_ven
	set    con_bol_ven.num_registrazione = :ll_num_registrazione
	where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la scrittura sulla tabella Controllo Bolle Vendita: " + sqlca.sqlerrtext
		return -1
	end if

	select tes_ord_ven.cod_operatore,   
			 tes_ord_ven.data_registrazione,   
			 tes_ord_ven.cod_cliente,   
			 tes_ord_ven.cod_des_cliente,   
			 tes_ord_ven.rag_soc_1,   
			 tes_ord_ven.rag_soc_2,   
			 tes_ord_ven.indirizzo,   
			 tes_ord_ven.localita,   
			 tes_ord_ven.frazione,   
			 tes_ord_ven.cap,   
			 tes_ord_ven.provincia,   
			 tes_ord_ven.cod_deposito,   
			 tes_ord_ven.cod_ubicazione,   
			 tes_ord_ven.cod_valuta,   
			 tes_ord_ven.cambio_ven,   
			 tes_ord_ven.cod_tipo_listino_prodotto,   
			 tes_ord_ven.cod_pagamento,   
			 tes_ord_ven.sconto,   
			 tes_ord_ven.cod_agente_1,
			 tes_ord_ven.cod_agente_2,
			 tes_ord_ven.cod_banca,   
			 tes_ord_ven.num_ord_cliente,   
			 tes_ord_ven.data_ord_cliente,   
			 tes_ord_ven.cod_imballo,   
			 tes_ord_ven.aspetto_beni,
			 tes_ord_ven.peso_netto,
			 tes_ord_ven.peso_lordo,
			 tes_ord_ven.num_colli,
			 tes_ord_ven.cod_vettore,   
			 tes_ord_ven.cod_inoltro,   
			 tes_ord_ven.cod_mezzo,
			 tes_ord_ven.causale_trasporto,
			 tes_ord_ven.cod_porto,   
			 tes_ord_ven.cod_resa,   
			 tes_ord_ven.nota_testata,   
			 tes_ord_ven.nota_piede,   
			 tes_ord_ven.flag_riep_fat,
			 tes_ord_ven.tot_val_evaso,
			 tes_ord_ven.tot_val_ordine,
			 tes_ord_ven.cod_banca_clien_for,
			 tes_ord_ven.cod_causale,
			 tes_ord_ven.flag_doc_suc_tes,
			 tes_ord_ven.flag_doc_suc_pie,
			 tes_ord_ven.cod_tipo_ord_ven
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ls_flag_riep_fat,
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_cod_causale,
			 :ls_flag_doc_suc_tes,
			 :ls_flag_doc_suc_pie,
			 :ls_cod_tipo_ord_ven
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :al_anno_ordine and  
			 tes_ord_ven.num_registrazione = :al_num_ordine;

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura Testata Ordine Vendita: " + sqlca.sqlerrtext
		return -1
	end if
	
	// stefanop: 18/07/2012 - l'operatore che inserire è SEMPRE l'operatore collegato ad Apice
	// ------------------------------------------------------
	ls_cod_operatore = guo_functions.uof_get_operatore_utente()
	// ------------------------------------------------------

	select tab_tipi_bol_ven.cod_tipo_analisi,
			 tab_tipi_bol_ven.causale_trasporto,
			 tab_tipi_bol_ven.cod_causale,
			 tab_tipi_bol_ven.flag_abilita_rif_ord_ven,
			 tab_tipi_bol_ven.cod_tipo_det_ven_rif_ord_ven,
			 tab_tipi_bol_ven.des_riferimento_ord_ven,
			 tab_tipi_bol_ven.flag_rif_anno_ord_ven,
			 tab_tipi_bol_ven.flag_rif_num_ord_ven,
			 tab_tipi_bol_ven.flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi_bol,
			 :ls_causale_trasporto_tab,
			 :ls_cod_causale_tab,
			 :ls_flag_abilita_rif_ord_ven,
			 :ls_cod_tipo_det_ven_rif_ord_ven,
			 :ls_des_riferimento_ord_ven,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_bol_ven  
	where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Si è verificato un errore nella lettura della Tabella Tipi Bolle: " + sqlca.sqlerrtext
		return -1
	end if


	ldt_data_registrazione = datetime(today())



//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------

//	if isnull(ls_nota_testata) then
//		ls_nota_testata = ""
//	end if

	if not isnull(ls_num_ord_cliente) and ls_cod_tipo_det_ven_rif_ord_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if

	if ls_cod_tipo_det_ven_rif_ord_ven = "T" then
		ls_nota_testata = ls_des_riferimento_ord_ven + string(al_anno_ordine) + "/" + &
								string(al_num_ordine) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
	end if
	
	// ----- controllo lunghezza note per ASE
	if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE

	
	
	if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
		ls_causale_trasporto = ls_causale_trasporto_tab
	end if
	if isnull(ls_cod_causale) then
		ls_cod_causale = ls_cod_causale_tab
	end if
	select parametri_azienda.stringa
	into   :ls_lire
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'CVL';

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore in lettura del parametro CVL da parametri_azienda: " + sqlca.sqlerrtext
		return -1
	end if

	select tab_pagamenti.sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if

	insert into tes_bol_ven
					(cod_azienda,   
					 anno_registrazione,   
					 num_registrazione,   
					 data_registrazione,   
					 cod_operatore,   
					 cod_tipo_bol_ven,   
					 cod_cliente,
					 cod_fornitore,
					 cod_des_cliente,   
					 cod_fil_fornitore,
					 rag_soc_1,   
					 rag_soc_2,   
					 indirizzo,   
					 localita,   
					 frazione,   
					 cap,   
					 provincia,   
					 cod_deposito,   
					 cod_deposito_tras,
					 cod_ubicazione,
					 cod_valuta,   
					 cambio_ven,   
					 cod_tipo_listino_prodotto,   
					 cod_pagamento,   
					 sconto,   
					 cod_agente_1,
					 cod_agente_2,
					 cod_banca,   
					 num_ord_cliente,   
					 data_ord_cliente,   
					 cod_imballo,   
					 aspetto_beni,
					 peso_netto,
					 peso_lordo,
					 num_colli,
					 cod_vettore,   
					 cod_inoltro,   
					 cod_mezzo,
					 causale_trasporto,
					 cod_porto,   
					 cod_resa,   
					 cod_documento,
					 numeratore_documento,
					 anno_documento,
					 num_documento,
					 data_bolla,
					 nota_testata,   
					 nota_piede,   
					 flag_fuori_fido,
					 flag_blocco,   
					 flag_movimenti,  
					 flag_riep_fat,
					 cod_banca_clien_for,
					 flag_gen_fat,
					 cod_causale,
					 flag_doc_suc_tes,
					 flag_st_note_tes,
					 flag_doc_suc_pie,
					 flag_st_note_pie)  					 
	values      (:s_cs_xx.cod_azienda,   
					 :ll_anno_registrazione,   
					 :ll_num_registrazione,   
					 :ldt_data_registrazione,   
					 :ls_cod_operatore,   
					 :ls_cod_tipo_bol_ven,   
					 :ls_cod_cliente[1],
					 null,
					 :ls_cod_des_cliente,   
					 null,
					 :ls_rag_soc_1,   
					 :ls_rag_soc_2,   
					 :ls_indirizzo,   
					 :ls_localita,   
					 :ls_frazione,   
					 :ls_cap,   
					 :ls_provincia,   
					 :ls_cod_deposito[1],   
					 null,
					 :ls_cod_ubicazione[1],
					 :ls_cod_valuta,   
					 :ld_cambio_ven,   
					 :ls_cod_tipo_listino_prodotto,   
					 :ls_cod_pagamento,   
					 :ld_sconto_testata,
					 :ls_cod_agente_1,
					 :ls_cod_agente_2,
					 :ls_cod_banca,   
					 :ls_num_ord_cliente,   
					 :ldt_data_ord_cliente,   
					 :ls_cod_imballo,   
					 :ls_aspetto_beni,
					 :ld_peso_netto,
					 :ld_peso_lordo,
					 :ld_num_colli,
					 :ls_cod_vettore,   
					 :ls_cod_inoltro,   
					 :ls_cod_mezzo,   
					 :ls_causale_trasporto,
					 :ls_cod_porto,   
					 :ls_cod_resa,   
					 null,
					 null,
					 0,
					 0,
					 null,
					 :ls_nota_testata,   
					 :ls_nota_piede,   
					 'N',
					 'N',   
					 'N',
					 :ls_flag_riep_fat,
					 :ls_cod_banca_clien_for,
					 'N',
					 :ls_cod_causale,
					 'I',
					 'I',
					 'I',
					 'I');

	 if sqlca.sqlcode <> 0 then
		 ls_messaggio = "Errore durante la scrittura Testata Bolle Vendita: " + sqlca.sqlerrtext
		 return -1
	 end if

	 if uof_crea_nota_fissa("BOL_VEN", ll_anno_registrazione, ll_num_registrazione, ls_messaggio) = -1 then return -1

	 declare cu_evas_ord_ven cursor for  
	 select  evas_ord_ven.prog_riga_ord_ven,   
				evas_ord_ven.prog_riga,   
				evas_ord_ven.cod_deposito,   
				evas_ord_ven.cod_ubicazione,   
				evas_ord_ven.cod_lotto,   
				evas_ord_ven.data_stock,   
				evas_ord_ven.prog_stock,   
				evas_ord_ven.quan_in_evasione
	from     evas_ord_ven  
	where    evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				evas_ord_ven.anno_registrazione = :al_anno_ordine and
				evas_ord_ven.num_registrazione = :al_num_ordine and
				evas_ord_ven.cod_utente = :s_cs_xx.cod_utente
	order by evas_ord_ven.prog_riga_ord_ven asc,   
				evas_ord_ven.prog_riga asc;

	open cu_evas_ord_ven;
	
	lb_riferimento = true
	ll_prog_riga_bol_ven = 0
	do while 0 = 0
		fetch cu_evas_ord_ven into :ll_prog_riga_ord_ven,   
											:ll_prog_riga,
											:ls_cod_deposito[1],   
											:ls_cod_ubicazione[1],   
											:ls_cod_lotto[1],   
											:ldt_data_stock[1],   
											:ll_progr_stock[1],   
											:ld_quan_in_evasione;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode < 0 then
			ls_messaggio =  "Errore durante la lettura Evasione Ordini Vendita: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		
		select	det_ord_ven.cod_tipo_det_ven,   
					det_ord_ven.cod_misura,   
					det_ord_ven.cod_prodotto,   
					det_ord_ven.des_prodotto,   
					det_ord_ven.quan_ordine,
					det_ord_ven.prezzo_vendita,   
					det_ord_ven.fat_conversione_ven,   
					det_ord_ven.sconto_1,   
					det_ord_ven.sconto_2,  
					det_ord_ven.provvigione_1,
					det_ord_ven.provvigione_2,
					det_ord_ven.cod_iva,   
					det_ord_ven.quan_evasa,
					det_ord_ven.nota_dettaglio,   
					det_ord_ven.anno_commessa,
					det_ord_ven.num_commessa,
					det_ord_ven.cod_centro_costo,
					det_ord_ven.sconto_3,   
					det_ord_ven.sconto_4,   
					det_ord_ven.sconto_5,   
					det_ord_ven.sconto_6,   
					det_ord_ven.sconto_7,   
					det_ord_ven.sconto_8,   
					det_ord_ven.sconto_9,   
					det_ord_ven.sconto_10,
					det_ord_ven.cod_versione,
					det_ord_ven.num_confezioni,
					det_ord_ven.num_pezzi_confezione,
					det_ord_ven.flag_doc_suc_det,
					det_ord_ven.quantita_um,
					det_ord_ven.prezzo_um
		into		:ls_cod_tipo_det_ven, 
					:ls_cod_misura, 
					:ls_cod_prodotto, 
					:ls_des_prodotto, 
					:ld_quan_ordine,
					:ld_prezzo_vendita, 
					:ld_fat_conversione_ven, 
					:ld_sconto_1, 
					:ld_sconto_2, 
					:ld_provvigione_1, 
					:ld_provvigione_2, 
					:ls_cod_iva, 
					:ld_quan_evasa,
					:ls_nota_dettaglio, 
					:ll_anno_commessa,
					:ll_num_commessa,
					:ls_cod_centro_costo,
					:ld_sconto_3, 
					:ld_sconto_4, 
					:ld_sconto_5, 
					:ld_sconto_6, 
					:ld_sconto_7, 
					:ld_sconto_8, 
					:ld_sconto_9, 
					:ld_sconto_10,
					:ls_cod_versione,
					:ld_num_confezioni,
					:ld_num_pezzi_confezione,
					:ls_flag_doc_suc_det,
					:ld_quantita_um,
					:ld_prezzo_um
		  from 	det_ord_ven  
		 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					det_ord_ven.anno_registrazione = :al_anno_ordine and  
					det_ord_ven.num_registrazione = :al_num_ordine and
					det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven
		order by det_ord_ven.prog_riga_ord_ven asc;
	
		if sqlca.sqlcode < 0 then
			ls_messaggio =  "Errore durante la lettura Dettagli Ordini Vendita: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
// ricalcolo la quantità di vendita nel caso di evasioni parziali
		if ld_quan_in_evasione <> ld_quan_ordine then
			ld_quantita_um = round(ld_quan_in_evasione * ld_fat_conversione_ven, 4)
		end if
//------------------------------------------------- Modifica Nicola ---------------------------------------------
		if ls_flag_doc_suc_det = 'I' then //nota dettaglio
			select flag_doc_suc_or
			  into :ls_flag_doc_suc_det
			  from tab_tipi_det_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		end if		
		
		
		if ls_flag_doc_suc_det = 'N' then
			ls_nota_dettaglio = ""
		end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------			
			
		select tab_tipi_det_ven.flag_tipo_det_ven,
				 tab_tipi_det_ven.cod_tipo_movimento,
				 tab_tipi_det_ven.flag_sola_iva
		into   :ls_flag_tipo_det_ven,
				 :ls_cod_tipo_movimento,
				 :ls_flag_sola_iva
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode < 0 then
			ls_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
				ls_messaggio = "Si è verificato un errore in fase di creazione destinazioni movimenti."
				close cu_evas_ord_ven;
				return -1
			end if

			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if lb_riferimento and ls_flag_abilita_rif_ord_ven = "D" then
			lb_riferimento = false
			setnull(ls_note_dettaglio_rif)
			ls_des_riferimento = ls_des_riferimento_ord_ven
			if ls_flag_rif_anno_ord_ven = "S" and not isnull(al_anno_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" +string(al_anno_ordine)
			end if
			if ls_flag_rif_num_ord_ven = "S" and not isnull(al_num_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(al_num_ordine)
			end if
			if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
			end if
			if not isnull(ls_num_ord_cliente) then
				ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
				if not isnull(ldt_data_ord_cliente) then
					ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
				end if
			end if
									
			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_ord_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
				g_mb.messagebox("Generazione Bolla Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if
			
			insert into det_bol_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_bol_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 progr_stock,
							 data_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_consegnata,   
							 prezzo_vendita,   
							 fat_conversione_ven,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 nota_dettaglio,   
							 anno_registrazione_ord_ven,   
							 num_registrazione_ord_ven,   
							 prog_riga_ord_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 anno_reg_bol_acq,
							 num_reg_bol_acq,
							 prog_riga_bol_acq,
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_doc_suc_det,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)							 
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_bol_ven,   
							 :ls_cod_tipo_det_ven_rif_ord_ven,   
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,   
							 null,   
							 :ls_des_riferimento,   
							 0,   
							 0,   
							 1,   
							 0,   
							 0,  
							 0,
							 0,
							 :ls_cod_iva_rif,   
							 null,
							 null,
							 :ls_note_dettaglio_rif,   
							 null,   
							 null,   
							 null,
							 null,   
							 null,   
							 null,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 0,
							 0,
							 'I',
							 'I',
							 0,
							 0);
	
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Errore durante la scrittura Dettagli Bolle Vendita: " + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
			ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		end if	

		insert into det_bol_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_bol_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 progr_stock,
						 data_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_consegnata,   
						 prezzo_vendita,   
						 fat_conversione_ven,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 nota_dettaglio,   
						 anno_registrazione_ord_ven,   
						 num_registrazione_ord_ven,   
						 prog_riga_ord_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 anno_reg_bol_acq,
						 num_reg_bol_acq,
						 prog_riga_bol_acq,
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 cod_versione,
						 num_confezioni,
						 num_pezzi_confezione,
						 flag_doc_suc_det,
						 flag_st_note_det,
						 quantita_um,
						 prezzo_um)
	  values			(:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_bol_ven,   
						 :ls_cod_tipo_det_ven,   
						 :ls_cod_deposito[1],
						 :ls_cod_ubicazione[1],
						 :ls_cod_lotto[1],
						 :ll_progr_stock[1],
						 :ldt_data_stock[1],
						 :ls_cod_misura,   
						 :ls_cod_prodotto,   
						 :ls_des_prodotto,   
						 :ld_quan_in_evasione,   
						 :ld_prezzo_vendita,   
						 :ld_fat_conversione_ven,   
						 :ld_sconto_1,   
						 :ld_sconto_2,  
						 :ld_provvigione_1,
						 :ld_provvigione_2,
						 :ls_cod_iva,   
						 :ls_cod_tipo_movimento,
						 null,
						 :ls_nota_dettaglio,   
						 :al_anno_ordine,   
						 :al_num_ordine,   
						 :ll_prog_riga_ord_ven,
						 :ll_anno_commessa,   
						 :ll_num_commessa,   
						 :ls_cod_centro_costo,   
						 :ld_sconto_3,   
						 :ld_sconto_4,   
						 :ld_sconto_5,   
						 :ld_sconto_6,   
						 :ld_sconto_7,   
						 :ld_sconto_8,   
						 :ld_sconto_9,   
						 :ld_sconto_10,
						 null,
						 null,
						 null,
						 null,
						 :ll_anno_reg_des_mov,
						 :ll_num_reg_des_mov,
						 :ls_cod_versione,
						 :ld_num_confezioni,
						 :ld_num_pezzi_confezione,
						 'I',
						 'I',
						 :ld_quantita_um,
						 :ld_prezzo_um);
						 
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la scrittura Dettagli Bolle Vendita: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			update anag_prodotti  
				set quan_assegnata = quan_assegnata - :ld_quan_in_evasione,
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Si è verificato un errore in fase di aggiornamento magazzino: " + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
			
			update stock 
			set 	 quan_assegnata = quan_assegnata - :ld_quan_in_evasione,   
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione  
			where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
					 stock.cod_prodotto = :ls_cod_prodotto and 
					 stock.cod_deposito = :ls_cod_deposito[1] and 
					 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
					 stock.cod_lotto = :ls_cod_lotto[1] and 
					 stock.data_stock = :ldt_data_stock[1] and 
					 stock.prog_stock = :ll_progr_stock[1];

			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Si è verificato un errore in fase di aggiornamento stock: " + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
		end if				

		ls_tabella_orig = "det_ord_ven_stat"
		ls_tabella_des = "det_bol_ven_stat"
		ls_prog_riga_doc_orig = "prog_riga_ord_ven"
		ls_prog_riga_doc_des = "prog_riga_bol_ven"

		if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, al_anno_ordine, al_num_ordine, ll_prog_riga_ord_ven, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven) = -1 then
			close cu_evas_ord_ven;
			return -1
		end if	

		if ls_cod_tipo_analisi_bol <> ls_cod_tipo_analisi_ord then
			if f_crea_distribuzione(ls_cod_tipo_analisi_bol, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_bol_ven, 5) = -1 then
				ls_messaggio = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if ld_quan_ordine <= ld_quan_evasa + ld_quan_in_evasione then
			ls_flag_evasione = "E"
		else
			ls_flag_evasione = "P"
		end if
		
		update det_ord_ven  
			set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione,
				 quan_evasa = quan_evasa + :ld_quan_in_evasione,
				 flag_evasione = :ls_flag_evasione
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :al_anno_ordine and  
				 det_ord_ven.num_registrazione = :al_num_ordine and
				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
		if sqlca.sqlcode < 0 then
			ls_messaggio = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Vendita: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_sola_iva = 'N' then
			ld_val_riga = ld_quan_in_evasione * ld_prezzo_vendita
			ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
			ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
			ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
			ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
			ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
			ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
			ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
			ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
			ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
			ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
			ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
			ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
			ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
			ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
			ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
			ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
			ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
			ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
			ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
			if ls_cod_valuta = ls_lire then
				ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
			else
				ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
			end if   
	
			if ls_flag_tipo_det_ven = "M" or &
				ls_flag_tipo_det_ven = "C" or &
				ls_flag_tipo_det_ven = "N" then
				ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
				ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
				ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
				ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
			elseif ls_flag_tipo_det_ven = "S" then
				ld_val_evaso = ld_val_riga_sconto_10 * -1
			else
				ld_val_evaso = ld_val_riga_sconto_10
			end if
			ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
		end if

		if ls_cod_valuta = ls_lire then
			ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
		end if   

		delete from evas_ord_ven 
		where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
						evas_ord_ven.anno_registrazione = :al_anno_ordine and
						evas_ord_ven.num_registrazione = :al_num_ordine and 
						evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
						evas_ord_ven.prog_riga = :ll_prog_riga and
						evas_ord_ven.cod_utente = :s_cs_xx.cod_utente;

		if sqlca.sqlcode < 0 then
			ls_messaggio = "Si è verificato un errore in fase di cancellazione Evasione Ordini: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if
	loop
	close cu_evas_ord_ven;

// ------------------------------  NUOVO SISTEMA DI CALCOLO STATO ORDINE ---------------------------------------

	uof_calcola_stato_ordine(al_anno_ordine, al_num_ordine)

end if

luo_calcolo = create uo_calcola_documento_euro

if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_ven",ls_messaggio) <> 0 then
	destroy luo_calcolo
	return -1
end if

destroy luo_calcolo

al_anno_bolla = ll_anno_registrazione

al_num_bolla = ll_num_registrazione

return 0
end function

public function integer uof_genera_fatture_singole (long al_anno_ordine, long al_num_ordine, string as_cod_tipo_ord_ven, ref string ls_messaggio, ref long al_anno_fattura, ref long al_num_fattura);boolean lb_riferimento
long ll_num_registrazione, &
	  ll_prog_riga_ord_ven, ll_anno_commessa, ll_num_commessa, ll_progr_stock[1], &
	  ll_prog_riga_fat_ven, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga, &
	  ll_anno_registrazione
string ls_cod_tipo_fat_ven, ls_nota_testata, ls_cod_cliente[1], &
		 ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
		 ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione[1], ls_cod_deposito[1], &
		 ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, &
		 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
		 ls_cod_resa, ls_nota_piede, ls_cod_banca, ls_cod_tipo_det_ven, ls_cod_prodotto, &
		 ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, &
		 ls_flag_tipo_det_ven, ls_cod_tipo_analisi_fat, ls_cod_tipo_analisi_ord, &
		 ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, ls_causale_trasporto, &
		 ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, &
		 ls_num_ord_cliente, ls_cod_lotto[1], ls_cod_tipo_movimento, ls_vostro_ordine, &
		 ls_vostro_ordine_data, ls_flag_evasione, ls_lire, ls_flag_sola_iva, &
		 ls_cod_fornitore[], ls_causale_trasporto_tab,ls_cod_versione, ls_flag_abilita_rif_bol_ven, &
		 ls_cod_tipo_det_ven_rif_bol_ven, ls_des_riferimento_ord_ven, ls_flag_rif_anno_ord_ven, &
		 ls_flag_rif_num_ord_ven, ls_flag_rif_data_ord_ven, ls_des_riferimento, ls_cod_iva_rif, &
		 ls_note_dettaglio_rif, ls_db, ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, &
		 ls_flag_doc_suc_det, ls_cod_tipo_ord_ven, ls_nota_prodotto
datetime ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock[1], &
		 ldt_vostro_ordine_data
decimal ld_cambio_ven, ld_quan_in_evasione, ld_prezzo_vendita, ld_fat_conversione_ven, &
		 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, &
		 ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_testata, &
		 ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, &
		 ld_quan_ordine, ld_quan_evasa, ld_tot_val_evaso, ld_tot_val_ordine, &
		 ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, &
		 ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, &
		 ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, &
		 ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, &
		 ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quantita_um, ld_prezzo_um, &
		 ld_num_pezzi_confezione, ld_num_confezioni
integer li_risposta

uo_calcola_documento_euro luo_calcolo

guo_functions.uof_get_parametro_azienda( "IRF", ref il_incremento_riga)
if isnull(il_incremento_riga) or il_incremento_riga = 0 then
	il_incremento_riga = 10
end if

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
if upper(ls_db) = "MSSQL" then ls_db="SYBASE_ASE"

ll_anno_registrazione = f_anno_esercizio()
lb_riferimento = true
setnull(ls_cod_fornitore[1])

select tab_tipi_ord_ven.cod_tipo_fat_ven,  
		 tab_tipi_ord_ven.cod_tipo_analisi  
into   :ls_cod_tipo_fat_ven,  
		 :ls_cod_tipo_analisi_ord
from   tab_tipi_ord_ven  
where  tab_tipi_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tab_tipi_ord_ven.cod_tipo_ord_ven = :as_cod_tipo_ord_ven;

if sqlca.sqlcode <> 0 then
	ls_messaggio = "Errore durante la lettura della tabella Tipi Fatture Vendita: " + sqlca.sqlerrtext
	return -1
end if

if not isnull(ls_cod_tipo_fat_ven) then
	select con_fat_ven.num_registrazione
	into   :ll_num_registrazione
	from   con_fat_ven
	where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura della tabella Controllo Fatture Vendita: " + sqlca.sqlerrtext
		return -1
	end if

	ll_num_registrazione ++
	
	update con_fat_ven
	set    con_fat_ven.num_registrazione = :ll_num_registrazione
	where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la scrittura sulla tabella Controllo Fatture Vendita: " + sqlca.sqlerrtext
		return -1
	end if

	select tes_ord_ven.cod_operatore,   
			 tes_ord_ven.data_registrazione,   
			 tes_ord_ven.cod_cliente,   
			 tes_ord_ven.cod_des_cliente,   
			 tes_ord_ven.rag_soc_1,   
			 tes_ord_ven.rag_soc_2,   
			 tes_ord_ven.indirizzo,   
			 tes_ord_ven.localita,   
			 tes_ord_ven.frazione,   
			 tes_ord_ven.cap,   
			 tes_ord_ven.provincia,   
			 tes_ord_ven.cod_deposito,   
			 tes_ord_ven.cod_ubicazione,   
			 tes_ord_ven.cod_valuta,   
			 tes_ord_ven.cambio_ven,   
			 tes_ord_ven.cod_tipo_listino_prodotto,   
			 tes_ord_ven.cod_pagamento,   
			 tes_ord_ven.sconto,   
			 tes_ord_ven.cod_agente_1,
			 tes_ord_ven.cod_agente_2,
			 tes_ord_ven.cod_banca,   
			 tes_ord_ven.num_ord_cliente,   
			 tes_ord_ven.data_ord_cliente,   
			 tes_ord_ven.cod_imballo,   
			 tes_ord_ven.aspetto_beni,
			 tes_ord_ven.peso_netto,
			 tes_ord_ven.peso_lordo,
			 tes_ord_ven.num_colli,
			 tes_ord_ven.cod_vettore,   
			 tes_ord_ven.cod_inoltro,   
			 tes_ord_ven.cod_mezzo,
			 tes_ord_ven.causale_trasporto,
			 tes_ord_ven.cod_porto,   
			 tes_ord_ven.cod_resa,   
			 tes_ord_ven.nota_testata,   
			 tes_ord_ven.nota_piede,   
			 tes_ord_ven.tot_val_evaso,
			 tes_ord_ven.tot_val_ordine,
			 tes_ord_ven.cod_banca_clien_for,
			 tes_ord_ven.flag_doc_suc_tes,
			 tes_ord_ven.flag_doc_suc_pie,
			 tes_ord_ven.cod_tipo_ord_ven
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_flag_doc_suc_tes,
			 :ls_flag_doc_suc_pie,
			 :ls_cod_tipo_ord_ven
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :al_anno_ordine and  
			 tes_ord_ven.num_registrazione = :al_num_ordine;

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura Testata Ordine Vendita: " + sqlca.sqlerrtext
		return -1
	end if
	
	// stefanop: 18/07/2012 - l'operatore che inserire è SEMPRE l'operatore collegato ad Apice
	// ------------------------------------------------------
	ls_cod_operatore = guo_functions.uof_get_operatore_utente()
	// ------------------------------------------------------

	select tab_tipi_fat_ven.cod_tipo_analisi,
			 tab_tipi_fat_ven.causale_trasporto,
			 tab_tipi_fat_ven.flag_abilita_rif_bol_ven,
			 tab_tipi_fat_ven.cod_tipo_det_ven_rif_bol_ven,
			 tab_tipi_fat_ven.des_riferimento_ord_ven,
			 tab_tipi_fat_ven.flag_rif_anno_ord_ven,
			 tab_tipi_fat_ven.flag_rif_num_ord_ven,
			 tab_tipi_fat_ven.flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi_fat,
			 :ls_causale_trasporto,
			 :ls_flag_abilita_rif_bol_ven,
			 :ls_cod_tipo_det_ven_rif_bol_ven,
			 :ls_des_riferimento_ord_ven,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_fat_ven  
	where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Si è verificato un errore nella lettura della Tabella Tipi Fatture: " + sqlca.sqlerrtext
		return -1
	end if
	
	ldt_data_registrazione = datetime(today())

	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if

	if not isnull(ls_num_ord_cliente)  and ls_cod_tipo_det_ven_rif_bol_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if
	
//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------
	
	
	if ls_cod_tipo_det_ven_rif_bol_ven = "T" then
		ls_nota_testata = "Rif. ns. Ordine Nr. " + string(al_anno_ordine) + "/" + &
								string(al_num_ordine) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
	end if
		
	// ----- controllo lunghezza note per ASE
	if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE


	if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
		ls_causale_trasporto = ls_causale_trasporto_tab
	end if

	select parametri_azienda.stringa
	into   :ls_lire
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'CVL';

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore in lettura del parametro CVL da parametri_azienda: " + sqlca.sqlerrtext
		return -1
	end if

	select tab_pagamenti.sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if

	insert into tes_fat_ven
					(cod_azienda,   
					 anno_registrazione,   
					 num_registrazione,   
					 data_registrazione,   
					 cod_operatore,   
					 cod_tipo_fat_ven,   
					 cod_cliente,
					 cod_des_cliente,   
					 rag_soc_1,   
					 rag_soc_2,   
					 indirizzo,   
					 localita,   
					 frazione,   
					 cap,   
					 provincia,   
					 cod_deposito,   
					 cod_ubicazione,
					 cod_valuta,   
					 cambio_ven,   
					 cod_tipo_listino_prodotto,   
					 cod_pagamento,   
					 sconto,   
					 cod_agente_1,
					 cod_agente_2,
					 cod_banca,   
					 num_ord_cliente,   
					 data_ord_cliente,   
					 cod_imballo,   
					 aspetto_beni,
					 peso_netto,
					 peso_lordo,
					 num_colli,
					 cod_vettore,   
					 cod_inoltro,   
					 cod_mezzo,
					 causale_trasporto,
					 cod_porto,   
					 cod_resa,   
					 cod_documento,
					 numeratore_documento,
					 anno_documento,
					 num_documento,
					 data_fattura,
					 nota_testata,   
					 nota_piede,   
					 flag_fuori_fido,
					 flag_blocco,   
					 flag_movimenti,  
					 flag_contabilita,
					 tot_merci,
					 tot_spese_trasporto,
					 tot_spese_imballo,
					 tot_spese_bolli,
					 tot_spese_varie,
					 tot_sconto_cassa,
					 tot_sconti_commerciali,
					 imponibile_provvigioni_1,
					 imponibile_provvigioni_2,
					 tot_provvigioni_1,
					 tot_provvigioni_2,
					 importo_iva,
					 imponibile_iva,
					 importo_iva_valuta,
					 imponibile_iva_valuta,
					 tot_fattura,
					 tot_fattura_valuta,
					 flag_cambio_zero,
					 cod_banca_clien_for,
					 flag_st_note_tes,
					 flag_st_note_pie)  					 
	values      (:s_cs_xx.cod_azienda,   
					 :ll_anno_registrazione,   
					 :ll_num_registrazione,   
					 :ldt_data_registrazione,   
					 :ls_cod_operatore,   
					 :ls_cod_tipo_fat_ven,   
					 :ls_cod_cliente[1],
					 :ls_cod_des_cliente,   
					 :ls_rag_soc_1,   
					 :ls_rag_soc_2,   
					 :ls_indirizzo,   
					 :ls_localita,   
					 :ls_frazione,   
					 :ls_cap,   
					 :ls_provincia,   
					 :ls_cod_deposito[1],   
					 :ls_cod_ubicazione[1],
					 :ls_cod_valuta,   
					 :ld_cambio_ven,   
					 :ls_cod_tipo_listino_prodotto,   
					 :ls_cod_pagamento,   
					 :ld_sconto_testata,
					 :ls_cod_agente_1,
					 :ls_cod_agente_2,
					 :ls_cod_banca,   
					 :ls_num_ord_cliente,   
					 :ldt_data_ord_cliente,   
					 :ls_cod_imballo,   
					 :ls_aspetto_beni,
					 :ld_peso_netto,
					 :ld_peso_lordo,
					 :ld_num_colli,
					 :ls_cod_vettore,   
					 :ls_cod_inoltro,   
					 :ls_cod_mezzo,   
					 :ls_causale_trasporto,
					 :ls_cod_porto,   
					 :ls_cod_resa,   
					 null,
					 null,
					 0,
					 0,
					 null,
					 :ls_nota_testata,   
					 :ls_nota_piede,   
					 'N',
					 'N',   
					 'N',
					 'N',
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 'N',
					 :ls_cod_banca_clien_for,
					 'I',
					 'I');					 

	 if sqlca.sqlcode <> 0 then
		 ls_messaggio = "Errore durante la scrittura Testata Fatture Vendita: " + sqlca.sqlerrtext
		 return -1
	 end if

	 if uof_crea_nota_fissa("FAT_VEN",ll_anno_registrazione, ll_num_registrazione, ls_messaggio) = -1 then return -1

	 declare cu_evas_ord_ven cursor for  
	 select  evas_ord_ven.prog_riga_ord_ven,   
				evas_ord_ven.prog_riga,   
				evas_ord_ven.cod_deposito,   
				evas_ord_ven.cod_ubicazione,   
				evas_ord_ven.cod_lotto,   
				evas_ord_ven.data_stock,   
				evas_ord_ven.prog_stock,   
				evas_ord_ven.quan_in_evasione
	from     evas_ord_ven  
	where    evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				evas_ord_ven.anno_registrazione = :al_anno_ordine and
				evas_ord_ven.num_registrazione = :al_num_ordine and
				evas_ord_ven.cod_utente = :s_cs_xx.cod_utente
	order by evas_ord_ven.prog_riga_ord_ven asc,   
				evas_ord_ven.prog_riga asc;

	open cu_evas_ord_ven;
	
	ll_prog_riga_fat_ven = 0
	do while 0 = 0
		fetch cu_evas_ord_ven into :ll_prog_riga_ord_ven,   
											:ll_prog_riga,
											:ls_cod_deposito[1],   
											:ls_cod_ubicazione[1],   
											:ls_cod_lotto[1],   
											:ldt_data_stock[1],   
											:ll_progr_stock[1],   
											:ld_quan_in_evasione;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode < 0 then
			ls_messaggio =  "Errore durante la lettura Evasione Ordini Vendita: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga
		
		select	det_ord_ven.cod_tipo_det_ven,   
					det_ord_ven.cod_misura,   
					det_ord_ven.cod_prodotto,   
					det_ord_ven.des_prodotto,   
					det_ord_ven.quan_ordine,
					det_ord_ven.prezzo_vendita,   
					det_ord_ven.fat_conversione_ven,   
					det_ord_ven.sconto_1,   
					det_ord_ven.sconto_2,  
					det_ord_ven.provvigione_1,
					det_ord_ven.provvigione_2,
					det_ord_ven.cod_iva,   
					det_ord_ven.quan_evasa,
					det_ord_ven.nota_dettaglio,   
					det_ord_ven.anno_commessa,
					det_ord_ven.num_commessa,
					det_ord_ven.cod_centro_costo,
					det_ord_ven.sconto_3,   
					det_ord_ven.sconto_4,   
					det_ord_ven.sconto_5,   
					det_ord_ven.sconto_6,   
					det_ord_ven.sconto_7,   
					det_ord_ven.sconto_8,   
					det_ord_ven.sconto_9,   
					det_ord_ven.sconto_10,
					det_ord_ven.cod_versione,
					det_ord_ven.num_confezioni,
					det_ord_ven.num_pezzi_confezione,
					det_ord_ven.flag_doc_suc_det,
					det_ord_ven.quantita_um,
					det_ord_ven.prezzo_um
		into		:ls_cod_tipo_det_ven, 
					:ls_cod_misura, 
					:ls_cod_prodotto, 
					:ls_des_prodotto, 
					:ld_quan_ordine,
					:ld_prezzo_vendita, 
					:ld_fat_conversione_ven, 
					:ld_sconto_1, 
					:ld_sconto_2, 
					:ld_provvigione_1, 
					:ld_provvigione_2, 
					:ls_cod_iva, 
					:ld_quan_evasa,
					:ls_nota_dettaglio, 
					:ll_anno_commessa,
					:ll_num_commessa,
					:ls_cod_centro_costo,
					:ld_sconto_3, 
					:ld_sconto_4, 
					:ld_sconto_5, 
					:ld_sconto_6, 
					:ld_sconto_7, 
					:ld_sconto_8, 
					:ld_sconto_9, 
					:ld_sconto_10,
					:ls_cod_versione,
					:ld_num_confezioni,
					:ld_num_pezzi_confezione,
					:ls_flag_doc_suc_det,
					:ld_quantita_um,
					:ld_prezzo_um
		  from 	det_ord_ven  
		 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					det_ord_ven.anno_registrazione = :al_anno_ordine and  
					det_ord_ven.num_registrazione = :al_num_ordine and
					det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven
		order by det_ord_ven.prog_riga_ord_ven asc;
	
		if sqlca.sqlcode < 0 then
			ls_messaggio =  "Errore durante la lettura Dettagli Ordini Vendita: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if
			
//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
//    ricalcolo la quantità di vendita nel caso di evasioni parziali
		if ld_quan_in_evasione <> ld_quan_ordine then
			ld_quantita_um = round(ld_quan_in_evasione * ld_fat_conversione_ven, 4)
		end if
//------------------------------------------------- Modifica Nicola ---------------------------------------------
		if ls_flag_doc_suc_det = 'I' then //nota dettaglio
			select flag_doc_suc_or
			  into :ls_flag_doc_suc_det
			  from tab_tipi_det_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		end if		
		
		
		if ls_flag_doc_suc_det = 'N' then
			ls_nota_dettaglio = ""
		end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------			
					
			
		select tab_tipi_det_ven.flag_tipo_det_ven,
				 tab_tipi_det_ven.cod_tipo_movimento,
				 tab_tipi_det_ven.flag_sola_iva
		into   :ls_flag_tipo_det_ven,
				 :ls_cod_tipo_movimento,
				 :ls_flag_sola_iva
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode < 0 then
			ls_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
				ls_messaggio = "Si è verificato un errore in fase di creazione destinazioni movimenti."
				close cu_evas_ord_ven;
				return -1
			end if

			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if lb_riferimento and ls_flag_abilita_rif_bol_ven = "D" then
			lb_riferimento = false
			setnull(ls_note_dettaglio_rif)
			ls_des_riferimento = ls_des_riferimento_ord_ven
			if ls_flag_rif_anno_ord_ven = "S" and not isnull(al_anno_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" +string(al_anno_ordine)
			end if
			if ls_flag_rif_num_ord_ven = "S" and not isnull(al_num_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(al_num_ordine)
			end if
			if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
			end if
			if not isnull(ls_num_ord_cliente) then
				ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
				if not isnull(ldt_data_ord_cliente) then
					ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
				end if
			end if
				
			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_bol_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
				g_mb.messagebox("Generazione Fattura Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if
			
			insert into det_fat_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_fat_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 data_stock,
							 progr_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_fatturata,   
							 prezzo_vendita,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 anno_registrazione_fat,
							 num_registrazione_fat,
							 nota_dettaglio,   
							 anno_registrazione_bol_ven,   
							 num_registrazione_bol_ven,   
							 prog_riga_bol_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 fat_conversione_ven,   
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 anno_reg_ord_ven,   
							 num_reg_ord_ven,   
							 prog_riga_ord_ven,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)							 
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_fat_ven,   
							 :ls_cod_tipo_det_ven_rif_bol_ven,   
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 :ls_des_riferimento,   
							 0,   
							 0,   
							 0,   
							 0,  
							 0,
							 0,
							 :ls_cod_iva_rif,   
							 null,
							 null,
							 null,
							 null,
							 null,   
							 null,
							 null,
							 null,
							 null,   
							 null,   
							 null,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,
							 null,
							 1,
							 null,
							 null,
							 null,   
							 null,   
							 null,
							 null,
							 0,
							 0,
							 'I',
							 0,
							 0);							 
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Errore durante la scrittura Dettagli Bolle Vendita: " + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
			ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga
		end if	

		// ** michela 13/04/2004: se è previsto, aggiungo le note associate al prodotto
		if ib_note_prodotto then
			
			if isnull(ls_nota_dettaglio) then ls_nota_dettaglio = ""
			
			select  anag_prodotti_note.nota_prodotto  
			into    :ls_nota_prodotto  
			from    anag_prodotti_note  
			where ( anag_prodotti_note.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         		( anag_prodotti_note.cod_prodotto = :ls_cod_prodotto ) AND  
         		( anag_prodotti_note.cod_nota_prodotto = 
							 ( select parametri_azienda.stringa  
								from parametri_azienda  
								where (parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda ) AND  
								      (parametri_azienda.flag_parametro = 'S' ) AND  
                              (parametri_azienda.cod_parametro = 'CNP' )  )) ;
			if sqlca.sqlcode = 0 then
				if isnull(ls_nota_prodotto) then ls_nota_prodotto = ""
				ls_nota_dettaglio = ls_nota_dettaglio + ls_nota_prodotto
			end if
			
		end if
		
		// ** fine modifiche

		insert into det_fat_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_fat_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 data_stock,
						 progr_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_fatturata,   
						 prezzo_vendita,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 anno_registrazione_fat,
						 num_registrazione_fat,
						 nota_dettaglio,   
						 anno_registrazione_bol_ven,   
						 num_registrazione_bol_ven,   
						 prog_riga_bol_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 fat_conversione_ven,   
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 anno_reg_ord_ven,   
						 num_reg_ord_ven,   
						 prog_riga_ord_ven,
						 cod_versione,
                   num_confezioni,
					    num_pezzi_confezione,
						 flag_st_note_det,
						 quantita_um,
						 prezzo_um)
	  values			(:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_fat_ven,   
						 :ls_cod_tipo_det_ven,   
						 :ls_cod_deposito[1],
						 :ls_cod_ubicazione[1],
						 :ls_cod_lotto[1],
						 :ldt_data_stock[1],
						 :ll_progr_stock[1],
						 :ls_cod_misura,   
						 :ls_cod_prodotto,   
						 :ls_des_prodotto,   
						 :ld_quan_in_evasione,   
						 :ld_prezzo_vendita,   
						 :ld_sconto_1,   
						 :ld_sconto_2,  
						 :ld_provvigione_1,
						 :ld_provvigione_2,
						 :ls_cod_iva,   
						 :ls_cod_tipo_movimento,
						 null,
						 null,
						 null,
						 :ls_nota_dettaglio,   
						 null,
						 null,
						 null,
						 :ll_anno_commessa,   
						 :ll_num_commessa,   
						 :ls_cod_centro_costo,   
						 :ld_sconto_3,   
						 :ld_sconto_4,   
						 :ld_sconto_5,   
						 :ld_sconto_6,   
						 :ld_sconto_7,   
						 :ld_sconto_8,   
						 :ld_sconto_9,   
						 :ld_sconto_10,
						 null,
						 :ld_fat_conversione_ven,
						 :ll_anno_reg_des_mov,
						 :ll_num_reg_des_mov,
						 :al_anno_ordine,   
						 :al_num_ordine,   
						 :ll_prog_riga_ord_ven,
						 :ls_cod_versione,
                   :ld_num_confezioni,
					    :ld_num_pezzi_confezione,
						 'I',
						 :ld_quantita_um,
						 :ld_prezzo_um);

		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la scrittura Dettagli Fatture Vendita.: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			update anag_prodotti  
				set quan_assegnata = quan_assegnata - :ld_quan_in_evasione,
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Si è verificato un errore in fase di aggiornamento magazzino: " + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
			
			update stock 
			set 	 quan_assegnata = quan_assegnata - :ld_quan_in_evasione,   
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione  
			where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
					 stock.cod_prodotto = :ls_cod_prodotto and 
					 stock.cod_deposito = :ls_cod_deposito[1] and 
					 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
					 stock.cod_lotto = :ls_cod_lotto[1] and 
					 stock.data_stock = :ldt_data_stock[1] and 
					 stock.prog_stock = :ll_progr_stock[1];

			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Si è verificato un errore in fase di aggiornamento stock: " + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
		end if				

		ls_tabella_orig = "det_ord_ven_stat"
		ls_tabella_des = "det_fat_ven_stat"
		ls_prog_riga_doc_orig = "prog_riga_ord_ven"
		ls_prog_riga_doc_des = "prog_riga_fat_ven"

		if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, al_anno_ordine, al_num_ordine, ll_prog_riga_ord_ven, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven) = -1 then
			close cu_evas_ord_ven;
			return -1
		end if	

		if ls_cod_tipo_analisi_fat <> ls_cod_tipo_analisi_ord then
			if f_crea_distribuzione(ls_cod_tipo_analisi_fat, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_fat_ven, 6) = -1 then
				ls_messaggio = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if ld_quan_ordine <= ld_quan_evasa + ld_quan_in_evasione then
			ls_flag_evasione = "E"
		else
			ls_flag_evasione = "P"
		end if
		
		update det_ord_ven  
			set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione,
				 quan_evasa = quan_evasa + :ld_quan_in_evasione,
				 flag_evasione = :ls_flag_evasione
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :al_anno_ordine and  
				 det_ord_ven.num_registrazione = :al_num_ordine and
				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
		if sqlca.sqlcode < 0 then
			ls_messaggio = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Vendita: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_sola_iva = 'N' then
			ld_val_riga = ld_quan_in_evasione * ld_prezzo_vendita
			ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
			ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
			ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
			ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
			ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
			ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
			ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
			ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
			ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
			ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
			ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
			ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
			ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
			ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
			ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
			ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
			ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
			ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
			ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
			if ls_cod_valuta = ls_lire then
				ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
			else
				ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
			end if   
	
			if ls_flag_tipo_det_ven = "M" or &
				ls_flag_tipo_det_ven = "C" or &
				ls_flag_tipo_det_ven = "N" then
				ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
				ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
				ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
				ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
			elseif ls_flag_tipo_det_ven = "S" then
				ld_val_evaso = ld_val_riga_sconto_10 * -1
			else
				ld_val_evaso = ld_val_riga_sconto_10
			end if
			ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
		end if

		if ls_cod_valuta = ls_lire then
			ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
		end if   

		delete from evas_ord_ven 
		where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
						evas_ord_ven.anno_registrazione = :al_anno_ordine and
						evas_ord_ven.num_registrazione = :al_num_ordine and 
						evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
						evas_ord_ven.prog_riga = :ll_prog_riga and
						evas_ord_ven.cod_utente = :s_cs_xx.cod_utente;

		if sqlca.sqlcode < 0 then
			ls_messaggio = "Si è verificato un errore in fase di cancellazione Evasione Ordini: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if
	loop
	close cu_evas_ord_ven;

	uof_calcola_stato_ordine(al_anno_ordine, al_num_ordine)

end if

luo_calcolo = create uo_calcola_documento_euro

if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"fat_ven",ls_messaggio) <> 0 then
	destroy luo_calcolo
	return -1
end if

destroy luo_calcolo

al_anno_fattura = ll_anno_registrazione

al_num_fattura = ll_num_registrazione

return 0
end function

public function integer uof_crea_aggiungi_bolla (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_bol_ven, ref long fl_num_reg_bol_ven, ref string fs_errore);boolean lb_riferimento, lb_aggiungi=false
long	 ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_anno_commessa, ll_num_commessa, &
    		ll_progr_stock[1], ll_prog_riga_bol_ven, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga, &
	  	ll_num_confezioni, ll_num_pezzi_confezione
string ls_cod_tipo_ord_ven, ls_cod_tipo_bol_ven, ls_nota_testata, ls_cod_cliente[1], ls_cod_operatore, ls_rag_soc_1, &
		 ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione[1], &
		 ls_cod_deposito[1], ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, ls_cod_imballo, &
		 ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_nota_piede, ls_cod_banca, ls_cod_tipo_det_ven, &
		 ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_ven, &
		 ls_cod_tipo_analisi_bol, ls_cod_tipo_analisi_ord, ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, ls_causale_trasporto, &
		 ls_flag_riep_fat, ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ls_num_ord_cliente, &
		 ls_cod_lotto[1], ls_cod_tipo_movimento, ls_vostro_ordine, ls_vostro_ordine_data, ls_flag_evasione, ls_lire, &
		 ls_flag_sola_iva, ls_cod_fornitore[], ls_causale_trasporto_tab, ls_cod_versione, ls_cod_causale_tab, ls_flag_abilita_rif_ord_ven, &
		 ls_cod_tipo_det_ven_rif_ord_ven, ls_des_riferimento_ord_ven, ls_flag_rif_anno_ord_ven, ls_flag_rif_num_ord_ven,&
		 ls_flag_rif_data_ord_ven, ls_des_riferimento, ls_cod_iva_rif, ls_cod_causale, ls_note_dettaglio_rif, &
		 ls_db,ls_cod_tipo_bol_ven_dest, ls_nota_piede_bolla, ls_messaggio, ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det, &
		 ls_nota_prodotto, ls_flag_trasporta_nota_ddt, ls_rif_packing_list,ls_cod_tipo_det_ven_new, ls_cod_tipo_det_ven_ord, &
		 ls_cod_tipo_movimento_ord, ls_cod_tipo_movimento_new
datetime ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock[1], ldt_vostro_ordine_data, ldt_ora_inizio_trasporto
dec{6} ld_cambio_ven, ld_quan_in_evasione, ld_prezzo_vendita, ld_fat_conversione_ven, ld_sconto_1, ld_sconto_2, &
       ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		 ld_sconto_testata, ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, ld_quan_ordine, &
		 ld_quan_evasa, ld_tot_val_evaso, ld_tot_val_ordine, ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, ld_val_riga_sconto_4, &
		 ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10, &
		 ld_val_sconto_testata, ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quantita_um, ld_prezzo_um
integer li_risposta

uo_calcola_documento_euro luo_calcolo


li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
if upper(ls_db) = "MSSQL" then ls_db="SYBASE_ASE"

ll_anno_registrazione = f_anno_esercizio()

if not isnull(fl_anno_reg_bol_ven) and fl_anno_reg_bol_ven > 0 then lb_aggiungi = true
if lb_aggiungi and (fl_num_reg_bol_ven = 0 or isnull(fl_num_reg_bol_ven)) then
	fs_errore = "Manca l'indicazione del numero di bolla di destinazione"
	return -1
end if
lb_riferimento = true
setnull(ls_cod_fornitore[1])

select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in fase di ricerca ordine: operazione annullata"
	return -1
end if

select cod_tipo_bol_ven, cod_tipo_analisi  
into :ls_cod_tipo_bol_ven,  :ls_cod_tipo_analisi_ord
from tab_tipi_ord_ven  
where  
	cod_azienda = :s_cs_xx.cod_azienda and  
	cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la lettura della tabella Tipi Ordini Vendita."
	return -1
end if
	
// stefanop 08/06/2010: non carico il tipo bol ven se impostato: progetto 140
if isnull(this.is_cod_tipo_bol_ven) or this.is_cod_tipo_bol_ven = "" then
	// lascio quello di default dell'applicazione, 99% dei casi
	this.ib_change_det_ven = false
	this.ib_change_mov = false
else
	
	// TIPO_DET_VEN
	if this.ib_change_det_ven then
		// recupero il dettaglio orignale che sarà sostituito con quello nuovo
		select cod_tipo_det_ven
		into :ls_cod_tipo_det_ven_ord
		from tab_tipi_bol_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
			
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante il recupero del dettaglio bolla.~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		// recupero il nuovo dettaglio
		select cod_tipo_det_ven
		into :ls_cod_tipo_det_ven_new
		from tab_tipi_bol_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_bol_ven = :is_cod_tipo_bol_ven;
			
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante il recupero del nuovo dettaglio bolla."
			return -1
		end if
	end if
	
	// MOV_MAG
	if this.ib_change_mov then
		// carico movimento default, mi serve per il confronto
		select cod_tipo_movimento
		into :ls_cod_tipo_movimento_ord
		from tab_tipi_det_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and 
			cod_tipo_det_ven = :ls_cod_tipo_det_ven_ord;
			
		if sqlca.sqlcode <> 0 then 
			fs_errore = "Errore durante la ricerca del tipo movimento."
			return -1
		end if
		
		// carico il nuovo movimento di magazzioni che sarà inserito
		select cod_tipo_movimento
		into :ls_cod_tipo_movimento_new
		from tab_tipi_det_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_det_ven = :ls_cod_tipo_det_ven_new;
			
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la ricerca del nuovo tipo movimento."
			return -1
		end if
	end if
	
	
	// la variabile viene impostata dalla finestra w_richiesta_dati_bolla
	ls_cod_tipo_bol_ven = this.is_cod_tipo_bol_ven
end if
// ----

if lb_aggiungi then
	select cod_tipo_bol_ven
	into   :ls_cod_tipo_bol_ven_dest
	from   tes_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :fl_anno_reg_bol_ven and
			 num_registrazione = :fl_num_reg_bol_ven;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in ricerca tipo bolla di vendita in bolla di destinazione. Dettaglio: " + sqlca.sqlerrtext
		return -1
	end if
	
	if ls_cod_tipo_bol_ven <> ls_cod_tipo_bol_ven_dest then
		fs_errore = "Il tipo bolla deve coincidere con il tipo bolla di destinazione indicato nel tipo ordine: controllare il tipo bolla di destinazione nella tabella tipi ordini"
		return -1
	end if
end if

if not isnull(ls_cod_tipo_bol_ven) then
	select tes_ord_ven.cod_operatore,   
			 tes_ord_ven.data_registrazione,   
			 tes_ord_ven.cod_cliente,   
			 tes_ord_ven.cod_des_cliente,   
			 tes_ord_ven.rag_soc_1,   
			 tes_ord_ven.rag_soc_2,   
			 tes_ord_ven.indirizzo,   
			 tes_ord_ven.localita,   
			 tes_ord_ven.frazione,   
			 tes_ord_ven.cap,   
			 tes_ord_ven.provincia,   
			 tes_ord_ven.cod_deposito,   
			 tes_ord_ven.cod_ubicazione,   
			 tes_ord_ven.cod_valuta,   
			 tes_ord_ven.cambio_ven,   
			 tes_ord_ven.cod_tipo_listino_prodotto,   
			 tes_ord_ven.cod_pagamento,   
			 tes_ord_ven.sconto,   
			 tes_ord_ven.cod_agente_1,
			 tes_ord_ven.cod_agente_2,
			 tes_ord_ven.cod_banca,   
			 tes_ord_ven.num_ord_cliente,   
			 tes_ord_ven.data_ord_cliente,   
			 tes_ord_ven.cod_imballo,   
			 tes_ord_ven.aspetto_beni,
			 tes_ord_ven.peso_netto,
			 tes_ord_ven.peso_lordo,
			 tes_ord_ven.num_colli,
			 tes_ord_ven.cod_vettore,   
			 tes_ord_ven.cod_inoltro,   
			 tes_ord_ven.cod_mezzo,
			 tes_ord_ven.causale_trasporto,
			 tes_ord_ven.cod_porto,   
			 tes_ord_ven.cod_resa,   
			 tes_ord_ven.nota_testata,   
			 tes_ord_ven.nota_piede,   
			 tes_ord_ven.flag_riep_fat,
			 tes_ord_ven.tot_val_evaso,
			 tes_ord_ven.tot_val_ordine,
			 tes_ord_ven.cod_banca_clien_for,
			 tes_ord_ven.cod_causale,
			 tes_ord_ven.flag_doc_suc_tes, 
			 tes_ord_ven.flag_doc_suc_pie			 
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ls_flag_riep_fat,
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_cod_causale,
			 :ls_flag_doc_suc_tes, 
			 :ls_flag_doc_suc_pie			 
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione and  
			 tes_ord_ven.num_registrazione = :fl_num_registrazione;

	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la lettura Testata Ordine Vendita."
		return -1
	end if
	
	// stefanop: 18/07/2012 - l'operatore che inserire è SEMPRE l'operatore collegato ad Apice
	// ------------------------------------------------------
	ls_cod_operatore = guo_functions.uof_get_operatore_utente()
	// ------------------------------------------------------

	select tab_tipi_bol_ven.cod_tipo_analisi,
			 tab_tipi_bol_ven.causale_trasporto,
			 tab_tipi_bol_ven.cod_causale,
			 tab_tipi_bol_ven.flag_abilita_rif_ord_ven,
			 tab_tipi_bol_ven.cod_tipo_det_ven_rif_ord_ven,
			 tab_tipi_bol_ven.des_riferimento_ord_ven,
			 tab_tipi_bol_ven.flag_rif_anno_ord_ven,
			 tab_tipi_bol_ven.flag_rif_num_ord_ven,
			 tab_tipi_bol_ven.flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi_bol,
			 :ls_causale_trasporto_tab,
			 :ls_cod_causale_tab,
			 :ls_flag_abilita_rif_ord_ven,
			 :ls_cod_tipo_det_ven_rif_ord_ven,
			 :ls_des_riferimento_ord_ven,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_bol_ven  
	where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Si è verificato un errore nella lettura della Tabella Tipi Bolle."
		return -1
	end if

	ldt_data_registrazione = datetime(today())
	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if

	if not isnull(ls_num_ord_cliente) and ls_flag_abilita_rif_ord_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if

//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------


	if ls_cod_tipo_det_ven_rif_ord_ven = "T" then
		if len(is_rif_packing_list) > 0 and not isnull(is_rif_packing_list) then ls_rif_packing_list = is_rif_packing_list + "~r~n"
		ls_nota_testata = ls_rif_packing_list + ls_des_riferimento_ord_ven + string(fl_anno_registrazione) + "/" + &
								string(fl_num_registrazione) + " del " + string(ldt_data_ordine, "dd/mm/yyyy")+ &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
	end if
	
	// ----- controllo lunghezza note per ASE
	if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE
	
	if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
		ls_causale_trasporto = ls_causale_trasporto_tab
	end if
	if isnull(ls_cod_causale) then
		ls_cod_causale = ls_cod_causale_tab
	end if
	select parametri_azienda.stringa
	into   :ls_lire
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LIR';

	if sqlca.sqlcode <> 0 then
		fs_errore = "Configurare il codice valuta per le Lire Italiane."
		return -1
	end if

	select tab_pagamenti.sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if
	
	if lb_aggiungi then
		ldt_ora_inizio_trasporto = datetime(date("01/01/1900"), it_ora_inizio_trasporto)
		ll_num_registrazione = fl_num_reg_bol_ven
		
		select nota_piede
		into   :ls_nota_piede_bolla
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_reg_bol_ven and
		       num_registrazione = :fl_num_reg_bol_ven;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante ricercaTestata Bolle Vendita. Dettaglio errore " + sqlca.sqlerrtext
			return -1
		end if

		if isnull(ls_nota_piede_bolla) then ls_nota_piede_bolla = ""
		if len(trim(ls_nota_piede_bolla)) > 0 then
			//ls_nota_piede_bolla = ls_nota_piede_bolla + "~r~n" + is_nota_piede
			ls_nota_piede_bolla = ls_nota_piede_bolla
		else
			//ls_nota_piede_bolla = ls_nota_piede_bolla + is_nota_piede
			ls_nota_piede_bolla = ls_nota_piede_bolla
		end if
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_piede_bolla) > 255 then
			ls_nota_piede_bolla = left(ls_nota_piede_bolla, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		
		update tes_bol_ven
		set    num_colli  = num_colli + :id_num_colli,
		       peso_lordo = peso_lordo + :id_peso_lordo,
				 peso_netto = peso_netto + :id_peso_netto,
				 rag_soc_1  = :is_rag_soc_1,
				 rag_soc_2  = :is_rag_soc_2,
				 indirizzo  = :is_indirizzo,
				 localita   = :is_localita,
				 cap = :is_cap,
				 provincia = :is_provincia,
				 frazione  = :is_frazione,
				 data_inizio_trasporto = :idt_data_inizio_trasporto,
				 ora_inizio_trasporto  = :ldt_ora_inizio_trasporto,
				 cod_des_cliente = :is_destinazione,
				 cod_causale  = :is_cod_causale,
				 aspetto_beni = :is_aspetto_beni,
				 cod_porto = :is_cod_porto,
				 cod_mezzo = :is_cod_mezzo,
				 cod_vettore = :is_cod_vettore,
				 cod_inoltro = :is_cod_inoltro,
				 cod_imballo = :is_cod_imballo,
				 nota_piede = :ls_nota_piede_bolla
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_reg_bol_ven and
				 num_registrazione = :fl_num_reg_bol_ven;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante aggiornamento Testata Bolle Vendita. Dettaglio errore " + sqlca.sqlerrtext
			return -1
		end if
	else
		select max(num_registrazione)
		into   :ll_num_registrazione
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la ricerca massimo numero registrazione per incremento numeratore interno bolle."
			return -1
		end if
		if isnull(ll_num_registrazione) then ll_num_registrazione = 0
		ll_num_registrazione ++
		update con_bol_ven
		set    num_registrazione = :ll_num_registrazione
		where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura sulla tabella Controllo Bolle Vendita."
			return -1
		end if
		
		if not isnull(is_cod_causale) then ls_cod_causale = is_cod_causale
		if not isnull(is_aspetto_beni) then ls_aspetto_beni = is_aspetto_beni
		if not isnull(is_cod_porto) then ls_cod_porto = is_cod_porto
		if not isnull(is_cod_mezzo) then ls_cod_mezzo = is_cod_mezzo
		if not isnull(is_cod_vettore) then ls_cod_vettore = is_cod_vettore
		if not isnull(is_cod_inoltro) then ls_cod_inoltro = is_cod_inoltro
		if not isnull(is_cod_resa) then ls_cod_resa = is_cod_resa
		if not isnull(is_destinazione) then
			ls_cod_des_cliente = is_destinazione
			ls_rag_soc_1 = is_rag_soc_1
			ls_rag_soc_2 = is_rag_soc_2
			ls_indirizzo = is_indirizzo
			ls_localita = is_localita
			ls_frazione = is_frazione
			ls_cap = is_cap
			ls_provincia = is_provincia
		end if
		if not isnull(id_num_colli) and id_num_colli <> 0 then ld_num_colli = id_num_colli
		if not isnull(id_peso_lordo) and id_peso_lordo <> 0 then ld_peso_lordo = id_peso_lordo
		if not isnull(id_peso_netto) and id_peso_netto <> 0 then ld_peso_netto = id_peso_netto
		ldt_ora_inizio_trasporto = datetime(date("01/01/1900"), it_ora_inizio_trasporto)
		if isnull(ls_num_ord_cliente) or len(ls_num_ord_cliente) < 1 then
			ls_num_ord_cliente = is_num_ord_cliente
		end if
		if isnull(ldt_data_ord_cliente) then
			ldt_data_ord_cliente = idt_data_ord_cliente
		end if
		if isnull(ls_nota_piede) then ls_nota_piede = ""
		if len(trim(ls_nota_piede)) > 0 then
			//ls_nota_piede = ls_nota_piede + "~r~n" + is_nota_piede
			ls_nota_piede = ls_nota_piede
		else
			//ls_nota_piede = ls_nota_piede + is_nota_piede
			ls_nota_piede = ls_nota_piede
		end if
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_piede) > 255 then
			ls_nota_piede = left(ls_nota_piede, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		
		insert into tes_bol_ven
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 data_registrazione,   
						 cod_operatore,   
						 cod_tipo_bol_ven,   
						 cod_cliente,
						 cod_fornitore,
						 cod_des_cliente,   
						 cod_fil_fornitore,
						 rag_soc_1,   
						 rag_soc_2,   
						 indirizzo,   
						 localita,   
						 frazione,   
						 cap,   
						 provincia,   
						 cod_deposito,   
						 cod_deposito_tras,
						 cod_ubicazione,
						 cod_valuta,   
						 cambio_ven,   
						 cod_tipo_listino_prodotto,   
						 cod_pagamento,   
						 sconto,   
						 cod_agente_1,
						 cod_agente_2,
						 cod_banca,   
						 num_ord_cliente,   
						 data_ord_cliente,   
						 cod_imballo,   
						 aspetto_beni,
						 peso_netto,
						 peso_lordo,
						 num_colli,
						 cod_vettore,   
						 cod_inoltro,   
						 cod_mezzo,
						 causale_trasporto,
						 cod_porto,   
						 cod_resa,   
						 cod_documento,
						 numeratore_documento,
						 anno_documento,
						 num_documento,
						 data_bolla,
						 nota_testata,   
						 nota_piede,   
						 flag_fuori_fido,
						 flag_blocco,   
						 flag_movimenti,  
						 flag_riep_fat,
						 cod_banca_clien_for,
						 flag_gen_fat,
						 cod_causale,
						 data_inizio_trasporto,
						 ora_inizio_trasporto,
						 flag_doc_suc_tes,
						 flag_st_note_tes,
						 flag_doc_suc_pie,
						 flag_st_note_pie)  						 
		values      (:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ldt_data_registrazione,   
						 :ls_cod_operatore,   
						 :ls_cod_tipo_bol_ven,   
						 :ls_cod_cliente[1],
						 null,
						 :ls_cod_des_cliente,   
						 null,
						 :ls_rag_soc_1,   
						 :ls_rag_soc_2,   
						 :ls_indirizzo,   
						 :ls_localita,   
						 :ls_frazione,   
						 :ls_cap,   
						 :ls_provincia,   
						 :ls_cod_deposito[1],   
						 null,
						 :ls_cod_ubicazione[1],
						 :ls_cod_valuta,   
						 :ld_cambio_ven,   
						 :ls_cod_tipo_listino_prodotto,   
						 :ls_cod_pagamento,   
						 :ld_sconto_testata,
						 :ls_cod_agente_1,
						 :ls_cod_agente_2,
						 :ls_cod_banca,   
						 :ls_num_ord_cliente,   
						 :ldt_data_ord_cliente,   
						 :ls_cod_imballo,   
						 :ls_aspetto_beni,
						 :ld_peso_netto,
						 :ld_peso_lordo,
						 :ld_num_colli,
						 :ls_cod_vettore,   
						 :ls_cod_inoltro,   
						 :ls_cod_mezzo,   
						 :ls_causale_trasporto,
						 :ls_cod_porto,   
						 :ls_cod_resa,   
						 null,
						 null,
						 0,
						 0,
						 null,
						 :ls_nota_testata,   
						 :ls_nota_piede,   
						 'N',
						 'N',   
						 'N',
						 :ls_flag_riep_fat,
						 :ls_cod_banca_clien_for,
						 'N',
						 :ls_cod_causale,
						 :idt_data_inizio_trasporto,
						 :ldt_ora_inizio_trasporto,
						 'I',
						 'I',
						 'I',
						 'I');						 
		 if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante inserimento Testata Bolle Vendita. Dettaglio errore " + sqlca.sqlerrtext
			return -1
		 end if
		 
		if uof_crea_nota_fissa("BOL_VEN", ll_anno_registrazione, ll_num_registrazione, fs_errore) = -1 then return -1
		 
	 end if

	 declare cu_evas_ord_ven cursor for  
	 select  evas_ord_ven.prog_riga_ord_ven,   
				evas_ord_ven.prog_riga,   
				evas_ord_ven.cod_deposito,   
				evas_ord_ven.cod_ubicazione,   
				evas_ord_ven.cod_lotto,   
				evas_ord_ven.data_stock,   
				evas_ord_ven.prog_stock,   
				evas_ord_ven.quan_in_evasione
	from     evas_ord_ven  
	where    evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				evas_ord_ven.anno_registrazione = :fl_anno_registrazione and
				evas_ord_ven.num_registrazione = :fl_num_registrazione and
				evas_ord_ven.cod_utente = :s_cs_xx.cod_utente
	order by evas_ord_ven.prog_riga_ord_ven asc,   
				evas_ord_ven.prog_riga asc;

	open cu_evas_ord_ven;
	
	lb_riferimento = true
	if lb_aggiungi then
		select max(prog_riga_bol_ven)
		into   :ll_prog_riga_bol_ven
		from   det_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
	else
		ll_prog_riga_bol_ven = 0	
	end if
	
	do while 0 = 0
		fetch cu_evas_ord_ven into :ll_prog_riga_ord_ven,   
											:ll_prog_riga,
											:ls_cod_deposito[1],   
											:ls_cod_ubicazione[1],   
											:ls_cod_lotto[1],   
											:ldt_data_stock[1],   
											:ll_progr_stock[1],   
											:ld_quan_in_evasione;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode = -1 then
			fs_errore =  "Errore durante la lettura Evasione Ordini Vendita."
			close cu_evas_ord_ven;
			return -1
		end if

		ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		
		select	det_ord_ven.cod_tipo_det_ven,   
					det_ord_ven.cod_misura,   
					det_ord_ven.cod_prodotto,   
					det_ord_ven.des_prodotto,   
					det_ord_ven.quan_ordine,
					det_ord_ven.prezzo_vendita,   
					det_ord_ven.fat_conversione_ven,   
					det_ord_ven.sconto_1,   
					det_ord_ven.sconto_2,  
					det_ord_ven.provvigione_1,
					det_ord_ven.provvigione_2,
					det_ord_ven.cod_iva,   
					det_ord_ven.quan_evasa,
					det_ord_ven.nota_dettaglio,   
					det_ord_ven.anno_commessa,
					det_ord_ven.num_commessa,
					det_ord_ven.cod_centro_costo,
					det_ord_ven.sconto_3,   
					det_ord_ven.sconto_4,   
					det_ord_ven.sconto_5,   
					det_ord_ven.sconto_6,   
					det_ord_ven.sconto_7,   
					det_ord_ven.sconto_8,   
					det_ord_ven.sconto_9,   
					det_ord_ven.sconto_10,
					det_ord_ven.cod_versione,
					det_ord_ven.num_confezioni,
					det_ord_ven.num_pezzi_confezione,
					det_ord_ven.flag_doc_suc_det,
					det_ord_ven.quantita_um,
					det_ord_ven.prezzo_um,
					det_ord_ven.nota_prodotto,
					det_ord_ven.flag_trasporta_nota_ddt
		into		:ls_cod_tipo_det_ven, 
					:ls_cod_misura, 
					:ls_cod_prodotto, 
					:ls_des_prodotto, 
					:ld_quan_ordine,
					:ld_prezzo_vendita, 
					:ld_fat_conversione_ven, 
					:ld_sconto_1, 
					:ld_sconto_2, 
					:ld_provvigione_1, 
					:ld_provvigione_2, 
					:ls_cod_iva, 
					:ld_quan_evasa,
					:ls_nota_dettaglio, 
					:ll_anno_commessa,
					:ll_num_commessa,
					:ls_cod_centro_costo,
					:ld_sconto_3, 
					:ld_sconto_4, 
					:ld_sconto_5, 
					:ld_sconto_6, 
					:ld_sconto_7, 
					:ld_sconto_8, 
					:ld_sconto_9, 
					:ld_sconto_10,
					:ls_cod_versione,
					:ll_num_confezioni,
					:ll_num_pezzi_confezione,
					:ls_flag_doc_suc_det,
					:ld_quantita_um,
					:ld_prezzo_um,
					:ls_nota_prodotto,
					:ls_flag_trasporta_nota_ddt
		  from 	det_ord_ven  
		 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
					det_ord_ven.num_registrazione = :fl_num_registrazione and
					det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven
		order by det_ord_ven.prog_riga_ord_ven asc;
	
		if sqlca.sqlcode = -1 then
			fs_errore =  "Errore durante la lettura Dettagli Ordini Vendita."
			close cu_evas_ord_ven;
			return -1
		end if

//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
// ricalcolo la quantità di vendita nel caso di evasioni parziali
		if ld_quan_in_evasione <> ld_quan_ordine then
			ld_quantita_um = round(ld_quan_in_evasione * ld_fat_conversione_ven, 4)
		end if
//------------------------------------------------- Modifica Nicola ---------------------------------------------
	if ls_flag_doc_suc_det = 'I' then //nota dettaglio
		select flag_doc_suc_or
		  into :ls_flag_doc_suc_det
		  from tab_tipi_det_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		
	
	
	if ls_flag_doc_suc_det = 'N' then
		setnull(ls_nota_dettaglio)
	end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------		
//------------------------------------------------- Modifica Michela ---------------------------------------------
// se si può allora aggiungo la nota prodotto alla nota dettaglio
	if ls_flag_trasporta_nota_ddt = "S" then
		if isnull(ls_nota_dettaglio) then 
			ls_nota_dettaglio = "Nota prodotto:" + ls_nota_prodotto
		else
			if not isnull(ls_nota_prodotto) then
				ls_nota_dettaglio = ls_nota_dettaglio + "~r~nNota prodotto:" + ls_nota_prodotto
			end if
		end if
	end if
//-------------------------------------------------- Fine Modifica ----------------------------------------------					
		select tab_tipi_det_ven.flag_tipo_det_ven,
				 tab_tipi_det_ven.cod_tipo_movimento,
				 tab_tipi_det_ven.flag_sola_iva
		into   :ls_flag_tipo_det_ven,
				 :ls_cod_tipo_movimento,
				 :ls_flag_sola_iva
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode = -1 then
			fs_errore = "Si è verificato un errore in fase di lettura tipi dettagli."
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
				fs_errore = "Si è verificato un errore in fase di creazione destinazioni movimenti."
				close cu_evas_ord_ven;
				return -1
			end if

			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if lb_riferimento and ls_flag_abilita_rif_ord_ven = "D" then
			lb_riferimento = false
			setnull(ls_note_dettaglio_rif)
			
			ls_des_riferimento = ls_des_riferimento_ord_ven
			if ls_flag_rif_anno_ord_ven = "S" and not isnull(fl_anno_registrazione) then
				ls_des_riferimento = ls_des_riferimento + "-" +string(fl_anno_registrazione)
			end if
			if ls_flag_rif_num_ord_ven = "S" and not isnull(fl_num_registrazione) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(fl_num_registrazione)
			end if
			if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
			end if
			
			if not isnull(is_rif_packing_list) and len(is_rif_packing_list) > 0 then
				if not isnull(ls_note_dettaglio_rif) and len(ls_note_dettaglio_rif) > 0 then 
					ls_note_dettaglio_rif += "~r~n"
				else
					ls_note_dettaglio_rif = ""
				end if
				ls_note_dettaglio_rif = ls_note_dettaglio_rif + is_rif_packing_list
				setnull(is_rif_packing_list)
			end if
			
			if not isnull(ls_num_ord_cliente) then
				if not isnull(ls_note_dettaglio_rif) and len(ls_note_dettaglio_rif) > 0 then 
					ls_note_dettaglio_rif += "~r~n"
				else
					ls_note_dettaglio_rif = ""
				end if
				ls_note_dettaglio_rif = ls_note_dettaglio_rif + "Vs.Ordine " + ls_num_ord_cliente
				if not isnull(ldt_data_ord_cliente) then
					ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
				end if
			end if
			
			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_ord_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
				g_mb.messagebox("Generazione Bolla Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if
			
			insert into det_bol_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_bol_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 progr_stock,
							 data_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_consegnata,   
							 prezzo_vendita,   
							 fat_conversione_ven,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 nota_dettaglio,   
							 anno_registrazione_ord_ven,   
							 num_registrazione_ord_ven,   
							 prog_riga_ord_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 anno_reg_bol_acq,
							 num_reg_bol_acq,
							 prog_riga_bol_acq,
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_doc_suc_det,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)							 
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_bol_ven,   
							 :ls_cod_tipo_det_ven_rif_ord_ven,   
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,   
							 null,   
							 :ls_des_riferimento,   
							 0,   
							 0,   
							 1,   
							 0,   
							 0,  
							 0,
							 0,
							 :ls_cod_iva_rif,   
							 null,
							 null,
							 :ls_note_dettaglio_rif,   
							 null,   
							 null,   
							 null,
							 null,   
							 null,   
							 null,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 0,
							 0,
							 'I',
							 'I',
							 0,
							 0);							 
	
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la scrittura Dettagli Bolle Vendita."
				close cu_evas_ord_ven;
				return -1
			end if
			ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		end if	
		
		// stefanop 09/05/2010: progetto 140: controllo se devo cambiare il dettaglio
		if this.ib_change_det_ven then
			// cambio il dettaglio solo se uguale a quello di default, altrimenti lascio invariato
			if ls_cod_tipo_det_ven_ord = ls_cod_tipo_det_ven then
				ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_new
			end if
		end if
		
		if this.ib_change_mov then
			if ls_cod_tipo_movimento = ls_cod_tipo_movimento_ord then
				ls_cod_tipo_movimento = ls_cod_tipo_movimento_new
			end if
		end if
		// ---
		
		//########################################################################################
		//Donato 27/06/2012: per nessun motivo deve finire in bolla una quantità superiore a quella della riga di ordine vendita riferito
		if uof_controlla_qta_in_documento(fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven, ld_quan_in_evasione, fs_errore) < 0 then
			//in fs_errore il messaggio
			close cu_evas_ord_ven;
			return -1
		end if
		//########################################################################################

		insert into det_bol_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_bol_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 progr_stock,
						 data_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_consegnata,   
						 prezzo_vendita,   
						 fat_conversione_ven,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 nota_dettaglio,   
						 anno_registrazione_ord_ven,   
						 num_registrazione_ord_ven,   
						 prog_riga_ord_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 anno_reg_bol_acq,
						 num_reg_bol_acq,
						 prog_riga_bol_acq,
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 cod_versione,
						 num_confezioni,
						 num_pezzi_confezione,
						 flag_doc_suc_det,
						 flag_st_note_det,
						 quantita_um,
						 prezzo_um)
	  values			(:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_bol_ven,   
						 :ls_cod_tipo_det_ven,   
						 :ls_cod_deposito[1],
						 :ls_cod_ubicazione[1],
						 :ls_cod_lotto[1],
						 :ll_progr_stock[1],
						 :ldt_data_stock[1],
						 :ls_cod_misura,   
						 :ls_cod_prodotto,   
						 :ls_des_prodotto,   
						 :ld_quan_in_evasione,   
						 :ld_prezzo_vendita,   
						 :ld_fat_conversione_ven,   
						 :ld_sconto_1,   
						 :ld_sconto_2,  
						 :ld_provvigione_1,
						 :ld_provvigione_2,
						 :ls_cod_iva,   
						 :ls_cod_tipo_movimento,
						 null,
						 :ls_nota_dettaglio,   
						 :fl_anno_registrazione,   
						 :fl_num_registrazione,   
						 :ll_prog_riga_ord_ven,
						 :ll_anno_commessa,   
						 :ll_num_commessa,   
						 :ls_cod_centro_costo,   
						 :ld_sconto_3,   
						 :ld_sconto_4,   
						 :ld_sconto_5,   
						 :ld_sconto_6,   
						 :ld_sconto_7,   
						 :ld_sconto_8,   
						 :ld_sconto_9,   
						 :ld_sconto_10,
						 null,
						 null,
						 null,
						 null,
						 :ll_anno_reg_des_mov,
						 :ll_num_reg_des_mov,
						 :ls_cod_versione,
						 :ll_num_confezioni,
						 :ll_num_pezzi_confezione,
						 'I',
						 'I',
						 :ld_quantita_um,
						 :ld_prezzo_um);								 
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura Dettagli Bolle Vendita."
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			update anag_prodotti  
				set quan_assegnata = quan_assegnata - :ld_quan_in_evasione,
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode <> 0 then
				fs_errore = "Si è verificato un errore in fase di aggiornamento magazzino."
				close cu_evas_ord_ven;
				return -1
			end if
			
			update stock 
			set 	 quan_assegnata = quan_assegnata - :ld_quan_in_evasione,   
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione  
			where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
					 stock.cod_prodotto = :ls_cod_prodotto and 
					 stock.cod_deposito = :ls_cod_deposito[1] and 
					 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
					 stock.cod_lotto = :ls_cod_lotto[1] and 
					 stock.data_stock = :ldt_data_stock[1] and 
					 stock.prog_stock = :ll_progr_stock[1];

			if sqlca.sqlcode <> 0 then
				fs_errore = "Si è verificato un errore in fase di aggiornamento stock."
				close cu_evas_ord_ven;
				return -1
			end if
		end if				

		ls_tabella_orig = "det_ord_ven_stat"
		ls_tabella_des = "det_bol_ven_stat"
		ls_prog_riga_doc_orig = "prog_riga_ord_ven"
		ls_prog_riga_doc_des = "prog_riga_bol_ven"

		if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven) = -1 then
			close cu_evas_ord_ven;
			return -1
		end if	

		if ls_cod_tipo_analisi_bol <> ls_cod_tipo_analisi_ord then
			if f_crea_distribuzione(ls_cod_tipo_analisi_bol, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_bol_ven, 5) = -1 then
				fs_errore = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if ld_quan_ordine <= ld_quan_evasa + ld_quan_in_evasione then
			ls_flag_evasione = "E"
		else
			ls_flag_evasione = "P"
		end if
		
		update det_ord_ven  
			set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione,
				 quan_evasa = quan_evasa + :ld_quan_in_evasione,
				 flag_evasione = :ls_flag_evasione
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
				 det_ord_ven.num_registrazione = :fl_num_registrazione and
				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
		if sqlca.sqlcode = -1 then
			fs_errore = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Vendita."
			close cu_evas_ord_ven;
			return -1
		end if


		//######################################################################################
		//Donato 27/06/2012 cerco di evitare i casi in cui si possono avere residuo negativo
		//la casistica prevede ad esempio che 
		//1)		la quantità in evasione non si azzera dopo l'evasione, 
		//			e allora il residuo dato da quan_ordine - quan_in_evasione - quan_evasa da un valore minore di zero (caso SACCOLONGO)
		
		//2)		in altri casi si ha invece un azzeramento (giustamente) della quan_in_evasione ma una quantità_evasa superiore a quella dell'ordine (caso VEGGIANO)
		
		uof_correggi_errori_in_evasione(fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven)
		//###########################################################

		if ls_flag_sola_iva = 'N' then
			ld_val_riga = ld_quan_in_evasione * ld_prezzo_vendita
			ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
			ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
			ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
			ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
			ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
			ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
			ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
			ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
			ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
			ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
			ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
			ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
			ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
			ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
			ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
			ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
			ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
			ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
			ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
			if ls_cod_valuta = ls_lire then
				ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
			else
				ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
			end if   
	
			if ls_flag_tipo_det_ven = "M" or &
				ls_flag_tipo_det_ven = "C" or &
				ls_flag_tipo_det_ven = "N" then
				ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
				ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
				ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
				ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
			elseif ls_flag_tipo_det_ven = "S" then
				ld_val_evaso = ld_val_riga_sconto_10 * -1
			else
				ld_val_evaso = ld_val_riga_sconto_10
			end if
			ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
		end if

		if ls_cod_valuta = ls_lire then
			ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
		end if   

		delete from evas_ord_ven 
		where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
						evas_ord_ven.anno_registrazione = :fl_anno_registrazione and
						evas_ord_ven.num_registrazione = :fl_num_registrazione and 
						evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
						evas_ord_ven.prog_riga = :ll_prog_riga and
						evas_ord_ven.cod_utente = :s_cs_xx.cod_utente;

		if sqlca.sqlcode = -1 then
			fs_errore = "Si è verificato un errore in fase di cancellazione Evasione Ordini."
			close cu_evas_ord_ven;
			return -1
		end if
	loop
	close cu_evas_ord_ven;

	if ld_tot_val_ordine <= ld_tot_val_evaso then
		ls_flag_evasione = "E"
	else
		ls_flag_evasione = "P"
	end if

//	update tes_ord_ven  
//		set tot_val_evaso = :ld_tot_val_evaso
//	 where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione and  
//			 tes_ord_ven.num_registrazione = :fl_num_registrazione;
//
//	if sqlca.sqlcode <> 0 then
//		fs_errore = "Si è verificato un errore in fase di aggiornamento Valore Evaso in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
//		return -1
//	end if
	
	if uof_calcola_stato_ordine(fl_anno_registrazione,fl_num_registrazione) = -1 then
		fs_errore = "Si è verificato un errore in fase di aggiornamento stato dell'ordine in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
		return -1
	end if

	luo_calcolo = create uo_calcola_documento_euro

	if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_ven",fs_errore) <> 0 then
		destroy luo_calcolo
		return -1
	end if
	
	destroy luo_calcolo

	fl_anno_reg_bol_ven = ll_anno_registrazione
	fl_num_reg_bol_ven = ll_num_registrazione
end if
return 0

end function

public function integer uof_crea_aggiungi_bolla_pack (string fs_elenco_pl[], long fl_anno_registrazione[], long fl_num_registrazione[], ref long fl_anno_reg_bol_ven, ref long fl_num_reg_bol_ven, ref string fs_errore);boolean lb_riferimento, lb_aggiungi=false
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_anno_commessa, ll_num_commessa, &
     ll_progr_stock[1], ll_prog_riga_bol_ven, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga, &
	  ll_num_confezioni, ll_num_pezzi_confezione
string ls_cod_tipo_ord_ven, ls_cod_tipo_bol_ven, ls_nota_testata, ls_cod_cliente[1], ls_cod_operatore, ls_rag_soc_1, &
		 ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione[1], &
		 ls_cod_deposito[1], ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, ls_cod_imballo, &
		 ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_nota_piede, ls_cod_banca, ls_cod_tipo_det_ven, &
		 ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_ven, &
		 ls_cod_tipo_analisi_bol, ls_cod_tipo_analisi_ord, ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, ls_causale_trasporto, &
		 ls_flag_riep_fat, ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ls_num_ord_cliente, &
		 ls_cod_lotto[1], ls_cod_tipo_movimento, ls_vostro_ordine, ls_vostro_ordine_data, ls_flag_evasione, ls_lire, &
		 ls_flag_sola_iva, ls_cod_fornitore[], ls_causale_trasporto_tab, ls_cod_versione, ls_cod_causale_tab, ls_flag_abilita_rif_ord_ven, &
		 ls_cod_tipo_det_ven_rif_ord_ven, ls_des_riferimento_ord_ven, ls_flag_rif_anno_ord_ven, ls_flag_rif_num_ord_ven,&
		 ls_flag_rif_data_ord_ven, ls_des_riferimento, ls_cod_iva_rif, ls_cod_causale, ls_note_dettaglio_rif, &
		 ls_db,ls_cod_tipo_bol_ven_dest, ls_nota_piede_bolla, ls_messaggio, ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det, &
		 ls_nota_prodotto, ls_flag_trasporta_nota_ddt, ls_rif_packing_list
datetime ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock[1], ldt_vostro_ordine_data, ldt_ora_inizio_trasporto
dec{6} ld_cambio_ven, ld_quan_in_evasione, ld_prezzo_vendita, ld_fat_conversione_ven, ld_sconto_1, ld_sconto_2, &
       ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		 ld_sconto_testata, ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, ld_quan_ordine, &
		 ld_quan_evasa, ld_tot_val_evaso, ld_tot_val_ordine, ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, ld_val_riga_sconto_4, &
		 ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10, &
		 ld_val_sconto_testata, ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quantita_um, ld_prezzo_um
integer li_risposta, li_j
string ls_vuoto[]

uo_calcola_documento_euro luo_calcolo


li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
if upper(ls_db) = "MSSQL" then ls_db="SYBASE_ASE"

ll_anno_registrazione = f_anno_esercizio()

if not isnull(fl_anno_reg_bol_ven) and fl_anno_reg_bol_ven > 0 then lb_aggiungi = true
if lb_aggiungi and (fl_num_reg_bol_ven = 0 or isnull(fl_num_reg_bol_ven)) then
	fs_errore = "Manca l'indicazione del numero di bolla di destinazione"
	return -1
end if
lb_riferimento = true
setnull(ls_cod_fornitore[1])



// *** controllo di passare solo una volta gli ordini
datastore lds_appo2
string    ls_sql_2, ls_where, ls_errore2, ls_syntax
long      ll_anno_ordine, ll_num_ordine

ls_sql_2 = " select anno_registrazione, " + &
           "        num_registrazione, " + &
			  "        cod_tipo_ord_ven " + &
			  " from   tes_ord_ven " + &
			  " where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and "

ls_where = ""			  
for li_j = 1 to Upperbound(fl_anno_registrazione)
	if ls_where = "" then 
		ls_where = " ( anno_registrazione = " + string(fl_anno_registrazione[li_j]) + " and num_registrazione = " + string(fl_num_registrazione[li_j]) + " ) "
	else
		ls_where += " or ( anno_registrazione = " + string(fl_anno_registrazione[li_j]) + " and num_registrazione = " + string(fl_num_registrazione[li_j]) + " ) "
	end if
next

ls_sql_2 += ls_where + " order by anno_registrazione asc, num_registrazione asc "
	
ls_syntax = sqlca.SyntaxFromSQL( ls_sql_2, "style(type=grid)", ls_errore2)
IF Len(ls_errore2) > 0 THEN
	fs_errore = ls_errore2
   RETURN -1
END IF

lds_appo2 = create datastore
lds_appo2.Create( ls_syntax, ls_errore2)

IF Len(ls_errore2) > 0 THEN
	fs_errore = ls_errore2
   RETURN -1
END IF

lds_appo2.settransobject( sqlca)
lds_appo2.retrieve()

// **** attenzione: a questo punto scorro l'array degli ordini di vendita
for li_j = 1 to lds_appo2.rowcount()

	ls_cod_tipo_ord_ven = lds_appo2.getitemstring( li_j, 3)
	ll_anno_ordine = lds_appo2.getitemnumber( li_j, 1)
	ll_num_ordine = lds_appo2.getitemnumber( li_j, 2)
	
	select cod_tipo_bol_ven,  
			 cod_tipo_analisi  
	into   :ls_cod_tipo_bol_ven,  
			 :ls_cod_tipo_analisi_ord
	from   tab_tipi_ord_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la lettura della tabella Tipi Ordini Vendita."
		return -1
	end if
	
	if lb_aggiungi then
		
		select cod_tipo_bol_ven
		into   :ls_cod_tipo_bol_ven_dest
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :fl_anno_reg_bol_ven and
				 num_registrazione = :fl_num_reg_bol_ven;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore in ricerca tipo bolla di vendita in bolla di destinazione. Dettaglio: " + sqlca.sqlerrtext
			return -1
		end if
		
		if ls_cod_tipo_bol_ven <> ls_cod_tipo_bol_ven_dest then
			fs_errore = "Il tipo bolla deve coincidere con il tipo bolla di destinazione indicato nel tipo ordine: controllare il tipo bolla di destinazione nella tabella tipi ordini"
			return -1
		end if
	end if
	
	if not isnull(ls_cod_tipo_bol_ven) then
		select tes_ord_ven.cod_operatore,   
				 tes_ord_ven.data_registrazione,   
				 tes_ord_ven.cod_cliente,   
				 tes_ord_ven.cod_des_cliente,   
				 tes_ord_ven.rag_soc_1,   
				 tes_ord_ven.rag_soc_2,   
				 tes_ord_ven.indirizzo,   
				 tes_ord_ven.localita,   
				 tes_ord_ven.frazione,   
				 tes_ord_ven.cap,   
				 tes_ord_ven.provincia,   
				 tes_ord_ven.cod_deposito,   
				 tes_ord_ven.cod_ubicazione,   
				 tes_ord_ven.cod_valuta,   
				 tes_ord_ven.cambio_ven,   
				 tes_ord_ven.cod_tipo_listino_prodotto,   
				 tes_ord_ven.cod_pagamento,   
				 tes_ord_ven.sconto,   
				 tes_ord_ven.cod_agente_1,
				 tes_ord_ven.cod_agente_2,
				 tes_ord_ven.cod_banca,   
				 tes_ord_ven.num_ord_cliente,   
				 tes_ord_ven.data_ord_cliente,   
				 tes_ord_ven.cod_imballo,   
				 tes_ord_ven.aspetto_beni,
				 tes_ord_ven.peso_netto,
				 tes_ord_ven.peso_lordo,
				 tes_ord_ven.num_colli,
				 tes_ord_ven.cod_vettore,   
				 tes_ord_ven.cod_inoltro,   
				 tes_ord_ven.cod_mezzo,
				 tes_ord_ven.causale_trasporto,
				 tes_ord_ven.cod_porto,   
				 tes_ord_ven.cod_resa,   
				 tes_ord_ven.nota_testata,   
				 tes_ord_ven.nota_piede,   
				 tes_ord_ven.flag_riep_fat,
				 tes_ord_ven.tot_val_evaso,
				 tes_ord_ven.tot_val_ordine,
				 tes_ord_ven.cod_banca_clien_for,
				 tes_ord_ven.cod_causale,
				 tes_ord_ven.flag_doc_suc_tes, 
				 tes_ord_ven.flag_doc_suc_pie			 
		 into  :ls_cod_operatore,   
				 :ldt_data_ordine,   
				 :ls_cod_cliente[1],   
				 :ls_cod_des_cliente,   
				 :ls_rag_soc_1,   
				 :ls_rag_soc_2,   
				 :ls_indirizzo,   
				 :ls_localita,   
				 :ls_frazione,   
				 :ls_cap,   
				 :ls_provincia,   
				 :ls_cod_deposito[1],   
				 :ls_cod_ubicazione[1],
				 :ls_cod_valuta,   
				 :ld_cambio_ven,   
				 :ls_cod_tipo_listino_prodotto,   
				 :ls_cod_pagamento,   
				 :ld_sconto_testata,   
				 :ls_cod_agente_1,
				 :ls_cod_agente_2,
				 :ls_cod_banca,   
				 :ls_num_ord_cliente,   
				 :ldt_data_ord_cliente,   
				 :ls_cod_imballo,   
				 :ls_aspetto_beni,
				 :ld_peso_netto,
				 :ld_peso_lordo,
				 :ld_num_colli,
				 :ls_cod_vettore,   
				 :ls_cod_inoltro,   
				 :ls_cod_mezzo,   
				 :ls_causale_trasporto,
				 :ls_cod_porto,
				 :ls_cod_resa,   
				 :ls_nota_testata,   
				 :ls_nota_piede,   
				 :ls_flag_riep_fat,
				 :ld_tot_val_evaso,
				 :ld_tot_val_ordine,
				 :ls_cod_banca_clien_for,
				 :ls_cod_causale,
				 :ls_flag_doc_suc_tes, 
				 :ls_flag_doc_suc_pie			 
		from   tes_ord_ven  
		where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tes_ord_ven.anno_registrazione = :ll_anno_ordine and  
				 tes_ord_ven.num_registrazione = :ll_num_ordine;
	
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la lettura Testata Ordine Vendita."
			return -1
		end if
		
		// stefanop: 18/07/2012 - l'operatore che inserire è SEMPRE l'operatore collegato ad Apice
		// ------------------------------------------------------
		ls_cod_operatore = guo_functions.uof_get_operatore_utente()
		// ------------------------------------------------------
	
		select tab_tipi_bol_ven.cod_tipo_analisi,
				 tab_tipi_bol_ven.causale_trasporto,
				 tab_tipi_bol_ven.cod_causale,
				 tab_tipi_bol_ven.flag_abilita_rif_ord_ven,
				 tab_tipi_bol_ven.cod_tipo_det_ven_rif_ord_ven,
				 tab_tipi_bol_ven.des_riferimento_ord_ven,
				 tab_tipi_bol_ven.flag_rif_anno_ord_ven,
				 tab_tipi_bol_ven.flag_rif_num_ord_ven,
				 tab_tipi_bol_ven.flag_rif_data_ord_ven
		into	 :ls_cod_tipo_analisi_bol,
				 :ls_causale_trasporto_tab,
				 :ls_cod_causale_tab,
				 :ls_flag_abilita_rif_ord_ven,
				 :ls_cod_tipo_det_ven_rif_ord_ven,
				 :ls_des_riferimento_ord_ven,
				 :ls_flag_rif_anno_ord_ven,
				 :ls_flag_rif_num_ord_ven,
				 :ls_flag_rif_data_ord_ven
		from   tab_tipi_bol_ven  
		where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
		
		if sqlca.sqlcode <> 0 then
			fs_errore = "Si è verificato un errore nella lettura della Tabella Tipi Bolle."
			return -1
		end if
	
		ldt_data_registrazione = datetime(today())
		if isnull(ls_nota_testata) then
			ls_nota_testata = ""
		end if
	
		if not isnull(ls_num_ord_cliente) and ls_flag_abilita_rif_ord_ven = "T" then
			ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
			if not isnull(ldt_data_ord_cliente) then
				ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
			else
				ls_vostro_ordine_data = ""
			end if
		else
			ls_vostro_ordine = ""
			ls_vostro_ordine_data = ""
		end if
	
	//-------------------------------------- Modifica Nicola -------------------------------------------------------
		if ls_flag_doc_suc_tes = 'I' then //nota di testata
			select flag_doc_suc
			  into :ls_flag_doc_suc_tes
			  from tab_tipi_ord_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		end if		
	
		if ls_flag_doc_suc_tes = 'N' then
			ls_nota_testata = ""
		end if			
	
		
		if ls_flag_doc_suc_pie = 'I' then //nota di piede
			select flag_doc_suc
			  into :ls_flag_doc_suc_pie
			  from tab_tipi_ord_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
		end if			
		
		if ls_flag_doc_suc_pie = 'N' then
			ls_nota_piede = ""
		end if				
	
	//--------------------------------------- Fine Modifica --------------------------------------------------------
	
	
		if ls_cod_tipo_det_ven_rif_ord_ven = "T" then
			if len(is_rif_packing_list) > 0 and not isnull(is_rif_packing_list) then ls_rif_packing_list = is_rif_packing_list + "~r~n"
			ls_nota_testata = ls_rif_packing_list + ls_des_riferimento_ord_ven + string(fl_anno_registrazione) + "/" + &
									string(fl_num_registrazione) + " del " + string(ldt_data_ordine, "dd/mm/yyyy")+ &
									ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
		end if
		
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
			ls_nota_testata = left(ls_nota_testata, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		
		if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
			ls_causale_trasporto = ls_causale_trasporto_tab
		end if
		if isnull(ls_cod_causale) then
			ls_cod_causale = ls_cod_causale_tab
		end if
		select parametri_azienda.stringa
		into   :ls_lire
		from   parametri_azienda
		where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
				 parametri_azienda.flag_parametro = 'S' and &
				 parametri_azienda.cod_parametro = 'LIR';
	
		if sqlca.sqlcode <> 0 then
			fs_errore = "Configurare il codice valuta per le Lire Italiane."
			return -1
		end if
	
		select tab_pagamenti.sconto
		into   :ld_sconto_pagamento
		from   tab_pagamenti
		where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;
	
		if sqlca.sqlcode <> 0 then
			ld_sconto_pagamento = 0
		end if
		
		if lb_aggiungi then
			
			ldt_ora_inizio_trasporto = datetime(date("01/01/1900"), it_ora_inizio_trasporto)
			ll_num_registrazione = fl_num_reg_bol_ven
			
			select nota_piede
			into   :ls_nota_piede_bolla
			from   tes_bol_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :fl_anno_reg_bol_ven and
					 num_registrazione = :fl_num_reg_bol_ven;
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante ricercaTestata Bolle Vendita. Dettaglio errore " + sqlca.sqlerrtext
				return -1
			end if
	
			if isnull(ls_nota_piede_bolla) then ls_nota_piede_bolla = ""
			if len(trim(ls_nota_piede_bolla)) > 0 then
				//ls_nota_piede_bolla = ls_nota_piede_bolla + "~r~n" + is_nota_piede
				ls_nota_piede_bolla = ls_nota_piede_bolla
			else
				//ls_nota_piede_bolla = ls_nota_piede_bolla + is_nota_piede
				ls_nota_piede_bolla = ls_nota_piede_bolla
			end if
			// ----- controllo lunghezza note per ASE
			if ls_db = "SYBASE_ASE" and len(ls_nota_piede_bolla) > 255 then
				ls_nota_piede_bolla = left(ls_nota_piede_bolla, 255)
			end if
			// -----  fine controllo lunghezza note per ASE
			
			update tes_bol_ven
			set    num_colli  = num_colli + :id_num_colli,
					 peso_lordo = peso_lordo + :id_peso_lordo,
					 peso_netto = peso_netto + :id_peso_netto,
					 rag_soc_1  = :is_rag_soc_1,
					 rag_soc_2  = :is_rag_soc_2,
					 indirizzo  = :is_indirizzo,
					 localita   = :is_localita,
					 cap = :is_cap,
					 provincia = :is_provincia,
					 frazione  = :is_frazione,
					 data_inizio_trasporto = :idt_data_inizio_trasporto,
					 ora_inizio_trasporto  = :ldt_ora_inizio_trasporto,
					 cod_des_cliente = :is_destinazione,
					 cod_causale  = :is_cod_causale,
					 aspetto_beni = :is_aspetto_beni,
					 cod_porto = :is_cod_porto,
					 cod_mezzo = :is_cod_mezzo,
					 cod_vettore = :is_cod_vettore,
					 cod_inoltro = :is_cod_inoltro,
					 cod_imballo = :is_cod_imballo,
					 nota_piede = :ls_nota_piede_bolla
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :fl_anno_reg_bol_ven and
					 num_registrazione = :fl_num_reg_bol_ven;
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante aggiornamento Testata Bolle Vendita. Dettaglio errore " + sqlca.sqlerrtext
				return -1
			end if
			
		else
			
			select max(num_registrazione)
			into   :ll_num_registrazione
			from   tes_bol_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione;
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la ricerca massimo numero registrazione per incremento numeratore interno bolle."
				return -1
			end if
			if isnull(ll_num_registrazione) then ll_num_registrazione = 0
			ll_num_registrazione ++
			update con_bol_ven
			set    num_registrazione = :ll_num_registrazione
			where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;
			
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la scrittura sulla tabella Controllo Bolle Vendita."
				return -1
			end if
			
			if not isnull(is_cod_causale) then ls_cod_causale = is_cod_causale
			if not isnull(is_aspetto_beni) then ls_aspetto_beni = is_aspetto_beni
			if not isnull(is_cod_porto) then ls_cod_porto = is_cod_porto
			if not isnull(is_cod_mezzo) then ls_cod_mezzo = is_cod_mezzo
			if not isnull(is_cod_vettore) then ls_cod_vettore = is_cod_vettore
			if not isnull(is_cod_inoltro) then ls_cod_inoltro = is_cod_inoltro
			if not isnull(is_cod_resa) then ls_cod_resa = is_cod_resa
			if not isnull(is_destinazione) then
				ls_cod_des_cliente = is_destinazione
				ls_rag_soc_1 = is_rag_soc_1
				ls_rag_soc_2 = is_rag_soc_2
				ls_indirizzo = is_indirizzo
				ls_localita = is_localita
				ls_frazione = is_frazione
				ls_cap = is_cap
				ls_provincia = is_provincia
			end if
			if not isnull(id_num_colli) and id_num_colli <> 0 then ld_num_colli = id_num_colli
			if not isnull(id_peso_lordo) and id_peso_lordo <> 0 then ld_peso_lordo = id_peso_lordo
			if not isnull(id_peso_netto) and id_peso_netto <> 0 then ld_peso_netto = id_peso_netto
			ldt_ora_inizio_trasporto = datetime(date("01/01/1900"), it_ora_inizio_trasporto)
			if isnull(ls_num_ord_cliente) or len(ls_num_ord_cliente) < 1 then
				ls_num_ord_cliente = is_num_ord_cliente
			end if
			if isnull(ldt_data_ord_cliente) then
				ldt_data_ord_cliente = idt_data_ord_cliente
			end if
			if isnull(ls_nota_piede) then ls_nota_piede = ""
			if len(trim(ls_nota_piede)) > 0 then
				//ls_nota_piede = ls_nota_piede + "~r~n" + is_nota_piede
				ls_nota_piede = ls_nota_piede
			else
				//ls_nota_piede = ls_nota_piede + is_nota_piede
				ls_nota_piede = ls_nota_piede
			end if
			// ----- controllo lunghezza note per ASE
			if ls_db = "SYBASE_ASE" and len(ls_nota_piede) > 255 then
				ls_nota_piede = left(ls_nota_piede, 255)
			end if
			// -----  fine controllo lunghezza note per ASE
			
			insert into tes_bol_ven
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 data_registrazione,   
							 cod_operatore,   
							 cod_tipo_bol_ven,   
							 cod_cliente,
							 cod_fornitore,
							 cod_des_cliente,   
							 cod_fil_fornitore,
							 rag_soc_1,   
							 rag_soc_2,   
							 indirizzo,   
							 localita,   
							 frazione,   
							 cap,   
							 provincia,   
							 cod_deposito,   
							 cod_deposito_tras,
							 cod_ubicazione,
							 cod_valuta,   
							 cambio_ven,   
							 cod_tipo_listino_prodotto,   
							 cod_pagamento,   
							 sconto,   
							 cod_agente_1,
							 cod_agente_2,
							 cod_banca,   
							 num_ord_cliente,   
							 data_ord_cliente,   
							 cod_imballo,   
							 aspetto_beni,
							 peso_netto,
							 peso_lordo,
							 num_colli,
							 cod_vettore,   
							 cod_inoltro,   
							 cod_mezzo,
							 causale_trasporto,
							 cod_porto,   
							 cod_resa,   
							 cod_documento,
							 numeratore_documento,
							 anno_documento,
							 num_documento,
							 data_bolla,
							 nota_testata,   
							 nota_piede,   
							 flag_fuori_fido,
							 flag_blocco,   
							 flag_movimenti,  
							 flag_riep_fat,
							 cod_banca_clien_for,
							 flag_gen_fat,
							 cod_causale,
							 data_inizio_trasporto,
							 ora_inizio_trasporto,
							 flag_doc_suc_tes,
							 flag_st_note_tes,
							 flag_doc_suc_pie,
							 flag_st_note_pie)  						 
			values      (:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ldt_data_registrazione,   
							 :ls_cod_operatore,   
							 :ls_cod_tipo_bol_ven,   
							 :ls_cod_cliente[1],
							 null,
							 :ls_cod_des_cliente,   
							 null,
							 :ls_rag_soc_1,   
							 :ls_rag_soc_2,   
							 :ls_indirizzo,   
							 :ls_localita,   
							 :ls_frazione,   
							 :ls_cap,   
							 :ls_provincia,   
							 :ls_cod_deposito[1],   
							 null,
							 :ls_cod_ubicazione[1],
							 :ls_cod_valuta,   
							 :ld_cambio_ven,   
							 :ls_cod_tipo_listino_prodotto,   
							 :ls_cod_pagamento,   
							 :ld_sconto_testata,
							 :ls_cod_agente_1,
							 :ls_cod_agente_2,
							 :ls_cod_banca,   
							 :ls_num_ord_cliente,   
							 :ldt_data_ord_cliente,   
							 :ls_cod_imballo,   
							 :ls_aspetto_beni,
							 :ld_peso_netto,
							 :ld_peso_lordo,
							 :ld_num_colli,
							 :ls_cod_vettore,   
							 :ls_cod_inoltro,   
							 :ls_cod_mezzo,   
							 :ls_causale_trasporto,
							 :ls_cod_porto,   
							 :ls_cod_resa,   
							 null,
							 null,
							 0,
							 0,
							 null,
							 :ls_nota_testata,   
							 :ls_nota_piede,   
							 'N',
							 'N',   
							 'N',
							 :ls_flag_riep_fat,
							 :ls_cod_banca_clien_for,
							 'N',
							 :ls_cod_causale,
							 :idt_data_inizio_trasporto,
							 :ldt_ora_inizio_trasporto,
							 'I',
							 'I',
							 'I',
							 'I');						 
			 if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante inserimento Testata Bolle Vendita. Dettaglio errore " + sqlca.sqlerrtext
				return -1
			 end if
			 
			 if uof_crea_nota_fissa("BOL_VEN", ll_anno_registrazione, ll_num_registrazione, fs_errore) = -1 then return -1
			 
			 lb_aggiungi = true
			 
		end if
	
		lb_riferimento = true
		if lb_aggiungi then
			select max(prog_riga_bol_ven)
			into   :ll_prog_riga_bol_ven
			from   det_bol_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione;
			
			if isnull(ll_prog_riga_bol_ven) then ll_prog_riga_bol_ven = 0
		else
			ll_prog_riga_bol_ven = 0	
		end if
		
		
		// ***** metto il riferimento all'ordine come prima cosa solo se il tipo della bolla lo richiede
		// *****
		if ls_flag_abilita_rif_ord_ven = "D" then
						
			lb_riferimento = false
			setnull(ls_note_dettaglio_rif)
							
			ls_des_riferimento = ls_des_riferimento_ord_ven
			if ls_flag_rif_anno_ord_ven = "S" and not isnull(ll_anno_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" +string(ll_anno_ordine)
			end if
			if ls_flag_rif_num_ord_ven = "S" and not isnull(ll_num_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(ll_num_ordine)
			end if
			if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
			end if
					
			if not isnull(ls_num_ord_cliente) then
				if not isnull(ls_note_dettaglio_rif) and len(ls_note_dettaglio_rif) > 0 then 
					ls_note_dettaglio_rif += "~r~n"
				else
					ls_note_dettaglio_rif = ""
				end if
				ls_note_dettaglio_rif = ls_note_dettaglio_rif + "Vs.Ordine " + ls_num_ord_cliente
				if not isnull(ldt_data_ord_cliente) then
					ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
				end if
			end if
						
			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_ord_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
				g_mb.messagebox("Generazione Bolla Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if
			
			ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
							
			insert into det_bol_ven  
								(cod_azienda,   
								 anno_registrazione,   
								 num_registrazione,   
								 prog_riga_bol_ven,   
								 cod_tipo_det_ven,   
								 cod_deposito,
								 cod_ubicazione,
								 cod_lotto,
								 progr_stock,
								 data_stock,
								 cod_misura,   
									cod_prodotto,   
									des_prodotto,   
									quan_consegnata,   
									prezzo_vendita,   
									fat_conversione_ven,   
											 sconto_1,   
											 sconto_2,   
											 provvigione_1,
											 provvigione_2,
											 cod_iva,   
											 cod_tipo_movimento,
											 num_registrazione_mov_mag,
											 nota_dettaglio,   
											 anno_registrazione_ord_ven,   
											 num_registrazione_ord_ven,   
											 prog_riga_ord_ven,
											 anno_commessa,
											 num_commessa,
											 cod_centro_costo,
											 sconto_3,   
											 sconto_4,   
											 sconto_5,   
											 sconto_6,   
											 sconto_7,   
											 sconto_8,   
											 sconto_9,   
											 sconto_10,
											 anno_registrazione_mov_mag,
											 anno_reg_bol_acq,
											 num_reg_bol_acq,
											 prog_riga_bol_acq,
											 anno_reg_des_mov,
											 num_reg_des_mov,
											 cod_versione,
											 num_confezioni,
											 num_pezzi_confezione,
											 flag_doc_suc_det,
											 flag_st_note_det,
											 quantita_um,
											 prezzo_um)							 
			  values			(:s_cs_xx.cod_azienda,   
											 :ll_anno_registrazione,   
											 :ll_num_registrazione,   
											 :ll_prog_riga_bol_ven,   
											 :ls_cod_tipo_det_ven_rif_ord_ven,   
											 null,
											 null,
											 null,
											 null,
											 null,
											 null,   
											 null,   
											 :ls_des_riferimento,   
											 0,   
											 0,   
											 1,   
											 0,   
											 0,  
											 0,
											 0,
											 :ls_cod_iva_rif,   
											 null,
											 null,
											 :ls_note_dettaglio_rif,   
											 null,   
											 null,   
											 null,
											 null,   
											 null,   
											 null,   
											 0,   
											 0,   
											 0,   
											 0,   
											 0,   
											 0,   
											 0,   
											 0,
											 null,
											 null,
											 null,
											 null,
											 null,
											 null,
											 null,
											 0,
											 0,
											 'I',
											 'I',
											 0,
											 0);							 
					
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la scrittura Dettagli Bolle Vendita: " + sqlca.sqlerrtext
				return -1
			end if
						
		end if		
	
		 // *** michela: uso un datastore anzichè un cursore
	
		datastore lds_appo, lds_appo3
		string    ls_sql, ls_errore, ls_sintassi, ls_flag_spedito, ls_rif
		long      ll_errore, ll_pos, ll_anno_pl, ll_num_pl, ll_cont
		integer   li_i, li_k
		decimal   ld_num_collo, ld_quan_pack_list
		
		// ordino per packing list
		
		ls_sql = "select  num_pack_list  " + & 
					"from    det_ord_ven_pack_list   " + &
					"where   cod_azienda = '" + s_cs_xx.cod_azienda + "' and  " + &
					"        anno_registrazione = " + string(ll_anno_ordine) + " and " + &
					"        num_registrazione = " + string(ll_num_ordine) + "  and ("
		
		for li_i = 1 to Upperbound(fs_elenco_pl)
			ls_sql += " ( num_pack_list = " + right( fs_elenco_pl[li_i], len(fs_elenco_pl[li_i]) - 5) + " )"
			if ( li_i + 1 ) <= Upperbound(fs_elenco_pl) then
				ls_sql += " or "
			end if
		next
		
		ls_sql += " ) group by num_pack_list order by num_pack_list  " 

		ls_sintassi = SQLCA.SyntaxFromSQL(ls_sql, "style(type=grid)", ls_errore)
		IF Len(ls_errore) > 0 THEN
			fs_errore =  "Errore durante la creazione del datastore:" + ls_errore
			return -1			
		END IF
		
		lds_appo3 = create datastore
		lds_appo3.Create( ls_sintassi, ls_errore)
		IF Len(ls_errore) > 0 THEN
			fs_errore =  "Errore durante la creazione del datastore:" + ls_errore
			return -1			
		END IF
		
		ll_errore = lds_appo3.settransobject( sqlca)
		if ll_errore < 0 then
			fs_errore =  "Errore durante impostazione oggetto transazione:" + sqlca.sqlerrtext
			return -1			
		end if
		ll_errore = lds_appo3.retrieve()
		if ll_errore < 0 then
			fs_errore =  "Errore durante la retrieve:" + sqlca.sqlerrtext
			return -1						
		end if		
		
		// adesso scorro tutti i packing list e guardo tutte le righe che contiene. se è contenuto faccio tutto
		
		for li_i = 1 to lds_appo3.rowcount()
			
			ll_anno_pl = ll_anno_ordine                      //long(left( fs_elenco_pl[li_i], 4))
			ll_num_pl = lds_appo3.getitemnumber( li_i, 1)    //long(right( fs_elenco_pl[li_i], len(fs_elenco_pl[li_i]) - 5))
			ls_rif = "Rif. Packing List " + string(ll_anno_pl) + "-" + string(ll_num_pl)
			
			
			/// **************** qui metto l'inserimento del riferimento al packing list
			
			ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10

			insert into det_bol_ven (cod_azienda,   
											 anno_registrazione,   
											 num_registrazione,   
											 prog_riga_bol_ven,   
											 cod_tipo_det_ven,   
											 cod_deposito,
											 cod_ubicazione,
											 cod_lotto,
											 progr_stock,
											 data_stock,
											 cod_misura,   
											 cod_prodotto,   
											 des_prodotto,   
											 quan_consegnata,   
											 prezzo_vendita,   
											 fat_conversione_ven,   
											 sconto_1,   
											 sconto_2,   
											 provvigione_1,
											 provvigione_2,
											 cod_iva,   
											 cod_tipo_movimento,
											 num_registrazione_mov_mag,
											 nota_dettaglio,   
											 anno_registrazione_ord_ven,   
											 num_registrazione_ord_ven,   
											 prog_riga_ord_ven,
											 anno_commessa,
											 num_commessa,
											 cod_centro_costo,
											 sconto_3,   
											 sconto_4,   
											 sconto_5,   
											 sconto_6,   
											 sconto_7,   
											 sconto_8,   
											 sconto_9,   
											 sconto_10,
											 anno_registrazione_mov_mag,
											 anno_reg_bol_acq,
											 num_reg_bol_acq,
											 prog_riga_bol_acq,
											 anno_reg_des_mov,
											 num_reg_des_mov,
											 cod_versione,
											 num_confezioni,
											 num_pezzi_confezione,
											 flag_doc_suc_det,
											 flag_st_note_det,
											 quantita_um,
											 prezzo_um)							 
			values			         (:s_cs_xx.cod_azienda,   
											 :ll_anno_registrazione,   
											 :ll_num_registrazione,   
											 :ll_prog_riga_bol_ven,   
											 :ls_cod_tipo_det_ven_rif_ord_ven,   
											 null,
											 null,
											 null,
											 null,
											 null,
											 null,   
											 null,   
											 :ls_rif,   
											 0,   
											 0,   
											 1,   
											 0,   
											 0,  
											 0,
											 0,
											 :ls_cod_iva_rif,   
											 null,
											 null,
											 :ls_rif,   
											 null,   
											 null,   
											 null,
											 null,   
											 null,   
											 null,   
											 0,   
											 0,   
											 0,   
											 0,   
											 0,   
											 0,   
											 0,   
											 0,
											 null,
											 null,
											 null,
											 null,
											 null,
											 null,
											 null,
											 0,
											 0,
											 'I',
											 'I',
											 0,
											 0);							 
						
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la scrittura Dettagli Bolle Vendita: " + sqlca.sqlerrtext
				destroy lds_appo3
				return -1
			end if			
						
			// ************ trovo tutte le righe dell'ordine di quel packing list, guardando che le righe evase che vado a mettere
			//              appartengano quindi a quel packing list
			
//			ls_sql = "select  evas_ord_ven.prog_riga_ord_ven,  " + & 
//					"        evas_ord_ven.prog_riga,  " + &
//					"        evas_ord_ven.cod_deposito,    " + &
//					"        evas_ord_ven.cod_ubicazione,    " + &
//					"        evas_ord_ven.cod_lotto,    " + &
//					"        evas_ord_ven.data_stock,    " + &
//					"        evas_ord_ven.prog_stock,    " + &
//					"        evas_ord_ven.quan_in_evasione " + &
//					"from    evas_ord_ven   " + &
//					"where   evas_ord_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' and  " + &
//					"        evas_ord_ven.anno_registrazione = " + string(ll_anno_ordine) + " and " + &
//					"        evas_ord_ven.num_registrazione = " + string(ll_num_ordine) + " and " + &
//					"        evas_ord_ven.prog_riga_ord_ven in " + &					
//					"              ( select  distinct(prog_riga_ord_ven)  " + & 
//					"                from    det_ord_ven_pack_list   " + &
//					"                where   cod_azienda = '" + s_cs_xx.cod_azienda + "' and  " + &
//					"                        anno_registrazione = " + string(ll_anno_ordine) + " and " + &
//					"                        num_registrazione = " + string(ll_num_ordine) + "  and " + &
//					"                        num_pack_list = " + string(ll_num_pl) + " " + &						
//					"              )                                                    " + &					
//					"order by evas_ord_ven.cod_prodotto "
//
//			ls_sintassi = SQLCA.SyntaxFromSQL(ls_sql, "style(type=grid)", ls_errore)
//			IF Len(ls_errore) > 0 THEN
//				fs_errore =  "Errore durante la creazione del datastore:" + ls_errore
//				destroy lds_appo3
//				return -1			
//			END IF
//			
//			lds_appo = create datastore
//			lds_appo.Create( ls_sintassi, ls_errore)
//			IF Len(ls_errore) > 0 THEN
//				fs_errore =  "Errore durante la creazione del datastore:" + ls_errore
//				destroy lds_appo3
//				return -1			
//			END IF
//			
//			ll_errore = lds_appo.settransobject( sqlca)
//			if ll_errore < 0 then
//				fs_errore =  "Errore durante impostazione oggetto transazione:" + sqlca.sqlerrtext
//				destroy lds_appo3
//				return -1			
//			end if
//			ll_errore = lds_appo.retrieve()
//			if ll_errore < 0 then
//				fs_errore =  "Errore durante la retrieve:" + sqlca.sqlerrtext
//				destroy lds_appo3
//				return -1						
//			end if
			
			lds_appo = create datastore
			lds_appo.dataobject = "d_pack_list_evas"
			lds_appo.settransobject( sqlca)
			lds_appo.retrieve( s_cs_xx.cod_azienda, ll_anno_ordine, ll_num_ordine, ll_num_pl)
			
			
			for li_k = 1 to lds_appo.rowcount()
				
				ll_prog_riga_ord_ven = lds_appo.getitemnumber( li_k, 1) 
				ll_prog_riga = lds_appo.getitemnumber( li_k, 2)
				ls_cod_deposito[1] = lds_appo.getitemstring( li_k, 4)
				ls_cod_ubicazione[1] = lds_appo.getitemstring( li_k, 5)
				ls_cod_lotto[1] = lds_appo.getitemstring( li_k, 6)
				ldt_data_stock[1] = lds_appo.getitemdatetime( li_k, 7)
				ll_progr_stock[1] = lds_appo.getitemnumber( li_k, 8)
				ld_quan_in_evasione = lds_appo.getitemnumber( li_k, 9)
				
				if isnull(ll_prog_riga) or ll_prog_riga < 0 then continue
								
				select	det_ord_ven.cod_tipo_det_ven,   
							det_ord_ven.cod_misura,   
							det_ord_ven.cod_prodotto,   
							det_ord_ven.des_prodotto,   
							det_ord_ven.quan_ordine,
							det_ord_ven.prezzo_vendita,   
							det_ord_ven.fat_conversione_ven,   
							det_ord_ven.sconto_1,   
							det_ord_ven.sconto_2,  
							det_ord_ven.provvigione_1,
							det_ord_ven.provvigione_2,
							det_ord_ven.cod_iva,   
							det_ord_ven.quan_evasa,
							det_ord_ven.nota_dettaglio,   
							det_ord_ven.anno_commessa,
							det_ord_ven.num_commessa,
							det_ord_ven.cod_centro_costo,
							det_ord_ven.sconto_3,   
							det_ord_ven.sconto_4,   
							det_ord_ven.sconto_5,   
							det_ord_ven.sconto_6,   
							det_ord_ven.sconto_7,   
							det_ord_ven.sconto_8,   
							det_ord_ven.sconto_9,   
							det_ord_ven.sconto_10,
							det_ord_ven.cod_versione,
							det_ord_ven.num_confezioni,
							det_ord_ven.num_pezzi_confezione,
							det_ord_ven.flag_doc_suc_det,
							det_ord_ven.quantita_um,
							det_ord_ven.prezzo_um,
							det_ord_ven.nota_prodotto,
							det_ord_ven.flag_trasporta_nota_ddt
				into		:ls_cod_tipo_det_ven, 
							:ls_cod_misura, 
							:ls_cod_prodotto, 
							:ls_des_prodotto, 
							:ld_quan_ordine,
							:ld_prezzo_vendita, 
							:ld_fat_conversione_ven, 
							:ld_sconto_1, 
							:ld_sconto_2, 
							:ld_provvigione_1, 
							:ld_provvigione_2, 
							:ls_cod_iva, 
							:ld_quan_evasa,
							:ls_nota_dettaglio, 
							:ll_anno_commessa,
							:ll_num_commessa,
							:ls_cod_centro_costo,
							:ld_sconto_3, 
							:ld_sconto_4, 
							:ld_sconto_5, 
							:ld_sconto_6, 
							:ld_sconto_7, 
							:ld_sconto_8, 
							:ld_sconto_9, 
							:ld_sconto_10,
							:ls_cod_versione,
							:ll_num_confezioni,
							:ll_num_pezzi_confezione,
							:ls_flag_doc_suc_det,
							:ld_quantita_um,
							:ld_prezzo_um,
							:ls_nota_prodotto,
							:ls_flag_trasporta_nota_ddt
				  from 	det_ord_ven  
				 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
							det_ord_ven.anno_registrazione = :ll_anno_ordine and  
							det_ord_ven.num_registrazione = :ll_num_ordine and
							det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven
				order by det_ord_ven.prog_riga_ord_ven asc;
				
				if sqlca.sqlcode = -1 then
					fs_errore =  "Errore durante la lettura Dettagli Ordini Vendita: " + sqlca.sqlerrtext
					destroy lds_appo3
					destroy lds_appo
					return -1
				end if					

				//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
				// ricalcolo la quantità di vendita nel caso di evasioni parziali
				if ld_quan_in_evasione <> ld_quan_ordine then
					ld_quantita_um = round(ld_quan_in_evasione * ld_fat_conversione_ven, 4)
				end if
				//------------------------------------------------- Modifica Nicola ---------------------------------------------
				if ls_flag_doc_suc_det = 'I' then //nota dettaglio
				
					select flag_doc_suc_or
				   into   :ls_flag_doc_suc_det
		         from   tab_tipi_det_ven
		         where  cod_azienda = :s_cs_xx.cod_azienda and 
					       cod_tipo_det_ven = :ls_cod_tipo_det_ven;
					end if		
	
	
					if ls_flag_doc_suc_det = 'N' then
						setnull(ls_nota_dettaglio)
					end if				

					//-------------------------------------------------- Fine Modifica ----------------------------------------------		
					//------------------------------------------------- Modifica Michela ---------------------------------------------
					// se si può allora aggiungo la nota prodotto alla nota dettaglio
					if ls_flag_trasporta_nota_ddt = "S" then
						if isnull(ls_nota_dettaglio) then 
							ls_nota_dettaglio = "Nota prodotto:" + ls_nota_prodotto
						else
							if not isnull(ls_nota_prodotto) then
								ls_nota_dettaglio = ls_nota_dettaglio + "~r~nNota prodotto:" + ls_nota_prodotto
							end if
						end if
					end if
					//-------------------------------------------------- Fine Modifica ----------------------------------------------					
					select tab_tipi_det_ven.flag_tipo_det_ven,
							 tab_tipi_det_ven.cod_tipo_movimento,
							 tab_tipi_det_ven.flag_sola_iva
					into   :ls_flag_tipo_det_ven,
							 :ls_cod_tipo_movimento,
							 :ls_flag_sola_iva
					from   tab_tipi_det_ven
					where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
							 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

					if sqlca.sqlcode = -1 then
						fs_errore = "Si è verificato un errore in fase di lettura tipi dettagli: " + sqlca.sqlerrtext
						destroy lds_appo3
						destroy lds_appo
					return -1
				end if

				if ls_flag_tipo_det_ven = "M" then
					if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
						fs_errore = "Si è verificato un errore in fase di creazione destinazioni movimenti."
						destroy lds_appo3
						destroy lds_appo
						return -1
					end if

					if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
						destroy lds_appo3
						destroy lds_appo
						return -1
					end if
				end if
				
				ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
	
				insert into det_bol_ven (cod_azienda,   
												 anno_registrazione,   
												 num_registrazione,   
												 prog_riga_bol_ven,   
												 cod_tipo_det_ven,   
												 cod_deposito,
												 cod_ubicazione,
												 cod_lotto,
												 progr_stock,
												 data_stock,
												 cod_misura,   
												 cod_prodotto,   
												 des_prodotto,   
												 quan_consegnata,   
												 prezzo_vendita,   
												 fat_conversione_ven,   
												 sconto_1,   
												 sconto_2,   
												 provvigione_1,
												 provvigione_2,
												 cod_iva,   
												 cod_tipo_movimento,
												 num_registrazione_mov_mag,
												 nota_dettaglio,   
												 anno_registrazione_ord_ven,   
												 num_registrazione_ord_ven,   
												 prog_riga_ord_ven,
												 anno_commessa,
												 num_commessa,
												 cod_centro_costo,
												 sconto_3,   
												 sconto_4,   
												 sconto_5,   
												 sconto_6,   
												 sconto_7,   
												 sconto_8,   
												 sconto_9,   
												 sconto_10,
												 anno_registrazione_mov_mag,
												 anno_reg_bol_acq,
												 num_reg_bol_acq,
												 prog_riga_bol_acq,
												 anno_reg_des_mov,
												 num_reg_des_mov,
												 cod_versione,
												 num_confezioni,
												 num_pezzi_confezione,
												 flag_doc_suc_det,
												 flag_st_note_det,
												 quantita_um,
												 prezzo_um)
					  values			      (:s_cs_xx.cod_azienda,   
												 :ll_anno_registrazione,   
												 :ll_num_registrazione,   
												 :ll_prog_riga_bol_ven,   
												 :ls_cod_tipo_det_ven,   
												 :ls_cod_deposito[1],
												 :ls_cod_ubicazione[1],
												 :ls_cod_lotto[1],
												 :ll_progr_stock[1],
												 :ldt_data_stock[1],
												 :ls_cod_misura,   
												 :ls_cod_prodotto,   
												 :ls_des_prodotto,   
												 :ld_quan_in_evasione,   
												 :ld_prezzo_vendita,   
												 :ld_fat_conversione_ven,   
												 :ld_sconto_1,   
												 :ld_sconto_2,  
												 :ld_provvigione_1,
												 :ld_provvigione_2,
												 :ls_cod_iva,   
												 :ls_cod_tipo_movimento,
												 null,
												 :ls_nota_dettaglio,   
												 :ll_anno_ordine,   
												 :ll_num_ordine,   
												 :ll_prog_riga_ord_ven,
												 :ll_anno_commessa,   
												 :ll_num_commessa,   
												 :ls_cod_centro_costo,   
												 :ld_sconto_3,   
												 :ld_sconto_4,   
												 :ld_sconto_5,   
												 :ld_sconto_6,   
												 :ld_sconto_7,   
												 :ld_sconto_8,   
												 :ld_sconto_9,   
												 :ld_sconto_10,
												 null,
												 null,
												 null,
												 null,
												 :ll_anno_reg_des_mov,
												 :ll_num_reg_des_mov,
												 :ls_cod_versione,
												 :ll_num_confezioni,
												 :ll_num_pezzi_confezione,
												 'I',
												 'I',
												 :ld_quantita_um,
												 :ld_prezzo_um);									 
												 
				if sqlca.sqlcode <> 0 then
					fs_errore = "Errore durante la scrittura Dettagli Bolle Vendita: " + sqlca.sqlerrtext
					destroy lds_appo3
					destroy lds_appo
					return -1
				end if

				if ls_flag_tipo_det_ven = "M" then
					
					update anag_prodotti  
					set    quan_assegnata = quan_assegnata - :ld_quan_in_evasione,
						    quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione
					where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						    anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
					if sqlca.sqlcode <> 0 then
						fs_errore = "Si è verificato un errore in fase di aggiornamento magazzino: " + sqlca.sqlerrtext
						destroy lds_appo3
						destroy lds_appo
						return -1
					end if
			
					update stock 
					set 	 quan_assegnata = quan_assegnata - :ld_quan_in_evasione,   
							 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione  
					where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
							 stock.cod_prodotto = :ls_cod_prodotto and 
							 stock.cod_deposito = :ls_cod_deposito[1] and 
							 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
							 stock.cod_lotto = :ls_cod_lotto[1] and 
							 stock.data_stock = :ldt_data_stock[1] and 
							 stock.prog_stock = :ll_progr_stock[1];

					if sqlca.sqlcode <> 0 then
						fs_errore = "Si è verificato un errore in fase di aggiornamento stock: " + sqlca.sqlerrtext
						destroy lds_appo3
						destroy lds_appo
						return -1
					end if
				end if				

				ls_tabella_orig = "det_ord_ven_stat"
				ls_tabella_des = "det_bol_ven_stat"
				ls_prog_riga_doc_orig = "prog_riga_ord_ven"
				ls_prog_riga_doc_des = "prog_riga_bol_ven"

				if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ll_anno_ordine, ll_num_ordine, ll_prog_riga_ord_ven, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven) = -1 then
					return -1
				end if	

				if ls_cod_tipo_analisi_bol <> ls_cod_tipo_analisi_ord then
					if f_crea_distribuzione(ls_cod_tipo_analisi_bol, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_bol_ven, 5) = -1 then
						fs_errore = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
						destroy lds_appo3
						destroy lds_appo
						return -1
					end if
				end if

				if ld_quan_ordine <= ld_quan_evasa + ld_quan_in_evasione then
					ls_flag_evasione = "E"
				else
					ls_flag_evasione = "P"
				end if
		
				update det_ord_ven  
				set    quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione,
				 		 quan_evasa = quan_evasa + :ld_quan_in_evasione,
				 		 flag_evasione = :ls_flag_evasione
		 		where  det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 		 det_ord_ven.anno_registrazione = :ll_anno_ordine and  
						 det_ord_ven.num_registrazione = :ll_num_ordine and
						 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
				if sqlca.sqlcode = -1 then
					fs_errore = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Vendita: " + sqlca.sqlerrtext
					destroy lds_appo3
					destroy lds_appo
					return -1
				end if

				if ls_flag_sola_iva = 'N' then
					ld_val_riga = ld_quan_in_evasione * ld_prezzo_vendita
					ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
					ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
					ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
					ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
					ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
					ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
					ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
					ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
					ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
					ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
					ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
					ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
					ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
					ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
					ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
					ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
					ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
					ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
					ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
		
					if ls_cod_valuta = ls_lire then
						ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
					else
						ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
					end if   
	
					if ls_flag_tipo_det_ven = "M" or &
						ls_flag_tipo_det_ven = "C" or &
						ls_flag_tipo_det_ven = "N" then
						ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
						ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
						ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
						ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
					elseif ls_flag_tipo_det_ven = "S" then
						ld_val_evaso = ld_val_riga_sconto_10 * -1
					else
						ld_val_evaso = ld_val_riga_sconto_10
					end if
					ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
				end if

				if ls_cod_valuta = ls_lire then
					ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
				end if   

				delete from evas_ord_ven 
				where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
								evas_ord_ven.anno_registrazione = :ll_anno_ordine and
								evas_ord_ven.num_registrazione = :ll_num_ordine and 
								evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
								evas_ord_ven.prog_riga = :ll_prog_riga;
	
				if sqlca.sqlcode = -1 then
					fs_errore = "Si è verificato un errore in fase di cancellazione Evasione Ordini."
					destroy lds_appo3
					destroy lds_appo
					return -1
				end if												
						
			next		
			
			destroy lds_appo			
			
		next		
		
		destroy lds_appo3;
		
		if ld_tot_val_ordine <= ld_tot_val_evaso then
			ls_flag_evasione = "E"
		else
			ls_flag_evasione = "P"
		end if

		if uof_calcola_stato_ordine(ll_anno_ordine,ll_num_ordine) = -1 then
			fs_errore = "Si è verificato un errore in fase di aggiornamento stato dell'ordine in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
			destroy lds_appo3
			destroy lds_appo
			return -1
		end if

		luo_calcolo = create uo_calcola_documento_euro

		if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_ven",fs_errore) <> 0 then
			destroy luo_calcolo
			destroy lds_appo3
			destroy lds_appo
			return -1
		end if
	
		destroy luo_calcolo

		fl_anno_reg_bol_ven = ll_anno_registrazione
		fl_num_reg_bol_ven = ll_num_registrazione
		
	end if

next   // fine del ciclo for

destroy lds_appo2
return 0

end function

public function integer uof_genera_rda (string fs_cod_prodotto, string fs_des_prodotto, string fs_cod_versione, decimal fd_quan_richiesta, long fl_anno_commessa, long fl_num_commessa, datetime fdt_data_disponibilita, string fs_cod_tipo_politica_riordino, string fs_flag_gen_commessa, string fs_motivazione, ref long fl_anno_registrazione, ref long fl_num_registrazione, ref string fs_errore);long li_ret, ll_null
string  ls_cod_fornitore, ls_des_specifica

setnull(ll_null)
setnull(ls_cod_fornitore)
setnull(ls_des_specifica)

li_ret = uof_genera_rda(fs_cod_prodotto, fs_des_prodotto, fs_cod_versione, ls_cod_fornitore, fd_quan_richiesta,0, fl_anno_commessa, fl_num_commessa, fdt_data_disponibilita, fs_cod_tipo_politica_riordino, fs_flag_gen_commessa,fs_motivazione, ls_des_specifica, ll_null, ll_null, ll_null,ref fl_anno_registrazione, ref fl_num_registrazione, ref fs_errore)
return li_ret
end function

public function integer uof_genera_commessa (string fs_cod_prodotto, string fs_cod_versione, decimal fd_quan_ordine, datetime fdt_data_consegna, boolean fb_attiva, ref long fl_anno_commessa, ref long fl_num_commessa, ref string fs_errore);string   ls_cod_tipo_commessa,ls_cod_deposito_versamento,ls_cod_deposito_prelievo, ls_flag_muovi_mp, ls_test, ls_cod_tipo_movimento, ls_cod_tipo_mov_anticipo
long     ll_anno_registrazione, ll_num_registrazione, ll_prog_riga
datetime ldt_data_registrazione
integer  li_risposta
dec{4}   ldd_quan_in_produzione

ll_anno_registrazione = f_anno_esercizio()

select cod_tipo_commessa
into   :ls_cod_tipo_commessa
from   con_rda
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	fs_errore = "Si è verificato un errore durante la lettura del tipo commessa dalla tabella con_rda." + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_tipo_commessa) then
	fs_errore = "Attenzione: nella con_rda non è specificato il tipo commessa."
	return -1
end if

select cod_deposito_versamento,
		 cod_deposito_prelievo,
		 flag_muovi_mp
into   :ls_cod_deposito_versamento,
		 :ls_cod_deposito_prelievo,
		 :ls_flag_muovi_mp
from   tab_tipi_commessa
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_tipo_commessa = :ls_cod_tipo_commessa;

if sqlca.sqlcode <> 0 then
	fs_errore = "Si è verificato un errore durante la lettura Tabella Tipi Commessa." + sqlca.sqlerrtext
	return -1
end if

ldt_data_registrazione = datetime(today())


select max(num_commessa)
into   :ll_num_registrazione
from   anag_commesse
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_commessa = :ll_anno_registrazione;
	
if sqlca.sqlcode <0 then
	fs_errore = "Errore durante la lettura delle Commesse." + sqlca.sqlerrtext
	return -1
end if

if isnull(ll_num_registrazione) then
	ll_num_registrazione= 1
else
	ll_num_registrazione++
end if


insert into anag_commesse  
				(cod_azienda,   
				 anno_commessa,   
				 num_commessa,   
				 cod_tipo_commessa,   
				 data_registrazione,
				 cod_operatore,
				 nota_testata,
				 nota_piede,
				 flag_blocco,
				 quan_ordine,   
				 quan_prodotta,
				 data_chiusura,
				 data_consegna,
				 cod_prodotto,
				 quan_assegnata,
				 quan_in_produzione,
				 cod_deposito_versamento,
				 cod_deposito_prelievo,
				 cod_versione)
values		(:s_cs_xx.cod_azienda,   
				 :ll_anno_registrazione,   
				 :ll_num_registrazione,   
				 :ls_cod_tipo_commessa,   
				 :ldt_data_registrazione,
				 null,
				 null,
				 null,
				 'N',
				 :fd_quan_ordine,
				 0,
				 null,
				 :fdt_data_consegna,
				 :fs_cod_prodotto,
				 0,
				 0,
				 :ls_cod_deposito_versamento,
				 :ls_cod_deposito_prelievo,
				 :fs_cod_versione);

if sqlca.sqlcode < 0 then
	fs_errore = "Errore durante la scrittura delle Commesse. Errore DB:" + sqlca.sqlerrtext
	return -1			
end if

fl_anno_commessa = ll_anno_registrazione
fl_num_commessa = ll_num_registrazione

if fb_attiva then
	
	select cod_prodotto_figlio
	into   :ls_test
	from   distinta
	where  cod_azienda = :s_cs_xx.cod_azienda and    
			 cod_prodotto_padre = :fs_cod_prodotto;
	
	if sqlca.sqlcode = 100 then
		fs_errore = "Attenzione! Il prodotto della commessa non ha un distinta base associata, pertanto non sarà possibile assegnare le materie prime. Creare la distinta base del prodotto"
		return -1
	end if

	select cod_tipo_mov_ver_prod_finiti,
			 cod_tipo_mov_anticipo
	into   :ls_cod_tipo_movimento,
			 :ls_cod_tipo_mov_anticipo
	from   tab_tipi_commessa
	where  cod_azienda = :s_cs_xx.cod_azienda and    
			 cod_tipo_commessa = :ls_cod_tipo_commessa;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore nel DB:" + sqlca.sqlerrtext
		return -1
	end if

	if isnull(ls_cod_tipo_movimento) or isnull(ls_cod_tipo_mov_anticipo) then
		fs_errore = "Manca la configurazione dei tipi movimento in tabella tipi commessa!"
		return -1
	end if

	

	select max(prog_riga)
	into   :ll_prog_riga
	from   det_anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and    
			 anno_commessa = :ll_anno_registrazione and    
			 num_commessa = :ll_num_registrazione;

	if sqlca.sqlcode<>0 then
		fs_errore = "Errore nel DB:" + sqlca.sqlerrtext
		return -1
	end if
	
	if not isnull(ll_prog_riga) then
		ll_prog_riga++
	else
		ll_prog_riga = 1
	end if
	
	INSERT INTO det_anag_commesse
				 ( cod_azienda,
					anno_commessa,   
					num_commessa,   
					prog_riga,   
					anno_registrazione,   
					num_registrazione,   
					quan_assegnata,   
					quan_in_produzione,   
					quan_prodotta,
					cod_tipo_movimento,
					anno_reg_des_mov,
					num_reg_des_mov,
					cod_tipo_mov_anticipo,
					quan_anticipo,
					anno_reg_anticipo,
					num_reg_anticipo )  
	VALUES    ( :s_cs_xx.cod_azienda,   
					:ll_anno_registrazione,   
					:ll_num_registrazione,   
					:ll_prog_riga,   
					null,   
					null,
					0,
					:fd_quan_ordine,
					0,
					:ls_cod_tipo_movimento,
					null,
					null,
					:ls_cod_tipo_mov_anticipo,
					0,
					null,
					null);
	
	if sqlca.sqlcode<>0 then
		fs_errore = "Errore nel DB:" + sqlca.sqlerrtext
		return -1
	end if
	
	li_risposta = f_avan_prod_com( ll_anno_registrazione, &
	                                ll_num_registrazione, &
											  ll_prog_riga, & 
											  fs_cod_prodotto, &
											  fs_cod_versione, &
											  fd_quan_ordine, & 
											  1, &
											  ls_cod_tipo_commessa, &
											  fs_errore )
	
	
	if li_risposta = -1 then
		return -1
	end if
	
	select sum(quan_in_produzione)
	into   :ldd_quan_in_produzione
	from   det_anag_commesse
	where  cod_azienda = :s_cs_xx.cod_azienda and    
			 anno_commessa = :ll_anno_registrazione and    
			 num_commessa = :ll_num_registrazione;
	
	if sqlca.sqlcode<>0 then
		fs_errore = "Errore nel DB:" + sqlca.sqlerrtext
		return -1
	end if
	
	update anag_commesse
	set    quan_in_produzione = :ldd_quan_in_produzione,
			 flag_tipo_avanzamento = '2'
	where  cod_azienda = :s_cs_xx.cod_azienda and    
			 anno_commessa = :ll_anno_registrazione and    
			 num_commessa = :ll_num_registrazione;
	
	if sqlca.sqlcode<>0 then
		fs_errore = "Errore nel DB:" + sqlca.sqlerrtext
		return -1
	end if	
	
end if
	

return 0
end function

public function integer uof_genera_ordini_fornitori (string fs_cod_prodotto[], string fs_des_prodotto[], decimal fd_quan_ordine[], datetime fdt_data_fabbisogno[], string fs_flag_gen_commessa[], string fs_cod_versione[], string fs_note[], ref long fl_anno_ordine[], ref long fl_num_ordine[], ref string fs_messaggio);/*
	FUNZIONE DI GENERAZIONE ORDINI FORNITORI IN BASE AD UNA LISTA
	DI PRODOTTI PASSATI; RITORNA L'ELENCO DEGLI ORDINI PASSATI.
	SE NON TROVA IL FORNITORE PREFERENZIALE NEI LISTINI FORNITORI O
	NELL'ANAGRAFICA PRODOTTI LA FUNZIONE PROVVEDE A RICHIEDERLO.
	
			PARAMETRO				SIGNIFICATO
	------------------------------------------------------------------------------------
			fs_cod_prodotto[]		elenco prodotti di cui eseguire ordine a fornitore
			fs_des_prodotto[]		eventuale descrizione; nel caso in cui il cod_prodotto sia null procedo con creazione riga descrittiva.
			fd_quan_ordine[]		per ogni prodotto è specificata la quantità
			fdt_data_fabbisogno[] data di disponibilità in sede del prodotto
			fs_flag_gen_commessa[] creazione di ordine di servizio e commessa di produzione
			fs_cod_versione[]		se il flag_gen_commessa=S allora è indispensabile avre il campo versione caricato
			fs_note[]				annotazioni da aggiiungere all'ordine
	ref	fl_anno_ordine[]		anno_ordine a fornitore generato
	ref	fl_num_ordine[]		numero ordine a fornitore generato
	ref	fs_messaggio			messaggio di errore generato
				
*/
datastore lds_lista_prodotti
boolean lb_primo_ordine = false
string ls_cod_fornitore, ls_cod_tipo_ord_acq, ls_cod_tipo_det_acq, ls_sort, ls_cod_valuta, ls_cod_prodotto, &
       ls_cod_tipo_listino_prodotto, ls_cod_fornitore_old, ls_cod_deposito, ls_cod_pagamento, ls_cod_resa, &
		 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_banca, ls_cod_banca_clien_for, &
		 ls_cod_porto, ls_cod_iva, ls_cod_misura, ls_errore,ls_cod_tipo_det_acq_des,ls_messaggio, ls_des_prodotto, &
		 ls_flag_gen_commessa, ls_nota_dettaglio, ls_cod_tipo_commessa, ls_cod_versione, ls_cod_tipo_riordino, &
		 ls_cod_prodotto_raggruppato
		 
long ll_i, ll_num_prodotti, ll_riga, ll_cont, ll_num_registrazione, ll_anno_registrazione, ll_tempo_approvvigionamento, &
     ll_prog_riga_acq, ll_y, ll_ret, ll_anno_commessa, ll_num_commessa
	  
dec{4} ld_sconto_acq, ld_prezzo_acquisto, ld_prezzo_acq, ld_cambio, ld_quan_ordine, ld_fat_conversione, ld_val_riga, &
		 ld_quan_raggruppo

datetime ldt_data_consegna, ldt_data_oggi, ldt_data_validita

uo_calcola_documento_euro luo_doc
	

ll_y = 0

ll_num_prodotti = upperbound(fs_cod_prodotto)

if ll_num_prodotti < 1 or isnull(ll_num_prodotti) then
	g_mb.messagebox("Creazione Ordini Acquisto","Nessun prodotto da ordinare")
	return 0
end if

lds_lista_prodotti = Create DataStore
lds_lista_prodotti.DataObject = "d_ext_ordine_prodotti"
	
for ll_i = 1 to ll_num_prodotti
	ldt_data_oggi = datetime(today(), 00:00:00)
	ll_cont = 0
	
	// cerca il fornitore e tipo listino preferenziale per il prodotto
	select cod_fornitore, count(cod_fornitore), max(data_inizio_val), max(cod_valuta)
	into  :ls_cod_fornitore, :ll_cont, :ldt_data_validita, :ls_cod_valuta
	from  listini_fornitori
	where cod_azienda = :s_cs_xx.cod_azienda and
	      cod_prodotto = :fs_cod_prodotto[ll_i] and
			data_inizio_val <= :ldt_data_oggi and
			flag_for_pref = 'S'
	group by cod_fornitore;
	
	
	// se manda la data fabbisogno mi calcolo la possibile data consegna
	if isnull(fdt_data_fabbisogno[ll_i]) then
		select tempo_app, cod_fornitore
		into   :ll_tempo_approvvigionamento, :ls_cod_fornitore
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :fs_cod_prodotto[ll_i] ;
				 
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in ricerca anagrafica prodotti~r~n"+sqlca.sqlerrtext
			return -1
		else
			if ll_tempo_approvvigionamento > 0 then
				ldt_data_consegna = datetime(relativedate(date(ldt_data_oggi), ll_tempo_approvvigionamento), 00:00:00)
			else
				ldt_data_consegna = ldt_data_oggi
			end if
		end if
		
	else
		
		ldt_data_consegna = fdt_data_fabbisogno[ll_i]
		
	end if
	
	s_cs_xx.parametri.parametro_s_3 = ""
	
	if isnull(ls_cod_fornitore) then
		
		if g_mb.messagebox("APICE","Il prodotto " + fs_cod_prodotto[ll_i] + " non è associato ad alcun fornitore. Vuoi indicarlo adesso?",Question!,YesNo!,2) = 1 then
			
			s_cs_xx.parametri.parametro_s_1 = fs_cod_prodotto[ll_i]
			window_open(w_ext_richiesta_fornitore, 0)
			if s_cs_xx.parametri.parametro_s_3 = "" then
				fs_messaggio = "Elaborazione interrotta dall'utente"
				return -1
			end if
			ls_cod_fornitore = s_cs_xx.parametri.parametro_s_3
			
		else
			// nessuna indicazione del fornitore; salto il prodotto.
			continue
		end if
		
	end if
	
	
	select cod_tipo_listino_prodotto, 
			 cod_valuta
	into   :ls_cod_tipo_listino_prodotto, 
			 :ls_cod_valuta
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_fornitore = :ls_cod_fornitore;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in ricerca dati fornitore in anagrafica for.~r~n"+sqlca.sqlerrtext
		return -1
	end if

	f_trova_cambio_acq(ls_cod_valuta, ldt_data_consegna, ref ld_cambio)
	
	// carico prezzo di acquisto
	if f_ricerca_list_prodotto_fornitore(ls_cod_tipo_listino_prodotto, &
												 ls_cod_fornitore, &
												 ls_cod_valuta, &
												 ld_cambio, &
												 fs_cod_prodotto[ll_i], &
												 fd_quan_ordine[ll_i], &
												 ldt_data_consegna, &
												 ld_prezzo_acq, &
												 ld_sconto_acq, &
												 ls_errore) <> 0 then
			fs_messaggio = ls_errore
			return -1
	end if
											 
	ll_riga = lds_lista_prodotti.insertrow(0)
	lds_lista_prodotti.setitem(ll_riga, "cod_prodotto", fs_cod_prodotto[ll_i])
	lds_lista_prodotti.setitem(ll_riga, "des_prodotto", fs_des_prodotto[ll_i])
	lds_lista_prodotti.setitem(ll_riga, "quan_ordine", fd_quan_ordine[ll_i])
	lds_lista_prodotti.setitem(ll_riga, "note", fs_note[ll_i])
	lds_lista_prodotti.setitem(ll_riga, "data_consegna", ldt_data_consegna)
	lds_lista_prodotti.setitem(ll_riga, "cod_fornitore", ls_cod_fornitore)
	lds_lista_prodotti.setitem(ll_riga, "prezzo_acquisto", ld_prezzo_acq)
	lds_lista_prodotti.setitem(ll_riga, "sconto", ld_sconto_acq)
	lds_lista_prodotti.setitem(ll_riga, "cod_valuta", ls_cod_valuta)
	lds_lista_prodotti.setitem(ll_riga, "cambio_acq", ld_cambio)
	lds_lista_prodotti.setitem(ll_riga, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
	lds_lista_prodotti.setitem(ll_riga, "flag_gen_commessa", fs_flag_gen_commessa[ll_i])
	lds_lista_prodotti.setitem(ll_riga, "cod_versione", fs_cod_versione[ll_i])
		
next	

ls_sort = "cod_fornitore A"
lds_lista_prodotti.SetSort(ls_sort)
lds_lista_prodotti.Sort()

ll_cont = lds_lista_prodotti.rowcount()	

ll_anno_registrazione = f_anno_esercizio()
ll_num_registrazione  = 0

// cerco il tipo ordine con cui generare
select cod_tipo_ord_acq
into   :ls_cod_tipo_ord_acq
from   con_rda
where  cod_azienda = :s_cs_xx.cod_azienda   ;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Impostare tabella parametri ordini acquisto"
	return -1
end if

// cerco tipo dettaglio per ordine prodotti e servizi
select cod_tipo_det_acq, cod_tipo_det_acq_des
into   :ls_cod_tipo_det_acq, :ls_cod_tipo_det_acq_des
from   con_rda 
where  cod_azienda = :s_cs_xx.cod_azienda AND  
		 cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Impostare tipo dettaglio acquisti in  tipi ordini acquisto"
	return -1
end if

ls_cod_fornitore_old = ""

	
setnull(ls_cod_pagamento)
setnull(ls_cod_deposito)
setnull(ls_cod_valuta)
setnull(ls_cod_imballo)
setnull(ls_cod_banca)
setnull(ls_cod_vettore)
setnull(ls_cod_inoltro)
setnull(ls_cod_banca_clien_for)
setnull(ls_cod_porto)
setnull(ls_cod_resa)
setnull(ls_cod_mezzo)
setnull(ldt_data_consegna)
setnull(ls_cod_tipo_listino_prodotto)

for ll_i = 1 to ll_cont
	ls_cod_fornitore = lds_lista_prodotti.getitemstring(ll_i, "cod_fornitore")
	ls_cod_prodotto  = lds_lista_prodotti.getitemstring(ll_i, "cod_prodotto")
	ls_des_prodotto  = lds_lista_prodotti.getitemstring(ll_i, "des_prodotto")
	ls_flag_gen_commessa = lds_lista_prodotti.getitemstring(ll_i, "flag_gen_commessa")
	ld_quan_ordine   = lds_lista_prodotti.getitemnumber(ll_i, "quan_ordine")
	ldt_data_consegna = lds_lista_prodotti.getitemdatetime(ll_i, "data_consegna")
	ld_prezzo_acq = lds_lista_prodotti.getitemnumber(ll_i, "prezzo_acquisto")
	ld_sconto_acq = lds_lista_prodotti.getitemnumber(ll_i, "sconto")
	ls_cod_valuta = lds_lista_prodotti.getitemstring(ll_i, "cod_valuta")
	ld_cambio = lds_lista_prodotti.getitemnumber(ll_i, "cambio_acq")
	ls_nota_dettaglio = lds_lista_prodotti.getitemstring(ll_i, "note")
	ls_cod_tipo_listino_prodotto = lds_lista_prodotti.getitemstring(ll_i, "cod_tipo_listino_prodotto")
	ls_cod_versione = lds_lista_prodotti.getitemstring(ll_i, "cod_versione")
	
	select cod_deposito
	into   :ls_cod_deposito
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ls_cod_deposito) then
		select cod_deposito
		into   :ls_cod_deposito
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = sqlca.sqlerrtext
			return -1
		end if
	end if
	
	select cod_pagamento, 
	       cod_vettore, 
			 cod_inoltro, 
			 cod_mezzo, 
			 cod_porto, 
			 cod_resa, 
			 cod_banca_clien_for
	into   :ls_cod_pagamento, 
	       :ls_cod_vettore, 
			 :ls_cod_inoltro, 
			 :ls_cod_mezzo, 
			 :ls_cod_porto, 
			 :ls_cod_resa, 
			 :ls_cod_banca_clien_for
	from   anag_fornitori
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_fornitore = :ls_cod_fornitore;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = sqlca.sqlerrtext
		return -1
	end if
	
	if ls_cod_fornitore <> ls_cod_fornitore_old then       // ----------   insert testata ordine --------------
		
		// cerco prossimo numero ordine
		select max(num_registrazione)
		into   :ll_num_registrazione
		from   tes_ord_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione;
				 
		if ll_num_registrazione = 0 then
			ll_num_registrazione = 1
		else
			ll_num_registrazione ++
		end if
		
		// procedo con inserimento testata ordine
	
		INSERT INTO tes_ord_acq  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  cod_tipo_ord_acq,   
				  data_registrazione,   
				  cod_operatore,   
				  cod_fornitore,   
				  rag_soc_1,   
				  rag_soc_2,   
				  indirizzo,   
				  localita,   
				  frazione,   
				  cap,   
				  provincia,   
				  cod_fil_fornitore,   
				  cod_des_fornitore,   
				  cod_deposito,   
				  cod_valuta,   
				  cambio_acq,   
				  cod_tipo_listino_prodotto,   
				  cod_pagamento,   
				  sconto,   
				  cod_banca_clien_for,   
				  num_ord_fornitore,   
				  data_ord_fornitore,   
				  cod_imballo,   
				  cod_vettore,   
				  cod_inoltro,   
				  cod_mezzo,   
				  cod_porto,   
				  cod_resa,   
				  flag_blocco,   
				  flag_evasione,   
				  tot_val_ordine,   
				  tot_val_evaso,   
				  nota_testata,   
				  nota_piede,   
				  data_consegna,   
				  cod_banca,   
				  data_consegna_fornitore )  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :ll_anno_registrazione,   
				  :ll_num_registrazione,   
				  :ls_cod_tipo_ord_acq,   
				  :ldt_data_oggi,   
				  null,   
				  :ls_cod_fornitore,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  :ls_cod_deposito,   
				  :ls_cod_valuta,   
				  :ld_cambio,   
				  :ls_cod_tipo_listino_prodotto,   
				  :ls_cod_pagamento,   
				  0,   
				  null,   
				  null,   
				  null,   
				  :ls_cod_imballo,   
				  :ls_cod_vettore,   
				  :ls_cod_inoltro,   
				  :ls_cod_mezzo,   
				  :ls_cod_porto,   
				  :ls_cod_resa,   
				  'N',   
				  'A',   
				  0,   
				  0,   
				  null,   
				  null,   
				  null,   
				  null,   
				  :ldt_data_consegna )  ;
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore in inserimento testata ordine acquisto ~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
			ll_y ++
			fl_anno_ordine[ll_y] = ll_anno_registrazione
			fl_num_ordine[ll_y]  = ll_num_registrazione
			
			ll_prog_riga_acq = 0
			ls_cod_fornitore_old = ls_cod_fornitore
		end if	
                                        // -------------  insert dettaglio ordine  ---------------------
		
		if isnull(ls_cod_prodotto) then
			select cod_iva
			into   :ls_cod_iva
			from   tab_tipi_det_acq
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_det_acq = :ls_cod_tipo_det_acq_des;
			
			ld_fat_conversione = 1
			setnull(ls_cod_misura)
		else
			select cod_iva, cod_misura_acq, fat_conversione_acq  
			into   :ls_cod_iva, :ls_cod_misura, :ld_fat_conversione  
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto;
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore in inserimento dettaglio ordine acquisto ~r~n" + sqlca.sqlerrtext
				return -1
			end if
		end if
		
		ll_prog_riga_acq = ll_prog_riga_acq + 10
	  
		ld_val_riga = (ld_prezzo_acq * ld_quan_ordine)
		ld_val_riga = ld_val_riga - ((ld_val_riga / 100) * ld_sconto_acq)
		ld_val_riga = round(ld_val_riga, 0)
		
		
		/////////////////////////////////////////////////////////////  creazione della commessa !!! 
		// ATTENZIONE: 1.LA VERSIONE NON E' INDICATA NELL'ORDINE DI ACQUISTO, DA DOVE LA VADO A PRENDERE?? PER ORA HO USATO L'OGGETTO UO_DEFAULT_PRODOTTO
		//             2.NELLA COMMESSA NON è POSSIBILE INDICARE IL FORNITORE E SE SI TRATTA DI UNA COMMESSA INTERNA O ESTERNA. CHE SI TRATTA DI UNA COMMESSA
		//               INTERNA O ESTERNA LO SO DAL TIPO COMMESSA.

//		select cod_tipo_politica_riordino
//		into   :ls_cod_tipo_riordino
//		from   anag_prodotti
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//		       cod_prodotto = :ls_cod_prodotto;
//				 
//		if not isnull(ls_cod_tipo_riordino) then

		if ls_flag_gen_commessa = "S" then
			
			if isnull(ls_cod_versione) or len(ls_cod_versione) < 0 then
				// prendo la versione di default del prodotto
				uo_default_prodotto luo_default_prodotto
				luo_default_prodotto = CREATE uo_default_prodotto
				if luo_default_prodotto.uof_flag_gen_commessa(ls_cod_prodotto) = 0 then
					ls_cod_versione = luo_default_prodotto.is_cod_versione
				end if
				destroy luo_default_prodotto			
			end if
			
			ll_ret = uof_genera_commessa( ls_cod_prodotto, &
													ls_cod_versione, &
													ld_quan_ordine, &
													ldt_data_consegna, &
													true, &		
													ref ll_anno_commessa, &
													ref ll_num_commessa, &
													ref fs_messaggio)					
					
//					ll_ret = uof_genera_commessa( ls_cod_prodotto, &
//															ls_cod_versione, &
//															ld_quan_ordine, &
//															ldt_data_consegna, &
//															false, &
//															ref ll_anno_commessa, &
//															ref ll_num_commessa, &
//															ref fs_messaggio)							
//			end choose
			
			if ll_ret < 0 then
				rollback;
				return -1
			end if
			
		end if


		// procedo con la creazione della riga d'ordine.	  
		INSERT INTO det_ord_acq  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  prog_riga_ordine_acq,   
		  cod_tipo_det_acq,   
		  cod_prodotto,   
		  cod_misura,   
		  des_prodotto,   
		  quan_ordinata,   
		  prezzo_acquisto,   
		  fat_conversione,   
		  sconto_1,   
		  sconto_2,   
		  sconto_3,   
		  cod_iva,   
		  data_consegna,   
		  cod_prod_fornitore,   
		  quan_arrivata,   
		  val_riga,   
		  flag_saldo,   
		  flag_blocco,   
		  nota_dettaglio,   
		  anno_commessa,   
		  num_commessa,   
		  cod_centro_costo,   
		  sconto_4,   
		  sconto_5,   
		  sconto_6,   
		  sconto_7,   
		  sconto_8,   
		  sconto_9,   
		  sconto_10,   
		  anno_off_acq,   
		  num_off_acq,   
		  prog_riga_off_acq,   
		  data_consegna_fornitore )  
	  VALUES ( :s_cs_xx.cod_azienda,   
		  :ll_anno_registrazione,   
		  :ll_num_registrazione,   
		  :ll_prog_riga_acq,   
		  :ls_cod_tipo_det_acq,   
		  :ls_cod_prodotto,   
		  :ls_cod_misura,   
		  :ls_des_prodotto,   
		  :ld_quan_ordine,   
		  :ld_prezzo_acq,   
		  :ld_fat_conversione,   
		  :ld_sconto_acq,   
		  0,   
		  0,   
		  :ls_cod_iva,   
		  :ldt_data_consegna,   
		  null,   
		  0,   
		  :ld_val_riga,   
		  'N',   
		  'N',   
		  null,   
		  :ll_anno_commessa,   
		  :ll_num_commessa,   
		  null,   
		  0,   
		  0,   
		  0,   
		  0,   
		  0,   
		  0,   
		  0,   
		  null,   
		  null,   
		  null,   
		  :ldt_data_consegna )  ;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in inserimento dettaglio ordine acquisto ~r~n" + sqlca.sqlerrtext
			rollback;
		end if
		
		// aggiorno quantità ordine a fornitore in anagrafica prodotti.
		update anag_prodotti
		set    quan_ordinata = quan_ordinata + :ld_quan_ordine
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in inserimento dettaglio ordine acquisto ~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		// enme 08/1/2006 gestione prodotto raggruppato
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then
			
			ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ordine, "M")
			
			update anag_prodotti  
				set quan_ordinata = quan_ordinata + :ld_quan_raggruppo
			 where cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto_raggruppato;

			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento ordinato magazzino del prodotto "+ls_cod_prodotto+". Dettaglio " + sqlca.sqlerrtext)
				return 1
			end if
		end if

next

update con_ord_acq	
set    num_registrazione = :ll_num_registrazione
where  cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in aggiornamento con_ord_acq ~r~n" + sqlca.sqlerrtext
	return -1
end if


for ll_i = 1 to upperbound(fl_anno_ordine)
	
	if not isnull(fl_anno_ordine[ll_i]) and fl_anno_ordine[ll_i] <> 0 and not isnull(fl_num_ordine[ll_i]) and fl_num_ordine[ll_i] <> 0 then
	
		luo_doc = create uo_calcola_documento_euro
	
		if luo_doc.uof_calcola_documento(fl_anno_ordine[ll_i],fl_num_ordine[ll_i],"ord_acq",ls_messaggio) <> 0 then
			g_mb.messagebox("Apice",ls_messaggio)
			destroy luo_doc
			return -1
		end if
	
		destroy luo_doc
	
	end if	
	
	next
return 0

end function

public function integer uof_crea_aggiungi_bolla_lavorazione (long fl_anno_reg_ord_acq, long fl_num_reg_ord_acq, datawindow fdw_righe_ordine, ref long fl_anno_reg_bol_ven, ref long fl_num_reg_bol_ven, ref string fs_errore);/*			funzione di generazione DDT lavorazione da un ordine di acquisto.
			EnMe 22/06/2010 per Progettotenda Specifica richieste_varie_2010
			
			Questa funzione invia automaticamente il grezzo (cioè il prodotto alternativo dell'anagrafica prodotti) 
			associato al prodotto che è nell'ordine del fornitore.
*/

boolean lb_riferimento, lb_aggiungi=false, lb_richiedi_grezzo
long	 ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_acq, ll_anno_commessa, ll_num_commessa, &
    		ll_progr_stock[], ll_prog_riga_bol_ven, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga, &
	  	ll_num_confezioni, ll_num_pezzi_confezione, ll_rows, ll_i, ll_y
string ls_cod_tipo_ord_ven, ls_nota_testata, ls_cod_cliente[], ls_cod_operatore, ls_rag_soc_1, ls_cod_prodotto_precedente, &
		 ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_des_fornitore, ls_cod_ubicazione[], &
		 ls_cod_deposito[], ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, ls_cod_imballo, &
		 ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_nota_piede, ls_cod_banca, ls_cod_tipo_det_ven, &
		 ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_ven, &
		 ls_cod_tipo_analisi_bol, ls_cod_tipo_analisi_ord, ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, ls_causale_trasporto, &
		 ls_flag_riep_fat, ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ls_num_ord_fornitore, &
		 ls_cod_lotto[], ls_cod_tipo_movimento, ls_vostro_ordine, ls_vostro_ordine_data, ls_flag_evasione, ls_lire, &
		 ls_flag_sola_iva, ls_cod_fornitore[], ls_causale_trasporto_tab, ls_cod_versione, ls_cod_causale_tab, ls_flag_abilita_rif_ord_ven, &
		 ls_cod_tipo_det_ven_rif_ord_ven, ls_des_riferimento_ord_ven, ls_flag_rif_anno_ord_ven, ls_flag_rif_num_ord_ven,&
		 ls_flag_rif_data_ord_ven, ls_des_riferimento, ls_cod_iva_rif, ls_cod_causale, ls_note_dettaglio_rif, &
		 ls_db,ls_cod_tipo_bol_ven_dest, ls_nota_piede_bolla, ls_messaggio, ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det, &
		 ls_nota_prodotto, ls_flag_trasporta_nota_ddt, ls_rif_packing_list, ls_cod_tipo_det_ven_ord, ls_cod_iva_prodotto, &
		 ls_cod_tipo_movimento_ord, ls_cod_tipo_movimento_new, ls_cod_fornitore_testata, ls_cod_prodotto_grezzo,ls_flag_evasione_totale
datetime ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_fornitore, ldt_data_stock[], ldt_vostro_ordine_data, ldt_ora_inizio_trasporto
dec{6} ld_cambio_ven, ld_prezzo_vendita, ld_fat_conversione_ven, ld_sconto_1, ld_sconto_2, &
       ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		 ld_sconto_testata, ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, ld_quan_ordine, &
		 ld_quan_evasa, ld_tot_val_evaso, ld_tot_val_ordine, ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
		 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, ld_val_riga_sconto_4, &
		 ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
		 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10, &
		 ld_val_sconto_testata, ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quan_in_evasione, &
		 ld_quantita_um, ld_prezzo_um
integer li_risposta
s_gen_bolla_uscita_lavorazione_richiesta lstr_grezzo
uo_calcola_documento_euro luo_calcolo

setnull(ls_cod_operatore)
setnull(ls_cod_ubicazione[1])
setnull(ls_cod_ubicazione[2])
setnull(ls_cod_valuta)
ld_cambio_ven = 1
setnull(ls_cod_tipo_listino_prodotto)
setnull(ls_cod_pagamento)
ld_sconto_testata = 0
setnull(ls_cod_agente_1)
setnull(ls_cod_agente_2)
setnull(ls_cod_banca) 
setnull(ls_cod_imballo)  
setnull(ls_cod_vettore)   
setnull(ls_cod_inoltro)   
setnull(ls_cod_mezzo)
setnull(ls_causale_trasporto)
setnull(ls_cod_porto)
setnull(ls_cod_resa)
setnull(ls_cod_banca_clien_for)
setnull(ls_cod_causale)
ls_flag_riep_fat ="S"

ls_db = f_db()
if upper(ls_db) = "MSSQL" then ls_db="SYBASE_ASE"

ll_anno_registrazione = f_anno_esercizio()

if not isnull(fl_anno_reg_bol_ven) and fl_anno_reg_bol_ven > 0 then lb_aggiungi = true
if lb_aggiungi and (fl_num_reg_bol_ven = 0 or isnull(fl_num_reg_bol_ven)) then
	fs_errore = "Manca l'indicazione del numero di bolla di destinazione"
	return -1
end if

lb_riferimento = true
	

// se manca il tipo bolla segnalo errore.
if isnull(is_cod_tipo_bol_ven) or len(is_cod_tipo_bol_ven) < 1 then
	
	fs_errore = "Manca l'indicazione del tipo DDT da generare."
	return -1
	
end if
// ----

if lb_aggiungi then
	
	select cod_tipo_bol_ven
	into   :ls_cod_tipo_bol_ven_dest
	from   tes_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :fl_anno_reg_bol_ven and
			 num_registrazione = :fl_num_reg_bol_ven;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in ricerca tipo bolla di vendita in bolla di destinazione. Dettaglio: " + sqlca.sqlerrtext
		return -1
	end if
	
	if is_cod_tipo_bol_ven <> ls_cod_tipo_bol_ven_dest then
		fs_errore = "Il tipo bolla deve coincidere con il tipo bolla di destinazione indicato nel tipo ordine: controllare il tipo bolla di destinazione nella tabella tipi ordini"
		return -1
	end if
	
end if

if not isnull(is_cod_tipo_bol_ven) then
	
	select cod_fornitore, 
			 cod_deposito,
			 cod_pagamento,
			 cod_valuta,
			 data_registrazione
	into   :ls_cod_fornitore_testata ,
			 :ls_cod_deposito[1],
			 :ls_cod_pagamento,
			 :ls_cod_valuta,
			 :ldt_data_ordine
	from   tes_ord_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :fl_anno_reg_ord_acq and
			 num_registrazione = :fl_num_reg_ord_acq;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Si è verificato un errore nella ricerca ordine di acquisto." + sqlca.sqlerrtext
		return -1
	end if

	select cod_tipo_det_ven,
			 cod_tipo_analisi,
			 causale_trasporto,
			 cod_causale,
			 flag_abilita_rif_ord_ven,
			 cod_tipo_det_ven_rif_ord_ven,
			 des_riferimento_ord_ven,
			 flag_rif_anno_ord_ven,
			 flag_rif_num_ord_ven,
			 flag_rif_data_ord_ven
	into	 :ls_cod_tipo_det_ven,
			 :ls_cod_tipo_analisi_bol,
			 :ls_causale_trasporto_tab,
			 :ls_cod_causale_tab,
			 :ls_flag_abilita_rif_ord_ven,
			 :ls_cod_tipo_det_ven_rif_ord_ven,
			 :ls_des_riferimento_ord_ven,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_bol_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_bol_ven = :is_cod_tipo_bol_ven;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Si è verificato un errore nella lettura della Tabella Tipi Bolle." + sqlca.sqlerrtext
		return -1
	end if

	ldt_data_registrazione = datetime(today())
	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if


//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_ord_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_acq = :is_cod_tipo_ord_acq;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_ord_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_acq = :is_cod_tipo_ord_acq;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------


	if ls_cod_tipo_det_ven_rif_ord_ven = "T" then
		ls_nota_testata = ls_des_riferimento_ord_ven + string(fl_anno_reg_ord_acq) + "/" + &
								string(fl_num_reg_ord_acq) + " del " + string(ldt_data_ordine, "dd/mm/yyyy")+ &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
	end if
	
	// ----- controllo lunghezza note per ASE
	if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE
	
	ls_causale_trasporto = ls_causale_trasporto_tab

	select stringa
	into   :ls_lire
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and &
			 flag_parametro = 'S' and &
			 cod_parametro = 'LIR';

	if sqlca.sqlcode <> 0 then
		fs_errore = "Configurare il codice valuta per le Lire Italiane.~r~n" + sqlca.sqlerrtext
		return -1
	end if

	select sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if
	
	if lb_aggiungi then
		
		// aggiungo ad un DDT esistente
		
		ldt_ora_inizio_trasporto = datetime(date("01/01/1900"), it_ora_inizio_trasporto)
		ll_num_registrazione = fl_num_reg_bol_ven
		
		select nota_piede
		into   :ls_nota_piede_bolla
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_reg_bol_ven and
		       num_registrazione = :fl_num_reg_bol_ven;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante ricercaTestata Bolle Vendita. Dettaglio errore " + sqlca.sqlerrtext
			return -1
		end if

		if isnull(ls_nota_piede_bolla) then ls_nota_piede_bolla = ""
		if len(trim(ls_nota_piede_bolla)) > 0 then
			ls_nota_piede_bolla = ls_nota_piede_bolla
		else
			ls_nota_piede_bolla = ls_nota_piede_bolla
		end if
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_piede_bolla) > 255 then
			ls_nota_piede_bolla = left(ls_nota_piede_bolla, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		
		update tes_bol_ven
		set    num_colli  = num_colli + :id_num_colli,
		       peso_lordo = peso_lordo + :id_peso_lordo,
				 peso_netto = peso_netto + :id_peso_netto,
				 rag_soc_1  = :is_rag_soc_1,
				 rag_soc_2  = :is_rag_soc_2,
				 indirizzo  = :is_indirizzo,
				 localita   = :is_localita,
				 cap = :is_cap,
				 provincia = :is_provincia,
				 frazione  = :is_frazione,
				 data_inizio_trasporto = :idt_data_inizio_trasporto,
				 ora_inizio_trasporto  = :ldt_ora_inizio_trasporto,
				 cod_des_cliente = :is_destinazione,
				 cod_causale  = :is_cod_causale,
				 aspetto_beni = :is_aspetto_beni,
				 cod_porto = :is_cod_porto,
				 cod_mezzo = :is_cod_mezzo,
				 cod_vettore = :is_cod_vettore,
				 cod_inoltro = :is_cod_inoltro,
				 cod_imballo = :is_cod_imballo,
				 nota_piede = :ls_nota_piede_bolla
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_reg_bol_ven and
				 num_registrazione = :fl_num_reg_bol_ven;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante aggiornamento Testata Bolle Vendita. Dettaglio errore " + sqlca.sqlerrtext
			return -1
		end if
		
	else
		
		// sono in fase di creazione di un nuovo DDT
		
		select max(num_registrazione)
		into   :ll_num_registrazione
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la ricerca massimo numero registrazione per incremento numeratore interno bolle.~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		if isnull(ll_num_registrazione) then ll_num_registrazione = 0
		ll_num_registrazione ++
		
		update con_bol_ven
		set    num_registrazione = :ll_num_registrazione
		where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura sulla tabella Controllo Bolle Vendita.~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		if not isnull(is_cod_causale) then ls_cod_causale = is_cod_causale
		if not isnull(is_aspetto_beni) then ls_aspetto_beni = is_aspetto_beni
		if not isnull(is_cod_porto) then ls_cod_porto = is_cod_porto
		if not isnull(is_cod_mezzo) then ls_cod_mezzo = is_cod_mezzo
		if not isnull(is_cod_vettore) then ls_cod_vettore = is_cod_vettore
		if not isnull(is_cod_inoltro) then ls_cod_inoltro = is_cod_inoltro
		if not isnull(is_cod_resa) then ls_cod_resa = is_cod_resa
		if not isnull(is_destinazione) then
			ls_cod_des_fornitore = is_destinazione
			ls_rag_soc_1 = is_rag_soc_1
			ls_rag_soc_2 = is_rag_soc_2
			ls_indirizzo = is_indirizzo
			ls_localita = is_localita
			ls_frazione = is_frazione
			ls_cap = is_cap
			ls_provincia = is_provincia
		end if
		if not isnull(id_num_colli) and id_num_colli <> 0 then ld_num_colli = id_num_colli
		if not isnull(id_peso_lordo) and id_peso_lordo <> 0 then ld_peso_lordo = id_peso_lordo
		if not isnull(id_peso_netto) and id_peso_netto <> 0 then ld_peso_netto = id_peso_netto
		ldt_ora_inizio_trasporto = datetime(date("01/01/1900"), it_ora_inizio_trasporto)
		if isnull(ls_num_ord_fornitore) or len(ls_num_ord_fornitore) < 1 then
			ls_num_ord_fornitore = is_num_ord_cliente
		end if
		if isnull(ldt_data_ord_fornitore) then
			ldt_data_ord_fornitore = idt_data_ord_cliente
		end if
		if isnull(ls_nota_piede) then ls_nota_piede = ""
		if len(trim(ls_nota_piede)) > 0 then
			//ls_nota_piede = ls_nota_piede + "~r~n" + is_nota_piede
			ls_nota_piede = ls_nota_piede
		else
			//ls_nota_piede = ls_nota_piede + is_nota_piede
			ls_nota_piede = ls_nota_piede
		end if
		// ----- controllo lunghezza note per ASE
		if ls_db = "SYBASE_ASE" and len(ls_nota_piede) > 255 then
			ls_nota_piede = left(ls_nota_piede, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		
		
		insert into tes_bol_ven
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 data_registrazione,   
						 cod_operatore,   
						 cod_tipo_bol_ven,   
						 cod_cliente,
						 cod_fornitore,
						 cod_des_cliente,   
						 cod_fil_fornitore,
						 rag_soc_1,   
						 rag_soc_2,   
						 indirizzo,   
						 localita,   
						 frazione,   
						 cap,   
						 provincia,   
						 cod_deposito,   
						 cod_deposito_tras,
						 cod_ubicazione,
						 cod_valuta,   
						 cambio_ven,   
						 cod_tipo_listino_prodotto,   
						 cod_pagamento,   
						 sconto,   
						 cod_agente_1,
						 cod_agente_2,
						 cod_banca,   
						 num_ord_cliente,   
						 data_ord_cliente,   
						 cod_imballo,   
						 aspetto_beni,
						 peso_netto,
						 peso_lordo,
						 num_colli,
						 cod_vettore,   
						 cod_inoltro,   
						 cod_mezzo,
						 causale_trasporto,
						 cod_porto,   
						 cod_resa,   
						 cod_documento,
						 numeratore_documento,
						 anno_documento,
						 num_documento,
						 data_bolla,
						 nota_testata,   
						 nota_piede,   
						 flag_fuori_fido,
						 flag_blocco,   
						 flag_movimenti,  
						 flag_riep_fat,
						 cod_banca_clien_for,
						 flag_gen_fat,
						 cod_causale,
						 data_inizio_trasporto,
						 ora_inizio_trasporto,
						 flag_doc_suc_tes,
						 flag_st_note_tes,
						 flag_doc_suc_pie,
						 flag_st_note_pie)  						 
		values      (:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ldt_data_registrazione,   
						 :ls_cod_operatore,   
						 :is_cod_tipo_bol_ven,   
						 null,
						 :ls_cod_fornitore_testata,
						 :ls_cod_des_fornitore,   
						 null,
						 :ls_rag_soc_1,   
						 :ls_rag_soc_2,   
						 :ls_indirizzo,   
						 :ls_localita,   
						 :ls_frazione,   
						 :ls_cap,   
						 :ls_provincia,   
						 :ls_cod_deposito[1],   
						 :is_cod_deposito,
						 :ls_cod_ubicazione[1],
						 :ls_cod_valuta,   
						 :ld_cambio_ven,   
						 :ls_cod_tipo_listino_prodotto,   
						 :ls_cod_pagamento,   
						 :ld_sconto_testata,
						 :ls_cod_agente_1,
						 :ls_cod_agente_2,
						 :ls_cod_banca,   
						 :ls_num_ord_fornitore,   
						 :ldt_data_ord_fornitore,   
						 :ls_cod_imballo,   
						 :ls_aspetto_beni,
						 :ld_peso_netto,
						 :ld_peso_lordo,
						 :ld_num_colli,
						 :ls_cod_vettore,   
						 :ls_cod_inoltro,   
						 :ls_cod_mezzo,   
						 :ls_causale_trasporto,
						 :ls_cod_porto,   
						 :ls_cod_resa,   
						 null,
						 null,
						 0,
						 0,
						 null,
						 :ls_nota_testata,   
						 :ls_nota_piede,   
						 'N',
						 'N',   
						 'N',
						 :ls_flag_riep_fat,
						 :ls_cod_banca_clien_for,
						 'N',
						 :ls_cod_causale,
						 :idt_data_inizio_trasporto,
						 :ldt_ora_inizio_trasporto,
						 'I',
						 'I',
						 'I',
						 'I');						 
		 if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante inserimento Testata Bolle Vendita. Dettaglio errore " + sqlca.sqlerrtext
			return -1
		 end if
		 
		if uof_crea_nota_fissa("BOL_VEN", ll_anno_registrazione, ll_num_registrazione, fs_errore) = -1 then return -1
		 
		
	end if
	 
	if lb_aggiungi then
		select max(prog_riga_bol_ven)
		into   :ll_prog_riga_bol_ven
		from   det_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
	else
		ll_prog_riga_bol_ven = 0	
	end if
	 
	ll_rows = fdw_righe_ordine.rowcount()
	
	fdw_righe_ordine.setsort("cod_prodotto")
	fdw_righe_ordine.sort()
	
	
	// Verifico se per ogni prodotto indicato nell'ordine esiste un prodotto alternativo.
	// Per quelli che non esiste, chiedo.
	
	ls_cod_prodotto_precedente = "<>"
	lb_richiedi_grezzo = false
	

	for ll_i = 1 to ll_rows
		
		setnull( lstr_grezzo.cod_prodotto[ll_i] )
		setnull( lstr_grezzo.cod_grezzo[ll_i] )
		setnull( lstr_grezzo.prog_riga_ord_acq[ll_i] )
		
		lstr_grezzo.cod_prodotto[ll_i]      = fdw_righe_ordine.getitemstring(ll_i, "cod_prodotto")
		lstr_grezzo.prog_riga_ord_acq[ll_i] = fdw_righe_ordine.getitemnumber(ll_i, "prog_riga_ordine_acq")
	
		// vado in cerca del prodotto grezzo
		select cod_prodotto_alt
		into   :ls_cod_prodotto_grezzo
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :lstr_grezzo.cod_prodotto[ll_i];
				 		
		if NOT isnull(ls_cod_prodotto_grezzo) then
			lstr_grezzo.cod_grezzo[ll_i] = ls_cod_prodotto_grezzo
		else
			lb_richiedi_grezzo = true
		end if
	
	next
	
	// Se ho verificiato che in qualche caso il grezzo è mancante, vado a chiedere conferma all'utente
	if lb_richiedi_grezzo then
		
		openwithparm(w_gen_bolla_uscita_lavorazione_richiesta, lstr_grezzo)
		
		lstr_grezzo = message.powerobjectparm
		
		for ll_i = 1 to ll_rows
		
			fdw_righe_ordine.setitem(ll_i, "cod_prodotto_alternativo", lstr_grezzo.cod_grezzo[ll_i])
		
		next	
			
	end if
	
	
	// associo il prodotto alternativo ad ogni riga ordine
	
	for ll_i = 1 to ll_rows
		
		for ll_y = 1 to upperbound(lstr_grezzo.cod_grezzo[])
			
			 if lstr_grezzo.prog_riga_ord_acq[ll_y] = fdw_righe_ordine.getitemnumber(ll_i, "prog_riga_ordine_acq") then
				fdw_righe_ordine.setitem(ll_i, "cod_prodotto_alternativo", lstr_grezzo.cod_grezzo[ll_y])
				exit
			end if
			
		next
		
	next
	
	// procedo con l'effettiva creazione delle righe bolla.
	
	ll_i = fdw_righe_ordine.setsort("cod_prodotto_alternativo")
	ll_i = fdw_righe_ordine.sort()

	ls_cod_prodotto_precedente = "<>"
	setnull(ls_cod_prodotto_grezzo)
	
	for ll_i = 1 to ll_rows

		ld_quan_in_evasione	= fdw_righe_ordine.getitemnumber(ll_i, "quan_da_evadere")
		ls_flag_evasione_totale = fdw_righe_ordine.getitemstring(ll_i, "flag_evasione_totale")
		
		if ld_quan_in_evasione <= 0 then continue
		
		ls_cod_prodotto        = fdw_righe_ordine.getitemstring(ll_i, "cod_prodotto")
		ls_cod_prodotto_grezzo = fdw_righe_ordine.getitemstring(ll_i, "cod_prodotto_alternativo")
		ll_prog_riga_ord_acq   = fdw_righe_ordine.getitemnumber(ll_i, "prog_riga_ordine_acq")

		if ls_cod_prodotto_grezzo = ls_cod_prodotto_precedente then
			// se è lo stesso prodotto continuo ad aggiungere sullo stesso ordine
			update det_bol_ven 
			set    quan_consegnata   = quan_consegnata + :ld_quan_in_evasione
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_registrazione and
					 num_registrazione  = :ll_num_registrazione and
					 prog_riga_bol_ven  = :ll_prog_riga_bol_ven;
			if sqlca.sqlcode = -1 then
				fs_errore = "Si è verificato un errore in fase incremento quantità sulla riga bolla.~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
			// aggiorno il riferimento alla bolla
			update det_ord_acq  
				set anno_reg_bol_ven  = :ll_anno_registrazione,
					 num_reg_bol_ven   = :ll_num_registrazione,
					 prog_riga_bol_ven = :ll_prog_riga_bol_ven
			 where cod_azienda          = :s_cs_xx.cod_azienda and  
					 anno_registrazione   = :fl_anno_reg_ord_acq and  
					 num_registrazione    = :fl_num_reg_ord_acq   and
					 prog_riga_ordine_acq = :ll_prog_riga_ord_acq;
		
			if sqlca.sqlcode = -1 then
				fs_errore = "Si è verificato un errore in fase di inserimento della referenza della bolla alla riga ordine.~r~n" + sqlca.sqlerrtext
				return -1
			end if
			
			// passo alla riga successiva
			continue
			
		end if
			
		ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
	
		// niente cod prodotto, salto
		if isnull(ls_cod_prodotto) then continue
				
		if NOT isnull(ls_cod_prodotto_grezzo) then
			ls_cod_prodotto = ls_cod_prodotto_grezzo
		end if
		
		ls_des_prodotto = fdw_righe_ordine.getitemstring(ll_i, "des_prodotto")
		
		select des_prodotto, 
				 cod_misura_mag,
				 cod_iva
		into   :ls_des_prodotto, 
				 :ls_cod_misura, 
				 :ls_cod_iva_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		// stefanop 29/10/2012: Fix
		// Prevenzione su codici prodotti NON codificati all'interno dell'anagrafica.
		// Può capitare che siano inseriti codici di prodotti alternativi NON validi.
		if sqlca.sqlcode = 100 then
			fs_errore = g_str.format("Il prodotto $1 (o prodotto alternativo) riferito alla riga ordine $3/$4/$2 non è stato trovato in anagrafica prodotti.", ls_cod_prodotto, ll_prog_riga_ord_acq, fl_anno_reg_ord_acq, fl_num_reg_ord_acq)
			return -1
		elseif sqlca.sqlcode < 0 then
			fs_errore = g_str.format("Errore in lettura dettagli del prodotto $1.~r~nErrore: $2", ls_cod_prodotto, sqlca.sqlerrtext)
			return -1
		end if
		// -----
		
		ld_quan_ordine = fdw_righe_ordine.getitemnumber(ll_i, "quan_da_evadere")
		ld_prezzo_vendita = 0
		ld_fat_conversione_ven = 1
		ld_sconto_1 = 0
		ld_sconto_2 = 0
		ld_sconto_3 = 0
		ld_sconto_4 = 0 
		ld_sconto_5 = 0 
		ld_sconto_6 = 0 
		ld_sconto_7 = 0 
		ld_sconto_8 = 0 
		ld_sconto_9 = 0 
		ld_sconto_10 = 0
		ld_provvigione_1 = 0
		ld_provvigione_2 = 0
		ld_quan_evasa = 0
		setnull(ll_anno_commessa)
		setnull(ll_num_commessa)
		setnull(ls_cod_centro_costo)
		setnull(ls_cod_versione)
		ll_num_confezioni = 0
		ll_num_pezzi_confezione = 0
		ls_flag_doc_suc_det = "I"
		ld_quantita_um = 0
		ld_prezzo_um = 0
		setnull(ls_nota_prodotto)
		ld_quantita_um = 0
	
		if ls_flag_doc_suc_det = 'N' then
			setnull(ls_nota_dettaglio)
		end if				
	
		select flag_tipo_det_ven,
				 cod_tipo_movimento,
				 flag_sola_iva,
				 cod_iva
		into   :ls_flag_tipo_det_ven,
				 :ls_cod_tipo_movimento,
				 :ls_flag_sola_iva,
				 :ls_cod_iva
		from   tab_tipi_det_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
		if sqlca.sqlcode = -1 then
			fs_errore = "Si è verificato un errore in fase di lettura tipi dettagli.~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		if not isnull(ls_cod_iva_prodotto) then
			ls_cod_iva = ls_cod_iva_prodotto
		end if
		
	
		if ls_flag_tipo_det_ven = "M" then
			// ********************sistemare i movimenti di magazzino**************************
			
			ls_cod_deposito[2] = is_cod_deposito
			ls_cod_fornitore[2]= ls_cod_fornitore_testata
			
			setnull(ls_cod_lotto[1])
			setnull(ldt_data_stock[1])
			setnull(ll_progr_stock[1])
			setnull(ls_cod_cliente[1])
			setnull(ls_cod_fornitore[1])
			setnull(ls_cod_lotto[2])
			setnull(ldt_data_stock[2])
			setnull(ll_progr_stock[2])
			setnull(ls_cod_cliente[2])
			setnull(ls_cod_fornitore[2])
			
			if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
				fs_errore = "Si è verificato un errore in fase di creazione destinazioni movimenti."
				return -1
			end if
	
			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
				return -1
			end if
		end if
	
		if lb_riferimento and ls_flag_abilita_rif_ord_ven = "D" then
			
			lb_riferimento = false
			setnull(ls_note_dettaglio_rif)
			
			ls_des_riferimento = ls_des_riferimento_ord_ven
			if ls_flag_rif_anno_ord_ven = "S" and not isnull(fl_anno_reg_ord_acq) then
				ls_des_riferimento = ls_des_riferimento + "-" +string(fl_anno_reg_ord_acq)
			end if
			if ls_flag_rif_num_ord_ven = "S" and not isnull(fl_num_reg_ord_acq) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(fl_num_reg_ord_acq)
			end if
			if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
			end if
			
			if not isnull(is_rif_packing_list) and len(is_rif_packing_list) > 0 then
				if not isnull(ls_note_dettaglio_rif) and len(ls_note_dettaglio_rif) > 0 then 
					ls_note_dettaglio_rif += "~r~n"
				else
					ls_note_dettaglio_rif = ""
				end if
				ls_note_dettaglio_rif = ls_note_dettaglio_rif 
				setnull(is_rif_packing_list)
			end if
			
			if not isnull(ls_num_ord_fornitore) then
				if not isnull(ls_note_dettaglio_rif) and len(ls_note_dettaglio_rif) > 0 then 
					ls_note_dettaglio_rif += "~r~n"
				else
					ls_note_dettaglio_rif = ""
				end if
				ls_note_dettaglio_rif = ls_note_dettaglio_rif + "Vs.Ordine " + ls_num_ord_fornitore
				if not isnull(ldt_data_ord_fornitore) then
					ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_fornitore,"dd/mm/yyyy")
				end if
			end if
			
			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_ord_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
				g_mb.messagebox("Generazione Bolla Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if
			
			insert into det_bol_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_bol_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 progr_stock,
							 data_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_consegnata,   
							 prezzo_vendita,   
							 fat_conversione_ven,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 nota_dettaglio,   
							 anno_registrazione_ord_ven,   
							 num_registrazione_ord_ven,   
							 prog_riga_ord_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 anno_reg_bol_acq,
							 num_reg_bol_acq,
							 prog_riga_bol_acq,
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_doc_suc_det,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)							 
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_bol_ven,   
							 :ls_cod_tipo_det_ven_rif_ord_ven,   
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,   
							 null,   
							 :ls_des_riferimento,   
							 0,   
							 0,   
							 1,   
							 0,   
							 0,  
							 0,
							 0,
							 :ls_cod_iva_rif,   
							 null,
							 null,
							 :ls_note_dettaglio_rif,   
							 null,   
							 null,   
							 null,
							 null,   
							 null,   
							 null,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 0,
							 0,
							 'I',
							 'I',
							 0,
							 0);							 
	
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la scrittura Dettagli Bolle Vendita.~r~n" + sqlca.sqlerrtext
				return -1
			end if
			ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		end if	
		

		insert into det_bol_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_bol_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 progr_stock,
						 data_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_consegnata,   
						 prezzo_vendita,   
						 fat_conversione_ven,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 nota_dettaglio,   
						 anno_registrazione_ord_ven,   
						 num_registrazione_ord_ven,   
						 prog_riga_ord_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 anno_reg_bol_acq,
						 num_reg_bol_acq,
						 prog_riga_bol_acq,
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 cod_versione,
						 num_confezioni,
						 num_pezzi_confezione,
						 flag_doc_suc_det,
						 flag_st_note_det,
						 quantita_um,
						 prezzo_um)
	  values			(:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_bol_ven,   
						 :ls_cod_tipo_det_ven,   
						 :ls_cod_deposito[1],
						 :ls_cod_ubicazione[1],
						 :ls_cod_lotto[1],
						 :ll_progr_stock[1],
						 :ldt_data_stock[1],
						 :ls_cod_misura,   
						 :ls_cod_prodotto,   
						 :ls_des_prodotto,   
						 :ld_quan_in_evasione,   
						 :ld_prezzo_vendita,   
						 :ld_fat_conversione_ven,   
						 :ld_sconto_1,   
						 :ld_sconto_2,  
						 :ld_provvigione_1,
						 :ld_provvigione_2,
						 :ls_cod_iva,   
						 :ls_cod_tipo_movimento,
						 null,
						 :ls_nota_dettaglio,   
						 null,   
						 null,   
						 null,
						 null,   
						 null,   
						 :ls_cod_centro_costo,   
						 :ld_sconto_3,   
						 :ld_sconto_4,   
						 :ld_sconto_5,   
						 :ld_sconto_6,   
						 :ld_sconto_7,   
						 :ld_sconto_8,   
						 :ld_sconto_9,   
						 :ld_sconto_10,
						 null,
						 null,
						 null,
						 null,
						 :ll_anno_reg_des_mov,
						 :ll_num_reg_des_mov,
						 :ls_cod_versione,
						 :ll_num_confezioni,
						 :ll_num_pezzi_confezione,
						 'I',
						 'I',
						 :ld_quantita_um,
						 :ld_prezzo_um);								 
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura Dettagli Bolle Vendita.~r~n" + sqlca.sqlerrtext
			return -1
		end if
		

//	************ SQL per inserimento riferimento della riga bolla nell'ordine di acquisto ******

		update det_ord_acq  
			set anno_reg_bol_ven  = :ll_anno_registrazione,
			    num_reg_bol_ven   = :ll_num_registrazione,
				 prog_riga_bol_ven = :ll_prog_riga_bol_ven
		 where cod_azienda          = :s_cs_xx.cod_azienda and  
				 anno_registrazione   = :fl_anno_reg_ord_acq and  
				 num_registrazione    = :fl_num_reg_ord_acq   and
				 prog_riga_ordine_acq = :ll_prog_riga_ord_acq;
	
		if sqlca.sqlcode = -1 then
			fs_errore = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Acquisto.~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		ls_cod_prodotto_precedente = ls_cod_prodotto_grezzo

	next
	

	luo_calcolo = create uo_calcola_documento_euro

	if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_ven",fs_errore) <> 0 then
		destroy luo_calcolo
		return -1
	end if
	
	destroy luo_calcolo

	fl_anno_reg_bol_ven = ll_anno_registrazione
	fl_num_reg_bol_ven = ll_num_registrazione
end if
return 0

end function

public function integer uof_genera_rda (string fs_cod_prodotto, string fs_des_prodotto, string fs_cod_versione, string fs_cod_fornitore, decimal fd_quan_richiesta, decimal fd_prezzo_richiesta, long fl_anno_commessa, long fl_num_commessa, datetime fdt_data_disponibilita, string fs_cod_tipo_politica_riordino, string fs_flag_gen_commessa, string fs_motivazione, string fs_des_specifica, long fl_anno_reg_ord_ven, long fl_num_reg_ord_ven, long fl_prog_riga_ord_ven, ref long fl_anno_registrazione, ref long fl_num_registrazione, ref string fs_errore);datetime ldt_data_richiesta,ldt_data_inizio_val_for

long     ll_prog_riga_rda

double 	ld_cambio_acq

string   ls_des_prodotto, ls_cod_operaio,ls_cod_valuta, ls_cod_fornitore_prefer,ls_cod_tipo_listino_prodotto, ls_cod_misura_ven, ls_des_prodotto_anag, ls_flag_copia_de_prod_ord_ven

dec{4}		ld_prezzo_rda, ld_prezzo_acquisto, &
			ld_quantita_1,ld_quantita_2,ld_quantita_3,ld_quantita_4,ld_quantita_5, &
 			ld_sconto_1,ld_sconto_2,ld_sconto_3,ld_sconto_4,ld_sconto_5, &
			ld_prezzo_1,ld_prezzo_2,ld_prezzo_3,ld_prezzo_4,ld_prezzo_5



if fs_cod_prodotto = "" then setnull(fs_cod_prodotto)
if fs_des_prodotto = "" then setnull(fs_des_prodotto)

ls_des_prodotto = fs_des_prodotto

select stringa
into   :ls_cod_valuta
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'LIR';
if sqlca.sqlcode = 100 then
	fs_errore = "Parametro LIR non trovato in parametri aziendali."
	rollback;
	return -1
end if

ls_flag_copia_de_prod_ord_ven = "N"

select flag_copia_de_prod_ord_ven
into 	:ls_flag_copia_de_prod_ord_ven
from	con_rda
where cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode = 100 then
	fs_errore = "Attnezione: parametri RDA non impostati."
	rollback;
	return -1
end if
	
if fs_cod_versione = "" then setnull(fs_cod_versione)

ldt_data_richiesta = datetime(date(today()), 00:00:00)

if isnull(fl_anno_registrazione) then 
	
	fl_anno_registrazione = f_anno_esercizio()
	setnull(fl_num_registrazione)

	ll_prog_riga_rda = 10

	select max(num_registrazione)
	into   :fl_num_registrazione
	from   det_rda
	where  cod_azienda = :s_cs_xx.cod_azienda and    
			 anno_registrazione = :fl_anno_registrazione;
	
	if sqlca.sqlcode = -1 then
		fs_errore = "Errore durante la ricerca del numero di registrazione: " + sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(fl_num_registrazione) then fl_num_registrazione = 0
	fl_num_registrazione ++
	
else
	
	select max(prog_riga_rda)
	into   :ll_prog_riga_rda
	from   det_rda
	where  cod_azienda = :s_cs_xx.cod_azienda and    
			 anno_registrazione = :fl_anno_registrazione and
			 num_registrazione = :fl_num_registrazione;
	
	if sqlca.sqlcode = -1 then
		fs_errore = "Errore durante la ricerca del numero di registrazione: " + sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(ll_prog_riga_rda) or ll_prog_riga_rda = 0 then 
		ll_prog_riga_rda = 1
	else
		ll_prog_riga_rda = ll_prog_riga_rda + 10
	end if
	
end if


setnull(ls_des_prodotto_anag)

if not isnull(fs_cod_prodotto) then
	select des_prodotto,
			 cod_fornitore,
			 prezzo_acquisto
	into   	:ls_des_prodotto_anag,
			 :ls_cod_fornitore_prefer,
			 :ld_prezzo_acquisto
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :fs_cod_prodotto;
			 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la ricerca dati del prodotto: " + sqlca.sqlerrtext
		return -1
	end if
	
	if isnull(fs_cod_fornitore) or len(fs_cod_fornitore) < 1 then
		// se il fornitore passato è nullo, uso quello preferenzia della anag prodotti
		fs_cod_fornitore = ls_cod_fornitore_prefer
	end if
end if

// Se c'è apposita opzione, prendo la descrizione del prodotto dalla riga ordine
if ls_flag_copia_de_prod_ord_ven = "S" and not isnull(fs_des_prodotto) then
	ls_des_prodotto = fs_des_prodotto
else
	ls_des_prodotto = ls_des_prodotto_anag
end if

if ls_des_prodotto = "" then setnull(ls_des_prodotto)
if fs_cod_fornitore = "" then setnull(fs_cod_fornitore)

if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	select cod_operaio
	into   :ls_cod_operaio
	from   anag_operai
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_utente  = :s_cs_xx.cod_utente;
	if sqlca.sqlcode = 100 then
		fs_errore = "Attenzione!! Utente non associato ad alcuna risorsa umana"
		rollback;
		return -1
	end if
	
else
	setnull(ls_cod_operaio)
end if

// carico prezzo di listino

ld_prezzo_rda = 0

if not isnull(fs_cod_fornitore) and not isnull(fs_cod_prodotto) and not isnull(ldt_data_richiesta) then
	
	select cod_tipo_listino_prodotto,
	       cod_valuta
	into   :ls_cod_tipo_listino_prodotto,
			 :ls_cod_valuta
	from   anag_fornitori
	where  cod_azienda = :s_cS_xx.cod_azienda and
	       cod_fornitore = :fs_cod_fornitore;
			 
	if sqlca.sqlcode = 0 then
		
		setnull(ldt_data_inizio_val_for)
		
		select max(data_inizio_val)
		into  :ldt_data_inizio_val_for
		from  listini_fornitori
		where cod_azienda = :s_cs_xx.cod_azienda and
		 	 	cod_prodotto = :fs_cod_prodotto and
				cod_fornitore = :fs_cod_fornitore and
				cod_valuta = :ls_cod_valuta and
				data_inizio_val <= :ldt_data_richiesta;
		
		if not isnull(ldt_data_inizio_val_for) then
			
			  SELECT quantita_1,   
						prezzo_1,   
						sconto_1,   
						quantita_2,   
						prezzo_2,   
						sconto_2,   
						quantita_3,   
						prezzo_3,   
						sconto_3,   
						quantita_4,   
						prezzo_4,   
						sconto_4,   
						quantita_5,   
						prezzo_5,   
						sconto_5  
				 INTO :ld_quantita_1,   
						:ld_prezzo_1,   
						:ld_sconto_1,   
						:ld_quantita_2,   
						:ld_prezzo_2,   
						:ld_sconto_2,   
						:ld_quantita_3,   
						:ld_prezzo_3,   
						:ld_sconto_3,   
						:ld_quantita_4,   
						:ld_prezzo_4,   
						:ld_sconto_4,   
						:ld_quantita_5,   
						:ld_prezzo_5,   
						:ld_sconto_5  
				 FROM listini_fornitori
				where cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :fs_cod_prodotto and
						cod_fornitore = :fs_cod_fornitore and
						cod_valuta = :ls_cod_valuta and
						data_inizio_val = :ldt_data_inizio_val_for;

			choose case fd_quan_richiesta
				case 0 to ld_quantita_1
					ld_prezzo_rda = ld_prezzo_1 
					
				case (ld_quantita_1 + 1) to ld_quantita_2
					ld_prezzo_rda = ld_prezzo_2 
					
				case (ld_quantita_2 + 1) to ld_quantita_3
					ld_prezzo_rda = ld_prezzo_3 
					
				case (ld_quantita_3 + 1) to ld_quantita_4
					ld_prezzo_rda = ld_prezzo_4 
					
				case else
					ld_prezzo_rda = ld_prezzo_5
					
			end choose
		end if
		
	end if
	
	if ld_prezzo_rda = 0 then ld_prezzo_rda = ld_prezzo_acquisto

end if

if  not ib_carica_sconti_listini_fornitori then
	// al momento non fa nulla
end if

if not ib_usa_prezzo_listino_fornitori then

	ld_prezzo_rda = fd_prezzo_richiesta
	
end if

insert into det_rda( cod_azienda, &
				anno_registrazione, &
				num_registrazione,  &
				prog_riga_rda, &
				cod_prodotto, &
				cod_versione, &
				des_prodotto, &
				cod_fornitore, &
				quan_richiesta, &
				cod_valuta, &
				prezzo_acquisto, &
				motivo_richiesta, &
				anno_commessa, &
				num_commessa, &
				cod_centro_costo, &
				cod_operaio, &
				data_richiesta, &
				data_disponibilita, &
				note, &
				flag_autorizzato, &
				data_autorizzazione, &
				cod_operaio_autorizzante, &
				flag_gen_ord_acq, &
				flag_gen_commessa, &
				cod_tipo_politica_riordino,
				des_specifica,
				anno_reg_ord_ven,
				num_reg_ord_ven,
				prog_riga_ord_ven)
values             ( :s_cs_xx.cod_azienda, &
				:fl_anno_registrazione, &
				:fl_num_registrazione, &
				:ll_prog_riga_rda, &							
				:fs_cod_prodotto, &
				:fs_cod_versione, &
				:ls_des_prodotto, &
				:fs_cod_fornitore, &
				:fd_quan_richiesta, &
				:ls_cod_valuta, &
				:ld_prezzo_rda, &
				null, &
				:fl_anno_commessa, &
				:fl_num_commessa, &
				null, &
				:ls_cod_operaio, &
				:ldt_data_richiesta, &
				:fdt_data_disponibilita, &
				:fs_motivazione, &
				'N', &
				null, &
				null, &
				'N', &
				:fs_flag_gen_commessa, &
				:fs_cod_tipo_politica_riordino,
				:fs_des_specifica,
				:fl_anno_reg_ord_ven,
				:fl_num_reg_ord_ven,
				:fl_prog_riga_ord_ven);
							
if sqlca.sqlcode < 0 then
	fs_errore = "Errore durante l'inserimento della riga di RDA:~r~n " + sqlca.sqlerrtext
	return -1
end if
		
return 0													
end function

public function integer uof_crea_bolla_trasferimento (string as_cod_deposito_partenza, string as_cod_deposito_arrivo, s_det_riga astr_righe_ordini[], ref long al_anno_bol_ven, ref long al_num_bol_ven, ref string as_error);/**
 * stefanop
 * 25/01/2012
 *
 * Creo una bolla di trasferimento
 **/

string ls_cod_cliente, ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_valuta, ls_cod_tipo_listino_prodotto, &
		ls_cod_pagamento, ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca, ls_nota_testata, ls_nota_piede, ls_flag_riep_fatt, ls_cod_banca_clien_for, &
		ls_cod_tipo_det_ven, ls_cod_misura, ls_cod_prodotto, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, ls_cod_versione, &
		ls_flag_doc_suc_det, ls_cod_tipo_movimento, ls_flag_tipo_det_ven, ls_flag_sola_iva, ls_cod_ubicazione_tes_ord_ven, ls_cod_tipo_det_ven_riga, &
		ls_flag_tipo_det_ven_riga
long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_prog_riga_bol_ven, ll_anno_commessa, ll_num_commessa, ll_num_confezioni, ll_num_pezzi_confezione
decimal ld_sconto_testata, ld_quan_ordine, ld_prezzo_vendita, ld_sconto_1, ld_prezzo_um, ld_ord_ven_prezzo_vendita_add, ld_sconto_sbt
datetime ldt_data_registrazione

//recupero cliente per fare la bolla dai parametri vendite
select cod_cliente_bol_trasf
into :ls_cod_cliente
from con_vendite;

if sqlca.sqlcode <> 0 or isnull(ls_cod_cliente) or len(ls_cod_cliente) < 1 then
	as_error = "Non è impostato nessun cliente per la generazione di bolle di traferimento.~r~nControllare i Parametri Vendita"
	return -1
end if
// ----

// Parametro SBT
guo_functions.uof_get_parametro_azienda("SBT", ld_sconto_sbt)
if isnull(ld_sconto_sbt) or ld_sconto_sbt < 0 then ld_sconto_sbt = 0
// ----

// anno, numero bolla
ll_anno_registrazione = f_anno_esercizio()

select max(num_registrazione)
into :ll_num_registrazione
from tes_bol_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_registrazione;
		 
if sqlca.sqlcode <> 0 or isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
	ll_num_registrazione = 0
end if

ll_num_registrazione++
// ----

// caricamento valori
ldt_data_registrazione = datetime(today(), 00:00:00)
ls_cod_operatore = guo_functions.uof_get_operatore_utente()

// cliente
select rag_soc_1, rag_soc_2, cod_valuta, cod_tipo_listino_prodotto, cod_pagamento, cod_agente_1, cod_agente_2, cod_banca, flag_riep_fatt, cod_banca_clien_for
into :ls_rag_soc_1, :ls_rag_soc_2, :ls_cod_valuta, :ls_cod_tipo_listino_prodotto, :ls_cod_pagamento, :ls_cod_agente_1, :ls_cod_agente_2, :ls_cod_banca, :ls_flag_riep_fatt, :ls_cod_banca_clien_for
from anag_clienti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :ls_cod_cliente;
		 
if sqlca.sqlcode <> 0 then
	as_error = "Errore durante la lettura valori del cliente."
	
	if sqlca.sqlcode < 0 then as_error += "~r~n" + sqlca.sqlerrtext
	
	return -1
end if

// sconto
select sconto
into :ld_sconto_testata
from tab_pagamenti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_pagamento = :ls_cod_pagamento;

// deposito
select indirizzo, localita, frazione, cap, provincia
into :ls_indirizzo, :ls_localita, :ls_frazione, :ls_cap, :ls_provincia
from anag_depositi
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_deposito = :as_cod_deposito_partenza;

if sqlca.sqlcode <> 0 then
	as_error = "Errore durante la lettura valori del deposito."
	
	if sqlca.sqlcode < 0 then as_error += "~r~n" + sqlca.sqlerrtext
	
	return -1
end if

// Note
if isnull(ls_nota_testata) then ls_nota_testata = ""
if isnull(ls_nota_piede) then ls_nota_piede = ""
// -- fine caricamento valori

// testata
INSERT INTO tes_bol_ven
	(cod_azienda,   
	anno_registrazione,   
	num_registrazione,   
	data_registrazione,   
	cod_operatore,   
	cod_tipo_bol_ven,   
	cod_cliente,
	cod_fornitore,
	cod_des_cliente,   
	cod_fil_fornitore,
	rag_soc_1,   
	rag_soc_2,   
	indirizzo,   
	localita,   
	frazione,   
	cap,   
	provincia,   
	cod_deposito,   
	cod_deposito_tras,
	cod_ubicazione,
	cod_valuta,   
	cambio_ven,   
	cod_tipo_listino_prodotto,   
	cod_pagamento,   
	sconto,   
	cod_agente_1,
	cod_agente_2,
	cod_banca,   
	num_ord_cliente,   
	data_ord_cliente,   
	cod_imballo,   
	aspetto_beni,
	peso_netto,
	peso_lordo,
	num_colli,
	cod_vettore,   
	cod_inoltro,   
	cod_mezzo,
	causale_trasporto,
	cod_porto,   
	cod_resa,   
	cod_documento,
	numeratore_documento,
	anno_documento,
	num_documento,
	data_bolla,
	nota_testata,   
	nota_piede,   
	flag_fuori_fido,
	flag_blocco,   
	flag_movimenti,  
	flag_riep_fat,
	cod_banca_clien_for,
	flag_gen_fat,
	cod_causale,
	flag_doc_suc_tes,
	flag_st_note_tes,
	flag_doc_suc_pie,
	flag_st_note_pie)  					 
VALUES (
	:s_cs_xx.cod_azienda,   
	:ll_anno_registrazione,   
	:ll_num_registrazione,   
	:ldt_data_registrazione,   
	:ls_cod_operatore,   
	:is_cod_tipo_bol_ven,
	:ls_cod_cliente,
	null,
	null,
	null,
	:ls_rag_soc_1,  
	:ls_rag_soc_2,
	:ls_indirizzo,
	:ls_localita,
	:ls_frazione,
	:ls_cap,
	:ls_provincia,
	:as_cod_deposito_partenza,
	:as_cod_deposito_arrivo,
	null,
	:ls_cod_valuta, 
	1,  // ld_cambio_ven vedere
	:ls_cod_tipo_listino_prodotto,
	:ls_cod_pagamento,
	:ld_sconto_testata,
	:ls_cod_agente_1,
	:ls_cod_agente_2,
	:ls_cod_banca,
	null,
	null,
	:is_cod_imballo,
	:is_aspetto_beni,
	:id_peso_netto,
	:id_peso_lordo,
	:id_num_colli,
	:is_cod_vettore,
	:is_cod_inoltro,
	:is_cod_mezzo,
	:is_causale_trasporto,
	:is_cod_porto,
	:is_cod_resa,
	null,
	null,
	0,
	0,
	null,
	:ls_nota_testata,   
	:ls_nota_piede,   
	'N',
	'N',   
	'N',
	:ls_flag_riep_fatt,
	:ls_cod_banca_clien_for,
	'N',
	:is_cod_causale,
	'I',
	'I',
	'I',
	'I');
	
if sqlca.sqlcode < 0 then
	as_error = "Errore durante il salvataggio della testa del nuovo DDT.~r~n" + sqlca.sqlerrtext
	return -1
end if
// -- fine testata

// dettaglio movimento
select cod_tipo_det_ven
into :ls_cod_tipo_det_ven
from tab_tipi_bol_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_bol_ven = :is_cod_tipo_bol_ven;
		 
if sqlca.sqlcode <> 0 then
	as_error = "Errore durante la lettura del dettaglio vendita per il tipo bolla impostato."
	return -1
end if

select flag_tipo_det_ven,
		 cod_tipo_movimento,
		 flag_sola_iva
into	:ls_flag_tipo_det_ven,
		 :ls_cod_tipo_movimento,
		 :ls_flag_sola_iva
from tab_tipi_det_ven
where cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
// ---

// carico il dettaglio
ll_prog_riga_bol_ven = 0

for ll_i = 1 to upperbound(astr_righe_ordini)
	
	string ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], ls_cod_cliente_a[], ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, &
			ls_flag_evasione
	long ll_progr_stock[], ll_anno_reg_des_mov, ll_num_reg_des_mov
	decimal ld_quan_in_evasione, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, &
				ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
				ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10,ld_val_sconto_testata, &
				ld_val_riga_sconto_testata,ld_val_sconto_pagamento, ld_sconto_pagamento, ld_val_evaso, ld_tot_val_evaso
	datetime ldt_data_stock[]
	
	ll_prog_riga_bol_ven += 10
	
	select cod_ubicazione
	into :ls_cod_ubicazione_tes_ord_ven
	from tes_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and  
			anno_registrazione = :astr_righe_ordini[ll_i].anno_registrazione and  
			num_registrazione = :astr_righe_ordini[ll_i].num_registrazione;
			
	if sqlca.sqlcode <> 0 then
		as_error = "Errore durante il recupero del codice ubicazione nella tes_ord_ven"
		return -1
	end if
	
	ld_prezzo_vendita = 0
	ld_ord_ven_prezzo_vendita_add = 0

	select cod_prodotto,   
			des_prodotto, 	  
			cod_misura,   
			quan_ordine,
			nota_dettaglio,
			cod_iva,
			cod_tipo_det_ven, // stefanop 29/06/2012,
			prezzo_vendita,
			sconto_1
	into 	:ls_cod_prodotto, 
			:ls_des_prodotto, 
			:ls_cod_misura, 
			:ld_quan_ordine,
			:ls_nota_dettaglio,
			:ls_cod_iva,
			:ls_cod_tipo_det_ven_riga,
			:ld_prezzo_vendita,
			:ld_sconto_1
	from det_ord_ven
	where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			det_ord_ven.anno_registrazione = :astr_righe_ordini[ll_i].anno_registrazione and  
			det_ord_ven.num_registrazione = :astr_righe_ordini[ll_i].num_registrazione and
			det_ord_ven.prog_riga_ord_ven = :astr_righe_ordini[ll_i].prog_riga;

	// stefanop 27/07/2012: si tratta di un semilavorato che il codice prodotto che arriva dall'array è diverso da quella della riga d'ordine del DB
	// infatti l'array viene valorizzato dalla finestra w_evas_ordini_barcode_trasf che fa lei i controlli di che prodotto bisogna trasferire
	// ---------------------------------------------------------------------------------
	//if not isnull(astr_righe_ordini[ll_i].cod_prodotto) and len(astr_righe_ordini[ll_i].cod_prodotto) > 0 then
	if astr_righe_ordini[ll_i].cod_prodotto <> ls_cod_prodotto then
		// si tratta di un semilavorato
		ls_cod_prodotto = astr_righe_ordini[ll_i].cod_prodotto
		
		select des_prodotto, cod_misura_mag
		into :ls_des_prodotto, :ls_cod_misura
		from anag_prodotti  
		where cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_prodotto = :ls_cod_prodotto;
		
		ld_quan_ordine = astr_righe_ordini[ll_i].quan_ordine
		ld_prezzo_vendita = astr_righe_ordini[ll_i].prezzo_vendita
		
		// penso che lo sconto qui vada a 0 perchè viene calcolato dalla finestra che chiama questa funzione
		ld_sconto_1 = 0
		
	else
		// E' il prodotto finito, vado a sommade le righe ADD collegate in quanto tale righe non vengono spedite e si
		// perderebbe il valore. Ad esempio un motore (ADD) non verrebbe conteggiato nella riga di trasferimento.
		// StPu 16/04/2014 eseguita correzione bug anomalia 2014/185
		
		select	sum( prezzo_vendita - (prezzo_vendita * sconto_1 / 100 ))
		into 		:ld_ord_ven_prezzo_vendita_add
		from 		det_ord_ven
		where 	cod_azienda = :s_cs_xx.cod_azienda and
				 	anno_registrazione = :astr_righe_ordini[ll_i].anno_registrazione and  
				 	num_registrazione = :astr_righe_ordini[ll_i].num_registrazione and
				 	num_riga_appartenenza = :astr_righe_ordini[ll_i].prog_riga and
				 	cod_tipo_det_ven = 'ADD';
				 
		if sqlca.sqlcode < 0 then
			as_error = g_str.format("Errore durante la somma delle righe ADDIZIONALI collegate alla riga d'ordine $1/$2/$3", astr_righe_ordini[ll_i].anno_registrazione, astr_righe_ordini[ll_i].num_registrazione, astr_righe_ordini[ll_i].prog_riga)
			as_error += "~r~n" + sqlca.sqlerrtext
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ld_ord_ven_prezzo_vendita_add) then
			ld_ord_ven_prezzo_vendita_add = 0
		end if
		
		ld_ord_ven_prezzo_vendita_add = round(ld_ord_ven_prezzo_vendita_add, 2)
	end if
	// ---------------------------------------------------------------------------------
		
	// Modifica Nicola
	if ls_flag_doc_suc_det = 'I' then //nota dettaglio
		select flag_doc_suc_or
		into :ls_flag_doc_suc_det
		from tab_tipi_det_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		
	
	if ls_flag_doc_suc_det = 'N' then
		setnull(ls_nota_dettaglio)
	end if		
	// -- Fine Modifica		
		
	if sqlca.sqlcode = -1 then
		as_error = "Si è verificato un errore in fase di lettura tipi dettagli.~n" + sqlca.sqlerrtext
		return -1
	end if
	
	// Deposito
	ls_cod_deposito[1] = as_cod_deposito_partenza
	ls_cod_ubicazione[1] = ls_cod_ubicazione_tes_ord_ven
	setnull(ls_cod_cliente_a[1])
	setnull(ls_cod_lotto[1])
	setnull(ll_progr_stock[1])
	setnull(ldt_data_stock[1])
	setnull(ls_cod_fornitore[1])
	
	ls_cod_deposito[2] = as_cod_deposito_arrivo
	ls_cod_ubicazione[2] = ls_cod_ubicazione_tes_ord_ven
	setnull(ls_cod_cliente_a[2])
	setnull(ls_cod_lotto[2])
	setnull(ll_progr_stock[2])
	setnull(ldt_data_stock[2])
	setnull(ls_cod_fornitore[2])
	// ----
	
	// stefanop 29/06/2012
	// controllo il tipo di dettaglio e se non fa movimenti di magazzino
	// lascio il tipo invariato altrimenti durante la conferma da errore.
	select flag_tipo_det_ven
	into :ls_flag_tipo_det_ven_riga
	from tab_tipi_det_ven
	where cod_azienda = :s_cs_xx.cod_azienda and 
				cod_tipo_det_ven = :ls_cod_tipo_det_ven_riga;
				
	if ls_flag_tipo_det_ven_riga = "M" then
		// Imposto il tipo dettaglio con quello imposto dalla testa, solitamente (TRI)
		ls_cod_tipo_det_ven_riga = ls_cod_tipo_det_ven
	end if
	// ----
	
	// Stefanop 08/08/2012: 1. applico lo sconto_1 alla riga, 2. aggiungo la somma delle righe ADD, 3. applico ulteriore sconto SBT
	// 1. Applico sconto_1
	if ld_sconto_1 > 0 then
		ld_prezzo_vendita = ld_prezzo_vendita * (1 - (ld_sconto_1 / 100))
	end if
	
	// 2. Sommo le righe ADD
	ld_prezzo_vendita += ld_ord_ven_prezzo_vendita_add
	
	// 3. Applico ulteriore sconto SBT
	if ld_sconto_sbt > 0 then
		ld_prezzo_vendita = ld_prezzo_vendita * (1 - (ld_sconto_sbt / 100))
	end if
	
	ld_prezzo_vendita = round(ld_prezzo_vendita, 2)
	// ----
	
	
	insert into det_bol_ven (
		cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		prog_riga_bol_ven,   
		cod_tipo_det_ven,   
		cod_deposito,
		cod_ubicazione,
		cod_lotto,
		progr_stock,
		data_stock,
		cod_misura,   
		cod_prodotto,   
		des_prodotto,   
		quan_consegnata,   
		anno_registrazione_ord_ven,   
		num_registrazione_ord_ven,   
		prog_riga_ord_ven,
		cod_versione,
		cod_iva,
		cod_tipo_movimento,
		prezzo_vendita)
	values (
		:s_cs_xx.cod_azienda,   
		:ll_anno_registrazione,   
		:ll_num_registrazione,   
		:ll_prog_riga_bol_ven,   
		:ls_cod_tipo_det_ven_riga,   
		:ls_cod_deposito[1],
		:ls_cod_ubicazione[1],
		:ls_cod_lotto[1],
		:ll_progr_stock[1],
		:ldt_data_stock[1],
		:ls_cod_misura,   
		:ls_cod_prodotto,   
		:ls_des_prodotto,   
		:ld_quan_ordine,
		:astr_righe_ordini[ll_i].anno_registrazione,   
		:astr_righe_ordini[ll_i].num_registrazione,   
		:astr_righe_ordini[ll_i].prog_riga,
		:ls_cod_versione,
		:ls_cod_iva,
		:ls_cod_tipo_movimento,
		:ld_prezzo_vendita);	
			
	if sqlca.sqlcode <> 0 then
		as_error = "Errore durante la scrittura Dettagli Bolle Vendita.~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente_a[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
		as_error = "Si è verificato un errore in fase di creazione destinazioni movimenti."
		return -1
	end if

	if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
		return 1
	end if
	
	update det_bol_ven
	set anno_reg_des_mov = :ll_anno_reg_des_mov,
		num_reg_des_mov = :ll_num_reg_des_mov
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione and
		prog_riga_bol_ven = :ll_prog_riga_bol_ven;
		
	if sqlca.sqlcode <> 0 then
		as_error = "Errore durante l'aggiornamento delle destinazioni movimenti.~n" + sqlca.sqlerrtext
		return -1
	end if

next
// -- fine dettaglio

uo_calcola_documento_euro luo_calcolo
luo_calcolo = create uo_calcola_documento_euro

if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_ven", as_error) <> 0 then
	destroy luo_calcolo
	return -1
end if

destroy luo_calcolo

al_anno_bol_ven = ll_anno_registrazione
al_num_bol_ven = ll_num_registrazione
return 0
end function

public function integer uof_crea_bolla_trasferimento (string as_cod_deposito_partenza, string as_cod_deposito_arrivo, string as_table, long al_anno, long al_numero, ref long al_anno_bol_ven, ref long al_num_bol_ven, ref string as_error);/** FUNZIONE DI CREAZIONE DI UNA BOLLA DI TRASFERIMENTO PARTENZO DA UNA BOLLA DI ENTRATA PASSATA COME PARAMETRO.
CREAZIONE: stefanop  * 25/01/2012
EnMe 04/07/2012: modificata la funzione per caricare il prezzo dell'ultima riga fattura (ovviamente la fattura con data <= bolla) che ha il flag_agg_mov impostato = SI

 Creo una bolla di trasferimento 
 **/

string ls_cod_cliente, ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_valuta, ls_cod_tipo_listino_prodotto, &
		ls_cod_pagamento, ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca, ls_nota_testata, ls_nota_piede, ls_flag_riep_fatt, ls_cod_banca_clien_for, &
		ls_cod_tipo_det_ven, ls_cod_misura, ls_cod_prodotto, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, ls_cod_versione, &
		ls_flag_doc_suc_det, ls_cod_tipo_movimento, ls_flag_tipo_det_ven, ls_flag_sola_iva, ls_sql, ls_cod_ubicazione_det, ls_prog_riga, ls_where, ls_quan_ordine, &
		ls_cod_tipo_det_ven_riga, ls_flag_tipo_det_ven_riga, ls_des_prodotto_anag, ls_cod_misura_mag_anag, ls_sql_prezzo,ls_cod_tipo_det_acq,ls_flag_lavorazione, &
		ls_allegati[],ls_errore_email
		
long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_prog_riga_bol_ven, ll_anno_commessa, ll_num_commessa, ll_num_confezioni, ll_num_pezzi_confezione, ll_rows, &
		ll_det_rows, ll_prog_riga, ll_anno_registrazione_ord_ven, ll_num_registrazione_ord_ven, ll_prog_riga_ord_ven, ll_anno_reg_bol_acq, ll_num_reg_bol_acq, &
		ll_prog_riga_bol_acq
		
decimal ld_sconto_testata, ld_quan_ordine, ld_prezzo_vendita, ld_fat_conversione_ven, ld_sconto_1, ld_sconto_2, ld_provvigione_1, ld_provvigione_2, &
		  ld_quan_evasa, ld_sconto_3, ld_sconto_4, ld_sconto_5,ld_sconto_6,ld_sconto_7,ld_sconto_8,ld_sconto_9,ld_sconto_10, ld_quantita_um, &
		  ld_prezzo_um,ld_prezzo_trasferimento
datetime ldt_data_registrazione, ldt_data_protocollo
datastore lds_store, lds_det_righe

string ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_fornitore[], ls_cod_cliente_a[], ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, &
			ls_flag_evasione
long ll_progr_stock[], ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_ret
decimal ld_quan_in_evasione, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, &
			ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
			ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10,ld_val_sconto_testata, &
			ld_val_riga_sconto_testata,ld_val_sconto_pagamento, ld_sconto_pagamento, ld_val_evaso, ld_tot_val_evaso, ld_ord_ven_prezzo_vendita_add, ld_sconto_sbt
datetime ldt_data_stock[]
datastore lds_fatture


//recupero cliente per fare la bolla dai parametri vendite
select cod_cliente_bol_trasf
into :ls_cod_cliente
from con_vendite;

if sqlca.sqlcode <> 0 or isnull(ls_cod_cliente) or len(ls_cod_cliente) < 1 then
	as_error = "Non è impostato nessun cliente per la generazione di bolle di traferimento.~r~nControllare i Parametri Vendita"
	return -1
end if
// ----

// Parametro SBT
guo_functions.uof_get_parametro_azienda("SBT", ld_sconto_sbt)
if isnull(ld_sconto_sbt) or ld_sconto_sbt < 0 then ld_sconto_sbt = 0
// ----

// anno, numero bolla
ll_anno_registrazione = f_anno_esercizio()

select max(num_registrazione)
into :ll_num_registrazione
from tes_bol_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_registrazione;
		 
if sqlca.sqlcode <> 0 or isnull(ll_num_registrazione) or ll_num_registrazione < 1 then
	ll_num_registrazione = 0
end if

ll_num_registrazione++
// ----

// caricamento valori
ldt_data_registrazione = datetime(today(), 00:00:00)
ls_cod_operatore = guo_functions.uof_get_operatore_utente()

// cliente
select rag_soc_1, rag_soc_2, cod_valuta, cod_tipo_listino_prodotto, cod_pagamento, cod_agente_1, cod_agente_2, cod_banca, flag_riep_fatt, cod_banca_clien_for
into :ls_rag_soc_1, :ls_rag_soc_2, :ls_cod_valuta, :ls_cod_tipo_listino_prodotto, :ls_cod_pagamento, :ls_cod_agente_1, :ls_cod_agente_2, :ls_cod_banca, :ls_flag_riep_fatt, :ls_cod_banca_clien_for
from anag_clienti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :ls_cod_cliente;
		 
if sqlca.sqlcode <> 0 then
	as_error = "Errore durante la lettura valori del cliente."
	
	if sqlca.sqlcode < 0 then as_error += "~r~n" + sqlca.sqlerrtext
	
	return -1
end if

// sconto
select sconto
into :ld_sconto_testata
from tab_pagamenti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_pagamento = :ls_cod_pagamento;

// deposito
select indirizzo, localita, frazione, cap, provincia
into :ls_indirizzo, :ls_localita, :ls_frazione, :ls_cap, :ls_provincia
from anag_depositi
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_deposito = :as_cod_deposito_partenza;

if sqlca.sqlcode <> 0 then
	as_error = "Errore durante la lettura valori del deposito."
	
	if sqlca.sqlcode < 0 then as_error += "~r~n" + sqlca.sqlerrtext
	
	return -1
end if

// Note
if isnull(ls_nota_testata) then ls_nota_testata = ""
if isnull(ls_nota_piede) then ls_nota_piede = ""
// -- fine caricamento valori

// testata
INSERT INTO tes_bol_ven
	(cod_azienda,   
	anno_registrazione,   
	num_registrazione,   
	data_registrazione,   
	cod_operatore,   
	cod_tipo_bol_ven,   
	cod_cliente,
	cod_fornitore,
	cod_des_cliente,   
	cod_fil_fornitore,
	rag_soc_1,   
	rag_soc_2,   
	indirizzo,   
	localita,   
	frazione,   
	cap,   
	provincia,   
	cod_deposito,   
	cod_deposito_tras,
	cod_ubicazione,
	cod_valuta,   
	cambio_ven,   
	cod_tipo_listino_prodotto,   
	cod_pagamento,   
	sconto,   
	cod_agente_1,
	cod_agente_2,
	cod_banca,   
	num_ord_cliente,   
	data_ord_cliente,   
	cod_imballo,   
	aspetto_beni,
	peso_netto,
	peso_lordo,
	num_colli,
	cod_vettore,   
	cod_inoltro,   
	cod_mezzo,
	causale_trasporto,
	cod_porto,   
	cod_resa,   
	cod_documento,
	numeratore_documento,
	anno_documento,
	num_documento,
	data_bolla,
	nota_testata,   
	nota_piede,   
	flag_fuori_fido,
	flag_blocco,   
	flag_movimenti,  
	flag_riep_fat,
	cod_banca_clien_for,
	flag_gen_fat,
	cod_causale,
	flag_doc_suc_tes,
	flag_st_note_tes,
	flag_doc_suc_pie,
	flag_st_note_pie)  					 
VALUES (
	:s_cs_xx.cod_azienda,   
	:ll_anno_registrazione,   
	:ll_num_registrazione,   
	:ldt_data_registrazione,   
	:ls_cod_operatore,   
	:is_cod_tipo_bol_ven,
	:ls_cod_cliente,
	null,
	null,
	null,
	:ls_rag_soc_1,  
	:ls_rag_soc_2,
	:ls_indirizzo,
	:ls_localita,
	:ls_frazione,
	:ls_cap,
	:ls_provincia,
	:as_cod_deposito_partenza,
	:as_cod_deposito_arrivo,
	null,
	:ls_cod_valuta, 
	1,  // ld_cambio_ven vedere
	:ls_cod_tipo_listino_prodotto,
	:ls_cod_pagamento,
	:ld_sconto_testata,
	:ls_cod_agente_1,
	:ls_cod_agente_2,
	:ls_cod_banca,
	null,
	null,
	:is_cod_imballo,
	:is_aspetto_beni,
	:id_peso_netto,
	:id_peso_lordo,
	:id_num_colli,
	:is_cod_vettore,
	:is_cod_inoltro,
	:is_cod_mezzo,
	:is_causale_trasporto,
	:is_cod_porto,
	:is_cod_resa,
	null,
	null,
	0,
	0,
	null,
	:ls_nota_testata,   
	:ls_nota_piede,   
	'N',
	'N',   
	'N',
	:ls_flag_riep_fatt,
	:ls_cod_banca_clien_for,
	'N',
	:is_cod_causale,
	'I',
	'I',
	'I',
	'I');
	
if sqlca.sqlcode < 0 then
	as_error = "Errore durante il salvataggio della testa del nuovo DDT.~r~n" + sqlca.sqlerrtext
	return -1
end if
// -- fine testata

// dettaglio movimento
select cod_tipo_det_ven
into :ls_cod_tipo_det_ven
from tab_tipi_bol_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_bol_ven = :is_cod_tipo_bol_ven;
		 
if sqlca.sqlcode <> 0 then
	as_error = "Errore durante la lettura del dettaglio vendita per il tipo bolla impostato."
	return -1
end if

select flag_tipo_det_ven,
		 cod_tipo_movimento,
		 flag_sola_iva
into	:ls_flag_tipo_det_ven,
		 :ls_cod_tipo_movimento,
		 :ls_flag_sola_iva
from tab_tipi_det_ven
where cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
// ---


setnull(ll_anno_registrazione_ord_ven)
setnull(ll_num_registrazione_ord_ven)
setnull(ll_prog_riga_ord_ven)
setnull(ll_anno_reg_bol_acq)
setnull(ll_num_reg_bol_acq)
setnull(ll_prog_riga_bol_acq)


// carico il dettaglio			
choose case as_table
	case "ord_acq"
		ls_prog_riga = "prog_riga_ordine_acq"
		ls_quan_ordine = "quan_ordinata"
		ls_where = "anno_registrazione=" + string(al_anno) + " AND num_registrazione=" + string(al_numero)
		
	case "ord_ven"
		ls_prog_riga = "prog_riga_ord_ven"
		ls_quan_ordine = "quan_ordine"
		ls_where = "anno_registrazione=" + string(al_anno) + " AND num_registrazione=" + string(al_numero)
		ll_anno_registrazione_ord_ven = al_anno
		ll_num_registrazione_ord_ven = al_numero

	case "bol_acq"
		ls_prog_riga = "prog_riga_bolla_acq"
		ls_quan_ordine = "quan_arrivata"
		ls_where = "anno_bolla_acq=" + string(al_anno) + " AND num_bolla_acq=" + string(al_numero)
		ll_anno_reg_bol_acq = al_anno
		ll_num_reg_bol_acq = al_numero
		
	case "bol_ven"
		ls_prog_riga = "prog_riga_bol_ven"
		ls_quan_ordine = "quan_ordine"
		ls_where = "anno_registrazione=" + string(al_anno) + " AND num_registrazione=" + string(al_numero)
		
	case "fat_ven"
		ls_prog_riga = "prog_riga_fat_ven"
		ls_quan_ordine = "quan_fatturata"
		ls_where = "anno_registrazione=" + string(al_anno) + " AND num_registrazione=" + string(al_numero)
		
	case "fat_acq"
		ls_prog_riga = "prog_riga_fat_acq"
		ls_quan_ordine = "quan_fatturata"
		ls_where = "anno_registrazione=" + string(al_anno) + " AND num_registrazione=" + string(al_numero)

end choose

ls_sql = "SELECT " + ls_prog_riga + ", cod_prodotto, des_prodotto, cod_misura, " + ls_quan_ordine + ", nota_dettaglio, cod_iva, cod_ubicazione, imponibile_iva, sconto_1 " + &
			"FROM det_" + as_table + " " + &
			"WHERE cod_azienda='" + s_cs_xx.cod_azienda + "' AND " + ls_where
			
ll_det_rows = guo_functions.uof_crea_datastore(lds_det_righe, ls_sql, as_error)
if ll_det_rows < 0 then return -1
			
ll_prog_riga_bol_ven = 0

for ll_i = 1 to ll_det_rows
	
	ld_prezzo_vendita = 0
	ld_ord_ven_prezzo_vendita_add = 0
	ld_sconto_1 = 0
	
	ll_prog_riga = lds_det_righe.getitemnumber(ll_i, ls_prog_riga)
	ls_cod_prodotto = lds_det_righe.getitemstring(ll_i, "cod_prodotto")
	ls_des_prodotto = lds_det_righe.getitemstring(ll_i, "des_prodotto")
	ls_cod_misura = lds_det_righe.getitemstring(ll_i, "cod_misura")
	ld_quan_ordine = lds_det_righe.getitemnumber(ll_i, 5) // quantità
	ls_nota_dettaglio = lds_det_righe.getitemstring(ll_i, "nota_dettaglio")
	ls_cod_iva = lds_det_righe.getitemstring(ll_i, "cod_iva")
	ls_cod_ubicazione_det = lds_det_righe.getitemstring(ll_i, "cod_ubicazione")
	ld_prezzo_vendita = lds_det_righe.getitemnumber(ll_i, "imponibile_iva")
	// prendo l'imponibile e divido per la quantità; infatti a me serve il prezzo netto, mentre nell'ordine poteva esserci un prezzo di acquisto con eventuali sconti.
	ld_prezzo_vendita = round(ld_prezzo_vendita / ld_quan_ordine, 4)
	ld_sconto_1 = lds_det_righe.getitemnumber(ll_i, "sconto_1")
	
	ll_prog_riga_bol_ven += 10
	ls_flag_lavorazione = "N"
			
	choose case as_table
		case "ord_acq"

		case "ord_ven"
			ll_prog_riga_ord_ven = ll_prog_riga
	
		case "bol_acq"
			ll_prog_riga_bol_acq = ll_prog_riga
			
			select cod_tipo_det_acq
			into 	:ls_cod_tipo_det_acq
			from det_bol_acq
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_bolla_acq = :al_anno and
					num_bolla_acq = :al_numero and
					prog_riga_bolla_acq = :ll_prog_riga;
			if sqlca.sqlcode = -1 then
				as_error =  "Errore durante la ricerca bolla (uof_crea_bolla_trasferimento).~n" + sqlca.sqlerrtext
				return -1
			end if
			if sqlca.sqlcode = 100 then
				as_error =  g_str.format("La bolla di acquisto $1 / $2 / $3 non può essere trovata ",al_anno,al_numero,ll_prog_riga)
				return -1
			end if
			
			select flag_riferito
			into 	:ls_flag_lavorazione
			from 	tab_tipi_det_acq
			where cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_det_acq = :ls_cod_tipo_det_acq;
			if sqlca.sqlcode = -1 then
				as_error =  "Errore durante la ricerca del tipo dettaglio acquisti (uof_crea_bolla_trasferimento).~n" + sqlca.sqlerrtext
				return -1
			end if
			if sqlca.sqlcode = 100 then
				as_error =  "Il tipo dettaglio "+ls_cod_tipo_det_acq+" non esiste nella tabella! Verificare"
				return -1
			end if
			
		case "bol_ven"
	
	end choose
	
	if not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 1 then
		
		select des_prodotto, 
				cod_misura_mag
		into 	:ls_des_prodotto_anag, 
				:ls_cod_misura_mag_anag
		from 	anag_prodotti  
		where cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode = -1 then
			as_error =  "Errore durante la ricerca del codice prodotto in anagrafica prodotti (uof_crea_bolla_trasferimento).~n" + sqlca.sqlerrtext
			return -1
		end if
		if sqlca.sqlcode = 100 then
			as_error =  "Il prodotto "+ls_cod_prodotto+" non esiste anagrafica! Verificare"
			return -1
		end if
		
		// (EnMe 03/08/2012) Bisogna distinguere 2 casi; il caso della riga di lavorazione e il caso della riga di pura materia prima
		// siccome il flag_riferito non è usato, allora vado a sfruttarlo per indicare i tipi dettaglio che sono lavorazioni
		if ls_flag_lavorazione = "S" then
		
			// (EnMe 04/07/2012) Eseguo la ricerca del prezzo; vado in cerca dell'ultima riga fattura del prodotto (di qualsiasi fornitore) con il flag_agg_mov=SI 
			// 							 e da quella prendo il prezzo.
			
			ls_sql_prezzo = 	"select data_protocollo, mov_magazzino.val_movimento from tes_fat_acq " + &
						" join det_fat_acq on tes_fat_acq.anno_registrazione =  det_fat_acq.anno_registrazione and tes_fat_acq.num_registrazione =  det_fat_acq.num_registrazione " + &
						" join mov_magazzino on  det_fat_acq.anno_registrazione_mov_mag = mov_magazzino.anno_registrazione and det_fat_acq.num_registrazione_mov_mag=mov_magazzino.num_registrazione " + &
						" where data_protocollo <= '"+string(ldt_data_registrazione, s_cs_xx.db_funzioni.formato_data)+"' and flag_agg_mov='S' and det_fat_acq.cod_prodotto = '" + ls_cod_prodotto + "' " + &
						" order by  tes_fat_acq.data_protocollo DESC , tes_fat_acq.num_registrazione DESC , det_fat_acq.prog_riga_fat_acq DESC "
						
			
			ll_ret = guo_functions.uof_crea_datastore( lds_fatture, ls_sql_prezzo)
			
			choose case ll_ret
				case is < 0
					as_error = "Si è verificato un errore cercando il prezzo del prodotto " + ls_cod_prodotto + " dalle fatture di acquisto "
					destroy lds_fatture
					return -1
				case 0
					// non ho trovato nessun acquisto precedente; mando la mail per segnalare che il prezzo va messo a mano.
					uo_outlook luo_mail
					luo_mail = create uo_outlook
					
					luo_mail.uof_invio_sendmail("A", &
														g_str.format("Avviso Anomalia Valore Trasferimento DDT $1 / $2 / $3", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven), &
														g_str.format("Salve,~r~nil DDT numero interno   $1 / $2 alla riga $3 presenta un valore a zero.~r~nTale DDT nasce dalla bolla di entrata $4 / $5 riga $6 e si riferisce ad un caso di trasferimento di un prodotto a cui è stata fatta una lavorazione; è possibile che in precedenza non esista un acquisto simile, per cui si rende necessario un intervento manuale.", ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven, al_anno, al_numero,ll_prog_riga),&
														"COMDDT", "TRASF", ls_allegati, ref ls_errore_email)
					
					destroy luo_mail
					
				case is > 0
					ldt_data_protocollo = lds_fatture.getitemdatetime(1, 1)
					// al prezzo di vendita vado a sostituire il rpezzo della lavorazione.
					ld_prezzo_vendita = lds_fatture.getitemnumber (1, 2)
			end choose
			
			destroy lds_fatture
			
		end if
		
	end if
		
		
	// Modifica Nicola
	if ls_flag_doc_suc_det = 'I' then //nota dettaglio
		select flag_doc_suc_or
		into :ls_flag_doc_suc_det
		from tab_tipi_det_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		
	
	
	if ls_flag_doc_suc_det = 'N' then
		setnull(ls_nota_dettaglio)
	end if		
	// -- Fine Modifica		
		
	if sqlca.sqlcode = -1 then
		as_error = "Si è verificato un errore in fase di lettura tipi dettagli.~n" + sqlca.sqlerrtext
		return -1
	end if
	
	// Deposito
	ls_cod_deposito[1] = as_cod_deposito_partenza
	ls_cod_ubicazione[1] = ls_cod_ubicazione_det
	setnull(ls_cod_cliente_a[1])
	setnull(ls_cod_lotto[1])
	setnull(ll_progr_stock[1])
	setnull(ldt_data_stock[1])
	setnull(ls_cod_fornitore[1])
	
	ls_cod_deposito[2] = as_cod_deposito_arrivo
	ls_cod_ubicazione[2] = ls_cod_ubicazione_det
	setnull(ls_cod_cliente_a[2])
	setnull(ls_cod_lotto[2])
	setnull(ll_progr_stock[2])
	setnull(ldt_data_stock[2])
	setnull(ls_cod_fornitore[2])
	// ----
	
	if as_table = "det_ord_ven" then
		// E' il prodotto finito, vado a sommade le righe ADD collegate in quanto tale righe non vengono spedite e si
		// perderebbe il valore. Ad esempio un motore (ADD) non verrebbe conteggiato nella riga di trasferimento.
		// StPu 16/04/2014 eseguita correzione bug anomalia 2014/185
		select	sum( prezzo_vendita - (prezzo_vendita * sconto_1 / 100 ))
		into :ld_ord_ven_prezzo_vendita_add
		from det_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :al_anno and  
				 num_registrazione = :al_numero and
				 num_riga_appartenenza = :ll_prog_riga and
				 cod_tipo_det_ven = 'ADD';
				 
		if sqlca.sqlcode < 0 then
			as_error = g_str.format("Errore durante la somma delle righe ADDIZIONALI collegate alla riga d'ordine $1/$2/$3", al_anno, al_numero, ll_prog_riga)
			as_error += "~r~n" + sqlca.sqlerrtext
			return -1
		elseif sqlca.sqlcode = 100 or isnull(ld_ord_ven_prezzo_vendita_add) then
			ld_ord_ven_prezzo_vendita_add = 0
		end if
		
		ld_ord_ven_prezzo_vendita_add = round(ld_ord_ven_prezzo_vendita_add, 2)
		
		ld_prezzo_vendita += ld_ord_ven_prezzo_vendita_add
		
		// 3. Applico ulteriore sconto SBT
		if ld_sconto_sbt > 0 then
			ld_prezzo_vendita = ld_prezzo_vendita * (1 - (ld_sconto_sbt / 100))
		end if
	end if

	ld_prezzo_vendita = round(ld_prezzo_vendita, 2)
	// ----
	
	
	insert into det_bol_ven (
		cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		prog_riga_bol_ven,   
		cod_tipo_det_ven,   
		cod_deposito,
		cod_ubicazione,
		cod_lotto,
		progr_stock,
		data_stock,
		cod_misura,   
		cod_prodotto,   
		des_prodotto,   
		quan_consegnata,
		prezzo_vendita,
		cod_versione,
		cod_iva,
		cod_tipo_movimento,
		anno_registrazione_ord_ven,   
		num_registrazione_ord_ven,   
		prog_riga_ord_ven,
		anno_reg_bol_acq,
		num_reg_bol_acq,
		prog_riga_bol_acq)
	values (
		:s_cs_xx.cod_azienda,   
		:ll_anno_registrazione,   
		:ll_num_registrazione,   
		:ll_prog_riga_bol_ven,   
		:ls_cod_tipo_det_ven,   
		:ls_cod_deposito[1],
		:ls_cod_ubicazione[1],
		:ls_cod_lotto[1],
		:ll_progr_stock[1],
		:ldt_data_stock[1],
		:ls_cod_misura,   
		:ls_cod_prodotto,   
		:ls_des_prodotto,   
		:ld_quan_ordine,
		:ld_prezzo_vendita,
		:ls_cod_versione,
		:ls_cod_iva,
		:ls_cod_tipo_movimento,
		:ll_anno_registrazione_ord_ven,
		:ll_num_registrazione_ord_ven,
		:ll_prog_riga_ord_ven,
		:ll_anno_reg_bol_acq,
		:ll_num_reg_bol_acq,
		:ll_prog_riga_bol_acq);	
			
	if sqlca.sqlcode <> 0 then
		as_error = "Errore durante la scrittura Dettagli Bolle Vendita.~n" + sqlca.sqlerrtext
		return -1
	end if
	
	if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente_a[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
		as_error = "Si è verificato un errore in fase di creazione destinazioni movimenti."
		return -1
	end if

	if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
		return 1
	end if
	
	update det_bol_ven
	set anno_reg_des_mov = :ll_anno_reg_des_mov,
		num_reg_des_mov = :ll_num_reg_des_mov
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione and
		prog_riga_bol_ven = :ll_prog_riga_bol_ven;
		
	if sqlca.sqlcode <> 0 then
		as_error = "Errore durante l'aggiornamento delle destinazioni movimenti.~n" + sqlca.sqlerrtext
		return -1
	end if

next
// -- fine dettaglio

uo_calcola_documento_euro luo_calcolo
luo_calcolo = create uo_calcola_documento_euro

if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_ven", as_error) <> 0 then
	destroy luo_calcolo
	return -1
end if

destroy luo_calcolo

al_anno_bol_ven = ll_anno_registrazione
al_num_bol_ven = ll_num_registrazione
return 0
end function

public function integer uof_controlla_ordine_in_fattura (integer fi_anno_fattura, long fl_num_fattura, integer fi_anno_ordine, long fl_num_ordine, long fl_riga_ordine, ref string fs_errore);long ll_count

//controllo se nel dettaglio della fattura esiste già la riga di ordine che si sta evadendo
//se si segnala l'errore all'operatore
select count(*)
into :ll_count
from det_fat_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_fattura and
			num_registrazione=:fl_num_fattura and
			anno_reg_ord_ven=:fi_anno_ordine and
			num_reg_ord_ven=:fl_num_ordine and
			prog_riga_ord_ven=:fl_riga_ordine;

if sqlca.sqlcode<0 then
	fs_errore = "Errore controllo esistenza riga ordine in fattura: "+sqlca.sqlerrtext
	return -1
end if

if ll_count>0 then
	fs_errore = "La riga di ordine "+string(fi_anno_ordine)+"/"+string(fl_num_ordine)+"/"+string(fl_riga_ordine)+" già esiste nella fattura: doppio inserimento!"
	return -1
end if

//se arrivi fin qui prosegui
return 0
end function

public function integer uof_controlla_ordine_in_spedizione (integer fi_anno_ordine, long fl_num_ordine, long fl_riga_ordine, decimal fd_qta_da_spedire, ref string fs_errore);decimal ld_quan_in_spedizione, ld_quan_evasa, ld_quan_ordine
string ls_riga_ordine

//controllo se si sta speendo una quantità che, sommata a quella già evasa e a quella già in spedizione, supera la quantità ordinata!!!!
//questo errore causa la evasione di una quantità superiore che di conseguenza fa diventare negativa la quantità evasa
//PROBLEMA DI RIGHE ORDINE DOPPIE PRESENTI IN FATTURA!!!!!!


ls_riga_ordine = string(fi_anno_ordine)+"/"+string(fl_num_ordine)+"/"+string(fl_riga_ordine)

if isnull(fd_qta_da_spedire) or fd_qta_da_spedire=0 then return 0

//quantità ordine per la riga di ordine ------------------------------------------------------------------------------------------
select quan_ordine
into :ld_quan_ordine
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_ordine and
			num_registrazione=:fl_num_ordine and
			prog_riga_ord_ven=:fl_riga_ordine and
			quan_ordine>0;

if sqlca.sqlcode<0 then
	fs_errore = "Errore controllo quantità evasa riga ordine "+ls_riga_ordine+": "+sqlca.sqlerrtext
	return -1
end if
if isnull(ld_quan_ordine) or ld_quan_ordine=0 then return 0


//quantità in evasione/spedizione per la riga di ordine ---------------------------------------------------------------------------
select sum(quan_in_evasione)
into :ld_quan_in_spedizione
from evas_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_ordine and
			num_registrazione=:fl_num_ordine and
			prog_riga_ord_ven=:fl_riga_ordine and
			quan_in_evasione is not null and
			cod_utente=:s_cs_xx.cod_utente;

if sqlca.sqlcode<0 then
	fs_errore = "Errore controllo quantità in spedizione riga ordine "+ls_riga_ordine+": "+sqlca.sqlerrtext
	return -1
end if
if isnull(ld_quan_in_spedizione) then ld_quan_in_spedizione=0


//quantità evasa per la riga di ordine -------------------------------------------------------------------------------------
select sum(quan_evasa)
into :ld_quan_evasa
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_ordine and
			num_registrazione=:fl_num_ordine and
			prog_riga_ord_ven=:fl_riga_ordine and
			quan_evasa is not null;

if sqlca.sqlcode<0 then
	fs_errore = "Errore controllo quantità evasa riga ordine "+ls_riga_ordine+": "+sqlca.sqlerrtext
	return -1
end if
if isnull(ld_quan_evasa) then ld_quan_evasa=0


if fd_qta_da_spedire + ld_quan_evasa + ld_quan_in_spedizione > ld_quan_ordine then
	fs_errore = "Attenzione: Spedizione di una quantità ("+string(fd_qta_da_spedire,"###,###,##0.00")+") "+&
								"che sommata a quello già spedita ("+string(ld_quan_in_spedizione,"###,###,##0.00")+")"+&
									" e già evasa ("+string(ld_quan_evasa,"###,###,##0.00")+") "+&
									"supera la quantità dell'ordine ("+string(ld_quan_ordine,"###,###,##0.00")+") - (Riga:"+ls_riga_ordine+"). Proseguire?"
	return 200
end if

//se arrivi fin qui prosegui
return 0
end function

public function integer uof_crea_bolla_acquisto (string as_cod_fornitore, string as_cod_tipo_bol_acq, string as_cod_deposito_partenza, string as_cod_deposito_arrivo, s_det_riga astr_righe[], ref long al_anno_bol_acq, ref long al_num_bol_acq, ref string as_error);/**
 * stefanop
 * 29/03/2012
 *
 * Crea una bolla di acquisto.
 * I depositi di partenza e di arrivano saranno usati nei dettagli per creare successivamente i movimenti
 * di magazzino
 **/
 
 
 string					ls_cod_tipo_bol_acq, ls_cod_operatore,ls_errore, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
							ls_cod_banca_clien_for, ls_cod_tipo_det_acq, ls_cod_iva, ls_flag_tipo_det_acq, ls_cod_tipo_movimento,ls_cod_misura_mag, &
							ls_cod_misura, ls_flag_decimali, ls_des_prodotto, ls_nota_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_cliente_a[], &
							ls_cod_lotto[], ls_cod_fornitore_stock[], ls_cod_misura_listini, ls_cod_tipo_det_rif
							
 long						ll_anno_bolla_acq, ll_num_bolla_acq, ll_i, ll_prog_riga_bolla_acq, ll_progr_stock[],ll_anno_reg_des_mov, ll_num_reg_des_mov
 
 decimal{4}			ld_sconto, ld_fat_conversione, ld_cambio_acq, ld_prezzo_acquisto, ld_sconto_1, ld_fat_conversione_listini
 
 datetime				ldt_data_registrazione, ldt_data_esenzione_iva, ldt_data_stock[]
 boolean lb_riga_prodotto
 
 uo_calcola_documento_euro luo_calcolo
 

ll_anno_bolla_acq = f_anno_esercizio()
ldt_data_registrazione = datetime(today(), 00:00:00)
setnull(ls_cod_operatore)


//----------------------------------------------
ld_cambio_acq = 1
//----------------------------------------------


// Movimenti
select cod_tipo_det_acq, cod_tipo_det_acq_rif
into :ls_cod_tipo_det_acq, :ls_cod_tipo_det_rif
from tab_tipi_bol_acq
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_bol_acq = :as_cod_tipo_bol_acq;
		 
if sqlca.sqlcode <> 0 or isnull(ls_cod_tipo_det_acq) then
	as_error = "Errore durante la lettura dettagli dalla tabella Tipi Bolle Acquisto"
	return -1
end if

select flag_tipo_det_acq,
		cod_tipo_movimento
into :ls_flag_tipo_det_acq,
	  :ls_cod_tipo_movimento
from tab_tipi_det_acq
where cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_tipo_det_acq = :ls_cod_tipo_det_acq;

if sqlca.sqlcode = -1 then
	as_error =  "Si è verificato un errore in fase di lettura tipi dettaglio."
	return -1
end if
// ----

// -- Operatore
if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	select cod_operatore
	into :ls_cod_operatore
	from tab_operatori_utenti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente = :s_cs_xx.cod_utente and
			flag_default = 'S';
			
	if sqlca.sqlcode = -1 then
		as_error = "Errore durante l'Estrazione dell'Operatore di Default"
		return -1
	end if
end if	

// -- Fornitore
select cod_deposito,
	cod_valuta,
	cod_tipo_listino_prodotto,
	cod_pagamento,
	sconto,
	cod_banca_clien_for,
	cod_iva,
	data_esenzione_iva
into :ls_cod_deposito[1],
	:ls_cod_valuta,
	:ls_cod_tipo_listino_prodotto,
	:ls_cod_pagamento,
	:ld_sconto,
	:ls_cod_banca_clien_for,
	:ls_cod_iva,
	:ldt_data_esenzione_iva
from anag_fornitori
where cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_fornitore = :as_cod_fornitore;
 
//if sqlca.sqlcode = 0 then f_cambio_acq(ls_cod_valuta, ldt_data_registrazione)
if ls_cod_iva = "" and (ldt_data_esenzione_iva > ldt_data_registrazione and not isnull(ldt_data_esenzione_iva)) then setnull(ls_cod_iva)
// ----
 
// Calcolo progressivo
select max(num_bolla_acq)
into :ll_num_bolla_acq
from tes_bol_acq
where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_bolla_acq = :ll_anno_bolla_acq;
		 
if sqlca.sqlcode < 0 then
	as_error = "Errore durante il calcolo del progressivo.~r~n" + sqlca.sqlerrtext
	return -1
elseif sqlca.sqlcode = 100 or isnull(ll_num_bolla_acq) then
	ll_num_bolla_acq = 0
end if

ll_num_bolla_acq++
// ----

INSERT INTO tes_bol_acq (
	cod_azienda,   
	anno_bolla_acq,   
	num_bolla_acq,   
	cod_fornitore,   
	cod_cliente,   
	cod_tipo_bol_acq,   
	data_registrazione,   
	cod_operatore,   
	cod_fil_fornitore,   
	cod_deposito,   
	cod_ubicazione,   
	cod_valuta,   
	cambio_acq,   
	cod_tipo_listino_prodotto,   
	cod_pagamento,   
	cod_banca_clien_for,   
	totale_val_bolla,   
	flag_movimenti,   
	flag_riorganizzabile,   
	cod_banca,   
	data_bolla,   
	sconto,   
	flag_blocco,   
	flag_gen_fat,   
	flag_doc_suc_tes,   
	flag_doc_suc_pie,   
	flag_st_note_tes,   
	flag_st_note_pie,   
	tot_merci,   
	tot_spese_trasporto,   
	tot_spese_imballo,   
	tot_spese_bolli,   
	tot_spese_varie,   
	tot_spese_cassa,   
	tot_sconti_commerciali,   
	tot_val_bol_acq,   
	tot_valuta_bol_acq,   
	importo_iva,   
	imponibile_iva,   
	importo_valuta_iva,   
	imponibile_valuta_iva,   
	num_bolla_fornitore
)  VALUES ( 
	:s_cs_xx.cod_azienda,   
	:ll_anno_bolla_acq,   
	:ll_num_bolla_acq,   
	:as_cod_fornitore,
	null, // cod_cliente,   
	:as_cod_tipo_bol_acq,
	:ldt_data_registrazione,   
	:ls_cod_operatore,   
	null, // cod_fil_fornitore,   
	:as_cod_deposito_partenza,   
	null, // cod_ubicazione,   
	:ls_cod_valuta,   
	:ld_cambio_acq,		//null, // cambio_acq,   
	:ls_cod_tipo_listino_prodotto,   
	:ls_cod_pagamento,   
	:ls_cod_banca_clien_for,   
	0, // totale_val_bolla,   
	'N', // flag_movimenti,   
	'S', // flag_riorganizzabile,   
	null, // cod_banca,   
	:ldt_data_registrazione,   
	:ld_sconto,   
	'N', // flag_blocco   
	'N', //	flag_gen_fat,   
	'I', //	flag_doc_suc_tes,   
	'I', //	flag_doc_suc_pie,   
	'I', //	flag_st_note_tes,   
	'I', //	flag_st_note_pie,   
	0, //	tot_merci,   
	0, //	tot_spese_trasporto,   
	0, //	tot_spese_imballo,   
	0, //	tot_spese_bolli,   
	0, //	tot_spese_varie,   
	0, //	tot_spese_cassa,   
	0, //	tot_sconti_commerciali,   
	0, //	tot_val_bol_acq,   
	0, //	tot_valuta_bol_acq,   
	0, //	importo_iva,   
	0, //	imponibile_iva,   
	0, //	importo_valuta_iva,   
	0, //	imponibile_valuta_iva,   
	null);  //	num_bolla_fornitore);
	
if sqlca.sqlcode < 0 then
	as_error = "Errore durante la creazione della testata bolla.~r~n" + sqlca.sqlerrtext
	return -1
end if

// -- DETTAGLIO
ll_prog_riga_bolla_acq = 0

for ll_i = 1 to upperbound(astr_righe)
	
	ll_prog_riga_bolla_acq += 10
	
	// Se c'è il prodotto allora controllo il prezzo ed eventuali pippe
	if g_str.isnotempty(astr_righe[ll_i].cod_prodotto) then	
		select cod_misura_mag,
				cod_misura_acq,
				fat_conversione_acq,
				flag_decimali,
				cod_iva,
				des_prodotto
		into   :ls_cod_misura_mag,
				:ls_cod_misura,
				:ld_fat_conversione,
				:ls_flag_decimali,
				:ls_cod_iva,
				:ls_des_prodotto
		from anag_prodotti
		where cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_prodotto = :astr_righe[ll_i].cod_prodotto;
			
		if ld_fat_conversione = 0 then ld_fat_conversione = 1
		
		// f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_bolla, ls_cod_tipo_det_acq)
		uof_listini_fornitori(		ls_cod_tipo_listino_prodotto, as_cod_fornitore, ls_cod_valuta, ld_cambio_acq,  astr_righe[ll_i].cod_prodotto, astr_righe[ll_i].quan_ordine, ldt_data_registrazione, ls_cod_tipo_det_acq, &
										ld_prezzo_acquisto, ld_sconto_1, ld_fat_conversione_listini, ls_cod_misura_listini)
		
		select nota_prodotto  
		into :ls_nota_prodotto  
		from anag_prodotti_note  
		where cod_azienda = :s_cs_xx.cod_azienda AND  
				cod_prodotto = :astr_righe[ll_i].cod_prodotto AND
				anag_prodotti_note.cod_nota_prodotto = (
					select stringa  
					from parametri_azienda  
					where cod_azienda = :s_cs_xx.cod_azienda AND  
					parametri_azienda.flag_parametro = 'S'AND  
					parametri_azienda.cod_parametro = 'CNP');
		
		if sqlca.sqlcode <> 0 then setnull(ls_nota_prodotto)
		
		// Deposito
		ls_cod_deposito[1] = as_cod_deposito_partenza
		setnull(ls_cod_ubicazione[1])
		setnull(ls_cod_cliente_a[1])
		setnull(ls_cod_lotto[1])
		setnull(ll_progr_stock[1])
		setnull(ldt_data_stock[1])
		setnull(ls_cod_fornitore_stock[1])
		
		ls_cod_deposito[2] = as_cod_deposito_arrivo
		setnull(ls_cod_ubicazione[2])
		setnull(ls_cod_cliente_a[2])
		setnull(ls_cod_lotto[2])
		setnull(ll_progr_stock[2])
		setnull(ldt_data_stock[2])
		setnull(ls_cod_fornitore_stock[2])
		// ----
		
		
		//------------------------------------------------------------------------------------
		//imposto il cod_iva della riga
		
		if ls_cod_iva="" or isnull(ls_cod_iva) then
		
			//lo leggo dal codice prodotto
			setnull(ls_cod_iva)
			select cod_iva
			into   :ls_cod_iva
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and 
							 cod_prodotto = :astr_righe[ll_i].cod_prodotto;
			
			if sqlca.sqlcode=100 or ls_cod_iva="" or isnull(ls_cod_iva) then
				//la leggo dal fornitore
				
				setnull(ls_cod_iva)
				select anag_fornitori.cod_iva
				into   :ls_cod_iva
				from   anag_fornitori
				where  cod_azienda = :s_cs_xx.cod_azienda and 
								 cod_fornitore = :as_cod_fornitore;
				
				if sqlca.sqlcode=100 or ls_cod_iva="" or isnull(ls_cod_iva) then
					//la leggo dalla tab_tipi_det_acq
					
					setnull(ls_cod_iva)
					select tab_tipi_det_acq.cod_iva  
					into   :ls_cod_iva  
					from   tab_tipi_det_acq  
					where  cod_azienda = :s_cs_xx.cod_azienda and  
										cod_tipo_det_acq = :ls_cod_tipo_det_acq;
										 
					if sqlca.sqlcode=100 or ls_cod_iva="" or isnull(ls_cod_iva) then
						setnull(ls_cod_iva)
					end if
				end if
			end if
		end if
		//-----------------------------------------------------------------------------------
		
		INSERT INTO det_bol_acq (
			cod_azienda,   
			anno_bolla_acq,   
			num_bolla_acq,   
			prog_riga_bolla_acq,   
			cod_tipo_det_acq,   
			cod_misura,   
			cod_prodotto,   
			des_prodotto,   
			quan_arrivata,   
			prezzo_acquisto,   
			fat_conversione,   
			sconto_1,
			cod_iva,   
			quan_fatturata,   
			val_riga,   
			cod_deposito,   
			cod_ubicazione,   
			cod_lotto,   
			data_stock,   
			cod_tipo_movimento, 
			flag_blocco,   
			nota_dettaglio
		) VALUES (
			:s_cs_xx.cod_azienda,   
			:ll_anno_bolla_acq,   
			:ll_num_bolla_acq,   
			:ll_prog_riga_bolla_acq,   
			:ls_cod_tipo_det_acq,   
			:ls_cod_misura_mag,   
			:astr_righe[ll_i].cod_prodotto,   
			:ls_des_prodotto, 
			:astr_righe[ll_i].quan_ordine,   
			:ld_prezzo_acquisto,		//0, //prezzo_acquisto,   
			:ld_fat_conversione,   
			:ld_sconto_1,		//null, //sconto_1,   
			:ls_cod_iva,   
			0, //quan_fatturata,   
			0, //al_riga,   
			:ls_cod_deposito[1],   
			null, //cod_ubicazione,   
			null, //cod_lotto,   
			null, //data_stock,   
			:ls_cod_tipo_movimento, 
			'N',   
			:ls_nota_prodotto);
			
		if sqlca.sqlcode <> 0 then
			as_error = "Errore durante l'inserimento del dettaglio bolla di acquisto."
			return -1
		end if
		
		
		if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, astr_righe[ll_i].cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente_a[], ls_cod_fornitore_stock[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
			as_error = "Si è verificato un errore in fase di creazione destinazioni movimenti."
			return -1
		end if
	
		if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, astr_righe[ll_i].cod_prodotto) = -1 then
			as_error = "Si è verificato un errore in verifica destinazioni movimenti."
			return -1
		end if
		
		update det_bol_acq
		set anno_reg_des_mov = :ll_anno_reg_des_mov,
			num_reg_des_mov = :ll_num_reg_des_mov
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_bolla_acq = :ll_anno_bolla_acq and
			num_bolla_acq = :ll_num_bolla_acq and
			prog_riga_bolla_acq = :ll_prog_riga_bolla_acq;
			
		if sqlca.sqlcode <> 0 then
			as_error = "Errore durante l'aggiornamento delle destinazioni movimenti.~n" + sqlca.sqlerrtext
			return -1
		end if
	else
		// E' una riga RIF
		INSERT INTO det_bol_acq (
			cod_azienda,   
			anno_bolla_acq,   
			num_bolla_acq,   
			prog_riga_bolla_acq,   
			cod_tipo_det_acq,   
			cod_misura,   
			cod_prodotto,   
			des_prodotto,   
			quan_arrivata,   
			prezzo_acquisto,   
			fat_conversione,   
			sconto_1,
			cod_iva,   
			quan_fatturata,   
			val_riga,   
			cod_deposito,   
			cod_ubicazione,   
			cod_lotto,   
			data_stock,   
			cod_tipo_movimento, 
			flag_blocco,   
			nota_dettaglio
		) VALUES (
			:s_cs_xx.cod_azienda,   
			:ll_anno_bolla_acq,   
			:ll_num_bolla_acq,   
			:ll_prog_riga_bolla_acq,   
			:ls_cod_tipo_det_rif,   
			null,   
			null,   
			:astr_righe[ll_i].des_riga, 
			1,   
			0,		//0, //prezzo_acquisto,   
			1,   
			0,		//null, //sconto_1,   
			:ls_cod_iva,   
			0, //quan_fatturata,   
			0, //al_riga,   
			:ls_cod_deposito[1],   
			null, //cod_ubicazione,   
			null, //cod_lotto,   
			null, //data_stock,   
			null,  // cod tipo movimento
			'N',   
			null);
			
		if sqlca.sqlcode <> 0 then
			as_error = "Errore durante l'inserimento del dettaglio bolla di acquisto."
			return -1
		end if
	end if
	
next

al_anno_bol_acq = ll_anno_bolla_acq
al_num_bol_acq = ll_num_bolla_acq

//----------------------------------------------------------------------
//adesso calcolo il documento
luo_calcolo = create uo_calcola_documento_euro

if luo_calcolo.uof_calcola_documento(al_anno_bol_acq, al_num_bol_acq, "bol_acq", as_error) <> 0 then
	destroy luo_calcolo
	return -1
end if

destroy luo_calcolo
//-----------------------------------------------------------------------

return 0
end function

public function integer uof_ass_ord_ven (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, string as_cod_tipo_det_ven, string as_cod_deposito, string as_cod_ubicazione, string as_cod_prodotto, double ad_quan_ordine, double ad_quan_commessa, double ad_quan_evasa, longlong al_prog_sessione);//Donato 13/04/2012
// creata questa funzione come copia della f_ass_ord_ven, con l'aggiunta di un argomento progr_sessione, generata dal clic sul pulsante SPEDISCI della finestra evasione ordini barcode

integer				li_return, li_ret_sped
string					ls_flag_tipo_det_ven, ls_ubicazione, ls_sql_stringa, ls_cod_ubicazione, ls_cod_lotto, ls_deposito, &
						ls_cod_deposito, ls_flag_negativo,ls_cod_prodotto_raggruppato, ls_errore
						
dec{4}				ld_quan_residua, ld_giacenza_stock, ld_quan_assegnata, ld_quan_in_spedizione, &
						ld_quan_in_evasione, ld_det_ord_ven_quan_ordine, ld_det_ord_ven_quan_in_evasione, ld_quan_raggruppo, ldd_quan_da_spedire, &
						ld_quan_assegnata_update
						
long					ll_prog_stock, ll_prog_riga

datetime				ldt_data_stock

boolean				lb_evasione=false, lb_parziale

uo_generazione_documenti					luo_gen_doc


li_return = 0

select flag
into   :ls_flag_negativo
from	 parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'CMN' and
		 flag_parametro = 'F';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura parametro CMN da parametri_azienda: " + sqlca.sqlerrtext,stopsign!)
	ls_flag_negativo = "N"
elseif sqlca.sqlcode = 100 or isnull(ls_flag_negativo) then
	ls_flag_negativo = "N"
end if

select flag_tipo_det_ven
into   :ls_flag_tipo_det_ven
from   tab_tipi_det_ven
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_tipo_det_ven = :as_cod_tipo_det_ven;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di ricerca del tipo dettaglio "+as_cod_tipo_det_ven+" nei tipi dettagli di vendita.", exclamation!, ok!)
	rollback;
	return 0
end if

if ls_flag_tipo_det_ven = "M" then
	declare cu_stock dynamic cursor for sqlsa;
	
	if not isnull(as_cod_ubicazione) and as_cod_ubicazione <> "" then
		if as_cod_ubicazione = "%" then
			ls_ubicazione = " and cod_ubicazione like '" + as_cod_ubicazione + "' "
		else
			ls_ubicazione = " and cod_ubicazione = '" + as_cod_ubicazione + "' "
		end if
	else
		ls_ubicazione = ""
	end if
	
	if not isnull(as_cod_deposito) and as_cod_deposito <> "" then
		if as_cod_deposito = "%" then
			ls_deposito = " and cod_deposito like '" + as_cod_deposito + "' "
		else
			ls_deposito = " and cod_deposito = '" + as_cod_deposito + "' "
		end if
	else
		ls_deposito = ""
	end if
	
	ls_sql_stringa = "select cod_deposito, cod_ubicazione, cod_lotto, data_stock, prog_stock, giacenza_stock, quan_assegnata, quan_in_spedizione from stock where cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_stato_lotto ='D' and cod_prodotto = '" + as_cod_prodotto + "' " + ls_deposito + ls_ubicazione
	
	if ls_flag_negativo <> "S" then
		ls_sql_stringa += " and giacenza_stock - (quan_assegnata + quan_in_spedizione) > 0 "
	end if
	
	ls_sql_stringa += " order by cod_lotto, data_stock, prog_stock "
	
	prepare sqlsa from :ls_sql_stringa;
	open dynamic cu_stock;
	
	if ad_quan_commessa <> 0 and not isnull(ad_quan_commessa) then
		ld_quan_residua = ad_quan_commessa - ad_quan_evasa
	else
		ld_quan_residua = ad_quan_ordine - ad_quan_evasa
	end if
	
	do while 0 = 0
		fetch cu_stock into :ls_cod_deposito, :ls_cod_ubicazione, :ls_cod_lotto, :ldt_data_stock, :ll_prog_stock, :ld_giacenza_stock, :ld_quan_assegnata, :ld_quan_in_spedizione;
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura del cursore degli stock. Prodotto=" + as_cod_prodotto + ". Dettaglio errore:"+sqlca.sqlerrtext, exclamation!, ok!)
				close cu_stock;
				rollback;
				return 0
			end if

		if sqlca.sqlcode <> 0 then exit

		if ld_quan_residua > 0 then
			if ld_quan_residua >= ld_giacenza_stock - (ld_quan_assegnata + ld_quan_in_spedizione) and ls_flag_negativo <> "S" then
								
				// stefanop 21/09/2012: eseguo prima la selezione della quantità altrimenti in alcuni casi da errore di ,
				ld_quan_assegnata_update = 0
				
				select quan_assegnata
				into :ld_quan_assegnata_update
				from stock
				where cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :as_cod_prodotto and 
						 cod_deposito = :ls_cod_deposito and
						 cod_ubicazione = :ls_cod_ubicazione and
						 cod_lotto = :ls_cod_lotto and
						 data_stock = :ldt_data_stock and
						 prog_stock = :ll_prog_stock;
						 
				if sqlca.sqlcode < 0 then
					g_mb.error("Errore durante il recupero della quan_assegnata nella tabella STOCK", sqlca)
					rollback;
					return 0
				elseif sqlca.sqlcode = 100 or isnull(ld_quan_assegnata) then
					ld_quan_assegnata_update = 0
				end if
				// ----

				ld_quan_in_evasione = ld_quan_in_evasione + (ld_giacenza_stock - (ld_quan_assegnata + ld_quan_in_spedizione))
				ld_quan_residua = ld_quan_residua - (ld_giacenza_stock - (ld_quan_assegnata + ld_quan_in_spedizione))
				ld_quan_assegnata_update = ld_quan_assegnata_update + (ld_giacenza_stock - (ld_quan_assegnata + ld_quan_in_spedizione))

				update stock  
				set stock.quan_assegnata = :ld_quan_assegnata_update
				 where stock.cod_azienda = :s_cs_xx.cod_azienda and
						 stock.cod_prodotto = :as_cod_prodotto and 
						 stock.cod_deposito = :ls_cod_deposito and
						 stock.cod_ubicazione = :ls_cod_ubicazione and
						 stock.cod_lotto = :ls_cod_lotto and
						 stock.data_stock = :ldt_data_stock and
						 stock.prog_stock = :ll_prog_stock;

				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento tabella stock. Prodotto=" + as_cod_prodotto + " Deposito = "+ ls_cod_deposito + " Ubic=" + ls_cod_ubicazione + " Lotto=" + ls_cod_lotto + "Data=" + string(ldt_data_stock, "dd/mm/yyyy") + " Prog=" + string(ll_prog_stock)  + ". Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
					close cu_stock;
					rollback;
					return 0
				end if

				select max(prog_riga)
				into	 :ll_prog_riga
				from	 evas_ord_ven
				where  evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and 
						 evas_ord_ven.anno_registrazione = :al_anno_registrazione and
						 evas_ord_ven.num_registrazione = :al_num_registrazione and
						 evas_ord_ven.prog_riga_ord_ven = :al_prog_riga_ord_ven;

				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura evas_ord_ven. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
					rollback;
					close cu_stock;
					return 0
				end if
				
				if isnull(ll_prog_riga) then
					ll_prog_riga = 1
				else
					ll_prog_riga ++
				end if
				
				//controlla se stai spedendo più della quantita ordine, tenendo conto di quanto già in spedizione ed evaso ---------------------------------------------
				ldd_quan_da_spedire = ld_giacenza_stock - (ld_quan_assegnata + ld_quan_in_spedizione)
				
				insert into evas_ord_ven  
								(cod_azienda,   
								 anno_registrazione,   
								 num_registrazione,   
								 prog_riga_ord_ven,   
								 prog_riga,
								 cod_deposito,   
								 cod_ubicazione,   
								 cod_lotto,   
								 data_stock,   
								 prog_stock,   
								 cod_prodotto,   
								 quan_in_evasione,
								 cod_utente,
								 prog_sessione)  
				values      (:s_cs_xx.cod_azienda,   
								 :al_anno_registrazione,   
								 :al_num_registrazione,   
								 :al_prog_riga_ord_ven,   
								 :ll_prog_riga,
								 :ls_cod_deposito,   
								 :ls_cod_ubicazione,   
								 :ls_cod_lotto,   
								 :ldt_data_stock,   
								 :ll_prog_stock,   
								 :as_cod_prodotto,   
								 :ldd_quan_da_spedire,
								 :s_cs_xx.cod_utente,
								 :al_prog_sessione);
				//prima era :ld_giacenza_stock - (:ld_quan_assegnata + :ld_quan_in_spedizione)
				
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di inserimento in tabella evas_ord_ven. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
					rollback;
					close cu_stock;
					return 0
				end if
			else
				ld_quan_in_evasione = ld_quan_in_evasione + ld_quan_residua

				update stock  
					set stock.quan_assegnata = stock.quan_assegnata + :ld_quan_residua  
				 where stock.cod_azienda = :s_cs_xx.cod_azienda and
						 stock.cod_prodotto = :as_cod_prodotto and 
						 stock.cod_deposito = :ls_cod_deposito and
						 stock.cod_ubicazione = :ls_cod_ubicazione and
						 stock.cod_lotto = :ls_cod_lotto and
						 stock.data_stock = :ldt_data_stock and
						 stock.prog_stock = :ll_prog_stock;

				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento tabella stock. Prodotto=" + as_cod_prodotto + " Deposito = "+ ls_cod_deposito + " Ubic=" + ls_cod_ubicazione + " Lotto=" + ls_cod_lotto + "Data=" + string(ldt_data_stock, "dd/mm/yyyy") + " Prog=" + string(ll_prog_stock)  + ". Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
					close cu_stock;
					rollback;
					return 0
				end if

				select max(prog_riga)
				into	 :ll_prog_riga
				from	 evas_ord_ven
				where  evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and 
						 evas_ord_ven.anno_registrazione = :al_anno_registrazione and
						 evas_ord_ven.num_registrazione = :al_num_registrazione and
						 evas_ord_ven.prog_riga_ord_ven = :al_prog_riga_ord_ven;

				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura evas_ord_ven. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
					rollback;
					close cu_stock;
					return 0
				end if
				
				if isnull(ll_prog_riga) then
					ll_prog_riga = 1
				else
					ll_prog_riga ++
				end if
				
				
				insert into evas_ord_ven  
								(cod_azienda,   
								 anno_registrazione,   
								 num_registrazione,   
								 prog_riga_ord_ven,   
								 prog_riga,
								 cod_deposito,   
								 cod_ubicazione,   
								 cod_lotto,   
								 data_stock,   
								 prog_stock,   
								 cod_prodotto,   
								 quan_in_evasione,
								 cod_utente,
								 prog_sessione)  
				values      (:s_cs_xx.cod_azienda,   
								 :al_anno_registrazione,   
								 :al_num_registrazione,   
								 :al_prog_riga_ord_ven,   
								 :ll_prog_riga,
								 :ls_cod_deposito,   
								 :ls_cod_ubicazione,   
								 :ls_cod_lotto,   
								 :ldt_data_stock,   
								 :ll_prog_stock,   
								 :as_cod_prodotto,   
								 :ld_quan_residua,
								 :s_cs_xx.cod_utente,
								 :al_prog_sessione); 
	
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di inserimento in tabella evas_ord_ven. Dettaglio errore:"+ sqlca.sqlerrtext, exclamation!, ok!)
					rollback;
					close cu_stock;
					return 0
				end if
				ld_quan_residua = 0
				exit
			end if
		end if
	loop
	close cu_stock;

	if ld_quan_residua <> 0 and &
		ld_quan_residua <> ad_quan_ordine - ad_quan_evasa then
		lb_parziale = true
	end if

	if ld_quan_in_evasione > 0 then

		select quan_ordine, quan_in_evasione
		into   :ld_det_ord_ven_quan_ordine, :ld_det_ord_ven_quan_in_evasione
		from   det_ord_ven
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :al_anno_registrazione and  
				 det_ord_ven.num_registrazione = :al_num_registrazione and
				 det_ord_ven.prog_riga_ord_ven = :al_prog_riga_ord_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di ricerca della riga "+ string(al_prog_riga_ord_ven) +"dettaglio ordine. Dettaglio errore " + sqlca.sqlerrtext,exclamation!, ok!)
			rollback;
			return 0
		end if
		
		if ld_det_ord_ven_quan_ordine - ld_det_ord_ven_quan_in_evasione < ad_quan_ordine then
			update anag_prodotti  
				set quan_impegnata = quan_impegnata - (:ld_det_ord_ven_quan_ordine - :ld_det_ord_ven_quan_in_evasione),
					 quan_assegnata = quan_assegnata + :ld_quan_in_evasione
			 where cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :as_cod_prodotto;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento anagrafica di magazzino del prodotto "+as_cod_prodotto+". Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
				rollback;
				return 0
			end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :as_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
				
				ld_quan_raggruppo = f_converti_qta_raggruppo(as_cod_prodotto, (ld_det_ord_ven_quan_ordine - ld_det_ord_ven_quan_in_evasione), "M")
	
				update anag_prodotti  
					set quan_impegnata = quan_impegnata - :ld_quan_raggruppo
				 where cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_prodotto = :ls_cod_prodotto_raggruppato;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento anagrafica di magazzino del prodotto "+as_cod_prodotto+". Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
					rollback;
					return 0
				end if
				
			end if
					 
		else
			update anag_prodotti  
				set quan_impegnata = quan_impegnata - :ld_quan_in_evasione,
					 quan_assegnata = quan_assegnata + :ld_quan_in_evasione
			 where cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :as_cod_prodotto;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento anagrafica di magazzino del prodotto "+as_cod_prodotto+". Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
				rollback;
				return 0
			end if
			
			// enme 08/1/2006 gestione prodotto raggruppato
			setnull(ls_cod_prodotto_raggruppato)
			
			select cod_prodotto_raggruppato
			into   :ls_cod_prodotto_raggruppato
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :as_cod_prodotto;
					 
			if not isnull(ls_cod_prodotto_raggruppato) then
				
				ld_quan_raggruppo = f_converti_qta_raggruppo(as_cod_prodotto, ld_quan_in_evasione, "M")
	
				update anag_prodotti  
					set quan_impegnata = quan_impegnata - :ld_quan_raggruppo
				 where cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_prodotto = :ls_cod_prodotto_raggruppato;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento anagrafica di magazzino del prodotto "+as_cod_prodotto+". Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
					rollback;
					return 0
				end if
			
			end if			
		end if
		
		update det_ord_ven
			set quan_in_evasione = quan_in_evasione + :ld_quan_in_evasione
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 anno_registrazione = :al_anno_registrazione and  
				 num_registrazione = :al_num_registrazione and
				 prog_riga_ord_ven = :al_prog_riga_ord_ven;

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento del dettaglio ordine alla riga "+string(al_prog_riga_ord_ven)+". Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
			rollback;
			return 0
		end if
		
		lb_evasione = true
	end if
else
	update det_ord_ven
		set det_ord_ven.quan_in_evasione = (:ad_quan_ordine - :ad_quan_evasa)
	 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 det_ord_ven.anno_registrazione = :al_anno_registrazione and  
			 det_ord_ven.num_registrazione = :al_num_registrazione and
			 det_ord_ven.prog_riga_ord_ven = :al_prog_riga_ord_ven;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento del dettaglio ordine alla riga "+string(al_prog_riga_ord_ven)+". Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
		rollback;
		return 0
	end if

	ld_quan_in_evasione = ad_quan_ordine - ad_quan_evasa
	lb_evasione = true

	select max(prog_riga)
	into	 :ll_prog_riga
	from	 evas_ord_ven
	where  evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 evas_ord_ven.anno_registrazione = :al_anno_registrazione and
			 evas_ord_ven.num_registrazione = :al_num_registrazione and
			 evas_ord_ven.prog_riga_ord_ven = :al_prog_riga_ord_ven;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase ricerca del max(prog_riga) della tabella evas_ord_ven . Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
		rollback;
		return 0
	end if
	
	if isnull(ll_prog_riga) then
		ll_prog_riga = 1
	else
		ll_prog_riga ++
	end if
	
	
	insert into evas_ord_ven  
					(cod_azienda,   
					 anno_registrazione,   
					 num_registrazione,   
					 prog_riga_ord_ven,   
					 prog_riga,
					 cod_deposito,   
					 cod_ubicazione,   
					 cod_lotto,   
					 data_stock,   
					 prog_stock,   
					 cod_prodotto,   
					 quan_in_evasione,
					 cod_utente,
					 prog_sessione)  
	values      (:s_cs_xx.cod_azienda,   
					 :al_anno_registrazione,   
					 :al_num_registrazione,   
					 :al_prog_riga_ord_ven,   
					 :ll_prog_riga,
					 :as_cod_deposito,   
					 :ls_cod_ubicazione,   
					 null,   
					 null,   
					 null,   
					 null,   
					 :ld_quan_in_evasione,
					 :s_cs_xx.cod_utente,
					 :al_prog_sessione);

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di inserimento nella tabella evas_ord_ven. Dettaglio errore " + sqlca.sqlerrtext, exclamation!, ok!)
		rollback;
		return 0
	end if
end if				

if lb_evasione then
	li_return = 1				// tutto OK
else
	li_return = 3				// nessuno stock trovato
end if

if lb_parziale then
	li_return = 2				// pariziale
end if

return li_return

end function

public function integer uof_crea_aggiungi_fattura_sessione (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_fat_ven, ref long fl_num_reg_fat_ven, longlong fl_prog_sessione, ref string fs_errore);boolean			lb_riferimento, lb_aggiungi=false

string				ls_cod_tipo_fat_ven, ls_nota_testata, ls_cod_cliente[1], ls_cod_tipo_ord_ven, ls_cod_operatore, ls_rag_soc_1, &
					ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione[1],&
					ls_cod_deposito[1], ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, &
					ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_nota_piede, ls_cod_banca, &
					ls_cod_tipo_det_ven, ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, &
					ls_flag_tipo_det_ven, ls_cod_tipo_analisi_fat, ls_cod_tipo_analisi_ord, ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, &
					ls_causale_trasporto, ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ls_num_ord_cliente, &
					ls_cod_lotto[1], ls_cod_tipo_movimento, ls_vostro_ordine, ls_vostro_ordine_data, ls_flag_evasione, ls_lire, ls_flag_sola_iva, &
					ls_cod_fornitore[], ls_causale_trasporto_tab,ls_cod_versione, ls_flag_abilita_rif_bol_ven, ls_cod_tipo_det_ven_rif_bol_ven, &
					ls_des_riferimento_ord_ven, ls_flag_rif_anno_ord_ven, ls_flag_rif_num_ord_ven, ls_flag_rif_data_ord_ven, ls_des_riferimento, &
					ls_cod_iva_rif, ls_note_dettaglio_rif, ls_db, ls_cod_tipo_fat_ven_dest, ls_cod_causale, ls_nota_piede_fattura, &
					ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det, ls_rif_packing_list, &
					ls_cod_tipo_det_ven_new, ls_cod_tipo_det_ven_ord, ls_cod_tipo_movimento_ord, ls_cod_tipo_movimento_new, &
					ls_cod_giro_consegna, ls_cod_deposito_giro, ls_temp, ls_cod_natura_intra
		 
integer			li_risposta

long				ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_anno_commessa, ll_num_commessa, &
					ll_progr_stock[1], ll_prog_riga_fat_ven, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga, ll_num_confezioni, &
					ll_num_pezzi_confezione
					
dec{6}			ld_cambio_ven, ld_quan_in_evasione, ld_prezzo_vendita, ld_fat_conversione_ven, ld_sconto_1, ld_sconto_2, ld_sconto_3, &
					ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_testata, &
					ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, ld_quan_ordine, ld_quan_evasa, &
					ld_tot_val_evaso, ld_tot_val_ordine, ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
					ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, ld_val_riga_sconto_4, &
					ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
					ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10, &
					ld_val_sconto_testata, ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quantita_um, ld_prezzo_um, &
					ld_quan_in_evasione_temp, ld_quan_evasa_temp
					
datetime			ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock[1], &
					ldt_vostro_ordine_data, ldt_ora_inizio_trasporto

uo_calcola_documento_euro luo_calcolo
uo_log_sistema				luo_log


li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)

guo_functions.uof_get_parametro_azienda( "IRF", ref il_incremento_riga)
if isnull(il_incremento_riga) or il_incremento_riga = 0 then
	il_incremento_riga = 10
end if

lb_riferimento = true
setnull(ls_cod_fornitore[1])
if not isnull(fl_anno_reg_fat_ven) and fl_anno_reg_fat_ven > 0 then 
	lb_aggiungi = true
	ll_anno_registrazione = fl_anno_reg_fat_ven
else
	ll_anno_registrazione = f_anno_esercizio()
end if
if lb_aggiungi and (fl_num_reg_fat_ven = 0 or isnull(fl_num_reg_fat_ven)) then
	fs_errore = "Manca l'indicazione del numero di fattura di destinazione"
	return -1
end if

select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and  
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la ricerca del tipo ordine di vendita in testata ordini di vendita.Dettaglio errore: " + sqlca.sqlerrtext
	return -1
end if

select cod_tipo_fat_ven, cod_tipo_analisi  
into :ls_cod_tipo_fat_ven, :ls_cod_tipo_analisi_ord
from tab_tipi_ord_ven  
where 
	cod_azienda = :s_cs_xx.cod_azienda and  
	cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la ricerca tipo fattura in tabella Tipi Ordini Vendita.Dettaglio errore: " + sqlca.sqlerrtext
	return -1
end if
	
// stefanop 08/06/2010: non carico il tipo bol ven se impostato: progetto 140
if isnull(this.is_cod_tipo_fat_ven) or this.is_cod_tipo_fat_ven = "" then
	// lascio quello di default dell'applicazione, 99% dei casi
	this.ib_change_det_ven = false
else
	
	if this.ib_change_det_ven then
		// recupero il dettaglio orignale che sarà sostituito con quello nuovo
		select cod_tipo_det_ven
		into :ls_cod_tipo_det_ven_ord
		from tab_tipi_fat_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
			
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante il recupero del dettaglio fattura.~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		// recupero il nuovo dettaglio
		select cod_tipo_det_ven
		into :ls_cod_tipo_det_ven_new
		from tab_tipi_fat_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_fat_ven = :is_cod_tipo_fat_ven;
			
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante il recupero del nuovo dettaglio bolla"
			return -1
		end if
	end if
	
	// MOV_MAG
	if this.ib_change_mov then
		// carico movimento default, mi serve per il confronto
		select cod_tipo_movimento
		into :ls_cod_tipo_movimento_ord
		from tab_tipi_det_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and 
			cod_tipo_det_ven = :ls_cod_tipo_det_ven_ord;
			
		if sqlca.sqlcode <> 0 then 
			fs_errore = "Errore durante la ricerca del tipo movimento."
			return -1
		end if
		
		// carico il nuovo movimento di magazzioni che sarà inserito
		select cod_tipo_movimento
		into :ls_cod_tipo_movimento_new
		from tab_tipi_det_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_det_ven = :ls_cod_tipo_det_ven_new;
			
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la ricerca del nuovo tipo movimento."
			return -1
		end if
	end if
	
	// la variabile viene impostata dalla finestra w_richiesta_dati_bolla
	ls_cod_tipo_fat_ven = this.is_cod_tipo_fat_ven
end if
// ----

if isnull(ls_cod_tipo_fat_ven) then
	fs_errore = "Nel tipo ordine manca l'indicazione del tipo fattura di destinazione; operazione annullata "
	return -1
end if
if lb_aggiungi then
	select cod_tipo_fat_ven
	into   :ls_cod_tipo_fat_ven_dest
	from   tes_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :fl_anno_reg_fat_ven and
			 num_registrazione = :fl_num_reg_fat_ven;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in ricerca tipo fattura di vendita in fattura di destinazione. Dettaglio: " + sqlca.sqlerrtext
		return -1
	end if
	
	if ls_cod_tipo_fat_ven <> ls_cod_tipo_fat_ven_dest then
		fs_errore = "Il tipo fattura deve coincidere con il tipo fattura di destinazione indicato nel tipo ordine: controllare il tipo fattura di destinazione nella tabella tipi ordini"
		return -1
	end if
end if

if not isnull(ls_cod_tipo_fat_ven) then
	select tes_ord_ven.cod_operatore,   
			 tes_ord_ven.data_registrazione,   
			 tes_ord_ven.cod_cliente,   
			 tes_ord_ven.cod_des_cliente,   
			 tes_ord_ven.rag_soc_1,   
			 tes_ord_ven.rag_soc_2,   
			 tes_ord_ven.indirizzo,   
			 tes_ord_ven.localita,   
			 tes_ord_ven.frazione,   
			 tes_ord_ven.cap,   
			 tes_ord_ven.provincia,   
			 tes_ord_ven.cod_deposito,   
			 tes_ord_ven.cod_ubicazione,   
			 tes_ord_ven.cod_valuta,   
			 tes_ord_ven.cambio_ven,   
			 tes_ord_ven.cod_tipo_listino_prodotto,   
			 tes_ord_ven.cod_pagamento,   
			 tes_ord_ven.sconto,   
			 tes_ord_ven.cod_agente_1,
			 tes_ord_ven.cod_agente_2,
			 tes_ord_ven.cod_banca,   
			 tes_ord_ven.num_ord_cliente,   
			 tes_ord_ven.data_ord_cliente,   
			 tes_ord_ven.cod_imballo,   
			 tes_ord_ven.aspetto_beni,
			 tes_ord_ven.peso_netto,
			 tes_ord_ven.peso_lordo,
			 tes_ord_ven.num_colli,
			 tes_ord_ven.cod_vettore,   
			 tes_ord_ven.cod_inoltro,   
			 tes_ord_ven.cod_mezzo,
			 tes_ord_ven.causale_trasporto,
			 tes_ord_ven.cod_porto,   
			 tes_ord_ven.cod_resa,   
			 tes_ord_ven.nota_testata,   
			 tes_ord_ven.nota_piede,   
			 tes_ord_ven.tot_val_evaso,
			 tes_ord_ven.tot_val_ordine,
			 tes_ord_ven.cod_banca_clien_for,
			 tes_ord_ven.cod_causale,
			 tes_ord_ven.flag_doc_suc_tes, 
			 tes_ord_ven.flag_doc_suc_pie,
			 tes_ord_ven.cod_giro_consegna,
			 tes_ord_ven.cod_natura_intra
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_cod_causale,
			 :ls_flag_doc_suc_tes, 
			 :ls_flag_doc_suc_pie,
			 :ls_cod_giro_consegna,
			 :ls_cod_natura_intra
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione and  
			 tes_ord_ven.num_registrazione = :fl_num_registrazione;

	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la lettura Testata Ordine Vendita.Dettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
	
	if ls_cod_natura_intra="" then setnull(ls_cod_natura_intra)
	
	//#####################################################################
	//se il giro consegna (quando specificato) ha un deposito diverso da quello origine ordine, allora il documento fiscale va creato in quel deposito
	if g_str.isnotempty(ls_cod_giro_consegna) then
		select cod_deposito
		into :ls_cod_deposito_giro
		from tes_giri_consegne
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_giro_consegna=:ls_cod_giro_consegna;
					
		if g_str.isnotempty(ls_cod_deposito_giro) and ls_cod_deposito_giro<>ls_cod_deposito[1] then
			//salvo il deposito origine ordine inn una variabile, mi serve per scriverlo nella log sistema
			ls_temp = ls_cod_deposito[1]
			ls_cod_deposito[1] = ls_cod_deposito_giro
			
			luo_log = create uo_log_sistema
			luo_log.uof_write_log_sistema_not_sqlca("DEP.CONS.EVASIONE", "Applicato deposito codice "+ls_cod_deposito_giro+" del  giro consegna codice "+ls_cod_giro_consegna+&
																						" al posto del deposito origine "+ls_temp+" in evasione ordine n. "+string(fl_anno_registrazione)+"/" + string(fl_num_registrazione) + &
																						" mediante fattura.")
			destroy luo_log
		end if
	end if
	//#####################################################################
	
	
	// stefanop: 18/07/2012 - l'operatore che inserisce è SEMPRE l'operatore collegato ad Apice
	// ------------------------------------------------------
	ls_cod_operatore = guo_functions.uof_get_operatore_utente()
	// ------------------------------------------------------

	select tab_tipi_fat_ven.cod_tipo_analisi,
			 tab_tipi_fat_ven.causale_trasporto,
			 tab_tipi_fat_ven.flag_abilita_rif_bol_ven,
			 tab_tipi_fat_ven.cod_tipo_det_ven_rif_bol_ven,
			 tab_tipi_fat_ven.des_riferimento_ord_ven,
			 tab_tipi_fat_ven.flag_rif_anno_ord_ven,
			 tab_tipi_fat_ven.flag_rif_num_ord_ven,
			 tab_tipi_fat_ven.flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi_fat,
			 :ls_causale_trasporto,
			 :ls_flag_abilita_rif_bol_ven,
			 :ls_cod_tipo_det_ven_rif_bol_ven,
			 :ls_des_riferimento_ord_ven,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_fat_ven  
	where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Si è verificato un errore nella lettura della Tabella Tipi Fatture.Dettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if
	
	ldt_data_registrazione = datetime(today())

	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if

	if not isnull(ls_num_ord_cliente)  and ls_cod_tipo_det_ven_rif_bol_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if

//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------	
	
	if ls_cod_tipo_det_ven_rif_bol_ven = "T" then
		if len(is_rif_packing_list) > 0 and not isnull(is_rif_packing_list) then ls_rif_packing_list = is_rif_packing_list + "~r~n"
		ls_nota_testata = ls_rif_packing_list + "Rif. ns. Ordine Nr. " + string(fl_anno_registrazione) + "/" + &
								string(fl_num_registrazione) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
	end if

		
	// ----- controllo lunghezza note per ASE
	if (upper(ls_db) = "SYBASE_ASE" or upper(ls_db) = "MSSQL") and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE


	if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
		ls_causale_trasporto = ls_causale_trasporto_tab
	end if

	select parametri_azienda.stringa
	into   :ls_lire
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LIR';

	if sqlca.sqlcode <> 0 then
		fs_errore = "Configurare il codice valuta per le Lire Italiane.Dettaglio errore: " + sqlca.sqlerrtext
		return -1
	end if

	select tab_pagamenti.sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if
	if lb_aggiungi then
		ldt_ora_inizio_trasporto = datetime(date("01/01/1900"), it_ora_inizio_trasporto)
		ll_num_registrazione = fl_num_reg_fat_ven
		
		select nota_piede
		into   :ls_nota_piede_fattura
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_reg_fat_ven and
		       num_registrazione = :fl_num_reg_fat_ven;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante ricercaTestata Bolle Vendita. Dettaglio errore " + sqlca.sqlerrtext
			return -1
		end if

		if isnull(ls_nota_piede_fattura) then ls_nota_piede_fattura = ""
		
		//QUESTO VALE SOLO PER ib_gibus = TRUE
		//17/06/2014 d'accordo con Alberto
		//in caso di aggiungi e modalità GIBUS se scrivi qualcosa nelle note accodala a quanto già presente
		//infatti se stai evadendo + ordini e questo è un ordine successivo al primo la variabile "is_nota_piede" è stata pulita
		//se invece provieni da AGGIUNGI A FATTURA ESISTENTE allora la variabile "is_nota_piede" potrebbe essere valorizzata
		//quindi in tal caso verrà accodata
		if ib_gibus and len(trim(is_nota_piede)) > 0 then
			//----------------------------------------------------------------------
			ls_nota_piede_fattura += "~r~n" + is_nota_piede
			//----------------------------------------------------------------------
		else
			if len(trim(ls_nota_piede_fattura)) > 0 then
				ls_nota_piede_fattura = ls_nota_piede_fattura
			else
				ls_nota_piede_fattura = ls_nota_piede_fattura
			end if
		end if
		
		
		// ----- controllo lunghezza note per ASE
		if (upper(ls_db) = "SYBASE_ASE" or upper(ls_db) = "MSSQL") and len(ls_nota_piede_fattura) > 255 then
			ls_nota_piede_fattura = left(ls_nota_piede_fattura, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		
		update tes_fat_ven
		set    num_colli  = num_colli + :id_num_colli,
		       peso_lordo = peso_lordo + :id_peso_lordo,
				 peso_netto = peso_netto + :id_peso_netto,
				 rag_soc_1  = :is_rag_soc_1,
				 rag_soc_2  = :is_rag_soc_2,
				 indirizzo  = :is_indirizzo,
				 localita   = :is_localita,
				 cap = :is_cap,
				 provincia = :is_provincia,
				 frazione  = :is_frazione,
				 data_inizio_trasporto = :idt_data_inizio_trasporto,
				 ora_inizio_trasporto  = :ldt_ora_inizio_trasporto,
				 cod_des_cliente = :is_destinazione,
				 cod_causale  = :is_cod_causale,
				 aspetto_beni = :is_aspetto_beni,
				 cod_porto = :is_cod_porto,
				 cod_mezzo = :is_cod_mezzo,
				 cod_vettore = :is_cod_vettore,
				 cod_inoltro = :is_cod_inoltro,
				 cod_imballo = :is_cod_imballo,
				 nota_piede = :ls_nota_piede_fattura
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_reg_fat_ven and
				 num_registrazione = :fl_num_reg_fat_ven;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante aggiornamento Testata Fatture Vendita. Dettaglio errore " + sqlca.sqlerrtext
			return -1
		end if
	else
		select max(num_registrazione)
		into   :ll_num_registrazione
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la ricerca massimo numero registrazione per incremento numeratore interno fatture.Dettaglio errore: " + sqlca.sqlerrtext
			return -1
		end if
		if isnull(ll_num_registrazione) then ll_num_registrazione = 0
		ll_num_registrazione ++
		update con_fat_ven
		set    num_registrazione = :ll_num_registrazione
		where  cod_azienda = :s_cs_xx.cod_azienda;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura sulla tabella Controllo Fatture Vendita.Dettaglio errore: " + sqlca.sqlerrtext
			return -1
		end if
		
		// *** Modifica Michela 05/12/2007: aggiungo anche l'imballo da segnalazione di viropa
		if not isnull(is_cod_imballo) then ls_cod_imballo = is_cod_imballo
		if not isnull(is_cod_causale) then ls_cod_causale = is_cod_causale
		if not isnull(is_aspetto_beni) then ls_aspetto_beni = is_aspetto_beni
		if not isnull(is_cod_porto) then ls_cod_porto = is_cod_porto
		if not isnull(is_cod_mezzo) then ls_cod_mezzo = is_cod_mezzo
		if not isnull(is_cod_vettore) then ls_cod_vettore = is_cod_vettore
		if not isnull(is_cod_inoltro) then ls_cod_inoltro = is_cod_inoltro
		if not isnull(is_cod_resa) then ls_cod_resa = is_cod_resa
		if not isnull(is_destinazione) then
			ls_cod_des_cliente = is_destinazione
			ls_rag_soc_1 = is_rag_soc_1
			ls_rag_soc_2 = is_rag_soc_2
			ls_indirizzo = is_indirizzo
			ls_localita = is_localita
			ls_frazione = is_frazione
			ls_cap = is_cap
			ls_provincia = is_provincia
		else
			if not isnull(is_rag_soc_1) and is_rag_soc_1 <> "" then
				ls_rag_soc_1 = is_rag_soc_1
			end if
			if not isnull(is_rag_soc_2) and is_rag_soc_2 <> "" then
				ls_rag_soc_2 = is_rag_soc_2
			end if
			if not isnull(is_indirizzo) and is_indirizzo <> "" then
				ls_indirizzo = is_indirizzo
			end if
			if not isnull(is_localita) and is_localita <> "" then
				ls_localita = is_localita
			end if
			if not isnull(is_frazione) and is_frazione <> "" then
				ls_frazione = is_frazione
			end if
			if not isnull(is_cap) and is_cap <> "" then
				ls_cap = is_cap
			end if			
			if not isnull(is_provincia) and is_provincia <> "" then
				ls_provincia = is_provincia
			end if									
		end if
		
		//11/02/2013 Donato -----------------------------------------------------------------------------------------------------------------------------------------------
		//per GIBUS: nel caso in cui l'operatore, prima di generare il documento mette zero nel campo colli della finestra di conferma dati,
		//il documento deve mettere zero in testata, invece prima metteva il numero colli dell'ordine
		if ib_gibus then
			if isnull(id_num_colli) then id_num_colli = 0
			ld_num_colli = id_num_colli
		else
			if not isnull(id_num_colli) and id_num_colli <> 0 then ld_num_colli = id_num_colli
		end if
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		
		if not isnull(id_peso_lordo) and id_peso_lordo <> 0 then ld_peso_lordo = id_peso_lordo
		if not isnull(id_peso_netto) and id_peso_netto <> 0 then ld_peso_netto = id_peso_netto
		ldt_ora_inizio_trasporto = datetime(date("01/01/1900"), it_ora_inizio_trasporto)
		if isnull(ls_num_ord_cliente) or len(ls_num_ord_cliente) < 1 then
			ls_num_ord_cliente = is_num_ord_cliente
		end if
		if isnull(ldt_data_ord_cliente) then
			ldt_data_ord_cliente = idt_data_ord_cliente
		end if
		if isnull(ls_nota_piede) then ls_nota_piede = ""
		
		//##########################################################################################
		//QUESTO VALE SOLO PER ib_gibus = TRUE
		//17/06/2014 d'accordo con Alberto
		//In caso di Nuova Fattura, se c'è qualcosa nella variabile is_nota_piede (proveniente dalla finestra di generazione doc.fiscale)
		//si tratta delle note piede presenti in ciascun ordine (se + di uno sono concatenate) o valore digitato/modificato a mano dall'operatore
		//quindi passa esattamente questo in note piede, poi al massimo una eventuale nota fissa verrà accodata
		//##########################################################################################
		if ib_gibus and len(trim(is_nota_piede)) > 0 then
			//-------------------------------------------------------------
			ls_nota_piede = is_nota_piede
			//-------------------------------------------------------------
			
			//dopo l'utilizzo pulisco questa variabile per evitare ripetizioni quando rientro (caso evasione + ordini con unica fattura ma in un colpo solo, non con tasto aggiungi)
			is_nota_piede = ""
			
		else
			if len(trim(ls_nota_piede)) > 0 then
				ls_nota_piede = ls_nota_piede
			else
				ls_nota_piede = ls_nota_piede
			end if
		end if
		
		// ----- controllo lunghezza note per ASE
		if (upper(ls_db) = "SYBASE_ASE" or upper(ls_db) = "MSSQL") and len(ls_nota_piede) > 255 then
			ls_nota_piede = left(ls_nota_piede, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		

		insert into tes_fat_ven
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 data_registrazione,   
						 cod_operatore,   
						 cod_tipo_fat_ven,   
						 cod_cliente,
						 cod_des_cliente,   
						 rag_soc_1,   
						 rag_soc_2,   
						 indirizzo,   
						 localita,   
						 frazione,   
						 cap,   
						 provincia,   
						 cod_deposito,   
						 cod_ubicazione,
						 cod_valuta,   
						 cambio_ven,   
						 cod_tipo_listino_prodotto,   
						 cod_pagamento,   
						 sconto,   
						 cod_agente_1,
						 cod_agente_2,
						 cod_banca,   
						 num_ord_cliente,   
						 data_ord_cliente,   
						 cod_imballo,   
						 aspetto_beni,
						 peso_netto,
						 peso_lordo,
						 num_colli,
						 cod_vettore,   
						 cod_inoltro,   
						 cod_mezzo,
						 causale_trasporto,
						 cod_porto,   
						 cod_resa,   
						 cod_documento,
						 numeratore_documento,
						 anno_documento,
						 num_documento,
						 data_fattura,
						 nota_testata,   
						 nota_piede,   
						 flag_fuori_fido,
						 flag_blocco,   
						 flag_movimenti,  
						 flag_contabilita,
						 tot_merci,
						 tot_spese_trasporto,
						 tot_spese_imballo,
						 tot_spese_bolli,
						 tot_spese_varie,
						 tot_sconto_cassa,
						 tot_sconti_commerciali,
						 imponibile_provvigioni_1,
						 imponibile_provvigioni_2,
						 tot_provvigioni_1,
						 tot_provvigioni_2,
						 importo_iva,
						 imponibile_iva,
						 importo_iva_valuta,
						 imponibile_iva_valuta,
						 tot_fattura,
						 tot_fattura_valuta,
						 flag_cambio_zero,
						 cod_banca_clien_for,
						 cod_causale,
						 data_inizio_trasporto,
						 ora_inizio_trasporto,
						 flag_st_note_tes,
						 flag_st_note_pie,
						 cod_natura_intra)  						 
		values      (:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ldt_data_registrazione,   
						 :ls_cod_operatore,   
						 :ls_cod_tipo_fat_ven,   
						 :ls_cod_cliente[1],
						 :ls_cod_des_cliente,   
						 :ls_rag_soc_1,   
						 :ls_rag_soc_2,   
						 :ls_indirizzo,   
						 :ls_localita,   
						 :ls_frazione,   
						 :ls_cap,   
						 :ls_provincia,   
						 :ls_cod_deposito[1],   
						 :ls_cod_ubicazione[1],
						 :ls_cod_valuta,   
						 :ld_cambio_ven,   
						 :ls_cod_tipo_listino_prodotto,   
						 :ls_cod_pagamento,   
						 :ld_sconto_testata,
						 :ls_cod_agente_1,
						 :ls_cod_agente_2,
						 :ls_cod_banca,   
						 :ls_num_ord_cliente,   
						 :ldt_data_ord_cliente,   
						 :ls_cod_imballo,   
						 :ls_aspetto_beni,
						 :ld_peso_netto,
						 :ld_peso_lordo,
						 :ld_num_colli,
						 :ls_cod_vettore,   
						 :ls_cod_inoltro,   
						 :ls_cod_mezzo,   
						 :ls_causale_trasporto,
						 :ls_cod_porto,   
						 :ls_cod_resa,   
						 null,
						 null,
						 0,
						 0,
						 null,
						 :ls_nota_testata,   
						 :ls_nota_piede,   
						 'N',
						 'N',   
						 'N',
						 'N',
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 'N',
						 :ls_cod_banca_clien_for,
						 :ls_cod_causale,
						 :idt_data_inizio_trasporto,
						 :ldt_ora_inizio_trasporto,
						 'I',
						 'I',
						 :ls_cod_natura_intra);						 
		 if sqlca.sqlcode <> 0 then
			 fs_errore = "Errore durante la scrittura Testata Fatture Vendita.Dettaglio errore: " + sqlca.sqlerrtext
			 return -1
		 end if
		 if uof_crea_nota_fissa("FAT_VEN", ll_anno_registrazione, ll_num_registrazione, fs_errore) = -1 then return -1
	 end if

	 declare cu_evas_ord_ven cursor for  
	 select  evas_ord_ven.prog_riga_ord_ven,   
				evas_ord_ven.prog_riga,   
				evas_ord_ven.cod_deposito,   
				evas_ord_ven.cod_ubicazione,   
				evas_ord_ven.cod_lotto,   
				evas_ord_ven.data_stock,   
				evas_ord_ven.prog_stock,   
				evas_ord_ven.quan_in_evasione
	from     evas_ord_ven  
	where    evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				evas_ord_ven.anno_registrazione = :fl_anno_registrazione and
				evas_ord_ven.num_registrazione = :fl_num_registrazione and
				evas_ord_ven.cod_utente = :s_cs_xx.cod_utente and
				evas_ord_ven.prog_sessione = :fl_prog_sessione and
				evas_ord_ven.flag_spedito = 'N'
	order by evas_ord_ven.prog_riga_ord_ven asc,   
				evas_ord_ven.prog_riga asc;

	open cu_evas_ord_ven;
	if lb_aggiungi then
		select max(prog_riga_fat_ven)
		into   :ll_prog_riga_fat_ven
		from   det_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
			if isnull(ll_prog_riga_fat_ven) then ll_prog_riga_fat_ven = 0
	else
		ll_prog_riga_fat_ven = 0	
	end if
	
	do while 0 = 0
		fetch cu_evas_ord_ven into :ll_prog_riga_ord_ven,   
											:ll_prog_riga,
											:ls_cod_deposito[1],   
											:ls_cod_ubicazione[1],   
											:ls_cod_lotto[1],   
											:ldt_data_stock[1],   
											:ll_progr_stock[1],   
											:ld_quan_in_evasione;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode = -1 then
			fs_errore =  "Errore durante la lettura Evasione Ordini Vendita.Dettaglio errore: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga
		
		select	det_ord_ven.cod_tipo_det_ven,   
					det_ord_ven.cod_misura,   
					det_ord_ven.cod_prodotto,   
					det_ord_ven.des_prodotto,   
					det_ord_ven.quan_ordine,
					det_ord_ven.prezzo_vendita,   
					det_ord_ven.fat_conversione_ven,   
					det_ord_ven.sconto_1,   
					det_ord_ven.sconto_2,  
					det_ord_ven.provvigione_1,
					det_ord_ven.provvigione_2,
					det_ord_ven.cod_iva,   
					det_ord_ven.quan_evasa,
					det_ord_ven.nota_dettaglio,   
					det_ord_ven.anno_commessa,
					det_ord_ven.num_commessa,
					det_ord_ven.cod_centro_costo,
					det_ord_ven.sconto_3,   
					det_ord_ven.sconto_4,   
					det_ord_ven.sconto_5,   
					det_ord_ven.sconto_6,   
					det_ord_ven.sconto_7,   
					det_ord_ven.sconto_8,   
					det_ord_ven.sconto_9,   
					det_ord_ven.sconto_10,
					det_ord_ven.cod_versione,
					det_ord_ven.num_confezioni,
					det_ord_ven.num_pezzi_confezione,
					det_ord_ven.flag_doc_suc_det,
					det_ord_ven.quantita_um,
					det_ord_ven.prezzo_um
		into		:ls_cod_tipo_det_ven, 
					:ls_cod_misura, 
					:ls_cod_prodotto, 
					:ls_des_prodotto, 
					:ld_quan_ordine,
					:ld_prezzo_vendita, 
					:ld_fat_conversione_ven, 
					:ld_sconto_1, 
					:ld_sconto_2, 
					:ld_provvigione_1, 
					:ld_provvigione_2, 
					:ls_cod_iva, 
					:ld_quan_evasa,
					:ls_nota_dettaglio, 
					:ll_anno_commessa,
					:ll_num_commessa,
					:ls_cod_centro_costo,
					:ld_sconto_3, 
					:ld_sconto_4, 
					:ld_sconto_5, 
					:ld_sconto_6, 
					:ld_sconto_7, 
					:ld_sconto_8, 
					:ld_sconto_9, 
					:ld_sconto_10,
					:ls_cod_versione,
					:ll_num_confezioni,
					:ll_num_pezzi_confezione,
					:ls_flag_doc_suc_det,
					:ld_quantita_um,
					:ld_prezzo_um
		  from 	det_ord_ven  
		 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
					det_ord_ven.num_registrazione = :fl_num_registrazione and
					det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven
		order by det_ord_ven.prog_riga_ord_ven asc;
	
		if sqlca.sqlcode <> 0 then
			fs_errore =  "Errore durante la lettura Dettagli Ordini Vendita.Dettaglio errore: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if
			
//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
// ricalcolo la quantità di vendita nel caso di evasioni parziali
		if ld_quan_in_evasione <> ld_quan_ordine then
			ld_quantita_um = round(ld_quan_in_evasione * ld_fat_conversione_ven, 4)
		end if

//------------------------------------------------- Modifica Nicola ---------------------------------------------
		if ls_flag_doc_suc_det = 'I' then //nota dettaglio
			select flag_doc_suc_or
			  into :ls_flag_doc_suc_det
			  from tab_tipi_det_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		end if		
		
		
		if ls_flag_doc_suc_det = 'N' then
			setnull(ls_nota_dettaglio)
		end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------		
					
			
		select tab_tipi_det_ven.flag_tipo_det_ven,
				 tab_tipi_det_ven.cod_tipo_movimento,
				 tab_tipi_det_ven.flag_sola_iva
		into   :ls_flag_tipo_det_ven,
				 :ls_cod_tipo_movimento,
				 :ls_flag_sola_iva
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode <> 0 then
			fs_errore = "Si è verificato un errore in fase di lettura tipi dettagli.Dettaglio errore: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
				fs_errore = "Si è verificato un errore in fase di creazione destinazioni movimenti."
				close cu_evas_ord_ven;
				return -1
			end if

			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if lb_riferimento and ls_flag_abilita_rif_bol_ven = "D" then
			lb_riferimento = false
			setnull(ls_note_dettaglio_rif)
			ls_des_riferimento = ls_des_riferimento_ord_ven
			if ls_flag_rif_anno_ord_ven = "S" and not isnull(fl_anno_registrazione) then
				ls_des_riferimento = ls_des_riferimento + "-" +string(fl_anno_registrazione)
			end if
			if ls_flag_rif_num_ord_ven = "S" and not isnull(fl_num_registrazione) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(fl_num_registrazione)
			end if
			if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
			end if
			
			if not isnull(is_rif_packing_list) and len(is_rif_packing_list) > 0 then
				if not isnull(ls_note_dettaglio_rif) and len(ls_note_dettaglio_rif) > 0 then 
					ls_note_dettaglio_rif += "~r~n"
				else
					ls_note_dettaglio_rif = ""
				end if
				ls_note_dettaglio_rif = ls_note_dettaglio_rif + is_rif_packing_list
			end if
			
			if not isnull(ls_num_ord_cliente) then
				if not isnull(ls_note_dettaglio_rif) and len(ls_note_dettaglio_rif) > 0 then 
					ls_note_dettaglio_rif += "~r~n"
				else
					ls_note_dettaglio_rif = ""
				end if
				ls_note_dettaglio_rif = "Vs.Ordine " + ls_num_ord_cliente
				if not isnull(ldt_data_ord_cliente) then
					ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
				end if
			end if
			
				
			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_bol_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
				g_mb.messagebox("Generazione Bolla Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if
			
			insert into det_fat_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_fat_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 data_stock,
							 progr_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_fatturata,   
							 prezzo_vendita,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 anno_registrazione_fat,
							 num_registrazione_fat,
							 nota_dettaglio,   
							 anno_registrazione_bol_ven,   
							 num_registrazione_bol_ven,   
							 prog_riga_bol_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 fat_conversione_ven,   
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 anno_reg_ord_ven,   
							 num_reg_ord_ven,   
							 prog_riga_ord_ven,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_fat_ven,   
							 :ls_cod_tipo_det_ven_rif_bol_ven,   
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 :ls_des_riferimento,   
							 0,   
							 0,   
							 0,   
							 0,  
							 0,
							 0,
							 :ls_cod_iva_rif,   
							 null,
							 null,
							 null,
							 null,
							 null,   
							 null,
							 null,
							 null,
							 null,   
							 null,   
							 null,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,
							 null,
							 1,
							 null,
							 null,
							 null,   
							 null,   
							 null,
							 null,
							 0,
							 0,
							 'I',
							 0,
							 0);							 
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la scrittura Dettagli Fatture Vendita.Dettaglio errore: " + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
			ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga
			
			
			//ticket 2014/  solo per GIBUS: mettere una ulteriore riga di riferimento vostro ordine (se nella testata ordine è valorizzato)
			if ib_gibus then
				if not isnull(ls_num_ord_cliente) and ls_num_ord_cliente<>"" then			
					ls_des_riferimento = "Rif.Vs Ordine " + ls_num_ord_cliente
					if not isnull(ldt_data_ord_cliente) then ls_des_riferimento += " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
					
					if uof_inserisci_rif_vs_ordine_in_fattura(	ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven, &
																		ls_cod_tipo_det_ven_rif_bol_ven, ls_des_riferimento, ls_cod_iva_rif, fs_errore) < 0 then
						close cu_evas_ord_ven;
						return -1
					end if
					
					ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga
					
				end if
			end if
			//fine modifica -------------------------------------
			
		end if	
		
		// stefanop 09/05/2010: progetto 140: controllo se devo cambiare il dettaglio
		if this.ib_change_det_ven then
			// cambio il dettaglio solo se uguale a quello di default, altrimenti lascio invariato
			if ls_cod_tipo_det_ven_ord = ls_cod_tipo_det_ven then
				ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_new
			end if
		end if
		
		if this.ib_change_mov then
			if ls_cod_tipo_movimento = ls_cod_tipo_movimento_ord then
				ls_cod_tipo_movimento = ls_cod_tipo_movimento_new
			end if
		end if
		// ---
		
		
		//#####################################################################
		//per ora LASCIAMO COMMENTATO QUESTO CONTROLLO
		//E ATIVIAMO IL CONTROLLO IN FASE DI SPEDIZIONE, CIOè IN EVAS_ORD_VEN
		
//		//Donato 12/03/2012 prima di inserire controlla se la riga di ordine riferita è già presente in fattura
//		//(problema doppia evasione riga ordine che porta a quantità residua negativa!!!!!)
//		if uof_controlla_ordine_in_fattura(	ll_anno_registrazione, ll_num_registrazione, &
//													fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven,fs_errore)<0 then
//			//in fs_errore il messaggio
//			close cu_evas_ord_ven;
//			return -1
//		end if
		//#####################################################################
		
		
		//########################################################################################
		//Donato 27/06/2012: per nessun motivo deve finire in bolla una quantità superiore a quella della riga di ordine vendita riferito
		if uof_controlla_qta_in_documento(fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven, ld_quan_in_evasione, fs_errore) < 0 then
			//in fs_errore il messaggio
			close cu_evas_ord_ven;
			return -1
		end if
		//########################################################################################
		
		
		insert into det_fat_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_fat_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 data_stock,
						 progr_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_fatturata,   
						 prezzo_vendita,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 anno_registrazione_fat,
						 num_registrazione_fat,
						 nota_dettaglio,   
						 anno_registrazione_bol_ven,   
						 num_registrazione_bol_ven,   
						 prog_riga_bol_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 fat_conversione_ven,   
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 anno_reg_ord_ven,   
						 num_reg_ord_ven,   
						 prog_riga_ord_ven,
						 cod_versione,
                   		num_confezioni,
					    	num_pezzi_confezione,
						 flag_st_note_det,
						 quantita_um,
						 prezzo_um)						 
	  values			(:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_fat_ven,   
						 :ls_cod_tipo_det_ven,   
						 :ls_cod_deposito[1],
						 :ls_cod_ubicazione[1],
						 :ls_cod_lotto[1],
						 :ldt_data_stock[1],
						 :ll_progr_stock[1],
						 :ls_cod_misura,   
						 :ls_cod_prodotto,   
						 :ls_des_prodotto,   
						 :ld_quan_in_evasione,   
						 :ld_prezzo_vendita,   
						 :ld_sconto_1,   
						 :ld_sconto_2,  
						 :ld_provvigione_1,
						 :ld_provvigione_2,
						 :ls_cod_iva,   
						 :ls_cod_tipo_movimento,
						 null,
						 null,
						 null,
						 :ls_nota_dettaglio,   
						 null,
						 null,
						 null,
						 :ll_anno_commessa,   
						 :ll_num_commessa,   
						 :ls_cod_centro_costo,   
						 :ld_sconto_3,   
						 :ld_sconto_4,   
						 :ld_sconto_5,   
						 :ld_sconto_6,   
						 :ld_sconto_7,   
						 :ld_sconto_8,   
						 :ld_sconto_9,   
						 :ld_sconto_10,
						 null,
						 :ld_fat_conversione_ven,
						 :ll_anno_reg_des_mov,
						 :ll_num_reg_des_mov,
						 :fl_anno_registrazione,   
						 :fl_num_registrazione,   
						 :ll_prog_riga_ord_ven,
						 :ls_cod_versione,
                  		 :ll_num_confezioni,
					      :ll_num_pezzi_confezione,
						 'I',
						 :ld_quantita_um,
						 :ld_prezzo_um);										 

		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura Dettagli Fatture Vendita.Dettaglio errore: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			update anag_prodotti  
				set quan_assegnata = quan_assegnata - :ld_quan_in_evasione,
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode <> 0 then
				fs_errore = "Si è verificato un errore in fase di aggiornamento magazzino.Dettaglio errore: " + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
			
			update stock 
			set 	 quan_assegnata = quan_assegnata - :ld_quan_in_evasione,   
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione  
			where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
					 stock.cod_prodotto = :ls_cod_prodotto and 
					 stock.cod_deposito = :ls_cod_deposito[1] and 
					 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
					 stock.cod_lotto = :ls_cod_lotto[1] and 
					 stock.data_stock = :ldt_data_stock[1] and 
					 stock.prog_stock = :ll_progr_stock[1];

			if sqlca.sqlcode <> 0 then
				fs_errore = "Si è verificato un errore in fase di aggiornamento stock.Dettaglio errore: " + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				return -1
			end if
		end if				

		ls_tabella_orig = "det_ord_ven_stat"
		ls_tabella_des = "det_fat_ven_stat"
		ls_prog_riga_doc_orig = "prog_riga_ord_ven"
		ls_prog_riga_doc_des = "prog_riga_fat_ven"

		if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven) = -1 then
			close cu_evas_ord_ven;
			return -1
		end if	

		if ls_cod_tipo_analisi_fat <> ls_cod_tipo_analisi_ord then
			if f_crea_distribuzione(ls_cod_tipo_analisi_fat, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_fat_ven, 6) = -1 then
				fs_errore = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
				close cu_evas_ord_ven;
				return -1
			end if
		end if
		
		
		if ld_quan_ordine <= ld_quan_evasa + ld_quan_in_evasione then
			ls_flag_evasione = "E"
		else
			ls_flag_evasione = "P"
		end if
		
		update det_ord_ven  
			set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione,
				 quan_evasa = quan_evasa + :ld_quan_in_evasione,
				 flag_evasione = :ls_flag_evasione
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
				 det_ord_ven.num_registrazione = :fl_num_registrazione and
				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
		if sqlca.sqlcode <> 0 then
			fs_errore = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Vendita.Dettaglio errore: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if
		
		//######################################################################################
		//Donato 27/06/2012 cerco di evitare i casi in cui si possono avere residuo negativo
		//la casistica prevede ad esempio che 
		//1)		la quantità in evasione non si azzera dopo l'evasione, 
		//			e allora il residuo dato da quan_ordine - quan_in_evasione - quan_evasa da un valore minore di zero (caso SACCOLONGO)
		
		//2)		in altri casi si ha invece un azzeramento (giustamente) della quan_in_evasione ma una quantità_evasa superiore a quella dell'ordine (caso VEGGIANO)
		
		uof_correggi_errori_in_evasione(fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven)
		//###########################################################
		

		if ls_flag_sola_iva = 'N' then
			ld_val_riga = ld_quan_in_evasione * ld_prezzo_vendita
			ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
			ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
			ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
			ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
			ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
			ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
			ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
			ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
			ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
			ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
			ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
			ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
			ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
			ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
			ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
			ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
			ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
			ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
			ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
			if ls_cod_valuta = ls_lire then
				ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
			else
				ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
			end if   
	
			if ls_flag_tipo_det_ven = "M" or &
				ls_flag_tipo_det_ven = "C" or &
				ls_flag_tipo_det_ven = "N" then
				ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
				ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
				ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
				ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
			elseif ls_flag_tipo_det_ven = "S" then
				ld_val_evaso = ld_val_riga_sconto_10 * -1
			else
				ld_val_evaso = ld_val_riga_sconto_10
			end if
			ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
		end if

		if ls_cod_valuta = ls_lire then
			ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
		end if   
		
		//Donato 13/04/2012 anzichè cancellare faccio un update a spedito = S
		update evas_ord_ven
		set flag_spedito='S'
		where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
						evas_ord_ven.anno_registrazione = :fl_anno_registrazione and
						evas_ord_ven.num_registrazione = :fl_num_registrazione and 
						evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
						evas_ord_ven.prog_riga = :ll_prog_riga and
						evas_ord_ven.cod_utente = :s_cs_xx.cod_utente and
						evas_ord_ven.prog_sessione = :fl_prog_sessione;
		
		if sqlca.sqlcode < 0 then
			fs_errore = "Si è verificato un errore in fase di cancellazione Evasione Ordini.Dettaglio errore: " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			return -1
		end if

		//--------------------------------------------------------------------
		//SR Note_conformita_prodotto
		if uof_note_in_fattura(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven, fs_errore) < 0 then
			close cu_evas_ord_ven;
			return -1
		end if
		//--------------------------------------------------------------------

	loop
	close cu_evas_ord_ven;

	if ld_tot_val_ordine <= ld_tot_val_evaso then
		ls_flag_evasione = "E"
	else
		ls_flag_evasione = "P"
	end if

//	update tes_ord_ven  
//		set tot_val_evaso = :ld_tot_val_evaso
//	 where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione and  
//			 tes_ord_ven.num_registrazione = :fl_num_registrazione;
//
//	if sqlca.sqlcode <> 0 then
//		fs_errore = "Si è verificato un errore in fase di aggiornamento Valore Evaso in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
//		return -1
//	end if
	
	if uof_calcola_stato_ordine(fl_anno_registrazione,fl_num_registrazione) = -1 then
		fs_errore = "Si è verificato un errore in fase di aggiornamento stato dell'ordine in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
		return -1
	end if
	
	// ORA CALCOLO IL DOCUMENTO PER CREARE GLI IMPONIBILI NECESSARI AL SUCCESSIVO CALCOLO DELLE SPES EDI TRASPORTO
	// QUINDI NON RIMUOVERE IL SUCCESSIVO CALCOLO
		
	luo_calcolo = create uo_calcola_documento_euro

	if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"fat_ven",fs_errore) <> 0 then
		destroy luo_calcolo
		return -1
	end if
	
	// *****************************
	// Calcolo le spese di trasporto
	uo_spese_trasporto luo_spese_trasporto
	luo_spese_trasporto = create uo_spese_trasporto
	
	if luo_spese_trasporto.uof_calcola_spese(ll_anno_registrazione, ll_num_registrazione, "FATVEN", ref fs_errore) < 0 then
		destroy luo_spese_trasporto
		destroy luo_calcolo
		return -1
	end if
		
	// di seguito ri-calcolo il documento per includere nel calcolo le spese di trasporto
	if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"fat_ven",fs_errore) <> 0 then
		destroy luo_calcolo
		destroy luo_spese_trasporto
		return -1
	end if
	
	if ib_visualizza_spese_trasp then
		//valori di ritorno della funzione
		//0		SPESE TRASPORTO CONFERMATE (o non abilitate dal parametro SSP), quindi non fare null'altro
		//1		SPESE TRASPORTO ANNULLATE, quindi chiama la funzione che toglie le righe trasporto e che ricalcola il documento
		//2		SPESE TRASPORTO DA SOLE PERCENTUALI IMPONIBILE RIGHE ORDINE. quindi chiama funzione opportuna
		//NOTA BENE anche nei nei casi di ritorno 1 o 2 ci sarà un commit fatto in tal caso all'uscita di questa funzione
		li_risposta = luo_spese_trasporto.uof_apri_storico(ll_anno_registrazione, ll_num_registrazione, "FATVEN")
		
		if li_risposta = 0 then
		else
			if li_risposta = 1 then
				//--------------------------------------------------------------------------------------------------------------------------------------------
				//ANNULLAMENTO SPESE TRASPORTO
				li_risposta = luo_spese_trasporto.uof_azzera_trasporto_documento(ll_anno_registrazione, ll_num_registrazione, "FATVEN", fs_errore)
				//--------------------------------------------------------------------------------------------------------------------------------------------
			elseif li_risposta = 2 then
				//--------------------------------------------------------------------------------------------------------------------------------------------
				//SPESE TRASPORTO DA SOLE PERCENTUALI IMPONIBILE RIGHE ORDINE
				li_risposta = luo_spese_trasporto.uof_imposta_trasporto_solo_percentuale(ll_anno_registrazione, ll_num_registrazione, "FATVEN", fs_errore)
				//--------------------------------------------------------------------------------------------------------------------------------------------
			end if
			
			if li_risposta<0 then
				destroy luo_spese_trasporto
				destroy luo_calcolo
				return -1
			end if
		end if
	end if
	
	destroy luo_spese_trasporto
	destroy luo_calcolo
	
	fl_anno_reg_fat_ven = ll_anno_registrazione
	fl_num_reg_fat_ven = ll_num_registrazione
end if

return 0
end function

public function integer uof_crea_aggiungi_bolla_sessione (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_bol_ven, ref long fl_num_reg_bol_ven, longlong fl_prog_sessione, ref string fs_errore);boolean					lb_riferimento, lb_aggiungi=false

long						ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_ord_ven, ll_anno_commessa, ll_num_commessa, &
							ll_progr_stock[1], ll_prog_riga_bol_ven, ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_prog_riga, &
							ll_num_confezioni, ll_num_pezzi_confezione
							
string						ls_cod_tipo_ord_ven, ls_cod_tipo_bol_ven, ls_nota_testata, ls_cod_cliente[1], ls_cod_operatore, ls_rag_soc_1, &
							ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione[1], &
							ls_cod_deposito[1], ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, ls_cod_imballo, &
							ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_nota_piede, ls_cod_banca, ls_cod_tipo_det_ven, &
							ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_ven, &
							ls_cod_tipo_analisi_bol, ls_cod_tipo_analisi_ord, ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, ls_causale_trasporto, &
							ls_flag_riep_fat, ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, ls_num_ord_cliente, &
							ls_cod_lotto[1], ls_cod_tipo_movimento, ls_vostro_ordine, ls_vostro_ordine_data, ls_flag_evasione, ls_lire, &
							ls_flag_sola_iva, ls_cod_fornitore[], ls_causale_trasporto_tab, ls_cod_versione, ls_cod_causale_tab, ls_flag_abilita_rif_ord_ven, &
							ls_cod_tipo_det_ven_rif_ord_ven, ls_des_riferimento_ord_ven, ls_flag_rif_anno_ord_ven, ls_flag_rif_num_ord_ven,&
							ls_flag_rif_data_ord_ven, ls_des_riferimento, ls_cod_iva_rif, ls_cod_causale, ls_note_dettaglio_rif, &
							ls_db,ls_cod_tipo_bol_ven_dest, ls_nota_piede_bolla, ls_messaggio, ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det, &
							ls_nota_prodotto, ls_flag_trasporta_nota_ddt, ls_rif_packing_list,ls_cod_tipo_det_ven_new, ls_cod_tipo_det_ven_ord, &
							ls_cod_tipo_movimento_ord, ls_cod_tipo_movimento_new, ls_cod_giro_consegna, ls_cod_deposito_giro, ls_temp, ls_cod_natura_intra
							
datetime					ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock[1], ldt_vostro_ordine_data, ldt_ora_inizio_trasporto

dec{6}					ld_cambio_ven, ld_quan_in_evasione, ld_prezzo_vendita, ld_fat_conversione_ven, ld_sconto_1, ld_sconto_2, &
							ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
							 ld_sconto_testata, ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, ld_quan_ordine, &
							 ld_quan_evasa, ld_tot_val_evaso, ld_tot_val_ordine, ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
							 ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, ld_val_riga_sconto_4, &
							 ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
							 ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10, &
							 ld_val_sconto_testata, ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quantita_um, ld_prezzo_um
							 
integer					li_risposta

uo_calcola_documento_euro	luo_calcolo
uo_log_sistema						luo_log



li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
ll_anno_registrazione = f_anno_esercizio()

if not isnull(fl_anno_reg_bol_ven) and fl_anno_reg_bol_ven > 0 then lb_aggiungi = true
if lb_aggiungi and (fl_num_reg_bol_ven = 0 or isnull(fl_num_reg_bol_ven)) then
	fs_errore = "Manca l'indicazione del numero di bolla di destinazione"
	return -1
end if
lb_riferimento = true
setnull(ls_cod_fornitore[1])

select cod_tipo_ord_ven
into   :ls_cod_tipo_ord_ven
from   tes_ord_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in fase di ricerca ordine: operazione annullata"
	return -1
end if

select cod_tipo_bol_ven, cod_tipo_analisi  
into :ls_cod_tipo_bol_ven,  :ls_cod_tipo_analisi_ord
from tab_tipi_ord_ven  
where  
	cod_azienda = :s_cs_xx.cod_azienda and  
	cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la lettura della tabella Tipi Ordini Vendita."
	return -1
end if
	
// stefanop 08/06/2010: non carico il tipo bol ven se impostato: progetto 140
if isnull(this.is_cod_tipo_bol_ven) or this.is_cod_tipo_bol_ven = "" then
	// lascio quello di default dell'applicazione, 99% dei casi
	this.ib_change_det_ven = false
	this.ib_change_mov = false
else
	
	// TIPO_DET_VEN
	if this.ib_change_det_ven then
		// recupero il dettaglio orignale che sarà sostituito con quello nuovo
		select cod_tipo_det_ven
		into :ls_cod_tipo_det_ven_ord
		from tab_tipi_bol_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
			
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante il recupero del dettaglio bolla.~r~n" + sqlca.sqlerrtext
			return -1
		end if
		
		// recupero il nuovo dettaglio
		select cod_tipo_det_ven
		into :ls_cod_tipo_det_ven_new
		from tab_tipi_bol_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_bol_ven = :is_cod_tipo_bol_ven;
			
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante il recupero del nuovo dettaglio bolla."
			return -1
		end if
	end if
	
	// MOV_MAG
	if this.ib_change_mov then
		// carico movimento default, mi serve per il confronto
		select cod_tipo_movimento
		into :ls_cod_tipo_movimento_ord
		from tab_tipi_det_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and 
			cod_tipo_det_ven = :ls_cod_tipo_det_ven_ord;
			
		if sqlca.sqlcode <> 0 then 
			fs_errore = "Errore durante la ricerca del tipo movimento."
			return -1
		end if
		
		// carico il nuovo movimento di magazzioni che sarà inserito
		select cod_tipo_movimento
		into :ls_cod_tipo_movimento_new
		from tab_tipi_det_ven
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_det_ven = :ls_cod_tipo_det_ven_new;
			
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la ricerca del nuovo tipo movimento."
			return -1
		end if
	end if
	
	
	// la variabile viene impostata dalla finestra w_richiesta_dati_bolla
	ls_cod_tipo_bol_ven = this.is_cod_tipo_bol_ven
end if
// ----

if lb_aggiungi then
	select cod_tipo_bol_ven
	into   :ls_cod_tipo_bol_ven_dest
	from   tes_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :fl_anno_reg_bol_ven and
			 num_registrazione = :fl_num_reg_bol_ven;
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in ricerca tipo bolla di vendita in bolla di destinazione. Dettaglio: " + sqlca.sqlerrtext
		return -1
	end if
	
	if ls_cod_tipo_bol_ven <> ls_cod_tipo_bol_ven_dest then
		fs_errore = "Il tipo bolla deve coincidere con il tipo bolla di destinazione indicato nel tipo ordine: controllare il tipo bolla di destinazione nella tabella tipi ordini"
		return -1
	end if
end if

if not isnull(ls_cod_tipo_bol_ven) then
	select tes_ord_ven.cod_operatore,   
			 tes_ord_ven.data_registrazione,   
			 tes_ord_ven.cod_cliente,   
			 tes_ord_ven.cod_des_cliente,   
			 tes_ord_ven.rag_soc_1,   
			 tes_ord_ven.rag_soc_2,   
			 tes_ord_ven.indirizzo,   
			 tes_ord_ven.localita,   
			 tes_ord_ven.frazione,   
			 tes_ord_ven.cap,   
			 tes_ord_ven.provincia,   
			 tes_ord_ven.cod_deposito,   
			 tes_ord_ven.cod_ubicazione,   
			 tes_ord_ven.cod_valuta,   
			 tes_ord_ven.cambio_ven,   
			 tes_ord_ven.cod_tipo_listino_prodotto,   
			 tes_ord_ven.cod_pagamento,   
			 tes_ord_ven.sconto,   
			 tes_ord_ven.cod_agente_1,
			 tes_ord_ven.cod_agente_2,
			 tes_ord_ven.cod_banca,   
			 tes_ord_ven.num_ord_cliente,   
			 tes_ord_ven.data_ord_cliente,   
			 tes_ord_ven.cod_imballo,   
			 tes_ord_ven.aspetto_beni,
			 tes_ord_ven.peso_netto,
			 tes_ord_ven.peso_lordo,
			 tes_ord_ven.num_colli,
			 tes_ord_ven.cod_vettore,   
			 tes_ord_ven.cod_inoltro,   
			 tes_ord_ven.cod_mezzo,
			 tes_ord_ven.causale_trasporto,
			 tes_ord_ven.cod_porto,   
			 tes_ord_ven.cod_resa,   
			 tes_ord_ven.nota_testata,   
			 tes_ord_ven.nota_piede,   
			 tes_ord_ven.flag_riep_fat,
			 tes_ord_ven.tot_val_evaso,
			 tes_ord_ven.tot_val_ordine,
			 tes_ord_ven.cod_banca_clien_for,
			 tes_ord_ven.cod_causale,
			 tes_ord_ven.flag_doc_suc_tes, 
			 tes_ord_ven.flag_doc_suc_pie,
			 tes_ord_ven.cod_giro_consegna,
			 tes_ord_ven.cod_natura_intra
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito[1],   
			 :ls_cod_ubicazione[1],
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ls_flag_riep_fat,
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for,
			 :ls_cod_causale,
			 :ls_flag_doc_suc_tes, 
			 :ls_flag_doc_suc_pie,
			 :ls_cod_giro_consegna,
			 :ls_cod_natura_intra
	from   tes_ord_ven  
	where  tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione and  
			 tes_ord_ven.num_registrazione = :fl_num_registrazione;

	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la lettura Testata Ordine Vendita."
		return -1
	end if
	
	if ls_cod_natura_intra="" then setnull(ls_cod_natura_intra)

	//#####################################################################
	//se il giro consegna (quando specificato) ha un deposito diverso da quello origine ordine, allora il documento fiscale va creato in quel deposito
	if g_str.isnotempty(ls_cod_giro_consegna) then
		select cod_deposito
		into :ls_cod_deposito_giro
		from tes_giri_consegne
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_giro_consegna=:ls_cod_giro_consegna;
					
		if g_str.isnotempty(ls_cod_deposito_giro) and ls_cod_deposito_giro<>ls_cod_deposito[1] then
			//salvo il deposito origine ordine inn una variabile, mi serve per scriverlo nella log sistema
			ls_temp = ls_cod_deposito[1]
			ls_cod_deposito[1] = ls_cod_deposito_giro
			
			luo_log = create uo_log_sistema
			luo_log.uof_write_log_sistema_not_sqlca("DEP.CONS.EVASIONE", "Applicato deposito codice "+ls_cod_deposito_giro+" del  giro consegna codice "+ls_cod_giro_consegna+&
																						" al posto del deposito origine "+ls_temp+" in evasione ordine n. "+string(fl_anno_registrazione)+"/" + string(fl_num_registrazione) + &
																						" mediante ddt.")
			destroy luo_log
		end if
	end if
	//#####################################################################


	// stefanop: 18/07/2012 - l'operatore che inserire è SEMPRE l'operatore collegato ad Apice
	// ------------------------------------------------------
	ls_cod_operatore = guo_functions.uof_get_operatore_utente()
	// ------------------------------------------------------
	
	select tab_tipi_bol_ven.cod_tipo_analisi,
			 tab_tipi_bol_ven.causale_trasporto,
			 tab_tipi_bol_ven.cod_causale,
			 tab_tipi_bol_ven.flag_abilita_rif_ord_ven,
			 tab_tipi_bol_ven.cod_tipo_det_ven_rif_ord_ven,
			 tab_tipi_bol_ven.des_riferimento_ord_ven,
			 tab_tipi_bol_ven.flag_rif_anno_ord_ven,
			 tab_tipi_bol_ven.flag_rif_num_ord_ven,
			 tab_tipi_bol_ven.flag_rif_data_ord_ven
	into	 :ls_cod_tipo_analisi_bol,
			 :ls_causale_trasporto_tab,
			 :ls_cod_causale_tab,
			 :ls_flag_abilita_rif_ord_ven,
			 :ls_cod_tipo_det_ven_rif_ord_ven,
			 :ls_des_riferimento_ord_ven,
			 :ls_flag_rif_anno_ord_ven,
			 :ls_flag_rif_num_ord_ven,
			 :ls_flag_rif_data_ord_ven
	from   tab_tipi_bol_ven  
	where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Si è verificato un errore nella lettura della Tabella Tipi Bolle."
		return -1
	end if

	ldt_data_registrazione = datetime(today())
	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if

	if not isnull(ls_num_ord_cliente) and ls_flag_abilita_rif_ord_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if

//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_ord_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_ord_ven = :ls_cod_tipo_ord_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------


	if ls_cod_tipo_det_ven_rif_ord_ven = "T" then
		if len(is_rif_packing_list) > 0 and not isnull(is_rif_packing_list) then ls_rif_packing_list = is_rif_packing_list + "~r~n"
		ls_nota_testata = ls_rif_packing_list + ls_des_riferimento_ord_ven + string(fl_anno_registrazione) + "/" + &
								string(fl_num_registrazione) + " del " + string(ldt_data_ordine, "dd/mm/yyyy")+ &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
	end if
	
	// ----- controllo lunghezza note per ASE
	if (upper(ls_db) = "SYBASE_ASE" or upper(ls_db) = "MSSQL") and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE
	
	if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
		ls_causale_trasporto = ls_causale_trasporto_tab
	end if
	if isnull(ls_cod_causale) then
		ls_cod_causale = ls_cod_causale_tab
	end if
	select parametri_azienda.stringa
	into   :ls_lire
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LIR';

	if sqlca.sqlcode <> 0 then
		fs_errore = "Configurare il codice valuta per le Lire Italiane."
		return -1
	end if

	select tab_pagamenti.sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if
	
	if lb_aggiungi then
		ldt_ora_inizio_trasporto = datetime(date("01/01/1900"), it_ora_inizio_trasporto)
		ll_num_registrazione = fl_num_reg_bol_ven
		
		select nota_piede
		into   :ls_nota_piede_bolla
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_reg_bol_ven and
		       num_registrazione = :fl_num_reg_bol_ven;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante ricercaTestata Bolle Vendita. Dettaglio errore " + sqlca.sqlerrtext
			return -1
		end if

		if isnull(ls_nota_piede_bolla) then ls_nota_piede_bolla = ""

		//17/06/2014 d'accordo con Alberto
		//QUESTO VALE SOLO PER ib_gibus = TRUE
		//in caso di aggiungi e modalità GIBUS se scrivi qualcosa nelle note accodala a quanto già presente
		//infatti se stai evadendo + ordini e questo è un ordine successivo al primo la variabile "is_nota_piede" è stata pulita
		//se invece provieni da AGGIUNGI A DDT ESISTENTE allora la variabile "is_nota_piede" potrebbe essere valorizzata
		//quindi in tal caso verrà accodata
		if ib_gibus and len(trim(is_nota_piede)) > 0 then
			//------------------------------------------------
			ls_nota_piede_bolla += "~r~n" + is_nota_piede
			//------------------------------------------------
		else
			if len(trim(ls_nota_piede_bolla)) > 0 then
				ls_nota_piede_bolla = ls_nota_piede_bolla
			else
				ls_nota_piede_bolla = ls_nota_piede_bolla
			end if
		end if
		
		// ----- controllo lunghezza note per ASE
		if (upper(ls_db) = "SYBASE_ASE" or upper(ls_db) = "MSSQL") and len(ls_nota_piede_bolla) > 255 then
			ls_nota_piede_bolla = left(ls_nota_piede_bolla, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		
		update tes_bol_ven
		set    num_colli  = num_colli + :id_num_colli,
		       peso_lordo = peso_lordo + :id_peso_lordo,
				 peso_netto = peso_netto + :id_peso_netto,
				 rag_soc_1  = :is_rag_soc_1,
				 rag_soc_2  = :is_rag_soc_2,
				 indirizzo  = :is_indirizzo,
				 localita   = :is_localita,
				 cap = :is_cap,
				 provincia = :is_provincia,
				 frazione  = :is_frazione,
				 data_inizio_trasporto = :idt_data_inizio_trasporto,
				 ora_inizio_trasporto  = :ldt_ora_inizio_trasporto,
				 cod_des_cliente = :is_destinazione,
				 cod_causale  = :is_cod_causale,
				 aspetto_beni = :is_aspetto_beni,
				 cod_porto = :is_cod_porto,
				 cod_mezzo = :is_cod_mezzo,
				 cod_vettore = :is_cod_vettore,
				 cod_inoltro = :is_cod_inoltro,
				 cod_imballo = :is_cod_imballo,
				 nota_piede = :ls_nota_piede_bolla
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :fl_anno_reg_bol_ven and
				 num_registrazione = :fl_num_reg_bol_ven;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante aggiornamento Testata Bolle Vendita. Dettaglio errore " + sqlca.sqlerrtext
			return -1
		end if
	else
		select max(num_registrazione)
		into   :ll_num_registrazione
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione;
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la ricerca massimo numero registrazione per incremento numeratore interno bolle."
			return -1
		end if
		if isnull(ll_num_registrazione) then ll_num_registrazione = 0
		ll_num_registrazione ++
		update con_bol_ven
		set    num_registrazione = :ll_num_registrazione
		where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura sulla tabella Controllo Bolle Vendita."
			return -1
		end if
		
		if not isnull(is_cod_causale) then ls_cod_causale = is_cod_causale
		if not isnull(is_aspetto_beni) then ls_aspetto_beni = is_aspetto_beni
		if not isnull(is_cod_porto) then ls_cod_porto = is_cod_porto
		if not isnull(is_cod_mezzo) then ls_cod_mezzo = is_cod_mezzo
		if not isnull(is_cod_vettore) then ls_cod_vettore = is_cod_vettore
		if not isnull(is_cod_inoltro) then ls_cod_inoltro = is_cod_inoltro
		if not isnull(is_cod_resa) then ls_cod_resa = is_cod_resa
		if not isnull(is_destinazione) then
			ls_cod_des_cliente = is_destinazione
			ls_rag_soc_1 = is_rag_soc_1
			ls_rag_soc_2 = is_rag_soc_2
			ls_indirizzo = is_indirizzo
			ls_localita = is_localita
			ls_frazione = is_frazione
			ls_cap = is_cap
			ls_provincia = is_provincia
		end if
		
		//11/02/2013 Donato -----------------------------------------------------------------------------------------------------------------------------------------------
		//per GIBUS: nel caso in cui l'operatore, prima di generare il documento mette zero nel campo colli della finestra di conferma dati,
		//il documento deve mettere zero in testata, invece prima metteva il numero colli dell'ordine
		if ib_gibus then
			if isnull(id_num_colli) then id_num_colli = 0
			ld_num_colli = id_num_colli
		else
			if not isnull(id_num_colli) and id_num_colli <> 0 then ld_num_colli = id_num_colli
		end if
		//-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
		
		
		if not isnull(id_peso_lordo) and id_peso_lordo <> 0 then ld_peso_lordo = id_peso_lordo
		if not isnull(id_peso_netto) and id_peso_netto <> 0 then ld_peso_netto = id_peso_netto
		ldt_ora_inizio_trasporto = datetime(date("01/01/1900"), it_ora_inizio_trasporto)
		if isnull(ls_num_ord_cliente) or len(ls_num_ord_cliente) < 1 then
			ls_num_ord_cliente = is_num_ord_cliente
		end if
		if isnull(ldt_data_ord_cliente) then
			ldt_data_ord_cliente = idt_data_ord_cliente
		end if
		
		if isnull(ls_nota_piede) then ls_nota_piede = ""
		
		
		//##########################################################################################
		//17/06/2014 d'accordo con Alberto
		//QUESTO VALE SOLO PER ib_gibus = TRUE
		//In caso di Nuovo DDT, se c'è qualcosa nella variabile is_nota_piede (proveniente dalla finestra di generazione doc.fiscale)
		//si tratta delle note piede presenti in ciascun ordine (se + di uno sono concatenate) o valore digitato/modificato a mano dall'operatore
		//quindi passa esattamente questo in note piede, poi al massimo una eventuale nota fissa verrà accodata
		//##########################################################################################
		if ib_gibus and len(trim(is_nota_piede)) > 0 then
			//------------------------------------------------
			ls_nota_piede = is_nota_piede
			//------------------------------------------------
			
			//dopo l'utilizzo pulisco questa variabile per evitare ripetizioni quando rientro (caso evasione + ordini con unica fattura ma in un colpo solo, non con tasto aggiungi)
			is_nota_piede = ""
			
		else
			if len(trim(ls_nota_piede)) > 0 then
				ls_nota_piede = ls_nota_piede
			else
				ls_nota_piede = ls_nota_piede
			end if
		end if

		// ----- controllo lunghezza note per ASE
		if (upper(ls_db) = "SYBASE_ASE" or upper(ls_db) = "MSSQL") and len(ls_nota_piede) > 255 then
			ls_nota_piede = left(ls_nota_piede, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
		
		insert into tes_bol_ven
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 data_registrazione,   
						 cod_operatore,   
						 cod_tipo_bol_ven,   
						 cod_cliente,
						 cod_fornitore,
						 cod_des_cliente,   
						 cod_fil_fornitore,
						 rag_soc_1,   
						 rag_soc_2,   
						 indirizzo,   
						 localita,   
						 frazione,   
						 cap,   
						 provincia,   
						 cod_deposito,   
						 cod_deposito_tras,
						 cod_ubicazione,
						 cod_valuta,   
						 cambio_ven,   
						 cod_tipo_listino_prodotto,   
						 cod_pagamento,   
						 sconto,   
						 cod_agente_1,
						 cod_agente_2,
						 cod_banca,   
						 num_ord_cliente,   
						 data_ord_cliente,   
						 cod_imballo,   
						 aspetto_beni,
						 peso_netto,
						 peso_lordo,
						 num_colli,
						 cod_vettore,   
						 cod_inoltro,   
						 cod_mezzo,
						 causale_trasporto,
						 cod_porto,   
						 cod_resa,   
						 cod_documento,
						 numeratore_documento,
						 anno_documento,
						 num_documento,
						 data_bolla,
						 nota_testata,   
						 nota_piede,   
						 flag_fuori_fido,
						 flag_blocco,   
						 flag_movimenti,  
						 flag_riep_fat,
						 cod_banca_clien_for,
						 flag_gen_fat,
						 cod_causale,
						 data_inizio_trasporto,
						 ora_inizio_trasporto,
						 flag_doc_suc_tes,
						 flag_st_note_tes,
						 flag_doc_suc_pie,
						 flag_st_note_pie,
						 cod_natura_intra)  						 
		values      (:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ldt_data_registrazione,   
						 :ls_cod_operatore,   
						 :ls_cod_tipo_bol_ven,   
						 :ls_cod_cliente[1],
						 null,
						 :ls_cod_des_cliente,   
						 null,
						 :ls_rag_soc_1,   
						 :ls_rag_soc_2,   
						 :ls_indirizzo,   
						 :ls_localita,   
						 :ls_frazione,   
						 :ls_cap,   
						 :ls_provincia,   
						 :ls_cod_deposito[1],   
						 null,
						 :ls_cod_ubicazione[1],
						 :ls_cod_valuta,   
						 :ld_cambio_ven,   
						 :ls_cod_tipo_listino_prodotto,   
						 :ls_cod_pagamento,   
						 :ld_sconto_testata,
						 :ls_cod_agente_1,
						 :ls_cod_agente_2,
						 :ls_cod_banca,   
						 :ls_num_ord_cliente,   
						 :ldt_data_ord_cliente,   
						 :ls_cod_imballo,   
						 :ls_aspetto_beni,
						 :ld_peso_netto,
						 :ld_peso_lordo,
						 :ld_num_colli,
						 :ls_cod_vettore,   
						 :ls_cod_inoltro,   
						 :ls_cod_mezzo,   
						 :ls_causale_trasporto,
						 :ls_cod_porto,   
						 :ls_cod_resa,   
						 null,
						 null,
						 0,
						 0,
						 null,
						 :ls_nota_testata,   
						 :ls_nota_piede,   
						 'N',
						 'N',   
						 'N',
						 :ls_flag_riep_fat,
						 :ls_cod_banca_clien_for,
						 'N',
						 :ls_cod_causale,
						 :idt_data_inizio_trasporto,
						 :ldt_ora_inizio_trasporto,
						 'I',
						 'I',
						 'I',
						 'I',
						 :ls_cod_natura_intra);						 
		 if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante inserimento Testata Bolle Vendita. Dettaglio errore " + sqlca.sqlerrtext
			return -1
		 end if
		 
		if uof_crea_nota_fissa("BOL_VEN", ll_anno_registrazione, ll_num_registrazione, fs_errore) = -1 then return -1
		 
	 end if

	 declare cu_evas_ord_ven cursor for  
	 select  evas_ord_ven.prog_riga_ord_ven,   
				evas_ord_ven.prog_riga,   
				evas_ord_ven.cod_deposito,   
				evas_ord_ven.cod_ubicazione,   
				evas_ord_ven.cod_lotto,   
				evas_ord_ven.data_stock,   
				evas_ord_ven.prog_stock,   
				evas_ord_ven.quan_in_evasione
	from     evas_ord_ven  
	where    evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
				evas_ord_ven.anno_registrazione = :fl_anno_registrazione and
				evas_ord_ven.num_registrazione = :fl_num_registrazione and
				evas_ord_ven.cod_utente = :s_cs_xx.cod_utente and
				evas_ord_ven.prog_sessione = :fl_prog_sessione and
				evas_ord_ven.flag_spedito='N'
	order by evas_ord_ven.prog_riga_ord_ven asc,   
				evas_ord_ven.prog_riga asc;

	open cu_evas_ord_ven;
	
	lb_riferimento = true
	if lb_aggiungi then
		select max(prog_riga_bol_ven)
		into   :ll_prog_riga_bol_ven
		from   det_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
		if isnull(ll_prog_riga_bol_ven) then ll_prog_riga_bol_ven = 0
	else
		ll_prog_riga_bol_ven = 0	
	end if
	
	do while 0 = 0
		fetch cu_evas_ord_ven into :ll_prog_riga_ord_ven,   
											:ll_prog_riga,
											:ls_cod_deposito[1],   
											:ls_cod_ubicazione[1],   
											:ls_cod_lotto[1],   
											:ldt_data_stock[1],   
											:ll_progr_stock[1],   
											:ld_quan_in_evasione;

		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode = -1 then
			fs_errore =  "Errore durante la lettura Evasione Ordini Vendita."
			close cu_evas_ord_ven;
			return -1
		end if

		ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		
		select	det_ord_ven.cod_tipo_det_ven,   
					det_ord_ven.cod_misura,   
					det_ord_ven.cod_prodotto,   
					det_ord_ven.des_prodotto,   
					det_ord_ven.quan_ordine,
					det_ord_ven.prezzo_vendita,   
					det_ord_ven.fat_conversione_ven,   
					det_ord_ven.sconto_1,   
					det_ord_ven.sconto_2,  
					det_ord_ven.provvigione_1,
					det_ord_ven.provvigione_2,
					det_ord_ven.cod_iva,   
					det_ord_ven.quan_evasa,
					det_ord_ven.nota_dettaglio,   
					det_ord_ven.anno_commessa,
					det_ord_ven.num_commessa,
					det_ord_ven.cod_centro_costo,
					det_ord_ven.sconto_3,   
					det_ord_ven.sconto_4,   
					det_ord_ven.sconto_5,   
					det_ord_ven.sconto_6,   
					det_ord_ven.sconto_7,   
					det_ord_ven.sconto_8,   
					det_ord_ven.sconto_9,   
					det_ord_ven.sconto_10,
					det_ord_ven.cod_versione,
					det_ord_ven.num_confezioni,
					det_ord_ven.num_pezzi_confezione,
					det_ord_ven.flag_doc_suc_det,
					det_ord_ven.quantita_um,
					det_ord_ven.prezzo_um,
					det_ord_ven.nota_prodotto,
					det_ord_ven.flag_trasporta_nota_ddt
		into		:ls_cod_tipo_det_ven, 
					:ls_cod_misura, 
					:ls_cod_prodotto, 
					:ls_des_prodotto, 
					:ld_quan_ordine,
					:ld_prezzo_vendita, 
					:ld_fat_conversione_ven, 
					:ld_sconto_1, 
					:ld_sconto_2, 
					:ld_provvigione_1, 
					:ld_provvigione_2, 
					:ls_cod_iva, 
					:ld_quan_evasa,
					:ls_nota_dettaglio, 
					:ll_anno_commessa,
					:ll_num_commessa,
					:ls_cod_centro_costo,
					:ld_sconto_3, 
					:ld_sconto_4, 
					:ld_sconto_5, 
					:ld_sconto_6, 
					:ld_sconto_7, 
					:ld_sconto_8, 
					:ld_sconto_9, 
					:ld_sconto_10,
					:ls_cod_versione,
					:ll_num_confezioni,
					:ll_num_pezzi_confezione,
					:ls_flag_doc_suc_det,
					:ld_quantita_um,
					:ld_prezzo_um,
					:ls_nota_prodotto,
					:ls_flag_trasporta_nota_ddt
		  from 	det_ord_ven  
		 where 	det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
					det_ord_ven.num_registrazione = :fl_num_registrazione and
					det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven
		order by det_ord_ven.prog_riga_ord_ven asc;
	
		if sqlca.sqlcode = -1 then
			fs_errore =  "Errore durante la lettura Dettagli Ordini Vendita."
			close cu_evas_ord_ven;
			return -1
		end if

//------------------------------------------------- MODIFICA DI ENRICO PER QUANTITA' VERSATILI ------------------
// ricalcolo la quantità di vendita nel caso di evasioni parziali
		if ld_quan_in_evasione <> ld_quan_ordine then
			ld_quantita_um = round(ld_quan_in_evasione * ld_fat_conversione_ven, 4)
		end if
//------------------------------------------------- Modifica Nicola ---------------------------------------------
	if ls_flag_doc_suc_det = 'I' then //nota dettaglio
		select flag_doc_suc_or
		  into :ls_flag_doc_suc_det
		  from tab_tipi_det_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		
	
	
	if ls_flag_doc_suc_det = 'N' then
		setnull(ls_nota_dettaglio)
	end if				

//-------------------------------------------------- Fine Modifica ----------------------------------------------		
//------------------------------------------------- Modifica Michela ---------------------------------------------
// se si può allora aggiungo la nota prodotto alla nota dettaglio
	if ls_flag_trasporta_nota_ddt = "S" then
		if isnull(ls_nota_dettaglio) then 
			ls_nota_dettaglio = "Nota prodotto:" + ls_nota_prodotto
		else
			if not isnull(ls_nota_prodotto) then
				ls_nota_dettaglio = ls_nota_dettaglio + "~r~nNota prodotto:" + ls_nota_prodotto
			end if
		end if
	end if
//-------------------------------------------------- Fine Modifica ----------------------------------------------					
		select tab_tipi_det_ven.flag_tipo_det_ven,
				 tab_tipi_det_ven.cod_tipo_movimento,
				 tab_tipi_det_ven.flag_sola_iva
		into   :ls_flag_tipo_det_ven,
				 :ls_cod_tipo_movimento,
				 :ls_flag_sola_iva
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode = -1 then
			fs_errore = "Si è verificato un errore in fase di lettura tipi dettagli."
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
				fs_errore = "Si è verificato un errore in fase di creazione destinazioni movimenti."
				close cu_evas_ord_ven;
				return -1
			end if

			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if lb_riferimento and ls_flag_abilita_rif_ord_ven = "D" then
			lb_riferimento = false
			setnull(ls_note_dettaglio_rif)
			
			ls_des_riferimento = ls_des_riferimento_ord_ven
			if ls_flag_rif_anno_ord_ven = "S" and not isnull(fl_anno_registrazione) then
				ls_des_riferimento = ls_des_riferimento + "-" +string(fl_anno_registrazione)
			end if
			if ls_flag_rif_num_ord_ven = "S" and not isnull(fl_num_registrazione) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(fl_num_registrazione)
			end if
			if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
				ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
			end if
			
			if not isnull(is_rif_packing_list) and len(is_rif_packing_list) > 0 then
				if not isnull(ls_note_dettaglio_rif) and len(ls_note_dettaglio_rif) > 0 then 
					ls_note_dettaglio_rif += "~r~n"
				else
					ls_note_dettaglio_rif = ""
				end if
				ls_note_dettaglio_rif = ls_note_dettaglio_rif + is_rif_packing_list
				setnull(is_rif_packing_list)
			end if
			
			if not isnull(ls_num_ord_cliente) then
				if not isnull(ls_note_dettaglio_rif) and len(ls_note_dettaglio_rif) > 0 then 
					ls_note_dettaglio_rif += "~r~n"
				else
					ls_note_dettaglio_rif = ""
				end if
				ls_note_dettaglio_rif = ls_note_dettaglio_rif + "Vs.Ordine " + ls_num_ord_cliente
				if not isnull(ldt_data_ord_cliente) then
					ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
				end if
			end if
			
			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_ord_ven, "C", ls_cod_cliente[1], ref ls_cod_iva_rif) <> 0 then
				g_mb.messagebox("Generazione Bolla Riepilogata","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if
			
			insert into det_bol_ven  
							(cod_azienda,   
							 anno_registrazione,   
							 num_registrazione,   
							 prog_riga_bol_ven,   
							 cod_tipo_det_ven,   
							 cod_deposito,
							 cod_ubicazione,
							 cod_lotto,
							 progr_stock,
							 data_stock,
							 cod_misura,   
							 cod_prodotto,   
							 des_prodotto,   
							 quan_consegnata,   
							 prezzo_vendita,   
							 fat_conversione_ven,   
							 sconto_1,   
							 sconto_2,   
							 provvigione_1,
							 provvigione_2,
							 cod_iva,   
							 cod_tipo_movimento,
							 num_registrazione_mov_mag,
							 nota_dettaglio,   
							 anno_registrazione_ord_ven,   
							 num_registrazione_ord_ven,   
							 prog_riga_ord_ven,
							 anno_commessa,
							 num_commessa,
							 cod_centro_costo,
							 sconto_3,   
							 sconto_4,   
							 sconto_5,   
							 sconto_6,   
							 sconto_7,   
							 sconto_8,   
							 sconto_9,   
							 sconto_10,
							 anno_registrazione_mov_mag,
							 anno_reg_bol_acq,
							 num_reg_bol_acq,
							 prog_riga_bol_acq,
							 anno_reg_des_mov,
							 num_reg_des_mov,
							 cod_versione,
							 num_confezioni,
							 num_pezzi_confezione,
							 flag_doc_suc_det,
							 flag_st_note_det,
							 quantita_um,
							 prezzo_um)							 
		  values			(:s_cs_xx.cod_azienda,   
							 :ll_anno_registrazione,   
							 :ll_num_registrazione,   
							 :ll_prog_riga_bol_ven,   
							 :ls_cod_tipo_det_ven_rif_ord_ven,   
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,   
							 null,   
							 :ls_des_riferimento,   
							 0,   
							 0,   
							 1,   
							 0,   
							 0,  
							 0,
							 0,
							 :ls_cod_iva_rif,   
							 null,
							 null,
							 :ls_note_dettaglio_rif,   
							 null,   
							 null,   
							 null,
							 null,   
							 null,   
							 null,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,   
							 0,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 null,
							 0,
							 0,
							 'I',
							 'I',
							 0,
							 0);							 
	
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore durante la scrittura Dettagli Bolle Vendita."
				close cu_evas_ord_ven;
				return -1
			end if
			ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
		end if	
		
		// stefanop 09/05/2010: progetto 140: controllo se devo cambiare il dettaglio
		if this.ib_change_det_ven then
			// cambio il dettaglio solo se uguale a quello di default, altrimenti lascio invariato
			if ls_cod_tipo_det_ven_ord = ls_cod_tipo_det_ven then
				ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_new
			end if
		end if
		
		if this.ib_change_mov then
			if ls_cod_tipo_movimento = ls_cod_tipo_movimento_ord then
				ls_cod_tipo_movimento = ls_cod_tipo_movimento_new
			end if
		end if
		// ---
		
		//########################################################################################
		//Donato 27/06/2012: per nessun motivo deve finire in bolla una quantità superiore a quella della riga di ordine vendita riferito
		if uof_controlla_qta_in_documento(fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven, ld_quan_in_evasione, fs_errore) < 0 then
			//in fs_errore il messaggio
			close cu_evas_ord_ven;
			return -1
		end if
		//########################################################################################

		insert into det_bol_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_bol_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 progr_stock,
						 data_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_consegnata,   
						 prezzo_vendita,   
						 fat_conversione_ven,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 nota_dettaglio,   
						 anno_registrazione_ord_ven,   
						 num_registrazione_ord_ven,   
						 prog_riga_ord_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 anno_reg_bol_acq,
						 num_reg_bol_acq,
						 prog_riga_bol_acq,
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 cod_versione,
						 num_confezioni,
						 num_pezzi_confezione,
						 flag_doc_suc_det,
						 flag_st_note_det,
						 quantita_um,
						 prezzo_um)
	  values			(:s_cs_xx.cod_azienda,   
						 :ll_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_bol_ven,   
						 :ls_cod_tipo_det_ven,   
						 :ls_cod_deposito[1],
						 :ls_cod_ubicazione[1],
						 :ls_cod_lotto[1],
						 :ll_progr_stock[1],
						 :ldt_data_stock[1],
						 :ls_cod_misura,   
						 :ls_cod_prodotto,   
						 :ls_des_prodotto,   
						 :ld_quan_in_evasione,   
						 :ld_prezzo_vendita,   
						 :ld_fat_conversione_ven,   
						 :ld_sconto_1,   
						 :ld_sconto_2,  
						 :ld_provvigione_1,
						 :ld_provvigione_2,
						 :ls_cod_iva,   
						 :ls_cod_tipo_movimento,
						 null,
						 :ls_nota_dettaglio,   
						 :fl_anno_registrazione,   
						 :fl_num_registrazione,   
						 :ll_prog_riga_ord_ven,
						 :ll_anno_commessa,   
						 :ll_num_commessa,   
						 :ls_cod_centro_costo,   
						 :ld_sconto_3,   
						 :ld_sconto_4,   
						 :ld_sconto_5,   
						 :ld_sconto_6,   
						 :ld_sconto_7,   
						 :ld_sconto_8,   
						 :ld_sconto_9,   
						 :ld_sconto_10,
						 null,
						 null,
						 null,
						 null,
						 :ll_anno_reg_des_mov,
						 :ll_num_reg_des_mov,
						 :ls_cod_versione,
						 :ll_num_confezioni,
						 :ll_num_pezzi_confezione,
						 'I',
						 'I',
						 :ld_quantita_um,
						 :ld_prezzo_um);								 
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura Dettagli Bolle Vendita."
			close cu_evas_ord_ven;
			return -1
		end if

		if ls_flag_tipo_det_ven = "M" then
			update anag_prodotti  
				set quan_assegnata = quan_assegnata - :ld_quan_in_evasione,
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode <> 0 then
				fs_errore = "Si è verificato un errore in fase di aggiornamento magazzino."
				close cu_evas_ord_ven;
				return -1
			end if
			
			update stock 
			set 	 quan_assegnata = quan_assegnata - :ld_quan_in_evasione,   
					 quan_in_spedizione = quan_in_spedizione + :ld_quan_in_evasione  
			where  stock.cod_azienda = :s_cs_xx.cod_azienda and 
					 stock.cod_prodotto = :ls_cod_prodotto and 
					 stock.cod_deposito = :ls_cod_deposito[1] and 
					 stock.cod_ubicazione = :ls_cod_ubicazione[1] and 
					 stock.cod_lotto = :ls_cod_lotto[1] and 
					 stock.data_stock = :ldt_data_stock[1] and 
					 stock.prog_stock = :ll_progr_stock[1];

			if sqlca.sqlcode <> 0 then
				fs_errore = "Si è verificato un errore in fase di aggiornamento stock."
				close cu_evas_ord_ven;
				return -1
			end if
		end if				

		ls_tabella_orig = "det_ord_ven_stat"
		ls_tabella_des = "det_bol_ven_stat"
		ls_prog_riga_doc_orig = "prog_riga_ord_ven"
		ls_prog_riga_doc_des = "prog_riga_bol_ven"

		if f_genera_det_stat(ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven) = -1 then
			close cu_evas_ord_ven;
			return -1
		end if	

		if ls_cod_tipo_analisi_bol <> ls_cod_tipo_analisi_ord then
			if f_crea_distribuzione(ls_cod_tipo_analisi_bol, ls_cod_prodotto, ls_cod_tipo_det_ven, ll_num_registrazione, ll_anno_registrazione, ll_prog_riga_bol_ven, 5) = -1 then
				fs_errore = "Si è verificato un errore nella Generazione dei Dettagli Statistici."
				close cu_evas_ord_ven;
				return -1
			end if
		end if

		if ld_quan_ordine <= ld_quan_evasa + ld_quan_in_evasione then
			ls_flag_evasione = "E"
		else
			ls_flag_evasione = "P"
		end if
		
		update det_ord_ven  
			set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione,
				 quan_evasa = quan_evasa + :ld_quan_in_evasione,
				 flag_evasione = :ls_flag_evasione
		 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
				 det_ord_ven.num_registrazione = :fl_num_registrazione and
				 det_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
		if sqlca.sqlcode = -1 then
			fs_errore = "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Vendita."
			close cu_evas_ord_ven;
			return -1
		end if
		
		
		//######################################################################################
		//Donato 27/06/2012 cerco di evitare i casi in cui si possono avere residuo negativo
		//la casistica prevede ad esempio che 
		//1)		la quantità in evasione non si azzera dopo l'evasione, 
		//			e allora il residuo dato da quan_ordine - quan_in_evasione - quan_evasa da un valore minore di zero (caso SACCOLONGO)
		
		//2)		in altri casi si ha invece un azzeramento (giustamente) della quan_in_evasione ma una quantità_evasa superiore a quella dell'ordine (caso VEGGIANO)
		
		uof_correggi_errori_in_evasione(fl_anno_registrazione, fl_num_registrazione, ll_prog_riga_ord_ven)
		//###########################################################
		

		if ls_flag_sola_iva = 'N' then
			ld_val_riga = ld_quan_in_evasione * ld_prezzo_vendita
			ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
			ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
			ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
			ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
			ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
			ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
			ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
			ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
			ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
			ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
			ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
			ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
			ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
			ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
			ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
			ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
			ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
			ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
			ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
			if ls_cod_valuta = ls_lire then
				ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
			else
				ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
			end if   
	
			if ls_flag_tipo_det_ven = "M" or &
				ls_flag_tipo_det_ven = "C" or &
				ls_flag_tipo_det_ven = "N" then
				ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
				ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
				ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
				ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
			elseif ls_flag_tipo_det_ven = "S" then
				ld_val_evaso = ld_val_riga_sconto_10 * -1
			else
				ld_val_evaso = ld_val_riga_sconto_10
			end if
			ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
		end if

		if ls_cod_valuta = ls_lire then
			ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
		end if   
		
		//Donato cancellare le righe metto flag_spedito S
		update evas_ord_ven
		set flag_spedito='S'
		where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
						evas_ord_ven.anno_registrazione = :fl_anno_registrazione and
						evas_ord_ven.num_registrazione = :fl_num_registrazione and 
						evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
						evas_ord_ven.prog_riga = :ll_prog_riga and
						evas_ord_ven.cod_utente = :s_cs_xx.cod_utente and
						evas_ord_ven.prog_sessione = :fl_prog_sessione;
		
//		delete from evas_ord_ven 
//		where    	evas_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and
//						evas_ord_ven.anno_registrazione = :fl_anno_registrazione and
//						evas_ord_ven.num_registrazione = :fl_num_registrazione and 
//						evas_ord_ven.prog_riga_ord_ven = :ll_prog_riga_ord_ven and 
//						evas_ord_ven.prog_riga = :ll_prog_riga and
//						evas_ord_ven.cod_utente = :s_cs_xx.cod_utente;

		if sqlca.sqlcode < 0 then
			fs_errore = "Si è verificato un errore in fase di cancellazione Evasione Ordini."
			close cu_evas_ord_ven;
			return -1
		end if
	loop
	close cu_evas_ord_ven;

	if ld_tot_val_ordine <= ld_tot_val_evaso then
		ls_flag_evasione = "E"
	else
		ls_flag_evasione = "P"
	end if

//	update tes_ord_ven  
//		set tot_val_evaso = :ld_tot_val_evaso
//	 where tes_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//			 tes_ord_ven.anno_registrazione = :fl_anno_registrazione and  
//			 tes_ord_ven.num_registrazione = :fl_num_registrazione;
//
//	if sqlca.sqlcode <> 0 then
//		fs_errore = "Si è verificato un errore in fase di aggiornamento Valore Evaso in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
//		return -1
//	end if
	
	if uof_calcola_stato_ordine(fl_anno_registrazione,fl_num_registrazione) = -1 then
		fs_errore = "Si è verificato un errore in fase di aggiornamento stato dell'ordine in Testata Ordine Vendita.~r~nErrore DB Nr." + string(sqlca.sqlcode) + " " + sqlca.sqlerrtext
		return -1
	end if

	
	// ORA CALCOLO IL DOCUMENTO PER CREARE GLI IMPONIBILI NECESSARI AL SUCCESSIVO CALCOLO DELLE SPES EDI TRASPORTO
	// QUINDI NON RIMUOVERE IL SUCCESSIVO CALCOLO
	luo_calcolo = create uo_calcola_documento_euro

	if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_ven",fs_errore) <> 0 then
		destroy luo_calcolo
		return -1
	end if
	
	// *****************************
	// Calcolo le spese di trasporto
	uo_spese_trasporto luo_spese_trasporto
	luo_spese_trasporto = create uo_spese_trasporto
	
	if luo_spese_trasporto.uof_calcola_spese(ll_anno_registrazione, ll_num_registrazione, "BOLVEN", ref fs_errore) < 0 then
		destroy luo_spese_trasporto
		return -1
	end if
	
	// di seguito ri-calcolo il documento per includere nel calcolo le spese di trasporto
	if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_ven", fs_errore) <> 0 then
		destroy luo_calcolo
		return -1
	end if
	
	//Donato 30/05/2014
	//SR Migliorie_spese_trasporto: la finestra storico si deve aprire anche per i D.d.T.
	if ib_visualizza_spese_trasp then
		//valori di ritorno della funzione
		//0		SPESE TRASPORTO CONFERMATE (o non abilitate dal parametro SSP), quindi non fare null'altro
		//1		SPESE TRASPORTO ANNULLATE, quindi chiama la funzione che toglie le righe trasporto e che ricalcola il documento
		//2		SPESE TRASPORTO DA SOLE PERCENTUALI IMPONIBILE RIGHE ORDINE. quindi chiama funzione opportuna
		//NOTA BENE anche nei nei casi di ritorno 1 o 2 ci sarà un commit fatto in tal caso all'uscita di questa funzione
		li_risposta = luo_spese_trasporto.uof_apri_storico(ll_anno_registrazione, ll_num_registrazione, "BOLVEN")
		
		if li_risposta = 0 then
		else
			if li_risposta = 1 then
				//--------------------------------------------------------------------------------------------------------------------------------------------
				//ANNULLAMENTO SPESE TRASPORTO
				li_risposta = luo_spese_trasporto.uof_azzera_trasporto_documento(ll_anno_registrazione, ll_num_registrazione, "BOLVEN", fs_errore)
				//--------------------------------------------------------------------------------------------------------------------------------------------
			elseif li_risposta = 2 then
				//--------------------------------------------------------------------------------------------------------------------------------------------
				//SPESE TRASPORTO DA SOLE PERCENTUALI IMPONIBILE RIGHE ORDINE
				li_risposta = luo_spese_trasporto.uof_imposta_trasporto_solo_percentuale(ll_anno_registrazione, ll_num_registrazione, "BOLVEN", fs_errore)
				//--------------------------------------------------------------------------------------------------------------------------------------------
			end if
			
			if li_risposta<0 then
				destroy luo_spese_trasporto
				destroy luo_calcolo
				return -1
			end if
		end if
		
	end if
	
	destroy luo_spese_trasporto
	destroy luo_calcolo

	fl_anno_reg_bol_ven = ll_anno_registrazione
	fl_num_reg_bol_ven = ll_num_registrazione
end if
return 0

end function

public function integer uof_azz_ass_ord_ven (long al_anno_registrazione, long al_num_registrazione, string as_perc, ref string as_messaggio);boolean				lb_fine

integer				li_return

long					ll_prog_riga_ord_ven, ll_prog_stock, ll_prog_riga, ll_anno_registrazione, &
						ll_num_registrazione
						
string					ls_cod_prodotto, ls_sql_stringa, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_ubicazione, ls_perc, &
						ls_flag_tipo_det_ven, ls_cod_tipo_det_ven, ls_flag_evasione,ls_cod_prodotto_raggruppato
						
dec{4}				ld_quan_ordine, ld_quan_in_evasione, ld_quan_evasa, ld_giacenza_stock, ld_quan_ripristino_quan_impegnata, &
						ld_quan_assegnata, ld_quan_in_spedizione, ld_quan_residua, ld_quan_raggruppo, ld_quan_assegnata_temp
						
datetime				ldt_data_stock

datastore			lds_data

longlong				ll_prog_sessione

li_return = 0


//mi prendo i dati relativi all'ultima spedizione, e cioè quelli con il valore + alto di prog_sessione ##########################################
ls_sql_stringa = 	"select max(prog_sessione) "+&
						"from evas_ord_ven "+&
						 "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(al_anno_registrazione) + " and num_registrazione = " + string(al_num_registrazione) + " "

ll_prog_sessione = 0
if left(as_perc,1) = '%' then		// verifico se devo fare intero ordine oppure una riga specifica
	if len(as_perc) > 1 then
		ll_prog_sessione = longlong( mid(as_perc,2) )
	end if
else
	ls_sql_stringa += " and prog_riga_ord_ven=" + as_perc + " "
end if

if ll_prog_sessione > 0 then
	
	li_return = guo_functions.uof_crea_datastore( lds_data, ls_sql_stringa, as_messaggio)
	if li_return>0 then
		//leggi prog_sessione
		ll_prog_sessione = lds_data.getitemnumber(1, 1)
		if ll_prog_sessione<= 0 then
			as_messaggio = "Impossibile annullare! Sessione non valida (ordine "+string(al_anno_registrazione) + "/" + string(al_num_registrazione)+")"
			if as_perc='%' then
			else
				as_messaggio += " - riga "+as_perc
			end if
			rollback;
			return 0
		end if
		
	elseif li_return=0 then
		//non sono state trovate spedizioni pendenti ...
		as_messaggio = "Nessuna spedizione trovata per l'ordine "+string(al_anno_registrazione) + "/" + string(al_num_registrazione)
		if as_perc='%' then
		else
			as_messaggio += " - riga "+as_perc
		end if
		
		rollback;
		return 0
		
	else
		//in as_messaggio il messaggio di errore
		rollback;
		return 0
	end if
	//##################################################################################################
end if


declare cu_evas_ord_ven dynamic cursor for sqlsa;
//imposto sql di base
ls_sql_stringa = "select anno_registrazione, " + &
						  "num_registrazione, " + &
						  "prog_riga_ord_ven, " + &
						  "prog_riga, " + &
						  "cod_deposito, " + &
						  "cod_ubicazione, " + &
						  "cod_lotto, " + &
						  "data_stock, " + &
						  "prog_stock, " + &
						  "cod_prodotto, " + &
						  "quan_in_evasione " + &
						  "from  evas_ord_ven " + &
						  "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(al_anno_registrazione) + " and num_registrazione = " + string(al_num_registrazione) + &
						  			" and prog_sessione="+string(ll_prog_sessione) + " "

if as_perc='%' then		// verifico se devo fare intero ordine oppure una riga specifica
else
	ls_sql_stringa += " and prog_riga_ord_ven = " + as_perc + " "
end if

 ls_sql_stringa += " order by prog_riga,prog_riga_ord_ven "

prepare sqlsa from :ls_sql_stringa;

open dynamic cu_evas_ord_ven;
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in fase di OPEN cursore evas_ord_ven (uof_azz_ass_ord_ven).~r~n" + sqlca.sqlerrtext
	rollback;
	return 0
end if	

do while 0 = 0
	fetch cu_evas_ord_ven into		:ll_anno_registrazione, 
												:ll_num_registrazione, 
												:ll_prog_riga_ord_ven, 
												:ll_prog_riga,
												:ls_cod_deposito,
												:ls_cod_ubicazione,
												:ls_cod_lotto,
												:ldt_data_stock,
												:ll_prog_stock,
												:ls_cod_prodotto, 
												:ld_quan_in_evasione; 

	if sqlca.sqlcode = -1 then
		as_messaggio = "Errore in fase di lettura evas_ord_ven. Dettaglio Errore:" + sqlca.sqlerrtext
		close cu_evas_ord_ven;
		rollback;
		return 0
	end if

	if sqlca.sqlcode = 100 then exit
	
	//leggo i dati della riga dell'ordine (tipo dettaglio, quantita ordine e cod prodotto)
	select		cod_tipo_det_ven, 
				quan_ordine, 
				cod_prodotto
	into   		:ls_cod_tipo_det_ven, 
				:ld_quan_ordine, 
				:ls_cod_prodotto
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_registrazione = :al_anno_registrazione and  
			 num_registrazione = :al_num_registrazione and
			 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
			 
	if sqlca.sqlcode <> 0 then
		as_messaggio = "Errore in fase di lettura dettagli righe ordine. Dettaglio Errore:" + sqlca.sqlerrtext
		close cu_evas_ord_ven;
		rollback;
		return 0
	end if

	//se la quantità spedita supera quella dell'ordine mi limiterò a ripristinare quella dell'ordine
	if ld_quan_in_evasione > ld_quan_ordine then
		ld_quan_ripristino_quan_impegnata = ld_quan_ordine
	else
		ld_quan_ripristino_quan_impegnata = ld_quan_in_evasione
	end if
	
	// cerco il flag tipo dettaglio per verificare se è prodotto a magazzino
	select flag_tipo_det_ven
	into :ls_flag_tipo_det_ven
	from tab_tipi_det_ven
	where cod_azienda = :s_cs_xx.cod_azienda
	and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in fase di lettura Tipi Dettaglio Vendita. Dettaglio Errore:" + sqlca.sqlerrtext
		return 0
	end if			
	
	//se tipo dettaglio è Prodotti a Magazzino, faccio cose aggiuntive
	if ls_flag_tipo_det_ven = 'M' and not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
		
		//aggiorno la quantà impegnata e quella assegnata (ma primo verifico caso mai va in negativo quella assegnata)
		select quan_assegnata
		into :ld_quan_assegnata_temp
		from anag_prodotti
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_prodotto = :ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			as_messaggio = "Errore in lettura q.tà impegnata e assegnata per il  prodotto "+ls_cod_prodotto+": " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			rollback;
			return 0
		end if
		
		if isnull(ld_quan_assegnata_temp) then ld_quan_assegnata_temp=0
		
		if ld_quan_assegnata_temp - ld_quan_in_evasione < 0 then
			//evito l'errore che va in negativo, quindi asewmplicemenet azzero la quantità assegnata in anag_prodotti
			update anag_prodotti  
			set quan_impegnata = quan_impegnata + :ld_quan_ripristino_quan_impegnata,
				 quan_assegnata = 0
			 where cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto;
					 
		else
			//OK, procedo tranquillamente, tanto niente può andare in negativo
			update anag_prodotti  
			set quan_impegnata = quan_impegnata + :ld_quan_ripristino_quan_impegnata,
				 quan_assegnata = quan_assegnata - :ld_quan_in_evasione
			 where cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto;
		end if
		
		if sqlca.sqlcode <> 0 then
			as_messaggio = "Errore in fase di aggiornamento impegnato e assegnato del prodotto "+ls_cod_prodotto+": " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			rollback;
			return 0
		end if
		
		
		// enme 08/1/2006 gestione prodotto raggruppato ------------------------------------------------
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then

			ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ripristino_quan_impegnata, "M")
			
			update anag_prodotti  
			set quan_impegnata = quan_impegnata + :ld_quan_raggruppo
			where cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto_raggruppato;
					 
			if sqlca.sqlcode < 0 then
				as_messaggio = "Errore in fase di aggiornamento prodotto raggruppato "+ls_cod_prodotto_raggruppato+" del prodotto "+ls_cod_prodotto+": " + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				rollback;
				return 0
			end if
		end if
		//fine gestione prodotto raggruppato ---------------------------------------------------------------------------------
		
		//aggiornamento quantità assegnata dello stock ------------------------------------------------------------------------------------------------
		//controlla caso mai va in negativo, però ...
		select quan_assegnata
		into :ld_quan_assegnata_temp
		from stock
		 where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto and 
				 cod_deposito = :ls_cod_deposito and
				 cod_ubicazione = :ls_cod_ubicazione and
				 cod_lotto = :ls_cod_lotto and
				 data_stock = :ldt_data_stock and
				 prog_stock = :ll_prog_stock;
		
		if sqlca.sqlcode < 0 then
			as_messaggio = "Errore in lettura q.tà assegnata dello stock per il  prodotto "+ls_cod_prodotto+": " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			rollback;
			return 0
		end if
		
		if isnull(ld_quan_assegnata_temp) then ld_quan_assegnata_temp=0
		
		if ld_quan_assegnata_temp - ld_quan_in_evasione < 0 then
			//evito l'errore che va in negativo, quindi semplicemente azzero la quantità assegnata in stock
			update stock  
			set quan_assegnata = 0
			 where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto and 
					 cod_deposito = :ls_cod_deposito and
					 cod_ubicazione = :ls_cod_ubicazione and
					 cod_lotto = :ls_cod_lotto and
					 data_stock = :ldt_data_stock and
					 prog_stock = :ll_prog_stock;
					 
		else
			//OK, procedo tranquillamente, tanto niente può andare in negativo
			update stock  
			set quan_assegnata = quan_assegnata - :ld_quan_in_evasione 
			 where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto and 
					 cod_deposito = :ls_cod_deposito and
					 cod_ubicazione = :ls_cod_ubicazione and
					 cod_lotto = :ls_cod_lotto and
					 data_stock = :ldt_data_stock and
					 prog_stock = :ll_prog_stock;
		end if
			 
		if sqlca.sqlcode < 0 then
			as_messaggio = "Errore in fase di aggiornamento stock. Dettaglio Errore:" + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			rollback;
			return 0
		end if
	end if
	
	
	//aggiorno la quantita in evasione in det_ord_ven, verificando prima se la porto in negativo
	select quan_in_evasione
	into :ld_quan_assegnata_temp
	from det_ord_ven
	 where cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_registrazione = :al_anno_registrazione and  
			 num_registrazione = :al_num_registrazione and
			 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in lettura q.tà  in evasione in det_ord_ven: " + sqlca.sqlerrtext
		close cu_evas_ord_ven;
		rollback;
		return 0
	end if
	
	if isnull(ld_quan_assegnata_temp) then ld_quan_assegnata_temp=0
	
	if ld_quan_assegnata_temp < ld_quan_in_evasione then
		update det_ord_ven
		set quan_in_evasione = 0
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 anno_registrazione = :al_anno_registrazione and  
				 num_registrazione = :al_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	else
		update det_ord_ven
		set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 anno_registrazione = :al_anno_registrazione and  
				 num_registrazione = :al_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	end if

	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in fase di aggiornamento riga ordine. Dettaglio Errore:" + sqlca.sqlerrtext
		rollback;
		close cu_evas_ord_ven;
		return 0
	end if

	delete evas_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and 
			anno_registrazione = :al_anno_registrazione and 
			num_registrazione  = :al_num_registrazione and
			prog_riga_ord_ven  = :ll_prog_riga_ord_ven and
			prog_riga = :ll_prog_riga; // and
			//prog_sessione=:ll_prog_sessione;
	
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in fase di cancellazione evas_ord_ven.Dettaglio Errore:" + sqlca.sqlerrtext
		rollback;
		return 0
	end if

	li_return = 1
loop

close cu_evas_ord_ven;

commit;

return li_return
end function

public function integer uof_import_righe_documento_excel (string fs_path_file, long fl_riga_partenza, string fs_tipo_documento, long fl_anno_documento, long fl_num_documento, string fs_cod_tipo_det_ven, ref string fs_errore);/*
					FUNZIONE CHE DATO UN FILE EXCEL CONTENENTE QUANTITA' E PREZZO, ESEGUE IL CARICAMENTO DEL DOCUMENTO

VARIABILE				SIGNIFICATO
fs_path_file				percorso del file di da importare
fl_riga_partenza		riga dalla quale partono i dati da importare (potrebbe essere che la prima sia di intestazione colonne)
fs_tipo_documento	tipo documento in cui caricare (FAT_VEN - BOL_VEN - ORD_VEN - OFF_VEN - ORD_ACQ - BOL_ACQ - FAT_ACQ)
fs_cod_tipo_det_ven	tipo dettaglio di vendita da associare alla riga importata
*/
boolean 	lb_ret, lb_errore=false
string		ls_cod_prodotto,ls_flag_tipo_det_ven, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_cod_tipo_movimento, &
			ls_cod_cliente[], ls_cod_fornitore[], ls_cod_iva_det, ls_cod_deposito_tes, ls_cod_iva_cliente, ls_cod_cliente_tes
long		ll_riga,ll_prog_riga_documento, ll_prog_stock[], ll_ordinamento,ll_anno_reg_des_mov, ll_num_reg_des_mov
dec{4}	ld_quantita, ld_prezzo
datetime	ldt_data_stock[], ldt_data_registrazione, ldt_data_esenzione_iva
uo_excel luo_excel


ldt_data_registrazione = datetime(today(), 00:00:00)

if isnull(fs_cod_tipo_det_ven) then
	fs_errore = "Atttenzione: il tipo dettaglio è un DATO indispensabile! " 
	return -1
end if

fs_tipo_documento = upper(fs_tipo_documento)

choose case fs_tipo_documento
	case "OFF_VEN", "ORD_VEN", "BOL_VEN", "FAT_VEN"
		select		flag_tipo_det_ven, 
					cod_tipo_movimento, 
					cod_iva
		into		:ls_flag_tipo_det_ven, 
					:ls_cod_tipo_movimento, 
					:ls_cod_iva_det
		from		tab_tipi_det_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_det_ven = :fs_cod_tipo_det_ven;
	case "ORD_ACQ", "BOL_ACQ", "FAT_ACQ"
		select		flag_tipo_det_acq, 
					cod_tipo_movimento, 
					cod_iva
		into		:ls_flag_tipo_det_ven, 
					:ls_cod_tipo_movimento, 
					:ls_cod_iva_det
		from		tab_tipi_det_acq
		where	cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_det_acq = :fs_cod_tipo_det_ven;
end choose			
			
if sqlca.sqlcode = 100 then
	fs_errore = "Tipo dettaglio non trovato in tabella tipi dettaglio"
	return -1
end if
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore in ricerca tipo dettaglio in tabella tipi dettaglio. " + sqlca.sqlerrtext
	return -1
end if

luo_excel = create uo_excel
lb_ret = luo_excel.uof_open(fs_path_file, false, true)
if not lb_ret then
	fs_errore = "Errore nell'apertura del file:" + fs_path_file
	return -1
end if

choose case fs_tipo_documento
	case "OFF_VEN"
		select		max(prog_riga_off_ven)
		into		:ll_prog_riga_documento
		from		det_off_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :fl_anno_documento and
					num_registrazione = :fl_num_documento;
					
		select		cod_deposito, 
					cod_cliente
		into		:ls_cod_deposito_tes,
					:ls_cod_cliente_tes
		from		tes_off_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :fl_anno_documento and
					num_registrazione = :fl_num_documento;
					
	case "ORD_VEN" 
		select		max(prog_riga_ord_ven)
		into		:ll_prog_riga_documento
		from		det_ord_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :fl_anno_documento and
					num_registrazione = :fl_num_documento;
					
		select		cod_deposito, 
					cod_cliente
		into		:ls_cod_deposito_tes,
					:ls_cod_cliente_tes
		from		tes_ord_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :fl_anno_documento and
					num_registrazione = :fl_num_documento;
					
	case "BOL_VEN"
		select		max(prog_riga_bol_ven)
		into		:ll_prog_riga_documento
		from		det_bol_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :fl_anno_documento and
					num_registrazione = :fl_num_documento;
					
		select		cod_deposito, 
					cod_cliente
		into		:ls_cod_deposito_tes,
					:ls_cod_cliente_tes
		from		tes_bol_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :fl_anno_documento and
					num_registrazione = :fl_num_documento;

	case "FAT_VEN"
		select		max(prog_riga_fat_ven)
		into		:ll_prog_riga_documento
		from		det_fat_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :fl_anno_documento and
					num_registrazione = :fl_num_documento;
					
		select		cod_deposito, 
					cod_cliente
		into		:ls_cod_deposito_tes,
					:ls_cod_cliente_tes
		from		tes_fat_ven
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :fl_anno_documento and
					num_registrazione = :fl_num_documento;

	case "ORD_ACQ"
		select		max(prog_riga_ordine_acq)
		into		:ll_prog_riga_documento
		from		det_ord_acq
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :fl_anno_documento and
					num_registrazione = :fl_num_documento;
	case "BOL_ACQ"
		select		max(prog_riga_bolla_acq)
		into		:ll_prog_riga_documento
		from		det_bol_acq
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_bolla_acq = :fl_anno_documento and
					num_bolla_acq = :fl_num_documento;
	case "FAT_ACQ"
		select		max(prog_riga_fat_acq)
		into		:ll_prog_riga_documento
		from		det_fat_acq
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :fl_anno_documento and
					num_registrazione = :fl_num_documento;
end choose			

if isnull(ll_prog_riga_documento) or ll_prog_riga_documento = 0 then
	ll_prog_riga_documento = 10
else
	ll_prog_riga_documento = ll_prog_riga_documento + 10
end if

ll_riga = fl_riga_partenza

do while true
	
	try
		ld_quantita = luo_excel.uof_read(ll_riga, "D")
		ld_quantita = round(ld_quantita, 4)
		// esco
		if isnull(ld_quantita) or ld_quantita = 0 then exit
		
		ls_cod_prodotto = luo_excel.uof_read(ll_riga, "A")
		ld_prezzo   = luo_excel.uof_read(ll_riga, "E")
		ld_prezzo = round(ld_prezzo, 4)
		
	catch ( PBXRuntimeError re )
		fs_errore = "fs_errore in lettura foglio Excel; dettaglio errore: " + re.getMessage()
		lb_errore = true	
	end try
	
	pcca.mdi_frame.setmicrohelp(string(ll_riga) + " - " + ls_cod_prodotto)
	
	
	if lb_errore then exit
	
	if ls_flag_tipo_det_ven = "M" then
		
		ll_ordinamento = 0		
		
		select min(ordinamento)
		into   :ll_ordinamento
		from   det_tipi_movimenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_movimento = :ls_cod_tipo_movimento;
				 
		if ll_ordinamento <> 0 and not isnull(ll_ordinamento) then
			setnull(ls_cod_deposito[1])
			setnull(ls_cod_ubicazione[1])
			setnull(ls_cod_lotto[1])
			setnull(ldt_data_stock[1])
			setnull(ll_prog_stock[1])
			setnull(ls_cod_cliente[1])
			setnull(ls_cod_fornitore[1])
			
			select cod_deposito,
					 cod_ubicazione,
					 cod_lotto,
					 data_stock,
					 prog_stock
			into     :ls_cod_deposito[1],
					 :ls_cod_ubicazione[1],
					 :ls_cod_lotto[1],
					 :ldt_data_stock[1],
					 :ll_prog_stock[1]
			from   det_tipi_movimenti
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_movimento = :ls_cod_tipo_movimento and
					 ordinamento = :ll_ordinamento;
					 
			if sqlca.sqlcode <> 0 then
				fs_errore = "Errore in ricerca dati stock da tipi_movimenti " + sqlca.sqlerrtext
				lb_errore = true
				exit
			end if
			
		end if
		
	else
		
		setnull(ls_cod_deposito[1])
		setnull(ls_cod_ubicazione[1])
		setnull(ls_cod_lotto[1])
		setnull(ldt_data_stock[1])
		setnull(ll_prog_stock[1])  
		
	end if
	
	select cod_misura_mag,
			des_prodotto,
			cod_iva
	into 	:ls_cod_misura,
			:ls_des_prodotto,
			:ls_cod_iva
	from	anag_prodotti
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_prodotto = :ls_cod_prodotto;
			
	if isnull(ls_cod_deposito[1]) then ls_cod_deposito[1] = ls_cod_deposito_tes
	if isnull(ls_cod_iva) then ls_cod_iva = ls_cod_iva_det
			
			
	choose case fs_tipo_documento
		case "OFF_VEN", "ORD_VEN", "BOL_VEN", "FAT_VEN"
			
				if not isnull(ls_cod_cliente) then
					select 	cod_iva,
							 	data_esenzione_iva
					into   		:ls_cod_iva_cliente,
							 	:ldt_data_esenzione_iva
					from   	anag_clienti
					where  	cod_azienda = :s_cs_xx.cod_azienda and 
							 	cod_cliente = :ls_cod_cliente_tes;
				end if
	
				if sqlca.sqlcode = 0 then
					if ls_cod_iva <> "" and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
						ls_cod_iva = ls_cod_iva_cliente
					end if
				end if

		case "ORD_ACQ", "BOL_ACQ", "FAT_ACQ"
				//  Esenzione del fornitore
	end choose			

	
	
	choose case fs_tipo_documento
		case "FAT_VEN"
		// procedo con la creazione della riga della fattura
		  INSERT INTO det_fat_ven  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  prog_riga_fat_ven,   
				  cod_prodotto,   
				  cod_tipo_det_ven,   
				  cod_deposito,   
				  cod_ubicazione,   
				  cod_lotto,   
				  data_stock,   
				  progr_stock,   
				  cod_misura,   
				  des_prodotto,   
				  quan_fatturata,   
				  prezzo_vendita,   
				  sconto_1,   
				  sconto_2,   
				  provvigione_1,   
				  provvigione_2,   
				  cod_iva,   
				  cod_tipo_movimento,   
				  num_registrazione_mov_mag,   
				  anno_registrazione_fat,   
				  num_registrazione_fat,   
				  nota_dettaglio,   
				  anno_registrazione_bol_ven,   
				  num_registrazione_bol_ven,   
				  prog_riga_bol_ven,   
				  anno_commessa,   
				  num_commessa,   
				  cod_centro_costo,   
				  sconto_3,   
				  sconto_4,   
				  sconto_5,   
				  sconto_6,   
				  sconto_7,   
				  sconto_8,   
				  sconto_9,   
				  sconto_10,   
				  anno_registrazione_mov_mag,   
				  fat_conversione_ven,   
				  anno_reg_des_mov,   
				  num_reg_des_mov,   
				  anno_reg_ord_ven,   
				  num_reg_ord_ven,   
				  prog_riga_ord_ven,   
				  cod_versione,   
				  num_confezioni,   
				  num_pezzi_confezione,   
				  imponibile_iva,   
				  imponibile_iva_valuta,   
				  flag_st_note_det,   
				  quantita_um,   
				  prezzo_um,   
				  prod_mancante,   
				  flag_cc_manuali,   
				  valore_premio,   
				  ordinamento )  
	  VALUES ( :s_cs_xx.cod_azienda,   
				  :fl_anno_documento,   
				  :fl_num_documento,   
				  :ll_prog_riga_documento,   
				  :ls_cod_prodotto,   
				  :fs_cod_tipo_det_ven,   
				  :ls_cod_deposito[1],   
				  :ls_cod_ubicazione[1],   
				  :ls_cod_lotto[1],   
				  :ldt_data_stock[1],   
				  :ll_prog_stock[1],   
				  :ls_cod_misura,   
				  :ls_des_prodotto,   
				  :ld_quantita,   
				  :ld_prezzo,   
				  0,   
				  0,   
				  0,   
				  0,   
				  :ls_cod_iva,   
				  :ls_cod_tipo_movimento,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  null,   
				  1,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  1,   
				  1,   
				  0,   
				  0,   
				  'I',   
				  0,   
				  0,   
				  '0',   
				  'N',   
				  0,   
				  :ll_prog_riga_documento )  ;
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in fase di insert det_fat_ven." + sqlca.sqlerrtext
			lb_errore=true
			exit
		end if

	end choose
	
	if ls_flag_tipo_det_ven = "M" then
		if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_prog_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
			fs_errore = "Si è verificato un errore in fase di creazione destinazioni movimenti."
			lb_errore=true
			exit
		end if

		if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
			fs_errore = "Errore in fase di verifica movimenti magazzino."
			lb_errore=true
			exit
		end if
		
		update 	det_fat_ven
		set 		anno_reg_des_mov = :ll_anno_reg_des_mov,
					num_reg_des_mov = :ll_num_reg_des_mov
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :fl_anno_documento and
					num_registrazione = :fl_num_documento and
					prog_riga_fat_ven = :ll_prog_riga_documento;
		if sqlca.sqlcode = -1 then
			fs_errore = "Errore in fase di update det_fat_ven con la destinazione movimento." + sqlca.sqlerrtext
			lb_errore=true
			exit
		end if

	end if
	
	ll_prog_riga_documento ++
	// fine creazione riga fattura
	ll_riga++
	
loop

if lb_errore then
	destroy luo_excel
	return -1
end if

destroy luo_excel
return 0
end function

public function integer uof_controlla_qta_in_documento (integer fi_anno_ordine, long fl_num_ordine, long fl_riga_ordine, ref decimal fd_quan_documento, ref string fs_errore);long ll_count
decimal ld_quan_ordine

//Donato 27/06/2012 per nessun motivo deve finire nel documento una quantità superiore a quella della riga ordine riferita
//controllo se nel dettaglio del docuemnto (bolla/fattura) stai per metettere una quantità che supera quella della riga ordine

// 24/01/2020 annullato tutto; sta roba non serve a  nulla.

return 0
/*
select quan_ordine
into :ld_quan_ordine
from det_ord_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_ordine and
			num_registrazione=:fl_num_ordine and
			prog_riga_ord_ven=:fl_riga_ordine;

if sqlca.sqlcode<0 then
	fs_errore = "(uof_controlla_qta_in_documento) Errore lettura quan_ordine: "+sqlca.sqlerrtext
	return -1
end if

if not isnull(ld_quan_ordine) and ld_quan_ordine<fd_quan_documento then
	//stai cercando di mettere in fattura/bolla una quantità superiore a quella dell'ordine
	
	//log dell'operazione
	f_log_sistema(		"Tentativo evasione q.ta " + string(fd_quan_documento, "###,###,##0.00") +&
							" superiore a quella dell'ordine: "+string(fi_anno_ordine)+"/"+string(fl_num_ordine)+"/"+string(fl_riga_ordine), "ERRQTADOC")
	
	//forza la quantità documento alla quantità ordine
	fd_quan_documento = ld_quan_ordine
	
end if

//se arrivi fin qui prosegui
return 0
*/
end function

public subroutine uof_correggi_errori_in_evasione (integer fl_anno_registrazione, long fl_num_registrazione, long fl_prog_riga_ord_ven);//######################################################################################
//Donato 27/06/2012 cerco di evitare i casi in cui si possono avere residuo negativo
//la casistica prevede ad esempio che 
//1)		la quantità in evasione non si azzera dopo l'evasione, 
//			e allora il residuo dato da quan_ordine - quan_in_evasione - quan_evasa da un valore minore di zero (caso SACCOLONGO)

//2)		in altri casi si ha invece un azzeramento (giustamente) della quan_in_evasione ma una quantità_evasa superiore a quella dell'ordine (caso VEGGIANO)

decimal ld_quan_in_evasione_temp, ld_quan_evasa_temp, ld_quan_ordine
string ls_flag_evasione


select 	quan_ordine, 
			quan_in_evasione, 
			quan_evasa, 
			flag_evasione
into 			:ld_quan_ordine, 
				:ld_quan_in_evasione_temp, 
				:ld_quan_evasa_temp, 
				:ls_flag_evasione
from det_ord_ven
where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
		 det_ord_ven.num_registrazione = :fl_num_registrazione and
		 det_ord_ven.prog_riga_ord_ven = :fl_prog_riga_ord_ven;

if (ld_quan_ordine < ld_quan_in_evasione_temp + ld_quan_evasa_temp) and ld_quan_in_evasione_temp>0 and ls_flag_evasione="E" then
	//residuo = quan ordine - quan in evasione - quan evasa
	//allora per evitare che mi venga un resduo in negativo forzo a zero la quan in evasione
	
	update det_ord_ven  
	set quan_in_evasione = 0
	 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
			 det_ord_ven.num_registrazione = :fl_num_registrazione and
			 det_ord_ven.prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	
	//mi conservo un log dell'operazione fatta
	f_log_sistema("Caso di residuo ordine NEGATIVO: forzata a zero la quan in evasione: ("&
							+string(fl_anno_registrazione)+"/"+string(fl_num_registrazione)+ "/" + string(fl_prog_riga_ord_ven) +")", "ERRQTAEVAS_SAC")
							
elseif ld_quan_ordine < ld_quan_evasa_temp  and ld_quan_in_evasione_temp=0 and ls_flag_evasione="E" then
	update det_ord_ven  
	set quan_evasa = quan_ordine
	 where det_ord_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 det_ord_ven.anno_registrazione = :fl_anno_registrazione and  
			 det_ord_ven.num_registrazione = :fl_num_registrazione and
			 det_ord_ven.prog_riga_ord_ven = :fl_prog_riga_ord_ven;
	
	//mi conservo un log dell'operazione fatta
	f_log_sistema("Caso di residuo ordine NEGATIVO: forzata a quan_ordine la quan evasa: ("&
							+string(fl_anno_registrazione)+"/"+string(fl_num_registrazione)+ "/" + string(fl_prog_riga_ord_ven) +")", "ERRQTAEVAS_VEG")				
end if
//###########################################################


return
end subroutine

public function integer uof_crea_fattura_proforma_offerta (long fl_anno_registrazione, long fl_num_registrazione, ref long fl_anno_reg_fat_ven, ref long fl_num_reg_fat_ven, ref string fs_errore);
integer								li_risposta, li_anno_registrazione

long									ll_num_registrazione, ll_num_confezioni, ll_num_pezzi_confezione, ll_prog_riga_fat_ven, ll_count

uo_calcola_documento_euro 	luo_calcolo

string									ls_cod_tipo_off_ven, ls_cod_contatto, ls_cod_operatore, ls_cod_des_cliente, ls_rag_soc_1, ls_rag_soc_2, ls_db, &
										ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_valuta, &
										ls_cod_tipo_listino_prodotto, ls_cod_pagamento,  ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca, ls_num_ord_cliente, &
										ls_cod_imballo, ls_aspetto_beni, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_causale_trasporto, ls_cod_tipo_fat_proforma, &
										ls_cod_porto, ls_cod_resa, ls_nota_testata, ls_nota_piede, ls_cod_banca_clien_for,  ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, &
										ls_flag_abilita_rif_bol_ven, ls_cod_tipo_det_ven_rif_bol_ven, ls_des_riferimento_ord_ven, ls_flag_rif_anno_ord_ven, &
										ls_flag_rif_num_ord_ven, ls_flag_rif_data_ord_ven, ls_vostro_ordine, ls_vostro_ordine_data, ls_lire, &
										ls_cod_tipo_det_ven, ls_cod_misura, ls_cod_prodotto, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, &
										ls_cod_versione, ls_flag_doc_suc_det, ls_flag_tipo_det_ven, ls_cod_tipo_movimento, ls_flag_sola_iva, ls_cod_tipo_det_ven_proforma, &
										ls_note_dettaglio_rif, ls_cod_iva_rif, ls_des_riferimento, ls_cod_cliente

boolean								lb_riferimento

datetime								ldt_data_ordine, ldt_data_ord_cliente, ldt_data_registrazione

dec{6}								ld_cambio_ven, ld_sconto_testata, ld_peso_netto, ld_peso_lordo, ld_num_colli, ld_sconto_pagamento, &
										ld_quan_offerta, ld_prezzo_vendita,ld_fat_conversione_ven, ld_sconto_1, ld_sconto_2, &
										ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
	 									ld_provvigione_1, ld_provvigione_2, ld_quantita_um, ld_prezzo_um, ll_prog_riga_off_ven
	 

//crea una nuova fattura proforma cliente da offerta vendita  ->  su contatto o cliente
//-------------------------------------------------------------------------------------------------------------------------------------	
setnull(s_cs_xx.parametri.parametro_d_1)
setnull(s_cs_xx.parametri.parametro_d_2)
setnull(s_cs_xx.parametri.parametro_b_1)
setnull(s_cs_xx.parametri.parametro_b_2)	

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)
if upper(ls_db) = "MSSQL" then ls_db="SYBASE_ASE"

lb_riferimento = true

//cod tipo off ven
select cod_tipo_off_ven
into   :ls_cod_tipo_off_ven
from   tes_off_ven
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione  = :fl_num_registrazione;
		 
if sqlca.sqlcode < 0 then
	fs_errore = "Errore durante la ricerca del tipo offerta di vendita: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	fs_errore = "Offerta vendita non trovata: n° "+string(fl_anno_registrazione)+"/"+string(fl_num_registrazione)
	return -1
	
elseif ls_cod_tipo_off_ven='' or isnull(ls_cod_tipo_off_ven) then
	fs_errore = "Tipo offerta vendita non specificato per offerta n° "+string(fl_anno_registrazione)+"/"+string(fl_num_registrazione)
	return -1
end if
//-------------------------------------------------------------------------------------------


//cod tipo fattura pro-forma
select cod_tipo_fat_proforma
into   :ls_cod_tipo_fat_proforma
from   tab_tipi_off_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 cod_tipo_off_ven = :ls_cod_tipo_off_ven;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la ricerca tipo fattura in tabella Tipi Ordini Vendita."
	return -1
end if

if isnull(ls_cod_tipo_fat_proforma) or ls_cod_tipo_fat_proforma="" then
	fs_errore = "Nel tipo offerta "+ls_cod_tipo_off_ven+" manca l'indicazione del tipo fattura proforma da creare; operazione annullata "
	return -1
end if
//-------------------------------------------------------------------------------------------


//verifica se l'offerta ha già una fattura proforma
select count(*)
into :ll_count
from det_fat_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione_off_ven=:fl_anno_registrazione and
			num_registrazione_off_ven=:fl_num_registrazione;

if sqlca.sqlcode<0 then
	fs_errore = "Errore durante controllo se l'offerta ha già una fattura proforma: "+sqlca.sqlerrtext
	return -1
end if

if isnull(ll_count) or ll_count=0 then
else
	if not g_mb.confirm("L'offerta n° "+string(fl_anno_registrazione)+"/"+string(fl_num_registrazione) + " ha già delle fatture proforma. Continuare lo stesso?") then
		return 1
	end if
end if
//----------------------------------------------------------------------------------------------------------


li_anno_registrazione = f_anno_esercizio()

select max(num_registrazione)
into   :ll_num_registrazione
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :li_anno_registrazione;

if li_anno_registrazione = 0 or isnull(ll_num_registrazione) then ll_num_registrazione = 0
ll_num_registrazione += 1

update con_fat_ven
set    con_fat_ven.num_registrazione = :ll_num_registrazione
where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore durante la scrittura sulla tabella Controllo Fatture Vendita: "+sqlca.sqlerrtext
	return -1
end if

select cod_operatore,   
		 data_registrazione,
		 cod_cliente,
		 cod_contatto,   
		 cod_des_cliente,   
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia,   
		 cod_deposito,   
		 cod_ubicazione,   
		 cod_valuta,   
		 cambio_ven,   
		 cod_tipo_listino_prodotto,   
		 cod_pagamento,   
		 sconto,   
		 cod_agente_1,
		 cod_agente_2,
		 cod_banca,   
		 num_ric_cliente,   
		 data_ric_cliente,   
		 cod_imballo,   
		 aspetto_beni,
		 peso_netto,
		 peso_lordo,
		 num_colli,
		 cod_vettore,   
		 cod_inoltro,   
		 cod_mezzo,
		 causale_trasporto,
		 cod_porto,   
		 cod_resa,   
		 nota_testata,   
		 nota_piede,
		 cod_banca_clien_for,
		 flag_doc_suc_tes, 
		 flag_doc_suc_pie			 
 into  :ls_cod_operatore,   
		 :ldt_data_ordine,
		 :ls_cod_cliente,
		 :ls_cod_contatto,   
		 :ls_cod_des_cliente,   
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,   
		 :ls_cod_deposito[1],   
		 :ls_cod_ubicazione[1],
		 :ls_cod_valuta,   
		 :ld_cambio_ven,   
		 :ls_cod_tipo_listino_prodotto,   
		 :ls_cod_pagamento,   
		 :ld_sconto_testata,   
		 :ls_cod_agente_1,
		 :ls_cod_agente_2,
		 :ls_cod_banca,   
		 :ls_num_ord_cliente,   
		 :ldt_data_ord_cliente,   
		 :ls_cod_imballo,   
		 :ls_aspetto_beni,
		 :ld_peso_netto,
		 :ld_peso_lordo,
		 :ld_num_colli,
		 :ls_cod_vettore,   
		 :ls_cod_inoltro,   
		 :ls_cod_mezzo,   
		 :ls_causale_trasporto,
		 :ls_cod_porto,
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,
		 :ls_cod_banca_clien_for,
		 :ls_flag_doc_suc_tes, 
		 :ls_flag_doc_suc_pie			 
from   tes_off_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :fl_anno_registrazione and  
		 num_registrazione = :fl_num_registrazione;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore durante la lettura Testata Offerta Vendita: "+sqlca.sqlerrtext
	return -1
end if

//Se nell'offerta è specificato il cliente, allora questo prevale sull'eventuale contatto
if ls_cod_cliente<>"" and not isnull(ls_cod_cliente) then
	if ls_cod_contatto = "" then setnull(ls_cod_contatto)
	
else
	setnull(ls_cod_cliente)
	
	//no Cliente, verifica se è stato specificato il contatto
	if ls_cod_contatto="" or isnull(ls_cod_contatto) then
		fs_errore = "E' possibile generare Proforma da Offerta solo è stato specificato il Clinete e/o il Contatto!"
		return -1
	end if
end if

if isnull(ls_nota_testata) then
	ls_nota_testata = ""
end if

if isnull(ls_nota_piede) then
	ls_nota_piede = ""
end if

uof_estrai_richtext_off_ven(fl_anno_registrazione, fl_num_registrazione, ls_nota_testata, ls_nota_piede)

ldt_data_registrazione = datetime(today())


// stefanop: 18/07/2012 - l'operatore che inserire è SEMPRE l'operatore collegato ad Apice
// ------------------------------------------------------
ls_cod_operatore = guo_functions.uof_get_operatore_utente()
// ------------------------------------------------------

//	select tab_tipi_fat_ven.causale_trasporto,
//			 tab_tipi_fat_ven.flag_abilita_rif_bol_ven,
//			 tab_tipi_fat_ven.cod_tipo_det_ven_rif_bol_ven,
//			 tab_tipi_fat_ven.des_riferimento_ord_ven,
//			 tab_tipi_fat_ven.flag_rif_anno_ord_ven,
//			 tab_tipi_fat_ven.flag_rif_num_ord_ven,
//			 tab_tipi_fat_ven.flag_rif_data_ord_ven
//	into	 :ls_causale_trasporto,
//			 :ls_flag_abilita_rif_bol_ven,
//			 :ls_cod_tipo_det_ven_rif_bol_ven,
//			 :ls_des_riferimento_ord_ven,
//			 :ls_flag_rif_anno_ord_ven,
//			 :ls_flag_rif_num_ord_ven,
//			 :ls_flag_rif_data_ord_ven
//	from   tab_tipi_fat_ven  
//	where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//			 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_proforma;
//			 
//	if sqlca.sqlcode < 0 then
//		fs_errore = "Si è verificato un errore nella lettura della Tabella Tipi Fatture: " + sqlca.sqlerrtext
//		return -1
//	end if
//	
//	
//	if not isnull(ls_num_ord_cliente)  and ls_cod_tipo_det_ven_rif_bol_ven = "T" then
//		ls_vostro_ordine = "~r~nVostra Offerta " + ls_num_ord_cliente
//		if not isnull(ldt_data_ord_cliente) then
//			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
//		else
//			ls_vostro_ordine_data = ""
//		end if
//	else
//		ls_vostro_ordine = ""
//		ls_vostro_ordine_data = ""
//	end if
//
//	if not isnull(ls_num_ord_cliente)  and ls_cod_tipo_det_ven_rif_bol_ven = "T" then
//		ls_vostro_ordine = "~r~nVostra Offerta " + ls_num_ord_cliente
//		if not isnull(ldt_data_ord_cliente) then
//			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
//		else
//			ls_vostro_ordine_data = ""
//		end if
//	else
//		ls_vostro_ordine = ""
//		ls_vostro_ordine_data = ""
//	end if

//PER ORA TRASCURO LA SELECT DI SOPRA E FACCIO SEMPRE COSI



if not isnull(ls_num_ord_cliente)  then
	ls_vostro_ordine = "~r~nVostra Offerta " + ls_num_ord_cliente
	if not isnull(ldt_data_ord_cliente) then
		ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
	else
		ls_vostro_ordine_data = ""
	end if
else
	ls_vostro_ordine = ""
	ls_vostro_ordine_data = ""
end if

//FINE -------------



//nota di testata ---------
if ls_flag_doc_suc_tes = 'I' then 
	select flag_doc_suc
	  into :ls_flag_doc_suc_tes
	  from tab_tipi_off_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_off_ven = :ls_cod_tipo_off_ven;
end if		

if ls_flag_doc_suc_tes = 'N' then
	ls_nota_testata = ""
end if
//---------------------------------------------------------------------------------------------------------------------

//nota di piede --------
if ls_flag_doc_suc_pie = 'I' then 
	select flag_doc_suc
	  into :ls_flag_doc_suc_pie
	  from tab_tipi_off_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_off_ven = :ls_cod_tipo_off_ven;
end if			

if ls_flag_doc_suc_pie = 'N' then
	ls_nota_piede = ""
end if
//---------------------------------------------------------------------------------------------------------------------

//TO.DO IMPORTANTE
//se le note sono formattate con rich text


//IGNORO ANCHE QUESTO IF PER ORA E ACCODO SEMPRE IL RIF NS OFFERTA

//	if ls_cod_tipo_det_ven_rif_bol_ven = "T" then
//		ls_nota_testata = "Rif. ns. Offerta Nr. " + string(fl_anno_registrazione) + "/" + &
//								string(fl_num_registrazione) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
//								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata
//	end if

ls_nota_testata = "Rif. ns. Offerta Nr. " + string(fl_anno_registrazione) + "/" + &
							string(fl_num_registrazione) + " del " + string(ldt_data_ordine, "dd/mm/yyyy") + &
							ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata

	
// ----- controllo lunghezza note per ASE
if ls_db = "SYBASE_ASE" and len(ls_nota_testata) > 255 then
	ls_nota_testata = left(ls_nota_testata, 255)
end if
// -----  fine controllo lunghezza note per ASE

if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
	ls_causale_trasporto = ""			//ls_causale_trasporto_tab
end if

select parametri_azienda.stringa
into   :ls_lire
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
		 parametri_azienda.flag_parametro = 'S' and &
		 parametri_azienda.cod_parametro = 'LIR';

if sqlca.sqlcode <> 0 then
	fs_errore = "Configurare il codice valuta per le Lire Italiane."
	return -1
end if

select tab_pagamenti.sconto
into   :ld_sconto_pagamento
from   tab_pagamenti
where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
		 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	ld_sconto_pagamento = 0
end if

insert into tes_fat_ven
				(cod_azienda,   
				 anno_registrazione,   
				 num_registrazione,   
				 data_registrazione,   
				 cod_operatore,   
				 cod_tipo_fat_ven,
				 cod_cliente,
				 cod_contatto,
				 cod_des_cliente,   
				 rag_soc_1,   
				 rag_soc_2,   
				 indirizzo,   
				 localita,   
				 frazione,   
				 cap,   
				 provincia,   
				 cod_deposito,   
				 cod_ubicazione,
				 cod_valuta,   
				 cambio_ven,   
				 cod_tipo_listino_prodotto,   
				 cod_pagamento,   
				 sconto,   
				 cod_agente_1,
				 cod_agente_2,
				 cod_banca,   
				 num_ord_cliente,   
				 data_ord_cliente,   
				 cod_imballo,   
				 aspetto_beni,
				 peso_netto,
				 peso_lordo,
				 num_colli,
				 cod_vettore,   
				 cod_inoltro,   
				 cod_mezzo,
				 causale_trasporto,
				 cod_porto,   
				 cod_resa,   
				 cod_documento,
				 numeratore_documento,
				 anno_documento,
				 num_documento,
				 data_fattura,
				 nota_testata,   
				 nota_piede,   
				 flag_fuori_fido,
				 flag_blocco,   
				 flag_movimenti,  
				 flag_contabilita,
				 tot_merci,
				 tot_spese_trasporto,
				 tot_spese_imballo,
				 tot_spese_bolli,
				 tot_spese_varie,
				 tot_sconto_cassa,
				 tot_sconti_commerciali,
				 imponibile_provvigioni_1,
				 imponibile_provvigioni_2,
				 tot_provvigioni_1,
				 tot_provvigioni_2,
				 importo_iva,
				 imponibile_iva,
				 importo_iva_valuta,
				 imponibile_iva_valuta,
				 tot_fattura,
				 tot_fattura_valuta,
				 flag_cambio_zero,
				 cod_banca_clien_for,
				 flag_st_note_tes,
				 flag_st_note_pie)  					 
values      (   :s_cs_xx.cod_azienda,   
				 :li_anno_registrazione,   
				 :ll_num_registrazione,   
				 :ldt_data_registrazione,   
				 :ls_cod_operatore,   
				 :ls_cod_tipo_fat_proforma,
				 :ls_cod_cliente,
				 :ls_cod_contatto,
				 :ls_cod_des_cliente,   
				 :ls_rag_soc_1,   
				 :ls_rag_soc_2,   
				 :ls_indirizzo,   
				 :ls_localita,   
				 :ls_frazione,   
				 :ls_cap,   
				 :ls_provincia,   
				 :ls_cod_deposito[1],   
				 :ls_cod_ubicazione[1],
				 :ls_cod_valuta,   
				 :ld_cambio_ven,   
				 :ls_cod_tipo_listino_prodotto,   
				 :ls_cod_pagamento,   
				 :ld_sconto_testata,
				 :ls_cod_agente_1,
				 :ls_cod_agente_2,
				 :ls_cod_banca,   
				 :ls_num_ord_cliente,   
				 :ldt_data_ord_cliente,   
				 :ls_cod_imballo,   
				 :ls_aspetto_beni,
				 :ld_peso_netto,
				 :ld_peso_lordo,
				 :ld_num_colli,
				 :ls_cod_vettore,   
				 :ls_cod_inoltro,   
				 :ls_cod_mezzo,   
				 :ls_causale_trasporto,
				 :ls_cod_porto,   
				 :ls_cod_resa,   
				 null,
				 null,
				 0,
				 0,
				 null,
				 :ls_nota_testata,   
				 :ls_nota_piede,   
				 'N',
				 'N',   
				 'S',
				 'S',
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 0,
				 'N',
				 :ls_cod_banca_clien_for,
				 'I',
				 'I');					 

 if sqlca.sqlcode <> 0 then
	 fs_errore = "Errore durante la scrittura Testata Fatture Vendita."
	 return -1
 end if
 
 
 //PER ORA NON C'è LA GESTIONE NOTE FISSE PER I CONTATTI
//if not isnull(ls_cod_cliente) then
//	if uof_crea_nota_fissa("FAT_PRO", li_anno_registrazione, ll_num_registrazione, fs_errore) = -1 then return -1
//end if



//CURSORE PER RIGHE FATTURA PROFORMA

declare  cu_righe_offerta cursor for  
select		cod_tipo_det_ven, cod_misura, cod_prodotto, des_prodotto, quan_offerta, prezzo_vendita, fat_conversione_ven, 
			sconto_1, sconto_2, sconto_3, sconto_4, sconto_5, sconto_6, sconto_7, sconto_8, sconto_9, sconto_10,
			provvigione_1,	provvigione_2,	cod_iva, nota_dettaglio,
			cod_centro_costo, cod_versione, num_confezioni, num_pezzi_confezione, flag_doc_suc_det,
			quantita_um, prezzo_um, prog_riga_off_ven
  from 	det_off_ven  
 where 	cod_azienda = :s_cs_xx.cod_azienda and  
			anno_registrazione = :fl_anno_registrazione and  
			num_registrazione = :fl_num_registrazione 
order by prog_riga_off_ven asc;

open cu_righe_offerta;
if sqlca.sqlcode <> 0 then
	fs_errore = "Errore durante la OPEN del curosore righe offerta: "+ sqlca.sqlerrtext
	return -1
end if

ll_prog_riga_fat_ven = 0
do while 0 = 0
	fetch cu_righe_offerta
	into		:ls_cod_tipo_det_ven, :ls_cod_misura, :ls_cod_prodotto, :ls_des_prodotto, :ld_quan_offerta, :ld_prezzo_vendita, 
				:ld_fat_conversione_ven, :ld_sconto_1, :ld_sconto_2,:ld_sconto_3, :ld_sconto_4, :ld_sconto_5, :ld_sconto_6, :ld_sconto_7, 
				:ld_sconto_8, :ld_sconto_9, :ld_sconto_10, :ld_provvigione_1, :ld_provvigione_2, :ls_cod_iva,
				:ls_nota_dettaglio, :ls_cod_centro_costo, :ls_cod_versione, :ll_num_confezioni,
				:ll_num_pezzi_confezione, :ls_flag_doc_suc_det, :ld_quantita_um, :ld_prezzo_um, :ll_prog_riga_off_ven;
				
	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode = -1 then
		fs_errore =  "Errore durante la lettura delle righe dell'offerta.~r~n" + sqlca.sqlerrtext
		close cu_righe_offerta;
		return -1
	end if
	
	ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10


	if ls_flag_doc_suc_det = 'I' then //nota dettaglio
		select flag_doc_suc_or
		  into :ls_flag_doc_suc_det
		  from tab_tipi_det_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		


	if ls_flag_doc_suc_det = 'N' then setnull(ls_nota_dettaglio)
	
	select		flag_tipo_det_ven,
				cod_tipo_movimento,
				flag_sola_iva,
				cod_tipo_det_ven_proforma
	into		:ls_flag_tipo_det_ven,
				:ls_cod_tipo_movimento,
				:ls_flag_sola_iva,
				:ls_cod_tipo_det_ven_proforma
	from   tab_tipi_det_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			 
	if sqlca.sqlcode = -1 then
		fs_errore = "Si è verificato un errore in fase di lettura tipi dettagli."
		close cu_righe_offerta;
		return -1
	end if

	if ls_flag_tipo_det_ven = "M" then
		if isnull(ls_cod_tipo_det_ven_proforma) then
			fs_errore = "Non è stato indicato il tipo dettaglio da usare nelle PROFORMA nel tipo dettaglio " + ls_cod_tipo_det_ven + ".~r~nAndare ad indicare il dato e ripetere l'operazione."
			close cu_righe_offerta;
			return -1
		end if
		ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_proforma
	end if

	if lb_riferimento and ls_flag_abilita_rif_bol_ven = "D" then
		lb_riferimento = false
		setnull(ls_note_dettaglio_rif)
		
//			ls_des_riferimento = ls_des_riferimento_ord_ven
//			if ls_flag_rif_anno_ord_ven = "S" and not isnull(fl_anno_registrazione) then
//				ls_des_riferimento = ls_des_riferimento + "-" +string(fl_anno_registrazione)
//			end if
//			if ls_flag_rif_num_ord_ven = "S" and not isnull(fl_num_registrazione) then
//				ls_des_riferimento = ls_des_riferimento + "-" + string(fl_num_registrazione)
//			end if
//			if ls_flag_rif_data_ord_ven = "S" and not isnull(ldt_data_ordine) then
//				ls_des_riferimento = ls_des_riferimento + "-" + string(ldt_data_ordine,"dd/mm/yyyy")
//			end if
//
//			if not isnull(ls_num_ord_cliente) then
//				ls_note_dettaglio_rif = "Vs.Offerta " + ls_num_ord_cliente
//				if not isnull(ldt_data_ord_cliente) then
//					ls_note_dettaglio_rif = ls_note_dettaglio_rif + "  del " + string(ldt_data_ord_cliente,"dd/mm/yyyy")
//				end if
//			end if

		
		ls_des_riferimento = "Rif. NS.Off. " + string(fl_anno_registrazione) + "/" + &
							string(fl_num_registrazione) + " del " + string(ldt_data_ordine, "dd/mm/yy")
		ls_des_riferimento = left(ls_des_riferimento, 40)
		
		
		//CHIAMO QUESTA FUNZIONE passando X  (C SAREBBE CLIENTE, F SAREBBE FORNITORE)
		
		if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven_rif_bol_ven, "X", "", ref ls_cod_iva_rif) <> 0 then
			g_mb.error("Attenzione! Errore non bloccante!","Errore durante la ricerca codice iva per riga riferimento:" + sqlca.sqlerrtext)
			setnull(ls_cod_iva_rif)
		end if
		
		insert into det_fat_ven  
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 prog_riga_fat_ven,   
						 cod_tipo_det_ven,   
						 cod_deposito,
						 cod_ubicazione,
						 cod_lotto,
						 data_stock,
						 progr_stock,
						 cod_misura,   
						 cod_prodotto,   
						 des_prodotto,   
						 quan_fatturata,   
						 prezzo_vendita,   
						 sconto_1,   
						 sconto_2,   
						 provvigione_1,
						 provvigione_2,
						 cod_iva,   
						 cod_tipo_movimento,
						 num_registrazione_mov_mag,
						 anno_registrazione_fat,
						 num_registrazione_fat,
						 nota_dettaglio,   
						 anno_registrazione_bol_ven,   
						 num_registrazione_bol_ven,   
						 prog_riga_bol_ven,
						 anno_commessa,
						 num_commessa,
						 cod_centro_costo,
						 sconto_3,   
						 sconto_4,   
						 sconto_5,   
						 sconto_6,   
						 sconto_7,   
						 sconto_8,   
						 sconto_9,   
						 sconto_10,
						 anno_registrazione_mov_mag,
						 fat_conversione_ven,   
						 anno_reg_des_mov,
						 num_reg_des_mov,
						 anno_reg_ord_ven,   
						 num_reg_ord_ven,   
						 prog_riga_ord_ven,
						 cod_versione,
						 num_confezioni,
						 num_pezzi_confezione,
						 flag_st_note_det,
						 quantita_um,
						 prezzo_um)							 
	  values			(:s_cs_xx.cod_azienda,   
						 :li_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ll_prog_riga_fat_ven,   
						 :ls_cod_tipo_det_ven_rif_bol_ven,   
						 null,
						 null,
						 null,
						 null,
						 null,
						 null,
						 null,
						 :ls_des_riferimento,   
						 0,   
						 0,   
						 0,   
						 0,  
						 0,
						 0,
						 :ls_cod_iva_rif,   
						 null,
						 null,
						 null,
						 null,
						 null,   
						 null,
						 null,
						 null,
						 null,   
						 null,   
						 null,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,   
						 0,
						 null,
						 1,
						 null,
						 null,
						 null,   
						 null,   
						 null,
						 null,
						 0,
						 0,
						 'I',
						 0,
						 0);

		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore durante la scrittura Dettagli Bolle Vendita.~r~n" + sqlca.sqlerrtext
			close cu_righe_offerta;
			return -1
		end if
		ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + 10
	end if	

	insert into det_fat_ven  
					(cod_azienda,   
					 anno_registrazione,   
					 num_registrazione,   
					 prog_riga_fat_ven,   
					 cod_tipo_det_ven,   
					 cod_deposito,
					 cod_ubicazione,
					 cod_lotto,
					 data_stock,
					 progr_stock,
					 cod_misura,   
					 cod_prodotto,   
					 des_prodotto,   
					 quan_fatturata,   
					 prezzo_vendita,   
					 sconto_1,   
					 sconto_2,   
					 provvigione_1,
					 provvigione_2,
					 cod_iva,   
					 cod_tipo_movimento,
					 num_registrazione_mov_mag,
					 anno_registrazione_fat,
					 num_registrazione_fat,
					 nota_dettaglio,   
					 anno_registrazione_bol_ven,   
					 num_registrazione_bol_ven,   
					 prog_riga_bol_ven,
					 anno_commessa,
					 num_commessa,
					 cod_centro_costo,
					 sconto_3,   
					 sconto_4,   
					 sconto_5,   
					 sconto_6,   
					 sconto_7,   
					 sconto_8,   
					 sconto_9,   
					 sconto_10,
					 anno_registrazione_mov_mag,
					 fat_conversione_ven,   
					 anno_reg_des_mov,
					 num_reg_des_mov,
					 anno_reg_ord_ven,   
					 num_reg_ord_ven,   
					 prog_riga_ord_ven,
					 cod_versione,
					 num_confezioni,
					 num_pezzi_confezione,
					 flag_st_note_det,
					 quantita_um,
					 prezzo_um,
					 anno_registrazione_off_ven,
					 num_registrazione_off_ven,
					 prog_riga_off_ven)			
  values			(:s_cs_xx.cod_azienda,   
					 :li_anno_registrazione,   
					 :ll_num_registrazione,   
					 :ll_prog_riga_fat_ven,   
					 :ls_cod_tipo_det_ven,   
					 null,
					 null,
					 null,
					 null,
					 null,
					 :ls_cod_misura,   
					 :ls_cod_prodotto,   
					 :ls_des_prodotto,   
					 :ld_quan_offerta,
					 :ld_prezzo_vendita,   
					 :ld_sconto_1,   
					 :ld_sconto_2,  
					 :ld_provvigione_1,
					 :ld_provvigione_2,
					 :ls_cod_iva,   
					 :ls_cod_tipo_movimento,
					 null,
					 null,
					 null,
					 :ls_nota_dettaglio,   
					 null,
					 null,
					 null,
					 null,   
					 null,   
					 :ls_cod_centro_costo,   
					 :ld_sconto_3,   
					 :ld_sconto_4,   
					 :ld_sconto_5,   
					 :ld_sconto_6,   
					 :ld_sconto_7,   
					 :ld_sconto_8,   
					 :ld_sconto_9,   
					 :ld_sconto_10,
					 null,
					 :ld_fat_conversione_ven,
					 null,
					 null,
					 null,   
					 null,   
					 null,
					 :ls_cod_versione,
					 :ll_num_confezioni,
					 :ll_num_pezzi_confezione,
					 'I',
					 :ld_quantita_um,
					 :ld_prezzo_um,
					 :fl_anno_registrazione,
					 :fl_num_registrazione,
					 :ll_prog_riga_off_ven);								 						 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore durante la scrittura Dettagli Fatture Vendita.~r~n" + sqlca.sqlerrtext
		close cu_righe_offerta;
		return -1
	end if
loop
close cu_righe_offerta;

luo_calcolo = create uo_calcola_documento_euro

if luo_calcolo.uof_calcola_documento(li_anno_registrazione,ll_num_registrazione,"fat_ven",fs_errore) <> 0 then
	destroy luo_calcolo
	return -1
end if

destroy luo_calcolo


fl_anno_reg_fat_ven = li_anno_registrazione
fl_num_reg_fat_ven = ll_num_registrazione



return 0

end function

public subroutine uof_estrai_richtext_off_ven (integer fi_anno_reg, long fl_num_reg, ref string fs_nota_testata, ref string fs_nota_piede);
//per il momento funziona solo sulle offerte vendita
//legge i valori delle colonne rich text note testata e pioede da tes_off_ven e li ritorna ripuliti di formattazione
//utile quando vuoi passare le note alla fattura proforma da generare a partire da offerta vendita

s_cs_xx_parametri lstr_parametri


lstr_parametri.parametro_d_1_a[1] = fi_anno_reg
lstr_parametri.parametro_d_1_a[2] = fl_num_reg

openwithparm(w_converti_rich_text_offerte, lstr_parametri)

lstr_parametri = message.powerobjectparm

fs_nota_testata = lstr_parametri.parametro_s_1_a[1]
fs_nota_piede = lstr_parametri.parametro_s_1_a[2]

return
end subroutine

public function integer uof_listini_fornitori (string as_cod_tipo_listino_prodotto, string as_cod_fornitore, string as_cod_valuta, double ad_cambio_acq, string as_cod_prodotto, double ad_quantita, datetime adt_data_consegna, string as_cod_tipo_det_acq, ref decimal ad_prezzo_acquisto, ref decimal ad_sconto_1, ref decimal ad_fat_conversione, ref string as_cod_misura);double ld_prezzo_1, ld_prezzo_2, ld_prezzo_3, ld_prezzo_4, ld_prezzo_5, &
       ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, &
       ld_quantita_1, ld_quantita_2, ld_quantita_3, ld_quantita_4, ld_quantita_5, &
       ld_fat_conversione
string ls_flag_for_pref, ls_cod_misura
datetime ldt_data_inizio_val
long ll_i
w_cs_xx_mdi lw_cs_xx_mdi


lw_cs_xx_mdi = pcca.mdi_frame

select anag_prodotti.prezzo_acquisto,
       anag_prodotti.sconto_acquisto
into   :ld_prezzo_1,
       :ld_sconto_1
from   anag_prodotti
where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
       anag_prodotti.cod_prodotto = :as_cod_prodotto;
   
if sqlca.sqlcode = 0 and ld_prezzo_1 <> 0 then
	ld_prezzo_1 = ld_prezzo_1 / ad_cambio_acq
	
	//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "prezzo_acquisto", ld_prezzo_1)
	//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "sconto_1", ld_sconto_1)
	ad_prezzo_acquisto = ld_prezzo_1
	ad_sconto_1 = ld_sconto_1

	lw_cs_xx_mdi.setmicrohelp("Applicato Listino Prodotto")
end if

declare cu_listini_prodotti cursor for 
	select	listini_prodotti.data_inizio_val,
				listini_prodotti.quantita_1,
				listini_prodotti.prezzo_1,
				listini_prodotti.sconto_1,
				listini_prodotti.quantita_2,
				listini_prodotti.prezzo_2,
				listini_prodotti.sconto_2,
				listini_prodotti.quantita_3,
				listini_prodotti.prezzo_3,
				listini_prodotti.sconto_3,
				listini_prodotti.quantita_4,
				listini_prodotti.prezzo_4,
				listini_prodotti.sconto_4,
				listini_prodotti.quantita_5,
				listini_prodotti.prezzo_5,
				listini_prodotti.sconto_5
	from listini_prodotti
	where	listini_prodotti.cod_azienda = :s_cs_xx.cod_azienda and listini_prodotti.cod_prodotto = :as_cod_prodotto and 
				listini_prodotti.cod_tipo_listino_prodotto = :as_cod_tipo_listino_prodotto and listini_prodotti.cod_valuta = :as_cod_valuta and 
				listini_prodotti.data_inizio_val <= :adt_data_consegna
	order by listini_prodotti.cod_azienda, listini_prodotti.cod_prodotto, listini_prodotti.cod_tipo_listino_prodotto, listini_prodotti.cod_valuta, listini_prodotti.data_inizio_val desc;
	
open cu_listini_prodotti;
ll_i = 0

do while 0 = 0
   ll_i ++
   fetch cu_listini_prodotti into	:ldt_data_inizio_val, :ld_quantita_1, :ld_prezzo_1, :ld_sconto_1, :ld_quantita_2, :ld_prezzo_2, :ld_sconto_2, :ld_quantita_3, :ld_prezzo_3, 
											:ld_sconto_3, :ld_quantita_4, :ld_prezzo_4, :ld_sconto_4, :ld_quantita_5, :ld_prezzo_5, :ld_sconto_5;

   if sqlca.sqlcode <> 0 or ll_i = 1 then exit
loop

if sqlca.sqlcode = 0 and (ld_prezzo_1 <> 0 or &
                          ld_prezzo_2 <> 0 or &
                          ld_prezzo_3 <> 0 or &
                          ld_prezzo_4 <> 0 or &
                          ld_prezzo_5 <> 0) then
								  
	if ad_quantita <= ld_quantita_1 then
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "prezzo_acquisto", ld_prezzo_1)
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "sconto_1", ld_sconto_1)
		ad_prezzo_acquisto = ld_prezzo_1
		ad_sconto_1 = ld_sconto_1
		
	elseif ad_quantita <= ld_quantita_2 then
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "prezzo_acquisto", ld_prezzo_2)
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "sconto_1", ld_sconto_2)
		ad_prezzo_acquisto = ld_prezzo_2
		ad_sconto_1 = ld_sconto_2
		
	elseif ad_quantita <= ld_quantita_3 then
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "prezzo_acquisto", ld_prezzo_3)
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "sconto_1", ld_sconto_3)
		ad_prezzo_acquisto = ld_prezzo_3
		ad_sconto_1 = ld_sconto_3
		
	elseif ad_quantita <= ld_quantita_4 then
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "prezzo_acquisto", ld_prezzo_4)
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "sconto_1", ld_sconto_4)
		ad_prezzo_acquisto = ld_prezzo_4
		ad_sconto_1 = ld_sconto_4
		
	elseif ad_quantita <= ld_quantita_5 then
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "prezzo_acquisto", ld_prezzo_5)
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "sconto_1", ld_sconto_5)
		ad_prezzo_acquisto = ld_prezzo_5
		ad_sconto_1 = ld_sconto_5
		
	end if
	
lw_cs_xx_mdi.setmicrohelp("Applicato Listino Prodotto con Data Validità " + string(ldt_data_inizio_val, "dd/mm/yyyy"))
end if

close cu_listini_prodotti;

declare cu_nome cursor for 
	select	listini_fornitori.data_inizio_val,
				listini_fornitori.quantita_1,
				listini_fornitori.prezzo_1,
				listini_fornitori.sconto_1,
				listini_fornitori.quantita_2,
				listini_fornitori.prezzo_2,
				listini_fornitori.sconto_2,
				listini_fornitori.quantita_3,
				listini_fornitori.prezzo_3,
				listini_fornitori.sconto_3,
				listini_fornitori.quantita_4,
				listini_fornitori.prezzo_4,
				listini_fornitori.sconto_4,
				listini_fornitori.quantita_5,
				listini_fornitori.prezzo_5,
				listini_fornitori.sconto_5,
				listini_fornitori.flag_for_pref,
				listini_fornitori.fat_conversione,
				listini_fornitori.cod_misura
	from listini_fornitori 
	where	listini_fornitori.cod_azienda = :s_cs_xx.cod_azienda and listini_fornitori.cod_prodotto = :as_cod_prodotto and 
				listini_fornitori.cod_fornitore = :as_cod_fornitore and listini_fornitori.cod_valuta = :as_cod_valuta and listini_fornitori.data_inizio_val <= :adt_data_consegna
	order by listini_fornitori.cod_azienda, listini_fornitori.cod_prodotto, listini_fornitori.cod_fornitore, listini_fornitori.cod_valuta, listini_fornitori.data_inizio_val desc;
	
open cu_nome;

ll_i = 0
do while 0 = 0
   ll_i ++
   fetch cu_nome into	:ldt_data_inizio_val, :ld_quantita_1, :ld_prezzo_1, :ld_sconto_1, :ld_quantita_2, :ld_prezzo_2, :ld_sconto_2, :ld_quantita_3, :ld_prezzo_3, 
								:ld_sconto_3, :ld_quantita_4, :ld_prezzo_4, :ld_sconto_4, :ld_quantita_5, :ld_prezzo_5, :ld_sconto_5, :ls_flag_for_pref, 
								:ld_fat_conversione, :ls_cod_misura;

   if sqlca.sqlcode <> 0 or ll_i = 1 then exit
loop

if sqlca.sqlcode = 0 and (ld_prezzo_1 <> 0 or &
                          ld_prezzo_2 <> 0 or &
                          ld_prezzo_3 <> 0 or &
                          ld_prezzo_4 <> 0 or &
                          ld_prezzo_5 <> 0) then
	if ad_quantita <= ld_quantita_1 then
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "prezzo_acquisto", ld_prezzo_1)
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "sconto_1", ld_sconto_1)
		ad_prezzo_acquisto = ld_prezzo_1
		ad_sconto_1 = ld_sconto_1
		
	elseif ad_quantita <= ld_quantita_2 then
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "prezzo_acquisto", ld_prezzo_2)
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "sconto_1", ld_sconto_2)
		ad_prezzo_acquisto = ld_prezzo_2
		ad_sconto_1 = ld_sconto_2
		
	elseif ad_quantita <= ld_quantita_3 then
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "prezzo_acquisto", ld_prezzo_3)
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "sconto_1", ld_sconto_3)
		ad_prezzo_acquisto = ld_prezzo_3
		ad_sconto_1 = ld_sconto_3
		
	elseif ad_quantita <= ld_quantita_4 then
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "prezzo_acquisto", ld_prezzo_4)
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "sconto_1", ld_sconto_4)
		ad_prezzo_acquisto = ld_prezzo_4
		ad_sconto_1 = ld_sconto_4
		
	elseif ad_quantita <= ld_quantita_5 then
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "prezzo_acquisto", ld_prezzo_5)
		//pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "sconto_1", ld_sconto_5)
		ad_prezzo_acquisto = ld_prezzo_5
		ad_sconto_1 = ld_sconto_5
		
	end if

    //pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "fat_conversione", ld_fat_conversione)
    //pcca.window_currentdw.setitem(pcca.window_currentdw.getrow(), "cod_misura", ls_cod_misura)
	ad_fat_conversione = ld_fat_conversione
	as_cod_misura = ls_cod_misura

   lw_cs_xx_mdi.setmicrohelp("Applicato Listino Fornitore con Data Validità " + string(ldt_data_inizio_val, "dd/mm/yyyy"))
//***********claudia 03/01/07 perchè richiesta da Riello di ptenda che vuole venga tolta 
//   if ls_flag_for_pref = 'N' then
//      messagebox("Attenzione", "Fornitore non Preferenziale.", exclamation!, ok!)
//   end if
end if

close cu_nome;

return 0
end function

public function integer uof_inserisci_rif_vs_ordine_in_fattura (integer ai_anno, long al_numero, long al_riga, string as_cod_tipo_det_ven, string as_descrizione, string as_cod_iva, ref string as_errore);insert into det_fat_ven  
	(	cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		prog_riga_fat_ven,   
		cod_tipo_det_ven,   
		cod_deposito,
		cod_ubicazione,
		cod_lotto,
		data_stock,
		progr_stock,
		cod_misura,   
		cod_prodotto,   
		des_prodotto,   
		quan_fatturata,   
		prezzo_vendita,   
		sconto_1,   
		sconto_2,   
		provvigione_1,
		provvigione_2,
		cod_iva,   
		cod_tipo_movimento,
		num_registrazione_mov_mag,
		anno_registrazione_fat,
		num_registrazione_fat,
		nota_dettaglio,   
		anno_registrazione_bol_ven,   
		num_registrazione_bol_ven,   
		prog_riga_bol_ven,
		anno_commessa,
		num_commessa,
		cod_centro_costo,
		sconto_3,   
		sconto_4,   
		sconto_5,   
		sconto_6,   
		sconto_7,   
		sconto_8,   
		sconto_9,   
		sconto_10,
		anno_registrazione_mov_mag,
		fat_conversione_ven,   
		anno_reg_des_mov,
		num_reg_des_mov,
		anno_reg_ord_ven,   
		num_reg_ord_ven,   
		prog_riga_ord_ven,
		cod_versione,
		num_confezioni,
		num_pezzi_confezione,
		flag_st_note_det,
		quantita_um,
		prezzo_um)
values	(	:s_cs_xx.cod_azienda,   
				:ai_anno,   
				:al_numero,   
				:al_riga,   
				:as_cod_tipo_det_ven,   
				null,
				null,
				null,
				null,
				null,
				null,
				null,
				:as_descrizione,   
				0,   
				0,   
				0,   
				0,  
				0,
				0,
				:as_cod_iva,   
				null,
				null,
				null,
				null,
				null,   
				null,
				null,
				null,
				null,   
				null,   
				null,   
				0,   
				0,   
				0,   
				0,   
				0,   
				0,   
				0,   
				0,
				null,
				1,
				null,
				null,
				null,   
				null,   
				null,
				null,
				0,
				0,
				'I',
				0,
				0);					
				
if sqlca.sqlcode <> 0 then
	as_errore = "Errore durante la scrittura Dettagli RIF. VS ORDINE in Fattura:  " + sqlca.sqlerrtext
	return -1
end if

return 0

end function

public function integer uof_controlla_buchi (string as_gestione, string as_cod_documento, string as_numeratore_documento, integer ai_anno_esercizio, long al_num_documento, datetime adt_oggi, ref string as_messaggio);string							ls_select, ls_errore, ls_colonne, ls_tabella, ls_where, ls_order_by, ls_testo_incongruenza_numeri, ls_testo_incongruenza_date, ls_tipo, ls_ret
long							ll_i, ll_rows, ll_numero_doc, ll_numero_doc_prec
datetime						ldt_data_doc, ldt_data_doc_prec
datastore					lds_data
s_cs_xx_parametri			lstr_parametri


ls_testo_incongruenza_numeri = ""
ls_testo_incongruenza_date = ""


choose case as_gestione
	case "bol_ven"
		ls_tipo = "DDT"
		ls_colonne = "num_documento, data_bolla"
		ls_tabella = "tes_bol_ven"
		ls_where = "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
								"anno_documento = " + string(ai_anno_esercizio) + " and "+&
								"cod_documento='" + as_cod_documento + "' and "+&
								"numeratore_documento='" + as_numeratore_documento + "' "
		ls_order_by = "order by num_documento, data_bolla "
		
	case "fat_ven"
		ls_tipo = "FATTURE"
		ls_colonne = "num_documento, data_fattura"
		ls_tabella = "tes_fat_ven"
		ls_where = "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
								"anno_documento = " + string(ai_anno_esercizio) + " and "+&
								"cod_documento='" + as_cod_documento + "' and "+&
								"numeratore_documento='" + as_numeratore_documento + "' "
		ls_order_by = "order by num_documento, data_fattura "

end choose


ls_select = "select "+ls_colonne+" from "+ls_tabella+" " + ls_where + " " + ls_order_by

ll_rows = guo_functions.uof_crea_datastore (lds_data, ls_select, as_messaggio)

if ll_rows < 0 then
	return -1
end if

for ll_i = 1 to ll_rows
	
	ll_numero_doc = lds_data.getitemnumber(ll_i, 1)
	ldt_data_doc = lds_data.getitemdatetime(ll_i, 2)
	
	if ll_i = 1 then
		ll_numero_doc_prec = ll_numero_doc
		ldt_data_doc_prec = ldt_data_doc
	else
		if (ll_numero_doc - ll_numero_doc_prec) <> 1 then
			
			//la prima volta valorizzo la parte iniziale
			if ls_testo_incongruenza_numeri="" then ls_testo_incongruenza_numeri = "Attenzione!~r~nNon vi è congruenza di numerazione nella sequenza "+ls_tipo+"~r~nnella serie " + &
																						as_cod_documento + " / " + as_numeratore_documento + " fra le seguenti coppie di numeri:~r~n"
			ls_testo_incongruenza_numeri += "~r~nfra " + ls_tipo + " numero " + string(ll_numero_doc_prec) + " e " + ls_tipo + " numero " + string(ll_numero_doc)
			
		end if
		
		if ldt_data_doc < ldt_data_doc_prec then
			//la prima volta valorizzo la parte iniziale
			if ls_testo_incongruenza_date="" then ls_testo_incongruenza_date = "Attenzione!~r~nNon vi è congruenza di date nella sequenza " + ls_tipo + "~r~nnella serie " + &
																						as_cod_documento + " / " + as_numeratore_documento + " fra le seguenti coppie di numeri:~r~n"
			ls_testo_incongruenza_date += "~r~nfra " + ls_tipo + " numero " + string(ll_numero_doc_prec) + " del " + string(ldt_data_doc_prec, "dd/mm/yyyy") + &
															 " e " + ls_tipo + " numero " + string(ll_numero_doc) + " del " + string(ldt_data_doc, "dd/mm/yyyy")
			
		end if
		
		ll_numero_doc_prec = ll_numero_doc
		ldt_data_doc_prec = ldt_data_doc
		
	end if
	
next

destroy lds_data


if len(ls_testo_incongruenza_numeri)>0 or len(ls_testo_incongruenza_date)>0 then
	lstr_parametri.parametro_s_1_a[1] = ls_testo_incongruenza_numeri
	lstr_parametri.parametro_s_1_a[2] = ls_testo_incongruenza_date
	openwithparm(w_mostra_buchi_documenti, lstr_parametri)
	
	ls_ret = message.stringparm
	
	if ls_ret="OK" then
		//hai risposto PROSEGUI
		return 0
	else
		//hai risposto ANNULLA
		return 1
	end if
end if

//NESSUNA INCONGRUENZA TROVATA
return 0
end function

public function integer uof_note_in_fattura (integer ai_anno_fattura, long al_num_fattura, ref long al_prog_riga_fat_ven, ref string as_errore);string						ls_cod_prodotto_nota, ls_cod_lingua, ls_cod_tipo_det_ven_nota, ls_des_prodotto_nota, ls_cod_tipo_fat_ven, ls_sql, ls_cod_nota, ls_nota_base, &
							ls_note_conformita[], ls_temp, ls_cod_iva, ls_testo_log, ls_cod_modello

datastore				lds_data

long						ll_tot, ll_index, ll_new
uo_log_sistema				luo_log


//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//se il parametro aziendale generale è disattivo esci subito
if not ib_ANC then return 0


//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//questa select verifica se
//	- il codice modello in fattura è abilitato all'uso delle note conformità		(colonna tab_flags_configuratore.cod_prodotto_nota NOT NULL)
//	- il tipo fattura prevede questa gestione											(colonna tab_tipi_fat_ven.flag_nota_conformita = 'S')

//la select recupera quindi le informazioni per poter inserire le righe di nota in fattura
//se non torna nessun dato allora vuol dire che non c'è l'abilitazione
select 	f.cod_prodotto_nota,
			c.cod_lingua,
			tipi.cod_tipo_det_ven_nota,
			tipi.des_prodotto_nota,
			t.cod_tipo_fat_ven,
			d.cod_iva,
			d.cod_prodotto
into		:ls_cod_prodotto_nota,
			:ls_cod_lingua,
			:ls_cod_tipo_det_ven_nota,
			:ls_des_prodotto_nota,
			:ls_cod_tipo_fat_ven,
			:ls_cod_iva,
			:ls_cod_modello
from det_fat_ven as d
join tab_flags_configuratore as f on 	f.cod_azienda=d.cod_azienda and
												f.cod_modello=d.cod_prodotto
join tes_fat_ven as t on 	t.cod_azienda=d.cod_azienda and
								t.anno_registrazione=d.anno_registrazione and
								t.num_registrazione=d.num_registrazione
join anag_clienti as c on c.cod_azienda=t.cod_azienda and
								c.cod_cliente=t.cod_cliente
join tab_tipi_fat_ven as tipi on 	tipi.cod_azienda=t.cod_azienda and
										tipi.cod_tipo_fat_ven=t.cod_tipo_fat_ven
where 	d.cod_azienda=:s_cs_xx.cod_azienda and
			d.anno_registrazione=:ai_anno_fattura and
			t.num_registrazione=:al_num_fattura and
			d.prog_riga_fat_ven=:al_prog_riga_fat_ven and
			f.cod_prodotto_nota is not null and 
			tipi.flag_nota_conformita='S';

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura valori per note conformità prod. da det_fat_ven/tipi_fat_ven/flags_per_configuratore: "+sqlca.sqlerrtext
	return -1
end if


//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//prodotto o tipo fattura non abilitato all'inserimento delle note
if g_str.isempty(ls_cod_prodotto_nota) or g_str.isempty(ls_cod_tipo_fat_ven) or sqlca.sqlcode=100 then return 0


//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//abilitato ma non è stato selezionato il tipo dettaglio da usare nelle righe di nota
if g_str.isempty(ls_cod_tipo_det_ven_nota) then
	as_errore = "Specificare il tipo dettaglio da utilizzare per le righe di note conformità in fattura (per il tipo fattura "+ls_cod_tipo_fat_ven+")"
	return -1
end if

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//leggo le note base (no traduzione)
ls_sql = 	"select cod_nota_prodotto, nota_prodotto "+&
			"from anag_prodotti_note "+&
				"where cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
							"cod_prodotto='"+ls_cod_prodotto_nota+"' "+&
				"order by cod_nota_prodotto asc"

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, as_errore)
if ll_tot<0 then
	return -1

elseif ll_tot=0 then
	//pur essendo sia il prodotto che il tipo fattura abilitati, non sono state definite note per il prodotto
	//quindi nessuna riga nota può essere inserita in fattura
	
	//----------------------------------
	//metto questo evento nel log
	ls_testo_log = "Riga Fattura "+string(ai_anno_fattura)+"/"+string(al_num_fattura)+"/"+string(al_prog_riga_fat_ven)+", Modello "+ls_cod_modello+ ", Prod.Fittizio "+ls_cod_prodotto_nota + "~r~n"
	ls_testo_log += "Nessuna nota definita in anagrafica prodotti note!"
	
	luo_log = create uo_log_sistema
	luo_log.uof_write_log_sistema_not_sqlca("NOTE.CONF.PROD", ls_testo_log)
	destroy luo_log
	//-----------------------------------
	
	destroy lds_data
	return 0
end if

if ls_cod_iva="" then setnull(ls_cod_iva)

//questo evita che venga stampato la descrizione di default del tipo dettaglio vendita
//in quanto se fosse NULL stamperebbe la descrizione default del tipo dettaglio utilizzato
if isnull(ls_des_prodotto_nota) then ls_des_prodotto_nota=""

ll_new = 0

for ll_index=1 to ll_tot
	ls_cod_nota			= lds_data.getitemstring(ll_index, 1)
	ls_nota_base		= lds_data.getitemstring(ll_index, 2)
	
	ls_temp = ""
	
	//se c'è una lingua cliente cerco la traduzione della nota, altrimenti prendo quella base
	if g_str.isnotempty(ls_cod_lingua) then
		
		//leggo la traduzione, se c'è
		select nota_prodotto_lingua
		into :ls_temp
		from anag_prodotti_note_lingue
		where	cod_azienda=:s_cs_xx.cod_azienda and
					cod_prodotto=:ls_cod_prodotto_nota and
					cod_nota_prodotto=:ls_cod_nota and
					cod_lingua=:ls_cod_lingua;

		if sqlca.sqlcode<0 then
			destroy lds_data
			as_errore = "Errore in lettura traduzione nota conformità del prodotto "+ls_cod_prodotto_nota+" : "+sqlca.sqlerrtext
			return -1
		end if

		if g_str.isnotempty(ls_temp) then ls_nota_base = ls_temp
	end if
	
	if g_str.isnotempty(ls_nota_base) then
		ll_new += 1
		ls_note_conformita[ll_index] = ls_nota_base
	else
		//-----------------------------------
		//metto nel log questo evento di nota conformità non inserita perchè vuota
		ls_testo_log = "Riga Fattura "+string(ai_anno_fattura)+"/"+string(al_num_fattura)+"/"+string(al_prog_riga_fat_ven)+", Modello "+ls_cod_modello+ ", Prod.Fittizio "+ls_cod_prodotto_nota + "~r~n"
		ls_testo_log += "La nota con codice "+ls_cod_nota+" in anagrafica prodotti note risulta vuota e quindi non è stata messa in fattura~r~n"
		if g_str.isnotempty(ls_cod_lingua) then
			ls_testo_log += "(cercato anche in eventuali traduzioni in lingua cliente "+ls_cod_lingua+") "
		else
			ls_testo_log += "(non cercato in traduzioni in quanto il cliente non ha alcuna lingua impostata) "
		end if
		
		luo_log = create uo_log_sistema
		luo_log.uof_write_log_sistema_not_sqlca("NOTE.CONF.PROD", ls_testo_log)
		destroy luo_log
		//-----------------------------------
		
	end if
next

destroy lds_data

//arrivato fin qui, se ho delle righe di note da mettere in fattura, queste sono nell'array ls_note_conformita[]
for ll_index=1 to upperbound(ls_note_conformita[])
	
	if ll_index > 1 then ls_des_prodotto_nota=""
	
	uof_note_in_fattura_prepare(ll_index, ls_des_prodotto_nota, ls_note_conformita[])
		
	//prima dell'inserimento incremento di 10 la variabile
	//questa variabile è BYREF e all'uscita della funzione conterrà il progressivo dell'ultima inserita
	al_prog_riga_fat_ven += 10
	
	insert into det_fat_ven  
		(cod_azienda,   
		 anno_registrazione,   
		 num_registrazione,   
		 prog_riga_fat_ven,   
		 cod_tipo_det_ven,   
		 cod_deposito,
		 cod_ubicazione,
		 cod_lotto,
		 data_stock,
		 progr_stock,
		 cod_misura,   
		 cod_prodotto,   
		 des_prodotto,   
		 quan_fatturata,   
		 prezzo_vendita,   
		 sconto_1,   
		 sconto_2,   
		 provvigione_1,
		 provvigione_2,
		 cod_iva,   
		 cod_tipo_movimento,
		 num_registrazione_mov_mag,
		 anno_registrazione_fat,
		 num_registrazione_fat,
		 nota_dettaglio,   
		 anno_registrazione_bol_ven,   
		 num_registrazione_bol_ven,   
		 prog_riga_bol_ven,
		 anno_commessa,
		 num_commessa,
		 cod_centro_costo,
		 sconto_3,   
		 sconto_4,   
		 sconto_5,   
		 sconto_6,   
		 sconto_7,   
		 sconto_8,   
		 sconto_9,   
		 sconto_10,
		 anno_registrazione_mov_mag,
		 fat_conversione_ven,   
		 anno_reg_des_mov,
		 num_reg_des_mov,
		 anno_reg_ord_ven,   
		 num_reg_ord_ven,   
		 prog_riga_ord_ven,
		 cod_versione,
		 num_confezioni,
		 num_pezzi_confezione,
		 flag_st_note_det,
		 quantita_um,
		 prezzo_um)
	values	(:s_cs_xx.cod_azienda,   
				 :ai_anno_fattura,   
				 :al_num_fattura,   
				 :al_prog_riga_fat_ven,   
				 :ls_cod_tipo_det_ven_nota,   
				 null,
				 null,
				 null,
				 null,
				 null,
				 null,
				 null,
				 :ls_des_prodotto_nota,   
				 0,   
				 0,   
				 0,   
				 0,  
				 0,
				 0,
				 :ls_cod_iva,   
				 null,
				 null,
				 null,
				 null,
				 :ls_note_conformita[ll_index],   
				 null,
				 null,
				 null,
				 null,   
				 null,   
				 null,   
				 0,   
				 0,   
				 0,   
				 0,   
				 0,   
				 0,   
				 0,   
				 0,
				 null,
				 1,
				 null,
				 null,
				 null,   
				 null,   
				 null,
				 null,
				 0,
				 0,
				 'I',
				 0,
				 0);
				 
	if sqlca.sqlcode < 0 then
		as_errore = "Errore in inserimento riga nota prodotto in fattura : " + sqlca.sqlerrtext
		return -1
	end if
	
next

return 0
end function

public subroutine uof_note_in_fattura_prepare (integer ai_index, ref string as_des_prodotto, ref string as_note[]);integer		li_pos


//se sei sulla prima nota e hai specificato una descrizione opzionale non fare nulla
if ai_index = 1 and g_str.isnotempty(as_des_prodotto) then return

if len(as_note[ai_index]) <= 40 then
	//carica tutta la nota nella des_prodotto e azzera la nota dettaglio
	as_des_prodotto = as_note[ai_index]
	as_note[ai_index] = ""
else
	//cerca l'ultimo carattere " " prima della posizione 40, caricando questa parte nella des_prodotto
	//e la restante parte nella nota dettaglio
	li_pos = lastpos(as_note[ai_index], " ", 40)
	
	if li_pos > 0 then
		//in li_pos la posizione dell'ultimo spazio presente tra i primi 40 caratteri
	else
		//nessuno spazio non trovato prima dei primi 40 caratteri
		//quindi prendi bruscamente i primi 40 caratteri e li metto in des_prodotto
		//e la restante parte nella nota dettaglio
		li_pos = 40
	end if
	
	as_des_prodotto = left(as_note[ai_index], li_pos)
	as_note[ai_index] = mid(as_note[ai_index], li_pos + 1, len(as_note[ai_index]))
	
end if


return
end subroutine

public function integer uof_azz_ass_ord_ven_sessione (long al_anno_registrazione, long al_num_registrazione, long al_prog_riga_ord_ven, longlong al_prog_sessione, ref string as_messaggio);boolean				lb_fine

integer				li_return

long					ll_prog_riga_ord_ven, ll_prog_stock, ll_prog_riga, ll_anno_registrazione, &
						ll_num_registrazione
						
string					ls_cod_prodotto, ls_sql_stringa, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_ubicazione, ls_perc, &
						ls_flag_tipo_det_ven, ls_cod_tipo_det_ven, ls_flag_evasione,ls_cod_prodotto_raggruppato
						
dec{4}				ld_quan_ordine, ld_quan_in_evasione, ld_quan_evasa, ld_giacenza_stock, ld_quan_ripristino_quan_impegnata, &
						ld_quan_assegnata, ld_quan_in_spedizione, ld_quan_residua, ld_quan_raggruppo, ld_quan_assegnata_temp
						
datetime				ldt_data_stock

datastore			lds_data

longlong				ll_prog_sessione

li_return = 0


if isnull(al_prog_sessione) or al_prog_sessione = 0 then
	//se non passo la sessione giusta, mi prendo i dati relativi all'ultima spedizione, e cioè quelli con il valore + alto di prog_sessione ##########################################
	ls_sql_stringa = 	"select max(prog_sessione) "+&
							"from evas_ord_ven "+&
							 "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(al_anno_registrazione) + " and num_registrazione = " + string(al_num_registrazione) + " "
	
	if not isnull(al_prog_riga_ord_ven) and al_prog_riga_ord_ven <> 0 then		// verifico se devo fare intero ordine oppure una riga specifica
		ls_sql_stringa += " and prog_riga_ord_ven=" + string(al_prog_riga_ord_ven) + " "
	end if

	li_return = guo_functions.uof_crea_datastore( lds_data, ls_sql_stringa, as_messaggio)
	if li_return>0 then
		//leggi prog_sessione
		ll_prog_sessione = lds_data.getitemnumber(1, 1)
		if ll_prog_sessione<= 0 then
			as_messaggio = g_str.format("Impossibile annullare! Sessione non valida [ordine $1/$2]", al_anno_registrazione, al_num_registrazione)
			if not isnull(al_prog_riga_ord_ven) and al_prog_riga_ord_ven > 0 then
				as_messaggio += " - riga "+ string(al_prog_riga_ord_ven)
			end if
			rollback;
			return 0
		end if
		
	elseif li_return=0 then
		//non sono state trovate spedizioni pendenti ...
		as_messaggio = g_str.format("Nessuna spedizione trovata per ordine $1/$2", al_anno_registrazione, al_num_registrazione)
		if not isnull(al_prog_riga_ord_ven) and al_prog_riga_ord_ven > 0 then
			as_messaggio += " - riga "+ string(al_prog_riga_ord_ven)
		end if
		rollback;
		return 0
		
	else
		//in as_messaggio il messaggio di errore
		rollback;
		return 0
	end if
	//##################################################################################################
else
	ll_prog_sessione = al_prog_sessione
end if


declare cu_evas_ord_ven dynamic cursor for sqlsa;
//imposto sql di base
ls_sql_stringa = "select anno_registrazione, " + &
						  "num_registrazione, " + &
						  "prog_riga_ord_ven, " + &
						  "prog_riga, " + &
						  "cod_deposito, " + &
						  "cod_ubicazione, " + &
						  "cod_lotto, " + &
						  "data_stock, " + &
						  "prog_stock, " + &
						  "cod_prodotto, " + &
						  "quan_in_evasione " + &
						  "from  evas_ord_ven " + &
						  "where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(al_anno_registrazione) + " and num_registrazione = " + string(al_num_registrazione) + &
						 " and prog_sessione="+string(ll_prog_sessione)

if not isnull(al_prog_riga_ord_ven) and al_prog_riga_ord_ven > 0 then
	ls_sql_stringa += " and prog_riga_ord_ven = " + string(al_prog_riga_ord_ven)
end if

 ls_sql_stringa += " order by prog_riga, prog_riga_ord_ven "

prepare sqlsa from :ls_sql_stringa;

open dynamic cu_evas_ord_ven;
if sqlca.sqlcode <> 0 then
	as_messaggio = "Errore in fase di OPEN cursore evas_ord_ven (uof_azz_ass_ord_ven).~r~n" + sqlca.sqlerrtext
	rollback;
	return 0
end if	

do while 0 = 0
	fetch cu_evas_ord_ven into		:ll_anno_registrazione, 
												:ll_num_registrazione, 
												:ll_prog_riga_ord_ven, 
												:ll_prog_riga,
												:ls_cod_deposito,
												:ls_cod_ubicazione,
												:ls_cod_lotto,
												:ldt_data_stock,
												:ll_prog_stock,
												:ls_cod_prodotto, 
												:ld_quan_in_evasione; 

	if sqlca.sqlcode = -1 then
		as_messaggio = "Errore in fase di lettura evas_ord_ven. Dettaglio Errore:" + sqlca.sqlerrtext
		close cu_evas_ord_ven;
		rollback;
		return 0
	end if

	if sqlca.sqlcode = 100 then exit
	
	//leggo i dati della riga dell'ordine (tipo dettaglio, quantita ordine e cod prodotto)
	select		cod_tipo_det_ven, 
				quan_ordine, 
				cod_prodotto
	into   		:ls_cod_tipo_det_ven, 
				:ld_quan_ordine, 
				:ls_cod_prodotto
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_registrazione = :al_anno_registrazione and  
			 num_registrazione = :al_num_registrazione and
			 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
			 
	if sqlca.sqlcode <> 0 then
		as_messaggio = "Errore in fase di lettura dettagli righe ordine. Dettaglio Errore:" + sqlca.sqlerrtext
		close cu_evas_ord_ven;
		rollback;
		return 0
	end if

	//se la quantità spedita supera quella dell'ordine mi limiterò a ripristinare quella dell'ordine
	if ld_quan_in_evasione > ld_quan_ordine then
		ld_quan_ripristino_quan_impegnata = ld_quan_ordine
	else
		ld_quan_ripristino_quan_impegnata = ld_quan_in_evasione
	end if
	
	// cerco il flag tipo dettaglio per verificare se è prodotto a magazzino
	select flag_tipo_det_ven
	into :ls_flag_tipo_det_ven
	from tab_tipi_det_ven
	where cod_azienda = :s_cs_xx.cod_azienda
	and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in fase di lettura Tipi Dettaglio Vendita. Dettaglio Errore:" + sqlca.sqlerrtext
		return 0
	end if			
	
	//se tipo dettaglio è Prodotti a Magazzino, faccio cose aggiuntive
	if ls_flag_tipo_det_ven = 'M' and not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
		
		//aggiorno la quantà impegnata e quella assegnata (ma primo verifico caso mai va in negativo quella assegnata)
		select quan_assegnata
		into :ld_quan_assegnata_temp
		from anag_prodotti
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_prodotto = :ls_cod_prodotto;
		
		if sqlca.sqlcode < 0 then
			as_messaggio = "Errore in lettura q.tà impegnata e assegnata per il  prodotto "+ls_cod_prodotto+": " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			rollback;
			return 0
		end if
		
		if isnull(ld_quan_assegnata_temp) then ld_quan_assegnata_temp=0
		
		if ld_quan_assegnata_temp - ld_quan_in_evasione < 0 then
			//evito l'errore che va in negativo, quindi asewmplicemenet azzero la quantità assegnata in anag_prodotti
			update anag_prodotti  
			set quan_impegnata = quan_impegnata + :ld_quan_ripristino_quan_impegnata,
				 quan_assegnata = 0
			 where cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto;
					 
		else
			//OK, procedo tranquillamente, tanto niente può andare in negativo
			update anag_prodotti  
			set quan_impegnata = quan_impegnata + :ld_quan_ripristino_quan_impegnata,
				 quan_assegnata = quan_assegnata - :ld_quan_in_evasione
			 where cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto;
		end if
		
		if sqlca.sqlcode <> 0 then
			as_messaggio = "Errore in fase di aggiornamento impegnato e assegnato del prodotto "+ls_cod_prodotto+": " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			rollback;
			return 0
		end if
		
		
		// enme 08/1/2006 gestione prodotto raggruppato ------------------------------------------------
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then

			ld_quan_raggruppo = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quan_ripristino_quan_impegnata, "M")
			
			update anag_prodotti  
			set quan_impegnata = quan_impegnata + :ld_quan_raggruppo
			where cod_azienda = :s_cs_xx.cod_azienda and  
					 cod_prodotto = :ls_cod_prodotto_raggruppato;
					 
			if sqlca.sqlcode < 0 then
				as_messaggio = "Errore in fase di aggiornamento prodotto raggruppato "+ls_cod_prodotto_raggruppato+" del prodotto "+ls_cod_prodotto+": " + sqlca.sqlerrtext
				close cu_evas_ord_ven;
				rollback;
				return 0
			end if
		end if
		//fine gestione prodotto raggruppato ---------------------------------------------------------------------------------
		
		//aggiornamento quantità assegnata dello stock ------------------------------------------------------------------------------------------------
		//controlla caso mai va in negativo, però ...
		select quan_assegnata
		into :ld_quan_assegnata_temp
		from stock
		 where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto and 
				 cod_deposito = :ls_cod_deposito and
				 cod_ubicazione = :ls_cod_ubicazione and
				 cod_lotto = :ls_cod_lotto and
				 data_stock = :ldt_data_stock and
				 prog_stock = :ll_prog_stock;
		
		if sqlca.sqlcode < 0 then
			as_messaggio = "Errore in lettura q.tà assegnata dello stock per il  prodotto "+ls_cod_prodotto+": " + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			rollback;
			return 0
		end if
		
		if isnull(ld_quan_assegnata_temp) then ld_quan_assegnata_temp=0
		
		if ld_quan_assegnata_temp - ld_quan_in_evasione < 0 then
			//evito l'errore che va in negativo, quindi semplicemente azzero la quantità assegnata in stock
			update stock  
			set quan_assegnata = 0
			 where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto and 
					 cod_deposito = :ls_cod_deposito and
					 cod_ubicazione = :ls_cod_ubicazione and
					 cod_lotto = :ls_cod_lotto and
					 data_stock = :ldt_data_stock and
					 prog_stock = :ll_prog_stock;
					 
		else
			//OK, procedo tranquillamente, tanto niente può andare in negativo
			update stock  
			set quan_assegnata = quan_assegnata - :ld_quan_in_evasione 
			 where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_prodotto = :ls_cod_prodotto and 
					 cod_deposito = :ls_cod_deposito and
					 cod_ubicazione = :ls_cod_ubicazione and
					 cod_lotto = :ls_cod_lotto and
					 data_stock = :ldt_data_stock and
					 prog_stock = :ll_prog_stock;
		end if
			 
		if sqlca.sqlcode < 0 then
			as_messaggio = "Errore in fase di aggiornamento stock. Dettaglio Errore:" + sqlca.sqlerrtext
			close cu_evas_ord_ven;
			rollback;
			return 0
		end if
	end if
	
	
	//aggiorno la quantita in evasione in det_ord_ven, verificando prima se la porto in negativo
	select quan_in_evasione
	into :ld_quan_assegnata_temp
	from det_ord_ven
	 where cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_registrazione = :al_anno_registrazione and  
			 num_registrazione = :al_num_registrazione and
			 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in lettura q.tà  in evasione in det_ord_ven: " + sqlca.sqlerrtext
		close cu_evas_ord_ven;
		rollback;
		return 0
	end if
	
	if isnull(ld_quan_assegnata_temp) then ld_quan_assegnata_temp=0
	
	if ld_quan_assegnata_temp < ld_quan_in_evasione then
		update det_ord_ven
		set quan_in_evasione = 0
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 anno_registrazione = :al_anno_registrazione and  
				 num_registrazione = :al_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	else
		update det_ord_ven
		set quan_in_evasione = quan_in_evasione - :ld_quan_in_evasione
		 where cod_azienda = :s_cs_xx.cod_azienda and  
				 anno_registrazione = :al_anno_registrazione and  
				 num_registrazione = :al_num_registrazione and
				 prog_riga_ord_ven = :ll_prog_riga_ord_ven;
	end if

	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in fase di aggiornamento riga ordine. Dettaglio Errore:" + sqlca.sqlerrtext
		rollback;
		close cu_evas_ord_ven;
		return 0
	end if

	delete evas_ord_ven
	where cod_azienda = :s_cs_xx.cod_azienda and 
			anno_registrazione = :al_anno_registrazione and 
			num_registrazione  = :al_num_registrazione and
			prog_riga_ord_ven  = :ll_prog_riga_ord_ven and
			prog_riga = :ll_prog_riga; // and
			//prog_sessione=:ll_prog_sessione;
	
	if sqlca.sqlcode < 0 then
		as_messaggio = "Errore in fase di cancellazione evas_ord_ven.Dettaglio Errore:" + sqlca.sqlerrtext
		rollback;
		return 0
	end if

	li_return = 1
loop

close cu_evas_ord_ven;

commit;

return li_return
end function

on uo_generazione_documenti.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_generazione_documenti.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

event constructor;guo_functions.uof_get_parametro_azienda( "GIB", ib_gibus)
if isnull(ib_gibus) then ib_gibus = false

guo_functions.uof_get_parametro( "MST", ib_mast)
if isnull(ib_mast) then ib_mast = false

guo_functions.uof_get_parametro_azienda( "ANC", ib_ANC)
if isnull(ib_ANC) then ib_ANC = false


end event

