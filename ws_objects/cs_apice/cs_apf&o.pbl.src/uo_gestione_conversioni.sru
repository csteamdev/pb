﻿$PBExportHeader$uo_gestione_conversioni.sru
$PBExportComments$UO Gestione delle conversioni con UM diverse nei documenti.
forward
global type uo_gestione_conversioni from nonvisualobject
end type
end forward

global type uo_gestione_conversioni from nonvisualobject
end type
global uo_gestione_conversioni uo_gestione_conversioni

type variables
string is_messaggio
end variables

forward prototypes
public function integer uof_visualizza_um (datawindow dw_dati, string fs_tipo_gestione)
public function integer uof_blocca_colonne (datawindow dw_dati, string fs_tipo_gestione)
public function integer uof_ricalcola_quantita (ref datawindow fdw_dati, string fs_tipo_gestione, string fs_colonna_cambiata, double fd_quan_mag, double fd_prezzo_mag, double fd_quan_um, double fd_prezzo_um, double fd_fat_conversione, string fs_cod_valuta)
public subroutine uof_arrotonda (ref decimal valore, decimal precisione)
end prototypes

public function integer uof_visualizza_um (datawindow dw_dati, string fs_tipo_gestione);// **************************************************************************************//
//
//			OGGETTO DI VISUALIZZAZIONE DATI RELATIVI ALLE U.M. SE DIVERSE FRA VENDITA E MAGAZZINO
//			
//
//       AUTORE: ENRICO MENEGOTTO x  PROGETTOTENDA S.P.A.
//
//       Variabile			Passaggio	Dato		Commento
//       =================================================================================
//       dw_dati				val			DW			DW su cui eseguire le operazioni
//			fs_tipo_gestione  val			DW			DW Tipo gestione
//
// **************************************************************************************//
string ls_flag_abilita_quan_mag, ls_flag_abilita_prezzo_mag, ls_flag_abilita_quan_um, ls_flag_abilita_prezzo_um, &
       ls_colonna_quantita, ls_colonna_prezzo, ls_cod_misura_prodotto, ls_cod_misura_ven, ls_cod_prodotto, ls_cod_tipo_det_ven, &
		 ls_flag_tipo_det_ven

if  dw_dati.getrow() <= 0 then
	dw_dati.Object.quantita_um.Visible = 0
	dw_dati.Object.prezzo_um.Visible = 0
	dw_dati.modify("st_prezzo.text='Prezzo:'")
	dw_dati.modify("st_quantita.text='Qtà:'")
	dw_dati.object.st_quan_um_t.visible = 0
	dw_dati.object.st_prezzo_um_t.visible = 0
	dw_dati.object.st_fattore_conv.visible = 0
	dw_dati.object.fat_conversione_ven_t.visible = 0
	dw_dati.object.fat_conversione_ven.visible = 0
	return 0
end if

ls_cod_misura_ven = dw_dati.getitemstring(dw_dati.getrow(),"cod_misura")
ls_cod_prodotto = dw_dati.getitemstring(dw_dati.getrow(),"cod_prodotto")
ls_cod_tipo_det_ven = dw_dati.getitemstring(dw_dati.getrow(),"cod_tipo_det_ven")

select flag_tipo_det_ven
into   :ls_flag_tipo_det_ven
from   tab_tipi_det_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_det_ven = :ls_cod_tipo_det_ven;
if sqlca.sqlcode <> 0 then
	ls_flag_tipo_det_ven = "N"
end if
choose case ls_flag_tipo_det_ven
	case "C", "M"
		
		select cod_misura_mag
		into   :ls_cod_misura_prodotto
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode <> 0 then
			dw_dati.modify("st_prezzo.text='Prezzo:'")
			dw_dati.modify("st_quantita.text='Qtà:'")
			dw_dati.object.st_quan_um_t.visible = 0
			dw_dati.object.st_prezzo_um_t.visible = 0
			dw_dati.object.st_fattore_conv.visible = 0
			dw_dati.object.fat_conversione_ven_t.visible = 0
			dw_dati.object.fat_conversione_ven.visible = 0
			return 0
		else
			if (ls_cod_misura_prodotto = ls_cod_misura_ven) or isnull(ls_cod_misura_prodotto) or isnull(ls_cod_misura_ven) then
				dw_dati.Object.quantita_um.Visible = 0
				dw_dati.Object.prezzo_um.Visible = 0
				dw_dati.object.st_quan_um_t.visible = 0
				dw_dati.object.st_prezzo_um_t.visible = 0
				dw_dati.object.st_fattore_conv.visible = 0
				dw_dati.object.fat_conversione_ven_t.visible = 0
				dw_dati.object.fat_conversione_ven.visible = 0
				dw_dati.modify("st_prezzo.text='Prezzo:'")
				dw_dati.modify("st_quantita.text='Qtà:'")
			else
				dw_dati.Object.quantita_um.Visible = 1
				dw_dati.Object.prezzo_um.Visible = 1
				dw_dati.object.st_quan_um_t.visible = 1
				dw_dati.object.st_prezzo_um_t.visible = 1
				dw_dati.object.st_fattore_conv.visible = 1
				dw_dati.object.fat_conversione_ven_t.visible = 1
				dw_dati.object.fat_conversione_ven.visible = 1
				dw_dati.modify("st_prezzo.text='Prezzo in " + ls_cod_misura_prodotto + ":'")
				dw_dati.modify("st_quantita.text='Qtà in " + ls_cod_misura_prodotto + ":'")
			end if
		end if
		
		if ls_cod_misura_prodotto <> dw_dati.getitemstring(dw_dati.getrow(),"cod_misura") and len(trim(dw_dati.getitemstring(dw_dati.getrow(),"cod_misura"))) <> 0 and len(trim(ls_cod_misura_prodotto)) <> 0 then
			dw_dati.modify("st_fattore_conv.text='(1 " + ls_cod_misura_prodotto + "=" + trim(f_double_string(dw_dati.getitemnumber(dw_dati.getrow(),"fat_conversione_ven"))) + " " + dw_dati.getitemstring(dw_dati.getrow(),"cod_misura") + ")'")
		else
			dw_dati.modify("st_fattore_conv.text=''")		
		end if
	case else
		dw_dati.Object.quantita_um.Visible = 0
		dw_dati.Object.prezzo_um.Visible = 0
		dw_dati.modify("st_prezzo.text='Prezzo:'")
		dw_dati.modify("st_quantita.text='Qtà:'")
		dw_dati.object.st_quan_um_t.visible = 0
		dw_dati.object.st_prezzo_um_t.visible = 0
		dw_dati.object.st_fattore_conv.visible = 0
		dw_dati.object.fat_conversione_ven_t.visible = 0
		dw_dati.object.fat_conversione_ven.visible = 0
end choose	
return 0
end function

public function integer uof_blocca_colonne (datawindow dw_dati, string fs_tipo_gestione);// **************************************************************************************//
//
//			OGGETTO DI ABILITAZIONE O DISATTIVAZIONE COLONNE QUANTITA' E PREZZO IN BASE
//			AI PARAMETRI DELLA CON_VENDITE
//
//       AUTORE: ENRICO MENEGOTTO x  PROGETTOTENDA S.P.A.
//
//       Variabile			Passaggio	Dato		Commento
//       =================================================================================
//       dw_dati				ref			DW			DW su cui eseguire le operazioni
//
// **************************************************************************************//
string ls_flag_abilita_quan_mag, ls_flag_abilita_prezzo_mag, ls_flag_abilita_quan_um, ls_flag_abilita_prezzo_um, &
       ls_colonna_quantita, ls_colonna_prezzo, ls_cod_misura_prodotto, ls_cod_misura_ven, ls_cod_prodotto


fs_tipo_gestione = upper(fs_tipo_gestione)
choose case fs_tipo_gestione
	case "OFF_VEN"
		ls_colonna_quantita = "quan_offerta"
		ls_colonna_prezzo = "prezzo_vendita"
	case "ORD_VEN"
		ls_colonna_quantita = "quan_ordine"
		ls_colonna_prezzo = "prezzo_vendita"
	case "BOL_VEN"
		ls_colonna_quantita = "quan_consegnata"
		ls_colonna_prezzo = "prezzo_vendita"
	case "FAT_VEN"
		ls_colonna_quantita = "quan_fatturata"
		ls_colonna_prezzo = "prezzo_vendita"
end choose

ls_cod_misura_ven = dw_dati.getitemstring(dw_dati.getrow(),"cod_misura")
ls_cod_prodotto = dw_dati.getitemstring(dw_dati.getrow(),"cod_prodotto")
select cod_misura_mag
into   :ls_cod_misura_prodotto
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_prodotto = :ls_cod_prodotto;
if sqlca.sqlcode <> 0 then   // ** prodotto inesistente o prodotto non  a magazzino
	dw_dati.modify(ls_colonna_quantita + ".protect=0")
	dw_dati.modify(ls_colonna_quantita + ".background.color='16777215'")
	dw_dati.modify(ls_colonna_prezzo + ".protect=0")
	dw_dati.modify(ls_colonna_prezzo + ".background.color='16777215'")
	return 0
else									// ** il prodotto esiste, ma vado a controllare se le unità di misura sono diverse
	if (ls_cod_misura_prodotto = ls_cod_misura_ven) or isnull(ls_cod_misura_prodotto) or isnull(ls_cod_misura_ven) then
		dw_dati.modify(ls_colonna_quantita + ".protect=0")
		dw_dati.modify(ls_colonna_quantita + ".background.color='16777215'")
		dw_dati.modify(ls_colonna_prezzo + ".protect=0")
		dw_dati.modify(ls_colonna_prezzo + ".background.color='16777215'")
		return 0
	end if
end if

// a questo punto sono in una riga in cui l'unità del dettaglio è diversa da quella di magazzino;
// vado quindi ad attivare tutti i campi necessari
select flag_abilita_quan_mag,
       flag_abilita_prezzo_mag,
		 flag_abilita_quan_ven,
		 flag_abilita_prezzo_ven
into   :ls_flag_abilita_quan_mag,
       :ls_flag_abilita_prezzo_mag,
		 :ls_flag_abilita_quan_um,
		 :ls_flag_abilita_prezzo_um
from   con_vendite
where  cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then
	is_messaggio = "Errore in select tabella parametri vendite. Dettaglio = " + sqlca.sqlerrtext
	return -1
end if

if ls_flag_abilita_quan_mag = "S" then
	dw_dati.modify(ls_colonna_quantita + ".protect=0")
	dw_dati.modify(ls_colonna_quantita + ".background.color='16777215'")
else
	dw_dati.modify(ls_colonna_quantita + ".protect=1")
	dw_dati.modify(ls_colonna_quantita + ".background.color='12632256'")
end if

if ls_flag_abilita_prezzo_mag = "S" then
	dw_dati.modify(ls_colonna_prezzo + ".protect=0")
	dw_dati.modify(ls_colonna_prezzo + ".background.color='16777215'")
else
	dw_dati.modify(ls_colonna_prezzo + ".protect=1")
	dw_dati.modify(ls_colonna_prezzo + ".background.color='12632256'")
end if
	
if ls_flag_abilita_quan_um = "S" then
	dw_dati.modify("quantita_um.protect=0")
	dw_dati.modify("quantita_um.background.color='16777215'")
else
	dw_dati.modify("quantita_um.protect=1")
	dw_dati.modify("quantita_um.background.color='12632256'")
end if

if ls_flag_abilita_prezzo_um = "S" then
	dw_dati.modify("prezzo_um.protect=0")
	dw_dati.modify("prezzo_um.background.color='16777215'")
else
	dw_dati.modify("prezzo_um.protect=1")
	dw_dati.modify("prezzo_um.background.color='12632256'")
end if
return 0
end function

public function integer uof_ricalcola_quantita (ref datawindow fdw_dati, string fs_tipo_gestione, string fs_colonna_cambiata, double fd_quan_mag, double fd_prezzo_mag, double fd_quan_um, double fd_prezzo_um, double fd_fat_conversione, string fs_cod_valuta);// **************************************************************************************//
//
//			OGGETTO DI GESTIONE DEI CALCOLI PER LE CONVERSIONI NEL CASO DI UTILIZZO
//       U.M. DIVERSE FRA MAGAZZINO E VENDITA NEI DOCUMENTI DI VENDITA
//
//       AUTORE: ENRICO MENEGOTTO x  PROGETTOTENDA S.P.A.
//
//       Variabile				Passaggio	Dato		Commento
//       =================================================================================
//       fdw_dati					ref			DW			DW su cui eseguire i calcoli e su cui
//																scrivere i risultati
//			fs_tipo_gestione  	val			string	Tipo gestione in cui mi trovo
//       fs_colonna_cambiata 	val			string   Colonna su cui è stato fatto il cambiamento; può avere solio i seguenti valori
//																	- "quan_doc" = quantità della riga riferita alla UM di magazzino
//																	- "prezzo_doc" = prezzo della riga riferita alla UM di magazzino
//																	- "quan_um" = quantità della riga riferita alla UM di vendita
//																	- "prezzo_um" = prezzo della riga riferita alla UM di vendita
//																	- "fattore" = fattore di conversione
//			fd_quan_mag				val			double	Quantità della riga del documento riferita all'unità di misura di magazzino
//			fd_prezzo_mag			val			double	Prezzo della riga del documento riferita all'unità di misura di magazzino
//			fd_quan_um				val			double	Quantità della riga del documento riferita all'unità di misura della riga
//			fd_prezzo_um			val			double	Prezzo della riga del documento riferita all'unità di misura della riga
//			fd_fat_conversione	val			double	fattore di conversione
//			fs_cod_valuta			val			string	valuta in cui sono espressi i valori
//
// **************************************************************************************//
string ls_flag_tipo_azione_quan_mag, ls_flag_tipo_azione_prezzo_mag, ls_flag_tipo_azione_quan_ven, ls_flag_tipo_azione_prezzo_ven, &
       ls_colonna_quantita, ls_colonna_prezzo, ls_flag_azione_conv_ven, ls_flag_azione_conv_mag, ls_errore
dec{5} ld_quantita_new, ld_prezzo_new,ld_precisione_prezzo_mag,ld_precisione_prezzo_ven

select flag_tipo_azione_quan_mag,
       flag_tipo_azione_prezzo_mag,
		 flag_tipo_azione_quan_ven,
		 flag_tipo_azione_prezzo_ven,
		 flag_azione_conv_mag,
		 flag_azione_conv_ven,
		 precisione_prezzo_mag,
		 precisione_prezzo_ven
into   :ls_flag_tipo_azione_quan_mag, 
       :ls_flag_tipo_azione_prezzo_mag, 
		 :ls_flag_tipo_azione_quan_ven, 
		 :ls_flag_tipo_azione_prezzo_ven,
		 :ls_flag_azione_conv_mag,
		 :ls_flag_azione_conv_ven,
		 :ld_precisione_prezzo_mag,
		 :ld_precisione_prezzo_ven
from   con_vendite
where  cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode <> 0 then
	is_messaggio = "Errore in select tabella parametri vendite. Dettaglio = " + sqlca.sqlerrtext
	return -1
end if

fs_tipo_gestione = upper(fs_tipo_gestione)
choose case fs_tipo_gestione
	case "OFF_VEN"
		ls_colonna_quantita = "quan_offerta"
		ls_colonna_prezzo = "prezzo_vendita"
	case "ORD_VEN"
		ls_colonna_quantita = "quan_ordine"
		ls_colonna_prezzo = "prezzo_vendita"
	case "BOL_VEN"
		ls_colonna_quantita = "quan_consegnata"
		ls_colonna_prezzo = "prezzo_vendita"
	case "FAT_VEN"
		ls_colonna_quantita = "quan_fatturata"
		ls_colonna_prezzo = "prezzo_vendita"
end choose

uo_condizioni_cliente luo_condizioni_cliente
luo_condizioni_cliente = CREATE uo_condizioni_cliente

fs_colonna_cambiata = lower (fs_colonna_cambiata)
choose case fs_colonna_cambiata
	case "quan_doc"
		choose case ls_flag_tipo_azione_quan_mag
			case "Q"
				ld_quantita_new = round(fd_quan_mag * fd_fat_conversione, 4)
				fdw_dati.setitem(fdw_dati.getrow(), "quantita_um", ld_quantita_new)
			case "P"
				ld_prezzo_new = fd_prezzo_mag / fd_fat_conversione
				uof_arrotonda(ld_prezzo_new, ld_precisione_prezzo_ven)
				fdw_dati.setitem(fdw_dati.getrow(), "prezzo_um", ld_prezzo_new)
			case "T"
				ld_quantita_new = round(fd_quan_mag * fd_fat_conversione, 4)
				fdw_dati.setitem(fdw_dati.getrow(), "quantita_um", ld_quantita_new)
				ld_prezzo_new = fd_prezzo_mag / fd_fat_conversione
				uof_arrotonda(ld_prezzo_new, ld_precisione_prezzo_ven)
				fdw_dati.setitem(fdw_dati.getrow(), "prezzo_um", ld_prezzo_new)
		end choose
	case "prezzo_doc"
		choose case ls_flag_tipo_azione_prezzo_mag
			case "Q"
				ld_quantita_new = round(fd_quan_mag * fd_fat_conversione, 4)
				fdw_dati.setitem(fdw_dati.getrow(), "quantita_um", ld_quantita_new)
			case "P"
				ld_prezzo_new = fd_prezzo_mag / fd_fat_conversione
				uof_arrotonda(ld_prezzo_new, ld_precisione_prezzo_ven)
				fdw_dati.setitem(fdw_dati.getrow(), "prezzo_um", ld_prezzo_new)
			case "T"
				ld_quantita_new = round(fd_quan_mag * fd_fat_conversione, 4)
				fdw_dati.setitem(fdw_dati.getrow(), "quantita_um", ld_quantita_new)
				ld_prezzo_new = fd_prezzo_mag / fd_fat_conversione
				uof_arrotonda(ld_prezzo_new, ld_precisione_prezzo_ven)
				fdw_dati.setitem(fdw_dati.getrow(), "prezzo_um", ld_prezzo_new)
		end choose
	case "quan_um"
		choose case ls_flag_tipo_azione_quan_ven
			case "Q"
				ld_quantita_new = round(fd_quan_um / fd_fat_conversione, 4)
				fdw_dati.setitem(fdw_dati.getrow(), ls_colonna_quantita, ld_quantita_new)
			case "P"
				ld_prezzo_new = fd_prezzo_um * fd_fat_conversione
				uof_arrotonda(ld_prezzo_new, ld_precisione_prezzo_mag)
				fdw_dati.setitem(fdw_dati.getrow(), ls_colonna_prezzo, ld_prezzo_new)
			case "T"
				ld_quantita_new = round(fd_quan_um / fd_fat_conversione, 4)
				fdw_dati.setitem(fdw_dati.getrow(), ls_colonna_quantita, ld_quantita_new)
				ld_prezzo_new = fd_prezzo_um * fd_fat_conversione
				uof_arrotonda(ld_prezzo_new, ld_precisione_prezzo_mag)
				fdw_dati.setitem(fdw_dati.getrow(), ls_colonna_prezzo, ld_prezzo_new)
		end choose
	case "prezzo_um"
		choose case ls_flag_tipo_azione_prezzo_ven
			case "Q"
				ld_quantita_new = round(fd_quan_um / fd_fat_conversione, 4)
				fdw_dati.setitem(fdw_dati.getrow(), ls_colonna_quantita, ld_quantita_new)
			case "P"
				ld_prezzo_new = fd_prezzo_um * fd_fat_conversione
				uof_arrotonda(ld_prezzo_new, ld_precisione_prezzo_mag)
				fdw_dati.setitem(fdw_dati.getrow(), ls_colonna_prezzo, ld_prezzo_new)
			case "T"
				ld_quantita_new = round(fd_quan_um / fd_fat_conversione, 4)
				fdw_dati.setitem(fdw_dati.getrow(), ls_colonna_quantita, ld_quantita_new)
				ld_prezzo_new = fd_prezzo_um * fd_fat_conversione
				uof_arrotonda(ld_prezzo_new, ld_precisione_prezzo_mag)
				fdw_dati.setitem(fdw_dati.getrow(), ls_colonna_prezzo, ld_prezzo_new)
		end choose
	case "fattore"
		choose case ls_flag_azione_conv_ven
			case "Q"
				ld_quantita_new = round(fd_quan_mag * fd_fat_conversione, 4)
				fdw_dati.setitem(fdw_dati.getrow(), "quantita_um", ld_quantita_new)
			case "P"
				ld_prezzo_new = fd_prezzo_mag / fd_fat_conversione
				uof_arrotonda(ld_prezzo_new, ld_precisione_prezzo_mag)
				fdw_dati.setitem(fdw_dati.getrow(), "prezzo_um", ld_prezzo_new)
			case "T"
				ld_quantita_new = round(fd_quan_mag * fd_fat_conversione, 4)
				fdw_dati.setitem(fdw_dati.getrow(), "quantita_um", ld_quantita_new)
				ld_prezzo_new = fd_prezzo_mag / fd_fat_conversione
				uof_arrotonda(ld_prezzo_new, ld_precisione_prezzo_ven)
				fdw_dati.setitem(fdw_dati.getrow(), "prezzo_um", ld_prezzo_new)
		end choose
		choose case ls_flag_azione_conv_mag
			case "Q"
				ld_quantita_new = round(fd_quan_um / fd_fat_conversione, 4)
				fdw_dati.setitem(fdw_dati.getrow(), ls_colonna_quantita, ld_quantita_new)
			case "P"
				ld_prezzo_new = fd_prezzo_um * fd_fat_conversione
				uof_arrotonda(ld_prezzo_new, ld_precisione_prezzo_ven)
				fdw_dati.setitem(fdw_dati.getrow(), ls_colonna_prezzo, ld_prezzo_new)
			case "T"
				ld_quantita_new = round(fd_quan_um / fd_fat_conversione, 4)
				fdw_dati.setitem(fdw_dati.getrow(), ls_colonna_quantita, ld_quantita_new)
				ld_prezzo_new = fd_prezzo_um * fd_fat_conversione
				uof_arrotonda(ld_prezzo_new, ld_precisione_prezzo_ven)
				fdw_dati.setitem(fdw_dati.getrow(), ls_colonna_prezzo, ld_prezzo_new)
		end choose
end choose
destroy luo_condizioni_cliente
return 0
end function

public subroutine uof_arrotonda (ref decimal valore, decimal precisione);dec{4} ld_prezzo_tondo

ld_prezzo_tondo = valore

if precisione <> 0 then
	// arrotondamento per difetto
	ld_prezzo_tondo = (truncate((valore/precisione), 0) * precisione)
	// arrotondamento per eccesso
	if (valore - ld_prezzo_tondo) > (precisione / 2) then
		ld_prezzo_tondo = ld_prezzo_tondo + precisione
	end if	
end if	
valore = ld_prezzo_tondo

end subroutine

on uo_gestione_conversioni.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_gestione_conversioni.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

