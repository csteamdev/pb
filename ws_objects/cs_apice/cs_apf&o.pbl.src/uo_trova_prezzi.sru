﻿$PBExportHeader$uo_trova_prezzi.sru
$PBExportComments$Ricerca Prezzi
forward
global type uo_trova_prezzi from nonvisualobject
end type
end forward

global type uo_trova_prezzi from nonvisualobject
end type
global uo_trova_prezzi uo_trova_prezzi

forward prototypes
public function integer wf_cerca_ultimo_prezzo_acq_fatture (string fs_cod_fornitore, string fs_cod_prodotto, ref double fd_prezzo_acquisto, ref string fs_messaggio)
public function integer uof_ultimo_acquisto (string as_tipo, datetime adt_data_rif, integer ai_gg_prima, string as_cod_prodotto, string as_flag_agg_mov, ref decimal ad_prezzo, ref string as_errore)
public function integer uof_costo_standard (string as_cod_prodotto, ref decimal ad_costo_standard, ref string as_error)
public function integer uof_costo_medio (string as_cod_prodotto, ref decimal ad_costo_medio, ref string as_error)
public function integer uof_prezzo_prodotto (string as_cod_prodotto, datetime adt_data_riferimento, integer ai_giorni_prima, ref decimal ad_prezzo_unitario, ref string as_error)
public function integer uof_prezzo_prodotto (string as_cod_prodotto, datetime adt_data_riferimento, integer ai_giorni_prima, ref decimal ad_prezzo_unitario, ref string as_tipo, ref string as_error)
end prototypes

public function integer wf_cerca_ultimo_prezzo_acq_fatture (string fs_cod_fornitore, string fs_cod_prodotto, ref double fd_prezzo_acquisto, ref string fs_messaggio);// ----------------------------------------------------------------------------------------------------------------
//
//	FUNZIONE DI RICERCA ULTIMO PREZZO DI ACQUISTO DEL PRODOTTO
//
// VARIABILE				TIPO				TIPO PASS.				COMMENTO
//	fs_cod_fornitore  	STRING			VAR						codice del fornitore
//	fs_cod_prodotto		STRING			VAR						codice del prodotto
// fd_prezzo_acquisto	DOUBLE			REF						ultimo prezzo di acquisto
//	fs_messaggio			STRING			REF						se return -1 : messaggio di errore	
//																				se return  1 : messaggio di errore	
//																				se return  0 : commento del tipo Prodotto-Descrizione-Data-Prezzo
//
//
string ls_sql  
long ll_anno_registrazione, ll_num_registrazione, ll_anno_documento, ll_num_documento, ld_quantita
datetime ldt_data_fattura, ldt_data_ricerca
double ld_prezzo_acquisto, ld_val_sconto_1, ld_val_sconto_2, ld_val_sconto_3, ld_val_sconto_4, ld_val_sconto_5, &
       ld_val_sconto_6, ld_val_sconto_7, ld_val_sconto_8, ld_val_sconto_9, ld_val_sconto_10, &
		 ld_val_riga_sconto_1, ld_val_riga_sconto_2, ld_val_riga_sconto_3, ld_val_riga_sconto_4, ld_val_riga_sconto_5, &
		 ld_val_riga_sconto_6, ld_val_riga_sconto_7, ld_val_riga_sconto_8, ld_val_riga_sconto_9, ld_val_riga_sconto_10, &
		 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, &
		 ld_sconto_9, ld_sconto_10

ldt_data_ricerca = datetime(relativedate(today(), -120) , 00:00:00)
  
ls_sql = "SELECT tes_fat_acq.anno_registrazione,  " + &
         "tes_fat_acq.num_registrazione,     " + &
         "det_fat_acq.prezzo_acquisto,     " + &			
         "det_fat_acq.sconto_1,     " + &
         "det_fat_acq.sconto_2,     " + &
         "det_fat_acq.sconto_3,     " + &
         "det_fat_acq.sconto_4,     " + &
         "det_fat_acq.sconto_5,     " + &
         "det_fat_acq.sconto_6,     " + &
         "det_fat_acq.sconto_7,     " + &
         "det_fat_acq.sconto_8,     " + &
         "det_fat_acq.sconto_9,     " + &
         "det_fat_acq.sconto_10,    " + &
         "det_fat_acq.quan_fatturata     " + &
         "FROM det_fat_acq, tes_fat_acq    " + &
			"WHERE ( tes_fat_acq.cod_azienda = det_fat_acq.cod_azienda ) and    " + &
			"( tes_fat_acq.anno_registrazione = det_fat_acq.anno_registrazione ) and    " + &
			"( tes_fat_acq.num_registrazione = det_fat_acq.num_registrazione ) and    " + &
			"( ( det_fat_acq.cod_azienda = '" + s_cs_xx.cod_azienda  + "' ) AND    " + &
			"( tes_fat_acq.cod_cliente = '" + fs_cod_fornitore + "' ) AND    " + &
			"( det_fat_acq.cod_prodotto = '" + fs_cod_prodotto  + "' ))  " + &
		   "ORDER BY tes_fat_acq.anno_registrazione ASC, tes_fat_acq.num_registrazione ASC   "
  
  

declare cu_ultimo_prezzo dynamic cursor for sqlsa;
prepare sqlsa from :ls_sql;

open dynamic cu_ultimo_prezzo;

do while 1=1
   fetch cu_ultimo_prezzo into :ll_anno_registrazione, :ll_num_registrazione, :ld_prezzo_acquisto, :ld_sconto_1, :ld_sconto_2, :ld_sconto_3, :ld_sconto_4, :ld_sconto_5, :ld_sconto_6, :ld_sconto_7, :ld_sconto_8, :ld_sconto_9, :ld_sconto_10, :ld_quantita;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) then
		fs_messaggio = "Nessun dato trovato"
		close cu_ultimo_prezzo;		
		return 1
		exit
	end if
	exit
loop

ld_val_sconto_1 = (ld_prezzo_acquisto * ld_sconto_1) / 100
ld_val_riga_sconto_1 = ld_prezzo_acquisto - ld_val_sconto_1
ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10

fd_prezzo_acquisto = ld_val_riga_sconto_10
fd_prezzo_acquisto = round(fd_prezzo_acquisto, 0)
fs_messaggio = "ULTIMO PREZZO: " + string(fd_prezzo_acquisto, "###,###,###,###")
close cu_ultimo_prezzo;
return 0
end function

public function integer uof_ultimo_acquisto (string as_tipo, datetime adt_data_rif, integer ai_gg_prima, string as_cod_prodotto, string as_flag_agg_mov, ref decimal ad_prezzo, ref string as_errore);


/*
RESTITUISCE IL VALORE UNITARIO DEL MOVIMENTO DALL'ULTIMA FATTURA DI ACQUISTO DI UN DATO PRODOTTO DALLA DATA DI RIFERIMENTO PASSATA
L'ARGOMENTO ai_gg_prima CONSENTE DI ESTERNDERE L'INTERVALLO A RITROSO DI QUANTO SI VUOLE

as_tipo			F, B, T  (solo fatture acquisto, solo ddt acquisto, entrambi)
*/


datetime					ldt_data_inizio
datastore				lds_data
string						ls_sql
dec{4}					ld_prezzo
long						ll_ret


//----------------------------------------------------------------------------------------------------------------
//elaboro il valore unitario del movimento dall'ultima fattura/ddt di acquisto confermati  (esso sarà in [€/UMmag])
//prendo per semplicità gli utlimi 365 giorni
ldt_data_inizio = datetime(RelativeDate(date(adt_data_rif), -ai_gg_prima) , 00:00:00)

if as_tipo="F" or as_tipo="T" or as_tipo="" or isnull(as_tipo) then
	ls_sql		= "select mov_magazzino.val_movimento,"+&
								"tes_fat_acq.data_registrazione as data_doc_fiscale,det_fat_acq.num_registrazione as num_doc_fiscale,det_fat_acq.prog_riga_fat_acq as riga_doc_fiscale,'F' as tipo_doc_fiscale "+&
					"from tes_fat_acq join det_fat_acq on tes_fat_acq.anno_registrazione=det_fat_acq.anno_registrazione and tes_fat_acq.num_registrazione=det_fat_acq.num_registrazione "+&
					"join mov_magazzino on det_fat_acq.anno_registrazione_mov_mag=mov_magazzino.anno_registrazione and det_fat_acq.num_registrazione_mov_mag=mov_magazzino.num_registrazione "+&
					"where tes_fat_acq.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
								"tes_fat_acq.data_registrazione<='"+string(adt_data_rif, s_cs_xx.db_funzioni.formato_data)+"' and "+&
								"tes_fat_acq.data_registrazione>='"+string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data)+"' "
								
if as_flag_agg_mov="S" or as_flag_agg_mov="N" then ls_sql+= " and det_fat_acq.flag_agg_mov='"+as_flag_agg_mov+"' " 
								
ls_sql += " and det_fat_acq.cod_prodotto='"+as_cod_prodotto+"' "
end if

if as_tipo="B" or as_tipo="T" or as_tipo="" or isnull(as_tipo) then
	
	if ls_sql<>"" then
		//accodo nella sintassi una union
		ls_sql += " union "
	end if
	
	ls_sql += "select mov_magazzino.val_movimento,"+&
								"tes_bol_acq.data_registrazione as data_doc_fiscale, tes_bol_acq.num_bolla_acq as num_doc_fiscale, det_bol_acq.prog_riga_bolla_acq as riga_doc_fiscale,'B' as tipo_doc_fiscale "+&
					"from det_bol_acq "+&
					"join tes_bol_acq on tes_bol_acq.cod_azienda=det_bol_acq.cod_azienda and tes_bol_acq.anno_bolla_acq=det_bol_acq.anno_bolla_acq and tes_bol_acq.num_bolla_acq=det_bol_acq.num_bolla_acq "+&
					"join mov_magazzino on det_bol_acq.anno_registrazione_mov_mag=mov_magazzino.anno_registrazione and det_bol_acq.num_registrazione_mov_mag=mov_magazzino.num_registrazione "+&
					"where tes_bol_acq.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
								"tes_bol_acq.data_registrazione<='"+string(adt_data_rif, s_cs_xx.db_funzioni.formato_data)+"' and "+&
								"tes_bol_acq.data_registrazione>='"+string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data)+"' and "+&
								"det_bol_acq.cod_prodotto='"+as_cod_prodotto+"' "
	
end if

ls_sql += "order by  data_doc_fiscale DESC, num_doc_fiscale DESC, riga_doc_fiscale DESC "

ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql, as_errore)

if ll_ret < 0 then
	return -1
end if

if ll_ret > 0 then
	//lo leggo dalla prima riga del datastore, prima colonna c'è il valore unitario del movimento
	ld_prezzo = lds_data.getitemnumber (1, 1)
	if isnull(ld_prezzo) then ld_prezzo = 0
else
	ld_prezzo = 0
end if

ad_prezzo = ld_prezzo

destroy lds_data

return 0



end function

public function integer uof_costo_standard (string as_cod_prodotto, ref decimal ad_costo_standard, ref string as_error);/**
 * stefanop
 * 18/09/2014
 *
 * Recupero il costo standard dall'anagrafica prodotti
 *
 * Ritorno: -1 error, 0 non trovato, 1 trovato
 **/
 
 
select costo_standard
into :ad_costo_standard
from anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :as_cod_prodotto;
		 
if sqlca.sqlcode < 0 then
	as_error = "Errore durante la lettura del costo standard per il prodotto " + g_str.safe(as_cod_prodotto) + ".~r~n" + sqlca.sqlerrtext
	return -1
elseif isnull(ad_costo_standard) then
	ad_costo_standard = 0
	return 0
else
	return 1
end if
end function

public function integer uof_costo_medio (string as_cod_prodotto, ref decimal ad_costo_medio, ref string as_error);/**
 * stefanop
 * 18/09/2014
 *
 * Recupero il costo standard dall'anagrafica prodotti
 *
 * Ritorno: -1 error, 0 non trovato, 1 trovato
 **/

dec{4} ld_val_inizio_anno, ld_val_quan_acquistata, ld_saldo_quan_inizio_anno, ld_prog_quan_acquistata
 
select val_inizio_anno, val_quan_acquistata, saldo_quan_inizio_anno, prog_quan_acquistata
into :ld_val_inizio_anno, :ld_val_quan_acquistata, :ld_saldo_quan_inizio_anno, :ld_prog_quan_acquistata
from anag_prodotti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_prodotto = :as_cod_prodotto;
		 
if sqlca.sqlcode < 0 then
	as_error = "Errore durante la lettura del costo standard per il prodotto " + g_str.safe(as_cod_prodotto) + ".~r~n" + sqlca.sqlerrtext
	return -1
end if

if isnull(ld_prog_quan_acquistata) or ld_prog_quan_acquistata < 0 then
	ad_costo_medio = 0
	return 0
end if

if ld_prog_quan_acquistata > 0 then
	ad_costo_medio = round( (ld_val_inizio_anno + ld_val_quan_acquistata) / ld_prog_quan_acquistata, 4)
else
	ad_costo_medio = 0
end if

return 1
end function

public function integer uof_prezzo_prodotto (string as_cod_prodotto, datetime adt_data_riferimento, integer ai_giorni_prima, ref decimal ad_prezzo_unitario, ref string as_error);/**
 * stefanop
 * 18/09/2014
 *
 * Calcolo il prezzo del prodotto passato
 * 1. Cerco il prezzo ultimo acquisto
 * 2. Cerco il costo standard
 * 3. Cerco il costo medio
 *
 * Ritorna: -1 error, 0 tutto ok
 **/

string ls_tipo

return uof_prezzo_prodotto(as_cod_prodotto, adt_data_riferimento, ai_giorni_prima, ad_prezzo_unitario, ls_tipo, as_error)
end function

public function integer uof_prezzo_prodotto (string as_cod_prodotto, datetime adt_data_riferimento, integer ai_giorni_prima, ref decimal ad_prezzo_unitario, ref string as_tipo, ref string as_error);/**
 * stefanop
 * 18/09/2014
 *
 * Calcolo il prezzo del prodotto passato
 * 1. Cerco il prezzo ultimo acquisto
 * 2. Cerco il costo standard
 * 3. Cerco il costo medio
 *
 * Ritorna: -1 error, 0 tutto ok
 **/
 
 ad_prezzo_unitario = 0

as_tipo = "U"
if uof_ultimo_acquisto('T', adt_data_riferimento,  ai_giorni_prima, as_cod_prodotto, "T", ad_prezzo_unitario, as_error) < 0 then
	return -1
end if

if ad_prezzo_unitario = 0 then
	// Se non trovo l'ultimo costo, provo il costo standard
	as_tipo = "S"
	if uof_costo_standard(as_cod_prodotto, ad_prezzo_unitario, as_error) < 0 then
		return -1
	end if
	
	if ad_prezzo_unitario = 0 then
		// Se non trovo il costo standard, provo con il costo medio
		as_tipo = "M"
		if uof_costo_medio(as_cod_prodotto, ad_prezzo_unitario, as_error) < 0 then
			return -1
		end if
	end if
end if

return 0
end function

on uo_trova_prezzi.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_trova_prezzi.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

