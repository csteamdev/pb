﻿$PBExportHeader$w_converti_rich_text_offerte.srw
forward
global type w_converti_rich_text_offerte from window
end type
type dw_1 from datawindow within w_converti_rich_text_offerte
end type
type st_1 from statictext within w_converti_rich_text_offerte
end type
end forward

global type w_converti_rich_text_offerte from window
boolean visible = false
integer width = 2034
integer height = 1360
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
windowtype windowtype = response!
windowstate windowstate = minimized!
long backcolor = 67108864
string icon = "AppIcon!"
boolean center = true
dw_1 dw_1
st_1 st_1
end type
global w_converti_rich_text_offerte w_converti_rich_text_offerte

type variables
integer li_anno_offerta
long ll_num_reg_offerta
end variables

event open;s_cs_xx_parametri lstr_parametri
integer li_row
string ls_nota_testata, ls_nota_piede

this.hide( )

lstr_parametri = message.powerobjectparm

li_anno_offerta = lstr_parametri.parametro_d_1_a[1]
ll_num_reg_offerta = lstr_parametri.parametro_d_1_a[2]

dw_1.settransobject(sqlca)
li_row = dw_1.retrieve(s_cs_xx.cod_azienda, li_anno_offerta, ll_num_reg_offerta)

if li_row > 0 then
	
	dw_1.setcolumn("nota_testata")
	dw_1.SelectText(1, Len(dw_1.GetText()))
	dw_1.copy()
	ls_nota_testata = clipboard()
	//------
	clipboard("")
	//------
	
	dw_1.setcolumn("nota_piede")
	dw_1.SelectText(1, Len(dw_1.GetText()))
	dw_1.copy()
	ls_nota_piede = clipboard()
	
	
else
	ls_nota_testata = ""
	ls_nota_piede = ""
	
end if


lstr_parametri.parametro_s_1_a[1] = ls_nota_testata
lstr_parametri.parametro_s_1_a[2] = ls_nota_piede

closewithreturn(this, lstr_parametri)
end event

on w_converti_rich_text_offerte.create
this.dw_1=create dw_1
this.st_1=create st_1
this.Control[]={this.dw_1,&
this.st_1}
end on

on w_converti_rich_text_offerte.destroy
destroy(this.dw_1)
destroy(this.st_1)
end on

type dw_1 from datawindow within w_converti_rich_text_offerte
boolean visible = false
integer x = 96
integer y = 388
integer width = 1829
integer height = 776
integer taborder = 10
string title = "none"
string dataobject = "d_tes_off_ven_det_4"
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_converti_rich_text_offerte
integer x = 91
integer y = 40
integer width = 1760
integer height = 300
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "Finestra invisibile all~'utente, serve ad estrarre dalla testata offerta i campi note e piede che sono rich text, ripulirli della formattazione, e quindi ritornarli per essere passati alla fattura proforma"
boolean focusrectangle = false
end type

