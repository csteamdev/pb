﻿$PBExportHeader$w_storico_spese_trasporto.srw
forward
global type w_storico_spese_trasporto from w_cs_xx_risposta
end type
type st_3 from statictext within w_storico_spese_trasporto
end type
type cb_ok_solo_percentuali from commandbutton within w_storico_spese_trasporto
end type
type em_totale_2 from editmask within w_storico_spese_trasporto
end type
type st_2 from statictext within w_storico_spese_trasporto
end type
type cb_annulla_spese from commandbutton within w_storico_spese_trasporto
end type
type em_totale from editmask within w_storico_spese_trasporto
end type
type cb_ok from commandbutton within w_storico_spese_trasporto
end type
type st_storico from statictext within w_storico_spese_trasporto
end type
type dw_list from datawindow within w_storico_spese_trasporto
end type
type st_1 from statictext within w_storico_spese_trasporto
end type
end forward

global type w_storico_spese_trasporto from w_cs_xx_risposta
boolean visible = false
integer width = 2354
integer height = 1580
string title = "Spese Trasporto"
boolean controlmenu = false
st_3 st_3
cb_ok_solo_percentuali cb_ok_solo_percentuali
em_totale_2 em_totale_2
st_2 st_2
cb_annulla_spese cb_annulla_spese
em_totale em_totale
cb_ok cb_ok
st_storico st_storico
dw_list dw_list
st_1 st_1
end type
global w_storico_spese_trasporto w_storico_spese_trasporto

type variables


string			is_tipo_documento
end variables

forward prototypes
public subroutine wf_log (string as_testo)
end prototypes

public subroutine wf_log (string as_testo);


return
end subroutine

on w_storico_spese_trasporto.create
int iCurrent
call super::create
this.st_3=create st_3
this.cb_ok_solo_percentuali=create cb_ok_solo_percentuali
this.em_totale_2=create em_totale_2
this.st_2=create st_2
this.cb_annulla_spese=create cb_annulla_spese
this.em_totale=create em_totale
this.cb_ok=create cb_ok
this.st_storico=create st_storico
this.dw_list=create dw_list
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_3
this.Control[iCurrent+2]=this.cb_ok_solo_percentuali
this.Control[iCurrent+3]=this.em_totale_2
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.cb_annulla_spese
this.Control[iCurrent+6]=this.em_totale
this.Control[iCurrent+7]=this.cb_ok
this.Control[iCurrent+8]=this.st_storico
this.Control[iCurrent+9]=this.dw_list
this.Control[iCurrent+10]=this.st_1
end on

on w_storico_spese_trasporto.destroy
call super::destroy
destroy(this.st_3)
destroy(this.cb_ok_solo_percentuali)
destroy(this.em_totale_2)
destroy(this.st_2)
destroy(this.cb_annulla_spese)
destroy(this.em_totale)
destroy(this.cb_ok)
destroy(this.st_storico)
destroy(this.dw_list)
destroy(this.st_1)
end on

event pc_setwindow;call super::pc_setwindow;/**
 * La finestra deve essere aperta tramite
 * uo_spese_trasporto.uof_apri_storico
 **/
 
string						ls_cod_cliente, ls_error, ls_tipologia
integer					ai_giorni_riferimento
long						al_anno, al_numero
decimal{4}				ld_prezzo, ld_prezzo_solo_perc
date						ldt_data_riferimento
s_cs_xx_parametri		lstr_parametri

uo_spese_trasporto luo_spese_trasporto
luo_spese_trasporto = create uo_spese_trasporto

lstr_parametri = message.powerobjectparm

ls_cod_cliente = lstr_parametri.parametro_s_1
al_anno = lstr_parametri.parametro_ul_1
al_numero  = lstr_parametri.parametro_ul_2

//B ddt altrimenti è fattura
ls_tipologia = lstr_parametri.parametro_s_2

if ls_tipologia="B" then
	dw_list.dataobject = "d_storico_spese_trasporto_ddt"
	is_tipo_documento = "BOLVEN"
else
	dw_list.dataobject = "d_storico_spese_trasporto"
	is_tipo_documento = "FATVEN"
end if

luo_spese_trasporto.uof_storico_data_riferimento(ldt_data_riferimento, ai_giorni_riferimento)

dw_list.settransobject(sqlca)
dw_list.retrieve(s_cs_xx.cod_azienda, ls_cod_cliente, ldt_data_riferimento, al_anno, al_numero)

ld_prezzo = lstr_parametri.parametro_dec4_1
ld_prezzo_solo_perc = lstr_parametri.parametro_dec4_2

//if isnull(ld_prezzo) or ld_prezzo = 0 then
//	if luo_spese_trasporto.uof_totale_spese_trasporto(al_anno, al_numero, is_tipo_documento, ref ld_prezzo, ls_error) < 0 then
//		g_mb.error(ls_error)
//	end if
//end if

em_totale.text = string(ld_prezzo)
em_totale_2.text = string(ld_prezzo_solo_perc)
st_storico.text = "Storico ultimi " + string(ai_giorni_riferimento) + " giorni"


destroy luo_spese_trasporto

visible = true
pcca.mdi_frame.setmicrohelp("")



end event

type st_3 from statictext within w_storico_spese_trasporto
integer x = 27
integer y = 400
integer width = 1646
integer height = 88
integer textsize = -11
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Per non inserire le spese trasporto nel documento >>>"
boolean focusrectangle = false
end type

type cb_ok_solo_percentuali from commandbutton within w_storico_spese_trasporto
integer x = 1682
integer y = 232
integer width = 590
integer height = 120
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "CONFERMA"
end type

event clicked;
string						ls_testo
s_cs_xx_parametri		lstr_parametri



ls_testo = "Sei sicuro di voler procedere calcolando le spese trasporto utilizzando le sole percentuali su imponibile righe ordine "

if is_tipo_documento = "BOLVEN" then
	ls_testo += "del D.d.T. creato?"
else
	ls_testo += "della Fattura creata?"
end if

if g_mb.confirm(ls_testo, 1) then
	lstr_parametri.parametro_s_1 = "SOLO%"
	closewithreturn(parent, lstr_parametri)
end if



end event

type em_totale_2 from editmask within w_storico_spese_trasporto
integer x = 1010
integer y = 232
integer width = 649
integer height = 120
integer taborder = 10
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
string text = "none"
alignment alignment = right!
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type st_2 from statictext within w_storico_spese_trasporto
integer x = 27
integer y = 232
integer width = 955
integer height = 120
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Solo con percentuali:"
boolean focusrectangle = false
end type

type cb_annulla_spese from commandbutton within w_storico_spese_trasporto
integer x = 1682
integer y = 384
integer width = 590
integer height = 120
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "NO SPESE TRASP."
end type

event clicked;
string						ls_testo
s_cs_xx_parametri		lstr_parametri


ls_testo = "Sei sicuro di voler procedere annullando tutte le righe di spesa trasporto "

if is_tipo_documento = "BOLVEN" then
	ls_testo += "del D.d.T. creato?"
else
	ls_testo += "della Fattura creata?"
end if

if g_mb.confirm(ls_testo, 1) then
	lstr_parametri.parametro_s_1 = "ZEROSPESE"
	closewithreturn(parent, lstr_parametri)
end if



end event

type em_totale from editmask within w_storico_spese_trasporto
integer x = 1006
integer y = 80
integer width = 649
integer height = 120
integer taborder = 10
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
string text = "none"
alignment alignment = right!
boolean displayonly = true
borderstyle borderstyle = stylelowered!
end type

type cb_ok from commandbutton within w_storico_spese_trasporto
integer x = 1682
integer y = 80
integer width = 590
integer height = 120
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "CONFERMA"
end type

event clicked;s_cs_xx_parametri lstr_parametri

lstr_parametri.parametro_s_1 = "OK"

closewithreturn(parent, lstr_parametri)
end event

type st_storico from statictext within w_storico_spese_trasporto
integer x = 46
integer y = 548
integer width = 2240
integer height = 84
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Storico:"
boolean focusrectangle = false
end type

type dw_list from datawindow within w_storico_spese_trasporto
integer x = 46
integer y = 656
integer width = 2249
integer height = 796
integer taborder = 10
string title = "none"
string dataobject = "d_storico_spese_trasporto"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type st_1 from statictext within w_storico_spese_trasporto
integer x = 27
integer y = 80
integer width = 955
integer height = 120
integer textsize = -16
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "Spesa Trasporto:"
boolean focusrectangle = false
end type

