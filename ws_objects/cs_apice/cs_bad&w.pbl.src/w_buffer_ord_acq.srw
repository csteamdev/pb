﻿$PBExportHeader$w_buffer_ord_acq.srw
forward
global type w_buffer_ord_acq from w_cs_xx_principale
end type
type cb_azzera from commandbutton within w_buffer_ord_acq
end type
type cb_tutti from commandbutton within w_buffer_ord_acq
end type
type cb_conferma from commandbutton within w_buffer_ord_acq
end type
type hpb_1 from hprogressbar within w_buffer_ord_acq
end type
type dw_buffer_ord_acq from uo_cs_xx_dw within w_buffer_ord_acq
end type
end forward

global type w_buffer_ord_acq from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 4361
integer height = 1604
string title = "Rilevazione Dati Terminali"
boolean maxbox = false
cb_azzera cb_azzera
cb_tutti cb_tutti
cb_conferma cb_conferma
hpb_1 hpb_1
dw_buffer_ord_acq dw_buffer_ord_acq
end type
global w_buffer_ord_acq w_buffer_ord_acq

type variables
boolean ib_barcode = false


end variables

forward prototypes
public function integer wf_calcola_stato_ordine (long fl_anno_ordine, long fl_num_ordine)
public function integer wf_genera_bolle_acquisto ()
public function integer wf_elimina_buffer ()
end prototypes

public function integer wf_calcola_stato_ordine (long fl_anno_ordine, long fl_num_ordine);string ls_flag_stato_ordine
long   ll_cont=0, ll_evasi=0, ll_parziali=0, ll_aperti=0

select count(*)
into   :ll_aperti
from   det_ord_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_ordine and
		 num_registrazione = :fl_num_ordine and
		 cod_prodotto is not null and
		 quan_arrivata = 0 and
		 flag_saldo = 'N';
if sqlca.sqlcode = -1 then return -1
		 
select count(*)
into   :ll_parziali
from   det_ord_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_ordine and
		 num_registrazione = :fl_num_ordine and
		 quan_arrivata > 0 and
		 quan_arrivata < quan_ordinata and
		 flag_saldo = 'N';
if sqlca.sqlcode = -1 then return -1
		 
select count(*)
into   :ll_evasi
from   det_ord_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_ordine and
		 num_registrazione = :fl_num_ordine and
		 (quan_arrivata = quan_ordinata or flag_saldo = 'S');
if sqlca.sqlcode = -1 then return -1

if isnull(ll_evasi) then ll_evasi = 0
if isnull(ll_aperti) then ll_aperti = 0
if isnull(ll_parziali) then ll_parziali = 0

ll_cont = ll_evasi + ll_parziali + ll_aperti
if ll_cont = 0 then 
	ls_flag_stato_ordine = "A"
else
	if ll_cont = ll_evasi then
		ls_flag_stato_ordine = "E"
	elseif ll_cont = ll_aperti then
		ls_flag_stato_ordine = "A"
	else
		ls_flag_stato_ordine = "P"
	end if
end if

update tes_ord_acq
set    flag_evasione = :ls_flag_stato_ordine
where  cod_azienda  = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_ordine and
		 num_registrazione = :fl_num_ordine;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in aggiornamento STATO ORDINE di acquisto. " + sqlca.sqlerrtext)
	return -1
end if

return 0
end function

public function integer wf_genera_bolle_acquisto ();string     ls_cod_tipo_det_acq_mag, ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ls_cod_deposito[], ls_cod_tipo_bol_acq, ls_cod_ubicazione[], &
           ls_cod_lotto[], ls_flag_saldo, ls_flag_agg_costo_ultimo, ls_cod_fornitore[], ls_cod_valuta, ls_cod_pagamento, ls_cod_banca, &
			  ls_cod_banca_clien_for, ls_cod_tipo_ord_acq, ls_cod_des_fornitore, ls_num_ord_fornitore, ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, &
			  ls_cod_porto, ls_cod_resa, ls_key_sort_old, ls_sort, ls_key_sort, ls_cod_tipo_analisi_ord, ls_cod_operatore, ls_cod_fil_fornitore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
			  ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_tipo_listino_prodotto, ls_nota_testata, ls_nota_piede, ls_cod_tipo_analisi_bol, &
			  ls_cod_tipo_det_acq, ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_cod_prod_fornitore, ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_doc_suc_det, ls_flag_tipo_det_acq, &
			  ls_flag_sola_iva, ls_cod_iva_det_acq, ls_cod_cliente[], ls_referenza, ls_flag_evasione, ls_conferma
			  
datetime   ldt_oggi, ldt_data_ordine, ldt_data_ord_fornitore, ldt_data_consegna_ordine, ldt_data_stock[], ldt_data_carico

long       ll_anno_esercizio, ll_riga, ll_i, ll_anno_ordine, ll_num_ordine, ll_prog_riga_ordine_acq, ll_anno_bolla_acq, ll_num_bolla_acq, &
           ll_prog_riga_bol_acq, ll_num_righe, ll_anno_commessa, ll_num_commessa, ll_prog_stock[], ll_anno_reg_dest_stock, ll_num_reg_dest_stock, ll_righe_non_saldate, &
			  ll_anno_confermato, ll_num_confermato, ll_prog_riga_confermata
dec{4}     ld_fat_conversione, ld_quan_acquisto, ld_tot_val_evaso, ld_tot_val_ordine, ld_cambio_acq, ld_sconto_testata, ld_sconto_pagamento, &
           ld_quan_ordine, ld_prezzo_acquisto, ld_fat_conversione_acq, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_quan_arrivata, ld_val_riga, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
			  ld_quan_ordinata, ld_quan_ordinata_new, ld_residua, ld_qta_carico

datastore  lds_rag_documenti, lds_riga_ord_acq

// *** leggo il tipo di acquisto

select cod_tipo_bol_acq,   
       cod_tipo_det_acq_mag
into   :ls_cod_tipo_bol_acq,
       :ls_cod_tipo_det_acq_mag
from   con_bol_acq  
where  cod_azienda = :s_cs_xx.cod_azienda;

select cod_tipo_movimento
into   :ls_cod_tipo_movimento
from   tab_tipi_det_acq  
where  cod_azienda = :s_cs_xx.cod_azienda  and
       cod_tipo_det_acq = :ls_cod_tipo_det_acq_mag;
		 
select cod_tipo_mov_det,
       cod_deposito,
       cod_ubicazione,   
       cod_lotto,
		 data_stock,
		 prog_stock
into   :ls_cod_tipo_mov_det,
       :ls_cod_deposito[1],
		 :ls_cod_ubicazione[1],
		 :ls_cod_lotto[1],
		 :ldt_data_stock[1],
		 :ll_prog_stock[1]
from   det_tipi_movimenti  
where  cod_azienda = :s_cs_xx.cod_azienda AND  
       cod_tipo_movimento = :ls_cod_tipo_movimento;

ls_flag_saldo = "N"
ls_flag_agg_costo_ultimo = "N"		 

lds_rag_documenti = create datastore
lds_rag_documenti.dataobject = "d_rag_ord_acq"
lds_rag_documenti.settransobject(sqlca)

for ll_i = 1 to dw_buffer_ord_acq.rowcount()
	
	ll_anno_confermato = dw_buffer_ord_acq.getitemnumber( ll_i, "anno_registrazione")
	ll_num_confermato = dw_buffer_ord_acq.getitemnumber( ll_i, "num_registrazione")
	ll_prog_riga_confermata = dw_buffer_ord_acq.getitemnumber( ll_i, "prog_riga_ordine_acq")
	ld_qta_carico = dw_buffer_ord_acq.getitemnumber( ll_i, "quan_arrivata")
	ldt_data_carico = dw_buffer_ord_acq.getitemdatetime( ll_i, "data_registrazione")
	ls_conferma = dw_buffer_ord_acq.getitemstring( ll_i, "flag_blocco")	
	
	if isnull(ls_conferma) or ls_conferma = "N" or ls_conferma = "" then continue
	
	select fat_conversione
	into   :ld_fat_conversione
	from   det_ord_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_confermato and
			 num_registrazione = :ll_num_confermato and
			 prog_riga_ordine_acq = :ll_prog_riga_confermata;
			 
	ld_qta_carico = ld_qta_carico / ld_fat_conversione
	ld_qta_carico = round(ld_qta_carico, 4)
		
	select tes_ord_acq.cod_fornitore,   
			 tes_ord_acq.cod_valuta,   
			 tes_ord_acq.cod_pagamento,   
			 tes_ord_acq.cod_banca,   
			 tes_ord_acq.cod_banca_clien_for,
			 tes_ord_acq.cod_tipo_ord_acq,
			 tes_ord_acq.cod_des_fornitore,   
			 tes_ord_acq.num_ord_fornitore,   
			 tes_ord_acq.cod_imballo,   
			 tes_ord_acq.cod_vettore,   
			 tes_ord_acq.cod_inoltro,   
			 tes_ord_acq.cod_mezzo,
			 tes_ord_acq.cod_porto,   
			 tes_ord_acq.cod_resa
	 into  :ls_cod_fornitore[1],
			 :ls_cod_valuta,   
			 :ls_cod_pagamento,   
			 :ls_cod_banca,   
			 :ls_cod_banca_clien_for,
			 :ls_cod_tipo_ord_acq,
			 :ls_cod_des_fornitore,   
			 :ls_num_ord_fornitore,   
			 :ls_cod_imballo,   
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_cod_porto,
			 :ls_cod_resa
	from   tes_ord_acq  
	where  tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_acq.anno_registrazione = :ll_anno_confermato and  
			 tes_ord_acq.num_registrazione = :ll_num_confermato;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "APICE", "Errore durante la lettura Testata Ordine Acquisto.~n~r" + sqlca.sqlerrtext, stopsign!)
		return -1
	end if
	
	ll_riga = lds_rag_documenti.insertrow(0)
	lds_rag_documenti.setrow(ll_riga)
	lds_rag_documenti.setitem(ll_riga, "anno_documento", ll_anno_confermato)
	lds_rag_documenti.setitem(ll_riga, "num_documento", ll_num_confermato)
	lds_rag_documenti.setitem(ll_riga, "prog_riga_documento", ll_prog_riga_confermata)
	lds_rag_documenti.setitem(ll_riga, "cod_fornitore", ls_cod_fornitore[1])
	lds_rag_documenti.setitem(ll_riga, "cod_valuta", ls_cod_valuta)
	lds_rag_documenti.setitem(ll_riga, "cod_pagamento", ls_cod_pagamento)
	lds_rag_documenti.setitem(ll_riga, "cod_banca", ls_cod_banca)
	lds_rag_documenti.setitem(ll_riga, "cod_banca_clien_for", ls_cod_banca_clien_for)
	lds_rag_documenti.setitem(ll_riga, "cod_tipo_documento", ls_cod_tipo_ord_acq	)
	lds_rag_documenti.setitem(ll_riga, "cod_imballo", ls_cod_imballo)
	lds_rag_documenti.setitem(ll_riga, "cod_vettore", ls_cod_vettore)
	lds_rag_documenti.setitem(ll_riga, "cod_inoltro", ls_cod_inoltro)
	lds_rag_documenti.setitem(ll_riga, "cod_mezzo", ls_cod_mezzo)
	lds_rag_documenti.setitem(ll_riga, "cod_porto", ls_cod_porto)
	lds_rag_documenti.setitem(ll_riga, "cod_resa", ls_cod_resa)
	lds_rag_documenti.setitem(ll_riga, "flag_saldo", 'N')
	lds_rag_documenti.setitem(ll_riga, "quan_acquisto", ld_qta_carico)
	
next	

ls_sort = "cod_fornitore A, cod_valuta A, cod_pagamento A, cod_banca A, cod_banca_clien_for A"
ls_sort += " , anno_documento A, num_documento A, prog_riga_documento A"
	
lds_rag_documenti.setsort(ls_sort)
lds_rag_documenti.sort()
	
ls_key_sort_old = ""
	
for ll_i = 1 to lds_rag_documenti.rowcount()
		
	ll_anno_ordine = lds_rag_documenti.getitemnumber(ll_i, "anno_documento")
	ll_num_ordine = lds_rag_documenti.getitemnumber(ll_i, "num_documento")
	ll_prog_riga_ordine_acq = lds_rag_documenti.getitemnumber(ll_i, "prog_riga_documento")
	ls_flag_saldo = lds_rag_documenti.getitemstring(ll_i, "flag_saldo")
	ld_quan_acquisto = lds_rag_documenti.getitemnumber(ll_i, "quan_acquisto")

	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_fornitore")) then
		ls_key_sort = lds_rag_documenti.getitemstring(ll_i, "cod_fornitore")
	else
		ls_key_sort = "      "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_valuta")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_valuta")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	ls_cod_tipo_ord_acq = lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")

	select tab_tipi_ord_acq.cod_tipo_analisi
	into   :ls_cod_tipo_analisi_ord
	from   tab_tipi_ord_acq  
	where  tab_tipi_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_ord_acq.cod_tipo_ord_acq = :ls_cod_tipo_ord_acq;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "APICE", "Errore durante la lettura della tabella Tipi Ordini Acquisto.~n~r" + sqlca.sqlerrtext, stopsign!)
		return -1
	end if
	
	select tes_ord_acq.cod_operatore,   
			 tes_ord_acq.data_registrazione,   
			 tes_ord_acq.cod_fornitore,   
			 tes_ord_acq.cod_des_fornitore,   
			 tes_ord_acq.cod_fil_fornitore,   			 
			 tes_ord_acq.rag_soc_1,   
			 tes_ord_acq.rag_soc_2,   
			 tes_ord_acq.indirizzo,   
			 tes_ord_acq.localita,   
			 tes_ord_acq.frazione,   
			 tes_ord_acq.cap,   
			 tes_ord_acq.provincia,   
			 tes_ord_acq.cod_valuta,   
			 tes_ord_acq.cambio_acq,   
			 tes_ord_acq.cod_tipo_listino_prodotto,   
			 tes_ord_acq.cod_pagamento,   
			 tes_ord_acq.sconto,   
			 tes_ord_acq.cod_banca,   
			 tes_ord_acq.num_ord_fornitore,   
			 tes_ord_acq.data_ord_fornitore,   
			 tes_ord_acq.cod_imballo,   
			 tes_ord_acq.cod_vettore,   
			 tes_ord_acq.cod_inoltro,   
			 tes_ord_acq.cod_mezzo,
			 tes_ord_acq.cod_porto,   
			 tes_ord_acq.cod_resa,   
			 tes_ord_acq.nota_testata,   
			 tes_ord_acq.nota_piede,   
			 tes_ord_acq.tot_val_evaso,
			 tes_ord_acq.tot_val_ordine,
			 tes_ord_acq.cod_banca_clien_for
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_fornitore[1],   
			 :ls_cod_des_fornitore,   
			 :ls_cod_fil_fornitore,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_valuta,   
			 :ld_cambio_acq,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_banca,   
			 :ls_num_ord_fornitore,   
			 :ldt_data_ord_fornitore,   
			 :ls_cod_imballo,   
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ld_tot_val_evaso,
			 :ld_tot_val_ordine,
			 :ls_cod_banca_clien_for
	from   tes_ord_acq  
	where  tes_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_ord_acq.anno_registrazione = :ll_anno_ordine and  
			 tes_ord_acq.num_registrazione = :ll_num_ordine;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "Apice", "Errore durante la lettura Testata Ordine Vendita.~n~r" + sqlca.sqlerrtext)
		destroy lds_rag_documenti
		return -1
	end if

	select tab_tipi_bol_acq.cod_tipo_analisi
	into	 :ls_cod_tipo_analisi_bol
	from   tab_tipi_bol_acq  
	where  tab_tipi_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_bol_acq.cod_tipo_bol_acq = :ls_cod_tipo_bol_acq;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "Apice", "Si è verificato un errore nella lettura della Tabella Tipi Bolle.~n~r" + sqlca.sqlerrtext)
		destroy lds_rag_documenti
		return -1
	end if

	// *****************************************************************************************
	// *****************************************************************************************
	// se questi due parametri sono diversi, allora vuol dire che devo creare una testata nuova.
	// *****************************************************************************************
	// *****************************************************************************************
	
	if ls_key_sort <> ls_key_sort_old then
		
		select tab_pagamenti.sconto
		into   :ld_sconto_pagamento
		from   tab_pagamenti
		where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;
	
		if sqlca.sqlcode <> 0 then
			ld_sconto_pagamento = 0
		end if
		
		ll_anno_bolla_acq = f_anno_esercizio()
		ll_num_bolla_acq = 0
		
		select max(num_bolla_acq) + 1
		into   :ll_num_bolla_acq
		from   tes_bol_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_bolla_acq = :ll_anno_bolla_acq;			
				 
		if isnull(ll_num_bolla_acq)  or ll_num_bolla_acq = 0 then 
				ll_num_bolla_acq = 1
		end if
		
		insert into tes_bol_acq	
						(cod_azienda,
						anno_bolla_acq,   
						num_bolla_acq,   
						cod_fornitore,   
						cod_cliente,   
						cod_tipo_bol_acq,   
						data_registrazione,   
						cod_operatore,   
						cod_fil_fornitore,   
						cod_deposito,   
						cod_ubicazione,   
						cod_valuta,   
						cambio_acq,   
						cod_tipo_listino_prodotto,   
						cod_pagamento,   
						cod_banca_clien_for,   
						totale_val_bolla,   
						flag_movimenti,   
						flag_riorganizzabile,   
						cod_banca,   
						data_bolla,   
						sconto,   
						flag_blocco,
						flag_gen_fat,
						num_bolla_fornitore)
			values	(:s_cs_xx.cod_azienda,   
						:ll_anno_bolla_acq,   
						:ll_num_bolla_acq,   
						:ls_cod_fornitore[1],   
						null,   
						:ls_cod_tipo_bol_acq,
						:ldt_data_carico,   
						:ls_cod_operatore,   
						:ls_cod_fil_fornitore,   
						:ls_cod_deposito[1],   
						:ls_cod_ubicazione[1],   
						:ls_cod_valuta,   
						:ld_cambio_acq,   
						:ls_cod_tipo_listino_prodotto,   
						:ls_cod_pagamento,   
						:ls_cod_banca_clien_for,   
						null,   
						'N',
						'N',   
						:ls_cod_banca,   
						:ldt_data_carico,   
						:ld_sconto_testata,   
						'N',
						'N',
						null);
						
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox( "APICE", "Errore durante la scrittura Testata Bolle Acquisto.~n~r" + sqlca.sqlerrtext)
			destroy lds_rag_documenti
			return -1
		end if

		ll_prog_riga_bol_acq = 0
		g_mb.messagebox( "APICE", " Bolla Generata: " + string(ll_anno_bolla_acq) + "/" + string(ll_num_bolla_acq))
	
	end if

	// *****************************************************************************************
	// *****************************************************************************************
	// *****************************************************************************************	

	ll_prog_riga_bol_acq = ll_prog_riga_bol_acq + 10
	
	lds_riga_ord_acq = create datastore
	lds_riga_ord_acq.dataobject = "d_ds_riga_ord_acq"
	lds_riga_ord_acq.settransobject(sqlca)
	ll_num_righe = lds_riga_ord_acq.retrieve(s_cs_xx.cod_azienda, ll_anno_ordine,ll_num_ordine,ll_prog_riga_ordine_acq)
	if ll_num_righe < 1 then
		g_mb.messagebox( "APICE", "Errore durante la lettura riga Ordine Acquisto.~n~r" + sqlca.sqlerrtext)
		destroy lds_rag_documenti
		return -1
	end if
		
	ls_cod_tipo_det_acq = lds_riga_ord_acq.getitemstring(1,"cod_tipo_det_acq")
	ls_cod_prodotto = lds_riga_ord_acq.getitemstring(1,"cod_prodotto") 
	ls_cod_misura = lds_riga_ord_acq.getitemstring(1,"cod_misura") 
	ls_des_prodotto = lds_riga_ord_acq.getitemstring(1,"des_prodotto")  
	ld_quan_ordine = lds_riga_ord_acq.getitemnumber(1,"quan_ordinata") 
	ld_prezzo_acquisto = lds_riga_ord_acq.getitemnumber(1,"prezzo_acquisto") 
	ld_fat_conversione_acq = lds_riga_ord_acq.getitemnumber(1,"fat_conversione") 
	ld_sconto_1 = lds_riga_ord_acq.getitemnumber(1,"sconto_1") 
	ld_sconto_2 = lds_riga_ord_acq.getitemnumber(1,"sconto_2") 
	ld_sconto_3 = lds_riga_ord_acq.getitemnumber(1,"sconto_3") 
	ls_cod_iva = lds_riga_ord_acq.getitemstring(1,"cod_iva")  
	ldt_data_consegna_ordine = lds_riga_ord_acq.getitemdatetime(1,"data_consegna")  
	ls_cod_prod_fornitore = lds_riga_ord_acq.getitemstring(1,"cod_prod_fornitore")  
	ld_quan_arrivata = lds_riga_ord_acq.getitemnumber(1,"quan_arrivata")  
	ld_val_riga = lds_riga_ord_acq.getitemnumber(1,"val_riga")  
	ls_nota_dettaglio = lds_riga_ord_acq.getitemstring(1,"nota_dettaglio")   
	ll_anno_commessa = lds_riga_ord_acq.getitemnumber(1,"anno_commessa")  
	ll_num_commessa = lds_riga_ord_acq.getitemnumber(1,"num_commessa")  
	ls_cod_centro_costo = lds_riga_ord_acq.getitemstring(1,"cod_centro_costo")   
	ld_sconto_4 = lds_riga_ord_acq.getitemnumber(1,"sconto_4")  
	ld_sconto_5 = lds_riga_ord_acq.getitemnumber(1,"sconto_5")  
	ld_sconto_6 = lds_riga_ord_acq.getitemnumber(1,"sconto_6")  
	ld_sconto_7 = lds_riga_ord_acq.getitemnumber(1,"sconto_7")  
	ld_sconto_8 = lds_riga_ord_acq.getitemnumber(1,"sconto_8")  
	ld_sconto_9 = lds_riga_ord_acq.getitemnumber(1,"sconto_9")  
	ld_sconto_10 = lds_riga_ord_acq.getitemnumber(1,"sconto_10") 
	ls_flag_doc_suc_det = lds_riga_ord_acq.getitemstring(1,"flag_doc_suc_det")   
	
	destroy lds_riga_ord_acq

	select tab_tipi_det_acq.flag_tipo_det_acq,
			 tab_tipi_det_acq.cod_tipo_movimento,
			 tab_tipi_det_acq.flag_sola_iva,
			 tab_tipi_det_acq.cod_iva
	into   :ls_flag_tipo_det_acq,
			 :ls_cod_tipo_movimento,
			 :ls_flag_sola_iva,
			 :ls_cod_iva_det_acq
	from   tab_tipi_det_acq
	where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox( "APICE", "Si è verificato un errore in fase di lettura tipi dettagli.~n~r" + sqlca.sqlerrtext)
		destroy lds_rag_documenti
		return -1
	end if
	if isnull(ls_cod_iva) or ls_cod_iva = "" then ls_cod_iva = ls_cod_iva_det_acq
		
	if ls_flag_tipo_det_acq = "M" then
		if sqlca.sqlcode = -1 then
			g_mb.messagebox( "APICE", "Si è verificato un errore in fase di lettura tipi dettagli.~n~r" + sqlca.sqlerrtext)
			destroy lds_rag_documenti
			return -1
		end if

		setnull(ldt_data_stock[1])
		setnull(ll_prog_stock[1])
		setnull(ls_cod_cliente[1])
		setnull(ls_cod_fornitore[1])
		
		setnull(ls_referenza)
		
		if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
												ls_cod_prodotto, &
												ls_cod_deposito[], &
												ls_cod_ubicazione[], &
												ls_cod_lotto[], &
												ldt_data_stock[], &
												ll_prog_stock[], &
												ls_cod_cliente[], &
												ls_cod_fornitore[], &
												ll_anno_reg_dest_stock, &
												ll_num_reg_dest_stock ) = -1 then
			ROLLBACK;
			g_mb.messagebox( "APICE",  "Si è verificato un errore in fase di Creazione Destinazioni Magazzino.~n~r" + sqlca.sqlerrtext)
			destroy lds_rag_documenti
			return -1
		end if
		
		if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
										 ll_num_reg_dest_stock, &
										 ls_cod_tipo_movimento, &
										 ls_cod_prodotto) = -1 then
			ROLLBACK;
			g_mb.messagebox( "APICE", "Si è verificato un errore in fase di Creazione di Magazzino.~n~r" + sqlca.sqlerrtext)
			destroy lds_rag_documenti
			return -1
		end if
	end if
	
	// fare update anag_prodotti	
	if ls_flag_tipo_det_acq = "M" then
		
		select quan_ordinata
		into   :ld_quan_ordinata
		from   anag_prodotti
		where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
				 
		ld_quan_ordinata_new = ld_quan_ordinata
		
		ld_residua = (ld_quan_ordine - ld_quan_arrivata)
		if ld_quan_acquisto >= ld_residua then // evasione totale (arriva di più del residuo)
			ls_flag_saldo = "S"
			ld_quan_ordinata_new = ld_quan_ordinata - ld_residua
		else
			if ls_flag_saldo = "S" then 	// evasione forzata (arriva meno del residuo e saldo riga)
				ld_quan_ordinata_new = ld_quan_ordinata - (ld_residua)
			else
				ld_quan_ordinata_new = ld_quan_ordinata - ld_quan_acquisto
			end if
		end if
		
		ld_quan_ordinata_new = round(ld_quan_ordinata_new, 4)
		update anag_prodotti  
			set quan_ordinata = :ld_quan_ordinata_new 
		 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
		if sqlca.sqlcode = -1 then
			g_mb.messagebox( "APICE", "Si è verificato un errore in fase di aggiornamento magazzino.~n~r" + sqlca.sqlerrtext)
			destroy lds_rag_documenti
			rollback;
			return -1
		end if
	end if
		
	ld_quan_acquisto = round(ld_quan_acquisto,4)
	
	update det_ord_acq
		set quan_arrivata = quan_arrivata + :ld_quan_acquisto,
			 flag_saldo = :ls_flag_saldo
	 where det_ord_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 det_ord_acq.anno_registrazione = :ll_anno_ordine and  
			 det_ord_acq.num_registrazione = :ll_num_ordine and
			 det_ord_acq.prog_riga_ordine_acq = :ll_prog_riga_ordine_acq;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox( "APICE", "Si è verificato un errore in fase di aggiornamento Dettaglio Ordine Acquisto.~n~r" + sqlca.sqlerrtext)
		destroy lds_rag_documenti
		return -1
	end if
		
	select count(*)
	into   :ll_righe_non_saldate
	from   det_ord_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and	 
			 anno_registrazione = :ll_anno_ordine and	 
			 num_registrazione = :ll_num_ordine and    
			 flag_saldo='N';

	if sqlca.sqlcode = -1 then
		g_mb.messagebox( "APICE", "Si è verificato un errore in fase di aggiornamento Testata Ordine Acquisto.~n~r" + sqlca.sqlerrtext)
		destroy lds_rag_documenti
		return -1
	end if
	
	if ll_righe_non_saldate >0 then
		ls_flag_evasione = "P"
	else
		ls_flag_evasione = "E"
	end if
	
//------------------------------------------------- Modifica Nicola ---------------------------------------------
	if ls_flag_doc_suc_det = 'I' then //nota dettaglio
		select flag_doc_suc_bl
		  into :ls_flag_doc_suc_det
		  from tab_tipi_det_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_tipo_det_acq = :ls_cod_tipo_det_acq;
	end if		
		
		
	if ls_flag_doc_suc_det = 'N' then
		setnull(ls_nota_dettaglio)
	end if				
//-------------------------------------------------- Fine Modifica ----------------------------------------------		

	insert into det_bol_acq
					(cod_azienda,   
					 anno_bolla_acq,   
					 num_bolla_acq,    
					 prog_riga_bolla_acq,   
					 cod_tipo_det_acq,   
					 cod_misura,   
					 cod_prodotto,   
					 des_prodotto,   
					 quan_arrivata,   
					 prezzo_acquisto,   
					 fat_conversione,   
					 sconto_1,   
					 sconto_2,   
					 sconto_3,   
					 cod_iva,   
					 quan_fatturata,
					 val_riga,				 
					 cod_deposito,
					 cod_ubicazione,
					 cod_lotto,
					 data_stock,
					 cod_tipo_movimento,
					 num_registrazione_mov_mag,
					 flag_saldo,
					 flag_accettazione,
					 flag_blocco,
					 nota_dettaglio,   
					 anno_registrazione,   
					 num_registrazione,   
					 prog_riga_ordine_acq,
					 anno_commessa,
					 num_commessa,
					 cod_centro_costo,
					 sconto_4,   
					 sconto_5,   
					 sconto_6,   
					 sconto_7,   
					 sconto_8,   
					 sconto_9,   
					 sconto_10,
					 anno_registrazione_mov_mag,
					 prog_stock,
					 anno_reg_des_mov,
					 num_reg_des_mov,
					 flag_doc_suc_det,
					 flag_st_note_det,
					 anno_reg_bol_ven,
					 num_reg_bol_ven,
					 prog_riga_bol_ven)						 
  values			(:s_cs_xx.cod_azienda,   
					 :ll_anno_bolla_acq,   
					 :ll_num_bolla_acq,     
					 :ll_prog_riga_bol_acq,   
					 :ls_cod_tipo_det_acq,   
					 :ls_cod_misura,   
					 :ls_cod_prodotto,   
					 :ls_des_prodotto,   
					 :ld_quan_acquisto,   
					 :ld_prezzo_acquisto,   
					 :ld_fat_conversione_acq,   
					 :ld_sconto_1,   
					 :ld_sconto_2,  
					 :ld_sconto_3,  
					 :ls_cod_iva,   
					 null,
					 0,
					 :ls_cod_deposito[1],
					 :ls_cod_ubicazione[1],
					 :ls_cod_lotto[1],
					 :ldt_data_stock[1],
					 :ls_cod_tipo_movimento,
					 null,
					 :ls_flag_saldo,
					 'N',
					 'N',
					 :ls_nota_dettaglio,   
					 :ll_anno_ordine,   
					 :ll_num_ordine,   
					 :ll_prog_riga_ordine_acq,
					 :ll_anno_commessa,
					 :ll_num_commessa,   
					 :ls_cod_centro_costo,   
					 :ld_sconto_4,   
					 :ld_sconto_5,   
					 :ld_sconto_6,   
					 :ld_sconto_7,   
					 :ld_sconto_8,   
					 :ld_sconto_9,   
					 :ld_sconto_10,
					 null,
					 :ll_prog_stock[1],
					 :ll_anno_reg_dest_stock,
					 :ll_num_reg_dest_stock,
					 'I',
					 'I',
					 null,
					 null,
					 null);						 

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "APICE", "Errore durante la scrittura Dettagli Bolle Acquisto.~n~r" + sqlca.sqlerrtext)
		destroy lds_rag_documenti
		return -1
	end if
	
	wf_calcola_stato_ordine(ll_anno_ordine, ll_num_ordine)
	ls_key_sort_old = ls_key_sort
next

//if ls_flag_conferma = "S" then
//	if f_conferma_bol_acq( ll_anno_esercizio, il_num_bolla_acq, ref fs_messaggio, ls_flag_acc_materiali ) = -1 then
//		destroy lds_rag_documenti
//		SetPointer(lp_old_pointer)
//		rollback;
//		return -1
//	else
//		commit;
//	end if
//end if

destroy lds_rag_documenti
return 0
end function

public function integer wf_elimina_buffer ();string ls_elimina
long   ll_i, ll_anno, ll_num, ll_prog_riga, ll_prog_riga_2

for ll_i = 1 to dw_buffer_ord_acq.rowcount()
	
	ls_elimina = dw_buffer_ord_acq.getitemstring( ll_i, "flag_blocco")
	if isnull(ls_elimina) or ls_elimina = "N" then continue
	
	ll_anno = dw_buffer_ord_acq.getitemnumber( ll_i, "anno_registrazione")
	ll_num = dw_buffer_ord_acq.getitemnumber( ll_i, "num_registrazione")
	ll_prog_riga = dw_buffer_ord_acq.getitemnumber( ll_i, "prog_riga_ordine_acq")
	ll_prog_riga_2 = dw_buffer_ord_acq.getitemnumber( ll_i, "prog_riga_evasione")
	
	delete from buffer_ord_acq
	where       cod_azienda = :s_cs_xx.cod_azienda and
	            anno_registrazione = :ll_anno and
					num_registrazione = :ll_num and
					prog_riga_ordine_acq = :ll_prog_riga and
					prog_riga_evasione = :ll_prog_riga_2;
					
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox( "APICE", "Errore durante la cancellazione dal buffer della riga: " + string(ll_anno) + "/" + string(ll_num) + "/" + string(ll_prog_riga) + "/" + string(ll_prog_riga_2))
		return -1
	end if
	
next

return 0
end function

on w_buffer_ord_acq.create
int iCurrent
call super::create
this.cb_azzera=create cb_azzera
this.cb_tutti=create cb_tutti
this.cb_conferma=create cb_conferma
this.hpb_1=create hpb_1
this.dw_buffer_ord_acq=create dw_buffer_ord_acq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_azzera
this.Control[iCurrent+2]=this.cb_tutti
this.Control[iCurrent+3]=this.cb_conferma
this.Control[iCurrent+4]=this.hpb_1
this.Control[iCurrent+5]=this.dw_buffer_ord_acq
end on

on w_buffer_ord_acq.destroy
call super::destroy
destroy(this.cb_azzera)
destroy(this.cb_tutti)
destroy(this.cb_conferma)
destroy(this.hpb_1)
destroy(this.dw_buffer_ord_acq)
end on

event pc_setwindow;call super::pc_setwindow;iuo_dw_main = dw_buffer_ord_acq

dw_buffer_ord_acq.change_dw_current()

dw_buffer_ord_acq.set_dw_options(sqlca, &
									  pcca.null_object, &
									  c_nonew + c_retrieveonopen, &
									  c_default)
									  
									  


end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_buffer_ord_acq,&
                 "cod_operaio", &
                 sqlca, &
					  "anag_operai",&
					  "cod_operaio",&
					  "cognome + ' ' + nome ", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


end event

event open;call super::open;triggerevent("pc_retrieve")
end event

event close;call super::close;dw_buffer_ord_acq.resetupdate()
end event

type cb_azzera from commandbutton within w_buffer_ord_acq
integer x = 320
integer y = 1380
integer width = 279
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Azzera"
end type

event clicked;long ll_i


for ll_i = 1 to dw_buffer_ord_acq.rowcount()
	dw_buffer_ord_acq.setitem(ll_i,"flag_blocco","N")
next

dw_buffer_ord_acq.triggerevent("pcd_save")
end event

type cb_tutti from commandbutton within w_buffer_ord_acq
integer x = 23
integer y = 1380
integer width = 279
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Tutti"
end type

event clicked;long ll_i


for ll_i = 1 to dw_buffer_ord_acq.rowcount()
	dw_buffer_ord_acq.setitem(ll_i,"flag_blocco","S")
next

dw_buffer_ord_acq.triggerevent("pcd_save")
end event

type cb_conferma from commandbutton within w_buffer_ord_acq
integer x = 3909
integer y = 1380
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Conferma"
end type

event clicked;long ll_return

ll_return = wf_genera_bolle_acquisto()
if ll_return = 0 then
	ll_return = wf_elimina_buffer()
	if ll_return = 0 then
		dw_buffer_ord_acq.resetupdate()
		commit;
		g_mb.messagebox( "APICE", "Procedura portata a termine con successo!")
		close(parent)
	else
		rollback;
	end if
else
	rollback;
end if
end event

type hpb_1 from hprogressbar within w_buffer_ord_acq
integer x = 777
integer y = 1380
integer width = 2697
integer height = 60
boolean bringtotop = true
unsignedinteger maxposition = 100
integer setstep = 10
end type

type dw_buffer_ord_acq from uo_cs_xx_dw within w_buffer_ord_acq
integer x = 23
integer y = 20
integer width = 4251
integer height = 1340
integer taborder = 20
string dataobject = "d_buffer_ord_acq"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve()
end event

event pcd_modify;call super::pcd_modify;cb_azzera.enabled = true
cb_tutti.enabled = true
cb_conferma.enabled = false

end event

event pcd_new;call super::pcd_new;cb_azzera.enabled = true
cb_tutti.enabled = true
cb_conferma.enabled = false

end event

event pcd_view;call super::pcd_view;cb_conferma.enabled = true
end event

event updateend;call super::updateend;//cb_azzera.enabled = false
//cb_tutti.enabled = false
//cb_operatori.enabled = true
//cb_risorse.enabled = true
//cb_conferma.enabled = true
end event

