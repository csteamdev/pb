﻿$PBExportHeader$w_carico_bolle_acq.srw
$PBExportComments$Finestra Carico Bolle Acquisto - Creazione Fatture Acquisto da Bolle Acquisto
forward
global type w_carico_bolle_acq from w_cs_xx_principale
end type
type cb_reset from commandbutton within w_carico_bolle_acq
end type
type cb_ricerca from commandbutton within w_carico_bolle_acq
end type
type cb_genera from commandbutton within w_carico_bolle_acq
end type
type dw_ext_carico_bolle from u_dw_search within w_carico_bolle_acq
end type
type cbx_quan_arrivata from checkbox within w_carico_bolle_acq
end type
type dw_lista_bolle_acquisto from uo_cs_xx_dw within w_carico_bolle_acq
end type
type dw_folder_search from u_folder within w_carico_bolle_acq
end type
type dw_ext_carico_bolle_acq_det from datawindow within w_carico_bolle_acq
end type
end forward

global type w_carico_bolle_acq from w_cs_xx_principale
integer width = 3520
integer height = 1972
string title = "Carico Acquisti"
cb_reset cb_reset
cb_ricerca cb_ricerca
cb_genera cb_genera
dw_ext_carico_bolle dw_ext_carico_bolle
cbx_quan_arrivata cbx_quan_arrivata
dw_lista_bolle_acquisto dw_lista_bolle_acquisto
dw_folder_search dw_folder_search
dw_ext_carico_bolle_acq_det dw_ext_carico_bolle_acq_det
end type
global w_carico_bolle_acq w_carico_bolle_acq

type variables
string is_cod_fornitore
string is_cod_doc_origine
string is_numeratore_doc_origine
integer ii_anno_doc_origine
long il_num_doc_origine
long il_prog_fattura
end variables

forward prototypes
public function integer wf_carica_dett_fatture (ref string fs_messaggio)
public function integer wf_genera_fatture_acq (ref string fs_messaggio)
end prototypes

public function integer wf_carica_dett_fatture (ref string fs_messaggio);string ls_cod_tipo_bol_acq, ls_flag_tipo_doc, ls_cod_fornitore[], &
		 ls_cod_tipo_analisi_bol, ls_cod_deposito, ls_lire, ls_cod_tipo_analisi_fat, &
		 ls_cod_tipo_det_acq, ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, &
		 ls_cod_iva, ls_cod_prod_fornitore, ls_cod_valuta, &
		 ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_acq, ls_cod_tipo_movimento, &
		 ls_flag_sola_iva, ls_flag_saldo, ls_cod_cliente[], &
		 ls_referenza, ls_flag_evasione, ls_num_bolla_acq, ls_cod_ubicazione, ls_cod_lotto
		 
double ld_cambio_acq, ld_sconto_testata, ld_tot_val_evaso, ld_tot_val_bolla, &
		 ld_sconto_pagamento, ld_quan_fatturata, ld_prezzo_acquisto, ld_fat_conversione_acq, &
		 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_quan_arrivata, ld_val_riga, ld_sconto_4, &
		 ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		 ld_quan_acquisto, ld_val_sconto_1, ld_val_riga_sconto_1, ld_val_sconto_2, &
		 ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, &
		 ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, &
		 ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, ld_val_sconto_8, &
		 ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, &
		 ld_val_riga_sconto_10, ld_val_sconto_testata, ld_val_riga_sconto_testata, &
		 ld_val_sconto_pagamento, ld_val_evaso

integer li_anno_bolla_acq, li_anno_registrazione_mov_mag

long ll_progressivo, ll_prog_riga_documento, ll_prog_riga_fat_acq ,&
	  ll_prog_riga_bolla_acq, ll_anno_commessa, ll_num_commessa, ll_num_registrazione_mov_mag, &
	  ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_i, ll_anno_reg_dest_stock, &
	  ll_num_reg_dest_stock, ll_anno_registrazione[], ll_num_registrazione[], ll_prog_stock

datetime ldt_data_stock

datastore lds_rag_documenti

pointer lp_old_pointer
lp_old_pointer = SetPointer(HourGlass!)

lds_rag_documenti = create datastore
lds_rag_documenti.dataobject = "d_rag_bol_acq"
lds_rag_documenti.settransobject(sqlca)

select cod_valuta
 into  :ls_cod_valuta
 from  tes_fat_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_doc_origine = :is_cod_doc_origine and
		 numeratore_doc_origine = :is_numeratore_doc_origine and
		 anno_doc_origine = :ii_anno_doc_origine and
		 num_doc_origine = :il_num_doc_origine and
		 progressivo = :il_prog_fattura;

for ll_i = 1 to dw_ext_carico_bolle_acq_det.rowcount()
	dw_ext_carico_bolle_acq_det.accepttext()
	li_anno_bolla_acq = dw_ext_carico_bolle_acq_det.getitemnumber(ll_i, "anno_bolla_acq")
	ls_num_bolla_acq = dw_ext_carico_bolle_acq_det.getitemstring(ll_i, "num_bolla_acq")
	ll_progressivo = dw_ext_carico_bolle_acq_det.getitemnumber(ll_i, "progressivo")
	ll_prog_riga_bolla_acq = dw_ext_carico_bolle_acq_det.getitemnumber(ll_i, "prog_riga_bolla_acq")
	ls_flag_saldo = dw_ext_carico_bolle_acq_det.getitemstring(ll_i, "flag_saldo")
	ld_quan_acquisto = dw_ext_carico_bolle_acq_det.getitemnumber(ll_i, "quan_acquisto")

	select tes_bol_acq.cod_fornitore,   
			 tes_bol_acq.cod_tipo_bol_acq,
			 tes_bol_acq.cod_deposito
	 into  :ls_cod_fornitore[1],
			 :ls_cod_tipo_bol_acq,
			 :ls_cod_deposito
	from   tes_bol_acq  
	where  tes_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_bol_acq.anno_bolla_acq = :li_anno_bolla_acq and  
			 tes_bol_acq.num_bolla_acq = :ls_num_bolla_acq and
			 tes_bol_acq.progressivo = :ll_progressivo;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la lettura Testata bolla Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	lds_rag_documenti.insertrow(0)
	lds_rag_documenti.setrow(lds_rag_documenti.rowcount())
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "anno_documento", li_anno_bolla_acq)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "num_documento", ls_num_bolla_acq)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "progressivo", ll_progressivo)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "prog_riga_documento", ll_prog_riga_bolla_acq)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_fornitore", ls_cod_fornitore[1])
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_tipo_documento", ls_cod_tipo_bol_acq	)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "flag_saldo", ls_flag_saldo)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "quan_acquisto", ld_quan_acquisto)
next

for ll_i = 1 to lds_rag_documenti.rowcount()
	select max(prog_riga_fat_acq)
		into :ll_prog_riga_fat_acq
		from det_fat_acq
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_doc_origine = :is_cod_doc_origine and
				numeratore_doc_origine = :is_numeratore_doc_origine and
				anno_doc_origine = :ii_anno_doc_origine and
				num_doc_origine = :il_num_doc_origine and
				progressivo = :il_prog_fattura;
	
	if sqlca.sqlcode = -1 then
		fs_messaggio =  "Errore durante la lettura Dettagli Fatture Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	if isnull(ll_prog_riga_fat_acq) then ll_prog_riga_fat_acq = 0
	ll_prog_riga_fat_acq = ll_prog_riga_fat_acq  + 10
			
	li_anno_bolla_acq = lds_rag_documenti.getitemnumber(ll_i, "anno_documento")
	ls_num_bolla_acq = lds_rag_documenti.getitemstring(ll_i, "num_documento")
	ll_progressivo = lds_rag_documenti.getitemnumber(ll_i, "progressivo")
	ll_prog_riga_bolla_acq = lds_rag_documenti.getitemnumber(ll_i, "prog_riga_documento")
	ls_flag_saldo = lds_rag_documenti.getitemstring(ll_i, "flag_saldo")

	ld_quan_acquisto = lds_rag_documenti.getitemnumber(ll_i, "quan_acquisto")
	ls_cod_tipo_bol_acq = lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")
	
	select	det_bol_acq.cod_tipo_det_acq,   
				det_bol_acq.cod_prodotto,   
				det_bol_acq.cod_misura,   
				det_bol_acq.des_prodotto,   
				det_bol_acq.quan_fatturata,
				det_bol_acq.prezzo_acquisto,   
				det_bol_acq.fat_conversione,   
				det_bol_acq.sconto_1,   
				det_bol_acq.sconto_2,  
				det_bol_acq.sconto_3,  
				det_bol_acq.cod_iva,   
				det_bol_acq.quan_arrivata,
				det_bol_acq.val_riga,				
				det_bol_acq.nota_dettaglio,   
				det_bol_acq.anno_commessa,
				det_bol_acq.num_commessa,
				det_bol_acq.cod_centro_costo,
				det_bol_acq.sconto_4,   
				det_bol_acq.sconto_5,   
				det_bol_acq.sconto_6,   
				det_bol_acq.sconto_7,   
				det_bol_acq.sconto_8,   
				det_bol_acq.sconto_9,   
				det_bol_acq.sconto_10,
				det_bol_acq.cod_ubicazione,   
				det_bol_acq.cod_lotto,   
				det_bol_acq.data_stock,   
				det_bol_acq.progr_stock,
				det_bol_acq.anno_registrazione_mov_mag,
				det_bol_acq.num_registrazione_mov_mag
	into		:ls_cod_tipo_det_acq, 
				:ls_cod_prodotto, 
				:ls_cod_misura, 
				:ls_des_prodotto, 
				:ld_quan_fatturata,
				:ld_prezzo_acquisto, 
				:ld_fat_conversione_acq, 
				:ld_sconto_1, 
				:ld_sconto_2, 
				:ld_sconto_3, 
				:ls_cod_iva, 
				:ld_quan_arrivata,
				:ld_val_riga,
				:ls_nota_dettaglio, 
				:ll_anno_commessa,
				:ll_num_commessa,
				:ls_cod_centro_costo,
				:ld_sconto_4, 
				:ld_sconto_5, 
				:ld_sconto_6, 
				:ld_sconto_7, 
				:ld_sconto_8, 
				:ld_sconto_9, 
				:ld_sconto_10,
				:ls_cod_ubicazione,
				:ls_cod_lotto,
				:ldt_data_stock,
				:ll_prog_stock,
				:li_anno_registrazione_mov_mag,
				:ll_num_registrazione_mov_mag
	from 		det_bol_acq  
	where 	det_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				det_bol_acq.anno_bolla_acq = :li_anno_bolla_acq and  
				det_bol_acq.num_bolla_acq = :ls_num_bolla_acq and
				det_bol_acq.progressivo = :ll_progressivo and
				det_bol_acq.prog_riga_bolla_acq = :ll_prog_riga_bolla_acq
	order by det_bol_acq.prog_riga_bolla_acq asc;

	if sqlca.sqlcode = -1 then
		fs_messaggio =  "Errore durante la lettura Dettagli bolle Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	select tab_tipi_det_acq.flag_tipo_det_acq,
			 tab_tipi_det_acq.cod_tipo_movimento,
			 tab_tipi_det_acq.flag_sola_iva
	into   :ls_flag_tipo_det_acq,
			 :ls_cod_tipo_movimento,
			 :ls_flag_sola_iva
	from   tab_tipi_det_acq
	where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	if ld_quan_acquisto >= (ld_quan_arrivata - ld_quan_fatturata) then ls_flag_saldo = "S"
	
	update det_bol_acq
	set    quan_fatturata = quan_fatturata + :ld_quan_acquisto,
			 flag_saldo    = :ls_flag_saldo
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_bolla_acq = :li_anno_bolla_acq and
			 num_bolla_acq = :ls_num_bolla_acq and
			 progressivo = :ll_progressivo and
			 prog_riga_bolla_acq = :ll_prog_riga_bolla_acq ;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento righe bolla"
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
		ROLLBACK;
	else
		COMMIT;
	end if

	if ls_flag_sola_iva = 'N' then
		ld_val_riga = ld_quan_acquisto * ld_prezzo_acquisto
		ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
		ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
		ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
		ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
		ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
		ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
		ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
		ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
		ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
		ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
		ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
		ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
		ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
		ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
		ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
		ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
		ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
		ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
		ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
		if ls_cod_valuta = ls_lire then
			ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
		else
			ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
		end if   
	
		if ls_flag_tipo_det_acq = "M" or &
			ls_flag_tipo_det_acq = "C" or &
			ls_flag_tipo_det_acq = "N" then
			ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
			ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
			ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
			ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
		elseif ls_flag_tipo_det_acq = "S" then
			ld_val_evaso = ld_val_riga_sconto_10 * -1
		else
			ld_val_evaso = ld_val_riga_sconto_10
		end if
		ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
	end if

	if ls_cod_valuta = ls_lire then
		ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
	end if   

	update tes_fat_acq
		set tot_val_fat_acq = tot_val_fat_acq + :ld_val_evaso
	 where tes_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_fat_acq.cod_doc_origine = :is_cod_doc_origine and
			 tes_fat_acq.numeratore_doc_origine = :is_numeratore_doc_origine and
			 tes_fat_acq.anno_doc_origine = :ii_anno_doc_origine and
			 tes_fat_acq.num_doc_origine = :il_num_doc_origine and
			 tes_fat_acq.progressivo = :il_prog_fattura;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento Testata Fatture Acquisto."			
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	insert into det_fat_acq
	   (cod_azienda,   
		cod_doc_origine,   
		numeratore_doc_origine,   
		anno_doc_origine,   
		num_doc_origine,   
		progressivo,   
		prog_riga_fat_acq,   
		cod_tipo_det_acq,   
		cod_misura,   
		cod_prodotto,   
		des_prodotto,   
		quan_fatturata,   
		prezzo_acquisto,   
		fat_conversione,   
		sconto_1,   
		sconto_2,   
		sconto_3,   
		cod_iva,   
		cod_deposito,   
		cod_ubicazione,   
		cod_lotto,   
		data_stock,   
		cod_tipo_movimento,   
		num_registrazione_mov_mag,   
		cod_conto,   
		nota_dettaglio,   
		anno_bolla_acq,   
		num_bolla_acq,   
		prog_riga_bolla_acq,   
		flag_accettazione,   
		anno_commessa,   
		num_commessa,   
		cod_centro_costo,   
		sconto_4,   
		sconto_5,   
		sconto_6,   
		sconto_7,   
		sconto_8,   
		sconto_9,   
		sconto_10,   
		anno_registrazione,   
		prog_stock,   
		anno_reg_des_mov,   
		num_reg_des_mov,   
		anno_reg_ord_acq,
		num_reg_ord_acq,
		prog_riga_ordine_acq,
		prog_bolla_acq)  
values (:s_cs_xx.cod_azienda,   
		:is_cod_doc_origine,   
		:is_numeratore_doc_origine,   
		:ii_anno_doc_origine,   
		:il_num_doc_origine,   
		:il_prog_fattura,   
		:ll_prog_riga_fat_acq,   
		:ls_cod_tipo_det_acq,   
		:ls_cod_misura,   
		:ls_cod_prodotto,   
		:ls_des_prodotto,   
		:ld_quan_acquisto,   
		:ld_prezzo_acquisto,   
		:ld_fat_conversione_acq,   
		:ld_sconto_1,   
		:ld_sconto_2,   
		:ld_sconto_3,   
		:ls_cod_iva,   
		:ls_cod_deposito,   
		:ls_cod_ubicazione,   
		:ls_cod_lotto,   
		:ldt_data_stock,   
		:ls_cod_tipo_movimento,   
		:ll_num_registrazione_mov_mag,
		null,   
		null,   
		:li_anno_bolla_acq,
		:ls_num_bolla_acq,
		:ll_prog_riga_bolla_acq,
		'N',   
		null,   
		null,   
		:ls_cod_centro_costo,   
		:ld_sconto_4,   
		:ld_sconto_5,   
		:ld_sconto_6,   
		:ld_sconto_7,   
		:ld_sconto_8,   
		:ld_sconto_9,   
		:ld_sconto_10,   
		:li_anno_registrazione_mov_mag,
		:ll_prog_stock,   
		:ll_anno_reg_dest_stock,
		:ll_num_reg_dest_stock,
		null,
		null,
		null,
		:ll_progressivo);

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la scrittura Dettagli Fatture Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	COMMIT;
next

destroy lds_rag_documenti
SetPointer(lp_old_pointer)
return 0
end function

public function integer wf_genera_fatture_acq (ref string fs_messaggio);string ls_cod_tipo_bol_acq, ls_flag_riep_fat, ls_cod_tipo_fat_acq, &
	    ls_flag_destinazione, ls_flag_num_doc, ls_flag_imballo, &
		 ls_flag_vettore, ls_flag_inoltro, ls_flag_mezzo, ls_flag_porto, ls_flag_resa, &
		 ls_cod_fornitore[], ls_cod_valuta, ls_cod_pagamento, &
		 ls_cod_banca, ls_cod_banca_clien_for, ls_flag_acc_materiali, &
		 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, &
		 ls_cod_porto, ls_cod_resa, ls_sort, ls_key_sort, ls_key_sort_old, ls_cod_tipo_fat_ven, &
		 ls_cod_operatore, ls_cod_tipo_analisi_bol, ls_cod_deposito[], &
		 ls_cod_tipo_listino_prodotto, ls_lire, &
		 ls_cod_tipo_analisi_fat, ls_cod_doc_origine, ls_cod_fil_fornitore, ls_cod_tipo_det_acq, &
		 ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, &
		 ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_acq, ls_cod_tipo_movimento, &
		 ls_flag_sola_iva, ls_flag_saldo, ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], &
		 ls_referenza, ls_flag_evasione, ls_numeratore_doc_origine, ls_num_bolla_acq
		 
integer li_anno_doc_origine, li_anno_bolla_acq, li_anno_registrazione_mov_mag

double ld_cambio_acq, ld_sconto_testata, ld_tot_val_evaso, ld_tot_val_bolla, &
		 ld_sconto_pagamento, ld_quan_bolla, ld_prezzo_acquisto, ld_fat_conversione_acq, &
		 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_quan_arrivata, ld_val_riga, ld_sconto_4, &
		 ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		 ld_quan_acquisto, ld_val_sconto_1, ld_val_riga_sconto_1, ld_val_sconto_2, &
		 ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, &
		 ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, &
		 ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, ld_val_sconto_8, &
		 ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, &
		 ld_val_riga_sconto_10, ld_val_sconto_testata, ld_val_riga_sconto_testata, &
		 ld_val_sconto_pagamento, ld_val_evaso

long ll_prog_riga_documento, ll_prog_bolla_acq, ll_prog_riga_fat_acq ,&
	  ll_prog_riga_bolla_acq, ll_anno_commessa, ll_num_commessa, ll_prog_stock[], &
	  ll_anno_reg_dest_stock, ll_i, ll_prog_fat_acq, ll_num_registrazione_mov_mag, &
	  ll_num_reg_dest_stock, ll_anno_registrazione[], ll_num_registrazione[], &
	  ll_anno_esercizio, ll_num_doc_origine

datetime ldt_data_bolla, ldt_data_fattura, ldt_data_stock[], ldt_oggi

datastore lds_rag_documenti
pointer lp_old_pointer

lp_old_pointer = SetPointer(HourGlass!)

lds_rag_documenti = create datastore
lds_rag_documenti.dataobject = "d_rag_bol_acq"
lds_rag_documenti.settransobject(sqlca)

ls_cod_tipo_fat_acq = s_cs_xx.parametri.parametro_s_1
ldt_data_fattura = s_cs_xx.parametri.parametro_data_1

ll_anno_esercizio = f_anno_esercizio()
li_anno_doc_origine = f_anno_esercizio()
ldt_oggi = datetime(today())

select	cod_documento,   
			numeratore_documento  
into 		:ls_cod_doc_origine,   
			:ls_numeratore_doc_origine  
from 		tab_tipi_fat_acq  
where 	tab_tipi_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and
			tab_tipi_fat_acq.cod_tipo_fat_acq = :ls_cod_tipo_fat_acq;
	
if isnull(ls_cod_doc_origine) or ls_cod_doc_origine = "" then
	fs_messaggio = "Documento non Definito in Tabella Tipi Fattura Acquisto"
	destroy lds_rag_documenti
	SetPointer(lp_old_pointer)
	return -1
end if

if isnull(ls_numeratore_doc_origine) or ls_cod_doc_origine = "" then
	fs_messaggio = "Numeratore non Definito in Tabella Tipi Fattura Acquisto"
	destroy lds_rag_documenti
	SetPointer(lp_old_pointer)
	return -1
end if
	
select	num_documento
into 		:ll_num_doc_origine
from 		numeratori
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_documento = :ls_cod_doc_origine and
			numeratore_documento = :ls_numeratore_doc_origine and
			anno_documento = :li_anno_doc_origine;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Numeratore non Definito per Codice Documento e Anno Esercizio Correnti"
	destroy lds_rag_documenti
	SetPointer(lp_old_pointer)
	return -1
end if
	
ll_num_doc_origine ++
	
update numeratori
set num_documento = :ll_num_doc_origine
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_documento = :ls_cod_doc_origine and
		numeratore_documento = :ls_numeratore_doc_origine and
		anno_documento = :li_anno_doc_origine;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore durante la scrittura Numeratori."
	destroy lds_rag_documenti
	SetPointer(lp_old_pointer)
	return -1
end if

for ll_i = 1 to dw_ext_carico_bolle_acq_det.rowcount()
	dw_ext_carico_bolle_acq_det.accepttext()
	li_anno_bolla_acq = dw_ext_carico_bolle_acq_det.getitemnumber(ll_i, "anno_bolla_acq")
	ls_num_bolla_acq = dw_ext_carico_bolle_acq_det.getitemstring(ll_i, "num_bolla_acq")
	ll_prog_bolla_acq = dw_ext_carico_bolle_acq_det.getitemnumber(ll_i, "progressivo")
	ll_prog_riga_bolla_acq = dw_ext_carico_bolle_acq_det.getitemnumber(ll_i, "prog_riga_bolla_acq")
	ls_flag_saldo = dw_ext_carico_bolle_acq_det.getitemstring(ll_i, "flag_saldo")
	ld_quan_acquisto = dw_ext_carico_bolle_acq_det.getitemnumber(ll_i, "quan_acquisto")
	
	select tes_bol_acq.cod_fornitore,   
			 tes_bol_acq.cod_valuta,   
			 tes_bol_acq.cod_pagamento,   
			 tes_bol_acq.cod_banca,   
			 tes_bol_acq.cod_banca_clien_for,
			 tes_bol_acq.cod_tipo_bol_acq
	 into  :ls_cod_fornitore[1],
			 :ls_cod_valuta,   
			 :ls_cod_pagamento,   
			 :ls_cod_banca,   
			 :ls_cod_banca_clien_for,
			 :ls_cod_tipo_bol_acq
	from   tes_bol_acq  
	where  tes_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_bol_acq.anno_bolla_acq = :li_anno_bolla_acq and  
			 tes_bol_acq.num_bolla_acq = :ls_num_bolla_acq and
			 tes_bol_acq.progressivo = :ll_prog_bolla_acq;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la lettura Testata Bolla Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	lds_rag_documenti.insertrow(0)
	lds_rag_documenti.setrow(lds_rag_documenti.rowcount())
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "anno_documento", li_anno_bolla_acq)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "num_documento", ls_num_bolla_acq)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "progressivo", ll_prog_bolla_acq)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "prog_riga_documento", ll_prog_riga_bolla_acq)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_fornitore", ls_cod_fornitore[1])
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_valuta", ls_cod_valuta)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_pagamento", ls_cod_pagamento)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca", ls_cod_banca)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca_clien_for", ls_cod_banca_clien_for)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_tipo_documento", ls_cod_tipo_bol_acq	)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "flag_saldo", ls_flag_saldo)
	lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "quan_acquisto", ld_quan_acquisto)
next

ls_sort = "cod_fornitore A, cod_valuta A, cod_pagamento A, cod_banca A, cod_banca_clien_for A, " + &
			 "anno_documento A, num_documento A, progressivo A, prog_riga_documento A"

lds_rag_documenti.setsort(ls_sort)

lds_rag_documenti.sort()

ls_key_sort_old = ""
for ll_i = 1 to lds_rag_documenti.rowcount()
	li_anno_bolla_acq = lds_rag_documenti.getitemnumber(ll_i, "anno_documento")
	ls_num_bolla_acq = lds_rag_documenti.getitemstring(ll_i, "num_documento")
	ll_prog_bolla_acq = lds_rag_documenti.getitemnumber(ll_i, "progressivo")
	ll_prog_riga_bolla_acq = lds_rag_documenti.getitemnumber(ll_i, "prog_riga_documento")
	ls_flag_saldo = lds_rag_documenti.getitemstring(ll_i, "flag_saldo")
	ld_quan_acquisto = lds_rag_documenti.getitemnumber(ll_i, "quan_acquisto")

	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_fornitore")) then
		ls_key_sort = lds_rag_documenti.getitemstring(ll_i, "cod_fornitore")
	else
		ls_key_sort = "      "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_valuta")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_valuta")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	ls_cod_tipo_bol_acq = lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")

	select tes_bol_acq.cod_operatore,   
			 tes_bol_acq.data_registrazione,   
			 tes_bol_acq.cod_fornitore,   
			 tes_bol_acq.cod_fil_fornitore,   			 
			 tes_bol_acq.cod_deposito,   
			 tes_bol_acq.cod_valuta,   
			 tes_bol_acq.cambio_acq,   
			 tes_bol_acq.cod_tipo_listino_prodotto,   
			 tes_bol_acq.cod_pagamento,   
			 tes_bol_acq.sconto,   
			 tes_bol_acq.cod_banca,   
			 tes_bol_acq.totale_val_bolla,
			 tes_bol_acq.cod_banca_clien_for
	 into  :ls_cod_operatore,   
			 :ldt_data_bolla,   
			 :ls_cod_fornitore[1],   
			 :ls_cod_fil_fornitore,   
			 :ls_cod_deposito[1],   
			 :ls_cod_valuta,   
			 :ld_cambio_acq,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_banca,   
			 :ld_tot_val_bolla,
			 :ls_cod_banca_clien_for
	from   tes_bol_acq  
	where  tes_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_bol_acq.anno_bolla_acq = :li_anno_bolla_acq and  
			 tes_bol_acq.num_bolla_acq = :ls_num_bolla_acq and
			 tes_bol_acq.progressivo = :ll_prog_bolla_acq;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la lettura Testata Bolla Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	if ls_key_sort <> ls_key_sort_old then
		select max(tes_fat_acq.progressivo)
		into :ll_prog_fat_acq  
		from tes_fat_acq 
		where tes_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				tes_fat_acq.cod_doc_origine = :ls_cod_doc_origine and
				tes_fat_acq.numeratore_doc_origine = :ls_numeratore_doc_origine and
				tes_fat_acq.anno_doc_origine = :li_anno_doc_origine and
				tes_fat_acq.num_doc_origine = :ll_num_doc_origine;

		if isnull(ll_prog_fat_acq) or ll_prog_fat_acq = 0 then
			ll_prog_fat_acq = 1
		else
			ll_prog_fat_acq ++
		end if

		select parametri_azienda.stringa
		into   :ls_lire
		from   parametri_azienda
		where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
				 parametri_azienda.flag_parametro = 'S' and &
				 parametri_azienda.cod_parametro = 'LIR';
	
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Configurare il codice valuta per le Lire Italiane."
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
	
		select tab_pagamenti.sconto
		into   :ld_sconto_pagamento
		from   tab_pagamenti
		where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;
	
		if sqlca.sqlcode <> 0 then
			ld_sconto_pagamento = 0
		end if

		insert into tes_fat_acq
						( cod_azienda,
						cod_doc_origine,
						numeratore_doc_origine,   
						anno_doc_origine,   
						num_doc_origine,   
						progressivo,   
						cod_tipo_fat_acq,   
						data_doc_origine,   
						cod_documento,   
						numeratore_documento,   
						anno_documento,   
						num_documento,   
						data_registrazione,   
						cod_operatore,   
						cod_fornitore,   
						cod_fil_fornitore,   
						cod_valuta,   
						cambio_acq,   
						cod_tipo_listino_prodotto,   
						cod_pagamento,   
						cod_banca,   
						cod_banca_clien_for,   
						cod_documento_nota,   
						numeratore_nota,   
						anno_nota,   
						num_nota,   
						tot_val_fat_acq,   
						tot_valuta_fat_acq,   
						importo_iva,   
						imponibile_iva,   
						importo_valuta_iva,   
						imponibile_valuta_iva,   
						importo_iva_indetraibile,   
						flag_fat_confermata,   
						sconto )
		values 		( :s_cs_xx.cod_azienda,   
						:ls_cod_doc_origine,   
						:ls_numeratore_doc_origine,   
						:li_anno_doc_origine,   
						:ll_num_doc_origine,   
						:ll_prog_fat_acq,   
						:ls_cod_tipo_fat_acq,   
						null,   
						null,   
						'',   
						0,   
						0,   
						:ldt_data_fattura,   
						:ls_cod_operatore,   
						:ls_cod_fornitore[1],   
						:ls_cod_fil_fornitore,   
						:ls_cod_valuta,   
						:ld_cambio_acq,   
						:ls_cod_tipo_listino_prodotto,   
						:ls_cod_pagamento,   
						:ls_cod_banca,   
						:ls_cod_banca_clien_for,   
						null,   
						'',   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						'S',   
						0 );

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la scrittura Testata Fatture Acquisto."
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if

		ll_prog_riga_fat_acq = 0
	
		fs_messaggio = fs_messaggio + ls_cod_doc_origine + "/" + ls_numeratore_doc_origine + "/" + &
						string(li_anno_doc_origine) + "/" + string(ll_num_doc_origine) + "/" + &
						string(ll_prog_fat_acq) + " "
	end if

	ll_prog_riga_fat_acq = ll_prog_riga_fat_acq + 10
	
	select	det_bol_acq.cod_tipo_det_acq,   
				det_bol_acq.cod_prodotto,   
				det_bol_acq.cod_misura,   
				det_bol_acq.des_prodotto,   
				det_bol_acq.quan_fatturata,
				det_bol_acq.prezzo_acquisto,   
				det_bol_acq.fat_conversione,   
				det_bol_acq.sconto_1,   
				det_bol_acq.sconto_2,  
				det_bol_acq.sconto_3,  
				det_bol_acq.cod_iva,   
				det_bol_acq.quan_arrivata,
				det_bol_acq.val_riga,				
				det_bol_acq.nota_dettaglio,   
				det_bol_acq.anno_commessa,
				det_bol_acq.num_commessa,
				det_bol_acq.cod_centro_costo,
				det_bol_acq.sconto_4,   
				det_bol_acq.sconto_5,   
				det_bol_acq.sconto_6,   
				det_bol_acq.sconto_7,   
				det_bol_acq.sconto_8,   
				det_bol_acq.sconto_9,   
				det_bol_acq.sconto_10,
				det_bol_acq.cod_deposito,
				det_bol_acq.cod_ubicazione,
				det_bol_acq.cod_lotto,
				det_bol_acq.data_stock,
				det_bol_acq.progr_stock,
				det_bol_acq.anno_registrazione_mov_mag,
				det_bol_acq.num_registrazione_mov_mag,
				det_bol_acq.flag_accettazione
	into		:ls_cod_tipo_det_acq, 
				:ls_cod_prodotto, 
				:ls_cod_misura, 
				:ls_des_prodotto, 
				:ld_quan_bolla,
				:ld_prezzo_acquisto, 
				:ld_fat_conversione_acq, 
				:ld_sconto_1, 
				:ld_sconto_2, 
				:ld_sconto_3, 
				:ls_cod_iva, 
				:ld_quan_arrivata,
				:ld_val_riga,
				:ls_nota_dettaglio, 
				:ll_anno_commessa,
				:ll_num_commessa,
				:ls_cod_centro_costo,
				:ld_sconto_4, 
				:ld_sconto_5, 
				:ld_sconto_6, 
				:ld_sconto_7, 
				:ld_sconto_8, 
				:ld_sconto_9, 
				:ld_sconto_10,
				:ls_cod_deposito[1],
				:ls_cod_ubicazione[1],
				:ls_cod_lotto[1],
				:ldt_data_stock[1],
				:ll_prog_stock[1],
				:li_anno_registrazione_mov_mag,
				:ll_num_registrazione_mov_mag,
				:ls_flag_acc_materiali
	from 		det_bol_acq  
	where 	det_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				det_bol_acq.anno_bolla_acq = :li_anno_bolla_acq and  
				det_bol_acq.num_bolla_acq = :ls_num_bolla_acq and
				det_bol_acq.progressivo = :ll_prog_bolla_acq and
				det_bol_acq.prog_riga_bolla_acq = :ll_prog_riga_bolla_acq
	order by det_bol_acq.prog_riga_bolla_acq asc;

	if sqlca.sqlcode = -1 then
		fs_messaggio =  "Errore durante la lettura Dettagli Bolle Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	select tab_tipi_det_acq.flag_tipo_det_acq,
			 tab_tipi_det_acq.cod_tipo_movimento,
			 tab_tipi_det_acq.flag_sola_iva
	into   :ls_flag_tipo_det_acq,
			 :ls_cod_tipo_movimento,
			 :ls_flag_sola_iva
	from   tab_tipi_det_acq
	where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	if ls_flag_sola_iva = 'N' then
		ld_val_riga = ld_quan_acquisto * ld_prezzo_acquisto
		ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
		ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
		ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
		ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
		ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
		ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
		ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
		ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
		ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
		ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
		ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
		ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
		ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
		ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
		ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
		ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
		ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
		ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
		ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
		if ls_cod_valuta = ls_lire then
			ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
		else
			ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
		end if   
	
		if ls_flag_tipo_det_acq = "M" or &
			ls_flag_tipo_det_acq = "C" or &
			ls_flag_tipo_det_acq = "N" then
			ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
			ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
			ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
			ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
		elseif ls_flag_tipo_det_acq = "S" then
			ld_val_evaso = ld_val_riga_sconto_10 * -1
		else
			ld_val_evaso = ld_val_riga_sconto_10
		end if
		ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
	end if

	if ls_cod_valuta = ls_lire then
		ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
	end if   

	update det_bol_acq
		set quan_fatturata = quan_fatturata + :ld_quan_acquisto
		where det_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 det_bol_acq.anno_bolla_acq = :li_anno_bolla_acq and  
			 det_bol_acq.num_bolla_acq = :ls_num_bolla_acq and
			 det_bol_acq.progressivo = :ll_prog_bolla_acq and
			 det_bol_acq.prog_riga_bolla_acq = :ll_prog_riga_bolla_acq;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento Dettaglio Bolla Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	update tes_fat_acq
		set tot_val_fat_acq = tot_val_fat_acq + :ld_val_evaso
	 where tes_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_fat_acq.cod_doc_origine = :ls_cod_doc_origine and  
			 tes_fat_acq.numeratore_doc_origine = :ls_numeratore_doc_origine and  
			 tes_fat_acq.anno_doc_origine = :li_anno_doc_origine and  
			 tes_fat_acq.num_doc_origine = :ll_num_doc_origine and  
			 tes_fat_acq.progressivo = :ll_prog_fat_acq;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento Testata Fatture Acquisto."			
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	insert into	det_fat_acq
					(cod_azienda,   
					cod_doc_origine,   
					numeratore_doc_origine,   
					anno_doc_origine,   
					num_doc_origine,   
					progressivo,   
					prog_riga_fat_acq,   
					cod_tipo_det_acq,   
					cod_misura,   
					cod_prodotto,   
					des_prodotto,   
					quan_fatturata,   
					prezzo_acquisto,   
					fat_conversione,   
					sconto_1,   
					sconto_2,   
					sconto_3,   
					cod_iva,   
					cod_deposito,   
					cod_ubicazione,   
					cod_lotto,   
					data_stock,   
					cod_tipo_movimento,   
					num_registrazione_mov_mag,   
					cod_conto,   
					nota_dettaglio,   
					anno_bolla_acq,   
					num_bolla_acq,   
					prog_riga_bolla_acq,   
					flag_accettazione,   
					anno_commessa,   
					num_commessa,   
					cod_centro_costo,   
					sconto_4,   
					sconto_5,   
					sconto_6,   
					sconto_7,   
					sconto_8,   
					sconto_9,   
					sconto_10,   
					anno_registrazione,   
					prog_stock,   
					anno_reg_des_mov,   
					num_reg_des_mov,
					anno_reg_ord_acq,
					num_reg_ord_acq,
					prog_riga_ordine_acq,
					prog_bolla_acq)
	values		(:s_cs_xx.cod_azienda,   
					:ls_cod_doc_origine,   
					:ls_numeratore_doc_origine,   
					:li_anno_doc_origine,   
					:ll_num_doc_origine,   
					:ll_prog_fat_acq,   
					:ll_prog_riga_fat_acq,   
					:ls_cod_tipo_det_acq,   
					:ls_cod_misura,   
					:ls_cod_prodotto,   
					:ls_des_prodotto,   
					:ld_quan_acquisto,   
					:ld_prezzo_acquisto,   
					:ld_fat_conversione_acq,   
					:ld_sconto_1,   
					:ld_sconto_2,   
					:ld_sconto_3,   
					:ls_cod_iva,   
					:ls_cod_deposito[1],   
					:ls_cod_ubicazione[1],   
					:ls_cod_lotto[1],   
					:ldt_data_stock[1],   
					:ls_cod_tipo_movimento,   
					:ll_num_registrazione_mov_mag,
					null,   
					null,   
					:li_anno_bolla_acq,
					:ls_num_bolla_acq,
					:ll_prog_riga_bolla_acq,
					:ls_flag_acc_materiali,   
					null,   
					null,   
					:ls_cod_centro_costo,   
					:ld_sconto_4,   
					:ld_sconto_5,   
					:ld_sconto_6,   
					:ld_sconto_7,   
					:ld_sconto_8,   
					:ld_sconto_9,   
					:ld_sconto_10,   
					:li_anno_registrazione_mov_mag,
					:ll_prog_stock[1],   
					:ll_anno_reg_dest_stock,
					:ll_num_reg_dest_stock,
					null,
					null,
					null,
					:ll_prog_bolla_acq);

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la scrittura Dettagli Fatture Acquisto."
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	ls_key_sort_old = ls_key_sort
	COMMIT;
next

destroy lds_rag_documenti

SetPointer(lp_old_pointer)
return 0
end function

event pc_setddlb;call super::pc_setddlb;dw_ext_carico_bolle.fu_loadcode("cod_fornitore", &
                        "anag_fornitori", &
								"cod_fornitore", &
								"rag_soc_1", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_fornitore asc", "" )

dw_ext_carico_bolle.fu_loadcode("cod_prodotto", &
                        "anag_prodotti", &
								"cod_prodotto", &
								"des_prodotto", &
								"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by cod_prodotto asc", "" )
end event

event pc_setwindow;call super::pc_setwindow;string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[],l_objects[]

dw_lista_bolle_acquisto.set_dw_options(sqlca, &
                                        pcca.null_object, &
													 c_nonew + &
													 c_nomodify + &
													 c_nodelete + &
													 c_disablecc + &
													 c_noretrieveonopen, &
													 c_default)

iuo_dw_main = dw_lista_bolle_acquisto


l_criteriacolumn[1] = "cod_fornitore"
l_criteriacolumn[2] = "cod_prodotto"
l_criteriacolumn[3] = "anno_registrazione"
l_criteriacolumn[4] = "num_registrazione"

l_searchtable[1] = "tes_bol_acq"
l_searchtable[2] = "tes_bol_acq"
l_searchtable[3] = "tes_bol_acq"
l_searchtable[4] = "tes_bol_acq"

l_searchcolumn[1] = "tes_bol_acq_cod_fornitore"
l_searchcolumn[2] = "det_bol_acq_cod_prodotto"
l_searchcolumn[3] = "det_bol_acq_anno_registrazione"
l_searchcolumn[4] = "det_bol_acq_num_registrazione"

dw_ext_carico_bolle.fu_wiredw(l_criteriacolumn[], &
											dw_lista_bolle_acquisto, &
											l_searchtable[], &
											l_searchcolumn[], &
											SQLCA)

dw_lista_bolle_acquisto.object.st_det_bol_acq_quan_arrivata.visible = 0
dw_lista_bolle_acquisto.object.det_bol_acq_quan_arrivata.visible = 0

l_objects[1] = dw_lista_bolle_acquisto
dw_folder_search.fu_assigntab(2, "Bolle", l_objects[])
l_objects[1] = dw_ext_carico_bolle_acq_det
l_objects[2] = cb_genera
dw_folder_search.fu_assigntab(3, "Fatture", l_objects[])
l_objects[1] = dw_ext_carico_bolle
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
//l_objects[4] = cb_ricerca_fornitore
//l_objects[4] = cb_ricerca_prodotto
l_objects[4] = cbx_quan_arrivata
dw_folder_search.fu_assigntab(1, "Filtra Bolle", l_objects[])

dw_folder_search.fu_foldercreate(2,2)
dw_folder_search.fu_selecttab(1)
dw_lista_bolle_acquisto.change_dw_current()

if not isnull(s_cs_xx.parametri.parametro_s_14) and s_cs_xx.parametri.parametro_s_14 <> "" then 
	is_cod_fornitore = s_cs_xx.parametri.parametro_s_14
	dw_ext_carico_bolle.setitem(1, "cod_fornitore", is_cod_fornitore)	
	dw_ext_carico_bolle.object.cod_fornitore.protect = 1	
//	cb_ricerca_fornitore.enabled = false
else
	setnull(is_cod_fornitore)
end if

if not isnull(s_cs_xx.parametri.parametro_s_15) and s_cs_xx.parametri.parametro_s_15 <> "" then 
	is_cod_doc_origine = s_cs_xx.parametri.parametro_s_15
else
	setnull(is_cod_doc_origine)
end if

if not isnull(s_cs_xx.parametri.parametro_s_13) and s_cs_xx.parametri.parametro_s_13 <> "" then 
	is_numeratore_doc_origine = s_cs_xx.parametri.parametro_s_13
else
	setnull(is_numeratore_doc_origine)
end if

if not isnull(s_cs_xx.parametri.parametro_i_1) and s_cs_xx.parametri.parametro_i_1 <> 0 then 
	ii_anno_doc_origine = s_cs_xx.parametri.parametro_i_1
else
	setnull(ii_anno_doc_origine)
end if
	
if not isnull(s_cs_xx.parametri.parametro_ul_1) and s_cs_xx.parametri.parametro_ul_1 <> 0 then 
	il_num_doc_origine = s_cs_xx.parametri.parametro_ul_1
else
	setnull(il_num_doc_origine)
end if

if not isnull(s_cs_xx.parametri.parametro_ul_2) and s_cs_xx.parametri.parametro_ul_2 <> 0 then 
	il_prog_fattura = s_cs_xx.parametri.parametro_ul_2
else
	setnull(il_prog_fattura)
end if
end event

on w_carico_bolle_acq.create
int iCurrent
call super::create
this.cb_reset=create cb_reset
this.cb_ricerca=create cb_ricerca
this.cb_genera=create cb_genera
this.dw_ext_carico_bolle=create dw_ext_carico_bolle
this.cbx_quan_arrivata=create cbx_quan_arrivata
this.dw_lista_bolle_acquisto=create dw_lista_bolle_acquisto
this.dw_folder_search=create dw_folder_search
this.dw_ext_carico_bolle_acq_det=create dw_ext_carico_bolle_acq_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_reset
this.Control[iCurrent+2]=this.cb_ricerca
this.Control[iCurrent+3]=this.cb_genera
this.Control[iCurrent+4]=this.dw_ext_carico_bolle
this.Control[iCurrent+5]=this.cbx_quan_arrivata
this.Control[iCurrent+6]=this.dw_lista_bolle_acquisto
this.Control[iCurrent+7]=this.dw_folder_search
this.Control[iCurrent+8]=this.dw_ext_carico_bolle_acq_det
end on

on w_carico_bolle_acq.destroy
call super::destroy
destroy(this.cb_reset)
destroy(this.cb_ricerca)
destroy(this.cb_genera)
destroy(this.dw_ext_carico_bolle)
destroy(this.cbx_quan_arrivata)
destroy(this.dw_lista_bolle_acquisto)
destroy(this.dw_folder_search)
destroy(this.dw_ext_carico_bolle_acq_det)
end on

type cb_reset from commandbutton within w_carico_bolle_acq
event clicked pbm_bnclicked
integer x = 1481
integer y = 380
integer width = 361
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;string ls_filtro, ls_null
datetime ldt_null

setnull(ls_null)
setnull(ldt_null)

if not isnull(is_cod_fornitore) then 
	dw_ext_carico_bolle.setitem(dw_ext_carico_bolle.getrow(),"cod_prodotto", ls_null)
//	dw_ext_carico_bolle.setitem(dw_ext_carico_bolle.getrow(),"data_consegna_inizio",ldt_null)
//	dw_ext_carico_bolle.setitem(dw_ext_carico_bolle.getrow(),"data_consegna_fine"  ,ldt_null)
else
	dw_ext_carico_bolle.setitem(dw_ext_carico_bolle.getrow(),"cod_prodotto", ls_null)
	dw_ext_carico_bolle.setitem(dw_ext_carico_bolle.getrow(),"cod_fornitore", ls_null)
//	dw_ext_carico_bolle.setitem(dw_ext_carico_bolle.getrow(),"data_consegna_inizio",ldt_null)
//	dw_ext_carico_bolle.setitem(dw_ext_carico_bolle.getrow(),"data_consegna_fine"  ,ldt_null)
	ls_filtro = ""
	dw_lista_bolle_acquisto.setfilter(ls_filtro)
	dw_lista_bolle_acquisto.filter()
	dw_folder_search.fu_SelectTab(2)
	dw_lista_bolle_acquisto.change_dw_current()
	parent.triggerevent("pc_retrieve")
end if
end event

type cb_ricerca from commandbutton within w_carico_bolle_acq
event clicked pbm_bnclicked
integer x = 1874
integer y = 380
integer width = 361
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;string ls_filtro, ls_cod_fornitore, ls_cod_prodotto

dw_ext_carico_bolle.setcolumn(3)
dw_ext_carico_bolle.setcolumn(4)
dw_ext_carico_bolle.triggerevent("itemchanged")

ls_cod_fornitore = dw_ext_carico_bolle.getitemstring(dw_ext_carico_bolle.getrow(),"cod_fornitore")
ls_cod_prodotto  = dw_ext_carico_bolle.getitemstring(dw_ext_carico_bolle.getrow(),"cod_prodotto")
ls_filtro = ""

if not isnull(ls_cod_fornitore) then
	ls_filtro = ls_filtro + "(tes_bol_acq_cod_fornitore = '" + ls_cod_fornitore + "'"
	if not isnull(ls_cod_prodotto) then
		ls_filtro = ls_filtro + " and "
	else
		ls_filtro = ls_filtro + ")"
	end if
end if

if not isnull(ls_cod_prodotto) then
	if ls_filtro = "" then ls_filtro = ls_filtro + "("
	ls_filtro = ls_filtro + "det_bol_acq_cod_prodotto = '" + ls_cod_prodotto + "')"
end if

dw_lista_bolle_acquisto.setfilter(ls_filtro)
dw_lista_bolle_acquisto.filter()

dw_folder_search.fu_SelectTab(2)
dw_lista_bolle_acquisto.change_dw_current()
parent.triggerevent("pc_retrieve")
end event

type cb_genera from commandbutton within w_carico_bolle_acq
event clicked pbm_bnclicked
integer x = 3040
integer y = 1740
integer width = 361
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Gen. Fatture"
end type

event clicked;integer li_ret
string ls_messaggio

if dw_ext_carico_bolle_acq_det.rowcount() > 0 then
	if isnull(is_cod_fornitore) then
		window_open(w_ext_dati_fattura_nomag, 0)
		if isnull(s_cs_xx.parametri.parametro_s_1) or isnull(s_cs_xx.parametri.parametro_data_1) then return
		li_ret = wf_genera_fatture_acq(ref ls_messaggio)
	else
		li_ret = wf_carica_dett_fatture(ref ls_messaggio)
	end if
	
	g_mb.messagebox("Carico Acquisti", ls_messaggio, exclamation!)
	dw_ext_carico_bolle_acq_det.reset()
	cb_ricerca.triggerevent("clicked")
else
	g_mb.messagebox("Carico Acquisti", "Nessuna Bolla da Caricare", exclamation!)
end if

end event

type dw_ext_carico_bolle from u_dw_search within w_carico_bolle_acq
integer x = 41
integer y = 140
integer width = 2263
integer height = 220
integer taborder = 30
string dataobject = "d_ext_carico_bolle"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ext_carico_bolle,"cod_fornitore")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ext_carico_bolle,"cod_prodotto")
end choose
end event

type cbx_quan_arrivata from checkbox within w_carico_bolle_acq
event clicked pbm_bnclicked
integer x = 87
integer y = 540
integer width = 809
integer height = 76
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Visualizza Quantità Arrivata"
end type

event clicked;if this.checked then
	dw_lista_bolle_acquisto.object.st_det_bol_acq_quan_arrivata.visible = 1
	dw_lista_bolle_acquisto.object.det_bol_acq_quan_arrivata.visible = 1
else	
	dw_lista_bolle_acquisto.object.st_det_bol_acq_quan_arrivata.visible = 0
	dw_lista_bolle_acquisto.object.det_bol_acq_quan_arrivata.visible = 0
end if
end event

type dw_lista_bolle_acquisto from uo_cs_xx_dw within w_carico_bolle_acq
integer x = 41
integer y = 140
integer width = 3360
integer height = 1580
integer taborder = 80
string dataobject = "d_lista_bolle_acquisto"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event doubleclicked;call super::doubleclicked;string ls_cod_banca, ls_cod_banca_old, ls_cod_banca_clien_for, ls_cod_banca_clien_for_old, &
       ls_cod_valuta, ls_cod_valuta_old, ls_cod_tipo_listino_prodotto, ls_cod_tipo_listino_prodotto_old, &
       ls_cod_pagamento, ls_cod_pagamento_old, ls_cod_fornitore_old, ls_cod_fornitore, &
		 ls_num_bolla_acq, ls_num_bolla_acq_old
long   ll_anno_bolla_acq, ll_riga, ll_i, ll_anno_bolla_acq_old, ll_progressivo, &
       ll_num_rows, ll_prog_riga_bolla_acq, ll_progressivo_old
double ld_cambio_acq, ld_sconto, ld_cambio_acq_old, ld_sconto_old

if row > 0 then
	ll_anno_bolla_acq = this.getitemnumber(row, "det_bol_acq_anno_bolla_acq")
	ls_num_bolla_acq = this.getitemstring(row, "det_bol_acq_num_bolla_acq")
	ll_progressivo = this.getitemnumber(row, "det_bol_acq_progressivo")
	ll_prog_riga_bolla_acq = this.getitemnumber(row, "det_bol_acq_prog_riga_bolla_acq")
	for ll_i = 1 to dw_ext_carico_bolle_acq_det.rowcount()
		if ll_anno_bolla_acq = dw_ext_carico_bolle_acq_det.getitemnumber(ll_i, "anno_bolla_acq") and &
			ls_num_bolla_acq  = dw_ext_carico_bolle_acq_det.getitemstring(ll_i, "num_bolla_acq") and &
			ll_progressivo  = dw_ext_carico_bolle_acq_det.getitemnumber(ll_i, "progressivo") and &
			ll_prog_riga_bolla_acq  = dw_ext_carico_bolle_acq_det.getitemnumber(ll_i, "prog_riga_bolla_acq") then
				g_mb.messagebox("Carico Acquisti","Questa riga bolla è già stata spostata in acquisto",Information!)
				return
		end if
	next
	ll_num_rows = dw_ext_carico_bolle_acq_det.rowcount()
	if ll_num_rows >= 1 then
		ll_anno_bolla_acq_old = dw_ext_carico_bolle_acq_det.getitemnumber(1, "anno_bolla_acq")
		ls_num_bolla_acq_old  = dw_ext_carico_bolle_acq_det.getitemstring(1, "num_bolla_acq")
		ll_progressivo_old = dw_ext_carico_bolle_acq_det.getitemnumber(1, "progressivo")

		select cod_fornitore,
		       cod_banca,
				 cod_banca_clien_for,
				 cod_valuta,
				 cambio_acq,
				 cod_tipo_listino_prodotto,
				 cod_pagamento,
				 sconto
		into   :ls_cod_fornitore_old,
		       :ls_cod_banca_old,
				 :ls_cod_banca_clien_for_old,
				 :ls_cod_valuta_old,
				 :ld_cambio_acq_old,
				 :ls_cod_tipo_listino_prodotto_old,
				 :ls_cod_pagamento_old,
				 :ld_sconto_old
		from   tes_bol_acq
		where  tes_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and
				 tes_bol_acq.anno_bolla_acq = :ll_anno_bolla_acq_old and
				 tes_bol_acq.num_bolla_acq = :ls_num_bolla_acq_old and
				 tes_bol_acq.progressivo = :ll_progressivo_old;

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore durante ricerca bolla acquisto")
			return
		end if	 

		select cod_fornitore,
		       cod_banca,
				 cod_banca_clien_for,
				 cod_valuta,
				 cambio_acq,
				 cod_tipo_listino_prodotto,
				 cod_pagamento,
				 sconto
		into   :ls_cod_fornitore,
		       :ls_cod_banca,
				 :ls_cod_banca_clien_for,
				 :ls_cod_valuta,
				 :ld_cambio_acq,
				 :ls_cod_tipo_listino_prodotto,
				 :ls_cod_pagamento,
				 :ld_sconto
		from   tes_bol_acq
		where  tes_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and
				 tes_bol_acq.anno_bolla_acq = :ll_anno_bolla_acq and
				 tes_bol_acq.num_bolla_acq = :ls_num_bolla_acq and
				 tes_bol_acq.progressivo = :ll_progressivo;

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore durante ricerca bolla acquisto")
			return
		end if	 

		if ls_cod_fornitore <> ls_cod_fornitore_old then
			g_mb.messagebox("APICE","Attenzione: è stato selezionato un fornitore diverso da quello iniziale")
			return
		end if
		
		if ls_cod_valuta <> ls_cod_valuta_old or &
		   ld_cambio_acq <> ld_cambio_acq_old or &
			ls_cod_tipo_listino_prodotto <> ls_cod_tipo_listino_prodotto_old or &
			ls_cod_pagamento <> ls_cod_pagamento_old or &
			ld_sconto <> ld_sconto_old or &
			ls_cod_banca <> ls_cod_banca_old or &
			ls_cod_banca_clien_for <> ls_cod_banca_clien_for then
			
			if g_mb.messagebox("APICE","Attenzione: condizioni differenti fra questo bolla ed il primo selezionato. Proseguo lo stesso", Question!, YesNo!, 1) = 2 then return
		end if			
	end if
	
	dw_ext_carico_bolle_acq_det.setfocus()
	ll_riga = dw_ext_carico_bolle_acq_det.insertrow(0)
	dw_ext_carico_bolle_acq_det.setitem(ll_riga, "anno_bolla_acq", &
		dw_lista_bolle_acquisto.getitemnumber(row, "det_bol_acq_anno_bolla_acq") )
	dw_ext_carico_bolle_acq_det.setitem(ll_riga, "num_bolla_acq", &
		dw_lista_bolle_acquisto.getitemstring(row, "det_bol_acq_num_bolla_acq") )
	dw_ext_carico_bolle_acq_det.setitem(ll_riga, "progressivo", &
		dw_lista_bolle_acquisto.getitemnumber(row, "det_bol_acq_progressivo") )
	dw_ext_carico_bolle_acq_det.setitem(ll_riga, "prog_riga_bolla_acq", &
		dw_lista_bolle_acquisto.getitemnumber(row, "det_bol_acq_prog_riga_bolla_acq") )
	dw_ext_carico_bolle_acq_det.setitem(ll_riga, "cod_fornitore", &
		dw_lista_bolle_acquisto.getitemstring(row, "tes_bol_acq_cod_fornitore") )
	dw_ext_carico_bolle_acq_det.setitem(ll_riga, "data_registrazione", &
		dw_lista_bolle_acquisto.getitemdatetime(row, "tes_bol_acq_data_registrazione") )
//	dw_ext_carico_bolle_acq_det.setitem(ll_riga, "cod_prod_fornitore", &
//		dw_lista_bolle_acquisto.getitemstring(row, "det_bol_acq_cod_prod_fornitore") )
	dw_ext_carico_bolle_acq_det.setitem(ll_riga, "cod_prodotto", &
		dw_lista_bolle_acquisto.getitemstring(row, "det_bol_acq_cod_prodotto") )
	dw_ext_carico_bolle_acq_det.setitem(ll_riga, "des_prodotto", &
		dw_lista_bolle_acquisto.getitemstring(row, "cf_des_prodotto") )
	dw_ext_carico_bolle_acq_det.setitem(ll_riga, "cod_misura", &
		dw_lista_bolle_acquisto.getitemstring(row, "det_bol_acq_cod_misura") )
	dw_ext_carico_bolle_acq_det.setitem(ll_riga, "quan_acquisto", &
		dw_lista_bolle_acquisto.getitemnumber(row, "cf_quan_evasione") )
	dw_ext_carico_bolle_acq_det.setitem(ll_riga, "prezzo_acquisto", &
		dw_lista_bolle_acquisto.getitemnumber(row, "det_bol_acq_prezzo_acquisto") )
	dw_ext_carico_bolle_acq_det.setitem(ll_riga, "flag_saldo", "N" )
end if
end event

type dw_folder_search from u_folder within w_carico_bolle_acq
integer x = 23
integer y = 20
integer width = 3429
integer height = 1820
integer taborder = 40
end type

type dw_ext_carico_bolle_acq_det from datawindow within w_carico_bolle_acq
event doubleclicked pbm_dwnlbuttondblclk
event itemchanged pbm_dwnitemchange
integer x = 41
integer y = 140
integer width = 3360
integer height = 1580
integer taborder = 91
string dataobject = "d_ext_carico_bolle_acq_det"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event doubleclicked;this.deleterow(row)
end event

event itemchanged;//string ls_nome_colonna
//double ld_quan_acquisto, ld_quan_acquisto_old
//
//ls_nome_colonna = this.getcolumnname()
//if ls_nome_colonna = "quan_acquisto" then
//	ld_quan_acquisto = double(data)
//	ld_quan_acquisto_old = dw_ext_carico_acq_det.getitemnumber(row,"quan_acquisto")
//	if ld_quan_acquisto < ld_quan_acquisto_old then
//		dw_ext_carico_acq_det.setitem(row, "flag_saldo", "N")
//	else
//		dw_ext_carico_acq_det.setitem(row, "flag_saldo", "S")
//	end if
//end if
end event

