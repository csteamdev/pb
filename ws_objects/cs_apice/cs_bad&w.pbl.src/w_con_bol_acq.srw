﻿$PBExportHeader$w_con_bol_acq.srw
$PBExportComments$Finestra Gestione Parametri Bolle Acquisto
forward
global type w_con_bol_acq from w_cs_xx_principale
end type
type dw_con_bol_acq from uo_cs_xx_dw within w_con_bol_acq
end type
end forward

global type w_con_bol_acq from w_cs_xx_principale
integer width = 2848
integer height = 564
string title = "Gestione Parametri Bolle Acquisto"
dw_con_bol_acq dw_con_bol_acq
end type
global w_con_bol_acq w_con_bol_acq

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_con_bol_acq, &
                 "cod_tipo_bol_acq", &
                 sqlca, &
                 "tab_tipi_bol_acq", &
                 "cod_tipo_bol_acq", &
                 "des_tipo_bol_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_con_bol_acq, &
                 "cod_tipo_det_acq_mag", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_con_bol_acq, &
                 "cod_tipo_det_acq_nomag", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

// stefanop 16/07/2010: specifica "modifiche_varie_2010" funzione 3 PTENDA
f_po_loaddddw_dw(dw_con_bol_acq, &
                 "cod_tipo_movimento_grezzo", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
// ----
end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_con_bol_acq.set_dw_key("cod_azienda")
dw_con_bol_acq.set_dw_options(sqlca, &
                              pcca.null_object, &
                              c_default, &
                              c_default)

end on

on w_con_bol_acq.create
int iCurrent
call super::create
this.dw_con_bol_acq=create dw_con_bol_acq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_con_bol_acq
end on

on w_con_bol_acq.destroy
call super::destroy
destroy(this.dw_con_bol_acq)
end on

type dw_con_bol_acq from uo_cs_xx_dw within w_con_bol_acq
integer x = 23
integer y = 20
integer width = 2766
integer height = 420
string dataobject = "d_con_bol_acq"
borderstyle borderstyle = styleraised!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

