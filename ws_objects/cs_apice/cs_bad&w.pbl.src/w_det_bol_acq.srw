﻿$PBExportHeader$w_det_bol_acq.srw
$PBExportComments$Finestra Gestione  Dettaglio Bolle Acquisto
forward
global type w_det_bol_acq from w_cs_xx_principale
end type
type cb_des_mov from commandbutton within w_det_bol_acq
end type
type st_1 from statictext within w_det_bol_acq
end type
type cb_prezzo from commandbutton within w_det_bol_acq
end type
type cb_sconti from commandbutton within w_det_bol_acq
end type
type cb_prodotti_note_ricerca from cb_prod_note_ricerca within w_det_bol_acq
end type
type pb_prod_view from cb_listview within w_det_bol_acq
end type
type cb_corrispondenze from commandbutton within w_det_bol_acq
end type
type cb_c_industriale from commandbutton within w_det_bol_acq
end type
type cb_stock from cb_stock_ricerca within w_det_bol_acq
end type
type dw_det_bol_acq_det_1 from uo_cs_xx_dw within w_det_bol_acq
end type
type cb_ric_bol_ven from commandbutton within w_det_bol_acq
end type
type dw_det_bol_acq_det_2 from uo_cs_xx_dw within w_det_bol_acq
end type
type dw_folder from u_folder within w_det_bol_acq
end type
type dw_det_bol_acq_lista from uo_cs_xx_dw within w_det_bol_acq
end type
end forward

global type w_det_bol_acq from w_cs_xx_principale
integer width = 3566
integer height = 1448
string title = "Righe Bolle Entrata"
boolean minbox = false
cb_des_mov cb_des_mov
st_1 st_1
cb_prezzo cb_prezzo
cb_sconti cb_sconti
cb_prodotti_note_ricerca cb_prodotti_note_ricerca
pb_prod_view pb_prod_view
cb_corrispondenze cb_corrispondenze
cb_c_industriale cb_c_industriale
cb_stock cb_stock
dw_det_bol_acq_det_1 dw_det_bol_acq_det_1
cb_ric_bol_ven cb_ric_bol_ven
dw_det_bol_acq_det_2 dw_det_bol_acq_det_2
dw_folder dw_folder
dw_det_bol_acq_lista dw_det_bol_acq_lista
end type
global w_det_bol_acq w_det_bol_acq

type variables
boolean ib_modifica=false, ib_nuovo=false, ib_update = false
end variables

event pc_setddlb;call super::pc_setddlb;
f_po_loaddddw_dw(dw_det_bol_acq_lista, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")


f_po_loaddddw_dw(dw_det_bol_acq_det_1, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_det_bol_acq_det_1, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//f_po_loaddddw_dw(dw_det_bol_acq_det_1, &
//                 "cod_prodotto", &
//                 sqlca, &
//                 "anag_prodotti", &
//                 "cod_prodotto", &
//                 "des_prodotto", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_bol_acq_det_1, &
                 "cod_misura", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_bol_acq_det_1, &
                 "cod_tipo_movimento", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_det_bol_acq_det_2, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
end event

event pc_setwindow;call super::pc_setwindow;dw_det_bol_acq_lista.set_dw_key("cod_azienda")
dw_det_bol_acq_lista.set_dw_key("anno_bol_acq")
dw_det_bol_acq_lista.set_dw_key("num_bol_acq")

dw_det_bol_acq_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + &
                                    c_nohighlightselected + c_ViewModeBorderUnchanged)
dw_det_bol_acq_det_1.set_dw_options(sqlca, &
                                    dw_det_bol_acq_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
dw_det_bol_acq_det_2.set_dw_options(sqlca, &
                                    dw_det_bol_acq_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

windowobject lw_oggetti[], lw_oggetti1[]

lw_oggetti1[1] = dw_det_bol_acq_lista
dw_folder.fu_assigntab(1, "Lista", lw_oggetti1[])
lw_oggetti[1] = dw_det_bol_acq_det_2
lw_oggetti[2] = cb_prodotti_note_ricerca
lw_oggetti[3] = cb_ric_bol_ven
dw_folder.fu_assigntab(3, "Analitica/Note", lw_oggetti[])
lw_oggetti[1] = dw_det_bol_acq_det_1
lw_oggetti[2] = cb_stock
lw_oggetti[3] = cb_sconti
lw_oggetti[4] = cb_prezzo
lw_oggetti[5] = cb_c_industriale
lw_oggetti[6] = cb_corrispondenze
lw_oggetti[7] = pb_prod_view
lw_oggetti[8] = cb_des_mov
dw_folder.fu_assigntab(2, "Dettaglio", lw_oggetti[])
dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

iuo_dw_main=dw_det_bol_acq_lista
cb_prezzo.enabled=false
cb_c_industriale.enabled = false
cb_corrispondenze.enabled = false

try
	dw_det_bol_acq_det_1.object.p_mov.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
	dw_det_bol_acq_det_1.object.p_mov_terzista.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
catch (runtimeerror err)
end try

dw_det_bol_acq_lista.is_cod_parametro_blocco_prodotto = 'PBA'
dw_det_bol_acq_det_1.is_cod_parametro_blocco_prodotto = 'PBA'

end event

on w_det_bol_acq.create
int iCurrent
call super::create
this.cb_des_mov=create cb_des_mov
this.st_1=create st_1
this.cb_prezzo=create cb_prezzo
this.cb_sconti=create cb_sconti
this.cb_prodotti_note_ricerca=create cb_prodotti_note_ricerca
this.pb_prod_view=create pb_prod_view
this.cb_corrispondenze=create cb_corrispondenze
this.cb_c_industriale=create cb_c_industriale
this.cb_stock=create cb_stock
this.dw_det_bol_acq_det_1=create dw_det_bol_acq_det_1
this.cb_ric_bol_ven=create cb_ric_bol_ven
this.dw_det_bol_acq_det_2=create dw_det_bol_acq_det_2
this.dw_folder=create dw_folder
this.dw_det_bol_acq_lista=create dw_det_bol_acq_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_des_mov
this.Control[iCurrent+2]=this.st_1
this.Control[iCurrent+3]=this.cb_prezzo
this.Control[iCurrent+4]=this.cb_sconti
this.Control[iCurrent+5]=this.cb_prodotti_note_ricerca
this.Control[iCurrent+6]=this.pb_prod_view
this.Control[iCurrent+7]=this.cb_corrispondenze
this.Control[iCurrent+8]=this.cb_c_industriale
this.Control[iCurrent+9]=this.cb_stock
this.Control[iCurrent+10]=this.dw_det_bol_acq_det_1
this.Control[iCurrent+11]=this.cb_ric_bol_ven
this.Control[iCurrent+12]=this.dw_det_bol_acq_det_2
this.Control[iCurrent+13]=this.dw_folder
this.Control[iCurrent+14]=this.dw_det_bol_acq_lista
end on

on w_det_bol_acq.destroy
call super::destroy
destroy(this.cb_des_mov)
destroy(this.st_1)
destroy(this.cb_prezzo)
destroy(this.cb_sconti)
destroy(this.cb_prodotti_note_ricerca)
destroy(this.pb_prod_view)
destroy(this.cb_corrispondenze)
destroy(this.cb_c_industriale)
destroy(this.cb_stock)
destroy(this.dw_det_bol_acq_det_1)
destroy(this.cb_ric_bol_ven)
destroy(this.dw_det_bol_acq_det_2)
destroy(this.dw_folder)
destroy(this.dw_det_bol_acq_lista)
end on

event pc_delete;call super::pc_delete;long ll_i

if dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "flag_movimenti") = "S" then
   g_mb.messagebox("Attenzione", "Bolla non cancellabile! E' già stata confermata.", &
              exclamation!, ok!)
   dw_det_bol_acq_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

if dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Bolla non cancellabile! E' stata bloccata.", &
              exclamation!, ok!)
   dw_det_bol_acq_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

for ll_i = 1 to dw_det_bol_acq_lista.deletedcount()
	if dw_det_bol_acq_lista.getitemstring(ll_i, "flag_blocco", delete!, true) = "S" then
		g_mb.messagebox("Attenzione", "Bolla non cancellabile! E' stata bloccata.", &
					  exclamation!, ok!)
		dw_det_bol_acq_lista.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
	
	if dw_det_bol_acq_lista.getitemnumber(ll_i, "quan_fatturata", delete!, true) > 0 then
		g_mb.messagebox("Attenzione", "Bolla non cancellabile! E' stata fatturata.", &
					  exclamation!, ok!)
		dw_det_bol_acq_lista.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
next

cb_prezzo.enabled=false
cb_c_industriale.enabled = false
cb_corrispondenze.enabled = false
cb_des_mov.enabled = false

end event

event pc_modify;call super::pc_modify;if dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "flag_movimenti") = "S" then
   g_mb.messagebox("Attenzione", "Bolla non modificabile! E' già stata confermata.", &
              exclamation!, ok!)
   dw_det_bol_acq_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

if dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Bolla non modificabile! E' stata bloccata.", &
              exclamation!, ok!)
   dw_det_bol_acq_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

if dw_det_bol_acq_lista.getitemstring(dw_det_bol_acq_lista.getrow(), "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Bolla non modificabile! E' stata bloccata.", &
              exclamation!, ok!)
   dw_det_bol_acq_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

if dw_det_bol_acq_lista.getitemnumber(dw_det_bol_acq_lista.getrow(), "quan_fatturata") > 0 then
   g_mb.messagebox("Attenzione", "Bolla non modificabile! E' stata fatturata.", &
              exclamation!, ok!)
   dw_det_bol_acq_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

end event

event pc_new;call super::pc_new;if dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "flag_movimenti") = "S" then
   g_mb.messagebox("Attenzione", "Bolla non modificabile! E' già stata confermata.", &
              exclamation!, ok!)
   dw_det_bol_acq_lista.set_dw_view(c_ignorechanges)
   dw_det_bol_acq_det_1.set_dw_view(c_ignorechanges)
   dw_det_bol_acq_det_2.set_dw_view(c_ignorechanges)
//   dw_det_bol_acq_det_3.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

if dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Bolla non modificabile! E' stata bloccata.", &
              exclamation!, ok!)
   dw_det_bol_acq_lista.set_dw_view(c_ignorechanges)
   dw_det_bol_acq_det_1.set_dw_view(c_ignorechanges)
   dw_det_bol_acq_det_2.set_dw_view(c_ignorechanges)
//   dw_det_bol_acq_det_3.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if
end event

type cb_des_mov from commandbutton within w_det_bol_acq
integer x = 1541
integer y = 1180
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Dest. Mov."
end type

event clicked;long ll_anno_reg_des_mov, ll_num_reg_des_mov
string ls_cod_prodotto

ll_anno_reg_des_mov = dw_det_bol_acq_det_1.getitemnumber(dw_det_bol_acq_det_1.getrow(), "anno_reg_des_mov")
ll_num_reg_des_mov = dw_det_bol_acq_det_1.getitemnumber(dw_det_bol_acq_det_1.getrow(), "num_reg_des_mov")
ls_cod_prodotto = dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(), "cod_prodotto")

s_cs_xx.parametri.parametro_d_1 = ll_anno_reg_des_mov
s_cs_xx.parametri.parametro_d_2 = ll_num_reg_des_mov
s_cs_xx.parametri.parametro_s_10 = ls_cod_prodotto
window_open(w_dest_mov_magazzino, 0)

end event

type st_1 from statictext within w_det_bol_acq
integer x = 69
integer y = 1200
integer width = 873
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
string text = "SHIFT - F4 sulla lista = Nuova Riga"
boolean focusrectangle = false
end type

type cb_prezzo from commandbutton within w_det_bol_acq
event clicked pbm_bnclicked
integer x = 2720
integer y = 1180
integer width = 361
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Prezzo"
end type

event clicked;dw_det_bol_acq_det_1.accepttext()

//dw_det_bol_acq_det_1.setcolumn(7)
dw_det_bol_acq_det_1.setcolumn(6)
s_cs_xx.parametri.parametro_s_1 = dw_det_bol_acq_det_1.gettext()
//dw_det_bol_acq_det_1.setcolumn(11)
dw_det_bol_acq_det_1.setcolumn(10)
s_cs_xx.parametri.parametro_d_1 = double(dw_det_bol_acq_det_1.gettext())
//dw_det_bol_acq_det_1.setcolumn(12)
dw_det_bol_acq_det_1.setcolumn(11)
s_cs_xx.parametri.parametro_d_2 = double(dw_det_bol_acq_det_1.gettext())
//dw_det_bol_acq_det_1.setcolumn(10)
dw_det_bol_acq_det_1.setcolumn(9)
s_cs_xx.parametri.parametro_d_3 = double(dw_det_bol_acq_det_1.gettext())

window_open(w_prezzo_um, 0)

if s_cs_xx.parametri.parametro_d_1 <> 0 then
   dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(), "prezzo_acquisto", s_cs_xx.parametri.parametro_d_1)
end if

if s_cs_xx.parametri.parametro_d_3 <> 0 then
   dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(), "quan_arrivata", s_cs_xx.parametri.parametro_d_3)
end if
end event

type cb_sconti from commandbutton within w_det_bol_acq
integer x = 3104
integer y = 1180
integer width = 361
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Sconti"
end type

event clicked;s_cs_xx.parametri.parametro_uo_dw_1 = dw_det_bol_acq_lista
window_open(w_sconti, 0)

end event

type cb_prodotti_note_ricerca from cb_prod_note_ricerca within w_det_bol_acq
integer x = 73
integer y = 392
integer height = 80
integer taborder = 20
boolean bringtotop = true
end type

event getfocus;call super::getfocus;s_cs_xx.parametri.parametro_s_1 = dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_prodotto")
dw_det_bol_acq_det_2.change_dw_current()
end event

type pb_prod_view from cb_listview within w_det_bol_acq
integer x = 2354
integer y = 240
integer width = 73
integer height = 80
integer taborder = 10
boolean bringtotop = true
end type

event clicked;call super::clicked;dw_det_bol_acq_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end event

type cb_corrispondenze from commandbutton within w_det_bol_acq
event clicked pbm_bnclicked
integer x = 1938
integer y = 1180
integer width = 361
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corrispond."
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "Bol_Acq"
window_open_parm(w_det_acq_ven_corr, -1, dw_det_bol_acq_lista)


end event

type cb_c_industriale from commandbutton within w_det_bol_acq
event clicked pbm_bnclicked
integer x = 2327
integer y = 1180
integer width = 361
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C.Industriale"
end type

event clicked;window_open_parm(w_det_bol_acq_stat,-1,dw_det_bol_acq_lista)
end event

type cb_stock from cb_stock_ricerca within w_det_bol_acq
event clicked pbm_bnclicked
integer x = 1829
integer y = 864
integer width = 73
integer height = 80
integer taborder = 2
end type

event clicked;call super::clicked;s_cs_xx.parametri.parametro_s_10 = dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0

dw_det_bol_acq_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"
setnull(s_cs_xx.parametri.parametro_s_7)
setnull(s_cs_xx.parametri.parametro_s_8)
window_open(w_ricerca_stock, 0)
end event

type dw_det_bol_acq_det_1 from uo_cs_xx_dw within w_det_bol_acq
integer x = 46
integer y = 120
integer width = 3447
integer height = 1060
integer taborder = 120
string dataobject = "d_det_bol_acq_det_1"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_cod_tipo_movimento, ls_cod_deposito, ls_messaggio, &
          ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, &
          ls_flag_tipo_det_acq, ls_modify, ls_null, ls_cod_tipo_det_acq, ls_flag_decimali, &
			 ls_cod_misura_mag, ls_nota_prodotto, ls_nota_testata, ls_stampa_piede, ls_nota_video
   double ld_fat_conversione, ld_quantita, ld_cambio_acq
   datetime ldt_data_bolla, ldt_data_esenzione_iva, ldt_null
	long ll_riga_origine, ll_i, ll_null

   setnull(ls_null)
   setnull(ll_null)
   setnull(ldt_null)

   ls_cod_tipo_listino_prodotto = dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
   ldt_data_bolla = dw_det_bol_acq_lista.i_parentdw.getitemdatetime(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "data_bolla")
   ls_cod_fornitore = dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")
   ls_cod_valuta = dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
   ld_cambio_acq = dw_det_bol_acq_lista.i_parentdw.getitemnumber(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "cambio_acq")
   ls_cod_tipo_det_acq = this.getitemstring(this.getrow(),"cod_tipo_det_acq")
   
	
   choose case i_colname
		case "prog_riga_bol_acq"
			ll_riga_origine = this.getrow()
			for ll_i = 1 to this.rowcount()
				if ll_i <> ll_riga_origine and &
					long(i_coltext) = this.getitemnumber(ll_i, "prog_riga_bol_acq") then
	            g_mb.messagebox("Attenzione", "Progressivo riga già utilizzato.", &
   	                     exclamation!, ok!)
               i_coltext = string(this.getitemnumber(ll_riga_origine, "prog_riga_bol_acq", primary!,true))
               this.setitem(ll_riga_origine, "prog_riga_bol_acq", double(i_coltext))
               this.settext(i_coltext)
               return 2
					pcca.error = c_fatal
				end if
			next
      case "cod_tipo_det_acq"
         ls_modify = "cod_prodotto.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "cod_prodotto.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "cod_misura.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "cod_misura.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "fat_conversione.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "fat_conversione.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "quan_arrivata.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "quan_arrivata.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "prezzo_acquisto.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "prezzo_acquisto.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_1.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_1.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_2.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_2.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "cod_iva.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "cod_iva.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_deposito.protect='0'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_deposito.background.color='16777215'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_ubicazione.protect='0'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_ubicazione.background.color='16777215'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_lotto.protect='0'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_lotto.background.color='16777215'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_tipo_movimento.protect='0'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_tipo_movimento.background.color='16777215'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "flag_blocco.protect='0'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "flag_blocco.background.color='16777215'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "flag_saldo.protect='0'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "flag_saldo.background.color='16777215'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			
			select flag_tipo_det_acq,
					 cod_tipo_movimento
			into   :ls_flag_tipo_det_acq,
					 :ls_cod_tipo_movimento
			from   tab_tipi_det_acq
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_tipo_det_acq = :i_coltext;
					 
		
			if sqlca.sqlcode = -1 then
				
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
							  exclamation!, ok!)
			end if
			
			
			if ls_flag_tipo_det_acq = "M" then
				this.setitem(this.getrow(), "cod_deposito", ls_cod_deposito)
				this.setitem(this.getrow(), "cod_tipo_movimento", ls_cod_tipo_movimento)
				cb_stock.enabled = true
				if f_proponi_stock_acq (i_coltext, this, row, ls_messaggio ) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
				end if
			else
				cb_stock.enabled = false
				if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_tipo_movimento")) then
					dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_tipo_movimento",ls_null)
				end if
				if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_deposito")) then
					dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_deposito",ls_null)
				end if
				if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_ubicazione")) then
					dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_ubicazione",ls_null)
				end if
				if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_lotto")) then
					dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_lotto",ls_null)
				end if
				if not isnull(dw_det_bol_acq_det_1.getitemdatetime(dw_det_bol_acq_det_1.getrow(),"data_stock")) then
					dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"data_stock", ldt_null)
				end if
				if not isnull(dw_det_bol_acq_det_1.getitemnumber(dw_det_bol_acq_det_1.getrow(),"prog_stock")) then
					dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"prog_stock",ll_null)
				end if
		
				ls_modify = "cod_tipo_movimento.protect='1'~t"
				ls_modify = ls_modify + "cod_tipo_movimento.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_deposito.protect='1'~t"
				ls_modify = ls_modify + "cod_deposito.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_ubicazione.protect='1'~t"
				ls_modify = ls_modify + "cod_ubicazione.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_lotto.protect='1'~t"
				ls_modify = ls_modify + "cod_lotto.background.color='12632256'~t"
				dw_det_bol_acq_det_1.modify(ls_modify)
			end if

         ld_datawindow = dw_det_bol_acq_lista
         f_tipo_dettaglio_acq_lista(ld_datawindow, i_coltext)			

         ld_datawindow = dw_det_bol_acq_det_1
			lp_prod_view = pb_prod_view
			setnull(lc_prodotti_ricerca)
         f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext)
         if isnull(this.getitemstring(this.getrow(),"cod_prodotto")) then
            cb_prodotti_note_ricerca.enabled=false
         end if

         if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
            select tab_tipi_det_acq.cod_iva  
            into   :ls_cod_iva  
            from   tab_tipi_det_acq  
            where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
                   tab_tipi_det_acq.cod_tipo_det_acq = :i_coltext;
            if sqlca.sqlcode = 0 then
               this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
            end if
         end if
			dw_det_bol_acq_det_2.setitem(dw_det_bol_acq_det_2.getrow(), "nota_dettaglio", ls_null)
			
      case "cod_prodotto"
		if guo_functions.uof_get_note_fisse(ls_null, ls_null, i_coltext, "BOL_ACQ", ls_null, ldt_data_bolla, ls_nota_testata, ls_stampa_piede, ls_nota_video) < 0 then
			g_mb.error(ls_nota_testata)
		else
			if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
		end if
		
		if event ue_check_prodotto_bloccato(ldt_data_bolla,row, dwo, data) > 0 then return 1

         select anag_prodotti.cod_misura_mag,
					 anag_prodotti.cod_misura_acq,
                anag_prodotti.fat_conversione_acq,
                anag_prodotti.flag_decimali,
                anag_prodotti.cod_iva
         into   :ls_cod_misura_mag,
					 :ls_cod_misura,
                :ld_fat_conversione,
                :ls_flag_decimali,
                :ls_cod_iva
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :i_coltext;
   
         if sqlca.sqlcode = 0 then
            this.setitem(i_rownbr, "cod_misura", ls_cod_misura)

				dw_det_bol_acq_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
				dw_det_bol_acq_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")

            if ld_fat_conversione <> 0 then
               this.setitem(i_rownbr, "fat_conversione", ld_fat_conversione)
            else
               this.setitem(i_rownbr, "fat_conversione", 1)
            end if
            this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
				if f_proponi_stock_acq (this.getitemstring(row,"cod_tipo_det_acq"), this, row, ls_messaggio ) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
				end if
			else
				dw_det_bol_acq_det_1.modify("st_prezzo.text='Prezzo:'")
				dw_det_bol_acq_det_1.modify("st_quantita.text='Quantità:'")
			end if

			if not isnull(this.getitemstring(i_rownbr, "cod_misura")) then
				cb_prezzo.text= "Prezzo al " + this.getitemstring(i_rownbr, "cod_misura")
			else
				cb_prezzo.text= "Prezzo"
			end if

			if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_bol_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(ld_fat_conversione)) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
			else
				dw_det_bol_acq_det_1.modify("st_fattore_conv.text=''")
			end if
			
         if not isnull(ls_cod_fornitore) then
            select anag_fornitori.cod_iva,
                   anag_fornitori.data_esenzione_iva
            into   :ls_cod_iva,
                   :ldt_data_esenzione_iva
            from   anag_fornitori
            where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                   anag_fornitori.cod_fornitore = :ls_cod_fornitore;
         end if

         if sqlca.sqlcode = 0 then
            if ls_cod_iva <> "" and &
               (ldt_data_esenzione_iva <= ldt_data_bolla or isnull(ldt_data_esenzione_iva)) then
               this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
            end if
         end if

         ls_cod_prodotto = i_coltext
         ld_quantita = this.getitemnumber(i_rownbr, "quan_arrivata")
         if ls_flag_decimali = "N" and &
            ld_quantita - int(ld_quantita) > 0 then
            ld_quantita = ceiling(ld_quantita)
            this.setitem(i_rownbr, "quan_arrivata", ld_quantita)
         end if
			
			change_dw_current()

         f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_bolla, ls_cod_tipo_det_acq)

         if not isnull(i_coltext) then
            cb_prodotti_note_ricerca.enabled = true
         else
            cb_prodotti_note_ricerca.enabled = false
         end if
			select  anag_prodotti_note.nota_prodotto  
			into    :ls_nota_prodotto  
			from    anag_prodotti_note  
			where ( anag_prodotti_note.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         		( anag_prodotti_note.cod_prodotto = :ls_cod_prodotto ) AND  
         		( anag_prodotti_note.cod_nota_prodotto = 
							 ( select parametri_azienda.stringa  
								from parametri_azienda  
								where (parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda ) AND  
								      (parametri_azienda.flag_parametro = 'S' ) AND  
                              (parametri_azienda.cod_parametro = 'CNP' )  )) ;
			if sqlca.sqlcode = 0 then
				dw_det_bol_acq_det_2.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
				dw_det_bol_acq_det_2.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if
		case "cod_misura"
			if not isnull(i_coltext) and i_coltext <> "" then
	   		cb_prezzo.text = "Prezzo al " + i_coltext
			else
				cb_prezzo.text = "Prezzo"
			end if
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.cod_misura_mag
         into   :ls_cod_misura_mag
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> i_coltext and len(trim(i_coltext)) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_bol_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + i_coltext + ")'")
			else
				dw_det_bol_acq_det_1.modify("st_fattore_conv.text=''")
			end if
		case "fat_conversione"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.cod_misura_mag
         into   :ls_cod_misura_mag
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> this.getitemstring(i_rownbr, "cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_bol_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + i_coltext + " " + this.getitemstring(i_rownbr, "cod_misura") + ")'")
			else
				dw_det_bol_acq_det_1.modify("st_fattore_conv.text=''")
			end if
		case "quan_arrivata"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.flag_decimali
         into   :ls_flag_decimali
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
         if sqlca.sqlcode = 0 then
            if ls_flag_decimali = "N" and &
               (double(i_coltext) - int(double(i_coltext))) > 0 then
               i_coltext = string(ceiling(double(i_coltext)))
               this.setitem(i_rownbr, "quan_arrivata", double(i_coltext))
               this.settext(i_coltext)
               return 2
            end if
         end if

         ld_quantita = double(i_coltext)
         f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_bolla, ls_cod_tipo_det_acq)
   end choose
end if
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_modify, ls_flag_tipo_det_acq, ls_cod_tipo_det_acq, &
       ls_null, ls_match, ls_match_1


setnull(ls_null)
if i_rownbr > 0 then
   ls_cod_tipo_det_acq = this.getitemstring(i_rownbr,"cod_tipo_det_acq")

   select tab_tipi_det_acq.flag_tipo_det_acq
   into   :ls_flag_tipo_det_acq
   from   tab_tipi_det_acq
   where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio acqdita.", &
                 exclamation!, ok!)
      return
   end if

   if i_insave > 0 then
      if ls_flag_tipo_det_acq = "M" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("cod_misura")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Unità di misura obbligatoria.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("quan_consegnata")
         if integer(get_col_data(i_rownbr, i_colnbr)) = 0 then
          	g_mb.messagebox("Attenzione", "Quantità bolla obbligatoria.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      elseif ls_flag_tipo_det_acq = "C" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      end if
   end if
end if
end event

event pcd_delete;
//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Delete
//  Description   : Deletes one or more rows from this DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_DeleteOk,    l_AskOk
INTEGER       l_Answer,      l_Idx
UNSIGNEDLONG  l_MBICode,     l_RefreshCmd
LONG          l_NumSelected, l_SelectedRows[]
DWITEMSTATUS  l_ItemStatus

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Delete, c_Show)

//----------
//  If this DataWindow is not use or is in "QUERY" mode, don't
//  allow deletes.
//----------

IF NOT i_InUse OR i_DWState = c_DWStateQuery THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Assume that we will have to ask the user if it is Ok to
//  delete the row.
//----------

l_AskOk = TRUE

//----------
//  Find the rows that are to be deleted.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  Make sure that deleting is allowed in this window.
//----------

l_DeleteOk = i_AllowDelete

//----------
//  PowerClass allows the user to delete new records that they
//  have inserted even if the DataWindow does not support delete.
//  We check for this case by:
//     a) Making sure that the user is allowed to add rows AND
//     b) Making sure there are New! rows in this DataWindow
//        (i.e. i_DoSetKey is TRUE) AND
//     c) Making sure there is at least one row selected AND
//     d) Making sure that there are not any retrievable
//        rows (i.e. if the rows are retrievable, they are
//        not New!).
//----------

IF i_AllowNew                     THEN
IF i_SharePrimary.i_ShareDoSetKey THEN
IF i_ShowRow > 0                  THEN
IF i_NumSelected = 0              THEN

   l_DeleteOk = TRUE

   //----------
   //  If the New! rows have not been modified, we won't ask the
   //  user about them.
   //----------

   IF Len(GetText()) = 0 OR Describe(GetColumnName() + ".Initial") = GetText() THEN
      l_AskOk = FALSE

      FOR l_Idx = 1 TO l_NumSelected
         l_ItemStatus = &
            GetItemStatus(l_SelectedRows[l_Idx], 0, Primary!)
         IF l_ItemStatus <> New! THEN
            l_AskOk = TRUE
            EXIT
         END IF
      NEXT
   END IF
END IF
END IF
END IF
END IF

//----------
//  If this DataWindow does not allow delete, tell the user and
//  exit the event.
//----------

IF NOT l_DeleteOk THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_DeleteNotAllowed, &
                      0, PCCA.MB.i_MB_Numbers[],         &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDelete, &
                      0, PCCA.MB.i_MB_Numbers[],     &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Make sure that changes have been saved to the database.
//----------

is_EventControl.Check_Cur_Instance = TRUE
is_EventControl.Only_Do_Children   = TRUE
Check_Save(l_RefreshCmd)

//----------
//  If the user canceled or there was an error during the save
//  process, we do not allow the delete to happen.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  If there were changes, Check_Save() may have indicated that
//  the hierarchy of DataWindows need to be refreshed.  Refresh
//  the entire hierarchy except for the children of this
//  DataWindow.  The rows in the children of this DataWindow are
//  going to be deleted so there is not need to refresh them.
//----------

IF l_RefreshCmd <> c_RefreshUndefined THEN
   i_RootDW.is_EventControl.Refresh_Cmd            = l_RefreshCmd
   i_RootDW.is_EventControl.Skip_DW_Children_Valid = TRUE
   i_RootDW.is_EventControl.Skip_DW_Children       = THIS
   i_RootDW.TriggerEvent("pcd_Refresh")
END IF

//----------
//  If there was an error during the refresh, exit the event.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  pcd_Refresh may have retrieved new rows.  Make sure there are
//  still rows to delete.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDeleteAfterSave, &
                      0, PCCA.MB.i_MB_Numbers[],              &
                      5, PCCA.MB.i_MB_Strings[])

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Only_Do_Children = TRUE
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Verify with the user that it is Ok to delete.
//----------

IF l_AskOk THEN
   IF l_NumSelected = 1 THEN
      l_MBICode = PCCA.MB.c_MBI_DW_OneAskDeleteOk
   ELSE
      l_MBICode = PCCA.MB.c_MBI_DW_AskDeleteOk
   END IF

   PCCA.MB.i_MB_Numbers[1] = l_NumSelected
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   l_Answer             = PCCA.MB.fu_MessageBox          &
                             (l_MBICode,              &
                              1, PCCA.MB.i_MB_Numbers[], &
                              5, PCCA.MB.i_MB_Strings[])
ELSE
   l_Answer = 0
END IF

//----------
//  If l_Answer is 0, we have a pure New! row.  Otherwise, if
//  l_Answer is not 1, the user indicated that they do not want
//  to delete the rows.
//----------


//-------------------------------------------------------------------------------------------------------

long ll_anno_registrazione, ll_num_registrazione, ll_progressivo

ll_anno_registrazione = this.getitemnumber(dw_det_bol_acq_lista.getrow(), "anno_bolla_acq")
ll_num_registrazione = this.getitemnumber(dw_det_bol_acq_lista.getrow(), "num_bolla_acq")
ll_progressivo = this.getitemnumber(dw_det_bol_acq_lista.getrow(), "prog_riga_bolla_acq")

delete from det_bol_acq_stat
where cod_azienda = :s_cs_xx.cod_azienda
  and anno_bolla_acq = :ll_anno_registrazione
  and num_bolla_acq = :ll_num_registrazione
  and prog_riga_bolla_acq = :ll_progressivo;
  
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "errore in cancellazione dati da tabella DET_FAT_ACQ_STAT.~n~r" + sqlca.sqlerrtext)
	return
end if	

//-------------------------------------------------------------------------------------------------------




IF l_Answer <> 0 AND l_Answer <> 1 THEN

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      is_EventControl.Only_Do_Children = TRUE
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If we get to here, we know that we can delete the rows.  Let
//  Delete_DW_Rows() take care of the work.
//----------

Delete_DW_Rows(l_NumSelected,   l_SelectedRows[], &
               c_IgnoreChanges, c_RefreshChildren)

Finished:

//----------
//  Remove the delete prompt.
//----------

PCCA.MDI.fu_Pop()

i_ExtendMode = i_InUse
end event

event ue_key;call super::ue_key;choose case this.getcolumnname()
	case "prezzo_acquisto"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_8 = getitemstring(getrow(),"cod_prodotto")
			s_cs_xx.parametri.parametro_s_9 = dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.getrow(), "cod_fornitore")
			s_cs_xx.parametri.parametro_s_10 = "B"
			//-CLAUDIA 23/07/07 MIGLIERAMENTO PERCHè SI POSSA RICERCARE PER DESCRIZIONE
			s_cs_xx.parametri.parametro_s_12 = '%'+Left (getitemstring(getrow(),"des_prodotto"), 20)+'%'
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_uo_dw_1 = this
			if isvalid(w_det_ord_acq_storico) then
				w_det_ord_acq_storico.show()
			else
				window_open(w_det_ord_acq_storico, 0)
			end if
		end if
end choose
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_det_bol_acq_det_1,"cod_prodotto")
end choose
end event

event clicked;call super::clicked;long								ll_num_mov, ll_row
integer							li_anno_mov
s_cs_xx_parametri				lstr_param
string								ls_anno_mov_col, ls_num_mov_col
w_mov_magazzino_tv		lw_window

choose case dwo.name
	case "p_mov", "p_mov_terzista"
		//apri la window dei movimenti di magazzino
		ll_row = dw_det_bol_acq_det_1.getrow()
		
		if ll_row>0 then
			
			if dwo.name = "p_mov" then
				ls_anno_mov_col = "anno_registrazione_mov_mag"
				ls_num_mov_col = "num_registrazione_mov_mag"
			else
				ls_anno_mov_col = "anno_reg_mov_mag_terzista"
				ls_num_mov_col = "num_reg_mov_mag_terzista"
			end if
			
			li_anno_mov = dw_det_bol_acq_det_1.getitemnumber(ll_row, ls_anno_mov_col)
			ll_num_mov = dw_det_bol_acq_det_1.getitemnumber(ll_row, ls_num_mov_col)
				
			if li_anno_mov>0 and ll_num_mov>0 then
				lstr_param.parametro_ul_1 = li_anno_mov
				lstr_param.parametro_ul_2 = ll_num_mov
				
				opensheetwithparm(lw_window, lstr_param, PCCA.MDI_Frame, 6,  Original!)
				//openwithparm(w_mov_magazzino_tv, lstr_param)
			else
				//nessuna movimento associato
				return
			end if
		end if
		
end choose
end event

type cb_ric_bol_ven from commandbutton within w_det_bol_acq
integer x = 1989
integer y = 1100
integer width = 73
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "..."
end type

event clicked;dw_det_bol_acq_det_2.change_dw_current()

s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw

s_cs_xx.parametri.parametro_s_1 = "anno_reg_bol_ven"

s_cs_xx.parametri.parametro_s_2 = "num_reg_bol_ven"

s_cs_xx.parametri.parametro_s_3 = "prog_riga_bol_ven"

window_open(w_ricerca_bolle_ven, 0)

if not ib_update then
	
	long ll_row, ll_null, ll_anno_ven, ll_num_ven, ll_riga_ven, ll_anno_acq, ll_num_acq, ll_riga_acq
	
	ll_row = dw_det_bol_acq_lista.getrow()
	
	ll_anno_acq = dw_det_bol_acq_lista.getitemnumber(ll_row,"anno_bolla_acq")
	
	ll_num_acq = dw_det_bol_acq_lista.getitemnumber(ll_row,"num_bolla_acq")
	
	ll_riga_acq = dw_det_bol_acq_lista.getitemnumber(ll_row,"prog_riga_bolla_acq")
	
	ll_anno_ven = dw_det_bol_acq_lista.getitemnumber(ll_row,"anno_reg_bol_ven")
	
	ll_num_ven = dw_det_bol_acq_lista.getitemnumber(ll_row,"num_reg_bol_ven")
	
	ll_riga_ven = dw_det_bol_acq_lista.getitemnumber(ll_row,"prog_riga_bol_ven")
	
	update det_bol_acq
	set    anno_reg_bol_ven = :ll_anno_ven,
			 num_reg_bol_ven = :ll_num_ven,
			 prog_riga_bol_ven = :ll_riga_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_bolla_acq = :ll_anno_acq and
			 num_bolla_acq = :ll_num_acq and
			 prog_riga_bolla_acq = :ll_riga_acq;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in aggiornamento riga bolla: " + sqlca.sqlerrtext)
		rollback;
		setnull(ll_null)
		dw_det_bol_acq_lista.setitem(ll_row,"anno_reg_bol_ven",ll_null)
		dw_det_bol_acq_lista.setitem(ll_row,"num_reg_bol_ven",ll_null)
		dw_det_bol_acq_lista.setitem(ll_row,"prog_riga_bol_ven",ll_null)
		dw_det_bol_acq_lista.resetupdate()
		return -1
	end if
	
	commit;
	
	dw_det_bol_acq_lista.resetupdate()
	
end if
end event

type dw_det_bol_acq_det_2 from uo_cs_xx_dw within w_det_bol_acq
integer x = 69
integer y = 120
integer width = 3383
integer height = 1080
integer taborder = 50
string dataobject = "d_det_bol_acq_det_2"
boolean border = false
end type

event ue_key;call super::ue_key;string ls_nota_fissa

choose case this.getcolumnname()
	
	case "nota_dettaglio"
	if key = keyF1!  and keyflags = 1 then
		s_cs_xx.parametri.parametro_s_1 = dw_det_bol_acq_det_1.getitemstring(this.getrow(),"cod_prodotto")
		dw_det_bol_acq_det_1.change_dw_current()
		setnull(s_cs_xx.parametri.parametro_s_2)
		window_open(w_prod_note_ricerca, 0)
		if not isnull(s_cs_xx.parametri.parametro_s_2) then
			this.setcolumn("nota_dettaglio")
			this.settext(this.gettext() + "~r~n" + s_cs_xx.parametri.parametro_s_2)
		end if
	end if
		
end choose

end event

type dw_folder from u_folder within w_det_bol_acq
integer x = 18
integer y = 16
integer width = 3493
integer height = 1280
integer taborder = 40
end type

type dw_det_bol_acq_lista from uo_cs_xx_dw within w_det_bol_acq
integer x = 46
integer y = 120
integer width = 3424
integer height = 1060
integer taborder = 30
string dataobject = "d_det_bol_acq_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

event pcd_new;call super::pcd_new;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_acq, ls_cod_tipo_bol_acq, ls_flag_tipo_det_acq, &
          ls_modify, ls_null, ls_cod_fornitore, ls_cod_iva, &
			 ls_cod_tipo_movimento, ls_cod_deposito, ls_cod_ubicazione
   long ll_anno_bolla_acq, ll_prog_riga_bolla_acq, ll_null, ll_num_bolla_acq
   datetime ldt_data_esenzione_iva, ldt_data_bolla, ldt_null
	real lr_progressivo

   setnull(ls_null)
	setnull(ll_null)
	setnull(ldt_null)

   ll_anno_bolla_acq = dw_det_bol_acq_lista.i_parentdw.getitemnumber(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "anno_bolla_acq")
   ll_num_bolla_acq = dw_det_bol_acq_lista.i_parentdw.getitemnumber(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "num_bolla_acq")
   ldt_data_bolla = dw_det_bol_acq_lista.i_parentdw.getitemdatetime(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "data_bolla")
   ls_cod_fornitore = dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")

   select max(det_bol_acq.prog_riga_bolla_acq)
   into   :ll_prog_riga_bolla_acq
   from   det_bol_acq
   where  det_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and
          det_bol_acq.anno_bolla_acq = :ll_anno_bolla_acq and
          det_bol_acq.num_bolla_acq = :ll_num_bolla_acq;

   if isnull(ll_prog_riga_bolla_acq) then
      dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(), "prog_riga_bolla_acq", 10)
   else
      dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(), "prog_riga_bolla_acq", ll_prog_riga_bolla_acq + 10)
   end if

   ls_cod_tipo_bol_acq = dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "cod_tipo_bol_acq")

   select tab_tipi_bol_acq.cod_tipo_det_acq
   into   :ls_cod_tipo_det_acq
   from   tab_tipi_bol_acq
   where  tab_tipi_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_bol_acq.cod_tipo_bol_acq = :ls_cod_tipo_bol_acq;
   
   if sqlca.sqlcode = 0 then
      dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(), "cod_tipo_det_acq", ls_cod_tipo_det_acq)
      if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
         select tab_tipi_det_acq.cod_iva  
         into   :ls_cod_iva  
         from   tab_tipi_det_acq  
         where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
                tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;
         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
         end if
      end if

   else
      ls_cod_tipo_det_acq = ls_null
   end if

   dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(), "quan_arrivata", 1)
   
   if not isnull(ls_cod_fornitore) then
      select anag_fornitori.cod_iva,
             anag_fornitori.data_esenzione_iva
      into   :ls_cod_iva,
             :ldt_data_esenzione_iva
      from   anag_fornitori
      where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
             anag_fornitori.cod_fornitore = :ls_cod_fornitore;
   end if
   
   if sqlca.sqlcode = 0 then
      if ls_cod_iva <> "" and &
         (ldt_data_esenzione_iva <= ldt_data_bolla or isnull(ldt_data_esenzione_iva)) then
         this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
      end if
   end if
   
	
					
	ls_modify = "cod_tipo_det_acq.protect='0'~t"
   dw_det_bol_acq_det_1.modify(ls_modify)
   ls_modify = "cod_tipo_det_acq.background.color='16777215'~t"
   dw_det_bol_acq_det_1.modify(ls_modify)

   if not isnull(ls_cod_tipo_det_acq) then
      dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(), "cod_tipo_det_acq", ls_cod_tipo_det_acq)
      ld_datawindow = dw_det_bol_acq_det_1
		lp_prod_view = pb_prod_view
		setnull(lc_prodotti_ricerca)
		
      f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_acq)
		
//--------------------		aggiunta  ------------------------------------//

 ls_cod_deposito = dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "cod_deposito")
 ls_cod_ubicazione = dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "cod_ubicazione")
	
		select tab_tipi_det_acq.flag_tipo_det_acq,
				 tab_tipi_det_acq.cod_tipo_movimento
		into   :ls_flag_tipo_det_acq,
				 :ls_cod_tipo_movimento
		from   tab_tipi_det_acq
		where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;
	
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
						  exclamation!, ok!)
		end if
	
		if ls_flag_tipo_det_acq = "M" then
			this.setitem(this.getrow(), "cod_deposito", ls_cod_deposito)
			this.setitem(this.getrow(), "cod_ubicazione", ls_cod_ubicazione)
			this.setitem(this.getrow(), "cod_tipo_movimento", ls_cod_tipo_movimento)
			cb_stock.enabled = true
		else
			if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_tipo_movimento")) then
				dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_tipo_movimento",ls_null)
			end if
			if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_deposito")) then
				dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_deposito",ls_null)
			end if
			if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_ubicazione")) then
				dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_ubicazione",ls_null)
			end if
			if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_lotto")) then
				dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_lotto",ls_null)
			end if
			if not isnull(dw_det_bol_acq_det_1.getitemdatetime(dw_det_bol_acq_det_1.getrow(),"data_stock")) then
				dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"data_stock",ldt_null)
			end if
			if not isnull(dw_det_bol_acq_det_1.getitemnumber(dw_det_bol_acq_det_1.getrow(),"progr_stock")) then
				dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"progr_stock",ll_null)
			end if
	
			ls_modify = "cod_tipo_movimento.protect='1'~t"
			ls_modify = ls_modify + "cod_tipo_movimento.background.color='12632256'~t"
			ls_modify = ls_modify + "cod_deposito.protect='1'~t"
			ls_modify = ls_modify + "cod_deposito.background.color='12632256'~t"
			ls_modify = ls_modify + "cod_ubicazione.protect='1'~t"
			ls_modify = ls_modify + "cod_ubicazione.background.color='12632256'~t"
			ls_modify = ls_modify + "cod_lotto.protect='1'~t"
			ls_modify = ls_modify + "cod_lotto.background.color='12632256'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
		end if

//-----------------------      fine  --------------------------------------//
	
    else
      ls_modify = "cod_prodotto.protect='1'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "cod_prodotto.background.color='12632256'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "des_prodotto.protect='1'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "des_prodotto.background.color='12632256'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "cod_misura.protect='1'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "cod_misura.background.color='12632256'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "fat_conversione.protect='1'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "fat_conversione.background.color='12632256'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "quan_arrivata.protect='1'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "quan_arrivata.background.color='12632256'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "prezzo_acquisto.protect='1'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "prezzo_acquisto.background.color='12632256'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_1.protect='1'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_1.background.color='12632256'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_2.protect='1'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_2.background.color='12632256'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "cod_iva.protect='1'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "cod_iva.background.color='12632256'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "cod_deposito.protect='1'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "cod_deposito.background.color='12632256'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "cod_ubicazione.protect='1'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "cod_ubicazione.background.color='12632256'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "cod_lotto.protect='1'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "cod_lotto.background.color='12632256'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "cod_tipo_movimento.protect='1'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "cod_tipo_movimento.background.color='12632256'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "flag_saldo.protect='1'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "flag_saldo.background.color='12632256'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "flag_blocco.protect='1'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "flag_blocco.background.color='12632256'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
   end if

   ib_nuovo = true
   cb_sconti.enabled = true
	cb_prezzo.enabled=true
	cb_stock.enabled=true
	cb_c_industriale.enabled = false
	cb_corrispondenze.enabled = false
	cb_des_mov.enabled = false
	ib_update = true
	
	dw_det_bol_acq_det_2.setitem(dw_det_bol_acq_lista.getrow(), "flag_doc_suc_det", "I")
	dw_det_bol_acq_det_2.setitem(dw_det_bol_acq_lista.getrow(), "flag_st_note_det", "I")

	
end if


end event

event pcd_view;call super::pcd_view;if i_extendmode then
   ib_modifica = false
   ib_nuovo = false
   cb_prodotti_note_ricerca.enabled = false
   dw_det_bol_acq_det_1.object.b_ricerca_prodotto.enabled = false
	pb_prod_view.enabled = false
   cb_sconti.enabled = false
	cb_prezzo.enabled=false
	cb_stock.enabled=false
	ib_update = false

	if this.getrow() > 0 then
		if this.getitemnumber(this.getrow(), "anno_bolla_acq") > 0 then
			cb_c_industriale.enabled=true
			cb_corrispondenze.enabled = true
			cb_des_mov.enabled = true
		end if
	else
		cb_c_industriale.enabled=false
		cb_corrispondenze.enabled = false
		cb_des_mov.enabled = false
	end if
end if
end event

event rowfocuschanged;call super::rowfocuschanged;string ls_cod_fornitore, ls_cod_iva, ls_cod_tipo_det_acq, ls_modify, ls_cod_prodotto, ls_cod_misura_mag, ls_cod_cliente
datawindow ld_datawindow
commandbutton lc_prodotti_ricerca
picturebutton lp_prod_view
datetime ldt_data_bolla, ldt_data_esenzione_iva

if i_extendmode then
	
//   ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")
//   ls_cod_fornitore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fornitore")
//   ldt_data_bolla = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_bolla")

	if this.getrow() > 0 then
		
		ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")
		ls_cod_fornitore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fornitore")
		ldt_data_bolla = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_bolla")
		
		ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")		

		select anag_prodotti.cod_misura_mag
		into :ls_cod_misura_mag
		from anag_prodotti
		where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

		if sqlca.sqlcode = 0 then
				dw_det_bol_acq_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
				dw_det_bol_acq_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")
		else
				dw_det_bol_acq_det_1.modify("st_prezzo.text='Prezzo:'")
				dw_det_bol_acq_det_1.modify("st_quantita.text='Quantità:'")
		end if

		if not isnull(this.getitemstring(this.getrow(),"cod_misura")) then
		   cb_prezzo.text = "Prezzo al " + this.getitemstring(this.getrow(),"cod_misura")
		else
		   cb_prezzo.text = "Prezzo"
		end if
	
		if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
			len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
			len(trim(ls_cod_misura_mag)) <> 0 then
			dw_det_bol_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
		else
			dw_det_bol_acq_det_1.modify("st_fattore_conv.text=''")		
		end if
	
		if not isnull(ls_cod_cliente) then
			select anag_clienti.cod_iva,
					 anag_clienti.data_esenzione_iva
			into   :ls_cod_iva,
					 :ldt_data_esenzione_iva
			from   anag_clienti
			where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_clienti.cod_cliente = :ls_cod_cliente;
		elseif not isnull(ls_cod_fornitore) then
			select anag_fornitori.cod_iva,
					 anag_fornitori.data_esenzione_iva
			into   :ls_cod_iva,
					 :ldt_data_esenzione_iva
			from   anag_fornitori
			where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_fornitori.cod_fornitore = :ls_cod_fornitore;
		end if
	
		if sqlca.sqlcode = 0 then
			if ls_cod_iva <> "" and ldt_data_esenzione_iva <= ldt_data_bolla then
				f_po_loaddddw_dw(dw_det_bol_acq_det_1, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_calcolo = 'N'")
			else
				f_po_loaddddw_dw(dw_det_bol_acq_det_1, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
			end if
		end if
	
		if ib_modifica or ib_nuovo then
			ls_cod_tipo_det_acq = dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(), "cod_tipo_det_acq")
			ld_datawindow = dw_det_bol_acq_det_1
			lp_prod_view = pb_prod_view
			setnull(lc_prodotti_ricerca)
			f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_acq)
		
			if not isnull(dw_det_bol_acq_lista.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_prodotto")) then
				cb_prodotti_note_ricerca.enabled = true
			else
				cb_prodotti_note_ricerca.enabled = false
			end if
		end if
		
	end if

   if ib_nuovo then
      ls_modify = "cod_tipo_det_acq.protect='0~tif(isrownew(),0,1)'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
      ls_modify = "cod_tipo_det_acq.background.color='16777215~tif(isrownew(),16777215,12632256)'~t"
      dw_det_bol_acq_det_1.modify(ls_modify)
   end if
	
end if
end event

event updatestart;call super::updatestart;string ls_messaggio
long ll_anno_bolla_acq, ll_i, ll_prog_riga_bolla_acq, ll_num_bolla_acq

if i_extendmode then

	ll_anno_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_bolla_acq")
	ll_num_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_bolla_acq")
	
	for ll_i = 1 to this.deletedcount()
		ll_prog_riga_bolla_acq = this.getitemnumber(ll_i, "prog_riga_bolla_acq", delete!, true)
		
		if f_del_bol_acq_det(ll_anno_bolla_acq, ll_num_bolla_acq, ll_prog_riga_bolla_acq, ref ls_messaggio) = -1 then
			g_mb.messagebox("Cancellazione Dettaglio Bolle Acquisto", ls_messaggio)
			return 1
		end if
	next
end if
end event

event updateend;call super::updateend;if rowsinserted + rowsupdated + rowsdeleted > 0 then

	this.postevent("pcd_view")
	
	// Il pulsante non esiste più, è stato inserito all'interno di menu,
	// Codice da cancellare tra 6 mesi, quando nessuno più usarerà la vecchia
	// maschera w_tes_bol_ven_lista
	long   ll_i
	
	if not isvalid(s_cs_xx.parametri.parametro_w_bol_acq) then return 0
	
	for ll_i = 1 to upperbound(s_cs_xx.parametri.parametro_w_bol_acq.control)	
		if s_cs_xx.parametri.parametro_w_bol_acq.control[ll_i].classname() = "cb_calcola" then
			s_cs_xx.parametri.parametro_w_bol_acq.control[ll_i].postevent("clicked")
			exit
		end if
	next
	// ------
	
	if isvalid(s_cs_xx.parametri.parametro_w_bol_acq) then
		s_cs_xx.parametri.parametro_w_bol_acq.postevent("pc_menu_calcola")
	end if
	
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_bolla_acq, ll_num_bolla_acq
real lr_progressivo


ll_anno_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_bolla_acq")
ll_num_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_bolla_acq")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_bolla_acq")) or &
      this.getitemnumber(ll_i, "anno_bolla_acq") = 0 then
      this.setitem(ll_i, "anno_bolla_acq", ll_anno_bolla_acq)
   end if
   if isnull(this.getitemnumber(ll_i, "num_bolla_acq")) then
      this.setitem(ll_i, "num_bolla_acq", ll_num_bolla_acq)
   end if
next

end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_anno_bolla_acq, ll_num_bolla_acq
real lr_progressivo


ll_anno_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_bolla_acq")
ll_num_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_bolla_acq")

parent.title = "Righe Bolle Entrata: " + string(ll_anno_bolla_acq) + "/" + string(ll_num_bolla_acq)

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_bolla_acq, ll_num_bolla_acq)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_acq, ls_modify, &
			 ls_flag_tipo_det_acq, ls_cod_deposito, ls_null
	datetime ldt_null
	long ll_null


	setnull(ls_null)
	setnull(ldt_null)
	setnull(ll_null)
   ib_modifica = true

   ls_modify = "cod_tipo_det_acq.protect='1'~t"
   ls_modify = ls_modify + "cod_tipo_det_acq.background.color='12632256'~t"
   dw_det_bol_acq_det_1.modify(ls_modify)
   ls_cod_tipo_det_acq = dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(), "cod_tipo_det_acq")
   ld_datawindow = dw_det_bol_acq_det_1
	lp_prod_view = pb_prod_view
	setnull(lc_prodotti_ricerca)
   f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_acq)
   ls_cod_deposito = dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "cod_deposito")

	select tab_tipi_det_acq.flag_tipo_det_acq 
	into   :ls_flag_tipo_det_acq
	from   tab_tipi_det_acq
	where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
					  exclamation!, ok!)
	end if

	if ls_flag_tipo_det_acq = "M" then
		cb_stock.enabled = true
		ls_modify = "cod_tipo_movimento.protect='0'~t"
		ls_modify = ls_modify + "cod_tipo_movimento.background.color='16777215'~t"
		ls_modify = ls_modify + "cod_deposito.protect='0'~t"
		ls_modify = ls_modify + "cod_deposito.background.color='16777215'~t"
		ls_modify = ls_modify + "cod_ubicazione.protect='0'~t"
		ls_modify = ls_modify + "cod_ubicazione.background.color='16777215'~t"
		ls_modify = ls_modify + "cod_lotto.protect='0'~t"
		ls_modify = ls_modify + "cod_lotto.background.color='16777215'~t"
		dw_det_bol_acq_det_1.modify(ls_modify)
	else
	   if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_tipo_movimento")) then
   	   dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_tipo_movimento",ls_null)
	   end if
	   if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_deposito")) then
   	   dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_deposito",ls_null)
	   end if
	   if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_ubicazione")) then
   	   dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_ubicazione",ls_null)
	   end if
	   if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_lotto")) then
   	   dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_lotto",ls_null)
	   end if
	   if not isnull(dw_det_bol_acq_det_1.getitemdatetime(dw_det_bol_acq_det_1.getrow(),"data_stock")) then
   	   dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"data_stock",ldt_null)
	   end if
	   if not isnull(dw_det_bol_acq_det_1.getitemnumber(dw_det_bol_acq_det_1.getrow(),"prog_stock")) then
   	   dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"prog_stock",ll_null)
	   end if

		ls_modify = "cod_tipo_movimento.protect='1'~t"
		ls_modify = ls_modify + "cod_tipo_movimento.background.color='12632256'~t"
		ls_modify = ls_modify + "cod_deposito.protect='1'~t"
		ls_modify = ls_modify + "cod_deposito.background.color='12632256'~t"
		ls_modify = ls_modify + "cod_ubicazione.protect='1'~t"
		ls_modify = ls_modify + "cod_ubicazione.background.color='12632256'~t"
		ls_modify = ls_modify + "cod_lotto.protect='1'~t"
		ls_modify = ls_modify + "cod_lotto.background.color='12632256'~t"
		dw_det_bol_acq_det_1.modify(ls_modify)
	end if

   if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_prodotto")) then
      cb_prodotti_note_ricerca.enabled = true
   else
      cb_prodotti_note_ricerca.enabled = false
   end if
   cb_sconti.enabled = true
	cb_prezzo.enabled=true
	cb_stock.enabled=true
	cb_c_industriale.enabled = false
	cb_corrispondenze.enabled = false
	cb_des_mov.enabled = false
	ib_update = true
end if


end event

event pcd_save;call super::pcd_save;if i_extendmode then
	long ll_anno_bolla_acq, ll_i, ll_i1,ll_prog_riga_bolla_acq, ll_num_bolla_acq
	string  ls_cod_tipo_analisi, ls_cod_prodotto, ls_cod_tipo_bol_acq, ls_cod_tipo_det_acq, &
			  ls_test
	integer li_risposta
	real	  lr_progressivo

	ll_i = i_parentdw.i_selectedrows[1]
	ll_i1 = getrow()

	ll_anno_bolla_acq = i_parentdw.getitemnumber(ll_i, "anno_bolla_acq")
	ll_num_bolla_acq = i_parentdw.getitemnumber(ll_i, "num_bolla_acq")
	ll_prog_riga_bolla_acq = getitemnumber(ll_i1, "prog_riga_bolla_acq")
	ls_cod_tipo_det_acq = getitemstring(ll_i1, "cod_tipo_det_acq")
	ls_cod_tipo_bol_acq = i_parentdw.getitemstring(ll_i, "cod_tipo_bol_acq")
	
	SELECT cod_tipo_analisi
	INTO   :ls_cod_tipo_analisi  
	FROM   tab_tipi_bol_acq
	WHERE  cod_azienda = :s_cs_xx.cod_azienda AND
			 cod_tipo_bol_acq = :ls_cod_tipo_bol_acq;

	select cod_azienda
	into   :ls_test
	from 	 det_bol_acq_stat
	where  cod_azienda=:s_cs_xx.cod_azienda and
			 anno_bolla_acq = :ll_anno_bolla_acq and
			 num_bolla_acq = :ll_num_bolla_acq and
			 prog_riga_bolla_acq = :ll_prog_riga_bolla_acq;

	if sqlca.sqlcode=100 then
		li_risposta = f_crea_distribuzione_bol_acq(ls_cod_tipo_analisi, ls_cod_prodotto, ls_cod_tipo_det_acq, ll_num_bolla_acq, ll_anno_bolla_acq, ll_prog_riga_bolla_acq)
		if li_risposta=-1 then
			g_mb.messagebox("Apice","Attenzione! Si è verificato un errore durante la creazione dei dettagli statistici.",exclamation!)
			pcca.error = c_fatal
		end if
	end if
end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_cod_tipo_movimento, ls_cod_deposito, ls_messaggio, &
          ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, &
          ls_flag_tipo_det_acq, ls_modify, ls_null, ls_cod_tipo_det_acq, ls_flag_decimali, &
			 ls_cod_misura_mag, ls_nota_prodotto, ls_des_prodotto, ls_nota_testata, ls_stampa_piede, ls_nota_video
   double ld_fat_conversione, ld_quantita, ld_cambio_acq
   datetime ldt_data_bolla, ldt_data_esenzione_iva, ldt_null
	long ll_riga_origine, ll_i, ll_null

   setnull(ls_null)
   setnull(ll_null)
   setnull(ldt_null)

   ls_cod_tipo_listino_prodotto = dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
   ldt_data_bolla = dw_det_bol_acq_lista.i_parentdw.getitemdatetime(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "data_bolla")
   ls_cod_fornitore = dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")
   ls_cod_valuta = dw_det_bol_acq_lista.i_parentdw.getitemstring(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
   ld_cambio_acq = dw_det_bol_acq_lista.i_parentdw.getitemnumber(dw_det_bol_acq_lista.i_parentdw.i_selectedrows[1], "cambio_acq")
   ls_cod_tipo_det_acq = this.getitemstring(this.getrow(),"cod_tipo_det_acq")

   choose case i_colname
		case "prog_riga_bol_acq"
			ll_riga_origine = this.getrow()
			for ll_i = 1 to this.rowcount()
				if ll_i <> ll_riga_origine and &
					long(i_coltext) = this.getitemnumber(ll_i, "prog_riga_bol_acq") then
	            g_mb.messagebox("Attenzione", "Progressivo riga già utilizzato.", &
   	                     exclamation!, ok!)
               i_coltext = string(this.getitemnumber(ll_riga_origine, "prog_riga_bol_acq", primary!,true))
               this.setitem(ll_riga_origine, "prog_riga_bol_acq", double(i_coltext))
               this.settext(i_coltext)
               return 2
					pcca.error = c_fatal
				end if
			next
      case "cod_tipo_det_acq"
         ls_modify = "cod_prodotto.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "cod_prodotto.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "cod_misura.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "cod_misura.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "fat_conversione.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "fat_conversione.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "quan_arrivata.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "quan_arrivata.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "prezzo_acquisto.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "prezzo_acquisto.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_1.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_1.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_2.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_2.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "cod_iva.protect='0'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
         ls_modify = "cod_iva.background.color='16777215'~t"
         dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_deposito.protect='0'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_deposito.background.color='16777215'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_ubicazione.protect='0'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_ubicazione.background.color='16777215'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_lotto.protect='0'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_lotto.background.color='16777215'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_tipo_movimento.protect='0'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "cod_tipo_movimento.background.color='16777215'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "flag_blocco.protect='0'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "flag_blocco.background.color='16777215'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "flag_saldo.protect='0'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			ls_modify = "flag_saldo.background.color='16777215'~t"
			dw_det_bol_acq_det_1.modify(ls_modify)
			
			select flag_tipo_det_acq,
					 cod_tipo_movimento
			into   :ls_flag_tipo_det_acq,
					 :ls_cod_tipo_movimento
			from   tab_tipi_det_acq
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_tipo_det_acq = :i_coltext;
					 
		
			if sqlca.sqlcode = -1 then
				
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
							  exclamation!, ok!)
			end if
			
			
			if ls_flag_tipo_det_acq = "M" then
				this.setitem(this.getrow(), "cod_deposito", ls_cod_deposito)
				this.setitem(this.getrow(), "cod_tipo_movimento", ls_cod_tipo_movimento)
				cb_stock.enabled = true
				if f_proponi_stock_acq (i_coltext, this, row, ls_messaggio ) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
				end if
			else
				cb_stock.enabled = false
				if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_tipo_movimento")) then
					dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_tipo_movimento",ls_null)
				end if
				if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_deposito")) then
					dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_deposito",ls_null)
				end if
				if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_ubicazione")) then
					dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_ubicazione",ls_null)
				end if
				if not isnull(dw_det_bol_acq_det_1.getitemstring(dw_det_bol_acq_det_1.getrow(),"cod_lotto")) then
					dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"cod_lotto",ls_null)
				end if
				if not isnull(dw_det_bol_acq_det_1.getitemdatetime(dw_det_bol_acq_det_1.getrow(),"data_stock")) then
					dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"data_stock", ldt_null)
				end if
				if not isnull(dw_det_bol_acq_det_1.getitemnumber(dw_det_bol_acq_det_1.getrow(),"prog_stock")) then
					dw_det_bol_acq_det_1.setitem(dw_det_bol_acq_det_1.getrow(),"prog_stock",ll_null)
				end if
		
				ls_modify = "cod_tipo_movimento.protect='1'~t"
				ls_modify = ls_modify + "cod_tipo_movimento.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_deposito.protect='1'~t"
				ls_modify = ls_modify + "cod_deposito.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_ubicazione.protect='1'~t"
				ls_modify = ls_modify + "cod_ubicazione.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_lotto.protect='1'~t"
				ls_modify = ls_modify + "cod_lotto.background.color='12632256'~t"
				dw_det_bol_acq_det_1.modify(ls_modify)
			end if

         ld_datawindow = dw_det_bol_acq_lista
         f_tipo_dettaglio_acq_lista(ld_datawindow, i_coltext)			

         ld_datawindow = dw_det_bol_acq_det_1
			lp_prod_view = pb_prod_view
			setnull(lc_prodotti_ricerca)
         f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext)
         if isnull(this.getitemstring(this.getrow(),"cod_prodotto")) then
            cb_prodotti_note_ricerca.enabled=false
         end if

         if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
            select tab_tipi_det_acq.cod_iva  
            into   :ls_cod_iva  
            from   tab_tipi_det_acq  
            where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
                   tab_tipi_det_acq.cod_tipo_det_acq = :i_coltext;
            if sqlca.sqlcode = 0 then
               this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
            end if
         end if
			dw_det_bol_acq_det_2.setitem(dw_det_bol_acq_det_2.getrow(), "nota_dettaglio", ls_null)
			
      case "cod_prodotto"
		if guo_functions.uof_get_note_fisse(ls_null, ls_null, i_coltext, "BOL_ACQ", ls_null, ldt_data_bolla, ls_nota_testata, ls_stampa_piede, ls_nota_video) < 0 then
			g_mb.error(ls_nota_testata)
		else
			if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
		end if
		if event ue_check_prodotto_bloccato(ldt_data_bolla,row, dwo, data) > 0 then return 1

         select anag_prodotti.cod_misura_mag,
					 anag_prodotti.cod_misura_acq,
                anag_prodotti.fat_conversione_acq,
                anag_prodotti.flag_decimali,
                anag_prodotti.cod_iva,
					 anag_prodotti.des_prodotto
         into   :ls_cod_misura_mag,
					 :ls_cod_misura,
                :ld_fat_conversione,
                :ls_flag_decimali,
                :ls_cod_iva,
					 :ls_des_prodotto
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :i_coltext;
   
         if sqlca.sqlcode = 0 then
            this.setitem(i_rownbr, "cod_misura", ls_cod_misura_mag)
				this.setitem(i_rownbr, "des_prodotto", ls_des_prodotto)
				dw_det_bol_acq_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
				dw_det_bol_acq_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")

            if ld_fat_conversione <> 0 then
               this.setitem(i_rownbr, "fat_conversione", ld_fat_conversione)
            else
               this.setitem(i_rownbr, "fat_conversione", 1)
            end if
            this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
				
				if f_proponi_stock_acq (this.getitemstring(row,"cod_tipo_det_acq"), this, row, ls_messaggio ) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
				end if
			else
				dw_det_bol_acq_det_1.modify("st_prezzo.text='Prezzo:'")
				dw_det_bol_acq_det_1.modify("st_quantita.text='Quantità:'")
				dw_det_bol_acq_lista.change_dw_current()
				guo_ricerca.uof_ricerca_prodotto(dw_det_bol_acq_lista,"cod_prodotto")	
			end if

			if not isnull(this.getitemstring(i_rownbr, "cod_misura")) then
				cb_prezzo.text= "Prezzo al " + this.getitemstring(i_rownbr, "cod_misura")
			else
				cb_prezzo.text= "Prezzo"
			end if

			if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_bol_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(ld_fat_conversione)) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
			else
				dw_det_bol_acq_det_1.modify("st_fattore_conv.text=''")
			end if
			
         if not isnull(ls_cod_fornitore) then
            select anag_fornitori.cod_iva,
                   anag_fornitori.data_esenzione_iva
            into   :ls_cod_iva,
                   :ldt_data_esenzione_iva
            from   anag_fornitori
            where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                   anag_fornitori.cod_fornitore = :ls_cod_fornitore;
         end if

         if sqlca.sqlcode = 0 then
            if ls_cod_iva <> "" and &
               (ldt_data_esenzione_iva <= ldt_data_bolla or isnull(ldt_data_esenzione_iva)) then
               this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
            end if
         end if

         ls_cod_prodotto = i_coltext
         ld_quantita = this.getitemnumber(i_rownbr, "quan_arrivata")
         if ls_flag_decimali = "N" and &
            ld_quantita - int(ld_quantita) > 0 then
            ld_quantita = ceiling(ld_quantita)
            this.setitem(i_rownbr, "quan_arrivata", ld_quantita)
         end if
			
			change_dw_current()
			
        	f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_bolla, ls_cod_tipo_det_acq)
			  
         if not isnull(i_coltext) then
            cb_prodotti_note_ricerca.enabled = true
         else
            cb_prodotti_note_ricerca.enabled = false
         end if
			select  anag_prodotti_note.nota_prodotto  
			into    :ls_nota_prodotto  
			from    anag_prodotti_note  
			where ( anag_prodotti_note.cod_azienda = :s_cs_xx.cod_azienda ) AND  
         		( anag_prodotti_note.cod_prodotto = :ls_cod_prodotto ) AND  
         		( anag_prodotti_note.cod_nota_prodotto = 
							 ( select parametri_azienda.stringa  
								from parametri_azienda  
								where (parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda ) AND  
								      (parametri_azienda.flag_parametro = 'S' ) AND  
                              (parametri_azienda.cod_parametro = 'CNP' )  )) ;
			if sqlca.sqlcode = 0 then
				dw_det_bol_acq_det_2.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
				dw_det_bol_acq_det_2.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if
			dw_det_bol_acq_lista.change_dw_current()
			this.setcolumn("cod_prodotto")
		case "cod_misura"
			if not isnull(i_coltext) and i_coltext <> "" then
	   		cb_prezzo.text = "Prezzo al " + i_coltext
			else
				cb_prezzo.text = "Prezzo"
			end if
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.cod_misura_mag
         into   :ls_cod_misura_mag
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> i_coltext and len(trim(i_coltext)) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_bol_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + i_coltext + ")'")
			else
				dw_det_bol_acq_det_1.modify("st_fattore_conv.text=''")
			end if
		case "fat_conversione"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.cod_misura_mag
         into   :ls_cod_misura_mag
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> this.getitemstring(i_rownbr, "cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_bol_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + i_coltext + " " + this.getitemstring(i_rownbr, "cod_misura") + ")'")
			else
				dw_det_bol_acq_det_1.modify("st_fattore_conv.text=''")
			end if
		case "quan_arrivata"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.flag_decimali
         into   :ls_flag_decimali
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
         if sqlca.sqlcode = 0 then
            if ls_flag_decimali = "N" and &
               (double(i_coltext) - int(double(i_coltext))) > 0 then
               i_coltext = string(ceiling(double(i_coltext)))
               this.setitem(i_rownbr, "quan_arrivata", double(i_coltext))
               this.settext(i_coltext)
               return 2
            end if
         end if

         ld_quantita = double(i_coltext)
         f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_bolla, ls_cod_tipo_det_acq)
   end choose
end if
end event

event ue_key;call super::ue_key;string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, &
       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_listino, ls_messaggio, &
		 ls_cod_misura, ls_flag_prezzo
long ll_i, ll_y
double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo, ll_sconti[], ll_maggiorazioni[], &
	    ld_min_fat_altezza,ld_min_fat_larghezza,ld_min_fat_profondita,ld_min_fat_superficie,ld_min_fat_volume, &
		 ld_num_confezioni, ld_num_pezzi, ld_importo_conai, ld_importo_conai_scon
datetime ldt_data_registrazione


choose case this.getcolumnname()

	case "cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
//			dw_det_bol_acq_lista.change_dw_current()
//			s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
//			s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
//			s_cs_xx.parametri.parametro_tipo_ricerca = 1
//			if not isvalid(w_prodotti_ricerca) then
//				window_open(w_prodotti_ricerca, 0)
//			end if
//			w_prodotti_ricerca.show()
			guo_ricerca.uof_set_response()
			guo_ricerca.uof_ricerca_prodotto(dw_det_bol_acq_lista, "cod_prodotto")
		end if
		
	case "prezzo_acquisto"	
		if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
			this.postevent("itemchanged")
		end if		

		if key = keyF1!  and keyflags = 1 then
			guo_ricerca.uof_set_response()
			guo_ricerca.uof_ricerca_prodotto(dw_det_bol_acq_lista,"cod_prodotto")	
		end if
end choose				


if key = keyF4! and keyflags = 1 then
	triggerevent("pcd_save")
	postevent("pcd_new")
end if


end event

event pcd_delete;//******************************************************************
//  PC Module     : uo_DW_Main
//  Event         : pcd_Delete
//  Description   : Deletes one or more rows from this DataWindow.
//
//  Change History:
//
//  Date     Person     Description of Change
//  -------- ---------- --------------------------------------------
//
//******************************************************************
//  Copyright ServerLogic 1992-1995.  All Rights Reserved.
//******************************************************************

BOOLEAN       l_DeleteOk,    l_AskOk
INTEGER       l_Answer,      l_Idx
UNSIGNEDLONG  l_MBICode,     l_RefreshCmd
LONG          l_NumSelected, l_SelectedRows[]
DWITEMSTATUS  l_ItemStatus

PCCA.MDI.fu_PushID(PCCA.MDI.c_MDI_DW_Delete, c_Show)

//----------
//  If this DataWindow is not use or is in "QUERY" mode, don't
//  allow deletes.
//----------

IF NOT i_InUse OR i_DWState = c_DWStateQuery THEN
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Assume that we will have to ask the user if it is Ok to
//  delete the row.
//----------

l_AskOk = TRUE

//----------
//  Find the rows that are to be deleted.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  Make sure that deleting is allowed in this window.
//----------

l_DeleteOk = i_AllowDelete

//----------
//  PowerClass allows the user to delete new records that they
//  have inserted even if the DataWindow does not support delete.
//  We check for this case by:
//     a) Making sure that the user is allowed to add rows AND
//     b) Making sure there are New! rows in this DataWindow
//        (i.e. i_DoSetKey is TRUE) AND
//     c) Making sure there is at least one row selected AND
//     d) Making sure that there are not any retrievable
//        rows (i.e. if the rows are retrievable, they are
//        not New!).
//----------

IF i_AllowNew                     THEN
IF i_SharePrimary.i_ShareDoSetKey THEN
IF i_ShowRow > 0                  THEN
IF i_NumSelected = 0              THEN

   l_DeleteOk = TRUE

   //----------
   //  If the New! rows have not been modified, we won't ask the
   //  user about them.
   //----------

   IF Len(GetText()) = 0 OR Describe(GetColumnName() + ".Initial") = GetText() THEN
      l_AskOk = FALSE

      FOR l_Idx = 1 TO l_NumSelected
         l_ItemStatus = &
            GetItemStatus(l_SelectedRows[l_Idx], 0, Primary!)
         IF l_ItemStatus <> New! THEN
            l_AskOk = TRUE
            EXIT
         END IF
      NEXT
   END IF
END IF
END IF
END IF
END IF

//----------
//  If this DataWindow does not allow delete, tell the user and
//  exit the event.
//----------

IF NOT l_DeleteOk THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_DeleteNotAllowed, &
                      0, PCCA.MB.i_MB_Numbers[],         &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDelete, &
                      0, PCCA.MB.i_MB_Numbers[],     &
                      5, PCCA.MB.i_MB_Strings[])
   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Make sure that changes have been saved to the database.
//----------

is_EventControl.Check_Cur_Instance = TRUE
is_EventControl.Only_Do_Children   = TRUE
Check_Save(l_RefreshCmd)

//----------
//  If the user canceled or there was an error during the save
//  process, we do not allow the delete to happen.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  If there were changes, Check_Save() may have indicated that
//  the hierarchy of DataWindows need to be refreshed.  Refresh
//  the entire hierarchy except for the children of this
//  DataWindow.  The rows in the children of this DataWindow are
//  going to be deleted so there is not need to refresh them.
//----------

IF l_RefreshCmd <> c_RefreshUndefined THEN
   i_RootDW.is_EventControl.Refresh_Cmd            = l_RefreshCmd
   i_RootDW.is_EventControl.Skip_DW_Children_Valid = TRUE
   i_RootDW.is_EventControl.Skip_DW_Children       = THIS
   i_RootDW.TriggerEvent("pcd_Refresh")
END IF

//----------
//  If there was an error during the refresh, exit the event.
//----------

IF PCCA.Error <> c_Success THEN
   GOTO Finished
END IF

//----------
//  pcd_Refresh may have retrieved new rows.  Make sure there are
//  still rows to delete.
//----------

l_NumSelected = Get_Selected_Rows(l_SelectedRows[])

//----------
//  If there are no rows to delete, tell the user.
//----------

IF l_NumSelected = 0 THEN
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   PCCA.MB.fu_MessageBox(PCCA.MB.c_MBI_DW_ZeroToDeleteAfterSave, &
                      0, PCCA.MB.i_MB_Numbers[],              &
                      5, PCCA.MB.i_MB_Strings[])

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Only_Do_Children = TRUE
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  Verify with the user that it is Ok to delete.
//----------

IF l_AskOk THEN
   IF l_NumSelected = 1 THEN
      l_MBICode = PCCA.MB.c_MBI_DW_OneAskDeleteOk
   ELSE
      l_MBICode = PCCA.MB.c_MBI_DW_AskDeleteOk
   END IF

   PCCA.MB.i_MB_Numbers[1] = l_NumSelected
   PCCA.MB.i_MB_Strings[1] = i_ObjectType + "::pcd_Delete"
   PCCA.MB.i_MB_Strings[2] = PCCA.Application_Name
   PCCA.MB.i_MB_Strings[3] = i_ClassName
   PCCA.MB.i_MB_Strings[4] = i_Window.Title
   PCCA.MB.i_MB_Strings[5] = DataObject
   l_Answer             = PCCA.MB.fu_MessageBox          &
                             (l_MBICode,              &
                              1, PCCA.MB.i_MB_Numbers[], &
                              5, PCCA.MB.i_MB_Strings[])
ELSE
   l_Answer = 0
END IF

//----------
//  If l_Answer is 0, we have a pure New! row.  Otherwise, if
//  l_Answer is not 1, the user indicated that they do not want
//  to delete the rows.
//----------

//-------------------------------------------------------------------------------------------------------

long ll_anno_registrazione, ll_num_registrazione, ll_progressivo

ll_anno_registrazione = this.getitemnumber(this.getrow(), "anno_bolla_acq")
ll_num_registrazione = this.getitemnumber(this.getrow(), "num_bolla_acq")
ll_progressivo = this.getitemnumber(this.getrow(), "prog_riga_bolla_acq")

delete from det_bol_acq_stat
where cod_azienda = :s_cs_xx.cod_azienda
  and anno_bolla_acq = :ll_anno_registrazione
  and num_bolla_acq = :ll_num_registrazione
  and prog_riga_bolla_acq = :ll_progressivo;
  
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "errore in cancellazione dati da tabella DET_FAT_ACQ_STAT.~n~r" + sqlca.sqlerrtext)
	return
end if	

//-------------------------------------------------------------------------------------------------------

IF l_Answer <> 0 AND l_Answer <> 1 THEN

   //----------
   //  We assumed earlier in this event that the rows in the
   //  children DataWindows were going to be deleted.  However,
   //  we now know that that assumption is false.  Therefore, we
   //  now have to refresh them.
   //----------

   IF l_RefreshCmd <> c_RefreshUndefined THEN
      is_EventControl.Refresh_Cmd      = l_RefreshCmd
      is_EventControl.Only_Do_Children = TRUE
      TriggerEvent("pcd_Refresh")
   END IF

   //----------
   //  Make sure the PCCA.Error indicates failure, remove the
   //  delete prompt, and exit the event.
   //----------

   PCCA.Error = c_Fatal
   GOTO Finished
END IF

//----------
//  If we get to here, we know that we can delete the rows.  Let
//  Delete_DW_Rows() take care of the work.
//----------

Delete_DW_Rows(l_NumSelected,   l_SelectedRows[], &
               c_IgnoreChanges, c_RefreshChildren)

Finished:

//----------
//  Remove the delete prompt.
//----------

PCCA.MDI.fu_Pop()

i_ExtendMode = i_InUse
end event

