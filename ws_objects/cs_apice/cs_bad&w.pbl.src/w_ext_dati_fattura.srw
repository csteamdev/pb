﻿$PBExportHeader$w_ext_dati_fattura.srw
$PBExportComments$Finestra Richiesta Dati Fattura Acquisto
forward
global type w_ext_dati_fattura from w_cs_xx_risposta
end type
type dw_ext_dati_fattura from uo_cs_xx_dw within w_ext_dati_fattura
end type
type cb_1 from commandbutton within w_ext_dati_fattura
end type
type cb_annulla from commandbutton within w_ext_dati_fattura
end type
end forward

global type w_ext_dati_fattura from w_cs_xx_risposta
integer width = 2313
integer height = 704
string title = "Dati Indentificati Bolla Acquisto"
event ue_imposta ( )
dw_ext_dati_fattura dw_ext_dati_fattura
cb_1 cb_1
cb_annulla cb_annulla
end type
global w_ext_dati_fattura w_ext_dati_fattura

event ue_imposta();dw_ext_dati_fattura.setitem(1, "cod_deposito", s_cs_xx.parametri.parametro_s_3)
setnull(s_cs_xx.parametri.parametro_s_3)
end event

on w_ext_dati_fattura.create
int iCurrent
call super::create
this.dw_ext_dati_fattura=create dw_ext_dati_fattura
this.cb_1=create cb_1
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ext_dati_fattura
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_annulla
end on

on w_ext_dati_fattura.destroy
call super::destroy
destroy(this.dw_ext_dati_fattura)
destroy(this.cb_1)
destroy(this.cb_annulla)
end on

event pc_setwindow;call super::pc_setwindow;dw_ext_dati_fattura.set_dw_options(sqlca, &
                                 pcca.null_object,&
											c_nodelete + &
											c_newonopen + &
											c_disablecc, &
											c_default)

if s_cs_xx.parametri.parametro_s_1 = "Bolle_Acq" then
	dw_ext_dati_fattura.object.cod_deposito.protect = 1
	dw_ext_dati_fattura.object.cod_ubicazione.protect = 1
	dw_ext_dati_fattura.object.cod_lotto.protect = 1
elseif s_cs_xx.parametri.parametro_s_1 = "Ordini_Acq" then
	dw_ext_dati_fattura.object.cod_deposito.protect = 0
	dw_ext_dati_fattura.object.cod_ubicazione.protect = 0
	dw_ext_dati_fattura.object.cod_lotto.protect = 0
end if

save_on_close(c_socnosave)

event post ue_imposta()
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_ext_dati_fattura,"cod_deposito",sqlca,&
                 "anag_depositi","cod_deposito","des_deposito", &
                 "anag_depositi.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type dw_ext_dati_fattura from uo_cs_xx_dw within w_ext_dati_fattura
integer x = 23
integer y = 20
integer width = 2240
integer height = 460
integer taborder = 30
string dataobject = "d_ext_dati_fattura"
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;//this.setitem(getrow(), "anno_doc_origine", f_anno_esercizio())
this.setitem(getrow(), "data_registrazione", datetime(today()))
end event

event itemchanged;call super::itemchanged;if i_colname = "flag_conferma" then
	if i_coltext = "N" then
		this.setitem(1, "flag_acc_materiali", "N")
	end if
end if
end event

type cb_1 from commandbutton within w_ext_dati_fattura
integer x = 1897
integer y = 500
integer width = 361
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;dw_ext_dati_fattura.Accepttext()
dw_ext_dati_fattura.setcolumn(2)
dw_ext_dati_fattura.setcolumn(4)

if isnull(dw_ext_dati_fattura.getitemdatetime(1, "data_registrazione")) then
	g_mb.messagebox("Generazione Fattura Acquisto", "Data Registrazione Fattura Obbligatoria", Information!)
	return
end if

if isnull(dw_ext_dati_fattura.getitemstring(1, "cod_deposito")) then
	g_mb.messagebox("Generazione Fattura Acquisto", "Deposito Fattura Obbligatorio", Information!)
	return
end if

s_cs_xx.parametri.parametro_s_1 = dw_ext_dati_fattura.getitemstring(1, "cod_deposito")
s_cs_xx.parametri.parametro_s_2 = dw_ext_dati_fattura.getitemstring(1, "cod_ubicazione")
s_cs_xx.parametri.parametro_s_3 = dw_ext_dati_fattura.getitemstring(1, "cod_lotto")
s_cs_xx.parametri.parametro_s_4 = dw_ext_dati_fattura.getitemstring(1, "flag_conferma")
s_cs_xx.parametri.parametro_s_5 = dw_ext_dati_fattura.getitemstring(1, "flag_acc_materiali")
s_cs_xx.parametri.parametro_s_6 = dw_ext_dati_fattura.getitemstring(1, "flag_calcola")
s_cs_xx.parametri.parametro_data_1 = datetime(dw_ext_dati_fattura.getitemdatetime(1, "data_registrazione"))

close(parent)
end event

type cb_annulla from commandbutton within w_ext_dati_fattura
event clicked pbm_bnclicked
integer x = 1509
integer y = 500
integer width = 361
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;setnull(s_cs_xx.parametri.parametro_s_1) //Deposito
setnull(s_cs_xx.parametri.parametro_s_2) //Ubicazione
setnull(s_cs_xx.parametri.parametro_s_3) //Lotto
setnull(s_cs_xx.parametri.parametro_s_4) //Flag Conferma Automatica
setnull(s_cs_xx.parametri.parametro_s_5) //Flag Accettazione Materiali
setnull(s_cs_xx.parametri.parametro_s_6) //Flag Calcolo Automatico
setnull(s_cs_xx.parametri.parametro_data_1) //Data_Registrazione

close(parent)
end event

