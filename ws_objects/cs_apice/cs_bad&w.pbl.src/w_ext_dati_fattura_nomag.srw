﻿$PBExportHeader$w_ext_dati_fattura_nomag.srw
$PBExportComments$Finestra Richiesta Dati Fattura Acquisto Da Bolle Acquisto Magazzino
forward
global type w_ext_dati_fattura_nomag from w_cs_xx_risposta
end type
type dw_ext_dati_fattura_nomag from uo_cs_xx_dw within w_ext_dati_fattura_nomag
end type
type cb_1 from commandbutton within w_ext_dati_fattura_nomag
end type
type cb_annulla from commandbutton within w_ext_dati_fattura_nomag
end type
end forward

global type w_ext_dati_fattura_nomag from w_cs_xx_risposta
integer width = 2327
integer height = 404
string title = "Generazione Fatture di Acquisto"
dw_ext_dati_fattura_nomag dw_ext_dati_fattura_nomag
cb_1 cb_1
cb_annulla cb_annulla
end type
global w_ext_dati_fattura_nomag w_ext_dati_fattura_nomag

on w_ext_dati_fattura_nomag.create
int iCurrent
call super::create
this.dw_ext_dati_fattura_nomag=create dw_ext_dati_fattura_nomag
this.cb_1=create cb_1
this.cb_annulla=create cb_annulla
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ext_dati_fattura_nomag
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.cb_annulla
end on

on w_ext_dati_fattura_nomag.destroy
call super::destroy
destroy(this.dw_ext_dati_fattura_nomag)
destroy(this.cb_1)
destroy(this.cb_annulla)
end on

event pc_setwindow;call super::pc_setwindow;//dw_ext_dati_fattura_nomag.set_dw_options(sqlca, &
//                                 pcca.null_object,&
//											c_nodelete + &
//											c_newonopen + &
//											c_disablecc, &
//											c_default)
dw_ext_dati_fattura_nomag.set_dw_options(sqlca, &
                                 pcca.null_object,&
											c_nodelete + &
											c_disablecc, &
											c_default)

save_on_close(c_socnosave)

dw_ext_dati_fattura_nomag.postevent("pcd_new")
end event

type dw_ext_dati_fattura_nomag from uo_cs_xx_dw within w_ext_dati_fattura_nomag
integer x = 23
integer y = 20
integer width = 2240
integer height = 160
integer taborder = 30
string dataobject = "d_ext_dati_fattura_nomag"
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;//this.setitem(getrow(), "anno_doc_origine", f_anno_esercizio())
this.setitem(getrow(), "data_registrazione", datetime(today(),00:00:00))
end event

type cb_1 from commandbutton within w_ext_dati_fattura_nomag
integer x = 1897
integer y = 200
integer width = 361
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;dw_ext_dati_fattura_nomag.accepttext()

if isnull(dw_ext_dati_fattura_nomag.getitemdatetime(1, "data_registrazione")) then
	g_mb.messagebox("Generazione Fattura Acquisto", "Data Registrazione Fattura Obbligatoria", Information!)
	return
end if

s_cs_xx.parametri.parametro_s_1 = dw_ext_dati_fattura_nomag.getitemstring(1, "flag_calcolo")
s_cs_xx.parametri.parametro_data_1 = dw_ext_dati_fattura_nomag.getitemdatetime(1, "data_registrazione")

close(parent)
end event

type cb_annulla from commandbutton within w_ext_dati_fattura_nomag
event clicked pbm_bnclicked
integer x = 1509
integer y = 200
integer width = 361
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;setnull(s_cs_xx.parametri.parametro_s_1) //Flag Calcolo Automatico
setnull(s_cs_xx.parametri.parametro_data_1) //Data_Registrazione

close(parent)
end event

