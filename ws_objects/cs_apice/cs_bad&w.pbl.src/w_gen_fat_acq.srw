﻿$PBExportHeader$w_gen_fat_acq.srw
$PBExportComments$Finestra Generazione bolle di acquisto
forward
global type w_gen_fat_acq from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_gen_fat_acq
end type
type cb_ricerca from commandbutton within w_gen_fat_acq
end type
type cb_annulla from commandbutton within w_gen_fat_acq
end type
type sle_filtro_data from u_sle_search within w_gen_fat_acq
end type
type st_filtro_data from statictext within w_gen_fat_acq
end type
type st_1 from statictext within w_gen_fat_acq
end type
type cb_genera_fat from commandbutton within w_gen_fat_acq
end type
type dw_gen_fat_acq_ricerca from u_dw_search within w_gen_fat_acq
end type
type dw_gen_orig_bol_dest_fat from uo_cs_xx_dw within w_gen_fat_acq
end type
type dw_lista_bolle_orig from uo_cs_xx_dw within w_gen_fat_acq
end type
type dw_folder from u_folder within w_gen_fat_acq
end type
end forward

global type w_gen_fat_acq from w_cs_xx_principale
integer width = 2958
integer height = 1384
string title = "Generazione Fatture Acquisto"
cb_1 cb_1
cb_ricerca cb_ricerca
cb_annulla cb_annulla
sle_filtro_data sle_filtro_data
st_filtro_data st_filtro_data
st_1 st_1
cb_genera_fat cb_genera_fat
dw_gen_fat_acq_ricerca dw_gen_fat_acq_ricerca
dw_gen_orig_bol_dest_fat dw_gen_orig_bol_dest_fat
dw_lista_bolle_orig dw_lista_bolle_orig
dw_folder dw_folder
end type
global w_gen_fat_acq w_gen_fat_acq

type variables
string is_cod_fornitore, is_cod_doc_origine, is_numeratore_doc_origine
integer ii_anno_doc_origine
long il_num_doc_origine, il_prog_fattura, il_anno_reg_fat_acq, il_num_reg_fat_acq
end variables

forward prototypes
public function integer wf_carica_dett_fatture (long fl_selected[], ref string fs_messaggio)
public function integer wf_genera_fatture_acq (long fl_selected[], ref string fs_messaggio)
end prototypes

public function integer wf_carica_dett_fatture (long fl_selected[], ref string fs_messaggio);string	ls_cod_tipo_bol_acq, ls_flag_tipo_doc, ls_cod_fornitore[], &
		 ls_cod_tipo_analisi_bol, ls_cod_deposito, ls_lire, ls_cod_tipo_analisi_fat, &
		 ls_cod_tipo_det_acq, ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, &
		 ls_cod_iva, ls_cod_prod_fornitore, ls_cod_valuta, &
		 ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_acq, ls_cod_tipo_movimento, &
		 ls_flag_sola_iva, ls_flag_saldo, ls_cod_cliente[], &
		 ls_referenza, ls_flag_evasione, ls_cod_ubicazione, ls_cod_lotto, ls_cod_prodotto_alt, &
		 ls_des_prodotto_alt

dec{4} ld_cambio_acq, ld_sconto_testata, ld_tot_val_evaso, ld_tot_val_bolla, &
		 ld_sconto_pagamento, ld_quan_fatturata, ld_prezzo_acquisto, ld_fat_conversione_acq, &
		 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_quan_arrivata, ld_val_riga, ld_sconto_4, &
		 ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		 ld_quan_acquisto, ld_val_sconto_1, ld_val_riga_sconto_1, ld_val_sconto_2, &
		 ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, &
		 ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, &
		 ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, ld_val_sconto_8, &
		 ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, &
		 ld_val_riga_sconto_10, ld_val_sconto_testata, ld_val_riga_sconto_testata, &
		 ld_val_sconto_pagamento, ld_val_evaso

integer li_anno_bolla_acq, li_anno_registrazione_mov_mag, li_anno_bolla_acq_old

long ll_progressivo, ll_prog_riga_documento, ll_prog_riga_fat_acq, ll_progressivo_old, &
	  ll_prog_riga_bolla_acq, ll_anno_commessa, ll_num_commessa, ll_num_registrazione_mov_mag, &
	  ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_i, ll_anno_reg_dest_stock, &
	  ll_num_reg_dest_stock, ll_anno_registrazione[], ll_num_registrazione[], ll_prog_stock, ll_num_bolla_acq, &
	  ll_num_bolla_acq_old, ll_anno_ordine_registrazione, ll_num_ordine_registrazione, ll_prog_riga_ordine_acq

datetime ldt_data_stock

datastore lds_rag_documenti

pointer lp_old_pointer


lp_old_pointer = SetPointer(HourGlass!)

lds_rag_documenti = create datastore
lds_rag_documenti.dataobject = "d_rag_bol_acq"
lds_rag_documenti.settransobject(sqlca)

select cod_valuta
 into  :ls_cod_valuta
 from  tes_fat_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :il_anno_reg_fat_acq and
		 num_registrazione = :il_num_reg_fat_acq;

for ll_i = 1 to upperbound(fl_selected)
	li_anno_bolla_acq = dw_lista_bolle_orig.getitemnumber(fl_selected[ll_i], "anno_bolla_acq")
	ll_num_bolla_acq = dw_lista_bolle_orig.getitemnumber(fl_selected[ll_i], "num_bolla_acq")
	ls_cod_fornitore[1] = dw_lista_bolle_orig.getitemstring(fl_selected[ll_i], "cod_fornitore")
	ls_cod_valuta = dw_lista_bolle_orig.getitemstring(fl_selected[ll_i], "cod_valuta")
	ls_cod_tipo_bol_acq = dw_lista_bolle_orig.getitemstring(fl_selected[ll_i], "cod_tipo_bol_acq")

	declare cur_det_bol_acq cursor for
	select prog_riga_bolla_acq,
			 quan_arrivata
	 from  det_bol_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_bolla_acq = :li_anno_bolla_acq and  
			 num_bolla_acq = :ll_num_bolla_acq;
	
	open cur_det_bol_acq;
	
	fetch cur_det_bol_acq into  :ll_prog_riga_bolla_acq, :ld_quan_acquisto;
	
	do while SQLCA.sqlcode = 0

		lds_rag_documenti.insertrow(0)
		lds_rag_documenti.setrow(lds_rag_documenti.rowcount())
		lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "anno_documento", li_anno_bolla_acq)
		lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "num_documento", ll_num_bolla_acq)
		lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "prog_riga_documento", ll_prog_riga_bolla_acq)
		lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_fornitore", ls_cod_fornitore[1])
		lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_tipo_documento", ls_cod_tipo_bol_acq	)
		lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "flag_saldo", ls_flag_saldo)
		lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "quan_acquisto", ld_quan_acquisto)
		fetch cur_det_bol_acq into  :ll_prog_riga_bolla_acq, :ld_quan_acquisto;
	loop
	close cur_det_bol_acq;
next

for ll_i = 1 to lds_rag_documenti.rowcount()
	select max(prog_riga_fat_acq)
		into :ll_prog_riga_fat_acq
		from det_fat_acq
		where cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :il_anno_reg_fat_acq and
		 num_registrazione = :il_num_reg_fat_acq;
	
	if sqlca.sqlcode = -1 then
		fs_messaggio =  "Errore durante la lettura Dettagli Fatture Acquisto. ~r~nDettaglio:" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	if isnull(ll_prog_riga_fat_acq) then ll_prog_riga_fat_acq = 0
	ll_prog_riga_fat_acq = ll_prog_riga_fat_acq  + 10
			
	li_anno_bolla_acq = lds_rag_documenti.getitemnumber(ll_i, "anno_documento")
	ll_num_bolla_acq = lds_rag_documenti.getitemnumber(ll_i, "num_documento")
	ll_prog_riga_bolla_acq = lds_rag_documenti.getitemnumber(ll_i, "prog_riga_documento")
	ls_flag_saldo = lds_rag_documenti.getitemstring(ll_i, "flag_saldo")
	
	// stefanop 01/08/2012: potrebbe essere nulla
	if isnull(ls_flag_saldo) or ls_flag_saldo = "" then ls_flag_saldo = "N"

	ld_quan_acquisto = lds_rag_documenti.getitemnumber(ll_i, "quan_acquisto")
	ls_cod_tipo_bol_acq = lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")
	// claudia 19/01/06 aggiungo la ricerca del riferimento all'ordine in modo che si veda anche in fattura
	
	select	det_bol_acq.cod_tipo_det_acq,   
				det_bol_acq.cod_prodotto,   
				det_bol_acq.cod_misura,   
				det_bol_acq.des_prodotto,   
				det_bol_acq.quan_fatturata,
				det_bol_acq.prezzo_acquisto,   
				det_bol_acq.fat_conversione,   
				det_bol_acq.sconto_1,   
				det_bol_acq.sconto_2,  
				det_bol_acq.sconto_3,  
				det_bol_acq.cod_iva,   
				det_bol_acq.quan_arrivata,
				det_bol_acq.val_riga,				
				det_bol_acq.nota_dettaglio,   
				det_bol_acq.anno_commessa,
				det_bol_acq.num_commessa,
				det_bol_acq.cod_centro_costo,
				det_bol_acq.sconto_4,   
				det_bol_acq.sconto_5,   
				det_bol_acq.sconto_6,   
				det_bol_acq.sconto_7,   
				det_bol_acq.sconto_8,   
				det_bol_acq.sconto_9,   
				det_bol_acq.sconto_10,
				det_bol_acq.cod_deposito,   
				det_bol_acq.cod_ubicazione,   
				det_bol_acq.cod_lotto,   
				det_bol_acq.data_stock,   
				det_bol_acq.prog_stock,
				det_bol_acq.anno_registrazione_mov_mag,
				det_bol_acq.num_registrazione_mov_mag,
				det_bol_acq.anno_registrazione,
				det_bol_acq.num_registrazione,
				det_bol_acq.prog_riga_ordine_acq
	into		:ls_cod_tipo_det_acq, 
				:ls_cod_prodotto, 
				:ls_cod_misura, 
				:ls_des_prodotto, 
				:ld_quan_fatturata,
				:ld_prezzo_acquisto, 
				:ld_fat_conversione_acq, 
				:ld_sconto_1, 
				:ld_sconto_2, 
				:ld_sconto_3, 
				:ls_cod_iva, 
				:ld_quan_arrivata,
				:ld_val_riga,
				:ls_nota_dettaglio, 
				:ll_anno_commessa,
				:ll_num_commessa,
				:ls_cod_centro_costo,
				:ld_sconto_4, 
				:ld_sconto_5, 
				:ld_sconto_6, 
				:ld_sconto_7, 
				:ld_sconto_8, 
				:ld_sconto_9, 
				:ld_sconto_10,
				:ls_cod_deposito,
				:ls_cod_ubicazione,
				:ls_cod_lotto,
				:ldt_data_stock,
				:ll_prog_stock,
				:li_anno_registrazione_mov_mag,
				:ll_num_registrazione_mov_mag,
				:ll_anno_ordine_registrazione,
				:ll_num_ordine_registrazione,
				:ll_prog_riga_ordine_acq
	from 		det_bol_acq  
	where 	det_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				det_bol_acq.anno_bolla_acq = :li_anno_bolla_acq and  
				det_bol_acq.num_bolla_acq = :ll_num_bolla_acq and
				det_bol_acq.prog_riga_bolla_acq = :ll_prog_riga_bolla_acq
	order by det_bol_acq.prog_riga_bolla_acq asc;

	if sqlca.sqlcode = -1 then
		fs_messaggio =  "Errore durante la lettura Dettagli bolle Acquisto. ~r~nDettaglio:" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	if ll_anno_commessa<=0 then setnull(ll_anno_commessa)
	if ll_num_commessa<=0 then setnull(ll_num_commessa)

	// *** michela: come da specifica di ptenda prodotti_grezzi, se esiste il codice alternativo lo metto
	//              nel prodotto grezzo
	
	setnull(ls_cod_prodotto_alt)
	setnull(ls_des_prodotto_alt)
	
	select cod_prodotto_alt
	into   :ls_cod_prodotto_alt
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto;
	
	if sqlca.sqlcode = -1 then
		fs_messaggio =  "Errore durante la lettura del prodotto alternativo. ~r~nDettaglio:" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	
	if not isnull(ls_cod_prodotto_alt) then
				
		select des_prodotto			       
		into   :ls_des_prodotto_alt
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_alt;
							 
		if sqlca.sqlcode = -1 then
			fs_messaggio =  "Errore durante la lettura del prodotto alternativo. ~r~nDettaglio:" + sqlca.sqlerrtext
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
	end if
	
	if ls_cod_prodotto_alt = "" then setnull(ls_cod_prodotto_alt)
	if ls_des_prodotto_alt = "" then setnull(ls_des_prodotto_alt)
	
	// *** fine modifica



	select tab_tipi_det_acq.flag_tipo_det_acq,
			 tab_tipi_det_acq.cod_tipo_movimento,
			 tab_tipi_det_acq.flag_sola_iva
	into   :ls_flag_tipo_det_acq,
			 :ls_cod_tipo_movimento,
			 :ls_flag_sola_iva
	from   tab_tipi_det_acq
	where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli. ~r~nDettaglio:" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	if ld_quan_acquisto >= (ld_quan_arrivata - ld_quan_fatturata) then ls_flag_saldo = "S"
	
	update tes_bol_acq
	set    flag_gen_fat = 'S'
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_bolla_acq = :li_anno_bolla_acq and
			 num_bolla_acq = :ll_num_bolla_acq;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento Testata bolla ~r~nDettaglio:" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
		ROLLBACK;
	end if
	
	update det_bol_acq
	set    quan_fatturata = quan_fatturata + :ld_quan_acquisto,
			 flag_saldo    = :ls_flag_saldo
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_bolla_acq = :li_anno_bolla_acq and
			 num_bolla_acq = :ll_num_bolla_acq and
			 prog_riga_bolla_acq = :ll_prog_riga_bolla_acq ;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante aggiornamento righe bolla ~r~nDettaglio:" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
		ROLLBACK;
	else
		COMMIT;
	end if

/*	if ls_flag_sola_iva = 'N' then
		ld_val_riga = ld_quan_acquisto * ld_prezzo_acquisto
		ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
		ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
		ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
		ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
		ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
		ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
		ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
		ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
		ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
		ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
		ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
		ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
		ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
		ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
		ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
		ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
		ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
		ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
		ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
		if ls_cod_valuta = ls_lire then
			ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
		else
			ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
		end if   
	
		if ls_flag_tipo_det_acq = "M" or &
			ls_flag_tipo_det_acq = "C" or &
			ls_flag_tipo_det_acq = "N" then
			ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
			ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
			ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
			ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
		elseif ls_flag_tipo_det_acq = "S" then
			ld_val_evaso = ld_val_riga_sconto_10 * -1
		else
			ld_val_evaso = ld_val_riga_sconto_10
		end if
		ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
	end if

	if ls_cod_valuta = ls_lire then
		ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
	end if   

	update tes_fat_acq
		set tot_val_fat_acq = tot_val_fat_acq + :ld_val_evaso
	 where tes_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :il_anno_reg_fat_acq and
		 num_registrazione = :il_num_reg_fat_acq;


	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento Testata Fatture Acquisto. ~r~nDettaglio:" + sqlca.sqlerrtext		
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if*/
	
	insert into det_fat_acq
	   (cod_azienda,   
		anno_registrazione,   
		num_registrazione,   
		prog_riga_fat_acq,   
		cod_tipo_det_acq,   
		cod_misura,   
		cod_prodotto,   
		des_prodotto,   
		quan_fatturata,   
		prezzo_acquisto,   
		fat_conversione,   
		sconto_1,   
		sconto_2,   
		sconto_3,   
		cod_iva,   
		cod_deposito,   
		cod_ubicazione,   
		cod_lotto,   
		data_stock,   
		cod_tipo_movimento,   
		num_registrazione_mov_mag,   
		cod_conto,   
		nota_dettaglio,   
		anno_bolla_acq,   
		num_bolla_acq,   
		prog_riga_bolla_acq,   
		flag_accettazione,   
		anno_commessa,   
		num_commessa,   
		cod_centro_costo,   
		sconto_4,   
		sconto_5,   
		sconto_6,   
		sconto_7,   
		sconto_8,   
		sconto_9,   
		sconto_10,   
		anno_registrazione_mov_mag,   
		prog_stock,   
		anno_reg_des_mov,   
		num_reg_des_mov,   
		anno_reg_ord_acq,
		num_reg_ord_acq,
		prog_riga_ordine_acq,
		prog_bolla_acq,
		anno_reg_mov_mag_ret,
		num_reg_mov_mag_ret,
		cod_prodotto_grezzo,
		des_prodotto_grezzo,
		flag_imponibile_forzato) //stefanop 26/05/2010: ticket 134: messo a N altrimenti non calcola gli sconti    
values (:s_cs_xx.cod_azienda,   
		:il_anno_reg_fat_acq,   
		:il_num_reg_fat_acq,   
		:ll_prog_riga_fat_acq,   
		:ls_cod_tipo_det_acq,   
		:ls_cod_misura,   
		:ls_cod_prodotto,   
		:ls_des_prodotto,   
		:ld_quan_acquisto,   
		:ld_prezzo_acquisto,   
		:ld_fat_conversione_acq,   
		:ld_sconto_1,   
		:ld_sconto_2,   
		:ld_sconto_3,   
		:ls_cod_iva,   
		:ls_cod_deposito,   
		:ls_cod_ubicazione,   
		:ls_cod_lotto,   
		:ldt_data_stock,   
		:ls_cod_tipo_movimento,   
		:ll_num_registrazione_mov_mag,
		null,   
		null,   
		:li_anno_bolla_acq,
		:ll_num_bolla_acq,
		:ll_prog_riga_bolla_acq,
		'N',   
		:ll_anno_commessa,   
		:ll_num_commessa,   
		:ls_cod_centro_costo,   
		:ld_sconto_4,   
		:ld_sconto_5,   
		:ld_sconto_6,   
		:ld_sconto_7,   
		:ld_sconto_8,   
		:ld_sconto_9,   
		:ld_sconto_10,   
		:li_anno_registrazione_mov_mag,
		:ll_prog_stock,   
		:ll_anno_reg_dest_stock,
		:ll_num_reg_dest_stock,
		:ll_anno_ordine_registrazione,
		:ll_num_ordine_registrazione,
		:ll_prog_riga_ordine_acq,
		:ll_progressivo,
		null,
		null,
		:ls_cod_prodotto_alt,
		:ls_des_prodotto_alt,
		'N');  //stefanop 26/05/2010: ticket 134: messo a N altrimenti non calcola gli sconti

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la scrittura Dettagli Fatture Acquisto. ~r~nDettaglio:" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	COMMIT;

	if li_anno_bolla_acq_old <> li_anno_bolla_acq or &
		ll_num_bolla_acq_old <> ll_num_bolla_acq then

		li_anno_bolla_acq_old = li_anno_bolla_acq
		ll_num_bolla_acq_old = ll_num_bolla_acq
		ll_progressivo_old = ll_progressivo
		dw_gen_orig_bol_dest_fat.insertrow(0)
		dw_gen_orig_bol_dest_fat.setrow(dw_gen_orig_bol_dest_fat.rowcount())
		dw_gen_orig_bol_dest_fat.setitem(dw_gen_orig_bol_dest_fat.getrow(), "anno_bolla_acq", li_anno_bolla_acq)
		dw_gen_orig_bol_dest_fat.setitem(dw_gen_orig_bol_dest_fat.getrow(), "num_bolla_acq", ll_num_bolla_acq)
		dw_gen_orig_bol_dest_fat.setitem(dw_gen_orig_bol_dest_fat.getrow(), "anno_registrazione", il_anno_reg_fat_acq)
		dw_gen_orig_bol_dest_fat.setitem(dw_gen_orig_bol_dest_fat.getrow(), "num_registrazione", il_num_reg_fat_acq)
		
		
	end if
next

destroy lds_rag_documenti
SetPointer(lp_old_pointer)
return 0
end function

public function integer wf_genera_fatture_acq (long fl_selected[], ref string fs_messaggio);string 	ls_cod_tipo_bol_acq, ls_flag_riep_fat, ls_cod_tipo_fat_acq, &
	    		ls_flag_destinazione, ls_flag_num_doc, ls_flag_imballo, &
		 	ls_flag_vettore, ls_flag_inoltro, ls_flag_mezzo, ls_flag_porto, ls_flag_resa, &
		 	ls_cod_fornitore[], ls_cod_valuta, ls_cod_pagamento, &
		 	ls_cod_banca, ls_cod_banca_clien_for, ls_flag_acc_materiali, &
		 	ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, &
		 	ls_cod_porto, ls_cod_resa, ls_sort, ls_key_sort, ls_key_sort_old, ls_cod_tipo_fat_ven, &
		 	ls_cod_operatore, ls_cod_tipo_analisi_bol, ls_cod_deposito[], &
		 	ls_cod_tipo_listino_prodotto, ls_lire, &
		 	ls_cod_tipo_analisi_fat, ls_cod_doc_origine, ls_cod_fil_fornitore, ls_cod_tipo_det_acq, &
		 	ls_cod_prodotto, ls_cod_misura, ls_des_prodotto, ls_cod_iva, &
		 	ls_nota_dettaglio, ls_cod_centro_costo, ls_flag_tipo_det_acq, ls_cod_tipo_movimento, &
		 	ls_flag_sola_iva, ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], &
		 	ls_referenza, ls_flag_evasione, ls_numeratore_doc_origine, ls_flag_calcolo, &
		 	ls_flag_doc_suc_det, ls_cod_documento, ls_numeratore_documento, ls_cod_lire, ls_messaggio, &
			ls_cod_valuta_corrente, ls_cod_prodotto_alt, ls_des_prodotto_alt, ls_cod_natura_intra
		 
integer 	li_anno_doc_origine, li_anno_bolla_acq, li_anno_registrazione_mov_mag, li_anno_bolla_acq_old, li_row

dec{4} 	ld_cambio_acq, ld_sconto_testata, ld_tot_val_evaso, ld_tot_val_bolla, &
		 	ld_sconto_pagamento, ld_quan_bolla, ld_prezzo_acquisto, ld_fat_conversione_acq, &
		 	ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_quan_arrivata, ld_val_riga, ld_sconto_4, &
		 	ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
		 	ld_quan_acquisto, ld_val_sconto_1, ld_val_riga_sconto_1, ld_val_sconto_2, &
		 	ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, &
		 	ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, ld_val_sconto_6, &
		 	ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, ld_val_sconto_8, &
		 	ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, &
		 	ld_val_riga_sconto_10, ld_val_sconto_testata, ld_val_riga_sconto_testata, &
		 	ld_val_sconto_pagamento, ld_val_evaso

long 		ll_prog_riga_documento, ll_prog_bolla_acq, ll_prog_riga_fat_acq ,&
	  		ll_prog_riga_bolla_acq, ll_anno_commessa, ll_num_commessa, ll_prog_stock[], &
	  		ll_anno_reg_dest_stock, ll_i, ll_prog_fat_acq, ll_num_registrazione_mov_mag, &
	  		ll_num_reg_dest_stock, ll_anno_registrazione[], ll_num_registrazione[], &
	  		ll_anno_esercizio, ll_num_doc_origine, ll_prog_bolla_acq_old, ll_max_num_reg, &
	  		ll_num_bolla_acq, ll_num_bolla_acq_old, ll_num_documento, ll_anno_bolla_acq_app, &
			ll_num_bolla_acq_app, ll_anno_ordine_registrazione, ll_num_ordine_registrazione, ll_prog_riga_ordine_acq

datetime ldt_data_bolla, ldt_data_fattura, ldt_data_stock[], ldt_oggi

datastore lds_rag_documenti

pointer lp_old_pointer

uo_calcola_documento_euro luo_doc

string ls_cod_operatore_new


lp_old_pointer = SetPointer(HourGlass!)

lds_rag_documenti = create datastore
lds_rag_documenti.dataobject = "d_rag_bol_acq"
lds_rag_documenti.settransobject(sqlca)

ls_flag_calcolo = s_cs_xx.parametri.parametro_s_1
ldt_data_fattura = s_cs_xx.parametri.parametro_data_1

ll_anno_esercizio = f_anno_esercizio()
li_anno_doc_origine = f_anno_esercizio()
ldt_oggi = datetime(today())

for ll_i = 1 to upperbound(fl_selected)
	li_anno_bolla_acq = dw_lista_bolle_orig.getitemnumber(fl_selected[ll_i], "anno_bolla_acq")
	ll_num_bolla_acq = dw_lista_bolle_orig.getitemnumber(fl_selected[ll_i], "num_bolla_acq")
	ls_cod_fornitore[1] = dw_lista_bolle_orig.getitemstring(fl_selected[ll_i], "cod_fornitore")
	ls_cod_valuta = dw_lista_bolle_orig.getitemstring(fl_selected[ll_i], "cod_valuta")
	ls_cod_pagamento = dw_lista_bolle_orig.getitemstring(fl_selected[ll_i], "cod_pagamento")
	ls_cod_banca = dw_lista_bolle_orig.getitemstring(fl_selected[ll_i], "cod_banca")
	ls_cod_banca_clien_for = dw_lista_bolle_orig.getitemstring(fl_selected[ll_i], "cod_banca_clien_for")
	ls_cod_tipo_bol_acq = dw_lista_bolle_orig.getitemstring(fl_selected[ll_i], "cod_tipo_bol_acq")


//------------------------------- Carico Numeratore Documento -----------------------------------------
	//ll_anno_bolla_acq_app = dw_lista_bolle_orig.getitemnumber(fl_selected[ll_i], "anno_bolla_acq")
	//ll_num_bolla_acq_app = dw_lista_bolle_orig.getitemnumber(fl_selected[ll_i], "num_bolla_acq")
	//
	//if not isnull(ll_anno_bolla_acq_app) and not isnull(ll_num_bolla_acq_app) then
	//	select cod_tipo_bol_acq
	//	  into :ls_cod_tipo_bol_acq
	//	  from tes_bol_acq
	//	 where cod_azienda = :s_cs_xx.cod_azienda
	//	   and anno_bolla_acq = :ll_anno_bolla_acq_app
	//		and num_bolla_acq = :ll_num_bolla_acq_app;
	//	if sqlca.sqlcode < 0 then
	//		fs_messaggio = "Errore in lettura dati da tabella TES_BOL_ACQ.~n~r" + sqlca.sqlerrtext
	//		return -1
	//	end if	
	//end if	
			
	if isnull(ls_cod_tipo_fat_acq) or ls_cod_tipo_fat_acq = "" then
		select	cod_tipo_fat_acq   
		into 		:ls_cod_tipo_fat_acq 
		from 		tab_tipi_bol_acq
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_bol_acq = :ls_cod_tipo_bol_acq;
		
		if sqlca.sqlcode < 0 then
			fs_messaggio = "Errore in lettura dati da tabella tipi fatture di acquisto.~n~r" + sqlca.sqlerrtext
			return -1
		end if	
	end if
	
	select cod_documento,
			 numeratore_documento
	  into :ls_cod_documento,
			 :ls_numeratore_documento
	  from tab_tipi_fat_acq
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_fat_acq = :ls_cod_tipo_fat_acq;
		
	if sqlca.sqlcode < 0 then
		fs_messaggio = "Errore in lettura dati da tabella Tipi Fatture Acquisto.~n~r" + sqlca.sqlerrtext
		return -1
	end if		
	
	select num_documento + 1
	  into :ll_num_documento
	  from numeratori
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_documento = :ls_cod_documento
		and numeratore_documento = :ls_numeratore_documento
		and anno_documento = :ll_anno_esercizio;
	
	if sqlca.sqlcode < 0 then
		fs_messaggio = "Errore in lettura dati da tabella Numeratori.~n~r" + sqlca.sqlerrtext
		return -1
	end if						
//-----------------------------------------------------------------------------------------------------

	declare cur_det_bol_acq cursor for
	select prog_riga_bolla_acq,
			 quan_arrivata
	 from  det_bol_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_bolla_acq = :li_anno_bolla_acq and  
			 num_bolla_acq = :ll_num_bolla_acq;
	
	open cur_det_bol_acq;
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore in OPEN del curose dati cur_det_bol_acq.~r~n" + sqlca.sqlerrtext
		return -1
	end if						
	
	do while true
		
		fetch cur_det_bol_acq into  :ll_prog_riga_bolla_acq, :ld_quan_acquisto;
		
		if sqlca.sqlcode = 100 then exit
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore in FETCH del curose dati cur_det_bol_acq.~r~n" + sqlca.sqlerrtext
			return -1
		end if						
	
		li_row = lds_rag_documenti.insertrow(0)
		lds_rag_documenti.setitem(li_row, "anno_documento", li_anno_bolla_acq)
		lds_rag_documenti.setitem(li_row, "num_documento", ll_num_bolla_acq)
		lds_rag_documenti.setitem(li_row, "prog_riga_documento", ll_prog_riga_bolla_acq)
		lds_rag_documenti.setitem(li_row, "cod_fornitore", ls_cod_fornitore[1])
		lds_rag_documenti.setitem(li_row, "cod_valuta", ls_cod_valuta)
		lds_rag_documenti.setitem(li_row, "cod_pagamento", ls_cod_pagamento)
		lds_rag_documenti.setitem(li_row, "cod_banca", ls_cod_banca)
		lds_rag_documenti.setitem(li_row, "cod_banca_clien_for", ls_cod_banca_clien_for)
		lds_rag_documenti.setitem(li_row, "cod_tipo_documento", ls_cod_tipo_bol_acq	)
		lds_rag_documenti.setitem(li_row, "quan_acquisto", ld_quan_acquisto)
		
	loop
	
	close cur_det_bol_acq;
	
next

ls_sort = "cod_fornitore A, cod_valuta A, cod_pagamento A, cod_banca A, cod_banca_clien_for A, " + &
			 "anno_documento A, num_documento A, progressivo A, prog_riga_documento A"

lds_rag_documenti.setsort(ls_sort)

lds_rag_documenti.sort()

ls_key_sort_old = ""
for ll_i = 1 to lds_rag_documenti.rowcount()
	li_anno_bolla_acq = lds_rag_documenti.getitemnumber(ll_i, "anno_documento")
	ll_num_bolla_acq = lds_rag_documenti.getitemnumber(ll_i, "num_documento")
	ll_prog_riga_bolla_acq = lds_rag_documenti.getitemnumber(ll_i, "prog_riga_documento")
	ld_quan_acquisto = lds_rag_documenti.getitemnumber(ll_i, "quan_acquisto")

	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_fornitore")) then
		ls_key_sort = lds_rag_documenti.getitemstring(ll_i, "cod_fornitore")
	else
		ls_key_sort = "      "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_valuta")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_valuta")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	ls_cod_tipo_bol_acq = lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")

	select tes_bol_acq.cod_operatore,   
			 tes_bol_acq.data_registrazione,   
			 tes_bol_acq.cod_fornitore,   
			 tes_bol_acq.cod_fil_fornitore,   			 
			 tes_bol_acq.cod_deposito,   
			 tes_bol_acq.cod_valuta,   
			 tes_bol_acq.cambio_acq,   
			 tes_bol_acq.cod_tipo_listino_prodotto,   
			 tes_bol_acq.cod_pagamento,   
			 tes_bol_acq.sconto,   
			 tes_bol_acq.cod_banca,   
			 tes_bol_acq.totale_val_bolla,
			 tes_bol_acq.cod_banca_clien_for,
			 tes_bol_acq.cod_mezzo,
			 tes_bol_acq.cod_natura_intra,
			 tes_bol_acq.cod_porto
	 into  :ls_cod_operatore,   
			 :ldt_data_bolla,   
			 :ls_cod_fornitore[1],   
			 :ls_cod_fil_fornitore,   
			 :ls_cod_deposito[1],   
			 :ls_cod_valuta,   
			 :ld_cambio_acq,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_banca,   
			 :ld_tot_val_bolla,
			 :ls_cod_banca_clien_for,
			 :ls_cod_mezzo,
			 :ls_cod_natura_intra,
			 :ls_cod_porto
	from   tes_bol_acq  
	where  tes_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_bol_acq.anno_bolla_acq = :li_anno_bolla_acq and  
			 tes_bol_acq.num_bolla_acq = :ll_num_bolla_acq;

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la lettura Testata Bolla Acquisto. ~r~nDettaglio:" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	if ls_cod_mezzo="" then setnull(ls_cod_mezzo)
	if ls_cod_natura_intra="" then setnull(ls_cod_natura_intra)
	if ls_cod_porto="" then setnull(ls_cod_porto)
	

	if ls_key_sort <> ls_key_sort_old then

		if ls_key_sort_old <> "" and ls_flag_calcolo = "S" then
			
			select stringa
			into   :ls_cod_lire
			from   parametri_azienda
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 flag_parametro = 'S' and
					 cod_parametro = 'CVL';
					 
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella select di parametri azienda: " + sqlca.sqlerrtext
				destroy lds_rag_documenti
				SetPointer(lp_old_pointer)
				return -1
			end if
			
			select stringa
			into   :ls_cod_valuta_corrente
			from   parametri_azienda
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 flag_parametro = 'S' and
					 cod_parametro = 'LIR';
					 
			if sqlca.sqlcode <> 0 then
				fs_messaggio = "Errore nella select di parametri azienda: " + sqlca.sqlerrtext
				destroy lds_rag_documenti
				SetPointer(lp_old_pointer)
				return -1
			end if
			
			luo_doc = create uo_calcola_documento_euro
			
			if luo_doc.uof_calcola_documento(il_anno_reg_fat_acq,il_num_reg_fat_acq,"fat_acq", ls_messaggio) <> 0 then
				fs_messaggio = ls_messaggio
				destroy luo_doc
				rollback;
				destroy lds_rag_documenti
				SetPointer(lp_old_pointer)
				return -1
			else
				destroy luo_doc
				commit;
			end if	
				
		end if
	
		select	cod_tipo_fat_acq   
		into 		:ls_cod_tipo_fat_acq 
		from 		tab_tipi_bol_acq
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_bol_acq = :ls_cod_tipo_bol_acq;
		
		if isnull(ls_cod_tipo_fat_acq) or ls_cod_tipo_fat_acq = "" then
			fs_messaggio = "Tipo Fattura di Acquisto non Definito in Tabella Bolle di Acquisto"
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
		
		select	cod_documento,   
					numeratore_documento  
		into 		:ls_cod_doc_origine,   
					:ls_numeratore_doc_origine  
		from 		tab_tipi_fat_acq  
		where 	tab_tipi_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and
					tab_tipi_fat_acq.cod_tipo_fat_acq = :ls_cod_tipo_fat_acq;
			
		if isnull(ls_cod_doc_origine) or ls_cod_doc_origine = "" then
			fs_messaggio = "Documento non Definito in Tabella Tipi Fattura Acquisto"
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
		
		if isnull(ls_numeratore_doc_origine) or ls_cod_doc_origine = "" then
			fs_messaggio = "Numeratore non Definito in Tabella Tipi Fattura Acquisto"
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
			
		select	num_documento
		into 		:ll_num_doc_origine
		from 		numeratori
		where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_documento = :ls_cod_doc_origine and
					numeratore_documento = :ls_numeratore_doc_origine and
					anno_documento = :li_anno_doc_origine;
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Numeratore non Definito per Codice Documento e Anno Esercizio Correnti ~r~nDettaglio:" + sqlca.sqlerrtext
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
			
		ll_num_doc_origine ++
			
		update numeratori
		set num_documento = :ll_num_doc_origine
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_documento = :ls_cod_doc_origine and
				numeratore_documento = :ls_numeratore_doc_origine and
				anno_documento = :li_anno_doc_origine;
		
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la scrittura Numeratori. ~r~nDettaglio:" + sqlca.sqlerrtext
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if

		select max(tes_fat_acq.progressivo)
		into :ll_prog_fat_acq  
		from tes_fat_acq 
		where tes_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				tes_fat_acq.anno_registrazione = :il_anno_reg_fat_acq and
				tes_fat_acq.num_registrazione = :il_num_reg_fat_acq;

		if isnull(ll_prog_fat_acq) or ll_prog_fat_acq = 0 then
			ll_prog_fat_acq = 1
		else
			ll_prog_fat_acq ++
		end if

		select parametri_azienda.stringa
		into   :ls_lire
		from   parametri_azienda
		where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
				 parametri_azienda.flag_parametro = 'S' and &
				 parametri_azienda.cod_parametro = 'LIR';
	
		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Configurare il codice valuta per le Lire Italiane. ~r~nDettaglio:" + sqlca.sqlerrtext
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
	
		select tab_pagamenti.sconto
		into   :ld_sconto_pagamento
		from   tab_pagamenti
		where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;
	
		if sqlca.sqlcode <> 0 then
			ld_sconto_pagamento = 0
		end if		
		
		il_anno_reg_fat_acq = ll_anno_esercizio
		
		select max(num_registrazione)
		into   :il_num_reg_fat_acq
		from   tes_fat_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and 
		       anno_registrazione = :il_anno_reg_fat_acq;
			
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Apice","Errore nella select di tes_fat_acq: " + sqlca.sqlerrtext)
			return -1
		end if
		
		if il_num_reg_fat_acq = 0 or isnull(il_num_reg_fat_acq) then
			il_num_reg_fat_acq = 1
		else		
			il_num_reg_fat_acq ++
		end if			
	
		//prima di fare la insert metti l'operatore collegato per il nuovo documento
		if f_get_operatore_doc(ls_cod_operatore_new, ls_messaggio)>0 then
			ls_cod_operatore = ls_cod_operatore_new
		end if
	
		insert into tes_fat_acq
						( cod_azienda,
						anno_registrazione,
						num_registrazione,  
						cod_doc_origine, 
						anno_doc_origine, 
						numeratore_doc_origine, 
						num_doc_origine, 									
						cod_tipo_fat_acq,   
						data_doc_origine,   
						cod_documento,   
						numeratore_documento,   
						anno_documento,   
						num_documento,   
						data_registrazione,   
						cod_operatore,   
						cod_fornitore,   
						cod_fil_fornitore,   
						cod_valuta,   
						cambio_acq,   
						cod_tipo_listino_prodotto,   
						cod_pagamento,   
						cod_banca,   
						cod_banca_clien_for,   
						cod_documento_nota,   
						numeratore_nota,   
						anno_nota,   
						num_nota,   
						tot_val_fat_acq,   
						tot_valuta_fat_acq,   
						importo_iva,   
						imponibile_iva,   
						importo_valuta_iva,   
						imponibile_valuta_iva,   
						importo_iva_indetraibile,   
						flag_fat_confermata,   
						sconto,
						cod_mezzo,
						cod_natura_intra,
						cod_porto)
		values 		( :s_cs_xx.cod_azienda,   
						:il_anno_reg_fat_acq,   
						:il_num_reg_fat_acq,   
						:ls_cod_documento,
						:ll_anno_esercizio,
						:ls_numeratore_documento,
						:ll_num_documento,						
						:ls_cod_tipo_fat_acq,   
						null,   
						null,   
						null,   
						0,   
						0,   
						:ldt_data_fattura,   
						:ls_cod_operatore,   
						:ls_cod_fornitore[1],   
						:ls_cod_fil_fornitore,   
						:ls_cod_valuta,   
						:ld_cambio_acq,   
						:ls_cod_tipo_listino_prodotto,   
						:ls_cod_pagamento,   
						:ls_cod_banca,   
						:ls_cod_banca_clien_for,   
						null,   
						null,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						0,   
						'S',   
						0,
						:ls_cod_mezzo,
						:ls_cod_natura_intra,
						:ls_cod_porto);

		if sqlca.sqlcode <> 0 then
			fs_messaggio = "Errore durante la scrittura Testata Fatture Acquisto. ~r~nDettaglio:" + sqlca.sqlerrtext
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if

		ll_prog_riga_fat_acq = 0
	
		fs_messaggio = fs_messaggio + string(il_anno_reg_fat_acq) + "/" + string(il_num_reg_fat_acq) + " "
	end if
	

	ll_prog_riga_fat_acq = ll_prog_riga_fat_acq + 10
	
	select	det_bol_acq.cod_tipo_det_acq,   
				det_bol_acq.cod_prodotto,   
				det_bol_acq.cod_misura,   
				det_bol_acq.des_prodotto,   
				det_bol_acq.quan_fatturata,
				det_bol_acq.prezzo_acquisto,   
				det_bol_acq.fat_conversione,   
				det_bol_acq.sconto_1,   
				det_bol_acq.sconto_2,  
				det_bol_acq.sconto_3,
				det_bol_acq.cod_iva,   
				det_bol_acq.quan_arrivata,
				det_bol_acq.val_riga,				
				det_bol_acq.nota_dettaglio,   
				det_bol_acq.anno_commessa,
				det_bol_acq.num_commessa,
				det_bol_acq.cod_centro_costo,
				det_bol_acq.sconto_4,   
				det_bol_acq.sconto_5,   
				det_bol_acq.sconto_6,   
				det_bol_acq.sconto_7,   
				det_bol_acq.sconto_8,   
				det_bol_acq.sconto_9,   
				det_bol_acq.sconto_10,
				det_bol_acq.cod_deposito,
				det_bol_acq.cod_ubicazione,
				det_bol_acq.cod_lotto,
				det_bol_acq.data_stock,
				det_bol_acq.prog_stock,
				det_bol_acq.anno_registrazione_mov_mag,
				det_bol_acq.num_registrazione_mov_mag,
				det_bol_acq.flag_accettazione,
				det_bol_acq.flag_doc_suc_det,
				det_bol_acq.anno_registrazione,
				det_bol_acq.num_registrazione,
				det_bol_acq.prog_riga_ordine_acq
	into		:ls_cod_tipo_det_acq, 
				:ls_cod_prodotto, 
				:ls_cod_misura, 
				:ls_des_prodotto, 
				:ld_quan_bolla,
				:ld_prezzo_acquisto, 
				:ld_fat_conversione_acq, 
				:ld_sconto_1, 
				:ld_sconto_2, 
				:ld_sconto_3, 
				:ls_cod_iva, 
				:ld_quan_arrivata,
				:ld_val_riga,
				:ls_nota_dettaglio, 
				:ll_anno_commessa,
				:ll_num_commessa,
				:ls_cod_centro_costo,
				:ld_sconto_4, 
				:ld_sconto_5, 
				:ld_sconto_6, 
				:ld_sconto_7, 
				:ld_sconto_8, 
				:ld_sconto_9, 
				:ld_sconto_10,
				:ls_cod_deposito[1],
				:ls_cod_ubicazione[1],
				:ls_cod_lotto[1],
				:ldt_data_stock[1],
				:ll_prog_stock[1],
				:li_anno_registrazione_mov_mag,
				:ll_num_registrazione_mov_mag,
				:ls_flag_acc_materiali,
				:ls_flag_doc_suc_det, 
				:ll_anno_ordine_registrazione,
				:ll_num_ordine_registrazione,
				:ll_prog_riga_ordine_acq
	from 		det_bol_acq  
	where 	det_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and  
				det_bol_acq.anno_bolla_acq = :li_anno_bolla_acq and  
				det_bol_acq.num_bolla_acq = :ll_num_bolla_acq and
				det_bol_acq.prog_riga_bolla_acq = :ll_prog_riga_bolla_acq
	order by det_bol_acq.prog_riga_bolla_acq asc;

	if sqlca.sqlcode = -1 then
		fs_messaggio =  "Errore durante la lettura Dettagli Bolle Acquisto. ~r~nDettaglio:" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	if ll_anno_commessa<=0 then setnull(ll_anno_commessa)
	if ll_num_commessa<=0 then setnull(ll_num_commessa)

	// *** michela: come da specifica di ptenda prodotti_grezzi, se esiste il codice alternativo lo metto
	//              nel prodotto grezzo
	
	setnull(ls_cod_prodotto_alt)
	setnull(ls_des_prodotto_alt)
	
	select cod_prodotto_alt
	into   :ls_cod_prodotto_alt
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_prodotto = :ls_cod_prodotto;
	
	if sqlca.sqlcode = -1 then
		fs_messaggio =  "Errore durante la lettura del prodotto alternativo. ~r~nDettaglio:" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	
	if not isnull(ls_cod_prodotto_alt) then
				
		select des_prodotto			       
		into   :ls_des_prodotto_alt
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_alt;
							 
		if sqlca.sqlcode = -1 then
			fs_messaggio =  "Errore durante la lettura del prodotto alternativo. ~r~nDettaglio:" + sqlca.sqlerrtext
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		end if
	end if
	
	if ls_cod_prodotto_alt = "" then setnull(ls_cod_prodotto_alt)
	if ls_des_prodotto_alt = "" then setnull(ls_des_prodotto_alt)
	
	// *** fine modifica


	select tab_tipi_det_acq.flag_tipo_det_acq,
			 tab_tipi_det_acq.cod_tipo_movimento,
			 tab_tipi_det_acq.flag_sola_iva
	into   :ls_flag_tipo_det_acq,
			 :ls_cod_tipo_movimento,
			 :ls_flag_sola_iva
	from   tab_tipi_det_acq
	where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di lettura tipi dettagli. ~r~nDettaglio:" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	if ls_flag_sola_iva = 'N' then
		ld_val_riga = ld_quan_acquisto * ld_prezzo_acquisto
		ld_val_sconto_1 = (ld_val_riga * ld_sconto_1) / 100
		ld_val_riga_sconto_1 = ld_val_riga - ld_val_sconto_1
		ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
		ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
		ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
		ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
		ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
		ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
		ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
		ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
		ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
		ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
		ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
		ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
		ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
		ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
		ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
		ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
		ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
	
		if ls_cod_valuta = ls_cod_lire then
			ld_val_riga_sconto_10 = round((ld_val_riga_sconto_9 - ld_val_sconto_10), 0)
		else
			ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
		end if   
	
		if ls_flag_tipo_det_acq = "M" or &
			ls_flag_tipo_det_acq = "C" or &
			ls_flag_tipo_det_acq = "N" then
			ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
			ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
			ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
			ld_val_evaso = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
		elseif ls_flag_tipo_det_acq = "S" then
			ld_val_evaso = ld_val_riga_sconto_10 * -1
		else
			ld_val_evaso = ld_val_riga_sconto_10
		end if
		ld_tot_val_evaso = ld_tot_val_evaso + ld_val_evaso
	end if

	if ls_cod_valuta = ls_cod_lire then
		ld_tot_val_evaso = round(ld_tot_val_evaso, 0)
	end if   

	update tes_bol_acq
		set flag_gen_fat = 'S'
		where cod_azienda = :s_cs_xx.cod_azienda and  
			 anno_bolla_acq = :li_anno_bolla_acq and  
			 num_bolla_acq = :ll_num_bolla_acq;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento Testata Bolla Acquisto. ~r~nDettaglio:" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
	update det_bol_acq
		set quan_fatturata = quan_fatturata + :ld_quan_acquisto
		where det_bol_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 det_bol_acq.anno_bolla_acq = :li_anno_bolla_acq and  
			 det_bol_acq.num_bolla_acq = :ll_num_bolla_acq and
			 det_bol_acq.prog_riga_bolla_acq = :ll_prog_riga_bolla_acq;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento Dettaglio Bolla Acquisto. ~r~nDettaglio:" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if
	
/*	update tes_fat_acq
		set tot_val_fat_acq = tot_val_fat_acq + :ld_val_evaso
	 where tes_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_fat_acq.anno_registrazione = :il_anno_reg_fat_acq and  
			 tes_fat_acq.num_registrazione = :il_num_reg_fat_acq;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase di aggiornamento Testata Fatture Acquisto. ~r~nDettaglio:" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if*/

//------------------------------------------------- Modifica Nicola ---------------------------------------------
	if ls_flag_doc_suc_det = 'I' then //nota dettaglio
		select flag_doc_suc_bl
		  into :ls_flag_doc_suc_det
		  from tab_tipi_det_acq
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_tipo_det_acq = :ls_cod_tipo_det_acq;
	end if		
	
	
	if ls_flag_doc_suc_det = 'N' then
		setnull(ls_nota_dettaglio)
	end if				
//-------------------------------------------------- Fine Modifica ----------------------------------------------		

	insert into	det_fat_acq
					(cod_azienda,   
					anno_registrazione,   
					num_registrazione,   
					prog_riga_fat_acq,   
					cod_tipo_det_acq,   
					cod_misura,   
					cod_prodotto,   
					des_prodotto,   
					quan_fatturata,   
					prezzo_acquisto,   
					fat_conversione,   
					sconto_1,   
					sconto_2,   
					sconto_3,   
					cod_iva,   
					cod_deposito,   
					cod_ubicazione,   
					cod_lotto,   
					data_stock,   
					cod_tipo_movimento,   
					num_registrazione_mov_mag,   
					cod_conto,   
					nota_dettaglio,   
					anno_bolla_acq,   
					num_bolla_acq,
					prog_riga_bolla_acq,
					flag_accettazione,   
					anno_commessa,   
					num_commessa,   
					cod_centro_costo,   
					sconto_4,   
					sconto_5,   
					sconto_6,   
					sconto_7,   
					sconto_8,   
					sconto_9,   
					sconto_10,   
					anno_registrazione_mov_mag,   
					prog_stock,   
					anno_reg_des_mov,   
					num_reg_des_mov,
					anno_reg_ord_acq,
					num_reg_ord_acq,
					prog_riga_ordine_acq,
					prog_bolla_acq,
					anno_reg_mov_mag_ret,
					num_reg_mov_mag_ret,
					cod_prodotto_grezzo,
					des_prodotto_grezzo,
					flag_imponibile_forzato) //stefanop 26/05/2010: ticket 134: messo a N altrimenti non calcola gli sconti  
	values		(:s_cs_xx.cod_azienda,   
					:il_anno_reg_fat_acq,   
					:il_num_reg_fat_acq,   
					:ll_prog_riga_fat_acq,   
					:ls_cod_tipo_det_acq,   
					:ls_cod_misura,   
					:ls_cod_prodotto,   
					:ls_des_prodotto,   
					:ld_quan_acquisto,   
					:ld_prezzo_acquisto,   
					:ld_fat_conversione_acq,   
					:ld_sconto_1,   
					:ld_sconto_2,   
					:ld_sconto_3,   
					:ls_cod_iva,   
					:ls_cod_deposito[1],   
					:ls_cod_ubicazione[1],   
					:ls_cod_lotto[1],   
					:ldt_data_stock[1],   
					:ls_cod_tipo_movimento,   
					:ll_num_registrazione_mov_mag,
					null,   
					:ls_nota_dettaglio,   
					:li_anno_bolla_acq,
					:ll_num_bolla_acq,
					:ll_prog_riga_bolla_acq,
					:ls_flag_acc_materiali,   
					:ll_anno_commessa,   
					:ll_num_commessa,   
					:ls_cod_centro_costo,   
					:ld_sconto_4,   
					:ld_sconto_5,   
					:ld_sconto_6,   
					:ld_sconto_7,   
					:ld_sconto_8,   
					:ld_sconto_9,   
					:ld_sconto_10,   
					:li_anno_registrazione_mov_mag,
					:ll_prog_stock[1],   
					:ll_anno_reg_dest_stock,
					:ll_num_reg_dest_stock,
					:ll_anno_ordine_registrazione,
					:ll_num_ordine_registrazione,
					:ll_prog_riga_ordine_acq,
					:ll_prog_bolla_acq,
					null,
					null,
					:ls_cod_prodotto_alt,
					:ls_des_prodotto_alt,
					'N'); //stefanop 26/05/2010: ticket 134: messo a N altrimenti non calcola gli sconti  

	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore durante la scrittura Dettagli Fatture Acquisto. ~r~nDettaglio:" + sqlca.sqlerrtext
		destroy lds_rag_documenti
		SetPointer(lp_old_pointer)
		return -1
	end if

	if li_anno_bolla_acq_old <> li_anno_bolla_acq or &
		ll_num_bolla_acq_old <> ll_num_bolla_acq then

		li_anno_bolla_acq_old = li_anno_bolla_acq
		ll_num_bolla_acq_old = ll_num_bolla_acq
		dw_gen_orig_bol_dest_fat.insertrow(0)
		dw_gen_orig_bol_dest_fat.setrow(dw_gen_orig_bol_dest_fat.rowcount())
		dw_gen_orig_bol_dest_fat.setitem(dw_gen_orig_bol_dest_fat.getrow(), "anno_bolla_acq", li_anno_bolla_acq)
		dw_gen_orig_bol_dest_fat.setitem(dw_gen_orig_bol_dest_fat.getrow(), "num_bolla_acq", ll_num_bolla_acq)
		dw_gen_orig_bol_dest_fat.setitem(dw_gen_orig_bol_dest_fat.getrow(), "anno_registrazione", il_anno_reg_fat_acq)
		dw_gen_orig_bol_dest_fat.setitem(dw_gen_orig_bol_dest_fat.getrow(), "num_registrazione", il_num_reg_fat_acq)

	end if

	if ll_i = lds_rag_documenti.rowcount() and ls_flag_calcolo = "S" then
		
		luo_doc = create uo_calcola_documento_euro
		
		if luo_doc.uof_calcola_documento(il_anno_reg_fat_acq,il_num_reg_fat_acq,"fat_acq",ls_messaggio) <> 0 then
			fs_messaggio = ls_messaggio
			destroy luo_doc
			rollback;
			destroy lds_rag_documenti
			SetPointer(lp_old_pointer)
			return -1
		else
			destroy luo_doc
			commit;
		end if	
			
	end if
	ls_key_sort_old = ls_key_sort
	COMMIT;
next

destroy lds_rag_documenti

SetPointer(lp_old_pointer)
return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]
string l_criteriacolumn[],l_searchtable[],l_searchcolumn[]


set_w_options(c_noenablepopup)

lw_oggetti[1] = dw_gen_orig_bol_dest_fat
lw_oggetti[1] = cb_1
dw_folder.fu_assigntab(3, "Fatture Generate", lw_oggetti[])

lw_oggetti[1] = dw_lista_bolle_orig
lw_oggetti[2] = cb_genera_fat
dw_folder.fu_assigntab(2, "Elenco Bolle", lw_oggetti[])

lw_oggetti[1] = dw_gen_fat_acq_ricerca
lw_oggetti[2] = st_filtro_data
lw_oggetti[3] = sle_filtro_data
lw_oggetti[4] = cb_ricerca
lw_oggetti[5] = cb_annulla
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])
dw_folder.fu_foldercreate(3, 4)

dw_lista_bolle_orig.set_dw_options(sqlca, &
                                  	  pcca.null_object, &
												  c_multiselect + &
												  c_nonew + &
												  c_nomodify + &
												  c_nodelete + &
												  c_disablecc + &
												  c_disableccinsert, &
												  c_default)

dw_gen_orig_bol_dest_fat.set_dw_options(sqlca, &
                             	  pcca.null_object, &
										  c_nonew + &
										  c_nomodify + &
										  c_nodelete + &
										  c_disablecc + &
										  c_disableccinsert, &
	                             c_viewmodeblack)
													 

l_criteriacolumn[1] = "cod_fornitore"
l_criteriacolumn[2] = "cod_tipo_bol_acq"

l_searchtable[1] = "tes_bol_acq"
l_searchtable[2] = "tes_bol_acq"

l_searchcolumn[1] = "cod_fornitore"
l_searchcolumn[2] = "cod_tipo_bol_acq"

dw_gen_fat_acq_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_lista_bolle_orig, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)

sle_filtro_data.fu_WireDW(dw_lista_bolle_orig, &
    "tes_bol_acq", &
    "data_bolla", &
    SQLCA)

save_on_close(c_socnosave)

// ------------------------------------------------------------------------------------------------------ //

if not isnull(s_cs_xx.parametri.parametro_s_14) and s_cs_xx.parametri.parametro_s_14 <> "" then 
	is_cod_fornitore = s_cs_xx.parametri.parametro_s_14
	dw_gen_fat_acq_ricerca.setitem(dw_gen_fat_acq_ricerca.getrow(),"cod_fornitore",is_cod_fornitore)
else
	setnull(is_cod_fornitore)
end if
	
if not isnull(s_cs_xx.parametri.parametro_ul_3) and s_cs_xx.parametri.parametro_ul_3 <> 0 then 
	il_anno_reg_fat_acq = s_cs_xx.parametri.parametro_ul_3
else
	setnull(il_anno_reg_fat_acq)
end if

if not isnull(s_cs_xx.parametri.parametro_ul_2) and s_cs_xx.parametri.parametro_ul_2 <> 0 then 
	il_num_reg_fat_acq = s_cs_xx.parametri.parametro_ul_2
else
	setnull(il_num_reg_fat_acq)
end if

setnull(s_cs_xx.parametri.parametro_s_14)
setnull(s_cs_xx.parametri.parametro_ul_3)
setnull(s_cs_xx.parametri.parametro_ul_2)

// ------------------------  segnalo se aggiungo ad una fattura già esistente -------------------
if isnull(il_anno_reg_fat_acq) or isnull(il_num_reg_fat_acq) then
	st_1.text = ""
	dw_folder.fu_selecttab(1)
else
	st_1.text = "ATTENZIONE: le bolle saranno caricate nella fattura " + string(il_anno_reg_fat_acq) + "/" + string(il_num_reg_fat_acq)
	cb_ricerca.postevent(clicked!)
end if

end event

on w_gen_fat_acq.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_ricerca=create cb_ricerca
this.cb_annulla=create cb_annulla
this.sle_filtro_data=create sle_filtro_data
this.st_filtro_data=create st_filtro_data
this.st_1=create st_1
this.cb_genera_fat=create cb_genera_fat
this.dw_gen_fat_acq_ricerca=create dw_gen_fat_acq_ricerca
this.dw_gen_orig_bol_dest_fat=create dw_gen_orig_bol_dest_fat
this.dw_lista_bolle_orig=create dw_lista_bolle_orig
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_ricerca
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.sle_filtro_data
this.Control[iCurrent+5]=this.st_filtro_data
this.Control[iCurrent+6]=this.st_1
this.Control[iCurrent+7]=this.cb_genera_fat
this.Control[iCurrent+8]=this.dw_gen_fat_acq_ricerca
this.Control[iCurrent+9]=this.dw_gen_orig_bol_dest_fat
this.Control[iCurrent+10]=this.dw_lista_bolle_orig
this.Control[iCurrent+11]=this.dw_folder
end on

on w_gen_fat_acq.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_ricerca)
destroy(this.cb_annulla)
destroy(this.sle_filtro_data)
destroy(this.st_filtro_data)
destroy(this.st_1)
destroy(this.cb_genera_fat)
destroy(this.dw_gen_fat_acq_ricerca)
destroy(this.dw_gen_orig_bol_dest_fat)
destroy(this.dw_lista_bolle_orig)
destroy(this.dw_folder)
end on

event pc_setddlb;call super::pc_setddlb;dw_gen_fat_acq_ricerca.fu_loadcode("cod_tipo_bol_acq", &
                       "tab_tipi_bol_acq", &
							  "cod_tipo_bol_acq", &
							  "des_tipo_bol_acq", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", "(Tutti)" )

end event

type cb_1 from commandbutton within w_gen_fat_acq
integer x = 2446
integer y = 1140
integer width = 357
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;long ll_riga

ll_riga = dw_gen_orig_bol_dest_fat.getrow()

if ll_riga > 0 then

	s_cs_xx.parametri.parametro_d_1 = dw_gen_orig_bol_dest_fat.getitemnumber( ll_riga, "anno_registrazione")
	
	s_cs_xx.parametri.parametro_d_2 = dw_gen_orig_bol_dest_fat.getitemnumber( ll_riga, "num_registrazione")
	
	window_open_parm(w_report_fat_acq_cc, -1, dw_gen_orig_bol_dest_fat)
	
end if

end event

type cb_ricerca from commandbutton within w_gen_fat_acq
integer x = 2496
integer y = 1148
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;// cb_search clicked event
dw_gen_fat_acq_ricerca.fu_BuildSearch(TRUE)
sle_filtro_data.fu_BuildSearch(false)
dw_folder.fu_SelectTab(2)
dw_lista_bolle_orig.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_annulla from commandbutton within w_gen_fat_acq
integer x = 2103
integer y = 1148
integer width = 366
integer height = 80
integer taborder = 60
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;dw_gen_fat_acq_ricerca.fu_Reset()
end event

type sle_filtro_data from u_sle_search within w_gen_fat_acq
integer x = 667
integer y = 1144
integer width = 1349
integer height = 80
integer taborder = 60
end type

type st_filtro_data from statictext within w_gen_fat_acq
integer x = 73
integer y = 1152
integer width = 581
integer height = 76
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Filtro su Data Ordine:"
alignment alignment = right!
boolean focusrectangle = false
end type

event getfocus;call super::getfocus;setnull(s_cs_xx.parametri.parametro_uo_dw_1)
s_cs_xx.parametri.parametro_uo_dw_search = dw_gen_fat_acq_ricerca
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore"
end event

type st_1 from statictext within w_gen_fat_acq
integer x = 46
integer y = 1160
integer width = 2405
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_genera_fat from commandbutton within w_gen_fat_acq
event clicked pbm_bnclicked
integer x = 2491
integer y = 1152
integer width = 375
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Gen. Fatture"
end type

event clicked;long ll_selected[]
integer li_ret
string ls_messaggio

dw_lista_bolle_orig.get_selected_rows(ll_selected[])
if upperbound(ll_selected) > 0 then
	if isnull(is_cod_fornitore) then
		window_open(w_ext_dati_fattura_nomag, 0)
		if isnull(s_cs_xx.parametri.parametro_s_1) or isnull(s_cs_xx.parametri.parametro_data_1) then return
		li_ret = wf_genera_fatture_acq(ll_selected, ref ls_messaggio)
	else
		li_ret = wf_carica_dett_fatture(ll_selected, ref ls_messaggio)
	end if
	
	if li_ret = -1 then 
		rollback;
		g_mb.messagebox("Generazione Fatture Acquisto", ls_messaggio, exclamation!)
	else
		commit;
	end if
	
	//09/06/06 claudia la riga sotto è stata inserita perchè staranamente non si vede la dw
	// però se la si commenta la si vede lo stesso, in pratica la window come è nel surcecontrol
	//è rotta, inserendo la riga, salvando e commentandola va in modo corretto??? Perchè?
	dw_gen_orig_bol_dest_fat.visible=true
	dw_folder.fu_selecttab(3)
	dw_lista_bolle_orig.triggerevent("pcd_retrieve")
else
	g_mb.messagebox("Generazione Fatture Acquisto", "Nessuna Bolla da Caricare", exclamation!)
end if
commit;
end event

type dw_gen_fat_acq_ricerca from u_dw_search within w_gen_fat_acq
event ue_imposta_date ( )
integer x = 50
integer y = 124
integer width = 2802
integer height = 1000
integer taborder = 60
string dataobject = "d_gen_fat_ven_selezione"
boolean border = false
end type

event ue_imposta_date();datetime ldt_data_fine, ldt_data_inizio
date ld_data_fine, ld_data_inizio

sle_filtro_data.text = ""

ldt_data_fine = getitemdatetime(getrow(),"data_fine")
ld_data_fine = date(ldt_data_fine)
ldt_data_inizio = getitemdatetime(getrow(),"data_inizio")
ld_data_inizio = date(ldt_data_inizio)

if not (isnull(ld_data_inizio) or ld_data_inizio <= date("01/01/1900")) and not (isnull(ld_data_fine) or ld_data_fine <= date("01/01/1900"))  then
	sle_filtro_data.text = string(ld_data_inizio,"dd/mm/yyyy") + " to " + string(ld_data_fine,"dd/mm/yyyy")	
elseif (isnull(ld_data_fine) or ld_data_fine <= date("01/01/1900")) and not (isnull(ld_data_inizio) or ld_data_inizio <= date("01/01/1900")) then
	sle_filtro_data.text = " >= " + string(ld_data_inizio, "dd/mm/yyyy")
elseif (isnull(ld_data_inizio) or ld_data_inizio <= date("01/01/1900")) and not (isnull(ld_data_fine) or ld_data_fine <= date("01/01/1900")) then
	sle_filtro_data.text = " <= " + string(ld_data_fine, "dd/mm/yyyy")
else
	sle_filtro_data.text = ""
end if

end event

event itemchanged;call super::itemchanged;postevent("ue_imposta_date")
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_gen_fat_acq_ricerca, "cod_fornitore")
end choose
end event

type dw_gen_orig_bol_dest_fat from uo_cs_xx_dw within w_gen_fat_acq
event pcd_retrieve pbm_custom60
integer x = 50
integer y = 112
integer width = 2752
integer height = 992
integer taborder = 10
string dataobject = "d_gen_orig_bol_dest_fat"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_print;call super::pcd_print;long job

job = PrintOpen( ) 

PrintDataWindow(job, this) 
PrintClose(job)
end event

type dw_lista_bolle_orig from uo_cs_xx_dw within w_gen_fat_acq
event pcd_retrieve pbm_custom60
integer x = 46
integer y = 124
integer width = 2816
integer height = 1008
integer taborder = 30
string dataobject = "d_lista_bolle_orig"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type dw_folder from u_folder within w_gen_fat_acq
integer x = 23
integer y = 16
integer width = 2880
integer height = 1244
integer taborder = 20
end type

