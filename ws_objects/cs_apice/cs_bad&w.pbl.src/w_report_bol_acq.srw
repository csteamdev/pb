﻿$PBExportHeader$w_report_bol_acq.srw
forward
global type w_report_bol_acq from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_bol_acq
end type
end forward

global type w_report_bol_acq from w_cs_xx_principale
integer width = 3849
integer height = 2724
string title = "Stampa D.d.T. Ingresso n°"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
dw_report dw_report
end type
global w_report_bol_acq w_report_bol_acq

type variables


integer				ii_anno_registrazione
long					il_num_registrazione
end variables

on w_report_bol_acq.create
int iCurrent
call super::create
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
end on

on w_report_bol_acq.destroy
call super::destroy
destroy(this.dw_report)
end on

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noresizewin)
ii_anno_registrazione = s_cs_xx.parametri.parametro_d_1
il_num_registrazione  = s_cs_xx.parametri.parametro_d_2

dw_report.ib_dw_report = true

iuo_dw_main = dw_report

dw_report.set_dw_options(		sqlca, &
										pcca.null_object, &
										c_nonew + &
										c_nomodify + &
										c_nodelete + &
										c_noenablenewonopen + &
										c_noenablemodifyonopen + &
										c_scrollparent + &
										c_disablecc, &
										c_noresizedw + &
										c_nohighlightselected + &
										c_nocursorrowfocusrect + &
										c_nocursorrowpointer)
											
dw_report.set_document_name("Bolla Acquisto " + string(ii_anno_registrazione) + "/" + string(il_num_registrazione))

save_on_close(c_socnosave)

dw_report.object.datawindow.print.preview = 'Yes'
dw_report.object.datawindow.print.preview.rulers = 'Yes'
dw_report.postevent("pcd_retrieve")

										

end event

type dw_report from uo_cs_xx_dw within w_report_bol_acq
integer x = 27
integer y = 36
integer width = 3776
integer height = 2576
integer taborder = 10
string dataobject = "d_report_bol_acq"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_retrieve;call super::pcd_retrieve;
//integer			li_anno
long				ll_errore//, ll_numero

//li_anno = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_bolla_acq")
//ll_numero = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_bolla_acq")

parent.title = "Stampa D.d.T. Ingresso n° " + string(ii_anno_registrazione) + "/" + string(il_num_registrazione)
set_document_name("DdT di acquisto " + string(ii_anno_registrazione) + "-" + string(il_num_registrazione))

ll_errore = retrieve(s_cs_xx.cod_azienda, ii_anno_registrazione, il_num_registrazione)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

