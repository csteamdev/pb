﻿$PBExportHeader$w_ricerca_bolle_acq.srw
forward
global type w_ricerca_bolle_acq from w_cs_xx_risposta
end type
type dw_ricerca_bolle from datawindow within w_ricerca_bolle_acq
end type
type st_ricerca_per from statictext within w_ricerca_bolle_acq
end type
type cb_ok from commandbutton within w_ricerca_bolle_acq
end type
type cb_chiudi from commandbutton within w_ricerca_bolle_acq
end type
type dw_stringa_ricerca_prodotti from datawindow within w_ricerca_bolle_acq
end type
type st_trovati from statictext within w_ricerca_bolle_acq
end type
type cb_azzera from commandbutton within w_ricerca_bolle_acq
end type
end forward

global type w_ricerca_bolle_acq from w_cs_xx_risposta
integer width = 3657
integer height = 1808
string title = "Ricerca Bolle di Acquisto"
boolean controlmenu = false
dw_ricerca_bolle dw_ricerca_bolle
st_ricerca_per st_ricerca_per
cb_ok cb_ok
cb_chiudi cb_chiudi
dw_stringa_ricerca_prodotti dw_stringa_ricerca_prodotti
st_trovati st_trovati
cb_azzera cb_azzera
end type
global w_ricerca_bolle_acq w_ricerca_bolle_acq

type variables
integer ii_tipo_ricerca
long il_row
end variables

forward prototypes
public function integer wf_carica ()
end prototypes

public function integer wf_carica ();datetime ldt_data_bolla

long    	ll_i, ll_y, ll_row, ll_anno_bolla, ll_num_bolla, ll_riga_bolla

string  	ls_num_fornitore, ls_cod_prodotto, ls_des_prodotto, ls_cod_fornitore, ls_rag_soc, ls_ricerca, ls_cursor


setpointer(hourglass!)

ls_ricerca = dw_stringa_ricerca_prodotti.gettext()

ll_i = len(ls_ricerca)

for ll_y = 1 to ll_i
	if mid(ls_ricerca, ll_y, 1) = "*" then
		ls_ricerca = replace(ls_ricerca, ll_y, 1, "%")
	end if
next

dw_ricerca_bolle.reset()

dw_ricerca_bolle.setredraw(false)

ls_cursor = "select det_bol_acq.anno_bolla_acq, " + &
				"       det_bol_acq.num_bolla_acq, " + &
				"		  det_bol_acq.prog_riga_bolla_acq, " + &
				"		  tes_bol_acq.data_registrazione, " + &
				"		  tes_bol_acq.num_bolla_fornitore, " + &
				"		  det_bol_acq.cod_prodotto, " + &
				"		  det_bol_acq.des_prodotto, " + &
				"		  tes_bol_acq.cod_fornitore, " + &
				"		  anag_fornitori.rag_soc_1 " + &
				"from   det_bol_acq, " + &
				"       tes_bol_acq, " + &
				"       anag_fornitori " + &
				"where  tes_bol_acq.cod_azienda = det_bol_acq.cod_azienda and " + &
				"       tes_bol_acq.anno_bolla_acq = det_bol_acq.anno_bolla_acq and " + &
				"       tes_bol_acq.num_bolla_acq = det_bol_acq.num_bolla_acq and " + &
				"       anag_fornitori.cod_azienda = tes_bol_acq.cod_azienda and " + &
				"       anag_fornitori.cod_fornitore = tes_bol_acq.cod_fornitore and " + &
				"       det_bol_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "' "

if not isnull(ls_ricerca) and ls_ricerca <> "" then

	choose case ii_tipo_ricerca
		
		case 1
						
			ls_cursor = ls_cursor + " and det_bol_acq.anno_bolla_acq = " + ls_ricerca
			
		case 2
			
			ls_cursor = ls_cursor + " and det_bol_acq.num_bolla_acq = " + ls_ricerca
			
		case 3
		
			ls_cursor = ls_cursor + " and det_bol_acq.prog_riga_bolla_acq = " + ls_ricerca
			
		case 4
		
			ls_cursor = ls_cursor + " and tes_bol_acq.data_registrazione = '" + &
			string(date(ls_ricerca),s_cs_xx.db_funzioni.formato_data) + "'"
			
		case 5
			
			ls_cursor = ls_cursor + " and tes_bol_acq.num_bolla_fornitore like '" + ls_ricerca + "'"
			
		case 6
			
			ls_cursor = ls_cursor + " and det_bol_acq.cod_prodotto like '" + ls_ricerca + "'"
			
		case 7
			
			ls_cursor = ls_cursor + " and det_bol_acq.des_prodotto like '" + ls_ricerca + "'"
			
		case 8
			
			ls_cursor = ls_cursor + " and tes_bol_acq.cod_fornitore like '" + ls_ricerca + "'"
			
		case 9
			
			ls_cursor = ls_cursor + " and anag_fornitori.rag_soc_1 like '" + ls_ricerca + "'"
			
	end choose
	
end if

ls_cursor = ls_cursor + " order by det_bol_acq.anno_bolla_acq ASC, det_bol_acq.num_bolla_acq ASC, " + &
								" det_bol_acq.prog_riga_bolla_acq ASC"

declare r_bolle dynamic cursor for sqlsa;

prepare sqlsa from :ls_cursor;

open dynamic r_bolle;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella open del cursore r_bolle: " + sqlca.sqlerrtext,stopsign!)
	setpointer(arrow!)
	return -1
end if

do while true
	
	fetch r_bolle
	into  :ll_anno_bolla,
			:ll_num_bolla,
			:ll_riga_bolla,
			:ldt_data_bolla,
			:ls_num_fornitore,
			:ls_cod_prodotto,
			:ls_des_prodotto,
			:ls_cod_fornitore,
			:ls_rag_soc;
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore nella fetch del cursore r_bolle: " + sqlca.sqlerrtext,stopsign!)
		close r_bolle;
		setpointer(arrow!)
		return -1
	elseif sqlca.sqlcode = 100 then
		exit
	end if
	
	ll_row = dw_ricerca_bolle.insertrow(0)
	dw_ricerca_bolle.setitem(ll_row,"anno_bolla",ll_anno_bolla)
	dw_ricerca_bolle.setitem(ll_row,"num_bolla",ll_num_bolla)
	dw_ricerca_bolle.setitem(ll_row,"riga_bolla",ll_riga_bolla)
	dw_ricerca_bolle.setitem(ll_row,"data_bolla",ldt_data_bolla)
	dw_ricerca_bolle.setitem(ll_row,"num_fornitore",ls_num_fornitore)
	dw_ricerca_bolle.setitem(ll_row,"cod_prodotto",ls_cod_prodotto)
	dw_ricerca_bolle.setitem(ll_row,"des_prodotto",ls_des_prodotto)
	dw_ricerca_bolle.setitem(ll_row,"cod_fornitore",ls_cod_fornitore)
	dw_ricerca_bolle.setitem(ll_row,"rag_soc",ls_rag_soc)

loop

close r_bolle;

dw_ricerca_bolle.setredraw(true)

setpointer(arrow!)

st_trovati.text = string(dw_ricerca_bolle.rowcount()) + " RECORD TROVATI"

return 0
end function

event pc_setwindow;call super::pc_setwindow;st_ricerca_per.text = "Ricerca per anno bolla"
ii_tipo_ricerca = 1
dw_stringa_ricerca_prodotti.insertrow(0)
end event

on w_ricerca_bolle_acq.create
int iCurrent
call super::create
this.dw_ricerca_bolle=create dw_ricerca_bolle
this.st_ricerca_per=create st_ricerca_per
this.cb_ok=create cb_ok
this.cb_chiudi=create cb_chiudi
this.dw_stringa_ricerca_prodotti=create dw_stringa_ricerca_prodotti
this.st_trovati=create st_trovati
this.cb_azzera=create cb_azzera
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ricerca_bolle
this.Control[iCurrent+2]=this.st_ricerca_per
this.Control[iCurrent+3]=this.cb_ok
this.Control[iCurrent+4]=this.cb_chiudi
this.Control[iCurrent+5]=this.dw_stringa_ricerca_prodotti
this.Control[iCurrent+6]=this.st_trovati
this.Control[iCurrent+7]=this.cb_azzera
end on

on w_ricerca_bolle_acq.destroy
call super::destroy
destroy(this.dw_ricerca_bolle)
destroy(this.st_ricerca_per)
destroy(this.cb_ok)
destroy(this.cb_chiudi)
destroy(this.dw_stringa_ricerca_prodotti)
destroy(this.st_trovati)
destroy(this.cb_azzera)
end on

type dw_ricerca_bolle from datawindow within w_ricerca_bolle_acq
event ue_key pbm_dwnkey
integer x = 23
integer y = 140
integer width = 3566
integer height = 1440
integer taborder = 20
string dataobject = "d_ricerca_bolle_acq"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event ue_key;CHOOSE CASE key
	CASE KeyEnter!
		il_row = getrow()
		cb_ok.triggerevent("clicked")
	Case keydownarrow! 
		if il_row < rowcount() then
			il_row = getrow() + 1
		end if
	Case keyuparrow!
		if il_row > 1 then
			il_row = getrow() - 1
		end if
END CHOOSE

end event

event clicked;choose case dwo.name
		
	case "t_anno"
		
		st_ricerca_per.text = "Ricerca per anno bolla"
		ii_tipo_ricerca = 1
		dw_stringa_ricerca_prodotti.setfocus()
		
	case "t_numero"
		
		st_ricerca_per.text = "Ricerca per numero bolla"
		ii_tipo_ricerca = 2
		dw_stringa_ricerca_prodotti.setfocus()
		
	case "t_riga"
		
		st_ricerca_per.text = "Ricerca per riga bolla"
		ii_tipo_ricerca = 3
		dw_stringa_ricerca_prodotti.setfocus()
	
	case "t_data"
		
		st_ricerca_per.text = "Ricerca per data bolla"
		ii_tipo_ricerca = 4
		dw_stringa_ricerca_prodotti.setfocus()
		
	case "t_num_fornitore"
		
		st_ricerca_per.text = "Ricerca per numero bolla fornitore"
		ii_tipo_ricerca = 5
		dw_stringa_ricerca_prodotti.setfocus()
		
	case "t_cod_prodotto"
		
		st_ricerca_per.text = "Ricerca per codice prodotto"
		ii_tipo_ricerca = 6
		dw_stringa_ricerca_prodotti.setfocus()
		
	case "t_des_prodotto"
		
		st_ricerca_per.text = "Ricerca per descrizione prodotto"
		ii_tipo_ricerca = 7
		dw_stringa_ricerca_prodotti.setfocus()
		
	case "t_cod_fornitore"
		
		st_ricerca_per.text = "Ricerca per codice fornitore"
		ii_tipo_ricerca = 8
		dw_stringa_ricerca_prodotti.setfocus()
		
	case "t_rag_soc"
		
		st_ricerca_per.text = "Ricerca per ragione sociale fornitore"
		ii_tipo_ricerca = 9
		dw_stringa_ricerca_prodotti.setfocus()
		
end choose
end event

event doubleclicked;if row > 0 then
	cb_ok.postevent(clicked!)
end if
end event

type st_ricerca_per from statictext within w_ricerca_bolle_acq
integer x = 23
integer y = 40
integer width = 983
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Ricerca per"
alignment alignment = right!
boolean focusrectangle = false
end type

type cb_ok from commandbutton within w_ricerca_bolle_acq
integer x = 3223
integer y = 1600
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ok"
end type

event clicked;if dw_ricerca_bolle.getrow() > 0 then

	if not isnull(s_cs_xx.parametri.parametro_uo_dw_1) then
		dw_ricerca_bolle.accepttext()
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
		s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, int(dw_ricerca_bolle.getitemnumber(dw_ricerca_bolle.getrow(),"anno_bolla")))
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
		s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, long(dw_ricerca_bolle.getitemnumber(dw_ricerca_bolle.getrow(),"num_bolla")))
		s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_3)
		s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_3, long(dw_ricerca_bolle.getitemnumber(dw_ricerca_bolle.getrow(),"riga_bolla")))
		s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
		s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)
	end if
	
	if il_row > 0 and not isnull(il_row) then
		dw_ricerca_bolle.setrow(il_row - 1)
		il_row = 0
	end if
	
	close(parent)
	
end if
end event

type cb_chiudi from commandbutton within w_ricerca_bolle_acq
integer x = 2834
integer y = 1600
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;close(parent)
end event

type dw_stringa_ricerca_prodotti from datawindow within w_ricerca_bolle_acq
event ue_key pbm_dwnkey
integer x = 1029
integer y = 20
integer width = 2560
integer height = 100
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_stringa_ricerca_prodotti"
end type

event ue_key;if key = keyenter! then
	wf_carica()
end if
end event

type st_trovati from statictext within w_ricerca_bolle_acq
integer x = 23
integer y = 1620
integer width = 2377
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_azzera from commandbutton within w_ricerca_bolle_acq
integer x = 2446
integer y = 1600
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Azzera"
end type

event clicked;long ll_null


setnull(ll_null)

s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_1)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_1, ll_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_2)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_2, ll_null)
s_cs_xx.parametri.parametro_uo_dw_1.setcolumn(s_cs_xx.parametri.parametro_s_3)
s_cs_xx.parametri.parametro_uo_dw_1.setitem(s_cs_xx.parametri.parametro_uo_dw_1.getrow(), s_cs_xx.parametri.parametro_s_3, ll_null)
s_cs_xx.parametri.parametro_uo_dw_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1.triggerevent(itemchanged!)

close(parent)
end event

