﻿$PBExportHeader$w_tes_bol_acq.srw
forward
global type w_tes_bol_acq from w_cs_xx_treeview
end type
type dw_1 from uo_cs_xx_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_2 from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_2 dw_2
end type
end forward

global type w_tes_bol_acq from w_cs_xx_treeview
integer width = 4626
integer height = 2280
string title = "Bolle di Acquisto"
event pc_menu_calcola ( )
event pc_menu_conferma ( )
event pc_menu_carica_ordine ( )
event pc_menu_corrisp ( )
event pc_menu_note ( )
event pc_menu_dettagli ( )
event pc_menu_stampa ( )
end type
global w_tes_bol_acq w_tes_bol_acq

type variables
private:
	string is_sql_prodotto
	long il_livello

	datastore ids_store
	
	int ICONA_CLIENTE, ICONA_FORNITORE, ICONA_PRODOTTO, ICONA_BOLLA,ICONA_BOLLA_CONFERMATA
	int ICONA_DEPOSITO, ICONA_PAGAMENTO, ICONA_TIPO_BOLLA, ICONA_NEW
	
	boolean ib_new
	
	string	is_cod_parametro_blocco_cliente="BDC", is_cod_parametro_blocco_fornitore="BDF"
	
end variables

forward prototypes
public subroutine wf_imposta_ricerca ()
public subroutine wf_valori_livelli ()
public function long wf_inserisci_clienti (long al_handle)
public function long wf_inserisci_fornitori (long al_handle)
public function long wf_inserisci_prodotti (long al_handle)
public function long wf_inserisci_bolla_acquisto (long al_handle, long al_anno_registrazione, long al_num_registrazione)
public function long wf_inserisci_bolle_acquisto (long al_handle)
public function long wf_inserisci_depositi (long al_handle)
public function long wf_inserisci_pagamenti (long al_handle)
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public function long wf_inserisci_tipi_bolle (long al_handle)
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public subroutine wf_treeview_icons ()
public subroutine wf_abilita_pulsanti_ricerca (boolean ab_status)
public function integer wf_cliente_fornitore (string fs_cod_tipo_bol_acq)
end prototypes

event pc_menu_calcola();string ls_messaggio

long   ll_num_registrazione, ll_anno_registrazione, ll_tab, ll_riga

uo_calcola_documento_euro luo_doc


if tab_dettaglio.det_1.dw_1.rowcount() < 1 then
	g_mb.messagebox("Bolle di acquisto", "nessuna bolla selezionata: calcolo bloccato")
	return
end if

ll_anno_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"anno_bolla_acq")
ll_num_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(),"num_bolla_acq")

luo_doc = create uo_calcola_documento_euro

if luo_doc.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_acq",ls_messaggio) <> 0 then
	g_mb.messagebox("Apice",ls_messaggio)
	destroy luo_doc
	rollback;
	return
else
	commit;
end if

destroy luo_doc

tab_dettaglio.det_1.dw_1.change_dw_current()
triggerevent("pc_retrieve")
end event

event pc_menu_conferma();integer li_anno_bolla_acq, li_ret
string ls_messaggio
long ll_progressivo, ll_num_bolla_acq

if tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_movimenti") = 'N' and &
	tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_blocco") = 'N' then
	li_anno_bolla_acq = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "anno_bolla_acq")
	ll_num_bolla_acq = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "num_bolla_acq")
	
	li_ret = f_conferma_bol_acq( li_anno_bolla_acq, ll_num_bolla_acq, ref ls_messaggio, "?" )
											
	if li_ret = -1 then
		rollback;
		g_mb.messagebox("Conferma Bolle Acquisto", ls_messaggio, exclamation!)
	elseif li_ret = 0 then
		commit;
		triggerevent("pc_retrieve")
		g_mb.success("Bolla confermata con successo")
	end if
else
	g_mb.messagebox("Conferma Bolle Acquisto", "Bolla Già Movimentata", exclamation!)
end if
end event

event pc_menu_carica_ordine();string ls_flag_tipo_bol_acq, ls_cod_tipo_bol_acq, ls_cod_fornitore
integer li_row, li_anno_bolla_acq
long ll_progressivo, ll_num_bolla_acq

li_row = tab_dettaglio.det_1.dw_1.getrow()

if tab_dettaglio.det_1.dw_1.getitemstring(li_row, "flag_movimenti") = "S" or &
	tab_dettaglio.det_1.dw_1.getitemstring(li_row, "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Bolla già movimentata.", exclamation!, ok!)
   return
end if

if li_row > 0 then
	ls_cod_tipo_bol_acq = tab_dettaglio.det_1.dw_1.getitemstring(li_row, "cod_tipo_bol_acq")

	select tipo_bol_acq
	into :ls_flag_tipo_bol_acq
	from tab_tipi_bol_acq
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_bol_acq = :ls_cod_tipo_bol_acq;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Gestione Bolle Acquisto","Errore Estrazione tipo Bolla")
	elseif ls_flag_tipo_bol_acq = "R" then //Reso non abbiamo il Fornitore
		g_mb.messagebox("Gestione Bolle Acquisto","Impossibile Caricare Ordini per i Clienti")
		return
	elseif ls_flag_tipo_bol_acq <> "R" then //Abbiamo il Fornitore
		ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(li_row, "cod_fornitore")
		ll_num_bolla_acq = tab_dettaglio.det_1.dw_1.getitemnumber(li_row, "num_bolla_acq")
		li_anno_bolla_acq = tab_dettaglio.det_1.dw_1.getitemnumber(li_row, "anno_bolla_acq")
		s_cs_xx.parametri.parametro_ul_3 = ll_num_bolla_acq
		s_cs_xx.parametri.parametro_s_14 = ls_cod_fornitore
		s_cs_xx.parametri.parametro_s_12 = "Bolla"
		s_cs_xx.parametri.parametro_i_1 = li_anno_bolla_acq
		
		window_open(w_carico_acquisti, -1)

		setnull(s_cs_xx.parametri.parametro_ul_3)
		setnull(s_cs_xx.parametri.parametro_s_14)
		setnull(s_cs_xx.parametri.parametro_s_12)
		setnull(s_cs_xx.parametri.parametro_i_1)

	end if
end if
end event

event pc_menu_corrisp();s_cs_xx.parametri.parametro_s_1 = "Bol_Acq"
window_open_parm(w_acq_ven_corr, -1, tab_dettaglio.det_1.dw_1)
end event

event pc_menu_note();s_cs_xx.parametri.parametro_s_1 = "Bol_Acq"
window_open_parm(w_acq_ven_note, -1, tab_dettaglio.det_1.dw_1)
end event

event pc_menu_dettagli();s_cs_xx.parametri.parametro_w_bol_acq = this

window_open_parm(w_det_bol_acq, -1, tab_dettaglio.det_1.dw_1)
end event

event pc_menu_stampa();integer				li_anno_bolla_acq, li_ret
string					ls_messaggio
long					ll_riga, ll_num_bolla_acq

ll_riga = tab_dettaglio.det_1.dw_1.getrow()

if ll_riga > 0 then
	//li_anno_bolla_acq = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "anno_bolla_acq")
	//ll_num_bolla_acq = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "num_bolla_acq")
	
	s_cs_xx.parametri.parametro_d_1 = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "anno_bolla_acq")
	s_cs_xx.parametri.parametro_d_2 = tab_dettaglio.det_1.dw_1.getitemnumber(ll_riga, "num_bolla_acq")
	
	window_open_parm(w_report_bol_acq, -1, tab_dettaglio.det_1.dw_1)
	
else
	g_mb.warning("Selezionare un D.d.T. da stampare!")
	return
end if

return
end event

public subroutine wf_imposta_ricerca ();long ll_anno_registrazione, ll_num_registrazione

is_sql_prodotto = ""
is_sql_filtro = ""
ll_anno_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "num_registrazione")

if not isnull(ll_anno_registrazione) and ll_anno_registrazione > 1900 then
	is_sql_filtro += " AND tes_bol_acq.anno_bolla_acq=" + string(ll_anno_registrazione)
end if
if not isnull(ll_num_registrazione) and ll_num_registrazione > 0 then
	is_sql_filtro += " AND tes_bol_acq.num_bolla_acq=" + string(ll_num_registrazione)
end if
if not isnull(ll_num_registrazione) and ll_num_registrazione > 0 and not isnull(ll_anno_registrazione) and ll_anno_registrazione > 1900 then
	// mi fermo non serve altro, filtro solo la singolo ordine
	return
end if

if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "num_bolla_fornitore")) then 
	is_sql_filtro += " AND tes_bol_acq.num_bolla_fornitore='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"num_bolla_fornitore") +"'"
end if
// ----

if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_registrazione")) then
	is_sql_filtro += " AND tes_bol_acq.data_registrazione='" +guo_functions.uof_data_db(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_registrazione")) +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_cliente")) then
	is_sql_filtro += " AND tes_bol_acq.cod_cliente='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_cliente") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fornitore")) then
	is_sql_filtro += " AND tes_bol_acq.cod_fornitore='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fornitore") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto")) then
	is_sql_filtro += " AND det_bol_acq.cod_prodotto='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto")) OR wf_has_livello("X") then
	is_sql_prodotto = " JOIN det_bol_acq on det_bol_acq.cod_azienda=tes_bol_acq.cod_azienda and "+&
							"det_bol_acq.anno_bolla_acq=tes_bol_acq.anno_bolla_acq and "+&
							"det_bol_acq.num_bolla_acq=tes_bol_acq.num_bolla_acq "
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito")) then
	is_sql_filtro += " AND tes_bol_acq.cod_deposito='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_pagamento")) then
	is_sql_filtro += " AND tes_bol_acq.cod_pagamento='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_pagamento") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_bolla")) then
	is_sql_filtro += " AND tes_bol_acq.cod_tipo_bol_acq='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_bolla") +"'"
end if

if  tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_gen_fat") <> 'X' then
	is_sql_filtro += " AND tes_bol_acq.flag_gen_fat='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_gen_fat") +"'"
end if
if tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_blocco") <> 'X'  then
	is_sql_filtro += " AND tes_bol_acq.flag_blocco='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_blocco") +"'"
end if
if tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_movimenti") <> 'X'  then
	is_sql_filtro += " AND tes_bol_acq.flag_movimenti='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_movimenti") +"'"
end if
end subroutine

public subroutine wf_valori_livelli ();wf_add_valore_livello("Non Specificato", "N")
wf_add_valore_livello("Cliente", "C")
wf_add_valore_livello("Fornitore", "F")
wf_add_valore_livello("Tipo Bolla", "T")
wf_add_valore_livello("Deposito", "D")
wf_add_valore_livello("Pagamento", "P")
wf_add_valore_livello("Prodotto", "X")
end subroutine

public function long wf_inserisci_clienti (long al_handle);string ls_sql, ls_label, ls_cod_cliente, ls_rag_soc_1, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_clienti.cod_cliente, anag_clienti.rag_soc_1 FROM tes_bol_acq &
LEFT OUTER JOIN anag_clienti ON anag_clienti.cod_azienda = tes_bol_acq.cod_azienda AND &
anag_clienti.cod_cliente = tes_bol_acq.cod_cliente " + is_sql_prodotto + " &
WHERE tes_bol_acq.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_cliente = ids_store.getitemstring(ll_i, 1)
	 ls_rag_soc_1 = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "C"
	lstr_data.codice = ls_cod_cliente
	
	if isnull(ls_cod_cliente) then
		ls_label = "<Cliente mancante>"
	elseif isnull(ls_rag_soc_1) then
		ls_label = ls_cod_cliente
	else
		ls_label = ls_cod_cliente + " - " + ls_rag_soc_1
	end if

	ltvi_item = wf_new_item(true, ICONA_CLIENTE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_fornitori (long al_handle);string ls_sql, ls_label, ls_cod_fornitore, ls_des_fornitore, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_fornitori.cod_fornitore, anag_fornitori.rag_soc_1 FROM tes_bol_acq &
LEFT OUTER JOIN anag_fornitori ON anag_fornitori.cod_azienda = tes_bol_acq.cod_azienda AND &
anag_fornitori.cod_fornitore = tes_bol_acq.cod_fornitore " + is_sql_prodotto + " &
WHERE tes_bol_acq.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows
	
	 ls_cod_fornitore = ids_store.getitemstring(ll_i, 1)
	 ls_des_fornitore = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "F"
	lstr_data.codice = ls_cod_fornitore
	
	if isnull(ls_cod_fornitore) and isnull(ls_des_fornitore) then
		ls_label = "<Fornitore mancante>"
	elseif isnull(ls_des_fornitore) then
		ls_label = ls_cod_fornitore
	else
		ls_label = ls_cod_fornitore + " - " + ls_des_fornitore
	end if

	ltvi_item = wf_new_item(true, ICONA_FORNITORE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_prodotti (long al_handle);string ls_sql, ls_label, ls_error, ls_cod_prodotto, ls_des_prodotto
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto FROM tes_bol_acq " + is_sql_prodotto + " &
LEFT OUTER JOIN anag_prodotti ON anag_prodotti.cod_azienda = tes_bol_acq.cod_azienda AND &
anag_prodotti.cod_prodotto = det_bol_acq.cod_prodotto &
WHERE tes_bol_acq.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_prodotto = ids_store.getitemstring(ll_i, 1)
	 ls_des_prodotto = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "X"
	lstr_data.codice = ls_cod_prodotto
	
	if isnull(ls_cod_prodotto) and isnull(ls_des_prodotto) then
		ls_label = "<Prodotto mancante>"
	elseif isnull(ls_des_prodotto) then
		ls_label = ls_cod_prodotto
	else
		ls_label = ls_cod_prodotto + " - " + ls_des_prodotto
	end if

	ltvi_item = wf_new_item(true, ICONA_PRODOTTO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_bolla_acquisto (long al_handle, long al_anno_registrazione, long al_num_registrazione);string ls_sql, ls_error
long ll_handle
treeviewitem ltvi_item

str_treeview lstr_data
	
lstr_data.livello = il_livello
lstr_data.tipo_livello = "N"
lstr_data.decimale[1] = al_anno_registrazione
lstr_data.decimale[2] = al_num_registrazione

ltvi_item = wf_new_item(false, ICONA_NEW)

ltvi_item.data = lstr_data
ltvi_item.label = "[NEW] " + string(al_anno_registrazione) + "/" + string(al_num_registrazione)
ltvi_item.selected = false

ib_fire_selection_changed = false

ll_handle = tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
tab_ricerca.selezione.tv_selezione.selectitem(ll_handle)

ib_fire_selection_changed = true

tab_ricerca.selezione.tv_selezione.event post selectionchanged(0, ll_handle)

return 1
end function

public function long wf_inserisci_bolle_acquisto (long al_handle);string ls_sql, ls_error, ls_label
long ll_rows, ll_i, ll_anno, ll_num
treeviewitem ltvi_item
str_treeview lstr_data


ls_sql = "SELECT tes_bol_acq.anno_bolla_acq, tes_bol_acq.num_bolla_acq, tes_bol_acq.flag_movimenti " + &
			"FROM tes_bol_acq " + is_sql_prodotto + " WHERE tes_bol_acq.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro
			
wf_leggi_parent(al_handle, ls_sql)

ls_sql += " ORDER BY tes_bol_acq.anno_bolla_acq, tes_bol_acq.num_bolla_acq"

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows
	
	ll_anno = ids_store.getitemnumber(ll_i, 1)
	ll_num = ids_store.getitemnumber(ll_i, 2)
	
	ls_label = string(ll_anno) + "/" + string(ll_num)
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "N"
	lstr_data.decimale[1] = ll_anno
	lstr_data.decimale[2] = ll_num
	
	if ids_store.getitemstring(ll_i, 3) = "S" then
		ltvi_item = wf_new_item(false, ICONA_BOLLA_CONFERMATA)
	else
		ltvi_item = wf_new_item(false, ICONA_BOLLA)
	end if
		
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
	
next

destroy ids_store

return ll_rows
end function

public function long wf_inserisci_depositi (long al_handle);string ls_sql, ls_label, ls_cod_deposito, ls_des_deposito, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_depositi.cod_deposito, anag_depositi.des_deposito FROM tes_bol_acq &
LEFT OUTER JOIN anag_depositi ON anag_depositi.cod_azienda = tes_bol_acq.cod_azienda AND &
anag_depositi.cod_deposito = tes_bol_acq.cod_deposito " + is_sql_prodotto + " &
WHERE tes_bol_acq.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_deposito = ids_store.getitemstring(ll_i, 1)
	 ls_des_deposito = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "D"
	lstr_data.codice = ls_cod_deposito
	
	if isnull(ls_cod_deposito) and isnull(ls_des_deposito) then
		ls_label = "<Deposito mancante>"
	elseif isnull(ls_des_deposito) then
		ls_label = ls_cod_deposito
	else
		ls_label = ls_cod_deposito + " - " + ls_des_deposito
	end if

	ltvi_item = wf_new_item(true, ICONA_DEPOSITO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_pagamenti (long al_handle);string ls_sql, ls_label, ls_cod_pagamento, ls_des_pagamento, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT tab_pagamenti.cod_pagamento, tab_pagamenti.des_pagamento FROM tes_bol_acq &
LEFT OUTER JOIN tab_pagamenti ON tab_pagamenti.cod_azienda = tes_bol_acq.cod_azienda AND &
tab_pagamenti.cod_pagamento = tes_bol_acq.cod_pagamento " + is_sql_prodotto + " &
WHERE tes_bol_acq.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_pagamento = ids_store.getitemstring(ll_i, 1)
	 ls_des_pagamento = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "P"
	lstr_data.codice = ls_cod_pagamento
	
	if isnull(ls_cod_pagamento) and isnull(ls_cod_pagamento) then
		ls_label = "<Pagamento mancante>"
	elseif isnull(ls_des_pagamento) then
		ls_label = ls_cod_pagamento
	else
		ls_label = ls_cod_pagamento + " - " + ls_des_pagamento
	end if

	ltvi_item = wf_new_item(true, ICONA_PAGAMENTO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_leggi_livello (long al_handle, integer ai_livello);il_livello = ai_livello

choose case wf_get_valore_livello(ai_livello)
		
	case "C"
		return wf_inserisci_clienti(al_handle)
		
	case "F"
		return wf_inserisci_fornitori(al_handle)
				
	case "T"
		return wf_inserisci_tipi_bolle(al_handle)
				
	case "D"
		return wf_inserisci_depositi(al_handle)
				
	case "P"
		return wf_inserisci_pagamenti(al_handle)
		
	case "X"
		return wf_inserisci_prodotti(al_handle)
						
	case else
		return wf_inserisci_bolle_acquisto(al_handle)
		
end choose
end function

public function long wf_inserisci_tipi_bolle (long al_handle);string ls_sql, ls_label, ls_cod_tipo_bol_ven, ls_des_tipo_bol_ven, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT tab_tipi_bol_acq.cod_tipo_bol_acq, tab_tipi_bol_acq.des_tipo_bol_acq FROM tes_bol_acq &
LEFT OUTER JOIN tab_tipi_bol_acq ON tab_tipi_bol_acq.cod_azienda = tes_bol_acq.cod_azienda AND &
tab_tipi_bol_acq.cod_tipo_bol_acq = tes_bol_acq.cod_tipo_bol_acq " + is_sql_prodotto + " &
WHERE tes_bol_acq.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_tipo_bol_ven = ids_store.getitemstring(ll_i, 1)
	 ls_des_tipo_bol_ven = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "T"
	lstr_data.codice = ls_cod_tipo_bol_ven
	
	if isnull(ls_cod_tipo_bol_ven) then
		ls_label = "<Tipo bolla mancante>"
	elseif isnull(ls_des_tipo_bol_ven) then
		ls_label = ls_cod_tipo_bol_ven
	else
		ls_label = ls_cod_tipo_bol_ven + " - " + ls_des_tipo_bol_ven
	end if

	ltvi_item = wf_new_item(true, ICONA_TIPO_BOLLA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_leggi_parent (long al_handle, ref string as_sql);long	ll_item
treeviewitem ltv_item
str_treeview lstr_data

if al_handle = 0 then return 0

do
	
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
		
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello
			
		case "C"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_bol_acq.cod_cliente is null "
			else
				as_sql += " AND tes_bol_acq.cod_cliente = '" + lstr_data.codice + "' "
			end if
			
		case "F"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_bol_acq.cod_fornitore is null "
			else
				as_sql += " AND tes_bol_acq.cod_fornitore = '" + lstr_data.codice + "' "
			end if

		case "T"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_bol_acq.cod_tipo_bol_acq is null "
			else
				as_sql += " AND tes_bol_acq.cod_tipo_bol_acq = '" + lstr_data.codice + "' "
			end if

		case "P"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_bol_acq.cod_pagamento is null "
			else
				as_sql += " AND tes_bol_acq.cod_pagamento = '" + lstr_data.codice + "' "
			end if
						
		case "D"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_bol_acq.cod_deposito is null "
			else
				as_sql += " AND tes_bol_acq.cod_deposito = '" + lstr_data.codice + "' "
			end if
					
		case "X"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND det_bol_acq.cod_prodotto is null "
			else
				as_sql += " AND det_bol_acq.cod_prodotto = '" + lstr_data.codice + "' "
			end if
			
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
end function

public subroutine wf_treeview_icons ();ICONA_BOLLA = wf_treeview_add_icon("treeview\documento_grigio.png")
ICONA_BOLLA_CONFERMATA =  wf_treeview_add_icon("treeview\documento_verde.png")
ICONA_DEPOSITO = wf_treeview_add_icon("treeview\deposito.png")
ICONA_PRODOTTO = wf_treeview_add_icon("treeview\prodotto.png")

ICONA_PAGAMENTO = wf_treeview_add_icon("treeview\offerta.png")
ICONA_TIPO_BOLLA = wf_treeview_add_icon("treeview\cartella.png")
ICONA_CLIENTE = wf_treeview_add_icon("treeview\cliente.png")
ICONA_FORNITORE = wf_treeview_add_icon("treeview\operatore.png")
ICONA_NEW = wf_treeview_add_icon("treeview\documento_new.png")
end subroutine

public subroutine wf_abilita_pulsanti_ricerca (boolean ab_status);tab_dettaglio.det_1.dw_1.object.b_ricerca_cliente.enabled = ab_status
tab_dettaglio.det_1.dw_1.object.b_ricerca_banca_for.enabled = ab_status
tab_dettaglio.det_1.dw_1.object.b_ricerca_fornitore.enabled = ab_status
end subroutine

public function integer wf_cliente_fornitore (string fs_cod_tipo_bol_acq);// fs_cod_tipo_bol_acq : codice tipo bolla Acquisto
// Se relativo tipo_bol_acq = 'R' (Reso) allra visualizzo i dati relativi al cliente altrimenti quelli
//										relativi al fornitore
string ls_flag_tipo_bol_acq

select tipo_bol_acq
into :ls_flag_tipo_bol_acq
from tab_tipi_bol_acq
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_bol_acq = :fs_cod_tipo_bol_acq;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Gestione Bolle Acquisto","Errore Estrazione tipo Bolla")
	return -1
else
	if ls_flag_tipo_bol_acq = "R" then //Reso = Cliente
		tab_dettaglio.det_1.dw_1.object.cod_fornitore.visible = 0
		tab_dettaglio.det_1.dw_1.object.cf_cod_fornitore.visible = 0
		tab_dettaglio.det_1.dw_1.object.cod_cliente.visible = 1
		tab_dettaglio.det_1.dw_1.object.cf_cod_cliente.visible = 1
		tab_dettaglio.det_1.dw_1.object.st_fornitore_cliente.text = "Cliente:"
		tab_dettaglio.det_1.dw_1.object.b_ricerca_cliente.visible = true
		tab_dettaglio.det_1.dw_1.object.b_ricerca_fornitore.visible = false

	elseif ls_flag_tipo_bol_acq <> "R" then //Acquisto o Conto Lavoro = Fornitore
		tab_dettaglio.det_1.dw_1.object.cod_fornitore.visible = 1
		tab_dettaglio.det_1.dw_1.object.cf_cod_fornitore.visible = 1
		tab_dettaglio.det_1.dw_1.object.cod_cliente.visible = 0
		tab_dettaglio.det_1.dw_1.object.cf_cod_cliente.visible = 0
		tab_dettaglio.det_1.dw_1.object.st_fornitore_cliente.text = "Fornitore:"
		tab_dettaglio.det_1.dw_1.object.b_ricerca_cliente.visible = false
		tab_dettaglio.det_1.dw_1.object.b_ricerca_fornitore.visible = true
	end if
	return 0
end if
end function

on w_tes_bol_acq.create
int iCurrent
call super::create
end on

on w_tes_bol_acq.destroy
call super::destroy
end on

event pc_setwindow;call super::pc_setwindow;// posizione dw
tab_dettaglio.det_1.dw_1.move(0,0)
tab_dettaglio.det_2.dw_2.move(0,0)
// ----

is_codice_filtro = "BLA"

tab_dettaglio.det_1.dw_1.set_dw_key("cod_azienda")
tab_dettaglio.det_1.dw_1.set_dw_key("anno_bolla_acq")
tab_dettaglio.det_1.dw_1.set_dw_key("num_bolla_acq")
tab_dettaglio.det_1.dw_1.set_dw_options(sqlca, i_openparm, c_scrollparent + c_noretrieveonopen, c_default)
tab_dettaglio.det_2.dw_2.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata + c_scrollparent, c_default)

tab_dettaglio.det_1.dw_1.ib_dw_detail = true
tab_dettaglio.det_2.dw_2.ib_dw_detail = true

iuo_dw_main = tab_dettaglio.det_1.dw_1

il_livello = 0
end event

event pc_setddlb;call super::pc_setddlb;/* --- RICERCA --- */
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
	"cod_deposito", &
	sqlca, &
	"anag_depositi", &
	"cod_deposito", &
	"des_deposito", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
	
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
	"cod_pagamento", &
	sqlca, &
	"tab_pagamenti", &
	"cod_pagamento", &
	"des_pagamento", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
	"cod_tipo_bolla", &
	sqlca, &
	"tab_tipi_bol_acq", &
	"cod_tipo_bol_acq", &
	"des_tipo_bol_acq", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
	
/* -- TESTATA -- */
string ls_select_operatori

if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + ")) and cod_operatore in (select cod_operatore from tab_operatori_utenti where cod_utente = '" + &
						  s_cs_xx.cod_utente + "')"
else
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + "))"
end if					  
							  
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_operatore", &
	sqlca, &
	"tab_operatori", &
	"cod_operatore", &
	"des_operatore", &
	ls_select_operatori)
	
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
                 "cod_tipo_bol_acq", &
                 sqlca, &
                 "tab_tipi_bol_acq", &
                 "cod_tipo_bol_acq", &
                 "des_tipo_bol_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_deposito", &
	sqlca, &
	"anag_depositi", &
	"cod_deposito", &
	"des_deposito", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_valuta", &
	sqlca, &
	"tab_valute", &
	"cod_valuta", &
	"des_valuta", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_tipo_listino_prodotto", &
	sqlca, &
	"tab_tipi_listini_prodotti", &
	"cod_tipo_listino_prodotto", &
	"des_tipo_listino_prodotto", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'A' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_pagamento", &
	sqlca, &
	"tab_pagamenti", &
	"cod_pagamento", &
	"des_pagamento", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_banca", &
	sqlca, &
	"anag_banche", &
	"cod_banca", &
	"des_banca", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_banca_clien_for", &
	sqlca, &
	"anag_banche_clien_for", &
	"cod_banca_clien_for", &
	"des_banca", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_mezzo", &
	sqlca, &
	"tab_mezzi", &
	"cod_mezzo", &
	"des_mezzo", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_porto", &
	sqlca, &
	"tab_porti", &
	"cod_porto", &
	"des_porto", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_natura_intra", &
	sqlca, &
	"tab_natura_intra", &
	"cod_natura_intra", &
	"descrizione", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")

			  
end event

event pc_view;call super::pc_view;ib_new = false
wf_abilita_pulsanti_ricerca(false)
end event

event pc_modify;call super::pc_modify;if tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_movimenti") = "S" or & 
	tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_blocco") = "S" then
	
   g_mb.warning("Bolla non modificabile! E' già stata confermata.")
   tab_dettaglio.det_1.dw_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

ib_new = false
wf_abilita_pulsanti_ricerca(true)
end event

event pc_new;call super::pc_new;ib_new = true
wf_abilita_pulsanti_ricerca(true)
end event

event pc_delete;call super::pc_delete;if tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_movimenti") = "S" or & 
	tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_blocco") = "S" then
	
   g_mb.warning("Bolla non modificabile! E' già stata confermata.")
   tab_dettaglio.det_1.dw_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

wf_abilita_pulsanti_ricerca(false)
end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_tes_bol_acq
integer width = 3191
integer height = 2076
det_2 det_2
end type

on tab_dettaglio.create
this.det_2=create det_2
call super::create
this.Control[]={this.det_1,&
this.det_2}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
end on

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
integer width = 3154
integer height = 1952
string text = "Testata"
dw_1 dw_1
end type

on det_1.create
this.dw_1=create dw_1
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on det_1.destroy
call super::destroy
destroy(this.dw_1)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_tes_bol_acq
integer height = 2136
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
integer height = 2012
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
integer height = 1968
string dataobject = "d_tes_bol_acq_ricerca_tv"
end type

event dw_ricerca::buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca,"cod_cliente")
				
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ricerca, "cod_fornitore")
		
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto")
		
end choose
end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
integer height = 2012
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
end type

event tv_selezione::itempopulate;call super::itempopulate;treeviewitem ltvi_item
str_treeview lstr_data

if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if wf_leggi_livello(handle, lstr_data.livello + 1) < 1 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if

end event

event tv_selezione::rightclicked;call super::rightclicked;long ll_row
treeviewitem ltvi_item
str_treeview lstr_data
m_bolle_acquisto lm_menu

if AncestorReturnValue < 0 then return

pcca.window_current = getwindow()
ll_row = tab_dettaglio.det_1.dw_1.getrow()

tab_ricerca.selezione.tv_selezione.getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if lstr_data.tipo_livello = "N" then

	lm_menu = create m_bolle_acquisto
		
	lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	
	destroy lm_menu
	
end if
end event

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data
	
tab_dettaglio.det_1.dw_1.change_dw_current()
getwindow().triggerevent("pc_retrieve")
end event

event tv_selezione::doubleclicked;call super::doubleclicked;treeviewitem ltvi_item
str_treeview lstr_data

if not wf_is_valid(handle) then return -1

tab_ricerca.selezione.tv_selezione.selectitem(handle)
getitem(handle, ltvi_item)
lstr_data = ltvi_item.data

if lstr_data.tipo_livello = "N" then getwindow().postevent("pc_menu_dettagli")
end event

type dw_1 from uo_cs_xx_dw within det_1
integer x = 14
integer y = 16
integer width = 3118
integer height = 1764
integer taborder = 30
string dataobject = "d_tes_bol_acq_1_tv"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

if not isvalid(istr_data) or isnull(istr_data) or UpperBound(istr_data.decimale) < 2 then return

ll_errore = retrieve(s_cs_xx.cod_azienda, istr_data.decimale[1],istr_data.decimale[2])

if ll_errore < 0 then
   pcca.error = c_fatal
end if

change_dw_current()
wf_cliente_fornitore(getitemstring(getrow(), "cod_tipo_bol_acq"))
end event

event pcd_new;call super::pcd_new;if i_extendmode then
	string ls_cod_tipo_bol_acq, ls_cod_operatore, ls_cod_deposito,ls_errore

   select cod_tipo_bol_acq
   into :ls_cod_tipo_bol_acq
   from con_bol_acq
   where cod_azienda = :s_cs_xx.cod_azienda;

	if sqlca.sqlcode = 0 then
		setitem(getrow(), "cod_tipo_bol_acq", ls_cod_tipo_bol_acq)
	end if
	
	setitem(getrow(), "data_bolla", datetime(today()))		
	setitem(getrow(), "data_registrazione", datetime(today()))
	setitem(getrow(), "anno_bolla_acq", f_anno_esercizio())
	setitem(getrow(), "anno_bolla_acq", 0)

	wf_cliente_fornitore(this.getitemstring(this.getrow(),"cod_tipo_bol_acq"))

	if s_cs_xx.cod_utente <> "CS_SYSTEM" then
		select cod_operatore
		into :ls_cod_operatore
		from tab_operatori_utenti
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_utente = :s_cs_xx.cod_utente and
				flag_default = 'S';
				
		if sqlca.sqlcode = -1 then
			g_mb.error("Errore durante l'Estrazione dell'Operatore di Default")
			pcca.error = c_fatal
			return
		elseif sqlca.sqlcode = 0 then
			setitem(getrow(), "cod_operatore", ls_cod_operatore)
		end if
	end if	

	// stefanop: 21/12/2011: aggiunto deposito collegato all'operatore
	guo_functions.uof_get_stabilimento_utente(ls_cod_deposito, ls_errore)
	if not isnull(ls_cod_deposito) then setitem(getrow(), "cod_deposito", ls_cod_deposito)
	// ----
end if

end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_bolla_acq, ll_num_bolla_acq

ll_anno_bolla_acq = f_anno_esercizio()

for ll_i = 1 to this.rowcount()
	if isnull(getitemstring(ll_i, "cod_azienda")) then
		setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if

	if isnull(getitemnumber(ll_i, "anno_bolla_acq")) or getitemnumber(ll_i, "anno_bolla_acq") = 0 then
		setitem(ll_i, "anno_bolla_acq", ll_anno_bolla_acq)
	end if
	
	if isnull(getitemnumber(ll_i, "num_bolla_acq")) or getitemnumber(ll_i, "num_bolla_acq") = 0 then
		select max(num_bolla_acq)
		into :ll_num_bolla_acq
		from tes_bol_acq
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_bolla_acq = :ll_anno_bolla_acq;
				
		if isnull(ll_num_bolla_acq) then ll_num_bolla_acq = 0
		
		ll_num_bolla_acq++

		setitem(ll_i, "num_bolla_acq", ll_num_bolla_acq)
	end if
next
end event

event updatestart;call super::updatestart;if i_extendmode then
	string ls_cod_fornitore, ls_cod_cliente, ls_null, ls_nota_testata, ls_nota_piede, ls_nota_video, ls_nota_old
	integer li_i
	long ll_anno_bolla_acq, ll_progressivo, ll_num_bolla_acq
	datetime ldt_data_registrazione
	
	for li_i = 1 to this.deletedcount()
		ll_anno_bolla_acq = this.getitemnumber(li_i, "anno_bolla_acq", delete!, true)
		ll_num_bolla_acq = this.getitemnumber(li_i, "num_bolla_acq", delete!, true)
	
		if f_del_bol_acq(ll_anno_bolla_acq, ll_num_bolla_acq) = -1 then
			return 1
		end if
		
		delete from iva_bol_acq
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_bolla_acq = :ll_anno_bolla_acq and
				num_bolla_acq = :ll_num_bolla_acq;
				
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore durante l'eliminazione delle righe iva. Dettaglio " + sqlca.sqlerrtext)
			return 1
		end if
	
		delete det_bol_acq_corrispondenze
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_bolla_acq = :ll_anno_bolla_acq and
				 num_bolla_acq = :ll_num_bolla_acq;
				 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione corrispondenze dettaglio bolle" + sqlca.sqlerrtext,stopsign!)
			return 1
		end if
	
		wf_set_deleted_item_status()
	next
	
	
	
	if getrow() > 0 then
		ll_anno_bolla_acq = getitemnumber(getrow(), "anno_bolla_acq")
		ll_num_bolla_acq = getitemnumber(getrow(), "num_bolla_acq")
				
		if ll_anno_bolla_acq > 0 then
			if getitemstring(getrow(), "flag_movimenti") = "S" then
				if not g_mb.confirm("ATTENZIONE! Si sta salvando le modifiche su una bolla già confermata;~nle modifiche non avranno effetto sui movimenti di magazzino.~nProseguo ?", 1) then
					set_dw_view(c_ignorechanges)
					pcca.error = c_fatal
					return 1
				end if
			end if
		end if
	
		if getitemstring(getrow(), "flag_blocco") = "S" then
			g_mb.messagebox("Attenzione", "Bolla non modificabile! E' Bloccata.", exclamation!, ok!)
			set_dw_view(c_ignorechanges)
			pcca.error = c_fatal
			return 1
		end if
		
   end if
end if

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_banca_for"
		guo_ricerca.uof_ricerca_banca_clifor(dw_1, "cod_banca_clien_for")
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_1, "cod_fornitore")
		
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_1, "cod_cliente")
		
end choose
end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string 		ls_null, ls_cod_cliente, ls_cod_deposito, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, ls_cod_fornitore, ls_messaggio
	integer 		li_ret
	double 		ld_sconto
	datetime 	ldt_data_bolla, ldt_data_registrazione
	uo_fido_cliente luo_fido_cliente

   setnull(ls_null)

   choose case dwo.name
      case "data_bolla"
         ls_cod_valuta = getitemstring(row, "cod_valuta")
         f_cambio_acq(ls_cod_valuta, datetime(date(data)))
			
	case "cod_valuta"
         ldt_data_bolla = getitemdatetime(row, "data_bolla")
         f_cambio_acq(data, ldt_data_bolla)
			
	case "cod_tipo_bol_acq"
		wf_cliente_fornitore(data)
			
      case "cod_cliente"
			
		ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
		luo_fido_cliente = create uo_fido_cliente
		li_ret = luo_fido_cliente.uof_get_blocco_cliente( data, ldt_data_registrazione, is_cod_parametro_blocco_cliente, ls_messaggio)
		if li_ret=2 then
			//blocco
			g_mb.error(ls_messaggio)
			this.setitem(i_rownbr, i_colname, ls_null)
			this.SetColumn ( i_colname )
			destroy luo_fido_cliente
			return 2
		elseif li_ret=1 then
			//solo warning
			g_mb.warning(ls_messaggio)
		end if
		destroy luo_fido_cliente
			
         select cod_deposito,
                cod_valuta,
                cod_tipo_listino_prodotto,
                cod_pagamento,
                sconto,
                cod_banca_clien_for
         into   :ls_cod_deposito,
                :ls_cod_valuta,
                :ls_cod_tipo_listino_prodotto,
                :ls_cod_pagamento,
                :ld_sconto,
                :ls_cod_banca_clien_for
         from anag_clienti
         where cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_cliente = :data;
 
         if sqlca.sqlcode = 0 then 
            this.setitem(row, "cod_deposito", ls_cod_deposito)
            this.setitem(row, "cod_valuta", ls_cod_valuta)
            this.setitem(row, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
            this.setitem(row, "cod_pagamento", ls_cod_pagamento)
            this.setitem(row, "sconto", ld_sconto)
            this.setitem(row, "cod_banca_clien_for", ls_cod_banca_clien_for)
            ldt_data_bolla = this.getitemdatetime(row, "data_bolla")
            f_cambio_acq(ls_cod_valuta, ldt_data_bolla)
         else
            this.setitem(row, "cod_deposito", ls_null)
            this.setitem(row, "cod_valuta", ls_null)
            this.setitem(row, "cod_tipo_listino_prodotto", ls_null)
            this.setitem(row, "cod_pagamento", ls_null)
            this.setitem(row, "sconto", ls_null)
            this.setitem(row, "cod_banca_clien_for", ls_null)
         end if 
			
      case "cod_fornitore"
		ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
		luo_fido_cliente = create uo_fido_cliente
		li_ret = luo_fido_cliente.uof_get_blocco_fornitore( data, ldt_data_registrazione, is_cod_parametro_blocco_fornitore, ls_messaggio)
		if li_ret=2 then
			//blocco
			g_mb.error(ls_messaggio)
			this.setitem(i_rownbr, i_colname, ls_null)
			this.SetColumn ( i_colname )
			destroy luo_fido_cliente
			return 2
		elseif li_ret=1 then
			//solo warning
			g_mb.warning(ls_messaggio)
		end if
		destroy luo_fido_cliente

         select cod_deposito,
                cod_valuta,
                cod_tipo_listino_prodotto,
                cod_pagamento,
                sconto,
                cod_banca_clien_for
         into   :ls_cod_deposito,
                :ls_cod_valuta,
                :ls_cod_tipo_listino_prodotto,
                :ls_cod_pagamento,
                :ld_sconto,
                :ls_cod_banca_clien_for
         from anag_fornitori
         where cod_azienda = :s_cs_xx.cod_azienda and 
				 cod_fornitore = :data;
 
         if sqlca.sqlcode = 0 then 
            this.setitem(row, "cod_deposito", ls_cod_deposito)
            this.setitem(row, "cod_valuta", ls_cod_valuta)
            this.setitem(row, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
            this.setitem(row, "cod_pagamento", ls_cod_pagamento)
            this.setitem(row, "sconto", ld_sconto)
            this.setitem(row, "cod_banca_clien_for", ls_cod_banca_clien_for)

            f_po_loaddddw_dw(this, &
                             "cod_fil_fornitore", &
                             sqlca, &
                             "anag_fil_fornitori", &
                             "cod_fil_fornitore", &
                             "rag_soc_1", &
                             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + i_coltext + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
    
            this.setitem(row, "cod_fil_fornitore", ls_null)

			ldt_data_bolla = this.getitemdatetime(row, "data_bolla")
			f_cambio_acq(ls_cod_valuta, ldt_data_bolla)
         else
            this.setitem(row, "cod_deposito", ls_null)
            this.setitem(row, "cod_valuta", ls_null)
            this.setitem(row, "cod_tipo_listino_prodotto", ls_null)
            this.setitem(row, "cod_pagamento", ls_null)
            this.setitem(row, "sconto", ls_null)
            this.setitem(row, "cod_banca_clien_for", ls_null)
            this.setitem(row, "cod_fil_fornitore", ls_null)
         end if 
		
   end choose
end if
end event

event updateend;call super::updateend;long ll_anno_bolla_acq, ll_num_bolla_acq

if rowsinserted > 0 then
	ll_anno_bolla_acq = getitemnumber(getrow(), "anno_bolla_acq")
	ll_num_bolla_acq = getitemnumber(getrow(), "num_bolla_acq")

	wf_inserisci_bolla_acquisto(0, ll_anno_bolla_acq, ll_num_bolla_acq)
end if
end event

type det_2 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3154
integer height = 1952
long backcolor = 12632256
string text = "Totali"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_2 dw_2
end type

on det_2.create
this.dw_2=create dw_2
this.Control[]={this.dw_2}
end on

on det_2.destroy
destroy(this.dw_2)
end on

type dw_2 from uo_cs_xx_dw within det_2
integer width = 3141
integer height = 1268
integer taborder = 11
string dataobject = "d_tes_bol_acq_2_tv"
boolean border = false
end type

