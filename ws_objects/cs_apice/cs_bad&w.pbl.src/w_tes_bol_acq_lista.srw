﻿$PBExportHeader$w_tes_bol_acq_lista.srw
$PBExportComments$Finestra testata bolle acquisto
forward
global type w_tes_bol_acq_lista from w_cs_xx_principale
end type
type cb_note from commandbutton within w_tes_bol_acq_lista
end type
type cb_calcola from commandbutton within w_tes_bol_acq_lista
end type
type cb_dettaglio from commandbutton within w_tes_bol_acq_lista
end type
type cb_corrispondenze from commandbutton within w_tes_bol_acq_lista
end type
type cb_carico_acquisti from commandbutton within w_tes_bol_acq_lista
end type
type cb_conferma_bolla from commandbutton within w_tes_bol_acq_lista
end type
type sle_filtro_data from u_sle_search within w_tes_bol_acq_lista
end type
type cb_annulla from commandbutton within w_tes_bol_acq_lista
end type
type cb_ricerca from commandbutton within w_tes_bol_acq_lista
end type
type st_filtro_data from statictext within w_tes_bol_acq_lista
end type
type dw_tes_bol_acq_det from uo_cs_xx_dw within w_tes_bol_acq_lista
end type
type dw_tes_bol_acq_sel from u_dw_search within w_tes_bol_acq_lista
end type
type dw_tes_bol_acq_lista from uo_cs_xx_dw within w_tes_bol_acq_lista
end type
type dw_folder from u_folder within w_tes_bol_acq_lista
end type
type dw_tes_bol_acq_det_1 from uo_cs_xx_dw within w_tes_bol_acq_lista
end type
end forward

global type w_tes_bol_acq_lista from w_cs_xx_principale
integer width = 3328
integer height = 1704
string title = "Bolle Entrata"
cb_note cb_note
cb_calcola cb_calcola
cb_dettaglio cb_dettaglio
cb_corrispondenze cb_corrispondenze
cb_carico_acquisti cb_carico_acquisti
cb_conferma_bolla cb_conferma_bolla
sle_filtro_data sle_filtro_data
cb_annulla cb_annulla
cb_ricerca cb_ricerca
st_filtro_data st_filtro_data
dw_tes_bol_acq_det dw_tes_bol_acq_det
dw_tes_bol_acq_sel dw_tes_bol_acq_sel
dw_tes_bol_acq_lista dw_tes_bol_acq_lista
dw_folder dw_folder
dw_tes_bol_acq_det_1 dw_tes_bol_acq_det_1
end type
global w_tes_bol_acq_lista w_tes_bol_acq_lista

type variables
boolean ib_apertura = false
long il_numero_riga
boolean ib_new = false


end variables

forward prototypes
public function integer wf_cliente_fornitore (string fs_cod_tipo_bol_acq)
end prototypes

public function integer wf_cliente_fornitore (string fs_cod_tipo_bol_acq);// fs_cod_tipo_bol_acq : codice tipo bolla Acquisto
// Se relativo tipo_bol_acq = 'R' (Reso) allra visualizzo i dati relativi al cliente altrimenti quelli
//										relativi al fornitore
string ls_flag_tipo_bol_acq

select tipo_bol_acq
 into :ls_flag_tipo_bol_acq
 from tab_tipi_bol_acq
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_tipo_bol_acq = :fs_cod_tipo_bol_acq;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Gestione Bolle Acquisto","Errore Estrazione tipo Bolla")
	return -1
else
	if ls_flag_tipo_bol_acq = "R" then //Reso = Cliente
		dw_tes_bol_acq_det.object.cod_fornitore.visible = 0
		dw_tes_bol_acq_det.object.cf_cod_fornitore.visible = 0
		dw_tes_bol_acq_det.object.cod_cliente.visible = 1
		dw_tes_bol_acq_det.object.cf_cod_cliente.visible = 1
		dw_tes_bol_acq_det.object.st_fornitore_cliente.text = "Cliente:"
		dw_tes_bol_acq_det.object.b_ricerca_cliente.visible = true
		dw_tes_bol_acq_det.object.b_ricerca_fornitore.visible = false

	elseif ls_flag_tipo_bol_acq <> "R" then //Acquisto o Conto Lavoro = Fornitore
		dw_tes_bol_acq_det.object.cod_fornitore.visible = 1
		dw_tes_bol_acq_det.object.cf_cod_fornitore.visible = 1
		dw_tes_bol_acq_det.object.cod_cliente.visible = 0
		dw_tes_bol_acq_det.object.cf_cod_cliente.visible = 0
		dw_tes_bol_acq_det.object.st_fornitore_cliente.text = "Fornitore:"
		dw_tes_bol_acq_det.object.b_ricerca_cliente.visible = false
		dw_tes_bol_acq_det.object.b_ricerca_fornitore.visible = true
	end if
	return 0
end if
end function

event pc_setwindow;call super::pc_setwindow;unsignedlong lul_modalita
string l_criteriacolumn[], l_searchtable[], l_searchcolumn[]
windowobject lw_oggetti[],l_objects[]

dw_tes_bol_acq_lista.set_dw_key("cod_azienda")
dw_tes_bol_acq_lista.set_dw_key("anno_bolla_acq")
dw_tes_bol_acq_lista.set_dw_key("num_bolla_acq")
dw_tes_bol_acq_lista.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_noretrieveonopen, &
                                    c_default)

dw_tes_bol_acq_det.set_dw_options(sqlca, &
												 dw_tes_bol_acq_lista, &
												 c_sharedata + c_scrollparent, &
												 c_default)
												 //dw_tes_bol_acq_det.set_dw_options(sqlca, &
//												 pcca.null_object, &
//												 c_noretrieveonopen, &
//												 c_default)
dw_tes_bol_acq_det_1.set_dw_options(sqlca, &
                                    dw_tes_bol_acq_det, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)																
												
												
iuo_dw_main = dw_tes_bol_acq_lista

dw_tes_bol_acq_sel.setitem(1, "anno_bolla_acq", f_anno_esercizio())

choose case s_cs_xx.parametri.parametro_s_1
   case "nuovo"
      lul_modalita = c_newonopen
   case "modifica"
      lul_modalita = c_modifyonopen
   case else
      lul_modalita = c_viewonopen
end choose

lw_oggetti[1] = dw_tes_bol_acq_sel
lw_oggetti[2] = cb_ricerca
lw_oggetti[3] = cb_annulla
lw_oggetti[4] = sle_filtro_data
lw_oggetti[5] = st_filtro_data
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])
lw_oggetti[1] = dw_tes_bol_acq_lista
lw_oggetti[2] = cb_conferma_bolla
lw_oggetti[3] = cb_corrispondenze
lw_oggetti[4] = cb_note
lw_oggetti[5] = cb_dettaglio
lw_oggetti[6] = cb_carico_acquisti
lw_oggetti[7] = cb_calcola
dw_folder.fu_assigntab(2, "Lista Bolle", lw_oggetti[])
lw_oggetti[1] = dw_tes_bol_acq_det_1
lw_oggetti[2] = cb_conferma_bolla
lw_oggetti[3] = cb_corrispondenze
lw_oggetti[4] = cb_note
lw_oggetti[5] = cb_dettaglio
lw_oggetti[6] = cb_carico_acquisti
lw_oggetti[7] = cb_calcola
dw_folder.fu_assigntab(4, "Totali", lw_oggetti[])
lw_oggetti[1] = dw_tes_bol_acq_det
lw_oggetti[2] = cb_conferma_bolla
lw_oggetti[3] = cb_corrispondenze
lw_oggetti[4] = cb_note
lw_oggetti[5] = cb_dettaglio
lw_oggetti[6] = cb_carico_acquisti
lw_oggetti[7] = cb_calcola
dw_folder.fu_assigntab(3, "Testata", lw_oggetti[])
dw_folder.fu_foldercreate(4, 4)
dw_folder.fu_selecttab(1)

l_criteriacolumn[1] = "anno_bolla_acq"
l_criteriacolumn[2] = "num_bolla_acq"
l_criteriacolumn[3] = "cod_cliente"
l_criteriacolumn[4] = "cod_fornitore"
l_criteriacolumn[5] = "cod_tipo_bolla"
l_criteriacolumn[6] = "flag_blocco"
l_criteriacolumn[7] = "flag_movimenti"
l_criteriacolumn[8] = "flag_gen_fat"
l_criteriacolumn[9] = "num_bolla_fornitore"


l_searchtable[1] = "tes_bol_acq"
l_searchtable[2] = "tes_bol_acq"
l_searchtable[3] = "tes_bol_acq"
l_searchtable[4] = "tes_bol_acq"
l_searchtable[5] = "tes_bol_acq"
l_searchtable[6] = "tes_bol_acq"
l_searchtable[7] = "tes_bol_acq"
l_searchtable[8] = "tes_bol_acq"
l_searchtable[9] = "tes_bol_acq"

l_searchcolumn[1] = "anno_bolla_acq"
l_searchcolumn[2] = "num_bolla_acq"
l_searchcolumn[3] = "cod_cliente"
l_searchcolumn[4] = "cod_fornitore"
l_searchcolumn[5] = "cod_tipo_bol_acq"
l_searchcolumn[6] = "flag_blocco"
l_searchcolumn[7] = "flag_movimenti"
l_searchcolumn[8] = "flag_gen_fat"
l_searchcolumn[9] = "num_bolla_fornitore"

dw_tes_bol_acq_sel.fu_wiredw(l_criteriacolumn[], &
                     dw_tes_bol_acq_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)
							
sle_filtro_data.fu_WireDW(dw_tes_bol_acq_lista, &
    "tes_bol_acq", &
    "data_bolla", &
    SQLCA)

ib_prima_riga = false
dw_tes_bol_acq_det.object.b_ricerca_banca_for.enabled = false
cb_dettaglio.enabled = false
end event

on w_tes_bol_acq_lista.create
int iCurrent
call super::create
this.cb_note=create cb_note
this.cb_calcola=create cb_calcola
this.cb_dettaglio=create cb_dettaglio
this.cb_corrispondenze=create cb_corrispondenze
this.cb_carico_acquisti=create cb_carico_acquisti
this.cb_conferma_bolla=create cb_conferma_bolla
this.sle_filtro_data=create sle_filtro_data
this.cb_annulla=create cb_annulla
this.cb_ricerca=create cb_ricerca
this.st_filtro_data=create st_filtro_data
this.dw_tes_bol_acq_det=create dw_tes_bol_acq_det
this.dw_tes_bol_acq_sel=create dw_tes_bol_acq_sel
this.dw_tes_bol_acq_lista=create dw_tes_bol_acq_lista
this.dw_folder=create dw_folder
this.dw_tes_bol_acq_det_1=create dw_tes_bol_acq_det_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_note
this.Control[iCurrent+2]=this.cb_calcola
this.Control[iCurrent+3]=this.cb_dettaglio
this.Control[iCurrent+4]=this.cb_corrispondenze
this.Control[iCurrent+5]=this.cb_carico_acquisti
this.Control[iCurrent+6]=this.cb_conferma_bolla
this.Control[iCurrent+7]=this.sle_filtro_data
this.Control[iCurrent+8]=this.cb_annulla
this.Control[iCurrent+9]=this.cb_ricerca
this.Control[iCurrent+10]=this.st_filtro_data
this.Control[iCurrent+11]=this.dw_tes_bol_acq_det
this.Control[iCurrent+12]=this.dw_tes_bol_acq_sel
this.Control[iCurrent+13]=this.dw_tes_bol_acq_lista
this.Control[iCurrent+14]=this.dw_folder
this.Control[iCurrent+15]=this.dw_tes_bol_acq_det_1
end on

on w_tes_bol_acq_lista.destroy
call super::destroy
destroy(this.cb_note)
destroy(this.cb_calcola)
destroy(this.cb_dettaglio)
destroy(this.cb_corrispondenze)
destroy(this.cb_carico_acquisti)
destroy(this.cb_conferma_bolla)
destroy(this.sle_filtro_data)
destroy(this.cb_annulla)
destroy(this.cb_ricerca)
destroy(this.st_filtro_data)
destroy(this.dw_tes_bol_acq_det)
destroy(this.dw_tes_bol_acq_sel)
destroy(this.dw_tes_bol_acq_lista)
destroy(this.dw_folder)
destroy(this.dw_tes_bol_acq_det_1)
end on

event pc_delete;call super::pc_delete;string ls_tabella
long ll_anno_registrazione, ll_num_registrazione
integer li_i


for li_i = 1 to dw_tes_bol_acq_lista.deletedcount()
	if dw_tes_bol_acq_lista.getitemstring(li_i, "flag_movimenti", delete!, true) = "S" then
	   g_mb.messagebox("Attenzione", "Bolla non cancellabile! E già stata confermata.", exclamation!, ok!)
	   dw_tes_bol_acq_lista.set_dw_view(c_ignorechanges)
		triggerevent("pc_retrieve")
	   pcca.error = c_fatal
	   return
	end if
	if dw_tes_bol_acq_lista.getitemstring(li_i, "flag_blocco", delete!, true) = "S" then
	   g_mb.messagebox("Attenzione", "Bolla non cancellabile! E' Bloccata.", exclamation!, ok!)
	   dw_tes_bol_acq_lista.set_dw_view(c_ignorechanges)
		triggerevent("pc_retrieve")
	   pcca.error = c_fatal
	   return
	end if
next

cb_dettaglio.enabled = false
dw_tes_bol_acq_det.object.b_ricerca_banca_for.enabled = false



end event

event pc_modify;call super::pc_modify;if dw_tes_bol_acq_lista.getitemstring(dw_tes_bol_acq_lista.getrow(), "flag_movimenti") = "S" then
   g_mb.messagebox("Attenzione", "ATTENZIONE !!! Questa bolla è confermata.~nLe modifiche che saranno apportate non avranno effetto sui movimenti di magazzino.~nI dettagli non sono modificabili.", exclamation!, ok!)
end if

if dw_tes_bol_acq_lista.getitemstring(dw_tes_bol_acq_lista.getrow(), "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Bolla non modificabile! E' Bloccata.", exclamation!, ok!)
   dw_tes_bol_acq_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

dw_tes_bol_acq_det.object.b_ricerca_banca_for.enabled = true
end event

event pc_setddlb;call super::pc_setddlb;string ls_select_operatori
if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + ")) and cod_operatore in (select cod_operatore from tab_operatori_utenti where cod_utente = '" + &
						  s_cs_xx.cod_utente + "')"
else
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + "))"
end if

dw_tes_bol_acq_sel.fu_loadcode("cod_tipo_bolla", &
					  "tab_tipi_bol_acq", &
					  "cod_tipo_bol_acq", &
					  "des_tipo_bol_acq", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "'", "(Tutti)" )
							  
							  
f_po_loaddddw_dw(dw_tes_bol_acq_det, &
                 "cod_operatore", &
                 sqlca, &
                 "tab_operatori", &
                 "cod_operatore", &
                 "des_operatore", &
					  ls_select_operatori)
f_po_loaddddw_dw(dw_tes_bol_acq_det, &
                 "cod_tipo_bol_acq", &
                 sqlca, &
                 "tab_tipi_bol_acq", &
                 "cod_tipo_bol_acq", &
                 "des_tipo_bol_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//f_po_loaddddw_dw(dw_tes_bol_acq_det, &
//                 "cod_cliente", &
//                 sqlca, &
//                 "anag_clienti", &
//                 "cod_cliente", &
//                 "rag_soc_1", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
//f_po_loaddddw_dw(dw_tes_bol_acq_det, &
//                 "cod_fornitore", &
//                 sqlca, &
//                 "anag_fornitori", &
//                 "cod_fornitore", &
//                 "rag_soc_1", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_bol_acq_det, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_bol_acq_det, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_bol_acq_det, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'A' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_bol_acq_det, &
                 "cod_pagamento", &
                 sqlca, &
                 "tab_pagamenti", &
                 "cod_pagamento", &
                 "des_pagamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_bol_acq_det, &
                 "cod_banca", &
                 sqlca, &
                 "anag_banche", &
                 "cod_banca", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_bol_acq_det, &
                 "cod_banca_clien_for", &
                 sqlca, &
                 "anag_banche_clien_for", &
                 "cod_banca_clien_for", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  							  
end event

event pc_new;call super::pc_new;dw_tes_bol_acq_det.object.b_ricerca_banca_for.enabled = true
end event

event pc_view;call super::pc_view;dw_tes_bol_acq_det.object.b_ricerca_banca_for.enabled = false
end event

type cb_note from commandbutton within w_tes_bol_acq_lista
integer x = 2505
integer y = 1480
integer width = 357
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Note"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "Bol_Acq"
window_open_parm(w_acq_ven_note, -1, dw_tes_bol_acq_det)
end event

event getfocus;call super::getfocus;dw_tes_bol_acq_det.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_banca_clien_for"
end event

type cb_calcola from commandbutton within w_tes_bol_acq_lista
integer x = 951
integer y = 1480
integer width = 357
integer height = 80
integer taborder = 180
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Calcola"
end type

event clicked;string ls_messaggio

long   ll_num_registrazione, ll_anno_registrazione, ll_tab, ll_riga

uo_calcola_documento_euro luo_doc


if dw_tes_bol_acq_det.rowcount() < 1 then
	g_mb.messagebox("Bolle di acquisto", "nessuna bolla selezionata: calcolo bloccato")
	return
end if

ll_anno_registrazione = dw_tes_bol_acq_det.getitemnumber(dw_tes_bol_acq_det.getrow(),"anno_bolla_acq")
ll_num_registrazione = dw_tes_bol_acq_det.getitemnumber(dw_tes_bol_acq_det.getrow(),"num_bolla_acq")

luo_doc = create uo_calcola_documento_euro

if luo_doc.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_acq",ls_messaggio) <> 0 then
	g_mb.messagebox("Apice",ls_messaggio)
	destroy luo_doc
	rollback;
	return -1
else
	commit;
end if

destroy luo_doc

dw_tes_bol_acq_det.change_dw_current()

ll_tab = dw_folder.i_selectedtab

ll_riga = dw_tes_bol_acq_lista.getrow()

cb_ricerca.triggerevent("clicked")

dw_tes_bol_acq_lista.setrow(ll_riga)

dw_folder.fu_selecttab(ll_tab)
end event

type cb_dettaglio from commandbutton within w_tes_bol_acq_lista
integer x = 2894
integer y = 1480
integer width = 357
integer height = 80
integer taborder = 130
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettagli"
end type

event clicked;LONG ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = dw_tes_bol_acq_det.getitemnumber(dw_tes_bol_acq_det.getrow(), "anno_bolla_acq")
ll_num_registrazione = dw_tes_bol_acq_det.getitemnumber(dw_tes_bol_acq_det.getrow(), "num_bolla_acq")
		
dw_tes_bol_acq_sel.setitem(1, "anno_bolla_acq", ll_anno_registrazione)
dw_tes_bol_acq_sel.setitem(1, "num_bolla_acq", ll_num_registrazione)

//cb_ricerca.triggerevent("clicked")

s_cs_xx.parametri.parametro_w_bol_acq = parent

window_open_parm(w_det_bol_acq, -1, dw_tes_bol_acq_lista)
end event

type cb_corrispondenze from commandbutton within w_tes_bol_acq_lista
integer x = 2117
integer y = 1480
integer width = 357
integer height = 80
integer taborder = 100
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corrispond."
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "Bol_Acq"
window_open_parm(w_acq_ven_corr, -1, dw_tes_bol_acq_det)


end event

type cb_carico_acquisti from commandbutton within w_tes_bol_acq_lista
integer x = 1728
integer y = 1480
integer width = 357
integer height = 80
integer taborder = 110
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C&arica Ord."
end type

event clicked;string ls_flag_tipo_bol_acq, ls_cod_tipo_bol_acq, ls_cod_fornitore
integer li_row, li_anno_bolla_acq
long ll_progressivo, ll_num_bolla_acq

li_row = dw_tes_bol_acq_det.getrow()

if dw_tes_bol_acq_det.getitemstring(li_row, "flag_movimenti") = "S" or &
	dw_tes_bol_acq_det.getitemstring(li_row, "flag_blocco") = "S" then
   g_mb.messagebox("Attenzione", "Bolla già movimentata.", &
              exclamation!, ok!)
   return
end if

if li_row > 0 then
	ls_cod_tipo_bol_acq = dw_tes_bol_acq_det.getitemstring(li_row, "cod_tipo_bol_acq")

	select tipo_bol_acq
	into :ls_flag_tipo_bol_acq
	from tab_tipi_bol_acq
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_bol_acq = :ls_cod_tipo_bol_acq;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Gestione Bolle Acquisto","Errore Estrazione tipo Bolla")
	elseif ls_flag_tipo_bol_acq = "R" then //Reso non abbiamo il Fornitore
		g_mb.messagebox("Gestione Bolle Acquisto","Impossibile Caricare Ordini per i Clienti")
		return
	elseif ls_flag_tipo_bol_acq <> "R" then //Abbiamo il Fornitore
		ls_cod_fornitore = dw_tes_bol_acq_det.getitemstring(li_row, "cod_fornitore")
		ll_num_bolla_acq = dw_tes_bol_acq_det.getitemnumber(li_row, "num_bolla_acq")
		li_anno_bolla_acq = dw_tes_bol_acq_det.getitemnumber(li_row, "anno_bolla_acq")
		s_cs_xx.parametri.parametro_ul_3 = ll_num_bolla_acq
		s_cs_xx.parametri.parametro_s_14 = ls_cod_fornitore
		s_cs_xx.parametri.parametro_s_12 = "Bolla"
		s_cs_xx.parametri.parametro_i_1 = li_anno_bolla_acq
		
		window_open(w_carico_acquisti, -1)

		setnull(s_cs_xx.parametri.parametro_ul_3)
		setnull(s_cs_xx.parametri.parametro_s_14)
		setnull(s_cs_xx.parametri.parametro_s_12)
		setnull(s_cs_xx.parametri.parametro_i_1)

	end if
end if
end event

type cb_conferma_bolla from commandbutton within w_tes_bol_acq_lista
integer x = 1339
integer y = 1480
integer width = 357
integer height = 80
integer taborder = 120
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Con&ferma"
end type

event clicked;integer li_anno_bolla_acq, li_ret
string ls_messaggio
long ll_progressivo, ll_num_bolla_acq

if dw_tes_bol_acq_det.getitemstring(dw_tes_bol_acq_det.getrow(), "flag_movimenti") = 'N' and &
	dw_tes_bol_acq_det.getitemstring(dw_tes_bol_acq_det.getrow(), "flag_blocco") = 'N' then
	li_anno_bolla_acq = dw_tes_bol_acq_det.getitemnumber(dw_tes_bol_acq_det.getrow(), "anno_bolla_acq")
	ll_num_bolla_acq = dw_tes_bol_acq_det.getitemnumber(dw_tes_bol_acq_det.getrow(), "num_bolla_acq")
	
	li_ret = f_conferma_bol_acq( li_anno_bolla_acq, ll_num_bolla_acq, &
											ref ls_messaggio, "?" )
											
	if li_ret = -1 then 
		g_mb.messagebox("Conferma Bolle Acquisto", ls_messaggio, exclamation!)
	else
		commit;
		parent.triggerevent("pc_retrieve")
	end if
else
	g_mb.messagebox("Conferma Bolle Acquisto", "Bolla Già Movimentata", exclamation!)
end if
end event

type sle_filtro_data from u_sle_search within w_tes_bol_acq_lista
integer x = 571
integer y = 660
integer width = 1029
integer height = 80
integer taborder = 40
end type

type cb_annulla from commandbutton within w_tes_bol_acq_lista
integer x = 1623
integer y = 660
integer width = 361
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;dw_tes_bol_acq_sel.fu_Reset()
end event

type cb_ricerca from commandbutton within w_tes_bol_acq_lista
integer x = 2011
integer y = 660
integer width = 361
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;// cb_search clicked event
dw_tes_bol_acq_sel.fu_BuildSearch(TRUE)
sle_filtro_data.fu_BuildSearch(false)
dw_tes_bol_acq_lista.change_dw_current()
parent.triggerevent("pc_retrieve")
dw_folder.fu_SelectTab(2)
end event

type st_filtro_data from statictext within w_tes_bol_acq_lista
integer x = 69
integer y = 680
integer width = 503
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 67108864
string text = "Filtro su Data Bolla:"
boolean focusrectangle = false
end type

type dw_tes_bol_acq_det from uo_cs_xx_dw within w_tes_bol_acq_lista
integer x = 46
integer y = 120
integer width = 3177
integer height = 1348
integer taborder = 50
string dataobject = "d_tes_bol_acq_det"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
   string ls_null, ls_cod_cliente, ls_cod_deposito, ls_cod_valuta, &
			 ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
          ls_cod_banca_clien_for, ls_cod_fornitore
   double ld_sconto
   datetime ldt_data_bolla

   setnull(ls_null)

   choose case i_colname
      case "data_bolla"
         ls_cod_valuta = this.getitemstring(i_rownbr, "cod_valuta")
         f_cambio_acq(ls_cod_valuta, datetime(date(i_coltext)))
      case "cod_cliente"
         select anag_clienti.cod_deposito,
                anag_clienti.cod_valuta,
                anag_clienti.cod_tipo_listino_prodotto,
                anag_clienti.cod_pagamento,
                anag_clienti.sconto,
                anag_clienti.cod_banca_clien_for
         into   :ls_cod_deposito,
                :ls_cod_valuta,
                :ls_cod_tipo_listino_prodotto,
                :ls_cod_pagamento,
                :ld_sconto,
                :ls_cod_banca_clien_for
         from   anag_clienti
         where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_clienti.cod_cliente = :i_coltext;
 
         if sqlca.sqlcode = 0 then 
            this.setitem(i_rownbr, "cod_deposito", ls_cod_deposito)
            this.setitem(i_rownbr, "cod_valuta", ls_cod_valuta)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
            this.setitem(i_rownbr, "cod_pagamento", ls_cod_pagamento)
            this.setitem(i_rownbr, "sconto", ld_sconto)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_cod_banca_clien_for)
            ldt_data_bolla = this.getitemdatetime(i_rownbr, "data_bolla")
            f_cambio_acq(ls_cod_valuta, ldt_data_bolla)
         else
            this.setitem(i_rownbr, "cod_deposito", ls_null)
            this.setitem(i_rownbr, "cod_valuta", ls_null)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_null)
            this.setitem(i_rownbr, "cod_pagamento", ls_null)
            this.setitem(i_rownbr, "sconto", ls_null)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_null)
         end if 
      case "cod_fornitore"
         select anag_fornitori.cod_deposito,
                anag_fornitori.cod_valuta,
                anag_fornitori.cod_tipo_listino_prodotto,
                anag_fornitori.cod_pagamento,
                anag_fornitori.sconto,
                anag_fornitori.cod_banca_clien_for
         into   :ls_cod_deposito,
                :ls_cod_valuta,
                :ls_cod_tipo_listino_prodotto,
                :ls_cod_pagamento,
                :ld_sconto,
                :ls_cod_banca_clien_for
         from   anag_fornitori
         where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_fornitori.cod_fornitore = :i_coltext;
 
         if sqlca.sqlcode = 0 then 
            this.setitem(i_rownbr, "cod_deposito", ls_cod_deposito)
            this.setitem(i_rownbr, "cod_valuta", ls_cod_valuta)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
            this.setitem(i_rownbr, "cod_pagamento", ls_cod_pagamento)
            this.setitem(i_rownbr, "sconto", ld_sconto)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_cod_banca_clien_for)

            f_po_loaddddw_dw(this, &
                             "cod_fil_fornitore", &
                             sqlca, &
                             "anag_fil_fornitori", &
                             "cod_fil_fornitore", &
                             "rag_soc_1", &
                             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + i_coltext + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
    
            this.setitem(i_rownbr, "cod_fil_fornitore", ls_null)

				ldt_data_bolla = this.getitemdatetime(i_rownbr, "data_bolla")
            f_cambio_acq(ls_cod_valuta, ldt_data_bolla)
         else
            this.setitem(i_rownbr, "cod_deposito", ls_null)
            this.setitem(i_rownbr, "cod_valuta", ls_null)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_null)
            this.setitem(i_rownbr, "cod_pagamento", ls_null)
            this.setitem(i_rownbr, "sconto", ls_null)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_null)
            this.setitem(i_rownbr, "cod_fil_fornitore", ls_null)
         end if 
		case "cod_valuta"
         ldt_data_bolla = this.getitemdatetime(i_rownbr, "data_bolla")
         f_cambio_acq(i_coltext, ldt_data_bolla)
		case "cod_tipo_bol_acq"
			wf_cliente_fornitore(i_coltext)
   end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_banca_for"
		guo_ricerca.uof_ricerca_banca_clifor(dw_tes_bol_acq_det,"cod_banca_clien_for")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_tes_bol_acq_det,"cod_fornitore")
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tes_bol_acq_det,"cod_cliente")
end choose
end event

type dw_tes_bol_acq_sel from u_dw_search within w_tes_bol_acq_lista
integer x = 46
integer y = 120
integer width = 3177
integer height = 800
integer taborder = 150
string dataobject = "d_tes_bol_acq_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tes_bol_acq_sel,"cod_cliente")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_tes_bol_acq_sel,"cod_fornitore")
end choose
end event

type dw_tes_bol_acq_lista from uo_cs_xx_dw within w_tes_bol_acq_lista
integer x = 46
integer y = 120
integer width = 3200
integer height = 1340
integer taborder = 170
string dataobject = "d_tes_bol_acq_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

event updatestart;call super::updatestart;if i_extendmode then
   integer li_i
	string ls_cod_fornitore, ls_cod_cliente, ls_null, ls_nota_testata, ls_nota_piede, ls_nota_video, ls_nota_old
   long ll_anno_bolla_acq, ll_progressivo, ll_num_bolla_acq
	datetime ldt_data_registrazione
	
   for li_i = 1 to this.deletedcount()
      ll_anno_bolla_acq = this.getitemnumber(li_i, "anno_bolla_acq", delete!, true)
      ll_num_bolla_acq = this.getitemnumber(li_i, "num_bolla_acq", delete!, true)

      if f_del_bol_acq(ll_anno_bolla_acq, ll_num_bolla_acq) = -1 then
			return 1
		end if
		delete from iva_bol_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_bolla_acq = :ll_anno_bolla_acq and
				 num_bolla_acq = :ll_num_bolla_acq;
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("APICE","Errore durante l'eliminazione delle righe iva. Dettaglio " + sqlca.sqlerrtext)
			return 1
		end if
		
		delete det_bol_acq_corrispondenze
		where  cod_azienda = :s_cs_xx.cod_azienda
		and    anno_bolla_acq = :ll_anno_bolla_acq
		and    num_bolla_acq = :ll_num_bolla_acq;
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione corrispondenze dettaglio bolle" + sqlca.sqlerrtext,stopsign!)
			return 1
		end if
		
   next
	
   if this.getrow() > 0 then
      ll_anno_bolla_acq = this.getitemnumber(this.getrow(), "anno_bolla_acq")
      ll_num_bolla_acq = this.getitemnumber(this.getrow(), "num_bolla_acq")
      if ll_anno_bolla_acq > 0 then
			if dw_tes_bol_acq_det.getitemstring(dw_tes_bol_acq_det.getrow(), "flag_movimenti") = "S" then
				if g_mb.messagebox("Attenzione", "ATTENZIONE! Si sta salvando le modifiche su una bolla già confermata;~n le modifiche non avranno effetto sui movimenti di magazzino.~nProseguo ?", Question!, YesNo!,1) = 2 then
					dw_tes_bol_acq_det.set_dw_view(c_ignorechanges)
					pcca.error = c_fatal
					return 1
				end if
			end if
			
			if dw_tes_bol_acq_det.getitemstring(dw_tes_bol_acq_det.getrow(), "flag_blocco") = "S" then
				g_mb.messagebox("Attenzione", "Bolla non modificabile! E' Bloccata.", exclamation!, ok!)
				dw_tes_bol_acq_det.set_dw_view(c_ignorechanges)
				pcca.error = c_fatal
				return 1
			end if
      end if
		
//		if this.getitemstatus(this.getrow(), 0, primary!) = NewModified!  then
//			setnull(ls_null)
//			ls_cod_fornitore = this.getitemstring(this.getrow(), "cod_fornitore")
//			ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
//			if f_crea_nota_fissa(ls_null, ls_cod_fornitore, "BOL_ACQ", ldt_data_registrazione, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) <> -1 then
//				ls_nota_old = this.getitemstring(this.getrow(), "nota_testata")
//				if isnull(ls_nota_old) then ls_nota_old = ""
//				this.setitem(this.getrow(), "nota_testata", ls_nota_testata + ls_nota_old )
//				ls_nota_old = this.getitemstring(this.getrow(), "nota_piede")
//				if isnull(ls_nota_old) then ls_nota_old = ""
//				this.setitem(this.getrow(), "nota_piede", ls_nota_piede + ls_nota_old )
//			end if
//		end if
		
   end if
end if

end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
elseif ll_errore > 0 then
	wf_cliente_fornitore(this.getitemstring(this.getrow(),"cod_tipo_bol_acq"))
end if

end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
dw_tes_bol_acq_det.object.b_ricerca_banca_for.enabled = true
dw_tes_bol_acq_det.object.b_ricerca_cliente.enabled = true
dw_tes_bol_acq_det.object.b_ricerca_fornitore.enabled = true
   cb_dettaglio.enabled = false
	cb_note.enabled = false
	cb_corrispondenze.enabled = false
	cb_conferma_bolla.enabled = false
	cb_carico_acquisti.enabled = false
	ib_new = true
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_cod_tipo_bol_acq, ls_cod_operatore, ls_cod_deposito, ls_errore
	long ll_num_bolla_acq, ll_anno_bolla_acq

   select cod_tipo_bol_acq
   into   :ls_cod_tipo_bol_acq
   from   con_bol_acq
   where  cod_azienda = :s_cs_xx.cod_azienda;

   if sqlca.sqlcode = 0 then
      setitem(getrow(), "cod_tipo_bol_acq", ls_cod_tipo_bol_acq)
	end if
	setitem(getrow(), "data_bolla", datetime(today()))		
	setitem(getrow(), "data_registrazione", datetime(today()))
	setitem(getrow(), "anno_bolla_acq", f_anno_esercizio())
	setitem(getrow(), "anno_bolla_acq", 0)
	wf_cliente_fornitore(this.getitemstring(this.getrow(),"cod_tipo_bol_acq"))

	if s_cs_xx.cod_utente <> "CS_SYSTEM" then
		select cod_operatore
		  into :ls_cod_operatore
		  from tab_operatori_utenti
		 where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_utente = :s_cs_xx.cod_utente and
				 flag_default = 'S';
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Estrazione Operatori", "Errore durante l'Estrazione dell'Operatore di Default")
			pcca.error = c_fatal
			return
		elseif sqlca.sqlcode = 0 then
			this.setitem(this.getrow(), "cod_operatore", ls_cod_operatore)
		end if
	end if	
	
	// stefanop: 21/12/2011: aggiunto deposito collegato all'operatore
	guo_functions.uof_get_stabilimento_utente(ls_cod_deposito, ls_errore)
	if not isnull(ls_cod_deposito) then setitem(getrow(), "cod_deposito", ls_cod_deposito)
	// ----

dw_tes_bol_acq_det.object.b_ricerca_banca_for.enabled = true
dw_tes_bol_acq_det.object.b_ricerca_cliente.enabled = true
dw_tes_bol_acq_det.object.b_ricerca_fornitore.enabled = true
   cb_dettaglio.enabled = false
	cb_note.enabled = false
	cb_corrispondenze.enabled = false
	cb_conferma_bolla.enabled = false
	cb_carico_acquisti.enabled = false
	ib_new = true
	dw_folder.fu_selecttab(3)
end if

end event

event pcd_save;call super::pcd_save;if i_extendmode then
	long ll_anno_registrazione, ll_num_registrazione
   if pcca.error = c_success then
dw_tes_bol_acq_det.object.b_ricerca_cliente.enabled = false
dw_tes_bol_acq_det.object.b_ricerca_fornitore.enabled = false
dw_tes_bol_acq_det.object.b_ricerca_banca_for.enabled = false

      if this.getrow() > 0 and this.getitemnumber(this.getrow(), "anno_bolla_acq") > 0 then
         cb_dettaglio.enabled = true
			cb_note.enabled = true
			cb_corrispondenze.enabled = true
			cb_conferma_bolla.enabled = true
			cb_carico_acquisti.enabled = true
      else
         cb_dettaglio.enabled = false
			cb_note.enabled = false
			cb_corrispondenze.enabled = false
			cb_conferma_bolla.enabled = false
			cb_carico_acquisti.enabled = false
      end if
   end if
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	
dw_tes_bol_acq_det.object.b_ricerca_cliente.enabled = false
dw_tes_bol_acq_det.object.b_ricerca_fornitore.enabled = false
dw_tes_bol_acq_det.object.b_ricerca_banca_for.enabled = false
	
   if this.getrow() > 0 and this.getitemnumber(this.getrow(), "anno_bolla_acq") > 0 then
      cb_dettaglio.enabled = true
		cb_note.enabled = true
		cb_corrispondenze.enabled = true
		cb_conferma_bolla.enabled = true
		cb_carico_acquisti.enabled = true
   else
      cb_dettaglio.enabled = false
		cb_note.enabled = false
		cb_corrispondenze.enabled = false
		cb_conferma_bolla.enabled = false
		cb_carico_acquisti.enabled = false
   end if
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_bolla_acq, ll_num_bolla_acq

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_bolla_acq")) or this.getitemnumber(ll_i, "anno_bolla_acq") = 0 then
		ll_anno_bolla_acq = f_anno_esercizio()
		this.setitem(ll_i, "anno_bolla_acq", ll_anno_bolla_acq)
	else
		ll_anno_bolla_acq = this.getitemnumber(ll_i, "anno_bolla_acq")
	end if
	
   if isnull(this.getitemnumber(ll_i, "num_bolla_acq")) or this.getitemnumber(ll_i, "num_bolla_acq") = 0 then
		select max(num_bolla_acq)
		into   :ll_num_bolla_acq
		from   tes_bol_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_bolla_acq = :ll_anno_bolla_acq;
		if isnull(ll_num_bolla_acq) or ll_num_bolla_acq = 0 then
			this.setitem(ll_i, "num_bolla_acq", 1)
		else
			this.setitem(ll_i, "num_bolla_acq", ll_num_bolla_acq + 1)
		end if
	end if
next
end event

type dw_folder from u_folder within w_tes_bol_acq_lista
integer x = 23
integer y = 20
integer width = 3246
integer height = 1560
integer taborder = 160
end type

type dw_tes_bol_acq_det_1 from uo_cs_xx_dw within w_tes_bol_acq_lista
integer x = 46
integer y = 120
integer width = 3200
integer height = 1340
integer taborder = 10
string dataobject = "d_tes_bol_acq_det_1"
boolean border = false
end type

