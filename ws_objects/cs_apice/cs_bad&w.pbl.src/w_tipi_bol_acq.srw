﻿$PBExportHeader$w_tipi_bol_acq.srw
$PBExportComments$Finestra Gestione Tipi Bolle Acquisto
forward
global type w_tipi_bol_acq from w_cs_xx_principale
end type
type dw_tipi_bol_acq_lista from uo_cs_xx_dw within w_tipi_bol_acq
end type
type dw_tipi_bol_acq_det from uo_cs_xx_dw within w_tipi_bol_acq
end type
end forward

global type w_tipi_bol_acq from w_cs_xx_principale
integer width = 2226
integer height = 1660
string title = "Gestione Tipi Bolle Acquisto"
dw_tipi_bol_acq_lista dw_tipi_bol_acq_lista
dw_tipi_bol_acq_det dw_tipi_bol_acq_det
end type
global w_tipi_bol_acq w_tipi_bol_acq

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tipi_bol_acq_lista.set_dw_key("cod_azienda")
dw_tipi_bol_acq_lista.set_dw_options(sqlca, &
                                     pcca.null_object, &
                                     c_default, &
                                     c_default)
dw_tipi_bol_acq_det.set_dw_options(sqlca, &
                                   dw_tipi_bol_acq_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)
iuo_dw_main = dw_tipi_bol_acq_lista
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tipi_bol_acq_det, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_bol_acq_det, &
                 "cod_tipo_movimento", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_bol_acq_det, &
                 "cod_tipo_analisi", &
                 sqlca, &
                 "tab_tipi_analisi", &
                 "cod_tipo_analisi", &
                 "des_tipo_analisi", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_bol_acq_det, &
                 "cod_tipo_fat_acq", &
                 sqlca, &
                 "tab_tipi_fat_acq", &
                 "cod_tipo_fat_acq", &
                 "des_tipi_fat_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_tipi_bol_acq_det, &
                 "cod_tipo_det_acq_rif", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_det_acq = 'N'")

end event

on w_tipi_bol_acq.create
int iCurrent
call super::create
this.dw_tipi_bol_acq_lista=create dw_tipi_bol_acq_lista
this.dw_tipi_bol_acq_det=create dw_tipi_bol_acq_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_bol_acq_lista
this.Control[iCurrent+2]=this.dw_tipi_bol_acq_det
end on

on w_tipi_bol_acq.destroy
call super::destroy
destroy(this.dw_tipi_bol_acq_lista)
destroy(this.dw_tipi_bol_acq_det)
end on

type dw_tipi_bol_acq_lista from uo_cs_xx_dw within w_tipi_bol_acq
integer x = 18
integer y = 20
integer width = 2144
integer height = 500
integer taborder = 10
string dataobject = "d_tipi_bol_acq_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event pcd_new;call super::pcd_new;dw_tipi_bol_acq_det.setitem(dw_tipi_bol_acq_lista.getrow(), "flag_doc_suc", "S")
dw_tipi_bol_acq_det.setitem(dw_tipi_bol_acq_lista.getrow(), "flag_st_note", "S")
end event

type dw_tipi_bol_acq_det from uo_cs_xx_dw within w_tipi_bol_acq
integer x = 18
integer y = 540
integer width = 2149
integer height = 1000
integer taborder = 20
string dataobject = "d_tipi_bol_acq_det"
borderstyle borderstyle = styleraised!
end type

