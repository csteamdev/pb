﻿$PBExportHeader$w_con_bol_ven.srw
$PBExportComments$Finestra Gestione Parametri Bolle Vendita
forward
global type w_con_bol_ven from w_cs_xx_principale
end type
type dw_con_bol_ven from uo_cs_xx_dw within w_con_bol_ven
end type
end forward

global type w_con_bol_ven from w_cs_xx_principale
int Width=2643
int Height=549
boolean TitleBar=true
string Title="Gestione Parametri Bolle Vendita"
dw_con_bol_ven dw_con_bol_ven
end type
global w_con_bol_ven w_con_bol_ven

on pc_setddlb;call w_cs_xx_principale::pc_setddlb;f_po_loaddddw_dw(dw_con_bol_ven, &
                 "cod_tipo_bol_ven", &
                 sqlca, &
                 "tab_tipi_bol_ven", &
                 "cod_tipo_bol_ven", &
                 "des_tipo_bol_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_con_bol_ven, &
                 "cod_tipo_det_ven_ant", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_con_bol_ven, &
                 "cod_tipo_det_ven_con", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end on

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_con_bol_ven.set_dw_key("cod_azienda")
dw_con_bol_ven.set_dw_options(sqlca, &
                              pcca.null_object, &
                              c_default, &
                              c_default)

end on

on w_con_bol_ven.create
int iCurrent
call w_cs_xx_principale::create
this.dw_con_bol_ven=create dw_con_bol_ven
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_con_bol_ven
end on

on w_con_bol_ven.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_con_bol_ven)
end on

type dw_con_bol_ven from uo_cs_xx_dw within w_con_bol_ven
int X=23
int Y=21
int Width=2561
int Height=401
string DataObject="d_con_bol_ven"
BorderStyle BorderStyle=StyleRaised!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

