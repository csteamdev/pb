﻿$PBExportHeader$w_conferma_bol_ven.srw
$PBExportComments$Finestra Conferma Bolle di Vendita
forward
global type w_conferma_bol_ven from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_conferma_bol_ven
end type
type cb_conferma from uo_cb_ok within w_conferma_bol_ven
end type
type st_1 from statictext within w_conferma_bol_ven
end type
type st_2 from statictext within w_conferma_bol_ven
end type
type st_3 from statictext within w_conferma_bol_ven
end type
type st_4 from statictext within w_conferma_bol_ven
end type
type cb_cerca from commandbutton within w_conferma_bol_ven
end type
type cb_annulla from commandbutton within w_conferma_bol_ven
end type
type dw_sel_conferma_bolle from uo_cs_xx_dw within w_conferma_bol_ven
end type
type dw_folder from u_folder within w_conferma_bol_ven
end type
type mle_1 from multilineedit within w_conferma_bol_ven
end type
type dw_conferma_bol_ven from uo_cs_xx_dw within w_conferma_bol_ven
end type
end forward

global type w_conferma_bol_ven from w_cs_xx_principale
integer width = 3538
integer height = 1912
string title = "Conferma Bolle"
cb_1 cb_1
cb_conferma cb_conferma
st_1 st_1
st_2 st_2
st_3 st_3
st_4 st_4
cb_cerca cb_cerca
cb_annulla cb_annulla
dw_sel_conferma_bolle dw_sel_conferma_bolle
dw_folder dw_folder
mle_1 mle_1
dw_conferma_bol_ven dw_conferma_bol_ven
end type
global w_conferma_bol_ven w_conferma_bol_ven

type variables

end variables

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]


set_w_options(c_noenablepopup)
lw_oggetti[1] = mle_1
lw_oggetti[2] = cb_1
dw_folder.fu_assigntab(3, "Errori", lw_oggetti[])
lw_oggetti[1] = dw_sel_conferma_bolle
lw_oggetti[2] = cb_cerca
lw_oggetti[3] = cb_annulla
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])
lw_oggetti[1] = dw_conferma_bol_ven
lw_oggetti[2] = cb_conferma
lw_oggetti[3] = st_2
lw_oggetti[4] = st_3
dw_folder.fu_assigntab(2, "Da Confermare", lw_oggetti[])
dw_conferma_bol_ven.set_dw_options(sqlca, &
                                   pcca.null_object, &
                                   c_multiselect + &
						    			     c_nonew + &
							    		     c_nomodify + &
											  c_nodelete + &
											  c_noretrieveonopen + &
											  c_disablecc + &
											  c_disableccinsert, &
                                   c_default)
											  
dw_sel_conferma_bolle.set_dw_options(sqlca, &
                                   pcca.null_object, &
						    			     c_newonopen + &
							    		     c_nomodify + &
											  c_nodelete + &
											  c_noretrieveonopen + &
											  c_disablecc + &
											  c_disableccinsert, &
                                   c_default)

dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

save_on_close(c_socnosave)

cb_annulla.postevent("clicked")
end event

on w_conferma_bol_ven.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.cb_conferma=create cb_conferma
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.st_4=create st_4
this.cb_cerca=create cb_cerca
this.cb_annulla=create cb_annulla
this.dw_sel_conferma_bolle=create dw_sel_conferma_bolle
this.dw_folder=create dw_folder
this.mle_1=create mle_1
this.dw_conferma_bol_ven=create dw_conferma_bol_ven
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.cb_conferma
this.Control[iCurrent+3]=this.st_1
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.st_3
this.Control[iCurrent+6]=this.st_4
this.Control[iCurrent+7]=this.cb_cerca
this.Control[iCurrent+8]=this.cb_annulla
this.Control[iCurrent+9]=this.dw_sel_conferma_bolle
this.Control[iCurrent+10]=this.dw_folder
this.Control[iCurrent+11]=this.mle_1
this.Control[iCurrent+12]=this.dw_conferma_bol_ven
end on

on w_conferma_bol_ven.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.cb_conferma)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.cb_cerca)
destroy(this.cb_annulla)
destroy(this.dw_sel_conferma_bolle)
destroy(this.dw_folder)
destroy(this.mle_1)
destroy(this.dw_conferma_bol_ven)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_sel_conferma_bolle,"cod_tipo",sqlca,"tab_tipi_bol_ven",&
					  "cod_tipo_bol_ven","des_tipo_bol_ven","cod_azienda = '" + &
					  s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw( dw_sel_conferma_bolle, &
                  "cod_causale", &
						sqlca, &
						"tab_causali_trasp", &
					   "cod_causale", &
						"des_causale", &
						"cod_azienda = '" + s_cs_xx.cod_azienda + "'")					  
					  
					  
f_po_loaddddw_dw(dw_sel_conferma_bolle, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
end event

event getfocus;call super::getfocus;dw_sel_conferma_bolle.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type cb_1 from commandbutton within w_conferma_bol_ven
integer x = 3063
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;long li_prt

li_prt = PrintOpen("Lista bolle non confermate")

Print(li_prt, "Lista bolle non confermate  - "  &
	+ String(Today(), "dd/mm/yyyy")  &
	+ " - "  &
	+ String(Now(), "HH:MM:SS"))
Print(li_prt, " ")
Print(li_prt, mle_1.text)

PrintClose(li_prt)
end event

type cb_conferma from uo_cb_ok within w_conferma_bol_ven
event clicked pbm_bnclicked
integer x = 3063
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Conferma"
end type

event clicked;call super::clicked;string ls_tes_tabella, ls_det_tabella, ls_prog_riga, ls_quantita, ls_messaggio, ls_str, &
       ls_cod_documento, ls_numeratore
long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_num_righe, ll_anno_documento, &
     ll_num_documento, li_result
datetime ldt_data_documento

setpointer(hourglass!)
mle_1.text = ""
ll_num_righe = dw_conferma_bol_ven.rowcount()
for ll_i = 1 to ll_num_righe
	ll_anno_documento = dw_conferma_bol_ven.getitemnumber(ll_i, "anno_documento")
   ll_num_documento  = dw_conferma_bol_ven.getitemnumber(ll_i, "num_documento")
	ls_cod_documento  = dw_conferma_bol_ven.getitemstring(ll_i, "cod_documento")
	ls_numeratore     = dw_conferma_bol_ven.getitemstring(ll_i, "numeratore_documento")
   ldt_data_documento    = dw_conferma_bol_ven.getitemdatetime(ll_i, "data_bolla")
	ll_anno_registrazione = dw_conferma_bol_ven.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione  = dw_conferma_bol_ven.getitemnumber(ll_i, "num_registrazione")
	
	st_3.text = ls_cod_documento + " / " + string(ll_anno_documento, "####") + " - " + ls_numeratore + " / " + string(ll_num_documento, "#####0") + "  " + string(ldt_data_documento, "dd/mm/yyyy") + "  (" + string(ll_anno_registrazione, "#####0") + "/" + string(ll_num_registrazione, "#####0") + ")"
	
	ls_tes_tabella = "tes_bol_ven"
	ls_det_tabella = "det_bol_ven"
	ls_prog_riga = "prog_riga_bol_ven"
	ls_quantita = "quan_consegnata"

	li_result = f_conferma_mov_mag(ls_tes_tabella, &
								 ls_det_tabella, &
								 ls_prog_riga, &
								 ls_quantita, &
								 ll_anno_registrazione, &
								 ll_num_registrazione, &
								 ls_messaggio)
	if li_result = -1 then
		ls_str = "Errore durante la conferma della bolla~r~nDettaglio errore:" + ls_messaggio + "~r~n"
		mle_1.text = mle_1.text + ls_str
		rollback;
	elseif li_result = 0 then
		commit;
	end if
next
commit;
dw_conferma_bol_ven.triggerevent("pcd_retrieve")

setpointer(arrow!)
end event

type st_1 from statictext within w_conferma_bol_ven
integer x = 23
integer y = 1280
integer width = 3429
integer height = 240
boolean bringtotop = true
integer textsize = -11
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "La CONFERMA della bolla provvede alla scrittura delle registrazioni sul GIORNALE DI MAGAZZINO. Non sarà più possibile modificare i documenti confermati e neppure cancellare i movimenti di magazzino così generati."
alignment alignment = center!
boolean focusrectangle = false
end type

type st_2 from statictext within w_conferma_bol_ven
integer x = 46
integer y = 1160
integer width = 160
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Bolla:"
boolean focusrectangle = false
end type

type st_3 from statictext within w_conferma_bol_ven
integer x = 229
integer y = 1160
integer width = 2418
integer height = 80
boolean bringtotop = true
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type st_4 from statictext within w_conferma_bol_ven
integer x = 23
integer y = 1520
integer width = 3429
integer height = 280
boolean bringtotop = true
integer textsize = -18
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean underline = true
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "ATTENZIONE: TUTTE LE BOLLE PRESENTI NELLA LISTA SARANNO CONFERMATE"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_cerca from commandbutton within w_conferma_bol_ven
integer x = 3063
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C&erca"
end type

event clicked;long     ll_anno, ll_num_da, ll_num_a

string   ls_cliente, ls_tipo, ls_select, ls_cod_causale, ls_cod_deposito

datetime ldt_data_da, ldt_data_a


dw_sel_conferma_bolle.accepttext()

ls_cliente = dw_sel_conferma_bolle.getitemstring( 1, "cod_cliente")

ls_tipo = dw_sel_conferma_bolle.getitemstring( 1, "cod_tipo")

ll_anno = dw_sel_conferma_bolle.getitemnumber( 1, "anno")

ll_num_da = dw_sel_conferma_bolle.getitemnumber( 1, "da_num")

ll_num_a = dw_sel_conferma_bolle.getitemnumber( 1, "a_num")

ldt_data_da = dw_sel_conferma_bolle.getitemdatetime( 1, "da_data")

ldt_data_a = dw_sel_conferma_bolle.getitemdatetime( 1, "a_data")

ls_cod_causale = dw_sel_conferma_bolle.getitemstring( 1, "cod_causale")

ls_cod_deposito = dw_sel_conferma_bolle.getitemstring( 1, "cod_deposito")

ls_select = &
"select cod_tipo_bol_ven, " + &
"       anno_registrazione, " + &
"       num_registrazione, " + &
"       data_registrazione, " + &
"       cod_documento, " + &
"       anno_documento, " + &
"       numeratore_documento, " + &
"       num_documento, " + &
"       data_bolla, " + &
"       cod_cliente, " + &
"       cod_fornitore " + &
"from   tes_bol_ven " + &
"where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
"       cod_documento is not null and " + &
"       flag_movimenti = 'N' and " + &
"       flag_blocco = 'N' and " + &
"       num_documento > 0 "

if not isnull(ls_cod_deposito) then
	ls_select = ls_select + " and cod_deposito = '" + ls_cod_deposito + "'"
end if

if not isnull(ls_cliente) then
	ls_select = ls_select + " and cod_cliente = '" + ls_cliente + "'"
end if

if not isnull(ls_tipo) then
	ls_select = ls_select + " and cod_tipo_bol_ven = '" + ls_tipo + "'"
end if

if not isnull(ll_anno) then
	ls_select = ls_select + " and anno_documento = " + string(ll_anno)
end if

if not isnull(ll_num_da) then
	ls_select = ls_select + " and num_documento >= " + string(ll_num_da)
end if

if not isnull(ll_num_a) then
	ls_select = ls_select + " and num_documento <= " + string(ll_num_a)
end if

if not isnull(ldt_data_da) then
	ls_select = ls_select + " and data_bolla >= '" + string(ldt_data_da,s_cs_xx.db_funzioni.formato_data) + "'"
end if

if not isnull(ldt_data_a) then
	ls_select = ls_select + " and data_bolla <= '" + string(ldt_data_a,s_cs_xx.db_funzioni.formato_data) + "'"
end if

if not isnull(ls_cod_causale) then
	ls_select = ls_select + " and cod_causale = '" + ls_cod_causale + "' "
end if

ls_select = ls_select + " order by anno_registrazione asc, num_registrazione asc"

if dw_conferma_bol_ven.setsqlselect(ls_select) = -1 then
	g_mb.messagebox("APICE","Errore in impostazione select!",stopsign!)
	return -1
end if

dw_folder.fu_selecttab(2)

dw_conferma_bol_ven.change_dw_current()

parent.triggerevent("pc_retrieve")
end event

type cb_annulla from commandbutton within w_conferma_bol_ven
integer x = 2674
integer y = 1160
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;long     ll_null

string   ls_null

datetime ldt_null


setnull(ll_null)

setnull(ls_null)

setnull(ldt_null)

dw_sel_conferma_bolle.setitem(1,"cod_deposito",ls_null)

dw_sel_conferma_bolle.setitem(1,"cod_cliente",ls_null)

dw_sel_conferma_bolle.setitem(1,"cod_tipo",ls_null)

dw_sel_conferma_bolle.setitem(1,"da_data",ldt_null)

dw_sel_conferma_bolle.setitem(1,"a_data",ldt_null)

dw_sel_conferma_bolle.setitem(1,"anno",ll_null)

dw_sel_conferma_bolle.setitem(1,"da_num",ll_null)

dw_sel_conferma_bolle.setitem(1,"a_num",ll_null)
end event

type dw_sel_conferma_bolle from uo_cs_xx_dw within w_conferma_bol_ven
integer x = 46
integer y = 140
integer width = 3406
integer height = 592
integer taborder = 30
string dataobject = "d_sel_conferma_bolle"
end type

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_sel_conferma_bolle,"cod_cliente")
end choose
end event

type dw_folder from u_folder within w_conferma_bol_ven
integer x = 23
integer y = 20
integer width = 3451
integer height = 1240
integer taborder = 10
end type

type mle_1 from multilineedit within w_conferma_bol_ven
integer x = 46
integer y = 140
integer width = 3383
integer height = 1000
integer taborder = 11
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean hscrollbar = true
boolean vscrollbar = true
boolean autohscroll = true
boolean autovscroll = true
end type

type dw_conferma_bol_ven from uo_cs_xx_dw within w_conferma_bol_ven
event pcd_retrieve pbm_custom60
integer x = 41
integer y = 140
integer width = 3406
integer height = 1000
integer taborder = 20
string dataobject = "d_conferma_bol_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

