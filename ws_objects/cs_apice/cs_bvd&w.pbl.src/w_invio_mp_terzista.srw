﻿$PBExportHeader$w_invio_mp_terzista.srw
forward
global type w_invio_mp_terzista from window
end type
type cb_annulla from commandbutton within w_invio_mp_terzista
end type
type cb_ok from commandbutton within w_invio_mp_terzista
end type
type dw_lista from datawindow within w_invio_mp_terzista
end type
type dw_ricerca from u_dw_search within w_invio_mp_terzista
end type
end forward

global type w_invio_mp_terzista from window
integer width = 4206
integer height = 2228
boolean titlebar = true
string title = "Invio MP a terzista da Commessa/Semilavorato"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
event ue_imposta_ricerca ( )
cb_annulla cb_annulla
cb_ok cb_ok
dw_lista dw_lista
dw_ricerca dw_ricerca
end type
global w_invio_mp_terzista w_invio_mp_terzista

type variables


string					is_cod_dep_prelievo, is_cod_dep_dest, is_cod_tipo_det_ven
end variables

forward prototypes
public function integer wf_materie_prime (integer ai_anno_commessa, long al_num_commessa, string as_cod_semilavorato, string as_cod_deposito_prelievo, string as_cod_deposito_dest, ref string as_errore)
public function integer wf_get_giacenza_deposito (string as_cod_prodotto, string as_cod_deposito, datetime adt_data_rif, ref double add_giacenza, ref string as_errore)
public subroutine wf_cerca ()
end prototypes

event ue_imposta_ricerca();
if is_cod_tipo_det_ven<>"" and not isnull(is_cod_tipo_det_ven) then dw_ricerca.setitem(1, "cod_tipo_det_ven", is_cod_tipo_det_ven)

//carico la dropdown solo per i tipi dettaglio vendita usato per
//Prodotti a Magazzino e Prodotti non Collegati
f_po_loaddddw_dw(dw_ricerca, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_det_ven in ('M','C')")

end event

public function integer wf_materie_prime (integer ai_anno_commessa, long al_num_commessa, string as_cod_semilavorato, string as_cod_deposito_prelievo, string as_cod_deposito_dest, ref string as_errore);
uo_funzioni_1			luo_f1

datetime					ldt_data_a, ldt_data_chiusura

dec{4}					ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[], ld_quan_prodotta, ld_quan_ordine

string						ls_deposito_mp, ls_where, ls_materia_prima[], ls_versione_prima[], ls_chiave[], ls_sl, ls_flag_tipo_avanzamento

integer					li_ret
double					ldd_quantita_utilizzo[], ldd_giacenza
long						ll_new, ll_index, ll_find



cb_ok.enabled = false

if ai_anno_commessa>0 then
else
	as_errore = "Inserire l'anno della commessa!"
	return 1
end if

if al_num_commessa>0 then
else
	as_errore = "Inserire il numero della commessa!"
	return 1
end if


select quan_prodotta, quan_ordine, data_chiusura, flag_tipo_avanzamento
into :ld_quan_prodotta, :ld_quan_ordine, :ldt_data_chiusura, :ls_flag_tipo_avanzamento
from anag_commesse
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_commessa=:ai_anno_commessa and
			num_commessa=:al_num_commessa;

if sqlca.sqlcode<0 then
	as_errore = "Errore in controllo commessa: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	as_errore = "Sembra che la commessa sia inesistente!"
	return 1
end if

if (not isnull(ldt_data_chiusura) and year(date(ldt_data_chiusura))>=1950) or &
	ld_quan_prodotta>=ld_quan_ordine or &
		ls_flag_tipo_avanzamento = "9" or ls_flag_tipo_avanzamento = "8" or ls_flag_tipo_avanzamento = "7" then
		
	as_errore = "Sembra che la commessa sia stata già chiusa!"
	return 1
		
end if


if as_cod_semilavorato="" or isnull(as_cod_semilavorato) then
	as_errore = "Indicare il semilavorato che il terzista dovrà produrre!"
	return 1
end if


ldt_data_a = datetime(today(), 00:00:00)
dw_lista.reset()
ls_sl = as_cod_semilavorato


//lettura MP
luo_f1 = create uo_funzioni_1
li_ret = luo_f1.uof_trova_mat_prime_varianti(	ai_anno_commessa, al_num_commessa, ls_sl, true, &
															ls_materia_prima[], ls_versione_prima[], ldd_quantita_utilizzo[], ls_deposito_mp, as_errore)
destroy luo_f1


for ll_index=1 to upperbound(ls_materia_prima[])
	ll_find = dw_lista.find("cod_prodotto='"+ls_materia_prima[ll_index]+"'", 1, dw_lista.rowcount())
	
	if ll_find>0 then
		//riga esistente, vai ad incremento della sola quantità prelievo
		dw_lista.setitem(ll_find, "qta_prelievo", dw_lista.getitemdecimal(ll_find, "qta_prelievo") + ldd_quantita_utilizzo[ll_index])
	else
		//----------------------------------------------------------------------------------------------------
		ll_new = dw_lista.insertrow(0)
		dw_lista.setitem(ll_new, "cod_prodotto", ls_materia_prima[ll_index])
		dw_lista.setitem(ll_new, "des_prodotto", f_des_tabella("anag_prodotti","cod_prodotto='" + ls_materia_prima[ll_index] +"'" ,"des_prodotto"))
		dw_lista.setitem(ll_new, "qta_prelievo", ldd_quantita_utilizzo[ll_index])
		dw_lista.setitem(ll_new, "um", f_des_tabella("anag_prodotti","cod_prodotto='" + ls_materia_prima[ll_index] +"'" ,"cod_misura_mag"))
		
		//prelievo da
		dw_lista.setitem(ll_new, "cod_deposito_prelievo", as_cod_deposito_prelievo)
		if wf_get_giacenza_deposito(ls_materia_prima[ll_index], as_cod_deposito_prelievo, ldt_data_a, ldd_giacenza, as_errore) < 0 then
			return -1
		end if
		dw_lista.setitem(ll_new, "giacenza_dep_prelievo", ldd_giacenza)
		ldd_giacenza = 0
		
		//versamento in
		dw_lista.setitem(ll_new, "cod_deposito_dest", as_cod_deposito_dest)
		if wf_get_giacenza_deposito(ls_materia_prima[ll_index], as_cod_deposito_dest, ldt_data_a, ldd_giacenza, as_errore) < 0 then
			return -1
		end if
		dw_lista.setitem(ll_new, "giacenza_dep_dest", ldd_giacenza)
		//----------------------------------------------------------------------------------------------------
	end if
	
next

if dw_lista.rowcount()>0 then
	cb_ok.enabled = true
end if



return 0
end function

public function integer wf_get_giacenza_deposito (string as_cod_prodotto, string as_cod_deposito, datetime adt_data_rif, ref double add_giacenza, ref string as_errore);
uo_magazzino			luo_mag

dec{4}					ld_quant_val[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[]

string						ls_where, ls_chiave[]

integer					li_ret


ls_where = "and cod_deposito=~~'"+as_cod_deposito+"~~'"

luo_mag = CREATE uo_magazzino
li_ret = luo_mag.uof_saldo_prod_date_decimal(as_cod_prodotto, adt_data_rif, ls_where, ld_quant_val[], as_errore, "D", &
																					ls_chiave[], ld_giacenza_stock[], ld_costo_medio_stock[], ld_quan_costo_medio_stock[])
destroy luo_mag

if li_ret <0 then
	return -1
end if
	
//prendo solo il primo (ce ne sarà uno solo perchè gli ho passato il flitro per deposito fornitore)
if upperbound(ld_giacenza_stock[]) > 0 then
	add_giacenza = ld_giacenza_stock[1]
end if

if isnull(add_giacenza) then add_giacenza = 0

return 0
	
end function

public subroutine wf_cerca ();integer			li_anno_commessa, li_ret
long				ll_num_commessa
string				ls_cod_sl, ls_errore


dw_ricerca.accepttext()


li_anno_commessa = dw_ricerca.getitemnumber(1, "anno_commessa")
ll_num_commessa = dw_ricerca.getitemnumber(1, "num_commessa")
ls_cod_sl = dw_ricerca.getitemstring(1, "cod_semilavorato")

li_ret = wf_materie_prime(li_anno_commessa, ll_num_commessa, ls_cod_sl, is_cod_dep_prelievo, is_cod_dep_dest, ls_errore)
if li_ret<0 then
	g_mb.error(ls_errore)
	return
	
elseif li_ret>0 then
	g_mb.warning(ls_errore)
	return
	
else
	//OK
end if

return
end subroutine

on w_invio_mp_terzista.create
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.dw_lista=create dw_lista
this.dw_ricerca=create dw_ricerca
this.Control[]={this.cb_annulla,&
this.cb_ok,&
this.dw_lista,&
this.dw_ricerca}
end on

on w_invio_mp_terzista.destroy
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.dw_lista)
destroy(this.dw_ricerca)
end on

event open;s_cs_xx_parametri			lstr_parametri


lstr_parametri = message.powerobjectparm

is_cod_dep_prelievo = lstr_parametri.parametro_s_1_a[1]
is_cod_dep_dest = lstr_parametri.parametro_s_1_a[2]
is_cod_tipo_det_ven = lstr_parametri.parametro_s_1_a[3]

this .postevent("ue_imposta_ricerca")
end event

type cb_annulla from commandbutton within w_invio_mp_terzista
integer x = 3360
integer y = 272
integer width = 402
integer height = 96
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;s_cs_xx_parametri			lstr_parametri


lstr_parametri.parametro_b_1 = false

closewithreturn(parent, lstr_parametri)
end event

type cb_ok from commandbutton within w_invio_mp_terzista
integer x = 3360
integer y = 112
integer width = 402
integer height = 96
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Prosegui"
end type

event clicked;s_cs_xx_parametri			lstr_parametri
long							ll_index, ll_new
string							ls_cod_tipo_det_ven


dw_lista.accepttext()


if dw_lista.rowcount()>0 then
else
	g_mb.warning("Nessun articolo da inviare a terzista!")
	return
end if


ls_cod_tipo_det_ven = dw_ricerca.getitemstring(1, "cod_tipo_det_ven")
if ls_cod_tipo_det_ven="" or isnull(ls_cod_tipo_det_ven) then
	g_mb.warning("Indicare il tipo dettaglio da usare per le righe del DDT!")
	return
end if

lstr_parametri.parametro_s_1 = ls_cod_tipo_det_ven
ll_new = 0

for ll_index=1 to dw_lista.rowcount()
	
	if dw_lista.getitemdecimal(ll_index, "qta_prelievo") <= 0 then
		if g_mb.confirm("Articolo " + dw_lista.getitemstring(ll_index, "cod_prodotto") + " presente con quantità prelievo pari a zero e pertanto verrà escluso. Continuare?") then
			continue
		else
			return
		end if
	end if
	
	ll_new += 1
	lstr_parametri.parametro_s_1_a[ll_new] = dw_lista.getitemstring(ll_index, "cod_prodotto")
	lstr_parametri.parametro_d_1_a[ll_new] = dw_lista.getitemdecimal(ll_index, "qta_prelievo")
next

lstr_parametri.parametro_b_1 = true


closewithreturn(parent, lstr_parametri)
end event

type dw_lista from datawindow within w_invio_mp_terzista
integer x = 23
integer y = 752
integer width = 4146
integer height = 1364
integer taborder = 20
string title = "none"
string dataobject = "d_invio_mp_terzista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event buttonclicked;if row>0 then
else
	return
end if

choose case dwo.name
	case "b_rimuovi"
		if g_mb.confirm("Rimuovere questa riga dall'invio?") then
			deleterow(row)
		end if
end choose
end event

type dw_ricerca from u_dw_search within w_invio_mp_terzista
event ue_keydown pbm_dwnkey
integer x = 23
integer y = 40
integer width = 2843
integer height = 676
integer taborder = 10
string dataobject = "d_invio_mp_terzista_sel"
boolean border = false
end type

event ue_keydown;

if key = KeyEnter! then
	wf_cerca()
end if
end event

event buttonclicked;call super::buttonclicked;



choose case dwo.name
	case "b_cerca"
		wf_cerca()
		
	case "b_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_semilavorato")
		
	case "b_commessa"
		guo_ricerca.uof_ricerca_commessa(dw_ricerca, "anno_commessa", "num_commessa")
		
end choose
end event

