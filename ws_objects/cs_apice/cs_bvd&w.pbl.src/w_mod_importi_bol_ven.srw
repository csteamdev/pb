﻿$PBExportHeader$w_mod_importi_bol_ven.srw
$PBExportComments$Finestra modifica importi addebiti clienti bolle vendita
forward
global type w_mod_importi_bol_ven from w_cs_xx_risposta
end type
type st_1 from statictext within w_mod_importi_bol_ven
end type
type cb_4 from commandbutton within w_mod_importi_bol_ven
end type
type cb_3 from commandbutton within w_mod_importi_bol_ven
end type
type dw_lista from datawindow within w_mod_importi_bol_ven
end type
type cb_annulla from commandbutton within w_mod_importi_bol_ven
end type
type cb_ok from commandbutton within w_mod_importi_bol_ven
end type
end forward

global type w_mod_importi_bol_ven from w_cs_xx_risposta
integer width = 2222
integer height = 1172
string title = "Modifica Addebiti Bolle Vendita"
boolean controlmenu = false
boolean resizable = false
st_1 st_1
cb_4 cb_4
cb_3 cb_3
dw_lista dw_lista
cb_annulla cb_annulla
cb_ok cb_ok
end type
global w_mod_importi_bol_ven w_mod_importi_bol_ven

type variables
integer il_anno_registrazione, il_num_registrazione
end variables

forward prototypes
public function integer wf_carica_addebiti (long fl_anno_fat_ven, long fl_num_fat_ven, ref string fs_messaggio)
public function integer wf_inserisci_addebiti (ref string fs_messaggio)
public function integer wf_cancella_addebiti (ref string fs_messaggio)
public function integer wf_arrotonda (ref decimal fd_valore, decimal fd_precisione, string fs_tipo_arrotondamento)
end prototypes

public function integer wf_carica_addebiti (long fl_anno_fat_ven, long fl_num_fat_ven, ref string fs_messaggio);string   ls_cod_valuta, ls_cod_cliente, ls_cod_documento, ls_cod_tipo_det_ven, ls_cod_addebito, ls_des_addebito

datetime ldt_data_registrazione, ldt_data_fattura, ldt_data_riferimento

dec{4}   ld_importo_addebito

long     ll_riga, ll_num_documento

select cod_valuta,
		 cod_cliente,		 
		 data_registrazione,		 
		 data_bolla,
		 cod_documento,
		 num_documento
into   :ls_cod_valuta,
       :ls_cod_cliente,		 
		 :ldt_data_registrazione,		 
		 :ldt_data_fattura,
		 :ls_cod_documento,
		 :ll_num_documento
from   tes_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
	    anno_registrazione = :fl_anno_fat_ven and
		 num_registrazione = :fl_num_fat_ven;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di tes_bol_ven: " + sqlca.sqlerrtext
	return -1
end if

if not isnull(ls_cod_documento) or ( not isnull(ll_num_documento) and ll_num_documento > 0) then
	fs_messaggio = "DDT confermato"
	return 100
end if

if isnull(ls_cod_documento) then
	
	ldt_data_riferimento = ldt_data_registrazione
	
	if isnull(ldt_data_riferimento) then
		ldt_data_riferimento = datetime(today(), 00:00:00)
	end if	
	
else
	ldt_data_riferimento = ldt_data_fattura
end if

declare cu_addebiti cursor for
select  cod_addebito,
        des_addebito,
        cod_tipo_det_ven,
		  importo_addebito
from    anag_clienti_addebiti
where   cod_azienda = :s_cs_xx.cod_azienda and
        cod_cliente = :ls_cod_cliente and
		  data_inizio <= :ldt_data_riferimento and
		  data_fine >= :ldt_data_riferimento and
		  cod_valuta = :ls_cod_valuta and
		  flag_mod_valore_bolle = 'S' and
		  flag_tipo_documento = 'B';
	 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di anag_clienti_addebiti: " + sqlca.sqlerrtext
	return -1
end if

open cu_addebiti;

if sqlca.sqlcode = -1 then
	fs_messaggio = "Errore nella open del cursore cu_addebiti: " + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_addebiti into :ls_cod_addebito,
	                       :ls_des_addebito,
								  :ls_cod_tipo_det_ven,
								  :ld_importo_addebito;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Errore nella fetch del cursore cu_addebiti: " + sqlca.sqlerrtext
		close cu_addebiti;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then
		exit
	end if	
	
	if isnull(ls_cod_tipo_det_ven) then
		fs_messaggio = "Errore nella fetch del cursore cu_addebiti: impostare il tipo dettaglio vendita negli addebiti clienti"
		close cu_addebiti;
		return -1
	end if
	
	if isnull(ls_cod_valuta) then
		fs_messaggio = "Errore nella fetch del cursore cu_addebiti: impostare la valuta negli addebiti clienti"
		close cu_addebiti;
		return -1
	end if	
	
	ll_riga = dw_lista.insertrow(0)
	dw_lista.setitem( ll_riga, "cod_addebito", ls_cod_addebito)
	dw_lista.setitem( ll_riga, "des_addebito", ls_des_addebito)
	dw_lista.setitem( ll_riga, "importo", ld_importo_addebito)
	dw_lista.setitem( ll_riga, "importo_originale", ld_importo_addebito)
	dw_lista.setitem( ll_riga, "elimina", "N")
		
loop
	
close cu_addebiti;
if sqlca.sqlcode < 0 then
	fs_messaggio = "Errore durante la chiusura del cursore: " + sqlca.sqlerrtext
	rollback;
	return -1
end if


return 0
end function

public function integer wf_inserisci_addebiti (ref string fs_messaggio);string   ls_cod_pagamento, ls_cod_valuta, ls_cod_cliente, ls_flag_calcolo, ls_cod_lingua, ls_cod_tipo_det_ven, &
         ls_messaggio, ls_stringa, ls_str, ls_cod_iva, ls_flag_tipo_pagamento, ls_cod_tipo_doc_ven, &
		   ls_cod_fornitore, ls_flag_tipo_doc_ven, ls_cod_documento,ls_des_addebito, ls_cod_prodotto_appo, ls_descrizione_appo
			
long     ll_prog_riga_bol_ven, ll_pos

dec{4}   ld_importo_addebito

datetime ldt_data_esenzione_iva, ldt_data_registrazione, ldt_data_bolla, ldt_data_riferimento

string   ls_cod_addebito_sel[], ls_elimina_sel[], ls_cod_addebito
dec{4}   ld_importo_addebito_sel[], ld_importo_addebito_orig_sel[], ld_precisione_valuta
long     ll_i
boolean  lb_continua

// *** leggo gli addebiti con gli importi
dw_lista.accepttext()
for ll_i = 1 to dw_lista.rowcount()
	ls_cod_addebito_sel[ll_i] = dw_lista.getitemstring( ll_i, "cod_addebito")
	ld_importo_addebito_sel[ll_i] = dw_lista.getitemnumber( ll_i, "importo")
	ld_importo_addebito_orig_sel[ll_i] = dw_lista.getitemnumber( ll_i, "importo_originale")
	ls_elimina_sel[ll_i] = dw_lista.getitemstring( ll_i, "elimina")
next
//

select cod_pagamento,
  		 cod_valuta,
		 cod_cliente,
		 cod_fornitore,
		 data_registrazione,
 		 cod_tipo_bol_ven,
		 data_bolla,
		 cod_documento
into   :ls_cod_pagamento,
		 :ls_cod_valuta,
       :ls_cod_cliente,
       :ls_cod_fornitore,
		 :ldt_data_registrazione,
		 :ls_cod_tipo_doc_ven,
		 :ldt_data_bolla,
		 :ls_cod_documento
from   tes_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
	    anno_registrazione = :il_anno_registrazione and
 		 num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di tes_fat_ven: " + sqlca.sqlerrtext
	return -1
end if

select flag_tipo_bol_ven
into   :ls_flag_tipo_doc_ven
from   tab_tipi_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_bol_ven = :ls_cod_tipo_doc_ven;
 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di tab_tipi_bol_ven: " + sqlca.sqlerrtext
	return -1
end if

if ls_flag_tipo_doc_ven <> "F" then
	
	select cod_lingua
	into   :ls_cod_lingua
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_cliente = :ls_cod_cliente;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di anag_clienti: " + sqlca.sqlerrtext
		return -1
	end if
	
else
	
	return 0		// per i fornitori non fare nulla
	
end if
	
if not isnull(ls_cod_lingua) then
	
	select cod_lingua
	into   :ls_cod_lingua
	from   tab_lingue
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_lingua = :ls_cod_lingua;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di tab_lingue: " + sqlca.sqlerrtext
		return -1
	end if
	
end if

if isnull(ls_cod_documento) then
	
	ldt_data_riferimento = ldt_data_registrazione
	
	if isnull(ldt_data_riferimento) then
		ldt_data_riferimento = datetime(today(), 00:00:00)
	end if	
	
else
	
	ldt_data_riferimento = ldt_data_bolla

end if

declare cu_addebiti cursor for
select cod_addebito,
       des_addebito,
       cod_tipo_det_ven,
       importo_addebito,
		 cod_valuta
from   anag_clienti_addebiti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente and
		 data_inizio <= :ldt_data_riferimento and
		 data_fine >= :ldt_data_riferimento and
		 cod_valuta = :ls_cod_valuta and
		 flag_tipo_documento = 'B';
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di anag_clienti_addebiti: " + sqlca.sqlerrtext
	return -1
end if

open cu_addebiti;

if sqlca.sqlcode = -1 then
	fs_messaggio = "Errore nella open del cursore cu_addebiti: " + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_addebiti into :ls_cod_addebito,
	                       :ls_des_addebito, 
								  :ls_cod_tipo_det_ven, 
								  :ld_importo_addebito, 
								  :ls_cod_valuta;
	
	if sqlca.sqlcode = -1 then
		fs_messaggio = "Errore nella fetch del cursore cu_addebiti: " + sqlca.sqlerrtext
		close cu_addebiti;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then
		exit
	end if	
	lb_continua = false
	//// ******** controllo se ha cambiato gli importi o se lo ha eliminato	
	for ll_i = 1 to Upperbound(ls_cod_addebito_sel)
		if not isnull(ls_cod_addebito) and not isnull(ls_cod_addebito_sel[ll_i]) and ls_cod_addebito_sel[ll_i] = ls_cod_addebito then
			if ls_elimina_sel[ll_i] = "S" and not isnull(ls_elimina_sel[ll_i]) and ls_elimina_sel[ll_i] <> "N" then
				lb_continua = true
			end if
			// Viene letto il valore di precisione per la valuta corrente
			select precisione
			into   :ld_precisione_valuta
			from   tab_valute
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_valuta = :ls_cod_valuta;	
			ld_importo_addebito = ld_importo_addebito_sel[ll_i]
			wf_arrotonda(ld_importo_addebito, ld_precisione_valuta, "euro")
		end if
	next
	
	if lb_continua then continue
	
	if isnull(ls_cod_tipo_det_ven) then
		fs_messaggio = "Errore nella fetch del cursore cu_addebiti: impostare il tipo dettaglio vendita negli addebiti clienti"
		close cu_addebiti;
		return -1
	end if
	
	if isnull(ls_cod_valuta) then
		fs_messaggio = "Errore nella fetch del cursore cu_addebiti: impostare la valuta negli addebiti clienti"
		close cu_addebiti;
		return -1
	end if
	
	// Ricerca della prima riga libera nel documento
	select max(prog_riga_bol_ven)
	into   :ll_prog_riga_bol_ven
	from   det_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione;
			 
	if ll_prog_riga_bol_ven = 0 or isnull(ll_prog_riga_bol_ven) then
		ll_prog_riga_bol_ven = 10
	else
		ll_prog_riga_bol_ven = ll_prog_riga_bol_ven + 10
	end if
	
	// Ricerca di aliquota iva o esenzione del dettaglio
	// La variabile ls_cod_tipo_det_ven non può essere a null: già verificato sopra	
	select cod_iva  
	into   :ls_str
	from   tab_tipi_det_ven  
	where  cod_azienda = :s_cs_xx.cod_azienda and  
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella select di tab_tipi_det_ven: " + sqlca.sqlerrtext
		close cu_addebiti;
		return -1
	end if
	
	if not isnull(ls_str) then ls_cod_iva = ls_str
	
	select cod_iva,
			 data_esenzione_iva
	into   :ls_str,
			 :ldt_data_esenzione_iva
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_cliente = :ls_cod_cliente;
			 
	if sqlca.sqlcode = 0 then
		if not isnull(ls_str) and (ldt_data_esenzione_iva <= ldt_data_riferimento or isnull(ldt_data_esenzione_iva)) then
			ls_cod_iva = ls_str
		end if	
	end if
	
	// *** Michela 26/01/2006: se il primo carattere della descrizione dell'addebito è = "*" allora la 
	//                         parte restante della descrizione è un codice prodotto e la descrizione vado a prendermela 
	//                         dall'anagrafica prodotti
	
	setnull(ls_cod_prodotto_appo)
	setnull(ls_descrizione_appo)
	ll_pos = 0
	ll_pos = pos( ls_des_addebito, "*")
	if not isnull(ll_pos) and ll_pos = 1 then
		ls_cod_prodotto_appo = mid( ls_des_addebito, 2)
		
		select des_prodotto
		into   :ls_descrizione_appo
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :ls_cod_prodotto_appo;
				 
		if sqlca.sqlcode <> 0 then
			setnull(ls_cod_prodotto_appo)
		else
			ls_des_addebito = ls_descrizione_appo
		end if
	end if		
	
	// La riga di spese bancarie viene inserita nel dettaglio del documento
	
	insert into det_bol_ven  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  prog_riga_bol_ven,   
				  cod_tipo_det_ven,   
				  cod_deposito,   
				  cod_ubicazione,   
				  cod_lotto,   
				  data_stock,   
				  progr_stock,   
				  cod_misura,   
				  cod_prodotto,   
				  des_prodotto,   
				  quan_consegnata,				  
				  prezzo_vendita,   
				  fat_conversione_ven,
				  sconto_1,   
				  sconto_2,   
				  provvigione_1,   
				  provvigione_2,   
				  cod_iva,   
				  cod_tipo_movimento,   
				  num_registrazione_mov_mag,   
				  nota_dettaglio,   
				  anno_registrazione_ord_ven,   
				  num_registrazione_ord_ven,   
				  prog_riga_ord_ven,   
				  anno_commessa,   
				  num_commessa,   
				  cod_centro_costo,   
				  sconto_3,   
				  sconto_4,   
				  sconto_5,   
				  sconto_6,   
				  sconto_7,   
				  sconto_8,   
				  sconto_9,   
				  sconto_10,   
				  anno_registrazione_mov_mag,   
				  anno_reg_bol_acq,   
				  num_reg_bol_acq,   
				  prog_riga_bol_acq,   
				  anno_reg_des_mov,   
				  num_reg_des_mov,   
				  cod_versione,   
				  num_confezioni,   
				  num_pezzi_confezione,   
				  flag_doc_suc_det,   
				  flag_st_note_det,   
				  quantita_um,   
				  prezzo_um,   
				  imponibile_iva,   
				  imponibile_iva_valuta )  
	  values ( :s_cs_xx.cod_azienda,   
				  :il_anno_registrazione,   
				  :il_num_registrazione,   
				  :ll_prog_riga_bol_ven,   
				  :ls_cod_tipo_det_ven,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  :ls_cod_prodotto_appo,   
				  :ls_des_addebito,   
				  1,   
				  :ld_importo_addebito,   
				  1,   
				  0,
				  0,   
				  0,   
				  0,   
				  :ls_cod_iva,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  null,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  0,   
				  null,   
				  0,   
				  null,   
				  1,   
				  null,   
				  null,   
				  null,   
				  0,   
				  0,   
				  'I',   
				  'I',   
				  0,
				  0,
				  0,
				  0)  ;
				  
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella insert di det_fat_ven: " + sqlca.sqlerrtext
		return -1
	end if
	
loop

close cu_addebiti;

return 0



return 0
end function

public function integer wf_cancella_addebiti (ref string fs_messaggio);string   ls_cod_valuta, ls_cod_cliente, ls_cod_documento, ls_cod_tipo_det_ven
datetime ldt_data_registrazione, ldt_data_bolla, ldt_data_riferimento

select cod_valuta,
		 cod_cliente,		 
		 data_registrazione,		 
		 data_bolla,
		 cod_documento
into   :ls_cod_valuta,
       :ls_cod_cliente,		 
		 :ldt_data_registrazione,		 
		 :ldt_data_bolla,
		 :ls_cod_documento
from   tes_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
	    anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di tes_bol_ven: " + sqlca.sqlerrtext
	return -1
end if

if isnull(ls_cod_documento) then
	
	ldt_data_riferimento = ldt_data_registrazione
	
	if isnull(ldt_data_riferimento) then
		ldt_data_riferimento = datetime(today(), 00:00:00)
	end if	
	
else
	ldt_data_riferimento = ldt_data_bolla
end if

declare cu_addebiti cursor for
select  cod_tipo_det_ven       
from    anag_clienti_addebiti
where   cod_azienda = :s_cs_xx.cod_azienda and
        cod_cliente = :ls_cod_cliente and
		  data_inizio <= :ldt_data_riferimento and
		  data_fine >= :ldt_data_riferimento and
		  cod_valuta = :ls_cod_valuta and
		  flag_tipo_documento = 'B';
		 
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella select di anag_clienti_addebiti: " + sqlca.sqlerrtext
	return -1
end if

open cu_addebiti;

if sqlca.sqlcode = -1 then
	fs_messaggio = "Errore nella open del cursore cu_addebiti: " + sqlca.sqlerrtext
	return -1
end if

do while true
	
	fetch cu_addebiti into :ls_cod_tipo_det_ven;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Errore nella fetch del cursore cu_addebiti: " + sqlca.sqlerrtext
		close cu_addebiti;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then
		exit
	end if	
	
	if isnull(ls_cod_tipo_det_ven) then
		fs_messaggio = "Errore nella fetch del cursore cu_addebiti: impostare il tipo dettaglio vendita negli addebiti clienti"
		close cu_addebiti;
		return -1
	end if
	
	if isnull(ls_cod_valuta) then
		fs_messaggio = "Errore nella fetch del cursore cu_addebiti: impostare la valuta negli addebiti clienti"
		close cu_addebiti;
		return -1
	end if	
		
	delete det_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione and
			 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			 
	if sqlca.sqlcode <> 0 then
		fs_messaggio = "Errore nella delete di det_bol_ven: " + sqlca.sqlerrtext
		return -1
	end if	
	
loop
	
close cu_addebiti;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore nella chiusura del cursore cu_addebiti: " + sqlca.sqlerrtext
	return -1
end if

return 0
end function

public function integer wf_arrotonda (ref decimal fd_valore, decimal fd_precisione, string fs_tipo_arrotondamento);dec{4} ld_valore_tondo, ld_scarto


// La precisione non può essere zero o negativa
if fd_precisione <= 0 then
	return -1
end if

// Il valore da arrotondare non può essere negativo
if fd_valore < 0 then
	return -10
end if

// Arrotonda il valore per difetto e annota lo scarto dal valore originale
ld_valore_tondo = truncate(fd_valore / fd_precisione, 0) * fd_precisione
ld_scarto = fd_valore - ld_valore_tondo

// Controlla se è necessario arrotondare il valore passato, altrimenti esce
if ld_scarto = 0 then
	return fd_valore
end if	

// Controllo del tipo di arrotondamento specificato
choose case fs_tipo_arrotondamento
	
	// arrotondamento standard: fino a 0.4 per difetto, da 0.5 in poi per eccesso (considerando la precisione come unità)
	case "round","iva","euro"
		
		// se la differenza fra il valore passato e quello troncato è maggiore o uguale a metà della precisione, allora
		// si arrotonda per eccesso e viene quindi aggiunta un'unità di precisione
		if ld_scarto >= (fd_precisione / 2) then
			ld_valore_tondo = ld_valore_tondo + fd_precisione
		end if
		
		// in caso contrario il valore è già arrotondato per difetto
		
		// se il valore arrotondato risulta inferiore alla precisione allora si arrotonda al valore di precisione
		if ld_valore_tondo < fd_precisione then
			ld_valore_tondo = fd_precisione
		end if
		
	// arrotondamento per eccesso
	case "eccesso"
		
		// si aggiunge un'unità di precisione
		ld_valore_tondo = ld_valore_tondo + fd_precisione		

	// arrotondamento per difetto
	case "difetto"
		
		// già implementato all'inizio della funzione
		
	// tipo di arrotondamento non riconosciuto dalla funzione
	case else
		
		return -100
	
end choose

// restituisce il valore arrotondato
fd_valore = ld_valore_tondo

return 0
end function

on w_mod_importi_bol_ven.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_4=create cb_4
this.cb_3=create cb_3
this.dw_lista=create dw_lista
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_4
this.Control[iCurrent+3]=this.cb_3
this.Control[iCurrent+4]=this.dw_lista
this.Control[iCurrent+5]=this.cb_annulla
this.Control[iCurrent+6]=this.cb_ok
end on

on w_mod_importi_bol_ven.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_4)
destroy(this.cb_3)
destroy(this.dw_lista)
destroy(this.cb_annulla)
destroy(this.cb_ok)
end on

event pc_setwindow;call super::pc_setwindow;string ls_messaggio
s_open_parm lstr_parm

lstr_parm = message.powerobjectparm

if isvalid(lstr_parm) and not isnull(lstr_parm) then
	il_anno_registrazione = lstr_parm.anno_registrazione
	il_num_registrazione  = lstr_parm.num_registrazione
else
	il_anno_registrazione = s_cs_xx.parametri.parametro_d_1
	il_num_registrazione  = s_cs_xx.parametri.parametro_d_2
end if

dw_lista.settransobject( sqlca)
wf_carica_addebiti( il_anno_registrazione, il_num_registrazione, ref ls_messaggio)
end event

type st_1 from statictext within w_mod_importi_bol_ven
integer x = 23
integer y = 20
integer width = 2171
integer height = 260
boolean bringtotop = true
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Per modificare l~'addebito nel DDT sostituire il vecchio importo con il nuovo. Per non addebitare la spesa mettere a SI il flag Elimina."
boolean focusrectangle = false
end type

type cb_4 from commandbutton within w_mod_importi_bol_ven
integer x = 1646
integer y = 1000
integer width = 549
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Deseleziona Elim."
end type

event clicked;long ll_i

for ll_i = 1 to dw_lista.rowcount()
	dw_lista.setitem( ll_i, "elimina", "N")
next
end event

type cb_3 from commandbutton within w_mod_importi_bol_ven
integer x = 1143
integer y = 1000
integer width = 480
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Seleziona Elim."
end type

event clicked;long ll_i

for ll_i = 1 to dw_lista.rowcount()
	dw_lista.setitem( ll_i, "elimina", "S")
next
end event

type dw_lista from datawindow within w_mod_importi_bol_ven
integer x = 23
integer y = 400
integer width = 2171
integer height = 580
integer taborder = 30
string title = "none"
string dataobject = "d_mod_importo_fattura_ext"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

type cb_annulla from commandbutton within w_mod_importi_bol_ven
integer x = 1417
integer y = 300
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = -1

close(parent)
end event

type cb_ok from commandbutton within w_mod_importi_bol_ven
integer x = 1829
integer y = 300
integer width = 366
integer height = 80
integer taborder = 3
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;string ls_errore
long   ll_ret

// *** chiedo conferma dei dati all'utente
if (g_mb.messagebox("APICE", "Sei sicuro di voler confermare i dati?", Exclamation!, OKCancel!, 2) <> 1 ) then
	g_mb.messagebox("APICE", "Operazione annullata. Usare il pulsante Annulla per chiudere la maschera.")
	return -1
end if

ll_ret = wf_cancella_addebiti(ref ls_errore)
if ll_ret < 0 then
	g_mb.messagebox( "APICE", ls_errore)
	rollback;
else
	ll_ret = wf_inserisci_addebiti( ref ls_errore)
	if ll_ret < 0 then
		g_mb.messagebox( "APICE", ls_Errore)
		rollback;
	else
		commit;
	end if
end if

close(parent)
end event

