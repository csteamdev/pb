﻿$PBExportHeader$w_report_bol_ven.srw
$PBExportComments$Finestra Report Bolle Vendita
forward
global type w_report_bol_ven from w_cs_xx_principale
end type
type cb_invia_pdf_outlook from commandbutton within w_report_bol_ven
end type
type cb_1 from commandbutton within w_report_bol_ven
end type
type st_copie from statictext within w_report_bol_ven
end type
type st_1 from statictext within w_report_bol_ven
end type
type dw_report_bol_ven from uo_cs_xx_dw within w_report_bol_ven
end type
end forward

global type w_report_bol_ven from w_cs_xx_principale
integer width = 4014
integer height = 2424
string title = "Stampa Bolle Uscita"
boolean minbox = false
event ue_avviso_spedizione ( )
cb_invia_pdf_outlook cb_invia_pdf_outlook
cb_1 cb_1
st_copie st_copie
st_1 st_1
dw_report_bol_ven dw_report_bol_ven
end type
global w_report_bol_ven w_report_bol_ven

type variables
boolean ib_modifica=false, ib_nuovo=false
boolean ib_estero
long il_anno_registrazione, il_num_registrazione
long il_num_copie

// ------------- dichiarate per stampa da pagina a pagina -----------
boolean ib_stampa = false
long    il_totale_pagine, il_pagina_corrente
end variables

forward prototypes
public subroutine wf_report ()
end prototypes

event ue_avviso_spedizione();uo_avviso_spedizioni luo_avviso_sped

string ls_tipo_gestione, ls_messaggio, ls_path_logo_intestazione
long ll_ret


if il_anno_registrazione>0 and il_num_registrazione>0 then
	
	luo_avviso_sped = create uo_avviso_spedizioni

	ls_tipo_gestione = "bol_ven"
	
	//passaggio path file dell'intestazione report ---------------------------------------
	try
		ls_path_logo_intestazione = dw_report_bol_ven.Describe ("intestazione.filename") 
	catch (throwable err)
		ls_path_logo_intestazione = ""
	end try
	
	luo_avviso_sped.is_path_logo_intestazione = ls_path_logo_intestazione
	//-----------------------------------------------------------------------------------------
	
	//passa l'oggetto dw del report
	luo_avviso_sped.idw_report = dw_report_bol_ven
	
	ll_ret = luo_avviso_sped.uof_avviso_spedizione(il_anno_registrazione,il_num_registrazione, ls_tipo_gestione, ls_messaggio)

	if ll_ret<0 then
		g_mb.error(ls_messaggio)
		rollback;
		return
	end if

	commit;
	
	destroy luo_avviso_sped
	
else
	g_mb.error("Nessun documento presente (>0)!")
	rollback;
	return
end if
end event

public subroutine wf_report ();string ls_cod_tipo_bol_ven, ls_cod_cliente, ls_cod_porto, ls_cod_resa, &
		 ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
		 ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
		 ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, &
		 ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli, &
		 ls_partita_iva, ls_cod_fiscale, ls_cod_lingua, ls_flag_tipo_cliente, &
		 ls_des_tipo_bol_ven, ls_des_porto, ls_des_porto_lingua, ls_des_resa, &
		 ls_des_resa_lingua, ls_des_prodotto_anag, ls_flag_stampa_bolla, ls_cod_tipo_det_ven, &
		 ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_cod_documento, ls_telefono, ls_fax, &
		 ls_numeratore_documento, ls_cod_vettore, ls_cod_inoltro, ls_cod_causale, &
		 ls_aspetto_beni, ls_rag_soc_1_vet, ls_rag_soc_2_vet, ls_indirizzo_vet, &
		 ls_localita_vet, ls_frazione_vet, ls_cap_vet, ls_provincia_vet, ls_des_causale_lingua, &
		 ls_rag_soc_1_ino, ls_rag_soc_2_ino, ls_indirizzo_ino, ls_des_causale, &
		 ls_localita_ino, ls_frazione_ino, ls_cap_ino, ls_provincia_ino, ls_cod_fornitore, &
		 ls_cod_deposito_tras, ls_cod_fil_fornitore, ls_flag_tipo_bol_ven, ls_des_deposito, &
		 ls_cod_mezzo, ls_des_mezzo, ls_des_mezzo_lingua,  ls_flag_st_note_tes, ls_flag_st_note_pie, ls_flag_st_note_det, &
		 ls_cod_contatto, ls_stato_cli
long ll_errore, ll_anno_documento, ll_num_documento, ll_num_colli
double ld_quan_consegnata, ld_fat_conversione_ven, ld_peso_netto, ld_peso_lordo
datetime ldt_data_bolla, ldt_data_inizio_trasporto, ldt_ora_inizio_trasporto

dw_report_bol_ven.reset()

select tes_bol_ven.cod_tipo_bol_ven,   
		 tes_bol_ven.cod_cliente,   
		 tes_bol_ven.cod_porto,   
		 tes_bol_ven.cod_resa,   
		 tes_bol_ven.nota_testata,   
		 tes_bol_ven.nota_piede,   
		 tes_bol_ven.rag_soc_1,   
		 tes_bol_ven.rag_soc_2,   
		 tes_bol_ven.indirizzo,   
		 tes_bol_ven.localita,   
		 tes_bol_ven.frazione,   
		 tes_bol_ven.cap,   
		 tes_bol_ven.provincia, 
		 tes_bol_ven.cod_documento,
		 tes_bol_ven.numeratore_documento,
		 tes_bol_ven.anno_documento,
		 tes_bol_ven.num_documento,
		 tes_bol_ven.data_bolla,
		 tes_bol_ven.cod_vettore,
		 tes_bol_ven.cod_inoltro,
		 tes_bol_ven.cod_causale,
		 tes_bol_ven.aspetto_beni,
		 tes_bol_ven.num_colli,
		 tes_bol_ven.cod_fornitore,
		 tes_bol_ven.cod_deposito_tras,
		 tes_bol_ven.cod_fil_fornitore,
		 tes_bol_ven.data_inizio_trasporto,
		 tes_bol_ven.ora_inizio_trasporto,
		 tes_bol_ven.cod_mezzo,
		 tes_bol_ven.peso_netto,
		 tes_bol_ven.peso_lordo,
		 tes_bol_ven.flag_st_note_tes,
		 tes_bol_ven.flag_st_note_pie,
		 tes_bol_ven.cod_contatto
into   :ls_cod_tipo_bol_ven,   
	 	 :ls_cod_cliente,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,
		 :ls_cod_documento,
		 :ls_numeratore_documento,
		 :ll_anno_documento,
		 :ll_num_documento,
		 :ldt_data_bolla,
		 :ls_cod_vettore,
		 :ls_cod_inoltro,
		 :ls_cod_causale,
		 :ls_aspetto_beni,
		 :ll_num_colli,
		 :ls_cod_fornitore,
		 :ls_cod_deposito_tras,
		 :ls_cod_fil_fornitore,
		 :ldt_data_inizio_trasporto,
		 :ldt_ora_inizio_trasporto,
		 :ls_cod_mezzo,
		 :ld_peso_netto,
		 :ld_peso_lordo,
		 :ls_flag_st_note_tes,
		 :ls_flag_st_note_pie,
		 :ls_cod_contatto // stefanop 09/02/2012
from   tes_bol_ven  
where  tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_bol_ven.anno_registrazione = :il_anno_registrazione and 
		 tes_bol_ven.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	setnull(ls_cod_tipo_bol_ven)
	setnull(ls_cod_cliente)
	setnull(ls_cod_porto)
	setnull(ls_cod_resa)
	setnull(ls_nota_testata)
	setnull(ls_nota_piede)
	setnull(ls_rag_soc_1)
	setnull(ls_rag_soc_2)
	setnull(ls_indirizzo)
	setnull(ls_localita)
	setnull(ls_frazione)
	setnull(ls_cap)
	setnull(ls_provincia)
	setnull(ls_cod_documento)
	setnull(ls_numeratore_documento)
	setnull(ll_anno_documento)
	setnull(ll_num_documento)
	setnull(ldt_data_bolla)
	setnull(ls_cod_vettore)
	setnull(ls_cod_inoltro)
	setnull(ls_cod_causale)
	setnull(ls_aspetto_beni)
	setnull(ll_num_colli)
	setnull(ls_cod_fornitore)
	setnull(ls_cod_deposito_tras)
	setnull(ls_cod_mezzo)
	setnull(ls_cod_contatto)
end if

// pulisco variabili
setnull(ls_rag_soc_1_cli)
setnull(ls_rag_soc_2_cli)
setnull(ls_indirizzo_cli)
setnull(ls_localita_cli)
setnull(ls_frazione_cli)
setnull(ls_cap_cli)
setnull(ls_provincia_cli)
setnull(ls_partita_iva)
setnull(ls_cod_fiscale)
setnull(ls_cod_lingua)
setnull(ls_flag_tipo_cliente)
setnull(ls_telefono)
setnull(ls_fax)
setnull(ls_stato_cli)

if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
	select anag_clienti.rag_soc_1,   
			 anag_clienti.rag_soc_2,   
			 anag_clienti.indirizzo,   
			 anag_clienti.localita,   
			 anag_clienti.frazione,   
			 anag_clienti.cap,   
			 anag_clienti.provincia,   
			 anag_clienti.partita_iva,   
			 anag_clienti.cod_fiscale,   
			 anag_clienti.cod_lingua,   
			 anag_clienti.flag_tipo_cliente,
			 anag_clienti.telefono,
			 anag_clienti.fax,
			 anag_clienti.stato
	into   :ls_rag_soc_1_cli,   
			 :ls_rag_soc_2_cli,   
			 :ls_indirizzo_cli,   
			 :ls_localita_cli,   
			 :ls_frazione_cli,   
			 :ls_cap_cli,   
			 :ls_provincia_cli,   
			 :ls_partita_iva,   
			 :ls_cod_fiscale,   
			 :ls_cod_lingua,   
			 :ls_flag_tipo_cliente,
			 :ls_telefono,
			 :ls_fax,
			 :ls_stato_cli
	from   anag_clienti  
	where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and
			 anag_clienti.cod_cliente = :ls_cod_cliente;

	if sqlca.sqlcode = 0 then
		if ls_flag_tipo_cliente = 'E' or ls_flag_tipo_cliente = 'C' then
			ls_partita_iva = ls_cod_fiscale
		end if
	end if
	
elseif not isnull(ls_cod_fornitore) and ls_cod_fornitore <> "" then
	select anag_fornitori.rag_soc_1,   
			 anag_fornitori.rag_soc_2,   
			 anag_fornitori.indirizzo,   
			 anag_fornitori.localita,   
			 anag_fornitori.frazione,   
			 anag_fornitori.cap,   
			 anag_fornitori.provincia,   
			 anag_fornitori.partita_iva,   
			 anag_fornitori.cod_fiscale,   
			 anag_fornitori.cod_lingua,   
			 anag_fornitori.flag_tipo_fornitore,
			 anag_fornitori.telefono,
			 anag_fornitori.fax,
			 anag_fornitori.stato
	into   :ls_rag_soc_1_cli,   
			 :ls_rag_soc_2_cli,   
			 :ls_indirizzo_cli,   
			 :ls_localita_cli,   
			 :ls_frazione_cli,   
			 :ls_cap_cli,   
			 :ls_provincia_cli,   
			 :ls_partita_iva,   
			 :ls_cod_fiscale,   
			 :ls_cod_lingua,   
			 :ls_flag_tipo_cliente,
			 :ls_telefono,
		 	 :ls_fax,
			 :ls_stato_cli
	from   anag_fornitori
	where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and
			 anag_fornitori.cod_fornitore = :ls_cod_fornitore;
	
	if sqlca.sqlcode = 0 then
		if ls_flag_tipo_cliente = 'E' or ls_flag_tipo_cliente = 'C' then
			ls_partita_iva = ls_cod_fiscale
		end if
	end if
	
	if not isnull(ls_cod_fil_fornitore) then
		setnull(ls_stato_cli)
		
		select anag_fil_fornitori.rag_soc_1,   
				 anag_fil_fornitori.rag_soc_2,   
				 anag_fil_fornitori.indirizzo,   
				 anag_fil_fornitori.localita,   
				 anag_fil_fornitori.frazione,   
				 anag_fil_fornitori.cap,   
				 anag_fil_fornitori.provincia
		into   :ls_rag_soc_1_cli,   
				 :ls_rag_soc_2_cli,   
				 :ls_indirizzo_cli,   
				 :ls_localita_cli,   
				 :ls_frazione_cli,   
				 :ls_cap_cli,   
				 :ls_provincia_cli
		from   anag_fil_fornitori
		where  anag_fil_fornitori.cod_azienda = :s_cs_xx.cod_azienda and
				 anag_fil_fornitori.cod_fornitore = :ls_cod_fornitore and
				 anag_fil_fornitori.cod_fil_fornitore = :ls_cod_fil_fornitore;
		
		if sqlca.sqlcode <> 0 then
			setnull(ls_rag_soc_1_cli)
			setnull(ls_rag_soc_2_cli)
			setnull(ls_indirizzo_cli)
			setnull(ls_localita_cli)
			setnull(ls_frazione_cli)
			setnull(ls_cap_cli)
			setnull(ls_provincia_cli)
		end if
	end if
elseif not isnull(ls_cod_contatto) and ls_cod_contatto <> "" then
	// CONTATTO
	setnull(ls_stato_cli)
	
	select rag_soc_1,   
			 rag_soc_2,   
			 indirizzo,   
			 localita,   
			 frazione,   
			 cap,   
			 provincia,   
			 partita_iva,   
			 cod_fiscale,   
			 cod_lingua,   
			 flag_tipo_cliente,
			 telefono,
			 fax
	into   :ls_rag_soc_1_cli,   
			 :ls_rag_soc_2_cli,   
			 :ls_indirizzo_cli,   
			 :ls_localita_cli,   
			 :ls_frazione_cli,   
			 :ls_cap_cli,   
			 :ls_provincia_cli,   
			 :ls_partita_iva,   
			 :ls_cod_fiscale,   
			 :ls_cod_lingua,   
			 :ls_flag_tipo_cliente,
			 :ls_telefono,
			 :ls_fax
	from   anag_contatti  
	where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and
			 anag_contatti.cod_contatto = :ls_cod_contatto;

	if sqlca.sqlcode = 0 then
		if ls_flag_tipo_cliente = 'E' or ls_flag_tipo_cliente = 'C' then
			ls_partita_iva = ls_cod_fiscale
		end if
	end if
end if


if not isnull(ls_stato_cli) and ls_stato_cli<>"" then
	if not isnull(ls_localita_cli) and ls_localita_cli<>"" then
		ls_localita_cli += " - " + ls_stato_cli
	else
		ls_localita_cli = ls_stato_cli
	end if
end if


select tab_tipi_bol_ven.des_tipo_bol_ven,
		 tab_tipi_bol_ven.flag_tipo_bol_ven
into   :ls_des_tipo_bol_ven, 
		 :ls_flag_tipo_bol_ven
from   tab_tipi_bol_ven  
where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and
       tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_bol_ven)
	setnull(ls_flag_tipo_bol_ven)
end if

if ls_flag_tipo_bol_ven = "T" then
   dw_report_bol_ven.modify("st_cliente_fornitore_dep.text='Deposito:'")

	// Stefanop: 17/05/2012: dopo consultazione con Beatrice si è deciso che la ragione sociale è quella del cliente
	// o fornitore e NON quella del deposito
	// ---------------------------------------
	//	ls_rag_soc_1_cli = ls_rag_soc_1
	//	ls_rag_soc_2_cli = ls_rag_soc_2   
	//	ls_indirizzo_cli = ls_indirizzo
	//	ls_localita_cli = ls_localita
	//	ls_frazione_cli = ls_frazione
	//	ls_cap_cli = ls_cap
	//	ls_provincia_cli = ls_provincia
	// ---------------------------------------
	
elseif ls_flag_tipo_bol_ven = "V" then
	// aggiunto contatto
	if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
		dw_report_bol_ven.modify("st_cliente_fornitore_dep.text='Cliente:'")
	else
		dw_report_bol_ven.modify("st_cliente_fornitore_dep.text='Contatto:'")
	end if
elseif ls_flag_tipo_bol_ven = "R" or &
		 ls_flag_tipo_bol_ven = "C" then
   dw_report_bol_ven.modify("st_cliente_fornitore_dep.text='Fornitore:'")
end if

select tab_porti.des_porto  
into   :ls_des_porto  
from   tab_porti  
where  tab_porti.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti.cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select tab_porti_lingue.des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  tab_porti_lingue.cod_azienda = :s_cs_xx.cod_azienda and
       tab_porti_lingue.cod_porto = :ls_cod_porto and
       tab_porti_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select tab_rese.des_resa
into   :ls_des_resa  
from   tab_rese  
where  tab_rese.cod_azienda = :s_cs_xx.cod_azienda and
       tab_rese.cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select tab_rese_lingue.des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  tab_rese_lingue.cod_azienda = :s_cs_xx.cod_azienda and 
       tab_rese_lingue.cod_resa = :ls_cod_resa and  
       tab_rese_lingue.cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

select des_causale
into   :ls_des_causale  
from   tab_causali_trasp  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_causale = :ls_cod_causale;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_causale)
end if

select des_causale
into   :ls_des_causale_lingua  
from   tab_causali_trasp_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_causale = :ls_cod_causale and  
       cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_causale_lingua)
end if
select des_mezzo
into   :ls_des_mezzo
from   tab_mezzi  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_mezzo = :ls_cod_mezzo;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo)
end if

select des_mezzo  
into   :ls_des_mezzo_lingua  
from   tab_mezzi_lingue 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_mezzo = :ls_cod_mezzo and
       cod_lingua = :ls_cod_lingua;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_mezzo_lingua)
end if


select anag_vettori.rag_soc_1,   
		 anag_vettori.rag_soc_2,   
		 anag_vettori.indirizzo,   
		 anag_vettori.localita,   
		 anag_vettori.frazione,   
		 anag_vettori.cap,   
		 anag_vettori.provincia
into   :ls_rag_soc_1_vet,   
		 :ls_rag_soc_2_vet,   
		 :ls_indirizzo_vet,   
		 :ls_localita_vet,   
		 :ls_frazione_vet,   
		 :ls_cap_vet,   
		 :ls_provincia_vet  
from   anag_vettori  
where  anag_vettori.cod_azienda = :s_cs_xx.cod_azienda and
		 anag_vettori.cod_vettore = :ls_cod_vettore;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_vet)
	setnull(ls_rag_soc_2_vet)
	setnull(ls_indirizzo_vet)
	setnull(ls_localita_vet)
	setnull(ls_frazione_vet)
	setnull(ls_cap_vet)
	setnull(ls_provincia_vet)
end if

select anag_vettori.rag_soc_1,   
		 anag_vettori.rag_soc_2,   
		 anag_vettori.indirizzo,   
		 anag_vettori.localita,   
		 anag_vettori.frazione,   
		 anag_vettori.cap,   
		 anag_vettori.provincia
into   :ls_rag_soc_1_ino,   
		 :ls_rag_soc_2_ino,   
		 :ls_indirizzo_ino,   
		 :ls_localita_ino,   
		 :ls_frazione_ino,   
		 :ls_cap_ino,   
		 :ls_provincia_ino  
from   anag_vettori  
where  anag_vettori.cod_azienda = :s_cs_xx.cod_azienda and
		 anag_vettori.cod_vettore = :ls_cod_inoltro;

if sqlca.sqlcode <> 0 then
	setnull(ls_rag_soc_1_ino)
	setnull(ls_rag_soc_2_ino)
	setnull(ls_indirizzo_ino)
	setnull(ls_localita_ino)
	setnull(ls_frazione_ino)
	setnull(ls_cap_ino)
	setnull(ls_provincia_ino)
end if

declare cu_dettagli cursor for 
	select   det_bol_ven.cod_tipo_det_ven, 
				det_bol_ven.cod_misura, 
				det_bol_ven.quan_consegnata, 
				det_bol_ven.nota_dettaglio, 
				det_bol_ven.cod_prodotto, 
				det_bol_ven.des_prodotto,
				det_bol_ven.fat_conversione_ven,
				det_bol_ven.flag_st_note_det				
	from     det_bol_ven 
	where    det_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				det_bol_ven.anno_registrazione = :il_anno_registrazione and 
				det_bol_ven.num_registrazione = :il_num_registrazione
	order by det_bol_ven.cod_azienda, 
				det_bol_ven.anno_registrazione, 
				det_bol_ven.num_registrazione,
				det_bol_ven.prog_riga_bol_ven;

open cu_dettagli;

do while 0 = 0
   fetch cu_dettagli into :ls_cod_tipo_det_ven, 
								  :ls_cod_misura, 
								  :ld_quan_consegnata, 
								  :ls_nota_dettaglio, 
								  :ls_cod_prodotto, 
								  :ls_des_prodotto,
								  :ld_fat_conversione_ven,
								  :ls_flag_st_note_det;								  

   if sqlca.sqlcode <> 0 then exit

	dw_report_bol_ven.insertrow(0)
	dw_report_bol_ven.setrow(dw_report_bol_ven.rowcount())

	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_documento", ls_cod_documento)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_numeratore_documento", ls_numeratore_documento)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_anno_documento", ll_anno_documento)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_num_documento", ll_num_documento)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_tipo_bol_ven", ls_cod_tipo_bol_ven)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_data_bolla", ldt_data_bolla)
	if ls_flag_tipo_bol_ven = "V" then
		// Aggiunto controllo contatto
		if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_cliente", ls_cod_cliente)
		else
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_cliente", ls_cod_contatto)
		end if
	elseif ls_flag_tipo_bol_ven = "R" or &
			 ls_flag_tipo_bol_ven = "C" then
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_cliente", ls_cod_fornitore)
	elseif ls_flag_tipo_bol_ven = "T" then
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_cliente", ls_cod_deposito_tras)
	end if
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_rag_soc_1", ls_rag_soc_1)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_rag_soc_2", ls_rag_soc_2)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_indirizzo", ls_indirizzo)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_localita", ls_localita)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_frazione", ls_frazione)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cap", ls_cap)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_provincia", ls_provincia)
	
//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_st_note_tes = 'I' then   //nota di testata
		select flag_st_note
		  into :ls_flag_st_note_tes
		  from tab_tipi_bol_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
	end if		

	if ls_flag_st_note_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_st_note_pie = 'I' then //nota di piede
		select flag_st_note
		  into :ls_flag_st_note_pie
		  from tab_tipi_bol_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
	end if			
	
	if ls_flag_st_note_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------	
	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_nota_testata", ls_nota_testata)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_nota_piede", ls_nota_piede)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_porto", ls_cod_porto)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_cod_resa", ls_cod_resa)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_causale_trasporto", ls_des_causale)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_causale_trasp_lingua", ls_des_causale_lingua)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_mezzi_des_mezzo", ls_des_mezzo)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_mezzi_lingue_des_mezzo", ls_des_mezzo_lingua)	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_aspetto_beni", ls_aspetto_beni)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_num_colli", ll_num_colli)
	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_indirizzo", ls_indirizzo_cli)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_localita", ls_localita_cli)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_frazione", ls_frazione_cli)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_cap", ls_cap_cli)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_provincia", ls_provincia_cli)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_partita_iva", ls_partita_iva)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_telefono", ls_telefono)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_clienti_fax", ls_fax)
	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_tipi_bol_ven_des_tipo_bol_ven", ls_des_tipo_bol_ven)
	
	select tab_tipi_det_ven.flag_stampa_bolla
	into   :ls_flag_stampa_bolla
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if sqlca.sqlcode <> 0 then
		setnull(ls_flag_stampa_bolla)
	end if

	if ls_flag_stampa_bolla = 'S' then
		select anag_prodotti.des_prodotto  
		into   :ls_des_prodotto_anag  
		from   anag_prodotti  
		where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
		if sqlca.sqlcode <> 0 then
			setnull(ls_des_prodotto_anag)
		end if

		select anag_prodotti_lingue.des_prodotto  
		into   :ls_des_prodotto_lingua  
		from   anag_prodotti_lingue  
		where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
				 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
				 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
		
		if sqlca.sqlcode <> 0 then
			setnull(ls_des_prodotto_lingua)
		end if

		select tab_prod_clienti.cod_prod_cliente  
		into   :ls_cod_prod_cliente  
		from   tab_prod_clienti  
		where  tab_prod_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_prod_clienti.cod_prodotto = :ls_cod_prodotto and   
				 tab_prod_clienti.cod_cliente = :ls_cod_cliente;

		if sqlca.sqlcode <> 0 then
			setnull(ls_cod_prod_cliente)
		end if

		if not isnull(ls_cod_fornitore) then
			select tab_prod_fornitori.cod_prod_fornitore  
			into   :ls_cod_prod_cliente
			from   tab_prod_fornitori  
			where  tab_prod_fornitori.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_prod_fornitori.cod_prodotto = :ls_cod_prodotto and   
					 tab_prod_fornitori.cod_fornitore = :ls_cod_fornitore;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_cod_prod_cliente)
			end if
		end if
		
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_cod_misura", ls_cod_misura)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_quan_consegnata", ld_quan_consegnata * ld_fat_conversione_ven)
		
//------------------------------------------------- Modifica Nicola ---------------------------------------------
	if ls_flag_st_note_det = 'I' then //nota dettaglio
		select flag_st_note_bl
		  into :ls_flag_st_note_det
		  from tab_tipi_det_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		
	
	if ls_flag_st_note_det = 'N' then
		ls_nota_dettaglio = ""
	end if				
//-------------------------------------------------- Fine Modifica ----------------------------------------------		
		
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_nota_dettaglio", ls_nota_dettaglio)
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_cod_prodotto", ls_cod_prodotto)
		
//		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_des_prodotto)
//
//		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_prodotti_des_prodotto", ls_des_prodotto_anag)
//		
//		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_prodotti_lingue_des_prodotto", ls_des_prodotto_lingua)
		
		dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_prod_clienti_cod_prod_cliente", ls_cod_prod_cliente)

		if not isnull(ls_cod_lingua) and not isnull(ls_des_prodotto_lingua) then
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_prodotti_lingue_des_prodotto", ls_des_prodotto_lingua)
		elseif not isnull(ls_des_prodotto) then
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "det_bol_ven_des_prodotto", ls_des_prodotto)
		elseif not isnull(ls_des_prodotto_anag) then
			dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_prodotti_des_prodotto", ls_des_prodotto_anag)
		end if

	end if
	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_porti_des_porto", ls_des_porto)
	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_rese_des_resa", ls_des_resa)
	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)

	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_rag_soc_1", ls_rag_soc_1_vet)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_rag_soc_2", ls_rag_soc_2_vet)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_indirizzo", ls_indirizzo_vet)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_localita", ls_localita_vet)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_frazione", ls_frazione_vet)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_cap", ls_cap_vet)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_vettori_provincia", ls_provincia_vet)

	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_rag_soc_1", ls_rag_soc_1_ino)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_rag_soc_2", ls_rag_soc_2_ino)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_indirizzo", ls_indirizzo_ino)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_localita", ls_localita_ino)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_frazione", ls_frazione_ino)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_cap", ls_cap_ino)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "anag_inoltro_provincia", ls_provincia_ino)

	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_data_inizio_trasporto", ldt_data_inizio_trasporto)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "tes_bol_ven_ora_inizio_trasporto", ldt_ora_inizio_trasporto)
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "peso_netto", ld_peso_netto)	
	dw_report_bol_ven.setitem(dw_report_bol_ven.getrow(), "peso_lordo", ld_peso_lordo)
loop
close cu_dettagli;
dw_report_bol_ven.reset_dw_modified(c_resetchildren)
end subroutine

event pc_setwindow;call super::pc_setwindow;string ls_path_logo_1, ls_path_logo_2, ls_modify, ls_cod_tipo_bol_ven, ls_cod_cliente, ls_flag_tipo, ls_cod_fornitore, &
		ls_cod_contatto
long ll_copie, ll_copie_cee, ll_copie_extra_cee

dw_report_bol_ven.ib_dw_report = true

set_w_options(c_noresizewin)
il_anno_registrazione = s_cs_xx.parametri.parametro_d_1
il_num_registrazione  = s_cs_xx.parametri.parametro_d_2

dw_report_bol_ven.set_dw_options(sqlca, &
                                 pcca.null_object, &
                                 c_nonew + &
                                 c_nomodify + &
                                 c_nodelete + &
                                 c_noenablenewonopen + &
                                 c_noenablemodifyonopen + &
                                 c_scrollparent + &
											c_disablecc, &
											c_noresizedw + &
                                 c_nohighlightselected + &
                                 c_nocursorrowfocusrect + &
                                 c_nocursorrowpointer)
											
dw_report_bol_ven.set_document_name("Bolla Vendita " + string(il_anno_registrazione) + "/" + string(il_num_registrazione))
											

// *** Michela 19/12/2005: se la data del documento è inferiore al parametro aziendale DLD metto 
//                         il logo contenuto nel parametro LI2

datetime ldt_dld, ldt_data_registrazione

select data
into   :ldt_dld
from   parametri_azienda 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'DLD';

if sqlca.sqlcode <> 0 then	
	setnull(ldt_dld)
end if

select data_registrazione 
into   :ldt_data_registrazione	 
from   tes_bol_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and  
		 anno_registrazione = :il_anno_registrazione and 
		 num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then	
	setnull(ldt_data_registrazione)
end if		 

if not isnull(ldt_data_registrazione) and not isnull(ldt_dld) then
	if ldt_data_registrazione < ldt_dld then
		select parametri_azienda.stringa
		into   :ls_path_logo_1
		from   parametri_azienda
		where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
				 parametri_azienda.flag_parametro = 'S' and &
				 parametri_azienda.cod_parametro = 'I2L';		
	else
		select parametri_azienda.stringa
		into   :ls_path_logo_1
		from   parametri_azienda
		where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
				 parametri_azienda.flag_parametro = 'S' and &
				 parametri_azienda.cod_parametro = 'LO1';		
	end if
else
		select parametri_azienda.stringa
		into   :ls_path_logo_1
		from   parametri_azienda
		where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
				 parametri_azienda.flag_parametro = 'S' and &
				 parametri_azienda.cod_parametro = 'LO1';	
end if											
												
//select parametri_azienda.stringa
//into   :ls_path_logo_1
//from   parametri_azienda
//where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
//       parametri_azienda.flag_parametro = 'S' and &
//       parametri_azienda.cod_parametro = 'LO1';

select parametri_azienda.stringa
into   :ls_path_logo_2
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO2';

ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
dw_report_bol_ven.modify(ls_modify)

ls_modify = "piede.filename='" + s_cs_xx.volume + ls_path_logo_2 + "'~t"
dw_report_bol_ven.modify(ls_modify)

save_on_close(c_socnosave)


ib_estero = false
il_num_copie = 3

select cod_cliente,
		 cod_fornitore,
		 cod_contatto,
       	cod_tipo_bol_ven
into   :ls_cod_cliente,
		 :ls_cod_fornitore,
		 :ls_cod_contatto,
		 :ls_cod_tipo_bol_ven
from   tes_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione and
		 num_registrazione = :il_num_registrazione;
		 
if sqlca.sqlcode = 0 then
	if not isnull(ls_cod_cliente) then
		select flag_tipo_cliente
		into   :ls_flag_tipo
		from   anag_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente;
	elseif not isnull(ls_cod_fornitore) then
		select flag_tipo_fornitore
		into   :ls_flag_tipo
		from   anag_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore;
	elseif not isnull(ls_cod_contatto) then
		select flag_tipo_cliente
		into   :ls_flag_tipo
		from   anag_contatti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_contatto = :ls_cod_contatto;

	end if
	
	
	if sqlca.sqlcode = 0 then
		select num_copie,
				 num_copie_cee,
				 num_copie_extra_cee
		into   :ll_copie,
				 :ll_copie_cee,
				 :ll_copie_extra_cee
		from   tab_tipi_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
		if sqlca.sqlcode = 0 then
			choose case ls_flag_tipo
				case "C"
					ib_estero = true
					il_num_copie = ll_copie_cee
				case "E"
					ib_estero = true
					il_num_copie = ll_copie_extra_cee
				case else
					ib_estero = false
					il_num_copie = ll_copie
			end choose
		end if
	end if
	
end if
st_copie.text = string(il_num_copie)
dw_report_bol_ven.object.datawindow.print.preview = 'Yes'
dw_report_bol_ven.object.datawindow.print.preview.rulers = 'Yes'
dw_report_bol_ven.postevent("pcd_print")
end event

on w_report_bol_ven.create
int iCurrent
call super::create
this.cb_invia_pdf_outlook=create cb_invia_pdf_outlook
this.cb_1=create cb_1
this.st_copie=create st_copie
this.st_1=create st_1
this.dw_report_bol_ven=create dw_report_bol_ven
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_invia_pdf_outlook
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.st_copie
this.Control[iCurrent+4]=this.st_1
this.Control[iCurrent+5]=this.dw_report_bol_ven
end on

on w_report_bol_ven.destroy
call super::destroy
destroy(this.cb_invia_pdf_outlook)
destroy(this.cb_1)
destroy(this.st_copie)
destroy(this.st_1)
destroy(this.dw_report_bol_ven)
end on

type cb_invia_pdf_outlook from commandbutton within w_report_bol_ven
integer x = 1157
integer y = 20
integer width = 759
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Invia PDF mediante Outlook"
end type

event clicked;long ll_hold_1, ll_hold_2
boolean lb_hold

 ll_hold_1 = il_pagina_corrente
 ll_hold_2 = il_num_copie
 lb_hold = ib_stampa

il_pagina_corrente = 1
il_num_copie = 1
ib_stampa = true

//for il_pagina_corrente = 1 to il_totale_pagine
	parent.triggerevent("ue_anteprima_pdf")
//next
//parent.triggerevent("pc_print")

 il_pagina_corrente = ll_hold_1
 il_num_copie = ll_hold_2
 ib_stampa = lb_hold
 
end event

type cb_1 from commandbutton within w_report_bol_ven
integer x = 571
integer y = 20
integer width = 503
integer height = 88
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "STAMPA COPIE"
end type

event clicked;long ll_i, ll_y


ib_stampa = true

for il_pagina_corrente = 1 to il_totale_pagine
	
	for ll_i = 1 to il_num_copie
		
		dw_report_bol_ven.triggerevent("pcd_print")
		
	next
	
	parent.postevent("ue_avviso_spedizione")
	
next

end event

type st_copie from statictext within w_report_bol_ven
integer x = 297
integer y = 20
integer width = 247
integer height = 88
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean enabled = false
alignment alignment = center!
boolean focusrectangle = false
end type

type st_1 from statictext within w_report_bol_ven
integer x = 23
integer y = 20
integer width = 274
integer height = 88
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "NR.COPIE:"
boolean focusrectangle = false
end type

type dw_report_bol_ven from uo_cs_xx_dw within w_report_bol_ven
integer x = 23
integer y = 140
integer width = 3931
integer height = 2160
integer taborder = 20
string dataobject = "d_report_bol_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;wf_report()
end event

event pcd_first;call super::pcd_first;wf_report()
end event

event pcd_last;call super::pcd_last;wf_report()
end event

event pcd_next;call super::pcd_next;wf_report()
end event

event pcd_previous;call super::pcd_previous;wf_report()
end event

event printpage;call super::printpage;if not(ib_stampa) then return 1
if pagenumber <> il_pagina_corrente then return 1
end event

event printstart;call super::printstart;il_totale_pagine = pagesmax
end event

