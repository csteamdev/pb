﻿$PBExportHeader$w_tes_bol_ven.srw
forward
global type w_tes_bol_ven from w_cs_xx_treeview
end type
type dw_1 from uo_cs_xx_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_2 from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_2 dw_2
end type
type det_3 from userobject within tab_dettaglio
end type
type dw_3 from uo_cs_xx_dw within det_3
end type
type det_3 from userobject within tab_dettaglio
dw_3 dw_3
end type
type det_4 from userobject within tab_dettaglio
end type
type dw_4 from uo_cs_xx_dw within det_4
end type
type det_4 from userobject within tab_dettaglio
dw_4 dw_4
end type
end forward

global type w_tes_bol_ven from w_cs_xx_treeview
integer width = 4722
integer height = 2788
string title = "Bolle di Vendita"
event pc_menu_sblocca ( )
event pc_menu_corrisp ( )
event pc_menu_blocca ( )
event pc_menu_trasporto ( )
event pc_menu_calcola ( )
event pc_menu_note ( )
event pc_menu_stampa ( )
event pc_menu_dettagli ( )
event pc_menu_conferma ( )
event pc_menu_ricalcola_spese_trasp ( )
event pc_menu_invio_terzista ( )
end type
global w_tes_bol_ven w_tes_bol_ven

type variables
private:
	string is_sql_prodotto, is_ordini_riaperti
	long il_livello
	int ICONA_BOLLA, ICONA_DEPOSITO, ICONA_PRODOTTO, ICONA_PAGAMENTO, ICONA_CAUSALE_TRASP
	int ICONA_TIPO_BOLLA, ICONA_CLIENTE, ICONA_FORNITORE, ICONA_CONTATTO, ICONA_BOLLA_CONFERMATA
	int ICONA_BOLLA_PARZIALE, ICONA_NEW
	datastore ids_store
	long il_max_record = 200
	
	//Gestione terzisti tramite scarico parziale ramo commessa interna
	boolean ib_SPT = false


protected:
	uo_fido_cliente		iuo_fido
	string					is_cod_parametro_blocco = "IBV"
	boolean				ib_spese_trasporto=false
	
	
public:
	boolean ib_new = false
	boolean ib_menu_conferma = false
	boolean ib_menu_trasporto = true
end variables

forward prototypes
public subroutine wf_imposta_ricerca ()
public subroutine wf_valori_livelli ()
public function long wf_inserisci_bolla_vendita (long al_handle, long al_anno_registrazione, long al_num_registrazione)
public function long wf_inserisci_bolle_vendita (long al_handle)
public function long wf_inserisci_fornitori (long al_handle)
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public subroutine wf_abilita_pulsanti_ricerca (boolean ab_status)
public subroutine wf_carica_contatto (string as_cod_contatto, boolean ab_all_data)
public subroutine wf_flag_cliente_contatto (string as_flag_tipo)
public subroutine wf_reset_tipo_bolla ()
public subroutine wf_tipo_bolla (string as_flag_tipo_bol_ven, boolean ab_rowfocuschange)
public function long wf_inserisci_prodotti (long al_handle)
public function long wf_inserisci_depositi (long al_handle)
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public subroutine wf_treeview_icons ()
public function long wf_inserisci_pagamenti (long al_handle)
public function long wf_inserisci_causali_trasp (long al_handle)
public function long wf_inserisci_tipi_bolle (long al_handle)
public function long wf_inserisci_clienti (long al_handle)
public function long wf_inserisci_contatti (long al_handle)
public function integer wf_carica_cliente (string as_cod_cliente, boolean ab_set_all_data)
public function integer wf_carica_fornitore (string as_cod_fornitore, boolean ab_set_all_data)
public subroutine wf_carica_deposito_trasf (string as_cod_deposito)
public subroutine wf_carica_deposito (string as_cod_deposito)
public function integer wf_riapri_ordini (integer ai_anno_ddt, long al_num_ddt, ref string as_errore)
public subroutine wf_lista_ordini_riaperti (string as_ordine, ref string as_ordini[], ref string as_testo)
end prototypes

event pc_menu_sblocca();long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_progr_stock, ll_prog_riga_bol_ven
string ls_cod_tipo_det_ven, ls_cod_prodotto, ls_sql_stringa, ls_flag_tipo_det_ven, &
		 ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto 
double ld_quan_consegnata, ld_quan_disponibile
datetime ldt_data_stock

if tab_dettaglio.det_1.dw_1.getrow()< 1 then return

if tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_blocco") = "S" then
	
	ll_anno_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "anno_registrazione")
	ll_num_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(tab_dettaglio.det_1.dw_1.getrow(), "num_registrazione")

	declare cu_dettagli dynamic cursor for sqlsa;

	ls_sql_stringa = "select det_bol_ven.cod_tipo_det_ven, det_bol_ven.cod_prodotto, det_bol_ven.quan_consegnata, det_bol_ven.cod_deposito, det_bol_ven.cod_ubicazione, det_bol_ven.cod_lotto, det_bol_ven.data_stock, det_bol_ven.progr_stock, det_bol_ven.prog_riga_bol_ven from det_bol_ven where det_bol_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_bol_ven.anno_registrazione = " + string(ll_anno_registrazione) + " and det_bol_ven.num_registrazione = " + string(ll_num_registrazione)

	prepare sqlsa from :ls_sql_stringa;
	open dynamic cu_dettagli;

	ll_i = 0
	do while true
		ll_i ++
		
		fetch cu_dettagli into :ls_cod_tipo_det_ven, :ls_cod_prodotto, :ld_quan_consegnata, :ls_cod_deposito, :ls_cod_ubicazione, :ls_cod_lotto, :ldt_data_stock, :ll_progr_stock, :ll_prog_riga_bol_ven;
			
		if sqlca.sqlcode = -1 then
			g_mb.error("Si è verificato un errore in fase di lettura per aggiornamento magazzino.")
			close cu_dettagli;
			rollback;
			return
		end if

		if sqlca.sqlcode <> 0 then exit

		select tab_tipi_det_ven.flag_tipo_det_ven
		into :ls_flag_tipo_det_ven
		from tab_tipi_det_ven
		where tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode = -1 then
			g_mb.error("Si è verificato un errore in fase di lettura tipi dettagli.")
			close cu_dettagli;
			rollback;
			return
		end if

		if ls_flag_tipo_det_ven = "M" then
			select stock.giacenza_stock - (stock.quan_assegnata + stock.quan_in_spedizione)  
			into :ld_quan_disponibile
			from stock
			where stock.cod_azienda = :s_cs_xx.cod_azienda and
					stock.cod_prodotto = :ls_cod_prodotto and
					stock.cod_deposito = :ls_cod_deposito and
					stock.cod_ubicazione = :ls_cod_ubicazione and
					stock.cod_lotto = :ls_cod_lotto and
					stock.data_stock = :ldt_data_stock and
					stock.prog_stock = :ll_progr_stock;

			if ld_quan_consegnata > ld_quan_disponibile then
				g_mb.warning("Nella Riga " + string(ll_prog_riga_bol_ven) + " Quantità bolla maggiore della quantità disponibile " + &
					f_double_string(ld_quan_disponibile) + ".")
				rollback;
				return
			end if

			update anag_prodotti  
			set quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
			where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
				 	 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode = -1 then
				g_mb.error("Si è verificato un errore in fase di aggiornamento magazzino.", sqlca)
				rollback;
				close cu_dettagli;
				return
			end if
		end if				
		
		update stock
		set quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
		 where stock.cod_azienda = :s_cs_xx.cod_azienda and  
				 stock.cod_prodotto = :ls_cod_prodotto and
				 stock.cod_deposito = :ls_cod_deposito and
				 stock.cod_ubicazione = :ls_cod_ubicazione and
				 stock.cod_lotto = :ls_cod_lotto and
				 stock.data_stock = :ldt_data_stock and
				 stock.prog_stock = :ll_progr_stock;

		if sqlca.sqlcode = -1 then
			g_mb.error("Si è verificato un errore in fase di aggiornamento stock.", sqlca)
			rollback;
			return
		end if
		
	loop
	
	update tes_bol_ven
	set flag_blocco = 'N'
	where tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			tes_bol_ven.anno_registrazione = :ll_anno_registrazione and  
			tes_bol_ven.num_registrazione = :ll_num_registrazione;
			
	if sqlca.sqlcode = -1 then
		g_mb.error("Si è verificato un errore in fase di aggiornamento testata boline.", sqlca)
		rollback;
		close cu_dettagli;
		return
	end if

	commit;
	close cu_dettagli;
	
	tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "flag_blocco", "N")
	tab_dettaglio.det_1.dw_1.setitemstatus(tab_dettaglio.det_1.dw_1.getrow(), "flag_blocco", primary!, notmodified!)
end if
end event

event pc_menu_corrisp();s_cs_xx.parametri.parametro_s_1 = "Bol_Ven"
window_open_parm(w_acq_ven_corr, -1, tab_dettaglio.det_1.dw_1)

end event

event pc_menu_blocca();string ls_cod_tipo_det_ven, ls_cod_prodotto, ls_sql_stringa, ls_flag_tipo_det_ven, &
		ls_cod_deposito ,ls_cod_ubicazione ,ls_cod_lotto
int li_row
long 	ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_bol_ven, ll_progr_stock
double ld_quan_consegnata
datetime ldt_data_stock

li_row = tab_dettaglio.det_1.dw_1.getrow()

if li_row < 1 then return


if not isnull(tab_dettaglio.det_1.dw_1.getitemstring(li_row, "cod_documento")) then
	g_mb.show("Attenzione", "Bolla non chiudibile. Bolla già stampata in maniera definitiva.")
	return
end if

if tab_dettaglio.det_1.dw_1.getitemstring(li_row, "flag_movimenti") = "S" then
	g_mb.show("Attenzione", "Bolla non chiudibile. Bolla già confermata.")
	return
end if

if tab_dettaglio.det_1.dw_1.getitemstring(li_row, "flag_blocco") = "N" then
	ll_anno_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(li_row, "anno_registrazione")
	ll_num_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(li_row, "num_registrazione")

	declare cu_dettagli dynamic cursor for sqlsa;

	ls_sql_stringa = "select det_bol_ven.cod_tipo_det_ven, det_bol_ven.cod_prodotto, det_bol_ven.quan_consegnata, det_bol_ven.cod_deposito, det_bol_ven.cod_ubicazione, det_bol_ven.cod_lotto, det_bol_ven.data_stock, det_bol_ven.progr_stock from det_bol_ven where det_bol_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' and det_bol_ven.anno_registrazione = " + string(ll_anno_registrazione) + " and det_bol_ven.num_registrazione = " + string(ll_num_registrazione)

	prepare sqlsa from :ls_sql_stringa;

	open dynamic cu_dettagli;

	ll_i = 0
	do while true
		ll_i ++
		fetch cu_dettagli into :ls_cod_tipo_det_ven, :ls_cod_prodotto, :ld_quan_consegnata, :ls_cod_deposito, :ls_cod_ubicazione, :ls_cod_lotto, :ldt_data_stock, :ll_progr_stock;
		
		if sqlca.sqlcode = -1 then
			g_mb.error("Si è verificato un errore in fase di lettura per aggiornamento magazzino.")
			close cu_dettagli;
			rollback;
			return
		end if

		if sqlca.sqlcode <> 0 then exit

		select tab_tipi_det_ven.flag_tipo_det_ven
		into :ls_flag_tipo_det_ven
		from tab_tipi_det_ven
		where tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode = -1 then
			g_mb.error("Si è verificato un errore in fase di lettura tipi dettagli.")
			close cu_dettagli;
			rollback;
			return
		end if

		if ls_flag_tipo_det_ven = "M" then
			update anag_prodotti  
			set quan_in_spedizione = quan_in_spedizione - :ld_quan_consegnata
			where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
			if sqlca.sqlcode = -1 then
				g_mb.error("Si è verificato un errore in fase di aggiornamento magazzino.")
				close cu_dettagli;
				rollback;
				return
			end if
		end if				

		update stock
		set quan_in_spedizione = quan_in_spedizione - :ld_quan_consegnata
		where stock.cod_azienda = :s_cs_xx.cod_azienda and  
				 stock.cod_prodotto = :ls_cod_prodotto and
				 stock.cod_deposito = :ls_cod_deposito and
				 stock.cod_ubicazione = :ls_cod_ubicazione and
				 stock.cod_lotto = :ls_cod_lotto and
				 stock.data_stock = :ldt_data_stock and
				 stock.prog_stock = :ll_progr_stock;

		if sqlca.sqlcode = -1 then
			g_mb.error("Si è verificato un errore in fase di aggiornamento stock.")
			rollback;
			return
		end if

		update tes_bol_ven
		set flag_blocco = 'S'
		where tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tes_bol_ven.anno_registrazione = :ll_anno_registrazione and  
				 tes_bol_ven.num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode = -1 then
			g_mb.error("Si è verificato un errore in fase di aggiornamento testata boline.")
			close cu_dettagli;
			rollback;
			return
		end if
	loop

	close cu_dettagli;
	
	tab_dettaglio.det_1.dw_1.setitem(li_row, "flag_blocco", "S")
	tab_dettaglio.det_1.dw_1.setitemstatus(li_row, "flag_blocco", primary!, notmodified!)
	commit;
end if
end event

event pc_menu_trasporto();
window_open_parm(w_tes_bol_ven_det_list_vettori, -1, tab_dettaglio.det_1.dw_1)
end event

event pc_menu_calcola();string ls_messaggio
long   ll_num_registrazione, ll_anno_registrazione
string ls_cod_fornitore
uo_calcola_documento_euro luo_calcola_documento_euro
boolean	lb_salta_esposizione_cliente


if tab_dettaglio.det_1.dw_1.rowcount() < 1 then return

ll_anno_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_dettaglio.det_1.dw_1.getitemnumber(1, "num_registrazione")
lb_salta_esposizione_cliente = false

//se è stato inserito il fornitore (e non il cliente, caso di RESO AL FORNITORE o DDT conto lavoro)
//non deve fare il controllo sul fido e/o sullo scoperto che è tipico del cliente
ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(1, "cod_fornitore")
if ls_cod_fornitore <> "" and not isnull(ls_cod_fornitore)  then
	//è stato inserito il fornitore non fare il controllo sul fido....
	lb_salta_esposizione_cliente = true
end if


luo_calcola_documento_euro = create uo_calcola_documento_euro

//Donato 06-11-2008 metto a true per non fare di nuovo il controllo sull'esposizione cliente
luo_calcola_documento_euro.ib_salta_esposiz_cliente = lb_salta_esposizione_cliente
//-----------------	

if luo_calcola_documento_euro.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_ven",ls_messaggio) <> 0 then
	
	//Donato 05-11-2008 dare msg solo se c'è
	if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.error(ls_messaggio)
	//g_mb.messagebox("APICE",ls_messaggio)
	
	destroy luo_calcola_documento_euro
	rollback;
	return
else
	
	commit;
end if	



////Donato 13/11/2008
////se è stato inserito il fornitore (e non il cliente, caso di RESO AL FORNITORE)
////non deve fare il controllo sul fido e/o sullo scoperto
//ls_cod_fornitore = tab_dettaglio.det_1.dw_1.getitemstring(1, "cod_fornitore")
////if ls_cod_fornitore <> "" and not isnull(ls_cod_fornitore)  then
////	//è stato inserito il fornitore non fare il controllo sul fido....
////	commit;
////else
//if isnull(ls_cod_fornitore) or ls_cod_fornitore = "" then
//	luo_calcola_documento_euro = create uo_calcola_documento_euro
//
//	//Donato 06-11-2008 metto a true per non fare di nuovo il controllo sull'esposizione cliente
//	luo_calcola_documento_euro.ib_salta_esposiz_cliente = true
//	//-----------------	
//	
//	if luo_calcola_documento_euro.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"bol_ven",ls_messaggio) <> 0 then
//		
//		//Donato 05-11-2008 dare msg solo se c'è
//		if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.error(ls_messaggio)
//		//g_mb.messagebox("APICE",ls_messaggio)
//		
//		destroy luo_calcola_documento_euro
//		rollback;
//		return
//	else
//		
//		commit;
//	end if	
//end if

tab_dettaglio.det_1.dw_1.change_dw_current()

triggerevent("pc_retrieve")
end event

event pc_menu_note();s_cs_xx.parametri.parametro_s_1 = "Bol_Ven"
window_open_parm(w_acq_ven_note, -1, tab_dettaglio.det_1.dw_1)

end event

event pc_menu_stampa();string					ls_window = "w_report_bol_ven_euro", ls_bve, ls_cod_cliente, ls_messaggio, ls_cod_documento
long					ll_anno_reg_mov, ll_num_reg_mov, ll_count, ll_row
datetime				ldt_data_stock, ldt_data_reg
window				lw_window
integer				li_ret



ll_row = tab_dettaglio.det_1.dw_1.getrow()

tab_dettaglio.det_1.dw_1.accepttext()
s_cs_xx.parametri.parametro_i_1 = 0
s_cs_xx.parametri.parametro_d_1 = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
s_cs_xx.parametri.parametro_d_2 = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")

ls_cod_cliente = tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_cliente")
ldt_data_reg = tab_dettaglio.det_1.dw_1.getitemdatetime(ll_row, "data_registrazione")


declare cu_det_bol_ven cursor for
	select anno_reg_des_mov, num_reg_des_mov
	from det_bol_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
			 num_registrazione = :s_cs_xx.parametri.parametro_d_2;

open cu_det_bol_ven;

do while true
	fetch cu_det_bol_ven into :ll_anno_reg_mov, :ll_num_reg_mov;
   
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode < 0 then
		g_mb.error("Anno registrazione " + string(s_cs_xx.parametri.parametro_d_1) + "  num_registrazione " + string(s_cs_xx.parametri.parametro_d_2) + " non esistono in DET_BOL_VEV")
		exit
	end if	
	
	select count(*)
	into :ll_count
	from dest_mov_magazzino	  
	where cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_reg_mov and
			 num_registrazione = :ll_num_reg_mov and
			 (cod_deposito is null or cod_ubicazione is null or cod_lotto is null or data_stock is null or prog_stock is null);
			 
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il controllo della dest_mov_magazzino (" + string(ll_anno_reg_mov) + "/" + string(ll_num_reg_mov) + ")", sqlca)
		close cu_det_bol_ven;
		return
	elseif ll_count > 0 then
		g_mb.error("Attenzione nella tabella DES_MOV_MAGAZZINO i dati dello stock corrispondenti all'anno registrazione " + string(ll_anno_reg_mov) + ", numero registrazione " + string(ll_num_reg_mov) + " sono incompleti")
		close cu_det_bol_ven;
		return
	end if
loop

close cu_det_bol_ven;

// Calcolo il documento
triggerevent("pc_menu_calcola")

ls_cod_documento = tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_documento")
if ls_cod_documento<>"" and not isnull(ls_cod_documento) then
	//vuol dire che il ddt è stata già stampato in definitivo, quindi non controllare
else
	//***************************************************************
	li_ret = iuo_fido.uof_get_blocco_cliente( ls_cod_cliente,&
														ldt_data_reg, "SBV", ls_messaggio)
	if li_ret=2 then
		//blocco
		g_mb.error(ls_messaggio)
		return
	elseif li_ret=1 then
		//solo warning
		g_mb.warning(ls_messaggio)
	end if
	//***************************************************************
end if


if isnull(tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_documento")) then
	window_open(w_tipo_stampa_bol_ven, 0)
	this.event pc_retrieve(0,0)
end if

if s_cs_xx.parametri.parametro_i_1 <> -1 then
	
	guo_functions.uof_get_parametro_azienda("BVE", ls_bve)
	
	if isnull(ls_bve) or ls_bve = "" then
		window_open(w_report_bol_ven, -1)
	else
		window_type_open(lw_window,ls_window, -1)
	end if
	
end if
end event

event pc_menu_dettagli();s_cs_xx.parametri.parametro_w_bol_ven = this

window_open_parm(w_det_bol_ven, -1, tab_dettaglio.det_1.dw_1)

end event

event pc_menu_ricalcola_spese_trasp();long									ll_row, ll_num_documento
integer								li_anno_documento, li_risposta
uo_spese_trasporto				luo_spese_trasporto
uo_calcola_documento_euro	luo_calcolo
string									ls_errore



ll_row = tab_dettaglio.det_1.dw_1.getrow()

tab_dettaglio.det_1.dw_1.accepttext()

li_anno_documento = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
ll_num_documento = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")

if not g_mb.confirm("Ricalcolare le spese trasporto (in base alle Zone e Livelli Prodotti) per il d.d.t. n° " + &
						   string(li_anno_documento)+"/"+string(ll_num_documento)+"?") 		then
	return
end if


setpointer(Hourglass!)
pcca.mdi_frame.setmicrohelp("Ricalcolo Spese trasporto in corso... ")


luo_calcolo = create uo_calcola_documento_euro

//di seguito calcolo il documento per includere eventuali modifiche a prezzo o varie ... nell'imponobile delle righe
if luo_calcolo.uof_calcola_documento(li_anno_documento, ll_num_documento,"bol_ven", ls_errore) <> 0 then
	rollback;
	destroy luo_calcolo
	pcca.mdi_frame.setmicrohelp("Fine!")
	setpointer(Arrow!)
	g_mb.error(ls_errore)
	return
end if

commit;


//Ricalcolo le spese di trasporto
luo_spese_trasporto = create uo_spese_trasporto

//controlli preliminari prima del ricalcolo
//ritorno:			-1		errore critico
//					1		ricalcolo non possibile. dare messaggio e non effettuare il ricalcolo
//					2		ricalcolo possibile previa conferma dell'operatore
li_risposta = luo_spese_trasporto.uof_check_per_ricalcolo(li_anno_documento, ll_num_documento, "BOLVEN", ref ls_errore)

choose case li_risposta
	case -1			//errore critico
		rollback;
		destroy luo_spese_trasporto
		destroy luo_calcolo
		pcca.mdi_frame.setmicrohelp("Fine!")
		setpointer(Arrow!)
		g_mb.error(ls_errore)
		return
		
	case 1			//ricalcolo non possibile
		rollback;
		destroy luo_spese_trasporto
		destroy luo_calcolo
		pcca.mdi_frame.setmicrohelp("Fine!")
		setpointer(Arrow!)
		g_mb.warning(ls_errore)
		return
		
	case 2			//ricalcolo possibile previa conferma dell'operatore
		if not g_mb.confirm(ls_errore) then
			//annullato dall'operatore
			rollback;
			destroy luo_spese_trasporto
			destroy luo_calcolo
			pcca.mdi_frame.setmicrohelp("Fine!")
			setpointer(Arrow!)
			g_mb.show("Annullato dall'operatore!")
			return
		end if
		
	case else
		//prosegui con il ricalcolo
		
end choose



if luo_spese_trasporto.uof_calcola_spese(li_anno_documento, ll_num_documento, "BOLVEN", ref ls_errore) < 0 then
	rollback;
	destroy luo_spese_trasporto
	destroy luo_calcolo
	pcca.mdi_frame.setmicrohelp("Fine!")
	setpointer(Arrow!)
	g_mb.error(ls_errore)
	return
end if

//commit necessario per evitare lock successivi (apertura finestra visualizzazione storico)
commit;


//di seguito ri-calcolo il documento per includere nel calcolo le spese di trasporto
if luo_calcolo.uof_calcola_documento(li_anno_documento, ll_num_documento,"bol_ven", ls_errore) <> 0 then
	rollback;
	destroy luo_calcolo
	destroy luo_spese_trasporto
	pcca.mdi_frame.setmicrohelp("Fine!")
	setpointer(Arrow!)
	g_mb.error(ls_errore)
	return
end if

//qui posso distruggere l'oggetto calcola documento
destroy luo_calcolo

//commit necessario per evitare lock successivi (apertura finestra visualizzazione storico)
commit;

luo_spese_trasporto.uof_log("Spese trasporto ricalcolate su fattura " + string(li_anno_documento) + "/" + string(ll_num_documento))


//valori di ritorno della funzione
//0		SPESE TRASPORTO CONFERMATE (o non abilitate dal parametro SSP), quindi non fare null'altro
//1		SPESE TRASPORTO ANNULLATE, quindi chiama la funzione che toglie le righe trasporto e che ricalcola il documento
//2		SPESE TRASPORTO DA SOLE PERCENTUALI IMPONIBILE RIGHE ORDINE. quindi chiama funzione opportuna
li_risposta = luo_spese_trasporto.uof_apri_storico(li_anno_documento, ll_num_documento, "BOLVEN")
		
if li_risposta = 0 then
else
	if li_risposta = 1 then
		//--------------------------------------------------------------------------------------------------------------------------------------------
		//ANNULLAMENTO SPESE TRASPORTO
		li_risposta = luo_spese_trasporto.uof_azzera_trasporto_documento(li_anno_documento, ll_num_documento, "BOLVEN", ls_errore)
		//--------------------------------------------------------------------------------------------------------------------------------------------
	elseif li_risposta = 2 then
		//--------------------------------------------------------------------------------------------------------------------------------------------
		//SPESE TRASPORTO DA SOLE PERCENTUALI IMPONIBILE RIGHE ORDINE
		li_risposta = luo_spese_trasporto.uof_imposta_trasporto_solo_percentuale(li_anno_documento, ll_num_documento, "BOLVEN", ls_errore)
		//--------------------------------------------------------------------------------------------------------------------------------------------
	end if
	
	if li_risposta<0 then
		rollback;
		destroy luo_spese_trasporto
		pcca.mdi_frame.setmicrohelp("Fine!")
		setpointer(Arrow!)
		g_mb.error(ls_errore)
		return
	end if
end if

destroy luo_spese_trasporto

setpointer(Arrow!)
pcca.mdi_frame.setmicrohelp("Fine!")

//se arrivi fin qui fai commit
commit;

g_mb.success("Operazione effettuata con successo!")

tab_dettaglio.det_1.dw_1.change_dw_current()
triggerevent("pc_retrieve")


end event

event pc_menu_invio_terzista();long									ll_row, ll_num_documento, ll_tot_righe
integer								li_anno_documento, li_risposta
uo_funzioni_1						luo_funz
uo_calcola_documento_euro	luo_calcolo
string									ls_errore



ll_row = tab_dettaglio.det_1.dw_1.getrow()
tab_dettaglio.det_1.dw_1.accepttext()

li_anno_documento = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "anno_registrazione")
ll_num_documento = tab_dettaglio.det_1.dw_1.getitemnumber(ll_row, "num_registrazione")

if li_anno_documento>0 and ll_num_documento>0 then
else
	g_mb.warning("Selezionare un DDT!")
	return
end if


setpointer(Hourglass!)

//torna 	-1 se fa errore critico
//			0 se non c'è nessuna riga da inserire, o warning vari che non permettono di inserire
//			oppure torna il numero di rihe inserite in DDT

luo_funz = create uo_funzioni_1
li_risposta = luo_funz.uof_invia_mp_terzista(li_anno_documento, ll_num_documento, ll_tot_righe, ls_errore)
destroy luo_funz

if li_risposta<0 then
	rollback;
	setpointer(Arrow!)
	g_mb.error(ls_errore)
	return
	
elseif li_risposta>0 then
	//annullato dall'operatore o warning vari
	rollback;
	setpointer(Arrow!)
	g_mb.warning(ls_errore)
	return
	
else
	//il valore di ritorno della fnzione è ZERO: nell'argomento "ll_tot_righe" ci sono le nuove righe inserite in DDT
	//righe inserite in DDT, fai un bel commit
	commit;
	
	g_mb.success("Operazione completata con successo! Inserite "+string(ll_tot_righe)+" righe in DDT. Premi OK per procedere al ricalcolo del documento!")
	pcca.mdi_frame.setmicrohelp("Ricalcolo Documento in corso... ")

	//ricalcolo del documento
	luo_calcolo = create uo_calcola_documento_euro
	
	//trattandosi di un DDT di uscita verso fornitore non controllare l'esposizione che è del cliente
	luo_calcolo.ib_salta_esposiz_cliente = true
	li_risposta = luo_calcolo.uof_calcola_documento(li_anno_documento, ll_num_documento,"bol_ven", ls_errore)
	destroy luo_calcolo
	
	if li_risposta <> 0 then
		rollback;
		pcca.mdi_frame.setmicrohelp("Fine!")
		setpointer(Arrow!)
		g_mb.error(ls_errore)
		return
	end if
	
	pcca.mdi_frame.setmicrohelp("Pronto!")
	commit;
end if

return
end event

public subroutine wf_imposta_ricerca ();long ll_anno_registrazione, ll_num_registrazione

is_sql_prodotto = ""
is_sql_filtro = ""
ll_anno_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "anno_registrazione")
ll_num_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "num_registrazione")
il_livello = 1

if not isnull(ll_anno_registrazione) and ll_anno_registrazione > 1900 then
	is_sql_filtro += " AND tes_bol_ven.anno_registrazione=" + string(ll_anno_registrazione)
end if
if not isnull(ll_num_registrazione) and ll_num_registrazione>0 then
	is_sql_filtro += " AND tes_bol_ven.num_registrazione=" + string(ll_num_registrazione)
end if
if not isnull(ll_anno_registrazione) and not isnull(ll_num_registrazione) and ll_anno_registrazione > 0 and ll_num_registrazione > 0 then
	// mi fermo non serve altro, filtro solo la singolo ordine
	return
end if

if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_documento")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_documento")<>"" then 
	is_sql_filtro += " AND tes_bol_ven.cod_documento='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_documento") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "anno_documento")) and tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "anno_documento")>0 then 
	is_sql_filtro += " AND tes_bol_ven.anno_documento=" + string(tab_ricerca.ricerca.dw_ricerca.getitemnumber(1,"anno_documento"))
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "numeratore_documento")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "numeratore_documento")<>"" then 
	is_sql_filtro += " AND tes_bol_ven.numeratore_documento='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"numeratore_documento") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "num_documento")) and tab_ricerca.ricerca.dw_ricerca.getitemnumber(1, "num_documento") > 0 then 
	is_sql_filtro += " AND tes_bol_ven.num_documento=" + string(tab_ricerca.ricerca.dw_ricerca.getitemnumber(1,"num_documento"))
end if
// ----

if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_bol_ven")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_bol_ven")<>"" then
	is_sql_filtro += " AND tes_bol_ven.cod_tipo_bol_ven='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_bol_ven") +"'"
end if

if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_registrazione_da")) and year(date(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_registrazione_da")))>1900 then
	is_sql_filtro += " AND tes_bol_ven.data_registrazione>='" + string(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_registrazione_da"), s_cs_xx.db_funzioni.formato_data) +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_registrazione_a")) and year(date(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_registrazione_a")))>1900 then
	is_sql_filtro += " AND tes_bol_ven.data_registrazione<='" + string(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_registrazione_a"), s_cs_xx.db_funzioni.formato_data) +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_bolla_da")) and year(date(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_bolla_da")))>1900 then
	is_sql_filtro += " AND tes_bol_ven.data_bolla >='" + string(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_bolla_da"), s_cs_xx.db_funzioni.formato_data) +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_bolla_a")) and year(date(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_bolla_a")))>1900 then
	is_sql_filtro += " AND tes_bol_ven.data_bolla <='" + string(tab_ricerca.ricerca.dw_ricerca.getitemdate(1,"data_bolla_a"), s_cs_xx.db_funzioni.formato_data) +"'"
end if

if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_cliente")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_cliente")<>"" then
	is_sql_filtro += " AND tes_bol_ven.cod_cliente='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_cliente") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fornitore")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fornitore")<>"" then
	is_sql_filtro += " AND tes_bol_ven.cod_fornitore='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_fornitore") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_contatto")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_contatto")<>"" then
	is_sql_filtro += " AND tes_bol_ven.cod_contatto='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_contatto") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto")<>"" then
	is_sql_filtro += " AND det_bol_ven.cod_prodotto='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto")) OR wf_has_livello("X") then
	is_sql_prodotto = " JOIN det_bol_ven on det_bol_ven.cod_azienda=tes_bol_ven.cod_azienda and "+&
							"det_bol_ven.anno_registrazione=tes_bol_ven.anno_registrazione and "+&
							"det_bol_ven.num_registrazione=tes_bol_ven.num_registrazione "
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito")<>"" then
	is_sql_filtro += " AND tes_bol_ven.cod_deposito='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_causale")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_causale")<>"" then
	is_sql_filtro += " AND tes_bol_ven.cod_causale='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_causale") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_pagamento")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_pagamento")<>"" then
	is_sql_filtro += " AND tes_bol_ven.cod_pagamento='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_pagamento") +"'"
end if
if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_vettore")) and tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_vettore")<>"" then
	is_sql_filtro += " AND tes_bol_ven.cod_vettore='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_vettore") +"'"
end if

if  tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_gen_fat") <> 'X' then
	is_sql_filtro += " AND tes_bol_ven.flag_gen_fat='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_gen_fat") +"'"
end if
if tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_blocco") <> 'X'  then
	is_sql_filtro += " AND tes_bol_ven.flag_blocco='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_blocco") +"'"
end if
if tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_movimenti") <> 'X'  then
	is_sql_filtro += " AND tes_bol_ven.flag_movimenti='" + tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_movimenti") +"'"
end if
end subroutine

public subroutine wf_valori_livelli ();wf_add_valore_livello("Non Specificato", "N")
wf_add_valore_livello("Cliente", "C")
wf_add_valore_livello("Fornitore", "F")
wf_add_valore_livello("Contatto", "B")
wf_add_valore_livello("Tipo Bolla", "T")
wf_add_valore_livello("Causale Trasp.", "E")
wf_add_valore_livello("Deposito", "D")
wf_add_valore_livello("Pagamento", "P")
wf_add_valore_livello("Prodotto", "X")
end subroutine

public function long wf_inserisci_bolla_vendita (long al_handle, long al_anno_registrazione, long al_num_registrazione);string ls_sql, ls_error
long ll_handle
treeviewitem ltvi_item

str_treeview lstr_data
	
lstr_data.livello = il_livello
lstr_data.tipo_livello = "N"
lstr_data.decimale[1] = al_anno_registrazione
lstr_data.decimale[2] = al_num_registrazione

ltvi_item = wf_new_item(false, ICONA_NEW)

ltvi_item.data = lstr_data
ltvi_item.label = "[NEW] " + string(long(lstr_data.decimale[1])) + "/" + string(long(lstr_data.decimale[2]))

ib_fire_selection_changed = false

istr_data = lstr_data 

ll_handle = tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
tab_ricerca.selezione.tv_selezione.SelectItem(ll_handle)

ib_fire_selection_changed = true

return 1
end function

public function long wf_inserisci_bolle_vendita (long al_handle);string ls_sql, ls_error, ls_label, ls_cod_cliente, ls_cod_contatto, ls_cod_fornitore, ls_anag_clienti_rag_soc_1, ls_anag_clienti_localita, &
		ls_anag_contatti_rag_soc_1, ls_anag_contatti_localita, ls_anag_fornitori_rag_soc_1, ls_anag_fornitori_localita
long ll_rows, ll_i, ll_anno, ll_num
treeviewitem ltvi_item
str_treeview lstr_data


ls_sql = " SELECT tes_bol_ven.anno_registrazione, tes_bol_ven.num_registrazione, tes_bol_ven.flag_movimenti, " + &
			" tes_bol_ven.cod_documento, tes_bol_ven.anno_documento, tes_bol_ven.numeratore_documento, tes_bol_ven.num_documento, tes_bol_ven.cod_cliente, anag_clienti.rag_soc_1, anag_clienti.localita, tes_bol_ven.cod_fornitore, anag_fornitori.rag_soc_1, anag_fornitori.localita, tes_bol_ven.cod_contatto, anag_contatti.rag_soc_1, anag_contatti.localita " + &
			" FROM tes_bol_ven " + &
			" left outer join anag_clienti on tes_bol_ven.cod_azienda=anag_clienti.cod_azienda and tes_bol_ven.cod_cliente = anag_clienti.cod_cliente " + &
			" left outer join anag_fornitori on tes_bol_ven.cod_azienda=anag_fornitori.cod_azienda and tes_bol_ven.cod_fornitore = anag_fornitori.cod_fornitore " + &
			" left outer join anag_contatti on tes_bol_ven.cod_azienda=anag_contatti.cod_azienda and tes_bol_ven.cod_contatto = anag_contatti.cod_contatto " + &
			+ is_sql_prodotto + " WHERE tes_bol_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ls_sql += " ORDER BY tes_bol_ven.cod_documento, tes_bol_ven.anno_documento, tes_bol_ven.data_registrazione,tes_bol_ven.anno_registrazione, tes_bol_ven.num_registrazione"

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

// stefanop 02/07/2012: aggiungo controllo per il numero massimo di risultati
if ll_rows > il_max_record  then
	if not g_mb.confirm("Sono state recuperate " + string(ll_rows) + " superando il limite consigliato.~r~nProcedere con la visualizzazione di tutte le bolle?") then
		// Visualizzo solo le richieste nel limite
		ll_rows = il_max_record
	end if
end if

for ll_i = 1 to ll_rows
	
	ll_anno = ids_store.getitemnumber(ll_i, 1)
	ll_num = ids_store.getitemnumber(ll_i, 2)
	
	ls_cod_cliente = ids_store.getitemstring(ll_i, 8)
	ls_anag_clienti_rag_soc_1 = ids_store.getitemstring(ll_i, 9)
	ls_anag_clienti_localita = ids_store.getitemstring(ll_i, 10)
	ls_cod_fornitore = ids_store.getitemstring(ll_i, 11)
	ls_anag_fornitori_rag_soc_1 = ids_store.getitemstring(ll_i, 12)
	ls_anag_fornitori_localita	 = ids_store.getitemstring(ll_i, 13)
	ls_cod_contatto = ids_store.getitemstring(ll_i, 14)
	ls_anag_contatti_rag_soc_1 = ids_store.getitemstring(ll_i, 15)
	ls_anag_contatti_localita = ids_store.getitemstring(ll_i, 16)
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "N"
	lstr_data.decimale[1] = ll_anno
	lstr_data.decimale[2] = ll_num
	
	if ids_store.getitemstring(ll_i, 3) = "S" then
		ltvi_item = wf_new_item(false, ICONA_BOLLA_CONFERMATA)
	else
		ltvi_item = wf_new_item(false, ICONA_BOLLA)
	end if
	
	if not isnull(ids_store.getitemstring(ll_i, 4)) then
		ls_label = g_str.format("$1 $2/$3 $4", ids_store.getitemstring(ll_i, 4), ids_store.getitemnumber(ll_i, 5), ids_store.getitemstring(ll_i, 6), ids_store.getitemnumber(ll_i, 7))
	else
		ls_label = g_str.format(" - [$1/$2]", ll_anno, ll_num)
	end if
	
	if not isnull(ls_cod_cliente) then
		ls_label += g_str.format(" - $2 $3 ($1)",ls_cod_cliente, ls_anag_clienti_rag_soc_1,ls_anag_clienti_localita)
	elseif not isnull(ls_cod_fornitore) then
		ls_label += g_str.format(" - $2 $3 ($1)",ls_cod_fornitore, ls_anag_fornitori_rag_soc_1,ls_anag_fornitori_localita)
	elseif not isnull(ls_cod_contatto) then
		ls_label += g_str.format(" - $2 $3 ($1)",ls_cod_contatto, ls_anag_contatti_rag_soc_1,ls_anag_contatti_localita)
	else
		ls_label += " Cliente/Fornitore/Contatto non precisato."
	end if
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
	
next

destroy ids_store

return ll_rows
end function

public function long wf_inserisci_fornitori (long al_handle);string ls_sql, ls_label, ls_cod_fornitore, ls_des_fornitore, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_fornitori.cod_fornitore, anag_fornitori.rag_soc_1 FROM tes_bol_ven &
LEFT OUTER JOIN anag_fornitori ON anag_fornitori.cod_azienda = tes_bol_ven.cod_azienda AND &
anag_fornitori.cod_fornitore = tes_bol_ven.cod_fornitore " + is_sql_prodotto + " &
WHERE tes_bol_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows
	
	 ls_cod_fornitore = ids_store.getitemstring(ll_i, 1)
	 ls_des_fornitore = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "F"
	lstr_data.codice = ls_cod_fornitore
	
	if isnull(ls_cod_fornitore) and isnull(ls_des_fornitore) then
		ls_label = "<Fornitore mancante>"
	elseif isnull(ls_des_fornitore) then
		ls_label = ls_cod_fornitore
	else
		ls_label = ls_cod_fornitore + " - " + ls_des_fornitore
	end if

	ltvi_item = wf_new_item(true, ICONA_FORNITORE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_leggi_livello (long al_handle, integer ai_livello);il_livello = ai_livello

choose case wf_get_valore_livello(ai_livello)
		
	case "C"
		return wf_inserisci_clienti(al_handle)
		
	case "F"
		return wf_inserisci_fornitori(al_handle)
		
	case "B"
		return wf_inserisci_contatti(al_handle)
		
	case "T"
		return wf_inserisci_tipi_bolle(al_handle)
		
	case "E"
		return wf_inserisci_causali_trasp(al_handle)
		
	case "D"
		return wf_inserisci_depositi(al_handle)
		
//	case "L"
//		return wf_inserisci_tipi_listini(al_handle)
		
	case "P"
		return wf_inserisci_pagamenti(al_handle)
		
	case "X"
		return wf_inserisci_prodotti(al_handle)
						
	case else
		return wf_inserisci_bolle_vendita(al_handle)
		
end choose
end function

public subroutine wf_abilita_pulsanti_ricerca (boolean ab_status);tab_dettaglio.det_1.dw_1.object.b_ricerca_banca_for.enabled = ab_status
tab_dettaglio.det_1.dw_1.object.b_ricerca_cliente.enabled = ab_status
tab_dettaglio.det_1.dw_1.object.b_ricerca_fornitore.enabled = ab_status
tab_dettaglio.det_1.dw_1.object.b_ricerca_contatto.enabled = ab_status
end subroutine

public subroutine wf_carica_contatto (string as_cod_contatto, boolean ab_all_data);/**
 * stefanop
 * 09/02/2012
 *
 * Recupero i dati del contatto e li mostro nella datawindow
 **/

string ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cap, ls_localita, ls_provincia, ls_telefono, ls_fax, ls_telex, &
		ls_partita_iva, ls_cod_fiscale, ls_cod_deposito, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
		ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca_clien_for, ls_cod_imballo, ls_cod_vettore, ls_cod_cliente, &
		ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_flag_fuori_fido, ls_flag_riep_fatt, ls_flag_blocco, &
		ls_null, ls_messaggio, ls_nota_testata, ls_nota_piede, ls_nota_video
long ll_row
decimal{4} ld_sconto
datetime ldt_data_registrazione, ldt_data_blocco

setnull(ls_null)
ll_row = tab_dettaglio.det_1.dw_1.getrow()
ldt_data_registrazione = tab_dettaglio.det_1.dw_1.getitemdatetime(ll_row, "data_registrazione")

if isnull(as_cod_contatto) or len(as_cod_contatto) < 1 then
	//  resetto i campi ed esco
	tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_deposito", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_valuta", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_tipo_listino_prodotto", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_pagamento", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_row, "sconto", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_agente_1", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_agente_2", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_banca_clien_for", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_banca", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_row, "flag_fuori_fido", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_row, "flag_riep_fat", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_des_cliente", ls_null)
	tab_dettaglio.det_2.dw_2.modify("st_cod_clien_for_dep.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_cod_contatto.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_frazione.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_cap.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_localita.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_provincia.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_telefono.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_fax.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_telex.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_partita_iva.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_cod_fiscale.text=''")
	tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_imballo", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_vettore", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_inoltro", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_mezzo", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_porto", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_resa", ls_null)
	return
end if

select rag_soc_1,
		rag_soc_2,
		indirizzo,
		frazione,
		cap,
		localita,
		provincia,
		telefono,
		fax,
		telex,
		partita_iva,
		cod_fiscale,
		cod_deposito,
		cod_valuta,
		cod_tipo_listino_prodotto,
		cod_pagamento,
		sconto,
		cod_agente_1,
		cod_agente_2,
		cod_banca_clien_for,
		cod_imballo,
		cod_vettore,
		cod_inoltro,
		cod_mezzo,
		cod_porto,
		cod_resa,
		flag_fuori_fido,
		flag_riep_fatt,
		flag_blocco,
		data_blocco,
		cod_cliente
into   :ls_rag_soc_1,    
		:ls_rag_soc_2,
		:ls_indirizzo,    
		:ls_frazione,
		:ls_cap,
		:ls_localita,
		:ls_provincia,
		:ls_telefono,
		:ls_fax,
		:ls_telex,
		:ls_partita_iva,
		:ls_cod_fiscale,
		:ls_cod_deposito,
		:ls_cod_valuta,
		:ls_cod_tipo_listino_prodotto,
		:ls_cod_pagamento,
		:ld_sconto,
		:ls_cod_agente_1,
		:ls_cod_agente_2,
		:ls_cod_banca_clien_for,
		:ls_cod_imballo,
		:ls_cod_vettore,
		:ls_cod_inoltro,
		:ls_cod_mezzo,
		:ls_cod_porto,
		:ls_cod_resa,
		:ls_flag_fuori_fido,
		:ls_flag_riep_fatt,
		:ls_flag_blocco,
		:ldt_data_blocco,
		:ls_cod_cliente
from   anag_contatti
where	cod_azienda = :s_cs_xx.cod_azienda and 
			cod_contatto = :as_cod_contatto;
			

tab_dettaglio.det_2.dw_2.modify("st_cod_clien_for_dep.text='" + g_str.safe(as_cod_contatto) + "'")
tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text='" + g_str.safe(ls_rag_soc_1) + "'")
tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text='" + g_str.safe(ls_rag_soc_2) + "'")
tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text='" + g_str.safe(ls_indirizzo) + "'")
tab_dettaglio.det_2.dw_2.modify("st_frazione.text='" + g_str.safe(ls_frazione) + "'")
tab_dettaglio.det_2.dw_2.modify("st_cap.text='" + g_str.safe(ls_cap) + "'")
tab_dettaglio.det_2.dw_2.modify("st_localita.text='" + g_str.safe(ls_localita) + "'")
tab_dettaglio.det_2.dw_2.modify("st_provincia.text='" + g_str.safe(ls_provincia) + "'")
tab_dettaglio.det_2.dw_2.modify("st_telefono.text='" + g_str.safe(ls_telefono) + "'")
tab_dettaglio.det_2.dw_2.modify("st_fax.text='" + g_str.safe(ls_fax) + "'")
tab_dettaglio.det_2.dw_2.modify("st_telex.text='" + g_str.safe(ls_telex) + "'")
tab_dettaglio.det_2.dw_2.modify("st_partita_iva.text='" + g_str.safe(ls_partita_iva) + "'")
tab_dettaglio.det_2.dw_2.modify("st_cod_fiscale.text='" + g_str.safe(ls_cod_fiscale) + "'")


// devo caricare anche i dati del contatto o sono già presenti in DB?
if not ab_all_data then return

tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_deposito", ls_cod_deposito)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_valuta", ls_cod_valuta)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_pagamento", ls_cod_pagamento)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "sconto", ld_sconto)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_agente_1", ls_cod_agente_1)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_agente_2", ls_cod_agente_2)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_banca_clien_for", ls_cod_banca_clien_for)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "flag_fuori_fido", ls_flag_fuori_fido)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "flag_riep_fat", ls_flag_riep_fatt)
tab_dettaglio.det_2.dw_2.modify("st_label_clien_for_dep_1.text='Contatto'")
tab_dettaglio.det_2.dw_2.modify("st_label_clien_for_dep_2.text='Contatto:'")
tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_imballo", ls_cod_imballo)
tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_vettore", ls_cod_vettore)
tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_inoltro", ls_cod_inoltro)
tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_mezzo", ls_cod_mezzo)
tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_porto", ls_cod_porto)
tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_resa", ls_cod_resa)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_des_cliente", ls_null)

if sqlca.sqlcode <> 0 then
	g_mb.error("Errore nella ricerca dei dettagli del contatto selezionato.", sqlca)
	return
end if
			
if ldt_data_blocco < ldt_data_registrazione and ls_flag_blocco = "S" then
	g_mb.warning("Attenzione: il contatto risulta bloccato, probabilmente è collegato ad un cliente!")
	tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_contatto", ls_null)
	tab_dettaglio.det_1.dw_1.SetColumn ("cod_contatto")
	return
end if
end subroutine

public subroutine wf_flag_cliente_contatto (string as_flag_tipo);/**
 * stefanop
 * 09/02/2012
 *
 * Abilito la visualizzazione del campo cliente / contatto in base la radio button
 **/

string ls_modify, ls_null
dwitemstatus ldw_status

setnull(ls_null)
if as_flag_tipo = "S" then
	tab_dettaglio.det_1.dw_1.object.b_ricerca_cliente.visible = true
	tab_dettaglio.det_1.dw_1.object.b_ricerca_contatto.visible = false
	
	ls_modify = "st_cliente_fornitore.text='Cliente:'~t"
	ls_modify += "cod_cliente.visible='1'~t"
	ls_modify += "cf_cod_cliente.visible='1'~t"
	ls_modify += "cod_contatto.visible='0'~t"
	ls_modify += "cf_cod_contatto.visible='0'~t"
	
	tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_contatto", ls_null)
else 
	tab_dettaglio.det_1.dw_1.object.b_ricerca_cliente.visible = false
	tab_dettaglio.det_1.dw_1.object.b_ricerca_contatto.visible = true
	
	ls_modify = "st_cliente_fornitore.text='Contatto:'~t"
	ls_modify += "cod_cliente.visible='0'~t"
	ls_modify += "cf_cod_cliente.visible='0'~t"
	ls_modify += "cod_contatto.visible='1'~t"
	ls_modify += "cf_cod_contatto.visible='1'~t"
	
	tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_cliente", ls_null)
end if

tab_dettaglio.det_1.dw_1.modify(ls_modify)

end subroutine

public subroutine wf_reset_tipo_bolla ();string ls_modify

tab_dettaglio.det_1.dw_1.object.b_ricerca_cliente.visible = false
tab_dettaglio.det_1.dw_1.object.b_ricerca_banca_for.visible = false
tab_dettaglio.det_1.dw_1.object.b_ricerca_fornitore.visible = false
tab_dettaglio.det_1.dw_1.object.b_ricerca_contatto.visible = false

ls_modify = "st_cliente_fornitore.text=''~t"
ls_modify = ls_modify + "cod_cliente.visible='0'~t"
ls_modify = ls_modify + "cod_fornitore.visible='0'~t"
ls_modify = ls_modify + "cod_des_cliente_t.visible='0'~t"
ls_modify = ls_modify + "cf_cod_des_cliente.visible='0'~t"
ls_modify = ls_modify + "cod_des_cliente.visible='0'~t"
ls_modify = ls_modify + "cf_cod_des_fornitore.visible='0'~t"
ls_modify = ls_modify + "cod_des_fornitore.visible='0'~t"
ls_modify = ls_modify + "cod_fil_fornitore_t.visible='0'~t"
ls_modify = ls_modify + "cf_cod_fil_fornitore.visible='0'~t"
ls_modify = ls_modify + "cod_fil_fornitore.visible='0'~t"
ls_modify = ls_modify + "cod_deposito_tras_t.visible='0'~t"
ls_modify = ls_modify + "cf_cod_deposito_tras.visible='0'~t"
ls_modify = ls_modify + "cod_deposito_tras.visible='0'~t"
ls_modify = ls_modify + "cod_contatto.visible='0'~t"
ls_modify = ls_modify + "cf_cod_contatto.visible='0'~t"
ls_modify = ls_modify + "flag_cliente_contatto.visible='0'~t"
tab_dettaglio.det_1.dw_1.modify(ls_modify)

end subroutine

public subroutine wf_tipo_bolla (string as_flag_tipo_bol_ven, boolean ab_rowfocuschange);string ls_modify, ls_null
long ll_riga

setnull(ls_null)
ll_riga = tab_dettaglio.det_1.dw_1.getrow()
wf_abilita_pulsanti_ricerca(true)
tab_dettaglio.det_1.dw_1.object.b_ricerca_contatto.visible = false
tab_dettaglio.det_1.dw_1.object.b_ricerca_cliente.visible = false
tab_dettaglio.det_1.dw_1.object.b_ricerca_fornitore.visible = false

choose case as_flag_tipo_bol_ven
		
	case "V"			// bolla vendita
		
		tab_dettaglio.det_1.dw_1.object.b_ricerca_banca_for.enabled = true
		tab_dettaglio.det_1.dw_1.object.b_ricerca_cliente.visible = true
		
		ls_modify = "st_cliente_fornitore.text='Cliente:'~t"
		ls_modify = ls_modify + "cf_cod_cliente.visible='1'~t"
		ls_modify = ls_modify + "cod_cliente.visible='1'~t"
		ls_modify = ls_modify + "cf_cod_fornitore.visible='0'~t"
		ls_modify = ls_modify + "cod_fornitore.visible='0'~t"
		ls_modify = ls_modify + "cod_des_cliente_t.visible='1'~t"
		ls_modify = ls_modify + "cf_cod_des_cliente.visible='1'~t"
		ls_modify = ls_modify + "cod_des_cliente.visible='1'~t"
		ls_modify = ls_modify + "cf_cod_des_fornitore.visible='0'~t"
		ls_modify = ls_modify + "cod_des_fornitore.visible='0'~t"
		ls_modify = ls_modify + "cod_fil_fornitore_t.visible='0'~t"
		ls_modify = ls_modify + "cf_cod_fil_fornitore.visible='0'~t"
		ls_modify = ls_modify + "cod_fil_fornitore.visible='0'~t"
		ls_modify = ls_modify + "cod_deposito_tras_t.visible='0'~t"
		ls_modify = ls_modify + "cf_cod_deposito_tras.visible='0'~t"
		ls_modify = ls_modify + "cod_deposito_tras.visible='0'~t"
		ls_modify = ls_modify + "flag_cliente_contatto.visible='1'~t" // stefanop 09/02/2012
		ls_modify = ls_modify + "cod_contatto.visible='0'~t"
		ls_modify = ls_modify + "cf_cod_contatto.visible='0'~t"
		
		tab_dettaglio.det_1.dw_1.modify(ls_modify)
		
		if not ab_rowfocuschange then
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_cliente", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_fornitore", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_des_cliente", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_des_fornitore", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_fil_fornitore", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_deposito_tras", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_contatto", ls_null)
		end if
			
	case "R"			// bolla di reso
		
		tab_dettaglio.det_1.dw_1.object.b_ricerca_banca_for.enabled = true
		tab_dettaglio.det_1.dw_1.object.b_ricerca_fornitore.visible = true
	
		ls_modify = "st_cliente_fornitore.text='Fornitore:'~t"
		ls_modify = ls_modify + "cf_cod_cliente.visible='0'~t"
		ls_modify = ls_modify + "cod_cliente.visible='0'~t"
		ls_modify = ls_modify + "cf_cod_fornitore.visible='1'~t"
		ls_modify = ls_modify + "cod_fornitore.visible='1'~t"
		ls_modify = ls_modify + "cod_des_cliente_t.visible='1'~t"
		ls_modify = ls_modify + "cf_cod_des_cliente.visible='0'~t"
		ls_modify = ls_modify + "cod_des_cliente.visible='0'~t"
		ls_modify = ls_modify + "cf_cod_des_fornitore.visible='1'~t"
		ls_modify = ls_modify + "cod_des_fornitore.visible='1'~t"
		ls_modify = ls_modify + "cod_fil_fornitore_t.visible='1'~t"
		ls_modify = ls_modify + "cf_cod_fil_fornitore.visible='1'~t"
		ls_modify = ls_modify + "cod_fil_fornitore.visible='1'~t"
		ls_modify = ls_modify + "cod_deposito_tras_t.visible='0'~t"
		ls_modify = ls_modify + "cf_cod_deposito_tras.visible='0'~t"
		ls_modify = ls_modify + "cod_deposito_tras.visible='0'~t"
		ls_modify = ls_modify + "flag_cliente_contatto.visible='0'~t" // stefanop 09/02/2012
		ls_modify = ls_modify + "cod_contatto.visible='0'~t"
		ls_modify = ls_modify + "cf_cod_contatto.visible='0'~t"
		
		tab_dettaglio.det_1.dw_1.modify(ls_modify)
		if not ab_rowfocuschange then
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_cliente", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_fornitore", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_des_cliente", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_des_fornitore", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_fil_fornitore", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_deposito_tras", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_contatto", ls_null)
		end if
		
	case "T"				// bolla di trasferimento
		
		tab_dettaglio.det_1.dw_1.object.b_ricerca_banca_for.enabled = true
		tab_dettaglio.det_1.dw_1.object.b_ricerca_cliente.visible = true
	
		ls_modify = "st_cliente_fornitore.text='Cliente:'~t"
		ls_modify = ls_modify + "cf_cod_cliente.visible='1'~t"
		ls_modify = ls_modify + "cod_cliente.visible='1'~t"
		ls_modify = ls_modify + "cf_cod_fornitore.visible='0'~t"
		ls_modify = ls_modify + "cod_fornitore.visible='0'~t"
		ls_modify = ls_modify + "cod_des_cliente_t.visible='1'~t"
		ls_modify = ls_modify + "cf_cod_des_cliente.visible='1'~t"
		ls_modify = ls_modify + "cod_des_cliente.visible='1'~t"
		ls_modify = ls_modify + "cf_cod_des_fornitore.visible='0'~t"
		ls_modify = ls_modify + "cod_des_fornitore.visible='0'~t"
		ls_modify = ls_modify + "cod_fil_fornitore_t.visible='0'~t"
		ls_modify = ls_modify + "cf_cod_fil_fornitore.visible='0'~t"
		ls_modify = ls_modify + "cod_fil_fornitore.visible='0'~t"
		ls_modify = ls_modify + "cod_deposito_tras_t.visible='1'~t"
		ls_modify = ls_modify + "cf_cod_deposito_tras.visible='1'~t"
		ls_modify = ls_modify + "cod_deposito_tras.visible='1'~t"
		ls_modify = ls_modify + "flag_cliente_contatto.visible='0'~t" // stefanop 09/02/2012
		ls_modify = ls_modify + "cod_contatto.visible='0'~t"
		ls_modify = ls_modify + "cf_cod_contatto.visible='0'~t"
		
		tab_dettaglio.det_1.dw_1.modify(ls_modify)
		if not ab_rowfocuschange then
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_cliente", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_fornitore", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_des_cliente", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_des_fornitore", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_fil_fornitore", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_deposito_tras", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_contatto", ls_null)
		end if
		
	case "C"				// bolla di c/lavorazione
		
		tab_dettaglio.det_1.dw_1.object.b_ricerca_banca_for.enabled = true
		tab_dettaglio.det_1.dw_1.object.b_ricerca_fornitore.visible = true
	
		ls_modify = "st_cliente_fornitore.text='Fornitore:'~t"
		ls_modify = ls_modify + "cf_cod_cliente.visible='0'~t"
		ls_modify = ls_modify + "cod_cliente.visible='0'~t"
		ls_modify = ls_modify + "cf_cod_fornitore.visible='1'~t"
		ls_modify = ls_modify + "cod_fornitore.visible='1'~t"
		ls_modify = ls_modify + "cod_des_cliente_t.visible='1'~t"
		ls_modify = ls_modify + "cf_cod_des_cliente.visible='0'~t"
		ls_modify = ls_modify + "cod_des_cliente.visible='0'~t"
		ls_modify = ls_modify + "cf_cod_des_fornitore.visible='1'~t"
		ls_modify = ls_modify + "cod_des_fornitore.visible='1'~t"
		ls_modify = ls_modify + "cod_fil_fornitore_t.visible='1'~t"
		ls_modify = ls_modify + "cf_cod_fil_fornitore.visible='1'~t"
		ls_modify = ls_modify + "cod_fil_fornitore.visible='1'~t"
		ls_modify = ls_modify + "cod_deposito_tras_t.visible='1'~t"
		ls_modify = ls_modify + "cf_cod_deposito_tras.visible='1'~t"
		ls_modify = ls_modify + "cod_deposito_tras.visible='1'~t"
		ls_modify = ls_modify + "flag_cliente_contatto.visible='0'~t" // stefanop 09/02/2012
		ls_modify = ls_modify + "cod_contatto.visible='0'~t"
		ls_modify = ls_modify + "cf_cod_contatto.visible='0'~t"
		
		tab_dettaglio.det_1.dw_1.modify(ls_modify)
		if not ab_rowfocuschange then
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_cliente", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_fornitore", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_des_cliente", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_des_fornitore", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_fil_fornitore", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_deposito_tras", ls_null)
			tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_contatto", ls_null)
		end if
		
	case else
		wf_reset_tipo_bolla()
		
end choose

if not ab_rowfocuschange then
	tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_deposito", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_valuta", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_tipo_listino_prodotto", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_pagamento", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_riga, "sconto", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_agente_1", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_agente_2", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_banca_clien_for", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_riga, "flag_fuori_fido", 'N')
	tab_dettaglio.det_1.dw_1.setitem(ll_riga, "flag_riep_fat", 'N')
	tab_dettaglio.det_2.dw_2.modify("st_label_clien_for_dep_1.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_label_clien_for_dep_2.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_cod_clien_for_dep.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_cod_contatto.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_frazione.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_cap.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_localita.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_provincia.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_telefono.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_fax.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_telex.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_partita_iva.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_cod_fiscale.text=''")
	tab_dettaglio.det_2.dw_2.setitem(ll_riga, "rag_soc_1", ls_null)
	tab_dettaglio.det_2.dw_2.setitem(ll_riga, "rag_soc_2", ls_null)
	tab_dettaglio.det_2.dw_2.setitem(ll_riga, "indirizzo", ls_null)
	tab_dettaglio.det_2.dw_2.setitem(ll_riga, "frazione", ls_null)
	tab_dettaglio.det_2.dw_2.setitem(ll_riga, "cap", ls_null)
	tab_dettaglio.det_2.dw_2.setitem(ll_riga, "localita", ls_null)
	tab_dettaglio.det_2.dw_2.setitem(ll_riga, "provincia", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_riga, "cod_imballo", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_riga, "cod_vettore", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_riga, "cod_inoltro", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_riga, "cod_mezzo", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_riga, "cod_porto", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_riga, "cod_resa", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_riga, "cod_contatto", ls_null)
end if
end subroutine

public function long wf_inserisci_prodotti (long al_handle);string ls_sql, ls_label, ls_error, ls_cod_prodotto, ls_des_prodotto
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_prodotti.cod_prodotto, anag_prodotti.des_prodotto FROM tes_bol_ven " + is_sql_prodotto + " &
LEFT OUTER JOIN anag_prodotti ON anag_prodotti.cod_azienda = tes_bol_ven.cod_azienda AND &
anag_prodotti.cod_prodotto = det_bol_ven.cod_prodotto &
WHERE tes_bol_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_prodotto = ids_store.getitemstring(ll_i, 1)
	 ls_des_prodotto = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "X"
	lstr_data.codice = ls_cod_prodotto
	
	if isnull(ls_cod_prodotto) and isnull(ls_des_prodotto) then
		ls_label = "<Prodotto mancante>"
	elseif isnull(ls_des_prodotto) then
		ls_label = ls_cod_prodotto
	else
		ls_label = ls_cod_prodotto + " - " + ls_des_prodotto
	end if

	ltvi_item = wf_new_item(true, ICONA_PRODOTTO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_depositi (long al_handle);string ls_sql, ls_label, ls_cod_deposito, ls_des_deposito, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_depositi.cod_deposito, anag_depositi.des_deposito FROM tes_bol_ven &
LEFT OUTER JOIN anag_depositi ON anag_depositi.cod_azienda = tes_bol_ven.cod_azienda AND &
anag_depositi.cod_deposito = tes_bol_ven.cod_deposito " + is_sql_prodotto + " &
WHERE tes_bol_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_deposito = ids_store.getitemstring(ll_i, 1)
	 ls_des_deposito = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "D"
	lstr_data.codice = ls_cod_deposito
	
	if isnull(ls_cod_deposito) and isnull(ls_des_deposito) then
		ls_label = "<Deposito mancante>"
	elseif isnull(ls_des_deposito) then
		ls_label = ls_cod_deposito
	else
		ls_label = ls_cod_deposito + " - " + ls_des_deposito
	end if

	ltvi_item = wf_new_item(true, ICONA_DEPOSITO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_leggi_parent (long al_handle, ref string as_sql);long	ll_item
treeviewitem ltv_item
str_treeview lstr_data

if al_handle = 0 then return 0

do
	
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
		
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello
			
		case "C"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_bol_ven.cod_cliente is null "
			else
				as_sql += " AND tes_bol_ven.cod_cliente = '" + lstr_data.codice + "' "
			end if
			
		case "F"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_bol_ven.cod_fornitore is null "
			else
				as_sql += " AND tes_bol_ven.cod_fornitore = '" + lstr_data.codice + "' "
			end if
			
		case "B"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_bol_ven.cod_contatto is null "
			else
				as_sql += " AND tes_bol_ven.cod_contatto = '" + lstr_data.codice + "' "
			end if

		case "T"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_bol_ven.cod_tipo_bol_ven is null "
			else
				as_sql += " AND tes_bol_ven.cod_tipo_bol_ven = '" + lstr_data.codice + "' "
			end if
			
		case "E"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_bol_ven.cod_causale is null "
			else
				as_sql += " AND tes_bol_ven.cod_causale = '" + lstr_data.codice + "' "
			end if
			
		case "P"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_bol_ven.cod_pagamento is null "
			else
				as_sql += " AND tes_bol_ven.cod_pagamento = '" + lstr_data.codice + "' "
			end if
						
		case "D"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_bol_ven.cod_deposito is null "
			else
				as_sql += " AND tes_bol_ven.cod_deposito = '" + lstr_data.codice + "' "
			end if
					
		case "X"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND det_bol_ven.cod_prodotto is null "
			else
				as_sql += " AND det_bol_ven.cod_prodotto = '" + lstr_data.codice + "' "
			end if
			
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
end function

public subroutine wf_treeview_icons ();ICONA_BOLLA = wf_treeview_add_icon("treeview\documento_grigio.png")
ICONA_BOLLA_CONFERMATA =  wf_treeview_add_icon("treeview\documento_verde.png")
ICONA_BOLLA_PARZIALE =  wf_treeview_add_icon("treeview\documento_blu.png")
ICONA_DEPOSITO = wf_treeview_add_icon("treeview\deposito.png")
ICONA_PRODOTTO = wf_treeview_add_icon("treeview\prodotto.png")

ICONA_PAGAMENTO = wf_treeview_add_icon("treeview\offerta.png")
ICONA_CAUSALE_TRASP = wf_treeview_add_icon("treeview\reparto.png")
ICONA_TIPO_BOLLA = wf_treeview_add_icon("treeview\cartella.png")
ICONA_CLIENTE = wf_treeview_add_icon("treeview\cliente.png")
ICONA_FORNITORE = wf_treeview_add_icon("treeview\operatore.png")
ICONA_CONTATTO = wf_treeview_add_icon("treeview\operatore.png")
ICONA_NEW = wf_treeview_add_icon("treeview\documento_new.png")


end subroutine

public function long wf_inserisci_pagamenti (long al_handle);string ls_sql, ls_label, ls_cod_pagamento, ls_des_pagamento, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT tab_pagamenti.cod_pagamento, tab_pagamenti.des_pagamento FROM tes_bol_ven &
LEFT OUTER JOIN tab_pagamenti ON tab_pagamenti.cod_azienda = tes_bol_ven.cod_azienda AND &
tab_pagamenti.cod_pagamento = tes_bol_ven.cod_pagamento " + is_sql_prodotto + " &
WHERE tes_bol_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_pagamento = ids_store.getitemstring(ll_i, 1)
	 ls_des_pagamento = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "P"
	lstr_data.codice = ls_cod_pagamento
	
	if isnull(ls_cod_pagamento) and isnull(ls_cod_pagamento) then
		ls_label = "<Pagamento mancante>"
	elseif isnull(ls_des_pagamento) then
		ls_label = ls_cod_pagamento
	else
		ls_label = ls_cod_pagamento + " - " + ls_des_pagamento
	end if

	ltvi_item = wf_new_item(true, ICONA_PAGAMENTO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_causali_trasp (long al_handle);string ls_sql, ls_label, ls_cod_causale, ls_des_causale, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT tab_causali_trasp.cod_causale, tab_causali_trasp.des_causale FROM tes_bol_ven &
LEFT OUTER JOIN tab_causali_trasp ON tab_causali_trasp.cod_azienda = tes_bol_ven.cod_azienda AND &
tab_causali_trasp.cod_causale = tes_bol_ven.cod_causale " + is_sql_prodotto + " &
WHERE tes_bol_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_causale = ids_store.getitemstring(ll_i, 1)
	 ls_des_causale = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "E"
	lstr_data.codice = ls_cod_causale
	
	if isnull(ls_cod_causale) and isnull(ls_cod_causale) then
		ls_label = "<Causale trasporto mancante>"
	elseif isnull(ls_des_causale) then
		ls_label = ls_cod_causale
	else
		ls_label = ls_cod_causale + " - " + ls_des_causale
	end if

	ltvi_item = wf_new_item(true, ICONA_CAUSALE_TRASP)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_tipi_bolle (long al_handle);string ls_sql, ls_label, ls_cod_tipo_bol_ven, ls_des_tipo_bol_ven, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT tab_tipi_bol_ven.cod_tipo_bol_ven, tab_tipi_bol_ven.des_tipo_bol_ven FROM tes_bol_ven &
LEFT OUTER JOIN tab_tipi_bol_ven ON tab_tipi_bol_ven.cod_azienda = tes_bol_ven.cod_azienda AND &
tab_tipi_bol_ven.cod_tipo_bol_ven = tes_bol_ven.cod_tipo_bol_ven " + is_sql_prodotto + " &
WHERE tes_bol_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_tipo_bol_ven = ids_store.getitemstring(ll_i, 1)
	 ls_des_tipo_bol_ven = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "T"
	lstr_data.codice = ls_cod_tipo_bol_ven
	
	if isnull(ls_cod_tipo_bol_ven) then
		ls_label = "<Tipo bolla mancante>"
	elseif isnull(ls_des_tipo_bol_ven) then
		ls_label = ls_cod_tipo_bol_ven
	else
		ls_label = ls_cod_tipo_bol_ven + " - " + ls_des_tipo_bol_ven
	end if

	ltvi_item = wf_new_item(true, ICONA_TIPO_BOLLA)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_clienti (long al_handle);string ls_sql, ls_label, ls_cod_cliente, ls_rag_soc_1, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_clienti.cod_cliente, anag_clienti.rag_soc_1 FROM tes_bol_ven &
LEFT OUTER JOIN anag_clienti ON anag_clienti.cod_azienda = tes_bol_ven.cod_azienda AND &
anag_clienti.cod_cliente = tes_bol_ven.cod_cliente " + is_sql_prodotto + " &
WHERE tes_bol_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)


for ll_i = 1 to ll_rows
	
	 ls_cod_cliente = ids_store.getitemstring(ll_i, 1)
	 ls_rag_soc_1 = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "C"
	lstr_data.codice = ls_cod_cliente
	
	if isnull(ls_cod_cliente) then
		ls_label = "<Cliente mancante>"
	elseif isnull(ls_rag_soc_1) then
		ls_label = ls_cod_cliente
	else
		ls_label = ls_cod_cliente + " - " + ls_rag_soc_1
	end if

	ltvi_item = wf_new_item(true, ICONA_CLIENTE)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function long wf_inserisci_contatti (long al_handle);string ls_sql, ls_label, ls_cod_contatto, ls_rag_soc_1, ls_error
long ll_rows, ll_i
treeviewitem ltvi_item


ls_sql = "SELECT DISTINCT anag_contatti.cod_contatto, anag_contatti.rag_soc_1 FROM tes_bol_ven &
LEFT OUTER JOIN anag_contatti ON anag_contatti.cod_azienda = tes_bol_ven.cod_azienda AND &
anag_contatti.cod_contatto = tes_bol_ven.cod_contatto " + is_sql_prodotto + " &
WHERE tes_bol_ven.cod_azienda='" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows
	
	 ls_cod_contatto = ids_store.getitemstring(ll_i, 1)
	 ls_rag_soc_1 = ids_store.getitemstring(ll_i, 2)
	
	str_treeview lstr_data
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "B"
	lstr_data.codice = ls_cod_contatto
	
	if isnull(ls_cod_contatto) and isnull(ls_rag_soc_1) then
		ls_label = "<Contatto mancante>"
	elseif isnull(ls_rag_soc_1) then
		ls_label = ls_cod_contatto
	else
		ls_label = ls_cod_contatto + " - " + ls_rag_soc_1
	end if

	ltvi_item = wf_new_item(true, ICONA_CONTATTO)
	
	ltvi_item.data = lstr_data
	ltvi_item.label = ls_label
	
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
next

return ll_rows
end function

public function integer wf_carica_cliente (string as_cod_cliente, boolean ab_set_all_data);string ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cap, ls_localita, ls_provincia, ls_telefono, ls_fax, ls_telex, ls_partita_iva, &
		ls_cod_fiscale, ls_cod_deposito, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_valuta,ls_cod_agente_1, ls_cod_agente_2, &
		ls_cod_banca_clien_for, ls_cod_banca, ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_poroto, ls_cod_resa, &
		ls_cod_porto, ls_flag_fuori_fido, ls_flag_riep_fatt, ls_flag_blocco, ls_flag_bol_fat, ls_null, ls_flag_dest_merce_fat, ls_messaggio, &
		ls_nota_testata, ls_nota_piede, ls_nota_video
decimal ld_sconto
datetime ldt_data_registrazione, ldt_data_blocco
integer li_ret


setnull(ls_null)
ldt_data_registrazione = tab_dettaglio.det_1.dw_1.getitemdatetime(tab_dettaglio.det_1.dw_1.getrow(), "data_registrazione")	

li_ret = iuo_fido.uof_get_blocco_cliente( as_cod_cliente,&
													ldt_data_registrazione, is_cod_parametro_blocco, ls_messaggio)
if li_ret=2 then
	//blocco
	g_mb.error(ls_messaggio)
	return 1
elseif li_ret=1 then
	//solo warning
	g_mb.warning(ls_messaggio)
end if



select anag_clienti.rag_soc_1,
		anag_clienti.rag_soc_2,
		anag_clienti.indirizzo,
		anag_clienti.frazione,
		anag_clienti.cap,
		anag_clienti.localita,
		anag_clienti.provincia,
		anag_clienti.telefono,
		anag_clienti.fax,
		anag_clienti.telex,
		anag_clienti.partita_iva,
		anag_clienti.cod_fiscale,
		anag_clienti.cod_deposito,
		anag_clienti.cod_valuta,
		anag_clienti.cod_tipo_listino_prodotto,
		anag_clienti.cod_pagamento,
		anag_clienti.sconto,
		anag_clienti.cod_agente_1,
		anag_clienti.cod_agente_2,
		anag_clienti.cod_banca_clien_for,
		anag_clienti.cod_banca,
		anag_clienti.cod_imballo,
		anag_clienti.cod_vettore,
		anag_clienti.cod_inoltro,
		anag_clienti.cod_mezzo,
		anag_clienti.cod_porto,
		anag_clienti.cod_resa,
		anag_clienti.flag_fuori_fido,
		anag_clienti.flag_riep_fatt,
		anag_clienti.flag_blocco,
		anag_clienti.data_blocco,
		flag_bol_fat
into   :ls_rag_soc_1,    
		:ls_rag_soc_2,
		:ls_indirizzo,    
		:ls_frazione,
		:ls_cap,
		:ls_localita,
		:ls_provincia,
		:ls_telefono,
		:ls_fax,
		:ls_telex,
		:ls_partita_iva,
		:ls_cod_fiscale,
		:ls_cod_deposito,
		:ls_cod_valuta,
		:ls_cod_tipo_listino_prodotto,
		:ls_cod_pagamento,
		:ld_sconto,
		:ls_cod_agente_1,
		:ls_cod_agente_2,
		:ls_cod_banca_clien_for,
		:ls_cod_banca,
		:ls_cod_imballo,
		:ls_cod_vettore,
		:ls_cod_inoltro,
		:ls_cod_mezzo,
		:ls_cod_porto,
		:ls_cod_resa,
		:ls_flag_fuori_fido,
		:ls_flag_riep_fatt,
		:ls_flag_blocco,
		:ldt_data_blocco,
		:ls_flag_bol_fat
from anag_clienti
where anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
		anag_clienti.cod_cliente = :as_cod_cliente;
		
		
if isnull(as_cod_cliente) then as_cod_cliente = ''
if isnull(ls_rag_soc_1) then ls_rag_soc_1 = ''
if isnull(ls_rag_soc_2) then ls_rag_soc_2 = ''
if isnull(ls_indirizzo) then ls_indirizzo = ''
if isnull(ls_frazione) then ls_frazione = ''
if isnull(ls_cap) then ls_cap = ''
if isnull(ls_localita) then ls_localita = ''
if isnull(ls_provincia) then ls_provincia = ''
if isnull(ls_telefono) then ls_telefono = ''
if isnull(ls_fax) then ls_fax = ''
if isnull(ls_telex) then ls_telex = ''
if isnull(ls_partita_iva) then ls_partita_iva = ''
if isnull(ls_cod_fiscale) then ls_cod_fiscale = ''
 
tab_dettaglio.det_2.dw_2.modify("st_label_clien_for_dep_1.text='Cliente'")
tab_dettaglio.det_2.dw_2.modify("st_label_clien_for_dep_2.text='Cliente:'")
tab_dettaglio.det_2.dw_2.modify("st_cod_clien_for_dep.text='" + as_cod_cliente + "'")
tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
tab_dettaglio.det_2.dw_2.modify("st_frazione.text='" + ls_frazione + "'")
tab_dettaglio.det_2.dw_2.modify("st_cap.text='" + ls_cap + "'")
tab_dettaglio.det_2.dw_2.modify("st_localita.text='" + ls_localita + "'")
tab_dettaglio.det_2.dw_2.modify("st_provincia.text='" + ls_provincia + "'")
tab_dettaglio.det_2.dw_2.modify("st_telefono.text='" + ls_telefono + "'")
tab_dettaglio.det_2.dw_2.modify("st_fax.text='" + ls_fax + "'")
tab_dettaglio.det_2.dw_2.modify("st_telex.text='" + ls_telex + "'")
tab_dettaglio.det_2.dw_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
tab_dettaglio.det_2.dw_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
   
ls_flag_dest_merce_fat = "S"
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_des_cliente", &
	sqlca, &
	"anag_des_clienti", &
	"cod_des_cliente", &
	"rag_soc_1", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + as_cod_cliente + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_dest_merce_fat = '" + ls_flag_dest_merce_fat + "'")
 		
if not ab_set_all_data then return -1

f_cambio_ven(ls_cod_valuta, ldt_data_registrazione)

if ls_flag_bol_fat = "F" then
	g_mb.warning("ATTENZIONE: il documento preferenziale per questo cliente è FATTURA")
end if

ldt_data_registrazione = tab_dettaglio.det_1.dw_1.getitemdatetime(tab_dettaglio.det_1.dw_1.getrow(), "data_registrazione")
if guo_functions.uof_get_note_fisse(as_cod_cliente, ls_null, ls_null, "BOL_VEN", ls_null, ldt_data_registrazione,  ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
	g_mb.error(ls_nota_testata)
else
	if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
end if

if f_cerca_destinazione(tab_dettaglio.det_2.dw_2, tab_dettaglio.det_1.dw_1.getrow(), as_cod_cliente, "C", ls_null, ls_messaggio) = -1 then
	g_mb.messagebox("APICE","Errore in ricerca destinazione diversa. ~r~nDettaglio: " +ls_messaggio)
end if

if ldt_data_blocco < ldt_data_registrazione and ls_flag_blocco = "S" then
	g_mb.warning("Attenzione cliente bloccato!")
	tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_cliente", ls_null)
	tab_dettaglio.det_1.dw_1.SetColumn ("cod_cliente")
	return 1
end if

tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_deposito", ls_cod_deposito)
tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_valuta", ls_cod_valuta)
tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_pagamento", ls_cod_pagamento)
tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "sconto", ld_sconto)
tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_agente_1", ls_cod_agente_1)
tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_agente_2", ls_cod_agente_2)
tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_banca_clien_for", ls_cod_banca_clien_for)
tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_banca", ls_cod_banca)
tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "flag_fuori_fido", ls_flag_fuori_fido)
tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "flag_riep_fat", ls_flag_riep_fatt)
tab_dettaglio.det_1.dw_1.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_des_cliente", ls_null)
tab_dettaglio.det_3.dw_3.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_imballo", ls_cod_imballo)
tab_dettaglio.det_3.dw_3.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_vettore", ls_cod_vettore)
tab_dettaglio.det_3.dw_3.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_inoltro", ls_cod_inoltro)
tab_dettaglio.det_3.dw_3.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_mezzo", ls_cod_mezzo)
tab_dettaglio.det_3.dw_3.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_porto", ls_cod_porto)
tab_dettaglio.det_3.dw_3.setitem(tab_dettaglio.det_1.dw_1.getrow(), "cod_resa", ls_cod_resa)
end function

public function integer wf_carica_fornitore (string as_cod_fornitore, boolean ab_set_all_data);string ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cap, ls_localita, ls_provincia, ls_telefono, ls_fax, ls_telex, ls_partita_iva, &
		ls_cod_fiscale, ls_cod_deposito, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for,ls_cod_banca, &
		ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, ls_cod_resa, ls_flag_terzista, ls_null, ls_cod_tipo_bol_ven, &
		ls_flag_tipo_bol_ven
decimal ld_sconto
datetime ldt_data_registrazione
long ll_row 

setnull(ls_null)

ll_row = tab_dettaglio.det_3.dw_3.getrow()

select anag_fornitori.rag_soc_1,
		anag_fornitori.rag_soc_2,
		anag_fornitori.indirizzo,
		anag_fornitori.frazione,
		anag_fornitori.cap,
		anag_fornitori.localita,
		anag_fornitori.provincia,
		anag_fornitori.telefono,
		anag_fornitori.fax,
		anag_fornitori.telex,
		anag_fornitori.partita_iva,
		anag_fornitori.cod_fiscale,
		anag_fornitori.cod_deposito,
		anag_fornitori.cod_valuta,
		anag_fornitori.cod_tipo_listino_prodotto,
		anag_fornitori.cod_pagamento,
		anag_fornitori.sconto,
		anag_fornitori.cod_banca_clien_for,
		anag_fornitori.cod_banca,
		anag_fornitori.cod_imballo,
		anag_fornitori.cod_vettore,
		anag_fornitori.cod_inoltro,
		anag_fornitori.cod_mezzo,
		anag_fornitori.cod_porto,
		anag_fornitori.cod_resa,
		anag_fornitori.flag_terzista
into   :ls_rag_soc_1,    
		:ls_rag_soc_2,
		:ls_indirizzo,    
		:ls_frazione,
		:ls_cap,
		:ls_localita,
		:ls_provincia,
		:ls_telefono,
		:ls_fax,
		:ls_telex,
		:ls_partita_iva,
		:ls_cod_fiscale,
		:ls_cod_deposito,
		:ls_cod_valuta,
		:ls_cod_tipo_listino_prodotto,
		:ls_cod_pagamento,
		:ld_sconto,
		:ls_cod_banca_clien_for,
		:ls_cod_banca,
		:ls_cod_imballo,
		:ls_cod_vettore,
		:ls_cod_inoltro,
		:ls_cod_mezzo,
		:ls_cod_porto,
		:ls_cod_resa,
		:ls_flag_terzista
from anag_fornitori
where anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
		 anag_fornitori.cod_fornitore = :as_cod_fornitore;

if sqlca.sqlcode = 0 then 	
	tab_dettaglio.det_2.dw_2.modify("st_label_clien_for_dep_1.text='Fornitore'")
	tab_dettaglio.det_2.dw_2.modify("st_label_clien_for_dep_2.text='Fornitore:'")
	
	if isnull(as_cod_fornitore) then as_cod_fornitore = ''
	if isnull(ls_rag_soc_1) then ls_rag_soc_1 = ''
	if isnull(ls_rag_soc_2) then ls_rag_soc_2 = ''
	if isnull(ls_indirizzo) then ls_indirizzo = ''
	if isnull(ls_frazione) then ls_frazione = ''
	if isnull(ls_cap) then ls_cap = ''
	if isnull(ls_localita) then ls_localita = ''
	if isnull(ls_provincia) then ls_provincia = ''
	if isnull(ls_telefono) then ls_telefono = ''
	if isnull(ls_fax) then ls_fax = ''
	if isnull(ls_telex) then ls_telex = ''
	if isnull(ls_partita_iva) then ls_partita_iva = ''
	if isnull(ls_cod_fiscale) then ls_cod_fiscale = ''
	
	tab_dettaglio.det_2.dw_2.modify("st_cod_clien_for_dep.text='" + as_cod_fornitore + "'")
	tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
	tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")	
	tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
	tab_dettaglio.det_2.dw_2.modify("st_frazione.text='" + ls_frazione + "'")
	tab_dettaglio.det_2.dw_2.modify("st_cap.text='" + ls_cap + "'")
	tab_dettaglio.det_2.dw_2.modify("st_localita.text='" + ls_localita + "'")
	tab_dettaglio.det_2.dw_2.modify("st_provincia.text='" + ls_provincia + "'")
	tab_dettaglio.det_2.dw_2.modify("st_telefono.text='" + ls_telefono + "'")
	tab_dettaglio.det_2.dw_2.modify("st_fax.text='" + ls_fax + "'")
	tab_dettaglio.det_2.dw_2.modify("st_telex.text='" + ls_telex + "'")
	tab_dettaglio.det_2.dw_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")	
	tab_dettaglio.det_2.dw_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
		
	f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
		"cod_des_fornitore", &
		sqlca, &
		"anag_des_fornitori", &
		"cod_des_fornitore", &
		"rag_soc_1", &
		"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + as_cod_fornitore + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		
	f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
		"cod_fil_fornitore", &
		sqlca, &
		"anag_fil_fornitori", &
		"cod_fil_fornitore", &
		"rag_soc_1", &
		"cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + as_cod_fornitore + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		
else
	tab_dettaglio.det_1.dw_1.setitem(1, "cod_deposito", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(1, "cod_valuta", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(1, "cod_tipo_listino_prodotto", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(1, "cod_pagamento", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(1, "sconto", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(1, "cod_banca_clien_for", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(1, "cod_banca", ls_null)
	tab_dettaglio.det_2.dw_2.modify("st_cod_clien_for_dep.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_frazione.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_cap.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_localita.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_provincia.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_telefono.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_fax.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_telex.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_partita_iva.text=''")
	tab_dettaglio.det_2.dw_2.modify("st_cod_fiscale.text=''")
	tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_imballo", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_vettore", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_inoltro", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_mezzo", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_porto", ls_null)
	tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_resa", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_des_fornitore", ls_null)
	tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_fil_fornitore", ls_null)
end if 

if not ab_set_all_data then return 1

tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_deposito", ls_cod_deposito)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_valuta", ls_cod_valuta)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_pagamento", ls_cod_pagamento)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "sconto", ld_sconto)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_banca_clien_for", ls_cod_banca_clien_for)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_banca", ls_cod_banca)
tab_dettaglio.det_1.dw_1.setitem(ll_row, "cod_fil_fornitore", ls_null)
tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_imballo", ls_cod_imballo)
tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_vettore", ls_cod_vettore)
tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_inoltro", ls_cod_inoltro)
tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_mezzo", ls_cod_mezzo)
tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_porto", ls_cod_porto)
tab_dettaglio.det_3.dw_3.setitem(ll_row, "cod_resa", ls_cod_resa)

ldt_data_registrazione = tab_dettaglio.det_1.dw_1.getitemdatetime(1, "data_registrazione")
f_cambio_ven(ls_cod_valuta, ldt_data_registrazione)
ls_cod_tipo_bol_ven = tab_dettaglio.det_1.dw_1.getitemstring(1,"cod_tipo_bol_ven")

if not isnull(ls_cod_tipo_bol_ven) then
	select flag_tipo_bol_ven
	into :ls_flag_tipo_bol_ven
	from tab_tipi_bol_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
			 
	if sqlca.sqlcode = 100 then
		g_mb.error("Errore in ricerca flag tipo bolla in tabella tipi bolle.", sqlca)
		return  -1
	end if
	
	if (ls_flag_tipo_bol_ven = 'T' or ls_flag_tipo_bol_ven = 'C') and ls_flag_terzista = 'N' then
		g_mb.error("Si sta tendando di fare una bolla di conto lavoro o trasferimeno ad un fornitore che non è terzista.~r~nQuesto potrebbe creare potenzialmente dei problemi nella gestione inventario di magazzino.")
	end if
end if

end function

public subroutine wf_carica_deposito_trasf (string as_cod_deposito);string ls_rag_soc_1, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_null
long ll_row

ll_row = tab_dettaglio.det_3.dw_3.getrow()

setnull(ls_null)

select anag_depositi.des_deposito,   
		anag_depositi.indirizzo,   
		anag_depositi.localita,   
		anag_depositi.frazione,   
		anag_depositi.cap,   
		anag_depositi.provincia  
into   :ls_rag_soc_1,   
		:ls_indirizzo,   
		:ls_localita,   
		:ls_frazione,   
		:ls_cap,   
		:ls_provincia  
from   anag_depositi
where anag_depositi.cod_azienda = :s_cs_xx.cod_azienda and 
		 anag_depositi.cod_deposito = :as_cod_deposito;
		 
if sqlca.sqlcode = 0 then
	tab_dettaglio.det_2.dw_2.setitem(ll_row, "rag_soc_1", ls_rag_soc_1)
	tab_dettaglio.det_2.dw_2.setitem(ll_row, "indirizzo", ls_indirizzo)
	tab_dettaglio.det_2.dw_2.setitem(ll_row, "frazione", ls_frazione)
	tab_dettaglio.det_2.dw_2.setitem(ll_row, "cap", ls_cap)
	tab_dettaglio.det_2.dw_2.setitem(ll_row, "localita", ls_localita)
	tab_dettaglio.det_2.dw_2.setitem(ll_row, "provincia", ls_provincia)
else
	tab_dettaglio.det_2.dw_2.setitem(ll_row, "rag_soc_1", ls_null)
	tab_dettaglio.det_2.dw_2.setitem(ll_row, "indirizzo", ls_null)
	tab_dettaglio.det_2.dw_2.setitem(ll_row, "frazione", ls_null)
	tab_dettaglio.det_2.dw_2.setitem(ll_row, "cap", ls_null)
	tab_dettaglio.det_2.dw_2.setitem(ll_row, "localita", ls_null)
	tab_dettaglio.det_2.dw_2.setitem(ll_row, "provincia", ls_null)
end if
end subroutine

public subroutine wf_carica_deposito (string as_cod_deposito);string ls_rag_soc_1, ls_indirizzo, ls_localita, ls_frazione, ls_cap, ls_provincia, ls_null, ls_telefono, ls_fax, ls_telex

setnull(ls_null)

select des_deposito,   
		indirizzo,   
		localita,   
		frazione,   
		cap,   
		provincia,
		telefono,
		fax,
		telex
into   :ls_rag_soc_1,   
		:ls_indirizzo,   
		:ls_localita,   
		:ls_frazione,   
		:ls_cap,   
		:ls_provincia,
		:ls_telefono,
		:ls_fax,
		:ls_telex
from anag_depositi
where cod_azienda = :s_cs_xx.cod_azienda and 
		cod_deposito = :as_cod_deposito;
		 

tab_dettaglio.det_2.dw_2.modify("st_cod_clien_for_dep.text='" + as_cod_deposito + "'")
tab_dettaglio.det_2.dw_2.modify("st_label_clien_for_dep_1.text='Deposito di Partenza'")
tab_dettaglio.det_2.dw_2.modify("st_label_clien_for_dep_2.text='Deposito:'")
tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text=''")
tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
tab_dettaglio.det_2.dw_2.modify("st_frazione.text='" + ls_frazione + "'")
tab_dettaglio.det_2.dw_2.modify("st_telefono.text='" + ls_telefono + "'")
tab_dettaglio.det_2.dw_2.modify("st_cap.text='" + ls_cap + "'")
tab_dettaglio.det_2.dw_2.modify("st_provincia.text='" + ls_provincia + "'")
tab_dettaglio.det_2.dw_2.modify("st_fax.text='" + ls_fax + "'")
tab_dettaglio.det_2.dw_2.modify("st_telex.text='" + ls_telex + "'")
tab_dettaglio.det_2.dw_2.modify("st_partita_iva.text=''")
tab_dettaglio.det_2.dw_2.modify("st_cod_fiscale.text=''")
end subroutine

public function integer wf_riapri_ordini (integer ai_anno_ddt, long al_num_ddt, ref string as_errore);
//questa funzione esegue l'eventuale riapertura riga ordine vendita alla cancellazione del ddt di vendita, dalla testata
//ma solo se NON si tratta di un ddt di tarsferimento (questo controllo è stato fatto prima di chiamare questa funzione)

string				ls_flag_evasione, ls_ordini_riaperti, ls_temp[], ls_riga_ordine
integer			li_anno_reg_ord_ven_cu
long				ll_prog_riga_bol_ven_cu, ll_num_reg_ord_ven_cu, ll_prog_riga_ord_ven_cu, ll_righe, ll_evase, ll_parziali
decimal			ld_quan_consegnata_cu, ld_quan_evasa


ls_ordini_riaperti = ""


//leggi le righe ddt legate ad una riga ordine di vendita ------------------------------------
declare cu_righe_ddt cursor for  
select prog_riga_bol_ven, anno_registrazione_ord_ven, num_registrazione_ord_ven, prog_riga_ord_ven, quan_consegnata
from det_bol_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:ai_anno_ddt and
			num_registrazione=:al_num_ddt and 
			anno_registrazione_ord_ven>0
order by prog_riga_bol_ven;

open cu_righe_ddt;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in OPEN cursore cu_righe_ddt : " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_righe_ddt into :ll_prog_riga_bol_ven_cu, :li_anno_reg_ord_ven_cu, :ll_num_reg_ord_ven_cu, :ll_prog_riga_ord_ven_cu,
								  :ld_quan_consegnata_cu;

	if sqlca.sqlcode < 0 then
		as_errore = "Errore in FETCH cursore cu_righe_ddt : " + sqlca.sqlerrtext
		close cu_righe_ddt;
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	//procedi
	
	if isnull(li_anno_reg_ord_ven_cu) or isnull(ll_num_reg_ord_ven_cu) or isnull(ll_prog_riga_ord_ven_cu) then
		continue
	end if
	
	ls_riga_ordine = string(li_anno_reg_ord_ven_cu)+"/"+string(ll_num_reg_ord_ven_cu)+"/"+string(ll_prog_riga_ord_ven_cu)
	
	//leggo quantita evasa dell'ordine
	select quan_evasa
	into   :ld_quan_evasa
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_reg_ord_ven_cu and
			 num_registrazione = :ll_num_reg_ord_ven_cu and
			 prog_riga_ord_ven = :ll_prog_riga_ord_ven_cu;
	
	if sqlca.sqlcode <> 0 then
		as_errore = "Errore in lettura quantità evasa riga ordine di origine ("+ls_riga_ordine+"): " + sqlca.sqlerrtext
		close cu_righe_ddt;
		return -1
	end if
	
	//NOTA: in alcuni casi può capitare di evadere una riga ordine con una quantità superiore a quella ordinata (es. Eurocablaggi)
	//allora per evitare problemi di valori negativi in ripsristino riga forzo la quan consegnata alla quantità evasa ...
	if ld_quan_consegnata_cu>ld_quan_evasa then ld_quan_consegnata_cu=ld_quan_evasa
	
	if ld_quan_evasa - ld_quan_consegnata_cu = 0 then
		//l'ordine va riaperto
		ls_flag_evasione = "A"
	else
		//l'ordine deve essere posto a parziale
		ls_flag_evasione = "P"
	end if
	
	update 	det_ord_ven
	set    		quan_evasa = quan_evasa - :ld_quan_consegnata_cu,
				flag_evasione = :ls_flag_evasione
	where  	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_reg_ord_ven_cu and
				num_registrazione = :ll_num_reg_ord_ven_cu and
				prog_riga_ord_ven = :ll_prog_riga_ord_ven_cu;
	
	if sqlca.sqlcode <> 0 then
		as_errore = "Errore in aggiornamento quantità evasa riga ordine di origine ("+ls_riga_ordine+"): " + sqlca.sqlerrtext
		close cu_righe_ddt;
		return -1
	end if
	
	select count(*)
	into   :ll_righe
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_reg_ord_ven_cu and
			 num_registrazione = :ll_num_reg_ord_ven_cu;
			 
	if sqlca.sqlcode <> 0 then
		as_errore = "Errore in conteggio righe ordine di origine ("+ls_riga_ordine+"): " + sqlca.sqlerrtext
		close cu_righe_ddt;
		return -1
	end if
	
	select count(*)
	into   :ll_evase
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_reg_ord_ven_cu and
			 num_registrazione = :ll_num_reg_ord_ven_cu and
			 flag_evasione = 'E';
			 
	if sqlca.sqlcode <> 0 then
		as_errore = "Errore in conteggio righe evase ordine di origine ("+ls_riga_ordine+"): " + sqlca.sqlerrtext
		close cu_righe_ddt;
		return 1
	end if
	
	select count(*)
	into   :ll_parziali
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_reg_ord_ven_cu and
			 num_registrazione = :ll_num_reg_ord_ven_cu and
			 flag_evasione = 'P';
			 
	if sqlca.sqlcode <> 0 then
		as_errore = "Errore in conteggio righe parziali ordine di origine ("+ls_riga_ordine+"): " + sqlca.sqlerrtext
		close cu_righe_ddt;
		return -1
	end if
	
	if ll_evase = ll_righe then
		ls_flag_evasione = "E"
	elseif ll_evase > 0 or ll_parziali > 0 then
		ls_flag_evasione = "P"
	else
		ls_flag_evasione = "A"	
	end if
	
	update 	tes_ord_ven
	set    		flag_evasione = :ls_flag_evasione
	where  	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_reg_ord_ven_cu and
				num_registrazione = :ll_num_reg_ord_ven_cu;

	if sqlca.sqlcode <> 0 then
		as_errore = "Errore in aggiornamento stato evasione ordine di origine ("+ls_riga_ordine+"): " + sqlca.sqlerrtext
		close cu_righe_ddt;
		return -1
	end if

	wf_lista_ordini_riaperti(string(li_anno_reg_ord_ven_cu)+"/"+string(ll_num_reg_ord_ven_cu), ls_temp[], ls_ordini_riaperti)

loop

close cu_righe_ddt;

//in as_errore metto la lista degli ordini riaperti
if ls_ordini_riaperti<>"" and not isnull(ls_ordini_riaperti) then as_errore = ls_ordini_riaperti

return 0
end function

public subroutine wf_lista_ordini_riaperti (string as_ordine, ref string as_ordini[], ref string as_testo);long			ll_index


for ll_index=1 to upperbound(as_ordini[])
	if as_ordine = as_ordini[ll_index] then
		//ordine già in lista
		return
	end if
next

//se arrivi fin qui aggiungi
 as_ordini[ upperbound(as_ordini[]) + 1] = as_ordine
 
 if as_testo<>"" then as_testo += ", "
 as_testo += as_ordine
 
 return
end subroutine

on w_tes_bol_ven.create
int iCurrent
call super::create
end on

on w_tes_bol_ven.destroy
call super::destroy
end on

event pc_delete;call super::pc_delete;string ls_tabella
long ll_anno_registrazione, ll_num_registrazione
integer li_i


for li_i = 1 to tab_dettaglio.det_1.dw_1.deletedcount()
	if 	tab_dettaglio.det_1.dw_1.getitemstring(li_i, "flag_movimenti", delete!, true) = "S" or &
		tab_dettaglio.det_1.dw_1.getitemstring(li_i, "flag_blocco", delete!, true) = "S"  	then
		
	   g_mb.warning("Bolla non cancellabile! E' già stata confermata.")
	   tab_dettaglio.det_1.dw_1.set_dw_view(c_ignorechanges)
	   pcca.error = c_fatal
	   return
	end if
	
next

wf_abilita_pulsanti_ricerca(false)
end event

event pc_modify;call super::pc_modify;if tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_movimenti") = "S" or & 
	tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_blocco") = "S" then
	
   g_mb.warning("Bolla non modificabile! E' già stata confermata.")
   tab_dettaglio.det_1.dw_1.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if
end event

event pc_setddlb;call super::pc_setddlb;string ls_select_operatori
if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + ")) and cod_operatore in (select cod_operatore from tab_operatori_utenti where cod_utente = '" + &
						  s_cs_xx.cod_utente + "')"
else
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + "))"
end if

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_operatore", &
	sqlca, &
	"tab_operatori", &
	"cod_operatore", &
	"des_operatore", &
	ls_select_operatori)
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_tipo_bol_ven", &
	sqlca, &
	"tab_tipi_bol_ven", &
	"cod_tipo_bol_ven", &
	"des_tipo_bol_ven", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_deposito", &
	sqlca, &
	"anag_depositi", &
	"cod_deposito", &
	"des_deposito", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_deposito_tras", &
	sqlca, &
	"anag_depositi", &
	"cod_deposito", &
	"des_deposito", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_valuta", &
	sqlca, &
	"tab_valute", &
	"cod_valuta", &
	"des_valuta", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_tipo_listino_prodotto", &
	sqlca, &
	"tab_tipi_listini_prodotti", &
	"cod_tipo_listino_prodotto", &
	"des_tipo_listino_prodotto", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_pagamento", &
	sqlca, &
	"tab_pagamenti", &
	"cod_pagamento", &
	"des_pagamento", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_agente_1", &
	sqlca, &
	"anag_agenti", &
	"cod_agente", &
	"rag_soc_1", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_agente_2", &
	sqlca, &
	"anag_agenti", &
	"cod_agente", &
	"rag_soc_1", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_banca", &
	sqlca, &
	"anag_banche", &
	"cod_banca", &
	"des_banca", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_banca_clien_for", &
	sqlca, &
	"anag_banche_clien_for", &
	"cod_banca_clien_for", &
	"des_banca", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_1, &
	"cod_documento", &
	sqlca, &
	"tab_documenti", &
	"cod_documento", &
	"des_documento", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")					  
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
	"cod_imballo", &
	sqlca, &
	"tab_imballi", &
	"cod_imballo", &
	"des_imballo", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
	"cod_vettore", &
	sqlca, &
	"anag_vettori", &
	"cod_vettore", &
	"rag_soc_1", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
	"cod_inoltro", &
	sqlca, &
	"anag_vettori", &
	"cod_vettore", &
	"rag_soc_1", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
	"cod_mezzo", &
	sqlca, &
	"tab_mezzi", &
	"cod_mezzo", &
	"des_mezzo", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
	"cod_natura_intra", &
	sqlca, &
	"tab_natura_intra", &
	"cod_natura_intra", &
	"descrizione", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
	"cod_porto", &
	sqlca, &
	"tab_porti", &
	"cod_porto", &
	"des_porto", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
	"cod_resa", &
	sqlca, &
	"tab_rese", &
	"cod_resa", &
	"des_resa", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")	  
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_3, &
	"cod_causale", &
	sqlca, &
	"tab_causali_trasp", &
	"cod_causale", &
	"des_causale", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
/* --- RICERCA --- */
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
	"cod_documento", &
	sqlca, &
	"tab_documenti", &
	"cod_documento", &
	"des_documento", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' AND cod_documento IN (select cod_documento from tab_tipi_bol_ven)" )
							  
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
	"cod_causale", &
	sqlca, &
	"tab_causali_trasp", &
	"cod_causale", &
	"des_causale", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
	"cod_vettore", &
	sqlca, &
	"anag_vettori", &
	"cod_vettore", &
	"rag_soc_1", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")							  

f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
	"cod_pagamento", &
	sqlca, &
	"tab_pagamenti", &
	"cod_pagamento", &
	"des_pagamento", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
	
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
	"cod_tipo_bol_ven", &
	sqlca, &
	"tab_tipi_bol_ven", &
	"cod_tipo_bol_ven", &
	"des_tipo_bol_ven", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
	
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
	"cod_deposito", &
	sqlca, &
	"anag_depositi", &
	"cod_deposito", &
	"des_deposito", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
			  
end event

event settoolbarpos;call super::settoolbarpos;s_cs_xx.parametri.parametro_s_1 = "Bol_Ven"
window_open_parm(w_acq_ven_corr, -1, tab_dettaglio.det_1.dw_1)
end event

event pc_setwindow;call super::pc_setwindow;
// posizione dw
tab_dettaglio.det_1.dw_1.move(0,0)
tab_dettaglio.det_2.dw_2.move(0,0)
tab_dettaglio.det_3.dw_3.move(0,0)
tab_dettaglio.det_4.dw_4.move(0,0)
// ----

is_codice_filtro = "BLV"

tab_dettaglio.det_1.dw_1.set_dw_key("cod_azienda")
tab_dettaglio.det_1.dw_1.set_dw_options(sqlca, i_openparm, c_scrollparent + c_noretrieveonopen, c_default)
tab_dettaglio.det_2.dw_2.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata + c_scrollparent, c_default)
tab_dettaglio.det_3.dw_3.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata + c_scrollparent, c_default)
tab_dettaglio.det_4.dw_4.set_dw_options(sqlca, tab_dettaglio.det_1.dw_1, c_sharedata + c_scrollparent, c_default)

tab_dettaglio.det_1.dw_1.ib_dw_detail = true
tab_dettaglio.det_2.dw_2.ib_dw_detail = true
tab_dettaglio.det_3.dw_3.ib_dw_detail = true
tab_dettaglio.det_4.dw_4.ib_dw_detail = true

iuo_dw_main = tab_dettaglio.det_1.dw_1

il_livello = 0

iuo_fido = create uo_fido_cliente


//Donato 04/06/2014 SR Migliorie_Spese_trasporto: Funzione 2
//attivazione funzione ricalcolo spese trasporto: disponibile solo se abilitate dal parametro aziendale relativo
guo_functions.uof_get_parametro_azienda("SSP", ib_spese_trasporto)
if isnull(ib_spese_trasporto) then ib_spese_trasporto = false
//--------------------------------------


//Gestione terzisti tramite scarico parziale ramo commessa interna
//Se SI allora è possibile mediante inserimento numero commessa e semilavorato recuperare le MP e metterle in dettaglio
//opzione disponibile solo per DDT conto lavoro
guo_functions.uof_get_parametro("SPT", ib_SPT)

end event

event close;call super::close;

destroy iuo_fido
end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_tes_bol_ven
integer width = 3291
integer height = 2160
det_2 det_2
det_3 det_3
det_4 det_4
end type

on tab_dettaglio.create
this.det_2=create det_2
this.det_3=create det_3
this.det_4=create det_4
call super::create
this.Control[]={this.det_1,&
this.det_2,&
this.det_3,&
this.det_4}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
destroy(this.det_3)
destroy(this.det_4)
end on

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
integer width = 3255
integer height = 2036
string text = "Testata"
dw_1 dw_1
end type

on det_1.create
this.dw_1=create dw_1
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
end on

on det_1.destroy
call super::destroy
destroy(this.dw_1)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_tes_bol_ven
integer height = 2648
end type

on tab_ricerca.create
call super::create
this.Control[]={this.ricerca,&
this.selezione}
end on

on tab_ricerca.destroy
call super::destroy
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
integer height = 2524
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
integer height = 2508
string dataobject = "d_tes_bol_ven_ricerca_tv"
end type

event dw_ricerca::buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_ricerca, "cod_cliente")
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_ricerca, "cod_fornitore")
		
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca, "cod_prodotto")
		
	case "b_ricerca_contatto"
		guo_ricerca.uof_ricerca_contatto(dw_ricerca, "cod_contatto")
		
end choose
end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
integer height = 2524
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
event pc_menu_corrisp ( )
integer height = 2496
integer textsize = -8
end type

event tv_selezione::doubleclicked;call super::doubleclicked;treeviewitem ltvi_item
str_treeview lstr_data

if not wf_is_valid(handle) then return -1

tab_ricerca.selezione.tv_selezione.selectitem(handle)
getitem(handle, ltvi_item)
lstr_data = ltvi_item.data

if lstr_data.tipo_livello = "N" then getwindow().postevent("pc_menu_dettagli")
end event

event tv_selezione::itempopulate;call super::itempopulate;treeviewitem ltvi_item
str_treeview lstr_data

if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if wf_leggi_livello(handle, lstr_data.livello + 1) < 1 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if

end event

event tv_selezione::rightclicked;call super::rightclicked;long ll_row
treeviewitem ltvi_item
str_treeview lstr_data
m_bolle_vendita lm_menu

if AncestorReturnValue < 0 then return

pcca.window_current = getwindow()
ll_row = tab_dettaglio.det_1.dw_1.getrow()

tab_ricerca.selezione.tv_selezione.getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if lstr_data.tipo_livello = "N" then

	lm_menu = create m_bolle_vendita
	
	//lm_menu.m_salda.enabled =  (tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "flag_evasione") <> "E")
	lm_menu.m_conferma.visible = ib_menu_conferma
	lm_menu.m_trasporto.visible = ib_menu_trasporto
	lm_menu.m_blocca.enabled = (tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "flag_blocco") = "N")
	lm_menu.m_sblocca.enabled = (tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "flag_blocco") = "S")
	
	
	//Gestione terzisti tramite scarico parziale ramo commessa interna
	//Se SI allora è possibile mediante inserimento numero commessa e semilavorato recuperare le MP e metterle in dettaglio
	//opzione disponibile solo per DDT conto lavoro
	if ib_SPT then
		lm_menu.m_inviompaterzista.visible = true
		//abilita questa voce solo se il ddt è del tipo conto lavorazione
		lm_menu.m_inviompaterzista.enabled = (f_des_tabella("tab_tipi_bol_ven","cod_tipo_bol_ven='"+tab_dettaglio.det_1.dw_1.getitemstring(ll_row, "cod_tipo_bol_ven")+"'","flag_tipo_bol_ven")="C")
	end if
	

	//ricalcolo spese trasporto
	lm_menu.m_ricalcolospesetrasp.visible = ib_spese_trasporto
	
	lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	
	destroy lm_menu
	
end if
end event

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data
	
tab_dettaglio.det_1.dw_1.change_dw_current()
getwindow().triggerevent("pc_retrieve")
end event

event tv_selezione::key;call super::key;long	ll_handle
string ls_messaggio, ls_flag_supervisore, ls_flag_movimenti,ls_stato_flag
treeviewitem ltv_item
ss_record lws_record
str_treeview lstr_data
uo_calcola_documento_euro luo_calcola_documento_euro

if key = KeyX! and keyflags=3 and s_cs_xx.cod_utente = "CS_SYSTEM" then
	
	if g_mb.confirm("Ricalcolare tutte le bolle nella lista?") then

		
		ll_handle = tv_selezione.FindItem(CurrentTreeItem!, 0)
		
		setpointer(HourGlass!)
		
		do while true
			
			if ll_handle <= 0 or isnull(ll_handle) then exit
			tv_selezione.getitem(ll_handle, ltv_item)
			
			lws_record = ltv_item.data
			
			if lws_record.anno_registrazione > 0 and lws_record.num_registrazione > 0 then
			
				luo_calcola_documento_euro = create uo_calcola_documento_euro
				
				luo_calcola_documento_euro.ib_salta_esposiz_cliente = true
				
				w_cs_xx_mdi.setmicrohelp("Calcolo bolla " + string(lws_record.anno_registrazione) + "-"  + string(lws_record.num_registrazione))
				Yield()
				
				if luo_calcola_documento_euro.uof_calcola_documento(lws_record.anno_registrazione, lws_record.num_registrazione,"bol_ven",ls_messaggio) <> 0 then
					if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.messagebox("APICE",ls_messaggio)
					rollback;
				else
					commit;
				end if
				destroy luo_calcola_documento_euro
			end if
			
			ll_handle = tv_selezione.finditem( NextTreeItem! ,ll_handle )

		loop
		w_cs_xx_mdi.setmicrohelp("Elaborazione Eseguita Correttamente; calcolo terminato!")
		setpointer(Arrow!)

	end if
	
if key = KeyM! and keyflags=2 then
	
	if s_cs_xx.cod_utente <> "CS_SYSTEM" then
		select flag_supervisore
		into	:ls_flag_supervisore
		from	utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode <> 0 then ls_flag_supervisore = "N"
	
		if ls_flag_supervisore = "N" then
			g_mb.warning("Funzione utilizzabile solo da utenti Supervisori.")
			return
		end if
	end if
	
	ll_handle = tab_ricerca.selezione.tv_selezione.FindItem(CurrentTreeItem!, 0)
	
	if ll_handle <= 0 or isnull(ll_handle) then return
	tab_ricerca.selezione.tv_selezione.getitem(ll_handle, ltv_item)
	lstr_data = ltv_item.data
	
	if lstr_data.decimale[1] > 0 and lstr_data.decimale[2] > 0 then
		select flag_movimenti
		into	:ls_flag_movimenti
		from	tes_bol_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lstr_data.decimale[1] and
				num_registrazione = :lstr_data.decimale[2];
		if sqlca.sqlcode <> 0 then
			g_mb.error("Bolla selezionata non trovata")
			return
		end if
		
		if ls_flag_movimenti = "S" then
			if g_mb.confirm("Sei sicuro di voler togliere check CONFERMATA dalla BOLLA? L'operazione sarà registrata in un LOG") then
				ls_stato_flag="N"
			else
				return
			end if
		else
			if g_mb.confirm("Sei sicuro di voler applicare il FLAG CONFERMATA alla BOLLA? L'operazione sarà registrata in un LOG") then
				ls_stato_flag="S"
			else
				return
			end if
		end if		
		
		f_scrivi_log( g_str.format( "FLAG CONFERMA portato allo stato '$3' nella BOLLA num int. [$1 / $2]", lstr_data.decimale[1], lstr_data.decimale[2], ls_stato_flag))
		
		
		update tes_bol_ven
		set flag_movimenti = :ls_stato_flag
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lstr_data.decimale[1] and
				num_registrazione = :lstr_data.decimale[2];
		if sqlca.sqlcode = 0 then
			g_mb.show("'Flag Bolla Confermata' rimosso con successo!")
			tab_dettaglio.det_1.dw_1.change_dw_current()
			getwindow().triggerevent("pc_retrieve")
			commit;
			// LOG
		else
			g_mb.error("Errore SQL in modifica 'Flag Bolla Confermata'")
			rollback;
			return
		end if
	end if
end if

if key = KeyF! and keyflags=3 then
	
	if s_cs_xx.cod_utente <> "CS_SYSTEM" then
		select flag_supervisore
		into	:ls_flag_supervisore
		from	utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode <> 0 then ls_flag_supervisore = "N"
	
		if ls_flag_supervisore = "N" then
			g_mb.warning("Funzione utilizzabile solo da utenti Supervisori.")
			return
		end if
	end if
	
	ll_handle = tab_ricerca.selezione.tv_selezione.FindItem(CurrentTreeItem!, 0)
	
	if ll_handle <= 0 or isnull(ll_handle) then return
	tab_ricerca.selezione.tv_selezione.getitem(ll_handle, ltv_item)
	lstr_data = ltv_item.data
	
	if lstr_data.decimale[1] > 0 and lstr_data.decimale[2] > 0 then
		select flag_gen_fat
		into	:ls_flag_movimenti
		from	tes_bol_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lstr_data.decimale[1] and
				num_registrazione = :lstr_data.decimale[2];
		if sqlca.sqlcode <> 0 then
			g_mb.error("Bolla selezionata non trovata")
			return
		end if
		
		if ls_flag_movimenti = "S" then
			if g_mb.confirm("Sei sicuro di voler togliere check FATTURATA dalla bolla? L'operazione sarà registrata in un LOG") then
				ls_stato_flag="N"
			else
				return
			end if
		else
			if g_mb.confirm("Sei sicuro di voler applicare il FLAG FATTURATO alla bolla? L'operazione sarà registrata in un LOG") then
				ls_stato_flag="S"
			else
				return
			end if
		end if		
		
		f_scrivi_log( g_str.format( "FLAG BOLLA FATTURATA portato allo stato '$3' nella BOLLA num int. [$1 / $2]", lstr_data.decimale[1], lstr_data.decimale[2], ls_stato_flag))
		
		
		update tes_bol_ven
		set flag_gen_fat = :ls_stato_flag
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lstr_data.decimale[1] and
				num_registrazione = :lstr_data.decimale[2];
		if sqlca.sqlcode = 0 then
			g_mb.show("'Flag Bolla Fatturata' rimosso con successo!")
			tab_dettaglio.det_1.dw_1.change_dw_current()
			getwindow().triggerevent("pc_retrieve")
			commit;
			// LOG
		else
			g_mb.error("Errore SQL in modifica 'Flag Bolla Fatturata'")
			rollback;
			return
		end if
	end if
end if	

end if
end event

type dw_1 from uo_cs_xx_dw within det_1
integer x = 5
integer y = 12
integer width = 3200
integer height = 1780
integer taborder = 30
string dataobject = "d_tes_bol_ven_det_1_tv"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

if not isvalid(istr_data) or isnull(istr_data) or UpperBound(istr_data.decimale) < 2 then return

ll_errore = retrieve(s_cs_xx.cod_azienda, istr_data.decimale[1],istr_data.decimale[2])

if ll_errore < 0 then
   pcca.error = c_fatal
end if

change_dw_current()
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_1,"cod_cliente")
		
	case "b_ricerca_banca_for"
		guo_ricerca.uof_ricerca_banca_clifor(dw_1,"cod_banca_clien_for")
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_1, "cod_fornitore")
		
	case "b_ricerca_contatto"
		guo_ricerca.uof_ricerca_contatto(dw_1, "cod_contatto")
		
end choose
end event

event itemchanged;call super::itemchanged;if i_extendmode then
   string ls_null, ls_cod_cliente, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
			 ls_frazione, ls_cap, ls_localita, ls_provincia, ls_telefono, &
			 ls_fax, ls_telex, ls_partita_iva, ls_cod_fiscale, ls_cod_deposito, &
			 ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
			 ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca_clien_for, ls_cod_banca,&
			 ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, &
			 ls_cod_porto, ls_cod_resa, ls_flag_fuori_fido, ls_flag_riep_fatt, &
			 ls_cod_fornitore, ls_flag_tipo_bol_ven, ls_causale_trasporto, ls_cod_causale, &
			 ls_nota_testata, ls_nota_piede, ls_nota_video, ls_messaggio, ls_flag_dest_merce_fat,&
			 ls_flag_blocco, ls_flag_bol_fat,ls_flag_terzista,ls_cod_tipo_bol_ven, &
			 ls_cod_documento, ls_numeratore, ls_duplicati
	long   ll_anno_documento, ll_num_documento, ll_anno, ll_num
	double ld_sconto
	datetime ldt_data_registrazione, ldt_data_blocco
	integer	li_ret
		
	setnull(ls_null)
	
   choose case i_colname
		//#########################################################################################################
		case "flag_cliente_contatto"
			wf_flag_cliente_contatto(data)
				
		//#########################################################################################################
		case "data_registrazione"
				ls_cod_valuta = this.getitemstring(1, "cod_valuta")
				f_cambio_ven(ls_cod_valuta, datetime(date(i_coltext)))
				
		//#########################################################################################################
		case "cod_contatto"
			wf_carica_contatto(i_coltext, true)	
			
		//#########################################################################################################
		case "cod_tipo_bol_ven"
			select tab_tipi_bol_ven.flag_tipo_bol_ven,   
					tab_tipi_bol_ven.causale_trasporto,
					tab_tipi_bol_ven.cod_causale
			into   :ls_flag_tipo_bol_ven,   
					:ls_causale_trasporto,
					:ls_cod_causale
			from   tab_tipi_bol_ven  
			where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and
					  tab_tipi_bol_ven.cod_tipo_bol_ven = :i_coltext;
			
			if sqlca.sqlcode <> 0 then
				g_mb.error("Si è verificato un errore in fase di lettura Tabella Tipi Bolle Vendita.")
				return
			end if
			
			tab_dettaglio.det_3.dw_3.setitem(row, "causale_trasporto", ls_causale_trasporto)
			tab_dettaglio.det_3.dw_3.setitem(row, "cod_causale", ls_cod_causale)
			
			wf_tipo_bolla(ls_flag_tipo_bol_ven, false)
			
			ls_cod_fornitore = getitemstring(getrow(),"cod_fornitore")
			if not isnull(ls_cod_fornitore) then
				select flag_terzista
				into   :ls_flag_terzista
				from   anag_fornitori
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_fornitore = :ls_cod_fornitore;
				if sqlca.sqlcode = 100 then
					g_mb.messagebox("APICE","Errore in ricerca flag terzista in anagrafica fornitori. ~r~nDettaglio: " +sqlca.sqlerrtext)
					return
				end if
				if (ls_flag_tipo_bol_ven = 'T' or ls_flag_tipo_bol_ven = 'C') and ls_flag_terzista = 'N' then
					g_mb.messagebox("APICE","Si sta tendando di fare una bolla di conto lavoro o trasferimeno ad un fornitore che non è terzista.~r~nQuesto potrebbe creare potenzialmente dei problemi nella gestione inventario di magazzino.", information!)
				end if
			end if
			
		//#########################################################################################################
		case "cod_cliente"
			li_ret = wf_carica_cliente(data, true)
			if li_ret=1 then
				this.setitem(i_rownbr, i_colname, ls_null)
				this.SetColumn ( i_colname )
				return 1
			end if
			
		//#########################################################################################################
		case "cod_des_cliente"
         	ls_cod_cliente = this.getitemstring(i_rownbr, "cod_cliente")
			if f_cerca_destinazione(tab_dettaglio.det_2.dw_2, i_rownbr, ls_cod_cliente, "C", i_coltext, ls_messaggio) = -1 then
				g_mb.messagebox("APICE","Errore in ricerca destinazione diversa. ~r~nDettaglio: " +ls_messaggio)
			end if
		
		//#########################################################################################################
		case "cod_fornitore"
			wf_carica_fornitore(data, true)
		
		//#########################################################################################################
     	case "cod_fil_fornitore"
         	ls_cod_fornitore = this.getitemstring(i_rownbr, "cod_fornitore")

			select anag_fil_fornitori.rag_soc_1,
					 anag_fil_fornitori.rag_soc_2,
					 anag_fil_fornitori.indirizzo,
					 anag_fil_fornitori.frazione,
					 anag_fil_fornitori.cap,
					 anag_fil_fornitori.localita,
					 anag_fil_fornitori.provincia,
					 anag_fil_fornitori.telefono,
					 anag_fil_fornitori.fax,
					 anag_fil_fornitori.telex
			into   :ls_rag_soc_1,    
					 :ls_rag_soc_2,
					 :ls_indirizzo,    
					 :ls_frazione,
					 :ls_cap,
					 :ls_localita,
					 :ls_provincia,
					 :ls_telefono,
					 :ls_fax,
					 :ls_telex
			from   anag_fil_fornitori
			where  anag_fil_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_fil_fornitori.cod_fornitore = :ls_cod_fornitore and 
					 anag_fil_fornitori.cod_fil_fornitore = :i_coltext;
 
			if sqlca.sqlcode = 0 then 
				if not isnull(ls_rag_soc_1) then
					tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text=''")
				end if
				if not isnull(ls_rag_soc_2) then
					tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text=''")
				end if
				if not isnull(ls_indirizzo) then
					tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text=''")
				end if
				if not isnull(ls_frazione) then
					tab_dettaglio.det_2.dw_2.modify("st_frazione.text='" + ls_frazione + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_frazione.text=''")
				end if
				if not isnull(ls_cap) then
					tab_dettaglio.det_2.dw_2.modify("st_cap.text='" + ls_cap + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_cap.text=''")
				end if
				if not isnull(ls_localita) then
					tab_dettaglio.det_2.dw_2.modify("st_localita.text='" + ls_localita + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_localita.text=''")
				end if
				if not isnull(ls_provincia) then
					tab_dettaglio.det_2.dw_2.modify("st_provincia.text='" + ls_provincia + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_provincia.text=''")
				end if
				if not isnull(ls_telefono) then
					tab_dettaglio.det_2.dw_2.modify("st_telefono.text='" + ls_telefono + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_telefono.text=''")
				end if
				if not isnull(ls_fax) then
					tab_dettaglio.det_2.dw_2.modify("st_fax.text='" + ls_fax + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_fax.text=''")
				end if
				if not isnull(ls_telex) then
					tab_dettaglio.det_2.dw_2.modify("st_telex.text='" + ls_telex + "'")
				else
					tab_dettaglio.det_2.dw_2.modify("st_telex.text=''")
				end if
				
			else
				
				select anag_fornitori.rag_soc_1,
						 anag_fornitori.rag_soc_2,
						 anag_fornitori.indirizzo,
						 anag_fornitori.frazione,
						 anag_fornitori.cap,
						 anag_fornitori.localita,
						 anag_fornitori.provincia,
						 anag_fornitori.telefono,
						 anag_fornitori.fax,
						 anag_fornitori.telex
				into   :ls_rag_soc_1,    
						 :ls_rag_soc_2,
						 :ls_indirizzo,    
						 :ls_frazione,
						 :ls_cap,
						 :ls_localita,
						 :ls_provincia,
						 :ls_telefono,
						 :ls_fax,
						 :ls_telex
				from   anag_fornitori
				where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
						 anag_fornitori.cod_fornitore = :ls_cod_fornitore;
 
				if sqlca.sqlcode = 0 then 
					if not isnull(ls_rag_soc_1) then
						tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
					else
						tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text=''")
					end if
					if not isnull(ls_rag_soc_2) then
						tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
					else
						tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text=''")
					end if
					if not isnull(ls_indirizzo) then
						tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
					else
						tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text=''")
					end if
					if not isnull(ls_frazione) then
						tab_dettaglio.det_2.dw_2.modify("st_frazione.text='" + ls_frazione + "'")
					else
						tab_dettaglio.det_2.dw_2.modify("st_frazione.text=''")
					end if
					if not isnull(ls_cap) then
						tab_dettaglio.det_2.dw_2.modify("st_cap.text='" + ls_cap + "'")
					else
						tab_dettaglio.det_2.dw_2.modify("st_cap.text=''")
					end if
					if not isnull(ls_localita) then
						tab_dettaglio.det_2.dw_2.modify("st_localita.text='" + ls_localita + "'")
					else
						tab_dettaglio.det_2.dw_2.modify("st_localita.text=''")
					end if
					if not isnull(ls_provincia) then
						tab_dettaglio.det_2.dw_2.modify("st_provincia.text='" + ls_provincia + "'")
					else
						tab_dettaglio.det_2.dw_2.modify("st_provincia.text=''")
					end if
					if not isnull(ls_telefono) then
						tab_dettaglio.det_2.dw_2.modify("st_telefono.text='" + ls_telefono + "'")
					else
						tab_dettaglio.det_2.dw_2.modify("st_telefono.text=''")
					end if
					if not isnull(ls_fax) then
						tab_dettaglio.det_2.dw_2.modify("st_fax.text='" + ls_fax + "'")
					else
						tab_dettaglio.det_2.dw_2.modify("st_fax.text=''")
					end if
					if not isnull(ls_telex) then
						tab_dettaglio.det_2.dw_2.modify("st_telex.text='" + ls_telex + "'")
					else
						tab_dettaglio.det_2.dw_2.modify("st_telex.text=''")
					end if
				else
					tab_dettaglio.det_2.dw_2.modify("st_rag_soc_1.text=''")
					tab_dettaglio.det_2.dw_2.modify("st_rag_soc_2.text=''")
					tab_dettaglio.det_2.dw_2.modify("st_indirizzo.text=''")
					tab_dettaglio.det_2.dw_2.modify("st_frazione.text=''")
					tab_dettaglio.det_2.dw_2.modify("st_cap.text=''")
					tab_dettaglio.det_2.dw_2.modify("st_localita.text=''")
					tab_dettaglio.det_2.dw_2.modify("st_provincia.text=''")
					tab_dettaglio.det_2.dw_2.modify("st_telefono.text=''")
					tab_dettaglio.det_2.dw_2.modify("st_fax.text=''")
					tab_dettaglio.det_2.dw_2.modify("st_telex.text=''")
				end if 
			end if
	
	//#########################################################################################################
      case "cod_des_fornitore"
         ls_cod_fornitore = this.getitemstring(i_rownbr, "cod_fornitore")
         if f_cerca_destinazione(tab_dettaglio.det_2.dw_2, i_rownbr, ls_cod_fornitore, "F", i_coltext, ls_messaggio) = -1 then
			g_mb.messagebox("APICE","Errore in ricerca destinazione diversa. ~r~nDettaglio: " +ls_messaggio)
		end if
	
	//#########################################################################################################
	case "cod_deposito_tras"
		wf_carica_deposito_trasf(data)
		
	//#########################################################################################################
	case "cod_deposito"
		wf_carica_deposito(data)
	
	//#########################################################################################################
	case "cod_valuta"
		ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
         f_cambio_ven(i_coltext, ldt_data_registrazione)
			
	//#########################################################################################################	
	case "cod_documento","anno_documento","numeratore_documento","num_documento"
		//--------------------------------------------------------
		choose case i_colname
			case "cod_documento"
				ls_cod_documento = i_coltext
				ll_anno_documento = getitemnumber(getrow(),"anno_documento")
				ls_numeratore = getitemstring(getrow(),"numeratore_documento")
				ll_num_documento = getitemnumber(getrow(),"num_documento")
			case "anno_documento"
				ls_cod_documento = getitemstring(getrow(),"cod_documento")
				ll_anno_documento = long(i_coltext)
				ls_numeratore = getitemstring(getrow(),"numeratore_documento")
				ll_num_documento = getitemnumber(getrow(),"num_documento")
			case "numeratore_documento"
				ls_cod_documento = getitemstring(getrow(),"cod_documento")
				ll_anno_documento = getitemnumber(getrow(),"anno_documento")
				ls_numeratore = i_coltext
				ll_num_documento = getitemnumber(getrow(),"num_documento")
			case "num_documento"
				ls_cod_documento = getitemstring(getrow(),"cod_documento")
				ll_anno_documento = getitemnumber(getrow(),"anno_documento")
				ls_numeratore = getitemstring(getrow(),"numeratore_documento")
				ll_num_documento = long(i_coltext)					
		end choose
		//--------------------------------------------------------
		
		declare duplicati cursor for
		select anno_registrazione,
				 num_registrazione
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_documento = :ls_cod_documento and
				 anno_documento = :ll_anno_documento and
				 numeratore_documento = :ls_numeratore and
				 num_documento = :ll_num_documento;
				 
		open duplicati;
			
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in controllo numeri fiscali doppi.~n" + &
						  "Errore nella open del cursore duplicati: " + sqlca.sqlerrtext)
			return 1
		end if
			
		ls_duplicati = ""
			
		do while true
			fetch duplicati
			into  :ll_anno, :ll_num;
						
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore in controllo numeri fiscali doppi.~n" + &
							  "Errore nella fetch del cursore duplicati: " + sqlca.sqlerrtext)
				close duplicati;
				return 1
			elseif sqlca.sqlcode = 100 then
				close duplicati;
				exit
			end if
				
			ls_duplicati = ls_duplicati + "~n" + string(ll_anno) + "/" + string(ll_num)
				
		loop
			
		if ls_duplicati <> "" then
			ls_duplicati = "ATTENZIONE!~nEsistono altri documenti con questo numero fiscale:" + ls_duplicati
			g_mb.messagebox("APICE",ls_duplicati)
		end if
			
		// *** controllo se è maggiore di quello esistente
		long ll_num_appo, ll_ret
		
		select max(num_documento)
		into   :ll_num_appo
		from   numeratori  
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_documento = :ls_cod_documento and
				 numeratore_documento = :ls_numeratore and
				 anno_documento = :ll_anno_documento;			
					 
		if not isnull(ll_num_appo) and ll_num_appo < ll_num_documento then   // il numero cambiato è + grande
			
			ll_ret = g_mb.messagebox( "APICE", "Aggiorno i numeratori fiscali?", Exclamation!, OKCancel!, 2)
			IF ll_ret = 1 THEN
				update numeratori
				set    num_documento = :ll_num_documento
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_documento = :ls_cod_documento and
						 numeratore_documento = :ls_numeratore and
						 anno_documento = :ll_anno_documento;		
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox( "APICE", "Errore durante l'aggiornamento dei numeratori: " + sqlca.sqlerrtext, stopsign!)
					rollback;
					return -1
				else
					commit;
					g_mb.messagebox( "APICE", "Numeratori_aggiornati")
				end if
			END IF
		
		end if
	
	//#########################################################################################################
	case "data_bolla"
		date ldt_1, ldt_2
			
		ldt_1 = date(left(i_coltext, 10))
		ldt_2 = today()
		
		if ldt_1 <> ldt_2 then
			g_mb.messagebox("APICE","ATTENZIONE!~nLa data inserita è diversa dalla data di sistema.")
		end if
			
   end choose
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	string ls_modify
	
	
	ls_modify = "cod_tipo_bol_ven.protect='0~tif(anno_registrazione = 0,0,1)'~t"
	ls_modify = ls_modify + "cod_tipo_bol_ven.background.color='12632256~tif(anno_registrazione = 0,16777215,12632256)'~t"
	tab_dettaglio.det_1.dw_1.modify(ls_modify)

	wf_abilita_pulsanti_ricerca(true)
	
	ib_new = false
end if
end event

event pcd_new;call super::pcd_new;string ls_cod_tipo_bol_ven, ls_flag_tipo_bol_ven, ls_causale_trasporto, &
		ls_modify, ls_cod_causale, ls_cod_operatore, ls_cod_deposito, ls_errore
	
select con_bol_ven.cod_tipo_bol_ven
into :ls_cod_tipo_bol_ven
from con_bol_ven
where con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	g_mb.error("Si è verificato un errore in fase di lettura Parametri Bolle Vendita.")
	wf_reset_tipo_bolla()
end if

this.setitem(this.getrow(), "cod_tipo_bol_ven", ls_cod_tipo_bol_ven)
this.setitem(this.getrow(), "data_registrazione", datetime(today()))
this.setitem(this.getrow(), "data_inizio_trasporto", datetime(today(), 00:00:00))
this.setitem(this.getrow(), "ora_inizio_trasporto", datetime(date("01/01/1900"),now()))

if not isnull(ls_cod_tipo_bol_ven) and ls_cod_tipo_bol_ven <> "" then
	
	select tab_tipi_bol_ven.flag_tipo_bol_ven,   
			 tab_tipi_bol_ven.causale_trasporto,
			 tab_tipi_bol_ven.cod_causale
	into 	:ls_flag_tipo_bol_ven,   
			:ls_causale_trasporto,
			:ls_cod_causale
	from tab_tipi_bol_ven  
	where tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and
			 tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
	
	if sqlca.sqlcode <> 0 then
		wf_reset_tipo_bolla()
	end if
	
end if

tab_dettaglio.det_3.dw_3.setitem(this.getrow(), "causale_trasporto", ls_causale_trasporto)
tab_dettaglio.det_3.dw_3.setitem(this.getrow(), "cod_causale", ls_cod_causale)

wf_tipo_bolla(ls_flag_tipo_bol_ven, false)
	
ls_modify = "cod_tipo_bol_ven.protect='0~tif(anno_registrazione = 0,0,1)'~t"
ls_modify = ls_modify + "cod_tipo_bol_ven.background.color='12632256~tif(anno_registrazione = 0,16777215,12632256)'~t"
tab_dettaglio.det_1.dw_1.modify(ls_modify)

select cod_operatore
into :ls_cod_operatore
from tab_operatori_utenti
where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente and
		 flag_default = 'S';
			 
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Estrazione Operatori", "Errore durante l'Estrazione dell'Operatore di Default")
	pcca.error = c_fatal
	return
elseif sqlca.sqlcode = 0 then
	this.setitem(this.getrow(), "cod_operatore", ls_cod_operatore)
end if
	
// stefanop: 21/12/2011: aggiunto deposito collegato all'operatore
guo_functions.uof_get_stabilimento_utente(ls_cod_deposito, ls_errore)
if not isnull(ls_cod_deposito) then setitem(getrow(), "cod_deposito", ls_cod_deposito)
// ----

wf_abilita_pulsanti_ricerca(true)
ib_new = true
	
tab_dettaglio.det_4.dw_4.setitem(tab_dettaglio.det_1.dw_1.getrow(), "flag_doc_suc_tes", "I")
tab_dettaglio.det_4.dw_4.setitem(tab_dettaglio.det_1.dw_1.getrow(), "flag_st_note_tes", "I")	
tab_dettaglio.det_4.dw_4.setitem(tab_dettaglio.det_1.dw_1.getrow(), "flag_doc_suc_pie", "I")
tab_dettaglio.det_4.dw_4.setitem(tab_dettaglio.det_1.dw_1.getrow(), "flag_st_note_pie", "I")	
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	if pcca.error = c_success then
		
		wf_abilita_pulsanti_ricerca(false)		
		
		//###########################################################
		//metto qui il messaggio per evitare eventuali locks: commit già è stato fatto
		//se ho cancellato il ddt e ho riaperto ordini ho la lista di questi ordini
		//in tutti gli altri casi la variabile sarà vuota e non partirà alcun messaggio
		if is_ordini_riaperti<>"" and not isnull(is_ordini_riaperti) then
			g_mb.warning("Al seguito della cancellazione del ddt sono stati riaperti i seguenti ordini:~r~n"+is_ordini_riaperti)
		end if
		is_ordini_riaperti = ""
		//###########################################################
		
	end if
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_max_registrazione

for ll_i = 1 to this.rowcount()
	
	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if
	
	if this.getitemnumber(ll_i, "anno_registrazione") = 0 or &
		isnull(this.getitemnumber(ll_i, "anno_registrazione")) then

		ll_anno_registrazione = f_anno_esercizio()
		if ll_anno_registrazione > 0 then
			this.setitem(this.getrow(), "anno_registrazione", int(ll_anno_registrazione))
		else
			g_mb.messagebox("Bolle uscita","Impostare l'anno di esercizio in parametri aziendali")
		end if
		
			select con_bol_ven.num_registrazione
			into   :ll_num_registrazione
			from   con_bol_ven
			where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;
		
		choose case sqlca.sqlcode
			case 0
				ll_num_registrazione ++
			case 100
				g_mb.messagebox("Bolle di uscita","Errore in assegnazione numero bolla interno: verificare di aver impostato i parametri offerte di vendita. ~r~nIl salvataggio verrà effettuato ugualmente")
			case else
				g_mb.messagebox("Bolle di uscita","Errore in assegnazione numero bolla interno.~r~nDettaglio errore "+ sqlca.sqlerrtext +"~r~nIl salvataggio verrà effettuato ugualmente")
		end choose
		
		ll_max_registrazione = 0
	
		select max(num_registrazione)
		into :ll_max_registrazione
		from tes_bol_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione;
				 
		if not isnull(ll_max_registrazione) then
			if ll_max_registrazione >= ll_num_registrazione then ll_num_registrazione = ll_max_registrazione + 1
		end if
		
		this.setitem(this.getrow(), "num_registrazione", ll_num_registrazione)
		
		update con_bol_ven
		set con_bol_ven.num_registrazione = :ll_num_registrazione
		where  con_bol_ven.cod_azienda = :s_cs_xx.cod_azienda;
    end if
next      
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_tipo_bol_ven, ls_flag_tipo_bol_ven, ls_cod_cliente, ls_cod_contatto

ls_cod_tipo_bol_ven = this.getitemstring(this.getrow(),"cod_tipo_bol_ven")

select tab_tipi_bol_ven.flag_tipo_bol_ven
into :ls_flag_tipo_bol_ven
from tab_tipi_bol_ven
where tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and
		 tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;

if i_insave > 0 then
   if this.getrow() > 0 then
		ls_cod_cliente = this.getitemstring(this.getrow(), "cod_cliente")
		ls_cod_contatto = this.getitemstring(this.getrow(), "cod_contatto")
		
		//if ls_flag_tipo_bol_ven = 'V' and isnull( this.getitemstring(this.getrow(), "cod_cliente")) then
		if ls_flag_tipo_bol_ven = 'V' then
			// controllo flag_cliente_contatto
			if getitemstring(getrow(), "flag_cliente_contatto") = "S" then
				// cliente
				if isnull(ls_cod_cliente) or len(ls_cod_cliente) < 1 then
					g_mb.messagebox("Attenzione", "Inserire Codice Cliente.", exclamation!, ok!)
					pcca.error = c_fatal
					return
				end if
			else
				// Contatto
				if isnull(ls_cod_contatto) or len(ls_cod_contatto) < 1 then
					g_mb.messagebox("Attenzione", "Inserire Codice Contatto.", exclamation!, ok!)
					pcca.error = c_fatal
					return
				end if
			end if
				
		elseif ls_flag_tipo_bol_ven = 'R' and isnull(this.getitemstring(this.getrow(), "cod_fornitore")) then
				g_mb.messagebox("Attenzione", "Inserire Codice Fornitore.", exclamation!, ok!)
				pcca.error = c_fatal
				return
		elseif ls_flag_tipo_bol_ven = 'T' and isnull(this.getitemstring(this.getrow(), "cod_deposito_tras")) then
				g_mb.messagebox("Attenzione", "Inserire Codice Deposito di Arrivo.", exclamation!, ok!)
				pcca.error = c_fatal
				return
		elseif ls_flag_tipo_bol_ven = 'C' and (isnull(this.getitemstring(this.getrow(), "cod_deposito_tras")) or &
				 isnull(this.getitemstring(this.getrow(), "cod_fornitore"))) then
         g_mb.messagebox("Attenzione", "Inserire Codice Fornitore e Codice Deposito di Arrivo.", &
                    exclamation!, ok!)
         pcca.error = c_fatal
         return
      end if
   end if
end if
end event

event pcd_view;call super::pcd_view;boolean lb_enabled

if i_extendmode then
	wf_abilita_pulsanti_ricerca(false)
	
	ib_new = false
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
   string ls_cod_cliente, ls_cod_fornitore, ls_cod_tipo_bol_ven, &
		 ls_flag_tipo_bol_ven, ls_cod_deposito,  ls_cod_contatto, ls_cod_deposito_trasf

	if this.getrow() > 0 then
		ls_cod_cliente = this.getitemstring(this.getrow(), "cod_cliente")
		ls_cod_fornitore = this.getitemstring(this.getrow(), "cod_fornitore")
		ls_cod_tipo_bol_ven = this.getitemstring(this.getrow(), "cod_tipo_bol_ven")
		ls_cod_deposito = this.getitemstring(this.getrow(), "cod_deposito")
		ls_cod_deposito_trasf = this.getitemstring(this.getrow(), "cod_deposito_tras")
		ls_cod_contatto = this.getitemstring(this.getrow(), "cod_contatto")
		
		select flag_tipo_bol_ven
		into :ls_flag_tipo_bol_ven
		from tab_tipi_bol_ven  
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;

		if not isnull(ls_cod_tipo_bol_ven) then
			wf_tipo_bolla(ls_flag_tipo_bol_ven, true)
		end if
		
		if not isnull(ls_cod_contatto) then
			wf_flag_cliente_contatto("N") // mostro i campi del contatto
			setitem(getrow(), "flag_cliente_contatto", "N")
			wf_carica_contatto(ls_cod_contatto, false)
			
		elseif not isnull(ls_cod_cliente) then
			wf_flag_cliente_contatto("S") // mostro i campi del cliente
			setitem(getrow(), "flag_cliente_contatto", "S")
			//wf_carica_deposito_trasf(ls_cod_deposito_trasf)
			wf_carica_cliente(ls_cod_cliente, false)
			
		elseif not isnull(ls_cod_fornitore) then
			//wf_carica_deposito_trasf(ls_cod_deposito_trasf)
			wf_carica_fornitore(ls_cod_fornitore, false)
			
		end if
		

		if ls_flag_tipo_bol_ven = 'T' then
			wf_carica_deposito(ls_cod_deposito)
		end if
	end if
	
	resetupdate()
end if
end event

event ue_key;call super::ue_key;choose case this.getcolumnname()

	case "cod_cliente"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_cliente(tab_dettaglio.det_1.dw_1,"cod_cliente")
		end if
		
	case "cod_fornitore"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_fornitore(tab_dettaglio.det_1.dw_1,"cod_fornitore")
		end if
end choose


end event

event updatestart;call super::updatestart;if i_extendmode then
	integer				li_i, ll_ret
	string					ls_tabella, ls_nome_prog, ls_cod_documento, ls_numeratore_documento, ls_sql, &
							ls_cod_deposito, ls_cod_deposito_tras, ls_cod_cliente, ls_nota_testata, ls_nota_piede, ls_nota_old, ls_null, ls_nota_video, &
							ls_cod_fornitore, ls_cod_contatto, ls_cod_tipo_bol_ven, ls_errore, ls_flag_tipo_bol_ven
	datetime ldt_data_registrazione
	long ll_anno_registrazione, ll_num_registrazione, ll_tot_ref
	dwitemstatus ldw_status
	uo_log_sistema luo_log

	setnull(ls_null)

	is_ordini_riaperti = ""


	for li_i = 1 to deletedcount()
		ll_anno_registrazione = this.getitemnumber(li_i, "anno_registrazione", delete!, true)
		ll_num_registrazione = this.getitemnumber(li_i, "num_registrazione", delete!, true)
		ls_cod_tipo_bol_ven = this.getitemstring(li_i, "cod_tipo_bol_ven", delete!, true)
		
		luo_log = create uo_log_sistema
		luo_log.uof_write_log_sistema_not_sqlca( "CANC_DDT", g_str.format("Cancellazione Testata DDT Uscita $1-$2",ll_anno_registrazione,ll_num_registrazione))
		destroy luo_log
		
		
		ls_tabella = "det_bol_ven_stat"
		ls_nome_prog = ""

		if f_elimina_det_stat(ls_tabella, ll_anno_registrazione, ll_num_registrazione, ls_nome_prog, 0) = -1 then return 1

		if f_riag_quan_in_spedizione_bolle(ll_anno_registrazione, ll_num_registrazione) = -1 then return 1

		delete det_bol_ven_note		
		where cod_azienda=:s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice","Errore durante cancellazione note dettaglio bolla" + sqlca.sqlerrtext,stopsign!)
			return 1
		end if
		
		delete det_bol_ven_corrispondenze
		where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione  = :ll_num_registrazione;
				 
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante cancellazione corrispondenze dettaglio bolla" + sqlca.sqlerrtext)
			return 1
		end if
      
		delete iva_bol_ven
		where cod_azienda = :s_cs_xx.cod_azienda
		and anno_registrazione = :ll_anno_registrazione
		and num_registrazione  = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore durante cancellazione dati iva della bolla." + sqlca.sqlerrtext)
			return 1
		end if
		
		//-----------------------------------------------------------------------------------------------------------------------------
		//eseguo riapertura ordini contenuti nel ddt (solo se non è un ddt di trasferimento)
		if ls_cod_tipo_bol_ven<>"T" then
			ll_ret =  wf_riapri_ordini(ll_anno_registrazione, ll_num_registrazione, ls_errore)
			if ll_ret<0 then
				g_mb.error("APICE", ls_errore)
				return 1
			elseif ll_ret = 0 and ls_errore<>"" and not isnull(ls_errore) then
				is_ordini_riaperti = ls_errore
				ls_errore = ""
			end if
		end if
		//-----------------------------------------------------------------------------------------------------------------------------
		
		//anag_comm_anticipi
		select count(*)
		into :ll_tot_ref
		from det_anag_comm_anticipi
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_reg_bol_ven=:ll_anno_registrazione and
					num_reg_bol_ven=:ll_num_registrazione;
		
		if sqlca.sqlcode=0 and ll_tot_ref>0 then
			ls_sql = "delete from 	det_anag_comm_anticipi "+&
						"where 	cod_azienda='"+s_cs_xx.cod_azienda +"'and "+&
									"anno_reg_bol_ven="+string(ll_anno_registrazione)+" and "+&
									"num_reg_bol_ven="+string(ll_num_registrazione)
			execute immediate :ls_sql;
			
			if sqlca.sqlcode < 0 then
				g_mb.error("Errore durante cancellazione dati det_anag_comm_anticipi della bolla." + sqlca.sqlerrtext)
				return 1
			end if
		end if
		
		
		ls_tabella = "det_bol_ven"
		if f_del_dest_mov_mag(ls_tabella, ll_anno_registrazione, ll_num_registrazione) = -1 then return 1
		if f_del_det(ls_tabella, ll_anno_registrazione, ll_num_registrazione) = -1 then return 1
		
		wf_set_deleted_item_status()
	next


	if this.getrow() > 0 then
		ll_anno_registrazione = this.getitemnumber(this.getrow(), "anno_registrazione")
		ll_num_registrazione = this.getitemnumber(this.getrow(), "num_registrazione")
		
		if ll_anno_registrazione <> 0 then
			ls_tabella = "det_bol_ven"
		
			if tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_movimenti") = "S" then
				g_mb.warning("Bolla non modificabile! E' già stata confermata.")
				tab_dettaglio.det_1.dw_1.set_dw_view(c_ignorechanges)
				pcca.error = c_fatal
				return 1
			end if
		
			if tab_dettaglio.det_1.dw_1.getitemstring(tab_dettaglio.det_1.dw_1.getrow(), "flag_blocco") = "S" then
				g_mb.warning("Bolla non modificabile! E' Bloccata.")
				tab_dettaglio.det_1.dw_1.set_dw_view(c_ignorechanges)
				pcca.error = c_fatal
			return 1
		end if
			
		if ib_new then
			
			wf_inserisci_bolla_vendita(0, ll_anno_registrazione, ll_num_registrazione)
			
			ls_cod_cliente = this.getitemstring(this.getrow(), "cod_cliente")
			ls_cod_fornitore = this.getitemstring(this.getrow(), "cod_fornitore")
			ls_cod_contatto = this.getitemstring(this.getrow(), "cod_contatto")
			ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
			ls_cod_tipo_bol_ven = this.getitemstring(this.getrow(), "cod_tipo_bol_ven")
			
			if isnull(ls_cod_contatto) or len(ls_cod_contatto) < 1 then
				// non è un contatto quindi controllo per le note fisse
				select flag_tipo_bol_ven
				into :ls_flag_tipo_bol_ven
				from tab_tipi_bol_ven
				where 	cod_azienda = :s_cs_xx.cod_azienda and
							cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
				if sqlca.sqlcode <> 0 then
					g_mb.error("Errore nella select del tipo bolla")
					return -1
				end if
				
				if ls_flag_tipo_bol_ven = "R" or ls_flag_tipo_bol_ven = "C" then // Fornitore
					if guo_functions.uof_get_note_fisse(ls_null, ls_cod_fornitore, ls_null, "BOL_VEN", ls_null, ldt_data_registrazione, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
						g_mb.error(ls_nota_testata)
					else
						if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
						ls_nota_old = this.getitemstring(this.getrow(), "nota_testata")
						if isnull(ls_nota_old) then ls_nota_old = ""
						if not isnull(ls_nota_testata) and ls_nota_testata <> "" then
							this.setitem(this.getrow(), "nota_testata", ls_nota_testata + "~n~r" + ls_nota_old )
						else	
							this.setitem(this.getrow(), "nota_testata", ls_nota_old )
						end if
						
						ls_nota_old = this.getitemstring(this.getrow(), "nota_piede")
						if isnull(ls_nota_old) then ls_nota_old = ""
						if not isnull(ls_nota_piede) and ls_nota_piede <> "" then
							this.setitem(this.getrow(), "nota_piede", ls_nota_piede + "~n~r" + ls_nota_old )
						else	
							this.setitem(this.getrow(), "nota_piede", ls_nota_old )
						end if	
					end if
				else // Cliente
					if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "BOL_VEN", ls_null, ldt_data_registrazione, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
						g_mb.error(ls_nota_testata)
					else
						if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
						ls_nota_old = this.getitemstring(this.getrow(), "nota_testata")
						if isnull(ls_nota_old) then ls_nota_old = ""
						if not isnull(ls_nota_testata) and ls_nota_testata <> "" then
							this.setitem(this.getrow(), "nota_testata", ls_nota_testata + "~n~r" + ls_nota_old )
						else	
							this.setitem(this.getrow(), "nota_testata", ls_nota_old )
						end if
						
						ls_nota_old = this.getitemstring(this.getrow(), "nota_piede")
						if isnull(ls_nota_old) then ls_nota_old = ""
						if not isnull(ls_nota_piede) and ls_nota_piede <> "" then
							this.setitem(this.getrow(), "nota_piede", ls_nota_piede + "~n~r" + ls_nota_old )
						else	
							this.setitem(this.getrow(), "nota_piede", ls_nota_old )
						end if	
					end if
				end if
			
			end if
			
			ib_new = false
		end if
			
		//se il flag_avviso_contrassegno è S allora avvisa l'utente di rigenerare il contrassegno
		uo_avviso_spedizioni luo_avviso_sped
		string ls_msg_avviso
		long ll_return
		
		luo_avviso_sped = create uo_avviso_spedizioni
		
		luo_avviso_sped.il_anno_documento = ll_anno_registrazione
		luo_avviso_sped.il_num_documento = ll_num_registrazione
		luo_avviso_sped.is_tipo_gestione = "bol_ven"
		
		ll_return= luo_avviso_sped.uof_check_flag_avviso_contrassegno(ls_msg_avviso)
		
		if ll_return<0 then
			g_mb.error(ls_msg_avviso)
			
		elseif ll_return=1 then
			g_mb.show(ls_msg_avviso)
		end if
		
		destroy luo_avviso_sped;
		//--------------------------------------------------------------------------------------------------
			
      end if
   end if

	for li_i = 1 to rowcount()
		// resetto sempre lo status della colonna perchè NON deve essere salvata!
		tab_dettaglio.det_1.dw_1.setitemstatus(tab_dettaglio.det_1.dw_1.getrow(), "flag_cliente_contatto", Primary!, NotModified!)
				
		ls_cod_documento = this.getitemstring(li_i, "cod_documento")
		ls_numeratore_documento = this.getitemstring(li_i, "numeratore_documento")
	
		if not f_numeratore(ls_numeratore_documento, ls_cod_documento) then
			g_mb.error("Numeratore non corretto!")
			return 1
		end if

		ls_cod_deposito = this.getitemstring(li_i, "cod_deposito")
		ls_cod_deposito_tras = this.getitemstring(li_i, "cod_deposito_tras")
		
		if ls_cod_deposito = ls_cod_deposito_tras then
			g_mb.error("Il deposito di destinazione coincide con il deposito di partenza!")
			return 1
		end if
	next
end if
end event

type det_2 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3255
integer height = 2036
long backcolor = 12632256
string text = "Destinazione"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_2 dw_2
end type

on det_2.create
this.dw_2=create dw_2
this.Control[]={this.dw_2}
end on

on det_2.destroy
destroy(this.dw_2)
end on

type dw_2 from uo_cs_xx_dw within det_2
integer x = 27
integer y = 12
integer width = 2926
integer height = 1360
integer taborder = 11
string dataobject = "d_tes_bol_ven_det_2_tv"
boolean border = false
end type

type det_3 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3255
integer height = 2036
long backcolor = 12632256
string text = "Trasporto / Totali"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_3 dw_3
end type

on det_3.create
this.dw_3=create dw_3
this.Control[]={this.dw_3}
end on

on det_3.destroy
destroy(this.dw_3)
end on

type dw_3 from uo_cs_xx_dw within det_3
integer x = 27
integer y = 12
integer width = 2743
integer height = 1624
integer taborder = 11
string dataobject = "d_tes_bol_ven_det_3_tv"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_aspetto_beni"
		window_open(w_aspetto_beni, 0)
		setitem(row, "aspetto_beni", s_cs_xx.parametri.parametro_s_1)

end choose
end event

type det_4 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 3255
integer height = 2036
long backcolor = 12632256
string text = "Note"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_4 dw_4
end type

on det_4.create
this.dw_4=create dw_4
this.Control[]={this.dw_4}
end on

on det_4.destroy
destroy(this.dw_4)
end on

type dw_4 from uo_cs_xx_dw within det_4
integer x = 27
integer y = 32
integer width = 3131
integer height = 1220
integer taborder = 11
string dataobject = "d_tes_bol_ven_det_4_tv"
boolean border = false
end type

