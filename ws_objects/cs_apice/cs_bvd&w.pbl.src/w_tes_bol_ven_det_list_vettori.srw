﻿$PBExportHeader$w_tes_bol_ven_det_list_vettori.srw
forward
global type w_tes_bol_ven_det_list_vettori from w_cs_xx_principale
end type
type st_1 from statictext within w_tes_bol_ven_det_list_vettori
end type
type cb_calcola from commandbutton within w_tes_bol_ven_det_list_vettori
end type
type dw_listino from uo_std_dw within w_tes_bol_ven_det_list_vettori
end type
end forward

global type w_tes_bol_ven_det_list_vettori from w_cs_xx_principale
integer width = 1906
integer height = 1520
string title = "Analisi Costi Trasporti"
st_1 st_1
cb_calcola cb_calcola
dw_listino dw_listino
end type
global w_tes_bol_ven_det_list_vettori w_tes_bol_ven_det_list_vettori

type variables
uo_cs_xx_dw luo_dw_parent
long il_anno, il_numero
string is_provincia_dest, is_cod_cliente, is_cod_tipo_bol_ven, is_cod_fornitore, is_flag_tipo_bol_ven


end variables

on w_tes_bol_ven_det_list_vettori.create
int iCurrent
call super::create
this.st_1=create st_1
this.cb_calcola=create cb_calcola
this.dw_listino=create dw_listino
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.cb_calcola
this.Control[iCurrent+3]=this.dw_listino
end on

on w_tes_bol_ven_det_list_vettori.destroy
call super::destroy
destroy(this.st_1)
destroy(this.cb_calcola)
destroy(this.dw_listino)
end on

event pc_setwindow;call super::pc_setwindow;string ls_flag_tipo_bol_ven
luo_dw_parent = create uo_cs_xx_dw
luo_dw_parent = i_openparm

il_anno = luo_dw_parent.getitemnumber(luo_dw_parent.getrow(),"anno_registrazione")
il_numero = luo_dw_parent.getitemnumber(luo_dw_parent.getrow(),"num_registrazione")
is_provincia_dest = luo_dw_parent.getitemstring(luo_dw_parent.getrow(),"provincia")
is_cod_tipo_bol_ven = luo_dw_parent.getitemstring(luo_dw_parent.getrow(),"cod_tipo_bol_ven")

if isnull(is_provincia_dest) or len(is_provincia_dest) < 1 then
	
	select flag_tipo_bol_ven
	into   :ls_flag_tipo_bol_ven
	from   tab_tipi_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_bol_ven = :is_cod_tipo_bol_ven;
	
	choose case ls_flag_tipo_bol_ven
		case "V", "T"
				is_cod_cliente = luo_dw_parent.getitemstring(luo_dw_parent.getrow(),"cod_cliente")
				
				select provincia
				into   :is_provincia_dest
				from   anag_clienti
				where  cod_azienda = :s_cs_xx.cod_azienda and
					 	 cod_cliente = :is_cod_cliente;
		case "R", "C"
				is_cod_fornitore = luo_dw_parent.getitemstring(luo_dw_parent.getrow(),"cod_fornitore")
				
				select provincia
				into   :is_provincia_dest
				from   anag_fornitori
				where  cod_azienda = :s_cs_xx.cod_azienda and
					 	 cod_fornitore = :is_cod_fornitore;
				
	

	end choose
end if

if isnull(is_provincia_dest) or len(is_provincia_dest) < 1 then
	cb_calcola.enabled = false
else
	st_1.text = "Simulazione costi spedizione per DEST=" + is_provincia_dest
end if
end event

type st_1 from statictext within w_tes_bol_ven_det_list_vettori
integer x = 27
integer y = 1312
integer width = 1435
integer height = 76
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_calcola from commandbutton within w_tes_bol_ven_det_list_vettori
integer x = 1481
integer y = 1312
integer width = 361
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Calcola"
end type

event clicked;long ll_row
dec{4} ld_costo_trasporto
string ls_errore, ls_cod_vettore, ls_rag_soc_1
uo_calcola_documento_euro uo_calcola

uo_calcola = create uo_calcola_documento_euro

uo_calcola.il_anno_registrazione = il_anno
uo_calcola.il_num_registrazione  = il_numero
uo_calcola.is_tipo_documento     = "bol_ven"


declare cu_vettori cursor for
select cod_vettore, rag_soc_1
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and flag_blocco = 'N';

open cu_vettori;

dw_listino.reset()

do while true
	
	fetch cu_vettori into :ls_cod_vettore, :ls_rag_soc_1;
	if sqlca.sqlcode = 100 then exit
	if sqlca.sqlcode = -1 then exit
	
	ld_costo_trasporto = 0
	ls_errore = ""
	
	uo_calcola.uof_calcola_costo_trasporto(ls_cod_vettore, "", ld_costo_trasporto, ls_errore)
	
	ll_row = dw_listino.insertrow(0)
	
	dw_listino.setitem(ll_row, "vettore", ls_cod_vettore + " - " + ls_rag_soc_1)
	dw_listino.setitem(ll_row, "costo", ld_costo_trasporto)
	
loop

destroy uo_calcola

end event

type dw_listino from uo_std_dw within w_tes_bol_ven_det_list_vettori
integer x = 18
integer y = 16
integer width = 1829
integer height = 1276
integer taborder = 10
string dataobject = "d_tes_bol_ven_det_listino_vettori"
boolean vscrollbar = true
end type

