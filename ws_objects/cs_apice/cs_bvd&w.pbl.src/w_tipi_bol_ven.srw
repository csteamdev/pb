﻿$PBExportHeader$w_tipi_bol_ven.srw
$PBExportComments$Finestra Gestione Tipi Bolle Vendita
forward
global type w_tipi_bol_ven from w_cs_xx_principale
end type
type dw_tipi_bol_ven_lista from uo_cs_xx_dw within w_tipi_bol_ven
end type
type dw_tipi_bol_ven_det from uo_cs_xx_dw within w_tipi_bol_ven
end type
end forward

global type w_tipi_bol_ven from w_cs_xx_principale
integer width = 2555
integer height = 2360
string title = "Gestione Tipi Bolle Vendita"
dw_tipi_bol_ven_lista dw_tipi_bol_ven_lista
dw_tipi_bol_ven_det dw_tipi_bol_ven_det
end type
global w_tipi_bol_ven w_tipi_bol_ven

forward prototypes
public subroutine wf_abilita_flag_riferimento (string fs_flag_riferimento)
end prototypes

public subroutine wf_abilita_flag_riferimento (string fs_flag_riferimento);if fs_flag_riferimento = "N" then
	dw_tipi_bol_ven_det.object.cod_tipo_det_ven_rif_ord_ven.protect = 1
	dw_tipi_bol_ven_det.object.cod_tipo_det_ven_rif_ord_ven.border = 6
	dw_tipi_bol_ven_det.object.cod_tipo_det_ven_rif_ord_ven.background.mode = 1

	dw_tipi_bol_ven_det.object.des_riferimento_ord_ven.protect = 1
	dw_tipi_bol_ven_det.object.des_riferimento_ord_ven.border = 6
	dw_tipi_bol_ven_det.object.des_riferimento_ord_ven.background.mode = 1

	dw_tipi_bol_ven_det.object.flag_rif_anno_ord_ven.protect = 1
	dw_tipi_bol_ven_det.object.flag_rif_num_ord_ven.protect = 1
	dw_tipi_bol_ven_det.object.flag_rif_data_ord_ven.protect = 1
	
else
	dw_tipi_bol_ven_det.object.cod_tipo_det_ven_rif_ord_ven.protect = 0
	dw_tipi_bol_ven_det.object.cod_tipo_det_ven_rif_ord_ven.border = 5
	dw_tipi_bol_ven_det.object.cod_tipo_det_ven_rif_ord_ven.background.mode = 0

	dw_tipi_bol_ven_det.object.des_riferimento_ord_ven.protect = 0
	dw_tipi_bol_ven_det.object.des_riferimento_ord_ven.border = 5
	dw_tipi_bol_ven_det.object.des_riferimento_ord_ven.background.mode = 0

	dw_tipi_bol_ven_det.object.flag_rif_anno_ord_ven.protect = 0
	dw_tipi_bol_ven_det.object.flag_rif_num_ord_ven.protect = 0
	dw_tipi_bol_ven_det.object.flag_rif_data_ord_ven.protect = 0
end if


RETURN
end subroutine

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_tipi_bol_ven_lista.set_dw_key("cod_azienda")
dw_tipi_bol_ven_lista.set_dw_options(sqlca, &
                                     pcca.null_object, &
                                     c_default, &
                                     c_default)
dw_tipi_bol_ven_det.set_dw_options(sqlca, &
                                   dw_tipi_bol_ven_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)
iuo_dw_main = dw_tipi_bol_ven_lista
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tipi_bol_ven_det, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_bol_ven_det, &
                 "cod_documento", &
                 sqlca, &
                 "tab_documenti", &
                 "cod_documento", &
                 "des_documento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tipi_bol_ven_det, &
                 "cod_tipo_analisi", &
                 sqlca, &
                 "tab_tipi_analisi", &
                 "cod_tipo_analisi", &
                 "des_tipo_analisi", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_bol_ven_det, &
                 "cod_tipo_fat_ven", &
                 sqlca, &
                 "tab_tipi_fat_ven", &
                 "cod_tipo_fat_ven", &
                 "des_tipo_fat_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and (flag_tipo_fat_ven = 'D' or flag_tipo_fat_ven = 'P' or flag_tipo_fat_ven = 'F')")
f_po_loaddddw_dw(dw_tipi_bol_ven_det, &
                 "cod_tipo_det_ven_rif_ord_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_bol_ven_det, &
                 "cod_causale", &
                 sqlca, &
                 "tab_causali_trasp", &
                 "cod_causale", &
                 "des_causale", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

//donato 25/07/2012: aggiunta selezione tipo commessa da generare sulle righe ordine ddt ------------------------------------------------
//usata per i ddt di trasf.interno con tipo det TRI e prodotto con politica riordino '002'
f_po_loaddddw_dw(dw_tipi_bol_ven_det, &
                 "cod_tipo_commessa", &
                 sqlca, &
                 "tab_tipi_commessa", &
                 "cod_tipo_commessa", &
                 "des_tipo_commessa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//-------------------------------------------------------------------------------------------------------------------------------------------------------
end event

on w_tipi_bol_ven.create
int iCurrent
call super::create
this.dw_tipi_bol_ven_lista=create dw_tipi_bol_ven_lista
this.dw_tipi_bol_ven_det=create dw_tipi_bol_ven_det
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_bol_ven_lista
this.Control[iCurrent+2]=this.dw_tipi_bol_ven_det
end on

on w_tipi_bol_ven.destroy
call super::destroy
destroy(this.dw_tipi_bol_ven_lista)
destroy(this.dw_tipi_bol_ven_det)
end on

type dw_tipi_bol_ven_lista from uo_cs_xx_dw within w_tipi_bol_ven
integer x = 23
integer y = 20
integer width = 2469
integer height = 500
integer taborder = 10
string dataobject = "d_tipi_bol_ven_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event updatestart;call super::updatestart;integer li_i
string ls_cod_documento, ls_numeratore_documento

for li_i = 1 to rowcount(this)
	ls_cod_documento = this.getitemstring(li_i, "cod_documento")
	ls_numeratore_documento = this.getitemstring(li_i, "numeratore_documento")

	if not f_numeratore(ls_numeratore_documento, ls_cod_documento) then
		g_mb.messagebox("Attenzione", "Numeratore non corretto.", exclamation!)
		return 1
	end if
next
end event

event pcd_new;call super::pcd_new;dw_tipi_bol_ven_det.setitem(dw_tipi_bol_ven_lista.getrow(), "flag_doc_suc", "S")
dw_tipi_bol_ven_det.setitem(dw_tipi_bol_ven_lista.getrow(), "flag_st_note", "S")
end event

type dw_tipi_bol_ven_det from uo_cs_xx_dw within w_tipi_bol_ven
integer x = 23
integer y = 540
integer width = 2469
integer height = 1708
integer taborder = 20
string dataobject = "d_tipi_bol_ven_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;date ld_nulla

time lt_nulla


setnull(ld_nulla)
setnull(lt_nulla)

if i_extendmode then
	
	if i_colname = "flag_abilita_rif_ord_ven" then
		wf_abilita_flag_riferimento(i_coltext)
	end if
	
	if i_colname = "flag_stampa_data_ora" then
		if i_coltext <> "F" then
			setitem(i_rownbr, "data_fissa", ld_nulla)
			setitem(i_rownbr, "ora_fissa", lt_nulla)
		end if
	end if
	
end if


	
	
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_moduli"
		window_open_parm(w_tipi_bol_ven_lingue_dw, -1, dw_tipi_bol_ven_lista)
		
end choose
end event

