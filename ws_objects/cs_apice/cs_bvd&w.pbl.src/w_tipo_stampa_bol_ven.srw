﻿$PBExportHeader$w_tipo_stampa_bol_ven.srw
$PBExportComments$Finestra Tipo Stampa Bolla Vendita
forward
global type w_tipo_stampa_bol_ven from w_cs_xx_risposta
end type
type gb_1 from groupbox within w_tipo_stampa_bol_ven
end type
type rb_prova from radiobutton within w_tipo_stampa_bol_ven
end type
type rb_definitiva from radiobutton within w_tipo_stampa_bol_ven
end type
type cb_annulla from commandbutton within w_tipo_stampa_bol_ven
end type
type cb_ok from commandbutton within w_tipo_stampa_bol_ven
end type
type cbx_conferma from checkbox within w_tipo_stampa_bol_ven
end type
end forward

global type w_tipo_stampa_bol_ven from w_cs_xx_risposta
integer width = 855
integer height = 440
string title = "Tipo Stampa"
boolean controlmenu = false
boolean resizable = false
gb_1 gb_1
rb_prova rb_prova
rb_definitiva rb_definitiva
cb_annulla cb_annulla
cb_ok cb_ok
cbx_conferma cbx_conferma
end type
global w_tipo_stampa_bol_ven w_tipo_stampa_bol_ven

forward prototypes
public function integer wf_controlla_buchi (string as_cod_documento, string as_numeratore_documento, long al_anno_esercizio, long al_num_documento, datetime adt_oggi)
end prototypes

public function integer wf_controlla_buchi (string as_cod_documento, string as_numeratore_documento, long al_anno_esercizio, long al_num_documento, datetime adt_oggi);string ls_select, ls_errore
long ll_i, ll_rows, ll_numero_bolla, ll_numero_bolla_prec
datetime ldt_data_bolla, ldt_data_bolla_prec
datastore lds_bolle

ls_select = "select num_documento, data_bolla from tes_bol_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_documento = " + string(al_anno_esercizio) + " and cod_documento='" + as_cod_documento + "' and numeratore_documento='" + as_numeratore_documento + "' order by num_documento, data_bolla "

if guo_functions.uof_crea_datastore ( ref lds_bolle,  ls_select ) < 0 then
	g_mb.error("Errore nella creazione del datastore nella funzione di controllo buchi")
	return 0
end if

ll_rows = lds_bolle.rowcount()

for ll_i = 1 to ll_rows
	
	ll_numero_bolla = lds_bolle.getitemnumber(ll_i, 1)
	ldt_data_bolla = lds_bolle.getitemdatetime(ll_i, 2)
	
	if ll_i = 1 then
		ll_numero_bolla_prec = ll_numero_bolla
		ldt_data_bolla_prec = ldt_data_bolla
	else
		if (ll_numero_bolla - ll_numero_bolla_prec) <> 1 then
			ls_errore = "Attenzione!! Non vi è congruenza di numerazione nella sequenza bolle nella serie bolle " + as_cod_documento + " / " + as_numeratore_documento + " fra il numero DDT " + string(ll_numero_bolla_prec) + " ed il numero " + string(ll_numero_bolla)
			f_scrivi_log(ls_errore)
			if g_mb.messagebox("Apice", ls_errore + " PROSEGUO LO STESSO ?", Question!, YesNo!, 2) = 2 then
				destroy lds_bolle
				return -1
			end if
		end if
		
		if ldt_data_bolla < ldt_data_bolla_prec then
			ls_errore = "Attenzione!! Non vi è congruenza di date nella sequenza bolle nella serie bolle " + as_cod_documento + " / " + as_numeratore_documento + " fra il numero DDT " + string(ll_numero_bolla_prec) + " ed il numero " + string(ll_numero_bolla)
			f_scrivi_log(ls_errore)
			if g_mb.messagebox("Apice", ls_errore + " PROSEGUO LO STESSO ?", Question!, YesNo!, 2) = 2 then
				destroy lds_bolle
				return -1
			end if
		end if
		
		ll_numero_bolla_prec = ll_numero_bolla
		ldt_data_bolla_prec = ldt_data_bolla
		
	end if
	
next

destroy lds_bolle

return 0
end function

on w_tipo_stampa_bol_ven.create
int iCurrent
call super::create
this.gb_1=create gb_1
this.rb_prova=create rb_prova
this.rb_definitiva=create rb_definitiva
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.cbx_conferma=create cbx_conferma
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.gb_1
this.Control[iCurrent+2]=this.rb_prova
this.Control[iCurrent+3]=this.rb_definitiva
this.Control[iCurrent+4]=this.cb_annulla
this.Control[iCurrent+5]=this.cb_ok
this.Control[iCurrent+6]=this.cbx_conferma
end on

on w_tipo_stampa_bol_ven.destroy
call super::destroy
destroy(this.gb_1)
destroy(this.rb_prova)
destroy(this.rb_definitiva)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.cbx_conferma)
end on

type gb_1 from groupbox within w_tipo_stampa_bol_ven
integer x = 23
integer y = 20
integer width = 800
integer height = 220
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tipo Stampa"
end type

type rb_prova from radiobutton within w_tipo_stampa_bol_ven
integer x = 46
integer y = 80
integer width = 361
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Prova"
boolean checked = true
boolean lefttext = true
end type

event clicked;cbx_conferma.enabled = false
cbx_conferma.checked = false
end event

type rb_definitiva from radiobutton within w_tipo_stampa_bol_ven
integer x = 434
integer y = 80
integer width = 361
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Definitiva"
boolean lefttext = true
end type

event clicked;cbx_conferma.enabled = true
end event

type cb_annulla from commandbutton within w_tipo_stampa_bol_ven
integer x = 46
integer y = 260
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = -1

close(parent)
end event

type cb_ok from commandbutton within w_tipo_stampa_bol_ven
integer x = 434
integer y = 260
integer width = 366
integer height = 80
integer taborder = 3
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;long							ll_num_documento, ll_anno_esercizio, ll_cont

string							ls_cod_tipo_bol_ven, ls_cod_deposito, ls_cod_documento, ls_numeratore_documento, ls_errore, &
								ls_flag_blocco, ls_flag_fuori_fido, ls_tes_tabella, ls_det_tabella, ls_prog_riga, &
								ls_quantita, ls_messaggio, ls_flag_tipo_bol_ven, ls_cod_causale, ls_cod_contatto, ls_flag_segue_fatture
								
datetime ldt_oggi

uo_generazione_documenti		luo_gen_doc

integer						li_ret



ldt_oggi = datetime(today())
ll_anno_esercizio = f_anno_esercizio()

select cod_tipo_bol_ven,
		cod_deposito,
		flag_blocco,
		flag_fuori_fido,
		cod_causale,
		cod_contatto
into	:ls_cod_tipo_bol_ven,
		:ls_cod_deposito,
		:ls_flag_blocco,
		:ls_flag_fuori_fido,
		:ls_cod_causale,
		:ls_cod_contatto
from   tes_bol_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
		 num_registrazione = :s_cs_xx.parametri.parametro_d_2;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Attenzione errore in ricerca bolla")
	return
end if

if rb_definitiva.checked then
	if ls_flag_blocco = 'S' then
		g_mb.messagebox("Attenzione", "Bolla Bloccata non stampabile in definitivo.", exclamation!)
		s_cs_xx.parametri.parametro_i_1 = -1
		rollback;
		close(parent)
		return
	end if
	
	if ls_flag_fuori_fido = 'S' then
		g_mb.messagebox("Attenzione", "Bolla con Cliente Fuori Fido non stampabile in definitivo.", exclamation!)
		s_cs_xx.parametri.parametro_i_1 = -1
		rollback;
		close(parent)
		return
	end if
	
	// stefanop: 09/02/2012se la bolla ha un contatto assegnato allora vuol dire che la bolla
	// è di tipo omaggio e devo controllare la causale se ha il flag_segue_fattura impostato
	if not isnull(ls_cod_contatto) and ls_cod_contatto <> "" then
		select flag_segue_fattura
		into :ls_flag_segue_fatture
		from tab_causali_trasp
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_causale = :ls_cod_causale;
				
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in controllo del flag segue fattura in Causali Trasporto.", sqlca)
			s_cs_xx.parametri.parametro_i_1 = -1
			rollback;
			close(parent)
			return
		end if
		
		if ls_flag_segue_fatture = "S" then
			g_mb.warning("La bolla che si sta per stampare è per un Contatto; la causale del trasporto non può essere fatturabile.")
			return
		end if
	end if
	// ----
	
	select anag_depositi.cod_documento,   
          anag_depositi.numeratore_documento  
   into   :ls_cod_documento,   
          :ls_numeratore_documento  
   from   anag_depositi  
   where  anag_depositi.cod_azienda = :s_cs_xx.cod_azienda and
          anag_depositi.cod_deposito = :ls_cod_deposito;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Errore durante la lettura dell'Anagrafica Depositi.", exclamation!)
		s_cs_xx.parametri.parametro_i_1 = -1
		rollback;
		close(parent)
		return
	end if

	if isnull(ls_cod_documento) then
		select tab_tipi_bol_ven.cod_documento,   
             tab_tipi_bol_ven.numeratore_documento
	   into   :ls_cod_documento,   
             :ls_numeratore_documento  
      from   tab_tipi_bol_ven
      where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and
             tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Errore durante la lettura della tabella tipi bolle.", exclamation!)
			s_cs_xx.parametri.parametro_i_1 = -1
			rollback;
			close(parent)
			return
		end if
	end if

	select max(numeratori.num_documento)
	into   :ll_num_documento  
	from   numeratori  
	where  numeratori.cod_azienda = :s_cs_xx.cod_azienda and
			 numeratori.cod_documento = :ls_cod_documento and
			 numeratori.numeratore_documento = :ls_numeratore_documento and
			 numeratori.anno_documento = :ll_anno_esercizio;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Attenzione", "Errore durante la lettura della tabella numeratori.", exclamation!)
		s_cs_xx.parametri.parametro_i_1 = -1
		rollback;
		close(parent)
		return
	elseif isnull(ll_num_documento) then
		ll_num_documento  = 1
		ll_cont = 0
		
		select count(*)
		into   :ll_cont
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       	cod_documento = :ls_cod_documento and
				 numeratore_documento = :ls_numeratore_documento and
				 anno_documento = :ll_anno_esercizio and
				 num_documento = :ll_num_documento;
		if ll_cont > 0 and not isnull(ll_cont) then
			if g_mb.messagebox("APICE","Attenzione: numeratore già usato in un'altra bolla.~r~nAccetto ???",Question!,YesNo!,2) = 2 then
				s_cs_xx.parametri.parametro_i_1 = -1
				rollback;
				close(parent)
				return
			end if		
		end if
		
		// A questo punto ho ottenuto il numero bolla.
		// Per sicurezza vado a verificare che non vi siano buchi nella numerazione della serie ed eventualmente lo segnalo all'utente
		// Controllo anche 
		
		
//		if wf_controlla_buchi(ls_cod_documento, ls_numeratore_documento, ll_anno_esercizio, ll_num_documento, ldt_oggi) < 0 then
//			s_cs_xx.parametri.parametro_i_1 = -1
//			rollback;
//			close(parent)
//			return
//		end if
		
		luo_gen_doc = create uo_generazione_documenti
		li_ret = luo_gen_doc.uof_controlla_buchi("bol_ven", ls_cod_documento,ls_numeratore_documento, ll_anno_esercizio, ll_num_documento, ldt_oggi, ls_errore)
		destroy luo_gen_doc
		
		if li_ret<0 then
			//errore critico nel controllo
			rollback;
			g_mb.error(ls_errore)
			s_cs_xx.parametri.parametro_i_1 = -1
			close(parent)
			return
		
		elseif li_ret = 1 then
			//hai scelto di annullare il proseguo al seguito di incongruenze in numerazione consecutiva o data bolla
			s_cs_xx.parametri.parametro_i_1 = -1
			rollback;
			close(parent)
			return
		end if
		
		
		insert into numeratori 
						(cod_azienda,   
						 cod_documento,   
						 numeratore_documento,   
						 anno_documento,   
						 num_documento)  
		values 		(:s_cs_xx.cod_azienda,   
						 :ls_cod_documento,   
						 :ls_numeratore_documento,   
						 :ll_anno_esercizio,   
						 :ll_num_documento);

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Errore durante l'inserimento del numeratore.", &
						  exclamation!)
			s_cs_xx.parametri.parametro_i_1 = -1
			rollback;
			close(parent)
			return
		end if
		
	elseif not isnull(ll_num_documento) then
		
		ll_num_documento ++
		ll_cont = 0
		
		select count(*)
		into   :ll_cont
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		      	 cod_documento = :ls_cod_documento and
				 numeratore_documento = :ls_numeratore_documento and
				 anno_documento = :ll_anno_esercizio and
				 num_documento = :ll_num_documento;
		if ll_cont > 0 and not isnull(ll_cont) then
			if g_mb.messagebox("APICE","Attenzione: numeratore già usato in un'altra bolla.~r~nAccetto ???",Question!,YesNo!,2) = 2 then
				s_cs_xx.parametri.parametro_i_1 = -1
				rollback;
				close(parent)
				return
			end if		
		end if
		
		// A questo punto ho ottenuto il numero bolla.
		// Per sicurezza vado a verificare che non vi siano buchi nella numerazione della serie ed eventualmente lo segnalo all'utente
		// Controllo anche
	
//		if wf_controlla_buchi(ls_cod_documento, ls_numeratore_documento, ll_anno_esercizio, ll_num_documento, ldt_oggi) < 0 then
//			s_cs_xx.parametri.parametro_i_1 = -1
//			rollback;
//			close(parent)
//			return
//		end if

		luo_gen_doc = create uo_generazione_documenti
		li_ret = luo_gen_doc.uof_controlla_buchi("bol_ven", ls_cod_documento,ls_numeratore_documento, ll_anno_esercizio, ll_num_documento, ldt_oggi, ls_errore)
		destroy luo_gen_doc

		if li_ret<0 then
			//errore critico nel controllo
			rollback;
			g_mb.error(ls_errore)
			s_cs_xx.parametri.parametro_i_1 = -1
			close(parent)
			return
		
		elseif li_ret = 1 then
			//hai scelto di annullare il proseguo al seguito di incongruenze in numerazione consecutiva o data bolla
			s_cs_xx.parametri.parametro_i_1 = -1
			rollback;
			close(parent)
			return
		end if
		
		update numeratori
		set num_documento = :ll_num_documento  
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_documento = :ls_cod_documento and
				 anno_documento = :ll_anno_esercizio and
				 numeratore_documento = :ls_numeratore_documento;
	
		if sqlca.sqlcode <> 0 then
			g_mb.error("Errore durante l'aggiornamento della tabella numeratori.", 	sqlca)
			s_cs_xx.parametri.parametro_i_1 = -1
			rollback;
			close(parent)
			return
		end if
	end if

	update tes_bol_ven
	set    cod_documento = :ls_cod_documento,
			 numeratore_documento = :ls_numeratore_documento,
			 anno_documento = :ll_anno_esercizio,
			 num_documento = :ll_num_documento,
			 data_bolla = :ldt_oggi
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
			 num_registrazione = :s_cs_xx.parametri.parametro_d_2;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore durante impostazione numero documento in tes_bol_ven. Dettaglio:"+sqlca.sqlerrtext)
		s_cs_xx.parametri.parametro_i_1 = -1
		rollback;
		close(parent)
		return
	end if
	
	f_scrivi_log("Assegnazione numero fiscale al DDT VENDITA [" + string(s_cs_xx.parametri.parametro_d_1) + "/" + string(s_cs_xx.parametri.parametro_d_2) + "]  FISCALE: "&
					+ ls_cod_documento + "-" +  ls_numeratore_documento + "/" +  string(ll_anno_esercizio) + "-" + string(ll_num_documento) )
	

	ls_tes_tabella = "tes_bol_ven"
	ls_det_tabella = "det_bol_ven"
	ls_prog_riga = "prog_riga_bol_ven"
	ls_quantita = "quan_consegnata"

	if cbx_conferma.checked = true then
		if f_conferma_mov_mag(ls_tes_tabella, &
									 ls_det_tabella, &
									 ls_prog_riga, &
									 ls_quantita, &
									 s_cs_xx.parametri.parametro_d_1, &
									 s_cs_xx.parametri.parametro_d_2, &
									 ls_messaggio) = -1 then
			g_mb.messagebox("Conferma Bolla","Errore durante conferma bolla ~r~nDettaglio errore: " + ls_messaggio,StopSign!)
			s_cs_xx.parametri.parametro_i_1 = -1
			rollback;
			close(parent)
			return
		end if
		
		update tes_bol_ven
		set    flag_movimenti = 'S'
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
				 num_registrazione = :s_cs_xx.parametri.parametro_d_2;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore durante impostazione flag_movimenti in tes_bol_ven. Dettaglio:"+sqlca.sqlerrtext)
			s_cs_xx.parametri.parametro_i_1 = -1
			rollback;
			close(parent)
			return
		end if
	end if

	commit;

end if

s_cs_xx.parametri.parametro_i_1 = 0

close(parent)
end event

type cbx_conferma from checkbox within w_tipo_stampa_bol_ven
integer x = 434
integer y = 160
integer width = 366
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Conferma"
boolean lefttext = true
end type

