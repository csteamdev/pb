﻿$PBExportHeader$w_acq_ven_corr.srw
$PBExportComments$Finestra Gestione Corrispondenze di Acquisto/Vendita
forward
global type w_acq_ven_corr from w_cs_xx_principale
end type
type dw_acq_ven_corr_lista from uo_cs_xx_dw within w_acq_ven_corr
end type
type cb_note_esterne from commandbutton within w_acq_ven_corr
end type
type dw_acq_ven_corr_det_1 from uo_cs_xx_dw within w_acq_ven_corr
end type
type dw_acq_ven_corr_det_2 from uo_cs_xx_dw within w_acq_ven_corr
end type
type dw_folder from u_folder within w_acq_ven_corr
end type
end forward

global type w_acq_ven_corr from w_cs_xx_principale
integer width = 2414
integer height = 1540
dw_acq_ven_corr_lista dw_acq_ven_corr_lista
cb_note_esterne cb_note_esterne
dw_acq_ven_corr_det_1 dw_acq_ven_corr_det_1
dw_acq_ven_corr_det_2 dw_acq_ven_corr_det_2
dw_folder dw_folder
end type
global w_acq_ven_corr w_acq_ven_corr

type variables
string is_tipo_gestione
boolean ib_in_new
integer ii_new_row
end variables

event pc_setwindow;call super::pc_setwindow;is_tipo_gestione = s_cs_xx.parametri.parametro_s_1

windowobject l_objects[ ]

l_objects[1] = dw_acq_ven_corr_det_1
dw_folder.fu_AssignTab(1, "&Dettaglio", l_Objects[])

l_objects[1] = dw_acq_ven_corr_det_2
dw_folder.fu_AssignTab(2, "&Agenda", l_Objects[])

dw_folder.fu_FolderCreate(2,4)

choose case is_tipo_gestione
	case "Off_Ven"
		dw_acq_ven_corr_lista.dataobject = "d_off_ven_corr_lista"
		dw_acq_ven_corr_det_1.dataobject = "d_off_ven_corr_det_1"
		dw_acq_ven_corr_det_2.dataobject = "d_off_ven_corr_det_2"
		this.title = 'Corrispondenze Offerte di Vendita'
		dw_acq_ven_corr_lista.set_dw_key("anno_registrazione")
		dw_acq_ven_corr_lista.set_dw_key("num_registrazione")
	case "Off_Acq"
		dw_acq_ven_corr_lista.dataobject = "d_off_acq_corr_lista"
		dw_acq_ven_corr_det_1.dataobject = "d_off_acq_corr_det_1"
		dw_acq_ven_corr_det_2.dataobject = "d_off_acq_corr_det_2"
		this.title = 'Corrispondenze Offerte di Acquisto'
		dw_acq_ven_corr_lista.set_dw_key("anno_registrazione")
		dw_acq_ven_corr_lista.set_dw_key("num_registrazione")
	case "Ord_Ven"
		dw_acq_ven_corr_lista.dataobject = "d_ord_ven_corr_lista"
		dw_acq_ven_corr_det_1.dataobject = "d_ord_ven_corr_det_1"
		dw_acq_ven_corr_det_2.dataobject = "d_ord_ven_corr_det_2"
		this.title = 'Corrispondenze Ordini di Vendita'
		dw_acq_ven_corr_lista.set_dw_key("anno_registrazione")
		dw_acq_ven_corr_lista.set_dw_key("num_registrazione")
	case "Ord_Acq"
		dw_acq_ven_corr_lista.dataobject = "d_ord_acq_corr_lista"
		dw_acq_ven_corr_det_1.dataobject = "d_ord_acq_corr_det_1"
		dw_acq_ven_corr_det_2.dataobject = "d_ord_acq_corr_det_2"
		this.title = 'Corrispondenze Ordini di Acquisto'
		dw_acq_ven_corr_lista.set_dw_key("anno_registrazione")
		dw_acq_ven_corr_lista.set_dw_key("num_registrazione")
	case "Bol_Ven"
		dw_acq_ven_corr_lista.dataobject = "d_bol_ven_corr_lista"
		dw_acq_ven_corr_det_1.dataobject = "d_bol_ven_corr_det_1"
		dw_acq_ven_corr_det_2.dataobject = "d_bol_ven_corr_det_2"
		this.title = 'Corrispondenze Bolle di Vendita'
		dw_acq_ven_corr_lista.set_dw_key("anno_registrazione")
		dw_acq_ven_corr_lista.set_dw_key("num_registrazione")
	case "Bol_Acq"
		dw_acq_ven_corr_lista.dataobject = "d_bol_acq_corr_lista"
		dw_acq_ven_corr_det_1.dataobject = "d_bol_acq_corr_det_1"
		dw_acq_ven_corr_det_2.dataobject = "d_bol_acq_corr_det_2"
		this.title = 'Corrispondenze Bolle di Acquisto'
		dw_acq_ven_corr_lista.set_dw_key("anno_bolla_acq")
		dw_acq_ven_corr_lista.set_dw_key("num_bolla_acq")
	case "Fat_Ven"
		dw_acq_ven_corr_lista.dataobject = "d_fat_ven_corr_lista"
		dw_acq_ven_corr_det_1.dataobject = "d_fat_ven_corr_det_1"
		dw_acq_ven_corr_det_2.dataobject = "d_fat_ven_corr_det_2"
		this.title = 'Corrispondenze Fatture di Vendita'
		dw_acq_ven_corr_lista.set_dw_key("anno_registrazione")
		dw_acq_ven_corr_lista.set_dw_key("num_registrazione")
	case "Fat_Acq"
		dw_acq_ven_corr_lista.dataobject = "d_fat_acq_corr_lista"
		dw_acq_ven_corr_det_1.dataobject = "d_fat_acq_corr_det_1"
		dw_acq_ven_corr_det_2.dataobject = "d_fat_acq_corr_det_2"
		this.title = 'Corrispondenze Fatture di Acquisto'
		dw_acq_ven_corr_lista.set_dw_key("anno_registrazione")
		dw_acq_ven_corr_lista.set_dw_key("num_registraziione")
end choose

dw_acq_ven_corr_lista.set_dw_key("cod_azienda")
dw_acq_ven_corr_lista.set_dw_key("cod_corrispondenza")
dw_acq_ven_corr_lista.set_dw_key("data_corrispondenza")
dw_acq_ven_corr_lista.set_dw_key("ora_corrispondenza")
dw_acq_ven_corr_lista.set_dw_key("prog_corrispondenza")

dw_acq_ven_corr_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)
dw_acq_ven_corr_det_1.set_dw_options(sqlca,dw_acq_ven_corr_lista,c_sharedata+c_scrollparent,c_default)
dw_acq_ven_corr_det_2.set_dw_options(sqlca,dw_acq_ven_corr_lista,c_sharedata+c_scrollparent,c_default)

dw_folder.fu_SelectTab(1)
iuo_dw_main = dw_acq_ven_corr_lista
end event

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_acq_ven_corr_det_1,"cod_corrispondenza",sqlca,&
                 "tab_corrispondenze","cod_corrispondenza","des_corrispondenza",&
                 "tab_corrispondenze.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_acq_ven_corr_det_1,"num_reg_lista",sqlca,&
                 "tes_liste_controllo","num_reg_lista","des_lista",&
                 "(tes_liste_controllo.cod_azienda = '" + s_cs_xx.cod_azienda + "') and (tes_liste_controllo.flag_valido = 'S')" )
					  
f_PO_LoadDDDW_DW(dw_acq_ven_corr_det_2,"cod_operaio",sqlca,&
                 "anag_operai","cod_operaio","cognome + ' ' + nome",&
                 "anag_operai.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_acq_ven_corr_det_2,"cod_tipo_agenda",sqlca,&
                 "tab_tipi_agende","cod_tipo_agenda","des_tipo_agenda",&
                 "tab_tipi_agende.cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDLB_DW(dw_acq_ven_corr_det_2,"cod_utente",sqlca,&
                 "utenti","cod_utente","cod_utente",&
                 "utenti.cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
					  "((utenti.flag_blocco <> 'S') or (utenti.flag_blocco = 'S' and utenti.data_blocco > " + s_cs_xx.db_funzioni.oggi  + "))")

end event

on w_acq_ven_corr.create
int iCurrent
call super::create
this.dw_acq_ven_corr_lista=create dw_acq_ven_corr_lista
this.cb_note_esterne=create cb_note_esterne
this.dw_acq_ven_corr_det_1=create dw_acq_ven_corr_det_1
this.dw_acq_ven_corr_det_2=create dw_acq_ven_corr_det_2
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_acq_ven_corr_lista
this.Control[iCurrent+2]=this.cb_note_esterne
this.Control[iCurrent+3]=this.dw_acq_ven_corr_det_1
this.Control[iCurrent+4]=this.dw_acq_ven_corr_det_2
this.Control[iCurrent+5]=this.dw_folder
end on

on w_acq_ven_corr.destroy
call super::destroy
destroy(this.dw_acq_ven_corr_lista)
destroy(this.cb_note_esterne)
destroy(this.dw_acq_ven_corr_det_1)
destroy(this.dw_acq_ven_corr_det_2)
destroy(this.dw_folder)
end on

event pc_delete;call super::pc_delete;ib_in_new = false
integer li_row
li_row = dw_acq_ven_corr_lista.getrow()
if li_row <= 0 or dw_acq_ven_corr_lista.GetItemnumber(li_row,"prog_corrispondenza") = 0 then
	cb_note_esterne.enabled = false
else
	cb_note_esterne.enabled = true
end if
end event

type dw_acq_ven_corr_lista from uo_cs_xx_dw within w_acq_ven_corr
integer x = 18
integer y = 20
integer width = 2327
integer height = 500
integer taborder = 10
string dataobject = ""
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error, ll_num_registrazione, ll_num_doc_origine, ll_progressivo, ll_num_bolla_acq
integer li_anno_bolla_acq, li_anno_registrazione, li_anno_doc_origine
string ls_cod_doc_origine, ls_numeratore_doc_origine

if is_tipo_gestione = "Bol_Acq" then
	li_anno_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_bolla_acq")
	ll_num_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_bolla_acq")
	l_Error = Retrieve(s_cs_xx.cod_azienda, li_anno_bolla_acq, ll_num_bolla_acq)
elseif is_tipo_gestione = "Fat_Acq" then
	li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	l_Error = Retrieve(s_cs_xx.cod_azienda, li_anno_registrazione, ll_num_registrazione)
elseif is_tipo_gestione <> "Bol_Acq" and is_tipo_gestione <> "Fat_Acq" then
	li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	l_Error = Retrieve(s_cs_xx.cod_azienda, li_anno_registrazione, ll_num_registrazione)
end if	

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;integer li_anno_registrazione, li_anno_bolla_acq, li_anno_doc_origine
long ll_num_registrazione, l_Idx, ll_num_doc_origine, ll_progressivo, ll_num_bolla_acq
string ls_cod_doc_origine, ls_numeratore_doc_origine

FOR l_Idx = 1 TO RowCount()
	IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
		SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
	END IF
NEXT
	
if is_tipo_gestione = "Bol_Acq" then
	li_anno_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_bolla_acq")
	ll_num_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_bolla_acq")
	
	FOR l_Idx = 1 TO RowCount()
		IF IsNull(getitemnumber(l_Idx, "anno_bolla_acq")) or getitemnumber(l_Idx, "anno_bolla_acq") = 0 THEN
			SetItem(l_Idx, "anno_bolla_acq", li_anno_bolla_acq)
		END IF
	NEXT
	
	FOR l_Idx = 1 TO RowCount()
		IF IsNull(getitemnumber(l_Idx, "num_bolla_acq")) or getitemnumber(l_Idx, "num_bolla_acq") = 0 THEN
			SetItem(l_Idx, "num_bolla_acq", ll_num_bolla_acq)
		END IF
	NEXT

elseif is_tipo_gestione = "Fat_Acq" then
	li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	
	FOR l_Idx = 1 TO RowCount()
		IF IsNull(getitemnumber(l_Idx, "anno_registrazione")) or getitemnumber(l_Idx, "anno_registrazione") = 0 THEN
			SetItem(l_Idx, "anno_registrazione", li_anno_registrazione)
		END IF
	NEXT
	
	FOR l_Idx = 1 TO RowCount()
		IF IsNull(getitemnumber(l_Idx, "num_registrazione")) or getitemnumber(l_Idx, "num_registrazione") = 0 THEN
			SetItem(l_Idx, "num_registrazione", ll_num_registrazione)
		END IF
	NEXT
elseif is_tipo_gestione <> "Bol_Acq" and is_tipo_gestione <> "Fat_Acq" then
	li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	
	FOR l_Idx = 1 TO RowCount()
		IF IsNull(getitemnumber(l_Idx, "anno_registrazione")) or getitemnumber(l_Idx, "anno_registrazione") = 0 THEN
			SetItem(l_Idx, "anno_registrazione", li_anno_registrazione)
		END IF
	NEXT
	
	FOR l_Idx = 1 TO RowCount()
		IF IsNull(getitemnumber(l_Idx, "num_registrazione")) or getitemnumber(l_Idx, "num_registrazione") = 0 THEN
			SetItem(l_Idx, "num_registrazione", ll_num_registrazione)
		END IF
	NEXT
end if
end event

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false
ib_in_new = false
end event

event pcd_new;call super::pcd_new;string ls_str, ls_cod_doc_origine, ls_numeratore_doc_origine
long ll_anno_registrazione, ll_num_registrazione, ll_num_doc_origine, ll_progressivo, ll_num_bolla_acq
datetime ldt_oggi
time lt_ora
integer li_anno_bolla_acq, li_anno_doc_origine

if is_tipo_gestione = "Bol_Acq" then
	li_anno_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_bolla_acq")
	ll_num_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_bolla_acq")
	dw_acq_ven_corr_lista.setitem(getrow(),"anno_bolla_acq",li_anno_bolla_acq)
	dw_acq_ven_corr_lista.setitem(getrow(),"num_bolla_acq",ll_num_bolla_acq)
elseif is_tipo_gestione = "Fat_Acq" then
	ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	dw_acq_ven_corr_lista.setitem(getrow(),"anno_registrazione",ll_anno_registrazione)
	dw_acq_ven_corr_lista.setitem(getrow(),"num_registrazione",ll_num_registrazione)
elseif is_tipo_gestione <> "Bol_Acq" and is_tipo_gestione <> "Fat_Acq" then
	ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	dw_acq_ven_corr_lista.setitem(getrow(),"anno_registrazione",ll_anno_registrazione)
	dw_acq_ven_corr_lista.setitem(getrow(),"num_registrazione",ll_num_registrazione)
end if

cb_note_esterne.enabled = false
ib_in_new = true
ii_new_row = getrow()

ls_str = string(now(),"hh:mm")
ldt_oggi = datetime(date(s_cs_xx.db_funzioni.data_neutra), time(ls_str))
this.setitem(this.getrow(),"ora_corrispondenza",ldt_oggi)

ldt_oggi = datetime(today())
this.setitem(this.getrow(),"data_corrispondenza",ldt_oggi)
end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not GetItemnumber(li_row,"prog_corrispondenza") = 0 then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
ib_in_new = false
end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not GetItemnumber(li_row,"prog_corrispondenza") = 0 then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
ib_in_new = false
end event

event updatestart;call super::updatestart;integer li_anno_registrazione, li_i, li_anno_bolla_acq, li_anno_doc_origine
long ll_num_registrazione, ll_prog_corrispondenza, ll_num_doc_origine, ll_progressivo, ll_num_bolla_acq
string ls_cod_corrispondenza, ls_cod_tipo_agenda, ls_cod_utente, &
	    ls_cod_doc_origine, ls_numeratore_doc_origine
datetime ldt_data_corrispondenza, ldt_ora_corrispondenza, ldt_data_agenda, ldt_ora_agenda

if ib_in_new then
	if is_tipo_gestione = "Bol_Acq" then
		li_anno_bolla_acq = dw_acq_ven_corr_det_1.getitemnumber( dw_acq_ven_corr_det_1.getrow(), "anno_bolla_acq" )
		ll_num_bolla_acq = dw_acq_ven_corr_det_1.getitemnumber( dw_acq_ven_corr_det_1.getrow(), "num_bolla_acq" )
	elseif is_tipo_gestione = "Fat_Acq" then
		li_anno_registrazione = dw_acq_ven_corr_det_1.getitemnumber( dw_acq_ven_corr_det_1.getrow(), "anno_registrazione" )
		ll_num_registrazione = dw_acq_ven_corr_det_1.getitemnumber( dw_acq_ven_corr_det_1.getrow(), "num_registrazione" )
	elseif is_tipo_gestione <> "Bol_Acq" and is_tipo_gestione <> "Fat_Acq" then
		li_anno_registrazione = dw_acq_ven_corr_det_1.getitemnumber( dw_acq_ven_corr_det_1.getrow(), "anno_registrazione" )
		ll_num_registrazione = dw_acq_ven_corr_det_1.getitemnumber( dw_acq_ven_corr_det_1.getrow(), "num_registrazione" )
	end if
	ls_cod_corrispondenza = dw_acq_ven_corr_det_1.getitemstring( dw_acq_ven_corr_det_1.getrow(), "cod_corrispondenza" )
	ldt_data_corrispondenza = dw_acq_ven_corr_det_1.getitemdatetime( dw_acq_ven_corr_det_1.getrow(), "data_corrispondenza" )
	ldt_ora_corrispondenza = dw_acq_ven_corr_det_1.getitemdatetime( dw_acq_ven_corr_det_1.getrow(), "ora_corrispondenza" )
   
	choose case is_tipo_gestione
		case "Off_Ven"
		  select max(tes_off_ven_corrispondenze.prog_corrispondenza)
			  into :ll_prog_corrispondenza
			  from tes_off_ven_corrispondenze
			  where (tes_off_ven_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda) and 
				  (tes_off_ven_corrispondenze.anno_registrazione = :li_anno_registrazione) and
				  (tes_off_ven_corrispondenze.num_registrazione = :ll_num_registrazione) and
				  (tes_off_ven_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza) and
				  (tes_off_ven_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza) and
				  (tes_off_ven_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza);
		case "Off_Acq"
		  select max(tes_off_acq_corrispondenze.prog_corrispondenza)
			  into :ll_prog_corrispondenza
			  from tes_off_acq_corrispondenze
			  where (tes_off_acq_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda) and 
				  (tes_off_acq_corrispondenze.anno_registrazione = :li_anno_registrazione) and
				  (tes_off_acq_corrispondenze.num_registrazione = :ll_num_registrazione) and
				  (tes_off_acq_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza) and
				  (tes_off_acq_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza) and
				  (tes_off_acq_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza);
		case "Ord_Ven"
		  select max(tes_ord_ven_corrispondenze.prog_corrispondenza)
			  into :ll_prog_corrispondenza
			  from tes_ord_ven_corrispondenze
			  where (tes_ord_ven_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda) and 
				  (tes_ord_ven_corrispondenze.anno_registrazione = :li_anno_registrazione) and
				  (tes_ord_ven_corrispondenze.num_registrazione = :ll_num_registrazione) and
				  (tes_ord_ven_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza) and
				  (tes_ord_ven_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza) and
				  (tes_ord_ven_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza);
		case "Ord_Acq"
		  select max(tes_ord_acq_corrispondenze.prog_corrispondenza)
			  into :ll_prog_corrispondenza
			  from tes_ord_acq_corrispondenze
			  where (tes_ord_acq_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda) and 
				  (tes_ord_acq_corrispondenze.anno_registrazione = :li_anno_registrazione) and
				  (tes_ord_acq_corrispondenze.num_registrazione = :ll_num_registrazione) and
				  (tes_ord_acq_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza) and
				  (tes_ord_acq_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza) and
				  (tes_ord_acq_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza);
		case "Bol_Ven"
		  select max(tes_bol_ven_corrispondenze.prog_corrispondenza)
			  into :ll_prog_corrispondenza
			  from tes_bol_ven_corrispondenze
			  where (tes_bol_ven_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda) and 
				  (tes_bol_ven_corrispondenze.anno_registrazione = :li_anno_registrazione) and
				  (tes_bol_ven_corrispondenze.num_registrazione = :ll_num_registrazione) and
				  (tes_bol_ven_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza) and
				  (tes_bol_ven_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza) and
				  (tes_bol_ven_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza);
		case "Bol_Acq"
		  select max(tes_bol_acq_corrispondenze.prog_corrispondenza)
			  into :ll_prog_corrispondenza
			  from tes_bol_acq_corrispondenze
			  where (tes_bol_acq_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda) and 
				  (tes_bol_acq_corrispondenze.anno_bolla_acq = :li_anno_bolla_acq) and
				  (tes_bol_acq_corrispondenze.num_bolla_acq = :ll_num_bolla_acq) and
				  (tes_bol_acq_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza) and
				  (tes_bol_acq_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza) and
				  (tes_bol_acq_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza);
		case "Fat_Ven"
		  select max(tes_fat_ven_corrispondenze.prog_corrispondenza)
			  into :ll_prog_corrispondenza
			  from tes_fat_ven_corrispondenze
			  where (tes_fat_ven_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda) and 
				  (tes_fat_ven_corrispondenze.anno_registrazione = :li_anno_registrazione) and
				  (tes_fat_ven_corrispondenze.num_registrazione = :ll_num_registrazione) and
				  (tes_fat_ven_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza) and
				  (tes_fat_ven_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza) and
				  (tes_fat_ven_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza);
		case "Fat_Acq"
		  select max(tes_fat_acq_corrispondenze.prog_corrispondenza)
			  into :ll_prog_corrispondenza
			  from tes_fat_acq_corrispondenze
			  where (tes_fat_acq_corrispondenze.cod_azienda = :s_cs_xx.cod_azienda) and 
				  (tes_fat_acq_corrispondenze.anno_registrazione = :li_anno_registrazione) and
				  (tes_fat_acq_corrispondenze.num_registrazione = :ll_num_registrazione) and
				  (tes_fat_acq_corrispondenze.cod_corrispondenza = :ls_cod_corrispondenza) and
				  (tes_fat_acq_corrispondenze.data_corrispondenza = :ldt_data_corrispondenza) and
				  (tes_fat_acq_corrispondenze.ora_corrispondenza = :ldt_ora_corrispondenza);
	end choose
		
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode = -1) or isnull(ll_prog_corrispondenza) then
      ll_prog_corrispondenza = 1
   else
      ll_prog_corrispondenza = ll_prog_corrispondenza + 1
   end if
	if is_tipo_gestione = "Bol_Acq" then
		this.SetItem (this.GetRow ( ),"anno_bolla_acq", li_anno_bolla_acq)
		this.SetItem (this.GetRow ( ),"num_bolla_acq", ll_num_bolla_acq)
	elseif is_tipo_gestione = "Bol_Acq" then
		this.SetItem (this.GetRow ( ),"cod_doc_origine", ls_cod_doc_origine)
		this.SetItem (this.GetRow ( ),"numeratore_doc_origine", ls_numeratore_doc_origine)
		this.SetItem (this.GetRow ( ),"anno_doc_origine", li_anno_doc_origine)
		this.SetItem (this.GetRow ( ),"num_doc_origine", ll_num_doc_origine)
		this.SetItem (this.GetRow ( ),"progressivo", ll_progressivo)
	elseif is_tipo_gestione <> "Bol_Acq" and is_tipo_gestione <> "Fat_Acq" then
		this.SetItem (this.GetRow ( ),"anno_registrazione", li_anno_registrazione)
		this.SetItem (this.GetRow ( ),"num_registrazione", ll_num_registrazione)
	end if
   this.SetItem (this.GetRow ( ),"cod_corrispondenza", ls_cod_corrispondenza)
   this.SetItem (this.GetRow ( ),"data_corrispondenza", ldt_data_corrispondenza)
   this.SetItem (this.GetRow ( ),"ora_corrispondenza", ldt_ora_corrispondenza)
   this.SetItem (this.GetRow ( ),"prog_corrispondenza", ll_prog_corrispondenza)

   ib_in_new = false
end if
end event

event updateend;call super::updateend;boolean lb_nuovo_appuntamento = false
string ls_cod_tipo_agenda, ls_cod_utente, ls_sql, ls_tabella, ls_where, ls_cod_corrispondenza, &
		 ls_cod_doc_origine, ls_numeratore_doc_origine
long ll_i, ll_rows, ll_anno_registrazione, ll_num_registrazione, &
	  ll_num_doc_origine, ll_progressivo, ll_num_bolla_acq
datetime ldt_data, ldt_data_old, ldt_ora, ldt_ora_old, ldt_data_corrispondenza, &
         ldt_ora_corrispondenza, ldt_data_agenda, ldt_ora_agenda
integer li_anno_bolla_acq, li_anno_doc_origine
time lt_1

choose case is_tipo_gestione
	case "Off_Ven"
		ls_tabella = "tes_off_ven_corrispondenze"
	case "Off_Acq"
		ls_tabella = "tes_off_acq_corrispondenze"
	case "Ord_Ven"
		ls_tabella = "tes_ord_ven_corrispondenze"
	case "Ord_Acq"
		ls_tabella = "tes_ord_acq_corrispondenze"
	case "Bol_Ven"
		ls_tabella = "tes_bol_ven_corrispondenze"
	case "Bol_Acq"
		ls_tabella = "tes_bol_acq_corrispondenze"
	case "Fat_Ven"
		ls_tabella = "tes_fat_ven_corrispondenze"
	case "Fat_Acq"
		ls_tabella = "tes_fat_acq_corrispondenze"
end choose

ll_rows = rowcount()
if ll_rows < 1 then return
for ll_i = 1 to ll_rows
	ldt_data = getitemdatetime(ll_i,"data_pros_corrispondenza")
	ldt_data_old = getitemdatetime(ll_i,"data_pros_corrispondenza",primary!,true)
	lt_1 = time(getitemdatetime(ll_i,"ora_pros_corrispondenza"))
	ldt_ora = datetime(date(s_cs_xx.db_funzioni.data_neutra), lt_1)
	lt_1 = time(getitemdatetime(ll_i,"ora_pros_corrispondenza",primary!,true))
	ldt_ora_old = datetime(date(s_cs_xx.db_funzioni.data_neutra), lt_1)
	if is_tipo_gestione = "Bol_Acq" then
		li_anno_bolla_acq = getitemnumber(ll_i, "anno_bolla_acq")
		ll_num_bolla_acq = getitemnumber(ll_i, "num_bolla_acq")
	elseif is_tipo_gestione = "Fat_Acq" then
		ll_anno_registrazione = getitemnumber(ll_i, "anno_registrazione")
		ll_num_registrazione = getitemnumber(ll_i, "num_registrazione")
	elseif is_tipo_gestione <> "Bol_Acq" and is_tipo_gestione <> "Fat_Acq" then
		ll_anno_registrazione = getitemnumber(ll_i, "anno_registrazione")
		ll_num_registrazione = getitemnumber(ll_i, "num_registrazione")
	end if
	ls_cod_corrispondenza = getitemstring(ll_i, "cod_corrispondenza")
	ldt_data_corrispondenza = getitemdatetime(ll_i,"data_corrispondenza")
	ldt_ora_corrispondenza = getitemdatetime(ll_i,"ora_corrispondenza")

	if ll_i = ii_new_row and not(isnull(ldt_data)) and not(isnull(ldt_ora)) then
		if f_crea_appuntamento(ldt_data, ldt_ora, "", getitemstring(ll_i,"note"), ls_cod_tipo_agenda, ls_cod_utente) = 0 then
			ls_where   = " where cod_azienda = '" + s_cs_xx.cod_azienda + "' and "
			if is_tipo_gestione = "Bol_Acq" then
				ls_where = ls_where + " anno_bolla_acq = " + string(li_anno_bolla_acq) + " and " + & 
							             " num_bolla_acq = '" + string(ll_num_bolla_acq) + "' and "
			elseif is_tipo_gestione = "Fat_Acq" then
				ls_where = ls_where + " anno_registrazione = " + string(ll_anno_registrazione) + " and " + & 
							             " num_registrazione = " + string(ll_num_registrazione) + " and " 
			elseif is_tipo_gestione <> "Bol_Acq" and is_tipo_gestione <> "Fat_Acq" then
				ls_where = ls_where + " anno_registrazione = " + string(ll_anno_registrazione) + " and " + & 
							             " num_registrazione = " + string(ll_num_registrazione) + " and "
			end if
			ls_where = ls_where + " cod_corrispondenza = '" + ls_cod_corrispondenza + "' and " + & 
							 "      data_corrispondenza = '" + string(ldt_data_corrispondenza, s_cs_xx.db_funzioni.formato_data) + "' and " + & 
							 "      ora_corrispondenza = '" + string(ldt_ora_corrispondenza, "yyyy-mm-dd hh:mm") + "' "
							 
			ls_sql = "update " + ls_tabella + &
						" set cod_tipo_agenda = '" + ls_cod_tipo_agenda + "'," + &
						"    cod_utente = '" + ls_cod_utente + "'," + &
						"    data_agenda = '" + string(ldt_data,s_cs_xx.db_funzioni.formato_data) + "'," + &
						"    ora_agenda = '" + string(ldt_ora, "yyyy-mm-dd hh:mm") + "'" + &
						ls_where			
			EXECUTE IMMEDIATE :ls_sql ;								  
		end if	
	end if	
	
	if not(isnull(ldt_data)) or not(isnull(ldt_data_old)) then
		if not isnull(ldt_data_old) then
			if ldt_data_old <> ldt_data then
				// elimina appuntamento segnalando e verificando la nota se è uguale
			end if
		end if
		if not isnull(ldt_data) then
			if ldt_data_old <> ldt_data then
				if f_crea_appuntamento(ldt_data, ldt_ora, "",getitemstring(ll_i,"note"), ls_cod_tipo_agenda, ls_cod_utente) = 0 then
					ls_where=" where cod_azienda = '" + s_cs_xx.cod_azienda + "' and "
					if is_tipo_gestione = "Bol_Acq" then
						ls_where = ls_where + " anno_bolla_acq = " + string(li_anno_bolla_acq) + " and " + & 
													 " num_bolla_acq = '" + string(ll_num_bolla_acq) + "' and "
					elseif is_tipo_gestione = "Fat_Acq" then
						ls_where = ls_where + " anno_registrazione = " + string(ll_anno_registrazione) + " and " + & 
													 " num_registrazione = " + string(ll_num_registrazione) + " and " 													 
					elseif is_tipo_gestione <> "Bol_Acq" and is_tipo_gestione <> "Fat_Acq" then
						ls_where = ls_where + " anno_registrazione = " + string(ll_anno_registrazione) + " and " + & 
													 " num_registrazione = " + string(ll_num_registrazione) + " and "
					end if
					ls_where = ls_where + " cod_corrispondenza = '" + ls_cod_corrispondenza + "' and " + & 
								"      data_corrispondenza = '" + string(ldt_data_corrispondenza, s_cs_xx.db_funzioni.formato_data) + "' and " + & 
								"      ora_corrispondenza = '" + string(ldt_ora_corrispondenza, "yyyy-mm-dd hh:mm") + "' " 		
					ls_sql = "update " + ls_tabella + &
								" set cod_tipo_agenda = '" + ls_cod_tipo_agenda + "'," + &
								"    cod_utente = '" + ls_cod_utente + "'," + &
								"    data_agenda = '" + string(ldt_data,s_cs_xx.db_funzioni.formato_data) + "'," + &
								"    ora_agenda = '" + string(ldt_ora, "yyyy-mm-dd hh:mm") + "'" + &
								ls_where			
					EXECUTE IMMEDIATE :ls_sql ;								  
				end if
			else
				if ldt_ora_old <> ldt_ora then
				// cambia orario
			end if
			end if
		end if
	end if
next

setnull(ii_new_row)

for ll_i = 1 to this.deletedcount()
	ls_cod_tipo_agenda = this.getitemstring(ll_i, "cod_tipo_agenda", delete!, true)
	ls_cod_utente = this.getitemstring(ll_i, "cod_utente", delete!, true)
	ldt_data_agenda= this.getitemdatetime(ll_i, "data_agenda", delete!, true)		
	ldt_ora_agenda= this.getitemdatetime(ll_i, "ora_agenda", delete!, true)		
	if not isnull(ls_cod_tipo_agenda) then
	  delete
	  from  agende_corrispondenze  
	  where cod_azienda = :s_cs_xx.cod_azienda and
			  cod_tipo_agenda = :ls_cod_tipo_agenda and
			  cod_utente = :ls_cod_utente and
			  data_agenda = :ldt_data_agenda and
			  ora_agenda = :ldt_ora_agenda;
	  delete
	  from  agende_note  
	  where cod_azienda = :s_cs_xx.cod_azienda and
			  cod_tipo_agenda = :ls_cod_tipo_agenda and
			  cod_utente = :ls_cod_utente and
			  data_agenda = :ldt_data_agenda and
			  ora_agenda = :ldt_ora_agenda;
	  delete
	  from  agende_utenti
	  where cod_azienda = :s_cs_xx.cod_azienda and
			  cod_tipo_agenda = :ls_cod_tipo_agenda and
			  cod_utente = :ls_cod_utente and
			  data_agenda = :ldt_data_agenda and
			  ora_agenda = :ldt_ora_agenda;
	  delete
	  from  agende  
	  where cod_azienda = :s_cs_xx.cod_azienda and
			  cod_tipo_agenda = :ls_cod_tipo_agenda and
			  cod_utente = :ls_cod_utente and
			  data_agenda = :ldt_data_agenda and
			  ora_agenda = :ldt_ora_agenda;
	end if
next
end event

type cb_note_esterne from commandbutton within w_acq_ven_corr
event clicked pbm_bnclicked
integer x = 1966
integer y = 1340
integer width = 361
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;str_acq_ven_corr_ole lstr_ole

lstr_ole.tipo_gestione= upper(is_tipo_gestione)
lstr_ole.anno_registrazione = dw_acq_ven_corr_det_1.getitemnumber( dw_acq_ven_corr_det_1.getrow(), "anno_registrazione" )
lstr_ole.num_registrazione = dw_acq_ven_corr_det_1.getitemnumber( dw_acq_ven_corr_det_1.getrow(), "num_registrazione" )
lstr_ole.cod_corrispondenza = dw_acq_ven_corr_det_1.getitemstring( dw_acq_ven_corr_det_1.getrow(), "cod_corrispondenza" )
lstr_ole.data_corrispondenza = dw_acq_ven_corr_det_1.getitemdatetime( dw_acq_ven_corr_det_1.getrow(), "data_corrispondenza" )
lstr_ole.ora_corrispondenza = dw_acq_ven_corr_det_1.getitemdatetime( dw_acq_ven_corr_det_1.getrow(), "ora_corrispondenza" )
lstr_ole.prog_corrispondenza = dw_acq_ven_corr_det_1.getitemnumber( dw_acq_ven_corr_det_1.getrow(), "prog_corrispondenza" )

window_open_parm(w_acq_ven_corr_ole, 0, lstr_ole)

commit;

end event

type dw_acq_ven_corr_det_1 from uo_cs_xx_dw within w_acq_ven_corr
integer x = 41
integer y = 640
integer width = 2240
integer height = 656
integer taborder = 30
string dataobject = ""
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then

	string ls_str, ls_des_corrispondenza, ls_oggetto, ls_note, ls_gestione, ls_cod_corrispondenza, &
			 ls_reg, ls_cod_doc_origine, ls_numeratore_doc_origine 
	long ll_anno_registrazione, ll_num_registrazione, ll_anno_bolla_acq, ll_num_doc_origine, &
		  ll_progressivo, ll_num_bolla_acq
	integer li_anno_doc_origine
	
	if i_colname = "cod_corrispondenza" then
		ls_cod_corrispondenza = i_coltext
	else
		ls_cod_corrispondenza = getitemstring(row, "cod_corrispondenza")
	end if

	setnull(ls_des_corrispondenza)
	if not isnull(ls_cod_corrispondenza) then
		select des_corrispondenza
		into   :ls_des_corrispondenza
		from   tab_corrispondenze
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_corrispondenza = :ls_cod_corrispondenza;
		if sqlca.sqlcode <> 0 then
			setnull(ls_des_corrispondenza)
		end if
	end if
	
	if i_colname = "oggetto" then
		ls_oggetto = i_coltext
	else
		ls_oggetto = getitemstring(row, "oggetto")
	end if
	
//	if i_colname = "note" then
//		ls_note = i_coltext
//	else
//		ls_note = getitemstring(row, "note")
//	end if
	if is_tipo_gestione = "Bol_Acq" then
		ll_anno_bolla_acq = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_bolla_acq")
		ll_num_bolla_acq = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_bolla_acq")
		ls_reg =	string(ll_anno_bolla_acq) + "/" + string(ll_num_bolla_acq)
	elseif is_tipo_gestione = "Fat_Acq" then
		ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
		ll_num_registrazione  = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")
		ls_reg =	string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
	elseif is_tipo_gestione <> "Bol_Acq" and is_tipo_gestione <> "Fat_Acq" then
		ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
		ll_num_registrazione  = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")
		ls_reg =	string(ll_anno_registrazione) + "/" + string(ll_num_registrazione)
	end if	
	if isnull(ls_des_corrispondenza) then
		ls_des_corrispondenza = ""
	else
		ls_des_corrispondenza = ls_des_corrispondenza + "~r~n"
	end if
	if isnull(ls_oggetto) then
		ls_oggetto = ""
	else
		ls_oggetto = ls_oggetto + "~r~n"
	end if
//	if isnull(ls_note) then
//		ls_note = ""
//	else
//		ls_note = ls_note + "~r~n"
//	end if

	choose case is_tipo_gestione
		case "Off_Ven"
			ls_gestione = "Offerta di vendita  "
		case "Off_Acq"
			ls_gestione = "Offerta di acquisto  "
		case "Ord_Ven"
			ls_gestione = "Ordine di vendita  "
		case "Ord_Acq"
			ls_gestione = "Ordine di Acquisto  "
		case "Bol_Ven"
			ls_gestione = "Bolla di vendita  "
		case "Bol_Acq"
			ls_gestione = "Bolla di Acquisto  "
		case "Fat_Ven"
			ls_gestione = "Fattura di vendita  "
		case "Fat_Acq"
			ls_gestione = "Fattura di acquisto  "
	end choose
	
	ls_str = ls_note + &
			ls_des_corrispondenza + &
			ls_oggetto + &
			ls_gestione + &
			ls_reg

	setitem(row, "note", ls_str)
end if
end event

type dw_acq_ven_corr_det_2 from uo_cs_xx_dw within w_acq_ven_corr
integer x = 41
integer y = 640
integer width = 2240
integer height = 656
integer taborder = 20
string dataobject = ""
boolean border = false
end type

type dw_folder from u_folder within w_acq_ven_corr
integer x = 18
integer y = 540
integer width = 2327
integer height = 780
integer taborder = 0
boolean border = false
end type

