﻿$PBExportHeader$w_acq_ven_corr_ole.srw
forward
global type w_acq_ven_corr_ole from w_ole_v2
end type
end forward

global type w_acq_ven_corr_ole from w_ole_v2
integer height = 1376
end type
global w_acq_ven_corr_ole w_acq_ven_corr_ole

type variables
str_acq_ven_corr_ole lstr_ole
end variables

forward prototypes
public subroutine wf_load_documents ()
public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob)
public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob)
public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data)
public function long wf_blob_size ()
end prototypes

public subroutine wf_load_documents ();string ls_sql
long li_rows, li_i
datastore lds_documenti
str_ole lstr_data

if wf_blob_size( ) <= 0 then return

ls_sql = "SELECT cod_corrispondenza, oggetto, prog_mimetype FROM " 
choose case upper(lstr_ole.tipo_gestione)
	case "OFF_VEN"
		ls_sql += " tes_off_ven_corrispondenze  "
	case "ORD_VEN"
		ls_sql += " tes_ord_ven_corrispondenze  "
	case "BOL_VEN"
		ls_sql += " tes_bol_ven_corrispondenze  "
	case "FAT_VEN"
		ls_sql += " tes_fat_ven_corrispondenze  "
	case "OFF_ACQ"
		ls_sql += " tes_off_acq_corrispondenze  "
	case "ORD_ACQ"
		ls_sql += " tes_ord_acq_corrispondenze  "
	case "BOL_ACQ"
		ls_sql += " tes_bol_acq_corrispondenze  "
	case "FAT_ACQ"
		ls_sql += " tes_fat_acq_corrispondenze  "
end choose

ls_sql += g_str.format("WHERE cod_azienda ='$1' and anno_registrazione = $2 and num_registrazione =$3 ORDER BY	cod_azienda ASC, anno_registrazione ASC, num_registrazione ASC ", s_cs_xx.cod_azienda, string(lstr_ole.anno_registrazione ),string(lstr_ole.num_registrazione ) ) 
	
if guo_functions.uof_crea_datastore(ref lds_documenti, ls_sql) <= 0 then
	return 
end if

lstr_data.anno_registrazione = lstr_ole.anno_registrazione
lstr_data.num_registrazione = lstr_ole.num_registrazione
lstr_data.progressivo_s = "001"
lstr_data.prog_mimetype = lds_documenti.getitemnumber(1, 3)

wf_add_document(lds_documenti.getitemstring(1, 1), lstr_data)

end subroutine

public function boolean wf_download_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data, ref blob ab_file_blob);choose case upper(lstr_ole.tipo_gestione)
	case "OFF_VEN"
		selectblob note_esterne
		into :ab_file_blob
		from tes_off_ven_corrispondenze
		where
				cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lstr_ole.anno_registrazione  and
				num_registrazione = :lstr_ole.num_registrazione and
				cod_corrispondenza = :lstr_ole.cod_corrispondenza and
				data_corrispondenza = :lstr_ole.data_corrispondenza and
				ora_corrispondenza = :lstr_ole.ora_corrispondenza and
				prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	 
	case "ORD_VEN"
		selectblob note_esterne
		into :ab_file_blob
		from tes_ord_ven_corrispondenze
		where
				cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lstr_ole.anno_registrazione  and
				num_registrazione = :lstr_ole.num_registrazione and
				cod_corrispondenza = :lstr_ole.cod_corrispondenza and
				data_corrispondenza = :lstr_ole.data_corrispondenza and
				ora_corrispondenza = :lstr_ole.ora_corrispondenza and
				prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	 
	case "BOL_VEN"
		selectblob note_esterne
		into :ab_file_blob
		from tes_bol_ven_corrispondenze
		where
				cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lstr_ole.anno_registrazione  and
				num_registrazione = :lstr_ole.num_registrazione and
				cod_corrispondenza = :lstr_ole.cod_corrispondenza and
				data_corrispondenza = :lstr_ole.data_corrispondenza and
				ora_corrispondenza = :lstr_ole.ora_corrispondenza and
				prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	 
	case "FAT_VEN"
		selectblob note_esterne
		into :ab_file_blob
		from tes_fat_ven_corrispondenze
		where
				cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lstr_ole.anno_registrazione  and
				num_registrazione = :lstr_ole.num_registrazione and
				cod_corrispondenza = :lstr_ole.cod_corrispondenza and
				data_corrispondenza = :lstr_ole.data_corrispondenza and
				ora_corrispondenza = :lstr_ole.ora_corrispondenza and
				prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	 
	case "OFF_ACQ"
		selectblob note_esterne
		into :ab_file_blob
		from tes_off_acq_corrispondenze
		where
				cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lstr_ole.anno_registrazione  and
				num_registrazione = :lstr_ole.num_registrazione and
				cod_corrispondenza = :lstr_ole.cod_corrispondenza and
				data_corrispondenza = :lstr_ole.data_corrispondenza and
				ora_corrispondenza = :lstr_ole.ora_corrispondenza and
				prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	 
	case "ORD_ACQ"
		selectblob note_esterne
		into :ab_file_blob
		from tes_ord_acq_corrispondenze
		where
				cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lstr_ole.anno_registrazione  and
				num_registrazione = :lstr_ole.num_registrazione and
				cod_corrispondenza = :lstr_ole.cod_corrispondenza and
				data_corrispondenza = :lstr_ole.data_corrispondenza and
				ora_corrispondenza = :lstr_ole.ora_corrispondenza and
				prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	 
	case "BOL_ACQ"
		selectblob note_esterne
		into :ab_file_blob
		from tes_bol_acq_corrispondenze
		where
				cod_azienda = :s_cs_xx.cod_azienda and
				anno_bolla_acq = :lstr_ole.anno_registrazione  and
				num_bolla_acq = :lstr_ole.num_registrazione and
				cod_corrispondenza = :lstr_ole.cod_corrispondenza and
				data_corrispondenza = :lstr_ole.data_corrispondenza and
				ora_corrispondenza = :lstr_ole.ora_corrispondenza and
				prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	 
	case "FAT_ACQ"
		selectblob note_esterne
		into :ab_file_blob
		from tes_fat_acq_corrispondenze
		where
				cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lstr_ole.anno_registrazione  and
				num_registrazione = :lstr_ole.num_registrazione and
				cod_corrispondenza = :lstr_ole.cod_corrispondenza and
				data_corrispondenza = :lstr_ole.data_corrispondenza and
				ora_corrispondenza = :lstr_ole.ora_corrispondenza and
				prog_corrispondenza = :lstr_ole.prog_corrispondenza;
		end choose
		
if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante la lettura del documento " + alv_item.label + ". " + sqlca.sqlerrtext)
	return false
else
	if lenA(ab_file_blob) = 0 or isnull(ab_file_blob) then
		return false
	else
		return true
	end if
end if
end function

public function boolean wf_upload_file (string as_file_path, string as_file_name, string as_file_ext, long al_prog_mimetype, ref blob ab_file_blob);str_ole lstr_data

choose case upper(lstr_ole.tipo_gestione)
	case "OFF_VEN"
		updateblob tes_off_ven_corrispondenze
		set note_esterne = :ab_file_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
	case "ORD_VEN"
		updateblob tes_ord_ven_corrispondenze
		set note_esterne = :ab_file_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
	case "BOL_VEN"
		updateblob tes_bol_ven_corrispondenze
		set note_esterne = :ab_file_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
	case "FAT_VEN"
		updateblob tes_fat_ven_corrispondenze
		set note_esterne = :ab_file_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
	case "OFF_ACQ"
		updateblob tes_off_acq_corrispondenze
		set note_esterne = :ab_file_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
	case "ORD_ACQ"
		updateblob tes_ord_acq_corrispondenze
		set note_esterne = :ab_file_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
	case "BOL_ACQ"
		updateblob tes_bol_acq_corrispondenze
		set note_esterne = :ab_file_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_bolla_acq = :lstr_ole.anno_registrazione  and
			num_bolla_acq = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
	case "FAT_ACQ"
		updateblob tes_fat_acq_corrispondenze
		set note_esterne = :ab_file_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
end choose

// Aggiornamento campo blob
if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il salvataggio del file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
end if

// update del prog_mimetype
choose case upper(lstr_ole.tipo_gestione)
	case "OFF_VEN"
		update tes_off_ven_corrispondenze
		set prog_mimetype = :al_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
	case "ORD_VEN"
		update tes_ord_ven_corrispondenze
		set prog_mimetype = :al_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
	case "BOL_VEN"
		update tes_bol_ven_corrispondenze
		set prog_mimetype = :al_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
	case "FAT_VEN"
		update tes_fat_ven_corrispondenze
		set prog_mimetype = :al_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
	case "OFF_ACQ"
		update tes_off_acq_corrispondenze
		set prog_mimetype = :al_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
	case "ORD_ACQ"
		update tes_ord_acq_corrispondenze
		set prog_mimetype = :al_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
	case "BOL_ACQ"
		update tes_bol_acq_corrispondenze
		set prog_mimetype = :al_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_bolla_acq = :lstr_ole.anno_registrazione  and
			num_bolla_acq = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
	case "FAT_ACQ"
		update tes_fat_acq_corrispondenze
		set prog_mimetype = :al_prog_mimetype
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
			
end choose

// Aggiornamento campo blob
if sqlca.sqlcode < 0 then
	g_mb.error("Documenti", "Errore durante il salvataggio del file " + as_file_name + ". " + sqlca.sqlerrtext)
	return false
end if

// Creo struttura e aggiongo icona
lstr_data.anno_registrazione = il_anno_registrazione
lstr_data.num_registrazione = il_num_registrazione
lstr_data.progressivo_s = "001"
lstr_data.prog_mimetype = al_prog_mimetype
// aggiungere informazioni alla struttura se necessario

wf_add_document(as_file_name, lstr_data)

return true
end function

public function boolean wf_delete_file (integer ai_index, ref listviewitem alv_item, str_ole astr_data);blob lb_blob

lb_blob = blob("A")

choose case upper(lstr_ole.tipo_gestione)
	case "OFF_VEN"
		updateblob tes_off_ven_corrispondenze
		set note_esterne = :lb_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	case "ORD_VEN"
		updateblob tes_ord_ven_corrispondenze
		set note_esterne = :lb_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	case "BOL_VEN"
		updateblob tes_bol_ven_corrispondenze
		set note_esterne = :lb_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	case "FAT_VEN"
		updateblob tes_fat_ven_corrispondenze
		set note_esterne = :lb_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
		
	case "OFF_ACQ"
		updateblob tes_off_acq_corrispondenze
		set note_esterne = :lb_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	case "ORD_ACQ"
		updateblob tes_ord_acq_corrispondenze
		set note_esterne = :lb_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_bolla_acq = :lstr_ole.anno_registrazione  and
			num_bolla_acq = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	case "BOL_ACQ"
		updateblob tes_bol_acq_corrispondenze
		set note_esterne = :lb_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	case "FAT_ACQ"
		updateblob tes_fat_acq_corrispondenze
		set note_esterne = :lb_blob
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;

end choose

if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante la cancellazione del documento " + alv_item.label +". " + sqlca.sqlerrtext)
	return false
else
	return true
end if
end function

public function long wf_blob_size ();string ls_str
blob lb_blob
 
choose case upper(lstr_ole.tipo_gestione)
	case "OFF_VEN"

		selectblob note_esterne
		into :lb_blob
		from tes_off_ven_corrispondenze
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	 
	case "ORD_VEN"

		selectblob note_esterne
		into :lb_blob
		from tes_ord_ven_corrispondenze
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	 
	case "BOL_VEN"

		selectblob note_esterne
		into :lb_blob
		from tes_bol_ven_corrispondenze
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	 
	case "FAT_VEN"

		selectblob note_esterne
		into :lb_blob
		from tes_fat_ven_corrispondenze
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	 
	case "OFF_ACQ"

		selectblob note_esterne
		into :lb_blob
		from tes_off_acq_corrispondenze
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	 
	case "ORD_ACQ"

		selectblob note_esterne
		into :lb_blob
		from tes_ord_acq_corrispondenze
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	 
	case "BOL_ACQ"

		selectblob note_esterne
		into :lb_blob
		from tes_bol_acq_corrispondenze
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_bolla_acq = :lstr_ole.anno_registrazione  and
			num_bolla_acq = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;
	 
	case "FAT_ACQ"

		selectblob note_esterne
		into :lb_blob
		from tes_fat_acq_corrispondenze
		where
			cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :lstr_ole.anno_registrazione  and
			num_registrazione = :lstr_ole.num_registrazione and
			cod_corrispondenza = :lstr_ole.cod_corrispondenza and
			data_corrispondenza = :lstr_ole.data_corrispondenza and
			ora_corrispondenza = :lstr_ole.ora_corrispondenza and
			prog_corrispondenza = :lstr_ole.prog_corrispondenza;

end choose
if sqlca.sqlcode <> 0 then
	g_mb.error("Documenti", "Errore durante la lettura del documento." + sqlca.sqlerrtext)
	return 0
else
	if string(lb_blob) = "A" then return 0
	return LenA(lb_blob)
end if
end function

on w_acq_ven_corr_ole.create
call super::create
end on

on w_acq_ven_corr_ole.destroy
call super::destroy
end on

event open;call super::open;lstr_ole = message.powerobjectparm

end event

type st_loading from w_ole_v2`st_loading within w_acq_ven_corr_ole
end type

type cb_cancella from w_ole_v2`cb_cancella within w_acq_ven_corr_ole
end type

type lv_documenti from w_ole_v2`lv_documenti within w_acq_ven_corr_ole
end type

type ddlb_style from w_ole_v2`ddlb_style within w_acq_ven_corr_ole
end type

type cb_sfoglia from w_ole_v2`cb_sfoglia within w_acq_ven_corr_ole
end type

event cb_sfoglia::clicked;if lv_documenti.totalitems( ) >= 1 then
	g_mb.show("Attenzione, è previsto l'inserimento di 1 solo documento")
	return
end if

Super::event clicked()
end event

