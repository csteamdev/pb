﻿$PBExportHeader$w_acq_ven_note.srw
$PBExportComments$Finestra Gestione Note di Acquisto/Vendita
forward
global type w_acq_ven_note from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_acq_ven_note
end type
type dw_acq_ven_note_lista from uo_cs_xx_dw within w_acq_ven_note
end type
type dw_acq_ven_note_det from uo_cs_xx_dw within w_acq_ven_note
end type
type cb_note_esterne from commandbutton within w_acq_ven_note
end type
end forward

global type w_acq_ven_note from w_cs_xx_principale
integer width = 2450
integer height = 1388
string title = "Note Offerte di Vendita"
cb_1 cb_1
dw_acq_ven_note_lista dw_acq_ven_note_lista
dw_acq_ven_note_det dw_acq_ven_note_det
cb_note_esterne cb_note_esterne
end type
global w_acq_ven_note w_acq_ven_note

type prototypes

end prototypes

type variables
string is_tipo_gestione
boolean ib_in_new
end variables

on w_acq_ven_note.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_acq_ven_note_lista=create dw_acq_ven_note_lista
this.dw_acq_ven_note_det=create dw_acq_ven_note_det
this.cb_note_esterne=create cb_note_esterne
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_acq_ven_note_lista
this.Control[iCurrent+3]=this.dw_acq_ven_note_det
this.Control[iCurrent+4]=this.cb_note_esterne
end on

on w_acq_ven_note.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_acq_ven_note_lista)
destroy(this.dw_acq_ven_note_det)
destroy(this.cb_note_esterne)
end on

event pc_setwindow;call super::pc_setwindow;string ls_gestione_doc

is_tipo_gestione = s_cs_xx.parametri.parametro_s_1

choose case is_tipo_gestione
	case "Off_Ven"
		dw_acq_ven_note_lista.dataobject = "d_off_ven_note_lista"
		dw_acq_ven_note_det.dataobject = "d_off_ven_note_det"
		this.title = 'Note Offerte di Vendita'
	case "Off_Acq"
		dw_acq_ven_note_lista.dataobject = "d_off_acq_note_lista"
		dw_acq_ven_note_det.dataobject = "d_off_acq_note_det"
		this.title = 'Note Offerte di Acquisto'
	case "Ord_Ven"
		dw_acq_ven_note_lista.dataobject = "d_ord_ven_note_lista"
		dw_acq_ven_note_det.dataobject = "d_ord_ven_note_det"
		this.title = 'Note Ordini di Vendita'
	case "Ord_Acq"
		dw_acq_ven_note_lista.dataobject = "d_ord_acq_note_lista"
		dw_acq_ven_note_det.dataobject = "d_ord_acq_note_det"
		this.title = 'Note Ordini di Acquisto'
	case "Bol_Ven"
		dw_acq_ven_note_lista.dataobject = "d_bol_ven_note_lista"
		dw_acq_ven_note_det.dataobject = "d_bol_ven_note_det"
		this.title = 'Note Bolle di Vendita'
	case "Bol_Acq"
		dw_acq_ven_note_lista.dataobject = "d_bol_acq_note_lista"
		dw_acq_ven_note_det.dataobject = "d_bol_acq_note_det"
		this.title = 'Note Bolle di Acquisto'
	case "Fat_Ven"
		dw_acq_ven_note_lista.dataobject = "d_fat_ven_note_lista"
		dw_acq_ven_note_det.dataobject = "d_fat_ven_note_det"
		this.title = 'Note Fatture di Vendita'
		
		cb_1.visible = true	
		cb_note_esterne.visible=false
	case "Fat_Acq"
		dw_acq_ven_note_lista.dataobject = "d_fat_acq_note_lista"
		dw_acq_ven_note_det.dataobject = "d_fat_acq_note_det"
		this.title = 'Note Fatture di Acquisto'
end choose

dw_acq_ven_note_lista.set_dw_key("cod_azienda")
if is_tipo_gestione = "Bol_Acq" then
	dw_acq_ven_note_lista.set_dw_key("anno_bolla_acq")
	dw_acq_ven_note_lista.set_dw_key("num_bolla_acq")
elseif is_tipo_gestione = "Fat_Acq" then
	dw_acq_ven_note_lista.set_dw_key("anno_registrazione")
	dw_acq_ven_note_lista.set_dw_key("num_registrazione")
elseif is_tipo_gestione <> "Bol_Acq" and is_tipo_gestione <> "Fat_Acq" then
	dw_acq_ven_note_lista.set_dw_key("anno_registrazione")
	dw_acq_ven_note_lista.set_dw_key("num_registrazione")
end if
dw_acq_ven_note_lista.set_dw_key("cod_nota")
dw_acq_ven_note_lista.set_dw_options(sqlca,i_openparm,c_default,c_default)

dw_acq_ven_note_det.set_dw_options(sqlca,dw_acq_ven_note_lista,c_sharedata+c_scrollparent,c_default)

iuo_dw_main = dw_acq_ven_note_lista

end event

event pc_delete;call super::pc_delete;ib_in_new = false
integer li_row
li_row = dw_acq_ven_note_lista.getrow()
if li_row <= 0 or isnull(dw_acq_ven_note_lista.GetItemstring(li_row,"cod_nota")) then
	cb_note_esterne.enabled = false
else
	cb_note_esterne.enabled = true
end if
end event

type cb_1 from commandbutton within w_acq_ven_note
integer x = 2034
integer y = 20
integer width = 361
integer height = 80
integer taborder = 31
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Visualizza"
end type

event clicked;long li_anno_registrazione, ll_num_registrazione, ll_ret, ll_prog
string ls_cod_nota, ls_user_path, ls_file_name,ls_estensione,ls_db,ls_errore
blob lb_blob
transaction sqlcb


li_anno_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "anno_registrazione")
ll_num_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "num_registrazione")
ls_cod_nota 			= dw_acq_ven_note_lista.getitemstring(dw_acq_ven_note_lista.getrow(), "cod_nota")

select  prog_mimetype
into    :ll_prog
from    tes_fat_ven_note
where   cod_azienda = :s_cs_xx.cod_azienda and
		  anno_registrazione = :li_anno_registrazione and 
		  num_registrazione = :ll_num_registrazione and 
		  cod_nota = :ls_cod_nota;
					  
if sqlca.sqlcode <> 0 then 
	g_mb.messagebox("APICE","Mymetype non trovato nel sistema")
	return
end if

select estensione
into   :ls_estensione
from   tab_mimetype
where  cod_azienda = :s_cs_xx.cod_azienda and
		 prog_mimetype = :ll_prog;

if sqlca.sqlcode <> 0 then 
	g_mb.messagebox("APICE","Estensione pdf non gestita dal sistema")
	return
end if
/*
if lower(ls_estensione) <> "pdf" then
	g_mb.messagebox("APICE","Estensione può essere solo PDF")
	return
end if
*/
f_getuserpath(ls_user_path)

ls_file_name = ls_user_path + "cs" + string(Hour(Now())) + string(Minute(Now())) + string(Second(Now())) + "."	 + ls_estensione

ls_db = f_db()
if ls_db = "MSSQL" then
	
	if not guo_functions.uof_create_transaction_from( ref sqlca, ref sqlcb, ls_errore) then
		g_mb.error(ls_errore)
		return
	end if
	
	selectblob 	  note_esterne
	into       		:lb_blob
	from       tes_fat_ven_note
	where      cod_azienda = :s_cs_xx.cod_azienda and
				  anno_registrazione = :li_anno_registrazione and 
				  num_registrazione = :ll_num_registrazione and 
				  cod_nota = :ls_cod_nota
	using         sqlcb;
	if sqlcb.sqlcode < 0 then
		g_mb.error("Errore durante lettura documento digitale dal DB." + sqlcb.sqlerrtext)
		return
	end if
	destroy sqlcb;
	
else
	
	selectblob    note_esterne
		into       :lb_blob
		from       tes_fat_ven_note
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_registrazione = :li_anno_registrazione and 
					  num_registrazione = :ll_num_registrazione and 
					  cod_nota = :ls_cod_nota;
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante lettura documento digitale dal DB." + sqlca.sqlerrtext)
		return
	end if
					  
end if

ll_ret = LEN(lb_blob)
ll_ret = guo_functions.uof_blob_to_file(ref lb_blob, ls_file_name)
if ll_ret < 0 then return -1

uo_shellexecute luo_run
luo_run = Create uo_shellexecute

ll_ret = luo_run.uof_run(handle(parent), ls_file_name)
if ll_ret <= 32 then
	g_mb.messagebox("Attenzione", "Impossibile visualizzare il documento ", exclamation!, ok!)		
end if		

destroy luo_run

end event

type dw_acq_ven_note_lista from uo_cs_xx_dw within w_acq_ven_note
integer x = 18
integer y = 20
integer width = 1984
integer height = 500
integer taborder = 10
string dataobject = ""
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_new;call super::pcd_new;integer li_anno_registrazione, li_anno_bolla_acq, li_anno_doc_origine
long ll_num_registrazione, ll_progressivo, ll_num_doc_origine, ll_num_bolla_acq
string ls_cod_doc_origine, ls_numeratore_doc_origine

if is_tipo_gestione = "Bol_Acq" then
	li_anno_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_bolla_acq")
	ll_num_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_bolla_acq")
	dw_acq_ven_note_lista.setitem(getrow(),"anno_bolla_acq",li_anno_bolla_acq)
	dw_acq_ven_note_lista.setitem(getrow(),"num_bolla_acq",ll_num_bolla_acq)
elseif is_tipo_gestione = "Fat_Acq" then
	li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	dw_acq_ven_note_lista.setitem(getrow(),"anno_registrazione",li_anno_registrazione)
	dw_acq_ven_note_lista.setitem(getrow(),"num_registrazione",ll_num_registrazione)
elseif is_tipo_gestione <> "Bol_Acq" and is_tipo_gestione <> "Fat_Acq" then
	li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	dw_acq_ven_note_lista.setitem(getrow(),"anno_registrazione",li_anno_registrazione)
	dw_acq_ven_note_lista.setitem(getrow(),"num_registrazione",ll_num_registrazione)
end if

cb_note_esterne.enabled = false

ib_in_new = true
end event

event pcd_retrieve;call super::pcd_retrieve;integer li_anno_registrazione, li_anno_bolla_acq, li_anno_doc_origine
long ll_num_registrazione, l_ERROR, ll_progressivo, ll_num_doc_origine, ll_num_bolla_acq
string ls_cod_doc_origine, ls_numeratore_doc_origine

if is_tipo_gestione = "Bol_Acq" then
	li_anno_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_bolla_acq")
	ll_num_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_bolla_acq")
	l_Error = Retrieve(s_cs_xx.cod_azienda, li_anno_bolla_acq, ll_num_bolla_acq)
elseif is_tipo_gestione = "Fat_Acq" then
	li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	l_Error = Retrieve(s_cs_xx.cod_azienda, li_anno_registrazione, ll_num_registrazione )
elseif is_tipo_gestione <> "Bol_Acq" and is_tipo_gestione <> "Fat_Acq" then
	li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	l_Error = Retrieve(s_cs_xx.cod_azienda, li_anno_registrazione, ll_num_registrazione )
end if

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF
end event

event pcd_setkey;call super::pcd_setkey;integer li_anno_registrazione, li_anno_bolla_acq, li_anno_doc_origine
long ll_num_registrazione, l_ERROR, ll_progressivo, l_idx, ll_num_doc_origine, ll_num_bolla_acq
string ls_cod_doc_origine, ls_numeratore_doc_origine

if is_tipo_gestione = "Bol_Acq" then
	li_anno_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_bolla_acq")
	ll_num_bolla_acq = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_bolla_acq")
	FOR l_Idx = 1 TO RowCount()
		IF IsNull(getitemnumber(l_Idx, "anno_bolla_acq")) or getitemnumber(l_Idx, "anno_bolla_acq") = 0 THEN
			SetItem(l_Idx, "anno_bolla_acq", li_anno_bolla_acq)
		END IF
	NEXT
	FOR l_Idx = 1 TO RowCount()
		IF IsNull(getitemnumber(l_Idx, "num_bolla_acq")) or getitemnumber(l_Idx, "num_bolla_acq") = 0 THEN
			SetItem(l_Idx, "num_bolla_acq", ll_num_bolla_acq)
		END IF
	NEXT
elseif is_tipo_gestione = "Fat_Acq" then
	li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	FOR l_Idx = 1 TO RowCount()
		IF IsNull(getitemnumber(l_Idx, "anno_registrazione")) or getitemnumber(l_Idx, "anno_registrazione") = 0 THEN
			SetItem(l_Idx, "anno_registrazione", li_anno_registrazione)
		END IF
	NEXT
	
	FOR l_Idx = 1 TO RowCount()
		IF IsNull(getitemnumber(l_Idx, "num_registrazione")) or getitemnumber(l_Idx, "num_registrazione") = 0 THEN
			SetItem(l_Idx, "num_registrazione", ll_num_registrazione)
		END IF
	NEXT
elseif is_tipo_gestione <> "Bol_Acq" and is_tipo_gestione <> "Fat_Acq" then
	li_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	FOR l_Idx = 1 TO RowCount()
		IF IsNull(getitemnumber(l_Idx, "anno_registrazione")) or getitemnumber(l_Idx, "anno_registrazione") = 0 THEN
			SetItem(l_Idx, "anno_registrazione", li_anno_registrazione)
		END IF
	NEXT
	
	FOR l_Idx = 1 TO RowCount()
		IF IsNull(getitemnumber(l_Idx, "num_registrazione")) or getitemnumber(l_Idx, "num_registrazione") = 0 THEN
			SetItem(l_Idx, "num_registrazione", ll_num_registrazione)
		END IF
	NEXT
end if

FOR l_Idx = 1 TO RowCount()
   IF IsNull(GetItemstring(l_Idx, "cod_azienda")) THEN
      SetItem(l_Idx, "cod_azienda", s_cs_xx.cod_azienda)
   END IF
NEXT

end event

event pcd_modify;call super::pcd_modify;cb_note_esterne.enabled = false

ib_in_new = false
end event

event pcd_save;call super::pcd_save;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_nota")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if

ib_in_new = false


end event

event pcd_view;call super::pcd_view;integer li_row
li_row = getrow()
if li_row > 0 and not isnull(GetItemstring(li_row,"cod_nota")) then
	cb_note_esterne.enabled = true
else
	cb_note_esterne.enabled = false
end if
ib_in_new = false
end event

type dw_acq_ven_note_det from uo_cs_xx_dw within w_acq_ven_note
integer x = 18
integer y = 540
integer width = 2373
integer height = 720
integer taborder = 20
boolean bringtotop = true
string dataobject = ""
borderstyle borderstyle = styleraised!
end type

type cb_note_esterne from commandbutton within w_acq_ven_note
event clicked pbm_bnclicked
integer x = 2034
integer y = 120
integer width = 361
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Documento"
end type

event clicked;integer     li_anno_registrazione, li_anno_bolla_acq, li_anno_doc_origine, li_risposta

long        ll_num_registrazione, ll_progressivo, l_Idx, ll_num_doc_origine, ll_num_bolla_acq, ll_prog_mimetype

string      ls_cod_nota, ls_cod_doc_origine, ls_numeratore_doc_origine, ls_db, ls_gestione_doc

transaction sqlcb

blob        lbl_null

boolean     lb_gestione = false

setnull(lbl_null)

ls_cod_nota = dw_acq_ven_note_lista.getitemstring(dw_acq_ven_note_lista.getrow(), "cod_nota")

// 17-07-2002 modifiche Michela: controllo l'enginetype

ls_db = f_db()

choose case is_tipo_gestione
	case "Off_Ven"
		li_anno_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "num_registrazione")
		
		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			selectblob tes_off_ven_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_off_ven_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_registrazione = :li_anno_registrazione and 
							  num_registrazione = :ll_num_registrazione and 
							  cod_nota = :ls_cod_nota
			using         sqlcb;
			
			//destroy sqlcb;
			
		else
			
			selectblob tes_off_ven_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_off_ven_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_registrazione = :li_anno_registrazione and 
							  num_registrazione = :ll_num_registrazione and 
							  cod_nota = :ls_cod_nota;
							  
		end if
						
	case "Off_Acq"
		li_anno_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "num_registrazione")

		if ls_db = "MSSQL" then

			li_risposta = f_crea_sqlcb(sqlcb)
			
			selectblob tes_off_acq_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_off_acq_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_registrazione = :li_anno_registrazione and 
							  num_registrazione = :ll_num_registrazione and 
							  cod_nota = :ls_cod_nota
			using         sqlcb;
			
			//destroy sqlcb;
			
		else
			
			selectblob tes_off_acq_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_off_acq_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_registrazione = :li_anno_registrazione and 
							  num_registrazione = :ll_num_registrazione and 
							  cod_nota = :ls_cod_nota;
							  
		end if
		
	case "Ord_Ven"
		li_anno_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "num_registrazione")

		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			selectblob tes_ord_ven_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_ord_ven_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_registrazione = :li_anno_registrazione and 
							  num_registrazione = :ll_num_registrazione and 
							  cod_nota = :ls_cod_nota
			using         sqlcb;
			
			//destroy sqlcb;
			
		else
			
			selectblob tes_ord_ven_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_ord_ven_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_registrazione = :li_anno_registrazione and 
							  num_registrazione = :ll_num_registrazione and 
							  cod_nota = :ls_cod_nota;
							  
		end if
						
	case "Ord_Acq"
		li_anno_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "num_registrazione")

		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			selectblob tes_ord_acq_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_ord_acq_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_registrazione = :li_anno_registrazione and 
							  num_registrazione = :ll_num_registrazione and 
							  cod_nota = :ls_cod_nota
			using         sqlcb;
			
			//destroy sqlcb;
			
		else
			
			selectblob tes_ord_acq_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_ord_acq_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_registrazione = :li_anno_registrazione and 
							  num_registrazione = :ll_num_registrazione and 
							  cod_nota = :ls_cod_nota;
							  
		end if
						
	case "Bol_Ven"
		li_anno_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "num_registrazione")
		
		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			selectblob tes_bol_ven_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_bol_ven_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_registrazione = :li_anno_registrazione and 
							  num_registrazione = :ll_num_registrazione and 
							  cod_nota = :ls_cod_nota
			using         sqlcb;
			
			//destroy sqlcb;
			
		else
			
			selectblob tes_bol_ven_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_bol_ven_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_registrazione = :li_anno_registrazione and 
							  num_registrazione = :ll_num_registrazione and 
							  cod_nota = :ls_cod_nota;
							  
		end if
					
	case "Bol_Acq"
		li_anno_bolla_acq = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "anno_bolla_acq")
		ll_num_bolla_acq = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "num_bolla_acq")

		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			selectblob tes_bol_acq_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_bol_acq_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_bolla_acq = :li_anno_bolla_acq and 
							  num_bolla_acq = :ll_num_bolla_acq and 
							  cod_nota = :ls_cod_nota
			using         sqlcb;
			
			//destroy sqlcb;
			
		else
			
			selectblob tes_bol_acq_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_bol_acq_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_bolla_acq = :li_anno_bolla_acq and 
							  num_bolla_acq = :ll_num_bolla_acq and 
							  cod_nota = :ls_cod_nota;
		
		end if
		
	case "Fat_Ven"
		li_anno_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "num_registrazione")

		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			selectblob tes_fat_ven_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_fat_ven_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_registrazione = :li_anno_registrazione and 
							  num_registrazione = :ll_num_registrazione and 
							  cod_nota = :ls_cod_nota
			using         sqlcb;
			
			//destroy sqlcb;
			
		else
			
			selectblob tes_fat_ven_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_fat_ven_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_registrazione = :li_anno_registrazione and 
							  num_registrazione = :ll_num_registrazione and 
							  cod_nota = :ls_cod_nota;
							  
		end if
						
	case "Fat_Acq"
		li_anno_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "anno_registrazione")
		ll_num_registrazione = dw_acq_ven_note_lista.getitemnumber(dw_acq_ven_note_lista.getrow(), "num_registrazione")
		
		if ls_db = "MSSQL" then
			
			li_risposta = f_crea_sqlcb(sqlcb)
			
			selectblob tes_fat_acq_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_fat_acq_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_registrazione = :li_anno_registrazione and
							  num_registrazione = :ll_num_registrazione and
							  cod_nota = :ls_cod_nota
			using         sqlcb;
			
			//destroy sqlcb;
			
		else
			
			selectblob tes_fat_acq_note.note_esterne
				into       :s_cs_xx.parametri.parametro_bl_1
				from       tes_fat_acq_note
				where      cod_azienda = :s_cs_xx.cod_azienda and
							  anno_registrazione = :li_anno_registrazione and
							  num_registrazione = :ll_num_registrazione and
							  cod_nota = :ls_cod_nota;
							  
		end if
						
end choose


if ls_db = "MSSQL" then
	
	if sqlcb.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
	disconnect using sqlcb;
	destroy sqlcb;
	
else
	
	if sqlca.sqlcode <> 0 then
	   s_cs_xx.parametri.parametro_bl_1 = lbl_null
	end if
	
end if

// ******* michela 17/05/2005: per la c&s la gestione dei blob delle fatture serve alla intranet

select flag
into   :ls_gestione_doc
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'BFV';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox( "OMNIA", "Errore durante la ricerca del parametro BFV: " + sqlca.sqlerrtext)
	return -1
end if

if sqlca.sqlcode = 0 and not isnull(ls_gestione_doc) and ls_gestione_doc = "S" then
	lb_gestione = true
else
	lb_gestione = false
end if



if lb_gestione = false then

	window_open(w_ole, 0)
	
	if not isnull(s_cs_xx.parametri.parametro_bl_1) then
		
		choose case is_tipo_gestione
				
			case "Off_Ven"
				
				if ls_db = "MSSQL" then
						
					li_risposta = f_crea_sqlcb(sqlcb)
						
					updateblob tes_off_ven_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :li_anno_registrazione and 
								  num_registrazione = :ll_num_registrazione and 
								  cod_nota = :ls_cod_nota
					using      sqlcb;
						
					//destroy sqlcb;
						
				else
						
					updateblob tes_off_ven_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :li_anno_registrazione and 
								  num_registrazione = :ll_num_registrazione and 
								  cod_nota = :ls_cod_nota;
									  
				end if
								
			case "Off_Acq"
				
				if ls_db = "MSSQL" then
					
					li_risposta = f_crea_sqlcb(sqlcb)
					
					updateblob tes_off_acq_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :li_anno_registrazione and 
								  num_registrazione = :ll_num_registrazione and 
								  cod_nota = :ls_cod_nota
					using      sqlcb;
					
					//destroy sqlcb;
					
				else
					
					updateblob tes_off_acq_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :li_anno_registrazione and 
								  num_registrazione = :ll_num_registrazione and 
								  cod_nota = :ls_cod_nota;
				
				end if
					
			case "Ord_Ven"
				
				if ls_db = "MSSQL" then
					
					li_risposta = f_crea_sqlcb(sqlcb)
					
					updateblob tes_ord_ven_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :li_anno_registrazione and 
								  num_registrazione = :ll_num_registrazione and 
								  cod_nota = :ls_cod_nota
					using      sqlcb;
						
					//destroy sqlcb;
						
				else
						
					updateblob tes_ord_ven_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :li_anno_registrazione and 
								  num_registrazione = :ll_num_registrazione and 
								  cod_nota = :ls_cod_nota;
									  
				end if								  
									  
			case "Ord_Acq"
				
				if ls_db = "MSSQL" then
					
					li_risposta = f_crea_sqlcb(sqlcb)
					
					updateblob tes_ord_acq_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :li_anno_registrazione and 
								  num_registrazione = :ll_num_registrazione and 
								  cod_nota = :ls_cod_nota
					using      sqlcb;
								  
					//destroy sqlcb;
					
				else
					
					updateblob tes_ord_acq_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :li_anno_registrazione and 
								  num_registrazione = :ll_num_registrazione and 
								  cod_nota = :ls_cod_nota;
								  
				end if
					
			case "Bol_Ven"
				
				if ls_db = "MSSQL" then
						
					li_risposta = f_crea_sqlcb(sqlcb)
						
					updateblob tes_bol_ven_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :li_anno_registrazione and 
								  num_registrazione = :ll_num_registrazione and 
								  tes_bol_ven_note.cod_nota = :ls_cod_nota
					using      sqlcb;
						
					//destroy sqlcb;
						
				else
						
					updateblob tes_bol_ven_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :li_anno_registrazione and 
								  num_registrazione = :ll_num_registrazione and 
								  tes_bol_ven_note.cod_nota = :ls_cod_nota;
								  
				end if
								
			case "Bol_Acq"
				
				if ls_db = "MSSQL" then
					
					li_risposta = f_crea_sqlcb(sqlcb)
					
					updateblob tes_bol_acq_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_bolla_acq = :li_anno_bolla_acq and 
								  num_bolla_acq = :ll_num_bolla_acq and 
								  cod_nota = :ls_cod_nota
					using      sqlcb;
					
					//destroy sqlcb;
						
				else
						
					updateblob tes_bol_acq_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_bolla_acq = :li_anno_bolla_acq and 
								  num_bolla_acq = :ll_num_bolla_acq and 
								  cod_nota = :ls_cod_nota;
								  
				end if
								
			case "Fat_Ven"
				
				if ls_db = "MSSQL" then
					
					li_risposta = f_crea_sqlcb(sqlcb)
					
					updateblob tes_fat_ven_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :li_anno_registrazione and 
								  num_registrazione = :ll_num_registrazione and 
								  cod_nota = :ls_cod_nota
					using      sqlcb;
					
					//destroy sqlcb;
						
				else
						
					updateblob tes_fat_ven_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :li_anno_registrazione and 
								  num_registrazione = :ll_num_registrazione and 
								  cod_nota = :ls_cod_nota;
				
				end if
				
			case "Fat_Acq"
				
				if ls_db = "MSSQL" then
					
					li_risposta = f_crea_sqlcb(sqlcb)
					
					updateblob tes_fat_acq_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :li_anno_registrazione and 
								  num_registrazione = :ll_num_registrazione and 
								  cod_nota = :ls_cod_nota
					using      sqlcb;
					
					//destroy sqlcb;
					
				else
						
					updateblob tes_fat_acq_note
					set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
					where      cod_azienda = :s_cs_xx.cod_azienda and
								  anno_registrazione = :li_anno_registrazione and 
								  num_registrazione = :ll_num_registrazione and 
								  cod_nota = :ls_cod_nota;
								  
				end if
								
		end choose
		
		if ls_db = "MSSQL" then
			commit using sqlcb;
			disconnect using sqlcb;
			destroy sqlcb;
			
		else
			commit;
		end if
		
		//commit;
		
	end if
	
else							// ************************************************* gestione dei documenti in base alla intranet
	
	ll_prog_mimetype = dw_acq_ven_note_lista.getitemnumber( dw_acq_ven_note_lista.getrow(), "prog_mimetype")

	s_cs_xx.parametri.parametro_d_1 = ll_prog_mimetype
	
	window_open(w_ole_documenti, 0)
	
	ll_prog_mimetype = s_cs_xx.parametri.parametro_d_1
	
	if not isnull(s_cs_xx.parametri.parametro_bl_1) and len(s_cs_xx.parametri.parametro_bl_1) > 0  and not isnull(ll_prog_mimetype) and ll_prog_mimetype > 0 then
		
		if not isnull(s_cs_xx.parametri.parametro_bl_1) then

			choose case is_tipo_gestione
					
				case "Off_Ven"
					
					if ls_db = "MSSQL" then
							
						li_risposta = f_crea_sqlcb(sqlcb)
							
						updateblob tes_off_ven_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_registrazione = :li_anno_registrazione and 
									  num_registrazione = :ll_num_registrazione and 
									  cod_nota = :ls_cod_nota
						using      sqlcb;
							
						//destroy sqlcb;
							
					else
							
						updateblob tes_off_ven_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_registrazione = :li_anno_registrazione and 
									  num_registrazione = :ll_num_registrazione and 
									  cod_nota = :ls_cod_nota;
										  
					end if
					
//					update tes_off_ven_note
//					set    prog_mimetype = :ll_prog_mimetype
//					where  cod_azienda = :s_cs_xx.cod_azienda and
//							 anno_registrazione = :li_anno_registrazione and 
//							 num_registrazione = :ll_num_registrazione and 
//							 cod_nota = :ls_cod_nota;							
									
				case "Off_Acq"
					
					if ls_db = "MSSQL" then
							
						li_risposta = f_crea_sqlcb(sqlcb)
							
						updateblob tes_off_acq_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_registrazione = :li_anno_registrazione and 
									  num_registrazione = :ll_num_registrazione and 
									  cod_nota = :ls_cod_nota
						using      sqlcb;
							
						//destroy sqlcb;
							
					else
							
						updateblob tes_off_acq_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_registrazione = :li_anno_registrazione and 
									  num_registrazione = :ll_num_registrazione and 
									  cod_nota = :ls_cod_nota;
					
					end if
					
//					update tes_off_acq_note
//					set    prog_mimetype = :ll_prog_mimetype
//					where  cod_azienda = :s_cs_xx.cod_azienda and
//							 anno_registrazione = :li_anno_registrazione and 
//							 num_registrazione = :ll_num_registrazione and 
//							 cod_nota = :ls_cod_nota;						
						
				case "Ord_Ven"
					
					if ls_db = "MSSQL" then
							
						li_risposta = f_crea_sqlcb(sqlcb)
							
						updateblob tes_ord_ven_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_registrazione = :li_anno_registrazione and 
									  num_registrazione = :ll_num_registrazione and 
									  cod_nota = :ls_cod_nota
						using      sqlcb;
							
						//destroy sqlcb;
							
					else
							
						updateblob tes_ord_ven_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_registrazione = :li_anno_registrazione and 
									  num_registrazione = :ll_num_registrazione and 
									  cod_nota = :ls_cod_nota;
									  
					end if	
					
//					update tes_ord_ven_note
//					set    prog_mimetype = :ll_prog_mimetype
//					where  cod_azienda = :s_cs_xx.cod_azienda and
//							 anno_registrazione = :li_anno_registrazione and 
//							 num_registrazione = :ll_num_registrazione and 
//							 cod_nota = :ls_cod_nota;					
										  
				case "Ord_Acq"
					
					if ls_db = "MSSQL" then
						
						li_risposta = f_crea_sqlcb(sqlcb)
						
						updateblob tes_ord_acq_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_registrazione = :li_anno_registrazione and 
									  num_registrazione = :ll_num_registrazione and 
									  cod_nota = :ls_cod_nota
						using      sqlcb;
										  
						//destroy sqlcb;
							
					else
							
						updateblob tes_ord_acq_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_registrazione = :li_anno_registrazione and 
									  num_registrazione = :ll_num_registrazione and 
									  cod_nota = :ls_cod_nota;
										  
					end if
					
//					update tes_ord_acq_note
//					set    prog_mimetype = :ll_prog_mimetype
//					where  cod_azienda = :s_cs_xx.cod_azienda and
//							 anno_registrazione = :li_anno_registrazione and 
//							 num_registrazione = :ll_num_registrazione and 
//							 cod_nota = :ls_cod_nota;								
						
				case "Bol_Ven"
					
					if ls_db = "MSSQL" then
							
						li_risposta = f_crea_sqlcb(sqlcb)
							
						updateblob tes_bol_ven_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_registrazione = :li_anno_registrazione and 
									  num_registrazione = :ll_num_registrazione and 
									  tes_bol_ven_note.cod_nota = :ls_cod_nota
						using      sqlcb;
							
						//destroy sqlcb;
						
					else
							
						updateblob tes_bol_ven_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_registrazione = :li_anno_registrazione and 
									  num_registrazione = :ll_num_registrazione and 
									  cod_nota = :ls_cod_nota;
										  
					end if
					
//					update tes_bol_ven_note
//					set    prog_mimetype = :ll_prog_mimetype
//					where  cod_azienda = :s_cs_xx.cod_azienda and
//							 anno_registrazione = :li_anno_registrazione and 
//							 num_registrazione = :ll_num_registrazione and 
//							 cod_nota = :ls_cod_nota;									
									
				case "Bol_Acq"
					
					if ls_db = "MSSQL" then
						
						li_risposta = f_crea_sqlcb(sqlcb)
						
						updateblob tes_bol_acq_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_bolla_acq = :li_anno_bolla_acq and 
									  num_bolla_acq = :ll_num_bolla_acq and 
									  cod_nota = :ls_cod_nota
						using      sqlcb;
						
						//destroy sqlcb;
						
					else
							
						updateblob tes_bol_acq_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_bolla_acq = :li_anno_bolla_acq and 
									  num_bolla_acq = :ll_num_bolla_acq and 
									  cod_nota = :ls_cod_nota;
									  
					end if
					
//					update tes_bol_acq_note
//					set    prog_mimetype = :ll_prog_mimetype
//					where  cod_azienda = :s_cs_xx.cod_azienda and
//							 anno_bolla_acq = :li_anno_bolla_acq and 
//							 num_bolla_acq = :ll_num_bolla_acq and 
//							 cod_nota = :ls_cod_nota;				
									
				case "Fat_Ven"
					
					if ls_db = "MSSQL" then
							
						li_risposta = f_crea_sqlcb(sqlcb)
						
						updateblob tes_fat_ven_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_registrazione = :li_anno_registrazione and 
									  num_registrazione = :ll_num_registrazione and 
									  cod_nota = :ls_cod_nota
						using      sqlcb;
						
						//destroy sqlcb;
						
					else
							
						updateblob tes_fat_ven_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_registrazione = :li_anno_registrazione and 
									  num_registrazione = :ll_num_registrazione and 
									  cod_nota = :ls_cod_nota;
						
					end if
					
//					update tes_fat_ven_note
//					set    prog_mimetype = :ll_prog_mimetype
//					where  cod_azienda = :s_cs_xx.cod_azienda and
//							 anno_registrazione = :li_anno_registrazione and 
//							 num_registrazione = :ll_num_registrazione and 
//							 cod_nota = :ls_cod_nota;					
					
				case "Fat_Acq"
					
					if ls_db = "MSSQL" then
							
						li_risposta = f_crea_sqlcb(sqlcb)
						
						updateblob tes_fat_acq_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_registrazione = :li_anno_registrazione and 
									  num_registrazione = :ll_num_registrazione and 
									  cod_nota = :ls_cod_nota
						using      sqlcb;
							
						//destroy sqlcb;
							
					else
							
						updateblob tes_fat_acq_note
						set        note_esterne = :s_cs_xx.parametri.parametro_bl_1
						where      cod_azienda = :s_cs_xx.cod_azienda and
									  anno_registrazione = :li_anno_registrazione and 
									  num_registrazione = :ll_num_registrazione and 
									  cod_nota = :ls_cod_nota;
									  
					end if
					
//					update tes_fat_acq_note
//					set    prog_mimetype = :ll_prog_mimetype
//					where  cod_azienda = :s_cs_xx.cod_azienda and
//							 anno_registrazione = :li_anno_registrazione and 
//							 num_registrazione = :ll_num_registrazione and 
//							 cod_nota = :ls_cod_nota;
									
			end choose	
			
			if ls_db = "MSSQL" then
				commit using sqlcb;
				disconnect using sqlcb;
				destroy sqlcb;
			else
				
			end if
			
			//commit;
			
			
			dw_acq_ven_note_lista.setitem( dw_acq_ven_note_lista.getrow(), "prog_mimetype", ll_prog_mimetype)				
			parent.triggerevent("pc_save")				
			parent.triggerevent("pc_view")						
			
		end if
		
	end if
	
end if

end event

