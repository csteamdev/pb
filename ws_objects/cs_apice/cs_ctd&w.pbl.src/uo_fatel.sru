﻿$PBExportHeader$uo_fatel.sru
forward
global type uo_fatel from nonvisualobject
end type
end forward

global type uo_fatel from nonvisualobject
end type
global uo_fatel uo_fatel

type variables
public:
		
		PBDOM_ELEMENT pbdom_header, pbdom_body
		
private:

// Elementi Header

PBDOM_ELEMENT 	pdel_datitrasmissione, &
						pdel_idtrasmittente, &
						pdel_idpaese, &
						pdel_idcodice, &
						pdel_progressivoinvio, &
						pdel_formatotrasmissione, &
						pdel_codicedestinatario, &
						pdel_contattitrasmittente, &
						pdel_contattitrasmittente_telefono ,&
						pdel_contattitrasmittente_email ,&
						pdel_pecdestinatario ,&
						pdel_cedenteprestatore, &
						pdel_cedenteprestatore_datianagrafici ,&
						pdel_cedenteprestatore_idfiscaleiva ,&
						pdel_cedenteprestatore_idpaese ,&
						pdel_cedenteprestatore_idcodice ,&
						pdel_cedenteprestatore_codicefiscale ,&
						pdel_cedenteprestatore_anagrafica ,&
						pdel_cedenteprestatore_denominazione ,&
						pdel_cedenteprestatore_nome ,&
						pdel_cedenteprestatore_cognome ,&
						pdel_cedenteprestatore_titolo ,&
						pdel_cedenteprestatore_codeori ,&
						pdel_cedenteprestatore_regimefiscale ,&
						pdel_cedenteprestatore_sede ,&
						pdel_cedenteprestatore_sede_indirizzo ,&
						pdel_cedenteprestatore_sede_numerocivico ,&
						pdel_cedenteprestatore_sede_cap ,&
						pdel_cedenteprestatore_sede_comune ,&
						pdel_cedenteprestatore_sede_provincia ,&
						pdel_cedenteprestatore_sede_nazione, &
						pdel_cedenteprestatore_contatti ,&
						pdel_cedenteprestatore_telefono ,&
						pdel_cedenteprestatore_fax ,&
						pdel_cedenteprestatore_email ,&
						pdel_cedenteprestatore_riferimentoamministrativo,&
						pdel_cessionariocommittente, &
						pdel_cessionariocommittente_datianagrafici, &						
						pdel_cessionariocommittente_idfiscaleiva, &						
						pdel_cessionariocommittente_idpaese, &						
						pdel_cessionariocommittente_idcodice, &						
						pdel_cessionariocommittente_codicefiscale, &						
						pdel_cessionariocommittente_anagrafica, &						
						pdel_cessionariocommittente_denominazione, &						
						pdel_cessionariocommittente_nome, &						
						pdel_cessionariocommittente_cognome, &						
						pdel_cessionariocommittente_codeori, &						
						pdel_cessionariocommittente_sede ,&
						pdel_cessionariocommittente_sede_indirizzo ,&
						pdel_cessionariocommittente_sede_numerocivico ,&
						pdel_cessionariocommittente_sede_cap ,&
						pdel_cessionariocommittente_sede_comune ,&
						pdel_cessionariocommittente_sede_provincia ,&
						pdel_cessionariocommittente_sede_nazione
						
// Elementi Body
//PBDOM_TEXT 		pdel_body_datibeniservizi_dett_descrizione
PBDOM_ELEMENT 	pdel_body_datigenerali, &
						pdel_body_datigeneralidocumento, &
						pdel_body_datigeneralidocumento_tipodocumento,&
						pdel_body_datigeneralidocumento_divisa, &
						pdel_body_datigeneralidocumento_data, &
						pdel_body_datigeneralidocumento_numero, &
						pdel_body_datigeneralidocumento_importo, &
						pdel_body_datigeneralidocumento_causale, &
						pdel_body_datigeneralidocumento_datiordineacquisto[], &		
						pdel_body_datigeneralidocumento_datifatturecollegate[], &		
						pdel_body_datigeneralidocumento_riferimentonumerolinea, &
						pdel_body_datigeneralidocumento_iddocumento, &
						pdel_body_datigeneralidocumento_codicecig, &
						pdel_body_datigeneralidocumento_numitem, &
						pdel_body_datigeneralidocumento_datibollo, &
						pdel_body_datigeneralidocumento_bollovirtuale, &
						pdel_body_datigeneralidocumento_importobollo, &
						pdel_body_datibeniservizi, &
						pdel_body_datibeniservizi_dett[],&
						pdel_body_datibeniservizi_dett_numerolinea, &
						pdel_body_datibeniservizi_dett_tipocessione, &
						pdel_body_datibeniservizi_dett_codart[], &
						pdel_body_datibeniservizi_dett_codart_tipo, &
						pdel_body_datibeniservizi_dett_codart_codice, & 
						pdel_body_datibeniservizi_dett_descrizione, &
						pdel_body_datibeniservizi_dett_quantita, &
						pdel_body_datibeniservizi_dett_um, &
						pdel_body_datibeniservizi_dett_prezzo, &
						pdel_body_datibeniservizi_dett_sconti[], &
						pdel_body_datibeniservizi_dett_sconti_tipo, &
						pdel_body_datibeniservizi_dett_sconti_percentuale, &
						pdel_body_datibeniservizi_dett_imponibile, &
						pdel_body_datibeniservizi_dett_aliquota, &
						pdel_body_datibeniservizi_dett_des_esenzione, &
						pdel_body_datibeniservizi_riepilogo[], &
						pdel_body_datibeniservizi_riepilogo_aliquota, &
						pdel_body_datibeniservizi_riepilogo_natura, &
						pdel_body_datibeniservizi_riepilogo_imponibile, &
						pdel_body_datibeniservizi_riepilogo_imposta, &
						pdel_body_datibeniservizi_riepilogo_esegibilita, &
						pdel_body_datibeniservizi_riepilogo_riferimentonormativo, &
						pdel_body_datipagamenti, &
						pdel_body_datipag_condizionipagamento, &
						pdel_body_datipag_dettaglio_pagamento[], &
						pdel_body_datipag_dettaglio_modalita, &
						pdel_body_datipag_dettaglio_scadenza, &
						pdel_body_datipag_dettaglio_importo, &
						pdel_body_datipag_dettaglio_istituto_finanziario, &
						pdel_body_datipag_dettaglio_iban, &
						pdel_body_datipag_dettaglio_abi, &
						pdel_body_datipag_dettaglio_cab, &
						pdel_body_datipag_dettaglio_bic
						


public:
// #### variabili che poi vengono scritte nella struttura XML
//  HEADER DATI TRASMISSIONE
string	is_idtrasmiittente_idpaese, is_idtrasmiittente_idcodice, is_datitrasmissione_progressivoinvio, is_datitrasmissione_formatotrasmissione, &
is_datitrasmissione_codicedestinatario, is_contattitrasmittente_telefono, is_contattitrasmittente_email, is_datitrasmissione_pecdestinatario
// HEADER CEDENTE PRESTATORE
string is_cedenteprestatore_id_paese, is_cedenteprestatore_idcodice, is_cedenteprestatore_codicefiscale, &
is_cedenteprestatore_denominazione, is_cedenteprestatore_nome, is_cedenteprestatore_cognome,is_cedenteprestatore_titolo, is_cedenteprestatore_codeori,is_cedenteprestatore_regimefiscale, &
is_cedenteprestatoresede_indirizzo, is_cedenteprestatoresede_numerocivico, is_cedenteprestatoresede_cap, is_cedenteprestatoresede_comune, &
is_cedenteprestatoresede_provincia, is_cedenteprestatoresede_nazione, is_cedenteprestatore_telefono, is_cedenteprestatore_fax, is_cedenteprestatore_email, &
is_cedenteprestatore_riferimento_amministrativo
//  HEADER CESSIONARIO COMMITTENTE
string	is_cessionario_idpaese, is_cessionario_idcodice, is_cessionario_codicefiscale, is_cessionario_denominazione, is_cessionario_nome, is_cessionario_cognome,&
is_cessionariosede_indirizzo, is_cessionariosede_numerocivico, is_cessionariosede_cap, is_cessionariosede_comune, is_cessionariosede_provincia, &
is_cessionariosede_nazione
//   BODY DATI GENERALI
string	is_datigen_tipo_documento, is_datigen_divisa, is_datigen_data, is_datigen_numero, is_datigen_totaledocumento, is_datigen_causale, is_datigen_nota_testata, is_datigen_nota_piede, &
		is_datigen_bollovirtuale, is_datigen_importo_bollo
str_fatel_datiordineacquisto istr_body_dati_ordine_acquisto[]
//   BODY DATI BENI SERVIZI
str_fatel_dettaglio_linee istr_bodybeni_dettaglio_linee[]
str_fatel_dati_riepilogo istr_bodybeni_dati_riepilogo[]
//   BODY DATI PAGAMENTI
string is_condizioni_pagamento
str_fatel_dett_pagamenti istr_body_pagamenti[]

// -------------  variabili per  archiviazione su Elastic Search ----------------- //
string is_elastic_metada[]
string	is_fatel_index[]

// -------------  usata per stabilire CIG e IDDOCUMENTO su fattura PA ----------------- //
string is_tipo_rif_datiordineacquisto="0"
// 0 = nessun riferimento
// 1 = un riferimento per tutta la fattura (quindi il ruiferimentonumerolinea viene omesso
// 2 = più riferimenti, quindi deve essere precisata la linea


end variables
forward prototypes
public function integer uof_header_create ()
private subroutine uof_add_header_codicedestinatario ()
private subroutine uof_add_header_formatotrasmissione ()
private subroutine uof_add_header_idpaese ()
private subroutine uof_add_header_progressivoinvio ()
private subroutine uof_add_header_idcodice ()
private subroutine uof_element_create (ref pbdom_element apdel_argument, string as_elementname)
private subroutine uof_add_header_email ()
private subroutine uof_add_header_telefono ()
private subroutine uof_add_header_pecdestinatario ()
public function integer uof_header_validate (ref string as_message)
private subroutine uof_add_header_cedente_idpaese ()
private subroutine uof_add_header_cedente_codicefiscale ()
private subroutine uof_add_header_cedente_denominazione ()
private subroutine uof_add_header_cedente_idcodice ()
private subroutine uof_add_header_cedente_nome ()
private subroutine uof_add_header_cedente_cognome ()
private subroutine uof_add_header_cedente_regimefiscale ()
private subroutine uof_header_cedentesede_indirizzo ()
private subroutine uof_header_cedentesede_numerocivico ()
private subroutine uof_header_cedentesede_cap ()
private subroutine uof_header_cedentesede_comune ()
private subroutine uof_header_cedentesede_provincia ()
private subroutine uof_header_cedentesede_nazione ()
private subroutine uof_add_header_cedente_telefono ()
private subroutine uof_add_header_cedente_fax ()
private subroutine uof_add_header_cedente_email ()
private subroutine uof_add_header_cessionario_idpaese ()
private subroutine uof_add_header_cessionario_idcodice ()
private subroutine uof_add_header_cessionario_denominazione ()
private subroutine uof_add_header_cessionario_nome ()
private subroutine uof_add_header_cessionario_cognome ()
private subroutine uof_add_header_cessionariosede_indirizzo ()
private subroutine uof_add_header_cessionariosede_numerociv ()
private subroutine uof_add_header_cessionariosede_cap ()
private subroutine uof_add_header_cessionariosede_comune ()
private subroutine uof_add_header_cessionariosede_provincia ()
private subroutine uof_add_header_cessionariosede_nazione ()
public function integer uof_body_create ()
private subroutine uof_add_body_divisa ()
private subroutine uof_add_body_numero ()
private subroutine uof_add_body_data ()
private subroutine uof_add_body_totaledocumento ()
public function string uof_dec_to_string (decimal as_decimal)
private subroutine uof_add_body_tipodocumento ()
private subroutine uof_add_body_det_numerolinea (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_det_tipocessione (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_det_codicetipo (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_det_codicevalore (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_det_descrizione (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_det_quantita (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_det_prezzo (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_det_aliquota (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_det_esenzione (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_det_imponibile (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_det_scontiperc (ref pbdom_element apbdom_element, long al_idx, string as_percentuale)
private subroutine uof_add_body_det_scontitipo (ref pbdom_element apbdom_element, long al_idx, string as_tipo_sconto)
private subroutine uof_add_body_det_um (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_header_cedente_titolo ()
private subroutine uof_add_header_cedente_codeori ()
private subroutine uof_add_body_riepilogo_aliquota (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_riepilogo_natura (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_riepilogo_imponibile (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_riepilogo_imposta (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_riepilogo_rif_norma (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_riepilogo_esigibilita (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_condizioni_pagamento ()
private subroutine uof_add_body_pag_modalita_pag (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_pag_data_scadenza (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_pag_importo_scadenza (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_pag_iban (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_pag_abi (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_pag_cab (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_pag_bic (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_pag_nome_banca (ref pbdom_element apbdom_element, long al_indice)
public function integer uof_fattura_xml (long al_anno_registrazione, long al_num_registrazione, ref string as_message)
public function integer uof_fattura_passiva_download_xml (long al_progessivo_id, ref string as_message)
private function integer uof_notifica_format_child (pbdom_element apbdom_elem, ref string as_testo_notifica, ref string as_identificativo_sdi)
public function integer uof_notifica_fatture_attive_download_xml (long al_progessivo_id, ref string as_testo_notifica, ref string as_message)
public function integer uof_notifiche_lista (string as_tipo_fattura, ref string as_message, ref str_fatel_notifica astr_notifiche[])
public function integer uof_fatture_passive_lista (ref string as_message, ref str_fatel_lista_fatture_passive astr_lista[])
public function integer uof_fattura_passiva_download_pdf (long al_progessivo_id, ref string as_message, ref blob ab_blob)
public function integer uof_fattura_archivia_nota (long al_anno_registrazione, long al_num_registrazione, readonly string as_tipo_fattura, readonly string as_tipo_nota, readonly string as_descrizione, readonly string as_nota, readonly string as_path_file, ref string as_error)
public function integer uof_fattura_archivia_nota (long al_anno_registrazione, long al_num_registrazione, readonly string as_tipo_fattura, readonly string as_tipo_nota, readonly string as_descrizione, readonly string as_nota, readonly blob ab_blob, readonly long al_prog_mimetype, ref string as_error)
public function integer uof_conferma_fattura_passiva (long al_progessivo_id, ref string as_message)
public function integer uof_fatture_passive_allegati_lista (long al_id_fattura, ref str_fatel_lista_allegati_fat_acq astr_lista[], ref string as_message)
public function integer uof_fattura_passiva_download_allegato (long al_progessivo_id, ref string as_message, ref blob ab_blob, ref string as_file_ext, ref string as_file_code)
private subroutine uof_add_body_causale ()
private subroutine uof_add_body_conai_assolto ()
private subroutine uof_add_body_datibollo ()
private function string uof_notifica_format (string as_xml, ref string as_format, ref string as_identificativo_sdi)
private subroutine uof_add_header_cessionario_codicefiscale ()
private subroutine uof_add_body_riferimentonumerolinea (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_iddocumento (ref pbdom_element apbdom_element, long al_indice)
private subroutine uof_add_body_codicecig (ref pbdom_element apbdom_element, long al_indice)
public function integer uof_invia_fattura (long al_anno_registrazione, long al_num_registrazione, string as_flag_cliente_pa, string as_path_file_p7m, string as_nome_file_p7m, ref string as_identificativo_sdi, ref string as_message, ref string as_error)
public subroutine uof_get_sigla_anno (long al_anno_registrazione, ref string as_sigla)
private subroutine uof_add_body_numitem (ref pbdom_element apbdom_element, long al_indice)
end prototypes

public function integer uof_header_create ();uof_element_create( ref pdel_datitrasmissione,"DatiTrasmissione")
uof_element_create( ref pdel_idtrasmittente,"IdTrasmittente")
uof_element_create( ref pdel_contattitrasmittente,"ContattiTrasmittente")
uof_element_create( ref pdel_cedenteprestatore,"CedentePrestatore")
uof_element_create( ref pdel_cedenteprestatore_datianagrafici,"DatiAnagrafici")
uof_element_create( ref pdel_cedenteprestatore_codicefiscale,"CodiceFiscale")
uof_element_create( ref pdel_cedenteprestatore_idfiscaleiva,"IdFiscaleIVA")
uof_element_create( ref pdel_cedenteprestatore_anagrafica,"Anagrafica")
uof_element_create( ref pdel_cedenteprestatore_sede,"Sede")
uof_element_create( ref pdel_cedenteprestatore_contatti,"Contatti")
uof_element_create( ref pdel_cessionariocommittente,"CessionarioCommittente")
uof_element_create( ref pdel_cessionariocommittente_datianagrafici,"DatiAnagrafici")
uof_element_create( ref pdel_cessionariocommittente_codicefiscale,"CodiceFiscale")
uof_element_create( ref pdel_cessionariocommittente_idfiscaleiva,"IdFiscaleIVA")
uof_element_create( ref pdel_cessionariocommittente_anagrafica,"Anagrafica")
uof_element_create( ref pdel_cessionariocommittente_sede,"Sede")


// creo la struttua XML della testata
TRY
	pbdom_header.addcontent(pdel_datitrasmissione)
		pdel_datitrasmissione.addcontent(pdel_idtrasmittente)
			uof_add_header_idpaese()
			uof_add_header_idcodice()
		uof_add_header_progressivoinvio()
		uof_add_header_formatotrasmissione()
		uof_add_header_codiceDestinatario()
		pdel_datitrasmissione.addcontent(pdel_contattitrasmittente)
			uof_add_header_telefono()	
			uof_add_header_email()
		uof_add_header_pecdestinatario()

	pbdom_header.addcontent(pdel_cedenteprestatore)
		pdel_cedenteprestatore.addcontent(pdel_cedenteprestatore_datianagrafici)
			pdel_cedenteprestatore_datianagrafici.addcontent(pdel_cedenteprestatore_idfiscaleiva)
				uof_add_header_cedente_idpaese()
				uof_add_header_cedente_idcodice()
			uof_add_header_cedente_codicefiscale()
			pdel_cedenteprestatore_datianagrafici.addcontent(pdel_cedenteprestatore_anagrafica)
				if not isnull(is_cedenteprestatore_denominazione) and len(is_cedenteprestatore_denominazione) > 0 then
					uof_add_header_cedente_denominazione()
				else
					uof_add_header_cedente_nome()
					uof_add_header_cedente_cognome()
					uof_add_header_cedente_titolo()
					uof_add_header_cedente_codeori()
				end if
			uof_add_header_cedente_regimefiscale( )
			
		pdel_cedenteprestatore.addcontent(pdel_cedenteprestatore_sede)
			uof_header_cedentesede_indirizzo()
			uof_header_cedentesede_numerocivico( )
			uof_header_cedentesede_cap( )
			uof_header_cedentesede_comune( )
			uof_header_cedentesede_provincia( )
			uof_header_cedentesede_nazione( )
		pdel_cedenteprestatore.addcontent(pdel_cedenteprestatore_contatti)
			uof_add_header_cedente_telefono()
			uof_add_header_cedente_fax()
			uof_add_header_cedente_email()
		
	pbdom_header.addcontent(pdel_cessionariocommittente)
		pdel_cessionariocommittente.addcontent(pdel_cessionariocommittente_datianagrafici)
			if is_datitrasmissione_formatotrasmissione="FPA12" then
				uof_add_header_cessionario_codicefiscale()
			else
				pdel_cessionariocommittente_datianagrafici.addcontent(pdel_cessionariocommittente_idfiscaleiva)
					uof_add_header_cessionario_idpaese( )
					uof_add_header_cessionario_idcodice( )
			end if
			pdel_cessionariocommittente_datianagrafici.addcontent(pdel_cessionariocommittente_anagrafica)
				uof_add_header_cessionario_denominazione( )
				//uof_add_header_cessionario_nome( )
				//uof_add_header_cessionario_cognome( )
		pdel_cessionariocommittente.addcontent(pdel_cessionariocommittente_sede)
				uof_add_header_cessionariosede_indirizzo( )
				uof_add_header_cessionariosede_numerociv( )
				uof_add_header_cessionariosede_cap( )
				uof_add_header_cessionariosede_comune( )
				uof_add_header_cessionariosede_provincia( )
				uof_add_header_cessionariosede_nazione( )
		
CATCH ( PBDOM_Exception pbde )   
	MessageBox( "PBDOM Exception", pbde.getMessage() )
CATCH ( PBXRuntimeError re )   
	MessageBox( "PBNI Exception", re.getMessage() )
END TRY

return 0

end function

private subroutine uof_add_header_codicedestinatario ();uof_element_create( ref pdel_codicedestinatario,"CodiceDestinatario")
pdel_codicedestinatario.setText(is_datitrasmissione_codicedestinatario)
pdel_datitrasmissione.addcontent(pdel_codicedestinatario)

end subroutine

private subroutine uof_add_header_formatotrasmissione ();uof_element_create( ref pdel_formatotrasmissione,"FormatoTrasmissione")
pdel_formatotrasmissione.setText(is_datitrasmissione_formatotrasmissione)
pdel_datitrasmissione.addcontent(pdel_formatotrasmissione)


end subroutine

private subroutine uof_add_header_idpaese ();uof_element_create( ref pdel_idpaese,"IdPaese")
pdel_idpaese.setText(is_idtrasmiittente_idpaese)
pdel_idtrasmittente.addcontent(pdel_idpaese)

end subroutine

private subroutine uof_add_header_progressivoinvio ();uof_element_create( ref pdel_progressivoinvio,"ProgressivoInvio")
pdel_progressivoinvio.setText(is_datitrasmissione_progressivoinvio)
pdel_datitrasmissione.addcontent(pdel_progressivoinvio)

end subroutine

private subroutine uof_add_header_idcodice ();uof_element_create( ref pdel_idcodice,"IdCodice")
pdel_idcodice.setText(is_idtrasmiittente_idcodice)
pdel_idtrasmittente.addcontent(pdel_idcodice)

end subroutine

private subroutine uof_element_create (ref pbdom_element apdel_argument, string as_elementname);apdel_argument = create PBDOM_ELEMENT

apdel_argument.setname(as_elementname)
end subroutine

private subroutine uof_add_header_email ();if not g_str.isempty(is_contattitrasmittente_email) and len(is_contattitrasmittente_email) >= 7 then
	uof_element_create( ref pdel_contattitrasmittente_email,"Email")
	pdel_contattitrasmittente_email.setText(is_contattitrasmittente_email)
	pdel_contattitrasmittente.addcontent(pdel_contattitrasmittente_email)
end if

end subroutine

private subroutine uof_add_header_telefono ();if not g_str.isempty(is_contattitrasmittente_telefono) and len(is_contattitrasmittente_telefono) >= 5 then
	uof_element_create( ref pdel_contattitrasmittente_telefono,"Telefono")
	pdel_contattitrasmittente_telefono.setText(is_contattitrasmittente_telefono)
	pdel_contattitrasmittente.addcontent(pdel_contattitrasmittente_telefono)
end if
end subroutine

private subroutine uof_add_header_pecdestinatario ();if not g_str.isempty(is_datitrasmissione_pecdestinatario)  then
	uof_element_create( ref pdel_pecdestinatario,"PECDestinatario")
	pdel_pecdestinatario.setText(is_datitrasmissione_pecdestinatario)
	pdel_datitrasmissione.addcontent(pdel_pecdestinatario)
end if
end subroutine

public function integer uof_header_validate (ref string as_message);/*  funzione di validazione dei dati della HEADER.
return 	0	= tutto ok
			-1	= errore
as_message= eventuale messaggio di errore
*/
if g_str.isempty(is_idtrasmiittente_idpaese) then 
	as_message="ID paese trasmittente: dato obbligatorio!"
	return -1
end if

if g_str.isempty(is_idtrasmiittente_idcodice) then
	as_message="ID codice trasmittente: dato obbligatorio!"
	return -1
end if
	
if g_str.isempty(is_datitrasmissione_progressivoinvio) then
	as_message="Progressivo Invio: dato obbligatorio!"
	return -1
end if
	
if g_str.isempty(is_datitrasmissione_formatotrasmissione) then
	as_message="Formato Trasmissione : dato obbligatorio!"
	return -1
end if
	
if g_str.isempty(is_datitrasmissione_codicedestinatario) and g_str.isempty(is_datitrasmissione_pecdestinatario) then
	as_message="Codice SDI destinatario oppure PEC destinatario: dati obbligatori!"
	return -1
end if

if g_str.isempty(is_datitrasmissione_codicedestinatario) then
	// se sono arrivato qui vuol dire che nella PEC DEST c'è qualcosa
	is_datitrasmissione_codicedestinatario = "0000000"
end if

// i dati non obbligatori (Es. telefono, fax ,etc) )vengono controllato nelle singole funzioni; così se sono vuoti evito semplicemente di inserirli.

end function

private subroutine uof_add_header_cedente_idpaese ();if not g_str.isempty(is_cedenteprestatore_id_paese)then
	uof_element_create( ref pdel_cedenteprestatore_idpaese,"IdPaese")
	pdel_cedenteprestatore_idpaese.setText(is_cedenteprestatore_id_paese)
	pdel_cedenteprestatore_idfiscaleiva.addcontent(pdel_cedenteprestatore_idpaese)
end if
end subroutine

private subroutine uof_add_header_cedente_codicefiscale ();if not g_str.isempty(is_cedenteprestatore_codicefiscale)then
	uof_element_create( ref pdel_cedenteprestatore_codicefiscale,"CodiceFiscale")
	pdel_cedenteprestatore_codicefiscale.setText(is_cedenteprestatore_codicefiscale)
	pdel_cedenteprestatore_datianagrafici.addcontent(pdel_cedenteprestatore_codicefiscale)
end if
end subroutine

private subroutine uof_add_header_cedente_denominazione ();if not g_str.isempty(is_cedenteprestatore_denominazione)then
	uof_element_create( ref pdel_cedenteprestatore_denominazione,"Denominazione")
	pdel_cedenteprestatore_denominazione.setText(is_cedenteprestatore_denominazione)
	pdel_cedenteprestatore_anagrafica.addcontent(pdel_cedenteprestatore_denominazione)
end if
end subroutine

private subroutine uof_add_header_cedente_idcodice ();if not g_str.isempty(is_cedenteprestatore_idcodice)then
	uof_element_create( ref pdel_cedenteprestatore_idcodice,"IdCodice")
	pdel_cedenteprestatore_idcodice.setText(is_cedenteprestatore_idcodice)
	pdel_cedenteprestatore_idfiscaleiva.addcontent(pdel_cedenteprestatore_idcodice)
end if
end subroutine

private subroutine uof_add_header_cedente_nome ();if not g_str.isempty(is_cedenteprestatore_nome)then
	uof_element_create( ref pdel_cedenteprestatore_nome,"Nome")
	pdel_cedenteprestatore_nome.setText(is_cedenteprestatore_nome)
	pdel_cedenteprestatore_anagrafica.addcontent(pdel_cedenteprestatore_nome)
end if
end subroutine

private subroutine uof_add_header_cedente_cognome ();if not g_str.isempty(is_cedenteprestatore_cognome)then
	uof_element_create( ref pdel_cedenteprestatore_cognome,"Cognome")
	pdel_cedenteprestatore_cognome.setText(is_cedenteprestatore_cognome)
	pdel_cedenteprestatore_anagrafica.addcontent(pdel_cedenteprestatore_cognome)
end if
end subroutine

private subroutine uof_add_header_cedente_regimefiscale ();if not g_str.isempty(is_cedenteprestatore_regimefiscale)then
	uof_element_create( ref pdel_cedenteprestatore_regimefiscale,"RegimeFiscale")
	pdel_cedenteprestatore_regimefiscale.setText(is_cedenteprestatore_regimefiscale)
	pdel_cedenteprestatore_datianagrafici.addcontent(pdel_cedenteprestatore_regimefiscale)
end if
end subroutine

private subroutine uof_header_cedentesede_indirizzo ();if not g_str.isempty(is_cedenteprestatoresede_indirizzo)then
	uof_element_create( ref pdel_cedenteprestatore_sede_indirizzo,"Indirizzo")
	pdel_cedenteprestatore_sede_indirizzo.setText(is_cedenteprestatoresede_indirizzo)
	pdel_cedenteprestatore_sede.addcontent(pdel_cedenteprestatore_sede_indirizzo)
end if
end subroutine

private subroutine uof_header_cedentesede_numerocivico ();if not g_str.isempty(is_cedenteprestatoresede_numerocivico)then
	uof_element_create( ref pdel_cedenteprestatore_sede_numerocivico,"NumeroCivico")
	pdel_cedenteprestatore_sede_numerocivico.setText(is_cedenteprestatoresede_numerocivico)
	pdel_cedenteprestatore_sede.addcontent(pdel_cedenteprestatore_sede_numerocivico)
end if
end subroutine

private subroutine uof_header_cedentesede_cap ();if not g_str.isempty(is_cedenteprestatoresede_cap)then
	uof_element_create( ref pdel_cedenteprestatore_sede_cap,"CAP")
	pdel_cedenteprestatore_sede_cap.setText(is_cedenteprestatoresede_cap)
	pdel_cedenteprestatore_sede.addcontent(pdel_cedenteprestatore_sede_cap)
end if
end subroutine

private subroutine uof_header_cedentesede_comune ();if not g_str.isempty(is_cedenteprestatoresede_comune)then
	uof_element_create( ref pdel_cedenteprestatore_sede_comune,"Comune")
	pdel_cedenteprestatore_sede_comune.setText(is_cedenteprestatoresede_comune)
	pdel_cedenteprestatore_sede.addcontent(pdel_cedenteprestatore_sede_comune)
end if
end subroutine

private subroutine uof_header_cedentesede_provincia ();if not g_str.isempty(is_cedenteprestatoresede_provincia)then
	uof_element_create( ref pdel_cedenteprestatore_sede_provincia,"Provincia")
	pdel_cedenteprestatore_sede_provincia.setText(is_cedenteprestatoresede_provincia)
	pdel_cedenteprestatore_sede.addcontent(pdel_cedenteprestatore_sede_provincia)
end if
end subroutine

private subroutine uof_header_cedentesede_nazione ();if not g_str.isempty(is_cedenteprestatoresede_nazione)then
	uof_element_create( ref pdel_cedenteprestatore_sede_nazione,"Nazione")
	pdel_cedenteprestatore_sede_nazione.setText(is_cedenteprestatoresede_nazione)
	pdel_cedenteprestatore_sede.addcontent(pdel_cedenteprestatore_sede_nazione)
end if
end subroutine

private subroutine uof_add_header_cedente_telefono ();if not g_str.isempty(is_cedenteprestatore_telefono)then
	uof_element_create( ref pdel_cedenteprestatore_telefono,"Telefono")
	pdel_cedenteprestatore_telefono.setText(is_cedenteprestatore_telefono)
	pdel_cedenteprestatore_contatti.addcontent(pdel_cedenteprestatore_telefono)
end if
end subroutine

private subroutine uof_add_header_cedente_fax ();if not g_str.isempty(is_cedenteprestatore_fax)then
	uof_element_create( ref pdel_cedenteprestatore_fax,"Fax")
	pdel_cedenteprestatore_fax.setText(is_cedenteprestatore_fax)
	pdel_cedenteprestatore_contatti.addcontent(pdel_cedenteprestatore_fax)
end if
end subroutine

private subroutine uof_add_header_cedente_email ();if not g_str.isempty(is_cedenteprestatore_email)then
	uof_element_create( ref pdel_cedenteprestatore_email,"Email")
	pdel_cedenteprestatore_email.setText(is_cedenteprestatore_email)
	pdel_cedenteprestatore_contatti.addcontent(pdel_cedenteprestatore_email)
end if
end subroutine

private subroutine uof_add_header_cessionario_idpaese ();if not g_str.isempty(is_cessionario_idpaese)then
	uof_element_create( ref pdel_cessionariocommittente_idpaese,"IdPaese")
	pdel_cessionariocommittente_idpaese.setText(is_cessionario_idpaese)
	pdel_cessionariocommittente_idfiscaleiva.addcontent(pdel_cessionariocommittente_idpaese)
end if
end subroutine

private subroutine uof_add_header_cessionario_idcodice ();if not g_str.isempty(is_cessionario_idcodice)then
	uof_element_create( ref pdel_cessionariocommittente_idcodice,"IdCodice")
	pdel_cessionariocommittente_idcodice.setText(is_cessionario_idcodice)
	pdel_cessionariocommittente_idfiscaleiva.addcontent(pdel_cessionariocommittente_idcodice)
end if
end subroutine

private subroutine uof_add_header_cessionario_denominazione ();if not g_str.isempty(is_cessionario_denominazione)then
	uof_element_create( ref pdel_cessionariocommittente_denominazione,"Denominazione")
	pdel_cessionariocommittente_denominazione.setText(is_cessionario_denominazione)
	pdel_cessionariocommittente_anagrafica.addcontent(pdel_cessionariocommittente_denominazione)
end if
end subroutine

private subroutine uof_add_header_cessionario_nome ();if not g_str.isempty(is_cessionario_nome)then
	uof_element_create( ref pdel_cessionariocommittente_nome,"Nome")
	pdel_cessionariocommittente_nome.setText(is_cessionario_nome)
	pdel_cessionariocommittente_anagrafica.addcontent(pdel_cessionariocommittente_nome)
end if
end subroutine

private subroutine uof_add_header_cessionario_cognome ();if not g_str.isempty(is_cessionario_cognome)then
	uof_element_create( ref pdel_cessionariocommittente_cognome,"Cognome")
	pdel_cessionariocommittente_cognome.setText(is_cessionario_cognome)
	pdel_cessionariocommittente_anagrafica.addcontent(pdel_cessionariocommittente_cognome)
end if
end subroutine

private subroutine uof_add_header_cessionariosede_indirizzo ();if not g_str.isempty(is_cessionariosede_indirizzo)then
	uof_element_create( ref pdel_cessionariocommittente_sede_indirizzo,"Indirizzo")
	pdel_cessionariocommittente_sede_indirizzo.setText(is_cessionariosede_indirizzo)
	pdel_cessionariocommittente_sede.addcontent(pdel_cessionariocommittente_sede_indirizzo)
end if
end subroutine

private subroutine uof_add_header_cessionariosede_numerociv ();if not g_str.isempty(is_cessionariosede_numerocivico)then
	uof_element_create( ref pdel_cessionariocommittente_sede_numerocivico,"NumeroCivico")
	pdel_cessionariocommittente_sede_numerocivico.setText(is_cessionariosede_numerocivico)
	pdel_cessionariocommittente_sede.addcontent(pdel_cessionariocommittente_sede_numerocivico)
end if
end subroutine

private subroutine uof_add_header_cessionariosede_cap ();if not g_str.isempty(is_cessionariosede_cap)then
	uof_element_create( ref pdel_cessionariocommittente_sede_numerocivico,"CAP")
	pdel_cessionariocommittente_sede_numerocivico.setText(is_cessionariosede_cap)
	pdel_cessionariocommittente_sede.addcontent(pdel_cessionariocommittente_sede_numerocivico)
end if
end subroutine

private subroutine uof_add_header_cessionariosede_comune ();if not g_str.isempty(is_cessionariosede_comune)then
	uof_element_create( ref pdel_cessionariocommittente_sede_numerocivico,"Comune")
	pdel_cessionariocommittente_sede_numerocivico.setText(is_cessionariosede_comune)
	pdel_cessionariocommittente_sede.addcontent(pdel_cessionariocommittente_sede_numerocivico)
end if
end subroutine

private subroutine uof_add_header_cessionariosede_provincia ();if not g_str.isempty(is_cessionariosede_provincia)then
	uof_element_create( ref pdel_cessionariocommittente_sede_numerocivico,"Provincia")
	pdel_cessionariocommittente_sede_numerocivico.setText(is_cessionariosede_provincia)
	pdel_cessionariocommittente_sede.addcontent(pdel_cessionariocommittente_sede_numerocivico)
end if
end subroutine

private subroutine uof_add_header_cessionariosede_nazione ();if not g_str.isempty(is_cessionariosede_nazione)then
	uof_element_create( ref pdel_cessionariocommittente_sede_numerocivico,"Nazione")
	pdel_cessionariocommittente_sede_numerocivico.setText(is_cessionariosede_nazione)
	pdel_cessionariocommittente_sede.addcontent(pdel_cessionariocommittente_sede_numerocivico)
end if
end subroutine

public function integer uof_body_create ();boolean lb_conai
long ll_i, ll_sconti

uof_element_create( ref pdel_body_datigenerali,"DatiGenerali")
uof_element_create( ref pdel_body_datigeneralidocumento,"DatiGeneraliDocumento")
uof_element_create( ref pdel_body_datibeniservizi,"DatiBeniServizi")
uof_element_create( ref pdel_body_datipagamenti,"DatiPagamento")
uof_element_create( ref pdel_body_datigeneralidocumento_datibollo,"DatiBollo")



// creo la struttua XML de corpo
TRY
	pbdom_body.addcontent(pdel_body_datigenerali)
		pdel_body_datigenerali.addcontent(pdel_body_datigeneralidocumento)
			uof_add_body_tipodocumento()
			uof_add_body_divisa()			
			uof_add_body_data()
			uof_add_body_numero()		
			if is_datigen_bollovirtuale = "SI" then
				pdel_body_datigeneralidocumento.addcontent(pdel_body_datigeneralidocumento_datibollo)
					uof_add_body_datibollo( )
				end if
			uof_add_body_totaledocumento()
			uof_add_body_causale()
			// verifico se devo aggiungere la dicitura CONAI
			guo_functions.uof_get_parametro_azienda("CNI", lb_conai)
			if lb_conai then
				uof_add_body_conai_assolto()
			end if		
			
		for ll_i = 1 to upperbound(istr_body_dati_ordine_acquisto)		
			choose case is_datigen_tipo_documento
				// EnMe 05-01-2021 Caso della NC in cui deve esserci il riferimento alle righe della fattura originale
				case "TD04"
					uof_element_create( ref pdel_body_datigeneralidocumento_datifatturecollegate[ll_i] ,"DatiFattureCollegate")
					pdel_body_datigenerali.addcontent(pdel_body_datigeneralidocumento_datifatturecollegate[ll_i])
					if len(istr_body_dati_ordine_acquisto[ll_i].riferimentonumerolinea) > 0 then
						uof_add_body_riferimentonumerolinea(pdel_body_datigeneralidocumento_datifatturecollegate[ll_i], ll_i)
					end if				
					if len(istr_body_dati_ordine_acquisto[ll_i].iddocumento) > 0 then
						uof_add_body_iddocumento(pdel_body_datigeneralidocumento_datifatturecollegate[ll_i], ll_i)
					end if				
					if len(istr_body_dati_ordine_acquisto[ll_i].codicecig) > 0 then
						uof_add_body_codicecig(pdel_body_datigeneralidocumento_datifatturecollegate[ll_i], ll_i)
					end if				
					if len(istr_body_dati_ordine_acquisto[ll_i].numitem) > 0 then
						uof_add_body_numitem(pdel_body_datigeneralidocumento_datifatturecollegate[ll_i], ll_i)
					end if				
					
				case else
					uof_element_create( ref pdel_body_datigeneralidocumento_datiordineacquisto[ll_i] ,"DatiOrdineAcquisto")
					pdel_body_datigenerali.addcontent(pdel_body_datigeneralidocumento_datiordineacquisto[ll_i])
					if is_tipo_rif_datiordineacquisto = "2" then		// inserisco il riferimento alla linea documento solo se ho più riferimenti, altrimenti vuol dire che è relativo a tutta la fattura
						if len(istr_body_dati_ordine_acquisto[ll_i].riferimentonumerolinea) > 0 then
							uof_add_body_riferimentonumerolinea(pdel_body_datigeneralidocumento_datiordineacquisto[ll_i], ll_i)
						end if				
					end if
					if len(istr_body_dati_ordine_acquisto[ll_i].iddocumento) > 0 then
						uof_add_body_iddocumento(pdel_body_datigeneralidocumento_datiordineacquisto[ll_i], ll_i)
					end if				
					if len(istr_body_dati_ordine_acquisto[ll_i].codicecig) > 0 then
						uof_add_body_codicecig(pdel_body_datigeneralidocumento_datiordineacquisto[ll_i], ll_i)
					end if				
			end choose
		next
			
	pbdom_body.addcontent(pdel_body_datibeniservizi)
//		pdel_body_datibeniservizi.addcontent(pdel_body_datibeniservizi_dettaglio)
			for ll_i = 1 to upperbound(istr_bodybeni_dettaglio_linee)
				uof_element_create( ref pdel_body_datibeniservizi_dett[ll_i] ,"DettaglioLinee")
				pdel_body_datibeniservizi.addcontent(pdel_body_datibeniservizi_dett[ll_i])
				uof_add_body_det_numerolinea(pdel_body_datibeniservizi_dett[ll_i], ll_i)
				uof_add_body_det_tipocessione(pdel_body_datibeniservizi_dett[ll_i],ll_i)
				
				if len(istr_bodybeni_dettaglio_linee[ll_i].codice_valore) > 0 then
					uof_element_create( ref pdel_body_datibeniservizi_dett_codart[ll_i] ,"CodiceArticolo")
					pdel_body_datibeniservizi_dett[ll_i].addcontent(pdel_body_datibeniservizi_dett_codart[ll_i])
						uof_add_body_det_codicetipo(pdel_body_datibeniservizi_dett_codart[ll_i], ll_i)
						uof_add_body_det_codicevalore(pdel_body_datibeniservizi_dett_codart[ll_i], ll_i)
				end if	
				
				uof_add_body_det_descrizione(pdel_body_datibeniservizi_dett[ll_i],ll_i)
				uof_add_body_det_quantita(pdel_body_datibeniservizi_dett[ll_i],ll_i)
				uof_add_body_det_um(pdel_body_datibeniservizi_dett[ll_i],ll_i)
				uof_add_body_det_prezzo(pdel_body_datibeniservizi_dett[ll_i],ll_i)
				
				if upperbound(istr_bodybeni_dettaglio_linee[ll_i].sconti) > 0 then
					for ll_sconti = 1 to upperbound(istr_bodybeni_dettaglio_linee[ll_i].sconti)
						uof_element_create( ref pdel_body_datibeniservizi_dett_sconti[ll_i] ,"ScontoMaggiorazione")
						pdel_body_datibeniservizi_dett[ll_i].addcontent(pdel_body_datibeniservizi_dett_sconti[ll_i])
						uof_add_body_det_scontitipo(pdel_body_datibeniservizi_dett_sconti[ll_i], ll_i, istr_bodybeni_dettaglio_linee[ll_i].sconti[ll_sconti].sconto_tipo )
						uof_add_body_det_scontiperc(pdel_body_datibeniservizi_dett_sconti[ll_i], ll_i, istr_bodybeni_dettaglio_linee[ll_i].sconti[ll_sconti].sconto_perc )
					next
				end if
				
				uof_add_body_det_imponibile(pdel_body_datibeniservizi_dett[ll_i],ll_i)
				uof_add_body_det_aliquota(pdel_body_datibeniservizi_dett[ll_i],ll_i)
				uof_add_body_det_esenzione(pdel_body_datibeniservizi_dett[ll_i],ll_i)
			next

			for ll_i = 1 to upperbound(istr_bodybeni_dati_riepilogo)
				uof_element_create( ref pdel_body_datibeniservizi_riepilogo[ll_i] ,"DatiRiepilogo")
				pdel_body_datibeniservizi.addcontent(pdel_body_datibeniservizi_riepilogo[ll_i])
				uof_add_body_riepilogo_aliquota(pdel_body_datibeniservizi_riepilogo[ll_i], ll_i)
				if istr_bodybeni_dati_riepilogo[ll_i].riepilogo_aliquota_iva = "0.00" then
					uof_add_body_riepilogo_natura(pdel_body_datibeniservizi_riepilogo[ll_i], ll_i)
				end if
				uof_add_body_riepilogo_imponibile(pdel_body_datibeniservizi_riepilogo[ll_i], ll_i)
				uof_add_body_riepilogo_imposta(pdel_body_datibeniservizi_riepilogo[ll_i], ll_i)
				uof_add_body_riepilogo_esigibilita(pdel_body_datibeniservizi_riepilogo[ll_i], ll_i)
				if istr_bodybeni_dati_riepilogo[ll_i].riepilogo_aliquota_iva = "0.00" then
					uof_add_body_riepilogo_rif_norma(pdel_body_datibeniservizi_riepilogo[ll_i], ll_i)
				end if
			
			next

	pbdom_body.addcontent(pdel_body_datipagamenti)
			uof_add_body_condizioni_pagamento()
			for ll_i = 1 to upperbound(istr_body_pagamenti[])
				uof_element_create( ref pdel_body_datipag_dettaglio_pagamento[ll_i] ,"DettaglioPagamento")
				pdel_body_datipagamenti.addcontent(pdel_body_datipag_dettaglio_pagamento[ll_i])
				
				uof_add_body_pag_modalita_pag(pdel_body_datipag_dettaglio_pagamento[ll_i], ll_i)
				uof_add_body_pag_data_scadenza(pdel_body_datipag_dettaglio_pagamento[ll_i], ll_i)
				uof_add_body_pag_importo_scadenza(pdel_body_datipag_dettaglio_pagamento[ll_i], ll_i)
				uof_add_body_pag_nome_banca(pdel_body_datipag_dettaglio_pagamento[ll_i], ll_i)
				uof_add_body_pag_iban(pdel_body_datipag_dettaglio_pagamento[ll_i], ll_i)
				uof_add_body_pag_abi(pdel_body_datipag_dettaglio_pagamento[ll_i], ll_i)
				uof_add_body_pag_cab(pdel_body_datipag_dettaglio_pagamento[ll_i], ll_i)
				uof_add_body_pag_bic(pdel_body_datipag_dettaglio_pagamento[ll_i], ll_i)
			next
			
CATCH ( PBDOM_Exception pbde )   
	MessageBox( "PBDOM Exception", pbde.getMessage() )
CATCH ( PBXRuntimeError re )   
	MessageBox( "PBNI Exception", re.getMessage() )
END TRY

return 0

end function

private subroutine uof_add_body_divisa ();if not g_str.isempty(is_datigen_divisa)then
	uof_element_create( ref pdel_body_datigeneralidocumento_divisa,"Divisa")
	pdel_body_datigeneralidocumento_divisa.setText(is_datigen_divisa)
	pdel_body_datigeneralidocumento.addcontent(pdel_body_datigeneralidocumento_divisa)
end if
end subroutine

private subroutine uof_add_body_numero ();if not g_str.isempty(is_datigen_numero)then
	uof_element_create( ref pdel_body_datigeneralidocumento_numero,"Numero")
	pdel_body_datigeneralidocumento_numero.setText(is_datigen_numero)
	pdel_body_datigeneralidocumento.addcontent(pdel_body_datigeneralidocumento_numero)
end if
end subroutine

private subroutine uof_add_body_data ();if not g_str.isempty(is_datigen_data)then
	uof_element_create( ref pdel_body_datigeneralidocumento_data,"Data")
	pdel_body_datigeneralidocumento_data.setText(is_datigen_data)
	pdel_body_datigeneralidocumento.addcontent(pdel_body_datigeneralidocumento_data)
end if
end subroutine

private subroutine uof_add_body_totaledocumento ();if not g_str.isempty(is_datigen_totaledocumento )then
	uof_element_create( ref pdel_body_datigeneralidocumento_importo  ,"ImportoTotaleDocumento")
	pdel_body_datigeneralidocumento_importo.setText(is_datigen_totaledocumento)
	pdel_body_datigeneralidocumento.addcontent(pdel_body_datigeneralidocumento_importo)
end if
end subroutine

public function string uof_dec_to_string (decimal as_decimal);string ls_str

if isnull(as_decimal) then return "0"

ls_str = string(as_decimal,"########0.00##")

ls_str = g_str.replaceall( ls_str, ",", ".", true)

return ls_str
end function

private subroutine uof_add_body_tipodocumento ();if not g_str.isempty(is_datigen_tipo_documento)then
	uof_element_create( ref pdel_body_datigeneralidocumento_tipodocumento,"TipoDocumento")
	pdel_body_datigeneralidocumento_tipodocumento.setText(is_datigen_tipo_documento)
	pdel_body_datigeneralidocumento.addcontent(pdel_body_datigeneralidocumento_tipodocumento)
end if
end subroutine

private subroutine uof_add_body_det_numerolinea (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dettaglio_linee[al_indice].numero_linea)then
	uof_element_create( ref pdel_body_datibeniservizi_dett_numerolinea,"NumeroLinea")
	pdel_body_datibeniservizi_dett_numerolinea.setText(istr_bodybeni_dettaglio_linee[al_indice].numero_linea)
	apbdom_element.addcontent(pdel_body_datibeniservizi_dett_numerolinea)
end if
end subroutine

private subroutine uof_add_body_det_tipocessione (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dettaglio_linee[al_indice].tipo_cessione) and istr_bodybeni_dettaglio_linee[al_indice].tipo_cessione <> "NO" then
	uof_element_create( ref pdel_body_datibeniservizi_dett_tipocessione,"TipoCessionePrestazione")
	pdel_body_datibeniservizi_dett_tipocessione.setText(istr_bodybeni_dettaglio_linee[al_indice].tipo_cessione)
	apbdom_element.addcontent(pdel_body_datibeniservizi_dett_tipocessione)
end if
end subroutine

private subroutine uof_add_body_det_codicetipo (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dettaglio_linee[al_indice].codice_tipo)then
	uof_element_create( ref pdel_body_datibeniservizi_dett_codart_tipo,"CodiceTipo")
	pdel_body_datibeniservizi_dett_codart_tipo.setText(istr_bodybeni_dettaglio_linee[al_indice].codice_tipo)
	apbdom_element.addcontent(pdel_body_datibeniservizi_dett_codart_tipo)
end if
end subroutine

private subroutine uof_add_body_det_codicevalore (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dettaglio_linee[al_indice].codice_valore)then
	uof_element_create( ref pdel_body_datibeniservizi_dett_codart_codice,"CodiceValore")
	pdel_body_datibeniservizi_dett_codart_codice.setText(istr_bodybeni_dettaglio_linee[al_indice].codice_valore)
	apbdom_element.addcontent(pdel_body_datibeniservizi_dett_codart_codice)
end if
end subroutine

private subroutine uof_add_body_det_descrizione (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dettaglio_linee[al_indice].descrizione)then
	string ls_descrizione
	uof_element_create( ref pdel_body_datibeniservizi_dett_descrizione,"Descrizione")
	
	ls_descrizione = istr_bodybeni_dettaglio_linee[al_indice].descrizione
	if not g_str.isempty(istr_bodybeni_dettaglio_linee[al_indice].nota_dettaglio) then
		ls_descrizione = ls_descrizione + ". " + istr_bodybeni_dettaglio_linee[al_indice].nota_dettaglio
	end if
	
	ls_descrizione = left(ls_descrizione, 2000)
	
	PBDOM_TEXT pbd_text
	pbd_text = create PBDOM_TEXT
	pbd_text.settext(istr_bodybeni_dettaglio_linee[al_indice].descrizione)
	pdel_body_datibeniservizi_dett_descrizione.addcontent(pbd_text)
	
//	pdel_body_datibeniservizi_dett_descrizione.setname("Descrizione")	
//	pdel_body_datibeniservizi_dett_descrizione.setText(istr_bodybeni_dettaglio_linee[al_indice].descrizione)
	
	apbdom_element.addcontent(pdel_body_datibeniservizi_dett_descrizione)
end if
end subroutine

private subroutine uof_add_body_det_quantita (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dettaglio_linee[al_indice].quantita)then
	uof_element_create( ref pdel_body_datibeniservizi_dett_quantita,"Quantita")
	pdel_body_datibeniservizi_dett_quantita.setText(istr_bodybeni_dettaglio_linee[al_indice].quantita)
	apbdom_element.addcontent(pdel_body_datibeniservizi_dett_quantita)
end if
end subroutine

private subroutine uof_add_body_det_prezzo (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dettaglio_linee[al_indice].prezzo_unitario)then
	uof_element_create( ref pdel_body_datibeniservizi_dett_prezzo,"PrezzoUnitario")
	pdel_body_datibeniservizi_dett_prezzo.setText(istr_bodybeni_dettaglio_linee[al_indice].prezzo_unitario)
	apbdom_element.addcontent(pdel_body_datibeniservizi_dett_prezzo)
end if
end subroutine

private subroutine uof_add_body_det_aliquota (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dettaglio_linee[al_indice].aliquota)then
	uof_element_create( ref pdel_body_datibeniservizi_dett_aliquota,"AliquotaIVA")
	pdel_body_datibeniservizi_dett_aliquota.setText(istr_bodybeni_dettaglio_linee[al_indice].aliquota)
	apbdom_element.addcontent(pdel_body_datibeniservizi_dett_aliquota)
end if
end subroutine

private subroutine uof_add_body_det_esenzione (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dettaglio_linee[al_indice].des_esenzione)then
	uof_element_create( ref pdel_body_datibeniservizi_dett_des_esenzione,"Natura")
	pdel_body_datibeniservizi_dett_des_esenzione.setText(istr_bodybeni_dettaglio_linee[al_indice].des_esenzione)
	apbdom_element.addcontent(pdel_body_datibeniservizi_dett_des_esenzione)
end if
end subroutine

private subroutine uof_add_body_det_imponibile (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dettaglio_linee[al_indice].prezzototale)then
	uof_element_create( ref pdel_body_datibeniservizi_dett_imponibile,"PrezzoTotale")
	pdel_body_datibeniservizi_dett_imponibile.setText(istr_bodybeni_dettaglio_linee[al_indice].prezzototale)
	apbdom_element.addcontent(pdel_body_datibeniservizi_dett_imponibile)
end if
end subroutine

private subroutine uof_add_body_det_scontiperc (ref pbdom_element apbdom_element, long al_idx, string as_percentuale);if not g_str.isempty(as_percentuale)then
	uof_element_create( ref pdel_body_datibeniservizi_dett_sconti_percentuale,"Percentuale")
	pdel_body_datibeniservizi_dett_sconti_percentuale.setText(as_percentuale)
	apbdom_element.addcontent(pdel_body_datibeniservizi_dett_sconti_percentuale)
end if
end subroutine

private subroutine uof_add_body_det_scontitipo (ref pbdom_element apbdom_element, long al_idx, string as_tipo_sconto);if not g_str.isempty(as_tipo_sconto)then
	uof_element_create( ref pdel_body_datibeniservizi_dett_sconti_tipo,"Tipo")
	pdel_body_datibeniservizi_dett_sconti_tipo.setText(as_tipo_sconto)
	apbdom_element.addcontent(pdel_body_datibeniservizi_dett_sconti_tipo)
end if
end subroutine

private subroutine uof_add_body_det_um (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dettaglio_linee[al_indice].um)then
	uof_element_create( ref pdel_body_datibeniservizi_dett_um,"UnitaMisura")
	pdel_body_datibeniservizi_dett_um.setText(istr_bodybeni_dettaglio_linee[al_indice].um)
	apbdom_element.addcontent(pdel_body_datibeniservizi_dett_um)
end if
end subroutine

private subroutine uof_add_header_cedente_titolo ();if not g_str.isempty(is_cedenteprestatore_titolo)then
	uof_element_create( ref pdel_cedenteprestatore_titolo,"Titolo")
	pdel_cedenteprestatore_titolo.setText(is_cedenteprestatore_titolo)
	pdel_cedenteprestatore_anagrafica.addcontent(pdel_cedenteprestatore_titolo)
end if
end subroutine

private subroutine uof_add_header_cedente_codeori ();if not g_str.isempty(is_cedenteprestatore_codeori)then
	uof_element_create( ref pdel_cedenteprestatore_codeori,"CodEORI")
	pdel_cedenteprestatore_codeori.setText(is_cedenteprestatore_codeori)
	pdel_cedenteprestatore_anagrafica.addcontent(pdel_cedenteprestatore_codeori)
end if
end subroutine

private subroutine uof_add_body_riepilogo_aliquota (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dati_riepilogo[al_indice].riepilogo_aliquota_iva)then
	uof_element_create( ref pdel_body_datibeniservizi_riepilogo_aliquota,"AliquotaIVA")
	pdel_body_datibeniservizi_riepilogo_aliquota.setText(istr_bodybeni_dati_riepilogo[al_indice].riepilogo_aliquota_iva)
	apbdom_element.addcontent(pdel_body_datibeniservizi_riepilogo_aliquota)
end if
end subroutine

private subroutine uof_add_body_riepilogo_natura (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dati_riepilogo[al_indice].riepilogo_iva_natura)then
	uof_element_create( ref pdel_body_datibeniservizi_riepilogo_natura,"Natura")
	pdel_body_datibeniservizi_riepilogo_natura.setText(istr_bodybeni_dati_riepilogo[al_indice].riepilogo_iva_natura)
	apbdom_element.addcontent(pdel_body_datibeniservizi_riepilogo_natura)
end if
end subroutine

private subroutine uof_add_body_riepilogo_imponibile (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dati_riepilogo[al_indice].riepilogo_iva_imponibile)then
	uof_element_create( ref pdel_body_datibeniservizi_riepilogo_imponibile,"ImponibileImporto")
	pdel_body_datibeniservizi_riepilogo_imponibile.setText(istr_bodybeni_dati_riepilogo[al_indice].riepilogo_iva_imponibile)
	apbdom_element.addcontent(pdel_body_datibeniservizi_riepilogo_imponibile)
end if
end subroutine

private subroutine uof_add_body_riepilogo_imposta (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dati_riepilogo[al_indice].riepilogo_iva_imposta)then
	uof_element_create( ref pdel_body_datibeniservizi_riepilogo_imposta,"Imposta")
	pdel_body_datibeniservizi_riepilogo_imposta.setText(istr_bodybeni_dati_riepilogo[al_indice].riepilogo_iva_imposta)
	apbdom_element.addcontent(pdel_body_datibeniservizi_riepilogo_imposta)
end if
end subroutine

private subroutine uof_add_body_riepilogo_rif_norma (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dati_riepilogo[al_indice].riepilogo_iva_riferimento_norm)then
	uof_element_create( ref pdel_body_datibeniservizi_riepilogo_riferimentonormativo,"RiferimentoNormativo")
	pdel_body_datibeniservizi_riepilogo_riferimentonormativo.setText(istr_bodybeni_dati_riepilogo[al_indice].riepilogo_iva_riferimento_norm)
	apbdom_element.addcontent(pdel_body_datibeniservizi_riepilogo_riferimentonormativo)
end if
end subroutine

private subroutine uof_add_body_riepilogo_esigibilita (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_bodybeni_dati_riepilogo[al_indice].riepilogo_iva_esigibilita_iva)then
	uof_element_create( ref pdel_body_datibeniservizi_riepilogo_esegibilita,"EsigibilitaIVA")
	pdel_body_datibeniservizi_riepilogo_esegibilita.setText(istr_bodybeni_dati_riepilogo[al_indice].riepilogo_iva_esigibilita_iva)
	apbdom_element.addcontent(pdel_body_datibeniservizi_riepilogo_esegibilita)
end if
end subroutine

private subroutine uof_add_body_condizioni_pagamento ();if not g_str.isempty(is_condizioni_pagamento )then
	uof_element_create( ref pdel_body_datipag_condizionipagamento  ,"CondizioniPagamento")
	pdel_body_datipag_condizionipagamento.setText(is_condizioni_pagamento)
	pdel_body_datipagamenti.addcontent(pdel_body_datipag_condizionipagamento)
end if
end subroutine

private subroutine uof_add_body_pag_modalita_pag (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_body_pagamenti[al_indice].modalita_pagamento)then
	uof_element_create( ref pdel_body_datipag_dettaglio_modalita,"ModalitaPagamento")
	pdel_body_datipag_dettaglio_modalita.setText(istr_body_pagamenti[al_indice].modalita_pagamento)
	apbdom_element.addcontent(pdel_body_datipag_dettaglio_modalita)
end if
end subroutine

private subroutine uof_add_body_pag_data_scadenza (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_body_pagamenti[al_indice].data_scadenza)then
	uof_element_create( ref pdel_body_datipag_dettaglio_scadenza,"DataScadenzaPagamento")
	pdel_body_datipag_dettaglio_scadenza.setText(istr_body_pagamenti[al_indice].data_scadenza)
	apbdom_element.addcontent(pdel_body_datipag_dettaglio_scadenza)
end if
end subroutine

private subroutine uof_add_body_pag_importo_scadenza (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_body_pagamenti[al_indice].importo_scadenza)then
	uof_element_create( ref pdel_body_datipag_dettaglio_importo,"ImportoPagamento")
	pdel_body_datipag_dettaglio_importo.setText(istr_body_pagamenti[al_indice].importo_scadenza)
	apbdom_element.addcontent(pdel_body_datipag_dettaglio_importo)
end if
end subroutine

private subroutine uof_add_body_pag_iban (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_body_pagamenti[al_indice].iban_banca)then
	uof_element_create( ref pdel_body_datipag_dettaglio_iban,"IBAN")
	pdel_body_datipag_dettaglio_iban.setText(istr_body_pagamenti[al_indice].iban_banca)
	apbdom_element.addcontent(pdel_body_datipag_dettaglio_iban)
end if
end subroutine

private subroutine uof_add_body_pag_abi (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_body_pagamenti[al_indice].abi_cliente)then
	uof_element_create( ref pdel_body_datipag_dettaglio_abi,"ABI")
	pdel_body_datipag_dettaglio_abi.setText(istr_body_pagamenti[al_indice].abi_cliente)
	apbdom_element.addcontent(pdel_body_datipag_dettaglio_abi)
end if
end subroutine

private subroutine uof_add_body_pag_cab (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_body_pagamenti[al_indice].cab_cliente)then
	uof_element_create( ref pdel_body_datipag_dettaglio_cab,"CAB")
	pdel_body_datipag_dettaglio_cab.setText(istr_body_pagamenti[al_indice].cab_cliente)
	apbdom_element.addcontent(pdel_body_datipag_dettaglio_cab)
end if
end subroutine

private subroutine uof_add_body_pag_bic (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_body_pagamenti[al_indice].bic_banca)then
	uof_element_create( ref pdel_body_datipag_dettaglio_bic,"BIC")
	pdel_body_datipag_dettaglio_bic.setText(istr_body_pagamenti[al_indice].bic_banca)
	apbdom_element.addcontent(pdel_body_datipag_dettaglio_bic)
end if
end subroutine

private subroutine uof_add_body_pag_nome_banca (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_body_pagamenti[al_indice].des_banca)then
	uof_element_create( ref pdel_body_datipag_dettaglio_istituto_finanziario,"IstitutoFinanziario")
	pdel_body_datipag_dettaglio_istituto_finanziario.setText(istr_body_pagamenti[al_indice].des_banca)
	apbdom_element.addcontent(pdel_body_datipag_dettaglio_istituto_finanziario)
end if
end subroutine

public function integer uof_fattura_xml (long al_anno_registrazione, long al_num_registrazione, ref string as_message);// ---------- carico i dati della fattura in variabili locali ---------------------------
boolean	lb_result=false
string		ls_sql

string		ls_numeratore_documento,ls_trasmittente_idpaese, ls_trasmittente_idcodice, ls_codice_destinatario, ls_pec_destinatario, ls_trasmittente_telefono, ls_trasmittente_email, &
			ls_pec, ls_flag_pubblica_amministrazione,ls_cedente_idpaese,ls_cedente_codice,ls_cedente_codicefiscale,ls_cedente_denominazione,ls_cedente_denominazione2,ls_cedente_nome,ls_cedente_cognome,&
			ls_cedente_regimefiscale,ls_cedente_sede_indirizzo,ls_cedente_sede_numerocivico,ls_cedente_sede_cap,ls_cedente_sede_comune,ls_cedente_sede_provincia, &
			ls_cedente_sede_cod_nazione,ls_cedente_telefono,ls_cedente_fax,ls_cessionario_idpaese,ls_flag_tipo_cliente,ls_cessionario_partita_iva,ls_cessionario_cod_fiscale, &
			ls_cessionario_rag_soc_1,ls_cessionario_rag_soc_2,ls_cessionario_nome,ls_cessionario_cognome,ls_cessionario_indirizzo,ls_cessionario_num_civico,ls_cessionario_cap,ls_cessionario_localita,&
			ls_cessionario_provincia, ls_cod_documento, ls_nota_testata, ls_flag_st_note_tes, ls_nota_piede, ls_flag_st_note_pie, ls_cod_tipo_fat_ven, ls_cod_doc_collegato, ls_numeratore_doc_collegato
			//body
string		ls_fatel_tipo_documento, ls_fatel_divisa, ls_filename, ls_num_prot_esenzione_iva
			
long		ll_anno_registrazione, ll_num_registrazione, ll_anno_documento, ll_num_documento, ll_index, ll_cont, ll_anno_fat_collegata, ll_num_fat_collegata, ll_prog_riga_fat_collegata, ll_anno_doc_collegato, ll_num_doc_collegato

dec{4}	ld_tot_fattura

datetime	ldt_data_fattura, ldt_data_esenzione_iva

datastore	lds_dettaglio, lds_iva, lds_scadenze



ll_anno_registrazione = al_anno_registrazione
ll_num_registrazione = al_num_registrazione

select 
TES.cod_documento, 
TES.anno_documento, 
TES.numeratore_documento,
TES.num_documento,
TES.data_fattura,
TES.tot_fattura,
AZI.fatel_idpaese_trasmittente,
AZI.fatel_idcodice_trasmittente,
CLI.fatel_cod_destinatario,
CLI.fatel_pec,
CLI.telefono,
CLI.email_amministrazione,
CLI.fatel_flag_pa,
AZI.fatel_cod_nazione,
AZI.partita_iva,
AZI.fatel_cod_fiscale,
AZI.rag_soc_1,
isnull(AZI.rag_soc_2,''),
AZI.fatel_nome,
AZI.fatel_cognome,
AZI.fatel_regime_fiscale,
AZI.fatel_sede_indirizzo,
AZI.fatel_sede_numerocivico,
AZI.fatel_sede_cap,
AZI.fatel_sede_comune,
AZI.fatel_sede_provincia,
AZI.fatel_sede_cod_nazione,
AZI.telefono,
AZI.fax,
CLI.fatel_idpaese,
CLI.flag_tipo_cliente,
CLI.partita_iva,
CLI.cod_fiscale,
CLI.rag_soc_1,
isnull(CLI.rag_soc_2,''),
CLI.fatel_nome,
CLI.fatel_cognome,
CLI.indirizzo,
CLI.fatel_num_civico,
CLI.cap,
CLI.localita,
CLI.provincia,
TIPOFAT.fatel_tipo_documento,
VAL.fatel_divisa,
TES.nota_testata,
TES.flag_st_note_tes,
TES.nota_piede,
TES.flag_st_note_pie,
TES.cod_tipo_fat_ven,
CLI.num_prot_esenzione_iva,
CLI.data_esenzione_iva
into 
:ls_cod_documento,
:ll_anno_documento,
:ls_numeratore_documento,
:ll_num_documento,
:ldt_data_fattura,
:ld_tot_fattura,
:ls_trasmittente_idpaese,
:ls_trasmittente_idcodice,
:ls_codice_destinatario,
:ls_pec_destinatario,
:ls_trasmittente_telefono,
:ls_trasmittente_email,
:ls_flag_pubblica_amministrazione,
:ls_cedente_idpaese,
:ls_cedente_codice,
:ls_cedente_codicefiscale,
:ls_cedente_denominazione,
:ls_cedente_denominazione2,
:ls_cedente_nome,
:ls_cedente_cognome,
:ls_cedente_regimefiscale,
:ls_cedente_sede_indirizzo,
:ls_cedente_sede_numerocivico,
:ls_cedente_sede_cap,
:ls_cedente_sede_comune,
:ls_cedente_sede_provincia,
:ls_cedente_sede_cod_nazione,
:ls_cedente_telefono,
:ls_cedente_fax,
:ls_cessionario_idpaese,
:ls_flag_tipo_cliente,
:ls_cessionario_partita_iva,
:ls_cessionario_cod_fiscale,
:ls_cessionario_rag_soc_1,
:ls_cessionario_rag_soc_2,
:ls_cessionario_nome,
:ls_cessionario_cognome,
:ls_cessionario_indirizzo,
:ls_cessionario_num_civico,
:ls_cessionario_cap,
:ls_cessionario_localita,
:ls_cessionario_provincia,
:ls_fatel_tipo_documento,
:ls_fatel_divisa,
:ls_nota_testata,
:ls_flag_st_note_tes,
:ls_nota_piede,
:ls_flag_st_note_pie,
:ls_cod_tipo_fat_ven,
:ls_num_prot_esenzione_iva,
:ldt_data_esenzione_iva
from tes_fat_ven TES
join aziende AZI on TES.cod_azienda = AZI.cod_azienda
join anag_clienti CLI on TES.cod_azienda=CLI.cod_azienda and TES.cod_cliente=CLI.cod_cliente
join tab_tipi_fat_ven TIPOFAT on TES.cod_azienda=TIPOFAT.cod_azienda and TES.cod_tipo_fat_ven=TIPOFAT.cod_tipo_fat_ven 
join tab_valute VAL on TES.cod_azienda=VAL.cod_azienda and TES.cod_valuta=VAL.cod_valuta
where TES.cod_azienda = :s_cs_xx.cod_azienda and
TES.anno_registrazione = :ll_anno_registrazione and
TES.num_registrazione = :ll_num_registrazione;
if sqlca.sqlcode = 100 then
	as_message = "La fattura richiesta non esiste"
	return -1
end if
if sqlca.sqlcode = -1 then
	as_message = g_str.format("Errore in lettura dati testata fattura $1/$2. $3",ll_anno_registrazione,ll_num_registrazione,sqlca.sqlerrtext)
	return -1
end if

// -----------  generazione fattura elettronica -------------------
string	ls_str, ls_errore
long ll_i, ll_row, ll_ret
PBDOM_BUILDER pb_builder
PBDOM_object pb_object
PBDOM_DOCUMENT pbdom_doc
PBDOM_ELEMENT pbdom_elem, pbdom_new_element, pbdoc_element_results, pb_elements[]
//PBDOM_PROCESSINGINSTRUCTION pbdom_instructions

pb_builder = create PBDOM_BUILDER
if ls_flag_pubblica_amministrazione="S" then
	ls_filename = s_cs_xx.volume + s_cs_xx.risorse + "fatel\basePA.xml"
else
	ls_filename = s_cs_xx.volume + s_cs_xx.risorse + "fatel\basePR.xml"
end if

pbdom_doc = pb_builder.buildfromfile( ls_filename)


// carico le variabili prese dalla fattura
is_idtrasmiittente_idpaese=ls_trasmittente_idpaese
is_idtrasmiittente_idcodice=ls_trasmittente_idcodice
is_datitrasmissione_progressivoinvio= g_str.format("$1$2$3",ll_anno_documento,ls_numeratore_documento,string(ll_num_documento,"00000"))
if ls_flag_pubblica_amministrazione="S" then
	is_datitrasmissione_formatotrasmissione="FPA12"
else
	is_datitrasmissione_formatotrasmissione="FPR12"
end if

//se il codice destinatario non è stato impostato vuol dire che il cliente non si è accreditato sul SDI e quindi la fattura va mandata via PEC
is_datitrasmissione_codicedestinatario=ls_codice_destinatario
is_datitrasmissione_pecdestinatario=left(ls_pec_destinatario,256)

if ls_flag_pubblica_amministrazione = "S" then
	if len(is_datitrasmissione_codicedestinatario) <> 6 then
		as_message = "Il destinatario è una Pubblica Amministrazione: il codice destinatario deve essere di 6 caratteri."
		return -1
	end if
	
	select count(*)
	into	:ll_cont
	from 	det_fat_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :al_anno_registrazione and
			num_registrazione = :al_num_registrazione and
			(len(fatel_iddocumento) > 0 or len(fatel_cig) > 0);
	choose case ll_cont
		case 0
			is_tipo_rif_datiordineacquisto = "0"
		case 1
			is_tipo_rif_datiordineacquisto = "1"
		case else
			is_tipo_rif_datiordineacquisto = "2"
	end choose
			
else
	if is_datitrasmissione_codicedestinatario = "0000000" or len(is_datitrasmissione_codicedestinatario) = 0 then /// uso la PEC
		is_datitrasmissione_codicedestinatario = "0000000"
		if len(is_datitrasmissione_pecdestinatario) < 7 then
			as_message = "Attenzione: il codice SDI del destinatario non è stato specificato e la PEC ha meno di 7 caratteri: verificare!"
			return -1
		end if
	end if
end if

is_contattitrasmittente_telefono=left(ls_trasmittente_telefono,12)
is_contattitrasmittente_email=left(ls_trasmittente_email,256)
is_cedenteprestatore_id_paese=ls_cedente_idpaese
is_cedenteprestatore_idcodice=ls_cedente_codice
is_cedenteprestatore_codicefiscale=ls_cedente_codicefiscale
is_cedenteprestatore_denominazione= ls_cedente_denominazione
if len(ls_cedente_denominazione2) > 0 then is_cedenteprestatore_denominazione += " " + ls_cedente_denominazione2
is_cedenteprestatore_nome=ls_cedente_nome
is_cedenteprestatore_cognome=ls_cedente_cognome
is_cedenteprestatore_regimefiscale=ls_cedente_regimefiscale
is_cedenteprestatoresede_indirizzo=ls_cedente_sede_indirizzo
is_cedenteprestatoresede_numerocivico=ls_cedente_sede_numerocivico
is_cedenteprestatoresede_cap=ls_cedente_sede_cap
is_cedenteprestatoresede_comune=ls_cedente_sede_comune
is_cedenteprestatoresede_provincia=ls_cedente_sede_provincia
is_cedenteprestatoresede_nazione=ls_cedente_sede_cod_nazione
is_cedenteprestatore_telefono=ls_cedente_telefono
is_cedenteprestatore_fax=ls_cedente_fax

is_cessionario_idpaese = ls_cessionario_idpaese
choose case ls_flag_tipo_cliente
	case "S", "F"
		is_cessionario_idcodice = ls_cessionario_partita_iva
		is_cessionario_codicefiscale = ls_cessionario_cod_fiscale
	case "C", "E"
		is_cessionario_idcodice = ls_cessionario_cod_fiscale
	case else
		// errore: si tratta di un privato o altro
		if sqlca.sqlcode = 100 then
			as_message = (g_str.format("Il cessionario $1 non è Società, Persona Fisica, Cee, Estero. Verificare il Tipo Cliente in anagrafica", ls_cessionario_rag_soc_1))
			return -1
		end if
end choose

is_cessionario_denominazione=ls_cessionario_rag_soc_1
if len(ls_cessionario_rag_soc_2) > 0 then is_cessionario_denominazione += " " + ls_cessionario_rag_soc_2
is_cessionario_nome= ls_cessionario_nome
is_cessionario_cognome= ls_cessionario_cognome
is_cessionariosede_indirizzo = ls_cessionario_indirizzo
is_cessionariosede_numerocivico= ls_cessionario_num_civico
is_cessionariosede_cap= ls_cessionario_cap
is_cessionariosede_comune= ls_cessionario_localita
is_cessionariosede_provincia= ls_cessionario_provincia
is_cessionariosede_nazione= ls_cessionario_idpaese

// caricamento variabili per Body
is_datigen_tipo_documento = ls_fatel_tipo_documento
is_datigen_divisa = ls_fatel_divisa
is_datigen_data = string(ldt_data_fattura, "YYYY-MM-DD")
is_datigen_numero = g_str.format( "$1-$2-$3-$4", ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento  )
if not isnull(ls_num_prot_esenzione_iva) and len(ls_num_prot_esenzione_iva) > 0 then
	is_datigen_causale = "DICHIARAZIONE D'INTENTO NUM " + ls_num_prot_esenzione_iva
	if not isnull(ldt_data_esenzione_iva) and ldt_data_esenzione_iva > datetime(date("01/01/1900"), 00:00:00) then
		is_datigen_causale = is_datigen_causale + "  DEL " + string(ldt_data_esenzione_iva,"dd/mm/yyyy")
	end if
end if	


is_datigen_totaledocumento = uof_dec_to_string( ld_tot_fattura)

if ls_flag_st_note_tes = 'I' then   //nota di testata
	select flag_st_note
	  into :ls_flag_st_note_tes
	  from tab_tipi_fat_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
end if		

if ls_flag_st_note_tes = 'N' then
	is_datigen_nota_testata = ""
else
	is_datigen_nota_testata = ls_nota_testata
end if			


if ls_flag_st_note_pie = 'I' then //nota di piede
	select flag_st_note
	  into :ls_flag_st_note_pie
	  from tab_tipi_fat_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
end if			

if ls_flag_st_note_pie = 'N' then
	is_datigen_nota_testata = ""
else
	is_datigen_nota_piede = ls_nota_piede
end if				



// ---------- caricamento dati del dettaglio fattura ----------------------------

ls_sql = g_str.format(  &
" select DET.prog_riga_fat_ven, DET.cod_prodotto, isnull(DET.des_prodotto, ANAG.des_prodotto), isnull(quan_fatturata,0), TIPODET.fatel_tipo_cessione, isnull(iva.aliquota,0), isnull(iva.des_iva,''), DET.cod_misura, isnull(DET.prezzo_vendita,0), isnull(imponibile_iva,0), isnull(sconto_1,0), isnull(sconto_2,0), isnull(sconto_3,0), IVA.fatel_natura_esenzione, TIPODET.des_tipo_det_ven, DET.nota_dettaglio, DET.flag_st_note_det, TIPODET.flag_st_note_ft, TIPODET.flag_tipo_det_ven, TIPODET.cod_tipo_det_ven, DET.fatel_cig, DET.fatel_iddocumento,DET.anno_reg_fat_ven_nc, DET.num_reg_fat_ven_nc, DET.prog_riga_fat_ven_nc   " +&
" from det_fat_ven DET " +&
" left join anag_prodotti ANAG on DET.cod_azienda = ANAG.cod_azienda and DET.cod_prodotto=ANAG.cod_prodotto  " +&
" left join tab_tipi_det_ven TIPODET on DET.cod_azienda = TIPODET.cod_azienda and DET.cod_tipo_det_ven=TIPODET.cod_tipo_det_ven  " +&
" left join tab_ive IVA on DET.cod_azienda = IVA.cod_azienda and DET.cod_iva=IVA.cod_iva " +&
" WHERE DET.cod_azienda = '$1' and anno_registrazione=$2 and num_registrazione=$3 ORDER BY 1", s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione ) 

ll_ret = guo_functions.uof_crea_datastore( lds_dettaglio, ls_sql, ls_errore)
if ll_ret < 0 then
	as_message =g_str.format("Errore in fase di creazione datastore del dettaglio della fattura.$1",ls_errore)
	return -1
end if

for ll_row = 1 to ll_ret
	
	// gestione del bollo virtuale. Noi lo mettiamo nelle righe mentre la fattura elettronica lo vuole in testata.
	if lds_dettaglio.getitemstring(ll_row,19 )="B" and lds_dettaglio.getitemstring(ll_row,20 )="SPB" then
		is_datigen_bollovirtuale = "SI"
		is_datigen_importo_bollo = uof_dec_to_string( lds_dettaglio.getitemnumber(ll_row,10) )
		continue
	end if
	
	istr_bodybeni_dettaglio_linee[ll_row].numero_linea= string( lds_dettaglio.getitemnumber(ll_row,1 ))
	
	if not isnull(lds_dettaglio.getitemstring(ll_row,2)) then
		istr_bodybeni_dettaglio_linee[ll_row].codice_tipo="NSCOD"
		istr_bodybeni_dettaglio_linee[ll_row].codice_valore=lds_dettaglio.getitemstring(ll_row,2)
	else
		setnull(istr_bodybeni_dettaglio_linee[ll_row].codice_tipo)
		setnull(istr_bodybeni_dettaglio_linee[ll_row].codice_valore)
	end if
	
	// la descrizione ci deve sempre essere, quindi se manca gli metto la descrizione del tipo dettaglio (come faccio per le fatture attive)
	if isnull(lds_dettaglio.getitemstring(ll_row,3)) or len(lds_dettaglio.getitemstring(ll_row,3)) = 0 then
		istr_bodybeni_dettaglio_linee[ll_row].descrizione=lds_dettaglio.getitemstring(ll_row,15)
	else
		istr_bodybeni_dettaglio_linee[ll_row].descrizione=lds_dettaglio.getitemstring(ll_row,3)
	end if
	// aggiungo eventualmente la nota di dettaglio
	if not isnull(lds_dettaglio.getitemstring(ll_row,16)) and len(lds_dettaglio.getitemstring(ll_row,16)) > 0 then
		istr_bodybeni_dettaglio_linee[ll_row].descrizione += "  " + lds_dettaglio.getitemstring(ll_row,16)
	end if

	istr_bodybeni_dettaglio_linee[ll_row].quantita= uof_dec_to_string( lds_dettaglio.getitemnumber(ll_row,4) )
	istr_bodybeni_dettaglio_linee[ll_row].tipo_cessione=lds_dettaglio.getitemstring(ll_row,5)
	istr_bodybeni_dettaglio_linee[ll_row].prezzototale=uof_dec_to_string( lds_dettaglio.getitemnumber(ll_row,10) )
	istr_bodybeni_dettaglio_linee[ll_row].um=lds_dettaglio.getitemstring(ll_row,8)
	
	// In caso di riga sconto incodizionato anche il totale riga deve essere negativo
	if istr_bodybeni_dettaglio_linee[ll_row].tipo_cessione="SC" then
		istr_bodybeni_dettaglio_linee[ll_row].prezzo_unitario=uof_dec_to_string( lds_dettaglio.getitemnumber(ll_row,9) * -1)
	else
		istr_bodybeni_dettaglio_linee[ll_row].prezzo_unitario=uof_dec_to_string( lds_dettaglio.getitemnumber(ll_row,9) )
	end if
	istr_bodybeni_dettaglio_linee[ll_row].aliquota = uof_dec_to_string(lds_dettaglio.getitemnumber(ll_row,6))
	
	// sconto_1
	if lds_dettaglio.getitemnumber(ll_row,11) > 0 then
		istr_bodybeni_dettaglio_linee[ll_row].sconti[1].sconto_tipo="SC"
		istr_bodybeni_dettaglio_linee[ll_row].sconti[1].sconto_perc=  uof_dec_to_string( lds_dettaglio.getitemnumber(ll_row,11))
		// sconto_2
		if lds_dettaglio.getitemnumber(ll_row,12) > 0 then
			istr_bodybeni_dettaglio_linee[ll_row].sconti[2].sconto_tipo="SC"
			istr_bodybeni_dettaglio_linee[ll_row].sconti[2].sconto_perc=  uof_dec_to_string( lds_dettaglio.getitemnumber(ll_row,12))
			// sconto_3
			if lds_dettaglio.getitemnumber(ll_row,13) > 0 then
				istr_bodybeni_dettaglio_linee[ll_row].sconti[3].sconto_tipo="SC"
				istr_bodybeni_dettaglio_linee[ll_row].sconti[3].sconto_perc=  uof_dec_to_string( lds_dettaglio.getitemnumber(ll_row,13))
			end if
		end if
	end if
	istr_bodybeni_dettaglio_linee[ll_row].des_esenzione=lds_dettaglio.getitemstring(ll_row,14)
	
	// nota dettaglio
	choose case lds_dettaglio.getitemstring(ll_row,17)
		case "N"
			istr_bodybeni_dettaglio_linee[ll_row].nota_dettaglio = ""
		case "S"
			if isnull(lds_dettaglio.getitemstring(ll_row,16)) then
				istr_bodybeni_dettaglio_linee[ll_row].nota_dettaglio = ""
			else
				istr_bodybeni_dettaglio_linee[ll_row].nota_dettaglio = lds_dettaglio.getitemstring(ll_row,16)
			end if
		case "I"
			if lds_dettaglio.getitemstring(ll_row,18) = "S" then
				if isnull(lds_dettaglio.getitemstring(ll_row,16)) then
					istr_bodybeni_dettaglio_linee[ll_row].nota_dettaglio = ""
				else
					istr_bodybeni_dettaglio_linee[ll_row].nota_dettaglio = lds_dettaglio.getitemstring(ll_row,16)
				end if
			else
				istr_bodybeni_dettaglio_linee[ll_row].nota_dettaglio = ""
			end if
	end choose
	
	// eventuale dato CIG o codice documento
	// EnMe 05/01/2020 inserito riferimento a riga fattura in caso di nota di credito. Attenzione!!! La nota di credito può essere solo un TD04. 
	// il tipo documento nota di credito TD08 è per il flusso semplificato che noi al momento non gestiamo.
	choose case ls_fatel_tipo_documento	
		case "TD04"
			if (not isnull(lds_dettaglio.getitemnumber(ll_row,23)) and lds_dettaglio.getitemnumber(ll_row,23) > 0) then
				
				ll_anno_fat_collegata = lds_dettaglio.getitemnumber(ll_row,23)
				ll_num_fat_collegata = lds_dettaglio.getitemnumber(ll_row,24)
				ll_prog_riga_fat_collegata = lds_dettaglio.getitemnumber(ll_row,25)
				
				select 	cod_documento, 
							numeratore_documento, 
							anno_documento, 
							num_documento
				into		:ls_cod_doc_collegato, 
							:ls_numeratore_doc_collegato, 
							:ll_anno_doc_collegato, 
							:ll_num_doc_collegato 
				from  		tes_fat_ven
				where 	cod_azienda = :s_cs_xx.cod_azienda and
							anno_registrazione = :ll_anno_fat_collegata and
							num_registrazione = :ll_num_fat_collegata;
				
				
				ll_index = upperbound(istr_body_dati_ordine_acquisto[])
				if isnull(ll_index) or ll_index = 0 then
					ll_index = 1
				else
					ll_index ++
				end if
				istr_body_dati_ordine_acquisto[ll_index].riferimentonumerolinea = istr_bodybeni_dettaglio_linee[ll_row].numero_linea
				istr_body_dati_ordine_acquisto[ll_index].numitem = string(ll_prog_riga_fat_collegata)
				istr_body_dati_ordine_acquisto[ll_index].iddocumento =  g_str.format( "$1-$2-$3-$4", ls_cod_doc_collegato, ls_numeratore_doc_collegato, ll_anno_doc_collegato, ll_num_doc_collegato)
				
				if not isnull(lds_dettaglio.getitemstring(ll_row,21)) and len(lds_dettaglio.getitemstring(ll_row,21)) > 0 then
					istr_body_dati_ordine_acquisto[ll_index].codicecig = left(lds_dettaglio.getitemstring(ll_row,21),15)
				end if
			end if
		case "TD08"
			as_message = "Formato Nota di credito TD08 non gestito perchè fa parte del flusso semplificato"
			return -1
			
		case else
			if (not isnull(lds_dettaglio.getitemstring(ll_row,21)) and len(lds_dettaglio.getitemstring(ll_row,21)) > 0) or (not isnull(lds_dettaglio.getitemstring(ll_row,22)) and len(lds_dettaglio.getitemstring(ll_row,22)) > 0) then
				ll_index = upperbound(istr_body_dati_ordine_acquisto[])
				if isnull(ll_index) or ll_index = 0 then
					ll_index = 1
				else
					ll_index ++
				end if
				istr_body_dati_ordine_acquisto[ll_index].riferimentonumerolinea = istr_bodybeni_dettaglio_linee[ll_row].numero_linea
				istr_body_dati_ordine_acquisto[ll_index].iddocumento = left(lds_dettaglio.getitemstring(ll_row,22),20)
				istr_body_dati_ordine_acquisto[ll_index].codicecig = left(lds_dettaglio.getitemstring(ll_row,21),15)
			end if
	end choose	
next


// ---------- caricamento dati riepilogo iva della fattura ----------------------------
ls_sql = g_str.format(  " select TES.cod_iva, isnull(TES.imponibile_iva,0) + isnull(TES.imponibile_solo_iva,0), isnull(TES.importo_iva,0), IVA.fatel_natura_esenzione, isnull(IVA.aliquota,0), fatel_esigibilita_iva, isnull(IVA.des_estesa, IVA.des_iva) from iva_fat_ven TES left join tab_ive IVA on TES.cod_azienda = IVA.cod_azienda and TES.cod_iva=IVA.cod_iva " +&
" WHERE TES.cod_azienda = '$1' and anno_registrazione=$2 and num_registrazione=$3 ORDER BY 1", s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione ) 

ll_ret = guo_functions.uof_crea_datastore( lds_iva, ls_sql, ls_errore)
if ll_ret < 0 then
	as_message=g_str.format("Errore in fase di creazione datastore del dettaglio della fattura.$1",ls_errore)
	return -1
end if

for ll_row = 1 to ll_ret
	istr_bodybeni_dati_riepilogo[ll_row].riepilogo_aliquota_iva = uof_dec_to_string( lds_iva.getitemnumber(ll_row, 5))
	istr_bodybeni_dati_riepilogo[ll_row].riepilogo_iva_natura = lds_iva.getitemstring(ll_row, 4)
	istr_bodybeni_dati_riepilogo[ll_row].riepilogo_iva_riferimento_norm =lds_iva.getitemstring(ll_row, 7)
	istr_bodybeni_dati_riepilogo[ll_row].riepilogo_iva_imponibile = uof_dec_to_string(lds_iva.getitemnumber(ll_row, 2))
	istr_bodybeni_dati_riepilogo[ll_row].riepilogo_iva_imposta = uof_dec_to_string(lds_iva.getitemnumber(ll_row, 3))
	istr_bodybeni_dati_riepilogo[ll_row].riepilogo_iva_esigibilita_iva = lds_iva.getitemstring(ll_row, 6)
next
// ----------------------------------------------------------------------------------

// ---------- caricamento dati pagamenti ----------------------------

ls_sql = g_str.format(  "select SCAD.prog_scadenza, SCAD.data_scadenza, SCAD.imp_rata, SCAD.cod_tipo_pagamento, SCAD.fatel_modalita_pagamento,TES.cod_banca, TES.cod_pagamento, PAG.fatel_condizioni_pagamento, PAG.flag_tipo_pagamento, BAN.iban, ABI1.des_abi, BAP.cod_abi, BAP.cod_cab, ABI2.des_abi, CAB.agenzia " + &
" from tes_fat_ven TES " + &
" left join scad_fat_ven SCAD on TES.cod_azienda = SCAD.cod_azienda and TES.anno_registrazione=SCAD.anno_registrazione and TES.num_registrazione=SCAD.num_registrazione " + &
" left join tab_pagamenti PAG on TES.cod_azienda = PAG.cod_azienda and TES.cod_pagamento=PAG.cod_pagamento " + &
" left join anag_banche BAN on TES.cod_azienda = BAN.cod_azienda and TES.cod_banca=BAN.cod_banca " + &
" left join anag_banche_clien_for BAP on TES.cod_azienda = BAP.cod_azienda and TES.cod_banca_clien_for=BAP.cod_banca_clien_for " + &
" left join tab_abi ABI1 on BAN.cod_abi=ABI1.cod_abi " + &
" left join tab_abi ABI2 on BAP.cod_abi=ABI2.cod_abi " + &
" left join tab_abicab CAB on BAP.cod_abi=CAB.cod_abi and BAP.cod_cab=CAB.cod_cab " + &
" WHERE TES.cod_azienda = '$1' and TES.anno_registrazione=$2 and TES.num_registrazione=$3 " + &
" ORDER BY 1 ", s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione ) 



ll_ret = guo_functions.uof_crea_datastore( lds_scadenze, ls_sql, ls_errore)
if ll_ret < 0 then
	as_message = g_str.format("Errore in fase di creazione datastore del dettaglio della fattura.$1",ls_errore)
	return -1
end if
for ll_row = 1 to ll_ret
	if ll_row = 1 then
		is_condizioni_pagamento = lds_scadenze.getitemstring(ll_row, 8)
	end if
	istr_body_pagamenti[ll_row].modalita_pagamento = lds_scadenze.getitemstring(ll_row, 5)
	istr_body_pagamenti[ll_row].data_scadenza = string(lds_scadenze.getitemdatetime(ll_row, 2), "YYYY-MM-DD")
	istr_body_pagamenti[ll_row].importo_scadenza = uof_dec_to_string( lds_scadenze.getitemnumber(ll_row, 3) )
	if lds_scadenze.getitemstring(ll_row, 4) = "R" then
		// RIBA quindi carico dati banca cliente
		istr_body_pagamenti[ll_row].des_banca = lds_scadenze.getitemstring(ll_row, 14) + lds_scadenze.getitemstring(ll_row, 15)
		istr_body_pagamenti[ll_row].abi_cliente =lds_scadenze.getitemstring(ll_row, 12)
		istr_body_pagamenti[ll_row].cab_cliente = lds_scadenze.getitemstring(ll_row, 13)
		setnull(istr_body_pagamenti[ll_row].iban_banca)
		setnull(istr_body_pagamenti[ll_row].bic_banca)
	else
		istr_body_pagamenti[ll_row].des_banca  = lds_scadenze.getitemstring(ll_row, 11)
		istr_body_pagamenti[ll_row].iban_banca = lds_scadenze.getitemstring(ll_row, 10)
		setnull(istr_body_pagamenti[ll_row].bic_banca)
		setnull(istr_body_pagamenti[ll_row].abi_cliente)
		setnull(istr_body_pagamenti[ll_row].cab_cliente)
	end if
next
// ----------------------------------------------------------------------------------



// avvio il procedimento di creazione XML
pbdom_doc.getrootelement( ).getchildelements(pb_elements[])
for ll_i = 1 to upperbound(pb_elements[])
	
	ls_str = pb_elements[ll_i].getname()
	
	choose case ll_i
		case 1 // header
			pbdom_header=pb_elements[1]
			if uof_header_validate( ref ls_errore) = -1 then
				as_message = ls_errore
				return -1
			end if
			uof_header_create(  )
			pb_elements[1] = pbdom_header
		case 2 // body
			pbdom_body=pb_elements[2]
			uof_body_create(  )
			pb_elements[1] = pbdom_body
			
	end choose
			

	
//	destroy pbdom_new_element
next

pbdom_doc.getrootelement( ).removecontent(pb_elements[1])
pbdom_doc.getrootelement( ).addcontent( pb_elements[1])
pbdom_doc.getrootelement( ).removecontent(pb_elements[2])
pbdom_doc.getrootelement( ).addcontent( pb_elements[2])

ls_filename = guo_functions.uof_get_user_temp_folder( ) + guo_functions.uof_get_random_filename( ) + ".xml"

if not pbdom_doc.SaveDocument (ls_filename ) then
	as_message = "Errore in salvataggio file XML"
else
	as_message = ls_filename
end if

return 0
end function

public function integer uof_fattura_passiva_download_xml (long al_progessivo_id, ref string as_message);/*
EnMe 24-12-2018 Download notifica in XML
Legge tutte le notifiche (attive e passive) e poi decide cosa  fare in base a quello che viene richiesto

RETURN		0= OK ci sono notifiche
				1= OK, ma niente notiche
				-1 = ERRORE
*/

string			ls_base64_auth, ls_partita_iva, ls_URL, ls_data, ls_risposta,ls_username, ls_nome_cognome
n_winhttp 	ln_http
ULong 		lul_length
any 			ls_response_json
uo_crypto 	luo_crypto
sailjson 		luo_parse, luo_sendjson

luo_crypto = create uo_crypto

select partita_iva
into	:ls_partita_iva
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;

ls_base64_auth = luo_crypto.uof_encode_64( ls_partita_iva + ":CS.$e95" )


ls_URL  = "http://fattura.csteam.com/api/download/fattura/passiva/" + string(al_progessivo_id)
ln_http.Open("POST", ls_URL)

ln_http.SetRequestHeader("Content-Type", "application/json;charset=utf-8")
ln_http.SetRequestHeader("Authorization", "Basic " + ls_base64_auth)

// aggiuungere nome e cognome utente connesso o altre info per log (100 car max)
if s_cs_xx.cod_utente = "CS_SYSTEM" then
	ls_data = g_str.format("COD_UTENTE=$1 USERNAME=$2  NOME=$3 DATA-ORA-CLIENT=$4 $5","CS_SYSTEM", "UTENTE DI SISTEMA" ,"", string(today(),"dd/mm/yyyy"), string(now(),"hh:mm:ss"))
else
	select username, 
			nome_cognome
	into	:ls_username,
			:ls_nome_cognome
	from utenti
	where cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode < 0 then
		as_message = g_str.format("Errore SQL in lettura utente $1", s_cs_xx.cod_utente, sqlca.sqlerrtext)
		return -1
	end if

	ls_data = g_str.format("COD_UTENTE=$1 USERNAME=$2  NOME=$3 DATA-ORA-CLIENT=$4 $5",s_cs_xx.cod_utente,ls_username,ls_nome_cognome, string(today(),"dd/mm/yyyy"), string(now(),"hh:mm:ss"))
end if

lul_length = ln_http.Send(ls_data)

If lul_length > 0 Then
	
	as_message = ln_http.ResponseText
	
	if left(as_message,1) = "{" then					// JSON quindi c'è un errore
		luo_parse=create sailjson
		luo_sendjson=create sailjson
		luo_parse.parse( ln_http.ResponseText)
		
		as_message = g_str.format( "Errore in download fattura.~r~nCodice errore $1, $2.~r~n$3",string(luo_parse.getattribute( "error")) , string(luo_parse.getattribute( "message")), string(luo_parse.getattribute( "status")))
		return -1
	end if
	
	if left(as_message,1) = "<" then 
		return 0		// è un XML posso ritornare tranquillamente
	end if
	
	// il servizio ha tornato una stringa con un messaggio che visualizzo come errore
	return -1
	
else
	// Errore impossibile raggiungere il server: forse non c'è internet o il servizio non risponde
	as_message =  ln_http.lasterrortext
	return -1
end if

return 0
end function

private function integer uof_notifica_format_child (pbdom_element apbdom_elem, ref string as_testo_notifica, ref string as_identificativo_sdi);CONSTANT STRING CSTR_NAMES="IdentificativoSdI,NomeFile,DataOraRicezione,ListaErrori,Codice,Descrizione"
boolean 	lb_success, lb_attributes
string		ls_name, ls_text
long 		ll_i, ll_cont, ll_handle
PBDOM_Element		pbdom_element_childs[]
PBDOM_Attribute		pbdom_attributes[]

try
	lb_success = apbdom_elem.getchildelements(pbdom_element_childs)
	ls_name = apbdom_elem.getname()
	for ll_i = 1 to upperbound(pbdom_element_childs)
		ls_name = pbdom_element_childs[ll_i].getname( )
		lb_attributes = pbdom_element_childs[ll_i].hasattributes( )
		lb_success = apbdom_elem.getchildelements(pbdom_element_childs)
		if upperbound(pbdom_element_childs) > 0 then 	
			if pos(CSTR_NAMES, pbdom_element_childs[ll_i].getname() ) > 0 then
				if pbdom_element_childs[ll_i].haschildelements() then
					as_testo_notifica = as_testo_notifica + "~r~n" + pbdom_element_childs[ll_i].getname()
				else
					if pbdom_element_childs[ll_i].getname() = "IdentificativoSdI" then as_identificativo_sdi=pbdom_element_childs[ll_i].gettext()
					as_testo_notifica = as_testo_notifica + "~r~n" + pbdom_element_childs[ll_i].getname() + " = " + pbdom_element_childs[ll_i].gettext()
				end if
			end if		
			uof_notifica_format_child( pbdom_element_childs[ll_i],ref as_testo_notifica, ref as_identificativo_sdi )
		end if
	
	next
CATCH ( PBDOM_Exception pbde )   
	as_testo_notifica = g_str.format( "PBDOM Exception: $1", pbde.getMessage() )
	return -1
CATCH ( PBXRuntimeError re )   
	as_testo_notifica = g_str.format( "PBNI Exception: $1", re.getMessage() )
	return -1
END TRY

return 0
end function

public function integer uof_notifica_fatture_attive_download_xml (long al_progessivo_id, ref string as_testo_notifica, ref string as_message);/*
EnMe 24-12-2018 Download notifica in XML
Legge tutte le notifiche (attive e passive) e poi decide cosa  fare in base a quello che viene richiesto

RETURN		0= OK ci sono notifiche
				-1 = ERRORE
*/

string			ls_base64_auth, ls_partita_iva, ls_URL, ls_data, ls_risposta,ls_username, ls_nome_cognome, ls_identificativo_sdi, &
				ls_cod_nota, ls_nota, ls_note, ls_db, ls_errore, ls_flag_esito_fatel, ls_esito
long			ll_anno_registrazione, ll_num_registrazione, ll_cont_sdi,ll_ret, ll_prog_mimetype
transaction	sqlcb
blob			l_blob
n_winhttp 	ln_http
ULong 		lul_length
uo_crypto 	luo_crypto
sailjson 		luo_parse, luo_sendjson


luo_crypto = create uo_crypto

select partita_iva
into	:ls_partita_iva
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;

ls_base64_auth = luo_crypto.uof_encode_64( ls_partita_iva + ":CS.$e95" )


ls_URL  = "http://fattura.csteam.com/api/download/notifica/" + string(al_progessivo_id)
ln_http.Open("POST", ls_URL)

ln_http.SetRequestHeader("Content-Type", "application/json;charset=utf-8")
ln_http.SetRequestHeader("Authorization", "Basic " + ls_base64_auth)

// aggiungere nome e cognome utente connesso o altre info per log (100 car max)
if s_cs_xx.cod_utente = "CS_SYSTEM" then
	ls_data = g_str.format("COD_UTENTE=$1 USERNAME=$2  NOME=$3 DATA-ORA-CLIENT=$4 $5","CS_SYSTEM", "UTENTE DI SISTEMA" ,"", string(today(),"dd/mm/yyyy"), string(now(),"hh:mm:ss"))
else
	select username, 
			nome_cognome
	into	:ls_username,
			:ls_nome_cognome
	from utenti
	where cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode < 0 then
		as_message = g_str.format("Errore SQL in lettura utente $1", s_cs_xx.cod_utente, sqlca.sqlerrtext)
		return -1
	end if

	ls_data = g_str.format("COD_UTENTE=$1 USERNAME=$2  NOME=$3 DATA-ORA-CLIENT=$4 $5",s_cs_xx.cod_utente,ls_username,ls_nome_cognome, string(today(),"dd/mm/yyyy"), string(now(),"hh:mm:ss"))
end if

lul_length = ln_http.Send(ls_data)

If lul_length > 0 Then
	
	luo_parse=create sailjson
	luo_sendjson=create sailjson
	
	luo_parse.parse( ln_http.ResponseText)
	
	ls_risposta = ln_http.ResponseText
	
else
	// Errore impossibile raggiungere il server: forse non c'è internet o il servizio non risponde
	as_message =  ln_http.lasterrortext
	return -1
end if
as_testo_notifica = ""
ls_esito = uof_notifica_format( ls_risposta, ref as_testo_notifica, ref ls_identificativo_sdi )
if ls_esito= "ER" then
	as_message = g_str.format("Errore nel format XML~r~n", as_testo_notifica)
	return -1
end if
// ---------------------- con l'dentificativo SDI trovo la fattura ---- //
select count(*) 
into	:ll_cont_sdi
from  tes_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and
		identificativo_sdi = :ls_identificativo_sdi;
choose case ll_cont_sdi
	case 0 // nessun ID SDI trovato
		as_message = g_str.format("Attenzione: per la notifica $1 non riesco a trovare la relativa fattura in Apice.",ls_identificativo_sdi)
		return -2
	case is > 1
		as_message = g_str.format("Attenzione: per la notifica $1 esistono più fatture.",ls_identificativo_sdi)
		return -2
	case 1
		select anno_registrazione, num_registrazione
		into	:ll_anno_registrazione, :ll_num_registrazione
		from  tes_fat_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				identificativo_sdi = :ls_identificativo_sdi;
		if sqlca.sqlcode < 0 then
			as_message = g_str.format("Errore SQL in fase di ricerca fattura relativa all'identificativo SDI $1.",ls_identificativo_sdi)
			return -2
		end if
end choose

// ---------------------- Aggiorno il flag esito SDI ------------------//

update tes_fat_ven
set flag_esito_fatel= :ls_esito
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_registrazione and
		 num_registrazione = :ll_num_registrazione;
if sqlca.sqlcode < 0 then
	as_message = g_str.format("Errore SQL in aggiornamento ESIITO invio SDI. $1.", sqlca.sqlerrtext)
	rollback;
	return -2
end if	
// ----------------------  archivio i risultati ------------------------- //
// Tutte le note relative alle notifiche delle fatture elettroniche iniziano con il carattere "N"

ls_nota = "Notifica Fattura Elettronica"
ls_note = "Notifica Scaricata dall'utente " + s_cs_xx.cod_utente + "~r~n" + &
				"Data:" + string(today(), "dd/mm/yyyy") + "  Ora:" + string(now(), "hh:mm:ss") + "r~n" + as_testo_notifica

// metto via XML della risposta
l_blob = blob(ls_risposta)

ll_prog_mimetype = guo_functions.uof_get_file_prog_mimetype( "XML" )

ll_ret = uof_fattura_archivia_nota(ll_anno_registrazione, ll_num_registrazione, "FATVEN", "N", ls_nota, ls_note, l_blob, ll_prog_mimetype, ref as_message)
if ll_ret < 0 then
	return -2
end if

return 0
end function

public function integer uof_notifiche_lista (string as_tipo_fattura, ref string as_message, ref str_fatel_notifica astr_notifiche[]);/*
EnMe 24-12-2018 Lettura delle notifiche fatture
Legge tutte le notifiche (attive e passive) e poi decide cosa  fare in base a quello che viene richiesto

PARAMETRI		as_tipo_fattura A=Attive   P=Passive

RETURN		0= OK ci sono notifiche
				1= OK, ma niente notiche
				-1 = ERRORE
*/

string			ls_base64_auth, ls_partita_iva, ls_URL, ls_data, ls_risposta, ls_parsed_json, ls_id, ls_tipo_notifica
long			ll_ret, ll_i, ll_idx
n_winhttp 	ln_http
ULong 		lul_length
any 			ls_response_json, ls_array[]
uo_crypto 	luo_crypto
sailjson 		luo_parse, luo_element_array

luo_crypto = create uo_crypto

select partita_iva
into	:ls_partita_iva
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;

ls_base64_auth = luo_crypto.uof_encode_64( ls_partita_iva + ":CS.$e95" )


ls_URL  = "http://fattura.csteam.com/api/apice/notifiche/new"
ln_http.Open("GET", ls_URL)

ln_http.SetRequestHeader("Content-Type", "application/json;charset=utf-8")
ln_http.SetRequestHeader("Authorization", "Basic " + ls_base64_auth)


lul_length = ln_http.Send(ls_data)

If lul_length > 0 Then
	
	luo_parse=create sailjson
	
	luo_parse.parse( ln_http.ResponseText)
	
	ls_risposta = ln_http.ResponseText
	// Se la risposta è un JSON o un array di JSON allora ho la lista, altrimenti lista vuota.
	if left(ls_risposta,1)="{" or left(ls_risposta,1)="[" then
		ls_parsed_json = luo_parse.parse( ls_risposta )
		luo_parse.getrootarray( 	ls_array[] )
	else
		as_message = ls_risposta
		return 1
	end if
	ll_ret = upperbound(	ls_array )
	
	if ll_ret > 0  then
		/*		
		*** significato del tipo notifica ***
		ATTIVE
			RICEVUTA_CONSEGNA("RC"),
			NOTIFICA_MANCATA_CONSEGNA("MC"),
			NOTIFICA_SCARTO("NS"),
			NOTIFICA_ESITO("NE"),
			ATTESTAZIONE_TRASMISSIONE_FATTURA("AT")
		
		PASSIVE
			NOTIFICA_ESITO_COMMITTENTE("EC"),
			NOTIFICA_SCARTO_ESITO_COMMITTENTE("SE"),
			NOTIFICA_DECORRENZA_TERMINI("DT")
		*/	
		ll_idx = 0
		for ll_i = 1 to ll_ret
			luo_element_array = ls_array[ll_i]
			ls_tipo_notifica = string(luo_element_array.getattribute( 'tipo'))
			
			// se ho chiesto le notifiche attive, salto quelle relative alle passive
			if upper(as_tipo_fattura) = "A" and ( upper(ls_tipo_notifica)="EC" or upper(ls_tipo_notifica)="SE" or upper(ls_tipo_notifica)="DT")  then continue
				
			// se ho richiesto le notifiche passive, salto quelle relative alle attive
			if upper(as_tipo_fattura) = "P" and ( upper(ls_tipo_notifica)="RC" or upper(ls_tipo_notifica)="MC" or upper(ls_tipo_notifica)="NS" or upper(ls_tipo_notifica)="NE" or upper(ls_tipo_notifica)="AT")  then continue
			ll_idx++
			astr_notifiche[ll_idx].id = string(luo_element_array.getattribute( 'id'))
			astr_notifiche[ll_idx].tipo = string(luo_element_array.getattribute( 'tipo'))
			astr_notifiche[ll_idx].identificativo_sdi = string(luo_element_array.getattribute( 'identificativoSdi'))
			astr_notifiche[ll_idx].esito = string(luo_element_array.getattribute( 'esito'))
		next
		
		return 0
	else
		as_message = ls_risposta
		return 1
	end if	

else
	// Errore impossibile raggiungere il server: forse non c'è internet o il servizio non risponde
	as_message =  ln_http.lasterrortext
	return -1
end if


return 0
end function

public function integer uof_fatture_passive_lista (ref string as_message, ref str_fatel_lista_fatture_passive astr_lista[]);/*
EnMe 24-12-2018 Lettura delle notifiche fatture
Legge tutte le notifiche (attive e passive) e poi decide cosa  fare in base a quello che viene richiesto

RETURN		0= OK ci sono notifiche
				1= OK, ma niente notiche
				-1 = ERRORE
*/

string			ls_base64_auth, ls_partita_iva, ls_URL, ls_data, ls_risposta
long			ll_max, ll_i
n_winhttp 	ln_http
ULong 		lul_length
any 			l_array[]
uo_crypto 	luo_crypto
sailjson 		luo_parse, ljson

luo_crypto = create uo_crypto

select partita_iva
into	:ls_partita_iva
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;

ls_base64_auth = luo_crypto.uof_encode_64( ls_partita_iva + ":CS.$e95" )


ls_URL  = "http://fattura.csteam.com/api/apice/fatture/passive"
ln_http.Open("GET", ls_URL)

ln_http.SetRequestHeader("Content-Type", "application/json;charset=utf-8")
ln_http.SetRequestHeader("Authorization", "Basic " + ls_base64_auth)


lul_length = ln_http.Send(ls_data)

If lul_length > 0 Then
	// mi torna un array ma devo sistemarlo perchè sia compatibile con il parser
	if left(ln_http.ResponseText,1) <> "{" and left(ln_http.ResponseText,1) <> "["  then
		// nessun dato
		as_message = ln_http.ResponseText
		return 1
	end if
	ls_risposta = '{"data":' + ln_http.ResponseText + '}'
	
	luo_parse=create sailjson
	ljson=create sailjson
	
	luo_parse.parse( ls_risposta )
	
	luo_parse.getarray("data", ref l_array)
	ll_max = upperbound(l_array)
	
	for ll_i = 1 to ll_max
		ljson = l_array[ll_i]
		astr_lista[ll_i].identificativo_sdi = string(ljson.getattribute( "identificativoSdi" ))
		astr_lista[ll_i].denominazione_fornitore = string(ljson.getattribute( "denominazioneFornitore"))
		astr_lista[ll_i].partita_iva = string(ljson.getattribute( "pivaFornitore"))
		astr_lista[ll_i].id = string(ljson.getattribute( "id"))
		astr_lista[ll_i].dataOraRicezione = string(ljson.getattribute( "dataOraRicezione"))
	next

else
	// Errore impossibile raggiungere il server: forse non c'è internet o il servizio non risponde
	as_message =  ln_http.lasterrortext
	return -1
end if


return 0
end function

public function integer uof_fattura_passiva_download_pdf (long al_progessivo_id, ref string as_message, ref blob ab_blob);/*
EnMe 24-12-2018 Download notifica in XML
Legge tutte le notifiche (attive e passive) e poi decide cosa  fare in base a quello che viene richiesto

RETURN		0= OK ci sono notifiche
				1= OK, ma niente notiche
				-1 = ERRORE
*/

string			ls_base64_auth, ls_partita_iva, ls_URL, ls_data, ls_risposta,ls_username, ls_nome_cognome
n_winhttp 	ln_http
ULong 		lul_length
any 			ls_response_json
uo_crypto 	luo_crypto
sailjson 		luo_parse, luo_sendjson

luo_crypto = create uo_crypto

select partita_iva
into	:ls_partita_iva
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;

ls_base64_auth = luo_crypto.uof_encode_64( ls_partita_iva + ":CS.$e95" )

ls_url = ""
guo_functions.uof_get_parametro_azienda("UFE", ls_url)

if isnull(ls_url) or len(ls_url) < 1 then
	ls_URL  = "http://fattura.csteam.com/api/download/fattura/pdf/2/passiva/" + string(al_progessivo_id)
else
	ls_URL  = ls_url + string(al_progessivo_id)
end if
ln_http.Open("POST", ls_URL)

ln_http.SetRequestHeader("Content-Type", "application/json;charset=utf-8")
ln_http.SetRequestHeader("Authorization", "Basic " + ls_base64_auth)

// aggiuungere nome e cognome utente connesso o altre info per log (100 car max)
if s_cs_xx.cod_utente = "CS_SYSTEM" then
	ls_data = g_str.format("COD_UTENTE=$1 USERNAME=$2  NOME=$3 DATA-ORA-CLIENT=$4 $5","CS_SYSTEM", "UTENTE DI SISTEMA" ,"", string(today(),"dd/mm/yyyy"), string(now(),"hh:mm:ss"))
else
	select username, 
			nome_cognome
	into	:ls_username,
			:ls_nome_cognome
	from utenti
	where cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode < 0 then
		as_message = g_str.format("Errore SQL in lettura utente $1", s_cs_xx.cod_utente, sqlca.sqlerrtext)
		return -1
	end if

	ls_data = g_str.format("COD_UTENTE=$1 USERNAME=$2  NOME=$3 DATA-ORA-CLIENT=$4 $5",s_cs_xx.cod_utente,ls_username,ls_nome_cognome, string(today(),"dd/mm/yyyy"), string(now(),"hh:mm:ss"))
end if

lul_length = ln_http.Send(ls_data, ref ab_blob)

If lul_length > 0 Then
	
	as_message = string(ab_blob, EncodingUtf8!)
	
	if left(as_message,1) = "{" then					// JSON quindi c'è un errore
		luo_parse=create sailjson
		luo_sendjson=create sailjson
		luo_parse.parse( ln_http.ResponseText)
		
		as_message = g_str.format( "Errore in download fattura.~r~nCodice errore $1, $2.~r~n$3",string(luo_parse.getattribute( "error")) , string(luo_parse.getattribute( "message")), string(luo_parse.getattribute( "status")))
		return -1
	end if
	
else
	// Errore impossibile raggiungere il server: forse non c'è internet o il servizio non risponde
	as_message =  ln_http.lasterrortext
	return -1
end if

return 0
end function

public function integer uof_fattura_archivia_nota (long al_anno_registrazione, long al_num_registrazione, readonly string as_tipo_fattura, readonly string as_tipo_nota, readonly string as_descrizione, readonly string as_nota, readonly string as_path_file, ref string as_error);/* Archiviazione note, e file spediti 
 Tutte le note relative alla fattura elettronica iniziano con il carattere "E"
 
al_anno_registrazione = anno registrazione fattura
al_num_registrazione = num registrazione fattura
as_tipo_fattura = FATACQ fattura di acquisto / FATVEN=fattura vendita
as_tipo_nota = E si tratta di un documento fattura  /  N si tratta di una notifica
 
*/
string ls_dir, ls_filename, ls_file_ext
long ll_prog_mimetype, ll_ret
blob l_blob

ll_ret = guo_functions.uof_file_to_blob( as_path_file, l_blob)
if ll_ret < 0 then
	as_error = "Errore in archiviazione nota; impossibile salvare il file su disco (uof_fattura_archivia_nota)."
	return -1
end if
guo_functions.uof_get_file_info( as_path_file, ls_dir, ls_filename, ls_file_ext)
ll_prog_mimetype = guo_functions.uof_get_file_prog_mimetype(upper(ls_file_ext))
if ll_prog_mimetype < 0 then
	as_error = "Documento Fattura iniviato, Errore in archiviazione documento (blob). Errore in ricerca mimetype P7M del file (uof_fattura_archivia_nota)"
	return -1
end if	


ll_ret = uof_fattura_archivia_nota( al_anno_registrazione,al_num_registrazione, as_tipo_fattura, as_tipo_nota,  as_descrizione, as_nota, l_blob, ll_prog_mimetype, ref as_error)

return ll_ret



end function

public function integer uof_fattura_archivia_nota (long al_anno_registrazione, long al_num_registrazione, readonly string as_tipo_fattura, readonly string as_tipo_nota, readonly string as_descrizione, readonly string as_nota, readonly blob ab_blob, readonly long al_prog_mimetype, ref string as_error);/* Archiviazione note, e file spediti 
 Tutte le note relative alla fattura elettronica iniziano con il carattere "E"
 
al_anno_registrazione = anno registrazione fattura
al_num_registrazione = num registrazione fattura
as_tipo_fattura = FATACQ fattura di acquisto / FATVEN=fattura vendita
as_tipo_nota = E si tratta di un documento fattura  /  N si tratta di una notifica
 
*/
string		ls_cod_nota, ls_db, ls_errore, ls_tipo_nota, ls_dir, ls_filename, ls_file_ext
long		ll_ret
transaction sqlcb

ls_tipo_nota = as_tipo_nota + "%"

choose case as_tipo_fattura
	case "FATVEN"
		select 	max(cod_nota)
		into   		:ls_cod_nota
		from   	tes_fat_ven_note
		where  	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :al_anno_registrazione and
					num_registrazione = :al_num_registrazione and
					cod_nota like :ls_tipo_nota;
	case "FATACQ"
		select 	max(cod_nota)
		into   		:ls_cod_nota
		from   	tes_fat_acq_note
		where  	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :al_anno_registrazione and
					num_registrazione = :al_num_registrazione and
					cod_nota like :ls_tipo_nota;
end choose				

if isnull(ls_cod_nota) or len(ls_cod_nota) < 1 then
	ls_cod_nota = as_tipo_nota + "01"
else
	ls_cod_nota = right(ls_cod_nota, 2)
	ll_ret = long(ls_cod_nota)
	ll_ret ++
	ls_cod_nota = as_tipo_nota + string(ll_ret, "00")
end if

choose case as_tipo_fattura
	case "FATVEN"
		INSERT INTO tes_fat_ven_note  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  cod_nota,   
				  des_nota, 
				  note,
				  prog_mimetype)  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :al_anno_registrazione,   
				  :al_num_registrazione,   
				  :ls_cod_nota,   
				  :as_descrizione,   
				  :as_nota,
				  :al_prog_mimetype )  ;
		if sqlca.sqlcode < 0 then
			as_error = "Documento Fattura iniviato, Errore in archiviazione documento (blob). "  + sqlca.sqlerrtext
			return -1
		end if
	case "FATACQ"
		INSERT INTO tes_fat_acq_note  
				( cod_azienda,   
				  anno_registrazione,   
				  num_registrazione,   
				  cod_nota,   
				  des_nota, 
				  note,
				  prog_mimetype)  
		VALUES ( :s_cs_xx.cod_azienda,   
				  :al_anno_registrazione,   
				  :al_num_registrazione,   
				  :ls_cod_nota,   
				  :as_descrizione,   
				  :as_nota,
				  :al_prog_mimetype )  ;
		if sqlca.sqlcode < 0 then
			as_error = "Documento Fattura iniviato, Errore in archiviazione documento (blob). "  + sqlca.sqlerrtext
			return -1
		end if
end choose
ls_db = f_db()


if ls_db = "MSSQL" then
	
	if not guo_functions.uof_create_transaction_from( sqlca, sqlcb, ls_errore) then
		g_mb.error(ls_errore)
		return -2
	end if
			
	// devo farfe il commit del record inserito nella transazione precedente
	commit using sqlca;
		
	choose case as_tipo_fattura
		case "FATVEN"
		updateblob 	tes_fat_ven_note
		set        		note_esterne = :ab_blob
		where      	cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :al_anno_registrazione and
						num_registrazione = :al_num_registrazione and
						cod_nota = :ls_cod_nota
		using sqlcb;
		case "FATACQ"
		updateblob 	tes_fat_acq_note
		set        		note_esterne = :ab_blob
		where      	cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :al_anno_registrazione and
						num_registrazione = :al_num_registrazione and
						cod_nota = :ls_cod_nota
		using sqlcb;
	end choose
	if sqlcb.sqlcode < 0 then
		as_error = "Documento Fattura iniviato, , ma c'è un errore in memorizzazione (blob) del documento XML" + sqlcb.sqlerrtext
		rollback using sqlcb;
		rollback using sqlca;
		return -1
	end if 
	commit using sqlcb;
	
	destroy sqlcb;

else 
	choose case as_tipo_fattura
		case "FATVEN"
		updateblob 	tes_fat_ven_note
		set        		note_esterne = :ab_blob
		where      	cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :al_anno_registrazione and
						num_registrazione = :al_num_registrazione and
						cod_nota = :ls_cod_nota;
		case "FATACQ"
		updateblob 	tes_fat_acq_note
		set        		note_esterne = :ab_blob
		where      	cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :al_anno_registrazione and
						num_registrazione = :al_num_registrazione and
						cod_nota = :ls_cod_nota;
	end choose
	if sqlca.sqlcode < 0 then
		as_error = "Documento Fattura iniviato, , ma c'è un errore in memorizzazione (blob) del documento XML" + sqlca.sqlerrtext
		rollback;
		return -1
	end if
end if 
return 0

end function

public function integer uof_conferma_fattura_passiva (long al_progessivo_id, ref string as_message);/*
EnMe 24-12-2018 Download notifica in XML
Legge tutte le notifiche (attive e passive) e poi decide cosa  fare in base a quello che viene richiesto

RETURN		0= OK ci sono notifiche
				-1 = ERRORE
*/

string			ls_base64_auth, ls_partita_iva, ls_URL, ls_data, ls_risposta,ls_username, ls_nome_cognome, ls_identificativo_sdi, &
				ls_cod_nota, ls_nota, ls_note, ls_db, ls_errore, ls_flag_esito_fatel
long			ll_anno_registrazione, ll_num_registrazione, ll_cont_sdi,ll_ret, ll_prog_mimetype, ll_esito_consegna
transaction	sqlcb
blob			l_blob
n_winhttp 	ln_http
ULong 		lul_length
uo_crypto 	luo_crypto
sailjson 		luo_parse, luo_sendjson


luo_crypto = create uo_crypto

select partita_iva
into	:ls_partita_iva
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;

ls_base64_auth = luo_crypto.uof_encode_64( ls_partita_iva + ":CS.$e95" )


ls_URL  = "http://fattura.csteam.com/api/apice/fattura/conferma?idDb=" + string(al_progessivo_id)
ln_http.Open("POST", ls_URL)

ln_http.SetRequestHeader("Content-Type", "application/json;charset=utf-8")
ln_http.SetRequestHeader("Authorization", "Basic " + ls_base64_auth)

// aggiungere nome e cognome utente connesso o altre info per log (100 car max)
if s_cs_xx.cod_utente = "CS_SYSTEM" then
	ls_data = g_str.format("COD_UTENTE=$1 USERNAME=$2  NOME=$3 DATA-ORA-CLIENT=$4 $5","CS_SYSTEM", "UTENTE DI SISTEMA" ,"", string(today(),"dd/mm/yyyy"), string(now(),"hh:mm:ss"))
else
	select username, 
			nome_cognome
	into	:ls_username,
			:ls_nome_cognome
	from utenti
	where cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode < 0 then
		as_message = g_str.format("Errore SQL in lettura utente $1", s_cs_xx.cod_utente, sqlca.sqlerrtext)
		return -1
	end if
	ls_data = g_str.format("COD_UTENTE=$1 USERNAME=$2  NOME=$3 DATA-ORA-CLIENT=$4 $5",s_cs_xx.cod_utente,ls_username,ls_nome_cognome, string(today(),"dd/mm/yyyy"), string(now(),"hh:mm:ss"))
end if

lul_length = ln_http.Send(ls_data)

If lul_length > 0 Then
	luo_parse=create sailjson
	luo_sendjson=create sailjson
	luo_parse.parse( ln_http.ResponseText)
	ls_risposta = ln_http.ResponseText	
else
	// Errore impossibile raggiungere il server: forse non c'è internet o il servizio non risponde
	as_message =  ln_http.lasterrortext
	return -1
end if

return 0

end function

public function integer uof_fatture_passive_allegati_lista (long al_id_fattura, ref str_fatel_lista_allegati_fat_acq astr_lista[], ref string as_message);/*
EnMe 05/02-2018 Lettura delle notifiche fatture
Legge tutte le notifiche (attive e passive) e poi decide cosa  fare in base a quello che viene richiesto

RETURN		0= OK ci sono notifiche
				1= OK, ma niente notiche
				-1 = ERRORE
*/

string			ls_base64_auth, ls_partita_iva, ls_URL, ls_data, ls_risposta
long			ll_max, ll_i
n_winhttp 	ln_http
ULong 		lul_length
any 			l_array[]
uo_crypto 	luo_crypto
sailjson 		luo_parse, ljson

luo_crypto = create uo_crypto

select partita_iva
into	:ls_partita_iva
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;

ls_base64_auth = luo_crypto.uof_encode_64( ls_partita_iva + ":CS.$e95" )


ls_URL  = "http://fattura.csteam.com/api/apice/fattura/allegati/" + string(al_id_fattura)
ln_http.Open("GET", ls_URL)

ln_http.SetRequestHeader("Content-Type", "application/json;charset=utf-8")
ln_http.SetRequestHeader("Authorization", "Basic " + ls_base64_auth)


lul_length = ln_http.Send(ls_data)

If lul_length > 0 Then
	// mi torna un array ma devo sistemarlo perchè sia compatibile con il parser
	if left(ln_http.ResponseText,1) <> "{" and left(ln_http.ResponseText,1) <> "["  then
		// nessun dato
		as_message = ln_http.ResponseText
		return 1
	end if
	ls_risposta = '{"data":' + ln_http.ResponseText + '}'
	
	luo_parse=create sailjson
	ljson=create sailjson
	
	luo_parse.parse( ls_risposta )
	
	luo_parse.getarray("data", ref l_array)
	ll_max = upperbound(l_array)
	
	for ll_i = 1 to ll_max
		ljson = l_array[ll_i]
		astr_lista[ll_i].id_fattura = string(ljson.getattribute( "idFattura"))
		astr_lista[ll_i].id_allegato = string(ljson.getattribute( "id" ))
		astr_lista[ll_i].nome_allegato = string(ljson.getattribute( "nomeAllegato"))
	next

else
	// Errore impossibile raggiungere il server: forse non c'è internet o il servizio non risponde
	as_message =  ln_http.lasterrortext
	return -1
end if


return 0
end function

public function integer uof_fattura_passiva_download_allegato (long al_progessivo_id, ref string as_message, ref blob ab_blob, ref string as_file_ext, ref string as_file_code);/*
EnMe 24-12-2018 Download notifica in XML
Legge tutte le notifiche (attive e passive) e poi decide cosa  fare in base a quello che viene richiesto

RETURN		0= OK ci sono notifiche
				1= OK, ma niente notiche
				-1 = ERRORE
*/

string			ls_base64_auth, ls_partita_iva, ls_URL, ls_data, ls_risposta,ls_username, ls_nome_cognome
long			ll_i
ULong 		lul_length
any 			ls_response_json
n_winhttp 	ln_http
uo_crypto 	luo_crypto
sailjson 		luo_parse, luo_sendjson

luo_crypto = create uo_crypto

select partita_iva
into	:ls_partita_iva
from aziende
where cod_azienda = :s_cs_xx.cod_azienda;

ls_base64_auth = luo_crypto.uof_encode_64( ls_partita_iva + ":CS.$e95" )

ls_URL  = "http://fattura.csteam.com/api/download/fattura/allegato/" + string(al_progessivo_id)

ln_http.Open("POST", ls_URL)

ln_http.SetRequestHeader("Content-Type", "application/json;charset=utf-8")
ln_http.SetRequestHeader("Authorization", "Basic " + ls_base64_auth)

// aggiuungere nome e cognome utente connesso o altre info per log (100 car max)
if s_cs_xx.cod_utente = "CS_SYSTEM" then
	ls_data = g_str.format("COD_UTENTE=$1 USERNAME=$2  NOME=$3 DATA-ORA-CLIENT=$4 $5","CS_SYSTEM", "UTENTE DI SISTEMA" ,"", string(today(),"dd/mm/yyyy"), string(now(),"hh:mm:ss"))
else
	select username, 
			nome_cognome
	into	:ls_username,
			:ls_nome_cognome
	from utenti
	where cod_utente = :s_cs_xx.cod_utente;
	
	if sqlca.sqlcode < 0 then
		as_message = g_str.format("Errore SQL in lettura utente $1", s_cs_xx.cod_utente, sqlca.sqlerrtext)
		return -1
	end if

	ls_data = g_str.format("COD_UTENTE=$1 USERNAME=$2  NOME=$3 DATA-ORA-CLIENT=$4 $5",s_cs_xx.cod_utente,ls_username,ls_nome_cognome, string(today(),"dd/mm/yyyy"), string(now(),"hh:mm:ss"))
end if

lul_length = ln_http.Send(ls_data, ref ab_blob)

If lul_length > 0 Then
	
	as_message = ln_http.ResponseText
//	as_message = string(ab_blob, EncodingUtf8!)
	
	if left(as_message,1) = "{" then					// JSON quindi c'è un errore
		luo_parse=create sailjson
		luo_sendjson=create sailjson
		luo_parse.parse( ln_http.ResponseText)
		
		as_message = g_str.format( "Errore in download fattura.~r~nCodice errore $1, $2.~r~n$3",string(luo_parse.getattribute( "error")) , string(luo_parse.getattribute( "message")), string(luo_parse.getattribute( "status")))
		return -1
	end if
	
	if left(as_message,1) = "<" then					// Torna un HTML quindi c'è un errore di sistema
		as_message = "Errore in download fattura.~r~nErrore di sistema remoto~r~ncontattare il servizio di Assistenza."
		return -1
	end if
	
	// Leggo il tipo di file che ho scaricato
	for ll_i = 1 to upperbound(ln_http.headers)
		if pos(lower(ln_http.headers[ll_i]),"content-disposition:") > 0 then
			as_file_ext = mid(ln_http.headers[ll_i], 21)
			if pos(as_file_ext,"=") > 0 then	// tipo di codifica
				as_file_ext= mid(as_file_ext,1,pos(as_file_ext,";") - 1)
				as_file_code= mid(as_file_ext,pos(as_file_ext,";") + 1)
			end if
		end if
	next
	
else
	// Errore impossibile raggiungere il server: forse non c'è internet o il servizio non risponde
	as_message =  ln_http.lasterrortext
	return -1
end if

return 0
end function

private subroutine uof_add_body_causale ();if not g_str.isempty(is_datigen_causale )then
	uof_element_create( ref pdel_body_datigeneralidocumento_causale  ,"Causale")
	pdel_body_datigeneralidocumento_causale.setText(is_datigen_causale)
	pdel_body_datigeneralidocumento.addcontent(pdel_body_datigeneralidocumento_causale)
end if
end subroutine

private subroutine uof_add_body_conai_assolto ();uof_element_create( ref pdel_body_datigeneralidocumento_causale  ,"Causale")
pdel_body_datigeneralidocumento_causale.setText("Contributo C.O.N.A.I. assolto ove dovuto.")
pdel_body_datigeneralidocumento.addcontent(pdel_body_datigeneralidocumento_causale)

end subroutine

private subroutine uof_add_body_datibollo ();if not g_str.isempty(is_datigen_bollovirtuale )then
	uof_element_create( ref pdel_body_datigeneralidocumento_bollovirtuale  ,"BolloVirtuale")
	pdel_body_datigeneralidocumento_bollovirtuale.setText(is_datigen_bollovirtuale)
	pdel_body_datigeneralidocumento_datibollo.addcontent(pdel_body_datigeneralidocumento_bollovirtuale)

	uof_element_create( ref pdel_body_datigeneralidocumento_importobollo  ,"ImportoBollo")
	pdel_body_datigeneralidocumento_importobollo.setText(is_datigen_importo_bollo)
	pdel_body_datigeneralidocumento_datibollo.addcontent(pdel_body_datigeneralidocumento_importobollo)
end if
end subroutine

private function string uof_notifica_format (string as_xml, ref string as_format, ref string as_identificativo_sdi);string 					ls_testo_notifica, ls_esito
PBDOM_BUILDER 		pb_builder
PBDOM_Document    	pbdom_doc	
PBDOM_Element     	pbdom_elem, pbdom_element_childs[]


TRY
	pb_builder = create PBDOM_BUILDER
	pbdom_doc = pb_builder.buildfromstring( as_xml )
	pbdom_elem = pbdom_doc.GetRootElement()
	if isvalid(pbdom_elem) then 
		ls_testo_notifica = pbdom_elem.GetName()
	else
		as_format = "Errore nel formato XML della notifica (uof_notifica_format)"
		return "ER"
	end if
	
CATCH ( PBDOM_Exception pbde )   
	as_format = g_str.format( "PBDOM Exception: $1", pbde.getMessage() )
	return "ER"
CATCH ( PBXRuntimeError re )   
	as_format = g_str.format( "PBNI Exception: $1", re.getMessage() )
	return "ER"
END TRY


choose case lower(ls_testo_notifica)
	case "ricevutaconsegna"
		ls_esito="RC"
	case "ricevutascarto"
		ls_esito ="NS"
	case "notificaesitocommittente"
		ls_esito="NE"
	case "notificamancataconsegna"
		ls_esito="MC"
	case "ricevutaimpossibilitarecapito"
		ls_esito = "EC"
	case "scartoesitocommittente"
		ls_esito = "SE"
	case "notificadecorrenzatermini"
		ls_esito = "DT"
	case "attestazionetrasmissionefattura"
		ls_esito = "AT"
end choose

// recupero quello che mi serve dalla ricevuta
if uof_notifica_format_child(pbdom_elem, ref ls_testo_notifica, ref as_identificativo_sdi) = -1 then ls_esito = "ER"
	
as_format=ls_testo_notifica 

return ls_esito

end function

private subroutine uof_add_header_cessionario_codicefiscale ();pdel_cessionariocommittente_codicefiscale.setText(is_cessionario_codicefiscale)
pdel_cessionariocommittente_datianagrafici.addcontent(pdel_cessionariocommittente_codicefiscale)

end subroutine

private subroutine uof_add_body_riferimentonumerolinea (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_body_dati_ordine_acquisto[al_indice].riferimentonumerolinea)then
	uof_element_create( ref pdel_body_datigeneralidocumento_riferimentonumerolinea,"RiferimentoNumeroLinea")
	pdel_body_datigeneralidocumento_riferimentonumerolinea.setText(istr_body_dati_ordine_acquisto[al_indice].riferimentonumerolinea)
	apbdom_element.addcontent(pdel_body_datigeneralidocumento_riferimentonumerolinea)
end if
end subroutine

private subroutine uof_add_body_iddocumento (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_body_dati_ordine_acquisto[al_indice].iddocumento)then
	uof_element_create( ref pdel_body_datigeneralidocumento_iddocumento,"IdDocumento")
	pdel_body_datigeneralidocumento_iddocumento.setText(istr_body_dati_ordine_acquisto[al_indice].iddocumento)
	apbdom_element.addcontent(pdel_body_datigeneralidocumento_iddocumento)
end if
end subroutine

private subroutine uof_add_body_codicecig (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_body_dati_ordine_acquisto[al_indice].codicecig)then
	uof_element_create( ref pdel_body_datigeneralidocumento_codicecig,"CodiceCIG")
	pdel_body_datigeneralidocumento_codicecig.setText(istr_body_dati_ordine_acquisto[al_indice].codicecig)
	apbdom_element.addcontent(pdel_body_datigeneralidocumento_codicecig)
end if
end subroutine

public function integer uof_invia_fattura (long al_anno_registrazione, long al_num_registrazione, string as_flag_cliente_pa, string as_path_file_p7m, string as_nome_file_p7m, ref string as_identificativo_sdi, ref string as_message, ref string as_error);// EnMe 15-12-2018 Invio fattura in XML tramite chiamata REST
/*
Long		al_anno_registrazione
			al_num_registrazione
String		as_message

RETURN	Integer 	0=OK
						-1=Errore
						-2=Fattura Elettronica OK, ma c'è un errore in archiviazione dei dati
*/

n_winhttp ln_http
string 	ls_URL, ls_title, ls_pathname, ls_filename, ls_token, ls_license_active, ls_license_status, ls_response, ls_file, ls_base64, &
			ls_filter, ls_mimetype, ls_data, ls_license_crtypted, ls_validation_code, ls_base64_auth, ls_partita_iva,ls_paese,ls_note, ls_risposta, &
			ls_cod_documento, ls_numeratore_documento, ls_file_xml, ls_base64_p7m, ls_cod_nota, ls_nota, ls_cod_cliente, ls_rag_soc_1, ls_sigla_anno
Integer 	li_rc, li_fnum
long		ll_anno_documento, ll_numero_documento,ll_ret, ll_seconds
dec{4} 	ld_importo_iva, ld_imponibile_iva, ld_tot_fattura
datetime	ldt_data_fattura
ULong 	lul_length
blob 		lblob_file, lblob_send
uo_crypto luo_crypto

sailjson luo_parse, luo_sendjson

if as_flag_cliente_pa="N" then
	// cliente privato
	if uof_fattura_xml( al_anno_registrazione, al_num_registrazione, ls_file) < 0 then
		as_message = ls_file 
		return -1
	end if
	// Endpoint per TEST INTERNO CS
	//ls_URL  = "http://192.168.200.124:8081/api/tx/fattura/FPR"
	// Endpoint produzione
	ls_URL  = "http://fattura.csteam.com/api/tx/fattura/FPR"
else
//	if uof_fattura_xml( al_anno_registrazione, al_num_registrazione, ls_file_xml) < 0 then
//		as_message = ls_file_xml 
//		return -1
//	end if
	// pubblica amministrazione; leggo il file dalla path caricata dal cliente
	ls_file = as_path_file_p7m
	// Endpoint per TEST INTERNO CS
	//ls_URL  = "http://192.168.200.124:8081/api/tx/fattura/FPA"
	// Endpoint produzione
	ls_URL  = "http://fattura.csteam.com/api/tx/fattura/FPA"
end if
// calcolo il nome del file //

select fatel_idpaese_trasmittente, 
		partita_iva
into	:ls_paese,
		:ls_partita_iva
from  aziende
where cod_azienda = :s_cs_xx.cod_azienda;
if sqlca.sqlcode < 0 then
	as_message = "(uof_invia_fattura) Errore in ricerca partita iva azienda."
	return -1
end if

select tes_fat_ven.cod_documento, 
		tes_fat_ven.numeratore_documento,
		tes_fat_ven.anno_documento,
		tes_fat_ven.num_documento,
		tes_fat_ven.data_fattura,
		anag_clienti.cod_cliente,
		anag_clienti.rag_soc_1,
		tes_fat_ven.importo_iva,
		tes_fat_ven.imponibile_iva,
		tes_fat_ven.tot_fattura
into	:ls_cod_documento, 
		:ls_numeratore_documento,
		:ll_anno_documento,
		:ll_numero_documento,
		:ldt_data_fattura,
		:ls_cod_cliente,
		:ls_rag_soc_1,
		:ld_importo_iva,
		:ld_imponibile_iva,
		:ld_tot_fattura
from 	tes_fat_ven
left join anag_clienti on anag_clienti.cod_azienda=tes_fat_ven.cod_azienda and anag_clienti.cod_cliente=tes_fat_ven.cod_cliente
where tes_fat_ven.cod_azienda=:s_cs_xx.cod_azienda and
		tes_fat_ven.anno_registrazione = :al_anno_registrazione and
		tes_fat_ven.num_registrazione = :al_num_registrazione;
if sqlca.sqlcode < 0 then
	as_message = "(uof_invia_fattura) Errore in ricerca fattura. " + sqlca.sqlerrtext
	return -1
end if
if sqlca.sqlcode = 100 then
	as_message = "(uof_invia_fattura) Fattura non trovata."
	return -1
end if

if len(ls_partita_iva) = 0 then
	as_message = "(uof_invia_fattura) partita iva vuota in anagrafica azienda."
	return -1
end if

uof_get_sigla_anno( al_anno_registrazione, ref ls_sigla_anno)
	
if as_flag_cliente_pa="S" then 
	ls_filename = ls_file
else
	ls_filename = ls_paese + ls_partita_iva + "_" + ls_sigla_anno + string(al_num_registrazione,"0000") + ".xml"
end if	

luo_crypto = create uo_crypto

if as_flag_cliente_pa="N" then
	guo_functions.uof_file_to_blob(ls_file, lblob_send)
	ls_base64 = luo_crypto.uof_encode_64(lblob_send)
else
	// se fattura PA passo XML e P7M
	guo_functions.uof_file_to_blob(ls_file, lblob_send)
	ls_base64_p7m = luo_crypto.uof_encode_64(lblob_send)

//	ls_base64_p7m=g_str.replaceall(ls_base64_p7m, char(10), "", true)
//	ls_base64_p7m=g_str.replaceall(ls_base64_p7m, char(13), "", true)

	// poi passo al file xml
//	li_fnum = FileOpen(ls_file_xml, TextMode!, Read!, LockReadWrite! , Append!, EncodingUTF8!)
//	lul_length = FileReadEx(li_fnum, ls_data)
//	FileClose(li_fnum)
/*	
	ls_base64 = luo_crypto.uof_encode_64( ls_data, 2 )
	ls_base64=g_str.replaceall(ls_base64, char(10), "", true)
	ls_base64=g_str.replaceall(ls_base64, char(13), "", true)
	*/
end if

ls_base64_auth = luo_crypto.uof_encode_64( ls_partita_iva + ":CS.$e95",2 )

ln_http.Open("POST", ls_URL)

ls_note = s_cs_xx.cod_utente + "_" + string(today()) + "_" +string(now())

if as_flag_cliente_pa="N" then
	ln_http.SetRequestHeader("Content-Type", "application/json;charset=utf-8")
	ln_http.SetRequestHeader("Authorization", "Basic " + ls_base64_auth)
	ls_data = '{"fattura":"'+ls_base64+'", "nomeFile":"'+ls_filename+'","note":"'+ls_note+'"}'
else
	ln_http.SetRequestHeader("Content-Type", "application/json;charset=utf-8")
	ln_http.SetRequestHeader("Authorization", "Basic " + ls_base64_auth)
	ls_data = '{"fattura":"' + ls_base64_p7m + '", "nomeFile":"'+as_nome_file_p7m+'","note":"'+ls_note+'"}'
end if	

lul_length = ln_http.Send(ls_data)
If lul_length > 0 Then
	
	luo_parse=create sailjson
	luo_sendjson=create sailjson
	
	luo_parse.parse( ln_http.ResponseText)
	
	ls_risposta = ln_http.ResponseText
	
	// se mi torna un JSON vuol dire che abbiamno un errore
	if left(ls_risposta,1)="{" then
		as_message = g_str.format("ERRORE in invio Fattura $1-$2 $3-$4. Contattare il servizio di assistenza. Dettaglio Errore $5 ", ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_numero_documento, ln_http.lasterrortext)
		return -1
	end if	
	
	as_identificativo_sdi = ls_risposta
	as_message= g_str.format("Invio Fattura $1-$2 $3-$4 Eseguito con successo. Codice Invio=$5", ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_numero_documento, ls_risposta)
	
	update 	tes_fat_ven
	set 		identificativo_sdi = :as_identificativo_sdi,
				flag_esito_fatel = 'NI'
	where  	cod_azienda = :s_cs_xx.cod_azienda and
			 	anno_registrazione = :al_anno_registrazione and
			 	num_registrazione = :al_num_registrazione;
	if sqlca.sqlcode < 0 then
		as_error = "Documento Fattura iniviato, Errore in memorizzazione identificativo SDI " + as_identificativo_sdi + "~r~n"  + sqlca.sqlerrtext
		return -2
	end if
	
else
	// Errore impossibile raggiungere il server: forse non c'è internet o il servizio non risponde
	as_message = g_str.format("ERRORI in invio Fattura $1-$2 $3-$4. Dettaglio Errore $5 ", ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_numero_documento, ln_http.lasterrortext)
	return -1
end if


// ----------------------  archivio i risultati ------------------------- //
// Tutte le note relative alla fattura elettronica iniziano con il carattere "E"

ls_nota = "Fattura elettronica - invio"
ls_note = "Fattura Inviata dall'utente " + s_cs_xx.cod_utente + "~r~n" + &
				"Data invio:" + string(today(), "dd/mm/yyyy") + "  Ora invio:" + string(now(), "hh:mm:ss") + "~r~n" + &
				"Risposta SDI:" + "~r~n" + ls_risposta
ll_ret = uof_fattura_archivia_nota( al_anno_registrazione, al_num_registrazione, "FATVEN", "E", ls_nota,ls_note, ls_file, ref as_error)
if ll_ret <> 0 then
	return -2
end if

/* ----------------------- Archivio i files anche su Elastic --------------------------- */ 
string ls_str, ls_messaggio, ls_local_metadata, ls_elastic
string	ls_elastic_file_dir, ls_elastic_filename, ls_elastic_file_ext
long  ll_identity

guo_functions.uof_get_parametro_azienda( "ELD", ls_elastic)
if isnull(ls_elastic) or len(ls_elastic)<0 then ls_elastic="N"

if ls_elastic="S" then
	
	
	guo_functions.uof_get_file_info( ls_file, ls_elastic_file_dir, ls_elastic_filename, ls_elastic_file_ext)
	ls_elastic_filename = ls_elastic_filename + ".xml"

	ls_local_metadata = "s_file_name=" + ls_elastic_filename + char(13) + char(10)
	ls_local_metadata += "s_cliente.id_file_trasmesso=" + ls_filename  + char(13) + char(10)
	ls_local_metadata += "s_cliente.id_codice_cliente=" + ls_cod_cliente  + char(13) + char(10)
	ls_local_metadata += "s_cliente.denominazione="  + ls_rag_soc_1 + char(13) + char(10)

	ls_str = string( date(ldt_data_fattura) )
	ll_seconds = guo_functions.uof_string_to_date_epoc(ls_str)
	ls_local_metadata += "d_fattura.data=" +  string(ll_seconds)  + "" + char(13) + char(10)
	ls_local_metadata += "s_fattura.numero="  + g_str.format("$1-$2/$3-$4", ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_numero_documento)  +  char(13) + char(10)
	
	ls_str= g_str.replaceall( string(ld_tot_fattura), ",", ".", true)
	ls_local_metadata += "n_fattura.importo_totale_documento=" + ls_str  + char(13) + char(10)
	
	ls_str= g_str.replaceall( string(ld_imponibile_iva), ",", ".", true)
	ls_local_metadata += "n_fattura.imponibile_documento=" + ls_str + char(13) + char(10)
	
	ls_str= g_str.replaceall( string(ld_importo_iva), ",", ".", true)
	ls_local_metadata += "n_fattura.iva_documento=" + ls_str  + char(13) + char(10)
	
	ls_local_metadata += "s_file_mimetype.estensione=xml"  + char(13) + char(10)
	ls_local_metadata += "s_file_mimetype.mimetype=text/xml" 
		
	// metto via il file XML
	ll_identity = len(lblob_send)

	
	INSERT INTO elastic_document
			( file_name,   
			  file_metadata,   
			  elastic_index,   
			  elastic_type,   
			  flag_elaborato )  
	VALUES ( :ls_elastic_filename,   
			  :ls_local_metadata,   
			  'omnia-fattura-attiva',   
			  'text/xml',   
			  'N')  ;
	if sqlca.sqlcode < 0 then
		g_mb.error("Documento Inviato, ma c'è stato un errore in Archiviazione metadati documento." + sqlca.sqlerrtext )
		rollback;
		return -2
	end if
	
	guo_functions.uof_get_identity( sqlca, ll_identity)
	
	updateblob elastic_document
	set file_data = :lblob_send 
	where id = :ll_identity;
	if sqlca.sqlcode < 0 then
		g_mb.error("Documento Inviato, ma c'è stato un errore in Archiviazione dati documento." + sqlca.sqlerrtext )
		rollback;
		return -2
	end if
	
	commit;
end if


return 0
end function

public subroutine uof_get_sigla_anno (long al_anno_registrazione, ref string as_sigla);long ll_offset
string ls_sigla

ll_offset = al_anno_registrazione - 2019
if ll_offset = 0 then
	as_sigla = "0"
else
	// parto dalla lettera A e arrivo alla Z
	as_sigla = charA(64 + ll_offset)
end if

return
end subroutine

private subroutine uof_add_body_numitem (ref pbdom_element apbdom_element, long al_indice);if not g_str.isempty(istr_body_dati_ordine_acquisto[al_indice].numitem)then
	uof_element_create( ref pdel_body_datigeneralidocumento_numitem,"NumItem")
	pdel_body_datigeneralidocumento_numitem.setText(istr_body_dati_ordine_acquisto[al_indice].numitem)
	apbdom_element.addcontent(pdel_body_datigeneralidocumento_numitem)
end if
end subroutine

on uo_fatel.create
call super::create
TriggerEvent( this, "constructor" )
end on

on uo_fatel.destroy
TriggerEvent( this, "destructor" )
call super::destroy
end on

