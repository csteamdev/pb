﻿$PBExportHeader$w_contabilizza_fat_acq_impresa.srw
$PBExportComments$Finestra Contabilizzazione Fatture di Acquisto
forward
global type w_contabilizza_fat_acq_impresa from w_cs_xx_principale
end type
type tab_1 from tab within w_contabilizza_fat_acq_impresa
end type
type tab_ricerca from userobject within tab_1
end type
type cb_cerca from commandbutton within tab_ricerca
end type
type cb_annulla from commandbutton within tab_ricerca
end type
type dw_selezione from uo_cs_xx_dw within tab_ricerca
end type
type tab_ricerca from userobject within tab_1
cb_cerca cb_cerca
cb_annulla cb_annulla
dw_selezione dw_selezione
end type
type tab_da_contabilizzare from userobject within tab_1
end type
type cb_contabilizza from commandbutton within tab_da_contabilizzare
end type
type dw_lista_contabilizzazione from uo_cs_xx_dw within tab_da_contabilizzare
end type
type tab_da_contabilizzare from userobject within tab_1
cb_contabilizza cb_contabilizza
dw_lista_contabilizzazione dw_lista_contabilizzazione
end type
type tab_contabilizzate from userobject within tab_1
end type
type cb_stampa_contabilizzate from commandbutton within tab_contabilizzate
end type
type mle_lista_fatt_contabilizzate from multilineedit within tab_contabilizzate
end type
type tab_contabilizzate from userobject within tab_1
cb_stampa_contabilizzate cb_stampa_contabilizzate
mle_lista_fatt_contabilizzate mle_lista_fatt_contabilizzate
end type
type tab_non_contabilizzate from userobject within tab_1
end type
type cb_stampa_non_contabilizzate from commandbutton within tab_non_contabilizzate
end type
type mle_lista_fatt_non_contabilizzate from multilineedit within tab_non_contabilizzate
end type
type tab_non_contabilizzate from userobject within tab_1
cb_stampa_non_contabilizzate cb_stampa_non_contabilizzate
mle_lista_fatt_non_contabilizzate mle_lista_fatt_non_contabilizzate
end type
type tab_1 from tab within w_contabilizza_fat_acq_impresa
tab_ricerca tab_ricerca
tab_da_contabilizzare tab_da_contabilizzare
tab_contabilizzate tab_contabilizzate
tab_non_contabilizzate tab_non_contabilizzate
end type
end forward

global type w_contabilizza_fat_acq_impresa from w_cs_xx_principale
integer width = 4361
integer height = 2340
string title = "Contabilizzazione Fatture di ACQUISTO"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
string pointer = "Cross!"
tab_1 tab_1
end type
global w_contabilizza_fat_acq_impresa w_contabilizza_fat_acq_impresa

type variables
string				is_sql_origine, is_mov_intra = ""
string				is_sql_orderby =	"order by tes_fat_acq.cod_doc_origine ASC,"+&
														"tes_fat_acq.anno_doc_origine ASC,"+&
														"tes_fat_acq.numeratore_doc_origine ASC,"+&
														"tes_fat_acq.num_doc_origine ASC,"+&
														"tes_fat_acq.data_doc_origine ASC "

end variables

forward prototypes
public function long wf_contabilizza (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio, ref string fs_errore)
public function string wf_tipi_det_acq_trasporti ()
end prototypes

public function long wf_contabilizza (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio, ref string fs_errore);long ll_return, ll_id_registrazione, ll_anno_registrazione, ll_num_registrazione, ll_num_reg_pd
uo_impresa lu_impresa

lu_impresa = CREATE uo_impresa

is_mov_intra = ""

lu_impresa.is_mov_intra = ""

lu_impresa.uof_set_tipo_det_trasporto()

ll_return = lu_impresa.uof_contabilizza_fat_acq(fl_anno_registrazione, fl_num_registrazione, ll_num_reg_pd)
if ll_return = 0 then
	fs_messaggio = lu_impresa.i_messaggio

	if lu_impresa.is_mov_intra<>"" and not isnull(lu_impresa.is_mov_intra) then
		is_mov_intra = lu_impresa.is_mov_intra
	end if

	destroy lu_impresa
	return ll_num_reg_pd
else
	fs_messaggio = lu_impresa.i_messaggio
	fs_errore    = string(lu_impresa.i_db_errore)
	destroy lu_impresa
	return ll_return
end if
return 0
end function

public function string wf_tipi_det_acq_trasporti ();string				ls_DTA, as_array[]
long				ll_index


guo_functions.uof_get_parametro_azienda("DTA", ls_DTA)

if isnull(ls_DTA) then ls_DTA = ""


g_str.explode(ls_DTA, ",", as_array[])

ls_DTA = ""

for ll_index=1 to upperbound(as_array[])
	if ll_index > 1 then ls_DTA += ","
	ls_DTA += "'" + as_array[ll_index] + "'"
next

return ls_DTA
end function

on w_contabilizza_fat_acq_impresa.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_contabilizza_fat_acq_impresa.destroy
call super::destroy
destroy(this.tab_1)
end on

event pc_setwindow;call super::pc_setwindow;string ls_messaggio


set_w_options(c_noenablepopup)


tab_1.tab_ricerca.dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

tab_1.tab_da_contabilizzare.dw_lista_contabilizzazione.set_dw_options(sqlca, &
                              pcca.null_object, &
										c_nonew + &
										c_nomodify + &
										c_nodelete + &
										c_disablecc  + &
										c_disableccinsert, &
										c_nohighlightselected + c_nocursorrowpointer + c_nocursorrowfocusrect)
										
save_on_close(c_socnosave)

//tab_1.tab_ricerca.cb_annulla.postevent("clicked")

is_sql_origine = tab_1.tab_da_contabilizzare.dw_lista_contabilizzazione.getsqlselect( )

return 0
end event

event pc_setddlb;f_po_loaddddw_dw(tab_1.tab_ricerca.dw_selezione, &
                 "cod_documento", &
                 sqlca, &
                 "tab_documenti", &
                 "cod_documento", &
                 "des_documento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
return 0
end event

type tab_1 from tab within w_contabilizza_fat_acq_impresa
event create ( )
event destroy ( )
integer x = 32
integer y = 36
integer width = 4302
integer height = 2200
integer taborder = 70
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
boolean boldselectedtext = true
integer selectedtab = 1
tab_ricerca tab_ricerca
tab_da_contabilizzare tab_da_contabilizzare
tab_contabilizzate tab_contabilizzate
tab_non_contabilizzate tab_non_contabilizzate
end type

on tab_1.create
this.tab_ricerca=create tab_ricerca
this.tab_da_contabilizzare=create tab_da_contabilizzare
this.tab_contabilizzate=create tab_contabilizzate
this.tab_non_contabilizzate=create tab_non_contabilizzate
this.Control[]={this.tab_ricerca,&
this.tab_da_contabilizzare,&
this.tab_contabilizzate,&
this.tab_non_contabilizzate}
end on

on tab_1.destroy
destroy(this.tab_ricerca)
destroy(this.tab_da_contabilizzare)
destroy(this.tab_contabilizzate)
destroy(this.tab_non_contabilizzate)
end on

type tab_ricerca from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 4265
integer height = 2072
long backcolor = 12632256
string text = "Ricerca"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
string picturename = "Search!"
long picturemaskcolor = 12632256
cb_cerca cb_cerca
cb_annulla cb_annulla
dw_selezione dw_selezione
end type

on tab_ricerca.create
this.cb_cerca=create cb_cerca
this.cb_annulla=create cb_annulla
this.dw_selezione=create dw_selezione
this.Control[]={this.cb_cerca,&
this.cb_annulla,&
this.dw_selezione}
end on

on tab_ricerca.destroy
destroy(this.cb_cerca)
destroy(this.cb_annulla)
destroy(this.dw_selezione)
end on

type cb_cerca from commandbutton within tab_ricerca
integer x = 1499
integer y = 564
integer width = 389
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;
string				ls_cod_documento, ls_cod_fornitore, ls_numeratore, ls_sql
long				ll_anno_documento, ll_num_documento, ll_num_documento_fine, ll_ret
date				ldd_data_inizio, ldd_data_fine



ls_sql = is_sql_origine
//accodo la parte della where che è fissa
ls_sql +=  " where tes_fat_acq.cod_azienda='"+s_cs_xx.cod_azienda+"' and "+&
						"tes_fat_acq.flag_fat_confermata='S' and "+&
						"tes_fat_acq.flag_contabilita='N' and "+&
						"tes_fat_acq.cod_doc_origine is not null "

dw_selezione.accepttext()

ls_cod_documento = dw_selezione.getitemstring(1,"cod_documento")
ll_anno_documento = dw_selezione.getitemnumber(1,"anno_documento")
ls_numeratore = dw_selezione.getitemstring(1,"numeratore_documento")
ll_num_documento = dw_selezione.getitemnumber(1,"num_documento")
ll_num_documento_fine = dw_selezione.getitemnumber(1,"num_documento_fine")
ls_cod_fornitore = dw_selezione.getitemstring(1,"cod_fornitore")
ldd_data_inizio = dw_selezione.getitemdate(1,"data_inizio")
ldd_data_fine = dw_selezione.getitemdate(1,"data_fine")

if not isnull(ls_cod_documento) and ls_cod_documento<>"" then ls_sql += "and tes_fat_acq.cod_doc_origine='" + ls_cod_documento + "' "
if not isnull(ll_anno_documento) and ll_anno_documento > 0 then ls_sql += "and tes_fat_acq.anno_doc_origine=" + string(ll_anno_documento)+" "
if not isnull(ll_num_documento) and ll_num_documento > 0 then ls_sql += "and tes_fat_acq.num_doc_origine>=" + string(ll_num_documento)+" "
if not isnull(ll_num_documento_fine) and ll_num_documento_fine > 0 then ls_sql += "and tes_fat_acq.num_doc_origine<=" + string(ll_num_documento_fine) + " "
if not isnull(ls_numeratore) and ls_numeratore<>"" then ls_sql += "and tes_fat_acq.numeratore_doc_origine='" + ls_numeratore + "' "
if not isnull(ls_cod_fornitore) and ls_cod_fornitore<>"" then	ls_sql += "and tes_fat_acq.cod_fornitore='" + ls_cod_fornitore + "' "
if not isnull(ldd_data_inizio) and year(date(ldd_data_inizio))>1950 then ls_sql += "and tes_fat_acq.data_doc_origine>=date('"+string(ldd_data_inizio, "dd/mm/yyyy")+"') "
if not isnull(ldd_data_fine) and year(date(ldd_data_fine))>1950 then ls_sql += "and tes_fat_acqtes_fat_acq.data_doc_origine<=date('"+string(ldd_data_fine, "dd/mm/yyyy")+"')"


//ora piazzo l'order by
ls_sql += " " + is_sql_orderby


if tab_1.tab_da_contabilizzare.dw_lista_contabilizzazione.setsqlselect(ls_sql) = -1 then
	g_mb.error("APICE","Errore nell'impostazione della select")
	return
end if

ll_ret =  tab_1.tab_da_contabilizzare.dw_lista_contabilizzazione.retrieve()

if ll_ret<0 then
	g_mb.error("APICE","Errore durante la retrieve della select!")
	return
end if

if isnull(ll_ret) then ll_ret = 0

//ripristino il testo di default
tab_1.tab_da_contabilizzare.text = "Da Contabilizzare ("+string(ll_ret)+")"
tab_1.tab_contabilizzate.text = "Contabilizzate"
tab_1.tab_non_contabilizzate.text = "Non Contabilizzate"

tab_1.selecttab(2)




end event

type cb_annulla from commandbutton within tab_ricerca
integer x = 1088
integer y = 564
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;string ls_null
date ldd_null

setnull(ls_null)
setnull(ldd_null)
dw_selezione.setitem(dw_selezione.getrow(), "anno_documento", f_anno_esercizio())
dw_selezione.setitem(dw_selezione.getrow(), "numeratore_documento", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "data_inizio", ldd_null)
dw_selezione.setitem(dw_selezione.getrow(), "data_fine",   ldd_null)
dw_selezione.setitem(dw_selezione.getrow(), "num_documento", 1)
dw_selezione.setitem(dw_selezione.getrow(), "num_documento_fine", 999999)
dw_selezione.setitem(dw_selezione.getrow(), "cod_fornitore", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "cod_documento", ls_null)

//cb_cerca.postevent("clicked")

end event

type dw_selezione from uo_cs_xx_dw within tab_ricerca
integer x = 59
integer y = 36
integer width = 2971
integer height = 480
integer taborder = 10
boolean bringtotop = true
string dataobject = "d_sel_contabilizzazione_fat_acq"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore")
end choose
end event

event doubleclicked;

//lasciare ancestor script disattivato
return
end event

type tab_da_contabilizzare from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 4265
integer height = 2072
long backcolor = 12632256
string text = "Da Contabilizzare"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
string picturename = "ArrangeTables5!"
long picturemaskcolor = 536870912
cb_contabilizza cb_contabilizza
dw_lista_contabilizzazione dw_lista_contabilizzazione
end type

on tab_da_contabilizzare.create
this.cb_contabilizza=create cb_contabilizza
this.dw_lista_contabilizzazione=create dw_lista_contabilizzazione
this.Control[]={this.cb_contabilizza,&
this.dw_lista_contabilizzazione}
end on

on tab_da_contabilizzare.destroy
destroy(this.cb_contabilizza)
destroy(this.dw_lista_contabilizzazione)
end on

type cb_contabilizza from commandbutton within tab_da_contabilizzare
integer x = 41
integer y = 36
integer width = 389
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Contabilizza"
end type

event clicked;string					ls_messaggio, ls_errore,ls_cod_documento, ls_numeratore_documento, ls_motivazione, ls_str
long					ll_anno_reg_fat_acq, ll_num_reg_fat_acq, ll_rows, ll_i, ll_anno_documento, &
						ll_num_documento, ll_anno_registrazione, ll_num_registrazione, ll_riga, ll_return, ll_cont
						
datetime				ldt_data_fattura

long					ll_count_error, ll_count_warning, ll_ok

uo_log_sistema luo_log_sistema


setnull(ls_messaggio)
setnull(ls_errore)
tab_1.tab_contabilizzate.mle_lista_fatt_contabilizzate.text = "ELENCO FATTURE CONTABILIZZATE CON SUCCESSO" + "~r~n"
tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text = "ELENCO FATTURE NON CONTABILIZZATE" + "~r~n"
ll_rows = dw_lista_contabilizzazione.rowcount()

ll_count_error = 0
ll_count_warning = 0
ll_ok = 0

//ripristino il testo di default
tab_1.tab_contabilizzate.text = "Contabilizzate"
tab_1.tab_non_contabilizzate.text = "Non Contabilizzate"

luo_log_sistema = create uo_log_sistema

if ll_rows < 1 then
	g_mb.warning("APICE","Nessuna fattura da contabilizzare")
	return
else
	setpointer(Hourglass!)
	
	for ll_i = 1 to ll_rows
		Yield()
		
		ll_anno_reg_fat_acq      = dw_lista_contabilizzazione.getitemnumber(ll_i, "anno_registrazione")
		ll_num_reg_fat_acq       = dw_lista_contabilizzazione.getitemnumber(ll_i, "num_registrazione")
		ls_numeratore_documento  = dw_lista_contabilizzazione.getitemstring(ll_i, "numeratore_doc_origine")
		ls_cod_documento         = dw_lista_contabilizzazione.getitemstring(ll_i, "cod_doc_origine")
		ll_anno_documento        = dw_lista_contabilizzazione.getitemnumber(ll_i, "anno_doc_origine")
		ll_num_documento         = dw_lista_contabilizzazione.getitemnumber(ll_i, "num_doc_origine")
		ldt_data_fattura         = dw_lista_contabilizzazione.getitemdatetime(ll_i, "data_doc_origine")
		
		pcca.mdi_frame.setmicrohelp(	"Contabilizzazione Fattura "+ &
												ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
												"/" + string(ll_num_documento,"###000")+"  ("+string(ll_i)+" di "+string(ll_rows)+") in corso...")

		// controllo se c'è iva, contropartite e scadenze
		
		select 	count(*) 
		into		:ll_cont
		from 		iva_fat_acq
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_reg_fat_acq and
					num_registrazione = :ll_num_reg_fat_acq;
		
		if ll_cont <= 0 then
			ls_str = "FATTURA " + ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
						"/" + string(ll_num_documento,"###000") + "  (int " + string(ll_anno_reg_fat_acq) + "/" + string(ll_num_reg_fat_acq) +") ~r~n" + &
						"La fattura è mancante dei dati IVA; si consiglia di rieseguire il calcolo della fattura.~r~n"
			
			ll_count_error += 1
			tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text = tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text + ls_str
			luo_log_sistema.uof_write_log_sistema_not_sqlca("CONT_FAT_ACQ", ls_str)
		end if
		
		select 	count(*) 
		into		:ll_cont
		from 		cont_fat_acq
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_reg_fat_acq and
					num_registrazione = :ll_num_reg_fat_acq;
		
		if ll_cont <= 0 then
			ls_str = "FATTURA " + ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
						"/" + string(ll_num_documento,"###000") + "  (int " + string(ll_anno_reg_fat_acq) + "/" + string(ll_num_reg_fat_acq) +") ~r~n" + &
						"La fattura è mancante delle CONTROPARTITE; si consiglia di rieseguire il calcolo della fattura.~r~n"
			
			ll_count_error += 1
			tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text = tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text + ls_str
			luo_log_sistema.uof_write_log_sistema_not_sqlca("CONT_FAT_ACQ", ls_str)
		end if
		
		select 	count(*) 
		into		:ll_cont
		from 		scad_fat_acq
		where	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_reg_fat_acq and
					num_registrazione = :ll_num_reg_fat_acq;
		
		if ll_cont <= 0 then
			ls_str = "FATTURA " + ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
						"/" + string(ll_num_documento,"###000") + "  (int " + string(ll_anno_reg_fat_acq) + "/" + string(ll_num_reg_fat_acq) +") ~r~n" + &
						"La fattura è mancante delle SCADENZE; si consiglia di rieseguire il calcolo della fattura.~r~n"
			
			ll_count_error += 1
			tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text = tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text + ls_str
			luo_log_sistema.uof_write_log_sistema_not_sqlca("CONT_FAT_ACQ", ls_str)
		end if
		

		ls_messaggio = ""
		ll_return = wf_contabilizza(ll_anno_reg_fat_acq, ll_num_reg_fat_acq, ls_messaggio, ls_errore)

		luo_log_sistema = create uo_log_sistema
		
		
		if ll_return = -8 then
			//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
			//errore specifico -8
			ls_str = "FATTURA " + ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
						"/" + string(ll_num_documento,"###000") + "  (int " + string(ll_anno_reg_fat_acq) + "/" + string(ll_num_reg_fat_acq) +") ~r~n" + &
						"Il protocollo risulta essere già esistente nel registro IVA~r~n"
			
			ll_count_error += 1
			
			tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text = tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text + ls_str
			
			luo_log_sistema.uof_write_log_sistema_not_sqlca("CONT_FAT_ACQ", ls_str)
			
			ROLLBACK using sqlca;
			ROLLBACK using sqlci;
			
		elseif ll_return < 0 then
			//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
			//errore -1
			ls_str = "FATTURA " + ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
						"/" + string(ll_num_documento,"###000") + "  (int " + string(ll_anno_reg_fat_acq) + "/" + string(ll_num_reg_fat_acq) +") ~r~n" + &
			         "Messaggio = " + ls_messaggio + "~r~n" + "Errore = " + ls_errore +  "~r~n"
			
			ll_count_error += 1
			
			luo_log_sistema.uof_write_log_sistema_not_sqlca("CONT_FAT_ACQ", ls_str)
			
			tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text = tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text + ls_str
			ROLLBACK using sqlca;
			ROLLBACK using sqlci;
			
		else
			//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
			//fin qui tutto a posto ma se non riesce a mettere SI al flag contabilizzata annulla la contabilizzazione della fattura
			update tes_fat_acq
			set    flag_contabilita = 'S'
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_reg_fat_acq and
			       num_registrazione = :ll_num_reg_fat_acq;
					 
			if sqlca.sqlcode <> 0 then
				ls_str = "FATTURA " + ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
							"/" + string(ll_num_documento,"###000") + "  (int " + string(ll_anno_reg_fat_acq) + "/" + string(ll_num_reg_fat_acq) +") ~r~n" + &
							"Errore durante aggiornamento segnale fattura contabilizzata" +  + "~r~n" + "Errore = " + sqlca.sqlerrtext +  "~r~n"
				
				ll_count_error += 1
				
				luo_log_sistema.uof_write_log_sistema_not_sqlca("CONT_FAT_ACQ", ls_str)
			
				tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text = tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text + ls_str
				ROLLBACK using sqlca;
				ROLLBACK using sqlci;
				
			else
				//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
				//OK contabilizzazione
				ls_str = "~r~n~r~n"
				ls_str += "-------------------------------------------------------------------------------------------------------------------------~r~n"
				ls_str += "FATTURA " + ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
							"/" + string(ll_num_documento,"###000") + "  (int " + string(ll_anno_reg_fat_acq) + "/" + string(ll_num_reg_fat_acq) +") - REGISTRAZIONE N. " + string(ll_return) +" ~r~n"
				
				if is_mov_intra<>"" and not isnull(is_mov_intra) then
					ls_str += is_mov_intra+"~r~n"
				end if
				is_mov_intra = ""
				
				tab_1.tab_contabilizzate.mle_lista_fatt_contabilizzate.text = tab_1.tab_contabilizzate.mle_lista_fatt_contabilizzate.text + ls_str
				
				if not isnull(ls_messaggio) and len(ls_messaggio) > 0 then
					//eventuale messaggio informativo in "ls_messaggio"
					
					ll_count_warning += 1
					
					tab_1.tab_contabilizzate.mle_lista_fatt_contabilizzate.text = tab_1.tab_contabilizzate.mle_lista_fatt_contabilizzate.text + "~r~n" + "*** ATTENZIONE:" + ls_messaggio
				end if
				
				luo_log_sistema.uof_write_log_sistema_not_sqlca("CONT_FAT_ACQ", ls_str)
			
				ll_ok += 1
				
				COMMIT using sqlca;
				COMMIT using sqlci;
				
			end if
		end if
		
		destroy luo_log_sistema
		
		tab_1.tab_non_contabilizzate.text = "Non Contabilizzate ("+string(ll_count_error)+" su "+string(ll_rows)+")"
		tab_1.tab_contabilizzate.text = "Contabilizzate ("+string(ll_ok)+" su "+string(ll_rows)+" e "+string(ll_count_warning)+" warning)"
		
	next
	
	setpointer(Arrow!)
	pcca.mdi_frame.setmicrohelp("Pronto!")
	
end if

if ll_count_error>0 then
	g_mb.error("Contabilizzazione Fatture Acquisto", "Procedura di contabilizzazione fatture terminata CON ERRORI!")
	tab_1.selecttab(4)
	
elseif ll_count_warning>0 then
	g_mb.warning("Contabilizzazione Fatture Acquisto", "Procedura di contabilizzazione fatture terminata con alcuni WARNING!")
	tab_1.selecttab(3)
	
else
	g_mb.success("Contabilizzazione Fatture Acquisto", "Procedura di contabilizzazione fatture terminata CON SUCCESSO!")
	tab_1.selecttab(3)
	
end if


dw_lista_contabilizzazione.reset()
tab_1.tab_da_contabilizzare.text = "Da Contabilizzare"

end event

type dw_lista_contabilizzazione from uo_cs_xx_dw within tab_da_contabilizzare
integer x = 41
integer y = 156
integer width = 4206
integer height = 1892
integer taborder = 80
string dataobject = "d_lista_contabilizzazione_fat_acq"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if

end event

type tab_contabilizzate from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 4265
integer height = 2072
long backcolor = 12632256
string text = "Contabilizzate"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
string picturename = "FormatDollar!"
long picturemaskcolor = 12632256
cb_stampa_contabilizzate cb_stampa_contabilizzate
mle_lista_fatt_contabilizzate mle_lista_fatt_contabilizzate
end type

on tab_contabilizzate.create
this.cb_stampa_contabilizzate=create cb_stampa_contabilizzate
this.mle_lista_fatt_contabilizzate=create mle_lista_fatt_contabilizzate
this.Control[]={this.cb_stampa_contabilizzate,&
this.mle_lista_fatt_contabilizzate}
end on

on tab_contabilizzate.destroy
destroy(this.cb_stampa_contabilizzate)
destroy(this.mle_lista_fatt_contabilizzate)
end on

type cb_stampa_contabilizzate from commandbutton within tab_contabilizzate
integer x = 41
integer y = 36
integer width = 389
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;long li_prt

li_prt = PrintOpen("Lista fatture contabilizzate")

Print(li_prt, "Lista fatture contabilizzate  - "  &
	+ String(Today(), "dd/mm/yyyy")  &
	+ " - "  &
	+ String(Now(), "HH:MM:SS"))
Print(li_prt, " ")
Print(li_prt, mle_lista_fatt_contabilizzate.text)

PrintClose(li_prt)
end event

type mle_lista_fatt_contabilizzate from multilineedit within tab_contabilizzate
integer x = 41
integer y = 156
integer width = 4206
integer height = 1892
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
boolean border = false
boolean hscrollbar = true
boolean vscrollbar = true
boolean autohscroll = true
boolean autovscroll = true
boolean displayonly = true
end type

type tab_non_contabilizzate from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 4265
integer height = 2072
long backcolor = 12632256
string text = "Non Contabilizzate"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
string picturename = "Error!"
long picturemaskcolor = 536870912
cb_stampa_non_contabilizzate cb_stampa_non_contabilizzate
mle_lista_fatt_non_contabilizzate mle_lista_fatt_non_contabilizzate
end type

on tab_non_contabilizzate.create
this.cb_stampa_non_contabilizzate=create cb_stampa_non_contabilizzate
this.mle_lista_fatt_non_contabilizzate=create mle_lista_fatt_non_contabilizzate
this.Control[]={this.cb_stampa_non_contabilizzate,&
this.mle_lista_fatt_non_contabilizzate}
end on

on tab_non_contabilizzate.destroy
destroy(this.cb_stampa_non_contabilizzate)
destroy(this.mle_lista_fatt_non_contabilizzate)
end on

type cb_stampa_non_contabilizzate from commandbutton within tab_non_contabilizzate
integer x = 41
integer y = 36
integer width = 389
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;long li_prt

li_prt = PrintOpen("Lista fatture non contabilizzate")

Print(li_prt, "Lista fatture non contabilizzate  - "  &
	+ String(Today(), "dd/mm/yyyy")  &
	+ " - "  &
	+ String(Now(), "HH:MM:SS"))
Print(li_prt, " ")
Print(li_prt, mle_lista_fatt_non_contabilizzate.text)

PrintClose(li_prt)
end event

type mle_lista_fatt_non_contabilizzate from multilineedit within tab_non_contabilizzate
integer x = 41
integer y = 156
integer width = 4206
integer height = 1892
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
boolean border = false
boolean hscrollbar = true
boolean vscrollbar = true
boolean autohscroll = true
boolean autovscroll = true
boolean displayonly = true
end type

