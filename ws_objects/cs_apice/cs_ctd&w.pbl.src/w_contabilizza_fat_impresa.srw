﻿$PBExportHeader$w_contabilizza_fat_impresa.srw
$PBExportComments$Finestra Contabilizzazione Fatture di Vendita
forward
global type w_contabilizza_fat_impresa from w_cs_xx_principale
end type
type tab_1 from tab within w_contabilizza_fat_impresa
end type
type tab_ricerca from userobject within tab_1
end type
type cb_ricerca from commandbutton within tab_ricerca
end type
type dw_selezione from uo_std_dw within tab_ricerca
end type
type tab_ricerca from userobject within tab_1
cb_ricerca cb_ricerca
dw_selezione dw_selezione
end type
type tab_da_contabilizzare from userobject within tab_1
end type
type cb_contabilizza from commandbutton within tab_da_contabilizzare
end type
type dw_lista_contabilizzazione from uo_std_dw within tab_da_contabilizzare
end type
type tab_da_contabilizzare from userobject within tab_1
cb_contabilizza cb_contabilizza
dw_lista_contabilizzazione dw_lista_contabilizzazione
end type
type tab_contabilizzate from userobject within tab_1
end type
type cb_stampa_2 from commandbutton within tab_contabilizzate
end type
type mle_lista_fatt_contabilizzate from multilineedit within tab_contabilizzate
end type
type tab_contabilizzate from userobject within tab_1
cb_stampa_2 cb_stampa_2
mle_lista_fatt_contabilizzate mle_lista_fatt_contabilizzate
end type
type tab_non_contabilizzate from userobject within tab_1
end type
type cb_stampa_1 from commandbutton within tab_non_contabilizzate
end type
type mle_lista_fatt_non_contabilizzate from multilineedit within tab_non_contabilizzate
end type
type tab_non_contabilizzate from userobject within tab_1
cb_stampa_1 cb_stampa_1
mle_lista_fatt_non_contabilizzate mle_lista_fatt_non_contabilizzate
end type
type tab_1 from tab within w_contabilizza_fat_impresa
tab_ricerca tab_ricerca
tab_da_contabilizzare tab_da_contabilizzare
tab_contabilizzate tab_contabilizzate
tab_non_contabilizzate tab_non_contabilizzate
end type
end forward

global type w_contabilizza_fat_impresa from w_cs_xx_principale
integer width = 4361
integer height = 2340
string title = "Contabilizzazione Fatture di VENDITA"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
tab_1 tab_1
end type
global w_contabilizza_fat_impresa w_contabilizza_fat_impresa

type variables
string			 is_mov_intra = ""
end variables

forward prototypes
public function long wf_contabilizza (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio, ref string fs_errore)
public function string wf_tipi_det_ven_trasporti ()
end prototypes

public function long wf_contabilizza (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio, ref string fs_errore);long ll_return, ll_id_registrazione, ll_anno_registrazione, ll_num_registrazione, ll_num_reg_pd
uo_impresa lu_impresa

lu_impresa = CREATE uo_impresa

is_mov_intra = ""

lu_impresa.is_mov_intra = ""

lu_impresa.uof_set_tipo_det_trasporto()

ll_return = lu_impresa.uof_contabilizza_fat_ven(fl_anno_registrazione, fl_num_registrazione, ll_num_reg_pd)
if ll_return = 0 then
	
	if lu_impresa.is_mov_intra<>"" and not isnull(lu_impresa.is_mov_intra) then
		is_mov_intra = lu_impresa.is_mov_intra
	end if
	
	destroy lu_impresa
	return ll_num_reg_pd
else
	fs_messaggio = lu_impresa.i_messaggio
	fs_errore    = string(lu_impresa.i_db_errore)
	destroy lu_impresa
	return -1
end if
return 0
end function

public function string wf_tipi_det_ven_trasporti ();string				ls_DTV, as_array[]
long				ll_index


guo_functions.uof_get_parametro_azienda("DTV", ls_DTV)

if isnull(ls_DTV) then ls_DTV = ""


g_str.explode(ls_DTV, ",", as_array[])

ls_DTV = ""

for ll_index=1 to upperbound(as_array[])
	if ll_index > 1 then ls_DTV += ","
	ls_DTV += "'" + as_array[ll_index] + "'"
next

return ls_DTV
end function

on w_contabilizza_fat_impresa.create
int iCurrent
call super::create
this.tab_1=create tab_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.tab_1
end on

on w_contabilizza_fat_impresa.destroy
call super::destroy
destroy(this.tab_1)
end on

event pc_setwindow;call super::pc_setwindow;

set_w_options(c_noenablepopup)
save_on_close(c_socnosave)
tab_1.tab_ricerca.dw_selezione.insertrow(0)

end event

event pc_setddlb;f_po_loaddddw_dw(tab_1.tab_ricerca.dw_selezione, &
                 "cod_documento", &
                 sqlca, &
                 "tab_documenti", &
                 "cod_documento", &
                 "des_documento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(tab_1.tab_ricerca.dw_selezione, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

type tab_1 from tab within w_contabilizza_fat_impresa
event create ( )
event destroy ( )
integer x = 32
integer y = 36
integer width = 4302
integer height = 2200
integer taborder = 10
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
boolean raggedright = true
boolean focusonbuttondown = true
boolean boldselectedtext = true
integer selectedtab = 1
tab_ricerca tab_ricerca
tab_da_contabilizzare tab_da_contabilizzare
tab_contabilizzate tab_contabilizzate
tab_non_contabilizzate tab_non_contabilizzate
end type

on tab_1.create
this.tab_ricerca=create tab_ricerca
this.tab_da_contabilizzare=create tab_da_contabilizzare
this.tab_contabilizzate=create tab_contabilizzate
this.tab_non_contabilizzate=create tab_non_contabilizzate
this.Control[]={this.tab_ricerca,&
this.tab_da_contabilizzare,&
this.tab_contabilizzate,&
this.tab_non_contabilizzate}
end on

on tab_1.destroy
destroy(this.tab_ricerca)
destroy(this.tab_da_contabilizzare)
destroy(this.tab_contabilizzate)
destroy(this.tab_non_contabilizzate)
end on

type tab_ricerca from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 4265
integer height = 2072
long backcolor = 12632256
string text = "Ricerca"
long tabtextcolor = 33554432
long tabbackcolor = 16777215
string picturename = "Search!"
long picturemaskcolor = 12632256
cb_ricerca cb_ricerca
dw_selezione dw_selezione
end type

on tab_ricerca.create
this.cb_ricerca=create cb_ricerca
this.dw_selezione=create dw_selezione
this.Control[]={this.cb_ricerca,&
this.dw_selezione}
end on

on tab_ricerca.destroy
destroy(this.cb_ricerca)
destroy(this.dw_selezione)
end on

type cb_ricerca from commandbutton within tab_ricerca
integer x = 1403
integer y = 928
integer width = 366
integer height = 80
integer taborder = 70
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ricerca"
end type

event clicked;string ls_sql, ls_cod_documento, ls_numeratore_documento, ls_cod_cliente, ls_cod_deposito
long 	ll_anno_documento, ll_num_documento_inizio, ll_num_documento_fine, ll_tot
date 	ld_data_inizio, ld_data_fine, ld_data_decorrenza_inizio, ld_data_decorrenza_fine

dw_selezione.accepttext()

ls_sql = 	"select cod_documento, numeratore_documento, anno_documento, num_documento, data_fattura, cod_tipo_fat_ven, anno_registrazione, num_registrazione, data_registrazione, cod_cliente  FROM tes_fat_ven  WHERE ( tes_fat_ven.cod_azienda = '"+ s_cs_xx.cod_azienda +"' ) AND  " + &
        		" ( cod_documento is not null ) AND  ( flag_calcolo = 'S' ) AND  ( flag_contabilita = 'N' ) AND  ( flag_movimenti = 'S' ) AND  ( cod_tipo_fat_ven in ( SELECT cod_tipo_fat_ven FROM tab_tipi_fat_ven WHERE ( cod_azienda = '" + s_cs_xx.cod_azienda + "' ) AND ( flag_tipo_fat_ven not in ('P', 'F') ) ))   "

ls_cod_documento = dw_selezione.getitemstring(1, "cod_documento")
if not isnull(ls_cod_documento) then
	ls_sql += " and cod_documento = '" + ls_cod_documento + "' "
end if

ls_numeratore_documento = dw_selezione.getitemstring(1, "numeratore_documento")
if not isnull(ls_numeratore_documento) then
	ls_sql += " and numeratore_documento = '" + ls_numeratore_documento + "' "
end if

ls_cod_cliente = dw_selezione.getitemstring(1, "cod_cliente")
if not isnull(ls_cod_cliente) then
	ls_sql += " and cod_cliente = '" + ls_cod_cliente + "' "
end if

ls_cod_deposito =dw_selezione.getitemstring(1, "cod_deposito")
if not isnull(ls_cod_deposito) then
	ls_sql += " and cod_deposito = '" + ls_cod_deposito + "' "
end if

ll_anno_documento = dw_selezione.getitemnumber(1, "anno_documento")
if not isnull(ll_anno_documento) and ll_anno_documento > 0  then
	ls_sql += " and anno_documento = " + string( ll_anno_documento )
end if

ll_num_documento_inizio = dw_selezione.getitemnumber(1, "num_documento")
if not isnull(ll_num_documento_inizio) and ll_num_documento_inizio > 0 then
	ls_sql += " and num_documento >=  " +  string(ll_num_documento_inizio )
end if

ll_num_documento_fine = dw_selezione.getitemnumber(1, "num_documento_fine")
if not isnull(ll_num_documento_fine) and ll_num_documento_fine > 0 then
	ls_sql += " and num_documento <= " + string( ll_num_documento_fine ) 
end if

ld_data_inizio = dw_selezione.getitemdate(1, "data_inizio")
if not isnull(ld_data_inizio) and ld_data_inizio > date("01/01/1900") then
	ls_sql += " and data_fattura >=  '" + string( ld_data_inizio , s_cs_xx.db_funzioni.formato_data) + "' "
end if

ld_data_fine = dw_selezione.getitemdate(1, "data_fine")
if not isnull(ld_data_fine) and ld_data_fine > date("01/01/1900") then
	ls_sql += " and data_fattura <=  '" + string( ld_data_fine , s_cs_xx.db_funzioni.formato_data) + "' "
end if

ld_data_decorrenza_inizio = dw_selezione.getitemdate(1, "data_decorrenza_inizio")
if not isnull(ld_data_decorrenza_inizio) and ld_data_decorrenza_inizio > date("01/01/1900") then
	ls_sql += " and data_decorrenza_pagamento >=  '" + string( ld_data_decorrenza_inizio , s_cs_xx.db_funzioni.formato_data) + "' "
end if

ld_data_decorrenza_fine = dw_selezione.getitemdate(1, "data_decorrenza_fine")
if not isnull(ld_data_decorrenza_fine) and ld_data_decorrenza_fine > date("01/01/1900") then
	ls_sql += " and data_decorrenza_pagamento <=  '" + string( ld_data_decorrenza_fine , s_cs_xx.db_funzioni.formato_data) + "' "
end if

ls_sql += " ORDER BY tes_fat_ven.cod_documento ASC,   tes_fat_ven.anno_documento ASC,   tes_fat_ven.numeratore_documento ASC,   tes_fat_ven.num_documento ASC,   tes_fat_ven.data_fattura ASC "

tab_1.tab_da_contabilizzare.dw_lista_contabilizzazione.settransobject(sqlca)
tab_1.tab_da_contabilizzare.dw_lista_contabilizzazione.setsqlselect(ls_sql)
ll_tot = tab_1.tab_da_contabilizzare.dw_lista_contabilizzazione.retrieve()

if isnull(ll_tot) then ll_tot = 0

//ripristino il testo di default
tab_1.tab_da_contabilizzare.text = "Da Contabilizzare ("+string(ll_tot)+")"
tab_1.tab_contabilizzate.text = "Contabilizzate"
tab_1.tab_non_contabilizzate.text = "Non Contabilizzate"

tab_1.selecttab(2)



end event

type dw_selezione from uo_std_dw within tab_ricerca
integer x = 64
integer y = 48
integer width = 3141
integer height = 820
integer taborder = 60
string dataobject = "d_sel_contabilizzazione"
boolean border = false
borderstyle borderstyle = stylebox!
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
end choose
end event

type tab_da_contabilizzare from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 4265
integer height = 2072
long backcolor = 12632256
string text = "Da Contabilizzare"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
string picturename = "ArrangeTables5!"
long picturemaskcolor = 536870912
cb_contabilizza cb_contabilizza
dw_lista_contabilizzazione dw_lista_contabilizzazione
end type

on tab_da_contabilizzare.create
this.cb_contabilizza=create cb_contabilizza
this.dw_lista_contabilizzazione=create dw_lista_contabilizzazione
this.Control[]={this.cb_contabilizza,&
this.dw_lista_contabilizzazione}
end on

on tab_da_contabilizzare.destroy
destroy(this.cb_contabilizza)
destroy(this.dw_lista_contabilizzazione)
end on

type cb_contabilizza from commandbutton within tab_da_contabilizzare
integer x = 41
integer y = 36
integer width = 389
integer height = 80
integer taborder = 70
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Contabilizza"
end type

event clicked;string				ls_messaggio, ls_errore,ls_cod_documento, ls_numeratore_documento, ls_motivazione, ls_str

long				ll_anno_reg_fat_ven, ll_num_reg_fat_ven, ll_rows, ll_i, ll_anno_documento, &
					ll_num_documento, ll_anno_registrazione, ll_num_registrazione, ll_riga, ll_return
					
datetime			ldt_data_fattura

long				ll_count_error, ll_count_warning, ll_ok

uo_log_sistema		luo_log_sistema



setnull(ls_messaggio)
setnull(ls_errore)
tab_1.tab_contabilizzate.mle_lista_fatt_contabilizzate.text = "ELENCO FATTURE CONTABILIZZATE CON SUCCESSO" + "~r~n"
tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text = "ELENCO FATTURE NON CONTABILIZZATE" + "~r~n"

//ripristino il testo di default
tab_1.tab_contabilizzate.text = "Contabilizzate"
tab_1.tab_non_contabilizzate.text = "Non Contabilizzate"

ll_count_error = 0
ll_count_warning = 0
ll_ok = 0

ll_rows = dw_lista_contabilizzazione.rowcount()
if ll_rows < 1 then
	g_mb.warning("APICE","Nessuna fattura da contabilizzare")
	return
else
	setpointer(Hourglass!)
	
	for ll_i = 1 to ll_rows
		Yield()
		
		ll_anno_reg_fat_ven      = dw_lista_contabilizzazione.getitemnumber(ll_i, "anno_registrazione")
		ll_num_reg_fat_ven       = dw_lista_contabilizzazione.getitemnumber(ll_i, "num_registrazione")
		ls_numeratore_documento  = dw_lista_contabilizzazione.getitemstring(ll_i, "numeratore_documento")
		ls_cod_documento         = dw_lista_contabilizzazione.getitemstring(ll_i, "cod_documento")
		ll_anno_documento        = dw_lista_contabilizzazione.getitemnumber(ll_i, "anno_documento")
		ll_num_documento         = dw_lista_contabilizzazione.getitemnumber(ll_i, "num_documento")
		ldt_data_fattura         = dw_lista_contabilizzazione.getitemdatetime(ll_i, "data_fattura")

		ls_messaggio = ""
		pcca.mdi_frame.setmicrohelp(	"Contabilizzazione Fattura "+ &
												ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
												"/" + string(ll_num_documento,"###000")+"  ("+string(ll_i)+" di "+string(ll_rows)+") in corso...")
		ll_return = wf_contabilizza(ll_anno_reg_fat_ven, ll_num_reg_fat_ven, ls_messaggio, ls_errore)
		
		//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
		//errore specifico -8
		
		luo_log_sistema = create uo_log_sistema
		
		if ll_return = -8 then
			ls_str = "FATTURA " + ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
						"/" + string(ll_num_documento,"###000") + "  (int " + string(ll_anno_reg_fat_ven) + "/" + string(ll_num_reg_fat_ven) +") ~r~n" + &
						"Il protocollo risulta essere già esistente nel registro IVA~r~n"
			
			ll_count_error += 1
			
			tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text = tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text + ls_str
			
			luo_log_sistema.uof_write_log_sistema_not_sqlca("CONT_FAT_VEN", ls_str)			
			
			ROLLBACK using sqlca;
			ROLLBACK using sqlci;
			
		//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
		//errore -1
		elseif ll_return < 0 then
			ls_str = "FATTURA " + ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
						"/" + string(ll_num_documento,"###000") + "  (int " + string(ll_anno_reg_fat_ven) + "/" + string(ll_num_reg_fat_ven) +") ~r~n" + &
						"Messaggio = " + ls_messaggio + "~r~n" + "Errore = " + ls_errore +  "~r~n"
			
			ll_count_error += 1
			
			tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text = tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text + ls_str
			luo_log_sistema.uof_write_log_sistema_not_sqlca("CONT_FAT_VEN", ls_str)			
			
			ROLLBACK using sqlca;
			ROLLBACK using sqlci;
			
		else
			//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
			//fin qui tutto a posto ma se non riesce a mettere SI al flag contabilizzata annulla la contabilizzazione della fattura
			update tes_fat_ven
			set    flag_contabilita = 'S'
			where  tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and
					 tes_fat_ven.anno_registrazione = :ll_anno_reg_fat_ven and
					 tes_fat_ven.num_registrazione = :ll_num_reg_fat_ven;
					 
			if sqlca.sqlcode <> 0 then
				ls_str = "FATTURA " + ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
				"/" + string(ll_num_documento,"###000") + "  (int " + string(ll_anno_reg_fat_ven) + "/" + string(ll_num_reg_fat_ven) +") ~r~n" + &
							"Errore durante aggiornamento segnale fattura contabilizzata" +  + "~r~n" + "Errore = " + sqlca.sqlerrtext +  "~r~n"
				
				ll_count_error += 1
				
				tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text = tab_1.tab_non_contabilizzate.mle_lista_fatt_non_contabilizzate.text + ls_str
				
				luo_log_sistema.uof_write_log_sistema_not_sqlca("CONT_FAT_VEN", ls_str)			
			
				ROLLBACK using sqlca;
				ROLLBACK using sqlci;
				
			else
				//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
				//OK contabilizzazione
				ls_str = "~r~n~r~n"
				ls_str += "-------------------------------------------------------------------------------------------------------------------------~r~n"
				ls_str += "FATTURA " + ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) &
							+ "/" + string(ll_num_documento,"###000") + "  (int " + string(ll_anno_reg_fat_ven) + "/" + string(ll_num_reg_fat_ven) +") - REGISTRAZIONE N. " + string(ll_return) +" ~r~n"
				
				if is_mov_intra<>"" and not isnull(is_mov_intra) then
					ls_str += is_mov_intra+"~r~n"
				end if
				is_mov_intra = ""
				
				tab_1.tab_contabilizzate.mle_lista_fatt_contabilizzate.text = tab_1.tab_contabilizzate.mle_lista_fatt_contabilizzate.text + ls_str
				
				luo_log_sistema.uof_write_log_sistema_not_sqlca("CONT_FAT_VEN", ls_str)			
			
				if not isnull(ls_messaggio) and len(ls_messaggio) > 0 then
					//eventuale messaggio informativo in "ls_messaggio"
					ll_count_warning += 1
					tab_1.tab_contabilizzate.mle_lista_fatt_contabilizzate.text = tab_1.tab_contabilizzate.mle_lista_fatt_contabilizzate.text + "~r~n" + "*** ATTENZIONE:" + ls_messaggio
				end if
				
				ll_ok += 1
				
				COMMIT using sqlca;
				COMMIT using sqlci;
				
			end if
		end if
		
		destroy luo_log_sistema
		
		tab_1.tab_non_contabilizzate.text = "Non Contabilizzate ("+string(ll_count_error)+" su "+string(ll_rows)+")"
		tab_1.tab_contabilizzate.text = "Contabilizzate ("+string(ll_ok)+" su "+string(ll_rows)+" e "+string(ll_count_warning)+" warning)"
		
	next
	
	setpointer(Arrow!)
	pcca.mdi_frame.setmicrohelp("Pronto!")
end if

if ll_count_error>0 then
	g_mb.error("Contabilizzazione Fatture Vendita", "Procedura di contabilizzazione fatture terminata CON ERRORI!")
	tab_1.selecttab(4)
	
elseif ll_count_warning>0 then
	g_mb.warning("Contabilizzazione Fatture Vendita", "Procedura di contabilizzazione fatture terminata con alcuni WARNING!")
	tab_1.selecttab(3)
	
else
	g_mb.success("Contabilizzazione Fatture Vendita", "Procedura di contabilizzazione fatture terminata CON SUCCESSO!")
	tab_1.selecttab(3)
	
end if

dw_lista_contabilizzazione.reset()
tab_1.tab_da_contabilizzare.text = "Da Contabilizzare"


end event

type dw_lista_contabilizzazione from uo_std_dw within tab_da_contabilizzare
integer x = 41
integer y = 156
integer width = 4206
integer height = 1892
integer taborder = 30
string dataobject = "d_lista_contabilizzazione"
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
borderstyle borderstyle = stylebox!
end type

type tab_contabilizzate from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 4265
integer height = 2072
long backcolor = 12632256
string text = "Contabilizzate"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
string picturename = "FormatDollar!"
long picturemaskcolor = 12632256
cb_stampa_2 cb_stampa_2
mle_lista_fatt_contabilizzate mle_lista_fatt_contabilizzate
end type

on tab_contabilizzate.create
this.cb_stampa_2=create cb_stampa_2
this.mle_lista_fatt_contabilizzate=create mle_lista_fatt_contabilizzate
this.Control[]={this.cb_stampa_2,&
this.mle_lista_fatt_contabilizzate}
end on

on tab_contabilizzate.destroy
destroy(this.cb_stampa_2)
destroy(this.mle_lista_fatt_contabilizzate)
end on

type cb_stampa_2 from commandbutton within tab_contabilizzate
integer x = 41
integer y = 36
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;long li_prt

li_prt = PrintOpen("Lista fatture contabilizzate")

Print(li_prt, "Lista fatture contabilizzate  - "  &
	+ String(Today(), "dd/mm/yyyy")  &
	+ " - "  &
	+ String(Now(), "HH:MM:SS"))
Print(li_prt, " ")
Print(li_prt, mle_lista_fatt_contabilizzate.text)

PrintClose(li_prt)
end event

type mle_lista_fatt_contabilizzate from multilineedit within tab_contabilizzate
integer x = 41
integer y = 156
integer width = 4215
integer height = 1892
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
boolean border = false
boolean hscrollbar = true
boolean vscrollbar = true
boolean autohscroll = true
boolean autovscroll = true
boolean displayonly = true
end type

type tab_non_contabilizzate from userobject within tab_1
event create ( )
event destroy ( )
integer x = 18
integer y = 112
integer width = 4265
integer height = 2072
long backcolor = 12632256
string text = "Non Contabilizzate"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
string picturename = "Error!"
long picturemaskcolor = 12632256
cb_stampa_1 cb_stampa_1
mle_lista_fatt_non_contabilizzate mle_lista_fatt_non_contabilizzate
end type

on tab_non_contabilizzate.create
this.cb_stampa_1=create cb_stampa_1
this.mle_lista_fatt_non_contabilizzate=create mle_lista_fatt_non_contabilizzate
this.Control[]={this.cb_stampa_1,&
this.mle_lista_fatt_non_contabilizzate}
end on

on tab_non_contabilizzate.destroy
destroy(this.cb_stampa_1)
destroy(this.mle_lista_fatt_non_contabilizzate)
end on

type cb_stampa_1 from commandbutton within tab_non_contabilizzate
integer x = 41
integer y = 36
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;long li_prt

li_prt = PrintOpen("Lista fatture non contabilizzate")

Print(li_prt, "Lista fatture non contabilizzate  - "  &
	+ String(Today(), "dd/mm/yyyy")  &
	+ " - "  &
	+ String(Now(), "HH:MM:SS"))
Print(li_prt, " ")
Print(li_prt, mle_lista_fatt_non_contabilizzate.text)

PrintClose(li_prt)
end event

type mle_lista_fatt_non_contabilizzate from multilineedit within tab_non_contabilizzate
integer x = 41
integer y = 156
integer width = 4201
integer height = 1896
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
boolean border = false
boolean hscrollbar = true
boolean vscrollbar = true
boolean autohscroll = true
boolean autovscroll = true
boolean displayonly = true
end type

