﻿$PBExportHeader$w_xml.srw
forward
global type w_xml from window
end type
type cb_4 from commandbutton within w_xml
end type
type cb_3 from commandbutton within w_xml
end type
type cb_1 from commandbutton within w_xml
end type
type em_num_fattura from editmask within w_xml
end type
type st_4 from statictext within w_xml
end type
type st_3 from statictext within w_xml
end type
type em_anno_fattura from editmask within w_xml
end type
type st_2 from statictext within w_xml
end type
type st_1 from statictext within w_xml
end type
type cb_5 from commandbutton within w_xml
end type
type tv_1 from treeview within w_xml
end type
type cb_2 from commandbutton within w_xml
end type
type sle_xml from singlelineedit within w_xml
end type
end forward

global type w_xml from window
integer width = 3264
integer height = 2704
boolean titlebar = true
string title = "Untitled"
boolean controlmenu = true
boolean minbox = true
boolean maxbox = true
boolean resizable = true
long backcolor = 134217730
string icon = "AppIcon!"
boolean center = true
cb_4 cb_4
cb_3 cb_3
cb_1 cb_1
em_num_fattura em_num_fattura
st_4 st_4
st_3 st_3
em_anno_fattura em_anno_fattura
st_2 st_2
st_1 st_1
cb_5 cb_5
tv_1 tv_1
cb_2 cb_2
sle_xml sle_xml
end type
global w_xml w_xml

type variables

end variables

forward prototypes
public function integer wf_getchild (pbdom_element apbdom_elem, long al_handle)
end prototypes

public function integer wf_getchild (pbdom_element apbdom_elem, long al_handle);boolean 	lb_success, lb_attributes
string		ls_name, ls_text
long 		ll_i, ll_cont, ll_handle
PBDOM_Element		pbdom_element_childs[]
PBDOM_Attribute		pbdom_attributes[]
treeviewitem tvi_campo


tvi_campo.expanded = false
tvi_campo.selected = false
tvi_campo.children = false

lb_success = apbdom_elem.getchildelements(pbdom_element_childs)
ls_name = apbdom_elem.getname()
for ll_i = 1 to upperbound(pbdom_element_childs)
	ls_name = pbdom_element_childs[ll_i].getname( )
	lb_attributes = pbdom_element_childs[ll_i].hasattributes( )
	lb_success = apbdom_elem.getchildelements(pbdom_element_childs)
	if upperbound(pbdom_element_childs) > 0 then 	
		if pbdom_element_childs[ll_i].haschildelements() then
			tvi_campo.children = true
			tvi_campo.label = pbdom_element_childs[ll_i].getname()
		else
			tvi_campo.children = false
			tvi_campo.label = pbdom_element_childs[ll_i].getname() + " = " + pbdom_element_childs[ll_i].gettext()
		end if
		ll_handle = tv_1.insertitemlast( al_handle, tvi_campo)
		
		wf_getchild( pbdom_element_childs[ll_i],ll_handle )
	end if

next

return 0
end function

on w_xml.create
this.cb_4=create cb_4
this.cb_3=create cb_3
this.cb_1=create cb_1
this.em_num_fattura=create em_num_fattura
this.st_4=create st_4
this.st_3=create st_3
this.em_anno_fattura=create em_anno_fattura
this.st_2=create st_2
this.st_1=create st_1
this.cb_5=create cb_5
this.tv_1=create tv_1
this.cb_2=create cb_2
this.sle_xml=create sle_xml
this.Control[]={this.cb_4,&
this.cb_3,&
this.cb_1,&
this.em_num_fattura,&
this.st_4,&
this.st_3,&
this.em_anno_fattura,&
this.st_2,&
this.st_1,&
this.cb_5,&
this.tv_1,&
this.cb_2,&
this.sle_xml}
end on

on w_xml.destroy
destroy(this.cb_4)
destroy(this.cb_3)
destroy(this.cb_1)
destroy(this.em_num_fattura)
destroy(this.st_4)
destroy(this.st_3)
destroy(this.em_anno_fattura)
destroy(this.st_2)
destroy(this.st_1)
destroy(this.cb_5)
destroy(this.tv_1)
destroy(this.cb_2)
destroy(this.sle_xml)
end on

type cb_4 from commandbutton within w_xml
integer x = 46
integer y = 940
integer width = 402
integer height = 112
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "XML1"
end type

event clicked;PBDOM_ELEMENT pbdom_elem_1
PBDOM_ELEMENT pbdom_elem_2
PBDOM_ELEMENT pbdom_elem_3
PBDOM_ELEMENT pbdom_elem_root
PBDOM_DOCUMENT pbdom_doc1
PBDOM_ATTRIBUTE   pbdom_attr


pbdom_elem_1 = Create PBDOM_ELEMENT
pbdom_elem_2 = Create PBDOM_ELEMENT
pbdom_elem_3 = Create PBDOM_ELEMENT

pbdom_elem_1.SetName("pbdom_elem_1")
pbdom_elem_2.SetName("pbdom_elem_2")
pbdom_elem_3.SetName("pbdom_elem_3")

pbdom_elem_1.AddContent(pbdom_elem_2)
pbdom_elem_2.AddContent(pbdom_elem_3)

//pbdom_elem_1.addnamespacedeclaration( "ds", "http://www.w3.org/2000/09/xmldsig#")

pbdom_doc1 = Create PBDOM_DOCUMENT
pbdom_doc1.NewDocument("Root_Element_From_Doc_1")

pbdom_elem_root = pbdom_doc1.GetRootElement()

//pbdom_elem_root.addnamespacedeclaration("xs","http://www.w3.org/2001/XMLSchema")
//pbdom_elem_root.addnamespacedeclaration( "ds", "http://www.w3.org/2000/09/xmldsig#")
//pbdom_elem_root.AddContent(pbdom_elem_1)
pbdom_elem_root.setnamespace( "xs","http://www.w3.org/2001/XMLSchema",false)
pbdom_elem_root.setnamespace( "ds","http://www.w3.org/2000/09/xmldsig#a",false)
pbdom_elem_root.setnamespace("","http://ivaservizi.agenziaentrate.gov.it/docs/xsd/fatture/v1.2",false)

pbdom_doc1.savedocument( "c:\temp\testxml.xml")
end event

type cb_3 from commandbutton within w_xml
integer x = 2583
integer y = 1000
integer width = 402
integer height = 112
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "ELASTIC DB"
end type

type cb_1 from commandbutton within w_xml
integer x = 2560
integer y = 1280
integer width = 549
integer height = 160
integer taborder = 60
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "ELASTIC WS"
end type

event clicked;n_winhttp ln_http
string 	ls_URL, ls_title, ls_pathname, ls_filename, ls_response, ls_file, ls_base64, &
			ls_filter, ls_mimetype, ls_data, ls_license_crtypted,  ls_base64_auth, ls_partita_iva,ls_paese,ls_note, ls_risposta, &
			ls_file_xml, ls_message, ls_num_fattura, ls_cod_utente,ls_password, ls_username
Integer 	li_rc, li_fnum
ULong 	lul_length
blob 		lblob_file, lblob_send
uo_crypto luo_crypto
sailjson luo_parse
n_cst_crypto lnv_crypt

//ls_URL  = "http://test.csteam.com/ragusa/api/elastic/save"
/*
select		username,
			password
into		:ls_username,
			:ls_password
from		utenti
where	cod_utente = :s_cs_xx.cod_utente;

lnv_crypt = CREATE n_cst_crypto

if lnv_crypt.DecryptData(ls_password,ls_password) < 0 then
	g_mb.error("Errore in Decrypt Password")
	return
end if

DESTROY lnv_crypt
*/
//ls_URL  = g_str.format("http://test.csteam.com/ragusa/login/authentication?username=$1&password=$2","ADMIN", "ADMIN")
ls_URL  = "http://test.csteam.com/ragusa/api/elastic/upload"

ln_http.Open("POST", ls_URL)

ln_http.SetRequestHeader("cache-control", "no-cache")
ln_http.SetRequestHeader("Postman-Token", "9ac4b8b3-187c-45da-9cec-2e017bd6ac39")

//DATEDIFF ( datepart , startdate , enddate )

lul_length = ln_http.Send(ls_data)
If lul_length > 0 Then
	luo_parse=create sailjson
	luo_parse.parse( ln_http.ResponseText)
	
	
else
	// Errore impossibile raggiungere il server: forse non c'è internet o il servizio non risponde
	return -1
end if

/*

luo_crypto = create uo_crypto

guo_functions.uof_file_to_blob(sle_xml.text, lblob_send)
ls_base64 = luo_crypto.uof_encode_64(lblob_send)

ls_base64_auth = luo_crypto.uof_encode_64( ls_partita_iva + ":CS.$e95",2 )

ln_http.Open("POST", ls_URL)

ls_note = s_cs_xx.cod_utente + "_" + string(today()) + "_" +string(now())

ln_http.SetRequestHeader("Content-Type", "application/json;charset=utf-8")
ln_http.SetRequestHeader("Authorization", "Basic " + ls_base64_auth)
ls_data = '{"fattura":"'+ls_base64+'", "nomeFile":"'+ls_filename+'","note":"'+ls_note+'"}'

lul_length = ln_http.Send(ls_data)

If lul_length > 0 Then
	
	luo_parse=create sailjson
	
	luo_parse.parse( ln_http.ResponseText)
	
	ls_risposta = ln_http.ResponseText
	
	// Valutare la risposta
	if left(ls_risposta,1)="{" then
		ls_message = g_str.format("ERRORE in invio Fattura $1. Contattare il servizio di assistenza. Dettaglio Errore $2 ", ls_num_fattura, ln_http.lasterrortext)
		return -1
	end if	
	
else
	// Errore impossibile raggiungere il server: forse non c'è internet o il servizio non risponde
	ls_message = g_str.format("ERRORI in invio Fattura $1. Dettaglio Errore $2 ", ls_num_fattura, ln_http.lasterrortext)
	return -1
end if
*/
return
end event

type em_num_fattura from editmask within w_xml
integer x = 2560
integer y = 680
integer width = 571
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "1238"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "######"
end type

type st_4 from statictext within w_xml
integer x = 2560
integer y = 620
integer width = 571
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean italic = true
long textcolor = 16711680
long backcolor = 553648127
string text = "numero fattura"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_3 from statictext within w_xml
integer x = 2560
integer y = 420
integer width = 571
integer height = 60
integer textsize = -8
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean italic = true
long textcolor = 16711680
long backcolor = 553648127
string text = "anno fattura"
alignment alignment = center!
boolean focusrectangle = false
end type

type em_anno_fattura from editmask within w_xml
integer x = 2560
integer y = 480
integer width = 571
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "2016"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "0000"
end type

type st_2 from statictext within w_xml
integer x = 2537
integer y = 160
integer width = 594
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 67108864
string text = "VENDITE"
boolean focusrectangle = false
end type

type st_1 from statictext within w_xml
integer x = 46
integer y = 160
integer width = 571
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 16711680
long backcolor = 67108864
string text = "ACQUISTI"
boolean focusrectangle = false
end type

type cb_5 from commandbutton within w_xml
integer x = 2537
integer y = 800
integer width = 617
integer height = 120
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "TEST FATT"
end type

event clicked;//// ---------- carico i dati della fattura in variabili locali ---------------------------
//boolean	lb_result=false
//string		ls_sql
//
//string		ls_numeratore_documento,ls_trasmittente_idpaese, ls_trasmittente_idcodice, ls_codice_destinatario, ls_pec_destinatario, ls_trasmittente_telefono, ls_trasmittente_email, &
//			ls_pec, ls_flag_pubblica_amministrazione,ls_cedente_idpaese,ls_cedente_codice,ls_cedente_codicefiscale,ls_cedente_denominazione,ls_cedente_denominazione2,ls_cedente_nome,ls_cedente_cognome,&
//			ls_cedente_regimefiscale,ls_cedente_sede_indirizzo,ls_cedente_sede_numerocivico,ls_cedente_sede_cap,ls_cedente_sede_comune,ls_cedente_sede_provincia, &
//			ls_cedente_sede_cod_nazione,ls_cedente_telefono,ls_cedente_fax,ls_cessionario_idpaese,ls_flag_tipo_cliente,ls_cessionario_partita_iva,ls_cessionario_cod_fiscale, &
//			ls_cessionario_rag_soc_1,ls_cessionario_rag_soc_2,ls_cessionario_nome,ls_cessionario_cognome,ls_cessionario_indirizzo,ls_cessionario_num_civico,ls_cessionario_cap,ls_cessionario_localita,&
//			ls_cessionario_provincia, ls_cod_documento
//			//body
//string		ls_fatel_tipo_documento, ls_fatel_divisa, ls_filename
//			
//long		ll_anno_registrazione, ll_num_registrazione, ll_anno_documento, ll_num_documento
//
//dec{4}	ld_tot_fattura
//
//datetime	ldt_data_fattura
//
//datastore	lds_dettaglio, lds_iva, lds_scadenze
//
//
//ll_anno_registrazione = long(em_anno_fattura.text)
//ll_num_registrazione=long(em_num_fattura.text)
//
//select 
//TES.cod_documento, 
//TES.anno_documento, 
//TES.numeratore_documento,
//TES.num_documento,
//TES.data_fattura,
//TES.tot_fattura,
//AZI.fatel_idpaese_trasmittente,
//AZI.fatel_idcodice_trasmittente,
//CLI.fatel_cod_destinatario,
//CLI.fatel_pec,
//CLI.telefono,
//CLI.email_amministrazione,
//CLI.fatel_flag_pa,
//AZI.fatel_cod_nazione,
//AZI.partita_iva,
//AZI.fatel_cod_fiscale,
//AZI.rag_soc_1,
//isnull(AZI.rag_soc_2,''),
//AZI.fatel_nome,
//AZI.fatel_cognome,
//AZI.fatel_regime_fiscale,
//AZI.fatel_sede_indirizzo,
//AZI.fatel_sede_numerocivico,
//AZI.fatel_sede_cap,
//AZI.fatel_sede_comune,
//AZI.fatel_sede_provincia,
//AZI.fatel_sede_cod_nazione,
//AZI.telefono,
//AZI.fax,
//CLI.fatel_idpaese,
//CLI.flag_tipo_cliente,
//CLI.partita_iva,
//CLI.cod_fiscale,
//CLI.rag_soc_1,
//isnull(CLI.rag_soc_2,''),
//CLI.fatel_nome,
//CLI.fatel_cognome,
//CLI.indirizzo,
//CLI.fatel_num_civico,
//CLI.cap,
//CLI.localita,
//CLI.provincia,
//TIPOFAT.fatel_tipo_documento,
//VAL.fatel_divisa
//into 
//:ls_cod_documento,
//:ll_anno_documento,
//:ls_numeratore_documento,
//:ll_num_documento,
//:ldt_data_fattura,
//:ld_tot_fattura,
//:ls_trasmittente_idpaese,
//:ls_trasmittente_idcodice,
//:ls_codice_destinatario,
//:ls_pec_destinatario,
//:ls_trasmittente_telefono,
//:ls_trasmittente_email,
//:ls_flag_pubblica_amministrazione,
//:ls_cedente_idpaese,
//:ls_cedente_codice,
//:ls_cedente_codicefiscale,
//:ls_cedente_denominazione,
//:ls_cedente_denominazione2,
//:ls_cedente_nome,
//:ls_cedente_cognome,
//:ls_cedente_regimefiscale,
//:ls_cedente_sede_indirizzo,
//:ls_cedente_sede_numerocivico,
//:ls_cedente_sede_cap,
//:ls_cedente_sede_comune,
//:ls_cedente_sede_provincia,
//:ls_cedente_sede_cod_nazione,
//:ls_cedente_telefono,
//:ls_cedente_fax,
//:ls_cessionario_idpaese,
//:ls_flag_tipo_cliente,
//:ls_cessionario_partita_iva,
//:ls_cessionario_cod_fiscale,
//:ls_cessionario_rag_soc_1,
//:ls_cessionario_rag_soc_2,
//:ls_cessionario_nome,
//:ls_cessionario_cognome,
//:ls_cessionario_indirizzo,
//:ls_cessionario_num_civico,
//:ls_cessionario_cap,
//:ls_cessionario_localita,
//:ls_cessionario_provincia,
//:ls_fatel_tipo_documento,
//:ls_fatel_divisa
//from tes_fat_ven TES
//join aziende AZI on TES.cod_azienda = AZI.cod_azienda
//join anag_clienti CLI on TES.cod_azienda=CLI.cod_azienda and TES.cod_cliente=CLI.cod_cliente
//join tab_tipi_fat_ven TIPOFAT on TES.cod_azienda=TIPOFAT.cod_azienda and TES.cod_tipo_fat_ven=TIPOFAT.cod_tipo_fat_ven 
//join tab_valute VAL on TES.cod_azienda=VAL.cod_azienda and TES.cod_valuta=VAL.cod_valuta
//where TES.cod_azienda = :s_cs_xx.cod_azienda and
//TES.anno_registrazione = :ll_anno_registrazione and
//TES.num_registrazione = :ll_num_registrazione;
//if sqlca.sqlcode = 100 then
//	g_mb.error("La fattura richiesta non esiste")
//	return
//end if
//if sqlca.sqlcode = -1 then
//	g_mb.error(sqlca.sqlerrtext)
//	return
//end if
//
//// -----------  generazione fattura elettronica -------------------
//string	ls_str, ls_errore
//long ll_i, ll_row, ll_ret
//PBDOM_BUILDER pb_builder
//PBDOM_object pb_object
//PBDOM_DOCUMENT pbdom_doc
//PBDOM_ELEMENT pbdom_elem, pbdom_new_element, pbdoc_element_results, pb_elements[]
//uo_fatel luo_fatel
//
//
//pb_builder = create PBDOM_BUILDER
//if ls_flag_pubblica_amministrazione="S" then
//	ls_filename = s_cs_xx.volume + s_cs_xx.risorse + "fatel\basePA.xml"
//else
//	ls_filename = s_cs_xx.volume + s_cs_xx.risorse + "fatel\basePR.xml"
//end if
//pbdom_doc = pb_builder.buildfromfile( ls_filename)
//luo_fatel = CREATE uo_fatel
//
//// carico le variabili prese dalla fattura
//luo_fatel.is_idtrasmiittente_idpaese=ls_trasmittente_idpaese
//luo_fatel.is_idtrasmiittente_idcodice=ls_trasmittente_idcodice
//luo_fatel.is_datitrasmissione_progressivoinvio= g_str.format("$1$2$3",ll_anno_documento,ls_numeratore_documento,string(ll_num_documento,"00000"))
//if ls_flag_pubblica_amministrazione="S" then
//	luo_fatel.is_datitrasmissione_formatotrasmissione="FPA12"
//else
//	luo_fatel.is_datitrasmissione_formatotrasmissione="FPR12"
//end if
//
////se il codice destinatario non è stato impostato vuol dire che il cliente non si è accreditato sul SDI e quindi la fattura va mandata via PEC
//luo_fatel.is_datitrasmissione_codicedestinatario=ls_codice_destinatario
//luo_fatel.is_datitrasmissione_pecdestinatario=left(ls_pec_destinatario,256)
//
//if ls_flag_pubblica_amministrazione = "S" then
//	if len(luo_fatel.is_datitrasmissione_codicedestinatario) <> 6 then
//		g_mb.error("Il destinatario è una Pubblica Amministrazione: il codice destinatario deve essere di 6 caratteri.")
//		return
//	end if
//else
//	if luo_fatel.is_datitrasmissione_codicedestinatario = "0000000" or len(luo_fatel.is_datitrasmissione_codicedestinatario) = 0 then /// uso la PEC
//		luo_fatel.is_datitrasmissione_codicedestinatario = "0000000"
//		if len(luo_fatel.is_datitrasmissione_pecdestinatario) < 7 then
//			g_mb.error("Attenzione: il codice SDI del destinatario non è stato specificato e la PEC ha meno di 7 caratteri: verificare!")
//			return
//		end if
//	end if
//end if
//
//luo_fatel.is_contattitrasmittente_telefono=left(ls_trasmittente_telefono,12)
//luo_fatel.is_contattitrasmittente_email=left(ls_trasmittente_email,256)
//luo_fatel.is_cedenteprestatore_id_paese=ls_cedente_idpaese
//luo_fatel.is_cedenteprestatore_idcodice=ls_cedente_codice
//luo_fatel.is_cedenteprestatore_codicefiscale=ls_cedente_codicefiscale
//luo_fatel.is_cedenteprestatore_denominazione= ls_cedente_denominazione
//if len(ls_cedente_denominazione2) > 0 then luo_fatel.is_cedenteprestatore_denominazione += " " + ls_cedente_denominazione2
//luo_fatel.is_cedenteprestatore_nome=ls_cedente_nome
//luo_fatel.is_cedenteprestatore_cognome=ls_cedente_cognome
//luo_fatel.is_cedenteprestatore_regimefiscale=ls_cedente_regimefiscale
//luo_fatel.is_cedenteprestatoresede_indirizzo=ls_cedente_sede_indirizzo
//luo_fatel.is_cedenteprestatoresede_numerocivico=ls_cedente_sede_numerocivico
//luo_fatel.is_cedenteprestatoresede_cap=ls_cedente_sede_cap
//luo_fatel.is_cedenteprestatoresede_comune=ls_cedente_sede_comune
//luo_fatel.is_cedenteprestatoresede_provincia=ls_cedente_sede_provincia
//luo_fatel.is_cedenteprestatoresede_nazione=ls_cedente_sede_cod_nazione
//luo_fatel.is_cedenteprestatore_telefono=ls_cedente_telefono
//luo_fatel.is_cedenteprestatore_fax=ls_cedente_fax
//
//luo_fatel.is_cessionario_idpaese = ls_cessionario_idpaese
//choose case ls_flag_tipo_cliente
//	case "S", "F"
//		luo_fatel.is_cessionario_idcodice = ls_cessionario_partita_iva
//	case "C", "E"
//		luo_fatel.is_cessionario_idcodice = ls_cessionario_cod_fiscale
//	case else
//		// errore: si tratta di un privato o altro
//		if sqlca.sqlcode = 100 then
//			g_mb.error(g_str.format("Il cessionario $1 non è Società, Persona Fisica, Cee, Estero. Verificare il Tipo Cliente in anagrafica", ls_cessionario_rag_soc_1))
//			return
//		end if
//end choose
//
//luo_fatel.is_cessionario_denominazione=ls_cessionario_rag_soc_1
//if len(ls_cessionario_rag_soc_2) > 0 then luo_fatel.is_cessionario_denominazione += " " + ls_cessionario_rag_soc_2
//luo_fatel.is_cessionario_nome= ls_cessionario_nome
//luo_fatel.is_cessionario_cognome= ls_cessionario_cognome
//luo_fatel.is_cessionariosede_indirizzo = ls_cessionario_indirizzo
//luo_fatel.is_cessionariosede_numerocivico= ls_cessionario_num_civico
//luo_fatel.is_cessionariosede_cap= ls_cessionario_cap
//luo_fatel.is_cessionariosede_comune= ls_cessionario_localita
//luo_fatel.is_cessionariosede_provincia= ls_cessionario_provincia
//luo_fatel.is_cessionariosede_nazione= ls_cessionario_idpaese
//
//// caricamento variabili per Body
//luo_fatel.is_datigen_tipo_documento = ls_fatel_tipo_documento
//luo_fatel.is_datigen_divisa = ls_fatel_divisa
//luo_fatel.is_datigen_data = string(ldt_data_fattura, "YYYY-MM-DD")
//luo_fatel.is_datigen_numero = g_str.format( "$1-$2-$3-$4", ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento  )
////luo_fatel.is_datigen_causale
//
//luo_fatel.is_datigen_totaledocumento = luo_fatel.uof_dec_to_string( ld_tot_fattura)
//
//
//// ---------- caricamento dati del dettaglio fattura ----------------------------
//
//ls_sql = g_str.format(  &
//" select prog_riga_fat_ven, DET.cod_prodotto, isnull(DET.des_prodotto, ANAG.des_prodotto), isnull(quan_fatturata,0), TIPODET.fatel_tipo_cessione, isnull(iva.aliquota,0), isnull(iva.des_iva,''), cod_misura, isnull(DET.prezzo_vendita,0), isnull(imponibile_iva,0), isnull(sconto_1,0), isnull(sconto_2,0), isnull(sconto_3,0), IVA.fatel_natura_esenzione  " +&
//" from det_fat_ven DET " +&
//" left join anag_prodotti ANAG on DET.cod_azienda = ANAG.cod_azienda and DET.cod_prodotto=ANAG.cod_prodotto  " +&
//" left join tab_tipi_det_ven TIPODET on DET.cod_azienda = TIPODET.cod_azienda and DET.cod_prodotto=TIPODET.cod_tipo_det_ven  " +&
//" left join tab_ive IVA on DET.cod_azienda = IVA.cod_azienda and DET.cod_iva=IVA.cod_iva " +&
//" WHERE DET.cod_azienda = '$1' and anno_registrazione=$2 and num_registrazione=$3 ORDER BY 1", s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione ) 
//
//ll_ret = guo_functions.uof_crea_datastore( lds_dettaglio, ls_sql, ls_errore)
//if ll_ret < 0 then
//	g_mb.error(g_str.format("Errore in fase di creazione datastore del dettaglio della fattura.$1",ls_errore))
//	return
//end if
//
//for ll_row = 1 to ll_ret
//	luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].numero_linea= string( lds_dettaglio.getitemnumber(ll_row,1 ))
//	if not isnull(lds_dettaglio.getitemstring(ll_row,2)) then
//		luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].codice_tipo="NSCOD"
//		luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].codice_valore=lds_dettaglio.getitemstring(ll_row,2)
//	else
//		setnull(luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].codice_tipo)
//		setnull(luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].codice_valore)
//	end if
//	luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].descrizione=lds_dettaglio.getitemstring(ll_row,3)
//	luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].quantita= luo_fatel.uof_dec_to_string( lds_dettaglio.getitemnumber(ll_row,4) )
//	luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].tipo_cessione=lds_dettaglio.getitemstring(ll_row,5)
//	luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].prezzototale=luo_fatel.uof_dec_to_string( lds_dettaglio.getitemnumber(ll_row,10) )
//	luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].um=lds_dettaglio.getitemstring(ll_row,8)
//	luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].prezzo_unitario=luo_fatel.uof_dec_to_string( lds_dettaglio.getitemnumber(ll_row,9) )
//	luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].aliquota = luo_fatel.uof_dec_to_string(lds_dettaglio.getitemnumber(ll_row,6))
//	// sconto_1
//	if lds_dettaglio.getitemnumber(ll_row,11) > 0 then
//		luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].sconti[1].sconto_tipo="SC"
//		luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].sconti[1].sconto_perc=  luo_fatel.uof_dec_to_string( lds_dettaglio.getitemnumber(ll_row,11))
//		// sconto_2
//		if lds_dettaglio.getitemnumber(ll_row,12) > 0 then
//			luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].sconti[2].sconto_tipo="SC"
//			luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].sconti[2].sconto_perc=  luo_fatel.uof_dec_to_string( lds_dettaglio.getitemnumber(ll_row,12))
//			// sconto_3
//			if lds_dettaglio.getitemnumber(ll_row,13) > 0 then
//				luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].sconti[3].sconto_tipo="SC"
//				luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].sconti[3].sconto_perc=  luo_fatel.uof_dec_to_string( lds_dettaglio.getitemnumber(ll_row,13))
//			end if
//		end if
//	end if
//	luo_fatel.istr_bodybeni_dettaglio_linee[ll_row].des_esenzione=lds_dettaglio.getitemstring(ll_row,14)
//	
//next
//
//
//// ---------- caricamento dati riepilogo iva della fattura ----------------------------
//ls_sql = g_str.format(  " select TES.cod_iva, isnull(TES.imponibile_iva,0), isnull(TES.importo_iva,0), IVA.fatel_natura_esenzione, isnull(IVA.aliquota,0), fatel_esigibilita_iva, isnull(IVA.des_estesa, IVA.des_iva) from iva_fat_ven TES left join tab_ive IVA on TES.cod_azienda = IVA.cod_azienda and TES.cod_iva=IVA.cod_iva " +&
//" WHERE TES.cod_azienda = '$1' and anno_registrazione=$2 and num_registrazione=$3 ORDER BY 1", s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione ) 
//
//ll_ret = guo_functions.uof_crea_datastore( lds_iva, ls_sql, ls_errore)
//if ll_ret < 0 then
//	g_mb.error(g_str.format("Errore in fase di creazione datastore del dettaglio della fattura.$1",ls_errore))
//	return
//end if
//
//for ll_row = 1 to ll_ret
//	luo_fatel.istr_bodybeni_dati_riepilogo[ll_row].riepilogo_aliquota_iva = luo_fatel.uof_dec_to_string( lds_iva.getitemnumber(ll_row, 5))
//	luo_fatel.istr_bodybeni_dati_riepilogo[ll_row].riepilogo_iva_natura = lds_iva.getitemstring(ll_row, 4)
//	luo_fatel.istr_bodybeni_dati_riepilogo[ll_row].riepilogo_iva_riferimento_norm =lds_iva.getitemstring(ll_row, 7)
//	luo_fatel.istr_bodybeni_dati_riepilogo[ll_row].riepilogo_iva_imponibile = luo_fatel.uof_dec_to_string(lds_iva.getitemnumber(ll_row, 2))
//	luo_fatel.istr_bodybeni_dati_riepilogo[ll_row].riepilogo_iva_imposta = luo_fatel.uof_dec_to_string(lds_iva.getitemnumber(ll_row, 3))
//	luo_fatel.istr_bodybeni_dati_riepilogo[ll_row].riepilogo_iva_esigibilita_iva = lds_iva.getitemstring(ll_row, 6)
//next
//// ----------------------------------------------------------------------------------
//
//
//// ---------- caricamento dati riepilogo iva della fattura ----------------------------
//ls_sql = g_str.format(  " select TES.cod_iva, isnull(TES.imponibile_iva,0), isnull(TES.importo_iva,0), IVA.fatel_natura_esenzione, isnull(IVA.aliquota,0), fatel_esigibilita_iva, isnull(IVA.des_estesa, IVA.des_iva) from iva_fat_ven TES left join tab_ive IVA on TES.cod_azienda = IVA.cod_azienda and TES.cod_iva=IVA.cod_iva " +&
//" WHERE TES.cod_azienda = '$1' and anno_registrazione=$2 and num_registrazione=$3 ORDER BY 1", s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione ) 
//
//ll_ret = guo_functions.uof_crea_datastore( lds_iva, ls_sql, ls_errore)
//if ll_ret < 0 then
//	g_mb.error(g_str.format("Errore in fase di creazione datastore del dettaglio della fattura.$1",ls_errore))
//	return
//end if
//
//for ll_row = 1 to ll_ret
//	luo_fatel.istr_bodybeni_dati_riepilogo[ll_row].riepilogo_aliquota_iva = luo_fatel.uof_dec_to_string( lds_iva.getitemnumber(ll_row, 5))
//	luo_fatel.istr_bodybeni_dati_riepilogo[ll_row].riepilogo_iva_natura = lds_iva.getitemstring(ll_row, 4)
//	luo_fatel.istr_bodybeni_dati_riepilogo[ll_row].riepilogo_iva_riferimento_norm =lds_iva.getitemstring(ll_row, 7)
//	luo_fatel.istr_bodybeni_dati_riepilogo[ll_row].riepilogo_iva_imponibile = luo_fatel.uof_dec_to_string(lds_iva.getitemnumber(ll_row, 2))
//	luo_fatel.istr_bodybeni_dati_riepilogo[ll_row].riepilogo_iva_imposta = luo_fatel.uof_dec_to_string(lds_iva.getitemnumber(ll_row, 3))
//	luo_fatel.istr_bodybeni_dati_riepilogo[ll_row].riepilogo_iva_esigibilita_iva = lds_iva.getitemstring(ll_row, 6)
//next
//// ----------------------------------------------------------------------------------
//
//// ---------- caricamento dati pagamenti ----------------------------
//
//ls_sql = g_str.format(  "select SCAD.prog_scadenza, SCAD.data_scadenza, SCAD.imp_rata, SCAD.cod_tipo_pagamento, SCAD.fatel_modalita_pagamento,TES.cod_banca, TES.cod_pagamento, PAG.fatel_condizioni_pagamento, PAG.flag_tipo_pagamento, BAN.iban, ABI1.des_abi, BAP.cod_abi, BAP.cod_cab, ABI2.des_abi, CAB.agenzia " + &
//" from tes_fat_ven TES " + &
//" left join scad_fat_ven SCAD on TES.cod_azienda = SCAD.cod_azienda and TES.anno_registrazione=SCAD.anno_registrazione and TES.num_registrazione=SCAD.num_registrazione " + &
//" left join tab_pagamenti PAG on TES.cod_azienda = PAG.cod_azienda and TES.cod_pagamento=PAG.cod_pagamento " + &
//" left join anag_banche BAN on TES.cod_azienda = BAN.cod_azienda and TES.cod_banca=BAN.cod_banca " + &
//" left join anag_banche_clien_for BAP on TES.cod_azienda = BAP.cod_azienda and TES.cod_banca_clien_for=BAP.cod_banca_clien_for " + &
//" left join tab_abi ABI1 on BAN.cod_abi=ABI1.cod_abi " + &
//" left join tab_abi ABI2 on BAP.cod_abi=ABI2.cod_abi " + &
//" left join tab_abicab CAB on BAP.cod_abi=CAB.cod_abi and BAP.cod_cab=CAB.cod_cab " + &
//" WHERE TES.cod_azienda = '$1' and TES.anno_registrazione=$2 and TES.num_registrazione=$3 " + &
//" ORDER BY 1 ", s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione ) 
//
//
//
//ll_ret = guo_functions.uof_crea_datastore( lds_scadenze, ls_sql, ls_errore)
//if ll_ret < 0 then
//	g_mb.error(g_str.format("Errore in fase di creazione datastore del dettaglio della fattura.$1",ls_errore))
//	return
//end if
//luo_fatel.is_condizioni_pagamento = lds_scadenze.getitemstring(ll_row, 8)
//for ll_row = 1 to ll_ret
//	luo_fatel.istr_body_pagamenti[ll_row].modalita_pagamento = lds_scadenze.getitemstring(ll_row, 5)
//	luo_fatel.istr_body_pagamenti[ll_row].data_scadenza = string(lds_scadenze.getitemdatetime(ll_row, 2), "YYYY-MM-DD")
//	luo_fatel.istr_body_pagamenti[ll_row].importo_scadenza = luo_fatel.uof_dec_to_string( lds_scadenze.getitemnumber(ll_row, 3) )
//	if lds_scadenze.getitemstring(ll_row, 4) = "R" then
//		// RIBA quindi carico dati banca cliente
//		luo_fatel.istr_body_pagamenti[ll_row].des_banca = lds_scadenze.getitemstring(ll_row, 14) + lds_scadenze.getitemstring(ll_row, 15)
//		luo_fatel.istr_body_pagamenti[ll_row].abi_cliente =lds_scadenze.getitemstring(ll_row, 12)
//		luo_fatel.istr_body_pagamenti[ll_row].cab_cliente = lds_scadenze.getitemstring(ll_row, 13)
//		setnull(luo_fatel.istr_body_pagamenti[ll_row].iban_banca)
//		setnull(luo_fatel.istr_body_pagamenti[ll_row].bic_banca)
//	else
//		luo_fatel.istr_body_pagamenti[ll_row].des_banca  = lds_scadenze.getitemstring(ll_row, 11)
//		luo_fatel.istr_body_pagamenti[ll_row].iban_banca = lds_scadenze.getitemstring(ll_row, 10)
//		setnull(luo_fatel.istr_body_pagamenti[ll_row].bic_banca)
//		setnull(luo_fatel.istr_body_pagamenti[ll_row].abi_cliente)
//		setnull(luo_fatel.istr_body_pagamenti[ll_row].cab_cliente)
//	end if
//next
//// ----------------------------------------------------------------------------------
//
//
//
//// avvio il procedimento di creazione XML
//pbdom_doc.getrootelement( ).getchildelements(pb_elements[])
//for ll_i = 1 to upperbound(pb_elements[])
//	
//	ls_str = pb_elements[ll_i].getname()
//	
//	choose case ll_i
//		case 1 // header
//			luo_fatel.pbdom_header=pb_elements[1]
//			if luo_fatel.uof_header_validate( ref ls_errore) = -1 then
//				g_mb.error(ls_errore)
//				destroy luo_fatel
//				return
//			end if
//			luo_fatel.uof_header_create(  )
//			pb_elements[1] = luo_fatel.pbdom_header
//		case 2 // body
//			luo_fatel.pbdom_body=pb_elements[2]
//			luo_fatel.uof_body_create(  )
//			pb_elements[1] = luo_fatel.pbdom_body
//			
//	end choose
//			
//
//	
////	destroy pbdom_new_element
//next
//
//pbdom_doc.getrootelement( ).removecontent(pb_elements[1])
//pbdom_doc.getrootelement( ).addcontent( pb_elements[1])
//pbdom_doc.getrootelement( ).removecontent(pb_elements[2])
//pbdom_doc.getrootelement( ).addcontent( pb_elements[2])
//
//ls_filename  = ls_cedente_idpaese + ls_cedente_codice + "_"
//if ls_flag_pubblica_amministrazione="S" then
//	ls_filename = s_cs_xx.volume + s_cs_xx.risorse + "fatel\" + ls_filename + "FPA01.xml"
//else
//	ls_filename = s_cs_xx.volume + s_cs_xx.risorse + "fatel\" + ls_filename + "FPR01.xml"
//end if
//sle_xml.text = ls_filename
//if not pbdom_doc.SaveDocument (ls_filename ) then
//	g_mb.error("Errore in salvataggio file")
//end if
//
//

string	ls_errore
long ll_anno_registrazione, ll_num_registrazione, ll_ret
uo_fatel luo_fatel

ll_anno_registrazione = long(em_anno_fattura.text)
ll_num_registrazione=long(em_num_fattura.text)

luo_fatel = CREATE uo_fatel


ll_ret = luo_fatel.uof_fattura_xml( ll_anno_registrazione, ll_num_registrazione, ls_errore)
if ll_ret < 0 then
	g_mb.error(ls_errore)
end if
destroy luo_fatel


end event

type tv_1 from treeview within w_xml
integer x = 663
integer y = 140
integer width = 1851
integer height = 2200
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
long picturemaskcolor = 536870912
long statepicturemaskcolor = 536870912
end type

type cb_2 from commandbutton within w_xml
integer x = 23
integer y = 340
integer width = 617
integer height = 120
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "XML"
end type

event clicked;boolean 	lb_success
string		ls_name
long 		ll_i, ll_cont
PBDOM_BUILDER pb_builder
PBDOM_Document    pbdom_doc	
PBDOM_Element     pbdom_elem, pbdom_element_childs[]


pb_builder = create PBDOM_BUILDER

pbdom_doc = pb_builder.buildfromfile( sle_xml.text )
pbdom_elem = pbdom_doc.GetRootElement()
ls_name = pbdom_elem.GetName()

tv_1.deleteitem(0)

wf_getchild(pbdom_elem,0)


destroy pbdom_doc
destroy pb_builder

end event

type sle_xml from singlelineedit within w_xml
integer x = 23
integer y = 20
integer width = 2971
integer height = 100
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
string text = "C:\cs_svil\FatturaPa\IT01234567890_FPR02.xml"
borderstyle borderstyle = stylelowered!
end type

