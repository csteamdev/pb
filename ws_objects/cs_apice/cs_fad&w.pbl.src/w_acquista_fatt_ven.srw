﻿$PBExportHeader$w_acquista_fatt_ven.srw
forward
global type w_acquista_fatt_ven from window
end type
type st_tot_fattura from statictext within w_acquista_fatt_ven
end type
type st_tot_righe from statictext within w_acquista_fatt_ven
end type
type sle_dbparm from singlelineedit within w_acquista_fatt_ven
end type
type st_9 from statictext within w_acquista_fatt_ven
end type
type st_8 from statictext within w_acquista_fatt_ven
end type
type st_7 from statictext within w_acquista_fatt_ven
end type
type sle_dbms from singlelineedit within w_acquista_fatt_ven
end type
type sle_servername from singlelineedit within w_acquista_fatt_ven
end type
type st_6 from statictext within w_acquista_fatt_ven
end type
type st_5 from statictext within w_acquista_fatt_ven
end type
type cb_annulla from commandbutton within w_acquista_fatt_ven
end type
type dw_tipo_det_acq from datawindow within w_acquista_fatt_ven
end type
type dw_lista from datawindow within w_acquista_fatt_ven
end type
type rb_ptenda from radiobutton within w_acquista_fatt_ven
end type
type rb_cgibus from radiobutton within w_acquista_fatt_ven
end type
type rb_viropa from radiobutton within w_acquista_fatt_ven
end type
type st_1 from statictext within w_acquista_fatt_ven
end type
type cb_conferma from commandbutton within w_acquista_fatt_ven
end type
type sle_cod_documento from singlelineedit within w_acquista_fatt_ven
end type
type sle_numeratore_documento from singlelineedit within w_acquista_fatt_ven
end type
type em_anno_documento from editmask within w_acquista_fatt_ven
end type
type em_num_documento from editmask within w_acquista_fatt_ven
end type
type st_3 from statictext within w_acquista_fatt_ven
end type
type st_4 from statictext within w_acquista_fatt_ven
end type
type cb_seleziona from commandbutton within w_acquista_fatt_ven
end type
type r_db from rectangle within w_acquista_fatt_ven
end type
type r_1 from rectangle within w_acquista_fatt_ven
end type
type st_2 from statictext within w_acquista_fatt_ven
end type
type r_2 from rectangle within w_acquista_fatt_ven
end type
type r_3 from rectangle within w_acquista_fatt_ven
end type
type st_anno_fat_ven from statictext within w_acquista_fatt_ven
end type
type st_num_fat_ven from statictext within w_acquista_fatt_ven
end type
type r_4 from rectangle within w_acquista_fatt_ven
end type
type r_5 from rectangle within w_acquista_fatt_ven
end type
type r_6 from rectangle within w_acquista_fatt_ven
end type
end forward

global type w_acquista_fatt_ven from window
integer width = 4343
integer height = 2396
boolean titlebar = true
string title = "Importa Fattura Vendita come Acquisto"
windowtype windowtype = response!
long backcolor = 12632256
string icon = "AppIcon!"
boolean center = true
st_tot_fattura st_tot_fattura
st_tot_righe st_tot_righe
sle_dbparm sle_dbparm
st_9 st_9
st_8 st_8
st_7 st_7
sle_dbms sle_dbms
sle_servername sle_servername
st_6 st_6
st_5 st_5
cb_annulla cb_annulla
dw_tipo_det_acq dw_tipo_det_acq
dw_lista dw_lista
rb_ptenda rb_ptenda
rb_cgibus rb_cgibus
rb_viropa rb_viropa
st_1 st_1
cb_conferma cb_conferma
sle_cod_documento sle_cod_documento
sle_numeratore_documento sle_numeratore_documento
em_anno_documento em_anno_documento
em_num_documento em_num_documento
st_3 st_3
st_4 st_4
cb_seleziona cb_seleziona
r_db r_db
r_1 r_1
st_2 st_2
r_2 r_2
r_3 r_3
st_anno_fat_ven st_anno_fat_ven
st_num_fat_ven st_num_fat_ven
r_4 r_4
r_5 r_5
r_6 r_6
end type
global w_acquista_fatt_ven w_acquista_fatt_ven

type variables
long		il_num_reg_fat_acq
integer	il_anno_reg_fat_acq
boolean	ib_continua=false
string		is_users_db[], is_pwd_db[], is_db_name[], is_cod_azienda_old="A01"
end variables

forward prototypes
public function integer wf_connetti (integer fi_database, ref transaction ftra_transaction, ref string fs_errore)
public subroutine wf_proteggi_colonne_dw ()
public function integer wf_crea_dettagli (ref string fs_errore)
public function integer wf_crea_dest_mov_fat_acq (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio)
public function integer wf_tipo_det_acq (long fl_row, string fs_cod_tipo_det_acq, boolean fb_itemchanged, ref string fs_errore)
public function integer wf_prodotto (long fl_row, string fs_cod_prodotto, ref string fs_errore)
public function integer wf_select_det_fat_ven (string fs_cod_documento, integer fi_anno_documento, string fs_numeratore_documento, long fl_num_documento, ref integer fi_anno_fat_ven, ref long fl_num_fat_ven, ref decimal fd_tot_fattura, ref string fs_errore)
end prototypes

public function integer wf_connetti (integer fi_database, ref transaction ftra_transaction, ref string fs_errore);//fi_database
//		1 database cs_db_ptenda
//		2 database cs_db_cgibus
//		3 database cs_db_viropa


string							ls_appo
n_cst_crypto				luo_crypto
long							ll_ret

ftra_transaction = create transaction


//SERVERNAME -----------------------------------------------------------------
ls_appo = sle_servername.text
if ls_appo="" or isnull(ls_appo) then
	fs_errore = "Indicare il parametro servername!"
	destroy ftra_transaction;
	return -1
end if
ftra_transaction.ServerName = ls_appo

//DBMS -------------------------------------------------------------------------
ls_appo = sle_dbms.text
if ls_appo="" or isnull(ls_appo) then
	fs_errore = "Indicare il parametro DBMS!"
	destroy ftra_transaction;
	return -1
end if
ftra_transaction.DBMS = ls_appo


//DATABASE -----------------------------------------------------------------------------------------
choose case fi_database
	case 1,2,3
		ftra_transaction.Database = is_db_name[fi_database]
		
	case else
		fs_errore = "Indicare un database tra quelli previsti!"
		destroy ftra_transaction;
		return -1
end choose


//LOGID, LOGPASS -----------------------------------------------------------------------------------------
ftra_transaction.LogId = is_users_db[fi_database]

ls_appo = is_pwd_db[fi_database]

luo_crypto = create n_cst_crypto
ll_ret = luo_crypto.decryptdata( ls_appo, ls_appo)  
destroy luo_crypto;

if ll_ret < 0 then
	fs_errore = "La password contiene caratteri non consentiti.I caratteri consentiti comprendono:" + &
				"- tutte le cifre numeriche 0,1,2,...,9  tutte le lettere maiuscole A,B,C,...,Z e minuscole a,b,c,...,z" + &
				"- alcuni simboli.Modificare la password e riprovare"
	return -1
end if
ftra_transaction.LogPass = ls_appo


//DBPARM ----------------------------------------------------------------------------------------------------
ls_appo = sle_dbparm.text
if ls_appo="" or isnull(ls_appo) then
	fs_errore = "Indicare il parametro DBPARM!"
	destroy ftra_transaction;
	return -1
end if
ftra_transaction.DBParm = ls_appo


//CONNESIONE ------------------------------------------------------------------------------------------------
disconnect using ftra_transaction;
connect using ftra_transaction;
if ftra_transaction.sqlcode <> 0 then
	fs_errore = "Errore durante la connessione al database~r~n" + ftra_transaction.sqlerrtext
	return -1
end if

return 1
end function

public subroutine wf_proteggi_colonne_dw ();string ls_count, ls_column_name, ls_col_index
long ll_index


//numero di colonne nella dw di dettaglio
ls_count = dw_lista.Describe("DataWindow.Column.Count")

for ll_index = 1 to integer(ls_count)
	ls_col_index = "#"+string(ll_index)
	ls_column_name = dw_lista.Describe(ls_col_index+".name")
	
	dw_lista.modify(ls_column_name+'.protect=1')
next
end subroutine

public function integer wf_crea_dettagli (ref string fs_errore);long						ll_index, ll_tot, ll_prog_prog_riga_fat_ven, ll_prog_stock, ll_anno_registrazione_mov_mag, ll_num_registrazione_mov_mag, &
							ll_anno_reg_des_mov, ll_num_reg_des_mov

string						ls_cod_tipo_det_acq, ls_cod_misura, ls_cod_prodotto, ls_des_prodotto, ls_cod_iva, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, &
							ls_cod_tipo_movimento, ls_nota_dettaglio, ls_flag_st_note_det, ls_flag_cc_manuali, ls_cod_prodotto_grezzo, ls_des_prodotto_grezzo, &
							ls_selezionato
							
datetime					ldt_data_stock

decimal					ld_quan_fatturata, ld_prezzo_vendita, ld_fat_conversione_ven, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, &
							ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_num_confezioni, ld_num_pezzi_confezione, ld_imponibile_iva, &
							ld_imponibile_iva_valuta

dw_lista.accepttext()
ll_tot = dw_lista.rowcount()

for ll_index=1 to ll_tot
	
	ls_selezionato = dw_lista.getitemstring(ll_index, "selezionato")
	if ls_selezionato<>"S" then continue
	
	ll_prog_prog_riga_fat_ven = dw_lista.getitemnumber(ll_index, "prog_riga_fat_ven")
	ls_cod_tipo_det_acq = dw_lista.getitemstring(ll_index, "cod_tipo_det_acq")
	
	ls_cod_misura = dw_lista.getitemstring(ll_index, "cod_misura")
	if ls_cod_misura="" then setnull(ls_cod_misura)
	
	ls_cod_prodotto = dw_lista.getitemstring(ll_index, "cod_prodotto")
	if ls_cod_prodotto="" then setnull(ls_cod_prodotto)
	
	ls_des_prodotto = dw_lista.getitemstring(ll_index, "des_prodotto")
	ld_quan_fatturata = dw_lista.getitemdecimal(ll_index, "quan_fatturata")
	ld_prezzo_vendita = dw_lista.getitemdecimal(ll_index, "prezzo_vendita")
	ld_fat_conversione_ven = dw_lista.getitemdecimal(ll_index, "fat_conversione_ven")
	ld_sconto_1 = dw_lista.getitemdecimal(ll_index, "sconto_1")
	ld_sconto_2 = dw_lista.getitemdecimal(ll_index, "sconto_2")
	ld_sconto_3 = dw_lista.getitemdecimal(ll_index, "sconto_3")
	ld_sconto_4 = dw_lista.getitemdecimal(ll_index, "sconto_4")
	ld_sconto_5 = dw_lista.getitemdecimal(ll_index, "sconto_5")
	ld_sconto_6 = dw_lista.getitemdecimal(ll_index, "sconto_6")
	ld_sconto_7 = dw_lista.getitemdecimal(ll_index, "sconto_7")
	ld_sconto_8 = dw_lista.getitemdecimal(ll_index, "sconto_8")
	ld_sconto_9 = dw_lista.getitemdecimal(ll_index, "sconto_9")
	ld_sconto_10 = dw_lista.getitemdecimal(ll_index, "sconto_10")
	
	//ls_cod_iva = dw_lista.getitemstring(ll_index, "cod_iva")
	ls_cod_iva = dw_lista.getitemstring(ll_index, "cod_iva_acq")
	if ls_cod_iva="" or isnull(ls_cod_iva) then
		fs_errore = "Iva da applicare non selezionata per la riga "+string(ll_prog_prog_riga_fat_ven)
		return -1
	end if
	
	//dati dello stock ------------------------
	ls_cod_deposito = dw_lista.getitemstring(ll_index, "cod_deposito_acq")
	ls_cod_ubicazione = dw_lista.getitemstring(ll_index, "cod_ubicazione_acq")
	ls_cod_lotto = dw_lista.getitemstring(ll_index, "cod_lotto_acq")
	ldt_data_stock = dw_lista.getitemdatetime(ll_index, "data_stock_acq")
	ll_prog_stock = dw_lista.getitemnumber(ll_index, "prog_stock_acq")
	
	//tipo movimento
	ls_cod_tipo_movimento = dw_lista.getitemstring(ll_index, "cod_tipo_mov_acq")
	setnull(ll_anno_registrazione_mov_mag)	
	setnull(ll_num_registrazione_mov_mag)
	
	ls_nota_dettaglio = dw_lista.getitemstring(ll_index, "nota_dettaglio")
	
	setnull(ll_anno_reg_des_mov)
	setnull(ll_num_reg_des_mov)
	
	ld_num_confezioni = dw_lista.getitemdecimal(ll_index, "num_confezioni")
	ld_num_pezzi_confezione = dw_lista.getitemdecimal(ll_index, "num_pezzi_confezione")
	ls_flag_st_note_det = dw_lista.getitemstring(ll_index, "flag_st_note_det")
	
	ld_imponibile_iva = dw_lista.getitemdecimal(ll_index, "imponibile_iva")
	ld_imponibile_iva_valuta = dw_lista.getitemdecimal(ll_index, "imponibile_iva_valuta")
	
	ls_cod_prodotto_grezzo = dw_lista.getitemstring(ll_index, "cod_prodotto_grezzo_acq")
	ls_des_prodotto_grezzo = dw_lista.getitemstring(ll_index, "des_prodotto_grezzo_acq")
	if ls_cod_prodotto_grezzo="" then setnull(ls_cod_prodotto_grezzo)
	if ls_des_prodotto_grezzo="" then setnull(ls_des_prodotto_grezzo)
	
	
	ls_flag_cc_manuali = "N"  //dw_lista.getitemstring(ll_index, "flag_cc_manuali")
	
	//---------------------------------------------------------------------
	insert into det_fat_acq  
			( cod_azienda,   
			anno_registrazione,   
			num_registrazione,   
			prog_riga_fat_acq,   
			anno_bolla_acq,   
			num_bolla_acq,   
			prog_riga_bolla_acq,   
			cod_tipo_det_acq,   
			cod_misura,   
			cod_prodotto,   
			des_prodotto,   
			quan_fatturata,   
			prezzo_acquisto,   
			fat_conversione,   
			sconto_1,   
			sconto_2,   
			sconto_3,   
			cod_iva,   
			cod_deposito,   
			cod_ubicazione,   
			cod_lotto,   
			data_stock,   
			cod_tipo_movimento,   
			anno_registrazione_mov_mag,   
			num_registrazione_mov_mag,   
			cod_conto,   
			nota_dettaglio,   
			flag_accettazione,   
			anno_commessa,   
			num_commessa,   
			cod_centro_costo,   
			sconto_4,   
			sconto_5,   
			sconto_6,   
			sconto_7,   
			sconto_8,   
			sconto_9,   
			sconto_10,   
			prog_stock,   
			anno_reg_des_mov,   
			num_reg_des_mov,   
			anno_reg_ord_acq,   
			num_reg_ord_acq,   
			prog_riga_ordine_acq,   
			anno_reg_mov_mag_ret,   
			num_reg_mov_mag_ret,   
			num_confezioni,   
			num_pezzi_confezione,   
			flag_st_note_det,   
			prog_bolla_acq,   
			quan_sconto_merce,   
			prezzo_acq_netto,   
			importo_conai,   
			importo_conai_scon,   
			flag_memo_prezzo_acq,   
			imponibile_iva,   
			imponibile_iva_valuta,   
			cod_prodotto_grezzo,   
			des_prodotto_grezzo,   
			val_grezzo,   
			flag_cc_manuali,   
			flag_imponibile_forzato,   
			flag_agg_mov,   
			anno_reg_mov_mag_grezzo,   
			num_reg_mov_mag_grezzo,   
			anno_reg_mov_mag_terzista,   
			num_reg_mov_mag_terzista )  
	values ( 	:s_cs_xx.cod_azienda,   
				:il_anno_reg_fat_acq,   
				:il_num_reg_fat_acq,   
				:ll_prog_prog_riga_fat_ven,   
				null,   										//anno_bolla_acq
				null,   										//num_bolla_acq
				null,   										//prog_riga_bolla_acq
				:ls_cod_tipo_det_acq,
				:ls_cod_misura,   
				:ls_cod_prodotto,   
				:ls_des_prodotto,   
				:ld_quan_fatturata,   
				:ld_prezzo_vendita,   						//prezzo_acquisto
				:ld_fat_conversione_ven,	   				//fat_conversione
				:ld_sconto_1,   
				:ld_sconto_2,   
				:ld_sconto_3,   
				:ls_cod_iva,    
				:ls_cod_deposito,   
				:ls_cod_ubicazione,   
				:ls_cod_lotto,   
				:ldt_data_stock,   
				:ls_cod_tipo_movimento,   
				:ll_anno_registrazione_mov_mag,   
				:ll_num_registrazione_mov_mag,   
				null,   										//cod_conto
				:ls_nota_dettaglio,   
				'N',   											//flag_accettazione
				null,											//anno_commessa   
				null,											//num_commessa   
				null,											//cod_centro_costo   
				:ld_sconto_4,   
				:ld_sconto_5,   
				:ld_sconto_6,   
				:ld_sconto_7,   
				:ld_sconto_8,   
				:ld_sconto_9,   
				:ld_sconto_10,   
				:ll_prog_stock,   
				:ll_anno_reg_des_mov,   
				:ll_num_reg_des_mov,   
				null,   										//anno_reg_ord_acq
				null,   										//num_reg_ord_acq
				null,   										//prog_riga_ordine_acq
				null,   										//anno_reg_mov_mag_ret
				null,   										//num_reg_mov_mag_ret
				:ld_num_confezioni,   
				:ld_num_pezzi_confezione,   
				:ls_flag_st_note_det,   
				null,   										//prog_bolla_acq
				0,   											//quan_sconto_merce
				:ld_prezzo_vendita,						//prezzo_acq_netto
				0,   											//importo_conai
				0,   											//importo_conai_scon
				'N',   											//flag_memo_prezzo_acq
				:ld_imponibile_iva,   
				:ld_imponibile_iva_valuta,   
				:ls_cod_prodotto_grezzo,
				:ls_des_prodotto_grezzo,
				0,   											//val_grezzo
				:ls_flag_cc_manuali,   
				'N',   											//flag_imponibile_forzato
				'N',   											//flag_agg_mov
				null,											//anno_reg_mov_mag_grezzo,   
				null,											//num_reg_mov_mag_grezzo,   
				null,											//anno_reg_mov_mag_terzista,   
				null)											//num_reg_mov_mag_terzista
	using sqlca;

	if sqlca.sqlcode<0 then
		fs_errore = "Errore inserimento dettaglio, riga "+string(ll_prog_prog_riga_fat_ven) + " : "+sqlca.sqlerrtext
		return -1
	end if
	
	//---------------------------------------------------------------------
	
next

//se arrivi fin qui crea i dest mov
if wf_crea_dest_mov_fat_acq(il_anno_reg_fat_acq, il_num_reg_fat_acq, fs_errore) < 0 then
	//in fs_errore il messaggio
	return -1
end if

return 1
end function

public function integer wf_crea_dest_mov_fat_acq (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio);string ls_cod_pagamento, ls_cod_tipo_det_acq, ls_cod_deposito[], ls_cod_ubicazione[], &
		 ls_cod_lotto[], ls_cod_prodotto, ls_cod_tipo_movimento, ls_flag_tipo_det_acq, &
		 ls_cod_cliente[], ls_cod_fornitore[], ls_cod_misura, &
		 ls_cod_for_acc_mat, ls_rif_fat_acq, ls_rif_ord_acq, ls_flag_agg_costo_ultimo,&
		 ls_cod_prodotto_raggruppato, ls_flag_acc_materiali
		 
long ll_prog_riga, ll_progr_stock[], ll_anno_reg_des_mov, ll_num_reg_des_mov, &
	  ll_anno_reg_mov_mag[], ll_num_reg_mov_mag[], ll_num_reg_ord_acq, ll_prog_ord_acq, &
	  ll_prog_bolla_acq, ll_prog_riga_bolla_acq, ll_anno_esercizio, ll_num_acc_materiali,&
	  ll_num_bolla_acq
	  
integer li_anno_reg_ord_acq, li_anno_bolla_acq

dec{4} ld_sconto_testata, ld_cambio_acq, ld_quantita, ld_prezzo_acquisto, ld_sconto_1, &
       ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, &
		 ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_pagamento, ld_val_sconto_1, &
		 ld_val_riga_sconto_1, ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, &
		 ld_val_riga_sconto_3, ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, &
		 ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, &
		 ld_val_riga_sconto_7, ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, &
		 ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, &
		 ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_riga_netto, ld_quan_ordinata, &
		 ld_num_pezzi_confezione, ld_quan_sconto_merce, ld_quan_raggruppo
		 
datetime ldt_data_stock[], ldt_data_documento

uo_magazzino luo_mag


//if fs_flag_acc_materiali = "?" then
//	if g_mb.messagebox("Conferma Fatture di Acquisto", "Vuoi Registrare un'Accettazione Materiali?", &
//						question!, yesno!) = 1 then
//		fs_flag_acc_materiali = "S"
//	else
//		fs_flag_acc_materiali = "N"
//	end if
//end if
ls_flag_acc_materiali = "N"


ll_anno_esercizio = f_anno_esercizio()

select cod_fornitore,
		 data_doc_origine,
		 cambio_acq,
		 cod_pagamento,
		 sconto
into   :ls_cod_for_acc_mat, 
		 :ldt_data_documento, 
		 :ld_cambio_acq,
		 :ls_cod_pagamento,
		 :ld_sconto_testata
from 	 tes_fat_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;

if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore durante la lettura Testata: "+sqlca.sqlerrtext
	rollback;
	return -1
end if

select sconto
into   :ld_sconto_pagamento
from   tab_pagamenti
where  cod_azienda = :s_cs_xx.cod_azienda and 
		 cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	ld_sconto_pagamento = 0
end if

declare cu_det cursor for 

select  prog_riga_fat_acq ,
  		  cod_misura,
		  cod_tipo_det_acq,
		  cod_deposito,
		  cod_ubicazione,
		  cod_lotto,
		  data_stock,
		  prog_stock,
		  cod_prodotto,
		  quan_fatturata,
		  prezzo_acquisto,
		  sconto_1,
		  sconto_2,
		  sconto_3,
		  sconto_4,
		  sconto_5,
		  sconto_6,
		  sconto_7,
		  sconto_8,
		  sconto_9,
		  sconto_10,
		  cod_tipo_movimento,
		  anno_reg_des_mov,
		  num_reg_des_mov,
		  anno_reg_ord_acq,
		  num_reg_ord_acq,
		  prog_riga_ordine_acq,
		  anno_bolla_acq,
		  num_bolla_acq,
		  prog_bolla_acq,
		  prog_riga_bolla_acq,
		  num_pezzi_confezione,
		  quan_sconto_merce
from det_fat_acq  
where   cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione
order by prog_riga_fat_acq asc;

open cu_det;

do while 0 = 0
	fetch cu_det into :ll_prog_riga,   
							:ls_cod_misura,
						   :ls_cod_tipo_det_acq,   
						   :ls_cod_deposito[1],   
						   :ls_cod_ubicazione[1],   
						   :ls_cod_lotto[1],   
						   :ldt_data_stock[1],   
						   :ll_progr_stock[1],   
						   :ls_cod_prodotto,   
						   :ld_quantita,   
						   :ld_prezzo_acquisto,   
						   :ld_sconto_1,   
						   :ld_sconto_2,  
						   :ld_sconto_3,   
						   :ld_sconto_4,   
						   :ld_sconto_5,   
						   :ld_sconto_6,   
						   :ld_sconto_7,   
						   :ld_sconto_8,   
						   :ld_sconto_9,   
						   :ld_sconto_10,
						   :ls_cod_tipo_movimento,
						   :ll_anno_reg_des_mov,
							:ll_num_reg_des_mov,
							:li_anno_reg_ord_acq,
							:ll_num_reg_ord_acq,
							:ll_prog_ord_acq,
							:li_anno_bolla_acq,
							:ll_num_bolla_acq,
							:ll_prog_bolla_acq,
							:ll_prog_riga_bolla_acq,
							:ld_num_pezzi_confezione, 
							:ld_quan_sconto_merce;

	if sqlca.sqlcode = 100 then exit
	
	if sqlca.sqlcode = -1 then
		fs_messaggio = "Errore durante la lettura Dettagli: "+sqlca.sqlerrtext
		rollback;
		close cu_det;
		return -1
	end if
	
	// enme 08/1/2006 gestione prodotto raggruppato
	setnull(ls_cod_prodotto_raggruppato)
	
	select cod_prodotto_raggruppato
	into   :ls_cod_prodotto_raggruppato
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_cod_prodotto;
			 
	if not isnull(ls_cod_prodotto_raggruppato) then
		ld_quantita = f_converti_qta_raggruppo(ls_cod_prodotto, ld_quantita, "M")
		ld_prezzo_acquisto = f_converti_qta_raggruppo(ls_cod_prodotto, ld_prezzo_acquisto, "D")
		//ls_cod_prodotto = ls_cod_prodotto_raggruppato
	end if
	

	if ld_quan_sconto_merce > 0 and ld_num_pezzi_confezione > 0 then
		ld_quantita = ld_quantita + (ld_quan_sconto_merce * ld_num_pezzi_confezione)
	end if

	select tab_tipi_det_acq.flag_tipo_det_acq
	into   :ls_flag_tipo_det_acq
	from   tab_tipi_det_acq
	where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	if sqlca.sqlcode = -1 then
		fs_messaggio = "Si è verificato un errore in fase lettura tabella tipi dettaglio acquisto: "+sqlca.sqlerrtext
		rollback;
		close cu_det;
		return -1
	end if

	if ls_flag_tipo_det_acq = "M" and &
		(isnull(li_anno_bolla_acq) or li_anno_bolla_acq = 0) and &
		(isnull(ll_num_bolla_acq) or ll_num_bolla_acq = 0) and &
		(isnull(ll_prog_bolla_acq) or ll_prog_bolla_acq = 0 ) and &
		(isnull(ll_prog_riga_bolla_acq) or ll_prog_riga_bolla_acq = 0 ) then

		ld_val_sconto_1 = (ld_prezzo_acquisto * ld_sconto_1) / 100
		ld_val_riga_sconto_1 = ld_prezzo_acquisto - ld_val_sconto_1
		ld_val_sconto_2 = (ld_val_riga_sconto_1 * ld_sconto_2) / 100
		ld_val_riga_sconto_2 = ld_val_riga_sconto_1 - ld_val_sconto_2
		ld_val_sconto_3 = (ld_val_riga_sconto_2 * ld_sconto_3) / 100
		ld_val_riga_sconto_3 = ld_val_riga_sconto_2 - ld_val_sconto_3
		ld_val_sconto_4 = (ld_val_riga_sconto_3 * ld_sconto_4) / 100
		ld_val_riga_sconto_4 = ld_val_riga_sconto_3 - ld_val_sconto_4
		ld_val_sconto_5 = (ld_val_riga_sconto_4 * ld_sconto_5) / 100
		ld_val_riga_sconto_5 = ld_val_riga_sconto_4 - ld_val_sconto_5
		ld_val_sconto_6 = (ld_val_riga_sconto_5 * ld_sconto_6) / 100
		ld_val_riga_sconto_6 = ld_val_riga_sconto_5 - ld_val_sconto_6
		ld_val_sconto_7 = (ld_val_riga_sconto_6 * ld_sconto_7) / 100
		ld_val_riga_sconto_7 = ld_val_riga_sconto_6 - ld_val_sconto_7
		ld_val_sconto_8 = (ld_val_riga_sconto_7 * ld_sconto_8) / 100
		ld_val_riga_sconto_8 = ld_val_riga_sconto_7 - ld_val_sconto_8
		ld_val_sconto_9 = (ld_val_riga_sconto_8 * ld_sconto_9) / 100
		ld_val_riga_sconto_9 = ld_val_riga_sconto_8 - ld_val_sconto_9
		ld_val_sconto_10 = (ld_val_riga_sconto_9 * ld_sconto_10) / 100
		ld_val_riga_sconto_10 = ld_val_riga_sconto_9 - ld_val_sconto_10
		ld_val_sconto_testata = (ld_val_riga_sconto_10 * ld_sconto_testata) / 100
		ld_val_riga_sconto_testata = ld_val_riga_sconto_10 - ld_val_sconto_testata
		ld_val_sconto_pagamento = (ld_val_riga_sconto_testata * ld_sconto_pagamento) / 100
		ld_val_riga_netto = ld_val_riga_sconto_testata - ld_val_sconto_pagamento
		ld_val_riga_netto = round((ld_val_riga_netto * ld_cambio_acq), 4)

		ls_cod_fornitore[1] = ls_cod_for_acc_mat
		setnull(ls_cod_cliente[1])

		if ll_anno_reg_des_mov = 0 or isnull(ll_anno_reg_des_mov) or &
			ll_num_reg_des_mov = 0 or isnull(ll_num_reg_des_mov) then
			if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento, &
													ls_cod_prodotto, &
													ls_cod_deposito[], &
													ls_cod_ubicazione[], &
													ls_cod_lotto[], &
													ldt_data_stock[], &
													ll_progr_stock[], &
													ls_cod_cliente[], &
													ls_cod_fornitore[], &
													ll_anno_reg_des_mov, &
													ll_num_reg_des_mov ) = -1 then
				fs_messaggio = "Riga "+string(ll_prog_riga)+": Si è verificato un errore in fase di Creazione Destinazioni Movimenti."
				rollback;
				close cu_det;
				return -1
			end if
	
			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, &
											 ll_num_reg_des_mov, &
											 ls_cod_tipo_movimento, &
											 ls_cod_prodotto) = -1 then
				fs_messaggio =  "Riga "+string(ll_prog_riga)+": Si è verificato un errore in fase di Verifica Destinazioni Movimenti."
				rollback;
				close cu_det;
				return -1
			end if
		end if
		
//		// **** vedo se aggiornare o no il costo ultimo
//		select flag_agg_costo_ultimo
//		into   :ls_flag_agg_costo_ultimo
//		from   det_ord_acq
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//		       anno_registrazione = :li_anno_reg_ord_acq and
//				 num_registrazione =	:ll_num_reg_ord_acq and
//				 prog_riga_ordine_acq = :ll_prog_ord_acq;
//		
//		if sqlca.sqlcode < 0 then
//			fs_messaggio = "Si è verificato un errore in fase di lettura Aggiorna Costo Ultimo. " + sqlca.sqlerrtext
//			rollback;
//			close cu_det;
//			return -1			
//		end if
//		
//		luo_mag = create uo_magazzino
//
//		if ls_flag_agg_costo_ultimo = "S" then
//			luo_mag.ib_flag_agg_costo_ultimo = true
//		else
//			luo_mag.ib_flag_agg_costo_ultimo = false
//		end if
//		
//		if luo_mag.uof_movimenti_mag(ldt_data_documento, &
//								 ls_cod_tipo_movimento, &
//								 'N', &
//								 ls_cod_prodotto, &
//								 ld_quantita, &
//								 ld_val_riga_netto, &
//								 ll_prog_ord_acq, &
//								 ldt_data_documento, &
//								 "", &
//								 ll_anno_reg_des_mov, &
//								 ll_num_reg_des_mov, &
//								 ls_cod_deposito[], &
//								 ls_cod_ubicazione[], &
//								 ls_cod_lotto[], &
//								 ldt_data_stock[], &
//								 ll_progr_stock[], &
//								 ls_cod_fornitore[], &
//								 ls_cod_cliente[], &
//								 ll_anno_reg_mov_mag[], &
//								 ll_num_reg_mov_mag[]) = -1 then
//			destroy luo_mag
//			fs_messaggio = "Si è verificato un errore in fase di generazione movimenti."
//			rollback;
//			close cu_det;
//			return -1
//		end if
//		
//
//		if f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
//			fs_messaggio = "Si è verificato un errore in fase di cancellazione destinazione movimenti."
//			rollback;
//			close cu_det;
//			return -1
//		end if
//		
//		
//		// stefanop: 15/11/2011: movimento prodotto raggruppato
//		long ll_anno_reg_mov_mag_rag[], ll_num_reg_mov_mag_rag[], ll_null
//		setnull(ll_null)
//		
//		if luo_mag.uof_movimenti_mag_ragguppato(ldt_data_documento, &
//								 ls_cod_tipo_movimento, &
//								 'N', &
//								 ls_cod_prodotto, &
//								 ld_quantita, &
//								 ld_val_riga_netto, &
//								 ll_prog_ord_acq, &
//								 ldt_data_documento, &
//								 "", &
//								 ll_anno_reg_des_mov, &
//								 ll_num_reg_des_mov, &
//								 ls_cod_deposito[], &
//								 ls_cod_ubicazione[], &
//								 ls_cod_lotto[], &
//								 ldt_data_stock[], &
//								 ll_progr_stock[], &
//								 ls_cod_fornitore[], &
//								 ls_cod_cliente[], &
//								 ll_anno_reg_mov_mag_rag[], &
//								 ll_num_reg_mov_mag_rag[],&
//								 ll_null,&
//								 ll_anno_reg_mov_mag[1],&
//								 ll_num_reg_mov_mag[1]) = -1 then
//								 
//			fs_messaggio = "Si è verificato nella gestione dei movimenti del prodotto raggruppato."
//			rollback;
//			close cu_det;
//			destroy luo_mag
//			return -1
//		end if	
//		// ----
		
		
		destroy luo_mag

//		update det_fat_acq
//		set 	anno_registrazione_mov_mag = :ll_anno_reg_mov_mag[1] ,
//			 	num_registrazione_mov_mag = :ll_num_reg_mov_mag[1],
//				cod_deposito = :ls_cod_deposito[1],
//				cod_ubicazione = :ls_cod_ubicazione[1],
//				cod_lotto = :ls_cod_lotto[1],
//				data_stock = :ldt_data_stock[1],
//				prog_stock = :ll_progr_stock[1],
//				flag_accettazione = :ls_flag_acc_materiali
//		where cod_azienda = :s_cs_xx.cod_azienda and
//				anno_registrazione = :fl_anno_registrazione and
//				num_registrazione = :fl_num_registrazione and 
//				prog_riga_fat_acq = :ll_prog_riga;
//
//		if sqlca.sqlcode = -1 then
//			fs_messaggio = "Si è verificato un errore in fase di aggiornamento dettaglio: "+sqlca.sqlerrtext
//			rollback;
//			close cu_det;
//			return -1
//		end if
		
//		if ls_flag_acc_materiali = "S" then
//			select max(num_acc_materiali)  
//				into :ll_num_acc_materiali  
//				from acc_materiali
//				where cod_azienda = :s_cs_xx.cod_azienda and  
//						anno_acc_materiali = :ll_anno_esercizio;
//
//			if sqlca.sqlcode <> 0 then
//				fs_messaggio = "Errore durante la lettura Accettazione Materiali."
//				rollback;
//				return -1
//			end if
//
//			if ll_num_acc_materiali = 0 or isnull(ll_num_acc_materiali) then
//				ll_num_acc_materiali = 1
//			else
//				ll_num_acc_materiali ++
//			end if
//	
//			ls_rif_fat_acq = "Riferimento Fattura: " + string(fl_anno_registrazione) + "/" + string(fl_num_registrazione)
//			ls_rif_ord_acq = "Riferimento Ordine: " + string(li_anno_reg_ord_acq) + "/" + string(ll_num_reg_ord_acq)+ &
//									"/" + string(ll_prog_ord_acq)
//			
//			insert into acc_materiali
//					(cod_azienda,
//					anno_acc_materiali,
//					num_acc_materiali,
//					cod_fornitore,
//					cod_prodotto,
//					cod_prod_fornitore,
//					cod_misura,
//					quan_arrivata,
//					prezzo_acquisto,
//					rif_ord_acq,
//					rif_bol_acq,
//					rif_fat_acq,
//					data_prev_consegna,
//					data_eff_consegna,
//					cod_deposito,
//					cod_ubicazione,
//					cod_lotto,
//					data_stock,
//					prog_stock,
//					nome_doc_compilato,
//					flag_cq,
//					nome_doc_cq,
//					anno_registrazione,
//					num_registrazione,
//					prog_riga_ordine_acq,
//					anno_bolla_acq,
//					num_bolla_acq,
//					progressivo,
//					prog_riga_bolla_acq,
//					anno_reg_fat_acq,
//					num_reg_fat_acq,
//					prog_riga_fat_acq,
//					anno_reg_campionamenti,
//					num_reg_campionamenti,
//					anno_non_conf,
//					num_non_conf,
//					flag_esito_campionamento)
//			values ( :s_cs_xx.cod_azienda,   
//					:ll_anno_esercizio,   
//					:ll_num_acc_materiali,   
//					:ls_cod_for_acc_mat,   
//					:ls_cod_prodotto,   
//					null,   
//					:ls_cod_misura,   
//					:ld_quantita,   
//					:ld_prezzo_acquisto,   
//					:ls_rif_ord_acq,   
//					null,   
//					:ls_rif_fat_acq,   
//					null,   
//					null,   
//					:ls_cod_deposito[1],   
//					:ls_cod_ubicazione[1],   
//					:ls_cod_lotto[1],   
//					:ldt_data_stock[1],   
//					:ll_progr_stock[1],   
//					null,   
//					null,   
//					null,   
//					:li_anno_reg_ord_acq,
//					:ll_num_reg_ord_acq,
//					:ll_prog_ord_acq,
//					null,
//					null,
//					null,
//					null,
//					:fl_anno_registrazione,
//					:fl_num_registrazione,
//					:ll_prog_riga,
//					null,
//					null,
//					null,
//					null,
//					'D');
//			if sqlca.sqlcode <> 0 then
//				fs_messaggio = "Errore durante la scrittura Accettazione Materiali."
//				rollback;
//				return -1
//			end if
//		end if

	end if
loop

close cu_det;

//update tes_fat_acq
//set    flag_fat_confermata = 'S'
//where  cod_azienda = :s_cs_xx.cod_azienda and
//		 anno_registrazione = :fl_anno_registrazione and
//		 num_registrazione = :fl_num_registrazione;
//
//if sqlca.sqlcode = -1 then
//	fs_messaggio = "Si è verificato un errore in fase di aggiornamento testata."
//	rollback;
//	return -1
//end if

return 1
end function

public function integer wf_tipo_det_acq (long fl_row, string fs_cod_tipo_det_acq, boolean fb_itemchanged, ref string fs_errore);string			ls_flag_tipo_det_acq, ls_cod_tipo_movimento, ls_cod_iva, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_cod_deposito_dddw

datetime		ldt_data_stock

long			ll_ordinamento, ll_prog_stock


ls_cod_deposito_dddw = dw_tipo_det_acq.getitemstring(1, "cod_deposito")
if ls_cod_deposito_dddw="" then setnull(ls_cod_deposito_dddw)

setnull(ls_cod_tipo_movimento)
setnull(ls_cod_deposito)
setnull(ls_cod_ubicazione)
setnull(ls_cod_lotto)
setnull(ldt_data_stock)
setnull(ll_prog_stock)

if fs_cod_tipo_det_acq="" or isnull(fs_cod_tipo_det_acq) then
	dw_lista.setitem(fl_row,"cod_tipo_mov_acq" ,ls_cod_tipo_movimento)
	dw_lista.setitem(fl_row,"cod_deposito_acq", ls_cod_deposito)
	dw_lista.setitem(fl_row,"cod_ubicazione_acq",ls_cod_ubicazione)
	dw_lista.setitem(fl_row,"cod_lotto_acq", ls_cod_lotto)
	dw_lista.setitem(fl_row,"data_stock_acq", ldt_data_stock)
	dw_lista.setitem(fl_row,"prog_stock_acq", ll_prog_stock)
	
	return 1
end if

select flag_tipo_det_acq,
		cod_tipo_movimento
into   	:ls_flag_tipo_det_acq,
		:ls_cod_tipo_movimento
from   tab_tipi_det_acq
where  cod_azienda = :s_cs_xx.cod_azienda and 
		  cod_tipo_det_acq = :fs_cod_tipo_det_acq
using sqlca;

if sqlca.sqlcode<0 then
	fs_errore = "Errore in lettura tipi dettaglio: "+sqlca.sqlerrtext
	return -1
end if

if ls_cod_tipo_movimento="" then setnull(ls_cod_tipo_movimento)

if ls_flag_tipo_det_acq = "M" then
	ll_ordinamento = 0
	
	select min(ordinamento)
	into   :ll_ordinamento
	from   det_tipi_movimenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_movimento = :ls_cod_tipo_movimento;
			 
	if ll_ordinamento <> 0 and not isnull(ll_ordinamento) then
		
		select cod_deposito,
				 cod_ubicazione,
				 cod_lotto,
				 data_stock,
				 prog_stock
		into   :ls_cod_deposito,
				 :ls_cod_ubicazione,
				 :ls_cod_lotto,
				 :ldt_data_stock,
				 :ll_prog_stock
		from   det_tipi_movimenti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_movimento = :ls_cod_tipo_movimento and
				 ordinamento = :ll_ordinamento;
				 
		if ls_cod_deposito="" then setnull(ls_cod_deposito)
		if ls_cod_ubicazione="" then setnull(ls_cod_ubicazione)
		if ls_cod_lotto="" then setnull(ls_cod_lotto)
		if year(date(ldt_data_stock))<1950 then setnull(ldt_data_stock)
		if ll_prog_stock<=0 then setnull(ll_prog_stock)
		
	end if
	
else
	setnull(ls_cod_tipo_movimento)
end if

dw_lista.setitem(fl_row,"cod_tipo_mov_acq", ls_cod_tipo_movimento)

if not isnull(ls_cod_deposito_dddw) and not fb_itemchanged then
	if not fb_itemchanged then
		dw_lista.setitem(fl_row,"cod_deposito_acq", ls_cod_deposito_dddw)
	else
		dw_lista.setitem(fl_row,"cod_deposito_acq", ls_cod_deposito)
	end if
else
	dw_lista.setitem(fl_row,"cod_deposito_acq", ls_cod_deposito)
end if

dw_lista.setitem(fl_row,"cod_ubicazione_acq",ls_cod_ubicazione)
dw_lista.setitem(fl_row,"cod_lotto_acq", ls_cod_lotto)
dw_lista.setitem(fl_row,"data_stock_acq", ldt_data_stock)
dw_lista.setitem(fl_row,"prog_stock_acq", ll_prog_stock)

//setnull(ls_cod_iva)
//
//select cod_iva  
//into   :ls_cod_iva  
//from   tab_tipi_det_acq  
//where  cod_azienda = :s_cs_xx.cod_azienda and  
//		 cod_tipo_det_acq = :fs_cod_tipo_det_acq;
//		 
//if sqlca.sqlcode = 0 then
//	dw_lista.setitem(fl_row, "cod_iva", ls_cod_iva)
//end if


return 1
end function

public function integer wf_prodotto (long fl_row, string fs_cod_prodotto, ref string fs_errore);

string				ls_cod_misura_mag, ls_cod_misura, ls_cod_iva, ls_cod_prodotto_alt, ls_des_prodotto_alt


 select 	cod_misura_mag,
			cod_misura_acq,
			cod_iva,
			cod_prodotto_alt
into   		 :ls_cod_misura_mag,
			 :ls_cod_misura,
			 :ls_cod_iva,
			 :ls_cod_prodotto_alt
from   anag_prodotti
where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_prodotto = :fs_cod_prodotto;
			 
if sqlca.sqlcode = 0 then
	//se esiste il codice alternativo lo metto nel prodotto grezzo
	if not isnull(ls_cod_prodotto_alt) then

		select des_prodotto			       
		into   :ls_des_prodotto_alt
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto_alt;
							 
		if sqlca.sqlcode < 0 then
			fs_errore = "Errore durante la ricerca del prodotto alternativo: " + sqlca.sqlerrtext
			return -1
		end if
					
		dw_lista.setitem(fl_row, "cod_prodotto_grezzo_acq", ls_cod_prodotto_alt)
		dw_lista.setitem(fl_row, "des_prodotto_grezzo_acq", ls_des_prodotto_alt)
				
	end if
				
	 //dw_lista.setitem(fl_row, "cod_misura", ls_cod_misura_mag)
	//if not isnull(ls_cod_iva) then dw_lista.setitem(fl_row, "cod_iva", ls_cod_iva)
end if

return 1
end function

public function integer wf_select_det_fat_ven (string fs_cod_documento, integer fi_anno_documento, string fs_numeratore_documento, long fl_num_documento, ref integer fi_anno_fat_ven, ref long fl_num_fat_ven, ref decimal fd_tot_fattura, ref string fs_errore);long ll_count
transaction lt_transaction
integer li_database
decimal ld_tot_fattura

if rb_ptenda.checked then
	li_database = 1
	
elseif rb_cgibus.checked then
	li_database = 2
	
elseif rb_viropa.checked then
	li_database = 3
	
else
	fs_errore = "Scegliere un Database!"
	return -1
end if

if wf_connetti(li_database, lt_transaction, fs_errore) < 0 then
	//in fs_errore il messaggio
	return -1
end if

//recupero anno e numero registrazione della fattura di vendita
select count(*)
into :ll_count
from tes_fat_ven
where cod_azienda=:is_cod_azienda_old and
		cod_documento=:fs_cod_documento and
		anno_documento=:fi_anno_documento and
		numeratore_documento=:fs_numeratore_documento and
		num_documento=:fl_num_documento
using lt_transaction;

if lt_transaction.sqlcode<0 then
	fs_errore = "Errore in controllo esistenza fattura di vendita! "+lt_transaction.sqlerrtext
	return -1
end if

if isnull(ll_count) then ll_count=0
if ll_count>1 then
	//incongruenza: + fatture di vendita corrispondenti alla stesa numerazione fiscale
	fs_errore = "Attenzione! Più fatture di vendita corrispondenti alla numerazione fiscale indicata!"
	return -1
	
elseif ll_count=0 then
	//Inesistente
	fs_errore = "Attenzione! Fatture di vendita INESISTENTE!"
	return -1
	
else
	//1 sola fattura di vendita: OK
end if

//prosegui con la retrieve
select anno_registrazione, num_registrazione, tot_fattura
into :fi_anno_fat_ven, :fl_num_fat_ven, :fd_tot_fattura
from tes_fat_ven
where  cod_azienda=:is_cod_azienda_old and
		cod_documento=:fs_cod_documento and
		anno_documento=:fi_anno_documento and
		numeratore_documento=:fs_numeratore_documento and
		num_documento=:fl_num_documento
using lt_transaction;

if lt_transaction.sqlcode<0 then
	fs_errore = "Errore in recupero anno/numero fattura di vendita! "+lt_transaction.sqlerrtext
	return -1
end if

if isnull(fd_tot_fattura) then fd_tot_fattura=0

dw_lista.settransobject(lt_transaction)

f_po_loaddddw_dw(dw_lista, &
                 "cod_tipo_det_ven", &
                 lt_transaction, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + is_cod_azienda_old + "'")

ll_count = dw_lista.retrieve(is_cod_azienda_old, fi_anno_fat_ven, fl_num_fat_ven)

disconnect using lt_transaction;
destroy lt_transaction;

return ll_count
end function

on w_acquista_fatt_ven.create
this.st_tot_fattura=create st_tot_fattura
this.st_tot_righe=create st_tot_righe
this.sle_dbparm=create sle_dbparm
this.st_9=create st_9
this.st_8=create st_8
this.st_7=create st_7
this.sle_dbms=create sle_dbms
this.sle_servername=create sle_servername
this.st_6=create st_6
this.st_5=create st_5
this.cb_annulla=create cb_annulla
this.dw_tipo_det_acq=create dw_tipo_det_acq
this.dw_lista=create dw_lista
this.rb_ptenda=create rb_ptenda
this.rb_cgibus=create rb_cgibus
this.rb_viropa=create rb_viropa
this.st_1=create st_1
this.cb_conferma=create cb_conferma
this.sle_cod_documento=create sle_cod_documento
this.sle_numeratore_documento=create sle_numeratore_documento
this.em_anno_documento=create em_anno_documento
this.em_num_documento=create em_num_documento
this.st_3=create st_3
this.st_4=create st_4
this.cb_seleziona=create cb_seleziona
this.r_db=create r_db
this.r_1=create r_1
this.st_2=create st_2
this.r_2=create r_2
this.r_3=create r_3
this.st_anno_fat_ven=create st_anno_fat_ven
this.st_num_fat_ven=create st_num_fat_ven
this.r_4=create r_4
this.r_5=create r_5
this.r_6=create r_6
this.Control[]={this.st_tot_fattura,&
this.st_tot_righe,&
this.sle_dbparm,&
this.st_9,&
this.st_8,&
this.st_7,&
this.sle_dbms,&
this.sle_servername,&
this.st_6,&
this.st_5,&
this.cb_annulla,&
this.dw_tipo_det_acq,&
this.dw_lista,&
this.rb_ptenda,&
this.rb_cgibus,&
this.rb_viropa,&
this.st_1,&
this.cb_conferma,&
this.sle_cod_documento,&
this.sle_numeratore_documento,&
this.em_anno_documento,&
this.em_num_documento,&
this.st_3,&
this.st_4,&
this.cb_seleziona,&
this.r_db,&
this.r_1,&
this.st_2,&
this.r_2,&
this.r_3,&
this.st_anno_fat_ven,&
this.st_num_fat_ven,&
this.r_4,&
this.r_5,&
this.r_6}
end on

on w_acquista_fatt_ven.destroy
destroy(this.st_tot_fattura)
destroy(this.st_tot_righe)
destroy(this.sle_dbparm)
destroy(this.st_9)
destroy(this.st_8)
destroy(this.st_7)
destroy(this.sle_dbms)
destroy(this.sle_servername)
destroy(this.st_6)
destroy(this.st_5)
destroy(this.cb_annulla)
destroy(this.dw_tipo_det_acq)
destroy(this.dw_lista)
destroy(this.rb_ptenda)
destroy(this.rb_cgibus)
destroy(this.rb_viropa)
destroy(this.st_1)
destroy(this.cb_conferma)
destroy(this.sle_cod_documento)
destroy(this.sle_numeratore_documento)
destroy(this.em_anno_documento)
destroy(this.em_num_documento)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.cb_seleziona)
destroy(this.r_db)
destroy(this.r_1)
destroy(this.st_2)
destroy(this.r_2)
destroy(this.r_3)
destroy(this.st_anno_fat_ven)
destroy(this.st_num_fat_ven)
destroy(this.r_4)
destroy(this.r_5)
destroy(this.r_6)
end on

event open;s_cs_xx_parametri ls_parametri
string ls_cod_tipo_fat_acq, ls_cod_tipo_det_acq

ls_parametri = message.powerobjectparm

il_anno_reg_fat_acq = ls_parametri.parametro_d_1_a[1]
il_num_reg_fat_acq = ls_parametri.parametro_d_1_a[2]

//-----------------------------------------------
is_users_db[1] = "ptt"
is_users_db[2] = "dba"
is_users_db[3] = "vrp"
//-----------------------------------------------
is_pwd_db[1] = "0670620554213519"
is_pwd_db[2] = "409873205833"
is_pwd_db[3] = "1268460162409965039682"
//-----------------------------------------------
is_db_name[1] = "cs_db_ptenda"
is_db_name[2] = "cs_db_cgibus"
is_db_name[3] = "cs_db_viropa"
//-----------------------------------------------

dw_tipo_det_acq.insertrow(0)

//carico le drop sulla dw di selezione dati default -----------
f_po_loaddddw_dw(dw_tipo_det_acq, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_tipo_det_acq, &
                 "cod_tipo_det_acq_rif", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_tipo_det_acq, &
                 "cod_tipo_det_acq_add", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_tipo_det_acq, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_tipo_det_acq, &
                 "cod_iva", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
//----------------------------------------------------------------------------------------------------


//commentato perchè carichiamo a mano sulla dw i valori di default del tipo dettaglio
//FAM per tipo det acq
//RIF per i riferimenti
//FAN per gli addizionali
//per tutte le righe che non sono addizionali e riferimenti proporrò quindi FAM

/*
select cod_tipo_fat_acq
into :ls_cod_tipo_fat_acq
from tes_fat_acq
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:il_anno_reg_fat_acq and
		num_registrazione=:il_num_reg_fat_acq
using sqlca;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in lettura tipo fattura acquisto: "+sqlca.sqlerrtext)
	return
end if

if ls_cod_tipo_fat_acq<>"" and not isnull(ls_cod_tipo_fat_acq) then
	//leggo il tipo dettaglio default per il tipo fattura acquisto
	select cod_tipo_det_acq
	into :ls_cod_tipo_det_acq
	from tab_tipi_fat_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and 
		 	cod_tipo_fat_acq = :ls_cod_tipo_fat_acq;
	
	if sqlca.sqlcode<0 then
		g_mb.error("Errore in lettura tipo dettaglio acquisto di default: "+sqlca.sqlerrtext)
		return
	end if
	
	if ls_cod_tipo_det_acq<>"" and not isnull(ls_cod_tipo_det_acq) then
		dw_tipo_det_acq.setitem(1, "cod_tipo_det_acq", ls_cod_tipo_det_acq)
	end if
	
end if
*/
//-----------------------------------


//wf_proteggi_colonne_dw()

//carico le drop sulla dw di lista ----------------------------
f_po_loaddddw_dw(dw_lista, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
	
f_po_loaddddw_dw(dw_lista, &
                 "cod_deposito_acq", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_lista, &
                 "cod_iva_acq", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//--------------------------------------------------------------------

end event

type st_tot_fattura from statictext within w_acquista_fatt_ven
integer x = 891
integer y = 456
integer width = 937
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
alignment alignment = center!
boolean focusrectangle = false
end type

type st_tot_righe from statictext within w_acquista_fatt_ven
integer x = 50
integer y = 456
integer width = 773
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Tot.Righe: 0"
alignment alignment = center!
boolean focusrectangle = false
end type

type sle_dbparm from singlelineedit within w_acquista_fatt_ven
integer x = 1947
integer y = 2140
integer width = 2240
integer height = 104
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "Release=~'15~',CharSet=~'cp850~'"
end type

type st_9 from statictext within w_acquista_fatt_ven
integer x = 1673
integer y = 2160
integer width = 261
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "DBParm:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_8 from statictext within w_acquista_fatt_ven
integer x = 960
integer y = 2160
integer width = 261
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "DBMS:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_7 from statictext within w_acquista_fatt_ven
integer x = 64
integer y = 2160
integer width = 389
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "ServerName:"
alignment alignment = right!
boolean focusrectangle = false
end type

type sle_dbms from singlelineedit within w_acquista_fatt_ven
integer x = 1234
integer y = 2140
integer width = 402
integer height = 104
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "ASE"
end type

type sle_servername from singlelineedit within w_acquista_fatt_ven
integer x = 471
integer y = 2140
integer width = 402
integer height = 104
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "ASE15"
end type

type st_6 from statictext within w_acquista_fatt_ven
integer x = 101
integer y = 2056
integer width = 1381
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Parametri fissi connessione: NON TOCCARE!!!!!"
alignment alignment = center!
boolean focusrectangle = false
end type

type st_5 from statictext within w_acquista_fatt_ven
integer x = 1531
integer y = 300
integer width = 110
integer height = 60
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "/"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_acquista_fatt_ven
integer x = 2258
integer y = 444
integer width = 384
integer height = 84
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;closewithreturn(parent, "")
end event

type dw_tipo_det_acq from datawindow within w_acquista_fatt_ven
integer x = 2702
integer y = 64
integer width = 1563
integer height = 440
integer taborder = 10
string title = "none"
string dataobject = "d_acquista_fatt_ven_tipodetacq"
boolean border = false
boolean livescroll = true
end type

event itemchanged;string				ls_appo
long				ll_index, ll_tot

if row>0 then
else
	return
end if

accepttext()

choose case dwo.name
	case "cod_iva"
		ls_appo = getitemstring(row, "cod_iva")
		if ls_appo="" then setnull(ls_appo)
		
		ll_tot = dw_lista.rowcount()
		for ll_index=1 to ll_tot
			dw_lista.setitem(ll_index, "cod_iva_acq", ls_appo)
		next
		
end choose

	
	
	
end event

type dw_lista from datawindow within w_acquista_fatt_ven
integer x = 32
integer y = 640
integer width = 4265
integer height = 1396
integer taborder = 30
string title = "none"
string dataobject = "d_det_fatven_fatacq"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event itemchanged;string				ls_cod_tipo_det_acq, ls_errore, ls_null
datetime			ldt_null
long				ll_null

if row>0 then
else
	return
end if

choose case dwo.name
	case "cod_tipo_det_acq"
		if wf_tipo_det_acq(row, data, true, ls_errore)<0 then
			g_mb.error("Riga "+string(row) + " - " + ls_errore)
		
			return
		end if
		
		if data="" or isnull(data) then
			setnull(ls_null)
			setnull(ll_null)
			setnull(ldt_null)
			
			dw_lista.setitem(row,"cod_deposito_acq", ls_null)
			dw_lista.setitem(row,"cod_ubicazione_acq",ls_null)
			dw_lista.setitem(row,"cod_lotto_acq", ls_null)
			dw_lista.setitem(row,"data_stock_acq", ldt_null)
			dw_lista.setitem(row,"prog_stock_acq", ll_null)
			dw_lista.setitem(row,"cod_tipo_mov_acq", ls_null)
		end if
		
end choose
end event

event buttonclicked;string				ls_sel, ls_text
long				ll_index, ll_tot

choose case dwo.name
	case "b_sel"
		//--------------------------------------------------------------------------------------
		ll_tot = rowcount()
		if ll_tot>0 then
		else
			return
		end if
		
		if dwo.text = "N" then
			//de-seleziona tutti
			ls_sel = "N"
			ls_text = "S"
		else
			//seleziona tutti
			ls_sel = "S"
			ls_text = "N"
		end if
		
		for ll_index=1 to ll_tot
			setitem(ll_index, "selezionato", ls_sel)
		next
		
		dwo.text = ls_text
		//--------------------------------------------------------------------------------------
		
end choose
end event

type rb_ptenda from radiobutton within w_acquista_fatt_ven
integer x = 69
integer y = 104
integer width = 549
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Progettotenda"
boolean checked = true
end type

type rb_cgibus from radiobutton within w_acquista_fatt_ven
integer x = 69
integer y = 200
integer width = 549
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Centrogibus"
end type

type rb_viropa from radiobutton within w_acquista_fatt_ven
integer x = 69
integer y = 296
integer width = 549
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Viropa"
end type

type st_1 from statictext within w_acquista_fatt_ven
integer x = 78
integer y = 28
integer width = 704
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Origine Fatture Vendita"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_conferma from commandbutton within w_acquista_fatt_ven
integer x = 1870
integer y = 444
integer width = 384
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
boolean enabled = false
string text = "Conferma"
end type

event clicked;string ls_errore

setpointer(Hourglass!)

if wf_crea_dettagli(ls_errore) < 0 then
	setpointer(Arrow!)
	g_mb.error(ls_errore)
	rollback using sqlca;
	return
end if

setpointer(Arrow!)

//se arrivi fin qui fai commit e chiudii la finestra
commit using sqlca;

closewithreturn(parent, "OK")
end event

type sle_cod_documento from singlelineedit within w_acquista_fatt_ven
integer x = 1010
integer y = 180
integer width = 247
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
end type

type sle_numeratore_documento from singlelineedit within w_acquista_fatt_ven
integer x = 1618
integer y = 180
integer width = 151
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "A"
end type

type em_anno_documento from editmask within w_acquista_fatt_ven
integer x = 1262
integer y = 180
integer width = 293
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "2012"
alignment alignment = center!
string mask = "###0"
boolean spin = true
double increment = 1
string minmax = "2000~~2020"
end type

type em_num_documento from editmask within w_acquista_fatt_ven
integer x = 1792
integer y = 180
integer width = 494
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
string text = "0"
alignment alignment = center!
string mask = "######0"
boolean spin = true
double increment = 1
string minmax = "2000~~2020"
end type

type st_3 from statictext within w_acquista_fatt_ven
integer x = 955
integer y = 100
integer width = 1381
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "es.     FV          2011          /      A                    123"
boolean focusrectangle = false
end type

type st_4 from statictext within w_acquista_fatt_ven
integer x = 914
integer y = 28
integer width = 855
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Identificazione Fattura di Vendita"
alignment alignment = center!
boolean focusrectangle = false
end type

type cb_seleziona from commandbutton within w_acquista_fatt_ven
integer x = 2299
integer y = 180
integer width = 329
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Seleziona"
end type

event clicked;string			ls_cod_documento, ls_numeratore_documento, ls_errore, ls_cod_tipo_det_acq, ls_appo, ls_cod_iva_dddw, &
				ls_cod_tipo_det_acq_dddw, ls_cod_prodotto, ls_cod_iva, ls_cod_tipo_det_acq_rif_dddw, ls_cod_tipo_det_acq_add_dddw
				
integer		li_anno_documento, li_anno_reg_fat_ven

long			ll_num_documento, ll_num_reg_fat_ven, ll_tot, ll_index

decimal		ld_tot_fattura


setpointer(Hourglass!) //----------------------------------------------------------------------------------------------------------------------------------------------

ls_cod_documento = sle_cod_documento.text
li_anno_documento = integer(em_anno_documento.text)
ls_numeratore_documento = sle_numeratore_documento.text
ll_num_documento = long(em_num_documento.text)

dw_tipo_det_acq.accepttext()

st_anno_fat_ven.text = ""
st_num_fat_ven.text = ""
st_tot_fattura.text = ""
st_tot_righe.text = "Tot.Righe: 0"
cb_conferma.enabled = false
dw_lista.object.b_sel.text = "N"
dw_lista.reset()

ll_tot = wf_select_det_fat_ven(ls_cod_documento, li_anno_documento, ls_numeratore_documento, ll_num_documento, &
								li_anno_reg_fat_ven, ll_num_reg_fat_ven, ld_tot_fattura, ls_errore)
if ll_tot<0 then
	setpointer(Arrow!)
	g_mb.error(ls_errore)
	
	return
end if

st_anno_fat_ven.text = string(li_anno_reg_fat_ven)
st_num_fat_ven.text = string(ll_num_reg_fat_ven)
st_tot_fattura.text = "Tot.Fattura € "+string(ld_tot_fattura, "#,###,###,##0.00")
st_tot_righe.text = "Tot.Righe: "+string(ll_tot)

if ll_tot>0 then
	cb_conferma.enabled = true
else
	cb_conferma.enabled = false
end if

//carico i valori di default dei tipi det acq
ls_cod_tipo_det_acq_dddw = dw_tipo_det_acq.getitemstring(1, "cod_tipo_det_acq")
ls_cod_tipo_det_acq_rif_dddw = dw_tipo_det_acq.getitemstring(1, "cod_tipo_det_acq_rif")
ls_cod_tipo_det_acq_add_dddw = dw_tipo_det_acq.getitemstring(1, "cod_tipo_det_acq_add")

if ls_cod_tipo_det_acq_dddw="" then setnull(ls_cod_tipo_det_acq_dddw)
if ls_cod_tipo_det_acq_rif_dddw="" then setnull(ls_cod_tipo_det_acq_rif_dddw)
if ls_cod_tipo_det_acq_add_dddw="" then setnull(ls_cod_tipo_det_acq_add_dddw)
//-----------------------------------------

//leggo il tipo det ven scelta dall'utente
ls_cod_iva_dddw = dw_tipo_det_acq.getitemstring(1, "cod_iva")
//-----------------------------------------

//prepara la dw
for ll_index=1 to ll_tot
	
	//FAM per tipo det acq
	//RIF per i riferimenti
	//FAN per gli addizionali (ADD)
	//per tutte le righe che non sono addizionali e riferimenti proporrò quindi FAM
	
	//leggo il tipo det ven
	ls_appo = dw_lista.getitemstring(ll_index, "cod_tipo_det_ven")
	
	choose case ls_appo
		case "LIB", "RIF", "NO1", "NO2"
			dw_lista.setitem(ll_index, "cod_tipo_det_acq", ls_cod_tipo_det_acq_rif_dddw)
			
		case "ADD", "ADZ"
			dw_lista.setitem(ll_index, "cod_tipo_det_acq", ls_cod_tipo_det_acq_add_dddw)
			
		case "TRA"  //per il trasporto metto già a trasporto
			dw_lista.setitem(ll_index, "cod_tipo_det_acq", "TRA")
		case "TR1"  //per il trasporto metto già a trasporto
			dw_lista.setitem(ll_index, "cod_tipo_det_acq", "TR1")
			
		case else
			dw_lista.setitem(ll_index, "cod_tipo_det_acq", ls_cod_tipo_det_acq_dddw)
			
	end choose
	//dw_lista.setitem(ll_index, "cod_tipo_det_acq", ls_cod_tipo_det_acq_dddw)
	//-------------------------------------------------------------------------------------------------------------
	
	//iva da applicare ------------------------------------------------------------------------------------------
	if ls_cod_iva_dddw="" then setnull(ls_cod_iva_dddw)
	dw_lista.setitem(ll_index, "cod_iva_acq", ls_cod_iva_dddw)
	//-------------------------------------------------------------------------------------------------------------
	
	
	//cod prodotto -------------------------------
	ls_cod_prodotto = dw_lista.getitemstring(ll_index, "cod_prodotto")
	if ls_cod_prodotto<>"" and not isnull(ls_cod_prodotto) then
		if wf_prodotto(ll_index, ls_cod_prodotto, ls_errore)<0 then
			setpointer(Arrow!)
			g_mb.error("Riga "+string(ll_index) + " - " + ls_errore)
	
			return
		end if
	else
//		setnull(ls_cod_iva)
//		
//		select cod_iva  
//		into   :ls_cod_iva  
//		from   tab_tipi_det_acq  
//		where  cod_azienda = :s_cs_xx.cod_azienda and  
//				 cod_tipo_det_acq = :ls_cod_tipo_det_acq_dddw;
//		if sqlca.sqlcode = 0 then
//			dw_lista.setitem(ll_index, "cod_iva", ls_cod_iva)
//		end if
	end if
	
	
	//tipo det acq --------------------------------------
	if wf_tipo_det_acq(ll_index, ls_cod_tipo_det_acq_dddw, false, ls_errore)<0 then
		setpointer(Arrow!)
		g_mb.error("Riga "+string(ll_index) + " - " + ls_errore)
	
		return
	end if
next

setpointer(Arrow!)


end event

type r_db from rectangle within w_acquista_fatt_ven
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 32
integer y = 48
integer width = 805
integer height = 360
end type

type r_1 from rectangle within w_acquista_fatt_ven
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 869
integer y = 48
integer width = 1792
integer height = 360
end type

type st_2 from statictext within w_acquista_fatt_ven
integer x = 1531
integer y = 188
integer width = 110
integer height = 60
integer textsize = -12
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 12632256
string text = "/"
alignment alignment = center!
boolean focusrectangle = false
end type

type r_2 from rectangle within w_acquista_fatt_ven
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 2688
integer y = 48
integer width = 1600
integer height = 488
end type

type r_3 from rectangle within w_acquista_fatt_ven
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 1861
integer y = 432
integer width = 800
integer height = 104
end type

type st_anno_fat_ven from statictext within w_acquista_fatt_ven
integer x = 1243
integer y = 284
integer width = 311
integer height = 88
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type st_num_fat_ven from statictext within w_acquista_fatt_ven
integer x = 1618
integer y = 284
integer width = 498
integer height = 88
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 128
long backcolor = 12632256
alignment alignment = center!
boolean border = true
boolean focusrectangle = false
end type

type r_4 from rectangle within w_acquista_fatt_ven
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 32
integer y = 2080
integer width = 4224
integer height = 220
end type

type r_5 from rectangle within w_acquista_fatt_ven
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 32
integer y = 432
integer width = 805
integer height = 104
end type

type r_6 from rectangle within w_acquista_fatt_ven
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 869
integer y = 432
integer width = 974
integer height = 104
end type

