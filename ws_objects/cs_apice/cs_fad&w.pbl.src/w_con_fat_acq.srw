﻿$PBExportHeader$w_con_fat_acq.srw
$PBExportComments$Finestra Gestione Parametri Fatture Acquisto
forward
global type w_con_fat_acq from w_cs_xx_principale
end type
type dw_con_fat_acq from uo_cs_xx_dw within w_con_fat_acq
end type
end forward

global type w_con_fat_acq from w_cs_xx_principale
integer width = 2939
integer height = 620
string title = "Gestione Parametri Fatture Acquisto"
dw_con_fat_acq dw_con_fat_acq
end type
global w_con_fat_acq w_con_fat_acq

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_con_fat_acq, &
                 "cod_documento", &
                 sqlca, &
                 "tab_documenti", &
                 "cod_documento", &
                 "des_documento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_con_fat_acq, &
                 "cod_gruppo", &
                 sqlca, &
                 "tab_gruppi", &
                 "cod_gruppo", &
                 "des_gruppo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_con_fat_acq, &
                 "cod_tipo_fat_acq", &
                 sqlca, &
                 "tab_tipi_fat_acq", &
                 "cod_tipo_fat_acq", &
                 "des_tipi_fat_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_po_loaddddw_dw(dw_con_fat_acq, &
                 "cod_tipo_movimento_grezzo", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

on pc_setwindow;call w_cs_xx_principale::pc_setwindow;dw_con_fat_acq.set_dw_key("cod_azienda")
dw_con_fat_acq.set_dw_options(sqlca, &
                              pcca.null_object, &
                              c_default, &
                              c_default)

end on

on w_con_fat_acq.create
int iCurrent
call super::create
this.dw_con_fat_acq=create dw_con_fat_acq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_con_fat_acq
end on

on w_con_fat_acq.destroy
call super::destroy
destroy(this.dw_con_fat_acq)
end on

type dw_con_fat_acq from uo_cs_xx_dw within w_con_fat_acq
integer x = 23
integer y = 20
integer width = 2857
integer height = 480
string dataobject = "d_con_fat_acq"
borderstyle borderstyle = styleraised!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

