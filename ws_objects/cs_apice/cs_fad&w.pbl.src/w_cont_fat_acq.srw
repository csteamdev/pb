﻿$PBExportHeader$w_cont_fat_acq.srw
$PBExportComments$Finestra Controllo Fatture Acquisto
forward
global type w_cont_fat_acq from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_cont_fat_acq
end type
type dw_cont_fat_acq from uo_cs_xx_dw within w_cont_fat_acq
end type
end forward

global type w_cont_fat_acq from w_cs_xx_principale
integer width = 4146
integer height = 1252
string title = "Contropartite"
cb_1 cb_1
dw_cont_fat_acq dw_cont_fat_acq
end type
global w_cont_fat_acq w_cont_fat_acq

event pc_setwindow;call super::pc_setwindow;dw_cont_fat_acq.set_dw_key("cod_azienda")
dw_cont_fat_acq.set_dw_key("anno_registrazione")
dw_cont_fat_acq.set_dw_key("num_registrazione")

dw_cont_fat_acq.set_dw_options(sqlca, &
                               i_openparm, &
                               c_scrollparent, &
                               c_default)

end event

on w_cont_fat_acq.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_cont_fat_acq=create dw_cont_fat_acq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_cont_fat_acq
end on

on w_cont_fat_acq.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_cont_fat_acq)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_cont_fat_acq, &
                 "cod_conto", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type cb_1 from commandbutton within w_cont_fat_acq
integer x = 3639
integer y = 1036
integer width = 430
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Centro di Costo"
end type

event clicked;if dw_cont_fat_acq.rowcount() > 0 then
	window_open_parm(w_cont_fat_acq_cc, -1, dw_cont_fat_acq)

end if
end event

type dw_cont_fat_acq from uo_cs_xx_dw within w_cont_fat_acq
integer x = 23
integer y = 20
integer width = 4050
integer height = 1000
integer taborder = 10
string dataobject = "d_cont_fat_acq"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long   l_errore, ll_anno_registrazione, ll_num_registrazione, ll_i

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	
for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if	
next
end event

event pcd_retrieve;call super::pcd_retrieve;long   l_errore, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	
l_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)

if l_Errore < 0 then
   pcca.error = c_Fatal
end if

end event

