﻿$PBExportHeader$w_cont_fat_acq_cc.srw
$PBExportComments$window che gestisce la dw_cont_fat_acq_cc
forward
global type w_cont_fat_acq_cc from w_cs_xx_principale
end type
type dw_cont_fat_acq_cc from uo_cs_xx_dw within w_cont_fat_acq_cc
end type
end forward

global type w_cont_fat_acq_cc from w_cs_xx_principale
integer width = 2615
integer height = 1128
string title = "Centri di Costo"
dw_cont_fat_acq_cc dw_cont_fat_acq_cc
end type
global w_cont_fat_acq_cc w_cont_fat_acq_cc

on w_cont_fat_acq_cc.create
int iCurrent
call super::create
this.dw_cont_fat_acq_cc=create dw_cont_fat_acq_cc
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_cont_fat_acq_cc
end on

on w_cont_fat_acq_cc.destroy
call super::destroy
destroy(this.dw_cont_fat_acq_cc)
end on

event pc_setwindow;call super::pc_setwindow;dw_cont_fat_acq_cc.set_dw_key("cod_azienda")
dw_cont_fat_acq_cc.set_dw_key("anno_registrazione")
dw_cont_fat_acq_cc.set_dw_key("num_registrazione")
dw_cont_fat_acq_cc.set_dw_key("cod_conto")

dw_cont_fat_acq_cc.set_dw_options(sqlca, &
                                  i_openparm, &
                                  c_scrollparent, &
                                  c_default)














end event

event pc_setddlb;call super::pc_setddlb;	f_po_loaddddw_dw(dw_cont_fat_acq_cc, &
                 	  "cod_centro_costo", &
                 		sqlca, &
                    "tab_centri_costo", &
                    "cod_centro_costo", &
                    "des_centro_costo", &
                    "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event closequery;call super::closequery;long   l_errore, ll_i
dec{4} ld_count

ld_count = 0

// controlla se c'è almeno una riga.
dw_cont_fat_acq_cc.acceptText()
if dw_cont_fat_acq_cc.rowcount() > 0 then
	//se c'è una riga controlla che il totale sia cento
	for ll_i = 1 to dw_cont_fat_acq_cc.rowcount() 
   	ld_count +=dw_cont_fat_acq_cc.getitemnumber(ll_i, "percentuale")  		
	next
//se il totale non è cento lancia un messaggio di avvertimento e non permette la chiusura
	if ld_count <> 100 or isnull(ld_count) then
		g_mb.messagebox("Errore", "Attenzione: la somma delle percentuali non è cento")
		return 1
	else
		call super::closequery
		return 0
	end if
	
else
	call super::closequery
	
end if

end event

type dw_cont_fat_acq_cc from uo_cs_xx_dw within w_cont_fat_acq_cc
integer width = 2560
integer height = 1000
integer taborder = 10
string dataobject = "d_cont_fat_acq_cc"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;string ls_cod_conto
long   l_errore, ll_anno_registrazione, ll_num_registrazione, ll_i, ll_max

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
ls_cod_conto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_conto")
	
for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if	
	if isnull(this.getitemstring(ll_i, "cod_conto")) or &
      this.getitemstring(ll_i, "cod_conto") = '' then
      this.setitem(ll_i, "cod_conto", ls_cod_conto)
   end if
	
next

end event

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_conto
long   l_errore, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
ls_cod_conto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_conto")

l_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione, ls_cod_conto)

if l_Errore < 0 then
   pcca.error = c_Fatal
end if

end event

event itemchanged;call super::itemchanged;decimal  ld_percentuale

ld_percentuale = dec(data)
if Dwo.name = "percentuale" then
	if ld_percentuale > 100 or ld_percentuale <= 0 then
		//errato
		g_mb.messagebox("Errore", "Attenzione: il valore inserito non è valido")
     	return -1
	end if
else
	return 0
end if

end event

