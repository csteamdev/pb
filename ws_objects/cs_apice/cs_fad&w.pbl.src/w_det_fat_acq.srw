﻿$PBExportHeader$w_det_fat_acq.srw
$PBExportComments$Finestra Dettaglio Fatture Acquisto
forward
global type w_det_fat_acq from w_cs_xx_principale
end type
type st_colonna_bolla_acquisto from statictext within w_det_fat_acq
end type
type st_bolla_acquisto from statictext within w_det_fat_acq
end type
type st_6 from statictext within w_det_fat_acq
end type
type pb_prod_view from cb_listview within w_det_fat_acq
end type
type st_1 from statictext within w_det_fat_acq
end type
type st_2 from statictext within w_det_fat_acq
end type
type st_3 from statictext within w_det_fat_acq
end type
type cb_agg_mov_mag from commandbutton within w_det_fat_acq
end type
type cb_prezzo from commandbutton within w_det_fat_acq
end type
type cb_sconti from commandbutton within w_det_fat_acq
end type
type cb_calcolo_speciale from commandbutton within w_det_fat_acq
end type
type cb_stock from cb_stock_ricerca within w_det_fat_acq
end type
type cb_c_industriale from commandbutton within w_det_fat_acq
end type
type cb_ripristina from commandbutton within w_det_fat_acq
end type
type cb_grezzo from commandbutton within w_det_fat_acq
end type
type cb_prodotti_grezzi_ricerca from cb_prod_ricerca within w_det_fat_acq
end type
type dw_det_fat_acq_lista from uo_cs_xx_dw within w_det_fat_acq
end type
type dw_det_fat_acq_det_1 from uo_cs_xx_dw within w_det_fat_acq
end type
type dw_det_fat_acq_cc from uo_cs_xx_dw within w_det_fat_acq
end type
type dw_det_fat_acq_det_3 from uo_cs_xx_dw within w_det_fat_acq
end type
type dw_supporto_dati from datawindow within w_det_fat_acq
end type
type dw_folder from u_folder within w_det_fat_acq
end type
end forward

global type w_det_fat_acq from w_cs_xx_principale
integer width = 3570
integer height = 2044
string title = "Righe Fatture Acquisto"
boolean minbox = false
st_colonna_bolla_acquisto st_colonna_bolla_acquisto
st_bolla_acquisto st_bolla_acquisto
st_6 st_6
pb_prod_view pb_prod_view
st_1 st_1
st_2 st_2
st_3 st_3
cb_agg_mov_mag cb_agg_mov_mag
cb_prezzo cb_prezzo
cb_sconti cb_sconti
cb_calcolo_speciale cb_calcolo_speciale
cb_stock cb_stock
cb_c_industriale cb_c_industriale
cb_ripristina cb_ripristina
cb_grezzo cb_grezzo
cb_prodotti_grezzi_ricerca cb_prodotti_grezzi_ricerca
dw_det_fat_acq_lista dw_det_fat_acq_lista
dw_det_fat_acq_det_1 dw_det_fat_acq_det_1
dw_det_fat_acq_cc dw_det_fat_acq_cc
dw_det_fat_acq_det_3 dw_det_fat_acq_det_3
dw_supporto_dati dw_supporto_dati
dw_folder dw_folder
end type
global w_det_fat_acq w_det_fat_acq

type variables
boolean ib_modifica=false, ib_nuovo=false
boolean ib_calcolo_speciale = false
long il_riga_corrente
string is_ord_ddt_rif = "DESC"
end variables

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det_fat_acq_lista, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_det_fat_acq_det_1, &
                 "cod_tipo_det_acq", &
                 sqlca, &
                 "tab_tipi_det_acq", &
                 "cod_tipo_det_acq", &
                 "des_tipo_det_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_det_fat_acq_det_1, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_det_fat_acq_det_1, &
                 "cod_misura", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_po_loaddddw_dw(dw_det_fat_acq_det_1, &
                 "cod_tipo_movimento", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_det_fat_acq_det_3, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_fat_acq_lista, &
                 "cod_iva", &
                 sqlca, &
                 "tab_ive", &
                 "cod_iva", &
                 "des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")	
f_po_loaddddw_dw(dw_det_fat_acq_cc, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  

end event

event pc_setwindow;call super::pc_setwindow;dw_det_fat_acq_lista.set_dw_key("cod_azienda")
dw_det_fat_acq_lista.set_dw_key("anno_registrazione")
dw_det_fat_acq_lista.set_dw_key("num_registrazione")

dw_det_fat_acq_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + &
                                    c_nohighlightselected + c_ViewModeBorderUnchanged)																			
dw_det_fat_acq_det_1.set_dw_options(sqlca, &
                                    dw_det_fat_acq_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
dw_det_fat_acq_det_3.set_dw_options(sqlca, &
                                    dw_det_fat_acq_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
												
dw_supporto_dati.insertrow(0)

dw_det_fat_acq_cc.set_dw_key("cod_azienda")
dw_det_fat_acq_cc.set_dw_key("anno_registrazione")
dw_det_fat_acq_cc.set_dw_key("num_registrazione")
dw_det_fat_acq_cc.set_dw_key("prog_riga_acq_ven")

dw_det_fat_acq_cc.set_dw_options(sqlca, &
											pcca.null_object, &
											c_scrollparent, &
											c_default + &
											c_nohighlightselected + c_ViewModeBorderUnchanged)

windowobject lw_oggetti[], lw_oggetti1[], lw_oggetti3[]
lw_oggetti1[1] = dw_det_fat_acq_cc
dw_folder.fu_assigntab(4, "Centri Costo", lw_oggetti1[])
lw_oggetti1[1] = dw_det_fat_acq_det_3
lw_oggetti1[2] = cb_grezzo
lw_oggetti1[3] = cb_prodotti_grezzi_ricerca
dw_folder.fu_assigntab(3, "Note", lw_oggetti1[])
lw_oggetti3[1] = dw_det_fat_acq_det_1
lw_oggetti3[2] = cb_sconti
lw_oggetti3[3] = cb_prezzo
lw_oggetti3[4] = cb_c_industriale
lw_oggetti3[5] = pb_prod_view
lw_oggetti3[6] = cb_stock
dw_folder.fu_assigntab(2, "Dettaglio", lw_oggetti3[])
lw_oggetti[1] = dw_det_fat_acq_lista
lw_oggetti[2] = dw_supporto_dati
lw_oggetti[3] = cb_calcolo_speciale
lw_oggetti[4] = cb_ripristina
lw_oggetti[5] = st_1
lw_oggetti[6] = st_2
lw_oggetti[7] = st_3
lw_oggetti[8] = st_6
lw_oggetti[9] = cb_agg_mov_mag
lw_oggetti[10] = st_bolla_acquisto
lw_oggetti[11] = st_colonna_bolla_acquisto
dw_folder.fu_assigntab(1, "Lista", lw_oggetti[])
dw_folder.fu_foldercreate(4, 5)
dw_folder.fu_selecttab(1)

//iuo_dw_main=dw_det_fat_acq_lista
cb_prezzo.enabled=false
cb_stock.enabled=false
cb_c_industriale.enabled = false
cb_grezzo.enabled=true


try
	dw_det_fat_acq_det_1.object.p_mov.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
	dw_det_fat_acq_det_1.object.p_mov_ret.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
	dw_det_fat_acq_det_1.object.p_mov_terzista.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
catch (runtimeerror err)
end try

dw_det_fat_acq_lista.is_cod_parametro_blocco_prodotto = 'PFA'
dw_det_fat_acq_det_1.is_cod_parametro_blocco_prodotto = 'PFA'


end event

on w_det_fat_acq.create
int iCurrent
call super::create
this.st_colonna_bolla_acquisto=create st_colonna_bolla_acquisto
this.st_bolla_acquisto=create st_bolla_acquisto
this.st_6=create st_6
this.pb_prod_view=create pb_prod_view
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.cb_agg_mov_mag=create cb_agg_mov_mag
this.cb_prezzo=create cb_prezzo
this.cb_sconti=create cb_sconti
this.cb_calcolo_speciale=create cb_calcolo_speciale
this.cb_stock=create cb_stock
this.cb_c_industriale=create cb_c_industriale
this.cb_ripristina=create cb_ripristina
this.cb_grezzo=create cb_grezzo
this.cb_prodotti_grezzi_ricerca=create cb_prodotti_grezzi_ricerca
this.dw_det_fat_acq_lista=create dw_det_fat_acq_lista
this.dw_det_fat_acq_det_1=create dw_det_fat_acq_det_1
this.dw_det_fat_acq_cc=create dw_det_fat_acq_cc
this.dw_det_fat_acq_det_3=create dw_det_fat_acq_det_3
this.dw_supporto_dati=create dw_supporto_dati
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_colonna_bolla_acquisto
this.Control[iCurrent+2]=this.st_bolla_acquisto
this.Control[iCurrent+3]=this.st_6
this.Control[iCurrent+4]=this.pb_prod_view
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.st_2
this.Control[iCurrent+7]=this.st_3
this.Control[iCurrent+8]=this.cb_agg_mov_mag
this.Control[iCurrent+9]=this.cb_prezzo
this.Control[iCurrent+10]=this.cb_sconti
this.Control[iCurrent+11]=this.cb_calcolo_speciale
this.Control[iCurrent+12]=this.cb_stock
this.Control[iCurrent+13]=this.cb_c_industriale
this.Control[iCurrent+14]=this.cb_ripristina
this.Control[iCurrent+15]=this.cb_grezzo
this.Control[iCurrent+16]=this.cb_prodotti_grezzi_ricerca
this.Control[iCurrent+17]=this.dw_det_fat_acq_lista
this.Control[iCurrent+18]=this.dw_det_fat_acq_det_1
this.Control[iCurrent+19]=this.dw_det_fat_acq_cc
this.Control[iCurrent+20]=this.dw_det_fat_acq_det_3
this.Control[iCurrent+21]=this.dw_supporto_dati
this.Control[iCurrent+22]=this.dw_folder
end on

on w_det_fat_acq.destroy
call super::destroy
destroy(this.st_colonna_bolla_acquisto)
destroy(this.st_bolla_acquisto)
destroy(this.st_6)
destroy(this.pb_prod_view)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.cb_agg_mov_mag)
destroy(this.cb_prezzo)
destroy(this.cb_sconti)
destroy(this.cb_calcolo_speciale)
destroy(this.cb_stock)
destroy(this.cb_c_industriale)
destroy(this.cb_ripristina)
destroy(this.cb_grezzo)
destroy(this.cb_prodotti_grezzi_ricerca)
destroy(this.dw_det_fat_acq_lista)
destroy(this.dw_det_fat_acq_det_1)
destroy(this.dw_det_fat_acq_cc)
destroy(this.dw_det_fat_acq_det_3)
destroy(this.dw_supporto_dati)
destroy(this.dw_folder)
end on

event pc_delete;call super::pc_delete;if dw_det_fat_acq_lista.i_parentdw.getitemstring(dw_det_fat_acq_lista.i_parentdw.getrow(), "flag_fat_confermata") = "S" then
   g_mb.messagebox("Attenzione", "Fattura non cancellabile! E' già stata confermata.", exclamation!, ok!)
   dw_det_fat_acq_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
   return
end if

cb_prezzo.enabled=false
cb_c_industriale.enabled = false

end event

event pc_save;call super::pc_save;dw_det_fat_acq_lista.postevent("ue_ultimo")
end event

type st_colonna_bolla_acquisto from statictext within w_det_fat_acq
integer x = 864
integer y = 1512
integer width = 786
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 15780518
alignment alignment = right!
boolean focusrectangle = false
end type

type st_bolla_acquisto from statictext within w_det_fat_acq
integer x = 315
integer y = 1512
integer width = 526
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Bolla acquisto:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_6 from statictext within w_det_fat_acq
integer x = 823
integer y = 1636
integer width = 576
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
string text = "Shift + F4 = nuova riga"
boolean focusrectangle = false
end type

type pb_prod_view from cb_listview within w_det_fat_acq
integer x = 2331
integer y = 240
integer width = 73
integer height = 80
integer taborder = 20
boolean bringtotop = true
end type

event clicked;call super::clicked;dw_det_fat_acq_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end event

type st_1 from statictext within w_det_fat_acq
integer x = 69
integer y = 1636
integer width = 686
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Shift + F1 = cerca prodotti  "
boolean focusrectangle = false
end type

type st_2 from statictext within w_det_fat_acq
integer x = 1509
integer y = 1636
integer width = 841
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Shift + F5 = conversione quantità "
boolean focusrectangle = false
end type

type st_3 from statictext within w_det_fat_acq
integer x = 2446
integer y = 1636
integer width = 1033
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 12632256
boolean enabled = false
string text = "Shift + F6 = ripristino situazione quantità "
boolean focusrectangle = false
end type

type cb_agg_mov_mag from commandbutton within w_det_fat_acq
integer x = 3086
integer y = 1100
integer width = 366
integer height = 80
integer taborder = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Agg. Mov."
end type

event clicked;string		ls_cod_prodotto, ls_des_prodotto, ls_cod_prodotto_grezzo, ls_des_prodotto_grezzo, &
			ls_cod_prodotto_raggruppato, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], &
			ls_cod_cliente[], ls_cod_terzista[], ls_cod_tipo_movimento, ls_cod_tipo_mov_det, &
			ls_cod_fornitore, ls_cod_tipo_movimento_grezzo, ls_test, ls_error
dec{4} 	ld_prezzo_netto, ld_prezzo_grezzo, ld_quantita, ld_quan_movimento, ld_val_movimento, ld_val_unitario
long   	ll_i, ll_anno_mov, ll_num_mov, ll_ret, ll_anno_reg_mov_grezzo[], ll_num_reg_mov_grezzo[], &
			ll_prog_stock[], ll_anno_reg_dest_stock,ll_num_reg_dest_stock, ll_anno_fattura, ll_num_fattura, &
			ll_prog_fattura, ll_anno_reg_mov_mag_grezzo, ll_num_reg_mov_mag_grezzo
datetime ldt_data_registrazione, ldt_data_stock[]
uo_magazzino luo_mag

s_cs_xx.parametri.parametro_s_1 = ""		// azione di ritorno
s_cs_xx.parametri.parametro_s_2 = ""		// esegue movimento di scarico del prodotto grezzo da magazzino
s_cs_xx.parametri.parametro_s_5 = ""		// aggiorna anche il costo ultimo nella scheda magazzino
s_cs_xx.parametri.parametro_s_6 = ""		// codice di deposito nuovo

open(w_richiesta_azione_agg_mov)

if s_cs_xx.parametri.parametro_s_1 = "4" then return

for ll_i = 1 to dw_det_fat_acq_lista.rowcount()
	
	// stefanop 26/05/2010: ticket 134: Aggiunto controllo sull'anno e numero mov mag grezzo
	// se questi sono inizializzati non devo creare il movimento
	ll_anno_reg_mov_mag_grezzo = dw_det_fat_acq_lista.getitemnumber(ll_i,"anno_reg_mov_mag_grezzo")
	ll_num_reg_mov_mag_grezzo = dw_det_fat_acq_lista.getitemnumber(ll_i,"num_reg_mov_mag_grezzo")
	
	if not isnull(ll_anno_reg_mov_mag_grezzo) and ll_anno_reg_mov_mag_grezzo > 1900 and  &
		not isnull(ll_num_reg_mov_mag_grezzo) and ll_num_reg_mov_mag_grezzo > 0 then

		g_mb.show("APICE", "Attenzione, movimento di magazzino già creato")
		continue
	end if
	// ----
	
	ll_anno_mov = dw_det_fat_acq_lista.getitemnumber(ll_i,"anno_registrazione_mov_mag")
	ll_num_mov = dw_det_fat_acq_lista.getitemnumber(ll_i,"num_registrazione_mov_mag")
	
	ls_cod_prodotto = dw_det_fat_acq_lista.getitemstring(ll_i,"cod_prodotto")
	ls_des_prodotto = dw_det_fat_acq_lista.getitemstring(ll_i,"des_prodotto")
	if isnull(ls_cod_prodotto) then ls_cod_prodotto = ""
	if isnull(ls_des_prodotto) then ls_des_prodotto = ""
	
	ls_cod_prodotto_grezzo = dw_det_fat_acq_lista.getitemstring(ll_i,"cod_prodotto_grezzo")
	ls_des_prodotto_grezzo = dw_det_fat_acq_lista.getitemstring(ll_i,"des_prodotto_grezzo")
	if isnull(ls_cod_prodotto_grezzo) then ls_cod_prodotto_grezzo = ""
	if isnull(ls_des_prodotto_grezzo) then ls_des_prodotto_grezzo = ""
	
	ld_prezzo_netto = dw_det_fat_acq_lista.getitemdecimal(ll_i,"prezzo_acq_netto")
	ld_prezzo_grezzo = dw_det_fat_acq_lista.getitemdecimal(ll_i,"val_grezzo")
	
	if not isnull(ll_anno_mov) and ll_anno_mov > 0 and not isnull(ll_num_mov) and ll_num_mov > 0  then
		
		// chiedi
		if s_cs_xx.parametri.parametro_s_1 ="1" and ld_prezzo_grezzo > 0 and not isnull(ld_prezzo_grezzo) then
			ll_ret = g_mb.messagebox("Valore Prodotto Grezzo","Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + "   Valore=" + string(ld_prezzo_netto,"#,##0.00##") + &
									  "~r~nProdotto Grezzo: " + ls_cod_prodotto_grezzo + " - " + ls_des_prodotto_grezzo + "   Valore=" + string(ld_prezzo_grezzo,"#,##0.00##") + &
									  "~r~n ~r~n     VALORIZZO IL MOVIMENTO DI MAGAZZINO SOMMANDO~r~n     IL VALORE DEL GREZZO AL PREZZO DI ACQUISTO ??",Question!,YesNo!,1)
			if ll_ret = 1 then 
				ld_prezzo_netto = ld_prezzo_netto + ld_prezzo_grezzo
			end if
		end if
		
		
		// somma sempre
		if s_cs_xx.parametri.parametro_s_1 ="2" and ld_prezzo_grezzo > 0 and not isnull(ld_prezzo_grezzo) then
			ld_prezzo_netto = ld_prezzo_netto + ld_prezzo_grezzo
		end if
		
		
		// **** leggo i dati che mi servo x l'aggiornamento dell'anagrafica
		ll_anno_fattura = dw_det_fat_acq_lista.getitemnumber(ll_i, "anno_registrazione")
		ll_num_fattura = dw_det_fat_acq_lista.getitemnumber(ll_i, "num_registrazione")
		ll_prog_fattura = dw_det_fat_acq_lista.getitemnumber(ll_i, "prog_riga_fat_acq")
		ld_quantita = dw_det_fat_acq_lista.getitemnumber(ll_i, "quan_fatturata")
				
		select cod_tipo_movimento,
				 cod_tipo_mov_det,
				 quan_movimento,
				 val_movimento
		into   :ls_cod_tipo_movimento,
				 :ls_cod_tipo_mov_det,
				 :ld_quan_movimento,
				 :ld_val_movimento
		from   mov_magazzino
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_mov and
				 num_registrazione = :ll_num_mov;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in ricerca tipo movimento magazzino. " + sqlca.sqlerrtext)
			rollback;
			return -1
		end if						
				
		ld_val_unitario = ld_val_movimento
		
		//###########################################
		//Donato 18/05/2010
		//recupero la data registrazione del movimento per creare il movimento in tale data		
		select cod_fornitore, data_registrazione
		into   :ls_cod_fornitore, :ldt_data_registrazione
		from   tes_fat_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_fattura and
				 num_registrazione = :ll_num_fattura;
		//fine modifica ---------------------------------------------------------------------------
		//###########################################

		luo_mag = create uo_magazzino

		// rettifico il movimento nell'anagrafica
		// !!! FATTA SOTTO !!!
		/*luo_mag.ib_flag_agg_costo_ultimo = false
		if luo_mag.uof_aggiorna_prodotti( ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ls_cod_prodotto, ld_quan_movimento, ld_val_unitario, ls_cod_fornitore, true) = -1  then
			g_mb.messagebox("APICE","Errore in Rettifica Anagrafica prodotti. " + sqlca.sqlerrtext)
			rollback;
			destroy luo_mag;
			return -1				
		end if*/


		luo_mag.ib_flag_agg_costo_ultimo = false

		if s_cs_xx.parametri.parametro_s_5 = "1" then
			ll_ret = g_mb.messagebox("Aggiorna Costo Ultimo","Prodotto: " + ls_cod_prodotto + " - " + ls_des_prodotto + &									  
									  "~r~n ~r~n     AGGIORNARE ANCHE IL COSTO ULTIMO DI ACQUISTO IN ANAGRAFICA ?",Question!,YesNo!,1)
			if ll_ret = 1 then 
				luo_mag.ib_flag_agg_costo_ultimo = true
			end if			

		elseif s_cs_xx.parametri.parametro_s_5 = "2" then
			
			luo_mag.ib_flag_agg_costo_ultimo = true			

		end if
		
		// --- enme 08/1/2006 gestione prodotto raggruppato ---------
		// aggiunto questo script il 11/08/2009 perchè mancava; segnalato da Beatrice.
		// stefanop: 20/04/2012: Commento perchè adesso vengono eseguiti due movimenti
		// uno per il prodotto e uno per il prodotto raggruppato, quindi devo fare due update nei 
		// movimenti di magazzino !!!
		/*
		setnull(ls_cod_prodotto_raggruppato)
		
		select cod_prodotto_raggruppato
		into   :ls_cod_prodotto_raggruppato
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_prodotto = :ls_cod_prodotto;
				 
		if not isnull(ls_cod_prodotto_raggruppato) then
			ld_prezzo_netto = f_converti_qta_raggruppo(ls_cod_prodotto, ld_prezzo_netto, "D")
			ls_cod_prodotto = ls_cod_prodotto_raggruppato
		end if
		*/
		//	-----------------------------------------------------------
		
		if luo_mag.uof_aggiorna_prodotti( ls_cod_tipo_movimento, ls_cod_tipo_mov_det, ls_cod_prodotto, ld_quantita, ld_prezzo_netto, ls_cod_fornitore, false) = -1  then
			g_mb.messagebox("APICE","Errore in Aggiornamento Anagrafica prodotti. " + sqlca.sqlerrtext)
			rollback;
			destroy luo_mag;
			return -1				
		end if		
		
		update mov_magazzino
		set val_movimento = :ld_prezzo_netto,
			 quan_movimento = :ld_quantita
		where cod_azienda   = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_mov and
				 num_registrazione  = :ll_num_mov;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in aggiornamento movimento di magazzino ("+string(ll_anno_mov)+ "/" +string(ll_num_mov)+"): " + sqlca.sqlerrtext)
			rollback;
			return -1
		end if
		
		// stefanop: 20/04/2012: Aggiorno il prezzo del prodotto raggruppato
		//	-----------------------------------------------------------
		if luo_mag.uof_aggiorna_mov_mag_raggruppato(ll_anno_mov, ll_num_mov, ls_cod_tipo_movimento, ls_cod_tipo_mov_det, &
								ls_cod_prodotto, ld_quantita, ld_prezzo_netto, ls_cod_fornitore, false, ref ls_error) = -1  then
			g_mb.error(ls_error)
			rollback;
			destroy luo_mag;
			return -1				
		end if
		//	-----------------------------------------------------------
		
		destroy luo_mag;		
		
		//Specifica richieste_varie_2010 funzione 5
		//aggiorna il campo flag_agg_mov
		update det_fat_acq
		set    flag_agg_mov = 'S'
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione  = :ll_anno_fattura and
				 num_registrazione   = :ll_num_fattura and
				 prog_riga_fat_acq   = :ll_prog_fattura;
		
		// eseguo il movimento di scarico del grezzo dal magazzino
		if s_cs_xx.parametri.parametro_s_2 = "1" then
			ls_cod_prodotto_grezzo = dw_det_fat_acq_lista.getitemstring(ll_i,"cod_prodotto_grezzo")
			ld_prezzo_grezzo = dw_det_fat_acq_lista.getitemdecimal(ll_i,"val_grezzo")
			ld_quantita = dw_det_fat_acq_lista.getitemnumber(ll_i, "quan_fatturata")
			
			select cod_tipo_movimento_grezzo
			into   :ls_cod_tipo_movimento_grezzo
			from   con_fat_acq
			where  cod_azienda = :s_cs_xx.cod_azienda;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in ricerca TIPO MOVIMENTO GREZZO in parametri fatture acquisti: tutto annullato " + sqlca.sqlerrtext)
				rollback;
				return -1
			end if	
			
			if isnull(ls_cod_tipo_movimento_grezzo) then
				g_mb.messagebox("APICE","Parametro TIPO MOVIMENTO GREZZO in parametri fatture acquisti non impostato: tutto annullato " + sqlca.sqlerrtext)
				rollback;
				return -1
			end if	

			if not isnull(ls_cod_prodotto_grezzo) and ld_quantita > 0 then
				
			// eseguo il movimento di magazzino.
				
				ls_cod_ubicazione[1]	= dw_det_fat_acq_lista.getitemstring(ll_i,"cod_ubicazione")
				ls_cod_lotto[1]			= dw_det_fat_acq_lista.getitemstring(ll_i,"cod_lotto")
				ldt_data_stock[1]		= dw_det_fat_acq_lista.getitemdatetime(ll_i,"data_stock")
				ll_prog_stock[1]		= dw_det_fat_acq_lista.getitemnumber(ll_i,"prog_stock")
				
				// stefanop 26/02/2010: imposto il deposito selezionato nella finestra w_richiesta_azione_agg_mov
				if s_cs_xx.parametri.parametro_s_6 <> "" then
					ls_cod_deposito[1] = s_cs_xx.parametri.parametro_s_6
					
					// controllo esistenza stock
					select cod_azienda
					into	:ls_test
					from	stock
					where
						cod_azienda = :s_cs_xx.cod_azienda and
						cod_prodotto = :ls_cod_prodotto_grezzo and
						cod_deposito = :ls_cod_deposito[1] and
						cod_ubicazione = :ls_cod_ubicazione[1] and
						cod_lotto = :ls_cod_lotto[1] and
						data_stock = :ldt_data_stock[1] and
						prog_stock = :ll_prog_stock[1];
						
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("APICE", "Errore in controllo esistenza stock.~r~n" + sqlca.sqlerrtext)
						rollback;
						return -1
					elseif sqlca.sqlcode = 100 then // stock non trovato
						// seleziono prima riga
						select cod_ubicazione, cod_lotto, data_stock, prog_stock
						into	:ls_cod_ubicazione[1], :ls_cod_lotto[1], :ldt_data_stock[1], :ll_prog_stock[1]
						from	stock
						where
							cod_azienda = :s_cs_xx.cod_azienda and
							cod_prodotto = :ls_cod_prodotto_grezzo and
							cod_deposito = :ls_cod_deposito[1];
							
						if sqlca.sqlcode < 0 then
							g_mb.messagebox("APICE", "Errore controllo esistenza stock alternativo.~r~n" + sqlca.sqlerrtext)
							rollback;
							return -1
						elseif sqlca.sqlcode = 100 then // non esiste nessun stock
							g_mb.messagebox("APICE", "Non esiste nessuno stock")
							rollback;
							return -1
						end if
					end if
					// ----
				else
					ls_cod_deposito[1]   = dw_det_fat_acq_lista.getitemstring(ll_i,"cod_deposito")
				end if
				// ----
								
				setnull(ls_cod_cliente[1])
				setnull(ls_cod_terzista[1])
				
				if f_crea_dest_mov_magazzino (ls_cod_tipo_movimento_grezzo, &
														ls_cod_prodotto_grezzo, &
														ls_cod_deposito[], &
														ls_cod_ubicazione[], &
														ls_cod_lotto[], &
														ldt_data_stock[], &
														ll_prog_stock[], &
														ls_cod_cliente[], &
														ls_cod_terzista[], &
														ll_anno_reg_dest_stock, &
														ll_num_reg_dest_stock ) = -1 then
					ROLLBACK;
					return -1
				end if
				
				if f_verifica_dest_mov_mag (ll_anno_reg_dest_stock, &
												 ll_num_reg_dest_stock, &
												 ls_cod_tipo_movimento_grezzo, &
												 ls_cod_prodotto_grezzo) = -1 then
					ROLLBACK;
					return 0
				end if
				
				luo_mag = create uo_magazzino
				
				//###########################################
				//Donato 18/05/2010 commentato perchè ho scritto nella variabile data la data registrazione del documento
				//vedi commento più in alto nella select dalla tes_fat_acq
				
				//ldt_data_registrazione = datetime( today(), time("00:00:00") )
				
				//fine modifica ---------------------------------------------------------
				//###########################################
				
				if luo_mag.uof_movimenti_mag ( ldt_data_registrazione, &
											ls_cod_tipo_movimento_grezzo, &
											"S", &
											ls_cod_prodotto_grezzo, &
											ld_quantita, &
											ld_prezzo_grezzo, &
											0, &
											ldt_data_registrazione, &
											'', &
											ll_anno_reg_dest_stock, &
											ll_num_reg_dest_stock, &
											ls_cod_deposito[], &
											ls_cod_ubicazione[], &
											ls_cod_lotto[], &
											ldt_data_stock[], &
											ll_prog_stock[], &
											ls_cod_terzista[], &
											ls_cod_cliente[], &
											ll_anno_reg_mov_grezzo[], &
											ll_num_reg_mov_grezzo[] ) = 0 then
					COMMIT;
				
					if f_elimina_dest_mov_mag (ll_anno_reg_dest_stock, &
														ll_num_reg_dest_stock) = -1 then
						ROLLBACK;         // rollback della sola eliminazione dest_mov_magazzino
					end if
				else
					g_mb.messagebox("APICE","Errore in registrazione movimento di magazzino di scarico GREZZO del prodotto " + ls_cod_prodotto_grezzo + "~r~n" + sqlca.sqlerrtext)
					rollback;
					return -1
				end if
				
			
			// fine del movimento di magazzino, aggiorno la riga
			
				update det_fat_acq
				set    anno_reg_mov_mag_grezzo = :ll_anno_reg_mov_grezzo[1],
				       num_reg_mov_mag_grezzo = :ll_num_reg_mov_grezzo[1]
				where  cod_azienda = :s_cs_xx.cod_azienda and
				       anno_registrazione  = :ll_anno_fattura and
						 num_registrazione   = :ll_num_fattura and
						 prog_riga_fat_acq   = :ll_prog_fattura;
			
				destroy luo_mag

			end if
			
		end if

	end if
next
commit;
g_mb.messagebox("APICE","Aggiornamento movimenti di magazzino completato.")
if s_cs_xx.parametri.parametro_s_5 <> "" then
	g_mb.messagebox("APICE","Aggiornamento costo ultimo in Anagrafica completato.")
end if

dw_det_fat_acq_lista.postevent("pcd_retrieve")



end event

type cb_prezzo from commandbutton within w_det_fat_acq
event clicked pbm_bnclicked
integer x = 2697
integer y = 1280
integer width = 361
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Prezzo"
end type

event clicked;dw_det_fat_acq_det_1.accepttext()

dw_det_fat_acq_det_1.setcolumn("cod_misura")
s_cs_xx.parametri.parametro_s_1 = dw_det_fat_acq_det_1.gettext()

dw_det_fat_acq_det_1.setcolumn("prezzo_acquisto")
s_cs_xx.parametri.parametro_d_1 = dec(dw_det_fat_acq_det_1.gettext())

dw_det_fat_acq_det_1.setcolumn("fat_conversione")
s_cs_xx.parametri.parametro_d_2 = dec(dw_det_fat_acq_det_1.gettext())

dw_det_fat_acq_det_1.setcolumn("quan_fatturata")
s_cs_xx.parametri.parametro_d_3 = dec(dw_det_fat_acq_det_1.gettext())

window_open(w_prezzo_um, 0)

if s_cs_xx.parametri.parametro_d_1 <> 0 then
   dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(), "prezzo_acquisto", s_cs_xx.parametri.parametro_d_1)
end if

if s_cs_xx.parametri.parametro_d_3 <> 0 then
   dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(), "quan_fatturata", s_cs_xx.parametri.parametro_d_3)
end if
end event

type cb_sconti from commandbutton within w_det_fat_acq
integer x = 3086
integer y = 1280
integer width = 361
integer height = 80
integer taborder = 130
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Sconti"
end type

event clicked;s_cs_xx.parametri.parametro_uo_dw_1 = dw_det_fat_acq_lista
window_open(w_sconti, 0)

end event

type cb_calcolo_speciale from commandbutton within w_det_fat_acq
integer x = 3086
integer y = 1440
integer width = 361
integer height = 80
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Calcolo"
end type

event clicked;dec{4} ld_prezzo_acquisto, ld_sconto_speciale, ld_pezzi_x_conf, ld_quan_sconto, ld_quan_fatturata, &
		 ld_costo_speciale, ld_totale, ld_percentuale, ld_sconto_riga, ld_costo_riga, &
		 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_importo_conai_scon, ld_importo_conai, &
		 ld_num_confezioni, ld_cambio, ld_prezzo_arrotondato, ld_val_riga_netto

string ls_cod_tipo_det_acq_costo, ls_cod_tipo_det_acq_sconto, ls_cod_tipo_det_acq_sconto_inc, &
		 ls_flag_tipo_ridistribuzione, ls_cod_valuta, ls_messaggio, ls_cod_tipo_det_acq, ls_flag_tipo_det_acq,&
		 ls_cod_lire, ls_cod_valuta_corrente

long   ll_num_righe, ll_num_riga_intermedio_i, ll_num_riga_intermedio_f, ll_inizio, &
	    ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_acq


if ib_calcolo_speciale then
	g_mb.messagebox("APICE","Calcolo già effettuato!")
	return -1
end if

ll_anno_registrazione = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "anno_registrazione")
ll_num_registrazione = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "num_registrazione")

//---------------------------------------- Calcolo Sconti e Costi Speciali ------------------------------------------
select cod_tipo_det_acq_costo,
		 cod_tipo_det_acq_sconto,
		 cod_tipo_det_acq_sconto_inc,
		 flag_tipo_ridistribuzione
  into :ls_cod_tipo_det_acq_costo,
		 :ls_cod_tipo_det_acq_sconto,
		 :ls_cod_tipo_det_acq_sconto_inc,
		 :ls_flag_tipo_ridistribuzione
  from con_acquisti
 where cod_azienda = :s_cs_xx.cod_azienda; 

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Errore in lettura dati dalla tabella CON_ACQUISTI.~n~r" + sqlca.sqlerrtext)
	return
end if	

ls_cod_valuta = dw_det_fat_acq_lista.i_parentdw.getitemstring(dw_det_fat_acq_lista.i_parentdw.getrow(), "cod_valuta")

// ************** Modifica Michele per Euro **************
select stringa
into   :ls_cod_lire
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'CVL';
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if

select stringa
into   :ls_cod_valuta_corrente
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 flag_parametro = 'S' and
		 cod_parametro = 'LIR';
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella select di parametri_azienda: " + sqlca.sqlerrtext)
	return -1
end if

if ls_cod_valuta_corrente = ls_cod_lire then
	
	ld_cambio = 1
	
else
	
	select cambio_acq
	into   :ld_cambio
	from   tab_valute
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_valuta = :ls_cod_valuta;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella select di tab_valute: " + sqlca.sqlerrtext)
		return -1
	end if	

end if
// **************** Fine modifica ************************

// ******************* 29/04/2003 Modifica Michele *******************
for ll_num_righe = 1 to dw_det_fat_acq_lista.rowcount()
	ld_prezzo_acquisto = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "prezzo_acquisto")	
	ld_importo_conai = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "importo_conai")
	ld_prezzo_acquisto = ld_prezzo_acquisto + ld_importo_conai
	ld_sconto_1 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_1")
	ld_sconto_2 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_2")
	ld_sconto_3 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_3")
	ld_sconto_4 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_4")
	ld_sconto_5 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_5")
	if not isnull(ld_sconto_1) then 
		ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_1) / 100
	end if	
	if not isnull(ld_sconto_2) then 
		ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_2) / 100
	end if	
	if not isnull(ld_sconto_3) then 
		ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_3) / 100
	end if	
	if not isnull(ld_sconto_4) then 
		ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_4) / 100
	end if	
	if not isnull(ld_sconto_5) then 
		ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_5) / 100
	end if		
	ld_importo_conai_scon = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "importo_conai_scon")
	ld_prezzo_acquisto = ld_prezzo_acquisto + ld_importo_conai_scon
	// Modifica per Euro
	ld_prezzo_acquisto = ld_prezzo_acquisto * ld_cambio
	
	ld_prezzo_arrotondato = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "prezzo_acq_netto")
	
	ld_quan_fatturata = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "quan_fatturata")	
	ld_pezzi_x_conf = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "num_pezzi_confezione")
	if dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") <> ls_cod_tipo_det_acq_costo and &
		dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") <> ls_cod_tipo_det_acq_sconto and &
		dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") <> ls_cod_tipo_det_acq_sconto_inc then	
		
		ls_cod_tipo_det_acq = dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq")
		
		select flag_tipo_det_acq
		  into :ls_flag_tipo_det_acq
		  from tab_tipi_det_acq
		 where cod_azienda = :s_cs_xx.cod_azienda 
			and cod_tipo_det_acq = :ls_cod_tipo_det_acq;
		
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice", "Errore in lettura dati in tabella TAB_TIPI_DET_ACQ.~n~r" + sqlca.sqlerrtext)
			return			
		end if													
		
		if ls_flag_tipo_det_acq = "M" or ls_flag_tipo_det_acq = "N" or ls_flag_tipo_det_acq = "C" then
//																									--> AGGIUNTO ANCHE CASO PRODOTTI NON COLLEGATI EnMe 31/12/2002

			ld_quan_sconto = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "quan_sconto_merce")
			if isnull(ld_quan_sconto) then
				ld_quan_sconto = 0
			end if
			if ld_quan_sconto > 0 then
				ld_prezzo_arrotondato = (ld_quan_fatturata * ld_prezzo_acquisto) / (ld_quan_fatturata + ld_quan_sconto * ld_pezzi_x_conf)
				dw_det_fat_acq_lista.setitem(ll_num_righe, "prezzo_acq_netto", ld_prezzo_arrotondato)
			end if

			ll_prog_riga_fat_acq = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "prog_riga_fat_acq")
			
			update det_fat_acq
				set prezzo_acq_netto = :ld_prezzo_arrotondato
			 where cod_azienda = :s_cs_xx.cod_azienda
				and anno_registrazione = :ll_anno_registrazione
				and num_registrazione = :ll_num_registrazione
				and prog_riga_fat_acq = :ll_prog_riga_fat_acq;
				
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE", "Errore in aggiornamento dati in tabella DET_FAT_ACQ.~n~r" + sqlca.sqlerrtext)
				return			
			end if
			
		end if	
	end if
next
// ******************* Fine Modifica *******************

ll_num_riga_intermedio_i = 1
ll_num_riga_intermedio_f = dw_det_fat_acq_lista.rowcount()
ll_inizio = 1

do while true
	
	for ll_num_righe = ll_num_riga_intermedio_i to ll_num_riga_intermedio_f  	//trovo sconto e costo speciali	
		ll_num_riga_intermedio_i = 0
		if dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") = ls_cod_tipo_det_acq_costo or &
			dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") = ls_cod_tipo_det_acq_sconto or &
			dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") = ls_cod_tipo_det_acq_sconto_inc then
			ll_num_riga_intermedio_i = ll_num_righe
			exit
		end if	
	next	
	
	if ll_num_riga_intermedio_i <> 0 then
		if ll_num_riga_intermedio_i < dw_det_fat_acq_lista.rowcount() then
			for ll_num_righe = (ll_num_riga_intermedio_i + 1) to ll_num_riga_intermedio_f
				ll_num_riga_intermedio_f = 0
				if dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") <> ls_cod_tipo_det_acq_costo and &
					dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") <> ls_cod_tipo_det_acq_sconto and &
					dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") <> ls_cod_tipo_det_acq_sconto_inc then
					ll_num_riga_intermedio_f = ll_num_righe
					exit
				end if
			next	
		else	
			ll_num_riga_intermedio_f = 0
		end if
	
		if ll_num_riga_intermedio_f <> 0 then
		
			for ll_num_righe = ll_num_riga_intermedio_i to (ll_num_riga_intermedio_f - 1)
				ld_prezzo_acquisto = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "prezzo_acquisto")
				ld_importo_conai = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "importo_conai")
				ld_prezzo_acquisto = ld_prezzo_acquisto + ld_importo_conai					
				ld_sconto_1 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_1")
				ld_sconto_2 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_2")
				ld_sconto_3 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_3")
				ld_sconto_4 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_4")
				ld_sconto_5 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_5")
				if not isnull(ld_sconto_1) then 
					ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_1) / 100
				end if	
				if not isnull(ld_sconto_2) then 
					ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_2) / 100
				end if	
				if not isnull(ld_sconto_3) then 
					ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_3) / 100
				end if	
				if not isnull(ld_sconto_4) then 
					ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_4) / 100
				end if	
				if not isnull(ld_sconto_5) then 
					ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_5) / 100
				end if	
				ld_importo_conai_scon = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "importo_conai_scon")
				ld_prezzo_acquisto = ld_prezzo_acquisto + ld_importo_conai_scon
				// Modifica per Euro
				ld_prezzo_acquisto = ld_prezzo_acquisto * ld_cambio
				
				ld_pezzi_x_conf = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "num_pezzi_confezione")	
				ld_quan_sconto = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "quan_sconto_merce")
				ld_quan_fatturata = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "quan_fatturata")
				
				if dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") = ls_cod_tipo_det_acq_costo or &
					dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") = ls_cod_tipo_det_acq_sconto or &
					dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") = ls_cod_tipo_det_acq_sconto_inc then
					
					choose case dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq")
						case ls_cod_tipo_det_acq_costo		
							ld_costo_speciale = ld_costo_speciale + (ld_prezzo_acquisto * ld_quan_fatturata)
						case ls_cod_tipo_det_acq_sconto
							ld_sconto_speciale = ld_sconto_speciale + (ld_prezzo_acquisto * ld_quan_fatturata)										
						case ls_cod_tipo_det_acq_sconto_inc
							ld_sconto_speciale = ld_sconto_speciale + (ld_prezzo_acquisto * ld_quan_fatturata)
					end choose
					
					dw_det_fat_acq_lista.setitem(ll_num_righe, "prezzo_acq_netto", 0)
					
					ll_prog_riga_fat_acq = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "prog_riga_fat_acq")
					
					update det_fat_acq
						set prezzo_acq_netto = 0
					 where cod_azienda = :s_cs_xx.cod_azienda
						and anno_registrazione = :ll_anno_registrazione
						and num_registrazione = :ll_num_registrazione
						and prog_riga_fat_acq = :ll_prog_riga_fat_acq;
					
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("APICE", "Errore in aggiornamento dati in tabella DET_FAT_ACQ.~n~r" + sqlca.sqlerrtext)
						return
					end if
					
				end if	
			next	
		else
			for ll_num_righe = ll_num_riga_intermedio_i to dw_det_fat_acq_lista.rowcount()
				ld_prezzo_acquisto = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "prezzo_acquisto")
				ld_importo_conai = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "importo_conai")
				ld_prezzo_acquisto = ld_prezzo_acquisto + ld_importo_conai					
				ld_sconto_1 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_1")
				ld_sconto_2 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_2")
				ld_sconto_3 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_3")
				ld_sconto_4 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_4")
				ld_sconto_5 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_5")
				if not isnull(ld_sconto_1) then
					ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_1) / 100
				end if
				if not isnull(ld_sconto_2) then
					ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_2) / 100
				end if
				if not isnull(ld_sconto_3) then
					ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_3) / 100
				end if	
				if not isnull(ld_sconto_4) then 
					ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_4) / 100
				end if	
				if not isnull(ld_sconto_5) then 
					ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_5) / 100
				end if	
				ld_importo_conai_scon = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "importo_conai_scon")
				ld_prezzo_acquisto = ld_prezzo_acquisto + ld_importo_conai_scon
				// Modifica per Euro
				ld_prezzo_acquisto = ld_prezzo_acquisto * ld_cambio
				
				ld_pezzi_x_conf = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "num_pezzi_confezione")	
				ld_quan_sconto = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "quan_sconto_merce")
				ld_quan_fatturata = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "quan_fatturata")
				
				if dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") = ls_cod_tipo_det_acq_costo or &
					dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") = ls_cod_tipo_det_acq_sconto or &
					dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") = ls_cod_tipo_det_acq_sconto_inc then
					
					choose case dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq")
						case ls_cod_tipo_det_acq_costo		
							ld_costo_speciale = ld_costo_speciale + (ld_prezzo_acquisto * ld_quan_fatturata)
						case ls_cod_tipo_det_acq_sconto
							ld_sconto_speciale = ld_sconto_speciale + (ld_prezzo_acquisto * ld_quan_fatturata)
						case ls_cod_tipo_det_acq_sconto_inc
							ld_sconto_speciale = ld_sconto_speciale + (ld_prezzo_acquisto * ld_quan_fatturata)
					end choose
					
					dw_det_fat_acq_lista.setitem(ll_num_righe, "prezzo_acq_netto", 0)
					
					ll_prog_riga_fat_acq = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "prog_riga_fat_acq")
					
					update det_fat_acq
						set prezzo_acq_netto = 0
					 where cod_azienda = :s_cs_xx.cod_azienda
						and anno_registrazione = :ll_anno_registrazione
						and num_registrazione = :ll_num_registrazione
						and prog_riga_fat_acq = :ll_prog_riga_fat_acq;
					
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("APICE", "Errore in aggiornamento dati in tabella DET_FAT_ACQ.~n~r" + sqlca.sqlerrtext)
						return
					end if
					
				end if	
			next		
			
		end if
		
		//sommo a valori
		
		for ll_num_righe = 1 to (ll_num_riga_intermedio_i - 1)
			ld_prezzo_acquisto = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "prezzo_acquisto")
			ld_importo_conai = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "importo_conai")
			ld_prezzo_acquisto = ld_prezzo_acquisto + ld_importo_conai
			ld_sconto_1 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_1")
			ld_sconto_2 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_2")
			ld_sconto_3 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_3")
			ld_sconto_4 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_4")
			ld_sconto_5 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_5")
			if not isnull(ld_sconto_1) then 
				ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_1) / 100
			end if	
			if not isnull(ld_sconto_2) then 
				ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_2) / 100
			end if	
			if not isnull(ld_sconto_3) then 
				ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_3) / 100
			end if	
			if not isnull(ld_sconto_4) then 
				ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_4) / 100
			end if	
			if not isnull(ld_sconto_5) then 
				ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_5) / 100
			end if	
			ld_importo_conai_scon = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "importo_conai_scon")
			ld_prezzo_acquisto = ld_prezzo_acquisto + ld_importo_conai_scon
			// Modifica per Euro
			ld_prezzo_acquisto = ld_prezzo_acquisto * ld_cambio
			
			ld_pezzi_x_conf = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "num_pezzi_confezione")	
			ld_quan_sconto = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "quan_sconto_merce")
			ld_quan_fatturata = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "quan_fatturata")		
	
// ******************* 29/04/2003 Modifica Michele *******************
			if ls_flag_tipo_ridistribuzione = "S" then
				ld_totale += ld_quan_fatturata
			else
				ld_totale += (ld_prezzo_acquisto * ld_quan_fatturata)
			end if
// ******************* Fine Modifica *******************
			
		next
		
		for ll_num_righe = 1 to (ll_num_riga_intermedio_i - 1)
			ld_prezzo_acquisto = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "prezzo_acquisto")	
			ld_importo_conai = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "importo_conai")
			ld_prezzo_acquisto = ld_prezzo_acquisto + ld_importo_conai
			ld_sconto_1 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_1")
			ld_sconto_2 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_2")
			ld_sconto_3 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_3")
			ld_sconto_4 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_4")
			ld_sconto_5 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_5")
			if not isnull(ld_sconto_1) then 
				ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_1) / 100
			end if	
			if not isnull(ld_sconto_2) then 
				ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_2) / 100
			end if	
			if not isnull(ld_sconto_3) then 
				ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_3) / 100
			end if	
			if not isnull(ld_sconto_4) then 
				ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_4) / 100
			end if	
			if not isnull(ld_sconto_5) then 
				ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_5) / 100
			end if		
			ld_importo_conai_scon = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "importo_conai_scon")
			ld_prezzo_acquisto = ld_prezzo_acquisto + ld_importo_conai_scon
			// Modifica per Euro
			ld_prezzo_acquisto = ld_prezzo_acquisto * ld_cambio
			
			ld_quan_fatturata = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "quan_fatturata")	
			ld_pezzi_x_conf = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "num_pezzi_confezione")
			if dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") <> ls_cod_tipo_det_acq_costo and &
				dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") <> ls_cod_tipo_det_acq_sconto and &
				dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq") <> ls_cod_tipo_det_acq_sconto_inc then	
				
				ls_cod_tipo_det_acq = dw_det_fat_acq_lista.getitemstring(ll_num_righe, "cod_tipo_det_acq")
				
				select flag_tipo_det_acq
				  into :ls_flag_tipo_det_acq
				  from tab_tipi_det_acq
				 where cod_azienda = :s_cs_xx.cod_azienda 
					and cod_tipo_det_acq = :ls_cod_tipo_det_acq;
				
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice", "Errore in lettura dati in tabella TAB_TIPI_DET_ACQ.~n~r" + sqlca.sqlerrtext)
					return			
				end if													
				
				if ls_flag_tipo_det_acq = "M" or ls_flag_tipo_det_acq = "N" or ls_flag_tipo_det_acq = "C" then
//																									--> AGGIUNTO ANCHE CASO PRODOTTI NON COLLEGATI EnMe 31/12/2002

// ******************* 29/04/2003 Modifica Michele *******************
					ld_quan_sconto = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "quan_sconto_merce")
					if isnull(ld_quan_sconto) then
						ld_quan_sconto = 0
					end if
					if ld_quan_sconto > 0 then
						ld_prezzo_arrotondato = (ld_quan_fatturata * ld_prezzo_acquisto) / (ld_quan_fatturata + ld_quan_sconto * ld_pezzi_x_conf)
						dw_det_fat_acq_lista.setitem(ll_num_righe, "prezzo_acq_netto", ld_prezzo_arrotondato)
					end if
// ******************* Fine Modifica *******************

					if ls_flag_tipo_ridistribuzione = "S" then
						ld_percentuale = (ld_quan_fatturata * 100) / ld_totale
					else	
						ld_percentuale = ((ld_prezzo_acquisto * ld_quan_fatturata) * 100) / ld_totale
					end if
					ld_sconto_riga = ld_sconto_speciale * ld_percentuale / 100
					ld_costo_riga = ld_costo_speciale * ld_percentuale / 100
					ld_val_riga_netto = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "prezzo_acq_netto")				
					
// ******************* 29/04/2003 Modifica Michele *******************
					ld_quan_fatturata += ld_quan_sconto * ld_pezzi_x_conf
// ******************* Fine Modifica *******************
					
					ld_val_riga_netto = ld_val_riga_netto - (ld_sconto_riga / ld_quan_fatturata) + (ld_costo_riga / ld_quan_fatturata)
					dw_det_fat_acq_lista.setitem(ll_num_righe, "prezzo_acq_netto", ld_val_riga_netto)
					
// ******************* 29/04/2003 Modifica Michele *******************
					ll_prog_riga_fat_acq = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "prog_riga_fat_acq")
					
					update det_fat_acq
						set prezzo_acq_netto = :ld_val_riga_netto
					 where cod_azienda = :s_cs_xx.cod_azienda
						and anno_registrazione = :ll_anno_registrazione
						and num_registrazione = :ll_num_registrazione
						and prog_riga_fat_acq = :ll_prog_riga_fat_acq;
						
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox("APICE", "Errore in aggiornamento dati in tabella DET_FAT_ACQ.~n~r" + sqlca.sqlerrtext)
						return			
					end if
// ******************* Fine Modifica *******************
					
				end if	
			end if
		next		
		
		if ll_num_riga_intermedio_f <> 0 then
			ll_inizio = ll_num_riga_intermedio_f
		else 
			exit
		end if	
		
		ll_num_riga_intermedio_i = ll_num_riga_intermedio_f
		ll_num_riga_intermedio_f = dw_det_fat_acq_lista.rowcount()
		ld_totale = 0
		ld_sconto_speciale = 0
		ld_percentuale = 0
		
	else
		exit
	end if	
loop

ib_calcolo_speciale = true
	
commit;

dw_det_fat_acq_lista.ResetUpdate()
dw_det_fat_acq_lista.postevent("pcd_retrieve")
end event

type cb_stock from cb_stock_ricerca within w_det_fat_acq
event clicked pbm_bnclicked
integer x = 1897
integer y = 964
integer width = 73
integer height = 72
integer taborder = 10
boolean bringtotop = true
end type

event clicked;call super::clicked;s_cs_xx.parametri.parametro_s_10 = dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0

dw_det_fat_acq_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "prog_stock"

window_open(w_ricerca_stock, 0)
end event

type cb_c_industriale from commandbutton within w_det_fat_acq
event clicked pbm_bnclicked
integer x = 2309
integer y = 1280
integer width = 361
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C.Industriale"
end type

event clicked;window_open_parm(w_det_fat_acq_stat,-1,dw_det_fat_acq_lista)
end event

type cb_ripristina from commandbutton within w_det_fat_acq
integer x = 2697
integer y = 1440
integer width = 361
integer height = 80
integer taborder = 112
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Ripristina"
end type

event clicked;long ll_num_righe, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_acq
dec{4} ld_prezzo_acquisto, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, &
		 ld_importo_conai, ld_importo_conai_scon

ib_calcolo_speciale = false

ll_anno_registrazione = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "anno_registrazione")
ll_num_registrazione = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "num_registrazione")

for ll_num_righe = 1 to dw_det_fat_acq_lista.rowcount()
	ld_prezzo_acquisto = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "prezzo_acquisto")
	ld_sconto_1 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_1")
	ld_sconto_2 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_2")
	ld_sconto_3 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_3")
	ld_sconto_4 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_4")
	ld_sconto_5 = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "sconto_5")
	ld_importo_conai = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "importo_conai")
	ld_importo_conai_scon = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "importo_conai_scon")
	
	ld_prezzo_acquisto = ld_prezzo_acquisto + ld_importo_conai
	
	if not isnull(ld_sconto_1) then 
		ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_1) / 100
	end if	
	if not isnull(ld_sconto_2) then 
		ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_2) / 100
	end if	
	if not isnull(ld_sconto_3) then 
		ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_3) / 100
	end if	
	if not isnull(ld_sconto_4) then 
		ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_4) / 100
	end if	
	if not isnull(ld_sconto_5) then 
		ld_prezzo_acquisto = ld_prezzo_acquisto - (ld_prezzo_acquisto * ld_sconto_5) / 100
	end if	

	ld_prezzo_acquisto = ld_prezzo_acquisto + ld_importo_conai_scon	
	
	dw_det_fat_acq_lista.setitem(ll_num_righe, "prezzo_acq_netto", ld_prezzo_acquisto)
	
	ll_prog_riga_fat_acq = dw_det_fat_acq_lista.getitemnumber(ll_num_righe, "prog_riga_fat_acq")
	
	update det_fat_acq
		set prezzo_acq_netto = :ld_prezzo_acquisto
	 where cod_azienda = :s_cs_xx.cod_azienda
		and anno_registrazione = :ll_anno_registrazione
		and num_registrazione = :ll_num_registrazione
		and prog_riga_fat_acq = :ll_prog_riga_fat_acq;
		
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Apice", "Errore in aggiornamento dati in tabella DET_FAT_ACQ.~n~r" + sqlca.sqlerrtext)
		return			
	end if														
	
next	

commit;
dw_det_fat_acq_lista.ResetUpdate( )
dw_det_fat_acq_lista.postevent("pcd_retrieve")
end event

event getfocus;call super::getfocus;dw_det_fat_acq_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
end event

type cb_grezzo from commandbutton within w_det_fat_acq
integer x = 2875
integer y = 444
integer width = 389
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Prod.Grezzo"
end type

event clicked;long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_acq


s_cs_xx.parametri.parametro_s_1 = ""
s_cs_xx.parametri.parametro_s_2 = ""

if isnull(dw_det_fat_acq_lista.getitemstring(dw_det_fat_acq_lista.getrow(), "cod_prodotto_grezzo")) then
	s_cs_xx.parametri.parametro_s_2 = dw_det_fat_acq_lista.getitemstring(dw_det_fat_acq_lista.getrow(), "cod_prodotto")
else
	s_cs_xx.parametri.parametro_s_2 = dw_det_fat_acq_lista.getitemstring(dw_det_fat_acq_lista.getrow(), "cod_prodotto_grezzo")
end if	

window_open(w_det_fat_acq_ricerca_grezzo, 0)

if s_cs_xx.parametri.parametro_s_1 <> "" then
	ll_anno_registrazione = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "num_registrazione")
	ll_prog_riga_fat_acq = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "prog_riga_fat_acq")
	// eseguo update sul database
	update det_fat_acq
	set   cod_prodotto_grezzo = :s_cs_xx.parametri.parametro_s_1,
	      des_prodotto_grezzo = :s_cs_xx.parametri.parametro_s_2,
		   val_grezzo = :s_cs_xx.parametri.parametro_dec4_1
	where cod_azienda = :s_cs_xx.cod_azienda and
	      anno_registrazione = :ll_anno_registrazione and
		   num_registrazione = :ll_num_registrazione and
		   prog_riga_fat_acq = :ll_prog_riga_fat_acq;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in aggiornamento riga con dati del prodotto GREZZO.~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	commit;
	
	// ricarico le righe e mi riposizioni su quella corrente
	dw_det_fat_acq_lista.change_dw_current()
	il_riga_corrente = dw_det_fat_acq_lista.getrow()
	parent.triggerevent("pc_retrieve")
	
	dw_det_fat_acq_lista.setrow(il_riga_corrente)
end if
end event

type cb_prodotti_grezzi_ricerca from cb_prod_ricerca within w_det_fat_acq
integer x = 2770
integer y = 448
integer width = 64
integer height = 72
integer taborder = 50
boolean bringtotop = true
end type

event getfocus;call super::getfocus;dw_det_fat_acq_det_3.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto_grezzo"
end event

type dw_det_fat_acq_lista from uo_cs_xx_dw within w_det_fat_acq
event ue_ultimo ( )
event ue_dopo_rowfc ( )
integer x = 69
integer y = 140
integer width = 3401
integer height = 920
integer taborder = 40
string dataobject = "d_det_fat_acq_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

event ue_ultimo;call super::ue_ultimo;string ls_flag_memo, ls_cod_prodotto
double ld_prezzo_acquisto
long ll_i

for ll_i = 1 to this.rowcount()
	ls_flag_memo = this.getitemstring(ll_i, "flag_memo_prezzo_acq")	
	ld_prezzo_acquisto = this.getitemnumber(ll_i, "prezzo_acquisto")	
	ls_cod_prodotto = this.getitemstring(ll_i, "cod_prodotto")	
	if ls_flag_memo = "S" then
		update anag_prodotti
			set prezzo_acquisto = :ld_prezzo_acquisto 
		 where cod_azienda = :s_cs_xx.cod_azienda
			and cod_prodotto = :ls_cod_prodotto;	
			
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Apice", "Errore in aggionamento dati in tabella ANAG_PRODOTTI. ~n~r" + sqlca.sqlerrtext)
			return
			exit
		end if	
		
		commit;
	end if	
next
end event

event ue_dopo_rowfc();string ls_cod_fornitore, ls_cod_iva, ls_cod_tipo_det_acq, ls_modify, &
		 ls_cod_prodotto, ls_cod_misura_mag
datetime ldt_data_doc_origine, ldt_data_esenzione_iva
double ld_quan_fatturata, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, &
		 ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_prezzo_acquisto, ld_sconti[], ld_sconto_calcolato, ld_prezzo_scontato, &
		 ld_valore_totale, ld_num_confezioni, ld_num_pezzi_confezione, ld_importo_conai, ld_importo_conai_scon, &
		 ld_quan_sconto_merce
long ll_anno_registrazione, ll_num_registrazione

if this.getrow() > 0 then
	
	ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")
	ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")
	ls_cod_fornitore = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_fornitore")
	
	select b.quan_fatturata,
			 b.sconto_1,
			 b.sconto_2,
			 b.sconto_3,
			 b.sconto_4,
			 b.sconto_5,
			 b.sconto_6,
			 b.sconto_7,
			 b.sconto_8,
			 b.sconto_9,
			 b.sconto_10,
			 b.prezzo_acquisto,
			 b.num_confezioni,
			 b.num_pezzi_confezione,
			 b.importo_conai,
			 b.importo_conai_scon,
			 b.quan_sconto_merce, 
			 b.cod_iva
	  into :ld_quan_fatturata,
			 :ld_sconto_1,
			 :ld_sconto_2,
			 :ld_sconto_3,
			 :ld_sconto_4,
			 :ld_sconto_5,
			 :ld_sconto_6,
			 :ld_sconto_7,
			 :ld_sconto_8,
			 :ld_sconto_9,
			 :ld_sconto_10,
			 :ld_prezzo_acquisto,
			 :ld_num_confezioni,
			 :ld_num_pezzi_confezione,
			 :ld_importo_conai,
			 :ld_importo_conai_scon,
			 :ld_quan_sconto_merce,
			 :ls_cod_iva
	  from tes_fat_acq a, det_fat_acq b
	 where b.cod_azienda = :s_cs_xx.cod_azienda
		and b.cod_prodotto = :ls_cod_prodotto
		and b.anno_registrazione = :ll_anno_registrazione
		and b.num_registrazione in (select max(b.num_registrazione)
												from tes_fat_acq a, det_fat_acq b
											  where b.cod_azienda = :s_cs_xx.cod_azienda
												 and b.anno_registrazione	= :ll_anno_registrazione
												 and b.cod_prodotto = :ls_cod_prodotto
												 and a.cod_azienda = b.cod_azienda
												 and a.anno_registrazione = b.anno_registrazione
												 and a.num_registrazione = b.num_registrazione
												 and a.cod_fornitore = :ls_cod_fornitore
												 and a.num_registrazione < :ll_num_registrazione)		
		and b.prog_riga_fat_acq in (select max(b.prog_riga_fat_acq)
												from tes_fat_acq a, det_fat_acq b
											  where b.cod_azienda = :s_cs_xx.cod_azienda
												 and b.anno_registrazione	= :ll_anno_registrazione
												 and b.cod_prodotto = :ls_cod_prodotto
												 and a.cod_azienda = b.cod_azienda
												 and a.anno_registrazione = b.anno_registrazione
												 and a.num_registrazione = b.num_registrazione
												 and a.cod_fornitore = :ls_cod_fornitore
												 and a.num_registrazione in (select max(b.num_registrazione)
																						from tes_fat_acq a, det_fat_acq b
																					  where b.cod_azienda = :s_cs_xx.cod_azienda
																						 and b.anno_registrazione	= :ll_anno_registrazione
																						 and b.cod_prodotto = :ls_cod_prodotto
																						 and a.cod_azienda = b.cod_azienda
																						 and a.anno_registrazione = b.anno_registrazione
																						 and a.num_registrazione = b.num_registrazione
																						 and a.cod_fornitore = :ls_cod_fornitore
																						 and a.num_registrazione < :ll_num_registrazione))															 
		and a.cod_azienda = b.cod_azienda
		and a.anno_registrazione = b.anno_registrazione
		and a.num_registrazione = b.num_registrazione
		and a.cod_fornitore = :ls_cod_fornitore
		and a.num_registrazione < :ll_num_registrazione;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati da tabella TES_FAT_ACQ e DET_FAT_ACQ " + sqlca.sqlerrtext)
		return
	elseif sqlca.sqlcode = 0 then			
		ld_sconti[1] = ld_sconto_1
		ld_sconti[2] = ld_sconto_2
		ld_sconti[3] = ld_sconto_3
		ld_sconti[4] = ld_sconto_4
		ld_sconti[5] = ld_sconto_5
		ld_sconti[6] = ld_sconto_6
		ld_sconti[7] = ld_sconto_7
		ld_sconti[8] = ld_sconto_8
		ld_sconti[9] = ld_sconto_9
		ld_sconti[10] = ld_sconto_10

		f_calcola_sconto(ld_sconti[], ld_prezzo_acquisto, ld_sconto_calcolato, ld_prezzo_scontato)
		ld_valore_totale = ld_quan_fatturata * ld_prezzo_scontato
		
		dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "num_confezioni", ld_num_confezioni)
		dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "num_pezzi_confezione", ld_num_pezzi_confezione)
		dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "quan_fatturata", ld_quan_fatturata)
		dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "quan_sconto_merce", ld_quan_sconto_merce)			
		dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "prezzo_acquisto", ld_prezzo_acquisto)
		dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "importo_conai", ld_importo_conai)
		dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "sconto_1", ld_sconto_1)			
		dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "sconto_2", ld_sconto_2)			
		dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "sconto_3", ld_sconto_3)			
		dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "sconto_4", ld_sconto_4)			
		dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "sconto_5", ld_sconto_5)						
		dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "importo_conai_scon", ld_importo_conai_scon)			
		dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "cod_iva", ls_cod_iva)			
											
	end if  
end if	
end event

event pcd_new;call super::pcd_new;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_acq, ls_cod_tipo_fat_acq, ls_flag_tipo_det_acq, &
          ls_modify, ls_null, ls_cod_fornitore, ls_cod_iva, ls_cod_doc_origine, &
			 ls_numeratore_doc_origine, ls_cod_deposito, ls_cod_ubicazione, &
			 ls_cod_tipo_movimento, ls_messaggio
   long ll_anno_doc_origine, ll_num_doc_origine, ll_prog_riga_fat_acq, ll_null, ll_anno_registrazione, ll_num_registrazione, &
		  ll_numero_riga
   datetime ldt_data_esenzione_iva, ldt_data_doc_origine, ldt_null, ldt_data_documento
	real lr_progressivo

   setnull(ls_null)
	setnull(ll_null)
	setnull(ldt_null)

   ls_cod_doc_origine = dw_det_fat_acq_lista.i_parentdw.getitemstring(dw_det_fat_acq_lista.i_parentdw.getrow(), "cod_doc_origine")
   ls_numeratore_doc_origine = dw_det_fat_acq_lista.i_parentdw.getitemstring(dw_det_fat_acq_lista.i_parentdw.getrow(), "numeratore_doc_origine")
   ll_anno_doc_origine = dw_det_fat_acq_lista.i_parentdw.getitemnumber(dw_det_fat_acq_lista.i_parentdw.getrow(), "anno_doc_origine")
   ll_num_doc_origine = dw_det_fat_acq_lista.i_parentdw.getitemnumber(dw_det_fat_acq_lista.i_parentdw.getrow(), "num_doc_origine")
   lr_progressivo = dw_det_fat_acq_lista.i_parentdw.getitemnumber(dw_det_fat_acq_lista.i_parentdw.getrow(), "progressivo")
   ldt_data_doc_origine = dw_det_fat_acq_lista.i_parentdw.getitemdatetime(dw_det_fat_acq_lista.i_parentdw.getrow(), "data_doc_origine")
   ls_cod_fornitore = dw_det_fat_acq_lista.i_parentdw.getitemstring(dw_det_fat_acq_lista.i_parentdw.getrow(), "cod_fornitore")
	ll_anno_registrazione = dw_det_fat_acq_lista.i_parentdw.getitemnumber(dw_det_fat_acq_lista.i_parentdw.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_det_fat_acq_lista.i_parentdw.getitemnumber(dw_det_fat_acq_lista.i_parentdw.getrow(), "num_registrazione")

   select max(det_fat_acq.prog_riga_fat_acq)
   into   :ll_prog_riga_fat_acq
   from   det_fat_acq
   where  det_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and
			 det_fat_acq.anno_registrazione = :ll_anno_registrazione and 
			 det_fat_acq.num_registrazione = :ll_num_registrazione;

   if isnull(ll_prog_riga_fat_acq) then
      dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(), "prog_riga_fat_acq", 10)
   else
      dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(), "prog_riga_fat_acq", ll_prog_riga_fat_acq + 10)
   end if

   ls_cod_tipo_fat_acq = dw_det_fat_acq_lista.i_parentdw.getitemstring(dw_det_fat_acq_lista.i_parentdw.getrow(), "cod_tipo_fat_acq")

   select tab_tipi_fat_acq.cod_tipo_det_acq
   into   :ls_cod_tipo_det_acq
   from   tab_tipi_fat_acq
   where  tab_tipi_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_fat_acq.cod_tipo_fat_acq = :ls_cod_tipo_fat_acq;
   
   if sqlca.sqlcode = 0 then
      dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(), "cod_tipo_det_acq", ls_cod_tipo_det_acq)
      if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
         select tab_tipi_det_acq.cod_iva  
         into   :ls_cod_iva  
         from   tab_tipi_det_acq  
         where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
                tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;
         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
         end if
      end if

   else
      ls_cod_tipo_det_acq = ls_null
   end if

   dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(), "quan_fatturata", 1)
   
   if not isnull(ls_cod_fornitore) then
      select anag_fornitori.cod_iva,
             anag_fornitori.data_esenzione_iva
      into   :ls_cod_iva,
             :ldt_data_esenzione_iva
      from   anag_fornitori
      where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
             anag_fornitori.cod_fornitore = :ls_cod_fornitore;
   end if
   
   if sqlca.sqlcode = 0 then
      if ls_cod_iva <> "" and &
         (ldt_data_esenzione_iva <= ldt_data_doc_origine or isnull(ldt_data_esenzione_iva)) then
         this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
      end if
   end if
   
   ls_modify = "cod_tipo_det_acq.protect='0'~t"
   dw_det_fat_acq_det_1.modify(ls_modify)
   ls_modify = "cod_tipo_det_acq.background.color='16777215'~t"
   dw_det_fat_acq_det_1.modify(ls_modify)

   if not isnull(ls_cod_tipo_det_acq) then
      dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(), "cod_tipo_det_acq", ls_cod_tipo_det_acq)
      ld_datawindow = dw_det_fat_acq_det_1
		lp_prod_view = pb_prod_view
		setnull(lc_prodotti_ricerca)
      f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_acq)
		
		//------------------- aggiunta  -------------------------//
		
		
	
		select tab_tipi_det_acq.flag_tipo_det_acq,
				 tab_tipi_det_acq.cod_tipo_movimento
		into   :ls_flag_tipo_det_acq,
				 :ls_cod_tipo_movimento
		from   tab_tipi_det_acq
		where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;
	
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
						  exclamation!, ok!)
		end if
	
		if ls_flag_tipo_det_acq = "M" then
			cb_stock.enabled = true
			dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(), "cod_deposito", ls_cod_deposito)
			dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(), "cod_tipo_movimento", ls_cod_tipo_movimento)
			
			ll_numero_riga = ll_prog_riga_fat_acq + 10
			//if f_proponi_stock_acq(ls_cod_tipo_det_acq, dw_det_fat_acq_det_1, ll_numero_riga, ls_messaggio ) <> 0 then
			if f_proponi_stock_acq(ls_cod_tipo_det_acq, dw_det_fat_acq_det_1, dw_det_fat_acq_det_1.getrow(), ls_messaggio ) <> 0 then
				g_mb.messagebox("APICE",ls_messaggio)
			end if
		else
			if not isnull(dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_tipo_movimento")) then
				dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"cod_tipo_movimento",ls_null)
			end if
			if not isnull(dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_deposito")) then
				dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"cod_deposito",ls_null)
			end if
			if not isnull(dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_ubicazione")) then
				dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"cod_ubicazione",ls_null)
			end if
			if not isnull(dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_lotto")) then
				dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"cod_lotto",ls_null)
			end if
			if not isnull(dw_det_fat_acq_det_1.getitemdatetime(dw_det_fat_acq_det_1.getrow(),"data_stock")) then
				dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"data_stock",ldt_null)
			end if
			if not isnull(dw_det_fat_acq_det_1.getitemnumber(dw_det_fat_acq_det_1.getrow(),"prog_stock")) then
				dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"prog_stock",ll_null)
			end if
	
			ls_modify = "cod_tipo_movimento.protect='1'~t"
			ls_modify = ls_modify + "cod_tipo_movimento.background.color='12632256'~t"
			ls_modify = ls_modify + "cod_deposito.protect='1'~t"
			ls_modify = ls_modify + "cod_deposito.background.color='12632256'~t"
			ls_modify = ls_modify + "cod_ubicazione.protect='1'~t"
			ls_modify = ls_modify + "cod_ubicazione.background.color='12632256'~t"
			ls_modify = ls_modify + "cod_lotto.protect='1'~t"
			ls_modify = ls_modify + "cod_lotto.background.color='12632256'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
		
	   end if
		
		
		//------------------- fine -----------------------------------//
		
   else
      ls_modify = "cod_prodotto.protect='1'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "cod_prodotto.background.color='12632256'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "des_prodotto.protect='1'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "des_prodotto.background.color='12632256'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "cod_misura.protect='1'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "cod_misura.background.color='12632256'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "fat_conversione.protect='1'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "fat_conversione.background.color='12632256'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "quan_fatturata.protect='1'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "quan_fatturata.background.color='12632256'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "prezzo_acquisto.protect='1'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "prezzo_acquisto.background.color='12632256'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_1.protect='1'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_1.background.color='12632256'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_2.protect='1'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "sconto_2.background.color='12632256'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "cod_iva.protect='1'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "cod_iva.background.color='12632256'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "cod_deposito.protect='1'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "cod_deposito.background.color='12632256'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "cod_ubicazione.protect='1'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "cod_ubicazione.background.color='12632256'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "cod_lotto.protect='1'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "cod_lotto.background.color='12632256'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "cod_tipo_movimento.protect='1'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "cod_tipo_movimento.background.color='12632256'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "cod_conto.protect='1'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
      ls_modify = "cod_conto.background.color='12632256'~t"
      dw_det_fat_acq_det_1.modify(ls_modify)
   end if

   ib_nuovo = true
   cb_sconti.enabled = true
	cb_prezzo.enabled=true
	cb_stock.enabled = true
	cb_c_industriale.enabled = false
	cb_grezzo.enabled=false
	cb_prodotti_grezzi_ricerca.enabled=true
	
	string ls_flag
	
	setnull(ls_flag)

	select flag
	into   :ls_flag
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 flag_parametro = 'F' and
			 cod_parametro = 'MFA';
			 
	if sqlca.sqlcode <> 0 or isnull(ls_flag) then
		ls_flag = "S"
	end if
	
	if ls_flag <> "S" then
		ls_flag = "N"
	end if

	dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "flag_memo_prezzo_acq", ls_flag)
	dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "importo_conai", 0)
	dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "importo_conai_scon", 0)

	dw_folder.fu_disabletab(4)

end if

cb_agg_mov_mag.enabled = false
end event

event pcd_view;call super::pcd_view;if i_extendmode then
   ib_modifica = false
   ib_nuovo = false
   dw_det_fat_acq_det_1.object.b_ricerca_prodotto.enabled = false
	pb_prod_view.enabled = false
   cb_sconti.enabled = false
	cb_prezzo.enabled=false
	cb_stock.enabled = false
	cb_grezzo.enabled=true
	cb_prodotti_grezzi_ricerca.enabled=false
	if this.getrow() > 0 then
		if this.getitemnumber(this.getrow(), "anno_registrazione") > 0 then
			cb_c_industriale.enabled=true
		end if
	else
		cb_c_industriale.enabled=false
	end if
	
	dw_folder.fu_enabletab(4)
	
end if

cb_agg_mov_mag.enabled = true
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_fornitore, ls_cod_iva, ls_cod_tipo_det_acq, ls_modify, &
          ls_cod_prodotto, ls_cod_misura_mag
   datetime ldt_data_doc_origine, ldt_data_esenzione_iva
	double ld_quan_fatturata, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, &
			 ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_prezzo_acquisto, ld_sconti[], ld_sconto_calcolato, ld_prezzo_scontato, &
			 ld_valore_totale, ld_num_confezioni, ld_num_pezzi_confezione, ld_importo_conai, ld_importo_conai_scon, &
			 ld_quan_sconto_merce
	long ll_anno_registrazione, ll_num_registrazione
	long ll_anno_bolla_acq, ll_num_bolla_acq, ll_prog_riga_bolla_acq
	string ls_bolla_acq
				 
	ls_cod_fornitore = i_parentdw.getitemstring(i_parentdw.getrow(), "cod_fornitore")
   	ldt_data_doc_origine = i_parentdw.getitemdatetime(i_parentdw.getrow(), "data_doc_origine")

	if this.getrow() > 0 then
		
		//visualizzazione numero bolla acquisto della riga selezionata ------------------------
		ll_anno_bolla_acq = getitemnumber(getrow(), "anno_bolla_acq")
		ll_num_bolla_acq = getitemnumber(getrow(), "num_bolla_acq")
		ll_prog_riga_bolla_acq = getitemnumber(getrow(), "prog_riga_bolla_acq")
		
		if not isnull(ll_anno_bolla_acq) and not isnull(ll_num_bolla_acq) and not isnull(ll_prog_riga_bolla_acq) and &
			ll_anno_bolla_acq>0 and ll_num_bolla_acq>0 and ll_prog_riga_bolla_acq>0 then
			
			ls_bolla_acq = string(ll_anno_bolla_acq) + " / " + string(ll_num_bolla_acq) + " / " + string(ll_prog_riga_bolla_acq)
			st_colonna_bolla_acquisto.text = ls_bolla_acq
		else
			st_colonna_bolla_acquisto.text = ""
		end if
		//------------------------------------------------------------------------------------------------
				
		ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")		

      select anag_prodotti.cod_misura_mag
      into   :ls_cod_misura_mag
      from   anag_prodotti
      where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
             anag_prodotti.cod_prodotto = :ls_cod_prodotto;

		if sqlca.sqlcode = 0 then
	   	dw_det_fat_acq_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
	   	dw_det_fat_acq_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")
		else
	   	dw_det_fat_acq_det_1.modify("st_prezzo.text='Prezzo:'")
	   	dw_det_fat_acq_det_1.modify("st_quantita.text='Quantità:'")
		end if

		if not isnull(this.getitemstring(this.getrow(),"cod_misura")) then
		   cb_prezzo.text = "Prezzo al " + this.getitemstring(this.getrow(),"cod_misura")
		else
		   cb_prezzo.text = "Prezzo"
		end if

		if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
			len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
			len(trim(ls_cod_misura_mag)) <> 0 then
			dw_det_fat_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
		else
			dw_det_fat_acq_det_1.modify("st_fattore_conv.text=''")		
		end if
		
		select anag_fornitori.cod_iva,
				 anag_fornitori.data_esenzione_iva
		into   :ls_cod_iva,
				 :ldt_data_esenzione_iva
		from   anag_fornitori
		where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
				 anag_fornitori.cod_fornitore = :ls_cod_fornitore;
	
		if sqlca.sqlcode = 0 then
			if ls_cod_iva <> "" and ldt_data_esenzione_iva <= ldt_data_doc_origine then
				f_po_loaddddw_dw(dw_det_fat_acq_det_1, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_calcolo = 'N'")
			else
				f_po_loaddddw_dw(dw_det_fat_acq_det_1, &
									  "cod_iva", &
									  sqlca, &
									  "tab_ive", &
									  "cod_iva", &
									  "des_iva", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
			end if
		end if
	
		if ib_stato_modifica or ib_stato_nuovo then
			ls_cod_tipo_det_acq = dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(), "cod_tipo_det_acq")
			ld_datawindow = dw_det_fat_acq_det_1
			lp_prod_view = pb_prod_view
			setnull(lc_prodotti_ricerca)
			f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_acq)
		
			if not isnull(getitemnumber(getrow(),"anno_reg_mov_mag_ret")) and getitemnumber(getrow(),"anno_reg_mov_mag_ret") > 0 then
				ls_modify = "cod_deposito.protect='1'~t"
				ls_modify = ls_modify + "cod_deposito.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_ubicazione.protect='1'~t"
				ls_modify = ls_modify + "cod_ubicazione.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_lotto.protect='1'~t"
				ls_modify = ls_modify + "cod_lotto.background.color='12632256'~t"
				ls_modify = ls_modify + "data_stock.protect='1'~t"
				ls_modify = ls_modify + "data_stock.background.color='12632256'~t"
				ls_modify = ls_modify + "prog_stock.protect='1'~t"
				ls_modify = ls_modify + "prog_stock.background.color='12632256'~t"
				ls_modify = ls_modify + "quan_fatturata.protect='1'~t"
				ls_modify = ls_modify + "quan_fatturata.background.color='12632256'~t"
				ls_modify = ls_modify + "prezzo_acquisto.protect='1'~t"
				ls_modify = ls_modify + "prezzo_acquisto.background.color='12632256'~t"
				dw_det_fat_acq_det_1.modify(ls_modify)
			else				
				ls_modify = "cod_deposito.protect='0'~t"
				ls_modify = ls_modify + "cod_deposito.background.color='16777215'~t"
				ls_modify = ls_modify + "cod_ubicazione.protect='0'~t"
				ls_modify = ls_modify + "cod_ubicazione.background.color='16777215'~t"
				ls_modify = ls_modify + "cod_lotto.protect='0'~t"
				ls_modify = ls_modify + "cod_lotto.background.color='16777215'~t"
				ls_modify = ls_modify + "data_stock.protect='0'~t"
				ls_modify = ls_modify + "data_stock.background.color='16777215'~t"
				ls_modify = ls_modify + "prog_stock.protect='0'~t"
				ls_modify = ls_modify + "prog_stock.background.color='16777215'~t"
				ls_modify = ls_modify + "quan_fatturata.protect='0'~t"
				ls_modify = ls_modify + "quan_fatturata.background.color='16777215'~t"
				ls_modify = ls_modify + "prezzo_acquisto.protect='0'~t"
				ls_modify = ls_modify + "prezzo_acquisto.background.color='16777215'~t"
				dw_det_fat_acq_det_1.modify(ls_modify)
			end if
		end if
	
		if ib_nuovo then
			ls_modify = "cod_tipo_det_acq.protect='0~tif(isrownew(),0,1)'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_tipo_det_acq.background.color='16777215~tif(isrownew(),16777215,12632256)'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
		end if
		
		ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")
		ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
		ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")
		
		select b.quan_fatturata,
				 b.sconto_1,
				 b.sconto_2,
				 b.sconto_3,
				 b.sconto_4,
				 b.sconto_5,
				 b.sconto_6,
				 b.sconto_7,
				 b.sconto_8,
				 b.sconto_9,
				 b.sconto_10,
				 b.prezzo_acquisto,
				 b.num_confezioni,
				 b.num_pezzi_confezione,
				 b.importo_conai,
				 b.importo_conai_scon,
				 b.quan_sconto_merce
		  into :ld_quan_fatturata,
				 :ld_sconto_1,
				 :ld_sconto_2,
				 :ld_sconto_3,
				 :ld_sconto_4,
				 :ld_sconto_5,
				 :ld_sconto_6,
				 :ld_sconto_7,
				 :ld_sconto_8,
				 :ld_sconto_9,
				 :ld_sconto_10,
				 :ld_prezzo_acquisto,
				 :ld_num_confezioni,
				 :ld_num_pezzi_confezione,
				 :ld_importo_conai,
				 :ld_importo_conai_scon,
				 :ld_quan_sconto_merce
		  from tes_fat_acq a, det_fat_acq b
		 where b.cod_azienda = :s_cs_xx.cod_azienda
		   and b.cod_prodotto = :ls_cod_prodotto
		   and b.anno_registrazione = :ll_anno_registrazione
		   and b.num_registrazione in (select max(b.num_registrazione)
												   from tes_fat_acq a, det_fat_acq b
												  where b.cod_azienda = :s_cs_xx.cod_azienda
												    and b.anno_registrazione	= :ll_anno_registrazione
												    and b.cod_prodotto = :ls_cod_prodotto
													 and a.cod_azienda = b.cod_azienda
													 and a.anno_registrazione = b.anno_registrazione
													 and a.num_registrazione = b.num_registrazione
													 and a.cod_fornitore = :ls_cod_fornitore
													 and a.num_registrazione < :ll_num_registrazione)		
			and b.prog_riga_fat_acq in (select max(b.prog_riga_fat_acq)
													from tes_fat_acq a, det_fat_acq b
												  where b.cod_azienda = :s_cs_xx.cod_azienda
													 and b.anno_registrazione	= :ll_anno_registrazione
													 and b.cod_prodotto = :ls_cod_prodotto
													 and a.cod_azienda = b.cod_azienda
													 and a.anno_registrazione = b.anno_registrazione
													 and a.num_registrazione = b.num_registrazione
													 and a.cod_fornitore = :ls_cod_fornitore
													 and a.num_registrazione in (select max(b.num_registrazione)
																							from tes_fat_acq a, det_fat_acq b
																						  where b.cod_azienda = :s_cs_xx.cod_azienda
																							 and b.anno_registrazione	= :ll_anno_registrazione
																							 and b.cod_prodotto = :ls_cod_prodotto
																							 and a.cod_azienda = b.cod_azienda
																							 and a.anno_registrazione = b.anno_registrazione
																							 and a.num_registrazione = b.num_registrazione
																							 and a.cod_fornitore = :ls_cod_fornitore
																							 and a.num_registrazione < :ll_num_registrazione))															 
		   and a.cod_azienda = b.cod_azienda
		   and a.anno_registrazione = b.anno_registrazione
		   and a.num_registrazione = b.num_registrazione
		   and a.cod_fornitore = :ls_cod_fornitore
			and a.num_registrazione < :ll_num_registrazione;
			
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice", "Errore in lettura dati da tabella TES_FAT_ACQ e DET_FAT_ACQ " + sqlca.sqlerrtext)
			dw_supporto_dati.reset()		
			dw_supporto_dati.insertrow(0)			
		elseif sqlca.sqlcode = 100 then
			dw_supporto_dati.reset()
			dw_supporto_dati.insertrow(0)			
		elseif sqlca.sqlcode = 0 then			
			dw_supporto_dati.reset()
			dw_supporto_dati.insertrow(0)
			dw_supporto_dati.setitem(1, "ultimo_prezzo_pagato_l", ld_prezzo_acquisto)
			dw_supporto_dati.setitem(1, "sconto_1", ld_sconto_1)
			dw_supporto_dati.setitem(1, "sconto_2", ld_sconto_2)
			dw_supporto_dati.setitem(1, "sconto_3", ld_sconto_3)
			dw_supporto_dati.setitem(1, "sconto_4", ld_sconto_4)
			dw_supporto_dati.setitem(1, "sconto_5", ld_sconto_5)
			dw_supporto_dati.setitem(1, "sconto_6", ld_sconto_6)
			dw_supporto_dati.setitem(1, "sconto_7", ld_sconto_7)
			dw_supporto_dati.setitem(1, "sconto_8", ld_sconto_8)
			dw_supporto_dati.setitem(1, "sconto_9", ld_sconto_9)
			dw_supporto_dati.setitem(1, "sconto_10", ld_sconto_10)
						
			ld_sconti[1] = ld_sconto_1
			ld_sconti[2] = ld_sconto_2
			ld_sconti[3] = ld_sconto_3
			ld_sconti[4] = ld_sconto_4
			ld_sconti[5] = ld_sconto_5
			ld_sconti[6] = ld_sconto_6
			ld_sconti[7] = ld_sconto_7
			ld_sconti[8] = ld_sconto_8
			ld_sconti[9] = ld_sconto_9
			ld_sconti[10] = ld_sconto_10

			f_calcola_sconto(ld_sconti[], ld_prezzo_acquisto, ld_sconto_calcolato, ld_prezzo_scontato)
			dw_supporto_dati.setitem(1, "ultimo_prezzo_pagato_n", ld_prezzo_scontato)			
			dw_supporto_dati.setitem(1, "quan_acquistata", ld_quan_fatturata)				
			dw_supporto_dati.setitem(1, "sconto_tot", ld_sconto_calcolato)							
			ld_valore_totale = ld_quan_fatturata * ld_prezzo_scontato
			dw_supporto_dati.setitem(1, "valore_totale", ld_valore_totale)		
			
		end if  
		
	end if
	
end if

end event

event updatestart;call super::updatestart;if i_extendmode then
	string   ls_messaggio, ls_cod_tipo_det_acq, ls_flag_tipo_det_acq, ls_cod_prodotto, ls_cod_tipo_movimento, &
				ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto
	long     ll_i, ll_prog_riga_fat_acq, ll_anno_registrazione, ll_num_registrazione, ll_prog_stock, &
			   ll_anno_mov, ll_num_mov
	datetime ldt_data_stock
	dec{4}   ld_prezzo

	ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")

   for ll_i = 1 to this.rowcount()
		
		ll_anno_mov = getitemnumber(ll_i, "anno_registrazione_mov_mag")
		ll_num_mov = getitemnumber(ll_i, "num_registrazione_mov_mag")
		ld_prezzo = getitemnumber(ll_i, "prezzo_acquisto")
		

// **** michela: commentato in data 25/03/2004 su richiesta di marco; per aggiornare il movimento di magazzino
// **** utilizzare l'apposito pulsante
// ****
//		if not isnull(ll_anno_mov) and not isnull(ll_num_mov) then
//			
//			if isnull(ld_prezzo) then
//				ld_prezzo = 0
//			end if
//			
//			update mov_magazzino
//			set    val_movimento = :ld_prezzo
//			where  cod_azienda = :s_cs_xx.cod_azienda and
//					 anno_registrazione = :ll_anno_mov and
//					 num_registrazione = :ll_num_mov;
//					 
//			if sqlca.sqlcode <> 0 then
//				messagebox("APICE","Errore in aggiornamento movimento di magazzino: " + sqlca.sqlerrtext)
//				return 1
//			end if
//			
//		end if
// *****
// *****
		
		
		ls_cod_tipo_det_acq = this.getitemstring(ll_i, "cod_tipo_det_acq")

	   select flag_tipo_det_acq
	   into   :ls_flag_tipo_det_acq
		from   tab_tipi_det_acq
	   where  cod_azienda = :s_cs_xx.cod_azienda and 
	          cod_tipo_det_acq = :ls_cod_tipo_det_acq;
	   if sqlca.sqlcode = -1 then
	      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", exclamation!, ok!)
			return 1
	   end if
		
		if ls_flag_tipo_det_acq = "C" or ls_flag_tipo_det_acq = "M" then
			ls_cod_prodotto = this.getitemstring(ll_i, "cod_prodotto")
			if isnull(ls_cod_prodotto) then
				g_mb.messagebox("Attenzione", "Indicazione del codice prodotto obbligatoria.", stopsign!, ok!)
				return 1
			end if
		end if	
		
		if ls_flag_tipo_det_acq = "M" then
			ls_cod_prodotto = this.getitemstring(ll_i, "cod_prodotto")
			ls_cod_tipo_movimento = this.getitemstring(ll_i, "cod_tipo_movimento")
			ls_cod_deposito = this.getitemstring(ll_i, "cod_deposito")
			ls_cod_ubicazione = this.getitemstring(ll_i, "cod_ubicazione")
			ls_cod_lotto = this.getitemstring(ll_i, "cod_lotto")
			ldt_data_stock = this.getitemdatetime(ll_i, "data_stock")
			ll_prog_stock = this.getitemnumber(ll_i, "prog_stock")
			if isnull(ls_cod_prodotto) then
				g_mb.messagebox("Attenzione", "Indicazione del codice prodotto obbligatoria.", stopsign!, ok!)
				return 1
			end if
			if isnull(ls_cod_tipo_movimento) then
				g_mb.messagebox("Attenzione", "Indicazione del tipo di movimento di magazzino obbligatoria.", stopsign!, ok!)
				return 1
			end if
			if isnull(ls_cod_deposito) then
				g_mb.messagebox("Attenzione", "Indicazione del codice del deposito obbligatoria.", stopsign!, ok!)
				return 1
			end if
			if isnull(ls_cod_ubicazione) then
				g_mb.messagebox("Attenzione", "Indicazione del codice ubicazione obbligatoria.", stopsign!, ok!)
				return 1
			end if
			if isnull(ls_cod_lotto) then
				g_mb.messagebox("Attenzione", "Indicazione del codice lotto obbligatoria.", stopsign!, ok!)
				return 1
			end if
			if isnull(ldt_data_stock) then
				g_mb.messagebox("Attenzione", "Indicazione della data stock obbligatoria.", stopsign!, ok!)
				return 1
			end if
			if isnull(ll_prog_stock) then
				g_mb.messagebox("Attenzione", "Indicazione progressivo stock obbligatoria.", stopsign!, ok!)
				return 1
			end if
		end if
	next
	
   for ll_i = 1 to this.deletedcount()
			
		// aggiunto per gestione centri di costo
		
		ll_anno_registrazione = this.getitemnumber(ll_i, "anno_registrazione", delete!, true)
		ll_num_registrazione = this.getitemnumber(ll_i, "num_registrazione", delete!, true)
		ll_prog_riga_fat_acq = this.getitemnumber(ll_i, "prog_riga_fat_acq", delete!, true)
		
		delete from det_fat_acq_cc
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione  and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_fat_acq = :ll_prog_riga_fat_acq;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE", "Errore in cancellazione dati centri di costo.~r~n"+sqlca.sqlerrtext)
			return 1
		end if
		
		ll_prog_riga_fat_acq = this.getitemnumber(ll_i, "prog_riga_fat_acq", delete!, true)		

      if f_del_fat_acq_det_new(ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_acq, ref ls_messaggio) = -1 then
			g_mb.messagebox("Cancellazione Dettaglio Fatture Acquisto", ls_messaggio)
			return 1
		end if
   next
	
	
end if
end event

event updateend;call super::updateend;
if rowsinserted + rowsupdated + rowsdeleted > 0 then
	this.postevent("pcd_view")
	
	long   ll_i
	
	if not isvalid(s_cs_xx.parametri.parametro_w_fat_acq) then return 0
	
	for ll_i = 1 to upperbound(s_cs_xx.parametri.parametro_w_fat_acq.control)	
		if s_cs_xx.parametri.parametro_w_fat_acq.control[ll_i].classname() = "cb_calcola" then
			s_cs_xx.parametri.parametro_w_fat_acq.control[ll_i].postevent("clicked")
			exit
		end if	
	next
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_doc_origine, ll_num_doc_origine, ll_anno_registrazione, ll_num_registrazione
string ls_cod_doc_origine, ls_numeratore_doc_origine
real lr_progressivo


ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if	
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if		
next

end event

event pcd_retrieve;call super::pcd_retrieve;long ll_anno_registrazione, ll_num_registrazione, ll_errore

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")

parent.title = "Righe Fatture Acquisto - Fattura: " + string(ll_anno_registrazione) + "/" + &
					string(ll_num_registrazione) + "   Fornitore: " + &
					i_parentdw.getitemstring(i_parentdw.getrow(), "cod_fornitore")

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)
if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_acq, ls_modify


   ib_modifica = true

   ls_modify = "cod_tipo_det_acq.protect='1'~t"
   dw_det_fat_acq_det_1.modify(ls_modify)
   ls_modify = "cod_tipo_det_acq.background.color='12632256'~t"
   dw_det_fat_acq_det_1.modify(ls_modify)
   ls_cod_tipo_det_acq = dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(), "cod_tipo_det_acq")
   ld_datawindow = dw_det_fat_acq_det_1
	lp_prod_view = pb_prod_view
	setnull(lc_prodotti_ricerca)
   f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_acq)
	f_tipo_dettaglio_acq_lista(ld_datawindow, ls_cod_tipo_det_acq)

   cb_sconti.enabled = true
	cb_prezzo.enabled=true
	cb_stock.enabled = true
	cb_c_industriale.enabled = false
	cb_grezzo.enabled=false
	cb_prodotti_grezzi_ricerca.enabled=true
	if not isnull(getitemnumber(getrow(),"anno_reg_mov_mag_ret")) and getitemnumber(getrow(),"anno_reg_mov_mag_ret") > 0 then
		ls_modify = "cod_deposito.protect='1'~t"
		ls_modify = ls_modify + "cod_deposito.background.color='12632256'~t"
		ls_modify = ls_modify + "cod_ubicazione.protect='1'~t"
		ls_modify = ls_modify + "cod_ubicazione.background.color='12632256'~t"
		ls_modify = ls_modify + "cod_lotto.protect='1'~t"
		ls_modify = ls_modify + "cod_lotto.background.color='12632256'~t"
		ls_modify = ls_modify + "data_stock.protect='1'~t"
		ls_modify = ls_modify + "data_stock.background.color='12632256'~t"
		ls_modify = ls_modify + "prog_stock.protect='1'~t"
		ls_modify = ls_modify + "prog_stock.background.color='12632256'~t"
		ls_modify = ls_modify + "quan_fatturata.protect='1'~t"
		ls_modify = ls_modify + "quan_fatturata.background.color='12632256'~t"
		ls_modify = ls_modify + "prezzo_acquisto.protect='1'~t"
		ls_modify = ls_modify + "prezzo_acquisto.background.color='12632256'~t"
		dw_det_fat_acq_det_1.modify(ls_modify)
	else				
		ls_modify = "cod_deposito.protect='0'~t"
		ls_modify = ls_modify + "cod_deposito.background.color='16777215'~t"
		ls_modify = ls_modify + "cod_ubicazione.protect='0'~t"
		ls_modify = ls_modify + "cod_ubicazione.background.color='16777215'~t"
		ls_modify = ls_modify + "cod_lotto.protect='0'~t"
		ls_modify = ls_modify + "cod_lotto.background.color='16777215'~t"
		ls_modify = ls_modify + "data_stock.protect='0'~t"
		ls_modify = ls_modify + "data_stock.background.color='16777215'~t"
		ls_modify = ls_modify + "prog_stock.protect='0'~t"
		ls_modify = ls_modify + "prog_stock.background.color='16777215'~t"
		ls_modify = ls_modify + "quan_fatturata.protect='0'~t"
		ls_modify = ls_modify + "quan_fatturata.background.color='16777215'~t"
		ls_modify = ls_modify + "prezzo_acquisto.protect='0'~t"
		ls_modify = ls_modify + "prezzo_acquisto.background.color='16777215'~t"
		dw_det_fat_acq_det_1.modify(ls_modify)
	end if
	
	dw_folder.fu_disabletab(4)

end if

cb_agg_mov_mag.enabled = false
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	long ll_anno_doc_origine, ll_num_doc_origine, ll_i, ll_i1, ll_prog_riga_fat_acq, ll_num_righe, ll_anno_registrazione, &
		  ll_num_registrazione
	string  ls_cod_tipo_analisi, ls_cod_prodotto, ls_cod_tipo_fat_acq, ls_cod_tipo_det_acq, &
			  ls_test, ls_cod_doc_origine, ls_numeratore_doc_origine, ls_flag_memo
	integer li_risposta
	real	  lr_progressivo
	
	ll_i = i_parentdw.getrow()
	ll_i1 = getrow()

	ll_anno_registrazione = i_parentdw.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(ll_i, "num_registrazione")	
	ll_prog_riga_fat_acq = getitemnumber(ll_i1, "prog_riga_fat_acq")	
	ls_cod_tipo_det_acq = getitemstring(ll_i1, "cod_tipo_det_acq")
	ls_cod_tipo_fat_acq = i_parentdw.getitemstring(ll_i, "cod_tipo_fat_acq")
	
	select cod_tipo_analisi
	into   :ls_cod_tipo_analisi  
	from   tab_tipi_fat_acq
	where  cod_azienda = :s_cs_xx.cod_azienda AND
			 cod_tipo_fat_acq = :ls_cod_tipo_fat_acq;

	select cod_azienda
	into   :ls_test
	from 	 det_fat_acq_stat
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione = :ll_num_registrazione and
			 prog_riga_fat_acq = :ll_prog_riga_fat_acq;

	if sqlca.sqlcode=100 then
		li_risposta = f_crea_distribuzione_fat_acq(ls_cod_tipo_analisi, ls_cod_prodotto, ls_cod_tipo_det_acq, ls_cod_doc_origine, ls_numeratore_doc_origine, ll_num_doc_origine, ll_anno_doc_origine, lr_progressivo, ll_prog_riga_fat_acq)
		if li_risposta=-1 then
			g_mb.messagebox("Apice","Attenzione! Si è verificato un errore durante la creazione dei dettagli statistici.",exclamation!)
			pcca.error = c_fatal
		end if
	end if
	


end if

cb_agg_mov_mag.enabled = true
end event

event ue_key;call super::ue_key;string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, &
       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_listino, ls_messaggio, &
		 ls_cod_misura, ls_flag_prezzo
long ll_i, ll_y
double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo, ll_sconti[], ll_maggiorazioni[], &
	    ld_min_fat_altezza,ld_min_fat_larghezza,ld_min_fat_profondita,ld_min_fat_superficie,ld_min_fat_volume, &
		 ld_num_confezioni, ld_num_pezzi, ld_importo_conai, ld_importo_conai_scon
datetime ldt_data_registrazione


choose case this.getcolumnname()

	case "flag_memo_prezzo_acq"
		if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
			//this.postevent("itemchanged")
			dw_det_fat_acq_lista.SetColumn("cod_prodotto") 
			//this.postevent("pcd_previous")
			//dw_det_fat_acq_lista.SetColumn(2) 
			
		end if
				

	case "cod_prodotto","des_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_fat_acq_lista,"cod_prodotto")	
		end if
		
		if key = keytab! then
			dw_det_fat_acq_lista.postevent("itemchanged")
		end if
		
	case "prezzo_acquisto"	
		ls_cod_prodotto = this.getitemstring(getrow(), "cod_prodotto")
		ld_num_confezioni = this.getitemnumber(this.getrow(),"num_confezioni")
		ld_num_pezzi  = this.getitemnumber(this.getrow(),"num_pezzi_confezione")
		ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")		
		ld_prezzo_acquisto = this.getitemnumber(this.getrow(),"prezzo_acquisto")		
		ld_importo_conai = this.getitemnumber(this.getrow(),"importo_conai")		
		ld_importo_conai_scon = this.getitemnumber(this.getrow(),"importo_conai_scon")		

		if key = keyF5!  and keyflags = 1 then
			if not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then			
				select flag_prezzo
				into   :ls_flag_prezzo
				from   tab_misure
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_misura = :ls_cod_misura;
				if sqlca.sqlcode = 0 then
					if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
						if ls_flag_prezzo = 'N' then
							ld_quantita = ld_num_pezzi * ld_num_confezioni
							ld_prezzo_acquisto = ld_prezzo_acquisto / ld_num_pezzi
							ld_importo_conai = ld_importo_conai / ld_num_pezzi
							ld_importo_conai_scon = ld_importo_conai_scon / ld_num_pezzi
							this.setitem(getrow(), "quan_fatturata", ld_quantita)
							this.setitem(getrow(), "prezzo_acquisto", ld_prezzo_acquisto)
							this.setitem(getrow(), "importo_conai", ld_importo_conai)
							this.setitem(getrow(), "importo_conai_scon", ld_importo_conai_scon)							
						end if
					end if
				else
					g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
					return 1
				end if	
			else
				ld_quantita = ld_num_pezzi * ld_num_confezioni
				this.setitem(getrow(), "quan_fatturata", ld_quantita)				
			end if
		end if
		
		if key = keyF6!  and keyflags = 1 then
			if not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then			
				select flag_prezzo
				into   :ls_flag_prezzo
				from   tab_misure
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_misura = :ls_cod_misura;
				if sqlca.sqlcode = 0 then
					if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
						if ls_flag_prezzo = 'N' then
							ld_quantita = ld_num_confezioni
							ld_prezzo_acquisto = ld_prezzo_acquisto * ld_num_pezzi
							ld_importo_conai = ld_importo_conai * ld_num_pezzi
							ld_importo_conai_scon = ld_importo_conai_scon * ld_num_pezzi							
							this.setitem(getrow(), "quan_fatturata", ld_quantita)
							this.setitem(getrow(), "prezzo_acquisto", ld_prezzo_acquisto)							
							this.setitem(getrow(), "importo_conai", ld_importo_conai)
							this.setitem(getrow(), "importo_conai_scon", ld_importo_conai_scon)														
						end if
					end if
				else
					g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
					return 1
				end if	
			else
				ld_quantita = ld_num_pezzi * ld_num_confezioni
				this.setitem(getrow(), "quan_fatturata", ld_quantita)				
			end if
		end if		
		
		if key = keyF1!  and keyflags = 1 then			// SHIFT-F1 ricerca nello storico prezzi
			s_cs_xx.parametri.parametro_s_8 = getitemstring(getrow(),"cod_prodotto")
			s_cs_xx.parametri.parametro_s_9 = dw_det_fat_acq_lista.i_parentdw.getitemstring(dw_det_fat_acq_lista.i_parentdw.getrow(), "cod_fornitore")
			s_cs_xx.parametri.parametro_s_10 = "T"
			//-CLAUDIA 23/07/07 MIGLIORAMENTO PERCHè SI POSSA RICERCARE PER DESCRIZIONE
			s_cs_xx.parametri.parametro_s_12 = '%'+Left (getitemstring(getrow(),"des_prodotto"), 20)+'%'
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_uo_dw_1 = this
			if isvalid(w_det_ord_acq_storico) then
				w_det_ord_acq_storico.show()
			else
				window_open(w_det_ord_acq_storico, 0)
			end if
		end if
		
end choose

if key = keyF4! and keyflags = 1 then
	triggerevent("pcd_save")
	postevent("pcd_new")
end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
	uo_trova_prezzi luo_trova_prezzi
   string ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_messaggio, ls_des_prodotto, &
          ls_cod_tipo_listino_prodotto, ls_cod_cliente, ls_cod_valuta, ls_colonna_sconto, &
          ls_flag_tipo_det_ven, ls_modify, ls_null, ls_cod_agente_1, ls_cod_tipo_det_acq, &
          ls_cod_agente_2, ls_cod_tipo_det_ven, ls_flag_decimali, ls_cod_versione, ls_flag_tipo_det_acq, &
			 ls_cod_misura_mag, ls_cod_deposito, ls_cod_tipo_movimento, ls_nota_prodotto, ls_cod_fornitore, &
			 ls_cod_iva_cliente, ls_cod_iva_prodotto, ls_cod_prodotto_td, ls_tot_netto_2, ls_flag_prezzo, ls_iva, &
			 ls_cod_iva_det, ls_cod_prodotto_alt, ls_des_prodotto_alt, ls_nota_testata, ls_stampa_piede, ls_nota_video
			 
   double ld_fat_conversione, ld_quantita, ld_cambio_ven, ld_quan_impegnata, ld_ultimo_prezzo, ld_variazioni[], &
			 ld_sconti[], ll_maggiorazioni[], ld_min_fat_altezza,ld_min_fat_larghezza,ld_min_fat_profondita, &
			 ld_min_fat_superficie,ld_min_fat_volume, ld_tot_netto, ld_tot_netto_2, &
			 ld_prezzo_acquisto, ld_quan_fatturata, ld_importo_conai, &
			 ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_pr_acq, ld_valore_totale, &
			 ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_calcolato, ld_prezzo_scontato, &
			 ld_pezzi_collo, ld_num_pezzi, ld_num_confezioni, ld_pz_collo, ld_colli, ld_quan_sc_merce, ld_pr_netto, &
			 ld_num_confezioni_det, ld_num_pezzi_confezione_det, ld_importo_conai_det, ld_importo_conai_scon_det, ld_quan_sconto_merce_det, &
			 ld_quan_fatt_pro, ld_quan_fatt_new_pro, ld_quan_sconto_pro, ld_quan_sconto_new_pro			 
   datetime ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva, ldt_null
	long ll_riga_origine, ll_i, ll_null, ll_y, ll_anno_registrazione, ll_num_registrazione

   setnull(ls_null)
   setnull(ldt_null)
   setnull(ll_null)
	
   ls_cod_fornitore = dw_det_fat_acq_lista.i_parentdw.getitemstring(dw_det_fat_acq_lista.i_parentdw.getrow(), "cod_fornitore")
   ls_cod_tipo_det_acq = this.getitemstring(this.getrow(),"cod_tipo_det_acq")
   
   choose case i_colname
		case "prog_riga_fat_acq"
			ll_riga_origine = this.getrow()
			for ll_i = 1 to this.rowcount()
				if ll_i <> ll_riga_origine and &
					long(i_coltext) = this.getitemnumber(ll_i, "prog_riga_bol_ven") then
	            g_mb.messagebox("Attenzione", "Progressivo riga già utilizzato.", &
   	                     exclamation!, ok!)
               i_coltext = string(this.getitemnumber(ll_riga_origine, "prog_riga_fat_acq", primary!,true))
               this.setitem(ll_riga_origine, "prog_riga_fat_acq", double(i_coltext))
               this.settext(i_coltext)
               return 2
					pcca.error = c_fatal
				end if
			next
      case "cod_tipo_det_acq"
			ls_modify = "cod_prodotto.protect='0'~t"
         ls_modify = "cod_prodotto.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "cod_prodotto.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "cod_misura.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "cod_misura.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "fat_conversione.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "fat_conversione.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "quan_fatturata.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "quan_fatturata.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "prezzo_acquisto.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "prezzo_acquisto.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_1.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_1.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_2.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_2.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "cod_iva.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "cod_iva.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_deposito.protect='0'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_deposito.background.color='16777215'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_ubicazione.protect='0'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_ubicazione.background.color='16777215'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_lotto.protect='0'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_lotto.background.color='16777215'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_tipo_movimento.protect='0'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_tipo_movimento.background.color='16777215'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_conto.protect='0'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_conto.background.color='16777215'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)


         ld_datawindow = dw_det_fat_acq_lista
         f_tipo_dettaglio_acq_lista(ld_datawindow, i_coltext)
			
         ld_datawindow = dw_det_fat_acq_det_1
			lp_prod_view = pb_prod_view
			setnull(lc_prodotti_ricerca)
         f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext)			
			
			select flag_tipo_det_acq,
					 cod_tipo_movimento
			into   :ls_flag_tipo_det_acq,
					 :ls_cod_tipo_movimento
			from   tab_tipi_det_acq
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_tipo_det_acq = :i_coltext;
		
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
							  exclamation!, ok!)
			end if
		
			if ls_flag_tipo_det_acq = "M" then
				this.setitem(this.getrow(), "cod_deposito", ls_cod_deposito)
				this.setitem(this.getrow(), "cod_tipo_movimento", ls_cod_tipo_movimento)
				cb_stock.enabled = true
				
				if f_proponi_stock_acq (i_coltext, this, row, ls_messaggio ) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
				end if
			else
				cb_stock.enabled = false
				if not isnull(dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_tipo_movimento")) then
					dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"cod_tipo_movimento",ls_null)
				end if
				if not isnull(dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_deposito")) then
					dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"cod_deposito",ls_null)
				end if
				if not isnull(dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_ubicazione")) then
					dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"cod_ubicazione",ls_null)
				end if
				if not isnull(dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_lotto")) then
					dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"cod_lotto",ls_null)
				end if
				if not isnull(dw_det_fat_acq_det_1.getitemdatetime(dw_det_fat_acq_det_1.getrow(),"data_stock")) then
					dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"data_stock", ldt_null)
				end if
				if not isnull(dw_det_fat_acq_det_1.getitemnumber(dw_det_fat_acq_det_1.getrow(),"prog_stock")) then
					dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"prog_stock",ll_null)
				end if
		
				ls_modify = "cod_tipo_movimento.protect='1'~t"
				ls_modify = ls_modify + "cod_tipo_movimento.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_deposito.protect='1'~t"
				ls_modify = ls_modify + "cod_deposito.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_ubicazione.protect='1'~t"
				ls_modify = ls_modify + "cod_ubicazione.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_lotto.protect='1'~t"
				ls_modify = ls_modify + "cod_lotto.background.color='12632256'~t"
				dw_det_fat_acq_det_1.modify(ls_modify)
			end if

         ld_datawindow = dw_det_fat_acq_det_1
			lp_prod_view = pb_prod_view
			setnull(lc_prodotti_ricerca)
         f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext)

         if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
            select tab_tipi_det_acq.cod_iva  
            into   :ls_cod_iva  
            from   tab_tipi_det_acq  
            where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
                   tab_tipi_det_acq.cod_tipo_det_acq = :i_coltext;
            if sqlca.sqlcode = 0 then
               this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
            end if
         end if
			dw_det_fat_acq_det_3.setitem(dw_det_fat_acq_det_3.getrow(), "nota_dettaglio", ls_null)
			
      case "cod_prodotto"
		if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1

         select anag_prodotti.cod_misura_mag,
					 anag_prodotti.cod_misura_acq,
                anag_prodotti.fat_conversione_acq,
                anag_prodotti.flag_decimali,
                anag_prodotti.cod_iva,
					 anag_prodotti.quan_impegnata,
					 anag_prodotti.des_prodotto,
					 anag_prodotti.prezzo_acquisto,
					 anag_prodotti.pezzi_collo,
					 anag_prodotti.cod_prodotto_alt
         into   :ls_cod_misura_mag,
					 :ls_cod_misura,
                :ld_fat_conversione,
                :ls_flag_decimali,
                :ls_cod_iva,
					 :ld_quan_impegnata,
					 :ls_des_prodotto,
					 :ld_pr_acq,
					 :ld_pezzi_collo,
					 :ls_cod_prodotto_alt
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :i_coltext;
   
         if sqlca.sqlcode = 0 then
				
				if guo_functions.uof_get_note_fisse(ls_null, ls_null, i_coltext, "FAT_ACQ", ls_null, ldt_data_registrazione, ls_nota_testata, ls_stampa_piede, ls_nota_video) < 0 then
					g_mb.error(ls_nota_testata)
				else
					if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
				end if
				// *** michela: come da specifica di ptenda prodotti_grezzi, se esiste il codice alternativo lo metto
				//              nel prodotto grezzo
				if not isnull(ls_cod_prodotto_alt) then
					
					select des_prodotto			       
					into   :ls_des_prodotto_alt
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_prodotto_alt;
							 
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("APICE", "Errore durante la ricerca del prodotto alternativo.~r~n" + sqlca.sqlerrtext)
					end if
					
					this.setitem(i_rownbr, "cod_prodotto_grezzo", ls_cod_prodotto_alt)
					this.setitem(i_rownbr, "des_prodotto_grezzo", ls_des_prodotto_alt)
				
				end if
				// *** fine modifica
				
				
            this.setitem(i_rownbr, "cod_misura", ls_cod_misura_mag)
				this.setitem(i_rownbr, "des_prodotto", ls_des_prodotto)

				dw_det_fat_acq_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
				dw_det_fat_acq_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")
//				dw_det_fat_ven_det_1.modify("prezzo_vendita_t.text='Prezzo " + ls_cod_misura + ":'")
//				dw_det_fat_ven_det_1.modify("quan_fatturata_t.text='Quantità " + ls_cod_misura + ":'")

            if ld_fat_conversione <> 0 then
               dw_det_fat_acq_det_1.setitem(i_rownbr, "fat_conversione", ld_fat_conversione)
            else
               dw_det_fat_acq_det_1.setitem(i_rownbr, "fat_conversione", 1)
            end if
            if not isnull(ls_cod_iva) then this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
//				f_situazione_prodotto(i_coltext, uo_1)
				ls_cod_prodotto = i_coltext
				
				luo_trova_prezzi = create uo_trova_prezzi				
				if luo_trova_prezzi.wf_cerca_ultimo_prezzo_acq_fatture (ls_cod_fornitore, ls_cod_prodotto, ld_ultimo_prezzo, ls_messaggio ) = 0 then
					g_mb.messagebox("Apice", ls_messaggio)
				end if
				
				if isnull(ld_ultimo_prezzo) or ld_ultimo_prezzo < 1 then
					if ld_pr_acq > 0 then
						dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "prezzo_acquisto", ld_pr_acq)
					else
						dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "prezzo_acquisto", 0)
					end if	
				else	
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "prezzo_acquisto", ld_ultimo_prezzo)
				end if
				
//				if f_proponi_stock (this.getitemstring(row,"cod_tipo_det_acq"), this, row, ls_messaggio ) <> 0 then
//					messagebox("APICE",ls_messaggio)
//				end if
				if f_proponi_stock_acq (this.getitemstring(row,"cod_tipo_det_acq"), this, row, ls_messaggio ) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
				end if

			else
				dw_det_fat_acq_det_1.modify("st_prezzo.text='Prezzo:'")
				dw_det_fat_acq_det_1.modify("st_quantita.text='Quantità:'")
				dw_det_fat_acq_det_1.change_dw_current()
				guo_ricerca.uof_ricerca_prodotto(dw_det_fat_acq_lista,"cod_prodotto")	
			end if
			
			if not isnull(ls_cod_fornitore) then
            select anag_fornitori.cod_iva,
                   anag_fornitori.data_esenzione_iva
            into   :ls_cod_iva,
                   :ldt_data_esenzione_iva
            from   anag_fornitori
            where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                   anag_fornitori.cod_fornitore = :ls_cod_fornitore;
         end if

         if sqlca.sqlcode = 0 then
            if ls_cod_iva <> "" and &
               (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
               this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
            end if
         end if

//----------------------------- carica dati di supporto ------------------------------
			if this.getrow() > 0 then
				ls_cod_prodotto = i_coltext
				ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
				ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")
				
			select b.quan_fatturata,
					 b.sconto_1,
					 b.sconto_2,
					 b.sconto_3,
					 b.sconto_4,
					 b.sconto_5,
					 b.sconto_6,
					 b.sconto_7,
					 b.sconto_8,
					 b.sconto_9,
					 b.sconto_10,
					 b.prezzo_acquisto,
					 b.num_confezioni,
					 b.num_pezzi_confezione,
					 b.importo_conai,
					 b.importo_conai_scon,
					 b.quan_sconto_merce,
					 b.cod_iva
			  into :ld_quan_fatturata,
					 :ld_sconto_1,
					 :ld_sconto_2,
					 :ld_sconto_3,
					 :ld_sconto_4,
					 :ld_sconto_5,
					 :ld_sconto_6,
					 :ld_sconto_7,
					 :ld_sconto_8,
					 :ld_sconto_9,
					 :ld_sconto_10,
					 :ld_prezzo_acquisto,
					 :ld_num_confezioni_det,
					 :ld_num_pezzi_confezione_det,
					 :ld_importo_conai_det,
					 :ld_importo_conai_scon_det,
					 :ld_quan_sconto_merce_det,
					 :ls_cod_iva_det
			  from tes_fat_acq a, det_fat_acq b
			 where b.cod_azienda = :s_cs_xx.cod_azienda
				and b.cod_prodotto = :ls_cod_prodotto
				and b.anno_registrazione = :ll_anno_registrazione
				and b.num_registrazione in (select max(b.num_registrazione)
														from tes_fat_acq a, det_fat_acq b
													  where b.cod_azienda = :s_cs_xx.cod_azienda
														 and b.anno_registrazione	= :ll_anno_registrazione
														 and b.cod_prodotto = :ls_cod_prodotto
														 and a.cod_azienda = b.cod_azienda
														 and a.anno_registrazione = b.anno_registrazione
														 and a.num_registrazione = b.num_registrazione
														 and a.cod_fornitore = :ls_cod_fornitore
														 and a.num_registrazione < :ll_num_registrazione)		
				and b.prog_riga_fat_acq in (select max(b.prog_riga_fat_acq)
														from tes_fat_acq a, det_fat_acq b
													  where b.cod_azienda = :s_cs_xx.cod_azienda
														 and b.anno_registrazione	= :ll_anno_registrazione
														 and b.cod_prodotto = :ls_cod_prodotto
														 and a.cod_azienda = b.cod_azienda
														 and a.anno_registrazione = b.anno_registrazione
														 and a.num_registrazione = b.num_registrazione
														 and a.cod_fornitore = :ls_cod_fornitore
														 and a.num_registrazione in (select max(b.num_registrazione)
																								from tes_fat_acq a, det_fat_acq b
																							  where b.cod_azienda = :s_cs_xx.cod_azienda
																								 and b.anno_registrazione	= :ll_anno_registrazione
																								 and b.cod_prodotto = :ls_cod_prodotto
																								 and a.cod_azienda = b.cod_azienda
																								 and a.anno_registrazione = b.anno_registrazione
																								 and a.num_registrazione = b.num_registrazione
																								 and a.cod_fornitore = :ls_cod_fornitore
																								 and a.num_registrazione < :ll_num_registrazione))															 
				and a.cod_azienda = b.cod_azienda
				and a.anno_registrazione = b.anno_registrazione
				and a.num_registrazione = b.num_registrazione
				and a.cod_fornitore = :ls_cod_fornitore
				and a.num_registrazione < :ll_num_registrazione;
					
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice", "Errore in lettura dati da tabella TES_FAT_ACQ e DET_FAT_ACQ " + sqlca.sqlerrtext)
					dw_supporto_dati.reset()		
					dw_supporto_dati.insertrow(0)			
				elseif sqlca.sqlcode = 100 then
					
					//------ Modifica Michele per impostare pz X conf anche se non ci sono fatture precedenti -------
						if not isnull(ld_pezzi_collo) then
							
							setitem(getrow(),"num_pezzi_confezione",ld_pezzi_collo)							
							
							ld_num_confezioni =getitemnumber(getrow(),"num_confezioni")
							
							select flag_prezzo
							into   :ls_flag_prezzo
							from   tab_misure
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_misura = :ls_cod_misura;
									 
							if sqlca.sqlcode = 0 then
								if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_pezzi_collo > 0 then
									if ls_flag_prezzo = 'S' then
										ld_quantita = ld_pezzi_collo * ld_num_confezioni
										this.setitem(getrow(), "quan_fatturata", ld_quantita)
									else
										ld_quantita = ld_num_confezioni
										this.setitem(getrow(), "quan_fatturata", ld_quantita)
									end if
								end if
							else
								g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
								return 1
							end if
							
						end if
					//---------------------------------------- Fine Modifica ----------------------------------------
					
					dw_supporto_dati.reset()
					dw_supporto_dati.insertrow(0)			
				elseif sqlca.sqlcode = 0 then			
					dw_supporto_dati.reset()
					dw_supporto_dati.insertrow(0)
					dw_supporto_dati.setitem(1, "sconto_1", ld_sconto_1)
					dw_supporto_dati.setitem(1, "sconto_2", ld_sconto_2)
					dw_supporto_dati.setitem(1, "sconto_3", ld_sconto_3)
					dw_supporto_dati.setitem(1, "sconto_4", ld_sconto_4)
					dw_supporto_dati.setitem(1, "sconto_5", ld_sconto_5)
					dw_supporto_dati.setitem(1, "sconto_6", ld_sconto_6)
					dw_supporto_dati.setitem(1, "sconto_7", ld_sconto_7)
					dw_supporto_dati.setitem(1, "sconto_8", ld_sconto_8)
					dw_supporto_dati.setitem(1, "sconto_9", ld_sconto_9)
					dw_supporto_dati.setitem(1, "sconto_10", ld_sconto_10)
								
					ld_sconti[1] = ld_sconto_1
					ld_sconti[2] = ld_sconto_2
					ld_sconti[3] = ld_sconto_3
					ld_sconti[4] = ld_sconto_4
					ld_sconti[5] = ld_sconto_5
					ld_sconti[6] = ld_sconto_6
					ld_sconti[7] = ld_sconto_7
					ld_sconti[8] = ld_sconto_8
					ld_sconti[9] = ld_sconto_9
					ld_sconti[10] = ld_sconto_10
		
					f_calcola_sconto(ld_sconti[], ld_prezzo_acquisto, ld_sconto_calcolato, ld_prezzo_scontato)
					dw_supporto_dati.setitem(1, "ultimo_prezzo_pagato_l", ld_prezzo_acquisto)			
					dw_supporto_dati.setitem(1, "ultimo_prezzo_pagato_n", ld_prezzo_scontato)					
					dw_supporto_dati.setitem(1, "quan_acquistata", ld_quan_fatturata)				
					dw_supporto_dati.setitem(1, "sconto_tot", ld_sconto_calcolato)							
					ld_valore_totale = ld_quan_fatturata * ld_prezzo_scontato
					dw_supporto_dati.setitem(1, "valore_totale", ld_valore_totale)					

					//------------carico riga di dettaglio---------------------
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "num_confezioni", ld_num_confezioni_det)
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "num_pezzi_confezione", ld_num_pezzi_confezione_det)
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "quan_fatturata", ld_quan_fatturata)
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "quan_sconto_merce", ld_quan_sconto_merce_det)			
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "prezzo_acquisto", ld_prezzo_acquisto)
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "importo_conai", ld_importo_conai_det)
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "sconto_1", ld_sconto_1)			
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "sconto_2", ld_sconto_2)			
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "sconto_3", ld_sconto_3)			
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "sconto_4", ld_sconto_4)			
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "sconto_5", ld_sconto_5)						
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "importo_conai_scon", ld_importo_conai_scon_det)			
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "cod_iva", ls_cod_iva_det)
																		
								
				end if  
			end if	
//----------------------------- fine modifica Nicola ----------------------------------------			
//         ls_cod_prodotto = i_coltext
//         ld_quantita = this.getitemnumber(i_rownbr, "quan_fatturata")
//         if ls_flag_decimali = "N" and &
//            ld_quantita - int(ld_quantita) > 0 then
//            ld_quantita = ceiling(ld_quantita)
//            this.setitem(i_rownbr, "quan_fatturata", ld_quantita)
//         end if

			if f_leggi_nota_prodotto(ls_cod_tipo_det_acq, ls_cod_prodotto, ls_nota_prodotto) = 0 then
				dw_det_fat_acq_det_1.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
				dw_det_fat_acq_det_1.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if
			
//			if not isnull(ld_pezzi_collo) then
//				this.setitem(this.getrow(), "num_pezzi_confezione", ld_pezzi_collo)
//			end if	

		case "quan_fatturata"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
			ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")			
			
         select anag_prodotti.flag_decimali
         into   :ls_flag_decimali
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
         if sqlca.sqlcode = 0 then
            if ls_flag_decimali = "N" and &
               (double(i_coltext) - int(double(i_coltext))) > 0 then
               i_coltext = string(ceiling(double(i_coltext)))
               this.setitem(i_rownbr, "quan_fatturata", double(i_coltext))
					this.settext(i_coltext)
               return 2
            end if
         end if
			
			select b.quan_fatturata,
					 b.quan_sconto_merce
			  into :ld_quan_fatt_pro,
			  		 :ld_quan_sconto_pro
			  from tes_fat_acq a, det_fat_acq b
			 where b.cod_azienda = :s_cs_xx.cod_azienda
				and b.cod_prodotto = :ls_cod_prodotto
				and b.anno_registrazione = :ll_anno_registrazione
				and b.num_registrazione in (select max(b.num_registrazione)
														from tes_fat_acq a, det_fat_acq b
													  where b.cod_azienda = :s_cs_xx.cod_azienda
														 and b.anno_registrazione	= :ll_anno_registrazione
														 and b.cod_prodotto = :ls_cod_prodotto
														 and a.cod_azienda = b.cod_azienda
														 and a.anno_registrazione = b.anno_registrazione
														 and a.num_registrazione = b.num_registrazione
														 and a.cod_fornitore = :ls_cod_fornitore
														 and a.num_registrazione < :ll_num_registrazione)		
				and b.prog_riga_fat_acq in (select max(b.prog_riga_fat_acq)
														from tes_fat_acq a, det_fat_acq b
													  where b.cod_azienda = :s_cs_xx.cod_azienda
														 and b.anno_registrazione	= :ll_anno_registrazione
														 and b.cod_prodotto = :ls_cod_prodotto
														 and a.cod_azienda = b.cod_azienda
														 and a.anno_registrazione = b.anno_registrazione
														 and a.num_registrazione = b.num_registrazione
														 and a.cod_fornitore = :ls_cod_fornitore
														 and a.num_registrazione in (select max(b.num_registrazione)
																								from tes_fat_acq a, det_fat_acq b
																							  where b.cod_azienda = :s_cs_xx.cod_azienda
																								 and b.anno_registrazione	= :ll_anno_registrazione
																								 and b.cod_prodotto = :ls_cod_prodotto
																								 and a.cod_azienda = b.cod_azienda
																								 and a.anno_registrazione = b.anno_registrazione
																								 and a.num_registrazione = b.num_registrazione
																								 and a.cod_fornitore = :ls_cod_fornitore
																								 and a.num_registrazione < :ll_num_registrazione))															 
				and a.cod_azienda = b.cod_azienda
				and a.anno_registrazione = b.anno_registrazione
				and a.num_registrazione = b.num_registrazione
				and a.cod_fornitore = :ls_cod_fornitore
				and a.num_registrazione < :ll_num_registrazione;
					
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice", "Errore in lettura dati da tabella TES_FAT_ACQ e DET_FAT_ACQ " + sqlca.sqlerrtext)
					dw_supporto_dati.reset()		
					dw_supporto_dati.insertrow(0)			
				elseif sqlca.sqlcode = 100 then
					dw_supporto_dati.reset()
					dw_supporto_dati.insertrow(0)			
				elseif sqlca.sqlcode = 0 then						
					ld_quan_fatt_new_pro = double(i_coltext)
					if ld_quan_fatt_pro > 0 then
						ld_quan_sconto_new_pro = (ld_quan_fatt_new_pro * ld_quan_sconto_pro) / ld_quan_fatt_pro
						if not isnull(ld_quan_sconto_new_pro) and ld_quan_sconto_new_pro > 0 then
							dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "quan_sconto_merce", ld_quan_sconto_new_pro)
						else
							dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "quan_sconto_merce", 0)
						end if	
					end if	
				end if			
				
		case "num_confezioni"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
			ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")	
			
			ls_cod_prodotto = this.getitemstring(getrow(), "cod_prodotto")
			ld_num_confezioni = double(i_coltext)
			ld_num_pezzi  = this.getitemnumber(this.getrow(),"num_pezzi_confezione")
			ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")			
			if not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then			
				select flag_prezzo
				into   :ls_flag_prezzo
				from   tab_misure
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_misura = :ls_cod_misura;
				if sqlca.sqlcode = 0 then
					if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
						if ls_flag_prezzo = 'S' then
							ld_quantita = ld_num_pezzi * ld_num_confezioni
							this.setitem(getrow(), "quan_fatturata", ld_quantita)
						else
							ld_quantita = ld_num_confezioni
							this.setitem(getrow(), "quan_fatturata", ld_quantita)
						end if
					end if
				else
					g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
					return 1
				end if	
			else
				ld_quantita = ld_num_pezzi * ld_num_confezioni
				this.setitem(getrow(), "quan_fatturata", ld_quantita)				
			end if
			
			
			select b.quan_fatturata,
					 b.quan_sconto_merce
			  into :ld_quan_fatt_pro,
			  		 :ld_quan_sconto_pro
			  from tes_fat_acq a, det_fat_acq b
			 where b.cod_azienda = :s_cs_xx.cod_azienda
				and b.cod_prodotto = :ls_cod_prodotto
				and b.anno_registrazione = :ll_anno_registrazione
				and b.num_registrazione in (select max(b.num_registrazione)
														from tes_fat_acq a, det_fat_acq b
													  where b.cod_azienda = :s_cs_xx.cod_azienda
														 and b.anno_registrazione	= :ll_anno_registrazione
														 and b.cod_prodotto = :ls_cod_prodotto
														 and a.cod_azienda = b.cod_azienda
														 and a.anno_registrazione = b.anno_registrazione
														 and a.num_registrazione = b.num_registrazione
														 and a.cod_fornitore = :ls_cod_fornitore
														 and a.num_registrazione < :ll_num_registrazione)		
				and b.prog_riga_fat_acq in (select max(b.prog_riga_fat_acq)
														from tes_fat_acq a, det_fat_acq b
													  where b.cod_azienda = :s_cs_xx.cod_azienda
														 and b.anno_registrazione	= :ll_anno_registrazione
														 and b.cod_prodotto = :ls_cod_prodotto
														 and a.cod_azienda = b.cod_azienda
														 and a.anno_registrazione = b.anno_registrazione
														 and a.num_registrazione = b.num_registrazione
														 and a.cod_fornitore = :ls_cod_fornitore
														 and a.num_registrazione in (select max(b.num_registrazione)
																								from tes_fat_acq a, det_fat_acq b
																							  where b.cod_azienda = :s_cs_xx.cod_azienda
																								 and b.anno_registrazione	= :ll_anno_registrazione
																								 and b.cod_prodotto = :ls_cod_prodotto
																								 and a.cod_azienda = b.cod_azienda
																								 and a.anno_registrazione = b.anno_registrazione
																								 and a.num_registrazione = b.num_registrazione
																								 and a.cod_fornitore = :ls_cod_fornitore
																								 and a.num_registrazione < :ll_num_registrazione))															 
				and a.cod_azienda = b.cod_azienda
				and a.anno_registrazione = b.anno_registrazione
				and a.num_registrazione = b.num_registrazione
				and a.cod_fornitore = :ls_cod_fornitore
				and a.num_registrazione < :ll_num_registrazione;
					
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice", "Errore in lettura dati da tabella TES_FAT_ACQ e DET_FAT_ACQ " + sqlca.sqlerrtext)
					dw_supporto_dati.reset()		
					dw_supporto_dati.insertrow(0)			
				elseif sqlca.sqlcode = 100 then
					dw_supporto_dati.reset()
					dw_supporto_dati.insertrow(0)			
				elseif sqlca.sqlcode = 0 then						
					ld_quan_fatt_new_pro = double(i_coltext)
					if ld_quan_fatt_pro > 0 then
						ld_quan_sconto_new_pro = (ld_quantita * ld_quan_sconto_pro) / ld_quan_fatt_pro
						if not isnull(ld_quan_sconto_new_pro) and ld_quan_sconto_new_pro > 0 then
							dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "quan_sconto_merce", ld_quan_sconto_new_pro)
						else
							dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "quan_sconto_merce", 0)
						end if	
					end if	
				end if						
			
		case "num_pezzi_confezione"			
			ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
			ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")				
			
			ls_cod_prodotto = this.getitemstring(getrow(), "cod_prodotto")
			ld_num_confezioni = this.getitemnumber(this.getrow(),"num_confezioni")
			ld_num_pezzi  = double(i_coltext)
			ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")
			if not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then
				select flag_prezzo
				into   :ls_flag_prezzo
				from   tab_misure
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_misura = :ls_cod_misura;
				if sqlca.sqlcode = 0 then
					if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
						if ls_flag_prezzo = 'S' then
							ld_quantita = ld_num_pezzi * ld_num_confezioni
							this.setitem(getrow(), "quan_fatturata", ld_quantita)
						else
							ld_quantita = ld_num_confezioni
							this.setitem(getrow(), "quan_fatturata", ld_quantita)
						end if
					end if
				else
					g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
					return 1
				end if					
			else
				ld_quantita = ld_num_pezzi * ld_num_confezioni
				this.setitem(getrow(), "quan_fatturata", ld_quantita)				
			end if
			
			select b.quan_fatturata,
					 b.quan_sconto_merce
			  into :ld_quan_fatt_pro,
			  		 :ld_quan_sconto_pro
			  from tes_fat_acq a, det_fat_acq b
			 where b.cod_azienda = :s_cs_xx.cod_azienda
				and b.cod_prodotto = :ls_cod_prodotto
				and b.anno_registrazione = :ll_anno_registrazione
				and b.num_registrazione in (select max(b.num_registrazione)
														from tes_fat_acq a, det_fat_acq b
													  where b.cod_azienda = :s_cs_xx.cod_azienda
														 and b.anno_registrazione	= :ll_anno_registrazione
														 and b.cod_prodotto = :ls_cod_prodotto
														 and a.cod_azienda = b.cod_azienda
														 and a.anno_registrazione = b.anno_registrazione
														 and a.num_registrazione = b.num_registrazione
														 and a.cod_fornitore = :ls_cod_fornitore
														 and a.num_registrazione < :ll_num_registrazione)		
				and b.prog_riga_fat_acq in (select max(b.prog_riga_fat_acq)
														from tes_fat_acq a, det_fat_acq b
													  where b.cod_azienda = :s_cs_xx.cod_azienda
														 and b.anno_registrazione	= :ll_anno_registrazione
														 and b.cod_prodotto = :ls_cod_prodotto
														 and a.cod_azienda = b.cod_azienda
														 and a.anno_registrazione = b.anno_registrazione
														 and a.num_registrazione = b.num_registrazione
														 and a.cod_fornitore = :ls_cod_fornitore
														 and a.num_registrazione in (select max(b.num_registrazione)
																								from tes_fat_acq a, det_fat_acq b
																							  where b.cod_azienda = :s_cs_xx.cod_azienda
																								 and b.anno_registrazione	= :ll_anno_registrazione
																								 and b.cod_prodotto = :ls_cod_prodotto
																								 and a.cod_azienda = b.cod_azienda
																								 and a.anno_registrazione = b.anno_registrazione
																								 and a.num_registrazione = b.num_registrazione
																								 and a.cod_fornitore = :ls_cod_fornitore
																								 and a.num_registrazione < :ll_num_registrazione))															 
				and a.cod_azienda = b.cod_azienda
				and a.anno_registrazione = b.anno_registrazione
				and a.num_registrazione = b.num_registrazione
				and a.cod_fornitore = :ls_cod_fornitore
				and a.num_registrazione < :ll_num_registrazione;
					
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice", "Errore in lettura dati da tabella TES_FAT_ACQ e DET_FAT_ACQ " + sqlca.sqlerrtext)
					dw_supporto_dati.reset()		
					dw_supporto_dati.insertrow(0)			
				elseif sqlca.sqlcode = 100 then
					dw_supporto_dati.reset()
					dw_supporto_dati.insertrow(0)			
				elseif sqlca.sqlcode = 0 then						
					ld_quan_fatt_new_pro = double(i_coltext)
					if ld_quan_fatt_pro > 0 then
						ld_quan_sconto_new_pro = (ld_quantita * ld_quan_sconto_pro) / ld_quan_fatt_pro
						if not isnull(ld_quan_sconto_new_pro) and ld_quan_sconto_new_pro > 0 then
							dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "quan_sconto_merce", ld_quan_sconto_new_pro)
						else
							dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "quan_sconto_merce", 0)
						end if	
					end if	
				end if									
			
			
			
		case "prezzo_acquisto"
			this.setitem(this.getrow(), "prezzo_acq_netto", double(i_coltext))
   end choose
end if
end event

event doubleclicked;call super::doubleclicked;string ls_str

if dwo.type = "text" and dwo.name="rif_ddt_t" then
	if is_ord_ddt_rif = "DESC" then
		is_ord_ddt_rif = "ASC"
		
	else
		is_ord_ddt_rif = "DESC"
	end if

	ls_str = "anno_bolla_acq "+is_ord_ddt_rif+","
	ls_str += "num_bolla_acq "+is_ord_ddt_rif+","
	ls_str += "prog_riga_bolla_acq "+is_ord_ddt_rif
	
	setsort(ls_str)
	sort()
end if
end event

type dw_det_fat_acq_det_1 from uo_cs_xx_dw within w_det_fat_acq
integer x = 46
integer y = 120
integer width = 3447
integer height = 1160
integer taborder = 140
string dataobject = "d_det_fat_acq_det_1"
boolean border = false
end type

event itemchanged;call super::itemchanged;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
	uo_trova_prezzi luo_trova_prezzi
	
   string ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_cod_tipo_movimento, ls_cod_deposito,&
          ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ls_messaggio, &
          ls_flag_tipo_det_acq, ls_modify, ls_null, ls_cod_tipo_det_acq, ls_flag_decimali, &
			 ls_cod_misura_mag, ls_nota_prodotto, ls_flag_prezzo, ls_des_prodotto, ls_cod_iva_det, &
			 ls_cod_prodotto_alt, ls_des_prodotto_alt, ls_nota_testata, ls_stampa_piede, ls_nota_video
			 
   double ld_fat_conversione, ld_quantita, ld_cambio_acq, ld_num_confezioni, ld_num_pezzi, ld_quan_impegnata, &
			 ld_pr_acq, ld_pezzi_collo, ld_ultimo_prezzo, ld_quan_fatturata, ld_sconto_1, ld_sconto_2, ld_sconto_3, &
			 ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
			 ld_prezzo_acquisto, ld_num_confezioni_det, ld_num_pezzi_confezione_det, ld_importo_conai_det, &
			 ld_importo_conai_scon_det, ld_quan_sconto_merce_det, ld_sconti[], ld_sconto_calcolato, &
			 ld_prezzo_scontato, ld_valore_totale
			 
	dec{4} ld_imponibile_iva
	
   datetime ldt_data_doc_origine, ldt_data_esenzione_iva, ldt_null, ldt_data_registrazione
	
	long ll_riga_origine, ll_i, ll_null, ll_anno_registrazione, ll_num_registrazione

   setnull(ls_null)
   setnull(ll_null)
   setnull(ldt_null)

   ls_cod_tipo_listino_prodotto = dw_det_fat_acq_lista.i_parentdw.getitemstring(dw_det_fat_acq_lista.i_parentdw.getrow(), "cod_tipo_listino_prodotto")
   ldt_data_doc_origine = dw_det_fat_acq_lista.i_parentdw.getitemdatetime(dw_det_fat_acq_lista.i_parentdw.getrow(), "data_doc_origine")
   ls_cod_fornitore = dw_det_fat_acq_lista.i_parentdw.getitemstring(dw_det_fat_acq_lista.i_parentdw.getrow(), "cod_fornitore")
   ls_cod_valuta = dw_det_fat_acq_lista.i_parentdw.getitemstring(dw_det_fat_acq_lista.i_parentdw.getrow(), "cod_valuta")
   ld_cambio_acq = dw_det_fat_acq_lista.i_parentdw.getitemnumber(dw_det_fat_acq_lista.i_parentdw.getrow(), "cambio_acq")
   ls_cod_tipo_det_acq = this.getitemstring(this.getrow(),"cod_tipo_det_acq")
   
   choose case i_colname
			
		case "prog_riga_fat_acq"
			
			ll_riga_origine = this.getrow()
			for ll_i = 1 to this.rowcount()
				if ll_i <> ll_riga_origine and &
					long(i_coltext) = this.getitemnumber(ll_i, "prog_riga_fat_acq") then
	            g_mb.messagebox("Attenzione", "Progressivo riga già utilizzato.", &
   	                     exclamation!, ok!)
               i_coltext = string(this.getitemnumber(ll_riga_origine, "prog_riga_fat_acq", primary!,true))
               this.setitem(ll_riga_origine, "prog_riga_fat_acq", double(i_coltext))
               this.settext(i_coltext)
               return 2
					pcca.error = c_fatal
				end if
			next
			
      case "cod_tipo_det_acq"
			
         ls_modify = "cod_prodotto.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "cod_prodotto.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "des_prodotto.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "cod_misura.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "cod_misura.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "fat_conversione.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "fat_conversione.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "quan_fatturata.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "quan_fatturata.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "prezzo_acquisto.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "prezzo_acquisto.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_1.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_1.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_2.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "sconto_2.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "cod_iva.protect='0'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
         ls_modify = "cod_iva.background.color='16777215'~t"
         dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_deposito.protect='0'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_deposito.background.color='16777215'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_ubicazione.protect='0'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_ubicazione.background.color='16777215'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_lotto.protect='0'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_lotto.background.color='16777215'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_tipo_movimento.protect='0'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_tipo_movimento.background.color='16777215'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_conto.protect='0'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)
			ls_modify = "cod_conto.background.color='16777215'~t"
			dw_det_fat_acq_det_1.modify(ls_modify)

         ld_datawindow = dw_det_fat_acq_det_1
			lp_prod_view = pb_prod_view
			setnull(lc_prodotti_ricerca)
         f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext)
			select flag_tipo_det_acq,
					 cod_tipo_movimento
			into   :ls_flag_tipo_det_acq,
					 :ls_cod_tipo_movimento
			from   tab_tipi_det_acq
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_tipo_det_acq = :i_coltext;
		
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
							  exclamation!, ok!)
			end if
		
			if ls_flag_tipo_det_acq = "M" then
				this.setitem(this.getrow(), "cod_deposito", ls_cod_deposito)
				this.setitem(this.getrow(), "cod_tipo_movimento", ls_cod_tipo_movimento)
				cb_stock.enabled = true
				if f_proponi_stock_acq (i_coltext, this, dw_det_fat_acq_lista.getrow(), ls_messaggio ) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
				end if
			else
				cb_stock.enabled = false
				if not isnull(dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_tipo_movimento")) then
					dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"cod_tipo_movimento",ls_null)
				end if
				if not isnull(dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_deposito")) then
					dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"cod_deposito",ls_null)
				end if
				if not isnull(dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_ubicazione")) then
					dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"cod_ubicazione",ls_null)
				end if
				if not isnull(dw_det_fat_acq_det_1.getitemstring(dw_det_fat_acq_det_1.getrow(),"cod_lotto")) then
					dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"cod_lotto",ls_null)
				end if
				if not isnull(dw_det_fat_acq_det_1.getitemdatetime(dw_det_fat_acq_det_1.getrow(),"data_stock")) then
					dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"data_stock", ldt_null)
				end if
				if not isnull(dw_det_fat_acq_det_1.getitemnumber(dw_det_fat_acq_det_1.getrow(),"prog_stock")) then
					dw_det_fat_acq_det_1.setitem(dw_det_fat_acq_det_1.getrow(),"prog_stock",ll_null)
				end if
		
				ls_modify = "cod_tipo_movimento.protect='1'~t"
				ls_modify = ls_modify + "cod_tipo_movimento.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_deposito.protect='1'~t"
				ls_modify = ls_modify + "cod_deposito.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_ubicazione.protect='1'~t"
				ls_modify = ls_modify + "cod_ubicazione.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_lotto.protect='1'~t"
				ls_modify = ls_modify + "cod_lotto.background.color='12632256'~t"
				dw_det_fat_acq_det_1.modify(ls_modify)
			end if

         ld_datawindow = dw_det_fat_acq_det_1
			lp_prod_view = pb_prod_view
			setnull(lc_prodotti_ricerca)
         f_tipo_dettaglio_acq(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext)

         if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
            select tab_tipi_det_acq.cod_iva  
            into   :ls_cod_iva  
            from   tab_tipi_det_acq  
            where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
                   tab_tipi_det_acq.cod_tipo_det_acq = :i_coltext;
            if sqlca.sqlcode = 0 then
               this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
            end if
         end if
			dw_det_fat_acq_det_3.setitem(dw_det_fat_acq_det_3.getrow(), "nota_dettaglio", ls_null)
			
      case "cod_prodotto"
			
			if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1

         select anag_prodotti.cod_misura_mag,
					 anag_prodotti.cod_misura_acq,
                anag_prodotti.fat_conversione_acq,
                anag_prodotti.flag_decimali,
                anag_prodotti.cod_iva,
					 anag_prodotti.quan_impegnata,
					 anag_prodotti.des_prodotto,
					 anag_prodotti.prezzo_acquisto,
					 anag_prodotti.pezzi_collo,
					 anag_prodotti.cod_prodotto_alt
         into   :ls_cod_misura_mag,
					 :ls_cod_misura,
                :ld_fat_conversione,
                :ls_flag_decimali,
                :ls_cod_iva,
					 :ld_quan_impegnata,
					 :ls_des_prodotto,
					 :ld_pr_acq,
					 :ld_pezzi_collo,
					 :ls_cod_prodotto_alt
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :i_coltext;
   
         if sqlca.sqlcode = 0 then
								
				if guo_functions.uof_get_note_fisse(ls_null, ls_null, i_coltext, "FAT_ACQ", ls_null, ldt_data_registrazione, ls_nota_testata, ls_stampa_piede, ls_nota_video) < 0 then
					g_mb.error(ls_nota_testata)
				else
					if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
				end if
							
				// *** michela: come da specifica di ptenda prodotti_grezzi, se esiste il codice alternativo lo metto
				//              nel prodotto grezzo
				if not isnull(ls_cod_prodotto_alt) then
					
					select des_prodotto			       
					into   :ls_des_prodotto_alt
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_prodotto = :ls_cod_prodotto_alt;
							 
					if sqlca.sqlcode < 0 then
						g_mb.messagebox("APICE", "Errore durante la ricerca del prodotto alternativo.~r~n" + sqlca.sqlerrtext)
					end if
					
					this.setitem(i_rownbr, "cod_prodotto_grezzo", ls_cod_prodotto_alt)
					this.setitem(i_rownbr, "des_prodotto_grezzo", ls_des_prodotto_alt)
				
				end if
				// *** fine modifica				
				
				
            this.setitem(i_rownbr, "cod_misura", ls_cod_misura)
				this.setitem(i_rownbr, "des_prodotto", ls_des_prodotto)
				
				dw_det_fat_acq_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
				dw_det_fat_acq_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")

            if ld_fat_conversione <> 0 then
               this.setitem(i_rownbr, "fat_conversione", ld_fat_conversione)
            else
               this.setitem(i_rownbr, "fat_conversione", 1)
            end if
				
            if not isnull(ls_cod_iva) then this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
				
				ls_cod_prodotto = i_coltext
				
				luo_trova_prezzi = create uo_trova_prezzi				
				if luo_trova_prezzi.wf_cerca_ultimo_prezzo_acq_fatture (ls_cod_fornitore, ls_cod_prodotto, ld_ultimo_prezzo, ls_messaggio ) = 0 then
					g_mb.messagebox("Apice", ls_messaggio)
				end if
				
				if isnull(ld_ultimo_prezzo) or ld_ultimo_prezzo < 1 then
					if ld_pr_acq > 0 then
						dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "prezzo_acquisto", ld_pr_acq)
					else
						dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "prezzo_acquisto", 0)
					end if	
				else	
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "prezzo_acquisto", ld_ultimo_prezzo)
				end if
				
				if f_proponi_stock_acq (this.getitemstring(row,"cod_tipo_det_acq"), this, dw_det_fat_acq_lista.getrow(), ls_messaggio ) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
				end if
			else
				dw_det_fat_acq_det_1.modify("st_prezzo.text='Prezzo:'")
				dw_det_fat_acq_det_1.modify("st_quantita.text='Quantità:'")
				dw_det_fat_acq_det_1.change_dw_current()
				guo_ricerca.uof_ricerca_prodotto(dw_det_fat_acq_det_1,"cod_prodotto")	
			end if

			if not isnull(this.getitemstring(i_rownbr, "cod_misura")) then
				cb_prezzo.text= "Prezzo al " + this.getitemstring(i_rownbr, "cod_misura")
			else
				cb_prezzo.text= "Prezzo"
			end if

			if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_fat_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(ld_fat_conversione)) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
			else
				dw_det_fat_acq_det_1.modify("st_fattore_conv.text=''")
			end if
			
			if not isnull(ls_cod_fornitore) then
            select anag_fornitori.cod_iva,
                   anag_fornitori.data_esenzione_iva
            into   :ls_cod_iva,
                   :ldt_data_esenzione_iva
            from   anag_fornitori
            where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                   anag_fornitori.cod_fornitore = :ls_cod_fornitore;
         end if

         if sqlca.sqlcode = 0 then
            if ls_cod_iva <> "" and &
               (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
               this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
            end if
         end if
			
//----------------------------- carica dati di supporto ------------------------------
			if this.getrow() > 0 then
				ls_cod_prodotto = i_coltext
				ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione")
				ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "num_registrazione")
				
			select b.quan_fatturata,
					 b.sconto_1,
					 b.sconto_2,
					 b.sconto_3,
					 b.sconto_4,
					 b.sconto_5,
					 b.sconto_6,
					 b.sconto_7,
					 b.sconto_8,
					 b.sconto_9,
					 b.sconto_10,
					 b.prezzo_acquisto,
					 b.num_confezioni,
					 b.num_pezzi_confezione,
					 b.importo_conai,
					 b.importo_conai_scon,
					 b.quan_sconto_merce,
					 b.cod_iva
			  into :ld_quan_fatturata,
					 :ld_sconto_1,
					 :ld_sconto_2,
					 :ld_sconto_3,
					 :ld_sconto_4,
					 :ld_sconto_5,
					 :ld_sconto_6,
					 :ld_sconto_7,
					 :ld_sconto_8,
					 :ld_sconto_9,
					 :ld_sconto_10,
					 :ld_prezzo_acquisto,
					 :ld_num_confezioni_det,
					 :ld_num_pezzi_confezione_det,
					 :ld_importo_conai_det,
					 :ld_importo_conai_scon_det,
					 :ld_quan_sconto_merce_det,
					 :ls_cod_iva_det
			  from tes_fat_acq a, det_fat_acq b
			 where b.cod_azienda = :s_cs_xx.cod_azienda
				and b.cod_prodotto = :ls_cod_prodotto
				and b.anno_registrazione = :ll_anno_registrazione
				and b.num_registrazione in (select max(b.num_registrazione)
														from tes_fat_acq a, det_fat_acq b
													  where b.cod_azienda = :s_cs_xx.cod_azienda
														 and b.anno_registrazione	= :ll_anno_registrazione
														 and b.cod_prodotto = :ls_cod_prodotto
														 and a.cod_azienda = b.cod_azienda
														 and a.anno_registrazione = b.anno_registrazione
														 and a.num_registrazione = b.num_registrazione
														 and a.cod_fornitore = :ls_cod_fornitore
														 and a.num_registrazione < :ll_num_registrazione)		
				and b.prog_riga_fat_acq in (select max(b.prog_riga_fat_acq)
														from tes_fat_acq a, det_fat_acq b
													  where b.cod_azienda = :s_cs_xx.cod_azienda
														 and b.anno_registrazione	= :ll_anno_registrazione
														 and b.cod_prodotto = :ls_cod_prodotto
														 and a.cod_azienda = b.cod_azienda
														 and a.anno_registrazione = b.anno_registrazione
														 and a.num_registrazione = b.num_registrazione
														 and a.cod_fornitore = :ls_cod_fornitore
														 and a.num_registrazione in (select max(b.num_registrazione)
																								from tes_fat_acq a, det_fat_acq b
																							  where b.cod_azienda = :s_cs_xx.cod_azienda
																								 and b.anno_registrazione	= :ll_anno_registrazione
																								 and b.cod_prodotto = :ls_cod_prodotto
																								 and a.cod_azienda = b.cod_azienda
																								 and a.anno_registrazione = b.anno_registrazione
																								 and a.num_registrazione = b.num_registrazione
																								 and a.cod_fornitore = :ls_cod_fornitore
																								 and a.num_registrazione < :ll_num_registrazione))															 
				and a.cod_azienda = b.cod_azienda
				and a.anno_registrazione = b.anno_registrazione
				and a.num_registrazione = b.num_registrazione
				and a.cod_fornitore = :ls_cod_fornitore
				and a.num_registrazione < :ll_num_registrazione;
					
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("Apice", "Errore in lettura dati da tabella TES_FAT_ACQ e DET_FAT_ACQ " + sqlca.sqlerrtext)
					dw_supporto_dati.reset()		
					dw_supporto_dati.insertrow(0)			
				elseif sqlca.sqlcode = 100 then
					
					//------ Modifica Michele per impostare pz X conf anche se non ci sono fatture precedenti -------
						if not isnull(ld_pezzi_collo) then
							
							setitem(getrow(),"num_pezzi_confezione",ld_pezzi_collo)							
							
							ld_num_confezioni =getitemnumber(getrow(),"num_confezioni")
							
							select flag_prezzo
							into   :ls_flag_prezzo
							from   tab_misure
							where  cod_azienda = :s_cs_xx.cod_azienda and
									 cod_misura = :ls_cod_misura;
									 
							if sqlca.sqlcode = 0 then
								if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_pezzi_collo > 0 then
									if ls_flag_prezzo = 'S' then
										ld_quantita = ld_pezzi_collo * ld_num_confezioni
										this.setitem(getrow(), "quan_fatturata", ld_quantita)
									else
										ld_quantita = ld_num_confezioni
										this.setitem(getrow(), "quan_fatturata", ld_quantita)
									end if
								end if
							else
								g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
								return 1
							end if
							
						end if
					//---------------------------------------- Fine Modifica ----------------------------------------
					
					dw_supporto_dati.reset()
					dw_supporto_dati.insertrow(0)			
				elseif sqlca.sqlcode = 0 then			
					dw_supporto_dati.reset()
					dw_supporto_dati.insertrow(0)
					dw_supporto_dati.setitem(1, "sconto_1", ld_sconto_1)
					dw_supporto_dati.setitem(1, "sconto_2", ld_sconto_2)
					dw_supporto_dati.setitem(1, "sconto_3", ld_sconto_3)
					dw_supporto_dati.setitem(1, "sconto_4", ld_sconto_4)
					dw_supporto_dati.setitem(1, "sconto_5", ld_sconto_5)
					dw_supporto_dati.setitem(1, "sconto_6", ld_sconto_6)
					dw_supporto_dati.setitem(1, "sconto_7", ld_sconto_7)
					dw_supporto_dati.setitem(1, "sconto_8", ld_sconto_8)
					dw_supporto_dati.setitem(1, "sconto_9", ld_sconto_9)
					dw_supporto_dati.setitem(1, "sconto_10", ld_sconto_10)
								
					ld_sconti[1] = ld_sconto_1
					ld_sconti[2] = ld_sconto_2
					ld_sconti[3] = ld_sconto_3
					ld_sconti[4] = ld_sconto_4
					ld_sconti[5] = ld_sconto_5
					ld_sconti[6] = ld_sconto_6
					ld_sconti[7] = ld_sconto_7
					ld_sconti[8] = ld_sconto_8
					ld_sconti[9] = ld_sconto_9
					ld_sconti[10] = ld_sconto_10
		
					f_calcola_sconto(ld_sconti[], ld_prezzo_acquisto, ld_sconto_calcolato, ld_prezzo_scontato)
					dw_supporto_dati.setitem(1, "ultimo_prezzo_pagato_l", ld_prezzo_acquisto)			
					dw_supporto_dati.setitem(1, "ultimo_prezzo_pagato_n", ld_prezzo_scontato)					
					dw_supporto_dati.setitem(1, "quan_acquistata", ld_quan_fatturata)				
					dw_supporto_dati.setitem(1, "sconto_tot", ld_sconto_calcolato)							
					ld_valore_totale = ld_quan_fatturata * ld_prezzo_scontato
					dw_supporto_dati.setitem(1, "valore_totale", ld_valore_totale)					

					//------------carico riga di dettaglio---------------------
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "num_confezioni", ld_num_confezioni_det)
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "num_pezzi_confezione", ld_num_pezzi_confezione_det)
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "quan_fatturata", ld_quan_fatturata)
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "quan_sconto_merce", ld_quan_sconto_merce_det)			
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "prezzo_acquisto", ld_prezzo_acquisto)
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "importo_conai", ld_importo_conai_det)
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "sconto_1", ld_sconto_1)			
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "sconto_2", ld_sconto_2)			
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "sconto_3", ld_sconto_3)			
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "sconto_4", ld_sconto_4)			
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "sconto_5", ld_sconto_5)						
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "importo_conai_scon", ld_importo_conai_scon_det)			
					dw_det_fat_acq_lista.setitem(dw_det_fat_acq_lista.getrow(), "cod_iva", ls_cod_iva_det)
																		
								
				end if  
			end if	
//----------------------------- fine modifica Nicola ----------------------------------------			

//         ls_cod_prodotto = i_coltext
//         ld_quantita = this.getitemnumber(i_rownbr, "quan_fatturata")
//         if ls_flag_decimali = "N" and &
//            ld_quantita - int(ld_quantita) > 0 then
//            ld_quantita = ceiling(ld_quantita)
//            this.setitem(i_rownbr, "quan_fatturata", ld_quantita)
//         end if

         if f_leggi_nota_prodotto(ls_cod_tipo_det_acq, ls_cod_prodotto, ls_nota_prodotto) = 0 then
				dw_det_fat_acq_det_1.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
				dw_det_fat_acq_det_1.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if
			
		case "cod_misura"
			
			if not isnull(i_coltext) and i_coltext <> "" then
	   		cb_prezzo.text = "Prezzo al " + i_coltext
			else
				cb_prezzo.text = "Prezzo"
			end if
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.cod_misura_mag
         into   :ls_cod_misura_mag
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> i_coltext and len(trim(i_coltext)) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_fat_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + i_coltext + ")'")
			else
				dw_det_fat_acq_det_1.modify("st_fattore_conv.text=''")
			end if
			
		case "fat_conversione"
			
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.cod_misura_mag
         into   :ls_cod_misura_mag
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> this.getitemstring(i_rownbr, "cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_fat_acq_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + i_coltext + " " + this.getitemstring(i_rownbr, "cod_misura") + ")'")
			else
				dw_det_fat_acq_det_1.modify("st_fattore_conv.text=''")
			end if
			
		case "quan_fatturata"
			
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.flag_decimali
         into   :ls_flag_decimali
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;
   
         if sqlca.sqlcode = 0 then
            if ls_flag_decimali = "N" and &
               (double(i_coltext) - int(double(i_coltext))) > 0 then
               i_coltext = string(ceiling(double(i_coltext)))
               this.setitem(i_rownbr, "quan_fatturata", double(i_coltext))
               this.settext(i_coltext)
               return 2
            end if
         end if

         ld_quantita = double(i_coltext)
         f_listini_fornitori(ls_cod_tipo_listino_prodotto, ls_cod_fornitore, ls_cod_valuta, ld_cambio_acq, ls_cod_prodotto, ld_quantita, ldt_data_doc_origine, ls_cod_tipo_det_acq)

		case "num_confezioni"	
			
			ls_cod_prodotto = this.getitemstring(dw_det_fat_acq_lista.getrow(), "cod_prodotto")
			ld_num_confezioni = double(i_coltext)
			ld_num_pezzi  = this.getitemnumber(dw_det_fat_acq_lista.getrow(),"num_pezzi_confezione")
			ls_cod_misura = this.getitemstring(dw_det_fat_acq_lista.getrow(),"cod_misura")			
			if not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then			
				select flag_prezzo
				into   :ls_flag_prezzo
				from   tab_misure
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_misura = :ls_cod_misura;
				if sqlca.sqlcode = 0 then
					if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
						if ls_flag_prezzo = 'S' then
							ld_quantita = ld_num_pezzi * ld_num_confezioni
							this.setitem(getrow(), "quan_fatturata", ld_quantita)
						else
							ld_quantita = ld_num_confezioni
							this.setitem(getrow(), "quan_fatturata", ld_quantita)
						end if
					end if
				else
					g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
					return 1
				end if	
			else
				ld_quantita = ld_num_pezzi * ld_num_confezioni
				this.setitem(getrow(), "quan_fatturata", ld_quantita)				
			end if
			
		case "num_pezzi_confezione"	
			
			ls_cod_prodotto = this.getitemstring(dw_det_fat_acq_lista.getrow(), "cod_prodotto")
			ld_num_confezioni = this.getitemnumber(dw_det_fat_acq_lista.getrow(),"num_confezioni")
			ld_num_pezzi  = double(i_coltext)
			ls_cod_misura = this.getitemstring(dw_det_fat_acq_lista.getrow(),"cod_misura")
			if not isnull(ls_cod_prodotto) and len(ls_cod_prodotto) > 0 then
				select flag_prezzo
				into   :ls_flag_prezzo
				from   tab_misure
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_misura = :ls_cod_misura;
				if sqlca.sqlcode = 0 then
					if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
						if ls_flag_prezzo = 'S' then
							ld_quantita = ld_num_pezzi * ld_num_confezioni
							this.setitem(getrow(), "quan_fatturata", ld_quantita)
						else
							ld_quantita = ld_num_confezioni
							this.setitem(getrow(), "quan_fatturata", ld_quantita)
						end if
					end if
				else
					g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
					return 1
				end if					
			else
				ld_quantita = ld_num_pezzi * ld_num_confezioni
				this.setitem(getrow(), "quan_fatturata", ld_quantita)				
			end if
			
		case "imponibile_iva_valuta"
			
			if g_mb.messagebox("APICE", "Forzo l'imponibile della riga =" +i_coltext+ " ?", question!, YesNo!,1) = 1 then
				setitem(getrow(),"flag_imponibile_forzato", "S")
				
				
				
				if getitemnumber(getrow(),"quan_fatturata") <> 0 then
					
					ld_imponibile_iva = dec(i_coltext)
					
					if not isnull(getitemnumber( getrow(), "sconto_10")) then
						
						if ( getitemnumber( getrow(), "sconto_10") > 0 ) then
							
							ld_imponibile_iva = ((ld_imponibile_iva * 100)/(100 - getitemnumber( getrow(), "sconto_10")))
							
						end if
						
					end if
					
					if not isnull(getitemnumber( getrow(), "sconto_9")) then
						
						if ( getitemnumber( getrow(), "sconto_9") > 0 ) then
							
							ld_imponibile_iva = ((ld_imponibile_iva * 100)/(100 - getitemnumber( getrow(), "sconto_9")))
							
						end if
						
					end if
					
					if not isnull(getitemnumber( getrow(), "sconto_8")) then
						
						if ( getitemnumber( getrow(), "sconto_8") > 0 ) then
							
							ld_imponibile_iva = ((ld_imponibile_iva * 100)/(100 - getitemnumber( getrow(), "sconto_8")))
							
						end if
						
					end if
					
					if not isnull(getitemnumber( getrow(), "sconto_7")) then
						
						if ( getitemnumber( getrow(), "sconto_7") > 0 ) then
							
							ld_imponibile_iva = ((ld_imponibile_iva * 100)/(100 - getitemnumber( getrow(), "sconto_7")))
							
						end if
						
					end if
					
					if not isnull(getitemnumber( getrow(), "sconto_6")) then
						
						if ( getitemnumber( getrow(), "sconto_6") > 0 ) then
							
							ld_imponibile_iva = ((ld_imponibile_iva * 100)/(100 - getitemnumber( getrow(), "sconto_6")))
							
						end if
						
					end if
					
					if not isnull(getitemnumber( getrow(), "sconto_5")) then
						
						if ( getitemnumber( getrow(), "sconto_5") > 0 ) then
							
							ld_imponibile_iva = ((ld_imponibile_iva * 100)/(100 - getitemnumber( getrow(), "sconto_5")))
							
						end if
						
					end if
					
					if not isnull(getitemnumber( getrow(), "sconto_4")) then
						
						if ( getitemnumber( getrow(), "sconto_4") > 0 ) then
							
							ld_imponibile_iva = ((ld_imponibile_iva * 100)/(100 - getitemnumber( getrow(), "sconto_4")))
							
						end if
						
					end if
					
					if not isnull(getitemnumber( getrow(), "sconto_3")) then
						
						if ( getitemnumber( getrow(), "sconto_3") > 0 ) then
							
							ld_imponibile_iva = ((ld_imponibile_iva * 100)/(100 - getitemnumber( getrow(), "sconto_3")))
							
						end if
						
					end if
					
					if not isnull(getitemnumber( getrow(), "sconto_2")) then
						
						if ( getitemnumber( getrow(), "sconto_2") > 0 ) then
							
							ld_imponibile_iva = ((ld_imponibile_iva * 100)/(100 - getitemnumber( getrow(), "sconto_2")))
							
						end if
						
					end if
					
					if not isnull(getitemnumber( getrow(), "sconto_1")) then
						
						if ( getitemnumber( getrow(), "sconto_1") > 0 ) then
							
							ld_imponibile_iva = ((ld_imponibile_iva * 100)/(100 - getitemnumber( getrow(), "sconto_1")))
							
						end if
						
					end if					
					
					setitem(getrow(),"prezzo_acquisto", ld_imponibile_iva / getitemnumber(getrow(),"quan_fatturata"))
				else
					setitem(getrow(),"prezzo_acquisto", 0)
				end if
				
			end if
			
   end choose
end if
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_modify, ls_flag_tipo_det_acq, ls_cod_tipo_det_acq, &
       ls_null, ls_match, ls_match_1


setnull(ls_null)
if i_rownbr > 0 then
   ls_cod_tipo_det_acq = this.getitemstring(i_rownbr,"cod_tipo_det_acq")

   select tab_tipi_det_acq.flag_tipo_det_acq
   into   :ls_flag_tipo_det_acq
   from   tab_tipi_det_acq
   where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio acquisti. " + sqlca.sqlerrtext, &
                 exclamation!, ok!)
      return
   end if

   if i_insave > 0 then
      if ls_flag_tipo_det_acq = "M" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("cod_misura")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Unità di misura obbligatoria.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("quan_fatturata")
         if dec(get_col_data(i_rownbr, i_colnbr)) = 0 then
          	g_mb.messagebox("Attenzione", "Quantità fattura obbligatoria.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      elseif ls_flag_tipo_det_acq = "C" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      end if
   end if
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	dw_det_fat_acq_lista.setrow(getrow())
end if
end event

event ue_key;call super::ue_key;long     ll_cont

datetime ldt_data_consegna

dec{6}   ld_fat_conversione

dec{4}   ld_prezzo_acquisto

double   ld_sconti[10], ld_sconto_tot

string   ls_cod_fornitore, ls_cod_valuta, ls_cod_misura, ls_cod_prodotto

uo_condizioni_cliente luo_condizioni_cliente


choose case getcolumnname()
		
	case "prezzo_acquisto"
		
		if key = keyF2!  and keyflags = 1 then
			
			if g_mb.messagebox("APICE","Memorizzo il prezzo di acquisto nei listini fornitori?", Question!, YesNo!, 1) = 1 then
				
				ls_cod_prodotto       = getitemstring(getrow(),"cod_prodotto")
				ls_cod_fornitore      = dw_det_fat_acq_lista.i_parentdw.getitemstring(dw_det_fat_acq_lista.i_parentdw.getrow(), "cod_fornitore")
				ls_cod_valuta         = dw_det_fat_acq_lista.i_parentdw.getitemstring(dw_det_fat_acq_lista.i_parentdw.getrow(), "cod_valuta")
				ls_cod_misura         = getitemstring(getrow(),"cod_misura")
				ld_fat_conversione    = getitemnumber(getrow(),"fat_conversione")
				ldt_data_consegna = dw_det_fat_acq_lista.i_parentdw.getitemdatetime(dw_det_fat_acq_lista.i_parentdw.getrow(), "data_registrazione")
								
				ld_prezzo_acquisto = dec(gettext())
				ld_sconti[1] = getitemnumber(getrow(),"sconto_1")
				ld_sconti[2] = getitemnumber(getrow(),"sconto_2")
				ld_sconti[3] = getitemnumber(getrow(),"sconto_3")
				ld_sconti[4] = getitemnumber(getrow(),"sconto_4")
				ld_sconti[5] = getitemnumber(getrow(),"sconto_5")
				ld_sconti[6] = getitemnumber(getrow(),"sconto_6")
				ld_sconti[7] = getitemnumber(getrow(),"sconto_7")
				ld_sconti[8] = getitemnumber(getrow(),"sconto_8")
				ld_sconti[9] = getitemnumber(getrow(),"sconto_9")
				ld_sconti[10] = getitemnumber(getrow(),"sconto_10")
				
				luo_condizioni_cliente = CREATE uo_condizioni_cliente 
				luo_condizioni_cliente.uof_calcola_sconto_tot(ld_sconti[], ld_sconto_tot)
				destroy luo_condizioni_cliente
				
				select count(*) 
				into   :ll_cont
				from   listini_fornitori
				where  cod_azienda   = :s_cs_xx.cod_azienda and
						 cod_prodotto  = :ls_cod_prodotto and
						 cod_fornitore = :ls_cod_fornitore and
						 cod_valuta    = :ls_cod_valuta and
						 data_inizio_val = :ldt_data_consegna;
						 
				if ll_cont = 0 then		// nessun record allora inserisco
				
					INSERT INTO listini_fornitori  
							( cod_azienda,   
							  cod_prodotto,   
							  cod_fornitore,   
							  cod_valuta,   
							  data_inizio_val,   
							  des_listino_for,   
							  quantita_1,   
							  prezzo_1,   
							  sconto_1,   
							  quantita_2,   
							  prezzo_2,   
							  sconto_2,   
							  quantita_3,   
							  prezzo_3,   
							  sconto_3,   
							  quantita_4,   
							  prezzo_4,   
							  sconto_4,   
							  quantita_5,   
							  prezzo_5,   
							  sconto_5,   
							  flag_for_pref,   
							  prezzo_ult_acquisto,   
							  data_ult_acquisto,   
							  cod_misura,   
							  fat_conversione )  
					VALUES ( :s_cs_xx.cod_azienda,   
							  :ls_cod_prodotto,   
							  :ls_cod_fornitore,   
							  :ls_cod_valuta,   
							  :ldt_data_consegna,   
							  null,   
							  99999999,   
							  :ld_prezzo_acquisto,   
							  :ld_sconto_tot,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  0,   
							  'N',   
							  0,   
							  null,   
							  :ls_cod_misura,   
							  :ld_fat_conversione)  ;
							  
						if sqlca.sqlcode <> 0 then
							g_mb.messagebox("APICE","Errore in inserimento condizione fornitore-prodotto. Dettaglio: " + sqlca.sqlerrtext)
							rollback;
							return -1
						end if
						
					else
						
					if g_mb.messagebox("APICE","Aggiorno la condizione del fornitore?",Question!,YesNo!,1) = 1 then
						
						update listini_fornitori
						set    prezzo_1 = :ld_prezzo_acquisto, 
								 sconto_1 = :ld_sconto_tot
						where  cod_azienda = :s_cs_xx.cod_azienda and
								 cod_prodotto = :ls_cod_prodotto and
								 cod_fornitore = :ls_cod_fornitore and
								 cod_valuta = :ls_cod_valuta and
								 data_inizio_val = :ldt_data_consegna;
								 
						if sqlca.sqlcode <> 0 then
							g_mb.messagebox("APICE","Errore in aggiornamento condizione fornitore-prodotto. Dettaglio: " + sqlca.sqlerrtext)
							rollback;
							return -1
						end if
						
					end if
					
				end if
				
			end if
			
			if g_mb.messagebox("APICE","Memorizzo il costo ultimo in anagrafica prodotti?", Question!, YesNo!, 1) = 1 then
				
				ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
				
				ld_prezzo_acquisto = dec(gettext())
				
				update anag_prodotti
				set    costo_ultimo = :ld_prezzo_acquisto
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :ls_cod_prodotto;
						 
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore nella update di anag_prodotti: " + sqlca.sqlerrtext)
					rollback;
				end if
				
			end if

		end if
		
		if key = keyF1!  and keyflags = 1 then			// SHIFT-F1 ricerca nello storico prezzi
			s_cs_xx.parametri.parametro_s_8 = getitemstring(getrow(),"cod_prodotto")
			s_cs_xx.parametri.parametro_s_9 = dw_det_fat_acq_lista.i_parentdw.getitemstring(dw_det_fat_acq_lista.i_parentdw.getrow(), "cod_fornitore")
			s_cs_xx.parametri.parametro_s_10 = "T"
			//-CLAUDIA 23/07/07 MIGLIORAMENTO PERCHè SI POSSA RICERCARE PER DESCRIZIONE
			s_cs_xx.parametri.parametro_s_12 = '%'+Left (getitemstring(getrow(),"des_prodotto"), 20)+'%'
			s_cs_xx.parametri.parametro_d_1 = 0
			s_cs_xx.parametri.parametro_uo_dw_1 = this
			if isvalid(w_det_ord_acq_storico) then
				w_det_ord_acq_storico.show()
			else
				window_open(w_det_ord_acq_storico, 0)
			end if
		end if
		
	//Giulio: 03/11/2011 modifica ricerca prodotti
	case "cod_prodotto","des_prodotto"
		if key = keyF1!  and keyflags = 1 then
			dw_det_fat_acq_det_1.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_fat_acq_det_1, "cod_prodotto")
		end if
end choose

commit;
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_det_fat_acq_det_1,"cod_prodotto")
end choose
end event

event clicked;call super::clicked;long								ll_num_mov, ll_row
integer							li_anno_mov
s_cs_xx_parametri				lstr_param
string								ls_anno_mov_col, ls_num_mov_col

w_mov_magazzino_tv							lw_window

choose case dwo.name
	case "p_mov", "p_mov_ret", "p_mov_terzista"
		//apri la window dei movimenti di magazzino
		ll_row = dw_det_fat_acq_det_1.getrow()
		
		if ll_row>0 then
			
			if dwo.name = "p_mov" then
				ls_anno_mov_col = "anno_registrazione_mov_mag"
				ls_num_mov_col = "num_registrazione_mov_mag"
			elseif dwo.name = "p_mov_ret" then
				ls_anno_mov_col = "anno_reg_mov_mag_ret"
				ls_num_mov_col = "num_reg_mov_mag_ret"
			else
				ls_anno_mov_col = "anno_reg_mov_mag_terzista"
				ls_num_mov_col = "num_reg_mov_mag_terzista"
			end if
			
			li_anno_mov = dw_det_fat_acq_det_1.getitemnumber(ll_row, ls_anno_mov_col)
			ll_num_mov = dw_det_fat_acq_det_1.getitemnumber(ll_row, ls_num_mov_col)
				
			if li_anno_mov>0 and ll_num_mov>0 then
				lstr_param.parametro_ul_1 = li_anno_mov
				lstr_param.parametro_ul_2 = ll_num_mov
				
				opensheetwithparm(lw_window, lstr_param, PCCA.MDI_Frame, 6,  Original!)
				//openwithparm(lw_window, lstr_param)
			else
				//nessun movimento associato
				return
			end if
		end if
		
end choose
end event

type dw_det_fat_acq_cc from uo_cs_xx_dw within w_det_fat_acq
integer x = 64
integer y = 124
integer width = 3419
integer height = 1036
integer taborder = 50
string dataobject = "d_det_fat_acq_cc"
boolean border = false
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_acq

if dw_det_fat_acq_lista.getrow() > 0 then
	ll_anno_registrazione = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "num_registrazione")
	ll_prog_riga_fat_acq = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "prog_riga_fat_acq")
	
	ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_fat_acq)
	
	if ll_errore < 0 then
		pcca.error = c_fatal
	end if
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_fat_acq

ll_anno_registrazione = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "anno_registrazione")
ll_num_registrazione = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "num_registrazione")
ll_prog_riga_fat_acq = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "prog_riga_fat_acq")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "prog_riga_fat_acq")) or &
      this.getitemnumber(ll_i, "prog_riga_fat_acq") = 0 then
      this.setitem(ll_i, "prog_riga_fat_acq", ll_prog_riga_fat_acq)
   end if
next

end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_flag_cc_manuali
	long ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_fat_acq
	dec{4} ld_imponibile_iva,ld_percento,ld_importo
	
	ll_anno_registrazione = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "num_registrazione")
	ll_prog_riga_fat_acq = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "prog_riga_fat_acq")
	ld_imponibile_iva = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "imponibile_iva")
	
	choose case i_colname
			
		case "percentuale"
			// se scrivo la percentuale, calcolo importo
			ld_percento = dec(i_coltext)
			ld_importo = (ld_imponibile_iva / 100 * ld_percento)
			setitem(getrow(),"importo", ld_importo)
			
		case "importo"
			// se scrivo l'importo, calcolo la percentuale
			if ld_imponibile_iva = 0 then 
				g_mb.messagebox("APICE", "Attenzione! L'imponibile della riga è zero.")
				return 0
			end if
			ld_importo = dec(i_coltext)
			ld_percento = round((ld_importo * 100) / ld_imponibile_iva, 2)
			setitem(getrow(),"percentuale", ld_percento)
			
	end choose
	
	select flag_cc_manuali
	into   :ls_flag_cc_manuali
	from   det_fat_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione  = :ll_num_registrazione and
			 prog_riga_fat_acq = :ll_prog_riga_fat_acq;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE", "Errore ricerca dello stato del flag centri costo manuali~r~n"+sqlca.sqlerrtext)
		return 0
	end if
	
	if ls_flag_cc_manuali = "N" then
		update det_fat_acq
		set flag_cc_manuali = 'S'
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione  = :ll_num_registrazione and
				 prog_riga_fat_acq = :ll_prog_riga_fat_acq;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE", "Errore nella impostazione del flag centri costo manuali~r~n"+sqlca.sqlerrtext)
			return 0
		end if
		
		commit;
	end if
end if
end event

event updateend;call super::updateend;long ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_fat_acq, ll_cont

ll_anno_registrazione = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "anno_registrazione")
ll_num_registrazione = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "num_registrazione")
ll_prog_riga_fat_acq = dw_det_fat_acq_lista.getitemnumber(dw_det_fat_acq_lista.getrow(), "prog_riga_fat_acq")

ll_cont = 0

select count(*)
into   :ll_cont
from   det_fat_acq_cc
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_registrazione and
		 num_registrazione  = :ll_num_registrazione and
		 prog_riga_fat_acq = :ll_prog_riga_fat_acq;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE", "Errore ricerca dello stato del flag centri costo manuali~r~n"+sqlca.sqlerrtext)
	return 0
end if

if ll_cont < 1 then

	update det_fat_acq
	set flag_cc_manuali = 'N'
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione  = :ll_num_registrazione and
			 prog_riga_fat_acq = :ll_prog_riga_fat_acq;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE", "Errore nella impostazione del flag centri costo manuali~r~n"+sqlca.sqlerrtext)
		return 0
	end if
	
	commit;
	
end if

end event

type dw_det_fat_acq_det_3 from uo_cs_xx_dw within w_det_fat_acq
event ue_azzera_campo ( )
integer x = 64
integer y = 124
integer width = 3429
integer height = 1364
integer taborder = 70
string dataobject = "d_det_fat_acq_det_3"
end type

event ue_azzera_campo();setitem(getrow(),"val_grezzo",0)
setcolumn("val_grezzo")
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	dw_det_fat_acq_lista.setrow(getrow())
end if
end event

event ue_key;call super::ue_key;string ls_nota_fissa

choose case this.getcolumnname()
	
//	case "nota_dettaglio"
//	if key = keyF1!  and keyflags = 1 then
//		s_cs_xx.parametri.parametro_s_1 = dw_det_fat_acq_det_1.getitemstring(this.getrow(),"cod_prodotto")
//		dw_det_fat_acq_det_1.change_dw_current()
//		setnull(s_cs_xx.parametri.parametro_s_2)
//		window_open(w_prod_note_ricerca, 0)
//		if not isnull(s_cs_xx.parametri.parametro_s_2) then
//			this.setcolumn("nota_dettaglio")
//			this.settext(this.gettext() + "~r~n" + s_cs_xx.parametri.parametro_s_2)
//		end if
//	end if
	
	//Giulio: 03/11/2011 modifica ricerca prodotti
	case "nota_dettaglio","des_prodotto", "cod_prodotto"
		if key = keyF1!  and keyflags = 1 then
			dw_det_fat_acq_det_1.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_fat_acq_det_3, "cod_prodotto")
		end if
	
end choose

end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_des_prodotto
	
	choose case i_colname
		case "cod_prodotto_grezzo"
			if isnull(i_coltext) or i_coltext = "" then
				setitem(getrow(),"des_prodotto_grezzo","")
			else
				select des_prodotto
				into   :ls_des_prodotto
				from   anag_prodotti
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_prodotto = :i_coltext;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore in ricerca prodotto grezzo~r~n"+sqlca.sqlerrtext, Stopsign!)
					return 1
				end if
				
				setitem(getrow(),"des_prodotto_grezzo",ls_des_prodotto)
			end if
		case "val_grezzo"
			if (not isnull(i_coltext) or dec(i_coltext) > 0) and isnull(getitemstring(getrow(),"cod_prodotto_grezzo")) then
				g_mb.messagebox("APICE","Specificare PRIMA un codice di grezzo!",information!)
				postevent("ue_azzera_campo")
			end if
				
	end choose
end if
end event

type dw_supporto_dati from datawindow within w_det_fat_acq
integer x = 69
integer y = 1080
integer width = 3246
integer height = 460
integer taborder = 100
string dataobject = "d_dati_supporto_fat_acq"
boolean border = false
boolean livescroll = true
end type

type dw_folder from u_folder within w_det_fat_acq
integer x = 23
integer y = 20
integer width = 3497
integer height = 1712
integer taborder = 50
end type

event po_tabclicked;call super::po_tabclicked;choose case i_SelectedTab	
	case 4
		
		dw_det_fat_acq_cc.change_dw_current()
		iuo_dw_main = dw_det_fat_acq_cc
		parent.postevent("pc_retrieve")
		dw_det_fat_acq_cc.ib_proteggi_chiavi = false
		
	case else
		
		iuo_dw_main = dw_det_fat_acq_lista
		dw_det_fat_acq_lista.ib_proteggi_chiavi = true
		
end choose		
end event

