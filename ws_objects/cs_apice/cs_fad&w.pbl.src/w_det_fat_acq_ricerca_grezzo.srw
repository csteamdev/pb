﻿$PBExportHeader$w_det_fat_acq_ricerca_grezzo.srw
forward
global type w_det_fat_acq_ricerca_grezzo from w_cs_xx_risposta
end type
type dw_ord_acq from uo_cs_xx_dw within w_det_fat_acq_ricerca_grezzo
end type
type dw_folder from u_folder within w_det_fat_acq_ricerca_grezzo
end type
type dw_ricerca from uo_cs_xx_dw within w_det_fat_acq_ricerca_grezzo
end type
type dw_fat_acq from uo_cs_xx_dw within w_det_fat_acq_ricerca_grezzo
end type
type dw_bol_acq from uo_cs_xx_dw within w_det_fat_acq_ricerca_grezzo
end type
type dw_mov_mag from uo_cs_xx_dw within w_det_fat_acq_ricerca_grezzo
end type
end forward

global type w_det_fat_acq_ricerca_grezzo from w_cs_xx_risposta
integer width = 3735
integer height = 1740
string title = "Ricerca Prodotto Grezzo"
dw_ord_acq dw_ord_acq
dw_folder dw_folder
dw_ricerca dw_ricerca
dw_fat_acq dw_fat_acq
dw_bol_acq dw_bol_acq
dw_mov_mag dw_mov_mag
end type
global w_det_fat_acq_ricerca_grezzo w_det_fat_acq_ricerca_grezzo

type variables
boolean ib_partenza=true
string is_sql_mov_mag, is_sql_ord_acq,is_sql_bol_acq,is_sql_fat_acq
end variables

forward prototypes
public subroutine wf_calcola_netto_riga (decimal fd_prezzo, decimal fd_sconto_1, decimal fd_sconto_2, decimal fd_sconto_3, decimal fd_sconto_4, decimal fd_sconto_5, decimal fd_sconto_6, decimal fd_sconto_7, decimal fd_sconto_8, decimal fd_sconto_9, decimal fd_sconto_10, ref decimal fd_prezzo_netto)
public function integer wf_imposta_sql (ref uo_cs_xx_dw fdw_oggetto, string fs_campo_azienda, string fs_campo_prodotto, string fs_campo_data, string ls_cod_prodotto, datetime ldt_data_inizio, datetime ldt_data_fine)
end prototypes

public subroutine wf_calcola_netto_riga (decimal fd_prezzo, decimal fd_sconto_1, decimal fd_sconto_2, decimal fd_sconto_3, decimal fd_sconto_4, decimal fd_sconto_5, decimal fd_sconto_6, decimal fd_sconto_7, decimal fd_sconto_8, decimal fd_sconto_9, decimal fd_sconto_10, ref decimal fd_prezzo_netto);dec{4} ld_perc, ld_sconto_tot, ld_prezzo_scontato, ld_precisione

ld_perc = 100
ld_perc = ld_perc - ((ld_perc * fd_sconto_1) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_2) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_3) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_4) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_5) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_6) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_7) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_8) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_9) / 100)
ld_perc = ld_perc - ((ld_perc * fd_sconto_10) / 100)

ld_sconto_tot = round(100 - ld_perc,4)

ld_prezzo_scontato = fd_prezzo - ((fd_prezzo * ld_sconto_tot) / 100)
		 
ld_prezzo_scontato = round(ld_prezzo_scontato, 4)	

fd_prezzo_netto = ld_prezzo_scontato
return
end subroutine

public function integer wf_imposta_sql (ref uo_cs_xx_dw fdw_oggetto, string fs_campo_azienda, string fs_campo_prodotto, string fs_campo_data, string ls_cod_prodotto, datetime ldt_data_inizio, datetime ldt_data_fine);string ls_sql, ls_sql_prima, ls_sql_dopo, ls_where, ls_sql_finale
long ll_pos, ll_ret

ls_sql = fdw_oggetto.getsqlselect()
if ls_sql = "" or isnull(ls_sql) then return -1

ll_pos = pos(ls_sql, "ORDER BY",1)
if ll_pos = 0 then
	ll_pos = pos(ls_sql, "GROUP BY",1)
	if ll_pos = 0 then
		// non ho trovato ne la clausola order ne group; quindi la select ha solo la where
		ll_pos = len(ls_sql)
		ls_sql_dopo = ""
	end if
end if
ls_sql_dopo = mid(ls_sql, ll_pos)
ls_sql_prima = left(ls_sql, ll_pos - 1)

// imposto il pezzo da aggiungere alla where

ls_where = " ( " + fs_campo_azienda + " = '" + s_cs_xx.cod_azienda + "') "

if not isnull(ls_cod_prodotto) then
	if len(ls_where) > 0 then ls_where = ls_where + " and "
	ls_where = ls_where + "( "+fs_campo_prodotto+" = '" + ls_cod_prodotto + "') "
end if

if not isnull(ldt_data_inizio) and ldt_data_inizio > datetime(date("01/01/1900"), 00:00:00) then
	if len(ls_where) > 0 then ls_where = ls_where + " and "
	ls_where = ls_where + " ( " + fs_campo_data + " >= '" + string(ldt_data_inizio,s_cs_xx.db_funzioni.formato_data) + "') "
end if
	
if not isnull(ldt_data_fine) and ldt_data_fine > datetime(date("01/01/1900"), 00:00:00) then
	if len(ls_where) > 0 then ls_where = ls_where + " and "
	ls_where = ls_where + " ( " + fs_campo_data + " <= '" + string(ldt_data_fine,s_cs_xx.db_funzioni.formato_data) + "') "
end if

// verifico se esiste già una where precedente a cui aggiungere
ll_pos = pos(ls_sql, "WHERE",1)
if ll_pos > 0 then
	ls_sql_finale = ls_sql_prima + " and "
else
	ls_sql_finale = ls_sql_prima + " WHERE "
end if
//ls_sql_finale = ls_sql_finale + "(" + ls_where + ") " + ls_sql_dopo
ls_sql_finale = ls_sql_finale + ls_where + ls_sql_dopo

ll_ret = fdw_oggetto.setsqlselect(ls_sql_finale)
if ll_ret = 1 then
	return 0
else
	return -1
end if

end function

on w_det_fat_acq_ricerca_grezzo.create
int iCurrent
call super::create
this.dw_ord_acq=create dw_ord_acq
this.dw_folder=create dw_folder
this.dw_ricerca=create dw_ricerca
this.dw_fat_acq=create dw_fat_acq
this.dw_bol_acq=create dw_bol_acq
this.dw_mov_mag=create dw_mov_mag
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_ord_acq
this.Control[iCurrent+2]=this.dw_folder
this.Control[iCurrent+3]=this.dw_ricerca
this.Control[iCurrent+4]=this.dw_fat_acq
this.Control[iCurrent+5]=this.dw_bol_acq
this.Control[iCurrent+6]=this.dw_mov_mag
end on

on w_det_fat_acq_ricerca_grezzo.destroy
call super::destroy
destroy(this.dw_ord_acq)
destroy(this.dw_folder)
destroy(this.dw_ricerca)
destroy(this.dw_fat_acq)
destroy(this.dw_bol_acq)
destroy(this.dw_mov_mag)
end on

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

set_w_options(c_closenosave + c_noresizewin)

dw_ricerca.set_dw_options(sqlca, &
                           pcca.null_object, &
                           c_nomodify + c_nodelete + c_newonopen + c_disableCC, &
                           c_noresizedw + c_nohighlightselected + c_cursorrowpointer)

dw_mov_mag.set_dw_options(sqlca, &
									pcca.null_object, &
									c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete + c_noenablenewonopen + c_noenablemodifyonopen + c_scrollparent + c_disablecc, &
									c_noresizedw + c_nohighlightselected + c_nocursorrowfocusrect + c_nocursorrowpointer + c_InactiveDWColorUnchanged)

dw_ord_acq.set_dw_options(sqlca, &
									pcca.null_object, &
									c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete + c_noenablenewonopen + c_noenablemodifyonopen + c_scrollparent + c_disablecc, &
									c_noresizedw + c_nohighlightselected + c_nocursorrowfocusrect + c_nocursorrowpointer)

dw_bol_acq.set_dw_options(sqlca, &
									pcca.null_object, &
									c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete + c_noenablenewonopen + c_noenablemodifyonopen + c_scrollparent + c_disablecc, &
									c_noresizedw + c_nohighlightselected + c_nocursorrowfocusrect + c_nocursorrowpointer)

dw_fat_acq.set_dw_options(sqlca, &
									pcca.null_object, &
									c_noretrieveonopen + c_nonew + c_nomodify + c_nodelete + c_noenablenewonopen + c_noenablemodifyonopen + c_scrollparent + c_disablecc, &
									c_noresizedw + c_nohighlightselected + c_nocursorrowfocusrect + c_nocursorrowpointer)

lw_oggetti[1] = dw_mov_mag
dw_folder.fu_assigntab(1, "Mov.Magazzino", lw_oggetti[])

lw_oggetti[1] = dw_ord_acq
dw_folder.fu_assigntab(2, "Ordini Acquisto", lw_oggetti[])

lw_oggetti[1] = dw_bol_acq
dw_folder.fu_assigntab(3, "Bolle Entrata", lw_oggetti[])

lw_oggetti[1] = dw_fat_acq
dw_folder.fu_assigntab(4, "Fatture Acquisto", lw_oggetti[])

dw_folder.fu_foldercreate(4, 4)
dw_folder.fu_selecttab(1)

ib_partenza = false
is_sql_mov_mag = dw_mov_mag.getsqlselect()
is_sql_ord_acq = dw_ord_acq.getsqlselect()
is_sql_bol_acq = dw_bol_acq.getsqlselect()
is_sql_fat_acq = dw_fat_acq.getsqlselect()

end event

type dw_ord_acq from uo_cs_xx_dw within w_det_fat_acq_ricerca_grezzo
event ue_retrieve ( )
integer x = 46
integer y = 396
integer width = 3607
integer height = 1204
integer taborder = 40
string dataobject = "d_det_fat_acq_ricerca_grezzo_ord_acq"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_retrieve();// modifica SQL della DW

dw_ord_acq.change_dw_focus(dw_ord_acq)
dw_ord_acq.setsqlselect(is_sql_ord_acq)

if wf_imposta_sql(ref dw_ord_acq, &
               "tes_ord_acq.cod_azienda", &
               "det_ord_acq.cod_prodotto", &
               "tes_ord_acq.data_registrazione", &
					dw_ricerca.getitemstring(dw_ricerca.getrow(),"cod_prodotto"), &
					dw_ricerca.getitemdatetime(dw_ricerca.getrow(),"data_inizio"), &
					dw_ricerca.getitemdatetime(dw_ricerca.getrow(),"data_fine")) = -1 then
	g_mb.messagebox("APICE","Errore nell'impostazione del SQL dinamico della DW; comunicare l'errore al servizio di assistenza",StopSign!)
	return
end if

// esegui retrieve
parent.postevent("pc_retrieve")

return
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event doubleclicked;call super::doubleclicked;if row > 0 then
	s_cs_xx.parametri.parametro_s_1 = getitemstring(getrow(),"det_ord_acq_cod_prodotto")
	s_cs_xx.parametri.parametro_s_2 = getitemstring(getrow(),"det_ord_acq_des_prodotto")
	if isnull(s_cs_xx.parametri.parametro_s_2) or len(s_cs_xx.parametri.parametro_s_2) < 1 then
		select des_prodotto
		into   :s_cs_xx.parametri.parametro_s_2
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :s_cs_xx.parametri.parametro_s_1;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore durante la ricerca della descrizione del prodotto in anagrafica prodotti.~r~n" + sqlca.sqlerrtext,stopsign!)
			s_cs_xx.parametri.parametro_s_1 = ""
			s_cs_xx.parametri.parametro_s_2 = ""
			return
		end if
	end if
	
	wf_calcola_netto_riga(getitemdecimal(getrow(),"det_ord_acq_prezzo_acquisto"), &
								 getitemdecimal(getrow(),"det_ord_acq_sconto_1"), &
								 getitemdecimal(getrow(),"det_ord_acq_sconto_2"), &
								 getitemdecimal(getrow(),"det_ord_acq_sconto_3"), &
								 getitemdecimal(getrow(),"det_ord_acq_sconto_4"), &
								 getitemdecimal(getrow(),"det_ord_acq_sconto_5"), &
								 getitemdecimal(getrow(),"det_ord_acq_sconto_6"), &
								 getitemdecimal(getrow(),"det_ord_acq_sconto_7"), &
								 getitemdecimal(getrow(),"det_ord_acq_sconto_8"), &
								 getitemdecimal(getrow(),"det_ord_acq_sconto_9"), &
								 getitemdecimal(getrow(),"det_ord_acq_sconto_10"), &
								 s_cs_xx.parametri.parametro_dec4_1)
	
	close(parent)
end if
end event

type dw_folder from u_folder within w_det_fat_acq_ricerca_grezzo
integer x = 23
integer y = 260
integer width = 3657
integer height = 1360
integer taborder = 20
end type

event clicked;call super::clicked;dw_ricerca.accepttext()
if not ib_partenza then
	choose case i_SelectedTab
		case 1
			dw_mov_mag.triggerevent("ue_retrieve")
		case 2
			dw_ord_acq.triggerevent("ue_retrieve")
		case 3
			dw_bol_acq.triggerevent("ue_retrieve")
		case 4
			dw_fat_acq.triggerevent("ue_retrieve")
	end choose
end if
end event

type dw_ricerca from uo_cs_xx_dw within w_det_fat_acq_ricerca_grezzo
integer x = 23
integer y = 20
integer width = 2176
integer height = 240
integer taborder = 10
string dataobject = "d_det_fat_acq_ricerca_grezzo_ricerca"
boolean border = false
end type

event pcd_new;call super::pcd_new;if i_extendmode then
	datetime ldt_data
	setitem(getrow(),"cod_prodotto",s_cs_xx.parametri.parametro_s_2)
	ldt_data = datetime(relativedate(today(),-180), 00:00:00)
	setitem(getrow(),"data_inizio",ldt_data)
	ldt_data = datetime(today(), 00:00:00)
	setitem(getrow(),"data_fine",ldt_data)
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto")
end choose
end event

type dw_fat_acq from uo_cs_xx_dw within w_det_fat_acq_ricerca_grezzo
event ue_retrieve ( )
integer x = 46
integer y = 396
integer width = 3607
integer height = 1204
integer taborder = 30
string dataobject = "d_det_fat_acq_ricerca_grezzo_fat_acq"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_retrieve();// modifica SQL della DW

dw_fat_acq.change_dw_focus(dw_fat_acq)
dw_fat_acq.setsqlselect(is_sql_fat_acq)

if wf_imposta_sql(ref dw_fat_acq, &
               "tes_fat_acq.cod_azienda", &
               "det_fat_acq.cod_prodotto", &
               "tes_fat_acq.data_registrazione", &
					dw_ricerca.getitemstring(dw_ricerca.getrow(),"cod_prodotto"), &
					dw_ricerca.getitemdatetime(dw_ricerca.getrow(),"data_inizio"), &
					dw_ricerca.getitemdatetime(dw_ricerca.getrow(),"data_fine")) = -1 then
	g_mb.messagebox("APICE","Errore nell'impostazione del SQL dinamico della DW; comunicare l'errore al servizio di assistenza",StopSign!)
	return
end if

// esegui retrieve
parent.postevent("pc_retrieve")

return
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event doubleclicked;call super::doubleclicked;if row > 0 then
	s_cs_xx.parametri.parametro_s_1 = getitemstring(getrow(),"det_fat_acq_cod_prodotto")
	s_cs_xx.parametri.parametro_s_2 = getitemstring(getrow(),"det_fat_acq_des_prodotto")
	if isnull(s_cs_xx.parametri.parametro_s_2) or len(s_cs_xx.parametri.parametro_s_2) < 1 then
		select des_prodotto
		into   :s_cs_xx.parametri.parametro_s_2
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :s_cs_xx.parametri.parametro_s_1;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore durante la ricerca della descrizione del prodotto in anagrafica prodotti.~r~n" + sqlca.sqlerrtext,stopsign!)
			s_cs_xx.parametri.parametro_s_1 = ""
			s_cs_xx.parametri.parametro_s_2 = ""
			return
		end if
	end if
	wf_calcola_netto_riga(getitemdecimal(getrow(),"det_fat_acq_prezzo_acquisto"), &
								 getitemdecimal(getrow(),"det_fat_acq_sconto_1"), &
								 getitemdecimal(getrow(),"det_fat_acq_sconto_2"), &
								 getitemdecimal(getrow(),"det_fat_acq_sconto_3"), &
								 getitemdecimal(getrow(),"det_fat_acq_sconto_4"), &
								 getitemdecimal(getrow(),"det_fat_acq_sconto_5"), &
								 getitemdecimal(getrow(),"det_fat_acq_sconto_6"), &
								 getitemdecimal(getrow(),"det_fat_acq_sconto_7"), &
								 getitemdecimal(getrow(),"det_fat_acq_sconto_8"), &
								 getitemdecimal(getrow(),"det_fat_acq_sconto_9"), &
								 getitemdecimal(getrow(),"det_fat_acq_sconto_10"), &
								 s_cs_xx.parametri.parametro_dec4_1)
	close(parent)
end if
end event

type dw_bol_acq from uo_cs_xx_dw within w_det_fat_acq_ricerca_grezzo
event ue_retrieve ( )
integer x = 46
integer y = 396
integer width = 3607
integer height = 1204
integer taborder = 50
string dataobject = "d_det_fat_acq_ricerca_grezzo_bol_acq"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_retrieve();// modifica SQL della DW

dw_bol_acq.change_dw_focus(dw_bol_acq)
dw_bol_acq.setsqlselect(is_sql_bol_acq)

if wf_imposta_sql(ref dw_bol_acq, &
               "tes_bol_acq.cod_azienda", &
               "det_bol_acq.cod_prodotto", &
               "tes_bol_acq.data_registrazione", &
					dw_ricerca.getitemstring(dw_ricerca.getrow(),"cod_prodotto"), &
					dw_ricerca.getitemdatetime(dw_ricerca.getrow(),"data_inizio"), &
					dw_ricerca.getitemdatetime(dw_ricerca.getrow(),"data_fine")) = -1 then
	g_mb.messagebox("APICE","Errore nell'impostazione del SQL dinamico della DW; comunicare l'errore al servizio di assistenza",StopSign!)
	return
end if

// esegui retrieve
parent.postevent("pc_retrieve")

return
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event doubleclicked;call super::doubleclicked;if row > 0 then
	s_cs_xx.parametri.parametro_s_1 = getitemstring(getrow(),"det_bol_acq_cod_prodotto")
	s_cs_xx.parametri.parametro_s_2 = getitemstring(getrow(),"det_bol_acq_des_prodotto")
	if isnull(s_cs_xx.parametri.parametro_s_2) or len(s_cs_xx.parametri.parametro_s_2) < 1 then
		select des_prodotto
		into   :s_cs_xx.parametri.parametro_s_2
		from   anag_prodotti
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_prodotto = :s_cs_xx.parametri.parametro_s_1;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore durante la ricerca della descrizione del prodotto in anagrafica prodotti.~r~n" + sqlca.sqlerrtext,stopsign!)
			s_cs_xx.parametri.parametro_s_1 = ""
			s_cs_xx.parametri.parametro_s_2 = ""
			return
		end if
	end if
	
	wf_calcola_netto_riga(getitemdecimal(getrow(),"det_bol_acq_prezzo_acquisto"), &
								 getitemdecimal(getrow(),"det_bol_acq_sconto_1"), &
								 getitemdecimal(getrow(),"det_bol_acq_sconto_2"), &
								 getitemdecimal(getrow(),"det_bol_acq_sconto_3"), &
								 getitemdecimal(getrow(),"det_bol_acq_sconto_4"), &
								 getitemdecimal(getrow(),"det_bol_acq_sconto_5"), &
								 getitemdecimal(getrow(),"det_bol_acq_sconto_6"), &
								 getitemdecimal(getrow(),"det_bol_acq_sconto_7"), &
								 getitemdecimal(getrow(),"det_bol_acq_sconto_8"), &
								 getitemdecimal(getrow(),"det_bol_acq_sconto_9"), &
								 getitemdecimal(getrow(),"det_bol_acq_sconto_10"), &
								 s_cs_xx.parametri.parametro_dec4_1)
	close(parent)
end if
end event

type dw_mov_mag from uo_cs_xx_dw within w_det_fat_acq_ricerca_grezzo
event ue_retrieve ( )
integer x = 46
integer y = 396
integer width = 3607
integer height = 1204
integer taborder = 30
string dataobject = "d_det_fat_acq_ricerca_grezzo_mov_mag"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event ue_retrieve();// modifica SQL della DW

dw_mov_mag.change_dw_focus(dw_mov_mag)
dw_mov_mag.setsqlselect(is_sql_mov_mag)

if wf_imposta_sql(ref dw_mov_mag, &
               "mov_magazzino.cod_azienda", &
               "mov_magazzino.cod_prodotto", &
               "mov_magazzino.data_registrazione", &
					dw_ricerca.getitemstring(dw_ricerca.getrow(),"cod_prodotto"), &
					dw_ricerca.getitemdatetime(dw_ricerca.getrow(),"data_inizio"), &
					dw_ricerca.getitemdatetime(dw_ricerca.getrow(),"data_fine")) = -1 then
	g_mb.messagebox("APICE","Errore nell'impostazione del SQL dinamico della DW; comunicare l'errore al servizio di assistenza",StopSign!)
	return
end if

// esegui retrieve
parent.postevent("pc_retrieve")

return
end event

event pcd_retrieve;call super::pcd_retrieve;LONG  l_Error

l_Error = Retrieve(s_cs_xx.cod_azienda)

IF l_Error < 0 THEN
   PCCA.Error = c_Fatal
END IF

end event

event doubleclicked;call super::doubleclicked;if row > 0 then
	s_cs_xx.parametri.parametro_s_1 = getitemstring(getrow(),"mov_magazzino_cod_prodotto")
	s_cs_xx.parametri.parametro_s_2 = getitemstring(getrow(),"anag_prodotti_des_prodotto")
	s_cs_xx.parametri.parametro_dec4_1 = getitemdecimal(getrow(),"val_movimento")
	close(parent)
end if
end event

