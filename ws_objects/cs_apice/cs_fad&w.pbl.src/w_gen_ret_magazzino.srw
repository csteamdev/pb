﻿$PBExportHeader$w_gen_ret_magazzino.srw
$PBExportComments$Finestra Generazione Rettifiche di Magazzino Da differenze Bolle/Fatture (Acquisto)
forward
global type w_gen_ret_magazzino from w_cs_xx_principale
end type
type cb_1 from uo_cb_close within w_gen_ret_magazzino
end type
type cb_genera_fat from commandbutton within w_gen_ret_magazzino
end type
type dw_lista_det_fat_acq from uo_cs_xx_dw within w_gen_ret_magazzino
end type
type dw_messaggi_errore from uo_cs_xx_dw within w_gen_ret_magazzino
end type
type dw_folder from u_folder within w_gen_ret_magazzino
end type
type ddlb_cod_tipo_movimento from dropdownlistbox within w_gen_ret_magazzino
end type
type st_cod_tipo_movimento from statictext within w_gen_ret_magazzino
end type
type st_2 from statictext within w_gen_ret_magazzino
end type
end forward

global type w_gen_ret_magazzino from w_cs_xx_principale
int Width=3563
int Height=1361
boolean TitleBar=true
string Title="Generazione Fatture Acquisto"
cb_1 cb_1
cb_genera_fat cb_genera_fat
dw_lista_det_fat_acq dw_lista_det_fat_acq
dw_messaggi_errore dw_messaggi_errore
dw_folder dw_folder
ddlb_cod_tipo_movimento ddlb_cod_tipo_movimento
st_cod_tipo_movimento st_cod_tipo_movimento
st_2 st_2
end type
global w_gen_ret_magazzino w_gen_ret_magazzino

type variables
string is_cod_fornitore, is_cod_doc_origine, is_numeratore_doc_origine
integer ii_anno_doc_origine
long il_num_doc_origine, il_prog_fattura
end variables

forward prototypes
public function integer wf_genera_ret_magazzino (long fl_selected[])
public subroutine wf_inserisci_errore (string fs_messaggio)
end prototypes

public function integer wf_genera_ret_magazzino (long fl_selected[]);string ls_cod_doc_origine, ls_numeratore_doc_origine, ls_num_bolla_acq, ls_messaggio, &
		 ls_cod_prodotto, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_cod_tipo_movimento
integer li_anno_doc_origine, li_anno_bolla_acq
long ll_num_doc_origine, ll_prog_fat_acq, ll_prog_riga_fat_acq, &
	  ll_prog_bolla_acq, ll_prog_riga_bolla_acq, ll_i, ll_prog_stock
double ld_quan_fatturata, ld_quan_arrivata, ld_imp_bolla, ld_imp_fattura, ld_quan_movimento
datetime ldt_data_stock

ls_cod_tipo_movimento = st_cod_tipo_movimento.text
if isnull(ls_cod_tipo_movimento) or ls_cod_tipo_movimento = "" then
	wf_inserisci_errore ("Errore: Codice Tipo Movimento non Valido (Fattura " + &
								ls_cod_doc_origine + "/" + ls_numeratore_doc_origine + "/" + &
								string(li_anno_doc_origine) + "/" + string(ll_num_doc_origine) + "/" + &
								string(ll_prog_fat_acq) + "/" + string(ll_prog_riga_fat_acq) + ")")
	return -1
end if

for ll_i = 1 to upperbound(fl_selected)
	ls_cod_doc_origine = dw_lista_det_fat_acq.getitemstring(fl_selected[ll_i], "cod_doc_origine")
	ls_numeratore_doc_origine = dw_lista_det_fat_acq.getitemstring(fl_selected[ll_i], "numeratore_doc_origine")
	li_anno_doc_origine = dw_lista_det_fat_acq.getitemnumber(fl_selected[ll_i], "anno_doc_origine")
	ll_num_doc_origine = dw_lista_det_fat_acq.getitemnumber(fl_selected[ll_i], "num_doc_origine")
	ll_prog_fat_acq = dw_lista_det_fat_acq.getitemnumber(fl_selected[ll_i], "progressivo")
	ll_prog_riga_fat_acq = dw_lista_det_fat_acq.getitemnumber(fl_selected[ll_i], "prog_riga_fat_acq")
	
	li_anno_bolla_acq = dw_lista_det_fat_acq.getitemnumber(fl_selected[ll_i], "det_bol_acq_anno_bolla_acq")
	ls_num_bolla_acq = dw_lista_det_fat_acq.getitemstring(fl_selected[ll_i], "det_bol_acq_num_bolla_acq")
	ll_prog_bolla_acq = dw_lista_det_fat_acq.getitemnumber(fl_selected[ll_i], "det_bol_acq_progressivo")
	ll_prog_riga_bolla_acq = dw_lista_det_fat_acq.getitemnumber(fl_selected[ll_i], "det_bol_acq_prog_riga_bolla_acq")

	ld_quan_fatturata = dw_lista_det_fat_acq.getitemnumber(fl_selected[ll_i], "quan_fatturata")
	ld_quan_arrivata = dw_lista_det_fat_acq.getitemnumber(fl_selected[ll_i], "det_bol_acq_quan_arrivata")
	
	ls_cod_prodotto = dw_lista_det_fat_acq.getitemstring(fl_selected[ll_i], "det_fat_acq_cod_prodotto")
	ls_cod_deposito = dw_lista_det_fat_acq.getitemstring(fl_selected[ll_i], "det_fat_acq_cod_deposito")
	ls_cod_ubicazione = dw_lista_det_fat_acq.getitemstring(fl_selected[ll_i], "det_fat_acq_cod_ubicazione")
	ls_cod_lotto = dw_lista_det_fat_acq.getitemstring(fl_selected[ll_i], "det_fat_acq_cod_lotto")
	ldt_data_stock = dw_lista_det_fat_acq.getitemdatetime(fl_selected[ll_i], "det_fat_acq_data_stock")
	ll_prog_stock = dw_lista_det_fat_acq.getitemnumber(fl_selected[ll_i], "det_fat_acq_prog_stock")
	
	ld_imp_fattura = f_valore_det_fat_acq(ls_cod_doc_origine, ls_numeratore_doc_origine, &
					li_anno_doc_origine,	ll_num_doc_origine, ll_prog_fat_acq, ll_prog_riga_fat_acq, &
					ref ls_messaggio)
					
	if ld_imp_fattura < 0 then
		wf_inserisci_errore ("Errore: " + ls_messaggio + " (Fattura " + &
									ls_cod_doc_origine + "/" + ls_numeratore_doc_origine + "/" + &
									string(li_anno_doc_origine) + "/" + string(ll_num_doc_origine) + "/" + &
									string(ll_prog_fat_acq) + "/" + string(ll_prog_riga_fat_acq) + ")")
		return -1
	end if

	ld_imp_bolla = f_valore_det_bol_acq(li_anno_bolla_acq, ls_num_bolla_acq, ll_prog_bolla_acq, &
					ll_prog_riga_bolla_acq, ref ls_messaggio)

	if ld_imp_Bolla < 0 then
		wf_inserisci_errore ("Errore: " + ls_messaggio + " (Fattura " + &
									ls_cod_doc_origine + "/" + ls_numeratore_doc_origine + "/" + &
									string(li_anno_doc_origine) + "/" + string(ll_num_doc_origine) + "/" + &
									string(ll_prog_fat_acq) + "/" + string(ll_prog_riga_fat_acq) + ")")
		return -1
	end if

	if ld_quan_fatturata <> ld_quan_arrivata then
		wf_inserisci_errore ("Informazione: Q.ta Fatturata diversa dalla Q.ta Arrivata (Fattura " + &
									ls_cod_doc_origine + "/" + ls_numeratore_doc_origine + "/" + &
									string(li_anno_doc_origine) + "/" + string(ll_num_doc_origine) + "/" + &
									string(ll_prog_fat_acq) + "/" + string(ll_prog_riga_fat_acq) + ")")
	end if

	ld_quan_movimento = abs(ld_quan_fatturata - ld_quan_arrivata)
	
	insert into ret_magazzino
			(cod_azienda,   
			cod_doc_origine,   
			numeratore_doc_origine,   
			anno_doc_origine,   
			num_doc_origine,   
			progressivo,   
			prog_riga_fat_acq,   
			imp_bolla,   
			imp_fattura,   
			cod_tipo_movimento,   
			cod_prodotto,   
			cod_deposito,   
			cod_ubicazione,   
			cod_lotto,   
			data_stock,   
			quan_movimento,   
			prog_stock)  
	values ( :s_cs_xx.cod_azienda,   
			:ls_cod_doc_origine,   
			:ls_numeratore_doc_origine,   
			:li_anno_doc_origine,   
			:ll_num_doc_origine,   
			:ll_prog_fat_acq,   
			:ll_prog_riga_fat_acq,   
			:ld_imp_bolla,   
			:ld_imp_fattura,   
			:ls_cod_tipo_movimento,   
			:ls_cod_prodotto,   
			:ls_cod_deposito,   
			:ls_cod_ubicazione,   
			:ls_cod_lotto,   
			:ldt_data_stock,   
			:ld_quan_movimento,   
			:ll_prog_stock);

	if sqlca.sqlcode = -1 then
		wf_inserisci_errore ("Errore: Inserimento Rettifica Fallito (Fattura " + &
									ls_cod_doc_origine + "/" + ls_numeratore_doc_origine + "/" + &
									string(li_anno_doc_origine) + "/" + string(ll_num_doc_origine) + "/" + &
									string(ll_prog_fat_acq) + "/" + string(ll_prog_riga_fat_acq) + ")")
		return -1
	else
		wf_inserisci_errore ("OK: Inserita Rettifica Magazzino (Fattura " + &
									ls_cod_doc_origine + "/" + ls_numeratore_doc_origine + "/" + &
									string(li_anno_doc_origine) + "/" + string(ll_num_doc_origine) + "/" + &
									string(ll_prog_fat_acq) + "/" + string(ll_prog_riga_fat_acq) + ")")
	end if
next
return 0
end function

public subroutine wf_inserisci_errore (string fs_messaggio);dw_messaggi_errore.insertrow(0)
dw_messaggi_errore.setrow(dw_messaggi_errore.rowcount())
dw_messaggi_errore.setitem(dw_messaggi_errore.getrow(), "messaggio_errore", fs_messaggio)

end subroutine

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]

set_w_options(c_noenablepopup)

lw_oggetti[1] = dw_lista_det_fat_acq
dw_folder.fu_assigntab(1, "Fatture / Bolle", lw_oggetti[])
lw_oggetti[1] = dw_messaggi_errore
dw_folder.fu_assigntab(2, "Messaggi di Generazione", lw_oggetti[])
dw_folder.fu_foldercreate(2, 4)
dw_folder.fu_selecttab(1)

dw_lista_det_fat_acq.set_dw_options(sqlca, &
                                  	  pcca.null_object, &
												  c_multiselect + &
												  c_nonew + &
												  c_nomodify + &
												  c_nodelete + &
												  c_disablecc + &
												  c_disableccinsert, &
												  c_default)

dw_messaggi_errore.set_dw_options(sqlca, &
                             	  pcca.null_object, &
										  c_nonew + &
										  c_nomodify + &
										  c_nodelete + &
										  c_disablecc + &
										  c_disableccinsert, &
	                             c_default)
//	                             c_viewmodeblack)
													 
save_on_close(c_socnosave)
end event

on w_gen_ret_magazzino.create
int iCurrent
call w_cs_xx_principale::create
this.cb_1=create cb_1
this.cb_genera_fat=create cb_genera_fat
this.dw_lista_det_fat_acq=create dw_lista_det_fat_acq
this.dw_messaggi_errore=create dw_messaggi_errore
this.dw_folder=create dw_folder
this.ddlb_cod_tipo_movimento=create ddlb_cod_tipo_movimento
this.st_cod_tipo_movimento=create st_cod_tipo_movimento
this.st_2=create st_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_1
this.Control[iCurrent+2]=cb_genera_fat
this.Control[iCurrent+3]=dw_lista_det_fat_acq
this.Control[iCurrent+4]=dw_messaggi_errore
this.Control[iCurrent+5]=dw_folder
this.Control[iCurrent+6]=ddlb_cod_tipo_movimento
this.Control[iCurrent+7]=st_cod_tipo_movimento
this.Control[iCurrent+8]=st_2
end on

on w_gen_ret_magazzino.destroy
call w_cs_xx_principale::destroy
destroy(this.cb_1)
destroy(this.cb_genera_fat)
destroy(this.dw_lista_det_fat_acq)
destroy(this.dw_messaggi_errore)
destroy(this.dw_folder)
destroy(this.ddlb_cod_tipo_movimento)
destroy(this.st_cod_tipo_movimento)
destroy(this.st_2)
end on

type cb_1 from uo_cb_close within w_gen_ret_magazzino
int X=3147
int Y=1153
int Width=363
int Height=81
int TabOrder=40
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_genera_fat from commandbutton within w_gen_ret_magazzino
event clicked pbm_bnclicked
int X=2756
int Y=1153
int Width=363
int Height=81
int TabOrder=50
boolean BringToTop=true
string Text="&Genera"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_selected[]
integer li_ret

dw_lista_det_fat_acq.get_selected_rows(ll_selected[])
if upperbound(ll_selected) > 0 then
	wf_genera_ret_magazzino(ll_selected)
	dw_folder.fu_selecttab(2)
	dw_lista_det_fat_acq.triggerevent("pcd_retrieve")
else
	g_mb.messagebox("Generazione Rettifiche Magazzino", "Nessuna Fattura da Rettificare", exclamation!)
end if

end event

type dw_lista_det_fat_acq from uo_cs_xx_dw within w_gen_ret_magazzino
event pcd_retrieve pbm_custom60
int X=36
int Y=113
int Width=3432
int Height=993
int TabOrder=30
string DataObject="d_lista_det_fat_acq"
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_fornitore
long ll_errore

if isnull(is_cod_fornitore) then
	ls_cod_fornitore = "%"
else
	ls_cod_fornitore = is_cod_fornitore
end if

ll_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_fornitore)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type dw_messaggi_errore from uo_cs_xx_dw within w_gen_ret_magazzino
event pcd_retrieve pbm_custom60
int X=36
int Y=113
int Width=2756
int Height=993
int TabOrder=10
string DataObject="d_messaggi_errore"
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_print;call super::pcd_print;long job

job = PrintOpen( ) 

PrintDataWindow(job, this) 
PrintClose(job)
end event

type dw_folder from u_folder within w_gen_ret_magazzino
int X=1
int Y=17
int Width=3503
int Height=1121
int TabOrder=20
boolean Border=false
BorderStyle BorderStyle=StyleBox!
end type

type ddlb_cod_tipo_movimento from dropdownlistbox within w_gen_ret_magazzino
event getfocus pbm_cbnsetfocus
event selectionchanged pbm_cbnselchange
int X=868
int Y=1156
int Width=1028
int Height=500
int TabOrder=41
boolean BringToTop=true
boolean VScrollBar=true
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event getfocus;ddlb_cod_tipo_movimento.reset()
f_po_loadddlb(ddlb_cod_tipo_movimento, &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'", &
                 "")
end event

event selectionchanged;st_cod_tipo_movimento.text=f_po_selectddlb(ddlb_cod_tipo_movimento)
end event

type st_cod_tipo_movimento from statictext within w_gen_ret_magazzino
int X=502
int Y=1156
int Width=342
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleRaised!
Alignment Alignment=Center!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_gen_ret_magazzino
int X=43
int Y=1156
int Width=456
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Tipo Movimento:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

