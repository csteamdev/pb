﻿$PBExportHeader$w_iva_fat_acq.srw
$PBExportComments$Finestra IVA Fatture Acquisto
forward
global type w_iva_fat_acq from w_cs_xx_principale
end type
type dw_iva_fat_acq from uo_cs_xx_dw within w_iva_fat_acq
end type
end forward

global type w_iva_fat_acq from w_cs_xx_principale
integer width = 3264
integer height = 1128
string title = "Iva Acquisti"
dw_iva_fat_acq dw_iva_fat_acq
end type
global w_iva_fat_acq w_iva_fat_acq

event pc_setwindow;call super::pc_setwindow;dw_iva_fat_acq.set_dw_options(sqlca, &
                               i_openparm, &
                               c_retrieveonopen + c_scrollparent, &
                               c_default)

iuo_dw_main = dw_iva_fat_acq
end event

on w_iva_fat_acq.create
int iCurrent
call super::create
this.dw_iva_fat_acq=create dw_iva_fat_acq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_iva_fat_acq
end on

on w_iva_fat_acq.destroy
call super::destroy
destroy(this.dw_iva_fat_acq)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_iva_fat_acq,"cod_iva",sqlca,&
                 "tab_ive","cod_iva","des_iva", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_iva_fat_acq from uo_cs_xx_dw within w_iva_fat_acq
integer x = 18
integer y = 20
integer width = 3200
integer height = 996
integer taborder = 20
string dataobject = "d_iva_fat_acq"
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long   l_errore, ll_anno_registrazione, ll_num_registrazione, ll_i

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	
for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if	
next
end event

event pcd_retrieve;call super::pcd_retrieve;long   l_errore, ll_anno_registrazione, ll_num_registrazione, ll_i

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	
l_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)

if l_Errore < 0 then
   pcca.error = c_Fatal
end if

end event

