﻿$PBExportHeader$w_report_fat_acq_cc.srw
$PBExportComments$Finestra Report Fattura di Acquisto cc
forward
global type w_report_fat_acq_cc from w_cs_xx_principale
end type
type dw_report from uo_cs_xx_dw within w_report_fat_acq_cc
end type
end forward

global type w_report_fat_acq_cc from w_cs_xx_principale
integer width = 4014
integer height = 2292
string title = "Report Fatture"
boolean minbox = false
event ue_close ( )
dw_report dw_report
end type
global w_report_fat_acq_cc w_report_fat_acq_cc

type variables
boolean ib_modifica=false, ib_nuovo=false, ib_nr_nr, ib_stampando = false
boolean ib_estero
long il_anno_registrazione, il_num_registrazione, il_anno[], il_num[], il_corrente = 0
long il_num_copie

// ------------- dichiarate per stampa da pagina a pagina -----------
boolean ib_stampa = false
long    il_totale_pagine, il_pagina_corrente
end variables

forward prototypes
public subroutine wf_leggi_iva (long fl_anno_registrazione, long fl_num_registrazione, ref decimal fd_aliquote_iva[], ref decimal fd_imponibile_iva_valuta[], ref decimal fd_imposta_iva_valuta[], ref string fs_des_esenzione[])
public subroutine wf_report ()
public subroutine wf_leggi_cc (long fl_anno_registrazione, long fl_num_registrazione, string fs_cod_conto, ref string fs_centro_costo[], ref decimal fd_percentuale[], ref decimal fd_importo[])
end prototypes

event ue_close();close(this)
end event

public subroutine wf_leggi_iva (long fl_anno_registrazione, long fl_num_registrazione, ref decimal fd_aliquote_iva[], ref decimal fd_imponibile_iva_valuta[], ref decimal fd_imposta_iva_valuta[], ref string fs_des_esenzione[]);string ls_cod_iva
long ll_i

declare cu_iva cursor for 
	select   iva_fat_ven.cod_iva, 
				iva_fat_ven.imponibile_iva, 
				iva_fat_ven.importo_iva,
				iva_fat_ven.perc_iva
	from     iva_fat_ven 
	where    iva_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				iva_fat_ven.anno_registrazione = :fl_anno_registrazione and 
				iva_fat_ven.num_registrazione = :fl_num_registrazione;

open cu_iva;

ll_i = 0
do while 0 = 0
	ll_i = ll_i + 1
   fetch cu_iva into :ls_cod_iva, 
							:fd_imponibile_iva_valuta[ll_i], 
							:fd_imposta_iva_valuta[ll_i],
							:fd_aliquote_iva[ll_i];

   if sqlca.sqlcode <> 0 then exit
	if fd_aliquote_iva[ll_i] = 0 then
		select tab_ive.des_iva
		into   :fs_des_esenzione[ll_i]
		from   tab_ive
		where  tab_ive.cod_azienda = :s_cs_xx.cod_azienda and
		       tab_ive.cod_iva = :ls_cod_iva;
	end if
loop

close cu_iva;

return

end subroutine

public subroutine wf_report ();long		ll_anno_doc_origine, ll_num_doc_origine, ll_progressivo, ll_numeratore_documento, ll_anno_documento, ll_num_documento, &
			ll_anno_nota, ll_num_nota, ll_riga, ll_cont, ll_num_reg_ord_acq, ll_prog_riga_ordine_acq
			
integer	li_anno_reg_ord_acq

string		ls_cod_doc_origine, ls_cod_tipo_fat_acq, ls_numeratore_doc_origine, ls_cod_documento, ls_cod_fornitore, ls_cod_operatore, &
			ls_cod_fil_fornitore, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca, ls_cod_banca_clien_for, &
			ls_cod_documento_nota, ls_numeratore_nota, ls_flag_fat_confermata, ls_des_num_fat_acq, ls_flag_st_note_tes, ls_flag_st_note_pie, &
			ls_protocollo, ls_flag_contabilita, ls_des_tipo_fat_acq, ls_flag_tipo_fat_acq, ls_des_pagamento, ls_flag_tipo_pagamento, &
			ls_des_banca, ls_cod_abi, ls_cod_cab, ls_des_valuta, ls_formato, ls_cod_tipo_det_acq, ls_cod_misura, ls_nota_dettaglio, ls_cod_prodotto, &
			ls_des_prodotto, ls_cod_iva, ls_flag_st_note_det, ls_nota_piede, ls_des_tipo_det_acq, ls_flag_tipo_det_acq, ls_des_prodotto_anag, &
			ls_des_iva, ls_des_fornitore, ls_cod_conto, ls_flag_dare_avere, ls_cod_cc, ls_rag_soc_1_azienda, ls_appo, ls_indirizzo, ls_cap, &
			ls_localita, ls_provincia, ls_partita_iva, ls_cod_fiscale, ls_des_specifica, ls_cod_prod_fornitore, ls_des_aggiuntiva

datetime ldt_data_doc_origine, ldt_data_registrazione, ldt_data_protocollo

dec{4}   ld_cambio_acq, ld_tot_val_fat_acq, ld_tot_valuta_fa_acq, ld_importo_iva, ld_imponibile_iva, ld_importo_valuta_iva, &
			ld_imponibile_valuta_iva, ld_importo_iva_indetraibile, ld_sconto, ld_tot_merci, ld_tot_spese_trasporto, ld_tot_spese_imballo, &
			ld_tot_spese_bolli, ld_tot_spese_varie, ld_tot_spese_cassa, ld_tot_sconti_commerciali, ld_imponibile_provvigioni_1, &
			ld_imponibile_provvigioni_2, ld_tot_provvigioni_1, ld_tot_provvigioni_2, ld_sconto_pagamento, ld_quan_fatturata, ld_prezzo_vendita, &
			ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, &
			ld_sconto_tot, ld_imponibile_riga, ld_perc_iva, ld_importo_riga, ld_importo_valuta, ld_percentuale_cc, ld_importo_cc, ld_totale, ld_totale_valuta

boolean  lb_prima_riga, lb_flag_nota_dettaglio, lb_flag_nota_piede

dec{5}   ld_fat_conversione_acq

dw_report.reset()

dw_report.Modify("dw_1.Nest_Arguments=((~"'" + s_cs_xx.cod_azienda + "'~"),(~"" + string(il_anno_registrazione) + "~"),(~"" + string(il_num_registrazione) + "~"))")

SELECT aziende.rag_soc_1  
INTO   :ls_rag_soc_1_azienda  
FROM   aziende  
where  cod_azienda = :s_cs_xx.cod_azienda;

dw_report.Modify("t_rag_soc_azienda.text='"+ ls_rag_soc_1_azienda + "'")

SELECT tes_fat_acq.numeratore_doc_origine,   
       tes_fat_acq.anno_doc_origine,   
       tes_fat_acq.num_doc_origine,   
       tes_fat_acq.progressivo,   
       tes_fat_acq.cod_doc_origine,   
       tes_fat_acq.cod_tipo_fat_acq,   
       tes_fat_acq.data_doc_origine,   
       tes_fat_acq.cod_documento,   
       tes_fat_acq.numeratore_documento,   
       tes_fat_acq.anno_documento,   
       tes_fat_acq.num_documento,   
       tes_fat_acq.cod_fornitore,   
       tes_fat_acq.data_registrazione,   
       tes_fat_acq.cod_operatore,   
       tes_fat_acq.cod_fil_fornitore,   
       tes_fat_acq.cod_valuta,   
       tes_fat_acq.cambio_acq,   
       tes_fat_acq.cod_tipo_listino_prodotto,   
       tes_fat_acq.cod_pagamento,   
       tes_fat_acq.cod_banca,   
       tes_fat_acq.cod_banca_clien_for,   
       tes_fat_acq.cod_documento_nota,   
       tes_fat_acq.numeratore_nota,   
       tes_fat_acq.anno_nota,   
       tes_fat_acq.num_nota,   
       tes_fat_acq.tot_val_fat_acq,   
       tes_fat_acq.tot_valuta_fat_acq,   
       tes_fat_acq.importo_iva,   
       tes_fat_acq.imponibile_iva,   
       tes_fat_acq.importo_valuta_iva,   
       tes_fat_acq.imponibile_valuta_iva,   
       tes_fat_acq.importo_iva_indetraibile,   
       tes_fat_acq.flag_fat_confermata,   
       tes_fat_acq.sconto,   
       tes_fat_acq.des_num_fat_acq,   
       tes_fat_acq.flag_st_note_tes,   
       tes_fat_acq.flag_st_note_pie,   
       tes_fat_acq.tot_merci,   
       tes_fat_acq.tot_spese_trasporto,   
       tes_fat_acq.tot_spese_imballo,   
       tes_fat_acq.tot_spese_bolli,   
       tes_fat_acq.tot_spese_varie,   
       tes_fat_acq.tot_spese_cassa,   
       tes_fat_acq.tot_sconti_commerciali,   
       tes_fat_acq.imponibile_provvigioni_1,   
       tes_fat_acq.imponibile_provvigioni_2,   
       tes_fat_acq.tot_provvigioni_1,   
       tes_fat_acq.tot_provvigioni_2,   
       tes_fat_acq.protocollo,   
       tes_fat_acq.data_protocollo,   
       tes_fat_acq.flag_contabilita  
INTO   :ls_numeratore_doc_origine,   
       :ll_anno_doc_origine,   
       :ll_num_doc_origine,   
       :ll_progressivo,   
       :ls_cod_doc_origine,   
       :ls_cod_tipo_fat_acq,   
       :ldt_data_doc_origine,   
       :ls_cod_documento,   
       :ll_numeratore_documento,   
       :ll_anno_documento,   
       :ll_num_documento,   
       :ls_cod_fornitore,   
       :ldt_data_registrazione,   
       :ls_cod_operatore,   
       :ls_cod_fil_fornitore,   
       :ls_cod_valuta,   
       :ld_cambio_acq,   
       :ls_cod_tipo_listino_prodotto,   
       :ls_cod_pagamento,   
       :ls_cod_banca,   
       :ls_cod_banca_clien_for,   
       :ls_cod_documento_nota,   
       :ls_numeratore_nota,   
       :ll_anno_nota,   
       :ll_num_nota,   
       :ld_tot_val_fat_acq,   
       :ld_tot_valuta_fa_acq,   
       :ld_importo_iva,   
       :ld_imponibile_iva,   
       :ld_importo_valuta_iva,   
       :ld_imponibile_valuta_iva,   
       :ld_importo_iva_indetraibile,   
       :ls_flag_fat_confermata,   
       :ld_sconto,   
       :ls_des_num_fat_acq,   
       :ls_flag_st_note_tes,   
       :ls_flag_st_note_pie,   
       :ld_tot_merci,   
       :ld_tot_spese_trasporto,   
       :ld_tot_spese_imballo,   
       :ld_tot_spese_bolli,   
       :ld_tot_spese_varie,   
       :ld_tot_spese_cassa,   
       :ld_tot_sconti_commerciali,   
       :ld_imponibile_provvigioni_1,   
       :ld_imponibile_provvigioni_2,   
       :ld_tot_provvigioni_1,   
       :ld_tot_provvigioni_2,   
       :ls_protocollo,   
       :ldt_data_protocollo,   
       :ls_flag_contabilita  
FROM   tes_fat_acq  
where  tes_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_fat_acq.anno_registrazione = :il_anno_registrazione and 
		 tes_fat_acq.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in ricerca testata fattura: " + sqlca.sqlerrtext, Stopsign!)
	return
end if

if sqlca.sqlcode = 100 then
	g_mb.messagebox("APICE","Errore in ricerca testata fattura: Fattura non trovata.", Stopsign!)
	return
end if

select des_tipi_fat_acq,
       flag_tipo_fat_acq
into   :ls_des_tipo_fat_acq,
       :ls_flag_tipo_fat_acq
from   tab_tipi_fat_acq  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_fat_acq = :ls_cod_tipo_fat_acq;
		 
if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_fat_acq)
end if

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca flag_tipo_fat_acq in tabella tab_tipi_fat_acq: " + sqlca.sqlerrtext, Stopsign!)
	return
end if
	
select des_pagamento,   
       sconto,
       flag_tipo_pagamento
into   :ls_des_pagamento,   
       :ld_sconto_pagamento,
       :ls_flag_tipo_pagamento
from   tab_pagamenti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
   ls_flag_tipo_pagamento = "D"
end if

if not isnull(ls_cod_banca) then
	
	select des_banca,
	       cod_abi,
			 cod_cab
	into   :ls_des_banca,
			 :ls_cod_abi,
			 :ls_cod_cab
	from   anag_banche
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_banca = :ls_cod_banca;

elseif not isnull(ls_cod_banca_clien_for) then
	
	select des_banca,
			 cod_abi,
			 cod_cab
	into   :ls_des_banca,
			 :ls_cod_abi,
			 :ls_cod_cab
	from   anag_banche_clien_for  
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_banca_clien_for = :ls_cod_banca_clien_for;
			 
end if

if sqlca.sqlcode <> 0 then
	ls_des_banca = ""
	ls_cod_abi = ""
	ls_cod_cab = ""
end if

select des_valuta,
		 formato
into   :ls_des_valuta,
		 :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta)
end if


select rag_soc_1,
       indirizzo,
		 cap,
		 localita,
		 provincia,
		 partita_iva,
		 cod_fiscale
into   :ls_des_fornitore,
       :ls_indirizzo,
		 :ls_cap,
		 :ls_localita,
		 :ls_provincia,
		 :ls_partita_iva,
		 :ls_cod_fiscale
from   anag_fornitori
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_fornitore = :ls_cod_fornitore;
			 
if isnull(ls_cod_fornitore) then ls_cod_fornitore = ""
	
if isnull(ls_des_fornitore) then ls_des_fornitore = ""
	
ls_des_fornitore = ls_cod_fornitore + " " + ls_des_fornitore

if isnull(ls_cap) then ls_cap = ""

if isnull(ls_localita) then ls_localita = ""

if isnull(ls_provincia) then ls_provincia = ""

ls_localita = ls_cap + " " + ls_localita + " " + "(" + ls_provincia + ")"

declare cu_dettagli cursor for 
	select   cod_tipo_det_acq, 
				cod_misura, 
				quan_fatturata, 
				prezzo_acquisto, 
				sconto_1, 
				sconto_2, 
				sconto_3, 
				sconto_4, 
				sconto_5, 
				sconto_6, 
				sconto_7, 
				sconto_8, 
				sconto_9, 
				sconto_10, 
				nota_dettaglio, 
				cod_prodotto, 
				des_prodotto,
				fat_conversione,
				cod_iva,
				flag_st_note_det,
				anno_reg_ord_acq,
				num_reg_ord_acq,
				prog_riga_ordine_acq
	from     det_fat_acq 
	where    cod_azienda = :s_cs_xx.cod_azienda and 
				anno_registrazione = :il_anno_registrazione and 
				num_registrazione = :il_num_registrazione
	order by cod_azienda, 
				anno_registrazione, 
				num_registrazione,
				prog_riga_fat_acq;

open cu_dettagli;

lb_prima_riga = false
lb_flag_nota_dettaglio = false
lb_flag_nota_piede = false

ld_totale = 0

ld_totale_valuta = 0

do while 0 = 0
	
	if not(lb_prima_riga) and not(lb_flag_nota_dettaglio) then
		
		fetch cu_dettagli into	:ls_cod_tipo_det_acq, 
									:ls_cod_misura, 
									:ld_quan_fatturata, 
									:ld_prezzo_vendita,   
									:ld_sconto_1, 
									:ld_sconto_2, 
									:ld_sconto_3, 
									:ld_sconto_4, 
									:ld_sconto_5, 
									:ld_sconto_6, 
									:ld_sconto_7, 
									:ld_sconto_8, 
									:ld_sconto_9, 
									:ld_sconto_10, 
									:ls_nota_dettaglio, 
									:ls_cod_prodotto, 
									:ls_des_prodotto,
									:ld_fat_conversione_acq,
									:ls_cod_iva,
									:ls_flag_st_note_det,
									:li_anno_reg_ord_acq,
									:ll_num_reg_ord_acq,
									:ll_prog_riga_ordine_acq;									  
	
		if sqlca.sqlcode <> 0 then
			if not isnull(ls_nota_piede) and len(trim(ls_nota_piede)) > 0 then
				lb_flag_nota_piede = true
			else
				exit
			end if
		end if
	end if
	
	if ls_flag_st_note_det = 'I' then //nota dettaglio
	
		select flag_st_note_ft
	   into   :ls_flag_st_note_det
		from   tab_tipi_det_acq
		where  cod_azienda = :s_cs_xx.cod_azienda and 
		       cod_tipo_det_acq = :ls_cod_tipo_det_acq;
	end if		
	
	if ls_flag_st_note_det = 'N' then
		ls_nota_dettaglio = ""
	end if				
	
	ld_sconto_tot = 0
   if ld_sconto_1 <> 0 and not isnull(ld_sconto_1) then ld_sconto_tot = 1 *             ( 1 - (ld_sconto_1 / 100))
   if ld_sconto_2 <> 0 and not isnull(ld_sconto_2) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_2 / 100))
   if ld_sconto_3 <> 0 and not isnull(ld_sconto_3) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_3 / 100))
   if ld_sconto_4 <> 0 and not isnull(ld_sconto_4) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_4 / 100))
   if ld_sconto_5 <> 0 and not isnull(ld_sconto_5) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_5 / 100))
   if ld_sconto_6 <> 0 and not isnull(ld_sconto_6) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_6 / 100))
   if ld_sconto_7 <> 0 and not isnull(ld_sconto_7) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_7 / 100))
   if ld_sconto_8 <> 0 and not isnull(ld_sconto_8) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_8 / 100))
   if ld_sconto_9 <> 0 and not isnull(ld_sconto_9) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_9 / 100))
   if ld_sconto_10<> 0 and not isnull(ld_sconto_10) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_10 / 100))

	dw_report.insertrow(0)
	
	dw_report.setrow(dw_report.rowcount())

	select des_tipo_det_acq,
			 flag_tipo_det_acq
	into   :ls_des_tipo_det_acq,
			 :ls_flag_tipo_det_acq
	from   tab_tipi_det_acq
	where  tab_tipi_det_acq.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_det_acq.cod_tipo_det_acq = :ls_cod_tipo_det_acq;

	if sqlca.sqlcode <> 0 then
		setnull(ls_des_tipo_det_acq)
	end if

	ld_imponibile_riga = (ld_quan_fatturata * ld_fat_conversione_acq) * (ld_prezzo_vendita / ld_fat_conversione_acq)

	if ld_sconto_tot <> 0 then
		ld_sconto_tot = (1 - ld_sconto_tot) * 100
		ld_imponibile_riga = ld_imponibile_riga - ( (ld_imponibile_riga/100) * ld_sconto_tot)
	end if
		
	select anag_prodotti.des_prodotto  
	into   :ls_des_prodotto_anag  
	from   anag_prodotti  
	where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
			 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
	if sqlca.sqlcode <> 0 then
		setnull(ls_des_prodotto_anag)
	end if
	
			
	select tab_ive.aliquota
	into   :ld_perc_iva
	from   tab_ive
	where  tab_ive.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_ive.cod_iva = :ls_cod_iva;

	if sqlca.sqlcode <> 0 then
		setnull(ls_des_iva)
	else
		ls_des_iva = string(ld_perc_iva,"###")
	end if

	dw_report.setitem( dw_report.getrow(), "fornitore", ls_des_fornitore)
	dw_report.setitem( dw_report.getrow(), "for_indirizzo", ls_indirizzo)
	dw_report.setitem( dw_report.getrow(), "for_localita", ls_localita)
	dw_report.setitem( dw_report.getrow(), "partita_iva", ls_partita_iva)
	dw_report.setitem( dw_report.getrow(), "cod_fiscale", ls_cod_fiscale)
	dw_report.setitem( dw_report.getrow(), "um", ls_cod_misura)
	dw_report.setitem( dw_report.getrow(), "quantita", ld_quan_fatturata * ld_fat_conversione_acq)
	dw_report.setitem( dw_report.getrow(), "prezzo", ld_prezzo_vendita / ld_fat_conversione_acq)
	dw_report.setitem( dw_report.getrow(), "sconti", ld_sconto_tot)
	
	
	ls_des_aggiuntiva = ""
	if li_anno_reg_ord_acq>0 and ll_num_reg_ord_acq>0 and ll_prog_riga_ordine_acq>0 then
		//leggo des_specifica e prodotto del fornitore
		select des_specifica, cod_prod_fornitore
		into :ls_des_specifica, :ls_cod_prod_fornitore
		from det_ord_acq
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:li_anno_reg_ord_acq and
					num_registrazione=:ll_num_reg_ord_acq and
					prog_riga_ordine_acq=:ll_prog_riga_ordine_acq;
		
		if ls_des_specifica<>"" and not isnull(ls_des_specifica) then
			ls_des_aggiuntiva +="Des.Specifica: "+ls_des_specifica
		end if
		
		if ls_cod_prod_fornitore<>"" and not isnull(ls_cod_prod_fornitore) then
			if ls_des_aggiuntiva<>"" then ls_des_aggiuntiva += "~r~n"
			ls_des_aggiuntiva +="Prod.Fornitore: "+ls_cod_prod_fornitore
		end if
	end if
	
	
	if ls_flag_tipo_det_acq <> "S" then  		// se non è uno sconto
		dw_report.setitem( dw_report.getrow(), "val_riga", ld_imponibile_riga)
		ld_totale = ld_totale + ld_imponibile_riga
	else
		dw_report.setitem( dw_report.getrow(), "val_riga", ld_imponibile_riga * -1)
		ld_totale = ld_totale + (ld_imponibile_riga * -1)		
	end if
	
	dw_report.setitem( dw_report.getrow(), "iva", ld_perc_iva)
	dw_report.setitem( dw_report.getrow(), "cod_prodotto", ls_cod_prodotto)

	if not isnull(ls_des_prodotto) and ls_des_prodotto<>"" then
		if ls_des_aggiuntiva<>"" then ls_des_prodotto += "~r~n" + ls_des_aggiuntiva
		dw_report.setitem( dw_report.getrow(), "des_prodotto", ls_des_prodotto)
		
	elseif not isnull(ls_des_prodotto_anag) and ls_des_prodotto_anag<>"" then
		if ls_des_aggiuntiva<>"" then ls_des_prodotto_anag += "~r~n" + ls_des_aggiuntiva
		dw_report.setitem( dw_report.getrow(), "des_prodotto", ls_des_prodotto_anag)
	else		
		ls_des_prodotto_anag = ls_des_tipo_det_acq
		dw_report.setitem( dw_report.getrow(), "des_prodotto", ls_des_prodotto_anag)
	end if
			
	if isnull(ls_cod_documento) then
		
		dw_report.setitem( dw_report.getrow(), "tipo_documento", ls_cod_documento)
		
		ls_appo = ls_cod_doc_origine + ' / ' +  ls_numeratore_doc_origine + ' / ' +  string(ll_anno_doc_origine)  + ' / ' +  string(ll_num_doc_origine)
		
		dw_report.setitem( dw_report.getrow(), "numero", ls_appo)
		
		dw_report.setitem( dw_report.getrow(), "data_fattura", ldt_data_doc_origine)
		
	end if
	
	dw_report.setitem( dw_report.getrow(), "protocollo", ls_protocollo)
	
	dw_report.setitem( dw_report.getrow(), "data_protocollo", ldt_data_protocollo)
	
	dw_report.setitem( dw_report.getrow(), "cambio", ld_cambio_acq)
	
	dw_report.setitem( dw_report.getrow(), "tipo_documento", ls_des_tipo_fat_acq)
	
	dw_report.setitem( dw_report.getrow(), "pagamento", ls_des_pagamento)
	
	dw_report.setitem( dw_report.getrow(), "sconto", ld_sconto_pagamento)

	dw_report.setitem( dw_report.getrow(), "banca", ls_des_banca)

	dw_report.setitem( dw_report.getrow(), "abi", ls_cod_abi)
	
	dw_report.setitem( dw_report.getrow(), "cab", ls_cod_cab)

	dw_report.setitem( dw_report.getrow(), "valuta", ls_des_valuta)
	
	dw_report.setitem( dw_report.getrow(), "tipo_riga", 1)
	
	dw_report.setitem( dw_report.getrow(), "cod_azienda", s_cs_xx.cod_azienda)
	
	dw_report.setitem( dw_report.getrow(), "ianno", il_anno_registrazione)
	
	dw_report.setitem( dw_report.getrow(), "inumero", il_num_registrazione)	
	
	dw_report.setitem( dw_report.getrow(), "totale_iva", ld_importo_iva)
	
	dw_report.setitem( dw_report.getrow(), "totale_imponibile", ld_imponibile_iva)
	
	dw_report.setitem( dw_report.getrow(), "totale", ld_tot_val_fat_acq)		
	
	ld_totale_valuta = ld_totale * ld_cambio_acq
	
	dw_report.setitem( dw_report.getrow(), "totale_valuta", ld_tot_valuta_fa_acq)			
		
	dw_report.setitem( dw_report.getrow(), "rag_soc_1", ls_rag_soc_1_azienda)
	
loop




close cu_dettagli;

dw_report.reset_dw_modified(c_resetchildren)

dw_report.change_dw_current()
end subroutine

public subroutine wf_leggi_cc (long fl_anno_registrazione, long fl_num_registrazione, string fs_cod_conto, ref string fs_centro_costo[], ref decimal fd_percentuale[], ref decimal fd_importo[]);long ll_count

DECLARE cu_cc CURSOR FOR  
	SELECT cont_fat_acq_cc.cod_centro_costo,   
          cont_fat_acq_cc.percentuale,   
          cont_fat_acq_cc.importo  
   FROM   cont_fat_acq_cc  
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione and
			 cod_conto = :fs_cod_conto;
					 
open cu_cc;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "APICE", "Errore durante l'apertura del cursore dei centri di costo: " + sqlca.sqlerrtext )
	close cu_cc;
	return
end if	

ll_count = 1
		
do while 1 = 1
		
	fetch cu_cc into :fs_centro_costo[ll_count],
						  :fd_percentuale[ll_count],
						  :fd_importo[ll_count];
							  
	if sqlca.sqlcode = 100 then exit
			
	if sqlca.sqlcode < 0 then
		g_mb.messagebox( "APICE", "Errore durante la fetch del cursore dei centri di costo: " + sqlca.sqlerrtext )
		close cu_cc;				
		return
	end if	
			
	ll_count = ll_count + 1
			
loop
		
close cu_cc;

if sqlca.sqlcode < 0 then
	g_mb.messagebox( "APICE", "Errore durante la chiusura del cursore dei centri di costo: " + sqlca.sqlerrtext )
	close cu_cc;
	return
end if	

return

end subroutine

event pc_setwindow;call super::pc_setwindow;string ls_path_logo_1, ls_modify
long   ll_i, ll_anno, ll_num

dw_report.ib_dw_report = true

set_w_options(c_noresizewin)

save_on_close(c_socnosave)

dw_report.set_dw_options(sqlca, &
								 pcca.null_object, &
								 c_nonew + &
								 c_nomodify + &
								 c_nodelete + &
								 c_noenablenewonopen + &
								 c_noenablemodifyonopen + &
								 c_scrollparent + &
								 c_disablecc, &
								 c_noresizedw + &
								 c_nohighlightselected + &
								 c_nocursorrowfocusrect + &
								 c_nocursorrowpointer)

il_anno_registrazione = s_cs_xx.parametri.parametro_d_1
il_num_registrazione = s_cs_xx.parametri.parametro_d_2
	
wf_report()

select parametri_azienda.stringa
into   :ls_path_logo_1
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
       parametri_azienda.flag_parametro = 'S' and &
       parametri_azienda.cod_parametro = 'LO3';

ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
dw_report.modify(ls_modify)
dw_report.set_document_name("Offerta Acquisto " + string(il_anno_registrazione) + "/" + string(il_num_registrazione))

	
// **************  metto la DW in anteprima  **********************
dw_report.object.datawindow.print.preview = 'Yes'
dw_report.object.datawindow.print.preview.rulers = 'Yes'


end event

on w_report_fat_acq_cc.create
int iCurrent
call super::create
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_report
end on

on w_report_fat_acq_cc.destroy
call super::destroy
destroy(this.dw_report)
end on

type dw_report from uo_cs_xx_dw within w_report_fat_acq_cc
integer x = 23
integer y = 20
integer width = 3931
integer height = 2160
integer taborder = 20
string dataobject = "d_report_fat_acq_cc"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

