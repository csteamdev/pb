﻿$PBExportHeader$w_report_fat_acq_euro.srw
$PBExportComments$Finestra Report Fatture Acquisto
forward
global type w_report_fat_acq_euro from w_cs_xx_principale
end type
type dw_selezione from uo_cs_xx_dw within w_report_fat_acq_euro
end type
type cb_annulla from commandbutton within w_report_fat_acq_euro
end type
type cb_report from commandbutton within w_report_fat_acq_euro
end type
type cb_selezione from commandbutton within w_report_fat_acq_euro
end type
type dw_report from uo_cs_xx_dw within w_report_fat_acq_euro
end type
type st_1 from statictext within w_report_fat_acq_euro
end type
end forward

global type w_report_fat_acq_euro from w_cs_xx_principale
integer width = 3895
integer height = 1940
string title = "Stampa Fatture di Acquisto"
dw_selezione dw_selezione
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_report dw_report
st_1 st_1
end type
global w_report_fat_acq_euro w_report_fat_acq_euro

on w_report_fat_acq_euro.create
int iCurrent
call super::create
this.dw_selezione=create dw_selezione
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_report=create dw_report
this.st_1=create st_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_selezione
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.cb_selezione
this.Control[iCurrent+5]=this.dw_report
this.Control[iCurrent+6]=this.st_1
end on

on w_report_fat_acq_euro.destroy
call super::destroy
destroy(this.dw_selezione)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_report)
destroy(this.st_1)
end on

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 677
this.y = 589
this.width = 2700
this.height = 800
cb_selezione.hide()
end event

event getfocus;call super::getfocus;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_fornitore_a"
end event

event resize;call super::resize;if cb_selezione.visible=true then
	dw_report.x=23
	dw_report.y=20
	dw_report.width = newwidth - 50
	dw_report.height = newheight - 150
	
	cb_selezione.x = newwidth - 400
	cb_selezione.y = newheight - 100
end if
end event

type dw_selezione from uo_cs_xx_dw within w_report_fat_acq_euro
integer x = 23
integer y = 20
integer width = 2651
integer height = 520
integer taborder = 50
string dataobject = "r_report_fat_acq_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore_da"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore_da")
	case "b_ricerca_fornitore_a"
		guo_ricerca.uof_ricerca_fornitore(dw_selezione,"cod_fornitore_a")
end choose
end event

type cb_annulla from commandbutton within w_report_fat_acq_euro
integer x = 1874
integer y = 540
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;dw_selezione.reset()
dw_selezione.insertrow(0)
end event

type cb_report from commandbutton within w_report_fat_acq_euro
integer x = 2263
integer y = 540
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;string 	ls_cod_fornitore_da, ls_cod_fornitore_a, ls_protocollo_da, ls_protocollo_a, ls_cod_fornitore, &
		 	ls_cod_pagamento, ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1, ls_cod_iva, ls_nota_dettaglio,&
			ls_messaggio, ls_cod_valuta, ls_formato, ls_cod_tipo_det_acq, ls_flag_tipo_det_acq, ls_sql, ls_des_valuta,&
			ls_cod_doc_origine, ls_numeratore_doc_origine, ls_protocollo, ls_rag_soc_fornitore, ls_localita, ls_stato, ls_des_nazione
		 
long 	 	ll_numero_da, ll_numero_a, ll_anno_documento, ll_num_documento, ll_ret, ll_row, ll_anno_doc_origine, ll_num_doc_origine, &
	  	 	ll_anno_registrazione, ll_num_registrazione, ll_i, ll_aliquota, ll_prog_riga, ll_cont, ll_anno_reg_uc, ll_num_reg_uc
	  
date 	 	ldt_data_reg_da, ldt_data_reg_a, ldt_data_fattura_da, ldt_data_fattura_a

datetime ldt_data_registrazione, ldt_data_doc_origine, ldt_data_protocollo

double 	ld_prezzo_acquisto, ld_sconto_1, ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_prezzo_acq_netto, &
		 	ld_quan_fatturata, ld_num_confezioni, ld_num_pezzi_confezione, ld_quan_sconto_merce, ld_sconto_pag, &
		 	ld_tot_val_fat_acq, ld_importo_iva, ld_imponibile_iva, ld_quan_fatturata_p, &
			ld_prezzo_acquisto_p, ld_sconti[], ld_sconto_calcolato, ld_prezzo_scontato, ld_valore_totale, &
		 	ld_tot_riga, ld_quan_fatt_tot, ld_imponibile_iva_riga


datastore lds_data, lds_ultima_condizione

dw_report.reset()

dw_selezione.accepttext()
ll_i = 1

ll_numero_da = dw_selezione.getitemnumber(1,"num_documento_da")
ll_numero_a = dw_selezione.getitemnumber(1,"num_documento_a")
ls_cod_fornitore_da = dw_selezione.getitemstring(1,"cod_fornitore_da")
ls_cod_fornitore_a = dw_selezione.getitemstring(1,"cod_fornitore_a")
ls_protocollo_da = dw_selezione.getitemstring(1,"protocollo_da")
ls_protocollo_a = dw_selezione.getitemstring(1,"protocollo_a")
ldt_data_reg_da = date(dw_selezione.getitemdatetime(1,"data_reg_da"))
ldt_data_reg_a = date(dw_selezione.getitemdatetime(1,"data_reg_a"))
ldt_data_fattura_da = date(dw_selezione.getitemdatetime(1,"data_fattura_da"))
ldt_data_fattura_a = date(dw_selezione.getitemdatetime(1,"data_fattura_a"))

ls_sql = " select T.anno_registrazione, T.num_registrazione,T. anno_documento,T.num_documento,T.data_registrazione,T.cod_fornitore,T.cod_pagamento,T.tot_val_fat_acq,T.importo_iva, " +&
		" T.imponibile_iva, T.cod_valuta,D.prog_riga_fat_acq, D.cod_prodotto, D.des_prodotto, D.prezzo_acquisto, D.sconto_1, D.sconto_2, D.sconto_3, D.sconto_4, D.sconto_5, " + &
		" D.quan_fatturata, D.num_confezioni, D.num_pezzi_confezione, D.quan_sconto_merce, D.prezzo_acq_netto, D.cod_iva, D.nota_dettaglio, D.cod_tipo_det_acq, V.des_valuta, V.formato,  " +&
		" A.rag_soc_1,T.data_doc_origine, TIP.flag_tipo_det_acq, D.imponibile_iva, T.cod_doc_origine, T.anno_doc_origine, T.numeratore_doc_origine, T.num_doc_origine, T.protocollo, T.data_protocollo " + &
		" from tes_fat_acq T " + &
		" left join det_fat_acq D on T.cod_azienda = D.cod_azienda and T.anno_registrazione=D.anno_registrazione and T.num_Registrazione= D.num_registrazione " +&
		" left join tab_valute V on T.cod_azienda = V.cod_azienda and T.cod_valuta=V.cod_valuta " +&
		" left join aziende A on T.cod_azienda=A.cod_azienda " +&
		" left join tab_tipi_det_acq TIP on D.cod_azienda=TIP.cod_azienda and D.cod_tipo_det_acq=TIP.cod_tipo_det_acq " + &
		g_str.format(" where T.cod_azienda = '$1'",s_cs_xx.cod_azienda)

if not isnull(ll_numero_da) and ll_numero_da > 0 then  ls_sql += g_str.format(" and T.num_registrazione >= $1 ", ll_numero_da)

if not isnull(ll_numero_a) and ll_numero_a > 0 then   ls_sql += g_str.format(" and T.num_registrazione <= $1 ", ll_numero_a)

if not isnull(ls_cod_fornitore_da) then  ls_sql += g_str.format(" and T.cod_fornitore >= '$1' ", ls_cod_fornitore_da)

if not isnull(ls_cod_fornitore_a) then  ls_sql += g_str.format(" and T.cod_fornitore <= '$1' ", ls_cod_fornitore_a)

if not isnull(ldt_data_reg_da) then  ls_sql += g_str.format(" and T.data_registrazione >= '$1' ", ldt_data_reg_da)

if not isnull(ldt_data_reg_a) then  ls_sql += g_str.format(" and T.data_registrazione <= '$1' ", ldt_data_reg_a)

if not isnull(ldt_data_fattura_da) then  ls_sql += g_str.format(" and T.data_doc_origine >= '$1' ", ldt_data_fattura_da)

if not isnull(ldt_data_fattura_a) then  ls_sql += g_str.format(" and T.data_doc_origine <= '$1' ", ldt_data_fattura_a)

if not isnull(ls_protocollo_da) then  ls_sql += g_str.format(" and T.protocollo >= '$1' ", ls_protocollo_da)

if not isnull(ls_protocollo_a) then  ls_sql += g_str.format(" and T.protocollo <= '$1' ", ls_protocollo_a)

ls_sql += " order by 3,4,5 "

ll_ret = guo_functions.uof_crea_datastore( lds_data, ls_sql)

for ll_row = 1 to ll_ret
			
			ll_anno_registrazione = lds_data.getitemnumber(ll_row, 1)
			ll_num_registrazione = lds_data.getitemnumber(ll_row, 2) 
			ll_anno_documento = lds_data.getitemnumber(ll_row, 3) 
			ll_num_documento = lds_data.getitemnumber(ll_row, 4)
			ldt_data_registrazione = lds_data.getitemdatetime(ll_row, 5)
			ls_cod_fornitore = lds_data.getitemstring(ll_row, 6)
			ls_cod_pagamento = lds_data.getitemstring(ll_row, 7)
			ld_tot_val_fat_acq = lds_data.getitemnumber(ll_row, 8)
			ld_importo_iva = lds_data.getitemnumber(ll_row, 9) 
			ld_imponibile_iva = lds_data.getitemnumber(ll_row, 10) 
			ls_cod_valuta = lds_data.getitemstring(ll_row, 11)
			ll_prog_riga = lds_data.getitemnumber(ll_row, 12) 
			ls_cod_prodotto = lds_data.getitemstring(ll_row, 13) 
			ls_des_prodotto = lds_data.getitemstring(ll_row, 14) 
			ld_prezzo_acquisto = lds_data.getitemnumber(ll_row, 15)
			ld_sconto_1 = lds_data.getitemnumber(ll_row, 16) 
			ld_sconto_2 = lds_data.getitemnumber(ll_row, 17) 
			ld_sconto_3 = lds_data.getitemnumber(ll_row, 18) 
			ld_sconto_4 = lds_data.getitemnumber(ll_row, 19)
			ld_sconto_5 = lds_data.getitemnumber(ll_row, 20)
			ld_quan_fatturata = lds_data.getitemnumber(ll_row, 21) 
			ld_num_confezioni = lds_data.getitemnumber(ll_row, 22)
			ld_num_pezzi_confezione = lds_data.getitemnumber(ll_row, 23)
			ld_quan_sconto_merce = lds_data.getitemnumber(ll_row, 24) 
			ld_prezzo_acq_netto = lds_data.getitemnumber(ll_row, 25) 
			ls_cod_iva = lds_data.getitemstring(ll_row, 26)
			ls_nota_dettaglio = lds_data.getitemstring(ll_row, 27)
			ls_cod_tipo_det_acq = lds_data.getitemstring(ll_row, 28)
			ls_des_valuta = lds_data.getitemstring(ll_row, 29)
			ls_formato = lds_data.getitemstring(ll_row, 30)
			ls_rag_soc_1 = lds_data.getitemstring(ll_row, 31)
			ldt_data_doc_origine = lds_data.getitemdatetime(ll_row, 32 )
			ls_flag_tipo_det_acq =  lds_data.getitemstring(ll_row, 33)
			ld_imponibile_iva_riga = lds_data.getitemnumber(ll_row, 34)
			ls_cod_doc_origine =  lds_data.getitemstring(ll_row, 35)
			ll_anno_doc_origine = lds_data.getitemnumber(ll_row, 36)
			ls_numeratore_doc_origine =  lds_data.getitemstring(ll_row, 37)
			ll_num_doc_origine = lds_data.getitemnumber(ll_row, 38)
			ls_protocollo =  lds_data.getitemstring(ll_row, 39)
			ldt_data_protocollo  = lds_data.getitemdatetime(ll_row, 40)
			
			ll_i = dw_report.insertrow(0)
			dw_report.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
			dw_report.setitem(ll_i, "rag_soc_1", ls_rag_soc_1)
			dw_report.setitem(ll_i, "formato", ls_formato)
						
			dw_report.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
			dw_report.setitem(ll_i, "num_registrazione", ll_num_registrazione)			
			dw_report.setitem(ll_i, "prog_riga_fat_acq", ll_prog_riga)			
			dw_report.setitem(ll_i, "data_registrazione", ldt_data_registrazione)
			dw_report.setitem(ll_i, "cod_fornitore", ls_cod_fornitore)			
			dw_report.setitem(ll_i, "cod_pagamento", ls_cod_pagamento)
			dw_report.setitem(ll_i, "cod_prodotto", ls_cod_prodotto)							
			dw_report.setitem(ll_i, "des_prodotto", ls_des_prodotto)
			if ls_flag_tipo_det_acq <> "S" then
				dw_report.setitem(ll_i, "prezzo_acquisto", ld_prezzo_acquisto)
			else
				dw_report.setitem(ll_i, "prezzo_acquisto", ld_prezzo_acquisto * -1)			
			end if
			dw_report.setitem(ll_i, "sconto_1", ld_sconto_1)
			dw_report.setitem(ll_i, "sconto_2", ld_sconto_2)			
			dw_report.setitem(ll_i, "sconto_3", ld_sconto_3)
			dw_report.setitem(ll_i, "sconto_4", ld_sconto_4)						
			dw_report.setitem(ll_i, "sconto_5", ld_sconto_5)			
			
			select sconto 
			into   :ld_sconto_pag
			from   tab_pagamenti
			where  cod_azienda = :s_cs_xx.cod_azienda and 
			       cod_pagamento = :ls_cod_pagamento;
			if sqlca.sqlcode < 0 then	
				g_mb.messagebox("Apice", "Errore in lettura dati da TAB_PAGAMENTI.~n~r" + sqlca.sqlerrtext)
				return
			end if
			
			if sqlca.sqlcode = 100 then ld_sconto_pag = 0
			
			dw_report.setitem(ll_i, "sconto_pagamento", ld_sconto_pag)		
			if ls_flag_tipo_det_acq <> "S" then
				dw_report.setitem(ll_i, "prezzo_netto", ld_prezzo_acq_netto)	
			else
				dw_report.setitem(ll_i, "prezzo_netto", ld_prezzo_acq_netto * -1)	
			end if
			
			if not isnull(ld_quan_sconto_merce) and ld_quan_sconto_merce > 0 and &
				not isnull(ld_num_pezzi_confezione) and ld_num_pezzi_confezione > 0 then
				ld_quan_fatt_tot = ld_quan_fatturata + (ld_quan_sconto_merce * ld_num_pezzi_confezione)
			else	
				ld_quan_fatt_tot = ld_quan_fatturata
			end if
			dw_report.setitem(ll_i, "quan_fatturata", ld_quan_fatt_tot)			
			dw_report.setitem(ll_i, "num_confezioni", ld_num_confezioni)
			dw_report.setitem(ll_i, "num_pezzi_confezione", ld_num_pezzi_confezione)						
			dw_report.setitem(ll_i, "quan_sconto_merce", ld_quan_sconto_merce)
			dw_report.setitem(ll_i, "nota_dettaglio", ls_nota_dettaglio)			
			dw_report.setitem(ll_i, "totale_fattura", ld_tot_val_fat_acq)
			dw_report.setitem(ll_i, "totale_iva", ld_importo_iva)
			dw_report.setitem(ll_i, "totale_imponibile", ld_imponibile_iva)			
			
			//ld_tot_riga = ld_prezzo_acq_netto * ld_quan_fatturata
			ld_tot_riga = ld_imponibile_iva_riga 
			
			if ls_flag_tipo_det_acq <> "S" then
				dw_report.setitem(ll_i, "tot_riga", ld_tot_riga)
			else
				dw_report.setitem(ll_i, "tot_riga", ld_tot_riga * -1)
			end if
			
			if not isnull(ls_cod_doc_origine) then
				dw_report.setitem(ll_i, "num_fattura", g_str.format("$1/$2 - $3/$4 del $5", ls_cod_doc_origine, ls_numeratore_doc_origine, ll_anno_doc_origine, ll_num_doc_origine, string(ldt_data_doc_origine,"dd/mm/yyyy")  )	)
			end if
			
			dw_report.setitem(ll_i, "protocollo_fornitore", g_str.format("$1 del $2", ls_protocollo,string(ldt_data_protocollo, "dd/mm/yyyy")))
			
			select F.rag_soc_1, 
					F.localita, 
					F.stato, 
					N.des_nazione
			into	:ls_rag_soc_fornitore,
					:ls_localita,
					:ls_stato,
					:ls_des_nazione
			from  anag_fornitori F
			left outer join tab_nazioni N on F.cod_azienda=N.cod_azienda and F.cod_nazione=N.cod_nazione
			where F.cod_azienda = :s_cs_xx.cod_azienda and
					F.cod_fornitore = :ls_cod_fornitore;
					
			dw_report.setitem(ll_i, "rag_sociale", g_str.format("$1 - $2 ($3) - $4", ls_rag_soc_fornitore, ls_localita, ls_stato, ls_des_nazione))
			
//-------------------------------------- ULTIMO PREZZO NETTO DEL PRODOTTO-------------------------------------------------			
			// solo se è stato indicato un codice prodotto, altrimenti non ha senso, anzi fa proprio senso !!!!
			if ls_flag_tipo_det_acq = "M" or ls_flag_tipo_det_acq = "C" then
				
				ls_sql = 	" select	b.quan_fatturata, b.sconto_1, b.sconto_2, b.sconto_3, b.sconto_4, b.sconto_5, b.sconto_6, b.sconto_7, b.sconto_8, b.sconto_9, b.sconto_10, b.prezzo_acquisto, a.data_doc_origine, a.anno_registrazione, a.num_registrazione " + &
							" from tes_fat_acq a " + & 
							" left join det_fat_acq b on a.cod_azienda = b.cod_azienda and a.anno_registrazione=b.anno_registrazione and a.num_registrazione= b.num_registrazione " + &
							g_str.format(" where a.cod_azienda = '$1' and", s_cs_xx.cod_azienda) + &
							g_str.format(" a.cod_fornitore = '$1' and ",ls_cod_fornitore) + &
							g_str.format(" b.cod_prodotto = '$1' and ",ls_cod_prodotto) + &
							g_str.format(" a.data_doc_origine < '$1' and data_doc_origine is not null and ", string(ldt_data_doc_origine,s_cs_xx.db_funzioni.formato_data)) + &
							g_str.format(" a.anno_registrazione <> $1 and a.num_registrazione <> $2 ", ll_anno_registrazione, ll_num_registrazione ) + &
							" group by b.quan_fatturata,b.sconto_1,b.sconto_2,b.sconto_3,b.sconto_4,b.sconto_5,b.sconto_6,b.sconto_7,b.sconto_8,b.sconto_9,b.sconto_10,b.prezzo_acquisto,a.data_doc_origine,a.anno_registrazione,a.num_registrazione " + &
							" order by a.data_doc_origine DESC "

				ll_cont = guo_functions.uof_crea_datastore(lds_ultima_condizione, ls_sql)
			
				if ll_cont > 0 then
					ld_sconti[1] = lds_ultima_condizione.getitemnumber(1,2)
					ld_sconti[2] = lds_ultima_condizione.getitemnumber(1,3)
					ld_sconti[3] = lds_ultima_condizione.getitemnumber(1,4)
					ld_sconti[4] = lds_ultima_condizione.getitemnumber(1,5)
					ld_sconti[5] = lds_ultima_condizione.getitemnumber(1,6)
					ld_sconti[6] = lds_ultima_condizione.getitemnumber(1,7)
					ld_sconti[7] = lds_ultima_condizione.getitemnumber(1,8)
					ld_sconti[8] = lds_ultima_condizione.getitemnumber(1,9)
					ld_sconti[9] = lds_ultima_condizione.getitemnumber(1,10)
					ld_sconti[10] = lds_ultima_condizione.getitemnumber(1,11)
					ld_quan_fatturata_p = lds_ultima_condizione.getitemnumber(1,1)
					ld_prezzo_acquisto_p = lds_ultima_condizione.getitemnumber(1,12)
					ll_anno_reg_uc = lds_ultima_condizione.getitemnumber(1,14)
					ll_num_reg_uc = lds_ultima_condizione.getitemnumber(1,15)
					
					f_calcola_sconto(ld_sconti[], ld_prezzo_acquisto_p, ld_sconto_calcolato, ld_prezzo_scontato)
					dw_report.setitem(ll_i, "prezzo_precedente", ld_prezzo_scontato)			
				elseif ll_cont < 0 then
					g_mb.error("Errore in ricerca ultimo prezzo netto")
					destroy lds_ultima_condizione
				end if  
				destroy lds_ultima_condizione

			end if
			
next

dw_report.setsort("anno_registrazione A, num_registrazione A, prog_riga_fat_acq A")
dw_report.sort()
dw_report.groupcalc()

cb_selezione.show()
dw_report.show()

parent.x = 100
parent.y = 50
parent.width = 3900
parent.height = 1945

dw_report.change_dw_current()
end event

type cb_selezione from commandbutton within w_report_fat_acq_euro
integer x = 3474
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_selezione.show()

parent.x = 677
parent.y = 589
parent.width = 2700
parent.height = 800

dw_report.hide()
cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_report from uo_cs_xx_dw within w_report_fat_acq_euro
boolean visible = false
integer x = 23
integer y = 20
integer width = 3817
integer height = 1700
integer taborder = 60
string dataobject = "d_report_fat_acq_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

type st_1 from statictext within w_report_fat_acq_euro
integer x = 23
integer y = 540
integer width = 1829
integer height = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 255
long backcolor = 79741120
boolean enabled = false
string text = "Attenzione: la stampa non calcola le fatture."
boolean focusrectangle = false
end type

