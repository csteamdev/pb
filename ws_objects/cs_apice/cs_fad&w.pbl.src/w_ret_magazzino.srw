﻿$PBExportHeader$w_ret_magazzino.srw
$PBExportComments$Finestra Gestione Rettifiche Magazzino
forward
global type w_ret_magazzino from w_cs_xx_principale
end type
type cb_ricerca from commandbutton within w_ret_magazzino
end type
type cb_conferma from commandbutton within w_ret_magazzino
end type
type cb_reset from commandbutton within w_ret_magazzino
end type
type cb_genera from commandbutton within w_ret_magazzino
end type
type dw_ret_magazzino_det from uo_cs_xx_dw within w_ret_magazzino
end type
type dw_generazione from datawindow within w_ret_magazzino
end type
type dw_ret_magazzino_lista from uo_cs_xx_dw within w_ret_magazzino
end type
type dw_folder_search from u_folder within w_ret_magazzino
end type
type dw_ricerca from u_dw_search within w_ret_magazzino
end type
end forward

global type w_ret_magazzino from w_cs_xx_principale
integer width = 3031
integer height = 1500
string title = "Rettifiche di Magazzino"
cb_ricerca cb_ricerca
cb_conferma cb_conferma
cb_reset cb_reset
cb_genera cb_genera
dw_ret_magazzino_det dw_ret_magazzino_det
dw_generazione dw_generazione
dw_ret_magazzino_lista dw_ret_magazzino_lista
dw_folder_search dw_folder_search
dw_ricerca dw_ricerca
end type
global w_ret_magazzino w_ret_magazzino

event pc_setwindow;call super::pc_setwindow;dw_ret_magazzino_lista.set_dw_key("cod_azienda")
dw_ret_magazzino_lista.set_dw_options(sqlca, &
                                     pcca.null_object, &
                                     c_nonew + c_noretrieveonopen, &
                                     c_default)
dw_ret_magazzino_det.set_dw_options(sqlca, &
                                   dw_ret_magazzino_lista, &
                                   c_sharedata + c_scrollparent + c_nonew, &
                                   c_default)
iuo_dw_main = dw_ret_magazzino_lista

// ****************************
string l_criteriacolumn[],l_searchtable[],l_searchcolumn[]
windowobject l_objects[]

l_criteriacolumn[1] = "anno_registrazione"
l_criteriacolumn[2] = "num_registrazione"
l_criteriacolumn[3] = "anno_documento"
l_criteriacolumn[4] = "num_documento"
l_criteriacolumn[5] = "protocollo"
l_criteriacolumn[6] = "cod_prodotto"
l_criteriacolumn[7] = "cod_deposito"

l_searchtable[1] = "ret_magazzino"
l_searchtable[2] = "ret_magazzino"
l_searchtable[3] = "ret_magazzino"
l_searchtable[4] = "ret_magazzino"
l_searchtable[5] = "ret_magazzino"
l_searchtable[6] = "ret_magazzino"
l_searchtable[7] = "ret_magazzino"

l_searchcolumn[1] = "anno_registrazione"
l_searchcolumn[2] = "num_registrazione"
l_searchcolumn[3] = "tes_fat_acq_anno_doc_origine"
l_searchcolumn[4] = "tes_fat_acq_num_doc_origine"
l_searchcolumn[5] = "protocollo"
l_searchcolumn[6] = "cod_prodotto"
l_searchcolumn[7] = "cod_deposito"

dw_ricerca.fu_wiredw(l_criteriacolumn[], &
                     dw_ret_magazzino_lista, &
							l_searchtable[], &
							l_searchcolumn[], &
							SQLCA)

dw_folder_search.fu_folderoptions(dw_folder_search.c_defaultheight, &
                                  dw_folder_search.c_foldertableft)

l_objects[1] = dw_ret_magazzino_lista
l_objects[2] = cb_conferma
dw_folder_search.fu_assigntab(1, "L.", l_objects[])
l_objects[1] = dw_generazione
l_objects[2] = cb_genera
//l_objects[3] = cb_ricerca_fornitore
//l_objects[3] = cb_ricerca_prodotto_1
dw_folder_search.fu_assigntab(3, "G.", l_objects[])
l_objects[1] = dw_ricerca
l_objects[2] = cb_ricerca
l_objects[3] = cb_reset
//l_objects[4] = cb_ricerca_prodotto
dw_folder_search.fu_assigntab(2, "R.", l_objects[])

dw_folder_search.fu_foldercreate(3,4)
dw_folder_search.fu_selecttab(2)
dw_generazione.insertrow(0)

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_ret_magazzino_det, &
                 "cod_tipo_movimento", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

on w_ret_magazzino.create
int iCurrent
call super::create
this.cb_ricerca=create cb_ricerca
this.cb_conferma=create cb_conferma
this.cb_reset=create cb_reset
this.cb_genera=create cb_genera
this.dw_ret_magazzino_det=create dw_ret_magazzino_det
this.dw_generazione=create dw_generazione
this.dw_ret_magazzino_lista=create dw_ret_magazzino_lista
this.dw_folder_search=create dw_folder_search
this.dw_ricerca=create dw_ricerca
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_ricerca
this.Control[iCurrent+2]=this.cb_conferma
this.Control[iCurrent+3]=this.cb_reset
this.Control[iCurrent+4]=this.cb_genera
this.Control[iCurrent+5]=this.dw_ret_magazzino_det
this.Control[iCurrent+6]=this.dw_generazione
this.Control[iCurrent+7]=this.dw_ret_magazzino_lista
this.Control[iCurrent+8]=this.dw_folder_search
this.Control[iCurrent+9]=this.dw_ricerca
end on

on w_ret_magazzino.destroy
call super::destroy
destroy(this.cb_ricerca)
destroy(this.cb_conferma)
destroy(this.cb_reset)
destroy(this.cb_genera)
destroy(this.dw_ret_magazzino_det)
destroy(this.dw_generazione)
destroy(this.dw_ret_magazzino_lista)
destroy(this.dw_folder_search)
destroy(this.dw_ricerca)
end on

type cb_ricerca from commandbutton within w_ret_magazzino
integer x = 2560
integer y = 160
integer width = 361
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;// cb_search clicked event
dw_ricerca.fu_BuildSearch(TRUE)
dw_folder_search.fu_SelectTab(1)
dw_ret_magazzino_lista.change_dw_current()
parent.triggerevent("pc_retrieve")


end event

type cb_conferma from commandbutton within w_ret_magazzino
event clicked pbm_bnclicked
integer x = 2560
integer y = 600
integer width = 361
integer height = 80
integer taborder = 21
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Con&ferma"
end type

event clicked;integer li_row, li_ret
string ls_messaggio
long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_acq
	
li_row = dw_ret_magazzino_lista.getrow()
ll_anno_registrazione = dw_ret_magazzino_lista.getitemnumber(li_row, "anno_registrazione")
ll_num_registrazione = dw_ret_magazzino_lista.getitemnumber(li_row, "num_registrazione")
ll_prog_riga_fat_acq = dw_ret_magazzino_lista.getitemnumber(li_row, "prog_riga_fat_acq")

li_ret = f_conferma_ret_mag( ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_acq, ref ls_messaggio)
										
if li_ret = -1 then 
	g_mb.messagebox("Conferma Rettifiche di Magazzino", ls_messaggio, exclamation!)
else
	dw_ret_magazzino_lista.triggerevent("pcd_retrieve")
end if
end event

type cb_reset from commandbutton within w_ret_magazzino
integer x = 2560
integer y = 60
integer width = 361
integer height = 80
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla Ric."
end type

event clicked;dw_ricerca.fu_Reset()



end event

type cb_genera from commandbutton within w_ret_magazzino
integer x = 2560
integer y = 600
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Genera Rett."
end type

event clicked;string ls_cod_prodotto, ls_cod_deposito, ls_protocollo, ls_cod_fornitore, ls_sql, ls_cod_tipo_det_acq, ls_flag_tipo_det_acq, &
       ls_cod_tipo_mov_inv_ret_pos,ls_cod_tipo_mov_inv_ret_neg, ls_cod_ubicazione, ls_cod_lotto, ls_cod_tipo_movimento, &
		 ls_default, ls_database
long   ll_num_reg_inizio, ll_num_reg_fine, ll_err, ll_righe, ll_i, ll_anno_bolla, ll_num_bolla, ll_riga_bolla, ll_prog_stock, &
       ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_fat_acq, ll_risposta
dec{4} ld_prezzo_bolla, ld_prezzo_fattura,ld_quan_movimento
datetime ldt_data_reg_inizio, ldt_data_reg_fine, ldt_data_fattura_inizio, ldt_data_fattura_fine, ldt_data_stock
datastore lds_datastore

dw_generazione.accepttext()

// ------------------ leggo campi di selezione per genrazione --------------------------------------------- //
ls_cod_prodotto = dw_generazione.getitemstring(dw_generazione.getrow(),"cod_prodotto")
ls_cod_deposito = dw_generazione.getitemstring(dw_generazione.getrow(),"cod_deposito")
ls_protocollo = dw_generazione.getitemstring(dw_generazione.getrow(),"protocollo")
ls_cod_fornitore = dw_generazione.getitemstring(dw_generazione.getrow(),"cod_fornitore")
ll_num_reg_inizio = dw_generazione.getitemnumber(dw_generazione.getrow(),"num_registrazione_inizio")
ll_num_reg_fine = dw_generazione.getitemnumber(dw_generazione.getrow(),"num_registrazione_fine")
ldt_data_reg_inizio = dw_generazione.getitemdatetime(dw_generazione.getrow(),"data_registrazione_inizio")
ldt_data_reg_fine = dw_generazione.getitemdatetime(dw_generazione.getrow(),"data_registrazione_fine")
ldt_data_fattura_inizio = dw_generazione.getitemdatetime(dw_generazione.getrow(),"data_fattura_inizio")
ldt_data_fattura_fine = dw_generazione.getitemdatetime(dw_generazione.getrow(),"data_fattura_fine")

if ll_num_reg_inizio > ll_num_reg_fine then
	g_mb.messagebox("APICE","Il numero di registrazione inziale è maggiore di quello finale!",stopsign!)
	return
end if

// ------------------- creazione datastore ---------------------------------------------------------------- //
lds_datastore = CREATE datastore
lds_datastore.dataobject = "d_ds_ret_magazzino_generazione"
lds_datastore.settransobject(sqlca)
ls_sql = lds_datastore.getsqlselect()

// ------------------ MODIFICA AL DATASTORE PER AGGIUNTA DELLA WHERE --------------------------------------- //

ll_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_database)
if upper(ls_database) = "SYBASE_ASA" then
	ls_sql = ls_sql  + " where "
else
	ls_sql = ls_sql + " and "
end if

ls_sql = ls_sql + " tes_fat_acq.cod_azienda = '" + s_cs_xx.cod_azienda + "' and tes_fat_acq.flag_fat_confermata = 'S' "
if not isnull(ls_cod_prodotto) then ls_sql = ls_sql + " and det_fat_acq.cod_prodotto = '" + ls_cod_prodotto + "' and det_fat_acq.cod_prodotto is not null "
if not isnull(ls_cod_deposito) then ls_sql = ls_sql + " and det_fat_acq.cod_deposito = '" + ls_cod_deposito + "' and det_fat_acq.cod_deposito is not null "
if not isnull(ls_cod_fornitore) then ls_sql = ls_sql + " and tes_fat_acq.cod_fornitore = '" + ls_cod_fornitore + "' "
if not isnull(ls_protocollo) then ls_sql = ls_sql + " and tes_fat_acq.protocollo = '" + ls_protocollo + "' "
if ll_num_reg_inizio > 0 and not isnull(ll_num_reg_inizio) then
	ls_sql = ls_sql + " and tes_fat_acq.num_registrazione >= " + string(ll_num_reg_inizio)
end if
if ll_num_reg_fine > 0 and not isnull(ll_num_reg_fine) then
	ls_sql = ls_sql + " and tes_fat_acq.num_registrazione <= " + string(ll_num_reg_fine)
end if
if not isnull(ldt_data_reg_inizio) then
	ls_sql = ls_sql + " and tes_fat_acq.data_registrazione >= '" + string(date(ldt_data_reg_inizio),s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_data_reg_fine) then
	ls_sql = ls_sql + " and tes_fat_acq.data_registrazione <= '" + string(date(ldt_data_reg_fine),s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_fattura_inizio) then
	ls_sql = ls_sql + " and tes_fat_acq.data_doc_origine >= '" + string(date(ldt_data_fattura_inizio),s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_data_fattura_fine) then
	ls_sql = ls_sql + " and tes_fat_acq.data_doc_origine <= '" + string(date(ldt_data_fattura_fine),s_cs_xx.db_funzioni.formato_data) + "' "
end if

// ---------------------------------  imposto SQL nel datastore ------------------------------------------------------- //
ll_err = lds_datastore.setsqlselect(ls_sql)
if ll_err = -1 then
	g_mb.messagebox("APICE","Errore nella generazione SQL per la ricerca~ndelle fatture di cui fare le rettifiche.~nTrascrivere i dati usati per la seleizione e comunicarli~al servizio di assistenza.",stopsign!)
	return
end if
ll_righe = lds_datastore.retrieve()
for ll_i = 1 to ll_righe
	// leggo il tipo dettaglio e controllo se è prodotto a magazzino; se non lo è salto
	ls_cod_tipo_det_acq = lds_datastore.getitemstring(ll_i,"det_fat_acq_cod_tipo_det_acq")
	select flag_tipo_det_acq,
	       cod_tipo_mov_ret_pos,
	       cod_tipo_mov_ret_neg
	into   :ls_flag_tipo_det_acq,
			 :ls_cod_tipo_mov_inv_ret_pos,
	       :ls_cod_tipo_mov_inv_ret_neg
	from   tab_tipi_det_acq
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_det_acq = :ls_cod_tipo_det_acq;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella ricerca del tipo dettaglio di acquisto.~n" + sqlca.sqlerrtext)
		continue
	end if
	if isnull(ls_cod_tipo_mov_inv_ret_pos) or isnull(ls_cod_tipo_mov_inv_ret_neg) then
		g_mb.messagebox("APICE","Attenzione ! Impostare i tipi movimenti di rettifica nel tipo dettaglio di acquisto " + ls_cod_tipo_det_acq + ".~nElaborazione interrotta",stopsign!)
		return
	end if
	if ls_flag_tipo_det_acq <> "M" then continue
	// se fattura immediata (nessun rif. alla bolla) allora salto; 
	// potrebbe esserci anche qualche riferimento incompleto : salto lo stesso.
	ll_anno_bolla = lds_datastore.getitemnumber(ll_i,"det_fat_acq_anno_bolla_acq")
	ll_num_bolla = lds_datastore.getitemnumber(ll_i,"det_fat_acq_num_bolla_acq")
	ll_riga_bolla = lds_datastore.getitemnumber(ll_i,"det_fat_acq_prog_riga_bolla_acq")
	if isnull(ll_anno_bolla) or isnull(ll_num_bolla) or isnull(ll_riga_bolla) or ll_anno_bolla = 0 or ll_num_bolla=0 or ll_riga_bolla=0 then continue
	// controllo lo stock; è completo ?
	ls_cod_prodotto =lds_datastore.getitemstring(ll_i,"det_fat_acq_cod_prodotto")
	ls_cod_deposito = lds_datastore.getitemstring(ll_i,"det_fat_acq_cod_deposito")
	ls_cod_ubicazione = lds_datastore.getitemstring(ll_i,"det_fat_acq_cod_ubicazione")
	ls_cod_lotto = lds_datastore.getitemstring(ll_i,"det_fat_acq_cod_lotto")
	ldt_data_stock = lds_datastore.getitemdatetime(ll_i,"det_fat_acq_data_stock")
	ll_prog_stock = lds_datastore.getitemnumber(ll_i,"det_fat_acq_prog_stock")
	if isnull(ls_cod_prodotto) or isnull(ls_cod_deposito) or isnull(ls_cod_ubicazione) or isnull(ls_cod_lotto) or isnull(ldt_data_stock) or isnull(ll_prog_stock) or ll_prog_stock = 0 then continue
	// leggo prezzo bolla e prezzo fattura; confronto e se diverse procedo con generazione rettifiche
	ld_prezzo_fattura = lds_datastore.getitemnumber(ll_i,"det_fat_acq_prezzo_acquisto")
	if isnull(ld_prezzo_fattura) then ld_prezzo_fattura = 0
	ld_prezzo_bolla = lds_datastore.getitemnumber(ll_i,"det_bol_acq_prezzo_acquisto")
	if isnull(ld_prezzo_bolla) then ld_prezzo_bolla = 0
	if ld_prezzo_bolla = ld_prezzo_fattura then continue
	if ld_prezzo_bolla > ld_prezzo_fattura then
		// se prezzo bolla > prezzo fattura allora rettifico in negativo
		ls_cod_tipo_movimento = ls_cod_tipo_mov_inv_ret_neg
	else
		// se prezzo bolla < prezzo fattura allora rettifico in positivo
		ls_cod_tipo_movimento = ls_cod_tipo_mov_inv_ret_pos
	end if
	// nella tabella ret_magazzino memorizzo la quantità bolla e non la quantità fattura, altrimenti rischio di non trovarmi con il valore medio.
	ld_quan_movimento = lds_datastore.getitemnumber(ll_i,"det_bol_acq_quan_arrivata")
	ll_anno_registrazione = lds_datastore.getitemnumber(ll_i,"det_fat_acq_anno_registrazione")
	ll_num_registrazione = lds_datastore.getitemnumber(ll_i,"det_fat_acq_num_registrazione")
	ll_prog_riga_fat_acq = lds_datastore.getitemnumber(ll_i,"det_fat_acq_prog_riga_fat_acq")
	
	INSERT INTO ret_magazzino  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  prog_riga_fat_acq,   
			  imp_bolla,   
			  imp_fattura,   
			  cod_tipo_movimento,   
			  cod_prodotto,   
			  cod_deposito,   
			  cod_ubicazione,   
			  cod_lotto,   
			  data_stock,   
			  quan_movimento,   
			  prog_stock )  
	VALUES (:s_cs_xx.cod_azienda,   
			  :ll_anno_registrazione,   
			  :ll_num_registrazione,   
			  :ll_prog_riga_fat_acq,   
			  :ld_prezzo_bolla,   
			  :ld_prezzo_fattura,   
			  :ls_cod_tipo_movimento,   
			  :ls_cod_prodotto,   
			  :ls_cod_deposito,   
			  :ls_cod_ubicazione,   
			  :ls_cod_lotto,   
			  :ldt_data_stock,   
			  :ld_quan_movimento,   
			  :ll_prog_stock )  ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in insert della tabella ret_magazzino.~n+sqlca.sqlerrtext")
		rollback;
	else
		commit;
	end if	
next

commit;
end event

type dw_ret_magazzino_det from uo_cs_xx_dw within w_ret_magazzino
integer x = 23
integer y = 720
integer width = 2949
integer height = 660
integer taborder = 20
string dataobject = "d_ret_magazzino_det"
borderstyle borderstyle = styleraised!
end type

type dw_generazione from datawindow within w_ret_magazzino
integer x = 183
integer y = 40
integer width = 2743
integer height = 640
integer taborder = 30
string title = "none"
string dataobject = "d_ret_magazzino_generazione"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;choose case dwo.name
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_generazione,"cod_fornitore")
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_generazione,"cod_prodotto")
end choose
end event

type dw_ret_magazzino_lista from uo_cs_xx_dw within w_ret_magazzino
integer x = 183
integer y = 60
integer width = 2743
integer height = 520
integer taborder = 10
string dataobject = "d_ret_magazzino_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

event pcd_delete;call super::pcd_delete;cb_conferma.enabled = false
end event

event pcd_modify;call super::pcd_modify;cb_conferma.enabled = false
end event

event pcd_new;call super::pcd_new;cb_conferma.enabled = false
end event

event pcd_save;call super::pcd_save;cb_conferma.enabled = true
end event

event pcd_view;call super::pcd_view;cb_conferma.enabled = true
end event

type dw_folder_search from u_folder within w_ret_magazzino
integer x = 23
integer y = 20
integer width = 2926
integer height = 680
integer taborder = 10
end type

type dw_ricerca from u_dw_search within w_ret_magazzino
event ue_key pbm_dwnkey
integer x = 183
integer y = 40
integer width = 2693
integer height = 516
integer taborder = 20
string dataobject = "d_ret_magazzino_filtro"
boolean border = false
end type

event ue_key;choose case this.getcolumnname()
	case "rs_cod_cliente"
		if key = keyF1!  and keyflags = 1 then
			guo_ricerca.uof_ricerca_cliente(dw_ricerca,"rs_cod_cliente")
		end if
end choose

if key = keyenter! then cb_ricerca.postevent("clicked")
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_ricerca,"cod_prodotto")
end choose
end event

