﻿$PBExportHeader$w_richiesta_azione_agg_mov.srw
forward
global type w_richiesta_azione_agg_mov from w_cs_xx_risposta
end type
type st_11 from statictext within w_richiesta_azione_agg_mov
end type
type dw_1 from uo_cs_xx_dw within w_richiesta_azione_agg_mov
end type
type cbx_scarica_grezzo from checkbox within w_richiesta_azione_agg_mov
end type
type rb_1 from radiobutton within w_richiesta_azione_agg_mov
end type
type rb_2 from radiobutton within w_richiesta_azione_agg_mov
end type
type cbx_1 from checkbox within w_richiesta_azione_agg_mov
end type
type cb_chiedi from commandbutton within w_richiesta_azione_agg_mov
end type
type cb_annulla from commandbutton within w_richiesta_azione_agg_mov
end type
type st_1 from statictext within w_richiesta_azione_agg_mov
end type
type st_2 from statictext within w_richiesta_azione_agg_mov
end type
type st_3 from statictext within w_richiesta_azione_agg_mov
end type
type st_4 from statictext within w_richiesta_azione_agg_mov
end type
type cb_somma_seempre from commandbutton within w_richiesta_azione_agg_mov
end type
type st_5 from statictext within w_richiesta_azione_agg_mov
end type
type st_6 from statictext within w_richiesta_azione_agg_mov
end type
type st_7 from statictext within w_richiesta_azione_agg_mov
end type
type cb_non_sommare from commandbutton within w_richiesta_azione_agg_mov
end type
type st_8 from statictext within w_richiesta_azione_agg_mov
end type
type st_9 from statictext within w_richiesta_azione_agg_mov
end type
type st_10 from statictext within w_richiesta_azione_agg_mov
end type
end forward

global type w_richiesta_azione_agg_mov from w_cs_xx_risposta
integer width = 2533
string title = "AGGIORNAMENTO VALORE MOVIMENTO DI MAGAZZINO"
st_11 st_11
dw_1 dw_1
cbx_scarica_grezzo cbx_scarica_grezzo
rb_1 rb_1
rb_2 rb_2
cbx_1 cbx_1
cb_chiedi cb_chiedi
cb_annulla cb_annulla
st_1 st_1
st_2 st_2
st_3 st_3
st_4 st_4
cb_somma_seempre cb_somma_seempre
st_5 st_5
st_6 st_6
st_7 st_7
cb_non_sommare cb_non_sommare
st_8 st_8
st_9 st_9
st_10 st_10
end type
global w_richiesta_azione_agg_mov w_richiesta_azione_agg_mov

type variables
boolean ib_close
end variables

forward prototypes
public subroutine wf_close (string as_number)
end prototypes

public subroutine wf_close (string as_number);s_cs_xx.parametri.parametro_s_1 = as_number
ib_close=true
close(this)

end subroutine

on w_richiesta_azione_agg_mov.create
int iCurrent
call super::create
this.st_11=create st_11
this.dw_1=create dw_1
this.cbx_scarica_grezzo=create cbx_scarica_grezzo
this.rb_1=create rb_1
this.rb_2=create rb_2
this.cbx_1=create cbx_1
this.cb_chiedi=create cb_chiedi
this.cb_annulla=create cb_annulla
this.st_1=create st_1
this.st_2=create st_2
this.st_3=create st_3
this.st_4=create st_4
this.cb_somma_seempre=create cb_somma_seempre
this.st_5=create st_5
this.st_6=create st_6
this.st_7=create st_7
this.cb_non_sommare=create cb_non_sommare
this.st_8=create st_8
this.st_9=create st_9
this.st_10=create st_10
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_11
this.Control[iCurrent+2]=this.dw_1
this.Control[iCurrent+3]=this.cbx_scarica_grezzo
this.Control[iCurrent+4]=this.rb_1
this.Control[iCurrent+5]=this.rb_2
this.Control[iCurrent+6]=this.cbx_1
this.Control[iCurrent+7]=this.cb_chiedi
this.Control[iCurrent+8]=this.cb_annulla
this.Control[iCurrent+9]=this.st_1
this.Control[iCurrent+10]=this.st_2
this.Control[iCurrent+11]=this.st_3
this.Control[iCurrent+12]=this.st_4
this.Control[iCurrent+13]=this.cb_somma_seempre
this.Control[iCurrent+14]=this.st_5
this.Control[iCurrent+15]=this.st_6
this.Control[iCurrent+16]=this.st_7
this.Control[iCurrent+17]=this.cb_non_sommare
this.Control[iCurrent+18]=this.st_8
this.Control[iCurrent+19]=this.st_9
this.Control[iCurrent+20]=this.st_10
end on

on w_richiesta_azione_agg_mov.destroy
call super::destroy
destroy(this.st_11)
destroy(this.dw_1)
destroy(this.cbx_scarica_grezzo)
destroy(this.rb_1)
destroy(this.rb_2)
destroy(this.cbx_1)
destroy(this.cb_chiedi)
destroy(this.cb_annulla)
destroy(this.st_1)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_4)
destroy(this.cb_somma_seempre)
destroy(this.st_5)
destroy(this.st_6)
destroy(this.st_7)
destroy(this.cb_non_sommare)
destroy(this.st_8)
destroy(this.st_9)
destroy(this.st_10)
end on

event closequery;call super::closequery;if not ib_close then return 1

if (cbx_1.checked) then
	if (rb_1.checked) then 
		s_cs_xx.parametri.parametro_s_5 = "1"  // chiedi ogni volta
	elseif (rb_2.checked) then
		s_cs_xx.parametri.parametro_s_5 = "2"  // fa senza chiedere
	end if
	
end if

if (cbx_scarica_grezzo.checked) then
		s_cs_xx.parametri.parametro_s_2 = "1"  
else
		s_cs_xx.parametri.parametro_s_2 = "0"  
end if


dw_1.resetupdate()
// -- stefanop 26/02/2010
if cbx_scarica_grezzo.checked then
	s_cs_xx.parametri.parametro_s_6 = dw_1.getitemstring(1, "cod_deposito")
else
	s_cs_xx.parametri.parametro_s_6 = ""
end if
end event

event pc_setwindow;call super::pc_setwindow;set_w_options(c_closenosave + c_autoposition + c_noresizewin + c_noenablepopup)

dw_1.set_dw_options(sqlca, &
  										pcca.null_object, &
										c_nomodify + &
										c_noretrieveonopen + &
										c_nodelete + &
										c_newonopen + &
										c_disableCC, &
										c_noresizedw + &
										c_nohighlightselected + &
										c_nocursorrowpointer +&
										c_nocursorrowfocusrect )
										
										
rb_1.visible = false
rb_2.visible = false	

st_11.visible = false
dw_1.visible = false										

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_1, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")
end event

type st_11 from statictext within w_richiesta_azione_agg_mov
integer x = 1097
integer y = 940
integer width = 389
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Deposito:"
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_1 from uo_cs_xx_dw within w_richiesta_azione_agg_mov
integer x = 1509
integer y = 920
integer width = 914
integer height = 100
integer taborder = 10
string dataobject = "d_richieste_mov_depositi"
boolean border = false
end type

type cbx_scarica_grezzo from checkbox within w_richiesta_azione_agg_mov
integer x = 46
integer y = 940
integer width = 855
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Scarica Grezzo dal Magazzino"
end type

event clicked;if (this.checked) then
	parent.rb_1.visible = true
	parent.rb_2.visible = true	

	parent.rb_1.enabled = true
	parent.rb_2.checked = true
	parent.rb_2.enabled = true
	
	parent.st_11.visible = true
	parent.dw_1.visible = true
else
	parent.rb_1.visible = false
	parent.rb_2.visible = false
	
	parent.rb_1.checked = false
	parent.rb_2.checked = false
	parent.rb_1.enabled = false
	parent.rb_2.enabled = false	
	
	parent.st_11.visible = false
	parent.dw_1.visible = false
end if
end event

type rb_1 from radiobutton within w_richiesta_azione_agg_mov
integer x = 46
integer y = 1040
integer width = 823
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Chiedi conferma ogni volta"
end type

type rb_2 from radiobutton within w_richiesta_azione_agg_mov
integer x = 1303
integer y = 1040
integer width = 1088
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Aggiorna tutte le righe senza conferma"
end type

type cbx_1 from checkbox within w_richiesta_azione_agg_mov
integer x = 46
integer y = 840
integer width = 686
integer height = 80
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Aggiorna Costo Ultimo "
end type

event clicked;if (this.checked) then
	parent.rb_1.enabled = true
	parent.rb_2.checked = true
	parent.rb_2.enabled = true
else
	parent.rb_1.checked = false
	parent.rb_2.checked = false
	parent.rb_1.enabled = false
	parent.rb_2.enabled = false	
end if
end event

type cb_chiedi from commandbutton within w_richiesta_azione_agg_mov
integer x = 46
integer y = 1180
integer width = 389
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiedi"
end type

event clicked;wf_close("1")
end event

type cb_annulla from commandbutton within w_richiesta_azione_agg_mov
integer x = 2057
integer y = 1180
integer width = 389
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;wf_close("4")
end event

type st_1 from statictext within w_richiesta_azione_agg_mov
integer x = 46
integer y = 20
integer width = 2423
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "L~'operazione richiesta aggiorna il valore del movimento di magazzino."
boolean focusrectangle = false
end type

type st_2 from statictext within w_richiesta_azione_agg_mov
integer x = 46
integer y = 100
integer width = 2423
integer height = 140
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Seleziona l~'azione da eseguire nel caso vi sia una valore di prodotto grezzo da sommare al valore del prodotto acquistato."
boolean focusrectangle = false
end type

type st_3 from statictext within w_richiesta_azione_agg_mov
integer x = 46
integer y = 280
integer width = 229
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "CHIEDI:"
boolean focusrectangle = false
end type

type st_4 from statictext within w_richiesta_azione_agg_mov
integer x = 594
integer y = 420
integer width = 1851
integer height = 140
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Se è caricato il valore del prodotto grezzo, verrà sempre sommato al prezzo netto di acquisto, senza alcuna richiesta di conferma."
boolean focusrectangle = false
end type

type cb_somma_seempre from commandbutton within w_richiesta_azione_agg_mov
integer x = 617
integer y = 1180
integer width = 571
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Somma Sempre"
end type

event clicked;wf_close("2")
end event

type st_5 from statictext within w_richiesta_azione_agg_mov
integer x = 594
integer y = 280
integer width = 1851
integer height = 140
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Ogni volta che viene trovata una riga con un valore di grezzo viene  chiesta l~'azione da eseguire."
boolean focusrectangle = false
end type

type st_6 from statictext within w_richiesta_azione_agg_mov
integer x = 46
integer y = 420
integer width = 526
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "SOMMA SEMPRE:"
boolean focusrectangle = false
end type

type st_7 from statictext within w_richiesta_azione_agg_mov
integer x = 46
integer y = 560
integer width = 526
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "IGNORA GREZZO:"
boolean focusrectangle = false
end type

type cb_non_sommare from commandbutton within w_richiesta_azione_agg_mov
integer x = 1326
integer y = 1180
integer width = 571
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Ignora Grezzo"
end type

event clicked;wf_close("3")
end event

type st_8 from statictext within w_richiesta_azione_agg_mov
integer x = 594
integer y = 560
integer width = 1829
integer height = 140
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Il valore del prodotto grezzo verrà ingorato; il movimento di magazzino sarà aggiornato con il solo prezzo netto di acquisto."
boolean focusrectangle = false
end type

type st_9 from statictext within w_richiesta_azione_agg_mov
integer x = 594
integer y = 700
integer width = 1307
integer height = 80
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Annulla l~'operazione e torna alle righe fattura."
boolean focusrectangle = false
end type

type st_10 from statictext within w_richiesta_azione_agg_mov
integer x = 46
integer y = 700
integer width = 320
integer height = 60
integer textsize = -10
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "ANNULLA:"
boolean focusrectangle = false
end type

