﻿$PBExportHeader$w_scad_fat_acq.srw
$PBExportComments$Finestra Scadenze Fatture Acquisto
forward
global type w_scad_fat_acq from w_cs_xx_principale
end type
type dw_scad_fat_acq from uo_cs_xx_dw within w_scad_fat_acq
end type
end forward

global type w_scad_fat_acq from w_cs_xx_principale
integer width = 2999
integer height = 1148
string title = "Scadenze"
dw_scad_fat_acq dw_scad_fat_acq
end type
global w_scad_fat_acq w_scad_fat_acq

event pc_setwindow;call super::pc_setwindow;dw_scad_fat_acq.set_dw_key("cod_azienda")
dw_scad_fat_acq.set_dw_key("anno_registrazione")
dw_scad_fat_acq.set_dw_key("num_registrazione")
dw_scad_fat_acq.set_dw_key("prog_scadenza")

dw_scad_fat_acq.set_dw_options(sqlca, &
                               i_openparm, &
                               c_scrollparent, &
                               c_default)

end event

on w_scad_fat_acq.create
int iCurrent
call super::create
this.dw_scad_fat_acq=create dw_scad_fat_acq
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_scad_fat_acq
end on

on w_scad_fat_acq.destroy
call super::destroy
destroy(this.dw_scad_fat_acq)
end on

event pc_setddlb;call super::pc_setddlb;if isvalid(sqlci) then
	f_po_loaddddw_dw(dw_scad_fat_acq,"cod_tipo_pagamento",sqlci,"tipo_pagamento","codice","descrizione","")
else
	f_po_loaddddw_dw(dw_scad_fat_acq,"cod_tipo_pagamento",sqlca,"tab_tipi_pagamenti", &
						 "cod_tipo_pagamento","des_tipo_pagamento","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end if
end event

type dw_scad_fat_acq from uo_cs_xx_dw within w_scad_fat_acq
integer x = 23
integer y = 20
integer width = 2903
integer height = 1000
integer taborder = 20
string dataobject = "d_scad_fat_acq"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;datetime ldt_data_scadenza
long   l_errore, ll_anno_registrazione, ll_num_registrazione, ll_i, ll_prog_scadenza

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	
for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if	

	if isnull(this.getitemnumber(ll_i, "prog_scadenza")) or &
		this.getitemnumber(ll_i, "prog_scadenza") = 0 then

		ldt_data_scadenza = this.getitemdatetime(ll_i, "data_scadenza")

		select max(scad_fat_acq.prog_scadenza)
		into   :ll_prog_scadenza
		from   scad_fat_acq
		where  scad_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and
				 scad_fat_acq.anno_registrazione = :ll_anno_registrazione and
				 scad_fat_acq.num_registrazione = :ll_num_registrazione and
				 scad_fat_acq.data_scadenza = :ldt_data_scadenza;

		if isnull(ll_prog_scadenza) then
			this.setitem(ll_i, "prog_scadenza", 1)
		else
			this.setitem(ll_i, "prog_scadenza", ll_prog_scadenza + 1)
		end if
	end if
next
end event

event pcd_retrieve;call super::pcd_retrieve;long   l_errore, ll_anno_registrazione, ll_num_registrazione, ll_i

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	
l_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)

if l_Errore < 0 then
   pcca.error = c_Fatal
end if

end event

