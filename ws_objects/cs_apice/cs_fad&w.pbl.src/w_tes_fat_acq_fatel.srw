﻿$PBExportHeader$w_tes_fat_acq_fatel.srw
forward
global type w_tes_fat_acq_fatel from w_std_principale
end type
type st_allegati_2 from statictext within w_tes_fat_acq_fatel
end type
type st_allegati from statictext within w_tes_fat_acq_fatel
end type
type dw_allegati from datawindow within w_tes_fat_acq_fatel
end type
type pb_pdf from picturebutton within w_tes_fat_acq_fatel
end type
type pb_archivia from picturebutton within w_tes_fat_acq_fatel
end type
type cb_5 from commandbutton within w_tes_fat_acq_fatel
end type
type dw_fattura from uo_std_dw within w_tes_fat_acq_fatel
end type
type cb_2 from commandbutton within w_tes_fat_acq_fatel
end type
end forward

global type w_tes_fat_acq_fatel from w_std_principale
integer width = 5161
integer height = 2624
event ue_set_labels ( )
st_allegati_2 st_allegati_2
st_allegati st_allegati
dw_allegati dw_allegati
pb_pdf pb_pdf
pb_archivia pb_archivia
cb_5 cb_5
dw_fattura dw_fattura
cb_2 cb_2
end type
global w_tes_fat_acq_fatel w_tes_fat_acq_fatel

type variables
str_tes_fat_acq_fatel_labels istr_labels[]
string	is_file_xml=""
string	is_file_pdf=""
long	il_fatel_idDb

string is_elastic_metada[]
string	is_fatel_index[]
string	is_archivia_allegati[]
end variables

forward prototypes
public function string wf_label (string as_label)
public subroutine wf_formatta ()
public function integer wf_getchild (pbdom_element apbdom_elem, long al_handle, long al_level, string as_level_path)
public subroutine wf_imposta_chiavi ()
public function integer wf_lista_allegati (ref string as_message)
end prototypes

event ue_set_labels();istr_labels[1].xml_label = "fatturaelettronicaheader"
istr_labels[1].visibile = "N"

istr_labels[2].xml_label = "datitrasmissione"
istr_labels[2].label = "Dati relativi alla trasmissione"

istr_labels[3].xml_label = "cedenteprestatore"
istr_labels[3].label = "Dati del cedente/prestatore"


end event

public function string wf_label (string as_label);long ll_i
for ll_i = 1 to upperbound(istr_labels)
	if lower(as_label) = istr_labels[ll_i].xml_label then
		if istr_labels[ll_i].visibile = "S" then
			// trovata label diversa
			return istr_labels[ll_i].label
		else
			// saltare
			return "!"
		end if
	end if
next
// non trovato
return "!"
end function

public subroutine wf_formatta ();string	ls_etichetta
boolean lb_titolo
long ll_livello, ll_i


dw_fattura.object.etichetta.font.Face = "Arial"
dw_fattura.object.dato.font.Face = "Arial"

for ll_i=1 to dw_fattura.rowcount()
	
	ll_livello = dw_fattura.getitemnumber(ll_i,3)
	if len(dw_fattura.getitemstring(ll_i,2)) < 1 or isnull(dw_fattura.getitemstring(ll_i,2)) then 
		lb_titolo=true
		dw_fattura.setitem(ll_i,"etichetta_bold",700)
		dw_fattura.setitem(ll_i,"flag_titolo","S")
		choose case ll_livello
			case 1
				dw_fattura.setitem(ll_i, "etichetta_size", -20)
				dw_fattura.setitem(ll_i,"etichetta_bold",700)
			case 2
				dw_fattura.setitem(ll_i, "etichetta_size", -18)
				dw_fattura.setitem(ll_i,"etichetta_bold",700)
				
			case 3
				dw_fattura.setitem(ll_i, "etichetta_size", -16)
				dw_fattura.setitem(ll_i,"etichetta_bold",700)
				
			case 4
				dw_fattura.setitem(ll_i, "etichetta_size", -14)
				dw_fattura.setitem(ll_i,"etichetta_bold",400)
				
			case else
				dw_fattura.setitem(ll_i, "etichetta_size", -12)
				dw_fattura.setitem(ll_i,"etichetta_bold",400)
	
		end choose			
	else
		lb_titolo=false
		dw_fattura.setitem(ll_i,"etichetta_bold",400)
		dw_fattura.setitem(ll_i,"flag_titolo","N")
		dw_fattura.setitem(ll_i, "etichetta_size", -12)
		dw_fattura.setitem(ll_i,"etichetta_bold",400)
		dw_fattura.setitem(ll_i, "dato_size", -12)
		dw_fattura.setitem(ll_i,"dato_bold",700)
	end if
	
next

end subroutine

public function integer wf_getchild (pbdom_element apbdom_elem, long al_handle, long al_level, string as_level_path);boolean 	lb_success, lb_attributes
string		ls_name, ls_text, ls_level
long 		ll_i, ll_cont, ll_handle, ll_row
PBDOM_Element		pbdom_element_childs[]
PBDOM_Attribute		pbdom_attributes[]


lb_success = apbdom_elem.getchildelements(pbdom_element_childs)
ls_name = apbdom_elem.getname()
for ll_i = 1 to upperbound(pbdom_element_childs)
	ls_name = pbdom_element_childs[ll_i].getname( )
	if ls_name = "Signature" or ls_name = "Allegati" then return 0
	lb_attributes = pbdom_element_childs[ll_i].hasattributes( )
	lb_success = apbdom_elem.getchildelements(pbdom_element_childs)
	if upperbound(pbdom_element_childs) > 0 then 	
		if pbdom_element_childs[ll_i].haschildelements() then
			ll_row=dw_fattura.insertrow(0)
			dw_fattura.setitem(ll_row, 1, pbdom_element_childs[ll_i].getname())
			dw_fattura.setitem(ll_row, 3, al_level)
			dw_fattura.setitem(ll_row, 8, "S")
		else
			ll_row=dw_fattura.insertrow(0)
			dw_fattura.setitem(ll_row, 1, pbdom_element_childs[ll_i].getname())
			dw_fattura.setitem(ll_row, 2, pbdom_element_childs[ll_i].gettext())
			dw_fattura.setitem(ll_row, 3, al_level)
			dw_fattura.setitem(ll_row, 8, "N")
		end if
		
		ls_level = as_level_path + "." + pbdom_element_childs[ll_i].getname()
		dw_fattura.setitem(ll_row, "level_path", ls_level)
		
		wf_getchild( pbdom_element_childs[ll_i],ll_handle, al_level + 1, ls_level )
	end if

next

return 0
end function

public subroutine wf_imposta_chiavi ();is_fatel_index[1] = "FatturaElettronica.FatturaElettronicaHeader.DatiTrasmissione.IdTrasmittente.IdPaese"
is_fatel_index[2] = "FatturaElettronica.FatturaElettronicaHeader.DatiTrasmissione.IdTrasmittente.IdCodice"
is_fatel_index[3] = "FatturaElettronica.FatturaElettronicaHeader.DatiTrasmissione.ProgressivoInvio"
is_fatel_index[4] = "FatturaElettronica.FatturaElettronicaHeader.CedentePrestatore.DatiAnagrafici.IdFiscaleIVA.IdPaese"
is_fatel_index[5] = "FatturaElettronica.FatturaElettronicaHeader.CedentePrestatore.DatiAnagrafici.IdFiscaleIVA.IdCodice"
is_fatel_index[6] = "FatturaElettronica.FatturaElettronicaHeader.CedentePrestatore.DatiAnagrafici.CodiceFiscale"
is_fatel_index[7] = "FatturaElettronica.FatturaElettronicaHeader.CedentePrestatore.DatiAnagrafici.Anagrafica.Denominazione"
is_fatel_index[8] = "FatturaElettronica.FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.TipoDocumento"
is_fatel_index[9] = "FatturaElettronica.FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.Divisa"
is_fatel_index[10] = "FatturaElettronica.FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.Data"
is_fatel_index[11] = "FatturaElettronica.FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.Numero"
is_fatel_index[12] = "FatturaElettronica.FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.ImportoTotaleDocumento"
is_fatel_index[13] = "FatturaElettronica.FatturaElettronicaBody.DatiGenerali.DatiGeneraliDocumento.Causale"

is_elastic_metada[1] = "s_idtrasmittente_idpaese"
is_elastic_metada[2] = "s_idtrasmittente_idcodice"
is_elastic_metada[3] = "s_datitx_progressivo_invio"
is_elastic_metada[4] = "s_fornitore.id_paese"
is_elastic_metada[5] = "s_fornitore.id_codice"
is_elastic_metada[6] = "s_fornitore.codice_fiscale"
is_elastic_metada[7] = "s_fornitore.denominazione"
is_elastic_metada[8] = "s_fattura.tipo_documento"
is_elastic_metada[9] = "s_fattura.divisa"
is_elastic_metada[10] = "d_fattura.data"
is_elastic_metada[11] = "s_fattura.numero"
is_elastic_metada[12] = "n_fattura.importo_totale_documento"
is_elastic_metada[13] = "s_fattura.causale"

return
end subroutine

public function integer wf_lista_allegati (ref string as_message);long 	ll_i, ll_row
uo_fatel luo_fatel
str_fatel_lista_allegati_fat_acq lstr_lista[]

luo_fatel = create uo_fatel
if luo_fatel.uof_fatture_passive_allegati_lista( il_fatel_idDb, ref lstr_lista[], ref as_message) < 0 then
	destroy luo_fatel
	return -1
end if
destroy luo_fatel

dw_allegati.reset()
if upperbound(lstr_lista) < 1 then
	dw_allegati.hide()
else
	dw_allegati.show()
end if

for ll_i = 1 to upperbound(lstr_lista)
	
	ll_row = dw_allegati.insertrow(0)
	dw_allegati.setitem(ll_row,"picturefile", s_cs_xx.volume + s_cs_xx.risorse + "12.5\attachment.png")
	dw_allegati.setitem(ll_row,"id_fattura", lstr_lista[ll_i].id_fattura)
	dw_allegati.setitem(ll_row,"id_allegato", lstr_lista[ll_i].id_allegato)
	dw_allegati.setitem(ll_row,"nome_allegato", lstr_lista[ll_i].nome_allegato)
	
next

return 0
end function

on w_tes_fat_acq_fatel.create
int iCurrent
call super::create
this.st_allegati_2=create st_allegati_2
this.st_allegati=create st_allegati
this.dw_allegati=create dw_allegati
this.pb_pdf=create pb_pdf
this.pb_archivia=create pb_archivia
this.cb_5=create cb_5
this.dw_fattura=create dw_fattura
this.cb_2=create cb_2
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_allegati_2
this.Control[iCurrent+2]=this.st_allegati
this.Control[iCurrent+3]=this.dw_allegati
this.Control[iCurrent+4]=this.pb_pdf
this.Control[iCurrent+5]=this.pb_archivia
this.Control[iCurrent+6]=this.cb_5
this.Control[iCurrent+7]=this.dw_fattura
this.Control[iCurrent+8]=this.cb_2
end on

on w_tes_fat_acq_fatel.destroy
call super::destroy
destroy(this.st_allegati_2)
destroy(this.st_allegati)
destroy(this.dw_allegati)
destroy(this.pb_pdf)
destroy(this.pb_archivia)
destroy(this.cb_5)
destroy(this.dw_fattura)
destroy(this.cb_2)
end on

event pc_setwindow;call super::pc_setwindow;boolean 	lb_success
long 		ll_i, ll_cont
string strErrorMessageArray[],ls_name, ls_errore
PBDOM_BUILDER pb_builder
PBDOM_Document    pbdom_doc	
PBDOM_Element     pbdom_elem[], pbdom_element_childs[]

pb_archivia.picturename=guo_functions.uof_risorse("12.5/vault-01.png")
pb_pdf.picturename=guo_functions.uof_risorse("12.5/pdf.png")
dw_fattura.ib_dw_report=true
wf_imposta_chiavi()

is_file_xml = s_cs_xx.parametri.parametro_s_15
is_file_pdf = s_cs_xx.parametri.parametro_s_14
il_fatel_idDb = long(s_cs_xx.parametri.parametro_ul_1)

dw_fattura.setredraw(false)
event ue_set_labels()

pb_builder = create PBDOM_BUILDER

s_cs_xx.parametri.parametro_s_15 = ""
s_cs_xx.parametri.parametro_s_14 = ""
s_cs_xx.parametri.parametro_s_13 = ""

pbdom_doc = pb_builder.buildfromfile( is_file_xml  )
pb_builder.GetParseErrors(ref  strErrorMessageArray[])
pbdom_elem[1] = pbdom_doc.GetRootElement()
ls_name = pbdom_elem[1].getname()
lb_success = pbdom_doc.getelementsbytagname("FatturaElettronica",pbdom_elem[])

wf_getchild(pbdom_elem[1],0, 1, "FatturaElettronica")


destroy pbdom_doc
destroy pb_builder

wf_formatta()

if wf_lista_allegati(ls_errore) < 0 then
	g_mb.error("Errore in ricerca Allegati: " + ls_errore)
end if

st_allegati. text = "Gli allegati vengono scaricati nella cartella FatturePassive della cartella documenti dell'utente. ("  + guo_functions.uof_get_user_documents_folder( ) + "FatturePassive\)"

dw_fattura.setredraw(true)

end event

type st_allegati_2 from statictext within w_tes_fat_acq_fatel
integer x = 23
integer y = 2440
integer width = 5074
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
string text = "SOLO GLI ALLEGATI SCARICATI VERRANNO ARCHIVIATI."
alignment alignment = right!
boolean focusrectangle = false
end type

type st_allegati from statictext within w_tes_fat_acq_fatel
integer x = 23
integer y = 2380
integer width = 5074
integer height = 60
integer textsize = -8
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 67108864
string text = "Gli allegati vengono salvati in ..."
alignment alignment = right!
boolean focusrectangle = false
end type

type dw_allegati from datawindow within w_tes_fat_acq_fatel
integer x = 3977
integer y = 20
integer width = 1120
integer height = 2140
integer taborder = 40
string title = "none"
string dataobject = "d_tes_fat_acq_fatel_allegati_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event buttonclicked;if dwo.name="b_download" then
	
	string		ls_message, ls_file_ext, ls_file_code, ls_nomeallegato, ls_filelocation, ls_final_path, ls_cartella_fornitore
	long 		ll_id_allegato, ll_ret, ll_i
	blob		lb_blob
	uo_fatel 	luo_fatel
	uo_shellexecute luo_exec
	
	
	
	luo_fatel = create uo_fatel
	
	ll_id_allegato = long(getitemstring(row,"id_allegato"))
	
	ll_ret = luo_fatel.uof_fattura_passiva_download_allegato( ll_id_allegato, ls_message, lb_blob, ls_file_ext, ls_file_code)
	
	destroy luo_fatel
	
	ls_nomeallegato = getitemstring(row,"nome_allegato")
	
	ls_filelocation = guo_functions.uof_get_user_documents_folder( )
	ls_final_path = ls_filelocation + "FatturePassive\"
	
	if not directoryExists(ls_final_path) then
		if CreateDirectory ( ls_final_path ) = -1 then
			g_mb.error("Errore nella creazione della cartella " + ls_final_path)
		end if
	end if
	
	ls_cartella_fornitore = ""
	ll_ret = dw_fattura.find( "level_path='FatturaElettronica.FatturaElettronicaHeader.DatiTrasmissione.IdTrasmittente.IdPaese'", 1, dw_fattura.rowcount())
	ls_cartella_fornitore = dw_fattura.getitemstring(ll_ret, "dato")
	
	ll_ret = dw_fattura.find( "level_path='FatturaElettronica.FatturaElettronicaHeader.DatiTrasmissione.IdTrasmittente.IdCodice'", 1, dw_fattura.rowcount())
	ls_cartella_fornitore += dw_fattura.getitemstring(ll_ret, "dato")
	
//	ll_ret = dw_fattura.find( "level_path='FatturaElettronica.FatturaElettronicaHeader.CedentePrestatore.DatiAnagrafici.Anagrafica.Denominazione'", 1, dw_fattura.rowcount())
//	ls_cartella_fornitore += dw_fattura.getitemstring(ll_ret, "dato")
	
	ls_final_path += ls_cartella_fornitore + "\"
	if not directoryExists(ls_final_path) then
		if CreateDirectory ( ls_final_path ) = -1 then
			g_mb.error("Errore nella creazione della cartella " + ls_final_path)
		end if
	end if
	
	ls_final_path = ls_final_path + ls_nomeallegato
	
	if FileExists(ls_final_path) then
		if g_mb.confirm( "File Xml Fattura Elettronica", "Il file selezionato esiste già nella cartella di destinazione: lo sovrascrivo?", 1) then 
			filedelete(ls_final_path)
		else
			return
		end if
	end if
	
	guo_functions.uof_blob_to_file( lb_blob, ls_final_path)
	
	ll_i = upperbound(is_archivia_allegati) + 1
	
	is_archivia_allegati[ll_i] = ls_final_path
	
	luo_exec=Create uo_shellexecute
	
	ll_ret = luo_exec.uof_run(handle(parent),  ls_final_path)
	if ll_ret <= 32 then
		g_mb.messagebox("Attenzione", "Impossibile visualizzare il documento ", exclamation!, ok!)		
	end if		
	
	destroy luo_exec
	
end if
end event

type pb_pdf from picturebutton within w_tes_fat_acq_fatel
integer x = 4617
integer y = 2180
integer width = 229
integer height = 200
integer taborder = 50
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string picturename = "C:\cs_125\risorse\12.5\pdf.png"
alignment htextalign = left!
end type

event clicked;long	ll_ret
uo_shellexecute luo_exec


luo_exec=Create uo_shellexecute

ll_ret = luo_exec.uof_run(handle(parent), is_file_pdf)
if ll_ret <= 32 then
	g_mb.messagebox("Attenzione", "Impossibile visualizzare il documento ", exclamation!, ok!)		
end if		

destroy luo_exec

end event

type pb_archivia from picturebutton within w_tes_fat_acq_fatel
integer x = 4869
integer y = 2180
integer width = 229
integer height = 200
integer taborder = 40
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string picturename = "C:\cs_125\risorse\12.5\Vault-01.png"
alignment htextalign = left!
string powertiptext = "Archivia ed Elimina dalla lista"
long backcolor = 553648127
end type

event clicked;string ls_str, ls_elastic_metada, ls_dato, ls_file_dir, ls_filename, ls_file_ext, ls_file_elastic, ls_messaggio, ls_local_metadata, ls_elastic, ls_mimetype
long ll_ret, ll_i, ll_seconds, ll_identity
blob	lb_blob
uo_fatel luo_fatel

guo_functions.uof_get_parametro_azienda( "ELD", ls_elastic)
if isnull(ls_elastic) or len(ls_elastic)<0 then ls_elastic="N"

if ls_elastic="S" then
	if g_mb.confirm( "Fattura Elettronica Passiva", "Confermi l'archiviazione della fattura e la sua eliminazione dalla lista SDI di Apice?", 2) = false then return
else
	if g_mb.confirm( "Fattura Elettronica Passiva", "Confermi l'eliminazione dalla lista SDI di Apice?", 2) = false then return
end if

if ls_elastic="S" then
	ls_elastic_metada = ""
	for ll_i = 1 to upperbound(is_fatel_index)
	
		ll_ret = dw_fattura.find( "level_path='" + is_fatel_index[ll_i] + "'", 1, dw_fattura.rowcount())
		ls_str = dw_fattura.getitemstring(ll_ret, "dato")
		
		// elimino eventuali simboli di uguale
		ls_str= g_str.replaceall( ls_str, "=", "", true)
		
		choose case ll_i
				// string
			case 1,2,3,4,5,6,7,8,9,13
				ls_elastic_metada += is_elastic_metada[ll_i] + "=" + ls_str + char(13) + char(10)
				//number
			case 11,12
				ls_str= g_str.replaceall( ls_str, ",", ".", true)
				ls_elastic_metada += is_elastic_metada[ll_i] + "=" + ls_str + char(13) + char(10)
				//date
			case 10
				
				ll_seconds = guo_functions.uof_string_to_date_epoc(ls_str)
				ls_elastic_metada += is_elastic_metada[ll_i] + "=" + string(ll_seconds) + char(13) + char(10)
		end choose
	next
	ls_elastic_metada += "s_protocollo_iva=Progressivo Mancante"  + char(13) + char(10)
	
	// metto via il file PDF
	guo_functions.uof_get_file_info( is_file_pdf, ls_file_dir, ls_filename, ls_file_ext)
	ls_file_elastic = ls_filename + "." + ls_file_ext
	
	ls_local_metadata = "s_file_name=" + ls_file_elastic + char(13) + char(10) + ls_elastic_metada
	ls_local_metadata += "s_file_mimetype.estensione=pdf"  + char(13) + char(10)
	ls_local_metadata += "s_file_mimetype.mimetype=application/pdf" 
	
	
	INSERT INTO elastic_document
			( file_name,   
			  file_metadata,   
			  elastic_index,   
			  elastic_type,   
			  flag_elaborato )  
	VALUES ( :ls_file_elastic,   
			  :ls_local_metadata,   
			  'omnia-fattura-passiva',   
			  'application/pdf',   
			  'N')  ;
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore in Archiviazione metadati documento." + sqlca.sqlerrtext )
		rollback;
		return
	end if
	
	guo_functions.uof_get_identity( sqlca, ll_identity)
	
	guo_functions.uof_file_to_blob( is_file_pdf , lb_blob)
	
	updateblob elastic_document
	set file_data = :lb_blob 
	where id = :ll_identity;
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore in Archiviazione dati documento." + sqlca.sqlerrtext )
		rollback;
		return
	end if
	
	commit;
	
	// metto via il file XML
	guo_functions.uof_get_file_info( is_file_xml, ls_file_dir, ls_filename, ls_file_ext)
	ls_file_elastic = ls_filename + ".xml"
	
	ls_local_metadata = "s_file_name=" + ls_file_elastic + char(13) + char(10) + ls_elastic_metada
	ls_local_metadata += "s_file_mimetype.estensione=xml"  + char(13) + char(10)
	ls_local_metadata += "s_file_mimetype.mimetype=text/xml" 
	
	INSERT INTO elastic_document
			( file_name,   
			  file_metadata,   
			  elastic_index,   
			  elastic_type,   
			  flag_elaborato )  
	VALUES ( :ls_file_elastic,   
			  :ls_local_metadata,   
			  'omnia-fattura-passiva',   
			  'text/xml',   
			  'N')  ;
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore in Archiviazione metadati documento." + sqlca.sqlerrtext )
		rollback;
		return
	end if
	
	guo_functions.uof_get_identity( sqlca, ll_identity)
	
	guo_functions.uof_file_to_blob( is_file_xml , lb_blob)
	
	updateblob elastic_document
	set file_data = :lb_blob 
	where id = :ll_identity;
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore in Archiviazione dati documento." + sqlca.sqlerrtext )
		rollback;
		return
	end if
	
	commit;
	
	// metto via gli allegati
	for ll_i = 1 to upperbound(is_archivia_allegati)
		guo_functions.uof_get_file_info( is_archivia_allegati[ll_i], ls_file_dir, ls_filename, ls_file_ext)
		ls_file_elastic = ls_filename + "." + ls_file_ext
		
		ls_mimetype = guo_functions.uof_get_file_mimetype( ls_file_ext)
		
		ls_local_metadata = "s_file_name=" + ls_file_elastic + char(13) + char(10) + ls_elastic_metada
		ls_local_metadata += g_str.format("s_file_mimetype.estensione=$1",ls_file_ext)  + char(13) + char(10)
		ls_local_metadata += "s_file_mimetype.mimetype="  + ls_mimetype
		
		INSERT INTO elastic_document
				( file_name,   
				  file_metadata,   
				  elastic_index,   
				  elastic_type,   
				  flag_elaborato )  
		VALUES ( :ls_file_elastic,   
				  :ls_local_metadata,   
				  'omnia-fattura-passiva',   
				  :ls_mimetype,   
				  'N')  ;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in Archiviazione metadati documento." + sqlca.sqlerrtext )
			rollback;
			return
		end if
		
		guo_functions.uof_get_identity( sqlca, ll_identity)
		
		guo_functions.uof_file_to_blob(  is_archivia_allegati[ll_i] , lb_blob)
		
		updateblob elastic_document
		set file_data = :lb_blob 
		where id = :ll_identity;
		if sqlca.sqlcode < 0 then
			g_mb.error("Errore in Archiviazione dati documento." + sqlca.sqlerrtext )
			rollback;
			return
		end if
	next
	commit;
end if

luo_fatel = create uo_fatel
ll_ret = luo_fatel.uof_conferma_fattura_passiva( il_fatel_idDb, ref ls_messaggio)
if ll_ret < 0 then
	g_mb.warning("Errore in Conferma Archiviazione fattura.~r~n"+ ls_messaggio)
	rollback;
	return
end if


/*
Archiviazione in ELASTIC della fattura passiva
INDEX: OMNIA_FATTURA_PASSIVA
IdTrasmittente_IdPaese  STRING
IdTrasmittente_IdCodice STRING
DatiTrasmissione_ProgressivoInvio  STRING

Fornitore_IdPaese  STRING
Fornitore_IdCodice  STRING
Fornitore_CodiceFiscale STRING
Fornitore_Denominazione STRING

Fattura_TipoDocumento STRING
Fattura_Divisa STRING
Fattura_Data DATE EPOC
Fattura_Numero STRING
Fattura_ImportoTotaleDocumento NUMBER
Fattura_Causale STRING

Protocollo_Iva STRING

*/
end event

type cb_5 from commandbutton within w_tes_fat_acq_fatel
boolean visible = false
integer x = 3794
integer y = 600
integer width = 402
integer height = 112
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Archivia"
end type

event clicked;string ls_str, ls_elastic_metada, ls_dato, ls_file_dir, ls_filename, ls_file_ext, ls_file_elastic, ls_messaggio, ls_local_metadata
long ll_ret, ll_i, ll_seconds, ll_identity
blob	lb_blob
uo_fatel luo_fatel

ls_elastic_metada = ""
for ll_i = 1 to upperbound(is_fatel_index)

	ll_ret = dw_fattura.find( "level_path='" + is_fatel_index[ll_i] + "'", 1, dw_fattura.rowcount())
	ls_str = dw_fattura.getitemstring(ll_ret, "dato")
	
	// elimino eventuali simboli di uguale
	ls_str= g_str.replaceall( ls_str, "=", "", true)
	
	choose case ll_i
			// string
		case 1,2,3,4,5,6,7,8,9,13
			ls_elastic_metada += is_elastic_metada[ll_i] + "=" + ls_str + char(13) + char(10)
			//number
		case 11,12
			ls_str= g_str.replaceall( ls_str, ",", ".", true)
			ls_elastic_metada += is_elastic_metada[ll_i] + "=" + ls_str + char(13) + char(10)
			//date
		case 10
			
			ll_seconds = guo_functions.uof_string_to_date_epoc(ls_str)
			/*
			long ll_seconds
			datetime  ldt_datetime
			ldt_datetime = datetime(date(ls_str), 00:00:00)
			
			select DATEDIFF(SECOND, '19700101', :ldt_datetime)
			into	:ll_seconds
			from	aziende;
			
//			SELECT UTC_TO_DATE (1463533832) FROM DUAL
//			SELECT date_to_utc(creation_date) FROM mytable
			*/
			ls_elastic_metada += is_elastic_metada[ll_i] + "=" + string(ll_seconds) + char(13) + char(10)
	end choose
next
ls_elastic_metada += "Protocollo_Iva=ProgressivoMancante"

// metto via il file PDF
guo_functions.uof_get_file_info( is_file_pdf, ls_file_dir, ls_filename, ls_file_ext)
ls_file_elastic = ls_filename + "." + ls_file_ext

ls_local_metadata = "Nome_File=" + ls_file_elastic + char(13) + char(10) + ls_elastic_metada


INSERT INTO elastic_document
		( file_name,   
		  file_metadata,   
		  elastic_index,   
		  elastic_type,   
		  flag_elaborato )  
VALUES ( :ls_file_elastic,   
		  :ls_local_metadata,   
		  'omnia-fattura-passiva',   
		  'application/pdf',   
		  'N')  ;
if sqlca.sqlcode < 0 then
	g_mb.error("Errore in Archiviazione metadati documento." + sqlca.sqlerrtext )
	rollback;
	return
end if

guo_functions.uof_get_identity( sqlca, ll_identity)

guo_functions.uof_file_to_blob( is_file_pdf , lb_blob)

updateblob elastic_document
set file_data = :lb_blob 
where id = :ll_identity;
if sqlca.sqlcode < 0 then
	g_mb.error("Errore in Archiviazione dati documento." + sqlca.sqlerrtext )
	rollback;
	return
end if

commit;

// metto via il file XML
guo_functions.uof_get_file_info( is_file_xml, ls_file_dir, ls_filename, ls_file_ext)
ls_file_elastic = ls_filename + ".xml"

ls_local_metadata = "Nome_File=" + ls_file_elastic + char(13) + char(10) + ls_elastic_metada

INSERT INTO elastic_document
		( file_name,   
		  file_metadata,   
		  elastic_index,   
		  elastic_type,   
		  flag_elaborato )  
VALUES ( :ls_file_elastic,   
		  :ls_local_metadata,   
		  'omnia-fattura-passiva',   
		  'text/xml',   
		  'N')  ;
if sqlca.sqlcode < 0 then
	g_mb.error("Errore in Archiviazione metadati documento." + sqlca.sqlerrtext )
	rollback;
	return
end if

guo_functions.uof_get_identity( sqlca, ll_identity)

guo_functions.uof_file_to_blob( is_file_xml , lb_blob)

updateblob elastic_document
set file_data = :lb_blob 
where id = :ll_identity;
if sqlca.sqlcode < 0 then
	g_mb.error("Errore in Archiviazione dati documento." + sqlca.sqlerrtext )
	rollback;
	return
end if

commit;

luo_fatel = create uo_fatel
ll_ret = luo_fatel.uof_conferma_fattura_passiva( il_fatel_idDb, ref ls_messaggio)
if ll_ret < 0 then
	g_mb.warning("Errore in Conferma Archiviazione fattura.~r~n"+ ls_messaggio)
	rollback;
	return
end if
	




/*
Archiviazione in ELASTIC della fattura passiva
INDEX: OMNIA_FATTURA_PASSIVA
IdTrasmittente_IdPaese  STRING
IdTrasmittente_IdCodice STRING
DatiTrasmissione_ProgressivoInvio  STRING

Fornitore_IdPaese  STRING
Fornitore_IdCodice  STRING
Fornitore_CodiceFiscale STRING
Fornitore_Denominazione STRING

Fattura_TipoDocumento STRING
Fattura_Divisa STRING
Fattura_Data DATE EPOC
Fattura_Numero STRING
Fattura_ImportoTotaleDocumento NUMBER
Fattura_Causale STRING

Protocollo_Iva STRING

*/
end event

type dw_fattura from uo_std_dw within w_tes_fat_acq_fatel
integer x = 23
integer y = 20
integer width = 3954
integer height = 2360
integer taborder = 40
string dataobject = "d_tes_fat_acq_fatel"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

type cb_2 from commandbutton within w_tes_fat_acq_fatel
boolean visible = false
integer x = 4000
integer y = 640
integer width = 402
integer height = 112
integer taborder = 30
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Formatta"
end type

event clicked;string	ls_etichetta
boolean lb_titolo
long ll_livello, ll_i


dw_fattura.setredraw(false)
dw_fattura.object.etichetta.font.Face = "Arial"
dw_fattura.object.dato.font.Face = "Arial"

for ll_i=1 to dw_fattura.rowcount()
	
	ll_livello = dw_fattura.getitemnumber(ll_i,3)
	if len(dw_fattura.getitemstring(ll_i,2)) < 1 or isnull(dw_fattura.getitemstring(ll_i,2)) then 
		lb_titolo=true
		dw_fattura.setitem(ll_i,"etichetta_bold",700)
		dw_fattura.setitem(ll_i,"flag_titolo","S")
		choose case ll_livello
			case 1
				dw_fattura.setitem(ll_i, "etichetta_size", -20)
				dw_fattura.setitem(ll_i,"etichetta_bold",700)
			case 2
				dw_fattura.setitem(ll_i, "etichetta_size", -18)
				dw_fattura.setitem(ll_i,"etichetta_bold",700)
				
			case 3
				dw_fattura.setitem(ll_i, "etichetta_size", -16)
				dw_fattura.setitem(ll_i,"etichetta_bold",700)
				
			case 4
				dw_fattura.setitem(ll_i, "etichetta_size", -14)
				dw_fattura.setitem(ll_i,"etichetta_bold",400)
				
			case else
				dw_fattura.setitem(ll_i, "etichetta_size", -12)
				dw_fattura.setitem(ll_i,"etichetta_bold",400)
	
		end choose			
	else
		lb_titolo=false
		dw_fattura.setitem(ll_i,"etichetta_bold",400)
		dw_fattura.setitem(ll_i,"flag_titolo","N")
		dw_fattura.setitem(ll_i, "etichetta_size", -12)
		dw_fattura.setitem(ll_i,"etichetta_bold",400)
		dw_fattura.setitem(ll_i, "dato_size", -12)
		dw_fattura.setitem(ll_i,"dato_bold",700)
	end if
	
next
dw_fattura.setredraw(true)
dw_fattura.ib_dw_report=true

end event

