﻿$PBExportHeader$w_tes_fat_acq_lista.srw
$PBExportComments$Finestra Testata Fatture Acquisto
forward
global type w_tes_fat_acq_lista from w_cs_xx_principale
end type
type b_cancella_filtro from picture within w_tes_fat_acq_lista
end type
type b_memorizza_filtro from picture within w_tes_fat_acq_lista
end type
type b_leggi_filtro from picture within w_tes_fat_acq_lista
end type
type dw_folder from u_folder within w_tes_fat_acq_lista
end type
type dw_tes_fat_acq_det_2 from uo_cs_xx_dw within w_tes_fat_acq_lista
end type
type dw_tes_fat_acq_det_1 from uo_cs_xx_dw within w_tes_fat_acq_lista
end type
type cb_1 from commandbutton within w_tes_fat_acq_lista
end type
type cb_calcola from commandbutton within w_tes_fat_acq_lista
end type
type cb_carico_bolle from commandbutton within w_tes_fat_acq_lista
end type
type cb_carico_ordini from commandbutton within w_tes_fat_acq_lista
end type
type cb_conferma_bolla from commandbutton within w_tes_fat_acq_lista
end type
type cb_contro from commandbutton within w_tes_fat_acq_lista
end type
type cb_corrispondenze from commandbutton within w_tes_fat_acq_lista
end type
type cb_dettaglio from commandbutton within w_tes_fat_acq_lista
end type
type cb_iva from commandbutton within w_tes_fat_acq_lista
end type
type cb_note from commandbutton within w_tes_fat_acq_lista
end type
type cb_numero_doc from commandbutton within w_tes_fat_acq_lista
end type
type cb_ricerca from commandbutton within w_tes_fat_acq_lista
end type
type cb_scadenze from commandbutton within w_tes_fat_acq_lista
end type
type cb_stampa from commandbutton within w_tes_fat_acq_lista
end type
type dw_notifiche from uo_std_dw within w_tes_fat_acq_lista
end type
type dw_tes_fat_acq_sel from u_dw_search within w_tes_fat_acq_lista
end type
type dw_folder_search from u_folder within w_tes_fat_acq_lista
end type
type tv_1 from treeview within w_tes_fat_acq_lista
end type
type ws_record from structure within w_tes_fat_acq_lista
end type
end forward

type ws_record from structure
	long		anno_registrazione
	long		num_registrazione
	string		descrizione_2
	string		codice
	string		descrizione
	long		handle_padre
	string		tipo_livello
	integer		livello
end type

global type w_tes_fat_acq_lista from w_cs_xx_principale
integer width = 4430
integer height = 2336
string title = "Fatture di Acquisto"
b_cancella_filtro b_cancella_filtro
b_memorizza_filtro b_memorizza_filtro
b_leggi_filtro b_leggi_filtro
dw_folder dw_folder
dw_tes_fat_acq_det_2 dw_tes_fat_acq_det_2
dw_tes_fat_acq_det_1 dw_tes_fat_acq_det_1
cb_1 cb_1
cb_calcola cb_calcola
cb_carico_bolle cb_carico_bolle
cb_carico_ordini cb_carico_ordini
cb_conferma_bolla cb_conferma_bolla
cb_contro cb_contro
cb_corrispondenze cb_corrispondenze
cb_dettaglio cb_dettaglio
cb_iva cb_iva
cb_note cb_note
cb_numero_doc cb_numero_doc
cb_ricerca cb_ricerca
cb_scadenze cb_scadenze
cb_stampa cb_stampa
dw_notifiche dw_notifiche
dw_tes_fat_acq_sel dw_tes_fat_acq_sel
dw_folder_search dw_folder_search
tv_1 tv_1
end type
global w_tes_fat_acq_lista w_tes_fat_acq_lista

type variables
boolean ib_apertura = false, ib_nuovo = false

long		il_numero_riga, il_livello_corrente, il_handle, il_anno_registrazione, il_num_registrazione,&
			il_handle_modificato, il_handle_cancellato

//treeviewitem tvi_campo

boolean ib_tree_deleting = false, ib_retrieve=true

string is_sql_filtro
string is_cod_filtro_memorizza
string	is_cod_parametro_blocco="BFA"
end variables

forward prototypes
public subroutine wf_imposta_treeview ()
public subroutine wf_cancella_treeview ()
public function integer wf_inserisci_fornitori (long fl_handle, string fs_cod_fornitore)
public function integer wf_inserisci_tipi_fatture_acquisto (long fl_handle, string fs_cod_tipo_fat_acq)
public function integer wf_inserisci_anni (long fl_handle, long fl_anno)
public function integer wf_inserisci_fatture (long fl_handle, string fs_cod)
public function integer wf_leggi_parent (long fl_handle, ref string fs_sql)
public function integer wf_inserisci_singola_fattura (long fl_handle, long fl_anno, long fl_numero)
public subroutine wf_memorizza_filtro ()
public subroutine wf_leggi_filtro ()
public function integer wf_assegna_num_fiscale (long ai_row, integer ai_anno_reg, long al_num_reg, ref string as_errore)
end prototypes

public subroutine wf_imposta_treeview ();string ls_null, ls_cod_attrezzatura, ls_des_attrezzatura
long ll_risposta, ll_1, ll_2, ll_3, ll_i, ll_tvi, ll_root, ll_anno_registrazione, ll_num_registrazione
ws_record lstr_record
treeviewitem ltvi_campo

setnull(ls_null)

ltvi_campo.expanded = false
ltvi_campo.selected = false
ltvi_campo.children = false	//altrimenti mostrava il segno di + anche accanto agli elementi senza sottoelementi

ll_i = 0
setpointer(HourGlass!)

tv_1.setredraw(false)

ll_anno_registrazione = dw_tes_fat_acq_sel.getitemnumber(1,"anno_registrazione")
ll_num_registrazione = dw_tes_fat_acq_sel.getitemnumber(1,"num_registrazione")

// ----------------- se è stata richiesta una specifica registrazione propongo solo quella e niente altro------------------
if ll_anno_registrazione > 0 and not isnull(ll_anno_registrazione) and &
		ll_num_registrazione > 0 and not isnull(ll_num_registrazione) then
	ib_retrieve=true
	wf_inserisci_singola_fattura(0, ll_anno_registrazione, ll_num_registrazione )
	tv_1.setredraw(true)
	return
end if

il_livello_corrente = 0

// controlla cosa c'è al livello successivo
choose case dw_tes_fat_acq_sel.getitemstring(1,"flag_tipo_livello_" + string(il_livello_corrente + 1))
	case "T"
		//caricamento tipo fattura acquisto
		wf_inserisci_tipi_fatture_acquisto(0, dw_tes_fat_acq_sel.getitemstring(1,"cod_tipo_fat_acq"))
		
	case "A"
		//caricamento anni
		wf_inserisci_anni(0, dw_tes_fat_acq_sel.getitemnumber(1,"anno_registrazione"))

	case "F"
		//caricamento fornitori
		wf_inserisci_fornitori(0, dw_tes_fat_acq_sel.getitemstring(1,"cod_fornitore"))
		
	case "N" 
		wf_inserisci_fatture(0, "")
end choose

tv_1.setredraw(true)

setpointer(arrow!)
return
end subroutine

public subroutine wf_cancella_treeview ();ib_tree_deleting = true
tv_1.deleteitem(0)
ib_tree_deleting = false
end subroutine

public function integer wf_inserisci_fornitori (long fl_handle, string fs_cod_fornitore);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello fornitori
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean  		lb_dati = false
string    			ls_sql, ls_cod_fornitore, ls_des_fornitore, ls_null, ls_appoggio, ls_sql_livello, ls_padre
long      			ll_risposta, ll_i, ll_livello
ws_record 	lstr_record

treeviewitem ltvi_campo

setnull(ls_null)

setpointer(Hourglass!)

ll_livello = il_livello_corrente + 1

ls_sql = "SELECT cod_fornitore, rag_soc_1 FROM anag_fornitori WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_fornitore)  and len(fs_cod_fornitore) > 0 then
	ls_sql += " and cod_fornitore = '" + fs_cod_fornitore + "' "
end if

ls_sql_livello = ""
ls_padre = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_fornitore in (select cod_fornitore from tes_fat_acq where cod_azienda = '" + s_cs_xx.cod_azienda + "'" + ls_sql_livello + ") "
end if

if len(is_sql_filtro) > 0 then
	ls_sql += " and cod_fornitore in (select cod_fornitore from tes_fat_acq where cod_azienda = '" + s_cs_xx.cod_azienda + "'" + is_sql_filtro + ") "
else
	ls_sql += " and cod_fornitore in (select cod_fornitore from tes_fat_acq where cod_azienda = '" + s_cs_xx.cod_azienda + "' )"
end if

//if ll_livello > 1 then
//	//ls_padre = wf_leggi_parent(fl_handle)
//end if
//
//if ls_padre = "E" then
//	//wf_leggi_parent(fl_handle,ls_sql_livello)
//end if
//
//if len(trim(ls_sql_livello)) > 0 then
//	ls_sql += " and cod_divisione IN ( select cod_divisione from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "'  " + ls_sql_livello + " ) "
//end if
//
//if is_tipo_ordinamento = "C" then
//	ls_sql = ls_sql + "ORDER BY cod_fornitore"
//else
//	ls_sql = ls_sql + "ORDER BY cod_fornitore"
//end if
ls_sql = ls_sql + "ORDER BY rag_soc_1 asc "

declare cu_fornitori dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic cu_fornitori;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_fornitori: " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	setpointer(Arrow!)
	close cu_fornitori;
	return 0
end if

do while 1=1
   fetch cu_fornitori into :ls_cod_fornitore, 
								   :ls_des_fornitore;
									
   if (sqlca.sqlcode = 100) then
		close cu_fornitori;
		if lb_dati then
			setpointer(Arrow!)
			return 0
		end if
		setpointer(Arrow!)
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_fornitori (w_tes_fat_acq_lista:wf_inserisci_fornitori)~r~n" + sqlca.sqlerrtext)
		setpointer(Arrow!)
		close cu_fornitori;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_fornitore
	lstr_record.descrizione = ls_des_fornitore
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	//setnull(lstr_record.des_manutenzione)
	lstr_record.tipo_livello = "F"
	ltvi_campo.data = lstr_record
	
//	if is_tipo_ordinamento = "D" then
//		tvi_campo.label = ls_des_divisione + ", " +  ls_cod_divisione
//	else
//		tvi_campo.label = ls_cod_divisione + ", " + ls_des_divisione
//	end if
	ltvi_campo.label = ls_des_fornitore + ", " +  ls_cod_fornitore

	ltvi_campo.pictureindex = 1
	ltvi_campo.selectedpictureindex = 1
	ltvi_campo.overlaypictureindex = 1
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento fornitori nel TREEVIEW al livello FORNITORI")
		setpointer(Arrow!)
		close cu_fornitori;
		return 0
	end if

loop
close cu_fornitori;
setpointer(Arrow!)

return 0
end function

public function integer wf_inserisci_tipi_fatture_acquisto (long fl_handle, string fs_cod_tipo_fat_acq);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello tipi fatture acquisto
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean   lb_dati = false
string    ls_sql, ls_cod_tipo_fat_acq, ls_des_tipo_fat_acq, ls_null, ls_appoggio, ls_sql_livello, ls_padre
long      ll_risposta, ll_i, ll_livello
ws_record lstr_record
treeviewitem ltvi_campo

setnull(ls_null)

ll_livello = il_livello_corrente + 1

ls_sql = "SELECT cod_tipo_fat_acq, des_tipi_fat_acq FROM tab_tipi_fat_acq WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fs_cod_tipo_fat_acq)  and len(fs_cod_tipo_fat_acq) > 0 then
	ls_sql += " and cod_tipo_fat_acq = '" + fs_cod_tipo_fat_acq + "' "
end if

// *** michela 19/07/2005: nel caso delle divisioni non mi interessa mettere alcuna where in più rispetto al padre ( ma solo nel caso delle aree)
ls_sql_livello = ""
ls_padre = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and cod_tipo_fat_acq in (select cod_tipo_fat_acq from tes_fat_acq where cod_azienda = '" + s_cs_xx.cod_azienda + "'" + ls_sql_livello + ") "
end if

if len(is_sql_filtro) > 0 then
	ls_sql += " and cod_tipo_fat_acq in (select cod_tipo_fat_acq from tes_fat_acq where cod_azienda = '" + s_cs_xx.cod_azienda + "'" + is_sql_filtro + ") "
else
	ls_sql += " and cod_tipo_fat_acq in (select cod_tipo_fat_acq from tes_fat_acq where cod_azienda = '" + s_cs_xx.cod_azienda + "' )"
end if

//if ll_livello > 1 then
//	//ls_padre = wf_leggi_parent(fl_handle)
//end if
//
//if ls_padre = "E" then
//	//wf_leggi_parent(fl_handle,ls_sql_livello)
//end if
//
//if len(trim(ls_sql_livello)) > 0 then
//	ls_sql += " and cod_divisione IN ( select cod_divisione from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "'  " + ls_sql_livello + " ) "
//end if
//
//if is_tipo_ordinamento = "C" then
//	ls_sql = ls_sql + "ORDER BY cod_fornitore"
//else
//	ls_sql = ls_sql + "ORDER BY cod_fornitore"
//end if
ls_sql = ls_sql + "ORDER BY des_tipi_fat_acq asc "

declare cu_tipi_fat_acq dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic cu_tipi_fat_acq;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_tipi_fat_acq: " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_tipi_fat_acq;
	return 0
end if

do while 1=1
   fetch cu_tipi_fat_acq into :ls_cod_tipo_fat_acq, 
								   :ls_des_tipo_fat_acq;
									
   if (sqlca.sqlcode = 100) then
		close cu_tipi_fat_acq;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_tipi_fat_acq (w_tes_fat_acq_lista:wf_inserisci_tipi_fatture_acquisto)~r~n" + sqlca.sqlerrtext)
		close cu_tipi_fat_acq;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = ls_cod_tipo_fat_acq
	lstr_record.descrizione = ls_des_tipo_fat_acq
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	//setnull(lstr_record.des_manutenzione)
	lstr_record.tipo_livello = "T"
	ltvi_campo.data = lstr_record
	
//	if is_tipo_ordinamento = "D" then
//		tvi_campo.label = ls_des_divisione + ", " +  ls_cod_divisione
//	else
//		tvi_campo.label = ls_cod_divisione + ", " + ls_des_divisione
//	end if
	ltvi_campo.label = ls_cod_tipo_fat_acq + ", " +  ls_des_tipo_fat_acq

	ltvi_campo.pictureindex = 3
	ltvi_campo.selectedpictureindex = 3
	ltvi_campo.overlaypictureindex = 3
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento tipi fatture acquisto nel TREEVIEW al livello TIPi FATTURE ACQUISTO")
		close cu_tipi_fat_acq;
		return 0
	end if

loop
close cu_tipi_fat_acq;

ll_risposta = tv_1.FindItem(RootTreeItem! , 0)
if ll_risposta > 0 then tv_1.ExpandItem(ll_risposta)

return 0

end function

public function integer wf_inserisci_anni (long fl_handle, long fl_anno);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello tipi fatture acquisto
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean   lb_dati = false
string    ls_sql, ls_null, ls_appoggio, ls_sql_livello, ls_padre
long      ll_risposta, ll_i, ll_livello, ll_anno
ws_record lstr_record
treeviewitem ltvi_campo

setnull(ls_null)

ll_livello = il_livello_corrente + 1

ls_sql = "SELECT distinct anno_registrazione FROM tes_fat_acq WHERE cod_azienda = '" + s_cs_xx.cod_azienda + "' "
if not isnull(fl_anno)  and fl_anno > 0 then
	ls_sql += " and anno_registrazione=" + string(fl_anno) + " "
end if

// *** michela 19/07/2005: nel caso delle divisioni non mi interessa mettere alcuna where in più rispetto al padre ( ma solo nel caso delle aree)
ls_sql_livello = ""
ls_padre = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql += " and anno_registrazione in (select anno_registrazione from tes_fat_acq where cod_azienda = '" + s_cs_xx.cod_azienda + "'" + ls_sql_livello + ") "
end if

if len(is_sql_filtro) > 0 then
	ls_sql += " and anno_registrazione in (select anno_registrazione from tes_fat_acq where cod_azienda = '" + s_cs_xx.cod_azienda + "'" + is_sql_filtro + ") "
else
	ls_sql += " and anno_registrazione in (select anno_registrazione from tes_fat_acq where cod_azienda = '" + s_cs_xx.cod_azienda + "' )"
end if

//if ll_livello > 1 then
//	//ls_padre = wf_leggi_parent(fl_handle)
//end if
//
//if ls_padre = "E" then
//	//wf_leggi_parent(fl_handle,ls_sql_livello)
//end if
//
//if len(trim(ls_sql_livello)) > 0 then
//	ls_sql += " and cod_divisione IN ( select cod_divisione from tab_aree_aziendali where cod_azienda = '" + s_cs_xx.cod_azienda + "'  " + ls_sql_livello + " ) "
//end if
//
//if is_tipo_ordinamento = "C" then
//	ls_sql = ls_sql + "ORDER BY cod_fornitore"
//else
//	ls_sql = ls_sql + "ORDER BY cod_fornitore"
//end if
ls_sql = ls_sql + "ORDER BY anno_registrazione desc "

declare cu_anni dynamic cursor for sqlsa;

prepare sqlsa from :ls_sql;

open dynamic cu_anni;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox( "OMNIA", "Errore durante l'apertura del cursore cu_anni: " + sqlca.sqlerrtext + ".~r~n" + ls_sql)
	close cu_anni;
	return 0
end if

do while 1=1
   fetch cu_anni into :ll_anno;
									
   if (sqlca.sqlcode = 100) then
		close cu_anni;
		if lb_dati then return 0
		return 1
	end if
	if (sqlca.sqlcode = -1) then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_anni (w_tes_fat_acq_lista:wf_inserisci_anni)~r~n" + sqlca.sqlerrtext)
		close cu_anni;
		return -1
	end if
	
	lb_dati = true
	lstr_record.anno_registrazione = 0
	lstr_record.num_registrazione = 0
	lstr_record.codice = string(ll_anno)
	lstr_record.descrizione = ""
	lstr_record.handle_padre = fl_handle
	lstr_record.livello = ll_livello
	//setnull(lstr_record.des_manutenzione)
	lstr_record.tipo_livello = "A"
	ltvi_campo.data = lstr_record
	
//	if is_tipo_ordinamento = "D" then
//		tvi_campo.label = ls_des_divisione + ", " +  ls_cod_divisione
//	else
//		tvi_campo.label = ls_cod_divisione + ", " + ls_des_divisione
//	end if
	ltvi_campo.label = "Anno "+string(ll_anno)

	ltvi_campo.pictureindex = 2
	ltvi_campo.selectedpictureindex = 2
	ltvi_campo.overlaypictureindex = 2
	ltvi_campo.children = true
	ltvi_campo.selected = false
		
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	if ll_risposta = -1 then
		g_mb.messagebox("Errore","Errore in inserimento anno nel TREEVIEW al livello ANNo FATTURA ACQUISTO")
		close cu_anni;
		return 0
	end if

loop
close cu_anni;

return 0

end function

public function integer wf_inserisci_fatture (long fl_handle, string fs_cod);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento fatture
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean	lb_dati = false
boolean	ib_max
long ll_livello, ll_anno_registrazione, ll_num_registrazione, ll_risposta, ll_anno_doc_origine, ll_num_doc_origine, ll_count, ll_max_count
string ls_sql, ls_sql_livello, ls_cod_doc_origine, ls_numeratore_doc_origine, ls_flag_agg_mov, ls_ret, ls_cod_fornitore, ls_rag_soc_1
dec{4} ld_tot_val_fat_acq
ws_record lstr_record
treeviewitem ltvi_campo

ll_livello = il_livello_corrente
ll_count = 0
ll_max_count = 500
ib_max = false

setpointer(Hourglass!)

declare cu_registrazioni dynamic cursor for sqlsa;
ls_sql = "SELECT anno_registrazione, " + &
         "       num_registrazione, cod_doc_origine, anno_doc_origine,numeratore_doc_origine,num_doc_origine, cod_fornitore, tot_val_fat_acq " + &			
			"FROM   tes_fat_acq " + &
			"WHERE  cod_azienda = '" + s_cs_xx.cod_azienda + "' "

//----------- Michele 26/04/2004 correzione caricamento registrazioni -------------
if len(is_sql_filtro) > 0 then
	ls_sql += is_sql_filtro
end if
//----------- Fine Modifica -------------

ls_flag_agg_mov = dw_tes_fat_acq_sel.getitemstring(1, "flag_agg_mov")

ls_sql_livello = ""

wf_leggi_parent(fl_handle,ls_sql_livello)

if len(trim(ls_sql_livello)) > 0 then
	ls_sql +=ls_sql_livello
end if

ls_sql = ls_sql + "ORDER BY anno_registrazione asc, num_registrazione asc "

prepare sqlsa from :ls_sql;

open dynamic cu_registrazioni;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA","Errore in OPEN cursore cu_attrezzature (w_manutenzioni:wf_inserisci_fatture)~r~n" + sqlca.sqlerrtext)
	setpointer(Arrow!)
	return -1
end if

do while true
	
	fetch cu_registrazioni into 	:ll_anno_registrazione, 
	                            			:ll_num_registrazione,
										:ls_cod_doc_origine,
										:ll_anno_doc_origine,
										:ls_numeratore_doc_origine,
										:ll_num_doc_origine,
										:ls_cod_fornitore,
										:ld_tot_val_fat_acq;

   if (sqlca.sqlcode = 100) then
		close cu_registrazioni;
		setpointer(Arrow!)
		if lb_dati then return 0
		return 1
	end if
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("OMNIA","Errore in FETCH cursore cu_registrazioni (w_tes_fat_acq_lista:wf_inserisci_fatture)~r~n" + sqlca.sqlerrtext)
		close cu_registrazioni;
		setpointer(Arrow!)
		return -1
	end if
	
	ll_count += 1
	
	//verifica il flag_agg_mov del dettaglio
	/*
		torna "0" se tutte le righe di dettaglio hanno flag_agg_mov='S' (corrisponde a T)
		torna "1" se tutte le righe di dettaglio hanno flag_agg_mov='N' (corrisponde a N)
		torna "2" se alcune righe di dettaglio hanno flag_agg_mov='S' ed altre hanno 'S' (corrisponde a P)
		torna "100" se la riga deve essere vista comunque
	*/
	if ls_flag_agg_mov<>'X' then
		ls_ret = f_check_flag_agg_mov(ll_anno_registrazione, ll_num_registrazione)
	else
		ls_ret='100'
	end if
	
	choose case ls_ret
		case "0"
			if ls_flag_agg_mov<>"T" then continue
			
		case "1"
			if ls_flag_agg_mov<>"N" then continue
			
		case "2"
			if ls_flag_agg_mov<>"P" then continue
			
		case "100"
			//falla vedere
			
	end choose
	
	
	lb_dati = true
		
	lstr_record.anno_registrazione = ll_anno_registrazione	
	lstr_record.num_registrazione = ll_num_registrazione
	
	//lstr_record.des_manutenzione = ls_des_tipo_manutenzione
	
	lstr_record.codice = ""		
	
	lstr_record.descrizione = ""
	
	lstr_record.livello = 100
	
	lstr_record.tipo_livello = "X"	
	
	ltvi_campo.data = lstr_record
	
	
	if not isnull(ls_cod_doc_origine) and ls_cod_doc_origine<>"" and &
			not isnull(ll_anno_doc_origine) and &
			not isnull(ls_numeratore_doc_origine) and ls_numeratore_doc_origine<>"" and &
			not isnull(ll_num_doc_origine) then
			
		ltvi_campo.label = ls_cod_doc_origine + " " + string(ll_anno_doc_origine) + "/"+ls_numeratore_doc_origine+" " &
								+ string(ll_num_doc_origine)
								
	else
		ltvi_campo.label = string(ll_anno_registrazione) + "/"+ string(ll_num_registrazione)
		
	end if
	
	if isnull(ld_tot_val_fat_acq) then ld_tot_val_fat_acq=0
	
	ltvi_campo.label += " Val. "+string(ld_tot_val_fat_acq, "###,###,##0.00")
	
	if not isnull(ls_cod_fornitore) and ls_cod_fornitore<>"" then
		
		select rag_soc_1
		into :ls_rag_soc_1
		from anag_fornitori
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					cod_fornitore=:ls_cod_fornitore;
		
		ltvi_campo.label += " - "+ls_rag_soc_1+" ("+ls_cod_fornitore+")"
		
	end if
	
	ltvi_campo.pictureindex = 4		
	ltvi_campo.selectedpictureindex = 4		
	ltvi_campo.overlaypictureindex = 4
	
	ltvi_campo.children = false
	ltvi_campo.selected = false
	ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)
	
	if ll_count > 500 and not ib_max then
		ib_max = true
		if g_mb.messagebox("APICE","Attenzione troppi dati caricati nell'albero delle fatture. Continuare?" + & 
					  "(Si consiglia di caricare annullare scegliendo NO, impostare qualche altro elemento nel filtro e riprovare.)",Question!, YesNo!,2)=2 then
			close cu_registrazioni;
			setpointer(Arrow!)
			return 0
		end if
	end if
loop

close cu_registrazioni;
setpointer(Arrow!)
return 0
end function

public function integer wf_leggi_parent (long fl_handle, ref string fs_sql);ws_record lstr_item
long	ll_item
treeviewitem ltv_item

ll_item = fl_handle

if ll_item = 0 then
	return 0
end if

do
	
	tv_1.getitem(ll_item,ltv_item)
		
	lstr_item = ltv_item.data
	
	choose case lstr_item.tipo_livello		
		case "T"
			fs_sql += " and cod_tipo_fat_acq = '" + lstr_item.codice + "' "
		case "A"
			fs_sql += " and anno_registrazione = " + lstr_item.codice + " "
		case "F"
			fs_sql += " and cod_fornitore = '" + lstr_item.codice + "' "		
		case "N"
	end choose
	
	ll_item = tv_1.finditem(parenttreeitem!,ll_item)
	
loop while ll_item <> -1

return 0
end function

public function integer wf_inserisci_singola_fattura (long fl_handle, long fl_anno, long fl_numero);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento singola fattura
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

long ll_livello, ll_anno_registrazione, ll_num_registrazione, ll_risposta, ll_anno_doc_origine, &
		ll_num_doc_origine
string ls_sql, ls_sql_livello, ls_cod_doc_origine, ls_numeratore_doc_origine, ls_str, ls_ret, ls_flag_agg_mov, ls_cod_fornitore, ls_rag_soc_1
dec{4} ld_tot_val_fat_acq
ws_record lstr_record
treeviewitem ltvi_campo

select 	anno_registrazione,
			num_registrazione, 
			cod_doc_origine, 
			anno_doc_origine,
			numeratore_doc_origine,
			num_doc_origine,
			cod_fornitore,
			tot_val_fat_acq
into	:ll_anno_registrazione,
		:ll_num_registrazione,
		:ls_cod_doc_origine,
		:ll_anno_doc_origine,
		:ls_numeratore_doc_origine,
		:ll_num_doc_origine,
		:ls_cod_fornitore,
		:ld_tot_val_fat_acq
from   tes_fat_acq 
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno and
		 num_registrazione = :fl_numero;

if sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA","La fattura è inesistente")
	return 0
end if

//verifica il flag_agg_mov del dettaglio
ls_flag_agg_mov = dw_tes_fat_acq_sel.getitemstring(1, "flag_agg_mov")

/*
	torna "0" se tutte le righe di dettaglio hanno flag_agg_mov='S' (corrisponde a T)
	torna "1" se tutte le righe di dettaglio hanno flag_agg_mov='N' (corrisponde a N)
	torna "2" se alcune righe di dettaglio hanno flag_agg_mov='S' ed altre hanno 'S' (corrisponde a P)
	torna "100" se la riga deve essere vista comunque
*/

if ls_flag_agg_mov<>'X' then
	ls_ret = f_check_flag_agg_mov(ll_anno_registrazione, ll_num_registrazione)
else
	ls_ret = "100"
end if

choose case ls_ret
	case "0"
		if ls_flag_agg_mov<>"T" then return 0
		
	case "1"
		if ls_flag_agg_mov<>"N" then return 0
		
	case "2"
		if ls_flag_agg_mov<>"P" then return 0
		
	case "100"
		//falla vedere
		
end choose

lstr_record.anno_registrazione = fl_anno
lstr_record.num_registrazione = fl_numero
//lstr_record.des_manutenzione = ls_des_tipo_manutenzione
lstr_record.codice = ""		
lstr_record.descrizione = ""
lstr_record.livello = 100
lstr_record.tipo_livello = "X"

				
if not isnull(ls_cod_doc_origine) and ls_cod_doc_origine<>"" and &
			not isnull(ll_anno_doc_origine) and &
			not isnull(ls_numeratore_doc_origine) and ls_numeratore_doc_origine<>"" and &
			not isnull(ll_num_doc_origine) then
			
		ltvi_campo.label += " "+ls_cod_doc_origine + " " + string(ll_anno_doc_origine) + "/"+ls_numeratore_doc_origine+" " &
								+ string(ll_num_doc_origine)

else
	ltvi_campo.label = 	string(fl_anno) + "/"+ string(fl_numero) 
end if

if isnull(ld_tot_val_fat_acq) then ld_tot_val_fat_acq=0

ltvi_campo.label += " Val. "+string(ld_tot_val_fat_acq, "###,###,##0.00")

if not isnull(ls_cod_fornitore) and ls_cod_fornitore<>"" then
		
	select rag_soc_1
	into :ls_rag_soc_1
	from anag_fornitori
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				cod_fornitore=:ls_cod_fornitore;
	
	ltvi_campo.label += " - "+ls_rag_soc_1+" ("+ls_cod_fornitore+")"
	
end if


ltvi_campo.pictureindex = 4		
ltvi_campo.selectedpictureindex = 4		
ltvi_campo.overlaypictureindex = 4

ltvi_campo.data = lstr_record

ltvi_campo.children = false

ltvi_campo.selected = false

ll_risposta = tv_1.insertitemlast(fl_handle, ltvi_campo)

return 0
end function

public subroutine wf_memorizza_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero,ll_cont
datetime ldt_data

if g_mb.messagebox("Omnia","Memorizza l'attuale impostazione dei filtro come predefinito?",Question!,YesNo!,2) = 2 then return

ls_colcount = dw_tes_fat_acq_sel.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""
for ll_i = 1 to ll_colonne
	ls_nome_colonna = dw_tes_fat_acq_sel.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_tes_fat_acq_sel.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		ldt_data = dw_tes_fat_acq_sel.getitemdatetime(dw_tes_fat_acq_sel.getrow(), ls_nome_colonna)
		if isnull(ldt_data) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ldt_data, "dd/mm/yyyy") + "~t"
		end if
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		ls_stringa = dw_tes_fat_acq_sel.getitemstring(dw_tes_fat_acq_sel.getrow(), ls_nome_colonna)
		if isnull(ls_stringa) then
			ls_memo += "NULL~t"
		else
			ls_memo += ls_stringa + "~t"
		end if
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		ll_numero = dw_tes_fat_acq_sel.getitemnumber(dw_tes_fat_acq_sel.getrow(), ls_nome_colonna)
		if isnull(ll_numero) then
			ls_memo += "NULL~t"
		else
			ls_memo += string(ll_numero) + "~t"
		end if
	end if

next

select count(*)
into   :ll_cont
from   filtri_manutenzioni
where cod_azienda = :s_cs_xx.cod_azienda and
      cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'FATACQ';
		
if ll_cont > 0 then
	update filtri_manutenzioni
	set filtri = :ls_memo
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_utente =  :ls_cod_utente and
		   tipo_filtro = 'FATACQ';
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
else
	insert into filtri_manutenzioni
		(cod_azienda,
		 cod_utente,
		 filtri,
		 tipo_filtro)
	 values
		 (:s_cs_xx.cod_azienda,
		  :ls_cod_utente,
		  :ls_memo,
		  'FATACQ');
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("OMNIA", "Errore in memorizzazione impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
		rollback;
		return
	end if
end if

commit;

return
end subroutine

public subroutine wf_leggi_filtro ();string ls_colcount, ls_nome_colonna, ls_tipo_colonna, ls_memo, ls_stringa, ls_cod_utente
long ll_i, ll_colonne, ll_numero
datetime ldt_data

ls_colcount = dw_tes_fat_acq_sel.Object.DataWindow.Column.Count
ll_colonne = long(ls_colcount)

ls_cod_utente = s_cs_xx.cod_utente

if ls_cod_utente = "CS_SYSTEM" then
	ls_cod_utente = "SYSTEM"
end if

ls_memo = ""

select filtri 
into   :ls_memo
from   filtri_manutenzioni
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_utente =  :ls_cod_utente and
		 tipo_filtro = 'FATACQ';
if sqlca.sqlcode = 100 then
	g_mb.messagebox("OMNIA", "Nessun filtro memorizzato.")
	return
end if
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("OMNIA", "Errore in lettura impostazioni predefinite filtri di ricerca~r~n" + sqlca.sqlerrtext)
	return
end if

for ll_i = 1 to ll_colonne
	
	ls_stringa = mid(ls_memo,1, pos(ls_memo, "~t") - 1)
	ls_memo = mid(ls_memo, pos(ls_memo, "~t") + 1)
	
	ls_nome_colonna = dw_tes_fat_acq_sel.describe("#"+string(ll_i)+".name")
	ls_tipo_colonna = dw_tes_fat_acq_sel.describe("#"+string(ll_i)+".coltype")

	if lower(ls_tipo_colonna) = "datetime" then
		if ls_stringa = "NULL" then
			setnull(ldt_data)
		else
			ldt_data = datetime(date(ls_stringa), 00:00:00)
		end if
		dw_tes_fat_acq_sel.setitem(dw_tes_fat_acq_sel.getrow(),ls_nome_colonna, ldt_data)
	end if
	
	if lower(left(ls_tipo_colonna,4)) = "char" then
		if ls_stringa = "NULL" then
			setnull(ls_stringa)
		end if
		dw_tes_fat_acq_sel.setitem(dw_tes_fat_acq_sel.getrow(),ls_nome_colonna, ls_stringa)
	end if

	if lower(ls_tipo_colonna) = "number" or lower(ls_tipo_colonna) = "long" then
		if ls_stringa = "NULL" then
			setnull(ll_numero)
		else
			ll_numero = long(ls_stringa)
		end if
		dw_tes_fat_acq_sel.setitem(dw_tes_fat_acq_sel.getrow(),ls_nome_colonna, ll_numero)
	end if

next

return
end subroutine

public function integer wf_assegna_num_fiscale (long ai_row, integer ai_anno_reg, long al_num_reg, ref string as_errore);string				ls_cod_tipo_fat_acq, ls_cod_documento, ls_numeratore_documento
long				ll_num_doc_origine, ll_num_documento, ll_count
integer			li_anno_esercizio
datetime			ldt_data_protocollo
s_cs_xx_parametri			lstr_param

/*
presentare una mascherina che propone i valori: modificabile solo la data protocollo

cod_doc_origine
anno_doc_origine
numeratore_doc_origine
num_doc_origine
data_protocollo
*/

//------------------------------------------------------------------------------------------------------------------------------------------------
//leggo tipo fattura di acquisto
select cod_tipo_fat_acq, num_doc_origine
into :ls_cod_tipo_fat_acq, :ll_num_doc_origine
from tes_fat_acq
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ai_anno_reg and
			num_registrazione = :al_num_reg;

if sqlca.sqlcode<0 then
	as_errore = "Errore in lettura tipo fattura: "+sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	as_errore = "La fattura è inesistene oppure non è stata ancora salvata!"
	return 1
		
else
	if g_str.isempty(ls_cod_tipo_fat_acq) then
		as_errore = "La fattura non ha il tipo fattura assegnato!"
		return 1
	end if
end if


//------------------------------------------------------------------------------------------------------------------------------------------------
//leggo cod_documento e numeratore documento dalla testata per verificare se per caso il numero fiscale già ce l'ha
if ll_num_doc_origine > 0 then
	as_errore = "La fattura non ha già una numerazione protocollo assegnata!"
	return 1
end if


//------------------------------------------------------------------------------------------------------------------------------------------------
//leggo cod_documento e numeratore documento da utilizzare
select		cod_documento, numeratore_documento
into		:ls_cod_documento, :ls_numeratore_documento
from tab_tipi_fat_acq
where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_fat_acq = :ls_cod_tipo_fat_acq;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in lettura cod_documento e numeratore_documento da tabella Tipi Fatture Acquisto: " + sqlca.sqlerrtext
	return -1
end if

if g_str.isempty(ls_cod_documento) then
	as_errore = "Manca l'impostazione del cod.documento da utilizzare in tabella Tipi Fatture Acquisto!"
	return 1
end if

if g_str.isempty(ls_numeratore_documento) then
	as_errore = "Manca l'impostazione del numeratore documento da utilizzare in tabella Tipi Fatture Acquisto!"
	return 1
end if



//prima di andare avanti apro finestrella riepilogo numerazione per confermare la data protocollo
setnull(s_cs_xx.parametri.parametro_data_4)
openwithparm(w_seleziona_data_chiusura, "Data Protocollo Fattura:")

if not s_cs_xx.parametri.parametro_b_1 then
	as_errore = "Annullato dall'operatore!"
	return 1
end if


ldt_data_protocollo = s_cs_xx.parametri.parametro_data_4
if not isnull(ldt_data_protocollo) and year(date(ldt_data_protocollo))> 1950 then
	ldt_data_protocollo = datetime(date(ldt_data_protocollo), 00:00:00)
else
	setnull(ldt_data_protocollo)
end if

//------------------------------------------------------------------------------------------------------------------------------------------------
//leggo il numero a cui si è arrivati in tabella numeratori
li_anno_esercizio = f_anno_esercizio()

select num_documento
into :ll_num_documento
from numeratori
where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_documento = :ls_cod_documento and
			numeratore_documento = :ls_numeratore_documento and
			anno_documento = :li_anno_esercizio;
	
if sqlca.sqlcode < 0 then
	as_errore = "Errore in lettura dati da tabella Numeratori: " + sqlca.sqlerrtext
	return -1
	
elseif sqlca.sqlcode=100 then
	as_errore = "Numeratore non impostato in tabella Numeratori "+&
					"(Cod.documento="+ls_cod_documento+" - Numeratore Doc.="+ls_numeratore_documento+&
					" - Anno="+string(li_anno_esercizio)+")"
	return -1
end if

if isnull(ll_num_documento) then ll_num_documento = 0
ll_num_documento += 1

//controllo caso mai esiste già una fattura con questa numerazione
select count(*)
into :ll_count
from tes_fat_acq
where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_doc_origine = :ls_cod_documento and
			anno_doc_origine = :li_anno_esercizio and
			numeratore_doc_origine = :ls_numeratore_documento and
			num_doc_origine = :ll_num_documento;

if ll_count>0 then
	as_errore = "Attenzione! Esiste già una fattura di acquisto con questa numerazione di protocollo: " +&
					ls_numeratore_documento + " " + string(li_anno_esercizio) + " / " + ls_numeratore_documento + " " + string(ll_num_documento)+"~r~n"+&
					"Controllare la tabella Numeratori per l'impostazione del numero documento per questo tipo fattura"
	return -1
end if


update tes_fat_acq
set 	cod_doc_origine=:ls_cod_documento,
		anno_doc_origine=:li_anno_esercizio,
		numeratore_doc_origine=:ls_numeratore_documento,
		num_doc_origine=:ll_num_documento,
		data_doc_origine=:ldt_data_protocollo
where	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ai_anno_reg and
			num_registrazione = :al_num_reg;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in aggiornamento numerazione protocollo fattura: " + sqlca.sqlerrtext
	rollback;
	return -1
end if

//aggiorno tabella numeratori
update numeratori
set num_documento = :ll_num_documento
where	cod_azienda = :s_cs_xx.cod_azienda and
			cod_documento = :ls_cod_documento and
			numeratore_documento = :ls_numeratore_documento and
			anno_documento = :li_anno_esercizio;

if sqlca.sqlcode < 0 then
	as_errore = "Errore in aggiornamento tabella numeratori: " + sqlca.sqlerrtext
	rollback;
	return -1
end if

commit;

// aggiorno campi DW
dw_tes_fat_acq_det_1.setitem(ai_row, "cod_doc_origine",				ls_cod_documento)
dw_tes_fat_acq_det_1.setitem(ai_row, "anno_doc_origine",			li_anno_esercizio)
dw_tes_fat_acq_det_1.setitem(ai_row, "numeratore_doc_origine",	ls_numeratore_documento)
dw_tes_fat_acq_det_1.setitem(ai_row, "num_doc_origine",				ll_num_documento)
dw_tes_fat_acq_det_1.setitem(ai_row, "data_doc_origine",				ldt_data_protocollo)
dw_tes_fat_acq_det_1.resetupdate()

return 0





end function

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[],l_objects[], lw_oggetti2[]


is_cod_filtro_memorizza = "FATACQ"
b_leggi_filtro.picturename = guo_functions.uof_risorse("treeview/filter.png")
b_memorizza_filtro.picturename = guo_functions.uof_risorse("treeview/filter-add.png")
b_cancella_filtro.picturename = guo_functions.uof_risorse("treeview/filter-delete.png")
 

dw_tes_fat_acq_det_1.set_dw_key("cod_azienda")
dw_tes_fat_acq_det_1.set_dw_key("anno_registrazione")
dw_tes_fat_acq_det_1.set_dw_key("num_registrazione")
								
dw_tes_fat_acq_det_1.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_noretrieveonopen, &
                                    c_default)					

dw_tes_fat_acq_det_2.set_dw_options(sqlca, &
                                    dw_tes_fat_acq_det_1, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)				
iuo_dw_main = dw_tes_fat_acq_det_1


lw_oggetti2[1] = tv_1
dw_folder_search.fu_assigntab(2, "Fatture", lw_oggetti2[])
lw_oggetti2[1] = dw_notifiche
dw_folder_search.fu_assigntab(3, "SDI", lw_oggetti2[])
lw_oggetti2[1] = dw_tes_fat_acq_sel
lw_oggetti2[2] = cb_ricerca
lw_oggetti2[3] = b_leggi_filtro
lw_oggetti2[4] = b_memorizza_filtro
lw_oggetti2[5] = b_cancella_filtro
dw_folder_search.fu_assigntab(1, "Ricerca", lw_oggetti2[])

dw_folder_search.fu_foldercreate(3, 3)
dw_folder_search.fu_selecttab(1)

lw_oggetti[1] = dw_tes_fat_acq_det_2
lw_oggetti[2] = cb_carico_bolle
lw_oggetti[3] = cb_carico_ordini
lw_oggetti[4] = cb_conferma_bolla
lw_oggetti[5] = cb_corrispondenze
lw_oggetti[6] = cb_note
lw_oggetti[7] = cb_calcola
lw_oggetti[8] = cb_dettaglio
lw_oggetti[9] = cb_stampa
dw_folder.fu_assigntab(2, "Totali", lw_oggetti[])

lw_oggetti[1] = dw_tes_fat_acq_det_1
lw_oggetti[2] = cb_iva
lw_oggetti[3] = cb_scadenze
lw_oggetti[4] = cb_contro
lw_oggetti[5] = cb_carico_bolle
lw_oggetti[6] = cb_carico_ordini
lw_oggetti[7] = cb_conferma_bolla
lw_oggetti[8] = cb_corrispondenze
lw_oggetti[9] = cb_note
lw_oggetti[10] = cb_calcola
lw_oggetti[11] = cb_dettaglio
lw_oggetti[12] = cb_stampa
lw_oggetti[13] = cb_1
//lw_oggetti[14] = cb_numero_doc
dw_folder.fu_assigntab(1, "Testata", lw_oggetti[])

dw_folder.fu_foldercreate(2,2)
dw_folder.fu_selecttab(1)

dw_notifiche.ib_dw_report=true 

dw_tes_fat_acq_sel.setitem(1, "anno_registrazione", f_anno_esercizio())

ib_prima_riga = false

dw_tes_fat_acq_det_1.object.b_ricerca_banca_for.enabled = false
dw_tes_fat_acq_det_1.object.b_ricerca_fornitore.enabled = false
cb_dettaglio.enabled = false
cb_contro.enabled = false
cb_scadenze.enabled = false
cb_iva.enabled = false
cb_corrispondenze.enabled = false
cb_note.enabled = false
cb_calcola.enabled = false

//immagini tree
tv_1.deletepictures()
//fornitore
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_area.png")

//anno
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_man.png")

//tipo fattura acquisto
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\tree_man_categorie.png")

//fattura
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_non_eseguita.png")

//fattura eliminata
tv_1.addpicture(s_cs_xx.volume + s_cs_xx.risorse + "11.5\man_reg_eliminata.png")

// ------------------------  richiamo le impostazioni predefinite del filtro --------------------------
b_leggi_filtro.postevent("clicked")
dw_notifiche.Object.b_download_notifica.FileName = s_cs_xx.volume + s_cs_xx.risorse + "12.5\fatel_read_notifica.png"

end event

on w_tes_fat_acq_lista.create
int iCurrent
call super::create
this.b_cancella_filtro=create b_cancella_filtro
this.b_memorizza_filtro=create b_memorizza_filtro
this.b_leggi_filtro=create b_leggi_filtro
this.dw_folder=create dw_folder
this.dw_tes_fat_acq_det_2=create dw_tes_fat_acq_det_2
this.dw_tes_fat_acq_det_1=create dw_tes_fat_acq_det_1
this.cb_1=create cb_1
this.cb_calcola=create cb_calcola
this.cb_carico_bolle=create cb_carico_bolle
this.cb_carico_ordini=create cb_carico_ordini
this.cb_conferma_bolla=create cb_conferma_bolla
this.cb_contro=create cb_contro
this.cb_corrispondenze=create cb_corrispondenze
this.cb_dettaglio=create cb_dettaglio
this.cb_iva=create cb_iva
this.cb_note=create cb_note
this.cb_numero_doc=create cb_numero_doc
this.cb_ricerca=create cb_ricerca
this.cb_scadenze=create cb_scadenze
this.cb_stampa=create cb_stampa
this.dw_notifiche=create dw_notifiche
this.dw_tes_fat_acq_sel=create dw_tes_fat_acq_sel
this.dw_folder_search=create dw_folder_search
this.tv_1=create tv_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.b_cancella_filtro
this.Control[iCurrent+2]=this.b_memorizza_filtro
this.Control[iCurrent+3]=this.b_leggi_filtro
this.Control[iCurrent+4]=this.dw_folder
this.Control[iCurrent+5]=this.dw_tes_fat_acq_det_2
this.Control[iCurrent+6]=this.dw_tes_fat_acq_det_1
this.Control[iCurrent+7]=this.cb_1
this.Control[iCurrent+8]=this.cb_calcola
this.Control[iCurrent+9]=this.cb_carico_bolle
this.Control[iCurrent+10]=this.cb_carico_ordini
this.Control[iCurrent+11]=this.cb_conferma_bolla
this.Control[iCurrent+12]=this.cb_contro
this.Control[iCurrent+13]=this.cb_corrispondenze
this.Control[iCurrent+14]=this.cb_dettaglio
this.Control[iCurrent+15]=this.cb_iva
this.Control[iCurrent+16]=this.cb_note
this.Control[iCurrent+17]=this.cb_numero_doc
this.Control[iCurrent+18]=this.cb_ricerca
this.Control[iCurrent+19]=this.cb_scadenze
this.Control[iCurrent+20]=this.cb_stampa
this.Control[iCurrent+21]=this.dw_notifiche
this.Control[iCurrent+22]=this.dw_tes_fat_acq_sel
this.Control[iCurrent+23]=this.dw_folder_search
this.Control[iCurrent+24]=this.tv_1
end on

on w_tes_fat_acq_lista.destroy
call super::destroy
destroy(this.b_cancella_filtro)
destroy(this.b_memorizza_filtro)
destroy(this.b_leggi_filtro)
destroy(this.dw_folder)
destroy(this.dw_tes_fat_acq_det_2)
destroy(this.dw_tes_fat_acq_det_1)
destroy(this.cb_1)
destroy(this.cb_calcola)
destroy(this.cb_carico_bolle)
destroy(this.cb_carico_ordini)
destroy(this.cb_conferma_bolla)
destroy(this.cb_contro)
destroy(this.cb_corrispondenze)
destroy(this.cb_dettaglio)
destroy(this.cb_iva)
destroy(this.cb_note)
destroy(this.cb_numero_doc)
destroy(this.cb_ricerca)
destroy(this.cb_scadenze)
destroy(this.cb_stampa)
destroy(this.dw_notifiche)
destroy(this.dw_tes_fat_acq_sel)
destroy(this.dw_folder_search)
destroy(this.tv_1)
end on

event pc_delete;call super::pc_delete;integer li_i

for li_i = 1 to dw_tes_fat_acq_det_1.deletedcount()
	if dw_tes_fat_acq_det_1.getitemstring(li_i, "flag_fat_confermata", delete!, true) = "S" then
	   g_mb.messagebox("Attenzione", "Fattura non cancellabile! E' già stata confermata;~nprocedere con movimenti di rettifica manuali in magazzino.", exclamation!, ok!)
	   dw_tes_fat_acq_det_1.set_dw_view(c_ignorechanges)
		triggerevent("pc_retrieve")
	   pcca.error = c_fatal
	   return
	end if
next

dw_tes_fat_acq_det_1.object.b_ricerca_banca_for.enabled = false
dw_tes_fat_acq_det_1.object.b_ricerca_fornitore.enabled = false
cb_dettaglio.enabled = false
cb_contro.enabled = false
cb_scadenze.enabled = false
cb_iva.enabled = false
cb_corrispondenze.enabled = false
cb_note.enabled = false
cb_calcola.enabled = false
cb_numero_doc.enabled = false

end event

event pc_modify;call super::pc_modify;if dw_tes_fat_acq_det_1.getitemstring(dw_tes_fat_acq_det_1.getrow(), "flag_fat_confermata") = "S" then
   if g_mb.messagebox("Attenzione", "ATTENZIONE! La fattura è confermata e quindi~nle modifiche apportate non avranno effetto~nsui movimenti di magazzino.~nContinuo lo stesso ?", Question!, YesNo!, 1) = 2 then
		dw_tes_fat_acq_det_1.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
end if
end event

event pc_setddlb;call super::pc_setddlb;string ls_select_operatori
if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + ")) and cod_operatore in (select cod_operatore from tab_operatori_utenti where cod_utente = '" + &
						  s_cs_xx.cod_utente + "')"
else
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + "))"
end if

f_po_loaddddw_dw(dw_tes_fat_acq_det_1, &
                 "cod_operatore", &
                 sqlca, &
                 "tab_operatori", &
                 "cod_operatore", &
                 "des_operatore", &
					  ls_select_operatori)

f_po_loaddddw_dw(dw_tes_fat_acq_sel, &
                 "cod_tipo_fat_acq", &
                 sqlca, &
                 "tab_tipi_fat_acq", &
                 "cod_tipo_fat_acq", &
                 "des_tipi_fat_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
							  
f_po_loaddddw_dw(dw_tes_fat_acq_det_1, &
                 "cod_tipo_fat_acq", &
                 sqlca, &
                 "tab_tipi_fat_acq", &
                 "cod_tipo_fat_acq", &
                 "des_tipi_fat_acq", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tes_fat_acq_det_1, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_fat_acq_det_1, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_fat_acq_det_1, &
                 "cod_pagamento", &
                 sqlca, &
                 "tab_pagamenti", &
                 "cod_pagamento", &
                 "des_pagamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_fat_acq_det_1, &
                 "cod_banca", &
                 sqlca, &
                 "anag_banche", &
                 "cod_banca", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_fat_acq_det_1, &
                 "cod_banca_clien_for", &
                 sqlca, &
                 "anag_banche_clien_for", &
                 "cod_banca_clien_for", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_tes_fat_acq_det_1, &
                 "cod_doc_origine", &
                 sqlca, &
                 "tab_documenti", &
                 "cod_documento", &
                 "des_documento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")					  

f_po_loaddddw_dw(dw_tes_fat_acq_det_1, &
	"cod_mezzo", &
	sqlca, &
	"tab_mezzi", &
	"cod_mezzo", &
	"des_mezzo", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_tes_fat_acq_det_1, &
	"cod_natura_intra", &
	sqlca, &
	"tab_natura_intra", &
	"cod_natura_intra", &
	"descrizione", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
	
f_po_loaddddw_dw(dw_tes_fat_acq_det_1, &
	"cod_porto", &
	sqlca, &
	"tab_porti", &
	"cod_porto", &
	"des_porto", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")




end event

type b_cancella_filtro from picture within w_tes_fat_acq_lista
integer x = 297
integer y = 2140
integer width = 73
integer height = 64
boolean originalsize = true
string picturename = "C:\cs_125\framework\risorse\treeview\filter-delete.png"
boolean focusrectangle = false
string powertiptext = "Cancella Filtro"
end type

event clicked;guo_functions.uof_cancella_filtro(dw_tes_fat_acq_sel, is_cod_filtro_memorizza)
end event

type b_memorizza_filtro from picture within w_tes_fat_acq_lista
integer x = 183
integer y = 2140
integer width = 73
integer height = 64
boolean originalsize = true
string picturename = "C:\cs_125\framework\risorse\treeview\filter-add.png"
boolean focusrectangle = false
string powertiptext = "Memorizza Filtro"
end type

event clicked;guo_functions.uof_memorizza_filtro(dw_tes_fat_acq_sel, is_cod_filtro_memorizza)
end event

type b_leggi_filtro from picture within w_tes_fat_acq_lista
integer x = 69
integer y = 2140
integer width = 73
integer height = 64
boolean originalsize = true
string picturename = "C:\cs_125\framework\risorse\treeview\filter.png"
boolean focusrectangle = false
string powertiptext = "Leggi Filtro"
end type

event clicked;guo_functions.uof_leggi_filtro(dw_tes_fat_acq_sel, is_cod_filtro_memorizza)
end event

type dw_folder from u_folder within w_tes_fat_acq_lista
integer x = 1554
integer y = 20
integer width = 2811
integer height = 2200
integer taborder = 170
end type

type dw_tes_fat_acq_det_2 from uo_cs_xx_dw within w_tes_fat_acq_lista
integer x = 1577
integer y = 120
integer width = 2729
integer height = 1600
integer taborder = 30
boolean bringtotop = true
string dataobject = "d_tes_fat_acq_det_2"
boolean border = false
end type

event rowfocuschanged;call super::rowfocuschanged;//if i_extendmode then
//	dw_tes_fat_acq_lista.setrow(getrow())
//end if
end event

type dw_tes_fat_acq_det_1 from uo_cs_xx_dw within w_tes_fat_acq_lista
event ue_verifica_num_documento ( )
event ue_acquista_fatt_ven ( )
integer x = 1577
integer y = 120
integer width = 2725
integer height = 1616
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_tes_fat_acq_det_1"
boolean border = false
end type

event ue_verifica_num_documento();string ls_cod_doc_origine, ls_numeratore_doc_origine
long   ll_anno_doc_origine, ll_num_doc_origine, ll_cont

ls_cod_doc_origine = getitemstring(getrow(),"cod_doc_origine")
ll_anno_doc_origine = getitemnumber(getrow(),"anno_doc_origine")
ls_numeratore_doc_origine = getitemstring(getrow(),"numeratore_doc_origine")
ll_num_doc_origine = getitemnumber(getrow(),"num_doc_origine")

if isnull(ls_cod_doc_origine) or len(ls_cod_doc_origine) = 0 or &
   isnull(ls_numeratore_doc_origine) or len(ls_numeratore_doc_origine) = 0 or &
	isnull(ll_anno_doc_origine) or ll_anno_doc_origine = 0 or &
	isnull(ll_num_doc_origine) or ll_num_doc_origine = 0 then
	
		return
end if

ll_cont = 0

select count(*)
into   :ll_cont
from   tes_fat_acq
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_doc_origine = :ls_cod_doc_origine and
		 anno_doc_origine = :ll_anno_doc_origine and
		 numeratore_doc_origine = :ls_numeratore_doc_origine and
		 num_doc_origine = :ll_num_doc_origine;
		 
if ll_cont > 0 then
	g_mb.messagebox("APICE", "Attenzione! Questo numero documento è presente in un'altra fattura.")
	return
end if

return
end event

event ue_acquista_fatt_ven();//evento creato per legegre fatture di vendita da vecchi database gruppo GIBUS (PTENDA - CGIBUS - VIROPA)
//e importarli in una fattura di acquisto

integer		li_anno_fat_acq
long			ll_num_fat_acq, ll_row
s_cs_xx_parametri ls_parametri
string			ls_ret

ll_row = dw_tes_fat_acq_det_1.getrow()

if ll_row>0 then
else
	g_mb.error("Nessuna Fattura di acquisto selezionata!")
	return
end if

li_anno_fat_acq = dw_tes_fat_acq_det_1.getitemnumber( ll_row, "anno_registrazione")
ll_num_fat_acq = dw_tes_fat_acq_det_1.getitemnumber( ll_row, "num_registrazione")

if li_anno_fat_acq>0 and ll_num_fat_acq>0 then
else
	g_mb.error("Nessuna Fattura di acquisto selezionata!")
	return
end if

//controlla che il dettaglio della fattura sia vuoto
select count(*)
into :ll_row
from det_fat_acq
where cod_azienda=:s_cs_xx.cod_azienda and
		anno_registrazione=:li_anno_fat_acq and
		num_registrazione=:ll_num_fat_acq;

if sqlca.sqlcode<0 then
	g_mb.error("Errore in controllo dettagli fattura di acquisto: "+sqlca.sqlerrtext)
	return
end if

if ll_row>0 then
	g_mb.error("La fattura di acquisto "+string(li_anno_fat_acq)+"/"+string(ll_num_fat_acq)+" contiene già righe. Impossibile continuare!")
	return
end if

//se arrivi fin qui apri la finestra di richiesta fattura di vendita
ls_parametri.parametro_d_1_a[1] = li_anno_fat_acq
ls_parametri.parametro_d_1_a[2] = ll_num_fat_acq
openwithparm(w_acquista_fatt_ven, ls_parametri)

//al rientro, vedi di fare il calcolo della fattura
ls_ret = message.stringparm
if ls_ret="OK" then
	//ricalcola fattura ...
	cb_calcola.postevent(clicked!)
end if


end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string 	ls_null, ls_cod_fornitore, ls_cod_fil_fornitore, ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
				ls_cod_banca_clien_for, ls_cod_documento, ls_numeratore_documento, ls_valuta_default, ls_messaggio
	integer 	li_ret
	dec{4}	ld_sconto
	dec{5} 	ld_cambio_acq
	datetime	ldt_data_registrazione
	integer 	li_numero
	long 		ll_num_documento
	uo_fido_cliente luo_fido_cliente


	setnull(ls_null)

   choose case i_colname
      case "data_registrazione"
         ls_cod_valuta = this.getitemstring(i_rownbr, "cod_valuta")
         f_cambio_acq(ls_cod_valuta, datetime(date(i_coltext)))
		
      case "data_protocollo"
		guo_functions.uof_get_parametro_azienda( "LIR", ls_valuta_default)
         ls_cod_valuta = this.getitemstring(i_rownbr, "cod_valuta")
         ld_cambio_acq = f_cambio_val_acq(ls_cod_valuta, datetime(date(i_coltext)))
			
		if ls_cod_valuta <> ls_valuta_default and getitemnumber(row, "cambio_acq") <> ld_cambio_acq then
			setitem(row, "cambio_acq", ld_cambio_acq)
			g_mb.warning("Cambio aggiornato in base alla data documento del fornitore impostata.")
		end if		
			
      case "cod_fornitore"
		ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
		luo_fido_cliente = create uo_fido_cliente
		li_ret = luo_fido_cliente.uof_get_blocco_fornitore( data, ldt_data_registrazione, is_cod_parametro_blocco, ls_messaggio)
		if li_ret=2 then
			//blocco
			g_mb.error(ls_messaggio)
			this.setitem(i_rownbr, i_colname, ls_null)
			this.SetColumn ( i_colname )
			destroy luo_fido_cliente
			return 2
		elseif li_ret=1 then
			//solo warning
			g_mb.warning(ls_messaggio)
		end if
		destroy luo_fido_cliente

         select cod_valuta,
                cod_tipo_listino_prodotto,
                cod_pagamento,
                sconto,
                cod_banca_clien_for
         into   :ls_cod_valuta,
                :ls_cod_tipo_listino_prodotto,
                :ls_cod_pagamento,
                :ld_sconto,
                :ls_cod_banca_clien_for
         from   anag_fornitori
         where  cod_azienda = :s_cs_xx.cod_azienda and 
                cod_fornitore = :i_coltext;
 
         if sqlca.sqlcode = 0 then 
            this.setitem(i_rownbr, "cod_valuta", ls_cod_valuta)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
            this.setitem(i_rownbr, "cod_pagamento", ls_cod_pagamento)
            this.setitem(i_rownbr, "sconto", ld_sconto)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_cod_banca_clien_for)
            f_po_loaddddw_dw(this, &
                             "cod_fil_fornitore", &
                             sqlca, &
                             "anag_fil_fornitori", &
                             "cod_fil_fornitore", &
                             "rag_soc_1", &
                             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + i_coltext + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
    
            this.setitem(i_rownbr, "cod_fil_fornitore", ls_null)
            ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
            f_cambio_acq(ls_cod_valuta, ldt_data_registrazione)
         else
            this.setitem(i_rownbr, "cod_valuta", ls_null)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_null)
            this.setitem(i_rownbr, "cod_pagamento", ls_null)
            this.setitem(i_rownbr, "sconto", ls_null)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_null)
            this.setitem(i_rownbr, "cod_fil_fornitore", ls_null)
         end if 
		case "cod_valuta"
         ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
         f_cambio_acq(i_coltext, ldt_data_registrazione)
		case "cod_tipo_fat_acq"
			select numero
			  into :li_numero		
			  from parametri_azienda
			 where cod_azienda = :s_cs_xx.cod_azienda
			   and flag_parametro = 'N'
				and cod_parametro = 'ESC';
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Apice", "Errore in lettura dati da tabella parametri azienda.~n~r" + sqlca.sqlerrtext)
				return
			end if
			
			select cod_documento,
					 numeratore_documento
			  into :ls_cod_documento,
			  		 :ls_numeratore_documento
			  from tab_tipi_fat_acq
			 where cod_azienda = :s_cs_xx.cod_azienda
			   and cod_tipo_fat_acq = :i_coltext;
				
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Apice", "Errore in lettura dati da tabella Tipi Fatture Acquisto.~n~r" + sqlca.sqlerrtext)
				return
			end if		
			
			select num_documento + 1
			  into :ll_num_documento
			  from numeratori
			 where cod_azienda = :s_cs_xx.cod_azienda
			   and cod_documento = :ls_cod_documento
				and numeratore_documento = :ls_numeratore_documento
				and anno_documento = :li_numero;

			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Apice", "Errore in lettura dati da tabella Numeratori.~n~r" + sqlca.sqlerrtext)
				return
			end if						
			
			dw_tes_fat_acq_det_1.setitem(dw_tes_fat_acq_det_1.getrow(), "cod_doc_origine", ls_cod_documento)			
			dw_tes_fat_acq_det_1.setitem(dw_tes_fat_acq_det_1.getrow(), "anno_doc_origine", li_numero)			
			dw_tes_fat_acq_det_1.setitem(dw_tes_fat_acq_det_1.getrow(), "numeratore_doc_origine", ls_numeratore_documento)
			dw_tes_fat_acq_det_1.setitem(dw_tes_fat_acq_det_1.getrow(), "num_doc_origine", ll_num_documento)						

		case "cod_doc_origine", "anno_doc_origine", "numeratore_doc_origine", "num_doc_origine"
			postevent("ue_verifica_num_documento")
			
   end choose
end if
end event

event pcd_retrieve;call super::pcd_retrieve;//long ll_errore, ll_anno_doc_origine, ll_num_doc_origine, ll_progressivo
//string ls_cod_doc_origine, ls_numeratore_doc_origine
//long ll_anno_registrazione, ll_num_registrazione, ll_errore
//
//if not isnull(dw_tes_fat_acq_lista.getrow()) and dw_tes_fat_acq_lista.getrow() > 0 then
//
//	ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.getrow(), "anno_registrazione") 
//	ll_num_registrazione = dw_tes_fat_acq_lista.getitemnumber(dw_tes_fat_acq_lista.getrow(), "num_registrazione")
//	ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)
//	
//	if ll_errore < 0 then
//		pcca.error = c_fatal
//	end if
//end if	

long  l_Error
ws_record lstr_record
treeviewitem ltvi_campo

tv_1.getitem(il_handle, ltvi_campo)
lstr_record = ltvi_campo.data

if not(lstr_record.anno_registrazione > 0 and lstr_record.num_registrazione > 0) then
	setnull(lstr_record.anno_registrazione)
	setnull(lstr_record.num_registrazione)
end if

l_Error = Retrieve(s_cs_xx.cod_azienda, lstr_record.anno_registrazione, lstr_record.num_registrazione)
if l_Error < 0 then
	PCCA.Error = c_Fatal
end if

il_anno_registrazione = lstr_record.anno_registrazione
il_num_registrazione = lstr_record.num_registrazione

end event

event updatestart;call super::updatestart;//if i_extendmode then
//   integer li_i
//   string ls_cod_doc_origine, ls_numeratore_doc_origine
//   long ll_anno_doc_origine, ll_num_doc_origine, ll_progressivo, ll_anno_registrazione, ll_num_registrazione
//		
//   for li_i = 1 to this.deletedcount()
//		ls_cod_doc_origine = this.getitemstring(li_i, "cod_doc_origine", delete!, true)
//		ls_numeratore_doc_origine = this.getitemstring(li_i, "numeratore_doc_origine", delete!, true)
//      ll_anno_doc_origine = this.getitemnumber(li_i, "anno_doc_origine", delete!, true)
//      ll_num_doc_origine = this.getitemnumber(li_i, "num_doc_origine", delete!, true)
//      ll_progressivo = this.getitemnumber(li_i, "progressivo", delete!, true)
//		ll_anno_registrazione = this.getitemnumber(li_i, "anno_registrazione", delete!, true)
//		ll_num_registrazione = this.getitemnumber(li_i, "num_registrazione", delete!, true)
//
//      if f_del_fat_acq_new(ls_cod_doc_origine, ls_numeratore_doc_origine, ll_anno_doc_origine, ll_num_doc_origine, ll_progressivo, ll_anno_registrazione, ll_num_registrazione) = -1 then
//			return 1
//		end if
//   next
//
//end if

if i_extendmode then
	integer li_i
	long ll_anno_doc_origine, ll_num_doc_origine, ll_progressivo, ll_anno_registrazione, ll_num_registrazione
	string ls_cod_doc_origine, ls_numeratore_doc_origine, ls_nome_prog, ls_flag_tipo_fat_acq

	for li_i = 1 to this.deletedcount()
		ls_cod_doc_origine = this.getitemstring(li_i, "cod_doc_origine", delete!, true)                              
		ls_numeratore_doc_origine = this.getitemstring(li_i, "numeratore_doc_origine", delete!, true)                           
		ll_anno_doc_origine = this.getitemnumber(li_i, "anno_doc_origine", delete!, true)
		ll_num_doc_origine = this.getitemnumber(li_i, "num_doc_origine", delete!, true)
		ll_progressivo = this.getitemnumber(li_i, "progressivo", delete!, true)
		ll_anno_registrazione = this.getitemnumber(li_i, "anno_registrazione", delete!, true)
		ll_num_registrazione = this.getitemnumber(li_i, "num_registrazione", delete!, true)                 

		delete from det_fat_ven_cc
		where  	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione  and
					num_registrazione = :ll_num_registrazione ;

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE", "Errore in cancellazione dati centri di costo.~r~n"+sqlca.sqlerrtext)
			return 1
		end if

		if f_elimina_det_stat_fat_acq(ll_anno_registrazione, ll_num_registrazione, 0) = -1 then
			return 1
		end if

		if f_del_fat_acq_new(ll_anno_registrazione, ll_num_registrazione) = -1 then
			return 1
		end if
	next

	if this.getrow() > 0 then
		if dw_tes_fat_acq_det_1.getitemstring(dw_tes_fat_acq_det_1.getrow(), "flag_fat_confermata") = "S" then
			g_mb.messagebox("Attenzione", "ATTENZIONE! La fattura è già confermata~npertanto le modifiche apportatenon~navranno effetto sui movimenti di magazzino.", exclamation!, ok!)
		end if
	end if

	for li_i = 1 to modifiedcount()
		string ls_cod_documento,ls_numeratore_documento
		long ll_anno_esercizio,ll_num_documento, ll_numero
		
		ls_cod_documento = getitemstring(getrow(), "cod_doc_origine")                                       
		ll_anno_esercizio = getitemnumber(getrow(), "anno_doc_origine")                                   
		ls_numeratore_documento = getitemstring(getrow(), "numeratore_doc_origine")
		ll_num_documento = getitemnumber(getrow(), "num_doc_origine")
		
		if not isnull(ls_cod_documento) and ll_anno_esercizio > 0 and not isnull(ls_numeratore_documento)  then
			select num_documento
			into :ll_numero
			from numeratori
			where cod_azienda = :s_cs_xx.cod_azienda and
								 cod_documento = :ls_cod_documento and
								 numeratore_documento = :ls_numeratore_documento and
								anno_documento = :ll_anno_esercizio;
		
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice", "Errore in lettura dati da tabella Numeratori.~n~r" + sqlca.sqlerrtext)
				return
			end if
		
			if ll_num_documento > ll_numero then
				update numeratori
				set    num_documento = :ll_num_documento
				where cod_azienda = :s_cs_xx.cod_azienda and
														 cod_documento = :ls_cod_documento and
														 numeratore_documento = :ls_numeratore_documento and
														 anno_documento = :ll_anno_esercizio;
			end if    
		end if                    
	next
end if

il_handle_modificato = 0
il_handle_cancellato = 0
if modifiedcount() > 0 then
	il_handle_modificato = tv_1.finditem(CurrentTreeItem!, 0)
end if
if deletedcount() > 0 then
	il_handle_cancellato = tv_1.finditem(CurrentTreeItem!, 0)
end if
end event

event updateend;call super::updateend;//long ll_numeratore, ll_anno_doc_origine
//string ls_numeratore_doc_origine, ls_cod_doc_origine
//
//ll_numeratore = dw_tes_fat_acq_det_1.getitemnumber(dw_tes_fat_acq_det_1.getrow(), "num_doc_origine")
//ls_cod_doc_origine = dw_tes_fat_acq_det_1.getitemstring(dw_tes_fat_acq_det_1.getrow(), "cod_doc_origine")
//ll_anno_doc_origine = dw_tes_fat_acq_det_1.getitemnumber(dw_tes_fat_acq_det_1.getrow(), "anno_doc_origine")
//ls_numeratore_doc_origine = dw_tes_fat_acq_det_1.getitemstring(dw_tes_fat_acq_det_1.getrow(), "numeratore_doc_origine")
//
//
//if not isnull(ll_numeratore) and ll_numeratore > 0 then
//	update numeratori
//	   set num_documento = :ll_numeratore
//	 where cod_azienda = :s_cs_xx.cod_azienda
//	   and cod_documento = :ls_cod_doc_origine
//		and numeratore_documento = :ls_numeratore_doc_origine
//		and anno_documento = :ll_anno_doc_origine;
//	if sqlca.sqlcode < 0 then 	
//		messagebox("Apice", "Errore in aggiornamento dati in tabella Numeratori" + sqlca.sqlerrtext)
//		return
//	end if	
//end if

//Donato: 08/06/2010 questo qui sopra era già commentato (cosi come nell'updatestart)

long ll_row, ll_risposta
ws_record    lstr_record
treeviewitem ltv_item

ll_row = dw_tes_fat_acq_det_1.getrow()

//if il_handle_modificato > 0 then

	choose case dw_tes_fat_acq_det_1.getitemstatus(ll_row, 0, Primary!)
		case DataModified!			
			tv_1.getitem(il_handle_modificato,ltv_item)
					
			ltv_item.pictureindex = 4
			ltv_item.selectedpictureindex = 4
			ltv_item.overlaypictureindex = 4
			
			tv_1.setitem(il_handle_modificato, ltv_item)
			
		case NewModified!	
			
			lstr_record.anno_registrazione = dw_tes_fat_acq_det_1.getitemnumber( ll_row, "anno_registrazione")
			lstr_record.num_registrazione = dw_tes_fat_acq_det_1.getitemnumber( ll_row, "num_registrazione")
			lstr_record.codice = ""//dw_manutenzioni_1.getitemstring( ll_row, "cod_attrezzatura")
			lstr_record.descrizione = ""
			lstr_record.livello = 100
			lstr_record.tipo_livello = "X"
			ltv_item.data = lstr_record
			ltv_item.label = "Nuova Fatt.: " + string(lstr_record.anno_registrazione) + "/" + string(lstr_record.num_registrazione)
			
			ltv_item.pictureindex = 4
			ltv_item.selectedpictureindex = 4
			ltv_item.overlaypictureindex = 4
						
			ltv_item.children = false
			ltv_item.selected = false
			//ll_risposta = tv_1.insertitemFirst(tv_1.Finditem(RootTreeItem! ,0), tvi_campo)
			ll_risposta = tv_1.insertitemLast(0, ltv_item)
			tv_1.SelectItem(ll_risposta)
			
		case New!
			
	end choose
//end if	

if il_handle_cancellato > 0 then
	tv_1.getitem(il_handle_cancellato,ltv_item)
	
	ltv_item.pictureindex = 5
	ltv_item.selectedpictureindex = 5		
	ltv_item.overlaypictureindex = 5
	
	tv_1.setitem(il_handle_cancellato, ltv_item)
end if
end event

event rowfocuschanged;call super::rowfocuschanged;//if i_extendmode then
//	dw_tes_fat_acq_lista.setrow(getrow())
//end if

if i_extendmode then
	if this.getrow() > 0 and not isnull(this.getitemstring(this.getrow(), "cod_fornitore")) then
		f_po_loaddddw_dw(dw_tes_fat_acq_det_1, &
									 "cod_fil_fornitore", &
									sqlca, &
									"anag_fil_fornitori", &
									"cod_fil_fornitore", &
									"rag_soc_1", &
									 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + &
									 				this.getitemstring(this.getrow(), "cod_fornitore") + &
													 "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
													 s_cs_xx.db_funzioni.oggi + "))")
	end if
end if


end event

event pcd_new;call super::pcd_new;if i_extendmode then
	string		ls_cod_tipo_fat_acq, ls_cod_operatore, ls_cod_documento, ls_numeratore_documento,ls_cod_deposito, ls_errore
	long			ll_max_registrazione, ll_anno_registrazione, ll_num_registrazione, ll_num_documento
	integer		li_numero


	object.b_fiscale.enabled = false
 
	select con_fat_acq.cod_tipo_fat_acq
	into   :ls_cod_tipo_fat_acq
	from   con_fat_acq
	where  con_fat_acq.cod_azienda = :s_cs_xx.cod_azienda;

	if sqlca.sqlcode = 0 then
		this.setitem(this.getrow(), "cod_tipo_fat_acq", ls_cod_tipo_fat_acq)
	end if    

	//----------------------------------------------------------------------------------------------
	select numero
	into :li_numero                             
	from parametri_azienda
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				flag_parametro = 'N' and
				cod_parametro = 'ESC';

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati da tabella parametri azienda.~n~r" + sqlca.sqlerrtext)
		return
	end if

	select	cod_documento,
				numeratore_documento
	into 		:ls_cod_documento,
				:ls_numeratore_documento
	from tab_tipi_fat_acq
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_fat_acq = :ls_cod_tipo_fat_acq;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati da tabella Tipi Fatture Acquisto.~n~r" + sqlca.sqlerrtext)
		return
	end if                    

	select num_documento + 1
	into :ll_num_documento
	from numeratori
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_documento = :ls_cod_documento and
				numeratore_documento = :ls_numeratore_documento and
				anno_documento = :li_numero;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati da tabella Numeratori.~n~r" + sqlca.sqlerrtext)
		return
	end if                                                                                  

	dw_tes_fat_acq_det_1.SetColumn ( "cod_doc_origine" )
	dw_tes_fat_acq_det_1.SetText ( ls_cod_documento )
	dw_tes_fat_acq_det_1.SetColumn ( "anno_doc_origine" )
	dw_tes_fat_acq_det_1.SetText ( string(li_numero) )
	dw_tes_fat_acq_det_1.SetColumn ( "numeratore_doc_origine" )
	dw_tes_fat_acq_det_1.SetText ( ls_numeratore_documento )
	dw_tes_fat_acq_det_1.SetColumn ( "num_doc_origine" )
	dw_tes_fat_acq_det_1.SetText ( string(ll_num_documento) )
	dw_tes_fat_acq_det_1.accepttext()
	//----------------------------------------------------------------------------------------------

	select cod_operatore
	into :ls_cod_operatore
	from tab_operatori_utenti
	where 	cod_azienda = :s_cs_xx.cod_azienda and
				cod_utente = :s_cs_xx.cod_utente and
				flag_default = 'S';

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Estrazione Operatori", "Errore durante l'Estrazione dell'Operatore di Default")
		pcca.error = c_fatal
		return
	elseif sqlca.sqlcode = 0 then
		this.setitem(this.getrow(), "cod_operatore", ls_cod_operatore)
	end if
	
	// stefanop: 21/12/2011: aggiunto deposito collegato all'operatore
//	guo_functions.uof_get_stabilimento_utente(ls_cod_deposito, ls_errore)
//	if not isnull(ls_cod_deposito) then setitem(getrow(), "cod_deposito", ls_cod_deposito)
	// ----
	 
	cb_numero_doc.enabled = true
	dw_tes_fat_acq_det_1.object.b_ricerca_banca_for.enabled = true
	dw_tes_fat_acq_det_1.object.b_ricerca_fornitore.enabled = true
	cb_dettaglio.enabled = false
	cb_contro.enabled = false
	cb_scadenze.enabled = false
	cb_iva.enabled = false
	cb_corrispondenze.enabled = false
	cb_note.enabled = false
	cb_calcola.enabled = false
	
	ll_anno_registrazione = f_anno_esercizio()

	if ll_anno_registrazione > 0 then
		this.setitem(this.getrow(), "anno_registrazione", int(ll_anno_registrazione))
	else
		g_mb.messagebox("Fatture Vendita","Impostare l'anno di esercizio in parametri aziendali")
	end if    

	ll_max_registrazione = 0
	
	select max(num_registrazione)
	into   :ll_max_registrazione
	from   tes_fat_acq
	where	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_registrazione;
	
	 if not isnull(ll_max_registrazione) and ll_max_registrazione > 0 then
		ll_num_registrazione = ll_max_registrazione + 1
	 else
		ll_num_registrazione = 1
	 end if
	
	this.setitem(this.getrow(), "num_registrazione", ll_num_registrazione)            
	this.setitem(this.getrow(), "data_registrazione", datetime(today()))   
	this.setitem(this.getrow(), "data_doc_origine", datetime(today()))                     
	
	ib_nuovo = true
end if

 

end event

event pcd_save;call super::pcd_save;if i_extendmode then
	long ll_num_registrazione

	if pcca.error = c_success then
dw_tes_fat_acq_det_1.object.b_ricerca_fornitore.enabled = false
dw_tes_fat_acq_det_1.object.b_ricerca_banca_for.enabled = false
		
		if this.getrow() > 0 and this.getitemnumber(this.getrow(), "anno_registrazione") > 0 then
			cb_dettaglio.enabled = true
			cb_contro.enabled = true
			cb_scadenze.enabled = true
			cb_iva.enabled = true
			cb_corrispondenze.enabled = true
			cb_note.enabled = true
			cb_calcola.enabled = true
		else
			cb_dettaglio.enabled = false
			cb_contro.enabled = false
			cb_scadenze.enabled = false
			cb_iva.enabled = false
			cb_corrispondenze.enabled = false
			cb_note.enabled = false
			cb_calcola.enabled = false
		end if
   end if
end if


end event

event pcd_setkey;call super::pcd_setkey;long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_progressivo, ll_max_registrazione

 
for ll_i = 1 to this.rowcount()

	if isnull(this.getitemstring(ll_i, "cod_azienda")) then
		this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
	end if

	ll_anno_registrazione = this.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = this.getitemnumber(ll_i, "num_registrazione")

	if this.getitemnumber(ll_i, "anno_registrazione") = 0 or &
		isnull(this.getitemnumber(ll_i, "anno_registrazione")) then
		ll_anno_registrazione = f_anno_esercizio()

		if ll_anno_registrazione > 0 then
			this.setitem(this.getrow(), "anno_registrazione", int(ll_anno_registrazione))
		else
			g_mb.messagebox("Fatture Vendita","Impostare l'anno di esercizio in parametri aziendali")
		end if

		ll_max_registrazione = 0

		select max(num_registrazione)
		into   :ll_max_registrazione
		from   tes_fat_acq
		where  	cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione = :ll_anno_registrazione;

		if not isnull(ll_max_registrazione) then
			ll_num_registrazione = ll_max_registrazione + 1
		else
			ll_num_registrazione = 1
		end if
	end if

	if isnull(this.getitemnumber(ll_i, "progressivo")) or &
		this.getitemnumber(ll_i, "progressivo") = 0 then
		
		select max(tes_fat_acq.progressivo)
		into   :ll_progressivo
		from   tes_fat_acq
		where  	tes_fat_acq.cod_azienda = :s_cs_xx.cod_azienda and
					tes_fat_acq.anno_registrazione = :ll_anno_registrazione and
					tes_fat_acq.num_registrazione = :ll_num_registrazione;

		if isnull(ll_progressivo) then
			this.setitem(ll_i, "progressivo", 1)
		else
			this.setitem(ll_i, "progressivo", ll_progressivo + 1)
		end if
	end if
next  

end event

event pcd_view;call super::pcd_view;if i_extendmode then
	
	object.b_fiscale.enabled = true
	
	dw_tes_fat_acq_det_1.object.b_ricerca_fornitore.enabled = false
	dw_tes_fat_acq_det_1.object.b_ricerca_banca_for.enabled = false

	cb_numero_doc.enabled = false

	if this.getrow() > 0 and this.getitemnumber(this.getrow(), "anno_registrazione") > 0 then
		cb_dettaglio.enabled = true
		cb_contro.enabled = true
		cb_scadenze.enabled = true
		cb_iva.enabled = true
		cb_corrispondenze.enabled = true
		cb_note.enabled = true
		cb_calcola.enabled = true
	else
		cb_dettaglio.enabled = false
		cb_contro.enabled = false
		cb_scadenze.enabled = false
		cb_iva.enabled = false
		cb_corrispondenze.enabled = false
		cb_note.enabled = false
		cb_calcola.enabled = true
	end if
	
end if


end event

event buttonclicked;call super::buttonclicked;long			ll_num_reg
integer		li_anno_reg, li_ret
string			ls_errore


choose case dwo.name

	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_tes_fat_acq_det_1,"cod_fornitore")


	case "b_ricerca_banca_for"
		guo_ricerca.uof_ricerca_banca_clifor(dw_tes_fat_acq_det_1,"cod_banca_clien_for")


	case "b_fiscale"
		if row>0 then
			li_anno_reg = dw_tes_fat_acq_det_1.getitemnumber(row, "anno_registrazione")
			ll_num_reg = dw_tes_fat_acq_det_1.getitemnumber(row, "num_registrazione")
			
			if li_anno_reg>0 and ll_num_reg>0 then
			
				li_ret = wf_assegna_num_fiscale(row, li_anno_reg, ll_num_reg, ls_errore)
				
				if li_ret<0 then
					//errore (rollback già fatto)
					g_mb.error(ls_errore)
					return
					
				elseif li_ret > 0 then
					//warning (es. fattura inesistente)
					g_mb.warning(ls_errore)
					return
					
				else
					//tutto OK
				end if
				
				return
				
			else
				g_mb.warning("Selezionare una fattura di acquisto!")
				return
			end if
			
		end if
		
end choose
end event

event pcd_modify;call super::pcd_modify;dw_tes_fat_acq_det_1.object.b_ricerca_banca_for.enabled = true
dw_tes_fat_acq_det_1.object.b_ricerca_fornitore.enabled = true

object.b_fiscale.enabled = false
end event

event ue_key;call super::ue_key;
if key = KeyA! and keyflags = 3 then	
	//CTRL + SHIFT + A
	postevent("ue_acquista_fatt_ven")
	
end if

end event

event pcd_delete;call super::pcd_delete;


object.b_fiscale.enabled = false
end event

type cb_1 from commandbutton within w_tes_fat_acq_lista
integer x = 3909
integer y = 1820
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report Fatt."
end type

event clicked;long ll_riga

ll_riga = dw_tes_fat_acq_det_1.getrow()

if ll_riga > 0 then

	s_cs_xx.parametri.parametro_d_1 = dw_tes_fat_acq_det_1.getitemnumber( ll_riga, "anno_registrazione")
	
	s_cs_xx.parametri.parametro_d_2 = dw_tes_fat_acq_det_1.getitemnumber( ll_riga, "num_registrazione")
	
	window_open_parm(w_report_fat_acq_cc, -1, dw_tes_fat_acq_det_1)
	
end if

end event

type cb_calcola from commandbutton within w_tes_fat_acq_lista
integer x = 3520
integer y = 2080
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Calcola"
end type

event clicked;string ls_messaggio

long   ll_num_registrazione, ll_anno_registrazione

uo_calcola_documento_euro luo_doc


if dw_tes_fat_acq_det_1.rowcount() < 1 then
	g_mb.messagebox("Fatture di Acquisto", "nessuna fattura selezionata: calcolo bloccato")
	return
end if

ll_anno_registrazione = dw_tes_fat_acq_det_1.getitemnumber(dw_tes_fat_acq_det_1.getrow(),"anno_registrazione")
ll_num_registrazione = dw_tes_fat_acq_det_1.getitemnumber(dw_tes_fat_acq_det_1.getrow(),"num_registrazione")

luo_doc = create uo_calcola_documento_euro

if luo_doc.uof_calcola_documento(ll_anno_registrazione, ll_num_registrazione,"fat_acq",ls_messaggio) <> 0 then
	g_mb.messagebox("Apice",ls_messaggio)
	destroy luo_doc
	rollback;
	return -1
else
	destroy luo_doc
	commit;
end if

dw_tes_fat_acq_det_1.change_dw_current()
cb_ricerca.triggerevent("clicked")
//dw_tes_fat_acq_lista.SetRow(il_numero_riga)
end event

type cb_carico_bolle from commandbutton within w_tes_fat_acq_lista
integer x = 1623
integer y = 1840
integer width = 366
integer height = 80
integer taborder = 140
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C&arica Bolle"
end type

event clicked;string ls_cod_fornitore, ls_cod_doc_origine, &
		 ls_numeratore_doc_origine
integer li_row, li_anno_doc_origine
long ll_progressivo, ll_num_doc_origine, ll_anno_registrazione, ll_num_registrazione

li_row = dw_tes_fat_acq_det_1.getrow()

if dw_tes_fat_acq_det_1.getitemstring(li_row, "flag_fat_confermata") = "S" then
   g_mb.messagebox("Attenzione", "Fattura già confermata.", &
              exclamation!, ok!)
   return
end if

if li_row > 0 then
	ll_anno_registrazione = dw_tes_fat_acq_det_1.getitemnumber(li_row, "anno_registrazione")
	ll_num_registrazione = dw_tes_fat_acq_det_1.getitemnumber(li_row, "num_registrazione")
	ls_cod_fornitore = dw_tes_fat_acq_det_1.getitemstring(li_row, "cod_fornitore")
	s_cs_xx.parametri.parametro_s_14 = ls_cod_fornitore
	s_cs_xx.parametri.parametro_ul_3 = ll_anno_registrazione
	s_cs_xx.parametri.parametro_ul_2 = ll_num_registrazione
	
	window_open(w_gen_fat_acq, -1)
	
	setnull(s_cs_xx.parametri.parametro_s_15)
	setnull(s_cs_xx.parametri.parametro_s_14)
	setnull(s_cs_xx.parametri.parametro_s_13)
	setnull(s_cs_xx.parametri.parametro_i_1)
	setnull(s_cs_xx.parametri.parametro_ul_3)
	setnull(s_cs_xx.parametri.parametro_ul_2)
end if
end event

type cb_carico_ordini from commandbutton within w_tes_fat_acq_lista
integer x = 1623
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 130
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C&arica Ord."
end type

event clicked;string ls_cod_fornitore, ls_cod_doc_origine, &
		 ls_numeratore_doc_origine
integer li_row, li_anno_doc_origine
long ll_progressivo, ll_num_doc_origine, ll_anno_registrazione, ll_num_registrazione

li_row = dw_tes_fat_acq_det_1.getrow()

li_row = dw_tes_fat_acq_det_1.getrow()

if dw_tes_fat_acq_det_1.getitemstring(li_row, "flag_fat_confermata") = "S" then
   g_mb.messagebox("Attenzione", "Fattura già confermata.", &
              exclamation!, ok!)
   return
end if

if li_row > 0 then
	ls_cod_fornitore = dw_tes_fat_acq_det_1.getitemstring(li_row, "cod_fornitore")
	ll_anno_registrazione = dw_tes_fat_acq_det_1.getitemnumber(li_row, "anno_registrazione")
	ll_num_registrazione = dw_tes_fat_acq_det_1.getitemnumber(li_row, "num_registrazione")
	

	s_cs_xx.parametri.parametro_s_14 = ls_cod_fornitore
	s_cs_xx.parametri.parametro_s_12 = "Fattura"
	s_cs_xx.parametri.parametro_ul_3 = ll_anno_registrazione
	s_cs_xx.parametri.parametro_ul_2 = ll_num_registrazione
	
	window_open(w_carico_acquisti, -1)

	setnull(s_cs_xx.parametri.parametro_s_14)
	setnull(s_cs_xx.parametri.parametro_s_12)
	setnull(s_cs_xx.parametri.parametro_i_1)
	setnull(s_cs_xx.parametri.parametro_ul_3)
	setnull(s_cs_xx.parametri.parametro_ul_2)
end if

end event

type cb_conferma_bolla from commandbutton within w_tes_fat_acq_lista
integer x = 3131
integer y = 2080
integer width = 366
integer height = 80
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Con&ferma"
end type

event clicked;integer li_row, li_ret
string ls_cod_doc_origine, ls_numeratore_doc_origine, ls_messaggio
long ll_anno_doc_origine, ll_num_doc_origine, ll_progressivo, ll_anno_registrazione, ll_num_registrazione, ll_numero_riga
	
li_row = dw_tes_fat_acq_det_1.getrow()
if dw_tes_fat_acq_det_1.getitemstring(li_row, "flag_fat_confermata") = 'N' then

ll_anno_registrazione = dw_tes_fat_acq_det_1.getitemnumber(li_row, "anno_registrazione")
ll_num_registrazione = dw_tes_fat_acq_det_1.getitemnumber(li_row, "num_registrazione")
	
	li_ret = f_conferma_fat_acq_new( ll_anno_registrazione, ll_num_registrazione, &
											ref ls_messaggio, "?" )
											
	if li_ret = -1 then
		rollback;
		g_mb.messagebox("Conferma Fatture Acquisto", ls_messaggio, exclamation!)
	elseif li_ret = 0 then
		commit;
		
		ll_numero_riga = dw_tes_fat_acq_det_1.getrow()
		dw_tes_fat_acq_det_1.change_dw_current()
		parent.triggerevent("pc_retrieve")
	end if
else
	g_mb.messagebox("Conferma Fatture Acquisto", "Fattura Già Confermata", exclamation!)
end if
end event

type cb_contro from commandbutton within w_tes_fat_acq_lista
integer x = 3177
integer y = 1720
integer width = 366
integer height = 80
integer taborder = 50
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Contropar."
end type

event clicked;window_open_parm(w_cont_fat_acq, -1, dw_tes_fat_acq_det_1)

end event

type cb_corrispondenze from commandbutton within w_tes_fat_acq_lista
integer x = 1623
integer y = 2080
integer width = 366
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corrispond."
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "Fat_Acq"
window_open_parm(w_acq_ven_corr, -1, dw_tes_fat_acq_det_1)

end event

type cb_dettaglio from commandbutton within w_tes_fat_acq_lista
integer x = 3909
integer y = 2080
integer width = 366
integer height = 80
integer taborder = 80
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Dettagli"
end type

event clicked;long ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = dw_tes_fat_acq_det_1.getitemnumber(dw_tes_fat_acq_det_1.getrow(), "anno_registrazione")
ll_num_registrazione = dw_tes_fat_acq_det_1.getitemnumber(dw_tes_fat_acq_det_1.getrow(), "num_registrazione")

dw_tes_fat_acq_sel.setitem(1, "anno_registrazione", ll_anno_registrazione)

dw_tes_fat_acq_sel.setitem(1, "num_registrazione", ll_num_registrazione)

s_cs_xx.parametri.parametro_w_fat_acq = parent

window_open_parm(w_det_fat_acq, -1, dw_tes_fat_acq_det_1)

end event

type cb_iva from commandbutton within w_tes_fat_acq_lista
integer x = 3543
integer y = 1720
integer width = 366
integer height = 80
integer taborder = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Iva"
end type

event clicked;window_open_parm(w_iva_fat_acq, -1, dw_tes_fat_acq_det_1)

end event

type cb_note from commandbutton within w_tes_fat_acq_lista
integer x = 1623
integer y = 1980
integer width = 366
integer height = 80
integer taborder = 100
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Note"
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "Fat_Acq"
window_open_parm(w_acq_ven_note, -1, dw_tes_fat_acq_det_1)

end event

type cb_numero_doc from commandbutton within w_tes_fat_acq_lista
boolean visible = false
integer x = 1413
integer y = 1764
integer width = 270
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "<<Succ."
end type

event clicked;string ls_cod_tipo_fat_acq, ls_cod_operatore, ls_cod_documento, ls_numeratore_documento
long ll_num_documento, ll_anno_esercizio,ll_cont

ls_cod_tipo_fat_acq = dw_tes_fat_acq_det_1.getitemstring(dw_tes_fat_acq_det_1.getrow(),"cod_tipo_fat_acq")
ls_cod_documento = dw_tes_fat_acq_det_1.getitemstring(dw_tes_fat_acq_det_1.getrow(),"cod_doc_origine")
ls_numeratore_documento = dw_tes_fat_acq_det_1.getitemstring(dw_tes_fat_acq_det_1.getrow(),"numeratore_doc_origine")
if isnull(ls_cod_documento) or isnull(ls_numeratore_documento) then
	select cod_documento,
			 numeratore_documento
	  into :ls_cod_documento,
			 :ls_numeratore_documento
	  from tab_tipi_fat_acq
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_fat_acq = :ls_cod_tipo_fat_acq;
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Apice", "Errore in lettura dati da tabella Tipi Fatture Acquisto.~n~r" + sqlca.sqlerrtext)
		return
	end if		
	if isnull(ls_cod_documento) then
		g_mb.messagebox("Apice", "Verificare che sia stato caricato il tipo documento nella tabella TIPI FATTURE DI ACQUISTO" + sqlca.sqlerrtext)
		return
	end if		
	if isnull(ls_numeratore_documento) then
		g_mb.messagebox("Apice", "Verificare che sia stato caricato il numeratore del documento nella tabella TIPI FATTURE DI ACQUISTO" + sqlca.sqlerrtext)
		return
	end if		
end if

ll_anno_esercizio = f_anno_esercizio()

select num_documento + 1
  into :ll_num_documento
  from numeratori
 where cod_azienda = :s_cs_xx.cod_azienda
	and cod_documento = :ls_cod_documento
	and numeratore_documento = :ls_numeratore_documento
	and anno_documento = :ll_anno_esercizio;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice", "Errore in lettura dati da tabella Numeratori.~n~r" + sqlca.sqlerrtext)
	return
end if						

		
select count(*)
  into :ll_cont
  from tes_fat_acq
 where cod_azienda = :s_cs_xx.cod_azienda
	and cod_doc_origine = :ls_cod_documento
	and numeratore_doc_origine = :ls_numeratore_documento
	and anno_doc_origine = :ll_anno_esercizio
	and num_doc_origine = :ll_num_documento;

if sqlca.sqlcode < 0 then
	g_mb.messagebox("Apice", "Errore in lettura dati da tabella Numeratori.~n~r" + sqlca.sqlerrtext)
	return
end if						
			
			
dw_tes_fat_acq_det_1.setitem(dw_tes_fat_acq_det_1.getrow(), "cod_doc_origine", ls_cod_documento)			
dw_tes_fat_acq_det_1.setitem(dw_tes_fat_acq_det_1.getrow(), "anno_doc_origine", ll_anno_esercizio)			
dw_tes_fat_acq_det_1.setitem(dw_tes_fat_acq_det_1.getrow(), "numeratore_doc_origine", ls_numeratore_documento)
dw_tes_fat_acq_det_1.setitem(dw_tes_fat_acq_det_1.getrow(), "num_doc_origine", ll_num_documento)						

end event

type cb_ricerca from commandbutton within w_tes_fat_acq_lista
integer x = 1143
integer y = 2120
integer width = 379
integer height = 80
integer taborder = 190
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;//dw_tes_fat_acq_sel.fu_BuildSearch(TRUE)
//sle_filtro_data.fu_BuildSearch(false)
//dw_tes_fat_acq_lista.change_dw_current()
//parent.triggerevent("pc_retrieve")
//
//dw_folder.fu_SelectTab(2)

string 		ls_flag_confermate, ls_flag_contabilizzate, ls_flag_cc_manuali, ls_flag_blocco, ls_cod_doc_orgine, ls_numeratore_doc_origine, &
				ls_cod_fornitore, ls_protocollo, ls_cod_tipo_fat_acq, ls_flag_agg_mov

long			ll_anno_registrazione, ll_num_registrazione, ll_anno_doc_origine, ll_num_doc_origine

datetime	ldt_data_registrazione, ldt_data_registrazione_a

dw_tes_fat_acq_sel.accepttext()

ls_flag_confermate = dw_tes_fat_acq_sel.getitemstring(1, "flag_fat_confermata")
ls_flag_contabilizzate = dw_tes_fat_acq_sel.getitemstring(1, "flag_contabilizzazione")
ls_flag_cc_manuali = dw_tes_fat_acq_sel.getitemstring(1, "flag_cc_manuali")

// -------------  compongo SQL con filtri ---------------------

is_sql_filtro = ""

ll_anno_registrazione = dw_tes_fat_acq_sel.getitemnumber(1,"anno_registrazione")
ll_num_registrazione = dw_tes_fat_acq_sel.getitemnumber(1,"num_registrazione")
ls_cod_doc_orgine = dw_tes_fat_acq_sel.getitemstring(1,"cod_doc_orgine")
ll_anno_doc_origine = dw_tes_fat_acq_sel.getitemnumber(1,"anno_doc_origine")
ls_numeratore_doc_origine = dw_tes_fat_acq_sel.getitemstring(1,"numeratore_doc_origine")
ll_num_doc_origine = dw_tes_fat_acq_sel.getitemnumber(1,"num_doc_origine")
ls_cod_fornitore = dw_tes_fat_acq_sel.getitemstring(1,"cod_fornitore")
ls_protocollo =dw_tes_fat_acq_sel.getitemstring(1, "protocollo")
ls_cod_tipo_fat_acq = dw_tes_fat_acq_sel.getitemstring(1, "cod_tipo_fat_acq")
ls_flag_agg_mov = dw_tes_fat_acq_sel.getitemstring(1, "flag_agg_mov")
ldt_data_registrazione = dw_tes_fat_acq_sel.getitemdatetime(1, "data_registrazione")
ldt_data_registrazione_a = dw_tes_fat_acq_sel.getitemdatetime(1, "data_registrazione_a")

if not isnull(ll_anno_registrazione) and ll_anno_registrazione>0 then
	is_sql_filtro += " and anno_registrazione = " + string(ll_anno_registrazione) + " "
end if
if not isnull(ll_num_registrazione) and ll_num_registrazione>0 then
	is_sql_filtro += " and num_registrazione = " + string(ll_num_registrazione) + " "
end if
if not isnull(ls_cod_doc_orgine) and ls_cod_doc_orgine<>"" then
	is_sql_filtro += " and cod_doc_origine = '" + ls_cod_doc_orgine + "' "
end if
if not isnull(ll_anno_doc_origine) and ll_anno_doc_origine>0 then
	is_sql_filtro += " and anno_doc_origine = " + string(ll_anno_doc_origine) + " "
end if
if not isnull(ls_numeratore_doc_origine) and ls_numeratore_doc_origine<>"" then
	is_sql_filtro += " and numeratore_doc_origine = '" + ls_numeratore_doc_origine + "' "
end if
if not isnull(ll_num_doc_origine) and ll_num_doc_origine>0 then
	is_sql_filtro += " and num_doc_origine = " + string(ll_num_doc_origine) + " "
end if
if not isnull(ls_numeratore_doc_origine) and ls_numeratore_doc_origine<>"" then
	is_sql_filtro += " and numeratore_doc_origine = '" + ls_numeratore_doc_origine + "' "
end if
if not isnull(ls_cod_fornitore) and ls_cod_fornitore<>"" then
	is_sql_filtro += " and cod_fornitore = '" + ls_cod_fornitore + "' "
end if
if not isnull(ls_protocollo) and ls_protocollo<>"" then
	is_sql_filtro += " and protocollo like '%" + ls_protocollo + "%' "
end if
if not isnull(ls_cod_tipo_fat_acq) and ls_cod_tipo_fat_acq<>"" then
	is_sql_filtro += " and cod_tipo_fat_acq = '" + ls_cod_tipo_fat_acq + "' "
end if

if not isnull(ldt_data_registrazione) then
	is_sql_filtro += " and data_registrazione >= '" + string(ldt_data_registrazione, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_data_registrazione_a) then
	is_sql_filtro += " and data_registrazione <= '" + string(ldt_data_registrazione_a, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if ls_flag_confermate="S" or ls_flag_confermate='N' then
	is_sql_filtro += " and flag_fat_confermata = '" + ls_flag_confermate + "' "
end if
if ls_flag_contabilizzate="S" or ls_flag_contabilizzate='N' then
	is_sql_filtro += " and flag_contabilita = '" + ls_flag_contabilizzate + "' "
end if
if ls_flag_cc_manuali="S" or ls_flag_cc_manuali='N' then
	is_sql_filtro += " and flag_cc_manuali = '" + ls_flag_cc_manuali + "' "
end if

//N.B. gestore il flag_agg_mov
//gestito nella wf_inserisci_fatture e wf_inserisci singola_fattura
// ------------------------------------------------------------

wf_cancella_treeview()

wf_imposta_treeview()

tv_1.setfocus()

dw_folder_search.fu_SelectTab(2)
dw_folder.fu_SelectTab(1)
end event

type cb_scadenze from commandbutton within w_tes_fat_acq_lista
integer x = 3909
integer y = 1720
integer width = 366
integer height = 80
integer taborder = 40
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Sc&adenze"
end type

event clicked;window_open_parm(w_scad_fat_acq, -1, dw_tes_fat_acq_det_1)

end event

type cb_stampa from commandbutton within w_tes_fat_acq_lista
integer x = 3909
integer y = 1920
integer width = 366
integer height = 80
integer taborder = 201
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;window_open(w_report_fat_acq_euro, -1)
end event

type dw_notifiche from uo_std_dw within w_tes_fat_acq_lista
integer x = 23
integer y = 160
integer width = 1486
integer height = 1940
integer taborder = 200
string dataobject = "d_tes_fat_acq_notifiche_sdi"
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

event doubleclicked;// evento commentato.
// non deve funzionare il doppio click
end event

event buttonclicked;call super::buttonclicked;boolean 	lb_append=false
string		ls_messaggio, ls_str,ls_file_name_xml,ls_file_name_pdf, ls_dir, ls_error, ls_file_pdf, ls_file_ext, ls_file_code
long 		ll_ret, ll_i, ll_row, ll_return
blob 		l_blob, l_blob_allegato
str_fatel_lista_fatture_passive lstr_lista_passive[]
uo_fatel luo_fatel
		

choose case dwo.name
	case "b_download_notifica"
		
		if row <= 0 then return
		if getitemnumber(row, 2) >= 0 then	
			luo_fatel = create uo_fatel
			
			ll_ret = luo_fatel.uof_fattura_passiva_download_xml( getitemnumber(row, 2), ref ls_messaggio)
			if ll_ret = -1 then
				// errore in download della notifica
				g_mb.error(g_str.format("Errore in Download notifica $1.~r~n$2", getitemnumber(row, 2), ls_messaggio))
				destroy luo_fatel
				return
			end if
			if ll_ret = -2 then
				// errore durante memo notifica in Apice
				g_mb.warning(g_str.format("Anomalia in lettura notifica  $1.~r~n$2", getitemnumber(row, 2), ls_messaggio))
			end if
			
			ll_ret = luo_fatel.uof_fattura_passiva_download_pdf( getitemnumber(row, 2), ref ls_file_pdf, ref l_blob)
			if ll_ret = -1 then
				// errore in download della notifica
				g_mb.error(g_str.format("Errore in Download notifica $1.~r~n$2", getitemnumber(row, 2), ls_file_pdf))
				destroy luo_fatel
				return
			end if
			if ll_ret = -2 then
				// errore durante memo notifica in Apice
				g_mb.warning(g_str.format("Anomalia in lettura notifica  $1.~r~n$2", getitemnumber(row, 2), ls_file_pdf))
			end if
			ls_file_name_pdf=guo_functions.uof_get_random_filename( ) + ".pdf"
			ls_file_name_xml=guo_functions.uof_get_random_filename( ) + ".xml"
			ls_dir = guo_functions.uof_get_user_temp_folder( )

			ll_ret = guo_functions.uof_blob_to_file( l_blob, ls_dir + ls_file_name_pdf)
			
			if lower(left(ls_messaggio, 5)) <> "<?xml" then
				// Sistemazione per casi di fatture senza l'intestazione, come ad esempio AMAZON AWS
				ls_messaggio = '<?xml version="1.0" encoding="utf-8" standalone="no"?>' + ls_messaggio
			end if
			
			guo_functions.uof_file_write_encoding(ls_dir + ls_file_name_xml, ls_messaggio, lb_append, EncodingUTF8!, ls_error)
			
			destroy luo_fatel
			
			// Apro la finestra in cui visualizzare la fattura			
			s_cs_xx.parametri.parametro_s_15 = ls_dir + ls_file_name_xml
			s_cs_xx.parametri.parametro_s_14 = ls_dir + ls_file_name_pdf 
			s_cs_xx.parametri.parametro_ul_1 = getitemnumber(row, 2)
			window_open_parm(w_tes_fat_acq_fatel, -1, dw_tes_fat_acq_det_1)
			
			return
		end if
		
		// leggo lista fatture presenti in SDI non ancora scaricate

case "b_notifiche"
		
		luo_fatel = create uo_fatel
		
		ll_ret = luo_fatel.uof_fatture_passive_lista( ref ls_messaggio, lstr_lista_passive[] )

		destroy luo_fatel

		choose case ll_ret
			case 1
				g_mb.show(ls_messaggio)
				return
			case -1
				g_mb.error(ls_messaggio)
				return
			case 0
				this.reset( )
				this.setredraw(false)
				for ll_i = 1 to upperbound(lstr_lista_passive)
					
					ll_row = insertrow(0)
					
					ls_str = g_str.format("$1 ($2)~r~nIdentificativo SDI Fattura=$3~r~nRicezione:$4",lstr_lista_passive[ll_i].denominazione_fornitore, lstr_lista_passive[ll_i].partita_iva, lstr_lista_passive[ll_i].identificativo_sdi, lstr_lista_passive[ll_i].dataOraRicezione  )
					
					setitem(ll_row, 1, ls_str)
					setitem(ll_row, 2, long(lstr_lista_passive[ll_i].id))
					
				next
				this.setredraw(true)
		end choose		
end choose

end event

type dw_tes_fat_acq_sel from u_dw_search within w_tes_fat_acq_lista
event ue_reset ( )
integer x = 32
integer y = 144
integer width = 1193
integer height = 1960
integer taborder = 160
string dataobject = "d_tes_fat_acq_sel"
boolean border = false
end type

event ue_reset();dw_tes_fat_acq_sel.resetupdate()
end event

event itemchanged;call super::itemchanged;choose case this.getcolumnname()
	case "cod_cliente"
		f_po_loaddddw_dw(this, &
								  "cod_des_cliente", &
								  sqlca, &
								  "anag_des_clienti", &
								  "cod_des_cliente", &
								  "rag_soc_1", &
								  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + data + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
								  
	case "flag_tipo_livello_1"
		setitem(row, "flag_tipo_livello_2", "N")
		setitem(row, "flag_tipo_livello_3", "N")
		settaborder("flag_tipo_livello_2", 0)
		settaborder("flag_tipo_livello_3", 0)
		if data <> "N" then
			settaborder("flag_tipo_livello_2", 150)
		end if
		
	case "flag_tipo_livello_2"
		setitem(row, "flag_tipo_livello_3", "N")
		settaborder("flag_tipo_livello_3", 0)
		if data <> "N" then
			settaborder("flag_tipo_livello_3", 160)
		end if								  
								  
end choose

end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_tes_fat_acq_sel, "cod_fornitore")
		
end choose
end event

type dw_folder_search from u_folder within w_tes_fat_acq_lista
integer y = 20
integer width = 1531
integer height = 2200
integer taborder = 30
end type

type tv_1 from treeview within w_tes_fat_acq_lista
integer x = 23
integer y = 160
integer width = 1486
integer height = 1960
integer taborder = 190
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long backcolor = 16777215
boolean border = false
boolean linesatroot = true
long picturemaskcolor = 16777215
long statepicturemaskcolor = 16777215
end type

event itempopulate;string ls_null
long ll_livello = 0
treeviewitem ltv_item
ws_record lws_record

setnull(ls_null)

if isnull(handle) or handle <= 0 or ib_tree_deleting then
	return 0
end if

getitem(handle,ltv_item)

lws_record = ltv_item.data
ll_livello = lws_record.livello

il_livello_corrente = ll_livello

if ll_livello < 3 then
	// controlla cosa c'è al livello successivo ???
	choose case dw_tes_fat_acq_sel.getitemstring(dw_tes_fat_acq_sel.getrow(),"flag_tipo_livello_" + string(ll_livello + 1))
		case "T"
			// caricamento tipi fatture vendita
			if wf_inserisci_tipi_fatture_acquisto(handle, dw_tes_fat_acq_sel.getitemstring(dw_tes_fat_acq_sel.getrow(),"cod_tipo_fat_acq")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "A"
			// caricamento anni registrazione
			if wf_inserisci_anni(handle, dw_tes_fat_acq_sel.getitemnumber(dw_tes_fat_acq_sel.getrow(),"anno_registrazione")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "F"
			// caricamento fornitori
			if wf_inserisci_fornitori(handle, dw_tes_fat_acq_sel.getitemstring(dw_tes_fat_acq_sel.getrow(),"cod_fornitore")) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case "N" 
			if wf_inserisci_fatture(handle, ls_null) = 1 then
				ltv_item.children = false
				tv_1.setitem(handle, ltv_item)
			end if
			
		case else
			ltv_item.children = false
			setitem(handle, ltv_item)
			
	end choose
else
	// inserisco le registrazioni
	if wf_inserisci_fatture(handle, ls_null) = 1 then
		ltv_item.children = false
		tv_1.setitem(handle, ltv_item)
	end if
	
end if

commit;
end event

event selectionchanged;treeviewitem ltv_item
ws_record lws_record

if isnull(newhandle) or newhandle <= 0 or ib_tree_deleting then
	return 0
end if
il_handle = newhandle
getitem(newhandle,ltv_item)
lws_record = ltv_item.data

dw_tes_fat_acq_det_1.change_dw_current()

parent.postevent("pc_retrieve")

end event

event key;long	ll_handle
string ls_messaggio, ls_flag_supervisore, ls_flag_movimenti, ls_stato_flag
treeviewitem ltv_item
ws_record lws_record
uo_calcola_documento_euro luo_calcola_documento_euro

if key = KeyX! and keyflags=3 and s_cs_xx.cod_utente = "CS_SYSTEM" then
	
	if s_cs_xx.cod_utente <> "CS_SYSTEM" then
		select flag_supervisore
		into	:ls_flag_supervisore
		from	utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode <> 0 then ls_flag_supervisore = "N"
	
		if ls_flag_supervisore = "N" then
			g_mb.warning("Funzione utilizzabile solo da utenti Supervisori.")
			return
		end if
	end if
	
	if g_mb.confirm("Ricalcolare tutte le fatture nella lista?") then

		ll_handle = tv_1.FindItem(CurrentTreeItem!, 0)
		
		setpointer(HourGlass!)
		
		do while true
			
			if ll_handle <= 0 or isnull(ll_handle) then exit
			tv_1.getitem(ll_handle, ltv_item)
			
			lws_record = ltv_item.data
			
			if lws_record.anno_registrazione > 0 and lws_record.num_registrazione > 0 then
			
				luo_calcola_documento_euro = create uo_calcola_documento_euro
				
				luo_calcola_documento_euro.ib_salta_esposiz_cliente = true
				
				w_cs_xx_mdi.setmicrohelp("Calcolo fattura " + string(lws_record.anno_registrazione) + "-"  + string(lws_record.num_registrazione))
				Yield()
				
				if luo_calcola_documento_euro.uof_calcola_documento(lws_record.anno_registrazione, lws_record.num_registrazione,"fat_acq",ls_messaggio) <> 0 then
					if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.messagebox("APICE",ls_messaggio)
					rollback;
				else
					commit;
				end if
				destroy luo_calcola_documento_euro
			end if
			
			ll_handle = tv_1.finditem( NextTreeItem! ,ll_handle )

		loop
		w_cs_xx_mdi.setmicrohelp("Elaborazione Eseguita Correttamente; calcolo terminato!")
		setpointer(Arrow!)

	end if

end if


if key = KeyM! and keyflags=2 then
	
	if s_cs_xx.cod_utente <> "CS_SYSTEM" then
		select flag_supervisore
		into	:ls_flag_supervisore
		from	utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode <> 0 then ls_flag_supervisore = "N"
	
		if ls_flag_supervisore = "N" then
			g_mb.warning("Funzione utilizzabile solo da utenti Supervisori.")
			return
		end if
	end if
	
	ll_handle = tv_1.FindItem(CurrentTreeItem!, 0)
	
	if ll_handle <= 0 or isnull(ll_handle) then return
	tv_1.getitem(ll_handle, ltv_item)
	
	lws_record = ltv_item.data
	
	if lws_record.anno_registrazione > 0 and lws_record.num_registrazione > 0 then
		select flag_fat_confermata
		into	:ls_flag_movimenti
		from	tes_fat_acq
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lws_record.anno_registrazione and
				num_registrazione = :lws_record.num_registrazione;
		if sqlca.sqlcode <> 0 then
			g_mb.error("Fattura selezionata non trovata")
			return
		end if
		
		if ls_flag_movimenti = "S" then
			if g_mb.confirm("Sei sicuro di voler togliere check CONFERMATA dalla fattura? L'operazione sarà registrata in un LOG") then
				ls_stato_flag="N"
			else
				return
			end if
		else
			if g_mb.confirm("Sei sicuro di voler applicare il FLAG CONFERMATA alla fattura? L'operazione sarà registrata in un LOG") then
				ls_stato_flag="S"
			else
				return
			end if
		end if		
		
		f_scrivi_log( g_str.format( "FLAG CONFERMA portato allo stato '$3' nella FATTURA num int. [$1 / $2]", lws_record.anno_registrazione, lws_record.num_registrazione, ls_stato_flag))
		
		
		update tes_fat_acq
		set flag_fat_confermata = :ls_stato_flag
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lws_record.anno_registrazione and
				num_registrazione = :lws_record.num_registrazione;
		if sqlca.sqlcode = 0 then
			g_mb.show("'Flag Fattura Confermata' rimosso con successo!")
			dw_tes_fat_acq_det_1.change_dw_current()
			parent.triggerevent("pc_retrieve")
			commit;
		else
			g_mb.error("Errore SQL in modifica 'Flag Fattura Confermata'")
			rollback;
			return
		end if
	end if
end if

if key = KeyI! and keyflags=2 then
	
	if s_cs_xx.cod_utente <> "CS_SYSTEM" then
		select flag_supervisore
		into	:ls_flag_supervisore
		from	utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode <> 0 then ls_flag_supervisore = "N"
	
		if ls_flag_supervisore = "N" then
			g_mb.warning("Funzione utilizzabile solo da utenti Supervisori.")
			return
		end if
	end if
	
	ll_handle = tv_1.FindItem(CurrentTreeItem!, 0)
	
	if ll_handle <= 0 or isnull(ll_handle) then return
	tv_1.getitem(ll_handle, ltv_item)
	
	lws_record = ltv_item.data
	
	
	if lws_record.anno_registrazione > 0 and lws_record.num_registrazione > 0 then
		select flag_contabilita
		into	:ls_flag_movimenti
		from	tes_fat_acq
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lws_record.anno_registrazione and
				num_registrazione = :lws_record.num_registrazione;
		if sqlca.sqlcode <> 0 then
			g_mb.error("Fattura selezionata non trovata")
			return
		end if
		
		if ls_flag_movimenti = "S" then
			if g_mb.confirm("Sei sicuro di voler togliere check CONTABILIZZATA dalla fattura? L'operazione sarà registrata in un LOG") then
				ls_stato_flag="N"
			else
				return
			end if
		else
			if g_mb.confirm("Sei sicuro di voler applicare il FLAG CONTABILIZZATA alla fattura? L'operazione sarà registrata in un LOG") then
				ls_stato_flag="S"
			else
				return
			end if
		end if		
		
		f_scrivi_log( g_str.format( "FLAG CONTABILIZZATA portato allo stato '$3' nella FATTURA num int. [$1 / $2]",  lws_record.anno_registrazione,  lws_record.num_registrazione, ls_stato_flag))
		
		
		update tes_fat_acq
		set flag_contabilita = :ls_stato_flag
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lws_record.anno_registrazione and
				num_registrazione = :lws_record.num_registrazione;
		if sqlca.sqlcode = 0 then
			g_mb.show("'Flag Fattura Contabilizzata' rimosso con successo!")
			dw_tes_fat_acq_det_1.change_dw_current()
			parent.triggerevent("pc_retrieve")
			commit;
			// LOG
		else
			g_mb.error("Errore SQL in modifica 'Flag Fattura Contabilizzata'")
			rollback;
			return
		end if
	end if
end if

end event

