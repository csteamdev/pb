﻿$PBExportHeader$w_calcola_fat_ven.srw
forward
global type w_calcola_fat_ven from w_cs_xx_principale
end type
type st_3 from statictext within w_calcola_fat_ven
end type
type st_2 from statictext within w_calcola_fat_ven
end type
type cb_provvigioni from commandbutton within w_calcola_fat_ven
end type
type cb_calcola from commandbutton within w_calcola_fat_ven
end type
type st_1 from statictext within w_calcola_fat_ven
end type
type cb_1 from commandbutton within w_calcola_fat_ven
end type
type dw_calcola_fat_ven_2 from datawindow within w_calcola_fat_ven
end type
type dw_calcola_fat_ven_1 from datawindow within w_calcola_fat_ven
end type
type gb_1 from groupbox within w_calcola_fat_ven
end type
end forward

global type w_calcola_fat_ven from w_cs_xx_principale
integer width = 3095
integer height = 1960
string title = "Ricalcolo Fatture"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
st_3 st_3
st_2 st_2
cb_provvigioni cb_provvigioni
cb_calcola cb_calcola
st_1 st_1
cb_1 cb_1
dw_calcola_fat_ven_2 dw_calcola_fat_ven_2
dw_calcola_fat_ven_1 dw_calcola_fat_ven_1
gb_1 gb_1
end type
global w_calcola_fat_ven w_calcola_fat_ven

type variables
string is_sql_hold
end variables

forward prototypes
public function integer wf_set_sql_sintax ()
end prototypes

public function integer wf_set_sql_sintax ();string   ls_flag_contabilita, ls_cod_deposito, ls_cod_agente_1, ls_cod_agente_2, ls_sql, ls_order_by

datetime ldt_da, ldt_a

long     ll_tot, ll_anno_documento, ll_num_documento



dw_calcola_fat_ven_1.accepttext()

ls_order_by = 	"order by tes_fat_ven.cod_documento,"+&
								"tes_fat_ven.numeratore_documento,"+&
								"tes_fat_ven.anno_documento,"+&
								"tes_fat_ven.num_documento,"+&
								"tes_fat_ven.data_fattura "


ls_sql = is_sql_hold
ls_sql += " where tes_fat_ven.cod_azienda='"+s_cs_xx.cod_azienda+"' "

//---------------------------------------------------
ll_anno_documento = dw_calcola_fat_ven_1.getitemnumber(1,"anno_fattura")
if not isnull(ll_anno_documento) and ll_anno_documento > 0 then
	ls_sql += " and tes_fat_ven.anno_documento = " + string(ll_anno_documento) + "  "
end if

//---------------------------------------------------
ll_num_documento = dw_calcola_fat_ven_1.getitemnumber(1,"num_fattura")
if not isnull(ll_num_documento) and ll_num_documento > 0 then
	ls_sql += " and tes_fat_ven.num_documento = " + string(ll_num_documento) + "  "
end if

//---------------------------------------------------
ldt_da = dw_calcola_fat_ven_1.getitemdatetime(1,"data_da")
if not isnull(ldt_da) and year(date(ldt_da))>1950 then
	ls_sql += " and tes_fat_ven.data_registrazione>='" + string(ldt_da, s_cs_xx.db_funzioni.formato_data) + "' "
end if

//---------------------------------------------------
ldt_a = dw_calcola_fat_ven_1.getitemdatetime(1,"data_a")
if not isnull(ldt_da) and year(date(ldt_da))>1950 then
	ls_sql += " and tes_fat_ven.data_registrazione<='" + string(ldt_a, s_cs_xx.db_funzioni.formato_data) + "' "
end if

//---------------------------------------------------
ls_flag_contabilita = dw_calcola_fat_ven_1.getitemstring(1,"flag_contabilita")
if ls_flag_contabilita <> "T" and not isnull(ls_flag_contabilita) and ls_flag_contabilita<>"" then
	ls_sql += " and tes_fat_ven.flag_contabilita='"+ls_flag_contabilita+"'"
end if

//---------------------------------------------------
ls_cod_deposito = dw_calcola_fat_ven_1.getitemstring(1,"cod_deposito")
if not isnull(ls_cod_deposito) and ls_cod_deposito <> ""  then
	ls_sql += " and tes_fat_ven.cod_deposito='"+ls_cod_deposito+"'"
end if

//---------------------------------------------------
ls_cod_agente_1 = dw_calcola_fat_ven_1.getitemstring(1,"cod_agente_1")
if not isnull(ls_cod_agente_1) and ls_cod_agente_1 <> ""  then
	ls_sql += " and tes_fat_ven.cod_agente_1='"+ls_cod_agente_1+"'"
end if

//---------------------------------------------------
ls_cod_agente_2 = dw_calcola_fat_ven_1.getitemstring(1,"cod_agente_2")
if not isnull(ls_cod_agente_2) and ls_cod_agente_2 <> ""  then
	ls_sql += " and tes_fat_ven.cod_agente_2='"+ls_cod_agente_2+"'"
end if

ls_sql += " " + ls_order_by

if dw_calcola_fat_ven_2.setsqlselect(ls_sql)<0 then
	g_mb.error("APICE","Errore in creazione sintassi datawindow: "+ls_sql)
	return -1
end if

ll_tot = dw_calcola_fat_ven_2.retrieve()

return ll_tot
end function

event pc_setwindow;call super::pc_setwindow;dw_calcola_fat_ven_1.insertrow(0)

dw_calcola_fat_ven_2.settransobject(sqlca)

cb_calcola.enabled=false
cb_provvigioni.enabled=false

is_sql_hold = dw_calcola_fat_ven_2.getsqlselect()
end event

on w_calcola_fat_ven.create
int iCurrent
call super::create
this.st_3=create st_3
this.st_2=create st_2
this.cb_provvigioni=create cb_provvigioni
this.cb_calcola=create cb_calcola
this.st_1=create st_1
this.cb_1=create cb_1
this.dw_calcola_fat_ven_2=create dw_calcola_fat_ven_2
this.dw_calcola_fat_ven_1=create dw_calcola_fat_ven_1
this.gb_1=create gb_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_3
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.cb_provvigioni
this.Control[iCurrent+4]=this.cb_calcola
this.Control[iCurrent+5]=this.st_1
this.Control[iCurrent+6]=this.cb_1
this.Control[iCurrent+7]=this.dw_calcola_fat_ven_2
this.Control[iCurrent+8]=this.dw_calcola_fat_ven_1
this.Control[iCurrent+9]=this.gb_1
end on

on w_calcola_fat_ven.destroy
call super::destroy
destroy(this.st_3)
destroy(this.st_2)
destroy(this.cb_provvigioni)
destroy(this.cb_calcola)
destroy(this.st_1)
destroy(this.cb_1)
destroy(this.dw_calcola_fat_ven_2)
destroy(this.dw_calcola_fat_ven_1)
destroy(this.gb_1)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_calcola_fat_ven_1, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_calcola_fat_ven_1, &
				"cod_agente_1", &
				sqlca, &
                       "anag_agenti", &
							  "cod_agente", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
							  
f_po_loaddddw_dw(dw_calcola_fat_ven_1, &
				"cod_agente_2", &
				sqlca, &
                       "anag_agenti", &
							  "cod_agente", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")


end event

type st_3 from statictext within w_calcola_fat_ven
integer x = 2423
integer y = 540
integer width = 603
integer height = 280
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = ">>> E~' possibile calcolare le provvigioni anche se la fattura è già contabilizzata."
boolean focusrectangle = false
end type

type st_2 from statictext within w_calcola_fat_ven
integer x = 2423
integer y = 260
integer width = 617
integer height = 220
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = ">>> Funziona solo per le fatture non ancora contabilizzate."
boolean focusrectangle = false
end type

type cb_provvigioni from commandbutton within w_calcola_fat_ven
integer x = 2011
integer y = 560
integer width = 411
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "PROVVIGIONI"
end type

event clicked;string   ls_messaggio//, ls_flag_contabilita,ls_cod_deposito

//datetime ldt_da, ldt_a

long     ll_i, ll_anno, ll_num, ll_count = 0

uo_calcola_documento_euro luo_calcolo


setpointer(hourglass!)

enabled = false

st_1.text = "Lettura elenco fatture..."

//dw_calcola_fat_ven_1.accepttext()
//ldt_da = dw_calcola_fat_ven_1.getitemdatetime(1,"data_da")
//ldt_a = dw_calcola_fat_ven_1.getitemdatetime(1,"data_a")
//ls_flag_contabilita = dw_calcola_fat_ven_1.getitemstring(1,"flag_contabilita")
//ls_cod_deposito = dw_calcola_fat_ven_1.getitemstring(1,"cod_deposito")
//
//if ls_flag_contabilita = "T" then
//	ls_flag_contabilita = "%"
//end if
//
//if isnull(ls_cod_deposito) or len(ls_cod_deposito) < 1  then
//	ls_cod_deposito = "%"
//end if
//
//dw_calcola_fat_ven_2.retrieve(s_cs_xx.cod_azienda,ldt_da,ldt_a, ls_flag_contabilita,ls_cod_deposito)

if wf_set_sql_sintax() < 0 then
	//messaggio di errore già dato
	st_1.text = "Operazione terminata a causa di errori"
	enabled = true
	setpointer(arrow!)
	
	return
end if


luo_calcolo = create uo_calcola_documento_euro

st_1.text = "Calcolo fatture in corso..."

for ll_i = 1 to dw_calcola_fat_ven_2.rowcount()
	
	ll_count ++
	
	dw_calcola_fat_ven_2.setrow(ll_i)
	dw_calcola_fat_ven_2.scrolltorow(ll_i)
	
	ll_anno = dw_calcola_fat_ven_2.getitemnumber(ll_i,"anno_registrazione")
	ll_num = dw_calcola_fat_ven_2.getitemnumber(ll_i,"num_registrazione")
	
	luo_calcolo.il_anno_registrazione = ll_anno
	luo_calcolo.il_num_registrazione  = ll_num
	luo_calcolo.is_tipo_documento = "fat_ven"
		
	if luo_calcolo.uof_calcola_provvigioni( ref ls_messaggio) <> 0 then
		rollback;
		g_mb.messagebox("APICE", "FATTURA:" + string(ll_anno) + "/" + string(ll_num) + "~r~n" +   ls_messaggio)
		ll_count = -1
		exit;
	else
		commit;
		f_scrivi_log("Eseguito ricacolo provvigioni dalla finestra w_calcola_fat_ven della fattura " + string(ll_anno) + "/" + string(ll_num) )
	end if
		
next

destroy luo_calcolo
dw_calcola_fat_ven_2.reset()

if ll_count = -1 then
	st_1.text = "Operazione terminata a causa di errori"
elseif ll_count = 0 then
	st_1.text = "Nessuna fattura trovato"
elseif ll_count > 0 then
	st_1.text = "Fine operazione: " + string(ll_count) + " fatture calcolate"
end if

enabled = true

setpointer(arrow!)
end event

type cb_calcola from commandbutton within w_calcola_fat_ven
integer x = 2011
integer y = 280
integer width = 411
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "CALCOLA"
end type

event clicked;string   ls_messaggio//, ls_flag_contabilita,ls_cod_deposito

//datetime ldt_da, ldt_a

long     ll_i, ll_anno, ll_num, ll_count = 0

uo_calcola_documento_euro luo_calcolo


setpointer(hourglass!)
enabled = false

st_1.text = "Lettura elenco fatture..."

//dw_calcola_fat_ven_1.accepttext()
//ldt_da = dw_calcola_fat_ven_1.getitemdatetime(1,"data_da")
//ldt_a = dw_calcola_fat_ven_1.getitemdatetime(1,"data_a")
//ls_flag_contabilita = dw_calcola_fat_ven_1.getitemstring(1,"flag_contabilita")
//ls_cod_deposito = dw_calcola_fat_ven_1.getitemstring(1,"cod_deposito")
//
//if ls_flag_contabilita = "T" then
//	ls_flag_contabilita = "%"
//end if
//
//if isnull(ls_cod_deposito) or len(ls_cod_deposito) < 1  then
//	ls_cod_deposito = "%"
//end if
//
//dw_calcola_fat_ven_2.retrieve(s_cs_xx.cod_azienda,ldt_da,ldt_a, ls_flag_contabilita,ls_cod_deposito)

if wf_set_sql_sintax() < 0 then
	//messaggio di errore già dato
	st_1.text = "Operazione terminata a causa di errori"
	enabled = true
	setpointer(arrow!)
	
	return
end if

luo_calcolo = create uo_calcola_documento_euro

st_1.text = "Calcolo fatture in corso..."

for ll_i = 1 to dw_calcola_fat_ven_2.rowcount()
	
	ll_count ++
	
	dw_calcola_fat_ven_2.setrow(ll_i)
	
	dw_calcola_fat_ven_2.scrolltorow(ll_i)
	
	ll_anno = dw_calcola_fat_ven_2.getitemnumber(ll_i,"anno_registrazione")
	
	ll_num = dw_calcola_fat_ven_2.getitemnumber(ll_i,"num_registrazione")
		
	if luo_calcolo.uof_calcola_documento(ll_anno,ll_num,"fat_ven",ls_messaggio) <> 0 then
		rollback;
		g_mb.messagebox("APICE",ls_messaggio)
		ll_count = -1
		exit;
	else
		commit;
	end if
		
next

destroy luo_calcolo

dw_calcola_fat_ven_2.reset()

if ll_count = -1 then
	st_1.text = "Operazione terminata a causa di errori"
elseif ll_count = 0 then
	st_1.text = "Nessuna fattura trovato"
elseif ll_count > 0 then
	st_1.text = "Fine operazione: " + string(ll_count) + " fatture calcolate"
end if

enabled = true

setpointer(arrow!)
end event

type st_1 from statictext within w_calcola_fat_ven
integer x = 46
integer y = 1780
integer width = 2990
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_1 from commandbutton within w_calcola_fat_ven
integer x = 2011
integer y = 20
integer width = 411
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "CERCA"
end type

event clicked;string   ls_messaggio//, ls_flag_contabilita, ls_cod_deposito

//datetime ldt_da, ldt_a

long     ll_i, ll_anno, ll_num, ll_count = 0

uo_calcola_documento_euro luo_calcolo


setpointer(hourglass!)
enabled = false

//dw_calcola_fat_ven_1.accepttext()
//ldt_da = dw_calcola_fat_ven_1.getitemdatetime(1,"data_da")
//ldt_a = dw_calcola_fat_ven_1.getitemdatetime(1,"data_a")
//ls_flag_contabilita = dw_calcola_fat_ven_1.getitemstring(1,"flag_contabilita")
//ls_cod_deposito = dw_calcola_fat_ven_1.getitemstring(1,"cod_deposito")
//
//if ls_flag_contabilita = "T" then
//	ls_flag_contabilita = "%"
//end if
//
//if isnull(ls_cod_deposito) or len(ls_cod_deposito) < 1  then
//	ls_cod_deposito = "%"
//end if
//
//dw_calcola_fat_ven_2.retrieve(s_cs_xx.cod_azienda,ldt_da,ldt_a, ls_flag_contabilita, ls_cod_deposito)

if wf_set_sql_sintax() < 0 then
	//messaggio di errore già dato
	st_1.text = "Operazione terminata a causa di errori"
	enabled = true
	setpointer(arrow!)
	
	return
end if

enabled = true

cb_calcola.enabled=true
cb_provvigioni.enabled=true

setpointer(arrow!)
end event

type dw_calcola_fat_ven_2 from datawindow within w_calcola_fat_ven
integer x = 23
integer y = 820
integer width = 3003
integer height = 872
integer taborder = 10
string dataobject = "d_calcola_fat_ven_2"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_calcola_fat_ven_1 from datawindow within w_calcola_fat_ven
integer x = 23
integer y = 20
integer width = 1943
integer height = 660
integer taborder = 10
string dataobject = "d_calcola_fat_ven_1"
boolean border = false
boolean livescroll = true
end type

type gb_1 from groupbox within w_calcola_fat_ven
integer x = 23
integer y = 1720
integer width = 3022
integer height = 132
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
end type

