﻿$PBExportHeader$w_clienti_tipi_fat_ven.srw
forward
global type w_clienti_tipi_fat_ven from w_cs_xx_principale
end type
type dw_tipi_fat_ven_lingue_dw_cliente from uo_cs_xx_dw within w_clienti_tipi_fat_ven
end type
end forward

global type w_clienti_tipi_fat_ven from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2117
integer height = 764
string title = "Moduli"
dw_tipi_fat_ven_lingue_dw_cliente dw_tipi_fat_ven_lingue_dw_cliente
end type
global w_clienti_tipi_fat_ven w_clienti_tipi_fat_ven

type variables
string is_cod_cliente, is_cod_lingua
end variables

on w_clienti_tipi_fat_ven.create
int iCurrent
call super::create
this.dw_tipi_fat_ven_lingue_dw_cliente=create dw_tipi_fat_ven_lingue_dw_cliente
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_fat_ven_lingue_dw_cliente
end on

on w_clienti_tipi_fat_ven.destroy
call super::destroy
destroy(this.dw_tipi_fat_ven_lingue_dw_cliente)
end on

event pc_setwindow;call super::pc_setwindow;dw_tipi_fat_ven_lingue_dw_cliente.set_dw_key("cod_azienda")
dw_tipi_fat_ven_lingue_dw_cliente.set_dw_key("cod_tipo_fat_ven")
dw_tipi_fat_ven_lingue_dw_cliente.set_dw_key("cod_lingua")
dw_tipi_fat_ven_lingue_dw_cliente.set_dw_key("progressivo")

dw_tipi_fat_ven_lingue_dw_cliente.set_dw_options(sqlca, &
											 i_openparm, &
											 c_scrollparent, &
											 c_default)

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw( dw_tipi_fat_ven_lingue_dw_cliente, &
                 "cod_tipo_fat_ven", &
                 sqlca, &
                 "tab_tipi_fat_ven", &
                 "cod_tipo_fat_ven", &
                 "des_tipo_fat_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

type dw_tipi_fat_ven_lingue_dw_cliente from uo_cs_xx_dw within w_clienti_tipi_fat_ven
integer width = 2057
integer height = 640
integer taborder = 10
string dataobject = "d_tipi_fat_ven_lingue_dw_cliente"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_cliente, ls_cod_lingua
long l_errore

ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")
ls_cod_lingua = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_lingua")

parent.title = "MODULI"
	
l_errore = retrieve(s_cs_xx.cod_azienda, ls_cod_lingua, ls_cod_cliente)

if l_Errore < 0 then
   pcca.error = c_Fatal
else
	is_cod_cliente = ls_cod_cliente
	is_cod_lingua = ls_cod_lingua
end if

end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_tipo_fat_ven
long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_max

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemstring(ll_i, "cod_lingua")) or this.getitemstring(ll_i, "cod_lingua") = "" then
      this.setitem(ll_i, "cod_lingua", is_cod_lingua)
   end if
   if isnull(this.getitemstring(ll_i, "cod_cliente")) or this.getitemstring(ll_i, "cod_cliente") = "" then
      this.setitem(ll_i, "cod_cliente", is_cod_cliente)
   end if	
	
next

end event

event updatestart;call super::updatestart;long	   ll_i, ll_progressivo
string	ls_cod_tipo_fat_ven, ls_cod_lingua, ls_cod_cliente, ls_dataobject

for ll_i = 1 to rowcount()
	
	ls_cod_tipo_fat_ven = getitemstring( ll_i, "cod_tipo_fat_ven")
	ls_cod_lingua = getitemstring( ll_i, "cod_lingua")
	ls_cod_cliente = getitemstring( ll_i, "cod_cliente")
	ls_dataobject = getitemstring( ll_i, "dataobject")
	ll_progressivo = getitemnumber( ll_i, "progressivo")
	
	if isnull(ls_cod_tipo_fat_ven) or ls_cod_tipo_fat_ven = "" then
		g_mb.messagebox( "APICE", "Attenzione: scegliere la tipologia della fattura di vendita!", stopsign!)
		return 1
	end if	
	
	if isnull(ls_cod_lingua) or ls_cod_lingua = "" then
		g_mb.messagebox( "APICE", "Attenzione: scegliere la lingua!", stopsign!)
		return 1
	end if	
	
	if isnull(ls_cod_cliente) or ls_cod_cliente = "" then
		g_mb.messagebox( "APICE", "Attenzione: scegliere il cliente!", stopsign!)
		return 1
	end if		
	
	if isnull(ll_progressivo) or ll_progressivo = 0 then
		
		select max(progressivo)
		into	:ll_progressivo
		from	tab_tipi_fat_ven_lingue_dw
		where	cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_fat_ven = :ls_cod_tipo_fat_ven and
				cod_lingua = :ls_cod_lingua;
				
		if isnull(ll_progressivo) then ll_progressivo = 0
		ll_progressivo ++
		
		setitem( ll_i, "progressivo", ll_progressivo)
		
	end if
	
next
end event

event pcd_new;call super::pcd_new;setitem( getrow(), "progressivo", 0)
end event

