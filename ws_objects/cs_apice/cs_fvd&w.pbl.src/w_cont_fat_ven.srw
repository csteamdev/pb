﻿$PBExportHeader$w_cont_fat_ven.srw
$PBExportComments$Finestra Gestione Contropartite Fatture Vendita
forward
global type w_cont_fat_ven from w_cs_xx_principale
end type
type cb_1 from commandbutton within w_cont_fat_ven
end type
type dw_cont_fat_ven from uo_cs_xx_dw within w_cont_fat_ven
end type
end forward

global type w_cont_fat_ven from w_cs_xx_principale
integer x = 673
integer y = 265
integer width = 2939
integer height = 1228
string title = "Contropartite"
cb_1 cb_1
dw_cont_fat_ven dw_cont_fat_ven
end type
global w_cont_fat_ven w_cont_fat_ven

event pc_setwindow;call super::pc_setwindow;dw_cont_fat_ven.set_dw_key("cod_azienda")
dw_cont_fat_ven.set_dw_key("anno_registrazione")
dw_cont_fat_ven.set_dw_key("num_registrazione")

dw_cont_fat_ven.set_dw_options(sqlca, &
                               i_openparm, &
                               c_scrollparent, &
                               c_default)

end event

on w_cont_fat_ven.create
int iCurrent
call super::create
this.cb_1=create cb_1
this.dw_cont_fat_ven=create dw_cont_fat_ven
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_1
this.Control[iCurrent+2]=this.dw_cont_fat_ven
end on

on w_cont_fat_ven.destroy
call super::destroy
destroy(this.cb_1)
destroy(this.dw_cont_fat_ven)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_cont_fat_ven, &
                 "cod_conto", &
                 sqlca, &
                 "anag_piano_conti", &
                 "cod_conto", &
                 "des_conto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type cb_1 from commandbutton within w_cont_fat_ven
integer x = 2505
integer y = 1036
integer width = 375
integer height = 81
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Centri Costo"
end type

event clicked;if dw_cont_fat_ven.getrow() > 0 then
	window_open_parm(w_cont_fat_ven_cc, -1, dw_cont_fat_ven)
end if
end event

type dw_cont_fat_ven from uo_cs_xx_dw within w_cont_fat_ven
integer x = 23
integer y = 20
integer width = 2857
integer height = 1000
integer taborder = 10
string dataobject = "d_cont_fat_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if
next

end event

event pcd_retrieve;call super::pcd_retrieve;long l_errore, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	
l_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)

if l_Errore < 0 then
   pcca.error = c_Fatal
end if

end event

