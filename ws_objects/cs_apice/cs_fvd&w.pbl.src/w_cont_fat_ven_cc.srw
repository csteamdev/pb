﻿$PBExportHeader$w_cont_fat_ven_cc.srw
forward
global type w_cont_fat_ven_cc from w_cs_xx_principale
end type
type dw_cont_fat_ven_cc from uo_cs_xx_dw within w_cont_fat_ven_cc
end type
end forward

global type w_cont_fat_ven_cc from w_cs_xx_principale
integer width = 2569
integer height = 1124
string title = "Centri di Costo"
dw_cont_fat_ven_cc dw_cont_fat_ven_cc
end type
global w_cont_fat_ven_cc w_cont_fat_ven_cc

on w_cont_fat_ven_cc.create
int iCurrent
call super::create
this.dw_cont_fat_ven_cc=create dw_cont_fat_ven_cc
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_cont_fat_ven_cc
end on

on w_cont_fat_ven_cc.destroy
call super::destroy
destroy(this.dw_cont_fat_ven_cc)
end on

event pc_setwindow;call super::pc_setwindow;dw_cont_fat_ven_cc.set_dw_key("cod_azienda")
dw_cont_fat_ven_cc.set_dw_key("anno_registrazione")
dw_cont_fat_ven_cc.set_dw_key("num_registrazione")
dw_cont_fat_ven_cc.set_dw_key("cod_conto")

dw_cont_fat_ven_cc.set_dw_options(sqlca, &
											 i_openparm, &
											 c_scrollparent, &
											 c_default)

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_cont_fat_ven_cc, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type dw_cont_fat_ven_cc from uo_cs_xx_dw within w_cont_fat_ven_cc
integer y = 8
integer width = 2514
integer height = 1000
integer taborder = 10
string dataobject = "d_cont_fat_ven_cc"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;string ls_cod_conto
long l_errore, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
ls_cod_conto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_conto")
	
l_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione, ls_cod_conto)

if l_Errore < 0 then
   pcca.error = c_Fatal
end if

end event

event pcd_setkey;call super::pcd_setkey;string ls_cod_conto
long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_max

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
ls_cod_conto = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_conto")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if
   if isnull(this.getitemstring(ll_i, "cod_conto")) then
      this.setitem(ll_i, "cod_conto", ls_cod_conto)
   end if
//   if isnull(this.getitemnumber(ll_i, "progressivo")) or &
//      this.getitemnumber(ll_i, "progressivo") = 0 then
//		
//		ll_max = 0
//		
//		select max(progressivo)
//		into   :ll_max
//		from   cont_fat_ven_cc
//		where  cod_azienda = :s_cs_xx.cod_azienda and
//		       anno_registrazione = :ll_anno_registrazione and
//				 num_registrazione = :ll_num_registrazione and
//				 cod_conto = :ls_cod_conto;
//
//		if ll_max = 0 or isnull(ll_max) then
//			ll_max = 10
//		else
//			ll_max += 10
//		end if
//		
//      this.setitem(ll_i, "progressivo", ll_max)
//   end if
	
next

end event

