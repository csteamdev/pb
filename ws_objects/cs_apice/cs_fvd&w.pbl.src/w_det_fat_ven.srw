﻿$PBExportHeader$w_det_fat_ven.srw
$PBExportComments$Finestra Dettaglio Fatture di Vendita
forward
global type w_det_fat_ven from w_cs_xx_principale
end type
type cb_prodotti_note_ricerca from cb_prod_note_ricerca within w_det_fat_ven
end type
type cb_sconti from commandbutton within w_det_fat_ven
end type
type cb_c_industriale from commandbutton within w_det_fat_ven
end type
type cb_des_mov from commandbutton within w_det_fat_ven
end type
type uo_1 from uo_situazione_prodotto within w_det_fat_ven
end type
type cb_corrispondenze from commandbutton within w_det_fat_ven
end type
type cb_stock from cb_stock_ricerca within w_det_fat_ven
end type
type dw_det_fat_ven_cc from uo_cs_xx_dw within w_det_fat_ven
end type
type dw_folder from u_folder within w_det_fat_ven
end type
type dw_documenti from uo_dw_drag_doc_acq_ven within w_det_fat_ven
end type
type dw_det_fat_ven_lista from uo_cs_xx_dw within w_det_fat_ven
end type
type dw_det_fat_ven_det_1 from uo_cs_xx_dw within w_det_fat_ven
end type
end forward

global type w_det_fat_ven from w_cs_xx_principale
integer x = 5
integer y = 4
integer width = 4745
integer height = 1984
string title = "Fatture di Vendita"
boolean minbox = false
cb_prodotti_note_ricerca cb_prodotti_note_ricerca
cb_sconti cb_sconti
cb_c_industriale cb_c_industriale
cb_des_mov cb_des_mov
uo_1 uo_1
cb_corrispondenze cb_corrispondenze
cb_stock cb_stock
dw_det_fat_ven_cc dw_det_fat_ven_cc
dw_folder dw_folder
dw_documenti dw_documenti
dw_det_fat_ven_lista dw_det_fat_ven_lista
dw_det_fat_ven_det_1 dw_det_fat_ven_det_1
end type
global w_det_fat_ven w_det_fat_ven

type variables
boolean ib_movimenti=false, ib_provvigioni=false
string is_flag_tipo_lista_fatture
uo_condizioni_cliente iuo_condizioni_cliente
uo_gestione_conversioni iuo_gestione_conversioni


// stefano
private string is_flag_tipo_fat_ven

// stefanop 09/05/2010: progetto 140: ordinamento righe
private:
	long il_row_draging = -1
	boolean ib_row_draging = false

end variables

forward prototypes
public function integer wf_ricalcola ()
public function boolean wf_cmn ()
end prototypes

public function integer wf_ricalcola ();long   ll_i

string ls_parametro, ls_prodotto, ls_misura_mag, ls_misura_ven

dec{4} ld_quan_mag, ld_prezzo_mag, ld_quan_ven, ld_prezzo_ven, ld_fat_conversione, ld_prec_mag, ld_prec_ven

uo_calcola_documento_euro luo_calcolo


select stringa
into   :ls_parametro
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'TRD';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura parametro TRD da parametri_azienda: " + sqlca.sqlerrtext)
	return -1
elseif sqlca.sqlcode = 100 or isnull(ls_parametro) or (ls_parametro <> "M" and ls_parametro <> "V") then
	return 0
end if

select precisione_prezzo_mag,
		 precisione_prezzo_ven
into   :ld_prec_mag,
		 :ld_prec_ven
from   con_vendite
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in lettura precisione prezzi da parametri vendite: " + sqlca.sqlerrtext)
	return -1
end if

if isnull(ld_prec_mag) or ld_prec_mag = 0 then
	g_mb.messagebox("APICE","Impostare la precisione del prezzo di magazzino in PARAMETRI VENDITE")
	return -1
end if

if isnull(ld_prec_ven) or ld_prec_ven = 0 then
	g_mb.messagebox("APICE","Impostare la precisione del prezzo di vendita in PARAMETRI VENDITE")
	return -1
end if

for ll_i = 1 to dw_det_fat_ven_lista.rowcount()
	
	ls_prodotto = dw_det_fat_ven_lista.getitemstring(ll_i,"cod_prodotto")
	
	if isnull(ls_prodotto) then
		continue
	end if
	
	ls_misura_ven = dw_det_fat_ven_lista.getitemstring(ll_i,"cod_misura")
	
	select cod_misura_mag
	into   :ls_misura_mag
	from   anag_prodotti
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_prodotto = :ls_prodotto;
			 
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in lettura dati prodotto da anag_prodotti: " + sqlca.sqlerrtext)
		return -1
	end if
	
	if ls_misura_ven = ls_misura_mag then
		continue
	end if
	
	ld_quan_mag = dw_det_fat_ven_lista.getitemnumber(ll_i,"quan_fatturata")
	
	ld_prezzo_mag = dw_det_fat_ven_lista.getitemnumber(ll_i,"prezzo_vendita")
	
	ld_quan_ven = dw_det_fat_ven_lista.getitemnumber(ll_i,"quantita_um")
	
	ld_prezzo_ven = dw_det_fat_ven_lista.getitemnumber(ll_i,"prezzo_um")
	
	ld_fat_conversione = dw_det_fat_ven_lista.getitemnumber(ll_i,"fat_conversione_ven")
	
	choose case ls_parametro
			
		case "M"
			
			ld_quan_ven = round(ld_quan_mag * ld_fat_conversione , 4)
			
			ld_prezzo_ven = ld_prezzo_mag / ld_fat_conversione
			
			luo_calcolo = create uo_calcola_documento_euro
			
			luo_calcolo.uof_arrotonda(ld_prezzo_ven,ld_prec_ven,"round")
			
			destroy luo_calcolo
			
		case "V"
			
			ld_quan_mag = round(ld_quan_ven / ld_fat_conversione , 4)
			
			ld_prezzo_mag = ld_prezzo_ven * ld_fat_conversione
			
			luo_calcolo = create uo_calcola_documento_euro
			
			luo_calcolo.uof_arrotonda(ld_prezzo_mag,ld_prec_mag,"round")
			
			destroy luo_calcolo
			
	end choose
	
	dw_det_fat_ven_lista.setitem(ll_i,"quan_fatturata",ld_quan_mag)
	
	dw_det_fat_ven_lista.setitem(ll_i,"prezzo_vendita",ld_prezzo_mag)
	
	dw_det_fat_ven_lista.setitem(ll_i,"quantita_um",ld_quan_ven)
	
	dw_det_fat_ven_lista.setitem(ll_i,"prezzo_um",ld_prezzo_ven)
	
next

return 0
end function

public function boolean wf_cmn ();string ls_flag_negativo

select flag
into   :ls_flag_negativo
from	 parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'CMN' and
		 flag_parametro = 'F';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura parametro CMN da parametri_azienda (L'errore NON è bloccante!): " + sqlca.sqlerrtext,stopsign!)
	return false
elseif  ls_flag_negativo = 'S' then
	return true
else
	return false
end if
end function

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_det_fat_ven_det_1, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_det_fat_ven_lista, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_det_fat_ven_det_1, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_fat_ven_det_1, &
                 "cod_misura", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_fat_ven_det_1, &
                 "cod_tipo_movimento", &
                 sqlca, &
                 "tab_tipi_movimenti", &
                 "cod_tipo_movimento", &
                 "des_tipo_movimento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_det_fat_ven_det_1, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(dw_det_fat_ven_det_1, &
					  "cod_iva", &
					  sqlca, &
					  "tab_ive", &
					  "cod_iva", &
					  "des_iva", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
		  
f_po_loaddddw_dw(dw_det_fat_ven_cc, &
                 "cod_centro_costo", &
                 sqlca, &
                 "tab_centri_costo", &
                 "cod_centro_costo", &
                 "des_centro_costo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event pc_setwindow;call super::pc_setwindow;long ll_anno_registrazione, ll_num_registrazione
string ls_cod_valuta

select flag_tipo_lista_fatture
  into :is_flag_tipo_lista_fatture
  from con_vendite
 where cod_azienda = :s_cs_xx.cod_azienda;
 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Apice", "Errore in lettura dati da tabella con_vendite " + sqlca.sqlerrtext)
	return
end if	
  
if is_flag_tipo_lista_fatture = 'S' then
	dw_det_fat_ven_lista.DataObject = 'd_det_fat_ven_lista'
else
	dw_det_fat_ven_lista.DataObject = 'd_det_fat_ven_lista_sin'		
end if	

dw_det_fat_ven_lista.set_dw_key("cod_azienda")
dw_det_fat_ven_lista.set_dw_key("anno_registrazione")
dw_det_fat_ven_lista.set_dw_key("num_registrazione")

dw_det_fat_ven_lista.set_dw_options(sqlca, &
                                    i_openparm, &
                                    c_scrollparent, &
                                    c_default + &
                                    c_nohighlightselected + c_ViewModeBorderUnchanged)
dw_det_fat_ven_det_1.set_dw_options(sqlca, &
                                    dw_det_fat_ven_lista, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
												
dw_det_fat_ven_cc.set_dw_key("cod_azienda")
dw_det_fat_ven_cc.set_dw_key("anno_registrazione")
dw_det_fat_ven_cc.set_dw_key("num_registrazione")
dw_det_fat_ven_cc.set_dw_key("prog_riga_fat_ven")

dw_det_fat_ven_cc.set_dw_options(sqlca, &
											pcca.null_object, &
											c_scrollparent, &
											c_default + &
											c_nohighlightselected + c_ViewModeBorderUnchanged)
											
windowobject lw_oggetti[], lw_vuoto[]
lw_oggetti[1] = dw_det_fat_ven_lista
lw_oggetti[2] = uo_1
dw_folder.fu_assigntab(1, "Lista", lw_oggetti[])

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_det_fat_ven_cc
lw_oggetti[2] = uo_1
dw_folder.fu_assigntab(3, "Centri di Costo", lw_oggetti[])

lw_oggetti[] = lw_vuoto[]
lw_oggetti[1] = dw_det_fat_ven_det_1
lw_oggetti[2] = cb_prodotti_note_ricerca
lw_oggetti[3] = cb_stock
lw_oggetti[4] = dw_documenti
lw_oggetti[5] = uo_1
dw_folder.fu_assigntab(2, "Dettaglio", lw_oggetti[])

dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

dw_documenti.uof_set_management("FATVEN", dw_det_fat_ven_lista)
dw_documenti.settransobject(sqlca)
dw_documenti.uof_enabled_delete_blob()
dw_documenti.object.p_collegato.FileName = s_cs_xx.volume + s_cs_xx.risorse + + "menu\indietro.png"


iuo_dw_main=dw_det_fat_ven_lista
cb_c_industriale.enabled = false
cb_corrispondenze.enabled = false
cb_des_mov.enabled=false

//pb_prod_view.picturename = s_cs_xx.volume + s_cs_xx.risorse + "listwiev_3.bmp"


dw_det_fat_ven_lista.Object.cf_unita_misura.Background.Color = RGB(192, 192, 192)
if is_flag_tipo_lista_fatture = 'S' then
	dw_det_fat_ven_lista.Object.cf_qta_mq.Background.Color = RGB(192, 192, 192)
	dw_det_fat_ven_lista.Object.cf_prezzo_mq.Background.Color = RGB(192, 192, 192)
	dw_det_fat_ven_lista.Object.cf_tot_netto.Background.Color = RGB(192, 192, 192)
	
	
	ll_anno_registrazione = dw_det_fat_ven_lista.i_parentdw.getitemnumber(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = dw_det_fat_ven_lista.i_parentdw.getitemnumber(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "num_registrazione")	
	ls_cod_valuta = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
	
	if not isnull(ls_cod_valuta) and ls_cod_valuta = "LIT" then
		dw_det_fat_ven_lista.Object.cf_tot_netto.format = "###,###,###,###"	
	elseif not isnull(ls_cod_valuta) then
		dw_det_fat_ven_lista.Object.cf_tot_netto.format = "###,###,###,###.00"					
	end if	

end if	

dw_det_fat_ven_lista.dragicon = s_cs_xx.volume + s_cs_xx.risorse + "11.5\arrow_left_ico.ico"

dw_det_fat_ven_lista.is_cod_parametro_blocco_prodotto = 'PFV'
dw_det_fat_ven_lista.is_cod_parametro_blocco_prodotto = 'PFV'


try
	dw_det_fat_ven_det_1.object.p_mov.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
	dw_det_fat_ven_det_1.object.p_commessa.FileName = s_cs_xx.volume + s_cs_xx.risorse + "11.5\find.png"
catch (runtimeerror err)
end try



end event

on w_det_fat_ven.create
int iCurrent
call super::create
this.cb_prodotti_note_ricerca=create cb_prodotti_note_ricerca
this.cb_sconti=create cb_sconti
this.cb_c_industriale=create cb_c_industriale
this.cb_des_mov=create cb_des_mov
this.uo_1=create uo_1
this.cb_corrispondenze=create cb_corrispondenze
this.cb_stock=create cb_stock
this.dw_det_fat_ven_cc=create dw_det_fat_ven_cc
this.dw_folder=create dw_folder
this.dw_documenti=create dw_documenti
this.dw_det_fat_ven_lista=create dw_det_fat_ven_lista
this.dw_det_fat_ven_det_1=create dw_det_fat_ven_det_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_prodotti_note_ricerca
this.Control[iCurrent+2]=this.cb_sconti
this.Control[iCurrent+3]=this.cb_c_industriale
this.Control[iCurrent+4]=this.cb_des_mov
this.Control[iCurrent+5]=this.uo_1
this.Control[iCurrent+6]=this.cb_corrispondenze
this.Control[iCurrent+7]=this.cb_stock
this.Control[iCurrent+8]=this.dw_det_fat_ven_cc
this.Control[iCurrent+9]=this.dw_folder
this.Control[iCurrent+10]=this.dw_documenti
this.Control[iCurrent+11]=this.dw_det_fat_ven_lista
this.Control[iCurrent+12]=this.dw_det_fat_ven_det_1
end on

on w_det_fat_ven.destroy
call super::destroy
destroy(this.cb_prodotti_note_ricerca)
destroy(this.cb_sconti)
destroy(this.cb_c_industriale)
destroy(this.cb_des_mov)
destroy(this.uo_1)
destroy(this.cb_corrispondenze)
destroy(this.cb_stock)
destroy(this.dw_det_fat_ven_cc)
destroy(this.dw_folder)
destroy(this.dw_documenti)
destroy(this.dw_det_fat_ven_lista)
destroy(this.dw_det_fat_ven_det_1)
end on

event pc_delete;call super::pc_delete;string ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven, ls_messaggio

if f_verifica_fat_ven(dw_det_fat_ven_lista.i_parentdw.getitemnumber(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "anno_registrazione"), &
							 dw_det_fat_ven_lista.i_parentdw.getitemnumber(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "num_registrazione"), &
							 ls_messaggio) = -1 then
	g_mb.messagebox("APICE", ls_messaggio, stopsign!)
  	dw_det_fat_ven_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
  	return
end if	

cb_c_industriale.enabled = false
cb_corrispondenze.enabled = false
cb_des_mov.enabled = false
end event

event pc_modify;call super::pc_modify;string ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven, ls_messaggio

if f_verifica_fat_ven(dw_det_fat_ven_lista.i_parentdw.getitemnumber(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "anno_registrazione"), &
							 dw_det_fat_ven_lista.i_parentdw.getitemnumber(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "num_registrazione"), &
							 ls_messaggio) = -1 then
	g_mb.messagebox("APICE", ls_messaggio, stopsign!)
  	dw_det_fat_ven_lista.set_dw_view(c_ignorechanges)
   pcca.error = c_fatal
  	return
end if	

ls_cod_tipo_fat_ven = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_fat_ven")

select tab_tipi_fat_ven.flag_tipo_fat_ven  
into   :ls_flag_tipo_fat_ven  
from   tab_tipi_fat_ven  
where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
if sqlca.sqlcode = 0 then

	if ((ls_flag_tipo_fat_ven = "I") or (ls_flag_tipo_fat_ven = "N") or (ls_flag_tipo_fat_ven = "D")) and dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "flag_contabilita") = "S" then
		g_mb.messagebox("Attenzione", "Fattura non modificabile! E' già stata contabilizzata.", exclamation!, ok!)
		dw_det_fat_ven_lista.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if
end if

//if dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco") = "S" then
//	messagebox("Attenzione", "Fattura non cancellabile! E' Bloccata.", &
//				  exclamation!, ok!)
//	dw_det_fat_ven_lista.set_dw_view(c_ignorechanges)
//	pcca.error = c_fatal
//	return
//end if
//
//if dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "flag_contabilita") = "S" then
//   messagebox("Attenzione", "Fattura non modificabile! E' già stata contabilizzata.", exclamation!, ok!)
//   dw_det_fat_ven_lista.set_dw_view(c_ignorechanges)
//   pcca.error = c_fatal
//   return
//end if
//
//ls_cod_tipo_fat_ven = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "flag_blocco")
//
//select tab_tipi_fat_ven.flag_tipo_fat_ven  
//into   :ls_flag_tipo_fat_ven  
//from   tab_tipi_fat_ven  
//where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
//		 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
//
//if sqlca.sqlcode <> 0 then
//  	dw_det_fat_ven_lista.set_dw_view(c_ignorechanges)
//  	pcca.error = c_fatal
//	return
//end if
//
//if ((ls_flag_tipo_fat_ven = "I") or (ls_flag_tipo_fat_ven = "N")) and &
//	(dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "flag_movimenti") = "S") then
//   messagebox("Attenzione", "Fattura non modificabile! E' già stata confermata.", &
//  	           exclamation!, ok!)
//  	dw_det_fat_ven_lista.set_dw_view(c_ignorechanges)
//  	pcca.error = c_fatal
//  	return
//end if
//
//if dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "flag_calcolo") = "S" then
//   messagebox("Attenzione", "Fattura non modificabile! E' già stata calcolata, azzerare il calcolo.", &
//              exclamation!, ok!)
//   dw_det_fat_ven_lista.set_dw_view(c_ignorechanges)
//   pcca.error = c_fatal
//   return
//end if
//
end event

event pc_new;call super::pc_new;string ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven, ls_messaggio

if f_verifica_fat_ven(dw_det_fat_ven_lista.i_parentdw.getitemnumber(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "anno_registrazione"), &
							 dw_det_fat_ven_lista.i_parentdw.getitemnumber(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "num_registrazione"), &
							 ls_messaggio) = -1 then
	g_mb.messagebox("APICE", ls_messaggio, stopsign!)
	dw_det_fat_ven_lista.set_dw_view(c_ignorechanges)
	dw_det_fat_ven_det_1.set_dw_view(c_ignorechanges)
	pcca.error = c_fatal
	return
end if	



end event

event open;call super::open;iuo_condizioni_cliente = create uo_condizioni_cliente
iuo_gestione_conversioni = create uo_gestione_conversioni
end event

event close;call super::close;destroy iuo_condizioni_cliente
destroy iuo_gestione_conversioni
end event

type cb_prodotti_note_ricerca from cb_prod_note_ricerca within w_det_fat_ven
integer x = 1449
integer y = 772
integer height = 80
integer taborder = 40
end type

event getfocus;call super::getfocus;s_cs_xx.parametri.parametro_s_1 = dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_prodotto")
dw_det_fat_ven_det_1.change_dw_current()
end event

type cb_sconti from commandbutton within w_det_fat_ven
integer x = 4306
integer y = 1772
integer width = 366
integer height = 80
integer taborder = 120
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Sconti"
end type

event clicked;s_cs_xx.parametri.parametro_uo_dw_1 = dw_det_fat_ven_lista
window_open(w_sconti, 0)

end event

type cb_c_industriale from commandbutton within w_det_fat_ven
event clicked pbm_bnclicked
integer x = 3918
integer y = 1772
integer width = 366
integer height = 80
integer taborder = 110
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "C.Industriale"
end type

event clicked;window_open_parm(w_det_fat_ven_stat,-1,dw_det_fat_ven_lista)
end event

type cb_des_mov from commandbutton within w_det_fat_ven
event clicked pbm_bnclicked
integer x = 3529
integer y = 1772
integer width = 366
integer height = 80
integer taborder = 90
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Dest. Mov."
end type

event clicked;long ll_anno_reg_des_mov, ll_num_reg_des_mov
string ls_cod_prodotto

ll_anno_reg_des_mov = dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(), "anno_reg_des_mov")
ll_num_reg_des_mov = dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(), "num_reg_des_mov")
ls_cod_prodotto = dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(), "cod_prodotto")

s_cs_xx.parametri.parametro_d_1 = ll_anno_reg_des_mov
s_cs_xx.parametri.parametro_d_2 = ll_num_reg_des_mov
s_cs_xx.parametri.parametro_s_10 = ls_cod_prodotto
window_open(w_dest_mov_magazzino, 0)

end event

type uo_1 from uo_situazione_prodotto within w_det_fat_ven
integer x = 46
integer y = 1460
integer width = 4581
integer height = 256
integer taborder = 10
boolean border = false
end type

on uo_1.destroy
call uo_situazione_prodotto::destroy
end on

type cb_corrispondenze from commandbutton within w_det_fat_ven
event clicked pbm_bnclicked
integer x = 3141
integer y = 1772
integer width = 366
integer height = 80
integer taborder = 80
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Corrispond."
end type

event clicked;s_cs_xx.parametri.parametro_s_1 = "Fat_Ven"
window_open_parm(w_det_acq_ven_corr, -1, dw_det_fat_ven_det_1)

end event

type cb_stock from cb_stock_ricerca within w_det_fat_ven
event clicked pbm_bnclicked
integer x = 1623
integer y = 620
integer width = 73
integer height = 80
integer taborder = 20
end type

event clicked;call super::clicked;s_cs_xx.parametri.parametro_s_10 = dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_prodotto")
s_cs_xx.parametri.parametro_s_11 = dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_deposito")
s_cs_xx.parametri.parametro_s_12 = dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_ubicazione")
s_cs_xx.parametri.parametro_s_13 = dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_lotto")
setnull(s_cs_xx.parametri.parametro_data_1)
s_cs_xx.parametri.parametro_d_1 = 0

dw_det_fat_ven_det_1.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_prodotto"
s_cs_xx.parametri.parametro_s_2 = "cod_deposito"
s_cs_xx.parametri.parametro_s_3 = "cod_ubicazione"
s_cs_xx.parametri.parametro_s_4 = "cod_lotto"
s_cs_xx.parametri.parametro_s_5 = "data_stock"
s_cs_xx.parametri.parametro_s_6 = "progr_stock"
setnull(s_cs_xx.parametri.parametro_s_7)
setnull(s_cs_xx.parametri.parametro_s_8)

window_open(w_ricerca_stock, 0)
end event

type dw_det_fat_ven_cc from uo_cs_xx_dw within w_det_fat_ven
integer x = 50
integer y = 124
integer width = 3401
integer height = 792
integer taborder = 60
boolean dragauto = true
string dataobject = "d_det_fat_ven_cc"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven

if dw_det_fat_ven_lista.getrow() > 0 then

	ll_anno_registrazione = dw_det_fat_ven_lista.getitemnumber(dw_det_fat_ven_lista.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_det_fat_ven_lista.getitemnumber(dw_det_fat_ven_lista.getrow(), "num_registrazione")
	ll_prog_riga_fat_ven = dw_det_fat_ven_lista.getitemnumber(dw_det_fat_ven_lista.getrow(), "prog_riga_fat_ven")
	
	ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_fat_ven)
	
	if ll_errore < 0 then
		pcca.error = c_fatal
	end if
	
end if
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_fat_ven

ll_anno_registrazione = dw_det_fat_ven_lista.getitemnumber(dw_det_fat_ven_lista.getrow(), "anno_registrazione")
ll_num_registrazione = dw_det_fat_ven_lista.getitemnumber(dw_det_fat_ven_lista.getrow(), "num_registrazione")
ll_prog_riga_fat_ven = dw_det_fat_ven_lista.getitemnumber(dw_det_fat_ven_lista.getrow(), "prog_riga_fat_ven")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "prog_riga_fat_ven")) or &
      this.getitemnumber(ll_i, "prog_riga_fat_ven") = 0 then
      this.setitem(ll_i, "prog_riga_fat_ven", ll_prog_riga_fat_ven)
   end if
next

end event

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_flag_cc_manuali
	long ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_fat_ven
	dec{4} ld_imponibile_iva,ld_percento,ld_importo
	
	ll_anno_registrazione = dw_det_fat_ven_lista.getitemnumber(dw_det_fat_ven_lista.getrow(), "anno_registrazione")
	ll_num_registrazione = dw_det_fat_ven_lista.getitemnumber(dw_det_fat_ven_lista.getrow(), "num_registrazione")
	ll_prog_riga_fat_ven = dw_det_fat_ven_lista.getitemnumber(dw_det_fat_ven_lista.getrow(), "prog_riga_fat_ven")
	ld_imponibile_iva = dw_det_fat_ven_lista.getitemnumber(dw_det_fat_ven_lista.getrow(), "imponibile_iva")
	
	choose case i_colname
			
		case "percentuale"
			// se scrivo la percentuale, calcolo importo
			ld_percento = dec(i_coltext)
			ld_importo = (ld_imponibile_iva / 100 * ld_percento)
			setitem(getrow(),"importo", ld_importo)
			
		case "importo"
			// se scrivo l'importo, calcolo la percentuale
			ld_importo = dec(i_coltext)
			ld_percento = round((ld_importo * 100) / ld_imponibile_iva, 2)
			setitem(getrow(),"percentuale", ld_percento)
			
	end choose
	
	select flag_cc_manuali
	into   :ls_flag_cc_manuali
	from   det_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       anno_registrazione = :ll_anno_registrazione and
	       num_registrazione  = :ll_num_registrazione and
			 prog_riga_fat_ven = :ll_prog_riga_fat_ven;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE", "Errore ricerca dello stato del flag centri costo manuali~r~n"+sqlca.sqlerrtext)
		return 0
	end if
	
	if ls_flag_cc_manuali = "N" then
		update det_fat_ven
		set flag_cc_manuali = 'S'
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione  = :ll_num_registrazione and
				 prog_riga_fat_ven = :ll_prog_riga_fat_ven;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE", "Errore nella impostazione del flag centri costo manuali~r~n"+sqlca.sqlerrtext)
			return 0
		end if
		
		commit;
	end if
end if
end event

event updateend;call super::updateend;long ll_anno_registrazione, ll_num_registrazione,ll_prog_riga_fat_ven, ll_cont

ll_anno_registrazione = dw_det_fat_ven_lista.getitemnumber(dw_det_fat_ven_lista.getrow(), "anno_registrazione")
ll_num_registrazione = dw_det_fat_ven_lista.getitemnumber(dw_det_fat_ven_lista.getrow(), "num_registrazione")
ll_prog_riga_fat_ven = dw_det_fat_ven_lista.getitemnumber(dw_det_fat_ven_lista.getrow(), "prog_riga_fat_ven")

ll_cont = 0

select count(*)
into   :ll_cont
from   det_fat_ven_cc
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :ll_anno_registrazione and
		 num_registrazione  = :ll_num_registrazione and
		 prog_riga_fat_ven = :ll_prog_riga_fat_ven;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE", "Errore ricerca dello stato del flag centri costo manuali~r~n"+sqlca.sqlerrtext)
	return 0
end if

if ll_cont < 1 then

	update det_fat_ven
	set flag_cc_manuali = 'N'
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :ll_anno_registrazione and
			 num_registrazione  = :ll_num_registrazione and
			 prog_riga_fat_ven = :ll_prog_riga_fat_ven;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE", "Errore nella impostazione del flag centri costo manuali~r~n"+sqlca.sqlerrtext)
		return 0
	end if
	
	commit;
	
end if

end event

type dw_folder from u_folder within w_det_fat_ven
integer x = 23
integer y = 20
integer width = 4654
integer height = 1728
integer taborder = 60
end type

event po_tabclicked;call super::po_tabclicked;choose case i_SelectedTab	
	case 3
		dw_det_fat_ven_cc.change_dw_current()
		iuo_dw_main = dw_det_fat_ven_cc
		dw_det_fat_ven_cc.ib_proteggi_chiavi = false
		parent.postevent("pc_retrieve")
	case else
		iuo_dw_main = dw_det_fat_ven_lista
		dw_det_fat_ven_lista.ib_proteggi_chiavi = true
end choose		
end event

type dw_documenti from uo_dw_drag_doc_acq_ven within w_det_fat_ven
integer x = 3479
integer y = 120
integer width = 1161
integer height = 1372
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_det_fat_ven_note_blob"
boolean vscrollbar = true
end type

type dw_det_fat_ven_lista from uo_cs_xx_dw within w_det_fat_ven
event ue_postopen ( )
event post_rowfocuschaged ( )
event ue_calcola ( )
integer x = 46
integer y = 120
integer width = 4576
integer height = 1336
integer taborder = 50
string dragicon = "C:\cs_115\framework\risorse\11.5\arrow_left_ico.ico"
string dataobject = "d_det_fat_ven_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean hsplitscroll = true
boolean livescroll = true
end type

event ue_postopen;if this.getrow() > 0 then
	datawindow ld_datawindow
	
	uo_gestione_conversioni iuo_oggetto
	iuo_oggetto = create uo_gestione_conversioni
	ld_datawindow  = dw_det_fat_ven_det_1
	iuo_oggetto.uof_visualizza_um(ld_datawindow, "fat_ven")
	if ib_stato_nuovo or ib_stato_modifica then 	iuo_gestione_conversioni.uof_blocca_colonne(ld_datawindow,"fat_ven")
	destroy iuo_oggetto
end if
end event

event post_rowfocuschaged();datawindow ld_datawindow

uo_gestione_conversioni iuo_oggetto
iuo_oggetto = create uo_gestione_conversioni
ld_datawindow  = dw_det_fat_ven_det_1
iuo_oggetto.uof_visualizza_um(ld_datawindow, "fat_ven")
if (ib_stato_modifica or ib_stato_nuovo) and not ib_provvigioni then
	iuo_oggetto.uof_blocca_colonne(ld_datawindow, "fat_ven")
end if
destroy iuo_oggetto
end event

event ue_calcola();//	*** Michela 24/09/2007: ho spostato tutto qui dentro ed ho sistemato i triggerevent/postevent perchè altrimenti il focus rimaneva sul pulsante "calcola" della testata ( più precisamente nella dw_det_1 ) e quando si 
//								   faceva una seconda cancellazione dopo il salva essa veniva effettuata nella testata!!

long   ll_i, ll_currow

triggerevent("pcd_view")

if not isvalid(s_cs_xx.parametri.parametro_w_fat_ven) then return

for ll_i = 1 to upperbound(s_cs_xx.parametri.parametro_w_fat_ven.control)	
   if s_cs_xx.parametri.parametro_w_fat_ven.control[ll_i].classname() = "cb_calcola" then
      s_cs_xx.parametri.parametro_w_fat_ven.control[ll_i].triggerevent("clicked")
      exit
   end if	
next

//Donato 06/10/2008 
//per non far selezionare di nuovo la prima riga dopo la retrieve
ll_currow = getrow()
//---------------

change_dw_current()
triggerevent("pcd_retrieve")

//Donato 06/10/2008 
//per non far selezionare di nuovo la prima riga dopo la retrieve
if ll_currow > 0 then setrow(ll_currow)
//---------------
end event

event pcd_new;call super::pcd_new;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_ven, ls_cod_tipo_fat_ven, ls_flag_tipo_det_ven, &
          ls_modify, ls_null, ls_cod_agente_1, ls_cod_agente_2, ls_cod_cliente, ls_flag_tipo_fat_ven, &
          ls_cod_iva, ls_cod_tipo_movimento, ls_cod_deposito, ls_cod_ubicazione, ls_cod_fornitore
   long ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven, ll_null
   datetime ldt_data_esenzione_iva, ldt_data_registrazione, ldt_null
	double ld_quan_proposta

	dw_det_fat_ven_lista.object.cod_tipo_det_ven.protect = 0

   setnull(ls_null)

   ll_anno_registrazione = dw_det_fat_ven_lista.i_parentdw.getitemnumber(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "anno_registrazione")
   ll_num_registrazione = dw_det_fat_ven_lista.i_parentdw.getitemnumber(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "num_registrazione")
   ldt_data_registrazione = dw_det_fat_ven_lista.i_parentdw.getitemdatetime(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
   ls_cod_cliente = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
   ls_cod_fornitore = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")

   select max(det_fat_ven.prog_riga_fat_ven)
   into   :ll_prog_riga_fat_ven
   from   det_fat_ven
   where  det_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and
          det_fat_ven.anno_registrazione = :ll_anno_registrazione and
          det_fat_ven.num_registrazione = :ll_num_registrazione;

   if isnull(ll_prog_riga_fat_ven) then
      dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(), "prog_riga_fat_ven", 10)
	   dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(), "ordinamento", 10)
   else
      dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(), "prog_riga_fat_ven", ll_prog_riga_fat_ven + 10)
	   dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(), "ordinamento", ll_prog_riga_fat_ven + 10)
   end if
	
	

   ls_cod_tipo_fat_ven = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_fat_ven")

   select cod_tipo_det_ven,
			 flag_tipo_fat_ven
   into   :ls_cod_tipo_det_ven,
	       :ls_flag_tipo_fat_ven
   from   tab_tipi_fat_ven
   where  cod_azienda = :s_cs_xx.cod_azienda and 
          cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
   
   if sqlca.sqlcode = 0 then
      dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(), "cod_tipo_det_ven", ls_cod_tipo_det_ven)
      if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
         select tab_tipi_det_ven.cod_iva  
         into   :ls_cod_iva  
         from   tab_tipi_det_ven  
         where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
                tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
         if sqlca.sqlcode = 0 then
            this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
         end if
      end if

   else
      ls_cod_tipo_det_ven = ls_null
   end if

//-------------------------------------- Modifica Nicola ---------------------------------------------------------------

	select quan_default
	  into :ld_quan_proposta
	  from tab_tipi_det_ven
	 where cod_azienda = :s_cs_xx.cod_azienda 
	   and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati da tabella tab_tipi_det_ven " + sqlca.sqlerrtext)
		return
	end if
	
	if isnull(ld_quan_proposta) then ld_quan_proposta = 0
	dw_det_fat_ven_lista.setitem(dw_det_fat_ven_lista.getrow(), "quan_fatturata", ld_quan_proposta)
//--------------------------------------- Fine Modifica ----------------------------------------------------------------

   //dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(), "quan_fatturata", 1)
   
	if ls_flag_tipo_fat_ven <> "F" then
		if not isnull(ls_cod_cliente) then
			select cod_iva,
					 data_esenzione_iva
			into   :ls_cod_iva,
					 :ldt_data_esenzione_iva
			from   anag_clienti
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_cliente = :ls_cod_cliente;
		end if
	else
		if not isnull(ls_cod_fornitore) then
			select cod_iva,
					 data_esenzione_iva
			into   :ls_cod_iva,
					 :ldt_data_esenzione_iva
			from   anag_fornitori
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_fornitore = :ls_cod_fornitore;
		end if
	end if		
   if sqlca.sqlcode = 0 then
      if ls_cod_iva <> "" and &
         (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
         this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
      end if
   end if
   
   ls_modify = "cod_tipo_det_ven.protect='0'~t"
   dw_det_fat_ven_det_1.modify(ls_modify)
   ls_modify = "cod_tipo_det_ven.background.color='16777215'~t"
   dw_det_fat_ven_det_1.modify(ls_modify)

   if not isnull(ls_cod_tipo_det_ven) then
      dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(), "cod_tipo_det_ven", ls_cod_tipo_det_ven)
      ld_datawindow = dw_det_fat_ven_det_1
	setnull(lp_prod_view)
	setnull(lc_prodotti_ricerca)
      ls_cod_agente_1 = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
      ls_cod_agente_2 = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
      f_tipo_dettaglio_ven(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_ven, ls_cod_agente_1, ls_cod_agente_2)
      ld_datawindow       = dw_det_fat_ven_lista
      f_tipo_dettaglio_ven_lista(ld_datawindow, ls_cod_tipo_det_ven)

	   ls_cod_deposito = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_deposito")
	   ls_cod_ubicazione = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_ubicazione")
	
		select tab_tipi_det_ven.flag_tipo_det_ven,
				 tab_tipi_det_ven.cod_tipo_movimento
		into   :ls_flag_tipo_det_ven,
				 :ls_cod_tipo_movimento
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
						  exclamation!, ok!)
		end if
	
		if ls_flag_tipo_det_ven = "M" then
			cb_stock.enabled = true
			this.setitem(this.getrow(), "cod_deposito", ls_cod_deposito)
			this.setitem(this.getrow(), "cod_ubicazione", ls_cod_ubicazione)
			this.setitem(this.getrow(), "cod_tipo_movimento", ls_cod_tipo_movimento)
		else
			if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_tipo_movimento")) then
				dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_tipo_movimento",ls_null)
			end if
			if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_deposito")) then
				dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_deposito",ls_null)
			end if
			if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_ubicazione")) then
				dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_ubicazione",ls_null)
			end if
			if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_lotto")) then
				dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_lotto",ls_null)
			end if
			if not isnull(dw_det_fat_ven_det_1.getitemdatetime(dw_det_fat_ven_det_1.getrow(),"data_stock")) then
				dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"data_stock",ldt_null)
			end if
			if not isnull(dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"progr_stock")) then
				dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"progr_stock",ll_null)
			end if
	
			ls_modify = "cod_tipo_movimento.protect='1'~t"
			ls_modify = ls_modify + "cod_tipo_movimento.background.color='12632256'~t"
			ls_modify = ls_modify + "cod_deposito.protect='1'~t"
			ls_modify = ls_modify + "cod_deposito.background.color='12632256'~t"
			ls_modify = ls_modify + "cod_ubicazione.protect='1'~t"
			ls_modify = ls_modify + "cod_ubicazione.background.color='12632256'~t"
			ls_modify = ls_modify + "cod_lotto.protect='1'~t"
			ls_modify = ls_modify + "cod_lotto.background.color='12632256'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
		end if

	else
      ls_modify = "cod_prodotto.protect='1'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "cod_prodotto.background.color='12632256'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "des_prodotto.protect='1'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "des_prodotto.background.color='12632256'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "cod_misura.protect='1'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "cod_misura.background.color='12632256'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "fat_conversione_ven.protect='1'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "fat_conversione_ven.background.color='12632256'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "quan_fatturata.protect='1'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "quan_fatturata.background.color='12632256'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "prezzo_vendita.protect='1'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "prezzo_vendita.background.color='12632256'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "sconto_1.protect='1'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "sconto_1.background.color='12632256'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "sconto_2.protect='1'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "sconto_2.background.color='12632256'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "cod_iva.protect='1'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "cod_iva.background.color='12632256'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "provvigione_1.protect='1'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "provvigione_1.background.color='12632256'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "provvigione_2.protect='1'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "provvigione_2.background.color='12632256'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "cod_deposito.protect='1'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "cod_deposito.background.color='12632256'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "cod_ubicazione.protect='1'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "cod_ubicazione.background.color='12632256'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "cod_lotto.protect='1'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "cod_lotto.background.color='12632256'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "cod_tipo_movimento.protect='1'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "cod_tipo_movimento.background.color='12632256'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
   end if

   cb_sconti.enabled = true
	cb_c_industriale.enabled = false
	cb_corrispondenze.enabled = false
	cb_des_mov.enabled = false
	
	dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_lista.getrow(), "flag_st_note_det", "I")

	dw_folder.fu_disabletab(3)
	
end if
end event

event pcd_view;call super::pcd_view;if i_extendmode then
	string ls_flag_tipo_fat_ven, ls_cod_tipo_fat_ven
   
   cb_prodotti_note_ricerca.enabled = false
   dw_det_fat_ven_det_1.object.b_ricerca_prodotto.enabled = false
//	pb_prod_view.enabled = false
   cb_sconti.enabled = false
   cb_stock.enabled = false

	ls_cod_tipo_fat_ven =i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_fat_ven")
	select flag_tipo_fat_ven
	into   :ls_flag_tipo_fat_ven  
	from   tab_tipi_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven ;

	// stefano 09/04/2010: copio varibile in variabile di istanza
	// Usata poi nell'evento rowsfocuschanged
	is_flag_tipo_fat_ven = ls_flag_tipo_fat_ven
	// ----
	
	if this.getrow() > 0 then
		if this.getitemnumber(this.getrow(), "anno_registrazione") > 0 then
			cb_c_industriale.enabled=true
			cb_corrispondenze.enabled = true
			if ls_flag_tipo_fat_ven = "I" then cb_des_mov.enabled = true
		end if
	else
		cb_c_industriale.enabled=false
		cb_corrispondenze.enabled = false
		cb_des_mov.enabled = false
	end if
	iuo_gestione_conversioni.uof_visualizza_um(dw_det_fat_ven_det_1, "fat_ven")

	dw_folder.fu_enabletab(3)
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if ib_row_draging then return 1

if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_fornitore, ls_cod_cliente, ls_cod_iva, ls_cod_tipo_det_ven, ls_modify, ls_cod_deposito,&
          ls_cod_agente_1, ls_cod_agente_2, ls_cod_prodotto, ls_cod_misura_mag, &
			 ls_flag_tipo_fat_ven, ls_cod_tipo_fat_ven,ls_flag_tipo_det_ven, ls_cod_misura_ven
   datetime ldt_data_registrazione, ldt_data_esenzione_iva

	ls_cod_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")
	ls_cod_fornitore = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_fornitore")
   ldt_data_registrazione = i_parentdw.getitemdatetime(i_parentdw.i_selectedrows[1], "data_registrazione")

	if ib_stato_modifica or ib_stato_nuovo then
		ls_cod_tipo_det_ven = dw_det_fat_ven_det_1.getitemstring(this.getrow(), "cod_tipo_det_ven")
		select tab_tipi_det_ven.flag_tipo_det_ven 
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
		if sqlca.sqlcode = 0 then
	
			if ls_flag_tipo_det_ven = "M" then
				cb_stock.enabled = true
				ls_modify = "cod_tipo_movimento.protect='0'~t"
				ls_modify = ls_modify + "cod_tipo_movimento.background.color='16777215'~t"
				ls_modify = ls_modify + "cod_deposito.protect='0'~t"
				ls_modify = ls_modify + "cod_deposito.background.color='16777215'~t"
				ls_modify = ls_modify + "cod_ubicazione.protect='0'~t"
				ls_modify = ls_modify + "cod_ubicazione.background.color='16777215'~t"
				ls_modify = ls_modify + "cod_lotto.protect='0'~t"
				ls_modify = ls_modify + "cod_lotto.background.color='16777215'~t"
				dw_det_fat_ven_det_1.modify(ls_modify)
			else
				cb_stock.enabled = false
				ls_modify = "cod_tipo_movimento.protect='1'~t"
				ls_modify = ls_modify + "cod_tipo_movimento.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_deposito.protect='1'~t"
				ls_modify = ls_modify + "cod_deposito.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_ubicazione.protect='1'~t"
				ls_modify = ls_modify + "cod_ubicazione.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_lotto.protect='1'~t"
				ls_modify = ls_modify + "cod_lotto.background.color='12632256'~t"
				dw_det_fat_ven_det_1.modify(ls_modify)
			end if
		end if
      ls_modify = "cod_tipo_det_ven.protect='0~tif(isrownew(),0,1)'~t"
      ls_modify = ls_modify + "cod_tipo_det_ven.background.color='16777215~tif(isrownew(),16777215,12632256)'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
		
		// stefano 09/04/2010
		cb_des_mov.enabled =false
		
	end if
	
	// stefano 09/04/2010: abilito il pulsante dest.mov controllando il flag_tipo_det_ven
	if not ib_stato_modifica and not ib_stato_nuovo then		
		if is_flag_tipo_fat_ven = "I" then // immediata lo mostra sempre
			cb_des_mov.enabled = true
		else
			ls_cod_tipo_det_ven = this.getitemstring(this.getrow(), "cod_tipo_det_ven")
			
			if isnull(ls_cod_tipo_det_ven) or ls_cod_tipo_det_ven = "" then
				cb_des_mov.enabled = false
			else
				select flag_tipo_det_ven
				into :ls_flag_tipo_det_ven
				from tab_tipi_det_ven
				where
					cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_det_ven = :ls_cod_tipo_det_ven;
					
				if sqlca.sqlcode = 0 and ls_flag_tipo_det_ven = "M" then
					cb_des_mov.enabled = true
				else
					cb_des_mov.enabled = false
				end if
			end if
		end if
	end if
	// ----
	
	if this.getrow() > 0 then
		
		dw_documenti.uof_retrieve_blob(getrow())
		
		ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")		
		ls_cod_misura_ven = this.getitemstring(this.getrow(),"cod_misura")

		select 	cod_misura_mag
		into   	:ls_cod_misura_mag
		from   	anag_prodotti
		where  	cod_azienda = :s_cs_xx.cod_azienda and 
				 	cod_prodotto = :ls_cod_prodotto;
		if sqlca.sqlcode = 0 then
			uo_1.uof_aggiorna(ls_cod_prodotto)
			uo_1.uof_ultimo_prezzo( ls_cod_cliente, ls_cod_prodotto)
		else
			uo_1.hide()
		end if

		f_po_loaddddw_dw(dw_det_fat_ven_det_1, &
							  "cod_iva", &
							  sqlca, &
							  "tab_ive", &
							  "cod_iva", &
							  "des_iva", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco = 'N') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
	
		f_PO_LoadDDDW_DW(dw_det_fat_ven_det_1,"cod_versione",sqlca,&
							  "distinta_padri","cod_versione","des_versione",&
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + this.getitemstring(this.getrow(),"cod_prodotto") + "'")
							  
		if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
			len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
			len(trim(ls_cod_misura_mag)) <> 0 then
			dw_det_fat_ven_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione_ven"))) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
		else
			dw_det_fat_ven_det_1.modify("st_fattore_conv.text=''")		
		end if
		
		ld_datawindow  = dw_det_fat_ven_det_1
		if (ib_stato_modifica or ib_stato_nuovo) and not ib_provvigioni then
			ls_cod_tipo_det_ven = dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(), "cod_tipo_det_ven")
			ld_datawindow = dw_det_fat_ven_det_1
			setnull(lp_prod_view)
			setnull(lc_prodotti_ricerca)
			f_tipo_dettaglio_ven_det(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_ven)
			ld_datawindow       = dw_det_fat_ven_lista
			f_tipo_dettaglio_ven_lista(ld_datawindow, ls_cod_tipo_det_ven)
		end if
		
		postevent("post_rowfocuschanged")
		
	end if

   if ib_stato_nuovo then
      ls_modify = "cod_tipo_det_ven.protect='0~tif(isrownew(),0,1)'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
      ls_modify = "cod_tipo_det_ven.background.color='16777215~tif(isrownew(),16777215,12632256)'~t"
      dw_det_fat_ven_det_1.modify(ls_modify)
   end if
	
end if
end event

event updatestart;call super::updatestart;if i_extendmode then
	long 	ll_anno_registrazione, ll_num_registrazione,ll_i,ll_prog_riga_fat_ven, ll_i2, ll_i3, ll_i4, &
		  	ll_controllo, ll_progr_stock[], ll_progr_stock_old, ll_anno_reg_des_mov_old, &
			ll_anno_reg_des_mov, ll_num_reg_des_mov_old, ll_num_reg_des_mov, &
			ll_anno_ordine, ll_num_ordine, ll_riga_ordine, ll_righe, ll_evase, ll_parziali
   dec{4} ld_sconto_testata, ld_quan_fatturata_old, ld_quan_fatturata, ld_quan_disponibile, ld_quantita, ld_quan_ordine
   string 	ls_cod_pagamento, ls_cod_valuta, ls_tabella, ls_quan_documento, ls_nome_prog, &
			ls_cod_tipo_fat_ven, ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, &
			ls_cod_prodotto_old, ls_cod_prodotto, ls_flag_tipo_fat_ven, ls_tot_val_documento, &
			ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_tipo_movimento_old, &
			ls_cod_deposito_old, ls_cod_ubicazione_old, ls_cod_lotto_old, ls_cod_cliente[], &
			ls_cod_fornitore[], ls_cod_tipo_movimento, ls_aspetto_beni, ls_des_imballo, ls_CMN, &
			ls_flag_evasione
			 
	datetime ldt_data_stock[], ldt_data_stock_old

	ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	ld_sconto_testata = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "sconto")
	ls_cod_pagamento = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_pagamento")
	ls_cod_valuta = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_valuta")
	ls_cod_tipo_fat_ven=i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_tipo_fat_ven")
	ls_aspetto_beni = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "aspetto_beni")
	ll_prog_riga_fat_ven = getitemnumber(getrow(),"prog_riga_fat_ven")
	ls_tabella = "tes_fat_ven"
	ls_quan_documento = "quan_fatturata"
	
	
	//se il flag_avviso_contrassegno è S allora avvisa l'utente di rigenerare il contrassegno
	uo_avviso_spedizioni luo_avviso_sped
	string ls_msg_avviso
	long ll_ret
	
	luo_avviso_sped = create uo_avviso_spedizioni
	
	luo_avviso_sped.il_anno_documento = ll_anno_registrazione
	luo_avviso_sped.il_num_documento = ll_num_registrazione
	luo_avviso_sped.is_tipo_gestione = "fat_ven"
	
	ll_ret = luo_avviso_sped.uof_check_flag_avviso_contrassegno(ls_msg_avviso)
	if ll_ret<0 then
		g_mb.error(ls_msg_avviso)
		
	elseif ll_ret=1 then
		g_mb.show(ls_msg_avviso)
	end if
	
	destroy luo_avviso_sped;
	//--------------------------------------------------------------------------------------------------
	
	dw_det_fat_ven_det_1.object.b_ricerca_prodotto.enabled = false
	cb_prodotti_note_ricerca.enabled = false
//	pb_prod_view.enabled = false
	cb_sconti.enabled = false
	
	select tab_tipi_fat_ven.flag_tipo_fat_ven  
	into   :ls_flag_tipo_fat_ven  
	from   tab_tipi_fat_ven  
	where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
	
		 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi fatture.", &
				  exclamation!, ok!)
	return 1
	end if
	
	
	ls_tabella = "det_fat_ven_stat"
	ls_nome_prog = "prog_riga_fat_ven"
	
	for ll_i = 1 to this.deletedcount()
		ll_anno_registrazione = this.getitemnumber(ll_i, "anno_registrazione", delete!, true)
		ll_num_registrazione = this.getitemnumber(ll_i, "num_registrazione", delete!, true)
		ll_prog_riga_fat_ven = this.getitemnumber(ll_i, "prog_riga_fat_ven", delete!, true)	
	
		delete from det_fat_ven_cc
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione  and
				 num_registrazione = :ll_num_registrazione and
				 prog_riga_fat_ven = :ll_prog_riga_fat_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE", "Errore in cancellazione dati centri di costo.~r~n"+sqlca.sqlerrtext)
			return 1
		end if
		
		if f_elimina_det_stat(ls_tabella, ll_anno_registrazione, ll_num_registrazione, ls_nome_prog, ll_prog_riga_fat_ven) = -1 then
			return 1
		end if
	next
	

	// vado a controllare il tipo fattura ......
	// se è una fattura immediata vado ad aggiornare magazzino ( prodotti, stock ecc..), mentre
	// se la fattura è differita la modifica non ha effetto sul magazzino
	
	// stefanop 14/06/2010: ticket 2010/148: abilito anche le note di credito
	//if ls_flag_tipo_fat_ven = "I" then
	if ls_flag_tipo_fat_ven = "I" or ls_flag_tipo_fat_ven = "N" then
		for ll_i2 = 1 to this.rowcount()
			ls_cod_prodotto = this.getitemstring(ll_i2, "cod_prodotto")
			select des_imballo
			 into  :ls_des_imballo
			 from  tab_imballi
			 where cod_azienda = :s_cs_xx.cod_azienda and
					 cod_imballo = (select cod_imballo 
										 from	  anag_prodotti
										 where  cod_azienda = :s_cs_xx.cod_azienda and
												  cod_prodotto = :ls_cod_prodotto);
	
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Errore Durante l'Estrazione Descrizione Imballo.", &
								exclamation!, ok!)
				return 1
			end if
	
			if (pos(ls_aspetto_beni, ls_des_imballo) = 0 or isnull(ls_aspetto_beni) or ls_aspetto_beni = "") and &
				ls_des_imballo <> "" and not isnull(ls_des_imballo) then
				if len(ls_aspetto_beni) > 0 then
					ls_aspetto_beni = ls_aspetto_beni + " - " + ls_des_imballo
				else
					ls_aspetto_beni = ls_des_imballo
				end if
			end if
					
			ls_cod_tipo_det_ven = this.getitemstring(ll_i2, "cod_tipo_det_ven")
	
			select tab_tipi_det_ven.flag_tipo_det_ven 
			into   :ls_flag_tipo_det_ven
			from   tab_tipi_det_ven
			where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
							  exclamation!, ok!)
				return 1
			end if
	
			if ls_flag_tipo_det_ven = "M" then
				ll_prog_riga_fat_ven = this.getitemnumber(ll_i2, "prog_riga_fat_ven")
				ls_cod_tipo_movimento = this.getitemstring(ll_i2, "cod_tipo_movimento", primary!, false)
				ls_cod_prodotto = this.getitemstring(ll_i2, "cod_prodotto", primary!, false)
				ls_cod_deposito[1] = this.getitemstring(ll_i2, "cod_deposito", primary!, false)
				ls_cod_ubicazione[1] = this.getitemstring(ll_i2, "cod_ubicazione", primary!, false)
				ls_cod_lotto[1] = this.getitemstring(ll_i2, "cod_lotto", primary!, false)
				ldt_data_stock[1] = this.getitemdatetime(ll_i2, "data_stock", primary!, false)
				ll_progr_stock[1] = this.getitemnumber(ll_i2, "progr_stock", primary!, false)
				ld_quan_fatturata = this.getitemnumber(ll_i2, "quan_fatturata", primary!, false)
				ll_anno_reg_des_mov = this.getitemnumber(ll_i2, "anno_reg_des_mov", primary!, false)
				ll_num_reg_des_mov = this.getitemnumber(ll_i2, "num_reg_des_mov", primary!, false)
				setnull(ls_cod_cliente[1])
				setnull(ls_cod_fornitore[1])
	
				select det_fat_ven.prog_riga_fat_ven
				into :ll_controllo
				from det_fat_ven
				where det_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and 
						det_fat_ven.anno_registrazione = :ll_anno_registrazione and 
						det_fat_ven.num_registrazione = :ll_num_registrazione and 
						det_fat_ven.prog_riga_fat_ven = :ll_prog_riga_fat_ven;

				if sqlca.sqlcode = 0 then
					ls_cod_prodotto_old = this.getitemstring(ll_i2, "cod_prodotto", primary!, true)
					ls_cod_deposito_old = this.getitemstring(ll_i2, "cod_deposito", primary!, true)
					ls_cod_ubicazione_old = this.getitemstring(ll_i2, "cod_ubicazione", primary!, true)
					ls_cod_lotto_old = this.getitemstring(ll_i2, "cod_lotto", primary!, true)
					ldt_data_stock_old = this.getitemdatetime(ll_i2, "data_stock", primary!, true)
					ll_progr_stock_old = this.getitemnumber(ll_i2, "progr_stock", primary!, true)
					ld_quan_fatturata_old = this.getitemnumber(ll_i2, "quan_fatturata", primary!, true)
					ll_anno_reg_des_mov_old = this.getitemnumber(ll_i2, "anno_reg_des_mov", primary!, true)
					ll_num_reg_des_mov_old = this.getitemnumber(ll_i2, "num_reg_des_mov", primary!, true)
					ls_cod_tipo_movimento_old = this.getitemstring(ll_i2, "cod_tipo_movimento", primary!, true)
	
					update anag_prodotti  
						set quan_in_spedizione = quan_in_spedizione - :ld_quan_fatturata_old
					 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
							 anag_prodotti.cod_prodotto = :ls_cod_prodotto_old;
	
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", &
									  exclamation!, ok!)
						rollback;
						return 1
					end if
	
					update stock
						set quan_in_spedizione = quan_in_spedizione - :ld_quan_fatturata_old
					 where stock.cod_azienda = :s_cs_xx.cod_azienda and  
							 stock.cod_prodotto = :ls_cod_prodotto_old and
							 stock.cod_deposito = :ls_cod_deposito_old and
							 stock.cod_ubicazione = :ls_cod_ubicazione_old and
							 stock.cod_lotto = :ls_cod_lotto_old and
							 stock.data_stock = :ldt_data_stock_old and
							 stock.prog_stock = :ll_progr_stock_old;
			
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento stock.", &
									  exclamation!, ok!)
						rollback;
						return 1
					end if
	
					if ls_cod_tipo_movimento <> ls_cod_tipo_movimento_old then
						delete from dest_mov_magazzino  
						where       dest_mov_magazzino.cod_azienda = :s_cs_xx.cod_azienda and
										dest_mov_magazzino.anno_registrazione = :ll_anno_reg_des_mov_old and
										dest_mov_magazzino.num_registrazione = :ll_num_reg_des_mov_old;
		
						if sqlca.sqlcode = -1 then
							g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di cancellazione destinazioni movimenti.", &
										  exclamation!, ok!)
							rollback;
							return 1
						end if
	
						this.setitem(ll_i2, "anno_reg_des_mov", 0)
						this.setitem(ll_i2, "num_reg_des_mov", 0)
					end if
				end if
	
				if isnull(ls_cod_deposito[1]) or ls_cod_deposito[1] = "" or &
					isnull(ls_cod_ubicazione[1]) or ls_cod_ubicazione[1] = "" or &
					isnull(ls_cod_lotto[1]) or ls_cod_lotto[1] = "" or &
					isnull(ldt_data_stock[1]) or isnull(ll_progr_stock[1]) or ll_progr_stock[1] = 0 then

					g_mb.messagebox("Attenzione", "Stock non Definito." , exclamation!, ok!)
					rollback;
					return 1
				end if

				select stock.giacenza_stock - (stock.quan_assegnata + stock.quan_in_spedizione)  
				into  :ld_quan_disponibile
				from  stock
				where stock.cod_azienda = :s_cs_xx.cod_azienda and
						stock.cod_prodotto = :ls_cod_prodotto and
						stock.cod_deposito = :ls_cod_deposito[1] and
						stock.cod_ubicazione = :ls_cod_ubicazione[1] and
						stock.cod_lotto = :ls_cod_lotto[1] and
						stock.data_stock = :ldt_data_stock[1] and
						stock.prog_stock = :ll_progr_stock[1];
				
				//Donato 23-09-2009 prima di dare il messaggio controlla il prm az. CMN
				//if ld_quan_fatturata > ld_quan_disponibile then
				if ld_quan_fatturata > ld_quan_disponibile and not wf_cmn() then
					g_mb.messagebox("Attenzione", "Quantità Fattura maggiore della quantità disponibile " + &
								  f_double_string(ld_quan_disponibile) + ".", exclamation!, ok!)
					rollback;
					return 1
				end if
	
				update anag_prodotti  
					set quan_in_spedizione = quan_in_spedizione + :ld_quan_fatturata
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", &
								  exclamation!, ok!)
					rollback;
					return 1
				end if
	
				update stock
					set quan_in_spedizione = quan_in_spedizione + :ld_quan_fatturata
				 where stock.cod_azienda = :s_cs_xx.cod_azienda and  
						 stock.cod_prodotto = :ls_cod_prodotto and
						 stock.cod_deposito = :ls_cod_deposito[1] and
						 stock.cod_ubicazione = :ls_cod_ubicazione[1] and
						 stock.cod_lotto = :ls_cod_lotto[1] and
						 stock.data_stock = :ldt_data_stock[1] and
						 stock.prog_stock = :ll_progr_stock[1];
		
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento stock.", &
								  exclamation!, ok!)
					rollback;
					return 1
				end if
	
				if ll_anno_reg_des_mov = 0 then
					if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
						g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di creazione destinazioni movimenti.", &
									  exclamation!, ok!)
						rollback;
						return 1
					end if
	
					if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
						rollback;
						return 1
					end if
	
					this.setitem(ll_i2, "anno_reg_des_mov", ll_anno_reg_des_mov)
					this.setitem(ll_i2, "num_reg_des_mov", ll_num_reg_des_mov)
				end if
			end if
		next
	
		for ll_i3 = 1 to deletedcount()
			
			// aggiunto per gestione centri di costo
			
			ll_anno_registrazione = this.getitemnumber(ll_i3, "anno_registrazione", delete!, true)
			ll_num_registrazione = this.getitemnumber(ll_i3, "num_registrazione", delete!, true)
			ll_prog_riga_fat_ven = this.getitemnumber(ll_i3, "prog_riga_fat_ven", delete!, true)
			
			ls_cod_tipo_det_ven = this.getitemstring(ll_i3, "cod_tipo_det_ven", delete!, true)
	
			select tab_tipi_det_ven.flag_tipo_det_ven
			into   :ls_flag_tipo_det_ven
			from   tab_tipi_det_ven
			where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
							  exclamation!, ok!)
				return 1
			end if
	
			if ls_flag_tipo_det_ven = "M" then
				ls_cod_prodotto = this.getitemstring(ll_i3, "cod_prodotto", delete!, true)
				ld_quan_fatturata = this.getitemnumber(ll_i3, "quan_fatturata", delete!, true)
		
				update anag_prodotti  
					set quan_in_spedizione = quan_in_spedizione - :ld_quan_fatturata
				 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
	
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.", &
								  exclamation!, ok!)
					rollback;
					return 1
				end if

				update stock
					set quan_in_spedizione = quan_in_spedizione - :ld_quan_fatturata
				 where stock.cod_azienda = :s_cs_xx.cod_azienda and  
						 stock.cod_prodotto = :ls_cod_prodotto and
						 stock.cod_deposito = :ls_cod_deposito[1] and
						 stock.cod_ubicazione = :ls_cod_ubicazione[1] and
						 stock.cod_lotto = :ls_cod_lotto[1] and
						 stock.data_stock = :ldt_data_stock[1] and
						 stock.prog_stock = :ll_progr_stock[1];
		
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento stock.", &
								  exclamation!, ok!)
					rollback;
					return 1
				end if
	
				if f_elimina_dest_mov_mag(ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di cancellazione destinazione movimenti.", &
								  exclamation!, ok!)
					rollback;
					return 1
				end if

			end if
		next
	end if

	for ll_i2 = 1 to this.rowcount()
		ls_cod_prodotto = this.getitemstring(ll_i2, "cod_prodotto")
		select des_imballo
		 into  :ls_des_imballo
		 from  tab_imballi
		 where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_imballo = (select cod_imballo 
									 from	  anag_prodotti
									 where  cod_azienda = :s_cs_xx.cod_azienda and
											  cod_prodotto = :ls_cod_prodotto);

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Errore Durante l'Estrazione Descrizione Imballo.", &
							exclamation!, ok!)
			return 1
		end if

		if (pos(ls_aspetto_beni, ls_des_imballo) = 0 or isnull(ls_aspetto_beni) or ls_aspetto_beni = "") and &
			ls_des_imballo <> "" and not isnull(ls_des_imballo) then
			if len(ls_aspetto_beni) > 0 then
				ls_aspetto_beni = ls_aspetto_beni + " - " + ls_des_imballo
			else
				ls_aspetto_beni = ls_des_imballo
			end if
		end if
	next

	update tes_fat_ven
	set	 aspetto_beni = :ls_aspetto_beni
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione;
			
	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Attenzione", "Errore Durante l'Aggiornamento Imballo Fattura.", &
						exclamation!, ok!)
		return 1
	end if
	
	if ls_flag_tipo_fat_ven = "I" then

		for ll_i4 = 1 to deletedcount()
		
			ll_anno_ordine = getitemnumber(ll_i4,"anno_reg_ord_ven",delete!,true)
			ll_num_ordine = getitemnumber(ll_i4,"num_reg_ord_ven",delete!,true)
			ll_riga_ordine = getitemnumber(ll_i4,"prog_riga_ord_ven",delete!,true)
			
			if isnull(ll_anno_ordine) or isnull(ll_num_ordine) or isnull(ll_riga_ordine) then
				continue
			end if
			
			ld_quantita = getitemnumber(ll_i4,"quan_fatturata",delete!,true)
			
			select quan_evasa
			into   :ld_quan_ordine
			from   det_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_ordine and
					 num_registrazione = :ll_num_ordine and
					 prog_riga_ord_ven = :ll_riga_ordine;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in lettura dati dalla riga ordine di origine.~nErrore nella select di det_ord_ven: " + sqlca.sqlerrtext)
				return 1
			end if
			
			if ld_quan_ordine - ld_quantita = 0 then
				ls_flag_evasione = "A"
			else
				ls_flag_evasione = "P"
			end if
			
			update 	det_ord_ven
			set    		quan_evasa = quan_evasa - :ld_quantita,
						flag_evasione = :ls_flag_evasione
			where  	cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :ll_anno_ordine and
						num_registrazione = :ll_num_ordine and
						prog_riga_ord_ven = :ll_riga_ordine;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in aggiornamento riga ordine di origine.~nErrore nella update di det_ord_ven: " + sqlca.sqlerrtext)
				return 1
			end if
			
			select count(*)
			into   :ll_righe
			from   det_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_ordine and
					 num_registrazione = :ll_num_ordine;
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in lettura righe ordine di origine.~nErrore nella select di det_ord_ven: " + sqlca.sqlerrtext)
				return 1
			end if
			
			select count(*)
			into   :ll_evase
			from   det_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_ordine and
					 num_registrazione = :ll_num_ordine and
					 flag_evasione = 'E';
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in lettura righe ordine di origine.~nErrore nella select di det_ord_ven: " + sqlca.sqlerrtext)
				return 1
			end if
			
			select count(*)
			into   :ll_parziali
			from   det_ord_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 anno_registrazione = :ll_anno_ordine and
					 num_registrazione = :ll_num_ordine and
					 flag_evasione = 'P';
					 
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in lettura righe ordine di origine.~nErrore nella select di det_ord_ven: " + sqlca.sqlerrtext)
				return 1
			end if
			
			if ll_evase = ll_righe then
				ls_flag_evasione = "E"
			elseif ll_evase > 0 or ll_parziali > 0 then
				ls_flag_evasione = "P"
			else
				ls_flag_evasione = "A"	
			end if
			
			update 	tes_ord_ven
			set    		flag_evasione = :ls_flag_evasione
			where  	cod_azienda = :s_cs_xx.cod_azienda and
						anno_registrazione = :ll_anno_ordine and
						num_registrazione = :ll_num_ordine;
	
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in aggiornamento ordine di origine.~nErrore nella update di tes_ord_ven: " + sqlca.sqlerrtext)
				return 1
			end if
			
			f_scrivi_log("Cancellazione riga fattura " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + "/" + string(ll_prog_riga_fat_ven) + " con riapertura ordine nr " +  string(ll_anno_ordine) + "/" + string(ll_num_ordine) + "/" + string(ll_riga_ordine))
		next	
		
	end if
	
end if

wf_ricalcola()

//inserire qui il codice per il calcolo dell'esposizione
uo_fido_cliente l_uo_fido_cliente
long ll_return
string ls_messaggio, ls_cliente

l_uo_fido_cliente = create uo_fido_cliente

ls_cliente = i_parentdw.getitemstring(i_parentdw.i_selectedrows[1], "cod_cliente")

l_uo_fido_cliente.id_importo = getitemnumber(getrow(),"prezzo_vendita")

ll_return = l_uo_fido_cliente.uof_check_cliente(ls_cliente,datetime(today(),00:00:00),ls_messaggio)
destroy l_uo_fido_cliente

if ll_return = -1 then
	g_mb.messagebox("APICE","Errore in verifica esposizione cliente.~n" + ls_messaggio,exclamation!)
	return -1
end if

//Donato 05-11-2008 Modifica per specifica cliente PTENDA_ gestione fidi
if ll_return = 2 then
	//blocca perchè non sono autorizzato
	dw_det_fat_ven_lista.resetupdate()
	dw_det_fat_ven_det_1.resetupdate()	
end if
//fine modifica ---------------------------
end event

event updateend;call super::updateend;if rowsinserted + rowsupdated + rowsdeleted > 0 then postevent("ue_calcola")
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

for ll_i = 1 to this.rowcount()
	
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
	
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
	
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if
	
	if isnull(getitemstring(ll_i,"cod_prodotto")) or len(getitemstring(ll_i,"cod_prodotto")) < 1 then
		this.setitem(ll_i, "quantita_um", 0)
		this.setitem(ll_i, "prezzo_um", 0)
	end if
	
next

end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore, ll_anno_registrazione, ll_num_registrazione, ll_i


ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

ll_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)
if ll_errore < 0 then
   pcca.error = c_fatal
else
	// stefanop 22/06/2010: controllo se è stato impostato l'ordinamento
	for ll_i = 1 to ll_errore
		// stefanop 29/06/2010: devo controllare anche se è 0!!!
		if isnull(getitemnumber(ll_i, "ordinamento")) or getitemnumber(ll_i, "ordinamento") < 1 then
			setitem(ll_i, "ordinamento", getitemnumber(ll_i, "prog_riga_fat_ven"))
		end if
	next
	resetupdate()
	// ----
end if
end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string ls_cod_tipo_det_ven, ls_modify, ls_cod_agente_1, ls_cod_agente_2, ls_cod_deposito, &
	       ls_flag_tipo_det_ven, ls_null, ll_null
	datetime ldt_null

	dw_det_fat_ven_lista.object.cod_tipo_det_ven.protect = 1
	dw_det_fat_ven_lista.object.cod_tipo_det_ven.background.color='12632256'

	setnull(ls_null)
	setnull(ll_null)
	setnull(ldt_null)

   ls_modify = "cod_tipo_det_ven.protect='1'~t"
   dw_det_fat_ven_det_1.modify(ls_modify)
   ls_modify = "cod_tipo_det_ven.background.color='12632256'~t"
   dw_det_fat_ven_det_1.modify(ls_modify)
   ls_cod_tipo_det_ven = dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(), "cod_tipo_det_ven")
   ld_datawindow = dw_det_fat_ven_det_1
	setnull(lp_prod_view)
	setnull(lc_prodotti_ricerca)
   ls_cod_agente_1 = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
   ls_cod_agente_2 = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
   f_tipo_dettaglio_ven(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, ls_cod_tipo_det_ven, ls_cod_agente_1, ls_cod_agente_2)

   ls_cod_deposito = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_deposito")

	select tab_tipi_det_ven.flag_tipo_det_ven 
	into   :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
					  exclamation!, ok!)
	end if

	if ls_flag_tipo_det_ven = "M" then
		cb_stock.enabled = true
		ls_modify = "cod_tipo_movimento.protect='0'~t"
		ls_modify = ls_modify + "cod_tipo_movimento.background.color='16777215'~t"
		ls_modify = ls_modify + "cod_deposito.protect='0'~t"
		ls_modify = ls_modify + "cod_deposito.background.color='16777215'~t"
		ls_modify = ls_modify + "cod_ubicazione.protect='0'~t"
		ls_modify = ls_modify + "cod_ubicazione.background.color='16777215'~t"
		ls_modify = ls_modify + "cod_lotto.protect='0'~t"
		ls_modify = ls_modify + "cod_lotto.background.color='16777215'~t"
		dw_det_fat_ven_det_1.modify(ls_modify)
	else
	   if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_tipo_movimento")) then
   	   dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_tipo_movimento",ls_null)
	   end if
	   if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_deposito")) then
   	   dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_deposito",ls_null)
	   end if
	   if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_ubicazione")) then
   	   dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_ubicazione",ls_null)
	   end if
	   if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_lotto")) then
   	   dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_lotto",ls_null)
	   end if
	   if not isnull(dw_det_fat_ven_det_1.getitemdatetime(dw_det_fat_ven_det_1.getrow(),"data_stock")) then
   	   dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"data_stock",ldt_null)
	   end if
	   if not isnull(dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"progr_stock")) then
   	   dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"progr_stock",ll_null)
	   end if

		ls_modify = "cod_tipo_movimento.protect='1'~t"
		ls_modify = ls_modify + "cod_tipo_movimento.background.color='12632256'~t"
		ls_modify = ls_modify + "cod_deposito.protect='1'~t"
		ls_modify = ls_modify + "cod_deposito.background.color='12632256'~t"
		ls_modify = ls_modify + "cod_ubicazione.protect='1'~t"
		ls_modify = ls_modify + "cod_ubicazione.background.color='12632256'~t"
		ls_modify = ls_modify + "cod_lotto.protect='1'~t"
		ls_modify = ls_modify + "cod_lotto.background.color='12632256'~t"
		dw_det_fat_ven_det_1.modify(ls_modify)
	end if

   if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_prodotto")) then
      cb_prodotti_note_ricerca.enabled = true
   else
      cb_prodotti_note_ricerca.enabled = false
   end if
   cb_sconti.enabled = true
	cb_c_industriale.enabled = false
	cb_corrispondenze.enabled = false
	cb_des_mov.enabled = false
	
	uo_gestione_conversioni iuo_oggetto
	iuo_oggetto = create uo_gestione_conversioni
	ld_datawindow  = dw_det_fat_ven_det_1
	iuo_oggetto.uof_blocca_colonne(ld_datawindow, "fat_ven")
	iuo_oggetto.uof_visualizza_um(ld_datawindow, "fat_ven")
	destroy iuo_oggetto
	
	dw_folder.fu_disabletab(3)
	
end if
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	long ll_anno_registrazione, ll_num_registrazione, ll_i, ll_i1,ll_prog_riga_fat_ven
	string  ls_cod_tipo_analisi,ls_cod_prodotto,ls_cod_tipo_fat_ven,ls_cod_tipo_det_ven,ls_test
	integer li_risposta

	ll_i = i_parentdw.i_selectedrows[1]
	ll_i1 = getrow()

	ll_anno_registrazione = i_parentdw.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = i_parentdw.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_fat_ven = getitemnumber(ll_i1, "prog_riga_fat_ven")
	ls_cod_tipo_det_ven = getitemstring(ll_i1, "cod_tipo_det_ven")
	ls_cod_tipo_fat_ven = i_parentdw.getitemstring(ll_i, "cod_tipo_fat_ven")
	
	SELECT cod_tipo_analisi
	INTO   :ls_cod_tipo_analisi  
	FROM   tab_tipi_fat_ven
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND    cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;

	select cod_azienda
	into   :ls_test
	from 	 det_fat_ven_stat
	where  cod_azienda=:s_cs_xx.cod_azienda
	and    anno_registrazione=:ll_anno_registrazione
	and    num_registrazione=:ll_num_registrazione
	and    prog_riga_fat_ven=:ll_prog_riga_fat_ven;

	if sqlca.sqlcode=100 then
		li_risposta = f_crea_distribuzione(ls_cod_tipo_analisi,ls_cod_prodotto,ls_cod_tipo_det_ven,ll_num_registrazione,ll_anno_registrazione,ll_prog_riga_fat_ven,6)
		if li_risposta=-1 then
			g_mb.messagebox("Apice","Attenzione! Si è verificato un errore durante la creazione dei dettagli statistici.",exclamation!)
			pcca.error = c_fatal
		end if
	end if
	
	if is_flag_tipo_lista_fatture = "S" then
		dw_det_fat_ven_lista.Object.cf_qta_mq.Background.Color = RGB(192, 192, 192)
	end if	

end if
end event

event itemchanged;call super::itemchanged;if i_extendmode then
   datawindow ld_datawindow
   commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
   string 		ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_messaggio, ls_des_prodotto, &
				ls_cod_tipo_listino_prodotto, ls_cod_cliente, ls_cod_valuta, ls_colonna_sconto, &
				ls_flag_tipo_det_ven, ls_modify, ls_null, ls_cod_agente_1, &
				ls_cod_agente_2, ls_cod_tipo_det_ven, ls_flag_decimali, ls_cod_versione, &
				ls_cod_misura_mag, ls_cod_deposito, ls_cod_tipo_movimento, ls_nota_prodotto, ls_cod_fornitore, &
				ls_cod_iva_cliente, ls_cod_iva_prodotto, ls_cod_prodotto_td, ls_nota_testata, ls_stampa_piede, ls_nota_video
	long ll_riga_origine, ll_i, ll_null, ll_y
   dec{4}	ld_fat_conversione, ld_quantita, ld_cambio_ven, ld_quan_impegnata, ld_ultimo_prezzo, ld_variazioni[], &
			 	ll_sconti[], ll_maggiorazioni[], ld_min_fat_altezza,ld_min_fat_larghezza,ld_min_fat_profondita, &
			 	ld_min_fat_superficie,ld_min_fat_volume, ld_quan_proposta
   datetime ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva, ldt_null

   setnull(ls_null)
   setnull(ldt_null)
   setnull(ll_null)
	
   ls_cod_tipo_listino_prodotto = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
   ldt_data_registrazione = dw_det_fat_ven_lista.i_parentdw.getitemdatetime(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
   ls_cod_cliente = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
   ls_cod_valuta = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
   ld_cambio_ven = dw_det_fat_ven_lista.i_parentdw.getitemnumber(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
   ls_cod_agente_1 = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
   ls_cod_agente_2 = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
   ls_cod_tipo_det_ven = this.getitemstring(this.getrow(),"cod_tipo_det_ven")
   
   choose case i_colname
		case "prog_riga_bol_ven"
			ll_riga_origine = this.getrow()
			for ll_i = 1 to this.rowcount()
				if ll_i <> ll_riga_origine and &
					long(i_coltext) = this.getitemnumber(ll_i, "prog_riga_bol_ven") then
	            g_mb.messagebox("Attenzione", "Progressivo riga già utilizzato.", &
   	                     exclamation!, ok!)
               i_coltext = string(this.getitemnumber(ll_riga_origine, "prog_riga_bol_ven", primary!,true))
               this.setitem(ll_riga_origine, "prog_riga_bol_ven", double(i_coltext))
               this.settext(i_coltext)
               return 2
					pcca.error = c_fatal
				end if
			next
			
		case "cod_tipo_det_ven"
			
			ls_modify = "cod_prodotto.protect='0'~t"
			ls_modify = ls_modify + "cod_prodotto.background.color='16777215'~t"
			ls_modify = ls_modify + "cod_versione.protect='0'~t"
			ls_modify = ls_modify + "cod_versione.background.color='16777215'~t"
			ls_modify = ls_modify + "des_prodotto.protect='0'~t"
			ls_modify = ls_modify + "des_prodotto.background.color='16777215'~t"
			ls_modify = ls_modify + "cod_misura.protect='0'~t"
			ls_modify = ls_modify + "cod_misura.background.color='16777215'~t"
			ls_modify = ls_modify + "fat_conversione_ven.protect='0'~t"
			ls_modify = ls_modify + "fat_conversione_ven.background.color='16777215'~t"
			ls_modify = ls_modify + "quan_fatturata.protect='0'~t"
			ls_modify = ls_modify + "quan_fatturata.background.color='16777215'~t"
			ls_modify = ls_modify + "prezzo_vendita.protect='0'~t"
			ls_modify = ls_modify + "prezzo_vendita.background.color='16777215'~t"
			ls_modify = ls_modify + "sconto_1.protect='0'~t"
			ls_modify = ls_modify + "sconto_1.background.color='16777215'~t"
			ls_modify = ls_modify + "sconto_2.protect='0'~t"
			ls_modify = ls_modify + "sconto_2.background.color='16777215'~t"
			ls_modify = ls_modify + "cod_iva.protect='0'~t"
			ls_modify = ls_modify + "cod_iva.background.color='16777215'~t"
			ls_modify = ls_modify + "provvigione_1.protect='0'~t"
			ls_modify = ls_modify + "provvigione_1.background.color='16777215'~t"
			ls_modify = ls_modify + "provvigione_2.protect='0'~t"
			ls_modify = ls_modify + "provvigione_2.background.color='16777215'~t"
			ls_modify = ls_modify + "cod_deposito.protect='0'~t"
			ls_modify = ls_modify + "cod_deposito.background.color='16777215'~t"
			ls_modify = ls_modify + "cod_ubicazione.protect='0'~t"
			ls_modify = ls_modify + "cod_ubicazione.background.color='16777215'~t"
			ls_modify = ls_modify + "cod_lotto.protect='0'~t"
			ls_modify = ls_modify + "cod_lotto.background.color='16777215'~t"
			ls_modify = ls_modify + "cod_tipo_movimento.protect='0'~t"
			ls_modify = ls_modify + "cod_tipo_movimento.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)

			ld_datawindow = dw_det_fat_ven_det_1
			setnull(lp_prod_view)
			setnull(lc_prodotti_ricerca)
			f_tipo_dettaglio_ven(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext, ls_cod_agente_1, ls_cod_agente_2)
			ld_datawindow = dw_det_fat_ven_lista
			f_tipo_dettaglio_ven_lista(ld_datawindow, i_coltext)

			ls_cod_deposito = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_deposito")

			select tab_tipi_det_ven.flag_tipo_det_ven,
					 tab_tipi_det_ven.cod_tipo_movimento
			into   :ls_flag_tipo_det_ven,
					 :ls_cod_tipo_movimento
			from   tab_tipi_det_ven
			where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_tipi_det_ven.cod_tipo_det_ven = :i_coltext;
		
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
							  exclamation!, ok!)
			end if
		
			if ls_flag_tipo_det_ven = "M" then
				this.setitem(this.getrow(), "cod_deposito", ls_cod_deposito)
				this.setitem(this.getrow(), "cod_tipo_movimento", ls_cod_tipo_movimento)
				cb_stock.enabled = true
				if f_proponi_stock (i_coltext, this, row, ls_messaggio ) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
				end if
			else
				cb_stock.enabled = false
				if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_tipo_movimento")) then
					dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_tipo_movimento",ls_null)
				end if
				if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_deposito")) then
					dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_deposito",ls_null)
				end if
				if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_ubicazione")) then
					dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_ubicazione",ls_null)
				end if
				if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_lotto")) then
					dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_lotto",ls_null)
				end if
				if not isnull(dw_det_fat_ven_det_1.getitemdatetime(dw_det_fat_ven_det_1.getrow(),"data_stock")) then
					dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"data_stock", ldt_null)
				end if
				if not isnull(dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"progr_stock")) then
					dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"progr_stock",ll_null)
				end if
		
				ls_modify = "cod_tipo_movimento.protect='1'~t"
				ls_modify = ls_modify + "cod_tipo_movimento.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_deposito.protect='1'~t"
				ls_modify = ls_modify + "cod_deposito.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_ubicazione.protect='1'~t"
				ls_modify = ls_modify + "cod_ubicazione.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_lotto.protect='1'~t"
				ls_modify = ls_modify + "cod_lotto.background.color='12632256'~t"
				dw_det_fat_ven_det_1.modify(ls_modify)
			end if

			if isnull(this.getitemstring(this.getrow(),"cod_prodotto")) then
				cb_prodotti_note_ricerca.enabled=false
			end if

			select cod_iva 
			into :ls_cod_iva_cliente
			from anag_clienti
			where cod_azienda = :s_cs_xx.cod_azienda
			and cod_cliente = :ls_cod_cliente;
			
			if isnull(this.getitemstring(this.getrow(),"cod_prodotto")) then
				
				ls_cod_prodotto_td = this.getitemstring(this.getrow(),"cod_prodotto")
				
				select 	cod_iva 
				into 		:ls_cod_iva_prodotto
				from 		anag_prodotti
				where 	cod_azienda = :s_cs_xx.cod_azienda and
							cod_prodotto = :ls_cod_prodotto_td;	
				
			end if 

			if (isnull(ls_cod_iva_cliente) or ls_cod_iva_cliente = "") and (isnull(ls_cod_iva_prodotto) or ls_cod_iva_prodotto = "") then	
				select tab_tipi_det_ven.cod_iva  
				into   :ls_cod_iva  
				from   tab_tipi_det_ven  
				where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
						 tab_tipi_det_ven.cod_tipo_det_ven = :i_coltext;
				if sqlca.sqlcode = 0 then
					this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
				end if
   			end if
				
			dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(), "nota_dettaglio", ls_null)
			
			select 	quan_default
			into 		:ld_quan_proposta
			from 		tab_tipi_det_ven
			where 	cod_azienda = :s_cs_xx.cod_azienda  and
						cod_tipo_det_ven = :i_coltext;
				
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice", "Errore in lettura dati da tabella tab_tipi_ord_ven " + sqlca.sqlerrtext)
				return
			end if
			
			if isnull(ld_quan_proposta) then ld_quan_proposta = 0
			dw_det_fat_ven_lista.setitem(dw_det_fat_ven_lista.getrow(), "quan_fatturata", ld_quan_proposta)			

//--------------------------------------- Fine Modifica ----------------------------------------------------------------	
			
			
		case "cod_prodotto"
			if isnull(data) or data = "" then return 0
			
			if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1
			
			select cod_misura_mag,
					cod_misura_ven,
					fat_conversione_ven,
					flag_decimali,
					cod_iva,
					quan_impegnata,
					des_prodotto
			into   :ls_cod_misura_mag,
					:ls_cod_misura,
					:ld_fat_conversione,
					:ls_flag_decimali,
					:ls_cod_iva,
					:ld_quan_impegnata,
					:ls_des_prodotto
			from   	anag_prodotti
			where  	cod_azienda = :s_cs_xx.cod_azienda and 
						cod_prodotto = :i_coltext;

			if sqlca.sqlcode = 0 then
				if guo_functions.uof_get_note_fisse(ls_null, ls_null, i_coltext, "FAT_VEN", ls_null, ldt_data_registrazione, ls_nota_testata, ls_stampa_piede, ls_nota_video) < 0 then
					g_mb.error(ls_nota_testata)
				else
					if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
				end if
				
				this.setitem(i_rownbr, "cod_misura", ls_cod_misura)
				this.setitem(i_rownbr, "des_prodotto", ls_des_prodotto)
				
				dw_det_fat_ven_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
				dw_det_fat_ven_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")
				dw_det_fat_ven_det_1.modify("prezzo_vendita_t.text='Prezzo " + ls_cod_misura + ":'")
				dw_det_fat_ven_det_1.modify("quan_fatturata_t.text='Quantità " + ls_cod_misura + ":'")
	
				if ld_fat_conversione <> 0 then 
					setitem(i_rownbr, "fat_conversione_ven", ld_fat_conversione)
				else
					ld_fat_conversione = 1
					setitem(i_rownbr, "fat_conversione_ven", 1)
				end if
				
				if not isnull(ls_cod_iva) and len(ls_cod_iva) > 0 then setitem(i_rownbr, "cod_iva", ls_cod_iva)
					
				uo_1.uof_aggiorna(i_coltext)
				ls_cod_prodotto = i_coltext
				uo_1.uof_ultimo_prezzo( ls_cod_cliente, ls_cod_prodotto)
				if f_proponi_stock (this.getitemstring(row,"cod_tipo_det_ven"), this, row, ls_messaggio ) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
				end if
				
			else
				dw_det_fat_ven_det_1.modify("st_prezzo.text='Prezzo:'")
				dw_det_fat_ven_det_1.modify("st_quantita.text='Quantità:'")
				dw_det_fat_ven_det_1.modify("prezzo_vendita_t.text='Prezzo:'")
				dw_det_fat_ven_det_1.modify("quan_fatturata_t.text='Quantità:'")
				guo_ricerca.uof_ricerca_prodotto(dw_det_fat_ven_lista,"cod_prodotto")
				return
			end if

			dw_det_fat_ven_lista.postevent("ue_postopen")

			if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_fat_ven_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(ld_fat_conversione)) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
			else
				dw_det_fat_ven_det_1.modify("st_fattore_conv.text=''")
			end if
			
			if not isnull(ls_cod_cliente) then
				select anag_clienti.cod_iva,
						 anag_clienti.data_esenzione_iva
				into   :ls_cod_iva,
						 :ldt_data_esenzione_iva
				from   anag_clienti
				where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
						 anag_clienti.cod_cliente = :ls_cod_cliente;
			end if

			if sqlca.sqlcode = 0 then
				if ls_cod_iva <> "" and &
					(ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
					this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
				end if
			end if

			ls_cod_fornitore = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")
			
			if not isnull(ls_cod_fornitore) then
				select 	cod_iva,
						 	data_esenzione_iva
				into   	:ls_cod_iva,
						 	:ldt_data_esenzione_iva
				from   	anag_fornitori
				where  	cod_azienda = :s_cs_xx.cod_azienda and 
						 	cod_fornitore = :ls_cod_fornitore;
			end if

			if sqlca.sqlcode = 0 then
				if ls_cod_iva <> "" and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
					this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
				end if
         	end if				

	         ls_cod_prodotto = i_coltext
			ld_quantita = this.getitemnumber(i_rownbr, "quan_fatturata")
			if ls_flag_decimali = "N" and &
				ld_quantita - int(ld_quantita) > 0 then
				ld_quantita = ceiling(ld_quantita)
				this.setitem(i_rownbr, "quan_fatturata", ld_quantita)
			end if

			if mid(f_flag_controllo(),1,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			if not isnull(i_coltext) then
				cb_prodotti_note_ricerca.enabled = true
			else
				cb_prodotti_note_ricerca.enabled = false
			end if
			
			if f_leggi_nota_prodotto(ls_cod_tipo_det_ven, ls_cod_prodotto, ls_nota_prodotto) = 0 then
			dw_det_fat_ven_det_1.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
			dw_det_fat_ven_det_1.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if

			f_PO_LoadDDDW_DW(dw_det_fat_ven_det_1,"cod_versione",sqlca,&
						  "distinta_padri","cod_versione","des_versione",&
						  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + i_coltext +"'")
						  
			select cod_versione
			into   :ls_cod_versione
			from   distinta_padri
			where  cod_azienda=:s_cs_xx.cod_azienda
			and    cod_prodotto=:i_coltext
			and    flag_predefinita='S';
			if sqlca.sqlcode = 0 then 
				setitem(getrow(), "cod_versione", ls_cod_versione)
			elseif sqlca.sqlcode = 100 then
				setitem(getrow(), "cod_versione", ls_null)
			else
				g_mb.messagebox("APICE","Errore in ricerca codice versiuone distinta predefinita. Dettaglio errore=" + sqlca.sqlerrtext)
			end if
			
			dw_det_fat_ven_det_1.postevent("ue_ricalcola_colonne")
			

		case "quan_fatturata"
			
      		ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			
			if not isnull(ls_cod_prodotto) and len(trim(ls_cod_prodotto)) > 0 then

				select anag_prodotti.flag_decimali
				into   :ls_flag_decimali
				from   anag_prodotti
				where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
				if sqlca.sqlcode = 0 then
					if ls_flag_decimali = "N" and &
						(double(i_coltext) - int(double(i_coltext))) > 0 then
						i_coltext = string(ceiling(double(i_coltext)))
						this.setitem(i_rownbr, "quan_fatturata", double(i_coltext))
						this.settext(i_coltext)
						return 2
					end if
				end if
	
				if mid(f_flag_controllo(),2,1) = "S" then
					ld_quantita = double(i_coltext)
					iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
					iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
					iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
					iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
					iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
					iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
					iuo_condizioni_cliente.str_parametri.dim_1 = 0
					iuo_condizioni_cliente.str_parametri.dim_2 = 0
					iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
					iuo_condizioni_cliente.str_parametri.valore = 0
					iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
					iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
					iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
					iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
					iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
					iuo_condizioni_cliente.wf_condizioni_cliente()
				end if			
				iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_fat_ven_det_1, "fat_ven", "quan_doc", &
																				ld_quantita, &
																				dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"prezzo_vendita"), &
																				dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"quantita_um"), &
																				dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"prezzo_um"), &
																				dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"fat_conversione_ven"), &
																				ls_cod_valuta)
																				
			end if
			
		case "sconto_1", "sconto_2", "sconto_3", "sconto_4", "sconto_5", "sconto_6", "sconto_7", "sconto_8", "sconto_9", "sconto_10"
			
			ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
						
			if mid(f_flag_controllo(),9,1) = "S" and not isnull(ls_cod_prodotto) and len(trim(ls_cod_prodotto)) > 0 then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.prezzo_listino = getitemnumber(getrow(),"prezzo_vendita")
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_fatturata")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
		case "prezzo_vendita"
			
			ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
						
			if mid(f_flag_controllo(),3,1) = "S" and not isnull(ls_cod_prodotto) and len(trim(ls_cod_prodotto)) > 0 then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.prezzo_listino = double(i_coltext)
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_fatturata")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_fat_ven_det_1, "fat_ven", "prezzo_doc", &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"quan_fatturata"), &
			              												double(i_coltext), &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
																						
   end choose
end if
end event

event ue_key;call super::ue_key;string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, &
       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_listino, ls_messaggio
long ll_i, ll_y
double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo, ll_sconti[], ll_maggiorazioni[], &
	    ld_min_fat_altezza,ld_min_fat_larghezza,ld_min_fat_profondita,ld_min_fat_superficie,ld_min_fat_volume
datetime ldt_data_registrazione


choose case this.getcolumnname()

	case "sconto_1"
		if key = keyenter! then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		end if
		if is_flag_tipo_lista_fatture = "N" then
			if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
				this.triggerevent("pcd_save")
				this.postevent("pcd_new")
			end if
		end if		
				
	case "prezzo_vendita"
		if is_flag_tipo_lista_fatture = "S" then		
			if key = keyTab! and keyflags <> 1 and keyflags <> 2 and keyflags <> 3 then
				this.triggerevent("pcd_save")
				this.postevent("pcd_new")
			end if				
		end if					
		if key = keyenter! then
			this.triggerevent("pcd_save")
			this.postevent("pcd_new")
		else
			if (key = keyF1! or key = keyF2! or key = keyF3! or key = keyF4! or key = keyF5!) and keyflags = 1 then
				setpointer(hourglass!)
				ldt_data_registrazione = dw_det_fat_ven_lista.i_parentdw.getitemdatetime(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
				ls_cod_cliente = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
				ls_cod_valuta = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
				ls_cod_agente_1 = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
				ls_cod_agente_2 = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
				ld_cambio_ven = dw_det_fat_ven_lista.i_parentdw.getitemnumber(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
				ls_cod_tipo_det_ven = dw_det_fat_ven_lista.getitemstring(dw_det_fat_ven_lista.getrow(), "cod_tipo_det_ven")
				ls_cod_prodotto = dw_det_fat_ven_lista.getitemstring(dw_det_fat_ven_lista.getrow(), "cod_prodotto")
				ld_quantita = dw_det_fat_ven_lista.getitemnumber(dw_det_fat_ven_lista.getrow(), "quan_fatturata")
				choose case key
					case keyF1!
						ls_listino = 'LI1'
					case keyF2!
						ls_listino = 'LI2'
					case keyF3!
						ls_listino = 'LI3'
					case keyF4!
						ls_listino = 'LI4'
					case keyF5!
						ls_listino = 'LI5'
					case else
						return
				end choose
				if ls_listino <> "LI5" then
					select stringa
					into  :ls_stringa
					from  parametri_azienda
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_parametro = :ls_listino ;
					if sqlca.sqlcode = 0 and mid(f_flag_controllo(),1,1) = "S" then
						iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
						iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_stringa
						iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
						iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
						iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
						iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
						iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
						iuo_condizioni_cliente.str_parametri.dim_1 = 0
						iuo_condizioni_cliente.str_parametri.dim_2 = 0
						iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
						iuo_condizioni_cliente.str_parametri.valore = 0
						iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
						iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
						iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
						iuo_condizioni_cliente.wf_condizioni_cliente()
					end if			
					setpointer(arrow!)
				else
					select prezzo_acquisto
					into   :ld_prezzo_acquisto
					from   anag_prodotti
					where  cod_azienda = :s_cs_xx.cod_azienda and
					       cod_prodotto = :ls_cod_prodotto;
					dw_det_fat_ven_lista.setitem(i_rownbr, "prezzo_vendita", ld_prezzo_acquisto)
				end if					 
				setpointer(arrow!)
			end if
		end if
	case "cod_prodotto", "des_prodotto"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_fat_ven_lista,"cod_prodotto")
		end if

end choose


end event

type dw_det_fat_ven_det_1 from uo_cs_xx_dw within w_det_fat_ven
event ue_ricalcola_colonne ( )
integer x = 46
integer y = 124
integer width = 3406
integer height = 1336
integer taborder = 130
boolean bringtotop = true
string dataobject = "d_det_fat_ven_det_1"
boolean border = false
end type

event ue_ricalcola_colonne;iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_fat_ven_det_1,"fat_ven", "prezzo_doc", &
																dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"quan_fatturata"), &
																dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"prezzo_vendita"), &
																dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"quantita_um"), &
																dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"prezzo_um"), &
																dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"fat_conversione_ven"), &
																dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta"))

end event

event itemchanged;call super::itemchanged;if i_extendmode then
	datawindow ld_datawindow
	commandbutton lc_prodotti_ricerca
	picturebutton lp_prod_view
	string ls_cod_misura, ls_cod_iva, ls_cod_prodotto, ls_cod_deposito, ls_cod_tipo_movimento, &
			 ls_cod_tipo_listino_prodotto, ls_cod_cliente, ls_cod_valuta, ls_flag_prezzo, &
			 ls_flag_tipo_det_ven, ls_modify, ls_null, ls_cod_agente_1, ls_des_prodotto, &
			 ls_cod_agente_2, ls_cod_tipo_det_ven, ls_flag_decimali, ls_colonna_sconto, &
			 ls_cod_misura_mag, ls_nota_prodotto, ls_cod_versione, ls_messaggio, ls_cod_fornitore, &
			 ls_cod_tipo_fat_ven, ls_cod_prodotto_td, ls_cod_iva_cliente, ls_cod_iva_prodotto, ls_nota_testata, ls_stampa_piede, ls_nota_video
	long 	ll_riga_origine, ll_i, ll_null, ll_y
	dec{4} ld_fat_conversione, ld_quantita, ld_cambio_ven, ld_ultimo_prezzo, ld_variazioni[], &
			 ld_num_confezioni, ld_num_pezzi, ll_sconti[], ll_maggiorazioni[], ld_min_fat_altezza, &
			 ld_min_fat_larghezza,ld_min_fat_profondita,ld_min_fat_superficie,ld_min_fat_volume
	datetime ldt_data_consegna, ldt_data_registrazione, ldt_data_esenzione_iva, ldt_null

   setnull(ls_null)
   setnull(ldt_null)
   setnull(ll_null)

   ls_cod_tipo_listino_prodotto = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_tipo_listino_prodotto")
   ldt_data_registrazione = dw_det_fat_ven_lista.i_parentdw.getitemdatetime(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "data_registrazione")
   ls_cod_cliente = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_cliente")
   ls_cod_valuta = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_valuta")
   ld_cambio_ven = dw_det_fat_ven_lista.i_parentdw.getitemnumber(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cambio_ven")
   ls_cod_agente_1 = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_1")
   ls_cod_agente_2 = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_agente_2")
   ls_cod_tipo_det_ven = this.getitemstring(this.getrow(),"cod_tipo_det_ven")
   
   choose case i_colname
			
		case "prog_riga_fat_ven"
			
			ll_riga_origine = this.getrow()
			for ll_i = 1 to this.rowcount()
				if ll_i <> ll_riga_origine and &
					long(i_coltext) = this.getitemnumber(ll_i, "prog_riga_fat_ven") then
	            g_mb.messagebox("Attenzione", "Progressivo riga già utilizzato.", &
   	                     exclamation!, ok!)
               i_coltext = string(this.getitemnumber(ll_riga_origine, "prog_riga_fat_ven", primary!,true))
               this.setitem(ll_riga_origine, "prog_riga_fat_ven", double(i_coltext))
               this.settext(i_coltext)
               return 2
					pcca.error = c_fatal
				end if
			next
			
		case "cod_tipo_det_ven"
			
			ls_modify = "cod_prodotto.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "cod_prodotto.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
		
			ls_modify = "cod_versione.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "cod_versione.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
		
			ls_modify = "des_prodotto.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "des_prodotto.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "cod_misura.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "cod_misura.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "fat_conversione_ven.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "fat_conversione_ven.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "quan_fatturata.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "quan_fatturata.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "prezzo_vendita.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "prezzo_vendita.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_1.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_1.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_2.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "sconto_2.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "cod_iva.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "cod_iva.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_1.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_1.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_2.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "provvigione_2.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "cod_deposito.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "cod_deposito.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "cod_ubicazione.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "cod_ubicazione.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "cod_lotto.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "cod_lotto.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "cod_tipo_movimento.protect='0'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
			ls_modify = "cod_tipo_movimento.background.color='16777215'~t"
			dw_det_fat_ven_det_1.modify(ls_modify)
		
			ld_datawindow = dw_det_fat_ven_det_1
			setnull(lp_prod_view)
			setnull(lc_prodotti_ricerca)
			f_tipo_dettaglio_ven(ld_datawindow, lc_prodotti_ricerca, lp_prod_view, i_coltext, ls_cod_agente_1, ls_cod_agente_2)
			ld_datawindow = dw_det_fat_ven_lista
			f_tipo_dettaglio_ven_lista(ld_datawindow, i_coltext)
		
			ls_cod_deposito = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_deposito")
		
			select tab_tipi_det_ven.flag_tipo_det_ven,
					 tab_tipi_det_ven.cod_tipo_movimento
			into   :ls_flag_tipo_det_ven,
					 :ls_cod_tipo_movimento
			from   tab_tipi_det_ven
			where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_tipi_det_ven.cod_tipo_det_ven = :i_coltext;
		
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", &
							  exclamation!, ok!)
			end if
		
			if ls_flag_tipo_det_ven = "M" then
				this.setitem(this.getrow(), "cod_deposito", ls_cod_deposito)
				this.setitem(this.getrow(), "cod_tipo_movimento", ls_cod_tipo_movimento)
				cb_stock.enabled = true
				if f_proponi_stock (i_coltext, this, row, ls_messaggio ) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
				end if
			else
				cb_stock.enabled = false
				if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_tipo_movimento")) then
					dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_tipo_movimento",ls_null)
				end if
				if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_deposito")) then
					dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_deposito",ls_null)
				end if
				if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_ubicazione")) then
					dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_ubicazione",ls_null)
				end if
				if not isnull(dw_det_fat_ven_det_1.getitemstring(dw_det_fat_ven_det_1.getrow(),"cod_lotto")) then
					dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"cod_lotto",ls_null)
				end if
				if not isnull(dw_det_fat_ven_det_1.getitemdatetime(dw_det_fat_ven_det_1.getrow(),"data_stock")) then
					dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"data_stock", ldt_null)
				end if
				if not isnull(dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"progr_stock")) then
					dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(),"progr_stock",ll_null)
				end if
		
				ls_modify = "cod_tipo_movimento.protect='1'~t"
				ls_modify = ls_modify + "cod_tipo_movimento.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_deposito.protect='1'~t"
				ls_modify = ls_modify + "cod_deposito.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_ubicazione.protect='1'~t"
				ls_modify = ls_modify + "cod_ubicazione.background.color='12632256'~t"
				ls_modify = ls_modify + "cod_lotto.protect='1'~t"
				ls_modify = ls_modify + "cod_lotto.background.color='12632256'~t"
				dw_det_fat_ven_det_1.modify(ls_modify)
			end if
			
			if isnull(this.getitemstring(this.getrow(),"cod_prodotto")) then
				cb_prodotti_note_ricerca.enabled=false
			end if
		
		//------------------------- Modifica Nicola 22/06/2000 -------------------------------------
		
			select cod_iva 
			  into :ls_cod_iva_cliente
			  from anag_clienti
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_cliente = :ls_cod_cliente;
			
			if isnull(this.getitemstring(this.getrow(),"cod_prodotto")) then
				ls_cod_prodotto_td = this.getitemstring(this.getrow(),"cod_prodotto")
				select cod_iva 
				  into :ls_cod_iva_prodotto
				  from anag_prodotti
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_prodotto = :ls_cod_prodotto_td;	
			end if 
		
		//------------------------- Fine modifica --------------------------------------------------
			if (isnull(ls_cod_iva_cliente) or ls_cod_iva_cliente = "") and (isnull(ls_cod_iva_prodotto) or ls_cod_iva_prodotto = "") then	
		
		//         if isnull(this.getitemstring(this.getrow(), "cod_iva")) then
				select tab_tipi_det_ven.cod_iva  
				into   :ls_cod_iva  
				from   tab_tipi_det_ven  
				where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
						 tab_tipi_det_ven.cod_tipo_det_ven = :i_coltext;
				if sqlca.sqlcode = 0 then
					this.setitem(this.getrow(), "cod_iva", ls_cod_iva)
				end if
			end if
			dw_det_fat_ven_det_1.setitem(dw_det_fat_ven_det_1.getrow(), "nota_dettaglio", ls_null)

		case "cod_prodotto"
			if isnull(data) or data = "" then return 0
			
			if event ue_check_prodotto_bloccato(ldt_data_registrazione,row, dwo, data) > 0 then return 1

			select cod_misura_mag,
					 cod_misura_ven,
					 fat_conversione_ven,
					 flag_decimali,
					 cod_iva,
					 des_prodotto
			into   :ls_cod_misura_mag,
					 :ls_cod_misura,
					 :ld_fat_conversione,
					 :ls_flag_decimali,
					 :ls_cod_iva,
					 :ls_des_prodotto
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and 
			 			cod_prodotto = :i_coltext;
   
			if sqlca.sqlcode = 0 then
				if guo_functions.uof_get_note_fisse(ls_null, ls_null, i_coltext, "FAT_VEN", ls_null, ldt_data_registrazione, ls_nota_testata, ls_stampa_piede, ls_nota_video) < 0 then
					g_mb.error(ls_nota_testata)
				else
					if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
				end if
				
				this.setitem(i_rownbr, "cod_misura", ls_cod_misura)
				this.setitem(i_rownbr, "des_prodotto", ls_des_prodotto)

				dw_det_fat_ven_det_1.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
				dw_det_fat_ven_det_1.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")
				dw_det_fat_ven_det_1.modify("prezzo_vendita_t.text='Prezzo " + ls_cod_misura + ":'")
				dw_det_fat_ven_det_1.modify("quan_fatturata_t.text='Quantità " + ls_cod_misura + ":'")
				
				if ld_fat_conversione <> 0 then
					this.setitem(i_rownbr, "fat_conversione_ven", ld_fat_conversione)
				else
					ld_fat_conversione = 1
					this.setitem(i_rownbr, "fat_conversione_ven", 1)
				end if
				
	            if not isnull(ls_cod_iva) then this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
					
				uo_1.uof_aggiorna(i_coltext)
				
				ls_cod_prodotto = i_coltext
				uo_1.uof_ultimo_prezzo( ls_cod_cliente, ls_cod_prodotto)
				
				if f_proponi_stock (this.getitemstring(row,"cod_tipo_det_ven"), this, row, ls_messaggio ) <> 0 then
					g_mb.messagebox("APICE",ls_messaggio)
				end if
				
			else
				dw_det_fat_ven_det_1.modify("st_prezzo.text='Prezzo:'")
				dw_det_fat_ven_det_1.modify("st_quantita.text='Quantità:'")
				dw_det_fat_ven_det_1.modify("prezzo_vendita_t.text='Prezzo:'")
				dw_det_fat_ven_det_1.modify("quan_fatturata_t.text='Quantità:'")
				guo_ricerca.uof_ricerca_prodotto(dw_det_fat_ven_det_1,"cod_prodotto")
				return 0
			end if
			
			dw_det_fat_ven_lista.postevent("ue_postopen")

			setnull(ls_cod_iva)
			
			if isnull(ls_cod_cliente) then
				if not isnull(ls_cod_fornitore) then
					
					select cod_iva,
							 data_esenzione_iva
					into   :ls_cod_iva,
							 :ldt_data_esenzione_iva
					from   anag_fornitori
					where  cod_azienda = :s_cs_xx.cod_azienda and 
							 cod_fornitore = :ls_cod_fornitore;
				end if			
				
			else
				
				select cod_iva,
						 data_esenzione_iva
				into   :ls_cod_iva,
						 :ldt_data_esenzione_iva
				from   anag_clienti
				where  cod_azienda = :s_cs_xx.cod_azienda and 
						 cod_cliente = :ls_cod_cliente;
						 
			end if

			if sqlca.sqlcode = 0 then
				if not isnull(ls_cod_iva) and (ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
					this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
				end if
			end if
			
			ls_cod_fornitore = dw_det_fat_ven_lista.i_parentdw.getitemstring(dw_det_fat_ven_lista.i_parentdw.i_selectedrows[1], "cod_fornitore")
			
			if not isnull(ls_cod_fornitore) then
				select cod_iva,
						 data_esenzione_iva
				into   :ls_cod_iva,
						 :ldt_data_esenzione_iva
				from   anag_fornitori
				where  cod_azienda = :s_cs_xx.cod_azienda and 
						 cod_fornitore = :ls_cod_fornitore;
	         end if

			if sqlca.sqlcode = 0 then
				if ls_cod_iva <> "" and &
					(ldt_data_esenzione_iva <= ldt_data_registrazione or isnull(ldt_data_esenzione_iva)) then
					this.setitem(i_rownbr, "cod_iva", ls_cod_iva)
				end if
			end if				


			ls_cod_prodotto = i_coltext
	         ld_quantita = this.getitemnumber(i_rownbr, "quan_fatturata")
			if ls_flag_decimali = "N" and ld_quantita - int(ld_quantita) > 0 then
				ld_quantita = ceiling(ld_quantita)
				this.setitem(i_rownbr, "quan_fatturata", ld_quantita)
			end if
			
			if mid(f_flag_controllo(),1,1) = "S" then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = ld_fat_conversione
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
			if not isnull(i_coltext) then
				cb_prodotti_note_ricerca.enabled = true
			else
				cb_prodotti_note_ricerca.enabled = false
			end if
			
			if f_leggi_nota_prodotto(ls_cod_tipo_det_ven, ls_cod_prodotto, ls_nota_prodotto) = 0 then
				dw_det_fat_ven_det_1.setitem(i_rownbr, "nota_dettaglio", ls_nota_prodotto)
			else
				dw_det_fat_ven_det_1.setitem(i_rownbr, "nota_dettaglio", ls_null)
			end if

			f_PO_LoadDDDW_DW(dw_det_fat_ven_det_1,"cod_versione",sqlca,&
         		        	  "distinta_padri","cod_versione","des_versione",&
	  		      		     "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_prodotto='" + i_coltext + "'")

			select cod_versione
			into   :ls_cod_versione
			from   distinta_padri
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_prodotto = :i_coltext and
			       flag_predefinita = 'S';
			if sqlca.sqlcode = 0 then 
				setitem(getrow(), "cod_versione", ls_cod_versione)
			elseif sqlca.sqlcode = 100 then
				setitem(getrow(), "cod_versione", ls_null)
			else
				g_mb.messagebox("APICE","Errore in ricerca codice versiuone distinta predefinita. Dettaglio errore=" + sqlca.sqlerrtext)
			end if
			
			postevent("ue_ricalcola_colonne")
			
		case "cod_misura"
			dw_det_fat_ven_lista.postevent("ue_postopen")

		case "fat_conversione_ven"
			ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			
			select cod_misura_mag
			into   :ls_cod_misura_mag
			from   anag_prodotti
			where  cod_azienda = :s_cs_xx.cod_azienda and 
					 cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> this.getitemstring(i_rownbr, "cod_misura") and &
				len(trim(this.getitemstring(this.getrow(),"cod_misura"))) <> 0 and &
				len(trim(ls_cod_misura_mag)) <> 0 then
				dw_det_fat_ven_det_1.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + i_coltext + " " + this.getitemstring(i_rownbr, "cod_misura") + ")'")
			else
				dw_det_fat_ven_det_1.modify("st_fattore_conv.text=''")
			end if
			
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_fat_ven_det_1, "fat_ven", "fattore", &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"quan_fatturata"), &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"prezzo_vendita"), &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"prezzo_um"), &
			              												double(i_coltext), &
																			ls_cod_valuta)
		case "quan_fatturata"
	         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			if not isnull(ls_cod_prodotto) and len(trim(ls_cod_prodotto)) > 0 then
				select anag_prodotti.flag_decimali
				into   :ls_flag_decimali
				from   anag_prodotti
				where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
						 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
		
				if sqlca.sqlcode = 0 then
					if ls_flag_decimali = "N" and &
						(double(i_coltext) - int(double(i_coltext))) > 0 then
						i_coltext = string(ceiling(double(i_coltext)))
						this.setitem(i_rownbr, "quan_fatturata", double(i_coltext))
						this.settext(i_coltext)
						return 2
					end if
				end if
	
				ld_quantita = double(i_coltext)
				
				if mid(f_flag_controllo(),2,1) = "S" then
					iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
					iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
					iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
					iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
					iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
					iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
					iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
					iuo_condizioni_cliente.str_parametri.dim_1 = 0
					iuo_condizioni_cliente.str_parametri.dim_2 = 0
					iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
					iuo_condizioni_cliente.str_parametri.valore = 0
					iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
					iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
					iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
					iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
					iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
					iuo_condizioni_cliente.wf_condizioni_cliente()
				end if
				iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_fat_ven_det_1, "fat_ven", "quan_doc", &
																				ld_quantita, &
																				dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"prezzo_vendita"), &
																				dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"quantita_um"), &
																				dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"prezzo_um"), &
																				dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"fat_conversione_ven"), &
																				ls_cod_valuta)
				
			end if
			
		case "num_confezioni"
			
	         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			ld_quantita = this.getitemnumber(this.getrow(),"quan_fatturata")
			ld_num_confezioni = double(i_coltext)
			ld_num_pezzi  = this.getitemnumber(this.getrow(),"num_pezzi_confezione")
			ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")
			select flag_prezzo
			into   :ls_flag_prezzo
			from   tab_misure
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_misura = :ls_cod_misura;
			if sqlca.sqlcode = 0 then
				if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
					if ls_flag_prezzo = 'S' then
						ld_quantita = ld_num_pezzi * ld_num_confezioni
						this.setitem(i_rownbr, "quan_fatturata", ld_quantita)
					else
						ld_quantita = ld_num_confezioni
						this.setitem(i_rownbr, "quan_fatturata", ld_quantita)
					end if
				end if
			else
				g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
				return 1
			end if	
			
			if mid(f_flag_controllo(),6,1) = "S" and not isnull(ls_cod_prodotto) and len(trim(ls_cod_prodotto)) > 0 then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
		case "num_pezzi_confezione"
			
	         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
			ld_quantita = this.getitemnumber(this.getrow(),"quan_fatturata")
			ld_num_pezzi = double(i_coltext)
			ld_num_confezioni = this.getitemnumber(this.getrow(),"num_confezioni")
			ls_cod_misura = this.getitemstring(this.getrow(),"cod_misura")
			select flag_prezzo
			into   :ls_flag_prezzo
			from   tab_misure
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_misura = :ls_cod_misura;
			if sqlca.sqlcode = 0 then
				if not isnull(ld_num_confezioni) and ld_num_confezioni <> 0 and ld_num_pezzi > 0 then
					if ls_flag_prezzo = 'S' then
						ld_quantita = ld_num_pezzi * ld_num_confezioni
						this.setitem(i_rownbr, "quan_fatturata", ld_quantita)
					else
						ld_quantita = ld_num_confezioni
						this.setitem(i_rownbr, "quan_fatturata", ld_quantita)
					end if
				end if
			else
				g_mb.messagebox("Dettagli fatture","Si è verificato un errore in ricerca flag_prezzo in tabella unità di misure~r~nErrore nr." + string(sqlca.sqlcode) + "~r~nDescrizione errore " + sqlca.sqlerrtext)
				return 1
			end if				
			if mid(f_flag_controllo(),5,1) = "S"  and not isnull(ls_cod_prodotto) and len(trim(ls_cod_prodotto)) > 0 then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.quantita = ld_quantita
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
		case "sconto_1", "sconto_2", "sconto_3", "sconto_4", "sconto_5", "sconto_6", "sconto_7", "sconto_8", "sconto_9", "sconto_10"
			ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
			if mid(f_flag_controllo(),9,1) = "S"  and not isnull(ls_cod_prodotto) and len(trim(ls_cod_prodotto)) > 0 then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.prezzo_listino = getitemnumber(getrow(),"prezzo_vendita")
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_fatturata")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			
		case "prezzo_vendita"
			
			ls_cod_prodotto = getitemstring(getrow(),"cod_prodotto")
			if mid(f_flag_controllo(),3,1) = "S" and not isnull(ls_cod_prodotto) and len(trim(ls_cod_prodotto)) > 0 then
				iuo_condizioni_cliente.str_parametri.ldw_oggetto = this
				iuo_condizioni_cliente.str_parametri.cod_tipo_listino_prodotto = ls_cod_tipo_listino_prodotto
				iuo_condizioni_cliente.str_parametri.cod_valuta = ls_cod_valuta
				iuo_condizioni_cliente.str_parametri.cambio_ven = ld_cambio_ven
				iuo_condizioni_cliente.str_parametri.data_riferimento = ldt_data_registrazione
				iuo_condizioni_cliente.str_parametri.cod_cliente = ls_cod_cliente
				iuo_condizioni_cliente.str_parametri.cod_prodotto = ls_cod_prodotto
				iuo_condizioni_cliente.str_parametri.dim_1 = 0
				iuo_condizioni_cliente.str_parametri.dim_2 = 0
				iuo_condizioni_cliente.str_parametri.valore = 0
				iuo_condizioni_cliente.str_parametri.cod_agente_1 = ls_cod_agente_1
				iuo_condizioni_cliente.str_parametri.cod_agente_2 = ls_cod_agente_2
				iuo_condizioni_cliente.str_parametri.colonna_quantita = "quan_fatturata"
				iuo_condizioni_cliente.str_parametri.colonna_prezzo = "prezzo_vendita"
				iuo_condizioni_cliente.str_parametri.prezzo_listino = double(i_coltext)
				iuo_condizioni_cliente.str_parametri.fat_conversione_ven = getitemnumber(getrow(),"fat_conversione_ven")
				iuo_condizioni_cliente.str_parametri.quantita = getitemnumber(getrow(),"quan_fatturata")
				iuo_condizioni_cliente.ib_prezzi = false
				for ll_i = 1 to 10
					if i_colname = "sconto_" + string(ll_i) then
						iuo_condizioni_cliente.str_output.sconti[ll_i] = double(i_coltext)
					else
						iuo_condizioni_cliente.str_output.sconti[ll_i] = getitemnumber(getrow(),"sconto_" + string(ll_i) )
					end if
				next
				iuo_condizioni_cliente.wf_condizioni_cliente()
			end if
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_fat_ven_det_1, "fat_ven", "prezzo_doc", &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"quan_fatturata"), &
			              												double(i_coltext), &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"quantita_um"), &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
		case "quantita_um"  
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_fat_ven_det_1, "fat_ven", "quan_um", &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"quan_fatturata"), &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"prezzo_vendita"), &
			              												double(i_coltext), &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"prezzo_um"), &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
		case "prezzo_um"  
			iuo_gestione_conversioni.uof_ricalcola_quantita(dw_det_fat_ven_det_1, "fat_ven", "prezzo_um", &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"quan_fatturata"), &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"prezzo_vendita"), &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"quantita_um"), &
			              												double(i_coltext), &
			              												dw_det_fat_ven_det_1.getitemnumber(dw_det_fat_ven_det_1.getrow(),"fat_conversione_ven"), &
																			ls_cod_valuta)
			
	end choose
end if
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_modify, ls_flag_tipo_det_ven, ls_cod_tipo_det_ven

if i_rownbr > 0 then
   ls_cod_tipo_det_ven = this.getitemstring(i_rownbr,"cod_tipo_det_ven")

   select tab_tipi_det_ven.flag_tipo_det_ven
   into   :ls_flag_tipo_det_ven
   from   tab_tipi_det_ven
   where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
          tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

   if sqlca.sqlcode <> 0 then
      g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura Tabella Tipi dettaglio Vendita.", &
                 exclamation!, ok!)
      return
   end if

   if i_insave > 0 then
      if ls_flag_tipo_det_ven = "M" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("cod_misura")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Unità di misura obbligatoria.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if

         i_colnbr = column_nbr("cod_deposito")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Deposito obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
         i_colnbr = column_nbr("cod_tipo_movimento")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Tipo movimento obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if

         i_colnbr = column_nbr("quan_fatturata")
         if double(get_col_data(i_rownbr, i_colnbr)) = 0 then
          	g_mb.messagebox("Attenzione", "Quantità fatturata obbligatoria.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      elseif ls_flag_tipo_det_ven = "C" then
         i_colnbr = column_nbr("cod_prodotto")
         if isnull(get_col_data(i_rownbr, i_colnbr)) then
          	g_mb.messagebox("Attenzione", "Codice prodotto obbligatorio.", &
                       exclamation!, ok!)
            pcca.error = c_fatal
            return
         end if
      end if
   end if
end if
end event

event ue_key;call super::ue_key;string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, &
       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_listino, ls_messaggio
long ll_sconti[], ll_maggiorazioni[], ll_i, ll_y
double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo
datetime ldt_data_registrazione


choose case this.getcolumnname()

//	case "cod_prodotto"
//		if key = keyF1!  and keyflags = 1 then
//			this.change_dw_current()
//			guo_ricerca.uof_ricerca_prodotto(dw_det_fat_ven_det_1,"cod_prodotto")
//		end if
//	case "des_prodotto"
//		if key = keyF1!  and keyflags = 1 then
//			this.change_dw_current()
//			guo_ricerca.uof_ricerca_prodotto(dw_det_fat_ven_det_1,"cod_prodotto")
//		end if
//	case "nota_dettaglio"
//		if key = keyF1!  and keyflags = 1 then
//			s_cs_xx.parametri.parametro_s_1 = dw_det_fat_ven_det_1.getitemstring(this.getrow(),"cod_prodotto")
//			dw_det_fat_ven_det_1.change_dw_current()
//			setnull(s_cs_xx.parametri.parametro_s_2)
//			window_open(w_prod_note_ricerca, 0)
//			if not isnull(s_cs_xx.parametri.parametro_s_2) then
//				this.setcolumn("nota_dettaglio")
//				this.settext(this.gettext() + "~r~n" + s_cs_xx.parametri.parametro_s_2)
//			end if
//		end if
		
	//Giulio: 03/11/2011 modifica ricerca prodotti
	case "des_prodotto","cod_prodotto","nota_dettaglio"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_prodotto(dw_det_fat_ven_det_1,"cod_prodotto")
		end if
end choose


end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	
	dw_documenti.uof_retrieve_blob(getrow())
	dw_det_fat_ven_lista.setrow(getrow())
	
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_det_fat_ven_det_1,"cod_prodotto")
end choose
end event

event pcd_view;call super::pcd_view;dw_det_fat_ven_det_1.object.b_ricerca_prodotto.visible = false
end event

event pcd_new;call super::pcd_new;dw_det_fat_ven_det_1.object.b_ricerca_prodotto.visible = true
end event

event pcd_modify;call super::pcd_modify;dw_det_fat_ven_det_1.object.b_ricerca_prodotto.visible = true
end event

event pcd_delete;call super::pcd_delete;dw_det_fat_ven_det_1.object.b_ricerca_prodotto.visible = false
end event

event clicked;call super::clicked;long											ll_numero, ll_row
integer										li_anno
s_cs_xx_parametri							lstr_param
w_commesse_produzione_tv			lw_window
w_mov_magazzino_tv					lw_window2

choose case dwo.name
	case "p_commessa"
		//apri la window delle commesse produzione
		ll_row = dw_det_fat_ven_det_1.getrow()
		
		if ll_row>0 then
			li_anno = dw_det_fat_ven_det_1.getitemnumber(ll_row, "anno_commessa")
			ll_numero = dw_det_fat_ven_det_1.getitemnumber(ll_row, "num_commessa")
				
			if li_anno>0 and ll_numero>0 then
				lstr_param.parametro_ul_1 = li_anno
				lstr_param.parametro_ul_2 = ll_numero
				
				opensheetwithparm(lw_window, lstr_param, PCCA.MDI_Frame, 6,  Original!)
				//openwithparm(w_commesse_produzione_tv, lstr_param)
			else
				//nessuna commessa associata
				return
			end if
		end if
	
	case "p_mov"
		//apri la window dei movimenti di magazzino
		ll_row = dw_det_fat_ven_det_1.getrow()
		
		if ll_row>0 then
			li_anno = dw_det_fat_ven_det_1.getitemnumber(ll_row, "anno_registrazione_mov_mag")
			ll_numero = dw_det_fat_ven_det_1.getitemnumber(ll_row, "num_registrazione_mov_mag")
				
			if li_anno>0 and ll_numero>0 then
				lstr_param.parametro_ul_1 = li_anno
				lstr_param.parametro_ul_2 = ll_numero
				
				opensheetwithparm(lw_window2, lstr_param, PCCA.MDI_Frame, 6,  Original!)
				//openwithparm(w_mov_magazzino_tv, lstr_param)
			else
				//nessuna movimento associato
				return
			end if
		end if
		
end choose


end event

