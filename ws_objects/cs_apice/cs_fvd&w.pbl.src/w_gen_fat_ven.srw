﻿$PBExportHeader$w_gen_fat_ven.srw
$PBExportComments$Finestra Generazione Fatture Vendita da Bolle Vendita
forward
global type w_gen_fat_ven from w_cs_xx_principale
end type
type dw_1 from datawindow within w_gen_fat_ven
end type
type dw_ext_sel_gen_fat_ven from uo_cs_xx_dw within w_gen_fat_ven
end type
type dw_folder from u_folder within w_gen_fat_ven
end type
type dw_gen_orig_dest from uo_cs_xx_dw within w_gen_fat_ven
end type
type cb_genera from uo_cb_ok within w_gen_fat_ven
end type
type cb_cerca from commandbutton within w_gen_fat_ven
end type
type dw_gen_fat_ven from uo_cs_xx_dw within w_gen_fat_ven
end type
end forward

global type w_gen_fat_ven from w_cs_xx_principale
integer width = 3511
integer height = 2448
string title = "Fatturazione Bolle"
dw_1 dw_1
dw_ext_sel_gen_fat_ven dw_ext_sel_gen_fat_ven
dw_folder dw_folder
dw_gen_orig_dest dw_gen_orig_dest
cb_genera cb_genera
cb_cerca cb_cerca
dw_gen_fat_ven dw_gen_fat_ven
end type
global w_gen_fat_ven w_gen_fat_ven

type variables
boolean					ib_messaggio_retrieve=true
string						is_cod_documento[], is_numeratore_documento[]
long						il_anno_documento[], il_num_documento[], il_incremento_riga

string						is_cod_parametro_blocco = "GFV"
uo_fido_cliente			iuo_fido

end variables

forward prototypes
public function integer wf_genera_fatture_singole (long al_anno_registrazione, ref string ls_messaggio)
public function integer wf_crea_nota_fissa (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio)
public function integer wf_genera_fatture_riepilogate (long al_anno_registrazione, ref string ls_messaggio)
public function integer wf_genera_fatture_singole_1 (long al_anno_registrazione, string as_cod_tipo_bol_ven, long al_anno_bolla, long al_num_bolla, ref string ls_messaggio)
public function integer wf_controlla_blocchi_clienti ()
end prototypes

public function integer wf_genera_fatture_singole (long al_anno_registrazione, ref string ls_messaggio);long ll_i, ll_rows, ll_anno_bolla, ll_num_bolla
//long ll_selected[]
string ls_cod_tipo_bol_ven

ll_rows = dw_gen_fat_ven.rowcount()
for ll_i = 1 to ll_rows
	
	//elabora solo quelle con il check box selezionato
	if dw_gen_fat_ven.getitemstring(ll_i, "selezionata") = "S" and &
			dw_gen_fat_ven.getitemstring(ll_i, "confermata") = "S" then
	else
		continue
	end if
	
	ls_cod_tipo_bol_ven = dw_gen_fat_ven.getitemstring(ll_i, "cod_tipo_bol_ven")
	ll_anno_bolla = dw_gen_fat_ven.getitemnumber(ll_i, "anno_registrazione")
	ll_num_bolla = dw_gen_fat_ven.getitemnumber(ll_i, "num_registrazione")
	
	if wf_genera_fatture_singole_1(al_anno_registrazione, ls_cod_tipo_bol_ven, ll_anno_bolla, ll_num_bolla, ls_messaggio) = -1 then
		return -1
	end if
next


return 0
end function

public function integer wf_crea_nota_fissa (long fl_anno_registrazione, long fl_num_registrazione, ref string fs_messaggio);string ls_cod_cliente, ls_cod_fornitore, ls_nota_testata, ls_nota_piede, ls_nota_video, &
		 ls_db, ls_nota_testata_1, ls_nota_piede_1, ls_cod_tipo_fat_ven, ls_null
datetime ldt_data_registrazione
integer li_risposta

li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)

			
select cod_cliente, 
       nota_testata,
		 nota_piede,
		 data_registrazione,
		 cod_tipo_fat_ven
into   :ls_cod_cliente,
       :ls_nota_testata,
		 :ls_nota_piede,
		 :ldt_data_registrazione,
		 :ls_cod_tipo_fat_ven
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione;
if sqlca.sqlcode <> 0 then
	fs_messaggio = "Errore in ricerca fattura per inserimento nota fissa: verificare"
	return -1
end if

setnull(ls_cod_fornitore)
setnull(ls_null)
if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_cod_fornitore, ls_null, "GEN_FAT_VEN", ls_cod_tipo_fat_ven, ldt_data_registrazione, ls_nota_testata, ls_nota_piede, ls_nota_video) < 0 then
	g_mb.error(ls_nota_testata)
else
	if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
end if
//f_crea_nota_fissa_tipo_doc(ls_cod_cliente, ls_cod_fornitore, "GEN_FAT_VEN", ls_cod_tipo_fat_ven, ldt_data_registrazione, ls_nota_testata, ls_nota_piede, ls_nota_video)

ls_nota_testata_1 = ls_nota_testata
ls_nota_piede_1 = ls_nota_piede

// ----- controllo lunghezza note per ASE
if (ls_db = "SYBASE_ASE" or ls_db = "MSSQL") and len(ls_nota_testata_1) > 255 then
	ls_nota_testata_1 = left(ls_nota_testata_1, 255)
end if
if (ls_db = "SYBASE_ASE" or ls_db = "MSSQL") and len(ls_nota_piede_1) > 255 then
	ls_nota_piede_1 = left(ls_nota_piede_1, 255)
end if
// -----  fine controllo lunghezza note per ASE



update tes_fat_ven  
	set nota_testata = :ls_nota_testata_1,
		 nota_piede = :ls_nota_piede_1
 where tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_fat_ven.anno_registrazione = :fl_anno_registrazione and  
		 tes_fat_ven.num_registrazione = :fl_num_registrazione;
		
if sqlca.sqlcode = -1 then
	fs_messaggio = "Si è verificato un errore in fase di aggiornamento note nella Testata Fattura Vendita."
	return -1
end if

return 0
end function

public function integer wf_genera_fatture_riepilogate (long al_anno_registrazione, ref string ls_messaggio);boolean						lb_nuova_aliquota

integer						li_risposta, li_riga_orig

string							ls_cod_tipo_bol_ven, ls_flag_riep_fat, ls_cod_tipo_fat_ven, ls_cod_cliente[1], &
								ls_cod_valuta, ls_cod_pagamento, ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca, &
								ls_cod_banca_clien_for, ls_cod_des_cliente, ls_num_ord_cliente, &
								ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
								ls_cod_resa, ls_flag_tipo_doc, ls_flag_destinazione, ls_flag_num_doc, &
								ls_flag_imballo, ls_flag_vettore, ls_flag_inoltro, ls_flag_mezzo, ls_flag_porto, &
								ls_flag_resa, ls_flag_data_consegna, ls_cod_tipo_rag, ls_sort, ls_key_sort, &
								ls_key_sort_old, ls_cod_tipo_analisi, ls_causale_trasporto_tab, ls_lire, &
								ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
								ls_cap, ls_provincia, ls_flag_fuori_fido, ls_flag_movimenti, &
								ls_cod_tipo_listino_prodotto, ls_aspetto_beni, ls_causale_trasporto, ls_nota_testata, &
								ls_nota_piede, ls_vostro_ordine, ls_vostro_ordine_data, ls_cod_documento ,ls_numeratore_documento,&
								ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_cod_tipo_det_ven, &
								ls_cod_misura, ls_cod_prodotto, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio,  &
								ls_cod_centro_costo, ls_flag_tipo_det_ven, ls_cod_tipo_movimento, ls_flag_sola_iva, &
								ls_cod_fornitore[1], ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, &
								ls_prog_riga_doc_des, ls_cod_tipo_analisi_ord, ls_flag_evasione, ls_cod_versione, &
								ls_flag_abilita_rif_bol_ven, ls_cod_tipo_det_ven_rif_bol_ven, ls_des_riferimento_fat_ven, &
								ls_flag_rif_cod_documento, ls_flag_rif_numeratore_doc, ls_flag_rif_anno_documento, &
								ls_flag_rif_num_documento, ls_flag_rif_data_fattura, ls_cod_causale, ls_flag_raggruppo_doc_iva, &
								ls_des_prodotto_iva[], ls_cod_ive[], ls_nota_testata_old, ls_nota_piede_old, ls_cod_tipo_det_ven_2, &
								ls_db, ls_cod_tipo_det_ven_negativo, ls_cod_tipo_det_ven_positivo, &
								ls_cod_des_fattura_com, ls_rag_soc_1_dest_fat, ls_rag_soc_2_dest_fat, ls_indirizzo_dest_fat, ls_localita_dest_fat, ls_frazione_dest_fat, &
								ls_cap_dest_fat, ls_provincia_dest_fat, ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det, &
								ls_cod_doc_orig,ls_numeratore_orig, ls_cod_misura_mag, ls_cod_natura_intra
								
long							ll_i, ll_num_righe, ll_anno_bolla, ll_num_bolla, ll_prog_riga_documento, &
								ll_num_registrazione, ll_prog_riga_fat_ven, ll_prog_riga_bol_ven, ll_prog_riga, &
								ll_progr_stock, ll_anno_commessa, ll_num_commessa, ll_anno_reg_des_mov, &
								ll_num_reg_des_mov, ll_num_bolla_old, ll_rows, ll_row, ll_iva, &
								ll_anno_registrazione_mov_mag, &
								ll_num_registrazione_mov_mag, ll_anno_reg_ord_ven, ll_num_reg_ord_ven, &
								ll_prog_riga_ord_ven, ll_anno_bol_ven, ll_num_bol_ven, ll_anno_documento, ll_num_documento, &
								ll_count, ll_anno_orig, ll_num_orig, ll_max_registrazione
								
double						ld_sconto_pagamento, ld_cambio_ven, ld_sconto_testata, ld_peso_netto, ld_peso_lordo, &
								ld_num_colli, ld_tot_val_evaso, ld_tot_val_ordine, &
								ld_quan_in_evasione, ld_quan_ordine, ld_prezzo_vendita, ld_fat_conversione_ven, &
								ld_sconto_1, ld_sconto_2, ld_provvigione_1, ld_provvigione_2, ld_quan_evasa, &
								ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, &
								ld_sconto_9, ld_sconto_10, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, &
								ld_val_sconto_2, ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, &
								ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, ld_val_riga_sconto_5, &
								ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, &
								ld_val_sconto_8, ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, &
								ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, ld_quan_consegnata, &
								ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_prezzo_vendita_iva[], ll_sconto_1, ll_sconto_2, &
								ll_sconto_3, ll_sconto_4, ll_sconto_5, ll_sconto_6, ll_sconto_7, ll_sconto_8, ll_sconto_9, ll_sconto_10, &
								ll_provvigione_1, ll_provvigione_2, ll_num_confezioni, ll_num_pezzi_confezione, ld_quantita_um, ld_prezzo_um
								
datetime						ldt_data_consegna, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_registrazione, ldt_data_orig, ldt_data_stock, &
								ldt_data_consegna_ordine, ldt_data_bolla, ldt_oggi
								
datastore					lds_rag_documenti, lds_det_bol_ven

string							ls_flag_forza_tipo_fattura, ls_cod_operatore_new

uo_generazione_documenti				luo_genera_doc
uo_calcola_documento_euro			luo_calcolo



ldt_oggi = datetime(today(),00:00:00)
li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)

ll_num_righe = dw_gen_fat_ven.rowcount()
lds_rag_documenti = create datastore
lds_rag_documenti.dataobject = "d_rag_documenti_bol"
lds_rag_documenti.settransobject(sqlca)
ls_cod_tipo_rag = dw_ext_sel_gen_fat_ven.getitemstring(1,"cod_tipo_raggruppo")

guo_functions.uof_get_parametro_azienda( "IRF", ref il_incremento_riga)
if isnull(il_incremento_riga) then il_incremento_riga = 10


for ll_i = 1 to ll_num_righe
	
	//elabora solo quelle con il check box selezionato
	if dw_gen_fat_ven.getitemstring(ll_i, "selezionata") = "S" and &
			dw_gen_fat_ven.getitemstring(ll_i, "confermata") = "S" then
	else
		continue
	end if
	
	ls_cod_tipo_bol_ven = dw_gen_fat_ven.getitemstring(ll_i, "cod_tipo_bol_ven")
	ll_anno_bolla = dw_gen_fat_ven.getitemnumber(ll_i, "anno_registrazione")
	ll_num_bolla = dw_gen_fat_ven.getitemnumber(ll_i, "num_registrazione")

	select tes_bol_ven.flag_riep_fat
	into   :ls_flag_riep_fat
	from   tes_bol_ven  
	where  tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and 
			 tes_bol_ven.anno_registrazione = :ll_anno_bolla and 
			 tes_bol_ven.num_registrazione = :ll_num_bolla;
			 
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura della Testata Bolla Vendita"
		return -1
	end if

	if ls_flag_riep_fat = 'N' then
		if wf_genera_fatture_singole_1(al_anno_registrazione, ls_cod_tipo_bol_ven, ll_anno_bolla, ll_num_bolla, ls_messaggio) = -1 then
			return -1
		end if
	else
		
		//Donato 03/04/2009 ---------------------------------------------------------------------------------------------------------------
		/*	Il tipo di fattura da generare dipende dal flag forza tipo fattura
			e dal valore del tipo fattura da usare. Se il falg di forzatura non viene specificato allora "comanda"
			il tipo fattura associato al tipo ddt della bolla da fatturare (cioè tutto come prima)
		*/
		ls_flag_forza_tipo_fattura = dw_ext_sel_gen_fat_ven.getitemstring(dw_ext_sel_gen_fat_ven.getrow(), &
																												"flag_forza_tipo")
		if ls_flag_forza_tipo_fattura = "S" then
			ls_cod_tipo_fat_ven = dw_ext_sel_gen_fat_ven.getitemstring(dw_ext_sel_gen_fat_ven.getrow(), &
																												"cod_tipo_fat_ven")
			if isnull(ls_cod_tipo_fat_ven) or ls_cod_tipo_fat_ven = "" then
				g_mb.messagebox("Attenzione", "E' necessario specificare il tipo fattura da usare nella generazione!", &
						  Exclamation!, ok!)
				dw_ext_sel_gen_fat_ven.setcolumn("cod_tipo_fat_ven")
				setpointer(arrow!)
				return -1
			end if
		else
			//tutto come prima
			select tab_tipi_bol_ven.cod_tipo_fat_ven
			into   :ls_cod_tipo_fat_ven
			from   tab_tipi_bol_ven  
			where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
			
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Errore durante la lettura della tabella Tipi Bolle Vendita."
				return -1
			end if
		end if
		
		
		if not isnull(ls_cod_tipo_fat_ven) then
			if not isnull(ls_cod_tipo_rag) and trim(ls_cod_tipo_rag) <> "" then
				select tab_tipi_rag.flag_tipo_doc,   
						 tab_tipi_rag.flag_destinazione,   
						 tab_tipi_rag.flag_num_doc,   
						 tab_tipi_rag.flag_imballo,   
						 tab_tipi_rag.flag_vettore,   
						 tab_tipi_rag.flag_inoltro,   
						 tab_tipi_rag.flag_mezzo,   
						 tab_tipi_rag.flag_porto,   
						 tab_tipi_rag.flag_resa,   
						 tab_tipi_rag.flag_data_consegna  
				into   :ls_flag_tipo_doc,   
						 :ls_flag_destinazione,   
						 :ls_flag_num_doc,   
						 :ls_flag_imballo,   
						 :ls_flag_vettore,   
						 :ls_flag_inoltro,   
						 :ls_flag_mezzo,   
						 :ls_flag_porto,   
						 :ls_flag_resa,   
						 :ls_flag_data_consegna  
				from   tab_tipi_rag  
				where  tab_tipi_rag.cod_azienda = :s_cs_xx.cod_azienda and
						 tab_tipi_rag.cod_tipo_rag = :ls_cod_tipo_rag;
	
				if sqlca.sqlcode <> 0 then
					ls_messaggio = "Errore durante la lettura Tabella Tipi Ragruppamenti."
					destroy lds_rag_documenti
					return -1
				end if
			end if
			
			select tes_bol_ven.cod_cliente,   
					 tes_bol_ven.cod_valuta,   
					 tes_bol_ven.cod_pagamento,   
					 tes_bol_ven.cod_agente_1,
					 tes_bol_ven.cod_agente_2,
					 tes_bol_ven.cod_banca,   
					 tes_bol_ven.cod_banca_clien_for,
					 tes_bol_ven.cod_tipo_bol_ven,
					 tes_bol_ven.cod_des_cliente,   
					 tes_bol_ven.num_ord_cliente,   
					 tes_bol_ven.cod_imballo,   
					 tes_bol_ven.cod_vettore,   
					 tes_bol_ven.cod_inoltro,   
					 tes_bol_ven.cod_mezzo,
					 tes_bol_ven.cod_porto,   
					 tes_bol_ven.cod_resa
			 into  :ls_cod_cliente[1],
					 :ls_cod_valuta,   
					 :ls_cod_pagamento,   
					 :ls_cod_agente_1,
					 :ls_cod_agente_2,
					 :ls_cod_banca,   
					 :ls_cod_banca_clien_for,
					 :ls_cod_tipo_bol_ven,
					 :ls_cod_des_cliente,   
					 :ls_num_ord_cliente,   
					 :ls_cod_imballo,   
					 :ls_cod_vettore,   
					 :ls_cod_inoltro,   
					 :ls_cod_mezzo,   
					 :ls_cod_porto,
					 :ls_cod_resa
			from   tes_bol_ven  
			where  tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
					 tes_bol_ven.anno_registrazione = :ll_anno_bolla and  
					 tes_bol_ven.num_registrazione = :ll_num_bolla;
		
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Errore durante la lettura Testata Bolle Vendita."
				destroy lds_rag_documenti
				return -1
			end if

			select flag_raggruppo_iva_doc
			  into :ls_flag_raggruppo_doc_iva
			  from anag_clienti
			 where cod_azienda = :s_cs_xx.cod_azienda and
			 		 cod_cliente = :ls_cod_cliente[1];

			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Errore durante la lettura Anagrafica Clienti."
				destroy lds_rag_documenti
				return -1
			end if

			lds_rag_documenti.insertrow(0)
			lds_rag_documenti.setrow(lds_rag_documenti.rowcount())
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "anno_documento", ll_anno_bolla)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "num_documento", ll_num_bolla)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_cliente", ls_cod_cliente[1])
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_valuta", ls_cod_valuta)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_pagamento", ls_cod_pagamento)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_agente_1", ls_cod_agente_1)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_agente_2", ls_cod_agente_2)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca", ls_cod_banca)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_banca_clien_for", ls_cod_banca_clien_for)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_tipo_documento", ls_cod_tipo_bol_ven)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_doc_esterno", ls_num_ord_cliente)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_imballo", ls_cod_imballo)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_vettore", ls_cod_vettore)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_inoltro", ls_cod_inoltro)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_mezzo", ls_cod_mezzo)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_porto", ls_cod_porto)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_resa", ls_cod_resa)
			lds_rag_documenti.setitem(lds_rag_documenti.getrow(), "cod_destinazione", ls_cod_des_cliente)
		end if
	end if
next

ls_sort = "cod_cliente A, cod_valuta A, cod_pagamento A, cod_agente_1 A, cod_agente_2 A, cod_banca A, cod_banca_clien_for A"

if ls_flag_tipo_doc = "S" then
	ls_sort = ls_sort + ", cod_tipo_documento A"
end if

if ls_flag_destinazione = "S" then
	ls_sort = ls_sort + ", cod_destinazione A"
end if

if ls_flag_num_doc = "S" then
	ls_sort = ls_sort + ", cod_doc_esterno A"
end if

if ls_flag_imballo = "S" then
	ls_sort = ls_sort + ", cod_imballo A"
end if

if ls_flag_vettore = "S" then
	ls_sort = ls_sort + ", cod_vettore A"
end if

if ls_flag_inoltro = "S" then
	ls_sort = ls_sort + ", cod_inoltro A"
end if

if ls_flag_mezzo = "S" then
	ls_sort = ls_sort + ", cod_mezzo A"
end if

if ls_flag_porto = "S" then
	ls_sort = ls_sort + ", cod_porto A"
end if

if ls_flag_resa = "S" then
	ls_sort = ls_sort + ", cod_resa A"
end if

ls_sort = ls_sort + ", anno_documento A, num_documento A, prog_riga_documento A"

lds_rag_documenti.setsort(ls_sort)

lds_rag_documenti.sort()

ls_key_sort_old = ""
ll_num_bolla_old = 0
for ll_i = 1 to lds_rag_documenti.rowcount()
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_cliente")) then
		ls_key_sort = lds_rag_documenti.getitemstring(ll_i, "cod_cliente")
	else
		ls_key_sort = "      "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_valuta")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_valuta")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_pagamento")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_agente_1")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_agente_1")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_agente_2")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_agente_2")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_banca_clien_for")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_tipo_doc = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	string ls_prova
	ls_prova = lds_rag_documenti.getitemstring(ll_i, "cod_destinazione")
	if ls_flag_destinazione = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_destinazione")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_destinazione")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_num_doc = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_doc_esterno")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_doc_esterno")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_imballo = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_imballo")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_imballo")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_vettore = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_vettore")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_vettore")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_inoltro = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_inoltro")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_inoltro")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_mezzo = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_mezzo")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_mezzo")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_porto = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_porto")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_porto")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	if ls_flag_resa = "S" and &
		not isnull(lds_rag_documenti.getitemstring(ll_i, "cod_resa")) then
		ls_key_sort = ls_key_sort + lds_rag_documenti.getitemstring(ll_i, "cod_resa")
	else
		ls_key_sort = ls_key_sort + "   "
	end if
	
	ll_anno_bolla = lds_rag_documenti.getitemnumber(ll_i, "anno_documento")
	ll_num_bolla = lds_rag_documenti.getitemnumber(ll_i, "num_documento")
	ls_cod_tipo_bol_ven = lds_rag_documenti.getitemstring(ll_i, "cod_tipo_documento")
	
	//Donato 03/04/2009 ---------------------------------------------------------------------------------------------------------------
	/*	Il tipo di fattura da generare dipende dal flag forza tipo fattura
		e dal valore del tipo fattura da usare. Se il flag di forzatura non viene specificato allora "comanda"
		il tipo fattura associato al tipo ddt della bolla da fatturare (cioè tutto come prima)
	*/
	ls_flag_forza_tipo_fattura = dw_ext_sel_gen_fat_ven.getitemstring(dw_ext_sel_gen_fat_ven.getrow(), &
																											"flag_forza_tipo")
	if ls_flag_forza_tipo_fattura = "S" then
		ls_cod_tipo_fat_ven = dw_ext_sel_gen_fat_ven.getitemstring(dw_ext_sel_gen_fat_ven.getrow(), &
																											"cod_tipo_fat_ven")
		if isnull(ls_cod_tipo_fat_ven) or ls_cod_tipo_fat_ven = "" then
			g_mb.messagebox("Attenzione", "E' necessario specificare il tipo fattura da usare nella generazione!", &
					  Exclamation!, ok!)
			dw_ext_sel_gen_fat_ven.setcolumn("cod_tipo_fat_ven")
			setpointer(arrow!)
			return -1
		end if
	else
		//tutto come prima
		select tab_tipi_bol_ven.cod_tipo_fat_ven,
				 tab_tipi_bol_ven.cod_tipo_analisi
		into   :ls_cod_tipo_fat_ven,
				 :ls_cod_tipo_analisi
		from   tab_tipi_bol_ven  
		where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tab_tipi_bol_ven.cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
		
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la lettura della tabella Tipi Bolle Vendita."
			return -1
		end if
	end if

	
	select tes_bol_ven.cod_operatore,   
			 tes_bol_ven.data_registrazione,   
			 tes_bol_ven.cod_cliente,   
			 tes_bol_ven.cod_des_cliente,   
			 tes_bol_ven.rag_soc_1,   
			 tes_bol_ven.rag_soc_2,   
			 tes_bol_ven.indirizzo,   
			 tes_bol_ven.localita,   
			 tes_bol_ven.frazione,   
			 tes_bol_ven.cap,   
			 tes_bol_ven.provincia,   
			 tes_bol_ven.cod_deposito,   
			 tes_bol_ven.cod_ubicazione,   
			 tes_bol_ven.cod_valuta,   
			 tes_bol_ven.cambio_ven,   
			 tes_bol_ven.cod_tipo_listino_prodotto,   
			 tes_bol_ven.cod_pagamento,   
			 tes_bol_ven.sconto,   
			 tes_bol_ven.cod_agente_1,
			 tes_bol_ven.cod_agente_2,
			 tes_bol_ven.cod_banca,   
			 tes_bol_ven.num_ord_cliente,   
			 tes_bol_ven.data_ord_cliente,   
			 tes_bol_ven.cod_imballo,   
			 tes_bol_ven.aspetto_beni,
			 tes_bol_ven.peso_netto,
			 tes_bol_ven.peso_lordo,
			 tes_bol_ven.num_colli,
			 tes_bol_ven.cod_vettore,   
			 tes_bol_ven.cod_inoltro,   
			 tes_bol_ven.cod_mezzo,
			 tes_bol_ven.causale_trasporto,
			 tes_bol_ven.cod_porto,   
			 tes_bol_ven.cod_resa,   
			 tes_bol_ven.nota_testata,   
			 tes_bol_ven.nota_piede,
			 tes_bol_ven.flag_fuori_fido,
			 tes_bol_ven.flag_movimenti,
			 tes_bol_ven.flag_riep_fat,
			 tes_bol_ven.cod_banca_clien_for,
			 tes_bol_ven.cod_documento,
			 tes_bol_ven.numeratore_documento,
			 tes_bol_ven.anno_documento,
			 tes_bol_ven.num_documento,
			 tes_bol_ven.data_bolla,
			 tes_bol_ven.cod_causale,
			 tes_bol_ven.flag_doc_suc_tes, 
			 tes_bol_ven.flag_doc_suc_pie,
			 tes_bol_ven.cod_natura_intra
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito,   
			 :ls_cod_ubicazione,
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ls_flag_fuori_fido,
			 :ls_flag_movimenti,
			 :ls_flag_riep_fat,
			 :ls_cod_banca_clien_for,
			 :ls_cod_documento,
			 :ls_numeratore_documento,
			 :ll_anno_documento,
			 :ll_num_documento,
			 :ldt_data_bolla,
			 :ls_cod_causale,
			 :ls_flag_doc_suc_tes, 
			 :ls_flag_doc_suc_pie,
			 :ls_cod_natura_intra
	from   tes_bol_ven  
	where  tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_bol_ven.anno_registrazione = :ll_anno_bolla and  
			 tes_bol_ven.num_registrazione = :ll_num_bolla;

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura Testata Bolla Vendita."
		destroy lds_rag_documenti
		return -1
	end if

	if ls_cod_natura_intra="" then setnull(ls_cod_natura_intra)

	select tab_tipi_fat_ven.cod_tipo_analisi,
			 tab_tipi_fat_ven.causale_trasporto,
			 tab_tipi_fat_ven.flag_abilita_rif_bol_ven,
			 tab_tipi_fat_ven.cod_tipo_det_ven_rif_bol_ven,
			 tab_tipi_fat_ven.des_riferimento_fat_ven,
			 tab_tipi_fat_ven.flag_rif_cod_documento,
			 tab_tipi_fat_ven.flag_rif_numeratore_doc,
			 tab_tipi_fat_ven.flag_rif_anno_documento,
			 tab_tipi_fat_ven.flag_rif_num_documento,
			 tab_tipi_fat_ven.flag_rif_data_fattura
	into	 :ls_cod_tipo_analisi,
			 :ls_causale_trasporto_tab,
			 :ls_flag_abilita_rif_bol_ven,
			 :ls_cod_tipo_det_ven_rif_bol_ven,
			 :ls_des_riferimento_fat_ven,
			 :ls_flag_rif_cod_documento,
			 :ls_flag_rif_numeratore_doc,
			 :ls_flag_rif_anno_documento,
			 :ls_flag_rif_num_documento,
			 :ls_flag_rif_data_fattura
	from   tab_tipi_fat_ven  
	where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Si è verificato un errore nella lettura della Tabella Tipi Fatture."
		destroy lds_rag_documenti
		return -1
	end if

	
 	if isnull(ls_nota_testata)  or len(trim(ls_nota_testata)) < 1 then
		ls_nota_testata = ""
	end if

	if isnull(ls_nota_piede) or len(trim(ls_nota_piede)) < 1 then
		ls_nota_piede = ""
	else
		ls_nota_piede = ls_nota_piede + "~r~n"
	end if
	
//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_bol_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_bol_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_bol_ven = :ls_cod_tipo_bol_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				

//--------------------------------------- Fine Modifica --------------------------------------------------------
	
	if not isnull(ls_num_ord_cliente) and ls_flag_abilita_rif_bol_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if

	if ls_flag_abilita_rif_bol_ven = "T" then 
		ls_nota_testata = "Rif.ns. D.D.T. Nr. " + string(ll_anno_documento) + "/" + &
								string(ll_num_documento) + " del " + string(ldt_data_bolla, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata + &
								"~r~n"
	end if

		
	// ----- controllo lunghezza note per ASE
	if (ls_db = "SYBASE_ASE" or ls_db = "MSSQL") and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE



	if ls_key_sort <> ls_key_sort_old then
		select con_fat_ven.num_registrazione
		into   :ll_num_registrazione
		from   con_fat_ven
		where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la lettura della tabella Controllo Fatture Vendita."
			destroy lds_rag_documenti
			return -1
		end if
	
		ll_num_registrazione ++
		
		ll_max_registrazione = 0
		
		select max(num_registrazione)
		into   :ll_max_registrazione
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :al_anno_registrazione;
		if not isnull(ll_max_registrazione) then
			if ll_max_registrazione >= ll_num_registrazione then
				ll_num_registrazione = ll_max_registrazione + 1
			end if
		end if
		
		update con_fat_ven
		set    con_fat_ven.num_registrazione = :ll_num_registrazione
		where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;
		
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la scrittura sulla tabella Controllo Fatture Vendita."
			destroy lds_rag_documenti
			return -1
		end if
	
		ldt_data_registrazione = datetime(today())
	
		// spostato da qui select tab_tipi_fat_ven   Enme 27/04/98
	
		if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
			ls_causale_trasporto = ls_causale_trasporto_tab
		end if
	
		select parametri_azienda.stringa
		into   :ls_lire

		from   parametri_azienda
		where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
				 parametri_azienda.flag_parametro = 'S' and &
				 parametri_azienda.cod_parametro = 'LIR';
	
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Configurare il codice valuta per le Lire Italiane."
			destroy lds_rag_documenti
			return -1
		end if
	
		select tab_pagamenti.sconto
		into   :ld_sconto_pagamento
		from   tab_pagamenti
		where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;
	
		if sqlca.sqlcode <> 0 then
			ld_sconto_pagamento = 0
		end if
	
//----------------------------------- Modifica Nicola ------------------------------------
		select cod_des_fattura
		  into :ls_cod_des_fattura_com
		  from tab_tipi_fat_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
			
		if sqlca.sqlcode <> 0 then	
			setnull(ls_rag_soc_1_dest_fat)
			setnull(ls_rag_soc_2_dest_fat)
			setnull(ls_indirizzo_dest_fat)   
			setnull(ls_localita_dest_fat)   
			setnull(ls_frazione_dest_fat)   
			setnull(ls_cap_dest_fat)   
			setnull(ls_provincia_dest_fat)
		else 
			select rag_soc_1,
					 rag_soc_2,
					 indirizzo,
					 localita,
					 frazione,
					 cap,
					 provincia
			  into :ls_rag_soc_1_dest_fat,
			  		 :ls_rag_soc_2_dest_fat,
					 :ls_indirizzo_dest_fat,
					 :ls_localita_dest_fat,
					 :ls_frazione_dest_fat,
					 :ls_cap_dest_fat,
					 :ls_provincia_dest_fat
			  from anag_des_clienti
			 where cod_azienda = :s_cs_xx.cod_azienda and
			       cod_cliente = :ls_cod_cliente[1] and
				    cod_des_cliente = :ls_cod_des_fattura_com and
					 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
				
			if sqlca.sqlcode <> 0 then
				setnull(ls_rag_soc_1_dest_fat)
				setnull(ls_rag_soc_2_dest_fat)
				setnull(ls_indirizzo_dest_fat)   
				setnull(ls_localita_dest_fat)   
				setnull(ls_frazione_dest_fat)   
				setnull(ls_cap_dest_fat)   
				setnull(ls_provincia_dest_fat)
			end if	
		end if		
		  
//------------------------------------ Fine modifica -------------------------------------
		
		//prima di fare la insert metti l'operatore collegato per il nuovo documento
		if f_get_operatore_doc(ls_cod_operatore_new, ls_messaggio)>0 then
			ls_cod_operatore = ls_cod_operatore_new
		end if
		
		insert into tes_fat_ven
						(cod_azienda,   
						 anno_registrazione,   
						 num_registrazione,   
						 data_registrazione,   
						 cod_operatore,   
						 cod_tipo_fat_ven,   
						 cod_cliente,
						 cod_des_cliente,   
						 rag_soc_1,   
						 rag_soc_2,   
						 indirizzo,   
						 localita,   
						 frazione,   
						 cap,   
						 provincia,   
						 rag_soc_1_dest_fat,   
						 rag_soc_2_dest_fat,   
						 indirizzo_dest_fat,   
						 localita_dest_fat,   
						 frazione_dest_fat,   
						 cap_dest_fat,   
						 provincia_dest_fat,   						 
						 cod_deposito,   
						 cod_ubicazione,
						 cod_valuta,   
						 cambio_ven,   
						 cod_tipo_listino_prodotto,   
						 cod_pagamento,   
						 sconto,   
						 cod_agente_1,
						 cod_agente_2,
						 cod_banca,   
						 num_ord_cliente,   
						 data_ord_cliente,   
						 cod_imballo,   
						 aspetto_beni,
						 peso_netto,
						 peso_lordo,
						 num_colli,
						 cod_vettore,   
						 cod_inoltro,   
						 cod_mezzo,
						 causale_trasporto,
						 cod_porto,   
						 cod_resa,   
						 cod_documento,
						 numeratore_documento,
						 anno_documento,
						 num_documento,
						 data_fattura,
						 nota_testata,   
						 nota_piede,   
						 flag_fuori_fido,
						 flag_blocco,   
						 flag_calcolo,
						 flag_movimenti,  
						 flag_contabilita,
						 tot_merci,
						 tot_spese_trasporto,
						 tot_spese_imballo,
						 tot_spese_bolli,
						 tot_spese_varie,
						 tot_sconto_cassa,
						 tot_sconti_commerciali,
						 imponibile_provvigioni_1,
						 imponibile_provvigioni_2,
						 tot_provvigioni_1,
						 tot_provvigioni_2,
						 importo_iva,
						 imponibile_iva,
						 importo_iva_valuta,
						 imponibile_iva_valuta,
						 tot_fattura,
						 tot_fattura_valuta,
						 flag_cambio_zero,
						 cod_banca_clien_for,
						 cod_causale,
						 flag_st_note_tes,					 
						 flag_st_note_pie,
						 cod_natura_intra)  
		values      (:s_cs_xx.cod_azienda,   
						 :al_anno_registrazione,   
						 :ll_num_registrazione,   
						 :ldt_data_registrazione,   
						 :ls_cod_operatore,   
						 :ls_cod_tipo_fat_ven,   
						 :ls_cod_cliente[1],
						 :ls_cod_des_cliente,   
						 :ls_rag_soc_1,   
						 :ls_rag_soc_2,   
						 :ls_indirizzo,   
						 :ls_localita,   
						 :ls_frazione,   
						 :ls_cap,   
						 :ls_provincia,   						 
						 :ls_rag_soc_1_dest_fat,   
						 :ls_rag_soc_2_dest_fat,   
						 :ls_indirizzo_dest_fat,   
						 :ls_localita_dest_fat,   
						 :ls_frazione_dest_fat,   
						 :ls_cap_dest_fat,   
						 :ls_provincia_dest_fat,   						 						 
						 :ls_cod_deposito,   
						 :ls_cod_ubicazione,
						 :ls_cod_valuta,   
						 :ld_cambio_ven,   
						 :ls_cod_tipo_listino_prodotto,   
						 :ls_cod_pagamento,   
						 :ld_sconto_testata,
						 :ls_cod_agente_1,
						 :ls_cod_agente_2,
						 :ls_cod_banca,   
						 :ls_num_ord_cliente,   
						 :ldt_data_ord_cliente,   
						 :ls_cod_imballo,   
						 :ls_aspetto_beni,
						 :ld_peso_netto,
						 :ld_peso_lordo,
						 :ld_num_colli,
						 :ls_cod_vettore,   
						 :ls_cod_inoltro,   
						 :ls_cod_mezzo,   
						 :ls_causale_trasporto,
						 :ls_cod_porto,   
						 :ls_cod_resa,   
						 null,
						 null,
						 0,
						 0,
						 null,
						 '',   
						 '',   
						 :ls_flag_fuori_fido,
						 'N',   
						 'N',
						 'S',
						 'N',
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 0,
						 'N',
						 :ls_cod_banca_clien_for,
						 :ls_cod_causale,
						 'I',
						 'I',
						 :ls_cod_natura_intra);						 
	
		if sqlca.sqlcode <> 0 then
			ls_messaggio = "Errore durante la scrittura Testata Fatture Vendita."
			destroy lds_rag_documenti
			return -1
		end if
	
		if wf_crea_nota_fissa(al_anno_registrazione, ll_num_registrazione, ls_messaggio) = -1 then return -1

		ll_prog_riga_fat_ven = 0
	end if

// ----------------------    insert righe     -------------------------------------------

 
	lds_det_bol_ven = Create DataStore
	lds_det_bol_ven.DataObject = "d_lista_det_bol_ven"
	lds_det_bol_ven.settransobject(sqlca)
	ll_rows = lds_det_bol_ven.retrieve(s_cs_xx.cod_azienda,&
												  ll_anno_bolla, &
												  ll_num_bolla)
	
	
	luo_genera_doc = create uo_generazione_documenti
	
	for ll_row = 1 to ll_rows
		if ll_row = 1 and ls_flag_abilita_rif_bol_ven = "D" then
			ll_anno_bol_ven = lds_det_bol_ven.getitemnumber(ll_row, "anno_registrazione")
			ll_num_bol_ven  = lds_det_bol_ven.getitemnumber(ll_row, "num_registrazione")
			ll_prog_riga_bol_ven = lds_det_bol_ven.getitemnumber(ll_row, "prog_riga_bol_ven")
			ls_cod_tipo_det_ven  = ls_cod_tipo_det_ven_rif_bol_ven

			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven, "C", ls_cod_cliente[1], ref ls_cod_iva) <> 0 then
				g_mb.messagebox("Generazione Fattura","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if

			setnull(ls_cod_deposito)
			setnull(ls_cod_ubicazione)
			setnull(ls_cod_lotto)
			setnull(ll_progr_stock)
			setnull(ldt_data_stock)
			setnull(ls_cod_misura)
			setnull(ls_cod_prodotto)

			ls_des_prodotto =  ls_des_riferimento_fat_ven
			if ls_flag_rif_cod_documento = "S" then
				ls_des_prodotto = ls_des_prodotto + "-" + ls_cod_documento
			end if
			if ls_flag_rif_numeratore_doc = "S" then
				ls_des_prodotto = ls_des_prodotto + ls_numeratore_documento
			end if
			if ls_flag_rif_anno_documento = "S" then
				ls_des_prodotto = ls_des_prodotto + "-" + string(ll_anno_documento)
			end if
			if ls_flag_rif_num_documento = "S" then
				ls_des_prodotto = ls_des_prodotto + "-" + string(ll_num_documento)
			end if
			if ls_flag_rif_data_fattura = "S" then
				ls_des_prodotto = ls_des_prodotto + "-" + string(ldt_data_bolla,"dd/mm/yyyy")
			end if
			
			ld_quan_consegnata = 0
			ld_prezzo_vendita  = 0
			ld_fat_conversione_ven = 1
			ll_sconto_1 = 0
			ll_sconto_2 = 0
			ll_sconto_3 = 0
			ll_sconto_4 = 0
			ll_sconto_5 = 0
			ll_sconto_6 = 0
			ll_sconto_7 = 0
			ll_sconto_8 = 0
			ll_sconto_9 = 0
			ll_sconto_10 = 0
			ll_provvigione_1 = 0
			ll_provvigione_2 = 0
			setnull(ls_cod_tipo_movimento)
			setnull(ll_anno_registrazione_mov_mag)
			setnull(ll_num_registrazione_mov_mag)
			setnull(ls_nota_dettaglio)
			setnull(ll_anno_reg_ord_ven)
			setnull(ll_num_reg_ord_ven)
			setnull(ll_prog_riga_ord_ven)
			setnull(ll_anno_commessa)
			setnull(ll_num_commessa)
			setnull(ls_cod_centro_costo)
			setnull(ls_cod_versione)
	
			ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga				
				
			insert into det_fat_ven(		cod_azienda, 
												anno_registrazione,
												num_registrazione,
												prog_riga_fat_ven,
												cod_tipo_det_ven,
												cod_deposito,
												cod_ubicazione,
												cod_lotto,
												data_stock,
												progr_stock,
												cod_misura,
												cod_prodotto,
												des_prodotto,
												quan_fatturata,
												prezzo_vendita,
												sconto_1,
												sconto_2,
												provvigione_1,
												provvigione_2,
												cod_iva,
												cod_tipo_movimento,
												num_registrazione_mov_mag,
												anno_registrazione_fat,
												num_registrazione_fat,
												nota_dettaglio,
												anno_registrazione_bol_ven,
												num_registrazione_bol_ven,
												prog_riga_bol_ven,
												anno_commessa,
												num_commessa,
												cod_centro_costo,
												sconto_3,
												sconto_4,
												sconto_5,
												sconto_6,
												sconto_7,
												sconto_8,
												sconto_9,
												sconto_10,
												anno_registrazione_mov_mag,
												fat_conversione_ven,
												anno_reg_des_mov,
												num_reg_des_mov,
												anno_reg_ord_ven,
												num_reg_ord_ven,
												prog_riga_ord_ven,	
												cod_versione,
												num_confezioni,
												num_pezzi_confezione,
												flag_st_note_det,
												quantita_um,
												prezzo_um)							
			values  (		:s_cs_xx.cod_azienda,
							:al_anno_registrazione,
							:ll_num_registrazione,
							:ll_prog_riga_fat_ven,
							:ls_cod_tipo_det_ven,
							:ls_cod_deposito,
							:ls_cod_ubicazione,
							:ls_cod_lotto,
							:ldt_data_stock,
							:ll_progr_stock,
							:ls_cod_misura,
							:ls_cod_prodotto,
							:ls_des_prodotto,
							:ld_quan_consegnata,
							:ld_prezzo_vendita,
							:ll_sconto_1,
							:ll_sconto_2,
							:ll_provvigione_1,
							:ll_provvigione_2,
							:ls_cod_iva,
							:ls_cod_tipo_movimento,
							:ll_num_registrazione_mov_mag,
							null,
							null,
							:ls_nota_dettaglio,
							:ll_anno_bol_ven,
							:ll_num_bol_ven,
							:ll_prog_riga_bol_ven,
							:ll_anno_commessa,
							:ll_num_commessa,
							:ls_cod_centro_costo,
							:ll_sconto_3,
							:ll_sconto_4,
							:ll_sconto_5,
							:ll_sconto_6,
							:ll_sconto_7,
							:ll_sconto_8,
							:ll_sconto_9,
							:ll_sconto_10,
							:ll_anno_registrazione_mov_mag,
							:ld_fat_conversione_ven,
							null,
							null,
							:ll_anno_reg_ord_ven,
							:ll_num_reg_ord_ven,
							:ll_prog_riga_ord_ven,
							:ls_cod_versione,
							0, 
							0,							
							'I',
							0,
							0);
							
			if sqlca.sqlcode = -1 then
				ls_messaggio = "Si è verificato un errore in fase di aggiornamento Dettaglio Fattura Vendita."
				destroy lds_rag_documenti
				destroy lds_det_bol_ven
				destroy luo_genera_doc
				return -1
			end if
			
		end if
		
		if ls_flag_raggruppo_doc_iva = 'S' and lds_det_bol_ven.getitemstring(ll_row, "cod_iva") <> "" and &
			not isnull(lds_det_bol_ven.getitemstring(ll_row, "cod_iva")) then
			ls_cod_tipo_det_ven_2 = lds_det_bol_ven.getitemstring(ll_row, "cod_tipo_det_ven")
			ls_cod_iva = lds_det_bol_ven.getitemstring(ll_row, "cod_iva")
			ls_cod_iva       =  lds_det_bol_ven.getitemstring(ll_row, "cod_iva")
			ld_prezzo_vendita = lds_det_bol_ven.getitemnumber(ll_row, "prezzo_vendita")
			ld_quan_consegnata = lds_det_bol_ven.getitemnumber(ll_row, "quan_consegnata")
			ll_sconto_1 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_1")
			ll_sconto_2 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_2")
			ll_sconto_3 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_3")
			ll_sconto_4 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_4")
			ll_sconto_5 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_5")
			ll_sconto_6 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_6")
			ll_sconto_7 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_7")
			ll_sconto_8 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_8")
			ll_sconto_9 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_9")
			ll_sconto_10 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_10")

			select tab_tipi_det_ven.flag_tipo_det_ven
			into   :ls_flag_tipo_det_ven
			from   tab_tipi_det_ven
			where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
					 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven_2;
		
			if sqlca.sqlcode = -1 then
				ls_messaggio = "Si è verificato un errore in ricerca tipi dettaglio vendita"
				destroy luo_genera_doc
				return -1
			end if

			if ls_flag_tipo_det_ven = "S" then //sconto
				ld_prezzo_vendita = ld_prezzo_vendita * (-1)
			end if


			ld_prezzo_vendita = ld_prezzo_vendita * ld_quan_consegnata
			if ll_sconto_1 > 0 and not isnull(ll_sconto_1) then
				ld_prezzo_vendita = ld_prezzo_vendita * (100 - ll_sconto_1) / 100
			end if
			if ll_sconto_2 > 0 and not isnull(ll_sconto_2) then
				ld_prezzo_vendita = ld_prezzo_vendita * (100 - ll_sconto_2) / 100
			end if
			if ll_sconto_3 > 0 and not isnull(ll_sconto_3) then
				ld_prezzo_vendita = ld_prezzo_vendita * (100 - ll_sconto_3) / 100
			end if
			if ll_sconto_4 > 0 and not isnull(ll_sconto_4) then
				ld_prezzo_vendita = ld_prezzo_vendita * (100 - ll_sconto_4) / 100
			end if
			if ll_sconto_5 > 0 and not isnull(ll_sconto_5) then
				ld_prezzo_vendita = ld_prezzo_vendita * (100 - ll_sconto_5) / 100
			end if
			if ll_sconto_6 > 0 and not isnull(ll_sconto_6) then
				ld_prezzo_vendita = ld_prezzo_vendita * (100 - ll_sconto_6) / 100
			end if
			if ll_sconto_7 > 0 and not isnull(ll_sconto_7) then
				ld_prezzo_vendita = ld_prezzo_vendita * (100 - ll_sconto_7) / 100
			end if
			if ll_sconto_8 > 0 and not isnull(ll_sconto_8) then
				ld_prezzo_vendita = ld_prezzo_vendita * (100 - ll_sconto_8) / 100
			end if
			if ll_sconto_9 > 0 and not isnull(ll_sconto_9) then
				ld_prezzo_vendita = ld_prezzo_vendita * (100 - ll_sconto_9) / 100
			end if
			if ll_sconto_10 > 0 and not isnull(ll_sconto_10) then
				ld_prezzo_vendita = ld_prezzo_vendita * (100 - ll_sconto_10) / 100
			end if

			ll_count = 1
			lb_nuova_aliquota = true
			do while ll_count <= upperbound(ls_cod_ive)
				if isnull(ls_cod_ive[ll_count]) then
					lb_nuova_aliquota = true
					exit
				end if
				if ls_cod_ive[ll_count] = ls_cod_iva then // IVA già inizializzata
					lb_nuova_aliquota = false
					exit
				end if
				lb_nuova_aliquota = true
				ll_count ++
			loop
			
			if lb_nuova_aliquota then
//			if ll_count > upperbound(ls_cod_ive) then // Nuova IVA
				ls_cod_ive[ll_count] = ls_cod_iva
				select des_iva
				  into :ls_des_prodotto_iva[ll_count]
				  from tab_ive
				 where cod_azienda = :s_cs_xx.cod_azienda and
				 		 cod_iva = :ls_cod_iva;

				if sqlca.sqlcode = -1 then
					ls_messaggio = "Si è verificato un errore in fase di Estrazione Tabella Iva."
					destroy lds_rag_documenti
					destroy lds_det_bol_ven
					destroy luo_genera_doc
					return -1
				end if
				ls_des_prodotto_iva[ll_count] = "Totale per Aliquota IVA: " + ls_des_prodotto_iva[ll_count]
				ld_prezzo_vendita_iva[ll_count] = 0
			end if
			
			ld_prezzo_vendita_iva[ll_count] = ld_prezzo_vendita_iva[ll_count] + ld_prezzo_vendita
			
//			ll_provvigione_1 = lds_det_bol_ven.getitemnumber(ll_row, "provvigione_1")
//			ll_provvigione_2 = lds_det_bol_ven.getitemnumber(ll_row, "provvigione_2")

		else
			ll_anno_bol_ven = lds_det_bol_ven.getitemnumber(ll_row, "anno_registrazione")
			ll_num_bol_ven  = lds_det_bol_ven.getitemnumber(ll_row, "num_registrazione")
			ll_prog_riga_bol_ven = lds_det_bol_ven.getitemnumber(ll_row, "prog_riga_bol_ven")
			ls_cod_tipo_det_ven  = lds_det_bol_ven.getitemstring(ll_row, "cod_tipo_det_ven")
			ls_cod_deposito   = lds_det_bol_ven.getitemstring(ll_row, "cod_deposito") 
			ls_cod_ubicazione =  lds_det_bol_ven.getitemstring(ll_row, "cod_ubicazione")
			ls_cod_lotto    = lds_det_bol_ven.getitemstring(ll_row, "cod_lotto")
			ll_progr_stock  = lds_det_bol_ven.getitemnumber(ll_row, "progr_stock")
			ldt_data_stock  = lds_det_bol_ven.getitemdatetime(ll_row, "data_stock")
			ls_cod_misura   =  lds_det_bol_ven.getitemstring(ll_row, "cod_misura")
			ls_cod_prodotto =  lds_det_bol_ven.getitemstring(ll_row, "cod_prodotto")
			ls_des_prodotto =  lds_det_bol_ven.getitemstring(ll_row, "des_prodotto")
			ld_quan_consegnata = lds_det_bol_ven.getitemnumber(ll_row, "quan_consegnata")
			ld_prezzo_vendita  = lds_det_bol_ven.getitemnumber(ll_row, "prezzo_vendita")
			ld_fat_conversione_ven = lds_det_bol_ven.getitemnumber(ll_row, "fat_conversione_ven")
			ll_sconto_1 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_1")
			ll_sconto_2 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_2")
			ll_sconto_3 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_3")
			ll_sconto_4 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_4")
			ll_sconto_5 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_5")
			ll_sconto_6 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_6")
			ll_sconto_7 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_7")
			ll_sconto_8 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_8")
			ll_sconto_9 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_9")
			ll_sconto_10 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_10")
			ll_provvigione_1 = lds_det_bol_ven.getitemnumber(ll_row, "provvigione_1")
			ll_provvigione_2 = lds_det_bol_ven.getitemnumber(ll_row, "provvigione_2")
			ls_cod_iva       =  lds_det_bol_ven.getitemstring(ll_row, "cod_iva")
			ls_cod_tipo_movimento         =  lds_det_bol_ven.getitemstring(ll_row, "cod_tipo_movimento")
			ll_anno_registrazione_mov_mag = lds_det_bol_ven.getitemnumber(ll_row, "anno_registrazione_mov_mag")
			ll_num_registrazione_mov_mag  = lds_det_bol_ven.getitemnumber(ll_row, "num_registrazione_mov_mag")
			ls_nota_dettaglio    = lds_det_bol_ven.getitemstring(ll_row, "nota_dettaglio")
			ll_anno_reg_ord_ven  = lds_det_bol_ven.getitemnumber(ll_row, "anno_registrazione_ord_ven")
			ll_num_reg_ord_ven   = lds_det_bol_ven.getitemnumber(ll_row, "num_registrazione_ord_ven")
			ll_prog_riga_ord_ven = lds_det_bol_ven.getitemnumber(ll_row, "prog_riga_ord_ven")
			ll_anno_commessa     = lds_det_bol_ven.getitemnumber(ll_row, "anno_commessa")
			ll_num_commessa      = lds_det_bol_ven.getitemnumber(ll_row, "num_commessa")
			ls_cod_centro_costo  =  lds_det_bol_ven.getitemstring(ll_row, "cod_centro_costo")
			ls_cod_versione  =  lds_det_bol_ven.getitemstring(ll_row, "cod_versione")
			ll_num_confezioni  =  lds_det_bol_ven.getitemnumber(ll_row, "num_confezioni")
			ll_num_pezzi_confezione  =  lds_det_bol_ven.getitemnumber(ll_row, "num_pezzi_confezione")
			ls_flag_doc_suc_det  =  lds_det_bol_ven.getitemstring(ll_row, "flag_doc_suc_det")			
			ld_quantita_um  =  lds_det_bol_ven.getitemnumber(ll_row, "quantita_um")			
			ld_prezzo_um  =  lds_det_bol_ven.getitemnumber(ll_row, "prezzo_um")			
	
	
//------------------------------------------------- Modifica Nicola ---------------------------------------------
			if ls_flag_doc_suc_det = 'I' then //nota dettaglio
				select flag_doc_suc_bl
				  into :ls_flag_doc_suc_det
				  from tab_tipi_det_ven
				 where cod_azienda = :s_cs_xx.cod_azienda
					and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
			end if		
			
			
			if ls_flag_doc_suc_det = 'N' then
				ls_nota_dettaglio = ""
			end if				
//-------------------------------------------------- Fine Modifica ----------------------------------------------	
	
			ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga
				
			insert into det_fat_ven
						  (cod_azienda, 
							anno_registrazione,
							num_registrazione,
							prog_riga_fat_ven,
							cod_tipo_det_ven,
							cod_deposito,
							cod_ubicazione,
							cod_lotto,
							data_stock,
							progr_stock,
							cod_misura,
							cod_prodotto,
							des_prodotto,
							quan_fatturata,
							prezzo_vendita,
							sconto_1,
							sconto_2,
							provvigione_1,
							provvigione_2,
							cod_iva,
							cod_tipo_movimento,
							num_registrazione_mov_mag,
							anno_registrazione_fat,
							num_registrazione_fat,
							nota_dettaglio,
							anno_registrazione_bol_ven,
							num_registrazione_bol_ven,
							prog_riga_bol_ven,
							anno_commessa,
							num_commessa,
							cod_centro_costo,
							sconto_3,
							sconto_4,
							sconto_5,
							sconto_6,
							sconto_7,
							sconto_8,
							sconto_9,
							sconto_10,
							anno_registrazione_mov_mag,
							fat_conversione_ven,
							anno_reg_des_mov,
							num_reg_des_mov,
							anno_reg_ord_ven,
							num_reg_ord_ven,
							prog_riga_ord_ven,	
							cod_versione,
							num_confezioni,
							num_pezzi_confezione,
							flag_st_note_det,
							quantita_um,
							prezzo_um)							
				values  (:s_cs_xx.cod_azienda,
							:al_anno_registrazione,
							:ll_num_registrazione,
							:ll_prog_riga_fat_ven,
							:ls_cod_tipo_det_ven,
							:ls_cod_deposito,
							:ls_cod_ubicazione,
							:ls_cod_lotto,
							:ldt_data_stock,
							:ll_progr_stock,
							:ls_cod_misura,
							:ls_cod_prodotto,
							:ls_des_prodotto,
							:ld_quan_consegnata,
							:ld_prezzo_vendita,
							:ll_sconto_1,
							:ll_sconto_2,
							:ll_provvigione_1,
							:ll_provvigione_2,
							:ls_cod_iva,
							:ls_cod_tipo_movimento,
							:ll_num_registrazione_mov_mag,
							null,
							null,
							:ls_nota_dettaglio,
							:ll_anno_bol_ven,
							:ll_num_bol_ven,
							:ll_prog_riga_bol_ven,
							:ll_anno_commessa,
							:ll_num_commessa,
							:ls_cod_centro_costo,
							:ll_sconto_3,
							:ll_sconto_4,
							:ll_sconto_5,
							:ll_sconto_6,
							:ll_sconto_7,
							:ll_sconto_8,
							:ll_sconto_9,
							:ll_sconto_10,
							:ll_anno_registrazione_mov_mag,
							:ld_fat_conversione_ven,
							null,
							null,
							:ll_anno_reg_ord_ven,
							:ll_num_reg_ord_ven,
							:ll_prog_riga_ord_ven,
							:ls_cod_versione,
							:ll_num_confezioni,
							:ll_num_pezzi_confezione,
							'I',
							:ld_quantita_um,
							:ld_prezzo_um);							
			if sqlca.sqlcode = -1 then
					ls_messaggio = "Si è verificato un errore in fase di aggiornamento Dettaglio Fattura Vendita."
					destroy lds_rag_documenti
					destroy lds_det_bol_ven
					destroy luo_genera_doc
					return -1
			end if
			
			
			//--------------------------------------------------------------------
			//SR Note_conformita_prodotto
			if luo_genera_doc.uof_note_in_fattura(al_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven, ls_messaggio)<0 then
				destroy lds_det_bol_ven
				destroy luo_genera_doc
				return -1
			end if
			//--------------------------------------------------------------------
			
		end if
	next
	
	destroy lds_det_bol_ven
	destroy luo_genera_doc
// ---------------------- fine insert righe  ---------------------------------------------


	if (string(ll_num_bolla) + ls_key_sort) <> (string(ll_num_bolla_old) + ls_key_sort_old) then
		
		//  gestione raggruppo aliquote iva
		
		
		// modifiche michela 02/04/2003 --> la select va fatta confrontando anche il codice dell'azienda. Se esistono 
		// più aziende dà errore!! sia nel param DVR che DV2
		
		if ls_flag_raggruppo_doc_iva = 'S' and upperbound(ls_cod_ive) > 0 then
			select stringa
			 into :ls_cod_tipo_det_ven_positivo
			 from parametri_azienda
			where flag_parametro = 'S'  and
					cod_parametro = 'DVR' and
					cod_azienda = :s_cs_xx.cod_azienda;
		
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Errore Estrazione Parametro DVR. Dettaglio: " + sqlca.sqlerrtext
				destroy lds_rag_documenti
				return -1
			end if
		// per aliquote negative	------ inizio
			select stringa
			 into :ls_cod_tipo_det_ven_negativo
			 from parametri_azienda
			where flag_parametro = 'S'  and
					cod_parametro = 'DV2' and
					cod_azienda = :s_cs_xx.cod_azienda;
		
			if sqlca.sqlcode <> 0 then
				ls_messaggio = "Errore Estrazione Parametro DV2. Dettaglio: " + sqlca.sqlerrtext
				destroy lds_rag_documenti
				return -1
			end if
		// per aliquote negative	------ fine
			
		
			ld_quan_consegnata = 1
			ld_fat_conversione_ven = 1

			ll_count = 1
			do while not isnull(ls_cod_ive[ll_count])
				if isnull(ll_prog_riga_fat_ven) then ll_prog_riga_fat_ven = 0
				ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga
		// per aliquote negative	------ inizio
				if ld_prezzo_vendita_iva[ll_count] >= 0 then
					ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_positivo
				else
					ls_cod_tipo_det_ven = ls_cod_tipo_det_ven_negativo
					ld_prezzo_vendita_iva[ll_count] = ld_prezzo_vendita_iva[ll_count] * (-1)
				end if
		// per aliquote negative	------ fine
				insert into det_fat_ven
							  (cod_azienda, 
								anno_registrazione,
								num_registrazione,
								prog_riga_fat_ven,
								cod_tipo_det_ven,
								cod_deposito,
								cod_ubicazione,
								cod_lotto,
								data_stock,
								progr_stock,
								cod_misura,
								cod_prodotto,
								des_prodotto,
								quan_fatturata,
								prezzo_vendita,
								sconto_1,
								sconto_2,
								provvigione_1,
								provvigione_2,
								cod_iva,
								cod_tipo_movimento,
								num_registrazione_mov_mag,
								anno_registrazione_fat,
								num_registrazione_fat,
								nota_dettaglio,
								anno_registrazione_bol_ven,
								num_registrazione_bol_ven,
								prog_riga_bol_ven,
								anno_commessa,
								num_commessa,
								cod_centro_costo,
								sconto_3,
								sconto_4,
								sconto_5,
								sconto_6,
								sconto_7,
								sconto_8,
								sconto_9,
								sconto_10,
								anno_registrazione_mov_mag,
								fat_conversione_ven,
								anno_reg_des_mov,
								num_reg_des_mov,
								anno_reg_ord_ven,
								num_reg_ord_ven,
								prog_riga_ord_ven,	
								cod_versione,
								num_confezioni,
								num_pezzi_confezione,
								flag_st_note_det,
								quantita_um,
								prezzo_um)								
					values  (:s_cs_xx.cod_azienda,
								:al_anno_registrazione,
								:ll_num_registrazione,
								:ll_prog_riga_fat_ven,
								:ls_cod_tipo_det_ven,
								null,
								null,
								null,
								null,
								null,
								:ls_cod_misura,
								null,
								:ls_des_prodotto_iva[ll_count],
								:ld_quan_consegnata,
								:ld_prezzo_vendita_iva[ll_count],
								0,
								0,
								0,
								0,
								:ls_cod_ive[ll_count],
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								null,
								0,
								0,
								0,
								0,
								0,
								0,
								0,
								0,
								null,
								:ld_fat_conversione_ven,
								null,
								null,
								null,
								null,
								null,
								null,
								0,
								0,
							   'I',
								0,
								0);								
				if sqlca.sqlcode = -1 then
					g_mb.messagebox(string(sqlca.sqlcode), sqlca.sqlerrtext)
					ls_messaggio = "Si è verificato un errore in fase di aggiornamento Dettaglio Fattura Vendita."
					return -1
				end if
				ll_count = ll_count + 1
				if upperbound(ls_cod_ive) < ll_count then exit
			loop
			
			for ll_iva = 1 to upperbound(ls_des_prodotto_iva)	
				setnull(ls_des_prodotto_iva[ll_iva])
			next
			for ll_iva = 1 to upperbound(ld_prezzo_vendita_iva)	
				ld_prezzo_vendita_iva[ll_iva] = 0
			next
			for ll_iva = 1 to upperbound(ls_cod_ive)	
				setnull(ls_cod_ive[ll_iva])
			next
			ll_count = 0
		end if
		
		select nota_testata, nota_piede
		into   :ls_nota_testata_old, :ls_nota_piede_old
		from   tes_fat_ven
		where  tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tes_fat_ven.anno_registrazione = :al_anno_registrazione and  
				 tes_fat_ven.num_registrazione = :ll_num_registrazione;
		if sqlca.sqlcode < 0 then
			ls_messaggio = "Si è verificato un errore in fase di ricerca note della Testata Fattura Vendita."
			destroy lds_rag_documenti
			return -1
		end if

		if isnull(ls_nota_testata) or len(trim(ls_nota_testata)) < 1 then
			ls_nota_testata = ls_nota_testata_old
		else
			if not isnull(ls_nota_testata_old) and len(ls_nota_testata_old) > 0 then
				ls_nota_testata = ls_nota_testata_old + "~r~n" + ls_nota_testata
			end if
		end if
		
		
		if isnull(ls_nota_piede) or len(trim(ls_nota_piede)) < 1 then
			ls_nota_piede = ls_nota_piede_old
		else
			if not isnull(ls_nota_piede_old) and len(ls_nota_piede_old) > 0 then
				ls_nota_piede = ls_nota_piede_old + "~r~n" + ls_nota_piede
			end if
		end if
			
		// ----- controllo lunghezza note per ASE
		if (ls_db = "SYBASE_ASE" or ls_db = "MSSQL") and len(ls_nota_testata) > 255 then
			ls_nota_testata = left(ls_nota_testata, 255)
		end if
		if (ls_db = "SYBASE_ASE" or ls_db = "MSSQL") and len(ls_nota_piede) > 255 then
			ls_nota_piede = left(ls_nota_piede, 255)
		end if
		// -----  fine controllo lunghezza note per ASE
	
		update tes_fat_ven  
			set nota_testata = :ls_nota_testata,
				 nota_piede = :ls_nota_piede
		 where tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				 tes_fat_ven.anno_registrazione = :al_anno_registrazione and  
				 tes_fat_ven.num_registrazione = :ll_num_registrazione;
	
		if sqlca.sqlcode = -1 then
			ls_messaggio = g_str.format("Cliente $4, DDT in elaborazione $5/$6 - Fattura $1/$2.Si è verificato un errore in fase di aggiornamento Testata Fattura Vendita $3",al_anno_registrazione,ll_num_registrazione,sqlca.sqlerrtext, ls_cod_cliente[1], ll_anno_bolla, ll_num_bolla)
			destroy lds_rag_documenti
			return -1
		end if
		
		luo_calcolo = create uo_calcola_documento_euro

		if luo_calcolo.uof_calcola_documento(al_anno_registrazione,ll_num_registrazione,"fat_ven",ls_messaggio) <> 0 then
			ls_messaggio = g_str.format("Cliente $4, DDT in elaborazione $5/$6 - Fattura $1/$2. $3",al_anno_registrazione,ll_num_registrazione,ls_messaggio,ls_cod_cliente[1], ll_anno_bolla, ll_num_bolla)
			destroy luo_calcolo
			return -1
		end if
		
		destroy luo_calcolo
		
		// Cerco il max riga perchè potrebbero essere stata aggiunta qualche riga automatica.
		
		select max(prog_riga_fat_ven)
		into   :ll_prog_riga_fat_ven
		from   det_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :al_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
		
		if isnull(ll_prog_riga_fat_ven) then ll_prog_riga_fat_ven = 0
		
		li_riga_orig = dw_gen_orig_dest.insertrow(0)
		dw_gen_orig_dest.setrow(dw_gen_orig_dest.rowcount())
		select cod_documento,
		       numeratore_documento,
				 anno_documento,
				 num_documento,
				 data_bolla
		into   :ls_cod_doc_orig,
		       :ls_numeratore_orig,
				 :ll_anno_orig,
				 :ll_num_orig,
				 :ldt_data_orig
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_bolla and
				 num_registrazione = :ll_num_bolla;
		if sqlca.sqlcode = 0 then
			dw_gen_orig_dest.setitem(li_riga_orig, "cod_documento", ls_cod_doc_orig)
			dw_gen_orig_dest.setitem(li_riga_orig, "numeratore", ls_numeratore_orig)
			dw_gen_orig_dest.setitem(li_riga_orig, "anno_documento", ll_anno_orig)
			dw_gen_orig_dest.setitem(li_riga_orig, "num_documento", ll_num_orig)
			dw_gen_orig_dest.setitem(li_riga_orig, "data_documento", ldt_data_orig)
		end if
		dw_gen_orig_dest.setitem(li_riga_orig, "anno_registrazione_orig", ll_anno_bolla)
		dw_gen_orig_dest.setitem(li_riga_orig, "num_registrazione_orig", ll_num_bolla)
		dw_gen_orig_dest.setitem(li_riga_orig, "anno_registrazione_dest", al_anno_registrazione)
		dw_gen_orig_dest.setitem(li_riga_orig, "num_registrazione_dest", ll_num_registrazione)
		f_scrivi_log("GEN.FATTURE: la bolla "+ + string(ll_anno_bolla) + "/" + string(ll_num_bolla)+" ha generato la fattura " + string(al_anno_registrazione) + "/" + string(ll_num_registrazione))
	end if
	
	update tes_bol_ven
	set    flag_gen_fat = 'S'
	where  tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and
	       tes_bol_ven.anno_registrazione = :ll_anno_bolla and
			 tes_bol_ven.num_registrazione = :ll_num_bolla ;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Generazione riepilogativa Fatture","Errore durante aggiornamento segnale di ~r~n" + &
		                                 "BOLLA FATTURATA in testata bolle vendita", information!)
	end if
	
	ls_key_sort_old = ls_key_sort
	ll_num_bolla_old = ll_num_bolla
next

destroy lds_rag_documenti


return 0
end function

public function integer wf_genera_fatture_singole_1 (long al_anno_registrazione, string as_cod_tipo_bol_ven, long al_anno_bolla, long al_num_bolla, ref string ls_messaggio);string								ls_cod_tipo_fat_ven, ls_nota_testata, ls_cod_cliente[1], ls_flag_riep_fat, &
									ls_cod_operatore, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_localita, ls_frazione, &
									ls_cap, ls_provincia, ls_cod_des_cliente, ls_cod_ubicazione, ls_cod_deposito, &
									ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, ls_cod_banca_clien_for, &
									ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, ls_cod_porto, &
									ls_cod_resa, ls_nota_piede, ls_cod_banca, ls_cod_tipo_det_ven, ls_cod_prodotto, &
									ls_cod_misura, ls_des_prodotto, ls_cod_iva, ls_nota_dettaglio, ls_cod_centro_costo, &
									ls_flag_tipo_det_ven, ls_cod_tipo_analisi_fat, ls_cod_tipo_analisi_ord, &
									ls_cod_agente_1, ls_cod_agente_2, ls_aspetto_beni, ls_causale_trasporto, &
									ls_tabella_orig, ls_tabella_des, ls_prog_riga_doc_orig, ls_prog_riga_doc_des, &
									ls_num_ord_cliente, ls_cod_lotto, ls_cod_tipo_movimento, ls_vostro_ordine, &
									ls_vostro_ordine_data, ls_flag_evasione, ls_lire, ls_flag_sola_iva, ls_cod_versione, &
									ls_cod_fornitore[], ls_causale_trasporto_tab, ls_cod_documento, ls_numeratore_documento, &
									ls_cod_tipo_analisi, ls_flag_abilita_rif_bol_ven, ls_cod_tipo_det_ven_rif_bol_ven, &
									ls_des_riferimento_fat_ven, ls_flag_rif_cod_documento, ls_flag_rif_numeratore_doc, &
									ls_flag_rif_anno_documento, ls_flag_rif_num_documento, ls_flag_rif_data_fattura, &
									ls_cod_causale, ls_cod_causale_tab, ls_db, ls_cod_des_fattura_com, &
									ls_rag_soc_1_dest_fat, ls_rag_soc_2_dest_fat, ls_indirizzo_dest_fat, ls_localita_dest_fat, &
									ls_frazione_dest_fat, ls_cap_dest_fat, ls_provincia_dest_fat, ls_cod_doc_orig,ls_numeratore_orig,&
									ls_flag_doc_suc_tes, ls_flag_doc_suc_pie, ls_flag_doc_suc_det, ls_cod_natura_intra
									
integer							li_risposta, li_riga_orig

long								ll_num_registrazione, ll_rows, ll_row, ll_anno_bol_ven, ll_anno_documento, ll_num_bol_ven, ll_prog_riga_bol_ven, &
									ll_num_documento, ll_anno_registrazione_mov_mag, ll_num_registrazione_mov_mag, ll_anno_reg_ord_ven, ll_num_reg_ord_ven, &
									ll_prog_riga_ord_ven, ll_anno_commessa, ll_num_commessa, ll_progr_stock, ll_anno_reg_des_mov, ll_num_reg_des_mov, &
									ll_prog_riga, ll_prog_riga_fat_ven,ll_anno_orig,ll_num_orig, ll_max_registrazione
									
double							ld_cambio_ven, ld_quan_in_evasione, ld_prezzo_vendita, ld_fat_conversione_ven, ld_sconto_1, ld_sconto_2, ld_sconto_3, &
									ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7, ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_testata, ld_peso_netto, &
									ld_peso_lordo, ld_num_colli, ld_provvigione_1, ld_provvigione_2, ld_quan_ordine, ld_quan_evasa, ld_tot_val_evaso, &
									ld_tot_val_ordine, ld_sconto_pagamento, ld_val_riga, ld_val_sconto_1, ld_val_riga_sconto_1, ld_val_sconto_2, &
									ld_val_riga_sconto_2, ld_val_sconto_3, ld_val_riga_sconto_3, ld_val_sconto_4, ld_val_riga_sconto_4, ld_val_sconto_5, &
									ld_val_riga_sconto_5, ld_val_sconto_6, ld_val_riga_sconto_6, ld_val_sconto_7, ld_val_riga_sconto_7, ld_val_sconto_8, &
									ld_val_riga_sconto_8, ld_val_sconto_9, ld_val_riga_sconto_9, ld_val_sconto_10, ld_val_riga_sconto_10, ld_val_sconto_testata, &
									ld_val_riga_sconto_testata, ld_val_sconto_pagamento, ld_val_evaso, ld_quan_consegnata, ll_sconto_1, ll_sconto_2, ll_sconto_3, &
									ll_sconto_4, ll_sconto_5, ll_sconto_6, ll_sconto_7, ll_sconto_8, ll_sconto_9, ll_sconto_10, ll_provvigione_1, ll_provvigione_2, &
									ll_num_confezioni, ll_num_pezzi_confezione, ld_quantita_um, ld_prezzo_um
									
datetime							ldt_data_registrazione, ldt_data_ordine, ldt_data_ord_cliente, ldt_data_stock, &
									ldt_vostro_ordine_data, ldt_data_bolla,ldt_data_orig, ldt_oggi
									
datastore						lds_det_bol_ven

string								ls_flag_forza_tipo_fattura, ls_cod_operatore_new

uo_calcola_documento_euro		luo_calcolo
uo_generazione_documenti			luo_genera_doc


ldt_oggi = datetime(today(),00:00:00)
li_risposta = Registryget(s_cs_xx.chiave_root + "database_" + s_cs_xx.profilocorrente, "enginetype", ls_db)

guo_functions.uof_get_parametro_azienda( "IRF", ref il_incremento_riga)
if isnull(il_incremento_riga) then il_incremento_riga = 10

setnull(ls_cod_fornitore[1])

//Donato 03/04/2009 
/*	Il tipo di fattura da generare dipende dal flag forza tipo fattura
	e dal valore del tipo fattura da usare. Se il falg di forzatura non viene specificato allora "comanda"
	il tipo fattura associato al tipo ddt della bolla da fatturare (cioè tutto come prima)
*/
ls_flag_forza_tipo_fattura = dw_ext_sel_gen_fat_ven.getitemstring(dw_ext_sel_gen_fat_ven.getrow(), 	"flag_forza_tipo")
if ls_flag_forza_tipo_fattura = "S" then
	ls_cod_tipo_fat_ven = dw_ext_sel_gen_fat_ven.getitemstring(dw_ext_sel_gen_fat_ven.getrow(), "cod_tipo_fat_ven")
	if isnull(ls_cod_tipo_fat_ven) or ls_cod_tipo_fat_ven = "" then
		g_mb.messagebox("Attenzione", "E' necessario specificare il tipo fattura da usare nella generazione!", Exclamation!, ok!)
		dw_ext_sel_gen_fat_ven.setcolumn("cod_tipo_fat_ven")
		setpointer(arrow!)
		return -1
	end if
else
	//tutto come prima
	select tab_tipi_bol_ven.cod_tipo_fat_ven,  
			 tab_tipi_bol_ven.cod_tipo_analisi  
	into   :ls_cod_tipo_fat_ven,  
			 :ls_cod_tipo_analisi_ord
	from   tab_tipi_bol_ven  
	where  tab_tipi_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_bol_ven.cod_tipo_bol_ven = :as_cod_tipo_bol_ven;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura della tabella Tipi Fatture Vendita."
		return -1
	end if
end if
//fine modifica ------------------------------------------------------------------------------------------------------

if not isnull(ls_cod_tipo_fat_ven) then
	select con_fat_ven.num_registrazione
	into   :ll_num_registrazione
	from   con_fat_ven
	where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura della tabella Controllo Fatture Vendita."
		return -1
	end if

	ll_num_registrazione ++
	
	ll_max_registrazione = 0
		
	select max(num_registrazione)
	into   :ll_max_registrazione
	from   tes_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :al_anno_registrazione;
	if not isnull(ll_max_registrazione) then
		if ll_max_registrazione >= ll_num_registrazione then
			ll_num_registrazione = ll_max_registrazione + 1
		end if
	end if
	
	update con_fat_ven
	set    con_fat_ven.num_registrazione = :ll_num_registrazione
	where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la scrittura sulla tabella Controllo Fatture Vendita."
		return -1
	end if

	select tes_bol_ven.cod_operatore,   
			 tes_bol_ven.data_registrazione,   
			 tes_bol_ven.cod_cliente,   
			 tes_bol_ven.cod_des_cliente,   
			 tes_bol_ven.rag_soc_1,   
			 tes_bol_ven.rag_soc_2,   
			 tes_bol_ven.indirizzo,   
			 tes_bol_ven.localita,   
			 tes_bol_ven.frazione,   
			 tes_bol_ven.cap,   
			 tes_bol_ven.provincia,   
			 tes_bol_ven.cod_deposito,   
			 tes_bol_ven.cod_ubicazione,   
			 tes_bol_ven.cod_valuta,   
			 tes_bol_ven.cambio_ven,   
			 tes_bol_ven.cod_tipo_listino_prodotto,   
			 tes_bol_ven.cod_pagamento,   
			 tes_bol_ven.sconto,   
			 tes_bol_ven.cod_agente_1,
			 tes_bol_ven.cod_agente_2,
			 tes_bol_ven.cod_banca,   
			 tes_bol_ven.num_ord_cliente,   
			 tes_bol_ven.data_ord_cliente,   
			 tes_bol_ven.cod_imballo,   
			 tes_bol_ven.aspetto_beni,
			 tes_bol_ven.peso_netto,
			 tes_bol_ven.peso_lordo,
			 tes_bol_ven.num_colli,
			 tes_bol_ven.cod_vettore,   
			 tes_bol_ven.cod_inoltro,   
			 tes_bol_ven.cod_mezzo,
			 tes_bol_ven.causale_trasporto,
			 tes_bol_ven.cod_porto,   
			 tes_bol_ven.cod_resa,   
			 tes_bol_ven.nota_testata,   
			 tes_bol_ven.nota_piede,   
			 tes_bol_ven.flag_riep_fat,
			 tes_bol_ven.cod_banca_clien_for,
			 tes_bol_ven.cod_documento,
			 tes_bol_ven.numeratore_documento,
			 tes_bol_ven.anno_documento,
			 tes_bol_ven.num_documento,
			 tes_bol_ven.data_bolla,
			 tes_bol_ven.cod_causale,
			 tes_bol_ven.flag_doc_suc_tes, 
			 tes_bol_ven.flag_doc_suc_pie,
			 tes_bol_ven.cod_natura_intra
	 into  :ls_cod_operatore,   
			 :ldt_data_ordine,   
			 :ls_cod_cliente[1],   
			 :ls_cod_des_cliente,   
			 :ls_rag_soc_1,   
			 :ls_rag_soc_2,   
			 :ls_indirizzo,   
			 :ls_localita,   
			 :ls_frazione,   
			 :ls_cap,   
			 :ls_provincia,   
			 :ls_cod_deposito,   
			 :ls_cod_ubicazione,
			 :ls_cod_valuta,   
			 :ld_cambio_ven,   
			 :ls_cod_tipo_listino_prodotto,   
			 :ls_cod_pagamento,   
			 :ld_sconto_testata,   
			 :ls_cod_agente_1,
			 :ls_cod_agente_2,
			 :ls_cod_banca,   
			 :ls_num_ord_cliente,   
			 :ldt_data_ord_cliente,   
			 :ls_cod_imballo,   
			 :ls_aspetto_beni,
			 :ld_peso_netto,
			 :ld_peso_lordo,
			 :ld_num_colli,
			 :ls_cod_vettore,   
			 :ls_cod_inoltro,   
			 :ls_cod_mezzo,   
			 :ls_causale_trasporto,
			 :ls_cod_porto,
			 :ls_cod_resa,   
			 :ls_nota_testata,   
			 :ls_nota_piede,   
			 :ls_flag_riep_fat,
			 :ls_cod_banca_clien_for,
			 :ls_cod_documento,
			 :ls_numeratore_documento,
			 :ll_anno_documento,
			 :ll_num_documento,
			 :ldt_data_bolla,
			 :ls_cod_causale,
			 :ls_flag_doc_suc_tes, 
			 :ls_flag_doc_suc_pie,
			 :ls_cod_natura_intra
	from   tes_bol_ven  
	where  tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tes_bol_ven.anno_registrazione = :al_anno_bolla and  
			 tes_bol_ven.num_registrazione = :al_num_bolla;

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Errore durante la lettura Testata Bolle Vendita."
		return -1
	end if

	ldt_data_registrazione = datetime(today())

	select tab_tipi_fat_ven.cod_tipo_analisi,
			 tab_tipi_fat_ven.causale_trasporto,
			 tab_tipi_fat_ven.flag_abilita_rif_bol_ven,
			 tab_tipi_fat_ven.cod_tipo_det_ven_rif_bol_ven,
			 tab_tipi_fat_ven.des_riferimento_fat_ven,
			 tab_tipi_fat_ven.flag_rif_cod_documento,
			 tab_tipi_fat_ven.flag_rif_numeratore_doc,
			 tab_tipi_fat_ven.flag_rif_anno_documento,
			 tab_tipi_fat_ven.flag_rif_num_documento,
			 tab_tipi_fat_ven.flag_rif_data_fattura,
			 tab_tipi_fat_ven.cod_causale
	into	 :ls_cod_tipo_analisi,
			 :ls_causale_trasporto_tab,
			 :ls_flag_abilita_rif_bol_ven,
			 :ls_cod_tipo_det_ven_rif_bol_ven,
			 :ls_des_riferimento_fat_ven,
			 :ls_flag_rif_cod_documento,
			 :ls_flag_rif_numeratore_doc,
			 :ls_flag_rif_anno_documento,
			 :ls_flag_rif_num_documento,
			 :ls_flag_rif_data_fattura,
			 :ls_cod_causale_tab
	from   tab_tipi_fat_ven  
	where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Si è verificato un errore nella lettura della Tabella Tipi Fatture."
		return -1
	end if
	
	if isnull(ls_nota_testata) then
		ls_nota_testata = ""
	end if

	if not isnull(ls_num_ord_cliente) and ls_flag_abilita_rif_bol_ven = "T" then
		ls_vostro_ordine = "~r~nVostro Ordine " + ls_num_ord_cliente
		if not isnull(ldt_data_ord_cliente) then
			ls_vostro_ordine_data = " del " + string(ldt_data_ord_cliente, "dd/mm/yyyy")
		else
			ls_vostro_ordine_data = ""
		end if
	else
		ls_vostro_ordine = ""
		ls_vostro_ordine_data = ""
	end if
	
//-------------------------------------- Modifica Nicola -------------------------------------------------------
	if ls_flag_doc_suc_tes = 'I' then //nota di testata
		select flag_doc_suc
		  into :ls_flag_doc_suc_tes
		  from tab_tipi_bol_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_bol_ven = :as_cod_tipo_bol_ven;
	end if		

	if ls_flag_doc_suc_tes = 'N' then
		ls_nota_testata = ""
	end if			

	
	if ls_flag_doc_suc_pie = 'I' then //nota di piede
		select flag_doc_suc
		  into :ls_flag_doc_suc_pie
		  from tab_tipi_bol_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_bol_ven = :as_cod_tipo_bol_ven;
	end if			
	
	if ls_flag_doc_suc_pie = 'N' then
		ls_nota_piede = ""
	end if				
//--------------------------------------- Fine Modifica --------------------------------------------------------	
	
	if ls_flag_abilita_rif_bol_ven = "T" then
		ls_nota_testata = ls_des_riferimento_fat_ven + string(ll_anno_documento) + "/" + &
								string(ll_num_documento) + " del " + string(ldt_data_bolla, "dd/mm/yyyy") + &
								ls_vostro_ordine + ls_vostro_ordine_data + "~r~n" + ls_nota_testata + &
								"~r~n"
	end if
	// spostato da qui la select tab_tipi_fat_ven

	
	// ----- controllo lunghezza note per ASE
	if (ls_db = "SYBASE_ASE" or ls_db = "MSSQL") and len(ls_nota_testata) > 255 then
		ls_nota_testata = left(ls_nota_testata, 255)
	end if
	// -----  fine controllo lunghezza note per ASE

	if isnull(ls_causale_trasporto) or trim(ls_causale_trasporto) = "" then
		ls_causale_trasporto = ls_causale_trasporto_tab
	end if
	if isnull(ls_cod_causale)  then
		ls_cod_causale = ls_cod_causale_tab
	end if

	select parametri_azienda.stringa
	into   :ls_lire
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LIR';

	if sqlca.sqlcode <> 0 then
		ls_messaggio = "Configurare il codice valuta per le Lire Italiane."
		return -1
	end if

	select tab_pagamenti.sconto
	into   :ld_sconto_pagamento
	from   tab_pagamenti
	where  tab_pagamenti.cod_azienda = :s_cs_xx.cod_azienda and 
			 tab_pagamenti.cod_pagamento = :ls_cod_pagamento;

	if sqlca.sqlcode <> 0 then
		ld_sconto_pagamento = 0
	end if

//----------------------------------- Modifica Nicola ------------------------------------
		select cod_des_fattura
		  into :ls_cod_des_fattura_com
		  from tab_tipi_fat_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
			
		if sqlca.sqlcode <> 0 then	
			setnull(ls_rag_soc_1_dest_fat)
			setnull(ls_rag_soc_2_dest_fat)
			setnull(ls_indirizzo_dest_fat)   
			setnull(ls_localita_dest_fat)   
			setnull(ls_frazione_dest_fat)   
			setnull(ls_cap_dest_fat)   
			setnull(ls_provincia_dest_fat)
		else 
			select rag_soc_1,
					 rag_soc_2,
					 indirizzo,
					 localita,
					 frazione,
					 cap,
					 provincia
			  into :ls_rag_soc_1_dest_fat,
			  		 :ls_rag_soc_2_dest_fat,
					 :ls_indirizzo_dest_fat,
					 :ls_localita_dest_fat,
					 :ls_frazione_dest_fat,
					 :ls_cap_dest_fat,
					 :ls_provincia_dest_fat
			  from anag_des_clienti
			 where cod_azienda = :s_cs_xx.cod_azienda and
			   	 cod_cliente = :ls_cod_cliente[1] and
					 cod_des_cliente = :ls_cod_des_fattura_com and
				 	 ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > :ldt_oggi));
					  
			if sqlca.sqlcode <> 0 then
				setnull(ls_rag_soc_1_dest_fat)
				setnull(ls_rag_soc_2_dest_fat)
				setnull(ls_indirizzo_dest_fat)   
				setnull(ls_localita_dest_fat)   
				setnull(ls_frazione_dest_fat)   
				setnull(ls_cap_dest_fat)   
				setnull(ls_provincia_dest_fat)
			end if	
		end if		
		  
//------------------------------------ Fine modifica -------------------------------------
	
	//prima di fare la insert metti l'operatore collegato per il nuovo documento
	if f_get_operatore_doc(ls_cod_operatore_new, ls_messaggio)>0 then
		ls_cod_operatore = ls_cod_operatore_new
	end if

	insert into tes_fat_ven
					(cod_azienda,   
					 anno_registrazione,   
					 num_registrazione,   
					 data_registrazione,   
					 cod_operatore,   
					 cod_tipo_fat_ven,   
					 cod_cliente,
					 cod_des_cliente,   
					 rag_soc_1,   
					 rag_soc_2,   
					 indirizzo,   
					 localita,   
					 frazione,   
					 cap,   
					 provincia,
			          rag_soc_1_dest_fat,
					 rag_soc_2_dest_fat,
					 indirizzo_dest_fat,
					 localita_dest_fat,
					 frazione_dest_fat,
					 cap_dest_fat,
					 provincia_dest_fat,					 
					 cod_deposito,   
					 cod_ubicazione,
					 cod_valuta,   
					 cambio_ven,   
					 cod_tipo_listino_prodotto,   
					 cod_pagamento,   
					 sconto,   
					 cod_agente_1,
					 cod_agente_2,
					 cod_banca,   
					 num_ord_cliente,   
					 data_ord_cliente,   
					 cod_imballo,   
					 aspetto_beni,
					 peso_netto,
					 peso_lordo,
					 num_colli,
					 cod_vettore,   
					 cod_inoltro,   
					 cod_mezzo,
					 causale_trasporto,
					 cod_porto,   
					 cod_resa,   
					 cod_documento,
					 numeratore_documento,
					 anno_documento,
					 num_documento,
					 data_fattura,
					 nota_testata,   
					 nota_piede,   
					 flag_fuori_fido,
					 flag_blocco,   
					 flag_movimenti,  
					 flag_contabilita,
					 tot_merci,
					 tot_spese_trasporto,
					 tot_spese_imballo,
					 tot_spese_bolli,
					 tot_spese_varie,
					 tot_sconto_cassa,
					 tot_sconti_commerciali,
					 imponibile_provvigioni_1,
					 imponibile_provvigioni_2,
					 tot_provvigioni_1,
					 tot_provvigioni_2,
					 importo_iva,
					 imponibile_iva,
					 importo_iva_valuta,
					 imponibile_iva_valuta,
					 tot_fattura,
					 tot_fattura_valuta,
					 flag_cambio_zero,
					 cod_banca_clien_for,
					 cod_causale,
					 flag_st_note_tes,  					 					 
					 flag_st_note_pie,
					 cod_natura_intra)  					 
	values      (:s_cs_xx.cod_azienda,   
					 :al_anno_registrazione,   
					 :ll_num_registrazione,   
					 :ldt_data_registrazione,   
					 :ls_cod_operatore,   
					 :ls_cod_tipo_fat_ven,   
					 :ls_cod_cliente[1],
					 :ls_cod_des_cliente,   
					 :ls_rag_soc_1,   
					 :ls_rag_soc_2,   
					 :ls_indirizzo,   
					 :ls_localita,   
					 :ls_frazione,   
					 :ls_cap,   
					 :ls_provincia,   
			          :ls_rag_soc_1_dest_fat,
			  		 :ls_rag_soc_2_dest_fat,
					 :ls_indirizzo_dest_fat,
					 :ls_localita_dest_fat,
					 :ls_frazione_dest_fat,
					 :ls_cap_dest_fat,
					 :ls_provincia_dest_fat,			 
					 :ls_cod_deposito,   
					 :ls_cod_ubicazione,
					 :ls_cod_valuta,   
					 :ld_cambio_ven,   
					 :ls_cod_tipo_listino_prodotto,   
					 :ls_cod_pagamento,   
					 :ld_sconto_testata,
					 :ls_cod_agente_1,
					 :ls_cod_agente_2,
					 :ls_cod_banca,   
					 :ls_num_ord_cliente,   
					 :ldt_data_ord_cliente,   
					 :ls_cod_imballo,   
					 :ls_aspetto_beni,
					 :ld_peso_netto,
					 :ld_peso_lordo,
					 :ld_num_colli,
					 :ls_cod_vettore,   
					 :ls_cod_inoltro,   
					 :ls_cod_mezzo,   
					 :ls_causale_trasporto,
					 :ls_cod_porto,   
					 :ls_cod_resa,   
					 null,
					 null,
					 0,
					 0,
					 null,
					 :ls_nota_testata,   
					 :ls_nota_piede,   
					 'N',
					 'N',   
					 'S',
					 'N',
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 0,
					 'N',
					 :ls_cod_banca_clien_for,
					 :ls_cod_causale,
					 'I',
					 'I',
					 :ls_cod_natura_intra);					 

	 if sqlca.sqlcode <> 0 then
		 ls_messaggio = "Errore durante la scrittura Testata Fatture Vendita."
		 return -1
	 end if

	if wf_crea_nota_fissa(al_anno_registrazione, ll_num_registrazione, ls_messaggio) = -1 then return -1

// ----------------------------------------------------------------------------------

	lds_det_bol_ven = Create DataStore
	lds_det_bol_ven.DataObject = "d_lista_det_bol_ven"
	lds_det_bol_ven.settransobject(sqlca)
	ll_rows = lds_det_bol_ven.retrieve(s_cs_xx.cod_azienda,&
												  al_anno_bolla, &
												  al_num_bolla)
	
	luo_genera_doc = create uo_generazione_documenti
	
	ll_prog_riga_fat_ven = 0
	for ll_row = 1 to ll_rows
		
		if ll_row = 1 and ls_flag_abilita_rif_bol_ven = "D" then
			ll_anno_bol_ven = lds_det_bol_ven.getitemnumber(ll_row, "anno_registrazione")
			ll_num_bol_ven  = lds_det_bol_ven.getitemnumber(ll_row, "num_registrazione")
			ll_prog_riga_bol_ven = lds_det_bol_ven.getitemnumber(ll_row, "prog_riga_bol_ven")
			ls_cod_tipo_det_ven  = ls_cod_tipo_det_ven_rif_bol_ven
			
			if f_trova_codice_iva(ldt_data_registrazione, ls_cod_tipo_det_ven, "C", ls_cod_cliente[1], ref ls_cod_iva) <> 0 then
				g_mb.messagebox("Generazione Fattura Pro-Forma","Errore durante la ricerca codice iva in tabella tipi dettaglio vendite." + sqlca.sqlerrtext,Information!)
			end if
			
			setnull(ls_cod_deposito)
			setnull(ls_cod_ubicazione)
			setnull(ls_cod_lotto)
			setnull(ll_progr_stock)
			setnull(ldt_data_stock)
			setnull(ls_cod_misura)
			setnull(ls_cod_prodotto)

			ls_des_prodotto =  ls_des_riferimento_fat_ven
			if ls_flag_rif_cod_documento = "S" then
				ls_des_prodotto = ls_des_prodotto + "-" + ls_cod_documento
			end if
			if ls_flag_rif_numeratore_doc = "S" then
				ls_des_prodotto = ls_des_prodotto + ls_numeratore_documento
			end if
			if ls_flag_rif_anno_documento = "S" then
				ls_des_prodotto = ls_des_prodotto + "-" + string(ll_anno_documento)
			end if
			if ls_flag_rif_num_documento = "S" then
				ls_des_prodotto = ls_des_prodotto + "-" + string(ll_num_documento)
			end if
			if ls_flag_rif_data_fattura = "S" then
				ls_des_prodotto = ls_des_prodotto + "-" + string(ldt_data_bolla,"dd/mm/yyyy")
			end if
			
			ld_quan_consegnata = 0
			ld_prezzo_vendita  = 0
			ld_fat_conversione_ven = 1
			ll_sconto_1 = 0
			ll_sconto_2 = 0
			ll_sconto_3 = 0
			ll_sconto_4 = 0
			ll_sconto_5 = 0
			ll_sconto_6 = 0
			ll_sconto_7 = 0
			ll_sconto_8 = 0
			ll_sconto_9 = 0
			ll_sconto_10 = 0
			ll_provvigione_1 = 0
			ll_provvigione_2 = 0
			setnull(ls_cod_tipo_movimento)
			setnull(ll_anno_registrazione_mov_mag)
			setnull(ll_num_registrazione_mov_mag)
			setnull(ls_nota_dettaglio)
			setnull(ll_anno_reg_ord_ven)
			setnull(ll_num_reg_ord_ven)
			setnull(ll_prog_riga_ord_ven)
			setnull(ll_anno_commessa)
			setnull(ll_num_commessa)
			setnull(ls_cod_centro_costo)
			setnull(ls_cod_versione)
	
			ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga				
				
			insert into det_fat_ven
						  (cod_azienda, 
							anno_registrazione,
							num_registrazione,
							prog_riga_fat_ven,
							cod_tipo_det_ven,
							cod_deposito,
							cod_ubicazione,
							cod_lotto,
							data_stock,
							progr_stock,
							cod_misura,
							cod_prodotto,
							des_prodotto,
							quan_fatturata,
							prezzo_vendita,
							sconto_1,
							sconto_2,
							provvigione_1,
							provvigione_2,
							cod_iva,
							cod_tipo_movimento,
							num_registrazione_mov_mag,
							anno_registrazione_fat,
							num_registrazione_fat,
							nota_dettaglio,
							anno_registrazione_bol_ven,
							num_registrazione_bol_ven,
							prog_riga_bol_ven,
							anno_commessa,
							num_commessa,
							cod_centro_costo,
							sconto_3,
							sconto_4,
							sconto_5,
							sconto_6,
							sconto_7,
							sconto_8,
							sconto_9,
							sconto_10,
							anno_registrazione_mov_mag,
							fat_conversione_ven,
							anno_reg_des_mov,
							num_reg_des_mov,
							anno_reg_ord_ven,
							num_reg_ord_ven,
							prog_riga_ord_ven,	
							cod_versione,
							num_confezioni,
							num_pezzi_confezione,
							flag_st_note_det,
							quantita_um,
							prezzo_um)							
				values  (:s_cs_xx.cod_azienda,
							:al_anno_registrazione,
							:ll_num_registrazione,
							:ll_prog_riga_fat_ven,
							:ls_cod_tipo_det_ven,
							:ls_cod_deposito,
							:ls_cod_ubicazione,
							:ls_cod_lotto,
							:ldt_data_stock,
							:ll_progr_stock,
							:ls_cod_misura,
							:ls_cod_prodotto,
							:ls_des_prodotto,
							:ld_quan_consegnata,
							:ld_prezzo_vendita,
							:ll_sconto_1,
							:ll_sconto_2,
							:ll_provvigione_1,
							:ll_provvigione_2,
							:ls_cod_iva,
							:ls_cod_tipo_movimento,
							:ll_num_registrazione_mov_mag,
							null,
							null,
							:ls_nota_dettaglio,
							:ll_anno_bol_ven,
							:ll_num_bol_ven,
							:ll_prog_riga_bol_ven,
							:ll_anno_commessa,
							:ll_num_commessa,
							:ls_cod_centro_costo,
							:ll_sconto_3,
							:ll_sconto_4,
							:ll_sconto_5,
							:ll_sconto_6,
							:ll_sconto_7,
							:ll_sconto_8,
							:ll_sconto_9,
							:ll_sconto_10,
							:ll_anno_registrazione_mov_mag,
							:ld_fat_conversione_ven,
							null,
							null,
							:ll_anno_reg_ord_ven,
							:ll_num_reg_ord_ven,
							:ll_prog_riga_ord_ven,
							:ls_cod_versione,
							0,
							0,
							'I',
							0,
							0);							
				if sqlca.sqlcode = -1 then
					ls_messaggio = "Si è verificato un errore in fase di aggiornamento Dettaglio Fattura Vendita."
					destroy lds_det_bol_ven
					destroy luo_genera_doc
					return -1
				end if
		end if
		
		ll_anno_bol_ven = lds_det_bol_ven.getitemnumber(ll_row, "anno_registrazione")
		ll_num_bol_ven  = lds_det_bol_ven.getitemnumber(ll_row, "num_registrazione")
		ll_prog_riga_bol_ven = lds_det_bol_ven.getitemnumber(ll_row, "prog_riga_bol_ven")
		ls_cod_tipo_det_ven  = lds_det_bol_ven.getitemstring(ll_row, "cod_tipo_det_ven")
		ls_cod_deposito   = lds_det_bol_ven.getitemstring(ll_row, "cod_deposito") 
		ls_cod_ubicazione =  lds_det_bol_ven.getitemstring(ll_row, "cod_ubicazione")
		ls_cod_lotto    = lds_det_bol_ven.getitemstring(ll_row, "cod_lotto")
		ll_progr_stock  = lds_det_bol_ven.getitemnumber(ll_row, "progr_stock")
		ldt_data_stock  = lds_det_bol_ven.getitemdatetime(ll_row, "data_stock")
		ls_cod_misura   =  lds_det_bol_ven.getitemstring(ll_row, "cod_misura")
		ls_cod_prodotto =  lds_det_bol_ven.getitemstring(ll_row, "cod_prodotto")
		ls_des_prodotto =  lds_det_bol_ven.getitemstring(ll_row, "des_prodotto")
		ld_quan_consegnata = lds_det_bol_ven.getitemnumber(ll_row, "quan_consegnata")
		ld_prezzo_vendita  = lds_det_bol_ven.getitemnumber(ll_row, "prezzo_vendita")
		ld_fat_conversione_ven = lds_det_bol_ven.getitemnumber(ll_row, "fat_conversione_ven")
		ll_sconto_1 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_1")
		ll_sconto_2 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_2")
		ll_sconto_3 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_3")
		ll_sconto_4 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_4")
		ll_sconto_5 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_5")
		ll_sconto_6 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_6")
		ll_sconto_7 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_7")
		ll_sconto_8 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_8")
		ll_sconto_9 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_9")
		ll_sconto_10 = lds_det_bol_ven.getitemnumber(ll_row, "sconto_10")
		ll_provvigione_1 = lds_det_bol_ven.getitemnumber(ll_row, "provvigione_1")
		ll_provvigione_2 = lds_det_bol_ven.getitemnumber(ll_row, "provvigione_2")
		ls_cod_iva       =  lds_det_bol_ven.getitemstring(ll_row, "cod_iva")
		ls_cod_tipo_movimento         =  lds_det_bol_ven.getitemstring(ll_row, "cod_tipo_movimento")
		ll_anno_registrazione_mov_mag = lds_det_bol_ven.getitemnumber(ll_row, "anno_registrazione_mov_mag")
		ll_num_registrazione_mov_mag  = lds_det_bol_ven.getitemnumber(ll_row, "num_registrazione_mov_mag")
		ls_nota_dettaglio    = lds_det_bol_ven.getitemstring(ll_row, "nota_dettaglio")
		ll_anno_reg_ord_ven  = lds_det_bol_ven.getitemnumber(ll_row, "anno_registrazione_ord_ven")
		ll_num_reg_ord_ven   = lds_det_bol_ven.getitemnumber(ll_row, "num_registrazione_ord_ven")
		ll_prog_riga_ord_ven = lds_det_bol_ven.getitemnumber(ll_row, "prog_riga_ord_ven")
		ll_anno_commessa     = lds_det_bol_ven.getitemnumber(ll_row, "anno_commessa")
		ll_num_commessa      = lds_det_bol_ven.getitemnumber(ll_row, "num_commessa")
		ls_cod_centro_costo  =  lds_det_bol_ven.getitemstring(ll_row, "cod_centro_costo")
		ls_cod_versione  =  lds_det_bol_ven.getitemstring(ll_row, "cod_versione")
		ll_num_confezioni  =  lds_det_bol_ven.getitemnumber(ll_row, "num_confezioni")
		ll_num_pezzi_confezione =  lds_det_bol_ven.getitemnumber(ll_row, "num_pezzi_confezione")
		ls_flag_doc_suc_det  =  lds_det_bol_ven.getitemstring(ll_row, "flag_doc_suc_det")		
		ld_quantita_um  = lds_det_bol_ven.getitemnumber(ll_row, "quantita_um")
		ld_prezzo_um  = lds_det_bol_ven.getitemnumber(ll_row, "prezzo_um")
		
//------------------------------------------------- Modifica Nicola ---------------------------------------------
		if ls_flag_doc_suc_det = 'I' then //nota dettaglio
			select flag_doc_suc_bl
			  into :ls_flag_doc_suc_det
			  from tab_tipi_det_ven
			 where cod_azienda = :s_cs_xx.cod_azienda
				and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		end if		
		
		
		if ls_flag_doc_suc_det = 'N' then
			ls_nota_dettaglio = ""
		end if				
//-------------------------------------------------- Fine Modifica ----------------------------------------------		
	
		ll_prog_riga_fat_ven = ll_prog_riga_fat_ven + il_incremento_riga
		
		insert into det_fat_ven(	cod_azienda, 
										anno_registrazione,
										num_registrazione,
										prog_riga_fat_ven,
										cod_tipo_det_ven,
										cod_deposito,
										cod_ubicazione,
										cod_lotto,
										data_stock,
										progr_stock,
										cod_misura,
										cod_prodotto,
										des_prodotto,
										quan_fatturata,
										prezzo_vendita,
										sconto_1,
										sconto_2,
										provvigione_1,
										provvigione_2,
										cod_iva,
										cod_tipo_movimento,
										num_registrazione_mov_mag,
										anno_registrazione_fat,
										num_registrazione_fat,
										nota_dettaglio,
										anno_registrazione_bol_ven,
										num_registrazione_bol_ven,
										prog_riga_bol_ven,
										anno_commessa,
										num_commessa,
										cod_centro_costo,
										sconto_3,
										sconto_4,
										sconto_5,
										sconto_6,
										sconto_7,
										sconto_8,
										sconto_9,
										sconto_10,
										anno_registrazione_mov_mag,
										fat_conversione_ven,
										anno_reg_des_mov,
										num_reg_des_mov,
										anno_reg_ord_ven,
										num_reg_ord_ven,
										prog_riga_ord_ven,
										cod_versione,
										num_confezioni,
										num_pezzi_confezione,
										flag_st_note_det,
										quantita_um,
										prezzo_um)
		values  (				:s_cs_xx.cod_azienda,
								:al_anno_registrazione,
								:ll_num_registrazione,
								:ll_prog_riga_fat_ven,
								:ls_cod_tipo_det_ven,
								:ls_cod_deposito,
								:ls_cod_ubicazione,
								:ls_cod_lotto,
								:ldt_data_stock,
								:ll_progr_stock,
								:ls_cod_misura,
								:ls_cod_prodotto,
								:ls_des_prodotto,
								:ld_quan_consegnata,
								:ld_prezzo_vendita,
								:ll_sconto_1,
								:ll_sconto_2,
								:ll_provvigione_1,
								:ll_provvigione_2,
								:ls_cod_iva,
								:ls_cod_tipo_movimento,
								:ll_num_registrazione_mov_mag,
								null,
								null,
								:ls_nota_dettaglio,
								:ll_anno_bol_ven,
								:ll_num_bol_ven,
								:ll_prog_riga_bol_ven,
								:ll_anno_commessa,
								:ll_num_commessa,
								:ls_cod_centro_costo,
								:ll_sconto_3,
								:ll_sconto_4,
								:ll_sconto_5,
								:ll_sconto_6,
								:ll_sconto_7,
								:ll_sconto_8,
								:ll_sconto_9,
								:ll_sconto_10,
								:ll_anno_registrazione_mov_mag,
								:ld_fat_conversione_ven,
								null,
								null,
								:ll_anno_reg_ord_ven,
								:ll_num_reg_ord_ven,
								:ll_prog_riga_ord_ven,
								:ls_cod_versione,
								:ll_num_confezioni,
								:ll_num_pezzi_confezione,
								'I',
								:ld_quantita_um,
								:ld_prezzo_um);						
						
		if sqlca.sqlcode = -1 then
			ls_messaggio = "Si è verificato un errore in fase di aggiornamento Dettaglio Fattura Vendita."
			destroy lds_det_bol_ven
			destroy luo_genera_doc
			return -1
		end if
		
		//--------------------------------------------------------------------
		//SR Note_conformita_prodotto
		if luo_genera_doc.uof_note_in_fattura(al_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven, ls_messaggio)<0 then
			destroy lds_det_bol_ven
			destroy luo_genera_doc
			return -1
		end if
		//--------------------------------------------------------------------
		
	next

	destroy lds_det_bol_ven
	destroy luo_genera_doc
	
	
	luo_calcolo = create uo_calcola_documento_euro

	if luo_calcolo.uof_calcola_documento(al_anno_registrazione,ll_num_registrazione,"fat_ven",ls_messaggio) <> 0 then
		destroy luo_calcolo
		return -1
	end if
	
	destroy luo_calcolo

	li_riga_orig = dw_gen_orig_dest.insertrow(0)
	dw_gen_orig_dest.setrow(dw_gen_orig_dest.rowcount())
	select cod_documento,
			 numeratore_documento,
			 anno_documento,
			 num_documento,
			 data_bolla
	into   :ls_cod_doc_orig,
			 :ls_numeratore_orig,
			 :ll_anno_orig,
			 :ll_num_orig,
			 :ldt_data_orig
	from   tes_bol_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :al_anno_bolla and
			 num_registrazione = :al_num_bolla;
	if sqlca.sqlcode = 0 then
		dw_gen_orig_dest.setitem(li_riga_orig, "cod_documento", ls_cod_doc_orig)
		dw_gen_orig_dest.setitem(li_riga_orig, "numeratore", ls_numeratore_orig)
		dw_gen_orig_dest.setitem(li_riga_orig, "anno_documento", ll_anno_orig)
		dw_gen_orig_dest.setitem(li_riga_orig, "num_documento", ll_num_orig)
		dw_gen_orig_dest.setitem(li_riga_orig, "data_documento", ldt_data_orig)
	end if
	dw_gen_orig_dest.setitem(li_riga_orig, "anno_registrazione_orig", al_anno_bolla)
	dw_gen_orig_dest.setitem(li_riga_orig, "num_registrazione_orig", al_num_bolla)
	dw_gen_orig_dest.setitem(li_riga_orig, "anno_registrazione_dest", al_anno_registrazione)
	dw_gen_orig_dest.setitem(li_riga_orig, "num_registrazione_dest", ll_num_registrazione)
	f_scrivi_log("GEN.FATTURE: la bolla "+ + string(al_anno_bolla) + "/" + string(al_num_bolla)+" ha generato la fattura " + string(al_anno_registrazione) + "/" + string(ll_num_registrazione))

	update tes_bol_ven
	set    flag_gen_fat = 'S'
	where  tes_bol_ven.cod_azienda = :s_cs_xx.cod_azienda and
	       tes_bol_ven.anno_registrazione = :al_anno_bolla and
			 tes_bol_ven.num_registrazione = :al_num_bolla;
	if sqlca.sqlcode <> 0 then 
		g_mb.messagebox("Generazione fatture singole","Errore durante aggiornamento segnale di ~r~n" + &
		                             "BOLLA FATTURATA in testata bolle", information!)
	end if
end if

return 0
end function

public function integer wf_controlla_blocchi_clienti ();long				ll_i, ll_rows, ll_num_bolla, ll_row_ds,ll_num_documento

string				ls_cod_cliente, ls_rag_soc_1, ls_clienti_elaborati[], ls_clienti_con_blocchi[], ls_clienti_con_warning[], ls_messaggio, &
					ls_cod_tipo_bol_ven, ls_cod_documento, ls_numeratore_documento, ls_numerazione
					
integer			li_anno_bolla, li_ret, li_anno_documento

datetime			ldt_oggi

boolean			lb_salta, lb_ddt_esclusi

datastore		lds_data




ldt_oggi = datetime(today(), 00:00:00)
lds_data = create datastore
lds_data.dataobject = "d_riepilogo_documenti_fido"
lds_data.settransobject(sqlca)
lb_ddt_esclusi = false


ll_rows = dw_gen_fat_ven.rowcount()
for ll_i = 1 to ll_rows
	
	//elabora solo quelle con il check box selezionato
	if dw_gen_fat_ven.getitemstring(ll_i, "selezionata") = "S" and dw_gen_fat_ven.getitemstring(ll_i, "confermata") = "S" then
	else
		continue
	end if
	
	ls_cod_cliente = dw_gen_fat_ven.getitemstring(ll_i, "cod_cliente")
	
	//#################################################################################
	li_ret = iuo_fido.uof_get_blocco_cliente_loop(	ls_cod_cliente, ldt_oggi, is_cod_parametro_blocco, &
																ls_clienti_elaborati[], ls_clienti_con_blocchi[], ls_clienti_con_warning[], &
																lb_salta, ls_messaggio)
	if li_ret <> 0 then
		//salvo in un datastore
		ls_rag_soc_1 = dw_gen_fat_ven.getitemstring(ll_i, "rag_soc_1")
		li_anno_bolla = dw_gen_fat_ven.getitemnumber(ll_i, "anno_registrazione")
		ll_num_bolla = dw_gen_fat_ven.getitemnumber(ll_i, "num_registrazione")
		ls_cod_tipo_bol_ven = dw_gen_fat_ven.getitemstring(ll_i, "cod_tipo_bol_ven")
		
		ls_cod_documento = dw_gen_fat_ven.getitemstring(ll_i, "cod_documento")
		ls_numeratore_documento = dw_gen_fat_ven.getitemstring(ll_i, "numeratore_documento")
		li_anno_documento = dw_gen_fat_ven.getitemnumber(ll_i, "anno_documento")
		ll_num_documento = dw_gen_fat_ven.getitemnumber(ll_i, "num_documento")
		
		ls_numerazione = ls_cod_tipo_bol_ven + " " + ls_cod_documento + " " + string(li_anno_documento) + "/" + ls_numeratore_documento + " " + string(ll_num_documento)
		
		ll_row_ds = lds_data.insertrow(0)
		lds_data.setitem(ll_row_ds, "cod_cliente", ls_cod_cliente)
		lds_data.setitem(ll_row_ds, "rag_soc_1", ls_rag_soc_1)
		lds_data.setitem(ll_row_ds, "anno_ordine", li_anno_bolla)
		lds_data.setitem(ll_row_ds, "num_ordine", ll_num_bolla)
		lds_data.setitem(ll_row_ds, "numerazione", ls_numerazione)
		if ls_messaggio<>"" then
			lds_data.setitem(ll_row_ds, "messaggio", ls_messaggio)
			lds_data.setitem(ll_row_ds, "ordinamento", 0)
		else
			lds_data.setitem(ll_row_ds, "ordinamento", 1)
		end if
		
		lds_data.setitem(ll_row_ds, "tipo_blocco", li_ret)
		
		//se li_ret=2 allora il cliente è bloccato (da anagrafica o finanziario), quindi de-seleziona il ddt
		if li_ret=2 then
			dw_gen_fat_ven.setitem(ll_i, "selezionata", "N")
			lb_ddt_esclusi = true
		end if
		
	end if
next

if lb_ddt_esclusi then
	g_mb.warning("Alcuni clienti nei ddt risultano bloccati o con blocco finanziario "+&
						"e la tabella di configurazione 'parametri blocco' indica di bloccare la generazione fattura!~r~nQuesti ddt saranno de-selezionati!~r~n"+&
						"Premere OK per visualizzare il riepilogo e successivamente proseguire nella generazione fatture.")
end if

s_cs_xx.parametri.parametro_b_1 = false

if lds_data.rowcount() > 0 then
	s_cs_xx.parametri.parametro_ds_1 = lds_data
	s_cs_xx.parametri.parametro_s_10 = "DDT CON CLIENTE BLOCCATO IN ANAGRAFICA O CON BLOCCO FINANZIARIO"
	s_cs_xx.parametri.parametro_b_1 = true
	window_open(w_riepilogo_documenti_fido, 0)
end if

destroy lds_data



return 0
end function

event pc_setwindow;call super::pc_setwindow;windowobject lw_oggetti[]


set_w_options(c_noenablepopup)


iuo_fido = create uo_fido_cliente


lw_oggetti[1] = dw_gen_orig_dest
dw_folder.fu_assigntab(3, "Fatture Generate", lw_oggetti[])

lw_oggetti[1] = dw_ext_sel_gen_fat_ven
lw_oggetti[2] = cb_cerca
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])

lw_oggetti[1] = dw_gen_fat_ven
lw_oggetti[2] = cb_genera
dw_folder.fu_assigntab(2, "Lista Bolle", lw_oggetti[])

dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

dw_gen_orig_dest.set_dw_options(sqlca, &
                             	  pcca.null_object, &
										  c_nonew + &
										  c_nomodify + &
										  c_nodelete + &
										  c_disablecc + &
										  c_disableccinsert, &
	                             c_viewmodeblack)
													 
dw_ext_sel_gen_fat_ven.set_dw_options(sqlca, &
												 pcca.null_object, &
												 c_nomodify + &
												 c_nodelete + &
												 c_newonopen + &
												 c_disableCC, &
												 c_noresizedw + &
												 c_nohighlightselected + &
												 c_cursorrowpointer)
dw_gen_fat_ven.set_dw_options(sqlca, &
									  pcca.null_object, &
									  c_nonew + &
									  c_nomodify + &
									  c_nodelete + &
									  c_disablecc + &
									  c_noretrieveonopen + &
									  c_disableccinsert, &
									  c_ViewModeBorderUnchanged + &
									  c_ViewModeColorUnchanged + &
									  c_NoCursorRowFocusRect + &
									  c_NoCursorRowPointer)

iuo_dw_main = dw_gen_fat_ven												 
save_on_close(c_socnosave)
dw_1.visible = false
end event

on w_gen_fat_ven.create
int iCurrent
call super::create
this.dw_1=create dw_1
this.dw_ext_sel_gen_fat_ven=create dw_ext_sel_gen_fat_ven
this.dw_folder=create dw_folder
this.dw_gen_orig_dest=create dw_gen_orig_dest
this.cb_genera=create cb_genera
this.cb_cerca=create cb_cerca
this.dw_gen_fat_ven=create dw_gen_fat_ven
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_1
this.Control[iCurrent+2]=this.dw_ext_sel_gen_fat_ven
this.Control[iCurrent+3]=this.dw_folder
this.Control[iCurrent+4]=this.dw_gen_orig_dest
this.Control[iCurrent+5]=this.cb_genera
this.Control[iCurrent+6]=this.cb_cerca
this.Control[iCurrent+7]=this.dw_gen_fat_ven
end on

on w_gen_fat_ven.destroy
call super::destroy
destroy(this.dw_1)
destroy(this.dw_ext_sel_gen_fat_ven)
destroy(this.dw_folder)
destroy(this.dw_gen_orig_dest)
destroy(this.cb_genera)
destroy(this.cb_cerca)
destroy(this.dw_gen_fat_ven)
end on

event pc_setddlb;call super::pc_setddlb;f_PO_LoadDDDW_DW(dw_ext_sel_gen_fat_ven,"cod_tipo_bol_ven",sqlca,&
                 "tab_tipi_bol_ven","cod_tipo_bol_ven","des_tipo_bol_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_PO_LoadDDDW_DW(dw_ext_sel_gen_fat_ven,"cod_tipo_raggruppo",sqlca,&
                 "tab_tipi_rag","cod_tipo_rag","des_tipo_rag",&
                 "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
					  "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW(dw_ext_sel_gen_fat_ven,"cod_causale_trasp",sqlca,&
                 "tab_causali_trasp","cod_causale","des_causale",&
                 "(cod_azienda = '" + s_cs_xx.cod_azienda + "') and " + &
                 "(flag_segue_fattura = 'S') and " + &
					  "((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_PO_LoadDDDW_DW(dw_ext_sel_gen_fat_ven,"cod_giro",sqlca, &
                 "tes_giri_consegne", &
                 "cod_giro_consegna", &
                 "des_giro_consegna", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
f_PO_LoadDDDW_DW(dw_ext_sel_gen_fat_ven,"cod_tipo_fat_ven",sqlca,&
                 "tab_tipi_fat_ven","cod_tipo_fat_ven","des_tipo_fat_ven",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_ext_sel_gen_fat_ven,"cod_documento",sqlca,&
                 "tab_documenti","cod_documento","des_documento",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_ext_sel_gen_fat_ven,"cod_agente_1",sqlca,&
                 "anag_agenti","cod_agente","rag_soc_1",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

f_PO_LoadDDDW_DW(dw_ext_sel_gen_fat_ven,"cod_agente_2",sqlca,&
                 "anag_agenti","cod_agente","rag_soc_1",&
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event close;call super::close;

destroy iuo_fido
end event

type dw_1 from datawindow within w_gen_fat_ven
integer x = 2496
integer y = 476
integer width = 503
integer height = 636
integer taborder = 80
string title = "none"
string dataobject = "d_ext_sel_gen_fat_ven_sel_bolle"
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

type dw_ext_sel_gen_fat_ven from uo_cs_xx_dw within w_gen_fat_ven
integer x = 46
integer y = 128
integer width = 3369
integer height = 1516
integer taborder = 50
string dataobject = "d_ext_sel_gen_fat_ven"
boolean border = false
end type

event itemchanged;call super::itemchanged;string ls_flag_forza_tipo_fattura, ls_null

setnull(ls_null)

if row > 0 then
	choose case i_colname
		case "flag_forza_tipo"
			if i_coltext = "N" then
				//blocca e pulisci il campo tipo fattura da generare
				this.object.cod_tipo_fat_ven.tabsequence = 0
				this.object.cod_tipo_fat_ven.background.color = 12632256
				this.setitem(row, "cod_tipo_fat_ven", ls_null)
			else
				//sproteggi il campo tipo fattura da generare
				this.object.cod_tipo_fat_ven.tabsequence = 1000
				this.object.cod_tipo_fat_ven.background.color = 16777215
			end if
	end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
		case "b_ricerca_cliente"
			guo_ricerca.uof_ricerca_cliente(dw_ext_sel_gen_fat_ven,"cod_cliente")
		case "b_ddt"
			long ll_i, ll_riga
			
			if dw_1.visible then
				dw_1.reset()
				dw_1.visible = false
			else
				dw_1.reset()
				for ll_i = 1 to 100
					ll_riga = dw_1.insertrow(0)
					dw_1.setitem(ll_riga, "num_bolla", 0)
				next
				dw_1.setrow(1)
				dw_1.visible = true
			end if
		
	end choose
end event

type dw_folder from u_folder within w_gen_fat_ven
integer x = 23
integer y = 20
integer width = 3429
integer height = 2308
integer taborder = 60
end type

type dw_gen_orig_dest from uo_cs_xx_dw within w_gen_fat_ven
event pcd_retrieve pbm_custom60
integer x = 46
integer y = 140
integer width = 3383
integer height = 2160
integer taborder = 40
string dataobject = "d_gen_orig_dest_fat"
boolean hscrollbar = true
boolean vscrollbar = true
end type

event pcd_print;call super::pcd_print;long job

job = PrintOpen( ) 

PrintDataWindow(job, this) 
PrintClose(job)
end event

type cb_genera from uo_cb_ok within w_gen_fat_ven
event clicked pbm_bnclicked
integer x = 3054
integer y = 2216
integer width = 366
integer height = 80
integer taborder = 90
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Genera"
end type

event clicked;string ls_messaggio, ls_flag_calcola, ls_flag_singole, ls_cod_valuta, ls_cod_lire, ls_cod_valuta_corrente
long ll_anno_registrazione, ll_anno_fat_ven, ll_num_fat_ven, ll_rows, ll_i
uo_calcola_documento_euro luo_doc
string ls_flag_forza_tipo_fattura, ls_tipo_fattura_dausare, ls_des_tipo_fat_ven

setpointer(hourglass!)

dw_gen_orig_dest.reset()
dw_ext_sel_gen_fat_ven.accepttext()
dw_gen_fat_ven.accepttext()

select parametri_azienda.numero
into   :ll_anno_registrazione
from   parametri_azienda
where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
		 parametri_azienda.flag_parametro = 'N' and &
		 parametri_azienda.cod_parametro = 'ESC';

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Errore durante la lettura del parametro ESC.", Exclamation!, ok!)
	setpointer(arrow!)
	return
end if


//se nella parametri blocco è previsto un blocco o un warning (GFV - Generazione fattura Vendita)
//allora fai un primo ciclo di controllo con i warning o i blocchi (in tal caso deseleziona i ddt con i clienti bloccati)
wf_controlla_blocchi_clienti()

//Donato 03/04/2009 verifica se è stato specificato il tipo di fattura da generare (forzato)
ls_flag_forza_tipo_fattura = dw_ext_sel_gen_fat_ven.getitemstring(dw_ext_sel_gen_fat_ven.getrow(), &
																										"flag_forza_tipo")
if ls_flag_forza_tipo_fattura = "S" then
	ls_tipo_fattura_dausare = dw_ext_sel_gen_fat_ven.getitemstring(dw_ext_sel_gen_fat_ven.getrow(), &
																										"cod_tipo_fat_ven")
	if isnull(ls_tipo_fattura_dausare) or ls_tipo_fattura_dausare = "" then
		g_mb.messagebox("Attenzione", "E' necessario specificare il tipo fattura da usare nella generazione!", &
				  Exclamation!, ok!)
		dw_ext_sel_gen_fat_ven.setcolumn("cod_tipo_fat_ven")
		setpointer(arrow!)
		return
	else
		//chiedere conferma dell'azione di forzatura
		select des_tipo_fat_ven
		into :ls_des_tipo_fat_ven
		from tab_tipi_fat_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_tipo_fat_ven = :ls_tipo_fattura_dausare;
				
		if isnull(ls_des_tipo_fat_ven) or ls_des_tipo_fat_ven = "" then ls_des_tipo_fat_ven = ls_tipo_fattura_dausare
		
		if g_mb.messagebox("APICE", "L'operazione forzerà il tipo per tutte le fatture da generare con il valore~r~n '"+ls_des_tipo_fat_ven+"'Continuare?", Question!, YesNo!, 1) = 1 then
		else
			setpointer(arrow!)
			return									
		end if
	end if
end if
//fine modifica --------------------------------------------------------------------------------------

ls_flag_singole = dw_ext_sel_gen_fat_ven.getitemstring(1,"flag_tipo_generazione")
ls_flag_calcola = dw_ext_sel_gen_fat_ven.getitemstring(1,"flag_calcolo_fattura")

if ls_flag_singole ="S" then
	if wf_genera_fatture_singole(ll_anno_registrazione, ls_messaggio) = -1 then
		g_mb.messagebox("Attenzione", ls_messaggio, Exclamation!, ok!)
		rollback;
		setpointer(arrow!)
		return
	else
		commit;

	end if
else
	if wf_genera_fatture_riepilogate(ll_anno_registrazione, ls_messaggio) = -1 then
		g_mb.messagebox("Attenzione", ls_messaggio, Exclamation!, ok!)
		rollback;
		setpointer(arrow!)
		return
	else
		commit;

	end if
end if

dw_folder.fu_selecttab(3)
ib_messaggio_retrieve = false
dw_gen_fat_ven.triggerevent("pcd_retrieve")
ib_messaggio_retrieve = true
dw_1.reset()
dw_1.visible = false
setpointer(arrow!)
end event

type cb_cerca from commandbutton within w_gen_fat_ven
integer x = 3058
integer y = 2220
integer width = 366
integer height = 80
integer taborder = 30
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Cerca"
end type

event clicked;dw_gen_fat_ven.resetupdate( )

dw_1.accepttext()
dw_ext_sel_gen_fat_ven.accepttext()
dw_ext_sel_gen_fat_ven.change_dw_focus(dw_gen_fat_ven)
parent.postevent("pc_retrieve")
dw_folder.fu_selecttab(2)


end event

type dw_gen_fat_ven from uo_cs_xx_dw within w_gen_fat_ven
event pcd_retrieve pbm_custom60
integer x = 46
integer y = 128
integer width = 3383
integer height = 2072
integer taborder = 70
boolean bringtotop = true
string dataobject = "d_ext_gen_fat_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
boolean livescroll = true
end type

event pcd_retrieve;call super::pcd_retrieve;boolean					lb_test
string						ls_cod_tipo_bol_ven, ls_cod_cliente, ls_cod_causale, ls_cod_giro,ls_des_giro,  ls_cod_cliente_fattura, ls_cod_tipo_raggruppo, ls_sql, ls_errore, &
							ls_cod_documento, ls_cod_agente_1, ls_cod_agente_2
long						ll_bolla_inizio, ll_bolla_fine, ll_errore, ll_i, ll_riga, ll_cont, ll_y, ll_anno_documento
date						ld_data_inizio, ld_data_fine
datetime					ldt_data_inizio, ldt_data_fine
datastore				lds_datastore

dw_gen_fat_ven.reset()


//10/12/2013 Donato
//tolta tutta quella porkeria che sta qua sopra che era arcaica, che se nn lo faccio io non lo fa mai nessuno
//e inserito creazione datastore dinamica, che mette nella where solo ciò che effetivamnete serve a filtrare, cazzo!!!
//ANCHE PERCHE' SU SQLSERVER DA PURE ERRORE CON QUELLA PORKERIA ......
ls_cod_tipo_bol_ven 			= dw_ext_sel_gen_fat_ven.getitemstring(1,"cod_tipo_bol_ven")
ls_cod_cliente 					= dw_ext_sel_gen_fat_ven.getitemstring(1,"cod_cliente")
ls_cod_causale 				= dw_ext_sel_gen_fat_ven.getitemstring(1,"cod_causale_trasp")
ls_cod_giro 						= dw_ext_sel_gen_fat_ven.getitemstring(1,"cod_giro")
ls_cod_tipo_raggruppo 		= dw_ext_sel_gen_fat_ven.getitemstring(1,"cod_tipo_raggruppo")
ll_bolla_inizio 					= dw_ext_sel_gen_fat_ven.getitemnumber(1,"bolla_inizio")
ll_bolla_fine   					= dw_ext_sel_gen_fat_ven.getitemnumber(1,"bolla_fine")
ldt_data_inizio					= dw_ext_sel_gen_fat_ven.getitemdatetime(1,"data_inizio")
ldt_data_fine					= dw_ext_sel_gen_fat_ven.getitemdatetime(1,"data_fine")

ld_data_inizio 					= date(ldt_data_inizio)
ld_data_fine   					= date(ldt_data_fine)

ls_cod_agente_1 				= dw_ext_sel_gen_fat_ven.getitemstring(1,"cod_agente_1")
ls_cod_agente_2 				= dw_ext_sel_gen_fat_ven.getitemstring(1,"cod_agente_2")
ls_cod_documento 			= dw_ext_sel_gen_fat_ven.getitemstring(1,"cod_documento")
ll_anno_documento			= dw_ext_sel_gen_fat_ven.getitemnumber(1,"anno_documento")



ls_sql = 	    "select tes_bol_ven.anno_registrazione,"+ &
						"tes_bol_ven.num_registrazione,"+ &
						"tes_bol_ven.data_registrazione,"+ &
						"tes_bol_ven.cod_cliente,"+ &
						"tes_bol_ven.cod_fornitore,"+ &
						"tes_bol_ven.cod_tipo_bol_ven,"+ &
						"tes_bol_ven.cod_documento,"+ &
						"tes_bol_ven.numeratore_documento,"+ &
						"tes_bol_ven.anno_documento,"+ &
						"tes_bol_ven.num_documento,"+ &
						"tes_bol_ven.data_bolla,"+ &
						"tes_bol_ven.cod_causale,"+ &
						"tab_causali_trasp.des_causale,"+ &
						"anag_clienti.rag_soc_1,"+ &
						"tab_causali_trasp.flag_segue_fattura,"+ &
						"tes_bol_ven.cod_des_cliente,"+ &
						"tes_bol_ven.flag_movimenti "+ &
			"from tes_bol_ven "+&
			"left outer join tab_causali_trasp on tes_bol_ven.cod_azienda=tab_causali_trasp.cod_azienda and tes_bol_ven.cod_causale = tab_causali_trasp.cod_causale "+&
			"left outer join anag_clienti on tes_bol_ven.cod_azienda=anag_clienti.cod_azienda and tes_bol_ven.cod_cliente = anag_clienti.cod_cliente "+&
			"where tes_bol_ven.cod_azienda='"+s_cs_xx.cod_azienda+"' and tab_causali_trasp.flag_blocco='N' and tes_bol_ven.flag_gen_fat='N' "
					
if ls_cod_tipo_bol_ven<>"" and not isnull(ls_cod_tipo_bol_ven) then
	ls_sql +=  " and tes_bol_ven.cod_tipo_bol_ven='"+ls_cod_tipo_bol_ven +"' "
end if
if ls_cod_cliente<>"" and not isnull(ls_cod_cliente) then
	ls_sql +=  " and tes_bol_ven.cod_cliente='"+ls_cod_cliente +"' "
end if
if ls_cod_causale<>"" and not isnull(ls_cod_causale) then
	ls_sql +=  " and tes_bol_ven.cod_causale='"+ls_cod_causale +"' "
end if

//per il giro, il filtro viene gestito più sotto ...
if ls_cod_giro="" then setnull(ls_cod_giro)

//il tipo raggruppo non viene gestito e non so perchè, quindi lascio tutto come era prima con la porkeria ...
//ls_cod_tipo_raggruppo

if ll_bolla_inizio>=0 and not isnull(ll_bolla_inizio) then
	ls_sql +=  " and tes_bol_ven.num_documento>="+string(ll_bolla_inizio) +" "
end if
if ll_bolla_fine>=0  and not isnull(ll_bolla_fine) then
	ls_sql +=  " and tes_bol_ven.num_documento<="+string(ll_bolla_fine) +" "
end if
if not isnull(ldt_data_inizio) and year(date(ldt_data_inizio))>1980 then
	ls_sql += " and  tes_bol_ven.data_bolla>='" + string(ldt_data_inizio, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_data_fine) and year(date(ldt_data_fine))>1980 then
	ls_sql += " and  tes_bol_ven.data_bolla<='" + string(ldt_data_fine, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if ll_anno_documento>=0 and not isnull(ll_anno_documento) then
	ls_sql +=  " and tes_bol_ven.anno_documento>="+string(ll_anno_documento) +" "
end if
if len(ls_cod_documento) >=0 and not isnull(ls_cod_documento) then
	ls_sql +=  " and tes_bol_ven.cod_documento>='"+ls_cod_documento +"' "
end if
if len(ls_cod_agente_1) >=0 and not isnull(ls_cod_agente_1) then
	ls_sql +=  " and tes_bol_ven.cod_agente_1='"+ls_cod_agente_1 +"' "
end if
if len(ls_cod_agente_2) >=0 and not isnull(ls_cod_agente_2) then
	ls_sql +=  " and tes_bol_ven.cod_agente_2='"+ls_cod_agente_2 +"' "
end if

ls_sql += "order by tes_bol_ven.cod_documento,tes_bol_ven.numeratore_documento,tes_bol_ven.anno_documento,tes_bol_ven.num_documento "

ll_errore = guo_functions.uof_crea_datastore(lds_datastore, ls_sql, ls_errore)
if ll_errore<0 then
	g_mb.error(ls_errore)
	return
	
elseif ll_errore=0 then
	
	if ib_messaggio_retrieve then g_mb.warning("Nessuna bolla corrisponde ai criteri di ricerca impostati!")
	
	dw_gen_fat_ven.reset()
	destroy lds_datastore
	return
	
end if



dw_gen_fat_ven.setredraw(false)
for ll_i = 1 to ll_errore

	//if lds_datastore.getitemstring(ll_i, "flag_segue_fattura") <> "S" or isnull(lds_datastore.getitemstring(ll_i, "flag_segue_fattura")) then continue
	if lds_datastore.getitemstring(ll_i, 15) <> "S" or isnull(lds_datastore.getitemstring(ll_i, 15)) then continue
	
	ls_cod_cliente_fattura = lds_datastore.getitemstring(ll_i, 4)
	if isnull(ls_cod_cliente_fattura) then continue
	if not isnull(ls_cod_giro) then
		select count(*)
		into   :ll_cont
		from   det_giri_consegne
		where  cod_azienda = :s_cs_xx.cod_azienda  and
		       cod_giro_consegna = :ls_cod_giro and
				 cod_cliente = :ls_cod_cliente_fattura;
		if ll_cont = 0 or isnull(ll_cont) then continue
	end if
	
	// aggiunto da EnMe per OCEM
	if dw_1.visible then
		lb_test = false
		for ll_y = 1 to dw_1.rowcount()
			if dw_1.getitemnumber(ll_y,"num_bolla") > 0 and dw_1.getitemnumber(ll_y,"num_bolla") = lds_datastore.getitemnumber(ll_i, 10) then
				lb_test = true
				exit
			end if				
		next
		if lb_test = false then continue
	end if
	// fine modifica

	ll_riga = dw_gen_fat_ven.insertrow(0)		
																																											//DALLA NUMERAZIONE COLONNE DEL DATASTORE
	dw_gen_fat_ven.setitem(ll_riga, "anno_registrazione",lds_datastore.getitemnumber(ll_i, 1))													//1.	anno_registrazione
	dw_gen_fat_ven.setitem(ll_riga, "num_registrazione",lds_datastore.getitemnumber(ll_i, 2) )													//2.	num_registrazione
	dw_gen_fat_ven.setitem(ll_riga, "cod_cliente", ls_cod_cliente_fattura)
	dw_gen_fat_ven.setitem(ll_riga, "rag_soc_1", lds_datastore.getitemstring(ll_i, 14))																//14.	rag_soc_1
	dw_gen_fat_ven.setitem(ll_riga, "cod_tipo_bol_ven", lds_datastore.getitemstring(ll_i, 6))														//6.	cod_tipo_bol_ven
	dw_gen_fat_ven.setitem(ll_riga, "cod_documento", lds_datastore.getitemstring(ll_i, 7))															//7.	cod_documento
	dw_gen_fat_ven.setitem(ll_riga, "numeratore_documento", lds_datastore.getitemstring(ll_i, 8))												//8.	numeratore_documento
	dw_gen_fat_ven.setitem(ll_riga, "anno_documento",lds_datastore.getitemnumber(ll_i, 9))														//9.	anno_documento
	dw_gen_fat_ven.setitem(ll_riga, "num_documento",lds_datastore.getitemnumber(ll_i, 10) )													//10.	num_documento
	dw_gen_fat_ven.setitem(ll_riga, "data_documento",lds_datastore.getitemdatetime(ll_i, 11) )													//11.	data_bolla
	dw_gen_fat_ven.setitem(ll_riga, "cod_causale", lds_datastore.getitemstring(ll_i, 12))																//12.	cod_causale
	dw_gen_fat_ven.setitem(ll_riga, "des_causale", lds_datastore.getitemstring(ll_i, 13))																//13.	des_causale
	dw_gen_fat_ven.setitem(ll_riga, "cod_giro", ls_cod_giro)
	dw_gen_fat_ven.setitem(ll_riga, "des_giro", ls_des_giro)
	
	if lds_datastore.getitemstring(ll_i, 17) = "S" then
		dw_gen_fat_ven.setitem(ll_riga, "confermata","S")
	end if
	
next

destroy lds_datastore
dw_gen_fat_ven.setredraw(true)

dw_gen_fat_ven.Reset_DW_Modified(c_ResetChildren)
dw_ext_sel_gen_fat_ven.Reset_DW_Modified(c_ResetChildren)


end event

event buttonclicked;call super::buttonclicked;long ll_index, ll_rows
string ls_seleziona

choose case dwo.name
	case "b_sel"
		ll_rows = rowcount()
		
		if object.b_sel.text = "Tutte" then
			object.b_sel.text = "Nessuna"
			ls_seleziona = "S"
		else
			object.b_sel.text = "Tutte"
			ls_seleziona = "N"
		end if
		
		for ll_index=1 to ll_rows
			if getitemstring(ll_index, "confermata")="S" then &
					setitem(ll_index, "selezionata", ls_seleziona)
		next
		
end choose
end event

