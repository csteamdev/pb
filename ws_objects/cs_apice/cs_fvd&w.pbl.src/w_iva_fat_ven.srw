﻿$PBExportHeader$w_iva_fat_ven.srw
$PBExportComments$Finestra Totali Iva Fattura Vendita
forward
global type w_iva_fat_ven from w_cs_xx_principale
end type
type dw_iva_fat_ven from uo_cs_xx_dw within w_iva_fat_ven
end type
end forward

global type w_iva_fat_ven from w_cs_xx_principale
int Width=3287
int Height=1149
boolean TitleBar=true
string Title="Iva Vendite"
dw_iva_fat_ven dw_iva_fat_ven
end type
global w_iva_fat_ven w_iva_fat_ven

event pc_setwindow;call super::pc_setwindow;dw_iva_fat_ven.set_dw_key("cod_azienda")
dw_iva_fat_ven.set_dw_key("anno_registrazione")
dw_iva_fat_ven.set_dw_key("num_registrazione")
dw_iva_fat_ven.set_dw_key("prog_scadenza")

dw_iva_fat_ven.set_dw_options(sqlca, &
                               i_openparm, &
                               c_scrollparent, &
                               c_default)

end event

on w_iva_fat_ven.create
int iCurrent
call w_cs_xx_principale::create
this.dw_iva_fat_ven=create dw_iva_fat_ven
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=dw_iva_fat_ven
end on

on w_iva_fat_ven.destroy
call w_cs_xx_principale::destroy
destroy(this.dw_iva_fat_ven)
end on

type dw_iva_fat_ven from uo_cs_xx_dw within w_iva_fat_ven
int X=23
int Y=21
int Width=3201
int Height=1001
int TabOrder=20
string DataObject="d_iva_fat_ven"
BorderStyle BorderStyle=StyleLowered!
boolean VScrollBar=true
end type

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_scadenza
datetime ldt_data_scadenza

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if
next

end event

event pcd_retrieve;call super::pcd_retrieve;long   l_errore, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	
l_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)

if l_Errore < 0 then
   pcca.error = c_Fatal
end if

end event

