﻿$PBExportHeader$w_prod_mancanti.srw
forward
global type w_prod_mancanti from w_cs_xx_principale
end type
type cb_2 from commandbutton within w_prod_mancanti
end type
type cb_1 from commandbutton within w_prod_mancanti
end type
type dw_prod_mancanti from uo_cs_xx_dw within w_prod_mancanti
end type
end forward

global type w_prod_mancanti from w_cs_xx_principale
integer width = 3095
integer height = 1840
string title = "Report Prodotti M."
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
cb_2 cb_2
cb_1 cb_1
dw_prod_mancanti dw_prod_mancanti
end type
global w_prod_mancanti w_prod_mancanti

type variables
long il_anno, il_num
end variables

on w_prod_mancanti.create
int iCurrent
call super::create
this.cb_2=create cb_2
this.cb_1=create cb_1
this.dw_prod_mancanti=create dw_prod_mancanti
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_2
this.Control[iCurrent+2]=this.cb_1
this.Control[iCurrent+3]=this.dw_prod_mancanti
end on

on w_prod_mancanti.destroy
call super::destroy
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.dw_prod_mancanti)
end on

event pc_setwindow;call super::pc_setwindow;string ls_nascondi

dw_prod_mancanti.set_dw_options(sqlca, &
                                pcca.null_object, &
                                c_nonew + &
                                c_nomodify + &
                                c_nodelete + &
                                c_noenablenewonopen + &
                                c_noenablemodifyonopen + &
                                c_scrollparent + &
										  c_disablecc, &
										  c_noresizedw + &
                                c_nohighlightselected + &
                                c_nocursorrowfocusrect + &
                                c_nocursorrowpointer)
										  
il_anno = s_cs_xx.parametri.parametro_d_1

il_num = s_cs_xx.parametri.parametro_d_2

select flag
into   :ls_nascondi
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'NTM';
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura parametro NTM da parametri_azienda: " + sqlca.sqlerrtext)
	ls_nascondi = "N"
elseif sqlca.sqlcode = 100 or isnull(ls_nascondi) then
	ls_nascondi = "N"
end if

if ls_nascondi = "S" then
	dw_prod_mancanti.object.cf_val_riga.visible = 0
	dw_prod_mancanti.object.cf_totale.visible = 0
end if

dw_prod_mancanti.object.datawindow.print.preview = 'Yes'
dw_prod_mancanti.object.datawindow.print.preview.rulers = 'Yes'
end event

type cb_2 from commandbutton within w_prod_mancanti
integer x = 2309
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Chiudi"
end type

event clicked;close(parent)
end event

type cb_1 from commandbutton within w_prod_mancanti
integer x = 2697
integer y = 1660
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Stampa"
end type

event clicked;dw_prod_mancanti.print()
end event

type dw_prod_mancanti from uo_cs_xx_dw within w_prod_mancanti
integer x = 23
integer y = 20
integer width = 3040
integer height = 1620
integer taborder = 10
string dataobject = "d_prod_mancanti"
boolean hscrollbar = true
boolean vscrollbar = true
borderstyle borderstyle = stylelowered!
end type

event pcd_retrieve;call super::pcd_retrieve;retrieve(s_cs_xx.cod_azienda,il_anno,il_num)
end event

