﻿$PBExportHeader$w_report_avviso_contrassegno.srw
forward
global type w_report_avviso_contrassegno from w_cs_xx_principale
end type
type dw_contrassegno from datawindow within w_report_avviso_contrassegno
end type
type cb_nota from commandbutton within w_report_avviso_contrassegno
end type
type cb_chiudi from commandbutton within w_report_avviso_contrassegno
end type
type cb_stampa from commandbutton within w_report_avviso_contrassegno
end type
type st_label_azioni from statictext within w_report_avviso_contrassegno
end type
type rb_webfax from radiobutton within w_report_avviso_contrassegno
end type
type rb_mail from radiobutton within w_report_avviso_contrassegno
end type
type rb_scegli from radiobutton within w_report_avviso_contrassegno
end type
type rb_predefinita from radiobutton within w_report_avviso_contrassegno
end type
type r_azioni from rectangle within w_report_avviso_contrassegno
end type
end forward

global type w_report_avviso_contrassegno from w_cs_xx_principale
integer width = 3950
integer height = 2724
string title = "Stampa Contrassegno"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
dw_contrassegno dw_contrassegno
cb_nota cb_nota
cb_chiudi cb_chiudi
cb_stampa cb_stampa
st_label_azioni st_label_azioni
rb_webfax rb_webfax
rb_mail rb_mail
rb_scegli rb_scegli
rb_predefinita rb_predefinita
r_azioni r_azioni
end type
global w_report_avviso_contrassegno w_report_avviso_contrassegno

type variables
//può essere fat_ven o bol_ven
string is_tipo_doc

long il_anno_doc, il_num_doc

//se i relativi flag nell'anagrafica clienti sono attivati
//questa variabile contiene la datawindow del report del documento
//da allegare assieme al contrassegno
datawindow idw_report_doc
boolean ib_invio_aut = false
end variables

forward prototypes
public function integer wf_impostazioni (string fs_testo, string fs_path_logo_intestazione)
public function integer wf_email (ref string fs_msg)
public function integer wf_webfax (ref string fs_msg)
public function integer wf_salva_contrassegno (string fs_path, string fs_tipo_invio, ref string fs_msg)
public function integer wf_crea_pdf (ref string fs_path, string fs_nome_file, datawindow fdw_data, ref string fs_msg)
public function integer wf_invia_contrassegno (string fs_path_pdf, string fs_path_report, string fs_tipo_invio, string fs_cod_webfax, ref string fs_msg)
public function integer wf_invia_webfax (string fs_cod_webfax, string fs_fax_cliente, string fs_allegati[], string fs_num_fiscale_doc, ref string fs_msg)
end prototypes

public function integer wf_impostazioni (string fs_testo, string fs_path_logo_intestazione);string ls_path_logo, ls_modify, ls_localita_azienda, ls_cod_cliente, &
		ls_rag_soc_1, ls_indirizzo, ls_cap, ls_localita, ls_provincia
long ll_row

dw_contrassegno.reset()

/*
//metti il logo -------------------------------------------------
if is_tipo_doc="fat_ven" then
	select stringa
	into   :ls_path_logo
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and &
			 flag_parametro = 'S' and &
			 cod_parametro = 'LO1';	
	
elseif is_tipo_doc="bol_ven" then
	select stringa
	into   :ls_path_logo
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and &
	       flag_parametro = 'S' and &
	       cod_parametro = 'LO3';

end if

ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo + "'~t"
*/

ls_modify = "intestazione.filename='" +fs_path_logo_intestazione + "'~t"
dw_contrassegno.modify(ls_modify)
//-------------------------------------------------------------

//compilo i campi
ll_row = dw_contrassegno.insertrow(0)

cb_nota.text = "MODIFICA NOTA"
dw_contrassegno.object.cf_testo_contrassegno.visible = true
dw_contrassegno.object.testo_contrassegno.visible = false
dw_contrassegno.object.datawindow.print.preview = "Yes"

//localita dell'azienda -----------------------------------------------------------------------
select localita
into :ls_localita_azienda
from aziende
where cod_azienda=:s_cs_xx.cod_azienda;

dw_contrassegno.setitem(ll_row, "localita_azienda", ls_localita_azienda)


//campi cliente --------------------------------------------------------------------------------
if is_tipo_doc="fat_ven" then
	select cod_cliente
	into :ls_cod_cliente
	from tes_fat_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:il_anno_doc and
				num_registrazione=:il_num_doc;
	
elseif is_tipo_doc="bol_ven" then
	select cod_cliente
	into :ls_cod_cliente
	from tes_bol_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:il_anno_doc and
				num_registrazione=:il_num_doc;
end if

select 	rag_soc_1,
			indirizzo,
			cap,
			localita,
			provincia
into		:ls_rag_soc_1, 
			:ls_indirizzo, 
			:ls_cap, 
			:ls_localita, 
			:ls_provincia
from anag_clienti
where cod_azienda=:s_cs_xx.cod_azienda and
			cod_cliente=:ls_cod_cliente;

dw_contrassegno.setitem(ll_row, "rag_soc_1_cliente", ls_rag_soc_1)
dw_contrassegno.setitem(ll_row, "indirizzo_cliente", ls_indirizzo)
dw_contrassegno.setitem(ll_row, "cap_cliente", ls_cap)
dw_contrassegno.setitem(ll_row, "localita_cliente", ls_localita)
dw_contrassegno.setitem(ll_row, "provincia_cliente", ls_provincia)


//testo standard ----------------------------------------------------------------------------
fs_testo += "~n~r"
dw_contrassegno.setitem(ll_row, "testo_contrassegno", fs_testo)


return 1
end function

public function integer wf_email (ref string fs_msg);string ls_path, ls_path_report, ls_nome_file
long ll_ret

//crea file pdf del contrassegno
ls_nome_file = "contrassegno"
ll_ret = wf_crea_pdf(ls_path, ls_nome_file, dw_contrassegno, fs_msg)

if ll_ret<0 then
	//errore in creazione pdf: in msg l'errore
	return -1
end if

ls_path_report = ""
if ib_invio_aut then
	
	if is_tipo_doc="bol_ven" then
		ls_nome_file = "DDT"
		
	elseif is_tipo_doc="fat_ven" then
		ls_nome_file = "fattura"
	end if
	
	//crea file pdf del report del documento che va allegato assieme al contrassegno
	ll_ret = wf_crea_pdf(ls_path_report, ls_nome_file, idw_report_doc, fs_msg)
	
	if ll_ret<0 then
		//errore in creazione pdf: in msg l'errore
		fs_msg = "rpt doc - " + fs_msg
		return -1
	end if
end if

ll_ret = wf_invia_contrassegno(ls_path, ls_path_report, "M", "", fs_msg)
if ll_ret<0 then
	//errore in creazione pdf: in msg l'errore
	filedelete(ls_path)
	
	if ib_invio_aut then
		filedelete(ls_path_report)
	end if
	
	return -1
end if

ll_ret =wf_salva_contrassegno(ls_path, "M", fs_msg)
if ll_ret<0 then
	//errore in creazione pdf: in msg l'errore
	filedelete(ls_path)
	
	if ib_invio_aut then
		filedelete(ls_path_report)
	end if
	
	return -1
end if

filedelete(ls_path)
if ib_invio_aut then
	filedelete(ls_path_report)
end if

return 1
end function

public function integer wf_webfax (ref string fs_msg);string ls_path, ls_nome_documento, ls_path_report, ls_cod_webfax
long ll_ret

//apri la selezione del webfax
open(w_seleziona_webfax)

ls_cod_webfax = message.stringparm
if ls_cod_webfax="" or isnull(ls_cod_webfax) then
	//selezione annullata
	return 0
end if
//------

//crea file pdf
ls_nome_documento = "contrassegno"
ll_ret = wf_crea_pdf(ls_path, ls_nome_documento, dw_contrassegno, fs_msg)

if ll_ret<0 then
	//errore in creazione pdf: in msg l'errore
	return -1
end if

//-----
ls_path_report = ""
if ib_invio_aut then
	
	if is_tipo_doc="bol_ven" then
		ls_nome_documento = "DDT"
		
	elseif is_tipo_doc="fat_ven" then
		ls_nome_documento = "fattura"
	end if
	
	//crea file pdf del report del documento che va allegato assieme al contrassegno
	ll_ret = wf_crea_pdf(ls_path_report, ls_nome_documento, idw_report_doc, fs_msg)
	
	if ll_ret<0 then
		//errore in creazione pdf: in msg l'errore
		fs_msg = "rpt doc - " + fs_msg
		return -1
	end if
end if

//------
ll_ret = wf_invia_contrassegno(ls_path, ls_path_report, "F", ls_cod_webfax, fs_msg)

if ll_ret<0 then
	//errore in creazione pdf: in msg l'errore
	filedelete(ls_path)
	
	if ib_invio_aut then
		filedelete(ls_path_report)
	end if
	
	return -1
end if


ll_ret =wf_salva_contrassegno(ls_path, "F", fs_msg)

//sleep(5)

if ll_ret<0 then
	//errore in creazione pdf: in msg l'errore
	filedelete(ls_path)
	
	if ib_invio_aut then
		filedelete(ls_path_report)
	end if
	
	return -1
end if

/*
filedelete(ls_path)
if ib_invio_aut then
	filedelete(ls_path_report)
end if
*/

return 1
end function

public function integer wf_salva_contrassegno (string fs_path, string fs_tipo_invio, ref string fs_msg);//il parametro fs_tipo_invio può essere "M" (mail) o "F" (web fax)


long ll_ret, ll_prog_mimytype
blob l_blob
string ls_cod_nota, ls_note, ls_nota, ls_documento

// salvo il flag contrassegno inviato

if is_tipo_doc = "bol_ven" then
	update tes_bol_ven
	set    flag_contrassegno_inviato = 'S'
	where  cod_azienda = :s_cs_xx.cod_azienda and	
			 anno_registrazione = :il_anno_doc and
			 num_registrazione = :il_num_doc;
	
	ls_documento = "DDT"
	
elseif is_tipo_doc = "fat_ven" then
	update tes_fat_ven
	set    flag_contrassegno_inviato = 'S'
	where  cod_azienda = :s_cs_xx.cod_azienda and	
			 anno_registrazione = :il_anno_doc and
			 num_registrazione = :il_num_doc;

	ls_documento = "Fattura"
end if

if sqlca.sqlcode<0 then
	fs_msg = "Errore in salvataggio flag contrassegno inviato ~r~n"+sqlca.sqlerrtext
	return -1
end if

//	wf_memorizza_blob(il_anno[il_corrente], il_num[il_corrente], ls_path)

ll_ret = f_file_to_blob(fs_path, l_blob)
if ll_ret < 0 then return -1

ll_ret = len(l_blob)

select prog_mimetype  
into   :ll_prog_mimytype  
from   tab_mimetype  
where  cod_azienda = :s_cs_xx.cod_azienda and
		 estensione = 'PDF';

if sqlca.sqlcode = 100 then
	fs_msg = "Impossibile archiviare il documento: impostare il mimetype PDF"
	return -1
end if

//preparazione dati per il salvataggio nella relativa tabella note (DDT o Fattura) -----------------------------------------

//ls_documento può essere "DDT" o "Fattura"
if fs_tipo_invio="M" then
	//invio tramite mail
	ls_nota = "E-mail contrassegno "+ls_documento
else
	//invio tramite web fax
	ls_nota = "WebFax contrassegno "+ls_documento
end if

ls_note = "Contrass. "+ls_documento + " inviato dall'utente " + s_cs_xx.cod_utente + "~r~n" + &
          "Data invio:" + string(today(), "dd/mm/yyyy") + "  Ora invio:" + string(now(), "hh:mm:ss")
//----------------------------------------------------------------------------------------------------------------------------------

setnull(ls_cod_nota)
//per il cod_nota facciamo una lettera ("C" per contrassegno) più un progressivo di 2 cifre
//esempio C01
if is_tipo_doc = "bol_ven" then
	select max(cod_nota)
	into   :ls_cod_nota
	from   tes_bol_ven_note
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_doc and
			 num_registrazione = :il_num_doc and
			 cod_nota like 'C%';
			 
	if sqlca.sqlcode<0 then
		fs_msg = "Errore in lettura max cod nota: ~r~n"+sqlca.sqlerrtext
		return -1
	end if
			 
	if isnull(ls_cod_nota) or len(ls_cod_nota) < 1 then
		ls_cod_nota = "C01"
	else
		ls_cod_nota = right(ls_cod_nota, 2)
		ll_ret = long(ls_cod_nota)
		ll_ret += 1
		ls_cod_nota = "C" + string(ll_ret, "00")
	end if
	
	insert into tes_bol_ven_note  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  cod_nota,   
		  des_nota, 
		  note,
		  prog_mimetype)  
	values ( :s_cs_xx.cod_azienda,   
			  :il_anno_doc,   
			  :il_num_doc,   
			  :ls_cod_nota,   
			  :ls_nota,   
			  :ls_note,
			  :ll_prog_mimytype );
	
	if sqlca.sqlcode<0 then
		fs_msg = "Errore in inserimento allegato (INSERT): ~r~n"+sqlca.sqlerrtext
		return -1
	end if
	
	updateblob tes_bol_ven_note
	set        note_esterne = :l_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
				  anno_registrazione = :il_anno_doc and
				  num_registrazione = :il_num_doc and
				  cod_nota = :ls_cod_nota;
	
	if sqlca.sqlcode<0 then
		fs_msg = "Errore in inserimento allegato (UPD BLOB): ~r~n"+sqlca.sqlerrtext
		return -1
	end if
	
elseif  is_tipo_doc = "fat_ven" then
	
	select max(cod_nota)
	into   :ls_cod_nota
	from   tes_fat_ven_note
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_doc and
			 num_registrazione = :il_num_doc and
			 cod_nota like 'C%';
			 
	if sqlca.sqlcode<0 then
		fs_msg = "Errore in lettura max cod nota: ~r~n"+sqlca.sqlerrtext
		return -1
	end if
			 
	if isnull(ls_cod_nota) or len(ls_cod_nota) < 1 then
		ls_cod_nota = "C01"
	else
		ls_cod_nota = right(ls_cod_nota, 2)
		ll_ret = long(ls_cod_nota)
		ll_ret += 1
		ls_cod_nota = "C" + string(ll_ret, "00")
	end if
	
	insert into tes_fat_ven_note  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  cod_nota,   
		  des_nota, 
		  note,
		  prog_mimetype)  
	values ( :s_cs_xx.cod_azienda,   
			  :il_anno_doc,   
			  :il_num_doc,   
			  :ls_cod_nota,   
			  :ls_nota,   
			  :ls_note,
			  :ll_prog_mimytype );
	
	if sqlca.sqlcode<0 then
		fs_msg = "Errore in inserimento allegato (INSERT): ~r~n"+sqlca.sqlerrtext
		return -1
	end if
	
	updateblob tes_fat_ven_note
	set        note_esterne = :l_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
				  anno_registrazione = :il_anno_doc and
				  num_registrazione = :il_num_doc and
				  cod_nota = :ls_cod_nota;
	
	if sqlca.sqlcode<0 then
		fs_msg = "Errore in inserimento allegato (UPD BLOB): ~r~n"+sqlca.sqlerrtext
		return -1
	end if
	
end if

return 1
end function

public function integer wf_crea_pdf (ref string fs_path, string fs_nome_file, datawindow fdw_data, ref string fs_msg);uo_archivia_pdf luo_pdf
luo_pdf = create uo_archivia_pdf

if pos(fs_nome_file, "contrassegno")>0 then
	fs_path = luo_pdf.uof_crea_pdf_path(fdw_data, fs_nome_file)
else
	fs_path = luo_pdf.uof_crea_pdf_path(fdw_data, true, fs_nome_file)
end if

if fs_path = "errore" then
	fs_msg = "Errore nella creazione del file pdf."
	fs_path = ""
	destroy luo_pdf;
	return -1
end if

destroy luo_pdf;

return 1
end function

public function integer wf_invia_contrassegno (string fs_path_pdf, string fs_path_report, string fs_tipo_invio, string fs_cod_webfax, ref string fs_msg);//fs_tipo_invio può essere "M" (email) o "F" (webfax)
//nel caso "F" non considerare la colonna email_amministrazione in anagrafica clienti

uo_outlook luo_outlook
string ls_cod_cliente, ls_email_dest, ls_destinatari[], ls_allegati[], ls_rag_soc_1, ls_oggetto_mail, ls_testo, ls_testo_2, &
			ls_fax_cliente, ls_num_fiscale_doc
long  ll_anno_doc_fiscale, ll_num_doc_fiscale
datetime ldt_data_doc_fiscale
integer li_ret

//leggo info per il testo email --------------------------------
if is_tipo_doc = "bol_ven" then
	select 	tes_bol_ven.cod_cliente,
				tes_bol_ven.anno_documento,
			 	tes_bol_ven.num_documento,
			 	tes_bol_ven.data_bolla,
				anag_clienti.rag_soc_1,
				anag_clienti.email_amministrazione,
				anag_clienti.fax
	into 		:ls_cod_cliente,
				:ll_anno_doc_fiscale,
			 	:ll_num_doc_fiscale,
				:ldt_data_doc_fiscale,
				:ls_rag_soc_1,
				:ls_email_dest,
				:ls_fax_cliente
	from tes_bol_ven
	join anag_clienti on anag_clienti.cod_azienda=tes_bol_ven.cod_azienda and
							anag_clienti.cod_cliente=tes_bol_ven.cod_cliente
	where 	tes_bol_ven.cod_azienda=:s_cs_xx.cod_azienda and
				tes_bol_ven.anno_registrazione=:il_anno_doc and
				tes_bol_ven.num_registrazione=:il_num_doc;
				
elseif is_tipo_doc = "fat_ven" then
	
	select 	tes_fat_ven.cod_cliente,
				tes_fat_ven.anno_documento,
			 	tes_fat_ven.num_documento,
			 	tes_fat_ven.data_fattura,
				anag_clienti.rag_soc_1,
				anag_clienti.email_amministrazione,
				anag_clienti.fax
	into 		:ls_cod_cliente,
				:ll_anno_doc_fiscale,
			 	:ll_num_doc_fiscale,
				:ldt_data_doc_fiscale,
				:ls_rag_soc_1,
				:ls_email_dest,
				:ls_fax_cliente
	from tes_fat_ven
	join anag_clienti on anag_clienti.cod_azienda=tes_fat_ven.cod_azienda and
							anag_clienti.cod_cliente=tes_fat_ven.cod_cliente
	where 	tes_fat_ven.cod_azienda=:s_cs_xx.cod_azienda and
				tes_fat_ven.anno_registrazione=:il_anno_doc and
				tes_fat_ven.num_registrazione=:il_num_doc;
				
end if

if sqlca.sqlcode<0 then
	fs_msg = "Errore in lettura informazioni su documento: ~r~n"+sqlca.sqlerrtext
	return -1
end if

if ls_email_dest="" or isnull(ls_email_dest) and fs_tipo_invio="M" then
	fs_msg = "Impossibile inviare e-mail: indirizzo amministrazione cliente non specificato in anagrafica!"
	return -1
end if

//gli allegati servono sia nel caso mail che webfax
ls_allegati[1]    = fs_path_pdf

ls_testo_2 = ""
if fs_path_report<>"" and not isnull(fs_path_report) and ib_invio_aut then
	ls_allegati[2]    = fs_path_report
	ls_testo_2 = "~r~n~r~nIn allegato anche copia del documento."
end if

if is_tipo_doc="bol_ven" then
	ls_num_fiscale_doc= "DDT"
	
elseif is_tipo_doc = "fat_ven" then
	ls_num_fiscale_doc= "Fattura"
	
end if

ls_num_fiscale_doc += " " + string(ll_anno_doc_fiscale) + "/" + string(ll_num_doc_fiscale)
if not isnull(ldt_data_doc_fiscale) then ls_num_fiscale_doc += " del "+string(ldt_data_doc_fiscale,"dd/mm/yyyy")

if fs_tipo_invio="F" then
	if wf_invia_webfax(fs_cod_webfax, ls_fax_cliente, ls_allegati[], ls_num_fiscale_doc, fs_msg) < 0 then
		//in fs_msg il messaggio di errore
		return -1
	end if
	
	//se arrivi fin qui esci correttamente, perchè il seguito viene fatto solo in caso mail e non in caso webfax
	return 1
end if


//invio mail ---------------------------------------------------------------------------
luo_outlook = CREATE uo_outlook

ls_destinatari[1] = ls_email_dest

ls_oggetto_mail = ls_rag_soc_1+" - Avviso di contrassegno per "
ls_testo = "SPETT.LE "+ ls_rag_soc_1+"~r~n"+"~r~n"
ls_testo += "Trasmettiamo in allegato avviso di contrassegno relativo a~r~n"+"~r~n"

if is_tipo_doc="bol_ven" then
	ls_oggetto_mail+= " DDT"
	ls_testo += " DDT"
	
elseif is_tipo_doc = "fat_ven" then
	ls_oggetto_mail+= " Fattura"
	ls_testo += " Fattura"
	
end if

ls_oggetto_mail += " nr. " + string(ll_anno_doc_fiscale) + "/" + string(ll_num_doc_fiscale)
ls_testo += " nr. " + string(ll_anno_doc_fiscale) + "/" + string(ll_num_doc_fiscale)

if not isnull(ldt_data_doc_fiscale) then
	ls_oggetto_mail += " del " + string(ldt_data_doc_fiscale,"dd/mm/yyyy")
	ls_testo += " del " + string(ldt_data_doc_fiscale,"dd/mm/yyyy")
end if

ls_testo += ls_testo_2

ls_testo += "~r~n~r~n~r~nDistinti Saluti"

// stefanop 16/02/2012: sostituito metodo con l'invio sendmail 
// ------------------------------------------------------------------------------------------------------
//li_ret = luo_outlook.uof_invio_outlook_redemption( 0, &
//														"M",  &
//														ls_oggetto_mail, &
//														ls_testo, &
//														ls_destinatari[], &
//														ls_allegati[], &
//														true, &
//														ref fs_msg)

if not luo_outlook.uof_invio_sendmail( ls_destinatari[], ls_oggetto_mail, ls_testo, ls_allegati[], ref fs_msg) then
//if li_ret <> 0 then
	fs_msg = "Errore in fase di invio documento~r~n" + fs_msg
	destroy luo_outlook;
	return -1	
end if
	
destroy luo_outlook

return 1
end function

public function integer wf_invia_webfax (string fs_cod_webfax, string fs_fax_cliente, string fs_allegati[], string fs_num_fiscale_doc, ref string fs_msg);uo_webfax luo_webfax
long ll_ret
string ls_testo


if fs_fax_cliente="" or isnull(fs_fax_cliente) then
	fs_msg = "Numero fax cliente del documento non specificato in anagrafica!"
	return -1
end if

ls_testo = ""	//lasciamo il testo del messaggio vuoto

luo_webfax = create uo_webfax
ll_ret = luo_webfax.uof_invia_webfax(fs_cod_webfax, ls_testo, fs_allegati[], fs_fax_cliente, fs_num_fiscale_doc, fs_msg)

if ll_ret < 0 then
	//in fs_msg il messaggio di errore
	
	destroy luo_webfax;
	return -1
end if

destroy luo_webfax;

return 1
end function

event pc_setwindow;call super::pc_setwindow;string ls_testo_standard, ls_vuoto[], ls_path_logo_intestazione

//recupero variabili
is_tipo_doc = s_cs_xx.parametri.parametro_s_1_a[1]
ls_testo_standard = s_cs_xx.parametri.parametro_s_1_a[2]
ls_path_logo_intestazione = s_cs_xx.parametri.parametro_s_1_a[3]
ib_invio_aut = s_cs_xx.parametri.parametro_b_1

if ib_invio_aut then
	idw_report_doc = s_cs_xx.parametri.parametro_dw_1
end if

il_anno_doc = s_cs_xx.parametri.parametro_d_1
il_num_doc  = s_cs_xx.parametri.parametro_d_2
//-------------------------------------


//ripulisci le variabili ----------------
s_cs_xx.parametri.parametro_s_1_a = ls_vuoto
setnull(s_cs_xx.parametri.parametro_d_1)
setnull(s_cs_xx.parametri.parametro_d_2)
setnull(s_cs_xx.parametri.parametro_dw_1)
s_cs_xx.parametri.parametro_b_1 = false
//-------------------------------------

set_w_options(c_noresizewin)
save_on_close(c_socnosave)

wf_impostazioni(ls_testo_standard, ls_path_logo_intestazione)

end event

on w_report_avviso_contrassegno.create
int iCurrent
call super::create
this.dw_contrassegno=create dw_contrassegno
this.cb_nota=create cb_nota
this.cb_chiudi=create cb_chiudi
this.cb_stampa=create cb_stampa
this.st_label_azioni=create st_label_azioni
this.rb_webfax=create rb_webfax
this.rb_mail=create rb_mail
this.rb_scegli=create rb_scegli
this.rb_predefinita=create rb_predefinita
this.r_azioni=create r_azioni
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_contrassegno
this.Control[iCurrent+2]=this.cb_nota
this.Control[iCurrent+3]=this.cb_chiudi
this.Control[iCurrent+4]=this.cb_stampa
this.Control[iCurrent+5]=this.st_label_azioni
this.Control[iCurrent+6]=this.rb_webfax
this.Control[iCurrent+7]=this.rb_mail
this.Control[iCurrent+8]=this.rb_scegli
this.Control[iCurrent+9]=this.rb_predefinita
this.Control[iCurrent+10]=this.r_azioni
end on

on w_report_avviso_contrassegno.destroy
call super::destroy
destroy(this.dw_contrassegno)
destroy(this.cb_nota)
destroy(this.cb_chiudi)
destroy(this.cb_stampa)
destroy(this.st_label_azioni)
destroy(this.rb_webfax)
destroy(this.rb_mail)
destroy(this.rb_scegli)
destroy(this.rb_predefinita)
destroy(this.r_azioni)
end on

type dw_contrassegno from datawindow within w_report_avviso_contrassegno
integer x = 32
integer y = 268
integer width = 3877
integer height = 2352
integer taborder = 30
string title = "none"
string dataobject = "d_report_avviso_contrassegno"
boolean vscrollbar = true
boolean livescroll = true
end type

type cb_nota from commandbutton within w_report_avviso_contrassegno
integer x = 3031
integer y = 148
integer width = 466
integer height = 84
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Modifica Nota"
end type

event clicked;if UPPER(this.text) = "MODIFICA NOTA" then
	dw_contrassegno.object.cf_testo_contrassegno.visible = false
	dw_contrassegno.object.testo_contrassegno.visible = true
	
	dw_contrassegno.object.testo_contrassegno.protect = 0
	dw_contrassegno.object.testo_contrassegno.tabsequence = 10
	
	this.text = "CONFERMA NOTA"
	dw_contrassegno.object.datawindow.print.preview = "No"
	
	//metti in silver
	dw_contrassegno.object.datawindow.color = rgb(192, 192, 192)
	
	dw_contrassegno.setfocus( )
else
	dw_contrassegno.accepttext()
	this.text = "MODIFICA NOTA"
	dw_contrassegno.object.cf_testo_contrassegno.visible = true
	dw_contrassegno.object.testo_contrassegno.visible = false
	
	dw_contrassegno.object.testo_contrassegno.protect = 1
	dw_contrassegno.object.datawindow.print.preview = "Yes"
	
	//metti in bianco
	dw_contrassegno.object.datawindow.color = rgb(255, 255, 255)
end if
end event

type cb_chiudi from commandbutton within w_report_avviso_contrassegno
integer x = 3479
integer y = 44
integer width = 430
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Chiudi"
end type

event clicked;close(parent)
end event

type cb_stampa from commandbutton within w_report_avviso_contrassegno
integer x = 3031
integer y = 48
integer width = 430
integer height = 84
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Stampa"
end type

event clicked;string ls_msg
long ll_ret, ll_job

if rb_webfax.checked then
	ll_ret = wf_webfax(ls_msg)
	
elseif rb_mail.checked then
	ll_ret = wf_email(ls_msg)
	
elseif rb_scegli.checked then
	//scegli dove stampare
	dw_contrassegno.print(true, true)
	
//	if ib_invio_aut then
//		idw_report_doc.print(true, true)
//	end if
	ll_ret = 1
	
else
	//stampa su stampante predefinita
	dw_contrassegno.print(true, false)
	
	ll_ret = 1
	
end if


if ll_ret<0 then
	//in ls_msg il messaggio di errore
	g_mb.error(ls_msg)
	rollback;
	return
elseif ll_ret=0 then
	rollback;
	return
	
else
	//OK
	commit;
	close(parent)
end if
end event

type st_label_azioni from statictext within w_report_avviso_contrassegno
integer x = 78
integer y = 24
integer width = 352
integer height = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Stampa su"
alignment alignment = center!
boolean focusrectangle = false
end type

type rb_webfax from radiobutton within w_report_avviso_contrassegno
integer x = 2487
integer y = 100
integer width = 398
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Web Fax"
end type

event clicked;cb_stampa.text = "Invia web-fax"
end event

type rb_mail from radiobutton within w_report_avviso_contrassegno
integer x = 1897
integer y = 100
integer width = 379
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "E-mail"
end type

event clicked;cb_stampa.text = "Invia e-mail"
end event

type rb_scegli from radiobutton within w_report_avviso_contrassegno
integer x = 1038
integer y = 100
integer width = 663
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Scegli Stampante"
end type

event clicked;cb_stampa.text = "Stampa"
end event

type rb_predefinita from radiobutton within w_report_avviso_contrassegno
integer x = 270
integer y = 100
integer width = 709
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Stampante predefinita"
boolean checked = true
end type

event clicked;cb_stampa.text = "Stampa"
end event

type r_azioni from rectangle within w_report_avviso_contrassegno
long linecolor = 33554432
integer linethickness = 4
long fillcolor = 12632256
integer x = 41
integer y = 48
integer width = 2894
integer height = 176
end type

