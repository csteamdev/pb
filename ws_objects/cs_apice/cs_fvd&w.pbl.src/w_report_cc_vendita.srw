﻿$PBExportHeader$w_report_cc_vendita.srw
$PBExportComments$Report centro di costo acquisto
forward
global type w_report_cc_vendita from w_cs_xx_principale
end type
type dw_selezione from uo_cs_xx_dw within w_report_cc_vendita
end type
type cb_annulla from commandbutton within w_report_cc_vendita
end type
type cb_report from commandbutton within w_report_cc_vendita
end type
type cb_selezione from commandbutton within w_report_cc_vendita
end type
type dw_report from uo_cs_xx_dw within w_report_cc_vendita
end type
end forward

global type w_report_cc_vendita from w_cs_xx_principale
integer width = 3707
integer height = 1936
string title = "Report Centri di Costo Vendita"
dw_selezione dw_selezione
cb_annulla cb_annulla
cb_report cb_report
cb_selezione cb_selezione
dw_report dw_report
end type
global w_report_cc_vendita w_report_cc_vendita

type variables
string  is_cc[], is_cp[], is_des_cc[], is_des_cp[]

decimal id_cc[], id_cp[]
end variables

forward prototypes
public function integer wf_carica_cc ()
public function integer wf_carica_cp ()
public function integer wf_aggiungi_importo_cc (string fs_cod_centro_costo, decimal fd_importo_cc, ref integer fi_indice)
public function integer wf_aggiungi_importo_cp (string fs_cod_conto, decimal fd_importo_cp, ref integer fi_indice)
end prototypes

public function integer wf_carica_cc ();string    ls_sql_cc, ls_errore, ls_syntax_cc, ls_vuoto[]

decimal{4}   ld_vuoto[]

integer   li_i

datastore lds_cc

is_cc = ls_vuoto

is_des_cc = ls_vuoto

id_cc = ld_vuoto

ls_sql_cc = "select cod_centro_costo, des_centro_costo from tab_centri_costo where cod_azienda = '" + s_cs_xx.cod_azienda + "' order by cod_centro_costo ASC "

ls_syntax_cc = sqlca.syntaxfromsql( ls_sql_cc, 'style(type=grid)', ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then 
	
	g_mb.messagebox( "APICE", "Errore durante la creazione della sintassi del datastore lds_cc: " + ls_errore)
	
	return -1
end if

lds_cc = create datastore

lds_cc.create(ls_syntax_cc, ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then
	
	destroy lds_cc

	g_mb.messagebox( "APICE", "Errore durante la creazione del datastore lds_cc: " + ls_errore)
	
	return -1
	
end if

lds_cc.settransobject(sqlca)

if lds_cc.retrieve() = -1 then
	
	destroy lds_cc
	
	return -1
	
end if	

for li_i = 1 to lds_cc.rowcount()
	
	is_cc[li_i] = lds_cc.getitemstring( li_i, "cod_centro_costo")
	
	is_des_cc[li_i] = lds_cc.getitemstring( li_i, "des_centro_costo")
	
	id_cc[li_i] = 0
next

destroy lds_cc;

return 0
end function

public function integer wf_carica_cp ();string    ls_sql_cp, ls_errore, ls_syntax_cp, ls_vuoto[]

decimal{4}   ld_vuoto[]

integer   li_i

datastore lds_cp

is_cp = ls_vuoto

is_des_cp = ls_vuoto

id_cp = ld_vuoto

ls_sql_cp = "select cod_conto, des_conto from anag_piano_conti where cod_azienda = '" + s_cs_xx.cod_azienda + "' order by cod_conto ASC "

ls_syntax_cp = sqlca.syntaxfromsql( ls_sql_cp, 'style(type=grid)', ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then 
	
	g_mb.messagebox( "APICE", "Errore durante la creazione della sintassi del datastore lds_cp: " + ls_errore)
	
	return -1
end if

lds_cp = create datastore

lds_cp.create(ls_syntax_cp, ls_errore)

if not isnull(ls_errore) and len(trim(ls_errore)) > 0 then
	
	destroy lds_cp

	g_mb.messagebox( "APICE", "Errore durante la creazione del datastore lds_cp: " + ls_errore)
	
	return -1
	
end if

lds_cp.settransobject(sqlca)

if lds_cp.retrieve() = -1 then
	
	destroy lds_cp
	
	return -1
	
end if	

for li_i = 1 to lds_cp.rowcount()
	
	is_cp[li_i] = lds_cp.getitemstring( li_i, "cod_conto")
	
	is_des_cp[li_i] = lds_cp.getitemstring( li_i, "des_conto")
	
	id_cp[li_i] = 0
next

destroy lds_cp;

return 0
end function

public function integer wf_aggiungi_importo_cc (string fs_cod_centro_costo, decimal fd_importo_cc, ref integer fi_indice);integer li_i

if fs_cod_centro_costo = "" or isnull(fs_cod_centro_costo) then return 0

fi_indice = 0

if isnull(fd_importo_cc) then fd_importo_cc = 0

for li_i = 1 to UpperBound(is_cc)
	
	if is_cc[li_i] = fs_cod_centro_costo then
		
		id_cc[li_i] = id_cc[li_i] + fd_importo_cc
		
		fi_indice = li_i
		
		exit
		
	end if
next

return 0
end function

public function integer wf_aggiungi_importo_cp (string fs_cod_conto, decimal fd_importo_cp, ref integer fi_indice);integer li_i

if fs_cod_conto = "" or isnull(fs_cod_conto) then return 0

fi_indice = 0

if isnull(fd_importo_cp) then fd_importo_cp = 0

for li_i = 1 to UpperBound(is_cp)
	
	if is_cp[li_i] = fs_cod_conto then
		
		id_cp[li_i] = id_cp[li_i] + fd_importo_cp
		
		fi_indice = li_i
		
		exit
		
	end if
next

return 0
end function

on w_report_cc_vendita.create
int iCurrent
call super::create
this.dw_selezione=create dw_selezione
this.cb_annulla=create cb_annulla
this.cb_report=create cb_report
this.cb_selezione=create cb_selezione
this.dw_report=create dw_report
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_selezione
this.Control[iCurrent+2]=this.cb_annulla
this.Control[iCurrent+3]=this.cb_report
this.Control[iCurrent+4]=this.cb_selezione
this.Control[iCurrent+5]=this.dw_report
end on

on w_report_cc_vendita.destroy
call super::destroy
destroy(this.dw_selezione)
destroy(this.cb_annulla)
destroy(this.cb_report)
destroy(this.cb_selezione)
destroy(this.dw_report)
end on

event pc_setwindow;call super::pc_setwindow;dw_report.ib_dw_report = true

set_w_options(c_closenosave + c_autoposition + c_noresizewin)

dw_report.set_dw_options(sqlca, &
                         pcca.null_object, &
                         c_nomodify + &
                         c_nodelete + &
                         c_newonopen + &
                         c_disableCC, &
                         c_noresizedw + &
                         c_nohighlightselected + &
                         c_nocursorrowpointer +&
                         c_nocursorrowfocusrect )
dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)
iuo_dw_main = dw_report

this.x = 677
this.y = 589
this.width = 2700
this.height = 780
cb_selezione.hide()
end event

event pc_setddlb;call super::pc_setddlb;	f_po_loaddddw_dw(dw_selezione, &
                 	  "cod_centro_costo", &
                 		sqlca, &
                    "tab_centri_costo", &
                    "cod_centro_costo", &
                    "des_centro_costo", &
                    "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

event clicked;call super::clicked;dw_selezione.change_dw_current()
s_cs_xx.parametri.parametro_uo_dw_1 = pcca.window_currentdw
s_cs_xx.parametri.parametro_s_1 = "cod_cliente"
end event

type dw_selezione from uo_cs_xx_dw within w_report_cc_vendita
integer x = 23
integer y = 20
integer width = 2651
integer height = 480
integer taborder = 50
string dataobject = "r_report_cc_vendita_sel"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
end choose
end event

type cb_annulla from commandbutton within w_report_cc_vendita
integer x = 1874
integer y = 520
integer width = 366
integer height = 80
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;dw_selezione.reset()
dw_selezione.insertrow(0)
end event

type cb_report from commandbutton within w_report_cc_vendita
integer x = 2258
integer y = 520
integer width = 366
integer height = 80
integer taborder = 20
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Report"
end type

event clicked;boolean lb_trovato

string   ls_protocollo, ls_cod_centro_costo, ls_flag, ls_sql, ls_cod_fornitore, ls_cod_conto, ls_cod_centro_costo_sel, ls_des_fornitore, &
         ls_des_contropartita, ls_des_centro_costo, ls_cod_documento, ls_numeratore_documento, ls_cod_cliente, ls_des_cliente, &
			ls_appo_1, ls_appo_2, ls_ds, ls_syntax, ls_error, ls_sql_1, ls_cod_1, ls_cod_2, ls_des_1, ls_des_2, ls_cu, ls_cod_conto_cp, &
			ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven, ls_tipi_fat_fat, ls_tipi_fat_nc, ls_str_tipo_fat,&
			ls_cod_conto_cp_1[]
		 
long     ll_numero_da, ll_numero_a, ll_anno_registrazione, ll_num_registrazione, ll_riga, ll_cp, ll_cc, ll_anno_documento, &
         ll_num_documento, ll_ret, ll_riga_partenza, ll_i, ll_appo, ll_cp_nc, ll_i_nc,ll_index_cp, ll_y

dateTIME ldt_data_fattura_da, ldt_data_fattura_a

datetime ldt_data_registrazione, ldt_data_documento

dec{4}   ld_importo_riga, ld_importo, ld_importo_appo, ld_importo_cc, ld_totale_cp

integer  li_indice_cp, li_indice_cc, li_ii

datastore lds_totali, lds_totali_cp,lds_totali_nc, lds_totali_cp_nc

dw_selezione.accepttext()

dw_report.reset()

ll_numero_da = dw_selezione.getitemnumber(1,"num_documento_da")

ll_numero_a = dw_selezione.getitemnumber(1,"num_documento_a")

ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")

ldt_data_fattura_da = dw_selezione.getitemdatetime(1,"data_fattura_da")

ldt_data_fattura_a = dw_selezione.getitemdatetime(1,"data_fattura_a")

ls_cod_centro_costo_sel = dw_selezione.getitemstring( 1, "cod_centro_costo")

ls_flag = dw_selezione.getitemstring( 1, "flag_contabilizzate")

ls_sql_1 = " select anno_registrazione, " + &
		   " 			num_registrazione, " + &
		   " 			numeratore_documento, " + &
		   " 			anno_documento, " + &			
		   " 			num_documento, " + &		
		   " 			cod_documento, " + &						
		   " 			data_fattura, " + &
		   " 			cod_cliente, " + &			
		   " 			cod_tipo_fat_ven " + &			
		   " from   tes_fat_ven " + &
		   " where  cod_azienda = '" + s_cs_xx.cod_azienda + "' "
			
ls_sql = ""			
			
if not isnull(ll_numero_da) and ll_numero_da > 0 then			
   ls_sql = ls_sql + " and (num_documento >= " + string(ll_numero_da) + ")"
end if

if not isnull(ll_numero_a) and ll_numero_a > 0 then			
   ls_sql = ls_sql + " and (num_documento <= " + string(ll_numero_a) + ")"
end if

if not isnull(ls_cod_cliente) then
	ls_sql = ls_sql + " and (cod_cliente = '" + ls_cod_cliente + "') "
end if

if not isnull(ldt_data_fattura_da) then			
   ls_sql = ls_sql + " and (data_fattura >= '" + string(ldt_data_fattura_da, s_cs_xx.db_funzioni.formato_data) + "')"
end if

if not isnull(ldt_data_fattura_a) then			
   ls_sql = ls_sql + " and (data_fattura <= '" + string(ldt_data_fattura_a, s_cs_xx.db_funzioni.formato_data) + "')"
end if

if not isnull(ls_cod_centro_costo_sel) then
	
	ls_sql += " and ( select count(*) " + &
	         "       from   cont_fat_ven_cc " + &
				"       where  cod_azienda = '" + s_cs_xx.cod_azienda + "' and " + &
            "              anno_registrazione = tes_fat_ven.anno_registrazione and " + &
				"              num_registrazione = tes_fat_ven.num_registrazione and " + &
				"              cod_centro_costo = '" + ls_cod_centro_costo_sel + "' ) > 0 "
	
end if

choose case ls_flag
		
	case 'S'
		ls_sql += " and flag_contabilita = 'S' "
	case 'N'
		ls_sql += " and flag_contabilita = 'N' "
end choose

ls_sql_1 = ls_sql_1 + ls_sql + " order by cod_documento, numeratore_documento, anno_documento, num_documento, data_fattura"
//ls_sql_1 = ls_sql_1 + ls_sql + " order by data_fattura ASC"

DECLARE cu_testata DYNAMIC CURSOR FOR SQLSA ;

PREPARE SQLSA FROM :ls_sql_1 ;

OPEN DYNAMIC cu_testata ;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in OPEN cursore cu_testata." + sqlca.sqlerrtext)
	rollback;
	return
end if 
do while 1 = 1
	fetch cu_testata into :ll_anno_registrazione, 
	                      :ll_num_registrazione, 
								 :ls_numeratore_documento,
								 :ll_anno_documento,
								 :ll_num_documento,								 
								 :ls_cod_documento,
								 :ldt_data_documento, 								 
								 :ls_cod_cliente,
								 :ls_cod_tipo_fat_ven;

	if sqlca.sqlcode < 0 then
		g_mb.messagebox("Apice", "Errore in lettura dati da TES_FAT_ven.~n~r" + sqlca.sqlerrtext)
		return
	end if	
	
	if sqlca.sqlcode = 100 then exit
	
	setnull(ls_flag_tipo_fat_ven)
	setnull(ls_des_cliente)
	
	select rag_soc_1
	into   :ls_des_cliente
	from   anag_clienti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_cliente = :ls_cod_cliente;
			 
	if isnull(ls_des_cliente) then ls_des_cliente = ""
	
	select flag_tipo_fat_ven
	into   :ls_flag_tipo_fat_ven
	from   tab_tipi_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
			 
	if isnull(ls_flag_tipo_fat_ven) then ls_flag_tipo_fat_ven = ""
	
	ls_protocollo =  ls_cod_documento + ' / ' +  ls_numeratore_documento + ' / ' +  string(ll_anno_documento)  + ' / ' +  string(ll_num_documento)
	
	ll_riga = dw_report.insertrow(0)
	
	dw_report.setitem( ll_riga, "protocollo", ls_protocollo)
	
	dw_report.setitem( ll_riga, "data_protocollo", ldt_data_documento)
	
	dw_report.setitem( ll_riga, "cliente_fornitore", ls_des_cliente)
	
	ll_cp = 0
	
   DECLARE cu_contropartite CURSOR FOR  
   	SELECT cod_conto,   
             imp_riga
      FROM   cont_fat_ven
      WHERE  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
				 
	open cu_contropartite;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in OPEN cursore cu_contropartite." + sqlca.sqlerrtext)
		rollback;
		return
	end if 
	
	do while 1 = 1
		
		fetch cu_contropartite into :ls_cod_conto,
		                            :ld_importo_riga;
											 
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("Apice", "Errore in lettura dati cursore contropartite.~n~r" + sqlca.sqlerrtext)
			return
		end if	
	
		if sqlca.sqlcode = 100 then exit
		
		select des_conto
		into   :ls_des_contropartita
		from   anag_piano_conti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_conto = :ls_cod_conto;
					 
		if isnull(ls_cod_conto) then ls_cod_conto = ""
		
		if isnull(ls_des_contropartita) then ls_des_contropartita = ""
		
		ls_des_contropartita = ls_cod_conto + " " + ls_des_contropartita
		
		ll_cp = ll_cp + 1
		
		if ll_cp > 1 then	ll_riga = dw_report.insertrow(0)
			
		dw_report.setitem( ll_riga, "cod_contropartita", ls_cod_conto)			
			
		dw_report.setitem( ll_riga, "contropartita", ls_des_contropartita)
		
		if ls_flag_tipo_fat_ven = "N" then
			dw_report.setitem( ll_riga, "imponibile", ld_importo_riga * -1)
		else
			dw_report.setitem( ll_riga, "imponibile", ld_importo_riga)
		end if
		
		DECLARE cu_cc CURSOR FOR  
			SELECT cod_centro_costo,   
                importo  
         FROM   cont_fat_ven_cc  
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :ll_anno_registrazione and
					 num_registrazione = :ll_num_registrazione and
					 cod_conto = :ls_cod_conto;
						 
		open cu_cc;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore in OPEN cursore cu_cc." + sqlca.sqlerrtext)
			rollback;
			return
		end if 
		
		ll_cc = 0
	
		do while 1 = 1
		
			fetch cu_cc into :ls_cod_centro_costo,
                          :ld_importo;
											 
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("Apice", "Errore in lettura dati cursore cc.~n~r" + sqlca.sqlerrtext)
				return
			end if	

			if sqlca.sqlcode = 100 then exit
			
			select des_centro_costo
			into   :ls_des_centro_costo
			from   tab_centri_costo 
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_centro_costo = :ls_cod_centro_costo;
						 
			if isnull(ls_cod_centro_costo) then ls_cod_centro_costo = ""
			
			if isnull(ls_des_centro_costo) then ls_des_centro_costo = ""
			
			ls_des_centro_costo = ls_cod_centro_costo + " " + ls_des_centro_costo
						
			ll_cc = ll_cc + 1
			
			if ll_cc > 1 then ll_riga = dw_report.insertrow(0)
			
			dw_report.setitem( ll_riga, "cod_centro_costo", ls_cod_centro_costo)
			
			dw_report.setitem( ll_riga, "centrocosto", ls_des_centro_costo)
			
			if ls_flag_tipo_fat_ven = "N" then
				dw_report.setitem( ll_riga, "importo", ld_importo * -1)
			else
				dw_report.setitem( ll_riga, "importo", ld_importo)
			end if
			
		loop
		close cu_cc;
			
	loop
	close cu_contropartite;
	
loop

close cu_testata;


// creo un elenco di tipi fatture che sono fatture e non note di credito
ls_tipi_fat_fat = ""

declare cu_ft cursor for
select cod_tipo_fat_ven
from   tab_tipi_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and 
       flag_tipo_fat_ven <> 'N';
		 
open cu_ft;

do while true
	fetch cu_ft into :ls_str_tipo_fat;
	if sqlca.sqlcode = 100 then exit
	if len(ls_tipi_fat_fat) > 0 then
		ls_tipi_fat_fat += ",'" + ls_str_tipo_fat + "'"
	else
		ls_tipi_fat_fat = "'" + ls_str_tipo_fat + "'"
	end if
loop
close cu_ft;

// creo un elenco di tipi fatture che sono note di credito e non fatture
ls_tipi_fat_nc = ""

declare cu_nc cursor for
select cod_tipo_fat_ven
from   tab_tipi_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and 
       flag_tipo_fat_ven = 'N';
		 
open cu_nc;

do while true
	fetch cu_nc into :ls_str_tipo_fat;
	if sqlca.sqlcode = 100 then exit
	if len(ls_tipi_fat_nc) > 0 then
		ls_tipi_fat_nc += ",'" + ls_str_tipo_fat + "'"
	else
		ls_tipi_fat_nc = "'" + ls_str_tipo_fat + "'"
	end if
loop
close cu_nc;

// ----------------------------------------------------------------------

ll_riga_partenza = ll_riga + 2

dw_report.insertrow(0)

// ****    carico tutte le contropartite con i suoi totali per i documenti di tipo FATTURA

ls_cu = " SELECT cont_fat_ven.cod_conto,   " + &
        "        sum(cont_fat_ven.imp_riga) as importo   " + &
        " FROM   cont_fat_ven,   " + &
        "        tes_fat_ven   " + &
        " WHERE  ( tes_fat_ven.cod_azienda = cont_fat_ven.cod_azienda ) and   " + &
        "        ( tes_fat_ven.anno_registrazione = cont_fat_ven.anno_registrazione ) and   " + &
        "        ( tes_fat_ven.num_registrazione = cont_fat_ven.num_registrazione )  and " + &
        "        ( tes_fat_ven.cod_tipo_fat_ven in (" + ls_tipi_fat_fat + ") )  and " + &
		  "        ( cont_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' )"
		  
if ls_sql <> "" then
	ls_cu = ls_cu + ls_sql
end if		  
		  
ls_cu = ls_cu + " GROUP BY cont_fat_ven.cod_conto  " + &
                " ORDER BY cont_fat_ven.cod_conto ASC "

ls_syntax = sqlca.syntaxfromsql( ls_cu, 'style(type=grid)', ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	
	g_mb.messagebox( "APICE", "Errore durante caricamento sql cp. " + ls_error)

	return -1
	
end if

lds_totali_cp = create datastore

lds_totali_cp.create(ls_syntax, ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	
	destroy lds_totali_cp;
	
	g_mb.messagebox( "APICE", "Errore durante creazione datastore cp. " + ls_error)

	return -1
	
end if

lds_totali_cp.settransobject(sqlca)

if lds_totali_cp.retrieve() = -1 then
	
	destroy lds_totali_cp;
	
	g_mb.messagebox( "APICE", "Errore durante la retrieve dei totali cp. " + sqlca.sqlerrtext)

	return -1
	
end if	


// ****    carico tutte le contropartite con i suoi totali per i documenti di tipo NOTA DI CREDITO
//Nota di DONATO al 25-01-2010: Le note di credito dovrebbero essere con il segno negativo
//quindi ho aggiunto "-" prima del sum nella select qua sotto

ls_cu = " SELECT cont_fat_ven.cod_conto,   " + &
        "        -sum(cont_fat_ven.imp_riga) as importo   " + &
        " FROM   cont_fat_ven,   " + &
        "        tes_fat_ven   " + &
        " WHERE  ( tes_fat_ven.cod_azienda = cont_fat_ven.cod_azienda ) and   " + &
        "        ( tes_fat_ven.anno_registrazione = cont_fat_ven.anno_registrazione ) and   " + &
        "        ( tes_fat_ven.num_registrazione = cont_fat_ven.num_registrazione )  and " + &
        "        ( tes_fat_ven.cod_tipo_fat_ven in (" + ls_tipi_fat_nc + ") )  and " + &
		  "        ( cont_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' )"
		  
if ls_sql <> "" then
	ls_cu = ls_cu + ls_sql
end if		  
		  
ls_cu = ls_cu + " GROUP BY cont_fat_ven.cod_conto  " + &
                " ORDER BY cont_fat_ven.cod_conto ASC "

ls_syntax = sqlca.syntaxfromsql( ls_cu, 'style(type=grid)', ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	
	g_mb.messagebox( "APICE", "Errore durante caricamento sql cp. " + ls_error)

	return -1
	
end if

lds_totali_cp_nc = create datastore

lds_totali_cp_nc.create(ls_syntax, ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	
	destroy lds_totali_cp_nc;
	
	g_mb.messagebox( "APICE", "Errore durante creazione datastore cp_nc. " + ls_error)

	return -1
	
end if

lds_totali_cp_nc.settransobject(sqlca)

if lds_totali_cp_nc.retrieve() = -1 then
	
	destroy lds_totali_cp_nc;
	
	g_mb.messagebox( "APICE", "Errore durante la retrieve dei totali cp. " + sqlca.sqlerrtext)

	return -1
	
end if	


// ****

ls_ds = " SELECT cont_fat_ven_cc.cod_conto ,  " + & 
        "        cont_fat_ven_cc.cod_centro_costo ,     " + &
        " 		  sum(importo) as importo    " + &
        " FROM   cont_fat_ven_cc,     " + &
        " 		  tes_fat_ven    " + &
        " WHERE  ( tes_fat_ven.cod_azienda = cont_fat_ven_cc.cod_azienda ) and    " + &
        " 		  ( tes_fat_ven.anno_registrazione = cont_fat_ven_cc.anno_registrazione ) and    " + &
        " 		  ( tes_fat_ven.num_registrazione = cont_fat_ven_cc.num_registrazione ) and    " + &
        "        ( tes_fat_ven.cod_tipo_fat_ven in (" + ls_tipi_fat_fat + ") )  and " + &
        "        ( cont_fat_ven_cc.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) "
		  
if ls_sql <> "" then
	ls_ds = ls_ds + ls_sql
end if

ls_ds = ls_ds + " GROUP BY cont_fat_ven_cc.cod_conto,   " + &
        			 "          cont_fat_ven_cc.cod_centro_costo  " + &
		          " ORDER BY cont_fat_ven_cc.cod_conto ASC,   " + &
		          "          cont_fat_ven_cc.cod_centro_costo ASC "

ls_syntax = sqlca.syntaxfromsql( ls_ds, 'style(type=grid)', ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	
	g_mb.messagebox( "APICE", "Errore durante caricamento sql. " + ls_error)

	return -1
	
end if

lds_totali = create datastore

lds_totali.create(ls_syntax, ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	
	destroy lds_totali;
	
	g_mb.messagebox( "APICE", "Errore durante creazione datastore. " + ls_error)

	return -1
	
end if

lds_totali.settransobject(sqlca)

if lds_totali.retrieve() = -1 then
	
	destroy lds_totali;
	
	g_mb.messagebox( "APICE", "Errore durante la retrieve dei totali. " + sqlca.sqlerrtext)

	return -1
	
end if		 

// procedo con note di credito
//Nota di DONATO al 25-01-2010: Le note di credito dovrebbero essere con il segno negativo
//quindi ho aggiunto "-" prima del sum nella select qua sotto
ls_ds = " SELECT cont_fat_ven_cc.cod_conto ,  " + & 
        "        cont_fat_ven_cc.cod_centro_costo ,     " + &
        " 		  -sum(importo) as importo    " + &
        " FROM   cont_fat_ven_cc,     " + &
        " 		  tes_fat_ven    " + &
        " WHERE  ( tes_fat_ven.cod_azienda = cont_fat_ven_cc.cod_azienda ) and    " + &
        " 		  ( tes_fat_ven.anno_registrazione = cont_fat_ven_cc.anno_registrazione ) and    " + &
        " 		  ( tes_fat_ven.num_registrazione = cont_fat_ven_cc.num_registrazione ) and    " + &
        "        ( tes_fat_ven.cod_tipo_fat_ven in (" + ls_tipi_fat_nc + ") )  and " + &
        "        ( cont_fat_ven_cc.cod_azienda = '" + s_cs_xx.cod_azienda + "' ) "
		  
if ls_sql <> "" then
	ls_ds = ls_ds + ls_sql
end if

ls_ds = ls_ds + " GROUP BY cont_fat_ven_cc.cod_conto,   " + &
        			 "          cont_fat_ven_cc.cod_centro_costo  " + &
		          " ORDER BY cont_fat_ven_cc.cod_conto ASC,   " + &
		          "          cont_fat_ven_cc.cod_centro_costo ASC "

ls_syntax = sqlca.syntaxfromsql( ls_ds, 'style(type=grid)', ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	
	g_mb.messagebox( "APICE", "Errore durante caricamento sql. " + ls_error)

	return -1
	
end if

lds_totali_nc = create datastore

lds_totali_nc.create(ls_syntax, ls_error)

if not isnull(ls_error) and len(trim(ls_error)) > 0 then
	
	destroy lds_totali_nc;
	
	g_mb.messagebox( "APICE", "Errore durante creazione datastore. " + ls_error)

	return -1
	
end if

lds_totali_nc.settransobject(sqlca)

if lds_totali_nc.retrieve() = -1 then
	
	destroy lds_totali_nc;
	
	g_mb.messagebox( "APICE", "Errore durante la retrieve dei totali. " + sqlca.sqlerrtext)

	return -1
	
end if		 


// =====================================

ls_appo_1 = ""

ld_importo_appo = 0

li_ii = 0
ll_index_cp = 0


for ll_cp = 1 to lds_totali_cp.rowcount()
	
	ll_riga = dw_report.insertrow(0)
		
	if ll_riga = ll_riga_partenza then
			
		dw_report.setitem( ll_riga, "cliente_fornitore", "TOTALI")
		
	end if	
	
	ls_cod_conto_cp = lds_totali_cp.getitemstring( ll_cp, 1)
	ll_index_cp ++
	ls_cod_conto_cp_1[ll_index_cp] = ls_cod_conto_cp
	
	ld_totale_cp = lds_totali_cp.getitemnumber( ll_cp, 2)
	
	for ll_cp_nc = 1 to lds_totali_cp_nc.rowcount()
		
		if lds_totali_cp_nc.getitemstring( ll_cp_nc, 1) = lds_totali_cp.getitemstring( ll_cp, 1) then
			// detraggo l'importo delle NC per la stessa contropartita
			ld_totale_cp = ld_totale_cp - lds_totali_cp_nc.getitemnumber( ll_cp_nc, 2)
		end if
		
	next
	
	select des_conto 
	into   :ls_des_1
	from   anag_piano_conti 
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_conto = :ls_cod_conto_cp;
				 
	dw_report.setitem( ll_riga, "contropartita", ls_cod_conto_cp + " " + ls_des_1)
		
	dw_report.setitem( ll_riga, "imponibile", ld_totale_cp)
	
	ll_appo = 0
	
	for ll_i = 1 to lds_totali.rowcount()
		
		ls_cod_1 = lds_totali.getitemstring( ll_i, 1)
		
		if ls_cod_1 <> ls_cod_conto_cp then continue

		ls_cod_2 = lds_totali.getitemstring( ll_i, 2)
		
		ld_importo_cc = lds_totali.getitemnumber( ll_i, 3)
		
		for ll_i_nc = 1 to lds_totali_nc.rowcount()
			
			if lds_totali.getitemstring( ll_i, 1) = lds_totali_nc.getitemstring( ll_i_nc, 1) and lds_totali.getitemstring( ll_i, 2) = lds_totali_nc.getitemstring( ll_i_nc, 2) then
				// detraggo l'importo delle NC per la stessa contropartita/centro di costo
				ld_importo_cc = ld_importo_cc - lds_totali_nc.getitemnumber( ll_i_nc, 3)
			
			end if
			
		next
		
		
		select des_centro_costo 
		into   :ls_des_2
		from   tab_centri_costo 
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_centro_costo = :ls_cod_2;
				 
		if ll_appo > 0 then
			
			ll_riga = dw_report.insertrow(0)
			
		end if				 
				 
		dw_report.setitem( ll_riga, "centrocosto", ls_cod_2 + " " + ls_des_2)
	
		dw_report.setitem( ll_riga, "importo", ld_importo_cc)		
		
		ll_appo = ll_appo + 1		
			
	next
	
	// controllo se nelle NC ci sono delle ctp che non erano presenti in fattura
	
	for ll_i = 1 to lds_totali_nc.rowcount()
		lb_trovato = false
		for ll_y = 1 to lds_totali.rowcount()
			if lds_totali_nc.getitemstring( ll_i, 1) = lds_totali.getitemstring( ll_cp, 1) then
				lb_trovato = true
				exit
			end if
		next
		
		if lb_trovato then continue
		
		ls_cod_1 = lds_totali_nc.getitemstring( ll_i, 1)
		
		if ls_cod_1 <> ls_cod_conto_cp then continue

		ls_cod_2 = lds_totali_nc.getitemstring( ll_i, 2)
		
		ld_importo_cc = lds_totali_nc.getitemnumber( ll_i, 3)
		
		select des_centro_costo 
		into   :ls_des_2
		from   tab_centri_costo 
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_centro_costo = :ls_cod_2;
				 
		if ll_appo > 0 then
			
			ll_riga = dw_report.insertrow(0)
			
		end if				 
				 
		dw_report.setitem( ll_riga, "centrocosto", ls_cod_2 + " " + ls_des_2)
	
		dw_report.setitem( ll_riga, "importo", ld_importo_cc)		
		
		ll_appo = ll_appo + 1		
			
	next
	
next

//----------------------
//Donato 25-01-2010
//aggiungo una riga xchè altrimenti va a scrivere su una già esistente
if lds_totali_cp_nc.rowcount() > 0 then
	ll_riga = dw_report.insertrow(0)
end if
//----------------------

for ll_i = 1 to lds_totali_cp_nc.rowcount()
	lb_trovato = false
	for ll_cp = 1 to lds_totali_cp_nc.rowcount()
		if lds_totali_cp.getitemstring( ll_cp, 1) = lds_totali_cp_nc.getitemstring( ll_cp, 1) then
			lb_trovato = true
			exit
		end if
	next
	
	if lb_trovato then continue
	
	// aggiungo la contropartita delle NC che non c'era fra le fatture
	
	ls_cod_conto_cp = lds_totali_cp_nc.getitemstring( ll_i, 1)
	
	ld_totale_cp = lds_totali_cp_nc.getitemnumber( ll_i, 2)
	
	select des_conto 
	into   :ls_des_1
	from   anag_piano_conti 
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_conto = :ls_cod_conto_cp;
				 
	dw_report.setitem( ll_riga, "contropartita", ls_cod_conto_cp + " " + ls_des_1)
		
	dw_report.setitem( ll_riga, "imponibile", ld_totale_cp)
	
	ll_appo = 0
	
	for ll_cp = 1 to lds_totali_nc.rowcount()
		
		ls_cod_1 = lds_totali_nc.getitemstring(ll_cp, 1)
		
		if ls_cod_1 <> ls_cod_conto_cp then continue

		ls_cod_2 = lds_totali_nc.getitemstring(ll_cp, 2)
		
		ld_importo_cc = lds_totali_nc.getitemnumber(ll_cp, 3)
		
		select des_centro_costo 
		into   :ls_des_2
		from   tab_centri_costo 
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_centro_costo = :ls_cod_2;
				 
		if ll_appo > 0 then
			
			ll_riga = dw_report.insertrow(0)
			
		end if				 
				 
		dw_report.setitem( ll_riga, "centrocosto", ls_cod_2 + " " + ls_des_2)
	
		dw_report.setitem( ll_riga, "importo", ld_importo_cc)		
		
		ll_appo = ll_appo + 1		
			
	next
	
next

// ================================

dw_report.sort()

dw_report.change_dw_current()

dw_selezione.hide()

dw_selezione.object.b_ricerca_cliente.visible=false

parent.x = 100
parent.y = 50
parent.width = 3700
parent.height = 1945

dw_report.show()

cb_selezione.show()

dw_report.change_dw_current()

dw_report.object.datawindow.print.preview = 'Yes'

dw_report.object.datawindow.print.preview.rulers = 'Yes'

end event

type cb_selezione from commandbutton within w_report_cc_vendita
integer x = 3269
integer y = 1740
integer width = 366
integer height = 80
integer taborder = 11
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Selezione"
end type

event clicked;dw_selezione.show()

dw_selezione.object.b_ricerca_cliente.visible=true

parent.x = 677
parent.y = 589
parent.width = 2700
parent.height = 780

dw_report.hide()

cb_selezione.hide()

dw_selezione.change_dw_current()
end event

type dw_report from uo_cs_xx_dw within w_report_cc_vendita
boolean visible = false
integer x = 23
integer y = 20
integer width = 3611
integer height = 1700
integer taborder = 60
string dataobject = "d_report_con_cc_ven"
boolean hscrollbar = true
boolean vscrollbar = true
boolean hsplitscroll = true
boolean livescroll = true
end type

