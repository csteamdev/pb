﻿$PBExportHeader$w_report_fat_ven_euro.srw
$PBExportComments$Finestra Report Fattura di Vendita
forward
global type w_report_fat_ven_euro from w_cs_xx_principale
end type
type cb_archivia from commandbutton within w_report_fat_ven_euro
end type
type cb_2 from commandbutton within w_report_fat_ven_euro
end type
type cb_singola from commandbutton within w_report_fat_ven_euro
end type
type cb_1 from commandbutton within w_report_fat_ven_euro
end type
type dw_report_fat_ven from uo_cs_xx_dw within w_report_fat_ven_euro
end type
end forward

global type w_report_fat_ven_euro from w_cs_xx_principale
integer width = 4014
integer height = 2424
string title = "Stampa Fatture"
boolean minbox = false
event ue_close ( )
event ue_avviso_spedizione ( )
cb_archivia cb_archivia
cb_2 cb_2
cb_singola cb_singola
cb_1 cb_1
dw_report_fat_ven dw_report_fat_ven
end type
global w_report_fat_ven_euro w_report_fat_ven_euro

type variables
boolean ib_modifica=false, ib_nuovo=false, ib_nr_nr, ib_stampando = false
boolean ib_estero
long il_anno_registrazione, il_num_registrazione, il_anno[], il_num[], il_corrente = 0
long il_num_copie

// ------------- dichiarate per stampa da pagina a pagina -----------
boolean ib_stampa = false
long    il_totale_pagine, il_pagina_corrente



boolean  ib_email=false
// ------------- dichiarate per stampa da pagina a pagina ----------
string is_flag_email[], is_email_amministrazione[], is_email_utente[]

// ------------- discalimer per invio fatture tramite email ----------
constant string is_disclaimer=""

boolean ib_nascondi_cod_documento = false

// stefanop 17/02/2012: percorso file per filigrana
private:
	string is_path_filigrana = ""
	string is_flag_tipo_fat_ven

end variables

forward prototypes
public subroutine wf_leggi_scadenze (long fl_anno_registrazione, long fl_num_registrazione, ref datetime fdt_data_scadenze[], ref decimal fd_importo_rata[])
public function integer wf_impostazioni ()
public subroutine wf_report ()
public function integer wf_email ()
public function integer wf_replace_marker (ref string as_source, string as_find, string as_replace, boolean ab_case_sensitive)
public function integer wf_testo_mail (string fs_rag_soc_1, long fl_anno, long fl_numero, datetime fdt_data, ref string fs_testo)
public subroutine wf_stampa (integer copie)
public function integer wf_memorizza_blob (long fl_anno_registrazione, long fl_num_registrazione, string fs_path, ref string as_errore)
end prototypes

event ue_close();close(this)
end event

event ue_avviso_spedizione();uo_avviso_spedizioni luo_avviso_sped

string ls_tipo_gestione, ls_messaggio, ls_path_logo_intestazione
long ll_ret

//solo se non hai stampato da numero a numero
if not ib_nr_nr then
	if upperbound(il_anno)>0 and upperbound(il_num)>0 then
		if il_anno[1]>0 and il_num[1]>0 then
		
			luo_avviso_sped = create uo_avviso_spedizioni
		
			ls_tipo_gestione = "fat_ven"
			
			//passaggio path file dell'intestazione report ---------------------------------------
			try
				ls_path_logo_intestazione = dw_report_fat_ven.Describe ("intestazione.filename") 
			catch (throwable err)
				ls_path_logo_intestazione = ""
			end try
			
			luo_avviso_sped.is_path_logo_intestazione = ls_path_logo_intestazione
			//-----------------------------------------------------------------------------------------
			
			//passa l'oggetto dw del report
			luo_avviso_sped.idw_report = dw_report_fat_ven
			
			ll_ret = luo_avviso_sped.uof_avviso_spedizione(il_anno[1], il_num[1], ls_tipo_gestione, ls_messaggio)
	
			if ll_ret<0 then
				g_mb.error(ls_messaggio)
				rollback;
				return
			end if
		
			commit;
			
			destroy luo_avviso_sped
		else
			g_mb.error("Nessun documento presente (>0)!")
			rollback;
			return
		end if
	else
		g_mb.error("Nessun documento presente (ubound)!")
		rollback;
		return
	end if
end if
end event

public subroutine wf_leggi_scadenze (long fl_anno_registrazione, long fl_num_registrazione, ref datetime fdt_data_scadenze[], ref decimal fd_importo_rata[]);long ll_i

declare cu_scadenze cursor for 
	select   scad_fat_ven.data_scadenza, 
				scad_fat_ven.imp_rata_valuta
	from     scad_fat_ven 
	where    scad_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				scad_fat_ven.anno_registrazione = :fl_anno_registrazione and 
				scad_fat_ven.num_registrazione = :fl_num_registrazione
	order by scad_fat_ven.data_scadenza;

open cu_scadenze;

ll_i = 0
do while 0 = 0
	ll_i = ll_i + 1
   fetch cu_scadenze into :fdt_data_scadenze[ll_i], 
							     :fd_importo_rata[ll_i];

   if sqlca.sqlcode <> 0 then exit
loop

close cu_scadenze;
return

end subroutine

public function integer wf_impostazioni ();string ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven, ls_cod_cliente, ls_flag_tipo_cliente, ls_path_logo_1, &
       ls_path_logo_2, ls_modify, ls_dataobject, ls_logo_testata, ls_cod_lingua

long   ll_copie, ll_copie_cee, ll_copie_extra_cee



save_on_close(c_socnosave)

ib_estero = false
il_num_copie = 3
setnull(ls_logo_testata)

select cod_cliente,
       	cod_tipo_fat_ven
into   	:ls_cod_cliente,
		:ls_cod_tipo_fat_ven
from   tes_fat_ven
where cod_azienda = :s_cs_xx.cod_azienda and
       	anno_registrazione = :il_anno_registrazione and
		num_registrazione = :il_num_registrazione;
		 
select flag_tipo_cliente,
		 cod_lingua
into   :ls_flag_tipo_cliente,
		 :ls_cod_lingua
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_cliente = :ls_cod_cliente;
		 
if sqlca.sqlcode = 0 and not isnull(ls_cod_cliente) then

	// *** Modifica Michela 04/12/2007: specifica report fatture di centro gibus rev.02 del 03/12/2007
	//												controllo se esistono per quel cliente particolari dw. altrimenti controllo se esistono particolari dw per 
	//												la sola lingua, altrimenti prendo il dataobject nel tipo fattura
	if not isnull( ls_cod_lingua) and ls_cod_lingua <> "" then
		
		select  dataobject, 
				 logo_testata
		into	 :ls_dataobject, 
				 :ls_logo_testata
		from	 tab_tipi_fat_ven_lingue_dw
		where	 cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven and
				 cod_lingua = :ls_cod_lingua and
				 cod_cliente = :ls_cod_cliente;
				 
		if sqlca.sqlcode <> 0 then
			
			// *** non esiste per quel cliente un modulo specifico; controllo se esiste a livello di lingua
			select dataobject,
					logo_testata
			into	 :ls_dataobject,
					:ls_logo_testata
			from	 tab_tipi_fat_ven_lingue_dw
			where	 cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven and
					 cod_lingua = :ls_cod_lingua and
					 ( cod_cliente IS NULL or cod_cliente = '' );
			
		end if
			
	end if
	
end if


// se il dataobject è vuoto, lo prendo dal tipo fattura; altrimenti tengo quello di default
//if isnull(ls_dataobject) or len(ls_dataobject) < 1 then
//	select dataobject
//	into	 :ls_dataobject
//	from	 tab_tipi_fat_ven
//	where	 cod_azienda = :s_cs_xx.cod_azienda and
//			 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
//			 
//end if
//				 
//if not isnull(ls_dataobject) and len(ls_dataobject) > 0  then
//	dw_report_fat_ven.dataobject = ls_dataobject
//end if

// determino quante copie stampare della fattura
select num_copie,
		 num_copie_cee,
		 num_copie_extra_cee
into     :ll_copie,
		 :ll_copie_cee,
		 :ll_copie_extra_cee
from   tab_tipi_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
if sqlca.sqlcode = 0 then
	choose case ls_flag_tipo_cliente
		case "C"
			ib_estero = true
			il_num_copie = ll_copie_cee
		case "E"
			ib_estero = true
			il_num_copie = ll_copie_extra_cee
		case else
			ib_estero = false
			il_num_copie = ll_copie
	end choose
end if
				

if isnull(ls_logo_testata) or len(ls_logo_testata) < 1 then

	select parametri_azienda.stringa
	into   :ls_path_logo_1
	from   parametri_azienda
	where  parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and &
			 parametri_azienda.flag_parametro = 'S' and &
			 parametri_azienda.cod_parametro = 'LO3';
	
	ls_modify = "intestazione.filename='" + s_cs_xx.volume + ls_path_logo_1 + "'~t"
	dw_report_fat_ven.modify(ls_modify)

else
	
	ls_modify = "intestazione.filename='" + s_cs_xx.volume + s_cs_xx.risorse + ls_logo_testata + "'~t"
	dw_report_fat_ven.modify(ls_modify)
	
end if

cb_1.text = "STAMPA " + string(il_num_copie) + " COPIE"

return 0
end function

public subroutine wf_report ();boolean  lb_prima_riga, lb_flag_prodotto_cliente, lb_flag_nota_dettaglio, lb_flag_nota_piede

string   ls_stringa, ls_stringa_euro, ls_cod_tipo_fat_ven, ls_cod_banca_clien_for, ls_cod_cliente, &
		   ls_cod_valuta, ls_cod_pagamento, ls_num_ord_cliente, ls_cod_porto, ls_cod_resa, &
		   ls_nota_testata, ls_nota_piede, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
		   ls_localita, ls_frazione, ls_cap, ls_provincia, ls_cod_misura, ls_nota_dettaglio, &
		   ls_cod_prodotto, ls_des_prodotto, ls_rag_soc_1_cli, ls_rag_soc_2_cli, &
		   ls_indirizzo_cli, ls_localita_cli, ls_frazione_cli, ls_cap_cli, ls_provincia_cli, &
		   ls_partita_iva, ls_cod_fiscale, ls_cod_lingua, ls_flag_tipo_cliente, &
		   ls_des_tipo_fat_ven, ls_des_pagamento, ls_des_pagamento_lingua, ls_des_banca, &
  		   ls_des_porto, ls_des_porto_lingua, ls_des_resa, ls_des_resa_lingua, ls_des_tipo_det_ven,&
		   ls_des_prodotto_anag, ls_flag_stampa_fattura, ls_cod_tipo_det_ven, ls_flag_tipo_fat_ven, &
		   ls_des_prodotto_lingua, ls_cod_prod_cliente, ls_cod_iva, ls_des_iva, ls_causale_trasporto, &
		   ls_cod_vettore, ls_vettore_rag_soc_1, ls_vettore_rag_soc_2, ls_vettore_indirizzo, &
		   ls_vettore_cap, ls_vettore_localita, ls_vettore_provincia, ls_des_esenzione_iva[], &
		   ls_cod_documento, ls_numeratore_doc, ls_cod_banca, ls_flag_tipo_pagamento, ls_cod_abi, ls_cod_cab, &
		   ls_cod_des_cliente, ls_telefono_cliente, ls_fax_cliente, ls_telefono_des, ls_fax_des, &
		   ls_des_valuta, ls_des_valuta_lingua, ls_dicitura_std, ls_cod_agente_1, ls_cod_agente_2,&
		   ls_anag_agenti_rag_soc_1, ls_anag_agenti_rag_soc_2, ls_cod_fornitore, ls_cod_des_fornitore, &
		   ls_cod_des_fattura, ls_rag_soc_1_dest_fat, ls_rag_soc_2_dest_fat, ls_indirizzo_dest_fat, &
		   ls_localita_dest_fat, ls_frazione_dest_fat, ls_cap_dest_fat, ls_provincia_dest_fat, ls_des_causale, &
		   ls_flag_st_note_tes, ls_flag_st_note_pie, ls_flag_st_note_det, ls_formato, ls_flag_tipo_det_ven, ls_iban, ls_cod_contatto, &
		   ls_des_estesa[], ls_des_aliquota, ls_codici_iva[], ls_stato_cli, ls_note, ls_db,ls_errore
			
long     ll_errore, ll_num_colli, ll_num_aliquote, ll_anno_documento, ll_num_documento, ll_prog_mimytype, ll_ret

dec{4}   ld_tot_fattura, ld_sconto, ld_quan_fatturata, ld_prezzo_vendita, ld_sconto_1, &
	      ld_sconto_2, ld_sconto_3, ld_sconto_4, ld_sconto_5, ld_sconto_6, ld_sconto_7,&
		   ld_sconto_8, ld_sconto_9, ld_sconto_10, ld_sconto_tot, ld_imponibile_riga, &
		   ld_val_riga, ld_sconto_pagamento, ld_cambio_ven, &
		   ld_imponibili_valuta[], ld_iva_valuta[],ld_aliquote_iva[] ,ld_rate_scadenze[],&
		   ld_imponibile_iva_valuta, ld_importo_iva_valuta, ld_perc_iva,&
         ld_tot_fattura_euro, ld_tot_fattura_valuta
			
dec{5}   ld_fat_conversione_ven
			
datetime ldt_data_ord_cliente, ldt_data_registrazione, ldt_scadenze[], ldt_data_fattura

blob     l_blob

transaction		sqlcb

dw_report_fat_ven.reset()

select parametri_azienda.stringa  
into  :ls_stringa  
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'CVL';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa)
end if

select parametri_azienda.stringa  
into  :ls_stringa_euro
from  parametri_azienda  
where parametri_azienda.cod_azienda = :s_cs_xx.cod_azienda and  
      parametri_azienda.flag_parametro = 'S' and  
      parametri_azienda.cod_parametro = 'EUR';

if sqlca.sqlcode <> 0 then
	setnull(ls_stringa_euro)
end if
select cod_tipo_fat_ven,   
		 data_registrazione,   
		 cod_cliente,   
		 cod_fornitore,
		 cod_valuta,   
		 cod_pagamento,   
		 sconto,   
		 cod_banca_clien_for,   
		 cod_banca,   
		 num_ord_cliente,   
		 data_ord_cliente,   
		 cod_porto,   
		 cod_resa,   
		 nota_testata,   
		 nota_piede,   
		 imponibile_iva_valuta,
		 importo_iva_valuta,
		 tot_fattura_valuta,
		 tot_fattura,
		 rag_soc_1,   
		 rag_soc_2,   
		 indirizzo,   
		 localita,   
		 frazione,   
		 cap,   
		 provincia,
 		 rag_soc_1_dest_fat,   
		 rag_soc_2_dest_fat,   
		 indirizzo_dest_fat,   
		 localita_dest_fat,   
		 frazione_dest_fat,   
		 cap_dest_fat,   
		 provincia_dest_fat,
		 cambio_ven,
		 num_colli,
		 cod_vettore,
		 cod_causale,
		 cod_documento,
		 numeratore_documento,
		 anno_documento,
		 num_documento,
		 data_fattura,
		 cod_agente_1,
		 cod_agente_2,
		 cod_des_cliente,
		 cod_des_fornitore,
		 flag_st_note_tes,
		 flag_st_note_pie,
		 cod_contatto
into   :ls_cod_tipo_fat_ven,  
	 	 :ldt_data_registrazione,   
	 	 :ls_cod_cliente,   
		 :ls_cod_fornitore,
		 :ls_cod_valuta,   
		 :ls_cod_pagamento,   
		 :ld_sconto,   
		 :ls_cod_banca_clien_for,   
		 :ls_cod_banca,
		 :ls_num_ord_cliente,   
		 :ldt_data_ord_cliente,   
		 :ls_cod_porto,   
		 :ls_cod_resa,   
		 :ls_nota_testata,   
		 :ls_nota_piede,   
		 :ld_imponibile_iva_valuta,
		 :ld_importo_iva_valuta,
		 :ld_tot_fattura_valuta,
		 :ld_tot_fattura_euro,
		 :ls_rag_soc_1,   
		 :ls_rag_soc_2,   
		 :ls_indirizzo,   
		 :ls_localita,   
		 :ls_frazione,   
		 :ls_cap,   
		 :ls_provincia,
		 :ls_rag_soc_1_dest_fat,   
		 :ls_rag_soc_2_dest_fat,   
		 :ls_indirizzo_dest_fat,   
		 :ls_localita_dest_fat,   
		 :ls_frazione_dest_fat,   
		 :ls_cap_dest_fat,   
		 :ls_provincia_dest_fat,		 
		 :ld_cambio_ven,
		 :ll_num_colli,
		 :ls_cod_vettore,
		 :ls_causale_trasporto,
		 :ls_cod_documento,
		 :ls_numeratore_doc,
		 :ll_anno_documento,
		 :ll_num_documento,
		 :ldt_data_fattura,
		 :ls_cod_agente_1,
		 :ls_cod_agente_2,
		 :ls_cod_des_cliente,
		 :ls_cod_des_fornitore,
		 :ls_flag_st_note_tes,
		 :ls_flag_st_note_pie,
		 :ls_cod_contatto
from   tes_fat_ven  
where  tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tes_fat_ven.anno_registrazione = :il_anno_registrazione and 
		 tes_fat_ven.num_registrazione = :il_num_registrazione;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca testata fattura: stampa interrotta", Stopsign!)
	return
end if

//-------------------------------------- Modifica Nicola -------------------------------------------------------
if ls_flag_st_note_tes = 'I' then   //nota di testata
	select flag_st_note
	  into :ls_flag_st_note_tes
	  from tab_tipi_fat_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
end if		

if ls_flag_st_note_tes = 'N' then
	ls_nota_testata = ""
end if			


if ls_flag_st_note_pie = 'I' then //nota di piede
	select flag_st_note
	  into :ls_flag_st_note_pie
	  from tab_tipi_fat_ven
	 where cod_azienda = :s_cs_xx.cod_azienda
		and cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
end if			

if ls_flag_st_note_pie = 'N' then
	ls_nota_piede = ""
end if				
//--------------------------------------- Fine Modifica --------------------------------------------------------		

select des_tipo_fat_ven,
       flag_tipo_fat_ven
into   :ls_des_tipo_fat_ven,
       :ls_flag_tipo_fat_ven
from   tab_tipi_fat_ven  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
if sqlca.sqlcode <> 0 then
	setnull(ls_des_tipo_fat_ven)
end if

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca flag_tipo_fat_ven in tabella tab_tipi_fat_ven: stampa interrotta", Stopsign!)
	return
end if
	
if ls_flag_tipo_fat_ven <> "F" then
	
	//se è popolato il contatto allora preleva i dati da anag_contatti
	
	if ls_cod_contatto<>"" and not isnull(ls_cod_contatto) then
		setnull(ls_stato_cli)
		
		select rag_soc_1,   
				 rag_soc_2,   
				 indirizzo,   
				 localita,   
				 frazione,   
				 cap,   
				 provincia,   
				 partita_iva,   
				 cod_fiscale,   
				 cod_lingua,   
				 telefono,   
				 fax,   
				 flag_tipo_cliente  
		into   :ls_rag_soc_1_cli,   
				 :ls_rag_soc_2_cli,   
				 :ls_indirizzo_cli,   
				 :ls_localita_cli,   
				 :ls_frazione_cli,   
				 :ls_cap_cli,   
				 :ls_provincia_cli,   
				 :ls_partita_iva,   
				 :ls_cod_fiscale,   
				 :ls_cod_lingua,  
				 :ls_telefono_cliente,
				 :ls_fax_cliente,
				 :ls_flag_tipo_cliente  
		from   anag_contatti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_contatto = :ls_cod_contatto;
		
	else
		select rag_soc_1,   
				 rag_soc_2,   
				 indirizzo,   
				 localita,   
				 frazione,   
				 cap,   
				 provincia,   
				 partita_iva,   
				 cod_fiscale,   
				 cod_lingua,   
				 telefono,   
				 fax,   
				 flag_tipo_cliente,
				 stato
		into   :ls_rag_soc_1_cli,   
				 :ls_rag_soc_2_cli,   
				 :ls_indirizzo_cli,   
				 :ls_localita_cli,   
				 :ls_frazione_cli,   
				 :ls_cap_cli,   
				 :ls_provincia_cli,   
				 :ls_partita_iva,   
				 :ls_cod_fiscale,   
				 :ls_cod_lingua,  
				 :ls_telefono_cliente,
				 :ls_fax_cliente,
				 :ls_flag_tipo_cliente,
				 :ls_stato_cli
		from   anag_clienti  
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente;
				 
	end if
				 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca anagrafica cliente: stampa interrotta", Stopsign!)
		return
	end if
	
else
	select rag_soc_1,   
			 rag_soc_2,   
			 indirizzo,   
			 localita,   
			 frazione,   
			 cap,   
			 provincia,   
			 partita_iva,   
			 cod_fiscale,   
			 cod_lingua,   
			 telefono,   
			 fax,   
			 flag_tipo_fornitore,
			 stato
	into   :ls_rag_soc_1_cli,   
			 :ls_rag_soc_2_cli,   
			 :ls_indirizzo_cli,   
			 :ls_localita_cli,   
			 :ls_frazione_cli,   
			 :ls_cap_cli,   
			 :ls_provincia_cli,   
			 :ls_partita_iva,   
			 :ls_cod_fiscale,   
			 :ls_cod_lingua,  
			 :ls_telefono_cliente,
			 :ls_fax_cliente,
			 :ls_flag_tipo_cliente,
			 :ls_stato_cli
	from   anag_fornitori  
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_fornitore = :ls_cod_fornitore;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in ricerca anagrafica cliente: stampa interrotta", Stopsign!)
		return
	end if
end if


if not isnull(ls_stato_cli) and ls_stato_cli<>"" then
	if not isnull(ls_localita_cli) and ls_localita_cli<>"" then
		ls_localita_cli += " - " + ls_stato_cli
	else
		ls_localita_cli = ls_stato_cli
	end if
end if


select rag_soc_1,
       indirizzo,   
       localita,   
       cap,   
       provincia  
into   :ls_vettore_rag_soc_1,   
       :ls_vettore_indirizzo,   
       :ls_vettore_localita,   
       :ls_vettore_cap,   
       :ls_vettore_provincia
from   anag_vettori
where  cod_azienda = :s_cs_xx.cod_azienda and
	    cod_vettore = :ls_cod_vettore;

if sqlca.sqlcode <> 0 then
   setnull(ls_vettore_rag_soc_1)   
   setnull(ls_vettore_indirizzo)   
   setnull(ls_vettore_localita)   
   setnull(ls_vettore_cap)   
   setnull(ls_vettore_provincia)
end if

//Giulio: 30/01/2012 Modifica report: nel report viene inserita sia P.IVA sia Cod. Fiscale.

//if ls_flag_tipo_cliente = 'E' or ls_flag_tipo_cliente = 'C' then
//	ls_partita_iva = ls_cod_fiscale
//end if

select des_pagamento,   
       sconto,
       flag_tipo_pagamento
into   :ls_des_pagamento,   
       :ld_sconto_pagamento,
       :ls_flag_tipo_pagamento
from   tab_pagamenti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_pagamento = :ls_cod_pagamento;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento)
	setnull(ld_sconto_pagamento)
   ls_flag_tipo_pagamento = "D"
end if

select des_pagamento
into   :ls_des_pagamento_lingua
from   tab_pagamenti_lingue  
where  cod_azienda   = :s_cs_xx.cod_azienda and 
       cod_pagamento = :ls_cod_pagamento and
       cod_lingua    = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_pagamento_lingua)
end if

if ls_flag_tipo_pagamento = "R"	then			// se ricevuta bancaria
	select des_banca,
			 cod_abi,
			 cod_cab,
			 iban
	into   :ls_des_banca,
			 :ls_cod_abi,
			 :ls_cod_cab,
			 :ls_iban
	from   anag_banche_clien_for  
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_banca_clien_for = :ls_cod_banca_clien_for;
else
	select des_banca,
	       cod_abi,
			 cod_cab,
			 iban
	into   :ls_des_banca,
			 :ls_cod_abi,
			 :ls_cod_cab,
			 :ls_iban
	from   anag_banche
	where  cod_azienda = :s_cs_xx.cod_azienda and 
			 cod_banca = :ls_cod_banca;
end if

if sqlca.sqlcode <> 0 then
	ls_des_banca = ""
	ls_cod_abi = ""
	ls_cod_cab = ""
	ls_iban = ""
end if

select des_porto  
into   :ls_des_porto  
from   tab_porti  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_porto = :ls_cod_porto;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto)
end if

select des_porto  
into   :ls_des_porto_lingua  
from   tab_porti_lingue 
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_porto = :ls_cod_porto and
       cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_porto_lingua)
end if

select des_resa
into   :ls_des_resa  
from   tab_rese  
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_resa = :ls_cod_resa;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa)
end if

select des_resa  
into   :ls_des_resa_lingua  
from   tab_rese_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_resa = :ls_cod_resa and  
       cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_resa_lingua)
end if

select des_valuta,
		 formato
into   :ls_des_valuta,
		 :ls_formato
from   tab_valute
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_valuta = :ls_cod_valuta;
		 
if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta)
end if

select des_valuta
into   :ls_des_valuta_lingua  
from   tab_valute_lingue  
where  cod_azienda = :s_cs_xx.cod_azienda and 
       cod_valuta = :ls_cod_valuta and  
       cod_lingua = :ls_cod_lingua;

if sqlca.sqlcode <> 0 then
	setnull(ls_des_valuta_lingua)
end if

if not isnull(ls_cod_agente_1) then
	select rag_soc_1
	into   :ls_anag_agenti_rag_soc_1
	from   anag_agenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_agente = :ls_cod_agente_1;
	if sqlca.sqlcode <> 0 then setnull(ls_anag_agenti_rag_soc_1)
end if

if not isnull(ls_cod_agente_2) then
	select rag_soc_1
	into   :ls_anag_agenti_rag_soc_2
	from   anag_agenti
	where  cod_azienda = :s_cs_xx.cod_azienda and
	       cod_agente = :ls_cod_agente_2;
	if sqlca.sqlcode <> 0 then setnull(ls_anag_agenti_rag_soc_2)
end if

if not isnull(ls_cod_des_cliente) then
	if ls_flag_tipo_fat_ven <> "F" then	
		select telefono,   
				 fax  
		into   :ls_telefono_des,   
				 :ls_fax_des  
		from   anag_des_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente and
				 cod_des_cliente = :ls_cod_des_cliente ;
		if sqlca.sqlcode <> 0 then
			setnull(ls_telefono_des)
			setnull(ls_fax_des)
		end if
	else
		select telefono,   
				 fax  
		into   :ls_telefono_des,   
				 :ls_fax_des  
		from   anag_des_fornitori
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_fornitore = :ls_cod_fornitore and
				 cod_des_fornitore = :ls_cod_des_fornitore ;
		if sqlca.sqlcode <> 0 then
			setnull(ls_telefono_des)
			setnull(ls_fax_des)
		end if
	end if
end if

//Donato 15/02/2013
//creata la funzione leggi iva centraliozzata in uo_functions, in base alla gestione ed alla lingua del cliente/fornitore
//se la lingua non è gestita passare stringa vuota nel relativo argomento
guo_functions.uof_leggi_iva(il_anno_registrazione, il_num_registrazione, ls_cod_lingua, "iva_fat_ven", 	ls_codici_iva[], &
																																	ld_aliquote_iva[], &
																																	ld_imponibili_valuta[], &
																																	ld_iva_valuta[], &
																																	ls_des_esenzione_iva[], &
																																	ls_des_estesa[])
//--------------------------------------------------------------------------------------------------------------------------------

wf_leggi_scadenze(il_anno_registrazione, &
                  il_num_registrazione, &
					   ldt_scadenze[], &
					   ld_rate_scadenze[])


declare cu_dettagli cursor for 
	select   cod_tipo_det_ven, 
				cod_misura, 
				quan_fatturata, 
				prezzo_vendita, 
				sconto_1, 
				sconto_2, 
				sconto_3, 
				sconto_4, 
				sconto_5, 
				sconto_6, 
				sconto_7, 
				sconto_8, 
				sconto_9, 
				sconto_10, 
				nota_dettaglio, 
				cod_prodotto, 
				des_prodotto,
				fat_conversione_ven,
				cod_iva,
				flag_st_note_det				
	from     det_fat_ven 
	where    cod_azienda = :s_cs_xx.cod_azienda and 
				anno_registrazione = :il_anno_registrazione and 
				num_registrazione = :il_num_registrazione
	order by cod_azienda, 
				anno_registrazione, 
				num_registrazione,
				ordinamento, //stefanop 09/05/2010: progetto 140: ordinamento righe dettaglio
				prog_riga_fat_ven;

open cu_dettagli;

if not isnull(ls_nota_testata) and len(trim(ls_nota_testata)) > 0 then lb_prima_riga = true

lb_flag_prodotto_cliente = false
lb_flag_nota_dettaglio = false
lb_flag_nota_piede = false

do while 0 = 0
	if not(lb_prima_riga) and not(lb_flag_prodotto_cliente) and not(lb_flag_nota_dettaglio) then
		fetch cu_dettagli into :ls_cod_tipo_det_ven, 
									  :ls_cod_misura, 
									  :ld_quan_fatturata, 
									  :ld_prezzo_vendita,   
									  :ld_sconto_1, 
									  :ld_sconto_2, 
									  :ld_sconto_3, 
									  :ld_sconto_4, 
									  :ld_sconto_5, 
									  :ld_sconto_6, 
									  :ld_sconto_7, 
									  :ld_sconto_8, 
									  :ld_sconto_9, 
									  :ld_sconto_10, 
									  :ls_nota_dettaglio, 
									  :ls_cod_prodotto, 
									  :ls_des_prodotto,
									  :ld_fat_conversione_ven,
									  :ls_cod_iva,
								  	  :ls_flag_st_note_det;									  
	
		if sqlca.sqlcode <> 0 then
			if not isnull(ls_nota_piede) and len(trim(ls_nota_piede)) > 0 then
				lb_flag_nota_piede = true
			else
				exit
			end if
		end if
	end if
	
//------------------------------------------------- Modifica Nicola ---------------------------------------------
	if ls_flag_st_note_det = 'I' then //nota dettaglio
		select flag_st_note_ft
		  into :ls_flag_st_note_det
		  from tab_tipi_det_ven
		 where cod_azienda = :s_cs_xx.cod_azienda
		   and cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	end if		
	
	if ls_flag_st_note_det = 'N' then
		ls_nota_dettaglio = ""
	end if				
//-------------------------------------------------- Fine Modifica ----------------------------------------------	
	
	
	ld_sconto_tot = 0
   if ld_sconto_1 <> 0 and not isnull(ld_sconto_1) then ld_sconto_tot = 1 *             ( 1 - (ld_sconto_1 / 100))
   if ld_sconto_2 <> 0 and not isnull(ld_sconto_2) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_2 / 100))
   if ld_sconto_3 <> 0 and not isnull(ld_sconto_3) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_3 / 100))
   if ld_sconto_4 <> 0 and not isnull(ld_sconto_4) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_4 / 100))
   if ld_sconto_5 <> 0 and not isnull(ld_sconto_5) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_5 / 100))
   if ld_sconto_6 <> 0 and not isnull(ld_sconto_6) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_6 / 100))
   if ld_sconto_7 <> 0 and not isnull(ld_sconto_7) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_7 / 100))
   if ld_sconto_8 <> 0 and not isnull(ld_sconto_8) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_8 / 100))
   if ld_sconto_9 <> 0 and not isnull(ld_sconto_9) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_9 / 100))
   if ld_sconto_10<> 0 and not isnull(ld_sconto_10) then ld_sconto_tot = ld_sconto_tot * ( 1 - (ld_sconto_10 / 100))

	dw_report_fat_ven.insertrow(0)
	dw_report_fat_ven.setrow(dw_report_fat_ven.rowcount())

	select flag_stampa_fattura,
	       des_tipo_det_ven,
			 flag_tipo_det_ven
	into   :ls_flag_stampa_fattura,
	       :ls_des_tipo_det_ven,
			 :ls_flag_tipo_det_ven
	from   tab_tipi_det_ven
	where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and  
			 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

	if sqlca.sqlcode <> 0 then
		setnull(ls_flag_stampa_fattura)
		setnull(ls_des_tipo_det_ven)
	end if

	if lb_prima_riga then
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_nota_testata)
	elseif lb_flag_prodotto_cliente then
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_prodotto", "Vs Codice")
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_cod_prod_cliente)
		lb_flag_prodotto_cliente = false
	elseif lb_flag_nota_dettaglio then
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_nota_dettaglio)
		lb_flag_nota_dettaglio = false		
	elseif lb_flag_nota_piede then
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_nota_piede)
	else	
		ld_imponibile_riga = (ld_quan_fatturata * ld_fat_conversione_ven) * (ld_prezzo_vendita / ld_fat_conversione_ven)
		if ld_sconto_tot <> 0 then
			ld_sconto_tot = (1 - ld_sconto_tot) * 100
			ld_imponibile_riga = ld_imponibile_riga - ( (ld_imponibile_riga/100) * ld_sconto_tot)
		end if
		
		if ls_flag_stampa_fattura = 'S' then
			select anag_prodotti.des_prodotto  
			into   :ls_des_prodotto_anag  
			from   anag_prodotti  
			where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_anag)
			end if
	
			select anag_prodotti_lingue.des_prodotto  
			into   :ls_des_prodotto_lingua  
			from   anag_prodotti_lingue  
			where  anag_prodotti_lingue.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti_lingue.cod_prodotto = :ls_cod_prodotto and
					 anag_prodotti_lingue.cod_lingua = :ls_cod_lingua;
			
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_prodotto_lingua)
			end if
	
			select tab_prod_clienti.cod_prod_cliente  
			into   :ls_cod_prod_cliente  
			from   tab_prod_clienti  
			where  tab_prod_clienti.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_prod_clienti.cod_prodotto = :ls_cod_prodotto and   
					 tab_prod_clienti.cod_cliente = :ls_cod_cliente;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_cod_prod_cliente)
			end if
			
			select tab_ive.aliquota
			into   :ld_perc_iva
			from   tab_ive
			where  tab_ive.cod_azienda = :s_cs_xx.cod_azienda and  
					 tab_ive.cod_iva = :ls_cod_iva;
	
			if sqlca.sqlcode <> 0 then
				setnull(ls_des_iva)
			else
				ls_des_iva = string(ld_perc_iva,"###")
			end if
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_misura", ls_cod_misura)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_quan_ordine", ld_quan_fatturata * ld_fat_conversione_ven)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_prezzo_vendita", ld_prezzo_vendita / ld_fat_conversione_ven)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_sconto_tot", ld_sconto_tot)
			if ls_flag_tipo_det_ven <> "S" then  		// se non è uno sconto
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_val_riga", ld_imponibile_riga)
			else
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_val_riga", ld_imponibile_riga * -1)
			end if
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_iva", ls_des_iva)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_nota_dettaglio", ls_nota_dettaglio)
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_cod_prodotto", ls_cod_prodotto)
	
			if not isnull(ls_cod_lingua) and not isnull(ls_des_prodotto_lingua) then
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_prodotti_lingue_des_prodotto", ls_des_prodotto_lingua)
			elseif not isnull(ls_des_prodotto) then
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "det_fat_ven_des_prodotto", ls_des_prodotto)
			elseif not isnull(ls_des_prodotto_anag) then
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_prodotti_des_prodotto", ls_des_prodotto_anag)
			else		
				ls_des_prodotto_anag = ls_des_tipo_det_ven
				dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_prodotti_des_prodotto", ls_des_prodotto_anag)
			end if
			
			if not isnull(ls_cod_prod_cliente) and len(trim(ls_cod_prod_cliente)) > 0 then
				lb_flag_prodotto_cliente = true
			end if
			if not isnull(ls_nota_dettaglio) and len(trim(ls_nota_dettaglio)) > 0 then
				lb_flag_nota_dettaglio = true
			end if
		end if
	end if	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "parametri_azienda_stringa", ls_stringa)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "parametri_azienda_stringa_euro", ls_stringa_euro)
	if isnull(ls_cod_documento) then
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_documento", ls_cod_documento)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_anno_registrazione", il_anno_registrazione)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_num_registrazione", il_num_registrazione)
	else
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_documento", ls_cod_documento)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_numeratore_documento", ls_numeratore_doc)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_anno_documento", ll_anno_documento)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_num_documento", ll_num_documento)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_data_fattura", ldt_data_fattura)
	end if
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_tipo_fat_ven", ls_cod_tipo_fat_ven)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_data_registrazione", ldt_data_registrazione)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_cliente", ls_cod_cliente)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_valuta", ls_cod_valuta)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_pagamento", ls_cod_pagamento)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_sconto", ld_sconto)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_banca_clien_for", ls_cod_banca_clien_for)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_num_ord_cliente", ls_num_ord_cliente)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_data_ord_cliente", ldt_data_ord_cliente)
	
//---------------------------------- Modifica Nicola ---------------------------------------
	if (isnull(ls_rag_soc_1) or len(ls_rag_soc_1) < 1) and &
		(isnull(ls_rag_soc_1_dest_fat) or len(ls_rag_soc_1_dest_fat) < 1) then
		ls_rag_soc_1 = ls_rag_soc_1_cli
		ls_rag_soc_2 = ls_rag_soc_2_cli
		ls_indirizzo = ls_indirizzo_cli
		ls_cap = ls_cap_cli
		ls_localita = ls_localita_cli
		ls_provincia = ls_provincia_cli	
	end if
	
	if (not isnull(ls_rag_soc_1_dest_fat) or len(ls_rag_soc_1_dest_fat) > 0) and ls_flag_tipo_fat_ven <> "I" then
		ls_rag_soc_1 = ls_rag_soc_1_dest_fat
		ls_rag_soc_2 = ls_rag_soc_2_dest_fat
		ls_indirizzo = ls_indirizzo_dest_fat
		ls_cap = ls_cap_dest_fat
		ls_localita = ls_localita_dest_fat
		ls_provincia = ls_provincia_dest_fat
	end if
//----------------------------------- Fine Modifica ----------------------------------------	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_rag_soc_1", ls_rag_soc_1)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_rag_soc_2", ls_rag_soc_2)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_indirizzo", ls_indirizzo)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_localita", ls_localita)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_frazione", ls_frazione)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cap", ls_cap)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_provincia", ls_provincia)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_nota_testata", ls_nota_testata)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_nota_piede", ls_nota_piede)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_porto", ls_cod_porto)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_resa", ls_cod_resa)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_agente_1", ls_cod_agente_1)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_agenti_rag_soc_1", ls_anag_agenti_rag_soc_1)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_agente_2", ls_cod_agente_2)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_agenti_rag_soc_2", ls_anag_agenti_rag_soc_2)
	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_tot_fattura_valuta", ld_tot_fattura_valuta)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_tot_fattura_euro", ld_tot_fattura_euro)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "formato", ls_formato)
		
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_imponibile_iva_valuta", 0)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_iva_valuta", 0)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cod_vettore", ls_cod_vettore)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_num_colli", ll_num_colli)

//---------------------------------------- Modifica Nicola -----------------------------------------------------
	select des_causale
	  into :ls_des_causale
	  from tab_causali_trasp
	 where cod_azienda = :s_cs_xx.cod_azienda
	   and cod_causale = :ls_causale_trasporto;
	
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in ricerca causale trasporto: stampa interrotta", Stopsign!)
		return
	elseif sqlca.sqlcode = 100 then
		ls_des_causale = ""
	end if	
//------------------------------------------ Fine Modifica ----------------------------------------------------- 	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_causale_trasporto", ls_des_causale)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_telefono", ls_telefono_cliente)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_fax", ls_fax_cliente)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_cambio_ven", ld_cambio_ven)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_rag_soc_1", ls_rag_soc_1_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_rag_soc_2", ls_rag_soc_2_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_indirizzo", ls_indirizzo_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_localita", ls_localita_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_frazione", ls_frazione_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_cap", ls_cap_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_provincia", ls_provincia_cli)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_partita_iva", ls_partita_iva)
	
	//Giulio 30/01/2012: Modifica portata dalla versione 11.5 per Sicurservice.
	if guo_functions.uof_dw_has_column(dw_report_fat_ven, "anag_clienti_cod_fiscale") then
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_cod_fiscale", ls_cod_fiscale)
	end if
	//fine modifica
	
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_telefono", ls_telefono_des)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_clienti_fax", ls_fax_des)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_tipi_fat_ven_des_tipo_fat_ven", ls_des_tipo_fat_ven)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_pagamenti_des_pagamento", ls_des_pagamento)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_pagamenti_sconto", ld_sconto_pagamento)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_pagamenti_lingue_des_pagamento", ls_des_pagamento_lingua)
	if ls_flag_tipo_pagamento = "R" then
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_clien_for_des_banca", ls_des_banca)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_clien_for_cod_abi", ls_cod_abi)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_clien_for_cod_cab", ls_cod_cab)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_clien_for_iban", ls_iban)
	else
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_des_banca", ls_des_banca)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_cod_abi", ls_cod_abi)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_cod_cab", ls_cod_cab)
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_banche_iban", ls_iban)
	end if
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_valute_des_valuta", ls_des_valuta)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_valute_lingue_des_valuta", ls_des_valuta_lingua)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_porti_des_porto", ls_des_porto)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_porti_lingue_des_porto", ls_des_porto_lingua)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_rese_des_resa", ls_des_resa)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tab_rese_lingue_des_resa", ls_des_resa_lingua)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_rag_soc_1", ls_vettore_rag_soc_1)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_indirizzo", ls_vettore_indirizzo)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_cap", ls_vettore_cap)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_localita", ls_vettore_localita)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "anag_vettori_provincia", ls_vettore_provincia)

	//##################################################################################################
	ll_num_aliquote = upperbound(ld_aliquote_iva) - 1
	//------------------------------------------------------------------------------------------------
	if ll_num_aliquote > 0 then
		if ld_aliquote_iva[1] > 0 and not isnull(ld_aliquote_iva[1]) then
			ls_des_aliquota = g_str.format("imponibile: $2  Imposta: $3   Iva: $1 %", string(ld_aliquote_iva[1],"#0"), string(ld_imponibili_valuta[1],ls_formato), string(ld_iva_valuta[1], ls_formato) )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_1", ld_aliquote_iva[1])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_1", ld_imponibili_valuta[1])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_1", ld_iva_valuta[1])
		else
			if len(ls_des_estesa[1]) > 0 then  ls_des_esenzione_iva[1]  = ls_des_estesa[1]
			ls_des_aliquota = g_str.format("Imponibile: $1  $2", string(ld_imponibili_valuta[1],ls_formato), ls_des_esenzione_iva[1] )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_1", ld_imponibili_valuta[1])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_1", ls_des_esenzione_iva[1])
		end if
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_1", ls_des_aliquota)
	end if
	//------------------------------------------------------------------------------------------------
	if ll_num_aliquote > 1 then
		if ld_aliquote_iva[2] > 0 and not isnull(ld_aliquote_iva[2]) then
			ls_des_aliquota = g_str.format("imponibile: $2  Imposta: $3   Iva: $1 %", string(ld_aliquote_iva[2],"#0"), string(ld_imponibili_valuta[2],ls_formato), string(ld_iva_valuta[2], ls_formato) )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_2", ld_aliquote_iva[2])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_2", ld_imponibili_valuta[2])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_2", ld_iva_valuta[2])
		else
			if len(ls_des_estesa[2]) > 0 then  ls_des_esenzione_iva[2]  = ls_des_estesa[2]
			ls_des_aliquota = g_str.format("Imponibile: $1  $2", string(ld_imponibili_valuta[2],ls_formato), ls_des_esenzione_iva[2] )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_2", ld_imponibili_valuta[2])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_2", ls_des_esenzione_iva[2])
		end if
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_2", ls_des_aliquota)
	end if
	//------------------------------------------------------------------------------------------------
	if ll_num_aliquote > 2 then
		if ld_aliquote_iva[3] > 0 and not isnull(ld_aliquote_iva[3]) then
			ls_des_aliquota = g_str.format("imponibile: $2  Imposta: $3   Iva: $1 %", string(ld_aliquote_iva[3],"#0"), string(ld_imponibili_valuta[3],ls_formato), string(ld_iva_valuta[3], ls_formato) )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_3", ld_aliquote_iva[3])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_3", ld_imponibili_valuta[3])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_3", ld_iva_valuta[3])
		else
			if len(ls_des_estesa[3]) > 0 then  ls_des_esenzione_iva[3]  = ls_des_estesa[3]
			ls_des_aliquota = g_str.format("Imponibile: $1  $2", string(ld_imponibili_valuta[3],ls_formato), ls_des_esenzione_iva[3] )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_3", ld_imponibili_valuta[3])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_3", ls_des_esenzione_iva[3])
		end if
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_3", ls_des_aliquota)
	end if
	//------------------------------------------------------------------------------------------------
	if ll_num_aliquote > 3 then
		if ld_aliquote_iva[4] > 0 and not isnull(ld_aliquote_iva[4]) then
			ls_des_aliquota = g_str.format("imponibile: $2  Imposta: $3   Iva: $1 %", string(ld_aliquote_iva[4],"#0"), string(ld_imponibili_valuta[4],ls_formato), string(ld_iva_valuta[4], ls_formato) )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_4", ld_aliquote_iva[4])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_4", ld_imponibili_valuta[4])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_4", ld_iva_valuta[4])
		else
			if len(ls_des_estesa[4]) > 0 then  ls_des_esenzione_iva[4]  = ls_des_estesa[4]
			ls_des_aliquota = g_str.format("Imponibile: $1  $2", string(ld_imponibili_valuta[4],ls_formato), ls_des_esenzione_iva[4] )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_4", ld_imponibili_valuta[4])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_4", ls_des_esenzione_iva[4])
		end if
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_4", ls_des_aliquota)
	end if
	//------------------------------------------------------------------------------------------------
	if ll_num_aliquote > 4 then
		if ld_aliquote_iva[5] > 0 and not isnull(ld_aliquote_iva[5]) then
			ls_des_aliquota = g_str.format("imponibile: $2  Imposta: $3   Iva: $1 %", string(ld_aliquote_iva[5],"#0"), string(ld_imponibili_valuta[5],ls_formato), string(ld_iva_valuta[5], ls_formato) )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_5", ld_aliquote_iva[5])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_5", ld_imponibili_valuta[5])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_5", ld_iva_valuta[5])
		else
			if len(ls_des_estesa[5]) > 0 then  ls_des_esenzione_iva[5]  = ls_des_estesa[5]
			ls_des_aliquota = g_str.format("Imponibile: $1  $2", string(ld_imponibili_valuta[5],ls_formato), ls_des_esenzione_iva[5] )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_5", ld_imponibili_valuta[5])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_5", ls_des_esenzione_iva[5])
		end if
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_5", ls_des_aliquota)
	end if
	//------------------------------------------------------------------------------------------------
	if ll_num_aliquote > 5 then
		if ld_aliquote_iva[6] > 0 and not isnull(ld_aliquote_iva[6]) then
			ls_des_aliquota = g_str.format("imponibile: $2  Imposta: $3   Iva: $1 %", string(ld_aliquote_iva[6],"#0"), string(ld_imponibili_valuta[6],ls_formato), string(ld_iva_valuta[6], ls_formato) )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_cod_iva_6", ld_aliquote_iva[6])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_6", ld_imponibili_valuta[6])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_importo_iva_6", ld_iva_valuta[6])
		else
			if len(ls_des_estesa[6]) > 0 then  ls_des_esenzione_iva[6]  = ls_des_estesa[6]
			ls_des_aliquota = g_str.format("Imponibile: $1  $2", string(ld_imponibili_valuta[6],ls_formato), ls_des_esenzione_iva[6] )
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_imponibile_iva_6", ld_imponibili_valuta[6])
			//dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_6", ls_des_esenzione_iva[6])
		end if
		dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "iva_fat_ven_des_esenzione_6", ls_des_aliquota)
	end if
	//##################################################################################################

	if upperbound(ldt_scadenze) > 0 then
		if date(ldt_scadenze[1]) > date("01/01/1900") and not isnull(ldt_scadenze[1]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_1", ldt_scadenze[1])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_1", ld_rate_scadenze[1])
		end if
	end if
	if upperbound(ldt_scadenze) > 1 then
		if date(ldt_scadenze[2]) > date("01/01/1900") and not isnull(ldt_scadenze[2]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_2", ldt_scadenze[2])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_2", ld_rate_scadenze[2])
		end if
	end if
	if upperbound(ldt_scadenze) > 2 then
		if date(ldt_scadenze[3]) > date("01/01/1900") and not isnull(ldt_scadenze[3]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_3", ldt_scadenze[3])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_3", ld_rate_scadenze[3])
		end if
	end if
	if upperbound(ldt_scadenze) > 3 then
		if date(ldt_scadenze[4]) > date("01/01/1900") and not isnull(ldt_scadenze[4]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_4", ldt_scadenze[4])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_4", ld_rate_scadenze[4])
		end if
	end if
	if upperbound(ldt_scadenze) > 4 then
		if date(ldt_scadenze[5]) > date("01/01/1900") and not isnull(ldt_scadenze[5]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_5", ldt_scadenze[5])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_5", ld_rate_scadenze[5])
		end if
	end if
	if upperbound(ldt_scadenze) > 5 then
		if date(ldt_scadenze[6]) > date("01/01/1900") and not isnull(ldt_scadenze[6]) then
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_scadenza_6", ldt_scadenze[6])
			dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "scad_fat_ven_rata_6", ld_rate_scadenze[6])
		end if
	end if

	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_imponibile_iva_valuta", ld_imponibile_iva_valuta)
	dw_report_fat_ven.setitem(dw_report_fat_ven.getrow(), "tes_fat_ven_iva_valuta", ld_importo_iva_valuta)
	lb_prima_riga = false
	if lb_flag_nota_piede then exit
loop
close cu_dettagli;
dw_report_fat_ven.reset_dw_modified(c_resetchildren)
dw_report_fat_ven.change_dw_current()

if ib_nascondi_cod_documento then
	
	try
	
		dw_report_fat_ven.object.tes_fat_ven_cod_documento.visible = false
		dw_report_fat_ven.object.t_3.visible = false
		dw_report_fat_ven.object.tes_fat_ven_numeratore_documento.visible = false
		
	catch(RuntimeError ex)
	end try
	
end if

commit;

// ------- Michela: creazione pdf
if dw_report_fat_ven.RowCount()<1 then
	g_mb.messagebox("APICE","La fattura non possiede nessun dettaglio: impossibile creare il pdf.", Stopsign!)
	return
end if

if ll_anno_documento > 0 and ll_num_documento > 0 and not isnull(ll_anno_documento) and not isnull(ll_num_documento) then

	

	uo_archivia_pdf luo_pdf
	luo_pdf = create uo_archivia_pdf
	ll_ret = luo_pdf.uof_crea_pdf(dw_report_fat_ven, ref l_blob)
	if ll_ret < 0 then
		g_mb.messagebox("APICE","Impossibile creare il PDF; verificare le impostazione del sistema.", Stopsign!)
		return
	end if
	
	SELECT prog_mimetype  
	INTO   :ll_prog_mimytype  
	FROM   tab_mimetype  
	WHERE  cod_azienda = :s_cs_xx.cod_azienda and
			 estensione = 'PDF'   ;
	if sqlca.sqlcode = 100 then
		g_mb.messagebox("APICE","Impossibile archiviare il documento: impostare il mimetype")
		return
	end if
	
	delete from tes_fat_ven_note
	where      cod_azienda = :s_cs_xx.cod_azienda and
	           anno_registrazione = :il_anno_registrazione and
				  num_registrazione = :il_num_registrazione and
				  cod_nota = 'FAT';
	
	
	ls_stringa = "Fattura nr " + string(ll_anno_documento) + "/" + string(ll_num_documento) + " del " + string(ldt_data_fattura,"dd/mm/yyyy")
	ls_note = g_str.format("Fattura creata dall'utente $1 in data $2 ore $3", s_cs_xx.cod_utente, string(today(),"dd/mm/yyyy"), string(now()) )
	INSERT INTO tes_fat_ven_note  
			( cod_azienda,   
			  anno_registrazione,   
			  num_registrazione,   
			  cod_nota,   
			  des_nota,   
			  prog_mimetype,
			  note)  
	VALUES ( :s_cs_xx.cod_azienda,   
			  :il_anno_registrazione,   
			  :il_num_registrazione,   
			  'FAT',   
			  :ls_stringa,   
			  :ll_prog_mimytype,
			  :ls_note)  ;
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in memorizzazione metadati fattura PDF" + sqlca.sqlerrtext)
		rollback;
		return
	end if
	
	ls_db = f_db()
	if ls_db = "MSSQL" then
	
		if not guo_functions.uof_create_transaction_from( sqlca, sqlcb, ls_errore) then
			g_mb.error(ls_errore)
			return 
		end if
		ll_ret = len(l_blob)
		
		// devo farfe il commit del record inserito nella transazione precedente
		commit using sqlca;
	
		updateblob tes_fat_ven_note
		set        note_esterne = :l_blob
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_registrazione = :il_anno_registrazione and
					  num_registrazione = :il_num_registrazione and
					  cod_nota = 'FAT'
		using sqlcb;
		if sqlcb.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore in memorizzazione (blob) fattura PDF" + sqlcb.sqlerrtext)
			rollback using sqlcb;
			rollback using sqlca;
			return
		end if 
		commit using sqlcb;
		
		destroy sqlcb;
		
		
	else 
		updateblob tes_fat_ven_note
		set        note_esterne = :l_blob
		where      cod_azienda = :s_cs_xx.cod_azienda and
					  anno_registrazione = :il_anno_registrazione and
					  num_registrazione = :il_num_registrazione and
					  cod_nota = 'FAT';
		if sqlca.sqlcode < 0 then
			g_mb.messagebox("APICE","Errore in memorizzazione (blob) fattura PDF" + sqlca.sqlerrtext)
			rollback;
			return
		end if
	end if 
	
	destroy luo_pdf
	commit;
	
end if


end subroutine

public function integer wf_email ();// funzione che provvedere all'invio MAIL della fattura corrente

integer 	li_ret
long    	ll_anno_documento, ll_num_documento, ll_prog_mimytype
string 	ls_path, ls_messaggio, ls_destinatari[], ls_allegati[], ls_subject, ls_message, &
			ls_rag_soc_1,ls_cod_cliente, ls_nota, ls_email_utente, ls_cc[], ls_errore
datetime ldt_data_fattura
uo_archivia_pdf luo_archivia_pdf
uo_outlook      luo_outlook
uo_archivia_pdf luo_pdf

luo_outlook = CREATE uo_outlook
luo_pdf = create uo_archivia_pdf

if ib_nr_nr then
	luo_outlook.ib_silent_mode = true
end if

ls_path = luo_pdf.uof_crea_pdf_path(dw_report_fat_ven)

if ls_path = "errore" then
	g_mb.messagebox ("Errore","Errore nella creazione del file pdf.")
	return -1
end if

ls_destinatari[1] = is_email_amministrazione[il_corrente]
if not isnull( is_email_utente[il_corrente] ) then
	ls_cc[1] = is_email_utente[il_corrente]
end if
ls_allegati[1]    = ls_path

select anno_documento,
       num_documento,
		 data_fattura, 
		 cod_cliente
into   :ll_anno_documento,
		 :ll_num_documento,
		 :ldt_data_fattura,
	    :ls_cod_cliente
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and	
       anno_registrazione = :il_anno[il_corrente] and
		 num_registrazione = :il_num[il_corrente];

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca dati fattura (wf_email)~r~n" + sqlca.sqlerrtext)
end if


select rag_soc_1
into   :ls_rag_soc_1
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca dati cliente (wf_email)~r~n" + sqlca.sqlerrtext)
end if

ls_subject = ls_rag_soc_1+" - Invio Fattura nr. " + string(ll_anno_documento) + "/" + string(ll_num_documento)

if not isnull(ldt_data_fattura) then ls_subject += " del " + string(ldt_data_fattura,"dd/mm/yyyy")


wf_testo_mail(ls_rag_soc_1, il_anno[il_corrente], il_num[il_corrente], ldt_data_fattura, ls_message)

// stefanop 16/02/2012: sostituito metodo con l'invio sendmail 
// ------------------------------------------------------------------------------------------------------
if not luo_outlook.uof_invio_sendmail( ls_destinatari[], ls_cc[], ls_subject, ls_message, ls_allegati[], ref ls_messaggio) then
	g_mb.messagebox("APICE", "Errore in fase di invio fattura~r~n" + ls_messaggio)
else
	// tutto bene; memorizzo pure in tes_fat_ven l'invio
	update tes_fat_ven
	set    flag_email_inviata = 'S'
	where  cod_azienda = :s_cs_xx.cod_azienda and	
			 anno_registrazione = :il_anno[il_corrente] and
			 num_registrazione = :il_num[il_corrente];
	
	if wf_memorizza_blob(il_anno[il_corrente], il_num[il_corrente], ls_path, ls_errore) < 0 then
		g_mb.messagebox("APICE", "Errore in fase di invio fattura~r~n" + ls_errore)
		rollback;
	else
		commit;
	end if
	
end if
// ------------------------------------------------------------------------------------------------------

destroy luo_archivia_pdf
destroy luo_outlook

filedelete(ls_path)

return 0
end function

public function integer wf_replace_marker (ref string as_source, string as_find, string as_replace, boolean ab_case_sensitive);/*
Function fof_replace_text
Created by: Michele
Creation Date: 13/12/2007
Comment: Given a string <as_source>, replaces all occurencies of <as_find> with <as_replace>

as_source                                                          Reference to the string to process
as_find                                                               The text we wish to change
as_replace                                                        The new text to be put in place of <as_find>

Return Values
Value                                                                   Comments
 1                                                                                           Everything OK
-1                                                                                          Some error occured
*/
long		ll_pos

 if isnull(as_source) or as_source = "" then
	return -1
end if

if isnull(as_find) or as_find = "" then
	return -1
end if

 ll_pos = 1

do
	 if ab_case_sensitive then
		ll_pos = pos(as_source,as_find,ll_pos)
	 else
		ll_pos = pos(lower(as_source),lower(as_find),ll_pos)
	 end if

	 if ll_pos = 0 then
		continue
	 end if
	 
	 as_source = replace(as_source,ll_pos,len(as_find),as_replace)	 
	 ll_pos += len(as_replace)

loop while ll_pos > 0

return 1


end function

public function integer wf_testo_mail (string fs_rag_soc_1, long fl_anno, long fl_numero, datetime fdt_data, ref string fs_testo);string 			ls_default, ls_path, ls_nome_file, ls_testo, ls_mark_ragsoc, ls_mark_numero
string			ls_mark_data, ls_rag_soc_azienda, ls_tel, ls_fax

integer			li_FileNum

long				ll_pos_ragsoc, ll_pos_numero, ll_pos_data

//parametri
ls_nome_file = "testo_mail.txt"
ls_mark_ragsoc = "[RAGIONESOCIALE]"
ls_mark_numero = "[NUMERO]"
ls_mark_data = "[DATA]"

//il percorso contiene anche il backslash finale
ls_path = s_cs_xx.volume + s_cs_xx.risorse +"11.5\" + ls_nome_file

ls_default = "Gentile Cliente " + fs_rag_soc_1 + ", " + &
					"con la presente Le trasmettiamo in allegato la nostra fattura N°" +  string(fl_anno) + "/" + &
					string(fl_numero)
if not isnull(fdt_data) then ls_default+=  " del " + string(fdt_data,"dd/mm/yyyy") + "."
	
ls_default += "~r~n"
ls_default += "Per qualunque chiarimento siamo a Vostra disposizione.~r~n~r~n" + &
					"Cordiali Saluti~r~n"

select rag_soc_1, telefono, fax
into :ls_rag_soc_azienda, :ls_tel, :ls_fax
from aziende
where cod_azienda=:s_cs_xx.cod_azienda;

if ls_rag_soc_azienda<>"" and not isnull(ls_rag_soc_azienda) then ls_default += ls_rag_soc_azienda + "~r~n"
if ls_tel<>"" and not isnull(ls_tel) then ls_default += "Tel. "+ls_tel + "~r~n"
if ls_fax<>"" and not isnull(ls_fax) then ls_default += "Fax. "+ls_fax + "~r~n"

ls_default += is_disclaimer

if not FileExists(ls_path) then
	
	//questo alert lo commentiamo, perchè altrimenti potrebbe scocciare l'utente...
	//se il cliente chiede una personalizzazione del testo basterà creargli questo file
	
	/*
	g_mb.messagebox("APICE","Il file di configurazione del testo del messaggio e-mail '"+ls_path+&
												"' non esiste oppure è stato spostato!~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	*/
	//-------------------------------------------------------------------------------------------------------
	
	fs_testo = ls_default
	return 1
end if

//leggo dal file il testo personalizzato
li_FileNum = FileOpen(ls_path, TextMode!)
FileReadEx(li_FileNum, ls_testo)
FileClose(li_FileNum)


//sostituire i marcatori con i valori opportuni
//[RAGIONESOCIALE]
//[NUMERO]
//[DATA]

ll_pos_ragsoc = pos(ls_testo, ls_mark_ragsoc)
ll_pos_numero = pos(ls_testo, ls_mark_numero)
ll_pos_data = pos(ls_testo, ls_mark_data)

if ll_pos_ragsoc>0 then
else
	g_mb.messagebox("APICE","Marcatore "+ls_mark_ragsoc+" non trovato nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

if ll_pos_numero>0 then
else
	g_mb.messagebox("APICE","Marcatore "+ls_mark_numero+" non trovato nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

if ll_pos_data>0 then
else
	g_mb.messagebox("APICE","Marcatore "+ls_mark_data+" non trovato nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

fs_testo = ls_testo

//ragione sociale
if wf_replace_marker(fs_testo, ls_mark_ragsoc, fs_rag_soc_1, false)=1 then
else
	g_mb.messagebox("APICE","Errore in sostituzione marcatore "+ls_mark_ragsoc+" nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

//  anno/numero
if wf_replace_marker(fs_testo, ls_mark_numero, string(fl_anno)+"/"+string(fl_numero), false)=1 then
else
	g_mb.messagebox("APICE","Errore in sostituzione marcatore "+ls_mark_numero+" nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

//data
if wf_replace_marker(fs_testo, ls_mark_data, string(fdt_data,"dd/mm/yyyy"), false)=1 then
else
	g_mb.messagebox("APICE","Errore in sostituzione marcatore "+ls_mark_numero+" nel file di configurazione del testo del messaggio e-mail '"+ls_path+"~n~r"+&
												"Verrà usato un testo di default!",Exclamation!)
	fs_testo = ls_default
	return 1
end if

return 1
end function

public subroutine wf_stampa (integer copie);string ls_spf, ls_error
long ll_i,ll_zoom


// il parametro ZFV determina lo zoom con cui le fatture devono essere stampate
select numero
into   :ll_zoom
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'ZFV';
		 
if sqlca.sqlcode <> 0 then ll_zoom = 100

if isnull(ll_zoom) then ll_zoom = 100


// il parametro SPF Stampa Prezzi Fattura determina se i prezzi devono essere stampati oppure no nella fattura accompagnatoria
// il parametro è una stringa del tipo "1001" dove 0=non stampa 1=stampa; la posizione del caratterere indica se stampare o no i prezzi
select stringa
into   :ls_spf
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_parametro = 'SPF';
		 
if sqlca.sqlcode <> 0 then ls_spf = "111111"

if isnull(ll_zoom) then ls_spf = "111111"


il_totale_pagine = 1

dw_report_fat_ven.object.datawindow.zoom = ll_zoom

// stefanop: Filigrana
ls_error = dw_report_fat_ven.modify("DataWindow.Picture.File='" + is_path_filigrana + "'")
if not isnull(ls_error) and ls_error <> "" then g_mb.error("File", ls_error)

ls_error = dw_report_fat_ven.modify("DataWindow.Picture.Mode=1")
if not isnull(ls_error) and ls_error <> "" then g_mb.error("Mode", ls_error)

ls_error = dw_report_fat_ven.modify("DataWindow.Picture.Transparency=80")
if not isnull(ls_error) and ls_error <> "" then g_mb.error("Transparency",ls_error)

ls_error = dw_report_fat_ven.modify("DataWindow.Print.Background	='Yes'")
if not isnull(ls_error) and ls_error <> "" then g_mb.error("Background", ls_error)
// ----

if ib_email then
	// processo di invio tramite EMAIL

	// invio la mail solo della prima copia
	if is_flag_email[il_corrente] = "S" then 
		wf_email()
	end if

else

	for il_pagina_corrente = 1 to il_totale_pagine
		
		for ll_i = 1 to copie
			/* Il cliente della fattura attuale accetta la fattura via mail,
				quindi NON invio la prima copia del destinatario              */
			if is_flag_email[il_corrente] = "S" and ll_i = 1 then continue
		
			// processo di normale stampa delle copie previste per il cliente
			
			choose case ll_i
				case 1 
					dw_report_fat_ven.object.st_copia.text = "Copia Destinatario"
				case 2
					dw_report_fat_ven.object.st_copia.text = "Copia Vettore / Raccolta Firme"
				case 3
					dw_report_fat_ven.object.st_copia.text = "Copia Mittente"
				case else  //  dalla quarta in poi.
					dw_report_fat_ven.object.st_copia.text = "Copia Interna"
			end choose
			
			ls_error = dw_report_fat_ven.modify("DataWindow.brushmode=0")
			
			// Aggiunto su richiesta di Claudia 11/01/2012: oscuramento prezzi nelle pagine dopo la prima.
			if is_flag_tipo_fat_ven = "I" then
				
				if mid(ls_spf, ll_i, 1) = "0" then
					// nascondi prezzi in fattura
					dw_report_fat_ven.object.det_fat_ven_cod_iva.visible = 0
					dw_report_fat_ven.object.det_fat_ven_val_riga.visible = 0
					dw_report_fat_ven.object.det_fat_ven_sconto_2.visible = 0
					dw_report_fat_ven.object.det_fat_ven_sconto_1.visible = 0
					dw_report_fat_ven.object.compute_4.visible = 0
					dw_report_fat_ven.object.scad_fat_ven.visible = 0
					dw_report_fat_ven.object.tes_fat_ven_sconto.visible = 0
					dw_report_fat_ven.object.tes_fat_ven_importo_sconto.visible = 0
					dw_report_fat_ven.object.tes_fat_ven_tot_fattura_euro.visible = 0
					dw_report_fat_ven.object.tes_fat_ven_tot_fattura_valuta.visible = 0
					dw_report_fat_ven.object.tes_fat_ven_imponibile_iva_valuta.visible = 0
					dw_report_fat_ven.object.tes_fat_ven_iva_valuta.visible = 0
					
					dw_report_fat_ven.object.iva_fat_ven_cod_iva_1.visible	= 0
					dw_report_fat_ven.object.iva_fat_ven_cod_iva_2.visible	= 0
					dw_report_fat_ven.object.iva_fat_ven_cod_iva_3.visible	= 0
					dw_report_fat_ven.object.iva_fat_ven_cod_iva_4.visible	= 0
					
					dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_1.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_importo_iva_1.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_des_esenzione_1.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_2.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_importo_iva_2.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_des_esenzione_2.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_3.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_importo_iva_3.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_des_esenzione_3.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_4.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_importo_iva_4.visible = 0
					dw_report_fat_ven.object.iva_fat_ven_des_esenzione_4.visible = 0
					
					// Visualizzo Filigrana
					ls_error = dw_report_fat_ven.modify("DataWindow.brushmode=6")
					if not isnull(ls_error) and ls_error <> "" then g_mb.error("Brushmode", ls_error)
					
				else
					dw_report_fat_ven.object.det_fat_ven_cod_iva.visible 					= "0~tif((det_fat_ven_val_riga <> 0), 1, 0)"
					dw_report_fat_ven.object.det_fat_ven_val_riga.visible 					= "0~tif ( det_fat_ven_val_riga <> 0 , 1 , 0 )"
					dw_report_fat_ven.object.det_fat_ven_sconto_2.visible 					= 1
					dw_report_fat_ven.object.det_fat_ven_sconto_1.visible 					= 1
					dw_report_fat_ven.object.compute_4.visible 								= "0~tif( det_fat_ven_prezzo_vendita <> 0,1,0)"
					dw_report_fat_ven.object.scad_fat_ven.visible 							= "0~tif(len( scad_fat_ven ) > 1 AND  page() = pagecount(), 1, 0)"
					dw_report_fat_ven.object.tes_fat_ven_sconto.visible 					= "0~tif (tes_fat_ven_sconto > 0, 1, 0)"
					dw_report_fat_ven.object.tes_fat_ven_importo_sconto.visible 			= "0~tif (tes_fat_ven_importo_sconto > 0, 1, 0)"
					dw_report_fat_ven.object.tes_fat_ven_tot_fattura_euro.visible 		= "0~tif(page() = pagecount(),1,0)"
					dw_report_fat_ven.object.tes_fat_ven_tot_fattura_valuta.visible		= "0~tif(parametri_azienda_stringa_euro <>  tes_fat_ven_cod_valuta and tes_fat_ven_tot_fattura_valuta <> 0 and page() = pagecount(),1,0)"
					dw_report_fat_ven.object.tes_fat_ven_imponibile_iva_valuta.visible 	= "0~tif(tes_fat_ven_tot_fattura_valuta <> 0 and page() = pagecount(),1,0)"
					dw_report_fat_ven.object.tes_fat_ven_iva_valuta.visible 				= "0~tif(tes_fat_ven_tot_fattura_valuta <> 0 and page() = pagecount(),1,0)"

					dw_report_fat_ven.object.iva_fat_ven_cod_iva_1.visible 				= "0~tif(page() = pagecount(),1,0)"
					dw_report_fat_ven.object.iva_fat_ven_cod_iva_2.visible 				= "0~tif(page() = pagecount(),1,0)"
					dw_report_fat_ven.object.iva_fat_ven_cod_iva_3.visible 				= "0~tif(page() = pagecount(),1,0)"
					dw_report_fat_ven.object.iva_fat_ven_cod_iva_4.visible 				= "0~tif(page() = pagecount(),1,0)"

					dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_1.visible 		= "0~tif(iva_fat_ven_imponibile_iva_1 <> 0 and page() = pagecount(),1,0)"
					dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_2.visible 		= "0~tif(iva_fat_ven_imponibile_iva_2 <> 0 and page() = pagecount(),1,0)"
					dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_3.visible 		= "0~tif(iva_fat_ven_imponibile_iva_3 <> 0 and page() = pagecount(),1,0)"
					dw_report_fat_ven.object.iva_fat_ven_imponibile_iva_4.visible 		= "0~tif(iva_fat_ven_imponibile_iva_4 <> 0 and page() = pagecount(),1,0)"
					dw_report_fat_ven.object.iva_fat_ven_importo_iva_1.visible			= "0~tif(iva_fat_ven_importo_iva_1 <> 0 and page() = pagecount(),1,0)"
					dw_report_fat_ven.object.iva_fat_ven_importo_iva_2.visible 			= "0~tif(iva_fat_ven_importo_iva_2 <> 0 and page() = pagecount(),1,0)"
					dw_report_fat_ven.object.iva_fat_ven_importo_iva_3.visible 			= "0~tif(iva_fat_ven_importo_iva_3 <> 0 and page() = pagecount(),1,0)"
					dw_report_fat_ven.object.iva_fat_ven_importo_iva_4.visible 			= "0~tif(iva_fat_ven_importo_iva_4 <> 0 and page() = pagecount(),1,0)"
					dw_report_fat_ven.object.iva_fat_ven_des_esenzione_1.visible 		= "0~tif((isnull(iva_fat_ven_importo_iva_1) or iva_fat_ven_importo_iva_1 = 0) and page() = pagecount(),1,0)"
					dw_report_fat_ven.object.iva_fat_ven_des_esenzione_2.visible 		= "0~tif((isnull(iva_fat_ven_importo_iva_2) or iva_fat_ven_importo_iva_2 = 0) and page() = pagecount(),1,0)"
					dw_report_fat_ven.object.iva_fat_ven_des_esenzione_3.visible 		= "0~tif((isnull(iva_fat_ven_importo_iva_3) or iva_fat_ven_importo_iva_3 = 0) and page() = pagecount(),1,0)"
					dw_report_fat_ven.object.iva_fat_ven_des_esenzione_4.visible 		= "0~tif((isnull(iva_fat_ven_importo_iva_4) or iva_fat_ven_importo_iva_4 = 0) and page() = pagecount(),1,0)"
				end if		
				
			end if
			
			dw_report_fat_ven.triggerevent("pcd_print")
				
		next
		
		this.postevent("ue_avviso_spedizione")
	
	next
	
end if

dw_report_fat_ven.object.st_copia.text = ""

end subroutine

public function integer wf_memorizza_blob (long fl_anno_registrazione, long fl_num_registrazione, string fs_path, ref string as_errore);string ls_note, ls_nota, ls_cod_nota, ls_db, ls_errore
long ll_prog_mimytype,ll_ret
blob l_blob
transaction sqlcb


ll_ret = guo_functions.uof_file_to_blob( fs_path, ref l_blob)

if ll_ret < 0 then return -1

ll_ret = len(l_blob)


SELECT prog_mimetype  
INTO   :ll_prog_mimytype  
FROM   tab_mimetype  
WHERE  cod_azienda = :s_cs_xx.cod_azienda and
		 estensione = 'PDF'   ;
if sqlca.sqlcode = 100 then
	g_mb.messagebox("APICE","Impossibile archiviare il documento: impostare il mimetype")
	return 0
end if

setnull(ls_cod_nota)

select max(cod_nota)
into   :ls_cod_nota
from   tes_fat_ven_note
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :fl_anno_registrazione and
		 num_registrazione = :fl_num_registrazione and
		 cod_nota like 'F%';
		 
if isnull(ls_cod_nota) or len(ls_cod_nota) < 1 then
	ls_cod_nota = "F01"
else
	ls_cod_nota = right(ls_cod_nota, 2)
	ll_ret = long(ls_cod_nota)
	ll_ret ++
	ls_cod_nota = "F" + string(ll_ret, "00")
end if

ls_nota = "Invio fattura PDF tramite e-mail"
ls_note = "Fattura Inviata dall'utente " + s_cs_xx.cod_utente + "~r~n" + &
          "Data invio:" + string(today(), "dd/mm/yyyy") + "  Ora invio:" + string(now(), "hh:mm:ss")

INSERT INTO tes_fat_ven_note  
		( cod_azienda,   
		  anno_registrazione,   
		  num_registrazione,   
		  cod_nota,   
		  des_nota, 
		  note,
		  prog_mimetype)  
VALUES ( :s_cs_xx.cod_azienda,   
		  :fl_anno_registrazione,   
		  :fl_num_registrazione,   
		  :ls_cod_nota,   
		  :ls_nota,   
		  :ls_note,
		  :ll_prog_mimytype )  ;
if sqlca.sqlcode < 0 then
	as_errore = "Errore in archiviazione documento (blob). "  + sqlca.sqlerrtext
	return -1
end if

ls_db = f_db()

if ls_db = "MSSQL" then
	
	if not guo_functions.uof_create_transaction_from( sqlca, sqlcb, ls_errore) then
		g_mb.error(ls_errore)
		return -1
	end if
			
	// devo farfe il commit del record inserito nella transazione precedente
	commit using sqlca;
		
	updateblob tes_fat_ven_note
	set        		note_esterne = :l_blob
	where      	cod_azienda = :s_cs_xx.cod_azienda and
				  	anno_registrazione = :il_anno_registrazione and
				  	num_registrazione = :il_num_registrazione and
				  	cod_nota = :ls_cod_nota
	using sqlcb;
	if sqlcb.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in memorizzazione (blob) fattura PDF" + sqlcb.sqlerrtext)
		rollback using sqlcb;
		rollback using sqlca;
		return -1
	end if 
	commit using sqlcb;
	
	destroy sqlcb;

else 
	updateblob tes_fat_ven_note
	set        note_esterne = :l_blob
	where      cod_azienda = :s_cs_xx.cod_azienda and
				  anno_registrazione = :il_anno_registrazione and
				  num_registrazione = :il_num_registrazione and
				  cod_nota = 'FAT';
	if sqlca.sqlcode < 0 then
		g_mb.messagebox("APICE","Errore in memorizzazione (blob) fattura PDF" + sqlca.sqlerrtext)
		rollback;
		return -1
	end if
end if 
	
return 0


end function

event pc_setwindow;call super::pc_setwindow;long   ll_i, ll_anno, ll_num,ll_ret
string ls_flag_email, ls_email_amministrazione, ls_cod_cliente, ls_flag_email_inviata, ls_email_utente, ls_cod_tipo_fat_ven
boolean lb_anteprima

guo_functions.uof_get_parametro_azienda("DPR", ref lb_anteprima)
guo_functions.uof_get_parametro_azienda("NNF", ref ib_nascondi_cod_documento)

set_w_options(c_noresizewin)
save_on_close(c_socnosave)
dw_report_fat_ven.ib_dw_report = true

il_anno_registrazione = s_cs_xx.parametri.parametro_d_1
il_num_registrazione  = s_cs_xx.parametri.parametro_d_2

if il_corrente = 0 then 
	ib_nr_nr = s_cs_xx.parametri.parametro_b_1
	ib_email = s_cs_xx.parametri.parametro_b_2
end if

//#############################################
if not ib_nr_nr  then

	select cod_tipo_fat_ven,
			cod_cliente,
			flag_email_inviata
	into  :ls_cod_tipo_fat_ven,
			:ls_cod_cliente,
			:ls_flag_email_inviata
	from   tes_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_registrazione and
			 num_registrazione = :il_num_registrazione;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Fattura richiesta non trovata~r~n" + sqlca.sqlerrtext)
		return
	end if
	
	if isnull(ls_cod_cliente) or ls_cod_cliente="" then
		ls_flag_email = "N"
		ls_email_amministrazione = "N"
		
	else
		select flag_accetta_mail,
				 email_amministrazione
		into   :ls_flag_email,
				 :ls_email_amministrazione
		from   anag_clienti
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_cliente = :ls_cod_cliente;
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Cliente " + ls_cod_cliente + " inesistente in anagrafica~r~n" + sqlca.sqlerrtext)
			return
		end if
		
	end if
	
end if
//##################################################

dw_report_fat_ven.set_dw_options(sqlca, &
											pcca.null_object, &
											c_nonew + &
											c_nomodify + &
											c_nodelete + &
											c_noenablenewonopen + &
											c_noenablemodifyonopen + &
											c_scrollparent + &
											c_disablecc, &
											c_noresizedw + &
											c_nohighlightselected + &
											c_nocursorrowfocusrect + &
											c_nocursorrowpointer)

if il_corrente = 0 then
	
	ib_nr_nr = s_cs_xx.parametri.parametro_b_1
	s_cs_xx.parametri.parametro_b_1 = false
	
	ib_email = s_cs_xx.parametri.parametro_b_2
	s_cs_xx.parametri.parametro_b_2 = false
	
	if ib_nr_nr then
		
		declare stampa cursor for
		select anno_registrazione,
				num_registrazione,
				flag_email,
				email_amministrazione,
				email_utente
		from stampa_fat_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				 cod_utente = :s_cs_xx.cod_utente
		order by anno_registrazione ASC,
					num_registrazione ASC;
					
		open stampa;
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE","Errore nella open del cursore stampa: " + sqlca.sqlerrtext)
			return
		end if
		
		ll_i = 1
		
		do while true
			
			fetch stampa
			into  :ll_anno,
					:ll_num,
					:ls_flag_email,
					:ls_email_amministrazione,
					:ls_email_utente;
					
			if sqlca.sqlcode < 0 then
				g_mb.messagebox("APICE","Errore nella fetch del cursore stampa: " + sqlca.sqlerrtext)
				close stampa;
				return
			elseif sqlca.sqlcode = 100 then
				close stampa;
				exit
			end if
			
			il_anno[ll_i] = ll_anno
			il_num[ll_i] = ll_num
			
			//################################################################
			is_flag_email[ll_i] = ls_flag_email
			is_email_amministrazione[ll_i] = ls_email_amministrazione
			is_email_utente[ll_i] = ls_email_utente
			//################################################################
			
			ll_i++
				
		loop
		
	else
		//################################################################
		ib_email = false
		is_flag_email[1] = "N"
		is_email_amministrazione[1] = ""
		//################################################################
		
		il_anno[1] = s_cs_xx.parametri.parametro_d_1
		il_num[1]  = s_cs_xx.parametri.parametro_d_2
		
	end if
	
end if

for il_corrente = 1 to upperbound(il_num[])
	
	il_anno_registrazione = il_anno[il_corrente]
	il_num_registrazione = il_num[il_corrente]
	
	wf_impostazioni()
	
	wf_report()
	
	if ib_nr_nr then
		triggerevent("pc_print")
		dw_report_fat_ven.reset()
		
	//###############################################################	
	else
		// l'attuale cliente prevede l'invio della fattura tramite mail
		if ls_flag_email = "S" and not isnull(ls_flag_email) then
			
			ll_ret = g_mb.messagebox("APICE","Il cliente selezionato prevede l'invio fattura tramite E-Mail: procedo con l'invio?",Question!, YesNo!,2) 
			
			if ll_ret = 1 then
			
				is_flag_email[1] = "S"
				is_email_amministrazione[1] = ls_email_amministrazione
				
				update tes_fat_ven
				set flag_email_inviata = 'S'
				where cod_azienda = :s_cs_xx.cod_azienda and
				      anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
						num_registrazione = :s_cs_xx.parametri.parametro_d_2; 
						
				cb_singola.event clicked()
				
				// il commit avviene dentro al pulsante cb_singola perchè la funzione provvede alla 
				// memorizzazione del documento nel database.
				
			else
				
				update tes_fat_ven
				set flag_email_inviata = 'N'
				where cod_azienda = :s_cs_xx.cod_azienda and
				      anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
						num_registrazione = :s_cs_xx.parametri.parametro_d_2; 
						
				ib_email = false
				
			end if
			
		end if
	//################################################################
	end if
	
next

il_corrente --

// stefanop: 10/07/2012: ridimensiono la finestra per occupare tutta l'altezza dell' MDI
guo_functions.uof_resize_max_mdi(w_report_fat_ven_euro, 1)

commit;

end event

on w_report_fat_ven_euro.create
int iCurrent
call super::create
this.cb_archivia=create cb_archivia
this.cb_2=create cb_2
this.cb_singola=create cb_singola
this.cb_1=create cb_1
this.dw_report_fat_ven=create dw_report_fat_ven
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_archivia
this.Control[iCurrent+2]=this.cb_2
this.Control[iCurrent+3]=this.cb_singola
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.dw_report_fat_ven
end on

on w_report_fat_ven_euro.destroy
call super::destroy
destroy(this.cb_archivia)
destroy(this.cb_2)
destroy(this.cb_singola)
destroy(this.cb_1)
destroy(this.dw_report_fat_ven)
end on

event pc_print;//IN QUESTO EVENTO IL FLAG EXTEND ANCESTOR DEVE ESSERE DISATTIVATO
cb_1.triggerevent("clicked")
end event

event close;call super::close;if ib_nr_nr then
	
	delete
	from   stampa_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_utente = :s_cs_xx.cod_utente;
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore nella delete di stampa_fat_ven: " + sqlca.sqlerrtext)
		rollback;
	else
		commit;
		g_mb.messagebox("APICE","Processo di stampa completato!")
	end if
	
end if
end event

event open;call super::open;if ib_nr_nr then
	postevent("ue_close")
end if
end event

type cb_archivia from commandbutton within w_report_fat_ven_euro
integer x = 1906
integer y = 20
integer width = 549
integer height = 88
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "ARCHIVIA"
end type

event clicked;string ls_path
uo_archivia_pdf luo_pdf
uo_upload_documenti luo_upload

luo_pdf = create uo_archivia_pdf

ls_path = luo_pdf.uof_crea_pdf_path(dw_report_fat_ven)

if ls_path = "errore" then
	g_mb.messagebox ("Errore","Errore nella creazione del file pdf.")
	return -1
end if


try 
	luo_upload = create uo_upload_documenti
	luo_upload.uof_archivio_fattura_vendita(il_anno_registrazione, il_num_registrazione, ls_path)
catch (uo_upload_exception e)
	g_mb.error(e.getMessage())
end try

g_mb.success("Documento archiviato con successo.")

return 0
end event

type cb_2 from commandbutton within w_report_fat_ven_euro
integer x = 1280
integer y = 20
integer width = 549
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "STAMPA 1 COPIA"
end type

event clicked;wf_stampa(1)
end event

type cb_singola from commandbutton within w_report_fat_ven_euro
integer x = 754
integer y = 20
integer width = 503
integer height = 88
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "E-MAIL"
end type

event clicked;string ls_cod_cliente, ls_flag_accetta_mail,ls_email_amministrazione, ls_email_utente

il_corrente = 1

select cod_cliente
into   :ls_cod_cliente
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_registrazione and
		 num_registrazione =  :il_num_registrazione;
		 
select flag_accetta_mail,
       email_amministrazione
into   :ls_flag_accetta_mail,
       :ls_email_amministrazione
from   anag_clienti
where  cod_azienda = :s_cs_xx.cod_azienda and
       cod_cliente = :ls_cod_cliente;
if sqlca.sqlcode <> 0 or len(ls_email_amministrazione) = 0 or isnull(ls_email_amministrazione) then
	g_mb.warning(g_str.format("Attenzione, il cliente $1 non è stato trovat oppure non è presente alcuna mail a cui inviare la fattura!",ls_cod_cliente))
	return
end if
		 

select e_mail
into	:ls_email_utente
from  utenti
where cod_utente = :s_cs_xx.cod_utente;
if sqlca.sqlcode <> 0 or len(ls_email_utente) = 0 or isnull(ls_email_utente) then
	g_mb.warning("Attenzione, verificare la mail dell'utente connesso!")
	return
end if

if ls_flag_accetta_mail = "S" then 
	ib_email = true
else
	return
end if

if isnull(ls_email_amministrazione) or len(ls_email_amministrazione) < 1 or pos(ls_email_amministrazione, "@")< 1 then
	g_mb.messagebox("APICE","Impossibile inviare la mail: verificare le impostazione in anagrafica cliente")
	return
end if

is_flag_email[1] = "S"
is_email_amministrazione[1] = ls_email_amministrazione
is_email_utente[1] = ls_email_utente
wf_stampa(1)

ib_email = false
end event

type cb_1 from commandbutton within w_report_fat_ven_euro
integer x = 46
integer y = 20
integer width = 503
integer height = 88
integer taborder = 10
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "STAMPA COPIE"
end type

event clicked;ib_stampa = true
wf_stampa(il_num_copie)
end event

type dw_report_fat_ven from uo_cs_xx_dw within w_report_fat_ven_euro
integer x = 23
integer y = 140
integer width = 3931
integer height = 2160
integer taborder = 20
string dataobject = "d_report_fat_ven_euro"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event pcd_first;call super::pcd_first;wf_report()
end event

event pcd_last;call super::pcd_last;wf_report()
end event

event pcd_next;call super::pcd_next;wf_report()
end event

event pcd_previous;call super::pcd_previous;wf_report()
end event

event printpage;call super::printpage;// stefanop: 17/02/2012: devo controlla se sto stampando per l'invo della mail o se è per la stampante
// Nel caso della mail non devo fare nessun controllo sul numero di pagina altrimenti il saveas della datawindow
// mi ritorna sempre una foglio bianco!
if ib_email then return 0
// -------------------------------------------------------------

if ib_stampa and pagenumber <> il_pagina_corrente then
	return 1
end if
end event

event printstart;call super::printstart;il_totale_pagine = pagesmax
end event

event pcd_saverowsas;//LASCIARE CHECK ANCESTOR SCRIPT DISATTIVATO
//la variabile ib_mail viene messa a true prima di creare il pdf e poi rimessa a false
//per evitare di generare un pdf con foglio bianco ....
//il problema lo si evince dallo script dell'evento printpage


//################################################
ib_email = true
//################################################

saveas()

//################################################
ib_email = false
//################################################
end event

event printend;call super::printend;ib_stampando = true
end event

