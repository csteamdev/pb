﻿$PBExportHeader$w_report_scadenze.srw
forward
global type w_report_scadenze from w_std_principale
end type
type cb_stampa from commandbutton within w_report_scadenze
end type
type dw_scadenze from uo_std_dw within w_report_scadenze
end type
end forward

global type w_report_scadenze from w_std_principale
integer width = 3150
integer height = 2224
string title = "Report Scadenze Clienti"
cb_stampa cb_stampa
dw_scadenze dw_scadenze
end type
global w_report_scadenze w_report_scadenze

event open;call super::open;dw_scadenze.ib_dw_report=true
dw_scadenze.settransobject(sqlca)
dw_scadenze.retrieve()


end event

on w_report_scadenze.create
int iCurrent
call super::create
this.cb_stampa=create cb_stampa
this.dw_scadenze=create dw_scadenze
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_stampa
this.Control[iCurrent+2]=this.dw_scadenze
end on

on w_report_scadenze.destroy
call super::destroy
destroy(this.cb_stampa)
destroy(this.dw_scadenze)
end on

type cb_stampa from commandbutton within w_report_scadenze
integer x = 32
integer y = 1984
integer width = 402
integer height = 112
integer taborder = 20
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Stampa"
end type

event clicked;if PrintSetup ( ) > 0 then
	dw_scadenze.print()
end if
end event

type dw_scadenze from uo_std_dw within w_report_scadenze
integer x = 27
integer y = 16
integer width = 3058
integer height = 1948
integer taborder = 10
string dataobject = "d_report_scadenze"
end type

