﻿$PBExportHeader$w_riconferma_fattura.srw
forward
global type w_riconferma_fattura from w_std_principale
end type
type cbx_1 from checkbox within w_riconferma_fattura
end type
type st_2 from statictext within w_riconferma_fattura
end type
type cb_2 from commandbutton within w_riconferma_fattura
end type
type cb_1 from commandbutton within w_riconferma_fattura
end type
type em_num_dicumento from editmask within w_riconferma_fattura
end type
type em_anno from editmask within w_riconferma_fattura
end type
type em_numeratore from editmask within w_riconferma_fattura
end type
type em_documento from editmask within w_riconferma_fattura
end type
type st_1 from statictext within w_riconferma_fattura
end type
type dw_1 from datawindow within w_riconferma_fattura
end type
end forward

global type w_riconferma_fattura from w_std_principale
integer height = 1772
string title = "De-Conferma Fatture"
cbx_1 cbx_1
st_2 st_2
cb_2 cb_2
cb_1 cb_1
em_num_dicumento em_num_dicumento
em_anno em_anno
em_numeratore em_numeratore
em_documento em_documento
st_1 st_1
dw_1 dw_1
end type
global w_riconferma_fattura w_riconferma_fattura

on w_riconferma_fattura.create
int iCurrent
call super::create
this.cbx_1=create cbx_1
this.st_2=create st_2
this.cb_2=create cb_2
this.cb_1=create cb_1
this.em_num_dicumento=create em_num_dicumento
this.em_anno=create em_anno
this.em_numeratore=create em_numeratore
this.em_documento=create em_documento
this.st_1=create st_1
this.dw_1=create dw_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_1
this.Control[iCurrent+2]=this.st_2
this.Control[iCurrent+3]=this.cb_2
this.Control[iCurrent+4]=this.cb_1
this.Control[iCurrent+5]=this.em_num_dicumento
this.Control[iCurrent+6]=this.em_anno
this.Control[iCurrent+7]=this.em_numeratore
this.Control[iCurrent+8]=this.em_documento
this.Control[iCurrent+9]=this.st_1
this.Control[iCurrent+10]=this.dw_1
end on

on w_riconferma_fattura.destroy
call super::destroy
destroy(this.cbx_1)
destroy(this.st_2)
destroy(this.cb_2)
destroy(this.cb_1)
destroy(this.em_num_dicumento)
destroy(this.em_anno)
destroy(this.em_numeratore)
destroy(this.em_documento)
destroy(this.st_1)
destroy(this.dw_1)
end on

type cbx_1 from checkbox within w_riconferma_fattura
integer x = 2149
integer y = 20
integer width = 1902
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
string text = "ESEGUI la RI-CONFERMA la fattura al termine."
end type

type st_2 from statictext within w_riconferma_fattura
integer x = 23
integer y = 1540
integer width = 3040
integer height = 100
integer textsize = -10
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 67108864
boolean focusrectangle = false
end type

type cb_2 from commandbutton within w_riconferma_fattura
integer x = 3086
integer y = 1540
integer width = 421
integer height = 100
integer taborder = 60
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
boolean enabled = false
string text = "De-Conferma"
end type

event clicked;string ls_cod_tipo_movimento,ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], ls_cod_fornitore[], &
		ls_cod_tipo_det_ven, ls_flag_tipo_det_ven, ls_tes_tabella, ls_det_tabella, ls_prog_riga, ls_quantita, ls_messaggio, ls_str
long ll_i,ll_anno_registrazione,ll_num_registrazione,ll_prog_riga_fat_ven,ll_anno_reg_mov,ll_num_reg_mov,ll_ret, ll_progr_stock[], &
		ll_anno_reg_des_mov, ll_num_reg_des_mov,ll_anno_reg_mov_origine, ll_num_reg_mov_origine, ll_anno_registrazione_bol_ven, &
		ll_num_registrazione_bol_ven, ll_prog_riga_bol_ven
datetime ldt_data_stock[]
uo_magazzino luo_mag

luo_mag = create uo_magazzino

for ll_i = 1 to dw_1.rowcount()
	ll_anno_registrazione = dw_1.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = dw_1.getitemnumber(ll_i, "num_registrazione")
	ll_prog_riga_fat_ven = dw_1.getitemnumber(ll_i, "prog_riga_fat_ven")
	ll_anno_reg_mov = dw_1.getitemnumber(ll_i, "anno_registrazione_mov_mag")
	ll_num_reg_mov = dw_1.getitemnumber(ll_i, "num_registrazione_mov_mag")
	ls_cod_tipo_det_ven =  dw_1.getitemstring(ll_i, "cod_tipo_det_ven")
	
	ll_anno_registrazione_bol_ven =  dw_1.getitemnumber(ll_i, "anno_registrazione_bol_ven")
	ll_num_registrazione_bol_ven  =  dw_1.getitemnumber(ll_i, "num_registrazione_bol_ven")
	ll_prog_riga_bol_ven =  dw_1.getitemnumber(ll_i, "prog_riga_bol_ven")
	
	if not isnull(ll_anno_registrazione_bol_ven) then
		g_mb.error("Attenzione! La fattura è differita e bisogna de-conferma la bolla " + string(ll_anno_registrazione_bol_ven) + "/" + string(ll_num_registrazione_bol_ven) )
		rollback;
		destroy luo_mag
		return
	end if
	
	st_2.text = " Riga " + string(ll_i) + "/" + string(dw_1.rowcount()) + " in elaborazione ...."
	Yield()
	
	select flag_tipo_det_ven
	into :ls_flag_tipo_det_ven
	from tab_tipi_det_ven
	where cod_azienda = :s_cs_xx.cod_azienda and
			cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	if sqlca.sqlcode < 0 then
		g_mb.error("ERRORE IN UPDATE  DET FAT VEN~r~n" + sqlca.sqlerrtext)
		rollback;
		destroy luo_mag
		return
	end if
	
	
	update det_fat_ven
	set 	anno_reg_des_mov = null,
			num_reg_des_mov = null,
			anno_registrazione_mov_mag = null,
			num_registrazione_mov_mag = null
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione and
			num_registrazione = :ll_num_registrazione and
			prog_riga_fat_ven = :ll_prog_riga_fat_ven;
	if sqlca.sqlcode < 0 then
		g_mb.error("ERRORE IN UPDATE  DET FAT VEN~r~n" + sqlca.sqlerrtext)
		rollback;
		destroy luo_mag
		return
	end if
	
	if ls_flag_tipo_det_ven = "M" then
		
		if NOT isnull(ll_anno_reg_mov) then
			
			select anno_registrazione, num_registrazione
			into 	:ll_anno_reg_mov_origine, :ll_num_reg_mov_origine
			from 	mov_magazzino
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_reg_mov_origine = :ll_anno_reg_mov and
					num_reg_mov_origine = :ll_num_reg_mov;
					
			if sqlca.sqlcode < 0 then
				g_mb.error("ERRORE IN ricerca movimento origine del prodotto raggruppato~r~n" + sqlca.sqlerrtext)
				rollback;
				destroy luo_mag
				return
			end if
			
			if sqlca.sqlcode = 0 and not isnull(ll_anno_reg_mov_origine) then
				// elimino il movimento collegato del raggruppato se c'è
				ll_ret = luo_mag.uof_elimina_movimenti( ll_anno_reg_mov_origine, ll_num_reg_mov_origine, true)
				
				if ll_ret < 0 then
					g_mb.error("ERRORE IN cancellazione movimenti magazzino~r~n" + sqlca.sqlerrtext)
					rollback;
					destroy luo_mag
					return
				end if
			end if
	
			ll_ret = luo_mag.uof_elimina_movimenti( ll_anno_reg_mov, ll_num_reg_mov, true)
			
			if ll_ret < 0 then
				g_mb.error("ERRORE IN cancellazione movimenti magazzino~r~n" + sqlca.sqlerrtext)
				rollback;
				destroy luo_mag
				return
			end if
			
		end if
		
		ls_cod_tipo_movimento = dw_1.getitemstring(ll_i, "cod_tipo_movimento")
		ls_cod_prodotto = dw_1.getitemstring(ll_i, "cod_prodotto")
		ls_cod_deposito[1] = dw_1.getitemstring(ll_i, "cod_deposito")
		ls_cod_ubicazione[1] = dw_1.getitemstring(ll_i, "cod_ubicazione")
		ls_cod_lotto[1] = dw_1.getitemstring(ll_i, "cod_lotto")
		ldt_data_stock[1] = dw_1.getitemdatetime(ll_i, "data_stock")
		ll_progr_stock[1] = dw_1.getitemnumber(ll_i, "progr_stock")
		ls_cod_lotto[1] = dw_1.getitemstring(ll_i, "cod_lotto")
		
		setnull(ls_cod_cliente[1])
		setnull(ls_cod_fornitore[1])
		
		if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di creazione destinazioni movimenti.",  exclamation!, ok!)
			rollback;
			destroy luo_mag
			return 1
		end if
	
		if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
			rollback;
			destroy luo_mag
			return 1
		end if
	
		update det_fat_ven
		set 	anno_reg_des_mov = :ll_anno_reg_des_mov,
				num_reg_des_mov = :ll_num_reg_des_mov
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_registrazione and
				num_registrazione = :ll_num_registrazione and
				prog_riga_fat_ven = :ll_prog_riga_fat_ven;
		if sqlca.sqlcode < 0 then
			g_mb.error("ERRORE IN UPDATE  DET FAT VEN~r~n" + sqlca.sqlerrtext)
			destroy luo_mag
			rollback;
			return
		end if
		
	end if
	
	st_2.text = " Riga " + string(ll_i) + "/" + string(dw_1.rowcount()) + " elaborata ...OK !!!! ."
	Yield()

next

update tes_fat_ven
set flag_movimenti = 'N'
where cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione= :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione;
if sqlca.sqlcode < 0 then
	ls_str = "Errore in aggiornamento flag_movimento della fattura.~r~nDettaglio errore: " + sqlca.sqlerrtext +  "~r~n"
	g_mb.error(ls_str)
	rollback;
end if

if cbx_1.checked then
	if dw_1.rowcount() > 1 then
		
		ll_anno_registrazione = dw_1.getitemnumber(1, "anno_registrazione")
		ll_num_registrazione = dw_1.getitemnumber(1, "num_registrazione")
		
		ls_tes_tabella = "tes_fat_ven"
		ls_det_tabella = "det_fat_ven"
		ls_prog_riga = "prog_riga_fat_ven"
		ls_quantita = "quan_fatturata"
		
		ll_ret = f_conferma_mov_mag(ls_tes_tabella, &
									 ls_det_tabella, &
									 ls_prog_riga, &
									 ls_quantita, &
									 ll_anno_registrazione, &
									 ll_num_registrazione, &
									 ls_messaggio) 
		if ll_ret = -1 then
			ls_str = "La conferma della fattura " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " ha provocato un errore.~r~nDettaglio errore: " + ls_messaggio +  "~r~n"
			g_mb.error(ls_str)
			rollback;
		end if
	end if
end if


commit;

destroy luo_mag

g_mb.warning("Fattura riconfermata con successo")

dw_1.reset()

cb_2.enabled=false

end event

type cb_1 from commandbutton within w_riconferma_fattura
integer x = 1623
integer y = 20
integer width = 480
integer height = 100
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
string text = "Cerca Fattura"
end type

event clicked;string ls_cod_documento, ls_numeratore_documento
long ll_anno_documento, ll_num_documento,ll_anno_registrazione,ll_num_registrazione

ls_cod_documento = em_documento.text
ls_numeratore_documento = em_numeratore.text
ll_anno_documento = long(em_anno.text)
ll_num_documento = long(em_num_dicumento.text)

select 	anno_registrazione, num_registrazione
into 		:ll_anno_registrazione, :ll_num_registrazione
from 		tes_fat_ven
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_documento = :ls_cod_documento and
			numeratore_documento = :ls_numeratore_documento and
			anno_documento = :ll_anno_documento and
			num_documento = :ll_num_documento;
			
if sqlca.sqlcode = 0 then
	dw_1.reset()
	dw_1.settransobject(sqlca)
	
	dw_1.retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)
	
	cb_2.enabled=true
end if




end event

type em_num_dicumento from editmask within w_riconferma_fattura
integer x = 1326
integer y = 20
integer width = 274
integer height = 100
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
string mask = "######"
end type

type em_anno from editmask within w_riconferma_fattura
integer x = 869
integer y = 20
integer width = 229
integer height = 100
integer taborder = 20
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
string mask = "####"
end type

type em_numeratore from editmask within w_riconferma_fattura
integer x = 1189
integer y = 20
integer width = 114
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string mask = "a"
end type

type em_documento from editmask within w_riconferma_fattura
integer x = 571
integer y = 20
integer width = 274
integer height = 100
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = stringmask!
string mask = "aaa"
end type

type st_1 from statictext within w_riconferma_fattura
integer x = 23
integer y = 20
integer width = 549
integer height = 100
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 553648127
string text = "NUMERO FISCALE:"
boolean focusrectangle = false
end type

type dw_1 from datawindow within w_riconferma_fattura
integer x = 23
integer y = 160
integer width = 3474
integer height = 1360
integer taborder = 70
string title = "none"
string dataobject = "d_riconferma_fattura"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

