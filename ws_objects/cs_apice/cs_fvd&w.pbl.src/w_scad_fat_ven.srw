﻿$PBExportHeader$w_scad_fat_ven.srw
$PBExportComments$Finestra Gestione Scadenze Fatture Vendita
forward
global type w_scad_fat_ven from w_cs_xx_principale
end type
type dw_scadenze_impresa from uo_std_dw within w_scad_fat_ven
end type
type cb_salva_pagamenti from commandbutton within w_scad_fat_ven
end type
type st_scadenze_impresa from statictext within w_scad_fat_ven
end type
type dw_scad_fat_ven from uo_cs_xx_dw within w_scad_fat_ven
end type
type ln_1 from line within w_scad_fat_ven
end type
end forward

global type w_scad_fat_ven from w_cs_xx_principale
integer width = 4507
integer height = 2128
string title = "Scadenze"
boolean resizable = false
boolean center = true
dw_scadenze_impresa dw_scadenze_impresa
cb_salva_pagamenti cb_salva_pagamenti
st_scadenze_impresa st_scadenze_impresa
dw_scad_fat_ven dw_scad_fat_ven
ln_1 ln_1
end type
global w_scad_fat_ven w_scad_fat_ven

type variables
string is_MSC = "N"
end variables

forward prototypes
public subroutine wf_scadenze_impresa (long al_anno_registrazione, long al_num_registrazione)
public function string get_tipo_documento (string ls_cod_tipo_fat_ven)
public function boolean wf_salva_scadenze ()
end prototypes

public subroutine wf_scadenze_impresa (long al_anno_registrazione, long al_num_registrazione);/**
 * Recupero le scadenze da impresa per dare la possibilità di forzare il pagamento
 **/
 
string ls_num_documento, ls_cod_cliente, ls_des_cliente, ls_tipo_documento, ls_cod_tipo_fat_ven,ls_cod_cliente_impresa, ls_cod_capoconto
decimal ld_num_documento, ld_imponibile_iva, ld_imponibile_provvigioni_1, ld_importo_provvigioni, ld_tot_fattura, ld_percentuale_incidenza, &
			ld_scadenza_dare, ld_scadenza_avere
long ll_rowcount, ll_i, ll_row, ll_anno_documento,ll_num_documento, ll_anno_reg_det_storico_prov, ll_id_scadenza, ll_num_reg_det_storico_prov, &
		ll_prog_reg_det_storico_prov
date ldt_data_fattura, ldt_data_forzato_pagato, ldt_data_scadenza
datetime ldt_data_registrazione
datastore lds_scadenze_impresa
uo_impresa luo_impresa

// recupero campi
select num_documento, data_fattura, cod_cliente
into :ll_num_documento, :ldt_data_fattura, :ls_cod_cliente
from tes_fat_ven
where
	cod_azienda = :s_cs_xx.cod_azienda and
	anno_registrazione = :al_anno_registrazione and 
	num_registrazione = :al_num_registrazione;
	
select cod_capoconto
into    :ls_cod_capoconto
from  anag_clienti
where cod_azienda = :s_cs_xx.cod_azienda and
		cod_cliente = :ls_cod_cliente;
	
// prelevo i dati delle scadenze maturate da impresa		 
luo_impresa = create uo_impresa
ls_cod_cliente_impresa = luo_impresa.uof_codice_cli_for("C", ls_cod_capoconto, ls_cod_cliente)
destroy luo_impresa


dw_scadenze_impresa.reset()

// datastore impresa
lds_scadenze_impresa = create datastore
lds_scadenze_impresa.dataobject = 'd_ds_scadenze_impresa'
lds_scadenze_impresa.settransobject(sqlci)

ll_rowcount = lds_scadenze_impresa.retrieve(ls_cod_cliente_impresa, string(ll_num_documento), date(ldt_data_fattura))

if ll_rowcount < 0 then
	g_mb.error("Errore durante la retrieve del datastore lds_scadenze_impresa.")
	return	
end if

for ll_i = 1 to ll_rowcount
	
	ll_row = dw_scadenze_impresa.insertrow(0)
	ll_id_scadenza = lds_scadenze_impresa.getitemnumber(ll_i, 3)
	ldt_data_scadenza =  date(lds_scadenze_impresa.getitemdatetime(ll_i, 4))
	ld_scadenza_dare = lds_scadenze_impresa.getitemdecimal(ll_i, 5)
	ld_scadenza_avere = lds_scadenze_impresa.getitemdecimal(ll_i, 6)
	
	// PK
	dw_scadenze_impresa.setitem(ll_row, "anno_registrazione", al_anno_registrazione)
	dw_scadenze_impresa.setitem(ll_row, "num_registrazione", al_num_registrazione)
	dw_scadenze_impresa.setitem(ll_row, "data_scadenza", ldt_data_scadenza)
	dw_scadenze_impresa.setitem(ll_row, "id_scadenza", ll_id_scadenza)
	dw_scadenze_impresa.setitem(ll_row, "scadenza_importo", ld_scadenza_dare - ld_scadenza_avere)
	
	// ----
	
	// carico dati dalla tabella della forzatura se presente
	select data_forzato_pagato, anno_reg_det_storico_prov, num_reg_det_storico_prov, prog_reg_det_storico_prov
	into :ldt_data_forzato_pagato, :ll_anno_reg_det_storico_prov, :ll_num_reg_det_storico_prov, :ll_prog_reg_det_storico_prov
	from scad_fat_ven_impresa
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :al_anno_registrazione and 
		num_registrazione = :al_num_registrazione and
		data_scadenza = :ldt_data_scadenza and
		id_scadenza = :ll_id_scadenza;
		
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il controllo della data di forzato pagamento", sqlca)
		return
	elseif sqlca.sqlcode = 0 then
		dw_scadenze_impresa.setitem(ll_row, "flag_forzatura", "S")
		dw_scadenze_impresa.setitem(ll_row, "data_forzato_pagato", ldt_data_forzato_pagato)
		dw_scadenze_impresa.setitem(ll_row, "anno_reg_det_storico_prov", ll_anno_reg_det_storico_prov)
		dw_scadenze_impresa.setitem(ll_row, "num_reg_det_storico_prov", ll_num_reg_det_storico_prov)
		dw_scadenze_impresa.setitem(ll_row, "prog_reg_det_storico_prov", ll_prog_reg_det_storico_prov)
	end if
	// ----
next

destroy lds_scadenze_impresa
end subroutine

public function string get_tipo_documento (string ls_cod_tipo_fat_ven);string ls_flag_tipo_fat_ven

select flag_tipo_fat_ven
into :ls_flag_tipo_fat_ven
from tab_tipi_fat_ven
where 
	cod_azienda = :s_cs_xx.cod_azienda and
	cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca tipo documento in tabella tab_tipi_fat_ven; proseguo ugualmente.~r~n" + sqlca.sqlerrtext)
	ls_flag_tipo_fat_ven = "I"
end if

return ls_flag_tipo_fat_ven
end function

public function boolean wf_salva_scadenze ();string ls_flag_forzata
long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_id_scadenza, ll_anno_reg_det_storico_prov, ll_num_reg_det_storico_prov, &
	ll_count, ll_prog_reg_det_storico_prov
date ld_data_scadenza, ld_data_forzato_pagato

dw_scadenze_impresa.accepttext()

if dw_scadenze_impresa.rowcount() < 1 then
	return true
end if

for ll_i = 1 to dw_scadenze_impresa.rowcount()
	
	ll_anno_registrazione = dw_scadenze_impresa.getitemnumber(ll_i, "anno_registrazione")
	ll_num_registrazione = dw_scadenze_impresa.getitemnumber(ll_i, "num_registrazione")
	ld_data_scadenza = dw_scadenze_impresa.getitemdate(ll_i, "data_scadenza")
	ll_id_scadenza = dw_scadenze_impresa.getitemnumber(ll_i, "id_scadenza")
	ll_anno_reg_det_storico_prov = dw_scadenze_impresa.getitemnumber(ll_i, "anno_reg_det_storico_prov")
	ll_num_reg_det_storico_prov = dw_scadenze_impresa.getitemnumber(ll_i, "num_reg_det_storico_prov")
	ll_prog_reg_det_storico_prov = dw_scadenze_impresa.getitemnumber(ll_i, "prog_reg_det_storico_prov")
	ld_data_forzato_pagato = dw_scadenze_impresa.getitemdate(ll_i, "data_forzato_pagato")
	ls_flag_forzata = dw_scadenze_impresa.getitemstring(ll_i, "flag_forzatura")
	
	// controllo se esiste
	select count(*)
	into :ll_count
	from scad_fat_ven_impresa
	where
		cod_azienda = :s_cs_xx.cod_azienda and
		anno_registrazione = :ll_anno_registrazione and
		num_registrazione = :ll_num_registrazione and
		data_scadenza = :ld_data_scadenza and
		id_scadenza  = :ll_id_scadenza;
		
	if sqlca.sqlcode < 0 then
		g_mb.error("Errore durante il controllo dei pagamenti forzati.", sqlca)
		return false
	elseif (sqlca.sqlcode = 100 or isnull(ll_count) or ll_count = 0) and ls_flag_forzata = "S" then
		// devo inserirla
		insert into 	scad_fat_ven_impresa(
			cod_azienda,
			anno_registrazione,
			num_registrazione,
			data_scadenza,
			id_scadenza,
			anno_reg_det_storico_prov,
			num_reg_det_storico_prov,
			prog_reg_det_storico_prov,
			data_forzato_pagato
			) values (
			:s_cs_xx.cod_azienda,
			:ll_anno_registrazione,
			:ll_num_registrazione,
			:ld_data_scadenza,
			:ll_id_scadenza,
			null,
			null,
			null,
			:ld_data_forzato_pagato);
		
		if sqlca.sqlcode <> 0 then
			g_mb.error("Errore durante l'inserimento della data di forzato pagamento.", sqlca)
			return false
		end if
			
		
	elseif sqlca.sqlcode = 0 and ll_count > 0 then
		// Esiste, devo controllare se cancellarla o aggiornarla con la nuova data
		if ls_flag_forzata = "S" then
			
			update scad_fat_ven_impresa
			set data_forzato_pagato = :ld_data_forzato_pagato
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_registrazione and
				num_registrazione = :ll_num_registrazione and
				data_scadenza = :ld_data_scadenza and
				id_scadenza  = :ll_id_scadenza;
				
			if sqlca.sqlcode <> 0 then
				g_mb.error("Errore durante l'aggiornamento della data di forzato pagamento", sqlca)
				return false
			end if
			
		else
			
			delete from scad_fat_ven_impresa
			where
				cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_registrazione and
				num_registrazione = :ll_num_registrazione and
				data_scadenza = :ld_data_scadenza and
				id_scadenza  = :ll_id_scadenza;
				
			if sqlca.sqlcode <> 0 then
				g_mb.error("Errore durante la cancellazione della data di forzato pagamento", sqlca)
				return false
			end if
		end if
	end if
next

return true
end function

event pc_setwindow;call super::pc_setwindow;dw_scad_fat_ven.set_dw_key("cod_azienda")
dw_scad_fat_ven.set_dw_key("anno_registrazione")
dw_scad_fat_ven.set_dw_key("num_registrazione")
dw_scad_fat_ven.set_dw_key("prog_scadenza")

dw_scad_fat_ven.set_dw_options(sqlca, &
                               i_openparm, &
                               c_scrollparent, &
                               c_default)

/*
select flag
into :is_MSC
from parametri_azienda
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_parametro = 'MSC' and
			flag_parametro = 'F';
			
if sqlca.sqlcode <> 0 or isnull(is_MSC) or is_MSC="" then
	is_MSC = "N"
end if

if is_MSC = "S" then
	dw_scad_fat_ven.object.flag_pagata.visible = true
else
	dw_scad_fat_ven.object.flag_pagata.visible = false
end if
*/

//stefanop 30/05/2011
dw_scadenze_impresa.ib_dw_report=true

if s_cs_xx.parametri.impresa then
	// la retrieve su impresa avviene dopo la quella della prima DW, quindi controllare pcd_retrieve della dw_scad_fat_ven
else
	this.height = 1100
end if
end event

on w_scad_fat_ven.create
int iCurrent
call super::create
this.dw_scadenze_impresa=create dw_scadenze_impresa
this.cb_salva_pagamenti=create cb_salva_pagamenti
this.st_scadenze_impresa=create st_scadenze_impresa
this.dw_scad_fat_ven=create dw_scad_fat_ven
this.ln_1=create ln_1
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_scadenze_impresa
this.Control[iCurrent+2]=this.cb_salva_pagamenti
this.Control[iCurrent+3]=this.st_scadenze_impresa
this.Control[iCurrent+4]=this.dw_scad_fat_ven
this.Control[iCurrent+5]=this.ln_1
end on

on w_scad_fat_ven.destroy
call super::destroy
destroy(this.dw_scadenze_impresa)
destroy(this.cb_salva_pagamenti)
destroy(this.st_scadenze_impresa)
destroy(this.dw_scad_fat_ven)
destroy(this.ln_1)
end on

event close;call super::close;if isvalid(w_tes_fat_ven) then
	w_tes_fat_ven.event post pc_menu_calcola()
end if
end event

event pc_setddlb;call super::pc_setddlb;if isvalid(sqlci) then
	f_po_loaddddw_dw(dw_scad_fat_ven,"cod_tipo_pagamento",sqlci,"tipo_pagamento","codice","descrizione","")
else
	f_po_loaddddw_dw(dw_scad_fat_ven,"cod_tipo_pagamento",sqlca,"tab_tipi_pagamenti", &
						 "cod_tipo_pagamento","des_tipo_pagamento","cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end if
f_po_loaddddw_dw(dw_scad_fat_ven,"fatel_modalita_pagamento",sqlca,"tab_pagamenti_modalita", &
						 "cod_modalita_pagamento","des_modalita_pagamento","cod_azienda = '" + s_cs_xx.cod_azienda + "'")

end event

event resize;/* ELIMINATO ANCHESTOR */
end event

type dw_scadenze_impresa from uo_std_dw within w_scad_fat_ven
integer x = 23
integer y = 1180
integer width = 2149
integer height = 840
integer taborder = 30
string dataobject = "d_scadenze_impresa"
end type

event itemchanged;call super::itemchanged;date ldt_null

choose case dwo.name
	case "flag_forzatura"
		
		if data  = "N" then
			setnull(ldt_null)
			setitem(row, "data_forzato_pagato", ldt_null)
		end if
		
end choose
end event

type cb_salva_pagamenti from commandbutton within w_scad_fat_ven
integer x = 2194
integer y = 1180
integer width = 745
integer height = 100
integer taborder = 30
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Salva Pagamenti forzati"
end type

event clicked;if wf_salva_scadenze() then
	commit;
	g_mb.show("Salvataggio completato con successo")
else
	rollback;
end if
end event

type st_scadenze_impresa from statictext within w_scad_fat_ven
integer x = 114
integer y = 1080
integer width = 517
integer height = 64
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = " Scadenze Impresa"
boolean focusrectangle = false
end type

type dw_scad_fat_ven from uo_cs_xx_dw within w_scad_fat_ven
integer x = 23
integer y = 20
integer width = 4457
integer height = 1000
integer taborder = 20
string dataobject = "d_scad_fat_ven"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_scadenza
datetime ldt_data_scadenza

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")

for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if isnull(this.getitemnumber(ll_i, "anno_registrazione")) or &
      this.getitemnumber(ll_i, "anno_registrazione") = 0 then
      this.setitem(ll_i, "anno_registrazione", ll_anno_registrazione)
   end if
   if isnull(this.getitemnumber(ll_i, "num_registrazione")) or &
      this.getitemnumber(ll_i, "num_registrazione") = 0 then
      this.setitem(ll_i, "num_registrazione", ll_num_registrazione)
   end if

	if isnull(this.getitemnumber(ll_i, "prog_scadenza")) or &
		this.getitemnumber(ll_i, "prog_scadenza") = 0 then

		ldt_data_scadenza = this.getitemdatetime(ll_i, "data_scadenza")

		select max(scad_fat_ven.prog_scadenza)
		into   :ll_prog_scadenza
		from   scad_fat_ven
		where  scad_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and
				 scad_fat_ven.anno_registrazione = :ll_anno_registrazione and
				 scad_fat_ven.num_registrazione = :ll_num_registrazione and
				 scad_fat_ven.data_scadenza = :ldt_data_scadenza;

		if isnull(ll_prog_scadenza) then
			this.setitem(ll_i, "prog_scadenza", 1)
		else
			this.setitem(ll_i, "prog_scadenza", ll_prog_scadenza + 1)
		end if
	end if
next

end event

event pcd_retrieve;call super::pcd_retrieve;long   l_errore, ll_anno_registrazione, ll_num_registrazione

ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
	
l_errore = retrieve(s_cs_xx.cod_azienda, ll_anno_registrazione, ll_num_registrazione)

if l_Errore < 0 then
   pcca.error = c_Fatal
end if

if s_cs_xx.parametri.impresa then
	
		wf_scadenze_impresa(ll_anno_registrazione, ll_num_registrazione)
	
end if
end event

event itemchanged;call super::itemchanged;string ls_flag_contabilita
long ll_anno_registrazione, ll_num_registrazione

choose case dwo.name
	case "flag_pagata"
		if data = "S" then
			//27/12/2010 per Sicurservice è stata introdotta la gestione flag_pagata in scad_fat_ven
			//Se il parametro aziendale di tipo flag MSC esiste e vale S
			//Allora la funzione f_conferma_mov_mag metterà anche il flag_contablita a S (oltre a flag_movimenti).
			//Successivamente, in questo modo, marcando le scadenze come pagate, 
			//un eventuale ricalcolo non farà perdere tali flags,
			//in quanto il calcolo viene saltato se flag_contabilita vale S.
			
			//se stai mettendo S verifica che la fattura sia contabilizzata, altrimenti allerta che un ricalcolloo della fattura farà perdere il flag pagata
			if is_MSC="S" then
				
				ll_anno_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "anno_registrazione")
				ll_num_registrazione = i_parentdw.getitemnumber(i_parentdw.i_selectedrows[1], "num_registrazione")
				
				select flag_contabilita
				into :ls_flag_contabilita
				from tes_fat_ven
				where 	cod_azienda=:s_cs_xx.cod_azienda and
							anno_registrazione=:ll_anno_registrazione and
							num_registrazione=:ll_num_registrazione;
							
				if ls_flag_contabilita="N" then
					//alert
					g_mb.show("Attenzione! La fattura non è stata contabilizzata! "+ &
								"Il ricalcolo della fattura causerà la perdita della memorizzazione del flag pagata per tutte le sue scadenze!")
				end if
			end if

		end if
end choose
end event

type ln_1 from line within w_scad_fat_ven
long linecolor = 33554432
integer linethickness = 4
integer beginx = 46
integer beginy = 1120
integer endx = 4457
integer endy = 1120
end type

