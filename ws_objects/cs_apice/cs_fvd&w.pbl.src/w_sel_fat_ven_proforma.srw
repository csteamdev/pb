﻿$PBExportHeader$w_sel_fat_ven_proforma.srw
forward
global type w_sel_fat_ven_proforma from w_cs_xx_risposta
end type
type st_righe from statictext within w_sel_fat_ven_proforma
end type
type st_cliente from statictext within w_sel_fat_ven_proforma
end type
type cb_annulla from commandbutton within w_sel_fat_ven_proforma
end type
type cb_seleziona from commandbutton within w_sel_fat_ven_proforma
end type
type dw_lista from datawindow within w_sel_fat_ven_proforma
end type
end forward

global type w_sel_fat_ven_proforma from w_cs_xx_risposta
integer width = 2871
integer height = 1684
string title = "Seleziona fattura vendita"
boolean controlmenu = false
st_righe st_righe
st_cliente st_cliente
cb_annulla cb_annulla
cb_seleziona cb_seleziona
dw_lista dw_lista
end type
global w_sel_fat_ven_proforma w_sel_fat_ven_proforma

type variables
string is_cod_pagamento_ordine = ""
end variables

on w_sel_fat_ven_proforma.create
int iCurrent
call super::create
this.st_righe=create st_righe
this.st_cliente=create st_cliente
this.cb_annulla=create cb_annulla
this.cb_seleziona=create cb_seleziona
this.dw_lista=create dw_lista
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_righe
this.Control[iCurrent+2]=this.st_cliente
this.Control[iCurrent+3]=this.cb_annulla
this.Control[iCurrent+4]=this.cb_seleziona
this.Control[iCurrent+5]=this.dw_lista
end on

on w_sel_fat_ven_proforma.destroy
call super::destroy
destroy(this.st_righe)
destroy(this.st_cliente)
destroy(this.cb_annulla)
destroy(this.cb_seleziona)
destroy(this.dw_lista)
end on

event open;call super::open;string	ls_cod_cliente_ordine, ls_rag_soc_cliente_ordine
long		ll_row

ls_cod_cliente_ordine = s_cs_xx.parametri.parametro_s_1
ls_rag_soc_cliente_ordine = s_cs_xx.parametri.parametro_s_2
is_cod_pagamento_ordine = s_cs_xx.parametri.parametro_s_3

if isnull(is_cod_pagamento_ordine) then is_cod_pagamento_ordine = ""
		
st_cliente.text = ls_cod_cliente_ordine + " - " + ls_rag_soc_cliente_ordine

dw_lista.settransobject(sqlca)
ll_row = dw_lista.retrieve(s_cs_xx.cod_azienda, ls_cod_cliente_ordine)

st_righe.text = "Totale fatture pro-forma presenti: " + string(ll_row)

if ll_row > 0 then
	dw_lista.setrow(1)
	dw_lista.selectrow(1, true)
end if
end event

type st_righe from statictext within w_sel_fat_ven_proforma
integer x = 1029
integer y = 1500
integer width = 1760
integer height = 60
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
string text = "Righe:"
alignment alignment = right!
boolean focusrectangle = false
end type

type st_cliente from statictext within w_sel_fat_ven_proforma
integer x = 869
integer y = 16
integer width = 1966
integer height = 100
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
long backcolor = 12632256
boolean focusrectangle = false
end type

type cb_annulla from commandbutton within w_sel_fat_ven_proforma
integer x = 366
integer y = 20
integer width = 311
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
boolean cancel = true
end type

event clicked;integer li_ret

li_ret = g_mb.messagebox("APICE","Si è deciso di annullare la selezione!" + char(13) + &
										"Continuare?" + char(13) + char(13) + &
										"N.B. se si sceglie SI, verrà annullata tutta l'operazione!" + char(13)+&
										"N.B. se si sceglie NO si potrà optare ancora per una fattura di vendita!", Question!, yesno!, 1)

choose case li_ret
	case 1 				//SI: 
		s_cs_xx.parametri.parametro_b_1 = true	//parametro per il rollback;
		close(parent)
	
end choose
end event

type cb_seleziona from commandbutton within w_sel_fat_ven_proforma
integer x = 46
integer y = 20
integer width = 311
integer height = 92
integer taborder = 10
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Seleziona"
boolean default = true
end type

event clicked;long ll_cur_row, ll_anno_reg_fattura, ll_num_reg_fattura
string ls_cod_pagamento_fattura, ls_errore
integer li_ret

ll_cur_row = dw_lista.getrow()

if ll_cur_row > 0 then	
	ls_cod_pagamento_fattura = dw_lista.getitemstring(ll_cur_row, "cod_pagamento")
	if isnull(ls_cod_pagamento_fattura) then ls_cod_pagamento_fattura = ""
	
	ll_anno_reg_fattura = dw_lista.getitemdecimal(ll_cur_row, "anno_registrazione")
	ll_num_reg_fattura = dw_lista.getitemdecimal(ll_cur_row, "num_registrazione")
	
	//controlla il tipo di pagamento della fattura
	if ls_cod_pagamento_fattura <> is_cod_pagamento_ordine then
		//tipo di pagamento diverso: chiedi come procedere....	
		
		li_ret = g_mb.messagebox("APICE","La fattura selezionata ha un tipo di pagamento diverso da quello dell'ordine!" + char(13) + &
										"Continuare?" + char(13) + char(13) + &
										"N.B. se si sceglie SI, il valore del tipo di pagamento della fattura selezionata verrà forzato al valore di quello dell'ordine!" + char(13) + &
										"N.B. se si sceglie di NO, si potrà optare per un'altra fattura nell'elenco!" + char(13) + &
										"N.B. se si sceglie ANNULLA verrà annullata tutta l'operazione!", Question!, YesNoCancel!, 1)
										
		choose case li_ret
			case 1		//OK: conferma forzando il tipo pagamento della fattura
				//conferma la fattura forzando il tipo di pagamento
								
				s_cs_xx.parametri.parametro_d_1 = ll_anno_reg_fattura	//anno_registrazione della fattura
				s_cs_xx.parametri.parametro_d_2 = ll_num_reg_fattura		//num_registrazione della fattura
				s_cs_xx.parametri.parametro_b_1 = false							//flag per il rollback;
				s_cs_xx.parametri.parametro_b_2 = true							//flag per l'update del cod. pagamento in testata fattura
				close(parent)
				
			case 2		//NO: seleziona un'altra fattura
				return
				
			case else	//ANNULLA: rollback di tutto
				s_cs_xx.parametri.parametro_b_1 = true							//flag per il rollback;
				close(parent)
				
		end choose
		
	else
		//i tipi di pagamento coincidono OK per la conferma
		s_cs_xx.parametri.parametro_d_1 = ll_anno_reg_fattura 	//anno_registrazione della fattura
		s_cs_xx.parametri.parametro_d_2 = ll_num_reg_fattura		//num_registrazione della fattura
		s_cs_xx.parametri.parametro_b_1 = false							//flag per il rollback;
		s_cs_xx.parametri.parametro_b_2 = false							//flag per l'update del cod. pagamento in testata fattura
		
		close(parent)
	end if

else
	g_mb.messagebox("APICE","E' necessario selezionare una fattura di vendita!", Exclamation!)
end if
end event

type dw_lista from datawindow within w_sel_fat_ven_proforma
integer x = 23
integer y = 140
integer width = 2789
integer height = 1340
integer taborder = 10
string title = "none"
string dataobject = "d_sel_fat_ven_proforma"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
end type

event rowfocuschanged;if currentrow > 0 then
	selectrow(0, false)
	selectrow(currentrow, true)
end if
end event

