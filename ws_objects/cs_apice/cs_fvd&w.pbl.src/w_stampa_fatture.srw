﻿$PBExportHeader$w_stampa_fatture.srw
$PBExportComments$Finestra Contabilizzazione Fatture di Vendita
forward
global type w_stampa_fatture from w_cs_xx_principale
end type
type st_1 from statictext within w_stampa_fatture
end type
type dw_selezione from uo_cs_xx_dw within w_stampa_fatture
end type
type dw_lista from uo_std_dw within w_stampa_fatture
end type
type dw_folder from u_folder within w_stampa_fatture
end type
type dw_log from datawindow within w_stampa_fatture
end type
type dw_esegui from datawindow within w_stampa_fatture
end type
end forward

global type w_stampa_fatture from w_cs_xx_principale
integer width = 4855
integer height = 2472
string title = "Stampa Fatture"
boolean minbox = false
boolean maxbox = false
boolean resizable = false
windowtype windowtype = response!
event ue_close ( )
event ue_azzera_ricerca ( )
st_1 st_1
dw_selezione dw_selezione
dw_lista dw_lista
dw_folder dw_folder
dw_log dw_log
dw_esegui dw_esegui
end type
global w_stampa_fatture w_stampa_fatture

type variables
private:
	string is_email_utente
end variables

forward prototypes
public function integer wf_definitiva (long al_anno, long al_num)
public function integer wf_annulla_ricerca ()
public function integer wf_carica_lista_fatture ()
public function integer wf_esegui_stampa_fatture ()
public function integer wf_invia_mail ()
public subroutine wf_invia_fatel_sdi ()
end prototypes

event ue_close();close(this)
end event

event ue_azzera_ricerca();wf_annulla_ricerca()

end event

public function integer wf_definitiva (long al_anno, long al_num);long     ll_anno_esercizio, ll_num_documento

string   ls_cod_tipo_fat_ven, ls_cod_deposito, ls_flag_blocco, ls_flag_fuori_fido, ls_flag_calcolo, &
         ls_cod_documento, ls_numeratore_documento, ls_flag_tipo_fat_ven

datetime ldt_oggi



ldt_oggi = dw_esegui.getitemdatetime(1,"data_fattura")

if isnull(ldt_oggi) then
	g_mb.error("Impostare una data di fatturazione valida!")
	return -1
else
	update 	parametri_azienda
	set 		data = :ldt_oggi
	where	cod_azienda = :s_cs_xx.cod_azienda and
     			cod_parametro = 'DFT';

	if sqlca.sqlcode <> 0 then
		ldt_oggi = datetime(today())
	end if
end if


select cod_tipo_fat_ven,
       cod_deposito,
		 flag_blocco,
		 flag_fuori_fido,
		 flag_calcolo
into   :ls_cod_tipo_fat_ven,
       :ls_cod_deposito,
       :ls_flag_blocco,
       :ls_flag_fuori_fido,
       :ls_flag_calcolo
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :al_anno and
		 num_registrazione = :al_num;
		 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","FATTURA = " + string(al_anno) + " / " + string(al_num) +  "~nErrore in ricerca testata fattura: " + sqlca.sqlerrtext)
	return -1
end if

ll_anno_esercizio = f_anno_esercizio()

if ls_flag_blocco = 'S' then
	g_mb.messagebox("APICE","FATTURA = " + string(al_anno) + " / " + string(al_num) + "~nFattura Bloccata non stampabile in definitivo.", exclamation!)
	rollback;
	return -1
end if

if ls_flag_fuori_fido = 'S' then
	g_mb.messagebox("APICE","FATTURA = " + string(al_anno) + " / " + string(al_num) + "~nFattura con Cliente Fuori Fido non stampabile in definitivo.", exclamation!)
	rollback;
	return -1
end if

setnull(ls_cod_documento)

select cod_documento,   
		 numeratore_documento  
into   :ls_cod_documento,   
		 :ls_numeratore_documento  
from   anag_depositi  
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_deposito = :ls_cod_deposito;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","FATTURA = " + string(al_anno) + " / " + string(al_num) + "~nErrore durante la lettura dell'Anagrafica Depositi: " + sqlca.sqlerrtext, exclamation!)
	rollback;
	return -1
end if

if isnull(ls_cod_documento) then
	
	select cod_documento,   
			 numeratore_documento,
			 flag_tipo_fat_ven
	into   :ls_cod_documento,   
			 :ls_numeratore_documento,
			 :ls_flag_tipo_fat_ven
	from   tab_tipi_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","FATTURA = " + string(al_anno) + " / " + string(al_num) +"~nErrore durante la lettura della tabella tipi fatture di vendita: " + sqlca.sqlerrtext, exclamation!)
		rollback;
		return -1
	end if
	
end if

select max(numeratori.num_documento)
into   :ll_num_documento  
from   numeratori  
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_documento = :ls_cod_documento and
		 numeratore_documento = :ls_numeratore_documento and
		 anno_documento = :ll_anno_esercizio;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("APICE","FATTURA = " + string(al_anno) + " / " + string(al_num) + &
	           "~nErrore durante la lettura della tabella numeratori: " + sqlca.sqlerrtext, exclamation!)
	rollback;
	return -1
elseif isnull(ll_num_documento) then
	
	ll_num_documento  = 1

	insert into numeratori 
					(cod_azienda,   
					 cod_documento,   
					 numeratore_documento,   
					 anno_documento,   
					 num_documento)  
	values 		(:s_cs_xx.cod_azienda,   
					 :ls_cod_documento,   
					 :ls_numeratore_documento,   
					 :ll_anno_esercizio,   
					 :ll_num_documento);

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","FATTURA = " + string(al_anno) + " / " + string(al_num) +"~nErrore durante l'inserimento del numeratore: " + sqlca.sqlerrtext, exclamation!)
		rollback;
		return -1
	end if
	
elseif not isnull(ll_num_documento) then
	
	ll_num_documento ++

	update numeratori
	set    num_documento = :ll_num_documento  
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_documento = :ls_cod_documento and
			 numeratore_documento = :ls_numeratore_documento and
			 anno_documento = :ll_anno_esercizio;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","FATTURA = " + string(al_anno) + " / " + string(al_num) +"~nErrore durante l'aggiornamento della tabella numeratori: " + sqlca.sqlerrtext, exclamation!)
		rollback;
		return -1
	end if
	
end if

update tes_fat_ven
set    cod_documento = :ls_cod_documento,   
		 numeratore_documento = :ls_numeratore_documento,   
		 anno_documento = :ll_anno_esercizio,   
		 num_documento = :ll_num_documento,   
		 data_fattura = :ldt_oggi
where  cod_azienda = :s_cs_xx.cod_azienda and
		 anno_registrazione = :al_anno and
		 num_registrazione = :al_num;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","FATTURA = " + string(al_anno) + " / " + string(al_num) +"~nErrore durante l'aggiornamento della testata bolla: " + sqlca.sqlerrtext, exclamation!)
	rollback;
	return -1
end if

return 0
end function

public function integer wf_annulla_ricerca ();string ls_null

date   ldd_null

long   ll_null, ll_anno_esercizio


setnull(ls_null)
setnull(ll_null)
setnull(ldd_null)

dw_selezione.setitem(dw_selezione.getrow(), "cod_cliente", ls_null)

ll_anno_esercizio =  f_anno_esercizio()
dw_selezione.setitem(dw_selezione.getrow(), "anno_documento", 2019)
dw_selezione.setitem(dw_selezione.getrow(), "numeratore_documento", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "data_inizio", ldd_null)
dw_selezione.setitem(dw_selezione.getrow(), "data_fine",   ldd_null)
dw_selezione.setitem(dw_selezione.getrow(), "num_documento", ll_null)
dw_selezione.setitem(dw_selezione.getrow(), "num_documento_fine", ll_null)
dw_selezione.setitem(dw_selezione.getrow(), "cod_documento", ls_null)

dw_selezione.setitem(dw_selezione.getrow(), "anno_registrazione", f_anno_esercizio())
dw_selezione.setitem(dw_selezione.getrow(), "data_reg_da", ldd_null)
dw_selezione.setitem(dw_selezione.getrow(), "data_reg_a",   ldd_null)
dw_selezione.setitem(dw_selezione.getrow(), "num_reg_da", ll_null)
dw_selezione.setitem(dw_selezione.getrow(), "num_reg_a", ll_null)
dw_selezione.setitem(dw_selezione.getrow(), "cod_tipo_fat_ven", ls_null)
dw_selezione.setitem(dw_selezione.getrow(), "flag_esito_fatel", "N")

if not isnull(is_email_utente) or is_email_utente <> "" then
	dw_selezione.object.flag_invia_copia_mail_mittente.CheckBox.text = "Invia una copia alla casella mittente:" + is_email_utente
	dw_selezione.setitem(1,"flag_invia_copia_mail_mittente","S")
else
	setnull(is_email_utente)
	dw_selezione.object.flag_invia_copia_mail_mittente.visible = '0'
	dw_selezione.setitem(1,"flag_invia_copia_mail_mittente","S")
end if


return 0
end function

public function integer wf_carica_lista_fatture ();boolean  lb_controllo

string   ls_cod_documento, ls_cod_cliente, ls_numeratore, ls_filtro, ls_cod_tipo_fat_ven, ls_select, &
         ls_flag_inviato_mail, ls_cod_agente_1, ls_cod_agente_2, ls_flag_esito_fatel

long     ll_anno_documento, ll_num_documento, ll_num_documento_fine, ll_ret, ll_anno_reg, ll_num_reg_da, &
         ll_num_reg_a, ll_errore

datetime ldt_data_inizio, ldt_data_fine, ldt_data_reg_da, ldt_data_reg_a


dw_selezione.accepttext()

ls_cod_documento = dw_selezione.getitemstring(1,"cod_documento")
ll_anno_documento = dw_selezione.getitemnumber(1,"anno_documento")
ls_numeratore = dw_selezione.getitemstring(1,"numeratore_documento")
ll_num_documento = dw_selezione.getitemnumber(1,"num_documento")
ll_num_documento_fine = dw_selezione.getitemnumber(1,"num_documento_fine")
ls_cod_cliente = dw_selezione.getitemstring(1,"cod_cliente")
ldt_data_inizio = dw_selezione.getitemdatetime(1,"data_inizio")
ldt_data_fine = dw_selezione.getitemdatetime(1,"data_fine")
ls_cod_tipo_fat_ven = dw_selezione.getitemstring(1,"cod_tipo_fat_ven")
ll_anno_reg = dw_selezione.getitemnumber(1,"anno_registrazione")
ll_num_reg_da = dw_selezione.getitemnumber(1,"num_reg_da")
ll_num_reg_a = dw_selezione.getitemnumber(1,"num_reg_a")
ldt_data_reg_da = dw_selezione.getitemdatetime(1,"data_reg_da")
ldt_data_reg_a = dw_selezione.getitemdatetime(1,"data_reg_a")
ls_flag_inviato_mail = dw_selezione.getitemstring(1,"flag_mail_inviata")
ls_cod_agente_1 = dw_selezione.getitemstring(1,"cod_agente_1")
ls_cod_agente_2 = dw_selezione.getitemstring(1,"cod_agente_2")
ls_flag_esito_fatel = dw_selezione.getitemstring(1,"flag_esito_fatel")


ls_filtro = "SELECT tes_fat_ven.cod_documento, "  + &
         "tes_fat_ven.numeratore_documento,   "  + &
         "tes_fat_ven.anno_documento,   " +  & 
         "tes_fat_ven.num_documento,   "  + &
         "tes_fat_ven.data_fattura,   " +  &
         "tes_fat_ven.cod_tipo_fat_ven,   "  + &
         "tes_fat_ven.anno_registrazione,   " +  & 
         "tes_fat_ven.num_registrazione,   " +  &
         "tes_fat_ven.data_registrazione,   " +  &
         "tes_fat_ven.cod_cliente,   " +  &
         "anag_clienti.rag_soc_1,   "  + &
         "anag_clienti.flag_accetta_mail,   " +  &
         "anag_clienti.email_amministrazione,   " +  &
         "tes_fat_ven.importo_iva_valuta,   "  + &
         "tes_fat_ven.imponibile_iva_valuta, " +  &  
         "tes_fat_ven.tot_fattura_valuta,   " +  &
         "tes_fat_ven.flag_email_inviata,  " +  &
		"tes_fat_ven.flag_esito_fatel, " +  &
  		"tes_fat_ven.identificativo_sdi, " + &
         "anag_clienti.fatel_flag_pa " +  &
	    "FROM tes_fat_ven  " +  &
"LEFT OUTER JOIN anag_clienti ON tes_fat_ven.cod_azienda = anag_clienti.cod_azienda AND tes_fat_ven.cod_cliente = anag_clienti.cod_cliente "+ &
" WHERE tes_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "'"


lb_controllo = false

if not isnull(ls_cod_documento) then
	ls_filtro = ls_filtro + " and tes_fat_ven.cod_documento = '" + ls_cod_documento + "'"
	lb_controllo = true
end if

if not isnull(ll_anno_documento) and ll_anno_documento > 0 then
	ls_filtro = ls_filtro + " and tes_fat_ven.anno_documento = " + string(ll_anno_documento)
	lb_controllo = true
end if

if not isnull(ll_num_documento) and ll_num_documento > 0 then
	ls_filtro = ls_filtro + " and tes_fat_ven.num_documento >= " + string(ll_num_documento)
	lb_controllo = true
end if

if not isnull(ll_num_documento_fine) and ll_num_documento_fine > 0 then
	ls_filtro = ls_filtro + " and tes_fat_ven.num_documento <= " + string(ll_num_documento_fine)
	lb_controllo = true
end if

if not isnull(ls_numeratore) then
	ls_filtro = ls_filtro + " and tes_fat_ven.numeratore_documento = '" + ls_numeratore + "'"
	lb_controllo = true
end if

if not isnull(ls_cod_cliente) then
	ls_filtro = ls_filtro + " and tes_fat_ven.cod_cliente = '" + ls_cod_cliente + "'"
	lb_controllo = true
end if

if not isnull(ldt_data_inizio) then
	ls_filtro = ls_filtro + " and tes_fat_ven.data_fattura >= '" + string(date(ldt_data_inizio),s_cs_xx.db_funzioni.formato_data) + "'"
	lb_controllo = true
end if

if not isnull(ldt_data_fine) then
	ls_filtro = ls_filtro + " and tes_fat_ven.data_fattura <= '" + string(date(ldt_data_fine),s_cs_xx.db_funzioni.formato_data) + "'"
	lb_controllo = true
end if

if not isnull(ls_cod_tipo_fat_ven) then
	ls_filtro = ls_filtro + " and tes_fat_ven.cod_tipo_fat_ven = '" + ls_cod_tipo_fat_ven + "'"
	lb_controllo = true
end if

if not isnull(ll_anno_reg) and ll_anno_reg > 0 then
	ls_filtro = ls_filtro + " and tes_fat_ven.anno_registrazione = " + string(ll_anno_reg)
	lb_controllo = true
end if

if not isnull(ll_num_reg_da) and ll_num_reg_da > 0  then
	ls_filtro = ls_filtro + " and tes_fat_ven.num_registrazione >= " + string(ll_num_reg_da)
	lb_controllo = true
end if

if not isnull(ll_num_reg_a) and ll_num_reg_a > 0 then
	ls_filtro = ls_filtro + " and tes_fat_ven.num_registrazione <= " + string(ll_num_reg_a)
	lb_controllo = true
end if

if not isnull(ldt_data_reg_da) then
	ls_filtro = ls_filtro + " and tes_fat_ven.data_registrazione >= '" + string(date(ldt_data_reg_da),s_cs_xx.db_funzioni.formato_data) + "'"
	lb_controllo = true
end if

if not isnull(ldt_data_reg_a) then
	ls_filtro = ls_filtro + " and tes_fat_ven.data_registrazione <= '" + string(date(ldt_data_reg_a),s_cs_xx.db_funzioni.formato_data) + "'"
	lb_controllo = true
end if

choose case ls_flag_inviato_mail
	case "S"
		ls_filtro = ls_filtro + " and tes_fat_ven.flag_email_inviata = 'S' "
		lb_controllo = true
		
	case "N"
		ls_filtro = ls_filtro + " and tes_fat_ven.flag_email_inviata = 'N' "
		lb_controllo = true
end choose

if not isnull(ls_cod_agente_1) then
	ls_filtro = ls_filtro + " and tes_fat_ven.cod_agente_1 = '" + ls_cod_agente_1 + "'"
	lb_controllo = true
end if

if not isnull(ls_cod_agente_2) then
	ls_filtro = ls_filtro + " and tes_fat_ven.cod_agente_2 = '" + ls_cod_agente_2 + "'"
	lb_controllo = true
end if

choose case ls_flag_esito_fatel
	case "N"
		ls_filtro = ls_filtro + " and tes_fat_ven.flag_esito_fatel = 'NI' and (tes_fat_ven.identificativo_sdi is null or tes_fat_ven.identificativo_sdi = '') "
		lb_controllo = true
	case "E"
		ls_filtro = ls_filtro + " and ( (tes_fat_ven.flag_esito_fatel = 'NI' and (tes_fat_ven.identificativo_sdi is null or tes_fat_ven.identificativo_sdi = '')) or tes_fat_ven.flag_esito_fatel in ('NI','EC') ) "
		lb_controllo = true
end choose

if lb_controllo then
	if dw_lista.settransobject(sqlca) <> 1 then
		g_mb.messagebox("APICE","Errore in caricamento elenco fatture:~nerrore nell'impostazione della transazione di dw_lista")
		return -1
	end if
	
	if dw_lista.setsqlselect(ls_filtro) <> 1 then
		g_mb.messagebox("APICE","Errore in caricamento elenco fatture:~nerrore nell'impostazione della select di dw_lista")
		return -1
	end if
end if	

ll_errore = dw_lista.retrieve()

if ll_errore < 0 then
   g_mb.messagebox("APICE","Errore in caricamento elenco fatture:~nerrore nella retrieve di dw_lista")
	return -1
end if

if dw_lista.rowcount() > 0 then
	dw_esegui.object.b_stampa.enabled='1'
	dw_esegui.object.b_mail.enabled='1'
end if

if dw_lista.setsort("tes_fat_ven.anno_registrazione A, tes_fat_ven.num_registrazione A") = -1 then
	g_mb.messagebox("APICE","Errore in impostazione tipo ordinamento elenco fatture")
	return -1
end if

if dw_lista.sort() = -1 then
	g_mb.messagebox("APICE","Errore in ordinamento elenco fatture")
	return -1
end if

dw_folder.fu_selecttab(2)
end function

public function integer wf_esegui_stampa_fatture ();long   ll_i, ll_anno, ll_num, ll_row, ll_num_documento, ll_anno_documento

string ls_mess, ls_flag_email, ls_email_amministrazione, ls_cod_cliente, ls_cod_documento, ls_numeratore_documento, ls_rag_soc_1

uo_calcola_documento_euro luo_calcola


dw_lista.accepttext()
dw_log.reset()
dw_folder.fu_selecttab( 4 )

dw_esegui.object.b_stampa.enabled='0'
//cb_stampa.enabled = false

delete
from   stampa_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente;
			 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella delete di stampa_fat_ven: " + sqlca.sqlerrtext)
	rollback;
	dw_esegui.object.b_stampa.enabled='1'
//	cb_stampa.enabled = true
	return -1
else
	commit;
end if

for ll_i = 1 to dw_lista.rowcount()
	
	ll_anno = dw_lista.getitemnumber(ll_i,"anno_registrazione")
	ll_num = dw_lista.getitemnumber(ll_i,"num_registrazione")
	
//	if cbx_definitiva.checked then
	if dw_esegui.getitemstring(1,"flag_stampa_definitiva")="S" then
		if isnull(dw_lista.getitemstring(ll_i, "cod_documento")) then
			if wf_definitiva(ll_anno,ll_num) = -1 then
				dw_esegui.object.b_stampa.enabled='1'
				//cb_stampa.enabled = true
				return -1
			end if	
		end if
	end if
	
	st_1.text = "Calcolo documento " + string(ll_anno) + "/" + string(ll_num) + " in corso ..."
	Yield()
	
	luo_calcola = create uo_calcola_documento_euro
	
	if luo_calcola.uof_calcola_documento(ll_anno,ll_num,"fat_ven",ls_mess) <> 0 then
		g_mb.messagebox("APICE","FATTURA = " + string(ll_anno) + " / " + string(ll_num) + "~n" + ls_mess)
		rollback;
	end if
	
	destroy luo_calcola
	
	ls_flag_email            = dw_lista.getitemstring(ll_i,"anag_clienti_flag_accetta_mail")
	ls_email_amministrazione = dw_lista.getitemstring(ll_i,"anag_clienti_email_amministrazione")
	
	if isnull(ls_flag_email) then ls_flag_email = "N"
	
	if isnull(ls_email_amministrazione) or len(trim(ls_email_amministrazione)) < 1 then ls_flag_email = "N"
	
	st_1.text = "Creazione coda di stampa (" + string(ll_anno) + "/" + string(ll_num) + ")"
	
	select 	T.cod_cliente, 
				C.rag_soc_1,
				T.cod_documento,
				T.anno_documento,
				T.numeratore_documento,
				T.num_documento
	into		:ls_cod_cliente,
				:ls_rag_soc_1,
				:ls_cod_documento,
				:ll_anno_documento,
				:ls_numeratore_documento,
				:ll_num_documento
	from		tes_fat_ven T
	left  join anag_clienti C on T.cod_azienda = C.cod_azienda and T.cod_cliente=C.cod_cliente
	where  	T.cod_azienda = :s_cs_xx.cod_azienda and
				T.anno_registrazione = :ll_anno and
				T.num_registrazione = :ll_num;
	if sqlca.sqlcode <> 0 then 
		g_mb.error(g_str.format("Errore in fase di lettura dati dalla fattura con anno registrazione=$1 e numero registrazione=$2.~r~n$3",ll_anno, ll_num, sqlca.sqlerrtext))
		rollback;
		return -1
	end if
	
	
	Yield()

	insert
	into   stampa_fat_ven
	       (cod_azienda,
		    cod_utente,
		    anno_registrazione,
		    num_registrazione,
			 flag_email,
			 email_amministrazione)
	values (:s_cs_xx.cod_azienda,
	        :s_cs_xx.cod_utente,
			  :ll_anno,
			  :ll_num,
			  :ls_flag_email,
			  :ls_email_amministrazione);
			  
	if sqlca.sqlcode <> 0 then
		ll_row = dw_log.insertrow(0)
		dw_log.setitem(ll_row, 1, g_str.format("Fattura $1/$2 $3/$4 cliente $5 $6 inserita nella coda di stampa ...",ls_cod_documento,ls_numeratore_documento,ll_anno_documento, ll_num_documento,ls_rag_soc_1, ls_cod_cliente))
		rollback;
		dw_esegui.object.b_stampa.enabled='1'
		return -1
	else
		ll_row = dw_log.insertrow(0)
		dw_log.setitem(ll_row, 1, g_str.format("Fattura $1/$2 $3/$4 cliente $5 $6 inserita nella coda di stampa ...",ls_cod_documento,ls_numeratore_documento,ll_anno_documento, ll_num_documento,ls_rag_soc_1, ls_cod_cliente))
	end if
	
next

commit;

s_cs_xx.parametri.parametro_b_1 = true
s_cs_xx.parametri.parametro_b_2 = false

st_1.text = "Stampa in corso. Attendere il termine."
Yield()

window_open(w_report_fat_ven_euro, -1)
end function

public function integer wf_invia_mail ();string ls_mess,  ls_flag_email, ls_email_amministrazione, ls_email_utente,ls_cod_cliente, ls_rag_soc_1, ls_cod_documento, ls_numeratore_documento
long   ll_i, ll_anno, ll_num, ll_sleep, ll_anno_documento, ll_num_documento, ll_row
uo_calcola_documento_euro luo_calcola


if g_mb.messagebox("APICE","Procedo con il processo di invio delle E-Mail delle fatture?", Question!, YesNo!,2) = 2 then return 0

dw_esegui.accepttext()

dw_log.reset()
dw_folder.fu_selecttab( 4 )

dw_esegui.object.b_mail.enabled='0'

// nuovo parametro WBS per tempo attesa fra una mail ed un'altra (ha effetto dentro all'oggetto SENDMAIL)
ll_sleep = dw_esegui.getitemnumber(1,"time_wait")

insert into parametri_azienda (cod_azienda, flag_parametro, cod_parametro, numero) values (:s_cs_xx.cod_azienda, 'N', 'WBS', :ll_sleep);

update parametri_azienda set numero = :ll_sleep where cod_azienda = :s_cs_xx.cod_azienda and cod_parametro = 'WBS';

// ------------------------------------------------------------------------------------------------ //

delete
from   stampa_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente;
			 
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore nella delete di stampa_fat_ven: " + sqlca.sqlerrtext)
	rollback;
	dw_esegui.object.b_stampa.enabled='1'
//	cb_stampa.enabled = true
	return -1
else
	commit;
end if

// stefanop: 17/06/2014
if dw_selezione.getitemstring(1,"flag_invia_copia_mail_mittente")="S" then
	ls_email_utente = is_email_utente
else
	setnull(ls_email_utente)
end if


for ll_i = 1 to dw_lista.rowcount()
	
	ls_cod_documento = dw_lista.getitemstring(ll_i,"cod_documento")
	
	// se la fattura non è stampata in definitivo non la mando via mail.
	if isnull(ls_cod_documento) or len(ls_cod_documento) < 1 then continue 
	
	ls_flag_email = dw_lista.getitemstring(ll_i,"anag_clienti_flag_accetta_mail")
	ls_email_amministrazione = dw_lista.getitemstring(ll_i,"anag_clienti_email_amministrazione")
	
	// la fattura non è selezionata per l'invio
	if ls_flag_email = "N" then continue 
	
	// indirizzo di destinazione non valido
	if isnull(ls_email_amministrazione) or len(ls_email_amministrazione) < 1 or pos(ls_email_amministrazione, "@") < 1 then continue
	
	ll_anno = dw_lista.getitemnumber(ll_i,"anno_registrazione")
	ll_num = dw_lista.getitemnumber(ll_i,"num_registrazione")
	
	if dw_esegui.getitemstring(1,"flag_stampa_definitiva")="S" then
		if isnull(dw_lista.getitemstring(ll_i, "cod_documento")) then
			if wf_definitiva(ll_anno,ll_num) = -1 then
				dw_esegui.object.b_stampa.enabled='1'
				return -1
			end if	
		end if
	end if
	
	st_1.text = "Calcolo documento " + string(ll_anno) + "/" + string(ll_num) + " in corso ..."
	Yield()

	luo_calcola = create uo_calcola_documento_euro
	
	if luo_calcola.uof_calcola_documento(ll_anno,ll_num,"fat_ven",ls_mess) <> 0 then
		g_mb.messagebox("APICE","FATTURA = " + string(ll_anno) + " / " + string(ll_num) + "~n" + ls_mess)
		rollback;
	end if
	
	destroy luo_calcola
	
	if isnull(ls_flag_email) then ls_flag_email = "N"
	
	if isnull(ls_email_amministrazione) or len(trim(ls_email_amministrazione)) < 1 then ls_flag_email = "N"
	
	st_1.text = "Creazione coda di stampa (" + string(ll_anno) + "/" + string(ll_num) + ")"
	
	select 	T.cod_cliente, 
				C.rag_soc_1,
				T.cod_documento,
				T.anno_documento,
				T.numeratore_documento,
				T.num_documento
	into		:ls_cod_cliente,
				:ls_rag_soc_1,
				:ls_cod_documento,
				:ll_anno_documento,
				:ls_numeratore_documento,
				:ll_num_documento
	from		tes_fat_ven T
	left  join anag_clienti C on T.cod_azienda = C.cod_azienda and T.cod_cliente=C.cod_cliente
	where  	T.cod_azienda = :s_cs_xx.cod_azienda and
				T.anno_registrazione = :ll_anno and
				T.num_registrazione = :ll_num;
	if sqlca.sqlcode <> 0 then 
		g_mb.error(g_str.format("Errore in fase di lettura dati dalla fattura con anno registrazione=$1 e numero registrazione=$2.~r~n$3",ll_anno, ll_num, sqlca.sqlerrtext))
		rollback;
		return -1
	end if
	
	Yield()

	insert into stampa_fat_ven (
		cod_azienda,
		cod_utente,
		anno_registrazione,
		num_registrazione,
		flag_email,
		email_amministrazione,
		email_utente
	) values (
		:s_cs_xx.cod_azienda,
		:s_cs_xx.cod_utente,
		:ll_anno,
		:ll_num,
		:ls_flag_email,
		:ls_email_amministrazione,
		:ls_email_utente
	);
			  
	if sqlca.sqlcode <> 0 then
		ll_row = dw_log.insertrow(0)
		dw_log.setitem(ll_row, 1, g_str.format("Fattura $1/$2 $3/$4 cliente $5 $6 inserita nella coda di invio mail ...",ls_cod_documento,ls_numeratore_documento,ll_anno_documento, ll_num_documento,ls_rag_soc_1, ls_cod_cliente))
		rollback;
		dw_esegui.object.b_stampa.enabled='1'
		return -1
	else
		ll_row = dw_log.insertrow(0)
		dw_log.setitem(ll_row, 1, g_str.format("Fattura $1/$2 $3/$4 cliente $5 $6 inserita nella coda di invio mail ...",ls_cod_documento,ls_numeratore_documento,ll_anno_documento, ll_num_documento,ls_rag_soc_1, ls_cod_cliente))
	end if
	
next

commit;

st_1.text = "Stampa in corso. Attendere il termine."
Yield()

s_cs_xx.parametri.parametro_b_1 = true
s_cs_xx.parametri.parametro_b_2 = true

window_open(w_report_fat_ven_euro, -1)
end function

public subroutine wf_invia_fatel_sdi ();string ls_mess, ls_cod_documento, ls_paese, ls_partita_iva, ls_filelocation, ls_message, ls_errore, ls_flag_esito_fatel, ls_identificativo_sdi, ls_flag_pa, &
		ls_cod_cliente, ls_blocco, ls_id_sdi, ls_flag_tipo_cliente,ls_numeratore_documento
long   ll_i, ll_anno_registrazione, ll_num_registrazione, ll_ret, ll_mancanti, ll_anno_documento,ll_num_documento, ll_row
uo_calcola_documento_euro luo_calcola
uo_fatel luo_fatel

if g_mb.messagebox("APICE","Procedo con il processo di invio delle fatture al SDI dell'Agenzia delle Entrate?", Question!, YesNo!,2) = 2 then return

dw_esegui.accepttext()
dw_log.reset()
dw_folder.fu_selecttab(4)

dw_esegui.object.b_sdi.enabled='0'

for ll_i = 1 to dw_lista.rowcount()
	
	ls_cod_documento = dw_lista.getitemstring(ll_i,"cod_documento")
	
	ll_anno_registrazione = dw_lista.getitemnumber(ll_i,"anno_registrazione")
	ll_num_registrazione = dw_lista.getitemnumber(ll_i,"num_registrazione")
	
	// Assegno il numero fiscale al documento
	if isnull(dw_lista.getitemstring(ll_i, "cod_documento")) then
		if wf_definitiva(ll_anno_registrazione,ll_num_registrazione) = -1 then
			g_mb.error(g_str.format("Errore in fase di assegnazione numero fiscale alla fattura con anno registrazione=$1 e numero registrazione=$2",ll_anno_registrazione, ll_num_registrazione))
			goto EndForError
		end if	
	end if
	
	st_1.text = "Calcolo documento " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + " in corso ..."
	Yield()

	luo_calcola = create uo_calcola_documento_euro
	
	if luo_calcola.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"fat_ven",ls_mess) <> 0 then
		g_mb.error("CALCOLO FATTURA","FATTURA = " + string(ll_anno_registrazione) + " / " + string(ll_num_registrazione) + "~n" + ls_mess)
		ll_row = dw_log.insertrow(0)
		dw_log.setitem(ll_row,1,"CALCOLO FATTURA: FATTURA = " + string(ll_anno_registrazione) + " / " + string(ll_num_registrazione) + "~n" + ls_mess)
		rollback;
		continue 
	end if
	
	destroy luo_calcola
	
	st_1.text = "Invio a SDI della farttura (" + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione) + ")"
	Yield()
	
	// -------------  inizio processo di invio fattura ------------------------
	
	luo_fatel = CREATE uo_fatel
	
	select 	T.cod_cliente, 
				T.flag_esito_fatel,
				C.fatel_flag_pa,
				T.identificativo_sdi,
				C.flag_tipo_cliente,
				T.cod_documento,
				T.anno_documento,
				T.numeratore_documento,
				T.num_documento
	into		:ls_cod_cliente,
				:ls_flag_esito_fatel,
				:ls_flag_pa,
				:ls_id_sdi,
				:ls_flag_tipo_cliente,
				:ls_cod_documento,
				:ll_anno_documento,
				:ls_numeratore_documento,
				:ll_num_documento
	from		tes_fat_ven T
	left  join anag_clienti C on T.cod_azienda = C.cod_azienda and T.cod_cliente=C.cod_cliente
	where  	T.cod_azienda = :s_cs_xx.cod_azienda and
				T.anno_registrazione = :ll_anno_registrazione and
				T.num_registrazione = :ll_num_registrazione;
	if sqlca.sqlcode <> 0 then 
		g_mb.error(g_str.format("Errore in fase di lettura dati dalla fattura con anno registrazione=$1 e numero registrazione=$2.~r~n$3",ll_anno_registrazione, ll_num_registrazione, sqlca.sqlerrtext))
		ll_row = dw_log.insertrow(0)
		dw_log.setitem(ll_row,1,g_str.format("Errore in fase di lettura dati dalla fattura con anno registrazione=$1 e numero registrazione=$2.~r~n$3",ll_anno_registrazione, ll_num_registrazione, sqlca.sqlerrtext))
		rollback;
		continue
	end if
	if ls_flag_tipo_cliente <> "F" and ls_flag_tipo_cliente <> "S" then
		g_mb.error(g_str.format("Fattura $1/$2 $3/$4",ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento),"La fattura elettronica può essere trasmessa solo a soggetti che sono Persone Fisiche o Società.")
		ll_row = dw_log.insertrow(0)
		dw_log.setitem(ll_row,1,g_str.format("Fattura $1/$2 $3/$4 La fattura elettronica può essere trasmessa solo a soggetti che sono Persone Fisiche o Società.",ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento))
		rollback;
		continue
	end if
		
	if not isnull(ls_id_sdi)  and len(ls_id_sdi) > 0 then
		if not g_mb.confirm(g_str.format("Fattura $1/$2 $3/$4",ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento),g_str.format("E' già stata eseguita una trasmissione con codice SDI=$1; procedo lo stesso?.",ls_id_sdi),2)  then 
			rollback;
			continue
		end if
	end if
		
	if ls_flag_esito_fatel = "RC" then
		g_mb.warning(g_str.format("Fattura $1/$2 $3/$4",ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento),"La fattura è già stata inviata con successo al SDI. Un ulteriore invio causerebbe un errore di trasmissione.")
		ll_row = dw_log.insertrow(0)
		dw_log.setitem(ll_row,1,g_str.format("Fattura $1/$2 $3/$4 La fattura è già stata inviata con successo al SDI. Un ulteriore invio causerebbe un errore di trasmissione.",ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento))
		continue
	end if

	if isnull(ls_flag_pa) or len(ls_flag_pa) < 1 then
		g_mb.error(g_str.format("Fattura $1/$2 $3/$4",ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento),"Attenzione: nell'anagrafica cliente manca l'indicazione se PRIVATO oppure PA. Modificare l'anagrafica e poi riprovare l'invio.")
		ll_row = dw_log.insertrow(0)
		dw_log.setitem(ll_row,1,g_str.format("Fattura $1/$2 $3/$4 Attenzione: nell'anagrafica cliente manca l'indicazione se PRIVATO oppure PA. Modificare l'anagrafica e poi riprovare l'invio." ,ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento))
		continue
	end if
	
	if ls_flag_pa = "S" then
		// Se cliente PA salto la fattura perchè il file p7m deve essere caricato a mano dalla gestione fatture
		// in teoria le fatture PA non vengono neanche caricate nella lista, ma non si sa mai ....
		ll_row = dw_log.insertrow(0)
		dw_log.setitem(ll_row,1,g_str.format("Fattura $1/$2 $3/$4 Attenzione: questa è una fattura PA e verrà saltata." ,ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento))
		continue
	end if
	

	select flag
	into   :ls_blocco
	from   parametri_azienda
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_parametro = 'BSM';
			 
	if sqlca.sqlcode <> 0 then
		ls_blocco = "N"
	end if
	
	if ls_blocco = "S" then
	
		select count(*)
		into   :ll_mancanti
		from   det_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione and
				 prod_mancante = 'S';
				 
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox(g_str.format("Fattura $1/$2 $3/$4",ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento),"Errore in controllo prodotti mancanti su det_fat_ven: " + sqlca.sqlerrtext)
		elseif not isnull(ll_mancanti) and ll_mancanti > 0 then
			g_mb.messagebox(g_str.format("Fattura $1/$2 $3/$4",ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento),"Sono presenti prodotti mancanti nei dettagli della fattura. Stampa interrotta.")
			return
		end if
		
	end if
	
	// Fine controlli
	
	ll_ret = luo_fatel.uof_invia_fattura( ll_anno_registrazione, ll_num_registrazione, ls_flag_pa, "", "", ref ls_identificativo_sdi, ref ls_message, ref ls_errore)
	
	choose case ll_ret
		case -1
			// errore durante invio
			ll_row = dw_log.insertrow(0)
			dw_log.setitem(ll_row,1,g_str.format("ERRORE INVIO FATTURA $1/$2 $3/$4 - $5",ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento, ls_message))
			g_mb.error(g_str.format("ERRORE INVIO FATTURA $1/$2 $3/$4",ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento), ls_message)
		case -2
			// errore durante archiviazione fattura
			ll_row = dw_log.insertrow(0)
			dw_log.setitem(ll_row,1,g_str.format("ERRORE ARCHIVIAZIONE IN APICE $1/$2 $3/$4 - $5",ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento, ls_message))
			g_mb.error(g_str.format("ERRORE ARCHIVIAZIONE IN APICE $1/$2 $3/$4",ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento), ls_message)
		case 0
			ll_row = dw_log.insertrow(0)
			dw_log.setitem(ll_row,1,g_str.format("Fattura $1/$2 $3/$4 fattura trasmessa correttamente al SDI con ID =$5~r~n$6." ,ls_cod_documento, ls_numeratore_documento, ll_anno_documento, ll_num_documento, ls_identificativo_sdi, ls_message))
			// invio OK, non segnalo niente.
			//g_mb.show( ls_message )
	end choose
	destroy luo_fatel

	
	// -------------------------------------------------------------------------

next

commit;
dw_esegui.object.b_sdi.enabled='1'
return

EndForError:
dw_esegui.object.b_sdi.enabled='1'
rollback;
return


end subroutine

on w_stampa_fatture.create
int iCurrent
call super::create
this.st_1=create st_1
this.dw_selezione=create dw_selezione
this.dw_lista=create dw_lista
this.dw_folder=create dw_folder
this.dw_log=create dw_log
this.dw_esegui=create dw_esegui
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.st_1
this.Control[iCurrent+2]=this.dw_selezione
this.Control[iCurrent+3]=this.dw_lista
this.Control[iCurrent+4]=this.dw_folder
this.Control[iCurrent+5]=this.dw_log
this.Control[iCurrent+6]=this.dw_esegui
end on

on w_stampa_fatture.destroy
call super::destroy
destroy(this.st_1)
destroy(this.dw_selezione)
destroy(this.dw_lista)
destroy(this.dw_folder)
destroy(this.dw_log)
destroy(this.dw_esegui)
end on

event pc_setwindow;call super::pc_setwindow;long ll_count, ll_sleep
date ldd_data
windowobject lw_oggetti[]

set_w_options(c_noenablepopup + c_closenosave)

dw_selezione.set_dw_options(sqlca, &
                            pcca.null_object, &
                            c_nomodify + &
                            c_nodelete + &
                            c_newonopen + &
                            c_disableCC, &
                            c_noresizedw + &
                            c_nohighlightselected + &
                            c_cursorrowpointer)

save_on_close(c_socnosave)


lw_oggetti[1] = dw_selezione
dw_folder.fu_assigntab(1, "Ricerca", lw_oggetti[])

lw_oggetti[1] = dw_lista
dw_folder.fu_assigntab(2, "Elenco Fatture", lw_oggetti[])

lw_oggetti[1] = dw_log
dw_folder.fu_assigntab(4, "Risultati", lw_oggetti[])

lw_oggetti[1] = st_1
lw_oggetti[2] = dw_esegui
dw_folder.fu_assigntab(3, "Invio", lw_oggetti[])

dw_folder.fu_foldercreate(4, 4)
dw_folder.fu_selecttab(1)

dw_esegui.insertrow(0)

guo_functions.uof_get_parametro_azienda("WBS",ll_sleep)		
if isnull(ll_sleep) or ll_sleep = 0 then ll_sleep = 15
dw_esegui.setitem(1,"time_wait", ll_sleep)

select data
into	:ldd_data
from	parametri_azienda
where cod_azienda = :s_cs_xx.cod_azienda and cod_parametro = 'DFT';
//guo_functions.uof_get_parametro_azienda("DFT",ldd_data)		
if isnull(ldd_data) or sqlca.sqlcode = 100 then ldd_data= today()
dw_esegui.setitem(1,"data_fattura", datetime(ldd_data,00:00:00))


select count(*)
into   :ll_count
from   stampa_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente;
		 
if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore nella select di stampa_fat_ven: " + sqlca.sqlerrtext)
	postevent("ue_close")
	return	
elseif ll_count > 0 then
	if g_mb.messagebox("APICE","Per questo utente è già attivo un processo di stampa." + &
	              "~nContinuando il processo attuale sarà sovrascritto. Procedere ugualmente?",question!,yesno!,2) = 2 then
		postevent("ue_close")
		return
	end if
end if	

// stefanop: 17/06/2014: aggiunta possibilità di inviare una mail all'utente collegato
select e_mail
into :is_email_utente
from utenti
where cod_utente = :s_cs_xx.cod_utente;

st_1.text = ""

event post ue_azzera_ricerca()




end event

event pc_setddlb;f_po_loaddddw_dw(dw_selezione, &
                 "cod_documento", &
                 sqlca, &
                 "tab_documenti", &
                 "cod_documento", &
                 "des_documento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
f_po_loaddddw_dw(dw_selezione, &
                 "cod_tipo_fat_ven", &
                 sqlca, &
                 "tab_tipi_fat_ven", &
                 "cod_tipo_fat_ven", &
                 "des_tipo_fat_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
 f_po_loaddddw_dw(dw_selezione, &
                 "cod_agente_1", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  
 f_po_loaddddw_dw(dw_selezione, &
                 "cod_agente_2", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
					  

end event

type st_1 from statictext within w_stampa_fatture
integer x = 23
integer y = 2280
integer width = 4800
integer height = 80
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Tahoma"
long textcolor = 33554432
long backcolor = 12632256
boolean border = true
borderstyle borderstyle = stylelowered!
boolean focusrectangle = false
end type

type dw_selezione from uo_cs_xx_dw within w_stampa_fatture
integer x = 69
integer y = 124
integer width = 3461
integer height = 1596
integer taborder = 10
string dataobject = "d_sel_stampa_fatt_nr_nr"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_selezione,"cod_cliente")
	case "b_annulla"
		wf_annulla_ricerca()
	case "b_ricerca"
		wf_carica_lista_fatture()
end choose
end event

type dw_lista from uo_std_dw within w_stampa_fatture
integer x = 69
integer y = 120
integer width = 4709
integer height = 2240
integer taborder = 90
string dataobject = "d_lista_stampa_fatt_nr_nr"
boolean hscrollbar = true
boolean vscrollbar = true
boolean border = false
end type

event doubleclicked;deleterow(row)

if rowcount() < 1 then
	dw_esegui.object.b_stampa.enabled='0'
//	cb_stampa.enabled = false
end if
end event

type dw_folder from u_folder within w_stampa_fatture
integer x = 23
integer width = 4800
integer height = 2380
integer taborder = 90
end type

type dw_log from datawindow within w_stampa_fatture
integer x = 46
integer y = 120
integer width = 4731
integer height = 2220
integer taborder = 110
string title = "none"
string dataobject = "d_lista_stampa_fatt_log"
boolean border = false
boolean livescroll = true
end type

type dw_esegui from datawindow within w_stampa_fatture
integer x = 69
integer y = 120
integer width = 4709
integer height = 1760
integer taborder = 100
string title = "none"
string dataobject = "d_lista_stampa_fatt_esegui"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;choose case dwo.name
	case "b_stampa"
		wf_esegui_stampa_fatture()
		
	case "b_mail"
		wf_invia_mail()

	case "b_sdi"
		wf_invia_fatel_sdi()

end choose
end event

