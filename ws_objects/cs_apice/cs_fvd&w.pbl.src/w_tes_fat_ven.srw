﻿$PBExportHeader$w_tes_fat_ven.srw
forward
global type w_tes_fat_ven from w_cs_xx_treeview
end type
type dw_tes_fat_ven_det_1 from uo_cs_xx_dw within det_1
end type
type det_2 from userobject within tab_dettaglio
end type
type dw_tes_fat_ven_det_2 from uo_cs_xx_dw within det_2
end type
type det_2 from userobject within tab_dettaglio
dw_tes_fat_ven_det_2 dw_tes_fat_ven_det_2
end type
type det_3 from userobject within tab_dettaglio
end type
type dw_tes_fat_ven_det_3 from uo_cs_xx_dw within det_3
end type
type det_3 from userobject within tab_dettaglio
dw_tes_fat_ven_det_3 dw_tes_fat_ven_det_3
end type
type det_4 from userobject within tab_dettaglio
end type
type dw_tes_fat_ven_det_4 from uo_cs_xx_dw within det_4
end type
type det_4 from userobject within tab_dettaglio
dw_tes_fat_ven_det_4 dw_tes_fat_ven_det_4
end type
type det_5 from userobject within tab_dettaglio
end type
type dw_tes_fat_ven_det_5 from uo_cs_xx_dw within det_5
end type
type det_5 from userobject within tab_dettaglio
dw_tes_fat_ven_det_5 dw_tes_fat_ven_det_5
end type
type tabpage_1 from userobject within tab_ricerca
end type
type dw_notifiche from uo_std_dw within tabpage_1
end type
type tabpage_1 from userobject within tab_ricerca
dw_notifiche dw_notifiche
end type
end forward

global type w_tes_fat_ven from w_cs_xx_treeview
integer width = 4603
integer height = 2632
string title = "Fatture Attive"
event pc_menu_dettagli ( )
event pc_menu_stampa ( )
event pc_menu_contabilizza ( )
event pc_menu_calcola ( )
event pc_menu_duplicadocumento ( )
event pc_menu_ricalcolaspesetrasp ( )
event pc_menu_conferma ( )
event pc_menu_blocca ( )
event pc_menu_sblocca ( )
event pc_menu_corrispondenze ( )
event pc_menu_note ( )
event pc_menu_import_excel ( )
event pc_menu_iva ( )
event pc_menu_contropartite ( )
event pc_menu_scadenze ( )
event pc_menu_xml_fatel ( )
event pc_menu_invio_fatel ( )
event ue_leggi_notifiche ( )
event pc_menu_duplicasunotacredito ( )
end type
global w_tes_fat_ven w_tes_fat_ven

type variables
private:
	int 	ICONA_CLIENTE,ICONA_ANNO, ICONA_TIPO_FATTURA,ICONA_FATTURA ,  ICONA_FATTURA_ELIMINATA ,&
			ICONA_FATTURA_SELEZIONATA ,ICONA_CONTATTO, ICONA_FORNITORE, &
			ICONA_PAGAMENTO, ICONA_CAUSALE, ICONA_PRODOTTO, ICONA_NEW, &
			ICONA_FATTURA_CONTABILIZZATA, ICONA_FATTURA_CONFERMATA, ICONA_FATTURA_ESITO_SCARTATA
	boolean ib_apertura = false
	
	boolean	ib_menu_conferma=true
	
	long		il_numero_riga, il_livello_corrente, il_handle, il_anno_registrazione, il_num_registrazione,&
				il_livello=0

	boolean 	ib_tree_deleting = false, ib_retrieve=true, ib_spese_trasporto=false

	string 	is_ordini_riaperti, is_sql_prodotto

	string		is_cod_parametro_blocco = "IFV"
	
	string		is_mov_intra=""

	uo_fido_cliente			iuo_fido
	datastore ids_store
	long il_max_record = 200

public:
	boolean ib_new
	
end variables

forward prototypes
public subroutine wf_treeview_icons ()
public subroutine wf_imposta_ricerca ()
public function boolean wf_cmn ()
public function boolean wf_controlla_duplicazione (integer ai_anno_doc, long al_num_doc)
public function integer wf_controlla_iva_righe (integer ai_anno_doc, long al_num_doc)
public subroutine wf_lista_ordini_riaperti (string as_ordine, ref string as_ordini[], ref string as_testo)
public subroutine wf_reset_tipo_fattura ()
public function integer wf_riapri_ordini (integer fi_anno_fattura, long fl_num_fattura, ref string fs_errore)
public function integer wf_tipo_fattura (string as_flag_tipo_fat_ven, boolean ab_rowfocuschange)
public function integer wf_inserisci_anni (long al_handle)
public function integer wf_inserisci_fatture (long al_handle)
public function long wf_leggi_livello (long al_handle, integer ai_livello)
public function integer wf_inserisci_soggetto (long al_handle)
public subroutine wf_valori_livelli ()
public function integer wf_leggi_parent (long al_handle, ref string as_sql)
public function integer wf_inserisci_pagamenti (long al_handle)
public function integer wf_inserisci_tipi_fatture (long al_handle)
public function integer wf_inserisci_causali (long al_handle)
public function any wf_inserisci_prodotti (long al_handle)
public function integer wf_inserisci_fattura_vendita (long al_handle, long al_anno_registrazione, long al_num_registrazione)
public subroutine wf_contabilizza_fattura (long al_anno_registrazione, long al_num_registrazione)
public subroutine wf_abilita_pulsanti (boolean ab_status)
public subroutine wf_duplica_documento (string as_flag_crea_nota_credito, string as_cod_tipo_fat_nc)
end prototypes

event pc_menu_dettagli();// Aggiunto EnMe 30-07-2010 per PTENDA; ordinamento righe fattura
// se le righe ancora non sono ordinate procedo con il riordino secondo secondo la riga fattura
boolean lb_flag=false  
long ll_anno_registrazione, ll_num_registrazione
window	lw_window 

ll_anno_registrazione = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"anno_registrazione")
ll_num_registrazione  = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"num_registrazione")


update 	det_fat_ven
set    		ordinamento = prog_riga_fat_ven
where  	cod_azienda = :s_cs_xx.cod_azienda and
       		anno_registrazione = :ll_anno_registrazione and
		 	num_registrazione = :ll_num_registrazione and (ordinamento is null or ordinamento = 0);

commit;

// poi procedo con l'apertura del dettaglio righe fattura.

s_cs_xx.parametri.parametro_w_fat_ven = this
guo_functions.uof_get_parametro_azienda( "GIB", lb_flag )
if not lb_flag then
	window_open_parm(w_det_fat_ven, -1, tab_dettaglio.det_1.dw_tes_fat_ven_det_1)
else 
	window_type_open_parm( lw_window, "w_det_fat_ven_ptenda", -1, tab_dettaglio.det_1.dw_tes_fat_ven_det_1)
end if






end event

event pc_menu_stampa();long			ll_mancanti

string			ls_blocco, ls_cod_cliente, ls_messaggio, ls_cod_documento

integer		li_ret

datetime		ldt_data_reg


s_cs_xx.parametri.parametro_d_1 = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"anno_registrazione")
s_cs_xx.parametri.parametro_d_2 = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"num_registrazione")

select flag
into   :ls_blocco
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'BSM';
		 
if sqlca.sqlcode <> 0 then
	ls_blocco = "N"
end if

if ls_blocco = "S" then

	select count(*)
	into   :ll_mancanti
	from   det_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
			 num_registrazione = :s_cs_xx.parametri.parametro_d_2 and
			 prod_mancante = 'S';
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in controllo prodotti mancanti su det_fat_ven: " + sqlca.sqlerrtext)
	elseif not isnull(ll_mancanti) and ll_mancanti > 0 then
		g_mb.messagebox("APICE","Sono presenti prodotti mancanti nei dettagli della fattura. Stampa interrotta.")
		return
	end if
	
end if

s_cs_xx.parametri.parametro_i_1 = 0

if isnull(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "cod_documento")) then
	window_open(w_tipo_stampa_fat_ven, 0)
	if s_cs_xx.parametri.parametro_i_1 = -1 then
		rollback;
		return
	end if
	triggerevent("pc_retrieve")
end if

triggerevent("pc_menu_calcola")

if tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow() > 0 then
	
	long ll_riga	
	
	ll_riga = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow()
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.change_dw_focus(tab_dettaglio.det_1.dw_tes_fat_ven_det_1)

	ls_cod_documento = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(ll_riga, "cod_documento")
	
	if ls_cod_documento<>"" and not isnull(ls_cod_documento) then
		//vuol dire che la fattura è stata già stampata in definitivo, quindi non controllare
	else
		ls_cod_cliente = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(ll_riga, "cod_cliente")
		ldt_data_reg = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemdatetime(ll_riga, "data_registrazione")
	
		//***************************************************************
		li_ret = iuo_fido.uof_get_blocco_cliente( ls_cod_cliente, ldt_data_reg, "SFV", ls_messaggio)
		if li_ret=2 then
			//blocco
			g_mb.error(ls_messaggio)
			return
		elseif li_ret=1 then
			//solo warning
			g_mb.warning(ls_messaggio)
		end if
		//***************************************************************
		
	end if

	s_cs_xx.parametri.parametro_d_1 = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(ll_riga,"anno_registrazione")
	s_cs_xx.parametri.parametro_d_2 = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(ll_riga,"num_registrazione")
		 
	window_open(w_report_fat_ven_euro, -1)
	
end if	
end event

event pc_menu_contabilizza();long ll_anno_registrazione, ll_num_registrazione


ll_anno_registrazione = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"anno_registrazione")
ll_num_registrazione = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"num_registrazione")

wf_contabilizza_fattura( ll_anno_registrazione, ll_num_registrazione)

tab_dettaglio.det_1.dw_tes_fat_ven_det_1.change_dw_current()
tab_dettaglio.det_1.dw_tes_fat_ven_det_1.postevent("pcd_retrieve")
end event

event pc_menu_calcola();string ls_messaggio, ls_MSC, ls_flag_contabilita
		 
long   ll_anno_reg, ll_num_reg, ll_count

uo_calcola_documento_euro luo_calcola_documento_euro


ll_anno_reg = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"anno_registrazione")
ll_num_reg = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"num_registrazione")


//27/12/2010 Donato x Sicurservice ------------------------------------------------------------------------------------------------------------
//se il parametro è posto a SI e sono presenti fatture di vendita con scadenze con flag pagato posto a SI
//Avvisa prima di procedere perchè il calcolo reimposta tale flag a No (delete + insert)
//tutto pero dipende dal parametro MSC

select flag
into :ls_MSC
from parametri_azienda
where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_parametro = 'MSC' and
			flag_parametro = 'F';
			
if sqlca.sqlcode <> 0 or isnull(ls_MSC) or ls_MSC="" then
	ls_MSC = "N"
end if

if ls_MSC="S" then
	select flag_contabilita
	into :ls_flag_contabilita
	from tes_fat_ven
	where 	cod_azienda=:s_cs_xx.cod_azienda and
				anno_registrazione=:ll_anno_reg and
				num_registrazione=:ll_num_reg;
				
	if ls_flag_contabilita="N" or isnull(ls_flag_contabilita) or ls_flag_contabilita="" then
		ll_count = 0
		
		//verifica l'esistenza di scadenze con flag_pagata posto a SI
		select count(*)
		into :ll_count
		from scad_fat_ven
		where 	cod_azienda=:s_cs_xx.cod_azienda and
					anno_registrazione=:ll_anno_reg and
					num_registrazione=:ll_num_reg and
					flag_pagata = 'S';
		
		if ll_count>0 then
			//alert
			if not g_mb.confirm("Attenzione! La fattura non è stata ancora contabilizzata ed esistono delle scadenze con flag pagata posto a Si! "+ &
						"Il ricalcolo della fattura causerà la perdita della memorizzazione del flag pagata per le scadenze di questa fattura! Continuare?") then
				return
			end if
		end if
	
	end if
end if
//--------------------------------------------------------------------------------------------------------------------------------------------------

luo_calcola_documento_euro = create uo_calcola_documento_euro

//Donato 06-11-2008 metto a true per non fare di nuovo il controllo sull'esposizione cliente
luo_calcola_documento_euro.ib_salta_esposiz_cliente = true

if luo_calcola_documento_euro.uof_calcola_documento(ll_anno_reg,ll_num_reg,"fat_ven",ls_messaggio) <> 0 then
	//Donato 05-11-2008 dare msg solo se c'è
	if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.messagebox("APICE",ls_messaggio)
	//g_mb.messagebox("APICE",ls_messaggio)
	rollback;
	return
else
	commit;
end if
/*
if luo_calcola_documento_euro.uof_calcola_documento(ll_anno_reg,ll_num_reg,"fat_ven",ls_messaggio) <> 0 then
	g_mb.messagebox("APICE",ls_messaggio)
	destroy luo_calcola_documento_euro
	rollback;
	return -1
else
	commit;		
end if
*/
destroy luo_calcola_documento_euro

tab_dettaglio.det_1.dw_tes_fat_ven_det_1.change_dw_current()
tab_dettaglio.det_1.dw_tes_fat_ven_det_1.postevent("pcd_retrieve")
end event

event pc_menu_duplicadocumento();wf_duplica_documento("N","")
end event

event pc_menu_ricalcolaspesetrasp();integer								li_anno_documento, li_risposta
long									ll_num_documento, ll_row
uo_spese_trasporto				luo_spese_trasporto
uo_calcola_documento_euro	luo_calcolo
string									ls_errore

//se le spese trasporto su Zone/Livelli Prodotti non sono attivate non fare niente
//(infatti in tal caso il pulsante non dovrebbe esere neanche visibile: vedi script pc_setwindow)
if not ib_spese_trasporto then return


ll_row = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow()

if ll_row>0 then
else
	return
end if

li_anno_documento = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(ll_row, "anno_registrazione")
ll_num_documento  = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(ll_row, "num_registrazione")

if not g_mb.confirm("Ricalcolare le spese trasporto (in base alle Zone e Livelli Prodotti) per la fattura n° " + &
						   string(li_anno_documento)+"/"+string(ll_num_documento)+"?") 		then
	return
end if

setpointer(Hourglass!)
pcca.mdi_frame.setmicrohelp("Ricalcolo Spese trasporto in corso... ")


//di seguito calcolo il documento per includere eventuali modifiche a prezzo o varie ... nell'imponibile delle righe
luo_calcolo = create uo_calcola_documento_euro

//di seguito ri-calcolo il documento per includere nel calcolo le spese di trasporto
if luo_calcolo.uof_calcola_documento(li_anno_documento, ll_num_documento,"fat_ven", ls_errore) <> 0 then
	rollback;
	destroy luo_calcolo
	pcca.mdi_frame.setmicrohelp("Fine!")
	setpointer(Arrow!)
	g_mb.error(ls_errore)
	return
end if


//Ricalcolo le spese di trasporto
luo_spese_trasporto = create uo_spese_trasporto

//controlli preliminari prima del ricalcolo
//ritorno:			-1		errore critico
//					1		ricalcolo non possibile. dare messaggio e non effettuare il ricalcolo
//					2		ricalcolo possibile previa conferma dell'operatore
li_risposta = luo_spese_trasporto.uof_check_per_ricalcolo(li_anno_documento, ll_num_documento, "FATVEN", ref ls_errore)

choose case li_risposta
	case -1			//errore critico
		rollback;
		destroy luo_spese_trasporto
		destroy luo_calcolo
		pcca.mdi_frame.setmicrohelp("Fine!")
		setpointer(Arrow!)
		g_mb.error(ls_errore)
		return
		
	case 1			//ricalcolo non possibile
		rollback;
		destroy luo_spese_trasporto
		destroy luo_calcolo
		pcca.mdi_frame.setmicrohelp("Fine!")
		setpointer(Arrow!)
		g_mb.warning(ls_errore)
		return
		
	case 2			//ricalcolo possibile previa conferma dell'operatore
		if not g_mb.confirm(ls_errore) then
			//annullato dall'operatore
			rollback;
			destroy luo_spese_trasporto
			destroy luo_calcolo
			pcca.mdi_frame.setmicrohelp("Fine!")
			setpointer(Arrow!)
			g_mb.show("Annullato dall'operatore!")
			return
		end if
		
	case else
		//prosegui con il ricalcolo
		
end choose



if luo_spese_trasporto.uof_calcola_spese(li_anno_documento, ll_num_documento, "FATVEN", ref ls_errore) < 0 then
	rollback;
	destroy luo_spese_trasporto
	destroy luo_calcolo
	pcca.mdi_frame.setmicrohelp("Fine!")
	setpointer(Arrow!)
	g_mb.error(ls_errore)
	return
end if

//commit necessario per evitare lock successivi (apertura finestra visualizzazione storico)
commit;


//di seguito ri-calcolo il documento per includere nel calcolo le spese di trasporto
if luo_calcolo.uof_calcola_documento(li_anno_documento, ll_num_documento,"fat_ven", ls_errore) <> 0 then
	rollback;
	destroy luo_calcolo
	destroy luo_spese_trasporto
	pcca.mdi_frame.setmicrohelp("Fine!")
	setpointer(Arrow!)
	g_mb.error(ls_errore)
	return
end if

//qui posso distruggere l'oggetto calcola documento
destroy luo_calcolo

//commit necessario per evitare lock successivi (apertura finestra visualizzazione storico)
commit;

luo_spese_trasporto.uof_log("Spese trasporto ricalcolate su fattura " + string(li_anno_documento) + "/" + string(ll_num_documento))


//valori di ritorno della funzione
//0		SPESE TRASPORTO CONFERMATE (o non abilitate dal parametro SSP), quindi non fare null'altro
//1		SPESE TRASPORTO ANNULLATE, quindi chiama la funzione che toglie le righe trasporto e che ricalcola il documento
//2		SPESE TRASPORTO DA SOLE PERCENTUALI IMPONIBILE RIGHE ORDINE. quindi chiama funzione opportuna
li_risposta = luo_spese_trasporto.uof_apri_storico(li_anno_documento, ll_num_documento, "FATVEN")
		
if li_risposta = 0 then
else
	if li_risposta = 1 then
		//--------------------------------------------------------------------------------------------------------------------------------------------
		//ANNULLAMENTO SPESE TRASPORTO
		li_risposta = luo_spese_trasporto.uof_azzera_trasporto_documento(li_anno_documento, ll_num_documento, "FATVEN", ls_errore)
		//--------------------------------------------------------------------------------------------------------------------------------------------
	elseif li_risposta = 2 then
		//--------------------------------------------------------------------------------------------------------------------------------------------
		//SPESE TRASPORTO DA SOLE PERCENTUALI IMPONIBILE RIGHE ORDINE
		li_risposta = luo_spese_trasporto.uof_imposta_trasporto_solo_percentuale(li_anno_documento, ll_num_documento, "FATVEN", ls_errore)
		//--------------------------------------------------------------------------------------------------------------------------------------------
	end if
	
	if li_risposta<0 then
		rollback;
		destroy luo_spese_trasporto
		pcca.mdi_frame.setmicrohelp("Fine!")
		setpointer(Arrow!)
		g_mb.error(ls_errore)
		return
	end if
end if

destroy luo_spese_trasporto
pcca.mdi_frame.setmicrohelp("Fine!")

//se arrivi fin qui fai commit
commit;

setpointer(Arrow!)

g_mb.success("Operazione effettuata con successo!")

tab_dettaglio.det_1.dw_tes_fat_ven_det_1.change_dw_current()
tab_dettaglio.det_1.dw_tes_fat_ven_det_1.postevent("pcd_retrieve")




end event

event pc_menu_conferma();string		ls_tes_tabella, ls_det_tabella, ls_prog_riga, ls_quantita, ls_messaggio, ls_flag_movimenti, ls_flag_calcolo

long ll_anno_registrazione, ll_num_registrazione, ll_anno_documento, ll_num_documento, ll_ret


ll_anno_registrazione = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"anno_registrazione")
ll_num_registrazione = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"num_registrazione")

ll_anno_documento = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"anno_documento")
ll_num_documento = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"num_documento")
ls_flag_movimenti = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"flag_movimenti")
ls_flag_calcolo = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"flag_calcolo")

if isnull(ll_anno_documento) then
	g_mb.warning("APICE","Fattura senza numero fiscale associato!")
	return
end if

if ls_flag_movimenti = "S"  then
	g_mb.warning("APICE","Fattura già confermata!")
	return
end if

if ls_flag_calcolo = "N" then
	g_mb.warning("APICE","Prima della conferma è necessario calcolare la fattura!")
	return
end if

	
ls_tes_tabella = "tes_fat_ven"
ls_det_tabella = "det_fat_ven"
ls_prog_riga = "prog_riga_fat_ven"
ls_quantita = "quan_fatturata"

ll_ret = f_conferma_mov_mag(ls_tes_tabella, &
							 ls_det_tabella, &
							 ls_prog_riga, &
							 ls_quantita, &
							 ll_anno_registrazione, &
							 ll_num_registrazione, &
							 ls_messaggio) 
if ll_ret = -1 then
	rollback;
	g_mb.error("La conferma della fattura " + string(ll_anno_documento) + "/" + string(ll_num_documento) + " ha provocato un errore.~r~nDettaglio errore: " + ls_messaggio +  "~r~n")
elseif ll_ret = 0 then
	commit;
end if

commit;
g_mb.messagebox("APICE","Fattura Confermata.", Information!)

tab_dettaglio.det_1.dw_tes_fat_ven_det_1.change_dw_current()
tab_dettaglio.det_1.dw_tes_fat_ven_det_1.postevent("pcd_retrieve")

end event

event pc_menu_blocca();string ls_cod_tipo_det_ven, ls_cod_prodotto, ls_sql_stringa, ls_flag_tipo_det_ven, &
		 ls_cod_deposito ,ls_cod_ubicazione ,ls_cod_lotto, ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven

long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_prog_riga_fat_ven,  ll_progr_stock, ll_rows

dec{4} ld_quan_consegnata

datetime ldt_data_stock

datastore lds_data



if tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow() > 0 then
	if not isnull(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "cod_documento")) then
		g_mb.messagebox("Attenzione", "Fattura non chiudubile. Fattura già stampata in maniera definitiva.",  exclamation!, ok!)
		return
	end if

	if tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "flag_movimenti") = "S" then
		g_mb.messagebox("Attenzione", "Fattura non bloccabile. Fattura già confermata.",  exclamation!, ok!)
		return
	end if

	if tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "flag_blocco") = "N" then
		ll_anno_registrazione = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "anno_registrazione")
		ll_num_registrazione = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "num_registrazione")
		ls_cod_tipo_fat_ven = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "cod_tipo_fat_ven")

		select 	tab_tipi_fat_ven.flag_tipo_fat_ven
		into   		:ls_flag_tipo_fat_ven
		from   	tab_tipi_fat_ven
		where  	cod_azienda      = :s_cs_xx.cod_azienda and
				 	cod_tipo_fat_ven = :ls_cod_tipo_fat_ven    ;
		if sqlca.sqlcode <> 0 then return

		if ls_flag_tipo_fat_ven = "I" then
			
			ls_sql_stringa = "select cod_tipo_det_ven, cod_prodotto, quan_consegnata, cod_deposito, cod_ubicazione, cod_lotto, data_stock, progr_stock from det_fat_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(ll_anno_registrazione) + " and num_registrazione = " + string(ll_num_registrazione)
			
			ll_rows = guo_functions.uof_crea_datastore(lds_data, ls_sql_stringa)
			
			for ll_i = 1 to ll_rows
				ls_cod_tipo_det_ven = lds_data.getitemstring(ll_i, 1)
				ls_cod_prodotto = lds_data.getitemstring(ll_i, 2)
				ld_quan_consegnata = lds_data.getitemnumber(ll_i, 3)
				ls_cod_deposito = lds_data.getitemstring(ll_i, 4)
				ls_cod_ubicazione = lds_data.getitemstring(ll_i, 1)
				ls_cod_lotto = lds_data.getitemstring(ll_i, 6)
				ldt_data_stock = lds_data.getitemdatetime(ll_i, 7)
				ll_progr_stock = lds_data.getitemnumber(ll_i, 8)
	
				select flag_tipo_det_ven
				into   :ls_flag_tipo_det_ven
				from   tab_tipi_det_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and 
						 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
	
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettagli.", exclamation!, ok!)
					rollback;
					return
				end if
	
				if ls_flag_tipo_det_ven = "M" then
					update anag_prodotti  
						set quan_in_spedizione = quan_in_spedizione - :ld_quan_consegnata
					 where cod_azienda = :s_cs_xx.cod_azienda and  
							 cod_prodotto = :ls_cod_prodotto;
			
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.",  exclamation!, ok!)
						rollback;
						return
					end if
				end if				
	
				update stock
					set quan_in_spedizione = quan_in_spedizione - :ld_quan_consegnata
				 where cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_prodotto = :ls_cod_prodotto and
						 cod_deposito = :ls_cod_deposito and
						 cod_ubicazione = :ls_cod_ubicazione and
						 cod_lotto = :ls_cod_lotto and
						 data_stock = :ldt_data_stock and
						 prog_stock = :ll_progr_stock;
		
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento stock.",  exclamation!, ok!)
					rollback;
					return
				end if
	
			next

		end if
	
		update tes_fat_ven
		set    flag_blocco = 'S'
		where  cod_azienda = :s_cs_xx.cod_azienda and  
				 anno_registrazione = :ll_anno_registrazione and  
				 num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata fatture.",   exclamation!, ok!)
			rollback;
			return
		end if
		
		commit;

		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "flag_blocco", "S")
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitemstatus(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "flag_blocco", primary!, notmodified!)
		
	end if
end if

tab_dettaglio.det_1.dw_tes_fat_ven_det_1.change_dw_current()
tab_dettaglio.det_1.dw_tes_fat_ven_det_1.postevent("pcd_retrieve")
end event

event pc_menu_sblocca();long 			ll_i, ll_anno_registrazione, ll_num_registrazione, ll_progr_stock, ll_prog_riga_fat_ven, ll_rows

string 		ls_cod_tipo_det_ven, ls_cod_prodotto, ls_sql_stringa, ls_flag_tipo_det_ven, ls_cod_deposito, ls_cod_ubicazione, ls_cod_lotto, ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven

dec{4}		ld_quan_consegnata, ld_quan_disponibile

datetime 	ldt_data_stock

datastore 	lds_data


if tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow() > 0 then
	if tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "flag_blocco") = "S" then
		ll_anno_registrazione = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "anno_registrazione")
		ll_num_registrazione = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "num_registrazione")
		ls_cod_tipo_fat_ven = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "cod_tipo_fat_ven")

		select flag_tipo_fat_ven
		into   :ls_flag_tipo_fat_ven
		from   tab_tipi_fat_ven
		where  cod_azienda      = :s_cs_xx.cod_azienda  and
				 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven    ;
		if sqlca.sqlcode <> 0 then return

		if ls_flag_tipo_fat_ven = "I" then
			ls_sql_stringa = "select cod_tipo_det_ven, cod_prodotto, quan_consegnata, cod_deposito, cod_ubicazione, cod_lotto, data_stock, progr_stock, prog_riga_fat_ven from det_fat_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(ll_anno_registrazione) + " and num_registrazione = " + string(ll_num_registrazione)
			ll_rows = guo_functions.uof_crea_datastore(lds_data, ls_sql_stringa)
			
			for ll_i = 1 to ll_rows
				ls_cod_tipo_det_ven = lds_data.getitemstring(ll_i, 1)
				ls_cod_prodotto = lds_data.getitemstring(ll_i, 2)
				ld_quan_consegnata = lds_data.getitemnumber(ll_i, 3)
				ls_cod_deposito = lds_data.getitemstring(ll_i, 4)
				ls_cod_ubicazione = lds_data.getitemstring(ll_i, 5)
				ls_cod_lotto = lds_data.getitemstring(ll_i, 6)
				ldt_data_stock = lds_data.getitemdatetime(ll_i, 7)
				ll_progr_stock = lds_data.getitemnumber(ll_i, 8)
				ll_prog_riga_fat_ven = lds_data.getitemnumber(ll_i, 9)
		
				select flag_tipo_det_ven
				into   :ls_flag_tipo_det_ven
				from   tab_tipi_det_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and 
						 cod_tipo_det_ven = :ls_cod_tipo_det_ven;
		
				if sqlca.sqlcode = -1 then
					g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettagli.",   exclamation!, ok!)
					rollback;
					return
				end if
		
				if ls_flag_tipo_det_ven = "M" then
					select stock.giacenza_stock - (stock.quan_assegnata + stock.quan_in_spedizione)  
					into  :ld_quan_disponibile
					from  stock
					where cod_azienda = :s_cs_xx.cod_azienda and
							cod_prodotto = :ls_cod_prodotto and
							cod_deposito = :ls_cod_deposito and
							cod_ubicazione = :ls_cod_ubicazione and
							cod_lotto = :ls_cod_lotto and
							data_stock = :ldt_data_stock and
							prog_stock = :ll_progr_stock;
		
					if ld_quan_consegnata > ld_quan_disponibile then
						g_mb.messagebox("Attenzione", g_str.format("Nella Riga $1 la quantità bolla è maggiore della quantità disponibile $2.", ll_prog_riga_fat_ven, ld_quan_disponibile), exclamation!, ok!)
						rollback;
						return
					end if
		
					update 	anag_prodotti  
						set 	quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
						where cod_azienda = :s_cs_xx.cod_azienda and  
							 	cod_prodotto = :ls_cod_prodotto;
			
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.",   exclamation!, ok!)
						rollback;
						return
					end if
				
					update stock
						set quan_in_spedizione = quan_in_spedizione + :ld_quan_consegnata
					 where cod_azienda = :s_cs_xx.cod_azienda and  
							 cod_prodotto = :ls_cod_prodotto and
							 cod_deposito = :ls_cod_deposito and
							 cod_ubicazione = :ls_cod_ubicazione and
							 cod_lotto = :ls_cod_lotto and
							 data_stock = :ldt_data_stock and
							 prog_stock = :ll_progr_stock;
			
					if sqlca.sqlcode = -1 then
						g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento stock.",   exclamation!, ok!)
						rollback;
						return
					end if
				end if				
			next
		end if
	
		update tes_fat_ven
		set flag_blocco = 'N'
		where tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
				tes_fat_ven.anno_registrazione = :ll_anno_registrazione and  
				tes_fat_ven.num_registrazione = :ll_num_registrazione;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento testata boline.",  exclamation!, ok!)
			rollback;
			return
		end if
		
		commit;
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "flag_blocco", "N")
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitemstatus(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "flag_blocco", primary!, notmodified!)
	end if
end if

tab_dettaglio.det_1.dw_tes_fat_ven_det_1.change_dw_current()
tab_dettaglio.det_1.dw_tes_fat_ven_det_1.postevent("pcd_retrieve")
end event

event pc_menu_corrispondenze();s_cs_xx.parametri.parametro_s_1 = "Fat_Ven"
window_open_parm(w_acq_ven_corr, -1, tab_dettaglio.det_1.dw_tes_fat_ven_det_1)

end event

event pc_menu_note();s_cs_xx.parametri.parametro_s_1 = "Fat_Ven"
window_open_parm(w_acq_ven_note, -1, tab_dettaglio.det_1.dw_tes_fat_ven_det_1)

end event

event pc_menu_import_excel();s_cs_xx.parametri.parametro_d_1 = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"anno_registrazione")
s_cs_xx.parametri.parametro_d_2 = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"num_registrazione")

open(w_import_righe_doc_excel)
end event

event pc_menu_iva();window_open_parm(w_iva_fat_ven, -1, tab_dettaglio.det_1.dw_tes_fat_ven_det_1)

end event

event pc_menu_contropartite();window_open_parm(w_cont_fat_ven, -1, tab_dettaglio.det_1.dw_tes_fat_ven_det_1)

end event

event pc_menu_scadenze();window_open_parm(w_scad_fat_ven, -1, tab_dettaglio.det_1.dw_tes_fat_ven_det_1)
end event

event pc_menu_xml_fatel();string	ls_path_file, ls_filename, ls_paese, ls_partita_iva, ls_filelocation, ls_final_path, ls_blocco, ls_sigla_anno
long ll_anno_registrazione, ll_num_registrazione, ll_ret, ll_mancanti
uo_fatel luo_fatel

luo_fatel = CREATE uo_fatel

ll_anno_registrazione = long(istr_data.decimale[1])
ll_num_registrazione = long(istr_data.decimale[2])

// Controlli prima di inviare
s_cs_xx.parametri.parametro_d_1 = ll_anno_registrazione
s_cs_xx.parametri.parametro_d_2 = ll_num_registrazione

select flag
into   :ls_blocco
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'BSM';
		 
if sqlca.sqlcode <> 0 then
	ls_blocco = "N"
end if

if ls_blocco = "S" then

	select count(*)
	into   :ll_mancanti
	from   det_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
			 num_registrazione = :s_cs_xx.parametri.parametro_d_2 and
			 prod_mancante = 'S';
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in controllo prodotti mancanti su det_fat_ven: " + sqlca.sqlerrtext)
	elseif not isnull(ll_mancanti) and ll_mancanti > 0 then
		g_mb.messagebox("APICE","Sono presenti prodotti mancanti nei dettagli della fattura. Stampa interrotta.")
		return
	end if
	
end if

s_cs_xx.parametri.parametro_i_1 = 0

if isnull(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "cod_documento")) then
	window_open(w_tipo_stampa_fat_ven, 0)
	if s_cs_xx.parametri.parametro_i_1 = -1 then
		rollback;
		return
	end if
	triggerevent("pc_retrieve")
end if

triggerevent("pc_menu_calcola")

// Fine controlli

ll_ret = luo_fatel.uof_fattura_xml( ll_anno_registrazione, ll_num_registrazione, ref ls_path_file)
if ll_ret < 0 then
	g_mb.error(ls_path_file)
else
	
	select fatel_idpaese_trasmittente, 
			partita_iva
	into	:ls_paese,
			:ls_partita_iva
	from  aziende
	where cod_azienda = :s_cs_xx.cod_azienda;
	
//	ls_filename = ls_paese + ls_partita_iva + "_" + string(ll_num_registrazione,"00000")
	luo_fatel.uof_get_sigla_anno(ll_anno_registrazione, ls_sigla_anno)
	ls_filename = ls_paese + ls_partita_iva + "_" + ls_sigla_anno + string(ll_num_registrazione,"0000")
	ls_filelocation = guo_functions.uof_get_user_documents_folder( )
	ls_final_path = ls_filelocation + ls_filename
	
	if GetFileSaveName ( "Select File",ls_final_path , ls_filename, "XML", "XML (*.xml),*.*" , ls_filelocation, 32770) = 1 then
		if FileExists(ls_final_path) then
			if g_mb.confirm( "File Xml Fattura Elettronica", "Il file selezionato esiste già, lo sovrascrivo?", 1) then 
				filedelete(ls_final_path)
			else
				return
			end if
		end if
		
		choose case FileMove ( ls_path_file, ls_final_path )
			case -1
				g_mb.error(g_str.format("Attenzione: impossibile accedere al file",ls_path_file))
			case -2
				g_mb.error(g_str.format("Attenzione: impossibile scrivere il file di destinazione prescelto",ls_final_path))
			case 1
				g_mb.show( "File Generato con successo" )
		end choose
	end if
	
end if
destroy luo_fatel
end event

event pc_menu_invio_fatel();string	ls_path_file, ls_filename, ls_paese, ls_partita_iva, ls_filelocation, ls_message, ls_errore, ls_flag_esito_fatel, ls_identificativo_sdi, ls_flag_pa, &
		ls_cod_cliente, ls_blocco, ls_id_sdi, ls_flag_tipo_cliente
long ll_anno_registrazione, ll_num_registrazione, ll_ret, ll_mancanti
uo_fatel luo_fatel

luo_fatel = CREATE uo_fatel

ll_anno_registrazione = long(istr_data.decimale[1])
ll_num_registrazione = long(istr_data.decimale[2])

select 	T.cod_cliente, 
			T.flag_esito_fatel,
			C.fatel_flag_pa,
			T.identificativo_sdi,
			C.flag_tipo_cliente
into		:ls_cod_cliente,
			:ls_flag_esito_fatel,
			:ls_flag_pa,
			:ls_id_sdi,
			:ls_flag_tipo_cliente
from		tes_fat_ven T
left  join anag_clienti C on T.cod_azienda = C.cod_azienda and T.cod_cliente=C.cod_cliente
where  	T.cod_azienda = :s_cs_xx.cod_azienda and
			T.anno_registrazione = :ll_anno_registrazione and
			T.num_registrazione = :ll_num_registrazione;
if sqlca.sqlcode <> 0 then return

if ls_flag_tipo_cliente <> "F" and ls_flag_tipo_cliente <> "S" and ls_flag_tipo_cliente <> "C" then
	g_mb.warning("La fattura elettronica può essere trasmessa solo a soggetti che sono Persone Fisiche / Società / UE.")
	return
end if
	
if not isnull(ls_id_sdi)  and len(ls_id_sdi) > 0 then
	if not g_mb.confirm(g_str.format("E' già stata eseguita una trasmissione con codice SDI=$1; procedo lo stesso?.",ls_id_sdi),2)  then return
end if
	
		
if ls_flag_esito_fatel = "S" then
	g_mb.warning("La fattura è già stata inviata con successo al SDI. Un ulteriore invio causerebbe un errore di trasmissione.")
	return
end if

if isnull(ls_flag_pa) then
	g_mb.error("Attenzione: nell'anagrafica cliente manca l'indicazione se PRIVATO oppure PA. Modificare l'anagrafica e poi riprovare l'invio.")
	return
end if

if ls_flag_pa = "S" then
	// Se cliente PA chiedo di caricare il file firmato
	if GetFileOpenName ("File Firmato Digitalmente", ls_path_file, ls_filename, "p7m", "P7m Files (*.p7m),*.p7m, Tutti (*.*), *.* ", "%userprofile%") <> 1 then return 
end if

// Controlli prima di inviare
s_cs_xx.parametri.parametro_d_1 = ll_anno_registrazione
s_cs_xx.parametri.parametro_d_2 = ll_num_registrazione

select flag
into   :ls_blocco
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'BSM';
		 
if sqlca.sqlcode <> 0 then
	ls_blocco = "N"
end if

if ls_blocco = "S" then

	select count(*)
	into   :ll_mancanti
	from   det_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :s_cs_xx.parametri.parametro_d_1 and
			 num_registrazione = :s_cs_xx.parametri.parametro_d_2 and
			 prod_mancante = 'S';
			 
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE","Errore in controllo prodotti mancanti su det_fat_ven: " + sqlca.sqlerrtext)
	elseif not isnull(ll_mancanti) and ll_mancanti > 0 then
		g_mb.messagebox("APICE","Sono presenti prodotti mancanti nei dettagli della fattura. Stampa interrotta.")
		return
	end if
	
end if

s_cs_xx.parametri.parametro_i_1 = 0

if isnull(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "cod_documento")) then
	window_open(w_tipo_stampa_fat_ven, 0)
	if s_cs_xx.parametri.parametro_i_1 = -1 then
		rollback;
		return
	end if
	triggerevent("pc_retrieve")
end if

triggerevent("pc_menu_calcola")

// Fine controlli
if not g_mb.confirm( "Invio Fattura Elettronica", "Procedo con l'invio della fattura?", 2) then return

ll_ret = luo_fatel.uof_invia_fattura( ll_anno_registrazione, ll_num_registrazione, ls_flag_pa, ls_path_file, ls_filename, ref ls_identificativo_sdi, ref ls_message, ref ls_errore)

choose case ll_ret
	case -1
		// errore durante invio
		g_mb.error(ls_message)
	case -2
		// errore durante archiviazione fattura
		g_mb.error( ls_errore )
	case 0
		g_mb.show( ls_message )
end choose
destroy luo_fatel


end event

event ue_leggi_notifiche();if tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_scarica_notifiche") = "S" then
	tab_ricerca.tabpage_1.dw_notifiche.event ue_leggi_lista_notifiche()
end if
end event

event pc_menu_duplicasunotacredito();window_open(w_tes_fat_ven_duplica_nc, 0)
if len(s_cs_xx.parametri.parametro_s_15) > 0 then
	wf_duplica_documento("S", s_cs_xx.parametri.parametro_s_15)
	s_cs_xx.parametri.parametro_s_15 = ""
end if


end event

public subroutine wf_treeview_icons ();ICONA_CLIENTE = wf_treeview_add_icon("11.5\tree_user.png")
ICONA_ANNO = wf_treeview_add_icon("11.5\tree_man.png")
ICONA_TIPO_FATTURA = wf_treeview_add_icon("11.5\tree_categorie.png")
ICONA_FATTURA = wf_treeview_add_icon("11.5\tree_editor_doc_child.png")
ICONA_FATTURA_ELIMINATA = wf_treeview_add_icon("11.5\man_reg_eliminata.png")
ICONA_FATTURA_SELEZIONATA = wf_treeview_add_icon("11.5\man_reg_modificata.png")
ICONA_PAGAMENTO = wf_treeview_add_icon("treeview\offerta.png")
ICONA_CAUSALE =  wf_treeview_add_icon("treeview\reparto.png")
ICONA_PRODOTTO = wf_treeview_add_icon("treeview\prodotto.png")
ICONA_NEW = wf_treeview_add_icon("treeview\documento_new.png")
ICONA_FORNITORE = wf_treeview_add_icon("treeview\operatore.png")
ICONA_CONTATTO = wf_treeview_add_icon("treeview\operatore.png")
ICONA_FATTURA_CONFERMATA = wf_treeview_add_icon("12.5\Doc_Confermato.png")
ICONA_FATTURA_CONTABILIZZATA = wf_treeview_add_icon("12.5\Doc_contabilizzato.png")
ICONA_FATTURA_ESITO_SCARTATA = wf_treeview_add_icon("12.5\Fatel_scartata.png")
end subroutine

public subroutine wf_imposta_ricerca ();string 		ls_flag_confermate, ls_flag_contabilizzate, ls_flag_cc_manuali, ls_flag_blocco, ls_cod_documento, ls_numeratore_documento, &
				ls_cod_cliente, ls_protocollo, ls_cod_tipo_fat_ven, ls_flag_scadenze_manuali, ls_cod_pagamento

long			ll_anno_registrazione, ll_num_registrazione, ll_anno_documento, ll_num_documento

datetime	ldt_data_registrazione, ldt_data_registrazione_a, ldt_data_fattura, ldt_data_fattura_al

string		ls_cod_tipo_listino, ls_cod_valuta, ls_cod_des_cliente, ls_cod_deposito, &
				ls_cod_agente_1, ls_cod_agente_2, ls_cod_causale, ls_cod_prodotto, ls_tes, ls_det, ls_cod_contatto, ls_flag_esito_fatel

tab_ricerca.ricerca.dw_ricerca.accepttext()

ls_tes = "tes_fat_ven."
ls_det = "det_fat_ven."

ls_flag_confermate = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_movimenti")
ls_flag_contabilizzate = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_contabilita")
ls_flag_cc_manuali = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_cc_manuali")

ls_flag_blocco = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_blocco")
ls_flag_scadenze_manuali = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "flag_scadenze_manuali")

// -------------  compongo SQL con filtri ---------------------

is_sql_filtro = ""
is_sql_prodotto = ""

ll_anno_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1,"anno_registrazione")
ll_num_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1,"num_registrazione")
ls_cod_documento = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_documento")
ll_anno_documento = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1,"anno_documento")
ls_numeratore_documento = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"numeratore_documento")
ll_num_documento = tab_ricerca.ricerca.dw_ricerca.getitemnumber(1,"num_documento")

ls_cod_cliente = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_cliente")
ls_cod_contatto = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_contatto")
ls_cod_tipo_fat_ven = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_tipo_fat_ven")
ls_cod_pagamento = tab_ricerca.ricerca.dw_ricerca.getitemstring(1, "cod_tipo_pagamento")
ldt_data_registrazione = tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1, "data_registrazione")
ldt_data_registrazione_a = tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1, "data_registrazione_a")
ldt_data_fattura = tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1, "data_fattura")
ldt_data_fattura_al = tab_ricerca.ricerca.dw_ricerca.getitemdatetime(1, "data_fattura_al")
				
ls_cod_tipo_listino = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_tipo_listino")
ls_cod_valuta = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_valuta")
ls_cod_des_cliente = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_des_cliente")
ls_cod_deposito = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_deposito")
ls_cod_agente_1 = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_agente_1")
ls_cod_agente_2 = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_agente_2")
ls_cod_causale = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_causale")
ls_cod_prodotto = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto")
ls_flag_esito_fatel = tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"flag_esito_fatel")

if not isnull(ll_anno_registrazione) and ll_anno_registrazione>0 then
	is_sql_filtro += " and "+ls_tes+"anno_registrazione = " + string(ll_anno_registrazione) + " "
end if
if not isnull(ll_num_registrazione) and ll_num_registrazione>0 then
	is_sql_filtro += " and "+ls_tes+"num_registrazione = " + string(ll_num_registrazione) + " "
end if
if not isnull(ls_cod_documento) and ls_cod_documento<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_documento = '" + ls_cod_documento + "' "
end if
if not isnull(ll_anno_documento) and ll_anno_documento>0 then
	is_sql_filtro += " and "+ls_tes+"anno_documento = " + string(ll_anno_documento) + " "
end if
if not isnull(ls_numeratore_documento) and ls_numeratore_documento<>"" then
	is_sql_filtro += " and "+ls_tes+"numeratore_documento = '" + ls_numeratore_documento + "' "
end if
if not isnull(ll_num_documento) and ll_num_documento>0 then
	is_sql_filtro += " and "+ls_tes+"num_documento = " + string(ll_num_documento) + " "
end if

if not isnull(ls_cod_cliente) and ls_cod_cliente<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_cliente = '" + ls_cod_cliente + "' "
end if

if not isnull(ls_cod_contatto) and ls_cod_contatto<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_contatto = '" + ls_cod_contatto + "' "
end if

if not isnull(ls_cod_tipo_fat_ven) and ls_cod_tipo_fat_ven<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_tipo_fat_ven = '" + ls_cod_tipo_fat_ven + "' "
end if

if not isnull(ls_cod_pagamento) and ls_cod_pagamento<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_pagamento = '" + ls_cod_pagamento + "' "
end if

if not isnull(ldt_data_registrazione) then
	is_sql_filtro += " and "+ls_tes+"data_registrazione >= '" + string(ldt_data_registrazione, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_data_registrazione_a) then
	is_sql_filtro += " and "+ls_tes+"data_registrazione <= '" + string(ldt_data_registrazione_a, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if not isnull(ldt_data_fattura) then
	is_sql_filtro += " and "+ls_tes+"data_fattura >= '" + string(ldt_data_fattura, s_cs_xx.db_funzioni.formato_data) + "' "
end if
if not isnull(ldt_data_fattura_al) then
	is_sql_filtro += " and "+ls_tes+"data_fattura <= '" + string(ldt_data_fattura_al, s_cs_xx.db_funzioni.formato_data) + "' "
end if

if ls_flag_confermate="S" or ls_flag_confermate='N' then
	is_sql_filtro += " and "+ls_tes+"flag_movimenti = '" + ls_flag_confermate + "' "
end if
if ls_flag_contabilizzate="S" or ls_flag_contabilizzate='N' then
	is_sql_filtro += " and "+ls_tes+"flag_contabilita = '" + ls_flag_contabilizzate + "' "
end if
if ls_flag_cc_manuali="S" or ls_flag_cc_manuali='N' then
	is_sql_filtro += " and "+ls_tes+"flag_cc_manuali = '" + ls_flag_cc_manuali + "' "
end if
if ls_flag_scadenze_manuali="S" or ls_flag_scadenze_manuali='N' then
	is_sql_filtro += " and "+ls_tes+"flag_scad_manuali = '" + ls_flag_scadenze_manuali + "' "
end if
if ls_flag_blocco="S" or ls_flag_blocco='N' then
	is_sql_filtro += " and "+ls_tes+"flag_blocco = '" + ls_flag_blocco + "' "
end if
if ls_flag_esito_fatel <> "X" and len(ls_flag_esito_fatel) > 0 then
	choose case ls_flag_esito_fatel
		case "N"  // scartate
			is_sql_filtro += " and "+ls_tes+"flag_esito_fatel = 'NS' " 
		case "S"
			is_sql_filtro += " and "+ls_tes+"flag_esito_fatel not in ('NS','NI')" 
		case "S"
			is_sql_filtro += " and "+ls_tes+"flag_esito_fatel not in ('NS','NI')" 
		case "I"
			is_sql_filtro += " and "+ls_tes+" (flag_esito_fatel = 'NI' or flag_esito_fatel is null) " 
	end choose			
end if

if not isnull(ls_cod_tipo_listino) and ls_cod_tipo_listino<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_tipo_listino_prodotto = '" + ls_cod_tipo_listino + "' "
end if
if not isnull(ls_cod_valuta) and ls_cod_valuta<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_valuta = '" + ls_cod_valuta + "' "
end if
if not isnull(ls_cod_des_cliente) and ls_cod_des_cliente<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_des_cliente = '" + ls_cod_des_cliente + "' "
end if
if not isnull(ls_cod_deposito) and ls_cod_deposito<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_deposito = '" + ls_cod_deposito + "' "
end if
if not isnull(ls_cod_agente_1) and ls_cod_agente_1<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_agente_1 = '" + ls_cod_agente_1 + "' "
end if
if not isnull(ls_cod_agente_2) and ls_cod_agente_2<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_agente_2 = '" + ls_cod_agente_2 + "' "
end if
if not isnull(ls_cod_causale) and ls_cod_causale<>"" then
	is_sql_filtro += " and "+ls_tes+"cod_causale = '" + ls_cod_causale + "' "
end if
if not isnull(ls_cod_prodotto) and ls_cod_prodotto<>"" then
	is_sql_filtro += " and "+ls_det+"cod_prodotto = '" + ls_cod_prodotto + "' "
end if
	

if not isnull(tab_ricerca.ricerca.dw_ricerca.getitemstring(1,"cod_prodotto")) OR wf_has_livello("X") then
	is_sql_prodotto = g_str.format(" JOIN $1 on $1.cod_azienda=$2.cod_azienda and "+&
							"$1.anno_registrazione=$2.anno_registrazione and "+&
							"$1.num_registrazione=$2.num_registrazione ", "det_fat_ven", "tes_fat_ven")
end if


end subroutine

public function boolean wf_cmn ();string ls_flag_negativo

select flag
into   :ls_flag_negativo
from	 parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'CMN' and
		 flag_parametro = 'F';

if sqlca.sqlcode < 0 then
	g_mb.messagebox("APICE","Errore in lettura parametro CMN da parametri_azienda (L'errore NON è bloccante!): " + sqlca.sqlerrtext,stopsign!)
	return false
elseif  ls_flag_negativo = 'S' then
	return true
else
	return false
end if
end function

public function boolean wf_controlla_duplicazione (integer ai_anno_doc, long al_num_doc);
//IN TESTATA
//se il tipo pagamento è diverso rispetto a quella impostata in anagrafica cliente.
//se la banca nostra è diversa rispetto a quella impostata in anagrafica cliente.
//se la banca cliente è diversa rispetto a quella impostata in anagrafica cliente.

//SUI DETTAGLI
//se il codice iva su almeno una riga è diverso da quello del relativo tipo dettaglio in tabella tipi dettaglio vendita.

//in questi casi dare un warning prima di procedere


string				ls_cod_pagamento, ls_cod_banca_clien_for, ls_cod_banca, ls_cod_cliente, ls_cod_contatto, &
					ls_cod_pagamento_ana, ls_cod_banca_clien_for_ana, ls_cod_banca_ana, ls_msg
integer			li_count

select count(*)
into :li_count
from tes_fat_ven
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ai_anno_doc and
			num_registrazione = :al_num_doc;

if isnull(li_count) then li_count = 0

if sqlca.sqlcode = 0 and li_count = 0 then
	//documento non trovato
	g_mb.warning("Attenzione: documento NON trovato, impossibile duplicare!")
	return false
end if

//leggo info di testata da controllare
select		cod_cliente, cod_contatto, cod_pagamento, cod_banca_clien_for, cod_banca
into		:ls_cod_cliente, :ls_cod_contatto :ls_cod_pagamento, :ls_cod_banca_clien_for, :ls_cod_banca
from tes_fat_ven
where 	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ai_anno_doc and
			num_registrazione = :al_num_doc;

if isnull(ls_cod_pagamento) or ls_cod_pagamento="" then ls_cod_pagamento="<<vuoto>>"
if isnull(ls_cod_banca_clien_for) or ls_cod_banca_clien_for="" then ls_cod_banca_clien_for="<<vuoto>>"
if isnull(ls_cod_banca) or ls_cod_banca="" then ls_cod_banca="<<vuoto>>"

if not isnull(ls_cod_cliente) and ls_cod_cliente<> "" then
	
	//leggo info da anagrafica per incrociare
	select		cod_pagamento, cod_banca_clien_for, cod_banca
	into		:ls_cod_pagamento_ana, :ls_cod_banca_clien_for_ana, :ls_cod_banca_ana
	from anag_clienti
	where 	cod_azienda = :s_cs_xx.cod_azienda and
			cod_cliente = :ls_cod_cliente;

	if isnull(ls_cod_pagamento_ana) or ls_cod_pagamento_ana="" then ls_cod_pagamento_ana="<<vuoto>>"
	if isnull(ls_cod_banca_clien_for_ana) or ls_cod_banca_clien_for_ana="" then ls_cod_banca_clien_for_ana="<<vuoto>>"
	if isnull(ls_cod_banca_ana) or ls_cod_banca_ana="" then ls_cod_banca_ana="<<vuoto>>"
				
	if ls_cod_pagamento<>ls_cod_pagamento_ana then
		ls_msg = "Attenzione: il tipo pagamento della fattura da duplicare ("+ls_cod_pagamento+") "+&
					"risulta diverso da quello impostato in anagrafica clienti ("+ls_cod_pagamento_ana+"). "+&
									"Vuoi continuare lo stesso?"
		if not g_mb.confirm(ls_msg) then
			return false
		end if
	end if
	
	if ls_cod_banca_clien_for<>ls_cod_banca_clien_for_ana then
		ls_msg = "Attenzione: la banca del cliente della fattura da duplicare ("+ls_cod_banca_clien_for+") "+&
					"risulta diversa da quella impostata in anagrafica clienti ("+ls_cod_banca_clien_for_ana+"). "+&
									"Vuoi continuare lo stesso?"
		if not g_mb.confirm(ls_msg) then
			return false
		end if
	end if
	
	if ls_cod_banca<>ls_cod_banca_ana then
		ls_msg = "Attenzione: la banca nostra (relativa al cliente) della fattura da duplicare ("+ls_cod_banca+") "+&
					"risulta diversa da quella impostata in anagrafica clienti ("+ls_cod_banca_ana+"). "+&
									"Vuoi continuare lo stesso?"
		if not g_mb.confirm(ls_msg) then
			return false
		end if
	end if
	
	
else
	//ometti controllo
	//return true
end if


//controllo iva sulle righe
li_count = wf_controlla_iva_righe(ai_anno_doc, al_num_doc)
if li_count > 0 then
	ls_msg = "Attenzione: ci sono n° "+string(li_count)+" righe del documento che hanno un codice IVA diverso da quello di riferimento per il rispettivo tipo dettaglio." +&
						  "Vuoi continuare lo stesso?"
	
	if not g_mb.confirm(ls_msg) then
		return false
	end if
end if


return true
end function

public function integer wf_controlla_iva_righe (integer ai_anno_doc, long al_num_doc);string				ls_sql, ls_err

datastore		lds_data

long				ll_index, ll_tot


//recupero le righe che hanno un cod.iva impostato diverso dal rispettivo associato al tipo dettaglio
ls_sql = "select count(*) "+&
			"from det_fat_ven "+&
			"join tab_tipi_det_ven on tab_tipi_det_ven.cod_azienda=det_fat_ven.cod_azienda and "+&
											"tab_tipi_det_ven.cod_tipo_det_ven=det_fat_ven.cod_tipo_det_ven "+&
			"where anno_registrazione="+string(ai_anno_doc) + " and num_registrazione="+string(al_num_doc)+ "and " +&
					"det_fat_ven.cod_iva <> tab_tipi_det_ven.cod_iva "

ll_tot = guo_functions.uof_crea_datastore(lds_data, ls_sql, ls_err)

if ll_tot>0 then
	ll_tot = lds_data.getitemnumber(1, 1)
end if

return ll_tot



end function

public subroutine wf_lista_ordini_riaperti (string as_ordine, ref string as_ordini[], ref string as_testo);long			ll_index


for ll_index=1 to upperbound(as_ordini[])
	if as_ordine = as_ordini[ll_index] then
		//ordine già in lista
		return
	end if
next

//se arrivi fin qui aggiungi
 as_ordini[ upperbound(as_ordini[]) + 1] = as_ordine
 
 if as_testo<>"" then as_testo += ", "
 as_testo += as_ordine
 
 return
end subroutine

public subroutine wf_reset_tipo_fattura ();string ls_modify

	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_cliente.visible=false
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_fornitore.visible=false


ls_modify = "st_cliente_fornitore.text=''~t"
ls_modify = ls_modify + "cod_contatto.visible='0'~t"
//ls_modify = ls_modify + "cf_cod_cliente.visible='0'~t"
ls_modify = ls_modify + "cod_cliente.visible='0'~t"
//ls_modify = ls_modify + "cf_cod_fornitore.visible='0'~t"
ls_modify = ls_modify + "cod_fornitore.visible='0'~t"
ls_modify = ls_modify + "cod_des_cliente_t.visible='0'~t"
ls_modify = ls_modify + "cf_cod_des_cliente.visible='0'~t"
ls_modify = ls_modify + "cod_des_cliente.visible='0'~t"
ls_modify = ls_modify + "cf_cod_des_fornitore.visible='0'~t"
ls_modify = ls_modify + "cod_des_fornitore.visible='0'~t"
ls_modify = ls_modify + "cod_fil_fornitore_t.visible='0'~t"
ls_modify = ls_modify + "cf_cod_fil_fornitore.visible='0'~t"
ls_modify = ls_modify + "cod_fil_fornitore.visible='0'~t"
tab_dettaglio.det_1.dw_tes_fat_ven_det_1.modify(ls_modify)
end subroutine

public function integer wf_riapri_ordini (integer fi_anno_fattura, long fl_num_fattura, ref string fs_errore);
//questa funzione eseegue l'eventuale riapertura riga ordine vendita alla cancellazione della fattura di vendita, dalla testata
//ma solo se si tratta di una fattura immediata (questo controllo è stato fatto prima di chiamare questa funzione)

string				ls_flag_evasione, ls_ordini_riaperti, ls_temp[], ls_riga_ordine
integer			li_anno_reg_ord_ven_cu
long				ll_prog_riga_fat_ven_cu, ll_num_reg_ord_ven_cu, ll_prog_riga_ord_ven_cu, ll_righe, ll_evase, ll_parziali
decimal			ld_quan_fatturata_cu, ld_quan_evasa


//leggi le righe fattura legate ad una riga ordine di vendita -------
declare cu_righe_fattura cursor for  
select prog_riga_fat_ven, anno_reg_ord_ven, num_reg_ord_ven, prog_riga_ord_ven, quan_fatturata
from det_fat_ven
where 	cod_azienda=:s_cs_xx.cod_azienda and
			anno_registrazione=:fi_anno_fattura and
			num_registrazione=:fl_num_fattura and 
			anno_reg_ord_ven>0
order by prog_riga_fat_ven;

open cu_righe_fattura;

if sqlca.sqlcode < 0 then
	fs_errore = "Errore in OPEN cursore cu_righe_fattura : " + sqlca.sqlerrtext
	return -1
end if

do while true
	fetch cu_righe_fattura into :ll_prog_riga_fat_ven_cu, :li_anno_reg_ord_ven_cu, :ll_num_reg_ord_ven_cu, :ll_prog_riga_ord_ven_cu,
										:ld_quan_fatturata_cu;

	if sqlca.sqlcode < 0 then
		close cu_righe_fattura;
		fs_errore = "Errore in FETCH cursore cu_righe_fattura : " + sqlca.sqlerrtext
		return -1
	end if
	
	if sqlca.sqlcode = 100 then exit
	//procedi
	
	if isnull(li_anno_reg_ord_ven_cu) or isnull(ll_num_reg_ord_ven_cu) or isnull(ll_prog_riga_ord_ven_cu) then
		continue
	end if
	
	ls_riga_ordine = string(li_anno_reg_ord_ven_cu)+"/"+string(ll_num_reg_ord_ven_cu)+"/"+string(ll_prog_riga_ord_ven_cu)
	
	
	//leggo quantita evasa dell'ordine
	select quan_evasa
	into   :ld_quan_evasa
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_reg_ord_ven_cu and
			 num_registrazione = :ll_num_reg_ord_ven_cu and
			 prog_riga_ord_ven = :ll_prog_riga_ord_ven_cu;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in lettura quantità evasa riga ordine di origine ("+ls_riga_ordine+"): " + sqlca.sqlerrtext
		close cu_righe_fattura;
		return -1
	end if
	
	
	//NOTA: in alcuni casi può capitare di evadere una riga ordine con una quantità superiore a quella ordinata (es. Eurocablaggi)
	//allora per evitare problemi di valori negativi in ripristino riga forzo la quan fatturata alla quantità evasa ...
	if ld_quan_fatturata_cu>ld_quan_evasa then ld_quan_fatturata_cu=ld_quan_evasa
	
	if ld_quan_evasa - ld_quan_fatturata_cu = 0 then
		//l'ordine va riaperto
		ls_flag_evasione = "A"
	else
		//l'ordine deve essre posto a parziale
		ls_flag_evasione = "P"
	end if
	
	update 	det_ord_ven
	set    		quan_evasa = quan_evasa - :ld_quan_fatturata_cu,
				flag_evasione = :ls_flag_evasione
	where  	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_reg_ord_ven_cu and
				num_registrazione = :ll_num_reg_ord_ven_cu and
				prog_riga_ord_ven = :ll_prog_riga_ord_ven_cu;
	
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in aggiornamento quantità evasa riga ordine di origine ("+ls_riga_ordine+"): " + sqlca.sqlerrtext
		close cu_righe_fattura;
		return -1
	end if
	
	select count(*)
	into   :ll_righe
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_reg_ord_ven_cu and
			 num_registrazione = :ll_num_reg_ord_ven_cu;
			 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in conteggio righe ordine di origine ("+ls_riga_ordine+"): " + sqlca.sqlerrtext
		close cu_righe_fattura;
		return -1
	end if
	
	select count(*)
	into   :ll_evase
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_reg_ord_ven_cu and
			 num_registrazione = :ll_num_reg_ord_ven_cu and
			 flag_evasione = 'E';
			 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in conteggio righe evase ordine di origine ("+ls_riga_ordine+"): " + sqlca.sqlerrtext
		close cu_righe_fattura;
		return 1
	end if
	
	select count(*)
	into   :ll_parziali
	from   det_ord_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :li_anno_reg_ord_ven_cu and
			 num_registrazione = :ll_num_reg_ord_ven_cu and
			 flag_evasione = 'P';
			 
	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in conteggio righe parziali ordine di origine ("+ls_riga_ordine+"): " + sqlca.sqlerrtext
		close cu_righe_fattura;
		return -1
	end if
	
	if ll_evase = ll_righe then
		ls_flag_evasione = "E"
	elseif ll_evase > 0 or ll_parziali > 0 then
		ls_flag_evasione = "P"
	else
		ls_flag_evasione = "A"	
	end if
	
	update 	tes_ord_ven
	set    		flag_evasione = :ls_flag_evasione
	where  	cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :li_anno_reg_ord_ven_cu and
				num_registrazione = :ll_num_reg_ord_ven_cu;

	if sqlca.sqlcode <> 0 then
		fs_errore = "Errore in aggiornamento stato evasione ordine di origine ("+ls_riga_ordine+"): " + sqlca.sqlerrtext
		close cu_righe_fattura;
		return -1
	end if
	
	wf_lista_ordini_riaperti(string(li_anno_reg_ord_ven_cu)+"/"+string(ll_num_reg_ord_ven_cu), ls_temp[], ls_ordini_riaperti)
	
	f_scrivi_log(	"Cancellazione riga fattura " + string(fi_anno_fattura) + "/" + string(fl_num_fattura) + "/" + string(ll_prog_riga_fat_ven_cu) + &
					" con riapertura ordine nr " +  string(li_anno_reg_ord_ven_cu) + "/" + string(ll_num_reg_ord_ven_cu) + "/" + string(ll_prog_riga_ord_ven_cu))
loop

close cu_righe_fattura;

//in as_errore metto la lista degli ordini riaperti
if ls_ordini_riaperti<>"" and not isnull(ls_ordini_riaperti) then fs_errore = ls_ordini_riaperti

return 0


end function

public function integer wf_tipo_fattura (string as_flag_tipo_fat_ven, boolean ab_rowfocuschange);string ls_modify, ls_null, ls_cod_contatto
long ll_riga, ll_riga_2, ll_null,ll_riga_3

setnull(ls_null)
setnull(ll_null)
ll_riga = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow()			// 2
ll_riga_2 = tab_dettaglio.det_2.dw_tes_fat_ven_det_2.getrow()			// 1
ll_riga_3 = tab_dettaglio.det_3.dw_tes_fat_ven_det_3.getrow()			// 2

//messagebox("Prova", "ll_riga " + string(ll_riga))
//messagebox("Prova", "ll_riga_2 " + string(ll_riga_2))
//messagebox("Prova", "ll_riga_3 " + string(ll_riga_3))

if as_flag_tipo_fat_ven <> 'F' then		// cliente o contatto


	ls_cod_contatto = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(ll_riga, "cod_contatto")
	
	if ls_cod_contatto<>"" and not isnull(ls_cod_contatto) then
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_cliente.visible=false
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_banca_for.visible=false
		
		ls_modify = "st_cliente_fornitore.text='Contatto:'~t"
		ls_modify += "cf_cod_contatto.visible='1'~t"
		ls_modify += "cod_contatto.visible='1'~t"
		ls_modify += "cf_cod_cliente.visible='0'~t"
		ls_modify += "cod_cliente.visible='0'~t"
		ls_modify += "cf_cod_fornitore.visible='0'~t"
		ls_modify += "cod_fornitore.visible='0'~t"
		ls_modify += "cod_des_cliente_t.visible='0'~t"
		ls_modify += "cf_cod_des_cliente.visible='0'~t"
		ls_modify += "cod_des_cliente.visible='0'~t"
		ls_modify += "cf_cod_des_fornitore.visible='0'~t"
		ls_modify += "cod_des_fornitore.visible='0'~t"
		ls_modify += "cod_fil_fornitore_t.visible='0'~t"
		ls_modify += "cf_cod_fil_fornitore.visible='0'~t"
		ls_modify += "cod_fil_fornitore.visible='0'~t"
		
	else
		//tutto come prima
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_cliente.visible=true
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_banca_for.visible=false
	
		ls_modify = "st_cliente_fornitore.text='Cliente:'~t"
		ls_modify += "cf_cod_contatto.visible='0'~t"
		ls_modify += "cod_contatto.visible='0'~t"
		ls_modify += "cf_cod_cliente.visible='1'~t"
		ls_modify += "cod_cliente.visible='1'~t"
		ls_modify += "cf_cod_fornitore.visible='0'~t"
		ls_modify += "cod_fornitore.visible='0'~t"
		ls_modify += "cod_des_cliente_t.visible='1'~t"
		ls_modify += "cf_cod_des_cliente.visible='1'~t"
		ls_modify += "cod_des_cliente.visible='1'~t"
		ls_modify += "cf_cod_des_fornitore.visible='0'~t"
		ls_modify += "cod_des_fornitore.visible='0'~t"
		ls_modify += "cod_fil_fornitore_t.visible='0'~t"
		ls_modify += "cf_cod_fil_fornitore.visible='0'~t"
		ls_modify += "cod_fil_fornitore.visible='0'~t"
		
	end if
	
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.modify(ls_modify)
	
	if not ab_rowfocuschange then
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_cliente", ls_null)
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_fornitore", ls_null)
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_contatto", ls_null)
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_des_cliente", ls_null)
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_des_fornitore", ls_null)
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_fil_fornitore", ls_null)
	end if
	
	
elseif as_flag_tipo_fat_ven = 'F' then
	
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_cliente.visible=false
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_banca_for.visible=true

	ls_modify = "st_cliente_fornitore.text='Fornitore:'~t"
	ls_modify += "cf_cod_contatto.visible='0'~t"
	ls_modify += "cod_contatto.visible='0'~t"
	ls_modify += "cf_cod_cliente.visible='0'~t"
	ls_modify += "cod_cliente.visible='0'~t"
	ls_modify += "cf_cod_fornitore.visible='1'~t"
	ls_modify += "cod_fornitore.visible='1'~t"
	ls_modify += "cod_des_cliente_t.visible='1'~t"
	ls_modify += "cf_cod_des_cliente.visible='0'~t"
	ls_modify += "cod_des_cliente.visible='0'~t"
	ls_modify += "cf_cod_des_fornitore.visible='1'~t"
	ls_modify += "cod_des_fornitore.visible='1'~t"
	ls_modify += "cod_fil_fornitore_t.visible='1'~t"
	ls_modify += "cf_cod_fil_fornitore.visible='1'~t"
	ls_modify += "cod_fil_fornitore.visible='1'~t"
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.modify(ls_modify)
	
	if not ab_rowfocuschange then
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_cliente", ls_null)
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_fornitore", ls_null)
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_des_cliente", ls_null)
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_des_fornitore", ls_null)
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_fil_fornitore", ls_null)
	end if
end if

if not ab_rowfocuschange then
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_deposito", ls_null)
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_valuta", ls_null)
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_tipo_listino_prodotto", ls_null)
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_pagamento", ls_null)
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "sconto", ls_null)
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_agente_1", ls_null)
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_agente_2", ls_null)
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "cod_banca_clien_for", ls_null)
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "flag_fuori_fido", 'N')
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.setitem(ll_riga, "sconto", ll_null)
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_label_clien_for_dep_1.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_label_clien_for_dep_2.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text=''")
//	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_contatto.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
//------------------------------ Modifica Nicola ------------------------------------------
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1_dest_fat.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2_dest_fat.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo_dest_fat.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione_dest_fat.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap_dest_fat.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita_dest_fat.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia_dest_fat.text=''")
//------------------------------- Fine Modifica -------------------------------------------
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text=''")
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.setitem(ll_riga_2, "rag_soc_1", ls_null)
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.setitem(ll_riga_2, "rag_soc_2", ls_null)
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.setitem(ll_riga_2, "indirizzo", ls_null)
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.setitem(ll_riga_2, "frazione", ls_null)
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.setitem(ll_riga_2, "cap", ls_null)
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.setitem(ll_riga_2, "localita", ls_null)
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.setitem(ll_riga_2, "provincia", ls_null)

	//--------------------------------- Modifiva Nicola ----------------------------------------
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.setitem(ll_riga_2, "rag_soc_1_dest_fat", ls_null)
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.setitem(ll_riga_2, "rag_soc_2_dest_fat", ls_null)
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.setitem(ll_riga_2, "indirizzo_dest_fat", ls_null)
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.setitem(ll_riga_2, "frazione_dest_fat", ls_null)
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.setitem(ll_riga_2, "cap_dest_fat", ls_null)
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.setitem(ll_riga_2, "localita_dest_fat", ls_null)
	tab_dettaglio.det_2.dw_tes_fat_ven_det_2.setitem(ll_riga_2, "provincia_dest_fat", ls_null)
	//---------------------------------- Fine Modifica -----------------------------------------	

	tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(ll_riga_3, "cod_imballo", ls_null)
	tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(ll_riga_3, "cod_vettore", ls_null)
	tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(ll_riga_3, "cod_inoltro", ls_null)
	tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(ll_riga_3, "cod_mezzo", ls_null)
	tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(ll_riga_3, "cod_porto", ls_null)
	tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(ll_riga_3, "cod_resa", ls_null)
end if
return 0
end function

public function integer wf_inserisci_anni (long al_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello anno fattura vendita
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean	lb_dati = false
string    	ls_sql, ls_null,  ls_sql_livello, ls_error, ls_anno_documento
long      	ll_i, ll_rows
str_treeview lstr_record
treeviewitem ltvi_campo

setnull(ls_null)


ls_sql = "select distinct isnull(tes_fat_ven.anno_documento,0) from tes_fat_ven "
ls_sql += "where tes_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows

	if isnull(ids_store.getitemnumber(ll_i, 1)) or ids_store.getitemnumber(ll_i, 1) = 0 then
		ls_anno_documento = "Non Fiscale"
		lstr_record.codice = "0"
	else
		ls_anno_documento = "Anno " + string( ids_store.getitemnumber(ll_i, 1) )
		lstr_record.codice = string( ids_store.getitemnumber(ll_i, 1) )
	end if
									
	lb_dati = true
	lstr_record.livello = il_livello
	lstr_record.tipo_livello = "A"
	
	ltvi_campo = wf_new_item(true, ICONA_ANNO)
	
	ltvi_campo.data = lstr_record
	
	ltvi_campo.label = ls_anno_documento
		
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_campo)

next

return ll_rows

end function

public function integer wf_inserisci_fatture (long al_handle);// ------------------------------------------------------------------------------------- //
//					funzione di caricamento fatture
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean				lb_dati = false
boolean				ib_max

long					ll_i, ll_anno_registrazione, ll_num_registrazione, ll_risposta, ll_anno_documento, ll_num_documento, ll_count, ll_max_count, ll_rows

string					ls_sql, ls_sql_livello, ls_cod_documento, ls_numeratore_documento, ls_error, ls_cod_cliente, ls_rag_soc_1, ls_localita, ls_cod_contatto, ls_soggetto

decimal				ld_tot_fattura

treeviewitem ltvi_item
str_treeview lstr_data


ls_sql =	"select distinct tes_fat_ven.anno_registrazione, tes_fat_ven.num_registrazione, tes_fat_ven.cod_documento, tes_fat_ven.anno_documento, tes_fat_ven.numeratore_documento, tes_fat_ven.num_documento,tes_fat_ven.tot_fattura, tes_fat_ven.cod_cliente, tes_fat_ven.cod_contatto, " + &			
			" anag_clienti.rag_soc_1, anag_clienti.localita, anag_contatti.rag_soc_1, anag_contatti.localita, tes_fat_ven.flag_contabilita, tes_fat_ven.flag_movimenti, tes_fat_ven.flag_esito_fatel" + &
			" from   tes_fat_ven " + &
			" left outer join anag_clienti on tes_fat_ven.cod_azienda=anag_clienti.cod_azienda and tes_fat_ven.cod_cliente = anag_clienti.cod_cliente " + &
			" left outer join anag_contatti on tes_fat_ven.cod_azienda=anag_contatti.cod_azienda and tes_fat_ven.cod_contatto = anag_contatti.cod_contatto " + is_sql_prodotto + " where tes_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' " + is_sql_filtro

wf_leggi_parent(al_handle,ls_sql)

ls_sql = ls_sql + " order by tes_fat_ven.cod_documento, tes_fat_ven.numeratore_documento, tes_fat_ven.anno_documento, tes_fat_ven.num_documento, tes_fat_ven.anno_registrazione, tes_fat_ven.num_registrazione "

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

// aggiungo controllo per il numero massimo di risultati
if ll_rows > il_max_record  then
	if not g_mb.confirm("Sono state recuperate " + string(ll_rows) + " superando il limite consigliato.~r~nProcedere con la visualizzazione di tutte le fatture?") then
		// Visualizzo solo le richieste nel limite
		ll_rows = il_max_record
	end if
end if

for ll_i = 1 to ll_rows
	
	ll_anno_registrazione  = ids_store.getitemnumber(ll_i, 1)
	ll_num_registrazione = ids_store.getitemnumber(ll_i, 2)
	ls_cod_documento = ids_store.getitemstring(ll_i, 3)
	ll_anno_documento = ids_store.getitemnumber(ll_i, 4)
	ls_numeratore_documento = ids_store.getitemstring(ll_i, 5)
	ll_num_documento = ids_store.getitemnumber(ll_i, 6)
	ld_tot_fattura = ids_store.getitemnumber(ll_i, 7)
	ls_cod_cliente = ids_store.getitemstring(ll_i, 8)
	ls_cod_contatto = ids_store.getitemstring(ll_i, 9)

	if not isnull(ls_cod_cliente) then
		
		ls_soggetto = " -" + ids_store.getitemstring(ll_i, 10) + " (" + ls_cod_cliente + ")"
		ls_localita = ids_store.getitemstring(ll_i, 11)
	elseif not isnull(ls_cod_contatto) then
		ls_soggetto = " -" + ids_store.getitemstring(ll_i, 12) + " (* " + ls_cod_contatto + " *)"
		ls_localita = ids_store.getitemstring(ll_i, 13)
		
	end if
	
	lstr_data.livello = il_livello
	lstr_data.tipo_livello = "N"
	lstr_data.decimale[1] = ll_anno_registrazione
	lstr_data.decimale[2] = ll_num_registrazione
	
	if ids_store.getitemstring(ll_i, 16) = "NS" then
		// fattura con esito SCARTATA
		ltvi_item = wf_new_item(false, ICONA_FATTURA_ESITO_SCARTATA )
	elseif ids_store.getitemstring(ll_i, 14) = "S" then
		ltvi_item = wf_new_item(false, ICONA_FATTURA_CONTABILIZZATA )
	elseif ids_store.getitemstring(ll_i, 15) = "S" then
		ltvi_item = wf_new_item(false, ICONA_FATTURA_CONFERMATA)
	else
		ltvi_item = wf_new_item(false, ICONA_FATTURA )
	end if

	ltvi_item.data = lstr_data
	
	ltvi_item.label = string(ll_anno_registrazione) + "/"+ string(ll_num_registrazione)
	
	if not isnull(ls_cod_documento) and ls_cod_documento<>"" and &
		not isnull(ll_anno_documento) and &
		not isnull(ls_numeratore_documento) and ls_numeratore_documento<>"" and &
		not isnull(ll_num_documento) then
			
		ltvi_item.label = " "+ls_cod_documento + " " + string(ll_anno_documento) + "/"+ls_numeratore_documento+" " &
								+ string(ll_num_documento)
	end if
	
	if isnull(ld_tot_fattura) then ld_tot_fattura=0
	ltvi_item.label += " € "+string(ld_tot_fattura, "#,###,###,##0.00")
	
	//-----------------------------------------------------------
	//if not isnull(ls_cod_cliente) then ltvi_campo.label +=" -" + ls_rag_soc_1 + " (" + ls_cod_cliente + ")"
	if not isnull(ls_soggetto) then ltvi_item.label += ls_soggetto
	//-----------------------------------------------------------
	
	if not isnull(ls_localita) then ltvi_item.label += "-" + ls_localita
	

	ll_risposta = tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
	
	if ll_count > 500 and not ib_max then
		ib_max = true
		if g_mb.messagebox("APICE","Attenzione troppi dati caricati nell'albero delle fatture. Continuare?" + & 
					  "(Si consiglia di caricare annullare scegliendo NO, impostare qualche altro elemento nel filtro e riprovare.)",Question!, YesNo!,2)=2 then
			return 0
		end if
	end if
next

return ll_rows
end function

public function long wf_leggi_livello (long al_handle, integer ai_livello);il_livello = ai_livello

choose case wf_get_valore_livello(ai_livello)
		
	case "A"
		return wf_inserisci_anni(al_handle)
		
	case "T"
		return wf_inserisci_tipi_fatture(al_handle)
		
	case "C"		// clienti - fornitori - contatti
		return wf_inserisci_soggetto(al_handle)
		
	case "S"
		return wf_inserisci_causali(al_handle)
		
	case "P"
		return wf_inserisci_pagamenti(al_handle)
		
	case "X"
		return wf_inserisci_prodotti(al_handle)
						
	case else
		return wf_inserisci_fatture(al_handle)
		
end choose


end function

public function integer wf_inserisci_soggetto (long al_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello clienti
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean	lb_dati = false
string    	ls_sql, ls_null,  ls_sql_livello, ls_error, ls_label
long      	ll_i, ll_rows
str_treeview lstr_record
treeviewitem ltvi_campo

setnull(ls_null)


ls_sql = 		" select distinct tes_fat_ven.cod_cliente, tes_fat_ven.cod_contatto, tes_fat_ven.cod_fornitore,anag_clienti.rag_soc_1, anag_clienti.localita, anag_fornitori.rag_soc_1, anag_fornitori.localita,anag_contatti.rag_soc_1, anag_contatti.localita "
ls_sql += 	" from tes_fat_ven "
ls_sql += 	" left outer join anag_clienti on anag_clienti.cod_azienda = tes_fat_ven.cod_azienda and anag_clienti.cod_cliente = tes_fat_ven.cod_cliente "
ls_sql += 	" left outer join anag_fornitori on anag_fornitori.cod_azienda = tes_fat_ven.cod_azienda and anag_fornitori.cod_fornitore= tes_fat_ven.cod_fornitore "
ls_sql += 	" left outer join anag_contatti on anag_contatti.cod_azienda = tes_fat_ven.cod_azienda and anag_contatti.cod_contatto= tes_fat_ven.cod_contatto "
ls_sql += 	" where tes_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ls_sql += " order by anag_clienti.rag_soc_1 "

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows

	lstr_record.livello = il_livello
	lstr_record.tipo_livello = "C"
	
	if not isnull( ids_store.getitemstring(ll_i, 1) ) then
		lstr_record.codice = ids_store.getitemstring(ll_i, 1)
		ls_label = g_str.format( "$1 - [$2]",ids_store.getitemstring(ll_i, 4), ids_store.getitemstring(ll_i, 1) )
		ltvi_campo = wf_new_item(true, ICONA_CLIENTE)
	elseif isnull( ids_store.getitemstring(ll_i, 2) ) then
		lstr_record.codice = ids_store.getitemstring(ll_i, 2)
		ls_label = g_str.format( "$1 - [$2]",ids_store.getitemstring(ll_i, 5), ids_store.getitemstring(ll_i, 2) )
		ltvi_campo = wf_new_item(true, ICONA_CONTATTO)
	elseif isnull( ids_store.getitemstring(ll_i, 3) ) then
		lstr_record.codice = ids_store.getitemstring(ll_i, 3)
		ls_label = g_str.format( "$1 - [$2]",ids_store.getitemstring(ll_i, 6), ids_store.getitemstring(ll_i, 3) )
		ltvi_campo = wf_new_item(true, ICONA_FORNITORE)
	else
		lstr_record.codice = ""
		ls_label = "Record Anomalo"
		ltvi_campo = wf_new_item(true, ICONA_CLIENTE)
	end if	
	
	ltvi_campo.data = lstr_record
	
	ltvi_campo.label = ls_label

		
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_campo)

next

return ll_rows

end function

public subroutine wf_valori_livelli ();wf_add_valore_livello("Non Specificato", "N")
wf_add_valore_livello("Anno Fattura", "A")
wf_add_valore_livello("Cliente-Contatto-Fornitore", "C")
wf_add_valore_livello("Tipo Fattura", "T")
wf_add_valore_livello("Pagamento", "P")
wf_add_valore_livello("Causale", "S")
wf_add_valore_livello("Prodotto", "X")

end subroutine

public function integer wf_leggi_parent (long al_handle, ref string as_sql);long	ll_item
treeviewitem ltv_item
str_treeview lstr_data

if al_handle = 0 then return 0

do
	
	tab_ricerca.selezione.tv_selezione.getitem(al_handle, ltv_item)
		
	lstr_data = ltv_item.data
	
	choose case lstr_data.tipo_livello
			
		case "A"
			if isnull(lstr_data.codice) or lstr_data.codice = "" or lstr_data.codice = "0" then
				as_sql += " AND (tes_fat_ven.anno_documento is null or tes_fat_ven.anno_documento = 0) "
			else
				as_sql += " AND tes_fat_ven.anno_documento = '" + lstr_data.codice + "' "
			end if
			
		case "C"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_fat_ven.cod_cliente is null "
			else
				as_sql += " AND tes_fat_ven.cod_cliente = '" + lstr_data.codice + "' "
			end if
			
		case "T"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_fat_ven.cod_tipo_fat_ven is null "
			else
				as_sql += " AND tes_fat_ven.cod_tipo_fat_ven = '" + lstr_data.codice + "' "
			end if
			
		case "S"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_fat_ven.cod_causale is null "
			else
				as_sql += " AND tes_fat_ven.cod_causale = '" + lstr_data.codice + "' "
			end if
			
		case "P"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND tes_fat_ven.cod_pagamento is null "
			else
				as_sql += " AND tes_fat_ven.cod_pagamento = '" + lstr_data.codice + "' "
			end if
						
		case "X"
			if isnull(lstr_data.codice) or lstr_data.codice = "" then
				as_sql += " AND det_fat_ven.cod_prodotto is null "
			else
				as_sql += " AND det_fat_ven.cod_prodotto = '" + lstr_data.codice + "' "
			end if
			
	end choose
	
	al_handle = tab_ricerca.selezione.tv_selezione.finditem(parenttreeitem!, al_handle)
	
loop while al_handle <> -1

return 0
end function

public function integer wf_inserisci_pagamenti (long al_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello pagamenti
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean	lb_dati = false
string    	ls_sql, ls_null,  ls_sql_livello, ls_error, ls_label
long      	ll_i, ll_rows
str_treeview lstr_record
treeviewitem ltvi_campo

setnull(ls_null)


ls_sql = 		" select distinct tes_fat_ven.cod_pagamento, tab_pagamenti.des_pagamento "
ls_sql += 	" from tes_fat_ven "
ls_sql += 	" left outer join tab_pagamenti on tab_pagamenti.cod_azienda = tes_fat_ven.cod_azienda and tab_pagamenti.cod_pagamento = tes_fat_ven.cod_pagamento "
ls_sql += 	" where tes_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ls_sql += " order by tab_pagamenti.des_pagamento "

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows

	lstr_record.livello = il_livello
	lstr_record.tipo_livello = "P"
	
	lstr_record.codice = ids_store.getitemstring(ll_i, 1)
	ls_label = g_str.format( "$1 - [$2]",ids_store.getitemstring(ll_i, 2), ids_store.getitemstring(ll_i, 1) )
	ltvi_campo = wf_new_item(true, ICONA_PAGAMENTO)
	
	ltvi_campo.data = lstr_record
	
	ltvi_campo.label = ls_label

		
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_campo)

next

return ll_rows

end function

public function integer wf_inserisci_tipi_fatture (long al_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello TIPI FATTURE
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean	lb_dati = false
string    	ls_sql, ls_null,  ls_sql_livello, ls_error, ls_label
long      	ll_i, ll_rows
str_treeview lstr_record
treeviewitem ltvi_campo

setnull(ls_null)


ls_sql = 		" select distinct tes_fat_ven.cod_tipo_fat_ven, tab_tipi_fat_ven.des_tipo_fat_ven "
ls_sql += 	" from tes_fat_ven "
ls_sql += 	" left outer join tab_tipi_fat_ven on tab_tipi_fat_ven.cod_azienda = tes_fat_ven.cod_azienda and tab_tipi_fat_ven.cod_tipo_fat_ven = tes_fat_ven.cod_tipo_fat_ven "
ls_sql += 	" where tes_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ls_sql += " order by tab_tipi_fat_ven.des_tipo_fat_ven "

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows

	lstr_record.livello = il_livello
	lstr_record.tipo_livello = "T"
	
	lstr_record.codice = ids_store.getitemstring(ll_i, 1)
	ls_label = g_str.format( "$1 - [$2]",ids_store.getitemstring(ll_i, 2), ids_store.getitemstring(ll_i, 1) )
	ltvi_campo = wf_new_item(true, ICONA_TIPO_FATTURA)
	
	ltvi_campo.data = lstr_record
	
	ltvi_campo.label = ls_label

		
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_campo)

next

return ll_rows

end function

public function integer wf_inserisci_causali (long al_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello TIPI FATTURE
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean	lb_dati = false
string    	ls_sql, ls_null,  ls_sql_livello, ls_error, ls_label
long      	ll_i, ll_rows
str_treeview lstr_record
treeviewitem ltvi_campo

setnull(ls_null)


ls_sql = 		" select distinct tes_fat_ven.cod_causale, tab_causali_trasp.des_causale "
ls_sql += 	" from tes_fat_ven "
ls_sql += 	" left outer join tab_causali_trasp on tab_causali_trasp.cod_azienda = tes_fat_ven.cod_azienda and tab_causali_trasp.cod_causale = tes_fat_ven.cod_causale "
ls_sql += 	" where tes_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ls_sql += " order by tab_causali_trasp.des_causale "

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows

	lstr_record.livello = il_livello
	lstr_record.tipo_livello = "S"
	
	lstr_record.codice = ids_store.getitemstring(ll_i, 1)
	ls_label = g_str.format( "$1 - [$2]",ids_store.getitemstring(ll_i, 2), ids_store.getitemstring(ll_i, 1) )
	ltvi_campo = wf_new_item(true, ICONA_CAUSALE)
	
	ltvi_campo.data = lstr_record
	
	ltvi_campo.label = ls_label

		
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_campo)

next

return ll_rows

end function

public function any wf_inserisci_prodotti (long al_handle);// ------------------------------------------------------------------------------------- //
//	funzione di caricamento livello prodotti
//		return -1 = errore
//		return  0 = tutto OK dati caricati
//		return  1 = tutto OK, ma non è stato trovato alcun dato
//
// ------------------------------------------------------------------------------------- //

boolean	lb_dati = false
string    	ls_sql, ls_null,  ls_sql_livello, ls_error, ls_label
long      	ll_i, ll_rows
str_treeview lstr_record
treeviewitem ltvi_campo

setnull(ls_null)


ls_sql = 		" select distinct det_fat_ven.cod_prodotto, anag_prodotti.des_prodotto, anag_prodotti.flag_blocco "
ls_sql += 	" from tes_fat_ven "
ls_sql += 	" left outer join det_fat_ven on tes_fat_ven.cod_azienda = det_fat_ven.cod_azienda and  tes_fat_ven.anno_registrazione = det_fat_ven.anno_registrazione and tes_fat_ven.num_registrazione = det_fat_ven.num_registrazione "
ls_sql += 	" left outer join anag_prodotti on anag_prodotti.cod_azienda = det_fat_ven.cod_azienda and anag_prodotti.cod_prodotto= det_fat_ven.cod_prodotto "
ls_sql += 	" where tes_fat_ven.cod_azienda = '" + s_cs_xx.cod_azienda + "' " +  is_sql_filtro

wf_leggi_parent(al_handle, ls_sql)

ls_sql += " order by  anag_prodotti.des_prodotto "

ll_rows = guo_functions.uof_crea_datastore(ids_store, ls_sql, ls_error)
if ll_rows < 0 then	g_mb.error(ls_error, sqlca)

for ll_i = 1 to ll_rows

	lstr_record.livello = il_livello
	lstr_record.tipo_livello = "X"
	lstr_record.codice = ids_store.getitemstring(ll_i, 1)
	
	if not isnull( ids_store.getitemstring(ll_i, 1) ) then
		ls_label = g_str.format( "$1 - [$2]",ids_store.getitemstring(ll_i, 2), ids_store.getitemstring(ll_i, 1) )
		if ids_store.getitemstring(ll_i, 3) = "S" then ls_label =+ "*BLOCCATO* "
	else
		ls_label = "Prodotto <NULL>"
	end if	
	
	ltvi_campo = wf_new_item(true, ICONA_PRODOTTO)
	
	ltvi_campo.data = lstr_record
	
	ltvi_campo.label = ls_label

		
	tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_campo)

next

return ll_rows

end function

public function integer wf_inserisci_fattura_vendita (long al_handle, long al_anno_registrazione, long al_num_registrazione);string ls_sql, ls_error
long ll_handle
treeviewitem ltvi_item

str_treeview lstr_data
	
lstr_data.livello = il_livello
lstr_data.tipo_livello = "N"
lstr_data.decimale[1] = al_anno_registrazione
lstr_data.decimale[2] = al_num_registrazione

ltvi_item = wf_new_item(false, ICONA_NEW)

ltvi_item.data = lstr_data
ltvi_item.label = "[NEW] " + string(long(lstr_data.decimale[1])) + "/" + string(long(lstr_data.decimale[2]))

ib_fire_selection_changed = false

istr_data = lstr_data 

ll_handle = tab_ricerca.selezione.tv_selezione.insertitemlast(al_handle, ltvi_item)
tab_ricerca.selezione.tv_selezione.SelectItem(ll_handle)

ib_fire_selection_changed = true

return 1
end function

public subroutine wf_contabilizza_fattura (long al_anno_registrazione, long al_num_registrazione);string				ls_messaggio, ls_errore,ls_cod_documento, ls_numeratore_documento, ls_motivazione, ls_str, ls_flag_movimenti,ls_flag_calcolo

long				ll_rows, ll_i, ll_anno_documento, ll_num_documento, ll_anno_registrazione, ll_num_registrazione, ll_riga, ll_return, &
					ll_id_registrazione, ll_num_reg_pd
					
datetime			ldt_data_fattura

uo_log_sistema		luo_log_sistema
uo_impresa lu_impresa




setnull(ls_messaggio)
setnull(ls_errore)

select		cod_documento,
			numeratore_documento,
			anno_documento,
			num_documento,
			data_fattura,
			flag_movimenti,
			flag_calcolo
into		:ls_cod_documento,
			:ls_numeratore_documento ,
			:ll_anno_documento ,
			:ll_num_documento ,
			:ldt_data_fattura,
			:ls_flag_movimenti,
			:ls_flag_calcolo
from		tes_fat_ven
where		cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :al_anno_registrazione and
			num_registrazione = :al_num_registrazione;

if sqlca.sqlcode = 100 then
	g_mb.warning("APICE","Nessuna fattura da contabilizzare!")
	return
end if

if isnull(ls_cod_documento) then
	g_mb.warning("APICE","Fattura senza numero fiscale associato!")
	return
end if

if ls_flag_movimenti = "N" or isnull(ls_flag_movimenti) then
	g_mb.warning("APICE","Fattura ancora da confermare!")
	return
end if

if ls_flag_calcolo = "N" or isnull(ls_flag_movimenti) then
	g_mb.warning("APICE","Fattura non calcolata!")
	return
end if

		
ls_messaggio = ""
pcca.mdi_frame.setmicrohelp(	"Contabilizzazione Fattura "+ &
										ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
										"/" + string(ll_num_documento,"###000")+"  ("+string(ll_i)+" di "+string(ll_rows)+") in corso...")
// -------------------------------------------------------------------------------------------------------------------------------------------------------------------
//ll_return = wf_contabilizza(al_anno_registrazione, al_num_registrazione, ls_messaggio, ls_errore)
lu_impresa = CREATE uo_impresa

is_mov_intra = ""

lu_impresa.is_mov_intra = ""

lu_impresa.uof_set_tipo_det_trasporto( )

ll_return = lu_impresa.uof_contabilizza_fat_ven(al_anno_registrazione, al_num_registrazione, ll_num_reg_pd)
if ll_return = 0 then
	
	if lu_impresa.is_mov_intra<>"" and not isnull(lu_impresa.is_mov_intra) then
		is_mov_intra = lu_impresa.is_mov_intra
	end if
	
	destroy lu_impresa
else
	ls_messaggio = lu_impresa.i_messaggio
	ls_errore    = string(lu_impresa.i_db_errore)
	destroy lu_impresa
end if
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
//errore specifico -8

luo_log_sistema = create uo_log_sistema

if ll_return = -8 then
	ls_str = "FATTURA " + ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
				"/" + string(ll_num_documento,"###000") + "  (int " + string(al_anno_registrazione) + "/" + string(al_num_registrazione) +") ~r~n" + &
				"Il protocollo risulta essere già esistente nel registro IVA~r~n"
	
	
	g_mb.error(ls_str)
	
	luo_log_sistema.uof_write_log_sistema_not_sqlca("CONT_FAT_VEN", ls_str)			
	
	ROLLBACK using sqlca;
	ROLLBACK using sqlci;
	
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
//errore -1
elseif ll_return < 0 then
	ls_str = "FATTURA " + ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
				"/" + string(ll_num_documento,"###000") + "  (int " + string(al_anno_registrazione) + "/" + string(al_num_registrazione) +") ~r~n" + &
				"Messaggio = " + ls_messaggio + "~r~n" + "Errore = " + ls_errore +  "~r~n"
	

	g_mb.error(ls_str)
	luo_log_sistema.uof_write_log_sistema_not_sqlca("CONT_FAT_VEN", ls_str)			
	
	ROLLBACK using sqlca;
	ROLLBACK using sqlci;
	
else
	//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//fin qui tutto a posto ma se non riesce a mettere SI al flag contabilizzata annulla la contabilizzazione della fattura
	update tes_fat_ven
	set    flag_contabilita = 'S'
	where  tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and
			 tes_fat_ven.anno_registrazione = :al_anno_registrazione and
			 tes_fat_ven.num_registrazione = :al_num_registrazione;
			 
	if sqlca.sqlcode <> 0 then
		ls_str = "FATTURA " + ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) + &
		"/" + string(ll_num_documento,"###000") + "  (int " + string(al_anno_registrazione) + "/" + string(al_num_registrazione) +") ~r~n" + &
					"Errore durante aggiornamento segnale fattura contabilizzata" +  + "~r~n" + "Errore = " + sqlca.sqlerrtext +  "~r~n"
		
		
		g_mb.error(ls_str)
		
		luo_log_sistema.uof_write_log_sistema_not_sqlca("CONT_FAT_VEN", ls_str)			
	
		ROLLBACK using sqlca;
		ROLLBACK using sqlci;
		
	else
		//--------------------------------------------------------------------------------------------------------------------------------------------------------------------
		//OK contabilizzazione
		ls_str = "~r~n~r~n"
		ls_str += "-------------------------------------------------------------------------------------------------------------------------~r~n"
		ls_str += "FATTURA " + ls_cod_documento + " - " + ls_numeratore_documento + " - " + string(ll_anno_documento) &
					+ "/" + string(ll_num_documento,"###000") + "  (int " + string(al_anno_registrazione) + "/" + string(al_num_registrazione) +") - REGISTRAZIONE N. " + string(ll_num_reg_pd) +" ~r~n"
		
		if is_mov_intra<>"" and not isnull(is_mov_intra) then
			ls_str += is_mov_intra+"~r~n"
		end if
		is_mov_intra = ""
		
		g_mb.success(ls_str)
		
		luo_log_sistema.uof_write_log_sistema_not_sqlca("CONT_FAT_VEN", ls_str)			
	
		if not isnull(ls_messaggio) and len(ls_messaggio) > 0 then
			g_mb.warning("*** ATTENZIONE:" + ls_messaggio)
		end if
		
		
		COMMIT using sqlca;
		COMMIT using sqlci;
		
	end if
end if

destroy luo_log_sistema



end subroutine

public subroutine wf_abilita_pulsanti (boolean ab_status);tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_banca_for.enabled = ab_status
tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_banca_for.enabled = ab_status
tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_cliente.enabled = ab_status
tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_contropartite.enabled = not(ab_status)
tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_scadenze.enabled = not(ab_status)
tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_iva.enabled = not(ab_status)

tab_dettaglio.det_2.dw_tes_fat_ven_det_2.object.b_ricerca_cliente.enabled = ab_status
end subroutine

public subroutine wf_duplica_documento (string as_flag_crea_nota_credito, string as_cod_tipo_fat_nc);long     ll_anno_registrazione, ll_num_registrazione, ll_anno_registrazione_orig, ll_num_registrazione_orig, ll_riga
string ls_cod_operatore, ls_messaggio, ls_cod_tipo_fat_ven_nc
datetime ldt_oggi
uo_calcola_documento_euro luo_calcolo


ll_anno_registrazione_orig = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "anno_registrazione")
ll_num_registrazione_orig = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "num_registrazione")

if as_flag_crea_nota_credito = "S" then 
	ls_cod_tipo_fat_ven_nc = as_cod_tipo_fat_nc
else
	select cod_tipo_fat_ven
	into :ls_cod_tipo_fat_ven_nc
	from tes_fat_ven
	  where cod_azienda = :s_cs_xx.cod_azienda and
			  anno_registrazione = :ll_anno_registrazione_orig and
			  num_registrazione = :ll_num_registrazione_orig;
	if sqlca.sqlcode <> 0 then
		g_mb.error("Duplicazione Fattura: errore durante la ricerca del tipo fattura da duplicare.")
		return
	end if
end if


if ll_anno_registrazione_orig>0 and ll_num_registrazione_orig>0 then
	if not g_mb.confirm("Duplicare il documento " +string(ll_anno_registrazione_orig)+"/"+string(ll_num_registrazione_orig)+ " creando una nuova fattura?") then
		g_mb.warning("Annullato dall'operatore!")
		return
	end if
end if

////-----------------------------------------------------------------------------------------------------------------------------------------------------
//prima di procedere fare alcune verifiche
//-----------------------------------------------------------------------------------------------------------------------------------------------------
if not wf_controlla_duplicazione(ll_anno_registrazione_orig, ll_num_registrazione_orig) then
	g_mb.warning("Annullato dall'operatore!")
	return
end if
//-----------------------------------------------------------------------------------------------------------------------------------------------------

ldt_oggi = datetime(today())

guo_functions.uof_get_parametro_azienda( "ESC", ll_anno_registrazione)

select 	max(num_registrazione)
into   		:ll_num_registrazione
from   	tes_fat_ven
where  	cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione;

if isnull(ll_num_registrazione) or ll_num_registrazione = 0 then 
	ll_num_registrazione = 1
else
	ll_num_registrazione++
end if

select cod_operatore
  into :ls_cod_operatore
  from tab_operatori_utenti
 where cod_azienda = :s_cs_xx.cod_azienda and
		 cod_utente = :s_cs_xx.cod_utente and
		 flag_default = 'S';
		 
if sqlca.sqlcode = -1 then
	g_mb.messagebox("Estrazione Operatori", "Errore durante l'Estrazione dell'Operatore di Default")
	pcca.error = c_fatal
	return
elseif sqlca.sqlcode = 100 then
	setnull(ls_cod_operatore)
end if

insert into tes_fat_ven
		( cod_azienda,
		  anno_registrazione,
		  num_registrazione,
		  data_registrazione,
		  cod_operatore,
		  cod_tipo_fat_ven,
		  cod_cliente,
		  cod_des_cliente,
		  rag_soc_1,
		  rag_soc_2,
		  indirizzo,
		  localita,
		  frazione,
		  cap,
		  provincia,
		  cod_deposito,
		  cod_ubicazione,
		  cod_valuta,
		  cambio_ven,
		  cod_tipo_listino_prodotto,
		  cod_pagamento,
		  sconto,
		  cod_agente_1,
		  cod_agente_2,
		  cod_banca,
		  num_ord_cliente,
		  data_ord_cliente,
		  cod_imballo,
		  aspetto_beni,
		  peso_netto,
		  peso_lordo,
		  num_colli,
		  cod_vettore,
		  cod_inoltro,
		  cod_mezzo,
		  causale_trasporto,
		  cod_porto,
		  cod_resa,
		  cod_documento,
		  numeratore_documento,
		  anno_documento,
		  num_documento,
		  data_fattura,
		  nota_testata,
		  nota_piede,
		  flag_fuori_fido,
		  flag_blocco,
		  flag_calcolo,
		  flag_movimenti,   
		  flag_contabilita,
		  tot_merci,
		  tot_spese_trasporto,
		  tot_spese_imballo,
		  tot_spese_bolli,
		  tot_spese_varie,
		  tot_sconto_cassa,
		  tot_sconti_commerciali,
		  imponibile_provvigioni_1,
		  imponibile_provvigioni_2,
		  tot_provvigioni_1,
		  tot_provvigioni_2,
		  importo_iva,
		  imponibile_iva,
		  importo_iva_valuta,
		  imponibile_iva_valuta,
		  tot_fattura,
		  tot_fattura_valuta,
		  flag_cambio_zero,
		  cod_banca_clien_for,
		  cod_causale,
		  cod_contatto,
		  cod_natura_intra)  
select  cod_azienda,
		  :ll_anno_registrazione,   
		  :ll_num_registrazione,   
		  :ldt_oggi,
		  :ls_cod_operatore,
		  :ls_cod_tipo_fat_ven_nc,
		  cod_cliente,
		  cod_des_cliente,
		  rag_soc_1,
		  rag_soc_2,
		  indirizzo,
		  localita,
		  frazione,
		  cap,
		  provincia,
		  cod_deposito,
		  cod_ubicazione,
		  cod_valuta,
		  cambio_ven,
		  cod_tipo_listino_prodotto,
		  cod_pagamento,
		  sconto,
		  cod_agente_1,
		  cod_agente_2,
		  cod_banca,
		  num_ord_cliente,
		  data_ord_cliente,
		  cod_imballo,
		  aspetto_beni,
		  peso_netto,
		  peso_lordo,
		  num_colli,
		  cod_vettore,
		  cod_inoltro,
		  cod_mezzo,
		  causale_trasporto,
		  cod_porto,
		  cod_resa,
		  null,
		  null,
		  null,
		  null,
		  null,
		  nota_testata,
		  nota_piede,
		  flag_fuori_fido,
		  'N',
		  'N',
		  'N',
		  'N',
		  null,
		  null,
		  null,
		  null,
		  null,
		  null,
		  null,
		  imponibile_provvigioni_1,
		  imponibile_provvigioni_2,
		  tot_provvigioni_1,
		  tot_provvigioni_2,
		  null,
		  null,
		  null,
		  null,
		  null,
		  null,
		  flag_cambio_zero,
		  cod_banca_clien_for,
		  cod_causale,
		  cod_contatto,
		  cod_natura_intra
	from tes_fat_ven
  where cod_azienda = :s_cs_xx.cod_azienda and
		  anno_registrazione = :ll_anno_registrazione_orig and
		  num_registrazione = :ll_num_registrazione_orig;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di duplicazione testata Fatture.", &
				  exclamation!, ok!)
	rollback;
	return
end if

if as_flag_crea_nota_credito = "S" then

	insert into det_fat_ven
		( cod_azienda,
		  anno_registrazione,
		  num_registrazione,
		  prog_riga_fat_ven,
		  cod_tipo_det_ven,
		  cod_deposito,
		  cod_ubicazione,
		  cod_lotto,
		  data_stock,
		  progr_stock,
		  cod_misura,
		  cod_prodotto,
		  des_prodotto,
		  quan_fatturata,
		  prezzo_vendita,
		  sconto_1,
		  sconto_2,
		  provvigione_1,
		  provvigione_2,
		  cod_iva,
		  cod_tipo_movimento,
		  num_registrazione_mov_mag,
		  anno_registrazione_fat,
		  num_registrazione_fat,
		  nota_dettaglio,
		  anno_registrazione_bol_ven,
		  num_registrazione_bol_ven,
		  prog_riga_bol_ven,
		  anno_commessa,
		  num_commessa,
		  cod_centro_costo,
		  sconto_3,
		  sconto_4,
		  sconto_5,
		  sconto_6,
		  sconto_7,
		  sconto_8,
		  sconto_9,
		  sconto_10,
		  anno_registrazione_mov_mag,
		  fat_conversione_ven,
		  anno_reg_des_mov,
		  num_reg_des_mov,
		  anno_reg_ord_ven,
		  num_reg_ord_ven,
		  prog_riga_ord_ven,
		  cod_versione,
		  num_confezioni,
		  num_pezzi_confezione,
		  flag_st_note_det,
		  quantita_um,
		  prezzo_um,
		  flag_cc_manuali,
		  ordinamento,
		  flag_riga_spesa_trasp,
		  prezzo_vendita_spesa_perc,
		  fatel_cig,
		  fatel_iddocumento,
		  anno_reg_fat_ven_nc,
		  num_reg_fat_ven_nc, 
		  prog_riga_fat_ven_nc)  
	select cod_azienda,
			:ll_anno_registrazione,   
			:ll_num_registrazione,   
			prog_riga_fat_ven,
			cod_tipo_det_ven,
			cod_deposito,
			cod_ubicazione,
			cod_lotto,
			data_stock,
			progr_stock,
			cod_misura,
			cod_prodotto,
			des_prodotto,
			quan_fatturata,
			prezzo_vendita,
			sconto_1,
			sconto_2,
			provvigione_1,
			provvigione_2,
			cod_iva,
			cod_tipo_movimento,
			null,
			null,
			null,
			nota_dettaglio,
			null,
			null,
			null,
			null,
			null,
			cod_centro_costo,
			sconto_3,
			sconto_4,
			sconto_5,
			sconto_6,
			sconto_7,
			sconto_8,
			sconto_9,
			sconto_10,
			null,
			fat_conversione_ven,
			null,
			null,
			null,
			null,
			null,
			cod_versione,
			num_confezioni,
			num_pezzi_confezione,
			flag_st_note_det,
			quantita_um,
			prezzo_um,
			flag_cc_manuali,
			ordinamento,
			flag_riga_spesa_trasp,
			prezzo_vendita_spesa_perc,
			fatel_cig,
			fatel_iddocumento,
			anno_registrazione,
			num_registrazione, 
			prog_riga_fat_ven
	 from det_fat_ven  
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione_orig and
			num_registrazione = :ll_num_registrazione_orig ;
else
	insert into det_fat_ven
		( cod_azienda,
		  anno_registrazione,
		  num_registrazione,
		  prog_riga_fat_ven,
		  cod_tipo_det_ven,
		  cod_deposito,
		  cod_ubicazione,
		  cod_lotto,
		  data_stock,
		  progr_stock,
		  cod_misura,
		  cod_prodotto,
		  des_prodotto,
		  quan_fatturata,
		  prezzo_vendita,
		  sconto_1,
		  sconto_2,
		  provvigione_1,
		  provvigione_2,
		  cod_iva,
		  cod_tipo_movimento,
		  num_registrazione_mov_mag,
		  anno_registrazione_fat,
		  num_registrazione_fat,
		  nota_dettaglio,
		  anno_registrazione_bol_ven,
		  num_registrazione_bol_ven,
		  prog_riga_bol_ven,
		  anno_commessa,
		  num_commessa,
		  cod_centro_costo,
		  sconto_3,
		  sconto_4,
		  sconto_5,
		  sconto_6,
		  sconto_7,
		  sconto_8,
		  sconto_9,
		  sconto_10,
		  anno_registrazione_mov_mag,
		  fat_conversione_ven,
		  anno_reg_des_mov,
		  num_reg_des_mov,
		  anno_reg_ord_ven,
		  num_reg_ord_ven,
		  prog_riga_ord_ven,
		  cod_versione,
		  num_confezioni,
		  num_pezzi_confezione,
		  flag_st_note_det,
		  quantita_um,
		  prezzo_um,
		  flag_cc_manuali,
		  ordinamento,
		  flag_riga_spesa_trasp,
		  prezzo_vendita_spesa_perc,
		  fatel_cig,
		  fatel_iddocumento,
		  anno_reg_fat_ven_nc,
		  num_reg_fat_ven_nc, 
		  prog_riga_fat_ven_nc)  
	select cod_azienda,
			:ll_anno_registrazione,   
			:ll_num_registrazione,   
			prog_riga_fat_ven,
			cod_tipo_det_ven,
			cod_deposito,
			cod_ubicazione,
			cod_lotto,
			data_stock,
			progr_stock,
			cod_misura,
			cod_prodotto,
			des_prodotto,
			quan_fatturata,
			prezzo_vendita,
			sconto_1,
			sconto_2,
			provvigione_1,
			provvigione_2,
			cod_iva,
			cod_tipo_movimento,
			null,
			null,
			null,
			nota_dettaglio,
			null,
			null,
			null,
			null,
			null,
			cod_centro_costo,
			sconto_3,
			sconto_4,
			sconto_5,
			sconto_6,
			sconto_7,
			sconto_8,
			sconto_9,
			sconto_10,
			null,
			fat_conversione_ven,
			null,
			null,
			null,
			null,
			null,
			cod_versione,
			num_confezioni,
			num_pezzi_confezione,
			flag_st_note_det,
			quantita_um,
			prezzo_um,
			flag_cc_manuali,
			ordinamento,
			flag_riga_spesa_trasp,
			prezzo_vendita_spesa_perc,
			fatel_cig,
			fatel_iddocumento,
			null,
			null,
			null
	 from det_fat_ven  
	where cod_azienda = :s_cs_xx.cod_azienda and
			anno_registrazione = :ll_anno_registrazione_orig and
			num_registrazione = :ll_num_registrazione_orig ;
end if
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di duplicazione dettagli Fatture.", &
				  exclamation!, ok!)
	rollback;
	return
end if

///// AGGIORNO I DATI DEI DETTAGLI IN BASE ALLE CARATTERISTICHE DI MAGAZZINO ////

string ls_flag_tipo_fat_ven, ls_cod_tipo_det_ven, ls_cod_tipo_movimento, &
		 ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ls_cod_cliente[], &
		 ls_cod_fornitore[], ls_flag_tipo_det_ven
datetime ldt_data_stock[]
long ll_prog_riga_fat_ven, ll_progr_stock[], ll_anno_reg_des_mov, ll_num_reg_des_mov, ll_i2
double ld_quan_fatturata, ld_quan_disponibile

select tab_tipi_fat_ven.flag_tipo_fat_ven  
into   :ls_flag_tipo_fat_ven  
from   tab_tipi_fat_ven  
where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and  
		 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven_nc;

if sqlca.sqlcode = -1 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi fatture.", &
				  exclamation!, ok!)
	return
end if

// vado a controllare il tipo fattura ......
// se è una fattura immediata vado ad aggiornare magazzino ( prodotti, stock ecc..), mentre
// se la fattura è differita la modifica non ha effetto sul magazzino
if ls_flag_tipo_fat_ven = "I" then
	declare cur_det_fat_ven cursor for
		select prog_riga_fat_ven,
				 cod_tipo_det_ven,
				 cod_tipo_movimento,
				 cod_prodotto,
				 cod_deposito,
				 cod_ubicazione,
				 cod_lotto,
				 data_stock,
				 progr_stock,
				 quan_fatturata
		  from det_fat_ven
		 where cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione and
				 num_registrazione = :ll_num_registrazione;
	
	open cur_det_fat_ven;
	fetch cur_det_fat_ven into :ll_prog_riga_fat_ven, 
										:ls_cod_tipo_det_ven,
										:ls_cod_tipo_movimento,
										:ls_cod_prodotto,
										:ls_cod_deposito[1],
										:ls_cod_ubicazione[1],
										:ls_cod_lotto[1],
										:ldt_data_stock[1],
										:ll_progr_stock[1],
										:ld_quan_fatturata;

	do while sqlca.sqlcode = 0
		select flag_tipo_det_ven 
		into   :ls_flag_tipo_det_ven
		from   tab_tipi_det_ven
		where  tab_tipi_det_ven.cod_azienda = :s_cs_xx.cod_azienda and 
				 tab_tipi_det_ven.cod_tipo_det_ven = :ls_cod_tipo_det_ven;

		if sqlca.sqlcode = -1 then
			g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura tipi dettaglio.", exclamation!, ok!)
			close cur_det_fat_ven;
			rollback;
			return
		end if

		if ls_flag_tipo_det_ven = "M" then
			setnull(ls_cod_cliente[1])
			setnull(ls_cod_fornitore[1])

			select stock.giacenza_stock - (stock.quan_assegnata + stock.quan_in_spedizione)  
			into  :ld_quan_disponibile
			from  stock
			where stock.cod_azienda = :s_cs_xx.cod_azienda and
					stock.cod_prodotto = :ls_cod_prodotto and
					stock.cod_deposito = :ls_cod_deposito[1] and
					stock.cod_ubicazione = :ls_cod_ubicazione[1] and
					stock.cod_lotto = :ls_cod_lotto[1] and
					stock.data_stock = :ldt_data_stock[1] and
					stock.prog_stock = :ll_progr_stock[1];
			
			//Donato 23-09-2009 prima di dare il msg controlla il prm az. CMN
			//if ld_quan_fatturata > ld_quan_disponibile then
			if ld_quan_fatturata > ld_quan_disponibile and not wf_cmn() then
				g_mb.messagebox("Attenzione", "Quantità Fattura maggiore della quantità disponibile " + f_double_string(ld_quan_disponibile) + ".", exclamation!, ok!)
				close cur_det_fat_ven;
				rollback;
				return
			end if

			update anag_prodotti  
				set quan_in_spedizione = quan_in_spedizione + :ld_quan_fatturata
			 where anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and  
					 anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento magazzino.",  exclamation!, ok!)
				close cur_det_fat_ven;
				rollback;
				return
			end if

			update stock
				set quan_in_spedizione = quan_in_spedizione + :ld_quan_fatturata
			 where stock.cod_azienda = :s_cs_xx.cod_azienda and  
					 stock.cod_prodotto = :ls_cod_prodotto and
					 stock.cod_deposito = :ls_cod_deposito[1] and
					 stock.cod_ubicazione = :ls_cod_ubicazione[1] and
					 stock.cod_lotto = :ls_cod_lotto[1] and
					 stock.data_stock = :ldt_data_stock[1] and
					 stock.prog_stock = :ll_progr_stock[1];
	
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento stock.", exclamation!, ok!)
				close cur_det_fat_ven;
				rollback;
				return
			end if

			if f_crea_dest_mov_magazzino(ls_cod_tipo_movimento, ls_cod_prodotto, ls_cod_deposito[], ls_cod_ubicazione[], ls_cod_lotto[], ldt_data_stock[], ll_progr_stock[], ls_cod_cliente[], ls_cod_fornitore[], ll_anno_reg_des_mov, ll_num_reg_des_mov) = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di creazione destinazioni movimenti.", &
							  exclamation!, ok!)
				rollback;
				return
			end if

			if f_verifica_dest_mov_mag (ll_anno_reg_des_mov, ll_num_reg_des_mov, ls_cod_tipo_movimento, ls_cod_prodotto) = -1 then
				close cur_det_fat_ven;
				rollback;
				return
			end if

			update det_fat_ven
			set anno_reg_des_mov = :ll_anno_reg_des_mov,
				 num_reg_des_mov = :ll_num_reg_des_mov
			where cod_azienda = :s_cs_xx.cod_azienda and
					anno_registrazione =:ll_anno_registrazione and
					num_registrazione =:ll_num_registrazione and
					prog_riga_fat_ven =:ll_prog_riga_fat_ven;
					
			if sqlca.sqlcode = -1 then
				g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento dettaglio fattura.", &
							  exclamation!, ok!)
				close cur_det_fat_ven;
				rollback;
				return
			end if					

		end if
		
		fetch cur_det_fat_ven into :ll_prog_riga_fat_ven, 
											:ls_cod_tipo_det_ven,
											:ls_cod_tipo_movimento,
											:ls_cod_prodotto,
											:ls_cod_deposito[1],
											:ls_cod_ubicazione[1],
											:ls_cod_lotto[1],
											:ldt_data_stock[1],
											:ll_progr_stock[1],
											:ld_quan_fatturata;

	loop
	close cur_det_fat_ven;
end if

///// FINE AGGIORNAMENTO DATI DEI DETTAGLI IN BASE ALLE CARATTERISTICHE DI MAGAZZINO ////


update con_fat_ven
set    num_registrazione = :ll_num_registrazione
where  cod_azienda = :s_cs_xx.cod_azienda;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di aggiornamento contatore.", &
				  exclamation!, ok!)
	rollback;
	return
end if

luo_calcolo = create uo_calcola_documento_euro

if luo_calcolo.uof_calcola_documento(ll_anno_registrazione,ll_num_registrazione,"fat_ven",ls_messaggio) <> 0 then
	g_mb.messagebox("APICE",ls_messaggio)
	rollback;
	destroy luo_calcolo
	return 
else
	commit;
end if

destroy luo_calcolo

commit;

g_mb.messagebox("Generazione Fattura", "Generata Fattura N. " + string(ll_anno_registrazione) + "/" + string(ll_num_registrazione), information!, ok!)

wf_inserisci_fattura_vendita( 0, ll_anno_registrazione, ll_num_registrazione)

end subroutine

event pc_close;call super::pc_close;

destroy iuo_fido
end event

event pc_setwindow;call super::pc_setwindow;windowobject	lw_oggetti[], lw_vuoto[]


iuo_fido = create uo_fido_cliente


is_codice_filtro = "FTV"
tab_dettaglio.det_1.dw_tes_fat_ven_det_1.set_dw_key("cod_azienda")
tab_dettaglio.det_1.dw_tes_fat_ven_det_1.set_dw_options(sqlca, &
                                    pcca.null_object, &
                                    c_noretrieveonopen, &
                                    c_default)
												
tab_dettaglio.det_2.dw_tes_fat_ven_det_2.set_dw_options(sqlca, &
                                    tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
tab_dettaglio.det_3.dw_tes_fat_ven_det_3.set_dw_options(sqlca, &
                                    tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
tab_dettaglio.det_4.dw_tes_fat_ven_det_4.set_dw_options(sqlca, &
                                    tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)
tab_dettaglio.det_5.dw_tes_fat_ven_det_5.set_dw_options(sqlca, &
                                    tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
                                    c_sharedata + c_scrollparent, &
                                    c_default)

il_livello = 0



iuo_dw_main = tab_dettaglio.det_1.dw_tes_fat_ven_det_1
ib_prima_riga = false
tab_ricerca.tabpage_1.dw_notifiche.ib_dw_report=true 


//Donato 04/06/2014 SR Migliorie_Spese_trasporto: Funzione 2
//pulsante ricalcolo spese trasporto visibile solo se abilitate dal parametro aziendale relativo
guo_functions.uof_get_parametro_azienda("SSP", ib_spese_trasporto)
if isnull(ib_spese_trasporto) then ib_spese_trasporto = false
//cb_ricalcolo_spese_trasp.visible = ib_spese_trasporto
//--------------------------------------


if s_cs_xx.parametri.parametro_i_1 > 0 and s_cs_xx.parametri.parametro_ul_1 > 0 then
	tab_ricerca.ricerca.dw_ricerca.setitem(1, "anno_registrazione", s_cs_xx.parametri.parametro_i_1)
	tab_ricerca.ricerca.dw_ricerca.setitem(1, "num_registrazione", s_cs_xx.parametri.parametro_ul_1)
	setnull(s_cs_xx.parametri.parametro_i_1)
	setnull(s_cs_xx.parametri.parametro_ul_1)
end if

tab_ricerca.ricerca.dw_ricerca.setitem(1, "anno_registrazione", f_anno_esercizio())

//tab_ricerca.tabpage_1.dw_notifiche.Object.b_download_notifica.FileName = s_cs_xx.volume + s_cs_xx.risorse + "12.5\fatel_read_notifica.png"

event post ue_leggi_notifiche()


end event

on w_tes_fat_ven.create
int iCurrent
call super::create
end on

on w_tes_fat_ven.destroy
call super::destroy
end on

event pc_delete;call super::pc_delete;string ls_tabella, ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven, ls_messaggio
long ll_anno_registrazione, ll_num_registrazione
integer li_i 


for li_i = 1 to tab_dettaglio.det_1.dw_tes_fat_ven_det_1.deletedcount()

	if f_verifica_fat_ven(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(li_i, "anno_registrazione", delete!, true), &
								 tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(li_i, "num_registrazione" , delete!, true), &
								 ls_messaggio) = -1 then
		g_mb.messagebox("APICE", ls_messaggio, stopsign!)
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.set_dw_view(c_ignorechanges)
		pcca.error = c_fatal
		return
	end if	

next
end event

event pc_modify;call super::pc_modify;string ls_tabella, ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven, ls_messaggio
long ll_anno_registrazione, ll_num_registrazione


if f_verifica_fat_ven(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "anno_registrazione"), &
							 tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemnumber(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "num_registrazione"), &
							 ls_messaggio) = -1 then
	g_mb.messagebox("APICE", ls_messaggio, stopsign!)
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.set_dw_view(c_ignorechanges)
	pcca.error = c_fatal
	return
end if	

end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
					"cod_documento", &
					sqlca, &
                       "tab_documenti", &
							  "cod_documento", &
							  "des_documento", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
				"cod_valuta", &
				sqlca, &
                       "tab_valute", &
							  "cod_valuta", &
							  "des_valuta", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
					"cod_deposito", &
					sqlca, &
                       "anag_depositi", &
							  "cod_deposito", &
							  "des_deposito", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
					"cod_tipo_listino", &
					sqlca, &
                       "tab_tipi_listini_prodotti", &
							  "cod_tipo_listino_prodotto", &
							  "des_tipo_listino_prodotto", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
				"cod_agente_1", &
				sqlca, &
                       "anag_agenti", &
							  "cod_agente", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
				"cod_agente_2", &
				sqlca, &
                       "anag_agenti", &
							  "cod_agente", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
					"cod_tipo_fat_ven", &
					sqlca, &
                       "tab_tipi_fat_ven", &
				"cod_tipo_fat_ven", &
				 "des_tipo_fat_ven", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
					"cod_causale", &
					sqlca, &
                       "tab_causali_trasp", &
							  "cod_causale", &
							  "des_causale", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_ricerca.ricerca.dw_ricerca, &
					"cod_tipo_pagamento", &
					sqlca, &
                       "tab_pagamenti", &
							  "cod_pagamento", &
							  "des_pagamento", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' ")		
//##########################################################################################################
string ls_select_operatori
if s_cs_xx.cod_utente <> "CS_SYSTEM" then
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + ")) and cod_operatore in (select cod_operatore from tab_operatori_utenti where cod_utente = '" + &
						  s_cs_xx.cod_utente + "')"
else
	ls_select_operatori = "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + &
					  	s_cs_xx.db_funzioni.oggi + "))"
end if

f_po_loaddddw_dw(tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
                 "cod_operatore", &
                 sqlca, &
                 "tab_operatori", &
                 "cod_operatore", &
                 "des_operatore", &
					  ls_select_operatori)
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
                 "cod_tipo_fat_ven", &
                 sqlca, &
                 "tab_tipi_fat_ven", &
                 "cod_tipo_fat_ven", &
                 "des_tipo_fat_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
//f_po_loaddddw_dw(tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
//                 "cod_cliente", &
//                 sqlca, &
//                 "anag_clienti", &
//                 "cod_cliente", &
//                 "rag_soc_1", &
//                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
                 "cod_deposito", &
                 sqlca, &
                 "anag_depositi", &
                 "cod_deposito", &
                 "des_deposito", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
                 "cod_tipo_listino_prodotto", &
                 sqlca, &
                 "tab_tipi_listini_prodotti", &
                 "cod_tipo_listino_prodotto", &
                 "des_tipo_listino_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_vendita_acquisto = 'V' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
                 "cod_pagamento", &
                 sqlca, &
                 "tab_pagamenti", &
                 "cod_pagamento", &
                 "des_pagamento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
                 "cod_agente_1", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
                 "cod_agente_2", &
                 sqlca, &
                 "anag_agenti", &
                 "cod_agente", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
                 "cod_banca", &
                 sqlca, &
                 "anag_banche", &
                 "cod_banca", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
                 "cod_banca_clien_for", &
                 sqlca, &
                 "anag_banche_clien_for", &
                 "cod_banca_clien_for", &
                 "des_banca", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_1.dw_tes_fat_ven_det_1, &
                 "cod_documento", &
                 sqlca, &
                 "tab_documenti", &
                 "cod_documento", &
                 "des_documento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")					  
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_tes_fat_ven_det_3, &
                 "cod_imballo", &
                 sqlca, &
                 "tab_imballi", &
                 "cod_imballo", &
                 "des_imballo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_tes_fat_ven_det_3, &
                 "cod_vettore", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_tes_fat_ven_det_3, &
                 "cod_inoltro", &
                 sqlca, &
                 "anag_vettori", &
                 "cod_vettore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_tes_fat_ven_det_3, &
                 "cod_mezzo", &
                 sqlca, &
                 "tab_mezzi", &
                 "cod_mezzo", &
                 "des_mezzo", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_tes_fat_ven_det_3, &
                 "cod_porto", &
                 sqlca, &
                 "tab_porti", &
                 "cod_porto", &
                 "des_porto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_tes_fat_ven_det_3, &
                 "cod_resa", &
                 sqlca, &
                 "tab_rese", &
                 "cod_resa", &
                 "des_resa", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_tes_fat_ven_det_3, &
	"cod_natura_intra", &
	sqlca, &
	"tab_natura_intra", &
	"cod_natura_intra", &
	"descrizione", &
	"cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(tab_dettaglio.det_3.dw_tes_fat_ven_det_3, &
                 "cod_causale", &
                 sqlca, &
                 "tab_causali_trasp", &
                 "cod_causale", &
                 "des_causale", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
					  
end event

type tab_dettaglio from w_cs_xx_treeview`tab_dettaglio within w_tes_fat_ven
integer x = 1760
integer width = 2789
integer height = 2480
det_2 det_2
det_3 det_3
det_4 det_4
det_5 det_5
end type

on tab_dettaglio.create
this.det_2=create det_2
this.det_3=create det_3
this.det_4=create det_4
this.det_5=create det_5
call super::create
this.Control[]={this.det_1,&
this.det_2,&
this.det_3,&
this.det_4,&
this.det_5}
end on

on tab_dettaglio.destroy
call super::destroy
destroy(this.det_2)
destroy(this.det_3)
destroy(this.det_4)
destroy(this.det_5)
end on

type det_1 from w_cs_xx_treeview`det_1 within tab_dettaglio
integer width = 2752
integer height = 2356
string text = "Testata"
dw_tes_fat_ven_det_1 dw_tes_fat_ven_det_1
end type

on det_1.create
this.dw_tes_fat_ven_det_1=create dw_tes_fat_ven_det_1
int iCurrent
call super::create
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_fat_ven_det_1
end on

on det_1.destroy
call super::destroy
destroy(this.dw_tes_fat_ven_det_1)
end on

type tab_ricerca from w_cs_xx_treeview`tab_ricerca within w_tes_fat_ven
integer width = 1733
integer height = 2484
tabpage_1 tabpage_1
end type

on tab_ricerca.create
this.tabpage_1=create tabpage_1
call super::create
this.Control[]={this.ricerca,&
this.selezione,&
this.tabpage_1}
end on

on tab_ricerca.destroy
call super::destroy
destroy(this.tabpage_1)
end on

type ricerca from w_cs_xx_treeview`ricerca within tab_ricerca
integer width = 1696
integer height = 2360
end type

type dw_ricerca from w_cs_xx_treeview`dw_ricerca within ricerca
event ue_reset ( )
integer width = 1682
integer height = 2336
string dataobject = "d_tes_fat_ven_ricerca_tv"
end type

event dw_ricerca::ue_reset();tab_ricerca.ricerca.dw_ricerca.resetupdate()
end event

event dw_ricerca::buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(tab_ricerca.ricerca.dw_ricerca,"cod_cliente")
		
	case "b_ricerca_contatto"
		guo_ricerca.uof_ricerca_contatto(tab_ricerca.ricerca.dw_ricerca,"cod_contatto")
		
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(tab_ricerca.ricerca.dw_ricerca,"cod_prodotto")
end choose
end event

event dw_ricerca::itemchanged;call super::itemchanged;choose case this.getcolumnname()
	case "cod_cliente"
		
		this.setitem(row,"cod_des_cliente","")
		f_po_loaddddw_dw(this, &
							  "cod_des_cliente", &
							  sqlca, &
							  "anag_des_clienti", &
							  "cod_des_cliente", &
							  "rag_soc_1", &
							  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + data + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

	case "flag_tipo_livello_1"
		setitem(row, "flag_tipo_livello_2", "N")
		setitem(row, "flag_tipo_livello_3", "N")
		settaborder("flag_tipo_livello_2", 0)
		settaborder("flag_tipo_livello_3", 0)
		if data <> "N" then
			settaborder("flag_tipo_livello_2", 280)
		end if
			
	case "flag_tipo_livello_2"
		setitem(row, "flag_tipo_livello_3", "N")
		settaborder("flag_tipo_livello_3", 0)
		if data <> "N" then
			settaborder("flag_tipo_livello_3", 290)
		end if								  
end choose

end event

type selezione from w_cs_xx_treeview`selezione within tab_ricerca
integer width = 1696
integer height = 2360
end type

type tv_selezione from w_cs_xx_treeview`tv_selezione within selezione
integer width = 1669
integer height = 2320
end type

event tv_selezione::selectionchanged;call super::selectionchanged;treeviewitem ltvi_item

if AncestorReturnValue < 0 then return

tab_ricerca.selezione.tv_selezione.getitem(newhandle, ltvi_item)

istr_data = ltvi_item.data
	
tab_dettaglio.det_1.dw_tes_fat_ven_det_1.change_dw_current()
getwindow().triggerevent("pc_retrieve")

end event

event tv_selezione::itempopulate;call super::itempopulate;treeviewitem ltvi_item
str_treeview lstr_data

if AncestorReturnValue < 0 then return

getitem(handle, ltvi_item)

lstr_data = ltvi_item.data

if wf_leggi_livello(handle, lstr_data.livello + 1) < 1 then
	ltvi_item.children = false
	setitem(handle, ltvi_item)
end if

end event

event tv_selezione::key;call super::key;string ls_messaggio, ls_flag_movimenti, ls_flag_supervisore, ls_stato_flag

long	ll_handle

treeviewitem ltv_item
str_treeview lstr_data

uo_calcola_documento_euro luo_calcola_documento_euro

if key = KeyX! and keyflags=3 then
	
	if s_cs_xx.cod_utente <> "CS_SYSTEM" then
		select flag_supervisore
		into	:ls_flag_supervisore
		from	utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode <> 0 then ls_flag_supervisore = "N"
	
		if ls_flag_supervisore = "N" then
			g_mb.warning("Funzione utilizzabile solo da utenti Supervisori.")
			return
		end if
	end if
	
	if g_mb.confirm("Ricalcolare tutte le fatture nella lista?") then

		ll_handle = tab_ricerca.selezione.tv_selezione.FindItem(CurrentTreeItem!, 0)
		
		setpointer(HourGlass!)
		
		do while true
			
			if ll_handle <= 0 or isnull(ll_handle) then exit
			tab_ricerca.selezione.tv_selezione.getitem(ll_handle, ltv_item)
			
			lstr_data = ltv_item.data
			
			if lstr_data.decimale[1] > 0 and lstr_data.decimale[2] > 0 then
			
				luo_calcola_documento_euro = create uo_calcola_documento_euro
				
				luo_calcola_documento_euro.ib_salta_esposiz_cliente = true
				
				w_cs_xx_mdi.setmicrohelp("Calcolo fattura " + string(lstr_data.decimale[1]) + "-"  + string(lstr_data.decimale[2]))
				Yield()
				
				if luo_calcola_documento_euro.uof_calcola_documento(lstr_data.decimale[1], lstr_data.decimale[2],"fat_ven",ls_messaggio) <> 0 then
					if not isnull(ls_messaggio) and ls_messaggio<> "" then g_mb.messagebox("APICE",ls_messaggio)
					rollback;
				else
					commit;
				end if
				destroy luo_calcola_documento_euro
			end if
			
			ll_handle = tab_ricerca.selezione.tv_selezione.finditem( NextTreeItem! ,ll_handle )

		loop
		w_cs_xx_mdi.setmicrohelp("Elaborazione Eseguita Correttamente; calcolo terminato!")
		setpointer(Arrow!)
		
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.change_dw_current()
		getwindow().triggerevent("pc_retrieve")

	end if

end if

if key = KeyM! and keyflags=2 then
	
	if s_cs_xx.cod_utente <> "CS_SYSTEM" then
		select flag_supervisore
		into	:ls_flag_supervisore
		from	utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode <> 0 then ls_flag_supervisore = "N"
	
		if ls_flag_supervisore = "N" then
			g_mb.warning("Funzione utilizzabile solo da utenti Supervisori.")
			return
		end if
	end if
	
	ll_handle = tab_ricerca.selezione.tv_selezione.FindItem(CurrentTreeItem!, 0)
	
	if ll_handle <= 0 or isnull(ll_handle) then return
	tab_ricerca.selezione.tv_selezione.getitem(ll_handle, ltv_item)
	lstr_data = ltv_item.data
	
	if lstr_data.decimale[1] > 0 and lstr_data.decimale[2] > 0 then
		select flag_movimenti
		into	:ls_flag_movimenti
		from	tes_fat_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lstr_data.decimale[1] and
				num_registrazione = :lstr_data.decimale[2];
		if sqlca.sqlcode <> 0 then
			g_mb.error("Fattura selezionata non trovata")
			return
		end if
		
		if ls_flag_movimenti = "S" then
			if g_mb.confirm("Sei sicuro di voler togliere check CONFERMATA dalla fattura? L'operazione sarà registrata in un LOG") then
				ls_stato_flag="N"
			else
				return
			end if
		else
			if g_mb.confirm("Sei sicuro di voler applicare il FLAG CONFERMATA alla fattura? L'operazione sarà registrata in un LOG") then
				ls_stato_flag="S"
			else
				return
			end if
		end if		
		
		f_scrivi_log( g_str.format( "FLAG CONFERMA portato allo stato '$3' nella FATTURA num int. [$1 / $2]", lstr_data.decimale[1], lstr_data.decimale[2], ls_stato_flag))
		
		
		update tes_fat_ven
		set flag_movimenti = :ls_stato_flag
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lstr_data.decimale[1] and
				num_registrazione = :lstr_data.decimale[2];
		if sqlca.sqlcode = 0 then
			g_mb.show("'Flag Fattura Confermata' rimosso con successo!")
			tab_dettaglio.det_1.dw_tes_fat_ven_det_1.change_dw_current()
			getwindow().triggerevent("pc_retrieve")
			commit;
			// LOG
		else
			g_mb.error("Errore SQL in modifica 'Flag Fattura Confermata'")
			rollback;
			return
		end if
	end if
end if

if key = KeyI! and keyflags=2 then
	
	if s_cs_xx.cod_utente <> "CS_SYSTEM" then
		select flag_supervisore
		into	:ls_flag_supervisore
		from	utenti
		where cod_utente = :s_cs_xx.cod_utente;
		
		if sqlca.sqlcode <> 0 then ls_flag_supervisore = "N"
	
		if ls_flag_supervisore = "N" then
			g_mb.warning("Funzione utilizzabile solo da utenti Supervisori.")
			return
		end if
	end if
	
	ll_handle = tab_ricerca.selezione.tv_selezione.FindItem(CurrentTreeItem!, 0)
	
	if ll_handle <= 0 or isnull(ll_handle) then return
	tab_ricerca.selezione.tv_selezione.getitem(ll_handle, ltv_item)
	lstr_data = ltv_item.data
	
	if lstr_data.decimale[1] > 0 and lstr_data.decimale[2] > 0 then
		select flag_contabilita
		into	:ls_flag_movimenti
		from	tes_fat_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lstr_data.decimale[1] and
				num_registrazione = :lstr_data.decimale[2];
		if sqlca.sqlcode <> 0 then
			g_mb.error("Fattura selezionata non trovata")
			return
		end if
		
		if ls_flag_movimenti = "S" then
			if g_mb.confirm("Sei sicuro di voler togliere check CONTABILIZZATA dalla fattura? L'operazione sarà registrata in un LOG") then
				ls_stato_flag="N"
			else
				return
			end if
		else
			if g_mb.confirm("Sei sicuro di voler applicare il FLAG CONTABILIZZATA alla fattura? L'operazione sarà registrata in un LOG") then
				ls_stato_flag="S"
			else
				return
			end if
		end if		
		
		f_scrivi_log( g_str.format( "FLAG CONTABILIZZATA portato allo stato '$3' nella FATTURA num int. [$1 / $2]", lstr_data.decimale[1], lstr_data.decimale[2], ls_stato_flag))
		
		
		update tes_fat_ven
		set flag_contabilita = :ls_stato_flag
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :lstr_data.decimale[1] and
				num_registrazione = :lstr_data.decimale[2];
		if sqlca.sqlcode = 0 then
			g_mb.show("'Flag Fattura Contabilizzata' rimosso con successo!")
			tab_dettaglio.det_1.dw_tes_fat_ven_det_1.change_dw_current()
			getwindow().triggerevent("pc_retrieve")
			commit;
			// LOG
		else
			g_mb.error("Errore SQL in modifica 'Flag Fattura Contabilizzata'")
			rollback;
			return
		end if
	end if
end if
end event

event tv_selezione::rightclicked;call super::rightclicked;long ll_row
treeviewitem ltvi_item
m_tes_fat_ven_std lm_menu

if AncestorReturnValue < 0 then return

pcca.window_current = getwindow()
ll_row = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow()

tab_ricerca.selezione.tv_selezione.getitem(handle, ltvi_item)

istr_data = ltvi_item.data

tab_dettaglio.det_1.dw_tes_fat_ven_det_1.change_dw_current()
getwindow().triggerevent("pc_retrieve")

if istr_data.tipo_livello = "N" then
	
	lm_menu = create m_tes_fat_ven_std
	
	lm_menu.m_conferma.visible = ib_menu_conferma
	lm_menu.m_blocca.enabled = (tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(ll_row, "flag_blocco") = "N")
	lm_menu.m_sblocca.enabled = (tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(ll_row, "flag_blocco") = "S")
	lm_menu.m_contabilizza.enabled = (tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(ll_row, "flag_movimenti") = "S") and (tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(ll_row, "flag_calcolo") = "S") and (tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(ll_row, "flag_contabilita") = "N")
	//ricalcolo spese trasporto
	lm_menu.m_ricacolaspesetrasp.visible = ib_spese_trasporto
	
	lm_menu.popmenu(w_cs_xx_mdi.pointerx(),w_cs_xx_mdi.pointery())
	
	destroy lm_menu
	
end if
end event

type dw_tes_fat_ven_det_1 from uo_cs_xx_dw within det_1
integer width = 2720
integer height = 2168
integer taborder = 30
string dataobject = "d_tes_fat_ven_det_1_tv"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;
choose case dwo.name
	case "b_ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(tab_dettaglio.det_1.dw_tes_fat_ven_det_1,"cod_cliente")
	case "b_ricerca_banca_for"
		guo_ricerca.uof_ricerca_banca_clifor(tab_dettaglio.det_1.dw_tes_fat_ven_det_1,"cod_banca_clien_for")
	case "b_iva"
		window_open_parm(w_iva_fat_ven, -1, this)
	case "b_contropartite"
		window_open_parm(w_cont_fat_ven, -1, this)
	case "b_scadenze"
		window_open_parm(w_scad_fat_ven, -1, this)
end choose
end event

event itemchanged;call super::itemchanged;if i_extendmode then
   string ls_null, ls_cod_cliente, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, &
          ls_frazione, ls_cap, ls_localita, ls_provincia, ls_telefono, &
          ls_fax, ls_telex, ls_partita_iva, ls_cod_fiscale, ls_cod_deposito, &
          ls_cod_valuta, ls_cod_tipo_listino_prodotto, ls_cod_pagamento, &
          ls_cod_agente_1, ls_cod_agente_2, ls_cod_banca_clien_for, ls_cod_banca,&
          ls_cod_imballo, ls_cod_vettore, ls_cod_inoltro, ls_cod_mezzo, &
          ls_cod_porto, ls_cod_resa, ls_flag_fuori_fido, ls_cod_causale, &
		 ls_nota_testata, ls_nota_piede, ls_nota_video, ls_messaggio, &
		 ls_cod_fornitore, ls_flag_tipo_fat_ven, ls_flag_tipo_fat_ven_com, ls_cod_tipo_fat_ven_com, &
		 ls_cod_destinazione, ls_flag_dest_merce_fat, ls_flag_blocco, ls_flag_bol_fat, ls_cod_tipo_fat_ven
   double ld_sconto
   datetime ldt_data_registrazione, ldt_data_blocco
	long ll_riga_dw_2
	uo_cerca_destinazione iuo_cerca_destinazione
	integer	li_ret

   setnull(ls_null)
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_banca_for.enabled = true
   choose case i_colname
			
      case "cod_tipo_fat_ven"
         select cod_causale, flag_tipo_fat_ven
			into   :ls_cod_causale, :ls_flag_tipo_fat_ven
			from   tab_tipi_fat_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_tipo_fat_ven = :i_coltext;
			if sqlca.sqlcode = 0 then
				this.setitem(i_rownbr,"cod_causale",ls_cod_causale)
			else
				g_mb.messagebox("APICE","Errore in ricerca tipo fattuta in tabella tab_tipi_fat_ven.~r~nDettaglio errore:"+sqlca.sqlerrtext)
				return 1
			end if

			iuo_cerca_destinazione = create uo_cerca_destinazione
			iuo_cerca_destinazione.wf_protezione(tab_dettaglio.det_2.dw_tes_fat_ven_det_2,ls_flag_tipo_fat_ven,tab_dettaglio.det_2.dw_tes_fat_ven_det_2.getrow())
			destroy iuo_cerca_destinazione

			wf_tipo_fattura(ls_flag_tipo_fat_ven, false)					
			
			
      case "data_registrazione"
         ls_cod_valuta = this.getitemstring(i_rownbr, "cod_valuta")
         f_cambio_ven(ls_cod_valuta, datetime(date(i_coltext)))
			
			
      case "cod_cliente"
			ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
			
			li_ret = iuo_fido.uof_get_blocco_cliente( i_coltext, ldt_data_registrazione, is_cod_parametro_blocco, ls_messaggio)
			if li_ret=2 then
				//blocco
				g_mb.error(ls_messaggio)
				this.setitem(i_rownbr, i_colname, ls_null)
				this.SetColumn ( i_colname )
				return 1
			elseif li_ret=1 then
				//solo warning
				g_mb.warning(ls_messaggio)
			end if
			
         select anag_clienti.rag_soc_1,
				anag_clienti.rag_soc_2,
				anag_clienti.indirizzo,
				anag_clienti.frazione,
				anag_clienti.cap,
				anag_clienti.localita,
				anag_clienti.provincia,
				anag_clienti.telefono,
				anag_clienti.fax,
				anag_clienti.telex,
				anag_clienti.partita_iva,
				anag_clienti.cod_fiscale,
				anag_clienti.cod_deposito,
				anag_clienti.cod_valuta,
				anag_clienti.cod_tipo_listino_prodotto,
				anag_clienti.cod_pagamento,
				anag_clienti.sconto,
				anag_clienti.cod_agente_1,
				anag_clienti.cod_agente_2,
				anag_clienti.cod_banca_clien_for,
				anag_clienti.cod_banca,
				anag_clienti.cod_imballo,
				anag_clienti.cod_vettore,
				anag_clienti.cod_inoltro,
				anag_clienti.cod_mezzo,
				anag_clienti.cod_porto,
				anag_clienti.cod_resa,
				anag_clienti.flag_fuori_fido,
				anag_clienti.flag_blocco,
				anag_clienti.data_blocco,
				flag_bol_fat
         into   :ls_rag_soc_1,    
                :ls_rag_soc_2,
                :ls_indirizzo,    
                :ls_frazione,
                :ls_cap,
                :ls_localita,
                :ls_provincia,
                :ls_telefono,
                :ls_fax,
                :ls_telex,
                :ls_partita_iva,
                :ls_cod_fiscale,
                :ls_cod_deposito,
                :ls_cod_valuta,
                :ls_cod_tipo_listino_prodotto,
                :ls_cod_pagamento,
                :ld_sconto,
                :ls_cod_agente_1,
                :ls_cod_agente_2,
                :ls_cod_banca_clien_for,
                :ls_cod_banca,
                :ls_cod_imballo,
                :ls_cod_vettore,
                :ls_cod_inoltro,
                :ls_cod_mezzo,
                :ls_cod_porto,
                :ls_cod_resa,
                :ls_flag_fuori_fido,
					 :ls_flag_blocco,
					 :ldt_data_blocco,
					 :ls_flag_bol_fat
         from   anag_clienti
         where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_clienti.cod_cliente = :i_coltext;
 
         if sqlca.sqlcode = 0 then 
				
			if ldt_data_blocco < ldt_data_registrazione and ls_flag_blocco = "S" then
				g_mb.messagebox("Apice","Attenzione cliente bloccato!")
				this.setitem(i_rownbr, i_colname, ls_null)
				this.SetColumn ( i_colname )
				return 1
			end if
			
			if ls_flag_bol_fat = "B" then
				g_mb.messagebox("APICE","ATTENZIONE: il documento preferenziale per questo cliente è BOLLA")
			end if
				
            this.setitem(i_rownbr, "cod_deposito", ls_cod_deposito)
            this.setitem(i_rownbr, "cod_valuta", ls_cod_valuta)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
            this.setitem(i_rownbr, "cod_pagamento", ls_cod_pagamento)
            this.setitem(i_rownbr, "sconto", ld_sconto)
            this.setitem(i_rownbr, "cod_agente_1", ls_cod_agente_1)
            this.setitem(i_rownbr, "cod_agente_2", ls_cod_agente_2)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_cod_banca_clien_for)
            this.setitem(i_rownbr, "cod_banca", ls_cod_banca)
            this.setitem(i_rownbr, "flag_fuori_fido", ls_flag_fuori_fido)
            if not isnull(i_coltext) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text='" + i_coltext + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text=''")
            end if
            if not isnull(ls_rag_soc_1) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
            end if
            if not isnull(ls_rag_soc_2) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
            end if
            if not isnull(ls_indirizzo) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
            end if
            if not isnull(ls_frazione) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text='" + ls_frazione + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
            end if
            if not isnull(ls_cap) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text='" + ls_cap + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
            end if
            if not isnull(ls_localita) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text='" + ls_localita + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
            end if
            if not isnull(ls_provincia) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text='" + ls_provincia + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
            end if
            if not isnull(ls_telefono) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text='" + ls_telefono + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
            end if
            if not isnull(ls_fax) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text='" + ls_fax + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
            end if
            if not isnull(ls_telex) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text='" + ls_telex + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
            end if
            if not isnull(ls_partita_iva) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text=''")
            end if
            if not isnull(ls_cod_fiscale) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text=''")
            end if
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_imballo", ls_cod_imballo)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_vettore", ls_cod_vettore)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_inoltro", ls_cod_inoltro)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_mezzo", ls_cod_mezzo)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_porto", ls_cod_porto)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_resa", ls_cod_resa)
   
			ls_cod_tipo_fat_ven_com = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "cod_tipo_fat_ven")

			select flag_tipo_fat_ven
			into   :ls_flag_tipo_fat_ven_com
			from   tab_tipi_fat_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven_com;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in ricerca tipo fattuta in tabella tab_tipi_fat_ven.~r~nDettaglio errore:"+sqlca.sqlerrtext)
				return 1
			end if			

			if ls_flag_tipo_fat_ven_com = "I" then
				ls_flag_dest_merce_fat = "S"
			else 
				ls_flag_dest_merce_fat = "N"
			end if	

	
            f_po_loaddddw_dw(this, &
                             "cod_des_cliente", &
                             sqlca, &
                             "anag_des_clienti", &
                             "cod_des_cliente", &
                             "rag_soc_1", &
                             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + i_coltext + "' and flag_dest_merce_fat = '" + ls_flag_dest_merce_fat + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
    
            this.setitem(i_rownbr, "cod_des_cliente", ls_null)
            ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
            f_cambio_ven(ls_cod_valuta, ldt_data_registrazione)
				
			ls_cod_destinazione = this.getitemstring(i_rownbr, "cod_des_cliente")
			ll_riga_dw_2 = tab_dettaglio.det_2.dw_tes_fat_ven_det_2.getrow()
			iuo_cerca_destinazione = create uo_cerca_destinazione
			if iuo_cerca_destinazione.wf_cerca_destinazione(tab_dettaglio.det_2.dw_tes_fat_ven_det_2, ll_riga_dw_2, i_coltext, "C", ls_cod_destinazione, ls_flag_tipo_fat_ven_com, ls_messaggio) = -1 then
				g_mb.messagebox("APICE","Errore in ricerca destinazione diversa. ~r~nDettaglio: " +ls_messaggio)
			end if			
			destroy iuo_cerca_destinazione

         else
            this.setitem(i_rownbr, "cod_deposito", ls_null)
            this.setitem(i_rownbr, "cod_valuta", ls_null)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_null)
            this.setitem(i_rownbr, "cod_pagamento", ls_null)
            this.setitem(i_rownbr, "sconto", ls_null)
            this.setitem(i_rownbr, "cod_agente_1", ls_null)
            this.setitem(i_rownbr, "cod_agente_2", ls_null)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_null)
            this.setitem(i_rownbr, "cod_banca", ls_null)
            this.setitem(i_rownbr, "flag_fuori_fido", ls_null)
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_contatto.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
				
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1_dest_fat.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2_dest_fat.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo_dest_fat.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione_dest_fat.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap_dest_fat.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita_dest_fat.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia_dest_fat.text=''")				
				
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text=''")
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_imballo", ls_null)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_vettore", ls_null)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_inoltro", ls_null)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_mezzo", ls_null)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_porto", ls_null)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_resa", ls_null)
            this.setitem(i_rownbr, "cod_des_cliente", ls_null)
         end if 
			
			setnull(ls_null)
			ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
			ls_cod_tipo_fat_ven = getitemstring(getrow(), "cod_tipo_fat_ven")
			
			if guo_functions.uof_get_note_fisse(i_coltext, ls_null, ls_null, "FAT_VEN", ls_cod_tipo_fat_ven, ldt_data_registrazione,  ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
				g_mb.error(ls_nota_testata)
			else
				if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
			end if
		case "cod_des_cliente"
//------------------------------------ Modifica Nicola -----------------------------------
			ls_cod_tipo_fat_ven_com = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(), "cod_tipo_fat_ven")

         select flag_tipo_fat_ven
			into   :ls_flag_tipo_fat_ven_com
			from   tab_tipi_fat_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       cod_tipo_fat_ven = :ls_cod_tipo_fat_ven_com;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in ricerca tipo fattuta in tabella tab_tipi_fat_ven.~r~nDettaglio errore:"+sqlca.sqlerrtext)
				return 1
			end if			
			
			ls_cod_cliente = this.getitemstring(i_rownbr, "cod_cliente")
			iuo_cerca_destinazione = create uo_cerca_destinazione
			ll_riga_dw_2 = tab_dettaglio.det_2.dw_tes_fat_ven_det_2.getrow()
			if iuo_cerca_destinazione.wf_cerca_destinazione(tab_dettaglio.det_2.dw_tes_fat_ven_det_2, ll_riga_dw_2, ls_cod_cliente, "C", i_coltext, ls_flag_tipo_fat_ven_com, ls_messaggio) = -1 then
				g_mb.messagebox("APICE","Errore in ricerca destinazione diversa. ~r~nDettaglio: " +ls_messaggio)
			end if			
			destroy iuo_cerca_destinazione
//------------------------------------- Fine modifica ------------------------------------			
		case "cod_des_fornitore"
         ls_cod_fornitore = this.getitemstring(i_rownbr, "cod_fornitore")
			if f_cerca_destinazione(tab_dettaglio.det_2.dw_tes_fat_ven_det_2, i_rownbr, ls_cod_fornitore, "F", i_coltext, ls_messaggio) = -1 then
				g_mb.messagebox("APICE","Errore in ricerca destinazione diversa. ~r~nDettaglio: " +ls_messaggio)
			end if
      case "cod_fornitore"
         select anag_fornitori.rag_soc_1,
                anag_fornitori.rag_soc_2,
                anag_fornitori.indirizzo,
                anag_fornitori.frazione,
                anag_fornitori.cap,
                anag_fornitori.localita,
                anag_fornitori.provincia,
                anag_fornitori.telefono,
                anag_fornitori.fax,
                anag_fornitori.telex,
                anag_fornitori.partita_iva,
                anag_fornitori.cod_fiscale,
                anag_fornitori.cod_deposito,
                anag_fornitori.cod_valuta,
                anag_fornitori.cod_tipo_listino_prodotto,
                anag_fornitori.cod_pagamento,
                anag_fornitori.sconto,
                anag_fornitori.cod_banca_clien_for,
                anag_fornitori.cod_banca,
                anag_fornitori.cod_imballo,
                anag_fornitori.cod_vettore,
                anag_fornitori.cod_inoltro,
                anag_fornitori.cod_mezzo,
                anag_fornitori.cod_porto,
                anag_fornitori.cod_resa
         into   :ls_rag_soc_1,    
                :ls_rag_soc_2,
                :ls_indirizzo,    
                :ls_frazione,
                :ls_cap,
                :ls_localita,
                :ls_provincia,
                :ls_telefono,
                :ls_fax,
                :ls_telex,
                :ls_partita_iva,
                :ls_cod_fiscale,
                :ls_cod_deposito,
                :ls_cod_valuta,
                :ls_cod_tipo_listino_prodotto,
                :ls_cod_pagamento,
                :ld_sconto,
                :ls_cod_banca_clien_for,
                :ls_cod_banca,
                :ls_cod_imballo,
                :ls_cod_vettore,
                :ls_cod_inoltro,
                :ls_cod_mezzo,
                :ls_cod_porto,
                :ls_cod_resa
         from   anag_fornitori
         where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_fornitori.cod_fornitore = :i_coltext;
 
         if sqlca.sqlcode = 0 then 
            this.setitem(i_rownbr, "cod_deposito", ls_cod_deposito)
            this.setitem(i_rownbr, "cod_valuta", ls_cod_valuta)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_cod_tipo_listino_prodotto)
            this.setitem(i_rownbr, "cod_pagamento", ls_cod_pagamento)
            this.setitem(i_rownbr, "sconto", ld_sconto)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_cod_banca_clien_for)
            this.setitem(i_rownbr, "cod_banca", ls_cod_banca)
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_label_clien_for_dep_1.text='Fornitore'")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_label_clien_for_dep_2.text='Fornitore:'")
            if not isnull(i_coltext) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text='" + i_coltext + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text=''")
            end if
            if not isnull(ls_rag_soc_1) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
            end if
            if not isnull(ls_rag_soc_2) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
            end if
            if not isnull(ls_indirizzo) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
            end if
            if not isnull(ls_frazione) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text='" + ls_frazione + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
            end if
            if not isnull(ls_cap) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text='" + ls_cap + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
            end if
            if not isnull(ls_localita) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text='" + ls_localita + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
            end if
            if not isnull(ls_provincia) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text='" + ls_provincia + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
            end if
            if not isnull(ls_telefono) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text='" + ls_telefono + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
            end if
            if not isnull(ls_fax) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text='" + ls_fax + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
            end if
            if not isnull(ls_telex) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text='" + ls_telex + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
            end if
            if not isnull(ls_partita_iva) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text=''")
            end if
            if not isnull(ls_cod_fiscale) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text=''")
            end if
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_imballo", ls_cod_imballo)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_vettore", ls_cod_vettore)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_inoltro", ls_cod_inoltro)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_mezzo", ls_cod_mezzo)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_porto", ls_cod_porto)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_resa", ls_cod_resa)
   
            f_po_loaddddw_dw(this, &
                             "cod_des_fornitore", &
                             sqlca, &
                             "anag_des_fornitori", &
                             "cod_des_fornitore", &
                             "rag_soc_1", &
                             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + i_coltext + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
    
            this.setitem(i_rownbr, "cod_des_fornitore", ls_null)

            f_po_loaddddw_dw(this, &
                             "cod_fil_fornitore", &
                             sqlca, &
                             "anag_fil_fornitori", &
                             "cod_fil_fornitore", &
                             "rag_soc_1", &
                             "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + i_coltext + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
    
            this.setitem(i_rownbr, "cod_fil_fornitore", ls_null)

				ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
            f_cambio_ven(ls_cod_valuta, ldt_data_registrazione)
         else
            this.setitem(i_rownbr, "cod_deposito", ls_null)
            this.setitem(i_rownbr, "cod_valuta", ls_null)
            this.setitem(i_rownbr, "cod_tipo_listino_prodotto", ls_null)
            this.setitem(i_rownbr, "cod_pagamento", ls_null)
            this.setitem(i_rownbr, "sconto", ls_null)
            this.setitem(i_rownbr, "cod_banca_clien_for", ls_null)
            this.setitem(i_rownbr, "cod_banca", ls_null)
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text=''")
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_imballo", ls_null)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_vettore", ls_null)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_inoltro", ls_null)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_mezzo", ls_null)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_porto", ls_null)
            tab_dettaglio.det_3.dw_tes_fat_ven_det_3.setitem(i_rownbr, "cod_resa", ls_null)
            this.setitem(i_rownbr, "cod_des_fornitore", ls_null)
            this.setitem(i_rownbr, "cod_fil_fornitore", ls_null)
				return 1
         end if 

		setnull(ls_null)
		ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
		ls_cod_tipo_fat_ven = getitemstring(getrow(), "cod_tipo_fat_ven")
		
		if guo_functions.uof_get_note_fisse(ls_null, i_coltext, ls_null, "FAT_VEN", ls_cod_tipo_fat_ven, ldt_data_registrazione,  ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
			g_mb.error(ls_nota_testata)
		else
			if len(ls_nota_video) > 0 then g_mb.warning("NOTA FISSA",ls_nota_video)
		end if
			
      case "cod_fil_fornitore"
         ls_cod_fornitore = this.getitemstring(i_rownbr, "cod_fornitore")

         select anag_fil_fornitori.rag_soc_1,
                anag_fil_fornitori.rag_soc_2,
                anag_fil_fornitori.indirizzo,
                anag_fil_fornitori.frazione,
                anag_fil_fornitori.cap,
                anag_fil_fornitori.localita,
                anag_fil_fornitori.provincia,
                anag_fil_fornitori.telefono,
                anag_fil_fornitori.fax,
                anag_fil_fornitori.telex
         into   :ls_rag_soc_1,    
                :ls_rag_soc_2,
                :ls_indirizzo,    
                :ls_frazione,
                :ls_cap,
                :ls_localita,
                :ls_provincia,
                :ls_telefono,
                :ls_fax,
                :ls_telex
         from   anag_fil_fornitori
         where  anag_fil_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_fil_fornitori.cod_fornitore = :ls_cod_fornitore and 
					 anag_fil_fornitori.cod_fil_fornitore = :i_coltext;
 
         if sqlca.sqlcode = 0 then 
            if not isnull(ls_rag_soc_1) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
            end if
            if not isnull(ls_rag_soc_2) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
            end if
            if not isnull(ls_indirizzo) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
            end if
            if not isnull(ls_frazione) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text='" + ls_frazione + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
            end if
            if not isnull(ls_cap) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text='" + ls_cap + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
            end if
            if not isnull(ls_localita) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text='" + ls_localita + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
            end if
            if not isnull(ls_provincia) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text='" + ls_provincia + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
            end if
            if not isnull(ls_telefono) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text='" + ls_telefono + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
            end if
            if not isnull(ls_fax) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text='" + ls_fax + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
            end if
            if not isnull(ls_telex) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text='" + ls_telex + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
            end if
			else
	         select anag_fornitori.rag_soc_1,
   	             anag_fornitori.rag_soc_2,
      	          anag_fornitori.indirizzo,
	                anag_fornitori.frazione,
   	             anag_fornitori.cap,
      	          anag_fornitori.localita,
         	       anag_fornitori.provincia,
            	    anag_fornitori.telefono,
	                anag_fornitori.fax,
	                anag_fornitori.telex
	         into   :ls_rag_soc_1,    
	                :ls_rag_soc_2,
	                :ls_indirizzo,    
	                :ls_frazione,
	                :ls_cap,
	                :ls_localita,
	                :ls_provincia,
	                :ls_telefono,
	                :ls_fax,
	                :ls_telex
	         from   anag_fornitori
	         where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
	                anag_fornitori.cod_fornitore = :ls_cod_fornitore;
 
	         if sqlca.sqlcode = 0 then 
	            if not isnull(ls_rag_soc_1) then
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
	            else
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
	            end if
	            if not isnull(ls_rag_soc_2) then
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
	            else
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
	            end if
	            if not isnull(ls_indirizzo) then
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
	            else
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
	            end if
	            if not isnull(ls_frazione) then
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text='" + ls_frazione + "'")
	            else
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
	            end if
	            if not isnull(ls_cap) then
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text='" + ls_cap + "'")
	            else
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
	            end if
	            if not isnull(ls_localita) then
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text='" + ls_localita + "'")
	            else
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
	            end if
	            if not isnull(ls_provincia) then
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text='" + ls_provincia + "'")
	            else
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
	            end if
	            if not isnull(ls_telefono) then
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text='" + ls_telefono + "'")
	            else
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
	            end if
	            if not isnull(ls_fax) then
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text='" + ls_fax + "'")
	            else
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
	            end if
	            if not isnull(ls_telex) then
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text='" + ls_telex + "'")
	            else
	               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
	            end if
	         else
	            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
	            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
	            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
	            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
	            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
	            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
	            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
	            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
	            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
	            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
				end if 
         end if
      case "cod_des_fornitore"
         ls_cod_fornitore = this.getitemstring(i_rownbr, "cod_fornitore")
         if f_cerca_destinazione(tab_dettaglio.det_2.dw_tes_fat_ven_det_2, i_rownbr, ls_cod_fornitore, "F", i_coltext, ls_messaggio) = -1 then
				g_mb.messagebox("APICE","Errore in ricerca destinazione diversa. ~r~nDettaglio: " +ls_messaggio)
			end if

      case "cod_deposito"
		   select anag_depositi.des_deposito,   
					 anag_depositi.indirizzo,   
					 anag_depositi.localita,   
					 anag_depositi.frazione,   
					 anag_depositi.cap,   
					 anag_depositi.provincia,
					 anag_depositi.telefono,
					 anag_depositi.fax,
					 anag_depositi.telex
			into   :ls_rag_soc_1,   
					 :ls_indirizzo,   
					 :ls_localita,   
					 :ls_frazione,   
					 :ls_cap,   
					 :ls_provincia,
					 :ls_telefono,
					 :ls_fax,
					 :ls_telex
			from   anag_depositi
         where  anag_depositi.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_depositi.cod_deposito = :i_coltext;
 
         if sqlca.sqlcode = 0 then 
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_label_clien_for_dep_1.text='Deposito di Partenza'")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_label_clien_for_dep_2.text='Deposito:'")
            if not isnull(i_coltext) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text='" + i_coltext + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text=''")
            end if
            if not isnull(ls_rag_soc_1) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
            end if
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
            if not isnull(ls_indirizzo) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
            end if
            if not isnull(ls_frazione) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text='" + ls_frazione + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
            end if
            if not isnull(ls_cap) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text='" + ls_cap + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
            end if
            if not isnull(ls_localita) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text='" + ls_localita + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
            end if
            if not isnull(ls_provincia) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text='" + ls_provincia + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
            end if
            if not isnull(ls_telefono) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text='" + ls_telefono + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
            end if
            if not isnull(ls_fax) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text='" + ls_fax + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
            end if
            if not isnull(ls_telex) then
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text='" + ls_telex + "'")
            else
               tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
            end if
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text=''")
         else
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_contatto.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text=''")
            tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text=''")
         end if 
			
		case "cod_valuta"
         ldt_data_registrazione = this.getitemdatetime(i_rownbr, "data_registrazione")
         f_cambio_ven(i_coltext, ldt_data_registrazione)
			
			
		case "cod_documento","anno_documento","numeratore_documento","num_documento"
			
			string ls_cod_documento, ls_numeratore, ls_duplicati
			long   ll_anno_documento, ll_num_documento, ll_anno, ll_num
			
			f_scrivi_log("NUMERATORI w_tes_fat_ven_lista - itemchanged (" + dwo.name + ")" )
			
			choose case i_colname
				case "cod_documento"
					ls_cod_documento = i_coltext
					ll_anno_documento = getitemnumber(getrow(),"anno_documento")
					ls_numeratore = getitemstring(getrow(),"numeratore_documento")
					ll_num_documento = getitemnumber(getrow(),"num_documento")
					f_scrivi_log("NUMERATORI w_tes_fat_ven_lista - itemchanged (cod_documento) dal valore " + getitemstring(getrow(),"cod_documento") + " al valore " + i_coltext)
				case "anno_documento"
					ls_cod_documento = getitemstring(getrow(),"cod_documento")
					ll_anno_documento = long(i_coltext)
					ls_numeratore = getitemstring(getrow(),"numeratore_documento")
					ll_num_documento = getitemnumber(getrow(),"num_documento")
					f_scrivi_log("NUMERATORI w_tes_fat_ven_lista - itemchanged (anno_documento) dal valore " + string(getitemnumber(getrow(),"anno_documento")) + " al valore " + i_coltext)
				case "numeratore_documento"
					ls_cod_documento = getitemstring(getrow(),"cod_documento")
					ll_anno_documento = getitemnumber(getrow(),"anno_documento")
					ls_numeratore = i_coltext
					ll_num_documento = getitemnumber(getrow(),"num_documento")
					f_scrivi_log("NUMERATORI w_tes_fat_ven_lista - itemchanged (numeratore_documento) dal valore " + getitemstring(getrow(),"numeratore_documento") + " al valore " + i_coltext)
				case "num_documento"
					ls_cod_documento = getitemstring(getrow(),"cod_documento")
					ll_anno_documento = getitemnumber(getrow(),"anno_documento")
					ls_numeratore = getitemstring(getrow(),"numeratore_documento")
					f_scrivi_log("NUMERATORI w_tes_fat_ven_lista - itemchanged (num_documento) dal valore " + string(getitemnumber(getrow(),"num_documento")) + " al valore " + i_coltext)
					ll_num_documento = long(i_coltext)
			end choose
			
			declare duplicati cursor for
			select anno_registrazione,
					 num_registrazione
			from   tes_fat_ven
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_documento = :ls_cod_documento and
					 anno_documento = :ll_anno_documento and
					 numeratore_documento = :ls_numeratore and
					 num_documento = :ll_num_documento;
					 
			open duplicati;
			
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("APICE","Errore in controllo numeri fiscali doppi.~n" + &
							  "Errore nella open del cursore duplicati: " + sqlca.sqlerrtext)
				return 1
			end if
			
			ls_duplicati = ""
			
			do while true
				
				fetch duplicati
				into  :ll_anno,
						:ll_num;
						
				if sqlca.sqlcode < 0 then
					g_mb.messagebox("APICE","Errore in controllo numeri fiscali doppi.~n" + &
								  "Errore nella fetch del cursore duplicati: " + sqlca.sqlerrtext)
					close duplicati;
					return 1
				elseif sqlca.sqlcode = 100 then
					close duplicati;
					exit
				end if
				
				ls_duplicati = ls_duplicati + "~n" + string(ll_anno) + "/" + string(ll_num)
				
			loop
			
			if ls_duplicati <> "" then
				ls_duplicati = "ATTENZIONE!~nEsistono altri documenti con questo numero fiscale:" + ls_duplicati
				g_mb.messagebox("APICE",ls_duplicati)
			end if
			
			// *** controllo se è maggiore di quello esistente
			long ll_num_appo, ll_ret
			
			select max(num_documento)
			into   :ll_num_appo
			from   numeratori  
			where  cod_azienda = :s_cs_xx.cod_azienda and
					 cod_documento = :ls_cod_documento and
					 numeratore_documento = :ls_numeratore and
					 anno_documento = :ll_anno_documento;			
					 
			if not isnull(ll_num_appo) and ll_num_appo < ll_num_documento then   // il numero cambiato è + grande
			
				ll_ret = g_mb.messagebox( "APICE", "Aggiorno i numeratori fiscali?", Exclamation!, OKCancel!, 2)
				IF ll_ret = 1 THEN
					update numeratori
					set    num_documento = :ll_num_documento
					where  cod_azienda = :s_cs_xx.cod_azienda and
							 cod_documento = :ls_cod_documento and
							 numeratore_documento = :ls_numeratore and
							 anno_documento = :ll_anno_documento;		
							 
					if sqlca.sqlcode <> 0 then
						g_mb.messagebox( "APICE", "Errore durante l'aggiornamento dei numeratori: " + sqlca.sqlerrtext, stopsign!)
						rollback;
						return -1
					else
						f_scrivi_log("Eseguito UPDATE tabella NUMERATORI Fatture. Il numeratore " + ls_cod_documento + "/" + ls_numeratore + "-" + string(ll_anno_documento) + " è stato impostato al valore " + string(ll_num_documento))
						commit;
						g_mb.messagebox( "APICE", "Numeratori_aggiornati")
					end if
				END IF
			
			end if			
		
		case "data_fattura"
		
			date ldt_1, ldt_2
			
			ldt_1 = date(left(i_coltext, 10))
			
			ldt_2 = today()
		
			if ldt_1 <> ldt_2 then
				g_mb.messagebox("APICE","ATTENZIONE!~nLa data inserita è diversa dalla data di sistema.")
			end if
		
   end choose
end if

end event

event pcd_modify;call super::pcd_modify;if i_extendmode then
	string ls_modify, ls_flag_tipo_fat_ven, ls_null, ls_cod_tipo_fat_ven
	
	ls_modify = "cod_tipo_fat_ven.protect='0~tif(anno_registrazione = 0,0,1)'~t"
	ls_modify = ls_modify + "cod_tipo_fat_ven.background.color='12632256~tif(anno_registrazione = 0,16777215,12632256)'~t"
	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.modify(ls_modify)
	
	setnull(ls_null)
	ls_cod_tipo_fat_ven = this.getitemstring(this.getrow(), "cod_tipo_fat_ven")
	
	wf_abilita_pulsanti(true)
//	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_cliente.enabled = true
//	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_banca_for.visible = true
//	tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_banca_for.enabled = true
	
	
//-------------------------------- Modifica Nicola ---------------------------------------
	select  flag_tipo_fat_ven
	into   :ls_flag_tipo_fat_ven
	from   tab_tipi_fat_ven
	where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and
			 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;

	if ls_flag_tipo_fat_ven = "I" then
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_1_dest_fat.Background.Color='12632256'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_1_dest_fat.Protect=1
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_2_dest_fat.Background.Color='12632256'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_2_dest_fat.Protect=1
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("indirizzo_dest_fat.Background.Color='12632256'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.indirizzo_dest_fat.Protect=1
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("frazione_dest_fat.Background.Color='12632256'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.frazione_dest_fat.Protect=1				
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("cap_dest_fat.Background.Color='12632256'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.cap_dest_fat.Protect=1								
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("localita_dest_fat.Background.Color='12632256'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.localita_dest_fat.Protect=1								
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("provincia_dest_fat.Background.Color='12632256'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.provincia_dest_fat.Protect=1								
		
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_1.Background.Color='16777215'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_1.Protect=0
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_2.Background.Color='16777215'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_2.Protect=0
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("indirizzo.Background.Color='16777215'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.indirizzo.Protect=0
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("frazione.Background.Color='16777215'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.frazione.Protect=0				
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("cap.Background.Color='16777215'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.cap.Protect=0								
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("localita.Background.Color='16777215'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.localita.Protect=0								
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("provincia.Background.Color='16777215'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.provincia.Protect=0															
	else 	
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_1.Background.Color='12632256'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_1.Protect=1
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_2.Background.Color='12632256'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_2.Protect=1
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("indirizzo.Background.Color='12632256'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.indirizzo.Protect=1
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("frazione.Background.Color='12632256'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.frazione.Protect=1				
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("cap.Background.Color='12632256'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.cap.Protect=1								
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("localita.Background.Color='12632256'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.localita.Protect=1								
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("provincia.Background.Color='12632256'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.provincia.Protect=1											
		
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_1_dest_fat.Background.Color='16777215'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_1_dest_fat.Protect=0
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("rag_soc_2_dest_fat.Background.Color='16777215'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.rag_soc_2_dest_fat.Protect=0
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("indirizzo_dest_fat.Background.Color='16777215'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.indirizzo_dest_fat.Protect=0
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("frazione_dest_fat.Background.Color='16777215'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.frazione_dest_fat.Protect=0				
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("cap_dest_fat.Background.Color='16777215'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.cap_dest_fat.Protect=0								
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("localita_dest_fat.Background.Color='16777215'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.localita_dest_fat.Protect=0								
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Modify("provincia_dest_fat.Background.Color='16777215'")
		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.Object.provincia_dest_fat.Protect=0								
	end if	
//--------------------------------- Fine Modifica ----------------------------------------	
end if
end event

event pcd_new;call super::pcd_new;if i_extendmode then
   string ls_cod_tipo_fat_ven, ls_cod_causale, ls_cod_operatore, ls_flag_tipo_fat_ven, ls_null,ls_cod_deposito, ls_errore
	
	setnull(ls_null)
   select con_fat_ven.cod_tipo_fat_ven
   into   :ls_cod_tipo_fat_ven
   from   con_fat_ven
   where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;

   if sqlca.sqlcode <>0 then
		g_mb.messagebox("Attenzione", "Si è verificato un errore in fase di lettura Parametri fatture Vendita.",  exclamation!, ok!)
		wf_reset_tipo_fattura()
		return
	end if

	this.setitem(this.getrow(), "cod_tipo_fat_ven", ls_cod_tipo_fat_ven)
	this.setitem(this.getrow(), "data_registrazione", datetime(today()))
	this.setitem(this.getrow(), "data_inizio_trasporto", datetime(today(), 00:00:00))
	this.setitem(this.getrow(), "ora_inizio_trasporto", datetime(date("01/01/1900"),now()))
	setitem(getrow(),"flag_scad_manuali","N")
	
	select tab_tipi_fat_ven.cod_causale, flag_tipo_fat_ven
	into   :ls_cod_causale, :ls_flag_tipo_fat_ven
	from   tab_tipi_fat_ven
	where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and
			 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	if sqlca.sqlcode = 0 then
		this.setitem(this.getrow(),"cod_causale",ls_cod_causale)
	wf_tipo_fattura(ls_flag_tipo_fat_ven, false)
	end if



	select cod_operatore
	  into :ls_cod_operatore
	  from tab_operatori_utenti
	 where cod_azienda = :s_cs_xx.cod_azienda and
	 		 cod_utente = :s_cs_xx.cod_utente and
			 flag_default = 'S';
			 
   if sqlca.sqlcode = -1 then
		g_mb.messagebox("Estrazione Operatori", "Errore durante l'Estrazione dell'Operatore di Default")
		pcca.error = c_fatal
		return
	elseif sqlca.sqlcode = 0 then
      this.setitem(this.getrow(), "cod_operatore", ls_cod_operatore)
	end if
	
	// stefanop: 21/12/2011: aggiunto deposito collegato all'operatore
	guo_functions.uof_get_stabilimento_utente(ls_cod_deposito, ls_errore)
	if not isnull(ls_cod_deposito) then setitem(getrow(), "cod_deposito", ls_cod_deposito)
	// ----
	
	wf_abilita_pulsanti(true)

//	dw_tes_fat_ven_det_1.object.b_ricerca_banca_for.enabled = true
//	dw_tes_fat_ven_det_1.object.b_ricerca_cliente.enabled=true
//	dw_tes_fat_ven_det_1.object.b_ricerca_banca_for.visible = true
	ib_new = true

	tab_dettaglio.det_4.dw_tes_fat_ven_det_4.setitem(this.getrow(),"flag_st_note_tes","I")
	tab_dettaglio.det_4.dw_tes_fat_ven_det_4.setitem(this.getrow(),"flag_st_note_pie","I")
end if
end event

event pcd_retrieve;call super::pcd_retrieve;long ll_errore

if not isvalid(istr_data) or isnull(istr_data) or UpperBound(istr_data.decimale) < 2 then return

ll_errore = retrieve(s_cs_xx.cod_azienda, istr_data.decimale[1],istr_data.decimale[2])

if ll_errore < 0 then
   pcca.error = c_fatal
end if

change_dw_current()
end event

event pcd_save;call super::pcd_save;if i_extendmode then
	if pcca.error = c_success then
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_banca_for.enabled = false
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_banca_for.visible = true
		tab_dettaglio.det_1.dw_tes_fat_ven_det_1.object.b_ricerca_cliente.enabled=false
//		cb_aspetto.enabled = false

		if this.getrow() > 0 and this.getitemnumber(this.getrow(), "num_registrazione") > 0 then
//			cb_dettaglio.enabled = true
//			cb_stampa.enabled = true
//			cb_excel.enabled = true
//			cb_blocca.enabled = true
//			cb_sblocca.enabled = true
//			cb_contro.enabled = true
//			cb_scadenze.enabled = true
//			cb_iva.enabled = true
//			cb_calcola.enabled = true
//			cb_corrispondenze.enabled = true
//			cb_duplica_doc.enabled = true
//			cb_note.enabled = true
//			cb_ricalcolo_spese_trasp.enabled=true
		else
//			cb_dettaglio.enabled = false
//			cb_stampa.enabled = false
//			cb_excel.enabled = false
//			cb_blocca.enabled = false
//			cb_sblocca.enabled = false
//			cb_contro.enabled = false
//			cb_scadenze.enabled = false
//			cb_iva.enabled = false
//			cb_calcola.enabled = false
//			cb_corrispondenze.enabled = false
//			cb_duplica_doc.enabled = false
//			cb_note.enabled = false
//			cb_ricalcolo_spese_trasp.enabled=false
		end if
		ib_new = false
		
		
		//###########################################################
		//metto qui il messaggio per evitare eventuali locks: commit già è stato fatto
		//se ho cancellato la fattura e ho riaperto ordini ho la lista di questi ordini
		//in tutti gli altri casi la variabile sarà vuota e non partirà alcun messaggio
		if is_ordini_riaperti<>"" and not isnull(is_ordini_riaperti) then
			g_mb.warning("Al seguito della cancellazione della fattura sono stati riaperti i seguenti ordini:~r~n"+is_ordini_riaperti)
		end if
		is_ordini_riaperti = ""
		//###########################################################
		
		
	end if
end if

						
end event

event pcd_setkey;call super::pcd_setkey;long ll_i, ll_anno_registrazione, ll_num_registrazione, ll_max_registrazione


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
   if this.getitemnumber(ll_i, "anno_registrazione") = 0 or &
      isnull(this.getitemnumber(ll_i, "anno_registrazione")) then
		ll_anno_registrazione = f_anno_esercizio()
      if ll_anno_registrazione > 0 then
         this.setitem(this.getrow(), "anno_registrazione", int(ll_anno_registrazione))
      else
			g_mb.messagebox("Fatture Vendita","Impostare l'anno di esercizio in parametri aziendali")
		end if
		
      select con_fat_ven.num_registrazione
      into   :ll_num_registrazione
      from   con_fat_ven
      where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;
		choose case sqlca.sqlcode
			case 0
				ll_num_registrazione ++
			case 100
				g_mb.messagebox("Fatture di Vendita","Errore in assegnazione progressivo fattura interno: verificare di aver impostato i parametri offerte di vendita. ~r~nIl salvataggio verrà effettuato ugualmente")
			case else
				g_mb.messagebox("Fatture di Vendita","Errore in assegnazione progressivo fattura interno.~r~nDettaglio errore "+ sqlca.sqlerrtext +"~r~nIl salvataggio verrà effettuato ugualmente")
		end choose
		
		ll_max_registrazione = 0
		select max(num_registrazione)
		into   :ll_max_registrazione
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_registrazione;
		if not isnull(ll_max_registrazione) then
			if ll_max_registrazione >= ll_num_registrazione then ll_num_registrazione = ll_max_registrazione + 1
		end if
			
      this.setitem(this.getrow(), "num_registrazione", ll_num_registrazione)
      update con_fat_ven
      set    con_fat_ven.num_registrazione = :ll_num_registrazione
      where  con_fat_ven.cod_azienda = :s_cs_xx.cod_azienda;
    end if
next      
end event

event pcd_validaterow;call super::pcd_validaterow;string ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven

ls_cod_tipo_fat_ven = this.getitemstring(this.getrow(),"cod_tipo_fat_ven")

select tab_tipi_fat_ven.flag_tipo_fat_ven
into   :ls_flag_tipo_fat_ven
from   tab_tipi_fat_ven
where  tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and
		 tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;

if i_insave > 0 then
   if this.getrow() > 0 then
		choose case ls_flag_tipo_fat_ven
				
			case "F"
				if isnull(this.getitemstring(this.getrow(), "cod_fornitore")) then
						g_mb.messagebox("Attenzione", "Inserire Codice Fornitore.", exclamation!, ok!)
						pcca.error = c_fatal
						return
					end if
					
			case else
				if isnull( this.getitemstring(this.getrow(), "cod_cliente")) and isnull( this.getitemstring(this.getrow(), "cod_contatto"))  then
						g_mb.messagebox("Attenzione", "Inserire Codice Cliente o Contatto.", exclamation!, ok!)
						pcca.error = c_fatal
						return
				end if
		end choose
   end if
end if

end event

event pcd_view;call super::pcd_view;if i_extendmode then
	wf_abilita_pulsanti(false)
//	dw_tes_fat_ven_det_1.object.b_ricerca_cliente.enabled = false
//	dw_tes_fat_ven_det_1.object.b_ricerca_banca_for.visible = true
//	dw_tes_fat_ven_det_1.object.b_ricerca_banca_for.enabled=false
//	cb_aspetto.enabled = false

	if this.getrow() > 0 and this.getitemnumber(this.getrow(), "num_registrazione") > 0 then
//		cb_dettaglio.enabled = true
//		cb_stampa.enabled = true
//		cb_excel.enabled = true
//		cb_blocca.enabled = true
//		cb_sblocca.enabled = true
//		cb_contro.enabled = true
//		cb_scadenze.enabled = true
//		cb_iva.enabled = true
//		cb_calcola.enabled = true
//		cb_corrispondenze.enabled = true
//		cb_duplica_doc.enabled = true
//		cb_note.enabled = true
//		cb_ricalcolo_spese_trasp.enabled=true
	else
//		cb_dettaglio.enabled = false
//		cb_stampa.enabled = false
//		cb_excel.enabled = false
//		cb_blocca.enabled = false
//		cb_sblocca.enabled = false
//		cb_contro.enabled = false
//		cb_scadenze.enabled = false
//		cb_iva.enabled = false
//		cb_calcola.enabled = false
//		cb_corrispondenze.enabled = false
//		cb_duplica_doc.enabled = false
//		cb_note.enabled = false
//		cb_ricalcolo_spese_trasp.enabled=false
	end if
	ib_new = false
end if
end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then	
	
   string			ls_cod_cliente, ls_rag_soc_1, ls_rag_soc_2, ls_indirizzo, ls_frazione, ls_cod_contatto, &
					ls_cap, ls_localita, ls_provincia, ls_telefono, ls_fax, ls_telex, &
					ls_partita_iva, ls_cod_fiscale, ls_cod_fornitore, ls_cod_tipo_fat_ven, &
					ls_flag_tipo_fat_ven, ls_cod_tipo_fat_ven_com, ls_flag_tipo_fat_ven_com, ls_flag_dest_merce_fat

	if getrow() > 0 then
		//##########################################################################
		ls_cod_cliente = getitemstring(getrow(), "cod_cliente")
		ls_cod_fornitore = getitemstring(getrow(), "cod_fornitore")
		ls_cod_contatto = getitemstring(getrow(), "cod_contatto")
		ls_cod_tipo_fat_ven= getitemstring(getrow(), "cod_tipo_fat_ven")
	
		
		select flag_tipo_fat_ven
		into   :ls_flag_tipo_fat_ven
		from   tab_tipi_fat_ven  
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;

		if not isnull(ls_flag_tipo_fat_ven) and  ls_flag_tipo_fat_ven<>"" then
			wf_tipo_fattura(ls_flag_tipo_fat_ven, true)
		end if
		
		//cliente --------------------------------------------------------------------------------------------------
		if not isnull(ls_cod_cliente) and ls_cod_cliente <> "" then
			select anag_clienti.rag_soc_1,
					 anag_clienti.rag_soc_2,
					 anag_clienti.indirizzo,
					 anag_clienti.frazione,
					 anag_clienti.cap,
					 anag_clienti.localita,
					 anag_clienti.provincia,
					 anag_clienti.telefono,
					 anag_clienti.fax,
					 anag_clienti.telex,
					 anag_clienti.partita_iva,
					 anag_clienti.cod_fiscale
			into   :ls_rag_soc_1,    
					 :ls_rag_soc_2,
					 :ls_indirizzo,    
					 :ls_frazione,
					 :ls_cap,
					 :ls_localita,
					 :ls_provincia,
					 :ls_telefono,
					 :ls_fax,
					 :ls_telex,
					 :ls_partita_iva,
					 :ls_cod_fiscale
			from   anag_clienti
			where  anag_clienti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_clienti.cod_cliente = :ls_cod_cliente;
	
			if sqlca.sqlcode = 0 then
				tab_dettaglio.det_1.dw_tes_fat_ven_det_1.modify("st_cliente_fornitore.text='Cliente'")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_label_clien_for_dep_1.text='Cliente'")
            		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_label_clien_for_dep_2.text='Cliente:'")
				if not isnull(ls_cod_cliente) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text='" + ls_cod_cliente + "'")
				else
					ls_cod_cliente = ""
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text=''")
				end if
				if not isnull(ls_rag_soc_1) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
				end if
				if not isnull(ls_rag_soc_2) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
				end if
				if not isnull(ls_indirizzo) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
				end if
				if not isnull(ls_frazione) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text='" + ls_frazione + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
				end if
				if not isnull(ls_cap) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text='" + ls_cap + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
				end if
				if not isnull(ls_localita) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text='" + ls_localita + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
				end if
				if not isnull(ls_provincia) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text='" + ls_provincia + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
				end if
				if not isnull(ls_telefono) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text='" + ls_telefono + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
				end if
				if not isnull(ls_fax) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text='" + ls_fax + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
				end if
				if not isnull(ls_telex) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text='" + ls_telex + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
				end if
				if not isnull(ls_partita_iva) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text=''")
				end if
				if not isnull(ls_cod_fiscale) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text=''")
				end if
				//---------------------------- Modfica Nicola ------------------------------------------
				ls_cod_tipo_fat_ven_com = dw_tes_fat_ven_det_1.getitemstring(dw_tes_fat_ven_det_1.getrow(), "cod_tipo_fat_ven")
	
				select flag_tipo_fat_ven
				into   :ls_flag_tipo_fat_ven_com
				from   tab_tipi_fat_ven
				where  cod_azienda = :s_cs_xx.cod_azienda and
						 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven_com;
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("APICE","Errore in ricerca tipo fattura in tabella tab_tipi_fat_ven.~r~nDettaglio errore:"+sqlca.sqlerrtext)
					return 1
				end if			
	
				if ls_flag_tipo_fat_ven_com = "I" then
					ls_flag_dest_merce_fat = "S"
				else 
					ls_flag_dest_merce_fat = "N"
				end if	
				//----------------------------- Fine Modifica ------------------------------------------
		
				f_po_loaddddw_dw(this, &
									  "cod_des_cliente", &
									  sqlca, &
									  "anag_des_clienti", &
									  "cod_des_cliente", &
									  "rag_soc_1", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_cliente = '" + ls_cod_cliente + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) and flag_dest_merce_fat = '" + ls_flag_dest_merce_fat + "'")
			else
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_contatto.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text=''")
			end if
		
		//fornitore --------------------------------------------------------------------------------------------------
		elseif not isnull(ls_cod_fornitore) and ls_cod_fornitore <> "" then
			select anag_fornitori.rag_soc_1,
					 anag_fornitori.rag_soc_2,
					 anag_fornitori.indirizzo,
					 anag_fornitori.frazione,
					 anag_fornitori.cap,
					 anag_fornitori.localita,
					 anag_fornitori.provincia,
					 anag_fornitori.telefono,
					 anag_fornitori.fax,
					 anag_fornitori.telex,
					 anag_fornitori.partita_iva,
					 anag_fornitori.cod_fiscale
			into   :ls_rag_soc_1,    
					 :ls_rag_soc_2,
					 :ls_indirizzo,    
					 :ls_frazione,
					 :ls_cap,
					 :ls_localita,
					 :ls_provincia,
					 :ls_telefono,
					 :ls_fax,
					 :ls_telex,
					 :ls_partita_iva,
					 :ls_cod_fiscale
			from   anag_fornitori
			where  anag_fornitori.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_fornitori.cod_fornitore = :ls_cod_fornitore;
	
			if sqlca.sqlcode = 0 then
				dw_tes_fat_ven_det_1.modify("st_cliente_fornitore.text='Fornitore'")
            		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_label_clien_for_dep_1.text='Fornitore'")
            		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_label_clien_for_dep_2.text='Fornitore:'")
						
				if not isnull(ls_cod_fornitore) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text='" + ls_cod_fornitore + "'")
				else
					ls_cod_cliente = ""
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text=''")
				end if
				if not isnull(ls_rag_soc_1) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
				end if
				if not isnull(ls_rag_soc_2) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
				end if
				if not isnull(ls_indirizzo) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
				end if
				if not isnull(ls_frazione) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text='" + ls_frazione + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
				end if
				if not isnull(ls_cap) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text='" + ls_cap + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
				end if
				if not isnull(ls_localita) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text='" + ls_localita + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
				end if
				if not isnull(ls_provincia) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text='" + ls_provincia + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
				end if
				if not isnull(ls_telefono) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text='" + ls_telefono + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
				end if
				if not isnull(ls_fax) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text='" + ls_fax + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
				end if
				if not isnull(ls_telex) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text='" + ls_telex + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
				end if
				if not isnull(ls_partita_iva) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text=''")
				end if
				if not isnull(ls_cod_fiscale) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text=''")
				end if
	
				f_po_loaddddw_dw(this, &
									  "cod_des_fornitore", &
									  sqlca, &
									  "anag_des_fornitori", &
									  "cod_des_fornitore", &
									  "rag_soc_1", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + ls_cod_fornitore + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
			else
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text=''")
			end if
		
		//contatto --------------------------------------------------------------------------------------------------
		elseif not isnull(ls_cod_contatto) and ls_cod_contatto <> "" then
			
			select anag_contatti.rag_soc_1,
					 anag_contatti.rag_soc_2,
					 anag_contatti.indirizzo,
					 anag_contatti.frazione,
					 anag_contatti.cap,
					 anag_contatti.localita,
					 anag_contatti.provincia,
					 anag_contatti.telefono,
					 anag_contatti.fax,
					 anag_contatti.telex,
					 anag_contatti.partita_iva,
					 anag_contatti.cod_fiscale
			into	:ls_rag_soc_1,    
					:ls_rag_soc_2,
					:ls_indirizzo,    
					:ls_frazione,
					:ls_cap,
					:ls_localita,
					:ls_provincia,
					:ls_telefono,
					:ls_fax,
					:ls_telex,
					:ls_partita_iva,
					:ls_cod_fiscale
			from   anag_contatti
			where  anag_contatti.cod_azienda = :s_cs_xx.cod_azienda and 
					 anag_contatti.cod_contatto = :ls_cod_contatto;
	
			if sqlca.sqlcode = 0 then
				dw_tes_fat_ven_det_1.modify("st_cliente_fornitore.text='Contatto'")
            		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_label_clien_for_dep_1.text=''")
            		tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_label_clien_for_dep_2.text=':'")
						
				if not isnull(ls_cod_fornitore) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text='" + "" + "'")
				else
					ls_cod_cliente = ""
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text=''")
				end if
				if not isnull(ls_rag_soc_1) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text='" + ls_rag_soc_1 + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
				end if
				if not isnull(ls_rag_soc_2) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text='" + ls_rag_soc_2 + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
				end if
				if not isnull(ls_indirizzo) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text='" + ls_indirizzo + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
				end if
				if not isnull(ls_frazione) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text='" + ls_frazione + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
				end if
				if not isnull(ls_cap) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text='" + ls_cap + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
				end if
				if not isnull(ls_localita) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text='" + ls_localita + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
				end if
				if not isnull(ls_provincia) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text='" + ls_provincia + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
				end if
				if not isnull(ls_telefono) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text='" + ls_telefono + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
				end if
				if not isnull(ls_fax) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text='" + ls_fax + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
				end if
				if not isnull(ls_telex) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text='" + ls_telex + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
				end if
				if not isnull(ls_partita_iva) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text='" + ls_partita_iva + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text=''")
				end if
				if not isnull(ls_cod_fiscale) then
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text='" + ls_cod_fiscale + "'")
				else
					tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text=''")
				end if
	
				f_po_loaddddw_dw(this, &
									  "cod_des_fornitore", &
									  sqlca, &
									  "anag_des_fornitori", &
									  "cod_des_fornitore", &
									  "rag_soc_1", &
									  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and cod_fornitore = '" + "CODICEINESISTENTE" + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")
			else
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_clien_for_dep.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_1.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_rag_soc_2.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_indirizzo.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_frazione.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cap.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_localita.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_provincia.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telefono.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_fax.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_telex.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_partita_iva.text=''")
				tab_dettaglio.det_2.dw_tes_fat_ven_det_2.modify("st_cod_fiscale.text=''")
			end if
			
		end if
		
		//##########################################################################
	end if	
end if
end event

event ue_key;call super::ue_key;string ls_colonna_sconto, ls_cod_valuta, ls_cod_cliente, ls_cod_prodotto, ls_stringa, &
       ls_cod_agente_1, ls_cod_agente_2, ls_cod_tipo_det_ven, ls_listino, ls_messaggio
long ll_sconti[], ll_maggiorazioni[], ll_i, ll_y
double ld_variazioni[], ld_quantita, ld_cambio_ven, ld_prezzo_acquisto, ld_ultimo_prezzo
datetime ldt_data_registrazione


choose case this.getcolumnname()

	case "cod_cliente"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_cliente(dw_tes_fat_ven_det_1,"cod_cliente")	
		end if
	case "cod_fornitore"
		if key = keyF1!  and keyflags = 1 then
			this.change_dw_current()
			guo_ricerca.uof_ricerca_fornitore(dw_tes_fat_ven_det_1,"cod_fornitore")	
		end if
end choose


end event

event updateend;call super::updateend;long ll_row, ll_risposta
ss_record    lstr_record
treeviewitem ltv_item

ll_row = this.getrow()

choose case this.getitemstatus(ll_row, 0, Primary!)
		
	case NewModified!	
		
		wf_inserisci_fattura_vendita(0, getitemnumber(ll_row,"anno_registrazione"), getitemnumber(ll_row,"num_registrazione"))
		
	case New!
		
end choose


end event

event updatestart;call super::updatestart;if i_extendmode then
	integer			li_i, li_risp
	long				ll_i
	string				ls_tabella, ls_tot_documento,ls_nome_prog,ls_cod_tipo_fat_ven, ls_flag_tipo_fat_ven, &
						ls_messaggio, ls_cod_cliente, ls_nota_testata, ls_nota_piede, ls_nota_old, ls_null, &
						ls_nota_video, ls_cod_fornitore, ls_log
	datetime			ldt_data_registrazione
	long				ll_anno_registrazione, ll_num_registrazione
	uo_log_sistema luo_log
	
	for li_i = 1 to this.deletedcount()
		ll_anno_registrazione = this.getitemnumber(li_i, "anno_registrazione", delete!, true)
		ll_num_registrazione 	= this.getitemnumber(li_i, "num_registrazione", delete!, true)
		ls_cod_tipo_fat_ven 	= this.getitemstring(li_i, "cod_tipo_fat_ven", delete!, true)		
		ls_cod_cliente 			= this.getitemstring(li_i, "cod_cliente", delete!, true)

		luo_log = create uo_log_sistema
		luo_log.uof_write_log_sistema_not_sqlca( "CANC_OFV", g_str.format("Cancellazione Testata Offerta Vendita $1-$2  Cliente $3",ll_anno_registrazione,ll_num_registrazione, ls_cod_cliente))
		destroy luo_log
		
		if f_dati_fat_ven(ll_anno_registrazione,ll_num_registrazione,ls_log) <> 0 then
			ls_log = string(ll_anno_registrazione) + " / " + string(ll_num_registrazione)
		end if

		ls_log = "Cancellata Fattura: " + ls_log
		f_scrivi_log(ls_log)

		ls_tabella = "det_fat_ven_stat"
		ls_nome_prog = ""

		if f_elimina_det_stat(ls_tabella, ll_anno_registrazione, ll_num_registrazione, ls_nome_prog, 0) = -1 then
			return 1
		end if

		ls_tabella = "det_fat_ven"
		
		setnull(ls_flag_tipo_fat_ven)
		
		select flag_tipo_fat_ven  
		into   :ls_flag_tipo_fat_ven  
		from   tab_tipi_fat_ven  
		where  cod_azienda = :s_cs_xx.cod_azienda and  
				 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
		if sqlca.sqlcode = 0 and ls_flag_tipo_fat_ven = "I" then
			if f_riag_quan_in_spedizione_fatture(ll_anno_registrazione, ll_num_registrazione) = -1 then
				return 1
			end if
		end if
		
		delete from det_fat_ven_cc
		where  cod_azienda = :s_cs_xx.cod_azienda and
				 anno_registrazione = :ll_anno_registrazione  and
				 num_registrazione = :ll_num_registrazione ;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE", "Errore in cancellazione dati centri di costo.~r~n"+sqlca.sqlerrtext)
			return 1
		end if
		
		//-----------------------------------------------------------------------------------------------------------------------------
		//eseguo riapertura ordini contenuti nella fattura (solo se è una fattura immediata, cioe creata direttamente da ordine)
		if ls_flag_tipo_fat_ven="I" then
			li_risp = wf_riapri_ordini(ll_anno_registrazione, ll_num_registrazione,ls_messaggio)
			if li_risp<0 then
				g_mb.error("APICE", ls_messaggio)
				return 1
				
			elseif li_risp=0 and ls_messaggio<>"" and not isnull(ls_messaggio) then
				is_ordini_riaperti = ls_messaggio
				ls_messaggio = ""
			end if
		end if
		//-----------------------------------------------------------------------------------------------------------------------------
	
		if f_del_det(ls_tabella, ll_anno_registrazione, ll_num_registrazione) = -1 then
			return 1
		end if

		if f_del_fat_ven(ll_anno_registrazione, ll_num_registrazione) = -1 then
			return 1
		end if
	next

	for ll_i = 1 to rowcount()
		
		dwItemStatus l_status
		
		l_status =  getitemstatus(ll_i, 0, primary!)
		
		if l_status = NewModified! then
			ll_anno_registrazione = getitemnumber(ll_i, "anno_registrazione")
			ll_num_registrazione = getitemnumber(ll_i, "num_registrazione")

			if ll_anno_registrazione <> 0 and not isnull(ll_anno_registrazione) then

				if dw_tes_fat_ven_det_1.getitemstring(dw_tes_fat_ven_det_1.getrow(), "flag_blocco") = "S" then
					g_mb.messagebox("Attenzione", "Fattura non modificabile! E' Bloccata.", exclamation!, ok!)
					dw_tes_fat_ven_det_1.set_dw_view(c_ignorechanges)
					pcca.error = c_fatal
					return 1
				end if
			
				ls_cod_tipo_fat_ven = getitemstring(li_i, "cod_tipo_fat_ven")

				select flag_tipo_fat_ven  
				into   :ls_flag_tipo_fat_ven  
				from   tab_tipi_fat_ven  
				where  cod_azienda = :s_cs_xx.cod_azienda and  
						 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
	
				if sqlca.sqlcode <> 0 then
					g_mb.messagebox("Attenzione", "Errore durante la ricerca del tipo fattura: verificare",  stopsign!, ok!)
					dw_tes_fat_ven_det_1.set_dw_view(c_ignorechanges)
					pcca.error = c_fatal
					return 1
				end if

				if dw_tes_fat_ven_det_1.getitemstring(dw_tes_fat_ven_det_1.getrow(), "flag_contabilita") = "S" and (ls_flag_tipo_fat_ven = "I" or ls_flag_tipo_fat_ven = "D" or ls_flag_tipo_fat_ven = "N") then
					g_mb.messagebox("Attenzione", "Fattura non modificabile! E' già stata contabilizzata",  exclamation!, ok!)
					dw_tes_fat_ven_det_1.set_dw_view(c_ignorechanges)
					pcca.error = c_fatal
					return 1
				end if

				if (ls_flag_tipo_fat_ven <> "D") and (ls_flag_tipo_fat_ven <> "F") and (ls_flag_tipo_fat_ven <> "P") and (dw_tes_fat_ven_det_1.getitemstring(dw_tes_fat_ven_det_1.getrow(), "flag_movimenti") = "S") then
						g_mb.messagebox("Attenzione", "Fattura non modificabile! E' già stata confermata.", exclamation!, ok!)
						dw_tes_fat_ven_det_1.set_dw_view(c_ignorechanges)
						pcca.error = c_fatal
						return 1
				end if
				
			else
				

				if (ls_flag_tipo_fat_ven = "P" or ls_flag_tipo_fat_ven = "F") then //fatture pro forma: non devono essere contabilizzate
					this.setitem(this.getrow(), "flag_contabilita", "S")
					this.setitem(this.getrow(), "flag_movimenti", "S")
				end if
				
				setnull(ls_null)
				ls_cod_cliente = this.getitemstring(this.getrow(), "cod_cliente")
				ls_cod_fornitore = this.getitemstring(this.getrow(), "cod_fornitore")
				ldt_data_registrazione = this.getitemdatetime(this.getrow(), "data_registrazione")
				ls_nota_testata = this.getitemstring(this.getrow(), "nota_testata")
				ls_nota_piede = this.getitemstring(this.getrow(), "nota_piede")
								
				if ls_flag_tipo_fat_ven = "F" then //fornitore
					if guo_functions.uof_get_note_fisse(ls_null, ls_cod_fornitore, ls_null, "FAT_VEN", ls_cod_tipo_fat_ven, ldt_data_registrazione, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
						g_mb.error(ls_nota_testata)
					else
						this.setitem(this.getrow(), "nota_testata", ls_nota_testata)
						this.setitem(this.getrow(), "nota_piede", ls_nota_piede + ls_nota_old )
					end if
				else			 // Cliente
					if guo_functions.uof_get_note_fisse(ls_cod_cliente, ls_null, ls_null, "FAT_VEN", ls_cod_tipo_fat_ven, ldt_data_registrazione, ref ls_nota_testata, ref ls_nota_piede, ref ls_nota_video) < 0 then
						g_mb.error(ls_nota_testata)
					else
						this.setitem(this.getrow(), "nota_testata", ls_nota_testata)
						this.setitem(this.getrow(), "nota_piede", ls_nota_piede + ls_nota_old )
					end if
				end if
				
				wf_inserisci_fattura_vendita( 0, ll_anno_registrazione, ll_num_registrazione)
			
			end if
			
			//se il flag_avviso_contrassegno è S allora avvisa l'utente di rigenerare il contrassegno
			uo_avviso_spedizioni luo_avviso_sped
			string ls_msg_avviso
			long ll_ret
			
			luo_avviso_sped = create uo_avviso_spedizioni
			
			luo_avviso_sped.il_anno_documento = ll_anno_registrazione
			luo_avviso_sped.il_num_documento = ll_num_registrazione
			luo_avviso_sped.is_tipo_gestione = "fat_ven"
			
			ll_ret = luo_avviso_sped.uof_check_flag_avviso_contrassegno(ls_msg_avviso)
			
			if ll_ret<0 then
				g_mb.error(ls_msg_avviso)
				
			elseif ll_ret=1 then
				g_mb.show(ls_msg_avviso)
			end if
		
			destroy luo_avviso_sped;
				//--------------------------------------------------------------------------------------------------
				
		end if
		
	next
	
end if


end event

type det_2 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 2752
integer height = 2356
long backcolor = 12632256
string text = "Destinazioni"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_tes_fat_ven_det_2 dw_tes_fat_ven_det_2
end type

on det_2.create
this.dw_tes_fat_ven_det_2=create dw_tes_fat_ven_det_2
this.Control[]={this.dw_tes_fat_ven_det_2}
end on

on det_2.destroy
destroy(this.dw_tes_fat_ven_det_2)
end on

type dw_tes_fat_ven_det_2 from uo_cs_xx_dw within det_2
integer width = 2670
integer height = 1740
integer taborder = 11
string dataobject = "d_tes_fat_ven_det_2_tv"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "ricerca_cliente"
		guo_ricerca.uof_ricerca_cliente(dw_tes_fat_ven_det_2,"cod_cliente")
end choose
end event

type det_3 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 2752
integer height = 2356
long backcolor = 12632256
string text = "Trasporto"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_tes_fat_ven_det_3 dw_tes_fat_ven_det_3
end type

on det_3.create
this.dw_tes_fat_ven_det_3=create dw_tes_fat_ven_det_3
this.Control[]={this.dw_tes_fat_ven_det_3}
end on

on det_3.destroy
destroy(this.dw_tes_fat_ven_det_3)
end on

type dw_tes_fat_ven_det_3 from uo_cs_xx_dw within det_3
integer width = 2711
integer height = 1964
integer taborder = 11
string dataobject = "d_tes_fat_ven_det_3_tv"
boolean border = false
end type

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_aspetto_beni"
		window_open(w_aspetto_beni, 0)
		dw_tes_fat_ven_det_3.setitem(dw_tes_fat_ven_det_3.getrow(),"aspetto_beni", s_cs_xx.parametri.parametro_s_1)		
end choose
end event

type det_4 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 2752
integer height = 2356
long backcolor = 12632256
string text = "Note"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_tes_fat_ven_det_4 dw_tes_fat_ven_det_4
end type

on det_4.create
this.dw_tes_fat_ven_det_4=create dw_tes_fat_ven_det_4
this.Control[]={this.dw_tes_fat_ven_det_4}
end on

on det_4.destroy
destroy(this.dw_tes_fat_ven_det_4)
end on

type dw_tes_fat_ven_det_4 from uo_cs_xx_dw within det_4
integer width = 2720
integer height = 2000
integer taborder = 11
string dataobject = "d_tes_fat_ven_det_4_tv"
boolean border = false
end type

event ue_key;call super::ue_key;string ls_nota_fissa

choose case this.getcolumnname()
	
	case "nota_testata"
		if key = keyF1!  and keyflags = 1 then
			s_cs_xx.parametri.parametro_s_1 = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"cod_cliente")
			s_cs_xx.parametri.parametro_s_2 = "%"
			s_cs_xx.parametri.parametro_s_3 = "%"
			s_cs_xx.parametri.parametro_s_4 = "%"
			s_cs_xx.parametri.parametro_s_5 = "%"
			s_cs_xx.parametri.parametro_s_6 = "S"
			s_cs_xx.parametri.parametro_s_7 = "%"
			s_cs_xx.parametri.parametro_s_8 = "%"
			s_cs_xx.parametri.parametro_s_9 = "T"
			s_cs_xx.parametri.parametro_data_1 = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemdatetime(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"data_registrazione")
			
			
			dw_tes_fat_ven_det_4.change_dw_current()
			setnull(s_cs_xx.parametri.parametro_s_11)
			window_open(w_ricerca_note_fisse, 0)
			if not isnull(s_cs_xx.parametri.parametro_s_11) then
				
				select nota_fissa
				  into :ls_nota_fissa
				  from tab_note_fisse
				 where cod_azienda = :s_cs_xx.cod_azienda
				   and cod_nota_fissa = :s_cs_xx.parametri.parametro_s_11;
				
				if not isnull(ls_nota_fissa) then
					if len(this.gettext()) > 0 then
						this.setcolumn("nota_testata")
						this.settext(this.gettext() + "~r~n" + ls_nota_fissa)
					else
						this.setcolumn("nota_testata")
						this.settext(ls_nota_fissa)
					end if	
				end if	
			end if
		end if
		
	case "nota_piede"
		if key = keyF1!  and keyflags = 1 then		
			s_cs_xx.parametri.parametro_s_1 = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemstring(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"cod_cliente")
			s_cs_xx.parametri.parametro_s_2 = "%"
			s_cs_xx.parametri.parametro_s_3 = "%"
			s_cs_xx.parametri.parametro_s_4 = "%"
			s_cs_xx.parametri.parametro_s_5 = "%"
			s_cs_xx.parametri.parametro_s_6 = "S"
			s_cs_xx.parametri.parametro_s_7 = "%"
			s_cs_xx.parametri.parametro_s_8 = "%"
			s_cs_xx.parametri.parametro_s_9 = "P"
			s_cs_xx.parametri.parametro_data_1 = tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getitemdatetime(tab_dettaglio.det_1.dw_tes_fat_ven_det_1.getrow(),"data_registrazione")
			
			
			dw_tes_fat_ven_det_4.change_dw_current()
			setnull(s_cs_xx.parametri.parametro_s_11)
			window_open(w_ricerca_note_fisse, 0)
			if not isnull(s_cs_xx.parametri.parametro_s_11) then
				
				select nota_fissa
				  into :ls_nota_fissa
				  from tab_note_fisse
				 where cod_azienda = :s_cs_xx.cod_azienda
				   and cod_nota_fissa = :s_cs_xx.parametri.parametro_s_11;
				
				if not isnull(ls_nota_fissa) then	
					if len(this.gettext()) > 0 then					
						this.setcolumn("nota_piede")
						this.settext(this.gettext() + "~r~n" + ls_nota_fissa)
					else	
						this.setcolumn("nota_piede")
						this.settext(ls_nota_fissa)
					end if	
				end if	
			end if
		end if	
		
end choose

end event

type det_5 from userobject within tab_dettaglio
integer x = 18
integer y = 108
integer width = 2752
integer height = 2356
long backcolor = 12632256
string text = "Totali"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_tes_fat_ven_det_5 dw_tes_fat_ven_det_5
end type

on det_5.create
this.dw_tes_fat_ven_det_5=create dw_tes_fat_ven_det_5
this.Control[]={this.dw_tes_fat_ven_det_5}
end on

on det_5.destroy
destroy(this.dw_tes_fat_ven_det_5)
end on

type dw_tes_fat_ven_det_5 from uo_cs_xx_dw within det_5
integer width = 2734
integer height = 2004
integer taborder = 11
string dataobject = "d_tes_fat_ven_det_5_tv"
boolean border = false
end type

type tabpage_1 from userobject within tab_ricerca
integer x = 18
integer y = 108
integer width = 1696
integer height = 2360
long backcolor = 12632256
string text = "SDI"
long tabtextcolor = 33554432
long tabbackcolor = 12632256
long picturemaskcolor = 536870912
dw_notifiche dw_notifiche
end type

on tabpage_1.create
this.dw_notifiche=create dw_notifiche
this.Control[]={this.dw_notifiche}
end on

on tabpage_1.destroy
destroy(this.dw_notifiche)
end on

type dw_notifiche from uo_std_dw within tabpage_1
event ue_leggi_lista_notifiche ( )
event ue_leggi_singola_notifica ( long al_row )
integer x = 5
integer y = 12
integer width = 1669
integer height = 2320
integer taborder = 30
string dataobject = "d_tes_fat_ven_notifiche_sdi"
boolean vscrollbar = true
boolean border = false
borderstyle borderstyle = stylebox!
end type

event ue_leggi_lista_notifiche();string		ls_messaggio, ls_str, ls_cod_documento,ls_numeratore_documento, ls_cod_cliente, ls_rag_soc_1, ls_testo_notifica
long 	ll_ret, ll_i, ll_row,ll_anno_registrazione,ll_num_registrazione,ll_anno_documento, ll_num_documento
datetime ldt_data_fattura
str_fatel_notifica lstr_notifiche[]
uo_fatel luo_fatel
uo_log_sistema luo_log		

luo_fatel = create uo_fatel
		
ll_ret = luo_fatel.uof_notifiche_lista( "A", ref ls_messaggio, ref lstr_notifiche[] )
destroy luo_fatel

choose case ll_ret
	case 1
		// Tutto OK, ma non ci sono notiche
		g_mb.show(ls_messaggio)
		return
	case -1
		// Errore nel caricamento lista notifiche
		g_mb.error(ls_messaggio)
		return
	case 0
		this.reset( )
		this.setredraw(false)
		for ll_i = 1 to upperbound(lstr_notifiche)
			
			ll_row = insertrow(0)
			
			select T.anno_registrazione,
					T.num_registrazione,
					T.cod_documento,
					T.numeratore_documento,
					T.anno_documento,
					T.num_documento,
					T.data_fattura,
					T.cod_cliente,
					A.rag_soc_1
			into	:ll_anno_registrazione,
					:ll_num_registrazione,
					:ls_cod_documento,
					:ls_numeratore_documento,
					:ll_anno_documento,
					:ll_num_documento,
					:ldt_data_fattura,
					:ls_cod_cliente,
					:ls_rag_soc_1
			from	tes_fat_ven T
			left join anag_clienti A on T.cod_azienda=A.cod_azienda and T.cod_cliente = A.cod_cliente
			where 	T.cod_azienda = :s_cs_xx.cod_azienda and
						T.identificativo_sdi = :lstr_notifiche[ll_i].identificativo_sdi;
			if sqlca.sqlcode = 0 then
				ls_str = g_str.format("Fattura $1/$2 - $3/$4    ", ls_cod_documento, ls_numeratore_documento, ll_anno_documento,ll_num_documento)
				ls_str = g_str.format("$1~r~nCliente ($2) $3    ", ls_str, ls_cod_cliente, ls_rag_soc_1)
				ls_str = g_str.format("$1~r~nIdentificativo SDI=$2 [ID interno=$3] ",ls_str, lstr_notifiche[ll_i].identificativo_sdi, lstr_notifiche[ll_i].id )
			else					
				ls_str = g_str.format("Identificativo SDI=$1 [ID interno=$2]",lstr_notifiche[ll_i].identificativo_sdi, lstr_notifiche[ll_i].identificativo_sdi )
			end if
			setitem(ll_row, 1, ls_str)
			setitem(ll_row, 2, long(lstr_notifiche[ll_i].id))
			
			if lstr_notifiche[ll_i].tipo="NS" then
				setitem(ll_row, 3, s_cs_xx.volume + s_cs_xx.risorse + "12.5\fatel_notifica_err.png")
			else
				setitem(ll_row, 3, s_cs_xx.volume + s_cs_xx.risorse + "12.5\fatel_notifica_ok.png")
			end if
		next
		
		tab_ricerca.tabpage_1.text = g_str.format("SDI ($1)",ll_i - 1)
		
		this.setredraw(true)
end choose		

end event

event ue_leggi_singola_notifica(long al_row);string ls_testo_notifica, ls_messaggio
long ll_ret
uo_fatel luo_fatel
uo_log_sistema luo_log

if al_row <= 0 then return
if getitemnumber(al_row, 2) >= 0 then			
	luo_fatel = create uo_fatel
	luo_log=CREATE uo_log_sistema
	ll_ret = luo_fatel.uof_notifica_fatture_attive_download_xml( getitemnumber(al_row, 2) , ref ls_testo_notifica, ref ls_messaggio)
	if ll_ret = -1 then
		// errore in download della notifica
		luo_log.uof_write_log_sistema( "FATEL", g_str.format("Errore in Download notifica $1.~r~n$2", getitemnumber(al_row, 2), ls_messaggio))
		g_mb.error(g_str.format("Errore in Download notifica $1.~r~n$2", getitemnumber(al_row, 2), ls_messaggio))
		destroy luo_fatel
		destroy luo_log
		return
	end if
	if ll_ret = -2 then
		// errore durante memo notifica in Apice
		luo_log.uof_write_log_sistema( "FATEL", g_str.format("Anomalia in lettura notifica  $1.~r~n$2", getitemnumber(al_row, 2), ls_messaggio))
		g_mb.warning(g_str.format("Anomalia in lettura notifica  $1.~r~n$2", getitemnumber(al_row, 2), ls_messaggio))
	end if
	
	luo_log.uof_write_log_sistema( "FATEL", ls_testo_notifica)
	g_mb.show("Notifica SDI",ls_testo_notifica)
	destroy luo_fatel
	destroy luo_log
	
	deleterow(al_row)
	// il commit è messo per il LOG
	commit;
end if

return
end event

event buttonclicked;call super::buttonclicked;string		ls_messaggio, ls_str, ls_cod_documento,ls_numeratore_documento, ls_cod_cliente, ls_rag_soc_1, ls_testo_notifica
long 	ll_ret, ll_i, ll_row,ll_anno_registrazione,ll_num_registrazione,ll_anno_documento, ll_num_documento, ll_cont
datetime ldt_data_fattura
str_fatel_notifica lstr_notifiche[]
uo_fatel luo_fatel
uo_log_sistema luo_log		

choose case dwo.name
	case "b_download_notifica"
		
		event ue_leggi_singola_notifica( row )

	case"b_esamina_notifiche"
		if rowcount() <= 0 then
			g_mb.show( "Nessuna notifica da esaminare: caricare prima le notifiche tramite l'opzione LEGGI NOTIFICHE" )
			return
		end if
		
		for ll_i = 1 to rowcount()
			
			if getitemnumber(ll_i, 2) >= 0 then			
				luo_fatel = create uo_fatel
				luo_log=CREATE uo_log_sistema
				ll_ret = luo_fatel.uof_notifica_fatture_attive_download_xml( getitemnumber(ll_i, 2) , ref ls_testo_notifica, ref ls_messaggio)
				if ll_ret = -1 then
					// errore in download della notifica
					luo_log.uof_write_log_sistema( "FATEL", g_str.format("Errore in Download notifica $1.~r~n$2", getitemnumber(ll_i, 2), ls_messaggio))
					g_mb.error(g_str.format("Errore in Download notifica $1.~r~n$2", getitemnumber(ll_i, 2), ls_messaggio))
					destroy luo_fatel
					destroy luo_log
					return
				end if
				if ll_ret = -2 then
					// errore durante memo notifica in Apice
					luo_log.uof_write_log_sistema( "FATEL", g_str.format("Anomalia in lettura notifica  $1.~r~n$2", getitemnumber(ll_i, 2), ls_messaggio))
					g_mb.warning(g_str.format("Anomalia in lettura notifica  $1.~r~n$2", getitemnumber(ll_i, 2), ls_messaggio))
				end if
				
				luo_log.uof_write_log_sistema( "FATEL", ls_testo_notifica)
				destroy luo_fatel
				destroy luo_log
				
				// il commit è messo per il LOG
				commit;
				Yield()
			end if
		next
		
		g_mb.show("Notifica SDI","Attenzione! Verificare la presenza di FATTURE SCARTATE dalla lista fatture")	
		
		event post ue_leggi_lista_notifiche( )

	case "b_notifiche"
		event post ue_leggi_lista_notifiche( )
		
end choose
end event

event doubleclicked;// evento commentato.
// non deve funzionare il doppio click
end event

