﻿$PBExportHeader$w_tes_fat_ven_duplica_nc.srw
forward
global type w_tes_fat_ven_duplica_nc from w_cs_xx_risposta
end type
type dw_tes_fat_ven_duplica_nc from datawindow within w_tes_fat_ven_duplica_nc
end type
end forward

global type w_tes_fat_ven_duplica_nc from w_cs_xx_risposta
integer width = 2281
integer height = 748
string title = "Genera Nota di Credito da Fattura."
boolean controlmenu = false
dw_tes_fat_ven_duplica_nc dw_tes_fat_ven_duplica_nc
end type
global w_tes_fat_ven_duplica_nc w_tes_fat_ven_duplica_nc

event pc_setwindow;call super::pc_setwindow;dw_tes_fat_ven_duplica_nc.insertrow(0)


end event

on w_tes_fat_ven_duplica_nc.create
int iCurrent
call super::create
this.dw_tes_fat_ven_duplica_nc=create dw_tes_fat_ven_duplica_nc
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tes_fat_ven_duplica_nc
end on

on w_tes_fat_ven_duplica_nc.destroy
call super::destroy
destroy(this.dw_tes_fat_ven_duplica_nc)
end on

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tes_fat_ven_duplica_nc, &
                 "cod_tipo_fat_ven_nc", &
                 sqlca, &
                 "tab_tipi_fat_ven", &
                 "cod_tipo_fat_ven", &
                 "des_tipo_fat_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and flag_tipo_fat_ven='N'  ")

end event

type dw_tes_fat_ven_duplica_nc from datawindow within w_tes_fat_ven_duplica_nc
integer x = 23
integer y = 20
integer width = 2217
integer height = 620
integer taborder = 10
string title = "none"
string dataobject = "d_tes_fat_ven_duplica_nc"
boolean border = false
boolean livescroll = true
end type

event buttonclicked;accepttext()
choose case dwo.name
	case "b_conferma"
		if len( getitemstring(1,1) ) < 1 then
			g_mb.show("Inserire un tipo nota di credito")
			return
		end if
		s_cs_xx.parametri.parametro_s_15 = getitemstring(1,1)
		
	case "b_annulla"
		s_cs_xx.parametri.parametro_s_15 =""
end choose

close(parent)

end event

