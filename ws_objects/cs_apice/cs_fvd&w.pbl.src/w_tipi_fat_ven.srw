﻿$PBExportHeader$w_tipi_fat_ven.srw
$PBExportComments$Finestra Gestione Tipi Fatture Vendita
forward
global type w_tipi_fat_ven from w_cs_xx_principale
end type
type dw_tipi_fat_ven_det_2 from uo_cs_xx_dw within w_tipi_fat_ven
end type
type cb_ok from commandbutton within w_tipi_fat_ven
end type
type dw_tipi_fat_ven_lista from uo_cs_xx_dw within w_tipi_fat_ven
end type
type dw_tipi_fat_ven_det from uo_cs_xx_dw within w_tipi_fat_ven
end type
type dw_folder from u_folder within w_tipi_fat_ven
end type
end forward

global type w_tipi_fat_ven from w_cs_xx_principale
integer width = 2587
integer height = 2684
string title = "Tipi Fatture Vendita"
dw_tipi_fat_ven_det_2 dw_tipi_fat_ven_det_2
cb_ok cb_ok
dw_tipi_fat_ven_lista dw_tipi_fat_ven_lista
dw_tipi_fat_ven_det dw_tipi_fat_ven_det
dw_folder dw_folder
end type
global w_tipi_fat_ven w_tipi_fat_ven

type variables

end variables

forward prototypes
public subroutine wf_abilita_flag_riferimento (string fs_flag_riferimento)
end prototypes

public subroutine wf_abilita_flag_riferimento (string fs_flag_riferimento);if fs_flag_riferimento = "N" then
	dw_tipi_fat_ven_det.object.cod_tipo_det_ven_rif_bol_ven.protect = 1
	dw_tipi_fat_ven_det.object.cod_tipo_det_ven_rif_bol_ven.border = 6
	dw_tipi_fat_ven_det.object.cod_tipo_det_ven_rif_bol_ven.background.mode = 1

	dw_tipi_fat_ven_det.object.des_riferimento_fat_ven.protect = 1
	dw_tipi_fat_ven_det.object.des_riferimento_fat_ven.border = 6
	dw_tipi_fat_ven_det.object.des_riferimento_fat_ven.background.mode = 1

	dw_tipi_fat_ven_det.object.flag_rif_cod_documento.protect = 1
	dw_tipi_fat_ven_det.object.flag_rif_numeratore_doc.protect = 1
	dw_tipi_fat_ven_det.object.flag_rif_anno_documento.protect = 1
	dw_tipi_fat_ven_det.object.flag_rif_num_documento.protect = 1
	dw_tipi_fat_ven_det.object.flag_rif_data_fattura.protect = 1
	
else
	dw_tipi_fat_ven_det.object.cod_tipo_det_ven_rif_bol_ven.protect = 0
	dw_tipi_fat_ven_det.object.cod_tipo_det_ven_rif_bol_ven.border = 5
	dw_tipi_fat_ven_det.object.cod_tipo_det_ven_rif_bol_ven.background.mode = 0

	dw_tipi_fat_ven_det.object.des_riferimento_fat_ven.protect = 0
	dw_tipi_fat_ven_det.object.des_riferimento_fat_ven.border = 5
	dw_tipi_fat_ven_det.object.des_riferimento_fat_ven.background.mode = 0

	dw_tipi_fat_ven_det.object.flag_rif_cod_documento.protect = 0
	dw_tipi_fat_ven_det.object.flag_rif_numeratore_doc.protect = 0
	dw_tipi_fat_ven_det.object.flag_rif_anno_documento.protect = 0
	dw_tipi_fat_ven_det.object.flag_rif_num_documento.protect = 0
	dw_tipi_fat_ven_det.object.flag_rif_data_fattura.protect = 0
end if


RETURN
end subroutine

event pc_setwindow;call super::pc_setwindow;windowobject			lw_oggetti[]


dw_tipi_fat_ven_lista.set_dw_key("cod_azienda")
dw_tipi_fat_ven_lista.set_dw_options(sqlca, &
                                     pcca.null_object, &
                                     c_default, &
                                     c_default)
dw_tipi_fat_ven_det.set_dw_options(sqlca, &
                                   dw_tipi_fat_ven_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)
dw_tipi_fat_ven_det_2.set_dw_options(sqlca, &
                                   dw_tipi_fat_ven_lista, &
                                   c_sharedata + c_scrollparent, &
                                   c_default)
iuo_dw_main = dw_tipi_fat_ven_lista

lw_oggetti[1] = dw_tipi_fat_ven_det
dw_folder.fu_assigntab(1, "Dettaglio", lw_oggetti[])

lw_oggetti[1] = dw_tipi_fat_ven_det_2
dw_folder.fu_assigntab(2, "Note Conformita", lw_oggetti[])

dw_folder.fu_foldercreate(2, 2)
dw_folder.fu_selecttab(1)
end event

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_tipi_fat_ven_det, &
                 "cod_tipo_det_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_fat_ven_det, &
                 "cod_tipo_analisi", &
                 sqlca, &
                 "tab_tipi_analisi", &
                 "cod_tipo_analisi", &
                 "des_tipo_analisi", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_fat_ven_det, &
                 "cod_documento", &
                 sqlca, &
                 "tab_documenti", &
                 "cod_documento", &
                 "des_documento", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_tipi_fat_ven_det, &
                 "cod_tipo_det_ven_rif_bol_ven", &
                 sqlca, &
                 "tab_tipi_det_ven", &
                 "cod_tipo_det_ven", &
                 "des_tipo_det_ven", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
f_po_loaddddw_dw(dw_tipi_fat_ven_det, &
                 "cod_causale", &
                 sqlca, &
                 "tab_causali_trasp", &
                 "cod_causale", &
                 "des_causale", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_tipi_fat_ven_det_2, &
					  "cod_tipo_det_ven_nota", &
					  sqlca, &
					  "tab_tipi_det_ven", &
					  "cod_tipo_det_ven", &
					  "des_tipo_det_ven", &
					  "cod_azienda = '" + s_cs_xx.cod_azienda + "' and "+&
					  "flag_tipo_det_ven not in ('M','C')")

if s_cs_xx.parametri.impresa then
	f_po_loaddddw_dw(dw_tipi_fat_ven_det, &
						  "profilo_impresa", &
						  sqlci, &
						  "prof_registrazione", &
						  "codice", &
						  "descrizione", &
						  "")
	f_po_loaddddw_dw(dw_tipi_fat_ven_det, &
						  "profilo_impresa_cee", &
						  sqlci, &
						  "prof_registrazione", &
						  "codice", &
						  "descrizione", &
						  "")
	f_po_loaddddw_dw(dw_tipi_fat_ven_det, &
						  "profilo_impresa_excee", &
						  sqlci, &
						  "prof_registrazione", &
						  "codice", &
						  "descrizione", &
						  "")
end if


end event

on w_tipi_fat_ven.create
int iCurrent
call super::create
this.dw_tipi_fat_ven_det_2=create dw_tipi_fat_ven_det_2
this.cb_ok=create cb_ok
this.dw_tipi_fat_ven_lista=create dw_tipi_fat_ven_lista
this.dw_tipi_fat_ven_det=create dw_tipi_fat_ven_det
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.dw_tipi_fat_ven_det_2
this.Control[iCurrent+2]=this.cb_ok
this.Control[iCurrent+3]=this.dw_tipi_fat_ven_lista
this.Control[iCurrent+4]=this.dw_tipi_fat_ven_det
this.Control[iCurrent+5]=this.dw_folder
end on

on w_tipi_fat_ven.destroy
call super::destroy
destroy(this.dw_tipi_fat_ven_det_2)
destroy(this.cb_ok)
destroy(this.dw_tipi_fat_ven_lista)
destroy(this.dw_tipi_fat_ven_det)
destroy(this.dw_folder)
end on

type dw_tipi_fat_ven_det_2 from uo_cs_xx_dw within w_tipi_fat_ven
integer x = 46
integer y = 660
integer width = 2423
integer height = 920
integer taborder = 30
string dataobject = "d_tipi_fat_ven_det_2"
boolean border = false
end type

type cb_ok from commandbutton within w_tipi_fat_ven
integer x = 2103
integer y = 440
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Moduli"
end type

event clicked;dw_tipi_fat_ven_lista.accepttext()
window_open_parm(w_tipi_fat_ven_lingue_dw, -1, dw_tipi_fat_ven_lista)
end event

type dw_tipi_fat_ven_lista from uo_cs_xx_dw within w_tipi_fat_ven
integer x = 23
integer y = 20
integer width = 2057
integer height = 500
integer taborder = 10
string dataobject = "d_tipi_fat_ven_lista"
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

on pcd_retrieve;call uo_cs_xx_dw::pcd_retrieve;long ll_errore


ll_errore = retrieve(s_cs_xx.cod_azienda)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end on

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	if (i_colname = "flag_abilita_rif_bol_ven") and (currentrow > 0)  then
		wf_abilita_flag_riferimento(this.getitemstring(currentrow,"flag_abilita_rif_bol_ven"))
	end if
end if
end event

event pcd_new;call super::pcd_new;dw_tipi_fat_ven_det.setitem(dw_tipi_fat_ven_lista.getrow(), "flag_st_note", "S")
end event

type dw_tipi_fat_ven_det from uo_cs_xx_dw within w_tipi_fat_ven
integer x = 23
integer y = 652
integer width = 2473
integer height = 1880
integer taborder = 20
string dataobject = "d_tipi_fat_ven_det"
boolean border = false
end type

event itemchanged;call super::itemchanged;date ld_nulla

time lt_nulla


setnull(ld_nulla)
setnull(lt_nulla)

if i_extendmode then
	
	if i_colname = "flag_abilita_rif_bol_ven" then
		wf_abilita_flag_riferimento(i_coltext)
	end if
	
	if i_colname = "flag_stampa_data_ora" then
		if i_coltext <> "F" then
			setitem(i_rownbr, "data_fissa", ld_nulla)
			setitem(i_rownbr, "ora_fissa", lt_nulla)
		end if
	end if
	
end if
end event

type dw_folder from u_folder within w_tipi_fat_ven
integer x = 14
integer y = 548
integer width = 2505
integer height = 1992
integer taborder = 30
boolean border = false
end type

