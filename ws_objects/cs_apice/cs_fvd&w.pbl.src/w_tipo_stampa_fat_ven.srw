﻿$PBExportHeader$w_tipo_stampa_fat_ven.srw
$PBExportComments$Finestra Tipo Stampa Fattura Vendita
forward
global type w_tipo_stampa_fat_ven from w_cs_xx_risposta
end type
type cbx_imposta_data from checkbox within w_tipo_stampa_fat_ven
end type
type st_3 from statictext within w_tipo_stampa_fat_ven
end type
type em_2 from editmask within w_tipo_stampa_fat_ven
end type
type st_2 from statictext within w_tipo_stampa_fat_ven
end type
type em_1 from editmask within w_tipo_stampa_fat_ven
end type
type st_1 from statictext within w_tipo_stampa_fat_ven
end type
type gb_1 from groupbox within w_tipo_stampa_fat_ven
end type
type rb_prova from radiobutton within w_tipo_stampa_fat_ven
end type
type rb_definitiva from radiobutton within w_tipo_stampa_fat_ven
end type
type cb_annulla from commandbutton within w_tipo_stampa_fat_ven
end type
type cb_ok from commandbutton within w_tipo_stampa_fat_ven
end type
type cbx_conferma from checkbox within w_tipo_stampa_fat_ven
end type
end forward

global type w_tipo_stampa_fat_ven from w_cs_xx_risposta
integer width = 978
integer height = 764
string title = "Tipo Stampa"
boolean controlmenu = false
boolean resizable = false
cbx_imposta_data cbx_imposta_data
st_3 st_3
em_2 em_2
st_2 st_2
em_1 em_1
st_1 st_1
gb_1 gb_1
rb_prova rb_prova
rb_definitiva rb_definitiva
cb_annulla cb_annulla
cb_ok cb_ok
cbx_conferma cbx_conferma
end type
global w_tipo_stampa_fat_ven w_tipo_stampa_fat_ven

type variables
long il_anno_fattura, il_num_fattura
end variables

forward prototypes
public function integer wf_cerca_data_fattura (ref date ad_data)
public function integer wf_verifica_ultimo_gg (datetime fdt_data_fattura, ref string fs_errore)
public function integer wf_controlla_buchi (string as_cod_documento, string as_numeratore_documento, long al_anno_esercizio, long al_num_documento, datetime adt_oggi)
end prototypes

public function integer wf_cerca_data_fattura (ref date ad_data);string ls_cod_tipo_fat_ven, ls_cod_deposito, ls_cod_documento, ls_numeratore_documento, &
		 ls_flag_blocco, ls_flag_fuori_fido, ls_tes_tabella, ls_det_tabella, ls_prog_riga, &
	  	 ls_quantita, ls_flag_calcolo, ls_flag_tipo_fat_ven, ls_messaggio, ls_flag_movimenti,&
		 ls_cod_doc_bolla,ls_num_doc_bolla
long   ll_num_documento, ll_anno_esercizio, ll_cont, ll_anno_bol_ven, ll_num_bol_ven,ll_anno_doc_bolla,ll_num_doc_bolla
date   ld_data
datetime ldt_oggi,ldt_data, ldt_data_bolla


ll_anno_esercizio = long (em_1.text)

select cod_tipo_fat_ven,
       cod_deposito,
		 flag_blocco,
		 flag_fuori_fido,
		 flag_calcolo,
		 flag_movimenti
into   :ls_cod_tipo_fat_ven,
       :ls_cod_deposito,
       :ls_flag_blocco,
       :ls_flag_fuori_fido,
       :ls_flag_calcolo,
		 :ls_flag_movimenti
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_fattura and
		 num_registrazione = :il_num_fattura;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca testata fattura. Dettaglio errore " + sqlca.sqlerrtext)
	return -1
end if

select cod_documento,   
		 numeratore_documento  
into   :ls_cod_documento,   
		 :ls_numeratore_documento  
from   anag_depositi  
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_deposito = :ls_cod_deposito;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Errore durante la lettura dell'Anagrafica Depositi.", exclamation!)
	return -1
end if

if isnull(ls_cod_documento) then
	
	select cod_documento,   
			 numeratore_documento,
			 flag_tipo_fat_ven
	into   :ls_cod_documento,   
			 :ls_numeratore_documento,
			 :ls_flag_tipo_fat_ven
	from   tab_tipi_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Errore durante la lettura della tabella tipi fatture di vendita.", exclamation!)
		return -1
	end if
end if

select data_ultima
into   :ld_data
from   numeratori  
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_documento = :ls_cod_documento and
		 numeratore_documento = :ls_numeratore_documento and
		 anno_documento = :ll_anno_esercizio;

if sqlca.sqlcode <> 0 then
	g_mb.messagebox("Attenzione", "Errore durante la ricerca num_documento in tabella numeratori.", exclamation!)
	return -1
end if

if isnull(ld_data) then
	ad_data = today()
else
	ad_data = ld_data
end if

return 0
end function

public function integer wf_verifica_ultimo_gg (datetime fdt_data_fattura, ref string fs_errore);boolean lb_imposta_decorrenza
string ls_flag_cdd, ls_sql, ls_flag_rc, ls_cod_giro_consegna
long ll_year, ll_month, ll_day, ll_ultimo_giorno,ll_rows,ll_i,ll_anno_reg_ord_ven,ll_num_reg_ord_ven, ll_day_number
date ld_data
datetime ldt_data_decorrenza
datastore lds_datastore

select flag
into   :ls_flag_cdd
from   parametri_azienda
where  cod_azienda = :s_cs_xx.cod_azienda and
		 cod_parametro = 'CDD';
		 
setnull(ldt_data_decorrenza)
		 
if sqlca.sqlcode <> 0 then ls_flag_cdd = "N"

if ls_flag_cdd = "S" then
	
	// Verifico se si tratta di un ritiro cliente o di un giro consegna
	ls_sql = "select distinct anno_reg_ord_ven, num_reg_ord_ven from det_fat_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_registrazione = " + string(il_anno_fattura) + " and num_registrazione = " + string(il_num_fattura) + " and anno_reg_ord_ven is not null "
	
	f_crea_datastore(lds_datastore,ls_sql)
	
	lds_datastore.settransobject(sqlca)
	
	ll_rows = lds_datastore.retrieve()
	
	for ll_i = 1 to ll_rows
		
		ll_anno_reg_ord_ven = lds_datastore.getitemnumber(ll_i, 1)
		ll_num_reg_ord_ven = lds_datastore.getitemnumber(ll_i, 2)
		setnull(ls_cod_giro_consegna)
		setnull(ls_flag_rc)
	
		select cod_giro_consegna
		into :ls_cod_giro_consegna
		from tes_ord_ven
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :ll_anno_reg_ord_ven and
				num_registrazione = :ll_num_reg_ord_ven;
				
		if isnull(ls_cod_giro_consegna) then continue
		
		select flag_rc
		into :ls_flag_rc
		from tes_giri_consegne
		where cod_azienda = :s_cs_xx.cod_azienda and
				cod_giro_consegna = :ls_cod_giro_consegna;
				
		if isnull(ls_flag_rc) then continue
		if isnull(ls_flag_rc) or ls_flag_rc = "S"  then continue
		
		lb_imposta_decorrenza = true
		exit
		
	next
	
	destroy lds_datastore
	
	
	
	if lb_imposta_decorrenza then

		ll_year = year( date(fdt_data_fattura) )
		ll_month = month( date(fdt_data_fattura) )
		ll_day =  day( date(fdt_data_fattura) )
		ld_data = date( fdt_data_fattura )
		
		ll_ultimo_giorno = guo_functions.uof_get_ultimo_giorno_mese(ll_month, ll_year)
			
		if ll_day = ll_ultimo_giorno then
			// Se è l'ultimo giorno del mese, calcolo il prossimo giorno lavorativo valido.
			guo_functions.uof_get_prossimo_giorno_lavorativo(ld_data, true, ld_data)
			ldt_data_decorrenza = datetime(ld_data, 00:00:00)
			
		elseif ((ll_day + 1) = ll_ultimo_giorno and (daynumber(relativedate(date(fdt_data_fattura), 1)) = 7 or daynumber(relativedate(date(fdt_data_fattura), 1)) = 1)) or &
				((ll_day + 2) = ll_ultimo_giorno and (daynumber(relativedate(date(fdt_data_fattura), 2)) = 7 or daynumber(relativedate(date(fdt_data_fattura),2)) = 1)) then
			// Se è il penultimo giorno del mese e il successivo (ultimo del mese) è un sabato o una domenica oppure
			// Se è il terzultimo del mese e dopo due giorni (ultimo del mese) è un sabato o una domenica oppure
			// calcolo il prossimo giorno lavorativo valido.
			guo_functions.uof_get_prossimo_giorno_lavorativo(ld_data, true, ld_data)
			ldt_data_decorrenza = datetime(ld_data, 00:00:00)
		end if
						
		update tes_fat_ven
		set data_decorrenza_pagamento = :ldt_data_decorrenza
		where cod_azienda = :s_cs_xx.cod_azienda and
				anno_registrazione = :il_anno_fattura and
				num_registrazione = :il_num_fattura;
		
		if sqlca.sqlcode <> 0 then
			fs_errore = "Errore in impostazione data decorrenza pagamento in testata fattura. " + sqlca.sqlerrtext
			return -1
		end if
		
	end if

end if

return 0
end function

public function integer wf_controlla_buchi (string as_cod_documento, string as_numeratore_documento, long al_anno_esercizio, long al_num_documento, datetime adt_oggi);string ls_select, ls_errore
long ll_i, ll_rows, ll_numero_fattura, ll_numero_fattura_prec
datetime ldt_data_fattura, ldt_data_fattura_prec
datastore lds_fatture

ls_select = "select num_documento, data_fattura from tes_fat_ven where cod_azienda = '" + s_cs_xx.cod_azienda + "' and anno_documento = " + string(al_anno_esercizio) + " and cod_documento='" + as_cod_documento + "' and numeratore_documento='" + as_numeratore_documento + "' order by num_documento, data_fattura "

if guo_functions.uof_crea_datastore ( ref lds_fatture,  ls_select ) < 0 then
	g_mb.error("Errore nella creazione del datastore nella funzione di controllo buchi")
	return 0
end if

ll_rows = lds_fatture.rowcount()

for ll_i = 1 to ll_rows
	
	ll_numero_fattura = lds_fatture.getitemnumber(ll_i, 1)
	ldt_data_fattura = lds_fatture.getitemdatetime(ll_i, 2)
	
	if ll_i = 1 then
		ll_numero_fattura_prec = ll_numero_fattura
		ldt_data_fattura_prec = ldt_data_fattura
	else
		if (ll_numero_fattura - ll_numero_fattura_prec) <> 1 then
			ls_errore = "Attenzione!! Non vi è congruenza di numerazione nella sequenza fatture nella serie fatture " + as_cod_documento + " / " + as_numeratore_documento + " fra il numero fattura " + string(ll_numero_fattura_prec) + " ed il numero " + string(ll_numero_fattura)
			f_scrivi_log(ls_errore)
			if g_mb.messagebox("Apice", ls_errore + " PROSEGUO LO STESSO ?", Question!, YesNo!, 2) = 2 then
				destroy lds_fatture
				return -1
			end if
		end if
		
		if ldt_data_fattura < ldt_data_fattura_prec then
			ls_errore = "Attenzione!! Non vi è congruenza di date nella sequenza fatture nella serie fatture " + as_cod_documento + " / " + as_numeratore_documento + " fra il numero fattura " + string(ll_numero_fattura_prec) + " ed il numero " + string(ll_numero_fattura)
			f_scrivi_log(ls_errore)
			if g_mb.messagebox("Apice", ls_errore + " PROSEGUO LO STESSO ?", Question!, YesNo!, 2) = 2 then
				destroy lds_fatture
				return -1
			end if
		end if
		
		ll_numero_fattura_prec = ll_numero_fattura
		ldt_data_fattura_prec = ldt_data_fattura
		
	end if
	
next

destroy lds_fatture

return 0
end function

on w_tipo_stampa_fat_ven.create
int iCurrent
call super::create
this.cbx_imposta_data=create cbx_imposta_data
this.st_3=create st_3
this.em_2=create em_2
this.st_2=create st_2
this.em_1=create em_1
this.st_1=create st_1
this.gb_1=create gb_1
this.rb_prova=create rb_prova
this.rb_definitiva=create rb_definitiva
this.cb_annulla=create cb_annulla
this.cb_ok=create cb_ok
this.cbx_conferma=create cbx_conferma
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cbx_imposta_data
this.Control[iCurrent+2]=this.st_3
this.Control[iCurrent+3]=this.em_2
this.Control[iCurrent+4]=this.st_2
this.Control[iCurrent+5]=this.em_1
this.Control[iCurrent+6]=this.st_1
this.Control[iCurrent+7]=this.gb_1
this.Control[iCurrent+8]=this.rb_prova
this.Control[iCurrent+9]=this.rb_definitiva
this.Control[iCurrent+10]=this.cb_annulla
this.Control[iCurrent+11]=this.cb_ok
this.Control[iCurrent+12]=this.cbx_conferma
end on

on w_tipo_stampa_fat_ven.destroy
call super::destroy
destroy(this.cbx_imposta_data)
destroy(this.st_3)
destroy(this.em_2)
destroy(this.st_2)
destroy(this.em_1)
destroy(this.st_1)
destroy(this.gb_1)
destroy(this.rb_prova)
destroy(this.rb_definitiva)
destroy(this.cb_annulla)
destroy(this.cb_ok)
destroy(this.cbx_conferma)
end on

event pc_setwindow;call super::pc_setwindow;il_anno_fattura = s_cs_xx.parametri.parametro_d_1
il_num_fattura = s_cs_xx.parametri.parametro_d_2



end event

type cbx_imposta_data from checkbox within w_tipo_stampa_fat_ven
integer x = 521
integer y = 576
integer width = 101
integer height = 84
integer textsize = -9
integer weight = 400
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
boolean enabled = false
end type

type st_3 from statictext within w_tipo_stampa_fat_ven
integer x = 41
integer y = 588
integer width = 471
integer height = 92
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Imposta Data:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_2 from editmask within w_tipo_stampa_fat_ven
integer x = 521
integer y = 476
integer width = 425
integer height = 84
integer taborder = 13
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
string text = "none"
borderstyle borderstyle = stylelowered!
maskdatatype maskdatatype = datemask!
string mask = "dd/mm/yyyy"
boolean spin = true
string minmax = "01/01/1900~~31/12/2999"
end type

event getfocus;SelectText(1,len(this.text))
end event

type st_2 from statictext within w_tipo_stampa_fat_ven
integer x = 41
integer y = 476
integer width = 471
integer height = 92
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Data Documento:"
alignment alignment = right!
boolean focusrectangle = false
end type

type em_1 from editmask within w_tipo_stampa_fat_ven
integer x = 521
integer y = 364
integer width = 288
integer height = 84
integer taborder = 13
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 33554432
boolean enabled = false
string text = "none"
alignment alignment = center!
borderstyle borderstyle = stylelowered!
string mask = "0000"
boolean spin = true
string minmax = "1900~~2999"
end type

event getfocus;SelectText(1,len(this.text))
end event

type st_1 from statictext within w_tipo_stampa_fat_ven
integer x = 41
integer y = 364
integer width = 471
integer height = 92
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 16711680
long backcolor = 12632256
string text = "Anno Documento:"
alignment alignment = right!
boolean focusrectangle = false
end type

type gb_1 from groupbox within w_tipo_stampa_fat_ven
integer x = 23
integer y = 20
integer width = 923
integer height = 220
integer taborder = 10
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Tipo Stampa"
end type

type rb_prova from radiobutton within w_tipo_stampa_fat_ven
integer x = 110
integer y = 80
integer width = 361
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Prova"
boolean checked = true
boolean lefttext = true
end type

event clicked;cbx_conferma.enabled = false
cbx_conferma.checked = false
em_1.enabled = false
em_2.enabled = false
cbx_imposta_data.enabled = false
end event

type rb_definitiva from radiobutton within w_tipo_stampa_fat_ven
integer x = 498
integer y = 80
integer width = 361
integer height = 76
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
string text = "Definitiva"
boolean lefttext = true
end type

event clicked;date ld_data

em_1.text = string(f_anno_esercizio())
wf_cerca_data_fattura(ref ld_data)
em_2.text = string(ld_data)

cbx_conferma.enabled = true
em_1.enabled = true
em_2.enabled = true
cbx_imposta_data.enabled = true
end event

type cb_annulla from commandbutton within w_tipo_stampa_fat_ven
integer x = 27
integer y = 260
integer width = 366
integer height = 80
integer taborder = 20
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&Annulla"
end type

event clicked;s_cs_xx.parametri.parametro_i_1 = -1

close(parent)
end event

type cb_ok from commandbutton within w_tipo_stampa_fat_ven
integer x = 581
integer y = 252
integer width = 366
integer height = 80
integer taborder = 3
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "&OK"
end type

event clicked;string											ls_cod_tipo_fat_ven, ls_cod_deposito, ls_cod_documento, ls_numeratore_documento, &
												ls_flag_blocco, ls_flag_fuori_fido, ls_tes_tabella, ls_det_tabella, ls_prog_riga, &
												ls_quantita, ls_flag_calcolo, ls_flag_tipo_fat_ven, ls_messaggio, ls_flag_movimenti,&
												ls_cod_doc_bolla,ls_num_doc_bolla, ls_errore
long											ll_num_documento, ll_anno_esercizio, ll_cont, ll_anno_bol_ven, ll_num_bol_ven,ll_anno_doc_bolla,ll_num_doc_bolla, ll_ret
date											ld_data
datetime										ldt_oggi,ldt_data, ldt_data_bolla, ldt_data_decorrenza
uo_generazione_documenti				luo_gen_doc
integer										li_ret


if rb_definitiva.checked then
	ld_data = date(em_2.text)
	if ld_data <= date("01/01/1900") or isnull(ld_data) then
		g_mb.messagebox("APICE","Data documento non congrua !")
		return
	end if
	ldt_oggi = datetime(ld_data,00:00:00)
	
	ll_anno_esercizio = long(em_1.text)
	if ll_anno_esercizio = 0 or isnull(ll_anno_esercizio) then
		g_mb.messagebox("APICE","Anno documento non congruo !")
		return
	end if
	
	if ll_anno_esercizio <> f_anno_esercizio() then
		if g_mb.messagebox("APICE","Attenzione !!! Anno documento diverso dall'esercizio corrente.~r~nProseguo ?",question!,yesno!,2) = 2 then
			return
		end if
	end if
	
end if

select cod_tipo_fat_ven,
       cod_deposito,
		 flag_blocco,
		 flag_fuori_fido,
		 flag_calcolo,
		 flag_movimenti
into   :ls_cod_tipo_fat_ven,
       :ls_cod_deposito,
       :ls_flag_blocco,
       :ls_flag_fuori_fido,
       :ls_flag_calcolo,
		 :ls_flag_movimenti
from   tes_fat_ven
where  cod_azienda = :s_cs_xx.cod_azienda and
       anno_registrazione = :il_anno_fattura and
		 num_registrazione = :il_num_fattura;
if sqlca.sqlcode <> 0 then
	g_mb.messagebox("APICE","Errore in ricerca testata fattura. Dettaglio errore " + sqlca.sqlerrtext)
	return
end if


if rb_definitiva.checked then
	
	// *** controllo che non vi sianoi bolle con data bolla superiore alla data della fattura ***
	
	declare cu_data_bolla cursor for
	select anno_registrazione_bol_ven, num_registrazione_bol_ven
	from   det_fat_ven
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 anno_registrazione = :il_anno_fattura and
		 	 num_registrazione  = :il_num_fattura and
	       anno_registrazione_bol_ven is not null 
	group by anno_registrazione_bol_ven, num_registrazione_bol_ven;	
	
	open cu_data_bolla;
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("APICE", "Errore in apertura del cursore cu_data_bolla.~r~n" + sqlca.sqlerrtext, exclamation!)
		s_cs_xx.parametri.parametro_i_1 = -1
		rollback;
		close(parent)
		return
	end if
		
	do while true	
		fetch cu_data_bolla into :ll_anno_bol_ven, :ll_num_bol_ven;
		if sqlca.sqlcode = 100 then exit
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE", "Errore in fecth del cursore cu_data_bolla.~r~n" + sqlca.sqlerrtext, exclamation!)
			s_cs_xx.parametri.parametro_i_1 = -1
			rollback;
			close(parent)
			return
		end if
		
		select cod_documento, 
		       anno_documento, 
				 numeratore_documento, 
				 num_documento, 
				 data_bolla
		into   :ls_cod_doc_bolla, 
		       :ll_anno_doc_bolla, 
				 :ls_num_doc_bolla, 
				 :ll_num_doc_bolla, 
				 :ldt_data_bolla
		from   tes_bol_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       anno_registrazione = :ll_anno_bol_ven and
				 num_registrazione = :ll_num_bol_ven;
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("APICE", "Errore in ricerca della bolla reg." + string(ll_anno_bol_ven) + "/" + string(ll_num_bol_ven) + ".~r~n" + sqlca.sqlerrtext, exclamation!)
			s_cs_xx.parametri.parametro_i_1 = -1
			rollback;
			close(parent)
			return
		end if
		
		if ldt_data_bolla > ldt_oggi then
			if g_mb.messagebox("APICE","Attenzione !!! La bolla " + ls_cod_doc_bolla + "-" + string(ll_anno_doc_bolla) + "/" + ls_num_doc_bolla + "-" + string(ll_num_doc_bolla) + " ha la data successiva alla data di questa fattura.~r~nPROSEGUO LO STESSO?",Question!,YesNo!,2) = 2 then
				s_cs_xx.parametri.parametro_i_1 = -1
				rollback;
				close(parent)
				return
			end if
		end if
	loop
	
	close cu_data_bolla;
	
	commit;
	
	// *******************  fine controllo data fattura *******************************************
	
	if ls_flag_blocco = 'S' then
		g_mb.messagebox("Attenzione", "Fattura Bloccata non stampabile in definitivo.", exclamation!)
		s_cs_xx.parametri.parametro_i_1 = -1
		rollback;
		close(parent)
		return
	end if
	
//	cOMMENTATO SU RICHIESTA DI BEATRICE 11-07-2013
//	if ls_flag_fuori_fido = 'S' then
//		g_mb.messagebox("Attenzione", "Fattura con Cliente Fuori Fido non stampabile in definitivo.", exclamation!)
//		s_cs_xx.parametri.parametro_i_1 = -1
//		rollback;
//		close(parent)
//		return
//	end if

setnull(ls_cod_documento)
/*	
	select cod_documento,   
			 numeratore_documento  
	into     :ls_cod_documento,   
			 :ls_numeratore_documento  
	from   anag_depositi  
	where cod_azienda = :s_cs_xx.cod_azienda and
			 cod_deposito = :ls_cod_deposito;
	
	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Errore durante la lettura dell'Anagrafica Depositi.", exclamation!)
		s_cs_xx.parametri.parametro_i_1 = -1
		rollback;
		close(parent)
		return
	end if
*/
	if isnull(ls_cod_documento) then
		select cod_documento,   
             		numeratore_documento,
             		flag_tipo_fat_ven
	   	into   :ls_cod_documento,   
             		:ls_numeratore_documento,
				:ls_flag_tipo_fat_ven
      	from   tab_tipi_fat_ven
      	where tab_tipi_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and
             		tab_tipi_fat_ven.cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Errore durante la lettura della tabella tipi fatture di vendita.", exclamation!)
			s_cs_xx.parametri.parametro_i_1 = -1
			rollback;
			close(parent)
			return
		end if
	end if

	select max(num_documento)
	into   :ll_num_documento  
	from   numeratori  
	where  cod_azienda = :s_cs_xx.cod_azienda and
			 cod_documento = :ls_cod_documento and
			 numeratore_documento = :ls_numeratore_documento and
			 anno_documento = :ll_anno_esercizio;

	if sqlca.sqlcode = -1 then
		g_mb.messagebox("Attenzione", "Errore durante la lettura della tabella numeratori.", exclamation!)
		s_cs_xx.parametri.parametro_i_1 = -1
		rollback;
		close(parent)
		return
		
	elseif isnull(ll_num_documento) then		// non cè la
		ll_num_documento  = 1
		ll_cont = 0
		
		select count(*)
		into   :ll_cont
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_documento = :ls_cod_documento and
				 numeratore_documento = :ls_numeratore_documento and
				 anno_documento = :ll_anno_esercizio and
				 num_documento = :ll_num_documento;
		if ll_cont > 0 and not isnull(ll_cont) then
			if g_mb.messagebox("APICE","Attenzione: numeratore già usato in un'altra fattura.~r~nAccetto ???",Question!,YesNo!,2) = 2 then
				s_cs_xx.parametri.parametro_i_1 = -1
				rollback;
				close(parent)
				return
			end if		
		end if
		
		// A questo punto ho ottenuto il numero fattura.
		// Per sicurezza vado a verificare che non vi siano buchi nella numerazione della serie ed eventualmente lo segnalo all'utente
		// Controllo anche 
		select	flag_tipo_fat_ven
	   	into		:ls_flag_tipo_fat_ven
      	from		tab_tipi_fat_ven
      	where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
					
		// nessun controllo buchi per le pro-forma
		if ls_flag_tipo_fat_ven <> "P" then
			
//			if wf_controlla_buchi(ls_cod_documento, ls_numeratore_documento, ll_anno_esercizio, ll_num_documento, ldt_oggi) < 0 then
//				s_cs_xx.parametri.parametro_i_1 = -1
//				rollback;
//				close(parent)
//				return
//			end if
			luo_gen_doc = create uo_generazione_documenti
			li_ret = luo_gen_doc.uof_controlla_buchi("fat_ven", ls_cod_documento,ls_numeratore_documento, ll_anno_esercizio, ll_num_documento, ldt_oggi, ls_errore)
			destroy luo_gen_doc
			
			if li_ret<0 then
				//errore critico nel controllo
				rollback;
				g_mb.error(ls_errore)
				s_cs_xx.parametri.parametro_i_1 = -1
				close(parent)
				return
			
			elseif li_ret = 1 then
				//hai scelto di annullare il proseguo al seguito di incongruenze in numerazione consecutiva o data bolla
				s_cs_xx.parametri.parametro_i_1 = -1
				rollback;
				close(parent)
				return
			end if
			
		end if
		
		// a questo punto procedo con assegnazione numero ufficiale al documento
		

		insert into numeratori 
						(cod_azienda,   
						 cod_documento,   
						 numeratore_documento,   
						 anno_documento,   
						 num_documento,
						 data_ultima)  
		values 		(:s_cs_xx.cod_azienda,   
						 :ls_cod_documento,   
						 :ls_numeratore_documento,   
						 :ll_anno_esercizio,   
						 :ll_num_documento,
						 :ldt_oggi);

		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Errore durante l'inserimento del numeratore.", exclamation!)
			s_cs_xx.parametri.parametro_i_1 = -1
			rollback;
			close(parent)
			return
		end if
		
	elseif not isnull(ll_num_documento) then
		
		ll_num_documento ++
		ll_cont = 0
		
		select count(*)
		into   :ll_cont
		from   tes_fat_ven
		where  cod_azienda = :s_cs_xx.cod_azienda and
		       cod_documento = :ls_cod_documento and
				 numeratore_documento = :ls_numeratore_documento and
				 anno_documento = :ll_anno_esercizio and
				 num_documento = :ll_num_documento;
		if ll_cont > 0 and not isnull(ll_cont) then
			if g_mb.messagebox("APICE","Attenzione: numeratore già usato in un'altra fattura.~r~nAccetto ???",Question!,YesNo!,2) = 2 then
				s_cs_xx.parametri.parametro_i_1 = -1
				rollback;
				close(parent)
				return
			end if		
		end if
		
		// A questo punto ho ottenuto il numero fattura.
		// Per sicurezza vado a verificare che non vi siano buchi nella numerazione della serie ed eventualmente lo segnalo all'utente
		// Controllo anche 
		select	flag_tipo_fat_ven
	   	into		:ls_flag_tipo_fat_ven
      	from		tab_tipi_fat_ven
      	where 	cod_azienda = :s_cs_xx.cod_azienda and
					cod_tipo_fat_ven = :ls_cod_tipo_fat_ven;
					
		// nessun controllo buchi per le pro-forma
		if ls_flag_tipo_fat_ven <> "P" then
			
//			if wf_controlla_buchi(ls_cod_documento, ls_numeratore_documento, ll_anno_esercizio, ll_num_documento, ldt_oggi) < 0 then
//				s_cs_xx.parametri.parametro_i_1 = -1
//				rollback;
//				close(parent)
//				return
//			end if
			luo_gen_doc = create uo_generazione_documenti
			li_ret = luo_gen_doc.uof_controlla_buchi("fat_ven", ls_cod_documento,ls_numeratore_documento, ll_anno_esercizio, ll_num_documento, ldt_oggi, ls_errore)
			destroy luo_gen_doc

			if li_ret<0 then
				//errore critico nel controllo
				rollback;
				g_mb.error(ls_errore)
				s_cs_xx.parametri.parametro_i_1 = -1
				close(parent)
				return
			
			elseif li_ret = 1 then
				//hai scelto di annullare il proseguo al seguito di incongruenze in numerazione consecutiva o data bolla
				s_cs_xx.parametri.parametro_i_1 = -1
				rollback;
				close(parent)
				return
			end if
		
		end if
		// a questo punto procedo con assegnazione numero ufficiale al documento

		if cbx_imposta_data.checked then
			// memorizzo anche la data ultimo documento
			update numeratori
			set    	num_documento = :ll_num_documento,
					data_ultima = :ldt_oggi
			where  numeratori.cod_azienda = :s_cs_xx.cod_azienda and
					 numeratori.cod_documento = :ls_cod_documento and
					 numeratori.numeratore_documento = :ls_numeratore_documento and
					 numeratori.anno_documento = :ll_anno_esercizio;
					 
		else
			
			update numeratori
			set    	num_documento = :ll_num_documento
			where  numeratori.cod_azienda = :s_cs_xx.cod_azienda and
					 numeratori.cod_documento = :ls_cod_documento and
					 numeratori.numeratore_documento = :ls_numeratore_documento and
					 numeratori.anno_documento = :ll_anno_esercizio;
					 
		end if
		
		if sqlca.sqlcode <> 0 then
			g_mb.messagebox("Attenzione", "Errore durante l'aggiornamento della tabella numeratori.", exclamation!)
			s_cs_xx.parametri.parametro_i_1 = -1
			rollback;
			close(parent)
			return
		end if

		
	end if

	update tes_fat_ven
	set    cod_documento = :ls_cod_documento,   
		    numeratore_documento = :ls_numeratore_documento,   
		    anno_documento = :ll_anno_esercizio,   
		    num_documento = :ll_num_documento,   
		    data_fattura = :ldt_oggi
	where  tes_fat_ven.cod_azienda = :s_cs_xx.cod_azienda and
			 tes_fat_ven.anno_registrazione = :il_anno_fattura and
			 tes_fat_ven.num_registrazione = :il_num_fattura;

	if sqlca.sqlcode <> 0 then
		g_mb.messagebox("Attenzione", "Errore durante l'aggiornamento della testata bolla.", exclamation!)
		s_cs_xx.parametri.parametro_i_1 = -1
		rollback;
		close(parent)
		return
	end if
	
	f_scrivi_log("Assegnazione numero fiscale alla FATTURA VENDITA [" + string(il_anno_fattura) + "/" + string(il_num_fattura) + "]  FISCALE: "&
					+ ls_cod_documento + "-" +  ls_numeratore_documento + "/" +  string(ll_anno_esercizio) + "-" + string(ll_num_documento) )

	// se la fattura non è differita eseguo movimenti di magazzino ossia la conferma della fattura
	if ls_flag_tipo_fat_ven <> "D" then
	
		ls_tes_tabella = "tes_fat_ven"
		ls_det_tabella = "det_fat_ven"
		ls_prog_riga = "prog_riga_fat_ven"
		ls_quantita = "quan_fatturata"
	
		if cbx_conferma.checked = true and ls_flag_movimenti <> "S" then
			if f_conferma_mov_mag(ls_tes_tabella, &
										 ls_det_tabella, &
										 ls_prog_riga, &
										 ls_quantita, &
										 il_anno_fattura, &
										 il_num_fattura, &
										 ls_messaggio) = -1 then
				g_mb.messagebox("Conferma Fattura","Errore durante conferma fattura.~r~nDettaglio errore" + ls_messaggio, StopSign!)
				s_cs_xx.parametri.parametro_i_1 = -1
				close(parent)
				return
			end if
			
			update tes_fat_ven
			set    flag_movimenti = 'S'
			where  cod_azienda = :s_cs_xx.cod_azienda and
			       anno_registrazione = :il_anno_fattura and
					 num_registrazione = :il_num_fattura;
			if sqlca.sqlcode <> 0 then
				g_mb.messagebox("Attenzione", "Errore in fase di aggiornamento flag_movimenti in testata fattura. Dettaglio:" + sqlca.sqlerrtext, exclamation!)
				s_cs_xx.parametri.parametro_i_1 = -1
				rollback;
				close(parent)
				return
			end if
		end if
	end if
	
	// ********* 02/00/2012 aggiunto per Gibus *********************
	// se la data del documento è fine mese e si tratta di una consegna ad un cliente che appartiene ad un giro, 
	// allora la data decorrenza viene impostata al giorno successivo del mese
	ll_ret = wf_verifica_ultimo_gg(ldt_oggi, ref ls_errore)
	
	if ll_ret < 0 then
		g_mb.messagebox("APICE",ls_errore)
		s_cs_xx.parametri.parametro_i_1 = -1
		rollback;
		close(parent)
		return
	end if
	
end if

s_cs_xx.parametri.parametro_i_1 = 0
commit;

close(parent)
end event

type cbx_conferma from checkbox within w_tipo_stampa_fat_ven
integer x = 498
integer y = 160
integer width = 366
integer height = 60
boolean bringtotop = true
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
long textcolor = 8388608
long backcolor = 12632256
boolean enabled = false
string text = "Conferma"
boolean lefttext = true
end type

