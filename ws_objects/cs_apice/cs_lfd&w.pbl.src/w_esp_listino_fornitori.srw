﻿$PBExportHeader$w_esp_listino_fornitori.srw
$PBExportComments$Window Esportazione Listino Fornitori
forward
global type w_esp_listino_fornitori from w_cs_xx_principale
end type
type cb_1 from uo_cb_close within w_esp_listino_fornitori
end type
type gb_1 from groupbox within w_esp_listino_fornitori
end type
type rb_totale from radiobutton within w_esp_listino_fornitori
end type
type rb_selezionati from radiobutton within w_esp_listino_fornitori
end type
type cb_esporta from uo_cb_ok within w_esp_listino_fornitori
end type
type ddlb_fornitori from dropdownlistbox within w_esp_listino_fornitori
end type
type ddlb_cat_mer from dropdownlistbox within w_esp_listino_fornitori
end type
type st_2 from statictext within w_esp_listino_fornitori
end type
type st_3 from statictext within w_esp_listino_fornitori
end type
type st_file_listino from statictext within w_esp_listino_fornitori
end type
type st_4 from statictext within w_esp_listino_fornitori
end type
type st_5 from statictext within w_esp_listino_fornitori
end type
type st_file_um from statictext within w_esp_listino_fornitori
end type
type st_7 from statictext within w_esp_listino_fornitori
end type
type st_file_valute from statictext within w_esp_listino_fornitori
end type
type cb_aggiungi from commandbutton within w_esp_listino_fornitori
end type
type cb_togli from commandbutton within w_esp_listino_fornitori
end type
type st_ist_1 from statictext within w_esp_listino_fornitori
end type
type st_ist_2 from statictext within w_esp_listino_fornitori
end type
type st_ist_3 from statictext within w_esp_listino_fornitori
end type
type st_ist_4 from statictext within w_esp_listino_fornitori
end type
type dw_esportazione_prodotti_finale from uo_cs_xx_dw within w_esp_listino_fornitori
end type
type dw_esportazione_prodotti_orig from uo_cs_xx_dw within w_esp_listino_fornitori
end type
type dw_folder from u_folder within w_esp_listino_fornitori
end type
end forward

global type w_esp_listino_fornitori from w_cs_xx_principale
int Width=2295
int Height=1405
boolean TitleBar=true
string Title="Esportazione Listino Prodotti"
cb_1 cb_1
gb_1 gb_1
rb_totale rb_totale
rb_selezionati rb_selezionati
cb_esporta cb_esporta
ddlb_fornitori ddlb_fornitori
ddlb_cat_mer ddlb_cat_mer
st_2 st_2
st_3 st_3
st_file_listino st_file_listino
st_4 st_4
st_5 st_5
st_file_um st_file_um
st_7 st_7
st_file_valute st_file_valute
cb_aggiungi cb_aggiungi
cb_togli cb_togli
st_ist_1 st_ist_1
st_ist_2 st_ist_2
st_ist_3 st_ist_3
st_ist_4 st_ist_4
dw_esportazione_prodotti_finale dw_esportazione_prodotti_finale
dw_esportazione_prodotti_orig dw_esportazione_prodotti_orig
dw_folder dw_folder
end type
global w_esp_listino_fornitori w_esp_listino_fornitori

type variables

end variables

forward prototypes
public function integer wf_controllo_prodotto (string fs_cod_prodotto)
public function integer wf_elimina (string fs_errore)
public function integer wf_elimina_orig (string fs_errore)
end prototypes

public function integer wf_controllo_prodotto (string fs_cod_prodotto);long ll_riga
string ls_cod_prodotto

for ll_riga= 1 to dw_esportazione_prodotti_finale.rowcount()
	ls_cod_prodotto=dw_esportazione_prodotti_finale.getitemstring(ll_riga,"cod_prodotto")
	if ls_cod_prodotto=fs_cod_prodotto then return -1
next

return 0
end function

public function integer wf_elimina (string fs_errore);string ls_elimina
long ll_numero_righe,ll_i,ll_conteggio
integer li_test

li_test=1
do while li_test = 1

	ll_numero_righe = dw_esportazione_prodotti_finale.rowcount()

	for ll_i = 1 to ll_numero_righe
		ls_elimina = dw_esportazione_prodotti_finale.getitemstring(ll_i,"elimina")
		if ls_elimina="S" then
			dw_esportazione_prodotti_finale.Delete_DW_Row(ll_i, c_ignoreChanges,c_noRefreshChildren)
			ll_i=ll_numero_righe
			continue
		end if
	next

	ll_numero_righe = dw_esportazione_prodotti_finale.rowcount()

	li_test=0
	for ll_i = 1 to ll_numero_righe
		ls_elimina = dw_esportazione_prodotti_finale.getitemstring(ll_i,"elimina")
		if ls_elimina="S" then
			li_test=1
		end if
	next

loop

dw_esportazione_prodotti_finale.resetupdate()

return 0
end function

public function integer wf_elimina_orig (string fs_errore);string ls_elimina
long ll_numero_righe,ll_i,ll_conteggio
integer li_test

li_test=1
do while li_test = 1

	ll_numero_righe = dw_esportazione_prodotti_orig.rowcount()

	for ll_i = 1 to ll_numero_righe
		ls_elimina = dw_esportazione_prodotti_orig.getitemstring(ll_i,"elimina")
		if ls_elimina="S" then
			dw_esportazione_prodotti_orig.Delete_DW_Row(ll_i, c_ignoreChanges,c_noRefreshChildren)
			ll_i=ll_numero_righe
			continue
		end if
	next

	ll_numero_righe = dw_esportazione_prodotti_orig.rowcount()

	li_test=0
	for ll_i = 1 to ll_numero_righe
		ls_elimina = dw_esportazione_prodotti_orig.getitemstring(ll_i,"elimina")
		if ls_elimina="S" then
			li_test=1
		end if
	next

loop

dw_esportazione_prodotti_orig.resetupdate()

return 0
end function

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noenablepopup)

windowobject lw_oggetti[]

lw_oggetti[1] = dw_esportazione_prodotti_orig
dw_folder.fu_assigntab(1, "Origine Dati", lw_oggetti[])
lw_oggetti[1] = dw_esportazione_prodotti_finale
dw_folder.fu_assigntab(2, "Dati Da Esportare", lw_oggetti[])
lw_oggetti[1] = st_ist_1
lw_oggetti[2] = st_ist_2
lw_oggetti[3] = st_ist_3
lw_oggetti[4] = st_ist_4
dw_folder.fu_assigntab(3, "Istruzioni", lw_oggetti[])
dw_folder.fu_foldercreate(3, 4)
dw_folder.fu_selecttab(1)

dw_esportazione_prodotti_orig.set_dw_options(sqlca, &
	                                          pcca.null_object, &
				   										c_noretrieveonopen + &
                                             c_multiselect + &
													 		c_nonew + &
													 		c_nomodify + &
													 		c_nodelete + &
												  	 		c_disablecc + &
													 		c_disableccinsert, &
                                        		c_viewmodeblack)
													 
dw_esportazione_prodotti_finale.set_dw_options(sqlca, &
                                       		  pcca.null_object, &
															  c_noretrieveonopen + &
                                        		  c_multiselect + &
													 		  c_nonew + &
													 		  c_nomodify + &
													 		  c_nodelete + &
												  	 		  c_disablecc + &
													 		  c_NoIgnoreNewRows + &
													 		  c_disableccinsert, &
                                       		  c_viewmodeblack)


SELECT stringa  
INTO   :st_file_listino.text  
FROM   parametri_azienda  
WHERE  cod_azienda = :s_cs_xx.cod_azienda
AND    cod_parametro = 'FLI';

SELECT stringa  
INTO   :st_file_um.text
FROM   parametri_azienda  
WHERE  cod_azienda = :s_cs_xx.cod_azienda
AND    cod_parametro = 'FUM';

SELECT stringa  
INTO   :st_file_valute.text
FROM   parametri_azienda  
WHERE  cod_azienda = :s_cs_xx.cod_azienda
AND    cod_parametro = 'FVA';

save_on_close(c_socnosave)

end event

on w_esp_listino_fornitori.create
int iCurrent
call w_cs_xx_principale::create
this.cb_1=create cb_1
this.gb_1=create gb_1
this.rb_totale=create rb_totale
this.rb_selezionati=create rb_selezionati
this.cb_esporta=create cb_esporta
this.ddlb_fornitori=create ddlb_fornitori
this.ddlb_cat_mer=create ddlb_cat_mer
this.st_2=create st_2
this.st_3=create st_3
this.st_file_listino=create st_file_listino
this.st_4=create st_4
this.st_5=create st_5
this.st_file_um=create st_file_um
this.st_7=create st_7
this.st_file_valute=create st_file_valute
this.cb_aggiungi=create cb_aggiungi
this.cb_togli=create cb_togli
this.st_ist_1=create st_ist_1
this.st_ist_2=create st_ist_2
this.st_ist_3=create st_ist_3
this.st_ist_4=create st_ist_4
this.dw_esportazione_prodotti_finale=create dw_esportazione_prodotti_finale
this.dw_esportazione_prodotti_orig=create dw_esportazione_prodotti_orig
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=cb_1
this.Control[iCurrent+2]=gb_1
this.Control[iCurrent+3]=rb_totale
this.Control[iCurrent+4]=rb_selezionati
this.Control[iCurrent+5]=cb_esporta
this.Control[iCurrent+6]=ddlb_fornitori
this.Control[iCurrent+7]=ddlb_cat_mer
this.Control[iCurrent+8]=st_2
this.Control[iCurrent+9]=st_3
this.Control[iCurrent+10]=st_file_listino
this.Control[iCurrent+11]=st_4
this.Control[iCurrent+12]=st_5
this.Control[iCurrent+13]=st_file_um
this.Control[iCurrent+14]=st_7
this.Control[iCurrent+15]=st_file_valute
this.Control[iCurrent+16]=cb_aggiungi
this.Control[iCurrent+17]=cb_togli
this.Control[iCurrent+18]=st_ist_1
this.Control[iCurrent+19]=st_ist_2
this.Control[iCurrent+20]=st_ist_3
this.Control[iCurrent+21]=st_ist_4
this.Control[iCurrent+22]=dw_esportazione_prodotti_finale
this.Control[iCurrent+23]=dw_esportazione_prodotti_orig
this.Control[iCurrent+24]=dw_folder
end on

on w_esp_listino_fornitori.destroy
call w_cs_xx_principale::destroy
destroy(this.cb_1)
destroy(this.gb_1)
destroy(this.rb_totale)
destroy(this.rb_selezionati)
destroy(this.cb_esporta)
destroy(this.ddlb_fornitori)
destroy(this.ddlb_cat_mer)
destroy(this.st_2)
destroy(this.st_3)
destroy(this.st_file_listino)
destroy(this.st_4)
destroy(this.st_5)
destroy(this.st_file_um)
destroy(this.st_7)
destroy(this.st_file_valute)
destroy(this.cb_aggiungi)
destroy(this.cb_togli)
destroy(this.st_ist_1)
destroy(this.st_ist_2)
destroy(this.st_ist_3)
destroy(this.st_ist_4)
destroy(this.dw_esportazione_prodotti_finale)
destroy(this.dw_esportazione_prodotti_orig)
destroy(this.dw_folder)
end on

event close;call super::close;commit;
end event

event pc_setddlb;call super::pc_setddlb;f_po_loadddlb(ddlb_fornitori, &
              sqlca, &
              "anag_fornitori", &
              "cod_fornitore", &
              "rag_soc_1", &
              "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))","")

f_po_loadddlb(ddlb_cat_mer, &
              sqlca, &
              "tab_cat_mer", &
              "cod_cat_mer", &
              "des_cat_mer", &
              "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))","")

end event

type cb_1 from uo_cb_close within w_esp_listino_fornitori
int X=1875
int Y=1201
int Width=366
int Height=81
int TabOrder=60
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_1 from groupbox within w_esp_listino_fornitori
int X=23
int Y=661
int Width=641
int Height=221
int TabOrder=90
string Text="Tipo Aggiungi/Togli"
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_totale from radiobutton within w_esp_listino_fornitori
int X=46
int Y=721
int Width=595
int Height=61
boolean BringToTop=true
string Text="Totale"
boolean Checked=true
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_selezionati from radiobutton within w_esp_listino_fornitori
int X=46
int Y=801
int Width=595
int Height=61
boolean BringToTop=true
string Text="Record Selezionati"
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_esporta from uo_cb_ok within w_esp_listino_fornitori
event clicked pbm_bnclicked
int X=1486
int Y=1201
int Width=366
int Height=81
int TabOrder=20
string Text="&Esporta"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;call super::clicked;long ll_riga, ll_errore, ll_selected[], ll_i, ll_conta, ll_totale, ll_riga_sel[], al_riga_sel
long   ll_lungo,ll_valore
string ls_cod_prodotto, ls_des_prodotto, ls_cod_iva, ls_cod_cat_mer, &
       ls_cod_fornitore, ls_controllo, &
       ls_cod_comodo,ls_cod_prodotto_fornitore,ls_cod_um_acquisto,ls_cod_um_interna, &
 		 ls_cod_valuta,ls_nome_file,ls_path,ls_cod_misura,ls_des_misura,ls_des_valuta,ls_errore
double ldd_fattore_conversione 
integer li_filenum,li_risposta

ls_cod_fornitore = f_po_selectddlb(ddlb_fornitori) 

if isnull(ls_cod_fornitore) or ls_cod_fornitore="" then 
	g_mb.messagebox("Apice","Attenzione! Selezionare un fornitore di destinazione.",exclamation!)
	return
end if
	
if fileexists(st_file_listino.text) then
	filedelete(st_file_listino.text)
end if

if fileexists(st_file_um.text) then
	filedelete(st_file_um.text)
end if

if fileexists(st_file_valute.text) then
	filedelete(st_file_valute.text)
end if

SELECT cod_valuta
INTO   :ls_cod_valuta  
FROM   anag_fornitori
WHERE  cod_azienda = :s_cs_xx.cod_azienda 
AND    cod_fornitore = :ls_cod_fornitore;
if isnull(ls_cod_valuta) then ls_cod_valuta=""

li_FileNum = FileOpen(st_file_listino.text,  &
							 LineMode!, Write!, LockWrite!, Append!)

setpointer(hourglass!)

for ll_riga = 1 to dw_esportazione_prodotti_finale.rowcount()
  	ls_cod_prodotto = dw_esportazione_prodotti_finale.getitemstring(ll_riga,"cod_prodotto")
	if isnull(ls_cod_prodotto) then ls_cod_prodotto=""
	ls_cod_comodo = dw_esportazione_prodotti_finale.getitemstring(ll_riga,"cod_comodo")
	if isnull(ls_cod_comodo) then ls_cod_comodo=""
	ls_des_prodotto = dw_esportazione_prodotti_finale.getitemstring(ll_riga,"des_prodotto")
	if isnull(ls_des_prodotto) then ls_des_prodotto=""
	ls_cod_um_acquisto = dw_esportazione_prodotti_finale.getitemstring(ll_riga,"cod_um_acquisto")
	if isnull(ls_cod_um_acquisto) then ls_cod_um_acquisto=""
	ls_cod_um_interna = dw_esportazione_prodotti_finale.getitemstring(ll_riga,"cod_um_interna")
	if isnull(ls_cod_um_interna) then ls_cod_um_interna=""
	ldd_fattore_conversione = dw_esportazione_prodotti_finale.getitemnumber(ll_riga,"fattore_conversione")
	if isnull(ldd_fattore_conversione) then ldd_fattore_conversione=1
	
	select cod_prod_fornitore
	into   :ls_cod_prodotto_fornitore
	from   tab_prod_fornitori
	where  cod_azienda=:s_cs_xx.cod_azienda
	and	 cod_prodotto=:ls_cod_prodotto
	and    cod_fornitore=:ls_cod_fornitore;
			
	if isnull(ls_cod_prodotto_fornitore) then ls_cod_prodotto_fornitore = ""
	
	FileWrite(li_FileNum, ls_cod_prodotto + ";" + ls_des_prodotto + ";" + ls_cod_comodo + ";" + ls_cod_fornitore + ";" + ls_cod_prodotto_fornitore + ";" + ls_cod_um_acquisto + ";" + ls_cod_um_interna + ";" + ls_cod_valuta + ";01/01/1900;" + string(ldd_fattore_conversione)+ ";0;0;0;0;0;0;0;0;0;0;0;0;0;0;0")

   dw_esportazione_prodotti_finale.setitem(ll_riga,"elimina","S")				      

next
fileclose(li_filenum)   	

li_FileNum = FileOpen(st_file_um.text,  &
							 LineMode!, Write!, LockWrite!, Append!)


DECLARE righe_misure CURSOR FOR  
SELECT  cod_misura,   
        des_misura  
FROM    tab_misure   
WHERE   cod_azienda = :s_cs_xx.cod_azienda;

open righe_misure;

do while 1 = 1
  	fetch righe_misure into :ls_cod_misura, :ls_des_misura;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode <> 0) then exit
	FileWrite(li_FileNum, ls_cod_misura + ";" + ls_des_misura )      	
loop

close righe_misure;
fileclose(li_filenum)

li_FileNum = FileOpen(st_file_valute.text,  &
							 LineMode!, Write!, LockWrite!, Append!)

DECLARE righe_valute CURSOR FOR  
SELECT  cod_valuta,   
        des_valuta  
FROM    tab_valute   
WHERE   cod_azienda = :s_cs_xx.cod_azienda;

open righe_valute;

do while 1 = 1
  	fetch righe_valute into :ls_cod_valuta, :ls_des_valuta;
   if (sqlca.sqlcode = 100) or (sqlca.sqlcode <> 0) then exit
	FileWrite(li_FileNum, ls_cod_valuta + ";" + ls_des_valuta )      	
loop

close righe_valute;
fileclose(li_filenum)

li_risposta = wf_elimina(ls_errore)
dw_esportazione_prodotti_finale.Reset_DW_Modified(c_ResetChildren)
dw_esportazione_prodotti_orig.Reset_DW_Modified(c_ResetChildren)

setpointer(arrow!)
end event

type ddlb_fornitori from dropdownlistbox within w_esp_listino_fornitori
int X=1098
int Y=661
int Width=1143
int Height=1361
int TabOrder=70
boolean BringToTop=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type ddlb_cat_mer from dropdownlistbox within w_esp_listino_fornitori
int X=1098
int Y=761
int Width=1143
int Height=1401
int TabOrder=80
boolean BringToTop=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
string Item[]={"^"}
end type

event selectionchanged;string ls_str

dw_esportazione_prodotti_finale.Reset_DW_Modified(c_ResetChildren)
dw_esportazione_prodotti_orig.Reset_DW_Modified(c_ResetChildren)
dw_esportazione_prodotti_orig.setfocus()
parent.triggerevent("pc_retrieve")

end event

type st_2 from statictext within w_esp_listino_fornitori
int X=823
int Y=681
int Width=261
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="Fornitore:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_esp_listino_fornitori
int X=801
int Y=781
int Width=289
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="Cat. Merc.:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_file_listino from statictext within w_esp_listino_fornitori
int X=321
int Y=901
int Width=1921
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_esp_listino_fornitori
int X=23
int Y=901
int Width=298
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="File Listino:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_5 from statictext within w_esp_listino_fornitori
int X=23
int Y=1001
int Width=298
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="File U.M.:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_file_um from statictext within w_esp_listino_fornitori
int X=321
int Y=1001
int Width=1921
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_7 from statictext within w_esp_listino_fornitori
int X=23
int Y=1101
int Width=298
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="File Valute:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_file_valute from statictext within w_esp_listino_fornitori
int X=321
int Y=1101
int Width=1921
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_aggiungi from commandbutton within w_esp_listino_fornitori
int X=1098
int Y=1201
int Width=366
int Height=81
int TabOrder=50
boolean BringToTop=true
string Text="&Aggiungi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_riga, ll_selected[],  ll_conta, ll_totale, ll_riga_sel[],ll_riga_aggiungi
string ls_cod_prodotto, ls_des_prodotto, &
       ls_cod_fornitore, &
       ls_cod_comodo,ls_cod_prodotto_fornitore,ls_cod_um_acquisto,ls_cod_um_interna, &
 		 ls_cod_valuta,ls_errore
double ldd_fattore_conversione 
integer li_risposta

setpointer(hourglass!)

if rb_totale.checked then
  	for ll_riga = 1 to dw_esportazione_prodotti_orig.rowcount()
      ls_cod_prodotto = dw_esportazione_prodotti_orig.getitemstring(ll_riga,"cod_prodotto")
		li_risposta=wf_controllo_prodotto(ls_cod_prodotto)
		if li_risposta=-1 then continue
	   if isnull(ls_cod_prodotto) then ls_cod_prodotto=""
		ls_cod_comodo = dw_esportazione_prodotti_orig.getitemstring(ll_riga,"cod_comodo")
		if isnull(ls_cod_comodo) then ls_cod_comodo=""
		ls_des_prodotto = dw_esportazione_prodotti_orig.getitemstring(ll_riga,"des_prodotto")
		if isnull(ls_des_prodotto) then ls_des_prodotto=""
		ls_cod_um_acquisto = dw_esportazione_prodotti_orig.getitemstring(ll_riga,"cod_misura_acq")
		if isnull(ls_cod_um_acquisto) then ls_cod_um_acquisto=""
		ls_cod_um_interna = dw_esportazione_prodotti_orig.getitemstring(ll_riga,"cod_misura_mag")
		if isnull(ls_cod_um_interna) then ls_cod_um_interna=""
		ldd_fattore_conversione = dw_esportazione_prodotti_orig.getitemnumber(ll_riga,"fat_conversione_acq")
		if isnull(ldd_fattore_conversione) then ldd_fattore_conversione=1
		ll_riga_aggiungi=dw_esportazione_prodotti_finale.rowcount()
		ll_riga_aggiungi++
		dw_esportazione_prodotti_finale.insertrow(ll_riga_aggiungi)
		dw_esportazione_prodotti_orig.setitem(ll_riga,"elimina","S")
		dw_esportazione_prodotti_finale.setitem(ll_riga_aggiungi,"cod_prodotto",ls_cod_prodotto)
		dw_esportazione_prodotti_finale.setitem(ll_riga_aggiungi,"des_prodotto",ls_des_prodotto)
		dw_esportazione_prodotti_finale.setitem(ll_riga_aggiungi,"cod_comodo",ls_cod_comodo)
		dw_esportazione_prodotti_finale.setitem(ll_riga_aggiungi,"cod_um_acquisto",ls_cod_um_acquisto)
		dw_esportazione_prodotti_finale.setitem(ll_riga_aggiungi,"cod_um_interna",ls_cod_um_interna)
		dw_esportazione_prodotti_finale.setitem(ll_riga_aggiungi,"fattore_conversione",ldd_fattore_conversione)
	next
else
  	dw_esportazione_prodotti_orig.get_selected_rows(ll_selected[])
	for ll_riga = 1 to upperbound(ll_selected[])
     	ls_cod_prodotto =dw_esportazione_prodotti_orig.getitemstring(ll_selected[ll_riga],"cod_prodotto")
		li_risposta=wf_controllo_prodotto(ls_cod_prodotto)
		if li_risposta=-1 then continue
		if isnull(ls_cod_prodotto) then ls_cod_prodotto=""
		ls_cod_comodo = dw_esportazione_prodotti_orig.getitemstring(ll_selected[ll_riga],"cod_comodo")
		if isnull(ls_cod_comodo) then ls_cod_comodo=""
		ls_des_prodotto = dw_esportazione_prodotti_orig.getitemstring(ll_selected[ll_riga],"des_prodotto")
		if isnull(ls_des_prodotto) then ls_des_prodotto=""
		ls_cod_um_acquisto = dw_esportazione_prodotti_orig.getitemstring(ll_selected[ll_riga],"cod_misura_acq")
		if isnull(ls_cod_um_acquisto) then ls_cod_um_acquisto=""
		ls_cod_um_interna = dw_esportazione_prodotti_orig.getitemstring(ll_selected[ll_riga],"cod_misura_mag")
		if isnull(ls_cod_um_interna) then ls_cod_um_interna=""
		ldd_fattore_conversione = dw_esportazione_prodotti_orig.getitemnumber(ll_selected[ll_riga],"fat_conversione_acq")
		if isnull(ldd_fattore_conversione) then ldd_fattore_conversione=1
		ll_riga_aggiungi=dw_esportazione_prodotti_finale.rowcount()
		ll_riga_aggiungi++
		dw_esportazione_prodotti_finale.insertrow(ll_riga_aggiungi)
		dw_esportazione_prodotti_orig.setitem(ll_selected[ll_riga],"elimina","S")
		dw_esportazione_prodotti_finale.setitem(ll_riga_aggiungi,"cod_prodotto",ls_cod_prodotto)
		dw_esportazione_prodotti_finale.setitem(ll_riga_aggiungi,"des_prodotto",ls_des_prodotto)
		dw_esportazione_prodotti_finale.setitem(ll_riga_aggiungi,"cod_comodo",ls_cod_comodo)
		dw_esportazione_prodotti_finale.setitem(ll_riga_aggiungi,"cod_um_acquisto",ls_cod_um_acquisto)
		dw_esportazione_prodotti_finale.setitem(ll_riga_aggiungi,"cod_um_interna",ls_cod_um_interna)
		dw_esportazione_prodotti_finale.setitem(ll_riga_aggiungi,"fattore_conversione",ldd_fattore_conversione)
  	next
end if

dw_esportazione_prodotti_finale.Reset_DW_Modified(c_ResetChildren)
dw_esportazione_prodotti_orig.Reset_DW_Modified(c_ResetChildren)
dw_esportazione_prodotti_finale.TriggerEvent("pcd_search")

li_risposta=wf_elimina_orig(ls_errore)

setpointer(arrow!)
end event

type cb_togli from commandbutton within w_esp_listino_fornitori
event clicked pbm_bnclicked
int X=709
int Y=1201
int Width=366
int Height=81
int TabOrder=21
boolean BringToTop=true
string Text="&Togli"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_riga, ll_selected[],ll_i
integer li_risposta
string ls_errore

setpointer(hourglass!)

if rb_totale.checked then
	ll_i=dw_esportazione_prodotti_finale.rowcount()
  	for ll_riga = 1 to ll_i
      dw_esportazione_prodotti_finale.setitem(ll_riga,"elimina","S")
	next
else
	dw_esportazione_prodotti_finale.Get_Retrieve_Rows(ll_selected[])
	for ll_riga = 1 to upperbound(ll_selected[])
   	dw_esportazione_prodotti_finale.setitem(ll_selected[ll_riga],"elimina","S")	
  	next
end if

li_risposta = wf_elimina(ls_errore)
dw_esportazione_prodotti_finale.Reset_DW_Modified(c_ResetChildren)
dw_esportazione_prodotti_orig.Reset_DW_Modified(c_ResetChildren)
setpointer(arrow!)
end event

type st_ist_1 from statictext within w_esp_listino_fornitori
int X=298
int Y=181
int Width=1125
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="1) Selezionare una Categoria Merceologica"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_ist_2 from statictext within w_esp_listino_fornitori
int X=298
int Y=261
int Width=1692
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="2) Aggiungere i dati selezionati o complessivi ai dati da esportare"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_ist_3 from statictext within w_esp_listino_fornitori
int X=298
int Y=341
int Width=1692
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="3) Selezionare un Fornitore di destinazione"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_ist_4 from statictext within w_esp_listino_fornitori
int X=298
int Y=421
int Width=1692
int Height=77
boolean Enabled=false
string Text="4) Esportare i dati nei file sottoindicati tramite il bottone Esporta"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_esportazione_prodotti_finale from uo_cs_xx_dw within w_esp_listino_fornitori
int X=46
int Y=121
int Width=2103
int Height=501
int TabOrder=10
string DataObject="d_esportazione_prodotti_finale"
boolean HScrollBar=true
boolean VScrollBar=true
end type

type dw_esportazione_prodotti_orig from uo_cs_xx_dw within w_esp_listino_fornitori
event pcd_retrieve pbm_custom60
int X=46
int Y=121
int Width=2103
int Height=501
int TabOrder=40
string DataObject="d_esportazione_prodotti_orig"
boolean HScrollBar=true
boolean VScrollBar=true
end type

event pcd_retrieve;call super::pcd_retrieve;long ll_errore
string ls_cod_cat_mer

ls_cod_cat_mer = f_po_selectddlb(ddlb_cat_mer)
if ls_cod_cat_mer="^" then ls_cod_cat_mer="%"

ll_errore = retrieve(s_cs_xx.cod_azienda,ls_cod_cat_mer)

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

type dw_folder from u_folder within w_esp_listino_fornitori
int X=1
int Y=21
int Width=2241
int Height=621
int TabOrder=30
end type

