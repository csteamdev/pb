﻿$PBExportHeader$w_imp_listino_fornitori.srw
$PBExportComments$Window Importa Listino Fornitori
forward
global type w_imp_listino_fornitori from w_cs_xx_principale
end type
type gb_3 from groupbox within w_imp_listino_fornitori
end type
type gb_1 from groupbox within w_imp_listino_fornitori
end type
type gb_2 from groupbox within w_imp_listino_fornitori
end type
type cb_1 from uo_cb_close within w_imp_listino_fornitori
end type
type st_2 from statictext within w_imp_listino_fornitori
end type
type st_file_listino from statictext within w_imp_listino_fornitori
end type
type st_4 from statictext within w_imp_listino_fornitori
end type
type st_1 from statictext within w_imp_listino_fornitori
end type
type st_3 from statictext within w_imp_listino_fornitori
end type
type cb_2 from commandbutton within w_imp_listino_fornitori
end type
type st_7 from statictext within w_imp_listino_fornitori
end type
type st_8 from statictext within w_imp_listino_fornitori
end type
type rb_1 from radiobutton within w_imp_listino_fornitori
end type
type rb_2 from radiobutton within w_imp_listino_fornitori
end type
type st_ist_3 from statictext within w_imp_listino_fornitori
end type
type st_ist_5 from statictext within w_imp_listino_fornitori
end type
type st_10 from statictext within w_imp_listino_fornitori
end type
type st_ist_7 from statictext within w_imp_listino_fornitori
end type
type st_ist_4 from statictext within w_imp_listino_fornitori
end type
type st_ist_2 from statictext within w_imp_listino_fornitori
end type
type cbx_valuta from checkbox within w_imp_listino_fornitori
end type
type cbx_fornitore from checkbox within w_imp_listino_fornitori
end type
type cbx_data_validita from checkbox within w_imp_listino_fornitori
end type
type em_data from editmask within w_imp_listino_fornitori
end type
type cbx_um_acquisto from checkbox within w_imp_listino_fornitori
end type
type cbx_fattore_conversione from checkbox within w_imp_listino_fornitori
end type
type em_fattore_conversione from editmask within w_imp_listino_fornitori
end type
type ddlb_um_acquisto from dropdownlistbox within w_imp_listino_fornitori
end type
type ddlb_valute from dropdownlistbox within w_imp_listino_fornitori
end type
type ddlb_fornitori from dropdownlistbox within w_imp_listino_fornitori
end type
type rb_selezionati from radiobutton within w_imp_listino_fornitori
end type
type rb_totale from radiobutton within w_imp_listino_fornitori
end type
type st_6 from statictext within w_imp_listino_fornitori
end type
type cb_leggi from uo_cb_ok within w_imp_listino_fornitori
end type
type cb_importa from uo_cb_ok within w_imp_listino_fornitori
end type
type cb_scrivi from uo_cb_ok within w_imp_listino_fornitori
end type
type cb_aggiungi from commandbutton within w_imp_listino_fornitori
end type
type cb_togli from commandbutton within w_imp_listino_fornitori
end type
type dw_importa_listino_orig from uo_cs_xx_dw within w_imp_listino_fornitori
end type
type dw_errore_scrittura from uo_cs_xx_dw within w_imp_listino_fornitori
end type
type st_ist_6 from statictext within w_imp_listino_fornitori
end type
type st_ist_1 from statictext within w_imp_listino_fornitori
end type
type dw_importa_listino_ok from uo_cs_xx_dw within w_imp_listino_fornitori
end type
type dw_importa_listino_errore from uo_cs_xx_dw within w_imp_listino_fornitori
end type
type dw_folder from u_folder within w_imp_listino_fornitori
end type
end forward

global type w_imp_listino_fornitori from w_cs_xx_principale
int Width=2958
int Height=1705
boolean TitleBar=true
string Title="Importazione Listino Prodotti"
gb_3 gb_3
gb_1 gb_1
gb_2 gb_2
cb_1 cb_1
st_2 st_2
st_file_listino st_file_listino
st_4 st_4
st_1 st_1
st_3 st_3
cb_2 cb_2
st_7 st_7
st_8 st_8
rb_1 rb_1
rb_2 rb_2
st_ist_3 st_ist_3
st_ist_5 st_ist_5
st_10 st_10
st_ist_7 st_ist_7
st_ist_4 st_ist_4
st_ist_2 st_ist_2
cbx_valuta cbx_valuta
cbx_fornitore cbx_fornitore
cbx_data_validita cbx_data_validita
em_data em_data
cbx_um_acquisto cbx_um_acquisto
cbx_fattore_conversione cbx_fattore_conversione
em_fattore_conversione em_fattore_conversione
ddlb_um_acquisto ddlb_um_acquisto
ddlb_valute ddlb_valute
ddlb_fornitori ddlb_fornitori
rb_selezionati rb_selezionati
rb_totale rb_totale
st_6 st_6
cb_leggi cb_leggi
cb_importa cb_importa
cb_scrivi cb_scrivi
cb_aggiungi cb_aggiungi
cb_togli cb_togli
dw_importa_listino_orig dw_importa_listino_orig
dw_errore_scrittura dw_errore_scrittura
st_ist_6 st_ist_6
st_ist_1 st_ist_1
dw_importa_listino_ok dw_importa_listino_ok
dw_importa_listino_errore dw_importa_listino_errore
dw_folder dw_folder
end type
global w_imp_listino_fornitori w_imp_listino_fornitori

type variables
transaction i_sqlorig
end variables

forward prototypes
public function integer wf_aggiorna_listino (long fl_numero_riga, ref string fs_errore)
public function integer wf_aggiungi_listino (long fl_numero_riga, string fs_errore)
public function integer wf_togli_listino (string fs_errore)
public function integer wf_elimina_dati_ok (string fs_errore)
public function integer wf_elimina_orig (string fs_errore)
end prototypes

public function integer wf_aggiorna_listino (long fl_numero_riga, ref string fs_errore);//	Window Function di verifica dati listino fornitori
//
// nome: wf_aggiorna_listino
// tipo: integer
//	ritorno: 0 execution passed
//  		  -1 execution failed
//
//	Variabili passate: 			nome					 tipo				passaggio per
//			
//									fl_numero_riga		    long 			valore
//									fs_errore				 string			riferimento
//									
//
//		Creata il 05-03-1997 
//		Autore Diego Ferrari
//


long   ll_contatore_righe_ok,ll_contatore_righe_errore
string ls_des_prodotto,ls_cod_prodotto,ls_cod_fornitore, &
       ls_cod_comodo,ls_cod_prodotto_fornitore,ls_cod_um_acquisto,ls_cod_um_interna, &
 		 ls_cod_valuta,ls_cod_misura,ls_riga
datetime ldt_data_validita
double ldd_fattore_conversione, &
		 ldd_quantita_1,ldd_prezzo_1,ldd_sconto_1, &
 		 ldd_quantita_2,ldd_prezzo_2,ldd_sconto_2, &
  		 ldd_quantita_3,ldd_prezzo_3,ldd_sconto_3, &
		 ldd_quantita_4,ldd_prezzo_4,ldd_sconto_4, &
		 ldd_quantita_5,ldd_prezzo_5,ldd_sconto_5 

dw_importa_listino_orig.setitem(fl_numero_riga,"elimina","S")
ls_cod_prodotto = dw_importa_listino_orig.getitemstring(fl_numero_riga,"cod_prodotto")			      
ls_des_prodotto = dw_importa_listino_orig.getitemstring(fl_numero_riga,"des_prodotto")			      
ls_cod_fornitore = dw_importa_listino_orig.getitemstring(fl_numero_riga,"cod_fornitore")			      
ls_cod_prodotto_fornitore = dw_importa_listino_orig.getitemstring(fl_numero_riga,"cod_prodotto_fornitore")			      
ls_cod_um_acquisto = dw_importa_listino_orig.getitemstring(fl_numero_riga,"cod_um_acquisto")			      
ls_cod_valuta = dw_importa_listino_orig.getitemstring(fl_numero_riga,"cod_valuta")			      
ldt_data_validita = dw_importa_listino_orig.getitemdatetime(fl_numero_riga,"data_validita")
ldd_fattore_conversione = dw_importa_listino_orig.getitemnumber(fl_numero_riga,"fattore_conversione")
ldd_quantita_1 = dw_importa_listino_orig.getitemnumber(fl_numero_riga,"quantita_1")
ldd_prezzo_1 = dw_importa_listino_orig.getitemnumber(fl_numero_riga,"prezzo_1")
ldd_sconto_1 = dw_importa_listino_orig.getitemnumber(fl_numero_riga,"sconto_1")   
ldd_quantita_2 = dw_importa_listino_orig.getitemnumber(fl_numero_riga,"quantita_2")
ldd_prezzo_2 = dw_importa_listino_orig.getitemnumber(fl_numero_riga,"prezzo_2")   
ldd_sconto_2 = dw_importa_listino_orig.getitemnumber(fl_numero_riga,"sconto_2")   
ldd_quantita_3 = dw_importa_listino_orig.getitemnumber(fl_numero_riga,"quantita_3")  
ldd_prezzo_3 = dw_importa_listino_orig.getitemnumber(fl_numero_riga,"prezzo_3")   
ldd_sconto_3 = dw_importa_listino_orig.getitemnumber(fl_numero_riga,"sconto_3")   
ldd_quantita_4 = dw_importa_listino_orig.getitemnumber(fl_numero_riga,"quantita_4")   
ldd_prezzo_4 = dw_importa_listino_orig.getitemnumber(fl_numero_riga,"prezzo_4")   
ldd_sconto_4 = dw_importa_listino_orig.getitemnumber(fl_numero_riga,"sconto_4")   
ldd_quantita_5 = dw_importa_listino_orig.getitemnumber(fl_numero_riga,"quantita_5")   
ldd_prezzo_5 = dw_importa_listino_orig.getitemnumber(fl_numero_riga,"prezzo_5")   
ldd_sconto_5 =dw_importa_listino_orig.getitemnumber(fl_numero_riga,"sconto_5")   

if not isnull(ls_cod_prodotto) or ls_cod_prodotto="" then
	SELECT cod_misura_mag
   INTO   :ls_cod_um_interna  
   FROM   anag_prodotti
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND    cod_prodotto = :ls_cod_prodotto;
end if

if isnull(ls_cod_prodotto) or ls_cod_prodotto="" or isnull(ls_cod_fornitore) & 
	or ls_cod_fornitore="" or isnull(ls_cod_um_acquisto) or ls_cod_um_acquisto="" or & 
	isnull(ls_cod_valuta) or ls_cod_valuta="" or isnull(ldd_fattore_conversione) or & 
	ldd_fattore_conversione=0 or ls_cod_prodotto_fornitore="" or & 
	isnull(ls_cod_prodotto_fornitore) or isnull(ldt_data_validita) or &
	(ldd_prezzo_1=0 and ldd_prezzo_2=0 and ldd_prezzo_3=0 and ldd_prezzo_4=0 and ldd_prezzo_5=0) then
	ll_contatore_righe_errore=dw_importa_listino_errore.rowcount()
	ll_contatore_righe_errore++
	dw_importa_listino_errore.insertrow(ll_contatore_righe_errore)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"cod_prodotto",ls_cod_prodotto)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"des_prodotto",ls_des_prodotto)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"cod_prodotto_fornitore",ls_cod_prodotto_fornitore)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"cod_fornitore",ls_cod_fornitore)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"cod_um_acquisto",ls_cod_um_acquisto)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"cod_um_interna",ls_cod_um_interna)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"cod_valuta",ls_cod_valuta)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"data_validita",ldt_data_validita)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"fattore_conversione",ldd_fattore_conversione)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"quantita_1",ldd_quantita_1)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"quantita_2",ldd_quantita_2)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"quantita_3",ldd_quantita_3)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"quantita_4",ldd_quantita_4)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"quantita_5",ldd_quantita_5)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"prezzo_1",ldd_prezzo_1)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"prezzo_2",ldd_prezzo_2)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"prezzo_3",ldd_prezzo_3)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"prezzo_4",ldd_prezzo_4)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"prezzo_5",ldd_prezzo_5)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"sconto_1",ldd_sconto_1)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"sconto_2",ldd_sconto_2)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"sconto_3",ldd_sconto_3)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"sconto_4",ldd_sconto_4)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"sconto_5",ldd_sconto_5)
	dw_importa_listino_errore.setitem(ll_contatore_righe_errore,"elimina","N")
else
	ll_contatore_righe_ok=dw_importa_listino_ok.rowcount()
	ll_contatore_righe_ok++
	dw_importa_listino_ok.insertrow(ll_contatore_righe_ok)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"cod_prodotto",ls_cod_prodotto)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"des_prodotto",ls_des_prodotto)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"cod_fornitore",ls_cod_fornitore)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"cod_prodotto_fornitore",ls_cod_prodotto_fornitore)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"cod_um_acquisto",ls_cod_um_acquisto)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"cod_um_interna",ls_cod_um_interna)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"cod_valuta",ls_cod_valuta)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"data_validita",ldt_data_validita)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"fattore_conversione",ldd_fattore_conversione)
	dw_importa_listino_ok.setitem(ll_contatore_righe_errore,"elimina","N")
	if ldd_quantita_1=99999999.9999 then
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_1",ldd_quantita_1)
	else
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_1",round(ldd_quantita_1/ldd_fattore_conversione,4))		
	end if
	if ldd_quantita_2=99999999.9999 then
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_2",ldd_quantita_2)
	else
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_2",round(ldd_quantita_2/ldd_fattore_conversione,4))		
	end if
	if ldd_quantita_3=99999999.9999 then
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_3",ldd_quantita_3)
	else
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_3",round(ldd_quantita_3/ldd_fattore_conversione,4))		
	end if
	if ldd_quantita_4=99999999.9999 then
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_4",ldd_quantita_4)
	else
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_4",round(ldd_quantita_4/ldd_fattore_conversione,4))		
	end if
	if ldd_quantita_5=99999999.9999 then
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_5",ldd_quantita_5)
	else
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_5",round(ldd_quantita_5/ldd_fattore_conversione,4))		
	end if

	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"prezzo_1",ldd_prezzo_1*ldd_fattore_conversione)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"prezzo_2",ldd_prezzo_2*ldd_fattore_conversione)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"prezzo_3",ldd_prezzo_3*ldd_fattore_conversione)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"prezzo_4",ldd_prezzo_4*ldd_fattore_conversione)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"prezzo_5",ldd_prezzo_5*ldd_fattore_conversione)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"sconto_1",ldd_sconto_1)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"sconto_2",ldd_sconto_2)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"sconto_3",ldd_sconto_3)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"sconto_4",ldd_sconto_4)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"sconto_5",ldd_sconto_5)
end if	

dw_importa_listino_errore.settaborder("cod_prodotto",10)
dw_importa_listino_errore.settaborder("cod_comodo",20)
dw_importa_listino_errore.settaborder("cod_prodotto_fornitore",30)

return 0
end function

public function integer wf_aggiungi_listino (long fl_numero_riga, string fs_errore);//	Window Function che aggiunge dati al listino fornitori
//
// nome: wf_aggiungi_listino
// tipo: integer
//	ritorno: 0 execution passed
//  		  -1 execution failed
//
//	Variabili passate: 			nome					 tipo				passaggio per
//			
//									fl_numero_riga		    long 			valore
//									fs_errore				 string			riferimento
//									
//
//		Creata il 07-03-1997 
//		Autore Diego Ferrari
//


long   ll_contatore_righe_ok,ll_contatore_righe_errore
string ls_des_prodotto,ls_cod_prodotto,ls_cod_fornitore, &
       ls_cod_comodo,ls_cod_prodotto_fornitore,ls_cod_um_acquisto,ls_cod_um_interna, &
 		 ls_cod_valuta,ls_cod_misura,ls_riga
datetime ldt_data_validita
double ldd_fattore_conversione, &
		 ldd_quantita_1,ldd_prezzo_1,ldd_sconto_1, &
 		 ldd_quantita_2,ldd_prezzo_2,ldd_sconto_2, &
  		 ldd_quantita_3,ldd_prezzo_3,ldd_sconto_3, &
		 ldd_quantita_4,ldd_prezzo_4,ldd_sconto_4, &
		 ldd_quantita_5,ldd_prezzo_5,ldd_sconto_5 

ls_cod_prodotto = dw_importa_listino_errore.getitemstring(fl_numero_riga,"cod_prodotto")			      
ls_des_prodotto = dw_importa_listino_errore.getitemstring(fl_numero_riga,"des_prodotto")			      
ls_cod_fornitore = dw_importa_listino_errore.getitemstring(fl_numero_riga,"cod_fornitore")			      
ls_cod_prodotto_fornitore = dw_importa_listino_errore.getitemstring(fl_numero_riga,"cod_prodotto_fornitore")			      
ls_cod_comodo = dw_importa_listino_errore.getitemstring(fl_numero_riga,"cod_comodo")			      
ls_cod_um_acquisto = dw_importa_listino_errore.getitemstring(fl_numero_riga,"cod_um_acquisto")			      
ls_cod_um_interna = dw_importa_listino_errore.getitemstring(fl_numero_riga,"cod_um_interna")			      
ls_cod_valuta = dw_importa_listino_errore.getitemstring(fl_numero_riga,"cod_valuta")			      
ldt_data_validita = dw_importa_listino_errore.getitemdatetime(fl_numero_riga,"data_validita")
ldd_fattore_conversione = dw_importa_listino_errore.getitemnumber(fl_numero_riga,"fattore_conversione")
ldd_quantita_1 = dw_importa_listino_errore.getitemnumber(fl_numero_riga,"quantita_1")
ldd_prezzo_1 = dw_importa_listino_errore.getitemnumber(fl_numero_riga,"prezzo_1")
ldd_sconto_1 = dw_importa_listino_errore.getitemnumber(fl_numero_riga,"sconto_1")   
ldd_quantita_2 = dw_importa_listino_errore.getitemnumber(fl_numero_riga,"quantita_2")
ldd_prezzo_2 = dw_importa_listino_errore.getitemnumber(fl_numero_riga,"prezzo_2")   
ldd_sconto_2 = dw_importa_listino_errore.getitemnumber(fl_numero_riga,"sconto_2")   
ldd_quantita_3 = dw_importa_listino_errore.getitemnumber(fl_numero_riga,"quantita_3")  
ldd_prezzo_3 = dw_importa_listino_errore.getitemnumber(fl_numero_riga,"prezzo_3")   
ldd_sconto_3 = dw_importa_listino_errore.getitemnumber(fl_numero_riga,"sconto_3")   
ldd_quantita_4 = dw_importa_listino_errore.getitemnumber(fl_numero_riga,"quantita_4")   
ldd_prezzo_4 = dw_importa_listino_errore.getitemnumber(fl_numero_riga,"prezzo_4")   
ldd_sconto_4 = dw_importa_listino_errore.getitemnumber(fl_numero_riga,"sconto_4")   
ldd_quantita_5 = dw_importa_listino_errore.getitemnumber(fl_numero_riga,"quantita_5")   
ldd_prezzo_5 = dw_importa_listino_errore.getitemnumber(fl_numero_riga,"prezzo_5")   
ldd_sconto_5 =dw_importa_listino_errore.getitemnumber(fl_numero_riga,"sconto_5")   

if not (isnull(ls_cod_prodotto) or ls_cod_prodotto="" or isnull(ls_cod_fornitore) or & 
   ls_cod_fornitore="" or isnull(ls_cod_um_acquisto) or ls_cod_um_acquisto="" or & 
	isnull(ls_cod_valuta) or ls_cod_valuta="" or isnull(ldd_fattore_conversione) or & 
	ldd_fattore_conversione=0 or ls_cod_prodotto_fornitore="" or & 
	isnull(ls_cod_prodotto_fornitore) or isnull(ldt_data_validita) or &
	(ldd_prezzo_1=0 and ldd_prezzo_2=0 and ldd_prezzo_3=0 and ldd_prezzo_4=0 and & 
	ldd_prezzo_5=0)) then
	ll_contatore_righe_ok=dw_importa_listino_ok.rowcount()
	ll_contatore_righe_ok++
	dw_importa_listino_ok.insertrow(ll_contatore_righe_ok)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"cod_prodotto",ls_cod_prodotto)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"des_prodotto",ls_des_prodotto)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"cod_fornitore",ls_cod_fornitore)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"cod_prodotto_fornitore",ls_cod_prodotto_fornitore)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"cod_comodo",ls_cod_comodo)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"cod_um_acquisto",ls_cod_um_acquisto)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"cod_um_interna",ls_cod_um_interna)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"cod_valuta",ls_cod_valuta)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"data_validita",ldt_data_validita)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"fattore_conversione",ldd_fattore_conversione)
	dw_importa_listino_ok.setitem(ll_contatore_righe_errore,"elimina","N")
	if ldd_quantita_1=99999999.9999 then
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_1",ldd_quantita_1)
	else
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_1",round(ldd_quantita_1/ldd_fattore_conversione,4))		
	end if
	if ldd_quantita_2=99999999.9999 then
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_2",ldd_quantita_2)
	else
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_2",round(ldd_quantita_2/ldd_fattore_conversione,4))		
	end if
	if ldd_quantita_3=99999999.9999 then
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_3",ldd_quantita_3)
	else
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_3",round(ldd_quantita_3/ldd_fattore_conversione,4))		
	end if
	if ldd_quantita_4=99999999.9999 then
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_4",ldd_quantita_4)
	else
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_4",round(ldd_quantita_4/ldd_fattore_conversione,4))		
	end if
	if ldd_quantita_5=99999999.9999 then
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_5",ldd_quantita_5)
	else
		dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"quantita_5",round(ldd_quantita_5/ldd_fattore_conversione,4))		
	end if
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"prezzo_1",ldd_prezzo_1*ldd_fattore_conversione)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"prezzo_2",ldd_prezzo_2*ldd_fattore_conversione)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"prezzo_3",ldd_prezzo_3*ldd_fattore_conversione)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"prezzo_4",ldd_prezzo_4*ldd_fattore_conversione)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"prezzo_5",ldd_prezzo_5*ldd_fattore_conversione)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"sconto_1",ldd_sconto_1)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"sconto_2",ldd_sconto_2)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"sconto_3",ldd_sconto_3)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"sconto_4",ldd_sconto_4)
	dw_importa_listino_ok.setitem(ll_contatore_righe_ok,"sconto_5",ldd_sconto_5)
	dw_importa_listino_errore.setitem(fl_numero_riga,"elimina","S")
end if	

return 0
end function

public function integer wf_togli_listino (string fs_errore);//	Window Function che toglie i dati al listino con errore
//
// nome: wf_togli_listino
// tipo: integer
//	ritorno: 0 execution passed
//  		  -1 execution failed
//
//	Variabili passate: 			nome					 tipo				passaggio per
//			
//									fl_numero_riga		    long 			valore
//									fs_errore				 string			riferimento
//									
//
//		Creata il 10-03-1997 
//		Autore Diego Ferrari
//

string ls_elimina
long ll_numero_righe,ll_i,ll_conteggio
integer li_test

li_test=1
do while li_test = 1

	ll_numero_righe = dw_importa_listino_errore.rowcount()

	for ll_i = 1 to ll_numero_righe
		ls_elimina = dw_importa_listino_errore.getitemstring(ll_i,"elimina")
		if ls_elimina="S" then
			dw_importa_listino_errore.Delete_DW_Row(ll_i, c_ignoreChanges,c_noRefreshChildren)
			ll_i=ll_numero_righe
			continue
		end if
	next

	ll_numero_righe = dw_importa_listino_errore.rowcount()

	li_test=0
	for ll_i = 1 to ll_numero_righe
		ls_elimina = dw_importa_listino_errore.getitemstring(ll_i,"elimina")
		if ls_elimina="S" then
			li_test=1
		end if
	next

loop

return 0
end function

public function integer wf_elimina_dati_ok (string fs_errore);string ls_elimina
long ll_numero_righe,ll_i,ll_conteggio
integer li_test

li_test=1
do while li_test = 1

	ll_numero_righe = dw_importa_listino_ok.rowcount()

	for ll_i = 1 to ll_numero_righe
		ls_elimina = dw_importa_listino_ok.getitemstring(ll_i,"elimina")
		if ls_elimina="S" then
			dw_importa_listino_ok.Delete_DW_Row(ll_i, c_ignoreChanges,c_noRefreshChildren)
			ll_i=ll_numero_righe
			continue
		end if
	next

	ll_numero_righe = dw_importa_listino_ok.rowcount()

	li_test=0
	for ll_i = 1 to ll_numero_righe
		ls_elimina = dw_importa_listino_ok.getitemstring(ll_i,"elimina")
		if ls_elimina="S" then
			li_test=1
		end if
	next

loop

return 0
end function

public function integer wf_elimina_orig (string fs_errore);string ls_elimina
long ll_numero_righe,ll_i,ll_conteggio
integer li_test

li_test=1
do while li_test = 1

	ll_numero_righe = dw_importa_listino_orig.rowcount()

	for ll_i = 1 to ll_numero_righe
		ls_elimina = dw_importa_listino_orig.getitemstring(ll_i,"elimina")
		if ls_elimina="S" then
			dw_importa_listino_orig.Delete_DW_Row(ll_i, c_ignoreChanges,c_noRefreshChildren)
			ll_i=ll_numero_righe
			continue
		end if
	next

	ll_numero_righe = dw_importa_listino_orig.rowcount()

	li_test=0
	for ll_i = 1 to ll_numero_righe
		ls_elimina = dw_importa_listino_orig.getitemstring(ll_i,"elimina")
		if ls_elimina="S" then
			li_test=1
		end if
	next

loop

return 0
end function

event pc_setwindow;call super::pc_setwindow;set_w_options(c_noenablepopup)
windowobject lw_oggetti_1[]
windowobject lw_oggetti_2[]
windowobject lw_oggetti_3[]
windowobject lw_oggetti_4[]
windowobject lw_oggetti_5[]

lw_oggetti_1[1] = dw_importa_listino_orig
lw_oggetti_1[2] = st_file_listino
lw_oggetti_1[3] = st_4
lw_oggetti_1[4] = cb_leggi
lw_oggetti_1[5] = cb_importa
lw_oggetti_1[6] = gb_1
lw_oggetti_1[7] = rb_totale
lw_oggetti_1[8] = rb_selezionati

dw_folder.fu_assigntab(1, "File Ascii", lw_oggetti_1[])

lw_oggetti_2[1] = dw_importa_listino_ok
lw_oggetti_2[2] = cb_scrivi
dw_folder.fu_assigntab(2, "Dati Ok", lw_oggetti_2[])

lw_oggetti_3[1] = dw_importa_listino_errore
lw_oggetti_3[2] = cbx_data_validita
lw_oggetti_3[3] = cbx_data_validita
lw_oggetti_3[4] = cbx_fattore_conversione
lw_oggetti_3[5] = cbx_fornitore
lw_oggetti_3[6] = cbx_um_acquisto
lw_oggetti_3[7] = cbx_valuta
lw_oggetti_3[8] = ddlb_fornitori
lw_oggetti_3[9] =ddlb_um_acquisto
lw_oggetti_3[10] =ddlb_valute
lw_oggetti_3[11] =em_data
lw_oggetti_3[12] =em_fattore_conversione
lw_oggetti_3[13] =gb_2
lw_oggetti_3[14] =gb_3
lw_oggetti_3[15] =rb_1
lw_oggetti_3[16] =rb_2
lw_oggetti_3[17] =st_8
lw_oggetti_3[18] =st_7
lw_oggetti_3[19] =st_2
lw_oggetti_3[20] =st_6
lw_oggetti_3[21] =st_1
lw_oggetti_3[22] =st_10
lw_oggetti_3[23] =st_3
lw_oggetti_3[24] =cb_2
lw_oggetti_3[25] =cb_aggiungi
lw_oggetti_3[26] =cbx_data_validita
lw_oggetti_3[27] =cbx_fattore_conversione
lw_oggetti_3[28] =cbx_fornitore
lw_oggetti_3[29] =cbx_um_acquisto
lw_oggetti_3[30] =cbx_valuta
lw_oggetti_3[31] =ddlb_fornitori
lw_oggetti_3[32] =ddlb_um_acquisto
lw_oggetti_3[33] =ddlb_valute
lw_oggetti_3[34] =em_data
lw_oggetti_3[35] =em_fattore_conversione

dw_folder.fu_assigntab(3, "Dati Errore", lw_oggetti_3[])

lw_oggetti_4[1] = dw_errore_scrittura
dw_folder.fu_assigntab(4, "Errori S.", lw_oggetti_4[])

lw_oggetti_5[1] = st_ist_1
lw_oggetti_5[2] = st_ist_2
lw_oggetti_5[3] = st_ist_3
lw_oggetti_5[4] = st_ist_4
lw_oggetti_5[5] = st_ist_5
lw_oggetti_5[6] = st_ist_6
lw_oggetti_5[7] = st_ist_7
dw_folder.fu_assigntab(5, "Istruzioni", lw_oggetti_5[])

dw_folder.fu_foldercreate(5, 5)
dw_folder.fu_selecttab(1)

dw_importa_listino_orig.set_dw_options(sqlca, &
                                       pcca.null_object, &
                                       c_multiselect + &
												   c_nonew + &
													c_nomodify + &
													c_nodelete + &
												  	c_disablecc + &
													c_noretrieveonopen + &
													c_disableccinsert , &
                                       c_viewmodeblack)
													 
dw_importa_listino_ok.set_dw_options(sqlca, &
                                     pcca.null_object, &
                                     c_multiselect + &
												 c_nonew + &
												 c_nomodify + &
												 c_nodelete + &
												 c_disablecc + &
												 c_noretrieveonopen + &
												 c_disableccinsert , &
                                     c_viewmodeblack)


dw_importa_listino_errore.set_dw_options(sqlca, &
													  pcca.null_object, &	
													  c_multiselect + &
													  c_nonew + &
												     c_nomodify + &
													  c_nodelete + &
												  	  c_disablecc + &
													  c_noretrieveonopen + &
													  c_disableccinsert, &
                                         c_viewmodeblack)
													 

dw_errore_scrittura.set_dw_options(sqlca, &
   										  pcca.null_object, &	
											  c_multiselect + &
											  c_nonew + &
											  c_nomodify + &
											  c_nodelete + &
											  c_disablecc + &
											  c_noretrieveonopen + &
											  c_disableccinsert, &
                                   c_viewmodeblack)

SELECT stringa  
INTO   :st_file_listino.text  
FROM   parametri_azienda  
WHERE  cod_azienda = :s_cs_xx.cod_azienda
AND    cod_parametro = 'FLI';

save_on_close(c_socnosave)

em_data.text=string(today())

cb_togli.visible=false

end event

on w_imp_listino_fornitori.create
int iCurrent
call w_cs_xx_principale::create
this.gb_3=create gb_3
this.gb_1=create gb_1
this.gb_2=create gb_2
this.cb_1=create cb_1
this.st_2=create st_2
this.st_file_listino=create st_file_listino
this.st_4=create st_4
this.st_1=create st_1
this.st_3=create st_3
this.cb_2=create cb_2
this.st_7=create st_7
this.st_8=create st_8
this.rb_1=create rb_1
this.rb_2=create rb_2
this.st_ist_3=create st_ist_3
this.st_ist_5=create st_ist_5
this.st_10=create st_10
this.st_ist_7=create st_ist_7
this.st_ist_4=create st_ist_4
this.st_ist_2=create st_ist_2
this.cbx_valuta=create cbx_valuta
this.cbx_fornitore=create cbx_fornitore
this.cbx_data_validita=create cbx_data_validita
this.em_data=create em_data
this.cbx_um_acquisto=create cbx_um_acquisto
this.cbx_fattore_conversione=create cbx_fattore_conversione
this.em_fattore_conversione=create em_fattore_conversione
this.ddlb_um_acquisto=create ddlb_um_acquisto
this.ddlb_valute=create ddlb_valute
this.ddlb_fornitori=create ddlb_fornitori
this.rb_selezionati=create rb_selezionati
this.rb_totale=create rb_totale
this.st_6=create st_6
this.cb_leggi=create cb_leggi
this.cb_importa=create cb_importa
this.cb_scrivi=create cb_scrivi
this.cb_aggiungi=create cb_aggiungi
this.cb_togli=create cb_togli
this.dw_importa_listino_orig=create dw_importa_listino_orig
this.dw_errore_scrittura=create dw_errore_scrittura
this.st_ist_6=create st_ist_6
this.st_ist_1=create st_ist_1
this.dw_importa_listino_ok=create dw_importa_listino_ok
this.dw_importa_listino_errore=create dw_importa_listino_errore
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=gb_3
this.Control[iCurrent+2]=gb_1
this.Control[iCurrent+3]=gb_2
this.Control[iCurrent+4]=cb_1
this.Control[iCurrent+5]=st_2
this.Control[iCurrent+6]=st_file_listino
this.Control[iCurrent+7]=st_4
this.Control[iCurrent+8]=st_1
this.Control[iCurrent+9]=st_3
this.Control[iCurrent+10]=cb_2
this.Control[iCurrent+11]=st_7
this.Control[iCurrent+12]=st_8
this.Control[iCurrent+13]=rb_1
this.Control[iCurrent+14]=rb_2
this.Control[iCurrent+15]=st_ist_3
this.Control[iCurrent+16]=st_ist_5
this.Control[iCurrent+17]=st_10
this.Control[iCurrent+18]=st_ist_7
this.Control[iCurrent+19]=st_ist_4
this.Control[iCurrent+20]=st_ist_2
this.Control[iCurrent+21]=cbx_valuta
this.Control[iCurrent+22]=cbx_fornitore
this.Control[iCurrent+23]=cbx_data_validita
this.Control[iCurrent+24]=em_data
this.Control[iCurrent+25]=cbx_um_acquisto
this.Control[iCurrent+26]=cbx_fattore_conversione
this.Control[iCurrent+27]=em_fattore_conversione
this.Control[iCurrent+28]=ddlb_um_acquisto
this.Control[iCurrent+29]=ddlb_valute
this.Control[iCurrent+30]=ddlb_fornitori
this.Control[iCurrent+31]=rb_selezionati
this.Control[iCurrent+32]=rb_totale
this.Control[iCurrent+33]=st_6
this.Control[iCurrent+34]=cb_leggi
this.Control[iCurrent+35]=cb_importa
this.Control[iCurrent+36]=cb_scrivi
this.Control[iCurrent+37]=cb_aggiungi
this.Control[iCurrent+38]=cb_togli
this.Control[iCurrent+39]=dw_importa_listino_orig
this.Control[iCurrent+40]=dw_errore_scrittura
this.Control[iCurrent+41]=st_ist_6
this.Control[iCurrent+42]=st_ist_1
this.Control[iCurrent+43]=dw_importa_listino_ok
this.Control[iCurrent+44]=dw_importa_listino_errore
this.Control[iCurrent+45]=dw_folder
end on

on w_imp_listino_fornitori.destroy
call w_cs_xx_principale::destroy
destroy(this.gb_3)
destroy(this.gb_1)
destroy(this.gb_2)
destroy(this.cb_1)
destroy(this.st_2)
destroy(this.st_file_listino)
destroy(this.st_4)
destroy(this.st_1)
destroy(this.st_3)
destroy(this.cb_2)
destroy(this.st_7)
destroy(this.st_8)
destroy(this.rb_1)
destroy(this.rb_2)
destroy(this.st_ist_3)
destroy(this.st_ist_5)
destroy(this.st_10)
destroy(this.st_ist_7)
destroy(this.st_ist_4)
destroy(this.st_ist_2)
destroy(this.cbx_valuta)
destroy(this.cbx_fornitore)
destroy(this.cbx_data_validita)
destroy(this.em_data)
destroy(this.cbx_um_acquisto)
destroy(this.cbx_fattore_conversione)
destroy(this.em_fattore_conversione)
destroy(this.ddlb_um_acquisto)
destroy(this.ddlb_valute)
destroy(this.ddlb_fornitori)
destroy(this.rb_selezionati)
destroy(this.rb_totale)
destroy(this.st_6)
destroy(this.cb_leggi)
destroy(this.cb_importa)
destroy(this.cb_scrivi)
destroy(this.cb_aggiungi)
destroy(this.cb_togli)
destroy(this.dw_importa_listino_orig)
destroy(this.dw_errore_scrittura)
destroy(this.st_ist_6)
destroy(this.st_ist_1)
destroy(this.dw_importa_listino_ok)
destroy(this.dw_importa_listino_errore)
destroy(this.dw_folder)
end on

event close;call super::close;commit;
end event

event pc_setddlb;call super::pc_setddlb;f_po_loadddlb(ddlb_fornitori, &
                 sqlca, &
                 "anag_fornitori", &
                 "cod_fornitore", &
                 "rag_soc_1", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by rag_soc_1","")

f_po_loadddlb(ddlb_um_acquisto, &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by des_misura","")

f_po_loadddlb(ddlb_valute, &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + ")) order by des_valuta","")

f_po_loaddddw_dw(dw_importa_listino_errore, &
					  "cod_prodotto", &
					  sqlca, &
                 "anag_prodotti", &
					  "cod_prodotto", &
					  "des_prodotto", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

end event

type gb_3 from groupbox within w_imp_listino_fornitori
int X=46
int Y=1321
int Width=2835
int Height=141
int TabOrder=190
string Text="Fattore Conversione"
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_1 from groupbox within w_imp_listino_fornitori
int X=46
int Y=1221
int Width=1303
int Height=141
int TabOrder=160
string Text="Tipo Importazione"
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type gb_2 from groupbox within w_imp_listino_fornitori
int X=46
int Y=841
int Width=2835
int Height=481
int TabOrder=170
string Text="Correzione Errori"
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_1 from uo_cb_close within w_imp_listino_fornitori
int X=2538
int Y=1501
int Width=366
int Height=81
int TabOrder=140
string Text="&Chiudi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_2 from statictext within w_imp_listino_fornitori
int X=69
int Y=1021
int Width=261
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="Fornitore:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_file_listino from statictext within w_imp_listino_fornitori
int X=343
int Y=1381
int Width=2538
int Height=81
boolean Enabled=false
boolean BringToTop=true
boolean Border=true
BorderStyle BorderStyle=StyleLowered!
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_4 from statictext within w_imp_listino_fornitori
int X=46
int Y=1381
int Width=298
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="File Listino:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_1 from statictext within w_imp_listino_fornitori
int X=549
int Y=1121
int Width=339
int Height=77
boolean Enabled=false
boolean BringToTop=true
string Text="Data Validità"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_3 from statictext within w_imp_listino_fornitori
int X=69
int Y=921
int Width=261
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="Valuta:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_2 from commandbutton within w_imp_listino_fornitori
int X=69
int Y=1221
int Width=366
int Height=81
int TabOrder=80
boolean BringToTop=true
string Text="&Correggi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_riga, ll_selected[]
string ls_errore

setpointer(hourglass!)

if rb_1.checked then
	for ll_riga = 1 to dw_importa_listino_errore.rowcount()
	
		if cbx_data_validita.checked = true then
			dw_importa_listino_errore.setitem(ll_riga,"data_validita",date(em_data.text))	
		end if

		if cbx_fattore_conversione.checked = true then
			dw_importa_listino_errore.setitem(ll_riga,"fattore_conversione",double(em_fattore_conversione.text))
		end if

		if cbx_fornitore.checked = true then
			dw_importa_listino_errore.setitem(ll_riga,"cod_fornitore",f_po_selectddlb(ddlb_fornitori))
		end if

		if cbx_um_acquisto.checked = true then
			dw_importa_listino_errore.setitem(ll_riga,"cod_um_acquisto",f_po_selectddlb(ddlb_um_acquisto))
		end if

		if cbx_valuta.checked = true then
			dw_importa_listino_errore.setitem(ll_riga,"cod_valuta",f_po_selectddlb(ddlb_valute))
		end if

	next
else
	dw_importa_listino_errore.get_selected_rows(ll_selected[])
	for ll_riga = 1 to upperbound(ll_selected)
		if cbx_data_validita.checked = true then
			dw_importa_listino_errore.setitem(ll_selected[ll_riga],"data_validita",date(em_data.text))	
		end if

		if cbx_fattore_conversione.checked = true then
			dw_importa_listino_errore.setitem(ll_selected[ll_riga],"fattore_conversione",double(em_fattore_conversione.text))
		end if

		if cbx_fornitore.checked = true then
			dw_importa_listino_errore.setitem(ll_selected[ll_riga],"cod_fornitore",f_po_selectddlb(ddlb_fornitori))
		end if

		if cbx_um_acquisto.checked = true then
			dw_importa_listino_errore.setitem(ll_selected[ll_riga],"cod_um_acquisto",f_po_selectddlb(ddlb_um_acquisto))
		end if

		if cbx_valuta.checked = true then
			dw_importa_listino_errore.setitem(ll_selected[ll_riga],"cod_valuta",f_po_selectddlb(ddlb_valute))
		end if
	next
end if

dw_importa_listino_orig.Reset_DW_Modified(c_ResetChildren)
dw_importa_listino_ok.Reset_DW_Modified(c_ResetChildren)
dw_importa_listino_errore.Reset_DW_Modified(c_ResetChildren)
end event

type st_7 from statictext within w_imp_listino_fornitori
int X=1418
int Y=921
int Width=375
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="U.M. acquisto:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_8 from statictext within w_imp_listino_fornitori
int X=1372
int Y=1021
int Width=412
int Height=81
boolean Enabled=false
boolean BringToTop=true
string Text="Fatt. Conv.:"
Alignment Alignment=Right!
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_1 from radiobutton within w_imp_listino_fornitori
int X=1532
int Y=1241
int Width=229
int Height=61
boolean BringToTop=true
string Text="Totale"
boolean Checked=true
boolean LeftText=true
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_2 from radiobutton within w_imp_listino_fornitori
int X=1783
int Y=1241
int Width=549
int Height=61
boolean BringToTop=true
string Text="Record Selezionati"
boolean LeftText=true
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_ist_3 from statictext within w_imp_listino_fornitori
int X=572
int Y=481
int Width=1980
int Height=61
boolean Enabled=false
string Text="3) Verificare i dati importati tramite le due finestre dati (dati Ok e dati Errore)"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_ist_5 from statictext within w_imp_listino_fornitori
int X=572
int Y=601
int Width=1921
int Height=61
boolean Enabled=false
string Text="5) Aggiungere i dati appena corretti ai dati ok tramite il bottone Aggiungi L."
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_10 from statictext within w_imp_listino_fornitori
int X=846
int Y=1241
int Width=677
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text="Tipo Correzione/Aggiunta:"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_ist_7 from statictext within w_imp_listino_fornitori
int X=572
int Y=721
int Width=1189
int Height=61
boolean Enabled=false
string Text="7) Scrivere i dati nel DB tramite il tasto Scrivi"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_ist_4 from statictext within w_imp_listino_fornitori
int X=572
int Y=541
int Width=2181
int Height=61
boolean Enabled=false
string Text="4) Correggere i dati con errore (es. cod prodotto, cod fornitore valute ecc. mancanti)"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_ist_2 from statictext within w_imp_listino_fornitori
int X=572
int Y=421
int Width=1532
int Height=61
boolean Enabled=false
string Text="2) Eseguire l'importazione tramite il bottone Importa"
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cbx_valuta from checkbox within w_imp_listino_fornitori
int X=1303
int Y=921
int Width=69
int Height=81
boolean BringToTop=true
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event losefocus;if f_po_selectddlb(ddlb_valute)="" or isnull(f_po_selectddlb(ddlb_valute)) then
	cbx_valuta.checked=false
end if
end event

type cbx_fornitore from checkbox within w_imp_listino_fornitori
int X=1303
int Y=1021
int Width=69
int Height=81
boolean BringToTop=true
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event losefocus;if f_po_selectddlb(ddlb_fornitori)="" or isnull(f_po_selectddlb(ddlb_fornitori)) then
	cbx_fornitore.checked=false
end if
end event

type cbx_data_validita from checkbox within w_imp_listino_fornitori
int X=1303
int Y=1121
int Width=69
int Height=81
boolean BringToTop=true
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event losefocus;if isnull(em_data.text) then
	cbx_data_validita.checked=false
end if
end event

type em_data from editmask within w_imp_listino_fornitori
int X=892
int Y=1121
int Width=389
int Height=81
int TabOrder=40
boolean BringToTop=true
string Mask="dd/mm/yyyy"
MaskDataType MaskDataType=DateMask!
boolean Spin=true
string DisplayData=""
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event losefocus;cbx_data_validita.checked=true
end event

type cbx_um_acquisto from checkbox within w_imp_listino_fornitori
int X=2766
int Y=921
int Width=69
int Height=81
boolean BringToTop=true
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event losefocus;if f_po_selectddlb(ddlb_um_acquisto)="" or isnull(f_po_selectddlb(ddlb_um_acquisto)) then
	cbx_um_acquisto.checked=false
end if
end event

type cbx_fattore_conversione from checkbox within w_imp_listino_fornitori
int X=2766
int Y=1021
int Width=69
int Height=81
boolean BringToTop=true
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;if isnull(em_fattore_conversione.text) or double(em_fattore_conversione.text) = 0 then
	cbx_data_validita.checked=false
end if
end event

type em_fattore_conversione from editmask within w_imp_listino_fornitori
int X=1806
int Y=1021
int Width=961
int Height=81
int TabOrder=50
boolean BringToTop=true
Alignment Alignment=Right!
BorderStyle BorderStyle=StyleLowered!
string Mask="##########.####"
string DisplayData="< w_imp_listino_prodotti"
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event losefocus;cbx_fattore_conversione.checked=true
end event

type ddlb_um_acquisto from dropdownlistbox within w_imp_listino_fornitori
int X=1806
int Y=921
int Width=961
int Height=1361
int TabOrder=180
boolean BringToTop=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event selectionchanged;	cbx_um_acquisto.checked=true
end event

type ddlb_valute from dropdownlistbox within w_imp_listino_fornitori
int X=343
int Y=921
int Width=961
int Height=1361
int TabOrder=200
boolean BringToTop=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event selectionchanged;cbx_valuta.checked=true
end event

type ddlb_fornitori from dropdownlistbox within w_imp_listino_fornitori
int X=343
int Y=1021
int Width=961
int Height=1361
int TabOrder=150
boolean BringToTop=true
boolean VScrollBar=true
long TextColor=8388608
long BackColor=16777215
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event selectionchanged;cbx_fornitore.checked=true
end event

type rb_selezionati from radiobutton within w_imp_listino_fornitori
int X=481
int Y=1281
int Width=572
int Height=61
boolean BringToTop=true
string Text="Record Selezionati"
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type rb_totale from radiobutton within w_imp_listino_fornitori
int X=69
int Y=1281
int Width=321
int Height=61
boolean BringToTop=true
string Text="Totale"
boolean Checked=true
boolean LeftText=true
long TextColor=8388608
long BackColor=12632256
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_6 from statictext within w_imp_listino_fornitori
int X=92
int Y=1381
int Width=2743
int Height=61
boolean Enabled=false
boolean BringToTop=true
string Text=" 1 x (U.M. acquisto) x (Fattore Conversione) = 1 x (U.M. magazzino)            Prezzi espressi secondo U.M. acquisto"
boolean FocusRectangle=false
long BackColor=12632256
int TextSize=-9
int Weight=400
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type cb_leggi from uo_cb_ok within w_imp_listino_fornitori
event clicked pbm_bnclicked
int X=1372
int Y=1281
int Width=366
int Height=81
int TabOrder=100
boolean BringToTop=true
string Text="&Leggi File"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;call super::clicked;long ll_riga, ll_errore, ll_selected[], ll_i, ll_conta, ll_totale, ll_riga_sel[], al_riga_sel
long   ll_lungo,ll_valore,ll_posizione,ll_posizione_prec,ll_contatore_righe
string ls_cod_prodotto, ls_des_prodotto, ls_cod_iva, ls_cod_cat_mer, &
       ls_cod_fornitore, ls_controllo,ls_cod_f, &
       ls_cod_comodo,ls_cod_prodotto_fornitore,ls_cod_um_acquisto,ls_cod_um_interna, &
 		 ls_cod_valuta,ls_nome_file,ls_path,ls_cod_misura,ls_des_misura,ls_des_valuta,ls_riga
datetime ldt_data_validita
double ldd_fattore_conversione, &
		 ldd_quantita_1,ldd_prezzo_1,ldd_sconto_1, &
 		 ldd_quantita_2,ldd_prezzo_2,ldd_sconto_2, &
  		 ldd_quantita_3,ldd_prezzo_3,ldd_sconto_3, &
		 ldd_quantita_4,ldd_prezzo_4,ldd_sconto_4, &
		 ldd_quantita_5,ldd_prezzo_5,ldd_sconto_5 
		 
integer li_filenum

if (isnull(st_file_listino.text)) or (st_file_listino.text="") then
	g_mb.messagebox("Apice","Attenzione! Il parametro FLI (file listino.txt) non è presente o non e corretto, verificare dalla gestione parametri azienda.",exclamation!)	
end if		
		
if fileexists(st_file_listino.text) then
	dw_importa_listino_errore.reset()
	dw_importa_listino_ok.reset()
	dw_importa_listino_orig.reset()
	
	li_FileNum = FileOpen(st_file_listino.text, LineMode!)

	setpointer(hourglass!)
	ll_totale = 0

	do while not (Fileread(li_FileNum, ls_riga)=-100)
		ll_contatore_righe ++
      ll_posizione = pos( ls_riga, ";",1)
      ls_cod_prodotto = mid(ls_riga, 1, ll_posizione - 1)
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ls_des_prodotto = mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1)
      ll_posizione_prec = ll_posizione
      ll_posizione = pos( ls_riga, ";",ll_posizione_prec + 1)
      ls_cod_comodo = mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1)
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ls_cod_fornitore = mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1)
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ls_cod_prodotto_fornitore = mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1)
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ls_cod_um_acquisto = mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1)
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ls_cod_um_interna = mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1)
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ls_cod_valuta = mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1)
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ldt_data_validita = datetime(Date(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1)))
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ldd_fattore_conversione = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1))
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ldd_quantita_1 = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1))
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ldd_prezzo_1 = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1))
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ldd_sconto_1 = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1))
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ldd_quantita_2 = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1))
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ldd_prezzo_2 = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1))
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ldd_sconto_2 = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1))
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ldd_quantita_3 = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1))
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ldd_prezzo_3 = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1))
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ldd_sconto_3 = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1))
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ldd_quantita_4 = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1))
      ll_posizione_prec = ll_posizione
      ll_posizione = pos( ls_riga, ";",ll_posizione_prec + 1)
      ldd_prezzo_4 = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1))
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ldd_sconto_4 = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1))
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ldd_quantita_5 = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1))
      ll_posizione_prec = ll_posizione
      ll_posizione = pos(ls_riga, ";",ll_posizione_prec + 1)
      ldd_prezzo_5 = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec - 1))
      ll_posizione_prec = ll_posizione
      ll_posizione = Len(ls_riga)
      ldd_sconto_5 = double(mid(ls_riga, ll_posizione_prec + 1, ll_posizione - ll_posizione_prec))
		if not ((isnull(ls_des_prodotto) or ls_des_prodotto="") and & 
				 (isnull(ls_cod_prodotto) or ls_cod_prodotto="") and &
			 	 (isnull(ls_cod_prodotto_fornitore) or ls_cod_prodotto_fornitore="") and &
				 (isnull(ls_cod_comodo) or ls_cod_comodo="") and & 
				 (isnull(ls_cod_fornitore) or ls_cod_fornitore="") and &
				 (isnull(ls_cod_um_acquisto) or ls_cod_um_acquisto="") and & 
				 (isnull(ls_cod_um_interna) or ls_cod_um_interna="") and &
				 (isnull(ls_cod_valuta) or ls_cod_valuta="")) then

			dw_importa_listino_orig.insertrow(ll_contatore_righe)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"des_prodotto",ls_des_prodotto)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"cod_prodotto",ls_cod_prodotto)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"cod_prodotto_fornitore",ls_cod_prodotto_fornitore)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"cod_comodo",ls_cod_comodo)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"cod_fornitore",ls_cod_fornitore)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"cod_um_acquisto",ls_cod_um_acquisto)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"cod_um_interna",ls_cod_um_interna)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"cod_valuta",ls_cod_valuta)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"data_validita",ldt_data_validita)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"fattore_conversione",ldd_fattore_conversione)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"quantita_1",ldd_quantita_1)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"quantita_2",ldd_quantita_2)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"quantita_3",ldd_quantita_3)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"quantita_4",ldd_quantita_4)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"quantita_5",ldd_quantita_5)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"prezzo_1",ldd_prezzo_1)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"prezzo_2",ldd_prezzo_2)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"prezzo_3",ldd_prezzo_3)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"prezzo_4",ldd_prezzo_4)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"prezzo_5",ldd_prezzo_5)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"sconto_1",ldd_sconto_1)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"sconto_2",ldd_sconto_2)
   		dw_importa_listino_orig.setitem(ll_contatore_righe,"sconto_3",ldd_sconto_3)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"sconto_4",ldd_sconto_4)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"sconto_5",ldd_sconto_5)
			dw_importa_listino_orig.setitem(ll_contatore_righe,"elimina","N")
		end if
	loop
   Fileclose(li_FileNum)
	dw_importa_listino_orig.Reset_DW_Modified(c_ResetChildren)
	dw_importa_listino_orig.TriggerEvent("pcd_search")
else
	g_mb.messagebox("Apice","Attenzione! Il file indicato dal parametro FLI (file listino.txt) non esiste, verificare dalla gestione parametri azienda.",exclamation!)	
end if
setpointer(arrow!)
end event

type cb_importa from uo_cb_ok within w_imp_listino_fornitori
event clicked pbm_bnclicked
int X=1761
int Y=1281
int Width=366
int Height=81
int TabOrder=110
boolean BringToTop=true
string Text="&Importa"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;call super::clicked;integer li_risposta
long ll_riga, ll_selected[]
string ls_errore

setpointer(hourglass!)
//dw_importa_listino_errore.reset()
//dw_importa_listino_ok.reset()

if rb_totale.checked then
  	for ll_riga = 1 to dw_importa_listino_orig.rowcount()
		li_risposta = wf_aggiorna_listino(ll_riga, ls_errore)
   next
else
	dw_importa_listino_orig.get_selected_rows(ll_selected[])
	for ll_riga = 1 to upperbound(ll_selected[])
		li_risposta = wf_aggiorna_listino(ll_selected[ll_riga], ls_errore)
	next
end if

if dw_importa_listino_errore.rowcount()>0 then
	dw_importa_listino_errore.resetupdate()
	dw_importa_listino_errore.TriggerEvent("pcd_search")
end if

if dw_importa_listino_ok.rowcount()>0 then
	dw_importa_listino_ok.resetupdate()
	dw_importa_listino_ok.TriggerEvent("pcd_search")
end if

li_risposta=wf_elimina_orig(ls_errore)
dw_importa_listino_orig.Reset_DW_Modified(c_ResetChildren)
dw_importa_listino_ok.Reset_DW_Modified(c_ResetChildren)
dw_importa_listino_errore.Reset_DW_Modified(c_ResetChildren)

setpointer(arrow!)
end event

type cb_scrivi from uo_cb_ok within w_imp_listino_fornitori
event clicked pbm_bnclicked
int X=2515
int Y=1361
int Width=366
int Height=81
int TabOrder=90
boolean BringToTop=true
string Text="&Scrivi"
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;call super::clicked;long   ll_contatore_righe_ok,ll_contatore_righe_errore,ll_riga
string ls_des_prodotto,ls_cod_prodotto,ls_cod_fornitore, &
       ls_cod_comodo,ls_cod_prodotto_fornitore,ls_cod_um_acquisto,ls_cod_um_interna, &
 		 ls_cod_valuta,ls_cod_misura,ls_test,ls_cod_cat_mer,ls_errore
datetime ldt_data_validita
double ldd_fattore_conversione, &
		 ldd_quantita_1,ldd_prezzo_1,ldd_sconto_1, &
 		 ldd_quantita_2,ldd_prezzo_2,ldd_sconto_2, &
  		 ldd_quantita_3,ldd_prezzo_3,ldd_sconto_3, &
		 ldd_quantita_4,ldd_prezzo_4,ldd_sconto_4, &
		 ldd_quantita_5,ldd_prezzo_5,ldd_sconto_5 
integer li_risposta

setpointer(hourglass!)
ll_contatore_righe_errore=dw_errore_scrittura.rowcount()

for ll_riga = 1 to dw_importa_listino_ok.rowcount()
   ls_cod_prodotto = dw_importa_listino_ok.getitemstring(ll_riga,"cod_prodotto")			      
	ls_des_prodotto = dw_importa_listino_ok.getitemstring(ll_riga,"des_prodotto")			      
	ls_cod_fornitore = dw_importa_listino_ok.getitemstring(ll_riga,"cod_fornitore")			      
	ls_cod_prodotto_fornitore = dw_importa_listino_ok.getitemstring(ll_riga,"cod_prodotto_fornitore")			      
	ls_cod_comodo = dw_importa_listino_ok.getitemstring(ll_riga,"cod_comodo")			      
	ls_cod_um_acquisto = dw_importa_listino_ok.getitemstring(ll_riga,"cod_um_acquisto")			      
	ls_cod_um_interna = dw_importa_listino_ok.getitemstring(ll_riga,"cod_um_interna")			      
	ls_cod_valuta = dw_importa_listino_ok.getitemstring(ll_riga,"cod_valuta")			      
	ldt_data_validita = dw_importa_listino_ok.getitemdatetime(ll_riga,"data_validita")
	ldd_fattore_conversione = dw_importa_listino_ok.getitemnumber(ll_riga,"fattore_conversione")
	ldd_quantita_1 = dw_importa_listino_ok.getitemnumber(ll_riga,"quantita_1")
	ldd_prezzo_1 = dw_importa_listino_ok.getitemnumber(ll_riga,"prezzo_1")
	ldd_sconto_1 = dw_importa_listino_ok.getitemnumber(ll_riga,"sconto_1")   
	ldd_quantita_2 = dw_importa_listino_ok.getitemnumber(ll_riga,"quantita_2")
	ldd_prezzo_2 = dw_importa_listino_ok.getitemnumber(ll_riga,"prezzo_2")   
	ldd_sconto_2 = dw_importa_listino_ok.getitemnumber(ll_riga,"sconto_2")   
	ldd_quantita_3 = dw_importa_listino_ok.getitemnumber(ll_riga,"quantita_3")  
	ldd_prezzo_3 = dw_importa_listino_ok.getitemnumber(ll_riga,"prezzo_3")   
	ldd_sconto_3 = dw_importa_listino_ok.getitemnumber(ll_riga,"sconto_3")   
	ldd_quantita_4 = dw_importa_listino_ok.getitemnumber(ll_riga,"quantita_4")   
	ldd_prezzo_4 = dw_importa_listino_ok.getitemnumber(ll_riga,"prezzo_4")   
	ldd_sconto_4 = dw_importa_listino_ok.getitemnumber(ll_riga,"sconto_4")   
	ldd_quantita_5 = dw_importa_listino_ok.getitemnumber(ll_riga,"quantita_5")   
	ldd_prezzo_5 = dw_importa_listino_ok.getitemnumber(ll_riga,"prezzo_5")   
	ldd_sconto_5 =dw_importa_listino_ok.getitemnumber(ll_riga,"sconto_5")   
	
   SELECT cod_azienda
   INTO   :ls_test  
   FROM   tab_prod_fornitori
   WHERE  cod_azienda = :s_cs_xx.cod_azienda 
   AND    cod_prodotto = :ls_cod_prodotto
	and    cod_fornitore = :ls_cod_fornitore
	and    cod_prod_fornitore = :ls_cod_prodotto_fornitore;

	if sqlca.sqlcode = 100 then
	   INSERT INTO tab_prod_fornitori
   	           (cod_azienda,   
      	         cod_prodotto,   
         	      cod_fornitore,   
            	   cod_prod_fornitore,   
	               des_prod_fornitore)  
	   VALUES     (:s_cs_xx.cod_azienda,   
   	            :ls_cod_prodotto,   
      	         :ls_cod_fornitore,   
         	      :ls_cod_prodotto_fornitore,   
            	   :ls_des_prodotto);
	end if

	if sqlca.sqlcode < 0 then
		dw_errore_scrittura.insertrow(ll_contatore_righe_errore)
		dw_errore_scrittura.setitem(ll_contatore_righe_errore,"descrizione_errore",sqlca.sqlerrtext)
		ll_contatore_righe_errore++
	end if
	
	DELETE FROM listini_fornitori
	WHERE  cod_azienda = :s_cs_xx.cod_azienda 
	AND    cod_prodotto = :ls_cod_prodotto 
	AND    cod_fornitore = :ls_cod_fornitore
	and    cod_valuta =:ls_cod_valuta
	and    data_inizio_val = :ldt_data_validita;

	INSERT INTO listini_fornitori  
   	        (cod_azienda,   
      	      cod_prodotto,   
         	   cod_fornitore,   
            	cod_valuta,   
	            data_inizio_val,   
   	         des_listino_for,   
      	      quantita_1,   
         	   prezzo_1,   
            	sconto_1,   
	            quantita_2,   
   	         prezzo_2,   
      	      sconto_2,   
	            quantita_3,   
   	         prezzo_3,   
      	      sconto_3,   
         	   quantita_4,   
            	prezzo_4,   
	            sconto_4,   
   	         quantita_5,   
      	      prezzo_5,   
         	   sconto_5,   
            	flag_for_pref,   
	            prezzo_ult_acquisto,   
   	         data_ult_acquisto,   
      	      cod_misura,   
         	   fat_conversione)  
	  VALUES ( :s_cs_xx.cod_azienda,   
   	        :ls_cod_prodotto,   
      	     :ls_cod_fornitore,   
         	  :ls_cod_valuta,   
	           :ldt_data_validita,   
   	        null,   
      	     :ldd_quantita_1,   
         	  :ldd_prezzo_1,   
	           :ldd_sconto_1,   
   	        :ldd_quantita_2,   
      	     :ldd_prezzo_2,   
         	  :ldd_sconto_2,   
	           :ldd_quantita_3,   
   	        :ldd_prezzo_3,   
      	     :ldd_sconto_3,   
         	  :ldd_quantita_4,   
	           :ldd_prezzo_4,   
   	        :ldd_sconto_4,   
      	     :ldd_quantita_5,   
         	  :ldd_prezzo_5,   
	           :ldd_sconto_5,   
   	        'N',   
      	     0,   
         	  null,   
	           :ls_cod_um_acquisto,   
   	        :ldd_fattore_conversione);

		if sqlca.sqlcode<0 then
			dw_errore_scrittura.insertrow(ll_contatore_righe_errore)
			dw_errore_scrittura.setitem(ll_contatore_righe_errore,"descrizione_errore",sqlca.sqlerrtext)
			ll_contatore_righe_errore++
		else
			dw_importa_listino_ok.setitem(ll_riga,"elimina","S")   
		end if
next

li_risposta=wf_elimina_dati_ok(ls_errore)

setpointer(arrow!)
end event

type cb_aggiungi from commandbutton within w_imp_listino_fornitori
event clicked pbm_bnclicked
int X=458
int Y=1221
int Width=366
int Height=81
int TabOrder=60
boolean BringToTop=true
string Text="&Aggiungi L."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;integer li_risposta
long ll_riga, ll_selected[]
string ls_errore

setpointer(hourglass!)

if rb_1.checked then
  	for ll_riga = 1 to dw_importa_listino_errore.rowcount()
		li_risposta = wf_aggiungi_listino(ll_riga, ls_errore)
   next

	li_risposta = wf_togli_listino(ls_errore)

else
	dw_importa_listino_errore.get_selected_rows(ll_selected[])
	for ll_riga = 1 to upperbound(ll_selected)
		li_risposta = wf_aggiungi_listino(ll_selected[ll_riga], ls_errore)
	next

	li_risposta = wf_togli_listino(ls_errore)

end if

dw_importa_listino_orig.Reset_DW_Modified(c_ResetChildren)
dw_importa_listino_ok.Reset_DW_Modified(c_ResetChildren)
dw_importa_listino_errore.Reset_DW_Modified(c_ResetChildren)

if dw_importa_listino_ok.rowcount()>0 then
	dw_importa_listino_ok.TriggerEvent("pcd_search")
end if

setpointer(arrow!)
end event

type cb_togli from commandbutton within w_imp_listino_fornitori
event clicked pbm_bnclicked
int X=846
int Y=1221
int Width=366
int Height=81
int TabOrder=70
boolean BringToTop=true
string Text="&Togli L."
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

event clicked;long ll_riga, ll_selected[]
string ls_errore

setpointer(hourglass!)

if rb_1.checked then
	do while dw_importa_listino_ok.rowcount()>0
		dw_importa_listino_ok.SelectRow ( 1, false )
		dw_importa_listino_ok.Delete_DW_Row(1, c_ignoreChanges,c_noRefreshChildren)
	loop
else
//	dw_importa_listino_ok.get_selected_rows(ll_selected[])
//	do while upperbound(ll_selected[])>0
//		dw_importa_listino_ok.Delete_DW_Row(ll_selected[1], c_ignoreChanges,c_noRefreshChildren)
//		dw_importa_listino_ok.get_selected_rows(ll_selected[])
//	loop
end if
end event

type dw_importa_listino_orig from uo_cs_xx_dw within w_imp_listino_fornitori
event pcd_retrieve pbm_custom60
int X=46
int Y=121
int Width=2835
int Height=1081
int TabOrder=130
string DataObject="d_importa_listino_orig"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
end type

type dw_errore_scrittura from uo_cs_xx_dw within w_imp_listino_fornitori
int X=46
int Y=121
int Width=2835
int Height=1341
int TabOrder=10
string DataObject="d_errore_scrittura"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
end type

type st_ist_6 from statictext within w_imp_listino_fornitori
int X=572
int Y=661
int Width=1509
int Height=61
boolean Enabled=false
string Text="6) Verificare nuovamente i dati che non vengono aggiunti."
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type st_ist_1 from statictext within w_imp_listino_fornitori
int X=572
int Y=361
int Width=1532
int Height=61
boolean Enabled=false
string Text="1) Leggere il File da importare tramite il bottone Leggi File."
boolean FocusRectangle=false
long TextColor=8388608
long BackColor=79741120
int TextSize=-9
int Weight=700
string FaceName="Arial"
FontFamily FontFamily=Swiss!
FontPitch FontPitch=Variable!
end type

type dw_importa_listino_ok from uo_cs_xx_dw within w_imp_listino_fornitori
int X=46
int Y=121
int Width=2835
int Height=1221
int TabOrder=30
string DataObject="d_importa_listino_ok"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
end type

type dw_importa_listino_errore from uo_cs_xx_dw within w_imp_listino_fornitori
int X=46
int Y=121
int Width=2835
int Height=721
int TabOrder=20
string DataObject="d_importa_listino_errore"
BorderStyle BorderStyle=StyleLowered!
boolean HScrollBar=true
boolean VScrollBar=true
end type

event itemchanged;call super::itemchanged;if i_extendmode then
	string ls_cod_prodotto,ls_cod_um_interna	

	if i_colname = "cod_prodotto" then
		ls_cod_prodotto = data
		if not isnull(ls_cod_prodotto) or ls_cod_prodotto = "" then
			SELECT cod_misura_mag
	   	INTO   :ls_cod_um_interna  
	 	  	FROM   anag_prodotti
			WHERE  cod_azienda = :s_cs_xx.cod_azienda 
			AND    cod_prodotto = :ls_cod_prodotto;	

			setitem(row,"cod_um_interna",ls_cod_um_interna)

		end if
	end if
end if
end event

type dw_folder from u_folder within w_imp_listino_fornitori
int X=23
int Y=21
int Width=2881
int Height=1461
int TabOrder=120
end type

