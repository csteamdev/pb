﻿$PBExportHeader$w_listini_fornitori.srw
$PBExportComments$Finestra Listini Fornitori
forward
global type w_listini_fornitori from w_cs_xx_principale
end type
type cb_annulla from commandbutton within w_listini_fornitori
end type
type cb_cerca from commandbutton within w_listini_fornitori
end type
type cb_prezzo_acq from commandbutton within w_listini_fornitori
end type
type dw_listini_fornitori_det from uo_cs_xx_dw within w_listini_fornitori
end type
type dw_listini_fornitori_sel from u_dw_search within w_listini_fornitori
end type
type dw_listini_fornitori_lista from uo_cs_xx_dw within w_listini_fornitori
end type
type dw_folder from u_folder within w_listini_fornitori
end type
end forward

global type w_listini_fornitori from w_cs_xx_principale
integer width = 4192
integer height = 1912
string title = "Listini Fornitori"
cb_annulla cb_annulla
cb_cerca cb_cerca
cb_prezzo_acq cb_prezzo_acq
dw_listini_fornitori_det dw_listini_fornitori_det
dw_listini_fornitori_sel dw_listini_fornitori_sel
dw_listini_fornitori_lista dw_listini_fornitori_lista
dw_folder dw_folder
end type
global w_listini_fornitori w_listini_fornitori

event pc_setddlb;call super::pc_setddlb;f_po_loaddddw_dw(dw_listini_fornitori_det, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_listini_fornitori_det, &
                 "cod_misura", &
                 sqlca, &
                 "tab_misure", &
                 "cod_misura", &
                 "des_misura", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "' and ((flag_blocco <> 'S') or (flag_blocco = 'S' and data_blocco > " + s_cs_xx.db_funzioni.oggi + "))")

f_po_loaddddw_dw(dw_listini_fornitori_sel, &
                 "cod_valuta", &
                 sqlca, &
                 "tab_valute", &
                 "cod_valuta", &
                 "des_valuta", &
                 "cod_azienda = '" + s_cs_xx.cod_azienda + "'")
end event

event pc_setwindow;call super::pc_setwindow;windowobject l_objects[]


dw_listini_fornitori_lista.set_dw_key("cod_azienda")

dw_listini_fornitori_lista.set_dw_options(sqlca, &
                                        pcca.null_object, &
                                        c_noretrieveonopen, &
                                        c_default)

dw_listini_fornitori_det.set_dw_options(sqlca, &
                                        dw_listini_fornitori_lista, &
                                        c_sharedata + c_scrollparent, &
                                        c_default)

iuo_dw_main = dw_listini_fornitori_lista

cb_prezzo_acq.enabled=false
dw_listini_fornitori_det.object.b_ricerca_fornitore.enabled=false
dw_listini_fornitori_det.object.b_ricerca_prodotto.enabled=false
dw_listini_fornitori_det.object.b_ricerca_fornitore.visible=true
dw_listini_fornitori_det.object.b_ricerca_prodotto.visible=true

dw_folder.fu_folderoptions(dw_folder.c_defaultheight, &
                           dw_folder.c_foldertableft)

l_objects[1] = dw_listini_fornitori_lista
l_objects[2] = cb_prezzo_acq
dw_folder.fu_assigntab(1, "L.", l_Objects[])

l_objects[1] = dw_listini_fornitori_sel
l_objects[2] = cb_cerca
l_objects[3] = cb_annulla
dw_folder.fu_assigntab(2, "R.", l_Objects[])

dw_folder.fu_foldercreate(2,2)

dw_folder.fu_selecttab(2)

cb_annulla.postevent("clicked")
end event

on w_listini_fornitori.create
int iCurrent
call super::create
this.cb_annulla=create cb_annulla
this.cb_cerca=create cb_cerca
this.cb_prezzo_acq=create cb_prezzo_acq
this.dw_listini_fornitori_det=create dw_listini_fornitori_det
this.dw_listini_fornitori_sel=create dw_listini_fornitori_sel
this.dw_listini_fornitori_lista=create dw_listini_fornitori_lista
this.dw_folder=create dw_folder
iCurrent=UpperBound(this.Control)
this.Control[iCurrent+1]=this.cb_annulla
this.Control[iCurrent+2]=this.cb_cerca
this.Control[iCurrent+3]=this.cb_prezzo_acq
this.Control[iCurrent+4]=this.dw_listini_fornitori_det
this.Control[iCurrent+5]=this.dw_listini_fornitori_sel
this.Control[iCurrent+6]=this.dw_listini_fornitori_lista
this.Control[iCurrent+7]=this.dw_folder
end on

on w_listini_fornitori.destroy
call super::destroy
destroy(this.cb_annulla)
destroy(this.cb_cerca)
destroy(this.cb_prezzo_acq)
destroy(this.dw_listini_fornitori_det)
destroy(this.dw_listini_fornitori_sel)
destroy(this.dw_listini_fornitori_lista)
destroy(this.dw_folder)
end on

event pc_delete;call super::pc_delete;cb_prezzo_acq.enabled=false
dw_listini_fornitori_det.object.b_ricerca_fornitore.enabled=false
dw_listini_fornitori_det.object.b_ricerca_prodotto.enabled=false

end event

event pc_new;call super::pc_new;dw_listini_fornitori_det.setitem(dw_listini_fornitori_det.getrow(), "data_inizio_val", datetime(today()))

end event

type cb_annulla from commandbutton within w_listini_fornitori
integer x = 3689
integer y = 136
integer width = 366
integer height = 80
integer taborder = 50
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Annulla"
end type

event clicked;string	ls_null

datetime ldt_null


setnull(ls_null)

setnull(ldt_null)

dw_listini_fornitori_sel.setitem(1,"data_da",ldt_null)

dw_listini_fornitori_sel.setitem(1,"data_a",ldt_null)

dw_listini_fornitori_sel.setitem(1,"cod_prodotto_da",ls_null)

dw_listini_fornitori_sel.setitem(1,"cod_prodotto_a",ls_null)

dw_listini_fornitori_sel.setitem(1,"cod_fornitore",ls_null)

dw_listini_fornitori_sel.setitem(1,"cod_valuta",ls_null)
end event

type cb_cerca from commandbutton within w_listini_fornitori
integer x = 3689
integer y = 36
integer width = 366
integer height = 80
integer taborder = 40
integer textsize = -9
integer weight = 700
fontcharset fontcharset = ansi!
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Cerca"
end type

event clicked;long		ll_pos

datetime ldt_da, ldt_a

string	ls_cod_prodotto_da, ls_cod_prodotto_a, ls_cod_fornitore, ls_cod_valuta, ls_sql, ls_flag_condizioni_recenti


dw_listini_fornitori_sel.accepttext()

ldt_da = dw_listini_fornitori_sel.getitemdatetime(1,"data_da")

ldt_a = dw_listini_fornitori_sel.getitemdatetime(1,"data_a")

ls_cod_prodotto_da = dw_listini_fornitori_sel.getitemstring(1,"cod_prodotto_da")

ls_cod_prodotto_a = dw_listini_fornitori_sel.getitemstring(1,"cod_prodotto_a")

ls_cod_fornitore = dw_listini_fornitori_sel.getitemstring(1,"cod_fornitore")

ls_cod_valuta = dw_listini_fornitori_sel.getitemstring(1,"cod_valuta")

ls_flag_condizioni_recenti =  dw_listini_fornitori_sel.getitemstring(1,"flag_condizioni_recenti")

ls_sql = dw_listini_fornitori_lista.getsqlselect()

//ls_sql = upper(ls_sql)

ll_pos = pos(upper(ls_sql),"WHERE",1)

if not isnull(ll_pos) and ll_pos > 0 then
	ls_sql = left(ls_sql,ll_pos - 1)
end if

ls_sql += " where cod_azienda = '" + s_cs_xx.cod_azienda + "'"

if not isnull(ldt_da) then
	ls_sql += " and data_inizio_val >= '" + string(ldt_da,s_cs_xx.db_funzioni.formato_data) + "'"
end if

if not isnull(ldt_a) then
	ls_sql += " and data_inizio_val <= '" + string(ldt_a,s_cs_xx.db_funzioni.formato_data) + "'"
end if

if not isnull(ls_cod_prodotto_da) then
	ls_sql += " and cod_prodotto >= '" + ls_cod_prodotto_da + "'"
end if

if not isnull(ls_cod_prodotto_a) then
	ls_sql += " and cod_prodotto <= '" + ls_cod_prodotto_a + "'"
end if

if not isnull(ls_cod_fornitore) then
	ls_sql += " and cod_fornitore = '" + ls_cod_fornitore + "'"
end if

if not isnull(ls_cod_valuta) then
	ls_sql += " and cod_valuta = '" + ls_cod_valuta + "'"
end if

if ls_flag_condizioni_recenti = "S" then
	ls_sql += " and  (SELECT COUNT(*) FROM listini_fornitori AS p WHERE p.cod_prodotto = listini_fornitori.cod_prodotto and p.cod_fornitore = listini_fornitori.cod_fornitore AND  abs(datediff(DAY, p.data_inizio_val, getdate())) <  abs(datediff(DAY, listini_fornitori.data_inizio_val, getdate()))    ) = 0 "
end if

ls_sql += " order by cod_prodotto ASC, cod_fornitore ASC, cod_valuta ASC, data_inizio_val ASC"

if dw_listini_fornitori_lista.setsqlselect(ls_sql) = -1 then
	g_mb.messagebox("APICE","Errore in impostazione select",stopsign!)
	return -1
end if

dw_folder.fu_selecttab(1)

dw_listini_fornitori_lista.change_dw_current()

parent.postevent("pc_retrieve")
end event

type cb_prezzo_acq from commandbutton within w_listini_fornitori
event clicked pbm_bnclicked
integer x = 3689
integer y = 36
integer width = 366
integer height = 80
integer taborder = 30
integer textsize = -9
integer weight = 700
fontpitch fontpitch = variable!
fontfamily fontfamily = swiss!
string facename = "Arial"
string text = "Prezzo"
end type

event clicked;dw_listini_fornitori_det.setcolumn(25)
s_cs_xx.parametri.parametro_s_1 = dw_listini_fornitori_det.gettext()
dw_listini_fornitori_det.setcolumn(8)
s_cs_xx.parametri.parametro_d_1 = double(dw_listini_fornitori_det.gettext())
dw_listini_fornitori_det.setcolumn(11)
s_cs_xx.parametri.parametro_d_2 = double(dw_listini_fornitori_det.gettext())
dw_listini_fornitori_det.setcolumn(14)
s_cs_xx.parametri.parametro_d_3 = double(dw_listini_fornitori_det.gettext())
dw_listini_fornitori_det.setcolumn(17)
s_cs_xx.parametri.parametro_d_4 = double(dw_listini_fornitori_det.gettext())
dw_listini_fornitori_det.setcolumn(20)
s_cs_xx.parametri.parametro_d_5 = double(dw_listini_fornitori_det.gettext())
dw_listini_fornitori_det.setcolumn(26)
s_cs_xx.parametri.parametro_d_6 = double(dw_listini_fornitori_det.gettext())
dw_listini_fornitori_det.setcolumn(7)
s_cs_xx.parametri.parametro_d_7 = double(dw_listini_fornitori_det.gettext())
dw_listini_fornitori_det.setcolumn(10)
s_cs_xx.parametri.parametro_d_8 = double(dw_listini_fornitori_det.gettext())
dw_listini_fornitori_det.setcolumn(13)
s_cs_xx.parametri.parametro_d_9 = double(dw_listini_fornitori_det.gettext())
dw_listini_fornitori_det.setcolumn(16)
s_cs_xx.parametri.parametro_d_10 = double(dw_listini_fornitori_det.gettext())
dw_listini_fornitori_det.setcolumn(19)
s_cs_xx.parametri.parametro_d_11 = double(dw_listini_fornitori_det.gettext())

window_open(w_prezzo_listini_um, 0)

if s_cs_xx.parametri.parametro_d_6 <> 0 then
   dw_listini_fornitori_det.setitem(dw_listini_fornitori_det.getrow(), "prezzo_1", s_cs_xx.parametri.parametro_d_1)
   dw_listini_fornitori_det.setitem(dw_listini_fornitori_det.getrow(), "prezzo_2", s_cs_xx.parametri.parametro_d_2)
   dw_listini_fornitori_det.setitem(dw_listini_fornitori_det.getrow(), "prezzo_3", s_cs_xx.parametri.parametro_d_3)
   dw_listini_fornitori_det.setitem(dw_listini_fornitori_det.getrow(), "prezzo_4", s_cs_xx.parametri.parametro_d_4)
   dw_listini_fornitori_det.setitem(dw_listini_fornitori_det.getrow(), "prezzo_5", s_cs_xx.parametri.parametro_d_5)
   dw_listini_fornitori_det.setitem(dw_listini_fornitori_det.getrow(), "quantita_1", s_cs_xx.parametri.parametro_d_7)
   dw_listini_fornitori_det.setitem(dw_listini_fornitori_det.getrow(), "quantita_2", s_cs_xx.parametri.parametro_d_8)
   dw_listini_fornitori_det.setitem(dw_listini_fornitori_det.getrow(), "quantita_3", s_cs_xx.parametri.parametro_d_9)
   dw_listini_fornitori_det.setitem(dw_listini_fornitori_det.getrow(), "quantita_4", s_cs_xx.parametri.parametro_d_10)
   dw_listini_fornitori_det.setitem(dw_listini_fornitori_det.getrow(), "quantita_5", s_cs_xx.parametri.parametro_d_11)
end if
end event

type dw_listini_fornitori_det from uo_cs_xx_dw within w_listini_fornitori
integer x = 32
integer y = 580
integer width = 2693
integer height = 1200
integer taborder = 40
string dataobject = "d_listini_fornitori_det"
borderstyle borderstyle = styleraised!
end type

event itemchanged;call super::itemchanged;if i_extendmode then
   string ls_cod_misura_acq, ls_cod_misura_mag, ls_cod_prodotto
   double ld_fat_conversione_acq

   
   choose case i_colname
      case "cod_prodotto"
         select anag_prodotti.cod_misura_mag,
					 anag_prodotti.cod_misura_acq,
                anag_prodotti.fat_conversione_acq
         into	 :ls_cod_misura_mag,
					 :ls_cod_misura_acq,
                :ld_fat_conversione_acq
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :i_coltext;
   
         this.setitem(i_rownbr, "cod_misura", ls_cod_misura_acq)
			dw_listini_fornitori_det.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + ":'")
			dw_listini_fornitori_det.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + ":'")
         this.setitem(i_rownbr, "fat_conversione", ld_fat_conversione_acq)
			cb_prezzo_acq.text= "Prezzo al " + this.getitemstring(i_rownbr, "cod_misura")
			if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") then
				this.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
			else
				this.modify("st_fattore_conv.text=''")
			end if
		case "cod_misura"
			if not isnull(i_coltext) and i_coltext <> "" then
	   		cb_prezzo_acq.text = "Prezzo al " + i_coltext
			else
				cb_prezzo_acq.text = "Prezzo"
			end if

         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.cod_misura_mag
         into   :ls_cod_misura_mag
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> i_coltext then
				this.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + i_coltext + ")'")
			else
				this.modify("st_fattore_conv.text=''")
			end if
		case "fat_conversione"
         ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
         select anag_prodotti.cod_misura_mag
         into   :ls_cod_misura_mag
         from   anag_prodotti
         where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
                anag_prodotti.cod_prodotto = :ls_cod_prodotto;

			if ls_cod_misura_mag <> this.getitemstring(i_rownbr, "cod_misura") then
				this.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + i_coltext + " " + this.getitemstring(i_rownbr, "cod_misura") + ")'")
			else
				this.modify("st_fattore_conv.text=''")
			end if
	end choose
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto"
		guo_ricerca.uof_ricerca_prodotto(dw_listini_fornitori_det,"cod_prodotto")
		
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_listini_fornitori_det,"cod_fornitore")
end choose
end event

type dw_listini_fornitori_sel from u_dw_search within w_listini_fornitori
integer x = 137
integer y = 40
integer width = 3529
integer height = 496
integer taborder = 40
string dataobject = "d_listini_fornitori_sel"
end type

event itemchanged;call super::itemchanged;if dwo.name = "cod_prodotto_da" and not isnull(data) then
	setitem(row,"cod_prodotto_a",data)
end if
end event

event buttonclicked;call super::buttonclicked;choose case dwo.name
	case "b_ricerca_prodotto_da"
		guo_ricerca.uof_ricerca_prodotto(dw_listini_fornitori_sel,"cod_prodotto_da")
	case "b_ricerca_prodotto_a"
		guo_ricerca.uof_ricerca_prodotto(dw_listini_fornitori_sel,"cod_prodotto_a")
	case "b_ricerca_fornitore"
		guo_ricerca.uof_ricerca_fornitore(dw_listini_fornitori_sel,"cod_fornitore")
end choose
end event

type dw_listini_fornitori_lista from uo_cs_xx_dw within w_listini_fornitori
integer x = 137
integer y = 40
integer width = 3538
integer height = 500
integer taborder = 20
string dataobject = "d_listini_fornitori_lista"
boolean hscrollbar = true
boolean vscrollbar = true
boolean livescroll = true
borderstyle borderstyle = stylelowered!
end type

event updatestart;call uo_cs_xx_dw::updatestart;integer li_i, li_i1
string  ls_tabella, ls_cod_prodotto, ls_cod_fornitore, ls_cod_fornitore_old, ls_flag_for_pref

if sqlca.sqlcode = 0 then
   li_i = 0
   do while li_i <= pcca.window_currentdw.rowcount()
      li_i = pcca.window_currentdw.getnextmodified(li_i, Primary!)

      if li_i = 0 then
         return
      end if

      ls_cod_prodotto = pcca.window_currentdw.getitemstring(li_i, "cod_prodotto")
      ls_cod_fornitore = pcca.window_currentdw.getitemstring(li_i, "cod_fornitore")
      ls_flag_for_pref = pcca.window_currentdw.getitemstring(li_i, "flag_for_pref")

      declare cu_listini cursor for select listini_fornitori.cod_fornitore from listini_fornitori where listini_fornitori.cod_azienda = :s_cs_xx.cod_azienda and listini_fornitori.cod_prodotto = :ls_cod_prodotto and listini_fornitori.cod_fornitore <> :ls_cod_fornitore and flag_for_pref = 'S';

      open cu_listini;

      li_i1 = 0
      do while 0 = 0
         li_i1 ++
         fetch cu_listini into :ls_cod_fornitore_old;

         if sqlca.sqlcode <> 0 or li_i1 = 1 then exit
      loop

      if sqlca.sqlcode = 0 and ls_flag_for_pref = 'S' then
      	g_mb.messagebox("Attenzione", "Fornitore Preferenziale già Assegnato" + ls_cod_fornitore_old + "!", &
                    exclamation!, ok!)
         pcca.error = pcca.window_currentdw.c_fatal
         close cu_listini;
      end if
      close cu_listini;
   loop
end if
end event

event pcd_validaterow;call super::pcd_validaterow;long ll_i
string ls_cod_prodotto, ls_cod_fornitore, ls_cod_valuta, ls_cod_fornitore_old
datetime ldt_data_inizio_val

f_listini_scaglioni()

if i_rownbr > 0 then
   if this.getitemstring(i_rownbr, "flag_for_pref") = "S" then
      ls_cod_prodotto = this.getitemstring(i_rownbr, "cod_prodotto")
      ls_cod_fornitore = this.getitemstring(i_rownbr, "cod_fornitore")
      ls_cod_valuta = this.getitemstring(i_rownbr, "cod_valuta")
      ldt_data_inizio_val = this.getitemdatetime(i_rownbr, "data_inizio_val")

      declare cu_fornitore cursor for select listini_fornitori.cod_fornitore from listini_fornitori where listini_fornitori.cod_azienda = :s_cs_xx.cod_azienda and listini_fornitori.cod_prodotto = :ls_cod_prodotto and listini_fornitori.cod_valuta = :ls_cod_valuta and listini_fornitori.data_inizio_val = :ldt_data_inizio_val and listini_fornitori.flag_for_pref = 'S' and listini_fornitori.cod_fornitore <> :ls_cod_fornitore;

      open cu_fornitore;

      ll_i = 0
      do while 0 = 0
         ll_i ++
         fetch cu_fornitore into :ls_cod_fornitore_old;

         if sqlca.sqlcode <> 0 or ll_i = 1 then exit
      loop

      if sqlca.sqlcode = 0 then
      	g_mb.messagebox("Attenzione", "Fornitore Preferenziale già segnalato: " + ls_cod_fornitore_old + "!", &
                    exclamation!, ok!)
         pcca.error = pcca.window_currentdw.c_fatal
         close cu_fornitore;
         return
      end if
      close cu_fornitore;
   end if
end if
end event

on pcd_setkey;call uo_cs_xx_dw::pcd_setkey;long ll_i


for ll_i = 1 to this.rowcount()
   if isnull(this.getitemstring(ll_i, "cod_azienda")) then
      this.setitem(ll_i, "cod_azienda", s_cs_xx.cod_azienda)
   end if
next
end on

event pcd_retrieve;call super::pcd_retrieve;long ll_errore


ll_errore = retrieve()

if ll_errore < 0 then
   pcca.error = c_fatal
end if
end event

event pcd_modify;call super::pcd_modify;cb_prezzo_acq.enabled=true
dw_listini_fornitori_det.object.b_ricerca_fornitore.enabled=false
dw_listini_fornitori_det.object.b_ricerca_prodotto.enabled=false

dw_folder.fu_selecttab(1)
end event

event pcd_new;call super::pcd_new;cb_prezzo_acq.enabled=true
dw_listini_fornitori_det.object.b_ricerca_fornitore.enabled=true
dw_listini_fornitori_det.object.b_ricerca_prodotto.enabled=true

dw_folder.fu_selecttab(1)
end event

event pcd_save;call super::pcd_save;cb_prezzo_acq.enabled=false
dw_listini_fornitori_det.object.b_ricerca_fornitore.enabled=false
dw_listini_fornitori_det.object.b_ricerca_prodotto.enabled=false

end event

event pcd_view;call super::pcd_view;cb_prezzo_acq.enabled=false
dw_listini_fornitori_det.object.b_ricerca_fornitore.enabled=false
dw_listini_fornitori_det.object.b_ricerca_prodotto.enabled=false

end event

event rowfocuschanged;call super::rowfocuschanged;if i_extendmode then
	string ls_cod_prodotto, ls_cod_misura_mag

	
	if this.getrow() > 0 then
		ls_cod_prodotto = this.getitemstring(this.getrow(),"cod_prodotto")		

      select anag_prodotti.cod_misura_mag
      into   :ls_cod_misura_mag
      from   anag_prodotti
      where  anag_prodotti.cod_azienda = :s_cs_xx.cod_azienda and 
             anag_prodotti.cod_prodotto = :ls_cod_prodotto;

		if sqlca.sqlcode = 0 then
	   	dw_listini_fornitori_det.modify("st_prezzo.text='Prezzo al " + ls_cod_misura_mag + "'")
	   	dw_listini_fornitori_det.modify("st_quantita.text='Quantità " + ls_cod_misura_mag + "'")
		else
			dw_listini_fornitori_det.modify("st_prezzo.text='Prezzo'")
			dw_listini_fornitori_det.modify("st_quantita.text='Quantità'")
		end if

		if not isnull(this.getitemstring(this.getrow(),"cod_misura")) then
		   cb_prezzo_acq.text = "Prezzo al " + this.getitemstring(this.getrow(),"cod_misura")
		else
		   cb_prezzo_acq.text = "Prezzo"
		end if
		if ls_cod_misura_mag <> this.getitemstring(this.getrow(),"cod_misura") then
			dw_listini_fornitori_det.modify("st_fattore_conv.text='(1 " + ls_cod_misura_mag + "=" + trim(f_double_string(this.getitemnumber(this.getrow(),"fat_conversione"))) + " " + this.getitemstring(this.getrow(),"cod_misura") + ")'")
		else
			dw_listini_fornitori_det.modify("st_fattore_conv.text=''")		
		end if
	end if
end if

end event

type dw_folder from u_folder within w_listini_fornitori
integer x = 23
integer y = 20
integer width = 4059
integer height = 540
integer taborder = 10
end type

